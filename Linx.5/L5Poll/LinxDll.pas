unit LinxDll;

interface

uses Classes, SysUtils, Dialogs, WinTypes, WinProcs,
     LinxIni;

const
     MAX_DELAY = 15;
     MAX_SEND_RETRIES = 5;

type
    L5Bool = LongInt;
    L5Int = LongInt;
    L5Word = Word;
    L5Long = LongInt;
    L5CharPtr = PChar;
    TMakeUnitListEvent = procedure of object;
    TFeedBackEvent = procedure( const sMensaje: String; iTerminal: Integer ) of object;
    TGaugeEvent = procedure( const sMsg: String; iValue: Integer ) of object;
    TL5Server = class;
    TL5Record = Class( TObject )
    private
      { Private declarations }
      FL5Server: TL5Server;
      FHandle: L5Int;
      FTipo: L5Int;
      FData: String;
      FAddress: String;
      FValidTokens: String;
      function GetRecordType: L5Int;
      function GetIsValid: Boolean;
      function GetIsNormal: Boolean;
      function GetIsStatus: Boolean;
      function GetData: String;
      function GetOriginalData: String;
      function GetAnomaly: String;
      function GetTerminalAddress: String;
      procedure SetData( const Value: String );
      procedure SetTerminalAddress( const sValue: String );
    protected
      { Protected declarations }
    public
      { Public declarations }
      constructor Create( Servidor: TL5Server);
      property Handle: L5Int read FHandle write FHandle;
      property Tipo: L5Int read GetRecordType write FTipo;
      property Data: String read GetData write SetData;
      property OriginalData: String read GetOriginalData;
      property Anomaly: String read GetAnomaly;
      property ValidTokens: String read FValidTokens write FValidTokens;
      property TerminalAddress: String read GetTerminalAddress write SetTerminalAddress;
      property Normal: Boolean read GetIsNormal;
      property Valid: Boolean read GetIsValid;
      property Status: Boolean read GetIsStatus;
    end;
    TL5Unit = Class( TObject )
    private
      { Private declarations }
      FUnitHandle: L5Int;
      FUnitAddress: String;
    protected
      { Protected declarations }
    public
      { Public declarations }
      constructor Create( const iHandle: L5Int; sTerminalAddress: String );
      property Handle: L5Int read FUnitHandle;
      property Address: String read FUnitAddress;
    end;
    TL5Server = Class( TObject )
    private
      { Private declarations }
      FSleep: Integer;
      FUsed: Boolean;
      FBefore: TStringList;
      FAfter: TStringList;
      FPrefix: String;
      FL5Data: L5CharPtr;
      FL5Address: L5CharPtr;
      FL5TerminalAddress: L5CharPtr;
      FOnFeedBack: TFeedBackEvent;
      FOnGaugeSetSize: TGaugeEvent;
      FOnGaugeAdvance: TGaugeEvent;
      FOnGaugeFinish: TGaugeEvent;
      FOnMakeUnitList: TMakeUnitListEvent;
      function DLL_L5MAKETABLE( iTableSize: Integer ): L5Bool;
      function DLL_L5ADDUNIT( Terminal: TTerminal ): L5Int;
      function DLL_L5RECEIVE( var sRecord: String; var iType: L5Int ): L5Int;
      function DLL_L5ADDRTEXT( const L5iUnit: L5Int ): String;
      function DLL_L5ADDRTEXT_MODE( const L5iUnit, L5Mode: L5Int ): String;
      function DLL_GETSTATETEXT( const iTerminal: Integer ): String;
      procedure DLL_L5SEND( const L5iHandle: L5Int; sRecord: String );
      function FormatIPAddress(const sDireccion: String): String;
      function FormatMasterStation(const sAddress: String): String;
      function GetUnits: L5Int;
      function GetHandle( const iTerminal: Integer ): L5Int;
      function GetTerminal: Integer;
      function GetRegistroEsNormal: Boolean;
      function GetRegistroLeido: String;
      function GetDatosEnviados: String;
      function DateTimeCommand( const dValue: TDateTime ): String;
      procedure InitSend;
      procedure SendDataFile( const iTerminal: Integer; sFileName, sInternalName: String );
      procedure SendFile( const iTerminal: Integer; sFileName: String );
      procedure SetSleepInterval(const Value: Integer);
      procedure SetValidTokens( const Value: String );
      procedure DoFeedBack( const iTerminal: Integer; sMsg: String );
      procedure DoGaugeSetSize( const sMsg: String; iSize: Integer );
      procedure DoGaugeAdvance( const sMsg: String; iStep: Integer );
      procedure DoGaugeFinish( const sMsg: String; iValue: Integer );
    protected
      { Protected declarations }
      FUnits: L5Int;
      FRegistro: TL5Record;
    public
      { Public declarations }
      constructor Create;
      destructor Destroy; override;
      property Registro: TL5Record read FRegistro;
      property RegistroNormal: Boolean read GetRegistroEsNormal;
      property RegistroLeido: String read GetRegistroLeido;
      property DatosEnviados: String read GetDatosEnviados;
      property SleepInterval: Integer write SetSleepInterval;
      property Terminal: Integer read GetTerminal;
      property Units: L5Int read GetUnits;
      property Used: Boolean read FUsed;
      property ValidTokens: String write SetValidTokens;
      property OnFeedBack: TFeedBackEvent read FOnFeedBack write FOnFeedBack;
      property OnGaugeSetSize: TGaugeEvent read FOnGaugeSetSize write FOnGaugeSetSize;
      property OnGaugeAdvance: TGaugeEvent read FOnGaugeAdvance write FOnGaugeAdvance;
      property OnGaugeFinish: TGaugeEvent read FOnGaugeFinish write FOnGaugeFinish;
      property OnMakeUnitList: TMakeUnitListEvent read FOnMakeUnitList write FOnMakeUnitList;
      function Init: Boolean; virtual;
      function Read: Boolean; virtual;
      function WriteCommand( const iTerminal: Integer; sCommand: String ): Boolean;
      function WriteDirective( const iTerminal: Integer; sDirective: String ): Boolean;
      function WriteToOneUnit( const iTerminal: Integer; sMensaje: String ): Boolean;
      function Write( const sMensaje: String ): Boolean; virtual;
      function GetTerminalInfo( const iTerminal: Integer ): TTerminal; overload;
      function GetTerminalInfo: TTerminal; overload;
      function GetTerminalAddress( const iTerminal: Integer ): String; virtual;
      function GetTerminalIPAddress( const iTerminal: Integer ): String;
      function GetTerminalMasterStation( const iTerminal: Integer ): String;
      procedure AbortApp( const iTerminal: Integer );
      procedure Close; virtual;
      procedure GetTerminalList( Lista: TStrings );
      procedure MakeNodeList;
      procedure ResetUnit( const iTerminal: Integer );
      procedure RebootUnit( const iTerminal: Integer );
      procedure RestartUnit( const iTerminal: Integer );
      procedure SendApplicationFile( const iTerminal: Integer; sFileName: String );
      procedure SendEmployeeListFile( const iTerminal: Integer; sFileName: String );
      procedure SendParameterFile( const iTerminal: Integer; sFileName: String );
      procedure SetDateTime( const iTerminal: Integer; dValue: TDateTime );
      procedure SetDateTimeAll( const dValue: TDateTime );
      procedure SendOSFile( const iTerminal: Integer; sFileName: String );
      procedure ShowDateTime( const iTerminal: Integer );
      procedure ShowDisplay( const iTerminal: Integer );
      procedure ShowSerialNumber( const iTerminal: Integer );
      procedure ShowVersion( const iTerminal: Integer );
      procedure Verify;
    end;
    TL5DummyServer = class( TL5Server )
    private
      { Private declarations }
      FLista: TStrings;
      FPtr: Integer;
    public
      { Public declarations }
      constructor Create;
      destructor Destroy; override;
      function GetTerminalAddress( const iTerminal: Integer ): String; override;
      function Init: Boolean; override;
      function Read: Boolean; override;
      function Write( const sMensaje: String ): Boolean; override;
      procedure MakeNodeList;
      procedure Close; override;
    end;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     LinxIniBase,
     FLinxTools;

const
     CR_LF = Chr( 13 ) + Chr( 10 );
     NO_SUBNETWORK = 0;
     SET_SUBNETWORK = 1;
     ADD_NETWORK = 2;
     ADD_SOCKET = 3;
     NETWORK_NODE = 0;
     NODE_MASTER_STATION = 1;
     DLL_FALSE_VALUE = 0;
     DLL_TRUE_VALUE = 1;
     MAX_SEND_ATTEMPTS = 3;
     SUBNETWORK_DELAY = 80;
     RECEIVE_TYPE_STATUS = 0;
     RECEIVE_TYPE_NORMAL = 1;
     RECEIVE_TYPE_BASIC = 100;
     RECEIVE_TYPE_ND = 101;
     RECEIVE_TYPE_NULL = 102;
     LINX_BASIC_ABORT = 'BASIC';
     LINX_BASIC_STATUS = 'ND';
     LINX_BASIC_ERROR = 'BASIC ERROR:';
     LINX_BASIC_COMMAND = '>';
     LINX_BASIC_DOUBLE_COMMAND = '>>';
     LINX_BASIC_DIRECTIVE = '<';
     LINX_ABORT = '>>A';
     LINX_RESTART = '>>G';
     LINX_RESET = '>F';
     LINX_REBOOT = '>R';
     LINX_SET_DATE_TIME = '>T ';
     LINX_SHOW_DISPLAY = '<D';
     LINX_SHOW_DISPLAY_REPLY = 'ND D';
     LINX_SHOW_SERIAL_NUMBER = '<S';
     LINX_SHOW_SERIAL_NUMBER_REPLY = 'ND S';
     LINX_SHOW_DATE_TIME = '<T';
     LINX_SHOW_DATE_TIME_REPLY = 'ND T';
     LINX_SHOW_VERSION = '<V';
     LINX_SHOW_VERSION_REPLY = 'ND V';
     ESTACION_CONCENTRADORA_EN_LINEA = 'CO';
     ESTACION_FUERA_DE_LINEA = 'OF';
     ESTACION_MAESTRA_EN_LINEA = 'MO';
     ESTACION_NORMAL_EN_LINEA = 'ON';
     ESTACION_SUBMAESTRA_EN_LINEA = 'SO';
     PARAMETER_FILE_NAME = 'ALARMA';
     EMPLOYEE_LIST_FILE_NAME = 'EMPLEADO';
     IP_ADDRESS_LENGTH = 20;
     L5_ADDRESS_SIZE = 34;
     L5_DATA_SIZE = 200;
     L5_STATE_TEXT_LEN = 11;
     LNAPI32_DLL = 'LnApi32.dll';

{ ******** LNAPI32.DLL Linx V Calls ******* }

function _LnADDUNIT( L5cpAddress: L5CharPtr ): L5Int; stdcall; external LNAPI32_DLL name 'LnAddUnit';
function _LnC: L5Int; stdcall; external LNAPI32_DLL name 'LnC';
function _LnOnlineCount: L5Int; stdcall; external LNAPI32_DLL name 'LnOnlineCount';
function _LnERRTEXT( L5cpErrorText: L5CharPtr ): L5Word; stdcall; external LNAPI32_DLL name 'LnErrText';
function _LnGETINFO( L5WhatInfo: L5Long; L5Info: L5CharPtr ): L5Long; stdcall; external LNAPI32_DLL name 'LnSetOption';
function _LnIDLE: L5Bool; stdcall; external LNAPI32_DLL name 'LnIdle';
function _LnMAKETABLE( L5iTableSize: L5Int ): L5Bool; stdcall; external LNAPI32_DLL name 'LnMakeTable';
function _LnOPEN: L5Bool; stdcall; external LNAPI32_DLL name 'LnOpen';
function _LnREADY( L5iHandle: L5Int ): L5Bool; stdcall; external LNAPI32_DLL name 'LnReady';
function _LnRECEIVE( L5cpRecord: L5CharPtr; var L5iRecordType: L5Int ): L5Int; stdcall; external LNAPI32_DLL name 'LnReceive';
function _LnSETOPTION( L5iOption: L5Int; L5lValue: L5Long ): L5Long; stdcall; external LNAPI32_DLL name 'LnSetOption';
function _LnSTATETEXT( L5iHandle: L5Int; L5cpStateText: L5CharPtr ): L5Int; stdcall; external LNAPI32_DLL name 'LnStateText';
procedure _LnADDRTEXT( L5iUnit: L5Int; L5cpAddress: L5CharPtr; L5iMode: L5Int ); stdcall; external LNAPI32_DLL name 'LnAddrText';
procedure _LnCLOSE; stdcall; external LNAPI32_DLL name 'LnClose';
procedure _LnSEND( L5iHandle: L5Int; L5cpRecord: L5CharPtr ); stdcall; external LNAPI32_DLL name 'LnSend';
procedure _LnSETMONITOR( L5iMode: L5Int ); stdcall; external LNAPI32_DLL name 'LnSetMonitor';
procedure _LnSETSERVER( L5iMode: L5Int ); stdcall; external LNAPI32_DLL name 'LnSetServer';
procedure _LnSETSUBNETWORK( L5iSubNetwork: L5Int ); stdcall; external LNAPI32_DLL name 'LnSetSubNetwork';

{ *********** TL5Record ********** }

constructor TL5Record.Create( Servidor: TL5Server );
begin
     inherited Create;
     FL5Server := Servidor;
     Handle := 0;
     Tipo := 0;
     Data := '';
     FValidTokens := '';
end;

function TL5Record.GetRecordType: L5Int;
begin
     case FTipo of
          0: Result := RECEIVE_TYPE_STATUS;
          1..99: Result := RECEIVE_TYPE_NORMAL;
          100: Result := RECEIVE_TYPE_BASIC;
          101: Result := RECEIVE_TYPE_ND;
     else
         Result := RECEIVE_TYPE_NULL;
     end;
end;

function TL5Record.GetTerminalAddress: String;
begin
     {Result := Copy( FAddress, ( iLen - 4 ), 2 ) +
               Copy( FAddress, ( iLen - 1 ), 2 ) +
               Copy( FAddress, 1, 6 );}
     Result := LinxIni.IniValues.ListaTerminales.GetTerminalID( FAddress );
     Result := Copy( Result + StringOfChar( 'X', 10 ), 1, 10 );
end;

procedure TL5Record.SetTerminalAddress( const sValue: String );
begin
     FAddress := sValue;
end;

function TL5Record.GetIsValid: Boolean;
begin
     Result := ( Pos( Copy( FData, 1, 1 ), FValidTokens ) > 0 );
end;

function TL5Record.GetIsNormal: Boolean;
begin
     if ( Tipo = RECEIVE_TYPE_NORMAL ) OR ( Tipo = RECEIVE_TYPE_NULL ) then
        Result := GetIsValid
     else
         Result := False;
end;

function TL5Record.GetIsStatus: Boolean;
begin
     Result := ( Tipo = RECEIVE_TYPE_STATUS );
end;

function TL5Record.GetOriginalData: String;
begin
     Result := FAddress + '-> (' + IntToStr( FTipo ) + ')=' + FData;
end;

function TL5Record.GetData: String;
begin
     Result := TerminalAddress + FData;
end;

function TL5Record.GetAnomaly: String;
var
   dValue: TDateTime;
begin
     if ( FData = ESTACION_FUERA_DE_LINEA ) then
        Result := 'Fuera De L�nea ( OFF-line )'
     else
     if ( FData = ESTACION_NORMAL_EN_LINEA ) then
        Result := 'En L�nea ( ON-line )'
     else
     if ( FData = ESTACION_MAESTRA_EN_LINEA ) then
        Result := 'Maestra En L�nea ( ON-line )'
     else
     if ( FData = ESTACION_SUBMAESTRA_EN_LINEA ) then
        Result := 'Sub-Maestra En L�nea ( ON-line )'
     else
     if ( FData = ESTACION_CONCENTRADORA_EN_LINEA ) then
        Result := 'Concentradora En L�nea ( ON-line )'
     else
     if ( Pos( LINX_BASIC_STATUS, UpperCase( FData ) ) > 0 ) then
     begin
          if ( Copy( FData, 1, 4 ) = LINX_SHOW_DATE_TIME_REPLY ) then
          begin
               try
                  {
                  12345678901234567890
                  ND T 131202940002197
                  }
                  dValue := EncodeDate( ZetaCommonTools.CodificaYear( StrToIntDef( Copy( FData, 14, 2 ), 0 ) ),
                                        StrToIntDef( Copy( FData, 16, 2 ), 0 ),
                                        StrToIntDef( Copy( FData, 18, 2 ), 0 ) ) +
                            EncodeTime( StrToIntDef( Copy( FData, 6, 2 ), 0 ),
                                        StrToIntDef( Copy( FData, 8, 2 ), 0 ),
                                        StrToIntDef( Copy( FData, 10, 2 ), 0 ),
                                        StrToIntDef( Copy( FData, 12, 2 ), 0 ) );
                  Result := 'Fecha: ' + FormatDateTime( 'dd/mmm/yyyy hh:nn:ss', dValue );
               except
                     Result := 'Fecha Inv�lida: ' + FData;
               end;
          end
          else
              Result := Copy( FData, 5, ( Length( FData ) - 4 ) );
     end
     else
         if ( Pos( LINX_BASIC_ERROR, UpperCase( FData ) ) > 0 ) then
            Result := FData
         else
             if GetIsValid then
                Result := 'ANOMALIA ' + IntToStr( FTipo ) + ':' + FData
             else
                 Result := 'FORMATO ILEGAL' + ':' + FData;
end;

procedure TL5Record.SetData( const Value: String );
begin
     FData := Value;
end;

{ ******* TL5Unit ********* }

constructor TL5Unit.Create( const iHandle: L5Int; sTerminalAddress: String );
begin
     inherited Create;
     FUnitHandle := iHandle;
     FUnitAddress := sTerminalAddress;
end;

{ ******* TL5Server ********* }

constructor TL5Server.Create;
begin
     inherited Create;
     FUnits := 0;
     FRegistro := TL5Record.Create( Self );
     FPrefix := '';
     FUsed := False;
     GetMem( FL5TerminalAddress, LinxIni.IniValues.GetAddressLength + 1 );
     GetMem( FL5Data, L5_DATA_SIZE );
     GetMem( FL5Address, L5_ADDRESS_SIZE );
     FBefore := TStringList.Create;
     FAfter := TStringList.Create;
end;

destructor TL5Server.Destroy;
begin
     Close;
     FAfter.Free;
     FBefore.Free;
     FreeMem( FL5Address, L5_ADDRESS_SIZE );
     FreeMem( FL5Data, L5_DATA_SIZE );
     FreeMem( FL5TerminalAddress, LinxIni.IniValues.GetAddressLength + 1 );
     FRegistro.Free;
     inherited Destroy;
end;

procedure TL5Server.MakeNodeList;
var
   iTerminals: L5Int;
begin
     iTerminals := _LnC;
     if ( iTerminals > 0 ) then
     begin
          if ( Units <> iTerminals ) then
          begin
               FUnits := iTerminals;
               if Assigned( FOnMakeUnitList ) then
                  FOnMakeUnitList;
          end;
     end;
end;

procedure TL5Server.SetValidTokens( const Value: String );
begin
     FRegistro.ValidTokens := Value;
end;

procedure TL5Server.SetSleepInterval(const Value: Integer);
begin
     FSleep := Value;
end;

function TL5Server.Init: Boolean;
var
   i: Integer;
   iValue: L5Long;
begin
     _LnSETOPTION( 0, 0 );     { Inicializa con Defaults }
     with LinxIni.IniValues.Options do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with Option[ i ] do
               begin
                    if IsDefault then
                    begin
                         if LinxIni.IniValues.UsarListaTerminales then { and ( Win32Platform <> VER_PLATFORM_WIN32_NT ) }
                         begin
                              { La siguiente secuencia se tomo de Option.Txt de L5SDK }
                              case Number of
                                   { Set the time to between offline polls to 1/5th of the broadcast time }
                                   Linx5OptNum7:
                                   begin
                                        iValue := _LnSETOPTION( -Linx5OptNum7, 0 );
                                        _LnSETOPTION( Linx5OptNum7, Round( iValue / 5 ) );
                                   end;
                                   { Disable broadcast mode }
                                   Linx5OptNum9: _LnSETOPTION( Linx5OptNum9, L5_Broadcast_False );
                              end;
                              { Fin de secuencia }
                         end;
                    end
                    else
                        _LnSETOPTION( Number, Value );
               end;
          end;
     end;
     with LinxIni.IniValues do
     begin
          SetSleepInterval( Sleep );
          if UsarListaTerminales then
          begin
               with ListaTerminales do
               begin
                    if ( DLL_L5MAKETABLE( Count ) = DLL_FALSE_VALUE ) then
                       raise Exception.Create( 'No Fu� Posible Crear Tabla De Terminales Linx' );
                    for i := 0 to ( Count - 1 ) do
                    begin
                         if ( DLL_L5ADDUNIT( TTerminal( Objects[ i ] ) ) = 0 ) then
                            raise Exception.Create( 'No Hay Espacio Disponible En Tabla De Terminales Linx' );
                    end;
               end;
          end;
     end;
     if ( _LnOPEN() = DLL_FALSE_VALUE ) then
     begin
          Close;
          raise Exception.Create( 'Servidor De Linx 5 No Pudo Ser Inicializado' );
     end
     else
     begin
          MakeNodeList;
          FUsed := True;
     end;
     if Used then
        Result := True
     else
     begin
          Result := False;
          Close;
     end;
end;

procedure TL5Server.Verify;
begin
     if ( Units = 0 ) then
        raise Exception.Create( 'No Se Detectaron Terminales' )
     else
         if ( LinxIni.IniValues.Terminales > 0 ) and ( Units <> LinxIni.IniValues.Terminales ) then
            raise Exception.Create( 'Teminales Configuradas: ' + IntToStr( LinxIni.IniValues.Terminales ) +
                                    ' Terminales Detectadas: ' + IntToStr( Units ) )
end;

procedure TL5Server.Close;
begin
     _LnCLOSE;
     FUsed := False;
end;

function TL5Server.GetUnits: L5Int;
begin
     Result := FUnits;
end;

function TL5Server.GetTerminal: Integer;
begin
     Result := Integer( FRegistro.Handle );
end;

function TL5Server.GetTerminalInfo( const iTerminal: Integer ): TTerminal;
var
   sAddress: String;
begin
     sAddress := GetTerminalAddress( iTerminal );
     sAddress := Format( '%s %s', [ FormatIPAddress( sAddress ), FormatMasterStation( sAddress ) ] );
     Result := LinxIni.IniValues.ListaTerminales.GetTerminalInfo( sAddress );
end;

function TL5Server.GetTerminalInfo: TTerminal;
begin
     Result := Self.GetTerminalInfo( Terminal );
end;

function TL5Server.FormatMasterStation( const sAddress: String ): String;
var
   iPtr: Integer;
begin
     iPtr := Pos( '-', sAddress );
     if ( iPtr > 0 ) then
        Result := Copy( sAddress, ( iPtr - 2 ), 2 ) + Copy( sAddress, ( iPtr + 1 ), 2 )
     else
         Result := '0000';
end;

function TL5Server.GetTerminalMasterStation( const iTerminal: Integer ): String;
begin
     Result := FormatMasterStation( GetTerminalAddress( iTerminal ) );
end;

function TL5Server.FormatIPAddress( const sDireccion: String ): String;
var
   sAddress: String;
begin
     sAddress := sDireccion;
     Result := Format( '%3.3d.%3.3d.%3.3d.%3.3d', [ StrToIntDef( strToken( sAddress, '.' ), 0 ),
                                                    StrToIntDef( strToken( sAddress, '.' ), 0 ),
                                                    StrToIntDef( strToken( sAddress, '.' ), 0 ),
                                                    StrToIntDef( strToken( sAddress, ' ' ), 0 ) ] );
end;

function TL5Server.GetTerminalIPAddress( const iTerminal: Integer ): String;
begin
     Result := FormatIPAddress( GetTerminalAddress( iTerminal ) );
end;

function TL5Server.GetTerminalAddress( const iTerminal: Integer ): String;
begin
     Result := DLL_L5ADDRTEXT( L5Int( iTerminal ) );
end;

function TL5Server.GetHandle( const iTerminal: Integer ): L5Int;
begin
     Result := L5Int( iTerminal );
end;

procedure TL5Server.GetTerminalList( Lista: TStrings );
var
   i, iMax: L5Int;
   sTitulo, sData: String;
begin
     iMax := _LnC;
     with Lista do
     begin
          BeginUpdate;
          Clear;
          for i := 1 to iMax do
          begin
               sData := DLL_L5ADDRTEXT_MODE( i, 1 );
               sData := GetIPSubAddress( sData ) +
                        GetIPSubAddress( sData ) +
                        GetIPSubAddress( sData ) +
                        GetIPSubAddress( sData ) +
                        ' ' +
                        sData;
               sTitulo := 'Terminal # ' + IntToStr( i );
               AddObject( sTitulo, TTerminal.Create( sTitulo + '=' + sData ) );
          end;
          EndUpdate;
     end;
end;

function TL5Server.GetRegistroEsNormal: Boolean;
begin
     Result := FRegistro.Normal;
end;

function TL5Server.GetDatosEnviados: String;
begin
     Result := FRegistro.OriginalData;
end;

function TL5Server.GetRegistroLeido: String;
begin
     with FRegistro do
     begin
          if Normal then
             Result := Data
          else
              if Status then
                 Result := DLL_GETSTATETEXT( Handle )
              else
                  Result := Anomaly;
     end;
end;

function TL5Server.WriteToOneUnit( const iTerminal: Integer; sMensaje: String ): Boolean;
begin
     FRegistro.Handle := GetHandle( iTerminal );
     Result := Write( sMensaje );
end;

function TL5Server.WriteCommand( const iTerminal: Integer; sCommand: String ): Boolean;
begin
     Result := WriteToOneUnit( iTerminal, LINX_BASIC_COMMAND + sCommand );
end;

function TL5Server.WriteDirective( const iTerminal: Integer; sDirective: String ): Boolean;
begin
     Result := WriteToOneUnit( iTerminal, LINX_BASIC_DIRECTIVE + sDirective );
end;

function TL5Server.Read: Boolean;
var
   L5iType: L5Int;
   L5iHandle: L5Int;
   sRecord: String;
begin
     sRecord := '';
     L5iType := 0;
     L5iHandle := DLL_L5Receive( sRecord, L5iType );
     Result := ( L5iHandle > 0 );
     if Result then
     begin
          with FRegistro do
          begin
               Handle := L5iHandle;
	           Tipo := L5iType;
               TerminalAddress := Self.GetTerminalAddress( Handle );
	           Data := sRecord;
          end;
     end
     else
         Sleep( FSleep ); { Dormir Para Liberar CPU }
end;

function TL5Server.Write( const sMensaje: String ): Boolean;
var
   bCtr: Byte;
begin
     Result := False;
     if ( FRegistro.Handle >= 0 ) then { Antes era > 0, lo cual bloqueaba Broadcasts }
     begin
          bCtr := 1;
          while not Result and ( bCtr <= MAX_SEND_ATTEMPTS ) do
          begin
               { 0 = Broadcast }
	           if ( FRegistro.Handle = 0 ) or ( _LnREADY( FRegistro.Handle ) <> DLL_FALSE_VALUE ) then
               begin
		            DLL_L5Send( FRegistro.Handle, sMensaje );
		            Result := True;
               end;
               Inc( bCtr );
          end;
     end;
end;

procedure TL5Server.DoFeedBack( const iTerminal: Integer; sMsg: String );
begin
     if Assigned( FOnFeedBack ) then
        FOnFeedBack( sMsg, iTerminal );
end;

procedure TL5Server.ShowDisplay( const iTerminal: Integer );
begin
     if WriteToOneUnit( iTerminal, LINX_SHOW_DISPLAY ) then
        DoFeedBack( iTerminal, 'Mostrar Pantalla' )
     else
         DoFeedBack( iTerminal, DLL_GETSTATETEXT( iTerminal ) );
end;

procedure TL5Server.ShowSerialNumber( const iTerminal: Integer );
begin
     if WriteToOneUnit( iTerminal, LINX_SHOW_SERIAL_NUMBER ) then
        DoFeedBack( iTerminal, 'Mostrar # de Serie' )
     else
         DoFeedBack( iTerminal, DLL_GETSTATETEXT( iTerminal ) );
end;

procedure TL5Server.ShowDateTime( const iTerminal: Integer );
begin
     if WriteToOneUnit( iTerminal, LINX_SHOW_DATE_TIME ) then
        DoFeedBack( iTerminal, 'Mostrar Fecha y Hora' )
     else
         DoFeedBack( iTerminal, DLL_GETSTATETEXT( iTerminal ) );
end;

procedure TL5Server.ShowVersion( const iTerminal: Integer );
begin
     if WriteToOneUnit( iTerminal, LINX_SHOW_VERSION ) then
        DoFeedBack( iTerminal, 'Mostrar Versi�n Interna' )
     else
         DoFeedBack( iTerminal, DLL_GETSTATETEXT( iTerminal ) );
end;

procedure TL5Server.SetDateTimeAll( const dValue: TDateTime );
begin
     WriteToOneUnit( 0, DateTimeCommand( dValue ) );
end;

procedure TL5Server.SetDateTime( const iTerminal: Integer; dValue: TDateTime );
begin
     WriteToOneUnit( iTerminal, DateTimeCommand( dValue ) );
end;

function TL5Server.DateTimeCommand( const dValue: TDateTime ): String;
begin
     Result := LINX_SET_DATE_TIME +
               FormatDateTime( 'hhnnss', dValue ) +
               '00' +
               FormatDateTime( 'yymmdd', dValue ) +
               Format( '%d', [ DayOfWeek( dValue ) ] );
end;

procedure TL5Server.AbortApp( const iTerminal: Integer );
begin
     if WriteToOneUnit( iTerminal, LINX_ABORT ) then
        DoFeedBack( iTerminal, 'Interrumpir' )
     else
         DoFeedBack( iTerminal, DLL_GETSTATETEXT( iTerminal ) );
end;

procedure TL5Server.ResetUnit( const iTerminal: Integer );
begin
     if WriteToOneUnit( iTerminal, LINX_RESET ) then
        DoFeedBack( iTerminal, 'Resetear Terminal' )
     else
         DoFeedBack( iTerminal, DLL_GETSTATETEXT( iTerminal ) );
end;

procedure TL5Server.RebootUnit( const iTerminal: Integer );
begin
     if WriteToOneUnit( iTerminal, LINX_REBOOT ) then
        DoFeedBack( iTerminal, 'Reboot Terminal' )
     else
         DoFeedBack( iTerminal, DLL_GETSTATETEXT( iTerminal ) );
end;

procedure TL5Server.RestartUnit( const iTerminal: Integer );
begin
     if WriteToOneUnit( iTerminal, LINX_RESTART ) then
        DoFeedBack( iTerminal, 'Reiniciar Programa' )
     else
         DoFeedBack( iTerminal, DLL_GETSTATETEXT( iTerminal ) );
end;

procedure TL5Server.SendParameterFile( const iTerminal: Integer; sFileName: String );
begin
     SendDataFile( iTerminal, sFileName, PARAMETER_FILE_NAME );
     RebootUnit( iTerminal );
end;

procedure TL5Server.SendEmployeeListFile( const iTerminal: Integer; sFileName: String );
begin
     SendDataFile( iTerminal, sFileName, EMPLOYEE_LIST_FILE_NAME );
     RebootUnit( iTerminal );
end;

procedure TL5Server.InitSend;
begin
     FBefore.Clear;
     FAfter.Clear;
     FPrefix := '';
end;

procedure TL5Server.SendDataFile( const iTerminal: Integer; sFileName, sInternalName: String );
begin
     InitSend;
     with FBefore do
     begin
          Add( '>+N' + sInternalName );
          Add( '>+O' );
     end;
     with FAfter do
     begin
          Add( '>+C' );
     end;
     FPrefix := '>+A';
     SendFile( iTerminal, sFileName );
end;

procedure TL5Server.SendApplicationFile( const iTerminal: Integer; sFileName: String );
begin
     InitSend;
     SendFile( iTerminal, sFileName );
end;

procedure TL5Server.SendOSFile( const iTerminal: Integer; sFileName: String );
begin
     InitSend;
     SendFile( iTerminal, sFileName );
end;

procedure TL5Server.SendFile( const iTerminal: Integer; sFileName: String );
var
   oByteFile: File of Byte;
   oFile: TextFile;
   sTextOut: String;
   i, iSize, iSent: LongInt;
begin
     try
        AssignFile( oByteFile, sFileName );
        Reset( oByteFile );
        iSize := FileSize( oByteFile );
        DoGaugeSetSize( '', iSize );
     finally
            CloseFile( oByteFile );
     end;
     try
        iSent := 0;
        AssignFile( oFile, sFileName );
        FileMode := 0;  { Set file access to read only  }
        Reset( oFile );
        for i := 0 to ( FBefore.Count - 1 ) do
            WriteToOneUnit( iTerminal, FBefore.Strings[ i ] );
        while not Eof( oFile ) do
        begin
             Readln( oFile, sTextOut );
             WriteToOneUnit( iTerminal, FPrefix + sTextOut );
             iSize := Length( sTextOut );
             DoGaugeAdvance( '', iSize );
             iSent := iSent + iSize;
        end;
        for i := 0 to ( FAfter.Count - 1 ) do
            WriteToOneUnit( iTerminal, FAfter.Strings[ i ] );
     finally
            CloseFile( oFile );
     end;
     DoGaugeFinish( '', 0 );
     DoFeedBack( iTerminal, 'Archivo ' + ExtractFileName( sFileName ) + ' Fu� Enviado ( ' + IntToStr( iSent ) + ' bytes )' );
end;

procedure TL5Server.DoGaugeSetSize( const sMsg: String; iSize: Integer );
begin
     if Assigned( FOnGaugeSetSize ) then
        FOnGaugeSetSize( sMsg, iSize );
end;

procedure TL5Server.DoGaugeAdvance( const sMsg: String; iStep: Integer );
begin
     if Assigned( FOnGaugeAdvance ) then
        FOnGaugeAdvance( sMsg, iStep );
end;

procedure TL5Server.DoGaugeFinish( const sMsg: String; iValue: Integer );
begin
     if Assigned( FOnGaugeFinish ) then
        FOnGaugeFinish( sMsg, iValue );
end;

{ *************** L5Dll Wrappers ************** }

function TL5Server.DLL_GETSTATETEXT( const iTerminal: Integer ): String;
var
   L5State: L5Int;
begin
     L5State := _LnStateText( iTerminal, FL5Data );
     Result := StrPas( FL5Data ) + ' (' + IntToStr( L5State ) + ')';
end;

function TL5Server.DLL_L5MAKETABLE( iTableSize: Integer ): L5Bool;
begin
     Result := _LnMAKETABLE( L5Int( iTableSize + 1 ) )
end;

function TL5Server.DLL_L5ADDUNIT( Terminal: TTerminal ): L5Int;
begin
     FL5TerminalAddress := StrPCopy( FL5TerminalAddress, Terminal.LinxAddress );
     Result := _LnADDUNIT( FL5TerminalAddress );
end;

function TL5Server.DLL_L5RECEIVE( var sRecord: String; var iType: L5Int ): L5Int;
begin
     Result := _LnRECEIVE( FL5Data, iType );
     sRecord := StrPas( FL5Data );
end;

function TL5Server.DLL_L5ADDRTEXT( const L5iUnit: L5Int ): String;
begin
     Result := DLL_L5ADDRTEXT_MODE( L5iUnit, NODE_MASTER_STATION );
end;

function TL5Server.DLL_L5ADDRTEXT_MODE( const L5iUnit, L5Mode: L5Int ): String;
begin
     _LnADDRTEXT( L5iUnit, FL5Address, L5Mode );
     Result := StrPas( FL5Address );
end;

procedure TL5Server.DLL_L5SEND( const L5iHandle: L5Int; sRecord: String );
begin
     FL5Data := StrPLCopy( FL5Data, sRecord, ( L5_DATA_SIZE - 1 ) );
     _LnSEND( L5iHandle, FL5Data );
end;

{ ******* TL5DummyServer ********* }

constructor TL5DummyServer.Create;
begin
     inherited Create;
     FLista := TStringList.Create;
end;

destructor TL5DummyServer.Destroy;
begin
     FLista.Free;
     inherited Destroy;
end;

function TL5DummyServer.Init: Boolean;
begin
     MakeNodeList;
     FUsed := True;
     if Used then
     begin
          Result := True;
          Randomize;
     end
     else
     begin
          Result := False;
          Close;
     end;
     with FLista do
     begin
          Clear;
          LoadFromFile( 'D:\3Win_13\Pruebas\Reloj.dat' );
          FPtr := 0;
     end;
end;

procedure TL5DummyServer.MakeNodeList;
var
   iTerminals: Integer;
begin
     iTerminals := 10;
     if ( iTerminals > 0 ) then
     begin
          if ( Units <> iTerminals ) then
          begin
               FUnits := iTerminals;
               if Assigned( FOnMakeUnitList ) then
                  FOnMakeUnitList;
          end;
     end;
end;

function TL5DummyServer.GetTerminalAddress( const iTerminal: Integer ): String;
begin
     Result := Format( '255255%3.3d055 01-%2.2d', [ iTerminal, iTerminal ] );
end;

procedure TL5DummyServer.Close;
begin
     FUsed := False;
end;

function TL5DummyServer.Read: Boolean;
var
   iValue: Integer;
   sData: String;

function RandomTerminal: L5Int;
begin
     Result := L5Int( Integer( Random( 10 ) + 1 ) );
end;

begin
     MakeNodeList;
     iValue := Integer( Random( 100 ) );
     Result := ( iValue >= 80 ) and ( iValue <= 99 );
     if Result then
     begin
          with FRegistro do
          begin
               Handle := RandomTerminal;
               TerminalAddress := Self.GetTerminalAddress( Handle );
               {
               case iValue of
                    99:
                    begin
                         Tipo := RECEIVE_TYPE_BASIC;
                         Data := 'Basic Error At Line XXX ' + DateTimeToStr( Now );
                    end;
                    98:
                    begin
                         Tipo := RECEIVE_TYPE_ND;
                         Data := 'Non-Determined Message ' + DateTimeToStr( Now );
                    end;
                    97:
                    begin
                         Tipo := RECEIVE_TYPE_STATUS;
                         Data := 'Status Message ' + DateTimeToStr( Now );
                    end;
                    0..96:
                    begin
               }
                         Tipo := RECEIVE_TYPE_NORMAL;
                         with FLista do
                         begin
                              if ( FPtr < Count ) then
                              begin
                                   sData := Strings[ FPtr ];
                                   Data := Copy( sData, 11, ( Length( sData ) - 10 ) );
                                   FPtr := FPtr + 1;
                              end
                              else
                                  Result := False;
                         end;
               {
                    end;
               end;
               }
          end;
     end;
end;

function TL5DummyServer.Write( const sMensaje: String ): Boolean;
begin
     Result := ( FRegistro.Handle > 0 );
end;

end.
