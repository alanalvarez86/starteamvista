unit FTerminales;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Mask, Buttons, ExtCtrls,
     {$ifndef VER130}MaskUtils,{$endif}
     LinxIni,
     LinxDLL,
     ZBaseDlgModal;

type
  TCapturarTerminales = class(TZetaDlgModal)
    Detectar: TBitBtn;
    PanelControles: TPanel;
    DescriptionLBL: TLabel;
    UnitIDLBL: TLabel;
    MasterIDLBL: TLabel;
    DireccionLBL: TLabel;
    Description: TEdit;
    MasterID: TMaskEdit;
    UnitID: TMaskEdit;
    DireccionIP: TMaskEdit;
    PanelLista: TPanel;
    TerminalList: TListBox;
    PanelABC: TPanel;
    Agregar: TBitBtn;
    Borrar: TBitBtn;
    Modificar: TBitBtn;
    lblIdentifica: TLabel;
    edIdentifica: TEdit;
    UseTerminalList: TCheckBox;
    AlarmaLBL: TLabel;
    EmpleadosLBL: TLabel;
    Alarma: TEdit;
    Empleados: TEdit;
    BuscarAlarma: TSpeedButton;
    BuscarEmpleados: TSpeedButton;
    OpenDialog: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure AgregarClick(Sender: TObject);
    procedure BorrarClick(Sender: TObject);
    procedure ModificarClick(Sender: TObject);
    procedure UseTerminalListClick(Sender: TObject);
    procedure TerminalListClick(Sender: TObject);
    procedure HayCambios(Sender: TObject);
    procedure HayCambiosDescripcion(Sender: TObject);
    procedure DetectarClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure edIdentificaEnter(Sender: TObject);
    procedure BuscarEmpleadosClick(Sender: TObject);
    procedure BuscarAlarmaClick(Sender: TObject);
  private
    { Private declarations }
    FRefreshing: Boolean;
    FLinxServer: TL5Server;
    function BuscaArchivo(const sFileName, sFilter, sTitle: String): String;
    function VerificaDeteccion: Boolean;
    function VerificaDatos( Lista: TStrings ): Boolean;
    procedure LimpiarLista;
    procedure SeleccionaPrimerTerminal;
    procedure ShowTerminalInfo;
  public
    { Public declarations }
    property LinxServer: TL5Server read FLinxServer write FLinxServer;
  end;

var
  CapturarTerminales: TCapturarTerminales;

implementation

uses ZetaDialogo;

{$R *.DFM}

procedure TCapturarTerminales.FormCreate(Sender: TObject);
begin
     inherited;
     FRefreshing := False;
end;

procedure TCapturarTerminales.FormShow(Sender: TObject);
const
     K_CON_DETECTAR = 86;
     K_SIN_DETECTAR = 4;
begin
     inherited;
     with LinxIni.IniValues do
     begin
          Self.UseTerminalList.Checked := UsarListaTerminales;
          ListaTerminales.Cargar( TerminalList.Items );
     end;
     Detectar.Visible := LinxServer.Used;
     UseTerminalListClick( Self );
     SeleccionaPrimerTerminal;
     ShowTerminalInfo;
     if Detectar.Visible then
        UseTerminalList.Left := K_CON_DETECTAR
     else
         UseTerminalList.Left := K_SIN_DETECTAR;
end;

procedure TCapturarTerminales.FormDestroy(Sender: TObject);
begin
     inherited;
     LimpiarLista;
end;

procedure TCapturarTerminales.LimpiarLista;
begin
     with TerminalList do
     begin
          LinxIni.IniValues.ListaTerminales.Limpiar( Items );
     end;
end;

procedure TCapturarTerminales.SeleccionaPrimerTerminal;
begin
     with TerminalList do
     begin
          if ( Items.Count > 0 ) then
             ItemIndex := 0;
     end;
end;

procedure TCapturarTerminales.ShowTerminalInfo;
var
   i: Integer;
begin
     with TerminalList do
     begin
          i := ItemIndex;
          if ( i >= 0 ) then
          begin
               FRefreshing := True;
               with TTerminal( Items.Objects[ i ] ) do
               begin
                    Self.DireccionIP.Text := Address;
                    Self.MasterID.Text := MasterID;
                    Self.UnitID.Text := UnitID;
                    Self.Description.Text := Description;
                    Self.edIdentifica.Text := Identificador;
                    Self.Alarma.Text := Alarma;
                    Self.Empleados.Text := Empleados;
               end;
               FRefreshing := False;
          end;
     end;
end;

function TCapturarTerminales.BuscaArchivo( const sFileName, sFilter, sTitle: String ): String;
begin
     with OpenDialog do
     begin
          Title := sTitle;
          FileName := sFileName;
          InitialDir := ExtractFilePath( sFileName );
          Filter := sFilter;
          FilterIndex := 0;
          if Execute then
             Result := FileName
          else
              Result := sFileName;
     end;
end;

function TCapturarTerminales.VerificaDeteccion: Boolean;
begin
     with TerminalList do
     begin
          if ( TerminalList.Items.Count > 0 ) then
             Result := zConfirm( '� Atenci�n !', '� Desea Sustituir Las Terminales Capturadas Por Las Terminales Detectadas ?', 0, mbNo )
          else
              Result := True;
     end;
end;

procedure TCapturarTerminales.HayCambios(Sender: TObject);
const
     K_BLANCO = ' ';
var
   iLenId, i: Integer;
   sIdentificador: String;
begin
     if not FRefreshing then
     begin
          with TerminalList do
          begin
               if ( ItemIndex >= 0 ) then
               begin
                    with TTerminal( Items.Objects[ ItemIndex ] ) do
                    begin
                         Address := Self.DireccionIP.Text;
                         Description := Self.Description.Text;
                         MasterID := Self.MasterID.EditText;
                         UnitID := Self.UnitID.EditText;
                         sIdentificador := Trim( Self.edIdentifica.Text );
                         iLenId := Length( sIdentificador );
                         for i := iLenId to 4 do
                         begin
                              sIdentificador := sIdentificador + K_BLANCO;
                         end;
                         Identificador := sIdentificador;
                         Alarma := Self.Alarma.Text;
                         Empleados := Self.Empleados.Text;
                    end;
               end;
          end;
     end;
end;

procedure TCapturarTerminales.HayCambiosDescripcion(Sender: TObject);
begin
     HayCambios( Sender );
     if not FRefreshing then
     begin
          with TerminalList do
          begin
               if ( ItemIndex >= 0 ) then
               begin
                    Items.Strings[ ItemIndex ] := Description.Text;
               end;
          end;
     end;
end;

procedure TCapturarTerminales.UseTerminalListClick(Sender: TObject);
begin
     inherited;
     //SetTerminalControls;
end;

procedure TCapturarTerminales.TerminalListClick(Sender: TObject);
begin
     inherited;
     ShowTerminalInfo;
end;

procedure TCapturarTerminales.AgregarClick(Sender: TObject);
begin
     inherited;
     if not VerificaDatos( TerminalList.Items ) then
     begin
          with TerminalList do
          begin
               Items.AddObject( 'TerminalNueva', TTerminal.Create( 'TerminalNueva' ) );
               ItemIndex := ( Items.Count - 1 );
          end;
          Modificar.Click;
     end;
end;

procedure TCapturarTerminales.BorrarClick(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     with TerminalList do
     begin
          i := ItemIndex;
          if ( i >= 0 ) then
          begin
               with Items do
               begin
                    TTerminal( Objects[ i ] ).Free;
                    Delete( i );
                    if ( i >= Count ) then
                       i := ( i - 1 );
               end;
               ItemIndex := i;
               ShowTerminalInfo;
          end;
     end;
end;

procedure TCapturarTerminales.ModificarClick(Sender: TObject);
begin
     inherited;
     ShowTerminalInfo;
     with DireccionIP do
     begin
          if CanFocus and Visible then
             SetFocus;
     end;
end;

procedure TCapturarTerminales.DetectarClick(Sender: TObject);
begin
     inherited;
     if VerificaDeteccion then
     begin
          LimpiarLista;
          LinxServer.GetTerminalList( TerminalList.Items );
          SeleccionaPrimerTerminal;
          ShowTerminalInfo;
     end;
end;

function TCapturarTerminales.VerificaDatos( Lista: TStrings ): Boolean;
var
   i, j: Integer;
   oInfoTerminalOrigen, oInfoTerminalDestino: TTerminal;

   function ChecaIdentificadores( oTerminalOrigen, oTerminalDestino: TTerminal ): Boolean;
   begin
        Result := ( oTerminalOrigen.Identificador = oTerminalDestino.Identificador ) and ( Trim( oTerminalDestino.Identificador ) <> '' );
   end;

begin
     Result := False;
     for i := 0 to ( Lista.Count - 1 ) do
     begin
          oInfoTerminalOrigen := TTerminal( Lista.Objects[ i ] );//.GetInfo;
          if( (Lista.Count) > 1 )then
          begin
               for j :=( i + 1 ) to ( Lista.Count - 1 ) do
               begin
                    oInfoTerminalDestino := TTerminal( Lista.Objects[ j ] );//.GetInfo;
                    if ( oInfoTerminalOrigen.LinxAddress = oInfoTerminalDestino.LinxAddress ) then
                    begin
                         ZetaDialogo.ZError( 'Error al Grabar', 'La Terminal ' + Trim( oInfoTerminalDestino.Description ) + ' tiene los mismos valores de direcci�n [ '+ oInfoTerminalOrigen.LinxAddress + ' ] que la Terminal ' + Trim( oInfoTerminalOrigen.Description ), 0 );
                         Result := True;
                         break;
                    end
                    else
                    begin
                         if ChecaIdentificadores( oInfoTerminalOrigen, oInfoTerminalDestino ) then
                         begin
                              ZetaDialogo.ZError( 'Error al Grabar', 'La Terminal ' + Trim( oInfoTerminalDestino.Description ) + ' tiene el mismo Identificador ['+ oInfoTerminalDestino.Identificador +'] que la Terminal ' + Trim( oInfoTerminalOrigen.Description ), 0 );
                              Result := True;
                              break;
                         end;
                    end;
               end;
          end;
          if Result then
             Break;
     end;
end;

procedure TCapturarTerminales.edIdentificaEnter(Sender: TObject);
begin
     inherited;
     edIdentifica.SelectAll;
end;

procedure TCapturarTerminales.BuscarEmpleadosClick(Sender: TObject);
begin
     inherited;
     Empleados.Text := BuscaArchivo( Empleados.Text, 'Empleado|*.val|Todos los Archivos|*.*', 'Archivo de Empleados para Aplicaciones de Linx' );
end;

procedure TCapturarTerminales.BuscarAlarmaClick(Sender: TObject);
begin
     inherited;
     Alarma.Text := BuscaArchivo( Alarma.Text, 'Alarmas|*.val|Todos los Archivos|*.*', 'Archivo de Alarma para Aplicaciones de Linx' );
end;

procedure TCapturarTerminales.OKClick(Sender: TObject);
begin
     inherited;
     with LinxIni.IniValues do
     begin
          UsarListaTerminales := Self.UseTerminalList.Checked;
          if VerificaDatos( TerminalList.Items ) then
          begin
               //ZetaDialogo.ZError( 'Error al Grabar', 'El Identificador de los relojes debe de ser �nico', 0 );
               ModalResult := mrNone;
          end
          else
              ListaTerminales.Descargar( TerminalList.Items );
     end;
end;

end.
