inherited CapturarTerminales: TCapturarTerminales
  Caption = 'Capturar Lista de Terminales'
  ClientHeight = 275
  ClientWidth = 559
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 239
    Width = 559
    TabOrder = 2
    inherited OK: TBitBtn
      Left = 391
      TabOrder = 2
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 476
      TabOrder = 3
    end
    object Detectar: TBitBtn
      Left = 6
      Top = 3
      Width = 75
      Height = 29
      Hint = 'Auto Detectar Lista de Terminales'
      Caption = '&Detectar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = DetectarClick
      Glyph.Data = {
        06020000424D0602000000000000760000002800000028000000140000000100
        0400000000009001000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF003333333FFFFF
        FFF00000333333333333333777773333333BFBFBFBF0FFF03333333333333337
        FFF73333333FFFFFFF000000333333333333337777773333333BFBFBF0FBFBFB
        333333333FFFF733FFFF3333333F00000FF000003333333377777FF777773333
        333B0FFF0000FFF0333333337FFF7777FFF73333333F00000FF000003333333F
        777773F777773333330BFBFBF0FBFBFB3333337FF333373FFFFF33333010FFFF
        FF00000033333777FF3333777777333330170BFBFBF0FFF0333337777FF33337
        FFF73333301170FFFFF0000033333777778F3337777333330711190BFBFBFBFB
        333377777378F3333333333308819990FFFFFFFF3333733733378F3333333330
        88FF9999033333333337333333FF7333333333088FFFF0003333333333733333
        F777333333333088FFF003333333333337333337733333333333088FFF033333
        333333337F33337333333333333308FFF09333333333333378F3373333333333
        333330FF0933333333333333378F733333333333333333003333333333333333
        33773333333333333333}
      NumGlyphs = 2
    end
    object UseTerminalList: TCheckBox
      Left = 86
      Top = 9
      Width = 138
      Height = 17
      Caption = '&Usar Lista de Terminales'
      TabOrder = 1
      OnClick = UseTerminalListClick
    end
  end
  object PanelControles: TPanel
    Left = 241
    Top = 0
    Width = 318
    Height = 239
    Align = alClient
    TabOrder = 1
    object DescriptionLBL: TLabel
      Left = 7
      Top = 82
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci�n:'
    end
    object UnitIDLBL: TLabel
      Left = 12
      Top = 58
      Width = 53
      Height = 13
      Alignment = taRightJustify
      Caption = '# Terminal:'
    end
    object MasterIDLBL: TLabel
      Left = 14
      Top = 33
      Width = 51
      Height = 13
      Alignment = taRightJustify
      Caption = '# Maestro:'
    end
    object DireccionLBL: TLabel
      Left = 17
      Top = 9
      Width = 48
      Height = 13
      Alignment = taRightJustify
      Caption = 'Direcci�n:'
    end
    object lblIdentifica: TLabel
      Left = 5
      Top = 106
      Width = 61
      Height = 13
      Alignment = taRightJustify
      Caption = 'Identificador:'
    end
    object AlarmaLBL: TLabel
      Left = 31
      Top = 130
      Width = 35
      Height = 13
      Alignment = taRightJustify
      Caption = 'Alarma:'
    end
    object EmpleadosLBL: TLabel
      Left = 11
      Top = 154
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empleados:'
    end
    object BuscarAlarma: TSpeedButton
      Left = 285
      Top = 125
      Width = 23
      Height = 24
      Hint = 'Buscar Archivo ALARMA.VAL'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
        300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
        330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
        333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
        339977FF777777773377000BFB03333333337773FF733333333F333000333333
        3300333777333333337733333333333333003333333333333377333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = BuscarAlarmaClick
    end
    object BuscarEmpleados: TSpeedButton
      Left = 285
      Top = 149
      Width = 23
      Height = 24
      Hint = 'Buscar Archivo EMPLEADOS.VAL'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
        300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
        330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
        333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
        339977FF777777773377000BFB03333333337773FF733333333F333000333333
        3300333777333333337733333333333333003333333333333377333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = BuscarEmpleadosClick
    end
    object Description: TEdit
      Left = 68
      Top = 78
      Width = 147
      Height = 21
      TabOrder = 3
      OnChange = HayCambiosDescripcion
    end
    object MasterID: TMaskEdit
      Left = 67
      Top = 30
      Width = 27
      Height = 21
      EditMask = '00;1;_'
      MaxLength = 2
      TabOrder = 1
      Text = '  '
      OnChange = HayCambios
    end
    object UnitID: TMaskEdit
      Left = 67
      Top = 54
      Width = 27
      Height = 21
      EditMask = '00;1;_'
      MaxLength = 2
      TabOrder = 2
      Text = '  '
      OnChange = HayCambios
    end
    object DireccionIP: TMaskEdit
      Left = 67
      Top = 6
      Width = 97
      Height = 21
      EditMask = '000\.000\.000\.000;0'
      MaxLength = 15
      TabOrder = 0
      Text = '000000000000'
      OnChange = HayCambios
    end
    object edIdentifica: TEdit
      Left = 68
      Top = 102
      Width = 52
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 4
      TabOrder = 4
      OnChange = HayCambios
      OnClick = edIdentificaEnter
      OnEnter = edIdentificaEnter
    end
    object Alarma: TEdit
      Left = 68
      Top = 126
      Width = 213
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 5
      OnChange = HayCambios
      OnClick = edIdentificaEnter
      OnEnter = edIdentificaEnter
    end
    object Empleados: TEdit
      Left = 68
      Top = 150
      Width = 213
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 6
      OnChange = HayCambios
      OnClick = edIdentificaEnter
      OnEnter = edIdentificaEnter
    end
  end
  object PanelLista: TPanel
    Left = 0
    Top = 0
    Width = 241
    Height = 239
    Align = alLeft
    TabOrder = 0
    object TerminalList: TListBox
      Left = 1
      Top = 1
      Width = 239
      Height = 202
      Align = alClient
      ItemHeight = 13
      TabOrder = 0
      OnClick = TerminalListClick
    end
    object PanelABC: TPanel
      Left = 1
      Top = 203
      Width = 239
      Height = 35
      Align = alBottom
      TabOrder = 1
      object Agregar: TBitBtn
        Left = 4
        Top = 4
        Width = 77
        Height = 27
        Hint = 'Agregar Rengl�n'
        Caption = '&Agregar'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = AgregarClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33333333FF33333333FF333993333333300033377F3333333777333993333333
          300033F77FFF3333377739999993333333333777777F3333333F399999933333
          33003777777333333377333993333333330033377F3333333377333993333333
          3333333773333333333F333333333333330033333333F33333773333333C3333
          330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
          993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
          333333333337733333FF3333333C333330003333333733333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
      end
      object Borrar: TBitBtn
        Left = 83
        Top = 4
        Width = 77
        Height = 27
        Hint = 'Borrar Rengl�n'
        Caption = '&Borrar'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BorrarClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
          305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
          005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
          B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
          B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
          B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
          B0557777FF577777F7F500000E055550805577777F7555575755500000555555
          05555777775555557F5555000555555505555577755555557555}
        NumGlyphs = 2
      end
      object Modificar: TBitBtn
        Left = 163
        Top = 4
        Width = 71
        Height = 27
        Caption = '&Modificar'
        TabOrder = 2
        OnClick = ModificarClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
          000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
          00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
          F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
          0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
          FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
          FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
          0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
          00333377737FFFFF773333303300000003333337337777777333}
        NumGlyphs = 2
      end
    end
  end
  object OpenDialog: TOpenDialog
    Left = 104
    Top = 16
  end
end
