unit FLinx5Poll;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      FLinx5Poll.pas                             ::
  :: Descripci�n: Programa principal de L5Poll.exe           ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Db, DBClient,
     Forms, Dialogs, ExtCtrls, Menus, StdCtrls, Buttons, ComCtrls, ComObj, ActiveX,
     LinxIni,
     LinxDll,
     FLinxBase,
     FHelpManager;

type
  TLinx5Poll = class(TLinxBase)
    N5: TMenuItem;
    ConfiguracionOptimizacion: TMenuItem;
    ConfiguracionArchivos: TMenuItem;
    TerminalesLinx5OS: TMenuItem;
    TerminalesLinx4OS: TMenuItem;
    TerminalesPantalla: TMenuItem;
    TerminalesSerie: TMenuItem;
    TerminalesFechaHora: TMenuItem;
    TerminalesInterrumpir: TMenuItem;
    TerminalesReiniciar: TMenuItem;
    Linx5OS: TBitBtn;
    Linx4OS: TBitBtn;
    Interrumpir: TBitBtn;
    Pantalla: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ConfiguracionClick(Sender: TObject);
    procedure ConfiguracionArchivosClick(Sender: TObject);
    procedure ConfiguracionOptimizacionClick(Sender: TObject);
    procedure ConfiguracionListaClick(Sender: TObject);
    procedure TerminalesLinx5OSClick(Sender: TObject);
    procedure TerminalesLinx4OSClick(Sender: TObject);
    procedure TerminalesPantallaClick(Sender: TObject);
    procedure TerminalesProgramaClick(Sender: TObject);
    procedure TerminalesClick(Sender: TObject);
    procedure TerminalesAlarmaClick(Sender: TObject);
    procedure TerminalesEmpleadosClick(Sender: TObject);
    procedure TerminalesVersionClick(Sender: TObject);
    procedure TerminalesSerieClick(Sender: TObject);
    procedure TerminalesFechaHoraClick(Sender: TObject);
    procedure TerminalesSincronizarClick(Sender: TObject);
    procedure TerminalesSincronizarTodasClick(Sender: TObject);
    procedure TerminalesInterrumpirClick(Sender: TObject);
    procedure TerminalesResetearClick(Sender: TObject);
    procedure TerminalesRebootClick(Sender: TObject);
    procedure TerminalesReiniciarClick(Sender: TObject);
    procedure ArchivoRecolectarClick(Sender: TObject);
  private
    { Private declarations }
    FLinxServer: TL5Server;
    FOldHelpEvent: THelpEvent;
    function HTMLHelpHook( Command: Word; Data: Longint; var CallHelp: Boolean ): Boolean;
    function PuedeEjecutarEnDemo: Boolean;
    procedure ExecuteCmd( Command: Executer );
    procedure SendFile( Command: FileSender; sFileName: String; lAskForFile: Boolean );
  protected
    { Protected declarations }
    function DatosEnviados: String; override;
    function InitAllServers: Boolean; override;
    function Read: Boolean; override;
    function RegistroLeido: String; override;
    function RegistroNormal: Boolean; override;
    function Terminal: Integer; override;
    function VerifyAllServers: Boolean; override;
    function PuedeRecolectar: Boolean; override;
    procedure AutoSincronizarTodas( const dValue: TDate ); override;
    procedure DownAllServers; override;
    procedure EnableControls( const lRecolectando: Boolean ); override;
    procedure MakeNodeList; override;
    procedure ProcesarLectura( var sData: String ); override;
    procedure SendAlarm( const sFileName: String ); override;
    procedure SendEmpleados( const sFileName: String ); override;
  public
    { Public declarations }
    property LinxServer: TL5Server read FLinxServer;
    function BatchModeEnabled: Boolean; override;
    procedure MakeUnitList; override;
  end;

var
  Linx5Poll: TLinx5Poll;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo,
     FSincronizar,
     FArchivos,
     FTerminales,
     FOptimizador;

{$R *.DFM}

{ ************* TLinx5Poll ************ }

procedure TLinx5Poll.FormCreate(Sender: TObject);
const
     {$ifdef DOS_CAPAS}
     K_TITULO = '%s Profesional';
     {$else}
     K_TITULO = '%s ';
     {$endif}
begin
     inherited;
     {$ifdef DUMMY_SERVER}
     FLinxServer := TL5DummyServer.Create;
     {$else}
     FLinxServer := TL5Server.Create;
     {$endif}
     with LinxServer do
     begin
          OnMakeUnitList := MakeUnitList;
          OnFeedBack := PostMessage;
          OnGaugeSetSize := GaugeSetSize;
          OnGaugeAdvance := GaugeAdvance;
          OnGaugeFinish := GaugeFinish;
          ValidTokens := TOKEN_ASISTENCIA;
     end;
     Self.Caption := Format( K_TITULO, [ Application.Title ] );
     FOldHelpEvent := Application.OnHelp;
     Application.OnHelp := HTMLHelpHook;
end;

procedure TLinx5Poll.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FLinxServer );
     inherited;
end;

function TLinx5Poll.HTMLHelpHook(Command: Word; Data: Integer; var CallHelp: Boolean): Boolean;
begin
     Result := FHelpManager.HtmlHelpHook( Application.HelpFile, Command, Data, CallHelp );
end;

function TLinx5Poll.BatchModeEnabled: Boolean;
begin
     Result := True;
end;

procedure TLinx5Poll.EnableControls( const lRecolectando: Boolean );
begin
     inherited EnableControls( lRecolectando );
     Linx5OS.Enabled := lRecolectando;
     Linx4OS.Enabled := lRecolectando;
     Pantalla.Enabled := lRecolectando;
     Interrumpir.Enabled := lRecolectando;
end;

{ ********** Abrir Servidores ************ }

function TLinx5Poll.InitAllServers: Boolean;
begin
     Result := LinxServer.Init;
     if Result then
        Result := inherited InitAllServers;
end;

function TLinx5Poll.VerifyAllServers: Boolean;
begin
     try
        LinxServer.Verify;
        Result := True;
     except
           on Error: Exception do
           begin
                ManejaError( 'Error Al Verificar Servidores', Error );
                Result := False;
           end;
     end;
end;

{ ********** Procesamiento ********** }

procedure TLinx5Poll.ProcesarLectura( var sData: String );
begin
     inherited ProcesarLectura( sData );
end;

{ ********** Cerrar Servidores ********** }

procedure TLinx5Poll.DownAllServers;
begin
     LinxServer.Close;
     if( PuedeRecolectar )then
         inherited DownAllServers;
end;

{ ********** M�todos de Manejo De Linx5Server ********** }

procedure TLinx5Poll.MakeUnitList;
var
   i: Integer;
begin
     with UnitList.Items do
     begin
          BeginUpdate;
          Clear;
          for i := 1 to LinxServer.Units do
          begin
               Add( LinxServer.GetTerminalAddress( i ) );
          end;
          EndUpdate;
     end;
end;

function TLinx5Poll.DatosEnviados: String;
begin
     Result := LinxServer.DatosEnviados;
end;

function TLinx5Poll.Read: Boolean;
begin
     Result := LinxServer.Read;
end;

function TLinx5Poll.RegistroLeido: String;
begin
     Result := LinxServer.RegistroLeido;
end;

function TLinx5Poll.RegistroNormal: Boolean;
begin
     Result := LinxServer.RegistroNormal;
end;

function TLinx5Poll.Terminal: Integer;
begin
     Result := LinxServer.Terminal;
end;

procedure TLinx5Poll.MakeNodeList;
begin
     LinxServer.MakeNodeList;
end;

procedure TLinx5Poll.AutoSincronizarTodas( const dValue: TDate );
var
   i: Integer;
begin
     for i := 0 to ( UnitList.Items.Count - 1 ) do
     begin
          LinxServer.SetDateTime( ( i + 1 ), dValue );
     end;
end;

procedure TLinx5Poll.SendAlarm( const sFileName: String );
var
   i: Integer;
   sAlarmFile: String;
   TerminalData: TTerminal;
begin
     with LinxServer do
     begin
          for i := 1 to Units do
          begin
               sAlarmFile := VACIO;
               if ZetaCommonTools.StrLleno( sFileName ) then
                  sAlarmFile := sFileName
               else
               begin
                    TerminalData := GetTerminalInfo( i );
                    if Assigned( TerminalData ) then
                    begin
                         sAlarmFile := TerminalData.Alarma;
                    end;
                    {
                    with IniValues.ListaTerminales do
                    begin
                         if Assigned( Terminal[ i ] ) then
                         begin
                              sAlarmFile := Terminal[ i ].Alarma;
                         end;
                    end;
                    }
               end;
               if ZetaCommonTools.StrLleno( sAlarmFile ) and FileExists( sAlarmFile ) then
               begin
                    SendParameterFile( i, sAlarmFile );
                    Bitacora.WriteTimeStamp( Format( 'Alarma %s Enviado A %s', [ sAlarmFile, GetTerminalAddress( i ) ] ) + ' ( %s )' );
               end
               else
                   if ZetaCommonTools.StrLleno( sAlarmFile ) and not FileExists( sAlarmFile) then
                   begin
                        Bitacora.WriteTimeStamp( Format( 'Terminal %s: Archivo De Alarmas %s No Existe', [ GetTerminalAddress( i ), sAlarmFile ] ) + ' ( %s )' );
                   end;
          end;
     end;
end;

procedure TLinx5Poll.SendEmpleados( const sFileName: String );
var
   i: Integer;
   sEmployeeFile: String;
   TerminalData: TTerminal;
begin
     with LinxServer do
     begin
          for i := 1 to Units do
          begin
               sEmployeeFile := VACIO;
               if ZetaCommonTools.StrLleno( sFileName ) then
                  sEmployeeFile := sFileName
               else
               begin
                    TerminalData := GetTerminalInfo( i );
                    if Assigned( TerminalData ) then
                    begin
                         sEmployeeFile := TerminalData.Empleados;
                    end;
                    {
                    with IniValues.ListaTerminales do
                    begin
                         if Assigned( Terminal[ i ] ) then
                         begin
                              sEmployeeFile := Terminal[ i ].Empleados;
                         end;
                    end;
                    }
               end;
               if ZetaCommonTools.StrLleno( sEmployeeFile ) and FileExists( sEmployeeFile ) then
               begin
                    SendEmployeeListFile( i, sEmployeeFile );
                    Bitacora.WriteTimeStamp( Format( 'Lista De Empleados %s Enviada A %s', [ sEmployeeFile, GetTerminalAddress( i ) ] ) + ' ( %s )' );
               end
               else
                   if ZetaCommonTools.StrLleno( sEmployeeFile ) and not FileExists( sEmployeeFile) then
                   begin
                        Bitacora.WriteTimeStamp( Format( 'Terminal %s: Lista De Empleados %s No Existe', [ GetTerminalAddress( i ), sEmployeeFile ] ) + ' ( %s )' );
                   end;
          end;
     end;
end;

procedure TLinx5Poll.ExecuteCmd( Command: Executer );
var
   i: Integer;
begin
     if ( UnitList.SelCount > 0 ) then
     begin
          SetCursorWait;
          for i := 0 to ( UnitList.Items.Count - 1 ) do
              if UnitList.Selected[ i ] then
              begin
                   Command( i + 1 );
              end;
          SetCursorNormal;
     end
     else
         ZetaDialogo.zWarning( '� No Se Escogi� Ninguna Terminal !', 'No Hay Ninguna Terminal Seleccionada', 0, mbOK );
end;

procedure TLinx5Poll.SendFile( Command: FileSender; sFileName: String; lAskForFile: Boolean );
var
   i: Integer;
begin
     if ( UnitList.SelCount > 0 ) then
     begin
          if NOT lAskForFile OR GetFileName( sFileName ) then
          begin
               if FileExists( sFileName ) then
               begin
                    SetCursorWait;
                    for i := 0 to ( UnitList.Items.Count - 1 ) do
                        if UnitList.Selected[ i ] then
                           Command( ( i + 1 ), sFileName );
                    SetCursorNormal;
               end
               else
                   zWarning( '� Archivo No Existe !', sFileName, 0, mbOK );
          end;
     end
     else
         ZetaDialogo.zWarning( '� No Se Escogi� Ninguna Terminal !', 'No Hay Ninguna Terminal Seleccionada', 0, mbOK );
end;

{ ************ Eventos del Men� *********** }

procedure TLinx5Poll.ConfiguracionClick(Sender: TObject);
begin
     inherited;
     ConfiguracionArchivos.Enabled := not Recolectando;
     ConfiguracionOptimizacion.Enabled := not Recolectando;
end;

procedure TLinx5Poll.ConfiguracionArchivosClick(Sender: TObject);
begin
     inherited;
     with TArchivos.Create( Self ) do
     begin
          try
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TLinx5Poll.ConfiguracionOptimizacionClick(Sender: TObject);
begin
     inherited;
     with TOptimizador.Create( Self ) do
     begin
          try
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TLinx5Poll.ConfiguracionListaClick(Sender: TObject);
begin
     inherited;
     with TCapturarTerminales.Create( Self ) do
     begin
          try
             LinxServer := Self.LinxServer;
             ShowModal;
             if ( ModalResult = mrOk ) and Recolectando then
             begin
                  ZetaDialogo.zInformation( '� Atenci�n !',
                                            'Para Que La Nueva Lista de Terminales' +
                                            CR_LF +
                                            'Sea Aplicada, Es Necesario Reiniciar' +
                                            CR_LF +
                                            'La Recolecci�n de Datos',
                                            0 );
             end;
          finally
                 Free;
          end;
     end;
end;

procedure TLinx5Poll.TerminalesClick(Sender: TObject);
begin
     inherited;
     TerminalesPantalla.Enabled := Recolectando;
     TerminalesLinx5OS.Enabled := Recolectando;
     TerminalesLinx4OS.Enabled := Recolectando;
     TerminalesInterrumpir.Enabled := Recolectando;
end;

procedure TLinx5Poll.TerminalesLinx5OSClick(Sender: TObject);
begin
     inherited;
     if ZetaDialogo.zConfirm( '� Atencion !',
                              '� Desea Enviar El Sistema Operativo Para' + CR_LF + 'Linx 5 A La Terminal Seleccionada?',
                              0,
                              mbOK ) then
     begin
          SendFile( LinxServer.SendOSFile, IniValues.Linx5OS, False );
     end;
end;

procedure TLinx5Poll.TerminalesLinx4OSClick(Sender: TObject);
begin
     inherited;
     if ZetaDialogo.zConfirm( '� Atencion !',
                              '� Desea Enviar El Sistema Operativo Para' + CR_LF + 'Linx 4 A La Terminal Seleccionada?',
                              0,
                              mbOK ) then
     begin
          SendFile( LinxServer.SendOSFile, IniValues.Linx4OS, False );
     end;
end;

procedure TLinx5Poll.TerminalesProgramaClick(Sender: TObject);
begin
     inherited;
     SendFile( LinxServer.SendApplicationFile, IniValues.Programa, True );
end;

procedure TLinx5Poll.TerminalesAlarmaClick(Sender: TObject);
begin
     inherited;
     SendFile( LinxServer.SendParameterFile, IniValues.Alarma, True );
end;

procedure TLinx5Poll.TerminalesPantallaClick(Sender: TObject);
begin
     inherited;
     ExecuteCmd( LinxServer.ShowDisplay );
end;

procedure TLinx5Poll.TerminalesEmpleadosClick(Sender: TObject);
begin
     inherited;
     SendFile( LinxServer.SendEmployeeListFile, IniValues.Empleados, True );
end;

procedure TLinx5Poll.TerminalesVersionClick(Sender: TObject);
begin
     inherited;
     ExecuteCmd( LinxServer.ShowVersion );
end;

procedure TLinx5Poll.TerminalesSerieClick(Sender: TObject);
begin
     inherited;
     ExecuteCmd( LinxServer.ShowSerialNumber );
end;

procedure TLinx5Poll.TerminalesFechaHoraClick(Sender: TObject);
begin
     inherited;
     ExecuteCmd( LinxServer.ShowDateTime );
end;

procedure TLinx5Poll.TerminalesSincronizarClick(Sender: TObject);
var
   i: Integer;
   dDateTime: TDateTime;
   lHoraSistema: Boolean;
begin
     inherited;
     if ( UnitList.SelCount > 0 ) then
     begin
          dDateTime := Now;
          if FSincronizar.GetDateTime( dDateTime, TRUE, lHoraSistema ) then
          begin
               SetCursorWait;
               for i := 0 to ( UnitList.Items.Count - 1 ) do
               begin
                    if UnitList.Selected[ i ] then
                    begin
                         if lHoraSistema then
                            dDateTime := Now;
                         LinxServer.SetDateTime( ( i + 1 ), dDateTime );
                    end;
               end;
               SetCursorNormal;
          end;
     end
     else
         ZetaDialogo.zWarning( '� No Se Escogi� Ninguna Terminal !', 'No Hay Ninguna Terminal Seleccionada', 0, mbOK );
end;

procedure TLinx5Poll.TerminalesSincronizarTodasClick(Sender: TObject);
var
   dDateTime: TDateTime;
   lHoraSistema: Boolean;
begin
     inherited;
     dDateTime := Now;
     if FSincronizar.GetDateTime( dDateTime, FALSE, lHoraSistema ) then
     begin
          SetCursorWait;
          LinxServer.SetDateTime( 0, dDateTime );
          SetCursorNormal;
     end;
end;

procedure TLinx5Poll.TerminalesInterrumpirClick(Sender: TObject);
begin
     inherited;
     if ZetaDialogo.zConfirm( '� Atencion !', '� Desea Interrumpir La Terminal Seleccionada?', 0, mbOK ) then
     begin
          ExecuteCmd( LinxServer.AbortApp );
     end;
end;

procedure TLinx5Poll.TerminalesResetearClick(Sender: TObject);
begin
     inherited;
     if ZetaDialogo.zConfirm( '� Atencion !', '� Desea Resetear La Terminal Seleccionada?', 0, mbOK ) then
     begin
          ExecuteCmd( LinxServer.ResetUnit );
     end;
end;

procedure TLinx5Poll.TerminalesRebootClick(Sender: TObject);
begin
     inherited;
     if ZetaDialogo.zConfirm( '� Atencion !', '� Desea Rebootear La Terminal Seleccionada?', 0, mbOK ) then
     begin
          ExecuteCmd( LinxServer.RebootUnit );
     end;
end;

procedure TLinx5Poll.TerminalesReiniciarClick(Sender: TObject);
begin
     inherited;
     if ZetaDialogo.zConfirm( '� Atencion !', '� Desea Reiniciar La Terminal Seleccionada?', 0, mbOK ) then
     begin
          ExecuteCmd( LinxServer.RestartUnit );
     end;
end;

procedure TLinx5Poll.ArchivoRecolectarClick(Sender: TObject);
begin
     if( PuedeRecolectar )then
         inherited
     else
         ZetaDialogo.zInformation( '� Atenci�n !',
                                   'Para realizar la Recolecci�n de checadas es' +
                                   CR_LF +
                                   'necesario tener una lista de Terminales',
                                   0 );
end;

function TLinx5Poll.PuedeRecolectar: Boolean;
begin
     Result := ( not EsDemo ) or PuedeEjecutarEnDemo;
end;

function TLinx5Poll.PuedeEjecutarEnDemo: Boolean;
begin
     Result := ( EsDemo and IniValues.UsarListaTerminales );
end;

end.
