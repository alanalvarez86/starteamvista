program L5Poll;

uses
  Forms,
  SysUtils,
  ZetaClientTools,
  FLinxBase in '..\LinxBase\FLinxBase.pas' {LinxBase},
  FAcercaDe in '..\LinxBase\FAcercaDe.pas' {AcercaDe},
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FLinx5Poll in 'FLinx5Poll.pas' {Linx5Poll},
  DPoll in '..\LinxBase\DPoll.pas' {dmPoll: TDataModule};

{$R *.RES}
{$R WindowsXP.res}

begin
     ZetaClientTools.InitDCOM;
     Application.Initialize;
     Application.Title := 'L5Poll';
     Application.HelpFile := 'L5Poll.chm';
  Application.CreateForm(TLinx5Poll, Linx5Poll);
  case ParamCount of
          1:
          begin
               Linx5Poll.ProcesaParametro( UpperCase( Trim( ParamStr( 1 ) ) ) );
               FreeAndNil( Linx5Poll );
          end;
          2:
          begin
               Linx5Poll.ProcesaParametro( UpperCase( Trim( ParamStr( 1 ) ) ), ParamStr( 2 ) );
               FreeAndNil( Linx5Poll );
          end;
     end;
     Application.Run;
end.
