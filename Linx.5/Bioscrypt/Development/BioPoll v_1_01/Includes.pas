unit Includes;

interface

type
  BII_Transaction_Log_Struct = record
    id: Cardinal;  // (c) unsigned int 32 bits
    reserved_1: Byte;
    index: Byte;
    year: Word;  // (c) unsigned short int 16 bits
    month: Byte;
    day: Byte;
    hour: Byte;
    min: Byte;
    sec: Byte;
    trans_code: Byte;  // = Action, 48=Verify ID
    flag_port: Byte;
    trans_log_data_1: Byte;
    trans_log_data_2: Byte;
    trans_log_data_3: Byte;
    reserved_2: Byte;
    status: Byte;  // = CmdStatus, 1=Success, 0=Failure
  end {BII_Transaction_Log_Struct};
  BII_Transaction_Log_Arr = array of BII_Transaction_Log_Struct;
  // PBII_Transaction_Log_Struct = ^BII_Transaction_Log_Struct;
  // PBII_Transaction_Log_Arr = ^BII_Transaction_Log_Arr;

function BII_Cleanup_Socket_Communications: Integer {$IFDEF WIN32} stdcall {$ENDIF};
function BII_Close_PC_Comm: Integer {$IFDEF WIN32} stdcall {$ENDIF};
function BII_Close_TCPIP_Communications: Integer {$IFDEF WIN32} stdcall {$ENDIF};
function BII_Erase_Transaction_Log(option: Integer;
                                   async: Integer): Integer {$IFDEF WIN32} stdcall {$ENDIF};
function BII_Get_Current_NetID: Integer {$IFDEF WIN32} stdcall {$ENDIF};
function BII_Get_Num_Transaction_Log(option: Integer): Integer {$IFDEF WIN32} stdcall {$ENDIF};
function BII_Get_Subnet_Mask(var subnetMask: Cardinal): Integer {$IFDEF WIN32} stdcall {$ENDIF};
function BII_Initialize_Socket_Communications: Integer {$IFDEF WIN32} stdcall {$ENDIF};
function BII_Is_Using_TCPIP: Integer {$IFDEF WIN32} stdcall {$ENDIF};
function BII_Open_TCPIP_Communications(host: PChar;
                                       port: Word): Integer {$IFDEF WIN32} stdcall {$ENDIF};
function BII_Read_Transaction_Log(option: Integer;
                                  updateFlag: Integer;
                                  maxEntries: Integer;
                                  Log: BII_Transaction_Log_Arr): Integer {$IFDEF WIN32} stdcall {$ENDIF};
function BII_Set_Subnet_Mask(subnetMask: Cardinal): Integer {$IFDEF WIN32} stdcall {$ENDIF};
function BII_Set_Current_NetID(net_id: Integer): Integer {$IFDEF WIN32} stdcall {$ENDIF};
function BII_Set_TCP_Connection_Timeout(timeout: Integer): Integer {$IFDEF WIN32} stdcall {$ENDIF};
function BII_Status: Integer {$IFDEF WIN32} stdcall {$ENDIF};

implementation

function BII_Cleanup_Socket_Communications; external 'BII_V1100.dll';
function BII_Close_TCPIP_Communications; external 'BII_V1100.dll';
function BII_Close_PC_Comm; external 'BII_V1100.dll';
function BII_Erase_Transaction_Log; external 'BII_V1100.dll';
function BII_Get_Current_NetID; external 'BII_V1100.dll';
function BII_Get_Num_Transaction_Log; external 'BII_V1100.dll';
function BII_Get_Subnet_Mask; external 'BII_V1100.dll';
function BII_Initialize_Socket_Communications; external 'BII_V1100.dll';
function BII_Is_Using_TCPIP; external 'BII_V1100.dll';
function BII_Open_TCPIP_Communications; external 'BII_V1100.dll';
function BII_Read_Transaction_Log; external 'BII_V1100.dll';
function BII_Set_Current_NetID; external 'BII_V1100.dll';
function BII_Set_Subnet_Mask; external 'BII_V1100.dll';
function BII_Set_TCP_Connection_Timeout; external 'BII_V1100.dll';
function BII_Status; external 'BII_V1100.dll';

end.
