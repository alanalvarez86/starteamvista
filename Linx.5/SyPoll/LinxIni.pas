unit LinxIni;

interface

uses Classes, IniFiles, SysUtils, Forms, Dialogs,
     LinxIniBase;

const
     K_ID_LEN = 4;
     K_UNIDAD_LEN = 1;
     K_SENTINEL_FILE = 'Terminales.dat';
     TERMINAL_FILE_NAME = 'SynelUnits.ini';
type
  eTerminalType = ( ttTCPIP, ttSerial, ttModem );
  eBaudRate = ( br300, br600, br1200, br2400, br4800, br9600, br19200, br38400 );
  eStopBits = ( sbOneStopBit, sbTwoStopBits );
  eParity = ( paOdd, paEven, paNone );
  eTerminalAuth = ( termAuthorized, termNoAuthorized, termNoAuthorizedSentinel);
  TTerminal = class( TObject )
  private
    { Private declarations }
    FIsConnected: Boolean;
    FUseCredentialsDefs: Boolean;
    FAutorizacion : eTerminalAuth;
    FRevisoAutorizacion : Boolean;
    FLetter: String;
    FDigit: String;
    FDescription: String;
    FMACAddress : String;
    FIdentificador: TIdentificaID;
    FUnidad : String;
    FTipo: eTerminalType;
    FCommData: Pointer;
    FTCPAddress1: Word;
    FTCPAddress2: Word;
    FTCPAddress3: Word;
    FTCPAddress4: Word;
    FBaudRate: eBaudRate;
    FStopBits: eStopBits;
    FParity: eParity;
    FSerialPort: Integer;
    FTCPPort: Integer;
    FTCPTimeOut: Integer;
    FBufferTimeOut: Integer;
    FEmpleados: String;
    FAlarma: String;
    FLecturas: Integer;
    function GetIdentificador: TIdentificaID;
    function GetUnidad: String;
    function GetIsConnected: Boolean;
    function GetData: String;
    function GetSerialPort: Integer;
    function GetTimeOut: Integer;
    function GetTCPAddress: String;
    function GetTCPPort: Integer;
    function GetTCPTimeOut: Integer;
    function GetBufferTimeOut: Integer;


    procedure SetData(const Value: String);
    procedure SetIdentificador(const Value: TIdentificaID);
    procedure SetUnidad(const Value: string);
    procedure SetIsConnected(const Value: Boolean);
    procedure SetTCPTimeOut( const iValue: Integer );
    procedure SetBufferTimeOut( const iValue: Integer );

  protected
    { protected declarations }
    property Data: String read GetData write SetData;
  public
    { Public declarations }
    constructor Create;
    property Description: String read FDescription write FDescription;
    property Identificador: TIdentificaID read GetIdentificador write SetIdentificador;
    property Unidad: string read GetUnidad write SetUnidad;
    property UseCredentialsDefs: Boolean read FUseCredentialsDefs write FUseCredentialsDefs;
    property Digit: String read FDigit write FDigit;
    property Letter: String read FLetter write FLetter;
    property Tipo: eTerminalType read FTipo write FTipo;
    property CommData: Pointer read FCommData write FCommData;
    property IsConnected: Boolean read GetIsConnected write SetIsConnected;
    property TCPAddress: String read GetTCPAddress;
    property TCPAddress1: Word read FTCPAddress1 write FTCPAddress1;
    property TCPAddress2: Word read FTCPAddress2 write FTCPAddress2;
    property TCPAddress3: Word read FTCPAddress3 write FTCPAddress3;
    property TCPAddress4: Word read FTCPAddress4 write FTCPAddress4;
    property TCPPort: Integer read GetTCPPort write FTCPPort;
    property TCPTimeOut: Integer read GetTCPTimeOut write SetTCPTimeOut;
    property BufferTimeOut: Integer read GetBufferTimeOut write SetBufferTimeOut;
    property TimeOut: Integer read GetTimeout;
    property SerialPort: Integer read GetSerialPort write FSerialPort;
    property BaudRate: eBaudRate read FBaudRate write FBaudRate;
    property StopBits: eStopBits read FStopBits write FStopBits;
    property Parity: eParity read FParity write FParity;
    property Alarma: String read FAlarma write FAlarma;
    property Empleados: String read FEmpleados write FEmpleados;
    property Lecturas: Integer read FLecturas write FLecturas;
    property MACAddress: String read FMACAddress write FMACAddress;
    property RevisoAutorizacion: Boolean read FRevisoAutorizacion;
    function IsAuthorized(const iNumeroSentinel : integer): eTerminalAuth;
    function GetInfo: String;
    function BaudRateWord: Word;
    function LetterWord: Word;
    function ParityWord: Word;
    function StopBitsWord: Word;
    function Verificar(Otra: TTerminal; var sMensaje: String): Boolean;
  end;
  TTerminalList = class( TStringList )
  private
    { Private declarations }
    function GetTerminal( Index: Integer ): TTerminal;
    function GetTerminalFileName: String;
    procedure Init;
    procedure Write;
  protected
    { protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Terminal[ Index: Integer ]: TTerminal read GetTerminal;
    procedure Cargar( Lista: TStrings );
    procedure Clear; override;
    procedure Descargar( Lista: TStrings );
    procedure Limpiar( Lista: TStrings );
  end;
  TSynelIniValues = class( TIniValues )
  private
    { Private declarations }
    FTerminalList: TTerminalList;
    function GetProgDir1: String;
    procedure PutProgDir1( const Valor: String );
    function GetProgDir2: String;
    procedure PutProgDir2( const Valor: String );
    function GetProgDir3: String;
    procedure PutProgDir3( const Valor: String );
    function GetSentinelFile: String;
    procedure PutSentinelFile( const Valor: String );

  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property ListaTerminales: TTerminalList read FTerminalList;
    property ProgDir1: String read GetProgDir1 write PutProgDir1;
    property ProgDir2: String read GetProgDir2 write PutProgDir2;
    property ProgDir3: String read GetProgDir3 write PutProgDir3;
    property SentinelFile: String read GetSentinelFile write PutSentinelFile;
  end;

var
   IniValues: TSynelIniValues;

procedure IniValuesInit;
procedure IniValuesClear;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaServerTools,
     FLinxTools;

const
     TERMINAL_SEPARATOR = '|';
     K_MAX_TIMEOUT = 5;
     K_MIN_TIMEOUT = 1;
     K_DEF_TCP_PORT = 3734;

     K_MIN_BUFFER_TIMEOUT  = 25;
     K_MAX_BUFFER_TIMEOUT  = 9999;
     K_DEF_BUFFER_TIMEOUT  = 100;

     Archivos_Config = 'Configuracion';
     Prog_Dir1 = 'Prog_Dir1' ;
     Prog_Dir2 = 'Prog_Dir2' ;
     Prog_Dir3 = 'Prog_Dir3' ;
     Sentinel_File = 'Sentinel_File';

procedure IniValuesInit;
begin
     if not Assigned( IniValues ) then
     begin
          IniValues := TSynelIniValues.Create;
     end;
end;

procedure IniValuesClear;
begin
     if Assigned( IniValues ) then
     begin
          FreeAndNil( IniValues );
     end;
end;


function EsRelojAutorizado
( const sMACAddress:widestring; const iNumeroSentinel : integer ;var Autorizado:Boolean ):Boolean;
var
   iniFile : TSynelIniValues;
   FlstRelojsAut, Reloj:TStrings;
   i:Integer;
begin

     iniFile := TSynelIniValues.Create;
     FlstRelojsAut := TStringList.Create;
     Reloj := TStringList.Create;
     try
        try
           FlstRelojsAut.LoadFromFile(iniFile.GetSentinelFile);

           Result := False;
           Autorizado := False;
           for i := 0 to ( FlstRelojsAut.Count - 1 ) do
           begin
                Reloj.Delimiter := '|';
                Reloj.DelimitedText := Decrypt( FlstRelojsAut.Strings[ i ] );
                Result :=  Reloj.Strings[1] = sMACAddress;
                if Result then
                begin
                     Autorizado := StrToInt( Reloj.Strings[0] ) = iNumeroSentinel;
                     break;
                end;
           end;
         except
              on Error: Exception do
              begin
                   Result := False;
                   Autorizado := False;
              end;
         end;
     finally
            FreeAndNil(Reloj);
            FreeAndNil(FlstRelojsAut);
            FreeAndNil(iniFile);
     end;

{$ifdef SENTINEL_DES}
     Result := True;
     Autorizado := True;
{$endif}

end;

{ ***** TTerminal ******** }

constructor TTerminal.Create;
begin
     FIsConnected := False;
     FTipo := ttTCPIP;
     FDescription := 'Terminal Nueva';
     FIdentificador := '';
     FUnidad := '0' ;
     FLetter := 'A';
     FDigit := '0';
     FTCPAddress1 := 0;
     FTCPAddress2 := 0;
     FTCPAddress3 := 0;
     FTCPAddress4 := 0;
     FTCPPort := K_DEF_TCP_PORT;
     FTCPTimeOut := K_MAX_TIMEOUT;
     FBufferTimeOut := K_DEF_BUFFER_TIMEOUT;
     FSerialPort := 1;
     FBaudRate := eBaudRate( 0 );
     FStopBits := eStopBits( 0 );
     FParity := eParity( 0 );
     FLecturas := 0;
     FMACAddress := VACIO;
     FRevisoAutorizacion := FALSE;
     FAutorizacion := termNoAuthorized;
end;

function TTerminal.GetIsConnected: Boolean;
begin
     Result := FIsConnected;
end;

procedure TTerminal.SetIsConnected(const Value: Boolean);
begin
     FIsConnected := Value;
end;

function TTerminal.GetIdentificador: TIdentificaID;
begin
     Result := Trim( FIdentificador );
end;

function TTerminal.GetSerialPort: Integer;
begin
     Result := FSerialPort
end;

function TTerminal.GetTCPPort: Integer;
begin
     Result := FTCPPort;
end;

function TTerminal.GetTimeOut: Integer;
begin
     Result := K_MAX_TIMEOUT;
end;

function TTerminal.GetTCPTimeOut: Integer;
begin
     Result := ZetaCommonTools.iMax( ZetaCommonTools.iMin( FTCPTimeOut, K_MAX_TIMEOUT ), K_MIN_TIMEOUT );
end;

function TTerminal.GetData: String;
begin
     Result := Format( '%1.1d|%-15.15s|%-4.4s|%-1.1s|%-1.1s|%-1.1s|%-1.1s', [ Ord( Tipo ), Description, Identificador, Unidad, zBoolToStr(UseCredentialsDefs), Digit, Letter ] );
     case Tipo of
          ttTCPIP: Result := Format( '%s|%3.3d|%3.3d|%3.3d|%3.3d|%5.5d|%2.2d|%4.4d|%s|%s', [ Result, TCPAddress1, TCPAddress2, TCPAddress3, TCPAddress4, TCPPort, TCPTimeOut, BufferTimeOut, Alarma, Empleados ] );
          ttSerial: Result := Format( '%s|%1.1d|%1.1d|%1.1d|%1.1d|%s|%s', [ Result, SerialPort, Ord( BaudRate ), Ord( StopBits ), Ord( Parity ), Alarma, Empleados ] );
     end;
end;

procedure TTerminal.SetIdentificador(const Value: TIdentificaID);
begin
     FIdentificador := Copy( Trim( Value ), 1, K_ID_LEN );
end;


function TTerminal.GetUnidad: String;
begin
    Result := Trim( FUnidad );
end;


procedure TTerminal.SetUnidad(const Value: string);
begin
     FUnidad := Copy( Trim( Value ), 1, K_UNIDAD_LEN );
end;


procedure TTerminal.SetTCPTimeOut( const iValue: Integer );
begin
     FTCPTimeOut := ZetaCommonTools.iMax( ZetaCommonTools.iMin( iValue, K_MAX_TIMEOUT ), K_MIN_TIMEOUT );
end;

procedure TTerminal.SetData(const Value: String);
var
   sArchivos: String;
begin
     FTipo := eTerminalType( StrToIntDef( Copy( Value, 1, 1 ), 0 ) );
     FDescription := Copy( Value, 3, 15 );
     FIdentificador := Copy( Value, 19, 4 );
     FUnidad := Copy( Value, 24, 1 );
     FUseCredentialsDefs := zStrToBool( Copy( Value, 26, 1 ) );
     FDigit := Copy( Value, 28, 1 );
     FLetter := Copy( Value, 30, 1 );
     case Tipo of
          ttTCPIP:
          begin
               FTCPAddress1 := StrToIntDef( Copy( Value, 32, 3 ), 0 );
               FTCPAddress2 := StrToIntDef( Copy( Value, 36, 3 ), 0 );
               FTCPAddress3 := StrToIntDef( Copy( Value, 40, 3 ), 0 );
               FTCPAddress4 := StrToIntDef( Copy( Value, 44, 3 ), 0 );
               FTCPPort := StrToIntDef( Copy( Value, 48, 5 ), 0 );
               FTCPTimeOut := StrToIntDef( Copy( Value, 54, 2 ), K_MAX_TIMEOUT );
               BufferTimeOut := StrToIntDef( Copy( Value, 57, 4 ), K_DEF_BUFFER_TIMEOUT );
               sArchivos := Copy( Value, 62, ( Length( Value ) - 50 ) );
               FSerialPort := 0;
               FBaudRate := eBaudRate( 0 );
               FStopBits := eStopBits( 0 );
               FParity := eParity( 0 );
          end;
     end;
     FAlarma := StrToken( sArchivos, '|' );
     FEmpleados := sArchivos;
end;

function TTerminal.GetTCPAddress: String;
begin
     Result := Format( '%d.%d.%d.%d', [ TCPAddress1, TCPAddress2, TCPAddress3, TCPAddress4 ] );
end;

function TTerminal.GetInfo: String;
const
     aConectada: array[ False..True ] of pChar = ( 'OFF', 'ON ' );
begin
     Result := Trim( Format( '%s: %-15.15s', [ aConectada[ IsConnected ], Description  ] ) );
end;

function TTerminal.BaudRateWord: Word;
begin
     case BaudRate of
          br300: Result := Word( '2' );
          br600: Result := Word( '3' );
          br1200: Result := Word( '4' );
          br2400: Result := Word( '5' );
          br4800: Result := Word( '6' );
          br9600: Result := Word( '7' );
          br19200: Result := Word( '8' );
          br38400: Result := Word( '9' );
     else
         Result := Word( '1' );
     end;
end;

function TTerminal.LetterWord: Word;
begin
     if ZetaCommonTools.StrLleno( Letter ) then
        Result := Word( Letter[ 1 ] )
     else
         Result := Word( 'A' );
end;

function TTerminal.ParityWord: Word;
begin
     case StopBits of
          sbOneStopBit: Result := Word( '1' );
          sbTwoStopBits: Result := Word( '2' );
     else
         Result := Word( '1' );
     end;
end;

function TTerminal.StopBitsWord: Word;
begin
     case Parity of
          paOdd: Result := Word( 'O' );
          paEven: Result := Word( 'E' );
          paNone: Result := Word( 'N' );
     else
         Result := Word( 'N' );
     end;
end;

function TTerminal.Verificar( Otra: TTerminal; var sMensaje: String ): Boolean;
begin
     if ZetaCommonTools.StrLleno( Identificador ) and ZetaCommonTools.StrLleno( Otra.Identificador ) and ( Identificador = Otra.Identificador ) then
     begin
          Result := False;
          sMensaje := Format( 'La Terminal %s Tiene El Mismo Identificador (%s) %s Que La Terminal %s', [ Description, Identificador, CR_LF, Otra.Description ] );
     end
     else
         if ( Tipo = ttTCPIP ) and ( TCPAddress = Otra.TCPAddress ) then
         begin
              Result := False;
              sMensaje := Format( 'La Terminal %s Tiene La Misma Direcci�n TCP/IP (%s) %s Que La Terminal %s', [ Description, TCPAddress, CR_LF, Otra.Description ] );
         end
         else
             Result := True;
end;

{ ***** TTerminalList ******** }

constructor TTerminalList.Create;
begin
     inherited Create;
end;

destructor TTerminalList.Destroy;
begin
     Clear;
     inherited Destroy;
end;

function TTerminalList.GetTerminal( Index: Integer ): TTerminal;
begin
     Result := TTerminal( Objects[ Index ] );
end;

procedure TTerminalList.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
     begin
          Terminal[ i ].Free;
     end;
     inherited Clear;
end;

function TTerminalList.GetTerminalFileName: String;
begin
     Result := FLinxTools.SetFileNameDefaultPath( TERMINAL_FILE_NAME );
end;

procedure TTerminalList.Init;
var
   UnitFile: TextFile;
   sData: String;
   Terminal: TTerminal;
begin
     if FileExists( GetTerminalFileName ) then
     begin
          Clear;
          try
             BeginUpdate;
             AssignFile( UnitFile, GetTerminalFileName );
             try
                Reset( UnitFile );
                while not Eof( UnitFile ) do
                begin
                     Readln( UnitFile, sData );
                     Terminal := TTerminal.Create;
                     Terminal.Data := sData;
                     AddObject( sData, Terminal );
                end;
             finally
                    CloseFile( UnitFile );
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TTerminalList.Cargar( Lista: TStrings );
var
   i: Integer;
   NewTerminal: TTerminal;
begin
     with Lista do
     begin
          Clear;
          try
             BeginUpdate;
             for i := 0 to ( Self.Count - 1 ) do
             begin
                  NewTerminal := TTerminal.Create;
                  NewTerminal.Data := Terminal[ i ].Data;
                  AddObject( NewTerminal.Description, NewTerminal );
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TTerminalList.Descargar( Lista: TStrings );
var
   i: Integer;
   NewTerminal: TTerminal;
begin
     Clear;
     try
        BeginUpdate;
        for i := 0 to ( Lista.Count - 1 ) do
        begin
             NewTerminal := TTerminal.Create;
             NewTerminal.Data := TTerminal( Lista.Objects[ i ] ).Data;
             AddObject( NewTerminal.Description, NewTerminal );
        end;
     finally
            EndUpdate;
     end;
     Write;
end;

procedure TTerminalList.Limpiar( Lista: TStrings );
var
   i: Integer;
begin
     with Lista do
     begin
          try
             BeginUpdate;
             for i := ( Count - 1 ) downto 0 do
             begin
                  TTerminal( Objects[ i ] ).Free;
             end;
             Clear;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TTerminalList.Write;
var
   i: Integer;
   UnitFile: TextFile;
begin
     AssignFile( UnitFile, GetTerminalFileName );
     try
        Rewrite( UnitFile );
        for i := 0 to ( Count - 1 ) do
        begin
             Writeln( UnitFile, Terminal[ i ].Data );
        end;
     finally
            CloseFile( UnitFile );
     end;
end;

{ ***** TL5IniValues ******** }

constructor TSynelIniValues.Create;
const
     INI_FILE_NAME = 'SYNEL.INI';
begin
     inherited Create( INI_FILE_NAME );
     FTerminalList := TTerminalList.Create;
     FTerminalList.Init;
end;

destructor TSynelIniValues.Destroy;
begin
     FTerminalList.Free;
     inherited Destroy;
end;

{ *** M�todos Read / Write de las propiedades *** }
function TSynelIniValues.GetProgDir1: String;
begin
     Result := IniFile.ReadString( INI_ARCHIVOS, Prog_Dir1, '' );
end;

procedure TSynelIniValues.PutProgDir1( const Valor: String );
begin
     IniFile.WriteString( INI_ARCHIVOS, Prog_Dir1, Valor );
end;

function TSynelIniValues.GetProgDir2: String;
begin
     Result := IniFile.ReadString( INI_ARCHIVOS, Prog_Dir2, '' );
end;

procedure TSynelIniValues.PutProgDir2( const Valor: String );
begin
     IniFile.WriteString( INI_ARCHIVOS, Prog_Dir2, Valor );
end;


function TSynelIniValues.GetProgDir3: String;
begin
     Result := IniFile.ReadString( INI_ARCHIVOS, Prog_Dir3, '' );
end;

procedure TSynelIniValues.PutProgDir3( const Valor: String );
begin
     IniFile.WriteString( INI_ARCHIVOS, Prog_Dir3, Valor );
end;

function TSynelIniValues.GetSentinelFile: String;
begin
     Result := IniFile.ReadString( INI_ARCHIVOS, Sentinel_File,  Format( '%s%s', [ ExtractFilePath( Application.ExeName ), K_SENTINEL_FILE ] ) );
end;

procedure TSynelIniValues.PutSentinelFile( const Valor: String );
begin
     IniFile.WriteString( INI_ARCHIVOS, Sentinel_File, Valor );
end;


function TTerminal.IsAuthorized(const iNumeroSentinel : integer): eTerminalAuth;
var
   lAutoriza : boolean;
begin
     // Aqui se va a leer la autorizaci�n del Archivo Sentinel por MAC Address
     lAutoriza := False;
     Result := termNoAuthorized;
     
     if StrLleno( MACAddress ) then
     begin
          if FRevisoAutorizacion then
             Result := FAutorizacion
          else
          begin
             FRevisoAutorizacion := TRUE;
             if EsRelojAutorizado( MACAddress, iNumeroSentinel, lAutoriza)  then
                  if lAutoriza then
                     Result := termAuthorized
                  else
                      Result := termNoAuthorizedSentinel
             else
                 Result := termNoAuthorized;


             FAutorizacion := Result;
          end;
     end;
end;



function TTerminal.GetBufferTimeOut: Integer;
begin
     Result := ZetaCommonTools.iMax( ZetaCommonTools.iMin( FBufferTimeOut, K_MAX_BUFFER_TIMEOUT ), K_MIN_BUFFER_TIMEOUT );
end;


procedure TTerminal.SetBufferTimeOut(const iValue: Integer);
begin
     FBufferTimeOut := ZetaCommonTools.iMax( ZetaCommonTools.iMin( iValue, K_MAX_BUFFER_TIMEOUT ), K_MIN_BUFFER_TIMEOUT );
end;

end.
