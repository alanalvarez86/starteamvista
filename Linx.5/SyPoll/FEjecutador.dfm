inherited ExecuteMgr: TExecuteMgr
  ActiveControl = Programa
  Caption = 'Ejecutar Un Programa En La Terminal'
  ClientHeight = 118
  ClientWidth = 341
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ProgramaLBL: TLabel [0]
    Left = 7
    Top = 36
    Width = 100
    Height = 13
    Alignment = taRightJustify
    Caption = 'Programa A Ejecutar:'
  end
  inherited PanelBotones: TPanel
    Top = 82
    Width = 341
    BevelOuter = bvNone
    inherited OK: TBitBtn
      Left = 181
      Hint = 'Ejecutar El Programa Seleccionado'
      Caption = '&Ejecutar'
      ModalResult = 0
      OnClick = OKClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333FFFFF3333333333000003333333333F777773FF333333008877700
        33333337733FFF773F33330887000777033333733F777FFF73F330880FAFAF07
        703337F37733377FF7F33080F00000F07033373733777337F73F087F00A2200F
        77037F3737333737FF7F080A0A2A220A07037F737F3333737F7F0F0F0AAAA20F
        07037F737F3333737F7F0F0A0FAA2A0A08037F737FF33373737F0F7F00FFA00F
        780373F737FFF737F3733080F00000F0803337F73377733737F330F80FAFAF08
        8033373F773337733733330F8700078803333373FF77733F733333300FFF8800
        3333333773FFFF77333333333000003333333333377777333333}
    end
    inherited Cancelar: TBitBtn
      Left = 260
      Hint = 'Salir De Esta Pantalla'
      Caption = '&Salir'
      Kind = bkClose
    end
  end
  object Programa: TComboBox
    Left = 112
    Top = 32
    Width = 209
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
  end
end
