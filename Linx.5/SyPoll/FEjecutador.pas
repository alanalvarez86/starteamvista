unit FEjecutador;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     LinxIni,
     FOnlineDll,
     ZBaseDlgModal;

type
  TExecuteMgr = class(TZetaDlgModal)
    ProgramaLBL: TLabel;
    Programa: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    FTerminalIndex: Integer;
    FOnlineMgr: TOnlineMgr;
  public
    { Public declarations }
    property TerminalIndex: Integer read FTerminalIndex write FTerminalIndex;
    property OnlineMgr: TOnlineMgr read FOnlineMgr write FOnlineMgr;
    function HayEjecutables: Boolean;
    procedure CargarArchivos( Lista: TStrings );
  end;

var
  ExecuteMgr: TExecuteMgr;

implementation

{$R *.DFM}

uses ZetaDialogo;

procedure TExecuteMgr.FormShow(Sender: TObject);
begin
     inherited;
     ActiveControl := Programa;
     with Programa do
     begin
          if ( Items.Count > 0 ) then
             ItemIndex := 0;
     end;
end;

procedure TExecuteMgr.CargarArchivos(Lista: TStrings);
const
     K_TERMINAL = 'TERMINAL';
var
   i: Integer;
   sArchivo: String;
begin
     with Programa.Items do
     begin
          Clear;
          BeginUpdate;
          try
             Add( K_TERMINAL );
             for i := 0 to ( Lista.Count - 1 ) do
             begin
                  sArchivo := Lista.Strings[ i ];
                  if ( Pos( '.EXE', UpperCase( sArchivo ) ) > 0 ) then
                     Add( sArchivo );
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

function TExecuteMgr.HayEjecutables: Boolean;
begin
     Result := ( Programa.Items.Count > 0 );
end;

procedure TExecuteMgr.OKClick(Sender: TObject);
var
   sArchivo: String;
   i: Integer;
begin
     inherited;
     with Programa do
     begin
          i := ItemIndex;
          if ( i >= 0 ) then
          begin
               sArchivo := Items[ i ];
               if ZetaDialogo.zConfirm( '� Atenci�n !', Format( '� Desea Ejecutar El Programa %s ?', [ sArchivo ] ), 0, mbNo ) then
               begin
                    if OnlineMgr.Ejecutar( TerminalIndex, sArchivo ) then
                    begin
                         ModalResult := mrOk;
                    end
                    else
                        ZetaDialogo.zError( '� Error !', Format( 'El Programa %s No Pudo Ser Ejecutado', [ sArchivo ] ), 0 );
               end;
          end;
     end;
end;

end.
