inherited CapturarTerminales: TCapturarTerminales
  Left = 216
  Top = 313
  Caption = 'Capturar Lista de Terminales'
  ClientHeight = 423
  ClientWidth = 672
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 387
    Width = 672
    TabOrder = 2
    inherited OK: TBitBtn
      Left = 504
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 589
    end
    object Agregar: TBitBtn
      Left = 4
      Top = 4
      Width = 77
      Height = 27
      Hint = 'Agregar Rengl'#243'n'
      Caption = '&Agregar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = AgregarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333FF33333333FF333993333333300033377F3333333777333993333333
        300033F77FFF3333377739999993333333333777777F3333333F399999933333
        33003777777333333377333993333333330033377F3333333377333993333333
        3333333773333333333F333333333333330033333333F33333773333333C3333
        330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
        333333333337733333FF3333333C333330003333333733333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
    end
    object Borrar: TBitBtn
      Left = 83
      Top = 4
      Width = 77
      Height = 27
      Hint = 'Borrar Rengl'#243'n'
      Caption = '&Borrar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = BorrarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
        305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
        005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
        B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
        B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
        B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
        B0557777FF577777F7F500000E055550805577777F7555575755500000555555
        05555777775555557F5555000555555505555577755555557555}
      NumGlyphs = 2
    end
    object Modificar: TBitBtn
      Left = 163
      Top = 4
      Width = 71
      Height = 27
      Caption = '&Modificar'
      TabOrder = 4
      OnClick = ModificarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
    end
  end
  object PanelControles: TPanel
    Left = 240
    Top = 0
    Width = 432
    Height = 387
    Align = alClient
    TabOrder = 1
    object TcpIpGB: TGroupBox
      Left = 1
      Top = 58
      Width = 430
      Height = 171
      Align = alClient
      Caption = ' TCP / IP '
      TabOrder = 1
      object TcpIpLBL: TLabel
        Left = 50
        Top = 18
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = 'Direcci'#243'n:'
      end
      object Punto1: TLabel
        Left = 137
        Top = 20
        Width = 3
        Height = 13
        Caption = '.'
      end
      object Label1: TLabel
        Left = 180
        Top = 20
        Width = 3
        Height = 13
        Caption = '.'
      end
      object Label2: TLabel
        Left = 223
        Top = 20
        Width = 3
        Height = 13
        Caption = '.'
      end
      object PuertoTCPLBL: TLabel
        Left = 64
        Top = 41
        Width = 34
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puerto:'
      end
      object TimeOutLBL: TLabel
        Left = 57
        Top = 64
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = 'Timeout:'
      end
      object TimeOutUnitLBL: TLabel
        Left = 157
        Top = 64
        Width = 51
        Height = 13
        Caption = 'Segundos.'
      end
      object Label3: TLabel
        Left = 64
        Top = 89
        Width = 37
        Height = 13
        Alignment = taRightJustify
        Caption = 'Unidad:'
      end
      object TCPIP1: TZetaNumero
        Left = 102
        Top = 14
        Width = 33
        Height = 21
        Mascara = mnDias
        MaxLength = 3
        TabOrder = 0
        Text = '0'
        OnChange = HayCambios
        OnExit = TCPIP1Exit
      end
      object TCPIP2: TZetaNumero
        Left = 143
        Top = 14
        Width = 33
        Height = 21
        Mascara = mnDias
        MaxLength = 3
        TabOrder = 1
        Text = '0'
        OnChange = HayCambios
        OnExit = TCPIP2Exit
      end
      object TCPIP3: TZetaNumero
        Left = 186
        Top = 14
        Width = 33
        Height = 21
        Mascara = mnDias
        MaxLength = 3
        TabOrder = 2
        Text = '0'
        OnChange = HayCambios
        OnExit = TCPIP3Exit
      end
      object TCPIP4: TZetaNumero
        Left = 229
        Top = 14
        Width = 33
        Height = 21
        Mascara = mnDias
        MaxLength = 3
        TabOrder = 3
        Text = '0'
        OnChange = HayCambios
        OnExit = TCPIP4Exit
      end
      object PuertoTCP: TZetaNumero
        Left = 102
        Top = 37
        Width = 52
        Height = 21
        Mascara = mnDias
        MaxLength = 5
        TabOrder = 4
        Text = '0'
        OnChange = HayCambios
        OnExit = PuertoTCPExit
      end
      object Timeout: TZetaNumero
        Left = 102
        Top = 60
        Width = 52
        Height = 21
        Mascara = mnDias
        MaxLength = 5
        TabOrder = 5
        Text = '0'
        OnChange = HayCambios
        OnExit = PuertoTCPExit
      end
      object Unidad: TComboBox
        Left = 102
        Top = 84
        Width = 52
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 6
        OnClick = HayCambios
        Items.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9')
      end
      object GroupBox1: TGroupBox
        Left = 13
        Top = 112
        Width = 385
        Height = 49
        Caption = ' Protocolo Synel: '
        TabOrder = 7
        object bufferTimeoutLBL: TLabel
          Left = 12
          Top = 20
          Width = 72
          Height = 13
          Alignment = taRightJustify
          Caption = 'Buffer Timeout:'
        end
        object bufferTimeoutUnitsLBL: TLabel
          Left = 144
          Top = 20
          Width = 28
          Height = 13
          Caption = 'mseg.'
        end
        object bufferTimeout: TZetaNumero
          Left = 89
          Top = 16
          Width = 52
          Height = 21
          AutoSize = False
          Mascara = mnDias
          MaxLength = 4
          TabOrder = 0
          Text = '0'
          OnChange = HayCambios
          OnExit = bufferTimeoutExit
        end
      end
    end
    object PanelGeneral: TPanel
      Left = 1
      Top = 1
      Width = 430
      Height = 57
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object DescriptionLBL: TLabel
        Left = 41
        Top = 12
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n:'
      end
      object IdentificadorLBL: TLabel
        Left = 39
        Top = 36
        Width = 61
        Height = 13
        Alignment = taRightJustify
        Caption = 'Identificador:'
      end
      object Description: TEdit
        Left = 102
        Top = 8
        Width = 160
        Height = 21
        MaxLength = 15
        TabOrder = 0
        OnChange = HayCambiosDescripcion
      end
      object Identificador: TEdit
        Left = 102
        Top = 32
        Width = 52
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 4
        TabOrder = 1
        OnChange = HayCambios
        OnClick = IdentificadorEnter
        OnEnter = IdentificadorEnter
      end
    end
    object OpcionalesGB: TGroupBox
      Left = 1
      Top = 306
      Width = 430
      Height = 80
      Align = alBottom
      Caption = '   '
      TabOrder = 3
      object LetraLBL: TLabel
        Left = 73
        Top = 51
        Width = 27
        Height = 13
        Alignment = taRightJustify
        Caption = 'Letra:'
      end
      object DigitoLBL: TLabel
        Left = 9
        Top = 27
        Width = 91
        Height = 13
        Alignment = taRightJustify
        Caption = 'D'#237'gito de Empresa:'
      end
      object Letra: TComboBox
        Left = 102
        Top = 47
        Width = 52
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        OnClick = HayCambios
        Items.Strings = (
          'A'
          'B'
          'C'
          'D'
          'E'
          'F'
          'G'
          'H'
          'I'
          'J'
          'K'
          'L'
          'M'
          'N'
          'O'
          'P'
          'Q'
          'R'
          'S'
          'T'
          'U'
          'V'
          'W'
          'X'
          'Y'
          'Z')
      end
      object Digito: TEdit
        Left = 102
        Top = 23
        Width = 52
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 1
        TabOrder = 0
        OnChange = HayCambios
        OnClick = IdentificadorEnter
        OnEnter = IdentificadorEnter
      end
      object UsarPredeterminados: TCheckBox
        Left = 12
        Top = 0
        Width = 137
        Height = 17
        Caption = ' Usar Predeterminados:'
        TabOrder = 2
        OnClick = UsarPredeterminadosClick
      end
    end
    object ArchivosGB: TGroupBox
      Left = 1
      Top = 229
      Width = 430
      Height = 77
      Align = alBottom
      Caption = ' Archivos: '
      TabOrder = 2
      object AlarmaLBL: TLabel
        Left = 64
        Top = 18
        Width = 35
        Height = 13
        Alignment = taRightJustify
        Caption = 'Alarma:'
      end
      object EmpleadosLBL: TLabel
        Left = 45
        Top = 41
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = 'Empleados:'
      end
      object BuscarAlarma: TSpeedButton
        Left = 374
        Top = 14
        Width = 23
        Height = 24
        Hint = 'Buscar Archivo ALARMA.VAL'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
          300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
          330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
          333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
          339977FF777777773377000BFB03333333337773FF733333333F333000333333
          3300333777333333337733333333333333003333333333333377333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = BuscarAlarmaClick
      end
      object BuscarEmpleados: TSpeedButton
        Left = 374
        Top = 38
        Width = 23
        Height = 24
        Hint = 'Buscar Archivo EMPLEADOS.VAL'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
          300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
          330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
          333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
          339977FF777777773377000BFB03333333337773FF733333333F333000333333
          3300333777333333337733333333333333003333333333333377333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = BuscarEmpleadosClick
      end
      object Alarma: TEdit
        Left = 102
        Top = 15
        Width = 269
        Height = 21
        TabOrder = 0
        OnChange = HayCambios
      end
      object Empleados: TEdit
        Left = 102
        Top = 38
        Width = 269
        Height = 21
        TabOrder = 1
        OnChange = HayCambios
      end
    end
  end
  object PanelLista: TPanel
    Left = 0
    Top = 0
    Width = 240
    Height = 387
    Align = alLeft
    TabOrder = 0
    object TerminalList: TListBox
      Left = 1
      Top = 1
      Width = 238
      Height = 385
      Align = alClient
      ItemHeight = 13
      TabOrder = 0
      OnClick = TerminalListClick
    end
  end
  object OpenDialog: TOpenDialog
    Left = 104
    Top = 16
  end
end
