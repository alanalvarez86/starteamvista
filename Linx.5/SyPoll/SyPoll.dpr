program SyPoll;

uses
  Forms,
  SysUtils,
  ZetaClientTools,
  FLinxBase in '..\LinxBase\FLinxBase.pas' {LinxBase},
  FAcercaDe in '..\LinxBase\FAcercaDe.pas' {AcercaDe},
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FSynelPoll in 'FSynelPoll.pas' {SynelPoller},
  RegExpr in '..\..\Tools\RegExpr.pas';

{$R *.RES}
{$R WindowsXP.res}

begin
     ZetaClientTools.InitDCOM;
     Application.Initialize;
     Application.Title := 'SyPoll';
  Application.HelpFile := 'SyPoll.chm';
  Application.CreateForm(TSynelPoller, SynelPoller);
  case ParamCount of
          1:
          begin
               SynelPoller.ProcesaParametro( UpperCase( Trim( ParamStr( 1 ) ) ) );
               FreeAndNil( SynelPoller );
          end;
          2:
          begin
               SynelPoller.ProcesaParametro( UpperCase( Trim( ParamStr( 1 ) ) ), ParamStr( 2 ) );
               FreeAndNil( SynelPoller );
          end;
          4:
          begin
               SynelPoller.ProcesaParametro( UpperCase( Trim( ParamStr( 1 ) ) ), ParamStr( 2 ), ParamStr( 3 ), ParamStr( 4 ) );
               FreeAndNil( SynelPoller );
          end;

     end;
     Application.Run;
end.
