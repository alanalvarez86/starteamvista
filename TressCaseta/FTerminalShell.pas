unit FTerminalShell;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ZBaseTerminalShell, StdCtrls, ExtCtrls, jpeg, DB,
  Grids, DBGrids, Buttons, VaClasses, VaComm, ZetaBanner,
  SFE, //Biom�trico
  SZCodeBaseX, //Biom�trico Base64 Serializer
  GrabSplitter,
  ZetaRegistryTerminal,
  ZetaCommonLists,
  ZetaCommonClasses, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxTextEdit, Vcl.Menus,
  cxLocalization, Vcl.ImgList, cxButtons, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridCustomView, cxGrid, dxSkinsForm,
  cxClasses, dxGDIPlusClasses;

type

  TCasetaRegistry = class(TZetaTerminalRegistry)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TTerminalShell = class(TBaseTerminalShell)
    dsInformacion: TDataSource;
    dsChecadas: TDataSource;
    imgListEntradasSalidas: TcxImageList;
    gridChecadas_DevEx: TcxGrid;
    gridChecadas_DevExDBTableView: TcxGridDBTableView;
    Codigo: TcxGridDBColumn;
    Nombre: TcxGridDBColumn;
    Hora: TcxGridDBColumn;
    gridChecadas_DevExLevel: TcxGridLevel;
    lblStatus: TLabel;
    lblEmpresa: TLabel;
    lblHorario: TLabel;
    imgGenericaEmpleado: TImage;
    imgFueraLinea: TImage;
    imgEnLinea: TImage;
    imgEnFueraLinea: TImage;
    DevEx_cxLocalizer: TcxLocalizer;
    StatusLBL: TLabel;
    EmpresaLBL: TLabel;
    HorarioLBL: TLabel;
    gridInformacion_DevEx: TcxGrid;
    gridInformacion_DevExDBTableView: TcxGridDBTableView;
    Informacion: TcxGridDBColumn;
    gridInformacion_DevExLevel: TcxGridLevel;
    StatusChecada: TcxGridDBColumn;
    TurnoLBL: TLabel;
    lblTurno: TLabel;
    SuperLBL: TLabel;
    lblSuper: TLabel;
    Status: TcxGridDBColumn;
    cxEstiloGridChecadas: TcxStyle;
    TimerPaseSuGafete: TTimer;
    Splitter1: TSplitter;
    btnEntradasSalidas: TcxButton;
    TimerSensorBiometrico: TTimer;
    procedure ChecadaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    //BIOMETRICO
    function CaptureFinger : Integer;
    function LeeArchivo:Boolean;
    procedure InicializaBiometrico;
    procedure TimerSensorBiometricoTimer(Sender: TObject);
    procedure InicializaControles(lBiometrico: Boolean);
    procedure DetieneLector;
    procedure switchWorking(bWorking : Boolean);
    procedure PrintError(error : Integer);
    function GetBioMessage(error: Integer) : String; // BIOMETRICO
    procedure LogPantalla(sMensaje : String);
    procedure InicializaSensorBiometrico;
    procedure SincronizaHuellas; // BIOMETRICO

    procedure btnModoClick(Sender: TObject);
    procedure VaCommAError(Sender: TObject; Errors: Integer);
    procedure FormShow(Sender: TObject);
    procedure gridChecadas_DevExDBTableViewCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure gridChecadas_DevExDBTableViewDblClick(Sender: TObject);
    procedure gridInformacion_DevExDBTableViewCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure TimerPaseSuGafeteTimer(Sender: TObject);
    procedure PanelFotoResize(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure panelRespuestaResize(Sender: TObject);
    procedure gridInformacion_DevExResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FStatus          : Integer;
    FMensaje         : string;
    FTipoChecada     : eTipoChecadas;
    FCredencialLeida : string;
    FHoraCal         : String;
    FDrivers         : boolean;
    function GetTipoChecada: eTipoChecadas;
    function RevisaChecada: boolean;
    procedure ChecadaEmpleado;
    procedure ChecadaOperador;
    procedure GetStatus(const iStatus: Integer);
    procedure GetFoto;
    procedure DespliegaDatos;
    procedure InitReglas;
    procedure GuardaChecada;
    procedure EnviaDatos;
    procedure ChecadaOK(const sMensaje: string = VACIO);
    procedure SeHaConectado(Sender: TObject);
    procedure SeHaDesConectado(Sender: TObject);
    procedure ErrorDeServidor(Sender: TObject);
    procedure establecerColores;
    procedure paseSuGafete;
    procedure ajustarPanelRespuesta;
  protected
    { Protected declarations }
    function ShowFormaConfiguracion(const lInicial: boolean): boolean; override;
    procedure InitTerminalRegistry; override;
    procedure ClearTerminalRegistry; override;
    procedure InitShellValues; override;
    procedure IniciaTerminal; override;
    procedure LimpiaDatos; override;
    procedure ProcesaChecada; override;
    procedure ProcesaChecadaSerial(const sTexto: string); override;
    procedure CargaTraducciones;
    procedure ConfiguraEstacion(const lInicial: Boolean = FALSE); override;
    procedure ShowResultado(const sResultado: String); override;
  public
    { Public declarations }
    procedure ShowTipoConexion(const lConectado: Boolean); override;
  end;

var
  RegistryCaseta: TCasetaRegistry;
  TerminalShell : TTerminalShell;
  //BIOMETRICO
  gTemplate         : array[0..1403] of Integer; //351*4 = 1404
  gImageBytes       : array[0..65535] of Byte; // 256 * 256
  addrOfImageBytes  : Pbyte;
  addrOfTemplate    : PByte;
  gWorking          : Boolean;

implementation

uses
  Variants, GraphicField, ZetaCommonTools, ZetaMessages, dCaseta, ZetaDialogo, FConfigura, CafeteraConsts;

{$R *.DFM}

const
  aTipoChecada: array [eTipoChecadas] of PChar = ('Entradas/Salidas',
                                                   'S�lo Entradas',
                                                   'S�lo Salidas',
                                                   'Inicio',
                                                   'Fin',
                                                   'Autoriza c/Goce',
                                                   'Autoriza s/Goce',
                                                   'Autoriza Extras',
                                                   'Autorizaci�n',
                                                   'Autoriza c/Goce Entrada',
                                                   'Autoriza s/Goce Entrada',
                                                   'Autoriza prepagadas F/Jornada',
                                                   'Autoriza prepagadas D/Jornada');

  aStatusChecada: array [eStatusChecada] of PChar = ('OK', 'Error', 'Advertencia');
  K_MAX_REGISTROS = 10;
  K_1000 = 1000;
  K_PASE_GAFETE = ' Pase su gafete';
  K_PASE_BIOMETRICO = ' Puede poner su huella en el lector';

  { TTerminalShell }

// BIOMETRICO
procedure TTerminalShell.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     //Finaliza Lector Biometrico
     if ( RegistryCaseta.TipoGafete = egBiometrico ) then
        DetieneLector;
end;

procedure TTerminalShell.FormCreate(Sender: TObject);
begin
      inherited;
      dmCaseta := TdmCaseta.Create(self);
      with dmCaseta do
      begin
           OnClientConnect    := SeHaConectado;
           OnClientDisConnect := SeHaDesConectado;
           OnClientError      := ErrorDeServidor;
      end;
      DataSource.DataSet    := dmCaseta.cdsDatosEmpleado;
      dsChecadas.DataSet    := dmCaseta.cdsUltimas;
      dsInformacion.DataSet := dmCaseta.cdsInformacion;
      FTipoChecada          := chDesconocido;
      FDrivers := LoadDLL;
      InitReglas;

      // Timer PaseSuGafete
      TimerPaseSuGafete.Interval :=  RegistryCaseta.LimpiarPantalla * K_1000;
end;

procedure TTerminalShell.FormDestroy(Sender: TObject);
begin
     TimerSensorBiometrico.Enabled := False;
  	 DataSource.DataSet    := nil;
  	 dsChecadas.DataSet    := nil;
  	 dsInformacion.DataSet := nil;
     FreeAndNil(dmCaseta);
     ClearTerminalRegistry;
  	 inherited;
end;

procedure TTerminalShell.FormResize(Sender: TObject);
const
    K_MIN_TOP_STATUS = 39;
    K_MAX_TOP_STATUS = 61;
    K_MAX_SEP_LABELS = 51;
    K_MIN_SEP_LABELS = 26;
    K_PANEL_LIMITE = 300;
var
  iSeparacion: Integer;
  iTop: Integer;
begin
  inherited;

  iTop := K_MIN_TOP_STATUS;
  iSeparacion := K_MIN_SEP_LABELS;
  if panelDatosEmpleado.Height >= K_PANEL_LIMITE then
  begin
    iTop :=  K_MAX_TOP_STATUS;
    iSeparacion := K_MAX_SEP_LABELS;
  end;

  StatusLBL.Top := iTop;
  lblStatus.Top := iTop;
  EmpresaLBL.Top := StatusLBL.Top + iSeparacion;
  lblEmpresa.Top := StatusLBL.Top + iSeparacion;
  HorarioLBL.Top := EmpresaLBL.Top + iSeparacion;
  lblHorario.Top := EmpresaLBL.Top + iSeparacion;
  TurnoLBL.Top := HorarioLBL.Top + iSeparacion;
  lblTurno.Top := HorarioLBL.Top + iSeparacion;
  SuperLBL.Top := TurnoLBL.Top + iSeparacion;
  lblSuper.Top := TurnoLBL.Top + iSeparacion;
end;

procedure TTerminalShell.PanelFotoResize(Sender: TObject);
begin
  inherited;
    PanelFoto.Width := PanelFoto.Height;
end;

procedure TTerminalShell.panelRespuestaResize(Sender: TObject);
begin
    inherited;
    ajustarPanelRespuesta;
end;

procedure TTerminalShell.ajustarPanelRespuesta;
begin
    // Cambiar el tama�o de la letra.
    // Evaluar tama�o del texto.
    panelRespuesta.Font.Size := MAX_SIZE_FONT;

    if Length (panelRespuesta.Caption) > CANT_CARAC_PANEL then
    begin
      if panelRespuesta.Width < MIN_SIZE_PANEL then
          panelRespuesta.Font.Size := MIN_SIZE_FONT
      else if panelRespuesta.Width <= MAX_SIZE_PANEL then
          panelRespuesta.Font.Size := MED_SIZE_FONT;
    end;
end;

procedure TTerminalShell.ShowResultado(const sResultado: String);
const K_ESPACIO = ' ';
begin
     inherited;
     ajustarPanelRespuesta;
end;

procedure TTerminalShell.FormShow(Sender: TObject);
var
   lBiometrico : Boolean;
begin
     inherited;

     // Traducciones.
     CargaTraducciones;

     // Colores.
     establecerColores;

     // Imagen gen�rica.
     // Foto.Picture := imgGenericaEmpleado.Picture;
     Foto.Picture := nil;
     gridChecadas_DevEx.Visible := FALSE;
     dmCaseta.PrimeraVez:= True; // BIOMETRICO: Primera vez que sincroniza huellas

     // BIOMETRICO
     lBiometrico := ( RegistryCaseta.TipoGafete = egBiometrico );
     if ( lBiometrico ) then
     begin
          InicializaControles( lBiometrico ); //Inicializa Timer!
          InicializaBiometrico;
     end;

     gridChecadas_DevExDBTableView.DataController.DataSource := dsChecadas;
     //Desactiva la posibilidad de agrupar
     gridChecadas_DevExDBTableView.OptionsCustomize.ColumnGrouping := False;
     //Esconde la caja de agrupamiento
     gridChecadas_DevExDBTableView.OptionsView.GroupByBox := False;
     //Para que nunca muestre el filterbox inferior
     gridChecadas_DevExDBTableView.FilterBox.Visible := fvNever;
     //Para que no aparezca el Custom Dialog
     gridChecadas_DevExDBTableView.FilterBox.CustomizeDialog := TRUE;
     // Nuevo Grid_DevEx para dsInformacion.
     gridInformacion_DevExDBTableView.DataController.DataSource := dsInformacion;
     //Desactiva la posibilidad de agrupar
     gridInformacion_DevExDBTableView.OptionsCustomize.ColumnGrouping := False;
     //Esconde la caja de agrupamiento
     gridInformacion_DevExDBTableView.OptionsView.GroupByBox := False;
     //Para que nunca muestre el filterbox inferior
     gridInformacion_DevExDBTableView.FilterBox.Visible := fvNever;
     //Para que no aparezca el Custom Dialog
     gridInformacion_DevExDBTableView.FilterBox.CustomizeDialog := TRUE;

     panelRespuesta.Caption := K_PASE_GAFETE;
     PanelFoto.Visible := RegistryCaseta.MostrarFoto;
end;

procedure TTerminalShell.CargaTraducciones;
begin
    DevEx_cxLocalizer.Active := True;
    DevEx_cxLocalizer.Locale := 2058;  // Cardinal para Espa�ol (M�xico)
end;

procedure TTerminalShell.InitTerminalRegistry;
begin
  RegistryCaseta   := TCasetaRegistry.Create(REG_KEY);
  TerminalRegistry := RegistryCaseta;
end;

procedure TTerminalShell.ClearTerminalRegistry;
begin
  FreeAndNil(RegistryCaseta);
end;

procedure TTerminalShell.InitShellValues;
begin
  Banner.Caption := TerminalRegistry.Banner;
  dmCaseta.Init;
end;

procedure TTerminalShell.LimpiaDatos;
begin
  inherited;
  lblStatus.Caption       := VACIO;
  lblEmpresa.Caption      := VACIO;
  lblHorario.Caption      := VACIO;
  lblTurno.Caption        := VACIO;
  lblSuper.Caption        := VACIO;
  // NombreLBL.Caption       := VACIO;
  EmpresaLBL.Caption      := VACIO;
  HorarioLBL.Caption      := VACIO;
  TurnoLBL.Caption        := VACIO;
  SuperLBL.Caption        := VACIO;
  StatusLBL.Caption       := VACIO;
  gridInformacion_DevEx.Visible := FALSE;
end;

procedure TTerminalShell.IniciaTerminal;
begin
  dmCaseta.Active := True;
end;

procedure TTerminalShell.SeHaConectado(Sender: TObject);
var
   I, iConexion: Integer;
begin
  iConexion := K_CAFETERA_FUERA_LINEA_TEXT;
  with dmCaseta do begin
    ShowTipoConexion(Active);
    if Active then
       iConexion := K_CAFETERA_EN_LINEA_TEXT;

    AgregaChecadaServidor(ESTADO_CONEXION[iConexion]);
    for I := 0 to Screen.FormCount - 1 do
      PostMessage(Screen.Forms[I].Handle, WM_KILLWINDOW, 0, 0);

    // Enviar peticion al servidor SOAP
    SOAPMsg := CreateSOAPMsg(K_ACCESOS_DEMO);
    if SendSOAPMsg(SOAPMsg) then
      SetLblDemo(SOAPMsg.DataSet);
    FreeAndNil(SOAPMsg);

    panelDemo.Visible := lblDEMO.Caption <> VACIO;

    ResetTimerConnect(ServerRunning);
  end;
end;

procedure TTerminalShell.SeHaDesConectado(Sender: TObject);
var
   iConexion: Integer;
begin
  iConexion := K_CAFETERA_FUERA_LINEA_TEXT;
  with dmCaseta do begin
    ShowTipoConexion(Active);
    if Active then
       iConexion := K_CAFETERA_EN_LINEA_TEXT;

    AgregaChecadaServidor(ESTADO_CONEXION[iConexion]);

    ResetTimerConnect( ServerRunning );

    CheckServidorDesconectado;
  end;
end;

procedure TTerminalShell.ErrorDeServidor(Sender: TObject);
begin
  // ToDO: Crear Una rutina de log de error de conexion
end;

function TTerminalShell.ShowFormaConfiguracion(const lInicial: boolean): boolean;
begin
  if (FormaConfigura = nil) then
    FormaConfigura := TFormaConfigura.Create(Application);
  if lInicial then
    FormaConfigura.SetControlesServidor;
  Result := (FormaConfigura.ShowModal = mrOK);
end;

procedure TTerminalShell.VaCommAError(Sender: TObject; Errors: Integer);
begin
  inherited;

end;

{ Evento de Checada }

procedure TTerminalShell.ChecadaClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if ( RegistryCaseta.TipoGafete <> egBiometrico ) or ( UpperCase( edCredencial.Text ) = RegistryCaseta.GafeteAdmon ) then // BIOMETRICO
        begin
             ProcesaChecada;
             FocusCredencial;
        end
        else
        begin
             TimerPaseSuGafete.Enabled := FALSE;
             try
                edCredencial.Text := VACIO;
                RechazaChecada;
                panelRespuesta.Color := RGB (227,66,51);
                panelRespuesta.Caption := '�No se permiten checadas con teclado!';
                Application.ProcessMessages;
             finally
                    TimerPaseSuGafete.Enabled := TRUE;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;


procedure TTerminalShell.ProcesaChecada;
begin
     // Detener timer para limpiar pantalla.
     TimerPaseSuGafete.Enabled := FALSE;
     if ( RegistryCaseta.TipoGafete = egBiometrico ) then // BIOMETRICO
          DetieneLector;

     LimpiaDatos;
     FCredencialLeida := GetTerminalChecada;
     { Limpiar el numero de credencial }
     SetCredencial(VACIO);
     if ( UpperCase( FCredencialLeida ) = RegistryCaseta.GafeteAdmon ) then
     begin
          ChecadaOperador;
     end
     else
     begin
          ChecadaEmpleado;
     end;

     // Activar timer para limpiar pantalla.
     TimerPaseSuGafete.Enabled := TRUE;

     if ( RegistryCaseta.TipoGafete = egBiometrico ) then // BIOMETRICO
         TimerSensorBiometrico.Enabled := True;

end;

procedure TTerminalShell.ProcesaChecadaSerial(const sTexto: string);
begin
  edCredencial.Text := sTexto;
  ProcesaChecada;
end;

procedure TTerminalShell.ChecadaOperador;
begin
     //Se agrega para que no haya operaciones de biometrico mientras se esta en la configuracion
     edCredencial.Enabled := false;
     try
        SetCredencial(VACIO);
        if ( RegistryCaseta.CanWrite ) then
           ConfiguraEstacion(TRUE)
        else
            ZetaDialogo.ZError( 'Acceso al Programa', 'No Tiene Configurado Acceso' + CR_LF + 'Requiere Derechos de Escritura en Registry'
                                                      + CR_LF + 'Para Realizar Esta Configuraci�n', 0 );
     finally
            edCredencial.Enabled := True;
     end;
end;

procedure TTerminalShell.ChecadaOK(const sMensaje: string = VACIO);
var
  eTipo: eTipoBitacora;
begin
  if strLleno(FMensaje) then
  begin
    eTipo := eTipoBitacora(FStatus);
    case eTipo of
      tbNormal: begin
          AceptaChecada;
          ShowResultado(FMensaje);
          // Pinta(Verde, clGreen);
          // Pinta(clGreen);
          Pinta(RGB (147,210,80));
        end;
      tbAdvertencia: begin
          AdvierteChecada;
          ShowResultado(FMensaje);
          // Pinta(Amarillo, clYellow);
          Pinta(RGB (255, 255, 85), TRUE);
        end;
      tbError, tbErrorGrave: begin
          RechazaChecada;
          ShowResultado(FMensaje);
          // Pinta(Rojo, clRed);
          Pinta(RGB (227,66,51));
        end;
    end;
  end else
  begin
    AceptaChecada;
    ShowResultado(sMensaje);
    // Pinta(Verde, clGreen);
    Pinta(RGB (147,210,80));
    FMensaje := sMensaje;
  end;
end;

procedure TTerminalShell.ChecadaEmpleado;
var
  sResultado: string;
begin
  try
    RevisaChecada;
    if (dmCaseta.ValidaResultado(sResultado) = echOK) then
    begin
      ChecadaOK(sResultado); // Se registr� checada - Evaluar Reglas y mostrar tipo de resultado
      DespliegaDatos;
      GuardaChecada; // Guardar la ultimas Checadas Validas
    end
    else
    begin
      RechazaChecada;
      DespliegaDatos; { Defecto #1095: No despliega datos cuando tarjeta es rechazada }
      ShowResultado(sResultado);
      // Pinta(Rojo, clRed);
      Pinta(RGB (227,66,51));
    end;
  except
    on E: Exception do
    begin
      RechazaChecada;
      ShowResultado(E.Message);
      // Pinta(Rojo, clRed);
      Pinta(RGB (227,66,51));
    end;
  end;

  gridChecadas_DevEx.Visible := not dmCaseta.cdsUltimas.IsEmpty;

  if gridChecadas_DevEx.Visible then
  begin
    Splitter1.Left := 432;
    Splitter1.Visible := TRUE;
  end;
end;

function TTerminalShell.RevisaChecada: boolean;
const
  K_GAFETE_ANCHO_MINIMO = 5;
var
  sGafete       : string;
  eKind         : eTipoKind;
  sValor, sLabel: string;
  iStatus       : Integer;
  lStatus       : boolean;

  { **** Autorizaciones del Empleado **** }
  procedure EscribeAutorizacion;
  const
    K_VALOR = '0.00';
  var
    sTemp: string;
  begin
    sTemp := sLabel + ': ' + sValor;
    with dmCaseta do
    begin
      with cdsInformacion do
      begin
        if (State in [dsInactive]) then
          Active := TRUE;
        if (sValor <> K_VALOR) then begin
          Append;
          FieldByName('Valor').AsString   := sTemp;
          FieldByName('Status').AsBoolean := TRUE;
          Post;
        end;
      end;
      with cdsInfoUltima do
      begin
        if (State in [dsInactive]) then
          Active := TRUE;
        if (sValor <> K_VALOR) then
        begin
          if Locate('Valor;Status;Codigo', VarArrayof([sTemp, TRUE, sGafete]), []) then
            Delete;
          Append;
          FieldByName('Valor').AsString   := sTemp;
          FieldByName('Status').AsBoolean := TRUE;
          FieldByName('Codigo').AsString  := sGafete;
          Post;
        end;
      end;
    end;
  end;

{ **** Reglas informativas **** }
  procedure EscribeReglaInformativa;
  var
    sTemp: string;
  begin
    sTemp := sLabel + ': ' + sValor;
    with dmCaseta do
    begin
      with cdsInformacion do
      begin
        if (State in [dsInactive]) then
          Active := TRUE;
        Append;
        FieldByName('Valor').AsString   := sTemp;
        FieldByName('Status').AsBoolean := TRUE;
        Post;
      end;
      with cdsInfoUltima do
      begin
        if (State in [dsInactive]) then
          Active := TRUE;
        if Locate('Valor;Status;Codigo', VarArrayof([sTemp, TRUE, sGafete]), []) then
          Delete;
        Append;
        FieldByName('Valor').AsString   := sTemp;
        FieldByName('Status').AsBoolean := TRUE;
        FieldByName('Codigo').AsString  := sGafete;
        Post;
      end;
    end;
  end;

{ **** Reglas Booleanas **** }
  procedure EscribeReglaBooleana;
  begin
    iStatus := dmCaseta.cdsEmpleado.FieldByName('AC_TIPO').AsInteger;
    if (FStatus < iStatus) then begin
      FStatus  := iStatus;
      FMensaje := sValor;
    end;
    with dmCaseta do
    begin
      with cdsInformacion do
      begin
        if (State in [dsInactive]) then
          Active := TRUE;
        Append;
        FieldByName('Valor').AsString   := sValor;
        FieldByName('Status').AsBoolean := lStatus;
        Post;
      end;
      with cdsInfoUltima do
      begin
        if (State in [dsInactive]) then
          Active := TRUE;
        if Locate('Valor;Status;Codigo', VarArrayof([sValor, lStatus, sGafete]), []) then
          Delete;
        Append;
        FieldByName('Valor').AsString   := sValor;
        FieldByName('Status').AsBoolean := lStatus;
        FieldByName('Codigo').AsString  := sGafete;
        Post;
      end;
    end;
  end;

{ **** Limpiar Info de DataSets **** }
  procedure LimpiaDatasets;
  begin
    with dmCaseta do
    begin
      if (cdsDatosEmpleado.State in [dsInactive]) then
          cdsDatosEmpleado.Active := True;
      cdsDatosEmpleado.EmptyDataSet;

      if (cdsInformacion.State in [dsInactive]) then
          cdsInformacion.Active := True;
      cdsInformacion.EmptyDataSet;

    end;
  end;

begin
  // Pinta(Amarillo, clYellow);
  Pinta(RGB (233,178,0));
  ShowResultado('Revisando...');
  sGafete := FCredencialLeida;
  if ZetaCommonTools.strLleno(sGafete) then begin
    with TerminalRegistry do begin
      case TipoGafete of
        egTress: begin
            if (Length(sGafete) < K_GAFETE_ANCHO_MINIMO) then begin
              sGafete := ZetaCommonTools.PadLCar(sGafete, K_GAFETE_ANCHO_MINIMO, '0');
            end;
            sGafete := Trim(DefaultEmpresa) + sGafete + Trim(DefaultCredencial);
          end;
      end;
    end;
  end;
  with dmCaseta do
  begin
    LimpiaDatasets;
    InitReglas;
    Result := ExisteEmpleado(sGafete, GetTipoChecada);
    if Result then
    begin
      SetCredencial(VACIO);
      with cdsDatosEmpleado do begin
        if (State in [dsInactive]) then
          Active := TRUE;
        if not(State in [dsEdit, dsInsert]) then
          Edit;
      end;
      with cdsEmpleado do begin
        while not EOF do begin
          sValor  := FieldByName('AC_VALUE').AsString;
          sLabel  := FieldByName('AC_LABEL').AsString;
          lStatus := FieldByName('AC_OK').AsBoolean;
          eKind   := eTipoKind(FieldByName('AC_KIND').AsInteger);
          case eKind of
            tkNombre: begin
                cdsDatosEmpleado.FieldByName('Nombre').AsString := sValor;
                cdsDatosEmpleado.FieldByName('NombreLBL').AsString := sLabel;
              end;
            tkEmpresa: begin
                cdsDatosEmpleado.FieldByName('Empresa').AsString := sValor;
                cdsDatosEmpleado.FieldByName('EmpresaLBL').AsString := sLabel;
              end;
            tkStatus:
              cdsDatosEmpleado.FieldByName('Status').AsInteger := StrToInt(sValor);
            tkHorario: begin
                cdsDatosEmpleado.FieldByName('Horario').AsString := sValor;
                cdsDatosEmpleado.FieldByName('HorarioLBL').AsString := sLabel;
              end;
            tkTurno: begin
                cdsDatosEmpleado.FieldByName('Turno').AsString := sValor;
                cdsDatosEmpleado.FieldByName('TurnoLBL').AsString := sLabel;
              end;
            tkFoto:
              TBlobField(cdsDatosEmpleado.FieldByName('Foto')).Assign(TBlobField(FieldByName('AC_BLOB')));
            tkSuper: begin
                cdsDatosEmpleado.FieldByName('Super').AsString := sValor;
                cdsDatosEmpleado.FieldByName('SuperLBL').AsString := sLabel;
              end;
            tkExtras, tkDescanso, tkPermisoCG, tkPermisoSG:
              EscribeAutorizacion;
            tkPermisoCGEntrada, tkPermisoSGEntrada:
              EscribeAutorizacion;
            tkInfo:
              EscribeReglaInformativa;
            tkEval:
              EscribeReglaBooleana;
          end;
          Next;
        end;
      end;
    end;
  end;
end;

function TTerminalShell.GetTipoChecada: eTipoChecadas;
begin
  Result := FTipoChecada;
end;

procedure TTerminalShell.GetStatus(const iStatus: Integer);
var
  eStatus: eStatusEmpleado;
  sTexto : string;
begin
  sTexto  := ZetaCommonLists.ObtieneElemento(lfStatusEmpleado, iStatus);
  eStatus := eStatusEmpleado(iStatus);
  case eStatus of
    steEmpleado:
      EscribeTextoColor(lblStatus, clGreen, Trim(strTransAll(sTexto, 'Empleado', VACIO)));
    steAnterior, steBaja:
      // EscribeTextoColor(lblStatus, RGB (196, 0, 0), sTexto);
      EscribeTextoColor(lblStatus, RGB (204, 87, 71), sTexto);
    else
      EscribeTextoColor(lblStatus, clBlue, sTexto);
  end;
end;

procedure TTerminalShell.GetFoto;
begin
  with dmCaseta.cdsDatosEmpleado do
  begin
    PanelFoto.Visible := RegistryCaseta.MostrarFoto;

    if (PanelFoto.Visible) then
    begin
        if (FindField('Foto') <> nil) and (not(FieldByName('Foto').IsNull)) then
          LoadPictureFromBlobField(FieldByName('Foto') as TBlobField, Foto.Picture)
        else
          Foto.Picture := imgGenericaEmpleado.Picture;
    end;
  end;
end;

procedure TTerminalShell.DespliegaDatos;
const
  K_DOSPUNTOS = ':';
var
  sSuperLbl, slblSuper: string;
begin
  with dmCaseta.cdsDatosEmpleado do
  begin
    if not IsEmpty then
    begin
      First;
      { Nombre del Empleado }
      // NombreLBL.Caption := FieldByName('NombreLBL').AsString + K_DOSPUNTOS;
      lblNombre.Caption := FieldByName('Nombre').AsString;
      { Nombre de la Empresa }
      EmpresaLBL.Caption := FieldByName('EmpresaLBL').AsString + K_DOSPUNTOS;
      lblEmpresa.Caption := FieldByName('Empresa').AsString;
      { Horario }
      HorarioLBL.Caption := FieldByName('HorarioLBL').AsString + K_DOSPUNTOS;
      lblHorario.Caption := FieldByName('Horario').AsString;
      { Turno }
      TurnoLBL.Caption := FieldByName('TurnoLBL').AsString + K_DOSPUNTOS;
      lblTurno.Caption := FieldByName('Turno').AsString;
      { Nivel de Supervisor }
      sSuperLbl := FieldByName('SuperLBL').AsString;
      slblSuper := FieldByName('Super').AsString;

      if (strLleno(sSuperLbl) and strLleno(slblSuper)) then begin
        SuperLBL.Caption := sSuperLbl + K_DOSPUNTOS;
        lblSuper.Caption := slblSuper;
      end;

      StatusLBL.Caption := 'Estatus:';

      { Estatus del Empleado }
      GetStatus(FieldByName('Status').AsInteger);
      { Foto del Empleado }
      GetFoto;
    end;
  end;

  // No mostrar gridInformacion.
  gridInformacion_DevEx.Visible := not dmCaseta.cdsInformacion.IsEmpty;

  // gridChecadas.Visible := not dmAccesos.cdsUltimas.IsEmpty;
end;

procedure TTerminalShell.InitReglas;
begin
  FStatus  := 0;
  FMensaje := VACIO;
end;

procedure TTerminalShell.btnModoClick(Sender: TObject);
const K_ESPACIO = '          ';
      K_ENTRADAS_SALIDAS = 0;
      K_ENTRADAS = 1;
      K_SALIDAS = 2;
var
  iTemp: Integer;
begin
  iTemp := Ord(FTipoChecada);
  if (iTemp < 2) then
  begin
    Inc(iTemp);
    FTipoChecada := eTipoChecadas(iTemp);
  end
  else
    FTipoChecada  := chDesconocido;

  btnEntradasSalidas.Caption := aTipoChecada[FTipoChecada];
  // Imagen.
  if FTipoChecada = chDesconocido then
      btnEntradasSalidas.OptionsImage.ImageIndex := K_ENTRADAS_SALIDAS
  else if FTipoChecada = chEntrada then
      btnEntradasSalidas.OptionsImage.ImageIndex := K_ENTRADAS
  else
      btnEntradasSalidas.OptionsImage.ImageIndex := K_SALIDAS;
end;

procedure TTerminalShell.GuardaChecada;
begin
  with dmCaseta do begin
    with cdsUltimas do begin
      if (State in [dsInactive]) then
        Active := TRUE;
      if (RecordCount = K_MAX_REGISTROS) then begin
        // Last;
        First;
        try
          Delete;
        finally
          Last;
        end;
      end;
    end; // with cdsUltimas do

    // Recorrer la cola de credenciales leidas
    {
      cdsDatosEmpleado.First;
      while not cdsDatosEmpleado.Eof do
      begin
    }
    cdsUltimas.Append;
    cdsUltimas.FieldByName('NombreLBL').AsString  := cdsDatosEmpleado.FieldByName('NombreLBL').AsString;
    cdsUltimas.FieldByName('Nombre').AsString     := cdsDatosEmpleado.FieldByName('Nombre').AsString;
    cdsUltimas.FieldByName('EmpresaLBL').AsString := cdsDatosEmpleado.FieldByName('EmpresaLBL').AsString;
    cdsUltimas.FieldByName('Empresa').AsString    := cdsDatosEmpleado.FieldByName('Empresa').AsString;
    cdsUltimas.FieldByName('HorarioLBL').AsString := cdsDatosEmpleado.FieldByName('HorarioLBL').AsString;
    cdsUltimas.FieldByName('Horario').AsString    := cdsDatosEmpleado.FieldByName('Horario').AsString;
    cdsUltimas.FieldByName('TurnoLBL').AsString   := cdsDatosEmpleado.FieldByName('TurnoLBL').AsString;
    cdsUltimas.FieldByName('Turno').AsString      := cdsDatosEmpleado.FieldByName('Turno').AsString;
    cdsUltimas.FieldByName('SuperLBL').AsString   := cdsDatosEmpleado.FieldByName('SuperLBL').AsString;
    cdsUltimas.FieldByName('Super').AsString      := cdsDatosEmpleado.FieldByName('Super').AsString;
    cdsUltimas.FieldByName('Status').AsInteger    := cdsDatosEmpleado.FieldByName('Status').AsInteger;
    TBlobField(cdsUltimas.FieldByName('Foto')).Assign(TBlobField(cdsDatosEmpleado.FieldByName('Foto')));
    cdsUltimas.FieldByName('StatusChecada').AsString := ObtieneElemento(lfTipoBitacora, FStatus);
    cdsUltimas.FieldByName('Codigo').AsString        := cdsEmpleado.Params.ParamByName('Gafete').AsString;
    cdsUltimas.FieldByName('Resultado').AsInteger    := FStatus;
    cdsUltimas.FieldByName('Mensaje').AsString       := FMensaje;
    cdsUltimas.FieldByName('Hora').AsString          := lblHora.Caption;

    cdsUltimas.Post;
    
  end; // with dmCaseta do
end;

procedure TTerminalShell.EnviaDatos;
var
  sCodigo: string;

  procedure EscribeInfo;
  begin
    with dmCaseta do
    begin
      with cdsInfoUltima do
      begin
        First;
        while not EOF do
        begin
          if (FieldByName('Codigo').AsString = sCodigo) then
          begin
            if (cdsInformacion.State in [dsInactive]) then
              cdsInformacion.Active := TRUE;
            cdsInformacion.Append;
            cdsInformacion.FieldByName('Valor').AsString   := FieldByName('Valor').AsString;
            cdsInformacion.FieldByName('Status').AsBoolean := FieldByName('Status').AsBoolean;
            cdsInformacion.Post;
          end;
          Next;
        end;
      end;
    end;
  end;

begin
  with dmCaseta do
  begin
    cdsInformacion.EmptyDataSet;
    cdsDatosEmpleado.EmptyDataSet;

    if (cdsDatosEmpleado.State in [dsInactive]) then
      cdsDatosEmpleado.Active := TRUE;
    cdsDatosEmpleado.Append;
    cdsDatosEmpleado.FieldByName('NombreLBL').AsString  := cdsUltimas.FieldByName('NombreLBL').AsString;
    cdsDatosEmpleado.FieldByName('Nombre').AsString     := cdsUltimas.FieldByName('Nombre').AsString;
    cdsDatosEmpleado.FieldByName('EmpresaLBL').AsString := cdsUltimas.FieldByName('EmpresaLBL').AsString;
    cdsDatosEmpleado.FieldByName('Empresa').AsString    := cdsUltimas.FieldByName('Empresa').AsString;
    cdsDatosEmpleado.FieldByName('HorarioLBL').AsString := cdsUltimas.FieldByName('HorarioLBL').AsString;
    cdsDatosEmpleado.FieldByName('Horario').AsString    := cdsUltimas.FieldByName('Horario').AsString;
    cdsDatosEmpleado.FieldByName('TurnoLBL').AsString   := cdsUltimas.FieldByName('TurnoLBL').AsString;
    cdsDatosEmpleado.FieldByName('Turno').AsString      := cdsUltimas.FieldByName('Turno').AsString;
    cdsDatosEmpleado.FieldByName('SuperLBL').AsString   := cdsUltimas.FieldByName('SuperLBL').AsString;
    cdsDatosEmpleado.FieldByName('Super').AsString      := cdsUltimas.FieldByName('Super').AsString;
    cdsDatosEmpleado.FieldByName('Status').AsInteger    := cdsUltimas.FieldByName('Status').AsInteger;
    TBlobField(cdsDatosEmpleado.FieldByName('Foto')).Assign(TBlobField(cdsUltimas.FieldByName('Foto')));
    sCodigo  := cdsUltimas.FieldByName('Codigo').AsString;
    FStatus  := cdsUltimas.FieldByName('Resultado').AsInteger;
    FMensaje := cdsUltimas.FieldByName('Mensaje').AsString;
    cdsDatosEmpleado.Post;
    EscribeInfo;
  end;
  LimpiaDatos;
  ChecadaOK;
  DespliegaDatos;
  SetCredencial(VACIO);
end;

procedure TTerminalShell.gridChecadas_DevExDBTableViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  // Color al fondo de los renglones.
  if Odd(AViewInfo.GridRecord.Index) then
  begin
    ACanvas.Brush.Color := RGB (247,247,247);
  end
  else
  begin
    ACanvas.Brush.Color := clWhite;
  end;
  {ACanvas.Font.Size := 14;
  ACanvas.Font.Style := [fsBold];}
end;

procedure TTerminalShell.gridChecadas_DevExDBTableViewDblClick(Sender: TObject);
begin
  inherited;
  if not dsChecadas.DataSet.IsEmpty then
  begin
    // Detener timer para limpiar pantalla.
    TimerPaseSuGafete.Enabled := FALSE;

    if (dmCaseta.cdsUltimas.RecordCount <> 0) then
      EnviaDatos;

    if dmCaseta.cdsInformacion.RecordCount > 0 then
      gridInformacion_DevEx.Visible := TRUE;

    // Activar timer para limpiar pantalla.
    TimerPaseSuGafete.Enabled := TRUE;
  end;
end;

procedure TTerminalShell.gridInformacion_DevExDBTableViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);

  procedure PintaRenglon;
  begin
    if (Sender.DataController.GetValue(AViewInfo.GridRecord.RecordIndex, 1) = false) then
        ACanvas.Font.Color := RGB (204, 87, 71)
    else
        ACanvas.Font.Color := RGB (54, 77, 121);
  end;

begin
    // Pintar rengl�n.
    PintaRenglon;
    // Color al fondo de los renglones.
    if Odd(AViewInfo.GridRecord.Index) then
        ACanvas.Brush.Color := RGB (247,247,247)
    else
        ACanvas.Brush.Color := clWhite;

    ACanvas.Font.Size := 14;
    ACanvas.Font.Style := [fsBold];
end;

procedure TTerminalShell.gridInformacion_DevExResize(Sender: TObject);
begin
  Informacion.Width := gridInformacion_DevEx.Width;
end;

procedure TTerminalShell.establecerColores;
begin
  // Colores.
  panelRespuesta.Color := RGB (195, 195, 195);
  Banner.Color := RGB (227, 221, 234);
  Banner.Font.Color := TColor ($4D3B4B);
  lblFecha.Font.Color := RGB (114, 114, 114);
  // lblDEMO.Font.Color := RGB (196, 0, 0);
  lblDEMO.Font.Color := RGB (227,66,51);
  // T�tulos.
  // NombreLBL.Font.Color := TColor ($4D3B4B);
  StatusLBL.Font.Color := TColor ($4D3B4B);
  EmpresaLBL.Font.Color := TColor ($4D3B4B);
  HorarioLBL.Font.Color := TColor ($4D3B4B);
  TurnoLBL.Font.Color := TColor ($4D3B4B);
  SuperLBL.Font.Color := TColor ($4D3B4B);
  // Datos.
  LblNombre.Font.Color := TColor ($4D3B4B);
  lblEmpresa.Font.Color := TColor ($4D3B4B);
  lblHorario.Font.Color := TColor ($4D3B4B);
  lblTurno.Font.Color := TColor ($4D3B4B);
  lblSuper.Font.Color := TColor ($4D3B4B);
end;

procedure TTerminalShell.ShowTipoConexion(const lConectado: Boolean);
begin
  inherited;
  if lConectado then
    imgEnFueraLinea.Picture := imgEnLinea.Picture
  else
    imgEnFueraLinea.Picture := imgFueraLinea.Picture;
end;

procedure TTerminalShell.TimerPaseSuGafeteTimer(Sender: TObject);
begin
  paseSuGafete;
end;

procedure TTerminalShell.paseSuGafete;
begin
    LimpiaDatos;
    gridInformacion_DevEx.Visible := FALSE;
    panelRespuesta.Color := RGB ( 195,195,195 );
    if ( RegistryCaseta.TipoGafete = egBiometrico ) then  //Biometrico  @DACP
    begin
         panelRespuesta.Caption := K_PASE_BIOMETRICO;
    end
    else
    begin
         panelRespuesta.Caption := K_PASE_GAFETE;
    end;
    panelRespuesta.Font.Color := clWhite;
    edCredencial.SetFocus;
    Foto.Picture := nil;
end;

procedure TTerminalShell.ConfiguraEstacion(const lInicial: Boolean);
begin
    inherited;
    // Timer PaseSuGafete
    TimerPaseSuGafete.Interval :=  RegistryCaseta.LimpiarPantalla * K_1000;
end;

// BIOMETRICO
procedure TTerminalShell.InicializaControles( lBiometrico : Boolean );
begin
     TimerSensorBiometrico.Enabled := lBiometrico;
end;

// BIOMETRICO
procedure TTerminalShell.InicializaBiometrico;
begin
     //Inicializa Biometrico
     addrOfImageBytes := addr( gImageBytes );
     addrOfTemplate := addr( gTemplate );
end;

//BIOMETRICO
procedure TTerminalShell.DetieneLector;
begin
     //'Hace Close del Lector'
     if ( FDrivers ) then
        SFE.Close();

     switchWorking(False);
     TimerSensorBiometrico.Enabled := False;
end;
// BIOMETRICO
function TTerminalShell.LeeArchivo:Boolean;
var
   strDbFileName : String;
   nRet : Integer;
begin
     try
        FDrivers := LoadDLL;
        nRet := -1;
        strDbFileName := RegistryCaseta.FileTemplates;
        if ( FDrivers ) then
             nRet := SFE.Open( PChar( strDbFileName ), K_TIPO_BIOMETRICO, 0 );

        if nRet = 0 then
        begin
             LogPantalla('Huellas: ' + IntToStr(SFE.GetEnrollCount()));
             Result := True;
        end
        else
        begin
             if ( FDrivers ) then
                Raise Exception.Create( '�No hay dispositivo biom�trico!' )
             else
                 Raise Exception.Create( '�Driver de biom�trico no detectado!' ) ;
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                switchWorking(False);
                LogPantalla(Error.Message);
                panelRespuesta.Color := RGB (227,66,51);
                TimerPaseSuGafete.Enabled := FALSE;
                Result := False;
           end;
     end;
end;


// BIOMETRICO
procedure TTerminalShell.InicializaSensorBiometrico;
var
   nRet, userId, fingerNum: Integer;

   function EsFormaActiva( oForma: TCustomForm ): Boolean;
   begin
        Result := ( oForma <> nil ) and ( oForma.Visible ) and ( oForma.Active );
   end;

   procedure ObtieneLectura;
   label
        stopWorking;
   begin
        if ( LeeArchivo ) then
        begin
             try
                switchWorking( True );
                LogPantalla('Detectando');
                while gWorking do
                begin
                     Application.ProcessMessages();
                     // Resetear TimerPaseSuGafete
                     TimerPaseSuGafete.Enabled := FALSE;
                     TimerPaseSuGafete.Enabled := TRUE;
                     // Siempre en el caso de pedir poner la huella en el lector,
                     // pintar de gris el panel.
                     panelRespuesta.Color := RGB (195,195,195);
                     ShowResultado( K_PASE_BIOMETRICO );
                     panelRespuesta.Font.Color := clWhite;
                     userId := 0;
                     FocusCredencial;
                     nRet := CaptureFinger; //Devuelve algun mensaje de lectura o error.

                     if nRet <> 1 then
                        goto stopWorking;

                     nRet := SFE.Identify(userId, fingerNum); //Obtiene el Identificador de la huella y el dedo
                     LogPantalla(Format('Id: %d Dedo %d',[userId, fingerNum]));

                     if nRet < 0 then
                     begin
                          PrintError(nRet);
                          RechazaChecada;
                          panelRespuesta.Color := clRed{RGB (227,66,51)};
                          Sleep( K_1000 );
                     end
                     else
                     begin
                          edCredencial.Text := IntToStr(userId);
                          goto stopWorking;
                     end;

                end;
   stopWorking:
                switchWorking(False);
             except
                   on Error: Exception do
                   begin
                        Application.HandleException( Error );
                        switchWorking(False);
                        ShowResultado( 'Reinicie Caseta' );
                   end;
             end;
         end;
   end;

begin
     switchWorking( True );
     nRet := -1;
     while gWorking do
     begin
          ObtieneLectura;
          if ( userId > 0 ) then
          begin
               ProcesaChecada;
          end;
          FocusCredencial;
     end;
end;

// BIOMETRICO
procedure TTerminalShell.SincronizaHuellas;
var
   sArchivo, sArchivoTemplate : String;
   nRet, iHuellas, iId, iDedo : Integer;
   ss           :TStringStream;
   ms           :TMemoryStream;
   sLista : String;
   oCursor: TCursor;

const
     K_EXT_TEMPLATE = '.tem';
  Procedure AnexaHuella( iHuella : Integer );
  begin
        if ( StrLleno( sLista ) ) then
            sLista := sLista + ',' + intToStr(iHuella)
        else
            sLista := intToStr(iHuella);
  end;

begin
     // Desactivar timerPaseSuGafete cuando se sincroniza huellas.
     TimerPaseSuGafete.Enabled := FALSE;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     edCredencial.Enabled := False;
     try
        iId := 0;
        iDedo := 1;
        nRet := -1;
        sLista := VACIO;
        //Proceso de Sincronizacion de Huellas
        sArchivo := RegistryCaseta.FileTemplates;
        try
           ShowResultado( 'Sincronizando huellas, espere un momento' );
           dmCaseta.SincronizaHuellas; //Proceso para traer las huellas
           ms := TMemoryStream.Create;

           if ( not LeeArchivo ) then
              Exit;

           with dmCaseta.cdsHuellas do
           begin
                iHuellas := Recordcount;
                ShowResultado( 'Sincronizando ' + intToStr(iHuellas) + ' huellas, espere un momento');
                iHuellas := 0;
                First;
                while not EOF do
                begin
                     inc( iHuellas );
                     try
                        iId := FieldByName( 'ID_NUMERO' ).AsInteger;
                        iDedo := FieldByName( 'HU_INDICE' ).AsInteger;
                        ss := TStringStream.Create( FieldByName( 'HU_HUELLA' ).AsString );

                        ms.Position := 0;
                        SZDecodeBase64( ss, ms );

                        sArchivoTemplate := ExtractFilePath( Application.ExeName ) + IntToStr( iId ) + '_' + IntToStr( iDedo ) +  K_EXT_TEMPLATE;
                        ms.SaveToFile( sArchivoTemplate);

                        nRet := SFE.LoadTemplateFromFile( PChar( sArchivoTemplate ), addrOfTemplate);

                        if nRet < 0 then
                        begin
                             raise Exception.Create(GetBioMessage(nRet));
                        end
                        else
                        begin
                             nRet := SFE.CheckFingerNum(iId, iDedo);
                             if nRet <> 0 then
                             begin
                                  nRet := SFE.TemplateEnroll( iId , iDedo, 0, addrOfTemplate);
                                  if ( ( nRet < 0 ) and ( nRet <> -102 ) ) then
                                  begin
                                       raise Exception.Create(GetBioMessage(nRet));
                                  end;

                                  AnexaHuella( FieldByName( 'HU_ID' ).AsInteger );
                             end;
                        end;
                     except
                        on Error: Exception do
                        begin
                             ShowResultado( Format( 'ERROR [%d_%d]: ' + Error.Message, [ iId, iDedo ] ) );
                        end;
                     end;

                     SysUtils.DeleteFile( sArchivoTemplate );
                     ShowResultado( 'Huella ' + intToStr(iHuellas) + '/' + intToStr(dmCaseta.cdsHuellas.Recordcount) );
                     Next;
                end;
           end;

           ms.Clear;
           Application.ProcessMessages();

           with dmCaseta do
           begin
                PrimeraVez := False;
                ReportaHuella( sLista );
           end;
           ShowResultado( 'Huellas Sincronizadas' );

           // Activar timerPaseSuGafete.
           Application.ProcessMessages();
        except
              on Error: Exception do
              begin
                   switchWorking(False);
                   ShowResultado( 'Lector inactivo. Error: ' + Error.Message );
              end;
        end;
     finally
            TimerPaseSuGafete.Enabled := TRUE;
            Screen.Cursor := oCursor;
            edCredencial.Enabled := True;
     end;
end;

// BIOMETRICO
function TTerminalShell.CaptureFinger() : Integer;
var
   nRet, nCapArea, nMaxCapArea, nResult : Integer;
begin
     try
        Result := 0;
        nResult := 1;
        nMaxCapArea := 0;
        nRet := -1;

        while True do
        begin
             try
                Application.ProcessMessages();
                if Not gWorking then
                begin
                     nResult := 0;
                     break;
                end;

                if ( FDrivers ) then
                   nRet := SFE.Capture();
                if nRet < 0 then
                begin
                     PrintError(nRet);
                     nResult := 0;
                     break;
                end;
                Application.ProcessMessages();
                if ( FDrivers ) then
                   nCapArea := SFE.IsFinger();
                if nCapArea >= 0 then
                begin
                     if nCapArea < nMaxCapArea + 2 then
                        break;

                     if nCapArea > nMaxCapArea then
                        nMaxCapArea := nCapArea;

                     if nCapArea > 45 then
                        break;
                end;
                Application.ProcessMessages();
             except
                   Result := 0;
                   Exit;
             end;
        end;
        Application.ProcessMessages();

        if nResult = 1 then
        begin
             if ( FDrivers ) then
                nRet := SFE.GetImage(addrOfImageBytes);
             if nRet < 0 then
             begin
                  PrintError( nRet );
                  nResult := 0;
             end
        end;
        Application.ProcessMessages();
        CaptureFinger := nResult;
     except
           on E : Exception do
           begin
                DetieneLector;
                InicializaControles( true );
                InicializaBiometrico;
                Result := 0;
           end;
     end;
end;

// BIOMETRICO
procedure TTerminalShell.switchWorking(bWorking : Boolean);
begin
     gWorking := bWorking;
end;

// BIOMETRICO
procedure TTerminalShell.TimerSensorBiometricoTimer(Sender: TObject);
var
   lPv : Boolean;

     function InicializaControl : Boolean;
     begin
          try
             Result := True;
          except
                on E : Exception do
                    ZetaDialogo.ZError(Self.Caption, 'Driver de dispositivo biom�trico no detectado.', 0);
          end;
     end;

begin
     TimerSensorBiometrico.Enabled := False; //Si no es biometrico, se apaga el timer
   //  LogPantalla('Termina ciclo de Timer');

     //Inicia Lector solo si es biometrico y si no hay alguna ventana abierta de configuracion
     if ( RegistryCaseta.TipoGafete = egBiometrico ) and ( edCredencial.Enabled ) then
     begin
          lPv := dmCaseta.PrimeraVez;
          dmCaseta.PrimeraVez := False;

          DetieneLector;

          if (  InicializaControl ) and ( LeeArchivo )   then
          begin
               if ( ( lPv ) and ( dmCaseta.Active ) ) then
               begin
                    ShowResultado( 'Lector biom�trico activo' );
                    SincronizaHuellas;
               end;
               InicializaSensorBiometrico;
          end;
     end;
end;

// BIOMETRICO
function TTerminalShell.GetBioMessage(error: Integer) : String;
begin
    case error of
        0: Result := 'Exito!';
        -1: Result := 'Error de imagen de huella!';
        -2: Result := 'Huella inv�lida!';
        -3: Result := 'Error de n�mero biom�trico!';
        -4: Result := 'Error de archivo de dispositivo!';
        -6: Result := 'Error de almacenamiento de dispositivo!';
        -7: Result := 'Error de sensor de dispositivo!';
        -8: Result := 'Orden de enrolamiento incorrecto!';
        -9: Result := 'No puede analizar las tres capturas, intente de nuevo!';
        -11: Result := 'La imagen no es un dedo!';
        -100: Result := 'No hay dispositivo biom�trico!';
        -101: Result := 'No puede abrir archivo de dispositivo local!';
        -102: Result := 'Ya ha enrolado esa huella!';
        -103: Result := 'Error de identificaci�n de huella!';
        -104: Result := 'Error de verificaci�n de huella!';
        -105: Result := 'No puede abrir la imagen de huella!';
        -106: Result := 'No puede crear imagen de huella!';
        -107: Result := 'No puede abrir huella!';
        -108: Result := 'No puede crear huella!';
        else Result := 'Error desconocido! (Numero =' + IntToStr(error) + ')';
    end;
end;
// BIOMETRICO
procedure TTerminalShell.LogPantalla(sMensaje : String);
begin
     panelRespuesta.Caption := sMensaje;
end;

// BIOMETRICO
procedure TTerminalShell.PrintError(error: Integer);
begin
      if( error <> 0 ) then
      begin
           panelRespuesta.Color := RGB (227,66,51);
      end;
      ShowResultado( GetBioMessage(error) );
end;

end.
