unit dCaseta;

interface

uses
  DBaseCafeCliente, CafeteraWSDL, FAsciiServer, ZetaCommonLists, ZetaClientDataSet, SysUtils, Forms, InvokeRegistry, DB, ExtCtrls,
  DBClient, Classes, Rio, SOAPHTTPClient, Types;

type
  eStatusChecada = (echOK, echError, echAdvertencia);

  TdmCaseta = class(TdmBaseCafeCliente)
    cdsDatosEmpleado: TClientDataSet;
    cdsDatosEmpleadoNombre: TStringField;
    cdsDatosEmpleadoEmpresa: TStringField;
    cdsDatosEmpleadoStatus: TIntegerField;
    cdsDatosEmpleadoHorario: TStringField;
    cdsDatosEmpleadoTurno: TStringField;
    cdsDatosEmpleadoSuper: TStringField;
    cdsDatosEmpleadoFoto: TBlobField;
    cdsDatosEmpleadoNombreLBL: TStringField;
    cdsDatosEmpleadoEmpresaLBL: TStringField;
    cdsDatosEmpleadoHorarioLBL: TStringField;
    cdsDatosEmpleadoTurnoLBL: TStringField;
    cdsDatosEmpleadoSuperLBL: TStringField;
    cdsUltimas: TClientDataSet;
    cdsInformacion: TClientDataSet;
    cdsInformacionValor: TStringField;
    cdsUltimasNombre: TStringField;
    cdsUltimasEmpresa: TStringField;
    cdsUltimasStatus: TIntegerField;
    cdsUltimasHorario: TStringField;
    cdsUltimasTurno: TStringField;
    cdsUltimasSuper: TStringField;
    cdsUltimasFoto: TBlobField;
    cdsUltimasNombreLBL: TStringField;
    cdsUltimasEmpresaLBL: TStringField;
    cdsUltimasHorarioLBL: TStringField;
    cdsUltimasTurnoLBL: TStringField;
    cdsUltimasSuperLBL: TStringField;
    cdsUltimasStatusChecada: TStringField;
    cdsUltimasCodigo: TStringField;
    cdsUltimasResultado: TIntegerField;
    cdsUltimasMensaje: TStringField;
    cdsInformacionStatus: TBooleanField;
    cdsInfoUltima: TClientDataSet;
    StringField1: TStringField;
    BooleanField1: TBooleanField;
    cdsInfoUltimaCodigo: TStringField;
    cdsUltimasHora: TStringField;
    cdsReiniciaHuellas: TClientDataSet;// BIOMETRICO
    dsReiniciaHuellas: TDataSource;// BIOMETRICO
    cdsHuellas: TClientDataSet;  // BIOMETRICO
    dsHuellas: TDataSource;// BIOMETRICO
    cdsReportaHuellas: TClientDataSet;// BIOMETRICO
    dsReportaHuellas: TDataSource;
    cdsListado: TClientDataSet;  // BIOMETRICO
  private
    { Private declarations }
    FCaseta        : String;
    FTipoCredencial: eTipoGafete;
    FUsaTeclado    : Boolean;
    FMostrarFoto   : Boolean;
    FPrimeraVez    : Boolean;    //BIOMETRICO
    function GetFileHuellas: String; // BIOMETRICO
    function GetArchivoHuellas: String; // BIOMETRICO
    function ChecadaToStr(const sGafete: String; const dValue: TDateTime): string;
    procedure SetCaseta(const Value: String);
    procedure SetSocket(const sDireccion: String; const iPuerto: Integer);
  public
    { Public declarations }
    property Caseta        : String read FCaseta write SetCaseta; // FEstacion;
    property UsaTeclado    : Boolean read FUsaTeclado write FUsaTeclado;
    property TipoCredencial: eTipoGafete read FTipoCredencial write FTipoCredencial;
    function ExisteEmpleado(var sGafete: String; const TipoChecada: eTipoChecadas): Boolean;
    function ValidaResultado(var sLetrero: String): eStatusChecada;
    procedure Init; override;
    procedure SetMostrarFoto(const lMostrar: Boolean);
    procedure SincronizaHuellas; // BIOMETRICO
    property PrimeraVez: Boolean read FPrimeraVez write FPrimeraVez; // BIOMETRICO
    procedure ReiniciaHuellas;                     // BIOMETRICO
    procedure ReportaHuella(sLista: String);       // BIOMETRICO
    property ArchivoHuellas: String read GetArchivoHuellas; // BIOMETRICO

  end;

var
  dmCaseta: TdmCaseta;

implementation

uses
    FTerminalShell, ZetaSystemWorking, ZetaCommonClasses, ZetaCommonTools,ZetaMessages,ZetaWinAPITools, CafeteraConsts, CafeteraUtils;

{$R *.DFM}
const
     K_FILE_HUELLAS = 'HUELLAS.DAT'; // BIOMETRICO

procedure TdmCaseta.Init;
begin
  inherited;
  with RegistryCaseta do begin
    Self.Caseta         := Identificacion;
    Self.UsaTeclado     := UsaTeclado; // Se usan Constantemente, para evitar leer el registry cada vez
    Self.TipoCredencial := TipoGafete;
    SetMostrarFoto(MostrarFoto);
    Self.Interval       := Intervalo;
    SetSocket(Servidor, Puerto);
  end;
end;

procedure TdmCaseta.SetMostrarFoto(const lMostrar: Boolean);
begin
  FMostrarFoto := lMostrar
end;

{ TAstaClientSocket }

procedure TdmCaseta.SetSocket(const sDireccion: String; const iPuerto: Integer);
begin
  ZetaSystemWorking.InitAnimation('Conectando Accesos..');
  Direccion := sDireccion;
  Puerto    := iPuerto;
  Active    := True;
  ZetaSystemWorking.EndAnimation;
end;

procedure TdmCaseta.SetCaseta(const Value: String);
var
  sCaseta: String;
begin
  sCaseta := Value;
  sCaseta := ZetaCommonTools.PadRCar(sCaseta, 4, ' ');
  sCaseta := ZetaCommonTools.PadRCar(sCaseta, 10, '0');
  FCaseta := sCaseta;
end;

function TdmCaseta.ValidaResultado(var sLetrero: String): eStatusChecada;
begin
  if Active then begin
    with cdsEmpleado do begin
      Result   := eStatusChecada(Params.ParamByName('Status').AsInteger);
      sLetrero := Params.ParamByName('Caseta').AsString;
    end;
  end else begin
    Result   := echError;
    sLetrero := 'Servidor Desconectado';
  end;
end;

function TdmCaseta.ExisteEmpleado(var sGafete: String; const TipoChecada: eTipoChecadas): Boolean;
var
  dValue: TDateTime;
begin
  dValue := Now;
  AsciiServer.Write(ChecadaToStr(sGafete, dValue));
  with cdsEmpleado do
  begin
      Open;
      EmptyDataSet;
      with Params do
      begin
          ParamByName('Gafete').AsString      := sGafete;
          ParamByName('FechaHora').AsDateTime := dValue;
          ParamByName('Caseta').AsString      := FCaseta;
          ParamByName('TipoGafete').AsInteger := Ord(FTipoCredencial);
          ParamByName('Tipo').AsInteger       := Ord(TipoChecada);
      end;

      // Enviar peticion al servidor SOAP
      SOAPMsg := CreateSOAPMsg(K_VALIDAR_ACCESO, ParamsToXML(Params));

      SendSOAPMsg(SOAPMsg, Params, cdsEmpleado);

      // SendSOAPMsg(SOAPMsg, Params, cdsEmpleado);
      FreeAndNil(SOAPMsg);

      Result  := (not IsEmpty);
      sGafete := Params.ParamByName('Gafete').AsString;
  end;
end;

function TdmCaseta.ChecadaToStr(const sGafete: String; const dValue: TDateTime): string;
begin
  Result := FCaseta + '#' + IntToStr(Ord(FTipoCredencial)) + sGafete + FormatDateTime('mmddhhnn', dValue);
end;


// BIOMETRICO
procedure TdmCaseta.SincronizaHuellas;
var
   sDataSet, sLetrero: AnsiString;
   aDetalle, aTotales : TByteDynArray;

   function Converter(P: TByteDynArray): string;
   var
      Buffer: AnsiString;
   begin
        SetLength(Buffer, Length(P) );
        System.Move(P[0], Buffer[1], Length(P));
        Result := string (  Buffer ) ;
   end;

begin
     with cdsHuellas do
     begin
          with Params do
          begin
               ParamByName('Huellas').AsString  := VACIO;
               ParamByName('Letrero').AsString  := VACIO;
               ParamByName('Computer').AsString := ZetaWinAPITools.GetComputerName;
               ParamByName('idCafeteria').AsString := RegistryCaseta.Identificacion;
               ParamByName('TipoEstacion').AsInteger := Ord( dpCaseta );
          end;

          sLetrero := Params.ParamByName('Letrero').AsString;
          sDataSet := Params.ParamByName('Huellas').AsString;


          SOAPMsg := CreateSOAPMsg(K_OBTENER_HUELLAS, ParamsToXML(Params));
          SendSOAPMsgXML_DetalleTotales(aDetalle, aTotales, SOAPMsg, Params, cdsListado);

          sDataSet :=  Converter( aDetalle);
          sLetrero :=  Converter( aTotales);
     end;
     if StrVacio( sLetrero ) then // Si llega vacio no se detectaron problemas
     begin
          try
             XMLToDataSet( sDataSet, cdsHuellas );
             if SysUtils.FileExists( GetFileHuellas ) then
                                    SysUtils.Deletefile( GetFileHuellas );
             cdsHuellas.SaveToFile(GetFileHuellas, dfXMLUTF8);
          except
               on Error: Exception do
               begin
                    sLetrero := 'No se gener� archivo de huellas: ' + Error.Message + CR_LF + sLetrero;
               end;
          end;
     end;
    if StrLleno( sLetrero ) then
       raise Exception.Create( sLetrero );
end;

// BIOMETRICO
procedure TdmCaseta.ReiniciaHuellas;
begin
     with cdsReiniciaHuellas do
     begin
           Params.ParamByName('Computer').AsString := ZetaWinAPITools.GetComputerName;
           // Enviar peticion al servidor SOAP
           SOAPMsg := CreateSOAPMsg(K_BORRAR_RELACION_HUELLAS, ParamsToXML(Params));
           SendSOAPMsg(SOAPMsg);
           FreeAndNil(SOAPMsg);
     end;
end;

// BIOMETRICO
procedure TdmCaseta.ReportaHuella(sLista: String);
begin
      with cdsReportaHuellas do
      begin
           Params.ParamByName('Computer').AsString := ZetaWinAPITools.GetComputerName;
           Params.ParamByName('Lista').AsString    := sLista;
           // Enviar peticion al servidor SOAP
           SOAPMsg := CreateSOAPMsg(K_REPORTAR_HUELLAS, ParamsToXML(Params));
           SendSOAPMsg(SOAPMsg);
           FreeAndNil(SOAPMsg);
      end;
end;

// BIOMETRICO
function TdmCaseta.GetArchivoHuellas: String;
begin
     Result := RegistryCaseta.FileTemplates;
end;

// BIOMETRICO
function TdmCaseta.GetFileHuellas: String;
begin
     Result := ZetaMessages.SetFileNameDefaultPath(K_FILE_HUELLAS);
end;


end.
