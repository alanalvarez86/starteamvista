unit FConfigura;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, ComCtrls, Registry, Grids, DBGrids, Db, FileCtrl,
     ZBaseDlgModal_DevEx,
     ZetaDBGrid,
     ZetaKeyCombo,
     ZetaMessages,SFE,
     FTerminalShell, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
     Vcl.Menus, dxBarBuiltInMenu, cxControls, cxContainer, cxEdit, ZetaEdit,
     Vcl.Mask, ZetaNumero, cxCheckBox, cxTrackBar, cxTextEdit, cxPC, Vcl.ImgList,
     cxButtons,cxGroupBox;

type
  TFormaConfigura = class(TZetaDlgModal_DevEx)
    OpenDialog: TOpenDialog;
    OpenDialogSonidos: TOpenDialog;
    Configuracion_DevEx: TcxPageControl;
    General_DevEx: TcxTabSheet;
    Server_DevEx: TcxTabSheet;
    tsSeguridad_DevEx: TcxTabSheet;
    Impresora_DevEx: TcxTabSheet;
    Sonidos_DevEx: TcxTabSheet;
    Serial_DevEx: TcxTabSheet;
    Label4: TLabel;
    Label16: TLabel;
    Label1: TLabel;
    editID: TcxTextEdit;
    Letrero: TGroupBox;
    Label3: TLabel;
    editLetrero: TcxTextEdit;
    cbTipoGafete: TZetaKeyCombo;
    cbLectorProximidad: TZetaKeyCombo;
    DefaultGB: TGroupBox;
    Label18: TLabel;
    Label19: TLabel;
    edDefaultEmpresa: TcxTextEdit;
    edDefaultCredencial: TcxTextEdit;
    Servidor: TGroupBox;
    Label6: TLabel;
    Label35: TLabel;
    Label34: TLabel;
    lblSegundos: TLabel;
    editDireccion: TcxTextEdit;
    editPuerto: TZetaNumero;
    editIntervalo: TZetaNumero;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    eGafeteAdmon: TcxTextEdit;
    eClaveAdmon: TcxTextEdit;
    LblPuerto: TLabel;
    EditImpresora: TcxTextEdit;
    Codigos: TGroupBox;
    Label2: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    EditCodigoInicial: TcxTextEdit;
    EditCodigoFinal: TcxTextEdit;
    EditEject: TcxTextEdit;
    GroupBox1: TGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    EAcepta: TcxTextEdit;
    ERechazo: TcxTextEdit;
    Label13: TLabel;
    Label14: TLabel;
    CBPuertoA: TZetaKeyCombo;
    CBPuertoB: TZetaKeyCombo;
    GBCodigosSalida: TGroupBox;
    LblCodigoAcepta: TLabel;
    LblCodigoRechazo: TLabel;
    EditSerialAcepta: TcxTextEdit;
    EditSerialRechazo: TcxTextEdit;
    GBCodigosApagados: TGroupBox;
    LblCodigoApagadoRechazo: TLabel;
    LblCodigoApagadoAceptado: TLabel;
    LblEsperaAceptado: TLabel;
    LblEsperaRechazado: TLabel;
    EditApagadoRechazo: TZetaEdit;
    EditApagadoAceptado: TZetaEdit;
    EditEsperaAceptado: TZetaNumero;
    EditEsperaRechazo: TZetaNumero;
    bPUERTO_DevEx: TcxButton;
    cxImageList19_PanelBotones: TcxImageList;
    bSonidoAceptacion: TcxButton;
    cxButton1: TcxButton;
    BtnConfigB_DevEx: TcxButton;
    BtnConfigA_DevEx: TcxButton;
    cbUsaTeclado_DevEx: TcxCheckBox;
    cbMostrarFoto_DevEx: TcxCheckBox;
    Label32: TLabel;
    editLimpiarPantalla: TZetaNumero;
    Label36: TLabel;
    tbVelocidad_DevEx: TcxTrackBar;
    Biometrico: TcxTabSheet;
    cxGroupBox1: TcxGroupBox;
    RutaGuardaHuellasLBL: TLabel;
    EditGuardaHuellas: TcxTextEdit;
    AlmacenaHuella: TcxButton;
    RecargaHuellas_DevEx: TcxButton;
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure bPUERTOClick(Sender: TObject);
    procedure CBPuertoAChange(Sender: TObject);
    procedure BtnConfigAClick(Sender: TObject);
    procedure cbTipoGafeteChange(Sender: TObject);
    procedure RecargaHuellas_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FComEntrada: String;
    FComSalida: String;
    FDrivers: boolean;
    procedure EscribeRegistry;
    procedure LeeRegistry;
    procedure HabilitaBtnSeriales;
    procedure CambiaConfigSerial(const iPuerto: Integer; var sConfig: String);
    procedure ToggleBiometric(lBiometrico: Boolean); // BIOMETRICO
    function HayDispositivo: Boolean;
  public
    { Public declarations }
    procedure SetControlesServidor;
  end;

var
  FormaConfigura: TFormaConfigura;

implementation

uses FConfigSerial,
     dcaseta,
     ZetaWinApiTools,
     ZetaNetworkBrowser_DevEx,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaCommonLists,
     ZetaCommonClasses;

{$R *.DFM}

procedure TFormaConfigura.FormCreate(Sender: TObject);
begin
     inherited;
     Configuracion_DevEx.ActivePage := General_DevEx;
     FDrivers := LoadDLL;
end;

procedure TFormaConfigura.FormShow(Sender: TObject);
begin
     inherited;
     LeeRegistry;
     HabilitaBtnSeriales;
     // Solo se activa si el tipo de gafete seleccionado es de proximidad.
     cbLectorProximidad.Enabled := ( cbTipoGafete.ItemIndex = Ord( egProximidad ) );
     // En caso de ser Lector Biometrico Activa Controles de Configuracion de Biom�trico
     ToggleBiometric( ( cbTipoGafete.ItemIndex = Ord( egBiometrico ) ) );
end;

procedure TFormaConfigura.LeeRegistry;
begin
     with RegistryCaseta do
     begin
          editID.Text              := Identificacion;
          cbTipoGafete.ItemIndex   := Ord( TipoGafete );
          cbLectorProximidad.ItemIndex := Ord( LectorProximidad );
          edDefaultEmpresa.Text    := DefaultEmpresa;
          edDefaultCredencial.Text := DefaultCredencial;
          cbMostrarFoto_DevEx.Checked := MostrarFoto;
          cbUsaTeclado_DevEx.Checked  := UsaTeclado;
          editLetrero.Text            := Banner;
          tbVelocidad_DevEx.Position  := Velocidad;
          editDireccion.Text       := Servidor;
          editPuerto.Valor         := Puerto;
          editIntervalo.Valor      := Intervalo;
          editLimpiarPantalla.Valor:= LimpiarPantalla;
          editImpresora.Text       := Impresora;
          editCodigoInicial.Text   := CodigoInicial;
          editCodigoFinal.Text     := CodigoFinal;
          editEject.Text           := Eject;
          eAcepta.Text             := SonidoAcepta;
          eRechazo.Text            := SonidoRechazo;
          eGafeteAdmon.Text        := GafeteAdmon;
          eClaveAdmon.Text         := ClaveAdmon;
          CBPuertoA.Valor          := SerialA;
          CBPuertoB.Valor          := SerialB;
          EditSerialAcepta.Text    := CodigoAcepta;
          EditSerialRechazo.Text   :=  CodigoRechazo;
          FComEntrada              := ComEntrada;
          FComSalida               := ComSalida;

          EditApagadoAceptado.Text := ApagadoAcepta;
          EditEsperaAceptado.Valor := EsperaAcepta;
          EditApagadoRechazo.Text  := ApagadoRechazo;
          EditEsperaRechazo.Valor  := EsperaRechazo;

          //BIOMETRICO    @DACP
          EditGuardaHuellas.Text   := FileTemplates;

          if StrVacio( SonidoRechazo ) and StrVacio( Servidor ) then
             eRechazo.Text := ZetaWinApiTools.GetWindowsDir + '\MEDIA\CHORD.WAV';
     end;
end;

procedure TFormaConfigura.EscribeRegistry;
const
      K_MAX_LIMPIAR_PANTALLA = 99;
begin
     //FTerminalShell.TipoGafete      := cbTipoGafete.ItemIndex;
     with RegistryCaseta do
     begin
          Identificacion   := editID.Text;
          TipoGafete       := eTipoGafete( cbTipoGafete.ItemIndex );
          LectorProximidad := eTipoProximidad( cbLectorProximidad.ItemIndex );
          DefaultEmpresa   := edDefaultEmpresa.Text;
          DefaultCredencial:= edDefaultCredencial.Text;
          MostrarFoto      := cbMostrarFoto_DevEx.Checked;
          UsaTeclado       := cbUsaTeclado_DevEx.Checked;
          Banner           := editLetrero.Text;
          Velocidad        := tbVelocidad_DevEx.Position;
          Servidor         := editDireccion.Text;
          Puerto           := editPuerto.ValorEntero;
          Intervalo        := editIntervalo.ValorEntero;
          // Cumplir requisito de valor para editTiempoEspera de m�ximo: K_MAX_LIMPIAR_PANTALLA = 99;
          if editLimpiarPantalla.ValorEntero > K_MAX_LIMPIAR_PANTALLA then
              LimpiarPantalla := K_MAX_LIMPIAR_PANTALLA
          else
            LimpiarPantalla      := editLimpiarPantalla.ValorEntero;

          Impresora        := editImpresora.Text;
          CodigoInicial    := editCodigoInicial.Text;
          CodigoFinal      := editCodigoFinal.Text;
          Eject            := editEject.Text;
          SonidoAcepta     := eAcepta.Text;
          SonidoRechazo    := eRechazo.Text;
          GafeteAdmon      := eGafeteAdmon.Text;
          ClaveAdmon       := eClaveAdmon.Text;
          SerialA          := CBPuertoA.Valor;
          SerialB          := CBPuertoB.Valor;
          CodigoAcepta     := EditSerialAcepta.Text;
          CodigoRechazo    := EditSerialRechazo.Text;
          ComEntrada       := FComEntrada;
          ComSalida        := FComSalida;

          ApagadoAcepta  := EditApagadoAceptado.Text;
          EsperaAcepta   := EditEsperaAceptado.ValorEntero;
          ApagadoRechazo := EditApagadoRechazo.Text;
          EsperaRechazo  := EditEsperaRechazo.ValorEntero;
          //BIOMETRICO
          FileTemplates  := EditGuardaHuellas.Text;

     end;
end;

procedure TFormaConfigura.OKClick(Sender: TObject);
const
K_MIN_LIMPIAR_PANTALLA = 5;
begin
     inherited;
     if strVacio( editDireccion.Text ) then
     begin
          ZetaDialogo.ZError( self.Caption, 'No puede quedar vaci� el servidor !', 0 );
          SetControlesServidor;
     end
     else if ( editLimpiarPantalla.ValorEntero < K_MIN_LIMPIAR_PANTALLA ) then
     begin
          ZetaDialogo.ZError(Self.Caption, Format ('El valor para limpiar pantalla no debe ser menor a %d segundos', [K_MIN_LIMPIAR_PANTALLA]), 0);
     end
     else if strVacio( EditGuardaHuellas.Text ) and  ( cbTipoGafete.ItemIndex = Ord( egBiometrico ) )  then
     begin
          ZetaDialogo.ZError( Self.Caption, '�Es necesario que tenga configurado el almacenamiento local de las huellas!', 0);
          Configuracion_DevEx.ActivePage := Biometrico;
          ActiveControl := EditGuardaHuellas;
     end
     else
     begin        
          if ( cbTipoGafete.ItemIndex = Ord( egBiometrico ) )then    //si existen alg�n cambio recargamos huellas
          begin                    
                if ( EditGuardaHuellas.Text <> RegistryCaseta.FileTemplates ) or ( eTipoGafete( cbTipoGafete.ItemIndex ) <> RegistryCaseta.TipoGafete ) then
                begin
                      with dmCaseta do 
                      begin
                           PrimeraVez := True;
                           ReiniciaHuellas;
                      end;
                end;
          end;

          EscribeRegistry;
          ModalResult := mrOk;
     end;
end;

{ Seleccion de Archivos }

procedure TFormaConfigura.ArchivoSeekClick(Sender: TObject);
var
   sArchivo: String;
   oEdit : TcxTextEdit;
   oDialog : TOpenDialog;
begin
     case TControl(Sender).Tag of
          2 :
          begin
               oEdit:= EAcepta;
               oDialog := OpenDialogSonidos;
          end;
          3 :
          begin
               oEdit:= ERechazo;
               oDialog := OpenDialogSonidos;
          end;
          4 :
          begin
               oEdit:= EditGuardaHuellas;
               oDialog := OpenDialog;
          end;
          else
          begin
               oEdit := nil;
               oDialog := nil;
          end;

     end;
     sArchivo := oEdit.Text;
     with oDialog do
     begin
          FileName := ExtractFileName( sArchivo );
          InitialDir := ExtractFilePath( sArchivo );
          if (oEdit.Tag in [2,3,4]) AND
             StrVacio(InitialDir) then
             InitialDir := ZetaWinApiTools.GetWinSysDir;

          if Execute then
             oEdit.Text := FileName;
     end;
end;

procedure TFormaConfigura.bPUERTOClick(Sender: TObject);
begin
     inherited;
     EditImpresora.Text := ZetaNetworkBrowser_DevEx.GetPrinterPort( EditImpresora.Text );
end;

procedure TFormaConfigura.CBPuertoAChange(Sender: TObject);
begin
     inherited;
     HabilitaBtnSeriales;
end;

procedure TFormaConfigura.ToggleBiometric(lBiometrico: Boolean);
begin
     EditGuardaHuellas.Enabled    := lBiometrico;
     AlmacenaHuella.Enabled       := lBiometrico;
     RecargaHuellas_DevEx.Enabled := lBiometrico;
end;

procedure TFormaConfigura.cbTipoGafeteChange(Sender: TObject);
var
  lSeleccionBiometrico: Boolean;
begin
     inherited;
     lSeleccionBiometrico := ( cbTipoGafete.ItemIndex = Ord( egBiometrico ) );

     // Activar cbLectorProximidad solo si el tipo de gafete es de proximidad
     cbLectorProximidad.Enabled := not lSeleccionBiometrico; 
     ToggleBiometric( lSeleccionBiometrico ); // En caso de ser Lector Biometrico Activa Controles de Configuracion de Biometrico
end;

procedure TFormaConfigura.HabilitaBtnSeriales;
begin
     BtnConfigA_DevEx.Enabled := ( CBPuertoA.Valor > 0 );
     BtnConfigB_DevEx.Enabled := ( CBPuertoB.Valor > 0 ) and ( CBPuertoA.Valor <> CBPuertoB.Valor );
     EditSerialAcepta.Enabled := ( CBPuertoB.Valor > 0 );
     EditSerialRechazo.Enabled := EditSerialAcepta.Enabled;
     LblCodigoAcepta.Enabled := EditSerialAcepta.Enabled;
     LblCodigoRechazo.Enabled := EditSerialAcepta.Enabled;
     GBCodigosSalida.Enabled := EditSerialAcepta.Enabled;
end;

procedure TFormaConfigura.BtnConfigAClick(Sender: TObject);
begin
     inherited;
     if ( TControl( Sender ).Tag = 0 ) then
        CambiaConfigSerial( CBPuertoA.Valor, FComEntrada )
     else
        CambiaConfigSerial( CBPuertoB.Valor, FComSalida );
end;

procedure TFormaConfigura.CambiaConfigSerial( const iPuerto: Integer; var sConfig: String );
begin
     if ( ConfigSerial = nil ) then
        ConfigSerial := TConfigSerial.Create( Application );
     with ConfigSerial do
     begin
          InfoPuerto := TerminalShell.GetInfoPuerto( iPuerto, sConfig );
          ShowModal;
          if ( ModalResult = mrOk ) then
             sConfig := TerminalShell.SetInfoPuerto( InfoPuerto );
     end;
end;

procedure TFormaConfigura.SetControlesServidor;
begin
     Configuracion_DevEx.ActivePage := Server_DevEx;
     self.ActiveControl := editDireccion;
end;

//BIOMETRICO
function TFormaConfigura.HayDispositivo: Boolean;
var
   nRet: Integer;

function InicializaControl: Boolean;
begin
     try
        Result := FDrivers;
     except
         on E: Exception do
           ZetaDialogo.ZError( Self.Caption, 'Driver de dispositivo biom�trico no detectado.', 0 );
     end;
end;

begin
     nRet := 1;
     if ( InicializaControl ) then
     begin
          nRet := SFE.Open( PChar( EditGuardaHuellas.Text ), K_TIPO_BIOMETRICO, 0 );
          SFE.Close;
     end;
     Result := ( ( nRet = 0 ) or ( nRet = -101 ) );
end;

 //BIOMETRICO
procedure TFormaConfigura.RecargaHuellas_DevExClick(Sender: TObject);
begin
     inherited;
     // Hace que la caseta recargue TODAS las huellas nuevamente la siguiente vez que se active el sensor.
     if ( cbTipoGafete.ItemIndex = Ord( egBiometrico ) ) then
     begin
          if ( HayDispositivo ) then
          begin
               if ( strLleno( EditGuardaHuellas.Text ) ) then
               begin
                    if ( ZConfirm( Self.Caption, '�Est� seguro de solicitar nuevamente todas las huellas? ' + CR_LF + 'Este proceso puede tomar unos minutos.', 0, mbNo ) ) then
                    begin
                         RegistryCaseta.FileTemplates := EditGuardaHuellas.Text;
                         DeleteFile( RegistryCaseta.FileTemplates );
                         with dmCaseta do
                         begin
                              PrimeraVez := True;
                              ReiniciaHuellas;
                         end;
                         ZInformation( Self.Caption, 'Al salir de la pantalla de la configuraci�n de caseta, se iniciar� la descarga de todas huellas.', 0);
                    end;
               end
               else
                   ZetaDialogo.ZError( Self.Caption, 'La ruta de almacenamiento local de huellas no puede quedar vacia.', 0 );
          end
          else
              ZetaDialogo.ZError( Self.Caption, 'No hay dispositivo biom�trico conectado.', 0 );
     end
     else
         ZetaDialogo.ZError( Self.Caption, 'Funcionalidad disponible �nicamente cuando esta seleccionado el tipo de gafete: Biom�trico.', 0);
end;

end.
