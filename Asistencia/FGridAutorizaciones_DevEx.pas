unit FGridAutorizaciones_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, Grids, DBGrids, DBCtrls, Buttons, ExtCtrls, StdCtrls, Mask,
     ZBaseGridEdicion_DevEx,
     ZetaFecha, ZetaDBGrid, ZetaKeyCombo, 
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxBarExtItems, dxSkinsdxBarPainter,
  cxLabel, cxDropDownEdit, dxBar, cxBarEditItem, cxClasses, ImgList,
  cxNavigator, cxDBNavigator, cxButtons;

type
  TGridAutorizaciones_DevEx = class(TBaseGridEdicion_DevEx)
    ZFecha: TZetaDBFecha;
    dxBarControlContainerItem1: TdxBarControlContainerItem;
    cxBarEditItem1: TcxBarEditItem;
    CcbOperacion: TCustomdxBarCombo;
    cxBarEditItem2: TcxBarEditItem;
    cbOperacion: TdxBarCombo;
    procedure FormCreate(Sender: TObject);
    procedure ZetaDBGridColExit(Sender: TObject);
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure OKClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    procedure BuscaTipo;
    procedure BuscaEmpleado;
    procedure BuscaMotivo;
    procedure ShowColumnasAutorizaciones( const lVisible: boolean );
    procedure LlenaCombo( const lGlobal: boolean );
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Buscar; override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
    {function GetFechaTarjeta: TDate;
    function PuedeBorrar: Boolean;override;
    function PuedeModificar: Boolean;override;}
  public
    { Public declarations }
  end;

var
  GridAutorizaciones_DevEx: TGridAutorizaciones_DevEx;

implementation

uses DAsistencia,
     DCliente,
     DTablas,
     DSistema,
     ZGlobalTress,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaBuscaEmpleado_DevEx,
     FTipoAutoriza_DevEx;

{$R *.DFM}

{ TGridAutorizaciones }

procedure TGridAutorizaciones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H20221_Autorizar_extras_y_permisos;
     LlenaCombo( dmAsistencia.AprobarPrendido );
     cbOperacion.ItemIndex := dmAsistencia.OperacionConflicto; // Default Anterior
end;

procedure TGridAutorizaciones_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     dmTablas.cdsMotAuto.Conectar;
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmAsistencia do
     begin
          cdsAutorizaciones.Refrescar;
          DataSource.DataSet:= cdsAutorizaciones;
     end;
     ShowColumnasAutorizaciones( dmAsistencia.AprobarPrendido );
end;

procedure TGridAutorizaciones_DevEx.Buscar;
begin
     with ZetaDBGrid.SelectedField do
     begin
          if ( FieldName = 'CB_CODIGO' ) then
             BuscaEmpleado
          else
              if ( FieldName = 'CH_TIPO' ) then
                 BuscaTipo
              else
                  if ( FieldName = 'CH_RELOJ' ) then
                     BuscaMotivo;
     end;
end;

procedure TGridAutorizaciones_DevEx.BuscaEmpleado;
var
   sKey, sDescription: String;
begin
      if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
      begin
          with dmAsistencia.cdsAutorizaciones do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CB_CODIGO' ).AsString := sKey;
          end;
      end;
end;

procedure TGridAutorizaciones_DevEx.BuscaTipo;
var
   iNewTipo: Integer;
begin
     with dmAsistencia.cdsAutorizaciones do
     begin
          iNewTipo := FtipoAutoriza_DevEx.ShowTipoAutoriza( FieldByName( 'CH_TIPO' ).AsInteger );
          if( iNewTipo <> FieldByName( 'CH_TIPO' ).AsInteger ) then
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CH_TIPO' ).AsInteger := iNewTipo;
          end;
     end;
end;

procedure TGridAutorizaciones_DevEx.BuscaMotivo;
var
   sMotivo, sMotivoDesc: String;
begin
     with dmAsistencia.cdsAutorizaciones do
     begin
          sMotivo := FieldByName( 'CH_RELOJ' ).AsString;
          if dmTablas.cdsMotAuto.Search_DevEx( Format(' TB_TIPO = %d or TB_TIPO = %d  ',[ K_MOTIVO_AUTORIZ_OFFSET,FieldByName( 'CH_TIPO' ).AsInteger - K_AUTORIZACIONES_OFFSET ]), sMotivo, sMotivoDesc ) then
          begin
               if ( sMotivo <> FieldByName( 'CH_RELOJ' ).AsString ) then
               begin
                    if not ( State in [ dsEdit, dsInsert ] ) then
                       Edit;
                    FieldByName( 'CH_RELOJ' ).AsString := sMotivo;
               end;
          end;
     end;
end;

procedure TGridAutorizaciones_DevEx.KeyPress(var Key: Char);

procedure CambiaTipo( const Tipo: eTipoChecadas );
begin
     with dmAsistencia.cdsAutorizaciones do
     begin
          if ( Ord( Tipo ) <> FieldByName( 'CH_TIPO' ).AsInteger ) then
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CH_TIPO' ).AsInteger := Ord( Tipo );
          end;
     end;
     Key := #0;
     with ZetaDBGrid do
     begin
          SetFocus;
          SelectedField := dmAsistencia.cdsAutorizaciones.FieldByName( 'CH_RELOJ' );
     end;
end;

begin
     inherited;
     if GridEnfocado then
     begin
          if ( ( Key <> Chr(9) ) and ( Key <> #0 ) ) then
          begin
               if ( ZetaDBGrid.SelectedField.FieldName = 'AU_FECHA' ) then
               begin
                    //zFecha.Visible:= True;
                    zFecha.SetFocus;
                    SendMessage( zFecha.Handle, WM_CHAR, Word( Key ), 0 );
                    Key := #0;
               end
               else
                   if ( ZetaDBGrid.SelectedField.FieldName = 'CH_TIPO' ) then
                   begin
                        case Key of
                            'X', 'x' : CambiaTipo( chConGoce );
                            'Y', 'y' : CambiaTipo( chSinGoce );
                            'E', 'e' : CambiaTipo( chExtras );
                            'S', 's' : CambiaTipo( chDescanso );
                            'Q', 'q' : CambiaTipo( chConGoceEntrada );
                            'R', 'r' : CambiaTipo( chSinGoceEntrada );
                            'P', 'p' : CambiaTipo( chPrepagFueraJornada );
                            'B', 'b' : CambiaTipo( chPrepagDentroJornada );
                        end;
                        Key:= #0;
                   end;
          end;
     end
     else
         if ( ActiveControl = zFecha ) and ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
         begin
              Key := #0;
              with ZetaDBGrid do
              begin
                   SetFocus;
                   SelectedField := dmAsistencia.cdsAutorizaciones.FieldByName( 'HORAS' );
              end;
         end;
end;

procedure TGridAutorizaciones_DevEx.ZetaDBGridColExit(Sender: TObject);
begin
     inherited;
     with ZetaDBGrid do
     begin
          if ( SelectedField <> nil ) and
             ( SelectedField.FieldName = 'AU_FECHA' ) and
             ( zFecha.Visible ) then
          begin
               zFecha.Visible:= False;
          end;
     end;
end;

procedure TGridAutorizaciones_DevEx.ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     inherited;
     if ( gdFocused in State ) then
     begin
          if ( Column.FieldName = 'AU_FECHA' ) then
          begin
               with zFecha do
               begin
                    Left := Rect.Left + ZetaDBGrid.Left;
                    Top := Rect.Top + ZetaDBGrid.Top;
                    Width := Column.Width + 2;
                    Visible := True;
               end;
          end;
     end;
end;

procedure TGridAutorizaciones_DevEx.OKClick(Sender: TObject);
begin
     dmAsistencia.OperacionConflicto := cbOperacion.ItemIndex;
     inherited;
end;


procedure TGridAutorizaciones_DevEx.ShowColumnasAutorizaciones( const lVisible: boolean );
const
     K_COLUMNA_APROBADAS = 8;
     K_COLUMNA_APROBADAS_POR = 9;
begin
     with ZetaDBGrid do
     begin
          //Se Checa el Global para ver si se tiene prendido
          Columns[ K_COLUMNA_APROBADAS ].Visible := lVisible;
          Columns[ K_COLUMNA_APROBADAS_POR ].Visible := lVisible;

          //Se checa el Derecho de Acceso para Aprobar Autorizaciones
          Columns[ K_COLUMNA_APROBADAS ].ReadOnly := not dmAsistencia.TieneDerechoAprobar;
     end;
end;

procedure TGridAutorizaciones_DevEx.Llenacombo( const lGlobal: boolean );
begin
     with cbOperacion.Items do
     begin
          BeginUpdate;
          try
             Clear;
             Add( 'Reportar Error' );
             Add( 'Dejar Anteriores' );
             if ( not lGlobal ) or ( dmAsistencia.TieneDerechoAprobar ) then
             begin
                  Add( 'Sumar Horas' );
                  Add( 'Sustituir Horas' );
             end;
          finally
                 EndUpdate;
          end;
     end;
end;
{
function TGridAutorizaciones.GetFechaTarjeta: TDate;
begin
     if ( DataSource.DataSet <> NIL ) then
        Result := DataSource.DataSet.FieldByName('AU_FECHA').AsDateTime
     else
         Result := NullDateTime;
end;

function TGridAutorizaciones.PuedeBorrar: Boolean;
begin
     Result := inherited PuedeBorrar;
     if Result then
     begin
          Result := dmCliente.PuedeCambiarTarjetaDlg( GetFechaTarjeta );
     end;
end;

function TGridAutorizaciones.PuedeModificar: Boolean;
begin
     Result := inherited PuedeModificar;
     if Result then
     begin
          Result := dmCliente.PuedeCambiarTarjetaDlg( GetFechaTarjeta );
     end;
end;
 }
procedure TGridAutorizaciones_DevEx.OK_DevExClick(Sender: TObject);
begin
    dmAsistencia.OperacionConflicto := cbOperacion.ItemIndex;
    inherited;

end;

end.
