unit FCalendarioEmpleado_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseCalendarioRitmo_DevEx, ComCtrls, StdCtrls, Spin, ZetaKeyCombo, Buttons,
  ExtCtrls, Grids, ZetaClientDataSet, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  dxSkinsCore, TressMorado2013, Menus, cxTextEdit,
  cxButtons, cxGroupBox;

type
  TCalendarioEmpleado_DevEx = class(TBaseCalendarioRitmo_DevEx)
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FDataset: TZetaClientDataset;
  protected
    procedure ResetCalendario; override;
    procedure ResetFecha( wDay: Word ); override;
    procedure SetInfoCeldaFecha( const dFecha: TDate; const sDay: String; const iCol, iRow: Integer ); override;
  public
    { Public declarations }
    property Dataset: TZetaClientDataset read FDataset write FDataset;
  end;

var
  CalendarioEmpleado_DevEx: TCalendarioEmpleado_DevEx;

procedure ShowCalendarioEmpleado( const iEmpleado: Integer; const dFecha: TDate; ZetaDataset: TZetaClientDataSet );

implementation

uses dCliente,
{$ifdef SUPERVISORES}
     dSuper,
{$else}
     dAsistencia,
{$endif}
     dCatalogos,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists;

{$R *.DFM}

procedure ShowCalendarioEmpleado( const iEmpleado: Integer; const dFecha: TDate;
                                  ZetaDataset: TZetaClientDataSet );
begin
     CalendarioEmpleado_DevEx := TCalendarioEmpleado_DevEx.Create(Application);
     try
        with CalendarioEmpleado_DevEx do
        begin
             Dataset := ZetaDataset;
             Fecha := dFecha;
             Init;
             ShowModal;
        end;
     finally
            FreeAndNil( CalendarioEmpleado_DevEx );
     end;
end;

{ *********** TCalendarioEmpleado ************ }

procedure TCalendarioEmpleado_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_Calendario_del_empleado;
end;

procedure TCalendarioEmpleado_DevEx.FormShow(Sender: TObject);
var
   DescEmp:String;
const
     K_MAX_DESCEMP = 65;
begin
     inherited;

     DescEmp := dmCliente.GetEmpleadoDescripcion;
     //Si el texto total es mayor a 65 caracteres
     if Length ( DescEmp ) > K_MAX_DESCEMP then
     begin
          //Cortar el nombre del empleado
          DescEmp := Copy( dmCliente.GetEmpleadoDescripcion,0,K_MAX_DESCEMP );
          DescEmp := DescEmp  + '...';
     end;
     PanelSuperior.Caption := DescEmp;
end;

procedure TCalendarioEmpleado_DevEx.ResetCalendario;
begin
{$ifdef SUPERVISORES}
     dmSuper.ResetCalendarioEmpleadoActivo( FirstDayOfMonth( Fecha ), LastDayOfMonth( Fecha ) );
{$else}
     dmAsistencia.ResetCalendarioEmpleadoActivo( FirstDayOfMonth( Fecha ), LastDayOfMonth( Fecha ) );
{$endif}
     inherited;
end;

procedure TCalendarioEmpleado_DevEx.SetInfoCeldaFecha(const dFecha: TDate; const sDay: String;
          const iCol, iRow: Integer);
const
     K_STATUS_NINGUNO = -1;
var
   sHorario : String;
   iStatus : Integer;
begin
     with FDataSet do
     begin
          if Locate( 'AU_FECHA', dFecha, [] ) then
          begin
               iStatus := FieldByName( 'AU_STATUS' ).AsInteger; // Si no est� activo viene K_STATUS_NINGUNO
               sHorario := FieldByName( 'HO_CODIGO' ).AsString; // Si no est� activo Viene VACIO del servidor
          end
          else // Si no viene el registro se reporta como Status Ninguno
          begin
               iStatus := K_STATUS_NINGUNO;
               sHorario := ZetaCommonClasses.VACIO;
          end;
     end;
     with StringGrid do
     begin
          Cells[ iCol, iRow ] := PadL( sDay, LONGITUD_DIA ) + sHorario;
          Objects[ iCol, iRow ] := TObject( iStatus );
     end;
end;

procedure TCalendarioEmpleado_DevEx.ResetFecha(wDay: Word);
var
   StatusEmp : eStatusEmpleado;
   sDescripTurno: String;
begin
     inherited;
     with FDataSet do
     begin
          if Locate( 'AU_FECHA', Fecha, [] ) then
          begin
               StatusEmp := eStatusEmpleado( FieldByName( 'STATUS_ACT' ).AsInteger );
               if ( StatusEmp = steEmpleado ) then
                  sDescripTurno := Format( '%s: %s', [ FieldByName( 'CB_TURNO' ).AsString,
                                   dmCatalogos.cdsTurnos.GetDescripcion( FieldByName( 'CB_TURNO' ).AsString ) ] )

               else
                   sDescripTurno := ZetaCommonLists.ObtieneElemento( lfStatusEmpleado, Ord( StatusEmp ) );
          end
          else
              sDescripTurno := ZetaCommonClasses.VACIO;
     end;
     StatusBar.Panels[ 3 ].Text := sDescripTurno;
end;

end.
