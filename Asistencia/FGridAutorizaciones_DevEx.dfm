inherited GridAutorizaciones_DevEx: TGridAutorizaciones_DevEx
  Left = 368
  Top = 259
  Caption = 'Autorizaciones de Extras y Permisos'
  ClientHeight = 313
  ClientWidth = 749
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 277
    Width = 749
    inherited OK_DevEx: TcxButton
      Left = 583
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 663
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 749
    inherited ValorActivo2: TPanel
      Width = 423
      inherited textoValorActivo2: TLabel
        Width = 417
      end
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 749
    Height = 227
    OnColExit = ZetaDBGridColExit
    Columns = <
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Caption = 'N'#250'mero'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRETTYNAME'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ReadOnly = True
        Title.Caption = 'Nombre Completo'
        Width = 192
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'AU_FECHA'
        Title.Caption = 'Fecha'
        Width = 100
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'HORAS'
        Title.Caption = 'Horas'
        Width = 35
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CH_TIPO'
        Title.Caption = 'Tipo'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CH_RELOJ'
        Title.Caption = 'Motivo'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MOTIVOAUTO'
        ReadOnly = True
        Title.Caption = 'Descripci'#243'n'
        Width = 85
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_DESCRIP'
        ReadOnly = True
        Title.Caption = 'Capturado Por'
        Width = 110
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CH_HOR_DES'
        Title.Caption = 'Aprobadas'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_DES_OK'
        ReadOnly = True
        Title.Caption = 'Aprobadas Por'
        Width = 110
        Visible = True
      end>
  end
  object ZFecha: TZetaDBFecha [4]
    Left = 272
    Top = 152
    Width = 105
    Height = 22
    Cursor = crArrow
    TabStop = False
    TabOrder = 4
    Text = '14/oct/99'
    Valor = 36447.000000000000000000
    Visible = False
    DataField = 'AU_FECHA'
    DataSource = DataSource
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited BarSuperior: TdxBar
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarControlContainerItem_DBNavigator'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_AgregarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_BorrarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_ModificarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_BuscarBtn'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton_ImprimirBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_ImprimirFormaBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_ExportarBtn'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton_CortarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_CopiarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_PegarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_UndoBtn'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 125
          Visible = True
          ItemName = 'cbOperacion'
        end>
    end
    inherited dxBarButton_CortarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_CopiarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_PegarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_UndoBtn: TdxBarButton
      Visible = ivNever
    end
    object dxBarControlContainerItem1: TdxBarControlContainerItem
      Category = 0
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'Si ya existen:'
      Category = 0
      Hint = 'Si ya existen:'
      Visible = ivAlways
      PropertiesClassName = 'TcxLabelProperties'
    end
    object CcbOperacion: TCustomdxBarCombo
      Category = 0
      Visible = ivAlways
    end
    object cxBarEditItem2: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxComboBoxProperties'
    end
    object cbOperacion: TdxBarCombo
      Caption = 'Si ya existen :'
      Category = 0
      Hint = 'Si ya existen :'
      Visible = ivAlways
      ShowCaption = True
      ShowEditor = False
      Items.Strings = (
        'Reportar Error'
        'Dejar Anteriores'
        'Sumar Horas'
        'Sustituir Horas')
      ItemIndex = -1
    end
  end
end
