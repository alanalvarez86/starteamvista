inherited AsisTarjeta_DevEx: TAsisTarjeta_DevEx
  Left = 375
  Top = 258
  Caption = 'Tarjeta Diaria'
  ClientHeight = 328
  ClientWidth = 701
  ExplicitWidth = 701
  ExplicitHeight = 328
  PixelsPerInch = 96
  TextHeight = 13
  object Label23: TLabel [0]
    Left = 398
    Top = 38
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel9'
  end
  object ZetaDBTextBox2: TZetaDBTextBox [1]
    Left = 434
    Top = 36
    Width = 48
    Height = 17
    AutoSize = False
    Caption = 'ZetaDBTextBox2'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'CB_NIVEL9'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  inherited PanelIdentifica: TPanel
    Width = 701
    ExplicitWidth = 701
    inherited Slider: TSplitter
      Left = 327
      ExplicitLeft = 327
    end
    inherited ValorActivo1: TPanel
      Width = 311
      ExplicitWidth = 311
      inherited textoValorActivo1: TLabel
        Width = 305
      end
    end
    inherited ValorActivo2: TPanel
      Left = 330
      Width = 371
      ExplicitLeft = 330
      ExplicitWidth = 371
      inherited textoValorActivo2: TLabel
        Width = 365
        ExplicitLeft = 285
      end
    end
  end
  object PageControl1: TcxPageControl [3]
    Left = 0
    Top = 19
    Width = 701
    Height = 158
    Hint = ''
    Align = alTop
    TabOrder = 2
    Properties.ActivePage = TabGenerales
    Properties.CustomButtons.Buttons = <>
    Properties.TabPosition = tpBottom
    ClientRectBottom = 131
    ClientRectLeft = 2
    ClientRectRight = 699
    ClientRectTop = 2
    object TabGenerales: TcxTabSheet
      Caption = 'Tarjeta'
      object Label1: TLabel
        Left = 24
        Top = 5
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha:'
      end
      object Label3: TLabel
        Left = 20
        Top = 79
        Width = 37
        Height = 13
        Caption = 'Horario:'
      end
      object HO_DESCRIP: TZetaDBTextBox
        Left = 140
        Top = 77
        Width = 191
        Height = 17
        AutoSize = False
        Caption = 'HO_DESCRIP'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'HO_DESCRIP'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object LblDia: TLabel
        Left = 36
        Top = 25
        Width = 21
        Height = 13
        Alignment = taRightJustify
        Caption = 'D�a:'
      end
      object Label4: TLabel
        Left = 33
        Top = 43
        Width = 24
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo:'
      end
      object Label11: TLabel
        Left = 5
        Top = 61
        Width = 52
        Height = 13
        Alignment = taRightJustify
        Caption = 'Incidencia:'
      end
      object AU_TIPO: TZetaDBTextBox
        Left = 61
        Top = 59
        Width = 75
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'AU_TIPO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AU_TIPO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object IN_ELEMENT: TZetaDBTextBox
        Left = 140
        Top = 59
        Width = 191
        Height = 17
        AutoSize = False
        Caption = 'IN_ELEMENT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'IN_ELEMENT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object HO_CODIGO: TZetaDBTextBox
        Left = 61
        Top = 77
        Width = 75
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'HO_CODIGO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'HO_CODIGO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object LblUsuario: TLabel
        Left = 374
        Top = 38
        Width = 43
        Height = 13
        Caption = 'Modific�:'
      end
      object US_CODIGO: TZetaDBTextBox
        Left = 420
        Top = 36
        Width = 169
        Height = 17
        AutoSize = False
        Caption = 'US_CODIGO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_CODIGO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label19: TLabel
        Left = 378
        Top = 2
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'N�mina:'
      end
      object Label21: TLabel
        Left = 374
        Top = 20
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Posici�n:'
      end
      object AU_POSICIO: TZetaDBTextBox
        Left = 420
        Top = 18
        Width = 60
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'AU_POSICIO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AU_POSICIO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label18: TLabel
        Left = 342
        Top = 55
        Width = 75
        Height = 13
        Alignment = taRightJustify
        Caption = 'Horario Manual:'
      end
      object AU_STATUS: TZetaDBTextBox
        Left = 61
        Top = 23
        Width = 186
        Height = 17
        AutoSize = False
        Caption = 'AU_STATUS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AU_STATUS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object AU_TIPODIA: TZetaDBTextBox
        Left = 61
        Top = 41
        Width = 186
        Height = 17
        AutoSize = False
        Caption = 'AU_TIPODIA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AU_TIPODIA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object AU_DIA_SEM: TZetaDBTextBox
        Left = 174
        Top = 4
        Width = 73
        Height = 17
        AutoSize = False
        Caption = 'AU_DIA_SEM'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AU_DIA_SEM'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object AU_PERIODO: TZetaDBTextBox
        Left = 420
        Top = 0
        Width = 169
        Height = 17
        AutoSize = False
        Caption = 'AU_PERIODO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AU_PERIODO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object lblTomoComida: TLabel
        Left = 349
        Top = 90
        Width = 68
        Height = 13
        Caption = 'Tom� Comida:'
      end
      object AU_OUT2EAT: TZetaDBTextBox
        Left = 420
        Top = 88
        Width = 169
        Height = 17
        AutoSize = False
        Caption = 'AU_OUT2EAT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AU_OUT2EAT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object AU_FECHA: TZetaDBTextBox
        Left = 287
        Top = 8
        Width = 75
        Height = 17
        AutoSize = False
        Caption = 'AU_FECHA'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AU_FECHA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label20: TLabel
        Left = 380
        Top = 72
        Width = 37
        Height = 13
        Caption = 'Festivo:'
      end
      object AU_STA_FES: TZetaDBTextBox
        Left = 420
        Top = 70
        Width = 169
        Height = 17
        AutoSize = False
        Caption = 'AU_STA_FES'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AU_STA_FES'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object DBCheckBox1: TDBCheckBox
        Left = 420
        Top = 53
        Width = 28
        Height = 17
        DataField = 'AU_HOR_MAN'
        DataSource = DataSource
        Enabled = False
        TabOrder = 2
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object Verificar: TcxButton
        Left = 252
        Top = 4
        Width = 25
        Height = 25
        Hint = 'Verifica Status del Empleado'
        OptionsImage.Glyph.Data = {
          7A080000424D7A08000000000000360000002800000017000000170000000100
          2000000000004408000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFAF4EFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAF4
          EFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDDB58FFFFFFFFFFFFAF4EFFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB
          81FFE5C6A9FFD8AA7FFFD8AA7FFFDDB58FFFFFFFFFFFFAF4EFFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFECD5BFFFFFFF
          FFFFF3E6D9FFDDB58FFFDDB58FFFFFFFFFFFFAF4EFFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFFFF
          FFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFDFBA97FFFEFEFDFFFEFCFBFFFFFF
          FFFFFDFBF9FFE9CEB5FFE4C3A5FFF6EBE1FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFFFFFFFFFFFF
          FFFFD8AA7FFFD8AA7FFFD9AB81FFF7EEE5FFFFFFFFFFE2BF9FFFE5C6A9FFFCF8
          F5FFFFFFFFFFF6ECE3FFDFBA97FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFFFFFFFFFFFFFFFFD8AA
          7FFFD8AA7FFFEAD1B9FFFFFFFFFFF0DECDFFD8AA7FFFD8AA7FFFDBB189FFF1E0
          D1FFFFFFFFFFFEFCFBFFECD5BFFFD9AB81FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA
          7FFFE7CAAFFFF9F2EBFFDAAF87FFD8AA7FFFD8AA7FFFD8AA7FFFD9AB81FFE4C3
          A5FFFBF6F1FFFFFFFFFFF8EFE7FFE1BD9BFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
          7FFFD9AB81FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDDB58FFFF5EADFFFE1BD
          9BFFEFDCCBFFFFFFFFFFFEFEFDFFEDD8C5FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDDB58FFFFFFFFFFFF9F3EDFFD8AA
          7FFFE2BF9FFFFAF4EFFFF0DFCFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAF4EFFFD8AA7FFFD8AA
          7FFFDAAE85FFDAAF87FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFFAF4EFFFFAF4EFFFFAF4EFFFFAF4EFFFFAF4EFFFFAF4EFFFFAF4EFFFFAF4
          EFFFFAF4EFFFFAF4EFFFFAF4EFFFFAF4EFFFF6EBE1FFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = VerificarClick
      end
      object ZFecha: TZetaFecha
        Left = 61
        Top = 0
        Width = 110
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '20/feb/01'
        UseEnterKey = True
        Valor = 36942.000000000000000000
        OnValidDate = ZFechaValidDate
      end
      object BtnCalendario: TcxButton
        Left = 252
        Top = 32
        Width = 25
        Height = 25
        Hint = 'Mostrar Calendario del Empleado'
        OptionsImage.Glyph.Data = {
          7A080000424D7A08000000000000360000002800000017000000170000000100
          20000000000044080000000000000000000000000000000000008B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFC5C0C6FFFDFD
          FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD2CED2FF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFF0EFF1FFFFFFFFFFE6E3
          E6FFE2DFE2FFE2DFE2FFF8F7F8FFE2DFE2FFE2DFE2FFE2DFE2FFF8F7F8FFE2DF
          E2FFE2DFE2FFE2DFE2FFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFF0EFF1FFFFFFFFFF9A919AFF8B81
          8CFF8B818CFFE2DFE2FF8B818CFF8B818CFF8B818CFFE2DFE2FF8B818CFF8B81
          8CFF8B818CFFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFF0EFF1FFFFFFFFFF9A919AFF8B818CFF8B81
          8CFFE2DFE2FF8B818CFF8B818CFF8B818CFFE2DFE2FF8B818CFF8B818CFF8B81
          8CFFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFF0EFF1FFFFFFFFFFE6E3E6FFE2DFE2FFE2DFE2FFF8F7
          F8FFE2DFE2FFE2DFE2FFE2DFE2FFF8F7F8FFE2DFE2FFE2DFE2FFE2DFE2FFFFFF
          FFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFF0EFF1FFFFFFFFFF9A919AFF8B818CFF8B818CFFE2DFE2FF8B81
          8CFF8B818CFF8B818CFFE2DFE2FF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFF
          FFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFF0EFF1FFFFFFFFFF9A919AFF8B818CFF8B818CFFE2DFE2FF8B818CFF8B81
          8CFF8B818CFFE2DFE2FF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFFFFFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFF0EF
          F1FFFFFFFFFFE6E3E6FFE2DFE2FFE2DFE2FFF8F7F8FFE2DFE2FFE2DFE2FFE2DF
          E2FFF8F7F8FFE2DFE2FFE2DFE2FFE2DFE2FFFFFFFFFFFFFFFFFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFF0EFF1FFFFFF
          FFFF9A919AFF8B818CFF8B818CFFE2DFE2FF8B818CFF8B818CFF8B818CFFE2DF
          E2FF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFF0EFF1FFFFFFFFFF9A91
          9AFF8B818CFF8B818CFFE2DFE2FF8B818CFF8B818CFF8B818CFFE2DFE2FF8B81
          8CFF8B818CFF8B818CFFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFF0EFF1FFFFFFFFFFF2F1F2FFF0EF
          F1FFF0EFF1FFFBFBFBFFF0EFF1FFF0EFF1FFF0EFF1FFFBFBFBFFF0EFF1FFF0EF
          F1FFF0EFF1FFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFF0EFF1FFFFFFFFFFEFEDEFFFD3D0D4FFD3D0
          D4FFD9D5D9FFFFFFFFFFFFFFFFFFFFFFFFFFD9D5D9FFD3D0D4FFD3D0D4FFE9E7
          E9FFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFF0EFF1FFFFFFFFFFD3D0D4FFA8A1A9FFC5C0C6FFA199
          A2FFFFFFFFFFFFFFFFFFFFFFFFFF9A919AFFBEB8BFFFAFA9B0FFC5C0C6FFFFFF
          FFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFC5C0C6FFFDFDFDFFD3D0D4FFC5C0C6FFFFFFFFFFA8A1A9FFFFFF
          FFFFFFFFFFFFFFFFFFFF9A919AFFF0EFF1FFD3D0D4FFC5C0C6FFFFFFFFFFD2CE
          D2FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF948B95FF948B95FFA8A1A9FFE7E5E8FF908791FF9A919AFF9A91
          9AFF9A919AFF8D838EFFD7D4D7FFBCB6BDFF928993FF968D97FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtnCalendarioClick
      end
    end
    object TabHoras: TcxTabSheet
      Caption = 'Horas'
      object GroupBox1: TcxGroupBox
        Left = 0
        Top = 0
        Hint = ''
        Align = alLeft
        Caption = ' Horas '
        TabOrder = 0
        Height = 129
        Width = 339
        object Label5: TLabel
          Left = 34
          Top = 14
          Width = 50
          Height = 13
          Alignment = taRightJustify
          Caption = 'Ordinarias:'
        end
        object AU_HORASCK: TZetaDBTextBox
          Left = 87
          Top = 12
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_HORASCK'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_HORASCK'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object AU_TARDES: TZetaDBTextBox
          Left = 87
          Top = 31
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_TARDES'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_TARDES'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label9: TLabel
          Left = 48
          Top = 33
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tardes:'
        end
        object Label6: TLabel
          Left = 21
          Top = 52
          Width = 63
          Height = 13
          Alignment = taRightJustify
          Caption = 'Permiso C/G:'
        end
        object AU_PER_CG: TZetaDBTextBox
          Left = 87
          Top = 50
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_PER_CG'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_PER_CG'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label8: TLabel
          Left = 21
          Top = 70
          Width = 63
          Height = 13
          Alignment = taRightJustify
          Caption = 'Permiso S/G:'
        end
        object AU_PER_SG: TZetaDBTextBox
          Left = 87
          Top = 68
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_PER_SG'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_PER_SG'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label17: TLabel
          Left = 158
          Top = 14
          Width = 104
          Height = 13
          Alignment = taRightJustify
          Caption = 'Descanso Autorizado:'
        end
        object AU_AUT_TRA: TZetaDBTextBox
          Left = 265
          Top = 12
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_AUT_TRA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_AUT_TRA'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label10: TLabel
          Left = 160
          Top = 32
          Width = 102
          Height = 13
          Alignment = taRightJustify
          Caption = 'Descanso Trabajado:'
        end
        object AU_DES_TRA: TZetaDBTextBox
          Left = 265
          Top = 30
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_DES_TRA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_DES_TRA'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
      object GroupBox2: TcxGroupBox
        Left = 339
        Top = 0
        Hint = ''
        Align = alClient
        Caption = ' Horas Extras '
        TabOrder = 1
        Height = 129
        Width = 358
        object Label12: TLabel
          Left = 25
          Top = 14
          Width = 55
          Height = 13
          Alignment = taRightJustify
          Caption = 'Calculadas:'
        end
        object AU_NUM_EXT: TZetaDBTextBox
          Left = 82
          Top = 12
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_NUM_EXT'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_NUM_EXT'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object lbAutorizadas: TLabel
          Left = 22
          Top = 33
          Width = 58
          Height = 13
          Alignment = taRightJustify
          Caption = 'Autorizadas:'
        end
        object AU_AUT_EXT: TZetaDBTextBox
          Left = 82
          Top = 31
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_AUT_EXT'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_AUT_EXT'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label14: TLabel
          Left = 39
          Top = 70
          Width = 41
          Height = 13
          Alignment = taRightJustify
          Caption = 'A Pagar:'
        end
        object AU_EXTRAS: TZetaDBTextBox
          Left = 82
          Top = 68
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_EXTRAS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_EXTRAS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label15: TLabel
          Left = 44
          Top = 89
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Dobles:'
        end
        object AU_DOBLES: TZetaDBTextBox
          Left = 82
          Top = 87
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_DOBLES'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_DOBLES'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label16: TLabel
          Left = 46
          Top = 108
          Width = 34
          Height = 13
          Alignment = taRightJustify
          Caption = 'Triples:'
        end
        object AU_TRIPLES: TZetaDBTextBox
          Left = 82
          Top = 106
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_TRIPLES'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_TRIPLES'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object AU_PRE_EXT: TZetaDBTextBox
          Left = 82
          Top = 50
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_PRE_EXT'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_PRE_EXT'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label13: TLabel
          Left = 20
          Top = 52
          Width = 60
          Height = 13
          Alignment = taRightJustify
          Caption = 'Prepagadas:'
        end
      end
    end
    object TabClasifica: TcxTabSheet
      Caption = 'Clasificaci�n'
      object CB_TURNO: TZetaDBTextBox
        Left = 103
        Top = 9
        Width = 60
        Height = 17
        AutoSize = False
        Caption = 'CB_TURNO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_TURNO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object TU_DESCRIP: TZetaDBTextBox
        Left = 171
        Top = 9
        Width = 200
        Height = 17
        AutoSize = False
        Caption = 'TU_DESCRIP'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'TU_DESCRIP'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object LblTurno: TLabel
        Left = 65
        Top = 11
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Turno:'
      end
      object Label2: TLabel
        Left = 60
        Top = 29
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puesto:'
      end
      object cb_puesto: TZetaDBTextBox
        Left = 103
        Top = 27
        Width = 60
        Height = 17
        AutoSize = False
        Caption = 'cb_puesto'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_PUESTO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object PU_DESCRIP: TZetaDBTextBox
        Left = 171
        Top = 27
        Width = 200
        Height = 17
        AutoSize = False
        Caption = 'PU_DESCRIP'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'PU_DESCRIP'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label7: TLabel
        Left = 34
        Top = 47
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Clasificaci�n:'
      end
      object cb_clasifi: TZetaDBTextBox
        Left = 103
        Top = 45
        Width = 60
        Height = 17
        AutoSize = False
        Caption = 'cb_clasifi'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_CLASIFI'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Clasifi: TZetaDBTextBox
        Left = 171
        Top = 45
        Width = 200
        Height = 17
        AutoSize = False
        Caption = 'Clasifi'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'Clasifi'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label22: TLabel
        Left = 33
        Top = 65
        Width = 63
        Height = 13
        Caption = 'Tipo N�mina:'
      end
      object CB_NOMINA: TZetaDBTextBox
        Left = 103
        Top = 63
        Width = 60
        Height = 17
        AutoSize = False
        Caption = 'CB_NOMINA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NOMINA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_DESCRIP: TZetaDBTextBox
        Left = 171
        Top = 63
        Width = 200
        Height = 17
        AutoSize = False
        Caption = 'NO_DESCRIP'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DESCRIP'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object bbMostrarCalendario: TcxButton
        Left = 375
        Top = 7
        Width = 22
        Height = 22
        Hint = 'Mostrar Calendario del Turno'
        OptionsImage.Glyph.Data = {
          76060000424D7606000000000000360000002800000014000000140000000100
          2000000000004006000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFEEDBC9FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF3E6D9FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFFAF4EFFFFFFFFFFFF0DFCFFFF0DFCFFFF2E3D5FFF9F3EDFFF0DF
          CFFFF0DFCFFFF8EFE7FFF2E3D5FFF0DFCFFFF0DFCFFFFDFBF9FFFFFFFFFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFD8AA
          7FFFD8AA7FFFDDB58FFFF0DFCFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA
          7FFFD8AA7FFFFAF4EFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFFAF4EFFFFFFFFFFFE2BF9FFFE2BF9FFFE5C7ABFFF4E7DBFFE2BF
          9FFFE2BF9FFFF0DFCFFFE5C7ABFFE2BF9FFFE2BF9FFFFBF7F3FFFFFFFFFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFECD5
          BFFFECD5BFFFEEDAC7FFF8EFE7FFECD5BFFFECD5BFFFF5EADFFFEEDAC7FFECD5
          BFFFECD5BFFFFDFAF7FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFFAF4EFFFFFFFFFFFD8AA7FFFD8AA7FFFDDB58FFFF0DFCFFFD8AA
          7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFECD5
          BFFFECD5BFFFEEDAC7FFF8EFE7FFECD5BFFFECD5BFFFF5EADFFFEEDAC7FFECD5
          BFFFECD5BFFFFDFAF7FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFFAF4EFFFFFFFFFFFE2BF9FFFE2BF9FFFE5C7ABFFF4E7DBFFE2BF
          9FFFE2BF9FFFF0DFCFFFE5C7ABFFE2BF9FFFE2BF9FFFFBF7F3FFFFFFFFFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFD8AA
          7FFFD8AA7FFFDDB58FFFF0DFCFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA
          7FFFD8AA7FFFFAF4EFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFFAF4EFFFFFFFFFFFF0DFCFFFF0DFCFFFF2E3D5FFF9F3EDFFF0DF
          CFFFF0DFCFFFF8EFE7FFF2E3D5FFF0DFCFFFF0DFCFFFFDFBF9FFFFFFFFFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFFCF8
          F5FFFAF4EFFFFAF4EFFFFDFAF7FFFFFFFFFFFFFFFFFFFDFBF9FFFAF4EFFFFAF4
          EFFFFCF8F5FFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFFAF4EFFFFFFFFFFFE7CAAFFFE3C2A3FFE3C2A3FFECD5BFFFFFFF
          FFFFFFFFFFFFF0DFCFFFE1BE9DFFE5C6A9FFE7CAAFFFFFFFFFFFFFFFFFFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFEFDCCBFFFFFFFFFFE7CA
          AFFFF5EADFFFF5EADFFFECD5BFFFFFFFFFFFFFFFFFFFF0DFCFFFF0DFCFFFFAF4
          EFFFE7CAAFFFFFFFFFFFF4E7DBFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFDCB38DFFDAAE85FFECD5BFFFEEDBC9FFDAAF87FFDDB5
          8FFFDDB58FFFDBB189FFE9CEB5FFF3E4D7FFDAAE85FFDDB58FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = bbMostrarCalendarioClick
      end
    end
    object TabNiveles: TcxTabSheet
      Caption = 'Niveles'
      object CB_NIVEL1lbl: TLabel
        Left = 149
        Top = 5
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel1:'
      end
      object CB_NIVEL2lbl: TLabel
        Left = 149
        Top = 23
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel2:'
      end
      object CB_NIVEL3lbl: TLabel
        Left = 149
        Top = 41
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel3:'
      end
      object CB_NIVEL4lbl: TLabel
        Left = 149
        Top = 59
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel4:'
      end
      object CB_NIVEL5lbl: TLabel
        Left = 149
        Top = 77
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel5:'
      end
      object CB_NIVEL1: TZetaDBTextBox
        Left = 188
        Top = 3
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL1'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL1'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL2: TZetaDBTextBox
        Left = 188
        Top = 21
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL2'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL2'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL3: TZetaDBTextBox
        Left = 188
        Top = 39
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL3'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL3'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL4: TZetaDBTextBox
        Left = 188
        Top = 57
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL4'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL4'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL5: TZetaDBTextBox
        Left = 188
        Top = 75
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL5'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL5'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL6lbl: TLabel
        Left = 149
        Top = 95
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel6:'
      end
      object CB_NIVEL7lbl: TLabel
        Left = 395
        Top = 4
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel7:'
      end
      object CB_NIVEL8lbl: TLabel
        Left = 395
        Top = 23
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel8:'
      end
      object CB_NIVEL9lbl: TLabel
        Left = 395
        Top = 41
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel9:'
      end
      object CB_NIVEL9: TZetaDBTextBox
        Left = 434
        Top = 39
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL9'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL9'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL8: TZetaDBTextBox
        Left = 434
        Top = 21
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL8'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL8'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL7: TZetaDBTextBox
        Left = 434
        Top = 3
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL7'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL7'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL6: TZetaDBTextBox
        Left = 188
        Top = 93
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL6'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL6'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL10lbl: TLabel
        Left = 389
        Top = 59
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel10:'
        Visible = False
      end
      object CB_NIVEL10: TZetaDBTextBox
        Left = 434
        Top = 57
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL10'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL10'
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL11lbl: TLabel
        Left = 389
        Top = 77
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel11:'
        Visible = False
      end
      object CB_NIVEL11: TZetaDBTextBox
        Left = 434
        Top = 75
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL11'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL11'
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL12lbl: TLabel
        Left = 389
        Top = 95
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel12:'
        Visible = False
      end
      object CB_NIVEL12: TZetaDBTextBox
        Left = 434
        Top = 93
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL12'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL12'
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 177
    Width = 701
    Height = 151
    ExplicitTop = 177
    ExplicitWidth = 701
    ExplicitHeight = 151
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = dsChecadas
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsData.DeletingConfirmation = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object CH_H_REAL: TcxGridDBColumn
        Caption = 'Real'
        DataBinding.FieldName = 'CH_H_REAL'
        Width = 44
      end
      object CH_H_AJUS: TcxGridDBColumn
        Caption = 'Ajustada'
        DataBinding.FieldName = 'CH_H_AJUS'
        Width = 44
      end
      object CH_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'CH_TIPO'
        Width = 122
      end
      object CH_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci�n'
        DataBinding.FieldName = 'CH_DESCRIP'
      end
      object CH_HOR_ORD: TcxGridDBColumn
        Caption = 'Ordinarias'
        DataBinding.FieldName = 'CH_HOR_ORD'
        Width = 53
      end
      object CH_HOR_EXT: TcxGridDBColumn
        Caption = 'Extras'
        DataBinding.FieldName = 'CH_HOR_EXT'
        Width = 40
      end
      object CH_HOR_DES: TcxGridDBColumn
        Caption = 'Descanso'
        DataBinding.FieldName = 'CH_HOR_DES'
        Width = 58
      end
      object CH_APRUEBA: TcxGridDBColumn
        Caption = 'Aprobadas'
        DataBinding.FieldName = 'CH_APRUEBA'
        Width = 58
      end
      object US_CODIGO_GRID: TcxGridDBColumn
        Caption = 'Modific�'
        DataBinding.FieldName = 'US_CODIGO'
        Width = 45
      end
      object CH_MOTIVO: TcxGridDBColumn
        Caption = 'Motivo'
        DataBinding.FieldName = 'CH_MOTIVO'
        Width = 45
      end
      object CH_SISTEMA: TcxGridDBColumn
        Caption = 'Sist'
        DataBinding.FieldName = 'CH_SISTEMA'
        Width = 30
      end
      object CH_GLOBAL: TcxGridDBColumn
        Caption = 'Auto'
        DataBinding.FieldName = 'CH_GLOBAL'
        Width = 30
      end
      object CH_RELOJ: TcxGridDBColumn
        Caption = 'Reloj'
        DataBinding.FieldName = 'CH_RELOJ'
        Width = 70
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 528
    Top = 128
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
    ImageInfo = <
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000001A140F7F5A4634BF3E3024A93E3024A95744
          32BD221B148B0000000000000000000000000000000000000000000000000000
          0000000000000000000000000012BF966FF5D8AA7DFFD8AA7DFFD8AA7DFFD8AA
          7DFFC09871F60000001300000000000000000000000000000000000000000000
          0000000000000000000000000013C09771F6D8AA7DFFD8AA7DFFD8AA7DFFD8AA
          7DFFC0966FF50000001200000000000000000000000000000000000000000000
          0000000000040000000000000015C59968F3D8AA7DFFD8AA7DFFD8AA7DFFD8AA
          7DFFC49869F300000012000000000000000D0000000000000000000000000505
          0659847992FA7474A4FF110E0E77785B3CCCD7AA7DFFD8AA7DFFD8AA7DFFD9AA
          7DFF7F6243D20303034B706F9AFA8D829BFF0605075C0000000000000000201E
          249E8B819CFF7776A3FF1411107D725637C7DCAD7BFFDDAD7BFFDDAD7BFFDEAE
          7BFF7B5F3FCF110F107B0C0C10781614188B2E2B34B200000000000000001E1B
          219A8B819CFF7D79A1FF0E0C0C720100002F18120D7B110D096E110D096E1712
          0D7A020101380705055A413F55CF5A5466DE232128A300000000000000001D1B
          21998B819CFF8C819CFF655F78E9100F127D0F0E107A1210138012101380100F
          117C0D0C0E75585261DB8A819CFF8B819CFF1D1B219900000000000000001D1B
          21998B819CFF8C819CFF8A819CFF847B9FFF847B9FFF847B9FFF847B9FFF847B
          9FFF837B9FFF89809DFF8C829CFF8B819CFF1D1B21990000000000000000201E
          249E8B819CFF8B819CFF8B819CFF8B819CFF8B819CFF8B819CFF8B819CFF8B81
          9CFF8B819CFF8B819CFF8B819CFF8B819CFF201E249E00000000000000001817
          1C918B819CFF8B819CFF8B819CFF8B819CFF8B819CFF8B819CFF8B819CFF8B81
          9CFF8B819CFF8B819CFF8B819CFF8B819CFF19181D9300000000000000000000
          002819181D9325232AA61110138001010136010101380101013B0101013B0000
          00300303034C1B191E95201E249E1B191E950000002B00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000348D62DF4ED293FF4ED293FF4ED2
          93FF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000348D62DF4ED293FF4ED293FF4ED2
          93FF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000348D62DF4ED293FF4ED293FF4ED2
          93FF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000348D62DF4ED293FF4ED293FF4ED2
          93FF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000348D62DF4ED293FF4ED293FF4ED2
          93FF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000348D62DF4ED293FF4ED293FF4ED2
          93FF0000000000000000000000000000000000000000000000004ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF348D62DF348D
          62DF348D62DF348D62DF348D62DF348D62DF4BC88DFB4ED293FF4ED293FF4ED2
          93FF348D62DF348D62DF348D62DF348D62DF348D62DF348D62DF000000000000
          000000000000000000000000000000000000348D62DF4ED293FF4ED293FF4ED2
          93FF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000348D62DF4ED293FF4ED293FF4ED2
          93FF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000348D62DF4ED293FF4ED293FF4ED2
          93FF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000348D62DF4ED293FF4ED293FF4ED2
          93FF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000348D62DF4ED293FF4ED293FF4ED2
          93FF000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000348D62DF4ED293FF4ED293FF4ED2
          93FF000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000607107015193DAB00000004000000000000000000000000000000000000
          0000000000000000000415193DAB060710700000000000000000000000000607
          10704656CCFF4656CCFF15193DAB000000040000000000000000000000000000
          00000000000415193DAB4656CCFF4656CCFF0607107000000000060710704656
          CCFF4656CCFF4656CCFF4656CCFF15193DAB0000000400000000000000000000
          000415193DAB4656CCFF4656CCFF4656CCFF4656CCFF060710700C0F238F4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF15193DAB00000004000000041519
          3DAB4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF0C0F238F000000000C0F
          238F4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF15193DAB15193DAB4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF0C0F238F00000000000000000000
          00000C0F238F4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF0C0F238F0000000000000000000000000000
          0000000000000C0F238F4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF0C0F238F000000000000000000000000000000000000
          000000000000000000000C0F238F4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF0C0F238F00000000000000000000000000000000000000000000
          0000000000000000000415193DAB4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF15193DAB00000004000000000000000000000000000000000000
          00000000000415193DAB4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF15193DAB000000040000000000000000000000000000
          000415193DAB4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF15193DAB0000000400000000000000041519
          3DAB4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF0C0F238F0C0F238F4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF15193DAB00000004131839A74656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF0C0F238F00000000000000000C0F
          238F4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF131839A7020206544353
          C3FB4656CCFF4656CCFF4656CCFF0C0F238F0000000000000000000000000000
          00000C0F238F4656CCFF4656CCFF4656CCFF4353C3FB02020654000000000202
          06544353C3FB4656CCFF0C0F238F000000000000000000000000000000000000
          0000000000000C0F238F4656CCFF4353C3FB0202065400000000000000000000
          0000020206540B0E208B00000000000000000000000000000000000000000000
          000000000000000000000B0E208B020206540000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          000000080A5C00384BAF0002023C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000090C6000B2E9FF00B2E9FF005975CB00080A5C00000004000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000011177800B2E9FF00B2E9FF0083ACE7000E137000000008000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000161D800092BEEF00161D8000000010000000000000000C000102380000
          0000000000000000000000000000000000000000000000000000000000000000
          00000001023800000018000000000000000000242F97007DA4E300536FC70000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000080A5C004962BF00A8DCFB00B2E9FF00B2E9FF0008
          0A5C000000000000000000000000000000000000000000000000000000000000
          00000000000000161D80008AB6EB00B2E9FF00B2E9FF00B2E9FF00B2E9FF0083
          ACE70000000C0000000000000000000000000000000000000000000000000000
          0000000000000000002000A0D4F700B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00181F830000000000000000000000000000000000000000000000000000
          0000000000000000000000181F8300B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00A0D4F70000002400000000000000000000000000000000000000000000
          000000000000000000000000000C0083ACE700B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF003446AB00000000000000000000000000000000000000000000
          000000000000000000000000000000090C6000B2E9FF00B2E9FF00B2E9FF00B2
          E9FF004E67C30001013400000000000000000000000000000000000000000000
          000000000000000000000000000000000000005975CB00B2E9FF00779BDF0006
          09580000000000000000000000100000000C0000000000000000000000000000
          0000000000000000000000000000000000000002023C00131A7C000000080000
          000000000004000E13700092BEEF001015740000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000005
          0650007093DB00B2E9FF00B2E9FF000B0F680000000000000000000000000000
          0000000000000000000000000000000000000000000000000004004155B700B2
          E9FF00B2E9FF005975CB0002023C000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000090C600059
          75CB00080A5C0000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end>
  end
  object dsChecadas: TDataSource
    Left = 496
    Top = 124
  end
end
