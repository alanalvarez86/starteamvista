unit FHorarioTemp_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Mask, Buttons, ExtCtrls,
     ZetaFecha, ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, ImgList, cxButtons;

type
  THorarioTemp_DevEx = class(TZetaDlgModal_DevEx)
    Label1: TLabel;
    FechaInicial: TZetaFecha;
    Label2: TLabel;
    FechaFinal: TZetaFecha;
    ValorActivo1: TPanel;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function GetFechasHorarioTemporal( var dInicial, dFinal: TDate ): Boolean;

var
  HorarioTemp_DevEx: THorarioTemp_DevEx;

implementation

uses DCliente,
     ZetaDialogo,
     ZetaCommonClasses;

{$R *.DFM}

function GetFechasHorarioTemporal( var dInicial, dFinal: TDate ): Boolean;
begin
     with THorarioTemp_DevEx.Create( Application ) do
     begin
          try
             if ( dInicial = NullDateTime ) then
             begin
              {$ifndef CAJAAHORRO}
                  dInicial := dmCliente.FechaAsistencia;
              {$endif}
                  dFinal := dInicial;
             end;
             FechaInicial.Valor := dInicial;
             FechaFinal.Valor := dFinal;
             ShowModal;
             Result := ( ModalResult = mrOk );
             if Result then
             begin
                  dInicial := FechaInicial.Valor;
                  dFinal := FechaFinal.Valor;
             end;
          finally
                 Free;
          end;
     end;
end;

{ ************* THorarioTemp ************ }

procedure THorarioTemp_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     ValorActivo1.Caption := dmCliente.GetEmpleadoDescripcion;
     ActiveControl := FechaInicial;
end;

procedure THorarioTemp_DevEx.OKClick(Sender: TObject);
begin
     inherited;
     if ( FechaInicial.Valor <= FechaFinal.Valor ) then
     begin
          if dmCliente.PuedeCambiarTarjetaDlg(FechaInicial.Valor, FechaFinal.Valor) then
             ModalResult := mrOk
     end
     else
     begin
          ZetaDialogo.zError( '� Error En Rango De Fechas !', 'La Fecha Final Debe Ser Mayor o Igual A La Fecha Inicial', 0 );
          FechaFinal.SetFocus;
     end;
end;

procedure THorarioTemp_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H20222_Horarios_Temporales;
end;

procedure THorarioTemp_DevEx.OK_DevExClick(Sender: TObject);
begin
   inherited;
     if ( FechaInicial.Valor <= FechaFinal.Valor ) then
     begin
          if dmCliente.PuedeCambiarTarjetaDlg(FechaInicial.Valor, FechaFinal.Valor) then
             ModalResult := mrOk
     end
     else
     begin
          ZetaDialogo.zError( '� Error En Rango De Fechas !', 'La Fecha Final Debe Ser Mayor o Igual A La Fecha Inicial', 0 );
          FechaFinal.SetFocus;
     end;

end;

end.
