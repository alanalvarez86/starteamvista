inherited EditAutorizaciones_DevEx: TEditAutorizaciones_DevEx
  Left = 345
  Top = 167
  Caption = 'Autorizaci'#243'n de Horas Extras y Permisos'
  ClientHeight = 251
  ClientWidth = 424
  PixelsPerInch = 96
  TextHeight = 13
  object TipoAutLbl: TLabel [0]
    Left = 41
    Top = 87
    Width = 61
    Height = 13
    Alignment = taRightJustify
    Caption = '&Autorizaci'#243'n:'
    FocusControl = CH_TIPO
  end
  object HorasLbl: TLabel [1]
    Left = 71
    Top = 112
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = '&Horas:'
    FocusControl = HORAS
  end
  object Label1: TLabel [2]
    Left = 152
    Top = 112
    Width = 6
    Height = 13
    Caption = '='
  end
  object Label2: TLabel [3]
    Left = 212
    Top = 112
    Width = 37
    Height = 13
    Caption = 'Minutos'
  end
  object MotivoLbl: TLabel [4]
    Left = 67
    Top = 138
    Width = 35
    Height = 13
    Alignment = taRightJustify
    Caption = '&Motivo:'
    FocusControl = MotivoAutorizacion
  end
  object EmpleadoLbl: TLabel [5]
    Left = 52
    Top = 61
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = '&Empleado:'
    FocusControl = CB_CODIGO
  end
  object Label3: TLabel [6]
    Left = 63
    Top = 163
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = '&Usuario:'
    FocusControl = MotivoAutorizacion
  end
  object US_DESCRIP: TZetaDBTextBox [7]
    Left = 104
    Top = 159
    Width = 140
    Height = 21
    AutoSize = False
    Caption = 'US_DESCRIP'
    ShowAccelChar = False
    Layout = tlCenter
    Brush.Color = clBtnFace
    Border = True
    DataField = 'US_DESCRIP'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object HorasAprobadaslbl: TLabel [8]
    Left = 17
    Top = 189
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ho&ras Aprobadas:'
    FocusControl = MotivoAutorizacion
  end
  object AprobadasPorlbl: TLabel [9]
    Left = 148
    Top = 189
    Width = 18
    Height = 13
    Alignment = taRightJustify
    Caption = 'por:'
    FocusControl = MotivoAutorizacion
  end
  object US_DES_OK: TZetaDBTextBox [10]
    Left = 168
    Top = 185
    Width = 140
    Height = 21
    AutoSize = False
    Caption = 'US_DES_OK'
    ShowAccelChar = False
    Layout = tlCenter
    Brush.Color = clBtnFace
    Border = True
    DataField = 'US_DES_OK'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  inherited PanelBotones: TPanel
    Top = 215
    Width = 424
    TabOrder = 8
    inherited OK_DevEx: TcxButton
      Left = 260
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 339
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 424
    TabOrder = 0
    inherited Splitter: TSplitter
      Left = 273
    end
    inherited ValorActivo1: TPanel
      Width = 273
      inherited textoValorActivo1: TLabel
        Width = 267
      end
    end
    inherited ValorActivo2: TPanel
      Left = 276
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 12
  end
  object HorasMin: TZetaNumero [14]
    Left = 167
    Top = 108
    Width = 40
    Height = 21
    Mascara = mnEmpleado
    MaxLength = 4
    TabOrder = 4
    OnExit = HorasMinExit
  end
  object CB_CODIGO: TZetaDBKeyLookup_DevEx [15]
    Left = 104
    Top = 57
    Width = 300
    Height = 21
    LookupDataset = dmCliente.cdsEmpleadoLookUp
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 1
    TabStop = True
    WidthLlave = 80
    DataField = 'CB_CODIGO'
    DataSource = DataSource
  end
  object MotivoAutorizacion: TZetaDBKeyLookup_DevEx [16]
    Left = 104
    Top = 134
    Width = 300
    Height = 21
    LookupDataset = dmTablas.cdsMotAuto
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 5
    TabStop = True
    WidthLlave = 60
    DataField = 'CH_RELOJ'
    DataSource = DataSource
  end
  object CH_TIPO: TZetaDBKeyCombo [17]
    Left = 104
    Top = 83
    Width = 145
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 2
    OnChange = CH_TIPOChange
    ListaFija = lfAutorizaChecadas
    ListaVariable = lvPuesto
    Offset = 5
    Opcional = False
    EsconderVacios = False
    DataField = 'CH_TIPO'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object HORAS: TZetaDBNumero [18]
    Left = 104
    Top = 108
    Width = 41
    Height = 21
    Mascara = mnHoras
    TabOrder = 3
    Text = '0.00'
    OnChange = HorasChange
    DataField = 'HORAS'
    DataSource = DataSource
  end
  object CH_HOR_DES: TZetaDBNumero [19]
    Left = 104
    Top = 185
    Width = 41
    Height = 21
    Mascara = mnHoras
    TabOrder = 6
    Text = '0.00'
    DataField = 'CH_HOR_DES'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 12
    Top = 113
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
