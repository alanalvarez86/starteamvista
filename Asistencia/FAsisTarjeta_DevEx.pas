unit FAsisTarjeta_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, ExtCtrls, StdCtrls, DBGrids, Mask, ComCtrls, DBCtrls, 
     ZBaseAsisConsulta_DevEx, ZetaDBTextBox, ZetaFecha,
     cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
     TressMorado2013, dxSkinsDefaultPainters,
     dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
     cxEdit, cxNavigator, cxDBData, Menus, ActnList, ImgList, cxGridLevel,
     cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
     cxGridDBTableView, cxGrid, ZetaCXGrid, cxPCdxBarPopupMenu, cxPC,
     cxContainer, cxGroupBox, cxButtons, System.Actions;

type
  TAsisTarjeta_DevEx = class(TBaseAsisConsulta_DevEx)
    dsChecadas: TDataSource;
    PageControl1: TcxPageControl;
    TabGenerales: TcxTabSheet;
    TabHoras: TcxTabSheet;
    Label1: TLabel;
    AU_FECHA: TZetaDBTextBox;
    Label3: TLabel;
    HO_DESCRIP: TZetaDBTextBox;
    LblDia: TLabel;
    Label4: TLabel;
    Label11: TLabel;
    AU_TIPO: TZetaDBTextBox;
    IN_ELEMENT: TZetaDBTextBox;
    HO_CODIGO: TZetaDBTextBox;
    LblUsuario: TLabel;
    US_CODIGO: TZetaDBTextBox;
    DBCheckBox1: TDBCheckBox;
    TabClasifica: TcxTabSheet;
    CB_TURNO: TZetaDBTextBox;
    TU_DESCRIP: TZetaDBTextBox;
    LblTurno: TLabel;
    Label2: TLabel;
    cb_puesto: TZetaDBTextBox;
    PU_DESCRIP: TZetaDBTextBox;
    Label7: TLabel;
    cb_clasifi: TZetaDBTextBox;
    Clasifi: TZetaDBTextBox;
    TabNiveles: TcxTabSheet;
    CB_NIVEL1lbl: TLabel;
    CB_NIVEL2lbl: TLabel;
    CB_NIVEL3lbl: TLabel;
    CB_NIVEL4lbl: TLabel;
    CB_NIVEL5lbl: TLabel;
    CB_NIVEL1: TZetaDBTextBox;
    CB_NIVEL2: TZetaDBTextBox;
    CB_NIVEL3: TZetaDBTextBox;
    CB_NIVEL4: TZetaDBTextBox;
    CB_NIVEL5: TZetaDBTextBox;
    Label19: TLabel;
    Label21: TLabel;
    AU_POSICIO: TZetaDBTextBox;
    GroupBox1: TcxGroupBox;
    Label5: TLabel;
    AU_HORASCK: TZetaDBTextBox;
    AU_TARDES: TZetaDBTextBox;
    Label9: TLabel;
    Label6: TLabel;
    AU_PER_CG: TZetaDBTextBox;
    Label8: TLabel;
    AU_PER_SG: TZetaDBTextBox;
    Label17: TLabel;
    AU_AUT_TRA: TZetaDBTextBox;
    Label10: TLabel;
    AU_DES_TRA: TZetaDBTextBox;
    GroupBox2: TcxGroupBox;
    Label12: TLabel;
    AU_NUM_EXT: TZetaDBTextBox;
    lbAutorizadas: TLabel;
    AU_AUT_EXT: TZetaDBTextBox;
    Label14: TLabel;
    AU_EXTRAS: TZetaDBTextBox;
    Label15: TLabel;
    AU_DOBLES: TZetaDBTextBox;
    Label16: TLabel;
    AU_TRIPLES: TZetaDBTextBox;
    CB_NIVEL6lbl: TLabel;
    CB_NIVEL7lbl: TLabel;
    CB_NIVEL8lbl: TLabel;
    CB_NIVEL9lbl: TLabel;
    CB_NIVEL9: TZetaDBTextBox;
    CB_NIVEL8: TZetaDBTextBox;
    CB_NIVEL7: TZetaDBTextBox;
    CB_NIVEL6: TZetaDBTextBox;
    Label18: TLabel;
    AU_STATUS: TZetaDBTextBox;
    AU_TIPODIA: TZetaDBTextBox;
    Verificar: TcxButton;
    AU_DIA_SEM: TZetaDBTextBox;
    AU_PERIODO: TZetaDBTextBox;
    ZFecha: TZetaFecha;
    lblTomoComida: TLabel;
    AU_OUT2EAT: TZetaDBTextBox;
    bbMostrarCalendario: TcxButton;
    BtnCalendario: TcxButton;
    Label20: TLabel;
    AU_STA_FES: TZetaDBTextBox;
    AU_PRE_EXT: TZetaDBTextBox;
    Label13: TLabel;
    Label22: TLabel;
    CB_NOMINA: TZetaDBTextBox;
    NO_DESCRIP: TZetaDBTextBox;
    CB_NIVEL10lbl: TLabel;
    CB_NIVEL10: TZetaDBTextBox;
    Label23: TLabel;
    ZetaDBTextBox2: TZetaDBTextBox;
    CB_NIVEL11lbl: TLabel;
    CB_NIVEL11: TZetaDBTextBox;
    CB_NIVEL12lbl: TLabel;
    CB_NIVEL12: TZetaDBTextBox;
    CH_H_REAL: TcxGridDBColumn;
    CH_H_AJUS: TcxGridDBColumn;
    CH_TIPO: TcxGridDBColumn;
    CH_DESCRIP: TcxGridDBColumn;
    CH_HOR_ORD: TcxGridDBColumn;
    CH_HOR_EXT: TcxGridDBColumn;
    CH_HOR_DES: TcxGridDBColumn;
    CH_APRUEBA: TcxGridDBColumn;
    US_CODIGO_GRID: TcxGridDBColumn;
    CH_MOTIVO: TcxGridDBColumn;
    CH_SISTEMA: TcxGridDBColumn;
    CH_GLOBAL: TcxGridDBColumn;
    CH_RELOJ: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure VerificarClick(Sender: TObject);
    procedure ZFechaValidDate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bbMostrarCalendarioClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure BtnCalendarioClick(Sender: TObject);
  private
    { Private declarations }
    procedure SetCamposNivel;
    procedure SetLabelAprobadas;
    {$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
    procedure CambiosVisuales;
    {$ifend}
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  AsisTarjeta_DevEx: TAsisTarjeta_DevEx;

implementation

{$R *.DFM}

uses ZGlobalTress,
     ZetaDialogo,
     ZetaClientTools,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses,
     DSistema,
     DAsistencia,
     DGlobal,
     DCatalogos,
     DTablas,
     DCliente,
     DRecursos,
     FTressShell,
     FToolsAsistencia, ZBaseGridLectura_DevEx;

procedure TAsisTarjeta_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     CB_NIVEL10lbl.Visible := True;
     CB_NIVEL11lbl.Visible := True;
     CB_NIVEL12lbl.Visible := True;
     CB_NIVEL10.Visible := True;
     CB_NIVEL11.Visible := True;
     CB_NIVEL12.Visible := True;
     CB_NIVEL10.DataSource := DataSource;
     CB_NIVEL11.DataSource := DataSource;
     CB_NIVEL12.DataSource := DataSource;
     {$endif}
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stFecha;
     HelpContext := H20211_Tarjeta_diaria;
     SetCamposNivel;
end;

procedure TAsisTarjeta_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmCatalogos do
     begin
         cdsHorarios.Conectar;
         cdsTurnos.Conectar;
         cdsPuestos.Conectar;
         cdsClasifi.Conectar;
         cdsTPeriodos.Conectar;
     end;
     dmTablas.cdsIncidencias.Conectar;
     with dmAsistencia do
     begin
          cdsAsisTarjeta.Conectar;
          DataSource.DataSet:= cdsAsisTarjeta;
          dsChecadas.DataSet := cdsChecadas;
     end;
     ZFecha.Valor := dmCliente.FechaAsistencia;
end;

procedure TAsisTarjeta_DevEx.Refresh;
begin
     dmAsistencia.cdsAsisTarjeta.Refrescar;
     ZFecha.Valor := dmCliente.FechaAsistencia;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TAsisTarjeta_DevEx.Agregar;
begin
     dmAsistencia.cdsAsisTarjeta.Agregar;
end;

procedure TAsisTarjeta_DevEx.Borrar;
begin
     dmAsistencia.cdsAsisTarjeta.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TAsisTarjeta_DevEx.Modificar;
begin
     dmAsistencia.cdsAsisTarjeta.Modificar;
end;

procedure TAsisTarjeta_DevEx.VerificarClick(Sender: TObject);
var
   eStatus: eStatusEmpleado;
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        eStatus := dmRecursos.GetStatus( dmCliente.Empleado, dmCliente.FechaAsistencia );
     finally
            Screen.Cursor := oCursor;
     end;
     ZetaDialogo.ZInformation( 'Verificación de Status',
                                'Empleado ' + ValorActivo1.Caption +
                                CR_LF +
                                CR_LF +
                                'Status Al ' + ZetaCommonTools.FechaLarga( dmCliente.FechaAsistencia ) + ':' +
                                CR_LF +
                                CR_LF +
                                ZetaCommonLists.ObtieneElemento( lfStatusEmpleado, Ord( eStatus ) ), 0 );
end;

procedure TAsisTarjeta_DevEx.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, CB_NIVEL1lbl, CB_NIVEL1 );
     SetCampoNivel( 2, iNiveles, CB_NIVEL2lbl, CB_NIVEL2 );
     SetCampoNivel( 3, iNiveles, CB_NIVEL3lbl, CB_NIVEL3 );
     SetCampoNivel( 4, iNiveles, CB_NIVEL4lbl, CB_NIVEL4 );
     SetCampoNivel( 5, iNiveles, CB_NIVEL5lbl, CB_NIVEL5 );
     SetCampoNivel( 6, iNiveles, CB_NIVEL6lbl, CB_NIVEL6 );
     SetCampoNivel( 7, iNiveles, CB_NIVEL7lbl, CB_NIVEL7 );
     SetCampoNivel( 8, iNiveles, CB_NIVEL8lbl, CB_NIVEL8 );
     SetCampoNivel( 9, iNiveles, CB_NIVEL9lbl, CB_NIVEL9 );
     {$ifdef ACS}
     SetCampoNivel( 10, iNiveles, CB_NIVEL10lbl, CB_NIVEL10 );
     SetCampoNivel( 11, iNiveles, CB_NIVEL11lbl, CB_NIVEL11 );
     SetCampoNivel( 12, iNiveles, CB_NIVEL12lbl, CB_NIVEL12 );
     {$endif}
end;

procedure TAsisTarjeta_DevEx.ZFechaValidDate(Sender: TObject);
begin
     inherited;
     TressShell.SetAsistencia( ZFecha.Valor );
end;

procedure TAsisTarjeta_DevEx.FormShow(Sender: TObject);
const
     COL_APROBADAS = 7;
     COL_MOTIVO = 9;
begin
     ApplyMinWidth;
     inherited;
     DGlobal.SetDescuentoComida( lblTomoComida, AU_OUT2EAT );
     SetLabelAProbadas;
     //ZDBGrid.Columns[COL_APROBADAS].Visible:= dmAsistencia.AprobarPrendido;
     //ZDBGrid.Columns[COL_MOTIVO].Visible:= FToolsAsistencia.PermitirCapturaMotivoCH;{ACL150409}

     ZetaDBGridDBTableView.Columns[COL_APROBADAS].Visible:= dmAsistencia.AprobarPrendido;
     ZetaDBGridDBTableView.Columns[COL_MOTIVO].Visible:= FToolsAsistencia.PermitirCapturaMotivoCH;{ACL150409}
     ZetaDBGridDBTableView.ApplyBestFit();
     {$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
     CambiosVisuales;
     {$ifend}
end;

procedure TAsisTarjeta_DevEx.SetLabelAProbadas;
const
     aCaption: array[ FALSE..TRUE ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Autorizadas:', 'Aprobadas:' );
begin
     lbAutorizadas.Caption := aCaption[ dmAsistencia.AprobarPrendido ];
end;

procedure TAsisTarjeta_DevEx.bbMostrarCalendarioClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.ShowCalendarioRitmo( CB_TURNO.Caption );
end;

procedure TAsisTarjeta_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with dmAsistencia.cdsAsisTarjeta do
          bbMostrarCalendario.Enabled := ZetaCommonTools.StrLleno( FieldByName( 'CB_TURNO' ).AsString );
end;

procedure TAsisTarjeta_DevEx.BtnCalendarioClick(Sender: TObject);
begin
     inherited;
     dmAsistencia.MuestraCalendarioEmpleado;
end;

{$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
procedure TAsisTarjeta_DevEx.CambiosVisuales;
begin
     CB_NIVEL1.Width := K_WIDTH_TEXTBOX;
     CB_NIVEL2.Width := K_WIDTH_TEXTBOX;
     CB_NIVEL3.Width := K_WIDTH_TEXTBOX;
     CB_NIVEL4.Width := K_WIDTH_TEXTBOX;
     CB_NIVEL5.Width := K_WIDTH_TEXTBOX;
     CB_NIVEL6.Width := K_WIDTH_TEXTBOX;
     CB_NIVEL7.Width := K_WIDTH_TEXTBOX;
     CB_NIVEL8.Width := K_WIDTH_TEXTBOX;
     CB_NIVEL9.Width := K_WIDTH_TEXTBOX;
     CB_NIVEL10.Width:= K_WIDTH_TEXTBOX;
     CB_NIVEL11.Width:= K_WIDTH_TEXTBOX;
     CB_NIVEL12.Width:= K_WIDTH_TEXTBOX;
end;
{$ifend}


end.
