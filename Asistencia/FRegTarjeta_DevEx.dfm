inherited RegTarjeta_DevEx: TRegTarjeta_DevEx
  Left = 574
  Top = 255
  Caption = 'Tarjeta Diaria'
  ClientHeight = 458
  ClientWidth = 639
  ExplicitWidth = 645
  ExplicitHeight = 487
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 422
    Width = 639
    ExplicitTop = 422
    ExplicitWidth = 639
    inherited OK_DevEx: TcxButton
      Left = 475
      ExplicitLeft = 475
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 554
      ExplicitLeft = 554
    end
  end
  inherited PanelIdentifica: TPanel
    Top = 0
    Width = 639
    ExplicitTop = 0
    ExplicitWidth = 639
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 317
      end
    end
    inherited ValorActivo2: TPanel
      Width = 313
      ExplicitWidth = 313
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 307
        ExplicitLeft = 227
      end
    end
  end
  inherited Panel1: TPanel
    Top = 19
    Width = 639
    Height = 174
    BevelOuter = bvNone
    ExplicitTop = 19
    ExplicitWidth = 639
    ExplicitHeight = 174
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 424
      Height = 174
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 424
        Height = 52
        Align = alTop
        TabOrder = 0
        object EmpleadoLbl: TLabel
          Left = 20
          Top = 8
          Width = 50
          Height = 13
          Alignment = taRightJustify
          Caption = '&Empleado:'
          FocusControl = CB_CODIGO
        end
        object FechaLbl: TLabel
          Left = 37
          Top = 31
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = '&Fecha:'
          FocusControl = AU_FECHA
        end
        object AU_DIA_SEM: TZetaDBTextBox
          Left = 305
          Top = 29
          Width = 96
          Height = 17
          AutoSize = False
          Caption = 'AU_DIA_SEM'
          ShowAccelChar = False
          Visible = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_DIA_SEM'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object DIA_SEMANA: TZetaTextBox
          Left = 193
          Top = 29
          Width = 96
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object CB_CODIGO: TZetaKeyLookup_DevEx
          Left = 73
          Top = 3
          Width = 331
          Height = 21
          LookupDataset = dmCliente.cdsEmpleadoLookUp
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 83
          OnValidKey = CB_CODIGOValidKey
        end
        object AU_FECHA: TZetaFecha
          Left = 73
          Top = 27
          Width = 115
          Height = 22
          Cursor = crArrow
          Enabled = False
          TabOrder = 1
          Text = '19/Dec/97'
          Valor = 35783.000000000000000000
          OnExit = AU_FECHAExit
          OnValidDate = AU_FECHAValidDate
        end
      end
      object PanelHorario: TPanel
        Left = 0
        Top = 52
        Width = 424
        Height = 122
        Align = alClient
        TabOrder = 1
        object DiaLbl: TLabel
          Left = 49
          Top = 7
          Width = 21
          Height = 13
          Alignment = taRightJustify
          Caption = '&D'#237'a:'
          FocusControl = AU_STATUS
        end
        object HorarioLbl: TLabel
          Left = 33
          Top = 31
          Width = 37
          Height = 13
          Alignment = taRightJustify
          Caption = '&Horario:'
          FocusControl = HO_CODIGO
        end
        object ImgNoRecord: TImage
          Left = 1
          Top = 1
          Width = 16
          Height = 120
          Hint = 'No Existe Informaci'#243'n'
          Align = alLeft
          ParentShowHint = False
          Picture.Data = {
            07544269746D617042010000424D420100000000000076000000280000001100
            0000110000000100040000000000CC0000000000000000000000100000001000
            000000000000000080000080000000808000800000008000800080800000C0C0
            C000808080000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
            FF00DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDDDDDDD0000000DD0000000000
            000DD0000000D999FFFFFFFF9990D0000000D999900000099990D0000000D099
            999FF99999F0D0000000D0F99999999990F0D0000000D0FFF999999FFFF0D000
            0000D0F00099990000F0D0000000D0FFF999999FFFF0D0000000D0F999999999
            90F0D0000000D099999FF99999F0D0000000D999900000099990D0000000D999
            FFFFFFFF9990D0000000D99000000000099DD0000000DDDDDDDDDDDDDDDDD000
            0000DDDDDDDDDDDDDDDDD0000000}
          ShowHint = True
          Transparent = True
          Visible = False
        end
        object ManualLBL: TLabel
          Left = 17
          Top = 51
          Width = 53
          Height = 13
          Alignment = taRightJustify
          Caption = 'Es Manual:'
          FocusControl = HO_CODIGO
        end
        object lblTomoComida: TLabel
          Left = 2
          Top = 95
          Width = 68
          Height = 13
          Caption = 'Tom'#243' Comida:'
        end
        object Label12: TLabel
          Left = 32
          Top = 71
          Width = 37
          Height = 13
          Caption = 'Festivo:'
        end
        object AU_STATUS: TZetaDBKeyCombo
          Left = 73
          Top = 3
          Width = 115
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 0
          TabStop = False
          ListaFija = lfStatusAusencia
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'AU_STATUS'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object HO_CODIGO: TZetaDBKeyLookup_DevEx
          Left = 73
          Top = 27
          Width = 331
          Height = 21
          LookupDataset = dmCatalogos.cdsHorarios
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 60
          DataField = 'HO_CODIGO'
          DataSource = DataSource
        end
        object AU_HOR_MAN: TDBCheckBox
          Left = 73
          Top = 49
          Width = 15
          Height = 17
          DataField = 'AU_HOR_MAN'
          DataSource = DataSource
          Enabled = False
          TabOrder = 2
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object AU_OUT2EAT: TZetaDBKeyCombo
          Left = 73
          Top = 91
          Width = 160
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 4
          ListaFija = lfOut2Eat
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'AU_OUT2EAT'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object AU_STA_FES: TZetaDBKeyCombo
          Left = 73
          Top = 67
          Width = 160
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 3
          OnChange = AU_STA_FESChange
          ListaFija = lfStatusFestivos
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'AU_STA_FES'
          DataSource = DataSource
          LlaveNumerica = True
        end
      end
    end
    object Panel3: TPanel
      Left = 424
      Top = 0
      Width = 215
      Height = 174
      Align = alClient
      TabOrder = 1
      object BTNuevo: TcxButton
        Left = 22
        Top = 16
        Width = 115
        Height = 25
        Hint = 'Nueva Tarjeta'
        Caption = '&Nueva Tarjeta'
        OptionsImage.Glyph.Data = {
          7A080000424D7A08000000000000360000002800000017000000170000000100
          200000000000440800000000000000000000000000000000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF7CDDAEFFFFFFFFFFFFFFFFFFFFFFFFFF92E3
          BCFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF7CDDAEFFFFFFFFFFFFFFFFFFFFFFFFFF92E3BCFF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF7CDDAEFFFFFFFFFFFFFFFFFFFFFFFFFF92E3BCFF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF7CDDAEFFFFFFFFFFFFFFFFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF7CDD
          AEFFFFFFFFFFFFFFFFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFFADEACCFFFFFF
          FFFFFFFFFFFFFFFFFFFFBAEDD5FF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3
          BCFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF7CDDAEFF7CDDAEFF7CDD
          AEFF7CDDAEFF7CDDAEFF9DE6C2FFFFFFFFFFFFFFFFFFFFFFFFFFADEACCFF7CDD
          AEFF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF7CDDAEFFFFFFFFFFFFFFFFFFFFFFFFFF92E3BCFF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF7CDDAEFFFFFFFFFFFFFFFFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF7CDD
          AEFFFFFFFFFFFFFFFFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF7CDDAEFFFFFF
          FFFFFFFFFFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF7CDDAEFFFFFFFFFFFFFF
          FFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TabStop = False
        OnClick = dxBarButton_AgregarBtnClick
      end
      object BTModificar: TcxButton
        Left = 22
        Top = 48
        Width = 115
        Height = 25
        Hint = 'Modificar Tarjeta'
        Caption = 'Modificar &Tarjeta'
        OptionsImage.Glyph.Data = {
          7A080000424D7A08000000000000360000002800000017000000170000000100
          20000000000044080000C40E0000C40E0000000000000000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF97E0F6FF97E0F6FF2CBFEDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF9FE2
          F7FFFFFFFFFFFFFFFFFFB7E9F9FF44C7EFFF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF9FE2F7FFFFFF
          FFFFFBFEFFFF9FE2F7FF18B9EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF9FE2F7FFAFE7F8FF24BD
          ECFF00B2E9FF0CB6EAFF68D1F2FF04B3E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF10B7EAFF00B2E9FF14B8EBFF4CC9
          F0FFFFFFFFFFFFFFFFFF64D0F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF40C5EFFFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFEBF9FDFF10B7EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FFA3E3F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF8BDCF5FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF1CBAEBFFF7FDFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFBFEFFFF28BEECFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF80D9F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFB7E9F9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF08B4EAFFE3F7FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7E9
          F9FF08B4EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF5CCEF1FFFFFFFFFFFFFFFFFFD7F3FCFF48C8EFFF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FFB7E9F9FF6CD3F2FF04B3E9FF00B2E9FF1CBAEBFFA3E3F7FF8BDC
          F5FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF08B4EAFF80D9F4FFF7FDFEFFFFFFFFFFA3E3F7FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF40C5EFFFE3F7FDFFFFFFFFFFF7FDFEFF7CD7F4FF08B4EAFF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF18B9
          EBFFBFECF9FFA7E4F7FF1CBAEBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        TabStop = False
        OnClick = dxBarButton_ModificarBtnClick
      end
      object BTBorrar: TcxButton
        Left = 22
        Top = 80
        Width = 115
        Height = 25
        Hint = 'Borrar Tarjeta'
        Caption = 'Borrar Tarjeta'
        OptionsImage.Glyph.Data = {
          7A080000424D7A08000000000000360000002800000017000000170000000100
          20000000000044080000000000000000000000000000000000004858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFFAFB6E9FFAFB6E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF98A1E2FFC3C8EEFF4B5BCDFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFAFB6E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF98A1
          E2FFFFFFFFFFFFFFFFFFC3C8EEFF4B5BCDFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFFACB3E8FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFAFB6E9FF4858CCFF4858CCFF4858CCFF98A1E2FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFC3C8EEFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF848FDDFFFCFCFEFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFAFB6E9FF4858CCFF98A1E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF98A1E2FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF848FDDFFFCFCFEFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFD1D5F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF98A1
          E2FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF848FDDFFFCFCFEFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF98A1E2FF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF848FDDFFFCFCFEFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF98A1E2FF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFFBAC0ECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFD1D5F2FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF98A1E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFAFB6E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF98A1
          E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFAFB6E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF98A1E2FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFBAC0ECFFFCFCFEFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFAFB6E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF98A1E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF98A1E2FF4858CCFF848FDDFFFCFCFEFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFAFB6E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF98A1E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF98A1
          E2FF4858CCFF4858CCFF4858CCFF848FDDFFFCFCFEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF98A1E2FFFFFFFFFFFFFFFFFF98A1E2FF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF848FDDFFFCFCFEFFFFFFFFFFAFB6E9FF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF98A1E2FF98A1E2FF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF848FDDFFACB3E8FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        TabStop = False
        OnClick = dxBarButton_BorrarBtnClick
      end
    end
  end
  inherited PageControl: TcxPageControl
    Top = 193
    Width = 639
    Height = 229
    Properties.ActivePage = Tabla
    ExplicitTop = 193
    ExplicitWidth = 639
    ExplicitHeight = 229
    ClientRectBottom = 227
    ClientRectRight = 637
    inherited Datos: TcxTabSheet
      Caption = 'Horas'
      ExplicitLeft = 2
      ExplicitTop = 27
      ExplicitWidth = 635
      ExplicitHeight = 200
      object GroupBox1: TcxGroupBox
        Left = 0
        Top = 0
        Align = alLeft
        Caption = ' Horas '
        TabOrder = 0
        Height = 200
        Width = 185
        object Label2: TLabel
          Left = 50
          Top = 27
          Width = 50
          Height = 13
          Alignment = taRightJustify
          Caption = 'Ordinarias:'
        end
        object Label4: TLabel
          Left = 64
          Top = 45
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tardes:'
        end
        object Label5: TLabel
          Left = 20
          Top = 63
          Width = 80
          Height = 13
          Alignment = taRightJustify
          Caption = 'Permiso c/Goce:'
        end
        object Label6: TLabel
          Left = 21
          Top = 81
          Width = 79
          Height = 13
          Alignment = taRightJustify
          Caption = 'Permiso s/Goce:'
        end
        object AU_HORASCK: TZetaDBTextBox
          Left = 103
          Top = 25
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_HORASCK'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_HORASCK'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object AU_TARDES: TZetaDBTextBox
          Left = 103
          Top = 43
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_TARDES'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_TARDES'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object AU_PER_CG: TZetaDBTextBox
          Left = 103
          Top = 61
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_PER_CG'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_PER_CG'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object AU_PER_SG: TZetaDBTextBox
          Left = 103
          Top = 79
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_PER_SG'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_PER_SG'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
      object GroupBox2: TcxGroupBox
        Left = 185
        Top = 0
        Align = alLeft
        Caption = ' Descanso '
        TabOrder = 1
        Height = 200
        Width = 168
        object Label1: TLabel
          Left = 32
          Top = 26
          Width = 50
          Height = 13
          Alignment = taRightJustify
          Caption = 'Calculado:'
        end
        object Label3: TLabel
          Left = 29
          Top = 44
          Width = 53
          Height = 13
          Alignment = taRightJustify
          Caption = 'Autorizado:'
        end
        object AU_DES_TRA: TZetaDBTextBox
          Left = 85
          Top = 25
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_DES_TRA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_DES_TRA'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object AU_AUT_TRA: TZetaDBTextBox
          Left = 85
          Top = 43
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_AUT_TRA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_AUT_TRA'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
      object GroupBox3: TcxGroupBox
        Left = 353
        Top = 0
        Align = alClient
        Caption = ' Extras '
        TabOrder = 2
        Height = 200
        Width = 282
        object Label7: TLabel
          Left = 25
          Top = 27
          Width = 55
          Height = 13
          Alignment = taRightJustify
          Caption = 'Calculadas:'
        end
        object Label8: TLabel
          Left = 22
          Top = 45
          Width = 58
          Height = 13
          Alignment = taRightJustify
          Caption = 'Autorizadas:'
        end
        object Label9: TLabel
          Left = 39
          Top = 81
          Width = 41
          Height = 13
          Alignment = taRightJustify
          Caption = 'A Pagar:'
        end
        object Label10: TLabel
          Left = 44
          Top = 99
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Dobles:'
        end
        object Label11: TLabel
          Left = 46
          Top = 117
          Width = 34
          Height = 13
          Alignment = taRightJustify
          Caption = 'Triples:'
        end
        object AU_NUM_EXT: TZetaDBTextBox
          Left = 83
          Top = 25
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_NUM_EXT'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_NUM_EXT'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object AU_AUT_EXT: TZetaDBTextBox
          Left = 83
          Top = 43
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_AUT_EXT'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_AUT_EXT'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object AU_EXTRAS: TZetaDBTextBox
          Left = 83
          Top = 79
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_EXTRAS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_EXTRAS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object AU_DOBLES: TZetaDBTextBox
          Left = 83
          Top = 97
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_DOBLES'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_DOBLES'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object AU_TRIPLES: TZetaDBTextBox
          Left = 83
          Top = 115
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_TRIPLES'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_TRIPLES'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object AU_PRE_EXT: TZetaDBTextBox
          Left = 83
          Top = 61
          Width = 60
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_PRE_EXT'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_PRE_EXT'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label13: TLabel
          Left = 20
          Top = 63
          Width = 60
          Height = 13
          Alignment = taRightJustify
          Caption = 'Prepagadas:'
        end
      end
    end
    inherited Tabla: TcxTabSheet
      Caption = 'Checadas'
      OnEnter = TablaEnter
      ExplicitLeft = 2
      ExplicitTop = 27
      ExplicitWidth = 635
      ExplicitHeight = 200
      inherited GridRenglones: TZetaDBGrid
        Width = 635
        Height = 171
        OnColEnter = GridRenglonesColEnter
        OnTitleClick = nil
        Columns = <
          item
            Expanded = False
            FieldName = 'CH_H_REAL'
            Title.Caption = 'Real'
            Width = 33
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'CH_H_AJUS'
            ReadOnly = True
            Title.Alignment = taRightJustify
            Title.Caption = 'Ajustada'
            Width = 47
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'ch_tipo'
            ReadOnly = True
            Title.Caption = 'Tipo'
            Width = 122
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'ch_descrip'
            ReadOnly = True
            Title.Caption = 'Descripci'#243'n'
            Width = 90
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'CH_HOR_ORD'
            ReadOnly = True
            Title.Alignment = taRightJustify
            Title.Caption = 'Ordinarias'
            Width = 56
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'CH_HOR_EXT'
            ReadOnly = True
            Title.Alignment = taRightJustify
            Title.Caption = 'Extras'
            Width = 38
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'CH_HOR_DES'
            ReadOnly = True
            Title.Alignment = taRightJustify
            Title.Caption = 'Descanso'
            Width = 58
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'CH_APRUEBA'
            ReadOnly = True
            Title.Alignment = taRightJustify
            Title.Caption = 'Aprobadas'
            Width = 58
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'US_CODIGO'
            ReadOnly = True
            Title.Caption = 'Modific'#243
            Width = 47
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CH_MOTIVO'
            Title.Caption = 'Motivo'
            Width = 45
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'CH_RELOJ'
            ReadOnly = True
            Title.Caption = 'Reloj'
            Width = 40
            Visible = True
          end>
      end
      inherited Panel2: TPanel
        Width = 635
        ExplicitWidth = 635
        object BuscarBtnCheca: TcxButton [0]
          Left = 489
          Top = 1
          Width = 24
          Height = 24
          Action = _E_BuscarCodigo
          Enabled = False
          OptionsImage.Glyph.Data = {
            7A080000424D7A08000000000000360000002800000017000000170000000100
            20000000000044080000000000000000000000000000000000008B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFFB7B0B7FFDCD9DDFF908791FF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA39B
            A3FFFBFBFBFFFFFFFFFFEDEBEDFF928993FF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF948B95FFF2F1F2FFFFFF
            FFFFFFFFFFFFD2CED2FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8D838EFFE0DDE0FFFFFFFFFFFFFFFFFFE7E5
            E8FF8F8590FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9B939CFFAEA7
            AEFFB3ACB4FF9A919AFFCAC6CBFFFFFFFFFFFFFFFFFFF4F3F4FF988F99FF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF968D97FFDEDBDFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFAAA3ABFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF928993FFEDEBEDFFFFFFFFFFFFFFFFFFE9E7E9FFE4E1E4FFFDFD
            FDFFFFFFFFFFFFFFFFFFC3BEC4FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFFC5C0C6FFFFFFFFFFF4F3F4FF9D959EFF8B818CFF8B818CFF9B939CFFEFED
            EFFFFFFFFFFFD3D0D4FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFF8F7
            F8FFFFFFFFFFAFA9B0FF8B818CFF8B818CFF8B818CFF8B818CFFA69FA7FFFFFF
            FFFFFDFDFDFF908791FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFF
            FFFF928993FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFF
            FFFFA39BA3FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFFBFBFBFFFFFFFFFF988F
            99FF8B818CFF8B818CFF8B818CFF8B818CFF928993FFFFFFFFFFFFFFFFFF988F
            99FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFFDBD7DBFFFFFFFFFFCECACEFF8B81
            8CFF8B818CFF8B818CFF8B818CFFC8C4C9FFFFFFFFFFEFEDEFFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFFA69FA7FFFDFDFDFFFFFFFFFFD5D2D6FFACA5
            ACFFACA5ACFFD3D0D4FFFFFFFFFFFFFFFFFFB7B0B7FF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFFB8B2B9FFFDFDFDFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFDFDFDFFC2BCC2FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF9B939CFFD2CED2FFE7E5E8FFEFEDEFFFD2CE
            D2FFA49DA5FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
          OptionsImage.Margin = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        inherited BBAgregar_DevEx: TcxButton
          Hint = 'Agregar Checada'
          Caption = ' Agregar Checada'
        end
        inherited BBBorrar_DevEx: TcxButton
          Hint = 'Borrar Checada'
          Caption = '  Borrar Checada'
        end
        inherited BBModificar_DevEx: TcxButton
          Hint = 'Modificar Checada'
          Caption = ' Modificar Checada'
        end
        object btnEntrar: TcxButton
          Left = 424
          Top = 1
          Width = 25
          Height = 25
          Hint = 'Agregar entrada puntual'
          Caption = 'E'
          OptionsImage.Glyph.Data = {
            7A080000424D7A08000000000000360000002800000017000000170000000100
            20000000000044080000000000000000000000000000000000008B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFFA8A1A9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA8A1
            A9FFFFFFFFFFEFEDEFFFD3D0D4FFD3D0D4FFD3D0D4FFD3D0D4FFD3D0D4FFD3D0
            D4FFD3D0D4FFD3D0D4FF9D959EFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFFFF
            FFFFD3D0D4FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFFFFFFFFD3D0
            D4FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFFFFFFFFD3D0D4FF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFFFFFFFFD3D0D4FF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFFA8A1A9FFFFFFFFFFEFEDEFFFD3D0D4FFD3D0D4FFD3D0
            D4FFD3D0D4FFD3D0D4FFD3D0D4FFD3D0D4FFA69FA7FF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFFA8A1A9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B0B7FF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFFA8A1A9FFFFFFFFFFD3D0D4FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA8A1
            A9FFFFFFFFFFD3D0D4FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFFFF
            FFFFD3D0D4FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFFFFFFFFD3D0
            D4FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFFFFFFFFEFEDEFFFD3D0
            D4FFD3D0D4FFD3D0D4FFD3D0D4FFD3D0D4FFD3D0D4FFD3D0D4FFCAC6CBFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0EFF1FF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
          OptionsImage.Margin = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          TabStop = False
          OnClick = TarjetaPuntualClick
        end
        object btnSalir: TcxButton
          Tag = 1
          Left = 456
          Top = 1
          Width = 25
          Height = 25
          Hint = 'Agregar salida puntual'
          Caption = 'S'
          OptionsImage.Glyph.Data = {
            7A080000424D7A08000000000000360000002800000017000000170000000100
            20000000000044080000000000000000000000000000000000008B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF948B95FFC7C2C7FFEFEDEFFFFFFFFFFFFFFFFFFFFBFB
            FBFFE0DDE0FFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF9D959EFFF4F3F4FFFFFFFFFFEFEDEFFFD3D0D4FFD3D0D4FFDBD7DBFFFAF9
            FAFFFFFFFFFFBCB6BDFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFE4E1
            E4FFFFFFFFFFC3BEC4FF8D838EFF8B818CFF8B818CFF8B818CFF928993FFE9E7
            E9FFFBFBFBFF968D97FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9D959EFFFFFFFFFFDBD7
            DBFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB3ACB4FFFFFF
            FFFFB7B0B7FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF9A919AFFD0CCD0FFACA5ACFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFC0BAC0FFFFFFFFFFB7B0
            B7FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF948B95FFBEB8BFFFFAF9FAFFFBFBFBFF968D97FF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA49DA5FFC5C0
            C6FFE7E5E8FFFFFFFFFFFFFFFFFFFBFBFBFFB1ABB2FF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFFB1ABB2FFF2F1F2FFFFFFFFFFFFFFFFFFFFFF
            FFFFEBE9EBFFC7C2C7FF9A919AFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFFAAA3ABFFFFFFFFFFFBFBFBFFD2CED2FFB3ACB4FF968D97FF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFFDEDBDFFFFFFFFFFFA199A2FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFFA199A2FF9B939CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFE4E1
            E4FFFDFDFDFF8D838EFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFF2F1
            F2FFFAF9FAFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCCC8CCFFFFFF
            FFFFC3BEC4FF8D838EFF8B818CFF8B818CFF8D838EFFC7C2C7FFFFFFFFFFD3D0
            D4FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF948B95FFEDEBEDFFFFFF
            FFFFF0EFF1FFDBD7DBFFDBD7DBFFF0EFF1FFFFFFFFFFEDEBEDFF968D97FF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8F8590FFC2BCC2FFE6E3
            E6FFF0EFF1FFF0EFF1FFE4E1E4FFC2BCC2FF908791FF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
            8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
          OptionsImage.Margin = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          TabStop = False
          OnClick = TarjetaPuntualClick
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 388
    Top = 9
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    ImageInfo = <
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF98A1E2FFF6F7FDFFB7BEEBFF4858CDFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4858CDFFB1B8
          E9FFF6F7FDFFACB3E8FF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFFE2E5F7FFFFFFFFFFFFFFFFFFC3C8EEFF4858CDFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4858CDFFC3C8EEFFFFFF
          FFFFFFFFFFFFF6F7FDFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF9BA4E3FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4858
          CDFF4555CCFF4555CCFF4555CCFF4555CCFF4858CDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFA6AEE6FF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4858CDFF4555CCFF4555CCFF4858CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4858CDFF4858CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFC3C8EEFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFFAFB6
          E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4858CDFFC3C8
          EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4858CDFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4858CDFFC3C8EEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4858CDFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4858CDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFAFB6E9FFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4858
          CDFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4858CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4555CCFF4555CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4858CDFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4858CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4555CCFF4555CCFF4555CCFF4555CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4858CDFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFFA9B0E7FFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFB7BEEBFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFFE2E5F7FFFFFFFFFFFFFFFFFFAFB6E9FF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFF6F7FDFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF8792DEFFDFE2F6FFA1A9E5FF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF9BA4
          E3FFDFE2F6FF98A1E2FF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF63D8
          A1FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF8CE2B8FFE9F9
          F1FF52D396FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF63D8A1FFF4FCF8FFFFFF
          FFFFADEACCFF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF50D395FFD6F4E6FFFFFFFFFFFFFF
          FFFFFCFEFDFF6EDAA7FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FFA5E8C8FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFD9F5E7FF50D395FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF76DDACFFFCFEFDFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF9AE5C1FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF58D59AFFE9F9F1FFFFFFFFFFFFFFFFFFC3EFDAFFBDEE
          D6FFFFFFFFFFFFFFFFFFF4FCF8FF60D79FFF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FFC3EFDAFFFFFFFFFFF7FDFAFF8CE2B8FF4DD293FF52D3
          96FFE1F7ECFFFFFFFFFFFFFFFFFFC8F1DDFF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF8AE1B7FFE6F9F0FF68D9A4FF4DD293FF4DD293FF4DD2
          93FF5DD69DFFE4F8EEFFFFFFFFFFFFFFFFFF8FE2BAFF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF50D395FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF63D8A1FFEFFBF5FFFFFFFFFFF7FDFAFF66D8A2FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF68D9A4FFEFFBF5FFFFFFFFFFDEF7EBFF50D395FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF76DDACFFF7FDFAFFFFFFFFFFB8EDD3FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF6EDAA7FFEFFBF5FFFFFFFFFFA0E6
          C4FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF63D8A1FFE1F7ECFFFFFF
          FFFF8FE2BAFF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF55D498FFBDEE
          D6FFFCFEFDFF8AE1B7FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF81DFB1FF8FE2BAFF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD2
          93FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF4DD293FF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF505FCFFF6A77D6FF8D97DFFFACB3E8FFC8CD
          F0FFE8EAF9FFBAC0ECFF6D7AD7FF7382D9FF626FD4FF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF848FDDFF8D97DFFFACB3E8FF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4555CCFF4555CCFFA4ACE6FF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4555CCFF4555CCFFA4ACE6FF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4555CCFF4555CCFFA4ACE6FF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4555CCFF4555CCFFA4ACE6FF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4555CCFF4555CCFF969FE2FF626FD4FF969FE2FF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
          FEFFD4D8F3FFBAC0ECFF4555CCFF4555CCFF969FE2FFF9FAFDFFFFFFFFFFFFFF
          FFFFA4ACE6FF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE5E7F8FFBAC0ECFF4555CCFF4555CCFF6D7AD7FFD1D5F2FFE5E7F8FFBAC0
          ECFF818CDCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4555CCFF4555CCFFA4ACE6FF4555CCFF6774D6FF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4555CCFF4555CCFFA4ACE6FF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4555CCFF4555CCFFA4ACE6FF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4555CCFF4555CCFFA4ACE6FF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4555CCFF4555CCFFA4ACE6FF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFFACB3E8FFBAC0ECFFB1B8E9FF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF5362D0FF7382D9FF969FE2FFB1B8
          E9FFD1D5F2FFACB3E8FF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555
          CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF4555CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end>
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited BarSuperior: TdxBar
      Visible = False
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    ImageInfo = <
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          000000000000000000000000001C190F057C492B10AFA16024E3BC702BEF6C40
          18C737200D9F0302004000000000000000000000000000000000000000000000
          00200000000009050258BC702BEFE58A34FFE58A34FFE58A34FFE58A34FFE58A
          34FFE58A34FFE58A34FF492B10AF000000240000000000000000000000006038
          15BF73441ACBE58A34FFE58A34FF985B22DF170E05780503014C04020044100A
          036C603815BFE58A34FFE58A34FF915621DB0000001800000000000000009156
          21DBE58A34FFE58A34FF593515BB0000000C0000000000000000000000000000
          00000000000005020148C4772DF3E58A34FF6C4018C70000000400000000BC70
          2BEFE58A34FFE58A34FFD88431FB040200440000000000000000000000000000
          0000000000000000000003020040E58A34FFE58A34FF0503014C00000000E58A
          34FFBC702BEF2818088F0000001C000000000000000000000000000000000000
          0000000000000000000000000000603815BFE58A34FF3F250EA700000018150C
          0474000000100000000000000000000000000000000000000000000000000000
          00000000000000000000000000000503014C603815BF533213B7000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000020100380302
          00400000001C0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000663C17C3E58A
          34FF3F250EA70000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000080C070260000000280D080364E58A
          34FFD07E2FF70000001400000000000000000000000000000000000000000000
          0000000000000100002C2B190993BC702BEFE58A34FF0000002000000010D07E
          2FF7E58A34FF533213B700000004000000000000000000000000000000000000
          00000100002CA16024E3E58A34FFE58A34FFE58A34FF00000010000000000D08
          0364E58A34FFE58A34FF492B10AF000000200000000000000000000000000000
          000000000020603815BFE58A34FFE58A34FFD88431FB00000000000000000000
          00002415088BE58A34FFE58A34FFD07E2FF7492B10AF211407872415088B4F2E
          12B3D07E2FF7E58A34FFE58A34FF603815BF88511FD700000000000000000000
          0000000000000A06025CA96427E7E58A34FFE58A34FFE58A34FFE58A34FFE58A
          34FFE58A34FFB26929EB07040154000000000000001800000000000000000000
          000000000000000000000000000C0F0803682F1C0A9773441ACB663C17C32818
          088F0A06025C0000001400000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00041A4831B34ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF256445C70000001000000000000000000205
          034C4ED293FF040C0864040A0760040A0760040A0760040A0760040A0760040A
          0760040A0760040A0760040C086443B47EF3050F0A6C0000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000611
          0C7043B47EF34ED293FF4ED293FF43B47EF3000201380000000000000000040A
          07604ED293FF00000000000000000000000000000000000000000000000047BD
          85F74ED293FF4ED293FF4BC88DFB02070454000000000000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000004ED2
          93FF4ED293FF4BC88DFB0207045400000000000000000000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000004ED2
          93FF4BC88DFB0207045400000000000000000000000000000000000000000103
          02444ED293FF050F0A6C040A0760040A0760040A0760040A0760040C08644ED2
          93FF0309065C0000000000000000000000000000000000000000000000000000
          00000E271B9347BD85F74ED293FF4ED293FF4ED293FF4ED293FF4ED293FF0611
          0C70000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00004656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF2F3A87DF090B1980161C40AF4656CCFF4656CCFF1D24
          55BF090B1980252E6DCF4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF1D2455BF0000000003040A604656CCFF4656CCFF090B
          1980000000001015319F4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF1D2455BF0000000003040A604656CCFF4656CCFF090B
          1980000000001015319F4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF1D2455BF0000000003040A604656CCFF4656CCFF090B
          1980000000001015319F4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF1D2455BF0000000003040A604656CCFF4656CCFF090B
          1980000000001015319F4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF1D2455BF0000000003040A604656CCFF4656CCFF090B
          1980000000001015319F4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF1D2455BF0000000003040A604656CCFF4656CCFF090B
          1980000000001015319F4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF1D2455BF0000000003040A604656CCFF4656CCFF090B
          1980000000001015319F4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF0000000000000000000000000000
          000003040A6003040A6003040A6003040A6003040A6003040A6003040A600304
          0A6003040A6003040A6003040A6003040A600000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000004656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF00000000000000004656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF00000000000000000000
          00000000000000000000000000004656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF0000000000000000000000000000000000000000000000000000
          00000000000000000000000000004656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF0000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          000000050750003C4FAF0002033C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000C106800BDF8FF006484CF000000200000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000C1068000A0E6400000004000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000080002033C0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000202B8F008BB8E7006B8BD30000
          0004000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000D126C007DA5DF00B3EBFB00BDF8FF00BDF8FF000C
          1068000000000000000000000000000000000000000000000000000000000000
          0000000000000019218300AADFF700BDF8FF00BDF8FF00BDF8FF00BDF8FF0092
          C2EB000000100000000000000000000000000000000000000000000000000000
          0000000000000000001800A2D5F300BDF8FF00BDF8FF00BDF8FF00BDF8FF00BD
          F8FF001D288B0000000000000000000000000000000000000000000000000000
          0000000000000000000000151C7C00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BD
          F8FF00B3EBFB0000002400000000000000000000000000000000000000000000
          0000000000000000000000000008007DA5DF00BDF8FF00BDF8FF00BDF8FF00BD
          F8FF00BDF8FF003C4FAF00000000000000000000000000000000000000000000
          00000000000000000000000000000007095800BDF8FF00BDF8FF00BDF8FF00BD
          F8FF004F68BF0001013000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000546FC300BDF8FF00769DDB0004
          064C000000000000000000000018000000100000000000000000000000000000
          00000000000000000000000000000000000000010234000D126C000000040000
          0000000000080013197800A2D5F3001921830000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000004
          064C00769DDB00BDF8FF00BDF8FF001116740000000000000000000000000000
          000000000000000000000000000000000000000000000000000000384AAB00BD
          F8FF00BDF8FF006484CF00030444000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000060854006B
          8BD3000A0E640000000400000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000002150C780957
          30BF095730BF095730BF095730BF095730BF00000000000000140D7541D30F8B
          4DDF0D7541D300000014000000000000000000000000000000000000001C12AA
          61EF16D075FF16D075FF16D075FF16D075FF00000000063E23AB16D075FF16D0
          75FF010F086C000000000000000000000000000000000000000000000000010A
          066016D075FF16D075FF0C7C45D7000301400007035416D075FF0F8B4DDF0642
          26AF00000000000000000427169305321B9F0000000800000000000000000000
          0000063E23AB16D075FF16D075FF01130A74119A57E700010034010A0660010A
          0660010A0660010A0660095C33C316D075FF0B693BCB00000018000000000000
          00000000001411A35BEB16D075FF16D075FF095C33C302170D7C16D075FF16D0
          75FF16D075FF16D075FF16D075FF16D075FF16D075FF0F9252E3000000000000
          0000000000000007035416D075FF16D075FF0F8B4DDF0005024C16C66EFB16D0
          75FF16D075FF16D075FF16D075FF16D075FF16D075FF031A0F80000000000000
          0000000000000000001013B264F316D075FF16D075FF010C0664000000000000
          0000000000000000000005321B9F14BC69F7010A066000000000000000000000
          00000000000005321B9F16D075FF16D075FF16D075FF12AA61EF0000001C0000
          0000000000000000000000030244000201380000000000000000000000000000
          00000005024C16D075FF16D075FF119A57E716D075FF16D075FF074728B30000
          0000000000000000000000000000000000000000000000000000000000000000
          00100F9252E316D075FF16C66EFB00010030063E23AB16D075FF16D075FF010A
          0660000000000000000000000000000000000000000000000000000000000532
          1B9F16D075FF16D075FF031A0F8000000000000000140F9252E316D075FF12AA
          61EF0000001C0000000000000000000000000000000000000000000000280957
          30BF095730BF063E23AB00000004000000000000000000040248095730BF0957
          30BF02150C780000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000D0A0E930D0A0E930000000400000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000303036049394BFF49394BFF201921C300000008000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000303F3140F349394BFF49394BFF100C109B00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0014312731DF49394BFF49394BFF251D26CB0000000800000000000000000000
          0000000000000000000000000000000000000000000000000000000000041C16
          1DBB49394BFF49394BFF382C3BEB0000001C0000000000000000000000000000
          00000000000000000038161116AB463548FB49394BFF372A38E7221A22C74939
          4BFF49394BFF463548FB01010144000000000000000000000000000000000000
          000002010254463548FB49394BFF49394BFF49394BFF49394BFF49394BFF4939
          4BFF49394BFF0806087C00000000000000000000000000000000000000000000
          0024463548FB49394BFF2B222DD70302035C0100014006040670372A38E74939
          4BFF463548FB000000180000000000000000000000000000000000000000120D
          129F49394BFF29202AD30000000C0000000000000000000000000000001C372A
          38E749394BFF0B090B8B00000000000000000000000000000000000000003127
          31DF49394BFF0202025800000000000000000000000000000000000000000806
          087C49394BFF251D26CB00000000000000000000000000000000000000004939
          4BFF49394BFF0000003C00000000000000000000000000000000000000000303
          036049394BFF312731DF00000000000000000000000000000000000000002E24
          2FDB49394BFF0403046400000000000000000000000000000000000000000C0A
          0D8F49394BFF201921C300000000000000000000000000000000000000000C0A
          0D8F49394BFF372A38E7000000180000000000000000000000000000002C4334
          45F749394BFF0706077800000000000000000000000000000000000000000000
          001C372A38E749394BFF372A38E7070607780303036009070A833F3140F34939
          4BFF342836E30000000800000000000000000000000000000000000000000000
          0000000000383B2E3DEF49394BFF49394BFF49394BFF49394BFF49394BFF2920
          2AD30000001C0000000000000000000000000000000000000000000000000000
          00000000000000000010090709801E1720BF312731DF1C161DBB0504056C0000
          000C000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000028292018940000001D0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000017120D7AD8AA7DFF372B20A20000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000001010031C99E74F9D8AA7DFF0403024600000000000000000201
          01390302013F0201013A0201013A040302450705045200000001000000000000
          000000000000000000000E0B0868D8AA7DFF5A4634BF0000000000000000AE89
          64EDD8AA7DFFD8AA7DFFD8AA7DFFD8AA7DFF19140F7E00000001000000000000
          000000000000000000000000000AD8AA7DFFC19871F600000000000000007D62
          49D5D8AA7DFFD8AA7DFFD8AA7DFF18130E7D0000000000000000000000000000
          0000000000000000000000000000D8AA7DFFCDA177FB00000000000000004F3E
          2EB7D8AA7DFFD8AA7DFFBD956EF4000000000000000000000000000000000000
          0000000000000000000000000029D8AA7DFF987758E30000000000000000382B
          20A3D8AA7DFFD8AA7DFFD8AA7DFF241C158E0000000000000000000000000000
          000000000000000000004E3D2DB6D8AA7DFF1F18128700000000000000005C48
          36C0907253DF01010033D8AA7DFFD8AA7DFF1F18128700000005000000000000
          00000000001331261C9CD8AA7DFFBA936CF30000001100000000000000000E0B
          08690100002D000000000A07055CD8AA7DFFD8AA7DFFA88662EB3D2F24A84334
          27ADC69B74F8D8AA7DFFC99E74F90201013A0000000000000000000000000000
          00000000000000000000000000000202013E5C4836C0D8AA7DFFD8AA7DFFD8AA
          7DFFD8AA7DFF463729B00000002A000000000000000000000000000000000000
          0000000000000000000000000000000000000000001A0B0906613E3024A9362A
          1FA1090605590000000F00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          000000000000000000000000000007050150B79622E7A38720DFA38720DFA387
          20DFA38720DFA38720DFA38720DFAD8E22E30000002400000000000000000000
          0000000000000000000000000000372C0A9B0C0A026000000000000000000000
          00000000000000000000000000003B300B9F0C0A026000000000000000000000
          0000070501503B300B9F3B300B9F9A7F1DDB0C0A026000000000000000000000
          00000000000000000000000000003B300B9F0C0A026000000000000000000000
          0020D3AE29F33B300B9F3B300B9F9A7F1DDB0C0A026000000000000000000000
          00000000000000000000000000003B300B9F0C0A026000000000000000000303
          0040A38720DF00000000000000003B300B9F0C0A026000000000000000000000
          00000000000000000000000000003B300B9F0C0A026000000000000000000303
          0040A38720DF00000000000000003B300B9F0C0A026000000000000000000000
          00000000000000000000000000003B300B9F0C0A026000000000000000000303
          0040A38720DF00000000000000003B300B9F1612047400000020000000100000
          00000000000000000000000000003B300B9F0C0A026000000000000000000303
          0040A38720DF00000000000000000302003CD3AE29F3F6CA2FFFF6CA2FFF1612
          04740000000000000000000000003B300B9F0C0A026000000000000000000303
          0040A38720DF0000000000000000000000000302003CD3AE29F3F6CA2FFF3B30
          0B9F0000000000000000000000003B300B9F0C0A026000000000000000000303
          0040A38720DF0000000000000000000000000000000007050150E9C12DFB3B30
          0B9F0000000000000000000000003B300B9F0C0A026000000000000000000303
          0040B79622E703030040030300400101002C000000000000000008060154AD8E
          22E3675414BF675414BF675414BFAD8E22E30000002800000000000000000000
          000C6D5915C3F6CA2FFFF6CA2FFFF6CA2FFF1E19068000000000000000000000
          001800000020493C0EAB493C0EAB0000000C0000000000000000000000000000
          00000000001492781BD7F6CA2FFFF6CA2FFF9A7F1DDB00000000000000000000
          0000000000003B300B9F3B300B9F000000000000000000000000000000000000
          0000000000000000001892781BD7F6CA2FFFA38720DF00000000000000000000
          0000000000003B300B9F3B300B9F000000000000000000000000000000000000
          0000000000000000000000000018A38720DFAD8E22E300000020000000200000
          0020000000205A4A11B72B23088F000000000000000000000000000000000000
          000000000000000000000000000000000028B79622E7F6CA2FFFF6CA2FFFF6CA
          2FFFF6CA2FFF7C6617CB00000028000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          000000000000000000000000001009070A830907098000000018000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000141014A72B222DD7201921C33F3140F3000000340000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000001C161DBB0E0B0F9700000000141014A71C161DBB0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000030303603B2E3DEF0000000C0101014449394BFF0000
          0004000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000008382C3BEB070607780201025049394BFF0000
          0010000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000B080B87382C3BEB433445F7130F13A30000
          0028010001400100014001000140010001400000001000000000000000000000
          00000000000000000000000000080100014049394BFF1B151BB7171218AF4939
          4BFF49394BFF49394BFF49394BFF49394BFF382C3BEB04030464000000281B15
          1BB7312731DF3B2E3DEF49394BFF49394BFF49394BFF463548FB251D26CB1712
          18AF0C0A0D8F06050674020202580000003C0000002000000000271E28CF1913
          19B303030360030303600100014049394BFF161116AB463548FB000000180000
          0000000000000000000000000000000000000000000000000000271E28CF0E0B
          0F9700000000000000040403046849394BFF0604067049394BFF070607780000
          000000000000000000000000000000000000000000000000000001000140372A
          38E73F3140F3382C3BEB433445F70604067000000024463548FB312731DF0000
          0004000000000000000000000000000000000000000000000000000000000000
          0014020102500101014C000000140000000000000000120D129F49394BFF0101
          014C000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000024463548FB1913
          19B3000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000C0A0D8F4939
          4BFF000000200000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000141410
          14A70B080B870000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000240000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000A0D6400485FBD00516BC5005A77CC003044A6020202435A3D27B7271E
          1791211913891F1812877C6048D4261E16900000000000000000000000000000
          0000004960BE00B2E9FF00465BBB005873CA002433962626219C25180F880000
          00150A08065F0101003215100C772D231A980000000000000000000000000000
          0000004960BE005069C400000000000000000000000018171383241911890404
          024A3B2E22A60F0C096B100C096C1B1510810000000000000000000000000000
          0000004B63C0005E7CCF00000000000000000000000020160E83231C158D0000
          001504030245000000070E0B08692B2119960000000000000000000000000000
          0000004B63C0005E7CCF0000000000000000000000001E171185271E17910000
          00000000000E18130E7DB0906CF1352B20A20000000000000000000000000000
          0000004B63C0005E7CCF0000000000000000000000002920189432271C9D0000
          000008060559DDAA7BFF53361EB0000000130000000000000000000000000000
          0000004B63C0005E7CCF00000000000000000000000009060458796046D3120F
          0B736F5034C73A28189F00000000000000000000000000000000000000000000
          0000004B63C0005E7CCF000000000000000000000000000000000302013D110B
          0668010100300813157A00131C7B000000000000000000000000000000000000
          0000004B63C0005F7ED000000000000000000000000000000000000000000000
          000000000000005F87CF001C248B000000000000000000000000000000000000
          0000004962BF004B63C00000000000101574001B238900161E80001B26890010
          167300000000004960BF001B2389000000000000000000000000000000000000
          000000465DBC005A77CC000000280098C6F200B2E9FF00B2E9FF00B2E9FF0098
          C6F200000029005873CA00171F83000000000000000000000000000000000000
          0000003C50B300B2E9FF00B2E9FF00B2E9FF00ACE0FC008DB9EC00ACE0FC00B2
          E9FF00B2E9FF00B2E9FF000F1472000000000000000000000000000000000000
          00000001023800080A5D00080B5E004B63C0007397DD00010237007DA4E30049
          60BE00080B5E00080A5C00000022000000000000000000000000000000000000
          000000000000000000000000000000000000000F1573006889D6000E126F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000252B23088F2B23088F2B23088F2B23088F2B24
          09902B23088F0302003E00000000000000000000000000000000000000000000
          00000000000000000000110D036CF7CB31FFF7CB31FFF7CB31FFF7CB31FFF7CB
          31FFF7CB31FF2C24099100000000000000000000000000000000000000000000
          000000000000000000000E0C0265F8CC32FFF8CC32FFF7CB31FFF7CB31FFF7CB
          31FFF8CC32FF2B24099000000000000000000000000000000000000000000000
          000000000000000000000E0C0265F7CB31FFF7CB31FFF8CC32FFF9CD33FFF7CB
          31FFF7CB31FF2B23088F000000000000000000000000000000000000000C0202
          0258030303600000003C0E0B0263F7CB31FFF6CA30FFF8CC32FFF6CA30FFF7CB
          31FFF8CC32FF2B23088F00000018030303600302035C000000141B151BB74939
          4BFF49394BFF120D129F0E0B0263F7CB31FFF7CB31FFF7CB31FFF8CC32FFF7CB
          31FFF7CB31FF2B23088F01010141433445F7372A38E72B222DD749394BFF4939
          4BFF49394BFF120D129F100D036AF8CC32FFF8CC32FFF8CC32FFF7CB31FFF8CC
          32FFF7CB31FF2B2409900101014109070A830000000449394BFF49394BFF4939
          4BFF49394BFF141014A70000002F201A098C201A098C231C0A91261F0B96261E
          0B9626200B980604035E010101482E242FDB161116AB49394BFF49394BFF4939
          4BFF49394BFF3F3140F306040671030303600303036003030360030303600303
          03600303036004030464271E28CF49394BFF49394BFF49394BFF49394BFF4939
          4BFF49394BFF49394BFF49394BFF49394BFF49394BFF49394BFF49394BFF4939
          4BFF49394BFF49394BFF49394BFF49394BFF49394BFF49394BFF49394BFF4939
          4BFF49394BFF49394BFF49394BFF49394BFF49394BFF49394BFF49394BFF4939
          4BFF49394BFF49394BFF49394BFF49394BFF49394BFF49394BFF49394BFF4939
          4BFF49394BFF49394BFF49394BFF49394BFF49394BFF49394BFF49394BFF4939
          4BFF49394BFF49394BFF49394BFF49394BFF49394BFF49394BFF271E28CF4939
          4BFF49394BFF49394BFF49394BFF49394BFF49394BFF49394BFF49394BFF4939
          4BFF49394BFF49394BFF49394BFF49394BFF49394BFF372A38E7000000301410
          14A71E1720BF1C161DBB02010250000000200000002000000020000000200000
          002000000020090709801E1720BF1E1720BF161116AB01000140000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000001000000020000000400000005000000120000
          000D000000150000000000000000000000000000000000000000000000000000
          00000000000000000000E6BD2BFBE6BD2BFBE6BD2BFBE6BD2BFBDDB629F8E3BA
          29FADDB629F8CFA926F200000000000000000000000000000000000000000000
          00000000000000000000E9BE2BFCF3C72DFF6A5716C2625013BC0000001DF7CB
          31FFF7CB31FFC6A325EF00000000000000000000000000000000000000000000
          00000000000000000000EFC32CFEF3C72DFF00000000100D036800000004DEB8
          2CF7F7CB31FFDBB429F700000000000000000000000000000000000000000000
          00000000000000000000EFC32CFEF3C72DFF000000000E0B0263000000040000
          0019534411B2DBB429F70000000000000000000000000000000002010250271E
          28CF312731DF0B090B8BEFC32CFEF3C72DFF00000000312809960604014D0000
          0010352B0A99DBB429F70000001D312731DF2B222DD704030464312731DF4939
          4BFF49394BFF120D129FF2C62CFFF3C72DFF000000006E5916C3F8CC32FF6250
          13BCF8CC32FFDBB429F700000021201921C309070A833F3140F349394BFF4939
          4BFF49394BFF120D129FE0B729F9F5C92FFF100D0368DEB82CF7F8CC32FFF8CC
          32FFF7CB31FFE0B729F90000001D0D0A0E930000000C49394BFF49394BFF4939
          4BFF49394BFF201921C30000001C0100002C0100002D0100002D0100002C0100
          002D02020142000000100403024F49394BFF49394BFF49394BFF49394BFF4939
          4BFF49394BFF49394BFF2B222DD71E1720BF1E1720BF1E1720BF1E1720BF1E17
          20BF1E1720BF221A22C7463548FB49394BFF49394BFF49394BFF49394BFF4939
          4BFF49394BFF49394BFF49394BFF49394BFF49394BFF49394BFF49394BFF4939
          4BFF49394BFF49394BFF49394BFF49394BFF49394BFF49394BFF4A394BFF4939
          4BFF49394BFF49394BFF49394BFF49394BFF49394BFF49394BFF49394BFF4939
          4BFF49394BFF49394BFF49394BFF49394BFF49394BFF49394BFF463548FB4939
          4BFF49394BFF49394BFF49394BFF49394BFF49394BFF49394BFF49394BFF4939
          4BFF49394BFF49394BFF49394BFF49394BFF49394BFF49394BFF100C109B4939
          4BFF49394BFF49394BFF2E242FDB1E1720BF1E1720BF1E1720BF1E1720BF1E17
          20BF1E1720BF3B2E3DEF49394BFF49394BFF49394BFF1C161DBB000000000000
          0030010001400100014000000004000000000000000000000000000000000000
          0000000000000000001801000140010001400000003800000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end>
  end
  inherited dsRenglon: TDataSource
    Left = 423
    Top = 9
  end
  object ActionList: TActionList
    Images = Image
    Left = 448
    Top = 171
    object _E_BuscarCodigo: TAction
      Hint = 'Buscar C'#243'digo de Tabla'
      ImageIndex = 9
      ShortCut = 16454
      OnExecute = _E_BuscarCodigoExecute
    end
  end
  object Image: TImageList
    ShareImages = True
    Left = 484
    Top = 168
    Bitmap = {
      494C01011A001D000C0010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000007000000001002000000000000070
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000316B
      4200316B4200316B4200296342002963420029634200215A3900215A3900215A
      3900185231001852310018523100184A29000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000006BA57B0063A5730063A5
      73005A9C73005A9C73005A946B005A946B004A945A004A945A004A945A00398C
      5200398C5200318C4A00318C4A00184A29000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF00FFFFFF00FFFFFF008400840084008400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00427B52006BA57B00EFF7EF00EFF7
      EF00E7F7E700E7EFE700E7EFE700DEEFDE00DEEFDE00DEEFDE00DEEFDE00DEEF
      DE00DEE7DE00DEE7DE00318C4A00184A29000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF00FFFFFF00FFFFFF00840084008400840084008400FFFFFF00FFFF
      FF0084008400FFFFFF00FFFFFF00FFFFFF00427B520073AD8400EFF7EF00EFF7
      EF00EFF7EF00E7F7E700E7EFE700E7EFE700DEEFDE00DEEFDE00DEEFDE00DEEF
      DE00DEEFDE00DEE7DE00398C5200185231000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF00FFFFFF00FFFFFF0084008400840084008400840084008400FFFFFF008400
      84008400840084008400FFFFFF00FFFFFF0042845A0073AD8400EFF7EF00EFF7
      EF00EFF7EF00EFF7EF00EFF7EF00E7EFE70084AD8C0029523100295231002952
      310029523100DEEFDE00398C5200185231000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084008400840084008400840084008400FFFFFF008400
      8400840084008400840084008400FFFFFF004A8463007BB58C00F7FFF70052A5
      5A0029523100295231002952310029523100187B18006BB573004A945A004A94
      4A00215A3900DEEFDE004A945A00215A39000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF00FFFFFF00840084008400
      84008400840084008400FFFFFF00840084008400840084008400840084008400
      840084008400840084008400840084008400528C630084BD9400F7FFF700F7F7
      F70052A55A0063B5730052A55A00187B18007BC684004A945A004A944A00215A
      390084AD8C00DEEFDE004A945A00215A39000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF00FFFFFF00840084008400
      8400840084008400840084008400840084008400840084008400840084008400
      8400840084008400840084008400840084005A946B008CB59400F7FFF700F7FF
      F700F7FFF70052A55A003184310084BD94004A945A004A944A00215A3900639C
      6300639C6300DEEFDE004A945A00215A39000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF00FFFFFF00840084008400
      8400840084008400840084008400840084008400840084008400840084008400
      8400840084008400840084008400840084005A9C730094C69C00FFFFFF00F7FF
      F700F7FFF7003184310094C69C005AAD630052A55A00316B4200006B0000E7F7
      E700E7EFE700E7EFE7005A946B00296342000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF008400
      8400840084008400840084008400840084008400840084008400840084008400
      84008400840084008400840084008400840063A5730094C69C00FFFFFF00FFFF
      FF00429442009CD6A5006BB573005AAD630042844A0052A55A0031843100006B
      0000E7F7E700E7EFE7005A946B00296342000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00840084008400840084008400840084008400840084008400840084008400
      8400840084008400840084008400840084006BA57B009CD6A500FFFFFF004A94
      4A00ADD6B50073BD7B0073BD7B004A945A0052845A006BB56B0052A55A003184
      3100006B0000E7F7E7005A9C7300316B42000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF008400
      8400840084008400840084008400840084008400840084008400840084008400
      84008400840084008400840084008400840073AD84009CD6A500FFFFFF006BB5
      6B0063AD6B0063AD6B00529C5A00F7FFF700F7FFF70052845A0052845A005284
      5A0052845A00EFF7EF005A9C7300316B42000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00840084008400840084008400840084008400840084008400840084008400
      84008400840084008400840084008400840073AD8400ADD6B500FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7FFF700F7FFF700F7F7F700EFF7
      EF00EFF7EF00EFF7EF0063A57300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008400840084008400840084008400840084008400840084008400
      8400840084008400840084008400840084007BB58C00ADD6B500FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7FFF700F7FFF700F7F7
      F700EFF7EF00EFF7EF0063A57300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084008400840084008400840084008400840084008400
      8400840084008400840084008400840084007BB58C00ADD6B500ADD6B5009CD6
      A5009CD6A50094C69C0094C69C0094C69C008CB5940084BD94007BB58C0073AD
      840073AD840073AD84006BA57B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840084008400840084008400840084008400
      84008400840084008400840084008400840084BD94007BB58C0073AD840073AD
      84006BA57B0063A573005A9C73005A946B00528C63004A8463004A8463004284
      5A00427B5200427B520000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484000000
      00008484840000000000848484000000000000000000C6C6C600000000000000
      000000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFF
      FF0000FFFF00FFFFFF0000FFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C600000000008484
      84000000000000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C6000000000000FF
      FF0000000000FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFF
      FF0000FFFF00FFFFFF0000FFFF00FFFFFF000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C6C6C600000000008484
      84000000000000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000FFFFFF000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C6C6C6000000000000FF
      FF00848484008484840000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFF
      FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C6C6C600000000008484
      840000FFFF0084848400FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF0000000000FFFFFF00000000000000
      0000FFFFFF00000000000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      FF00000000000000000000000000FFFFFF0000000000BDBDBD00000000000000
      0000FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF00C6C6C6000000000000FF
      FF00848484008484840084848400848484008484840084848400848484008484
      84008484840084848400848484008484840000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400C6C6C600000000008484
      840000FFFF008484840000FFFF0084848400FFFF0000FFFF0000FFFF0000FFFF
      0000848484008484840000FFFF008484840000000000FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000FF000000
      FF0000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF0000000000FFFFFF000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000FFFF00C6C6C600000000000000
      00000000000000FFFF008484840000FFFF0000000000FFFF0000FFFF0000FFFF
      00008484840084848400848484008484840000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000FF000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF0000000000FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400C6C6C600C6C6C600C6C6
      C600C6C6C600000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF
      00008484840000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF0000000000BDBDBD00FFFFFF000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000BD00000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000084848400C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C60000000000FFFF0000FFFF0000FFFF000000000000FFFF
      00008484840000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF0000000000FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BD000000BD000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000FFFF00C6C6C600C6C6C600C6C6
      C600C6C6C60000000000FFFF0000FFFF0000FFFF000000000000000000008484
      84008484840000000000000000000000000000000000FFFFFF0000000000BDBD
      BD00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000BD000000BD00
      0000BD000000BD000000BD000000BD0000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C6C6C600C6C6C600C6C6
      C60000000000FFFF0000FFFF0000FFFF00000000000000000000000000000000
      00008484840000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BD000000BD000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C6C6C600C6C6C600C6C6
      C600C6C6C60000000000FFFF0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BD00000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFF00008484000084000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFF000000000000000000000000000084848400848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484008484840084848400000000000000000000000000848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400FFFFFF0000FF
      FF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FF
      FF00C6C6C60000FFFF008484840000000000000000000000000084848400FFFF
      FF0000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6
      C60000FFFF008484840000000000000000000000000000000000000000008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848400008400000084000000000000000000000084848400FFFFFF00C6C6
      C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6
      C60000FFFF00C6C6C60084848400000000000000000084848400FFFFFF0000FF
      FF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FF
      FF00C6C6C6000000000084848400000000000000000000000000000000008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFF00008484000084000000000000000000000084848400FFFFFF0000FF
      FF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FF
      FF00C6C6C60000FFFF0084848400000000000000000084848400FFFFFF00C6C6
      C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6
      C600848484000000000084848400000000000000000000000000000000008484
      84008484840084848400848484008484840000FFFF0084848400848484008484
      8400848484000000000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFF00008484000084840000840000000000000084848400FFFFFF00C6C6
      C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6
      C60000FFFF00C6C6C600848484000000000084848400FFFFFF00C6C6C60000FF
      FF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FF
      FF00000000008484840084848400000000000000000000000000000000008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484000000000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFF0000FFFF0000848400000000000084848400FFFFFF0000FF
      FF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FF
      FF00C6C6C60000FFFF00848484000000000084848400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484
      840000000000C6C6C60084848400000000000000000000000000000000008484
      840084848400848484008484840084848400848484008484840000FFFF008484
      8400848484000000000084848400848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848400008400
      00008400000000000000FFFF0000848400000000000084848400FFFFFF00C6C6
      C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6
      C60000FFFF00C6C6C60084848400000000008484840084848400848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      84008484840000FFFF0084848400000000000000000000000000000000008484
      84008484840084848400848484008484840084848400848484008484840000FF
      FF0084848400000000008484840000FFFF000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000000FFFF00FFFF00008484
      00008400000000000000FFFF0000848400000000000084848400FFFFFF0000FF
      FF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FF
      FF00C6C6C60000FFFF0084848400000000000000000084848400FFFFFF00C6C6
      C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6
      C60000FFFF00C6C6C60084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848484000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000000FFFF00FFFF00008484
      0000848400008400000084840000848400000000000084848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084848400000000000000000084848400FFFFFF0000FF
      FF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C600FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084848400000000000000000000000000000000000000
      0000848484008484840084848400848484008484840000000000000000000000
      0000000000000000000000000000848484000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000000FFFF0000FFFF00FFFF
      0000848400008484000084840000848400000000000084848400C6C6C60000FF
      FF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60084848400848484008484
      8400848484008484840084848400000000000000000084848400FFFFFF00C6C6
      C60000FFFF00C6C6C60000FFFF00C6C6C600FFFFFF0084848400848484008484
      8400848484008484840084848400000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0084848400000000008484840000FFFF000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000840000008400000084
      0000FFFF0000FFFF0000FFFF000000000000000000000000000084848400C6C6
      C60000FFFF00C6C6C60000FFFF00C6C6C6008484840000000000000000000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF008484
      8400000000000000000084848400000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000840000008400000084
      0000000000000000000000000000000000000000000000000000000000008484
      8400848484008484840084848400848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400848484008484840084848400848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000840000008400000084
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000008400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084008400000000008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000840000008400000000000000
      0000000000000000000000000000000084000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084008400840084000000000000000000C6C6C600000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000840000000000000000000000000000000084840000848400008484000084
      8400008484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000840000008400000084000000
      0000000000000000000000008400000000000000000000000000000000008484
      8400000000000000000084848400000000000000000084848400000000000000
      0000848484000000000000000000000000000000000000000000000000000000
      0000840084008400840084008400000000008484840000000000C6C6C6000000
      0000C6C6C6000000000000000000000000008400000084000000840000008400
      0000840000008400000000000000000000000000000000000000000000000000
      0000000000000000000000848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000008400000084000000
      000000000000000084000000840000000000000000000000000000000000FFFF
      FF000000000000000000FFFFFF000000000000000000FFFFFF00000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000008400
      8400840084008400840084008400C6C6C600000000000000000084848400C6C6
      C60000000000C6C6C60000000000000000000000000000000000000000008400
      000084000000000000000000000000000000FFFFFF0000000000FFFFFF000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      8400000084008400840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000840084008400
      84008400840084008400C6C6C600840084008400840084008400000000000000
      0000848484000000000084848400000000000000000000000000000000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000840084000000
      8400000084000000000000000000000000000000000000000000000000008484
      8400000000000000000084848400000000000000000084848400000000000000
      0000848484000000000000000000000000000000000084008400840084008400
      840084008400C6C6C60084008400840084008400840000FFFF00840084008400
      8400000000000000000084848400000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000084008400
      840000008400000084000000000000000000000000000000000000000000FFFF
      FF000000000000000000FFFFFF000000000000000000FFFFFF00000000000000
      0000FFFFFF000000000000000000000000000000000084008400840084008400
      8400C6C6C6008400840084008400840084008400840084008400840084008400
      8400840084008400840000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00848484008484
      8400FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000008400000084000000
      0000000000000000840000008400840084000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008400840084008400C6C6
      C6008400840084008400840084008400840000FFFF0084008400840084008400
      840084008400840084000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000084000000840000008400
      0000840000008400000084000000840084000000840084000000840000008400
      0000840000000000000084008400000084000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084008400C6C6C6008400
      84008400840000FFFF00840084008400840000FFFF0084008400840084008400
      8400840084000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000840084008400FFFFFF00FFFFFF00FFFF
      FF00840000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000000000000000000000000000C6C6C600840084008400
      84008400840000FFFF00840084008400840000FFFF0084008400840084008400
      84000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000FFFFFF000000
      000000000000FFFFFF000000000000000000FFFFFF000000000000000000FFFF
      FF00840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      8400840084008400840000FFFF0000FFFF008400840084008400840084000000
      00000000000000000000000000000000000000000000FFFFFF0084848400FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400840084008400840084008400840084008400000000000000
      00000000000000000000000000000000000000000000FFFFFF0084848400FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000840084008400840000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000000000008400
      0000840000000000000084000000840000000000000084000000840000000000
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400000000008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000008484840000848400008484000000
      0000848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF0000000000FFFF
      FF000000000000000000FFFFFF000000000084848400C6C6C600008484000084
      8400000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000000
      0000C6C6C600000000000000000000000000FFFF0000000000000000000000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000848400C6C6C6000084
      8400008484000000000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400000000008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C6C6000000000000000000FFFF00000000000000FFFF00FFFF
      FF0000FFFF00FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF0000000000000000000000000000848400C6C6
      C600008484000084840000000000848484008484840084848400848484008484
      8400848484000000000000000000000000000000000000000000000000000000
      0000848484000000000000000000848484000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600FFFFFF00FFFFFF00FFFFFF00C6C6C600C6C6
      C60000000000000000000000000000000000FFFF000000000000FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000000000FFFFFF000000
      000000000000FFFFFF00FFFFFF00000000000000000000000000000000000084
      8400C6C6C6000084840000848400000000000000000000000000000000000000
      0000848484008484840000000000000000000000000000000000000000000000
      0000000000000000000084848400000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C6000000FF000000FF000000FF00C6C6C600C6C6
      C60000000000C6C6C6000000000000000000FFFF00000000000000FFFF00FFFF
      FF0000FFFF00FFFFFF00000000000000000000000000000000000000000000FF
      FF0000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000848400C6C6C600008484000084840084848400C6C6C600C6C6C600C6C6
      C600848484000000000084848400000000000000000000000000000000008484
      8400000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C6C600C6C6C60000000000FFFF000000000000FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000000084840084848400FFFFFF00C6C6C600FFFFFF00C6C6
      C600C6C6C6008484840000000000848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000000
      0000C6C6C60000000000C6C6C60000000000FFFF00000000000000FFFF00FFFF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000084848400FFFFFF00FF000000FF000000FF000000FF00
      0000FF000000C6C6C60084848400848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C6
      C60000000000C6C6C600000000000000000000000000000000000000000000FF
      FF00FFFFFF0000FFFF00000000000000000000FFFF0000000000FFFFFF00FFFF
      FF000000000000000000FFFFFF0000000000000000000000000000000000FF00
      00000000000000000000FFFFFF00C6C6C600FF000000C6C6C600FFFFFF00C6C6
      C600FF000000C6C6C600C6C6C600000000000000000000000000000000000000
      0000000000008484840000000000000000000000000084848400000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000C6C6C60000000000C6C6C600000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000000000FF00
      0000FF00000000000000C6C6C600FFFFFF00FF000000FF000000FF000000FF00
      0000FF000000FFFFFF00C6C6C600000000000000000000000000000000008484
      8400000000000000000084848400000000008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FF000000FF000000FF000000FF000000FF000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000000000000000000000000000000000000000000000000000FF00
      00000000000000000000FFFFFF00C6C6C600FF000000FFFFFF00FFFFFF00FF00
      0000FFFFFF00C6C6C600C6C6C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000000000FFFFFF00FFFFFF000000000000000000FFFF
      FF0000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000FF0000008484840084848400FFFFFF00FF000000FF000000FF000000FF00
      0000C6C6C600FFFFFF0084848400848484000000000000000000000000000000
      0000000000008484840000000000000000000000000084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FF000000FF000000FF000000FF000000FF000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000FFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      000000000000000000000000000084848400FFFFFF00C6C6C600FFFFFF00C6C6
      C600FFFFFF008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      FF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000084848400FFFFFF00C6C6C600FFFF
      FF00848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840000000000000000000000
      0000848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000000000000000000000
      000000000000000000000000000000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF000000
      000084848400848484008484840000FFFF0000FFFF0084848400848484008484
      84008484840000FFFF0000FFFF00000000008484840084848400848484008484
      8400848484008484840084848400000000008484840000000000000000000000
      FF000000FF000000FF0000000000848484000000000084848400848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008484000000000000000000000000000000000084848400848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      840000000000000000000000000000000000FFFF0000000000000000000000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000008484840000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF000000000000000000000000000000000084848400848484008484
      8400848484008484840000FFFF00848484008484840084848400848484000000
      000084848400000000000000000000000000FFFF00000000000000FFFF00FFFF
      FF0000FFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000084848400848484008484
      8400848484008484840084848400848484008484840084848400848484000000
      000084848400000000000000000000FFFF00FFFF000000000000FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000008484840000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF0000FFFF000000000000FFFF00FFFFFF000000
      0000FFFFFF000000000000000000000000000000000084848400848484008484
      84008484840084848400848484008484840000FFFF0084848400848484000000
      0000848484008484840000FFFF0084848400FFFF00000000000000FFFF00FFFF
      FF0000FFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000008484840000000000000000000000000000000000000000000000
      000000000000FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000000000FFFF
      FF0000FFFF000000000000000000000000000000000084848400848484008484
      8400848484008484840084848400848484008484840000FFFF00848484000000
      00008484840000FFFF008484840000000000FFFF000000000000FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
      FF0000000000000000000000FF000000FF0000FFFF0000FFFF0000FFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000FFFF0000FFFF00000000000000000000000000000000000000
      00000000000000FFFF00FFFFFF0000FFFF00FFFFFF0000000000FFFFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840000FFFF0000FFFF00FFFF00000000000000FFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000000000FFFF0000FFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000FFFF0000FFFF0000FFFF000000000000000000000000000000
      000000000000FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF000000
      000000FFFF000000000000000000000000000000000000000000848484008484
      8400848484008484840084848400000000000000000000000000000000000000
      00000000000084848400848484000000000000000000000000000000000000FF
      FF00FFFFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      00000000000000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00000000000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF00848484000000
      00008484840000FFFF0084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF00000000000000FFFF00FFFFFF0000FFFF000000000000FFFF000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0084848400000000000000
      0000848484000000000000FFFF00848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF000000000000FF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      0000FFFF0000FFFF00000000000000FFFF00FFFFFF0000FFFF00FFFFFF000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000FFFF0000FFFF0000FFFF0000000000000000000000000000000000000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000000000000000000000000000000000000000000000
      000000000000FFFF000000000000000000000000000000000000000000000000
      0000C6C6C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF000000
      000000000000000000000000000000FFFF0000FFFF0000000000000000000000
      00000000000000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000000000000000000000
      000000000000000000000000000000FFFF000000000000000000000000000000
      000000000000000000000000000000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFF00000000000000000000FFFF0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFF00000000000000000000FFFF0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFF0000FFFF000000000000FFFF0000FFFF0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084840000848400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484000084840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFF0000FFFF000000000000FFFF0000FFFF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFF0000FFFF000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF000000000000000000000000000000000000000000000000
      0000000000008484000084840000848400008484000084840000848400008484
      0000000000000000000000000000000000000000000000000000000000000000
      0000848400008484000084840000848400008484000084840000848400000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FFFF0000FFFF
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000FFFF000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF000000000000000000000000000000000000000000000000
      0000848400008484000084840000848400008484000084840000848400008484
      0000000000000000000000000000000000000000000000000000000000000000
      0000848400008484000084840000848400008484000084840000848400008484
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FFFF
      0000FFFF00000000000000000000000000000000000000000000000000000000
      0000FFFF0000FFFF000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF000000000000000000000000000000000000000000000000
      0000000000008484000084840000848400008484000084840000848400008484
      0000000000000000000000000000000000000000000000000000000000000000
      0000848400008484000084840000848400008484000084840000848400000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FFFF0000FFFF
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFF0000FFFF000000000000FFFF0000FFFF0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084840000848400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484000084840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFF0000FFFF000000000000FFFF0000FFFF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFF00000000000000000000FFFF0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFF00000000000000000000FFFF0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000700000000100010000000000800300000000000000000000
      000000000000000000000000FFFFFF000000E000000000000000800000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000001000000000000000100000000
      000000010000000000000003000000008000FFDFFFFFE7958000FFCFFC00FFBF
      8000FFC7FC00E78080000003FC00E78080000001FC00E38080000000EC00F980
      80000001E400998000000003E0008180000000070000C3800000000F0001FF80
      0007001F0003FB800007007F0007F980002700FF000FC080807701FFE3FFF980
      80FF03FFE7FFFB8081FFFFFFEFFFFFFFFFFBFFFFFFFFFFFFFFF1C000E000FFFF
      FFFB8000C000C001FFFF8000C000C001FFF180008000C001FFF180008000C005
      FFF080000000C005DFF080000000C004754480000000C004DC0480008000C00E
      740080008000E03EDC0080018001F0647401C07FC07FFFCDDC07E0FFE0FFFFFD
      7407FFFFFFFFFFFDFD5FFFFFFFFFFFFFFFFFFFFFFFFFFC7FFF03FFBF8000F81F
      EE01FF3EBFFEF0A7E600FF1DA492E0510200FF99A492C008E6540003BFFE8005
      EE047FC7A4920001FF014943A4920000C1037F98BFFE0001C1810004A00E0003
      01C18007A00E000701FF8007A00E800F01FF8007BFFEE01F03FF8007C001F83F
      07FFA497FFFFFE7F0FFF8007FFFFFFFFFC008FFFFFFFFFFFFC0007FFFFFFC007
      200003FFFFFF8003000081FFFC7F00010000C007F0FF00010000E003F1FF0001
      0000F001E3FF00000000F800E7FF00000000F800E70780000000E800E387C000
      E000E000E107E001F800E800F007E007F000F000F837F007E001FC01FFFFF003
      C403FE03FFFFF803EC07FF07FFFFFFFFFFFFFFFFFF7EFFFF0007FFF890010162
      000720F8C003FFE30007007FE003FE630017007CE003FC030016003CE003F803
      0010000FE003F003001100040001F0030038000C8000E00380F901FFE007C003
      C191E3FCE00F8003FF34FFFCE00F0003FFF7FFFFE0270003FFF7FFF8C07301E3
      FFFFFFF89E7983F7FFFFFFFF7EFEC7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFEDFFF7FFEFFFB7FFC9FFE7FFE7FF93FF81FFC7FFE3FF81F
      F003F807E01F800FE003F007E00F8007C003E007E0078003E003F007E00F8007
      F003F807E01F800FF81FFC7FFE3FF81FFC9FFE7FFE7FF93FFEDFFF7FFEFFFB7F
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
end
