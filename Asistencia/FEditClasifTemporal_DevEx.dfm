inherited EditClasifTemporal_DevEx: TEditClasifTemporal_DevEx
  Left = 515
  Top = 114
  Caption = 'Clasificaciones Temporales'
  ClientHeight = 466
  ClientWidth = 419
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 430
    Width = 419
    TabOrder = 18
    inherited OK_DevEx: TcxButton
      Left = 253
      Top = 5
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 332
      Top = 5
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 419
    TabOrder = 0
    inherited Splitter: TSplitter
      Left = 297
    end
    inherited ValorActivo1: TPanel
      Width = 297
    end
    inherited ValorActivo2: TPanel
      Left = 300
      Width = 119
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 22
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 50
    Width = 419
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object EmpleadoLbl: TLabel
      Left = 53
      Top = 17
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empleado:'
      FocusControl = CB_CODIGO
    end
    object CB_CODIGO: TZetaDBKeyLookup_DevEx
      Left = 107
      Top = 13
      Width = 300
      Height = 21
      LookupDataset = dmCliente.cdsEmpleadoLookUp
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      DataField = 'CB_CODIGO'
      DataSource = DataSource
    end
  end
  object Nivel1: TPanel [4]
    Left = 0
    Top = 158
    Width = 419
    Height = 22
    BevelOuter = bvNone
    TabOrder = 5
    object LNivel1: TLabel
      Left = 13
      Top = 5
      Width = 90
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Nivel 1'
    end
    object CB_NIVEL1: TZetaDBKeyLookup_DevEx
      Left = 107
      Top = 1
      Width = 300
      Height = 21
      LookupDataset = dmTablas.cdsNivel1
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      DataField = 'CB_NIVEL1'
      DataSource = DataSource
    end
  end
  object Nivel2: TPanel [5]
    Left = 0
    Top = 180
    Width = 419
    Height = 22
    BevelOuter = bvNone
    TabOrder = 6
    object Lnivel2: TLabel
      Left = 13
      Top = 5
      Width = 90
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Nivel 2'
    end
    object CB_NIVEL2: TZetaDBKeyLookup_DevEx
      Left = 107
      Top = 1
      Width = 300
      Height = 21
      LookupDataset = dmTablas.cdsNivel2
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidLookup = CB_NIVEL2ValidLookup
      DataField = 'CB_NIVEL2'
      DataSource = DataSource
    end
  end
  object Nivel3: TPanel [6]
    Left = 0
    Top = 202
    Width = 419
    Height = 22
    BevelOuter = bvNone
    TabOrder = 7
    object Lnivel3: TLabel
      Left = 13
      Top = 5
      Width = 90
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Nivel 3'
    end
    object CB_NIVEL3: TZetaDBKeyLookup_DevEx
      Left = 107
      Top = 1
      Width = 300
      Height = 21
      LookupDataset = dmTablas.cdsNivel3
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidLookup = CB_NIVEL3ValidLookup
      DataField = 'CB_NIVEL3'
      DataSource = DataSource
    end
  end
  object Nivel4: TPanel [7]
    Left = 0
    Top = 224
    Width = 419
    Height = 22
    BevelOuter = bvNone
    TabOrder = 8
    object LNivel4: TLabel
      Left = 13
      Top = 5
      Width = 90
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Nivel 4'
    end
    object CB_NIVEL4: TZetaDBKeyLookup_DevEx
      Left = 107
      Top = 1
      Width = 300
      Height = 21
      LookupDataset = dmTablas.cdsNivel4
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidLookup = CB_NIVEL4ValidLookup
      DataField = 'CB_NIVEL4'
      DataSource = DataSource
    end
  end
  object Nivel5: TPanel [8]
    Left = 0
    Top = 246
    Width = 419
    Height = 22
    BevelOuter = bvNone
    TabOrder = 9
    object LNivel5: TLabel
      Left = 13
      Top = 5
      Width = 90
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Nivel 5'
    end
    object CB_NIVEL5: TZetaDBKeyLookup_DevEx
      Left = 107
      Top = 1
      Width = 300
      Height = 21
      LookupDataset = dmTablas.cdsNivel5
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidLookup = CB_NIVEL5ValidLookup
      DataField = 'CB_NIVEL5'
      DataSource = DataSource
    end
  end
  object Nivel6: TPanel [9]
    Left = 0
    Top = 268
    Width = 419
    Height = 22
    BevelOuter = bvNone
    TabOrder = 10
    object LNivel6: TLabel
      Left = 13
      Top = 5
      Width = 90
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Nivel 6'
    end
    object CB_NIVEL6: TZetaDBKeyLookup_DevEx
      Left = 107
      Top = 1
      Width = 300
      Height = 21
      LookupDataset = dmTablas.cdsNivel6
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidLookup = CB_NIVEL6ValidLookup
      DataField = 'CB_NIVEL6'
      DataSource = DataSource
    end
  end
  object Nivel7: TPanel [10]
    Left = 0
    Top = 290
    Width = 419
    Height = 22
    BevelOuter = bvNone
    TabOrder = 11
    object LNivel7: TLabel
      Left = 13
      Top = 5
      Width = 90
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Nivel 7'
    end
    object CB_NIVEL7: TZetaDBKeyLookup_DevEx
      Left = 107
      Top = 1
      Width = 300
      Height = 21
      LookupDataset = dmTablas.cdsNivel7
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidLookup = CB_NIVEL7ValidLookup
      DataField = 'CB_NIVEL7'
      DataSource = DataSource
    end
  end
  object Nivel8: TPanel [11]
    Left = 0
    Top = 312
    Width = 419
    Height = 22
    BevelOuter = bvNone
    TabOrder = 12
    object LNivel8: TLabel
      Left = 13
      Top = 5
      Width = 90
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Nivel 8'
    end
    object CB_NIVEL8: TZetaDBKeyLookup_DevEx
      Left = 107
      Top = 1
      Width = 300
      Height = 21
      LookupDataset = dmTablas.cdsNivel8
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidLookup = CB_NIVEL8ValidLookup
      DataField = 'CB_NIVEL8'
      DataSource = DataSource
    end
  end
  object Nivel9: TPanel [12]
    Left = 0
    Top = 334
    Width = 419
    Height = 22
    BevelOuter = bvNone
    TabOrder = 13
    object LNivel9: TLabel
      Left = 13
      Top = 5
      Width = 90
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Nivel 9'
    end
    object CB_NIVEL9: TZetaDBKeyLookup_DevEx
      Left = 107
      Top = 1
      Width = 300
      Height = 21
      LookupDataset = dmTablas.cdsNivel9
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidLookup = CB_NIVEL9ValidLookup
      DataField = 'CB_NIVEL9'
      DataSource = DataSource
    end
  end
  object Turno: TPanel [13]
    Left = 0
    Top = 135
    Width = 419
    Height = 22
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object LTurno: TLabel
      Left = 72
      Top = 5
      Width = 31
      Height = 13
      Alignment = taRightJustify
      Caption = 'Turno:'
    end
    object CB_TURNO: TZetaDBKeyLookup_DevEx
      Left = 107
      Top = 1
      Width = 300
      Height = 21
      LookupDataset = dmCatalogos.cdsTurnos
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      DataField = 'CB_TURNO'
      DataSource = DataSource
    end
  end
  object Clasificacion: TPanel [14]
    Left = 0
    Top = 113
    Width = 419
    Height = 22
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object LClasificacion: TLabel
      Left = 41
      Top = 5
      Width = 62
      Height = 13
      Alignment = taRightJustify
      Caption = 'Clasificaci'#243'n:'
    end
    object CB_CLASIFI: TZetaDBKeyLookup_DevEx
      Left = 107
      Top = 1
      Width = 300
      Height = 21
      LookupDataset = dmCatalogos.cdsClasifi
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      DataField = 'CB_CLASIFI'
      DataSource = DataSource
    end
  end
  object Puesto: TPanel [15]
    Left = 0
    Top = 91
    Width = 419
    Height = 22
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object LPuesto: TLabel
      Left = 67
      Top = 5
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'Puesto:'
    end
    object CB_PUESTO: TZetaDBKeyLookup_DevEx
      Left = 107
      Top = 1
      Width = 300
      Height = 21
      LookupDataset = dmCatalogos.cdsPuestos
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      DataField = 'CB_PUESTO'
      DataSource = DataSource
    end
  end
  object Nivel12: TPanel [16]
    Left = 0
    Top = 400
    Width = 419
    Height = 22
    BevelOuter = bvNone
    TabOrder = 16
    Visible = False
    object LNivel12: TLabel
      Left = 13
      Top = 5
      Width = 90
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Nivel 12'
      Visible = False
    end
    object CB_NIVEL12: TZetaDBKeyLookup_DevEx
      Left = 107
      Top = 1
      Width = 300
      Height = 21
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      Visible = False
      WidthLlave = 60
      OnValidLookup = CB_NIVEL12ValidLookup
      DataField = 'CB_NIVEL12'
    end
  end
  object Nivel11: TPanel [17]
    Left = 0
    Top = 378
    Width = 419
    Height = 22
    BevelOuter = bvNone
    TabOrder = 15
    Visible = False
    object LNivel11: TLabel
      Left = 13
      Top = 5
      Width = 90
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Nivel 11'
      Visible = False
    end
    object CB_NIVEL11: TZetaDBKeyLookup_DevEx
      Left = 107
      Top = 1
      Width = 300
      Height = 21
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      Visible = False
      WidthLlave = 60
      OnValidLookup = CB_NIVEL11ValidLookup
      DataField = 'CB_NIVEL11'
    end
  end
  object Nivel10: TPanel [18]
    Left = 0
    Top = 356
    Width = 419
    Height = 22
    BevelOuter = bvNone
    TabOrder = 14
    Visible = False
    object LNivel10: TLabel
      Left = 13
      Top = 5
      Width = 90
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Nivel 10'
      Visible = False
    end
    object CB_NIVEL10: TZetaDBKeyLookup_DevEx
      Left = 107
      Top = 1
      Width = 300
      Height = 21
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      Visible = False
      WidthLlave = 60
      OnValidLookup = CB_NIVEL10ValidLookup
      DataField = 'CB_NIVEL10'
    end
  end
  inherited DataSource: TDataSource
    Left = 388
    Top = 15
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
