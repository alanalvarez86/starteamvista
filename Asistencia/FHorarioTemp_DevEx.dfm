inherited HorarioTemp_DevEx: THorarioTemp_DevEx
  Top = 281
  Caption = 'Horario Temporal'
  ClientHeight = 133
  ClientWidth = 370
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 90
    Top = 34
    Width = 63
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha &Inicial:'
    FocusControl = FechaInicial
  end
  object Label2: TLabel [1]
    Left = 95
    Top = 65
    Width = 58
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha &Final:'
    FocusControl = FechaFinal
  end
  inherited PanelBotones: TPanel
    Top = 97
    Width = 370
    inherited OK_DevEx: TcxButton
      Left = 204
      OnClick = OK_DevExClick
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 284
    end
  end
  object FechaInicial: TZetaFecha [3]
    Left = 156
    Top = 29
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 1
    Text = '22/dic/97'
    Valor = 35786.000000000000000000
  end
  object FechaFinal: TZetaFecha [4]
    Left = 156
    Top = 60
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 2
    Text = '22/dic/97'
    Valor = 35786.000000000000000000
  end
  object ValorActivo1: TPanel [5]
    Left = 0
    Top = 0
    Width = 370
    Height = 19
    Align = alTop
    Alignment = taLeftJustify
    BorderWidth = 8
    Caption = 'ValorActivo1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
