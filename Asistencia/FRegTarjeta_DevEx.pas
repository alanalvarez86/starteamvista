unit FRegTarjeta_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, Grids, DBGrids, ComCtrls, ExtCtrls, Mask, DBCtrls, Buttons, StdCtrls,
     ZBaseEdicionRenglon_DevEx, ZetaDBGrid, ZetaDBTextBox, ZetaKeyCombo,
     ZetaFecha, ZetaMessages, ZetaCommonClasses, ZetaSmartLists, ActnList, ImgList,
     cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  cxPC, cxNavigator, cxDBNavigator, cxButtons, cxContainer, cxEdit,
  cxGroupBox, ZetaKeyLookup_DevEx, dxBarBuiltInMenu{$ifdef TRESS_DELPHIXE5_UP}, System.Actions{$endif};

type
  TRegTarjeta_DevEx = class(TBaseEdicionRenglon_DevEx)
    GroupBox1: TcxGroupBox;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    AU_HORASCK: TZetaDBTextBox;
    AU_TARDES: TZetaDBTextBox;
    AU_PER_CG: TZetaDBTextBox;
    AU_PER_SG: TZetaDBTextBox;
    GroupBox2: TcxGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    AU_DES_TRA: TZetaDBTextBox;
    AU_AUT_TRA: TZetaDBTextBox;
    GroupBox3: TcxGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    AU_NUM_EXT: TZetaDBTextBox;
    AU_AUT_EXT: TZetaDBTextBox;
    AU_EXTRAS: TZetaDBTextBox;
    AU_DOBLES: TZetaDBTextBox;
    AU_TRIPLES: TZetaDBTextBox;
    Panel4: TPanel;
    Panel5: TPanel;
    EmpleadoLbl: TLabel;
    FechaLbl: TLabel;
    AU_DIA_SEM: TZetaDBTextBox;
    CB_CODIGO: TZetaKeyLookup_DevEx;
    AU_FECHA: TZetaFecha;
    PanelHorario: TPanel;
    DiaLbl: TLabel;
    HorarioLbl: TLabel;
    AU_STATUS: TZetaDBKeyCombo;
    HO_CODIGO: TZetaDBKeyLookup_DevEx;
    Panel3: TPanel;
    BTNuevo: TcxButton;
    BTModificar: TcxButton;
    BTBorrar: TcxButton;
    ImgNoRecord: TImage;
    DIA_SEMANA: TZetaTextBox;
    AU_HOR_MAN: TDBCheckBox;
    ManualLBL: TLabel;
    lblTomoComida: TLabel;
    AU_OUT2EAT: TZetaDBKeyCombo;
    btnEntrar: TcxButton;
    btnSalir: TcxButton;
    Label12: TLabel;
    AU_STA_FES: TZetaDBKeyCombo;
    AU_PRE_EXT: TZetaDBTextBox;
    Label13: TLabel;
    BuscarBtnCheca: TcxButton;
    ActionList: TActionList;
    _E_BuscarCodigo: TAction;
    Image: TImageList;
    procedure FormCreate(Sender: TObject);
    procedure GridRenglonesEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure CB_CODIGOValidKey(Sender: TObject);
    procedure AU_FECHAValidDate(Sender: TObject);
    procedure dxBarButton_ModificarBtnClick(Sender: TObject);
    procedure dxBarButton_BorrarBtnClick(Sender: TObject);
    procedure dxBarButton_AgregarBtnClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure OK_DevExClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure TablaEnter(Sender: TObject);
    procedure AU_FECHAExit(Sender: TObject);
    procedure TarjetaPuntualClick(Sender: TObject);
    procedure _E_BuscarCodigoExecute(Sender: TObject);
    procedure GridRenglonesColEnter(Sender: TObject);
    procedure AU_STA_FESChange(Sender: TObject);
    procedure SetEditarSoloActivos;


  private
    { Private declarations }
    Conectando: Boolean;
    FTipoRegTarjeta: TTipoRegTarjeta;
    function NoData: Boolean;
    procedure SetControlesRegistro;
    procedure CambioTarjeta;
    procedure SetTipoRegistro;
    procedure SeleccionaSiguienteRenglon;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    //function GetFechaTarjeta: TDate;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoCancelChanges; override;
    procedure HabilitaControles; override;
    procedure KeyDown( var Key: Word; Shift: TShiftState ); override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
    function PuedeBorrar(var sMensaje: String): Boolean;override;
    function PuedeModificar(var sMensaje: String): Boolean;override;
  public
    { Public declarations }
    property TipoRegTarjeta : TTipoRegTarjeta read FTipoRegTarjeta write FTipoRegTarjeta default eRegistroTarjeta;
  end;

var
  RegTarjeta_DevEx: TRegTarjeta_DevEx;

implementation

uses DAsistencia, DCatalogos, DTablas, DCliente, DGlobal,
     ZetaCommonTools, ZetaCommonLists, ZAccesosTress,
     ZetaDialogo, FToolsAsistencia;

{$R *.DFM}

procedure TRegTarjeta_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_ASIS_DATOS_TARJETA_DIARIA;
     HO_CODIGO.TabStop := True;
     HO_CODIGO.LookupDataset := dmCatalogos.cdsHorarios;
     CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookUp;
     HelpContext := H22111_Tarjeta_diaria_edicion; //H20211_Tarjeta_diaria;

     btnEntrar.Tag := 0;
     btnSalir.Tag := 1;
     PageControl.ActivePage:= Tabla;
     //FirstControl := CB_CODIGO;  .. Se Pone el Focus en el Connect

     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TRegTarjeta_DevEx.FormShow(Sender: TObject);
const
     COL_APROBADAS = 7;
     COL_MOTIVO = 9;     
begin
     inherited;
     PageControl.ActivePage:= Tabla;
     SetTipoRegistro;
     DGlobal.SetDescuentoComida( lblTomoComida, AU_OUT2EAT );
     GridRenglones.Columns[COL_APROBADAS].Visible:= dmAsistencia.AprobarPrendido;
     GridRenglones.Columns[COL_MOTIVO].Visible:= FToolsAsistencia.PermitirCapturaMotivoCH;{ACL150409}
     BuscarBtnCheca.Visible:= FToolsAsistencia.PermitirCapturaMotivoCH;{ACL13059}
     if (GridRenglones.Columns[COL_APROBADAS].Visible ) and ( GridRenglones.Columns[COL_MOTIVO].Visible ) then
        RegTarjeta_DevEx.Width:= 695
     else
        RegTarjeta_DevEx.Width:= 647;

     ActiveControl := GridRenglones;
end;

procedure TRegTarjeta_DevEx.Connect;
begin
     Conectando:= TRUE;
     with dmCatalogos do
     begin
         cdsHorarios.Conectar;
         cdsTurnos.Conectar;
         cdsPuestos.Conectar;
         cdsClasifi.Conectar;
     end;
     with dmTablas do
     begin
          cdsIncidencias.Conectar;
          cdsMotCheca.Conectar; {ACL170409}
     end;
     with dmAsistencia do
     begin
          with dmCliente do
          begin
               EmpleadoRegTarjeta:= Empleado;
               CB_CODIGO.SetLlaveDescripcion( IntToStr( Empleado ), GetDatosEmpleadoActivo.Nombre );  // Evita que se Haga el DoLookup del Control
               //CB_CODIGO.Valor := EmpleadoRegTarjeta;
                {$ifndef CAJAAHORRO}
               AU_FECHA.Valor := FechaAsistencia;
               {$endif}
          end;
          cdsAsisTarjeta.Conectar;
          DataSource.DataSet:= cdsAsisTarjeta;
          cdsChecadas.First;
          dsRenglon.DataSet := cdsChecadas;
     end;
     Conectando:= FALSE;
     // Primer Control
     if NoData then
        BTNuevo.SetFocus
     else
        BTModificar.SetFocus;
end;

procedure TRegTarjeta_DevEx.GridRenglonesEnter(Sender: TObject);
begin
     inherited;
//     GridRenglones.SelectedIndex := 0;
end;

procedure TRegTarjeta_DevEx.DoCancelChanges;
begin
     dmAsistencia.cdsChecadas.CancelUpdates;
     inherited DoCancelChanges;
end;

procedure TRegTarjeta_DevEx.BBModificarClick(Sender: TObject);
begin
//     if ( eTipoChecadas( dmAsistencia.cdsChecadas.FieldByName( 'CH_TIPO' ).AsInteger ) in [ chConGoce, chSinGoce, chExtras, chDescanso, chConGoceEntrada, chSinGoceEntrada ] ) then
//        dmAsistencia.cdsChecadas.Modificar
//     else
        inherited;
end;

procedure TRegTarjeta_DevEx.CB_CODIGOValidKey(Sender: TObject);
begin
     inherited;
     with dmAsistencia do
     begin
          if ( CB_CODIGO.Valor <> EmpleadoRegTarjeta ) then
          begin
               EmpleadoRegTarjeta:= CB_CODIGO.Valor;
               if not Conectando then
                  CambioTarjeta;
          end;
     end;
end;

procedure TRegTarjeta_DevEx.AU_FECHAValidDate(Sender: TObject);
begin
     inherited;
     with dmAsistencia do
     begin
          if ( FechaRegTarjeta <> AU_FECHA.Valor ) then
          begin
               FechaRegTarjeta:= AU_FECHA.Valor;
               if not Conectando then
                  CambioTarjeta;
          end;
     end;
     DIA_SEMANA.Caption := ZetaCommonTools.DiaSemana( AU_FECHA.Valor );
end;

procedure TRegTarjeta_DevEx.AU_STA_FESChange(Sender: TObject);
var
   sTU_HOR_FES : String;
   sHorario: String;
   sHorarioAutomatico: String;
begin
     inherited;
     sHorario := HO_CODIGO.Llave;
     sHorarioAutomatico := dmAsistencia.GetStatusHorario(dmAsistencia.cdsAsisTarjeta.FieldByName( 'CB_TURNO' ).AsString, AU_FECHA.Valor).Horario;

     dmCatalogos.cdsTurnos.Locate('TU_CODIGO', dmAsistencia.cdsAsisTarjeta.FieldByName( 'CB_TURNO' ).AsString,[]);
     sTU_HOR_FES := dmCatalogos.cdsTurnos.FieldByName( 'TU_HOR_FES' ).AsString;

     if NOT AU_HOR_MAN.Checked and ( sTU_HOR_FES <> '' ) then
     begin
          if (eStatusFestivo(AU_STA_FES.ItemIndex) = esfFestivo ) and (sTU_HOR_FES <> HO_CODIGO.Llave) then   //FESTIVO
          begin
               if ZetaDialogo.ZConfirm(Caption, '�Asignar el horario de festivo trabajado definido en el turno?', 0, mbYes ) then
               begin
                    // Asignar el horario de festivo trabajado definido en el turno
                    sHorario := sTU_HOR_FES;
               end
          end
          else if (eStatusFestivo(AU_STA_FES.ItemIndex) = esfAutomatico ) and (sHorarioAutomatico <> HO_CODIGO.Llave) then   //AUTOMATICO
          begin
               if (ZetaDialogo.ZConfirm(Caption, '�Asignar el horario determinado por el sistema para este d�a?', 0, mbYes )) then
               begin
                    sHorario := sHorarioAutomatico;
               end
          end
          else if (eStatusFestivo(AU_STA_FES.ItemIndex) = esfFestivoTransferido ) and (sTU_HOR_FES = HO_CODIGO.Llave) then //NO ES FESTIVO
          begin
               if ZetaDialogo.ZConfirm(Caption, '�Asignar el horario ordinario para este d�a?', 0, mbYes ) then
               begin
                    // Asignar el horario ordinario definido en el turno
                    sHorario := dmAsistencia.GetHorarioTurno(CB_CODIGO.Valor, AU_FECHA.Valor, ord (esfFestivoTransferido));
               end
          end
     end;

     HO_CODIGO.Llave := sHorario;
     dmAsistencia.CambiaComboFestivo := TRUE;
end;

procedure TRegTarjeta_DevEx.CambioTarjeta;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        dmAsistencia.cdsAsisTarjeta.Refrescar;
        SetControlesRegistro;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TRegTarjeta_DevEx.dxBarButton_AgregarBtnClick(Sender: TObject);
begin
     inherited;
     if Inserting then
        GridRenglones.SetFocus;
end;

procedure TRegTarjeta_DevEx.dxBarButton_ModificarBtnClick(Sender: TObject);
begin
     inherited;
     if Editing then
        GridRenglones.SetFocus;
end;

procedure TRegTarjeta_DevEx.dxBarButton_BorrarBtnClick(Sender: TObject);
begin
     inherited;
     HabilitaControles;
     if not Editing then
        CB_CODIGO.SetFocus;
end;

procedure TRegTarjeta_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     // inherited;     Si esta vacio no se quiere cerrar la forma
end;

procedure TRegTarjeta_DevEx.HabilitaControles;
begin
     inherited HabilitaControles;
     SetControlesRegistro;
end;

procedure TRegTarjeta_DevEx.SetControlesRegistro;
begin
     with CB_CODIGO do
     begin
          Enabled := not self.Editing;
          EmpleadoLbl.Enabled:= Enabled;
          AU_FECHA.Enabled:= Enabled;
          FechaLbl.Enabled:= Enabled;
          // Botones
          BTNuevo.Enabled := Enabled and NoData;
          BTModificar.Enabled:= Enabled and ( not NoData );
          BTBorrar.Enabled:= Enabled and ( not NoData );
          //ImgNoRecord.Visible:= Enabled and NoData; //DevEx(@am): No es necesario enla Nueva Imagen.
     end;
     BTNuevo.TabStop := BTNuevo.Enabled;
     BTModificar.TabStop := BTModificar.Enabled;
     BTBorrar.TabStop := BTBorrar.Enabled;
     with AU_STATUS do
     begin
          Enabled:= not BTNuevo.Enabled;
          DiaLbl.Enabled:= Enabled;
          HO_CODIGO.Enabled:= Enabled;
          HorarioLbl.Enabled:= Enabled;
          Tabla.Enabled:= Enabled;
     end;
     {
     AU_HOR_MAN.Enabled:= Enabled;
     ManualLbl.Enabled:= Enabled;
     AU_STATUS.TabStop := AU_STATUS.Enabled;
     HO_CODIGO.TabStop := HO_CODIGO.Enabled;
     AU_HOR_MAN.TabStop:= AU_HOR_MAN.Enabled;
     }
end;

function TRegTarjeta_DevEx.NoData: Boolean;
begin
     with Datasource do
     begin
          if ( Dataset = nil ) then
             Result := False
          else
             Result := ( Dataset.RecordCount = 0 );
     end;
end;

procedure TRegTarjeta_DevEx.SetTipoRegistro;
begin
     case TipoRegTarjeta of
          eAgregarTarjeta  : begin
                                  if NoData then
                                  begin
                                       DataSource.DataSet.Append;
                                       GridRenglones.SetFocus;
                                  end
                                  else
                                       CB_CODIGO.SetFocus
                             end;
          eModificarTarjeta :GridRenglones.SetFocus;
     end;
end;

procedure TRegTarjeta_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     if CB_CODIGO.Enabled then
        CB_CODIGO.SetFocus;
end;

procedure TRegTarjeta_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
     inherited;
     if CB_CODIGO.Enabled then
        CB_CODIGO.SetFocus;
end;

procedure TRegTarjeta_DevEx.TablaEnter(Sender: TObject);
begin
     inherited;
     GridRenglones.SetFocus;
end;

// Control de Edici�n del Grid
procedure TRegTarjeta_DevEx.KeyDown(var Key: Word; Shift: TShiftState);
begin
     if ( GridEnfocado) then
     begin
          if ( ssCtrl in Shift ) then { CTRL }
             case Key of
                   66:  { Letra F = Buscar }
                   begin
                        Key := 0;
                        DoLookup;
                   end;
              end;
          if ( Key = VK_RETURN ) then
          begin
               Key := 0;
          end;
     end;
     inherited KeyDown( Key, Shift );
end;

procedure TRegTarjeta_DevEx.KeyPress(var Key: Char);
begin
     if ( GridEnfocado ) then
     begin
          if ( Key = Chr( VK_RETURN ) ) then
          begin
               Key := #0;
               SeleccionaSiguienteRenglon;
          end
          else
              if ( Key = Chr( VK_ESCAPE ) ) then
              begin
                   GridRenglones.selectedIndex := 0;
              end;
     end
     else
          inherited KeyPress( Key );
end;

procedure TRegTarjeta_DevEx.SeleccionaSiguienteRenglon;
begin
     with dmAsistencia.cdsChecadas do
     begin
          Next;
          if EOF then
             Self.Agregar;
     end;
end;

procedure TRegTarjeta_DevEx.WMExaminar(var Message: TMessage);
begin
     if ( TExaminador(  Message.LParam ) = exEnter ) then
        if GridEnfocado then
        begin
             if StrVacio( dmAsistencia.cdsChecadas.FieldByName('CH_H_REAL' ).AsString ) then
                OK_DevEx.SetFocus
             else
                SeleccionaSiguienteRenglon;
        end;
end;

procedure TRegTarjeta_DevEx.AU_FECHAExit(Sender: TObject);
begin
     inherited;
     if ( Self.ActiveControl = nil ) then  // Asignar BTNuevo o BTModificar
     begin
          if BTNuevo.Enabled then
             Self.ActiveControl := BTNuevo
          else
             Self.ActiveControl := BTModificar;
     end;
end;

{function TRegTarjeta.GetFechaTarjeta: TDate;
begin
     if ( DataSource.DataSet <> NIL ) then
        Result := DataSource.DataSet.FieldByName('AU_FECHA').AsDateTime
     else
         Result := NullDateTime;
end;}

function TRegTarjeta_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeBorrar( sMensaje );
     if Result then
     begin
          Result := dmCliente.PuedeCambiarTarjeta( DataSource.DataSet, sMensaje );
     end;
end;

function TRegTarjeta_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := dmCliente.PuedeCambiarTarjeta( DataSource.DataSet, sMensaje );
     end;
end;

procedure TRegTarjeta_DevEx.TarjetaPuntualClick(Sender: TObject);
var
   sChecada: string;
begin
     inherited;
     if (dmAsistencia.GetChecadaPuntual(HO_CODIGO.LLave, TBitBtn(Sender).Tag, sChecada)) then
     begin
          dmAsistencia.RegistrarTarjetaPuntual(sChecada);
     end
     else
     begin
          Zerror(self.Caption, sChecada, 0);
     end;
end;

{ACL150409}
procedure TRegTarjeta_DevEx._E_BuscarCodigoExecute(Sender: TObject);
const
     COL_MOTIVO = 9;
var
   sKey, sDescription: String;
begin
     inherited;
     with GridRenglones do
     begin
          if SelectedIndex = COL_MOTIVO then
          begin
               with dmAsistencia.cdsChecadas do
               begin
                    if ( not EsTipoChecadaAutorizacion( eTipoChecadas( FieldByName('CH_TIPO').AsInteger ) ) ) and dmTablas.cdsMotCheca.Search_DevEx( '', sKey, sDescription ) then
                    begin
                         if not ( State in [ dsEdit, dsInsert ] ) then
                            Edit;
                         FieldByName( 'CH_MOTIVO' ).AsString := sKey;
                    end;
               end;
          end;
     end;

end;

{ACL210409}
procedure TRegTarjeta_DevEx.GridRenglonesColEnter(Sender: TObject);
const
     COL_MOTIVO = 9;
begin
     inherited;
     with GridRenglones do
     begin
          if BuscarBtnCheca.Visible then
             BuscarBtnCheca.Enabled := ( SelectedIndex = COL_MOTIVO );
     end;
end;

procedure TRegTarjeta_DevEx.SetEditarSoloActivos;
begin
     HO_CODIGO.EditarSoloActivos := TRUE;
end;

end.
