unit FEditAutorizaciones_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Buttons, DBCtrls, StdCtrls, Mask,
     {$ifndef VER130}
     Variants,
     {$endif}  
     ZetaNumero,
     ZetaFecha,
     ZetaCommonClasses,
     ZetaKeyCombo, ZetaDBTextBox, ZetaSmartLists,
  ZBaseAsisEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  ZetaKeyLookup_DevEx;

type
  TEditAutorizaciones_DevEx = class(TBaseAsisEdicion_DevEx)
    TipoAutLbl: TLabel;
    HorasLbl: TLabel;
    Label1: TLabel;
    HorasMin: TZetaNumero;
    Label2: TLabel;
    MotivoLbl: TLabel;
    EmpleadoLbl: TLabel;
    CB_CODIGO: TZetaDBKeyLookup_DevEx;
    MotivoAutorizacion: TZetaDBKeyLookup_DevEx;
    CH_TIPO: TZetaDBKeyCombo;
    HORAS: TZetaDBNumero;
    Label3: TLabel;
    US_DESCRIP: TZetaDBTextBox;
    CH_HOR_DES: TZetaDBNumero;
    HorasAprobadaslbl: TLabel;
    AprobadasPorlbl: TLabel;
    US_DES_OK: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure HorasMinExit(Sender: TObject);
    procedure HORASChange(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure CH_TIPOChange(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    procedure SetHorasAprobadas( lblLabel: TLabel; lblControl : TControl );
    procedure SetControlesAprobacion( const lHabilita: boolean );    
    procedure SetControles;
  protected
    { Protected declarations }
    function PuedeModificar(var sMensaje: String): Boolean;override;
    function PuedeBorrar(var sMensaje: String): Boolean;override;
    procedure Connect; override;
    procedure HabilitaControles; override;
    procedure SetPrimerControl; virtual;
  public
    { Public declarations }
  end;

var
  EditAutorizaciones_DevEx: TEditAutorizaciones_DevEx;

implementation

uses DAsistencia,
     DCliente,
     DTablas,
     ZAccesosMgr,
     ZetaCommonLists,
     ZetaCommonTools,
     ZAccesosTress;

{$R *.DFM}

procedure TEditAutorizaciones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := CB_CODIGO;
     IndexDerechos := D_ASIS_DATOS_AUTORIZACIONES;
     HelpContext := H20221_Autorizar_extras_y_permisos;
     TipoValorActivo1 := stFecha;
     CH_TIPO.OffSet := K_AUTORIZACIONES_OFFSET;
     CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookUp;
     MotivoAutorizacion.LookupDataset := dmTablas.cdsMotAuto;

end;

procedure TEditAutorizaciones_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     SetHorasAprobadas( HorasAprobadaslbl, CH_HOR_DES );
     SetHorasAprobadas( AprobadasPorlbl, US_DES_OK );
end;

procedure TEditAutorizaciones_DevEx.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     dmTablas.cdsMotAuto.Conectar;
     with dmAsistencia do
     begin
          cdsAutorizaciones.Conectar;
          DataSource.DataSet := cdsAutorizaciones;
     end;
end;

procedure TEditAutorizaciones_DevEx.HORASChange(Sender: TObject);
begin
     HorasMin.Valor:= Round( HORAS.Valor * 60 );
end;

procedure TEditAutorizaciones_DevEx.HorasMinExit(Sender: TObject);
begin
     with dmAsistencia.cdsAutorizaciones do
     begin
          if ( FieldByName( 'HORAS' ).AsFloat <> ( HorasMin.Valor / 60 ) ) then
          begin
               if not Editing then
                  Edit;
               FieldByName( 'HORAS' ).AsFloat:= ( HorasMin.Valor / 60 );
          end;
     end;
end;

procedure TEditAutorizaciones_DevEx.SetPrimerControl;
begin
     if Inserting then
        FirstControl := CB_CODIGO
     else
        FirstControl := CH_TIPO;
end;

procedure TEditAutorizaciones_DevEx.HabilitaControles;
begin
     CB_CODIGO.Enabled := Inserting;
     SetPrimerControl;
     inherited;
end;

procedure TEditAutorizaciones_DevEx.SetHorasAprobadas(lblLabel: TLabel; lblControl: TControl);

begin
     lblLabel.Visible := dmAsistencia.AprobarPrendido;
     lblControl.Visible := lblLabel.Visible;
end;

procedure TEditAutorizaciones_DevEx.SetControlesAprobacion( const lHabilita: boolean );
begin
     CH_TIPO.Enabled := lHabilita;
     HORAS.Enabled := lHabilita;
     HorasMin.Enabled := lHabilita;
     MotivoAutorizacion.Enabled := lHabilita;
end;

procedure TEditAutorizaciones_DevEx.SetControles;
var
   lEnabled: boolean;
begin
     lEnabled := TRUE;
     if ( dmAsistencia.AprobarPrendido ) then
     begin
          if ( not Inserting ) and ( not dmAsistencia.TieneDerechoAprobar ) then
             lEnabled := ( dmAsistencia.cdsAutorizaciones.FieldByName( 'US_COD_OK' ).AsInteger = 0 );
     end;
     SetControlesAprobacion( lEnabled );
     CH_HOR_DES.Enabled := dmAsistencia.TieneDerechoAprobar;
end;

function TEditAutorizaciones_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := Inherited PuedeBorrar( sMensaje );
     if Result then
     begin
          if ( dmAsistencia.AprobarPrendido ) then
          begin
               if ( dmAsistencia.cdsAutorizaciones.FieldByName( 'US_COD_OK' ).AsInteger > 0 ) then
               begin
                    Result := dmAsistencia.TieneDerechoAprobar;
                    if not Result then
                    begin
                         sMensaje := 'Usuario No Puede Borrar Autorización Ya Aprobada';
                    end;
               end;
          end;
     end;
end;

function TEditAutorizaciones_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := Inherited PuedeModificar( sMensaje );
     if Result then
     begin
          if ( dmAsistencia.AprobarPrendido ) then
          begin
               if ( dmAsistencia.cdsAutorizaciones.FieldByName( 'US_COD_OK' ).AsInteger > 0 ) then
               begin
                    Result := dmAsistencia.TieneDerechoAprobar;
                    if not Result then
                    begin
                         sMensaje := 'Usuario No Puede Modificar Autorización Ya Aprobada';
                    end;
               end;
          end;
     end;
end;

procedure TEditAutorizaciones_DevEx.DataSourceDataChange(Sender: TObject;Field: TField);
begin
     inherited;
     if ( Field = nil ) then
          Setcontroles;
     if ( Field <> nil ) and (Field.FieldName = 'CH_TIPO') then
     begin
          MotivoAutorizacion.Filtro :=  Format('TB_TIPO = %d or TB_TIPO = %d',[K_MOTIVO_AUTORIZ_OFFSET, CH_TIPO.Valor - K_AUTORIZACIONES_OFFSET ]);
     end;
end;

procedure TEditAutorizaciones_DevEx.CH_TIPOChange(Sender: TObject);
begin
     inherited;
     MotivoAutorizacion.Filtro :=  Format('TB_TIPO = %d or TB_TIPO = %d',[K_MOTIVO_AUTORIZ_OFFSET, CH_TIPO.Valor - K_AUTORIZACIONES_OFFSET ]);
end;

procedure TEditAutorizaciones_DevEx.OK_DevExClick(Sender: TObject);
var
  iEmpleado, iTipo: Integer;
  dFecha: TDate;
begin
 with dmAsistencia do
     begin
          OperacionConflicto := Ord( ocReportar );
          CambioAutorizacion := FALSE;

          inherited;

          if CambioAutorizacion then
             with cdsAutorizaciones do
             begin
                  iEmpleado:= FieldByName( 'CB_CODIGO' ).AsInteger;
                  dFecha:=    FieldByName( 'AU_FECHA' ).AsDateTime;
                  iTipo:=     FieldByName( 'CH_TIPO' ).AsInteger;
                  Refrescar;
                  Locate( 'CB_CODIGO;AU_FECHA;CH_TIPO',VarArrayOf([ iEmpleado,
                        dFecha, iTipo]), [] );
             end;
     end;
end;

end.
