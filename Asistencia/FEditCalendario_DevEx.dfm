inherited EditCalendario_DevEx: TEditCalendario_DevEx
  Left = 311
  Top = 259
  Caption = 'Calendario'
  ClientHeight = 425
  ClientWidth = 588
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 389
    Width = 588
    inherited OK_DevEx: TcxButton
      Left = 424
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 503
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 588
    inherited ValorActivo2: TPanel
      Width = 262
      inherited textoValorActivo2: TLabel
        Width = 256
      end
    end
  end
  object PageControl: TcxPageControl [2]
    Left = 0
    Top = 89
    Width = 588
    Height = 300
    Align = alClient
    TabOrder = 3
    Properties.ActivePage = NivelesTab
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 298
    ClientRectLeft = 2
    ClientRectRight = 586
    ClientRectTop = 28
    object HorasTab: TcxTabSheet
      Caption = 'Horas'
      object Label13: TLabel
        Left = 83
        Top = 6
        Width = 50
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ordinarias:'
      end
      object Label14: TLabel
        Left = 101
        Top = 82
        Width = 32
        Height = 13
        Alignment = taRightJustify
        Caption = 'Extras:'
      end
      object Label15: TLabel
        Left = 97
        Top = 44
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Dobles:'
      end
      object Label16: TLabel
        Left = 97
        Top = 158
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tardes:'
      end
      object Label17: TLabel
        Left = 52
        Top = 101
        Width = 80
        Height = 13
        Alignment = taRightJustify
        Caption = 'Permiso c/Goce:'
      end
      object Label18: TLabel
        Left = 53
        Top = 120
        Width = 79
        Height = 13
        Alignment = taRightJustify
        Caption = 'Permiso s/Goce:'
      end
      object Label19: TLabel
        Left = 30
        Top = 139
        Width = 102
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descanso Trabajado:'
      end
      object Label5: TLabel
        Left = 99
        Top = 63
        Width = 34
        Height = 13
        Alignment = taRightJustify
        Caption = 'Triples:'
      end
      object AU_HORASCK: TZetaDBTextBox
        Left = 136
        Top = 4
        Width = 40
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'AU_HORASCK'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AU_HORASCK'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object AU_DOBLES: TZetaDBTextBox
        Left = 136
        Top = 42
        Width = 40
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'AU_DOBLES'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AU_DOBLES'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object AU_TRIPLES: TZetaDBTextBox
        Left = 136
        Top = 61
        Width = 40
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'AU_TRIPLES'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AU_TRIPLES'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object AU_EXTRAS: TZetaDBTextBox
        Left = 136
        Top = 80
        Width = 40
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'AU_EXTRAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AU_EXTRAS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object AU_PER_CG: TZetaDBTextBox
        Left = 136
        Top = 99
        Width = 40
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'AU_PER_CG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AU_PER_CG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object AU_PER_SG: TZetaDBTextBox
        Left = 136
        Top = 118
        Width = 40
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'AU_PER_SG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AU_PER_SG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object AU_DES_TRA: TZetaDBTextBox
        Left = 136
        Top = 137
        Width = 40
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'AU_DES_TRA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AU_DES_TRA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object AU_TARDES: TZetaDBTextBox
        Left = 136
        Top = 156
        Width = 40
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'AU_TARDES'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AU_TARDES'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label6: TLabel
        Left = 92
        Top = 25
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = 'A Pagar:'
      end
      object AU_HORAS: TZetaDBTextBox
        Left = 136
        Top = 23
        Width = 40
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'AU_HORAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AU_HORAS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
    object ClasificacionTab: TcxTabSheet
      Caption = 'Clasificaci'#243'n'
      object Label10: TLabel
        Left = 66
        Top = 103
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = '&Turno:'
        FocusControl = CB_TURNO
      end
      object Label11: TLabel
        Left = 61
        Top = 125
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = '&Puesto:'
        FocusControl = CB_PUESTO
      end
      object Label12: TLabel
        Left = 35
        Top = 147
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = '&Clasificaci'#243'n:'
        FocusControl = CB_CLASIFI
      end
      object Label22: TLabel
        Left = 54
        Top = 168
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modific'#243':'
      end
      object US_CODIGO: TZetaDBTextBox
        Left = 100
        Top = 166
        Width = 145
        Height = 17
        AutoSize = False
        Caption = 'US_CODIGO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_CODIGO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label1: TLabel
        Left = 76
        Top = 15
        Width = 21
        Height = 13
        Alignment = taRightJustify
        Caption = '&D'#237'a:'
        FocusControl = AU_STATUS
      end
      object Label2: TLabel
        Left = 73
        Top = 37
        Width = 24
        Height = 13
        Alignment = taRightJustify
        Caption = '&Tipo:'
        FocusControl = AU_TIPODIA
      end
      object Label3: TLabel
        Left = 45
        Top = 59
        Width = 52
        Height = 13
        Alignment = taRightJustify
        Caption = '&Incidencia:'
        FocusControl = AU_TIPO
      end
      object Label4: TLabel
        Left = 60
        Top = 81
        Width = 37
        Height = 13
        Alignment = taRightJustify
        Caption = '&Horario:'
        FocusControl = HO_CODIGO
      end
      object Label7: TLabel
        Left = 408
        Top = 80
        Width = 53
        Height = 13
        Caption = 'Es Manual:'
      end
      object bbMostrarCalendario: TcxButton
        Left = 401
        Top = 99
        Width = 21
        Height = 21
        Hint = 'Mostrar Calendario del Turno'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
        OnClick = bbMostrarCalendarioClick
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A405000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFF0DECDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5E8DDFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5
          BFFFECD5BFFFF5EADFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5
          BFFFFAF4EFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB5
          8FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5BFFFECD5BFFFF5EA
          DFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5BFFFFAF4EFFFFFFF
          FFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF6
          F1FFDDB58FFFDDB58FFFEEDAC7FFE1BE9DFFDDB58FFFE1BE9DFFEEDAC7FFDDB5
          8FFFDDB58FFFF6ECE3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA
          7FFFDDB58FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFBF9FFF0DFCFFFF0DF
          CFFFF8EFE7FFF2E3D5FFF0DFCFFFF2E3D5FFF8EFE7FFF0DFCFFFF0DFCFFFFBF7
          F3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4
          EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB58FFFECD5
          BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF7F3FFE2BF9FFFE2BF9FFFF0DFCFFFE5C7
          ABFFE2BF9FFFE5C7ABFFF0DFCFFFE2BF9FFFE2BF9FFFF8EFE7FFFFFFFFFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFFAF4EFFFFFFFFFFFE5C7ABFFE1BE9DFFDFBA97FFFBF6F1FFFFFFFFFFFFFF
          FFFFDEB793FFE2BF9FFFE2BF9FFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFF3E4D7FFFFFFFFFFE2BF9FFFFAF4EFFFECD5
          BFFFFAF4EFFFFFFFFFFFFFFFFFFFE2BF9FFFFFFFFFFFE2BF9FFFFFFFFFFFF8EF
          E7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB81FFE1BE
          9DFFDAAF87FFF2E3D5FFE5C7ABFFE1BD9BFFE2BF9FFFE2BF9FFFDEB691FFF8F0
          E9FFDBB189FFE1BE9DFFD9AD83FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
        OptionsImage.Margin = 1
      end
      object CB_TURNO: TZetaDBKeyLookup_DevEx
        Left = 100
        Top = 99
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsTurnos
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_TURNO'
        DataSource = DataSource
      end
      object CB_PUESTO: TZetaDBKeyLookup_DevEx
        Left = 100
        Top = 121
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsPuestos
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_PUESTO'
        DataSource = DataSource
      end
      object CB_CLASIFI: TZetaDBKeyLookup_DevEx
        Left = 100
        Top = 143
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsClasifi
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_CLASIFI'
        DataSource = DataSource
      end
      object AU_STATUS: TZetaDBKeyCombo
        Left = 100
        Top = 11
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 0
        ListaFija = lfStatusAusencia
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'AU_STATUS'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object AU_TIPODIA: TZetaDBKeyCombo
        Left = 100
        Top = 33
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 1
        ListaFija = lfTipoDiaAusencia
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'AU_TIPODIA'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object AU_TIPO: TZetaDBKeyLookup_DevEx
        Left = 100
        Top = 55
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsIncidencias
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        DataField = 'AU_TIPO'
        DataSource = DataSource
      end
      object HO_CODIGO: TZetaDBKeyLookup_DevEx
        Left = 100
        Top = 77
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsHorarios
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'HO_CODIGO'
        DataSource = DataSource
      end
      object AU_HOR_MAN: TDBCheckBox
        Left = 464
        Top = 79
        Width = 25
        Height = 17
        DataField = 'AU_HOR_MAN'
        DataSource = DataSource
        Enabled = False
        ReadOnly = True
        TabOrder = 7
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
    end
    object NivelesTab: TcxTabSheet
      Caption = 'Niveles'
      object CB_NIVEL1Lbl: TLabel
        Left = 242
        Top = 8
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel1'
      end
      object CB_NIVEL2Lbl: TLabel
        Left = 242
        Top = 30
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel2'
      end
      object CB_NIVEL3Lbl: TLabel
        Left = 242
        Top = 52
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel3'
      end
      object CB_NIVEL4Lbl: TLabel
        Left = 242
        Top = 74
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel4'
      end
      object CB_NIVEL5Lbl: TLabel
        Left = 242
        Top = 96
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel5'
      end
      object CB_NIVEL6Lbl: TLabel
        Left = 242
        Top = 118
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel6'
      end
      object CB_NIVEL7Lbl: TLabel
        Left = 242
        Top = 140
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel7'
      end
      object CB_NIVEL8Lbl: TLabel
        Left = 242
        Top = 162
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel8'
      end
      object CB_NIVEL9Lbl: TLabel
        Left = 242
        Top = 184
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel9'
      end
      object CB_NIVEL10Lbl: TLabel
        Left = 236
        Top = 206
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel10'
        Visible = False
      end
      object CB_NIVEL11Lbl: TLabel
        Left = 236
        Top = 228
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel11'
        Visible = False
      end
      object CB_NIVEL12Lbl: TLabel
        Left = 236
        Top = 250
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel12'
        Visible = False
      end
      object CB_NIVEL1: TZetaDBKeyLookup_DevEx
        Left = 279
        Top = 4
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel1
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL1'
        DataSource = DataSource
      end
      object CB_NIVEL2: TZetaDBKeyLookup_DevEx
        Left = 279
        Top = 26
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel2
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL2'
        DataSource = DataSource
      end
      object CB_NIVEL3: TZetaDBKeyLookup_DevEx
        Left = 279
        Top = 48
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel3
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL3'
        DataSource = DataSource
      end
      object CB_NIVEL4: TZetaDBKeyLookup_DevEx
        Left = 279
        Top = 70
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel4
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL4'
        DataSource = DataSource
      end
      object CB_NIVEL5: TZetaDBKeyLookup_DevEx
        Left = 279
        Top = 92
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel5
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL5'
        DataSource = DataSource
      end
      object CB_NIVEL6: TZetaDBKeyLookup_DevEx
        Left = 279
        Top = 114
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel6
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL6'
        DataSource = DataSource
      end
      object CB_NIVEL7: TZetaDBKeyLookup_DevEx
        Left = 279
        Top = 136
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel7
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL7'
        DataSource = DataSource
      end
      object CB_NIVEL8: TZetaDBKeyLookup_DevEx
        Left = 279
        Top = 158
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel8
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL8'
        DataSource = DataSource
      end
      object CB_NIVEL9: TZetaDBKeyLookup_DevEx
        Left = 279
        Top = 180
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel9
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 8
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL9'
        DataSource = DataSource
      end
      object CB_NIVEL10: TZetaDBKeyLookup_DevEx
        Left = 279
        Top = 202
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        Visible = False
        WidthLlave = 60
        DataField = 'CB_NIVEL10'
      end
      object CB_NIVEL11: TZetaDBKeyLookup_DevEx
        Left = 279
        Top = 224
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 10
        TabStop = True
        Visible = False
        WidthLlave = 60
        DataField = 'CB_NIVEL11'
      end
      object CB_NIVEL12: TZetaDBKeyLookup_DevEx
        Left = 279
        Top = 246
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 11
        TabStop = True
        Visible = False
        WidthLlave = 60
        DataField = 'CB_NIVEL12'
      end
    end
  end
  object Panel3: TPanel [4]
    Left = 0
    Top = 50
    Width = 588
    Height = 39
    Align = alTop
    TabOrder = 4
    object Label20: TLabel
      Left = 35
      Top = 13
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = '&Fecha:'
      FocusControl = AU_FECHA
    end
    object AU_DIA_SEM: TZetaDBTextBox
      Left = 195
      Top = 11
      Width = 115
      Height = 17
      AutoSize = False
      Caption = 'AU_DIA_SEM'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AU_DIA_SEM'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object AU_FECHA: TZetaDBFecha
      Left = 72
      Top = 8
      Width = 115
      Height = 22
      Cursor = crArrow
      Enabled = False
      TabOrder = 0
      Text = '22/dic/97'
      Valor = 35786.000000000000000000
      DataField = 'AU_FECHA'
      DataSource = DataSource
    end
  end
  inherited DataSource: TDataSource
    Left = 348
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_AgregarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_ExportarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
