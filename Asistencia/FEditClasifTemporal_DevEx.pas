unit FEditClasifTemporal_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, 
  ZetaSmartLists, ZetaKeyLookup_DevEx, ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TEditClasifTemporal_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    EmpleadoLbl: TLabel;
    CB_CODIGO: TZetaDBKeyLookup_DevEx;
    Nivel1: TPanel;
    LNivel1: TLabel;
    CB_NIVEL1: TZetaDBKeyLookup_DevEx;
    Nivel2: TPanel;
    Lnivel2: TLabel;
    CB_NIVEL2: TZetaDBKeyLookup_DevEx;
    Nivel3: TPanel;
    Lnivel3: TLabel;
    CB_NIVEL3: TZetaDBKeyLookup_DevEx;
    Nivel4: TPanel;
    LNivel4: TLabel;
    CB_NIVEL4: TZetaDBKeyLookup_DevEx;
    Nivel5: TPanel;
    LNivel5: TLabel;
    CB_NIVEL5: TZetaDBKeyLookup_DevEx;
    Nivel6: TPanel;
    LNivel6: TLabel;
    CB_NIVEL6: TZetaDBKeyLookup_DevEx;
    Nivel7: TPanel;
    LNivel7: TLabel;
    CB_NIVEL7: TZetaDBKeyLookup_DevEx;
    Nivel8: TPanel;
    LNivel8: TLabel;
    CB_NIVEL8: TZetaDBKeyLookup_DevEx;
    Nivel9: TPanel;
    LNivel9: TLabel;
    CB_NIVEL9: TZetaDBKeyLookup_DevEx;
    Turno: TPanel;
    LTurno: TLabel;
    CB_TURNO: TZetaDBKeyLookup_DevEx;
    Clasificacion: TPanel;
    Puesto: TPanel;
    LClasificacion: TLabel;
    CB_CLASIFI: TZetaDBKeyLookup_DevEx;
    LPuesto: TLabel;
    CB_PUESTO: TZetaDBKeyLookup_DevEx;
    Nivel12: TPanel;
    LNivel12: TLabel;
    CB_NIVEL12: TZetaDBKeyLookup_DevEx;
    Nivel11: TPanel;
    LNivel11: TLabel;
    CB_NIVEL11: TZetaDBKeyLookup_DevEx;
    Nivel10: TPanel;
    LNivel10: TLabel;
    CB_NIVEL10: TZetaDBKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CB_NIVEL12ValidLookup(Sender: TObject);
    procedure CB_NIVEL11ValidLookup(Sender: TObject);
    procedure CB_NIVEL10ValidLookup(Sender: TObject);
    procedure CB_NIVEL9ValidLookup(Sender: TObject);
    procedure CB_NIVEL8ValidLookup(Sender: TObject);
    procedure CB_NIVEL7ValidLookup(Sender: TObject);
    procedure CB_NIVEL6ValidLookup(Sender: TObject);
    procedure CB_NIVEL5ValidLookup(Sender: TObject);
    procedure CB_NIVEL4ValidLookup(Sender: TObject);
    procedure CB_NIVEL3ValidLookup(Sender: TObject);
    procedure CB_NIVEL2ValidLookup(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    { Private declarations }
    Fcontroles: integer;
    procedure SetCamposNivel;
    {$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
    procedure CambiosVisuales;
    {$ifend}
    procedure SetCamposDerechos;
    {$ifdef ACS}
    procedure LimpiaLookUpOlfKey;{ACS}
    {$endif}
  protected
    procedure Connect; override;
    procedure HabilitaControles; override;
  public
    { Public declarations }
  end;

var
  EditClasifTemporal_DevEx: TEditClasifTemporal_DevEx;

implementation

uses dTablas, dCatalogos, dCliente,
{$ifdef SUPERVISORES}
     dSuper,
{$else}
     dAsistencia,
{$endif}
     dGlobal, ZAccesosMgr, ZAccesosTress,
     ZetaCommonLists, ZetaCommonTools, ZetaClientTools, ZetaCommonClasses;

{$R *.DFM}

const
     K_MIN_ALTURA = 225;
     K_LOOKUP_ALTURA = 23;

procedure TEditClasifTemporal_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stFecha;
     FirstControl := CB_CODIGO;
     {$ifdef ACS}
     CB_NIVEL10.DataSource := DataSource;
     CB_NIVEL11.DataSource := DataSource;
     CB_NIVEL12.DataSource := DataSource;

     NIVEL10.Visible := True;
     NIVEL11.Visible := True;
     NIVEL12.Visible := True;
     CB_NIVEL10.Visible := True;
     CB_NIVEL11.Visible := True;
     CB_NIVEL12.Visible := True;
     LNivel10.Visible := True;
     LNivel11.Visible := True;
     LNivel12.Visible := True;

     NIVEL12.Top := NIVEL12.Top;
     NIVEL11.Top := NIVEL12.Top;
     NIVEL10.Top := NIVEL12.Top;
     NIVEL9.Top := NIVEL12.Top;
     NIVEL8.Top := NIVEL12.Top;
     NIVEL7.Top := NIVEL12.Top;
     NIVEL6.Top := NIVEL12.Top;
     NIVEL5.Top := NIVEL12.Top;
     NIVEL4.Top := NIVEL12.Top;
     NIVEL3.Top := NIVEL12.Top;
     NIVEL2.Top := NIVEL12.Top;
     NIVEL1.Top := NIVEL12.Top;

     NIVEL12.Align := alTop;
     NIVEL11.Align := alTop;
     NIVEL10.Align := alTop;
     NIVEL9.Align := alTop;
     NIVEL8.Align := alTop;
     NIVEL7.Align := alTop;
     NIVEL6.Align := alTop;
     NIVEL5.Align := alTop;
     NIVEL4.Align := alTop;
     NIVEL3.Align := alTop;
     NIVEL2.Align := alTop;
     NIVEL1.Align := alTop;

     NIVEL12.TabOrder := 6;
     NIVEL11.TabOrder := 7;
     NIVEL10.TabOrder := 8;
     NIVEL9.TabOrder := 9;
     NIVEL8.TabOrder := 10;
     NIVEL7.TabOrder := 11;
     NIVEL6.TabOrder := 12;
     NIVEL5.TabOrder := 13;
     NIVEL4.TabOrder := 14;
     NIVEL3.TabOrder := 15;
     NIVEL2.TabOrder := 16;
     NIVEL1.TabOrder := 17;
     {$else}
     NIVEL1.Align := alTop;
     NIVEL2.Align := alTop;
     NIVEL3.Align := alTop;
     NIVEL4.Align := alTop;
     NIVEL5.Align := alTop;
     NIVEL6.Align := alTop;
     NIVEL7.Align := alTop;
     NIVEL8.Align := alTop;
     NIVEL9.Align := alTop;
     NIVEL10.Align := alTop;
     NIVEL11.Align := alTop;
     NIVEL12.Align := alTop;
     {$endif}
{$ifdef SUPERVISORES}
     HelpContext := H_Super_Clasificaciones_Temporales;
     IndexDerechos := D_SUPER_CLASIFI_TEMP;
{$else}
     HelpContext := H_Asistencia_Clasificaciones_Temporales;
     IndexDerechos := D_ASIS_DATOS_CLASIFI_TEMP;
{$endif}
     //SetCamposNivel;
     CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookUp;
     with dmCatalogos do
     begin
          CB_PUESTO.LookupDataset := cdsPuestos;
          CB_CLASIFI.LookupDataset := cdsClasifi;
          CB_TURNO.LookupDataset := cdsTurnos;
     end;
     with dmTablas do
     begin
          CB_NIVEL1.LookupDataset := cdsNivel1;
          CB_NIVEL2.LookupDataset := cdsNivel2;
          CB_NIVEL3.LookupDataset := cdsNivel3;
          CB_NIVEL4.LookupDataset := cdsNivel4;
          CB_NIVEL5.LookupDataset := cdsNivel5;
          CB_NIVEL6.LookupDataset := cdsNivel6;
          CB_NIVEL7.LookupDataset := cdsNivel7;
          CB_NIVEL8.LookupDataset := cdsNivel8;
          CB_NIVEL9.LookupDataset := cdsNivel9;
          {$ifdef ACS}
          CB_NIVEL10.LookupDataset := cdsNivel10;
          CB_NIVEL11.LookupDataset := cdsNivel11;
          CB_NIVEL12.LookupDataset := cdsNivel12;
          {$endif}
     end;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TEditClasifTemporal_DevEx.FormShow(Sender: TObject);

begin
     inherited;
     SetCamposNivel;
     SetCamposDerechos;
     self.Height := K_MIN_ALTURA + ( FControles * K_LOOKUP_ALTURA )+17;
     {$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
     CambiosVisuales;
     {$ifend}
end;

procedure TEditClasifTemporal_DevEx.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmCatalogos do
     begin
          cdsClasifi.Conectar;
          cdsPuestos.Conectar;
          cdsTurnos.Conectar;
     end;
     with dmTablas do
     begin
          {$ifdef ACS}
          LimpiaLookUpOlfKey;
          {$endif}
          if CB_NIVEL1.Visible then cdsNivel1.Conectar;
          if CB_NIVEL2.Visible then cdsNivel2.Conectar;
          if CB_NIVEL3.Visible then cdsNivel3.Conectar;
          if CB_NIVEL4.Visible then cdsNivel4.Conectar;
          if CB_NIVEL5.Visible then cdsNivel5.Conectar;
          if CB_NIVEL6.Visible then cdsNivel6.Conectar;
          if CB_NIVEL7.Visible then cdsNivel7.Conectar;
          if CB_NIVEL8.Visible then cdsNivel8.Conectar;
          if CB_NIVEL9.Visible then cdsNivel9.Conectar;
          {$ifdef ACS}
          if CB_NIVEL10.Visible then cdsNivel10.Conectar;
          if CB_NIVEL11.Visible then cdsNivel11.Conectar;
          if CB_NIVEL12.Visible then cdsNivel12.Conectar;
          {$endif}
     end;
{$ifdef SUPERVISORES}
     with dmSuper do
{$else}
     with dmAsistencia do
{$endif}
     begin
          DataSource.DataSet:= cdsClasifiTemp;
     end;
end;

procedure TEditClasifTemporal_DevEx.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, LNivel1, CB_NIVEL1 );
     SetCampoNivel( 2, iNiveles, LNivel2, CB_NIVEL2 );
     SetCampoNivel( 3, iNiveles, LNivel3, CB_NIVEL3 );
     SetCampoNivel( 4, iNiveles, LNivel4, CB_NIVEL4 );
     SetCampoNivel( 5, iNiveles, LNivel5, CB_NIVEL5 );
     SetCampoNivel( 6, iNiveles, LNivel6, CB_NIVEL6 );
     SetCampoNivel( 7, iNiveles, LNivel7, CB_NIVEL7 );
     SetCampoNivel( 8, iNiveles, LNivel8, CB_NIVEL8 );
     SetCampoNivel( 9, iNiveles, LNivel9, CB_NIVEL9 );
     {$ifdef ACS}
     SetCampoNivel( 10, iNiveles, LNivel10, CB_NIVEL10 );
     SetCampoNivel( 11, iNiveles, LNivel11, CB_NIVEL11 );
     SetCampoNivel( 12, iNiveles, LNivel12, CB_NIVEL12 );
     {$endif}
end;

procedure TEditClasifTemporal_DevEx.SetCamposDerechos;
var
   iDerecho: integer;
   lVisible: boolean;

   procedure IncrementaControles( const lVisible: boolean );
   begin
        if lVisible then
           Inc( FControles );
   end;

begin
     FControles := 0;
{$ifdef SUPERVISORES}
     iDerecho := D_SUPER_CLASIFI_TEMP_NIVELES;
{$else}
     iDerecho := D_ASIS_DATOS_CLASIFI_TEMP_NIVELES;
{$endif}

     Nivel1.Visible := CB_NIVEL1.Visible;
     Nivel2.Visible := CB_NIVEL2.Visible;
     Nivel3.Visible := CB_NIVEL3.Visible;
     Nivel4.Visible := CB_NIVEL4.Visible;
     Nivel5.Visible := CB_NIVEL5.Visible;
     Nivel6.Visible := CB_NIVEL6.Visible;
     Nivel7.Visible := CB_NIVEL7.Visible;
     Nivel8.Visible := CB_NIVEL8.Visible;
     Nivel9.Visible := CB_NIVEL9.Visible;
     {$ifdef ACS}
     Nivel10.Visible := CB_NIVEL10.Visible;
     Nivel11.Visible := CB_NIVEL11.Visible;
     Nivel12.Visible := CB_NIVEL12.Visible;
     {$endif}

     Puesto.Visible := ZAccesosMgr.CheckDerecho( iDerecho, K_DERECHO_NIVEL_PUESTO );
     Clasificacion.Visible := ZAccesosMgr.CheckDerecho( iDerecho, K_DERECHO_NIVEL_CLASIFICACION );
     Turno.Visible := ZAccesosMgr.CheckDerecho( iDerecho, K_DERECHO_NIVEL_TURNO );
     if Nivel1.Visible then
     begin
          lVisible := ZAccesosMgr.CheckDerecho( iDerecho, K_DERECHO_NIVEL_1 );
          Nivel1.Visible := lVisible;
          IncrementaControles( lVisible );
     end;
     if Nivel2.Visible then
     begin
          lVisible := ZAccesosMgr.CheckDerecho( iDerecho, K_DERECHO_NIVEL_2 );
          Nivel2.Visible := lVisible;
          IncrementaControles( lVisible );
     end;
     if Nivel3.Visible then
     begin
          lVisible := ZAccesosMgr.CheckDerecho( iDerecho, K_DERECHO_NIVEL_3 );
          Nivel3.Visible := lVisible;
          IncrementaControles( lVisible );
     end;
     if Nivel4.Visible then
     begin
          lVisible := ZAccesosMgr.CheckDerecho( iDerecho, K_DERECHO_NIVEL_4 );
          Nivel4.Visible := lVisible;
          IncrementaControles( lVisible );
     end;
     if Nivel5.Visible then
     begin
          lVisible := ZAccesosMgr.CheckDerecho( iDerecho, K_DERECHO_NIVEL_5 );
          Nivel5.Visible := lVisible;
          IncrementaControles( lVisible );
     end;
     if Nivel6.Visible then
     begin
          lvisible := ZAccesosMgr.CheckDerecho( iDerecho, K_DERECHO_NIVEL_6 );
          Nivel6.Visible := lVisible;
          IncrementaControles( lVisible );
     end;
     if Nivel7.Visible then
     begin
          lvisible := ZAccesosMgr.CheckDerecho( iDerecho, K_DERECHO_NIVEL_7 );
          Nivel7.Visible := lVisible;
          IncrementaControles( lVisible );
     end;
     if Nivel8.Visible then
     begin
          lVisible := ZAccesosMgr.CheckDerecho( iDerecho, K_DERECHO_NIVEL_8 );
          Nivel8.Visible := lVisible;
          IncrementaControles( lVisible );
     end;
     if Nivel9.Visible then
     begin
          lVisible := ZAccesosMgr.CheckDerecho( iDerecho, K_DERECHO_NIVEL_9 );
          Nivel9.Visible := lVisible;
          IncrementaControles( lVisible );
     end;
     {$ifdef ACS}
     if Nivel10.Visible then
     begin
          lVisible := ZAccesosMgr.CheckDerecho( iDerecho, K_DERECHO_NIVEL_10 );
          Nivel10.Visible := lVisible;
          IncrementaControles( lVisible );
     end;
     if Nivel11.Visible then
     begin
          lVisible := ZAccesosMgr.CheckDerecho( iDerecho, K_DERECHO_NIVEL_11 );
          Nivel11.Visible := lVisible;
          IncrementaControles( lVisible );
     end;
     if Nivel12.Visible then
     begin
          lVisible := ZAccesosMgr.CheckDerecho( iDerecho, K_DERECHO_NIVEL_12 );
          Nivel12.Visible := lVisible;
          IncrementaControles( lVisible );
     end;
     {$endif}
end;

procedure TEditClasifTemporal_DevEx.CB_NIVEL12ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel12.FieldByName( 'TB_RELACIO' ).AsString ) and Nivel11.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL11' ).AsString := dmTablas.cdsNivel12.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditClasifTemporal_DevEx.CB_NIVEL11ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel11.FieldByName( 'TB_RELACIO' ).AsString ) and Nivel10.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL10' ).AsString := dmTablas.cdsNivel11.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditClasifTemporal_DevEx.CB_NIVEL10ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel10.FieldByName( 'TB_RELACIO' ).AsString ) and Nivel9.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL9' ).AsString := dmTablas.cdsNivel10.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditClasifTemporal_DevEx.CB_NIVEL9ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel9.FieldByName( 'TB_RELACIO' ).AsString ) and Nivel8.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL8' ).AsString := dmTablas.cdsNivel9.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditClasifTemporal_DevEx.CB_NIVEL8ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel8.FieldByName( 'TB_RELACIO' ).AsString ) and Nivel7.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL7' ).AsString := dmTablas.cdsNivel8.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditClasifTemporal_DevEx.CB_NIVEL7ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel7.FieldByName( 'TB_RELACIO' ).AsString ) and Nivel6.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL6' ).AsString := dmTablas.cdsNivel7.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditClasifTemporal_DevEx.CB_NIVEL6ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel6.FieldByName( 'TB_RELACIO' ).AsString ) and Nivel5.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL5' ).AsString := dmTablas.cdsNivel6.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditClasifTemporal_DevEx.CB_NIVEL5ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel5.FieldByName( 'TB_RELACIO' ).AsString ) and Nivel4.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL4' ).AsString := dmTablas.cdsNivel5.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditClasifTemporal_DevEx.CB_NIVEL4ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel4.FieldByName( 'TB_RELACIO' ).AsString ) and Nivel3.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL3' ).AsString := dmTablas.cdsNivel4.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditClasifTemporal_DevEx.CB_NIVEL3ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel3.FieldByName( 'TB_RELACIO' ).AsString ) and Nivel2.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL2' ).AsString := dmTablas.cdsNivel3.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditClasifTemporal_DevEx.CB_NIVEL2ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel2.FieldByName( 'TB_RELACIO' ).AsString ) and Nivel1.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL1' ).AsString := dmTablas.cdsNivel2.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

{$ifdef ACS}
{Este procedimiento sirve para eliminar cualquier OLDKEY de los lookups de niveles}
procedure TEditClasifTemporal_DevEx.LimpiaLookUpOlfKey;
begin
          CB_NIVEL1.ResetMemory;
          CB_NIVEL2.ResetMemory;
          CB_NIVEL3.ResetMemory;
          CB_NIVEL4.ResetMemory;
          CB_NIVEL5.ResetMemory;
          CB_NIVEL6.ResetMemory;
          CB_NIVEL7.ResetMemory;
          CB_NIVEL8.ResetMemory;
          CB_NIVEL9.ResetMemory;
          CB_NIVEL10.ResetMemory;
          CB_NIVEL11.ResetMemory;
          CB_NIVEL12.ResetMemory;
end;
{$endif}


procedure TEditClasifTemporal_DevEx.HabilitaControles;
begin
     inherited;
     EmpleadoLbl.Enabled := Inserting;
     CB_CODIGO.Enabled := Inserting;

end;

{$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
procedure TEditClasifTemporal_DevEx.CambiosVisuales;
begin
     Self.Width:= K_WIDTH_SMALLFORM;

     CB_CODIGO.Width := K_WIDTH_LOOKUP;
     CB_PUESTO.Width := K_WIDTH_LOOKUP;
     CB_CLASIFI.Width:= K_WIDTH_LOOKUP;
     CB_TURNO.Width :=  K_WIDTH_LOOKUP;

     CB_NIVEL1.Width := K_WIDTH_LOOKUP;
     CB_NIVEL1.WidthLlave := K_WIDTHLLAVE;
     CB_NIVEL2.Width := K_WIDTH_LOOKUP;
     CB_NIVEL2.WidthLlave := K_WIDTHLLAVE;
     CB_NIVEL3.Width := K_WIDTH_LOOKUP;
     CB_NIVEL3.WidthLlave := K_WIDTHLLAVE;
     CB_NIVEL4.Width := K_WIDTH_LOOKUP;
     CB_NIVEL4.WidthLlave := K_WIDTHLLAVE;
     CB_NIVEL5.Width := K_WIDTH_LOOKUP;
     CB_NIVEL5.WidthLlave := K_WIDTHLLAVE;
     CB_NIVEL6.Width := K_WIDTH_LOOKUP;
     CB_NIVEL6.WidthLlave := K_WIDTHLLAVE;
     CB_NIVEL7.Width := K_WIDTH_LOOKUP;
     CB_NIVEL7.WidthLlave := K_WIDTHLLAVE;
     CB_NIVEL8.Width := K_WIDTH_LOOKUP;
     CB_NIVEL8.WidthLlave := K_WIDTHLLAVE;
     CB_NIVEL9.Width := K_WIDTH_LOOKUP;
     CB_NIVEL9.WidthLlave := K_WIDTHLLAVE;
     CB_NIVEL10.Width := K_WIDTH_LOOKUP;
     CB_NIVEL10.WidthLlave := K_WIDTHLLAVE;
     CB_NIVEL11.Width := K_WIDTH_LOOKUP;
     CB_NIVEL11.WidthLlave := K_WIDTHLLAVE;
     CB_NIVEL12.Width := K_WIDTH_LOOKUP;
     CB_NIVEL12.WidthLlave := K_WIDTHLLAVE;
end;
{$IFEND}

procedure TEditClasifTemporal_DevEx.SetEditarSoloActivos;
begin
     CB_PUESTO.EditarSoloActivos := TRUE;
     CB_CLASIFI.EditarSoloActivos := TRUE;
     CB_TURNO.EditarSoloActivos := TRUE;
     CB_NIVEL1.EditarSoloActivos := TRUE;
     CB_NIVEL2.EditarSoloActivos := TRUE;
     CB_NIVEL3.EditarSoloActivos := TRUE;
     CB_NIVEL4.EditarSoloActivos := TRUE;
     CB_NIVEL5.EditarSoloActivos := TRUE;
     CB_NIVEL6.EditarSoloActivos := TRUE;
     CB_NIVEL7.EditarSoloActivos := TRUE;
     CB_NIVEL8.EditarSoloActivos := TRUE;
     CB_NIVEL9.EditarSoloActivos := TRUE;
     CB_NIVEL10.EditarSoloActivos := TRUE;
     CB_NIVEL11.EditarSoloActivos := TRUE;
     CB_NIVEL12.EditarSoloActivos := TRUE;
end;

end.
