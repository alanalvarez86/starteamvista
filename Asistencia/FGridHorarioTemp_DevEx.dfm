inherited GridHorarioTemp_DevEx: TGridHorarioTemp_DevEx
  Caption = 'Horario Temporal'
  ClientHeight = 324
  ClientWidth = 457
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 288
    Width = 457
    inherited OK_DevEx: TcxButton
      Left = 292
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 372
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 457
    inherited ValorActivo2: TPanel
      Width = 131
      inherited textoValorActivo2: TLabel
        Width = 125
      end
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 457
    Height = 238
    OnColExit = ZetaDBGridColExit
    Columns = <
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'AU_FECHA'
        ReadOnly = True
        Title.Caption = 'Fecha'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'HO_CODIGO'
        Title.Caption = 'Horario'
        Width = 45
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'HO_DESCRIP'
        ReadOnly = True
        Title.Caption = 'Descripci'#243'n'
        Width = 130
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AU_HOR_MAN'
        ReadOnly = True
        Title.Caption = 'Manual'
        Width = 40
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'AU_STATUS'
        Title.Caption = 'D'#237'a'
        Width = 85
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AU_HORASCK'
        ReadOnly = True
        Title.Caption = 'Horas'
        Width = 45
        Visible = True
      end>
  end
  object zCombo: TZetaDBKeyCombo [4]
    Left = 264
    Top = 160
    Width = 83
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    DropDownCount = 3
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 4
    TabStop = False
    Visible = False
    OnClick = zComboClick
    Items.Strings = (
      'Descanso')
    ListaFija = lfStatusAusencia
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'AU_STATUS'
    DataSource = DataSource
    LlaveNumerica = True
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DataSource: TDataSource
    Left = 340
    Top = 65
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
end
