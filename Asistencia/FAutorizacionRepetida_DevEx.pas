unit FAutorizacionRepetida_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ZetaDBTextBox, ZetaCommonClasses, ZetaCommonLists,
  Mask, ZetaNumero, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore, TressMorado2013, dxSkinsDefaultPainters, cxButtons,
  dxGDIPlusClasses;

type

  TAutorizacionRepetida_DevEx = class(TForm)
    Mensaje: TLabel;
    Image1: TImage;
    IgnoreBtn: TcxButton;
    ReplaceBtn: TcxButton;
    SumarBtn: TcxButton;
    EmpleadoLb: TLabel;
    FechaLb: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    AnteriorNu: TZetaNumero;
    NuevoNu: TZetaNumero;
    TipoAutLbl: TLabel;
    FechaLbl: TLabel;
    EmpleadoLbl: TLabel;
    AutorizaLb: TLabel;
    lblAprobacion: TLabel;
    AprobacionLb: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

    function ShowDlgAutorizaRepetida( iEmpleado: TNumEmp; Fecha: TDate; sAutoriza: String; Anterior, Nuevo, Aprobadas: Double; AprobadasPor: integer ): eOperacionConflicto;

var
  AutorizacionRepetida_DevEx: TAutorizacionRepetida_DevEx;

implementation

uses
    DAsistencia,
    ZetaCommonTools;

{$R *.DFM}

procedure TAutorizacionRepetida_DevEx.FormCreate(Sender: TObject);
begin
     HelpContext:= H22211_Existe_autorizacion_extras;
end;

function ShowDlgAutorizaRepetida( iEmpleado: TNumEmp; Fecha: TDate; sAutoriza: String; Anterior, Nuevo, Aprobadas: Double; AprobadasPor: integer ): eOperacionConflicto;
var
   lEnabled: boolean;
begin
     if ( AutorizacionRepetida_DevEx = nil ) then
     begin
          AutorizacionRepetida_DevEx := TAutorizacionRepetida_DevEx.Create( Application ); // Se destruye al Salir del Programa //
     end;

     with AutorizacionRepetida_DevEx do
     begin
          AprobacionLb.Visible := dmAsistencia.AprobarPrendido;
          with AprobacionLb do
          begin
               lblAprobacion.Visible := Visible;
               if Visible then
               begin
                    if ( AprobadasPor <> 0 ) then
                       Caption := Format( '%2.2n Horas Por Usuario %d', [ Aprobadas, AprobadasPor ] )
                    else
                        Caption := Format( '%2.2n Horas', [ Aprobadas ] );
                    lEnabled := dmAsistencia.TieneDerechoAprobar;
                    ReplaceBtn.Enabled := lEnabled;
                    SumarBtn.Enabled := lEnabled;
               end;
          end;
          EmpleadoLb.Caption:= IntToStr(iEmpleado);
          FechaLb.Caption:= FechaCorta( Fecha );
          AutorizaLb.Caption:= sAutoriza;
          AnteriorNu.Valor:= Anterior;
          NuevoNu.Valor:= Nuevo;

          ShowModal;
          case ModalResult of
               mrOk:  Result:= ocSustituir;
               mrYes: Result:= ocSumar;
          else Result:= ocIgnorar;
          end;
     end;
end;

procedure TAutorizacionRepetida_DevEx.FormShow(Sender: TObject);
begin
     IgnoreBtn.SetFocus;
end;

end.
