unit FClasifTemporal_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {ZBaseConsulta,} Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, ZBaseGridLectura_DevEx,
  ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  TressMorado2013, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,StdCtrls, Mask,
  ZetaFecha;

type
  TClasifTemporal_DevEx = class(TBaseGridLectura_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CB_PUESTO: TcxGridDBColumn;
    CB_CLASIFI: TcxGridDBColumn;
    CB_TURNO: TcxGridDBColumn;
    PanelBoton: TPanel;
    Panel1: TPanel;
    AsistenciaFechaZF: TZetaFecha;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AsistenciaFechaZFValidDate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    procedure AgregaNivelesAlGrid;
    procedure AgregaUsuariosAlGrid;
  protected
    procedure Connect; override;
    procedure Refresh;override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  ClasifTemporal_DevEx: TClasifTemporal_DevEx;

implementation

uses DCliente, DGlobal,
{$ifdef SUPERVISORES}
     DSuper,
{$else}
     DAsistencia,
{$endif}
     DSistema, ZGlobalTress, ZAccesosMgr, ZAccesosTress,
     ZetaCommonLists, ZetaTipoEntidad, ZetaCommonClasses,
     ZetaCommonTools, ZetaClientDataSet, FTressShell;

{$R *.DFM}

procedure TClasifTemporal_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
{$ifdef SUPERVISORES}
     HelpContext := H_Super_Clasificaciones_Temporales;
     Panel1.visible := false;
     PanelBoton.visible:=false;
{$else}
     HelpContext := H_Asistencia_Clasificaciones_Temporales;
{$endif}
     AgregaNivelesAlGrid;
     AgregaUsuariosAlGrid;
end;

procedure TClasifTemporal_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     dmCliente.cdsEmpleadoLookup.Conectar;
{$ifdef SUPERVISORES}
     with dmSuper do
{$else}
     with dmAsistencia do
{$endif}
     begin
          cdsClasifiTemp.Conectar;
          DataSource.DataSet := cdsClasifiTemp;
     end;
end;

procedure TClasifTemporal_DevEx.Refresh;
begin
     AsistenciaFechaZF.Valor :=  dmCliente.FechaAsistencia;
     TZetaClientDataSet( DataSource.DataSet ).Refrescar;
     DoBestFit;
end;

procedure TClasifTemporal_DevEx.Agregar;
begin
     TZetaClientDataSet( DataSource.DataSet ).Agregar;
end;

procedure TClasifTemporal_DevEx.Modificar;
begin
     TZetaClientDataSet( DataSource.DataSet ).Modificar;
end;

procedure TClasifTemporal_DevEx.Borrar;
begin
     TZetaClientDataSet( DataSource.DataSet ).Borrar;
     DoBestFit;
end;

procedure TClasifTemporal_DevEx.AgregaNivelesAlGrid;
var
   i, indexColumn : integer;
const
     widthColumnOptions =40;
begin
     with Global do
     begin
          for i := 1 to NumNiveles do
          begin
               with ZetaDBGridDBTableView do
               begin
                    CreateColumn;
                    indexColumn := ColumnCount-1;
                    Columns[indexColumn].DataBinding.FieldName :=  'CB_NIVEL' + IntToStr( i );
                    Columns[indexColumn].Caption := NombreNivel( i );
                    Columns[indexColumn].MinWidth :=  Canvas.TextWidth( Columns[indexColumn].Caption)+ 15;

               end;
          end;
     end;
end;

procedure TClasifTemporal_DevEx.AgregaUsuariosAlGrid;
var
   indexColumn : integer;
const
     widthColumnOptions =40;
begin
     with ZetaDBGridDBTableView do
     begin
          CreateColumn;
          indexColumn := ColumnCount-1;
          Columns[indexColumn].DataBinding.FieldName := 'US_DESCRIP';
          Columns[indexColumn].Caption := 'Captur�';
          Columns[indexColumn].MinWidth := Canvas.TextWidth( Columns[indexColumn].Caption)+ 15;
          Columns[indexColumn].Options.Grouping:=false;
     end;
     {
     with TColumn.Create( GridClasifi.Columns ) do
     begin
          FieldName := 'US_COD_OK';
          Title.Caption := 'Aprob�';
     end;
     }
end;

procedure TClasifTemporal_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
  inherited;
   AsistenciaFechaZF.Valor :=  dmCliente.FechaAsistencia;
    ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
  ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
  //CreaColumaSumatoria('US_DESCRIP', skCount );

end;

procedure TClasifTemporal_DevEx.AsistenciaFechaZFValidDate(
  Sender: TObject);
begin
  inherited;
   TressShell.AsistenciaFechavalid(AsistenciaFechaZF.Valor);
end;

procedure TClasifTemporal_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
 AsistenciaFechaZF.Valor :=  dmCliente.FechaAsistencia;
end;

end.
