unit FTipoAutoriza_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Buttons, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters,  Menus, cxButtons, cxContainer, cxEdit, cxListBox;

type
  TTipoAutoriza_DevEx = class(TForm)
    ListAuto: TcxListBox;
    OK_DevEx: TcxButton;
    Cancelar_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure ListAutoDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  function ShowTipoAutoriza( iTipo: Integer ): Integer;

var
  TipoAutoriza_DevEx: TTipoAutoriza_DevEx;

implementation

uses ZetaCommonLists;

{$R *.DFM}

function ShowTipoAutoriza( iTipo: Integer ): Integer;
begin
     if ( TipoAutoriza_DevEx = nil ) then
         TipoAutoriza_DevEx:= TTipoAutoriza_DevEx.Create( Application.MainForm );
     with TipoAutoriza_DevEx do
     begin
          ListAuto.ItemIndex := iTipo - 5;
          ShowModal;
          if ModalResult = mrOk then
             Result := TipoAutoriza_DevEx.ListAuto.ItemIndex + 5
          else
             Result := iTipo;
     end;
end;

procedure TTipoAutoriza_DevEx.FormCreate(Sender: TObject);
begin
     with ListAuto do
     begin
          Clear;
          Items.Add( 'X : ' + ObtieneElemento(lfAutorizaChecadas, Ord( acConGoce ) ) );
          Items.Add( 'Y : ' + ObtieneElemento(lfAutorizaChecadas, Ord( acSinGoce ) ) );
          Items.Add( 'E : ' + ObtieneElemento(lfAutorizaChecadas, Ord( acExtras ) ) );
          Items.Add( 'S : ' + ObtieneElemento(lfAutorizaChecadas, Ord( acDescanso ) ) );
          Items.Add( 'Q : ' + ObtieneElemento(lfAutorizaChecadas, Ord( acConGoceEntrada ) ) );
          Items.Add( 'R : ' + ObtieneElemento(lfAutorizaChecadas, Ord( acSinGoceEntrada ) ) );
          Items.Add( 'P : ' + ObtieneElemento(lfAutorizaChecadas, Ord( acPrepagFueraJornada ) ) );
          Items.Add( 'B : ' + ObtieneElemento(lfAutorizaChecadas, Ord( acPrepagDentroJornada ) ) );
     end;
end;

procedure TTipoAutoriza_DevEx.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if ( Key = Chr(VK_ESCAPE) ) then
        ModalResult:= mrCancel
     else if ( Key = Chr(VK_RETURN) ) and ( ActiveControl is TListBox ) then
        ModalResult:= mrOk
     else
         case Key of
              'X', 'x' : ListAuto.ItemIndex := Ord( acConGoce );
              'Y', 'y' : ListAuto.ItemIndex := Ord( acSinGoce );
              'E', 'e' : ListAuto.ItemIndex := Ord( acExtras );
              'S', 's' : ListAuto.ItemIndex := Ord( acDescanso );
              'Q', 'q' : ListAuto.ItemIndex := Ord( acConGoceEntrada );
              'R', 'r' : ListAuto.ItemIndex := Ord( acSinGoceEntrada );
              'P', 'p' : ListAuto.ItemIndex := Ord( acPrepagFueraJornada );
              'B', 'b' : ListAuto.ItemIndex := Ord( acPrepagDentroJornada );
         end;
end;

procedure TTipoAutoriza_DevEx.ListAutoDblClick(Sender: TObject);
begin
     ModalResult := mrOk;
end;


end.
