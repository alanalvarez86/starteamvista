unit ZetaBuscaDerechosAcceso_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons,
  ExtCtrls, ZetaClientDataset, ZetaBusqueda_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, cxControls, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxClasses, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons;

type
  TBusquedaDerechosAcceso_DevEx = class(TBusqueda_DevEx)
    SOURCE: TcxGridDBColumn;
    MODULO: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  BusquedaDerechosAcceso_DevEx: TBusquedaDerechosAcceso_DevEx;

function BuscaDerechosAccesoDialogo( LookupDataset: TZetaLookupDataset; var sKey, sDescription: String): Boolean;

implementation
uses ZetaCommonClasses;

{$R *.dfm}

function BuscaDerechosAccesoDialogo( LookupDataset: TZetaLookupDataset; var sKey, sDescription: String ): Boolean;
var
   oBusqueda: TBusqueda_DevEx;
begin
//   if ( Busqueda = nil ) then
//   Se agreg� un objeto TBusqueda para manejar multiples instancias
//   de la forma de busqueda

     oBusqueda := TBusquedaDerechosAcceso_DevEx.Create( Application );
     try
        with oBusqueda do
        begin
             Dataset := LookupDataset;
             Filtro := VACIO;
             Codigo := sKey;
             ShowModal;
             Result := ( ModalResult = mrOk );
             if Result then
             begin
                  sKey := Codigo;
                  sDescription := LookupDataset.GetDescription;
             end;
        end;
     finally
            oBusqueda.Free;
     end;
end;


procedure TBusquedaDerechosAcceso_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with DBGrid_DevExDBTableview do
     begin
     SOURCE.Index := 0;
     MODULO.index := 1 ;
     Codigo_DevEx.index := 2;
     DBGrid_DevExDBTableViewColumn2.index :=3;
          Columns[ 0 ].databinding.FieldName := 'AA_SOURCE';
          Columns[ 1 ].databinding.FieldName := 'MO_NOMBRE';
          Columns[ 2 ].databinding.FieldName := 'AX_NUMERO';
          Columns[ 3 ].databinding.FieldName := 'AA_DESCRIP';
     end;
end;

end.
