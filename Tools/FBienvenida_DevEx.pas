unit FBienvenida_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls,ZetaCommonLists,ZetaCommonClasses, dxSkinsCore,
  dxSkinsDefaultPainters, 
  cxLookAndFeels, dxSkinsForm, dxGDIPlusClasses, 
  TressMorado2013, cxGraphics, cxLookAndFeelPainters, Menus, cxButtons,
  cxControls, cxContainer, cxEdit, cxClasses, dxImageSlider, cxCheckBox,
  cxLabel;

type
  TZBienvenida_DevEx = class(TForm)
    Panel1: TPanel;
    NoMostrarPresentacion_DevEx: TcxCheckBox;
    Slider_DevEx: TdxImageSlider;
    ListaDipositivas_DevEx: TcxImageCollection;
    cxlblIndicaciones: TcxLabel;
    ListaDipositivas_DevExItem1: TcxImageCollectionItem;
    ListaDipositivas_DevExItem4: TcxImageCollectionItem;
    ListaDipositivas_DevExItem5: TcxImageCollectionItem;
    ListaDipositivas_DevExItem2: TcxImageCollectionItem;
    ListaDipositivas_DevExItem3: TcxImageCollectionItem;
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    TipoEntrada : Byte;
  end;

const
     ES_DEMO = 0;
     ES_KIT = 1;
var
   ZBienvenida_DevEx: TZBienvenida_DevEx;

procedure EntradaBienvenida_DevEx;

implementation

{$R *.DFM}

uses ZetaRegistryCliente;

procedure EntradaBienvenida_DevEx;
begin
     ZBienvenida_DevEx:= TZBienvenida_DevEx.Create( Application );
     try
        with ZBienvenida_DevEx do
        begin
             ShowModal;
             //DevEx(@am): Antes de cerrar la pantalla guardar el valor del checkbox en la variable del registry.
             //ClientRegistry.VerPresentacion := not(MostrarPresentacion_DevEx.Checked);
             if NoMostrarPresentacion_DevEx.Checked then
                ClientRegistry.VSPresentacion := VERSION_COMPILACION;
        end;
     finally
            FreeAndNil( ZBienvenida_DevEx );
     end;
end;

procedure TZBienvenida_DevEx.FormShow(Sender: TObject);
begin
     //
end;

procedure TZBienvenida_DevEx.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     case key of
       VK_ESCAPE: Close;
     end;
end;

end.
