unit FSeleccionarConfidencialidad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseDlgModal, StdCtrls, CheckLst, Buttons, ExtCtrls,
  ZetaCommonTools;

type
  TSeleccionarConfidencialidad = class(TZetaDlgModal)
    CM_NIVEL0: TCheckListBox;
  private
    { Private declarations }
    procedure SetListConfidencialidad( sValores : string;  sConfidencialidadEmpresa : string = '' );
    function GetListConfidencialidad : string;
  public
    { Public declarations }
    function GetConfidencialidad( sCM_NIVEL0 : string;  lValores : TStringList ; lFromTressCatalogo : boolean = FALSE; sConfidencialidadEmpresa : string = '' ) : string;

  end;

var
  SeleccionarConfidencialidad: TSeleccionarConfidencialidad;

implementation

{$R *.dfm}

{ TSeleccionarConfidencialidad }

procedure TSeleccionarConfidencialidad.SetListConfidencialidad( sValores : string ; sConfidencialidadEmpresa : string );
var
   Lista : TStringList;
   ListaEmpresa : TStringList;
   i, j, idx : integer;
   sNivel0: string;
begin
    Lista := TStringList.Create;
    ListaEmpresa :=TStringList.Create;

    sValores := Trim( sValores );
    if StrLleno( sValores ) then
        Lista.CommaText  := sValores;

    sValores := Trim( sConfidencialidadEmpresa );
    if StrLleno( sValores ) then
        ListaEmpresa.CommaText  := sValores;


    for i:= 0 to CM_NIVEL0.Items.Count - 1 do
    begin

        CM_NIVEL0.Checked[i] := FALSE;
        sNivel0 :=  Trim( CM_NIVEL0.Items.Names[i] );

        if ( Lista.Count > 0 ) then
        begin
                j := Lista.IndexOf( sNivel0  );
                CM_NIVEL0.Checked[i] :=  ( j >= 0 );
        end;

        CM_NIVEL0.ItemEnabled[i] := TRUE;

        if Strlleno( sConfidencialidadEmpresa ) then
        begin
              CM_NIVEL0.ItemEnabled[i] :=  ListaEmpresa.Find(sNivel0,idx);
        end;


    end;

    FreeAndNil( Lista );
    FreeAndNil( ListaEmpresa );

end;

function TSeleccionarConfidencialidad.GetListConfidencialidad : string;
var
   i : integer;
begin
    Result := '';

    for i:= 0 to CM_NIVEL0.Items.Count - 1 do
    begin
        if CM_NIVEL0.Checked[i] then
        begin
             if StrLleno( Result ) then
                Result := Result + ',';
             Result := Result +  Trim( CM_NIVEL0.Items.Names[i] );
        end;
    end;

end;




function TSeleccionarConfidencialidad.GetConfidencialidad(
  sCM_NIVEL0: string;   lValores : TStringList;  lFromTressCatalogo : boolean = FALSE; sConfidencialidadEmpresa : string = '' ): string;
begin
    Result := sCM_NIVEL0;

    CM_NIVEL0.Items := lValores;
    if (  lFromTressCatalogo ) then
       SetListConfidencialidad( sCM_NIVEL0, sConfidencialidadEmpresa)
    else
        SetListConfidencialidad( sCM_NIVEL0 ) ;

    ShowModal;
    if ( ModalResult = mrOk ) then
    begin
         try
             Result := GetListConfidencialidad;
         except
               on Error: Exception do
               begin
                    Application.HandleException( Error );
               end;
         end;
    end;
end;

end.
