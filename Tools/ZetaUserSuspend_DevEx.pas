unit ZetaUserSuspend_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask,
  {$ifndef VER130}MaskUtils,{$endif}
  StdCtrls, Buttons, ExtCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, cxButtons, dxGDIPlusClasses;

type
  TZUserSuspend_DevEx = class(TForm)
    Advertencia: TMemo;
    Image1: TImage;
    LblClave: TLabel;
    PassWord: TMaskEdit;
    ContinuarBtn_DevEx: TcxButton;
    OtroBtn_DevEx: TcxButton;
    SalirBtn_DevEx: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure ContinuarBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FSegundos : Integer;
    Intentos : Integer;
    procedure SetAdvertencia;
    procedure SetBotones;
  public
    { Public declarations }
    property Segundos : Integer read FSegundos write FSegundos;
  end;

const
     MAX_INTENTOS = 3;
     LEFT_CONTINUAR = 43;
     LEFT_SALIR = 200;

var
  ZUserSuspend_DevEx: TZUserSuspend_DevEx;

implementation

uses dCliente, ZetaCommonTools;

{$R *.DFM}

procedure TZUserSuspend_DevEx.FormCreate(Sender: TObject);
begin
     SetBotones;
end;

procedure TZUserSuspend_DevEx.FormShow(Sender: TObject);
begin
     SetAdvertencia;
     Password.Text := '';
     ActiveControl := Password;
     Intentos := 0;
end;

procedure TZUserSuspend_DevEx.SetAdvertencia;
begin
     with Advertencia.Lines do
     begin
          Clear;
          Add( Format( '%s, Por su Seguridad,', [ dmCliente.GetDatosUsuarioActivo.NombreCorto ] ) );
          Add( Format( 'como Pasaron %d Segundos sin uso,', [ Segundos ] ) );
          Add( 'el Sistema ha sido Suspendido.' );
     end;
end;

procedure TZUserSuspend_DevEx.ContinuarBtnClick(Sender: TObject);

   function GetMaxIntentos: Integer;
   begin
        with dmCliente.DatosSeguridad do
             if ( PasswordIntentos > 0 ) then
                Result := PasswordIntentos
             else
                Result := MAX_INTENTOS;
   end;

begin
     Inc( Intentos );
     if ( dmCliente.PasswordUsuario = PassWord.Text ) then
        ModalResult := mrYes
     else
     begin
          Password.Text := '';
          PassWord.SetFocus;
          Beep;
          if ( Intentos = GetMaxIntentos ) then
             ModalResult := mrNo;
     end;
end;

procedure TZUserSuspend_DevEx.SetBotones;
begin
     OtroBtn_DevEx.Caption := dmCliente.CaptionLogoff;
     if strVacio( OtroBtn_DevEx.Caption ) then          // Si se usa el Bot�n ya est�n acomodados
     begin
          OtroBtn_DevEx.Visible := FALSE;
          ContinuarBtn_DEvEx.Left := LEFT_CONTINUAR;
          SalirBtn_DevEx.Left := LEFT_SALIR;
     end;
end;

end.
