unit ZBasicoSelectGrid;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, Grids, DBGrids, Db,
     ZBaseDlgModal,
     ZetaMessages,
     ZetaClientDataset,
     ZetaCommonClasses,
     ZetaDBGrid;

type
  TBasicoGridSelect = class(TZetaDlgModal)
    ZetaDBGrid: TZetaDBGrid;
    DataSource: TDataSource;
    Excluir: TBitBtn;
    Imprimir: TBitBtn;
    Revertir: TBitBtn;
    Exportar: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure ExcluirClick(Sender: TObject);
    procedure ZetaDBGridCellClick(Column: TColumn);
    procedure ImprimirClick(Sender: TObject);
    procedure RevertirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ExportarClick(Sender: TObject);
  private
    { Private declarations }
    FDataset: TZetaClientDataset;
    FFiltro : String;
    FHayFiltro : Boolean;
    function GetDataset: TZetaClientDataset;
    procedure Exclude;
    procedure SetDataset( Value: TZetaClientDataset );
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    procedure SetControls;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property Dataset: TZetaClientDataset read GetDataset write SetDataset;
    property Filtro: String read FFiltro write FFiltro;
    property HayFiltro: Boolean read FHayFiltro write FHayFiltro;
  end;
  TBasicoGridSelectClass = class of TBasicoGridSelect;

var
  BasicoGridSelect: TBasicoGridSelect;
  ParametrosGrid: TzetaParams;

function GridSelectBasico( ZetaDataset: TZetaClientDataset; GridSelectClass: TBasicoGridSelectClass;
                           ValidarCambios: Boolean = TRUE ): Boolean;

implementation

uses ZetaDialogo,
     ZetaCommonLists,
     FBaseReportes;

{$R *.DFM}

function GridSelectBasico( ZetaDataset: TZetaClientDataset; GridSelectClass: TBasicoGridSelectClass;
                           ValidarCambios: Boolean = TRUE ): Boolean;
begin
     if ZetaDataset.IsEmpty then
     begin
          ZetaDialogo.zInformation( '� Atenci�n !', 'La Lista A Verificar Est� Vac�a', 0 );
          Result := False;
     end
     else
     begin
          with GridSelectClass.Create( Application ) as TBasicoGridSelect do
          begin
               try
                  Dataset := ZetaDataset;
                  try
                     with Dataset do
                     begin
                          HayFiltro := Filtered;
                          Filtro := Filter;
                     end;
                     ShowModal;
                  finally
                         with Dataset do
                         begin
                              if ( Filter <> Filtro ) or ( HayFiltro <> Filtered ) then
                              begin
                                   Filter := Filtro;
                                   Filtered := HayFiltro;
                              end;
                         end
                  end;
{
                  ShowModal;
}
                  Result := ( ModalResult = mrOk ) and ( ( not ValidarCambios ) or ( ZetaDataset.ChangeCount > 0 ) );
                  Dataset := nil;
               finally
                      Free;
               end;
          end;
     end;
end;

{ *********** TBaseGridSelect *********** }

procedure TBasicoGridSelect.FormShow(Sender: TObject);
begin
     inherited;
     SetControls;
end;

function TBasicoGridSelect.GetDataset: TZetaClientDataset;
begin
     Result := FDataset;
end;

procedure TBasicoGridSelect.SetDataset( Value: TZetaClientDataset );
begin
     if ( FDataset <> Value ) then
     begin
          FDataset := Value;
          if ( Value <> nil ) then
             Datasource.Dataset := Value;
     end;
end;

procedure TBasicoGridSelect.SetControls;
begin
     //Excluir.Enabled := ( ZetaDBGrid.SelectedRows.Count > 0 );
     Revertir.Enabled := ( FDataset.ChangeCount > 0 );
end;

procedure TBasicoGridSelect.Exclude;
begin
     with ZetaDBGrid do
     begin
          if ( SelectedRows.Count > 0 ) then
          begin
               with FDataset do
               begin
                    DisableControls;
                    SelectedRows.Delete;
                    EnableControls;
               end;
          end
          else if ( not FDataSet.IsEmpty ) then
              FDataSet.Delete;
     end;
     SetControls;
end;

procedure TBasicoGridSelect.WMExaminar(var Message: TMessage);
begin
     Exclude;
end;

procedure TBasicoGridSelect.ExcluirClick(Sender: TObject);
begin
     inherited;
     Exclude;
     SetControls;
end;

procedure TBasicoGridSelect.RevertirClick(Sender: TObject);
begin
     inherited;
     with FDataset do
     begin
          DisableControls;
          UndoLastChange( True );
          EnableControls;
     end;
     SetControls;
end;

procedure TBasicoGridSelect.ZetaDBGridCellClick(Column: TColumn);
begin
     inherited;
     SetControls;
end;

procedure TBasicoGridSelect.ImprimirClick(Sender: TObject);
begin
     inherited;
     if ZetaDialogo.zConfirm( 'Imprimir...', '� Desea Imprimir La Lista Mostrada ?', 0, mbYes ) then
        FBaseReportes.ImprimirGridParams( ZetaDBGrid, ZetaDBGrid.DataSource.DataSet, Caption, 'IM', ParametrosGrid);
end;

procedure TBasicoGridSelect.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H95002_Lista_de_empleados;
end;

procedure TBasicoGridSelect.ExportarClick(Sender: TObject);
begin
     inherited;
     if ZetaDialogo.zConfirm( 'Exportar...', '� Desea exportar a Excel la lista mostrada ?', 0, mbYes ) then
        FBaseReportes.ExportarGridParams( ZetaDBGrid, ZetaDBGrid.DataSource.DataSet, ParametrosGrid, Caption, 'IM');
end;

end.
