unit FBaseGridLayaout;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ZBaseConsulta, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, VclTee.TeeGDIPlus, VCLTee.TeEngine, VCLTee.TeeProcs, VCLTee.Chart,
  MultiMon;

type
  TBaseGridLayaout = class(TBaseConsulta)
    GridPanelBase: TGridPanel;
    procedure FormCreate(Sender: TObject);
    procedure GridPanelBaseResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function IsFUllHD: Boolean;
    procedure AplicarMinimos;
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure WMDisplayChange(var Message: TWMDisplayChange); message WM_DISPLAYCHANGE;
  end;

const
 tColoresGrafica : array [0..15] of string = (
                '$00D8AA7F',
                '$0000B2E9',
                '$004858CC',
                '$0050D293',
                '$003399FF',
                '$005D4E64',
                '$00DCAB00',
                '$00738D2E',
                '$004B3B4D',
                '$008162F5',
                '$00566FF3',
                '$008B818C',
                '$003F21C3',
                '$00D1D600',
                '$00CDD768',
                '$00A494C2'
              );

     tColores : array [0..0] of string = (
              '$00F3F3F3' //Gris de Background
              );
var
  BaseGridLayaout: TBaseGridLayaout;

implementation

{$R *.dfm}

procedure TBaseGridLayaout.Connect;
begin
end;

procedure TBaseGridLayaout.FormCreate(Sender: TObject);
begin
     inherited;
     //Colores TPanel
     ParentColor := False;
     GridPanelBase.ParentColor := False;
     GridPanelBase.Color := StringToColor(tColores[0]);
     Self.Color := StringToColor(tColores[0]);
     GridPanelBase.Update;
end;

procedure TBaseGridLayaout.GridPanelBaseResize(Sender: TObject);
begin
     inherited;
     GridPanelBase.Update;
     AplicarMinimos;
end;

procedure TBaseGridLayaout.Refresh;
begin

end;

function TBaseGridLayaout.IsFUllHD: Boolean;
var
  MonInfo: TMonitorInfo;
  iWidthResolucion: Integer;//1920px
  iHeightResolucion: Integer;//1080px
begin
    MonInfo.cbSize := SizeOf(MonInfo);
    GetMonitorInfo(MonitorFromWindow(  Handle, MONITOR_DEFAULTTONEAREST), @MonInfo);
    iWidthResolucion := MonInfo.rcMonitor.Right - MonInfo.rcMonitor.Left;
    iHeightResolucion:= MonInfo.rcMonitor.Bottom - MonInfo.rcMonitor.Top;
    Result := False;
    if ( iWidthResolucion = 1920 ) and ( iHeightResolucion = 1080 ) then
       Result := True;
end;

procedure TBaseGridLayaout.AplicarMinimos;
begin
     if IsFUllHD then
     begin
          Self.GridPanelBase.Constraints.MinHeight := 513;
          Self.GridPanelBase.Constraints.MinWidth := 800;
     end
     else
     begin
          Self.GridPanelBase.Constraints.MinHeight := 423;
          Self.GridPanelBase.Constraints.MinWidth := 748;
     end;
end;

procedure TBaseGridLayaout.WMDisplayChange(var Message: TWMDisplayChange);
begin
//     ShowMessageFmt('The screen resolution has changed to %d�%d�%d.',
//     [Message.Width, Message.Height, Message.BitsPerPixel]);
end;

end.
