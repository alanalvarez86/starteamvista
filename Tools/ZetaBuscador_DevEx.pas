unit ZetaBuscador_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, DB,
     ZetaClientDataset,
     ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore,  
  TressMorado2013, dxSkinsDefaultPainters,
  cxButtons;

type
  TBuscador_DevEx = class(TZetaDlgModal_DevEx)
    CodigoLBL: TLabel;
    Codigo: TEdit;
    NoExisteLBL: TLabel;
    procedure CodigoChange(Sender: TObject);
    procedure CodigoKeyPress(Sender: TObject; var Key: Char);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FEncontrado: boolean;
    FCampo: String;
    FDataset: TZetaClientDataset;
    function Buscar: Boolean;
    procedure SetCampo( const Value: String );
    procedure SetNombreLlave( const Value: String );
    procedure SetNombreTabla(const Value: String);
  public
    { Public declarations }
    property Campo: String read FCampo write SetCampo;
    property Dataset: TZetaClientDataset read FDataset write FDataset;
    property NombreLlave: String write SetNombreLlave;
    property NombreTabla: String write SetNombreTabla;
    property Encontrado: boolean read FEncontrado;
  end;

//function BuscarCodigoValor( const sLlave, sTabla, sCampo: String; Tabla: TZetaClientDataset; var sValor: string ): boolean;
procedure BuscarCodigo( const sLlave, sTabla, sCampo: String; Tabla: TZetaClientDataset );

var
  Buscador_DevEx: TBuscador_DevEx;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo,
     ZetaBusqueda_DevEx;

{$R *.DFM}

procedure BuscarCodigo( const sLlave, sTabla, sCampo: String; Tabla: TZetaClientDataset );
var
   skey, sDescription: String;
   cdsLookupDataSet: TZetaLookupDataSet;
begin
     if Tabla.IsEmpty then
        ZetaDialogo.zInformation( 'B�squeda de ' + sLlave, 'No Hay Datos Para Buscar', 0 )
     else
     begin
          if Tabla is TZetaLookupDataSet then
          begin
               cdsLookupDataSet := TZetaLookupDataSet.Create( nil );
               try
                  with TZetaLookupDataSet( Tabla ) do
                  begin
                       cdsLookupDataSet.Data := Data;
                       cdsLookupDataSet.LookupName := LookupName;
                       cdsLookupDataSet.LookupKeyField := LookupKeyField;
                       cdsLookupDataSet.LookupDescriptionField := LookupDescriptionField;
                       cdsLookupDataSet.LookupActivoField := LookupActivoField;
                       cdsLookupDataSet.LookupConfidenField := LookupConfidenField;
                       cdsLookupDataSet.OnLookupDescription := OnLookupDescription;
                  end;
                  if ZetaBusqueda_DevEx.ShowSearchForm( cdsLookupDataSet, VACIO, sKey, sDescription, FALSE, FALSE ) then
                  begin
                       if ( StrLLeno ( sKey ) ) then
                          Tabla.Locate( TZetaLookupDataSet( Tabla ).LookupKeyField, sKey, [] )
                  end;
               finally
                      FreeAndNil( cdsLookupDataSet );
               end;
          end
          else
          begin
               with TBuscador_DevEx.Create( Application ) do
               begin
                    try
                       Dataset := Tabla;
                       NombreLlave := sLlave;
                       NombreTabla := sTabla;
                       Campo := sCampo;
                       ShowModal;
                    finally
                           Free;
                    end;
               end;
          end;
     end;
end;
{
function BuscarCodigoValor( const sLlave, sTabla, sCampo: String; Tabla: TZetaClientDataset; var sValor: string ): boolean;
begin
     Result := False;

     if Tabla.IsEmpty then
        ZetaDialogo.zInformation( 'B�squeda de ' + sLlave, 'No Hay Datos Para Buscar', 0 )
     else
     begin
          with TBuscador.Create( Application ) do
          begin
               try
                  begin
                       Campo := sCampo;
                       Dataset := Tabla;
                       NombreLlave := sLlave;
                       NombreTabla := sTabla;
                       //ShowModal;
                       Result := Encontrado;
                       if ( Result ) then
                       sValor := DataSet.FieldByName( FCampo ).AsString
                       else
                       sValor := VACIO;
                  end;
                  if ( Tabla is TZetaLookupDataSet) then
                  begin
                       ZetaBusqueda_DevEx.ShowSearchForm( TZetaLookupDataSet( DataSet ), VACIO, VACIO, VACIO, FALSE );
                  end else
                  begin
                       ShowModal;
                  end;

               finally
                      Free;
               end;
          end;
     end;
end;
}
{ ******** TBuscador_DevEx ******* }

procedure TBuscador_DevEx.SetCampo(const Value: String);
begin
     FCampo := Value;
end;

procedure TBuscador_DevEx.SetNombreLlave(const Value: String);
begin
     CodigoLBL.Caption := Value + ':';
end;

procedure TBuscador_DevEx.SetNombreTabla(const Value: String);
begin
     Self.Caption := 'Buscar ' + Value;
end;

procedure TBuscador_DevEx.CodigoChange(Sender: TObject);
begin
     inherited;
     NoExisteLBL.Visible := False;
end;

function TBuscador_DevEx.Buscar: Boolean;
begin
     Result := Dataset.Locate( FCampo, Codigo.Text, [ loCaseInsensitive, loPartialKey ] );
end;

procedure TBuscador_DevEx.CodigoKeyPress(Sender: TObject; var Key: Char);
begin
     if ( Key = ZetaCommonClasses.UnaCOMILLA ) then
        Key := Chr( 0 );
     inherited KeyPress( Key );
end;

procedure TBuscador_DevEx.OK_DevExClick(Sender: TObject);
begin
  inherited;
  FEncontrado := Buscar;
     if FEncontrado then
        Close
     else
     begin
          ZetaCommonTools.Chicharra;
          NoExisteLBL.Visible := True;
          ActiveControl := Codigo;
     end;
end;

end.
