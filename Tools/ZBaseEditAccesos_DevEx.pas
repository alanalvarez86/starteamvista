unit ZBaseEditAccesos_Devex;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, ImgList, Forms,
     Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, Math, ShellAPI, DBClient,
     DBaseSistema,
     ZAccesosMgr,
     ZetaDBTextBox,
     DB, ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxButtons, cxControls,
  cxContainer, cxEdit, cxTreeView, dxSkinscxPCPainter, cxPCdxBarPopupMenu,
  cxPC;

const
     K_UN_CHECK = 1;
     K_CHECK = 2;
     D_TEXT_CONSULTA = 'Consultar';
     D_TEXT_ALTA = 'Agregar';
     D_TEXT_BAJA = 'Borrar';
     D_TEXT_EDICION= 'Modificar';
     D_TEXT_IMPRESION = 'Imprimir';

type
  eDerecho = ( edConsulta, edAlta, edBaja, edModificacion, edImpresion, edBorraSistKardex, edBanca, edNivel0, edConfigurar, edAdicional9, edAdicional10, edAdicional11, edAdicional12, edAdicional13, edAdicional14, edAdicional15 );
  eTipoNodo = ( tpUnDerecho, tpConDerechos, tpEspecial );
  TPosicionDerecho = Word;
  TBuscaDerecho = function( const iIndex: Integer ): TDerechosAcceso of object;

  TZetaEditAccesos_DevEx = class(TZetaDlgModal_DevEx)
    PanelTitulo: TPanel;
    CompanyLBL: TLabel;
    GrupoLBL: TLabel;
    Grupo: TZetaTextBox;
    Company: TZetaTextBox;
    ArbolImages: TImageList;
    PanelTools: TPanel;
    Prender: TcxButton;
    PrenderTodos: TcxButton;
    ApagarTodos: TcxButton;
    Expander: TcxButton;
    Compactar: TcxButton;
    Buscar: TcxButton;
    Copiar: TcxButton;
    Exportar: TcxButton;
    DlgSaveFile: TSaveDialog;
    Imprimir: TcxButton;
    PageControl: TcxPageControl;
    tsTress: TcxTabSheet;
    ArbolBase: TcxTreeView;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure ExpanderClick(Sender: TObject);
    procedure CompactarClick(Sender: TObject);
    procedure ArbolBaseKeyPress(Sender: TObject; var Key: Char);
    procedure ArbolBaseMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure PrenderTodosClick(Sender: TObject);
    procedure ApagarTodosClick(Sender: TObject);
    procedure PrenderClick(Sender: TObject);
    procedure BuscarClick(Sender: TObject);
    procedure CopiarClick(Sender: TObject);
    procedure ExportarClick(Sender: TObject);
    procedure ImprimirClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  
  private
    { Private declarations }
    FSoloLectura: Boolean;
    FTextoGet: Boolean;
    FTexto: String;
    FElemento: TTreeNode;
    function GetDerechosNodo(Nodo: TTreeNode; const Derecho: EDerecho): TDerechosAcceso;
    function GetDerechos(Nodo: TTreeNode): TDerechosAcceso;
    function GetDerechosPadre(Nodo: TTreeNode): TDerechosAcceso;
    function GetDerechosEspeciales(Nodo: TTreeNode): TDerechosAcceso;
    function GetTipoNodo(Nodo: TTreeNode): eTipoNodo;
    function PesoDerecho(const iPosicionDerecho: TPosicionDerecho): TDerechosAcceso;
    function TieneDerecho(const iValorDerecho: TDerechosAcceso; const iPosicionDerecho: TPosicionDerecho): Boolean;
    //function ExecuteFile( const FileName, Params, DefaultDir: String ): THandle;
    procedure CambiaImagen(Nodo: TTreeNode; const iImagen: Integer);
    procedure CambioArbol;
    procedure InitArbolStates(const iImagen: Integer);
    procedure SetDerechosEspeciales(Nodo: TTreeNode; const iDerecho: TDerechosAcceso);
    procedure SetDerechosHijos(Nodo: TTreeNode; const iDerecho: TDerechosAcceso);
    procedure SetDerechosPadres(Nodo: TTreeNode; const iDerecho: TDerechosAcceso);
    procedure SetSoloLectura(const Value: Boolean);
    procedure SetControl(Control: TControl; const lEnabled: Boolean);
    procedure TextoAdd( Nodo: TTreeNode; const iPos: Integer; const eTipo: eTipoNodo );
    procedure TextoInit;
    function GetTextoBitacora( Nodo: TTreeNode; const iDerecho: TDerechosAcceso ): String;
    function GetArbolSeleccionado: eArbolDerechos;
    procedure SetArbolSeleccionado(const Value: eArbolDerechos);


  protected
    { Protected declarations }
    FEmpresaCopiado : string;
    FGrupoCopiado: integer;
    procedure Cargar;virtual;
    procedure SetEditState;
    procedure RefrescaArbol( Metodo: TBuscaDerecho );
    function EsDerechoEspecial(const iIndex: Integer): Boolean; virtual;
    function GetDerechosHijos(Nodo: TTreeNode): TDerechosAcceso;virtual;
    //function GetConstantAsText(const iValor: Integer): String; virtual;
    procedure SetDerechos(Nodo: TTreeNode; const iDerecho: TDerechosAcceso);
    function GetTipoDerecho(Nodo: TTreeNode): eDerecho; virtual;
    function NodoEspecial( const iIndex: Integer ): eTipoNodo; virtual;
    function TextoEspecial(const sText: String): Boolean; virtual;
    procedure AddElemento( const iNivel, iDerecho: Integer; const sTexto: String );
    procedure AddElementoYDerechos( const iNivel, iDerecho: Integer; const sTexto: String );
    procedure AddDerecho( const sTexto: String );
    procedure AddDerechoAlta;
    procedure AddDerechoBaja;
    procedure AddDerechoConsulta;
    procedure AddDerechoEdicion;
    procedure AddDerechoImpresion;
    procedure AddDerechosTodos;
    function GetArbol:TcxTreeView;virtual;
    property Arbol: TcxTreeView read GetArbol;
    property Elemento: TTreeNode read FElemento;
    procedure ArbolConstruir;
    procedure ArbolDefinir; virtual;
    function GetImageIndex: integer;virtual;
    function GetImageIndexLimit: integer;virtual;
    property ArbolSeleccionado: eArbolDerechos read GetArbolSeleccionado write SetArbolSeleccionado;
    procedure SetDerechosSinValor; virtual;
    procedure CopiaDerechos;virtual;
    procedure CambiosAdicionales; virtual;

  public
    { Public declarations }
    property SoloLectura: Boolean read FSoloLectura write SetSoloLectura;
    procedure DatasetDerechosCargar( Dataset: TClientDataset; const lCargaTodos: Boolean );
    procedure Descargar( TopNodo:TTreeNode = NIL; LastNodo:TTreeNode = NIL);
  end;

var
  ZetaEditAccesos_DevEx: TZetaEditAccesos_DevEx;

implementation

uses DSistema,
     ZAccesosTress,
     FEscogeGrupoEmpresa_DevEx,
     //FAccesosRTF,
     ZetaCommonTools,
     //ZetaWinAPITools,
     ZetaCommonClasses,
     ZetaDialogo,
     ZArbolFind_DevEx,
     ZBasePrintAccesos,     
     DCliente;



{$R *.DFM}

{ ************* TSistEditAccesos ************** }

procedure TZetaEditAccesos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H80812_Grupos_usuarios;
     FSoloLectura := False;
     FTextoGet := False;
     TextoInit;
end;

procedure TZetaEditAccesos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Cargar;
     SetEditState;
end;

procedure TZetaEditAccesos_DevEx.SetControl( Control: TControl; const lEnabled: Boolean );
begin
     with Control do
     begin
          Enabled := lEnabled;
          Visible := lEnabled;
     end;
end;

procedure TZetaEditAccesos_DevEx.SetSoloLectura(const Value: Boolean);
begin
     FSoloLectura := Value;
     SetControl( OK_DevEx, not FSoloLectura );
     SetControl( Copiar, not FSoloLectura );
     SetControl( Prender, not FSoloLectura );
     SetControl( PrenderTodos, not FSoloLectura );
     SetControl( ApagarTodos, not FSoloLectura );
     with Cancelar_DevEx do
     begin
          if FSoloLectura then
          begin
               modalResult := MrAbort;
               Caption := '&Salir';
          end;
     end;
end;

procedure TZetaEditAccesos_DevEx.SetEditState;
begin
     OK_DevEx.Enabled := True;
end;

procedure TZetaEditAccesos_DevEx.CambiaImagen( Nodo: TTreeNode; const iImagen: Integer );
begin
     Nodo.StateIndex := iImagen;
end;

procedure TZetaEditAccesos_DevEx.InitArbolStates( const iImagen: Integer );
var
   Nodo: TTreeNode;
begin
     with Arbol do
     begin
          Items.BeginUpdate;
          Nodo := Items[ 0 ];
          repeat
                CambiaImagen( Nodo, iImagen );
                Nodo := Nodo.GetNext;
          until ( Nodo = nil );
          Items.EndUpdate;
     end;
     SetEditState;
end;

procedure TZetaEditAccesos_DevEx.CambioArbol;

procedure ApagaHijos( Nodo: TTreeNode );
begin
     if Nodo.HasChildren then
     begin
          Nodo := Nodo.Item[ 0 ];
          repeat
                CambiaImagen( Nodo, Nodo.Parent.StateIndex );
                if Nodo.HasChildren then
                   ApagaHijos( Nodo );
                Nodo := Nodo.GetNextSibling;
          until ( Nodo = nil );
     end;
end;

procedure PrendeParent( Nodo: TTreeNode );

   function TieneDerechosHijos( Nodo: TTreeNode ): Boolean;
   begin
        Result:= TRUE;
        if Nodo.HasChildren then
        begin
             Nodo := Nodo.Item[ 0 ];
             repeat
                   Result:= ( Nodo.StateIndex = K_CHECK );
{
                   if Nodo.HasChildren then
                      TieneDerechosHijos( Nodo );
}
                   Nodo := Nodo.GetNextSibling;
             until ( Result ) or ( Nodo = nil );
        end;
   end;

begin
     if ( Nodo.StateIndex = K_CHECK ) then
     begin
          while ( Nodo.Parent <> nil ) do
          begin
               Nodo := Nodo.Parent;
               CambiaImagen( Nodo, K_CHECK );
          end;
     end
     else
     begin
          while ( Nodo.Parent <> nil ) do
          begin
               Nodo:= Nodo.Parent;
               if not TieneDerechosHijos(Nodo) then
                 CambiaImagen( Nodo, K_UN_CHECK );
          end;
     end;
end;

begin
     with Arbol do
     begin
          if ( Selected <> nil ) then
          begin
               Items.BeginUpdate;
               if ( Selected.StateIndex = K_CHECK ) then
                  CambiaImagen( Selected, K_UN_CHECK )
               else
                   CambiaImagen( Selected, K_CHECK );
               ApagaHijos( Selected );
               PrendeParent( Selected );
               Items.EndUpdate;
               SetEditState;
          end;
     end;
end;

function TZetaEditAccesos_DevEx.TextoEspecial( const sText: String ): Boolean;
begin
     Result := False;
end;

function TZetaEditAccesos_DevEx.NodoEspecial( const iIndex: Integer ): eTipoNodo;
begin
     Result := tpConDerechos;
end;

function TZetaEditAccesos_DevEx.EsDerechoEspecial( const iIndex: Integer ): Boolean;
begin
     Result := False;
end;

function TZetaEditAccesos_DevEx.GetTipoNodo( Nodo: TTreeNode ): eTipoNodo;
begin
     with Nodo do
     begin
          if ( Parent <> nil ) and TextoEspecial( Parent.Text ) then
             Result := tpEspecial
          else
              if ( Nodo <> nil ) and TextoEspecial( Text ) then
                 Result := tpEspecial
              else
                  if HasChildren and ( Item[ 0 ].HasChildren ) then
                     Result := tpUnDerecho
                  else
                      Result := NodoEspecial( ImageIndex );
     end;
end;

function TZetaEditAccesos_DevEx.GetTipoDerecho( Nodo: TTreeNode ): eDerecho;
begin
     with Nodo do
     begin
          if ( Text = D_TEXT_ALTA ) then
              Result := edAlta
          else
              if ( Text = D_TEXT_BAJA ) then
                 Result := edBaja
              else
                  if ( Text = D_TEXT_EDICION ) then
                     Result :=  edModificacion
                  else
                      if ( Text = D_TEXT_IMPRESION ) then
                         Result := edImpresion
                      else
                          Result := edConsulta;
     end;
end;

function TZetaEditAccesos_DevEx.PesoDerecho( const iPosicionDerecho: TPosicionDerecho ): TDerechosAcceso;
begin
     Result := Trunc( IntPower( 2, iPosicionDerecho ) );
end;

function TZetaEditAccesos_DevEx.TieneDerecho( const iValorDerecho: TDerechosAcceso; const iPosicionDerecho: TPosicionDerecho ): Boolean;
begin
     Result := ( iValorDerecho and PesoDerecho( iPosicionDerecho ) ) > 0;
end;

function TZetaEditAccesos_DevEx.GetDerechosNodo( Nodo: TTreeNode; const Derecho: EDerecho ): TDerechosAcceso;
begin
     if ( Nodo.StateIndex = K_CHECK ) then
        Result := PesoDerecho( Ord( Derecho ) )
     else
         Result := 0;
end;

function TZetaEditAccesos_DevEx.GetDerechosPadre( Nodo: TTreeNode ): TDerechosAcceso;
begin
     Result := GetDerechosNodo( Nodo, edConsulta );
     {
     TextoAdd( Nodo, 0, tpUnDerecho );
     }
end;

procedure TZetaEditAccesos_DevEx.SetDerechosPadres( Nodo: TTreeNode; const iDerecho: TDerechosAcceso );
begin
     with Nodo do
     begin
          if ( iDerecho > 0 ) then
             StateIndex := K_CHECK
          else
              StateIndex := K_UN_CHECK;
     end;
end;

function TZetaEditAccesos_DevEx.GetDerechosHijos( Nodo: TTreeNode ): TDerechosAcceso;
var
   i: Integer;
begin
     Result := 0;
     with Nodo do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               Result := Result + GetDerechosNodo( Item[ i ], GetTipoDerecho( Item[ i ] ) );
               TextoAdd( Item[ i ], i, tpConDerechos );
          end;
     end;
end;

procedure TZetaEditAccesos_DevEx.SetDerechosHijos( Nodo: TTreeNode; const iDerecho: TDerechosAcceso );
var
   i: Integer;
begin
     with Nodo do
     begin
          StateIndex := K_UN_CHECK;
          for i := 0 to ( Count - 1 ) do
          begin
               if TieneDerecho( iDerecho, Ord( GetTipoDerecho( Item[ i ] ) ) ) then
               begin
                    Item[ i ].StateIndex := K_CHECK;
                    StateIndex := K_CHECK;
               end
               else
               begin
                    Item[ i ].StateIndex := K_UN_CHECK;
               end;
          end;
     end;
end;

function TZetaEditAccesos_DevEx.GetDerechosEspeciales( Nodo: TTreeNode ): TDerechosAcceso;

function GetDerechoEspecial: Integer;
var
   i: Integer;
begin
     Result := 0;
     with Nodo do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if ( Item[ i ].StateIndex = K_CHECK ) then
                  Result := Result + PesoDerecho( i );
               TextoAdd( Item[ i ], i, tpEspecial );
          end;
     end;
end;

begin
     if EsDerechoEspecial( Nodo.ImageIndex ) then
        Result := GetDerechoEspecial
     else
         Result := GetDerechosPadre( Nodo );
end;

procedure TZetaEditAccesos_DevEx.SetDerechosEspeciales( Nodo: TTreeNode; const iDerecho: TDerechosAcceso );

procedure SetDerechoEspecial;
var
   i: Integer;
begin
     with Nodo do
     begin
          StateIndex := K_UN_CHECK;
          for i := 0 to ( Count - 1 ) do
          begin
               if TieneDerecho( iDerecho, i ) then
               begin
                    Item[ i ].StateIndex := K_CHECK;
                    StateIndex := K_CHECK;
               end
               else
                   Item[ i ].StateIndex := K_UN_CHECK;
          end;
     end;
end;

begin
     if EsDerechoEspecial( Nodo.ImageIndex ) then
        SetDerechoEspecial
     else
         SetDerechosPadres( Nodo, iDerecho );
end;

function TZetaEditAccesos_DevEx.GetDerechos( Nodo: TTreeNode ): TDerechosAcceso;
begin
     case GetTipoNodo( Nodo ) of
          tpConDerechos: Result := GetDerechosHijos( Nodo );
          tpEspecial: Result := GetDerechosEspeciales( Nodo );
     else
         Result := GetDerechosPadre( Nodo );
     end;
end;

procedure TZetaEditAccesos_DevEx.SetDerechos( Nodo: TTreeNode; const iDerecho: TDerechosAcceso );
begin
     case GetTipoNodo( Nodo ) of
          tpConDerechos: SetDerechosHijos( Nodo, iDerecho );
          tpEspecial: SetDerechosEspeciales( Nodo, iDerecho );
     else
         SetDerechosPadres( Nodo, iDerecho );
     end;
end;

function TZetaEditAccesos_DevEx.GetImageIndex: integer;
begin
     Result := K_IMAGENINDEX;
end;

function TZetaEditAccesos_DevEx.GetImageIndexLimit: integer;
begin
     {$ifdef TRESS}
     Result := K_IMAGENINDEX_LIMIT
     {$else}
     Result := MAXINT;
     {$endif}
end;


procedure TZetaEditAccesos_DevEx.RefrescaArbol( Metodo: TBuscaDerecho );
var
   Nodo: TTreeNode;
begin
     with Arbol do
     begin
          Items.BeginUpdate;
          Nodo := Arbol.Items[ 0 ];
          repeat
                if ( Nodo.ImageIndex > GetImageIndex ) then
                   SetDerechos( Nodo, Metodo( Nodo.ImageIndex ) );
                Nodo := Nodo.GetNext;
          until ( Nodo = nil );
          Items.EndUpdate;
     end;
end;

procedure TZetaEditAccesos_DevEx.Cargar;
var
   oCursor: TCursor;
   sGroupName, sCompanyName: String;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        dmSistema.ConectaAccesos( sGroupName, sCompanyName );
        RefrescaArbol( dmSistema.BuscaDerecho );
        Grupo.Caption := sGroupName;
        Company.Caption := sCompanyName;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TZetaEditAccesos_DevEx.Descargar( TopNodo:TTreeNode = NIL; LastNodo:TTreeNode = NIL);
var
   oCursor: TCursor;
   Nodo: TTreeNode;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmSistema do
        begin
             if ( ArbolSeleccionado = adTress ) and ( cdsAccesos.State <> dsInactive ) then
                SetDerechosSinValor;  //Se usa para apagar aquellos derechos que ya no esten en la rama
             with Arbol do
             begin
                  if TopNodo = NIL then
                     Nodo := Items[ 0 ]
                  else
                      Nodo := TopNodo;
                      
                  if Nodo <> NIL then
                  begin
                       repeat
                          if ( Nodo.ImageIndex > GetImageIndex ) and
                             ( Nodo.ImageIndex < GetImageIndexLimit ) then
                             GrabaDerecho( Nodo.ImageIndex, GetDerechos( Nodo ), Nodo, GetTextoBitacora );
                          Nodo := Nodo.GetNext;
                       until ( Nodo = nil ) or (Nodo = LastNodo);
                       cdsAccesos.Enviar;
                  end;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;



procedure TZetaEditAccesos_DevEx.SetDerechosSinValor;
var
   oNodo: TTreeNode;
   iPos:Integer;
   Lista: TList;


   function EstaEnArbol( iDerecho: Integer ): Boolean;
   var
      c: Integer;
      lEstaEnRango: Boolean;
   begin
        Result:= True;
        with Lista do
        begin
             for c:= iPos to Count - 1 do
             begin
                  {  Si ya se evaluaron TODOS los elementos de la lista ordenada significa que apartir de ahi no existen el Rango es de 1 hasta Count -1}
                  lEstaEnRango:= ( c < Count - 1 );
                  Result:= ( Integer( Items[c] ) = iDerecho ) and lEstaEnRango;
                  if Result or ( ( iPos > 0 ) and ( lEstaEnRango ) and ( iDerecho < Integer( Items[c+1] ) ) ) then
                     Break;
             end;
        end;
   end;

    function OrdenaArbol( Item1, Item2: Pointer ): Integer;
    var
       iPosicion1, iPosicion2 : Integer;
    begin
         iPosicion1 := Integer( Item1 );
         iPosicion2 := Integer( Item2 );
         Result:= iPosicion1 - iPosicion2;
    end;

begin
     Lista:= TList.Create; //Es una copia del Arbol en una lista
     try
        with Arbol do
        begin
             oNodo := Items[ 0 ];
             repeat
                   if ( oNodo.ImageIndex > GetImageIndex ) then
                        Lista.Add(Pointer( oNodo.ImageIndex ));
                   oNodo := oNodo.GetNext;
             until ( oNodo = nil );
        end;
       Lista.Sort(@OrdenaArbol);  //Ordena la lista

       with dmSistema.cdsAccesosBase do
       begin
            First;
            iPos:= 0;

            while not EOF do
            begin
                 if ( FieldByName('AX_DERECHO').AsInteger > 0 ) and ( FieldByName('AX_NUMERO').AsInteger <= K_MAX_DERECHOS )then
                 begin
                      if not EstaEnArbol( dmSistema.cdsAccesosBase.FieldByName('AX_NUMERO').AsInteger ) then
                      begin
                           if not ( State in [ dsEdit, dsInsert ] ) then
                              Edit;
                           dmSistema.cdsAccesosBase.FieldByName('AX_DERECHO').AsInteger:= 0;
                      end
                      else
                          Inc(iPos); //Se compara la lista con el primer valor y se guarda la posici�n para empezar de ahi en la siguiente
                                     //comparaci�n.
                 end;
                 Next;
            end;
       end;

     finally
            FreeAndNil(Lista);
     end;
end;


procedure TZetaEditAccesos_DevEx.PrenderClick(Sender: TObject);
begin
     inherited;
     if Arbol.Selected <> nil then
        CambioArbol;
end;

procedure TZetaEditAccesos_DevEx.PrenderTodosClick(Sender: TObject);
begin
     inherited;
     InitArbolStates( K_CHECK );
end;

procedure TZetaEditAccesos_DevEx.ApagarTodosClick(Sender: TObject);
begin
     inherited;
     InitArbolStates( K_UN_CHECK );
end;

procedure TZetaEditAccesos_DevEx.ExpanderClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with Arbol do
        begin
             Items.BeginUpdate;
             FullExpand;
             Items.EndUpdate;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TZetaEditAccesos_DevEx.CompactarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with Arbol do
        begin
             Items.BeginUpdate;
             FullCollapse;
             Items.EndUpdate;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TZetaEditAccesos_DevEx.BuscarClick(Sender: TObject);
begin
     inherited;
     ZArbolFind_DevEx.BuscaNodoDialogo( Arbol, Caption );
end;

procedure TZetaEditAccesos_DevEx.ArbolBaseKeyPress(Sender: TObject; var Key: Char);
begin
     inherited;
     if ( Key = Chr( VK_RETURN ) ) then
     begin
          Prender.Click;
     end;
end;

procedure TZetaEditAccesos_DevEx.ArbolBaseMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     inherited;
     if ( Button = mbRight ) then
     begin
          with Arbol do
          begin
               Selected := GetNodeAt( X, Y );
          end;
          CambioArbol;
     end;
end;

procedure TZetaEditAccesos_DevEx.CopiarClick(Sender: TObject);

begin
     inherited;
     CopiaDerechos;
end;

procedure TZetaEditAccesos_DevEx.CopiaDerechos;
var
   oCursor: TCursor;
   iGrupo: Integer;
   sEmpresa: String;
begin
     inherited;
     if FEscogeGrupoEmpresa_DevEx.SeleccionaGrupoEmpresa( iGrupo, sEmpresa ) then
     begin

          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             FEmpresaCopiado := sEmpresa;
             FGrupoCopiado := iGrupo;
             dmSistema.CopyAccesos( iGrupo, sEmpresa );
             RefrescaArbol( dmSistema.BuscaDerechoCopiado );
             SetEditState;
             CambiosAdicionales;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TZetaEditAccesos_DevEx.OKClick(Sender: TObject);
begin
     inherited;
     Descargar;
end;

procedure TZetaEditAccesos_DevEx.ExportarClick(Sender: TObject);
var
   {oCursor: TCursor;
   sFileName: String;}
   oCursor: TCursor;
   lTodos: Boolean;
begin
     inherited;
     lTodos := True;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmSistema do
        begin
             DatasetDerechosCrear;
             DatasetDerechosCargar( cdsDerechos, lTodos );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     RepDerechos := TRepDerechos.Create( Self );
     try
        with RepDerechos do
        begin
             Todos := lTodos;
             Datos := dmSistema.cdsDerechos;
             Empresa := Self.Company.Caption;
             Grupo := Self.Grupo.Caption;

             Exporta;
        end;
     finally
            FreeAndNil( RepDerechos );
     end;
end;

procedure TZetaEditAccesos_DevEx.TextoInit;
begin
     FTexto := VACIO;
end;

procedure TZetaEditAccesos_DevEx.TextoAdd( Nodo: TTreeNode; const iPos: Integer; const eTipo: eTipoNodo );
var
   sTexto: String;
   iValue: Integer;
begin
     if FTextoGet then
     begin
          sTexto := Nodo.Text;
          if ( ( sTexto <> D_TEXT_CONSULTA ) and
             ( sTexto <> D_TEXT_ALTA ) and
             ( sTexto <> D_TEXT_BAJA ) and
             ( sTexto <> D_TEXT_EDICION ) and
             ( sTexto <> D_TEXT_IMPRESION ) ) then
          begin
               case eTipo of
                    tpConDerechos: iValue := Ord( GetTipoDerecho( Nodo ) );
                    tpEspecial: iValue := iPos;
               else
                   iValue := iPos;
               end;
               Inc( iValue );
               if ZetaCommonTools.StrVacio( FTexto ) then
                  FTexto := FTexto + Format( '%d=%s', [ iValue, sTexto ] )
               else
                   FTexto := FTexto + CR_LF + Format( '%d=%s', [ iValue, sTexto ] );
          end;
     end;
end;

procedure TZetaEditAccesos_DevEx.DatasetDerechosCargar( Dataset: TClientDataset; const lCargaTodos: Boolean );
var
   Nodo, Padre: TTreeNode;
   iDerechos: TDerechosAcceso;
   eValor, eTope: eDerecho;
   sNombre: String;
begin
     eTope := High( eDerecho );
     FTextoGet := True;
     try
        with Arbol.Items do
        begin
             Nodo := GetFirstNode;
             repeat
                   if ( Nodo.ImageIndex > GetImageIndex ) then
                   begin
                        TextoInit;
                        iDerechos := GetDerechos( Nodo );
                        sNombre := VACIO;
                        Padre := Nodo;
                        repeat
                              sNombre := Padre.Text + '-' + sNombre;
                              Padre := Padre.Parent;
                        until not Assigned( Padre );
                        sNombre := ZetaCommonTools.CortaUltimo( sNombre );
                        if lCargaTodos or ( iDerechos > 0 ) then
                        begin
                             with Dataset do
                             begin
                                  Append;
                                  FieldByName( 'RI_POS' ).AsInteger := Nodo.AbsoluteIndex;
                                  FieldByName( 'RI_NIVEL' ).AsInteger := Nodo.Level;
                                  FieldByName( 'RI_NOMBRE' ).AsString := sNombre;
                                  for eValor := Low( eDerecho ) to eTope do
                                  begin
                                       with FieldByName( Format( 'RI_ITEM_%2.2d', [ Ord( eValor ) ] ) ) do
                                       begin
                                            if TieneDerecho( iDerechos, Ord( eValor ) ) then
                                               AsString := K_GLOBAL_SI
                                            else
                                                AsString := K_GLOBAL_NO;
                                       end;
                                  end;
                                  FieldByName( 'RI_COMENTA' ).AsString := FTexto;
                                  Post;
                             end;
                        end;
                   end;
                   Nodo := Nodo.GetNext;
             until ( Nodo = nil );
        end;
     finally
            FTextoGet := False;
     end;
end;

procedure TZetaEditAccesos_DevEx.ImprimirClick(Sender: TObject);
var
   oCursor: TCursor;
   lTodos: Boolean;
begin
     inherited;
     lTodos := True;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmSistema do
        begin
             DatasetDerechosCrear;
             DatasetDerechosCargar( cdsDerechos, lTodos );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     RepDerechos := TRepDerechos.Create( Self );
     try
        with RepDerechos do
        begin
             Todos := lTodos;
             Datos := dmSistema.cdsDerechos;
             Empresa := Self.Company.Caption;
             Grupo := Self.Grupo.Caption;
             Preview;
        end;
     finally
            FreeAndNil( RepDerechos );
     end;
end;

procedure TZetaEditAccesos_DevEx.AddElemento( const iNivel, iDerecho: Integer; const sTexto: String );

procedure AgregaElemento( Nodo: TTreeNode );
begin
     FElemento := Nodo;
     with FElemento do
     begin
          ImageIndex := iDerecho;
          SelectedIndex := 0;
          StateIndex := 1;
     end
end;

begin
     if ( iNivel = 0 ) then
        AgregaElemento( Arbol.Items.Add( nil, sTexto ) )
     else
         if Assigned( FElemento ) then
         begin
              if ( iNivel = FElemento.Level ) then
                 AgregaElemento( Arbol.Items.Add( FElemento, sTexto ) )
              else
                  if ( iNivel < FElemento.Level ) then
                  begin
                       while Assigned( FElemento ) and ( FElemento.Level > iNivel ) do
                       begin
                            FElemento := FElemento.Parent;
                       end;
                       if Assigned( FElemento ) then
                          AgregaElemento( Arbol.Items.Add( FElemento, sTexto ) )
                       else
                           raise Exception.Create( Format( 'El Elemento %d = "%s" En El Nivel %d Se Sale De Estructura', [ iDerecho, sTexto, iNivel ] ) );
                  end
                  else
                      if ( iNivel = ( FElemento.Level + 1 ) ) then
                      begin
                           AgregaElemento( Arbol.Items.AddChild( FElemento, sTexto ) )
                      end
                      else
                          raise Exception.Create( Format( 'El Elemento %d = "%s" En El Nivel %d No Tiene Un Padre En El Nivel Anterior', [ iDerecho, sTexto, iNivel ] ) );
         end
         else
             raise Exception.Create( Format( 'No Hay Nodo Padre Al Cual Agregar El Elemento %d = "%s" En El Nivel %d', [ iDerecho, sTexto, iNivel ] ) );
end;

procedure TZetaEditAccesos_DevEx.AddElementoYDerechos( const iNivel, iDerecho: Integer; const sTexto: String );
begin
     AddElemento( iNivel, iDerecho, sTexto );
     AddDerechosTodos;
end;

procedure TZetaEditAccesos_DevEx.AddDerecho( const sTexto: String );
begin
     if Assigned( FElemento ) then
     begin
          with Arbol.Items.AddChild( FElemento, sTexto ) do
          begin
               ImageIndex := GetImageIndex;
               SelectedIndex := 0;
               StateIndex := -1;
          end;
     end
     else
         raise Exception.Create( Format( 'No Hay Nodo Padre Al Cual Agregar El Derecho "%s"', [ sTexto ] ) );
end;

procedure TZetaEditAccesos_DevEx.AddDerechoConsulta;
begin
     AddDerecho( D_TEXT_CONSULTA );
end;

procedure TZetaEditAccesos_DevEx.AddDerechoAlta;
begin
     AddDerecho( D_TEXT_ALTA );
end;

procedure TZetaEditAccesos_DevEx.AddDerechoBaja;
begin
     AddDerecho( D_TEXT_BAJA );
end;

procedure TZetaEditAccesos_DevEx.AddDerechoEdicion;
begin
     AddDerecho( D_TEXT_EDICION );
end;

procedure TZetaEditAccesos_DevEx.AddDerechoImpresion;
begin
     AddDerecho( D_TEXT_IMPRESION );
end;

procedure TZetaEditAccesos_DevEx.AddDerechosTodos;
begin
     AddDerechoConsulta;
     AddDerechoAlta;
     AddDerechoBaja;
     AddDerechoEdicion;
     AddDerechoImpresion;
end;

procedure TZetaEditAccesos_DevEx.ArbolConstruir;
begin
     with Arbol do
     begin
          with Items do
          begin
               Clear;
               BeginUpdate;
               try
                  ArbolDefinir;
               finally
                      EndUpdate;
               end;
          end;
     end;
end;

procedure TZetaEditAccesos_DevEx.ArbolDefinir;
begin
end;

{ *********** Para Pasar del DFM a Codigo ********** }

function TZetaEditAccesos_DevEx.GetArbol: TcxTreeView;
begin
     Result := ArbolBase;
end;

function TZetaEditAccesos_DevEx.GetTextoBitacora( Nodo: TTreeNode; const iDerecho: TDerechosAcceso): String;
var
   sTextoNodo: String;
   i: Integer;
const
     K_COMA = ',';
begin
     sTextoNodo:= VACIO;

     case GetTipoNodo( Nodo ) of
          tpConDerechos:
          begin
               with Nodo do
               begin
                    for i:= 0 to ( Count - 1 ) do
                    begin
                         if TieneDerecho( iDerecho, Ord( GetTipoDerecho( Item[ i ] ) ) ) then
                         begin
                              sTextoNodo:= ZetaCommonTools.ConcatString(sTextoNodo, Item[ i ].Text, K_COMA ) ;
                         end;
                    end;
               end;
          end;

          tpEspecial:
          begin
               with Nodo do
               begin
                    if HasChildren and ( not TextoEspecial( Nodo.Text ) ) then
                    begin
                         for i := 0 to ( Count - 1 ) do
                         begin
                              if TieneDerecho( iDerecho, i ) then
                              begin
                                   sTextoNodo:= ZetaCommonTools.ConcatString(sTextoNodo, Item[ i ].Text, K_COMA )  ;
                              end;
                         end;
                    end
                    else
                    begin
                         sTextoNodo := ZetaCommonTools.ConcatString(sTextoNodo, zBoolToStr( TieneDerecho( iDerecho, Ord( edConsulta ) ) ), K_COMA )  ;
                    end;
               end;
          end;
          else
              with Nodo do
              begin
                   sTextoNodo:= zBoolToStr( iDerecho > 0 );
              end;
          end;

     if strVacio( sTextoNodo ) then
        Result:= 'Ning�n Derecho'
     else
        Result:= sTextoNodo;
end;

function TZetaEditAccesos_DevEx.GetArbolSeleccionado: eArbolDerechos;
begin
     Result := dmSistema.ArbolSeleccionado;
end;

procedure TZetaEditAccesos_DevEx.SetArbolSeleccionado( const Value: eArbolDerechos);
begin
     dmSistema.ArbolSeleccionado := Value;
end;

procedure TZetaEditAccesos_DevEx.CambiosAdicionales;
begin
     //
end;

procedure TZetaEditAccesos_DevEx.OK_DevExClick(Sender: TObject);
begin
 inherited;
     Descargar;
end;

end.

