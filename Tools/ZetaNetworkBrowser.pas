unit ZetaNetworkBrowser;

interface

uses
  SysUtils, Windows, Forms, ImgList, Controls, StdCtrls, Buttons, ExtCtrls, Classes, ComCtrls;

{$DEFINE GET_INFO}
{$UNDEF GET_INFO}

type
  TNetResA = class(TObject)
  private
    { Private declarations }
    FScope: Cardinal;
    FResourceType: Cardinal;
    FDisplayType: Cardinal;
    FUsage: Cardinal;
    FLocalName: String;
    FRemoteName: String;
    FComment: String;
    FProvider: String;
    FLoaded: Boolean;
    FIsLeaf: Boolean;
    FIsEmpty: Boolean;
  public
    { Public declarations }
    property Scope: Cardinal read FScope write FScope;
    property ResourceType: Cardinal read FResourceType write FResourceType;
    property DisplayType: Cardinal read FDisplayType write FDisplayType;
    property Usage: Cardinal read FUsage write FUsage;
    property LocalName: String read FLocalName write FLocalName;
    property RemoteName: String read FRemoteName write FRemoteName;
    property Comment: String read FComment write FComment;
    property Provider: String read FProvider write FProvider;
    property Loaded: Boolean read FLoaded write FLoaded;
    property IsLeaf: Boolean read FIsLeaf write FIsLeaf;
    property IsEmpty: Boolean read FIsEmpty write FIsEmpty;
  end;

  PPNetResA = ^TNetResA;

  TNetworkBrowser = class(TForm)
    Images: TImageList;
    TreeView: TTreeView;
    PanelControles: TPanel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure TreeViewDblClick(Sender: TObject);
  private
    { Private declarations }
    FResources: TStrings;
    FReadPrinters: Boolean;
    function GetSelectedValue: String;
    procedure ClearNodes;
    procedure ReadResources(ParentNode: TTreeNode);
    procedure ShowEmptyMessage;
    procedure ShowStatusMessage(const iStatus: Cardinal);
    {$IFDEF GET_INFO}
    procedure GetInfo(Node: TTreeNode);
    {$ENDIF}
  public
    { Public declarations }
    procedure SetUpForPrinters;
    procedure SetUpForServers;
    property SelectedValue: String read GetSelectedValue;
  end;

var
  NetworkBrowser: TNetworkBrowser;

function GetPrinterPort(const sDefault: String): String;
function GetServer(const sDefault: String): String;
function GetServerName(const sDefault: String): String;

implementation

uses
  ZetaDialogo, ZetaCommonClasses;

{$R *.DFM}

function GetNetworkResource(const lPrinters: Boolean; const sDefault: String): String;
begin
  Result := sDefault;
  if (NetworkBrowser = nil) then
    NetworkBrowser := TNetworkBrowser.Create(Application);
  if (NetworkBrowser <> nil) then begin
    with NetworkBrowser do begin
      if lPrinters then
        SetUpForPrinters
      else
        SetUpForServers;
      ShowModal;
      if (ModalResult = mrOk) then
        Result := SelectedValue;
    end;
  end;
end;

function GetPrinterPort(const sDefault: String): String;
begin
  Result := GetNetworkResource(True, sDefault);
end;

function GetServer(const sDefault: String): String;
begin
  Result := GetNetworkResource(False, sDefault);
end;

function GetServerName(const sDefault: String): String;
var
  iPos: Integer;
begin
  Result := GetServer(sDefault);
  iPos := Pos('\\', Result);
  if (iPos > 0) then
    Result := Copy(Result, (iPos + 2), (Length(Result) - (iPos + 1)));
end;

{$WARNINGS OFF}

function GetNetworkResources(ResourceList: TStrings; ResourceOwner: TNetResA;
  const lPrinters: Boolean; var ReturnValue: Cardinal): Boolean;
type
  TCompResArray = array [0 .. 10000] of TNetResource;
  PCompResArray = ^TCompResArray;
var
  i: Integer;
  ResourceType, ResourceEntries, BufferSize: DWORD;
  ResourceHandle: THandle;
  Resource: TNetResA;
  ComputerResources: PCompResArray;
  PNetResA: Windows.PNetResource;
  NetResA: Windows.TNetResource;

  function CheckOpenStatus: Boolean;
  begin
    Result := (ReturnValue = NO_ERROR);
  end;

  function CheckEnumStatus: Boolean;
  begin
    case ReturnValue of
      NO_ERROR:
        Result := True;
      ERROR_MORE_DATA:
        Result := True;
      else
        Result := True;
    end;
  end;

  function CheckFilter(Resource: TNetResource): Boolean;
  begin
    if lPrinters then
      Result := True
    else begin
          {
          Result := ( Resource.dwType = RESOURCETYPE_ANY ); // GA: parece ser que esto trabaja en WinNT pero no en Win95 //
 }
      Result := True;
    end;
  end;

begin
  Result := False;
  if lPrinters then
    ResourceType := RESOURCETYPE_PRINT
  else
    ResourceType := RESOURCETYPE_ANY;
     // Convert the ResourceOwner which is a TNetResA Object to a PNetResourceA record. If
     // ResourceOwner is nil this indicates that the top level network resource is being enumerated
  if Assigned(ResourceOwner) then begin
    NetResA.dwScope := ResourceOwner.Scope;
    NetResA.dwType := ResourceOwner.ResourceType;
    NetResA.dwDisplayType := ResourceOwner.DisplayType;
    NetResA.dwUsage := ResourceOwner.Usage;
    NetResA.lpLocalName := StrAlloc(Length(ResourceOwner.LocalName) + 1);
    StrPCopy(NetResA.lpLocalName, ResourceOwner.LocalName);
    NetResA.lpRemoteName := StrAlloc(Length(ResourceOwner.RemoteName) + 1);
    StrPCopy(NetResA.lpRemoteName, ResourceOwner.RemoteName);
    NetResA.lpComment := StrAlloc(Length(ResourceOwner.Comment) + 1);
    StrPCopy(NetResA.lpComment, ResourceOwner.Comment);
    NetResA.lpProvider := StrAlloc(Length(ResourceOwner.Provider) + 1);
    StrPCopy(NetResA.lpProvider, ResourceOwner.Provider);
    PNetResA := Addr(NetResA);
  end
  else
    PNetResA := nil;
     // Open handle to network resource
  ReturnValue := Windows.WNetOpenEnum(RESOURCE_GLOBALNET, ResourceType, 0, PNetResA,
    ResourceHandle);
  if CheckOpenStatus then begin
          // Intialize Values for API call.  The buffer size is the amount of memory
          // needed to store the record structures returned.
    BufferSize := 0;
          // Initialize entries value and allocate the memory for the enumeration buffer
    ResourceEntries := MaxDWord;
    New(ComputerResources);
          // This API call is used to enumerate the container network resource to obtain
          // the other network resources that fall below this item in the network heiarchy.
          // The first call to this API function is used to determine the size of memory block
          // that will be needed to hold the array values.  The size of the memory block will
          // be stored in the variable BufferSize
    ReturnValue := Windows.WNetEnumResource(ResourceHandle, ResourceEntries, ComputerResources,
      BufferSize);
    if CheckEnumStatus then begin
      repeat
                     // Obtain the resoures that were enumerated with the first call to WNetEnumResource
        for i := 0 to (ResourceEntries - 1) do begin
          if CheckFilter(ComputerResources^[i]) then begin
            with ComputerResources^[i] do begin
              Resource := TNetResA.Create;
              with Resource do begin
                Scope := dwScope;
                ResourceType := dwType;
                DisplayType := dwDisplayType;
                Usage := dwUsage;
                LocalName := StrPas(lpLocalName);
                RemoteName := StrPas(lpRemoteName);
                Comment := StrPas(lpComment);
                Provider := StrPas(lpProvider);
                ResourceList.AddObject(IntToStr(i) + ' - ' + RemoteName, Resource);
              end;
            end;
          end;
        end;
                     // Free the memory for the buffer and re-intialize the variables
        FreeMem(ComputerResources, BufferSize);
        ResourceEntries := MaxDWord;
        BufferSize := 0;
                     // Call WNetEnumResource again to determine the amount of memory that is
                     // needed for the next enumeration of network resource array information
        Windows.WNetEnumResource(ResourceHandle, ResourceEntries, ComputerResources, BufferSize);
                     // Allocate the memory and re-intialize the the ResourceEntries variable
        GetMem(ComputerResources, BufferSize);
        ResourceEntries := MaxDWord;
                     // Call WNetEnumResource again to continue the enumeration of the network resource
        ReturnValue := Windows.WNetEnumResource(ResourceHandle, ResourceEntries, ComputerResources,
          BufferSize);
      until not(ReturnValue in [NO_ERROR, ERROR_MORE_DATA]);
               // Close the handle to that was opened with the call to WNetOpenEnem
      Result := True;
    end;
          // Dispose of the pchar memory that was allocated for the PNetResourceA structure
    Dispose(ComputerResources);
  end;
  Windows.WNetCloseEnum(ResourceHandle);
  if Assigned(ResourceOwner) then begin
    with NetResA do begin
      StrDispose(lpLocalName);
      StrDispose(lpRemoteName);
      StrDispose(lpComment);
      StrDispose(lpProvider);
    end;
  end;
end;
{$WARNINGS ON}
{ *********** TNetworkBrowser ************ }

procedure TNetworkBrowser.FormCreate(Sender: TObject);
begin
  FResources := TStringList.Create;
  FReadPrinters := False;
  HelpContext := H80084_Especificar_Servidor;
end;

procedure TNetworkBrowser.FormDestroy(Sender: TObject);
begin
  ClearNodes;
  FResources.Free;
end;

function TNetworkBrowser.GetSelectedValue: String;
begin
  with TreeView do begin
    if (Selected = nil) then
      Result := ''
    else
      Result := Selected.Text;
  end;
end;

procedure TNetworkBrowser.ClearNodes;
var
  i: Integer;
begin
  with TreeView.Items do begin
    BeginUpdate;
    for i := 0 to (Count - 1) do begin
      with Item[i] do begin
        if (Data <> nil) then
          TNetResA(Data).Free;
      end;
    end;
    Clear;
    EndUpdate;
  end;
end;

procedure TNetworkBrowser.SetUpForPrinters;
begin
  FReadPrinters := True;
  ClearNodes;
  ReadResources(nil);
  Caption := 'Seleccione La Impresora de Red Deseada';
end;

procedure TNetworkBrowser.SetUpForServers;
begin
  FReadPrinters := False;
  ClearNodes;
  ReadResources(nil);
  Caption := 'Seleccione El Servidor Deseado';
end;

{$IFDEF GET_INFO}

procedure TNetworkBrowser.GetInfo(Node: TTreeNode);
var
  Resource: TNetResA;
  Value: String;
begin
  Resource := TNetResA(Node.Data);
  with Memo.Lines do begin
    BeginUpdate;
    Clear;
          // Display dwScope element
    case Resource.Scope of
      1:
        Value := 'RESOURCE_CONNECTED';
      2:
        Value := 'RESOURCE_GLOBALNET';
      3:
        Value := 'RESOURCE_REMEMBERED';
      4:
        Value := 'RESOURCE_RECENT';
      5:
        Value := 'RESOURCE_CONTEXT';
      else
        Value := IntToStr(Resource.Scope);
    end;
    Add('Scope = ' + Value);
          // Display dwType element
    case Resource.ResourceType of
      0:
        Value := 'RESOURCETYPE_ANY';
      1:
        Value := 'RESOURCETYPE_DISK';
      2:
        Value := 'RESOURCETYPE_PRINT';
      8:
        Value := 'RESOURCETYPE_RESERVED'; {
               4294967295: Value := 'RESOURCETYPE_UNKNOWN';
 }
      else
        Value := IntToStr(Resource.ResourceType);
    end;
    Add('ResourceType = ' + Value);
    case Resource.DisplayType of
      0:
        Value := 'RESOURCEDISPLAYTYPE_GENERIC';
      1:
        Value := 'RESOURCEDISPLAYTYPE_DOMAIN';
      2:
        Value := 'RESOURCEDISPLAYTYPE_SERVER';
      3:
        Value := 'RESOURCEDISPLAYTYPE_SHARE';
      4:
        Value := 'RESOURCEDISPLAYTYPE_FILE';
      5:
        Value := 'RESOURCEDISPLAYTYPE_GROUP';
      6:
        Value := 'RESOURCEDISPLAYTYPE_NETWORK';
      7:
        Value := 'RESOURCEDISPLAYTYPE_ROOT';
      8:
        Value := 'RESOURCEDISPLAYTYPE_SHAREADMIN';
      9:
        Value := 'RESOURCEDISPLAYTYPE_DIRECTORY';
      10:
        Value := 'RESOURCEDISPLAYTYPE_TREE';
      11:
        Value := 'RESOURCEDISPLAYTYPE_NDSCONTAINER';
      else
        Value := IntToStr(Resource.DisplayType);
    end;
    Add('DisplayType = ' + Value);
    case Resource.Usage of
      1:
        Value := 'RESOURCEUSAGE_CONNECTABLE';
      2:
        Value := 'RESOURCEUSAGE_CONTAINER';
      4:
        Value := 'RESOURCEUSAGE_NOLOCALDEVICE';
      8:
        Value := 'RESOURCEUSAGE_SIBLING';
      16:
        Value := 'RESOURCEUSAGE_ATTACHED';
      19:
        Value := 'RESOURCEUSAGE_ALL'; {
              2147483648: Value := 'RESOURCEUSAGE_RESERVED';
 }
      else
        Value := IntToStr(Resource.Usage);
    end;
    Add('Usage = ' + Value);
    Add('LocalName = ' + Resource.LocalName);
    Add('RemoteName = ' + Resource.RemoteName);
    Add('Comment = ' + Resource.Comment);
    Add('Provider = ' + Resource.Provider);
    EndUpdate;
  end;
end;
{$ENDIF}

procedure TNetworkBrowser.ShowEmptyMessage;
begin
  ShowStatusMessage(ERROR_NO_MORE_ITEMS);
end;

procedure TNetworkBrowser.ShowStatusMessage(const iStatus: Cardinal);
begin
  case iStatus of
    ERROR_NO_MORE_ITEMS: begin
        if FReadPrinters then
          zInformation(Caption, '� No Hay Impresoras !', 0)
        else
          zInformation(Caption, '� No Hay Servidores !', 0);
      end;
    else
      zError(Caption, 'WNETOPEN/WNETENUMTRESOURCE Error ' + IntToStr(iStatus), 0);
  end;
end;

// Reads Network Resources
procedure TNetworkBrowser.ReadResources(ParentNode: TTreeNode);
var
  lLoad: Boolean;
  i: Integer;
  iStatus: Cardinal;
  ResourceOwner: TNetResA;
  oCursor: TCursor;

  procedure ConfigureNode(Node: TTreeNode);
  const
    I_RESOURCETYPE_DISK                = 0;
    I_RESOURCETYPE_PRINT               = 9;
    I_RESOURCEDISPLAYTYPE_GENERIC      = 5;
    I_RESOURCEDISPLAYTYPE_DOMAIN       = 8;
    I_RESOURCEDISPLAYTYPE_SERVER       = 6;
    I_RESOURCEDISPLAYTYPE_SHARE        = 4;
    I_RESOURCEDISPLAYTYPE_FILE         = 4;
    I_RESOURCEDISPLAYTYPE_GROUP        = 8;
    I_RESOURCEDISPLAYTYPE_NETWORK      = 7;
    I_RESOURCEDISPLAYTYPE_ROOT         = 3;
    I_RESOURCEDISPLAYTYPE_SHAREADMIN   = 0;
    I_RESOURCEDISPLAYTYPE_DIRECTORY    = 4;
    I_RESOURCEDISPLAYTYPE_TREE         = 3;
    I_RESOURCEDISPLAYTYPE_NDSCONTAINER = 5;
  var
    Resource: TNetResA;
  begin
    with Node do begin
      Resource := TNetResA(Data);
          // Loading the appropriate image for each list view item
      case Resource.ResourceType of
               {
               1: ImageIndex := I_RESOURCETYPE_DISK;
 }
        2:
          ImageIndex := I_RESOURCETYPE_PRINT;
        else
          case Resource.DisplayType Of
            0:
              ImageIndex := I_RESOURCEDISPLAYTYPE_GENERIC;
            1:
              ImageIndex := I_RESOURCEDISPLAYTYPE_DOMAIN;
            2:
              ImageIndex := I_RESOURCEDISPLAYTYPE_SERVER;
            3:
              ImageIndex := I_RESOURCEDISPLAYTYPE_SHARE;
            4:
              ImageIndex := I_RESOURCEDISPLAYTYPE_FILE;
            5:
              ImageIndex := I_RESOURCEDISPLAYTYPE_GROUP;
            6:
              ImageIndex := I_RESOURCEDISPLAYTYPE_NETWORK;
            7:
              ImageIndex := I_RESOURCEDISPLAYTYPE_ROOT;
            8:
              ImageIndex := I_RESOURCEDISPLAYTYPE_SHAREADMIN;
            9:
              ImageIndex := I_RESOURCEDISPLAYTYPE_DIRECTORY;
            10:
              ImageIndex := I_RESOURCEDISPLAYTYPE_TREE;
            11:
              ImageIndex := I_RESOURCEDISPLAYTYPE_NDSCONTAINER;
          end;
      end;
      SelectedIndex := ImageIndex;
      if (Resource.RemoteName = '') then
        Text := Resource.Provider
      else
        Text := Resource.RemoteName;
      if FReadPrinters then
        Resource.IsLeaf := (Resource.DisplayType = RESOURCEDISPLAYTYPE_SHARE)
      else
        Resource.IsLeaf := (Resource.DisplayType = RESOURCEDISPLAYTYPE_SERVER);
    end;
  end;

begin
  if (ParentNode = nil) then
    lLoad := True
  else if (ParentNode.Data = nil) then
    lLoad := True
  else begin
    with TNetResA(ParentNode.Data) do begin
      if Loaded then begin
        lLoad := False;
        if IsEmpty then begin
          ShowEmptyMessage;
        end;
      end else if IsLeaf then
        lLoad := False
      else
        lLoad := True;
    end;
  end;
  if lLoad then begin
    oCursor := Screen.Cursor;
    Screen.Cursor := crHourglass;
    try
      with FResources do begin
        BeginUpdate;
        Clear;
        if (ParentNode = nil) or (ParentNode.Data = nil) then
          ResourceOwner := nil
        else
          ResourceOwner := TNetResA(ParentNode.Data);
        if GetNetworkResources(FResources, ResourceOwner, FReadPrinters, iStatus) then begin
          if (FResources.Count = 0) then begin
            if (ResourceOwner <> nil) then begin
              with ResourceOwner do begin
                Loaded := True;
                IsEmpty := True;
              end;
            end;
            ShowEmptyMessage;
          end else begin
            with TreeView.Items do begin
              BeginUpdate;
              for i := 0 to (FResources.Count - 1) do begin
                                      // Load the object from the string list into a variable
                                      // Add a new node to the tree and configure it
                ConfigureNode(AddChildObject(ParentNode, '', TNetResA(FResources.Objects[i])));
              end;
              EndUpdate;
            end;
            if (ParentNode <> nil) then begin
              if (ResourceOwner <> nil) then
                ResourceOwner.Loaded := True;
              with ParentNode do begin
                Expand(False);
                if HasChildren then
                  TreeView.Selected := GetFirstChild;
              end;
            end;
          end;
        end
        else
          ShowStatusMessage(iStatus);
        EndUpdate;
      end;
    finally
      Screen.Cursor := oCursor;
    end;
  end;
end;

procedure TNetworkBrowser.TreeViewDblClick(Sender: TObject);
begin
  with TreeView do begin
    if (Selected <> nil) then begin
      if TNetResA(Selected.Data).IsLeaf then
        OK.Click
      else
        ReadResources(Selected);
    end;
  end;
end;

end.
