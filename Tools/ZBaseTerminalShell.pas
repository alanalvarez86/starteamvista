unit ZBaseTerminalShell;

interface

uses
  VaPrst, VaConst, Graphics, SysUtils, Dialogs, Windows, MMSystem, Forms, ExtCtrls, VaClasses, VaComm, StdCtrls,
  jpeg, Controls, Classes, DB, ZetaBanner,
  //
  ZetaRegistryTerminal, dxSkinsCore, TressMorado2013, cxLookAndFeels,
  dxSkinsForm, cxStyles, cxClasses, dxGDIPlusClasses;

type

  TInfoPuerto = record
    PortNum: Integer;
    BaudRate: TVaBaudrate;
    DataBits: TVaDataBits;
    Parity: TVaParity;
    StopBits: TVaStopBits;
    DTRControl: TVaControlDTR;
    FlowControl: Integer; // TVaFlowControl;
    RTSControl: TVaControlRTS;
    EventChar: AnsiChar;
  end;

  TBaseTerminalShell = class(TForm)
    PanelGeneral: TPanel;
    PanelInfoAdicional: TPanel;
    VaCommA: TVaComm;
    VaCommB: TVaComm;
    TimerBanner: TTimer;
    PanelBanner: TPanel;
    DataSource: TDataSource;
    Banner: TZetaBanner;
    Desconectado: TTimer;
    cxStyleRepository: TcxStyleRepository;
    cxStyleStatusBarLabel: TcxStyle;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxsColumnas: TcxStyle;
    DevEx_SkinController: TdxSkinController;
    panelDemo: TPanel;
    lblDEMO: TLabel;
    panelEnLinea: TPanel;
    LblConectado: TLabel;
    panelSuperior: TPanel;
    PanelFoto: TPanel;
    Foto: TImage;
    panelTodosLosDatos: TPanel;
    panelRespuesta: TPanel;
    panelTarjeta: TPanel;
    LblCredencial: TLabel;
    edCredencial: TEdit;
    panelEmpleado: TPanel;
    panelDatosEmpleado: TPanel;
    ImageLogo: TImage;
    LblNombre: TLabel;
    Checada: TButton;
    pFechaHora: TPanel;
    lblHora: TLabel;
    lblFecha: TLabel;
    procedure TimerBannerTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure VaCommABreak(Sender: TObject);
    procedure VaCommAError(Sender: TObject; Errors: Integer);
    procedure VaCommARx80Full(Sender: TObject);
    procedure VaCommATxEmpty(Sender: TObject);
    procedure VaCommARxFlag(Sender: TObject);
    procedure ChecadaClick(Sender: TObject);
    procedure ChecadaEnter(Sender: TObject);
    procedure DesconectadoTimer(Sender: TObject);
  private
    { Private declarations }
    aSerialSalida    : array [FALSE .. TRUE] of String;
    FTerminalRegistry: TZetaTerminalRegistry;
    FVaCommSalida    : TVaComm;
    function GetConfiguracion(const lEntrada, lIguales: Boolean): String;
    function LeeParametros: Boolean;
    procedure AbrePuerto(VaCommControl: TVaComm; PuertoInfo: TInfoPuerto; const lEntrada: Boolean);
    procedure InitSerialControl;
    procedure InitSerial(const lEntrada, lIguales: Boolean; const iPuerto: Integer);
    procedure WarningMessage(const sLetrero: String);
    procedure SerialResultado(const lOk: Boolean);
    procedure IniciaSerial;
    procedure SonidoAcepta;
    procedure SonidoRechaza;
  protected
    { Protected declarations }
    property TerminalRegistry: TZetaTerminalRegistry read FTerminalRegistry write FTerminalRegistry;
    function ShowFormaConfiguracion(const lInicial: Boolean): Boolean; virtual;
    function GetTerminalChecada: String;
    procedure Pinta(eColor: TColor; bEsAmarillo: Boolean = FALSE); dynamic;
    procedure ShowResultado(const sResultado: String); dynamic;
    procedure EscribeTextoColor(const oLabel: TLabel; const eColor: TColor; const sTexto: String);
    procedure SetCredencial(const sValue: String);
    procedure InitTerminalRegistry; virtual; abstract;
    procedure ClearTerminalRegistry; virtual; abstract;
    procedure IniciaTerminal; virtual; abstract;

    procedure CheckStatusServidor; virtual; abstract;
    procedure RetryConnectServidor; virtual; abstract;

    procedure ProcesaChecada; virtual; abstract;
    procedure ProcesaChecadaSerial(const sTexto: String); virtual;
    procedure LimpiaDatos; virtual;
    procedure InitShellValues; dynamic;
    procedure ConfiguraEstacion(const lInicial: Boolean = FALSE); dynamic;
    procedure AceptaChecada;
    procedure RechazaChecada;
    procedure AdvierteChecada;
    procedure CheckServidorDesconectado;
    procedure FocusCredencial;
  public
    { Public declarations }
    function GetInfoPuerto(const iPuerto: Integer; const sConfiguracion: String): TInfoPuerto;
    function SetInfoPuerto(const InfoPuerto: TInfoPuerto): String;
    procedure SetLblDemo(const sMensaje: String);
    procedure ResetTimerConnect( const lConectado: Boolean );
    procedure ShowTipoConexion(const lConectado: Boolean); dynamic;
  end;

const
  MIN_SIZE_FONT = 18;
  MED_SIZE_FONT = 30;
  MAX_SIZE_FONT = 46;
  MIN_SIZE_PANEL = 700;
  MAX_SIZE_PANEL = 1020;
  CANT_CARAC_PANEL = 25;

var
  BaseTerminalShell: TBaseTerminalShell;

implementation

uses
  ZetaCommonClasses, ZetaCommonLists, ZetaCommonTools, ZetaDialogo, ZetaClientTools, ZetaMessages, FPreguntaPassWord,
  FServidorDesconectado;

{$R *.DFM}

procedure TBaseTerminalShell.FormCreate(Sender: TObject);
begin
  Environment;
  InitTerminalRegistry;
  WindowState := wsMaximized;
  LimpiaDatos;
  with Application do begin
    UpdateFormatSettings := FALSE;
  end;

  Checada.Width := 0; // Ocultar el boton para checada

  with Banner do begin
    StepsPerSecond := TerminalRegistry.Velocidad * 3;
    Active         := True;
  end;
end;

procedure TBaseTerminalShell.FormDestroy(Sender: TObject);
begin
  ClearTerminalRegistry;
end;

procedure TBaseTerminalShell.FormShow(Sender: TObject);
begin
  if (LeeParametros) then begin
    IniciaTerminal;
  end else begin
    if (TerminalRegistry.CanWrite) then
      ConfiguraEstacion(TRUE)
    else begin
      ZetaDialogo.ZError('Acceso al Programa', 'No Tiene Configurado Acceso' + CR_LF +
          'Requiere Derechos de Escritura en Registry' + CR_LF + 'Para Realizar Esta Configuración', 0);
      Close;
    end;
  end;
end;

function TBaseTerminalShell.LeeParametros: Boolean;
var
  oCursor: TCursor;
begin
  oCursor       := Screen.Cursor;
  Screen.Cursor := crHourglass;
  try
    InitShellValues;
    Result := StrLleno(TerminalRegistry.Servidor) and (TerminalRegistry.Puerto > 0);
    if Result then
      IniciaSerial;
  finally
    Screen.Cursor := oCursor;
  end;
end;

procedure TBaseTerminalShell.InitShellValues;
begin

end;

procedure TBaseTerminalShell.IniciaSerial;
var
  lIguales: Boolean;
begin
  // Configura Entrada y Salida Serial
  InitSerialControl;
  with TerminalRegistry do begin
    lIguales := (SerialA = SerialB);
    if (SerialA > 0) then // Entrada Serial
    begin
      InitSerial(TRUE, lIguales, SerialA);
    end;
    if (SerialB > 0) then // Salida Serial
    begin
      InitSerial(FALSE, lIguales, SerialB);
    end;
  end;
end;

procedure TBaseTerminalShell.SetCredencial(const sValue: String);
begin
  edCredencial.Text := sValue;
end;

function TBaseTerminalShell.ShowFormaConfiguracion(const lInicial: Boolean): Boolean;
begin
  Result := TRUE;
end;

procedure TBaseTerminalShell.ConfiguraEstacion(const lInicial: Boolean);
begin
  if ValidaPasswordOperador(TerminalRegistry.ClaveAdmon) then begin
    SetCredencial(VACIO);
    if ShowFormaConfiguracion(lInicial) then begin
      LeeParametros;
      LimpiaDatos;
    end else if ZetaCommonTools.StrVacio(TerminalRegistry.Servidor) then
      Close;
  end;
end;

procedure TBaseTerminalShell.DesconectadoTimer(Sender: TObject);
var
  i : integer;
begin
    with Screen do
    begin
        for I := 0 to FormCount - 1 do
          PostMessage( Forms[I].Handle, WM_KILLWINDOW, 0, 0);
    end;
end;

procedure TBaseTerminalShell.LimpiaDatos;
begin
  ShowResultado(VACIO);
  LblNombre.Caption := VACIO;
  Foto.Picture      := nil;
end;

procedure TBaseTerminalShell.Pinta(eColor: TColor; bEsAmarillo: Boolean = FALSE);
begin
  panelRespuesta.Font.Color := clWhite;
  panelRespuesta.Color := eColor;
  if bEsAmarillo then
    // panelRespuesta.Font.Color := RGB (113, 113, 113);
    // panelRespuesta.Font.Color := clBlack;
    panelRespuesta.Font.Color := TColor ($4D3B4B);
  Application.ProcessMessages;
end;

procedure TBaseTerminalShell.ShowResultado(const sResultado: String);
const K_ESPACIO = ' ';
begin
  with panelRespuesta do
  begin
    {if ZetaCommonTools.StrLleno(sResultado) then
    begin
      Font.Size := ZetaCommonTools.iMin(MAX_SIZE_FONT, ZetaCommonTools.iMax(MIN_SIZE_FONT,
          Trunc(Width / Length(sResultado)) + 1));
    end;}

    Caption := K_ESPACIO + sResultado;
  end;
  Application.ProcessMessages;
end;

procedure TBaseTerminalShell.EscribeTextoColor(const oLabel: TLabel; const eColor: TColor; const sTexto: String);
begin
  with oLabel do
  begin
    if ZetaCommonTools.StrLleno(sTexto) then
    begin
      Caption    := sTexto;
      Font.Color := eColor;
      Visible    := TRUE;
    end;
  end;
  Application.ProcessMessages;
end;

procedure TBaseTerminalShell.WarningMessage(const sLetrero: String);
begin
  Pinta(RGB (255, 255, 85), TRUE);
  ShowResultado(sLetrero);
  // Sonido ... Si se quiere sonido para warning de comunicación serial
end;

{ Comunicación con dispositivos seriales }

procedure TBaseTerminalShell.InitSerialControl;

  procedure InitCommControl(CommControl: TVaComm);
  const
    K_TIMEOUT_SERIAL = 500;
    K_TIMEOUT_CLOSE  = 2000;
  begin
    with CommControl do begin
      if Active then begin
        PurgeReadWrite;
        Close;
      end;
      PortNum  := 0;
      OnRxFlag := nil;
    end;
    Sleep(K_TIMEOUT_SERIAL); // Requiere de un timeout para que se libere el puerto de la computadora
  end;

begin
  FVaCommSalida := nil;
  InitCommControl(VaCommA);
  InitCommControl(VaCommB);
end;

procedure TBaseTerminalShell.InitSerial(const lEntrada, lIguales: Boolean; const iPuerto: Integer);
begin
  if (VaCommA.PortNum = 0) or (VaCommA.PortNum = iPuerto) then
    AbrePuerto(VaCommA, GetInfoPuerto(iPuerto, GetConfiguracion(lEntrada, lIguales)), lEntrada)
  else
    AbrePuerto(VaCommB, GetInfoPuerto(iPuerto, GetConfiguracion(lEntrada, lIguales)), lEntrada);
end;

procedure TBaseTerminalShell.AbrePuerto(VaCommControl: TVaComm; PuertoInfo: TInfoPuerto; const lEntrada: Boolean);
begin
  with VaCommControl do begin
    if not Active then begin
      PortNum                := PuertoInfo.PortNum;
      BaudRate               := PuertoInfo.BaudRate;
      DataBits               := PuertoInfo.DataBits;
      Parity                 := PuertoInfo.Parity;
      StopBits               := PuertoInfo.StopBits;
      FlowControl.ControlDtr := PuertoInfo.DTRControl;

      FlowControl.ControlRTS := PuertoInfo.RTSControl;
      // TVaFlowControl = (fcNone, fcXonXoff, fcRtsCts, fcDtrDsr);
      FlowControl.XonXoffIn  := PuertoInfo.FlowControl = 1;  // fcXonXoff
      FlowControl.XonXoffOut := PuertoInfo.FlowControl = 1;  // fcXonXoff
      FlowControl.OutCtsFlow := PuertoInfo.FlowControl = 2;  // fcRtsCts
      //ToDo: Verificar el mapeo entre el FlowContol enumerado y el FlowControl Nuevo
      //FlowControl.ControlDtr := PuertoInfo.FlowControl = 3;  // fcDtrDsr

      if lEntrada then begin
        EventChars.EventChar := PuertoInfo.EventChar;
        OnRxFlag             := VaCommARxFlag;
      end;
      try
        Open;
      except
        on Error: Exception do begin
          ShowMessage(VaCommControl.Name);
          Application.HandleException(Error);
          VaCommControl.Close;
        end;
      end;
    end;
    if Active and not lEntrada then begin
      FVaCommSalida        := VaCommControl; // Direcciona para el Destino de las Salidas
      aSerialSalida[FALSE] := ZetaCommonTools.GetCodigosASCII(FTerminalRegistry.CodigoRechazo);
      aSerialSalida[TRUE]  := ZetaCommonTools.GetCodigosASCII(FTerminalRegistry.CodigoAcepta);
    end;
  end;
end;

function TBaseTerminalShell.GetConfiguracion(const lEntrada, lIguales: Boolean): String;
begin
  if (lEntrada or lIguales) then
    Result := FTerminalRegistry.ComEntrada
  else
    Result := FTerminalRegistry.ComSalida;
end;

function TBaseTerminalShell.GetInfoPuerto(const iPuerto: Integer; const sConfiguracion: String): TInfoPuerto;
var
  oLista: TStringList;

  function GetValorLista(const indice: Integer): Integer;
  begin
    if (oLista.Count - 1 < indice) then
      Result := 0
    else
      Result := StrToIntDef(oLista[indice], 0);
  end;

begin
  oLista := TStringList.Create;
  try
    oLista.CommaText := sConfiguracion;
    with Result do begin
      PortNum     := iPuerto;
      BaudRate    := TVaBaudrate(GetValorLista(0));
      DataBits    := TVaDataBits(GetValorLista(1));
      Parity      := TVaParity(GetValorLista(2));
      StopBits    := TVaStopBits(GetValorLista(3));
      DTRControl  := TVaControlDTR(GetValorLista(4));
      FlowControl := GetValorLista(5);
      RTSControl  := TVaControlRTS(GetValorLista(6));
      EventChar   := AnsiChar(GetValorLista(7));
    end;
  finally
    oLista.Free;
  end;
end;

function TBaseTerminalShell.SetInfoPuerto(const InfoPuerto: TInfoPuerto): String;
begin
  Result := VACIO;
  with InfoPuerto do begin
    Result := ConcatString(Result, IntToStr(Ord(BaudRate)), ',');
    Result := ConcatString(Result, IntToStr(Ord(DataBits)), ',');
    Result := ConcatString(Result, IntToStr(Ord(Parity)), ',');
    Result := ConcatString(Result, IntToStr(Ord(StopBits)), ',');
    Result := ConcatString(Result, IntToStr(Ord(DTRControl)), ',');
    Result := ConcatString( Result, IntToStr( FlowControl  ), ',' );
    Result := ConcatString(Result, IntToStr(Ord(RTSControl)), ',');
    Result := ConcatString(Result, IntToStr(Ord(EventChar)), ',');
  end;
end;

procedure TBaseTerminalShell.SerialResultado(const lOk: Boolean);
var
  lRes: Boolean;
begin
  if Assigned(FVaCommSalida) then begin
    lRes := FVaCommSalida.WriteText(aSerialSalida[lOk]);
    if not lRes then
      WarningMessage('Error al Transmitir a Puerto Serial')
    else begin
      lRes := TRUE;
      if (lOk) and StrLleno(TerminalRegistry.ApagadoAcepta) and (TerminalRegistry.EsperaAcepta <> 0) then begin
        Sleep(TerminalRegistry.EsperaAcepta);
        lRes := FVaCommSalida.WriteText(GetCodigosASCII(TerminalRegistry.ApagadoAcepta));
      end else if StrLleno(TerminalRegistry.ApagadoRechazo) and (TerminalRegistry.EsperaRechazo <> 0) then begin
        Sleep(TerminalRegistry.EsperaAcepta);
        lRes := FVaCommSalida.WriteText(GetCodigosASCII(TerminalRegistry.ApagadoRechazo));
      end;
      if not lRes then
        WarningMessage('Error al Transmitir a Puerto Serial');
    end;
  end;
end;

procedure TBaseTerminalShell.VaCommABreak(Sender: TObject);
begin
  WarningMessage('Break Signal Detected...');
end;

procedure TBaseTerminalShell.VaCommAError(Sender: TObject; Errors: Integer);
begin
  case Errors of
    CE_BREAK:
      WarningMessage(sCE_BREAK);
    CE_DNS:
      WarningMessage(sCE_DNS);
    CE_FRAME:
      WarningMessage(sCE_FRAME);
    CE_IOE:
      WarningMessage(sCE_IOE);
    CE_MODE:
      WarningMessage(sCE_MODE);
    CE_OOP:
      WarningMessage(sCE_OOP);
    CE_OVERRUN:
      WarningMessage(sCE_OVERRUN);
    CE_PTO:
      WarningMessage(sCE_PTO);
    CE_RXOVER:
      WarningMessage(sCE_RXOVER);
    CE_RXPARITY:
      WarningMessage(sCE_RXPARITY);
    CE_TXFULL:
      WarningMessage(sCE_TXFULL);
    else
      WarningMessage(Format('Error # %d', [Errors]));
  end;
end;

procedure TBaseTerminalShell.VaCommARx80Full(Sender: TObject);
begin
//  WarningMessage('Receiver Buffer Is 80% Full');
end;

procedure TBaseTerminalShell.VaCommATxEmpty(Sender: TObject);
begin
//  WarningMessage('TxEmpty Signal Detected...');
end;

{ Si se manejan varias formas se debe programar otro evento }

procedure TBaseTerminalShell.VaCommARxFlag(Sender: TObject);
var
  sTexto: String;
begin
  sTexto := TVaComm(Sender).ReadText;
  sTexto := Trim(Copy(sTexto, 1, Length(sTexto) - 1));
  if ZetaCommonTools.StrLleno(sTexto) then begin
    with FTerminalRegistry do begin
      case TipoGafete of
        egProximidad: begin
            case LectorProximidad of
              epSerial:
                sTexto := ZetaCommonTools.SerialToWiegand(sTexto);
              epClockData:
                sTexto := ZetaCommonTools.ClockAndDataToWiegand(sTexto);
            end;
          end;
      end;
    end;
    ProcesaChecadaSerial(sTexto);
  end;
end;

procedure TBaseTerminalShell.ProcesaChecadaSerial(const sTexto: String);
begin
  if (Self.Visible) and (Self.Active) then begin
    ZetaClientTools.EnviaMensaje(Self.ActiveControl, sTexto);
  end;
end;

procedure TBaseTerminalShell.SetLblDemo(const sMensaje: String);
begin
  lblDEMO.Caption := sMensaje
end;

{ Manejo de Timers }

procedure TBaseTerminalShell.TimerBannerTimer(Sender: TObject);
begin
  lblFecha.Caption := FormatDateTime('d/mmm/yy', Date);
  lblHora.Caption  := FormatDateTime('hh:mm AM/PM', Now);
end;

procedure TBaseTerminalShell.ResetTimerConnect( const lConectado: Boolean );
begin
     Desconectado.Enabled := ( not lConectado );
end;


procedure TBaseTerminalShell.ChecadaClick(Sender: TObject);
var
  oCursor: TCursor;
begin
  oCursor       := Screen.Cursor;
  Screen.Cursor := crHourglass;
  try
    ProcesaChecada;
    FocusCredencial;
  finally
    Screen.Cursor := oCursor;
  end;
end;

procedure TBaseTerminalShell.ChecadaEnter(Sender: TObject);
begin
     SetFocus;
end;

function TBaseTerminalShell.GetTerminalChecada: String;
begin
  Result := edCredencial.Text;
end;

procedure TBaseTerminalShell.FocusCredencial;
begin
  with edCredencial do begin
    SelStart  := 0;
    SelLength := Length(Text);
    SetFocus;
  end;
end;

procedure TBaseTerminalShell.AceptaChecada;
begin
  SerialResultado(TRUE);
  SonidoAcepta;
end;

procedure TBaseTerminalShell.AdvierteChecada;
begin
  SerialResultado(TRUE); // Acepta para abrir puertas
  SonidoRechaza;         // Pita rechazo para que se revise
end;

procedure TBaseTerminalShell.RechazaChecada;
begin
  SerialResultado(FALSE);
  SonidoRechaza;
end;

procedure TBaseTerminalShell.SonidoAcepta;
begin
  SndPlaySound({$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(TerminalRegistry.SonidoAcepta), SND_SYNC);
end;

procedure TBaseTerminalShell.SonidoRechaza;
begin
  SndPlaySound({$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(TerminalRegistry.SonidoRechazo), SND_SYNC);
end;

procedure TBaseTerminalShell.ShowTipoConexion(const lConectado: Boolean);
begin
  with LblConectado do
  begin
    if lConectado then
    begin
      Caption    := 'En línea';
      // Font.Color := clGreen; //$00CDEDEC
      Font.Color := RGB(147, 210, 80);
    end else begin
      Caption    := 'Fuera de línea';
      // Font.Color := clRed;
      Font.Color := RGB (227,66,51);
    end;
  end;
end;

procedure TBaseTerminalShell.CheckServidorDesconectado;
begin
  with TerminalRegistry do begin
    case FServidorDesconectado.ShowDlgDesconectado(CanWrite, Servidor, Puerto) of
      osReintentar:
        LeeParametros;
      osConfigurar:
        ConfiguraEstacion;
      osSalir:
        Close;
    end;
  end;
end;

end.
