unit ZBaseGridLecturaImss_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid, StdCtrls, Menus, ActnList, ImgList, TressMorado2013, ComCtrls,
  ZetaStateComboBox, ZBaseGridLectura_DevEx,ftressshell,ZetaCommonTools,ZetaCommonLists;

type
  TBaseGridLecturaImss_DevEx = class(TBaseGridLectura_DevEx)
    IMSSPanel: TPanel;
    IMSSMesLBL: TLabel;
    IMSSYearLBL: TLabel;
    IMSSPatronLBL: TLabel;
    IMSSTipoLBL: TLabel;
    IMSSMesCB: TStateComboBox;
    IMSSAnioCB: TStateComboBox;
    IMSSPatronCB: TStateComboBox;
    IMSSTipoCB: TStateComboBox;
    IMSSYearUpDown: TUpDown;
    procedure IMSSTipoCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure IMSSYearUpDownClick(Sender: TObject; Button: TUDBtnType);
    procedure IMSSAnioCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure IMSSMesCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure IMSSPatronCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);

  private
    { Private declarations }
  protected
    { Protected declarations }
    //DevEx (by ame): Proceso Agregado para asignar el MinWidth Ideal


  public
   { Public declarations }
  end;

var
  BaseGridLecturaImss_DevEx: TBaseGridLecturaImss_DevEx;

implementation

uses DCliente;

{$R *.dfm}

{ TCtasBancarias }


procedure TBaseGridLecturaImss_DevEx.IMSSTipoCBLookUp(Sender: TObject;
  var lOk: Boolean);
begin
  inherited;
  dmCliente.IMSSTipo := eTipoLiqIMSS( IMSSTipoCB.Indice );
  TressShell.RefrescaIMSSActivoPublico;
end;

procedure TBaseGridLecturaImss_DevEx.IMSSYearUpDownClick(Sender: TObject;
  Button: TUDBtnType);
begin
  inherited;
     with IMSSAnioCB do
     begin
          case Button of
               btNext: AsignaValorEntero( ValorEntero + 1 );
               btPrev: AsignaValorEntero( ValorEntero - 1 );
          end;
     end;
end;

procedure TBaseGridLecturaImss_DevEx.IMSSAnioCBLookUp(Sender: TObject;
  var lOk: Boolean);
begin
  inherited;
      dmCliente.IMSSYear := IMSSAnioCB.ValorEntero;
      TressSHell.RefrescaIMSSActivoPublico;
end;

procedure TBaseGridLecturaImss_DevEx.IMSSMesCBLookUp(Sender: TObject;
  var lOk: Boolean);
begin
  inherited;
     dmCliente.IMSSMes := IMSSMesCB.Indice;
     TressSHell.RefrescaIMSSActivoPublico;
end;

procedure TBaseGridLecturaImss_DevEx.IMSSPatronCBLookUp(Sender: TObject;
  var lOk: Boolean);
begin
  inherited;
     dmCliente.IMSSPatron := IMSSPatronCB.Llave;
     TressShell.RefrescaIMSSActivoPublico;
end;

procedure TBaseGridLecturaImss_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
          IMSSPatronCB.Llave := dmCliente.IMSSPatron;
          IMSSMesCB.Indice := dmCliente.IMSSMes;
          IMSSAnioCB.ValorEntero := dmCliente.IMSSYear;
          IMSSTipoCB.Indice := Ord( dmCliente.IMSSTipo );

end;

procedure TBaseGridLecturaImss_DevEx.FormShow(Sender: TObject);
begin
  inherited;
    dmCLiente.LeePatrones( Self.IMSSPatronCB.Lista );
    IMSSPatronCB.Llave := dmCliente.IMSSPatron;
    with self.IMSSPatronCB do
      begin
        DropDownCount := iMin( Items.Count, 12 );   // M�ximo muestra 12 sin que aparezca el Scroll
      end;
    IMSSMesCB.Indice := dmCliente.IMSSMes;
    IMSSAnioCB.ValorEntero := dmCliente.IMSSYear;
    IMSSTipoCB.Indice := Ord( dmCliente.IMSSTipo );

end;

end.
