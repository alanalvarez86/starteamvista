inherited ConfirmaVerificacion: TConfirmaVerificacion
  Left = 421
  Top = 401
  ActiveControl = OK
  Caption = '� Atenci�n !'
  ClientHeight = 151
  ClientWidth = 343
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Mensaje: TLabel [0]
    Left = 53
    Top = 32
    Width = 236
    Height = 48
    Alignment = taCenter
    AutoSize = False
    Caption = '� La Lista de %s Ya Ha Sido Verificada !'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    WordWrap = True
  end
  inherited PanelBotones: TPanel
    Top = 115
    Width = 343
    BevelOuter = bvNone
    inherited OK: TBitBtn
      Left = 1
      Top = 3
      Width = 124
      Height = 32
      Hint = 'Conservar La Lista Ya Verificada'
      Caption = '&Conservar Lista'
      Default = True
      ModalResult = 7
    end
    inherited Cancelar: TBitBtn
      Left = 258
      Top = 3
      Width = 81
      Height = 32
    end
    object CrearLista: TBitBtn
      Left = 128
      Top = 3
      Width = 124
      Height = 32
      Hint = 'Crear Una Nueva Lista Para Verificarla'
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = '&Crear Lista Nueva'
      ModalResult = 6
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Glyph.Data = {
        96010000424D9601000000000000760000002800000018000000180000000100
        0400000000002001000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333888888888888888333333330000000000000008833333
        30BBBBBBBBBBBBBBB083333330B8000000000008B083333330B0FFFFFFFFFFF0
        B08333334FB0F777777777F0B083333344F0FFFFFFFFFFF0B0833333444F7FFF
        FFFFFFF0B08334444444F7FF777777F0B083344444444F7FFFFFFFF0B0833444
        444444F7FFFFFFF0B083344444444F7FF77777F0B08334444444F7FFFFFFFFF0
        B0833333444F7FFFFFFFFFF0B083333344F0FF77777777F0B08333334FB0FFFF
        FFFFFFF0B083333330B0FFFFFFFFFFF0B083333330B0FF0000000FF0B0833333
        30B80007F7770008B083333330BBBBB07F70BBBBB03333333300000007000000
        0333333333333330F77033333333333333333330000033333333}
    end
  end
end
