unit FEscogeGlobal;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, Grids, DBGrids, StdCtrls, Buttons, ExtCtrls,
     ZBaseEscogeGrid,
     ZetaDBGrid;

type
  TEscogeGlobal = class(TBaseEscogeGrid)
    TipoGlobal: TComboBox;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TipoGlobalChange(Sender: TObject);
  private
    { Private declarations }
    procedure FiltraTipoGlobal;
  protected
    { Protected declarations }
    function GetFormula: String ; override;
    procedure QuitaFiltro;
    procedure Connect; override;
  public
    { Public declarations }
  end;

function PickGlobal: String;

var
  EscogeGlobal: TEscogeGlobal;

implementation

uses DDiccionario,
     ZetaCommonClasses;

{$R *.DFM}

function PickGlobal: String;
begin
     if ( EscogeGlobal = nil ) then
        EscogeGlobal:= TEscogeGlobal.Create( Application.MainForm );
     with EscogeGlobal do
     begin
          ShowModal;
          if ( ModalResult = mrOk ) then
             Result := Formula;
          QuitaFiltro;
     end;
end;

{ *********** TEscogeGlobal ********* }

procedure TEscogeGlobal.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H66216_Escoge_global;
     {
     SqlNumber:= Q_ESCOGE_GLOBAL;
     }
end;

procedure TEscogeGlobal.FormShow(Sender: TObject);
begin
     inherited;
     with TipoGlobal do
     begin
          if ( ItemIndex < 0 ) then
             ItemIndex := 5;
     end;
end;

procedure TEscogeGlobal.Connect;
begin
     with dmDiccionario do
     begin
          ConectaGlobal;
          DataSource.DataSet := cdsGlobal;
     end;
end;

function TEscogeGlobal.GetFormula: String;
begin
     with dmDiccionario.cdsGlobal do
     begin
          if not IsEmpty then
             Result:= 'Global( ' + FieldByName( 'GL_CODIGO' ).AsString + ' )'
          else
              Result:= '';
     end;
     {
     if DatasetLleno( FQuery ) then
        Result:= 'Global( ' + FQuery.FieldByName('GL_CODIGO').AsString + ' )';
     }
end;

procedure TEscogeGlobal.QuitaFiltro;
begin
     with dmDiccionario.cdsGlobal do
     begin
          if Filtered then
          begin
               Filtered := False;
               Filter := '';
          end;
     end;
end;

procedure TEscogeGlobal.TipoGlobalChange(Sender: TObject);
begin
     FiltraTipoGlobal;
     {
     GeneraQuery;
     }
end;

procedure TEscogeGlobal.FiltraTipoGlobal;
begin
     with dmDiccionario.cdsGlobal do
     begin
          try
             DisableControls;
             with TipoGlobal do
             begin
                  if ( ItemIndex <= 4 ) then
                  begin
                       Filter := Format( 'GL_TIPO = %d', [ ItemIndex ] );
                       Filtered := True;
                  end
                  else
                  begin
                       Filtered := False;
                       Filter := '';
                  end;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

{
procedure TEscogeGlobal.GeneraQuery;
var
   sScript: String;
begin
     FQuery := dmQuerys.QueryMgr.CreateQuery( SqlNumber, False );
     with FQuery do
     begin
          sScript := SqlText;
          if ( TipoGlobal.ItemIndex <= 4 ) then
             sScript := Format( sScript, [ 'where GL_TIPO = ' + IntToStr( TipoGlobal.ItemIndex ) ] )
          else
              sScript := Format( sScript, [ '' ] );
          Sql.Text:= sScript;
          Open;
     end;
     Datasource.Dataset:= FQuery;
end;
}

end.

