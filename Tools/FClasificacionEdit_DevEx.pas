unit FClasificacionEdit_Devex;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls,
     FCamposMgr,
     ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, ImgList, cxButtons;

type
  TEditClasificacion_DevEx = class(TZetaDlgModal_DevEx)
    CodigoLBL: TLabel;
    Codigo: TEdit;
    DescripcionLBL: TLabel;
    Descripcion: TEdit;
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FClasificacion: TClasificacion;
    FAlta: Boolean;
    FClasificaciones: TListaClasificaciones;
    procedure SetClasificacion(const Value: TClasificacion);
    procedure SetEsAlta(const Value: Boolean);
    procedure PosicionaPrimerControl;
  public
    { Public declarations }
    property Clasificacion: TClasificacion read FClasificacion write SetClasificacion;
    property EsAlta: Boolean write SetEsAlta;
    property Clasificaciones: TListaClasificaciones write FClasificaciones;
  end;

var
  EditClasificacion_DevEx: TEditClasificacion_DevEx;

implementation

uses {DMedico,}ZetaDialogo,ZetaCommonTools, ZetaCommonClasses;

{$R *.DFM}

{ TFEditClasificacion }

procedure TEditClasificacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
{
     Descripcion.MaxLength := K_MAX_LENGTH_DESCRIP;
}
     Descripcion.MaxLength := K_ANCHO_OBSERVACIONES;
end;

procedure TEditClasificacion_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PosicionaPrimerControl;
end;

procedure TEditClasificacion_DevEx.SetClasificacion(const Value: TClasificacion);
begin
     FClasificacion := Value;
     Codigo.Text := FClasificacion.Codigo;
     Descripcion.Text := FClasificacion.Nombre;
end;

procedure TEditClasificacion_DevEx.SetEsAlta(const Value: Boolean);
begin
     if Value then
        Caption := 'Agregar Clasificaci�n'
     else
         Caption := 'Modificar Clasificaci�n';
     FAlta:=Value;
     Codigo.Enabled := FAlta;   // Solo se puede editar en Cambios - Afecta a campos existentes de clasificaci�n
end;

procedure TEditClasificacion_DevEx.OKClick(Sender: TObject);
var
   lOk :Boolean;
   sCodigo,sNombre : String;
   oClasificacion : TClasificacion;
begin
     inherited;
     lOk := False;
     sCodigo := Codigo.Text;
     sNombre := Descripcion.Text;
     if StrVacio(sCodigo) then
     begin
          ZError(Caption,'C�digo No Puede Quedar Vac�o',0);
          ActiveControl := Codigo;
     end
     else if StrVacio(sNombre) then
     begin
          ZError(Caption,'La Descripci�n No Puede Quedar Vac�a',0);
          ActiveControl := Descripcion;
     end
     else
     begin
          if FAlta then
             oClasificacion := NIL
          else
              oClasificacion := FClasificacion;

          lOk := NOT FClasificaciones.Repetido( sCodigo, sNombre, oClasificacion );
          if NOT lOk then
          begin
               ZError(Caption,'Existe Otro Grupo Con Los Mismos Datos',0);
               PosicionaPrimerControl;
               ActiveControl := Codigo;
          end
     end;

     if lOk then
     begin
          FClasificacion.Codigo := Codigo.Text;
          FClasificacion.Nombre := Descripcion.Text;
          ModalResult := mrOk;
     end;
end;

procedure TEditClasificacion_DevEx.PosicionaPrimerControl;
begin
     if Codigo.Enabled then
        ActiveControl := Codigo
     else
        ActiveControl := Descripcion;
end;

end.
