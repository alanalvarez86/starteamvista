unit ZBaseTablasConsulta_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, Db, ExtCtrls, DBClient,
     ZBaseConsulta,
     ZetaDBGrid,
     ZetaClientDataSet,
     ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, StdCtrls,
  TressMorado2013, Menus, ActnList, ImgList, System.Actions;

type
  TTablaOpcion_DevEx = class(TBaseGridLectura_DevEx)
    TB_CODIGO: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    TB_INGLES: TcxGridDBColumn;
    TB_NUMERO: TcxGridDBColumn;
    TB_TEXTO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FDataset: TZetaClientDataset;
  protected
    { Protected declarations }
    property ZetaDataset: TZetaClientDataSet read FDataset write FDataset;
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure AfterCreate; virtual;
  public
    { Public declarations }
    procedure DoLookup; override;
    procedure Agrega_Tipo;
  end;
  TTablaLookup = class(TTablaOpcion_DevEx)
  private
    { Private declarations }
    procedure SetLookupDataset(const Value: TZetaLookupDataSet);
    function GetLookupDataset: TZetaLookupDataSet;
  protected
    { Protected declarations }
    property LookupDataset: TZetaLookupDataSet read GetLookupDataset write SetLookupDataset;
    procedure Connect; override;
  end;

var
  TablaOpcion_DevEx: TTablaOpcion_DevEx;

implementation

{$R *.DFM}

uses ZetaBuscador_DevEx,
     ZetaCommonClasses;

{ ******** TTablaOpcion ****** }

procedure TTablaOpcion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     AfterCreate;
end;

procedure TTablaOpcion_DevEx.AfterCreate;
begin
end;

procedure TTablaOpcion_DevEx.Agregar;
begin
     ZetaDataset.Agregar;
end;

procedure TTablaOpcion_DevEx.Agrega_Tipo;
var
   aCol: TcxGridDBColumn;
begin
    aCol := ZetaDBGridDBTableView.CreateColumn;
    with aCol do begin
           DataBinding.FieldName := 'TB_TIPO';
           Name := 'TB_TIPO';
           Caption := 'Tipo';
           Width := 90;
           Index := 2;
      end;
end;

procedure TTablaOpcion_DevEx.Borrar;
begin
     ZetaDataset.Borrar;
     //Aplica el BestFit para las columnas despues de que se ha eliminado el registro
     DoBestFit;
end;

procedure TTablaOpcion_DevEx.Connect;
begin
     ZetaDataset.Conectar;
     DataSource.DataSet := ZetaDataset;
end;

procedure TTablaOpcion_DevEx.Modificar;
begin
     ZetaDataset.Modificar;
end;

procedure TTablaOpcion_DevEx.Refresh;
begin
     ZetaDataset.Refrescar;
end;

procedure TTablaOpcion_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', ZetaDataset );
end;

{ ********** TTablaLookup ********** }

function TTablaLookup.GetLookupDataset: TZetaLookupDataSet;
begin
     Result := TZetaLookupDataset( Self.ZetaDataset );
end;

procedure TTablaLookup.SetLookupDataset(const Value: TZetaLookupDataSet);
begin
     Self.ZetaDataset := Value;
end;

procedure TTablaLookup.Connect;
begin
     inherited Connect;
     Caption := LookupDataset.LookupName;
end;

procedure TTablaOpcion_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth; //DevEx: Se aplica el min width despues de haber creado todas las columnas
  inherited;
end;

end.
