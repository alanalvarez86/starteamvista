unit ZAccesosTress;

interface

{$define QUINCENALES}

const
     {CV (RDD): Esta constante se agrego porque inicialmente en Tress el derecho #0 no existe.
                En Reporteador DataDriven, existen objetos (Entidades y Clasificaciones) cuyo codigo
                empieza en Cero, y no podemos hacer la renumeracion. Esto significa que el IMAGEINDEX en
                RDD empieza es -1}

     K_IMAGENINDEX                      = 0;
     K_IMAGENINDEXRDD                   =-1;
     K_IMAGENINDEX_LIMIT                = 9999;

     {Constantes para Derechos}
     {******** Empleados **********}
     D_EMPLEADOS                        = 1;
     D_EMP_DATOS                        = 2;
     D_EMP_DATOS_IDENTIFICA             = 3;
     D_EMP_DATOS_CONTRATA               = 4;
     D_EMP_DATOS_AREA                   = 5;
     D_EMP_DATOS_PERCEP                 = 6;
     D_EMP_DATOS_OTROS                  = 7;
     D_EMP_DATOS_ADICION                = 8;
     D_EMP_DATOS_GAFETE_BIOMETRICO      = 770;
     D_EMP_CURRICULUM                   = 9;
     D_EMP_CURR_PERSONALES              = 10;
     D_EMP_CURR_FOTO                    = 11;
     D_EMP_CURR_DOCUMENTO               = 629; // (JB) Anexar al expediente del trabajador documentos CR1880 T1107
     D_EMP_CURR_PARIENTES               = 12;
     D_EMP_CURR_EXPERIENCIA             = 13;
     D_EMP_CURR_CURSOS                  = 14;
     D_EMP_CURR_PUESTOS                 = 15;
     D_EMP_EXPEDIENTE                   = 16;
     D_EMP_EXP_KARDEX                   = 17;
     D_EMP_EXP_VACA                     = 18;
     D_EMP_EXP_INCA                     = 19;
     D_EMP_EXP_PERM                     = 20;
     D_EMP_EXP_CURSOS_TOMADOS           = 21;
     D_EMP_EXP_PAGOS_IMSS               = 22;
     D_EMP_EXP_CERTIFICACIONES          = 505;
     D_EMP_EXP_CERTIFIC_PROG            = 591;
     D_EMP_EXP_INFONAVIT                = 595;
     D_EMP_PLANCAPACITA                 = 679;
     D_EMP_HIS_COMPETEN                 = 703;
     D_EMP_HIS_EVA_COMPETEN             = 694;
     D_EMP_SEGUROS_GASTOS_MEDICOS       = 660;
     D_EMP_NOMINA                       = 23;
     D_EMP_NOM_AHORROS                  = 24;
     D_EMP_NOM_PRESTAMOS                = 25;
     D_EMP_NOM_PENSIONES                = 654;
     D_EMP_NOM_ACUMULA                  = 26;
     D_EMP_NOM_HISTORIAL                = 27;
     D_EMP_NOM_SIM_FINIQUITOS           = 605;
     D_EMP_SIM_FINIQUITOS_EMPLEADO      = 521;
     D_EMP_SIM_FINIQUITOS_TOTALES       = 606;
     D_EMP_REGISTRO                     = 28;
     D_EMP_REG_ALTA                     = 29;
     D_EMP_REG_BAJA                     = 30;
     D_EMP_REG_REINGRESO                = 31;
     D_EMP_REG_CAMBIO_SALARIO           = 32;
     D_EMP_REG_CAMBIO_TURNO             = 33;
     D_EMP_REG_CAMBIO_PUESTO            = 34;
     D_EMP_REG_CAMBIO_AREA              = 35;
     D_EMP_REG_CAMBIO_CONTRATO          = 36;
     D_EMP_REG_CAMBIO_PRESTA            = 37;
     D_EMP_REG_CAMBIO_PLAZA             = 520;
     D_EMP_REG_CAMBIO_TIPO_NOMINA       = 523;
     D_EMP_REG_VACACIONES               = 38;
     D_EMP_REG_INCAPACIDAD              = 39;
     D_EMP_REG_PERMISO                  = 40;
     D_EMP_REG_KARDEX                   = 41;
     D_EMP_REG_CURSO_TOMADO             = 42;
     D_EMP_REG_PRESTAMO                 = 373;
     D_EMP_PROCESOS                     = 43;
     D_EMP_PROC_RECALC_INT              = 44;
     D_EMP_PROC_PROM_VARIABLES          = 45;
     D_EMP_PROC_SALARIO_GLOBAL          = 46;
     D_EMP_PROC_VACA_GLOBAL             = 47;
     D_EMP_PROC_TABULADOR               = 48;
     D_EMP_PROC_APLICA_EVENTOS          = 49;
     D_EMP_PROC_IMPORTA_KARDEX          = 50;
     D_EMP_PROC_CANCELA_KARDEX          = 51;
     D_EMP_PROC_CURSO_GLOBAL            = 52;
     D_EMP_PROC_BORRAR_CURSO_GLOBAL     = 392;
     D_EMP_PROC_IMPORTAR_ALTAS          = 361;
     D_EMP_PROC_RENUMERA                = 366;
     D_EMP_PROC_PERMISO_GLOBAL          = 367;
     D_EMP_PROC_CANCELA_PERMISO_GLOBAL  = 516;
     D_EMP_PROC_COMIDAS_GRUPALES        = 396;
     D_EMP_PROC_CORREGIR_FECHAS_CAFE    = 397;
     D_EMP_PROC_IMP_ASISTENCIA_SESIONES = 457;
     D_EMP_PROC_IMP_AHORROS_PRESTAMOS   = 517;
     D_EMP_PROC_RECALCULA_SALDOS_VACA   = 519;
     D_EMP_PROC_FOLIAR_CAPACITACIONES   = 618;
     D_EMP_PROC_IMP_SGM                 = 661;
     D_EMP_PROC_RENUEVA_SGM             = 662;
     D_EMP_PROC_BORRA_SGM               = 663;
     D_EMP_PROC_REVISAR_CAPACITACIONES  = 671;
     D_EMP_PROC_REINICIAR_CAPACITACIONES= 672;
     D_EMP_PROC_IMPORTA_ORG             = 674;
     D_EMP_IMPORTAR_IMAGENES            = 714;
     D_EMP_PROG_CAMBIO_TURNO            = 751;

     {********** Caseta ************}
     D_EMP_CASETA                       = 598;
     D_EMP_CASETA_DIARIAS               = 599;
     D_EMP_REG_CASETA_DIARIAS           = 600;
     {******** Asistencia **********}
     D_ASISTENCIA                       = 53;
     D_ASIS_DATOS                       = 54;
     D_ASIS_DATOS_TARJETA_DIARIA        = 55;
     D_ASIS_DATOS_PRENOMINA             = 56;
     D_ASIS_DATOS_CALENDARIO            = 57;
     D_ASIS_REGISTRO                    = 58;
     D_ASIS_REG_AUTO_EXTRAS             = 59;
     D_ASIS_REG_HORARIO_TEMPO           = 60;
     D_ASIS_REG_AUTORIZACIONES_APROBAR  = 399;
     D_ASIS_PROCESOS                    = 61;
     D_ASIS_PROC_POLL_RELOJES           = 62;
     D_ASIS_PROC_PROC_TARJETAS          = 63;
     D_ASIS_PROC_CALC_PRENOMINA         = 64;
     D_ASIS_PROC_EXTRAS_GLOBAL          = 65;
     D_ASIS_PROC_REG_AUTOMATICO         = 66;
     D_ASIS_PROC_FECHAS_GLOBAL          = 67;
     D_ASIS_PROC_RECALC_TARJETA         = 68;
     D_ASIS_BLOQUEO_CAMBIOS             = 398;
     D_ASIS_DATOS_CLASIFI               = 407;
     D_ASIS_DATOS_CLASIFI_TEMP          = 408;
     D_ASIS_DATOS_CLASIFI_TEMP_NIVELES  = 409;
     D_ASIS_PROC_INTERCAMBIO_FESTIVOS   = 514;
     D_ASIS_PROC_CANCELAR_EXCEP_FEST    = 515;
     D_ASIS_AJUST_INCAPA_CAL            = 611; {acl 6.Abril.09}
     D_ASIS_PROC_AUTORIZ_PRENOM         = 622; {version 2011}
     D_ASIS_PROC_REGISTRAR_EXCEP_FEST   = 627;
     D_CAT_GRAL_CLASIF_DOCS             = 636; {EZ}
     D_ASIS_PROC_IMPORTA_CLASIFICACIONES = 637; {JB - Importacion de Clasificaciones Temporales}

     {******** N�mina **********}
     D_NOMINA                           = 69;
     D_NOM_DATOS                        = 70;
     D_NOM_DATOS_TOTALES                = 71;
     D_NOM_DATOS_DIAS_HORAS             = 72;
     D_NOM_DATOS_MONTOS                 = 73;
     D_NOM_DATOS_PRENOMINA              = 74;
     D_NOM_DATOS_CLASIFI                = 75;
     D_NOM_DATOS_POLIZAS	        = 506;
     D_NOM_EXCEPCIONES                  = 76;
     D_NOM_EXCEPCIONES_LIM_MONTO        = 612;
     D_NOM_EXCEP_DIAS_HORAS             = 77;
     D_NOM_EXCEP_MONTOS                 = 78;
     D_NOM_REGISTRO                     = 79;
     D_NOM_REG_NOMINA_NUEVA             = 80;
     D_NOM_REG_BORRA_ACTIVA             = 81;
     D_NOM_REG_EXCEP_DIA_HORA           = 82;
     D_NOM_REG_EXCEP_MONTO              = 83;
     D_NOM_REG_LIQUIDACION              = 84;
     D_NOM_PROCESOS                     = 85;
     D_NOM_PROC_CALCULAR                = 86;
     D_NOM_PROC_AFECTAR                 = 87;
     D_NOM_PROC_FOLIAR_RECIBOS          = 88;
     D_NOM_PROC_IMP_LISTADO             = 89;
     D_NOM_PROC_IMP_RECIBOS             = 90;
     D_NOM_PROC_CALC_POLIZA             = 91;
     D_NOM_PROC_IMP_POLIZA              = 92;
     D_NOM_PROC_EXP_POLIZA              = 93;
     D_NOM_PROC_CRED_APLICADO           = 360;
     D_NOM_PROC_DESAFECTAR              = 94;
     D_NOM_PROC_LIMPIA_ACUM             = 95;
     D_EMP_REG_CIERRE_VACA              = 96;
     D_CAT_NOMINA_PARAMETROS            = 97;
     D_NOM_PROC_IMP_MOVIMIEN            = 98;
     D_NOM_PROC_EXP_MOVIMIEN            = 99;
     D_NOM_PROC_PAGAR_FUERA             = 100;
     D_NOM_PROC_CANCELA_PASADA          = 101;
     D_NOM_PROC_LIQUIDA_GLOBAL          = 102;
     D_NOM_PROC_NETO_BRUTO              = 103;
     D_NOM_PROC_RASTREO                 = 104;
     D_NOM_PROC_RETROACTIVOS            = 374;
     D_NOM_PROC_AJUSTE_RET_FONACOT      = 535;
     D_NOM_PROC_AJUSTE_RET_FONACOT_CANC = 749;
     D_NOM_PROC_CALCULO_PAGO_FONACOT    = 536;
     D_NOM_FONACOT_IMPORTAR_CEDULA      = 767;
     D_NOM_FONACOT_GENERAR_CEDULA       = 768;
     D_NOM_FONACOT_ELIMINAR_CEDULA      = 769;
     D_NOM_ANUALES                      = 105;
     D_NOM_ANUAL_PERIODOS               = 106;
     D_NOM_ANUAL_CALC_AGUINALDO         = 107;
     D_NOM_ANUAL_CALC_AHORRO            = 108;
     D_NOM_ANUAL_CALC_PTU               = 109;
     D_NOM_ANUAL_CREDITO                = 110;
     D_NOM_ANUAL_COMPARA_ISPT           = 111;
     D_NOM_PROC_PREVIO_ISR              = 639;
     D_NOM_PROC_PREVIO_ISR_IMPRIMIR     = 640;

     {******** Pagos IMSS **********}
     D_IMSS                             = 112;
     D_IMSS_DATOS                       = 113;
     D_IMSS_DATOS_TOTALES               = 114;
     D_IMSS_DATOS_EMP_MEN               = 115;
     D_IMSS_DATOS_EMP_BIM               = 116;
     D_IMSS_HISTORIA                    = 117;
     D_IMSS_HIST_EMP_MEN                = 118;
     D_IMSS_HIST_EMP_BIM                = 119;
     D_IMSS_REGISTRO                    = 120;
     D_IMSS_REG_NUEVO                   = 121;
     D_IMSS_REG_BORRAR_ACTIVO           = 122;
     D_IMSS_PROCESOS                    = 123;
     D_IMSS_PROC_CALCULAR               = 124;
     D_IMSS_PROC_RECARGOS               = 125;
     D_IMSS_PROC_REVISAR_SUA            = 126;
     D_IMSS_PROC_EXP_SUA                = 127;
     D_IMSS_PROC_CALC_RIESGO            = 128;
     D_IMSS_CONCILIA_INFONAVIT          = 596;
     D_IMSS_PROC_AJUS_INFONAVIT         = 597;
     D_IMSS_PROC_VALID_MOV_IDSE         = 630; 
     {******** Consultas **********}
     D_CONSULTAS                        = 129;
     D_CONS_REPORTES                    = 130;
     D_CONS_ENVIOS_PROGRAMADOS          = 763;
     D_CONS_CONTEO_PERSONAL             = 131;
     D_CONTEO_CONFIGURACION             = 132;
     D_CONS_SQL                         = 133;
     D_CONS_ORGANIGRAMA                 = 375;
     D_CONS_PLANVACACION                = 508;
     {******** Cat�logos **********}
     D_CATALOGOS                        = 134;
     D_CAT_CONTRATACION                 = 135;
     D_CAT_CONT_PUESTOS                 = 136;
     D_CAT_DESCRIPCION_PUESTOS          = 511;
     D_CAT_DESC_PERF_PUESTOS            = 522;
     D_CAT_DESC_SECC_PERF               = 512;
     D_CAT_DESC_CAMPOS_PERF             = 513;
     D_CAT_VALUACION_PUESTOS            = 530;
     D_CAT_VALUACIONES                  = 534;
     D_CAT_VAL_PLANTILLAS               = 531;
     D_CAT_VAL_FACTORES                 = 532;
     D_CAT_VAL_SUBFACTORES              = 533;
     D_CAT_CONT_CLASIFI                 = 137;
     D_CAT_CONT_PER_FIJAS               = 138;
     D_CAT_CONT_TURNOS                  = 139;
     D_CAT_CONT_HORARIOS                = 140;
     D_CAT_CONT_PRESTA                  = 141;
     D_CAT_CONT_CONTRATO                = 142;
     D_CAT_NOMINA                       = 143;
     D_CAT_NOMINA_CONCEPTOS             = 144;
     D_CAT_NOMINA_PERIODOS              = 145;
     D_CAT_NOMINA_FOLIOS                = 146;
     D_CAT_NOMINA_POLIZAS               = 507;
     D_CAT_NOMINA_TPENSION              = 653;
{$ifdef QUINCENALES}
     D_CAT_TPERIODOS                    = 510;
{$endif}
     D_CAT_GENERALES                    = 147;
     D_CAT_GRALES_SOCIALES              = 566;
     D_CAT_GRALES_PATRONALES            = 148;
     D_CAT_GRALES_FEST_GRAL             = 149;
     D_CAT_GRALES_FEST_TURNO            = 150;
     D_CAT_GRALES_CONDICIONES           = 151;
     D_CAT_GRALES_EVENTOS               = 152;
     D_CAT_GRAL_SEG_GASTOS_MEDICOS      = 659;
     D_CAT_GRAL_TABLA_AMORTIZACION      = 664;
     D_CAT_CAPACITACION                 = 153;
     D_CAT_CAPA_CURSOS                  = 154;
     D_CAT_CLASIFI_CURSO                = 372;
     D_CAT_CAPA_MAESTROS                = 155;
     D_CAT_CAPA_PROVEEDORES             = 529;
     D_CAT_CAPA_MATRIZ_CURSO            = 156;
     D_CAT_CAPA_MATRIZ_PUESTO           = 157;
     D_CAT_CAPA_MATRIZ_CERT_PUESTO      = 592;
     D_CAT_CAPA_MATRIZ_PUESTO_CERT      = 593;
     D_CAT_CAPA_CALENDARIO              = 158;
     D_CAT_CONFIGURACION                = 159;
     D_CAT_CONFI_GLOBALES               = 160;
     //** Derechos de acceso de Globales de Empresa *****//
     D_CAT_CONFI_IDENTIFICA          = 488;
     D_CAT_CONFI_AREAS               = 489;
     D_CAT_CONFI_ADICIONALES         = 490;
     D_CAT_CONFI_RECURSOS_H          = 491;
     D_CAT_CONFI_NOMINA              = 492;
     D_CAT_CONFI_ASISTENCIA          = 493;
     D_CAT_CONFI_IMSS_INFO           = 494;
     D_CAT_CONFI_CAPACITACION        = 495;
     D_CAT_CONFI_VAR_GLOBALES        = 496;
     D_CAT_CONFI_EMPLEADOS           = 497;
     D_CAT_CONFI_CAFETERIA           = 498;
     D_CAT_CONFI_SUPERVISORES        = 499;
     D_CAT_CONFI_REP_EMAIL           = 500;
     D_CAT_CONFI_SEGURIDAD           = 501;
     D_CAT_CONFI_BITACORA            = 502;
     D_CAT_CONFI_BLOQ_ASIST          = 503;
     D_CAT_CONFI_ENROLL              = 580;
     D_CAT_CONFI_MISDATOS            = 634;
     D_CONS_BITACORA                 = 161;
     D_CAT_CONFI_DICCIONARIO         = 162;
     D_CAT_CAPA_AULAS                = 473;
     D_CAT_PREREQUISITOS_CURSO       = 474;
     D_CAT_CERTIFICACIONES           = 504;
     D_GLOBAL_DISPOSITIVOS           = 609;
     D_CAT_ESTABLECIMIENTOS          = 620;
     D_CAT_COMPETENCIAS              = 678;
     D_CAT_CPERFILES                 = 677;
     D_CAT_MAT_PERFILES_COMP         = 682;
     D_CAT_NODO_COMPETENCIAS         = 683;
     D_CAT_TAB_NAC_COMP              = 699;
     {*** US 13038: Notificaciones de Licencia y Timbrado ***}
     D_CAT_CONFI_NOTIFICACIONES      = 748;
     { HUECO 163 }
     {******** Tablas **********}
     D_TABLAS                           = 164;
     D_TAB_PERSONALES                   = 165;
     D_TAB_PER_EDOCIVIL                 = 166;
     D_TAB_PER_VIVE_EN                  = 167;
     D_TAB_PER_VIVE_CON                 = 168;
     D_TAB_PER_ESTUDIOS                 = 169;
     D_TAB_PER_TRANSPORTE               = 170;
     D_TAB_PER_ESTADOS                  = 171;
     D_TAB_PER_MUNICIPIOS               = 619;
     D_TAB_AREAS                        = 172;
     D_TAB_AREAS_NIVEL1                 = 173;
     D_TAB_AREAS_NIVEL2                 = 174;
     D_TAB_AREAS_NIVEL3                 = 175;
     D_TAB_AREAS_NIVEL4                 = 176;
     D_TAB_AREAS_NIVEL5                 = 177;
     D_TAB_AREAS_NIVEL6                 = 178;
     D_TAB_AREAS_NIVEL7                 = 179;
     D_TAB_AREAS_NIVEL8                 = 180;
     D_TAB_AREAS_NIVEL9                 = 181;
     { $ifdef ACS}
     D_TAB_AREAS_NIVEL10                 = 601;
     D_TAB_AREAS_NIVEL11                 = 602;
     D_TAB_AREAS_NIVEL12                 = 603;
     { $endif}
     D_TAB_ADICIONALES                  = 182;
     D_TAB_ADICION_TABLA1               = 183;
     D_TAB_ADICION_TABLA2               = 184;
     D_TAB_ADICION_TABLA3               = 185;
     D_TAB_ADICION_TABLA4               = 186;
     D_TAB_ADICION_TABLA5               = 426;
     D_TAB_ADICION_TABLA6               = 427;
     D_TAB_ADICION_TABLA7               = 428;
     D_TAB_ADICION_TABLA8               = 429;
     D_TAB_ADICION_TABLA9               = 430;
     D_TAB_ADICION_TABLA10              = 431;
     D_TAB_ADICION_TABLA11              = 432;
     D_TAB_ADICION_TABLA12              = 433;
     D_TAB_ADICION_TABLA13              = 434;
     D_TAB_ADICION_TABLA14              = 435;

     D_TAB_HISTORIAL                    = 187;
     D_TAB_HIST_TIPO_KARDEX             = 188;
     D_TAB_HIST_MOT_BAJA                = 189;
     D_TAB_HIST_INCIDEN                 = 190;
     D_CAT_CAPA_TIPO_CURSO              = 191;
     D_TAB_NOMINA                       = 192;
     D_TAB_NOM_TIPO_AHORRO              = 193;
     D_TAB_NOM_TIPO_PRESTA              = 194;
     D_TAB_NOM_MONEDAS                  = 195;
     D_TAB_NOM_REGLAS_PRESTA            = 604;
     D_TAB_OFICIALES                    = 196;
     D_TAB_OFI_SAL_MINIMOS              = 197;
     D_TAB_OFI_CUOTAS_IMSS              = 198;
     D_TAB_OFI_ISPT_NUMERICAS           = 199;
     D_TAB_OFI_GRADOS_RIESGO            = 200;
     D_TAB_COLONIA                      = 422;
     D_TAB_HIST_CHECA_MANUAL            = 613; 
     D_TAB_OFI_STPS                     = 615;
     D_TAB_OFI_STPS_CAT_NAC_OCUPA       = 616;
     D_TAB_OFI_STPS_ARE_TEM_CUR         = 617;
     D_TAB_CAPACITACION                 = 681;
     D_TAB_TCOMPETENCIAS                = 675;
     D_TAB_TPERFILES                    = 676;
     D_TAB_BANCOS                       = 715;
     D_TAB_OFI_SAT                      = 727;
     D_TAB_OFI_SAT_TIPO_CONTRATO        = 728;
     D_TAB_OFI_SAT_TIPO_JORNADA         = 729;
     D_TAB_OFI_SAT_RIESGO_PUESTO        = 730;
     D_TAB_UMA                          = 731;
     D_TAB_OFI_SAT_TIPOS_CONCEPTO       = 732;

     {******** Sistema **********}
     D_SISTEMA                          = 201;
     D_SIST_DATOS                       = 202;
     D_SIST_DATOS_EMPRESAS              = 203;
     D_SIST_DATOS_USUARIOS              = 204;
     D_SIST_DATOS_GRUPOS                = 205;
     D_CAT_ROLES                        = 581;
     D_SIST_DATOS_POLL_PENDIENTES       = 206;
     D_SIST_REGISTRO                    = 207;
     D_SIST_REG_EMPRESA_NUEVA           = 208;
     D_SIST_REG_BORRAR_EMPRESA          = 209;
     D_SIST_PROCESOS                    = 210;
     D_EMP_PROC_CANCELAR_VACA           = 211;
     D_SIST_PROC_BORRAR_BAJAS           = 212;
     D_SIST_PROC_BORRAR_TARJETAS        = 213;
     D_SIST_PRENDE_USUARIOS             = 368;     //Se dejar� hasta la pr�xima versi�n AP: 05/Jun/2007
     D_SIST_APAGA_USUARIOS              = 369;     //Se dejar� hasta la pr�xima versi�n AP: 05/Jun/2007
     D_SIST_ASIGNAR_SUPER               = 370;
     D_SIST_ASIGNAR_AREAS               = 371;
     D_SIST_PROC_NUMERO_TARJETA         = 403;
     D_SIST_BITACORA                    = 526;
     D_SIST_BITACORA_REPORTES           = 760;
     D_SIST_SOLICITUD_ENVIO             = 766;
     D_SIST_PROC_ENROLAMIENTO_MASIVO    = 582;
     D_SIST_PROC_IMP_ENROLAMIENTO_MAS   = 583;
     D_SIST_LST_DISPOSITIVOS            = 608;
     D_SIST_SOLICITUD_CORREOS           = 754;
     D_SIST_CALENDARIO_REPORTES         = 755;
     D_SIST_PROC_ASIGNA_NUM_BIO         = 642; // SYNERGY
     D_SIST_BITACORA_BIO                = 643; // SYNERGY
     D_SIST_PROC_DEPURA_BIT_BIO         = 644; // SYNERGY
     D_SIST_LISTA_GRUPOS                = 651; // SYNERGY
     D_SIST_TERM_GRUPO                  = 652; // SYNERGY
     D_SIST_PROC_ASIGNA_GRUPO_TERM      = 655; // SYNERGY
     D_SIST_ACTUALIZA_TERMINALES        = 657; // SYNERGY
     D_SIST_MENSAJES                    = 665; // SYNERGY
     D_SIST_MENSAJES_TERM               = 666; // SYNERGY
     D_SIST_TERMINALES                  = 667; // SYNERGY
     D_SIST_TERMINALESGTI               = 668; // SYNERGY
     D_SIST_BASE_DATOS                  = 708;
     D_SIST_PROC_BD_TRESS               = 709;
     D_SIST_PROC_BD_PRUEBAS             = 710;
     D_SIST_PROC_BD_SELECCION           = 711;
     D_SIST_PROC_BD_VISITANTES          = 712;
     D_SIST_PROC_BD_PRESUPUESTOS        = 713;
     D_SIST_ACTUALIZAR_BDS              = 717;
     D_SIST_TERM_EXTRAER_CHECADAS       = 725; // SYNERGY
     D_SIST_TERM_RECUPERAR_CHECADAS     = 726;// SYNERGY  @DC
     D_SIST_IMPORTAR_CAFETERIA          = 758;
     D_SIST_REPORTES_EMAIL              = 765;

     { *********** Nuevos ***********}
     D_NOM_PROC_COPIAR_NOMINA           = 214;
     D_EMP_EXP_CURSOS_PROGRAMADOS       = 215;
     D_EMP_REG_CAMBIO_MULTIPLE          = 216;
     D_EMP_PROC_CURSO_PROG_GLOBAL       = 607;
     D_EMP_PROC_CANC_CUR_PROG_GLOB      = 610;
     {********** Reportes ***********}
     D_REPORTES_EMPLEADOS               = 217;
     D_REPORTES_ASISTENCIA              = 218;
     D_REPORTES_NOMINA                  = 219;
     D_REPORTES_IMSS                    = 220;
     D_REPORTES_CONSULTAS               = 221;
     D_REPORTES_CATALOGOS               = 222;
     D_REPORTES_TABLAS                  = 223;
     D_REPORTES_MIGRACION               = 274;
     D_REPORTES_CURSOS                  = 275;
     D_REPORTES_ENVIO_EMAIL             = 635;

     D_REPORTES_ACCESOS                 = 423;
     D_REPORTES_KIOSCO                  = 424;
     D_REPORTES_CAJAAHORRO              = 518;
     D_REPORTES_LABOR                   = 276;
     D_REPORTES_EVALUACION              = 461;
     D_REPORTES_MEDICOS                 = 321;
     D_REPORTES_CARRERA                 = 391;
     D_INICIO                           = 750;
     D_TABLEROS_ROLES                   = 761;
     D_TABLEROS                         = 762;
     D_MOSTRAR_GRAFICAS                 = 752;
     //D_REPORTES_EVALUACION              = 425;

     { *********** Nuevos ***********}
     D_IMSS_PROC_CALC_TASA_INFO         = 224;
     { PENDIENTE ??? }
     //D_CHECADAS_EMPLEADO              = 225; //No se esta usando, se quito la rama del arbol
     D_NOM_PROC_RECALC_ACUMULADOS       = 226;
     D_EMP_PROC_TRANSFERENCIA           = 227;
     D_NOM_ANUAL_PAGA_AGUINALDO         = 228;
     { PENDIENTE: Repetido con 108? }
     D_NOM_ANUAL_CALC_AHORROS           = 229;
     D_NOM_ANUAL_CIERRE_AHORRO          = 230;
     D_NOM_ANUAL_CIERRE_PRESTA          = 231;
     D_SIST_PROC_BORRA_NOMINAS          = 232;
     D_SIST_PROC_BORRA_TIMBRADO_NOMINAS = 719; 
     D_NOM_ANUAL_IMP_AGUINALDO          = 233;
     D_NOM_ANUAL_IMP_AHORRO             = 234;
     D_NOM_ANUAL_DIF_COMPARA            = 235;
     D_NOM_ANUAL_IMP_COMPARA            = 236;
     D_NOM_ANUAL_IMP_CREDITO            = 237;
     D_NOM_ANUAL_IMP_PTU                = 238;
     D_NOM_ANUAL_PAGA_PTU               = 239;
     D_NOM_ANUAL_DECLARACION_ANUAL      = 393;
     D_NOM_ANUAL_IMP_DECLARACION_ANUAL  = 394;
     D_NOM_ANUAL_DECLARACION_ANUAL_CIERRE = 621;
     D_NOM_PROC_RECALC_AHORROS            = 756;
     D_NOM_PROC_RECALC_PRESTAMOS          = 764;

     {************ Cafeter�a **********}
     D_EMP_CAFETERIA                    = 240;
     D_EMP_CAFE_COMIDAS_DIA             = 241;
     D_EMP_CAFE_COMIDAS_PERIODO         = 242;
     D_EMP_CAFE_INVITACIONES            = 243;
     D_CAT_CAFETERIA                    = 244;
     D_CAT_CAFE_REGLAS                  = 245;
     D_CAT_CAFE_INVITADORES             = 246;
     D_REPORTES_CAFETERIA               = 247;
     D_REPORTES_SUPERVISORES            = 248;
     D_SIST_DATOS_IMPRESORAS            = 249;
     D_EMP_REG_COMIDAS                  = 250;
     D_NOM_EXCEP_GLOBALES               = 251;
     //D_NOM_REG_EXCEP_GLOBAL             = 251;
     { ********* Supervisores ****** }
     D_SUPER_AJUSTAR_ASISTENCIA         = 252;
     D_SUPER_PERMISOS                   = 253;
     D_SUPER_KARDEX                     = 254;
     D_SUPER_CONSULTAR_PRENOMINA        = 255;
     D_SUPER_BITACORA                   = 256;
     D_SUPER_ASIGNAR_EMPLEADOS          = 257;
     D_SUPER_DATOS_EMPLEADOS            = 258;
     D_NOM_REG_EXCEP_MONTO_GLOBAL       = 259;
     D_SUPER_AJUSTAR_ASISTENCIA_ESPECIAL= 260;
     D_SUP_PLAN_CAPACITA                = 696;
     D_SUP_MAT_HBLDS                    = 697;
     D_SUP_MAT_CURSOS                   = 759;
     D_SUP_EVAL_COMPETEN                = 698;

     { HUECO 261 }
     D_SUPER_AUTORIZACION_COLECTIVA     = 262;
     D_SUPER_TIPO_KARDEX                = 263;
     D_NOM_PROC_CALC_DIFERENCIAS        = 264;
     D_ASIS_DATOS_AUTORIZACIONES        = 265;
     D_TAB_HIST_AUT_EXTRAS              = 266;
     D_TAB_OFI_TIPOS_CAMBIO             = 267;
     D_REPORTES_CONFIDENCIALES          = 268;
     D_NOM_REG_PAGO_RECIBOS             = 269;
     D_NOM_PROC_RE_FOLIAR               = 270;
     D_NOM_PROC_IMP_PAGO_RECIBO         = 271;
     D_SUPER_VER_BAJAS                  = 272;
     D_SIST_BORRAR_POLL                 = 273;
     D_SUPER_REGISTRO_BREAKS            = 322;
     D_SUPER_CANCELA_BREAKS             = 323;
     D_SUPER_ACTIVA_CACHE               = 324;
     D_SUPER_PLANVACACION               = 509;
     D_SUSCRIPCION_REPORTE              = 527;
     D_SUSCRIPCION_USUARIOS             = 528;
     D_ENCRIPTACION_REPORTE             = 721; //Encriptaci�n Dispersi�n de N�mina
     D_SUPER_STATUS_EMPLEADO            = 614;

     //******Derechos de acceso de cursos en supervisores****//
     D_SUPER_CURSOS_TOMADOS             = 562;
     D_SUPER_CURSOS_PROGRAMADOS         = 563;
     D_SUPER_GRUPOS                     = 564;
     D_SUPER_SESIONES_OTROS             = 565;

     { ********* Accesos ****** }
     D_CAT_ACCESOS_REGLAS               = 406;

     {***CV estOS derechos estan con sus demas primos mas arriba.**}
     {**SI SE TIENEN QUE TOMAR EN CUENTA!!! ***}
     //D_REPORTES_MIGRACION             = 274;
     //D_REPORTES_CURSOS                = 275;
     //D_REPORTES_LABOR                 = 276;

     D_SIST_BORRAR_BITACORA             = 277;
     D_SUPER_VER_COLABORA               = 278;
     D_SUPER_TIPO_PERMISO               = 279;
     D_SUPER_LABOR                      = 280;
     D_SUPER_CEDULAS                    = 281;
     D_SIST_NIVEL0                      = 282;
     D_CAT_HERRAMIENTAS                 = 283;
     D_CAT_HERR_TOOLS                   = 284;
     D_TAB_HERR_TALLAS                  = 285;
     D_TAB_HERR_MOTTOOL                 = 286;
     D_EMP_EXP_TOOLS                    = 287;
     D_EMP_REG_ENTREGA_TOOLS            = 288;
     D_EMP_REG_ENTREGA_GLOBAL           = 289;
     D_EMP_REG_REGRESA_TOOLS            = 290;
     D_EMP_PROC_CIERRE_VACACIONES       = 291;
     D_EMP_PROC_ENTREGAR_HERRAMIENTA    = 292;
     D_EMP_PROC_REGRESAR_HERRAMIENTA    = 293;
     D_SIST_BORRAR_HERRAMIENTA          = 294;
     D_SUPER_ASIGNAR_AREAS              = 295;
     D_REPORTES_USUARIO_MODIFICAR       = 296;      // Derecho Disponible - NO se Usa
     D_EMP_PROC_CANCELA_CIERRE_VACA     = 460;
     D_EMP_PROC_CERTIFIC_EMP            = 524;
     D_EMP_PROC_CERTIFIC_GLOBAL         = 525;
     D_LAY_PRODUCCION                   = 623;

     //MA:Derechos de acceso para servicios medicos
     D_EMP_SERVICIO_MEDICO              = 297;
     D_EMP_SERV_EXPEDIENTE              = 298;
     D_REG_CONS_MED_GLOBAL              = 313;
     D_EMP_KARDEX_CONSULTAS             = 299;
     D_EMP_KARDEX_EMBARAZOS             = 300;
     D_EMP_KARDEX_ACCIDENTES            = 301;
     D_EMP_INCAPACIDADES                = 314;
     D_EMP_CLASE_PERMISO                = 315;
     D_SERV_REGISTRO                    = 302;
     D_SERV_PROCESOS                    = 303;
     D_SERV_CATALOGOS                   = 304;
     D_SERV_CONSULTAS                   = 316;
     D_CONSULTA_SQL                     = 317;
     D_CONSULTA_PROCESOS                = 318;
     D_CAT_MEDICAMENTOS                 = 305;
     D_CAT_DIAGNOSTICO                  = 306;
     D_CAT_TIPO_CONSULTA                = 307;
     D_CAT_TIPO_EST_LAB                 = 308;
     D_CAT_TIPO_ACC                     = 309;
     D_CAT_CAU_ACC                      = 310;
     D_CAT_MOT_ACC                      = 359;
     D_SERV_SISTEMA                     = 311;
     D_SERV_GLOBALES                    = 312;
     D_MODULO_MEDICO                    = 319;
     D_MEDICINA_ENTR                    = 320;
     D_PACIENTE_PERMISOS                = 458;
     D_PACIENTE_INCAPACIDADES           = 459;

     //Constantes de Acceso Para el M�dulo Labor
     D_LABOR                            = 325;
       D_LAB_EMPLEADOS                  = 326;  //Empleados:Datos,Operaciones,Ordenes Fijas,Kardex de Areas
         D_LAB_EMP_DATOS                = 327;  //Consultar,Imprimir
         D_LAB_EMP_OPERACIONES          = 328;  //Consultar,Agregar,Borrar,Modificar,Imprimir
         D_LAB_EMP_ORDENES_FIJAS        = 329;  //Consultar,Agregar,Borrar,Modificar,Imprimir
         D_LAB_EMP_KARDEX_AREAS         = 330;  //Consultar,Agregar,Borrar,Modificar,Imprimir
       D_LAB_LABOR                      = 331;  //Labor:Orden de Trabajo,Kardex de Orden de Trabajo,Historial de Orden de Trabajo, Registro, Procesos
         D_LAB_WORK_ORDER               = 332;  //Consultar,Agregar,Borrar,Modificar,Imprimir
         D_LAB_KARDEX_ORDER             = 333;  //Consultar,Imprimir
         D_LAB_HISTORIA_ORDENES         = 334;  //Consultar,Agregar,Borrar,Modificar,Imprimir
         D_LAB_CEDULAS                  = 343;  //Consultar,Borrar,Modificar,Imprimir,Editar Lista de Empleados
       D_LAB_CALIDAD                    = 364;
         D_LAB_CEDULAS_INSP             = 363;  //Consultar,Agregar,Borrar,Modificar,Imprimir,Editar Cedulas de otros Inspectores
         D_LAB_CEDULAS_SCRAP            = 365;  //Consultar,Agregar,Borrar,Modificar,Imprimir,Editar Cedulas de otros Inspectores
         D_LAB_CAT_TDEFECTO             = 362;  //Consultar,Agregar,Borrar,Modificar,Imprimir
         D_LAB_CAT_COMPONENTES          = 377;  //Consultar,Agregar,Borrar,Modificar,Imprimir
         D_LAB_CAT_MOT_SCRAP            = 378;  //Consultar,Agregar,Borrar,Modificar,Imprimir
         //D_TEXT_EDITAR_OTROS_INSP       = 365;
       D_LAB_REGISTRO                   = 335;  //Registro:Multi-Lote
           D_LAB_REG_MULTILOTE          = 336;
         D_LAB_PROCESO                  = 337;  //Procesos:C�lculo de Tiempos,Importar Ordenes de Trabajo,Importar Partes, Importar Componentes, Depuraci�n
           D_LAB_PROC_CALC_TIEMPOS      = 338;
           D_LAB_PROC_AFECTARLABOR      = 376;
           D_LAB_PROC_IMP_ORDENES       = 339;
           D_LAB_PROC_IMP_PARTES        = 340;
           D_LAB_PROC_IMP_COMPONENTES   = 379;
           D_LAB_PROC_IMP_CEDULAS       = 400;
           D_LAB_PROC_IMP_CAMBIO_AREAS  = 401;
           D_LAB_PROC_IMP_PROD_IND      = 402;
           D_LAB_PROC_DEPURACION        = 341;
         D_LAB_PLANTILLAS               = 624;
       D_LAB_CONSULTAS                  = 342;  //Consultas:C�dulas,Reportes*
         D_LAB_CONS_SQL                 = 356;  //Consultar,Imprimir
         D_LAB_CONS_PROCESOS            = 357;  //Consultar,Cancelar Procesos,Imprimir
       D_LAB_CATALOGOS                  = 344;  //Cat�logos:Partes,Operaciones,Clase de Partes,Clase de Operaciones,Areas,Tiempo Muerto,Breaks,Modulador #1,Modulador #2,Modulador #3,Globales
         D_LAB_CAT_PARTES               = 345;  //Consultar,Agregar,Borrar,Modificar,Imprimir
         D_LAB_CAT_OPERACIONES          = 346;  //Consultar,Agregar,Borrar,Modificar,Imprimir
         D_LAB_CAT_TIPO_PARTE           = 347;  //Consultar,Agregar,Borrar,Modificar,Imprimir
         D_LAB_CAT_TIPO_OPERACION       = 348;  //Consultar,Agregar,Borrar,Modificar,Imprimir
         D_LAB_CAT_AREA                 = 349;  //Consultar,Agregar,Borrar,Modificar,Imprimir
         D_LAB_CAT_MAQUINAS             = 625;
         D_LAB_CAT_TMAQUINAS            = 626;


         D_LAB_CAT_TIEMPO_MUERTO        = 350;  //Consultar,Agregar,Borrar,Modificar,Imprimir
         D_LAB_CAT_BREAKS               = 351;  //Consultar,Agregar,Borrar,Modificar,Imprimir
         D_LAB_CAT_MODULA_1             = 352;  //Consultar,Agregar,Borrar,Modificar,Imprimir
         D_LAB_CAT_MODULA_2             = 353;  //Consultar,Agregar,Borrar,Modificar,Imprimir
         D_LAB_CAT_MODULA_3             = 354;  //Consultar,Agregar,Borrar,Modificar,Imprimir
         D_LAB_CAT_GLOBAL               = 355;  //Consultar,Modificar,Imprimir

         D_SUPER_AREAS_SIN_ASIGNAR      = 358;
         D_SUPER_VACACIONES             = 404;
         D_SUPER_CAMBIO_TURNO           = 405;
         D_SUPER_CLASIFI                = 410;
         D_SUPER_CLASIFI_TEMP           = 411;
         D_SUPER_CLASIFI_TEMP_NIVELES   = 412;
         D_SUPER_AUTORIZAR_PRENOMINA    = 475;
         {$ifdef COMMSCOPE}
         D_SUPER_AUTORIZAR_EVALUACION   = 720;
         {$endif}

         {******** Cat�logos Plan de Carrera (Adm)**********}
         D_CARRERA                      = 380;
           D_CARRERA_CATALOGOS          = 381;
             D_CARRERA_CAT_TCOMPETEN    = 382;
             D_CARRERA_CAT_CALIFICA     = 383;
             D_CARRERA_CAT_COMPETEN     = 384;
             D_CARRERA_CAT_ACCION       = 385;
             D_CARRERA_CAT_PUESTO       = 386;
             D_CARRERA_CAT_FAMILIA      = 387;
             D_CARRERA_CAT_NIVEL        = 388;
             D_CARRERA_CAT_DIMENSIO     = 389;
           D_CARRERA_CONSULTAS          = 390;
           D_CARRERA_SISTEMA            = 413;
             D_CARRERA_GLOBALES         = 414;
         {******** Cat�logos Plan de Carrera (Web)**********}
         D_CARRERA_WEB                  = 415;
          D_CARRERA_CONSULTAS_WEB       = 416;
          D_CARRERA_PUESTOS_WEB         = 417;
          D_CARRERA_CATALOGOS_WEB       = 418;
          D_CARRERA_DETALLE_CATALOGOS   = 419;
          D_CARRERA_ANALITICOS_WEB      = 420;
          D_CARRERA_MIS_COLABORADORES   = 421;

           {******** Admin. Evaluaci�n/Encuestas (Adm)**********}
         D_EVALUACION                   = 425;
           D_EVAL_DISENO_ENCUESTA       = 436;
             D_CRITERIOS_EVAL           = 437;
             D_PREG_CRITERIO            = 438;
             D_ESCALAS                  = 439;
             D_PERFIL_EVAL              = 440;
             D_EMPLEADOS_EVAL           = 441;
             D_ENCUESTAS                = 442;
             D_DISENO_REG               = 443;
               D_AGREGAR_EMP_EVAL       = 476;
             D_DISENO_PROC              = 444;
               D_GENERAR_LISTA          = 477;
               D_PREPARA_EVAL           = 478;
               D_CALCULA_RESUL          = 479;
               D_RECALCULA_PROM         = 480;
               D_CORREOS_INVITACION     = 482;
               D_CORREOS_NOTIFICACION   = 483;
               D_ASIGNA_RELACIONES      = 485;
               D_CREA_ENCUESTA_SIMPLE   = 487;
           D_EVAL_RESULTADOS_ENC        = 445;
             D_TOTALES_ENCUESTA         = 446; //Ya se habia asignado para la Ver. 2.6 por eso qued� con ese n�mero
             D_EMPLEADOS_EVALUADOS      = 447;
             D_EVALUADORES              = 448;
             D_EVALUACIONES             = 449;
             D_ANALISIS_CRITERIO        = 450;
             D_ANALISIS_PREGUNTA        = 451;
             D_FRECUENCIA_RESP          = 452;
           D_EVA_RESULTADOS_INDIV       = 453;
             D_RESULTADO_CRITERIO       = 454;
             D_RESULTADO_PREGUNTA       = 455;
           D_EVALUACION_CONSULTAS       = 456;
             D_EVAL_CONSULTAS           = 462;
             D_EVAL_PROCESOS            = 463;
             D_EVAL_CORREOS             = 481;
             D_EVAL_REPORTA             = 484;
           D_EVALUACION_CATALOGOS       = 464;
             D_CAT_EVAL_COMPETEN        = 465;
             D_CAT_EVAL_PREGUNTAS       = 466;
             D_CAT_EVAL_TCOMPETEN       = 467;
             D_CAT_EVAL_ESCALAS         = 468;
           D_EVALUACION_SISTEMA         = 469;
             D_GLOBALES_EVALUACION      = 470;
             D_EVAL_DICCIONARIO         = 486;

    //******* Derechos de acceso de Capacitaci�n ***********//
           D_CAPACITACION               = 471;
             D_CAT_CAPA_SESIONES        = 395;
             D_CAPA_RESERVAS	        = 472;
             D_CAP_MATRIZ_HBLDS         = 695;
             D_CAP_MATRIZ_CURSOS        = 757;

    //******* Derechos de acceso de Caja de Ahorro *******//
      D_CAJA_AHORRO                     = 538;
          D_AHORRO_ADMON                = 539;
             D_AHORRO_SALDOS_X_EMP      = 540;
             D_AHORRO_TOT_CAJA_FONDO    = 541;
             D_AHORRO_EDO_CUENTA_BANC   = 542;
          D_AHORRO_ADMON_REGISTRO       = 543;
             D_AHORRO_REG_INSCRIP_EMP   = 544;
             D_AHORRO_REG_LIQ_RET       = 545;
             //D_AHORRO_REG_DEP_RET       = 546;
          D_AHORRO_ADMON_PROCESO        = 547;
            D_AHORRO_DEPOSITAR_RET      = 548;
          D_AHORRO_CONSULTAS            = 549;
            D_AHORRO_SQL                = 550;
            D_AHORRO_BITACORA           = 551;
          D_AHORRO_CATALOGOS            = 552;
            D_AHORRO_CAT_CUENTAS_BANC   = 553;
            D_AHORRO_CAT_TIPO_DEPOSITO  = 554;

    //******* Derechos de acceso de Timbrado *******//
    D_TIMBRADO_NOMINA                   = 733;
      D_TIMBRADO                        = 734;
       D_TIM_DATOS_PERIODOS             = 735;
       D_TIM_DATOS_SALDO                = 736;
       D_TIM_DATOS_CUENTAS              = 737;
       D_TIM_DATOS_CONTRIBUYENTES       = 738;
       D_TIM_REG_REGISTRAR_CONTRIBUYENTE= 739;
       D_TIM_REG_CANCELAR_CONTRIBUYENTE = 740;
       D_TIM_PROC_TIMBRAR_NOMINA        = 741;
       D_TIM_PROC_TOMAR_RECIBOS         = 742;
       D_TIM_PROC_TOMAR_RECIBOS_EMPLEADO= 743;
       D_TIM_PROC_ENVIAR_RECIBOS        = 744;
       D_TIM_PROC_CANCELAR_TIMBRADO     = 745;
       D_TIM_PROC_ADQUIRIR_FOLIOS       = 746;
       D_TIM_PROCESOS                   = 747;



    //******Derechos de acceso de Presupuesto****//
      D_PRESUPUESTO                     = 555;
        D_PRESUP_CONFI                  = 556;
          D_PRESUP_CONTRATACIONES       = 557;
          D_PRESUP_ESCENARIOS           = 628; // (JB) Escenarios de presupuestos T1060 CR1872
          D_PRESUP_SUPUESTO_PER         = 558;
        D_PRESUP_PROC                   = 559;
          D_PRESUP_PROC_SIMULA          = 560;
          D_PRESUP_PROC_IMPRIME         = 561;
          D_PRESUP_IMP_KARDEX           = 673;
          D_PRESUP_PROC_GEN_POLIZA      = 700;
          D_PRESUP_PROC_IMP_POLIZA      = 701;
          D_PRESUP_PROC_EXP_POLIZA      = 702;

      //****** CONFIGURADOR REPORTEADOR ****//
      D_CONFIGURADOR_REPORTEADOR        = 567;
      D_CR_REPORTEADOR                  = 568;
      D_CRR_REPORTES                    = 569;//--
      D_CRR_BITACORA_PROCESOS           = 570;
      D_CRR_SQL                         = 571;
      D_CR_CONFIGURACION                = 572;
      D_CRC_CATALOGOS_CLASIFICACIONES   = 573;//--
      D_CRC_DICCIONARIO_DATOS           = 574;
      D_CRC_CATALOGO_VALORES            = 575;//
      D_CRC_CATALOGOS_CONDICIONES       = 576;//
      D_CRC_CATALOGOS_MODULOS           = 577;//
      D_CR_SISTEMA                      = 578;
      D_CRS_GRUPO_USUARIOS              = 579;
      D_CRR_BITACORA                    = 585;
      D_CRC_PROCESO                     = 586;
      D_CRC_PROC_IMPORTAR               = 587;
      D_CRC_PROC_EXPORTAR               = 594;


      D_EMP_DATOS_USUARIO                = 584;

   //*******Derechos de Pagos Fonacot********//
       D_PAGOS_FONACOT                  = 588;
         D_PAGOS_FONACOT_TOT            = 589;
         D_PAGOS_FONACOT_TOT_EMPL       = 590;


   { Derechos de acceso para Env�o de Excepciones - Tec - Milenio }
     D_MOD_EXCEPCIONES     = 900;
     D_ADMON_EXCEPCIONES   = 901;
     D_EXCEPCIONES         = 902;
     D_MOTIVOS_PAGO        = 903;
     D_PROCESOS_EXCEPCIONES = 904;
     D_ENVIO_EXCEPCIONES   = 905;

   //Pantallas de Costeo
       D_COSTEO_TRANSFERENCIAS          = 631;
       D_COSTEO_MOTIVOS_TRANSFER        = 632;
       D_NOM_PROC_TRANSFERENCIAS_COSTEO = 633;
       D_NOM_PROC_CALCULO_COSTEO        = 645;
       D_SUPER_TRANSFERENCIAS           = 646;
       D_NOM_PROC_CANCEL_TRANSF_COSTEO  = 647;
       D_COSTEO_GRUPOS                  = 648;
       D_COSTEO_CRITERIOS               = 649;
       D_COSTEO_CRITERIOS_CONCEPTO      = 650;
       D_SUPER_APROBAR_TRANSFERENCIAS   = 656;

   { V2013
    Pantalla Otros }                      
       D_EMP_DATOS_OTROS_FONACOT = 684;
       D_EMP_DATOS_OTROS_PRIMER_EMPLEO = 685;
       D_EMP_DATOS_OTROS_ASISTENCIA = 686;
       D_EMP_DATOS_OTROS_EVALUACION = 687;
       D_EMP_DATOS_OTROS_CUENTAS = 688;
       D_EMP_DATOS_OTROS_NIVEL0 = 689;
       D_EMP_DATOS_OTROS_MEDICOS = 690;
       D_EMP_DATOS_OTROS_BRIGADAS = 691;
       D_EMP_DATOS_OTROS_INFONAVIT  = 692;
       D_EMP_DATOS_OTROS_MISDATOS  = 693;
       D_EMP_DATOS_OTROS_TIMBRADO = 716;


       D_DIFERENCIAS_TOTALES = 719;    //conciliador
     // Nota: Si agregas un derecho, aumenta K_MAX_DERECHOS
     {$ifdef TECMILENIO}
     K_MAX_DERECHOS                     = 929;
     {$else}
     K_MAX_DERECHOS                     = 771;
     {$endif}

implementation

end.
