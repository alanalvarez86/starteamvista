unit FDialogoLista_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, CheckLst, Buttons, ExtCtrls, ZetaClientDataset,
  ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, ImgList, cxButtons, cxControls, cxContainer,
  cxEdit, cxCheckListBox;

type
  TDialogoLista_DevEx = class(TZetaDlgModal_DevEx)
    cbLista: TcxCheckListBox;
    procedure FormShow(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FDataSetChecked: TZetaClientDataset;
    FDataSetInicial: TZetaLookupDataset;
    function YaExistia(sCodigo: string): Boolean;
  protected
    property DataSetChecked: TZetaClientDataset read FDataSetChecked write  FDataSetChecked;
    property DataSetInicial: TZetaLookupDataset read FDataSetInicial write  FDataSetInicial;
  public
    { Public declarations }
  end;

var
  DialogoLista_DevEx: TDialogoLista_DevEx;

implementation
uses
    ZetaDialogo,
    DDiccionario;

{$R *.dfm}



procedure TDialogoLista_DevEx.FormShow(Sender: TObject);
var
   sCodigo:string;
   iPosicion : integer;
begin
     inherited;
     iPosicion := 0;
     cbLista.Items.Clear;
     with DataSetInicial do
     begin
          First;
          cbLista.Clear;
          while not Eof do
          begin
               with cbLista.Items.Add do
               begin
                  Text := FieldByName( LookupKeyField ).AsString + '=' +
                                  FieldByName( LookupDescriptionField ).AsString;
                  Checked := false;

               end;

               sCodigo := FieldByName(LookupKeyField).AsString;
               if YaExistia( sCodigo ) then
                  cbLista.Items[iPosicion].Checked := TRUE;
               iPosicion := iPosicion + 1;
               Next;
          end;

          {First;
          while NOT EOF do
          begin
               cbLista.Items.AddObject(FieldByName(LookupDescriptionField).AsString, TObject(FieldByName(LookupKeyField).AsString));
               cbLista.Checked[cbLista.Items.Count-1] := FDataSetChecked.Locate(LookupKeyField,FieldByName(LookupKeyField).AsString,[]);
               Next;
          end;
          }
     end;

end;

function TDialogoLista_DevEx.YaExistia(sCodigo:string):Boolean;
begin
     Result := DataSetChecked.Locate(FDataSetInicial.LookupKeyField,sCodigo,[]);
end;



procedure TDialogoLista_DevEx.OK_DevExClick(Sender: TObject);
procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ;
begin
   ListOfStrings.Clear;
   ListOfStrings.Delimiter     := Delimiter;
   ListOfStrings.DelimitedText := Str;
end;
var
i:integer;
sCodigo:string;
Lista : TStringList;
begin
   inherited;
   lista := Tstringlist.create();
     for i:=0 to cbLista.Items.Count - 1 do
     begin
          with FDataSetChecked do
          begin
               Split('=',CbLista.Items[i].Text,Lista);
               sCodigo :=  trim(lista[0]);
               if cbLista.Items[i].Checked then
               begin
                    if not YaExistia( sCodigo ) then
                    begin
                         Append;
                         FieldByName(FDataSetInicial.LookupKeyField).AsString := sCodigo;
                         Post;
                    end;
               end
               else
               begin
                    if YaExistia( sCodigo ) then
                       Delete;
               end;
          end;
     end;
     FDataSetChecked.Enviar;


end;

end.

