inherited Buscador_DevEx: TBuscador_DevEx
  ActiveControl = Codigo
  Caption = 'Buscar '
  ClientHeight = 96
  ClientWidth = 250
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object CodigoLBL: TLabel [0]
    Left = 45
    Top = 21
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'CodigoLBL:'
    FocusControl = Codigo
  end
  object NoExisteLBL: TLabel [1]
    Left = 168
    Top = 21
    Width = 71
    Height = 13
    Caption = #161' No Existe !'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Visible = False
  end
  object Codigo: TEdit [2]
    Left = 103
    Top = 17
    Width = 58
    Height = 21
    TabOrder = 0
    OnChange = CodigoChange
    OnKeyPress = CodigoKeyPress
  end
  inherited PanelBotones: TPanel
    Top = 60
    Width = 250
    TabOrder = 1
    inherited OK_DevEx: TcxButton
      Left = 51
      ModalResult = 0
      OnClick = OK_DevExClick
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 131
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
