unit ZetaSystemWorking;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, dxGDIPlusClasses, cxImage, dxImageSlider,
  cxClasses;

type
  TZSystemWorking = class(TForm)
    PanelGlobal: TPanel;
    cxImage1: TcxImage;
    Panel1: TPanel;
    PanelMensaje: TLabel;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetMensaje( const Value: String );
    procedure WriteMensaje(const Value: String);
  end;

procedure InitAnimation( const sMensaje: String );
procedure EndAnimation;

var
  ZSystemWorking: TZSystemWorking;

implementation

{$R *.DFM}
procedure InitAnimation( const sMensaje: String );
begin
     EndAnimation;
     ZSystemWorking := TZSystemWorking.Create( Application );
     with ZSystemWorking do
     begin
          SetMensaje( sMensaje );
          Show;
          Update;
     end;
end;

procedure EndAnimation;
begin
     if ( ZSystemWorking <> nil ) then
     begin
       FreeAndNil(ZSystemWorking);
     end;
end;

{ ********* TZSystemWorking ********* }

procedure TZSystemWorking.FormShow(Sender: TObject);
begin
     Width := PanelMensaje.Width + 17;
end;

procedure TZSystemWorking.SetMensaje( const Value: String );
begin
     PanelMensaje.Caption := Value;
end;

procedure TZSystemWorking.WriteMensaje( const Value: String );
begin
     SetMensaje( Value );
     Application.ProcessMessages;
end;



end.
