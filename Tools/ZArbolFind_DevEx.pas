unit ZArbolFind_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ExtCtrls, ComCtrls, Buttons, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, cxButtons, cxTreeview;

type
  TBuscaArbol_DevEx = class(TForm)
    Nodo: TEdit;
    NodoLBL: TLabel;
    OK_DevEx: TcxButton;
    Salir_DevEx: TcxButton;
    procedure FormShow(Sender: TObject);
    //procedure OKClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FArbol: TcxTreeView;
    FCaption : String;
  public
    { Public declarations }
  end;

var
  BuscaArbol_DevEx: TBuscaArbol_DevEx;

procedure BuscaNodoDialogo( Target: TcxTreeView; sCaption : String );

implementation

{$R *.DFM}

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo,
     ZArbolTools;

procedure BuscaNodoDialogo( Target: TcxTreeView; sCaption : String );
begin
     if ( BuscaArbol_DevEx = nil ) then
        BuscaArbol_DevEx := TBuscaArbol_DevEx.Create( Application );

     BuscaNodoReset;
     with BuscaArbol_DevEx do
     begin
          FArbol   := Target;
          FCaption := sCaption;
          ShowModal;
     end;
end;

{ ********** TBuscaArbol ********** }

procedure TBuscaArbol_DevEx.FormShow(Sender: TObject);
begin
     with Nodo do
     begin
          SetFocus;
          SelectAll;
     end;
end;

{procedure TBuscaArbol_DevEx.OKClick(Sender: TObject);
begin
     with Nodo do
     begin
          if StrLleno( Text ) then
          begin
               BuscaNodoTexto( FArbol, FCaption, Text );
               SetFocus;
               SelectAll;
          end;
     end;
end; }



procedure TBuscaArbol_DevEx.OK_DevExClick(Sender: TObject);
begin
     with Nodo do
     begin
          if StrLleno( Text ) then
          begin
               BuscaNodoTexto( FArbol, FCaption, Text );
               SetFocus;
               SelectAll;
          end;
     end;
end;

end.
