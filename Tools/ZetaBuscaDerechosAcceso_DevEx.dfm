inherited BusquedaDerechosAcceso_DevEx: TBusquedaDerechosAcceso_DevEx
  Left = 275
  Top = 225
  Caption = 'B'#250'squeda de Derechos de Acceso'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelSuperior: TPanel
    inherited MostrarActivos: TCheckBox
      Visible = False
    end
  end
  inherited DBGrid_DevEx: TZetaCXGrid
    inherited DBGrid_DevExDBTableView: TcxGridDBTableView
      object SOURCE: TcxGridDBColumn [0]
        Caption = 'Derechos sobre'
        DataBinding.FieldName = 'AA_SOURCE'
        MinWidth = 100
        Width = 100
      end
      object MODULO: TcxGridDBColumn [1]
        Caption = 'M'#243'dulo'
        DataBinding.FieldName = 'MO_NOMBRE'
        MinWidth = 70
      end
      inherited Codigo_DevEx: TcxGridDBColumn
        Caption = 'Derecho'
        DataBinding.FieldName = 'AX_NUMERO'
        MinWidth = 80
      end
      inherited DBGrid_DevExDBTableViewColumn2: TcxGridDBColumn
        DataBinding.FieldName = 'AA_DESCRIP'
        MinWidth = 200
      end
    end
  end
  inherited cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
  end
end
