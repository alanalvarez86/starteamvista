unit ZetaWinAPITools;

interface

uses Windows, SysUtils, Forms, Consts, Registry, ShellAPI,
     WinSvc, Classes, ImageHlp, Controls, Dialogs, FileCtrl,
     PSAPI, TLHelp32, ShlObj, DBClient{$ifdef TRESSCFG},
     //System.IOUtils,
     System.Types  {$endif} {$IFDEF TRESS_DELPHIXE5_UP},System.IOUtils{$ENDIF}
     {$IFDEF DEBUGSENTINEL}
     // US #9660.
     , ZHardwareId
     {$ENDIF}
     ;

const
     K_DISK_A = 65; { Asc( 'A' ) }
     K_DISK_C = 67; { Asc( 'C' ) }
     K_DISK_Z = 90; { Asc( 'Z' ) }

  // Service Types
  SERVICE_KERNEL_DRIVER = $00000001;
  SERVICE_FILE_SYSTEM_DRIVER = $00000002;
  SERVICE_ADAPTER = $00000004;
  SERVICE_RECOGNIZER_DRIVER = $00000008;

  SERVICE_DRIVER =
    (SERVICE_KERNEL_DRIVER or
    SERVICE_FILE_SYSTEM_DRIVER or
    SERVICE_RECOGNIZER_DRIVER);

  SERVICE_WIN32_OWN_PROCESS = $00000010;
  SERVICE_WIN32_SHARE_PROCESS = $00000020;
  SERVICE_WIN32 =
    (SERVICE_WIN32_OWN_PROCESS or
    SERVICE_WIN32_SHARE_PROCESS);

  SERVICE_INTERACTIVE_PROCESS = $00000100;

  SERVICE_TYPE_ALL =
    (SERVICE_WIN32 or
    SERVICE_ADAPTER or
    SERVICE_DRIVER or
    SERVICE_INTERACTIVE_PROCESS);

type
  DWORDLONG = UInt64;

  PMemoryStatusEx = ^TMemoryStatusEx;
  TMemoryStatusEx = packed record
    dwLength: DWORD;
    dwMemoryLoad: DWORD;
    ullTotalPhys: DWORDLONG;
    ullAvailPhys: DWORDLONG;
    ullTotalPageFile: DWORDLONG;
    ullAvailPageFile: DWORDLONG;
    ullTotalVirtual: DWORDLONG;
    ullAvailVirtual: DWORDLONG;
    ullAvailExtendedVirtual: DWORDLONG;
  end;

function GlobalMemoryStatusEx(var lpBuffer: TMemoryStatusEx): BOOL; stdcall; external kernel32;

function BytesAMegas( const iBytes: Int64 ): Real;
function GetSize( const iBytes: Int64 ): String;
function ChangeServiceType( const sName: String; const dwType: DWORD ): Boolean;
function DriveIsFixed( const sDrive: String ): Boolean;
function FindDirectory( const sValue: String ): String;
function FindInPaths( var sFileName: String ): Boolean;
function FindService( const sName: String ): Boolean;overload;
function FindService( Manager: SC_HANDLE; const sName: String ): Boolean;overload;
function FormatDirectory( const sDirectory: String ): String;
function GetADOVersion: String;
function GetComputerName: String;
function GetBDEDirectory( var sDirectory: String ): Boolean;
function GetDiskSpace( const iDrive: Byte; var iTotalSpace, iFreeSpaceAvailable: Int64 ): Bool;
function GetEnvironmentVariable( const sVariable: String ): String;
function GetErrorMessage: String;
function GetGMTDateTime: TDateTime;
function GetInternetExplorerVersion: String;
function GetInterbaseServerDirectory: String;
function GetInterbaseIsInstalled: Boolean;
function GetFirebirdIsInstalled: Boolean;
function GetFirebirdServerDirectory: String;
function GetLongPathName( const sValue: String ): String;
function GetNumberOfProcessors: Integer;
function GetODBCAdministrator( var sFileName: String ): Boolean;
function GetODBCVersion: String;
function GetRegistryStringValue( const sKey, sName: String; var sValue: String ): Boolean; overload;
function GetRegistryDateTimeValue( const sKey, sName: String; var dValue: TDateTime): Boolean; overload;
function GetServiceStatus( const sName: String ): DWord; overload;
function GetServiceStatus( Manager: SC_HANDLE; const sName: String ): DWord; overload;
function GetTempDir: String;
function GetTransactionServerVersion: String;
function GetVersionInfo( const sFileName: String ): String;
function GetVersionInfoAlmostFull( const sFileName: String ): String;
function GetVersionInfoComments( const sFileName: String ): String;
function GetInfoDate(const FileName: string; var Created: TDateTime; var Accessed: TDateTime; var Modified: TDateTime): Boolean;
function GetInfoComments( const sFileName: String ): String;
function GetInfoFileDescription( const sFileName: String ): String;
function GetInfoProductVersion( const sFileName: String ): String;
function GetInfoFileVersion( const sFileName: String ): String;
function GetInfoDateModified( const sFileName: String ): String;
function GetDllVersion :System.Cardinal;
function GetVersionInfoFull( const sFileName: String ): String;
function GetWinSysDir: String;
function GetWindowsDir : string;
function GetMyDocumentsDir: string;
function InitService(const sName: String; const sMachineName: string = ''; const WaitForService: Boolean = True): Boolean;
function LockServices: Boolean;
function PauseService( const sName: String ): Boolean;
function OpenServiceManager: Boolean;overload;
function OpenServiceManager( var Manager: SC_HANDLE ): Boolean;overload;
function RemoveService( const sName: String; const sMachineName: string = '' ): Boolean;
function RestartService( const sName: String; const sMachineName: string = ''; const WaitForService: Boolean = True ): Boolean;
{$ifdef TRESSCFG}
function ServiceGetList(sMachine: string; dwServiceType, dwServiceState: DWord; cds: TClientDataSet) : boolean;
{$endif}
function ServiceGetListCorreo(sMachine: string; dwServiceType, dwServiceState: DWord; cds: TClientDataSet) : boolean;
function StopService(const sName: string; const sMachineName: string = ''; const WaitForService: Boolean = True ): Boolean;
function ProcessID( const sProcess: String ): Integer;
function ProcessIsRunning( const sProcess: String ): Boolean;
procedure ProcessWalk( Lista: TStrings );
procedure CloseServiceManager;overload;
procedure CloseServiceManager( var Manager: SC_HANDLE );overload;
procedure UnLockServices;
procedure WriteSystemInfo( Lineas: TStrings; lEscribirXML : boolean = FALSE  );
{$IFDEF DEBUGSENTINEL}
procedure WriteSystemShortInfo ( Lineas: TStrings );
{$ENDIF}
// US #9660.
{$IFDEF TRESSCFG}
procedure WriteFingerPrintData( Lineas: TStrings ; lEscribirXML : boolean );
{$ENDIF}
{$IFDEF DEBUGSENTINEL}
function GetOSSerial:AnsiString;
function GetBiosSerial:AnsiString;
{$ENDIF}
function GetWindowsVersionName( const iMajor, iMinor: Integer ): String;
function GetArchitecture( const iArchitecture, iLevel, iRevision: DWORD ): String;
function GetCurrentUserName: String;
function NetFrameworkInstalled(NetFrameWorkId:String): Boolean;
function GetNetFrameworkVersion: String;
{$ifdef TRESSCFG}
function GetLogicalDrives: TStringDynArray;
{$endif}
function GetDiskSize(iDrive : Integer): Currency;
function GetDiskFree(iDrive : Integer): Currency;
procedure SearchFile(const PathName, FileMask: string; Result: TStringList; const Recursive: Boolean = False);
function AddService( const ServiceName: String; const DisplayName: String; const ExecutablePath: String; const sMachineName: string = ''; const sUser : string = ''; const sPwd: string = ''): Boolean;

const
  K_INTERBASE_KEY   = 'SOFTWARE\Interbase Corp\Interbase\CurrentVersion';
  K_FIREBIRD_KEY    = 'SOFTWARE\Firebird Project\Firebird Server\Instances';
  K_NO_VERSION_INFO = 'INDETERMINADO';
  K_KILO            = 1024;
  K_MEGA            = K_KILO * K_KILO;
  K_GIGA            = K_MEGA * K_KILO;
  NetFWKv1          = 'SOFTWARE\Microsoft\.NETFramework\policy\v1.0';
  NetFWKv1_1        = 'SOFTWARE\Microsoft\NET Framework Setup\NDP\v1.1.4322';
  NetFWK2           = 'SOFTWARE\Microsoft\NET Framework Setup\NDP\v2.0.50727';
  NetFWKv3          = 'SOFTWARE\Microsoft\NET Framework Setup\NDP\v3.0';
  NetFWKv3_5        = 'SOFTWARE\Microsoft\NET Framework Setup\NDP\v3.5';
  NetFWKv4          = 'SOFTWARE\Microsoft\NET Framework Setup\NDP\v4';
  NetFWKv4_5        = 'SOFTWARE\Microsoft\NET Framework Setup\NDP\v4.5';
  NetFrameworkv1    = '.NET Framework v1.0';
  NetFrameworkv1_1  = '.NET Framework v1.1.4322';
  NetFrameworkv2    = '.NET Framework v2.0.50727';
  NetFrameworkv3    = '.NET Framework v3.0';
  NetFrameworkv3_5  = '.NET Framework v3.5';
  NetFrameworkv4    = '.NET Framework v4';
  NetFrameworkv4_5  = '.NET Framework v4.5';

implementation

uses ZetaCommonClasses, ZetaCommonTools;

type
    TVersion = record
      Major: Integer;
      Minor: Integer;
      Release: Integer;
      Build: Integer;
      Version : Integer;
      Comments: String;
      ProductVersion: String;
      FileDescription: String;
      FileVersion : String;
      DateTimeModified: TDateTime;
    end;
    TPathBuffer = array[ 0..MAX_PATH ] of Char;
var
   ServiceManager: SC_HANDLE;
   Lock: SC_LOCK;

function BoolToSiNo( const lValue: Boolean ): String;
begin
     if lValue then
        Result := 'Si'
     else
         Result := 'No';
end;


function BoolToSN( const lValue: Boolean ): String;
begin
     if lValue then
        Result := 'S'
     else
         Result := 'N';
end;

function HighWord( const Value: DWord ): Word;
begin
     Result := ( ( Value and $FFFF0000 ) shr $10 );
end;

function LowWord( const Value: DWord ): Word;
begin
     Result := ( Value and $0000FFFF );
end;

function FormatDirectory( const sDirectory: String ): String;
begin
     Result := sDirectory;
     if ( Result <> '' ) and ( Result[ Length( Result ) ] <> '\' ) then
        Result := Result + '\';
end;

function BytesAMegas( const iBytes: Int64 ): Real;
begin
     Result := iBytes / K_MEGA;
end;

function GetSize( const iBytes: Int64 ): String;
begin
     if ( iBytes < K_MEGA  ) then
        Result := Format( '%6.2n KB', [ iBytes / K_KILO ] )
     else
         if ( iBytes < K_GIGA  ) then
            Result := Format( '%6.2n MB', [ iBytes / K_MEGA ] )
         else
            Result := Format( '%6.2n GB', [ iBytes / K_GIGA ] );
end;

function GetCurrentUserName: String;
var
   pValue: array[ 0..127 ] of Char;
   iSize:DWord;
begin
     iSize := SizeOf( pValue );
     Windows.GetUserName( pValue, iSize );
     Result := pValue;
end;

function NetFrameworkInstalled(NetFrameWorkId:String): Boolean;
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create(KEY_READ);
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    Result := Reg.KeyExists(NetFrameWorkId);
  except
    Result := False;
  end;
  Reg.Free;
end;

function GetNetFrameworkVersion : String;
var Version : String;
begin
    if ZetaWinAPITools.NetFrameworkInstalled(NetFWKv1) then
    version := NetFrameworkv1;
    if ZetaWinAPITools.NetFrameworkInstalled(NetFWKv1_1) then
    version := NetFrameworkv1_1;
    if ZetaWinAPITools.NetFrameworkInstalled(NetFWK2) then
    version := NetFrameworkv2;
    if ZetaWinAPITools.NetFrameworkInstalled(NetFWKv3) then
    version := NetFrameworkv3;
    if ZetaWinAPITools.NetFrameworkInstalled(NetFWKv3_5) then
    version := NetFrameworkv3_5;
    if ZetaWinAPITools.NetFrameworkInstalled(NetFWKv4) then
    version := NetFrameworkv4;
    if ZetaWinAPITools.NetFrameworkInstalled(NetFWKv4_5) then
    version := NetFrameworkv4_5;

    Result :=  Version;
end;

function UserIsAdministrator: Boolean;
const
     SECURITY_DIALUP_RID = $1;
     SECURITY_NETWORK_RID = $2;
     SECURITY_BATCH_RID = $3;
     SECURITY_INTERACTIVE_RID = $4;
     SECURITY_SERVICE_RID = $6;
     SECURITY_ANONYMOUS_LOGON_RID = $7;
     SECURITY_PROXY_RID = $8;
     SECURITY_SERVER_LOGON_RID = $9;
     SECURITY_PRINCIPAL_SELF_RID = $A;
     SECURITY_AUTHENTICATED_USER_RID = $B;
     SECURITY_LOGON_IDS_RID = 5;
     SECURITY_LOGON_IDS_RID_COUNT = 3;
     SECURITY_LOCAL_SYSTEM_RID = $12;
     SECURITY_NT_NON_UNIQUE = $15;
     SECURITY_BUILTIN_DOMAIN_RID = $20;
     DOMAIN_ALIAS_RID_ADMINS = $220;
     DOMAIN_ALIAS_RID_USERS = $221;
     DOMAIN_ALIAS_RID_GUESTS = $222;
     DOMAIN_ALIAS_RID_POWER_USERS = $223;
     DOMAIN_ALIAS_RID_ACCOUNT_OPS = $224;
     DOMAIN_ALIAS_RID_SYSTEM_OPS = $225;
     DOMAIN_ALIAS_RID_PRINT_OPS = $226;
     DOMAIN_ALIAS_RID_BACKUP_OPS = $227;
     DOMAIN_ALIAS_RID_REPLICATOR = $228;
var
   hAccessToken: THandle;
   ptgGroups: PTokenGroups;
   dwInfoBufferSize: DWORD;
   psidAdministrators: PSID;
   x: Integer;
   bSuccess: BOOL;
   IdAuthority: TSIDIdentifierAuthority;
begin
     Result := False;
     bSuccess := OpenThreadToken( GetCurrentThread, TOKEN_QUERY, True, hAccessToken );
     if not bSuccess then
     begin
          if ( GetLastError = ERROR_NO_TOKEN ) then
             bSuccess := OpenProcessToken( GetCurrentProcess, TOKEN_QUERY, hAccessToken );
     end;
     if bSuccess then
     begin
          GetMem( ptgGroups, K_MEGA );
          try
             bSuccess := GetTokenInformation(hAccessToken, TokenGroups, ptgGroups, K_MEGA, dwInfoBufferSize );
             CloseHandle( hAccessToken );
             if bSuccess then
             begin
                  {
                  SECURITY_NT_AUTHORITY: ( $0, $0, $0, $0, $0, $5 ); // ntifs //
                  }
                  with IdAuthority do
                  begin
                       Value[ 0 ] := $0;
                       Value[ 1 ] := $0;
                       Value[ 2 ] := $0;
                       Value[ 3 ] := $0;
                       Value[ 4 ] := $0;
                       Value[ 5 ] := $5;
                  end;
                  AllocateAndInitializeSid( IdAuthority, 2, SECURITY_BUILTIN_DOMAIN_RID, DOMAIN_ALIAS_RID_ADMINS, 0, 0, 0, 0, 0, 0, psidAdministrators );
                  try
                     {$R-}
                     for x := 0 to ( ptgGroups.GroupCount - 1 ) do
                     begin
                          if EqualSid( psidAdministrators, ptgGroups.Groups[ x ].Sid ) then
                          begin
                               Result := True;
                               Break;
                          end;
                     end;
                     {$R+}
                  finally
                         FreeSid( psidAdministrators );
                  end;
             end;
          finally
                 FreeMem( ptgGroups );
          end;
     end;
end;

function GetVolumeSupportsNTFS( const sDrive: String ): Boolean;
var
   sFileSystem: String;
   NotUsed, VolFlags: DWORD;
   Buffer: array [ 0..MAX_PATH ] of Char;
begin
     Buffer[ 0 ] := #$00;
     if Windows.GetVolumeInformation( PChar( sDrive ), nil, 0,  nil, NotUsed, VolFlags, Buffer, DWORD( SizeOf( Buffer ) ) ) then
     begin
          SetString( sFileSystem, Buffer, StrLen( Buffer ) );
          Result := Pos( 'NTFS', sFileSystem ) > 0;
     end
     else
         Result := True;
end;

function GetFullVersion( const sFileName: String ): TVersion;
var
   Size, Value: DWord;
   pFileName: PChar;
   Data, Info: Pointer;
   iLen: UINT;
   fechaCreacion, ultimoAcceso, ultimaModificacion :TDateTime;
begin
     with Result do
     begin
          Major := 0;
          Minor := 0;
          Release := 0;
          Build := 0;
          Comments := '';
          ProductVersion := '';
          FileDescription := '';
          FileVersion := '';
          DateTimeModified := NullDateTime;
     end;
     if ( sFileName > '' ) and FileExists( sFileName ) then
     begin
          pFileName := StrAlloc( Length( sFileName ) + 1 );
          try
             pFileName := StrPCopy( pFileName, sFileName );
             Size := Windows.GetFileVersionInfoSize( pFileName, Value );
             if ( Size > 0 ) then
             begin
                  GetMem( Data, Size );
                  try
                     if Windows.GetFileVersionInfo( pFileName, Value, Size, Data ) then
                     begin
                          if Windows.VerQueryValue( Data, '\\', Info, iLen ) then
                          begin
                               with PVSFixedFileInfo( Info )^ do
                               begin
                                    with Result do
                                    begin
                                         Major := HighWord( dwFileVersionMS );
                                         Minor := LowWord( dwFileVersionMS );
                                         Release := HighWord( dwFileVersionLS );
                                         Build := LowWord( dwFileVersionLS );
                                    end;
                               end;
                          end;
                          if VerQueryValue( Data, pChar( 'StringFileInfo\040904E4\Comments' ), Info, iLen ) then
                          begin
                               Result.Comments := pChar( Info );
                          end;
                          if VerQueryValue( Data, pChar( 'StringFileInfo\040904E4\FileDescription' ), Info, iLen ) then
                          begin
                               Result.FileDescription := pChar( Info );
                          end;
                          if VerQueryValue( Data, pChar( 'StringFileInfo\040904E4\FileVersion' ), Info, iLen ) then
                          begin
                               Result.FileVersion := pChar( Info );
                          end;
                          if VerQueryValue( Data, pChar( 'StringFileInfo\040904E4\ProductVersion' ), Info, iLen ) then
                          begin
                               Result.ProductVersion := pChar( Info );
                          end;
                          if GetInfoDate( pFileName, fechaCreacion, ultimoAcceso, ultimaModificacion) then
                          begin
                               Result.DateTimeModified := ultimaModificacion;
                          end;

                     end;
                  finally
                         FreeMem( Data );
                  end;
             end;
          finally
                 StrDispose( pFileName );
          end;
     end;
end;

function GetInfoDate(const FileName: string; var Created: TDateTime;
         var Accessed: TDateTime; var Modified: TDateTime): Boolean;
var
  h: THandle;
  Info1, Info2, Info3: TFileTime;
  SysTimeStruct: SYSTEMTIME;
  TimeZoneInfo: TTimeZoneInformation;
  Bias: Double;
begin
  Result := False;
  Bias   := 0;
  h      := FileOpen(FileName, fmOpenRead or fmShareDenyNone);
  if h > 0 then
  begin
    try
      if GetTimeZoneInformation(TimeZoneInfo) <> $FFFFFFFF then
        Bias := TimeZoneInfo.Bias / 1440; // 60x24
      GetFileTime(h, @Info1, @Info2, @Info3);
      if FileTimeToSystemTime(Info1, SysTimeStruct) then
        Created := SystemTimeToDateTime(SysTimeStruct) - Bias;
      if FileTimeToSystemTime(Info2, SysTimeStruct) then
        Accessed := SystemTimeToDateTime(SysTimeStruct) - Bias;
      if FileTimeToSystemTime(Info3, SysTimeStruct) then
        Modified := SystemTimeToDateTime(SysTimeStruct) - Bias;
      Result := True;
    finally
      FileClose(h);
    end;
  end;
end;

function GetDllVersion: System.Cardinal;
var
  fn: string;
begin
  fn:= GetModuleName(HInstance);
  Result := SysUtils.GetFileVersion(fn);
end;

function GetVersionInfoAlmostFull( const sFileName: String ): String;
begin
     with GetFullVersion( sFileName ) do
     begin
          Result := Format( '%d.%d.%d', [ Major, Minor, Release ] );
     end;
end;

function GetVersionInfoFull( const sFileName: String ): String;
begin
     with GetFullVersion( sFileName ) do
     begin
          Result := Format( '%d.%d.%d.%d.%s.%s.%s', [ Major, Minor, Release, Build, FileDescription, DateToStr(DateTimeModified), FileVersion] );
     end;
end;

function GetVersionInfo( const sFileName: String ): String;
begin
     with GetFullVersion( sFileName ) do
     begin
          Result := Format( '%d.%d', [ Major, Minor ] );
     end;
end;

function GetVersionInfoComments( const sFileName: String ): String;
begin
     with GetFullVersion( sFileName ) do
     begin
          Result := Format( '%d.%d.%d.%d %s', [ Major, Minor, Release, Build, Comments ] );
     end;
end;

function GetInfoComments( const sFileName: String ): String;
begin
     with GetFullVersion( sFileName ) do
     begin
          Result := Format( '%s', [ Comments ] );
     end;
end;

function GetInfoFileDescription( const sFileName: String ): String;
begin
     with GetFullVersion( sFileName ) do
     begin
          Result := Format( '%s', [ FileDescription ] );
     end;
end;

function GetInfoProductVersion( const sFileName: String ): String;
begin
     with GetFullVersion( sFileName ) do
     begin
          Result := Format( '%s', [ ProductVersion ] );
     end;
end;

function GetInfoFileVersion( const sFileName: String ): String;
begin
     with GetFullVersion( sFileName ) do
     begin
          Result := Format( '%s', [ FileVersion ] );
     end;
end;

function GetInfoDateModified( const sFileName: String ): String;
begin
     with GetFullVersion( sFileName ) do
     begin
          Result := Format( '%s', [ DateToStr(DateTimeModified)] );
     end;
end;

function GetRegistryStringValue( const sKey, sName: String; var sValue: String ): Boolean;
begin
     sValue := '';
     with TRegistry.Create do
     begin
          try
             RootKey := HKEY_LOCAL_MACHINE;
             try
                Result := OpenKey( sKey, False );
                if Result then
                begin
                     sValue := ReadString( sName );
                     Result := True;
                end
             except
                   Result := False;
             end
          finally
                 Free;
          end;
     end;
end;

function GetRegistryDateTimeValue( const sKey, sName: String; var dValue: TDateTime): Boolean;
begin
     dValue := 0;
     with TRegistry.Create do
     begin
          try
             RootKey := HKEY_LOCAL_MACHINE;
             try
                 Result := OpenKey( sKey, False );
                 if Result then
                 begin
                      dValue := ReadDateTime( sName );
                      Result := True;
                 end
             except
                   Result := False;
             end
          finally
                 Free;
          end;
     end;
end;

function FindDirectory(const sValue: string): string;
var
  cBuffer: TPathBuffer;
begin
//  if SearchTreeForFile(PChar(ExtractFileDrive(sValue), PChar(sValue), cBuffer) then begin
if SearchTreeForFile(PAnsiChar( {$ifdef TRESS_DELPHIXE5_UP}AnsiString{$endif}( ExtractFileDrive(sValue) ) ), PAnsiChar({$ifdef TRESS_DELPHIXE5_UP}AnsiString{$endif}( sValue ) ), @cBuffer) then begin
    Result := cBuffer;
  end else
    Result := ''
end;

function GetComputerName: String;
var
   LocalMachine: array [ 0..MAX_COMPUTERNAME_LENGTH ] of Char;
   Size: DWORD;
begin
     Size := SizeOf( LocalMachine );
     if Windows.GetComputerName( LocalMachine, Size ) then
        Result := LocalMachine
     else
         RaiseLastWin32Error;
end;

function GetEnvironmentVariable( const sVariable: String ): String;
var
   cBuffer: array[ 0..4095 ] of Char;
begin
     Windows.GetEnvironmentVariable( pChar( sVariable ), cBuffer, SizeOf( cBuffer ) );
     Result := cBuffer;
end;

function GetWindowsDir : string;
var
   cBuffer: TPathBuffer;
begin
     if ( Windows.GetWindowsDirectory( cBuffer, SizeOf( cBuffer ) ) = 0 ) then
        RaiseLastWin32Error
     else
         Result := cBuffer;
end;

function GetWinSysDir: String;
var
   cBuffer: TPathBuffer;
begin
     if ( Windows.GetSystemDirectory( cBuffer, SizeOf( cBuffer ) ) = 0 ) then
        RaiseLastWin32Error
     else
         Result := cBuffer;
end;

function GetTempDir: String;
var
   {$IFDEF TRESS_DELPHIXE5_UP}
     cBufferResult: String;
   {$ELSE}
     cBuffer: TPathBuffer;
   {$ENDIF}
begin
    {***(@am): Para la compilacion del proyecto en XE5 la logica que manejabamos para
               obtener el path de Temp ya estaba obsoleta por lo que necesitamos obtener
               este path con las nueva version de esta funcion. ***}
    {$IFDEF TRESS_DELPHIXE5_UP}
     cBufferResult := System.IOUtils.TPath.GetTempPath;
     if Length( cBufferResult )<=0 then
         RaiseLastWin32Error
     else
         Result := cBufferResult;
    {$ElSE}
        if ( Windows.GetTempPath( SizeOf( cBuffer ), cBuffer ) = 0 ) then
            RaiseLastWin32Error
        else
            Result := cBuffer;
    {$ENDIF}
end;

function OpenServiceManager( var Manager: SC_HANDLE ): Boolean;
begin
     Result := False;
     Manager := WinSvc.OpenSCManager( nil, nil, SC_MANAGER_ALL_ACCESS );

     if ( Manager = 0 ) then
        RaiseLastWin32Error
     else
         Result := True;
end;

function OpenServiceManager: Boolean;
begin
     Result := FALSE;
     try
        Result := OpenServiceManager( ServiceManager );
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

procedure CloseServiceManager( var Manager: SC_HANDLE );overload;
begin
     WinSvc.CloseServiceHandle( Manager );
     Manager := 0;
end;

procedure CloseServiceManager;
begin
     CloseServiceManager( ServiceManager );
end;

function LockServices: Boolean;
begin
     Result := False;
     Lock := WinSvc.LockServiceDatabase( ServiceManager );
     try
        if ( Lock = nil ) then
           RaiseLastWin32Error
        else
            Result := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

procedure UnLockServices;
begin
     WinSvc.UnlockServiceDatabase( Lock );
     Lock := nil;
end;

function ChangeServiceType( const sName: String; const dwType: DWORD ): Boolean;
var
   Service: SC_HANDLE;
begin
     Result := False;
     try
        Service := WinSvc.OpenService( ServiceManager, pChar( sName ), SERVICE_CHANGE_CONFIG );
        if ( Service = 0 ) then
           RaiseLastWin32Error
        else
        begin
             try
                if ChangeServiceConfig( Service,
                                        dwType,
                                        SERVICE_NO_CHANGE,
                                        SERVICE_NO_CHANGE,
                                        nil,
                                        nil,
                                        nil,
                                        nil,
                                        nil,
                                        nil,
                                        nil ) then
                begin
                     Result := True;
                end
                else
                    RaiseLastWin32Error;
             finally
                    CloseServiceHandle( Service );
             end;
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

function FindService( Manager: SC_HANDLE; const sName: String ): Boolean;
var
   Service: SC_HANDLE;
begin
     Result := False;
     Service := WinSvc.OpenService( Manager, pChar( sName ), SERVICE_INTERROGATE );
     if ( Service <> 0 ) then
     begin
          Result := True;
          CloseServiceHandle( Service );
     end;
end;

function FindService( const sName: String): Boolean;
var
   hSCMgr: SC_HANDLE;
   sMachineName : String;
begin
     Result := FALSE;
     sMachineName := '';
     try
        hSCMgr := OpenSCManager(PChar(sMachineName), nil, SC_MANAGER_CREATE_SERVICE);
        Result := FindService( hSCMgr, sName );
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

// Fuente: http://www.delphifaq.com/faq/delphi_windows_API/f521.shtml
function InitService(const sName: String; const sMachineName: string = ''; const WaitForService: Boolean = True): Boolean;
var
  schm, schs: SC_HANDLE;
  ss        : TServiceStatus;
  psTemp    : PChar;
  dwChkP    : DWORD;
  Counter   : Integer;
begin
  if sMachineName = '' then
    psTemp := nil
  else
    psTemp := @sMachineName[1];

  ss.dwCurrentState := 1;
  schm := OpenSCManager(psTemp, nil, SC_MANAGER_CONNECT);
  if schm > 0 then begin
    schs := OpenService(schm, PChar(sName), SERVICE_START or SERVICE_QUERY_STATUS);
    if schs > 0 then begin
      psTemp := nil;
      if StartService(schs, 0, psTemp) then
        if QueryServiceStatus(schs, ss) then
          if WaitForService then begin
            Counter := 0;
            while ss.dwCurrentState <> SERVICE_RUNNING do begin
              dwChkP := ss.dwCheckPoint;
              Sleep(ss.dwWaitHint);
              if (Counter > 10) or (not QueryServiceStatus(schs, ss)) or (ss.dwCheckPoint < dwChkP) then
                Break;
              Inc(Counter);
            end;
          end;
      CloseServiceHandle(schs);
    end;
    CloseServiceHandle(schm);
  end;
  Result := SERVICE_RUNNING = ss.dwCurrentState;
end;

// Fuente: http://www.chami.com/tips./delphi/031598D.html
function StopService(const sName: String; const sMachineName: string = ''; const WaitForService: Boolean = True): Boolean;
var
  schm, schs: SC_Handle;
  ss: TServiceStatus;
  dwChkP: DWord;
begin
  schm := OpenSCManager(PChar(sMachineName), nil, SC_MANAGER_CONNECT);
  if (schm>0) then
  begin
    schs := OpenService(schm, PChar(sName), SERVICE_STOP or
      SERVICE_QUERY_STATUS);
    if (schs>0) then
    begin
      if (ControlService(schs, SERVICE_CONTROL_STOP, ss)) then
        if (QueryServiceStatus(schs, ss)) then
          while (SERVICE_STOPPED<>ss.dwCurrentState) do
          begin
            dwChkP := ss.dwCheckPoint;
            Sleep(ss.dwWaitHint);
            if (not QueryServiceStatus(schs, ss)) then
              Break;
            if (ss.dwCheckPoint < dwChkP) then
              Break;
          end;
      CloseServiceHandle(schs);
    end;
    CloseServiceHandle(schm);
  end;
  Result := SERVICE_STOPPED=ss.dwCurrentState;
end;

function RestartService (const sName: String; const sMachineName: string = ''; const WaitForService: Boolean = True ): Boolean;
begin
  Result := False;
  if StopService(sName, sMachineName, True) then
    Result := InitService(sName, sMachineName, WaitForService);
end;

function GetServiceStatus( Manager: SC_HANDLE; const sName: String ): DWord;
var
   Service: SC_HANDLE;
   Data: TServiceStatus;
begin
     Result := 0;
     Service := WinSvc.OpenService( Manager, pChar( sName ), SERVICE_ALL_ACCESS );
     if ( Service = 0 ) then
        RaiseLastWin32Error
     else
     begin
          try
             if WinSvc.QueryServiceStatus( Service, Data ) then
                Result := Data.dwCurrentState
             else
                 RaiseLastWin32Error;
          finally
                 CloseServiceHandle( Service );
          end;
     end;
end;

function GetServiceStatus( const sName: String ): DWord;
begin
     try
        Result := GetServiceStatus( ServiceManager, sName );
     except
           on Error: Exception do
           begin
                Result := 0;
                Application.HandleException( Error );
           end;
     end;
end;

function PauseService( const sName: String ): Boolean;
var
   Service: SC_HANDLE;
   Data: TServiceStatus;
begin
     Result := False;
     try
        Service := WinSvc.OpenService( ServiceManager, pChar( sName ), SERVICE_PAUSE_CONTINUE );
        if ( Service = 0 ) then
           RaiseLastWin32Error
        else
        begin
             try
                if WinSvc.ControlService( Service, SERVICE_CONTROL_PAUSE, Data ) then
                   Result := True
                else
                    RaiseLastWin32Error;
             finally
                    CloseServiceHandle( Service );
             end;
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

function RemoveService( const sName: String; const sMachineName: string = '' ): Boolean;
var
   hSCMgr, Service: SC_HANDLE;
begin
    Result := False;
    try
    begin
        hSCMgr := OpenSCManager(PChar(sMachineName), nil, SC_MANAGER_CREATE_SERVICE);
        if hSCMgr > 0 then
        begin
           try
              Service := WinSvc.OpenService( hSCMgr, pChar( sName ), SERVICE_ALL_ACCESS );
              if ( Service = 0 ) then
                 //RaiseLastWin32Error
              else
              begin
                   try
                      if WinSvc.DeleteService( Service ) then
                         Result := True;
                   finally
                          CloseServiceHandle( Service );
                   end;
              end;
           except
                 on Error: Exception do
                 begin
                      Application.HandleException( Error );
                 end;
           end
        end;
    end;
    except
        on Error: Exception do
        begin
            Application.HandleException( Error );
        end;
    end
end;

function FindInPaths( var sFileName: String ): Boolean;
var
   cBuffer: TPathBuffer;
   cFilePart: pChar;
begin
     if ( SearchPath( nil,
                      pChar( sFileName ),
                      pChar( ExtractFileExt( sFileName ) ),
                      SizeOf( cBuffer ),
                      cBuffer,
                      cFilePart ) > 0 ) then
     begin
          Result := True;
          sFileName := cBuffer;
     end
     else
         Result := False;
end;

function GetInternetExplorerVersion: String;
const
     K_IE_SETUP_KEY = 'SOFTWARE\Microsoft\IE Setup\SETUP';
     K_IE_PATH = 'Path';
var
   sDirectory: String;
begin
     if GetRegistryStringValue( K_IE_SETUP_KEY, K_IE_PATH, sDirectory ) then
        Result := GetVersionInfo( FormatDirectory( sDirectory ) + 'Iexplore.exe' )
     else
         Result := K_NO_VERSION_INFO;
end;

function GetInterbaseVersion: String;
const
     K_INTERBASE_VERSION = 'Version';
var
   sVersion: String;
begin
     if GetRegistryStringValue( K_INTERBASE_KEY, K_INTERBASE_VERSION, sVersion ) then
        Result := sVersion
     else
         Result := K_NO_VERSION_INFO;
end;

function GetInterbaseServerDirectory: String;
const
     K_INTERBASE_BIN = 'ServerDirectory';
var
   sDirectory: String;
begin
     if GetRegistryStringValue( K_INTERBASE_KEY, K_INTERBASE_BIN, sDirectory ) then
        Result := FormatDirectory( sDirectory )
     else
         Result := '';
end;

function GetFirebirdServerDirectory: String;
const
     K_FIREBIRD_BIN = 'DefaultInstance';
var
   sDirectory: String;
begin
     if GetRegistryStringValue( K_FIREBIRD_KEY, K_FIREBIRD_BIN, sDirectory ) then
        Result := FormatDirectory( FormatDirectory( sDirectory ) + 'bin' )
     else
         Result := '';
end;

function GetInterbaseIsInstalled: Boolean;
var
   sGds32dllFileName: String;
begin
     sGds32dllFileName := ZetaWinAPITools.FormatDirectory( ZetaWinAPITools.GetWinSysDir ) + 'gds32.dll';
     Result := ( Copy( ZetaWinAPITools.GetVersionInfoAlmostFull( sGds32dllFileName ), 1, 3 ) <= '5.6' );
end;

function GetFirebirdIsInstalled: Boolean;
begin
     Result := not GetInterbaseIsInstalled;
end;

function GetTransactionServerVersion: String;
begin
     Result := GetVersionInfo( FileSearch( 'mtx.exe', GetWinSysDir ) );
end;

function GetBDEDirectory( var sDirectory: String ): Boolean;
const
     K_BDE_KEY = 'Software\Borland\Database Engine';
     K_BDE_DLL_PATH = 'DLLPATH';
begin
     if GetRegistryStringValue( K_BDE_KEY, K_BDE_DLL_PATH, sDirectory ) then
     begin
          Result := True;
          sDirectory := FormatDirectory( sDirectory );
     end
     else
         Result := False;
end;

function GetBDEVersion: String;
var
   sDirectory: String;
begin
     if GetBDEDirectory( sDirectory ) then
        Result := GetVersionInfo( sDirectory + 'IDAPI32.DLL' )
     else
         Result := K_NO_VERSION_INFO;
end;

function GetODBCAdministrator( var sFileName: String ): Boolean;
begin
     sFileName := 'ODBCad32.exe';
     Result := FindInPaths( sFileName );
end;

function GetODBCVersion: String;
begin
     Result := GetVersionInfo( FileSearch( 'odbc32.dll', GetWinSysDir ) );
end;

function GetADOVersion: String;
begin
     Result := GetVersionInfo( FindDirectory( 'C:\Oledb32.dll' ) );
end;

function GetNumberOfProcessors: Integer;
var
   SysInfo: TSystemInfo;
begin
     Windows.GetSystemInfo( SysInfo );
     Result := SysInfo.dwNumberOfProcessors;
end;

function DriveIsFixed( const sDrive: String ): Boolean;
//var
//   sDrivePath: Array[ 0..3 ] of Char;     // holds the root directory to query //
begin
     //StrPCopy( sDrivePath, sDrive + ':\' );
     case Windows.GetDriveType( PChar(sDrive) ) of
          DRIVE_UNKNOWN: Result := False;
          DRIVE_NO_ROOT_DIR: Result := False;
          DRIVE_REMOVABLE: Result := False;
          DRIVE_FIXED: Result := True;
          DRIVE_REMOTE: Result := False;
          DRIVE_CDROM: Result := False;
          DRIVE_RAMDISK: Result := False;
     else
         Result := False;
     end;
end;

function GetDiskSpace( const iDrive: Byte; var iTotalSpace, iFreeSpaceAvailable: Int64 ): Bool;
var
   RootPath: array[ 0..4 ] of Char;
   RootPtr: PChar;
begin
     RootPtr := nil;
     if ( iDrive > 0 ) then
     begin
          RootPath[ 0 ] := Char( iDrive + K_DISK_A );
          RootPath[ 1 ] := ':';
          RootPath[ 2 ] := '\';
          RootPath[ 3 ] := #0;
          RootPtr := RootPath;
     end;
     Result := Windows.GetDiskFreeSpaceEx( RootPtr, iFreeSpaceAvailable, iTotalSpace, nil );
end;

function GetWindowsVersionName( const iMajor, iMinor: Integer ): String;
begin
     if ( iMajor = 5 ) and ( iMinor =1  ) then
        Result := 'Windows XP'
     else
     if ( iMajor = 5 ) and ( iMinor =2  ) then
        Result := 'Windows Server 2003 / Windows 2003 R2'
     else
     if ( iMajor = 6 ) and ( iMinor =0  ) then
        Result := 'Windows Vista / Windows Server 2008'
     else
     if ( iMajor = 6 ) and ( iMinor =1  ) then
        Result := 'Windows 7 / Windows Server 2008 R2'
     else
     if ( iMajor = 6 ) and ( iMinor =2  ) then
        Result := 'Windows 8'
     else
     case iMajor of
          5: Result := '2000';
          4: Result := 'NT 4.0';
     else
         Result := Format( '%d.%d', [ iMajor, iMinor ] );
     end;
end;

function GetArchitecture( const iArchitecture, iLevel, iRevision: DWORD ): String;
const
     PROCESSOR_ARCHITECTURE_INTEL = 0;
     PROCESSOR_ARCHITECTURE_MIPS = 1;
     PROCESSOR_ARCHITECTURE_ALPHA = 2;
     PROCESSOR_ARCHITECTURE_PPC = 3;
begin
     case iArchitecture of
          PROCESSOR_ARCHITECTURE_INTEL:
          begin
               case iLevel of
                    3: Result := 'Intel 80386';
                    4: Result := 'Intel 80486';
                    5: Result := 'Intel Pentium';
                    6: Result := Format( 'Intel Pentium Pro Rev %d', [iRevision] ) ;
               else
                   Result := Format( 'Intel Level %d' , [ilevel]);
               end;
          end;
          PROCESSOR_ARCHITECTURE_MIPS:
          begin
               case iLevel of
                    0004: Result := 'MIPS R4000';
               else
                   Result := 'MIPS ?????';
               end;
          end;
          PROCESSOR_ARCHITECTURE_ALPHA:
          begin
               case iLevel of
                    21064: Result := 'Alpha 21064';
                    21066: Result := 'Alpha 21066';
                    21164: Result := 'Alpha 21164';
               else
                   Result := 'Alpha ?????';
               end;
          end;
          PROCESSOR_ARCHITECTURE_PPC:
          begin
               case iLevel of
                    1: Result := 'PPC 601';
                    3: Result := 'PPC 603';
                    4: Result := 'PPC 604';
                    6: Result := 'PPC 603+';
                    9: Result := 'PPC 604+';
                    20: Result := 'PPC 620';
               else
                   Result := 'Power PC ?????';
               end;
          end;
     else
         Result := '????';
     end;
end;
//(@am): Agregado para extraer del servidor la informacion utilizada en la valdacion de la licencia
{$IFDEF DEBUGSENTINEL}
procedure WriteSystemShortInfo ( Lineas: TStrings );
begin
    with Lineas do
    begin
        Add('<FingerPrintData>');
        Add('<ServerInfo>');
        Add('<ServerName>'+ GetComputerName +'</ServerName>');
        Add('<LenStr>'+ IntToStr(Length(GetComputerName)) +'</LenStr>');
        Add('</ServerInfo>');
        Add('<OS>');
        Add('<SerialNumber>'+GetOSSerial+'</SerialNumber>');
        Add('<LenStr>'+ IntToStr(Length(GetOSSerial)) +'</LenStr>');
        Add('</OS>');
        Add('<Bios>');
        Add('<SerialNumber>'+GetBiosSerial+'</SerialNumber>');
        Add('<LenStr>'+ IntToStr(Length(GetBiosSerial)) +'</LenStr>');
        Add('</Bios>');
        Add('</FingerPrintData>');
    end;
end;
{$ENDIF}
procedure WriteSystemInfo( Lineas: TStrings ; lEscribirXML : boolean );
var
   SysInfo: TSystemInfo;
   {$ifdef ANTES}
   MemoryInfo: TMemoryStatus;
   {$else}
   MemoryInfo: TMemoryStatusEx;
   {$endif}
   i: Word;
   iSize, iAvailable: Int64;



function BoolToAttBoolean(lValue: boolean): String;
begin
     if ( lValue ) then
        Result := 'true'
     else
         Result := 'false';
end;



begin
     Windows.GetSystemInfo( SysInfo );
     {$ifdef ANTES}
     MemoryInfo.dwLength := SizeOf( TMemoryStatus);
     Windows.GlobalMemoryStatus( MemoryInfo );
     {$else}
     FillChar(MemoryInfo, SizeOf(MemoryInfo), 0);
     MemoryInfo.dwLength := SizeOf(MemoryInfo);
     // check return code for errors
     Win32Check(GlobalMemoryStatusEx(MemoryInfo));
     {$endif}

     with Lineas do
     begin
          BeginUpdate;
          if ( not lEscribirXML ) then
             Clear;
          if ( SysUtils.Win32Platform = VER_PLATFORM_WIN32_NT	) then
          begin
               if ( lEscribirXML ) then
               begin
                   Add( Format( '<Servidor Tipo="%s" VersionOS="%s" Nombre="%s" Usuario="%s" EsAdministrador="%s" >' ,
                        [
                        GetWindowsVersionName( SysUtils.Win32MajorVersion, SysUtils.Win32MinorVersion ),
                         Format( '%d.%d Build %d %s', [ SysUtils.Win32MajorVersion, SysUtils.Win32MinorVersion, SysUtils.Win32BuildNumber, SysUtils.Win32CSDVersion ] ),
                        UpperCase( ZetaWinAPITools.GetComputerName ),
                        GetCurrentUserName,
                        BoolToAttBoolean( UserIsAdministrator )
                        ] ) );


                   with SysInfo do
                   begin
                        Add( Format( '<Procesadores Cantidad="%d" Nombre="%s"/>',[dwNumberOfProcessors, GetArchitecture( wProcessorArchitecture, wProcessorLevel, wProcessorRevision ) ] ) );
                   end;

                   with MemoryInfo do
                   begin
                        Add( '<Memoria ' );
                        Add( Format( 'RecursosLibres="%f" ', [ ( 100 - dwMemoryLoad ) / 1 ] ));
                        Add( Format( 'MemoriaRAM="%f" ', [ BytesAMegas( ullTotalPhys ) ] ) );
                        Add( Format( 'LibreRAM="%f" ', [ BytesAMegas( ullAvailPhys ) ] ) );
                        Add( Format( 'Virtual="%f"', [ BytesAMegas( ullTotalVirtual ) ] ) );
                        Add( Format( 'LibreVirtual="%f"', [ BytesAMegas( ullAvailVirtual ) ] ) );
                        ADd('/>' );
                   end;
                   Add( '<HDD>' ) ;
                   for i := K_DISK_C to K_DISK_Z do
                   begin
                        if DriveIsFixed( Chr( i ) ) then
                        begin
                             GetDiskSpace( i - K_DISK_A, iSize, iAvailable );
                             Add( Format( '<Disco Drive="%s" Capacidad="%f" Libre="%f"  Ocupacion="%f" />', [ Chr( i ), BytesAMegas( iSize ), BytesAMegas( iAvailable ), 100.00-( 100 * iAvailable / iSize ) ] )  );
                        end;
                   end;
                   Add( '</HDD>' );

                   {No solicitado en XML if ( SysUtils.Win32MajorVersion < 5 ) then
                   begin
                        Add( 'Internet Explorer:  ' + GetInternetExplorerVersion );
                        Add( 'Transaction Server: ' + GetTransactionServerVersion );
                   end;}
                   Add( '<ODBCDrivers ' );
                   Add( ' ODBC="' + GetODBCVersion +'"');
                   Add( ' BDE="' + GetBDEVersion +'"' );
                   Add( ' Interbase="' + GetInterbaseVersion +'" ' );
                   Add( '/>' );

                   // Agregar nuevo elemento al XML de 3DT.
                   // FingerPrintData
                   {$IFDEF TRESSCFG}
                   WriteFingerPrintData( Lineas, True);
                   {$ENDIF}

                   Add( '</Servidor>' );
               end
               else
               begin
                   Add( '' );
                   Add( Format( 'Servidor de Windows: %s', [ GetWindowsVersionName( SysUtils.Win32MajorVersion, SysUtils.Win32MinorVersion ) ] ) );
                   Add( '' );
                   Add( Format( 'Nombre:  %s', [ UpperCase( ZetaWinAPITools.GetComputerName ) ] ) );
                   Add( Format( 'Usuario: %s ( %s Es Administrador )', [ GetCurrentUserName,  BoolToSiNo( UserIsAdministrator ) ] ) );
                   Add( '' );
                   Add( Format( 'Versi�n %d.%d Build %d %s', [ SysUtils.Win32MajorVersion, SysUtils.Win32MinorVersion, SysUtils.Win32BuildNumber, SysUtils.Win32CSDVersion ] ) );
                   Add( '' );
                   with SysInfo do
                   begin
                        Add( Format( 'Procesadores:               %d %s', [ dwNumberOfProcessors, GetArchitecture( wProcessorArchitecture, wProcessorLevel, wProcessorRevision ) ] ) );
                        Add( '' );
                   end;
                   with MemoryInfo do
                   begin
                        Add( Format( 'Recursos Libres:            %.0n', [ ( 100 - dwMemoryLoad ) / 1 ] ) + ' %' );

                        {$ifdef ANTES}
                        Add( Format( 'Memoria RAM:                %.2n MegaBytes', [ BytesAMegas( dwTotalPhys ) ] ) );
                        Add( Format( 'Memoria RAM Disponible:     %.2n MegaBytes', [ BytesAMegas( dwAvailPhys ) ] ) );
                        Add( Format( 'Memoria Virtual:            %.2n MegaBytes', [ BytesAMegas( dwTotalVirtual ) ] ) );
                        Add( Format( 'Memoria Virtual Disponible: %.2n MegaBytes', [ BytesAMegas( dwAvailVirtual ) ] ) );
                        {$else}
                        Add( Format( 'Memoria RAM:                %.2n MegaBytes', [ BytesAMegas( ullTotalPhys ) ] ) );
                        Add( Format( 'Memoria RAM Disponible:     %.2n MegaBytes', [ BytesAMegas( ullAvailPhys ) ] ) );
                        Add( Format( 'Memoria Virtual:            %.2n MegaBytes', [ BytesAMegas( ullTotalVirtual ) ] ) );
                        Add( Format( 'Memoria Virtual Disponible: %.2n MegaBytes', [ BytesAMegas( ullAvailVirtual ) ] ) );
                        {$endif}
                   end;
                   Add( '' );
                   if ( SysUtils.Win32MajorVersion < 5 ) then
                   begin
                        Add( 'Internet Explorer:  ' + GetInternetExplorerVersion );
                        Add( 'Transaction Server: ' + GetTransactionServerVersion );
                   end;
                   Add( 'ODBC:               ' + GetODBCVersion );
                   Add( 'BDE:                ' + GetBDEVersion );
                   Add( 'Interbase:          ' + GetInterbaseVersion );
                   Add( '' );
                   Add( 'Disco   Tama�o      Libre   Ocupaci�n' );
                   Add( '----- ---------- ---------- ---------' );
                   {
                   Add( '  A   999,999 xx 999,999 xx   99.9%' );
                   }
                   for i := K_DISK_C to K_DISK_Z do
                   begin
                        if DriveIsFixed( Chr( i ) ) then
                        begin
                             GetDiskSpace( i - K_DISK_A, iSize, iAvailable );
                             Add( Format( '  %s    %s  %s   %3.1n', [ Chr( i ), GetSize( iSize ), GetSize( iAvailable ), 100.00-( 100 * iAvailable / iSize ) ] ) + ' %' );
                        end;
                   end;
               end;
          end
          else
          begin
               if ( lEscribirXML ) then
               begin
                   Add( Format( '<Servidor Tipo="Windows 95/98" VersionOS="%s" Nombre="%s" Usuario="" EsAdministrador="S" >' ,
                        [
                        Format( '%d.%d Build %d %s', [ SysUtils.Win32MajorVersion, SysUtils.Win32MinorVersion, SysUtils.Win32BuildNumber, SysUtils.Win32CSDVersion ] ),
                        UpperCase( ZetaWinAPITools.GetComputerName )
                        ] ) );
                   Add( '<Procesadores Cantidad="0"/>' );
                   Add( '<Memoria ' );
                   Add( 'RecursosLibres="0.00" ' );
                   Add( 'MemoriaRAM="0.00" ');
                   Add( 'LibreRAM="0.00" ');
                   Add( 'Virtual="0.00" ');
                   Add( 'LibreVirtual="0.00" ');
                   Add('/>' );
                   Add( '<HDD/>' ) ;
                   Add( '<ODBCDrivers ' );
                   Add( ' ODBC="" ');
                   Add( ' BDE="" ' );
                   Add( ' Interbase="" ' );
                   Add( '/>' );
                   Add( '</Servidor>' );
               end
               else
               begin
                   Add( '' );
                   Add( 'Servidor de Windows 95/98' );
                   Add( '' );
                   Add( 'Nombre: ' + UpperCase( ZetaWinAPITools.GetComputerName ) );
                   Add( '' );
                   Add( Format( 'Versi�n %d.%d', [ SysUtils.Win32MajorVersion, SysUtils.Win32MinorVersion ] ) );
                   Add( '' );
                   Add( SysUtils.Win32CSDVersion );
               end;
          end;
          EndUpdate;
     end;
end;

{$IFDEF DEBUGSENTINEL}

//(@am): Se agrega para la validacion de licencias
function GetOSSerial:AnsiString;
var
   HWID : THardwareId;
   sResult: AnsiString;
begin
    HWID:=THardwareId.Create(False);
    sResult := VACIO;
    try
        sResult := HWID.GenerateOSSerial;
    finally
        HWID.Free;
    end;
    Result := sResult;
end;

//(@am): Se agrega para la validacion de licencias
function GetBIOSSerial:AnsiString;
var
   HWID : THardwareId;
   sResult: AnsiString;
begin
    HWID:=THardwareId.Create(False);
    sResult := VACIO;
    try
        sResult := HWID.GenerateBIOSSerial;
    finally
        HWID.Free;
    end;
    Result := sResult;
end;
{$ENDIF}
// US #9660.
{$IFDEF TRESSCFG}

procedure InfoALineas (Lineas: TStrings; slTagTemportal: TStringList);
begin
  // Agregar info a Lineas
  Lineas.BeginUpdate;
  Lineas.Add(slTagTemportal.Text);
  Lineas.EndUpdate;
end;

procedure WriteFingerPrintData( Lineas: TStrings ; lEscribirXML : boolean );
var
   HWID : THardwareId;
   sBuffer: String;
   iIndex: Integer;
   slTagTemportal: TStringList;
   slTagErrores: TStringList;

   procedure Motherboard;
   begin
      with slTagTemportal do
      begin
        Add ('<Motherboard>');
        Add ('<SerialNumber>' + HWID.GetPropiedad( Ord ( Mb_SerialNumber ) ) );
        Add ('</SerialNumber>');
        Add ('<Manufacturer>' + HWID.GetPropiedad( Ord ( Mb_Manufacturer ) ) );
        Add ('</Manufacturer>');
        Add ('<Product>' + HWID.GetPropiedad( Ord ( Mb_Product ) ) );
        Add ('</Product>');
        Add ('</Motherboard>');

        // Agregar info a Lineas
        {Lineas.BeginUpdate;
        Lineas.Add(slTagTemportal.Text);
        Lineas.EndUpdate;}
        InfoALineas (Lineas, slTagTemportal);
      end;
   end;

   procedure OS;
   var  sMachineGUID: String;
   begin
      try
          sMachineGUID := HWID.MachineGUID;
      except
        on Error: Exception do
        begin
          sMachineGUID := vacio;
          slTagErrores.BeginUpdate;
          slTagErrores.Add(Format('<Error Descripcion = ''%s'' Detalle = ''%s'' />', ['OS error al obtener MachineGUID', Error.Message]));
          slTagErrores.EndUpdate;
        end;
      end;
      {***(@am): Se agrega el tag de inicio  y fin en la misma linea, pues a la informacion dentro del tag se le estaba agregando
                 el caracter CR_LF al final. Lo cual repercutia en al validacion de lincencias sentinel. Se podia notar al dar F7
                 a slTagTemporal.Text***}
      with slTagTemportal do
      begin
        Add ('<OS>');
        Add ('<BuildNumber>'   + HWID.GetPropiedad( Ord ( Os_BuildNumber ) )  + '</BuildNumber>');
        Add ('<BuildType>'     + HWID.GetPropiedad( Ord ( Os_BuildType ) )    + '</BuildType>');
        Add ('<Manufacturer>'  + HWID.GetPropiedad( Ord ( Os_Manufacturer ) ) + '</Manufacturer>');
        Add ('<Name>'          + HWID.GetPropiedad( Ord ( Os_Name ) )         + '</Name>');
        Add ('<SerialNumber>'  + HWID.GetPropiedad( Ord ( Os_SerialNumber ) ) + '</SerialNumber>');
        Add ('<Version>'       + HWID.GetPropiedad( Ord ( Os_Version ) )      + '</Version>');
        {Add ('<WindowsId>'); // ---> ??
        Add ('</WindowsId>');}
        Add ('<MachineGuid>'   + sMachineGUID                                 + '</MachineGuid>');
        Add ('</OS>');
        // Agregar info a Lineas
        InfoALineas (Lineas, slTagTemportal);
      end;
   end;

{***(@am): Se agrega el tag de inicio  y fin en la misma linea, pues a la informacion dentro del tag se le estaba agregando
           el caracter CR_LF al final. Lo cual repercutia en al validacion de lincencias sentinel. Se podia notar al dar F7
           a slTagTemporal.Text***}
   procedure Bios;
   begin
      with slTagTemportal do
      begin
        Add ('<Bios>');
        Add ('<BIOSVersion>'  + HWID.GetPropiedad( Ord ( Bs_BIOSVersion ) )  + '</BIOSVersion>');
        Add ('<BuildNumber>'  + HWID.GetPropiedad( Ord ( Bs_BuildNumber ) )  + '</BuildNumber>');
        Add ('<Description>'  + HWID.GetPropiedad( Ord ( Bs_Description ) )  + '</Description>');
        Add ('<Manufacturer>' + HWID.GetPropiedad( Ord ( Bs_Manufacturer ) ) + '</Manufacturer>');
        Add ('<Name>'         + HWID.GetPropiedad( Ord ( Bs_Name ) )         + '</Name>');
        Add ('<SerialNumber>' + HWID.GetPropiedad( Ord ( Bs_SerialNumber ) ) + '</SerialNumber>');
        Add ('<Version>'      + HWID.GetPropiedad( Ord ( Bs_Version ) )      + '</Version>');
        Add ('</Bios>');

        // Agregar info a Lineas
        InfoALineas (Lineas, slTagTemportal);
      end;
   end;

   procedure Processor;
   begin
      with slTagTemportal do
      begin
        Add ('<Processor>');
        Add ('<Description>' + HWID.GetPropiedad( Ord ( Pr_Description ) ) );
        Add ('</Description>');
        Add ('<Manufacturer>' + HWID.GetPropiedad( Ord ( Pr_Manufacturer ) ) );
        Add ('</Manufacturer>');
        Add ('<Name>' + HWID.GetPropiedad( Ord ( Pr_Name ) ) );
        Add ('</Name>');
        Add ('<ProcessorId>' + HWID.GetPropiedad( Ord ( Pr_ProcessorId ) ) );
        Add ('</ProcessorId>');
        Add ('<UniqueId>' + HWID.GetPropiedad( Ord ( Pr_UniqueId ) ) );
        Add ('</UniqueId>');
        Add ('</Processor>');

        // Agregar info a Lineas
        InfoALineas (Lineas, slTagTemportal);
      end;
   end;

   procedure VideoController;
   begin
      with slTagTemportal do
      begin
        Add ('<VideoController>');
        Add ('<DriverVersion>' + HWID.GetPropiedad( Ord ( Vc_DriverVersion ) ) );
        Add ('</DriverVersion>');
        Add ('<Name>' + HWID.GetPropiedad( Ord ( Vc_Name ) ) );
        Add ('</Name>');
        Add ('</VideoController>');

        // Agregar info a Lineas
        InfoALineas (Lineas, slTagTemportal);
      end;
   end;

   function ObtenerHostName: String;
   begin
        iIndex := 0;
        Result := VACIO;
        while iIndex < (HWID.TempInfoList.Count-1 ) do
        begin
             if HWID.GetPropiedad( iIndex ) <> VACIO then
             begin
                  Result := UpperCase( HWID.GetPropiedad( iIndex ) );
                  break;
             end;
             Inc( iIndex );
        end;
        if StrVacio( Result ) then
           Result := UpperCase( ZetaWinAPITools.GetComputerName );
   end;

   procedure Network;
   begin
      with slTagTemportal do
      begin
        Add ('<HostName>' + ObtenerHostName );
        Add ('</HostName>');
        // Add ('<MacAddress>' + HWID.HardwareMACAD);
        Add ('<MacAddress>' + HWID.HardwareMACAD );
        Add ('</MacAddress>');
      end;
   end;

   procedure ListaDispositivos;
   begin
      with slTagTemportal do
      begin
            // Recorrer lista de dispositivos que han quedado como lista
            // en slPropiedades.
            Add ('<Devices>');
            iIndex := 0;
            while iIndex < (HWID.TempInfoList.Count-1) do
            begin
              Add (Format ('<Device DeviceID = "%s" Name = "%s" />', [ HWID.GetPropiedad( iIndex ), HWID.GetPropiedad( iIndex + 1 ) ] ) );
              iIndex := iIndex + 2;
            end;
            Add ('</Devices>');

      end;
   end;

begin
   try
       //slPropiedades := TStringList.Create;
       slTagTemportal := TStringList.Create;
       slTagErrores := TStringList.Create;

       Lineas.BeginUpdate;
       Lineas.Add ('<FingerPrintData>');
       Lineas.EndUpdate;

       HWID:=THardwareId.Create(False);
       try
           with slTagTemportal do
           begin
                BeginUpdate;
                try
                  if ( not lEscribirXML ) then
                     Clear;

                  // MotherBoard Info.
                  HWID.GenerateMotherBoardInfo (slTagErrores);
                  if HWID.HayPropiedades then
                  begin
                       // Motherboard
                       Motherboard;
                       EndUpdate;
                  end;
                except
                  on Error: Exception do
                  begin
                      Motherboard;
                      EndUpdate;
                      slTagErrores.BeginUpdate;
                      slTagErrores.Add(Format('<Error Descripcion = ''%s'' Detalle = ''%s'' />', ['Motherboard error', Error.Message]));
                      slTagErrores.EndUpdate;
                  end;
                end;
           end;

           with slTagTemportal do
           begin
                BeginUpdate;
                slTagTemportal.Clear;
                try
                  // OS Info.
                  HWID.GenerateOSInfo (slTagErrores);
                  if HWID.HayPropiedades then
                  begin
                       // OS
                       OS;
                       EndUpdate;
                  end;
                except
                  on Error: Exception do
                  begin
                      OS;
                      EndUpdate;
                      slTagErrores.BeginUpdate;
                      slTagErrores.Add(Format('<Error Descripcion = ''%s'' Detalle = ''%s'' />', ['OS error', Error.Message]));
                      slTagErrores.EndUpdate;
                  end;
                end;
           end;

           with slTagTemportal do
           begin
                BeginUpdate;
                slTagTemportal.Clear;
                try
                  // Bios Info.
                  HWID.GenerateBIOSInfo (slTagErrores);
                  if HWID.HayPropiedades then
                  begin
                       Bios;
                       EndUpdate;
                  end;
                except
                  on Error: Exception do
                  begin
                      Bios;
                      EndUpdate;
                      slTagErrores.BeginUpdate;
                      slTagErrores.Add(Format('<Error Descripcion = ''%s'' Detalle = ''%s'' />', ['Bios error', Error.Message]));
                      slTagErrores.EndUpdate;
                  end;
                end;
           end;

           with slTagTemportal do
           begin
                BeginUpdate;
                slTagTemportal.Clear;
                try

                  // Processor Info.
                  HWID.GenerateProcessorInfo (slTagErrores);
                  if HWID.HayPropiedades then
                  begin
                       Processor;
                       EndUpdate;
                  end;
                except
                  on Error: Exception do
                  begin
                      Processor;
                      EndUpdate;
                      slTagErrores.BeginUpdate;
                      slTagErrores.Add(Format('<Error Descripcion = ''%s'' Detalle = ''%s'' />', ['Processor error', Error.Message]));
                      slTagErrores.EndUpdate;
                  end;
                end;
           end;

           with slTagTemportal do
           begin
                BeginUpdate;
                slTagTemportal.Clear;
                try

                  // Video Controller Info.
                  HWID.GenerateVideoControllerInfo (slTagErrores);
                  if HWID.HayPropiedades then
                  begin
                       VideoController;
                       EndUpdate;
                  end;
                except
                  on Error: Exception do
                  begin
                      VideoController;
                      EndUpdate;
                      slTagErrores.BeginUpdate;
                      slTagErrores.Add(Format('<Error Descripcion = ''%s'' Detalle = ''%s'' />', ['VideoController error', Error.Message]));
                      slTagErrores.EndUpdate;
                  end;
                end;
           end;

           with slTagTemportal do
           begin
                BeginUpdate;
                slTagTemportal.Clear;
                try
                  // Network Info.
                  HWID.GenerateNetworkInfo (slTagErrores);
                  if HWID.HayPropiedades then
                  begin
                       // Network
                       Network;
                       EndUpdate;
                  end;
                except
                  on Error: Exception do
                  begin
                      Network;
                      EndUpdate;
                      slTagErrores.BeginUpdate;
                      slTagErrores.Add(Format('<Error Descripcion = ''%s'' Detalle = ''%s'' />', ['Network error', Error.Message]));
                      slTagErrores.EndUpdate;
                  end;
                end;
           end;

           with slTagTemportal do
           begin
                BeginUpdate;
                try
                    // Generar lista de dispositivos
                    HWID.GenerarListaDispositivos(slTagErrores);
                    if HWID.HayPropiedades then
                      ListaDispositivos;
                    EndUpdate;
                except
                  on Error: Exception do
                  begin
                      // ListaDispositivos;
                      Add ('<Devices>');
                      Add ('</Devices>');
                      EndUpdate;
                      slTagErrores.BeginUpdate;
                      slTagErrores.Add(Format('<Error Descripcion = ''%s'' Detalle = ''%s'' />', ['Network - Lista de dispositivos error �� �', Error.Message]));
                      slTagErrores.EndUpdate;
                  end;
                end;

                // Agregar info a Lineas
                // Network info y lista de dispositovos.
                Lineas.BeginUpdate;
                Lineas.Add('<Network>');
                InfoALineas (Lineas, slTagTemportal);
                Lineas.Add('</Network>');
                Lineas.EndUpdate;
           end;

           Lineas.BeginUpdate;

           Lineas.Add('<Errores>');
           Lineas.Add(slTagErrores.Text);
           Lineas.Add('</Errores>');

           Lineas.Add ('</FingerPrintData>');
           Lineas.EndUpdate;

           // Quitar acentos
           Lineas.DelimitedText := QuitaAcentos(Lineas.DelimitedText);

        finally
         HWID.Free;
        end;
   except
   end;
end;
{$ENDIF}

function GetLongPathName( const sValue: String ): String;
const
     K_SEPARATOR = '\';
var
   lFound: Boolean;
   FindData: TSearchRec;
   sDrive, sShortFolder, sShortPath, sDirectory, sFileName: String;

function StrToken( var sValue: String ): String;
var
   i: Word;
begin
     i := Pos( K_SEPARATOR, sValue );
     if ( i = 0 ) then
     begin
          Result := sValue;
          sValue := '';
     end
     else
     begin
          Result := System.Copy( sValue, 1, ( i - 1 ) );
          System.Delete( sValue, 1, i );
     end;
end;

begin
     if ( sValue = '' ) then
        Result := ''
     else
     begin
          sFileName := ExtractFileName( sValue );
          if ( sFileName = '' ) then
             sDirectory := FormatDirectory( sValue )
          else
              sDirectory := ExtractFilePath( FormatDirectory( sValue ) );
          if ( Pos( '~', sDirectory ) = 0 ) or ( sDirectory <> ExtractShortPathName( sDirectory ) ) then
             Result := sDirectory
          else
          begin
               sDrive := StrToken( sDirectory );
               sShortPath := sDrive;
               Result := sDrive;
               while ( sDirectory <> '' ) do
               begin
                    sShortFolder := StrToken( sDirectory );
                    sShortPath := sShortPath + K_SEPARATOR + sShortFolder;
                    lFound := False;
                    if ( FindFirst( Result + K_SEPARATOR + '*.*', faDirectory, FindData ) = 0 ) then
                    begin
                         repeat
                               with FindData do
                               begin
                                    if ( Attr = faDirectory	) then
                                    begin
                                         if AnsiSameText( sShortPath, ExtractShortPathName( Result + K_SEPARATOR + Name ) ) then
                                         begin
                                              lFound := True;
                                              sShortFolder := Name;
                                         end;
                                    end;
                               end;
                         until lFound or ( FindNext( FindData ) <> 0 );
                         FindClose( FindData );
                    end;
                    if lFound then
                       Result := Result + K_SEPARATOR + sShortFolder;
               end;
               Result := FormatDirectory( Result ) + sFileName;
          end;
     end;
end;

{ *********** Process Management ************ }

function GetErrorMessage: String;
var
   iError: DWORD;
begin
     iError := Windows.GetLastError;
     Result := Format( 'Error %d = %s', [ iError, SysUtils.SysErrorMessage( iError ) ] );
end;

function GetProcessName( const iProcessID: DWORD ): String;
type
    aModuleList = array[ 0..127 ] of DWord;
var
   Handle: THandle;
   iNeeded: DWord;
   aModules: aModuleList;
   pName: array[ 0..MAX_PATH ] of Char;
begin
     Handle := Windows.OpenProcess( PROCESS_QUERY_INFORMATION or PROCESS_VM_READ, False, iProcessID );
     if ( Handle > 0 ) then
     begin
          try
             if PSAPI.EnumProcessModules( Handle, @aModules, SizeOf( aModules ), iNeeded ) then
             begin
                  {
                  if ( PSAPI.GetModuleFileNameEx( Handle, aModules[ 0 ], pName, SizeOf( pName ) ) > 0 ) then
                  }
                  if ( PSAPI.GetModuleBaseName( Handle, aModules[ 0 ], pName, SizeOf( pName ) ) > 0 ) then
                     Result := pName
                  else
                      Result := GetErrorMessage;
             end
             else
                 Result := Format( 'Unknown ID: %s', [ GetErrorMessage ] );
          finally
                 Windows.CloseHandle( Handle );
          end;
     end
     else
         Result := Format( 'Unknown: %s', [ GetErrorMessage ] );
end;

procedure ProcessWalkNT( Lista: TStrings );
type
    aProcessList = array[ 0..1023 ] of DWord;
var
   aProcesos: aProcessList;
   i, iNeeded, iTop, iProceso: DWord;
begin
     with Lista do
     begin
          BeginUpdate;
          try
             Clear;
             if PSAPI.EnumProcesses( @aProcesos, SizeOf( aProcesos ), iNeeded ) then
             begin
                  iTop := Trunc( iNeeded / SizeOf( iNeeded ) ) - 1;
                  for i := 0 to iTop do
                  begin
                       iProceso := aProcesos[ i ];
                       if ( iProceso > 0 ) then
                       begin
                            {
                            Add( Format( '%d = %s', [ iProceso, SysUtils.ExtractFileName( GetProcessName( iProceso ) ) ] ) );
                            }
                            Add( Format( '%d = %s', [ iProceso, GetProcessName( iProceso ) ] ) );
                       end;
                  end;
             end
             else
                 raise Exception.Create( Format( 'Unable To Ennumerate Processes ( %s )', [ GetErrorMessage  ] ) );
          finally
                 EndUpdate;
          end;
     end;
end;

procedure ProcessWalkNotNT( Lista: TStrings );
var
   Handle: THandle;
   Entry: TProcessEntry32;
begin
     with Lista do
     begin
          BeginUpdate;
          try
             Clear;
             Handle := TLHelp32.CreateToolHelp32Snapshot( TH32CS_SNAPPROCESS, 0 );
             if ( Handle > 0 ) then
             begin
                  try
                     Entry.dwSize := SizeOf( Entry );
                     if TLHelp32.Process32First( Handle, Entry ) then
                     begin
                          repeat
                                with Entry do
                                begin
                                     Add( Format( '%d = %s', [ th32ProcessID, szExeFile ] ) );
                                     {
                                     Add( Format( '%d = %s', [ th32ProcessID, GetProcessName( th32ProcessID ) ] ) );
                                     }
                                end;
                          until not TLHelp32.Process32Next( Handle, Entry );
                     end
                     else
                         raise Exception.Create( Format( 'Unable To Find First Process ( %s )', [ GetErrorMessage  ] ) );
                  finally
                         if not Windows.CloseHandle( Handle ) then
                            raise Exception.Create( Format( 'Unable To Destroy Handle ( %s )', [ GetErrorMessage  ] ) );
                  end;
             end
             else
                 raise Exception.Create( Format( 'Unable To Create SnapShot ( %s )', [ GetErrorMessage  ] ) );
          finally
                 EndUpdate;
          end;
     end;
end;

function ProcessIDNT( sProcess: String ): Integer;
type
    aProcessList = array[ 0..1023 ] of DWord;
var
   aProcesos: aProcessList;
   i, iNeeded, iTop, iProceso: DWord;
begin
     Result := -1;
     sProcess := AnsiUpperCase( sProcess );
     if PSAPI.EnumProcesses( @aProcesos, SizeOf( aProcesos ), iNeeded ) then
     begin
          iTop := Trunc( iNeeded / SizeOf( iNeeded ) ) - 1;
          i := 0;
          while ( Result < 0 ) and ( i <= iTop ) do
          begin
               iProceso := aProcesos[ i ];
               if ( iProceso > 0 ) and ( AnsiUpperCase( GetProcessName( iProceso ) ) = sProcess ) then
                  Result := iProceso;
          end;
     end;
end;

function ProcessIDNotNT( sProcess: String ): Integer;
var
   Handle: THandle;
   Entry: TProcessEntry32;
begin
     Result := -1;
     sProcess := AnsiUpperCase( sProcess );
     Handle := TLHelp32.CreateToolHelp32Snapshot( TH32CS_SNAPPROCESS, 0 );
     if ( Handle > 0 ) then
     begin
          try
             Entry.dwSize := SizeOf( Entry );
             if TLHelp32.Process32First( Handle, Entry ) then
             begin
                  repeat
                        if ( Pos( AnsiUpperCase( Entry.szExeFile ), sProcess ) > 0 ) then
                           Result := Entry.th32ProcessID;
                  until not TLHelp32.Process32Next( Handle, Entry ) or ( Result > 0 );
             end;
          finally
                 Windows.CloseHandle( Handle );
          end;
     end;
end;

function ProcessIsRunningNT( sProcess: String ): Boolean;
type
    aProcessList = array[ 0..1023 ] of DWord;
var
   aProcesos: aProcessList;
   i, iNeeded, iTop, iProceso, iCtr: DWord;
begin
     sProcess := AnsiUpperCase( sProcess );
     if PSAPI.EnumProcesses( @aProcesos, SizeOf( aProcesos ), iNeeded ) then
     begin
          iTop := Trunc( iNeeded / SizeOf( iNeeded ) ) - 1;
          i := 0;
          iCtr := 0;
          while ( i <= iTop ) do
          begin
               iProceso := aProcesos[ i ];
               if ( iProceso > 0 ) then
               begin
                    if ( AnsiUpperCase( GetProcessName( iProceso ) ) = sProcess ) then
                       Inc( iCtr );
               end;
               Inc( i );
          end;
          Result := ( iCtr > 1 );
     end
     else
         Result := False;
end;

function ProcessIsRunningNotNT( sProcess: String ): Boolean;
var
   Handle: THandle;
   Entry: TProcessEntry32;
   iCtr: Integer;
begin
     Result := False;
     sProcess := AnsiUpperCase( sProcess );
     Handle := TLHelp32.CreateToolHelp32Snapshot( TH32CS_SNAPPROCESS, 0 );
     if ( Handle > 0 ) then
     begin
          try
             iCtr := 0;
             Entry.dwSize := SizeOf( Entry );
             if TLHelp32.Process32First( Handle, Entry ) then
             begin
                  repeat
                        if ( Pos( AnsiUpperCase( Entry.szExeFile ), sProcess ) > 0 ) then
                           Inc( iCtr );
                  until not TLHelp32.Process32Next( Handle, Entry );
             end;
             Result := ( iCtr > 1 );
          finally
                 Windows.CloseHandle( Handle );
          end;
     end;
end;

function ProcessID( const sProcess: String ): Integer;
begin
     case SysUtils.Win32Platform of
          VER_PLATFORM_WIN32_NT:
          begin
               if ( SysUtils.Win32MajorVersion > 4 ) then
                  Result := ProcessIDNotNT( sProcess )
               else
                   Result := ProcessIDNT( sProcess );
          end;
          VER_PLATFORM_WIN32_WINDOWS: Result := ProcessIDNotNT( sProcess );
          VER_PLATFORM_WIN32s: Result := ProcessIDNotNT( sProcess );
     else
         Result := -1;
     end;
end;

function ProcessIsRunning( const sProcess: String ): Boolean;
begin
     case SysUtils.Win32Platform of
          VER_PLATFORM_WIN32_NT:
          begin
               if ( SysUtils.Win32MajorVersion > 4 ) then
                  Result := ProcessIsRunningNotNT( sProcess )
               else
                   Result := ProcessIsRunningNT( sProcess );
          end;
          VER_PLATFORM_WIN32_WINDOWS: Result := ProcessIsRunningNotNT( sProcess );
          VER_PLATFORM_WIN32s: Result := ProcessIsRunningNotNT( sProcess );
     else
         Result := False;
     end;
end;

procedure ProcessWalk( Lista: TStrings );
begin
     case SysUtils.Win32Platform of
          VER_PLATFORM_WIN32_NT:
          begin
               if ( SysUtils.Win32MajorVersion > 4 ) then
                  ProcessWalkNotNT( Lista )
               else
                   ProcessWalkNT( Lista );
          end;
          VER_PLATFORM_WIN32_WINDOWS: ProcessWalkNotNT( Lista );
          VER_PLATFORM_WIN32s: ProcessWalkNotNT( Lista );
     end;
end;

    function GetGMTDateTime: TDateTime;
    var
       lpTimeZoneInformation: TTimeZoneInformation;

    function GetOffset( const iBias: Integer ): TDateTime;
    begin
         Result := EncodeTime( Abs( iBias ) div 60, Abs( iBias ) mod 60, 0, 0 );
         if ( iBias < 0 ) then
            Result := Result * -1;
    end;

begin
     case Windows.GetTimeZoneInformation( lpTimeZoneInformation ) of
          TIME_ZONE_ID_UNKNOWN:  { The operating system cannot determine the current time zone. This is usually because a previous call to the SetTimeZoneInformation function supplied only the bias (and no transition dates). }
          begin
               Result := Now;
          end;
          TIME_ZONE_ID_STANDARD: { The operating system is operating in the range covered by the StandardDate member of the structure pointed to by the lpTimeZoneInformation parameter }
          begin
               Result := Now + GetOffset( lpTimeZoneInformation.Bias );
          end;
          TIME_ZONE_ID_DAYLIGHT:
          begin
               Result := Now + GetOffset( lpTimeZoneInformation.Bias + lpTimeZoneInformation.DaylightBias );
          end;
     else
         Result := 0;
     end;
end;

{***(@am): Se utilizan las funciones proporcionadas por DELPHI XE5 para obtener el path de MyDocuments***}
function GetMyDocumentsDir: string;
{$IFNDEF TRESS_DELPHIXE5_UP}
var
   MyDocuments: PItemIDList;
   sPath:pchar;
{$ENDIF}
begin
{$IFDEF TRESS_DELPHIXE5_UP}
      Result:= System.IOUtils.TPath.GetDocumentsPath;
{$ELSE}
     sPath := StrAlloc( MAX_PATH );
     try
        SHGetSpecialFolderLocation( 0, CSIDL_PERSONAL, MyDocuments );
        if SHGetPathFromIDList( MyDocuments, sPath ) then
           Result := sPath
        else
            Result := VACIO;
     finally
            StrDispose( sPath );
     end;
{$ENDIF}
end;

//-------------------------------------
// Get a list of services
//
// return TRUE if successful
//
// sMachine:
//   machine name, ie: \\SERVER
//   empty = local machine
//
// dwServiceType
//   SERVICE_WIN32,
//   SERVICE_DRIVER or
//   SERVICE_TYPE_ALL
//
// dwServiceState
//   SERVICE_ACTIVE,
//   SERVICE_INACTIVE or
//   SERVICE_STATE_ALL
//
// slServicesList
//   TStrings variable to storage
//

function ServiceGetListCorreo(sMachine: string; dwServiceType, dwServiceState: DWord; cds: TClientDataSet) : boolean;
const
  cnMaxServices = 4096;
type
  TSvcA = array[0..cnMaxServices]
    of TEnumServiceStatus;
  PSvcA = ^TSvcA;
var
  ServicioTress :string;
  j: integer;
  schm: SC_Handle;
  nBytesNeeded,
  nServices,
  nResumeHandle: DWord;
  ssa: PSvcA;
  estatus : string;
  sEstadoServicio : string;
begin
  Result := false;
  sEstadoServicio := VACIO;
  schm := OpenSCManager(
    PChar(sMachine),
    nil,
    SC_MANAGER_ALL_ACCESS);
  if (schm > 0) then
  begin
    nResumeHandle := 0;
    New(ssa);
    EnumServicesStatus(
      schm,
      dwServiceType,
      dwServiceState,
      ssa^[0],
      SizeOf(ssa^),
      nBytesNeeded,
      nServices,
      nResumeHandle);
    for j := 0 to nServices - 1 do
    begin
        ServicioTress := (StrPas(ssa^[j].lpServiceName));
        if (StrPas(ssa^[j].lpServiceName) = 'TressCorreosServicio')
        then
        begin
            cds.Insert;
            cds.FieldByName('colServicios').AsString := StrPas(ssa^[j].lpDisplayName);
            cds.FieldByName('colServiceName').AsString := StrPas(ssa^[j].lpServiceName);
            cds.FieldByName('colServiceRealName').AsString := StrPas(ssa^[j].lpServiceName);
            if (StrPas(ssa^[j].lpServiceName) = 'TressCorreosServicio')  then
            begin
                 cds.FieldByName('colServiceType').AsString := 'TressCorreos'
            end;
            case ssa^[j].ServiceStatus.dwCurrentState of
            SERVICE_STOPPED:          estatus := 'Apagado';
            SERVICE_START_PENDING:
            begin
                estatus := 'Prendido';
                Result := true;
            end;
            SERVICE_STOP_PENDING:     estatus := 'Apagado';
            SERVICE_RUNNING:
            begin
                estatus := 'Prendido';
                Result := true;
            end;
            SERVICE_CONTINUE_PENDING: estatus := 'Contin�e';
            SERVICE_PAUSE_PENDING:    estatus := 'Pausa';
            SERVICE_PAUSED:           estatus := 'Pausa';
            else
            raise Exception.Create('Estatus del Servicio no identificado');
        end;
        cds.FieldByName('colStatus').AsString := estatus;
        cds.Post;
    end;
    end;

    Dispose(ssa);
    CloseServiceHandle(schm);
  end;
end;

{$ifdef TRESSCFG}
function ServiceGetList(sMachine: string; dwServiceType, dwServiceState: DWord; cds: TClientDataSet) : boolean;
const
  //assume that the total number of services is less than 4096. increase if necessary
  cnMaxServices = 4096;
  Starter_CafeteraServicio = 'CafeteraServicio_';
type
  TSvcA = array[0..cnMaxServices]
    of TEnumServiceStatus;
  PSvcA = ^TSvcA;
var
  ServicioTress :string;
  // temp. use
  j: integer;
  // service control manager handle
  schm: SC_Handle;
  // bytes needed for the
  // next buffer, if any
  nBytesNeeded,
  // number of services
  nServices,
  // pointer to the next unread service entry
  nResumeHandle: DWord;
  // service status array
  ssa: PSvcA;
  estatus : string;
begin
  Result := false;
  // connect to the service control manager
  schm := OpenSCManager(
    PChar(sMachine),
    nil,
    SC_MANAGER_ALL_ACCESS);
  // if successful...
  if (schm > 0) then
  begin
    nResumeHandle := 0;
    New(ssa);
    EnumServicesStatus(
      schm,
      dwServiceType,
      dwServiceState,
      ssa^[0],
      SizeOf(ssa^),
      nBytesNeeded,
      nServices,
      nResumeHandle);
    // assume that our initial array was large enough to hold all entries. add code to enumerate if necessary.
    for j := 0 to nServices - 1 do
    begin
        ServicioTress := (StrPas(ssa^[j].lpServiceName));
        if (StrPas(ssa^[j].lpServiceName) = 'CafeteraServicio')or
        ServicioTress.StartsWith(Starter_CafeteraServicio)or
        (StrPas(ssa^[j].lpServiceName) = 'WorkFlowEMailMgr')or
        (StrPas(ssa^[j].lpServiceName) = 'WorkFlowManager')or
        (StrPas(ssa^[j].lpServiceName) = 'Sentinel3Service') or
        (StrPas(ssa^[j].lpServiceName) = 'TressCorreosServicio') or
        (StrPas(ssa^[j].lpServiceName) = 'TressReportesServicio')
        then
        begin
            cds.Insert;
            cds.FieldByName('colServicios').AsString := StrPas(ssa^[j].lpDisplayName);
            cds.FieldByName('colServiceName').AsString := StrPas(ssa^[j].lpServiceName);
            cds.FieldByName('colServiceRealName').AsString := StrPas(ssa^[j].lpServiceName);
            if (StrPas(ssa^[j].lpServiceName) = 'CafeteraServicio') or (ServicioTress.StartsWith(Starter_CafeteraServicio))  then
               cds.FieldByName('colServiceType').AsString := 'Cafeteria'
            else if (StrPas(ssa^[j].lpServiceName) = 'TressCorreosServicio')  then
               cds.FieldByName('colServiceType').AsString := 'TressCorreos'
            else if (StrPas(ssa^[j].lpServiceName) = 'TressReportesServicio')  then
               cds.FieldByName('colServiceType').AsString := 'TressReportes'
            else
               cds.FieldByName('colServiceType').AsString := 'Otro';
            case ssa^[j].ServiceStatus.dwCurrentState of
            SERVICE_STOPPED:          estatus := 'Apagado';
            SERVICE_START_PENDING:    estatus := 'Prendido';
            SERVICE_STOP_PENDING:     estatus := 'Apagado';
            SERVICE_RUNNING:          estatus := 'Prendido';
            SERVICE_CONTINUE_PENDING: estatus := 'Contin�e';
            SERVICE_PAUSE_PENDING:    estatus := 'Pausa';
            SERVICE_PAUSED:           estatus := 'Pausa';
            else                      raise Exception.Create('Estatus del Servicio no identificado');
        end;
        cds.FieldByName('colStatus').AsString := estatus;
        cds.Post;
    end;


    end;
    Result := true;
    Dispose(ssa);
    // close service control manager handle
    CloseServiceHandle(schm);
  end;
end;
{$endif}

{$ifdef TRESSCFG}
function GetLogicalDrives: TStringDynArray;
begin
    Result := System.IOUtils.TDirectory.GetLogicalDrives;
end;
{$endif}

function GetDiskSize(iDrive : Integer): Currency;
begin
  // Despliega el tama�o del disco duro de la m�quina.
    Result :=  ByteToGigaByte(DiskSize(iDrive));
end;

function GetDiskFree(iDrive : Integer): Currency;
begin
    // Despliega el espacio libre del disco duro de la m�quina.
    Result := ByteToGigaByte(DiskFree(iDrive));
end;

{* Busca la posicion de SubStr en S de derecha a izquierda
   @autor Ricardo Carrillo Morales 01/Mar/2014
   @param PathName Carpeta d�nde buscar
   @param FileMask Nombre de archivo a buscar
   @param Result Objeto StringList d�nde regresar los archivos encontrados
   @param Recursive Especifica si se busca en subcarpetas
   @param ClearResult (Interno)
}
procedure SearchFile(const PathName, FileMask: string; Result: TStringList; const Recursive: Boolean = False);

  procedure RecursiveSearch(const PathName, FileMask: string; Result: TStringList; const Recursive: Boolean = False);
  var
    Rec : TSearchRec;
    Path: string;
  begin
    Path := IncludeTrailingPathDelimiter(PathName);
    if FindFirst(Path + FileMask, faAnyFile - faDirectory, Rec) = 0 then
      try
        repeat
          Result.Add(Path + Rec.Name);
        until FindNext(Rec) <> 0;
      finally
        FindClose(Rec);
      end;

    if not Recursive then begin
      Exit;
    end;

    if FindFirst(Path + '*.*', faDirectory, Rec) = 0 then
      try
        repeat
          if ((Rec.Attr and faDirectory) <> 0) and (Rec.Name <> '.') and (Rec.Name <> '..') then
            RecursiveSearch(Path + Rec.Name, FileMask, Result, Recursive);
        until FindNext(Rec) <> 0;
      finally
        FindClose(Rec);
      end;
  end;

begin
  Result.Clear;
  RecursiveSearch(PathName, FileMask, Result, Recursive);
end;

function AddService( const ServiceName: String; const DisplayName: String; const ExecutablePath: String; const sMachineName: string = ''; const sUser : string = ''; const sPwd: string = '' ): Boolean;
var
   hSCMgr, Service: SC_HANDLE;
begin
    Result := False;
    try
    begin
         hSCMgr := OpenSCManager(PChar(sMachineName), nil, SC_MANAGER_CREATE_SERVICE);
         if hSCMgr > 0 then
         begin
              if StrVacio(sUser) then
                 Service :=  WinSVC.CreateService(hSCMgr,
                         PChar(ServiceName),
                         PChar(DisplayName),
                         STANDARD_RIGHTS_REQUIRED, SERVICE_WIN32_OWN_PROCESS, SERVICE_AUTO_START, SERVICE_ERROR_NORMAL,
                         PChar(ExecutablePath), nil, nil, nil, nil, nil)
              else
                  Service :=  WinSVC.CreateService(hSCMgr,
                          PChar(ServiceName),
                          PChar(DisplayName),
                          STANDARD_RIGHTS_REQUIRED, SERVICE_WIN32_OWN_PROCESS, SERVICE_AUTO_START, SERVICE_ERROR_NORMAL,
                          PChar(ExecutablePath), nil, nil, nil, PChar(sUser), PChar(sPwd));

              Result := Service > 0;
              if Result then
                 ZetaWinAPITools.InitService(ServiceName);

              CloseServiceHandle (Service);
        end
    end;
    except
          on Error: Exception do
          begin
               Application.HandleException( Error );
          end;
     end;
end;

end.
