unit FBuscaServidor_DevEx;

interface

{$ifndef DOS_CAPAS}
{$define HTTP_CONNECTION}
{$endif}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Buttons, StdCtrls, ExtCtrls,
     {$ifdef HTTP_CONNECTION}
     ZetaRegistryCliente,
     {$endif}
     ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, dxSkinsDefaultPainters,
  cxButtons, ImgList, TressMorado2013;

type
  TFindServidor_DevEx = class(TZetaDlgModal_DevEx)
    DCOMgb: TGroupBox;
    RemoteServerLBL: TLabel;
    RemoteServer: TEdit;
    UsarDCOM: TRadioButton;
    PanelRelleno: TPanel;
    HTTPgb: TGroupBox;
    PanelEnMedio: TPanel;
    UsarHTTP: TRadioButton;
    SitioWebLBL: TLabel;
    UserNameLBL: TLabel;
    PasswordLBL: TLabel;
    SitioWeb: TEdit;
    UserName: TEdit;
    Password: TEdit;
    ServidorSeek_DevEx: TcxButton;
    procedure ServidorSeekClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure UsarDCOMClick(Sender: TObject);
    procedure UsarHTTPClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    function GetServidor: String;
    {$ifdef HTTP_CONNECTION}
    function GetWebPassword: String;
    function GetConnectionType: TTipoConexion;
    function GetWebPage: String;
    function GetWebUser: String;
    procedure SetWebPassword(const Value: String);
    procedure SetConnectionType(const Value: TTipoConexion);
    procedure SetWebPage(const Value: String);
    procedure SetWebUser(const Value: String);
    procedure SetControls;
    {$endif}
    procedure SetServidor(const Value: String);
  public
    { Public declarations }
    property Servidor: String read GetServidor write SetServidor;
    {$ifdef HTTP_CONNECTION}
    property ConnectionType: TTipoConexion read GetConnectionType write SetConnectionType;
    property WebPage: String read GetWebPage write SetWebPage;
    property WebUser: String read GetWebUser write SetWebUser;
    property WebPassword: String read GetWebPassword write SetWebPassword;
    {$endif}
  end;

var
  FindServidor_DevEx: TFindServidor_DevEx;

implementation

uses ZetaNetworkBrowser_DevEx,
     ZetaDialogo,
     ZetaWinAPITools,
     ZetaCommonTools,
     ZetaCommonClasses;

{$R *.DFM}

{ TFindServidor }

procedure TFindServidor_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H80084_Especificar_Servidor;
end;

procedure TFindServidor_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     {$ifdef HTTP_CONNECTION}
     SetControls;
     {$endif}
end;

function TFindServidor_DevEx.GetServidor: String;
begin
     Result := RemoteServer.Text;
end;

procedure TFindServidor_DevEx.SetServidor(const Value: String);
begin
     RemoteServer.Text := Value;
end;

{$ifdef HTTP_CONNECTION}
function TFindServidor_DevEx.GetWebPassword: String;
begin
     Result := Self.Password.Text;
end;

function TFindServidor_DevEx.GetConnectionType: TTipoConexion;
begin
     if Self.UsarHTTP.Checked then
        Result := conxHTTP
     else
         Result := conxDCOM;
end;

function TFindServidor_DevEx.GetWebPage: String;
begin
     Result := Self.SitioWeb.Text;
end;

function TFindServidor_DevEx.GetWebUser: String;
begin
     Result := Self.UserName.Text;
end;

procedure TFindServidor_DevEx.SetWebPassword(const Value: String);
begin
     Self.Password.Text := Value;
end;

procedure TFindServidor_DevEx.SetConnectionType(const Value: TTipoConexion);
begin
     case Value of
          conxHTTP:
          begin
               Self.UsarDCOM.Checked := False;
               Self.UsarHTTP.Checked := True;
          end
          else
          begin
               Self.UsarDCOM.Checked := True;
               Self.UsarHTTP.Checked := False;
          end;
     end;
end;

procedure TFindServidor_DevEx.SetWebPage(const Value: String);
begin
     Self.SitioWeb.Text := Value;
end;

procedure TFindServidor_DevEx.SetWebUser(const Value: String);
begin
     Self.UserName.Text := Value;
end;

procedure TFindServidor_DevEx.SetControls;
begin
     RemoteServerLBL.Enabled := UsarDCOM.Checked;
     RemoteServer.Enabled := UsarDCOM.Checked;
     ServidorSeek_DevEx.Enabled := UsarDCOM.Checked;
     SitioWebLBL.Enabled := UsarHTTP.Checked;
     SitioWeb.Enabled := UsarHTTP.Checked;
     UserNameLBL.Enabled := UsarHTTP.Checked;
     UserName.Enabled := UsarHTTP.Checked;
     PasswordLBL.Enabled := UsarHTTP.Checked;
     Password.Enabled := UsarHTTP.Checked;
     if UsarDCOM.Checked then
        ActiveControl := RemoteServer;
     if UsarHTTp.Checked then
        ActiveControl := SitioWeb;
end;
{$endif}

procedure TFindServidor_DevEx.ServidorSeekClick(Sender: TObject);
begin
     inherited;
     Servidor := ZetaNetworkBrowser_DevEx.GetServer( RemoteServer.Text );
end;


procedure TFindServidor_DevEx.UsarDCOMClick(Sender: TObject);
begin
     inherited;
     {$ifdef HTTP_CONNECTION}
     Self.UsarHTTP.Checked := not Self.UsarDCOM.Checked;
     SetControls;
     {$endif}
end;

procedure TFindServidor_DevEx.UsarHTTPClick(Sender: TObject);
begin
     inherited;
     {$ifdef HTTP_CONNECTION}
     Self.UsarDCOM.Checked := not Self.UsarHTTP.Checked;
     SetControls;
     {$endif}
end;

procedure TFindServidor_DevEx.OK_DevExClick(Sender: TObject);
{$ifdef HTTP_CONNECTION}
const
     K_WININET = 'WinInet.dll';
var
   sArchivo: string;
{$endif}
begin

     inherited;
     {$ifdef HTTP_CONNECTION}
     if ( ConnectionType = conxHTTP ) then
     begin
          sArchivo := VerificaDir( ZetaWinAPITools.GetWinSysDir ) + K_WININET;
          if FileExists( sArchivo ) then
             ModalResult := mrOk
          else
             ZError(Caption, Format( 'No se encontr� instalado el archivo  "%s". ' + CR_LF +
                                     'Consulte a su Administrador', [sArchivo] ), 0);
     end
     else
     {$endif}
         ModalResult := mrOk;
end;

end.
