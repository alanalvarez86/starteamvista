unit ZetaBuscaEmpleado;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, Grids, DBGrids,
     ExtCtrls, Db, Mask,
     {$ifndef VER130}MaskUtils,{$endif}
     ZetaMessages,
     ZetaDBGrid,
     ZetaClientDataset, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore, TressMorado2013, cxButtons, ImgList, cxControls,
  cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  ZetaCXGrid;

type
  TBuscaEmpleado = class(TForm)
    PanelControles: TPanel;
    ApePatLabel: TLabel;
    ApeMatLabel: TLabel;
    NombresLBL: TLabel;
    ApePatEdit: TEdit;
    ApeMatEdit: TEdit;
    NombresEdit: TEdit;
    RFCLabel: TLabel;
    NSSLabel: TLabel;
    NSSEdit: TEdit;
    BuscarBtn: TcxButton;
    AceptarBtn: TcxButton;
    CancelarBtn: TcxButton;
    DataSource: TDataSource;
    RFCEdit: TMaskEdit;
    MostrarBajas: TCheckBox;
    BancaLabel: TLabel;
    BancaEdit: TEdit;
    cxImageList24_PanelBotones: TcxImageList;
    GridEmpleados: TZetaCXGrid;
    GridEmpleadosDBTableView: TcxGridDBTableView;
    GridEmpleadosLevel: TcxGridLevel;
    CB_CODIGO: TcxGridDBColumn;
    CB_APE_PAT: TcxGridDBColumn;
    CB_APE_MAT: TcxGridDBColumn;
    CB_NOMBRES: TcxGridDBColumn;
    CB_RFC: TcxGridDBColumn;
    CB_SEGSOC: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BuscarBtnClick(Sender: TObject);
    procedure AceptarBtnClick(Sender: TObject);
    procedure ApePatEditChange(Sender: TObject);
    procedure GridEmpleadosDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure MostrarBajasClick(Sender: TObject);
    procedure FiltraTextoEditChange(Sender: TObject);
  private
    { Private declarations }
    FFiltro: String;
    FNumeroEmpleado: Integer;
    FNombreEmpleado: String;
    FCapturando: Boolean;
    FLookupDataSet: TZetaClientDataset;
    FEmpresaActual: OleVariant;
    //function LookupDataset: TZetaClientDataset;
    procedure Buscar;
    procedure SetNumero;
    procedure SetBotones( const Vacio : Boolean );
    procedure ShowGrid( const lVisible: Boolean );
    procedure Connect;
    procedure Disconnect;
    procedure RemoveFilter;
    procedure SetFilter;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    function GetStatusAct: Boolean;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property NumeroEmpleado: Integer read FNumeroEmpleado;
    property NombreEmpleado: String read FNombreEmpleado;
    property Filtro: String read FFiltro write FFiltro;
    property LookupDataSet: TZetaClientDataset read FLookupDataSet write FLookupDataSet;
    property EmpresaActual: OleVariant read FEmpresaActual write FEmpresaActual;
  end;

procedure SetOnlyActivos( const lActivos: Boolean );

var
  BuscaEmpleado: TBuscaEmpleado;

function BuscaEmpleadoDialogoDataSet( const sFilter: String; var sKey, sDescription: String; const oDataSet: TZetaClientDataset; oEmpresa: OleVariant ): Boolean;
function BuscaEmpleadoDialogo( const sFilter: String; var sKey, sDescription: String ): Boolean;

implementation

uses ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaClientTools,
     DGlobal,
     ZGlobalTress,
     DCliente,
     ZFiltroSQLTools;

var
   FShowOnlyActivos: Boolean;

{$R *.DFM}

procedure SetOnlyActivos( const lActivos: Boolean );
begin
     FShowOnlyActivos := lActivos;
end;



function BuscaEmpleadoDialogoDataSet( const sFilter: String; var sKey, sDescription: String; const oDataSet: TZetaClientDataset; oEmpresa: OleVariant ): Boolean;
begin
     Result := False;
     if ( BuscaEmpleado = nil ) then
        BuscaEmpleado := TBuscaEmpleado.Create( Application );
     if ( BuscaEmpleado <> nil ) then
     begin
          with BuscaEmpleado do
          begin
               LookupDataset := oDataSet;
               EmpresaActual := oEmpresa;
               Filtro := sFilter;
               ShowModal;
               if ( ModalResult = mrOk ) and ( NumeroEmpleado <> 0 ) then
               begin
                    sKey := IntToStr( NumeroEmpleado );
                    sDescription := NombreEmpleado;
                    Result := True;
               end;
          end;
     end;
end;

function BuscaEmpleadoDialogo( const sFilter: String; var sKey, sDescription: String ): Boolean;
begin
     Result := BuscaEmpleadoDialogoDataSet( sFilter, sKey, sDescription, dmCliente.cdsEmpleadoLookup, dmCliente.Empresa );
end;

{************** TBuscaEmpleado ************** }

procedure TBuscaEmpleado.FormCreate(Sender: TObject);
begin
     HelpContext := H00012_Busqueda_empleados;
     BancaEdit.MaxLength := 30;
end;

procedure TBuscaEmpleado.FormShow(Sender: TObject);
begin

     WindowState := wsNormal;
     ActiveControl := ApePatEdit;
     Connect;
end;

procedure TBuscaEmpleado.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Disconnect;
     Action := caHide;
end;

{function TBuscaEmpleado.LookupDataset: TZetaClientDataset;
begin
     Result := dmCliente.cdsEmpleadoLookup;
end;}

function TBuscaEmpleado.GetStatusAct:Boolean;
begin
     Result := LookupDataset.FindField('STATUS') <> NIL;
     if Result then
        Result := LookupDataSet.FieldByName('STATUS').AsInteger <> Ord(steBaja);
end;


procedure TBuscaEmpleado.SetFilter;
var
   sStatusFiltro :string;
begin
     sStatusFiltro := VACIO;

     if LookupDataset.FindField('STATUS') <> NIL then
        sStatusFiltro := Format( 'or ( STATUS <> %d )',[ Ord( steBaja ) ] );
        
     with LookupDataset do
     begin
          DisableControls;
          try
             Filtered := False;
             if MostrarBajas.Checked then
                Filter := Filtro
             else
                 Filter := ZetaCommonTools.ConcatFiltros( Filtro, Format('( CB_ACTIVO = %s ) %s',[EntreComillas(K_GLOBAL_SI),sStatusFiltro ] ) );
             Filtered := True;
          finally
                 EnableControls;
          end;
     end;
end;



procedure TBuscaEmpleado.RemoveFilter;
begin
     with LookupDataset do
     begin
          DisableControls;
          try
             if Filtered then
             begin
                  Filtered := False;
                  Filter := VACIO;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TBuscaEmpleado.Connect;
begin
     SetFilter;
     ShowGrid( False );
     SetBotones( True );
     ActiveControl := ApePatEdit;
     with MostrarBajas do
     begin
          if FShowOnlyActivos then
          begin
               Checked := False;
               Enabled := False;
               Visible := False;
          end
          else
          begin
               Enabled := True;
               Visible := True;
          end;
     end;
     ApePatEdit.Clear;
     ApeMatEdit.Clear;
     NombresEdit.Clear;
     RFCEdit.Clear;
     NSSEdit.Clear;
     BancaEdit.Clear;
     Datasource.Dataset := LookupDataset;
end;

procedure TBuscaEmpleado.Disconnect;
begin
     Datasource.Dataset := nil;
     with LookupDataset do
     begin
          if Filtered then
          begin
               RemoveFilter;
          end;
     end;
end;

procedure TBuscaEmpleado.Buscar;
var
   oCursor: TCursor;
begin
     AceptarBtn.Enabled := False;
     Application.ProcessMessages;
     oCursor := Screen.Cursor;
     try
        Screen.Cursor := crHourglass;
        with LookupDataset do
        begin
             DisableControls;
             try
                Active := False;
                dmCliente.GetEmpleadosBuscados( ApePatEdit.Text,
                                                ApeMatEdit.Text,
                                                NombresEdit.Text,
                                                RFCEdit.Text,
                                                NSSEdit.Text,
                                                BancaEdit.Text,
                                                EmpresaActual,
                                                LookupDataSet );
                Active := True;
             finally
                    EnableControls;
             end;
             if IsEmpty then
             begin
                  zWarning( 'B�squeda de Empleados', '� No Hay Empleados Con Estos Datos !', 0, mbOK );
                  SetBotones( True );
                  ActiveControl := ApePatEdit;
             end
             else
             begin
                  SetFilter; 
                  ShowGrid( True );
                  SetBotones( False );
                  ActiveControl := GridEmpleados;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBuscaEmpleado.SetNumero;
begin
     with LookupDataset do
     begin
          FNumeroEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
          FNombreEmpleado := FieldByName( 'PrettyName' ).AsString;
     end;
     ModalResult := mrOK;
end;

procedure TBuscaEmpleado.SetBotones( const Vacio: Boolean );
begin
     FCapturando := Vacio;
     BuscarBtn.Default := Vacio;
     with AceptarBtn do
     begin
          Default := not Vacio;
          Enabled := not Vacio;
     end;
end;

procedure TBuscaEmpleado.ShowGrid( const lVisible: Boolean );
const
     GRID_HEIGHT = 140;
begin
     with GridEmpleados do
     begin
          if lVisible then
          begin
               Visible:= True;
               Height := ZetaClientTools.GetScaledHeight( GRID_HEIGHT );
               Self.ClientHeight := ZetaClientTools.GetScaledHeight( PanelControles.Height + GRID_HEIGHT );
          end
          else
          begin
               Visible:= False;
               Height := 0;
               Self.ClientHeight := ZetaClientTools.GetScaledHeight( PanelControles.Height );
          end;
     end;
     Repaint;
end;

procedure TBuscaEmpleado.WMExaminar(var Message: TMessage);
begin
     AceptarBtn.Click;
end;

procedure TBuscaEmpleado.BuscarBtnClick(Sender: TObject);
begin
     Buscar;
end;

procedure TBuscaEmpleado.AceptarBtnClick(Sender: TObject);
begin
     if not LookupDataset.IsEmpty then
        SetNumero;
end;

procedure TBuscaEmpleado.ApePatEditChange(Sender: TObject);
begin
     if not FCapturando then
        SetBotones( True );

     FiltraTextoEditChange( Sender );
end;

procedure TBuscaEmpleado.GridEmpleadosDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     with GridEmpleados do
     begin
          if ( gdSelected in State )  then
          begin
               with Canvas do
               begin
                    Font.Color := clWhite;
                    Brush.Color := clNavy;
               end;
          end
          else
          begin
               with Canvas do
               begin
                    Brush.Color := clWhite;
                    if(  zStrToBool( LookupDataset.FieldByName( 'CB_ACTIVO' ).AsString ) or  GetStatusAct )then
                       Font.Color := GridEmpleados.Font.Color
                    else
                        Font.Color := clRed;
               end;
          end;
          //DefaultDrawDataCell( Rect, Column.Field, State );
     end;
end;

procedure TBuscaEmpleado.MostrarBajasClick(Sender: TObject);
begin
     SetFilter;
end;

procedure TBuscaEmpleado.FiltraTextoEditChange(Sender: TObject);
var
   OldChange: TNotifyEvent;
   OldStart : Integer;
begin
    with (Sender as TEdit ) do
    begin
         OldChange := OnChange;
         OnChange  := nil;
         OldStart  := SelStart;
         Text := FiltrarTextoSQL( Text );
         OnChange := OldChange;
         SelStart := OldStart;
    end;
end;

end.
