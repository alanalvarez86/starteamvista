unit FToolsFoto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DBCtrls, Db, ExtCtrls, ExtDlgs,
  TDMULTIP, TMultiP;

function ResizeImageBestFit( PMultiImage : TPDBMultiImage; Height : Integer; Width : Integer): Boolean;{OP: 02/06/08}
function ResizeImageKeepProportions( PMultiImage  : TPDBMultiImage; NewDimension : Integer; IsWidth : Boolean): Boolean;{OP: 02/06/08}

function ResizePMultiImageBestFit( PMultiImage : TPMultiImage; Height : Integer; Width : Integer): Boolean;{AV: 12/29/11}
function ResizePMultiImageKeepProportions( PMultiImage  : TPMultiImage; NewDimension : Integer; IsWidth : Boolean): Boolean;{AV: 12/29/11}


implementation

function ResizeImageBestFit( PMultiImage : TPDBMultiImage; Height : Integer; Width : Integer): Boolean;{OP: 02/06/08}
begin
   Result := ResizePMultiImageBestFit( TPMultiImage( PMultiImage ), Height, Width );
end;

function ResizeImageKeepProportions( PMultiImage  : TPDBMultiImage; NewDimension : Integer; IsWidth : Boolean): Boolean;{OP: 02/06/08}
begin
     Result := ResizePMultiImageKeepProportions(   TPMultiImage( PMultiImage ), NewDimension, IsWidth );
end;

{OP: 02/06/08}
function ResizePMultiImageBestFit( PMultiImage  : TPMultiImage; Height       : Integer; Width        : Integer): Boolean;
Var
  inWidthOld   : Integer;
  inHeightOld  : Integer;
  boStretch    : Boolean;
  IsWidth      : Boolean;
  NewDimension : Integer;
begin
  boStretch    := PMultiImage.StretchRatio;
  Try
    Try
      PMultiImage.StretchRatio := False;
      inWidthOld  := PMultiImage.Picture.Bitmap.Width;
      inHeightOld := PMultiImage.Picture.Bitmap.Height;
      IsWidth     := (((inHeightOld * Width) div inWidthOld)<=Height);
      If IsWidth Then
      Begin
        NewDimension := Width;
      End
      Else
      Begin
        NewDimension := Height;
      End;
      Result :=
        ResizePMultiImageKeepProportions(
          PMultiImage  ,  //PMultiImage  : TPMultiImage;
          NewDimension ,  //NewDimension : Integer;
          IsWidth      ); //IsWidth      : Boolean): Boolean;
    Except
      Result := False;
    End;
  Finally
    PMultiImage.StretchRatio := boStretch;
  End;
end;

{OP: 02/06/08}
function ResizePMultiImageKeepProportions( PMultiImage  : TPMultiImage; NewDimension : Integer; IsWidth : Boolean): Boolean;
Var
  inWidthOld   : Integer;
  inWidthNew   : Integer;
  inHeightOld  : Integer;
  inHeightNew  : Integer;
  Bitmap       : TBitmap;
  boStretch    : Boolean;
begin
  boStretch:= PMultiImage.StretchRatio;
  Bitmap       := TBitmap.Create;
  Try
    Try
      {Set Stretch Off}
      PMultiImage.StretchRatio := False;
      {Create a new bitmap and set its size}
      inWidthOld  := PMultiImage.Picture.Bitmap.Width;
      inHeightOld := PMultiImage.Picture.Bitmap.Height;
      If IsWidth Then
      Begin
       inWidthNew  := NewDimension;
       inHeightNew := (inHeightOld * inWidthNew) div inWidthOld;
      End
      Else
      Begin
        inHeightNew := NewDimension;
        inWidthNew  := (inWidthOld * inHeightNew) div inHeightOld;
      End;
      Bitmap.Width  := inWidthNew;
      Bitmap.Height := inHeightNew;
      {Copy the palette}
      Bitmap.Palette:=PMultiImage.Picture.Bitmap.Palette;
      {Delete the lines needed to shrink}
      SetStretchBltMode(Bitmap.Canvas.Handle,STRETCH_DELETESCANS);
      {Resize it}
      Bitmap.Canvas.Copyrect(Rect(0,
                                 0,
                                 inWidthNew,
                                 inHeightNew),
                            PMultiImage.Picture.Bitmap.Canvas,
                            Rect(0,
                                 0,
                                 PMultiImage.Picture.Bitmap.Width,
                                 PMultiImage.Picture.Bitmap.Height));
      {Copy the palette}
      Bitmap.Palette:=PMultiImage.Picture.Bitmap.Palette;
      {Assign the new smaller bitmap}
      PMultiImage.Picture.Bitmap.Assign(Bitmap);
      {Free the bitmap}

      Result := True;
    Except
      Result := False;
    End;
  Finally
    Bitmap.Free;
    PMultiImage.StretchRatio := boStretch;
  End;
end;

end.
 