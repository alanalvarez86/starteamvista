unit ZetaBuscaPeriodo_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, Grids, DBGrids,
     StdCtrls, Buttons, ExtCtrls,
     ZetaMessages,
     ZetaClientDataset,
     ZetaDBGrid, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, Menus, cxButtons, cxCalendar;

type
  TBuscaPeriodo_DevEx = class(TForm)
    PanelInferior: TPanel;
    DataSource: TDataSource;
    DBGrid_DevEx: TZetaCXGrid;
    DBGrid_DevExDBTableView: TcxGridDBTableView;
    PE_NUMERO: TcxGridDBColumn;
    PE_FEC_INI: TcxGridDBColumn;
    DBGrid_DevExLevel: TcxGridLevel;
    PE_FEC_FIN: TcxGridDBColumn;
    PE_FEC_PAG: TcxGridDBColumn;
    PE_YEAR: TcxGridDBColumn;
    OK_DevEx: TcxButton;
    Cancelar_DevEx: TcxButton;
    Refrescar_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure OKClick(Sender: TObject);
    procedure RefrescarClick(Sender: TObject);
    procedure DBGrid_DevExDBTableViewCellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure DBGrid_DevExDBTableViewKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    FDataset: TZetaLookupDataset;
    FNumero: Integer;
    FFiltro: String;
    procedure Connect;
    procedure Disconnect;
    procedure SetDataset( Value: TZetaLookupDataset );
    {
    procedure SetFiltro( const Value: String );
    }
    procedure RemoveFilter;
    procedure SetFilter;
    //procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
  public
    { Public declarations }
    property Dataset: TZetaLookupDataset read FDataset write SetDataset;
    property Numero: Integer read FNumero write FNumero;
    property Filtro: String read FFiltro write FFiltro;
  end;

function BuscaPeriodoDialogo( LookupDataset: TZetaLookupDataset; const sFilter: String; var sKey, sDescription: String ): Boolean;

var
  BuscaPeriodo_DevEx: TBuscaPeriodo_DevEx;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo;

{$R *.DFM}

function BuscaPeriodoDialogo( LookupDataset: TZetaLookupDataset; const sFilter: String; var sKey, sDescription: String ): Boolean;
begin
     Result := False;
     if ( BuscaPeriodo_DevEx = nil ) then
        BuscaPeriodo_DevEx := TBuscaPeriodo_DevEx.Create( Application );
     if ( BuscaPeriodo_DevEx <> nil ) then
     begin
          with BuscaPeriodo_DevEx do
          begin
               Dataset := LookupDataset;
               Filtro := sFilter;
               Numero := StrToIntDef( sKey, 0 );
               ShowModal;
               if ( ModalResult = mrOk ) and ( Numero <> 0 ) then
               begin
                    sKey := IntToStr( Numero );
                    sDescription := Dataset.GetDescription;
                    Result := True;
               end;
          end;
     end;
end;

{ ******* TBuscaPeriodo ********* }

procedure TBuscaPeriodo_DevEx.FormCreate(Sender: TObject);
begin
     HelpContext := H00015_busqueda_catalogos;
end;

procedure TBuscaPeriodo_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     {Se conecta primero ya que tenia problemas al momento de hacer el setfilter para volver a posicionarse}
     Connect;
     with Dataset do
     begin
          if ( Numero <> 0 ) then
             Locate( 'PE_NUMERO', Numero, [] );
     end;
     ActiveControl := DBGrid_DevEx;
end;

procedure TBuscaPeriodo_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Disconnect;
     Action := caHide;
end;

{
procedure TBuscaPeriodo.SetFiltro( const Value: String );
begin
     if ( Value = '' ) then
        FFiltro := ''
     else
         FFiltro := ZetaCommonTools.Parentesis( Value );
end;
}

procedure TBuscaPeriodo_DevEx.SetDataset( Value: TZetaLookupDataset );
begin
     if ( FDataset <> Value ) then
     begin
          FDataset := Value;
     end;
end;

procedure TBuscaPeriodo_DevEx.SetFilter;
var
   Pos : TBookMark;
begin
     if ( Filtro <> '' ) then
     begin
          with Dataset do
          begin
               DisableControls;
               try
                  Pos:= GetBookMark;
                  Filtered := False;
                  Filter := Filtro;
                  Filtered := True;
                  if ( Pos <> nil ) then
                  begin
                       if BookMarkValid( Pos ) then
                          GotoBookMark( Pos );
                       FreeBookMark( Pos );
                  end;
               finally
                      EnableControls;
               end;
          end;
     end;
end;

procedure TBuscaPeriodo_DevEx.RemoveFilter;
begin
     with Dataset do
     begin
          if Filtered then
          begin
               Filtered := False;
               Filter := '';
          end;
     end;
end;

procedure TBuscaPeriodo_DevEx.Connect;
begin
     SetFilter;
     Datasource.Dataset := Dataset;
end;

procedure TBuscaPeriodo_DevEx.Disconnect;
var
   bAnterior: TBookMark;
begin
     Datasource.Dataset := nil;
     with DataSet do
     begin
          if Filtered then
          begin
               bAnterior := GetBookMark;
               RemoveFilter;
               GotoBookMark( bAnterior );
               FreeBookMark( bAnterior );
          end;
     end;
end;

{procedure TBuscaPeriodo_DevEx.WMExaminar(var Message: TMessage);
begin
     OK_DevEx.Click;
end;}

procedure TBuscaPeriodo_DevEx.OKClick(Sender: TObject);
begin
     FNumero := Dataset.FieldByName( 'PE_NUMERO' ).AsInteger;
end;

procedure TBuscaPeriodo_DevEx.RefrescarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        Disconnect;
        Dataset.Refrescar;
        Connect;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBuscaPeriodo_DevEx.DBGrid_DevExDBTableViewCellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
   OK_DevEx.Click;
end;

procedure TBuscaPeriodo_DevEx.DBGrid_DevExDBTableViewKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     if Key = VK_RETURN then
     begin
           if OK_DevEx.Visible and OK_DevEx.Enabled then
          begin
               OK_DevEx.SetFocus;
               OK_DevEx.Click;
          end;
     end;
end;

end.
