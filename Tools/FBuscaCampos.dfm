object BuscaCampos: TBuscaCampos
  Left = 413
  Top = 127
  AutoScroll = False
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Diccionario de Datos'
  ClientHeight = 310
  ClientWidth = 461
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 269
    Width = 461
    Height = 41
    Align = alBottom
    TabOrder = 1
    object Panel3: TPanel
      Left = 275
      Top = 1
      Width = 185
      Height = 39
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object OK: TBitBtn
        Left = 14
        Top = 8
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = '&OK'
        ModalResult = 1
        TabOrder = 0
        OnClick = OKClick
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000010000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333330000333333333333333333333333F33333333333
          00003333344333333333333333388F3333333333000033334224333333333333
          338338F3333333330000333422224333333333333833338F3333333300003342
          222224333333333383333338F3333333000034222A22224333333338F338F333
          8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
          33333338F83338F338F33333000033A33333A222433333338333338F338F3333
          0000333333333A222433333333333338F338F33300003333333333A222433333
          333333338F338F33000033333333333A222433333333333338F338F300003333
          33333333A222433333333333338F338F00003333333333333A22433333333333
          3338F38F000033333333333333A223333333333333338F830000333333333333
          333A333333333333333338330000333333333333333333333333333333333333
          0000}
        NumGlyphs = 2
      end
      object BitBtn2: TBitBtn
        Left = 98
        Top = 8
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = '&Cancelar'
        TabOrder = 1
        OnClick = BitBtn2Click
        Kind = bkCancel
      end
    end
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 461
    Height = 269
    ActivePage = tsPorCampo
    Align = alClient
    TabOrder = 0
    object tsPorCampo: TTabSheet
      Caption = 'Por Campo'
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 453
        Height = 61
        Align = alTop
        TabOrder = 0
        object Label2: TLabel
          Left = 12
          Top = 21
          Width = 36
          Height = 13
          Caption = '&Campo:'
          FocusControl = NombreEdit
        end
        object NombreEdit: TEdit
          Left = 52
          Top = 17
          Width = 201
          Height = 21
          CharCase = ecUpperCase
          MaxLength = 10
          TabOrder = 0
          OnChange = NombreEditChange
        end
        object BuscarBtn: TBitBtn
          Left = 262
          Top = 15
          Width = 75
          Height = 25
          Caption = '&Buscar'
          Enabled = False
          TabOrder = 1
          OnClick = BuscarBtnClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33033333333333333F7F3333333333333000333333333333F777333333333333
            000333333333333F777333333333333000333333333333F77733333333333300
            033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
            33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
            3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
            33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
            333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
            333333773FF77333333333370007333333333333777333333333}
          NumGlyphs = 2
        end
        object CriterioRG: TRadioGroup
          Left = 344
          Top = 3
          Width = 97
          Height = 49
          Caption = 'Por'
          ItemIndex = 1
          Items.Strings = (
            'Nombre'
            'Descripción')
          TabOrder = 2
          OnClick = CriterioRGClick
        end
      end
      object GridPorCampo: TZetaDBGrid
        Left = 0
        Top = 61
        Width = 453
        Height = 180
        Align = alClient
        DataSource = dsPorCampo
        Options = [dgTitles, dgColumnResize, dgTabs, dgRowSelect, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = GridPorCampoDblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'DI_TABLA'
            Title.Caption = 'Tabla'
            Width = 75
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DI_NOMBRE'
            Title.Caption = 'Nombre'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DI_TITULO'
            Title.Caption = 'Descripción'
            Width = 254
            Visible = True
          end>
      end
    end
    object tsPorTabla: TTabSheet
      Caption = 'Por Tabla'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 453
        Height = 57
        Align = alTop
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 23
          Width = 30
          Height = 13
          Caption = '&Tabla:'
          FocusControl = EntidadesCombo
        end
        object EntidadesCombo: TComboBox
          Left = 48
          Top = 18
          Width = 289
          Height = 22
          Style = csDropDownList
          DropDownCount = 15
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ItemHeight = 0
          ParentFont = False
          TabOrder = 0
          OnChange = EntidadesComboChange
        end
        object rgMostrar: TRadioGroup
          Left = 344
          Top = 2
          Width = 105
          Height = 54
          Caption = 'Usar'
          ItemIndex = 0
          Items.Strings = (
            'Códigos'
            'Descripciones')
          TabOrder = 1
          OnClick = EntidadesComboChange
        end
      end
      object GridPorTabla: TZetaDBGrid
        Left = 0
        Top = 57
        Width = 453
        Height = 184
        Align = alClient
        DataSource = dsPorTabla
        Options = [dgTitles, dgColumnResize, dgTabs, dgRowSelect, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'DI_NOMBRE'
            Title.Caption = 'Nombre'
            Width = 104
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DI_TITULO'
            Title.Caption = 'Descripción'
            Width = 328
            Visible = True
          end>
      end
    end
  end
  object dsPorCampo: TDataSource
    Left = 224
    Top = 4
  end
  object dsPorTabla: TDataSource
    Left = 256
    Top = 4
  end
end
