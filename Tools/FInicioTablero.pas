unit FInicioTablero;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ZBaseConsulta, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls,UITypes,
  VCLTee.TeCanvas,
  FIndicadores,
  FIndicadorListado,
  FGridPanel,
  FGraficasDashlets,
  DDashlets,
  ZetaClientDataSet,
  ZetaCommonLists,
  FGeneradorLinksTableros,
  ZetaRegistryCliente,
  FBaseGridLayaout,
  ZetaCommonTools,
  ZetaDialogo, VclTee.TeeGDIPlus, VCLTee.TeeData, VCLTee.TeEngine,
  VCLTee.TeeFunci, VCLTee.Series, VCLTee.TeeProcs, VCLTee.Chart,
  cxGrid, VCLTee.DBChart, VCLTee.TeeDBCrossTab, ZetaFecha, ZetaCXGrid,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDBData, Vcl.Mask, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGraphics,
  cxControls, Vcl.ToolWin, Vcl.ActnMan, Vcl.ActnCtrls, Vcl.ActnMenus,
  dxCustomTileControl, dxTileControl, cxContainer, cxLabel, ZetaCommonClasses;

type
  TInicioTablero = class(TBaseConsulta)
    PanelFechaGraficaAsistencia: TPanel;
    pnlParentLinks: TPanel;
    panelGraficas: TPanel;
    GraficaBasePie: TChart;
    BarSeries3: TPieSeries;
    AverageTeeFunction4: TAverageTeeFunction;
    BaseGridListado: TZetaCXGrid;
    BaseGridListadoDBTableView1: TcxGridDBTableView;
    BaseGridListadoLevel1: TcxGridLevel;
    GraficaBaseHorizBarras: TChart;
    HorizBarSeries2: THorizBarSeries;
    GraficaBaseColumnasBarras: TChart;
    HorizBarSeries3: TBarSeries;
    PanelControl: TPanel;
    ckMostrarPantalla: TCheckBox;
    PanelFecha: TPanel;
    FechaActiva: TLabel;
    AsistenciaFechaGrafica: TZetaFecha;
    BaseHorizStack: TChart;
    HorizBarSeries1: THorizBarSeries;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure AsistenciaFechaGraficaValidDate(Sender: TObject);
    procedure ckMostrarPantallaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FDashleetsConsulta: TdmDashleets;
    GeneradorLayout: TGridPanel;
    FCambioFechaAsistencia: Boolean;
    FDatosListaTableros: TZetaClientDataSet;
    FRefrescar: Boolean;
    oLinks : TGeneradorLinksTableros;
    csCriticalSection: TRTLCriticalSection;
    procedure ACallBack( const sNombreComponente: String; const oDataSet: TZetaClientDataSet; const sError: String; const eClassEstilo: eEstiloDashlet; const iPosTablero: Integer; const iTableroID: Integer );
    procedure ACallBackAnimation( const iPosTablero: Integer; const iTableroID: Integer; const lInicioAnimacion: Boolean );
    procedure InicializarIndicadores;
  public
    { Public declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure WMDisplayChange(var Message: TWMDisplayChange); message WM_DISPLAYCHANGE;
    procedure Exportar;override;
    procedure Imprimir; override;
  end;

const
     tColores : array [0..0] of string = (
              '$00F3F3F3' //Gris de Background
              );

var
  InicioTablero: TInicioTablero;

implementation

{$R *.dfm}

uses DCliente, FTressShell, ZAccesosTress, ZAccesosMgr;

{ TInicioPruebas2 }


procedure TInicioTablero.ACallBackAnimation(const iPosTablero, iTableroID: Integer; const lInicioAnimacion: Boolean);
begin
     inherited;
     Application.ProcessMessages;
     if Assigned( oLinks ) and ( oLinks.ExistenLinks ) and ( lInicioAnimacion ) then
        oLinks.MostrarMensajeEsperaTablero( True );
end;

procedure TInicioTablero.AsistenciaFechaGraficaValidDate(Sender: TObject);
begin
     TressShell.ActualizarGraficasInicio := True;
     if FCambioFechaAsistencia then
     begin
          dmCliente.FechaAsistencia := AsistenciaFechaGrafica.Valor;
          dmCliente.SesionIDDashlet := FDashleetsConsulta.SetSesionGraficas;
          Connect;
     end;
     if not FCambioFechaAsistencia then FCambioFechaAsistencia := True;
     TressShell.AsistenciaFechavalid(AsistenciaFechaGrafica.Valor);
end;

procedure TInicioTablero.ckMostrarPantallaClick(Sender: TObject);
begin
     inherited;
     ClientRegistry.HomeSistema := ckMostrarPantalla.Checked;
end;

procedure TInicioTablero.Connect;
begin
     inherited;
     if ( dmCliente.CambioValoresActivosGraficas ) or ( TressShell.ActualizarGraficasInicio ) then
     begin
         AsistenciaFechaGrafica.Valor := dmCliente.FechaAsistencia;
         InicializarIndicadores;
     end;
     TressShell.ActualizarGraficasInicio := False;
     dmCliente.RespaldaValoresActivosGraficas;
end;

procedure TInicioTablero.Exportar;
begin
     zInformation( 'Exportar...',Format( 'La Pantalla %s no cuenta con exportaci�n a Excel', [Caption] ), 0);
end;

procedure TInicioTablero.FormCreate(Sender: TObject);
begin
     inherited;
     //Colores TPanel
     panelGraficas.Color := StringToColor(tColores[0]);
     Self.Color := StringToColor(tColores[0]);
     PanelFechaGraficaAsistencia.Color := StringToColor(tColores[0]);
     FechaActiva.Color := StringToColor(tColores[0]);
     PanelControl.Color := StringToColor(tColores[0]);
     //Dataset
     FDashleetsConsulta := TdmDashleets.Create( Self );
     FDatosListaTableros := TZetaClientDataSet.Create( Self );

     //Valores Activos e Inicializacion GRID PANEL
     FCambioFechaAsistencia := False;
     //InicializarIndicadores;
     AsistenciaFechaGrafica.Valor := dmCliente.FechaAsistencia;
     HelpContext := H394003_PantallaInicio_TRESS;
     InitializeCriticalSection(csCriticalSection);
end;

procedure TInicioTablero.FormDestroy(Sender: TObject);
begin
    inherited;
    FreeAndNil( oLinks );
    FreeAndNil( FDashleetsConsulta );
    FreeAndNil( FDatosListaTableros );
    DeleteCriticalSection(csCriticalSection);
end;

procedure TInicioTablero.FormShow(Sender: TObject);
begin
     inherited;
     ckMostrarPantalla.Checked := ClientRegistry.HomeSistema;
     panelGraficas.Visible := Revisa( D_INICIO );
end;

procedure TInicioTablero.ACallBack( const sNombreComponente: String; const oDataSet: TZetaClientDataSet; const sError: String; const eClassEstilo: eEstiloDashlet; const iPosTablero: Integer; const iTableroID: Integer );
var
   I: Integer;
begin
     Application.ProcessMessages;
     EnterCriticalSection(csCriticalSection);
     if Assigned( oLinks.ArrayThreadGraficas[ iPosTablero ] ) and ( not oLinks.ArrayThreadGraficas[ iPosTablero ].StatusTerminate ) and Assigned( oLinks.ArrayBaseGridLayout[ iPosTablero ] ) then
     begin
          GeneradorLayout := TGridPanel( oLinks.ArrayBaseGridLayout[ iPosTablero ].FindComponent( 'GridPanelBase' ));
          if Assigned( GeneradorLayout ) then
          begin
               for I := 0 to GeneradorLayout.ControlCollection.Count - 1 do
               begin
                    if ( oLinks.ArrayThreadGraficas[ iPosTablero ].StatusTerminate ) then
                       Break;
                    with GeneradorLayout.Controls[I] do
                    begin
                         if ( ClassType = TIndicadoresListado ) and (  Name = sNombreComponente  ) then //Indicadores Listado TcxGrid
                         begin
                              if ( oDataSet = nil ) or ( oDataSet.EOF ) then
                              begin
                                   TIndicadoresListado( GeneradorLayout.Controls[I] ).LimpiarIndicador;
                              end;
                              if ( oDataSet <> nil ) and not ( oDataSet.EOF ) then
                              begin
                                   TIndicadoresListado( GeneradorLayout.Controls[I] ).DataSource.DataSet := oDataSet;
                                   TIndicadoresListado( GeneradorLayout.Controls[I] ).MostrarDatos;
                                   TIndicadoresListado( GeneradorLayout.Controls[I] ).ApplyMinWidth;
                              end;
                              Break;
                         end;
                         if ( ClassType = TIndicadores ) and (  Name = sNombreComponente  ) then //Indicadores
                         begin
                              if ( oDataSet = nil ) or ( oDataSet.EOF ) then
                              begin
                                   TIndicadores( GeneradorLayout.Controls[I] ).LimpiarIndicador;
                              end;
                              if ( oDataSet <> nil ) and not ( oDataSet.EOF ) then
                              begin
                                   TIndicadores( GeneradorLayout.Controls[I] ).Valor := oDataSet.FieldByName( 'valor' ).AsFloat;
                                   TIndicadores( GeneradorLayout.Controls[I] ).Tendencia := oDataSet.FieldByName( 'tendencia' ).AsFloat;
                                   TIndicadores( GeneradorLayout.Controls[I] ).SetPosicionarLabelTendenciaDescripcion;
                                   TIndicadores( GeneradorLayout.Controls[I] ).TamanioTexto;
                              end;
                              Break;
                         end;

                         if ( ClassType = TGraficasDashletsColumnas ) and ( Name = sNombreComponente ) and ( eClassEstilo = tpColumnas ) then //Grafica de Columnas
                         begin
                              if ( oDataSet = nil ) or ( oDataSet.EOF ) then
                              begin
                                   TGraficasDashletsColumnas( GeneradorLayout.Controls[I] ).BorrarSeries;
                              end;
                              if ( oDataSet <> nil ) and not ( oDataSet.EOF ) then
                              begin
                                   TGraficasDashletsColumnas( GeneradorLayout.Controls[I] ).InitChart;
                                   TGraficasDashletsColumnas( GeneradorLayout.Controls[I] ).DataSet := oDataSet;
                                   TGraficasDashletsColumnas( GeneradorLayout.Controls[I] ).Graficar( TBarSeries );
                              end;
                              break;
                         end;

                         if ( ClassType = TGraficasDashletsBarras ) and ( Name = sNombreComponente ) and ( eClassEstilo = tpBarras ) then //Grafica de Barras Horizontales
                         begin
                              if ( oDataSet = nil ) or ( oDataSet.EOF ) then
                              begin
                                   TGraficasDashletsBarras( GeneradorLayout.Controls[I] ).BorrarSeries;
                              end;
                              if ( oDataSet <> nil ) and not ( oDataSet.EOF ) then
                              begin
                                   TGraficasDashletsBarras( GeneradorLayout.Controls[I] ).InitChart;
                                   TGraficasDashletsBarras( GeneradorLayout.Controls[I] ).DataSet := oDataSet;
                                   TGraficasDashletsBarras( GeneradorLayout.Controls[I] ).Graficar( THorizBarSeries );
                              end;
                              break;
                         end;

                         if ( ClassType = TGraficasDashletsPie ) and ( Name = sNombreComponente ) and ( eClassEstilo = tpPastel ) then //Grafica de Pastel
                         begin
                              if ( oDataSet = nil ) or ( oDataSet.EOF ) then
                              begin
                                   TGraficasDashletsPie( GeneradorLayout.Controls[I] ).BorrarSeries;
                              end;
                              if ( oDataSet <> nil ) and not ( oDataSet.EOF ) then
                              begin
                                   TGraficasDashletsPie( GeneradorLayout.Controls[I] ).InitChart;
                                   TGraficasDashletsPie( GeneradorLayout.Controls[I] ).DataSet := oDataSet;
                                   TGraficasDashletsPie( GeneradorLayout.Controls[I] ).Graficar( TPieSeries );
                              end;
                              break;
                         end;

                         if ( ClassType = TGraficasDashletsHorizStack ) and ( Name = sNombreComponente ) and ( eClassEstilo = tpHorizStack ) then //Grafica Stack
                         begin
                              if ( oDataSet = nil ) or ( oDataSet.EOF ) then
                              begin
                                   TGraficasDashletsHorizStack( GeneradorLayout.Controls[I] ).BorrarSeries;
                              end;
                              if ( oDataSet <> nil ) and not ( oDataSet.EOF ) then
                              begin
                                   TGraficasDashletsHorizStack( GeneradorLayout.Controls[I] ).InitChart;
                                   TGraficasDashletsHorizStack( GeneradorLayout.Controls[I] ).DataSet := oDataSet;
                                   TGraficasDashletsHorizStack( GeneradorLayout.Controls[I] ).Graficar( THorizBarSeries );
                              end;
                              break;
                         end;
                    end;
               end;
          end;//validacion componente Grid
     end;//validacion componente Forma
     LeaveCriticalSection(csCriticalSection);
end;

procedure TInicioTablero.Imprimir;
begin
     zInformation( Caption, 'Esta Pantalla no Cuenta con Impresi�n de Formas', 0 );
end;

procedure TInicioTablero.InicializarIndicadores;
var
   sError: String;
begin
     if Revisa( D_INICIO ) then
     begin
          dmCliente.SesionIDDashlet := FDashleetsConsulta.SetSesionGraficas;
          FDatosListaTableros.Data := FDashleetsConsulta.GetListaTableros(sError, dmCliente.SesionIDDashlet);
          if StrVacio( sError ) and not Assigned( oLinks ) then
          begin
               oLinks := TGeneradorLinksTableros.Create( FDatosListaTableros, Self, pnlParentLinks, dmCliente.SesionIDDashlet );
               oLinks.GenerarLinks;
               oLinks.BaseChartBarrasHorizontal := GraficaBaseHorizBarras;
               oLinks.BaseChartColumnasVertical := GraficaBaseColumnasBarras;
               oLinks.BaseChartPie := GraficaBasePie;
               oLinks.BaseGridListado := BaseGridListado;
               oLinks.BaseHorizStack := BaseHorizStack;
               oLinks.AParentPanelBase := panelGraficas;
               oLinks.AOwnerPanelBase := Self;
               oLinks.AParentMain := panelGraficas;
               oLinks.AOwnerMain := Self;
               oLinks.ACallBackGraficas := ACallBack;
               oLinks.ACallBackGraficasAnimation := ACallBackAnimation;
               if oLinks.ExistenLinks then
               begin
                    oLinks.SetValoresActivosTablero;
                    oLinks.CargarDashlets( oLinks.ArrayLabelLink[0] );//Poner El primer elemento del Arreglo Visible
               end;
          end
          else if Assigned( oLinks ) then
          begin
               oLinks.CambioValoresActivosGraficas := dmCliente.CambioValoresActivosGraficas;
               oLinks.ActualizarGraficasInicio := TressShell.ActualizarGraficasInicio;
               oLinks.Refrescar := FRefrescar;
               if oLinks.ExistenLinks then
               begin
                    oLinks.SetValoresActivosTablero;
                    oLinks.CargarDashlets( oLinks.ArrayLabelLink[oLinks.TableroPosActivo] );//Poner El primer elemento del Arreglo Visible

               end;
          end;
     end;
end;

procedure TInicioTablero.WMDisplayChange(var Message: TWMDisplayChange);
begin
//     ShowMessageFmt('The screen resolution has changed to %d�%d�%d.',
//     [Message.Width, Message.Height, Message.BitsPerPixel]);
end;

function TInicioTablero.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede agregar en esta pantalla';
     Result := False;
end;

function TInicioTablero.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede borrar en esta pantalla';
     Result := False;
end;

function TInicioTablero.PuedeModificar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede modificar en esta pantalla';
     Result := False;
end;

procedure TInicioTablero.Refresh;
begin
     FRefrescar := True;
     AsistenciaFechaGrafica.Valor := dmCliente.FechaAsistencia;
     dmCliente.SesionIDDashlet := FDashleetsConsulta.SetSesionGraficas;
     InicializarIndicadores;
     FRefrescar := False;
end;

end.
