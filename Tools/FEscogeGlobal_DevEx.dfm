inherited EscogeGlobal_DevEx: TEscogeGlobal_DevEx
  Top = 222
  Caption = 'Escoge Global'
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel2: TPanel
    object Label1: TLabel
      Left = 16
      Top = 14
      Width = 65
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo de Dato:'
    end
    object TipoGlobal: TComboBox
      Left = 87
      Top = 11
      Width = 145
      Height = 21
      BevelKind = bkFlat
      Ctl3D = False
      DropDownCount = 6
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 0
      OnChange = TipoGlobalChange
      Items.Strings = (
        'L'#243'gicos'
        'N'#250'meros con Decimales'
        'N'#250'meros Enteros'
        'Fechas'
        'Textos'
        'Todos')
    end
  end
  inherited Grid_DevEx: TZetaCXGrid
    inherited Grid_DevExDBTableView: TcxGridDBTableView
      object gl_codigo: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'gl_codigo'
        MinWidth = 70
      end
      object gl_descrip: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'gl_descrip'
        MinWidth = 400
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 344
  end
end
