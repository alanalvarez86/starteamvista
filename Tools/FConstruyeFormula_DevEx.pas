unit FConstruyeFormula_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,
  ZBaseConstruyeFormula_DevEx,
  ZetaTipoEntidad,
  ZetaCommonLists, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore,  
  TressMorado2013, dxSkinsDefaultPainters,
  cxButtons, dxGDIPlusClasses;

type
  TConstruyeFormula_DevEx = class(TBaseConstruyeFormula_DevEx)
    ParamBtn_DevEx: TcxButton;
    ConceptoBtn_DevEx: TcxButton;
    AcumuladoBtn_DevEx: TcxButton;
    //procedure ParamBtnClick(Sender: TObject);
    //procedure ConceptoBtnClick(Sender: TObject);
    //procedure AcumuladoBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ParamBtn_DevExClick(Sender: TObject);
    procedure ConceptoBtn_DevExClick(Sender: TObject);
    procedure AcumuladoBtn_DevExClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ConstruyeFormula_DevEx: TConstruyeFormula_DevEx;


implementation
uses
     FEscogeParametro_DevEx,
     FEscogeConcepto_DevEx,
     FEscogeAcumulado_DevEx,
     dCatalogos;

{$R *.DFM}




procedure TConstruyeFormula_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     //ParamBtn.Enabled := ( FEntidadActiva in NomParamEntidades );
     ParamBtn_DevEx.Enabled := ( FEntidadActiva in NomParamEntidades );
end;

{procedure TConstruyeFormula_DevEx.ParamBtnClick(Sender: TObject);
begin
     dmCatalogos.cdsNomParamLookUp.Conectar;
     AgregaTexto( PickParametro );
end; }

{procedure TConstruyeFormula_DevEx.ConceptoBtnClick(Sender: TObject);
begin


     dmCatalogos.cdsConceptosLookUp.Conectar;
      //TEMPORAL
      if ( dmCliente.GetDatosUsuarioActivo.Vista = tvClasica )then
         AgregaTexto( FEscogeConcepto.PickConcepto )
      else
         AgregaTexto( FEscogeConcepto_DevEx.PickConcepto );
end; }

{procedure TConstruyeFormula_DevEx.AcumuladoBtnClick(Sender: TObject);
begin
     //TEMPORAL
      if ( dmCliente.GetDatosUsuarioActivo.Vista = tvClasica )then
         AgregaTexto( FEscogeAcumulado.PickAcumulado )
      else
          AgregaTexto( FEscogeAcumulado_DevEx.PickAcumulado );
end;  }


procedure TConstruyeFormula_DevEx.ParamBtn_DevExClick(Sender: TObject);
begin
  inherited;
  dmCatalogos.cdsNomParamLookUp.Conectar;
     AgregaTexto( PickParametro );
end;

procedure TConstruyeFormula_DevEx.ConceptoBtn_DevExClick(Sender: TObject);
begin
  inherited;
  dmCatalogos.cdsConceptosLookUp.Conectar;
  AgregaTexto( FEscogeConcepto_DevEx.PickConcepto );
end;

procedure TConstruyeFormula_DevEx.AcumuladoBtn_DevExClick(Sender: TObject);
begin
  inherited;
  AgregaTexto( FEscogeAcumulado_DevEx.PickAcumulado );
end;

end.
