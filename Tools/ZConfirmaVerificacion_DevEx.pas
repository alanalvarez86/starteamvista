unit ZConfirmaVerificacion_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ExtCtrls,
     ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
     cxLookAndFeelPainters, Menus, dxSkinsCore, 
     TressMorado2013, dxSkinsDefaultPainters, ImgList, cxButtons;

type
  TConfirmaVerificacion_DevEx = class(TZetaDlgModal_DevEx)
    CrearLista: TcxButton;
    Mensaje: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ConfirmaVerificacion_DevEx: TConfirmaVerificacion_DevEx;

function ConfirmVerification: TModalResult;

implementation

{$R *.DFM}

function ConfirmVerification: TModalResult;
begin
     with TConfirmaVerificacion_DevEx.Create( Application ) do
     begin
          try
             ShowModal;
             Result := ModalResult;
          finally
                 Free;
          end;
     end;
end;

procedure TConfirmaVerificacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ADUANAS}
     Mensaje.Caption := Format(Mensaje.Caption, ['Pedimentos']);
     {$else}
     Mensaje.Caption := Format(Mensaje.Caption, ['Empleados']);
     {$endif}
end;

end.
