{* Constantes para la unidad ZReport}
unit ZReportConst;

interface

{$INCLUDE DEFINES.INC}

uses
  ZetaTipoEntidad, Variants, ZetaCommonLists;
// TReportesEspeciales = ListaEntidades;

// eClasifiReporte: integer;

const
  K_PARAMETROS = 1;
  {$IFDEF RDD}
  K_CONFIDENCIAL = ' AND R_ATRIBUTO.AT_CONFI =''N'' ';
  {$ELSE}
  K_CONFIDENCIAL = ' AND DICCION.DI_CONFI =''N'' ';
  {$ENDIF}
  K_MAXGRUPOS = 5;
  // PAra RDD se mantiene esta constante, siempre va a ser -1. En cualquier tipo de sistema
  {$IFDEF RDD}
  crFavoritos            = MaxInt;
  crSuscripciones        = MaxInt - 1;
  crKiosco               = 15;
  crSupervisor           = 7;
  crClasificacionSistema = 999;
  {$ELSE}
  {$ENDIF}
  K_MIS_FAVORITOS_DES     = 'Mis Favoritos';
  K_MIS_SUSCRIPCIONES_DES = 'Mis Suscripciones';
  Q_CHR_RELLENO           = CHR(254);
  Q_GRUPOS                = Q_CHR_RELLENO + Q_CHR_RELLENO + Q_CHR_RELLENO;
  Q_EMPRESA               = Q_CHR_RELLENO + Q_CHR_RELLENO + Q_CHR_RELLENO + Q_CHR_RELLENO + Q_CHR_RELLENO;
  Q_MAX_EMPLEADO          = '999999999';
  Q_PRETTY_EMP            = 'PRETTYNAME';
  Q_PRETTY_EXP            = 'PACIENTE()';

function GetParametro(Parametros: Olevariant; const iParam: integer; var eTipo: eTipoGlobal): Olevariant;

implementation

{* RCM No se qu� hace O.o }
function GetParametro(Parametros: Olevariant; const iParam: integer; var eTipo: eTipoGlobal): Olevariant;
begin
  eTipo := tgAutomatico;
  try
    Result := Parametros[iParam];
    if VarIsArray(Result) then begin
      Result := Parametros[iParam][2];
      eTipo  := eTipoGlobal(Parametros[iParam][3]);
    end
    else
      Result := Null;
  except
    Result := Null;
  end;
end;

end.
