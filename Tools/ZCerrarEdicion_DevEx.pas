unit ZCerrarEdicion_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013,
  dxSkinsDefaultPainters, cxButtons;

type
  TCerrarEdicion_DevEx = class(TForm)
    Mensaje: TLabel;
    Image: TImage;
    Pregunta: TLabel;
    Guardar_DevEx: TcxButton;
    SalirSinGuardar_DevEx: TcxButton;
    NoSalir_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CerrarEdicion_DevEx: TCerrarEdicion_DevEx;

function CierraEdicion: TModalResult;

implementation

{$R *.DFM}

uses ZetaCommonClasses;

function CierraEdicion: TModalResult;
begin
     if ( CerrarEdicion_DevEx = nil ) then
     begin
          CerrarEdicion_DevEx :=  TCerrarEdicion_DevEx.Create( Application ); // Se destruye al Salir del Programa //
     end;
     with CerrarEdicion_DevEx do
     begin
          ShowModal;
          Result := ModalResult;
     end;
end;

{ ***** TCerrarEdicion ******** }

procedure TCerrarEdicion_DevEx.FormCreate(Sender: TObject);
begin
     HelpContext := H95001_Confirmacion;
end;

procedure TCerrarEdicion_DevEx.FormShow(Sender: TObject);
begin
     Guardar_DevEx.SetFocus;
end;

end.
