inherited PreguntaPassWord: TPreguntaPassWord
  Left = 446
  Top = 246
  Caption = 'Configuración de Accesos'
  ClientHeight = 89
  ClientWidth = 235
  Color = clTeal
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LblCred: TLabel [0]
    Left = 28
    Top = 13
    Width = 54
    Height = 23
    Caption = 'Clave:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Arial Black'
    Font.Style = []
    ParentFont = False
  end
  inherited PanelBotones: TPanel
    Top = 53
    Width = 235
    TabOrder = 1
    inherited OK: TBitBtn
      Left = 67
      Default = True
    end
    inherited Cancelar: TBitBtn
      Left = 152
    end
  end
  object ePassWord: TZetaEdit
    Left = 87
    Top = 9
    Width = 121
    Height = 31
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial Black'
    Font.Style = []
    HideSelection = False
    ParentFont = False
    PasswordChar = '*'
    TabOrder = 0
  end
end
