unit ZetaDespConsulta;

interface

uses Forms, Controls, Sysutils, Dialogs,
     ZBaseConsulta;

{$define QUINCENALES}

type
  eFormaConsulta = ( efcNinguna,
                     efcEmpIdentifica,
                     efcEmpContratacion,
                     efcEmpArea,
                     efcEmpPercepcion,
                     efcEmpOtros,
                     efcEmpAdicionales,
                     efcEmpPersonal,
                     efcEmpFoto,
                     efcEmpDocumento, // (JB) Anexar al expediente del trabajador documentos CR1880 T1107
                     efcEmpParientes,
                     efcEmpExperiencia,
                     efcEmpAntesCur,
                     efcEmpAntesPto,
                     efcHisKardex,
                     efcHisVacacion,
                     efcHisIncapaci,
                     efcHisPermisos,
                     efcHisCursos,
                     efcHisCursosProg,
                     efcHisPagosIMSS,
                     efcHisAhorros,
                     efcHisPrestamos,
                     efcHisAcumulados,
                     efcHisNominas,
                     efcCafDiarias,
                     efcCafPeriodo,
                     efcCafInvita,
                     efcRegEmpleado,
                     efcProcEmpleado,
                     efcAsisTarjeta,
                     efcNomDatosAsist,
                     efcAsisCalendario,
                     efcAutorizaciones,
                     efcRegAsistencia,
                     efcProcAsistencia,
                     efcNomTotales,
                     efcNomDiasHoras,
                     efcNomMontos,
                     efcNomDatosClasifi,
                     efcNomExcepciones,
                     efcNomExcepMontos,
                     efcNomExcepGlobales,
                     efcRegNomina,
                     efcProcNomina,
                     efcAnualNomina,
                     efcIMSSDatosTot,
                     efcIMSSDatosMensual,
                     efcIMSSDatosBimestral,
                     efcIMSSHisMensual,
                     efcIMSSHisBimestral,
                     efcRegIMSS,
                     efcProcIMSS,
                     efcReportes,
                     efcQueryGral,
                     efcSistBitacora,
                     efcCatPuestos,
                     efcCatClasifi,
                     efcCatOtrasPer,
                     efcCatTurnos,
                     efcCatHorarios,
                     efcCatPrestaciones,
                     efcCatContratos,
                     efcCatConceptos,
                     efcNomParams,
                     efcCatPeriodos,
                     efcCatFolios,
                     efcCatRSocial,
                     efcCatRPatron,
                     efcCatFestGrales,
                     efcCatFestTurno,
                     efcCatCondiciones,
                     efcCatEventos,
                     efcTOTipoCursos,
                     efcCatCursos,
                     efcCatMaestros,
                     efcCatProvCap,
                     efcCatMatrizCurso,
                     efcCatMatrizPuesto,
                     efcCatCalendario,
                     efcSistGlobales,
                     efcDiccion,
                     efcCatReglas,
                     efcCatInvitadores,
                     efcTOEstadoCivil,
                     efcTOHabitacion,
                     efcTOViveCon,
                     efcTOEstudios,
                     efcTOTransporte,
                     efcTOEstado,
                     efcTONivel1,
                     efcTONivel2,
                     efcTONivel3,
                     efcTONivel4,
                     efcTONivel5,
                     efcTONivel6,
                     efcTONivel7,
                     efcTONivel8,
                     efcTONivel9,
                     efcTOExtra1,
                     efcTOExtra2,
                     efcTOExtra3,
                     efcTOExtra4,
                     efcTOMovKardex,
                     efcTOMotivoBaja,
                     efcTOIncidencias,
                     efcTOMotAuto,
                     efcTOTAhorro,
                     efcTOTPresta,
                     efcMonedas,
                     efcSistSalMin,
                     efcSistLeyIMSS,
                     efcSistNumerica,
                     efcSistRiesgo,
                     efcTipoCambio,
                     efcSistEmpresas,
                     efcSistGrupos,
                     efcSistUsuarios,
                     efcSistPoll,
                     efcImpresoras,
                     efcRegSist,
                     efcProcSist,
                     efcAsisPreNomina,
                     efcSistProcesos,
                     efcSistNivel0,
                     efcCatTools,
                     efcTOTallas,
                     efcTOMotTool,
                     efcHisTools,
                     efcConteo,
                     efcTTOClasifiCurso,
                     efcOrganigrama,
                     efcCatSesiones,
                     efcCatAccReglas,
                     efcAsisClasifiTemp,
                     efcTTOColonia,
                     efcTOExtra5,
                     efcTOExtra6,
                     efcTOExtra7,
                     efcTOExtra8,
                     efcTOExtra9,
                     efcTOExtra10,
                     efcTOExtra11,
                     efcTOExtra12,
                     efcTOExtra13,
                     efcTOExtra14,
                     efcCatAulas,
                     efcReservas,
                     efcPrerequisitosCurso,
                     efcCertificaciones,
                     efcKarCertificaciones,
                     efcPolizas,
	                efcTiposPoliza,
{$ifdef QUINCENALES}
                     efcCatTPeriodos,
{$endif}
                     efcPlanVacacion,
                     efcCatPerfPuestos,
                     efcCatSeccPerf,
                     efcCatCamposPerf,
{$IFNDEF DOS_CAPAS}
                     efcCatValuaciones,
                     efcCatValPlantillas,
                     efcCatValFactores,
                     efcCatValSubfactores,
{$ENDIF}
                     efcSimulaMontos,
                     efcBitacoraSistema,
                     efcSistRoles,
                     efcEmpUsuario,
                     efcCatMatrizCertsPuesto,
                     efcCatMatrizPuestosCert,
                     efcKarCertificProgramadas,
                     efcNomFonacotTotales,
                     efcNomFonacotEmpleado,
                     efcHisCreInfonavit,
                     efcIMSSConciliaInfonavit
                     {$ifdef ACS}
                     ,efcTONivel10
                     ,efcTONivel11
                     ,efcTONivel12
                     {$endif}
                     ,efcReglasPrestamos
                     ,efcCasDiarias
                     ,efcSistDispositivos
                     ,efcSimulaMontoGlobal
                     ,efcMotChecaManual
                     ,efcTOcupaNac
                     ,efcTAreaTemCur
                     ,efcTOMunicipios
                     ,efcEstablecimientos
{$IFNDEF DOS_CAPAS}
                     ,efcCosteoTransferencias
                     ,efcCosteoMotTransfer
                     ,efcCatCosteoGrupos
                     ,efcCatCosteoCriterios
                     ,efcCatCosteoCriteriosPorConcepto
                     ,efcBitacoraBio // SYNERGY
                     ,efcSistListaGrupos // SYNERGY
                     ,efcSistTermPorGrupos // SYNERGY
{$ENDIF}
                     ,efcTiposPension
                     ,efcHisPensiones
                     ,efcCatSegurosGastosMedicos
                     ,efcHisSegurosGastosMedicos
                     ,efcTabTCompetencias
                     ,efcTabTPerfiles
                     ,efcCatCompetencias
                     ,efcCatPerfiles
                     ,efcCatMatPerfilComps
                     ,efcHisPlanCapacitacion
                     ,efcHisEvaluaCompeten
                     ,efcMtzHabilidades
                     ,efcCatNacCompetencias
{$IFNDEF DOS_CAPAS}
                     ,efcSistMensajes // SYNERGY
                     ,efcSistMensajesPorTerminal // SYNERGY
                     ,efcSistTerminales // SYNERGY
{$ENDIF}
                     ,efcCatTabAmortizacion
                     ,efcSistBaseDatos
                     ,efcSistActualizarBDs
                     ,efcBancos
                     ,efcUma
                     ,efcTTipoContratoSAT
                     ,efcTTipoJornadaSAT
                     ,efcTRiesgoPuestoSAT
                     ,efcTTiposConceptosSAT
                     ,efcInicio
                     ,efcTablerosPorRoles
                     ,efcHisAcumulados_RS
                     ,efcMtzCursos
                     ,efcSistSolicitudCorreo
                     ,efcSistCalendarioReportes
                     ,efcSistBitacoraReportes
                     ,efcEnviosProgramados
                     ,efcSolicitudEnviosProgramados
                     ,efcEmpDatosGafeteBiometrico
                     );

  TPropConsulta = record
    IndexDerechos: Integer;
    ClaseForma: TBaseConsultaClass;
  end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;

implementation

uses
{$IFDEF TRESS_DELPHIXE5_UP}
     FEmpIdentifica_DevEx,
     FEmpContratacion_DevEx,
     FEmpArea_DevEx,
     FEmpPercepcion_DevEx,
     FEmpOtros_DevEx,
     FEmpAdicionales_DevEx,
     FEmpPersonal,
     FEmpFoto_DevEx,
     FEmpParientes_DevEx,
     FEmpExperiencia,
     FEmpAntesCur_DevEx,
     FEmpAntesPto_DevEx,
     FHisKardex_DevEx,
     FHisVacacion_DevEx,
     FHisIncapaci_DevEx,
     FHisPermisos_DevEx,
     FHisCursos_DevEx,
     FHisCursosProg_DevEx,
     FHisPagosIMSS_DevEx,
     FHisAhorros_DevEx,
     FHisPrestamos_DevEx,
     FHisAcumulados_DevEx,
     FHisNominas_DevEx,
     FCafDiarias_DevEx,
     FCafPeriodo_DevEx,
     FCafInvita_DevEx,
     FAsisTarjeta_DevEx,
     FNomDatosAsist_DevEx,
     FAsisCalendario_DevEx,
     FAutorizaciones_DevEx,
     FNomTotales_DevEx,
     FNomDiasHoras_DevEx,
     FNomMontos_DevEx,
     FNomDatosClasifi_DevEx,
     FNomExcepciones_DevEx,//by mP
     FNomExcepMontos_DevEx,//by mP
     FNomExcepGlobales_DevEx,//by mP
     FIMSSDatosTot_DevEx,
     FIMSSDatosMensual_DevEx,
     FIMSSDatosBimestral_DevEx,
     FIMSSHisMensual_DevEx,
     FIMSSHisBimestral_DevEx,
     FReportes_DevEx,
     FQueryGral_DevEx,
     FCatPuestos_DevEx,
     FCatClasifi_DevEx,
     FCatOtrasPer_DevEx,
     FCatTurnos_DevEx,
     FCatHorarios_DevEx,
     FCatPrestaciones_DevEx,
     FCatContratos_DevEx,
     FCatConceptos_DevEx,
     FNomParams_DevEx,
     FCatPeriodos_DevEx,//by mP
     FCatFolios_DevEx,//by mP
     FCatRPatron_DevEx,
     FCatFestGrales_DevEx,
     FCatFestTurno_DevEx,
     FCatCondiciones_DevEx,
     FCatEventos_DevEx,
     FCatCursos_DevEx,
     FCatMaestros_DevEx,
     FCatMatrizCurso_DevEx,
     FCatMatrizPuesto_DevEx,
     FCatCalendario_DevEx,
     FSistGlobales_DevEx,
//**     FDiccion,
     FCatReglas_DevEx,   //DevEx (icm)
     FCatInvitadores_DevEx,      //DevEx (icm)
     FMonedas_DevEx,
     FSalMin_DevEx,
     FLeyIMSS_DevEx,
     FNumerica_DevEx,
     FGradoRiesgo_DevEx,
     FTipoCambio_DevEx,
     FSistEmpresas_DevEx,
     FSistGrupos_DevEx,
     FSistUsuarios_DevEx,
     FSistPoll_DevEx,
     FSistBitacora_DevEx,
     FSistProcesos_DevEx,
     FImpresoras_DevEx,
     FSistNivel0_DevEx,
     FTablas_DevEx,              //DevEx (icm)
     FCatTools_DevEx,            //DevEx (icm)
     FHisTools_DevEx,
     FConteo_DevEx,
     FOrganigrama_DevEx,
     FSesiones_DevEx,
     FCatAccReglas_DevEx,
     FClasifTemporal_DevEx,
     FCatAulas_DevEx,
     FReservaAula_DevEx,
     FPrerequisitosCurso_DevEx,
     FCatCertificaciones_DevEx,
     FKardexCertificaciones_DevEx,
     FCatTiposPoliza_DevEx,//by mP
{$ifdef QUINCENALES}
     FCatTPeriodos_DevEx,//by mP
{$endif}
     FNomPolizas_DevEx,//by mP
     FPlanVacacion_DevEx,
     FCatPerfilesPuesto_DevEx,
     FCatSeccionesPerfil_DevEx,
     FCatCamposPerfil_DevEx,
     FSimulaMontos_DevEx,
     FSistBitacoraSistema_DevEx,
     FSistBitacoraReportes_DevEx,
{$IFNDEF DOS_CAPAS}
     FCatValuaciones_DevEx,
     FCatValPlantillas_DevEx,
     FCatValFactores_DevEx,
     FCatValSubfactores_DevEx,
{$ENDIF}
     FCatProvCap_DevEx,
     FCatRSocial_DevEx,
     FEmpDatosUsuario_DevEx,
     FCatRoles_DevEx,
     FMatrizCertificaPuesto_DevEx,
     FMatrizPuestoCertifica_DevEx,
     FHisCertificacionesProg_DevEx,
     FFonacotDatosEmpleado_DevEx,
     FFonacotDatosTot_DevEx,
     FHisCreInfonavit_DevEx,
     FIMSSHisConciliacion_DevEx,
     FCasDiarias_DevEx,
     FSistlstDispositivos_DevEx,
     FReglasPrestamos_DevEx,
     FSimulaMontosGlobal_DevEx,
     FMunicipios_DevEx,     //DevEx (icm)
     FCatEstablecimientos_DevEx,
     FEmpDocumento_DevEx,
{$ifndef DOS_CAPAS}
     FCosteoTransferencias_DevEx,
     FCosteoGrupos_DevEx,//by mP
     FCosteoCriterios_DevEx,//by mP
     FCosteoCriteriosPorConcepto_DevEx,
     FBitacoraRegBio_DevEx, // SYNERGY
     FSistListaGrupos_DevEx, // SYNERGY
     FSistDispPorGrupo_DevEx, // SYNERGY
     //FMensajes, // SYNERGY   //No esta la funcionalidad disponible en el arbol de tress
     FMensajesPorTerminal_DevEx, // SYNERGY
     FTerminales_DevEx, // SYNERGY
{$endif}
     FCatTiposPension_DevEx,//by mP
     FHisPensionesAlimenticias_DevEx,
     FCatSegurosGastosMedicos_DevEx,
     FHisSeguroGastosMedicos_DevEx,
     FCatCompetencias_DevEx,
     FCatPerfiles_DevEx,
     FCatMatrizPerfilesCompetencias_devEx,
     FHisPlanCapacitacion_DevEx,
     FHisEvaluaCompeten_DevEx,
     FMatrizHabilidades_DevEx,
     FMatrizCursos_DevEx,
     FCatNacCompetencias_DevEx,
     FCatTablasAmortizacion_DevEx,
     FBancos_DevEx,
     FTabOfiSatTipoContrato_DevEx,
     FTabOfiSatTipoJornada_DevEx,
     FTabOfiSatRiesgoPuesto_DevEx,
     FCatTipoSAT_DevEx,
     FSistBaseDatos_DevEx,
     FUma_DevEx,
     {$ifndef INTERFAZ_TRESS}
     FInicioTablero,
     FSistTablerosRoles_DevEx,
     {$endif}
     FHisAcumulados_RS_DevEx,
     FSistSolicitudReportes_DevEx, //US 14257
     FSistCalendarioReportes_DevEx,
     FEnviosProgramados_DevEx,
     FSistSolicitudEnvios_DevEx,
     FEmpGafeteBiometrico_DevEx,
{$ENDIF}
     ZAccesosMgr,
     ZAccesosTress;

function Consulta( const iDerechos: Integer; const Forma: TBaseConsultaClass ): TPropConsulta;
begin
     with Result do
     begin
          IndexDerechos := iDerechos;
          ClaseForma := Forma;
     end;
end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;
begin
{$IFDEF TRESS_DELPHIXE5_UP}
      case Forma of
       efcEmpIdentifica          : Result := Consulta( D_EMP_DATOS_IDENTIFICA, TEmpIdentifica_DevEx );
       efcEmpContratacion        : Result := Consulta( D_EMP_DATOS_CONTRATA, TEmpContratacion_DevEx );
       efcEmpArea                : Result := Consulta( D_EMP_DATOS_AREA, TEmpArea_DevEx );
       efcEmpPercepcion          : Result := Consulta( D_EMP_DATOS_PERCEP, TEmpPercepcion_DevEx );
       efcEmpOtros               : Result := Consulta( D_EMP_DATOS_OTROS, TEmpOtros_DevEx );
       efcEmpAdicionales         : Result := Consulta( D_EMP_DATOS_ADICION, TEmpAdicionales_DevEx );
       efcEmpUsuario             : Result := Consulta( D_EMP_DATOS_USUARIO, TEmpDatosUsuario_DevEx );
       efcEmpPersonal            : Result := Consulta( D_EMP_CURR_PERSONALES, TEmpPersonal );  //DevEx, cambio llamado a edicion
       efcEmpFoto                : Result := Consulta( D_EMP_CURR_FOTO, TEmpFoto_DevEx );
       efcEmpDocumento           : Result := Consulta( D_EMP_CURR_DOCUMENTO, TEmpDocumento_DevEx ); // mp (JB) Anexar al expediente del trabajador documentos CR1880 T1107
       efcEmpParientes           : Result := Consulta( D_EMP_CURR_PARIENTES, TEmpParientes_DevEx ); //mp
       efcEmpExperiencia         : Result := Consulta( D_EMP_CURR_EXPERIENCIA, TEmpExperiencia );
       efcEmpAntesCur            : Result := Consulta( D_EMP_CURR_CURSOS, TEmpAntesCur_DevEx ); //mp
       efcEmpAntesPto            : Result := Consulta( D_EMP_CURR_PUESTOS, TEmpAntesPto_Devex );//mP
       efcHisKardex              : Result := Consulta( D_EMP_EXP_KARDEX, THisKardex_DevEx ); // DevEx (by am)
       efcHisVacacion            : Result := Consulta( D_EMP_EXP_VACA, THisVacacion_DevEx );
       efcHisIncapaci            : Result := Consulta( D_EMP_EXP_INCA, THisIncapaci_DevEx ); //DevEx (by am)
       efcHisPermisos            : Result := Consulta( D_EMP_EXP_PERM, THisPermisos_DevEx ); //DevEx (by am)
       efcHisCursos              : Result := Consulta( D_EMP_EXP_CURSOS_TOMADOS, THisCursos_DevEx ); //MP
       efcHisCursosProg          : Result := Consulta( D_EMP_EXP_CURSOS_PROGRAMADOS, THisCursosProg_DevEx );//MP
       efcHisPagosIMSS           : Result := Consulta( D_EMP_EXP_PAGOS_IMSS, THisPagosIMSS_DevEx ); //DevEx (by am)
       efcHisCreInfonavit        : Result := Consulta( D_EMP_EXP_INFONAVIT, THisCreInfonavit_DevEx ); //DevEx (by am)
       efcHisSegurosGastosMedicos: Result := Consulta( D_EMP_SEGUROS_GASTOS_MEDICOS, THisSegurosGastosMedicos_DevEx );
       efcHisPlanCapacitacion    : Result := Consulta( D_EMP_PLANCAPACITA, THisPlanCapacita_DevEx );//mp
       efcHisEvaluaCompeten      : Result := Consulta( D_EMP_HIS_EVA_COMPETEN , THisEvaluaCompeten_DevEx );//mp
       efcHisAhorros             : Result := Consulta( D_EMP_NOM_AHORROS, THisAhorros_DevEx ); //DevEx (by am)
       efcHisPrestamos           : Result := Consulta( D_EMP_NOM_PRESTAMOS, THisPrestamos_DevEx ); //DevEx (by am)
       efcHisPensiones           : Result := Consulta( D_EMP_NOM_PENSIONES, THisPensionesAlimenticias_DevEx ); //DevEx (by am)
       efcHisAcumulados          : Result := Consulta( D_EMP_NOM_ACUMULA, THisAcumulados_DevEx ); //DevEx (by am)
       efcHisNominas             : Result := Consulta( D_EMP_NOM_HISTORIAL, THisNominas_DevEx );
       efcSimulaMontos           : Result := Consulta( D_EMP_SIM_FINIQUITOS_EMPLEADO, TSimulaMontos_DevEx );
       efcCafDiarias             : Result := Consulta( D_EMP_CAFE_COMIDAS_DIA, TCafDiarias_DevEx ); //mp
       efcCafPeriodo             : Result := Consulta( D_EMP_CAFE_COMIDAS_PERIODO, TCafPeriodo_DevEx );//mp
       efcCafInvita              : Result := Consulta( D_EMP_CAFE_INVITACIONES, TCafInvita_DevEx );//mp
       //efcRegEmpleado            : Result := Consulta( D_EMP_REGISTRO, TRegEmpleado );
       //efcProcEmpleado           : Result := Consulta( D_EMP_PROCESOS, TProcEmpleado );
       efcAsisTarjeta            : Result := Consulta( D_ASIS_DATOS_TARJETA_DIARIA, TAsisTarjeta_DevEx );
       efcAsisPreNomina          : Result := Consulta( D_ASIS_DATOS_PRENOMINA, TNomDatosAsist_DevEx );
       efcAsisCalendario         : Result := Consulta( D_ASIS_DATOS_CALENDARIO, TAsisCalendario_DevEx );
       efcAutorizaciones         : Result := Consulta( D_ASIS_DATOS_AUTORIZACIONES, TAutorizaciones_DevEx );//mp
       //efcRegAsistencia          : Result := Consulta( D_ASIS_REGISTRO, TRegAsistencia );
       //efcProcAsistencia         : Result := Consulta( D_ASIS_PROCESOS, TProcAsistencia );
       efcNomTotales             : Result := Consulta( D_NOM_DATOS_TOTALES, TNomTotales_DevEx );
       efcNomDiasHoras           : Result := Consulta( D_NOM_DATOS_DIAS_HORAS, TNomDiasHoras_DevEx );
       efcNomMontos              : Result := Consulta( D_NOM_DATOS_MONTOS, TNomMontos_DevEx );
       efcNomDatosClasifi        : Result := Consulta( D_NOM_DATOS_CLASIFI, TNomDatosClasifi_DevEx );
       efcNomDatosAsist          : Result := Consulta( D_NOM_DATOS_PRENOMINA, TNomDatosAsist_DevEx );
       efcNomExcepciones         : Result := Consulta( D_NOM_EXCEP_DIAS_HORAS, TNomExcepciones_DevEx );//by mP
       efcNomExcepMontos         : Result := Consulta( D_NOM_EXCEP_MONTOS, TNomExcepMontos_DevEx );//MP
       efcNomExcepGlobales       : Result := Consulta( D_NOM_EXCEP_GLOBALES, TNomExcepGlobales_DevEx );//by mP
       //efcRegNomina              : Result := Consulta( D_NOM_REGISTRO, TRegNomina );
       //efcProcNomina             : Result := Consulta( D_NOM_PROCESOS, TProcNomina );
       //efcAnualNomina            : Result := Consulta( D_NOM_ANUALES, TAnualNomina );
       efcIMSSDatosTot           : Result := Consulta( D_IMSS_DATOS_TOTALES, TIMSSDatosTot_DevEx ); //mp
       efcIMSSDatosMensual       : Result := Consulta( D_IMSS_DATOS_EMP_MEN, TIMSSDatosMensual_DevEx ); //mp
       efcIMSSDatosBimestral     : Result := Consulta( D_IMSS_DATOS_EMP_BIM, TTIMSSDatosBimestral_DevEx ); //mp
       efcIMSSConciliaInfonavit  : Result := Consulta( D_IMSS_CONCILIA_INFONAVIT, TIMSSHisConciliacion_DevEx );//mp
       efcIMSSHisMensual         : Result := Consulta( D_IMSS_HIST_EMP_MEN, TIMSSHisMensual_DevEx );//mp
       efcIMSSHisBimestral       : Result := Consulta( D_IMSS_HIST_EMP_BIM, TIMSSHisBimestral_DevEx );//mp
       //efcRegIMSS                : Result := Consulta( D_IMSS_REGISTRO, TRegIMSS );
       //efcProcIMSS               : Result := Consulta( D_IMSS_PROCESOS, TProcIMSS );
       efcReportes               : Result := Consulta( D_CONS_REPORTES, TReportes_DevEx );//mp
       efcQueryGral              : Result := Consulta( D_CONS_SQL, TQueryGral_DevEx ); //DevEx(by am)
       efcSistBitacora           : Result := Consulta( D_CONS_BITACORA, TSistBitacora_DevEx );
       efcCatPuestos             : Result := Consulta( D_CAT_CONT_PUESTOS, TCatPuestos_DevEx );//DevEx(by am)

       efcCatClasifi             : Result := Consulta( D_CAT_CONT_CLASIFI, TCatClasifi_DevEx ); //DevEx (by am)
       efcCatOtrasPer            : Result := Consulta( D_CAT_CONT_PER_FIJAS, TCatOtrasPer_DevEx ); //DevEx (by am)
       efcCatTurnos              : Result := Consulta( D_CAT_CONT_TURNOS, TCatTurnos_DevEx ); //DevEx (by am)
       efcCatHorarios            : Result := Consulta( D_CAT_CONT_HORARIOS, TCatHorarios_DevEx ); //DevEx (by am)
       efcCatPrestaciones        : Result := Consulta( D_CAT_CONT_PRESTA, TCatPrestaciones_DevEx ); //DevEx (by am)
       efcCatContratos           : Result := Consulta( D_CAT_CONT_CONTRATO, TCatContratos_DevEx ); //DevEx (by am)
       efcCatConceptos           : Result := Consulta( D_CAT_NOMINA_CONCEPTOS, TCatConceptos_DevEx ); //By Mp
       efcNomParams              : Result := Consulta( D_CAT_NOMINA_PARAMETROS, TNomParams_DevEx ); //By Mp

       efcCatPeriodos            : Result := Consulta( D_CAT_NOMINA_PERIODOS, TCatPeriodos_DevEx );//by mP
       efcCatFolios              : Result := Consulta( D_CAT_NOMINA_FOLIOS, TCatFolios_DevEx );//by mP
       efcCatRSocial             : Result := Consulta( D_CAT_GRALES_SOCIALES, TCatRSocial_DevEx );
       efcCatRPatron             : Result := Consulta( D_CAT_GRALES_PATRONALES, TCatRPatron_DevEx );
       efcCatFestGrales          : Result := Consulta( D_CAT_GRALES_FEST_GRAL, TCatFestGrales_DevEx );
       efcCatFestTurno           : Result := Consulta( D_CAT_GRALES_FEST_TURNO, TCatFestTurno_DevEx );
       efcCatCondiciones         : Result := Consulta( D_CAT_GRALES_CONDICIONES, TCatCondiciones_DevEx );
       efcCatEventos             : Result := Consulta( D_CAT_GRALES_EVENTOS, TCatEventos_DevEx );
       efcCatSegurosGastosMedicos: Result := Consulta( D_CAT_GRAL_SEG_GASTOS_MEDICOS, TCatSegurosGastosMedicos_DevEx );
       efcCatTabAmortizacion     : Result := Consulta( D_CAT_GRAL_TABLA_AMORTIZACION, TCatTablasAmortizacion_DevEx );
       efcTOTipoCursos           : Result := Consulta( D_CAT_CAPA_TIPO_CURSO, TTOTipoCursos_DevEx ); //DevEx (by am)
       efcCatCursos              : Result := Consulta( D_CAT_CAPA_CURSOS, TCatCursos_DevEx );//DevEx (by am)
       efcCatMaestros            : Result := Consulta( D_CAT_CAPA_MAESTROS, TCatMaestros_DevEx );//DevEx (by am)
       efcCatProvCap             : Result := Consulta( D_CAT_CAPA_PROVEEDORES, TCatProvCap_DevEx ); //DevEx (by am)
       efcCatMatrizCurso         : Result := Consulta( D_CAT_CAPA_MATRIZ_CURSO, TCatMatrizCurso_DevEx ); //DevEx (by am)
       efcCatMatrizPuesto        : Result := Consulta( D_CAT_CAPA_MATRIZ_PUESTO, TCatMatrizPuesto_DevEx ); //DevEx (by am)
       efcCatMatrizCertsPuesto   : Result := Consulta( D_CAT_CAPA_MATRIZ_CERT_PUESTO, TMatrizCertificaPuesto_DevEx ); //DevEx (by am)
       efcCatMatrizPuestosCert   : Result := Consulta( D_CAT_CAPA_MATRIZ_PUESTO_CERT, TMatrizPuestosCertificacion_DevEx ); //DevEx (by am)
       efcCatCalendario          : Result := Consulta( D_CAT_CAPA_CALENDARIO, TCatCalendario_DevEx ); //DevEx (by am)
       efcSistGlobales           : Result := Consulta( D_CAT_CONFI_GLOBALES, TSistGlobales_DevEx );
 //**      efcDiccion                : Result := Consulta( D_CAT_CONFI_DICCIONARIO, TDiccion );
       efcCatReglas              : Result := Consulta( D_CAT_CAFE_REGLAS, TCatReglas_DevEx );    //(icm)
       efcCatInvitadores         : Result := Consulta( D_CAT_CAFE_INVITADORES, TCatInvitadores_DevEx );           //(icm)
       efcTOEstadoCivil          : Result := Consulta( D_TAB_PER_EDOCIVIL, TTOEstadoCivil_DevEx );    //(icm)
       efcTOHabitacion           : Result := Consulta( D_TAB_PER_VIVE_EN, TTOHabitacion_DevEx );      //(icm)
       efcTOViveCon              : Result := Consulta( D_TAB_PER_VIVE_CON, TTOViveCon_DevEx );        //(icm)
       efcTOEstudios             : Result := Consulta( D_TAB_PER_ESTUDIOS, TTOEstudios_DevEx );       //(icm)
       efcTOTransporte           : Result := Consulta( D_TAB_PER_TRANSPORTE, TTOTransporte_DevEx );   //(icm)
       efcTOEstado               : Result := Consulta( D_TAB_PER_ESTADOS, TTOEstado_DevEx );     //(icm)
       efcTOMunicipios           : Result := Consulta( D_TAB_PER_MUNICIPIOS, TMunicipios_DevEx ); //(icm)
       efcTONivel1               : Result := Consulta( D_TAB_AREAS_NIVEL1, TTONivel1_DevEx );
       efcTONivel2               : Result := Consulta( D_TAB_AREAS_NIVEL2, TTONivel2_DevEx );
       efcTONivel3               : Result := Consulta( D_TAB_AREAS_NIVEL3, TTONivel3_DevEx );
       efcTONivel4               : Result := Consulta( D_TAB_AREAS_NIVEL4, TTONivel4_DevEx );
       efcTONivel5               : Result := Consulta( D_TAB_AREAS_NIVEL5, TTONivel5_DevEx );
       efcTONivel6               : Result := Consulta( D_TAB_AREAS_NIVEL6, TTONivel6_DevEx );
       efcTONivel7               : Result := Consulta( D_TAB_AREAS_NIVEL7, TTONivel7_DevEx );
       efcTONivel8               : Result := Consulta( D_TAB_AREAS_NIVEL8, TTONivel8_DevEx );
       efcTONivel9               : Result := Consulta( D_TAB_AREAS_NIVEL9, TTONivel9_DevEx );
       efcTOExtra1               : Result := Consulta( D_TAB_ADICION_TABLA1, TTOExtra1_DevEx );
       efcTOExtra2               : Result := Consulta( D_TAB_ADICION_TABLA2, TTOExtra2_DevEx );
       efcTOExtra3               : Result := Consulta( D_TAB_ADICION_TABLA3, TTOExtra3_DevEx );
       efcTOExtra4               : Result := Consulta( D_TAB_ADICION_TABLA4, TTOExtra4_DevEx );
       efcTOExtra5               : Result := Consulta( D_TAB_ADICION_TABLA5, TTOExtra5_DevEx );
       efcTOExtra6               : Result := Consulta( D_TAB_ADICION_TABLA6, TTOExtra6_DevEx );
       efcTOExtra7               : Result := Consulta( D_TAB_ADICION_TABLA7, TTOExtra7_DevEx );
       efcTOExtra8               : Result := Consulta( D_TAB_ADICION_TABLA8, TTOExtra8_DevEx );
       efcTOExtra9               : Result := Consulta( D_TAB_ADICION_TABLA9, TTOExtra9_DevEx );
       efcTOExtra10              : Result := Consulta( D_TAB_ADICION_TABLA10, TTOExtra10_DevEx );
       efcTOExtra11              : Result := Consulta( D_TAB_ADICION_TABLA11, TTOExtra11_DevEx );
       efcTOExtra12              : Result := Consulta( D_TAB_ADICION_TABLA12, TTOExtra12_DevEx );
       efcTOExtra13              : Result := Consulta( D_TAB_ADICION_TABLA13, TTOExtra13_DevEx );
       efcTOExtra14              : Result := Consulta( D_TAB_ADICION_TABLA14, TTOExtra14_DevEx );

       efcTOMovKardex            : Result := Consulta( D_TAB_HIST_TIPO_KARDEX, TTOMovKardex_DevEx );
       efcTOMotivoBaja           : Result := Consulta( D_TAB_HIST_MOT_BAJA, TTOMotivoBaja_DevEx );
       efcTOIncidencias          : Result := Consulta( D_TAB_HIST_INCIDEN, TTOIncidencias_DevEx );
       efcTOMotAuto              : Result := Consulta( D_TAB_HIST_AUT_EXTRAS, TTOMotAuto_DevEx );
       efcTOTAhorro              : Result := Consulta( D_TAB_NOM_TIPO_AHORRO, TTOTAhorro_DevEx ); //By mp
       efcTOTPresta              : Result := Consulta( D_TAB_NOM_TIPO_PRESTA, TTOTPresta_DevEx ); //By mp
       efcTabTCompetencias       : Result := Consulta( D_TAB_TCOMPETENCIAS , TTOTCompetencias_DevEx);
       efcTabTPerfiles           : Result := Consulta( D_TAB_TPERFILES , TTOTPerfiles_DevEx );
       efcMonedas                : Result := Consulta( D_TAB_NOM_MONEDAS, TMonedas_DevEx ); //By mp
       efcSistSalMin             : Result := Consulta( D_TAB_OFI_SAL_MINIMOS, TSistSalMin_DevEx );
       efcSistLeyIMSS            : Result := Consulta( D_TAB_OFI_CUOTAS_IMSS, TSistLeyIMSS_DevEX );
       efcSistNumerica           : Result := Consulta( D_TAB_OFI_ISPT_NUMERICAS, TSistNumerica_DevEx );
       efcSistRiesgo             : Result := Consulta( D_TAB_OFI_GRADOS_RIESGO, TRiesgo_DevEx );
       efcTipoCambio             : Result := Consulta( D_TAB_OFI_TIPOS_CAMBIO, TTipoCambio_DevEx );
       efcSistEmpresas           : Result := Consulta( D_SIST_DATOS_EMPRESAS, TSistEmpresas_DevEx );
       efcSistGrupos             : Result := Consulta( D_SIST_DATOS_GRUPOS, TSistGrupos_DevEx );//by MP
       efcSistRoles              : Result := Consulta( D_CAT_ROLES, TCatRoles_DevEx );//by MP
       efcSistUsuarios           : Result := Consulta( D_SIST_DATOS_USUARIOS, TSistUsuarios_DevEx );//by MP
       efcSistPoll               : Result := Consulta( D_SIST_DATOS_POLL_PENDIENTES, TSistPoll_DevEx );//by MP
       efcImpresoras             : Result := Consulta( D_SIST_DATOS_IMPRESORAS, TImpresoras_DevEx ); //DevEx (by am)
       //efcRegSist                : Result := Consulta( D_SIST_REGISTRO, TRegSist );
       //efcProcSist               : Result := Consulta( D_SIST_PROCESOS, TProcSist );
       efcSistProcesos           : Result := Consulta( D_CONS_BITACORA, TSistProcesos_DevEx );
       efcSistNivel0             : Result := Consulta( D_SIST_NIVEL0, TNivel0_DevEx ); //DevEx (by am)
       efcCatTools               : Result := Consulta( D_CAT_HERR_TOOLS, TCatTools_DevEx );   //(icm)
       efcTOTallas               : Result := Consulta( D_TAB_HERR_TALLAS, TTOTallas_DevEx );  // (icm)
       efcTOMotTool              : Result := Consulta( D_TAB_HERR_MOTTOOL, TTOMotTool_DevEx );        //(icm)
       efcHisTools               : Result := Consulta( D_EMP_EXP_TOOLS, THisTools_DevEx ); //DevEx (by am)
       efcConteo                 : Result := Consulta( D_CONS_CONTEO_PERSONAL, TConteo_DevEx ); //DevEx (by am)
       efcTTOClasifiCurso        : Result := Consulta( D_CAT_CLASIFI_CURSO, TTOClasifiCurso_DevEx ); //DevEx (by am)
       efcOrganigrama            : Result := Consulta( D_CONS_ORGANIGRAMA, TOrganigrama_DevEx ); //DevEx (by am)
       efcCatSesiones            : Result := Consulta( D_CAT_CAPA_SESIONES, TSesiones_DevEx );//by MP
       //efcCatSesiones            : Result := Consulta( D_CAT_CAPA_SESIONES, TCatSesiones );
       efcCatAccReglas           : Result := Consulta( D_CAT_ACCESOS_REGLAS, TCatAccReglas_DevEx );
       efcAsisClasifiTemp        : Result := Consulta( D_ASIS_DATOS_CLASIFI_TEMP, TClasifTemporal_DevEx );//mp
       efcTTOColonia             : Result := Consulta( D_TAB_COLONIA, TTOColonia_DevEx );         //(icm)
       efcCatAulas               : Result := Consulta( D_CAT_CAPA_AULAS, TCatAulas_DevEx ); //ByMP
       efcReservas               : Result := Consulta( D_CAPA_RESERVAS, TReservaAula_DevEx );//by MP
       efcMtzHabilidades         : Result := Consulta( D_CAP_MATRIZ_HBLDS, TMatrizHabilidades_DevEx );//by MP
       efcMtzCursos              : Result := Consulta( D_CAP_MATRIZ_CURSOS, TMatrizCursos_DevEx );
       efcPrerequisitosCurso     : Result := Consulta( D_CAT_PREREQUISITOS_CURSO, TPrerequisitosCurso_DevEx ); //ByMP
       efcCertificaciones        : Result := Consulta( D_CAT_CERTIFICACIONES, TCatCertificaciones_DevEx );//ByMP
       efcKarCertificaciones     : Result := Consulta( D_EMP_EXP_CERTIFICACIONES, TKardexCertificaciones_DevEx ); //DevEx (by am)
       efcKarCertificProgramadas : Result := Consulta( D_EMP_EXP_CERTIFIC_PROG, THisCertificacionesProg_DevEx );//mp
       efcPolizas  	             : Result := Consulta( D_NOM_DATOS_POLIZAS, TNomPolizas_DevEx );//MP
       efcTiposPoliza            : Result := Consulta( D_CAT_NOMINA_POLIZAS, TCatTiposPoliza_DevEx );//by mP
       efcTiposPension           : Result := Consulta( D_CAT_NOMINA_TPENSION, TCatTiposPension_DevEx );//by mP
{$ifdef QUINCENALES}
       efcCatTPeriodos           : Result := Consulta( D_CAT_TPERIODOS, TCatTPeriodos_DevEx );//by MP
{$endif}
       efcPlanVacacion           : Result := Consulta( D_CONS_PLANVACACION, TPlanVacacion_DevEx ); //mp
       efcCatPerfPuestos         : Result := Consulta( D_CAT_DESC_PERF_PUESTOS, TCatPerfilesPuesto_DevEx );
       efcCatSeccPerf            : Result := Consulta( D_CAT_DESC_SECC_PERF, TCatSeccionesPerfil_DevEx );
       efcCatCamposPerf          : Result := Consulta( D_CAT_DESC_CAMPOS_PERF, TCatCamposPerfil_DevEx );
{$IFNDEF DOS_CAPAS}
       efcCatValuaciones         : Result := Consulta( D_CAT_VALUACIONES, TCatValuaciones_DevEx );
       efcCatValPlantillas       : Result := Consulta( D_CAT_VAL_PLANTILLAS, TCatValPlantillas_DevEx );
       efcCatValFactores         : Result := Consulta( D_CAT_VAL_FACTORES, TCatValFactores_DevEx );
       efcCatValSubFactores      : Result := Consulta( D_CAT_VAL_SUBFACTORES, TCatValSubfactores_DevEx );
       efcCatCompetencias        : Result := Consulta( D_CAT_COMPETENCIAS , TCatCompetencias_DevEx ); //mp
       efcCatPerfiles            : Result := Consulta( D_CAT_CPERFILES , TCatPerfiles_DevEx );//mp
       efcCatMatPerfilComps      : Result := Consulta( D_CAT_MAT_PERFILES_COMP , TCatMatrizPerfilComps_DevEx ); //mp
{$ENDIF}
       efcBitacoraSistema       : Result := Consulta( D_SIST_BITACORA, TSistBitacoraSistema_DevEx );//DevEx(by am)
       efcNomFonacotTotales     : Result := Consulta( D_PAGOS_FONACOT_TOT, TFonacotDatosTot_DevEx );
       efcNomFonacotEmpleado    : Result := Consulta( D_PAGOS_FONACOT_TOT_EMPL, TFonacotDatosEmpleado_DevEx );
{$ifdef ACS}
       efcTONivel10              : Result := Consulta( D_TAB_AREAS_NIVEL10, TTONivel10_DevEx );
       efcTONivel11              : Result := Consulta( D_TAB_AREAS_NIVEL11, TTONivel11_DevEx );
       efcTONivel12              : Result := Consulta( D_TAB_AREAS_NIVEL12, TTONivel12_DevEx );
{$endif}
       efcReglasPrestamos        : Result := Consulta( D_TAB_NOM_REGLAS_PRESTA, TReglasPrestamos_DevEx ); //By mp

       efcCasDiarias            : Result := Consulta( D_EMP_CASETA_DIARIAS, TCasDiarias_DevEx ); //mp
       efcSistDispositivos      : Result := Consulta( D_SIST_LST_DISPOSITIVOS, TSistLstDispositivos_DevEx ); //DevEx: (By am)
       efcSimulaMontoGlobal     : Result := Consulta( D_EMP_SIM_FINIQUITOS_TOTALES, TSimulaMontosGlobal_DevEx );
       efcMotChecaManual        : Result :=Consulta( D_TAB_HIST_CHECA_MANUAL, TTOMotCheca_DevEx );
       efcTOcupaNac              : Result :=Consulta( D_TAB_OFI_STPS_CAT_NAC_OCUPA , TTOOcupaNac_DevEx );
       efcTAreaTemCur            : Result :=Consulta( D_TAB_OFI_STPS_ARE_TEM_CUR , TTOAreaTematicaCursos_DevEx );
       efcEstablecimientos       : Result :=Consulta( D_CAT_ESTABLECIMIENTOS  , TEstablecimientos_DevEx ); //ByMP
{$ifndef DOS_CAPAS}
       efcCosteoTransferencias   : Result :=Consulta( D_COSTEO_TRANSFERENCIAS  , TCosteoTransferencias_DevEx );
       efcCosteoMotTransfer      : Result :=Consulta( D_COSTEO_MOTIVOS_TRANSFER  , TTOMotivoTransfer_DevEx );
       efcCatCosteoGrupos               : Result :=Consulta( D_COSTEO_GRUPOS  , TCosteoGrupos_DevEx ); {JB}//by mP
       efcCatCosteoCriterios            : Result :=Consulta( D_COSTEO_CRITERIOS , TCosteoCriterios_DevEx  ); {JB}//by mP
       efcCatCosteoCriteriosPorConcepto : Result :=Consulta( D_COSTEO_CRITERIOS_CONCEPTO, TCosteoCriteriosPorConcepto_DevEx ); {JB}
       efcBitacoraBio            : Result := Consulta( D_SIST_BITACORA_BIO, TBitacoraRegBio_DevEx ); // SYNERGY //DevEx (by am)
       efcSistListaGrupos        : Result := Consulta( D_SIST_LISTA_GRUPOS, TSistListaGrupos_DevEx ); // SYNERGY //DevEx: (By am)
       efcSistTermPorGrupos      : Result := Consulta( D_SIST_TERM_GRUPO, TSistDispPorGrupo_DevEx ); // SYNERGY
       //efcSistMensajes           : Result := Consulta( D_SIST_MENSAJES, TMensajes ); // SYNERGY
       efcSistMensajesPorTerminal: Result := Consulta( D_SIST_MENSAJES_TERM, TMensajesPorTerminal_DevEx ); // SYNERGY  //DevEx (by am)
       efcSistTerminales         : Result := Consulta( D_SIST_TERMINALES, TTerminales_DevEx ); // SYNERGY  //DevEx (by am)
       efcCatNacCompetencias     : Result := Consulta( D_CAT_TAB_NAC_COMP, TCatNacCompetencias_DevEx );
       efcSistBaseDatos          : Result := Consulta( D_SIST_BASE_DATOS, TSistBaseDatos_DevEx);
{$endif}
       efcBancos     : Result := Consulta( D_TAB_BANCOS, TBancos_DevEx );
       efcUma      : Result := Consulta( D_TAB_UMA, TUma_DevEx );
       efcTTipoContratoSAT     :Result := Consulta(D_TAB_OFI_SAT_TIPO_CONTRATO,TTabOfiSatTipoContrato_DevEx);
       efcTTipoJornadaSAT     :Result := Consulta(D_TAB_OFI_SAT_TIPO_JORNADA,TTabOfiSatTipoJornada_DevEx);
       efcTRiesgoPuestoSAT    :Result := Consulta(D_TAB_OFI_SAT_RIESGO_PUESTO,TTabOfiSatRiesgoPuesto_DevEx);
       efcTTiposConceptosSAT    :Result := Consulta(D_TAB_OFI_SAT_TIPOS_CONCEPTO,TCatTipoSAT_DevEx);
       {$ifndef INTERFAZ_TRESS}
       efcInicio                : Result := Consulta(D_INICIO, TInicioTablero);
       efcTablerosPorRoles      : Result := Consulta(D_TABLEROS_ROLES, TSistTablerosRoles_DevEx);
       {$endif}
       efcHisAcumulados_RS      : Result := Consulta( D_EMP_NOM_ACUMULA, THisAcumulados_RS_DevEx ); //DevEx (by am)
       efcSistSolicitudCorreo   : Result := Consulta( D_SIST_SOLICITUD_CORREOS, TSistLstSolicitudReportes_DevEx ); //14322
       efcSistCalendarioReportes: Result := Consulta( D_SIST_CALENDARIO_REPORTES, TSistCalendarioReportes_DevEx); //14915
       efcSistBitacoraReportes:   Result := Consulta( D_SIST_BITACORA_REPORTES, TSistBitacoraReportes_DevEx);
       efcSolicitudEnviosProgramados: Result := Consulta( D_SIST_SOLICITUD_ENVIO, TSistSolicitudEnvios_DevEx);
       efcEnviosProgramados:   Result := Consulta( D_CONS_ENVIOS_PROGRAMADOS, TEnviosProgramados_DevEx);
       efcEmpDatosGafeteBiometrico:   Result := Consulta( D_EMP_DATOS_GAFETE_BIOMETRICO, TEmpGafeteBiometrico_DevEx);
      else
         Result := Consulta( 0, nil );
      end;
{$ENDIF}
end;

end.
