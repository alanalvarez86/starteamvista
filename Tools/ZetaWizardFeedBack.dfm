inherited WizardFeedback: TWizardFeedback
  Left = 431
  Top = 211
  ActiveControl = Cancelar
  Caption = 'Wizard Terminado'
  ClientHeight = 271
  ClientWidth = 340
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Imagen: TImage [0]
    Left = 0
    Top = 0
    Width = 65
    Height = 239
    Align = alLeft
    Center = True
  end
  object InicioLBL: TLabel [1]
    Left = 96
    Top = 56
    Width = 28
    Height = 13
    Alignment = taRightJustify
    Caption = 'Inici�:'
  end
  object Inicio: TZetaTextBox [2]
    Left = 126
    Top = 54
    Width = 149
    Height = 17
    AutoSize = False
    Caption = 'Inicio'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object FinLBL: TLabel [3]
    Left = 83
    Top = 73
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Termin�:'
  end
  object Fin: TZetaTextBox [4]
    Left = 126
    Top = 71
    Width = 149
    Height = 17
    AutoSize = False
    Caption = 'Fin'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object Duracion: TZetaTextBox [5]
    Left = 126
    Top = 88
    Width = 149
    Height = 17
    AutoSize = False
    Caption = 'Duracion'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object DuracionLBL: TLabel [6]
    Left = 78
    Top = 90
    Width = 46
    Height = 13
    Alignment = taRightJustify
    Caption = 'Duraci�n:'
  end
  object MaximoLBL: TLabel [7]
    Left = 69
    Top = 107
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empleados:'
  end
  object Maximo: TZetaTextBox [8]
    Left = 126
    Top = 105
    Width = 149
    Height = 17
    AutoSize = False
    Caption = 'Maximo'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object Avance: TZetaTextBox [9]
    Left = 126
    Top = 139
    Width = 71
    Height = 17
    AutoSize = False
    Caption = 'Avance'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object AvanceLBL: TLabel [10]
    Left = 84
    Top = 141
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Avance:'
  end
  object EmpleadoLBL: TLabel [11]
    Left = 92
    Top = 124
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ultimo:'
  end
  object Empleado: TZetaTextBox [12]
    Left = 126
    Top = 122
    Width = 71
    Height = 17
    AutoSize = False
    Caption = 'Empleado'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object FolioLBL: TLabel [13]
    Left = 99
    Top = 5
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = 'Folio:'
  end
  object Folio: TZetaTextBox [14]
    Left = 126
    Top = 3
    Width = 71
    Height = 17
    AutoSize = False
    Caption = 'Folio'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object ProcesoLBL: TLabel [15]
    Left = 82
    Top = 22
    Width = 42
    Height = 13
    Caption = 'Proceso:'
  end
  object Proceso: TZetaTextBox [16]
    Left = 126
    Top = 20
    Width = 212
    Height = 17
    AutoSize = False
    Caption = 'Proceso'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object Status: TZetaTextBox [17]
    Left = 126
    Top = 37
    Width = 212
    Height = 17
    AutoSize = False
    Caption = 'Status'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object StatusLBL: TLabel [18]
    Left = 91
    Top = 40
    Width = 33
    Height = 13
    Caption = 'Status:'
  end
  object MensajesLBL: TLabel [19]
    Left = 76
    Top = 159
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Mensajes:'
  end
  inherited PanelBotones: TPanel
    Top = 239
    Width = 340
    Height = 32
    BevelOuter = bvNone
    inherited OK: TBitBtn
      Left = 237
      Top = 5
      Width = 102
      Hint = 'Aceptar y Continuar'
      Anchors = [akRight, akBottom]
      Cancel = True
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
    end
    inherited Cancelar: TBitBtn
      Left = 9
      Top = 5
      Width = 111
      Hint = 'Accesar Bit�cora Del Proceso'
      Anchors = [akLeft, akBottom]
      Cancel = False
      Caption = '&Ver Bit�cora'
      Default = True
      ModalResult = 6
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333FF3333333333333C0C333333333333F777F3333333333CC0F0C3
        333333333777377F33333333C30F0F0C333333337F737377F333333C00FFF0F0
        C33333F7773337377F333CC0FFFFFF0F0C3337773F33337377F3C30F0FFFFFF0
        F0C37F7373F33337377F00FFF0FFFFFF0F0C7733373F333373770FFFFF0FFFFF
        F0F073F33373F333373730FFFFF0FFFFFF03373F33373F333F73330FFFFF0FFF
        00333373F33373FF77333330FFFFF000333333373F333777333333330FFF0333
        3333333373FF7333333333333000333333333333377733333333333333333333
        3333333333333333333333333333333333333333333333333333}
      Kind = bkCustom
    end
  end
  object Mensajes: TMemo
    Left = 126
    Top = 156
    Width = 211
    Height = 82
    Color = clBtnFace
    ScrollBars = ssBoth
    TabOrder = 1
  end
end
