unit ZBaseGridEdicion;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, DBCtrls, Buttons, ExtCtrls, StdCtrls, Grids, DBGrids,
     {$ifndef VER130}Variants,{$endif}     
     ZBaseDlgModal,
     ZetaClientDataset,
     ZetaCommonLists, ZetaMessages,
     dbClient, ZetaDBGrid, ZetaSmartLists;

type
  TBaseGridEdicion = class(TZetaDlgModal)
    PanelIdentifica: TPanel;
    Splitter: TSplitter;
    ValorActivo1: TPanel;
    ValorActivo2: TPanel;
    AgregarBtn: TSpeedButton;
    BorrarBtn: TSpeedButton;
    ModificarBtn: TSpeedButton;
    BuscarBtn: TSpeedButton;
    ImprimirBtn: TSpeedButton;
    ImprimirFormaBtn: TSpeedButton;
    CortarBtn: TSpeedButton;
    CopiarBtn: TSpeedButton;
    PegarBtn: TSpeedButton;
    UndoBtn: TSpeedButton;
    DBNavigator: TDBNavigator;
    DataSource: TDataSource;
    ZetaDBGrid: TZetaDBGrid;
    PanelSuperior: TPanel;
    ExportarBtn: TZetaSpeedButton;
    textoValorActivo1: TLabel;
    textoValorActivo2: TLabel;
    procedure AgregarBtnClick(Sender: TObject);
    procedure BorrarBtnClick(Sender: TObject);
    procedure ModificarBtnClick(Sender: TObject);
    procedure BuscarBtnClick(Sender: TObject);
    procedure ImprimirBtnClick(Sender: TObject);
    procedure ImprimirFormaBtnClick(Sender: TObject);
    procedure CortarBtnClick(Sender: TObject);
    procedure CopiarBtnClick(Sender: TObject);
    procedure PegarBtnClick(Sender: TObject);
    procedure UndoBtnClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSourceStateChange(Sender: TObject);
    procedure ZetaDBGridColEnter(Sender: TObject);
    procedure ExportarBtnClick(Sender: TObject);
{    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState); }
  private
    { Private declarations }
    FTipoValorActivo1: TipoEstado;
    FTipoValorActivo2: TipoEstado;
    FPrimerColumna: Integer;
    FHuboCambios: Boolean;
    FHuboErrores : Boolean;
    FInicial: Boolean;
    FGeneral: Variant;
    FIndexDerechos: Integer;
    FPermiteAltas: Boolean;
    procedure AsignaValoresActivos;
    procedure InitValoresActivos;
    procedure InitColumnasActivas;
    procedure SetHuboCambios( const Value: Boolean );
    procedure ImprimirForma;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    procedure BorraCelda;
    procedure SetAccesos;
  protected
    { Protected declarations }
    property TipoValorActivo1: TipoEstado read FTipoValorActivo1 write FTipoValorActivo1;
    property TipoValorActivo2: TipoEstado read FTipoValorActivo2 write FTipoValorActivo2;
    property PrimerColumna: Integer read FPrimerColumna write FPrimerColumna;
    property HuboCambios: Boolean read FHuboCambios write SetHuboCambios;
    property IndexDerechos: Integer read FIndexDerechos write FIndexDerechos;
    property PermiteAltas: Boolean read FPermiteAltas write FPermiteAltas;
    function GridEnfocado: Boolean;
    function PosicionaSiguienteColumna: Integer; virtual;
    function Editing: Boolean; dynamic;
    function Inserting: Boolean;
    function SoloAltas: Boolean; dynamic;
    function ClientDataset: TZetaClientDataset;
    function DoConnect: Boolean;
    function ValoresGrid: Variant; virtual;
    function CheckDerechos(const iDerecho: Integer): Boolean;
    function PuedeAgregar: Boolean; dynamic;
    function PuedeBorrar: Boolean; dynamic;
    function PuedeModificar: Boolean; dynamic;
    function PuedeImprimir: Boolean; dynamic;
    procedure DoDisconnect;
    procedure EscribirCambios; dynamic;
    procedure CancelarCambios; dynamic;
    procedure Connect; virtual; abstract;
    procedure Disconnect; virtual;
    procedure Agregar; virtual;
    procedure Borrar; virtual;
    procedure Modificar; virtual;
    procedure Imprimir; dynamic;
    procedure Exportar;virtual;
    procedure Buscar; virtual;
    procedure HabilitaControles; dynamic;
    procedure SeleccionaPrimerColumna;
    procedure SeleccionaSiguienteRenglon;
    procedure SetCycleControl; dynamic;
    procedure KeyDown( var Key: Word; Shift: TShiftState ); override; { TWinControl }
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    property General: Variant read FGeneral write FGeneral;
  end;
  TBaseGridEdicionClass = class of TBaseGridEdicion;

var
  BaseGridEdicion: TBaseGridEdicion;

procedure ShowGridEdicion( var Forma; EdicionClass: TBaseGridEdicionClass; const Parametro: Variant );

implementation

uses DCliente,
     FBaseReportes,
     ZetaDialogo,
     ZCerrarEdicion,
     ZetaCommonClasses,
     ZAccesosMgr;

{$R *.DFM}

procedure ShowGridEdicion( var Forma; EdicionClass: TBaseGridEdicionClass; const Parametro: Variant );
begin
     try
        TBaseGridEdicion( Forma ) := EdicionClass.Create( Application ) as TBaseGridEdicion;
        with TBaseGridEdicion( Forma ) do
        begin
             General := Parametro;
             ShowModal;
        end;
     finally
            TBaseGridEdicion( Forma ).Free;
     end;
end;

{ ************ TBaseEdicionGrid *********** }

constructor TBaseGridEdicion.Create(AOwner: TComponent);
begin
     FTipoValorActivo1 := stNinguno;
     FTipoValorActivo2 := stNinguno;
     FPrimerColumna := 0;
     FIndexDerechos := 0;
     FPermiteAltas := TRUE;
     inherited Create( AOwner );
     InitValoresActivos;
     InitColumnasActivas;
end;

procedure TBaseGridEdicion.FormCreate(Sender: TObject);
begin
     inherited;
     // Para permitir a descendientes invocar este evento //
end;

procedure TBaseGridEdicion.FormShow(Sender: TObject);
begin
     inherited;
     FInicial := TRUE;
     FHuboErrores := FALSE;
     if DoConnect then
     begin
          SetAccesos;
          AsignaValoresActivos;
          SeleccionaPrimerColumna;
     end;
end;

procedure TBaseGridEdicion.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     DoDisconnect;
     Action := caHide;
end;

procedure TBaseGridEdicion.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     inherited;
     CanClose := True;
     if Editing then
     begin
          case ZCerrarEdicion.CierraEdicion of
               mrOk: EscribirCambios;
               mrIgnore: CancelarCambios;
          else
              CanClose := False;
          end;
     end;
end;

function TBaseGridEdicion.DoConnect: Boolean;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             try
                Connect;
                Result := True;
             except
                   on Error: Exception do
                   begin
                        zExcepcion( '� Error Al Conectar Forma !', '� Ventana No Pudo Ser Conectada A Sus Datos !', Error, 0 );
                        Result := False;
                   end;
             end;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

procedure TBaseGridEdicion.DoDisconnect;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Disconnect;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

procedure TBaseGridEdicion.Disconnect;
begin
     Datasource.Dataset := nil;
end;

procedure TBaseGridEdicion.EscribirCambios;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with ClientDataset do
        begin
             Enviar;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBaseGridEdicion.CancelarCambios;
begin
     with ClientDataset do
     begin
          if State in [ dsEdit, dsInsert ] then
             Cancel;
          CancelUpdates;
          DataSourceStateChange( Self );
     end;
     if SoloAltas then
        Close
     else
        SeleccionaPrimerColumna;
end;

{***DevEx (by am): Se dejo de usar el caption de los paneles para mostrar los valores activos.
Ahora se utilizara una label dentro del panel. ***}
procedure TBaseGridEdicion.AsignaValoresActivos;
begin
     with dmCliente do
     begin
          textoValorActivo1.Caption := GetValorActivoStr( TipoValorActivo1 );
          textoValorActivo2.Caption := GetValorActivoStr( TipoValorActivo2 )
     end;

end;

procedure TBaseGridEdicion.InitValoresActivos;
begin
     if ( TipoValorActivo1 = stNinguno ) and ( TipoValorActivo2 = stNinguno ) then
     begin
          PanelIdentifica.Visible := False;
          ValorActivo1.Visible := False;
          ValorActivo2.Visible := False;
          Splitter.Visible := False;
     end
     else
     begin
          if ( TipoValorActivo1 = stNinguno ) then
          begin
               ValorActivo1.Align := alNone;
               ValorActivo1.Visible := False;
               ValorActivo2.Align := alClient;
               Splitter.Visible := False;
          end;
          if ( TipoValorActivo2 = stNinguno ) then
          begin
               ValorActivo2.Align := alNone;
               ValorActivo2.Visible := False;
               ValorActivo1.Align := alClient;
               Splitter.Visible := False;
          end;
     end;
end;

procedure TBaseGridEdicion.InitColumnasActivas;
var
   i: Integer;
begin
     with ZetaDBGrid do
     begin
          for i := 0 to ( Columns.Count - 1 ) do
          begin
               if Columns[ i ].ReadOnly then
               begin
                    Columns[ i ].Color := clInfoBk;
                    Columns[ i ].Font.Color := clInfoText;
               end
               else
               begin
                    Columns[ i ].Color := clWindow;
                    Columns[ i ].Font.Color := clWindowText;
               end;
          end;
     end;
end;

procedure TBaseGridEdicion.OKClick(Sender: TObject);
begin
     inherited;
     EscribirCambios;
     with ClientDataSet do
     begin
          if ( HuboErrores ) then
          begin
               FHuboErrores := TRUE;
               DisableControls;
               try
                  PosicionaError;
               finally
                  EnableControls;
               end;
          end
          else
              Self.Close;
     end;
end;

procedure TBaseGridEdicion.CancelarClick(Sender: TObject);
begin
     if Editing then
        CancelarCambios
     else
        Close;
end;

function TBaseGridEdicion.ClientDataset: TZetaClientDataset;
begin
     Result := TZetaClientDataset( Datasource.Dataset );
end;

function TBaseGridEdicion.Editing: Boolean;
begin
     if ( Datasource.State <> dsInactive ) then
        Result := ( ClientDataSet.ChangeCount > 0 ) or ( DataSource.State in [ dsEdit, dsInsert ] )
     else
         Result := False;
end;

function TBaseGridEdicion.Inserting: Boolean;
begin
     if ( Datasource.State <> dsInactive ) then
        Result :=( SoloAltas ) and ( ( ClientDataSet.ChangeCount > 0 ) or ( DataSource.State in [ dsEdit, dsInsert ] ) )
     else
         Result := False;
end;

function TBaseGridEdicion.SoloAltas: Boolean;
begin
     Result := False;
     if ( VarType( FGeneral ) = varBoolean ) then
        Result := FGeneral;
end;

procedure TBaseGridEdicion.Agregar;
begin
     if PuedeAgregar then
     begin
          ClientDataset.Append;
          SeleccionaPrimerColumna;
     end;
end;

procedure TBaseGridEdicion.Borrar;
begin
     if PuedeBorrar then
        with ClientDataset do
             if not IsEmpty then
             begin
                  Delete;
                  DataSourceStateChange( Self );
                  SeleccionaPrimerColumna;
             end;
end;

procedure TBaseGridEdicion.Modificar;
begin
     if PuedeModificar then
        ClientDataset.Edit;
end;

function TBaseGridEdicion.ValoresGrid : Variant;
begin
     Result := VarArrayOf( [ textoValorActivo1.Caption, textoValorActivo2.Caption, TipoValorActivo1, TipoValorActivo2 ] );
end;

procedure TBaseGridEdicion.Imprimir;
var
   Valor : Variant;
begin
     if PuedeImprimir then
        if zConfirm( 'Imprimir...', '� Desea Imprimir El Grid De La Pantalla ' + Caption + ' ?', 0, mbYes ) then
        begin
             Valor := ValoresGrid;
             FBaseReportes.ImprimirGrid( ZetaDBGrid, ZetaDBGrid.DataSource.DataSet, Caption, 'IM',
                                         Valor[0],Valor[1],Valor[2],Valor[3]);
        end;
end;

procedure TBaseGridEdicion.Exportar;
var
   Valor : Variant;
begin
     if PuedeImprimir then
        if zConfirm( 'Exportar...', '� Desea exportar a Excel la informaci�n de ' + Caption + ' ?', 0, mbYes ) then
        begin
             Valor := ValoresGrid;
             FBaseReportes.ExportarGrid( ZetaDBGrid,
                                         ZetaDBGrid.DataSource.DataSet,
                                         Caption,
                                         'IM',
                                         Valor[0],
                                         Valor[1],
                                         Valor[2],
                                         Valor[3] );
        end;
end;

procedure TBaseGridEdicion.ImprimirForma;
begin
     if zConfirm( 'Imprimir...', '� Desea Imprimir La Pantalla ' + Caption + ' ?', 0, mbYes ) then
        Print;
end;

procedure TBaseGridEdicion.Buscar;
begin
     { Para Habilitar Busqueda sobre columnas al presionar CTRL + F }
end;

procedure TBaseGridEdicion.SeleccionaPrimerColumna;
begin
     Self.ActiveControl := ZetaDBGrid;
     ZetaDBGrid.SelectedField := ZetaDBGrid.Columns[ FPrimerColumna ].Field;   // Posicionar en la primer columna
end;

procedure TBaseGridEdicion.SeleccionaSiguienteRenglon;
begin
     with ClientDataSet do
     begin
          Next;
          if EOF then
             if FPermiteAltas then
                Self.Agregar
             else
                SetCycleControl;
     end;
end;

procedure TBaseGridEdicion.SetCycleControl;
begin
     if OK.Enabled then
        OK.SetFocus
     else
        Cancelar.SetFocus;
end;

procedure TBaseGridEdicion.BorraCelda;
begin
     if PuedeModificar then
        with ZetaDBGrid do
        begin
             if ( not Columns[ SelectedIndex ].ReadOnly ) then
             begin
                  with ClientDataSet do
                       if not ( State in [ dsEdit, dsInsert ] ) then
                          Edit;
                  with SelectedField do
                       case DataType of
                            ftSmallint, ftInteger, ftWord : AsInteger := 0;
                            ftString : AsString := VACIO;
                            ftFloat, ftCurrency, ftBCD: AsFloat := 0;
                            //ftDate, ftTime, ftDateTime : No se Pueden Borrar Fechas
                       end;
             end;
        end;
end;

{ ************* Consulta de Derechos de Acceso ************* }

function TBaseGridEdicion.CheckDerechos(const iDerecho: Integer): Boolean;
begin
     Result := ( FIndexDerechos = 0 ) or ( ZAccesosMgr.CheckDerecho( FIndexDerechos, iDerecho ) );
end;

function TBaseGridEdicion.PuedeAgregar: Boolean;
begin
     Result := CheckDerechos( K_DERECHO_ALTA );
end;

function TBaseGridEdicion.PuedeModificar: Boolean;
begin
     Result := CheckDerechos( K_DERECHO_CAMBIO );
end;

function TBaseGridEdicion.PuedeBorrar: Boolean;
begin
     Result := CheckDerechos( K_DERECHO_BAJA );
end;

function TBaseGridEdicion.PuedeImprimir: Boolean;
begin
     Result := CheckDerechos( K_DERECHO_IMPRESION );
end;

procedure TBaseGridEdicion.SetAccesos;

   procedure SetAccesoBoton( oControl: TControl; const lEnabled: Boolean );
   begin
        with oControl do
             if Visible then
                Enabled := lEnabled;
   end;

begin
     SetAccesoBoton( AgregarBtn, PuedeAgregar );
     SetAccesoBoton( ModificarBtn, PuedeModificar );
     SetAccesoBoton( BorrarBtn, PuedeBorrar );
     SetAccesoBoton( ImprimirBtn, PuedeImprimir );
     SetAccesoBoton( ImprimirFormaBtn, PuedeImprimir );
     DataSource.AutoEdit := PuedeModificar;
end;

{ ************* Eventos de Controles ************* }

procedure TBaseGridEdicion.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     if ( Editing or Inserting ) then
        HuboCambios := True
     else
         HuboCambios := False;
end;

procedure TBaseGridEdicion.SetHuboCambios( const Value: Boolean );
begin
     if ( FHuboCambios <> Value ) or ( FInicial ) then
     begin
          FHuboCambios := Value;
          HabilitaControles;
          FInicial := False;
     end;
end;

procedure TBaseGridEdicion.HabilitaControles;
begin
     OK.Enabled := Editing;
     if Editing then
     begin
          with Cancelar do
          begin
               Kind := bkCancel;
               ModalResult := mrNone;
               Cancel := False;
               Caption := '&Cancelar';
               Hint := 'Cancelar Cambios';
          end;
     end
     else
     begin
          // En casos donde hubo errores y el usuario decide borrar registros
          // del grid hasta que el ChangeCount llega a 0
          if ( FHuboErrores ) then
          begin
            // ZetaDialogo.ZInformation( Self.Caption, 'Todos los errores han sido corregidos' + CR_LF + 'y los registros fueron grabados', 0 );
            if ZetaDialogo.ZConfirm( Self.Caption, 'Todos los errores han sido corregidos' + CR_LF + 'y los registros fueron grabados.' + CR_LF + '� Desea Salir ?', 0, mbYes ) then
              Close;
            FHuboErrores := FALSE;
          end;
          with Cancelar do
          begin
               Kind := bkClose;
               ModalResult := mrNone;
               Cancel := True;
               Caption := '&Salir';
               Hint := 'Cerrar Pantalla y Salir';
          end;
     end;
     Self.ActiveControl := ZetaDBGrid;
     Application.ProcessMessages;
end;

procedure TBaseGridEdicion.AgregarBtnClick(Sender: TObject);
begin
     inherited;
     Agregar;
end;

procedure TBaseGridEdicion.BorrarBtnClick(Sender: TObject);
begin
     inherited;
     Borrar;
end;

procedure TBaseGridEdicion.ModificarBtnClick(Sender: TObject);
begin
     inherited;
     Modificar;
end;

procedure TBaseGridEdicion.ImprimirBtnClick(Sender: TObject);
begin
     inherited;
     Imprimir;
end;

procedure TBaseGridEdicion.ImprimirFormaBtnClick(Sender: TObject);
begin
     inherited;
     ImprimirForma;
end;

procedure TBaseGridEdicion.BuscarBtnClick(Sender: TObject);
begin
     inherited;
     Buscar;
end;

procedure TBaseGridEdicion.CortarBtnClick(Sender: TObject);
begin
     inherited;
     SendMessage( ActiveControl.Handle, WM_CUT, 0, 0 );
end;

procedure TBaseGridEdicion.CopiarBtnClick(Sender: TObject);
begin
     inherited;
     SendMessage( ActiveControl.Handle, WM_COPY, 0, 0 );
end;

procedure TBaseGridEdicion.PegarBtnClick(Sender: TObject);
begin
     inherited;
     SendMessage( ActiveControl.Handle, WM_PASTE, 0, 0 );
end;

procedure TBaseGridEdicion.UndoBtnClick(Sender: TObject);
begin
     inherited;
     SendMessage( ActiveControl.Handle, WM_UNDO, 0, 0 );
end;

function TBaseGridEdicion.PosicionaSiguienteColumna: Integer;
var
   i: Integer;
begin
     with ZetaDBGrid do
     begin
          i := SelectedIndex + 1;
          while ( i < Columns.Count ) and ( Columns[ i ].ReadOnly or not Columns[ i ].Visible ) do
          begin
               i := i + 1;
          end;
          if ( i = Columns.Count ) then
             Result := FPrimerColumna
          else
              Result := i;
     end;
end;

procedure TBaseGridEdicion.KeyDown(var Key: Word; Shift: TShiftState);
begin
     if ( GridEnfocado ) then
     begin
          if ( ssShift in Shift ) then { SHIFT }
          begin
               case Key of
                    VK_INSERT:    { INS = Agregar }
                    begin
                         Key := 0;
                         Agregar;
                    end;
                    VK_DELETE:    { DEL = Borrar }
                    begin
                         Key := 0;
                         Borrar;
                    end;
               end;
          end
          else
          begin
              if ( ssCtrl in Shift ) then { CTRL }
              begin
                   case Key of
                        VK_INSERT:   { INS = Modificar }
                        begin
                             Key := 0;
                             Modificar;
                        end;
                        80: { Letra P = Imprimir }
                        begin
                             Key := 0;
                             Imprimir;
                        end;
                        70: { Letra F = Buscar }
                        begin
                             Key := 0;
                             Buscar;
                        end;
                   end;
              end;
          end;
          if ( Key = VK_RETURN ) then
          begin
               Key := 0;
          end;
          if ( Key = VK_DOWN ) then
          begin
               Key := 0;
               SeleccionaSiguienteRenglon;
          end;
          if ( Key = VK_DELETE ) and ( not ZetaDBGrid.EditorMode ) then     // Si est� editando la celda lo deja pasar
          begin
               Key := 0;
               BorraCelda;
          end;
     end
     else
         inherited KeyDown( Key, Shift );
end;

procedure TBaseGridEdicion.KeyPress(var Key: Char);
begin
     if ( GridEnfocado ) then
     begin
          if ( Key = Chr( VK_RETURN ) ) then
          begin
               Key := #0;
               ZetaDBGrid.SelectedIndex := PosicionaSiguienteColumna;
               if ( ZetaDBGrid.SelectedIndex = FPrimerColumna ) then
                  SeleccionaSiguienteRenglon;
          end
          else
              if ( Key = Chr( VK_ESCAPE ) ) then
              begin
                   SeleccionaPrimerColumna;
              end;
     end
     else if ActiveControl.TabStop then
          inherited KeyPress( Key );
end;

function TBaseGridEdicion.GridEnfocado: Boolean;
begin
     Result := ( ActiveControl = ZetaDBGrid );
end;

procedure TBaseGridEdicion.WMExaminar(var Message: TMessage);
begin
     if ( TExaminador(  Message.LParam ) = exEnter ) then
     begin
          ZetaDBGrid.SelectedIndex := PosicionaSiguienteColumna;
          if ( ZetaDBGrid.SelectedIndex = FPrimerColumna ) then
             SeleccionaSiguienteRenglon;
     end;
end;

procedure TBaseGridEdicion.ZetaDBGridColEnter(Sender: TObject);
begin
     inherited;
     with ZetaDBGrid do
          with Columns[SelectedIndex] do
               BuscarBtn.Enabled := ( not ReadOnly ) and ( ButtonStyle = cbsAuto );
end;

procedure TBaseGridEdicion.ExportarBtnClick(Sender: TObject);
begin
     inherited;
     Exportar;
end;

end.

