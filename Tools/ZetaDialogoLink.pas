unit ZetaDialogoLink;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, CommDlg,ZetaCommonLists;

function ZetaMessageLink( const sCaption, sMsgUrl: String; var sMsgCompleto: String; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): TModalResult; forward;
function ZMensajeConfirm( const sCaption: String; var sMsg: String; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): Boolean;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaMsgDlgLink;
var
   ZDialogoLink: TZDialogoLink;


function GetDialogo: TZDialogoLink;  // Siempre debe ser Privada a esta unidad //
begin
     if ( ZDialogoLink = nil ) then
     begin
          ZDialogoLink := TZDialogoLink.Create( Application ); // Application Debe Destruirlo Al Finalizar //
     end;
     Result := ZDialogoLink;
end;

function ZetaMessageLink( const sCaption, sMsgUrl: String; var sMsgCompleto: String; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): TModalResult;
begin
     with GetDialogo do
     begin
          Caption := sCaption;
          MensajeLink := sMsgUrl;
          MensajeCompleto := sMsgCompleto;
          Tipo := DlgType;
          Botones := Buttons;
          DefaultBoton := oDefaultBoton;
          HelpCtx := iHelpCtx;
          Result := Execute;
     end;
end;

function ZMensajeConfirm( const sCaption: String; var sMsg: String; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): Boolean;
begin
     Result := ( ZetaMessageLink( sCaption,sMsg, sMsg, mtCustom, [ mbOk, mbCancel ], iHelpCtx, mbOk ) = mrOK )
end;

end.

