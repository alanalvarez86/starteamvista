unit ZBaseConsultaBotones;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, ComCtrls,
     ZBaseConsulta, ToolWin, ImgList, ActnList, StdCtrls;

type
  TBaseBotones = class(TBaseConsulta)
    MainToolBar: TToolBar;
    Imagenes: TImageList;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Disconnect; override;
    procedure Refresh; override;
    procedure SetImageIndex(const iIndex: Integer);
  public
    { Public declarations }
  end;

var
  BaseBotones: TBaseBotones;

implementation

{$R *.DFM}

procedure TBaseBotones.FormCreate(Sender: TObject);
begin
     inherited;
     EsConsulta := False;
end;

procedure TBaseBotones.Connect;
var
   i: Integer;
begin
     with MainToolBar do
     begin
          { Evita que los short-cuts de los botones est�n activos }
          { cuando la forma no es la forma activa del Shell }
          { Ver TBaseBotones.Disconnect }
          Enabled := True;
          for i := 0 to ( ButtonCount - 1 ) do
          begin
               with Buttons[ i ] do
               begin
                    if ( ImageIndex < 0 ) then
                       ImageIndex := 0;
                    Enabled := TAction( Action ).Enabled;
               end;
          end;
     end;
end;

procedure TBaseBotones.SetImageIndex( const iIndex: Integer );
var
   i: Integer;
begin
     with MainToolBar do
     begin
          for i := 0 to ( ButtonCount - 1 ) do
          begin
               with Buttons[ i ] do
               begin
                    ImageIndex := iIndex;
               end;
          end;
     end;
end;

procedure TBaseBotones.Disconnect;
begin
     inherited Disconnect;
     with MainToolBar do
     begin
          { Evita que los short-cuts de los botones est�n activos }
          { cuando la forma no es la forma activa del Shell }
          { � Quien sabe por qu� ? � ActionList del Shell ? }
          Enabled := False;
     end;
end;

procedure TBaseBotones.Refresh;
begin
end;

end.
