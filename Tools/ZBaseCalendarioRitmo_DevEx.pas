unit ZBaseCalendarioRitmo_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, ExtCtrls, Grids,
     StdCtrls, Buttons, ComCtrls, Spin,
     {$ifdef TRESS_DELPHIXE5_UP}System.UITypes,{$endif}
     ZetaCommonClasses,
     ZetaKeyCombo, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore, TressMorado2013, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxButtons, cxGroupBox,
  dxSkinsDefaultPainters;

type
  TBaseCalendarioRitmo_DevEx = class(TForm)
    StringGrid: TStringGrid;
    PanelHeader: TPanel;
    PanelSuperior: TPanel;
    StatusBar: TStatusBar;
    PanelInferior: TPanel;
    Bevel1: TBevel;
    LblHabil: TLabel;
    LblDescanso: TLabel;
    LblSabado: TLabel;
    LblNinguno: TLabel;
    PanelShowHabil: TPanel;
    imgHabil: TImage;
    PanelShowDescanso: TPanel;
    imgHabil2: TImage;
    PanelShowSabado: TPanel;
    imgSabado: TImage;
    PanelShowNinguno: TPanel;
    imgNinguno: TImage;
    grpMes: TcxGroupBox;
    btnMonthBefore_DevEx: TcxButton;
    MonthValue_DevEx: TcxTextEdit;
    btnMonthNext_DevEx: TcxButton;
    grpAno: TcxGroupBox;
    btnYearBefore_DevEx: TcxButton;
    YearValue_DevEx: TcxTextEdit;
    btnYearNext_DevEx: TcxButton;
    OK_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure StringGridDrawCell(Sender: TObject; Col, Row: Integer; Rect: TRect; State: TGridDrawState);
    procedure StringGridClick(Sender: TObject);
    procedure StringGridSelectCell(Sender: TObject; Col, Row: Integer; var CanSelect: Boolean);
    procedure btnMonthNext_DevExClick(Sender: TObject);
    procedure btnMonthBefore_DevExClick(Sender: TObject);
    procedure YearValue_DevExPropertiesChange(Sender: TObject);
    procedure btnYearNext_DevExClick(Sender: TObject);
    procedure btnYearBefore_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FFecha: TDate;
    FChanging: Boolean;
    FStartPos: Word;
    FActualMonthValue: Integer;
    FNewMonthValue:Integer;
    FActualYearValue:Integer;
    FNewYearValue:Integer;
    procedure SetFecha( const Value: TDate );
    procedure SetInfoCelda(const sDay: String; const iCol, iRow: Integer);
    {***DevEx(by am): Metodos agregados para los nuevos componentes de el a�o y el mes***}
    procedure AumentaMes;
    procedure DecrementaMes;
    procedure CalculaAnio(Operacion, NewYear:Integer);
    procedure HabilitaBotonesAnio;
    procedure HabilitaBotonesMes;
    {***}
  protected
    procedure ResetCalendario; virtual;
    procedure ResetFecha( wDay: Word ); virtual;
    procedure SetInfoCeldaFecha( const dFecha: TDate; const sDay: String; const iCol, iRow: Integer );virtual; abstract;
  public
    { Public declarations }
    property Fecha: TDate read FFecha write SetFecha;
    procedure Init;
    //DevEx (by am): Propiedades agregadas para leer campos del caluclod e a�o y mes
    property ActualMonthValue: Integer read FActualMonthValue write FActualMonthValue;
    property NewMonthValue:Integer read  FNewMonthValue write FNewMonthValue;
    property ActualYearValue:Integer read FActualYearValue write FActualYearValue;
    property NewYearValue:Integer read FNewYearValue write FNewYearValue;

  end;

const
     LONGITUD_DIA = 2;
     COLOR_DIA_HABIL = $0080FFFF;
     COLOR_DIA_SABADO = $0080FF90;
     COLOR_DIA_DESCANSO = clWhite;
     COLOR_DIA_NINGUNO = clSilver;
     COLOR_DIA_VACA = clSkyBlue;{OP: 11/06/08}
     DIA_VACACION = 3;{OP: 11/06/08}
     {***DevEx (by am): Constantes agregadas para el cambio de componentes de A�o y mes en el cambio de imagen***}
     K_MANUAL = 0;
     K_ANTERIOR = 1;
     K_SIGUIENTE = 2;
     K_MAX_YEAR = 2050;
     K_MIN_YEAR = 1899;
     {***}

var
  BaseCalendarioRitmo_DevEx: TBaseCalendarioRitmo_DevEx;
  //ActualMonthValue, NewMonthValue, ActualYearValue, NewYearValue:Integer; //TEST MES

implementation

uses dCatalogos, ZetaCommonTools, ZetaCommonLists;

{$R *.DFM}

{ *********** TBaseCalendarioRitmo ************ }

procedure TBaseCalendarioRitmo_DevEx.FormCreate(Sender: TObject);
var
   i: Integer;
begin
     with StringGrid do
     begin
          for i := 1 to K_DIAS_SEMANA do
              Cells[ ( i - 1 ), 0 ] := {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}ShortDayNames[ i ];
     end;
     LblHabil.Caption := ZetaCommonLists.ObtieneElemento( lfStatusAusencia, Ord( auHabil ) );
     LblDescanso.Caption := ZetaCommonLists.ObtieneElemento( lfStatusAusencia, Ord( auDescanso ) );
     LblSabado.Caption := ZetaCommonLists.ObtieneElemento( lfStatusAusencia, Ord( auSabado ) );
     LblNinguno.Caption := 'Ninguno';
     PanelShowHabil.Color := COLOR_DIA_HABIL;
     PanelShowSabado.Color := COLOR_DIA_SABADO;
     PanelShowDescanso.Color := clWhite;
     PanelShowNinguno.Color := clSilver;

     //TEST MES
      ActualMonthValue:= -1;
      NewMonthValue:= -1;
      ActualYearValue:= -1;
      NewYearValue:= -1;
end;

procedure TBaseCalendarioRitmo_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Action := caHide;
end;

procedure TBaseCalendarioRitmo_DevEx.Init;
begin
     OK_DevEx.Default := True;
     ActiveControl := OK_DevEx;
end;

procedure TBaseCalendarioRitmo_DevEx.SetFecha( const Value: TDate );
var
   Year, Month, Day: Word;
   Anio, Mes, Dia: Word;
begin
     if ( FFecha <> Value ) and not FChanging then
     begin
          FChanging := True;
          try
             DecodeDate( FFecha, Anio, Mes, Dia );
             DecodeDate( Value, Year, Month, Day );
             FFecha := Value;
             if ( Anio <> Year ) then
             begin
                ActualYearValue := Year;
                YearValue_DevEx.Text := IntToStr( ActualYearValue );
             end;
             if ( Mes <> Month ) or ( ActualMonthValue < 0 ) then
             begin
                ActualMonthValue := ( Month - 1 );
                MonthValue_DevEx.Text :=  ObtieneElemento( lfMeses, ActualMonthValue );
             end;
             if ( Anio <> Year ) or ( Mes <> Month ) then
                ResetCalendario;
             ResetFecha( Day );
          finally
                 FChanging := False;
          end;
     end;
end;

procedure TBaseCalendarioRitmo_DevEx.ResetCalendario;
var
   i, j, iDay, iFirstDay, iLastDay: Integer;
   sDay: String;
begin
     iFirstDay := DayOfWeek( FirstDayOfMonth( Fecha ) );
     iLastDay := TheDay( LastDayOfMonth( Fecha ) );
     iDay := 0;
     with StringGrid do
     begin
          for j := 2 to RowCount do // Empieza en 2 por el Encabezado //
          begin
               for i := 1 to ColCount do // Conteo basado en 1 para i, j //
               begin
                    if ( iDay = 0 ) and ( i = iFirstDay ) then
                    begin
                         iDay := 1;
                         sDay := '1';
                         FStartPos := ( ( j - 1 ) * ColCount ) + ( i - 1 );
                    end
                    else if ( iDay > 0 ) and ( iDay < iLastDay ) then
                    begin
                         iDay := iDay + 1;
                         sDay := IntToStr( iDay );
                    end
                    else
                         sDay := '';
                    SetInfoCelda( sDay, ( i - 1 ), ( j - 1 ) );
               end;
          end;
     end;
end;

procedure TBaseCalendarioRitmo_DevEx.SetInfoCelda( const sDay: String; const iCol, iRow: Integer );
var
   Year, Month, Day: Word;
   FechaEval: TDate;
begin
     with StringGrid do
     begin
          if strLleno( sDay ) then
          begin
               DecodeDate( Fecha, Year, Month, Day );
               Day := StrAsInteger( sDay );
               if DayIsValid( Year, Month, Day ) then
               begin
                    FechaEval := EncodeDate( Year, Month, Day );
                    SetInfoCeldaFecha( FechaEval, sDay, iCol, iRow );
{
                    with FRitmo.GetStatusHorario( FechaEval ) do
                    begin
                         Cells[ iCol, iRow ] := PadL( sDay, LONGITUD_DIA ) + Horario;
                         Objects[ iCol, iRow ] := TObject( Ord( Status ) );
                    end;
}
               end;
          end
          else
              Cells[ iCol, iRow ] := sDay;
     end;
end;

procedure TBaseCalendarioRitmo_DevEx.ResetFecha( wDay: Word );
var
   iTipoDia: Integer;
   sStatusDia : String;
begin
     wDay := FStartPos + wDay - 1;
     with StringGrid do
     begin
          Row := Trunc( wDay / ColCount );
          Col := ( wDay mod ColCount );
          // Investiga Status del Dia - Si es menor a cero se pone status: Ninguno
          iTipoDia := Integer( Objects[ Col, Row ] );
          if ( iTipoDia >= 0 ) then
             sStatusDia := ZetaCommonLists.ObtieneElemento( lfStatusAusencia, iTipoDia )
          else
              sStatusDia := 'Ninguno';
          with StatusBar do
          begin
               Panels[ 0 ].Text := FechaCorta( Fecha );
               Panels[ 1 ].Text := sStatusDia;
               Panels[ 2 ].Text := dmCatalogos.cdsHorarios.GetDescripcion( Copy( Cells[ Col, Row ], LONGITUD_DIA + 1, K_ANCHO_HORARIO ) );
          end;
     end;
end;

procedure TBaseCalendarioRitmo_DevEx.StringGridDrawCell( Sender: TObject; Col, Row: Integer; Rect: TRect; State: TGridDrawState );
const
     ESPACIO_RECT = 2;
     ESPACIO_ROW = 20;
var
   oColor, oColorFont: TColor;
   iLen: Integer;
   sTexto: String;
   //TipoDia: eStatusAusencia;
   iTipoDia : Integer;
begin
     if ( TGridDrawState( state ) <> [gdFixed] ) then
        with StringGrid.Canvas do
        begin
             oColor := Brush.Color;
             oColorFont := Font.Color;
             try
                Font.Color := clWindowText;
                if strVacio( StringGrid.Cells[ Col, Row ] ) then
                begin
                     Brush.Color := clInfoBk;
                     FillRect( Rect );
                end
                else
                begin
{
                     TipoDia := eStatusAusencia( Integer( StringGrid.Objects[ Col, Row ] ) );
                     if ( TipoDia = auHabil ) then
                        Brush.Color := COLOR_DIA_HABIL
                     else
                        Brush.Color := clWhite;
}
                     iTipoDia := Integer( StringGrid.Objects[ Col, Row ] );
                     if ( iTipoDia >= 0 ) then
                     begin
                          if( iTipoDia = DIA_VACACION ) then
                              Brush.Color := COLOR_DIA_VACA{OP: 11/06/08}
                          else
                          begin
                               case eStatusAusencia( iTipoDia ) of
                                    auSabado   : Brush.Color := COLOR_DIA_SABADO;
                                    auDescanso : Brush.Color := COLOR_DIA_DESCANSO;
                               else
                                   Brush.Color := COLOR_DIA_HABIL;
                               end;
                          end;
                     end
                     else
                     begin
                          Brush.Color := COLOR_DIA_NINGUNO; { No es Tipo de Dia valido }
                     end;

                     FillRect( Rect );
                     //TextOut( Rect.Left + ESPACIO_RECT, Rect.Top + ESPACIO_RECT, strLeft( ObtieneElemento( lfStatusAusencia, Ord( TipoDia ) ), 1 ) );
                     sTexto := StrLeft( StringGrid.Cells[ Col, Row ], LONGITUD_DIA );
                     try
                        Font.Style := [ fsBold ];
                        iLen := TextWidth( sTexto );
                        TextOut( Rect.Right - ( ESPACIO_RECT + iLen ), Rect.Top + ESPACIO_RECT, sTexto );
                     finally
                        Font.Style := [];
                     end;
                     TextOut( Rect.Left + ESPACIO_RECT, Rect.Top + ESPACIO_ROW, Copy( StringGrid.Cells[ Col, Row ], LONGITUD_DIA + 1, K_ANCHO_HORARIO ) );
                end;
             finally
                Font.Color := oColorFont;
                Brush.Color := oColor;
             end;
        end;
end;

procedure TBaseCalendarioRitmo_DevEx.StringGridClick(Sender: TObject);
var
   Year, Month, Day: Word;
begin
     DecodeDate( Fecha, Year, Month, Day );
     with StringGrid do
          Day := StrAsInteger( StrLeft( Cells[ Col, Row ], LONGITUD_DIA ) );
     if DayIsValid( Year, Month, Day ) then
        Fecha := EncodeDate( Year, Month, Day );
end;

procedure TBaseCalendarioRitmo_DevEx.StringGridSelectCell(Sender: TObject; Col, Row: Integer; var CanSelect: Boolean);
begin
     CanSelect := ( StringGrid.Cells[ Col, Row ] <> VACIO );
end;

//DevEx (by am): Metodo para aumentar el mes al dar clic en el boton siguiente
procedure TBaseCalendarioRitmo_DevEx.AumentaMes;
Const
     K_DICIEMBRE = 11;
     K_ENERO=0;
begin
     NewMonthValue := ActualMonthValue+1;
     if NewMonthValue > K_DICIEMBRE then
     begin
        CalculaAnio ( K_SIGUIENTE , ActualYearValue );
        //Si se pudo realizar el cambio de a�o recalcula a�o y actualiza el mes, si no lo deja igual y desactiva el boton de siguiente mes
        if btnYearNext_DevEx.Enabled then
        begin
             Fecha := DateTheYear( Fecha, ActualYearValue );
             ActualMonthValue := K_ENERO;
        end
        else
        begin
             btnMonthNext_DevEx.Enabled := FALSE;
        end;
     end
     else
     begin
        ActualMonthValue := NewMonthValue;
        HabilitaBotonesMes;
     end;
     Fecha := DateTheMonth( Fecha, ( ActualMonthValue + 1 ) );
end;

//DevEx (by am): Metodo para decrementar el mes al dar clic en el boton anterior
procedure TBaseCalendarioRitmo_DevEx.DecrementaMes;
Const
     K_DICIEMBRE = 11;
     K_ENERO=0;
begin
     NewMonthValue := ActualMonthValue-1;
     if NewMonthValue < K_ENERO then
     begin
        CalculaAnio ( K_ANTERIOR , ActualYearValue );
        //Si se pudo realizar el cambio de a�o recalcula a�o y actualiza el mes, si no lo deja igual y desactiva el boton de mes anterior
        if btnYearBefore_DevEx.Enabled then
        begin
             Fecha := DateTheYear( Fecha, ActualYearValue );
             ActualMonthValue := K_DICIEMBRE;
        end
        else
             btnMonthBefore_DevEx.Enabled  := FALSE;
     end
     else
     begin
        ActualMonthValue := NewMonthValue;
        HabilitaBotonesMes;
     end;
     Fecha := DateTheMonth( Fecha, ( ActualMonthValue + 1 ) );
end;

//DevEx (by am): Botones para Habilitar los botones de los componentes de mes y a�o
procedure TBaseCalendarioRitmo_DevEx.HabilitaBotonesAnio;
begin
     btnYearNext_DevEx.Enabled := TRUE;
     btnYearBefore_DevEx.Enabled := TRUE;
end;

procedure TBaseCalendarioRitmo_DevEx.HabilitaBotonesMes;
begin
     btnMonthNext_DevEx.Enabled := TRUE;
     btnMonthBefore_DevEx.Enabled := TRUE;
end;

//DevEx (by am): Metodo para calcular el a�o para el calculo de la fecha
procedure TBaseCalendarioRitmo_DevEx.CalculaAnio(Operacion, NewYear:Integer);
 procedure ValidaNewYear;
 begin
     if NewYearValue > K_MAX_YEAR then
     begin
        ActualYearValue := K_MAX_YEAR;
        btnYearNext_DevEx.Enabled := FALSE;  //Deshabilita btnYearSiguiente
     end
     else if NewYearValue < K_MIN_YEAR then
     begin
        ActualYearValue := K_MIN_YEAR;
        btnYearBefore_DevEx.Enabled := FALSE;//Deshabilita btnYearAnterior
     end
     else
     begin
        ActualYearValue := NewYearValue;
        HabilitaBotonesAnio;
        HabilitaBotonesMes;
     end;
     //Result := ActualYearValue;
 end;
begin
NewYearValue := NewYear;
     case Operacion of
          K_ANTERIOR:
               NewYearValue := NewYearValue-1;
          K_SIGUIENTE:
               NewYearValue := NewYearValue+1;
     end;
     ValidaNewYear;
end;

procedure TBaseCalendarioRitmo_DevEx.btnMonthNext_DevExClick(
  Sender: TObject);
begin
  AumentaMes;
end;

procedure TBaseCalendarioRitmo_DevEx.btnMonthBefore_DevExClick(
  Sender: TObject);
begin
  DecrementaMes;
end;

procedure TBaseCalendarioRitmo_DevEx.YearValue_DevExPropertiesChange(
  Sender: TObject);
begin
     if ( Length( YearValue_DevEx.Text ) = 4 ) then
     begin
        CalculaAnio ( K_MANUAL, StrToInt( YearValue_DevEx.Text ) );
        Fecha := DateTheYear( Fecha, ActualYearValue );
     end;
end;

procedure TBaseCalendarioRitmo_DevEx.btnYearNext_DevExClick(
  Sender: TObject);
begin
        CalculaAnio ( K_SIGUIENTE , StrToInt( YearValue_DevEx.Text ));
        Fecha := DateTheYear( Fecha, ActualYearValue );
end;

procedure TBaseCalendarioRitmo_DevEx.btnYearBefore_DevExClick(
  Sender: TObject);
begin
        CalculaAnio ( K_ANTERIOR , StrToInt( YearValue_DevEx.Text ));
        Fecha := DateTheYear( Fecha, ActualYearValue );
end;

end.
