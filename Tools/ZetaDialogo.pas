unit ZetaDialogo;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, CommDlg,ZetaCommonLists;

function ZetaMessage( const sCaption, sMsg: String; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): TModalResult; forward;
function ZErrorConfirm( const sCaption, sMsg: String; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): Boolean;
function ZExcepcionConfirm( const sCaption, sMsg: String; Problema: Exception; const iHelpCtx: Longint ): Boolean;
function ZWarningConfirm( const sCaption, sMsg: String; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): Boolean;
function ZSiNoCancel( const sCaption, sMsg: String; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): TModalResult;
function ZYesToAll( const sCaption, sMsg: String; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): TModalResult;
function ZConfirm( const sCaption, sMsg: String; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): Boolean;
function ZRetry( const sCaption, sMsg: String; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): Boolean;
procedure ZError( const sCaption, sMsg: String; const iHelpCtx: Longint );
procedure ZWarning( const sCaption, sMsg: String; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn );
procedure ZExcepcion( const sCaption, sMsg: String; Problema: Exception; const iHelpCtx: Longint );
procedure ZInformation( const sCaption, sMsg: String; const iHelpCtx: Longint );
function zWarningYesNoConfirm( const sCaption, sMsg: String; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): Boolean;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaMsgDlg,
     ZetaRegistryCliente;

var
   ZDialogo: TZDialogo;


function GetDialogo: TZDialogo;  // Siempre debe ser Privada a esta unidad //
begin
     if ( ZDialogo = nil ) then
     begin
          ZDialogo := TZDialogo.Create( Application ); // Application Debe Destruirlo Al Finalizar //
     end;
     Result := ZDialogo;
end;

{ ************** Funci�n General de Mensajes ****************** }

function ZetaMessage( const sCaption, sMsg: String; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): TModalResult;
begin
     //dmCliente := TdmCliente.Create( Self );
     with GetDialogo do
     begin
          Caption := sCaption;
          Mensaje := sMsg;
          Tipo := DlgType;
          Botones := Buttons;
          DefaultBoton := oDefaultBoton;
          HelpCtx := iHelpCtx;
          Result := Execute;
     end;
end;

{ ************* Dialogos de Errores ************ }

function ZExcepcionConfirm( const sCaption, sMsg: String; Problema: Exception; const iHelpCtx: Longint ): Boolean;
begin
     Result := ( ZetaMessage( sCaption, sMsg + CR_LF + GetExceptionInfo( Problema ), mtError, [ mbOk, mbCancel ], iHelpCtx, mbOk ) = mrOK );
end;

procedure zExcepcion( const sCaption, sMsg: String; Problema: Exception; const iHelpCtx: Longint );
begin
     {
     GetDialogo.Excepcion( sMsg, Problema );
     }
     ZetaMessage( sCaption, sMsg + CR_LF + GetExceptionInfo( Problema ), mtError, [ mbOk ], iHelpCtx, mbOk );
end;

procedure ZError( const sCaption, sMsg: String; const iHelpCtx: Longint );
begin
     ZetaMessage( sCaption, sMsg, mtError, [ mbOk ], iHelpCtx, mbOk );
end;

function ZErrorConfirm( const sCaption, sMsg: String; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): Boolean;
begin
     Result := ( ZetaMessage( sCaption, sMsg, mtError, [mbOk, mbCancel ], iHelpCtx, oDefaultBoton ) = mrOK );
end;

{ ********** Dialogos de Adventencia ************* }

procedure ZWarning( const sCaption, sMsg: String; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn );
begin
     ZetaMessage( sCaption, sMsg, mtWarning, [ mbOk ], iHelpCtx, oDefaultBoton );
end;

function ZWarningConfirm( const sCaption, sMsg: String; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): Boolean;
begin
     Result := ( ZetaMessage( sCaption, sMsg, mtWarning, [ mbOk, mbCancel ], iHelpCtx, oDefaultBoton ) = mrOK )
end;

{ ************ Dialogos de Informacion ************* }

procedure ZInformation( const sCaption, sMsg: String; const iHelpCtx: Longint );
begin
     ZetaMessage( sCaption, sMsg, mtInformation, [ mbOk ], iHelpCtx, mbOk );
end;

{ ************ Dialogos de Confirmacion ************* }

function zConfirm( const sCaption, sMsg: String; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): Boolean;
begin
     Result := ( ZetaMessage( sCaption, sMsg, mtConfirmation, [ mbYes, mbNo ], iHelpCtx, oDefaultBoton ) = mrYes );
end;

function zWarningYesNoConfirm( const sCaption, sMsg: String; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): Boolean;
begin
     Result := ( ZetaMessage( sCaption, sMsg, mtWarning, [ mbYes, mbNo ], iHelpCtx, oDefaultBoton ) = mrYes );
end;

{ ************* Dialogos de Reintentar ************ }

function zRetry( const sCaption, sMsg: String; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): Boolean;
begin
     Result := ( ZetaMessage( sCaption, sMsg, mtConfirmation, [ mbRetry, mbCancel ], iHelpCtx, oDefaultBoton ) = mrRetry );
end;

function ZSiNoCancel( const sCaption, sMsg: String; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): TModalResult;
begin
     Result := ZetaMessage( sCaption, sMsg, mtInformation, [ mbYes, mbNo, mbCancel ], iHelpCtx, oDefaultBoton );
end;

function ZYesToAll( const sCaption, sMsg: String; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): TModalResult;
begin
     Result := ZetaMessage( sCaption, sMsg, mtInformation, [ mbYes, mbNo, mbYesToAll ], iHelpCtx, oDefaultBoton );
end;

end.

