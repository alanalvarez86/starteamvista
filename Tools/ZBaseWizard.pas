unit ZBaseWizard;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls,
     ZetaKeyLookup,
     {$ifdef DOS_CAPAS}
     DZetaServerProvider,
     {$endif}
     ZWizardBasico,
     ZetaCommonClasses,
     ZetaWizard;

type
  TBaseWizard = class(TWizardBasico)
    Parametros: TTabSheet;
    Ejecucion: TTabSheet;
    Advertencia: TMemo;
    ProgressPanel: TPanel;
    ProgressBar: TProgressBar;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
  private
    { Private declarations }
    FParametrosControl: TWinControl;
    FParameterList: TZetaParams;
    FDescripciones: TZetaParams;
    FGlobales: TZetaParams;
    FErrores: Integer;
    {$ifdef DOS_CAPAS}
    oZetaProvider: TdmZetaServerProvider;
    procedure CallMeBackStart( Sender: TObject; const sMsg: String; const iMax: Integer );
    procedure CallMeBack( Sender: TObject; const sMsg: String; const iStep: Integer; var Continue: Boolean );
    procedure CallMeBackEnd( Sender: TObject; const sMsg: String );
    {$endif}
    procedure CargarGlobales;
    procedure DescargarGlobales;
    procedure EnfocaParametrosControl;
  protected
    { Protected declarations }
    property ParametrosControl: TWinControl read FParametrosControl write FParametrosControl;
    property ParameterList: TZetaParams read FParameterList;
    property Descripciones: TZetaParams read FDescripciones;
    function CancelarWizard: Boolean; virtual;
    function EjecutarWizard: Boolean; virtual; abstract;
    function Error(const sError: String; oControl: TWinControl): Boolean;
    function EjecutaProgramaArchivo( const sPrograma, sParametros, sArchivo: String; var sMensaje: String ): Boolean;
    procedure CargaParametros; virtual;
    procedure TerminarWizard; dynamic;
    function GetDescripLlave( oLookup: TZetaKeyLookup ): string;

  public
    { Public declarations }
  end;

var
  BaseWizard: TBaseWizard;

implementation

uses DGlobal,
     ZetaDialogo,
     ZetaMessages,
     ZBasicoSelectGrid,
     ZetaCommonTools;

{$R *.DFM}

procedure TBaseWizard.FormCreate(Sender: TObject);
begin
     inherited;
     FParameterList := TZetaParams.Create;
     FDescripciones := TZetaParams.Create;
     FGlobales := TZetaParams.Create;
     ZBasicoSelectGrid.ParametrosGrid := FDescripciones;
     FErrores := 0;
     {$ifdef DOS_CAPAS}
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
     {$endif}
end;

procedure TBaseWizard.FormShow(Sender: TObject);
{$ifdef DOS_CAPAS}
const
     K_MARGEN_PANEL = 23;
{$endif}
begin
     inherited;
     CargarGlobales;
     EnfocaParametrosControl;
     {$ifdef DOS_CAPAS}
     ProgressPanel.Visible := True;
     with ProgressBar do
     begin
          Visible := True;
          Left := K_MARGEN_PANEL + 1;
          Width := iMax( 0, ProgressPanel.Width - ( K_MARGEN_PANEL * 2 ) );
     end;
     {$else}
     ProgressPanel.Visible := False;
     ProgressBar.Visible := False;
     {$endif}
end;

procedure TBaseWizard.FormDestroy(Sender: TObject);
begin
     inherited;
     FGlobales.Free;
     FDescripciones.Free;
     FParameterList.Free;
end;

procedure TBaseWizard.CargarGlobales;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           Global.CargaControles( FGlobales, Self );
        except
              on Error: Exception do
              begin
                   zExcepcion( Caption, 'Error Al Leer Defaults del Wizard', Error, 0 );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBaseWizard.DescargarGlobales;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           Global.DescargaControles( FGlobales, Self );
        except
              on Error: Exception do
              begin
                   zExcepcion( Caption, 'Error Al Escribir Defaults del Wizard', Error, 0 );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBaseWizard.CargaParametros;
begin
     
end;

function TBaseWizard.Error( const sError: String; oControl: TWinControl ): Boolean;
begin
     ZetaDialogo.ZError( Caption, sError, 0 );
     oControl.SetFocus;
     Result := False;
end;

function TBaseWizard.EjecutaProgramaArchivo( const sPrograma, sParametros, sArchivo: String; var sMensaje: String ): Boolean;
const
     K_OK_EXECUTE = 0;
     K_ERROR_EXECUTE = -1;
     K_ERROR_NOT_FOUND = 1;
     K_MESS_ERROR_EXECUTE = 'Error Al Ejecutar El Programa: %s%s';
     K_MESS_ERROR_EXE_NOT_FOUND = 'El Programa: %s%s%s No Fu� Encontrado';
     K_MESS_ERROR_FILE_NOT_FOUND = 'Archivo %s No Fu� Creado';
var
   oCursor: TCursor;
   iResultado : Integer;
begin
     if ZetaCommonTools.StrLleno( sPrograma ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             iResultado := ZetaMessages.WinExecAndWait32( sPrograma, sParametros, SW_SHOWNORMAL );
          finally
                 Screen.Cursor := oCursor;
          end;
          Result := ( iResultado = K_OK_EXECUTE );
          if Result then
          begin
               Result := FileExists( sArchivo );
               if not Result then
                  sMensaje := Format( K_MESS_ERROR_FILE_NOT_FOUND, [ sArchivo ] );
          end
          else
          begin
               case iResultado of
                    K_ERROR_EXECUTE: sMensaje := Format( K_MESS_ERROR_EXECUTE, [ CR_LF, sPrograma ] );
                    K_ERROR_NOT_FOUND: sMensaje := Format( K_MESS_ERROR_EXE_NOT_FOUND, [ CR_LF, sPrograma, CR_LF ] );
               { else
                     ZetaMessages.WinExecAndWait32 solo puede regresar valores : -1, 0 y 1 }
               end;
          end;
     end
     else
         Result := True;
end;

procedure TBaseWizard.EnfocaParametrosControl;
var
   lEnfocar: Boolean;
begin
//     if Wizard.EsPaginaActual( Parametros ) then
//     begin
          if ( ParametrosControl <> nil ) then
          begin
               with ParametrosControl do
               begin
                    lEnfocar := Visible and Enabled and CanFocus;
               end;
               if lEnfocar then
                  self.ActiveControl := ParametrosControl;
               //ActiveControl := ParametrosControl;
          end;
//     end;
end;

function TBaseWizard.CancelarWizard: Boolean;
begin
     Close;
     Result := True;
end;

procedure TBaseWizard.TerminarWizard;
begin
     DescargarGlobales;
     if not Wizard.Reejecutar then
     begin
{$ifndef DOS_CAPAS}
          WindowState := wsMinimized; { Efecto de 'Zoom small' }
{$endif}
          Close;
          Windows.SetForegroundWindow( Handle ); { Evita que la aplicaci�n pierda el Focus }
     end;
end;

{$ifdef DOS_CAPAS}
procedure TBaseWizard.CallMeBackStart( Sender: TObject; const sMsg: String; const iMax: Integer );
begin
     with ProgressBar do
     begin
          Max := iMax;
          Step := 1;
     end;
     with ProgressPanel do
     begin
          if ( sMsg = '' ) then
             Caption := 'Iniciando Procesamiento'
          else
              Caption := sMsg;
     end;
end;

procedure TBaseWizard.CallMeBack( Sender: TObject; const sMsg: String; const iStep: Integer; var Continue: Boolean );
begin
     with ProgressBar do
     begin
          Position := iStep;
          with ProgressPanel do
          begin
               if ( sMsg = '' ) and ( Max > 0 ) then
                  Caption := Format( 'Avance %4.1n', [ 100 * iStep / Max ] ) + ' %'
               else
                   Caption := sMsg;
          end;
     end;
     Application.ProcessMessages;
     Continue := not Wizard.Cancelado;
end;

procedure TBaseWizard.CallMeBackEnd( Sender: TObject; const sMsg: String );
begin
     with ProgressBar do
     begin
          Position := Max;
     end;
     with ProgressPanel do
     begin
          if ( sMsg = '' ) then
             Caption := 'Proceso Terminado'
          else
              Caption := sMsg;
     end;
end;
{$endif}

procedure TBaseWizard.WizardAfterMove(Sender: TObject);
begin
     inherited;
     EnfocaParametrosControl;
end;

procedure TBaseWizard.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        Application.ProcessMessages;
        {$ifdef DOS_CAPAS}
        with oZetaProvider do
        begin
             OnCallBackStart := CallMeBackStart;
             OnCallBack := CallMeBack;
             OnCallBackEnd := CallMeBackEnd;
        end;
        try
        {$endif}
           try
              CargaParametros;
              lOk := EjecutarWizard;
           except
                 on Error: Exception do
                 begin
                      zExcepcion( Caption, 'Error Al Ejecutar Wizard', Error, 0 );
                      lOk := False;
                 end;
           end;
        {$ifdef DOS_CAPAS}
        finally
               with oZetaProvider do
               begin
                    OnCallBackStart := nil;
                    OnCallBack := nil;
                    OnCallBackEnd := nil;
               end;
        end;
        {$endif}
     finally
            Screen.Cursor := oCursor;
     end;
     if lOk then
        TerminarWizard;
end;

procedure TBaseWizard.WizardAlCancelar(Sender: TObject; var lOk: Boolean);
begin
     inherited;
     lOk := CancelarWizard;
end;


function TBaseWizard.GetDescripLlave( oLookup: TZetaKeyLookup ): string;
begin
     Result := VACIO;
     with oLookup do
          if StrLleno(Llave) then
             Result := Llave + ': ' + Descripcion;
end;
end.
