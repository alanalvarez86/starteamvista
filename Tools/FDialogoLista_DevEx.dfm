inherited DialogoLista_DevEx: TDialogoLista_DevEx
  Left = 580
  Top = 291
  Caption = 'DialogoLista_DevEx'
  ClientHeight = 291
  ClientWidth = 268
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 255
    Width = 268
    DesignSize = (
      268
      36)
    inherited OK_DevEx: TcxButton
      Left = 103
      OnClick = OK_DevExClick
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 182
    end
  end
  object cbLista: TcxCheckListBox [1]
    Left = 0
    Top = 0
    Width = 268
    Height = 255
    Align = alClient
    EditValueFormat = cvfStatesString
    Items = <>
    TabOrder = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
