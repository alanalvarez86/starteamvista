inherited BaseEdicion: TBaseEdicion
  Left = 389
  Top = 215
  Caption = 'BaseEdicion'
  ClientHeight = 225
  ClientWidth = 473
  OldCreateOrder = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 189
    Width = 473
    DesignSize = (
      473
      36)
    inherited OK: TBitBtn
      Left = 305
      Anchors = [akRight, akBottom]
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 390
      Anchors = [akRight, akBottom]
      ModalResult = 0
      OnClick = CancelarClick
      Kind = bkCustom
    end
  end
  object PanelSuperior: TPanel
    Left = 0
    Top = 0
    Width = 473
    Height = 32
    Align = alTop
    TabOrder = 1
    object AgregarBtn: TSpeedButton
      Left = 106
      Top = 4
      Width = 24
      Height = 24
      Hint = 'Agregar Registro (Shift+Ins)'
      Enabled = False
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
        333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
        0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
        07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
        07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
        0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
        33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
        B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
        3BB33773333773333773B333333B3333333B7333333733333337}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = AgregarBtnClick
    end
    object BorrarBtn: TSpeedButton
      Left = 130
      Top = 4
      Width = 24
      Height = 24
      Hint = 'Borrar Registro (Shift+Del)'
      Enabled = False
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
        305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
        005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
        B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
        B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
        B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
        B0557777FF577777F7F500000E055550805577777F7555575755500000555555
        05555777775555557F5555000555555505555577755555557555}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = BorrarBtnClick
    end
    object ModificarBtn: TSpeedButton
      Left = 154
      Top = 4
      Width = 24
      Height = 24
      Hint = 'Modificar Registro (Ctrl+Ins)'
      Enabled = False
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = ModificarBtnClick
    end
    object BuscarBtn: TSpeedButton
      Left = 178
      Top = 4
      Width = 24
      Height = 24
      Hint = 'Buscar (Ctrl+B)'
      Enabled = False
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clSilver
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDD0000DDDDDDDDDDDDDDDDDDDD0000DDD707DDDDDDDDDDDDDD0000DD73
        307DDDDDDDDDDDDD0000DD783307DDDDDDDDDDDD0000DDD383307DDDDDDDDDDD
        0000DDDD38330777777DDDDD0000DDDDD38330000077DDDD0000DDDDDD383378
        88707DDD0000DDDDDDD037F8F88707DD0000DDDDDDD07FCCCCC8777D0000DDDD
        DCD0F8C8F8C880CD0000DDDDDCC08FCCCCCF80DD0000DDDDDCD0F8CFFCF880DD
        0000DDDDDDC77FCCCC8F77CD0000DDDDDDDD07F8F8F70DDD0000DDDDDDDDD07F
        8F70DDDD0000DDDDDDDDDD70007DDDDD0000DDDDDDDDDDDDDDDDDDDD0000DDDD
        DDDDDDDDDDDDDDDD0000}
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      Visible = False
      OnClick = BuscarBtnClick
    end
    object ImprimirBtn: TSpeedButton
      Left = 210
      Top = 4
      Width = 24
      Height = 24
      Hint = 'Imprimir (Ctrl+P)'
      Enabled = False
      Flat = True
      Glyph.Data = {
        4E010000424D4E01000000000000760000002800000012000000120000000100
        040000000000D800000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDD000000DDD00000000000DDDD000000DD0777777777070DDD000000D000
        000000000070DD000000D0777777FFF77000DD000000D077777799977070DD00
        0000D0000000000000770D000000D0777777777707070D000000DD0000000000
        70700D000000DDD0FFFFFFFF07070D000000DDDD0FCCCCCF0000DD000000DDDD
        0FFFFFFFF0DDDD000000DDDDD0FCCCCCF0DDDD000000DDDDD0FFFFFFFF0DDD00
        0000DDDDDD000000000DDD000000DDDDDDDDDDDDDDDDDD000000DDDDDDDDDDDD
        DDDDDD000000DDDDDDDDDDDDDDDDDD000000}
      ParentShowHint = False
      ShowHint = True
      OnClick = ImprimirBtnClick
    end
    object ImprimirFormaBtn: TSpeedButton
      Left = 234
      Top = 4
      Width = 24
      Height = 24
      Hint = 'Imprimir Forma...'
      Enabled = False
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777777770000007777747770000000077774477033333000444444700000
        003077744770F7F7F7007774777000000780777777770FFFF007770000070F88
        F077770FFF0770FFFF0700000F07770000070FFF0F07777777770F8F00077777
        77770F8F0077777777770FF00777777777770000777777777777}
      ParentShowHint = False
      ShowHint = True
      OnClick = ImprimirFormaBtnClick
    end
    object CortarBtn: TSpeedButton
      Left = 293
      Top = 4
      Width = 24
      Height = 24
      Hint = 'Cortar ( Ctrl-X)'
      Flat = True
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000B7AFCFB7AFCF
        B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF800000800000B7
        AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF
        B7AFCFB7AFCF800000B7AFCFB7AFCF800000B7AFCFB7AFCF800000800000B7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF800000B7AFCFB7AFCF80
        0000B7AFCF800000B7AFCFB7AFCF800000B7AFCFB7AFCFB7AFCFB7AFCFB7AFCF
        B7AFCFB7AFCF800000B7AFCFB7AFCF800000B7AFCF800000B7AFCFB7AFCF8000
        00B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF80000080000080
        0000B7AFCF800000B7AFCFB7AFCF800000B7AFCFB7AFCFB7AFCFB7AFCFB7AFCF
        B7AFCFB7AFCFB7AFCFB7AFCFB7AFCF800000B7AFCF800000800000800000B7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF80
        0000000000800000B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF
        B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF000000B7AFCFB7AFCFB7AFCFB7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF00
        0000000000000000B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF
        B7AFCFB7AFCFB7AFCFB7AFCFB7AFCF000000B7AFCF000000B7AFCFB7AFCFB7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF00000000
        0000B7AFCF000000000000B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF
        B7AFCFB7AFCFB7AFCFB7AFCF000000B7AFCFB7AFCFB7AFCF000000B7AFCFB7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF000000B7
        AFCFB7AFCFB7AFCF000000B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF
        B7AFCFB7AFCFB7AFCFB7AFCF000000B7AFCFB7AFCFB7AFCF000000B7AFCFB7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7
        AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF}
      ParentShowHint = False
      ShowHint = True
      OnClick = CortarBtnClick
    end
    object CopiarBtn: TSpeedButton
      Left = 317
      Top = 4
      Width = 24
      Height = 24
      Hint = 'Copiar ( Ctrl-C)'
      Flat = True
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000B7AFCFB7AFCF
        B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF80
        0000800000800000800000800000800000800000800000800000B7AFCFB7AFCF
        B7AFCFB7AFCFB7AFCFB7AFCFB7AFCF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF800000B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF80
        0000FFFFFF000000000000000000000000000000FFFFFF800000B7AFCF000000
        000000000000000000000000000000800000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF800000B7AFCF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80
        0000FFFFFF000000000000000000000000000000FFFFFF800000B7AFCF000000
        FFFFFF000000000000000000000000800000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF800000B7AFCF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80
        0000FFFFFF000000000000FFFFFF800000800000800000800000B7AFCF000000
        FFFFFF000000000000000000000000800000FFFFFFFFFFFFFFFFFFFFFFFF8000
        00DCD8E7800000B7AFCFB7AFCF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80
        0000FFFFFFFFFFFFFFFFFFFFFFFF800000800000B7AFCFB7AFCFB7AFCF000000
        FFFFFF000000000000FFFFFF0000008000008000008000008000008000008000
        00B7AFCFB7AFCFB7AFCFB7AFCF000000FFFFFFFFFFFFFFFFFFFFFFFF000000DC
        D8E7000000B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF000000
        FFFFFFFFFFFFFFFFFFFFFFFF000000000000B7AFCFB7AFCFB7AFCFB7AFCFB7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCF000000000000000000000000000000000000B7
        AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF
        B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7
        AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF}
      ParentShowHint = False
      ShowHint = True
      OnClick = CopiarBtnClick
    end
    object PegarBtn: TSpeedButton
      Left = 341
      Top = 4
      Width = 24
      Height = 24
      Hint = 'Pegar ( Ctrl-V)'
      Flat = True
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000B7AFCFB7AFCF
        B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF80000080
        0000800000800000800000800000800000800000800000800000B7AFCF000000
        000000000000000000000000800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF80000000000070609F00808070609F00808070609F800000FF
        FFFF000000000000000000000000000000000000FFFFFF800000000000008080
        70609F00808070609F008080800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF80000000000070609F00808070609F00808070609F800000FF
        FFFF000000000000000000FFFFFF800000800000800000800000000000008080
        70609F00808070609F008080800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000
        00DCD8E7800000B7AFCF00000070609F00808070609F00808070609F800000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000B7AFCFB7AFCF000000008080
        70609F00808070609F0080808000008000008000008000008000008000008000
        00000000B7AFCFB7AFCF00000070609F00808070609F00808070609F00808070
        609F00808070609F00808070609F008080000000B7AFCFB7AFCF000000008080
        70609F00000000000000000000000000000000000000000000000070609F7060
        9F000000B7AFCFB7AFCF00000070609F70609F000000B7AFCFB7AFCFB7AFCFB7
        AFCFB7AFCFB7AFCF00000070609F008080000000B7AFCFB7AFCF000000008080
        70609F00808000000000FFFF00000000000000FFFF00000070609F0080807060
        9F000000B7AFCFB7AFCFB7AFCF00000000000000000000000000000000FFFF00
        FFFF000000000000000000000000000000B7AFCFB7AFCFB7AFCFB7AFCFB7AFCF
        B7AFCFB7AFCFB7AFCF000000000000000000000000B7AFCFB7AFCFB7AFCFB7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7
        AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF}
      ParentShowHint = False
      ShowHint = True
      OnClick = PegarBtnClick
    end
    object UndoBtn: TSpeedButton
      Left = 365
      Top = 4
      Width = 24
      Height = 24
      Hint = 'Deshacer (Ctrl-Z)'
      Flat = True
      Glyph.Data = {
        76020000424D7602000000000000760000002800000040000000100000000100
        0400000000000002000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00331111133333
        333333FFFFF33333333333222223332222233333333333444443333199933333
        333333388883333333333332AAA333AAA2333333333333CCC433333199933333
        1133333888833333FF333332AAA333AAA2333334433333CCC433339919933333
        99133388F883333388F333AA2AA333AA2A2333CC433333CC4C43339133933333
        3913338F33833333388F33A233A333A33A2333C4333333C33CC4391333333333
        339138F333333333338F3A233333333333A23C433333333333C4391333333333
        339138F333333333338F3A233333333333A23C433333333333C4391333333333
        339138F333333333338F3A233333333333A23C433333333333C4391333333333
        339138F333333333338F3A233333333333A23C433333333333C4391333333333
        339138F333333333338F3A233333333333A23C433333333333C4339133333333
        3991338F33333333388F33A2333333333AA233C4333333333CC4339913333333
        99133388F333333388F333AA23333333AA2333CC43333333CC43333991333339
        913333388F3333388F33333AA233333AA233333CC433333CC433333399111119
        1333333388FFFFF8F3333333AA22222A23333333CC44444C4333333333999993
        33333333338888833333333333AAAAA33333333333CCCCC33333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 4
      ParentShowHint = False
      ShowHint = True
      OnClick = UndoBtnClick
    end
    object ExportarBtn: TZetaSpeedButton
      Left = 258
      Top = 4
      Width = 24
      Height = 24
      Flat = True
      Glyph.Data = {
        36030000424D3603000000000000360200002800000010000000100000000100
        08000000000000010000C40E0000C40E0000800000008000000049935F003A73
        4C00265D38002C52300055A45A0058966B005F9B7200336D470094C79C0066A0
        77006CA67C009DD0A700AAD6B20071AA810057825A007CB58A002C6642001E53
        3100E6F0E6004B964D00EDF5ED003D8C570032873500E4EFE400DCEADB00F9FB
        FA00417B570076AF850081BC9000EBF3EB00FDFEFD00DFECDF0004690400EFF6
        EF004D876200F4F9F400F1F7F100358C4F00FCFDFB00E2EEE300D8E7D800F6FA
        F600F3F8F3001A4D2A005CAA65006DB67500F5F9F500DCEADD001D781E00E9F2
        E90073BB7C0081AA8D00FEFEFE00558E67008AB7950064AC69006199640068B0
        6F00F8FAF800DEEBDE00F6F9F600FFFFFF00DAE9DA0047815D00E8F1E800E1ED
        E1007CC18500EAF1E900D9E8D90063B276004093470042874900FCFDFC00FEFF
        FE00579A5E000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000010101070707
        1010100202021111112B010A090906060505000000151525252B1A0A1D401217
        271F3B2F18442828252B1A0D141D431217271F3B18183E2815113F1B21141D31
        401733030303033E1511220F2A0403030303302D001302180002351C23240445
        043042001302332F00020536292E2A04161C00130238381F0002060819292E16
        082C040720122741051009082619460B2D2C47041620121705100A0B48130C32
        32000E390416201206070D0B343937374A3C2A0E0E0E0E3106071B0C341E1E26
        193A3C232421141D09010F0C3D3D491E26193A292324211409010F0C0C0B0B08
        0808361C0F1B0D0D0A011C0F1B0D0A0906053522223F1A1A0101}
      ParentShowHint = False
      ShowHint = True
      OnClick = ExportarBtnClick
    end
    object DBNavigator: TDBNavigator
      Left = 6
      Top = 4
      Width = 88
      Height = 24
      DataSource = DataSource
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
      Flat = True
      Hints.Strings = (
        'Primero'
        'Anterior'
        'Siguiente'
        'UItimo')
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
  end
  object PanelIdentifica: TPanel
    Left = 0
    Top = 32
    Width = 473
    Height = 19
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Splitter: TSplitter
      Left = 323
      Top = 0
      Height = 19
    end
    object ValorActivo1: TPanel
      Left = 0
      Top = 0
      Width = 323
      Height = 19
      Align = alLeft
      Alignment = taLeftJustify
      BorderWidth = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object textoValorActivo1: TLabel
        Left = 3
        Top = 3
        Width = 317
        Height = 13
        Align = alTop
        Caption = 'textoValorActivo1'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object ValorActivo2: TPanel
      Left = 326
      Top = 0
      Width = 147
      Height = 19
      Align = alClient
      Alignment = taRightJustify
      BorderWidth = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object textoValorActivo2: TLabel
        Left = 3
        Top = 3
        Width = 141
        Height = 13
        Align = alTop
        Alignment = taRightJustify
        Caption = 'textoValorActivo2'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object DataSource: TDataSource
    OnStateChange = DataSourceStateChange
    OnDataChange = DataSourceDataChange
    Left = 44
    Top = 65
  end
end
