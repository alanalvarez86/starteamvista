inherited BuscaEmpleadoNombre: TBuscaEmpleadoNombre
  Left = 525
  Top = 275
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'B'#250'squeda de Empleados'
  ClientHeight = 149
  ClientWidth = 310
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 113
    Width = 310
    TabOrder = 1
    inherited OK_DevEx: TcxButton
      Left = 144
      Top = 5
      Hint = 'Aceptar'
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 224
      Top = 5
      Hint = 'Cancelar'
      Cancel = True
    end
  end
  object PanelControles: TPanel [1]
    Left = 0
    Top = 0
    Width = 310
    Height = 113
    Align = alClient
    TabOrder = 0
    object ApellidoPaternoLabel: TLabel
      Left = 37
      Top = 50
      Width = 80
      Height = 13
      Caption = 'Apellido &Paterno:'
      FocusControl = ApellidoPaternoEdit
    end
    object ApellidoMaternoLabel: TLabel
      Left = 35
      Top = 77
      Width = 82
      Height = 13
      Caption = 'Apellido &Materno:'
      FocusControl = ApellidoMaternoEdit
    end
    object NombresLBL: TLabel
      Left = 66
      Top = 23
      Width = 51
      Height = 13
      Caption = '&Nombre(s):'
      FocusControl = NombresEdit
    end
    object ApellidoPaternoEdit: TEdit
      Left = 122
      Top = 46
      Width = 154
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 20
      TabOrder = 1
      OnChange = FiltraTextoEditChange
    end
    object ApellidoMaternoEdit: TEdit
      Left = 122
      Top = 73
      Width = 154
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 20
      TabOrder = 2
      OnChange = FiltraTextoEditChange
    end
    object NombresEdit: TEdit
      Left = 122
      Top = 19
      Width = 154
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 20
      TabOrder = 0
      OnChange = FiltraTextoEditChange
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 3670264
  end
  object DataSource: TDataSource
    Left = 365
    Top = 65527
  end
end
