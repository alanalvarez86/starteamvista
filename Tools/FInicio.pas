{$HINTS OFF}
unit FInicio;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ZBaseConsulta, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, TressMorado2013, dxCustomTileControl, cxClasses, dxTileControl,
  ZetaRegistryCliente, Vcl.ImgList, cxSplitter, dxScreenTip, dxCustomHint,
  cxHint, cxStyles, dxSkinscxPCPainter, cxCustomData, cxDBData, cxGridLevel,
  cxGridChartView, cxGridDBChartView, ZetaGridChart, cxGridCustomView,
  cxGrid,ThreadGraficasTRESS, ZetaCommonClasses, Vcl.Mask, ZetaFecha, dCliente,
  cxPC, dxDockControl, dxDockPanel, FireDAC.VCLUI.Controls, cxScrollBox,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, dxflchrt, Vcl.ComCtrls, ZetaClientDataSet;

type
  TInicio = class(TBaseConsulta)
    dxAccesos: TdxTileControl;
    GrupoAccesos: TdxTileControlGroup;
    Acceso01: TdxTileControlItem;
    Acceso02: TdxTileControlItem;
    Acceso10: TdxTileControlItem;
    Acceso04: TdxTileControlItem;
    Acceso05: TdxTileControlItem;
    Acceso06: TdxTileControlItem;
    Acceso07: TdxTileControlItem;
    Acceso08: TdxTileControlItem;
    Acceso09: TdxTileControlItem;
    Acceso03: TdxTileControlItem;
    Acceso11: TdxTileControlItem;
    Acceso12: TdxTileControlItem;
    PanelControl: TPanel;
    ckMostrarPantalla: TCheckBox;
    cxSplitter: TcxSplitter;
    PanelAccesos: TPanel;
    cxInformacion: TcxHintStyleController;
    PanelConfiguracion: TPanel;
    lbProcesos: TLabel;
    cxStyleGrafica: TcxStyleRepository;
    cxStyleTituloGraficas: TcxStyle;
    DataSourceGraficaRotacionPeriodo: TDataSource;
    cxStyleColorSeriesRotacion: TcxStyle;
    PanelFechaGraficaAsistencia: TPanel;
    PanelFecha: TPanel;
    FechaActiva: TLabel;
    AsistenciaFechaGrafica: TZetaFecha;
    DataSourceGraficaAsistenciaDia: TDataSource;
    ImagenesAccesos: TcxImageList;
    PanelGraficas: TPanel;
    GridPanelGraficas: TGridPanel;
    PanelGraficaRotacion: TPanel;
    GraficaRotacionPeriodo: TZetaGridChart;
    GraficaRotacionPeriodoDBChartView: TZetaDBChartView;
    GraficaRotacionPeriodoDataGroup: TcxGridDBChartDataGroup;
    GraficaRotacionPeriodoSeries: TZetaGridDBChartSeries;
    GraficaRotacionPeriodoLevel: TcxGridLevel;
    PanelGraficaAsistencia: TPanel;
    GraficaAsistenciaDia: TZetaGridChart;
    GraficaAsistenciaDiaDBChartView: TZetaDBChartView;
    GraficaAsistenciaDiaDataGroup: TcxGridDBChartDataGroup;
    GraficaAsistenciaDiaSeries: TZetaGridDBChartSeries;
    GraficaAsistenciaDiaLevel: TcxGridLevel;
    PanelIndicador2: TPanel;
    GridPanelHeadCount2: TGridPanel;
    PanelHCHeaderContorno2: TPanel;
    PanelHCHeader2: TPanel;
    lblHCTitulo2: TLabel;
    lblHCSubtitulo2: TLabel;
    PanelHCDescripcionContorno2: TPanel;
    PanelHCDescripcion2: TPanel;
    GridPanelHCDescripcion2: TGridPanel;
    PanelHCDescripcionCantidad2: TPanel;
    lblHCDescripcion2: TLabel;
    PanelHCSubDescripcion2: TPanel;
    lblHCSubDescripcionPorcentaje2: TLabel;
    lblHCSubDescripcionTexto2: TLabel;
    PanelIndicador3: TPanel;
    GridPanelHeadCount3: TGridPanel;
    PanelHCHeaderContorno3: TPanel;
    PanelHCHeader3: TPanel;
    lblHCTitulo3: TLabel;
    lblHCSubtitulo3: TLabel;
    PanelHCDescripcionContorno3: TPanel;
    PanelHCDescripcion3: TPanel;
    lblHCDescripcion3: TLabel;
    lblHCSubDescripcionPorcentaje3: TLabel;
    lblHCSubDescripcionTexto3: TLabel;
    PanelIndicador1: TPanel;
    GridPanelHeadCount1: TGridPanel;
    PanelHCHeaderContorno1: TPanel;
    PanelHCHeader1: TPanel;
    lblHCTitulo1: TLabel;
    lblHCSubtitulo1: TLabel;
    PanelHCDescripcionContorno1: TPanel;
    PanelHCDescripcion1: TPanel;
    GridPanelHCDescripcionContorno1: TGridPanel;
    PanelHCDescripcionCantidad1: TPanel;
    lblHCDescripcion: TLabel;
    PanelHCSubDescripcion1: TPanel;
    lblHCSubDescripcionPorcentaje1: TLabel;
    lblHCSubDescripcionTexto1: TLabel;
    PanelIndicador4: TPanel;
    GridPanelHeadCount4: TGridPanel;
    PanelHCHeaderContorno4: TPanel;
    PanelHCHeader4: TPanel;
    lblHCTitulo4: TLabel;
    lblHCSubtitulo4: TLabel;
    PanelHCDescripcionContorno4: TPanel;
    PanelHCDescripcionCantidad4: TPanel;
    lblHCDescripcion4: TLabel;
    lblHCSubDescripcionPorcentaje4: TLabel;
    lblHCSubDescripcionTexto4: TLabel;
    procedure FormShow(Sender: TObject);
    procedure ckMostrarPantallaClick(Sender: TObject);
    procedure dxAccesosMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure dxAccesosResize(Sender: TObject);
    {*** US 13656: Dashboard - Que el usuario pueda consultar en una ventana datos sobre RH o Nomina ***}
    procedure GraficaRotacionPeriodoAlAdquirirDatosChart(Sender: TZetaGridChart);
    procedure GraficaAsistenciaDiaAlAdquirirDatosChart(Sender: TZetaGridChart);
    procedure IniciarActualizacionGraficas( var GraficaTRESS: TZetaGridChart );
    procedure GraficarDato(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure AsistenciaFechaGraficaValidDate(Sender: TObject);
    procedure HabilitarControl( const lHabilitar: Boolean );
    procedure MostrarGraficaDatosThread;
    procedure InicializarThread;
    procedure dxAccesosMouseLeave(Sender: TObject);
    {*** FIN ***}
  private
    { Private declarations }
    iTotal    : integer;
    {*** US 13656: Dashboard - Que el usuario pueda consultar en una ventana datos sobre RH o Nomina ***}
    GraficarDatosThread : TThreadGraficasTRESS;
    FCambioFechaAsistencia: Boolean;
    procedure ActualizarMensajeGrafica( var GraficaTRESS: TZetaGridChart; const sMensaje: String );
    procedure GenerarInformacionIndicadores(Sender: TObject);
    procedure LlenarInformacionGridPanelHeadCount( TituloIndicadores, SubTituloIndicadores, DescripcionIndicadores, SubDescripcionPorcentaje, SubDescripcionTexto : TLabel; Sender: TObject );
    procedure LimpiarIndicadores( TituloIndicadores, SubTituloIndicadores, DescripcionIndicadores, SubDescripcionPorcentaje, SubDescripcionTexto : TLabel);
    procedure InicializarIndicadores;
    procedure TerminarThread( var hThreadGrafica: TThreadGraficasTRESS );
    procedure NotificacionErroresGraficas( const sMensaje: String; var GraficaTRESS: TZetaGridChart );
    procedure TerminarActualizacionGraficas( var GraficaTRESS: TZetaGridChart; var GraficaTRESSDBChartView : TZetaDBChartView; var DataSourceGraficaTRESS: TDataSource; oDataSource: TZetaClientDataSet; const sMensaje: String );
    {*** FIN ***}
    procedure OnItemClick(Sender: TdxTileControlItem);
    procedure ObtenerAccesos;
   protected
    procedure Connect; override;
    procedure Refresh; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    //function PuedeRefrescar( var sMensaje: String ): Boolean; override;
  public
    { Public declarations }
    FItem: TdxTileControlItem;
  end;


const
     dxItems = 12;
     K_NO_HAY_DATOS_GRAFICA = 'No hay datos para mostrar';
     tColores : array [0..7] of string = (
              '$00F3F3F3', //Gris de Background
              '$004B3B4D', //Gris de Titulos
              '$20D6D6D6', //Borde Gris para Tiles
              '$20AA8B99', //Borde Morado para Tiles
              '$00262ECD', //Indicador color Rojo pos4
              '$008EB11A', //Indicador Color Verde pos5
              '$002BFFFF', //Indicador Color Amarillo pos6
              '$007F7F7F'  //Indicador Color Gris pos7
              );

var
   Inicio: TInicio;

implementation

uses FTressShell, ZetaDialogo, ZAccesosTress, ZAccesosMgr, ZetaCommonTools, ZetaClientTools;

{$R *.dfm}

procedure TInicio.ckMostrarPantallaClick(Sender: TObject);
begin
     inherited;
     ClientRegistry.HomeSistema := ckMostrarPantalla.Checked;
end;

procedure TInicio.Connect;
begin
     if ( dmCliente.CambioValoresActivosGraficas ) or ( TressShell.ActualizarGraficasInicio ) then
     begin
         AsistenciaFechaGrafica.Valor := dmCliente.FechaAsistencia;
         {*** US 13656: Dashboard - Que el usuario pueda consultar en una ventana datos sobre RH o Nomina ***}
         MostrarGraficaDatosThread;
     end;
     {*** US 14266: Las gr�ficas se refrescan cada vez que cambia el empleado activo ***}
     TressShell.ActualizarGraficasInicio := False;
     dmCliente.RespaldaValoresActivosGraficas;
end;

procedure TInicio.dxAccesosMouseLeave(Sender: TObject);
begin
     inherited;
     cxInformacion.HideHint;
end;

procedure TInicio.dxAccesosMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
     inherited;
     if dxAccesos.ActiveHitTest.Item <> nil then
     begin
          if (FItem = nil) or (FItem <> dxAccesos.ActiveHitTest.Item) then
          begin
               cxInformacion.ShowHint(Mouse.CursorPos.X-20, Mouse.CursorPos.Y+50, '',
               dxAccesos.ActiveHitTest.Item.Text1.Value);
          end;
     end
     else
         cxInformacion.HideHint;
end;

procedure TInicio.dxAccesosResize(Sender: TObject);
var
   i: integer;
begin
     inherited;
     if PanelAccesos.Width < 656 then
     begin
          dxAccesos.OptionsView.GroupMaxRowCount := 4;

          if (iTotal <= dxItems) and (iTotal > 8) then
          begin
               for i := (iTotal) Downto 8+1 do
                   dxAccesos.Items[i-1].Visible := False;
          end;
     end
     else
     begin
          dxAccesos.OptionsView.GroupMaxRowCount := 3;
          if (iTotal <= dxItems) and (iTotal > 8) then
          begin
               for i := (iTotal) Downto 8+1 do
                   dxAccesos.Items[i-1].Visible := True;
          end;
     end
end;

procedure TInicio.Refresh;
begin
     AsistenciaFechaGrafica.Valor := dmCliente.FechaAsistencia;
     {*** US 13656: Dashboard - Que el usuario pueda consultar en una ventana datos sobre RH o Nomina ***}
     MostrarGraficaDatosThread;
end;

procedure TInicio.FormCreate(Sender: TObject);
begin
     inherited;
     PanelAccesos.Color := StringToColor(tColores[0]);
     PanelGraficas.Color := StringToColor(tColores[0]);
     GridPanelGraficas.Color := StringToColor(tColores[0]);
     PanelHCDescripcionContorno1.Color := StringToColor(tColores[0]);
     PanelHCDescripcionContorno2.Color := StringToColor(tColores[0]);
     PanelHCDescripcionContorno3.Color := StringToColor(tColores[0]);
     PanelHCDescripcionContorno4.Color := StringToColor(tColores[0]);
     Self.Color := StringToColor(tColores[0]);
     PanelIndicador1.BevelOuter := bvNone;
     PanelIndicador2.BevelOuter := bvNone;
     PanelIndicador3.BevelOuter := bvNone;
     PanelIndicador4.BevelOuter := bvNone;
     PanelControl.Color := StringToColor(tColores[0]);
     PanelFechaGraficaAsistencia.Color := StringToColor(tColores[0]);
     PanelConfiguracion.Color := StringToColor(tColores[0]);
     cxSplitter.Color := StringToColor(tColores[0]);
     lbProcesos.Font.Color := StringToColor(tColores[1]);
     dxAccesos.Style.FocusedColor := StringToColor(tColores[3]);
     FCambioFechaAsistencia := False;
     InicializarIndicadores;
     AsistenciaFechaGrafica.Valor := dmCliente.FechaAsistencia;
end;

procedure TInicio.FormDestroy(Sender: TObject);
begin
     inherited;
     {*** US 13656: Dashboard - Que el usuario pueda consultar en una ventana datos sobre RH o Nomina ***}
     TerminarThread( GraficarDatosThread );
     {*** FIN ***}
end;

procedure TInicio.FormShow(Sender: TObject);
begin
     inherited;
     dxAccesos.OptionsBehavior.ItemMoving := False; //Remueve el DragAndDrop
     dxAccesos.LookAndFeel.SkinName := 'TressMorado2013'; //Ajusta el Skin
     ckMostrarPantalla.Checked := ClientRegistry.HomeSistema;

     PanelAccesos.Visible := Revisa( D_INICIO ) and ( lstTopAccesosDirectos.Count > 0 );
     PanelGraficas.Visible := Revisa( D_MOSTRAR_GRAFICAS );
     cxSplitter.Visible := ( PanelAccesos.Visible and PanelGraficas.Visible );
     PanelConfiguracion.Visible := ( not PanelAccesos.Visible ) and ( not PanelGraficas.Visible ); // Sin botones o graficas

     if PanelConfiguracion.Visible then   // Mostrar panel de configuraci�n
     begin
          PanelConfiguracion.Align := AlClient;
          PanelConfiguracion.Caption := 'No tiene la configuraci�n necesaria para utilizar esta herramienta.';
     end
     else
     begin
          ObtenerAccesos;
          if PanelAccesos.Visible then
          begin
               dxAccesos.Focused;
               if ( not PanelGraficas.Visible ) then
                  PanelAccesos.Align := alClient;
          end
          else
          begin

               PanelGraficas.Focused;
          end;
     end;
end;

{*** US 13656: Dashboard - Que el usuario pueda consultar en una ventana datos sobre RH o Nomina ***}
procedure TInicio.GraficaAsistenciaDiaAlAdquirirDatosChart(Sender: TZetaGridChart);
begin
     IniciarActualizacionGraficas( Sender );
end;

procedure TInicio.GraficaRotacionPeriodoAlAdquirirDatosChart(Sender: TZetaGridChart);
begin
     IniciarActualizacionGraficas( Sender );
end;

procedure TInicio.IniciarActualizacionGraficas( var GraficaTRESS: TZetaGridChart );
begin
     GraficaTRESS.LockedStateImageOptions.ShowText := TRUE;
     GraficaTRESS.Enabled := False;
     GraficaTRESS.BeginUpdate(lsimImmediate);
end;

procedure TInicio.GraficarDato(Sender: TObject);
begin
     GraficaRotacionPeriodo.Graficar;
     GraficaAsistenciaDia.Graficar;
     TerminarActualizacionGraficas( GraficaRotacionPeriodo, GraficaRotacionPeriodoDBChartView, DataSourceGraficaRotacionPeriodo, TThreadGraficasTRESS( Sender ).DatosGraficaRotacion, TThreadGraficasTRESS( Sender ).ErroresRotacion );
     TerminarActualizacionGraficas( GraficaAsistenciaDia, GraficaAsistenciaDiaDBChartView, DataSourceGraficaAsistenciaDia, TThreadGraficasTRESS( Sender ).DatosGraficaAsistencia, TThreadGraficasTRESS( Sender ).ErroresAsistencia );
     GenerarInformacionIndicadores( Sender );
end;

procedure TInicio.NotificacionErroresGraficas( const sMensaje: String; var GraficaTRESS: TZetaGridChart );
begin
     if sMensaje <> VACIO then
        ActualizarMensajeGrafica( GraficaTRESS, sMensaje )
     else
         ActualizarMensajeGrafica( GraficaTRESS, K_NO_HAY_DATOS_GRAFICA );
end;

procedure TInicio.TerminarActualizacionGraficas( var GraficaTRESS: TZetaGridChart; var GraficaTRESSDBChartView : TZetaDBChartView; var DataSourceGraficaTRESS: TDataSource; oDataSource: TZetaClientDataSet; const sMensaje: String );
begin
     if ( oDataSource <> nil ) then
     begin
          DataSourceGraficaTRESS.dataset := oDataSource;
          GraficaTRESSDBChartView.DataController.BeginUpdate;
          GraficaTRESSDBChartView.DataController.DataSource := DataSourceGraficaTRESS;
          GraficaTRESSDBChartView.DataController.EndUpdate;
          GraficaTRESS.EndUpdate;
          GraficaTRESS.Enabled := True;
     end
     else
     begin
          NotificacionErroresGraficas( sMensaje, GraficaTRESS );
     end;
end;

procedure TInicio.ActualizarMensajeGrafica( var GraficaTRESS: TZetaGridChart; const sMensaje: String );
begin
     GraficaTRESS.LockedStateImageOptions.Text := sMensaje;
     GraficaTRESS.LockedStateImageOptions.ShowText := TRUE;
     GraficaTRESS.Enabled := False;
     GraficaTRESS.BeginUpdate(lsimImmediate);
end;

procedure TInicio.TerminarThread( var hThreadGrafica: TThreadGraficasTRESS );
begin
     if Assigned( hThreadGrafica ) and ( hThreadGrafica.StatusTerminate ) then
     begin
          hThreadGrafica.WaitFor;
          FreeAndNil( hThreadGrafica );
     end
     else if Assigned( hThreadGrafica ) and ( not hThreadGrafica.StatusTerminate ) then
     begin
          hThreadGrafica.Terminate;
          TerminateThread( hThreadGrafica.Handle, THREAD_PRIORITY_NORMAL );
          hThreadGrafica.WaitFor;
          FreeAndNil( hThreadGrafica );
     end;
end;

procedure TInicio.AsistenciaFechaGraficaValidDate(Sender: TObject);
begin
     {*** US 14266: Las gr�ficas se refrescan cada vez que cambia el empleado activo ***}
     TressShell.ActualizarGraficasInicio := True;
     if FCambioFechaAsistencia then
     begin
          MostrarGraficaDatosThread;
     end;
     if not FCambioFechaAsistencia then FCambioFechaAsistencia := True;
     TressShell.AsistenciaFechavalid(AsistenciaFechaGrafica.Valor);
end;

procedure TInicio.HabilitarControl( const lHabilitar: Boolean );
begin
     try
        AsistenciaFechaGrafica.Enabled := lHabilitar;
     except
           on Error: Exception do
           begin
                ShowMessage(Error.Message);
           end;
     end;
end;

procedure TInicio.MostrarGraficaDatosThread;
begin
     if Revisa( D_MOSTRAR_GRAFICAS ) then
     begin
          if Assigned( GraficarDatosThread ) and ( GraficarDatosThread.StatusTerminate ) then
          begin
               InicializarThread;
          end;
          if not Assigned( GraficarDatosThread ) then
          begin
               InicializarThread;
          end;
     end;
end;

procedure TInicio.InicializarThread;
const
     K_QUERY_INDICADORES = 'SP_Dashlet_GetIndicadores @FechaAsistencia, @PeriodoActivoTipo, @PeriodoActivoNumero, @PeriodoActivoYear, @Confidencialidad';
begin
     GraficarDatosThread := TThreadGraficasTRESS.Create( True,False );
     GraficarDatosThread.QueryGraficaRotacion := GraficaRotacionPeriodo.Query;
     GraficarDatosThread.QueryGraficaAsistencia := GraficaAsistenciaDia.Query;
     GraficarDatosThread.QueryIndicadores := K_QUERY_INDICADORES;
     GraficarDatosThread.FechaAsistenciaControl := AsistenciaFechaGrafica.Valor;
     GraficarDatosThread.OnTerminate := GraficarDato;
     GraficarDatosThread.Start;
end;

procedure TInicio.GenerarInformacionIndicadores( Sender: TObject );
const
     K_INDICADOR_EMP_ACTIVOS = 1;
     K_INDICADOR_CON_RENOVAR = 2;
     K_INDICADOR_PEN_TIMBRAR = 3;
     K_INDICADOR_ASI_MOMENTO = 4;
begin
     try
        InicializarIndicadores;
        if TThreadGraficasTRESS( Sender ).DatosGraficaIndicadores <> nil then
        begin
             with TThreadGraficasTRESS( Sender ).DatosGraficaIndicadores do
             begin
                  First;
                  while ( not EOF ) do
                  begin
                       if TThreadGraficasTRESS( Sender ).DatosGraficaIndicadores.FieldByName( 'IndicadorID' ).AsInteger = K_INDICADOR_EMP_ACTIVOS then
                          LlenarInformacionGridPanelHeadCount( lblHCTitulo1, lblHCSubtitulo1, lblHCDescripcion, lblHCSubDescripcionPorcentaje1, lblHCSubDescripcionTexto1, Sender );
                       if TThreadGraficasTRESS( Sender ).DatosGraficaIndicadores.FieldByName( 'IndicadorID' ).AsInteger = K_INDICADOR_CON_RENOVAR then
                          LlenarInformacionGridPanelHeadCount( lblHCTitulo2, lblHCSubtitulo2, lblHCDescripcion2, lblHCSubDescripcionPorcentaje2, lblHCSubDescripcionTexto2, Sender );
                       if TThreadGraficasTRESS( Sender ).DatosGraficaIndicadores.FieldByName( 'IndicadorID' ).AsInteger = K_INDICADOR_PEN_TIMBRAR then
                          LlenarInformacionGridPanelHeadCount( lblHCTitulo3, lblHCSubtitulo3, lblHCDescripcion3, lblHCSubDescripcionPorcentaje3, lblHCSubDescripcionTexto3, Sender );
                       if TThreadGraficasTRESS( Sender ).DatosGraficaIndicadores.FieldByName( 'IndicadorID' ).AsInteger = K_INDICADOR_ASI_MOMENTO then
                          LlenarInformacionGridPanelHeadCount( lblHCTitulo4, lblHCSubtitulo4, lblHCDescripcion4, lblHCSubDescripcionPorcentaje4, lblHCSubDescripcionTexto4, Sender );
                       Next;
                  end;
             end;
        end;
     except
           on Error: Exception do
           begin
                ZetaDialogo.ZError( self.Caption, Error.Message, 0 );
           end;
     end;
end;

procedure TInicio.LlenarInformacionGridPanelHeadCount( TituloIndicadores, SubTituloIndicadores, DescripcionIndicadores, SubDescripcionPorcentaje, SubDescripcionTexto : TLabel; Sender: TObject );
const
     K_VERDE = 'verde';
     K_ROJO = 'rojo';
     K_AMARILLO = 'amarillo';
     K_ESPACIO = ' ';
     K_LEFT_DEFAULT = 10;
var
   iWidthSubDescripcionPorcentaje: Integer;
begin
     TituloIndicadores.Caption := TThreadGraficasTRESS( Sender ).DatosGraficaIndicadores.FieldByName( 'IndicadorTitulo' ).AsString;
     SubTituloIndicadores.Caption := TThreadGraficasTRESS( Sender ).DatosGraficaIndicadores.FieldByName( 'IndicadorSubTitulo' ).AsString;
     DescripcionIndicadores.Caption := TThreadGraficasTRESS( Sender ).DatosGraficaIndicadores.FieldByName( 'IndicadorValor' ).AsString;
     SubDescripcionPorcentaje.Caption := TThreadGraficasTRESS( Sender ).DatosGraficaIndicadores.FieldByName( 'Tendencia' ).AsString;;
     SubDescripcionTexto.Caption := TThreadGraficasTRESS( Sender ).DatosGraficaIndicadores.FieldByName( 'TendenciaDescripcion' ).AsString;
     SubDescripcionPorcentaje.Left := K_LEFT_DEFAULT;
     //Asinar Color
     if TThreadGraficasTRESS( Sender ).DatosGraficaIndicadores.FieldByName( 'Tendencia' ).AsString <> VACIO then
     begin
          iWidthSubDescripcionPorcentaje := trunc( Canvas.TextWidth(SubDescripcionPorcentaje.Caption + K_ESPACIO ) ) + 8;
          SubDescripcionPorcentaje.Width := iWidthSubDescripcionPorcentaje;
          SubDescripcionTexto.Left := K_LEFT_DEFAULT + iWidthSubDescripcionPorcentaje + 4;
          if TThreadGraficasTRESS( Sender ).DatosGraficaIndicadores.FieldByName( 'TendenciaColor' ).AsString = K_ROJO then
             SubDescripcionPorcentaje.Font.Color := StringToColor(tColores[4])
          else
          if TThreadGraficasTRESS( Sender ).DatosGraficaIndicadores.FieldByName( 'TendenciaColor' ).AsString = K_VERDE then
             SubDescripcionPorcentaje.Font.Color := StringToColor(tColores[5])
          else
          if TThreadGraficasTRESS( Sender ).DatosGraficaIndicadores.FieldByName( 'TendenciaColor' ).AsString = K_AMARILLO then
             SubDescripcionPorcentaje.Font.Color := StringToColor(tColores[6])
          else
              SubDescripcionPorcentaje.Font.Color := StringToColor(tColores[7]);
     end
     else
          SubDescripcionTexto.Left := SubDescripcionPorcentaje.Left;
end;

procedure TInicio.InicializarIndicadores;
begin
     LimpiarIndicadores( lblHCTitulo1, lblHCSubtitulo1, lblHCDescripcion, lblHCSubDescripcionPorcentaje1, lblHCSubDescripcionTexto1 );
     LimpiarIndicadores( lblHCTitulo2, lblHCSubtitulo2, lblHCDescripcion2, lblHCSubDescripcionPorcentaje2, lblHCSubDescripcionTexto2 );
     LimpiarIndicadores( lblHCTitulo3, lblHCSubtitulo3, lblHCDescripcion3, lblHCSubDescripcionPorcentaje3, lblHCSubDescripcionTexto3 );
     LimpiarIndicadores( lblHCTitulo4, lblHCSubtitulo4, lblHCDescripcion4, lblHCSubDescripcionPorcentaje4, lblHCSubDescripcionTexto4 );
end;

procedure TInicio.LimpiarIndicadores( TituloIndicadores, SubTituloIndicadores, DescripcionIndicadores, SubDescripcionPorcentaje, SubDescripcionTexto : TLabel );
begin
     TituloIndicadores.Caption := VACIO;
     SubTituloIndicadores.Caption := VACIO;
     DescripcionIndicadores.Caption := VACIO;
     SubDescripcionPorcentaje.Caption := VACIO;
     SubDescripcionTexto.Caption := VACIO;
end;

{*** FIN ***}

procedure TInicio.OnItemClick(Sender: TdxTileControlItem);
begin
     if (TressShell.DevEx_BarManager.Items[Sender.Tag].Enabled) then
          TressShell.DevEx_BarManager.Items[Sender.Tag].OnClick(Self)
     else
          ZetaDialogo.ZError( self.Caption, 'No tiene derecho de consulta sobre esta opci�n.', 0 );
end;


procedure TInicio.ObtenerAccesos;
const
     K_NUM_SECCIONES = 2;
var
   AccesoSeccion  : TStringList;
   i, iImgTemp : integer;
   sEtiqueta              : string;
begin
     AccesoSeccion := TStringList.Create;

     try
        dxAccesos.Images := ImagenesAccesos; //Establece el origen de las imagenes

        for i := 0 to lstTopAccesosDirectos.Count-1 do
        begin
                  AccesoSeccion.Clear;
                  if SplitList(':', lstTopAccesosDirectos[i], AccesoSeccion) then
                  begin
                       if (AccesoSeccion.Count = K_NUM_SECCIONES) then
                       begin
                            dxAccesos.Items[i].Visible := True;
                            iImgTemp := TressShell.DevEx_BarManager.Items[(StrToInt(AccesoSeccion[0]))].Tag;
                            if ( iImgTemp > ImagenesAccesos.Count -1 ) then
                                iImgTemp := -1 ;

                            dxAccesos.Items[i].Glyph.ImageIndex := iImgTemp;
                            dxAccesos.Items[i].Style.Gradient := gmHorizontal;
                            dxAccesos.Items[i].Style.GradientBeginColor := clWhite;
                            dxAccesos.Items[i].Style.GradientEndColor := clWhite ;
                            dxAccesos.Items[i].Style.BorderColor := StringToColor(tColores[2]);
                            if StrLleno(TressShell.DevEx_BarManager.Items[StrToInt(AccesoSeccion[0])].Description) then
                               sEtiqueta := TressShell.DevEx_BarManager.Items[StrToInt(AccesoSeccion[0])].Description
                            else
                                sEtiqueta := StringReplace (TressShell.DevEx_BarManager.Items[StrToInt(AccesoSeccion[0])].Caption, '&', '',  [rfReplaceAll, rfIgnoreCase]);
                            dxAccesos.Items[i].Text1.Value := sEtiqueta;
                            dxAccesos.Items[i].Text1.Align := oaBottomCenter;
                            dxAccesos.Items[i].Text1.WordWrap := False;
                            dxAccesos.Items[i].Style.Font.Color := clBlack;
                            dxAccesos.Items[i].Tag := StrToInt(AccesoSeccion[0]);
                            dxAccesos.Items[i].OnClick := OnItemClick;
                       end;
                       iTotal := i+1;
                  end;
        end;
    finally
           FreeAndNil(AccesoSeccion);
    end
end;

function TInicio.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede agregar en esta pantalla';
     Result := False;
end;

function TInicio.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede borrar en esta pantalla';
     Result := False;
end;

function TInicio.PuedeModificar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede modificar en esta pantalla';
     Result := False;
end;

end.
