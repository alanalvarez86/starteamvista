unit ZBaseSelectGrid_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons,
  ZetaClientDataSet,
  ExtCtrls, ZBasicoSelectGrid_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, cxControls, cxStyles, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid, cxButtons,
  dxSkinsDefaultPainters;

type
  TBaseGridSelect_DevEx = class(TBasicoGridSelect_DevEx)
    PanelSuperior: TPanel;
    PistaLBL: TLabel;
    Pista: TEdit;
    BtnFiltrar: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure PistaChange(Sender: TObject);
    procedure PistaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BtnFiltrarClick(Sender: TObject);
  private
    { Private declarations }
    function Llave: String;
    function GetFiltroDataset: String;
    function GetFiltroPista: String;
    procedure SetFilter;
  protected
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
    { Public declarations }
  end;

var
  BaseGridSelect_DevEx: TBaseGridSelect_DevEx;

function GridSelect( ZetaDataset: TZetaClientDataset; GridSelectClass: TBasicoGridSelectClass;
                     ValidarCambios: Boolean = TRUE ): Boolean;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo;

{$R *.DFM}

function GridSelect( ZetaDataset: TZetaClientDataset; GridSelectClass: TBasicoGridSelectClass;
                     ValidarCambios: Boolean = TRUE ): Boolean;
begin
     Result := ZBasicoSelectGrid_DevEx.GridSelectBasico( ZetaDataset, GridSelectClass, ValidarCambios );
end;

procedure TBaseGridSelect_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Pista.Clear;
     SetFilter;
end;

function TBaseGridSelect_DevEx.Llave: String;
begin
     Result := Pista.Text;
end;

function TBaseGridSelect_DevEx.GetFiltroDataset: String;
begin
     if ZetaCommonTools.strVacio( Filtro ) then
        Result := ZetaCommonClasses.VACIO
     else
         Result := ZetaCommonTools.Parentesis( Filtro );
end;

function TBaseGridSelect_DevEx.GetFiltroPista: String;
var
   iEmpleado : Integer;
begin
     Result := ZetaCommonClasses.VACIO;
     if ZetaCommonTools.strLleno( Llave ) then
     begin
          iEmpleado := StrToIntDef( Llave, 0 );
          if ( iEmpleado > 0 ) then
          begin
               if Assigned( DataSet.FindField( 'CB_CODIGO' ) ) then
                  Result := Format( '( CB_CODIGO = %d )', [ iEmpleado ] )
               else
                   ZetaDialogo.ZError( self.Caption, 'No Se Puede Filtrar Por Empleado - DataSet Inv�lido', 0 );
          end
          else
          begin
               if Assigned( DataSet.FindField( 'PRETTYNAME' ) ) then
                  Result := Format( '( Upper( PRETTYNAME ) like %s )', [ EntreComillas( '%' + Llave + '%' ) ] )
               else
                   ZetaDialogo.ZError( self.Caption, 'No Se Puede Filtrar Por Nombre - DataSet Inv�lido', 0 );
          end;
     end;
end;

procedure TBaseGridSelect_DevEx.SetFilter;
var
   Pos : TBookMark;
begin
     with Dataset do
     begin
          DisableControls;
          try
             if strLleno( Filtro ) or strLleno( Llave ) then
             begin
                  Pos:= GetBookMark;
                  ZetaDBGridDBTableview.Controller.ClearSelection;  // Se hacen invalidos los bookmarks cuando se filtra el dataset
                  Filtered := False;
                  Filter := ZetaCommonTools.ConcatFiltros( GetFiltroDataset, GetFiltroPista );
                  Filtered := True;
                  if ( Pos <> nil ) then
                  begin
                       if BookMarkValid( Pos ) then
                          GotoBookMark( Pos );
                       FreeBookMark( Pos );
                  end;
             end
             else if Filtered then
             begin
                  Pos:= GetBookMark;
                  Filtered := False;
                  Filter := ZetaCommonClasses.VACIO;
                  if ( Pos <> nil ) then
                  begin
                       if BookMarkValid( Pos ) then
                          GotoBookMark( Pos );
                       FreeBookMark( Pos );
                  end;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TBaseGridSelect_DevEx.BtnFiltrarClick(Sender: TObject);
begin
     inherited;
     SetFilter;
end;

procedure TBaseGridSelect_DevEx.PistaChange(Sender: TObject);
begin
     inherited;
     if ZetaCommonTools.StrVacio( Llave ) then
     begin
          SetFilter;
          BtnFiltrar.Enabled := False;
     end
     else
          BtnFiltrar.Enabled := True;
end;

procedure TBaseGridSelect_DevEx.PistaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     inherited;
   { with ZetaDBGrid do
     begin
          case Key of
               VK_PRIOR: Perform( WM_KEYDOWN, VK_PRIOR, 0 );   // PgUp //
               VK_NEXT: Perform( WM_KEYDOWN, VK_NEXT, 0 );   // PgDn //
               VK_UP: Perform( WM_KEYDOWN, VK_UP, 0 );   //Up Arrow //
               VK_DOWN: Perform( WM_KEYDOWN, VK_DOWN, 0 );   //Down Arrow //
          end;
     end;}
end;

procedure TBaseGridSelect_DevEx.KeyPress(var Key: Char);
begin
     if ( ActiveControl = Pista ) then
     begin
          if ( Key = Chr( VK_RETURN ) ) then
          begin
               Key := #0;
               if self.BtnFiltrar.Enabled then
                  self.BtnFiltrar.Click
               else
                   ZetaDBGrid.SetFocus;
          end;
     end
     else
         inherited KeyPress( Key );
end;

end.
