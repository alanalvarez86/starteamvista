unit ZetaEscogeCia_DevEx;

interface

{$ifndef DOS_CAPAS}
{$define HTTP_CONNECTION}
{$endif}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Mask, Buttons, ExtCtrls,
     {$ifndef VER130}MaskUtils,{$endif}
     cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, dxLayoutContainer, dxLayoutControl,
  dxLayoutControlAdapters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid,
  ZBaseShell, ZetaCommonLists, ZetaCommonClasses, ImgList, cxTextEdit,
  cxContainer, cxCheckBox, cxLabel, Menus, cxButtons, dxGDIPlusClasses,
  dxLayoutcxEditAdapters, jpeg, TressMorado2013, ZBaseDlgModal_DevEx;

type
  TcxViewInfoAcess = class(TcxGridTableDataCellViewInfo);
  TcxPainterAccess = class(TcxGridTableDataCellPainter);

type
  TEscogeCia_DevEx = class(TZetaDlgModal_DevEx)
    LayoutControl_DevExGroup_Root: TdxLayoutGroup;
    LayoutControl_DevEx: TdxLayoutControl;
    GridEmpresasDBTableView: TcxGridDBTableView;
    GridEmpresasLevel: TcxGridLevel;
    GridEmpresas: TZetaCXGrid;
    LayoutControl_DevExItem2: TdxLayoutItem;
    dsEmpresas: TDataSource;
    LayoutControl_DevExBody: TdxLayoutGroup;
    CM_NOMBRE: TcxGridDBColumn;
    CM_TIPO_DESCRIP: TcxGridDBColumn;
    CM_CTRL_RL: TcxGridDBColumn;
    cxGridImageList: TcxImageList;
    PanelOpcionesGrid: TPanel;
    LayoutControl_DevExItem3: TdxLayoutItem;
    cxlblBuscar: TcxLabel;
    cxTEditBusca: TcxTextEdit;
    cxCkbEmpresasDemo: TcxCheckBox;
    LayoutControl_DevExHeaderForm: TdxLayoutGroup;
    LayoutControl_DevExItem6: TdxLayoutItem;
    ImageSistemaTRESS: TImage;
    LayoutControl_DevExDatosUsuario: TdxLayoutGroup;
    LayoutControl_DevExAutorizacion: TdxLayoutGroup;
    ImageUsuario: TImage;
    cxDemo: TcxButton;
    LayoutControl_DevExItemBotonDemo: TdxLayoutItem;
    LayoutControl_DevExGroupDatos: TdxLayoutGroup;
    LayoutControl_DevExGroupImage: TdxLayoutGroup;
    LayoutControl_DevExItem5: TdxLayoutItem;
    cxlblGruposUsuario: TcxLabel;
    LayoutControl_DevExItem1: TdxLayoutItem;
    cxlblNombreUsuario: TcxLabel;
    LayoutControl_DevExItem7: TdxLayoutItem;
    cxlblUltimaEntrada: TcxLabel;
    LayoutControl_DevExItem9: TdxLayoutItem;
    cxAutorizacion: TcxLabel;
    LayoutControl_DevExItemAutorizacion: TdxLayoutItem;
    LayoutControl_DevExGroup1: TdxLayoutGroup;
    cxBtnHelp: TcxButton;
    cxlblVersion: TcxLabel;
    LayoutControl_DevExItem8: TdxLayoutItem;
    cxlblVersionInfo: TcxLabel;
    LayoutControl_DevExItem10: TdxLayoutItem;
    LayoutControl_DevExGroup2: TdxLayoutGroup;
    procedure FormShow(Sender: TObject);
    procedure GridEmpresasDBTableViewDblClick(Sender: TObject);
    procedure CM_TIPO_DESCRIPCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure cxTEditBuscaPropertiesChange(Sender: TObject);
    procedure cxCkbEmpresasDemoClick(Sender: TObject);
    procedure cxDemoClick(Sender: TObject);
    procedure cxBtnHelpClick(Sender: TObject);
    procedure CM_TIPO_DESCRIPHeaderClick(Sender: TObject);
    procedure cxCkbEmpresasDemoPropertiesChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GridEmpresasDBTableViewKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    FEsDemo: Boolean;
    FEsKit: Boolean;
    procedure FiltraEmpresasPrueba;
    procedure FiltraEmpresas;
    function NoHayDatosGridEmpresas: Boolean;
    procedure EligeBotones;
    procedure SeleccionaEmpresa;
  public
    { Public declarations }
    //DevEx: 
    property SetEsDemo: Boolean write FEsDemo;
    property SetEsKit: Boolean write FEsKit;
    //
  end;

  {function TReportes_DevEx.NoHayDatosGridFavoritos: Boolean;
begin
     if ZetaDBGridDBTableView_Favoritos.DataController.RowCount<=0 then
        Result := True
     else
         Result := False;
end;}

var
   EscogeCia_DevEx: TEscogeCia_DevEx;

function EscogeUnaEmpresa_DevEx( EsDemo, EsKit :Boolean ; const sEmpresaDef: String = ZetaCommonClasses.VACIO ): Boolean;

implementation

uses
    //ZetaRegistryCliente,  //Se comento porque no se usa
    //FBuscaServidor,       //Se comento porque no se usa
    //FTressShell, //BORRAR
    DCliente,
    ZetaCommonTools,
    ZetaClientTools,
    FEntradaDemoKit,
    ZetaDialogo,
    StrUtils;

{$R *.DFM}
//Recibe parametro de si es demo o no y lo asigna a una variable privada de la forma
//Despues en el FormShow se configura la forma segun sea el caso.

{$ifdef TRESSEXCEL}
function EscogeUnaEmpresa_DevEx( EsDemo, EsKit :Boolean; const sEmpresaDef: String = ZetaCommonClasses.VACIO  ): Boolean;
var
    sOldEmpresa : String;
begin
      sOldEmpresa := dmCliente.Compania;
      if ( not dmCliente.EmpresaAbierta ) and ZetaCommonTools.strLleno( sEmpresaDef ) then
        dmCliente.FindCompany( sEmpresaDef );

        if EscogeCia_DevEx = nil then
        begin
              EscogeCia_DevEx := TEscogeCia_DevEx.Create( Application );
        end;
        with EscogeCia_DevEx do
        begin
             try
                 SetEsDemo := EsDemo;
                 SetEsKit := EsKit;
                 dsEmpresas.DataSet := dmCliente.cdsCompany;
                 ShowModal;
                 Result := ( ModalResult = mrOk );
             finally
                free;
             end;
        end;
        EscogeCia_DevEx  := nil;

        if ( not Result ) then
           dmCliente.FindCompany( sOldEmpresa );
end;
{$else}

function EscogeUnaEmpresa_DevEx( EsDemo, EsKit :Boolean; const sEmpresaDef: String = ZetaCommonClasses.VACIO  ): Boolean;
var
    sOldEmpresa : String;
begin
     sOldEmpresa := dmCliente.Compania;
     if ( not dmCliente.EmpresaAbierta ) and ZetaCommonTools.strLleno( sEmpresaDef ) then
        dmCliente.FindCompany( sEmpresaDef );
     EscogeCia_DevEx := TEscogeCia_DevEx.Create( Application );
     //Validar si es Demo o Kit
     EscogeCia_DevEx.SetEsDemo := EsDemo;
     EscogeCia_DevEx.SetEsKit := EsKit;
     try
        with EscogeCia_DevEx do
        begin
             //dmCliente.FindCompany(sOldEmpresa);
             dsEmpresas.DataSet := dmCliente.cdsCompany;
             //dmCliente.cdsCompany.Filtered := False;
             ShowModal;
             Result := ( ModalResult = mrOk );
        end;
     finally
            EscogeCia_DevEx.Free;
     end;
     if ( not Result ) then
        dmCliente.FindCompany( sOldEmpresa );

end;

{$endif}

{ ********* TZUserLogin ********* }

procedure TEscogeCia_DevEx.FormShow(Sender: TObject);
var
 sSelectedCompany: String;
const
 K_POS_TRESSPRUEBA = 4;
begin
     inherited;
     //Se limbia el texto de numero de version
     cxlblVersionInfo.Caption :='';

     //Necesario para que LayoutControl tome el skin
     LayoutControl_DevEx.ParentBackground := True;
     LayoutControl_DevExGroupImage.Hidden := True;

     //Boton Demo
     if FEsDemo or FEsKit then
     begin
          //Si es DEMO hacer visible el Boton para ver la info de Demo
          LayoutControl_DevExItemAutorizacion.Visible := True;

          //Se define el caption del boton basandonos en si es Demo o Kit.
          if FEsDemo then
             LayoutControl_DevExItemBotonDemo.Caption := 'DEMO';
          if FEsKit then
             LayoutControl_DevExItemBotonDemo.Caption := 'KIT';

          LayoutControl_DevExItemBotonDemo.Visible := True;
          //Aplicar directamente el estilo deseado al control

          with  dmCliente, cxDemo.LookAndFeel do
          begin
               if SkinController <> nil then
               begin
                    NativeStyle := SkinController.NativeStyle;
                    SkinName := SkinController.SkinName;
                    Kind:= SkinController.Kind;
               end;
          end;   

     end;

     if FEsKit then
     begin
          EntradaVersionDemoKit( ES_KIT );
     end;

     ActiveControl := GridEmpresas;
     //Aplica filtro para empresas Demo
     sSelectedCompany := dmCliente.Compania;
     FiltraEmpresasPrueba;
     {***Este FindCompany es Necesario cuando la empresa seleccionada es de prueba
         pues despues del Filtrado de empresas al no aparecer en el grid, este posiciona
         el cds en el primer registro.***}
     dmCliente.FindCompany( sSelectedCompany );

     //Si la empresa es de prueba se desactiva el Filter del Grid y se Restaura la seleccion en el cdsCompany
     if dmCliente.cdsCompany.FieldByName('CM_CTRL_RL').AsString = ZetaCommonLists.ObtieneElemento(lfTipoCompany, K_POS_TRESSPRUEBA) then
     begin
          //Hay cambios asi que se va al evento OnChange ligado al Checkbox
          cxCkbEmpresasDemo.Checked := True;
          //Se Restaura la empresa seleccionada
          dmCliente.FindCompany( sSelectedCompany );
     end
     else
     begin
          cxCkbEmpresasDemo.Checked := False;
          GridEmpresasDBTableView.DataController.Filter.Active := not cxCkbEmpresasDemo.Checked;
     end;

     //Se especifica de forma dinamica el numero de version
     //cxlblVersionInfo.Caption := GetApplicationVersionInfo( 'ProductVersion' ) ;
     cxlblVersionInfo.Caption := GetApplicationProductVersionSimple;

     //Datos del usuario
     cxlblNombreUsuario.Caption := 'Usuario: '+ dmCliente.GetDatosUsuarioActivo.Nombre ;
     cxlblGruposUsuario.Caption := 'Grupo: '+ dmCliente.GetDatosUsuarioActivo.GrupoDescrip ;
     cxlblUltimaEntrada.Caption := '�ltima Entrada: '+ dmCliente.GetDatosUsuarioActivo.UltimaEntrada ;

     //Aplicar directamente el Estilo para el Grid
     with  dmCliente, GridEmpresas.LookAndFeel do
     begin
          if SkinController <> nil then
          begin
               NativeStyle := SkinController.NativeStyle;
               SkinName := SkinController.SkinName;
          end;
     end;
     //Elegir Botones
     EligeBotones;
     GridEmpresas.SetFocus;
end;

//DevEx: Como esta pantalla es la misma para ambias vistas se debe cambiar la img. de los botones de OK y Cancelar
procedure TEscogeCia_DevEx.EligeBotones;
begin
     with  OK_DevEx do
     begin
          OptionsImage.ImageIndex:= 1;
          Margin := 1;
     end;
     with Cancelar_DevEx do
     begin
          OptionsImage.ImageIndex:= 0;
          Margin := 1;
     end;
end;

//DevEx: configuracion de elementos visibles
procedure TEscogeCia_DevEx.FiltraEmpresas;
begin
     with dmCliente.cdsCompany do
     begin
          Filter := 'UPPER(CM_NOMBRE) LIKE '+ chr(39)+'%'+ UpperCase(cxTEditBusca.Text) + '%' + chr(39);
          Filtered := True;
     end;
end;
procedure TEscogeCia_DevEx.FiltraEmpresasPrueba;
var
  FilterString: string;
begin
     FilterString := '3PRUEBA'; //PENDIENTE SACAR DESCRIPCIOND DEL ENUMERADO
     with GridEmpresasDBTableView.DataController.Filter.Root do
     begin
       Clear;
       AddItem (CM_CTRL_RL, foNotEqual,FilterString, FilterString);
     end;
  GridEmpresasDBTableView.DataController.Filter.Active := True;
end;

function TEscogeCia_DevEx.NoHayDatosGridEmpresas: Boolean;
begin
     if GridEmpresasDBTableView.DataController.RowCount<=0 then
        Result := True
     else
         Result := False;
end;

procedure TEscogeCia_DevEx.SeleccionaEmpresa;
begin
     if NoHayDatosGridEmpresas then
        ZetaDialogo.ZInformation('� Atenci�n !','No hay Empresas por Seleccionar.',0)
     else
         ModalResult := mrOK;
end;

procedure TEscogeCia_DevEx.GridEmpresasDBTableViewDblClick(
  Sender: TObject);
begin
  inherited;
  SeleccionaEmpresa;
end;

procedure TEscogeCia_DevEx.CM_TIPO_DESCRIPCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   sTipo: String;
   APainter: TcxPainterAccess;
   i: Integer;
begin
  inherited;
  if not (AViewInfo.EditViewInfo is TcxCustomTextEditViewInfo) then
     Exit;
  //Determinamos el tipo de la Empresa
  sTipo := AViewInfo.GridRecord.DisplayTexts [CM_CTRL_RL.Index];
  APainter := TcxPainterAccess(TcxViewInfoAcess(AViewInfo).GetPainterClass.Create(ACanvas, AViewInfo));
  with APainter do
        begin
        try
           with TcxCustomTextEditViewInfo(AViewInfo.EditViewInfo).TextRect do
                Left := Left + cxGridImageList.Width + 1;
           DrawContent;
           DrawBorders;
           //For que recorra la lista, PENDIENTE:como sacar un count de la lista?
           for i:=0 to 5 do
           begin
                if sTipo = ZetaCommonLists.ObtieneElemento(lfTipoCompany,i) then
                begin
                     with AViewInfo.ClientBounds do
                          cxGridImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, i);
                     Break;
                end;
           end;

        finally
               Free;
        end;
  end;
  ADone := True;
end;

procedure TEscogeCia_DevEx.cxTEditBuscaPropertiesChange(Sender: TObject);
begin
  inherited;
  if Length(cxTEditBusca.Text)>0 then
     FiltraEmpresas
  else
      dmCliente.cdsCompany.Filtered := False;
  //Habilita los botones solo cuando existen registros
  //OK.Enabled := not NoHayDatosGridEmpresas ;
  OK_DevEx.Enabled := not NoHayDatosGridEmpresas ;
end;

procedure TEscogeCia_DevEx.cxCkbEmpresasDemoClick(Sender: TObject);
begin
  inherited;
  if cxCkbEmpresasDemo.Checked then
     GridEmpresasDBTableView.DataController.Filter.Active := False
  else
     GridEmpresasDBTableView.DataController.Filter.Active := True;
end;

procedure TEscogeCia_DevEx.cxDemoClick(Sender: TObject);
begin
  inherited;
  if FEsDemo then
     EntradaVersionDemoKit( ES_DEMO );
  if FEsKit then
     EntradaVersionDemoKit( ES_KIT );
end;

procedure TEscogeCia_DevEx.cxBtnHelpClick(Sender: TObject);
const
     K_HELP_CAMBIOS = 'Version.chm';
var
   sHelpFile: String;
begin
     with Application do
     begin
          sHelpFile := HelpFile;
          try
             HelpFile := K_HELP_CAMBIOS;
             HelpCommand( HELP_FINDER, 0 );
          finally
             HelpFile := sHelpFile;
          end;
     end;
end;
{begin
  inherited;
  TressShell._H_Contenido.Execute;
end;}

procedure TEscogeCia_DevEx.CM_TIPO_DESCRIPHeaderClick(Sender: TObject);
begin
  inherited;
  ZetaClientTools.OrdenarPor(dmCliente.cdsCompany,'CM_CTRL_RL');
end;

procedure TEscogeCia_DevEx.cxCkbEmpresasDemoPropertiesChange(
  Sender: TObject);
begin
  inherited;
     GridEmpresasDBTableView.DataController.Filter.Active := not cxCkbEmpresasDemo.Checked;

end;

{***DevEx(@am):Agregado para que al dar Enter sobre el grid se conecte a la empresa seleccionada.
Y Al dar SCAPE sin haber seleccionado ninguna opcion cierre la pantalla de selecciond e empresas***}

procedure TEscogeCia_DevEx.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case key of
       VK_ESCAPE: ModalResult := mrCancel;
  end;
end;

procedure TEscogeCia_DevEx.GridEmpresasDBTableViewKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
       VK_RETURN: SeleccionaEmpresa;
  end;
end;

end.
