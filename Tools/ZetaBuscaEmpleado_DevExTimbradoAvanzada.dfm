object BuscaEmpleado_DevExTimbradoAvanzada: TBuscaEmpleado_DevExTimbradoAvanzada
  Left = 644
  Top = 145
  Width = 676
  Height = 250
  ActiveControl = BuscarEdit
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'B'#250'squeda de Empleados'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelControles: TPanel
    Left = 0
    Top = 0
    Width = 660
    Height = 66
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object BuscarLBL: TcxLabel
      Left = 5
      Top = 17
      Caption = '&Buscar:'
      FocusControl = BuscarEdit
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Segoe UI'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      Transparent = True
    end
    object BuscarEdit: TEdit
      Left = 48
      Top = 15
      Width = 390
      Height = 21
      CharCase = ecUpperCase
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      MaxLength = 63
      ParentFont = False
      TabOrder = 0
      OnChange = BuscarEditChange
    end
    object MostrarBajas: TcxCheckBox
      Left = 45
      Top = 42
      Caption = 'Mostrar Ba&jas y Transferidos:'
      ParentFont = False
      Properties.Alignment = taRightJustify
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Segoe UI'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 3
      Transparent = True
      OnClick = MostrarBajasClick
      Width = 172
    end
    object BuscarBtn_DevEx: TcxButton
      Left = 444
      Top = 15
      Width = 21
      Height = 21
      Hint = 'Buscar un empleado'
      Default = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BuscarBtn_DevExClick
      OptionsImage.Glyph.Data = {
        DA050000424DDA05000000000000360000002800000013000000130000000100
        200000000000A4050000000000000000000000000000000000008B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCCC8CCFFCAC6
        CBFF8D838EFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFFB5AEB5FFFFFFFFFFFDFDFDFF9F97A0FF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF9F97A0FFFAF9FAFFFFFFFFFFBAB4BBFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF988F99FFC0BAC0FFD2CED2FFBEB8BFFFEFEDEFFFFFFFFFFFD3D0
        D4FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFAF9FAFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFE9E7E9FF8F8590FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8F8590FFF4F3F4FFF0EF
        F1FFAFA9B0FF908791FFAFA9B0FFFBFBFBFFE9E7E9FF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFFA8A1A9FFFFFFFFFFC5C0C6FF8B818CFF8B818CFF8B818CFFCAC6CBFFFFFF
        FFFF9D959EFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFAAA3ABFFFFFFFFFFB7B0B7FF8B818CFF8B81
        8CFF8B818CFFC5C0C6FFFFFFFFFFA39BA3FF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF908791FFFBFB
        FBFFE9E7E9FF8B818CFF8B818CFF968D97FFEDEBEDFFF8F7F8FF908791FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFFBEB8BFFFFFFFFFFFF8F7F8FFDCD9DDFFFAF9FAFFFDFD
        FDFFB5AEB5FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFAEA7AEFFE7E5
        E8FFF0EFF1FFE0DDE0FFAAA3ABFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
    end
    object BtnBusquedaAvanzada: TcxButton
      Left = 493
      Top = 15
      Width = 145
      Height = 21
      Hint = 'B'#250'squeda por nombre'
      Caption = '  B'#250'squeda &avanzada'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtnBusquedaAvanzadaClick
      OptionsImage.Glyph.Data = {
        DA050000424DDA05000000000000360000002800000013000000130000000100
        200000000000A4050000000000000000000000000000000000005B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF6599FFFFF5F8
        FFFFFFFFFFFFF2F6FFFF87AFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5E94FFFF5E94
        FFFF5B92FFFF9BBDFFFFFFFFFFFFFFFFFFFFFFFFFFFFB4CDFFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFFA0C0FFFFF0F5FFFFDBE7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFDEE9FFFFDEE9FFFFCCDDFFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF6599FFFFF5F8FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFF5F8FFFFDEE9FFFFF2F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF94B8
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFADC9FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFD1E0FFFF6599FFFF5B92FFFF5E94FFFFBCD3FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFC9DBFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF6095FFFFD8E5FFFFFFFFFFFFFFFFFFFF689BFFFF5B92FFFF5B92
        FFFF5B92FFFF5E94FFFFF2F6FFFFFFFFFFFFE8F0FFFF6A9CFFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFC1D6FFFFFFFFFFFFF2F6
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFDEE9FFFFFFFFFFFFD6E4
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF6D9E
        FFFFE3ECFFFFFFFFFFFFFFFFFFFF70A0FFFF5B92FFFF5B92FFFF5B92FFFF6599
        FFFFF5F8FFFFFFFFFFFFEAF1FFFF70A0FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFFAFCAFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3ECFFFF70A0
        FFFF5B92FFFF6D9EFFFFD8E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFBCD3FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF70A0FFFFFAFCFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFF7FAFFFF6A9CFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFFA0C0FFFFCEDFFFFFC1D6FFFFFCFDFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFCCDDFFFFD6E4FFFFABC7FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF96B9
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFA0C0FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF70A0FFFFDEE9FFFFEAF1FFFFE3ECFFFF72A1FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF}
      OptionsImage.Margin = 1
    end
    object EjemploLbl: TcxLabel
      Left = 291
      Top = 35
      Caption = 'Ejemplo: JUAN+P'#201'REZ+L'#211'PEZ'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Segoe UI'
      Style.Font.Style = []
      Style.TextColor = clGrayText
      Style.TextStyle = [fsItalic]
      Style.IsFontAssigned = True
      Transparent = True
    end
  end
  object GridEmpleados_DevEx: TZetaCXGrid
    Left = 0
    Top = 66
    Width = 660
    Height = 146
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object GridEmpleados_DevExDBTableView: TcxGridDBTableView
      OnDblClick = GridEmpleados_DevExDBTableViewDblClick
      OnKeyPress = GridEmpleados_DevExDBTableViewKeyPress
      Navigator.Buttons.CustomButtons = <>
      FilterBox.CustomizeDialog = False
      FilterBox.Visible = fvNever
      OnCustomDrawCell = GridEmpleados_DevExDBTableViewCustomDrawCell
      DataController.DataSource = DataSource
      DataController.Filter.OnGetValueList = GridEmpleados_DevExDBTableViewDataControllerFilterGetValueList
      DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnFilteredItemsList = True
      OptionsCustomize.ColumnGrouping = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'CB_CODIGO'
        OnCustomDrawCell = CB_CODIGOCustomDrawCell
        MinWidth = 75
        Width = 75
      end
      object CB_APE_PAT: TcxGridDBColumn
        Caption = 'Apellido Paterno'
        DataBinding.FieldName = 'CB_APE_PAT'
        OnCustomDrawCell = CB_APE_PATCustomDrawCell
        MinWidth = 122
        Width = 122
      end
      object CB_APE_MAT: TcxGridDBColumn
        Caption = 'Apellido Materno'
        DataBinding.FieldName = 'CB_APE_MAT'
        OnCustomDrawCell = CB_APE_MATCustomDrawCell
        MinWidth = 114
        Width = 114
      end
      object CB_NOMBRES: TcxGridDBColumn
        Caption = 'Nombre(s)'
        DataBinding.FieldName = 'CB_NOMBRES'
        OnCustomDrawCell = CB_NOMBRESCustomDrawCell
        MinWidth = 86
        Width = 86
      end
      object CB_RFC: TcxGridDBColumn
        Caption = 'R.F.C.'
        DataBinding.FieldName = 'CB_RFC'
        OnCustomDrawCell = CB_RFCCustomDrawCell
        MinWidth = 65
        Width = 65
      end
      object CB_SEGSOC: TcxGridDBColumn
        Caption = 'N.S.S.'
        DataBinding.FieldName = 'CB_SEGSOC'
        OnCustomDrawCell = CB_SEGSOCCustomDrawCell
        MinWidth = 76
        Width = 76
      end
      object CB_BAN_ELE: TcxGridDBColumn
        Caption = 'Banca Electr'#243'nica'
        DataBinding.FieldName = 'CB_BAN_ELE'
        Visible = False
        MinWidth = 115
        Width = 115
      end
      object CB_ACTIVO: TcxGridDBColumn
        DataBinding.FieldName = 'CB_ACTIVO'
        Visible = False
      end
      object STATUS: TcxGridDBColumn
        DataBinding.FieldName = 'STATUS'
        Visible = False
      end
      object TABLA: TcxGridDBColumn
        DataBinding.FieldName = 'TABLA'
        Visible = False
      end
    end
    object GridEmpleados_DevExLevel: TcxGridLevel
      GridView = GridEmpleados_DevExDBTableView
    end
  end
  object DataSource: TDataSource
    Left = 149
    Top = 143
  end
end
