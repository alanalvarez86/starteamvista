object BaseShell: TBaseShell
  Left = 99
  Top = 87
  Width = 514
  Height = 253
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar_DevEx: TdxRibbonStatusBar
    Left = 0
    Top = 202
    Width = 506
    Height = 20
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 80
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 150
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 115
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 100
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 50
      end>
    Ribbon = DevEx_ShellRibbon
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = False
    LookAndFeel.SkinName = 'TressMorado2013'
    Color = clFuchsia
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
  end
  object DevEx_ShellRibbon: TdxRibbon
    Left = 0
    Top = 0
    Width = 506
    Height = 24
    Style = rs2013
    ColorSchemeName = 'TressMorado2013'
    ShowTabHeaders = False
    Contexts = <>
    TabOrder = 1
    TabStop = False
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 183
    Width = 506
    Height = 19
    Panels = <
      item
        Width = 80
      end
      item
        Width = 150
      end
      item
        Width = 115
      end
      item
        Width = 100
      end
      item
        Width = 50
      end>
  end
  object LogoffTimer: TTimer
    Enabled = False
    Interval = 120000
    OnTimer = LogoffTimerTimer
    Left = 152
    Top = 56
  end
  object DevEx_SkinController: TdxSkinController
    Kind = lfFlat
    NativeStyle = False
    SkinName = 'TressMorado2013'
    Left = 251
    Top = 58
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 48
    Top = 64
    PixelsPerInch = 96
    object cxStyleStatusBarLabel: TcxStyle
      AssignedValues = [svBitmap, svColor, svFont, svTextColor]
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowFrame
      Font.Height = -12
      Font.Name = 'Segoe UI'
      Font.Style = []
      TextColor = clWindowText
    end
  end
end
