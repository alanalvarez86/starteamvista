unit FIndicadores;

interface

{$R *.RES}

uses
   Controls,
   Classes,
   ExtCtrls,
   Windows,
   Graphics,
   StdCtrls,
   SysUtils,
   MultiMon;

type
  eDashletIconos = (eNingunEnfoque, eEnfoquePositivo, eEnfoqueNegativo );
  eDashletIconoColor = (eSinIcono, eAlza, eBaja );
  TIndicadores = class(TPanel)//(TCustomPanel)
  private
    { private declarations here }
    FLabelTitulo: TLabel;//Titulo Indicador
    FLabelSubTitulo: TLabel;//SubTitulo Indicador
    FLabelValor: TLabel;//Valor Indicador
    FIcono: TImage;//Icono para el indicardor
    FLabelTendencia: TLabel;//Para indicar el Porcentaje;
    FLabelDescripcionTendencia: TLabel;//Descripcion Indicador
    FPanelValorTendencia: TPanel;//Panel Tendencia Valor

    //Textos
    FTitulo: String;//Titulo Indicador
    FSubTitulo: String;//Titulo Indicador
    FValor: Double;//Valor Indicador
    FIconoTendencia: Integer;
    FTendencia: Double;//Icono para el indicardor
    FDescripcionTendencia: String;//Descripcion Indicador
    FDashletIconosDesc: String;
    FPanelDescripcion: TPanel;
    FPanelHeader: TPanel;
    FDashletIconos: Integer;
    FDashletFormato: String;
    FDashletFormatoSub: String;
    FDashletTendenciaDesc: String;
    FSizeFontHeight: Integer;
    FSizeFontWidth: Integer;
    FHeightAux: Integer;
    FWidthAux: Integer;
    procedure SetLabelTitulo(const Value: TLabel);
    procedure SetTitulo(Value: String);
    procedure SetDescripcionTendencia(const Value: String);
    procedure SetIcono(const Value: TImage);
    procedure SetIconoTendencia(const Value: Integer);
    procedure SetLabelDescripcionTendencia(const Value: TLabel);
    procedure SetLabelTendencia(const Value: TLabel);
    procedure SetLabelSubTitulo(const Value: TLabel);
    procedure SetLabelValor(const Value: TLabel);
    procedure SetSubTitulo(const Value: String);
    procedure SetTendencia(const Value: Double);
    procedure SetValor(const Value: Double);
    function GetDashletIconos: Integer;
    function GetDashletIconosDesc: String;
    procedure SetDashletIconos(const Value: Integer);
    procedure SetDashletIconosDesc(const Value: String);
    function GetDashletFormato: String;
    function GetDashletFormatoSub: String;
    procedure SetDashletFormato(const Value: String);
    procedure SetDashletFormatoSub(const Value: String);
    function GetDashletTendenciaDesc: String;
    procedure SetDashletTendenciaDesc(const Value: String);//Descripcion Indicador}
    procedure SetIconoIndicador;
    procedure SetImageEnfoque( eEnfoque: eDashletIconos; eEnfoqueColor: eDashletIconoColor );
  protected
    { protected declarations here }
    //Componentes
    procedure ACanResize(Sender: TObject; var NewWidth, NewHeight: Integer; var Resize: Boolean);


  public
    { public declarations here }
    FBandera: Boolean;
    constructor Create( AOwner : TComponent); overload; override;
    constructor Create(const ControlName: string; AOwner : TComponent; Parent: TWinControl ); reintroduce; overload;
    procedure SetPosicionarLabelTendenciaDescripcion;
    procedure SetBorde;
    procedure LimpiarIndicador;
    procedure MensajeEspera;
    procedure TamanioTexto;
    function TextResize( Canvas: TCanvas; Text: string; Width: Integer; Heigth: Integer ): Integer;
    function IsFUllHD: Boolean;
    procedure ValoresDefaulPanel;
    property LabelTitulo: TLabel read FLabelTitulo write SetLabelTitulo;//Titulo Indicador
    property LabelSubTitulo: TLabel read FLabelSubTitulo write SetLabelSubTitulo;//SubTitulo Indicador
    property LabelValor: TLabel read FLabelValor write SetLabelValor;//Valor Indicador
    property Icono: TImage read FIcono write SetIcono;//Icono para el indicardor
    property LabelTendencia: TLabel read FLabelTendencia write SetLabelTendencia;//Para indicar el Porcentaje;
    property LabelDescripcionTendencia: TLabel read FLabelDescripcionTendencia write SetLabelDescripcionTendencia;//Descripcion Indicador
    property PanelDescripcion: TPanel read FPanelDescripcion write FPanelDescripcion;//Descripcion Indicador
    property PanelHeader: TPanel read FPanelHeader write FPanelHeader;//Panel Header Indicador
    property PanelValorTendencia: TPanel read FPanelValorTendencia write FPanelValorTendencia;

    //Textos
    property Titulo: String read FTitulo write SetTitulo;//Titulo Indicador
    property SubTitulo: String read FSubTitulo write SetSubTitulo;//Titulo Indicador
    property Valor: Double read FValor write SetValor;//Valor Indicador
    property IconoTendencia: Integer read FIconoTendencia write SetIconoTendencia;
    property Tendencia: Double read FTendencia write SetTendencia;//Icono para el indicardor
    property DescripcionTendencia: String read FDescripcionTendencia write SetDescripcionTendencia;//Descripcion Indicador
    property DashletIconosDesc: String read GetDashletIconosDesc write SetDashletIconosDesc;
    property DashletIconos: Integer read GetDashletIconos write SetDashletIconos;
    property DashletFormato: String read GetDashletFormato write SetDashletFormato;
    property DashletFormatoSub: String read GetDashletFormatoSub write SetDashletFormatoSub;
    property DashletTendenciaDesc: String read GetDashletTendenciaDesc write SetDashletTendenciaDesc;

  published
    { published declarations here }

end;

const
     K_MARGIN = 5;
     K_MENSJAE_THREAD = 'REFRESCANDO...';
     K_MARGIN_TEXTOS = 0;
var
   Indicadores: TIndicadores;

implementation

constructor TIndicadores.Create(const ControlName: string; AOwner: TComponent; Parent: TWinControl);
begin
     inherited Create( AOwner );

     Self.Parent := Parent;
     Self.Name := ControlName;

     //Bordes
     SetBorde;

     Self.Width := 150 + 30;
     Self.Height := 87 + 10;
     //Self.Constraints.MinHeight := 200;

     //Background
     Self.ParentBackground:= False;
     Self.ParentColor := False;
     Self.Color := clWhite;

     //Padding
     Padding.Left := K_MARGIN;
     Padding.Right := K_MARGIN;
     Padding.Top := K_MARGIN;
     Padding.Bottom := K_MARGIN;

     //Valor Centrado
     Caption := '';

     FPanelValorTendencia := TPanel.Create( Self );
     with FPanelValorTendencia do
     begin
          ParentBackground:= False;
          ParentColor := False;
          Color := clWhite;
          Parent := Self;
          Align := alClient;
          Padding.Left := K_MARGIN;
          Padding.Right := K_MARGIN;
          Padding.Top := K_MARGIN;
          Padding.Bottom := K_MARGIN;
          BevelEdges := [];
          BevelInner := bvNone;
          BevelKind := bkNone;
          BevelOuter := bvNone;
          //Constraints.MinHeight := 27;
     end;

     FLabelValor := TLabel.Create(Self);
     with FLabelValor do
     begin
          Parent := FPanelValorTendencia;
          Align := alClient;
          Alignment := taCenter;
          AutoSize := False;
          Layout := tlCenter;
          Caption := '';
          Font.Charset := DEFAULT_CHARSET;
          Font.Color := clWindowText;
          Font.Name := 'Segoe UI';
          Font.Size := 14;
          Font.Style := [fsBold];
          ParentBiDiMode := False;
          ParentFont := False;
     end;

     FPanelHeader := TPanel.Create( Self );
     with FPanelHeader do
     begin
          Parent := Self;
          Height := 29;
          Align := alTop;
          Padding.Left := K_MARGIN;
          Padding.Right := K_MARGIN;
          Padding.Top := K_MARGIN;
          Padding.Bottom := K_MARGIN;
          BevelEdges := [];
          BevelInner := bvNone;
          BevelKind := bkNone;
          BevelOuter := bvNone;
     end;

     //Titulo
     FLabelTitulo := TLabel.Create(Self);
     with FLabelTitulo do
     begin
          Parent := FPanelHeader;
          Left := K_MARGIN_TEXTOS;
          Width := 96;
          Height := 12;
          Caption := '';
          Font.Charset := DEFAULT_CHARSET;
          Font.Size := 10;
          Font.Name := 'Segoe UI';
          Font.Style := [fsBold];
          Font.Color := $003E3E3E;
          ParentFont := False;
     end;

     //Subtitulo
     FLabelSubTitulo := TLabel.Create(Self);
     with FLabelSubTitulo do
     begin
          Parent := FPanelHeader;
          Left := FLabelTitulo.Left;
          Top := FLabelTitulo.Top + FLabelTitulo.Height;
          Width := 96;
          Height := 12;
          Caption := '';
          Font.Charset := DEFAULT_CHARSET;
          Font.Color := clWindowText;
          Font.Size := 8;
          Font.Name := 'Segoe UI';
          Font.Style := [];
          ParentFont := False;
     end;

     FPanelDescripcion := TPanel.Create( Self );
     with FPanelDescripcion do
     begin
          ParentBackground:= False;
          ParentColor := False;
          Color := clWhite;
          Parent := Self;
          Height := 16;
          Constraints.MinHeight := 12;
          Align := alBottom;
          Padding.Left := K_MARGIN;
          Padding.Right := K_MARGIN;
          Padding.Top := K_MARGIN;
          Padding.Bottom := K_MARGIN;
          BevelEdges := [];
          BevelInner := bvNone;
          BevelKind := bkNone;
          BevelOuter := bvNone;
     end;

     //FIcono
     FIcono := TImage.Create(Self);
     with FIcono do
     begin
          Parent := FPanelDescripcion;
          Left := LabelTitulo.Left;
          Width := 16;
          Height := 16;
          Touch.ParentTabletOptions := False;
          Touch.TabletOptions := [toPressAndHold, toPenTapFeedback];
          Picture.Bitmap.LoadFromResourceName( HInstance,'TENDENCIA_ROJO_UP');
     end;

     //Tendencia
     FLabelTendencia := TLabel.Create(Self);
     with FLabelTendencia do
     begin
          Parent := FPanelDescripcion;
          Left := FIcono.Left + FIcono.Width + 1;
          Width := Self.Width - Left;
          Height := 12;
          Caption := '';
          Font.Charset := DEFAULT_CHARSET;
          Font.Color := clWindowText;
          Font.Size := 8;
          Font.Name := 'Segoe UI';
          Font.Style := [fsBold];
          ParentFont := False;
     end;

     FLabelDescripcionTendencia := TLabel.Create(Self);
     with FLabelDescripcionTendencia do
     begin
          Parent := FPanelDescripcion;
          Left := FIcono.Left + FIcono.Width + 1;
          Width := Self.Width - Left;
          Height := 12;
          Caption := '';
          Font.Charset := DEFAULT_CHARSET;
          Font.Color := clWindowText;
          Font.Size := 8;
          Font.Name := 'Segoe UI';
          Font.Style := [];
          ParentFont := False;
     end;
     Self.OnCanResize := ACanResize;
     FSizeFontHeight := 1;
     FSizeFontWidth := 1;
     FHeightAux := Self.Height;
     FWidthAux := Self.Width;
     FBandera := False;
end;

procedure TIndicadores.ACanResize(Sender: TObject; var NewWidth, NewHeight: Integer; var Resize: Boolean);
begin
     if Self.FLabelValor.Caption <> '' then
        Self.FLabelValor.Font.Size := TextResize( Self.Canvas, Self.FLabelValor.Caption, Self.FLabelValor.Width, Self.FLabelValor.Height );
end;


procedure TIndicadores.TamanioTexto;
begin
     Self.LabelValor.Font.Size := TextResize( Self.Canvas, Self.FLabelValor.Caption, Self.FLabelValor.Width, Self.FLabelValor.Height );
end;

function TIndicadores.IsFUllHD: Boolean;
var
  MonInfo: TMonitorInfo;
  iWidthResolucion: Integer;//1920px
  iHeightResolucion: Integer;//1080px
begin
    MonInfo.cbSize := SizeOf(MonInfo);
    GetMonitorInfo(MonitorFromWindow(  Handle, MONITOR_DEFAULTTONEAREST), @MonInfo);
    iWidthResolucion := MonInfo.rcMonitor.Right - MonInfo.rcMonitor.Left;
    iHeightResolucion:= MonInfo.rcMonitor.Bottom - MonInfo.rcMonitor.Top;
    Result := False;
    if ( iWidthResolucion = 1920 ) and ( iHeightResolucion = 1080 ) then
       Result := True;
end;

function TIndicadores.TextResize( Canvas: TCanvas; Text: string; Width: Integer; Heigth: Integer ): Integer;
var
   Font: TFont;
   FontRecall: TFontRecall;
   InitialTextWidth: Integer;
   InitialTextHeigth: Integer;
   lContinuar: Boolean;

   function PararWhile: Boolean;
   begin
        Result:= False;
        if ( Font.Size = -99 ) or ( Font.Size > 99 )  then
        Result := True;
   end;
begin
     lContinuar := True;
     Canvas := Self.Canvas;
     Font := Canvas.Font;
     if ( IsFullHD ) then
        Font.Size := 14
     else
         Font.Size := 8;
     FontRecall := TFontRecall.Create(Font);
     try
        InitialTextWidth := Canvas.TextWidth(Text);
        Font.Size := MulDiv(Font.Size, Width, InitialTextWidth);
        if InitialTextWidth < Width then
        begin
             while lContinuar do
             begin
                  Font.Size := Font.Size + 1;
                  if Canvas.TextWidth(Text) > Width then
                  begin
                       Font.Size := Font.Size - 1;
                       break;
                  end;
                  if PararWhile then
                     break;
             end;
        end;
        if InitialTextWidth > Width then
        begin
             while lContinuar do
             begin
                  Font.Size := Font.Size - 1;
                  if Canvas.TextWidth(Text) <= Width then
                  begin
                       Font.Size := Font.Size;
                       break;
                  end;
                  if PararWhile then
                     break;
             end;
        end;
        ///Heigth
        InitialTextHeigth := Canvas.TextHeight(Text);
        Font.Size := MulDiv(Font.Size, Height, InitialTextHeigth);

        if InitialTextHeigth < Heigth then
        begin
             while lContinuar do
             begin
                  Font.Size := Font.Size + 1;
                  if Canvas.TextHeight(Text) > Heigth then
                  begin
                       Font.Size := Font.Size - 1;
                       break;
                  end;
                  if PararWhile then
                     break;
             end;
        end;
        if InitialTextHeigth > Heigth then
        begin
             while lContinuar do
             begin
                  Font.Size := Font.Size - 1;
                  if Canvas.TextHeight(Text) <= Heigth then
                  begin
                       Font.Size := Font.Size;
                       break;
                  end;
                  if PararWhile then
                     break;
             end;
        end;
        if ( IsFUllHD ) and ( Font.Size > 23 ) then
           Font.Size := 23;
        if ( IsFUllHD ) and ( ( Font.Size = 0 ) or ( Font.Size < 0 ) or ( Font.Size < 12 ) ) then
           Font.Size := 12;
        if not( IsFUllHD ) and ( ( Font.Size = 0 ) or ( Font.Size < 0 ) or ( Font.Size < 8 ) ) then
           Font.Size := 8;
        Result := Font.Size;
     finally
            FontRecall.Free;
     end;
end;

function TIndicadores.GetDashletFormato: String;
begin
     Result := FDashletFormato;
end;

function TIndicadores.GetDashletFormatoSub: String;
begin
     Result := FDashletFormatoSub;
end;

function TIndicadores.GetDashletIconos: Integer;
begin
     Result := FDashletIconos;
end;

function TIndicadores.GetDashletIconosDesc: String;
begin
     Result := FDashletIconosDesc;
end;

function TIndicadores.GetDashletTendenciaDesc: String;
begin
     Result := FDashletTendenciaDesc;
end;

constructor TIndicadores.Create(AOwner: TComponent);
begin
     inherited Create( AOwner );
end;


procedure TIndicadores.SetBorde;
begin
     Self.BevelEdges := [beLeft,beTop,beRight,beBottom];
     Self.BevelInner := bvNone;
     Self.BevelKind := bkTile;
     Self.BevelOuter := bvNone;
end;

procedure TIndicadores.SetDashletFormato(const Value: String);
begin
     Self.FDashletFormato := Value;
end;

procedure TIndicadores.SetDashletFormatoSub(const Value: String);
begin
     Self.FDashletFormatoSub := Value;
end;

procedure TIndicadores.SetDashletIconos(const Value: Integer);
begin
     Self.FDashletIconos := Value;
end;

procedure TIndicadores.SetDashletIconosDesc(const Value: String);
begin
     Self.FDashletIconosDesc := Value;
end;

procedure TIndicadores.SetDashletTendenciaDesc(const Value: String);
begin
     Self.FDashletTendenciaDesc := Value;
end;

procedure TIndicadores.SetDescripcionTendencia(const Value: String);
begin
     FDescripcionTendencia := Value;
     FLabelDescripcionTendencia.Caption := Value;
end;

procedure TIndicadores.SetIcono(const Value: TImage);
begin
     FIcono := Value;
end;

procedure TIndicadores.SetIconoTendencia(const Value: Integer);
begin
     FIconoTendencia := Value;
end;

procedure TIndicadores.SetImageEnfoque( eEnfoque: eDashletIconos; eEnfoqueColor: eDashletIconoColor );
const
     K_TENDENCIA_ROJO_UP = 'TENDENCIA_ROJO_UP';
     K_TENDENCIA_VERDE_UP = 'TENDENCIA_VERDE_UP';
     K_TENDENCIA_ROJO_DOWN = 'TENDENCIA_ROJO_DOWN';
     K_TENDENCIA_VERDE_DOWN = 'TENDENCIA_VERDE_DOWN';
     K_TENDENCIA_CERO = 'TENDENCIA_CERO';
begin
     FIcono.Visible := True;
     FLabelTendencia.Font.Color := RGB(0,0,0);
     if eEnfoque = eNingunEnfoque then
        FIcono.Visible := False
     else if ( eEnfoque = eEnfoquePositivo ) and ( eEnfoqueColor = eSinIcono ) then
     begin
          FIcono.Picture.Bitmap.LoadFromResourceName( HInstance, K_TENDENCIA_CERO );
          FLabelTendencia.Font.Color := RGB(0,0,0);
     end
     else if ( eEnfoque = eEnfoqueNegativo ) and ( eEnfoqueColor = eSinIcono ) then
     begin
          FIcono.Picture.Bitmap.LoadFromResourceName( HInstance, K_TENDENCIA_CERO );
          FLabelTendencia.Font.Color := RGB(0,0,0);
     end
     else if ( eEnfoque = eEnfoquePositivo ) and ( eEnfoqueColor = eAlza ) then
     begin
          FIcono.Picture.Bitmap.LoadFromResourceName( HInstance, K_TENDENCIA_VERDE_UP );
          FLabelTendencia.Font.Color := RGB(147,210,80);
     end
     else if ( eEnfoque = eEnfoquePositivo ) and ( eEnfoqueColor = eBaja ) then
     begin
          FIcono.Picture.Bitmap.LoadFromResourceName( HInstance, K_TENDENCIA_ROJO_DOWN );
          FLabelTendencia.Font.Color := RGB(204,88,72);
     end
     else if ( eEnfoque = eEnfoqueNegativo ) and ( eEnfoqueColor = eAlza ) then
     begin
          FIcono.Picture.Bitmap.LoadFromResourceName( HInstance, K_TENDENCIA_ROJO_UP );
          FLabelTendencia.Font.Color := RGB(204,88,72);
     end
     else if ( eEnfoque = eEnfoqueNegativo ) and ( eEnfoqueColor = eBaja ) then
     begin
          FIcono.Picture.Bitmap.LoadFromResourceName( HInstance, K_TENDENCIA_VERDE_DOWN );
          FLabelTendencia.Font.Color := RGB(147,210,80);
     end;
end;

procedure TIndicadores.SetLabelDescripcionTendencia(const Value: TLabel);
begin
     FLabelTitulo := Value;
end;

procedure TIndicadores.SetLabelTendencia(const Value: TLabel);
begin
     FLabelTendencia := Value;
end;

procedure TIndicadores.SetLabelSubTitulo(const Value: TLabel);
begin
     FLabelSubTitulo := Value;
end;

procedure TIndicadores.SetLabelTitulo(const Value: TLabel);
begin
     FLabelTitulo := Value;
end;

procedure TIndicadores.SetLabelValor(const Value: TLabel);
begin
     FLabelValor := Value;
end;

procedure TIndicadores.SetSubTitulo(const Value: String);
begin
     FSubTitulo := Value;
     FLabelSubTitulo.Caption := Value;
end;

procedure TIndicadores.SetTendencia(const Value: Double);
begin
     FTendencia := Value;
     try
        FLabelTendencia.Caption := Format( Self.FDashletFormatoSub, [ Value ] );
        FLabelDescripcionTendencia.Caption := Self.DashletTendenciaDesc;
        SetIconoIndicador;
     except
           on Error: Exception do
           begin
                FLabelTendencia.Caption := Error.Message;
           end;
     end;
end;

procedure TIndicadores.SetPosicionarLabelTendenciaDescripcion;
const
     K_ESPACIO = 5;
   function GetLabelWidth( etiqueta : TLabel  ) : integer;
   var
      bmWidth: TBitmap;
      begin
        bmWidth := TBitmap.Create;
        try
          bmWidth.Canvas.Font.Assign(etiqueta.Font);
          Result := bmWidth.Canvas.TextWidth(etiqueta.Caption);
        finally
          bmWidth.Free;
      end;
   end;
begin
     if ( FIcono.Visible ) and ( FLabelTendencia.Caption = '' ) and ( FLabelDescripcionTendencia.Caption <> '' ) then
     begin
          FLabelDescripcionTendencia.Left := FLabelTendencia.Left;
          FLabelDescripcionTendencia.Width :=  GetLabelWidth( FLabelDescripcionTendencia ) + K_ESPACIO;
     end;
     if ( FIcono.Visible ) and ( FLabelTendencia.Caption <> '' ) and ( FLabelDescripcionTendencia.Caption <> '' ) then
     begin
          FLabelTendencia.Width := GetLabelWidth( FLabelTendencia );
          FLabelTendencia.left :=  FIcono.Width + FIcono.Left + K_ESPACIO;
          FLabelDescripcionTendencia.Width := GetLabelWidth( FLabelDescripcionTendencia ) + K_ESPACIO;
          FLabelDescripcionTendencia.Left := GetLabelWidth( FLabelTendencia ) + FIcono.Width + (FIcono.Left * 2) + K_ESPACIO * 2;
     end;
     if ( not FIcono.Visible ) and ( FLabelTendencia.Caption = '' ) and ( FLabelDescripcionTendencia.Caption <> '' ) then
     begin
          FLabelDescripcionTendencia.Left := FIcono.Left;
          FLabelDescripcionTendencia.Width := GetLabelWidth( FLabelDescripcionTendencia ) + K_ESPACIO;
     end;
     if ( not FIcono.Visible ) and ( FLabelTendencia.Caption <> '' ) and ( FLabelDescripcionTendencia.Caption <> '' ) then
     begin
          FLabelTendencia.Width := GetLabelWidth( FLabelTendencia );
          FLabelTendencia.left := FIcono.Left;
          FLabelDescripcionTendencia.Width :=  GetLabelWidth( FLabelDescripcionTendencia ) + K_ESPACIO;
          FLabelDescripcionTendencia.Left := FLabelTendencia.Width + FIcono.Left + 2;
     end;
end;

procedure TIndicadores.SetIconoIndicador;
begin
     if ( eDashletIconos( Self.FDashletIconos ) <> eNingunEnfoque ) and ( eDashletIconos( Self.FDashletIconos ) = eEnfoquePositivo ) then
     begin
          if FTendencia = 0 then
             SetImageEnfoque( eNingunEnfoque, eSinIcono )
          else if FTendencia > 0 then
               SetImageEnfoque( eEnfoquePositivo,  eAlza )
          else if FTendencia < 0 then
               SetImageEnfoque( eEnfoquePositivo,  eBaja );
     end
     else if ( eDashletIconos( Self.FDashletIconos ) <> eNingunEnfoque ) and ( eDashletIconos( Self.FDashletIconos ) = eEnfoqueNegativo ) then
     begin
          if FTendencia = 0 then
             SetImageEnfoque( eNingunEnfoque, eSinIcono )
          else if FTendencia > 0 then
               SetImageEnfoque( eEnfoqueNegativo,  eAlza )
          else if FTendencia < 0 then
               SetImageEnfoque( eEnfoqueNegativo,  eBaja );
     end;
end;

procedure TIndicadores.SetTitulo(Value: String);
begin
     FTitulo := Value;
     FLabelTitulo.Caption := Value;
end;

procedure TIndicadores.SetValor(const Value: Double);
begin
     FValor := Value;
     try
        ValoresDefaulPanel;
        Self.FLabelValor.Caption := Format( Self.FDashletFormato, [ Value ] );
     except
           on Error: Exception do
           begin
                 Self.FLabelValor.Caption := Error.Message;
           end;
     end;
end;

procedure TIndicadores.LimpiarIndicador;
begin
     Self.FLabelValor.Caption := '';
     Self.FLabelTendencia.Caption := '';
     Self.FLabelDescripcionTendencia.Caption := '';
end;

procedure TIndicadores.ValoresDefaulPanel;
begin
     Self.FPanelValorTendencia.Color := clWhite;
     Self.FPanelValorTendencia.Caption := '';
     Self.FPanelDescripcion.Color := clWhite;
     with Self.FPanelValorTendencia do
     begin
          Font.Name := 'Segoe UI';
          Font.Style := [fsBold];
     end;
end;

procedure TIndicadores.MensajeEspera;
begin
     Self.FIcono.Hide;
     with Self.FPanelValorTendencia do
     begin
          Color := $00F4F4F4;
          Font.Name := 'Segoe UI';
          Font.Size := 10;
          Caption := K_MENSJAE_THREAD;
          Font.Color := $00A6A6A6;
     end;
     Self.FPanelDescripcion.Color := $00F4F4F4;
     Self.FLabelValor.Caption := '';
     Self.FLabelTendencia.Caption := '';
     Self.FLabelDescripcionTendencia.Caption := '';
end;

end.


