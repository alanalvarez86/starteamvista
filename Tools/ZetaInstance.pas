unit ZetaInstance;

interface

uses Windows, SysUtils, Forms;

function RunOnlyOneInstance( const sClassName: String ): Boolean;

implementation

type
  PHWND = ^HWND;

function CheckPreviousInstance( const sClassName: String ): Boolean;
var
   Previous: HWND;
   {
   Mutex: THandle;
   sClassName, sTitle: String;
   }
   sTitle: String;
   pClassName, pTitle: {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif};
begin
     {$ifdef FALSE}
     Mutex := Windows.CreateMutex( NIL, FALSE, 'SingleInstanceProgramHMutex' );
     {wait if other instance still initializing; quit if too long}
     if ( Windows.WaitForSingleObject( Mutex, 2000 ) = WAIT_TIMEOUT ) then
        Result := False
     else
     begin
          with Application.MainForm do
          begin
               sClassName := ClassName;
               sTitle := Caption;
          end;
     {$endif}
          sTitle := 'CafeServer';
          GetMem( pClassName, Length( sClassName ) + 1 );
          try
             pClassName := StrPCopy( pClassName, sClassName );
             GetMem( pTitle, Length( sTitle ) + 1 );
             try
                pTitle := StrPCopy( pTitle, sTitle );
                Previous := Windows.FindWindow( pClassName, pTitle );
             finally
                    FreeMem( pTitle );
             end;
          finally
                 FreeMem( pClassName );
          end;
          if ( Previous <> 0 ) then
             Previous := Windows.GetWindow( Previous, GW_OWNER );
          if ( Previous = 0 ) then
             Result := True
          else
          begin
               Result := False;
               if IsIconic( Previous ) then
                  Windows.ShowWindow( Previous, SW_SHOWNORMAL )
               else
                   Windows.SetForegroundWindow( Previous );
          end;
     {$ifdef FALSE}
     end;
     {$endif}
end;

function EnumFunc( Window: HWND; TargetWindow: PHWND ): Boolean;
var
   ClassName: array[ 0..30 ] of Char;
   Previous: LongWord;
begin
     Result := True;
     Previous := Windows.GetWindowLong( Window, GWL_HINSTANCE );
     if ( Previous = hPrevInst ) then
     begin
          Windows.GetClassName( Window, ClassName, 30 );
          if ( StrIComp( ClassName, 'TApplication' ) = 0 ) then
          begin
               TargetWindow^ := Window;
               Result := False;
          end;
     end;
end;

function GotoPreviousInstance: Boolean;
var
   PreviousWindow: HWND;
begin
     PreviousWindow := 0;
     Windows.EnumWindows( @EnumFunc, Longint( PreviousWindow ) );
     if ( PreviousWindow = 0 ) then
        Result := True
     else
     begin
          Result := True;
          if IsIconic( PreviousWindow ) then
             ShowWindow( PreviousWindow, SW_RESTORE )
          else
              BringWindowToTop( PreviousWindow );
     end;
end;

function RunOnlyOneInstance( const sClassName: String ): Boolean;
begin
{$ifdef FALSE}
     if ( hPrevInst = 0 ) then
        Result := True { No hay instancias previas }
     else
     begin
          GotoPreviousInstance;
          Result := False;
     end;
{$else}
     Result := CheckPreviousInstance( sClassName );
{$endif}
end;

end.

