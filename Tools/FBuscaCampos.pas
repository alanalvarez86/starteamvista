unit FBuscaCampos;

interface
{$INCLUDE DEFINES.INC}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Db, ComCtrls, Grids, DBGrids,
  DDiccionario, ZetaCommonLists, ZetaTipoEntidad, ZetaCommonClasses,
  ZReportTools, ZetaDBGrid, ZetaMessages;

type
  TBuscaCampos = class(TForm)
    Panel2: TPanel;
    Panel3: TPanel;
    OK: TBitBtn;
    BitBtn2: TBitBtn;
    PageControl: TPageControl;
    tsPorTabla: TTabSheet;
    tsPorCampo: TTabSheet;
    Panel1: TPanel;
    Label1: TLabel;
    EntidadesCombo: TComboBox;
    Panel4: TPanel;
    Label2: TLabel;
    NombreEdit: TEdit;
    BuscarBtn: TBitBtn;
    dsPorCampo: TDataSource;
    CriterioRG: TRadioGroup;
    GridPorCampo: TZetaDBGrid;
    dsPorTabla: TDataSource;
    GridPorTabla: TZetaDBGrid;
    rgMostrar: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure EntidadesComboChange(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure GridPorCampoDblClick(Sender: TObject);
    procedure NombreEditChange(Sender: TObject);
    procedure BuscarBtnClick(Sender: TObject);
    procedure CriterioRGClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  protected
    procedure KeyDown( var Key: Word; Shift: TShiftState );override;

  private
    { Private declarations }
    FEntidad : TipoEntidad;
    FTodos : Boolean;
    FConRelaciones : Boolean;
    FResultado: TDiccionRecord;
    procedure LlenaLista;
    procedure SeleccionaCampo;
    procedure BuscarCampo;

    procedure Connect;
    procedure LimpiaRecord;
    procedure WmExaminar(var Message: TMessage);message WM_EXAMINAR;
  public
    { Public declarations }
    property Entidad : TipoEntidad read FEntidad write FEntidad;
    property Resultado : TDiccionRecord read FResultado;
    property Todos : Boolean read FTodos write FTodos;
    property ConRelaciones : Boolean read FConRelaciones write FConRelaciones;

    procedure SetPestanaActiva( const iPosicion : integer );
    procedure LlenaTodasListas;

  end;

function BuscarCampos( const iEntidad : TipoEntidad; const lTodos, lConRelaciones : Boolean ) : TDiccionRecord;
function PickCampo( const iEntidad : TipoEntidad; const lRelaciones : Boolean = TRUE ) : string;

implementation

uses ZetaCommonTools,
     ZetaDialogo,
     ZetaTipoEntidadTools, DBaseDiccionario;

var BuscaCampos: TBuscaCampos;

{$R *.DFM}

function PickCampo( const iEntidad : TipoEntidad; const lRelaciones : Boolean = TRUE ) : string;
begin
     with BuscarCampos( iEntidad, TRUE, lRelaciones  ) do
          if DI_CLASIFI <> enNinguno then
             if DI_CLASIFI = enFunciones then
                Result := DI_REQUIER
             else Result := DI_TABLA+'.'+DI_NOMBRE;
end;

function BuscarCampos( const iEntidad : TipoEntidad; const lTodos, lConRelaciones : Boolean  ) : TDiccionRecord;
begin
     if EntidadValida( iEntidad ) then
     begin
          if BuscaCampos = NIL then BuscaCampos := TBuscaCampos.Create( Application.MainForm );
          with BuscaCampos do
          begin
               if iEntidad <> enNinguno then Entidad := iEntidad;
               Todos := lTodos;
               ConRelaciones := lConRelaciones;

               ShowModal;
               Result := Resultado;
          end;
     end
     else
     begin
          Result.DI_CLASIFI := enNinguno;
          ZetaDialogo.ZError('Tabla No V�lida', 'El Reporte Contiene Una Tabla Para Uso Futuro', 0);
     end;
end;
     {
function BuscarCampoFuncion( iEntidad : TipoEntidad; lTodos, lConRelaciones : Boolean  ) : String;
 var oContainer : TCampoMaster;
begin
     {oContainer := BuscarCampos( iEntidad, lTodos, lConRelaciones );
     with oContainer do
     begin
          if oContainer <> NIL then
          begin
               if Entidad = enFunciones then
                  Result := TressShell.Provider.Diccionario.GetCampo( oContainer, Titulo ).Formula
               else Result := Tabla + PUNTO + Formula
          end
          else Result := VACIO;
     end;
end;}

procedure TBuscaCampos.FormCreate(Sender: TObject);
begin
     HelpContext := H55121_Generales_Agrega_campo;
     PageControl.ActivePage := tsPorCampo;

     //GridPorCampo.Options := [dgEditing,dgTitles,dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgTabs,dgCancelOnExit];
     //GridPorTabla.Options := [dgEditing,dgTitles,dgColumnResize,dgTabs,dgCancelOnExit];

     {$ifdef RDD}
     GridPorCampo.Columns[0].FieldName := 'EN_TABLA';
     GridPorCampo.Columns[1].FieldName := 'AT_CAMPO';
     GridPorCampo.Columns[2].FieldName := 'AT_TITULO';

     GridPorTabla.Columns[0].FieldName := 'AT_CAMPO';
     GridPorTabla.Columns[1].FieldName := 'AT_TITULO';
     {$else}
     GridPorCampo.Columns[0].FieldName := 'DI_TABLA';
     GridPorCampo.Columns[1].FieldName := 'DI_NOMBRE';
     GridPorCampo.Columns[2].FieldName := 'DI_TITULO';

     GridPorTabla.Columns[0].FieldName := 'DI_NOMBRE';
     GridPorTabla.Columns[1].FieldName := 'DI_TITULO';
     {$endif}

end;

procedure TBuscaCampos.LlenaTodasListas;
 var i : integer;
begin
     with EntidadesCombo do
     begin
          Clear;
          dmDiccionario.GetListaTablas( FEntidad,Items, FTodos, FConRelaciones );
          for i:= 0 to Items.Count -1 do
              if TCampoMaster( Items.Objects[ i ] ).Entidad = FEntidad then
              begin
                   ItemIndex := i;
                   Break;
              end;
     end;
     LlenaLista;
end;

procedure TBuscaCampos.FormShow(Sender: TObject);
begin
     Connect;

     if PageControl.ActivePage = tsPorCampo then
     begin
          NombreEdit.Text := VACIO;
          NombreEdit.SetFocus;
     end
     else GridPorTabla.SetFocus;

     rgMostrar.ItemIndex := 0;
     rgMostrar.Visible := FTodos;
end;

procedure TBuscaCampos.EntidadesComboChange(Sender: TObject);
begin
     LlenaLista;
end;

procedure TBuscaCampos.LlenaLista;
begin
     with EntidadesCombo do
          dmDiccionario.GetListaDatosTablas( TCampoMaster( Items.Objects[ ItemIndex ] ).Entidad,
                                             FTodos, rgMostrar.ItemIndex = 1 );
end;

procedure TBuscaCampos.SeleccionaCampo;

 procedure LlenaRecord(cdsDataSet : TDataset);
 begin
      if cdsDataSet.BOF AND cdsDataSet.EOF then
      begin
           LimpiaRecord;
           ModalResult := mrCancel;
      end
      else
      begin
           FResultado := GetDiccionRecord(cdsDataSet);
           ModalResult := mrOK;
      end;
 end;
begin
     // PorTabla
     if ( PageControl.ActivePage = tsPorTabla ) then
     begin
          LlenaRecord( dmDiccionario.cdsBuscaporTabla );
          if rgMostrar.ItemIndex = 1 then
             with FResultado do
             begin
                  DI_REQUIER := ZReportTools.GetCampoDescripcion( FResultado );
                  DI_CLASIFI := enFunciones;
                  DI_CALC    := -1;
                  DI_ANCHO   := 25;
                  DI_MASCARA := '';
                  DI_TFIELD  := tgTexto;
                  DI_TABLA   := '';
             end;

     end
     else LlenaRecord( dmDiccionario.cdsBuscaporCampo );
     Close;
end;

procedure TBuscaCampos.OKClick(Sender: TObject);
begin
     SeleccionaCampo;
end;

procedure TBuscaCampos.BuscarCampo;
begin
     with dsPorCampo.DataSet do
     begin
          dmDiccionario.BuscarCampo( FEntidad,NombreEdit.Text, CriterioRG.ItemIndex = 0, FTodos, FConRelaciones );
          if Eof then
          begin
               NombreEdit.SetFocus;
               Beep;
          end
          else GridPorCampo.SetFocus;
     end;
end;

procedure TBuscaCampos.GridPorCampoDblClick(Sender: TObject);
begin
     SeleccionaCampo;
end;

procedure TBuscaCampos.NombreEditChange(Sender: TObject);
begin
     BuscarBtn.Enabled := Length( NombreEdit.Text ) > 0;
end;

procedure TBuscaCampos.BuscarBtnClick(Sender: TObject);
begin
     BuscarCampo;
end;

procedure TBuscaCampos.CriterioRGClick(Sender: TObject);
begin
     if Length( NombreEdit.Text ) > 0 then BuscarCampo;
end;

procedure TBuscaCampos.SetPestanaActiva( const iPosicion : integer );
begin
     with PageControl do
          if tsPorCampo.PageIndex = iPosicion then ActivePage := tsPorCampo
          else ActivePage := tsPorTabla;
end;



procedure TBuscaCampos.Connect;
begin
     with dmDiccionario do
     begin
          if cdsBuscaPorCampo.Active then cdsBuscaPorCampo.EmptyDataSet;
          if cdsBuscaPorTabla.Active then cdsBuscaPorTabla.EmptyDataSet;
          dsPorCampo.Dataset := cdsBuscaPorCampo;
          dsPorTabla.Dataset := cdsBuscaPorTabla;
     end;

     LlenaTodasListas;
end;

procedure TBuscaCampos.WmExaminar(var Message: TMessage);
begin
     SeleccionaCampo;
end;

procedure TBuscaCampos.BitBtn2Click(Sender: TObject);
begin
     LimpiaRecord;
end;

procedure TBuscaCampos.LimpiaRecord;
begin
     with FResultado do
     begin
          DI_CLASIFI := enNinguno;
          DI_NOMBRE  := '';
          DI_TITULO  := '';
          DI_CALC    := 0;
          DI_ANCHO   := 0;
          DI_MASCARA := '';
          DI_TFIELD  := eTipoGlobal(0);
          DI_ORDEN   := FALSE;
          DI_REQUIER := '';
          DI_TRANGO  := eTipoRango(0);
          DI_NUMERO  := 0;
          DI_VALORAC := {$ifdef RDD}vaSinValor;{$else}VACIO;{$endif}
          DI_RANGOAC := 0;
          DI_TCORTO  := '';
     end;
end;

procedure TBuscaCampos.KeyDown( var Key: Word; Shift: TShiftState );
begin
     if ( Key = VK_RETURN ) then
     begin
          if ( ActiveControl = NombreEdit ) then
          begin
               Key := 0;
               BuscarCampo;
          end;
     end;
end;

end.

