unit FToolsVideo;

interface
uses
    SysUtils, Classes, Graphics, Controls, Forms, Dialogs,TDMULTIP, Videocap,
    ZetaDialogo, StdCtrls;

     type
         TSetControls = procedure( lHabilita:Boolean ) of object ;
         TSetDriver = procedure of object;

    function InicializaVideo( VideoCap1 : TVideoCap ; cbDrivers:TComboBox ; lblDriver:TLabel ): Boolean;
    function DriverManager ( cbDrivers:TComboBox ; lblDriver:TLabel ): Boolean;
var
   SetControls : TSetControls;
   SetDriver : TSetDriver;

implementation


function InicializaVideo( VideoCap1:TVideoCap ; cbDrivers:TComboBox ; lblDriver:TLabel ): Boolean;
var
   oCursor: TCursor;
begin
     Result := True;
     with VideoCap1 do
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             try
                VideoPreview := True;
                SetControls(False);
            except
                  on Error: ENotConnectException do
                  begin
                       SetControls( True );
                      // ZetaDialogo.ZError('Error Al Capturar Imagen', 'El Dispositivo Seleccionado No Est� Disponible', 0 );
                       ZetaDialogo.ZError('Error Al Capturar Imagen', 'Favor de conectar la c�mara al equipo', 0 );                      
                       Result := False;
                  end;
                  on Error: ENoDriverException do
                  begin
                       SetControls( True );
                       ZetaDialogo.ZError('Error Al Capturar Imagen', 'Favor de conectar la c�mara al equipo', 0 );
                       Result := False;
                  end;
            end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

function DriverManager( cbDrivers:TComboBox ; lblDriver:TLabel ): Boolean;
var
   i: Integer;
   oDriverList: TStringList;
begin
     Result := True;
     oDriverList := TStringList.Create;
     with oDriverList do
     begin
          try
             oDriverList := VideoCap.GetDriverList;
             if( Count > 0 )then
             begin
                  for i:=0 to ( Count - 1 ) do
                  begin
                       cbDrivers.Items.Add( Strings[i] );
                  end;
                  cbDrivers.ItemIndex := 0;
                  cbDrivers.Visible := ( Count > 1 );
                  lblDriver.Visible := ( Count > 1 );
                  SetDriver;
             end
             else
             begin
                  Result := False;
                  cbDrivers.Visible := False;
                  lblDriver.Visible := False;
             end;
          finally
                 FreeAndNil( oDriverList );
          end;
     end;
end;

end.
