inherited EditClasificacion_DevEx: TEditClasificacion_DevEx
  Left = 213
  Top = 365
  HelpContext = 101
  ActiveControl = Codigo
  Caption = 'Clasificaci'#243'n'
  ClientHeight = 105
  ClientWidth = 317
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object CodigoLBL: TLabel [0]
    Left = 28
    Top = 14
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'&digo:'
    FocusControl = Codigo
  end
  object DescripcionLBL: TLabel [1]
    Left = 5
    Top = 36
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'D&escripci'#243'n:'
    FocusControl = Descripcion
  end
  inherited PanelBotones: TPanel
    Top = 69
    Width = 317
    BevelOuter = bvNone
    TabOrder = 2
    inherited OK_DevEx: TcxButton
      Left = 152
      Top = 5
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 232
      Top = 5
    end
  end
  object Codigo: TEdit [3]
    Left = 67
    Top = 10
    Width = 121
    Height = 21
    HelpContext = 101
    CharCase = ecUpperCase
    MaxLength = 6
    TabOrder = 0
  end
  object Descripcion: TEdit [4]
    Left = 67
    Top = 32
    Width = 241
    Height = 21
    HelpContext = 101
    TabOrder = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
