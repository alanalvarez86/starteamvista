unit FEscogeGlobal_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, Grids, DBGrids, StdCtrls, Buttons, ExtCtrls,
     ZBaseEscogeGrid_DevEx,
     ZetaDBGrid, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters,
  cxControls, cxStyles, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid, cxButtons;

type
  TEscogeGlobal_DevEx = class(TBaseEscogeGrid_DevEx)
    TipoGlobal: TComboBox;
    Label1: TLabel;
    gl_codigo: TcxGridDBColumn;
    gl_descrip: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TipoGlobalChange(Sender: TObject);
  private
    { Private declarations }
    procedure FiltraTipoGlobal;
  protected
    { Protected declarations }
    function GetFormula: String ; override;
    procedure QuitaFiltro;
    procedure Connect; override;
  public
    { Public declarations }
  end;

function PickGlobal: String;

var
  EscogeGlobal_DevEx: TEscogeGlobal_DevEx;

implementation

uses DDiccionario,
     ZetaCommonClasses;

{$R *.DFM}

function PickGlobal: String;
begin
     if ( EscogeGlobal_DevEx = nil ) then
        EscogeGlobal_DevEx:= TEscogeGlobal_DevEx.Create( Application.MainForm );
     with EscogeGlobal_DevEx do
     begin
          ShowModal;
          if ( ModalResult = mrOk ) then
             Result := Formula;
          QuitaFiltro;
     end;
end;

{ *********** TEscogeGlobal ********* }

procedure TEscogeGlobal_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H66216_Escoge_global;
     {
     SqlNumber:= Q_ESCOGE_GLOBAL;
     }
end;

procedure TEscogeGlobal_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with TipoGlobal do
     begin
          if ( ItemIndex < 0 ) then
             ItemIndex := 5;
     end;
end;

procedure TEscogeGlobal_DevEx.Connect;
begin
     with dmDiccionario do
     begin
          ConectaGlobal;
          DataSource.DataSet := cdsGlobal;
     end;
end;

function TEscogeGlobal_DevEx.GetFormula: String;
begin
     with dmDiccionario.cdsGlobal do
     begin
          if not IsEmpty then
             Result:= 'Global( ' + FieldByName( 'GL_CODIGO' ).AsString + ' )'
          else
              Result:= '';
     end;
     {
     if DatasetLleno( FQuery ) then
        Result:= 'Global( ' + FQuery.FieldByName('GL_CODIGO').AsString + ' )';
     }
end;

procedure TEscogeGlobal_DevEx.QuitaFiltro;
begin
     with dmDiccionario.cdsGlobal do
     begin
          if Filtered then
          begin
               Filtered := False;
               Filter := '';
          end;
     end;
end;

procedure TEscogeGlobal_DevEx.TipoGlobalChange(Sender: TObject);
begin
     FiltraTipoGlobal;
     {
     GeneraQuery;
     }
end;

procedure TEscogeGlobal_DevEx.FiltraTipoGlobal;
begin
     with dmDiccionario.cdsGlobal do
     begin
          try
             DisableControls;
             with TipoGlobal do
             begin
                  if ( ItemIndex <= 4 ) then
                  begin
                       Filter := Format( 'GL_TIPO = %d', [ ItemIndex ] );
                       Filtered := True;
                  end
                  else
                  begin
                       Filtered := False;
                       Filter := '';
                  end;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

{
procedure TEscogeGlobal.GeneraQuery;
var
   sScript: String;
begin
     FQuery := dmQuerys.QueryMgr.CreateQuery( SqlNumber, False );
     with FQuery do
     begin
          sScript := SqlText;
          if ( TipoGlobal.ItemIndex <= 4 ) then
             sScript := Format( sScript, [ 'where GL_TIPO = ' + IntToStr( TipoGlobal.ItemIndex ) ] )
          else
              sScript := Format( sScript, [ '' ] );
          Sql.Text:= sScript;
          Open;
     end;
     Datasource.Dataset:= FQuery;
end;
}

end.

