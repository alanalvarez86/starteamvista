unit ZBaseConsulta;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBGrids, StdCtrls, ExtCtrls, ComCtrls,
     {$ifndef VER130}Variants,{$endif}
     ZAccesosMgr,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaMessages,
     cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, dxSkinsCore,
  dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxPC;

type
  TBaseConsulta = class;
{$ifndef TRESS_DELPHIXE5_UP}
  TTabForm = class (TTabSheet)
  private
   // Private declarations
    FForma: TBaseConsulta;
    procedure SetForma( Value: TBaseConsulta );
  public
    // Public declarations
    property Forma: TBaseConsulta read FForma write SetForma;
    procedure AbreForma;
    procedure CierraForma;
  end;
{$endif}
  //DevEx: cxTabForm
  TTabForm_DevEx = class (TcxTabSheet)
  private
    { Private declarations }
    FForma: TBaseConsulta;
    procedure SetForma( Value: TBaseConsulta );
  public
    { Public declarations }
    property Forma: TBaseConsulta read FForma write SetForma;
    procedure AbreForma;
    procedure CierraForma;
  end;
  TBaseConsulta = class(TForm)
    DataSource: TDataSource;
    PanelIdentifica: TPanel;
    ValorActivo1: TPanel;
    ImgNoRecord: TImage;
    Slider: TSplitter;
    ValorActivo2: TPanel;
    textoValorActivo1: TLabel;
    textoValorActivo2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDblClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    FIndexDerechos: Integer;
    FEsConsulta: Boolean;
    FCanLookup: Boolean;
    FTipoValorActivo1: TipoEstado;
    FTipoValorActivo2: TipoEstado;
{$ifndef TRESS_DELPHIXE5_UP}
    FPestania: TTabForm;
{$endif}
    //DevEx: Pestania para los tabs del nuevo PageControl
    FPestania_DevEx: TTabForm_DevEx;
    procedure AsignaDblClickControls;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
{$ifndef TRESS_DELPHIXE5_UP}
    function ConvierteATDBGrid (DevEx_TableView: TcxGridDBTableView): TDBGrid;
{$endif}
  protected
    { Protected declarations }
    function NoData( Fuente: TDatasource ): Boolean;
    function NoHayDatos: Boolean; dynamic;
    function PuedeAgregar( var sMensaje: String ): Boolean; dynamic;
    function PuedeBorrar( var sMensaje: String ): Boolean; dynamic;
    function PuedeModificar( var sMensaje: String ): Boolean; dynamic;
    function PuedeImprimir( var sMensaje: String ): Boolean; dynamic;
    function ValoresGrid : Variant;virtual;
    procedure Agregar; virtual;
    procedure Borrar; virtual;
    procedure Modificar; virtual;
    procedure Imprimir; virtual;
    procedure ImprimirForma; virtual;
    procedure Connect; virtual; abstract;
    procedure Refresh; virtual; abstract;
    procedure Disconnect; virtual;
    procedure InitValoresActivos;
    procedure Exportar;virtual;
    procedure ReconnectGrid; virtual;
    procedure RefreshGrid; virtual;
    procedure ConnectGrid; virtual;
    procedure DisconnectGrid; virtual;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    property IndexDerechos: Integer read FIndexDerechos write FIndexDerechos;
    property EsConsulta: Boolean read FEsConsulta write FEsConsulta;
    property CanLookup: Boolean read FCanLookup write FCanLookup;
{$ifndef TRESS_DELPHIXE5_UP}
    property Pestania: TTabForm read FPestania write FPestania;
{$endif}
    property Pestania_DevEx: TTabForm_DevEx read FPestania_DevEx write FPestania_DevEx;
    property TipoValorActivo1: TipoEstado read FTipoValorActivo1 write FTipoValorActivo1;
    property TipoValorActivo2: TipoEstado read FTipoValorActivo2 write FTipoValorActivo2;
    function DoConnect: Boolean;
    function CheckDerechos( const iDerecho: Integer ): Boolean;
    procedure AsignaValoresActivos;
    procedure DoDisconnect;
    procedure DoRefresh;
    procedure DoLookup; virtual;
    procedure DoDelete;
    procedure DoEdit;
    procedure DoInsert;
    procedure DoPrint;
    procedure DoPrintForma;
    procedure DoExportar;
    procedure ReConnect;
    procedure CargaParametrosReporte(Parametros: TZetaParams);virtual;
    procedure DoBestFit;
    //DevEx: Este metodo debe ser publico por las formas que tengan varios grids. Se debe hacer override a Exportar
    //function ConvierteATDBGrid (DevEx_TableView: TcxGridDBTableView): TDBGrid;
    procedure ExportarEspecial ( oGrid: TDBGrid); 
  end;
  TBaseConsultaClass = class of TBaseConsulta;
var
  BaseConsulta: TBaseConsulta;

implementation

uses
  //(@am): Se migro la funcionalidad de impresion y exportacion de grids a la unidad ZImprimeGrid para la compilacion de XE5.
  {$IFDEF TRESS_DELPHIXE5_UP}ZImprimeGrid{$ELSE}FBaseReportes{$ENDIF},
  ZetaDialogo,
  ZetaSmartLists,
  DCliente;

{$R *.DFM}


{$ifndef TRESS_DELPHIXE5_UP}
{ ************** TTabForm ******************* }

procedure TTabForm.SetForma( Value: TBaseConsulta );
begin
     if ( FForma <> Value ) then
     begin
          FForma := Value;
          if ( FForma <> nil ) then
          begin
               Caption := FForma.Caption;
               HelpContext := FForma.HelpContext;
               FForma.Parent := Self;
               FForma.Pestania := Self;
          end;
     end;
end;

procedure TTabForm.AbreForma;
begin
     with FForma do
     begin
          if not Visible then
             Show;
     end;
     PageControl.ActivePage := Self;
end;

procedure TTabForm.CierraForma;
begin
     with FForma do
     begin
          Pestania := nil;
          Close;
     end;
end;
{$endif}

{ ************** TTabForm_DevEx ******************* }
procedure  TTabForm_DevEx.SetForma( Value: TBaseConsulta );
begin
     if ( FForma <> Value ) then
     begin
          FForma := Value;
          if ( FForma <> nil ) then
          begin
               Caption := FForma.Caption;
               HelpContext := FForma.HelpContext;
               FForma.Parent := Self;
               FForma.Pestania_DevEx := Self;
          end;
     end;
end;

procedure TTabForm_DevEx.AbreForma;
begin
     with FForma do
     begin
          if not Visible then
             Show;
     end;
     PageControl.ActivePage := Self;
end;

procedure TTabForm_DevEx.CierraForma;
begin
     with FForma do
     begin
          Pestania_DevEx := nil;
          Close;
     end;
end;

{ ************* TBaseConsulta ************ }

constructor TBaseConsulta.Create( AOwner: TComponent );
begin
     FEsConsulta := True;
     FCanLookup := False;
     FTipoValorActivo1 := stNinguno;
     FTipoValorActivo2 := stNinguno;
     inherited Create( AOwner );
     Align := alClient;
     BorderStyle := bsNone;
     AutoScroll := True;
     InitValoresActivos;
end;

procedure TBaseConsulta.FormCreate(Sender: TObject);
begin
     AsignaDblClickControls;
end;

procedure TBaseConsulta.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   //  Pestania := nil;
   Pestania_DevEx := nil;
end;

procedure TBaseConsulta.FormDblClick(Sender: TObject);
begin
     DoEdit;
end;

procedure TBaseConsulta.AsignaDblClickControls;
var
   i: Integer;
   Control: TControl;
begin
     for i := 0 to ( ControlCount - 1 ) do
     begin
          Control := Controls[ i ];
          if Control is TLabel then
          begin
               with TLabel( Control ) do
               begin
                    if not Assigned( OnDblClick ) then
                       OnDblClick := FormDblClick;
               end;
          end
          else
              if Control is TGroupBox then
              begin
                   with TGroupBox( Control ) do
                   begin
                        if not Assigned( OnDblClick ) then
                           OnDblClick := FormDblClick;
                   end;
              end
              else
                  if Control is TPanel then
                  begin
                       with TPanel( Control ) do
                       begin
                            if not Assigned( OnDblClick ) then
                               OnDblClick := FormDblClick;
                       end;
                  end;
     end;
end;

procedure TBaseConsulta.WMExaminar(var Message: TMessage);
begin
     DblClick;
end;

function TBaseConsulta.CheckDerechos( const iDerecho: Integer ): Boolean;
begin
     Result := ZAccesosMgr.CheckDerecho( IndexDerechos, iDerecho );
end;

{ ********* Manejo de Valores Activos ********** }

procedure TBaseConsulta.InitValoresActivos;
begin
     if ( TipoValorActivo1 = stNinguno ) and ( TipoValorActivo2 = stNinguno ) then
     begin
          PanelIdentifica.Visible := False;
          ValorActivo1.Visible := False;
          ValorActivo2.Visible := False;
          Slider.Visible := False;
     end
     else
     begin
          if ( TipoValorActivo1 = stNinguno ) then
          begin
               ValorActivo1.Align := alNone;
               ValorActivo1.Visible := False;
               ValorActivo2.Align := alClient;
               Slider.Visible := False;
          end;
          if ( TipoValorActivo2 = stNinguno ) then
          begin
               ValorActivo2.Align := alNone;
               ValorActivo2.Visible := False;
               ValorActivo1.Align := alClient;
               Slider.Visible := False;
          end;
     end;
end;

{***DevEx (by am): Se dejo de usar el caption de los paneles para mostrar los valores activos.
Ahora se utilizara una label dentro del panel. ***}
procedure TBaseConsulta.AsignaValoresActivos;
begin
     with dmCliente do
     begin
          textoValorActivo1.Caption := GetValorActivoStr( TipoValorActivo1 );
          textoValorActivo2.Caption := GetValorActivoStr( TipoValorActivo2 )
     end;

end;

{ ************* Conexi�n a Datasets ************* }

function TBaseConsulta.NoData( Fuente: TDatasource ): Boolean;
begin
     with Fuente do
     begin
          if ( Dataset = nil ) then
             Result := False
          else
              Result := Dataset.IsEmpty;
     end;
end;

function TBaseConsulta.NoHayDatos: Boolean;
begin
     with Datasource do
     begin
          if ( Dataset = nil ) then
             Result := False
          else
              Result := Dataset.IsEmpty;
     end;
end;

procedure TBaseConsulta.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     //DevEx (by am): Se conserva la imagen de No Hay Datos solo para la vista clasica
     ImgNoRecord.Visible:= FALSE;
end;

function TBaseConsulta.DoConnect: Boolean;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             try
                Connect;
                ConnectGrid;
                AsignaValoresActivos;
                Result := True;
             except
                   on Error: Exception do
                   begin
                        zExcepcion( '� Error Al Conectar Forma !', '� Ventana No Pudo Ser Conectada A Sus Datos !', Error, 0 );
                        Result := False;
                   end;
             end;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

procedure TBaseConsulta.DoDisconnect;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             DisconnectGrid;
             Disconnect;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

procedure TBaseConsulta.ReConnect;
begin
     DoConnect;
     ReConnectGrid;
end;

procedure TBaseConsulta.DoRefresh;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Refresh;
             RefreshGrid;
             AsignaValoresActivos;
          finally
                 Cursor := oCursor;
          end;
     end;
end;
//DevEx: Este metodo es invocado solo para la vista Actual
procedure TBaseConsulta.DoBestFit;
var
   i: Integer;
begin
     for i := 0 to ( ComponentCount - 1 ) do
     begin
          if ( Components[ i ] is TcxGridDBTableView ) then
          begin
               ( Components[ i ] as TcxGridDBTableView ).ApplyBestFit();
               Break;
          end;
     end;
end;
procedure TBaseConsulta.DoLookup;
begin
end;

procedure TBaseConsulta.Disconnect;
begin
     Datasource.Dataset := nil;
end;

procedure TBaseConsulta.ReconnectGrid;
begin
end;

procedure TBaseConsulta.RefreshGrid;
begin
end;

procedure TBaseConsulta.DisconnectGrid;
begin
end;

procedure TBaseConsulta.ConnectGrid;
begin
end;

{ ******** Manejo de Altas, Bajas, Cambios e Impresi�n ********** }

function TBaseConsulta.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     Result := CheckDerechos( K_DERECHO_ALTA );
     if not Result then
        sMensaje := Format('No Tiene Permiso Para %s Registros', [ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord(ukInsert) )] );
end;

procedure TBaseConsulta.Agregar;
begin
end;

procedure TBaseConsulta.DoInsert;
var
   sMensaje: String;
begin
     if PuedeAgregar( sMensaje ) then
        Agregar
     else
         zInformation( Caption, sMensaje, 0 );
end;

function TBaseConsulta.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result := CheckDerechos( K_DERECHO_BAJA );
     if not Result then
        sMensaje := Format('No Tiene Permiso Para %s Registros', [ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord(ukDelete) )] );
end;

procedure TBaseConsulta.Borrar;
begin
end;

procedure TBaseConsulta.DoDelete;
var
   sMensaje: String;
begin
     if NoHayDatos then
         zInformation( Caption, Format( 'No Hay Datos Para %s', [ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord(ukDelete) )] ), 0 )
     else
         if PuedeBorrar( sMensaje ) then
            Borrar
         else
             zInformation( Caption, sMensaje, 0 );
end;

function TBaseConsulta.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result := CheckDerechos( K_DERECHO_CAMBIO );
     if not Result then
        sMensaje := Format('No Tiene Permiso Para %s Registros', [ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord(ukModify) )] );
end;

procedure TBaseConsulta.Modificar;
begin
end;

procedure TBaseConsulta.DoEdit;
var
   sMensaje: String;
begin
     if NoHayDatos then
         zInformation( Caption, Format('No Hay Datos Para %s', [ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord(ukModify) )] ), 0 )
     else
         if PuedeModificar( sMensaje ) then
            Modificar
         else
             zInformation( Caption, sMensaje, 0 );
end;

function TBaseConsulta.PuedeImprimir( var sMensaje: String ): Boolean;
begin
     Result := CheckDerechos( K_DERECHO_IMPRESION );
     if not Result then                                                           
        sMensaje := 'No Tiene Permiso Para Imprimir Registros';
end;

{***DevEx (by am): Se dejo de usar el caption de los paneles para mostrar los valores activos.
Ahora se utilizara una label dentro del panel. ***}
function TBaseConsulta.ValoresGrid : Variant;
begin
     Result := VarArrayOf( [ textoValorActivo1.Caption,
                             textoValorActivo2.Caption,
                             TipoValorActivo1,
                             TipoValorActivo2 ] );
end;

procedure TBaseConsulta.Imprimir;
 var lImprime : Boolean;
     i : integer;
     Valor : Variant;
 //DevEx
 oGrid: TDBGrid;
begin
     lImprime := FALSE;
     oGrid := nil;
     for i := 0 to ( ComponentCount - 1 ) do
     begin
          if ( (Components[ i ] is TDBGrid) or ( Components[ i ] is TcxGridDBTableView ) ) then
          begin
               lImprime := TRUE;
               if zConfirm( 'Imprimir...', '� Desea Imprimir El Grid De La Pantalla ' + Caption + ' ?', 0, mbYes ) then
               begin
                    Valor := ValoresGrid;
                   if (Components[ i ] is TDBGrid) then
                        {$IFDEF TRESS_DELPHIXE5_UP}ZImprimeGrid{$ELSE}FBaseReportes{$ENDIF}.ImprimirGrid( TDBGrid( Components[ i ] ), TDBGrid( Components[ i ] ).DataSource.DataSet, Caption, 'IM',
                                                Valor[0],Valor[1],Valor[2],Valor[3])
                    else
                    begin
                         try
                             {$IFDEF TRESS_DELPHIXE5_UP}
                                  ZImprimeGrid.ImprimirGrid( (Components[ i ] as TcxGridDBTableView), (Components[ i ] as TcxGridDBTableView).DataController.DataSource.DataSet, Caption, 'IM',
                                                Valor[0],Valor[1],Valor[2],Valor[3]);
                             {$ELSE}
                                  oGrid := ConvierteATDBGrid ( ( Components[ i ] as TcxGridDBTableView ) );  //(@am): En ZImprimeGrid ya se implemento el metodo para TcxGridDBtableview
                                  //if oGrid <> nil then
                                      FBaseReportes.ImprimirGrid( oGrid, oGrid.DataSource.DataSet , Caption, 'IM',
                                                Valor[0],Valor[1],Valor[2],Valor[3]);
                             {$ENDIF}

                         finally
                            oGrid.Free;
                         end;
                    end;
               end;
               Break;
          end;
     end;
     if not lImprime AND
        zConfirm( 'Imprimir...', '� Desea Imprimir La Pantalla ' + Caption + ' ?', 0, mbYes ) then
        Print;
end;

procedure TBaseConsulta.DoPrint;
var
   sMensaje: String;
begin
     if PuedeImprimir( sMensaje ) then
        Imprimir
     else
         zInformation( Caption, sMensaje, 0 );
end;

procedure TBaseConsulta.ImprimirForma;
begin
     ZInformation(Caption, 'Esta Pantalla no Cuenta con Impresi�n de Formas',0);
end;

//CV
procedure TBaseConsulta.DoPrintForma;
var
   sMensaje: String;
begin
     if PuedeImprimir( sMensaje ) then
        ImprimirForma
     else
         zInformation( Caption, sMensaje, 0 );
end;

procedure TBaseConsulta.DoExportar;
var
   sMensaje: String;
begin
     if PuedeImprimir( sMensaje ) then
        Exportar
     else
         zInformation( Caption, 'No Tiene Permiso Para Exportar Registros', 0 );
end;
{$ifndef TRESS_DELPHIXE5_UP}
function TBaseConsulta.ConvierteATDBGrid (DevEx_TableView: TcxGridDBTableView): TDBGrid;
var
i: Integer;
oGrid: TDBGrid;
begin
          oGrid := TDBGrid.Create(Self);
          for i := 0 to DevEx_TableView.ColumnCount -1 do
          begin

               with oGrid.Columns.Add do
               begin
                    Visible       := DevEx_TableView.Columns[ i ].Visible;
                    FieldName     := DevEx_TableView.Columns[ i ].DataBinding.FieldName;
                    Title.Caption := DevEx_TableView.Columns[ i ].Caption;
               end;
          end;
             oGrid.DataSource :=  DevEx_TableView.DataController.DataSource;
          result := oGrid;
end;
{$endif}
procedure TBaseConsulta.Exportar;
 var
    lExporta: Boolean;
    i: integer;
    Valor : Variant;
    //DevEx
    oGrid: TDBGrid;
begin
     lExporta:= FALSE;
     oGrid := nil;
     for i := 0 to ( ComponentCount - 1 ) do
     begin
          if ( (Components[ i ] is TDBGrid) or ( Components[ i ] is TcxGridDBTableView ) ) then
          begin
               lExporta:= TRUE;
               if zConfirm( 'Exportar...', '� Desea exportar a Excel la informaci�n de ' + Caption + ' ?', 0, mbYes ) then
               begin
                    Valor := ValoresGrid;
                    if (Components[ i ] is TDBGrid) then
                              {$IFDEF TRESS_DELPHIXE5_UP}ZImprimeGrid{$ELSE}FBaseReportes{$ENDIF}.ExportarGrid( TDBGrid( Components[ i ] ),
                                                TDBGrid( Components[ i ] ).DataSource.DataSet, Caption, 'IM',
                                                Valor[0],Valor[1],Valor[2],Valor[3])
                    else
                    begin
                         try
                            {$IFDEF TRESS_DELPHIXE5_UP}
                                ZImprimeGrid.ExportarGrid(  ( Components[ i ] as TcxGridDBTableView ),
                                                           (Components[ i ] as TcxGridDBTableView).DataController.DataSource.DataSet, Caption, 'IM',
                                                            Valor[0],Valor[1],Valor[2],Valor[3]);
                            {$ELSE}
                                oGrid := ConvierteATDBGrid ( ( Components[ i ] as TcxGridDBTableView ) );
                                FBaseReportes.ExportarGrid(  oGrid,
                                                            oGrid.DataSource.DataSet, Caption, 'IM',
                                                            Valor[0],Valor[1],Valor[2],Valor[3]);
                            {$ENDIF}

                         finally
                            oGrid.Free;
                         end;
                    end;
               end;
               Break;
          end;
     end;
     if not lExporta then
        zInformation( 'Exportar...',Format( 'La Pantalla %s no Contiene Ning�n Grid para Exportar', [Caption] ), 0);
end;
procedure TBaseConsulta.ExportarEspecial( oGrid: TDBGrid);
var
   Valor : Variant;
begin
     Valor := ValoresGrid;
     {$IFDEF TRESS_DELPHIXE5_UP}ZImprimeGrid{$ELSE}FBaseReportes{$ENDIF}.ExportarGrid(  oGrid,oGrid.DataSource.DataSet, Caption, 'IM', Valor[0],Valor[1],Valor[2],Valor[3]);
end;

procedure TBaseConsulta.CargaParametrosReporte(Parametros: TZetaParams);
begin

end;

end.



