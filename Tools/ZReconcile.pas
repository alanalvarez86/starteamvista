unit ZReconcile;

interface

uses DB, DBClient;

function HandleReconcileError( DataSet: TDataSet;
                               UpdateKind: TUpdateKind;
                               ReconcileError: EReconcileError ): TReconcileAction;

function HandleReconcilePrestamosWrng( DataSet: TDataSet; UpdateKind: TUpdateKind; ReconcileError: EReconcileError; ErrorMsg: string ): TReconcileAction;

function HandleReconcilePrestamosError( DataSet: TDataSet;
                                        UpdateKind: TUpdateKind;
                                        ReconcileError: EReconcileError;
                                        ErrorMsg: string ): TReconcileAction;

function HandleReconcileMontoGlobalError( DataSet: TDataSet;
                                          UpdateKind: TUpdateKind;
                                          ReconcileError: EReconcileError ): TReconcileAction;

implementation

uses ZetaDialogo,
     ZetaClientTools,
     ZetaCommonlists,
     Dialogs,
     ZetaCommonClasses;


function HandleReconcileError( DataSet: TDataSet; UpdateKind: TUpdateKind; ReconcileError: EReconcileError ): TReconcileAction;
var
   sError: String;
begin
     with ReconcileError do
     begin
          { PK_VIOLATION }
          if PK_VIOLATION( ReconcileError ) then //if ( ErrorCode = 3604 ) or ( Pos( 'PRIMARY', Message ) > 0 ) then
             sError := '� C�digo Ya Existe !'
          else
              sError := Message;
          if ( Context <> '' ) then
              sError := sError + CR_LF + Context;
     end;
     ZetaDialogo.zError( 'Error al ' + ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( UpdateKind ) ), sError, 0 ); // UpdateKindStr[ UpdateKind ], sError, 0 );
     case UpdateKind of
          ukDelete: Result := raCancel;
     else
         Result := raAbort;
     end;
end;

function HandleReconcilePrestamosWrng( DataSet: TDataSet; UpdateKind: TUpdateKind; ReconcileError: EReconcileError; ErrorMsg: string ): TReconcileAction;
var
   sError: String;
begin
     with ReconcileError do
     begin
          //sError := ReconcileError.Message;
          sError := ErrorMsg;
          if ( Context <> '' ) then
              sError := sError + CR_LF + Context;
     end;
     if ZetaDialogo.ZWarningConfirm ( 'Error al ' + ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( UpdateKind ) ), 'El Pr�stamo no cumple con los siguientes requisitos: '+CR_LF+ sError + CR_LF +'� Desea grabarlo ?',0,mbOK  )then
     begin
          Result := raCorrect;
     end
     else
         Result := raAbort;

end;


function HandleReconcilePrestamosError( DataSet: TDataSet; UpdateKind: TUpdateKind; ReconcileError: EReconcileError; ErrorMsg: string ): TReconcileAction;
var
   sError: String;
begin
     with ReconcileError do
     begin
          //sError := Message;
          sError := ErrorMsg;
          if ( Context <> '' ) then
              sError := sError + CR_LF + Context;
     end;
     ZetaDialogo.zError( 'Error al ' + ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( UpdateKind ) ), 'El Pr�stamo no cumple con los siguientes requisitos: '+CR_LF+ sError , 0 ); // UpdateKindStr[ UpdateKind ], sError, 0 );
     case UpdateKind of
          ukDelete: Result := raCancel;
     else
         Result := raAbort;
     end;
end;

//US 14933: El usuario requiere que los procesos de nomina sean funcionales con cualquier tipo de periodo (Parte1)
function HandleReconcileMontoGlobalError( DataSet: TDataSet; UpdateKind: TUpdateKind; ReconcileError: EReconcileError ): TReconcileAction;
var
   sError: String;
begin
     with ReconcileError do
     begin
          { PK_VIOLATION }
          if PK_VIOLATION( ReconcileError ) then //if ( ErrorCode = 3604 ) or ( Pos( 'PRIMARY', Message ) > 0 ) then
             sError := '� C�digo Ya Existe !'
          else if FK_VIOLATION( ReconcileError ) then { FK_VIOLATION }
               sError := 'El tipo de n�mina es invalido.'
          else
              sError := Message;
          if ( Context <> '' ) then
              sError := sError + CR_LF + Context;
     end;
     ZetaDialogo.zError( 'Error al ' + ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( UpdateKind ) ), sError, 0 );
     case UpdateKind of
          ukDelete: Result := raCancel;
     else
         Result := raAbort;
     end;
end;



end.
