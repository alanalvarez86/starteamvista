unit ZBaseConstruyeFormula;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Buttons, StdCtrls, ExtCtrls, ComCtrls,
     DDiccionario,
     ZetaTipoEntidad,
     ZetaCommonLists,
     ZetaCommonClasses;

type
  TBaseConstruyeFormula = class(TForm)
    Memo: TMemo;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    CampoBtn: TBitBtn;
    FuncionBtn: TBitBtn;
    GlobalBtn: TBitBtn;
    TestBtn: TBitBtn;
    OkImg: TImage;
    ErrorImg: TImage;
    procedure CancelarClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CampoBtnClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure FuncionBtnClick(Sender: TObject);
    procedure GlobalBtnClick(Sender: TObject);
    procedure TestBtnClick(Sender: TObject);
    procedure MemoChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FFormula: String;
    FPosCursor: Integer;
    FTipoEvaluador: eTipoEvaluador;
    FTruncar: Boolean;
    procedure SetFormula( const sFormula: String  );
    procedure SetTruncar( const lValue: Boolean );
    procedure CierraForma;
  protected
    FEntidadActiva: TipoEntidad;
    FParams : TStrings;
    FRelaciones: Boolean;
    procedure AgregaTexto( sTexto: String );
  public
    { Public declarations }
    property Formula: String read FFormula write SetFormula;
    property PosCursor: Integer read FPosCursor write FPosCursor;
    property EntidadActiva: TipoEntidad read FEntidadActiva write FEntidadActiva;
    property TipoEvaluador: eTipoEvaluador read FTipoEvaluador write FTipoEvaluador;
    property Truncar: Boolean read FTruncar write SetTruncar;
    property Params: TStrings read FParams write FParams;
    property Relaciones: Boolean read FRelaciones write FRelaciones;
  end;


var
  BaseConstruyeFormula: TBaseConstruyeFormula;

implementation

uses ZetaCommonTools,
     ZetaDialogo,
     FBuscaCampos,
     FEscogeFuncion,
     FEscogeGlobal,
     DCatalogos;

{$R *.DFM}

const
     K_MAXIMO = 255;


function PalabraPosicionada( Sender: TCustomEdit ) : String;
var
   iPos, iCursor, iPosInicial, iPosFinal: Integer;
   sLetra, sTexto: String;

function LetraValida( const cLetra: Char ): Boolean;
begin
     Result := ( ( cLetra = '_' ) or ( cLetra in [ 'A'..'Z', '0'..'9' ] ) );
end;

begin           // Palabra Posicionada
     with Sender do
     begin
          sTexto := Text;
          if ( SelLength > 0 ) then
             Result := SelText
          else
          begin
               iCursor := SelStart - 1;
               // Brinca par�ntesis abierto y espacios
               while ( iCursor >= 0 ) do
               begin
                    sLetra := AnsiUpperCase( sTexto[ iCursor + 1 ] );
                    if ( sLetra[ 1 ] in [ ' ', '(' ] ) then
                       iCursor := iCursor - 1
                    else
                        Break;
               end;

               // Busca Inicio Palabra
               iPos := iCursor;
               while ( iPos >= 0 ) do
               begin
                    sLetra := AnsiUpperCase( sTexto[ iPos + 1 ] );
                    if not LetraValida( sLetra[ 1 ] ) then
                       Break;
                    iPos := iPos - 1;
               end;
               iPosInicial := iPos + 2;

               // Busca Fin Palabra
               iPos := iCursor + 1;
               while ( iPos < Length( sTexto ) ) do
               begin
                    sLetra := AnsiUpperCase( sTexto[ iPos + 1 ] );
                    if not LetraValida( sLetra[ 1 ] ) then
                       Break;
                    iPos := iPos + 1;
               end;
               iPosFinal := iPos;
               Result := Copy( sTexto, iPosInicial, ( iPosFinal - iPosInicial + 1 ) );
          end;
     end;
end;

procedure LlamaAyuda( const sTexto: String );
begin
     Application.HelpCommand( HELP_PARTIALKEY,  Longint( sTexto + '()' ));
end;

{ *********** TConstruyeFormula ************ }

procedure TBaseConstruyeFormula.FormShow(Sender: TObject);
begin
     ActiveControl := Memo;
     Memo.SelStart := FPosCursor;
     OkImg.Visible := FALSE;
     ErrorImg.Visible := FALSE;
end;

procedure TBaseConstruyeFormula.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     if ( Key = 112 ) and ( ActiveControl is TMemo ) then
     begin
          Key := 0;
          if ( Length( Trim(Memo.Lines.Text))=0) then
             Application.HelpContext( H66211_Evaluador_expresiones )
          else
              LlamaAyuda( PalabraPosicionada( TCustomEdit( ActiveControl ) ) );
     end
     else
         if ( Key = 27 ) and ( ActiveControl is TMemo ) then
         begin
              Key := 0;
              CancelarClick( Sender )
         end
         else
             if ( Key = 112 ) and not ( ActiveControl is TMemo) then
             begin
                  Key := 0;
                  Application.HelpContext( H66211_Evaluador_expresiones);
             end
             else
                 inherited;
end;

procedure TBaseConstruyeFormula.CierraForma;
begin
     ModalResult := mrOk;
     FFormula := Memo.Lines.Text;
     if FTruncar then
        FFormula := Copy( FFormula, 1, K_MAXIMO );
end;

procedure TBaseConstruyeFormula.OKClick(Sender: TObject);
begin
     if FTruncar and ( Length( Memo.Lines.Text ) > K_MAXIMO ) then
     begin
          if ZetaDialogo.ZConfirm( Caption,
                                   'La Longitud De La F�rmula Excede Los ' + IntToStr( K_MAXIMO ) + ' Caracteres' +
                                   CR_LF +
                                   '� Desea Recortarla ?', 0, mbCancel ) then
          begin
               CierraForma;
          end;
     end
     else
         CierraForma;
end;

procedure TBaseConstruyeFormula.CancelarClick(Sender: TObject);
begin
     ModalResult := mrCancel;
end;

procedure TBaseConstruyeFormula.AgregaTexto( sTexto : String );
var
   sTemp: String;
   iPos: Integer;
begin
     if ZetaCommonTools.StrLleno( sTexto ) then
     begin
          sTexto := ' ' + sTexto;
          with Memo do
          begin
               iPos := SelStart;
               with Lines do
               begin
                    sTemp := Copy( Text, 0, iPos ) + sTexto + Copy( Text, iPos + 1, Length( Text ) - 1 );
                    Text:= sTemp;
               end;
               SelStart := iPos + Length( sTexto );
               SetFocus;
          end;
     end;
end;

procedure TBaseConstruyeFormula.SetTruncar(const lValue: Boolean);
begin
     FTruncar := lValue;
     with Memo do
     begin
          if FTruncar then
             MaxLength := K_MAXIMO
          else
              MaxLength := 0;
     end;
end;

procedure TBaseConstruyeFormula.SetFormula( const sFormula: String  );
begin
     FFormula := sFormula;
     with Memo.Lines do
     begin
          Clear;
          if StrLleno( sFormula ) then
             Add( sFormula );
     end;
end;

procedure TBaseConstruyeFormula.CampoBtnClick(Sender: TObject);
begin
     AgregaTexto( PickCampo( FEntidadActiva, FRelaciones ) );
end;

procedure TBaseConstruyeFormula.FuncionBtnClick(Sender: TObject);
begin
     AgregaTexto( PickFuncion );
end;

procedure TBaseConstruyeFormula.GlobalBtnClick(Sender: TObject);
begin
     AgregaTexto( PickGlobal );
end;


procedure TBaseConstruyeFormula.MemoChange(Sender: TObject);
begin
     ErrorImg.Visible := False;
     OkImg.Visible := False;
end;

procedure TBaseConstruyeFormula.TestBtnClick(Sender: TObject);
var
   sFormula: String;
begin
     OkImg.Visible := True;
     ErrorImg.Visible := False;
     sFormula := Memo.Lines.Text;
     if not dmDiccionario.PruebaFormula( sFormula, EntidadActiva, FTipoEvaluador, FParams ) then
     begin
          OkImg.Visible := False;
          ErrorImg.Visible := True;
          ZetaDialogo.ZError( Caption, sFormula, 0 );
     end;
end;

procedure TBaseConstruyeFormula.FormCreate(Sender: TObject);
begin
     FRelaciones := TRUE;
     {$ifdef WORKFLOWCFG}
     Testbtn.Visible := FALSE;
     {$endif}
end;

end.
