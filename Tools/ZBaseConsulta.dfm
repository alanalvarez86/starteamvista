object BaseConsulta: TBaseConsulta
  Left = 203
  Top = 141
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'BaseConsulta'
  ClientHeight = 273
  ClientWidth = 429
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnDblClick = FormDblClick
  PixelsPerInch = 96
  TextHeight = 13
  object PanelIdentifica: TPanel
    Left = 0
    Top = 0
    Width = 429
    Height = 19
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object ImgNoRecord: TImage
      Left = 0
      Top = 0
      Width = 16
      Height = 19
      Hint = 'No Existe Informaci'#243'n'
      Align = alLeft
      ParentShowHint = False
      Picture.Data = {
        07544269746D617042010000424D420100000000000076000000280000001100
        0000110000000100040000000000CC0000000000000000000000100000001000
        000000000000000080000080000000808000800000008000800080800000C0C0
        C000808080000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
        FF00DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDDDDDDD0000000DD0000000000
        000DD0000000D999FFFFFFFF9990D0000000D999900000099990D0000000D099
        999FF99999F0D0000000D0F99999999990F0D0000000D0FFF999999FFFF0D000
        0000D0F00099990000F0D0000000D0FFF999999FFFF0D0000000D0F999999999
        90F0D0000000D099999FF99999F0D0000000D999900000099990D0000000D999
        FFFFFFFF9990D0000000D99000000000099DD0000000DDDDDDDDDDDDDDDDD000
        0000DDDDDDDDDDDDDDDDD0000000}
      ShowHint = True
      Transparent = True
      Visible = False
    end
    object Slider: TSplitter
      Left = 256
      Top = 0
      Height = 19
    end
    object ValorActivo1: TPanel
      Left = 16
      Top = 0
      Width = 240
      Height = 19
      Align = alLeft
      Alignment = taLeftJustify
      BorderWidth = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object textoValorActivo1: TLabel
        Left = 3
        Top = 3
        Width = 83
        Height = 13
        Align = alTop
        Caption = 'textoValorActivo1'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object ValorActivo2: TPanel
      Left = 259
      Top = 0
      Width = 170
      Height = 19
      Align = alClient
      Alignment = taRightJustify
      BorderWidth = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object textoValorActivo2: TLabel
        Left = 84
        Top = 3
        Width = 83
        Height = 13
        Align = alTop
        Alignment = taRightJustify
        Caption = 'textoValorActivo2'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object DataSource: TDataSource
    OnDataChange = DataSourceDataChange
    Left = 160
    Top = 72
  end
end
