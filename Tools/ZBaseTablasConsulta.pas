unit ZBaseTablasConsulta;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, Db, ExtCtrls, DBClient,
     ZBaseConsulta,
     ZetaDBGrid,
     ZetaClientDataSet;

type
  TTablaOpcion = class(TBaseConsulta)
    DBGrid: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FDataset: TZetaClientDataset;
  protected
    { Protected declarations }
    property ZetaDataset: TZetaClientDataSet read FDataset write FDataset;
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure AfterCreate; virtual;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;
  TTablaLookup = class(TTablaOpcion)
  private
    { Private declarations }
    procedure SetLookupDataset(const Value: TZetaLookupDataSet);
    function GetLookupDataset: TZetaLookupDataSet;
  protected
    { Protected declarations }
    property LookupDataset: TZetaLookupDataSet read GetLookupDataset write SetLookupDataset;
    procedure Connect; override;
  end;

var
  TablaOpcion: TTablaOpcion;

implementation

{$R *.DFM}

uses ZetaBuscador,
     ZetaCommonClasses;

{ ******** TTablaOpcion ****** }

procedure TTablaOpcion.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     AfterCreate;
end;

procedure TTablaOpcion.AfterCreate;
begin
end;

procedure TTablaOpcion.Agregar;
begin
     ZetaDataset.Agregar;
end;

procedure TTablaOpcion.Borrar;
begin
     ZetaDataset.Borrar;
end;

procedure TTablaOpcion.Connect;
begin
     ZetaDataset.Conectar;
     DataSource.DataSet := ZetaDataset;
end;

procedure TTablaOpcion.Modificar;
begin
     ZetaDataset.Modificar;
end;

procedure TTablaOpcion.Refresh;
begin
     ZetaDataset.Refrescar;
end;

procedure TTablaOpcion.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', ZetaDataset );
end;

{ ********** TTablaLookup ********** }

function TTablaLookup.GetLookupDataset: TZetaLookupDataSet;
begin
     Result := TZetaLookupDataset( Self.ZetaDataset );
end;

procedure TTablaLookup.SetLookupDataset(const Value: TZetaLookupDataSet);
begin
     Self.ZetaDataset := Value;
end;

procedure TTablaLookup.Connect;
begin
     inherited Connect;
     Caption := LookupDataset.LookupName;
end;

end.
