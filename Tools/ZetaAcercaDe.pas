unit ZetaAcercaDe;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
     Buttons, ExtCtrls, ComCtrls, Checklst, TMultiP, jpeg, dxGDIPlusClasses,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, cxImage;

type
  TZAcercaDe = class(TForm)
    OK: TBitBtn;
    PageControl: TPageControl;
    PageVersion: TTabSheet;
    PageAutorizacion: TTabSheet;
    PageRecursos: TTabSheet;
    Label2: TLabel;
    Label3: TLabel;
    DiscoGB: TGroupBox;
    DiscoTotalLBL: TLabel;
    DiscoDisponible: TLabel;
    DiscoTotal: TLabel;
    DiscoDisponibleLBL: TLabel;
    DiscoOcupadoLBL: TLabel;
    DiscoOcupado: TLabel;
    MemoriaGB: TGroupBox;
    RecursosLibresLBL: TLabel;
    RecursosLibres: TLabel;
    MemoriaRAMLBL: TLabel;
    MemoriaRAM: TLabel;
    MemoriaRAMDisponible: TLabel;
    MemoriaRAMDisponibleLBL: TLabel;
    MemoriaVirtualLBL: TLabel;
    MemoriaVirtual: TLabel;
    MemoriaVirtualDisponible: TLabel;
    MemoriaVirtualDisponibleLBL: TLabel;
    ModulosGB: TGroupBox;
    Modulos: TCheckListBox;
    Caducidad: TGroupBox;
    Prestamos: TCheckListBox;
    SerialNumberLBL: TLabel;
    SerialNumber: TLabel;
    EmpresaLBL: TLabel;
    Empresa: TLabel;
    EmpleadosLBL: TLabel;
    Empleados: TLabel;
    VersionLBL: TLabel;
    Version: TLabel;
    VencimientoLBL: TLabel;
    Vencimiento: TLabel;
    UsuariosLBL: TLabel;
    Usuarios: TLabel;
    EsKit: TCheckBox;
    EsKitLBL: TLabel;
    PanelVersion: TPanel;
    VersionBuild: TLabel;
    Label1: TLabel;
    Copyright: TLabel;
    lbServicePackDatos: TLabel;
    SQLEngineLBL: TLabel;
    SQLEngine: TLabel;
    LblFileVersion: TLabel;
    Label6: TLabel;
    lblPlataforma: TLabel;
    ProgramIcon: TcxImage;
    procedure OKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure LlenaPageRecursos;
    procedure LlenaPageAutorizacion;
  public
    { Public declarations }
  end;

var
  ZAcercaDe: TZAcercaDe;

implementation

{$R *.DFM}

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaClientTools,
     ZGlobalTress,
     FAutoClasses,
     DGlobal;

function BytesAMegas( const Bytes: Integer ): Real;
begin
     Result := Bytes / ( 1024 * 1024 );
end;

procedure TZAcercaDe.FormCreate(Sender: TObject);
const
     {$ifdef DOS_CAPAS}
     //K_MENSAJE ='Acerca De %s Profesional';
     {$else}
     //K_MENSAJE ='Acerca De %s %s';
     {$endif}
     //DevEx(by am): Se requirio que solo apareciera el nombre de la aplicacion.
     K_MENSAJE = 'Acerca de %s';
var
   sVersion: String;
   {$ifndef VALIDADORDIMM}
   sDatos: String;
   {$endif}
begin
     //DevEx(by am): Se requirio que solo apareciera el nombre de la aplicacion.
     //Caption := Format( K_MENSAJE, [ Application.Title ,Autorizacion.GetPlataformaStr] );
     Caption := Format( K_MENSAJE, [ Application.Title ] );
     sVersion := GetApplicationProductVersion;
{$ifdef HAY_AUTORIZACION}
     if Autorizacion.EsDemo then
        sVersion := sVersion + ' ( DEMO )';
{$endif}
     VersionBuild.Caption := sVersion;
     LblFileVersion.Caption := GetApplicationVersionBuild;
     LlenaPageAutorizacion;
     LlenaPageRecursos;
     HelpContext := H00022_Acerca_de_Tress;
     PageControl.ActivePage := PageVersion;
     {$ifdef RDDAPP}
     {$else}
     {$ifdef VALIDADORDIMM}
     {$else}
     sDatos := Global.GetGlobalString(K_GLOBAL_VERSION_DATOS);
     {$endif}
     {$endif}

     {$ifdef VALIDADORDIMM}
     lbServicePackDatos.Caption := ZetaCommonClasses.VACIO;
     {$else}
     if Length(sDatos) = 0 then
        sDatos := '<Indefinida>';
     lbServicePackDatos.Caption := 'Versi�n Datos: ' + sDatos;
     {$endif}

     {$ifdef WORKFLOWCFG}
     lbServicePackDatos.Visible := False;
     PanelVersion.Height := 79;
     Label1.Top := 40;
     Copyright.Top := 56;
     {$endif}

     {@(am): La descripcion en "Derechos reservados" era estatica. Esto se cambia
     para que al igual que la etiqueta de "Version <a�o>" sea dinamica, y no sea necesario cambiarla manualmente
     cada a�o.}
     Copyright.Caption :=  'Derechos Reservados 1996-2016';
end;

procedure TZAcercaDe.OKClick(Sender: TObject);
begin
     Close;
end;

procedure TZAcercaDe.LlenaPageRecursos;
var
   rSize, rAvailable: Real;
   MemoryStatus: TMemoryStatus;
begin
     MemoryStatus.dwLength := SizeOf( MemoryStatus );
     GlobalMemoryStatus( MemoryStatus );
     with MemoryStatus do
     begin
          RecursosLibres.Caption := Format( '%.0n', [( 100 - dwMemoryLoad ) / 1 ] ) + '%';
          MemoriaRAM.Caption := Format( '%.2n MegaBytes', [ BytesAMegas( dwTotalPhys ) ] );
          MemoriaRAMDisponible.Caption := Format( '%.2n MegaBytes', [ BytesAMegas( dwAvailPhys ) ] );
          MemoriaVirtual.Caption := Format( '%.2n MegaBytes', [ BytesAMegas( dwTotalVirtual ) ] );
          MemoriaVirtualDisponible.Caption := Format( '%.2n MegaBytes', [ BytesAMegas( dwAvailVirtual ) ] );
     end;
     rSize := BytesAMegas( DiskSize( 0 ) );
     rAvailable := BytesAMegas( DiskFree( 0 ) );
     DiscoTotal.Caption := Format( '%.2n MegaBytes', [ rSize ] );
     DiscoDisponible.Caption := Format( '%.2n MegaBytes', [ rAvailable ] );
     DiscoOcupado.Caption := Format( '%.2n', [ 100 * ( 1 - rAvailable / rSize ) ] ) + '%';
end;

procedure TZAcercaDe.LlenaPageAutorizacion;
begin
     with Autorizacion do
     begin
          if EsDemo then
          begin
               Self.SerialNumber.Caption := 'DEMO';
               Self.Empresa.Caption := 'DEMO';
          end
          else
          begin
               Self.SerialNumber.Caption := IntToStr( NumeroSerie );
               Self.Empresa.Caption := IntToStr( Empresa );
          end;
          Self.Version.Caption := Version;
          Self.Empleados.Caption := GetEmpleadosStr;
          Self.Vencimiento.Caption := GetVencimientoStr;
          Self.Caducidad.Caption := ' Pr�stamos: ' + GetCaducidadStr + ' ';
          Self.Usuarios.Caption := IntToStr( Usuarios );
          Self.EsKit.Checked := EsKit;
          Self.SQLEngine.Caption := GetSQLEngineStr;
          Self.lblPlataforma.Caption := GetPlataformaStr; 
          GetModulos( Self.Modulos );
          GetPrestamos( Self.Prestamos );
     end;
end;

end.

