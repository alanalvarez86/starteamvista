unit ZetaAliasCrear;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, ComCtrls,
     ZetaWizard,
     ZetaAliasManager;

type
  TAliasCrear = class(TForm)
    Wizard: TZetaWizard;
    Anterior: TZetaWizardButton;
    Siguiente: TZetaWizardButton;
    CrearAlias: TZetaWizardButton;
    Salir: TZetaWizardButton;
    PageControl: TPageControl;
    Inicio: TTabSheet;
    Parametros: TTabSheet;
    Imagen: TImage;
    MensajeInicial: TMemo;
    AliasNameLBL: TLabel;
    UserNameLBL: TLabel;
    UserName: TEdit;
    OpenDialog: TOpenDialog;
    ServerNameLBL: TLabel;
    ServerName: TEdit;
    BuscarServerName: TSpeedButton;
    AliasName: TComboBox;
    ComputerName: TEdit;
    ComputerNameLBL: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure BuscarServerNameClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure AliasNameChange(Sender: TObject);
  private
    { Private declarations }
    FAliasManager: TZetaAliasManager;
    function GetAlias: String;
    function GetFullDatabaseName: String;
    procedure SetAliasManager( Value: TZetaAliasManager );
    procedure SetAlias( const sValue: String );
    procedure SetValues;
  public
    { Public declarations }
    property AliasManager: TZetaAliasManager read FAliasManager write SetAliasManager;
    property Alias: String read GetAlias write SetAlias;
    property FullDatabaseName: String read GetFullDatabaseName;
  end;

var
  AliasCrear: TAliasCrear;

function CreaUnAlias( Manager: TZetaAliasManager; var sAliasName: String ): Boolean;
function CreaUnAliasDatabase( Manager: TZetaAliasManager; var sAliasName, sDatabase: String ): Boolean;

implementation

uses ZetaCommonTools,
     ZetaWinAPITools,
     ZetaODBCTools,
     ZetaCommonClasses,
     ZetaDialogo;

{$R *.DFM}

function CreaUnAliasDatabase( Manager: TZetaAliasManager; var sAliasName, sDatabase: String ): Boolean;
begin
     with TAliasCrear.Create( Application.Mainform ) do
     begin
          try
             AliasManager := Manager;
             Alias := sAliasName;
             ShowModal;
             Result := Wizard.Ejecutado;
             if Result then
             begin
                  sAliasName := Alias;
                  sDatabase := FullDatabaseName;
             end;
          finally
                 Free;
          end;
     end;
end;

function CreaUnAlias( Manager: TZetaAliasManager; var sAliasName: String ): Boolean;
var
   sDatabase: String;
begin
     Result := CreaUnAliasDatabase( Manager, sAliasName, sDatabase );
end;

{ ******************* TAliasCrear *********************** }

procedure TAliasCrear.FormCreate(Sender: TObject);
begin
     Wizard.Primero;
     ComputerName.Text := ZetaWinAPITools.GetComputerName;
     {$ifdef INTERBASE}
     Caption := 'Construcci�n de Alias de BDE';
     BuscarServerName.Visible := True;
     {$endif}
     {$ifdef MSSQL}
     Caption := 'Datasources de ODBC';
     BuscarServerName.Visible := False;
     {$endif}
     with MensajeInicial.Lines do
     begin
          Clear;
          Add( '' );
          {$ifdef INTERBASE}
          Add( 'Este proceso crea un Alias del' );
          Add( 'BDE (Borland Database Engine)' );
          Add( 'necesario para efectuar la conexi�n' );
          Add( 'con base de datos de Tress Windows' );
          Add( '' );
          Add( 'El Alias es creado para conectarse' );
          Add( 'a una base de datos de Interbase' );
          {$endif}
          {$ifdef MSSQL}
          Add( 'Este proceso crea un System' );
          Add( 'Datasource de Microsoft ODBC' );
          Add( 'necesario para efectuar la conexi�n' );
          Add( 'con base de datos de Tress Windows' );
          Add( '' );
          Add( 'El Datasource de ODBC es creado para' );
          Add( 'conectarse a una base de datos de' );
          Add( 'Microsoft SQL Server 2000' );
          {$endif}
     end;
     {$ifdef INTERBASE}
     HelpContext := H00030_Construccion_de_Alias_de_BDE;
     {$endif}
     {$ifdef MSSQL}
     HelpContext := 0;
     {$endif}
end;

procedure TAliasCrear.SetAliasManager( Value: TZetaAliasManager );
begin
     if ( FAliasManager <> Value ) then
     begin
          FAliasManager := Value;
          with FAliasManager do
          begin
               GetAliasList( AliasName.Items );
          end;
     end;
end;

function TAliasCrear.GetAlias: String;
begin
     Result := AliasName.Text;
end;

procedure TAliasCrear.SetAlias( const sValue: String );
begin
     AliasName.Text := sValue;
     SetValues;
end;

procedure TAliasCrear.BuscarServerNameClick(Sender: TObject);
var
   sArchivo: String;
begin
     sArchivo := ServerName.Text;
     with OpenDialog do
     begin
          DefaultExt := 'gdb';
          Filter := 'Archivos Interbase (*.gdb)|*.gdb|Todos (*.*)|*.*' ;
          FileName := ExtractFileName( sArchivo );
          InitialDir := ExtractFilePath( sArchivo );
          Title := 'Buscar Base de Datos Interbase';
          if Execute then
          begin
               ServerName.Text := FileName;
          end;
     end;
end;

procedure TAliasCrear.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     if Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
     begin
          if StrVacio( Alias ) then
          begin
               ZetaDialogo.zError( Caption, 'Nombre de Alias No Ha Sido Definido', 0 );
               ActiveControl := AliasName;
               CanMove := False;
          end;
     end;
end;

procedure TAliasCrear.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual( Parametros ) then
     begin
          ActiveControl := AliasName;
     end;
end;

function GetDatabaseComputerName( const sValue: String ): String;
var
   iPos: Integer;
begin
     iPos := Pos( ':\', sValue );
     if ( iPos > 3 ) then
        Result := Copy( sValue, 1, ( iPos - 3 ) ) 
     else
         Result := ZetaWinAPITools.GetComputerName;
end;

function GetDatabaseFileName( const sValue: String ): String;
var
   iPos: Integer;
begin
     iPos := Pos( ':\', sValue );
     if ( iPos > 1 ) then
        Result := Copy( sValue, ( iPos - 1 ), ( Length( sValue ) - ( iPos - 2 ) ) )
     else
         Result := sValue;
end;

procedure TAliasCrear.SetValues;
begin
     with FAliasManager do
     begin
          if ZetaCommonTools.StrLleno( Alias ) and Exists( Alias ) then
          begin
               GetParams( Alias, Params );  //  Get the parameters for an alias
               with Params do
               begin
                    ComputerName.Text := GetDatabaseComputerName( Values[ K_SERVER_NAME ] );
                    ServerName.Text := GetDatabaseFileName( Values[ K_SERVER_NAME ] );
                    UserName.Text := Values[ K_USER_NAME ];
               end;
          end;
     end;
end;

procedure TAliasCrear.AliasNameChange(Sender: TObject);
begin
     SetValues;
end;

function TAliasCrear.GetFullDatabaseName: String;
begin
     Result := ZetaAliasManager.BuildFullDatabaseName( ServerName.Text, ComputerName.Text );
end;

procedure TAliasCrear.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
var
   sError: String;
   oCursor: TCursor;
begin
     if StrLleno( ComputerName.Text ) or ZetaDialogo.ZConfirm('� Atenci�n !', 'No Se Indic� El Nombre De La Computadora' + CR_LF + '� Desea Usar Interbase Con Una Licencia Local ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourGlass;
          try
             with FAliasManager do
             begin
                  if CreateAlias( Alias, GetFullDatabaseName, UserName.Text, sError ) then
                  begin
                       lOk := True;
                       ZetaODBCTools.SetIntersolvDataSource( Alias, Alias, GetFullDatabaseName, sError )
                  end
                  else
                  begin
                       ZetaDialogo.zError( Caption, sError, 0 );
                       lOk := False;
                  end;
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
          if lOk then
             Close;
     end
     else
         lOk := False;
end;

procedure TAliasCrear.WizardAlCancelar(Sender: TObject; var lOk: Boolean);
begin
     lOk := True;
     Close;
end;

end.
