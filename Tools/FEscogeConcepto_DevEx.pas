unit FEscogeConcepto_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, Grids, DBGrids, StdCtrls, Buttons, ExtCtrls,
     ZBaseEscogeGrid_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ZetaCXGrid, cxButtons;

type
  TEscogeConcepto_DevEx = class(TBaseEscogeGrid_DevEx)
    co_numero: TcxGridDBColumn;
    co_descrip: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure SetFiltro( const lEnabled: Boolean );
  protected
    { Protected declarations }
    function GetFormula: String; override;
    procedure Connect; override;
  public
    { Public declarations }
  end;

function PickConcepto: String;

var
  EscogeConcepto_DevEx: TEscogeConcepto_DevEx;

implementation

uses DCatalogos,
     ZetaCommonClasses;

{$R *.DFM}

function PickConcepto: String;
begin
     if ( EscogeConcepto_DevEx = nil ) then
        EscogeConcepto_DevEx:= TEscogeConcepto_DevEx.Create( Application.MainForm );
     with EscogeConcepto_DevEx do
     begin
          ShowModal;
          if ( ModalResult = mrOk ) then
             Result := Formula;
          SetFiltro( False );
     end;
end;

{ ******* TEscogeConcepto ******* }

procedure TEscogeConcepto_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H66214_Escoge_concepto;
     {
     SqlNumber:= Q_ESCOGE_CONCEPTO;
     }
end;

function TEscogeConcepto_DevEx.GetFormula: String;
begin
     with dmCatalogos.cdsConceptosLookUp do
     begin
          if not IsEmpty then
             Result:= 'C( ' + FieldByName( 'CO_NUMERO' ).AsString + ' )'
          else
              Result:= '';
     end;
     {
     if DatasetLleno( FQuery ) then
        Result:= 'C( ' + FQuery.FieldByName( 'CO_NUMERO' ).AsString + ' )';
     }
end;

procedure TEscogeConcepto_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          SetFiltro( True );
          DataSource.DataSet := cdsConceptosLookUp;
     end;
end;

procedure TEscogeConcepto_DevEx.SetFiltro( const lEnabled: Boolean );
begin
     with dmCatalogos.cdsConceptosLookUp do
     begin
          if lEnabled then
          begin
               Filter := 'CO_TIPO > 0';
               Filtered := True;
          end
          else
          begin
              Filtered := False;
              Filter := '';
          end;
     end;
end;

end.
