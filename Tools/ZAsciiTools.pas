unit ZAsciiTools;

interface

uses Classes, SysUtils, db, dbClient;

   procedure FiltraASCII( cdsDataSet: TClientDataSet; const lFiltrar: Boolean );
   procedure FillcdsASCII( cdsASCII: TClientDataSet; const sArchivo: String);
   procedure ClearErroresASCII( oDataSet: TDataSet );
   procedure GrabaBitacora( oLista: TStrings; const sArchivo: String );

implementation

uses ZetaAsciiFile;

procedure FiltraASCII( cdsDataSet: TClientDataSet; const lFiltrar: Boolean );
begin
     with cdsDataSet do
     begin
          if lFiltrar then
          begin
               Filter := 'NUM_ERROR > 0';
               Filtered := True;
          end
          else
          begin
               Filtered := False;
               Filter := '';
          end;
     end;
end;

procedure FillcdsASCII( cdsASCII: TClientDataSet; const sArchivo: String );
var
   FAsciiServer: TAsciiServer;
begin
     with cdsASCII do
     begin
          if Active then
             Close;
          CreateDataSet;
          Open;
     end;
     FAsciiServer := TAsciiServer.Create;
     with FAsciiServer do
     begin
          try
             if Open( sArchivo ) then
             begin
                  repeat
                        cdsASCII.AppendRecord( [ Data ] );
                  until not Read;
                  Close;
                  //Borrar;
             end;
          finally
                 Free;
          end;
     end;
end;

procedure ClearErroresASCII( oDataSet: TDataSet );
begin
     with oDataSet do
     begin
          First;
          while not Eof do
          begin
               if ( FieldByName( 'NUM_ERROR' ).AsInteger > 0 ) then
                  Delete
               else
                   Next;
          end;
          First;
     end;
end;

procedure GrabaBitacora( oLista: TStrings; const sArchivo: String );
var
   i : Integer;
   FAsciiLog: TAsciiLog;
begin
     if ( oLista.Count > 0 ) then
     begin
          FAsciiLog := TAsciiLog.Create;
          try
             with FAsciiLog do
             begin
                  try
                     Init( sArchivo );
                  except
                        on Error: Exception do
                           DataBaseError( 'Error al Abrir Archivo:' + sArchivo );
                  end;
                  for i := 0 to oLista.Count - 1 do
                      WriteTexto( oLista.Strings[i] );
                  Close;
             end;
          finally
             FreeAndNil( FAsciiLog );
          end;
     end;
end;

end.
 