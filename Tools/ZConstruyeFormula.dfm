�
 TCONSTRUYEFORMULA 0_  TPF0TConstruyeFormulaConstruyeFormulaLefteTop� ActiveControlMemoBorderStylebsDialogCaptionConstructor de F�rmulasClientHeight� ClientWidthColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
KeyPreview	OldCreateOrder	PositionpoScreenCenter	OnKeyDownFormKeyDownOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left Top� WidthHeight'AlignalBottomTabOrder TImageOkImgLeft� Top
WidthHeightPicture.Data
N  TBitmapB  BMB      v   (               �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwwwp   wwwwwwwwp   wwwwt�wwp   wwwwDOwwp   wwwtDOwwp   p  DOD�wp   p��D�tOwp   p��O�wOwp   p����wt�p   p����wwOp   p����wwt�   p��w�www@   p��  wwwp   p��wwwp   p�� wwwwp   p  wwwwp   wwwwwwwwp   Stretch	Visible  TImageErrorImgLeft� TopWidthHeightPicture.Data
  TBitmap�   BM�       v   (               �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwwwwwwwwwwwww     www����www����ww����wq����ww����wwqQx���wwu����wu�  wqW��www�� wwww   wwwwwwwwwwwwwwwwwwStretch	Visible  TBitBtnOKLeftcTopWidthKHeightCaption&OKTabOrder OnClickOKClickKindbkOK  TBitBtnCancelarLeft�TopWidthKHeightCaption	&CancelarTabOrderOnClickCancelarClickKindbkCancel  TBitBtnTestBtnLeftJTopWidthWHeightCaptionProbarTabOrderOnClickTestBtnClick
Glyph.Data
F  B  BMB      v   (               �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwwwp   wwwwwwwwp   wwwwt�wwp   wwwwDOwwp   wwwtDOwwp   p  DOD�wp   p��D�tOwp   p��O�wOwp   p����wt�p   p����wwOp   p����wwt�   p��w�www@   p��  wwwp   p��wwwp   p�� wwwwp   p  wwwwp   wwwwwwwwp   Margin   TMemoMemoLeft Top Width�Height� AlignalClientFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 	MaxLength� 
ParentFont
ScrollBars
ssVerticalTabOrder OnChange
MemoChange  	TGroupBox	GroupBox1Left�Top WidthHeight� AlignalRightCaption	 Agregar TabOrder TBitBtnCampoBtnLeftTopWidthnHeightCaptionCa&mpoTabOrder OnClickCampoBtnClick
Glyph.Data
F  B  BMB      v   (               �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwwwp   wwwwwwwwp   wwwwwwwwp   w  wwwwwp   w�wDDDDp   w�wwwwwp   w wwwwwp   w wwwwwp   wwwwwwwwp   wwwwwwwwp   w  wwwwwp   w�wDDDDp   w�wwwwwp   w wwwwwp   w wwwwwp   wwwwwwwwp   wwwwwwwwp   Margin  TBitBtn
FuncionBtnLeftTop5WidthnHeightCaption&Funci�nTabOrderOnClickFuncionBtnClick
Glyph.Data
j  f  BMf      v   (               �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� ����������  ����������  ����������  ����������  ����������  ����DH����  ����G�����  �����x����  ���������  ������x���  ���������  �������x��  �������G��  ���DDDDG��  ���DDDDH��  ����������  ����������  ����������  ����������  ����������  Margin  TBitBtnParamBtnLeftTopRWidthnHeightCaption&Par�metrosTabOrderOnClickParamBtnClick
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333333333333�3333333333333<3333337w�33333���33333w73333<�33337w�w�333�����333s7733<���33733w�w�3L������333773�����3w�3�w�w�<L������773�7s3����33w�7�ww33<L ��3337w37�333����3333w�3�3333<L��333377s33333��333333ws3333333333333333333Margin	NumGlyphs  TBitBtnConceptoBtnLeftTopnWidthnHeightCaption
C&onceptosTabOrderOnClickConceptoBtnClick
Glyph.Data
F  B  BMB      v   (               �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� ���������   ���������   �D�������   �D�    �   �D�������   ���������   ���������   �D�������   �D�    �   �D�������   ���������   ���������   �D�������   �D�    �   �D�������   ���������   ���������   Margin  TBitBtnAcumuladoBtnLeftTop� WidthnHeightCaption&AcumuladosTabOrderOnClickAcumuladoBtnClick
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333333333333333333?�����33     33wwwww330330337�37333 333333w�333330333337�3s3333 3333333w�33333303333337�333333 3333333w33333303333337s333333 3333333w33?3330333337s33333 333333w333330330337���33     33wwwwws33333333333333333Margin	NumGlyphs  TBitBtn	GlobalBtnLeftTop� WidthnHeightCaption	&GlobalesTabOrderOnClickGlobalBtnClick
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� UUUUUUUUUUU���UUUUTLLUUUUU�ww_�UUT�D�DUUUWu�Uw_UULC4DDEUUuWUU�T��4��3UW�W�Uw�T��3DD3UWUWw�Uw_���3���EU�w�UUL�33��D�Www��U�333����www��UL�333<LEWwwwU��<�3<��u�u_w�u\��333LUW��www��T�3333�UWWwwww�UU33�<3�UUwwUuwuUUS<���UUUWu��wUUUU\L�UUUUUWwwUUUMargin	NumGlyphs    