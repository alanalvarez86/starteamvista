inherited EscogeParametro_DevEx: TEscogeParametro_DevEx
  Caption = 'Escoge Par'#225'metro de N'#243'mina'
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel2: TPanel
    Visible = False
  end
  inherited Grid_DevEx: TZetaCXGrid
    inherited Grid_DevExDBTableView: TcxGridDBTableView
      object np_nombre: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'np_nombre'
        MinWidth = 70
      end
      object np_descrip: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'np_descrip'
        MinWidth = 400
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 344
  end
end
