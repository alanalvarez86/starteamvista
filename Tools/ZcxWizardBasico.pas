unit ZcxWizardBasico;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxGraphics, cxLookAndFeelPainters, cxLookAndFeels,
  dxCustomWizardControl, dxWizardControl, dxWizardControlForm,ZetaCxWizard,
  dxSkinsCore, cxContainer, cxEdit, StdCtrls,cxButtons, dxGDIPlusClasses,
  cxImage, cxLabel, cxGroupBox ,ZetaDBTextBox,ZetaDialogo, TressMorado2013,
  dxSkinsDefaultPainters,cxMemo,cxCheckBox;

type

  TCXWizardBasico = class(TdxWizardControlForm)
    WizardControl: TdxWizardControl;
    Parametros: TdxWizardControlPage;
    Ejecucion: TdxWizardControlPage;
    Wizard: TZetaCXWizard;
    GrupoParametros: TcxGroupBox;
    cxGroupBox1: TcxGroupBox;
    Advertencia: TcxLabel;
    cxImage1: TcxImage;
    procedure WizardControlButtonClick(Sender: TObject;
      AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure WizardControlPageChanged(Sender: TObject);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
     FSigControl:Boolean;
     function GetAnterior:TdxWizardControlBackButton;
     function GetSiguiente:TdxWizardControlNextButton;
     function GetSalir:TdxWizardControlCancelButton;
     function GetEjecutar:TdxWizardControlFinishButton;
  public
    { Public declarations }
   Property Anterior: TdxWizardControlBackButton read GetAnterior;
   Property Siguiente: TdxWizardControlNextButton read GetSiguiente;
   Property Salir: TdxWizardControlCancelButton read GetSalir;
   Property Ejecutar: TdxWizardControlFinishButton read GetEjecutar;
  protected
   procedure KeyPress( var Key: Char ); override; {TWinControl} 
  end;
  TcxWizardBasicoClass = class of TcxWizardBasico;

var
  CXWizardBasico: TCXWizardBasico;
  function ShowWizard( WizardClass: TcxWizardBasicoClass ): Boolean;

implementation

uses ZetaSmartLists_DevEx,ZetaSmartLists;

{$R *.dfm}

function ShowWizard( WizardClass: TcxWizardBasicoClass ): Boolean;
begin
     with WizardClass.Create( Application ) as TcxWizardBasico do
     begin
          try
             ShowModal;
             Result := Wizard.Ejecutado;
          finally
                 Free;
          end;
     end;
end;



{ TCXWizardBasico }

procedure TCXWizardBasico.WizardControlButtonClick(Sender: TObject;
  AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
begin
     inherited;
     {***NOTA IMPORTANTE (by am):Si el valor de AHandled es FALSE se pasara automaticamente al la siguiente pagina cuando Akind sea igual a wcbkNext y vcbkBack
     Esa es su funcionalidad por default. Cuando nosotros queremos controlar el movimiento y pasar a una pagina en especifica ya sea hacia adelante o
     hacia atras el valor de Handled debe ser TRUE, de esta forma no ejecutara la accion por defecto del boton en los tipos mencionados anteriormente.***}
     AHandled := FSigControl;
     if ( not FSigControl )then
     begin
          if AKind = wcbkFinish  then
          begin
               WizardControl.Buttons.Finish.Enabled := false;
               Wizard.Ejecutar;
          end;

          if AKind = wcbkNext  then
          begin
               //DevEx(by am): Salto especidal indica que quieres ir a una pagina especifica del wizard
               if Wizard.SaltoEspecial then
                   AHandled := Wizard.SaltarAdelanteEspecial( wcbkNext )
               else
                   AHandled := Wizard.SaltarAdelante(WizardControl.ActivePageIndex + 1 );
          end;

          if AKind = wcbkBack then
          begin
               //DevEx(by am): Salto especidal indica que quieres ir a una pagina especifica del wizard
               if Wizard.SaltoEspecial then
                  AHandled:= Wizard.SaltarAtrasEspecial(wcbkBack)
               else
                   Wizard.Anterior;
          end;

          if AKind = wcbkCancel  then
          begin
               Wizard.Cancelar;
               if Wizard.Estado = wzCanceled then
                  Self.Close;
          end;
     end;
     FSigControl:= False;
end;

procedure TCXWizardBasico.FormShow(Sender: TObject);
begin
     with Wizard do
     begin
          Reset;
          FSigControl := False;
          Primero;
     end;

end;

{ TODO: Remover?}
procedure TCXWizardBasico.KeyPress( var Key: Char );
begin

     case Key of
          Chr( VK_RETURN ):
          begin
               if ( not ( ActiveControl is TButton ) ) and
                  ( not ( ActiveControl is TcxButton ) ) and
                  ( not ( ActiveControl is TZetaSmartListBox ) ) and
                  ( not ( ActiveControl is TZetaSmartListBox_DevEx ) ) and
                  ( not ( ActiveControl is TcxMemo ) ) and
                  ( not ( ActiveControl is TCustomMemo ) ) then
               begin
                    Key := #0;
                    Perform( WM_NEXTDLGCTL, ( Hi( GetKeyState( VK_SHIFT ) ) and 1 ), 0 );
                    FSigControl := False;
               end;
          end;
          Chr( VK_ESCAPE ):
          begin
               Key := #0;
               Close;
          end;
          else
              inherited KeyPress( Key );
     end;
end;


function TCXWizardBasico.GetAnterior: TdxWizardControlBackButton;
begin
     Result := WizardControl.Buttons.Back;
end;

function TCXWizardBasico.GetEjecutar: TdxWizardControlFinishButton;
begin
     Result := WizardControl.Buttons.Finish ;
end;

function TCXWizardBasico.GetSalir: TdxWizardControlCancelButton;
begin
     Result := WizardControl.Buttons.Cancel ;
end;

function TCXWizardBasico.GetSiguiente: TdxWizardControlNextButton;
begin
     Result := WizardControl.Buttons.Next;
end;

procedure TCXWizardBasico.WizardControlPageChanged(Sender: TObject);
begin
      if Assigned( Wizard.AfterMove ) then
             Wizard.AfterMove( Self );
end;

procedure TCXWizardBasico.WizardAlCancelar(Sender: TObject;
  var lOk: Boolean);
begin
     lOk := ZetaDialogo.ZConfirm(Self.Caption,'� Est� seguro de cancelar y salir del proceso ?',0,mbOk );
     //PENDIENTE: AGREGAR QUE AL DAR EN NO, el lOK reciba un FALSE.
end;

procedure TCXWizardBasico.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
          if Key = VK_RETURN then
     begin
          Key := Word(#0);
          if ( ( ActiveControl is TcxCheckBox  ) or
               ( ActiveControl is TcxLabel  )or
               ( ActiveControl is TCheckBox  ) ) then
               begin
                    Perform( WM_NEXTDLGCTL, ( Hi( GetKeyState( VK_SHIFT ) ) and 1 ), 0 );
                    FSigControl:= True;
               end;
     end;
end;

end.
