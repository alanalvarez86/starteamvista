unit ZetaRegistryServerEditor;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, FileCtrl,
     DB, DBTables,
     ZetaRegistryServer,
     ZBaseDlgModal;

type
  TServerRegistryEditor = class(TZetaDlgModal)
    UserName: TEdit;
    UserNameLBL: TLabel;
    PasswordLBL: TLabel;
    Password: TEdit;
    PasswordCheck: TEdit;
    PasswordCheckLBL: TLabel;
    DatabaseLBL: TLabel;
    Database: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure PasswordChange(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
  private
    { Private declarations }
    FRegistry: TZetaRegistryServer;
  protected
    { Protected declarations }
    property Registry: TZetaRegistryServer read FRegistry;
    procedure LoadControls; virtual;
    procedure SetControls; dynamic;
    procedure UnloadControls; virtual;
  public
    { Public declarations }
    function IsValid: Boolean;
    function CanWrite: Boolean;
  end;

var
  ServerRegistryEditor: TServerRegistryEditor;

function EspecificaComparte: Boolean;
function InitComparte: Boolean;

implementation

uses ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaWinAPITools;

{$ifndef TRESSCFG}
{$R *.DFM}
{$endif}

const
     K_TOKEN = ':\';

function GetDatabaseServer( const sValue: String ): String;
var
   iPos: Integer;
begin
     iPos := Pos( K_TOKEN, sValue );
     if ( iPos > 3 ) then
        Result := Copy( sValue, 1, ( iPos - 3 ) )
     else
         Result := ZetaWinAPITools.GetComputerName;
end;

function GetDatabaseName( const sValue: String ): String;
var
   iPos: Integer;
begin
     iPos := Pos( K_TOKEN, sValue );
     if ( iPos > 1 ) then
        Result := Copy( sValue, ( iPos - 1 ), ( Length( sValue ) - ( iPos - 2 ) ) )
     else
         Result := sValue;
end;

function BuildFullDatabaseName( const sDatabase, sComputerName: String ): String;
begin
     Result := sDatabase;
     if ( Pos( sComputerName, Result ) = 0 ) then
        Result := Format( '%s:%s', [ sComputerName, Result ] );
end;

function EspecificaComparte: Boolean;
begin
     with TServerRegistryEditor.Create( Application ) do
     begin
          try
             if CanWrite then
             begin
                  ShowModal;
                  Result := ( ModalResult = mrOK );
             end
             else
             begin
                  Result := FALSE;
                  ZetaDialogo.ZError( 'Acceso al Sistema', 'No Tiene Configurado Acceso' + CR_LF + 'Requiere Derechos de Escritura en Registry' + CR_LF + 'Para Realizar Esta Configuración', 0 );
             end;
          finally
                 Free;
          end;
     end;
end;

function InitComparte: Boolean;
begin
     with TServerRegistryEditor.Create( Application ) do
     begin
          try
             if IsValid then
                Result := True
             else if CanWrite then
             begin
                  ShowModal;
                  Result := ModalResult = mrOK;
             end
             else
             begin
                  Result := FALSE;
                  ZetaDialogo.ZError( 'Acceso al Sistema', 'No Tiene Configurado Acceso' + CR_LF + 'Requiere Derechos de Escritura en Registry' + CR_LF + 'Para Realizar Esta Configuración', 0 );
             end;
          finally
                 Free;
          end;
     end;
end;

{ ************* TServerRegistryEditor ******** }

procedure TServerRegistryEditor.FormCreate(Sender: TObject);
begin
     inherited;
     FRegistry := TZetaRegistryServer.Create;
     HelpContext := H80081_Especificar_base_de_datos;
end;

procedure TServerRegistryEditor.FormShow(Sender: TObject);
begin
     inherited;
     LoadControls;
     SetControls;
end;

procedure TServerRegistryEditor.FormDestroy(Sender: TObject);
begin
     FRegistry.Free;
     inherited;
end;

function TServerRegistryEditor.IsValid: Boolean;
begin
     Result := FRegistry.IsValid;
end;

function TServerRegistryEditor.CanWrite: Boolean;
begin
     Result := FRegistry.CanWrite;
end;

procedure TServerRegistryEditor.LoadControls;
begin
     with Registry do
     begin
          Self.Database.Text := Database;
          Self.UserName.Text := UserName;
          Self.Password.Text := Password;
          Self.PasswordCheck.Text := Password;
     end;
end;

procedure TServerRegistryEditor.SetControls;
begin
end;

procedure TServerRegistryEditor.UnloadControls;
begin
     with Registry do
     begin
          Database := Self.Database.Text;
          UserName := Self.UserName.Text;
          Password := Self.Password.Text;
     end;
end;

procedure TServerRegistryEditor.PasswordChange(Sender: TObject);
begin
     inherited;
     PasswordCheck.Text := '';
end;

procedure TServerRegistryEditor.OKClick(Sender: TObject);
begin
     inherited;
     if ( Password.Text = PasswordCheck.Text ) then
     begin
          UnloadControls;
          ModalResult := mrOk;
     end
     else
     begin
          ZetaDialogo.zError( Caption, 'Clave de Acceso No Fué Capturada Correctamente', 0 );
          ActiveControl := Password;
     end;
end;

procedure TServerRegistryEditor.CancelarClick(Sender: TObject);
begin
     inherited;
     Close;
end;

end.
