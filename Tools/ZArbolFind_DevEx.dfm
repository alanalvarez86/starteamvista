object BuscaArbol_DevEx: TBuscaArbol_DevEx
  Left = 315
  Top = 262
  Hint = 'Buscar Siguiente'
  ActiveControl = Nodo
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'B'#250'squeda en '#193'rbol'
  ClientHeight = 70
  ClientWidth = 335
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object NodoLBL: TLabel
    Left = 34
    Top = 14
    Width = 77
    Height = 13
    Alignment = taRightJustify
    Caption = 'Forma a Buscar:'
    FocusControl = Nodo
  end
  object Nodo: TEdit
    Left = 118
    Top = 10
    Width = 171
    Height = 21
    TabOrder = 0
  end
  object OK_DevEx: TcxButton
    Left = 118
    Top = 38
    Width = 81
    Height = 26
    Caption = '&Siguiente'
    Default = True
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnClick = OK_DevExClick
    OptionsImage.Glyph.Data = {
      36090000424D3609000000000000360000002800000018000000180000000100
      200000000000000900000000000000000000000000000000000050D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8
      A1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF8CE2B8FFE9F9
      F1FF55D396FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8A1FFF4FCF8FFFFFF
      FFFFADEACCFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF53D395FFD6F4E6FFFFFFFFFFFFFF
      FFFFFCFEFDFF71DAA7FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FFA5E8C8FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFD9F5E7FF53D395FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF79DDACFFFCFEFDFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFF9AE5C1FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF5BD59AFFE9F9F1FFFFFFFFFFFFFFFFFFC3EFDAFFBDEE
      D6FFFFFFFFFFFFFFFFFFF4FCF8FF63D79FFF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FFC3EFDAFFFFFFFFFFF7FDFAFF8CE2B8FF50D293FF55D3
      96FFE1F7ECFFFFFFFFFFFFFFFFFFC8F1DDFF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF8AE1B7FFE6F9F0FF6BD9A4FF50D293FF50D293FF50D2
      93FF60D69DFFE4F8EEFFFFFFFFFFFFFFFFFF8FE2BAFF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF53D395FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF66D8A1FFEFFBF5FFFFFFFFFFF7FDFAFF69D8A2FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF6BD9A4FFEFFBF5FFFFFFFFFFDEF7EBFF53D395FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF79DDACFFF7FDFAFFFFFFFFFFB8EDD3FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF71DAA7FFEFFBF5FFFFFFFFFFA0E6
      C4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8A1FFE1F7ECFFFFFF
      FFFF8FE2BAFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF58D498FFBDEE
      D6FFFCFEFDFF8AE1B7FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF81DFB1FF8FE2BAFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
      93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
    OptionsImage.Margin = 1
  end
  object Salir_DevEx: TcxButton
    Left = 208
    Top = 38
    Width = 81
    Height = 26
    Hint = 'Salir'
    Cancel = True
    Caption = '   &Salir'
    ModalResult = 2
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    OptionsImage.Glyph.Data = {
      36090000424D3609000000000000360000002800000018000000180000000100
      20000000000000090000000000000000000000000000000000004858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF5362CFFF6D7AD6FF8D97DFFFACB3E8FFC8CD
      F0FFE8EAF9FFBAC0ECFF707DD7FF7682D9FF6572D4FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFF848FDDFF8D97DFFFACB3E8FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFF969FE2FF6572D4FF969FE2FF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
      FEFFD4D8F3FFBAC0ECFF4858CCFF4858CCFF969FE2FFF9FAFDFFFFFFFFFFFFFF
      FFFFA4ACE6FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFE5E7F8FFBAC0ECFF4858CCFF4858CCFF707DD7FFD1D5F2FFE5E7F8FFBAC0
      ECFF818CDCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF6A77D6FF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFFACB3E8FFBAC0ECFFB1B8E9FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF5665D0FF7682D9FF969FE2FFB1B8
      E9FFD1D5F2FFACB3E8FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
    OptionsImage.Margin = 1
  end
end
