unit ZBaseGridLectura_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBDataDefinitions,
  cxGridDBTableView, cxGrid, ZetaCXGrid, StdCtrls,   Menus, ActnList, ImgList, TressMorado2013,
 {$ifdef TRESS_DELPHIXE5_UP} Actions,{$endif}
  ZetaClientDataSet;

type
  TBaseGridLectura_DevEx = class(TBaseConsulta)
    ZetaDBGridDBTableView: TcxGridDBTableView;
    ZetaDBGridLevel: TcxGridLevel;
    ZetaDBGrid: TZetaCXGrid;
    cxImage16: TcxImageList;
    ActionList: TActionList;
    _A_Imprimir: TAction;
    _E_Agregar: TAction;
    _E_Borrar: TAction;
    _E_Modificar: TAction;
    PopupMenu1: TPopupMenu;
    mImprimir: TMenuItem;
    mAgregar: TMenuItem;
    mBorrar: TMenuItem;
    mModificar: TMenuItem;
    procedure ZetaDBGridDBTableViewDblClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewDataControllerFilterGetValueList(Sender: TcxFilterCriteria; AItemIndex: Integer; AValueList: TcxDataFilterValueList);
    procedure FormShow(Sender: TObject);
    procedure DataSourceUpdateData(Sender: TObject);
    procedure _A_ImprimirExecute(Sender: TObject);
    procedure _E_AgregarExecute(Sender: TObject);
    procedure _E_BorrarExecute(Sender: TObject);
    procedure _E_ModificarExecute(Sender: TObject);
    procedure ZetaDBGridDBTableViewKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure ZetaDBGridDBTableViewColumnHeaderClick(Sender: TcxGridTableView; AColumn: TcxGridColumn);
    procedure ZetaDBGridDBTableViewDataControllerSortingChanged(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ZetaDBGridDBTableViewDataControllerSummaryAfterSummary(ASender: TcxDataSummary);
  private
    { private declarations }
  protected
    { Protected declarations }
    AColumn: TcxGridDBColumn;
    procedure ReconnectGrid; override;
    procedure RefreshGrid; override;
    procedure DisconnectGrid; override;
    procedure ConnectGrid; override;
    //DevEx (by ame): Proceso Agregado para asignar el MinWidth Ideal
    procedure ApplyMinWidth; Dynamic;
    //Procedure CreaColumaSumatoria(Columna:String;footer : tcxSummaryKind ); overload;
    Procedure CreaColumaSumatoria(Columna:TcxGridDBColumn; TipoFormato: Integer ; TextoSumatoria: String ; FooterKind : tcxSummaryKind ); overload;
    Procedure CreaColumaAgrupadaSumatoria(Columna:String; FooterKind : TcxSummaryKind );
  public
   { Public declarations }
  end;

var
  BaseGridLectura_DevEx: TBaseGridLectura_DevEx;

const
     {***Formastos para las sumatorias***}
     K_SIN_TIPO = 0;
     K_TIPO_MONEDA = 1;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZAccesosTress,
     ZGridModeTools,
     DCliente;

{$R *.dfm}

{ TBaseGridLectura_DevEx }

{***(@am):Por defecto las formas que hereden de esta base trabajaran en GridMode, si se desea trabajar sin gridmode, sobreescribir este eventos
       y cambiar la propiedad a False.***}

procedure TBaseGridLectura_DevEx.FormCreate(Sender: TObject);
begin
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= True;
     inherited;
end;

procedure TBaseGridLectura_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     //Activa el filtrado por DataSet
     if ZetaDBGridDBTableView.DataController.IsGridMode then
        ZetaDBGridDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
     //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
     //Esconde la caja de agrupamiento
     ZetaDBGridDBTableView.OptionsView.GroupByBox := False;
     //Para que nunca muestre el filterbox inferior
     ZetaDBGridDBTableView.FilterBox.Visible := fvNever;
     //Para que no aparezca el Custom Dialog
     ZetaDBGridDBTableView.FilterBox.CustomizeDialog := False;
     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TBaseGridLectura_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     ZGridModeTools.LimpiaCacheGrid( ZetaDBGridDBTableView );
     inherited;
end;

procedure TBaseGridLectura_DevEx.ReconnectGrid;
begin
     inherited;
     ZGridModeTools.LimpiaCacheGrid( ZetaDBGridDBTableView );
end;

procedure TBaseGridLectura_DevEx.RefreshGrid;
begin
     inherited;
     ZGridModeTools.LimpiaCacheGrid( ZetaDBGridDBTableView );
end;

procedure TBaseGridLectura_DevEx.ConnectGrid;
begin
     inherited;
     if ZetaDBGridDBTableView.DataController.IsGridMode then
        ZetaDBGridDBTableView.DataController.Filter.Active := TRUE;
end;

procedure TBaseGridLectura_DevEx.DisconnectGrid;
begin
     inherited;
     if ZetaDBGridDBTableView.DataController.IsGridMode then
        ZetaDBGridDBTableView.DataController.Filter.Active := FALSE;
end;

{***(am):Trabaja SIN GridMode***}
procedure TBaseGridLectura_DevEx.ZetaDBGridDBTableViewDataControllerFilterGetValueList(Sender: TcxFilterCriteria;
          AItemIndex: Integer; AValueList: TcxDataFilterValueList);
var
   oField: TField;
   sFieldName: String;
   cdsClone: TZetaClientDataSet;
   oListaLookup: TStringList;

   procedure StoreLookupValues;
   var
      ValorAnterior, ValorActual: String;
   begin
        sFieldName := oField.KeyFields;
        oListaLookup := TStringList.Create;
        cdsClone := TZetaClientDataSet.Create(self);
        try
           with cdsClone do
           begin
                CloneCursor( TZetaClientDataSet( ZetaDBGridDBTableView.DataController.DataSet ), TRUE, TRUE );
                IndexFieldNames := sFieldName;
                ValorAnterior := ZetaCommonClasses.VACIO;
                First;
                while ( not EOF ) do
                begin
                     ValorActual := FieldByName( sFieldName ).AsString;
                     if ( ValorActual <> ValorAnterior ) then
                     begin
                          oListaLookup.Add( Format( '%s=%s', [ TZetaLookupDataSet( oField.LookupDataSet ).GetDescripcion( ValorActual ), ValorActual ] ) );
                          ValorAnterior := ValorActual;
                     end;
                     Next;
                end;
                Close;
                oListaLookup.Sort;
           end;
        finally
               ZetaDBGridDBTableView.Columns[ AItemIndex ].Tag := Integer( oListaLookup );
               cdsClone.Free;
        end;
   end;

   procedure LlenaLookupValues;
   var
      i: Integer;
   begin
        if ( ZetaDBGridDBTableView.Columns[ AItemIndex ].Tag = 0 ) then
           StoreLookupValues;
        if ( ZetaDBGridDBTableView.Columns[ AItemIndex ].Tag > 0 ) then
        begin
             try
                oListaLookup := TStringList( ZetaDBGridDBTableView.Columns[ AItemIndex ].Tag );
                for i := 0 to oListaLookup.Count - 1 do
                begin
                     AValueList.Add( fviValue, oListaLookup.ValueFromIndex[i], oListaLookup.Names[i], TRUE );
                end;
             except
                   //Abort;   // excepci�n silenciosa - No continua con el llenado
             end;
        end;
   end;

begin
     inherited;
     ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if ZetaDBGridDBTableView.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( ZetaDBGridDBTableView, AItemIndex, AValueList );
end;

{***(am):Trabaja CON GridMode***}
procedure TBaseGridLectura_DevEx.ZetaDBGridDBTableViewColumnHeaderClick(Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
     if ZetaDBGridDBTableView.DataController.IsGridMode then
     begin
          inherited;
          self.AColumn := TcxGridDBColumn(AColumn);
     end;
end;

{***(am):Trabaja CON GridMode***}
procedure TBaseGridLectura_DevEx.ZetaDBGridDBTableViewDataControllerSortingChanged(Sender: TObject);
begin
     if ZetaDBGridDBTableView.DataController.IsGridMode then
     begin
          inherited;
          ZetaDBGrid.OrdenarPor( AColumn , TZetaClientDataset(DataSource.DataSet) );
     end;
end;

procedure TBaseGridLectura_DevEx.DataSourceUpdateData(Sender: TObject);
begin
     inherited;
     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TBaseGridLectura_DevEx.ZetaDBGridDBTableViewDblClick(Sender: TObject);
begin
     inherited;
     DoEdit;
end;

procedure TBaseGridLectura_DevEx._A_ImprimirExecute(Sender: TObject);
begin
     inherited;
     DoPrint;
end;

procedure TBaseGridLectura_DevEx._E_AgregarExecute(Sender: TObject);
begin
     inherited;
     DoInsert;
end;

procedure TBaseGridLectura_DevEx._E_BorrarExecute(Sender: TObject);
begin
     inherited;
     DoDelete;
end;

procedure TBaseGridLectura_DevEx._E_ModificarExecute(Sender: TObject);
begin
     inherited;
     DoEdit;
end;

//edit by mp: Metodo para Generar Columna Sumatoria parametro : el nombre de la columna
{
Procedure TbaseGridLectura_DevEx.CreaColumaSumatoria(Columna:String;footer : tcxSummaryKind );
begin
     if ZetaDbGridDBTableView.OptionsView.Footer = false then
        ZetaDbGridDBTableView.OptionsView.Footer := true;
     ZetaDbGridDBTableView.GetColumnByFieldName(Columna).Summary.FooterKind := footer;
end;
}

//DevEx (by am): Se sobrecarga el metodo para invocar este cuando se desee aplicar un formato a la columna
procedure TbaseGridLectura_DevEx.CreaColumaSumatoria(Columna:TcxGridDBColumn; TipoFormato: Integer ; TextoSumatoria: String ; FooterKind : tcxSummaryKind );
begin
     ZGridModeTools.CreaColumnaSumatoria( Columna, TipoFormato, TextoSumatoria, FooterKind );
end;

procedure TBaseGridLectura_DevEx.ZetaDBGridDBTableViewDataControllerSummaryAfterSummary(ASender: TcxDataSummary);
begin
     inherited;
     if ZetaDBGridDBTableView.DataController.IsGridMode and ZetaDbGridDBTableView.OptionsView.Footer then      // Hay columnas totalizadas
        ZGridModeTools.AsignaValorColumnaSummary( ASender );
end;

{***(am):Trabaja SIN GridMode***}
//DevEx (by am): Metodo creado para reflejar el sumatorias cuando este agrupada la columna.
Procedure TbaseGridLectura_DevEx.CreaColumaAgrupadaSumatoria(Columna:String; FooterKind : TcxSummaryKind );
begin
     ZetaDbGridDBTableView.GetColumnByFieldName(Columna).Summary.GroupKind := FooterKind;
end;

procedure TBaseGridLectura_DevEx.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =40;
begin
     with ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;
end;

procedure TBaseGridLectura_DevEx.ZetaDBGridDBTableViewKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     inherited;
     if Key = VK_RETURN then
     begin
          DoEdit;
     end;
end;

end.
