unit ZNavBarTools;

interface

uses ComCtrls, ZetaDespConsulta, {ZArbolTools,} dxNavBar, dxNavBarBase, dxNavBarCollns;

const K_ARBOL_NODO_RAIZ = -1;

type
TGrupoInfo = record
    Caption : String;
    IndexDerechos : Integer ;
  end;
  TDatosArbolUsuario = record
    IncluirSistema: Boolean;
    TieneArbol : Boolean;
    NombreNodoInicial: String;
    NodoDefault: Integer;
  end;

type
  TNavBarMgr = class(TObject)
  private
   FNavBar: TdxNavBar;
   FNumDefault : Integer;
   FUltimoGrupo : TdxNavBarGroup;  //Intersante
   FConfigura : Boolean;
   FDatosUsuario : TDatosArbolUsuario;
   function AgregaGrupo ( const sCaption: String; const iDerechos, iGroupIndex : Integer ; const iNodo: Integer ): TdxNavBarGroup;
  public
   property NavBar: TdxNavBar read FNavBar;
   property UltimoGrupo: TdxNavBarGroup read FUltimoGrupo; //Para saber el index del grupo
   property NumDefault : Integer read FNumDefault write FNumDefault;
   property Configuracion : Boolean read FConfigura write FConfigura;
   property  DatosUsuario : TDatosArbolUsuario read FDatosUsuario write FDatosUsuario;
   constructor Create( dxNavBar: TdxNavBar );
   function NodoNivel( const oGrupo: TGrupoInfo; iGroupIndex : Integer ; const iNodo: Integer  ): Boolean;
  end;

  function BuscaTextoGrupo( oNavBar: TdxNavBar; ANavBarGroup: TdxNavBarGroup; sTexto : String ): Boolean;
  procedure BuscaNodoReset;
  procedure NotificacionBusqueda ( lEncontro: Boolean; sCaption, sTexto : String);

var FUltimoTextoGrupo : String;
    FNodosTextoGrupo, FUltimoGrupo  : Integer;


implementation

uses Forms, ZAccesosMgr, SysUtils, ZetaDialogo, ZetaCommonClasses, ZetaCommonTools;

const
 K_FULL_ARBOL_TRESS = 9999;
{************ TNavBarMgr ***************}
//DevEx (by am): Unidad creada para realizar las funciones que hace ZArbolTools pero para la NavBar
constructor TNavBarMgr.Create( dxNavBar: TdxNavBar );
begin
     FNavBar:= dxNavBar;
     FUltimoGrupo   := NIL;
     FNumDefault   := 0;
     FConfigura := FALSE;
end;

function TNavBarMgr.AgregaGrupo( const sCaption: String; const iDerechos, iGroupIndex : Integer ;const iNodo: Integer  ): TdxNavBarGroup;
var
oNewGroup: TdxNavBarGroup;
begin
     //Agregar iNodo otravez
     if FConfigura  or ( iNodo = K_ARBOL_NODO_RAIZ ) or ZAccesosMgr.CheckDerecho( iDerechos, K_DERECHO_CONSULTA ) or (iDerechos =  K_FULL_ARBOL_TRESS) then//edit by MP: el iDerechos = 99999 es un derecho adicional creado para el Arbol clasico
     begin
          oNewGroup := FNavBar.Groups.Add;
          Result := oNewGroup;
          if (Result <> Nil) then
          begin
               Result.Caption := sCaption;
               Result.UseControl := True;
               Result.ShowControl := True;
               Result.Control.UseStyle := True;
               Result.Index := iGroupIndex;
               FUltimoGrupo := Result;
          end;
     end
     else
         Result := NIL;
         FUltimoGrupo := Result;
     begin
     end;
end;

function TNavBarMgr.NodoNivel( const oGrupo: TGrupoInfo; iGroupIndex : Integer ; const iNodo: Integer  ): Boolean;
var
   oNewGroup: TdxNavBarGroup;
begin
     with oGrupo do
     begin
          oNewGroup := AgregaGrupo( Caption,IndexDerechos,iGroupIndex, iNodo);
          Result := Assigned(oNewGroup);
     end;

end;

function BuscaTextoGrupo( oNavBar: TdxNavBar; ANavBarGroup: TdxNavBarGroup; sTexto : String ): Boolean;
begin
     Result := False;
     sTexto := UpperCase( ZetaCommonTools.QuitaAcentos( sTexto ));
     if ANavBarGroup <> nil then
     begin
          if ( Pos( sTexto, UpperCase( QuitaAcentos( ANavBarGroup.Caption ))) > 0 ) then
          begin
                Result := TRUE;
                //Como sabemos que encontro el nodo, ponemos activo el grupo en el que se encuentra
                oNavBar.ActiveGroup := ANavBarGroup;
          end;
     end;
end;
procedure BuscaNodoReset;
begin
  FUltimoTextoGrupo := '';
  FUltimoGrupo      := 0 ;
end;
procedure NotificacionBusqueda ( lEncontro: Boolean; sCaption, sTexto : String);
var
   sDescrip : String;
begin
     sTexto := UpperCase( ZetaCommonTools.QuitaAcentos( sTexto ));
     if not lEncontro then
     begin
          if ( FNodosTextoGrupo = 0 ) then
             sDescrip := 'ninguna'
          else
              sDescrip := 'otra';
          ZInformation( sCaption, Format( 'No hay %s Forma que contenga la palabra ' + CR_LF + '< %s >', [ sDescrip, sTexto ] ) , 0 );
     end;
end;
end.
