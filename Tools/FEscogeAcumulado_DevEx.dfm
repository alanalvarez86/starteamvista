inherited EscogeAcumulado_DevEx: TEscogeAcumulado_DevEx
  Caption = 'Escoge Acumulado'
  ClientWidth = 405
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Top = 227
    Width = 405
    Height = 33
    inherited OK_DevEx: TcxButton
      Left = 244
      Top = 4
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 327
      Top = 4
    end
  end
  inherited Panel2: TPanel
    Width = 405
    Height = 48
    object TipoAcumula: TRadioGroup
      Left = 6
      Top = 3
      Width = 394
      Height = 40
      Caption = ' Tipo Acumulado '
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        '&Mensual'
        '&Bimestral'
        '&Anual')
      TabOrder = 0
    end
  end
  inherited Grid_DevEx: TZetaCXGrid
    Top = 48
    Width = 405
    Height = 179
    inherited Grid_DevExDBTableView: TcxGridDBTableView
      object co_numero: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'co_numero'
        MinWidth = 70
      end
      object co_descrip: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'co_descrip'
        MinWidth = 300
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 40
    Top = 144
  end
end
