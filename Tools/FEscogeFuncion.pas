unit FEscogeFuncion;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEscogeGrid, Db, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons,
  ExtCtrls, ZetaClientDataSet;

type
  TEscogeFuncion = class(TBaseEscogeGrid)
    Label1: TLabel;
    BuscarEdit: TEdit;
    Buscar: TBitBtn;
    btnAyuda: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BuscarClick(Sender: TObject);
    procedure btnAyudaClick(Sender: TObject);
  private
    FFiltro : string;
    procedure QuitaFiltro;
    procedure FiltraFunciones( Palabra : String );
  protected
    function GetFormula: String ; override;
    procedure Connect; override;
    function cdsDiccion : TZetaClientDataSet;
  public
  end;

  function PickFuncion: String;

var
  EscogeFuncion: TEscogeFuncion;

implementation

uses dDiccionario, ZetaCommonClasses, ZetaCommonTools;

{$R *.DFM}

{DevEX: Antes invocaba el chm de CajaAhorro en vez de la descripcion de la funcion como en Tress.
Se corrigio este error como parte del proyecto de la Nueva Interfaz }
procedure LlamaAyuda( sTexto : String );
const
     K_HELP_TRESS = 'Tress.chm';
var
   sHelpFile: String;
begin
     with Application do
     begin
          sHelpFile := HelpFile;
          try
             HelpFile := K_HELP_TRESS;
             HelpCommand( HELP_PARTIALKEY, Longint( sTexto ));
          finally
             HelpFile := sHelpFile;
          end;
     end;
     // Application.HelpCommand( HELP_PARTIALKEY, Longint( sTexto ));  //OLD
end;

function PickFuncion: String;
begin
     if ( EscogeFuncion = nil ) then EscogeFuncion:= TEscogeFuncion.Create( Application.MainForm );
     with EscogeFuncion do
     begin
          ShowModal;
          if ( ModalResult = mrOk ) then
             Result:= Formula;
          BuscarEdit.Text:= VACIO;
          QuitaFiltro;
     end;
end;


procedure TEscogeFuncion.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H66212_Escoge_funcion;
end;

procedure TEscogeFuncion.FormShow(Sender: TObject);
begin
     inherited;
     Formula:= '';
     btnAyuda.Enabled := not cdsDiccion.Eof;
     ActiveControl:= BuscarEdit;
end;

function TEscogeFuncion.cdsDiccion : TZetaClientDataSet;
begin
     Result := dmDiccionario.cdsFunciones;
end;

function TEscogeFuncion.GetFormula: String;
begin
     with cdsDiccion do
     begin
          if not IsEmpty then
             Result:= FieldByName( 'DI_NOMBRE' ).AsString
          else
             Result:= VACIO;
     end;
end;

procedure TEscogeFuncion.Connect;
begin
     dmDiccionario.GetListaFunciones;
     DataSource.DataSet:= cdsDiccion;
     {$ifdef DOS_CAPAS}
     cdsDiccion.Filtered := True;
     FFiltro := cdsDiccion.Filter;
     {$else}
     {$endif}
end;

procedure TEscogeFuncion.QuitaFiltro;
begin
     with cdsDiccion do
     begin
          if Filtered then
          begin
              Filter := FFiltro;
          end;
          //Filter := VACIO;
          Filtered := FALSE;
     end;
end;

procedure TEscogeFuncion.BuscarClick(Sender: TObject);
begin
     FiltraFunciones( BuscarEdit.Text );
     BuscarEdit.SelectAll;
end;

procedure TEscogeFuncion.FiltraFunciones( Palabra : String );

  function Limita( Ancho : Integer ) : String;
  begin
       Result := Trim( Palabra );
       if ( Length( Result ) >= Ancho ) then
          Result := Copy( Result, 1, Ancho )
       else if ( Length( Result ) + 1 = Ancho ) then
          Result := Result + '%'
       else
           Result := '%' + Result + '%';
  end;

begin  { FiltraFunciones }
     with cdsDiccion do
     begin
          try
             DisableControls;
             Filter:= ConcatFiltros(FFiltro,Format( '(DI_NOMBRE LIKE ''%s'' or DI_TITULO LIKE ''%s'' or  DI_CLAVES LIKE ''%s'')', [ Limita( 10 ), Limita( 30 ), Limita( 255 ) ] ) );
             Filtered := TRUE;
             btnAyuda.Enabled := not Eof;
          finally
             EnableControls;
          end;
     end;
end;

procedure TEscogeFuncion.btnAyudaClick(Sender: TObject);
var
   sFuncion : String;
begin
     sFuncion := Trim( cdsDiccion.FieldByName( 'DI_NOMBRE' ).AsString );
     if Length( sFuncion  )< 10 then
        sFuncion := sFuncion + '()';
     LlamaAyuda( sFuncion );
end;

end.
