unit ZBaseWizardFiltro;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ComCtrls, ExtCtrls, DbGrids,
     ZetaCommonLists,
     ZBaseWizard,
     ZetaWizard,
     ZetaKeyLookup,
     ZetaEdit;

type
  TBaseWizardFiltro = class(TBaseWizard)
    FiltrosCondiciones: TTabSheet;
    FiltrosGB: TGroupBox;
    sFiltroLBL: TLabel;
    sCondicionLBl: TLabel;
    sFiltro: TMemo;
    GBRango: TGroupBox;
    lbInicial: TLabel;
    lbFinal: TLabel;
    BFinal: TSpeedButton;
    BInicial: TSpeedButton;
    BLista: TSpeedButton;
    RBTodos: TRadioButton;
    RBRango: TRadioButton;
    RBLista: TRadioButton;
    ECondicion: TZetaKeyLookup;
    EInicial: TZetaEdit;
    EFinal: TZetaEdit;
    ELista: TZetaEdit;
    BAgregaCampo: TBitBtn;
    Seleccionar: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RBTodosClick(Sender: TObject);
    procedure RBRangoClick(Sender: TObject);
    procedure RBListaClick(Sender: TObject);
    procedure BInicialClick(Sender: TObject);
    procedure BFinalClick(Sender: TObject);
    procedure BListaClick(Sender: TObject);
    procedure BAgregaCampoClick(Sender: TObject);
    procedure SeleccionarClick(Sender: TObject);
  private
    { Private declarations }
    FTipoRango: eTipoRangoActivo;
    FEmpleadoCodigo: String;
    FEmpleadoFiltro: String;
    FVerificacion: Boolean;
    function BuscaEmpleado(const lConcatena: Boolean; const sLlave: String): String;
    procedure SetVerificacion( const Value: Boolean );
  protected
    { Protected declarations }
    property EmpleadoCodigo: String read FEmpleadoCodigo write FEmpleadoCodigo;
    property EmpleadoFiltro: String read FEmpleadoFiltro write FEmpleadoFiltro;
    property Verificacion: Boolean read FVerificacion write FVerificacion;
    function Verificar: Boolean; virtual; abstract;
    function GetFiltro: String;
    function GetRango: String;
    function GetCondicion: String;
    procedure CargaListaVerificacion; virtual; abstract;
    procedure CargaParametros; override;
    procedure EnabledBotones( const eTipo: eTipoRangoActivo );
  public
    { Public declarations }
    property TipoRango: eTipoRangoActivo read FTipoRango;
  end;

var
  BaseWizardFiltro: TBaseWizardFiltro;

implementation

uses ZetaClientTools,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaBuscaEmpleado,
     ZConstruyeFormula,
     ZConfirmaVerificacion,
     DCliente,
     DCatalogos;

{$R *.DFM}

{ ************ TBaseWizardFiltro ************ }

procedure TBaseWizardFiltro.FormCreate(Sender: TObject);
begin
     inherited;
     FEmpleadoCodigo := ARROBA_TABLA + '.CB_CODIGO';
     FEmpleadoFiltro := '';
     ECondicion.LookupDataset := dmCatalogos.cdsCondiciones;
end;

procedure TBaseWizardFiltro.FormShow(Sender: TObject);
begin
     inherited;
     FVerificacion := False;
     ECondicion.Llave := VACIO;
     sFiltro.Text := VACIO;
     ELista.Text := IntToStr( dmCliente.Empleado );
     RBTodos.Checked := True;
     dmCatalogos.cdsCondiciones.Conectar;
end;

procedure TBaseWizardFiltro.SetVerificacion( const Value: Boolean );
begin
     FVerificacion := FVerificacion or Value;
end;

procedure TBaseWizardFiltro.EnabledBotones( const eTipo: eTipoRangoActivo );
var
   lEnabled: Boolean;
begin
     FTipoRango := eTipo;
     lEnabled := ( eTipo = raRango );
     lbInicial.Enabled := lEnabled;
     lbFinal.Enabled := lEnabled;
     EInicial.Enabled := lEnabled;
     EFinal.Enabled := lEnabled;
     bInicial.Enabled := lEnabled;
     bFinal.Enabled := lEnabled;
     lEnabled := ( eTipo = raLista );
     ELista.Enabled := lEnabled;
     bLista.Enabled := lEnabled;
end;

function TBaseWizardFiltro.GetRango: String;
begin
     case TipoRango of
          raRango: Result := GetFiltroRango( FEmpleadoCodigo, Trim( EInicial.Text ), Trim( EFinal.Text ) );
          raLista: Result := GetFiltroLista( FEmpleadoCodigo, Trim( ELista.Text ) );
     else
         Result := '';
     end;
end;

function TBaseWizardFiltro.GetCondicion: String;
begin
     if StrLleno( ECondicion.Llave ) then
     begin
          Result := Trim( ECondicion.LookUpDataSet.FieldByName( 'QU_FILTRO' ).AsString );
          { Causa problemas porque le quita la '@'; los paréntesis no son necesarios
          if EsFormula( Result ) then
             Result := Parentesis( Result );
          }
     end
     else
         Result := '';
end;

function TBaseWizardFiltro.GetFiltro: String;
begin
     Result := Trim( sFiltro.Text );
     { Los Paréntesis no son necesarios
     if StrLleno( Result ) then
        Result := Parentesis( Result );
     }
end;

procedure TBaseWizardFiltro.CargaParametros;
begin
     with ParameterList do
     begin
          AddString( 'RangoLista', GetRango );
          AddString( 'Condicion', GetCondicion );
          AddString( 'Filtro', GetFiltro );
     end;

     with Descripciones do
     begin
          AddString( 'Lista', GetRango );
          AddString( 'Condicion', GetCondicion );
          AddString( 'Filtro', GetFiltro );
     end;

     with dmCliente do
     begin
          CargaActivosIMSS( ParameterList );
          CargaActivosPeriodo( ParameterList );
          CargaActivosSistema( ParameterList );
     end;
end;

function TBaseWizardFiltro.BuscaEmpleado( const lConcatena: Boolean; const sLlave: String ): String;
var
   sKey, sDescripcion: String;
begin
     if ZetaBuscaEmpleado.BuscaEmpleadoDialogo( FEmpleadoFiltro, sKey, sDescripcion ) then
     begin
          if lConcatena and ZetaCommonTools.StrLleno( Text ) then
             Result := sLlave + ',' + sKey
          else
              Result := sKey;
     end;
end;

{ ********** Eventos ******** }

procedure TBaseWizardFiltro.RBTodosClick(Sender: TObject);
begin
     EnabledBotones( raTodos );
end;

procedure TBaseWizardFiltro.RBRangoClick(Sender: TObject);
begin
     EnabledBotones( raRango );
end;

procedure TBaseWizardFiltro.RBListaClick(Sender: TObject);
begin
     EnabledBotones( raLista );
end;

procedure TBaseWizardFiltro.BInicialClick(Sender: TObject);
begin
     with EInicial do
     begin
          Text := BuscaEmpleado( False, Text );
     end;
end;

procedure TBaseWizardFiltro.BFinalClick(Sender: TObject);
begin
     with EFinal do
     begin
          Text := BuscaEmpleado( False, Text );
     end;
end;

procedure TBaseWizardFiltro.BListaClick(Sender: TObject);
begin
     with ELista do
     begin
          Text := BuscaEmpleado( True, Text );
     end;
end;

procedure TBaseWizardFiltro.BAgregaCampoClick(Sender: TObject);
begin
     with sFiltro do
     begin
          Text:= ZConstruyeFormula.GetFormulaConst( enEmpleado, Text, SelStart, evBase );
     end;
end;

procedure TBaseWizardFiltro.SeleccionarClick(Sender: TObject);
var
   lCarga: Boolean;
   lOk: Boolean;
   oCursor: TCursor;
begin
     inherited;
     if Verificacion then
     begin
          case ZConfirmaVerificacion.ConfirmVerification of
               mrYes:
               begin
                    lCarga := True;
                    lOk := True;
               end;
               mrNo:
               begin
                    lCarga := False;
                    lOk := True;
               end;
          else
              begin
                   lCarga := False;
                   lOk := False;
              end;
          end;
     end
     else
     begin
          lCarga := True;
          lOk := True;
     end;
     if lOk then
     begin
          if lCarga then
          begin
               oCursor := Screen.Cursor;
               Screen.Cursor := crHourglass;
               try
                  CargaParametros;
                  CargaListaVerificacion;
               finally
                      Screen.Cursor := oCursor;
               end;
          end;
          SetVerificacion( Verificar );
     end;
end;

end.



