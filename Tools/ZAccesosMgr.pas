unit ZAccesosMgr;

interface

uses SysUtils,
     {$ifndef VER130}
     Variants,
     {$endif}
     ZetaCommonClasses,
     ZGlobalTress,
     ZAccesosTress;

type
     TDerechosAcceso = Smallint;
const
     K_SIN_RESTRICCION   = High(TDerechosAcceso);


function  CheckDerecho( const iPos, iDerecho : Integer ) : Boolean;
function  CheckUnDerecho( const NumeroDerecho, iDerecho : Integer ) : Boolean;
function  Revisa( const iPos: Integer ): Boolean;
function  RevisaCualquiera( const iPos : Integer ) : Boolean;
procedure LoadDerechos;
procedure SetValidacion( const lValor : Boolean );
procedure DerechosIguales( const iDerecho1, iDerecho2: Integer );
procedure ResetDerecho( const iDerecho, iNuevoDerecho: integer );


implementation

uses DCliente;

var
   aDerechos : Array[ 1..K_MAX_DERECHOS ] of TDerechosAcceso;
   lValidacion : Boolean;

procedure SetValidacion( const lValor : Boolean );
begin
    lValidacion := lValor;
end;

procedure LoadDerechos;
var
   i: Integer;
   aAccesos: Variant;
begin
     // Si grupo Activo es igual a 1 (Administradores):
     // tiene todos los derechos, else, ninguno.
     with dmCliente do
     begin
          if (  GetGrupoActivo = D_GRUPO_SIN_RESTRICCION ) or not lValidacion then
          begin
               for i := 1 to K_MAX_DERECHOS do
                   aDerechos[ i ] := K_SIN_RESTRICCION
          end
          else
          begin
               aAccesos := GetAccesos;
               //(@am): Esta linea de codigo, no es necesaria. Hacia que la aplicacion fallara al acceder con un usuario sin ningun derecho.
               {if aAccesos = NULL then
                    raise Exception.Create( 'No Tiene Derechos De Acceso A Esta Empresa' )
               else}
               if aAccesos <> NULL then
               begin
                    for i := 1 to K_MAX_DERECHOS do
                    begin
                         aDerechos[ i ] := aAccesos[ i ];
                    end;
               end;
          end;
     end;
end;

function  CheckUnDerecho( const NumeroDerecho, iDerecho : Integer ) : Boolean;
begin
     // Se requiere validar que sea mayor que 0
     if ( lValidacion ) then
        Result :=  ( ( NumeroDerecho and iDerecho ) > K_SIN_DERECHOS )
     else
        Result := TRUE;
end;

function CheckDerecho( const iPos, iDerecho : Integer ) : Boolean;
begin
     // Se requiere validar que sea mayor que 0
     if ( lValidacion ) then
        Result := ( iPos > 0 ) and (( aDerechos[ iPos ] and iDerecho ) > K_SIN_DERECHOS )
     else
        Result := TRUE;
     {$ifdef APAGA_DERECHOS_COVIDIEN}
     if NOT Result and Global.OK_ProyectoEspecial( peTransferencias ) and ( (iPos = D_COSTEO_TRANSFERENCIAS) or (iPos = D_COSTEO_MOTIVOS_TRANSFER)) then
        Result := TRUE;
     {$ENDIF}
end;

function RevisaCualquiera( const iPos : Integer ) : Boolean;
begin
     // Se requiere validar que sea mayor que 0
     // No valida contra un tipo de derecho, revisa que tenga al menos un derecho
     if ( lValidacion ) then
        Result := ( iPos > 0 ) and ( aDerechos[ iPos ] > K_SIN_DERECHOS )
     else
        Result := TRUE;
end;

procedure DerechosIguales( const iDerecho1, iDerecho2: Integer );
begin
    { Permite tener dos derechos de acceso que afectan a la misma forma }
     aDerechos[ iDerecho1 ] := aDerechos[ iDerecho1 ] or aDerechos[ iDerecho2 ];
     aDerechos[ iDerecho2 ] := aDerechos[ iDerecho1 ];
end;

procedure ResetDerecho( const iDerecho, iNuevoDerecho: integer );
begin
     aDerechos[ iDerecho ] := iNuevoDerecho;
end;

function Revisa( const iPos: Integer ): Boolean;
begin
     Result := ZAccesosMgr.CheckDerecho( iPos, K_DERECHO_CONSULTA );
end;

initialization
    lValidacion := TRUE;

end.
