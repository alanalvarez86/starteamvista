unit ZTreeTools;

interface

uses Forms,
     SysUtils,
     ComCtrls,
     ImgList,
     ZetaClientDataSet,
     ZetaCommonClasses,
     Controls,
     cxTreeview,
     CxGraphics;

const
     K_SEPARADOR = ' = ';

function GetNodo( oArbol: TTreeView; const sCodigo: String ): TTreeNode;overload
function GetNodo( oArbol: TcxTreeView; const sCodigo: String ): TTreeNode;overload;
function GetTextoNodo( const oNodo: TTreeNode; sSeparador: string = ' = ' ): String;
function ObtenNodoSeleccionado( oArbol: TTreeview ): string; overload;
function ObtenNodoSeleccionado( oArbol: TcxTreeview ): string; overload;
function InitArbol( oArbol: TTreeView; ImageList: TImageList; const sNodoPadre: string ): TTreeNode;overload;
function InitArbol( oArbol: TcxTreeView; ImageList: TcxImageList; const sNodoPadre: string ): TTreeNode;overload;
function EsNodo( const oNodo: TTreeNode ): boolean;
procedure CreaArbol( oArbol: TTreeView; ImageList: TImageList; oDataset: TZetaClientDataSet; oPadre: TTreeNode;
                     const sCampoPadre, sCampoCodigo, sCampoDescripcion: string;
                     const sCodigoDefault: string = VACIO ); overload;
procedure CreaArbol( oArbol: TcxTreeView; ImageList: TcxImageList; oDataset: TZetaClientDataSet; oPadre: TTreeNode;
                     const sCampoPadre, sCampoCodigo, sCampoDescripcion: string;
                     const sCodigoDefault: string = VACIO );  overload;
procedure SetImage( const oNodo: TTreeNode; ImageList: TImageList; const iImageIndex: integer; iImageSelected: integer = 0 );overload ;
procedure SetImage( const oNodo: TTreeNode; ImageList: TcxImageList; const iImageIndex: integer; iImageSelected: integer = 0 );overload;
procedure CambiaArbol( oArbol: TTreeView; const lAccion: boolean ); overload ;
procedure CambiaArbol( oArbol: TcxTreeView; const lAccion: boolean ); overload;
implementation

uses
    DCliente,
    DCatalogos,
    ZetaCommonTools,
    ZetaDialogo;

const
     K_IMAGEN_PADRE = 0;                  //Siempre debe ser la Imagen 0 del Imagelist
     K_IMAGEN_CODIGO = 1;                 //Siempre debe ser la Imagen 1 del ImageList
     K_IMAGEN_CODIGO_SELECCIONADO = 2;    //Siempre debe ser la Imagen 2 del Imagelist

function InitArbol( oArbol: TTreeView; ImageList: TImageList; const sNodoPadre: string ): TTreeNode;
var
   oPadre: TTreeNode;
begin
     oArbol.Items.BeginUpdate;
     try
        oArbol.Items.Clear;
        oPadre := oArbol.Items.AddFirst( NIL, sNodoPadre );
        SetImage( oPadre, ImageList,  K_IMAGEN_PADRE );
        Result := oPadre;
     finally
            oArbol.Items.EndUpdate;
     end;
end;

function InitArbol( oArbol: TcxTreeView; ImageList: TcxImageList; const sNodoPadre: string ): TTreeNode;
var
   oPadre: TTreeNode;
begin
     oArbol.Items.BeginUpdate;
     try
        oArbol.Items.Clear;
        oPadre := oArbol.Items.AddFirst( NIL, sNodoPadre );
        SetImage( oPadre, ImageList,  K_IMAGEN_PADRE );
        Result := oPadre;
     finally
            oArbol.Items.EndUpdate;
     end;
end;
procedure CreaArbol( oArbol: TTreeView; ImageList: TImageList; oDataset: TZetaClientDataSet; oPadre: TTreeNode;
                     const sCampoPadre, sCampoCodigo, sCampoDescripcion: string;
                     const sCodigoDefault: string = VACIO );
const
     K_MAX_NIVEL = 100;
     K_SIN_NODO = 0;
     K_CON_NODO = 1;

var
   oHijo, oNodoDefault: TTreeNode;
   i, iContador, iCount: integer;
   lOk: boolean;
   sPadre, sCodigo: string;
   aVerifica: Array of integer;

begin
     oNodoDefault := oPadre;
     sPadre := GetTextoNodo( oPadre );
     oArbol.Items.BeginUpdate;
     try
        iContador := 0;
        with oDataSet do
        begin
             DisableControls;
             try
                First;
                SetLength( aVerifica, RecordCount );
                { Se llena el Arreglo de Veficación}
                for i := 0 to ( RecordCount - 1 ) do
                    aVerifica[ i ] := K_SIN_NODO;

                Repeat
                      lOk := True;
                      iCount := 0;
                      while not EOF do
                      begin
                           if ( aVerifica[ iCount ] <> K_CON_NODO ) then
                           begin
                                sCodigo := FieldByName( sCampoCodigo ).AsString;
                                if ( sCodigo = sPadre ) then
                                   aVerifica[ iCount ] := K_CON_NODO
                                else
                                begin
                                     oPadre := GetNodo( oArbol, FieldByName( sCampoPadre ).AsString );
                                     if ( oPadre = nil ) then
                                        lOk := False
                                     else
                                     begin
                                          oHijo := oArbol.Items.AddChild( oPadre, sCodigo + K_SEPARADOR + FieldByName( sCampoDescripcion ).AsString );
                                          if ( strLleno( sCodigoDefault ) and ( sCodigoDefault = sCodigo ) ) then
                                             oNodoDefault := oHijo;
                                          SetImage( oHijo, ImageList, K_IMAGEN_CODIGO, K_IMAGEN_CODIGO_SELECCIONADO );
                                          aVerifica[ iCount ] := K_CON_NODO;
                                     end;
                                end;
                           end;
                           Inc( iCount );
                           Next;
                      end; //While
                      Inc( iContador );
                      if ( iContador >= K_MAX_NIVEL ) then     //100 Niveles Maximo
                      begin
                           lOk := True;
                           ZetaDialogo.ZError( 'Aviso', 'ˇ Existe una Relación Circular !', 0 );
                      end;
                      if not lOk then
                         First;
                until ( lOk = True );
             finally
                    EnableControls;
             end;
        end;
     finally
            with oArbol do
            begin
                 if ( oNodoDefault <> nil ) then
                 begin
                      with oNodoDefault do
                      begin
                           Expanded := True;
                           Selected := True;
                      end;
                 end;
                 FullExpand;
                 Items.EndUpdate;
            end;
     end;
end;

procedure CreaArbol( oArbol: TcxTreeView; ImageList: TcxImageList; oDataset: TZetaClientDataSet; oPadre: TTreeNode;
                     const sCampoPadre, sCampoCodigo, sCampoDescripcion: string;
                     const sCodigoDefault: string = VACIO );
const
     K_MAX_NIVEL = 100;
     K_SIN_NODO = 0;
     K_CON_NODO = 1;

var
   oHijo, oNodoDefault: TTreeNode;
   i, iContador, iCount: integer;
   lOk: boolean;
   sPadre, sCodigo: string;
   aVerifica: Array of integer;

begin
     oNodoDefault := oPadre;
     sPadre := GetTextoNodo( oPadre );
     oArbol.Items.BeginUpdate;
     try
        iContador := 0;
        with oDataSet do
        begin
             DisableControls;
             try
                First;
                SetLength( aVerifica, RecordCount );
                { Se llena el Arreglo de Veficación}
                for i := 0 to ( RecordCount - 1 ) do
                    aVerifica[ i ] := K_SIN_NODO;

                Repeat
                      lOk := True;
                      iCount := 0;
                      while not EOF do
                      begin
                           if ( aVerifica[ iCount ] <> K_CON_NODO ) then
                           begin
                                sCodigo := FieldByName( sCampoCodigo ).AsString;
                                if ( sCodigo = sPadre ) then
                                   aVerifica[ iCount ] := K_CON_NODO
                                else
                                begin
                                     oPadre := GetNodo( oArbol, FieldByName( sCampoPadre ).AsString );
                                     if ( oPadre = nil ) then
                                        lOk := False
                                     else
                                     begin
                                          oHijo := oArbol.Items.AddChild( oPadre, sCodigo + K_SEPARADOR + FieldByName( sCampoDescripcion ).AsString );
                                          if ( strLleno( sCodigoDefault ) and ( sCodigoDefault = sCodigo ) ) then
                                             oNodoDefault := oHijo;
                                          SetImage( oHijo, ImageList, K_IMAGEN_CODIGO, K_IMAGEN_CODIGO_SELECCIONADO );
                                          aVerifica[ iCount ] := K_CON_NODO;
                                     end;
                                end;
                           end;
                           Inc( iCount );
                           Next;
                      end; //While
                      Inc( iContador );
                      if ( iContador >= K_MAX_NIVEL ) then     //100 Niveles Maximo
                      begin
                           lOk := True;
                           ZetaDialogo.ZError( 'Aviso', 'ˇ Existe una Relación Circular !', 0 );
                      end;
                      if not lOk then
                         First;
                until ( lOk = True );
             finally
                    EnableControls;
             end;
        end;
     finally
            with oArbol do
            begin
                 if ( oNodoDefault <> nil ) then
                 begin
                      with oNodoDefault do
                      begin
                           Expanded := True;
                           Selected := True;
                      end;
                 end;
                 FullExpand;
                 Items.EndUpdate;
            end;
     end;
end;



function ObtenNodoSeleccionado( oArbol: TcxTreeview ): string;
var
   oNodo: TTreeNode;
begin
     oNodo := oArbol.Selected;
     if ( oNodo <> nil ) then
        Result := GetTextoNodo( oNodo )
     else
         Result := VACIO;
end;

function ObtenNodoSeleccionado( oArbol: TTreeview ): string;
var
   oNodo: TTreeNode;
begin
     oNodo := oArbol.Selected;
     if ( oNodo <> nil ) then
        Result := GetTextoNodo( oNodo )
     else
         Result := VACIO;
end;

procedure SetImage( const oNodo: TTreeNode; ImageList: TImageList; const iImageIndex: integer; iImageSelected: integer = 0 );
begin
     with oNodo do
     begin
          ImageIndex := iImageIndex;
          SelectedIndex := iImageSelected;
     end;
end;
procedure SetImage( const oNodo: TTreeNode; ImageList: TcxImageList; const iImageIndex: integer; iImageSelected: integer = 0 );
begin
     with oNodo do
     begin
          ImageIndex := iImageIndex;
          SelectedIndex := iImageSelected;
     end;
end;

function GetNodo( oArbol: TTreeView; const sCodigo: String ): TTreeNode;
var
   oNodo: TTreeNode;
   lSalir: Boolean;

begin
     Result := nil;

     oNodo  := oArbol.Items[ 0 ];
     lSalir := False;
     repeat
           if ( sCodigo = GetTextoNodo( oNodo ) ) then
           begin
                Result := oNodo;
                lSalir := True;
           end;
           oNodo := oNodo.GetNext;
     until ( oNodo = nil ) or lSalir;
end;
function GetNodo( oArbol: TcxTreeView; const sCodigo: String ): TTreeNode;
var
   oNodo: TTreeNode;
   lSalir: Boolean;

begin
     Result := nil;

     oNodo  := oArbol.Items[ 0 ];
     lSalir := False;
     repeat
           if ( sCodigo = GetTextoNodo( oNodo ) ) then
           begin
                Result := oNodo;
                lSalir := True;
           end;
           oNodo := oNodo.GetNext;
     until ( oNodo = nil ) or lSalir;
end;

function GetTextoNodo( const oNodo: TTreeNode; sSeparador: string = K_SEPARADOR ): String;
var
   sTemporal: string;
begin
     sTemporal := oNodo.Text;
     Result := copy( sTemporal, 1, pos( sSeparador, sTemporal ) - 1 );
end;

procedure CambiaArbol( oArbol: TTreeView; const lAccion: boolean );
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with oArbol do
        begin
             if lAccion then
                FullCollapse
             else
                 FullExpand;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure CambiaArbol( oArbol: TcxTreeView; const lAccion: boolean );
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with oArbol do
        begin
             if lAccion then
                FullCollapse
             else
                 FullExpand;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;


function EsNodo( const oNodo: TTreeNode): Boolean;
begin
     Result := Assigned( oNodo );
end;

end.
