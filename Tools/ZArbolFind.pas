unit ZArbolFind;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ExtCtrls, ComCtrls, Buttons;

type
  TBuscaArbol = class(TForm)
    Nodo: TEdit;
    NodoLBL: TLabel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    FArbol: TTreeView;
    FCaption : String;
  public
    { Public declarations }
  end;

var
  BuscaArbol: TBuscaArbol;

procedure BuscaNodoDialogo( Target: TTreeView; sCaption : String );

implementation

{$R *.DFM}

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo,
     ZArbolTools;

procedure BuscaNodoDialogo( Target: TTreeView; sCaption : String );
begin
     if ( BuscaArbol = nil ) then
        BuscaArbol := TBuscaArbol.Create( Application );

     BuscaNodoReset;
     with BuscaArbol do
     begin
          FArbol   := Target;
          FCaption := sCaption;
          ShowModal;
     end;
end;

{ ********** TBuscaArbol ********** }

procedure TBuscaArbol.FormShow(Sender: TObject);
begin
     with Nodo do
     begin
          SetFocus;
          SelectAll;
     end;
end;

procedure TBuscaArbol.OKClick(Sender: TObject);
begin
     with Nodo do
     begin
          if StrLleno( Text ) then
          begin
               BuscaNodoTexto( FArbol, FCaption, Text );
               SetFocus;
               SelectAll;
          end;
     end;
end;



end.
