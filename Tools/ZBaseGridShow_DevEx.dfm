inherited BaseGridShow_DevEx: TBaseGridShow_DevEx
  BorderIcons = [biSystemMenu, biMaximize]
  BorderStyle = bsSizeable
  Caption = 'BaseGridShow_DevEx'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    inherited OK_DevEx: TcxButton
      Left = 211
      Top = 5
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 290
      Top = 5
      Cancel = True
    end
  end
  object ZetaDBGrid: TZetaCXGrid [1]
    Left = 0
    Top = 0
    Width = 375
    Height = 164
    Align = alClient
    TabOrder = 1
    object ZetaDBGridDBTableView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      FilterBox.Visible = fvNever
      DataController.DataSource = DataSource
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.FocusCellOnTab = True
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnHidingOnGrouping = False
      OptionsCustomize.ColumnMoving = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsSelection.CellSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
    end
    object ZetaDBGridLevel: TcxGridLevel
      GridView = ZetaDBGridDBTableView
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  object DataSource: TDataSource
    Left = 104
    Top = 8
  end
end
