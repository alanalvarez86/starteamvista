unit FSeleccionarConfidencialidad_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, CheckLst, Buttons, ExtCtrls,
  ZetaCommonTools, ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, ImgList, cxButtons, cxControls, cxContainer, cxEdit,
  cxCheckListBox;

type
  TSeleccionarConfidencialidad_DevEx = class(TZetaDlgModal_DevEx)
    CM_NIVEL0_DevEx: TcxCheckListBox;
  private
    { Private declarations }
    procedure SetListConfidencialidad( sValores : string;  sConfidencialidadEmpresa : string = '' );
    function GetListConfidencialidad : string;
    function GetName(sTexto:String):string;
  public
    { Public declarations }
    function GetConfidencialidad( sCM_NIVEL0 : string;  lValores : TStringList ; lFromTressCatalogo : boolean = FALSE; sConfidencialidadEmpresa : string = '' ) : string;

  end;

var
  SeleccionarConfidencialidad_DevEx: TSeleccionarConfidencialidad_DevEx;

implementation

{$R *.dfm}

{ TSeleccionarConfidencialidad }

procedure TSeleccionarConfidencialidad_DevEx.SetListConfidencialidad( sValores : string ; sConfidencialidadEmpresa : string );
var
   Lista : TStringList;
   ListaEmpresa : TStringList;
   i, j, idx : integer;
   sNivel0: string;
begin
    Lista := TStringList.Create;
    ListaEmpresa :=TStringList.Create;

    sValores := Trim( sValores );
    if StrLleno( sValores ) then
        Lista.CommaText  := sValores;

    sValores := Trim( sConfidencialidadEmpresa );
    if StrLleno( sValores ) then
        ListaEmpresa.CommaText  := sValores;

    for i:= 0 to CM_NIVEL0_DevEx.Items.Count - 1 do
    begin
        CM_NIVEL0_DevEx.Items[i].Checked := FALSE;
        sNivel0:= GetName(CM_NIVEL0_DevEx.Items[i].Text);

        if ( Lista.Count > 0 ) then
        begin
                j := Lista.IndexOf( sNivel0  );
                CM_NIVEL0_DevEx.Items[i].Checked :=  ( j >= 0 );
        end;
        CM_NIVEL0_DevEx.Items[i].Enabled := TRUE;

        if Strlleno( sConfidencialidadEmpresa ) then
        begin
              CM_NIVEL0_DevEx.Items[i].Enabled :=  ListaEmpresa.Find(sNivel0,idx);
        end;
    end;
    FreeAndNil( Lista );
    FreeAndNil( ListaEmpresa );
end;

//DevEx(by am): Agregado porque el checklistbox de DevExpresss no tiene propiedad Names.
function TSeleccionarConfidencialidad_DevEx.GetName(sTexto:String):string;
var
   posSigno:Integer;
const
     K_IGUAL= '=';
     K_VACIO= '';
begin
     Result:=K_VACIO;
      posSigno:= Pos(K_IGUAL,sTexto);
        if posSigno <>0 then
           Result:=Trim(Copy(sTexto,0,posSigno-1));
end;

function TSeleccionarConfidencialidad_DevEx.GetListConfidencialidad : string;
var
   i : integer;
begin
    Result := '';
    for i:= 0 to CM_NIVEL0_DevEx.Items.Count - 1 do
    begin
        if CM_NIVEL0_DevEx.Items[i].Checked then
        begin
             if StrLleno( Result ) then
                Result := Result + ',';
             Result := Result +  GetName(CM_NIVEL0_DevEx.Items[i].Text);
        end;
    end;

end;

function TSeleccionarConfidencialidad_DevEx.GetConfidencialidad(
  sCM_NIVEL0: string;   lValores : TStringList;  lFromTressCatalogo : boolean = FALSE; sConfidencialidadEmpresa : string = '' ): string;
procedure FillCheckBoxList;
var i:Integer;
begin
    with CM_NIVEL0_DevEx do
     begin
          Items.BeginUpdate;
          try
             Clear;
             with lValores do
             begin
                  for i := 0 to ( Count - 1 ) do
                  begin
                       Items.Add.Text := lValores[i];
                  end;
             end;
          finally
                 Items.EndUpdate;
          end;
     end;
end;
begin
    Result := sCM_NIVEL0;
    FillCheckBoxList; //DevEx(by am):No se pueden pasar directamente un StringList a un cxCheckBoxList pues este recibe CheckBoxListItems no StringListItems.

    if (  lFromTressCatalogo ) then
       SetListConfidencialidad( sCM_NIVEL0, sConfidencialidadEmpresa)
    else
        SetListConfidencialidad( sCM_NIVEL0 ) ;

    ShowModal;
    if ( ModalResult = mrOk ) then
    begin
         try
             Result := GetListConfidencialidad;
         except
               on Error: Exception do
               begin
                    Application.HandleException( Error );
               end;
         end;
    end;
end;

end.
