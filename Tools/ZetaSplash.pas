unit ZetaSplash;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, StdCtrls, ExtCtrls, jpeg;

type
  TSplashScreen = class(TForm)
    Logo: TImage;
    Copyright: TLabel;
    Version: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Show;
  end;

var
  SplashScreen: TSplashScreen;

implementation

uses ZetaClientTools,
     ZetaCommonTools;

{$R *.DFM}

procedure TSplashScreen.FormCreate(Sender: TObject);
const
     {$ifdef DOS_CAPAS}
     K_MENSAJE ='%s Profesional';
     {$else}
     K_MENSAJE ='%s ';
     {$endif}
begin
     Version.Caption := Format( K_MENSAJE, [ ZetaClientTools.GetApplicationProductVersion ] );
     Copyright.Caption := Format( '(c) 1996, %d Grupo TRESS Internacional, S.A. de C.V.', [ ZetaCommonTools.TheYear( Date ) ] );
end;

procedure TSplashScreen.Show;
begin
     inherited;
     Update;
end;

end.
