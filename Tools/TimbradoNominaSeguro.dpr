program TimbradoNominaSeguro;

uses
  Forms,
  ComObj,
  ActiveX,
  ZBaseShell in 'ZBaseShell.pas' {BaseShell},
  ZBaseImportaShell in 'ZBaseImportaShell.pas' {BaseImportaShell},
  ZetaSplash in 'ZetaSplash.pas' {SplashScreen},
  ZBaseDlgModal in 'ZBaseDlgModal.pas' {ZetaDlgModal},
  FTressShell in '..\Utileria\RecibosDigitales\FTressShell.pas' {TressShell},
  DInterfase in '..\Utileria\RecibosDigitales\DInterfase.pas' {dmInterfase: TDataModule},
  Timbres in '..\Utileria\RecibosDigitales\Timbres.pas',
  FTimbramexHelper in '..\Utileria\RecibosDigitales\FTimbramexHelper.pas',
  ZetaClientTools in '..\Componentes\ZetaClientTools.pas',
  FTimbramexClasses in '..\Utileria\RecibosDigitales\FTimbramexClasses.pas';

{$R *.RES}
{$R WindowsXP.res}
{$R ..\Super\Languages.RES}

procedure MuestraSplash;
begin
     SplashScreen := TSplashScreen.Create( Application );
     with SplashScreen do
     begin
          Show;
          Update;
     end;
end;

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;

 

  SplashScreen := TSplashScreen.Create( Application );
  with SplashScreen do
  begin
       Show;
       Update;
  end;
  Application.Title := 'Timbrado de N�mina - Sistema TRESS';
  Application.HelpFile := 'TimbradoNomina.chm';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
     begin

        if Login( False ) then
        begin
             CierraSplash;
             Show;
             Update;
             BeforeRun;
             Application.Run;
        end
        else
        begin
             CierraSplash;
             Free;
        end;

     end;
end.
