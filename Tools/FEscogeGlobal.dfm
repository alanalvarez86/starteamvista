inherited EscogeGlobal: TEscogeGlobal
  Top = 222
  Caption = 'Escoge Global'
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel2: TPanel
    object Label1: TLabel
      Left = 16
      Top = 14
      Width = 65
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo de Dato:'
    end
    object TipoGlobal: TComboBox
      Left = 87
      Top = 11
      Width = 145
      Height = 21
      DropDownCount = 6
      ItemHeight = 13
      TabOrder = 0
      OnChange = TipoGlobalChange
      Items.Strings = (
        'L�gicos'
        'N�meros con Decimales'
        'N�meros Enteros'
        'Fechas'
        'Textos'
        'Todos')
    end
  end
  inherited Grid: TZetaDBGrid
    Columns = <
      item
        Expanded = False
        FieldName = 'gl_codigo'
        PickList.Strings = ()
        Title.Caption = 'C�digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'gl_descrip'
        PickList.Strings = ()
        Title.Caption = 'Descripci�n'
        Width = 354
        Visible = True
      end>
  end
  inherited DataSource: TDataSource
    Left = 344
  end
end
