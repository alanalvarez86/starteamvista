unit ZBaseEdicionRenglon_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, ComCtrls, Db,
     ExtCtrls, Buttons, DBCtrls, StdCtrls,
     ZetaClientDataset,
     ZetaDBGrid,
     ZetaMessages, ZetaSmartLists,ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013,
  dxSkinsDefaultPainters, cxControls,
  dxSkinsdxBarPainter, ImgList, dxBarExtItems, dxBar, cxClasses,
  cxNavigator, cxDBNavigator, cxButtons, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxPC;

type
  TBaseEdicionRenglon_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    PageControl: TcxPageControl;
    Datos: TcxTabSheet;
    Tabla: TcxTabSheet;
    GridRenglones: TZetaDBGrid;
    dsRenglon: TDataSource;
    Panel2: TPanel;
    BBAgregar_DevEx: TcxButton;
    BBBorrar_DevEx: TcxButton;
    BBModificar_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure GridRenglonesTitleClick(Column: TColumn);
    procedure GridRenglonesDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure MyMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
  private
    { Private declarations }
    FPrimerColumna: Integer;
  protected
    { Protected declarations }
    property PrimerColumna: Integer read FPrimerColumna write FPrimerColumna;
    function ClientDatasetHijo: TZetaClientDataset;
    function GridEnfocado: Boolean;
    function CheckDerechosPadre: Boolean;
    function PosicionaSiguienteColumna: Integer; Virtual;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure HabilitaControles; override;
    procedure KeyDown( var Key: Word; Shift: TShiftState ); override;
    procedure SeleccionaPrimerColumna;
    procedure Setok;

  public
    { Public declarations }
  end;
  TDBGridHack = class(TDBGrid);

var
  BaseEdicionRenglon_DevEx: TBaseEdicionRenglon_DevEx;

implementation

uses ZetaDialogo,
     ZetaCommonClasses;

{$R *.DFM}

procedure TBaseEdicionRenglon_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     GridRenglones.Options := [ dgEditing,
                                dgTitles,
                                dgIndicator,
                                dgColumnResize,
                                dgColLines,
                                dgRowLines,
                                dgTabs,
                                dgCancelOnExit ];
     FPrimerColumna := 0;

     //DevEx (by am):Colores Base para el grid de Edicion
    GridRenglones.FixedColor := RGB(235,235,235);//Gris
    GridRenglones.TitleFont.Color := RGB(77,59,75);//Gris-Morado tono fuerte
    GridRenglones.Font.Color := RGB(156,129,139);//Gris-Morado
    //DevEx (by am): Asigna el evento OnMouseWheel al grid de edicion
    TDBGridHack( GridRenglones ).OnMouseWheel:= MyMouseWheel;
end;

procedure TBaseEdicionRenglon_DevEx.MyMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
     Handled := True;
     with GridRenglones.DataSource.DataSet do
     begin
          if WheelDelta > 0 then
             Prior
          else
              Next;
     end;
end;

procedure TBaseEdicionRenglon_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := Datos;
end;

procedure TBaseEdicionRenglon_DevEx.HabilitaControles;
begin
     inherited HabilitaControles;
     { Impide que usuarios sin derechos de Modificación }
     { puedan llegar a modo de Edición simplemente cambiando }
     { el contenido de un control }
     dsRenglon.AutoEdit := Editing or CheckDerechos( K_DERECHO_CAMBIO );
end;

procedure TBaseEdicionRenglon_DevEx.KeyDown( var Key: Word; Shift: TShiftState );
begin
     if GridEnfocado and not ( ClientDatasetHijo.State in [ dsInsert, dsEdit ] ) then
     begin
          if ( ssShift in Shift ) then { SHIFT }
          begin
               case Key of
                    VK_INSERT:    { INS = Agregar }
                    begin
                         Key := 0;
                         DoInsert;
                    end;
                    VK_DELETE:    { DEL = Borrar }
                    begin
                         Key := 0;
                         DoDelete;
                    end;
               end;
          end
          else
              if ( ssCtrl in Shift ) then { CTRL }
              begin
                   case Key of
                        VK_INSERT:   { INS = Modificar }
                        begin
                             Key := 0;
                             DoEdit;
                        end;
                   end;
              end;
     end;
     inherited KeyDown( Key, Shift );
end;

function TBaseEdicionRenglon_DevEx.ClientDatasetHijo: TZetaClientDataset;
begin
     Result := TZetaClientDataset( dsRenglon.Dataset );
end;

function TBaseEdicionRenglon_DevEx.CheckDerechosPadre: Boolean;
var
   sMensaje: String;
begin
     if Editing then
        Result := True
     else
         if PuedeModificar( sMensaje ) then
            Result := True
         else
         begin
              ZetaDialogo.zInformation( Caption, sMensaje, 0 );
              Result := False;
         end;
end;

function TBaseEdicionRenglon_DevEx.GridEnfocado: Boolean;
begin
     Result := ( ActiveControl = GridRenglones );
end;

function TBaseEdicionRenglon_DevEx.PosicionaSiguienteColumna: Integer;
var
   i: Integer;
begin
     with GridRenglones do
     begin
          i := SelectedIndex + 1;
          while ( i < Columns.Count ) and ( Columns[ i ].ReadOnly or not Columns[ i ].Visible ) do
          begin
               i := i + 1;
          end;
          if ( i = Columns.Count ) then
             Result := FPrimerColumna
          else
              Result := i;
     end;
end;

procedure TBaseEdicionRenglon_DevEx.SeleccionaPrimerColumna;
begin
     Self.ActiveControl := GridRenglones;
     GridRenglones.SelectedField := GridRenglones.Columns[ FPrimerColumna ].Field;
end;

procedure TBaseEdicionRenglon_DevEx.Setok;
begin
     if OK_DevEx.Enabled then
        OK_DevEx.SetFocus
     else
         Cancelar_DevEx.SetFocus;
end;

procedure TBaseEdicionRenglon_DevEx.Agregar;
begin
     if GridEnfocado then
        BBAgregar_DevEx.Click
     else
         inherited Agregar;
end;

procedure TBaseEdicionRenglon_DevEx.BBAgregarClick(Sender: TObject);
begin
     inherited;
     if CheckDerechosPadre then
     begin
          with ClientDatasetHijo do
          begin
               Append;
          end;
          GridRenglones.SetFocus;
     end;                              
end;

procedure TBaseEdicionRenglon_DevEx.Borrar;
begin
     if GridEnfocado then
        BBBorrar_DevEx.Click
     else
         inherited Borrar;
end;

procedure TBaseEdicionRenglon_DevEx.BBBorrarClick(Sender: TObject);
begin
     inherited;
     if CheckDerechosPadre then
     begin
          with ClientDatasetHijo do
          begin
               if not IsEmpty then
                  Delete;
          end;
     end;
end;

procedure TBaseEdicionRenglon_DevEx.Modificar;
begin
     if GridEnfocado then
        BBModificar_DevEx.Click
     else
         inherited Modificar;
end;

procedure TBaseEdicionRenglon_DevEx.BBModificarClick(Sender: TObject);
begin
     inherited;
     if CheckDerechosPadre then
     begin
          with ClientDatasetHijo do
          begin
               if not IsEmpty then
                  Edit;
          end;
          GridRenglones.SetFocus;
     end;
end;

procedure TBaseEdicionRenglon_DevEx.GridRenglonesTitleClick(Column: TColumn);
begin
     inherited;
     { QUEDA VACIA HASTA QUE ENCONTREMOS UNA SOLUCION }
     { PARA QUE NO APAREZCAN TODO EL HIJO }
end;

procedure TBaseEdicionRenglon_DevEx.GridRenglonesDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
   iCurrentRow, iRecNo: Integer;
begin
  inherited;
  with GridRenglones, Canvas, Brush do
    begin
         if ( State = [] ) then
         begin
              iCurrentRow := Rect.Top div (1+Rect.Bottom-Rect.Top);
              iRecNo:= TDBGridHack( GridRenglones ).Row;
              if iCurrentRow = iRecNo then
              begin
                   //Color:= clBtnShadow;
                   Color:=  RGB(237,230,249);//RGB(156,129,139); //morado row seleccionado
                   Font.Color := clWindowText;
              end;
         end
         else if gdSelected in State then
         begin
              //Color:= clHighlight;
              Color:= RGB(212,188,251);//RGB(136,174,213); //Azul de celda seleccionado
              Font.Color := clHighlightText; //Texto en blanco para celda seleccionada
         end;
         DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;
    {with GridRenglones, Canvas do
         begin
              if gdSelected in State then
               begin
                    //Color:= clHighlight;
                    Brush.Color:= RGB(212,188,251);//RGB(136,174,213); //Azul de celda seleccionado
                    Font.Color := clHighlightText; //Texto en blanco para celda seleccionada
              end;
              DefaultDrawColumnCell(Rect, DataCol, Column, State);
         end;}
end;

end.
