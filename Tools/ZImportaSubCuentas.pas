unit ZImportaSubCuentas;

interface
uses Windows, Messages, SysUtils, Classes, dbClient, Dialogs,
     {$ifndef VER130}Variants,{$endif}
     ZetaClientDataSet;

type
  TImportaSubCuentas = class

  private
    FCodigo: string;
    FSubCuenta: string;
    FRenglon: string;
    FCampoSubCuenta: string;
    FCampoCodigo: string;
    FDataSetCatalogo: TZetaClientDataset;
    FDatasetImportar: TClientDataSet;
    procedure EditaDataSet;
    function GetSubCuentaAscii: Boolean;

  public
    constructor Create;
    destructor Destroy;override;
    property CampoCodigo : string read FCampoCodigo write FCampoCodigo;
    property CampoSubCuenta : string read FCampoSubcuenta write FCampoSubCuenta;
    property DataSetCatalogo : TZetaClientDataset read FDataSetCatalogo write FDataSetCatalogo;
    procedure ImportaAscii(const sArchivo: string);
    procedure ImportaDataset(const sArchivo: string);
  end;


procedure ImportaArchivo( oDataSet: TZetaClientDataset; const sCodigo, sSubCuenta: string );

implementation
uses
    DCatalogos,
    ZetaCommonClasses,
    ZetaDialogo;


procedure ImportaArchivo( oDataSet: TZetaClientDataset; const sCodigo, sSubCuenta: string );
 var OpenDialog : TOpenDialog;
     oImportador: TImportaSubCuentas;
begin
     OpenDialog := TOpenDialog.Create( NIL );

     try
        //OpenDialog.HelpContext := Pendiente;

        with OpenDialog do
        begin
             DefaultExt := 'cds';
             Filename := '*.cds';
             Filter := 'Archivos Temporales (*.cds)|*.cds|'+
                       'Ascii Delimitado (*.txt)|*.txt|';

             if Execute then
             begin
                  oImportador := TImportaSubCuentas.Create;
                  try
                     with oImportador do
                     begin
                          CampoCodigo := sCodigo;
                          CampoSubCuenta := sSubCuenta;
                          DataSetCatalogo := oDataset;

                          if Pos( 'cds', FileName ) > 0  then
                             ImportaDataset( FileName )
                          else
                              ImportaAscii( Filename );
                     end;

                  finally
                         FreeAndNil( oImportador );
                  end;
             end;
        end;
     finally
            FreeAndNil( OpenDialog );
     end;
end;


constructor TImportaSubCuentas.Create;
begin
     FDatasetImportar := TClientDataset.Create( nil );
end;

destructor TImportaSubCuentas.Destroy;
begin
     FreeAndNil( FDatasetImportar );
end;

function TImportaSubCuentas.GetSubCuentaAscii: Boolean;
 var
    iPos: integer;
begin
     iPos := Pos(',', FRenglon);
     Result := ( iPos > 0 );
     if Result then
     begin

          FCodigo := Copy( FRenglon, 1, iPos - 1 );
          FSubCuenta := Copy( FRenglon, iPos + 1, Length(FRenglon) );
     end;
end;

procedure TImportaSubCuentas.EditaDataSet;
begin
     with FDataSetCatalogo do
     begin
          DisableControls;
          try
             if Locate( FCampoCodigo, VarArrayOf([FCodigo]), [] ) then
             begin
                  Edit;
                  FieldByName(FCampoSubCuenta).AsString := FSubCuenta;
                  Post;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TImportaSubCuentas.ImportaAscii( const sArchivo: string );
 var
    FArchivo : TStrings;
    i: integer;
begin
     if FileExists( sArchivo ) then
     begin
          FArchivo := TStringList.Create;
          try
             FArchivo.LoadFromFile( sArchivo );
             for i:= 0 to FArchivo.Count - 1 do
             begin
                  FRenglon:= FArchivo[i];
                  if GetSubCuentaAscii then
                  begin
                       EditaDataset;
                  end;
             end;
          finally
                 if ( FDataSetCatalogo.ChangeCount > 0 ) then
                    FDataSetCatalogo.Enviar;
                 ZetaDialogo.ZInformation( '', Format( 'El archivo %s fu� importado.', [sArchivo]), 0 );

                 FreeAndNil( FArchivo );
          end;
     end
     else
     begin
          ZetaDialogo.ZError( 'Error', Format('El Archivo %s No Existe', [sArchivo]), 0 )
     end;
end;

procedure TImportaSubCuentas.ImportaDataset( const sArchivo: string );
begin
     if FileExists( sArchivo ) then
     begin
          FDatasetImportar.LoadFromFile(sArchivo);
          with FDatasetImportar do
          begin
               DisableControls;
               try
                  while NOT EOF do
                  begin
                       FCodigo := Fields[0].AsString;
                       FSubCuenta := FieldByName('SUBCUENTA').AsString;

                       EditaDataset;
                       Next;
                  end;

                  if ( FDataSetCatalogo.ChangeCount > 0 ) then
                       FDataSetCatalogo.Enviar;

                  ZetaDialogo.ZInformation( '', Format( 'El archivo %s fu� importado.', [sArchivo]), 0 );
               finally
                      EnableControls;
               end;
          end;
     end
     else
     begin
          ZetaDialogo.ZError( 'Error', Format('El archivo %s no existe', [sArchivo]), 0 )
     end;
end;



end.
