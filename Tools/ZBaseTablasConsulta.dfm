inherited TablaOpcion: TTablaOpcion
  Left = 202
  Top = 175
  Caption = 'TablaOpcion'
  ClientHeight = 286
  ClientWidth = 554
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid: TZetaDBGrid [0]
    Left = 0
    Top = 19
    Width = 554
    Height = 267
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'TB_CODIGO'
        Title.Caption = 'C�digo'
        Width = 61
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TB_ELEMENT'
        Title.Caption = 'Descripci�n'
        Width = 205
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TB_INGLES'
        Title.Caption = 'Ingl�s'
        Width = 210
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TB_NUMERO'
        Title.Alignment = taRightJustify
        Title.Caption = 'N�mero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TB_TEXTO'
        Title.Caption = 'Texto'
        Width = 189
        Visible = True
      end>
  end
  inherited PanelIdentifica: TPanel
    Width = 554
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 228
    end
  end
  inherited DataSource: TDataSource
    Left = 32
    Top = 96
  end
end
