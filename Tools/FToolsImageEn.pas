unit FToolsImageEn;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Db, ExtCtrls, ExtDlgs, imageenview, ieview, hyiedefs, ZetaCommonClasses;

function ResizeImagen( oImageEnView: TImageEnView; var sError: String ): Boolean;
procedure SetDriver( oImageEnView: TImageEnView; const idx: Integer );
procedure AsignaBlobAImagen( oImageEnView: TImageEnView; oDataSet: TDataSet; const sField: String );
procedure AsignaImagenABlob( oImageEnView: TImageEnView; oDataSet: TDataSet; const sField: String );

const
     IMAGE_SIZE_OK = 300;

implementation

procedure SetDriver( oImageEnView: TImageEnView; const idx: Integer );
const
     W_VGA = 640;
     H_VGA = 480;
begin
     with oImageEnView.IO.DShowParams do
     begin
          SetVideoInput( idx, 0, W_VGA, H_VGA );
          RenderAudio := FALSE;
          RenderVideo := TRUE;
     end;
end;

function ResizeImagen( oImageEnView: TImageEnView; var sError: String ): Boolean;
begin
     Result := TRUE;
     if ( oImageEnView.IO.Params.Width > IMAGE_SIZE_OK ) then
     begin
          try
             oImageEnView.Proc.Resample( IMAGE_SIZE_OK, IMAGE_SIZE_OK, rfLanczos3, TRUE );
          except
                on Error: Exception do
                begin
                     Result := FALSE;
                     sError := 'No se pudo ajustar el tama�o de la fotograf�a. Se guardar� con su tama�o original' + CR_LF + Error.Message;
                end;
          end;
     end;
end;

procedure AsignaBlobAImagen( oImageEnView: TImageEnView; oDataSet: TDataSet; const sField: String );
var
   oImagenStream: TStream;
begin
     try
        with oDataSet do
        begin
            oImagenStream := CreateBlobStream( FieldByName( sField ), bmRead );
            if oDataSet.FieldByName( sField ).AsString = VACIO then
                oImageEnView.Blank
            else
                oImageEnView.IO.LoadFromStream( oImagenStream );
        end;

     finally
            FreeAndNil( oImagenStream );
     end;
end;


procedure AsignaImagenABlob( oImageEnView: TImageEnView; oDataSet: TDataSet; const sField: String );
var
   oImagenStream: TStream;
begin
     with oDataSet do
     begin
          if not ( state in [ dsEdit, dsInsert ] ) then
             Edit;
          oImagenStream := CreateBlobStream( FieldByName( sField ), bmReadWrite );
     end;
     try
        with oImageEnView.IO do
        begin
             StreamHeaders:= FALSE;
             Params.JPEG_Quality := 75;
             SaveToStreamJPEG( oImagenStream );
        end;
     finally
            FreeAndNil( oImagenStream );
     end;
end;

end.
