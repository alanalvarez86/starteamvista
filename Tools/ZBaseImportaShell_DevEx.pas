unit ZBaseImportaShell_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseShell, ExtCtrls, ComCtrls, StdCtrls, Buttons, {Psock, NMsmtp,} 
  ImgList, Menus, ActnList, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, TressMorado2013, dxStatusBar, 
  dxRibbonStatusBar, cxButtons, dxSkinscxPCPainter, cxPCdxBarPopupMenu,
  cxPC, cxClasses, dxSkinsForm, cxContainer, cxEdit, cxTextEdit, cxMemo,
  dxSkinsdxBarPainter, dxBar, cxGroupBox, dxRibbonSkins,IdSMTP, DEmailService,
  dxSkinsdxRibbonPainter, cxStyles, dxRibbon, dxRibbonCustomizationForm,
  dxBarBuiltInMenu, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase, System.Actions;

type
  TBaseImportaShell_DevEx = class(TBaseShell)
    ActionList: TActionList;
    _A_OtraEmpresa: TAction;
    _A_Servidor: TAction;
    _A_SalirSistema: TAction;
    _P_Procesar: TAction;
    _P_Cancelar: TAction;
    _H_AcercaDe: TAction;
    OpenDialog: TOpenDialog;
    Email: TIdSMTP;
    PageControl: TcxPageControl;
    TabBitacora: TcxTabSheet;
    MemBitacora: TcxMemo;
    PanelBottom: TPanel;
    BitacoraSeek: TcxButton;
    LblGuardarBIT: TLabel;
    ArchBitacora: TEdit;
    TabErrores: TcxTabSheet;
    Panel1: TPanel;
    ErroresSeek: TcxButton;
    LblGuardarERR: TLabel;
    ArchErrores: TEdit;
    MemErrores: TcxMemo;
    PanelBotones: TPanel;
    BtnProcesar: TcxButton;
    BtnDetener: TcxButton;
    BtnSalir: TcxButton;
    PanelEncabezado: TPanel;
    SistemaFechaDia: TLabel;
    _Per_GenerarPoliza: TAction;
    DevEx_dxBarManager: TdxBarManager;
    dxBarManagerBar1: TdxBar;
    Archivo_DevEx: TdxBarSubItem;
    Ayuda_DevEx: TdxBarSubItem;
    ImageList1: TImageList;
    TabArchivo_btnServidor: TdxBarLargeButton;
    TabArchivo_btnProcesar: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    TabArchivo_btnCancelarProceso: TdxBarLargeButton;
    TabArchivo_btnSalir: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    TabAyuda_BtnAcercade: TdxBarLargeButton;
    cxGroupBox1: TcxGroupBox;
    LblArchivo: TLabel;
    ArchOrigen: TEdit;
    ArchivoSeek: TcxButton;
    ArbolImages: TImageList;
    TabArchivo_btnOtraEmpresa: TdxBarLargeButton;
    procedure _A_OtraEmpresaExecute(Sender: TObject);
    procedure _A_ServidorExecute(Sender: TObject);
    procedure _A_SalirSistemaExecute(Sender: TObject);
    procedure _P_ProcesarExecute(Sender: TObject);
    procedure _P_CancelarExecute(Sender: TObject);
    procedure _H_AcercaDeExecute(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure BitacoraSeekClick(Sender: TObject);
    procedure ErroresSeekClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure _Per_GenerarPolizaExecute(Sender: TObject);
  private
    { Private declarations }
    FModoBatch: Boolean;
    FEnviarCorreo: boolean;
    FProcesa: Boolean;
    FBatchLog: TStrings;
    FMinParamsBatch : Integer;
    FSintaxisBatch  : String;
    FLogNameBatch   : String;
{$ifdef INTERFAZ_GREATBATCH}
    FUsuarioInterfaz: string;
    FClaveInterfaz: string;
{$ifdef XEROX_INTERFAZ}
    FEmailInterfaz: string;
{$endif}
    FTodosLosCorreos: string;
{$endif}
    function LoginBatch: Boolean;
    function AbreEmpresaBatch: Boolean;
    procedure ManejaExcepcionLog(Sender: TObject; Error: Exception);
    {$ifndef REGAL_INTERFAZ_ORACLE}
    procedure EnviarCorreoBitacoras;
    procedure AgregaBitacoraBatch(const sMensaje: String);
    {$endif}
    procedure GrabaBitacora(oMemo: TcxMemo; const sArchivo: String);

  protected
    procedure InitBitacora;
    procedure EscribeBitacoras; virtual;
    procedure ActivaControles( const lProcesando: Boolean ); virtual;
    procedure SetControlesBatch; virtual;
    procedure ProcesarArchivo; virtual;
    procedure CancelarProceso; virtual;
    procedure DoProcesar; virtual; abstract;
    procedure ReportaErrorDialogo(const sTitulo, sMensaje: String);
    {$ifdef REGAL_INTERFAZ_ORACLE}
    procedure EnviarCorreoBitacoras; virtual;
    {$endif}
  public
    { Public declarations }
    property MinParamsBatch : Integer read FMinParamsBatch write FMinParamsBatch;
    property SintaxisBatch : String read FSintaxisBatch write FSintaxisBatch;
    property LogNameBatch : String read FLogNameBatch write FLogNameBatch;
    property ModoBatch : Boolean read FModoBatch Write FModoBatch;
    property EnviarCorreo : Boolean write FEnviarCorreo;
{$ifdef USUARIO_INTERFAZ}
    property UsuarioInterfaz: string write FUsuarioInterfaz;
    property ClaveInterfaz: string write FClaveInterfaz;
{$ifdef XEROX_INTERFAZ}
    property EmailInterfaz: string write FEmailInterfaz;
    property BatchLog: TStrings read FBatchLog write FBatchLog;
{$endif}
    property TodosLosCorreos: string write FTodosLosCorreos;
{$endif}
{$ifdef INTERFAZ_ELECTROLUX_KRONOS}
    property BatchLog: TStrings read FBatchLog write FBatchLog;
{$endif}
{$ifdef INTERFAZ_NYPRO}
    property Procesa: Boolean read FProcesa write FProcesa;
{$endif}
    procedure EscribeBitacora( const sMensaje: String );
    procedure EscribeError( const sMensaje: String );
    procedure ProcesarBatch; virtual;
    {$ifdef REGAL_INTERFAZ_ORACLE}
    procedure AgregaBitacoraBatch(const sMensaje: String);
    {$endif}
  end;

const
     K_PARAM_EMPRESA  = 1;
     K_PARAM_USUARIO  = 2;
     K_PARAM_PASSWORD = 3;
     K_PARAM_ARCHIVO  = 4;
     K_BATCH_LOG = 'Interfase.LOG';
     K_BATCH_SINTAXIS = '%s EMPRESA USUARIO PASSWORD ARCHIVO';

var
  BaseImportaShell_DevEx: TBaseImportaShell_DevEx;

implementation

uses dGlobal, dCliente, dDiccionario,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaCommonLists,
     ZetaMessages,
     ZGlobalTress,
     ZAsciiTools,
     ZetaDialogo,
{$ifdef INTERFAZ_ELECTROLUX_KRONOS}
    DInterfase,
{$endif}
     ZetaAcercaDe_DevEx;

{$R *.DFM}

procedure TBaseImportaShell_DevEx.FormCreate(Sender: TObject);
begin
     FEnviarCorreo := TRUE;
     FMinParamsBatch := K_PARAM_ARCHIVO;
     FSintaxisBatch := Format( K_BATCH_SINTAXIS, [ ExtractFileName( Application.ExeName ) ] );
     FLogNameBatch  := K_BATCH_LOG;

     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( Self );
     dmDiccionario := TdmDiccionario.Create( Self );
     dmEmailService := TdmEmailService.Create(Self);
     FBatchLog := TStringList.Create;
     inherited;
end;

procedure TBaseImportaShell_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     FBatchLog.Free;
     dmCliente.Free;
     Global.Free;
     dmEmailService.Free;
end;

procedure TBaseImportaShell_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := TabBitacora;
     ActivaControles( FALSE );
end;

procedure TBaseImportaShell_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     if FModoBatch then
        ZAsciiTools.GrabaBitacora( FBatchLog, ZetaMessages.SetFileNameDefaultPath( FLogNameBatch ) );
end;

procedure TBaseImportaShell_DevEx.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     CanClose := TRUE;
end;

procedure TBaseImportaShell_DevEx.ManejaExcepcionLog(Sender: TObject; Error: Exception);
begin
     AgregaBitacoraBatch( Error.Message );
end;

{ Procesar Batch }

procedure TBaseImportaShell_DevEx.ProcesarBatch;
{$IFDEF INTERFAZ_ELECTROLUX_KRONOS}
const K_PARAM_NOMINA  = 4;
{$ENDIF}
var
   sNombreEmpresa: String;
{$IFDEF INTERFAZ_ELECTROLUX_KRONOS}
        iYear, iPeriodo, iEstatus, iTimbro: Integer;
{$ENDIF}
begin
     Application.OnException := ManejaExcepcionLog;
     FModoBatch := TRUE;
     try
        if ( ParamCount >= FMinParamsBatch ) then   // Deben incluirse todos los parametros
        begin
             if LoginBatch and AbreEmpresaBatch then
             begin
                  SetControlesBatch;
                  sNombreEmpresa := dmCliente.GetDatosEmpresaActiva.Nombre;
                  AgregaBitacoraBatch( Format( 'Inici� Proceso = Empresa: %s - Archivo: %s - Fecha Default: %s', [ sNombreEmpresa, ArchOrigen.Text, FechaCorta( dmCliente.FechaDefault ) ] ) );

                  {$IFDEF INTERFAZ_ELECTROLUX_KRONOS}
                  // Asignaci�n de Tipo de Periodo en modo Batch
                  if StrToInt( ParamStr( K_PARAM_NOMINA )) = 1 then
                     dmCliente.PeriodoTipo := ord( tpSemanal )
                  else if StrToInt( ParamStr( K_PARAM_NOMINA )) = 2 then
                      dmCliente.PeriodoTipo := ord( tpCatorcenal );

                  dmInterfase.ObtenPeriodoMovimiento(dmCliente.FechaDefault, ord (dmCliente.PeriodoTipo), dmCliente.YearDefault, iPeriodo, iEstatus, iTimbro);

                  if (dmInterfase.ImportarBonosPuntualidadPerfecta  or dmInterfase.ImportarBonosAsistenciaPerfecta or dmInterfase.ImportarBonosPuntualidadPerfectaMensual ) then
                  begin
                       dmCliente.SetPeriodoNumero( iPeriodo );
                       with dmCliente.GetDatosPeriodoActivo do
                       begin
                            dmInterfase.InicioPeriodo := Inicio;
                            dmInterfase.FinPeriodo := Fin;
                       end;
                       ProcesarArchivo;
                  end
                  else
                  begin
                        if ( ( dmCliente.PeriodoTipo = ord( tpSemanal   ) )
                          or ( dmCliente.PeriodoTipo = ord( tpCatorcenal) ) ) then
                        begin
                             if iEstatus <> ord (spAfectadaTotal) then
                             begin
                                  dmCliente.SetPeriodoNumero( iPeriodo );
                                  with dmCliente.GetDatosPeriodoActivo do
                                  begin
                                       dmInterfase.InicioPeriodo := Inicio;
                                       dmInterfase.FinPeriodo := Fin;
                                  end;
                                  ProcesarArchivo;
                             end
                             else
                             begin
                                  // EscribeError(Format ('No es posible ejecutar el proceso para la n�mina %d, su estatus es Afectada Total.', [iPeriodo]));
                                  AgregaBitacoraBatch (Format ('No es posible ejecutar el proceso para la n�mina %d, su estatus es Afectada Total.', [iPeriodo]));
                             end;
                        end
                        else
                        begin
                             AgregaBitacoraBatch ('No es posible ejecutar el proceso, solo se permiten tipo de n�mina Semanal (1) o Catorcenal (2).');
                        end;
                  end;
                  {$ELSE}
                  ProcesarArchivo;
                  {$ENDIF}

                  // ProcesarArchivo;
                  if FEnviarCorreo then
                     EnviarCorreoBitacoras;
                  AgregaBitacoraBatch( Format( 'Termin� Proceso = Empresa: %s - Bit�cora de Cambios: %s - Errores: %s', [ sNombreEmpresa, ArchBitacora.Text, ArchErrores.Text ] ) );
             end;
        end
        else
            AgregaBitacoraBatch( Format( '� Faltan Par�metros ! - Sintaxis: %s', [ FSintaxisBatch ] ) );
     except
        on Error: Exception do
        begin
             Application.HandleException( Error );
        end;
     end;
     Close;         // Salir del Programa
end;

function TBaseImportaShell_DevEx.LoginBatch: Boolean;
var
   eReply: eLogReply;
begin
     Result := FALSE;
     with dmCliente do
     begin
          {$ifdef INTERFAZ_GREATBATCH}
          eReply := GetUsuario( FUsuarioInterfaz, FClaveInterfaz, TRUE );
          {$else}
          eReply := GetUsuario( ParamStr( K_PARAM_USUARIO ), ParamStr( K_PARAM_PASSWORD ), TRUE );
          {$endif}
          case eReply of
               lrOK: Result := TRUE;
               {$ifndef USUARIO_INTERFAZ}
               lrLoggedIn: AgregaBitacoraBatch( '� Usuario Ya Entr� Al Sistema !' + ' - Ya Hay Otro Usuario Con Esta Clave En El Sistema' );
               lrLockedOut: AgregaBitacoraBatch( '� Usuario Bloqueado !' + ' - El Administrador Ha Bloqueado Su Acceso Al Sistema' );
               lrChangePassword: AgregaBitacoraBatch( '� Debe Cambiar Clave de Usuario !' + ' - El Administrador Ha forzado que Cambie su Password' );
               lrExpiredPassword: AgregaBitacoraBatch( '� Clave de Usuario Vencida !' + ' - Su Clave de Acceso Anterior Ha Expirado' );
               lrInactiveBlock: AgregaBitacoraBatch( '� Usuario Bloqueado !' + ' - Usuario Ha Sido Bloqueado Por No Entrar Al Sistema Durante Mucho Tiempo' );
               lrNotFound: AgregaBitacoraBatch( '� No se Encontr� el Usuario !' + ' - Verifique que sea Correcto el Nombre del Usuario' );
               lrAccessDenied: AgregaBitacoraBatch( '� Password Incorrecto de Usuario !' + ' - Verifique el Password del Usuario' );
               lrSystemBlock: AgregaBitacoraBatch( '� Sistema Bloqueado !' +  ' - El Administrador Ha Bloqueado el Acceso Al Sistema' );
               {$else}
               lrLoggedIn: Result := TRUE;
               lrLockedOut: Result := TRUE;
               lrChangePassword: Result := TRUE;
               lrExpiredPassword: Result := TRUE;
               lrInactiveBlock: Result := TRUE;
               lrNotFound: Result := TRUE;
               lrAccessDenied: Result := TRUE;
               lrSystemBlock: Result := TRUE;
               {$endif}
          else
               AgregaBitacoraBatch( '� Error General al Verificar Usuario !' + ' - Intentar con otro Usuario' );
          end;
     end;
end;

function TBaseImportaShell_DevEx.AbreEmpresaBatch: Boolean;
var
   iCompanys: Integer;
   sEmpresa : String;
begin
     Result := FALSE;
     sEmpresa := ParamStr( K_PARAM_EMPRESA );
     // Localiza Empresa
     iCompanys := dmCliente.InitCompany;
     if ( iCompanys = 0 ) then
        AgregaBitacoraBatch( '� No Hay Compa��as !' + ' - No Hay Empresas En El Sistema' )
     else
     begin
          Result := dmCliente.FindCompany( sEmpresa );
          if not Result then
             AgregaBitacoraBatch( '� No se Encontr� Empresa !' + ' - Verifique el C�digo: ' + sEmpresa );
     end;
     // Abre Empresa
     if Result then
     begin
          Application.ProcessMessages;
          CierraTodo;
          with dmCliente do
          begin
               SetCompany;
               SetCacheLookup;
          end;
          DoOpenAll;
     end;
end;

procedure TBaseImportaShell_DevEx.SetControlesBatch;
begin
     ArchOrigen.Text := ParamStr( K_PARAM_ARCHIVO );
end;

procedure TBaseImportaShell_DevEx.EnviarCorreoBitacoras;
const
     K_EMAIL_TIMEOUT = 5000;
var
   sHost: String;
   Puerto:Integer;
   //dmEmailService : TdmEmailService;
   sMensajeError : string;

   procedure AgregaFileBitacora( const sArchivo: String );
   begin
        if strLleno( sArchivo ) and FileExists( sArchivo ) then
           dmEmailService.Attachments.Add( sArchivo );
   end;

begin
     sHost := Global.GetGlobalString( K_GLOBAL_EMAIL_HOST );
     if strVacio( sHost ) then
        AgregaBitacoraBatch( '� No se Pudo Enviar Email ! - No est� Configurado el Servidor de Correos en Globales de Empresa' )
     else
     begin
          with dmEmailService do
          begin
               NewEmail;
               NewBodyMessage;
               MailServer := sHost;
               Puerto := Global.GetGlobalInteger( K_GLOBAL_EMAIL_PORT );
               if Puerto > 0 then
                  Port := Puerto;
               User := StrDef( Global.GetGlobalString( K_GLOBAL_EMAIL_USERID ), Application.Title );
               Password := Global.GetGlobalString( K_GLOBAL_EMAIL_PSWD );
               TimeOut := K_EMAIL_TIMEOUT;
               AuthMethod   := eAuthTressEmail( Global.GetGlobalInteger( K_GLOBAL_EMAIL_AUTH ) -1 );
               FromAddress := StrDef( Global.GetGlobalString( K_GLOBAL_EMAIL_FROMADRESS ), 'Interfase@dominio.com' );
               FromName := StrDef( Global.GetGlobalString( K_GLOBAL_EMAIL_FROMNAME ), 'Interfase - Sistema TRESS' );
               Subject := Application.Title + ' - ' + dmCliente.GetDatosEmpresaActiva.Nombre;
               ToAddress.Add( dmCliente.cdsUsuario.FieldByName( 'US_EMAIL' ).AsString );
               MessageText.Add( 'Bit�coras de Proceso: ' + Application.Title + ' - ' + dmCliente.GetDatosEmpresaActiva.Nombre );
               AgregaFileBitacora( ArchBitacora.Text );
               AgregaFileBitacora( ArchErrores.Text );
               try
                  if not dmEmailService.SendEmail( sMensajeError ) then
                     AgregaBitacoraBatch( Format( 'El servidor %s de E-mail no se pudo conectar o no fue posible enviar el correo ( Detalle: %s ) ', [ MailServer , sMensajeError] ));

               except
                  on Error: Exception do
                  begin
                        AgregaBitacoraBatch( '� No se Pudo Enviar Email ! - Error al Enviar Correo - ' + Error.Message );
                  end;
               end;
          end;
     end
end;

{ Procesos Generales del Shell }

procedure TBaseImportaShell_DevEx.ProcesarArchivo;
begin
     if strLleno( ArchOrigen.Text ) then
     begin
          if FileExists( ArchOrigen.Text ) then
          begin
               ActivaControles( TRUE );
               try
                  InitBitacora;
                  DoProcesar;
               finally
                  EscribeBitacoras;
                  ActivaControles( FALSE );
               end;
          end
          else
          begin
               ReportaErrorDialogo( self.Caption, 'No se Encontr� el Archivo de Origen Especificado' );
               ActiveControl := ArchOrigen;
          end;
     end
     else
     begin
          ReportaErrorDialogo( self.Caption, 'Falta Especificar Archivo de Origen' );
          ActiveControl := ArchOrigen;
     end;
end;

procedure TBaseImportaShell_DevEx.CancelarProceso;
begin
     // No hace nada en la base
end;

procedure TBaseImportaShell_DevEx.InitBitacora;
begin
     MemBitacora.Clear;
     MemErrores.Clear;
     Application.ProcessMessages;
end;

procedure TBaseImportaShell_DevEx.EscribeBitacoras;
begin
     self.GrabaBitacora( MemBitacora, ArchBitacora.Text );
     self.GrabaBitacora( MemErrores, ArchErrores.Text );
end;

procedure TBaseImportaShell_DevEx.ActivaControles( const lProcesando: Boolean );
begin
     with dmCliente do
     begin
          LblArchivo.Enabled := EmpresaAbierta and ( not lProcesando );
          ArchOrigen.Enabled := EmpresaAbierta and ( not lProcesando );
          ArchivoSeek.Enabled := EmpresaAbierta and ( not lProcesando );
          _P_Procesar.Enabled := EmpresaAbierta and ( not lProcesando );
          _A_SalirSistema.Enabled := ( not lProcesando );
          _P_Cancelar.Enabled := EmpresaAbierta and lProcesando;
     end;
end;

procedure TBaseImportaShell_DevEx.AgregaBitacoraBatch(const sMensaje: String);
begin
     FBatchLog.Add( FormatDateTime( 'dd/mm/yyyy-hh:mm:ss', now ) + ': ' + sMensaje );
end;

procedure TBaseImportaShell_DevEx.ReportaErrorDialogo( const sTitulo, sMensaje: String );
begin
     if FModoBatch then
        AgregaBitacoraBatch( strTransAll( sMensaje, CR_LF, ZetaCommonClasses.VACIO ) )
     else
        ZetaDialogo.ZError( sTitulo, sMensaje, 0 );
end;

procedure TBaseImportaShell_DevEx.GrabaBitacora(oMemo: TcxMemo; const sArchivo: String);
begin
     try
        oMemo.Lines.SaveToFile( sArchivo );
     except
        on Error : Exception do
        begin
             ReportaErrorDialogo( self.Caption, 'Error al Grabar ' + oMemo.Hint + ' al Archivo ' + CR_LF + sArchivo );
        end;
     end;
end;

procedure TBaseImportaShell_DevEx.EscribeBitacora(const sMensaje: String);
begin
     MemBitacora.Lines.Add( sMensaje );
end;

procedure TBaseImportaShell_DevEx.EscribeError(const sMensaje: String);
begin
     MemErrores.Lines.Add( sMensaje );
end;

{ Action List }

procedure TBaseImportaShell_DevEx._A_OtraEmpresaExecute(Sender: TObject);
begin
     AbreEmpresa( False );
end;

procedure TBaseImportaShell_DevEx._A_ServidorExecute(Sender: TObject);
begin
     CambiaServidor;
end;

procedure TBaseImportaShell_DevEx._A_SalirSistemaExecute(Sender: TObject);
begin
     Close;
end;

procedure TBaseImportaShell_DevEx._P_ProcesarExecute(Sender: TObject);
begin
     inherited;
     {$ifdef INTERFAZ_NYPRO}
     if ( FProcesa ) then
     begin
          ProcesarArchivo;
     end
     else
         ZetaDialogo.ZError( 'Error al conectar', 'Empresa no autorizada para este proceso.', 0 );
     {$else}
     ProcesarArchivo;
     {$endif}
end;

procedure TBaseImportaShell_DevEx._P_CancelarExecute(Sender: TObject);
begin
     inherited;
     CancelarProceso;
end;

procedure TBaseImportaShell_DevEx._H_AcercaDeExecute(Sender: TObject);
begin
     try
        ZAcercaDe_DevEx := TZAcercaDe_DevEx.Create( Self );
        ZAcercaDe_DevEx.ShowModal;
     finally
            ZAcercaDe_DevEx.Free;
     end;
end;

{ Eventos del Shell }

procedure TBaseImportaShell_DevEx.ArchivoSeekClick(Sender: TObject);
begin
     ArchOrigen.Text := ZetaClientTools.AbreDialogo( OpenDialog, ArchOrigen.Text, 'Dat' );
end;

procedure TBaseImportaShell_DevEx.BitacoraSeekClick(Sender: TObject);
begin
     ArchBitacora.Text := ZetaClientTools.AbreDialogo( OpenDialog, ArchBitacora.Text, 'Log' );
end;

procedure TBaseImportaShell_DevEx.ErroresSeekClick(Sender: TObject);
begin
     ArchErrores.Text := ZetaClientTools.AbreDialogo( OpenDialog, ArchErrores.Text, 'Log' );
end;

procedure TBaseImportaShell_DevEx._Per_GenerarPolizaExecute(Sender: TObject);
begin
     inherited;
     { No se hace nada } 
end;

end.
