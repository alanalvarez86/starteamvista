inherited EscogeConcepto_DevEx: TEscogeConcepto_DevEx
  Caption = 'Escoge Concepto'
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Top = 227
    Height = 33
  end
  inherited Panel2: TPanel
    Visible = False
  end
  inherited Grid_DevEx: TZetaCXGrid
    Height = 186
    inherited Grid_DevExDBTableView: TcxGridDBTableView
      object co_numero: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'co_numero'
        MinWidth = 70
      end
      object co_descrip: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'co_descrip'
        MinWidth = 400
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 408
  end
end
