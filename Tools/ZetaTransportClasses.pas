unit ZetaTransportClasses;

interface

uses SysUtils, Variants, Classes, DB,
     {$ifndef DOTNET}Controls,{$endif}
     ZetaCommonLists,
     ZetaCommonClasses;

type
  TMoney = Extended;
  TTexto = {$ifdef DOTNET}&String{$else}String{$endif};
  TPrimaryKey = TTexto;
  TDatosBlob = TTexto;
  TDataTransport = class;
  TIntegerKey = class( TObject )
  private
    { Private declarations }
    FValue: Integer;
    FOldValue: Integer;
  public
    { Public declarations }
    property Value: Integer read FValue write FValue;
    property OldValue: Integer read FOldValue write FOldValue;
    procedure CargaParametros( Parametros: TDataTransport; const sTag: String );
    procedure DescargaParametros( Parametros: TDataTransport; const sTag: String );
  end;
  TFaltaDiasKey = class( TObject )
  private
    { Private declarations }
    FValue: eMotivoFaltaDias;
    FOldValue: eMotivoFaltaDias;
  public
    { Public declarations }
    property Value: eMotivoFaltaDias read FValue write FValue;
    property OldValue: eMotivoFaltaDias read FOldValue write FOldValue;
    procedure CargaParametros( Parametros: TDataTransport; const sTag: String );
    procedure DescargaParametros( Parametros: TDataTransport; const sTag: String );
  end;
  TFaltaHorasKey = class( TObject )
  private
    { Private declarations }
    FValue: eMotivoFaltaHoras;
    FOldValue: eMotivoFaltaHoras;
  public
    { Public declarations }
    property Value: eMotivoFaltaHoras read FValue write FValue;
    property OldValue: eMotivoFaltaHoras read FOldValue write FOldValue;
    procedure CargaParametros( Parametros: TDataTransport; const sTag: String );
    procedure DescargaParametros( Parametros: TDataTransport; const sTag: String );
  end;
  TEmpleado = TNumEmp;
  TDateKey = class( TObject )
  private
    { Private declarations }
    FValue: TDate;
    FOldValue: TDate;
  public
    { Public declarations }
    property Value: TDate read FValue write FValue;
    property OldValue: TDate read FOldValue write FOldValue;
    procedure CargaParametros( Parametros: TDataTransport; const sTag: String );
    procedure DescargaParametros( Parametros: TDataTransport; const sTag: String );
  end;
  TStringKey = class( TObject )
  private
    { Private declarations }
    FValue: TTexto;
    FOldValue: TTexto;
  public
    { Public declarations }
    property Value: TTexto read FValue write FValue;
    property OldValue: TTexto read FOldValue write FOldValue;
    procedure CargaParametros( Parametros: TDataTransport; const sTag: String );
    procedure DescargaParametros( Parametros: TDataTransport; const sTag: String );
  end;
  TIntervalo = class( TObject )
  private
    { Private declarations }
    FDias: Integer;
    FHoras: Integer;
    FMinutos: Integer;
    FSegundos: Integer;
    procedure SetLapso(const Value: TDateTime);
  public
    { Public declarations }
    property Lapso: TDateTime write SetLapso;
    property Dias: Integer read FDias;
    property Horas: Integer read FHoras;
    property Minutos: Integer read FMinutos;
    property Segundos: Integer read FSegundos;
    procedure CargaParametros( Parametros: TDataTransport );
    procedure DescargaParametros( Parametros: TDataTransport );
  end;
  TProcessResult = class( TObject )
  private
    { Private declarations }
    FStatus: eProcessStatus;
    FFolio: Integer;
    FMaxStep: Integer;
    FErrores: Integer;
    FEventos: Integer;
    FAdvertencias: Integer;
    FProcessID: Procesos;
    FProcesados: Integer;
    FDuracion: TIntervalo;
    FUltimoEmpleado: TNumEmp;
    FInicio: TDateTime;
    FFinal: TDateTime;
    function GetAsTexto: TTexto;
    function GetAsVariant: OleVariant;
    function GetDateTimeAsText(const dValue: TDateTime): TTexto;
    function GetTextAsDateTime(const sValue: TTexto): TDateTime;
    procedure SetAsTexto(const Value: TTexto);
    procedure SetAsVariant(const Value: OleVariant);
  public
    { Public declarations }
    property Folio: Integer read FFolio;
    property Status: eProcessStatus read FStatus;
    property ProcessID: Procesos read FProcessID;
    property MaxStep: Integer read FMaxStep;
    property Procesados: Integer read FProcesados;
    property UltimoEmpleado: TNumEmp read FUltimoEmpleado;
    property Inicio: TDateTime read FInicio write FInicio;
    property Final: TDateTime read FFinal write FFinal;
    property Duracion: TIntervalo read FDuracion;
    property Errores: Integer read FErrores;
    property Advertencias: Integer read FAdvertencias;
    property Eventos: Integer read FEventos;
    property AsVariant: OleVariant read GetAsVariant write SetAsVariant;
    property AsText: TTexto read GetAsTexto write SetAsTexto;
    constructor Create;
    destructor Destroy; override;
    procedure CargaParametros( Parametros: TDataTransport );
    procedure DescargaParametros( Parametros: TDataTransport );
    {$ifndef DOTNET}
    procedure LoadException( Error: Exception );
    {$endif}
  end;
  TEmpresaActiva = class( TObject )
  private
    { Private declarations }
    FConfidencialidad: TTexto;
    FUserName: TTexto;
    FUserPassword: TTexto;
    FBaseDeDatos: TTexto;
    FCodigo: TTexto;
    FUsuario: Integer;
    FGrupo : Integer;
    function GetValue: Variant;
    procedure SetValue(const Value: Variant);
  public
    { Public declarations }
    property BaseDeDatos: String read FBaseDeDatos write FBaseDeDatos;
    property UserName: String read FUserName write FUserName;
    property UserPassword: String read FUserPassword write FUserPassword;
    property Usuario: Integer read FUsuario write FUsuario;
    property Confidencialidad: String read FConfidencialidad write FConfidencialidad;
    property Codigo: String read FCodigo write FCodigo;
    property Grupo : Integer read FGrupo write FGrupo ;
    property Value: Variant read GetValue write SetValue;
    procedure CargaParametros( Parametros: TDataTransport );
    procedure DescargaParametros( Parametros: TDataTransport );
  end;
  TDatosPeriodo = class( TObject )
  private
    { Private declarations }
    FDescripcion: TTexto;
    FDescuentaAhorros: Boolean;
    FSoloExcepciones: Boolean;
    FDescuentaPrestamos: Boolean;
    FIncluyeBajas: Boolean;
    FMes: Byte;
    FMesActivo: Byte;
    FStatus: eStatusPeriodo;
    FTipo: eTipoPeriodo;
    FUso: eUsoPeriodo;
    FDias: Integer;
    FUsuario: Integer;
    FDiasAcumula: Integer;
    FYear: Integer;
    FNumeroEmpleados: Integer;
    FPerMes: Integer;
    FPerAnio: Integer;
    FPosMes: Integer;
    FNumero: Integer;
    FFinAsis: TDate;
    FPago: TDate;
    FInicioAsis: TDate;
    FInicio: TDate;
    FFin: TDate;
  public
    { Public declarations }
    property Year: Integer read FYear write FYear;
    property Tipo: eTipoPeriodo read FTipo write FTipo;
    property Numero: Integer read FNumero write FNumero;
    property Inicio: TDate read FInicio write FInicio;
    property Fin: TDate read FFin write FFin;
    property Pago: TDate read FPago write FPago;
    property InicioAsis: TDate read FInicioAsis write FInicioAsis;
    property FinAsis: TDate read FFinAsis write FFinAsis;
    property Dias: Integer read FDias write FDias;
    property Status: eStatusPeriodo read FStatus write FStatus;
    property Uso: eUsoPeriodo read FUso write FUso;
    property Mes: Byte read FMes write FMes;
    property MesActivo: Byte read FMesActivo write FMesActivo;
    property SoloExcepciones: Boolean read FSoloExcepciones write FSoloExcepciones;
    property IncluyeBajas: Boolean read FIncluyeBajas write FIncluyeBajas;
    property PosMes: Integer read FPosMes write FPosMes;
    property PerMes: Integer read FPerMes write FPerMes;
    property DiasAcumula: Integer read FDiasAcumula write FDiasAcumula;
    property PerAnio: Integer read FPerAnio write FPerAnio;
    property Usuario: Integer read FUsuario write FUsuario;
    property DescuentaAhorros: Boolean read FDescuentaAhorros write FDescuentaAhorros;
    property DescuentaPrestamos: Boolean read FDescuentaPrestamos write FDescuentaPrestamos;
    property Descripcion: String read FDescripcion write FDescripcion;
    property NumeroEmpleados: Integer read FNumeroEmpleados write FNumeroEmpleados;
    procedure CargaZetaParams( Parametros: TZetaParams );
    procedure CargaParametros( Parametros: TDataTransport );
    procedure DescargaParametros( Parametros: TDataTransport );
  end;
  TDatosImss = class( TObject )
  private
    { Private declarations }
    FPatron: TTexto;
    FStatus: eStatusLiqIMSS;
    FTipo: eTipoLiqIMSS;
    FYear: Integer;
    FMes: Integer;
    FModifico: Integer;
  public
    { Public declarations }
    property Patron: String read FPatron write FPatron;
    property Mes: Integer read FMes write FMes;
    property Year: Integer read FYear write FYear;
    property Tipo: eTipoLiqIMSS read FTipo write FTipo;
    property Status: eStatusLiqIMSS read FStatus write FStatus;
    property Modifico: Integer read FModifico write FModifico;
    procedure CargaZetaParams( Parametros: TZetaParams );
    procedure CargaParametros( Parametros: TDataTransport );
    procedure DescargaParametros( Parametros: TDataTransport );
  end;
  TDatosActivos = class( TObject )
  private
    { Private declarations }
    FNombreUsuario: TTexto;
    FCodigoEmpresa: TTexto;
    FYearDefault: Integer;
    FEmpleadoActivo: Integer;
    FFechaAsistencia: TDate;
    FFechaDefault: TDate;
  public
    { Public declarations }
    constructor Create;
    property FechaAsistencia: TDate read FFechaAsistencia write FFechaAsistencia;
    property FechaDefault: TDate read FFechaDefault write FFechaDefault;
    property YearDefault: Integer read FYearDefault write FYearDefault;
    property EmpleadoActivo: Integer read FEmpleadoActivo write FEmpleadoActivo;
    property NombreUsuario: String read FNombreUsuario write FNombreUsuario;
    property CodigoEmpresa: String read FCodigoEmpresa write FCodigoEmpresa;
    procedure CargaZetaParams( Parametros: TZetaParams );
    procedure CargaParametros( Parametros: TDataTransport );
    procedure DescargaParametros( Parametros: TDataTransport );
  end;
  TProcessFilter = class( TObject )
  private
    { Private declarations }
    FCondicion: String;
    FFiltro: String;
    FRangoLista: String;
    FRangoCampo: String;
    FTipoRango: eTipoRangoActivo;
    FRangoInicio: String;
    FRangoFin: String;
    function GetRangeExpression: String;
  public
    { Public declarations }
    constructor Create;
    property RangoCampo: String read FRangoCampo write FRangoCampo;
    property Condicion: String read FCondicion write FCondicion;
    property Filtro: String read FFiltro write FFiltro;
    property RangoInicio: String read FRangoInicio write FRangoInicio;
    property RangoFin: String read FRangoFin write FRangoFin;
    property RangoLista: String read FRangoLista write FRangoLista;
    property TipoRango: eTipoRangoActivo read FTipoRango write FTipoRango;
    procedure CargaZetaParams( Parametros: TZetaParams );
    procedure CargaParametros( Parametros: TDataTransport );
    procedure DescargaParametros( Parametros: TDataTransport );
  end;
  TDataTransport = class( TZetaParams )
  private
    { Private declarations }
    FEmpresaActiva: TEmpresaActiva;
    FMensaje: TTexto;
    function GetTextValue: String;
    procedure SetTextValue( const Value: String );
  protected
    { Protected declarations }
    function GetVarValues: OleVariant; virtual;
    procedure SetVarValues( Value: OleVariant ); virtual;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property EmpresaActiva: TEmpresaActiva read FEmpresaActiva;
    property Mensaje: String read FMensaje write FMensaje;
    property TextValue: String read GetTextValue write SetTextValue;
    function ParamAsBlob( const sName: String ): TDatosBlob;
    function ParamAsBoolean( const sName: String ): Boolean;
    function ParamAsDate( const sName: String ): TDate;
    function ParamAsFloat( const sName: String ): Extended;
    function ParamAsInteger( const sName: String ): Integer;
    function ParamAsMemo( const sName: String ): String;
    function ParamAsString( const sName: String ): String;
    procedure CargaParametros( Lista: TDataTransport ); virtual;
    procedure DescargaParametros( Lista: TDataTransport ); virtual;
  end;
  TDatosEmpleado = class( TDataTransport )
  private
    { Private declarations }
    FEmpleado: TEmpleado;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Empleado: TEmpleado read FEmpleado write FEmpleado;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosExcepcion = class( TDatosEmpleado )
  private
    { Private declarations }
    FDatosPeriodo: TDatosPeriodo;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property DatosPeriodo: TDatosPeriodo read FDatosPeriodo;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosExcepcionMonto = class( TDatosExcepcion )
  private
    { Private declarations }
    FConcepto: TIntegerKey;
    FReferencia: TStringKey;
    FMonto: TMoney;
    FIndividual: Boolean;
    FActivo: Boolean;
    FEsPercepcion: Boolean;
    FISPTIndividual: TMoney;
    FParteExenta: TMoney;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Concepto: TIntegerKey read FConcepto;
    property Referencia: TStringKey read FReferencia;
    property Monto: TMoney read FMonto write FMonto;
    property EsPercepcion: Boolean read FEsPercepcion write FEsPercepcion;
    property Activo: Boolean read FActivo write FActivo;
    property ISPTIndividual: TMoney read FISPTIndividual write FISPTIndividual;
    property Individual: Boolean read FIndividual write FIndividual;
    property ParteExenta: TMoney read FParteExenta write FParteExenta;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosExcepcionDiaHora = class( TDatosExcepcion )
  private
    { Private declarations }
    FFecha: TDateKey;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Fecha: TDateKey read FFecha;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosExcepcionDia = class( TDatosExcepcionDiaHora )
  private
    { Private declarations }
    FTipo: TFaltaDiasKey;
    FDias: Extended;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Tipo: TFaltaDiasKey read FTipo;
    property Dias: Extended read FDias write FDias;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosExcepcionHora = class( TDatosExcepcionDiaHora )
  private
    { Private declarations }
    FTipo: TFaltaHorasKey;
    FHoras: Extended;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Tipo: TFaltaHorasKey read FTipo;
    property Horas: Extended read FHoras write FHoras;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosNominaClasificacion = class( TDatosExcepcion )
  private
    { Private declarations }
    FPuesto: TPrimaryKey;
    FTurno: TPrimaryKey;
    FClasificacion: TPrimaryKey;
    FObservaciones: String;
    FPagoPorFuera: Boolean;
    FPagadoEn: TDate;
    FRegistroPatronal: TPrimaryKey;
    FZonaGeografica: String;
    FNivel1: TPrimaryKey;
    FNivel2: TPrimaryKey;
    FNivel3: TPrimaryKey;
    FNivel4: TPrimaryKey;
    FNivel5: TPrimaryKey;
    FNivel6: TPrimaryKey;
    FNivel7: TPrimaryKey;
    FNivel8: TPrimaryKey;
    FNivel9: TPrimaryKey;
{$ifdef ACS}
    FNivel10: TPrimaryKey;
    FNivel11: TPrimaryKey;
    FNivel12: TPrimaryKey;
{$endif}
    FFolio1: Integer;
    FFolio2: Integer;
    FFolio3: Integer;
    FFolio4: Integer;
    FFolio5: Integer;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property Turno: TPrimaryKey read FTurno write FTurno;
    property Puesto: TPrimaryKey read FPuesto write FPuesto;
    property Clasificacion: TPrimaryKey read FClasificacion write FClasificacion;
    property Observaciones: String read FObservaciones write FObservaciones;
    property Nivel1: TPrimaryKey read FNivel1 write FNivel1;
    property Nivel2: TPrimaryKey read FNivel2 write FNivel2;
    property Nivel3: TPrimaryKey read FNivel3 write FNivel3;
    property Nivel4: TPrimaryKey read FNivel4 write FNivel4;
    property Nivel5: TPrimaryKey read FNivel5 write FNivel5;
    property Nivel6: TPrimaryKey read FNivel6 write FNivel6;
    property Nivel7: TPrimaryKey read FNivel7 write FNivel7;
    property Nivel8: TPrimaryKey read FNivel8 write FNivel8;
    property Nivel9: TPrimaryKey read FNivel9 write FNivel9;
{$ifdef ACS}
    property Nivel10: TPrimaryKey read FNivel10 write FNivel10;
    property Nivel11: TPrimaryKey read FNivel11 write FNivel11;
    property Nivel12: TPrimaryKey read FNivel12 write FNivel12;
{$endif}
    property RegistroPatronal: TPrimaryKey read FRegistroPatronal write FRegistroPatronal;
    property PagoPorFuera: Boolean read FPagoPorFuera write FPagoPorFuera;
    property PagadoEn: TDate read FPagadoEn write FPagadoEn;
    property ZonaGeografica: String read FZonaGeografica write FZonaGeografica;
    property Folio1: Integer read FFolio1 write FFolio1;
    property Folio2: Integer read FFolio2 write FFolio2;
    property Folio3: Integer read FFolio3 write FFolio3;
    property Folio4: Integer read FFolio4 write FFolio4;
    property Folio5: Integer read FFolio5 write FFolio5;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosEmpleadoInformacion = class( TDatosEmpleado )
  private
    { Private declarations }
    FNacionalidad: TTexto;
    FIMSSAfiliacion: TTexto;
    FNombres: TTexto;
    FApellidoPaterno: TTexto;
    FSexo: TTexto;
    FCURP: TTexto;
    FTransporte: TTexto;
    FNacimientoEntidad: TTexto;
    FNacimientoLugar: TTexto;
    FApellidoMaterno: TTexto;
    FEstadoCivil: TTexto;
    FRFC: TTexto;
    FDatosMatrimonio: TTexto;
    FTienePasaporte: Boolean;
    FNacimientoFecha: TDate;
    FResidenciaCiudadZona: TTexto;
    FResidenciaCalle: TTexto;
    FViveEn: TTexto;
    FIMSSClinica: TTexto;
    FResidenciaCodigoPostal: TTexto;
    FResidenciaCiudad: TTexto;
    FViveCon: TTexto;
    FResidenciaYears: TMoney;
    FResidenciaTelefono: TTexto;
    FResidenciaColoniaCodigo: TTexto;
    FResidenciaMonths: TMoney;
    FResidenciaEstado: TTexto;
    FResidenciaColoniaNombre: TTexto;
    FCarreraTerminada: TTexto;
    FCarreraHorario: TTexto;
    FMaquinasConocidas: TTexto;
    FExperiencia: TTexto;
    FIdiomaDominio: TTexto;
    FHablaOtroIdioma: Boolean;
    FEstudiaActualmente: Boolean;
    FGradoEstudios: TTexto;
    FINFONAVITCredito: TTexto;
    FINFONAVITMantenimiento: Boolean;
    FINFONAVITTipoPrestamo: eTipoInfonavit;
    FINFONAVITInicioCredito: TDate;
    FINFONAVITAmortizacion: TMoney;
    FINFONAVITTasaAnterior: TTasa;
    FConfidencialidad: TTexto;
    FBancaElectronica: TTexto;
    FAsistenciaNumeroTarjeta: TTexto;
    FAsistenciaLetraCredencial: TTexto;
    FAsistenciaCheca: Boolean;
    FEvaluacionFecha: TDate;
    FEvaluacionProxima: TDate;
    FEvaluacionResultado: TMoney;
    FExtraLogico1: Boolean;
    FExtraLogico2: Boolean;
    FExtraLogico3: Boolean;
    FExtraLogico4: Boolean;
    FExtraLogico5: Boolean;
    FExtraLogico6: Boolean;
    FExtraLogico7: Boolean;
    FExtraLogico8: Boolean;
    FExtraTabla1: TPrimaryKey;
    FExtraTabla2: TPrimaryKey;
    FExtraTabla3: TPrimaryKey;
    FExtraTabla4: TPrimaryKey;
    FExtraTabla5: TPrimaryKey;
    FExtraTabla6: TPrimaryKey;
    FExtraTabla7: TPrimaryKey;
    FExtraTabla8: TPrimaryKey;
    FExtraTabla9: TPrimaryKey;
    FExtraTabla10: TPrimaryKey;
    FExtraTabla11: TPrimaryKey;
    FExtraTabla12: TPrimaryKey;
    FExtraTabla13: TPrimaryKey;
    FExtraTabla14: TPrimaryKey;
    FExtraTexto1: String;
    FExtraTexto2: String;
    FExtraTexto3: String;
    FExtraTexto4: String;
    FExtraTexto5: String;
    FExtraTexto6: String;
    FExtraTexto7: String;
    FExtraTexto8: String;
    FExtraTexto9: String;
    FExtraTexto10: String;
    FExtraTexto11: String;
    FExtraTexto12: String;
    FExtraTexto13: String;
    FExtraTexto14: String;
    FExtraTexto15: String;
    FExtraTexto16: String;
    FExtraTexto17: String;
    FExtraTexto18: String;
    FExtraTexto19: String;
    FExtraFecha1: TDate;
    FExtraFecha2: TDate;
    FExtraFecha3: TDate;
    FExtraFecha4: TDate;
    FExtraFecha5: TDate;
    FExtraFecha6: TDate;
    FExtraFecha7: TDate;
    FExtraFecha8: TDate;
    FExtraNumero1: TMoney;
    FExtraNumero2: TMoney;
    FExtraNumero3: TMoney;
    FExtraNumero4: TMoney;
    FExtraNumero5: TMoney;
    FExtraNumero6: TMoney;
    FExtraNumero7: TMoney;
    FExtraNumero8: TMoney;
    FExtraNumero9: TMoney;
    FExtraNumero10: TMoney;
    FExtraNumero11: TMoney;
    FExtraNumero12: TMoney;
    FExtraNumero13: TMoney;
    FExtraNumero14: TMoney;
    FExtraNumero15: TMoney;
    FExtraNumero16: TMoney;
    FExtraNumero17: TMoney;
    FExtraNumero18: TMoney;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property ApellidoPaterno: String read FApellidoPaterno write FApellidoPaterno;
    property ApellidoMaterno: String read FApellidoMaterno write FApellidoMaterno;
    property Nombres: String read FNombres write FNombres;
    property Sexo: String read FSexo write FSexo;
    property NacimientoFecha: TDate read FNacimientoFecha write FNacimientoFecha;
    property NacimientoLugar: String read FNacimientoLugar write FNacimientoLugar;
    property NacimientoEntidad: String read FNacimientoEntidad write FNacimientoEntidad;
    property RFC: String read FRFC write FRFC;
    property IMSSAfiliacion: String read FIMSSAfiliacion write FIMSSAfiliacion;
    property IMSSClinica: String read FIMSSClinica write FIMSSClinica;
    property CURP: String read FCURP write FCURP;
    property Nacionalidad: String read FNacionalidad write FNacionalidad;
    property TienePasaporte: Boolean read FTienePasaporte write FTienePasaporte;
    property EstadoCivil: String read FEstadoCivil write FEstadoCivil;
    property DatosMatrimonio: String read FDatosMatrimonio write FDatosMatrimonio;
    property Transporte: String read FTransporte write FTransporte;
    property ResidenciaCalle: String read FResidenciaCalle write FResidenciaCalle;
    property ResidenciaColoniaCodigo: String read FResidenciaColoniaCodigo write FResidenciaColoniaCodigo;
    property ResidenciaColoniaNombre: String read FResidenciaColoniaNombre write FResidenciaColoniaNombre;
    property ResidenciaCodigoPostal: String read FResidenciaCodigoPostal write FResidenciaCodigoPostal;
    property ResidenciaCiudad: String read FResidenciaCiudad write FResidenciaCiudad;
    property ResidenciaCiudadZona: String read FResidenciaCiudadZona write FResidenciaCiudadZona;
    property ResidenciaEstado: String read FResidenciaEstado write FResidenciaEstado;
    property ResidenciaTelefono: String read FResidenciaTelefono write FResidenciaTelefono;
    property ResidenciaYears: TMoney read FResidenciaYears write FResidenciaYears;
    property ResidenciaMonths: TMoney read FResidenciaMonths write FResidenciaMonths;
    property ViveCon: String read FViveCon write FViveCon;
    property ViveEn: String read FViveEn write FViveEn;
    property GradoEstudios: String read FGradoEstudios write FGradoEstudios;
    property CarreraTerminada: String read FCarreraTerminada write FCarreraTerminada;
    property EstudiaActualmente: Boolean read FEstudiaActualmente write FEstudiaActualmente;
    property CarreraHorario: String read FCarreraHorario write FCarreraHorario;
    property HablaOtroIdioma: Boolean read FHablaOtroIdioma write FHablaOtroIdioma;
    property IdiomaDominio: String read FIdiomaDominio write FIdiomaDominio;
    property MaquinasConocidas: String read FMaquinasConocidas write FMaquinasConocidas;
    property Experiencia: String read FExperiencia write FExperiencia;
    property INFONAVITTipoPrestamo: eTipoInfonavit read FINFONAVITTipoPrestamo write FINFONAVITTipoPrestamo;
    property INFONAVITCredito: String read FINFONAVITCredito write FINFONAVITCredito;
    property INFONAVITAmortizacion: TMoney read FINFONAVITAmortizacion write FINFONAVITAmortizacion;
    property INFONAVITTasaAnterior: TTasa read FINFONAVITTasaAnterior write FINFONAVITTasaAnterior;
    property INFONAVITMantenimiento: Boolean read FINFONAVITMantenimiento write FINFONAVITMantenimiento;
    property INFONAVITInicioCredito: TDate read FINFONAVITInicioCredito write FINFONAVITInicioCredito;
    property EvaluacionFecha: TDate read FEvaluacionFecha write FEvaluacionFecha;
    property EvaluacionResultado: TMoney read FEvaluacionResultado write FEvaluacionResultado;
    property EvaluacionProxima: TDate read FEvaluacionProxima write FEvaluacionProxima;
    property AsistenciaCheca: Boolean read FAsistenciaCheca write FAsistenciaCheca;
    property AsistenciaLetraCredencial: String read FAsistenciaLetraCredencial write FAsistenciaLetraCredencial;
    property AsistenciaNumeroTarjeta: String read FAsistenciaNumeroTarjeta write FAsistenciaNumeroTarjeta;
    property BancaElectronica: String read FBancaElectronica write FBancaElectronica;
    property Confidencialidad: String read FConfidencialidad write FConfidencialidad;
    
    property ExtraFecha1: TDate read FExtraFecha1 write FExtraFecha1;
    property ExtraFecha2: TDate read FExtraFecha2 write FExtraFecha2;
    property ExtraFecha3: TDate read FExtraFecha3 write FExtraFecha3;
    property ExtraFecha4: TDate read FExtraFecha4 write FExtraFecha4;
    property ExtraFecha5: TDate read FExtraFecha5 write FExtraFecha5;
    property ExtraFecha6: TDate read FExtraFecha6 write FExtraFecha6;
    property ExtraFecha7: TDate read FExtraFecha7 write FExtraFecha7;
    property ExtraFecha8: TDate read FExtraFecha8 write FExtraFecha8;
    property ExtraLogico1: Boolean read FExtraLogico1 write FExtraLogico1;
    property ExtraLogico2: Boolean read FExtraLogico2 write FExtraLogico2;
    property ExtraLogico3: Boolean read FExtraLogico3 write FExtraLogico3;
    property ExtraLogico4: Boolean read FExtraLogico4 write FExtraLogico4;
    property ExtraLogico5: Boolean read FExtraLogico5 write FExtraLogico5;
    property ExtraLogico6: Boolean read FExtraLogico6 write FExtraLogico6;
    property ExtraLogico7: Boolean read FExtraLogico7 write FExtraLogico7;
    property ExtraLogico8: Boolean read FExtraLogico8 write FExtraLogico8;
    property ExtraNumero1: TMoney read FExtraNumero1 write FExtraNumero1;
    property ExtraNumero2: TMoney read FExtraNumero2 write FExtraNumero2;
    property ExtraNumero3: TMoney read FExtraNumero3 write FExtraNumero3;
    property ExtraNumero4: TMoney read FExtraNumero4 write FExtraNumero4;
    property ExtraNumero5: TMoney read FExtraNumero5 write FExtraNumero5;
    property ExtraNumero6: TMoney read FExtraNumero6 write FExtraNumero6;
    property ExtraNumero7: TMoney read FExtraNumero7 write FExtraNumero7;
    property ExtraNumero8: TMoney read FExtraNumero8 write FExtraNumero8;
    property ExtraNumero9: TMoney read FExtraNumero9 write FExtraNumero9;
    property ExtraNumero10: TMoney read FExtraNumero10 write FExtraNumero10;
    property ExtraNumero11: TMoney read FExtraNumero11 write FExtraNumero11;
    property ExtraNumero12: TMoney read FExtraNumero12 write FExtraNumero12;
    property ExtraNumero13: TMoney read FExtraNumero13 write FExtraNumero13;
    property ExtraNumero14: TMoney read FExtraNumero14 write FExtraNumero14;
    property ExtraNumero15: TMoney read FExtraNumero15 write FExtraNumero15;
    property ExtraNumero16: TMoney read FExtraNumero16 write FExtraNumero16;
    property ExtraNumero17: TMoney read FExtraNumero17 write FExtraNumero17;
    property ExtraNumero18: TMoney read FExtraNumero18 write FExtraNumero18;
    property ExtraTabla1: TPrimaryKey read FExtraTabla1 write FExtraTabla1;
    property ExtraTabla2: TPrimaryKey read FExtraTabla2 write FExtraTabla2;
    property ExtraTabla3: TPrimaryKey read FExtraTabla3 write FExtraTabla3;
    property ExtraTabla4: TPrimaryKey read FExtraTabla4 write FExtraTabla4;
    property ExtraTabla5: TPrimaryKey read FExtraTabla5 write FExtraTabla5;
    property ExtraTabla6: TPrimaryKey read FExtraTabla6 write FExtraTabla6;
    property ExtraTabla7: TPrimaryKey read FExtraTabla7 write FExtraTabla7;
    property ExtraTabla8: TPrimaryKey read FExtraTabla8 write FExtraTabla8;
    property ExtraTabla9: TPrimaryKey read FExtraTabla9 write FExtraTabla9;
    property ExtraTabla10: TPrimaryKey read FExtraTabla10 write FExtraTabla10;
    property ExtraTabla11: TPrimaryKey read FExtraTabla11 write FExtraTabla11;
    property ExtraTabla12: TPrimaryKey read FExtraTabla12 write FExtraTabla12;
    property ExtraTabla13: TPrimaryKey read FExtraTabla13 write FExtraTabla13;
    property ExtraTabla14: TPrimaryKey read FExtraTabla14 write FExtraTabla14;
    property ExtraTexto1: String read FExtraTexto1 write FExtraTexto1;
    property ExtraTexto2: String read FExtraTexto2 write FExtraTexto2;
    property ExtraTexto3: String read FExtraTexto3 write FExtraTexto3;
    property ExtraTexto4: String read FExtraTexto4 write FExtraTexto4;
    property ExtraTexto5: String read FExtraTexto5 write FExtraTexto5;
    property ExtraTexto6: String read FExtraTexto6 write FExtraTexto6;
    property ExtraTexto7: String read FExtraTexto7 write FExtraTexto7;
    property ExtraTexto8: String read FExtraTexto8 write FExtraTexto8;
    property ExtraTexto9: String read FExtraTexto9 write FExtraTexto9;
    property ExtraTexto10: String read FExtraTexto10 write FExtraTexto10;
    property ExtraTexto11: String read FExtraTexto11 write FExtraTexto11;
    property ExtraTexto12: String read FExtraTexto12 write FExtraTexto12;
    property ExtraTexto13: String read FExtraTexto13 write FExtraTexto13;
    property ExtraTexto14: String read FExtraTexto14 write FExtraTexto14;
    property ExtraTexto15: String read FExtraTexto15 write FExtraTexto15;
    property ExtraTexto16: String read FExtraTexto16 write FExtraTexto16;
    property ExtraTexto17: String read FExtraTexto17 write FExtraTexto17;
    property ExtraTexto18: String read FExtraTexto18 write FExtraTexto18;
    property ExtraTexto19: String read FExtraTexto19 write FExtraTexto19;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosEmpleadoAlta = class( TDatosEmpleadoInformacion )
  private
    { Private declarations }
    FZonaGeografica: TTexto;
    FUsaTabulador: Boolean;
    FNivel4: TPrimaryKey;
    FNivel7: TPrimaryKey;
    FNivel6: TPrimaryKey;
    FTurno: TPrimaryKey;
    FPuesto: TPrimaryKey;
    FTablaPrestaciones: TPrimaryKey;
    FNivel5: TPrimaryKey;
    FNivel1: TPrimaryKey;
    FNivel9: TPrimaryKey;
    FClasificacion: TPrimaryKey;
    FNivel8: TPrimaryKey;
    FNivel3: TPrimaryKey;
    FNivel2: TPrimaryKey;
{$ifdef ACS}
    FNivel10: TPrimaryKey;
    FNivel11: TPrimaryKey;
    FNivel12: TPrimaryKey;
{$endif}
    FContratoTipo: TPrimaryKey;
    FContratoInicio: TDate;
    FAvisoIMSS: TDate;
    FSalarioDiario: TMoney;
    FPromedioVariables: TMoney;
    FRegistroPatronal: TPrimaryKey;
    FFechaAntiguedad: TDate;
    FFechaIngreso: TDate;
    FPercepFijas: TTexto;
    FPadre: TTexto;
    FMadre: TTexto;
    FVacacionCierre: TDate;
    FVacacionDiasPagar: TMoney;
    FVacacionDiasGozar: TMoney;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property UsaTabulador: Boolean read FUsaTabulador write FUsaTabulador;
    property SalarioDiario: TMoney read FSalarioDiario write FSalarioDiario;
    property PromedioVariables: TMoney read FPromedioVariables write FPromedioVariables;
    property ZonaGeografica: String read FZonaGeografica write FZonaGeografica;
    property AvisoIMSS: TDate read FAvisoIMSS write FAvisoIMSS;
    property FechaIngreso: TDate read FFechaIngreso write FFechaIngreso;
    property FechaAntiguedad: TDate read FFechaAntiguedad write FFechaAntiguedad;
    property Turno: TPrimaryKey read FTurno write FTurno;
    property Puesto: TPrimaryKey read FPuesto write FPuesto;
    property Clasificacion: TPrimaryKey read FClasificacion write FClasificacion;
    property Nivel1: TPrimaryKey read FNivel1 write FNivel1;
    property Nivel2: TPrimaryKey read FNivel2 write FNivel2;
    property Nivel3: TPrimaryKey read FNivel3 write FNivel3;
    property Nivel4: TPrimaryKey read FNivel4 write FNivel4;
    property Nivel5: TPrimaryKey read FNivel5 write FNivel5;
    property Nivel6: TPrimaryKey read FNivel6 write FNivel6;
    property Nivel7: TPrimaryKey read FNivel7 write FNivel7;
    property Nivel8: TPrimaryKey read FNivel8 write FNivel8;
    property Nivel9: TPrimaryKey read FNivel9 write FNivel9;
{$ifdef ACS}
    property Nivel10: TPrimaryKey read FNivel10 write FNivel10;
    property Nivel11: TPrimaryKey read FNivel11 write FNivel11;
    property Nivel12: TPrimaryKey read FNivel12 write FNivel12;
{$endif}
    property ContratoTipo: TPrimaryKey read FContratoTipo write FContratoTipo;
    property ContratoInicio: TDate read FContratoInicio write FContratoInicio;
    property TablaPrestaciones: TPrimaryKey read FTablaPrestaciones write FTablaPrestaciones;
    property RegistroPatronal: TPrimaryKey read FRegistroPatronal write FRegistroPatronal;
    property PercepFijas: TTexto read FPercepFijas write FPercepFijas;
    property Padre: String read FPadre write FPadre;
    property Madre: String read FMadre write FMadre;
    property VacacionCierre: TDate read FVacacionCierre write FVacacionCierre;
    property VacacionDiasPagar: TMoney read FVacacionDiasPagar write FVacacionDiasPagar;
    property VacacionDiasGozar: TMoney read FVacacionDiasGozar write FVacacionDiasGozar;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosImagen = class( TDatosEmpleado )
  private
    { Private declarations }
    FTipo: TTexto;
    FObservaciones: TTexto;
    FImagen: TDatosBlob;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property Tipo: String read FTipo write FTipo;
    property Observaciones: String read FObservaciones write FObservaciones;
    property Imagen: TDatosBlob read FImagen write FImagen;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosKardex = class( TDatosEmpleado )
  private
    { Private declarations }
    FFecha: TDateKey;
    FTipo: String;
    FObservaciones: String;
    FNotas: String;
    FEditando: Boolean;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Fecha: TDateKey read FFecha;
    property Tipo: String read FTipo;
    property Notas: String read FNotas write FNotas;
    property Observaciones: String read FObservaciones write FObservaciones;
    property Editando: Boolean read FEditando write FEditando;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosKardexGeneral = class( TDatosKardex )
  private
    { Private declarations }
    FTipoGral: TStringKey;
    FMonto: TMoney;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property TipoGral: TStringKey read FTipoGral write FTipoGral;
    property Monto: TMoney read FMonto write FMonto;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosKardexBaja = class( TDatosKardex )
  private
    { Private declarations }
    FMotivo: TPrimaryKey;
    FBajaIMSS: TDate;
    FPeriodoPago: TDatosPeriodo;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Motivo: TPrimaryKey read FMotivo write FMotivo;
    property BajaIMSS: TDate read FBajaIMSS write FBajaIMSS;
    property PeriodoPago: TDatosPeriodo read FPeriodoPago;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosKardexReingreso = class( TDatosKardex )
  private
    { Private declarations }
    FDatosEmpleado: TDatosEmpleadoAlta;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property DatosEmpleado: TDatosEmpleadoAlta read FDatosEmpleado;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosKardexCambioSalario = class( TDatosKardex )
  private
    { Private declarations }
    FZonaGeografica: TTexto;
    FUsaTabulador: Boolean;
    FAvisoIMSS: TDate;
    FPromedioVariables: TMoney;
    FSalarioDiario: TMoney;
    FPercepFijas: TTexto;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    property UsaTabulador: Boolean read FUsaTabulador write FUsaTabulador;
    property SalarioDiario: TMoney read FSalarioDiario write FSalarioDiario;
    property PromedioVariables: TMoney read FPromedioVariables write FPromedioVariables;
    property ZonaGeografica: String read FZonaGeografica write FZonaGeografica;
    property AvisoIMSS: TDate read FAvisoIMSS write FAvisoIMSS;
    property PercepFijas: TTexto read FPercepFijas write FPercepFijas;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosKardexCambioTurno = class( TDatosKardex )
  private
    { Private declarations }
    FTurno: TPrimaryKey;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    property Turno: TPrimaryKey read FTurno write FTurno;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosKardexCambioPuesto = class( TDatosKardex )
  private
    { Private declarations }
    FPuesto: TPrimaryKey;
    FClasificacion: TPrimaryKey;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    property Puesto: TPrimaryKey read FPuesto write FPuesto;
    property Clasificacion: TPrimaryKey read FClasificacion write FClasificacion;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosKardexCambioArea = class( TDatosKardex )
  private
    { Private declarations }
    FNivel1: TPrimaryKey;
    FNivel2: TPrimaryKey;
    FNivel3: TPrimaryKey;
    FNivel4: TPrimaryKey;
    FNivel5: TPrimaryKey;
    FNivel6: TPrimaryKey;
    FNivel7: TPrimaryKey;
    FNivel8: TPrimaryKey;
    FNivel9: TPrimaryKey;
{$ifdef ACS}
    FNivel10: TPrimaryKey;
    FNivel11: TPrimaryKey;
    FNivel12: TPrimaryKey;
{$endif}
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    property Nivel1: TPrimaryKey read FNivel1 write FNivel1;
    property Nivel2: TPrimaryKey read FNivel2 write FNivel2;
    property Nivel3: TPrimaryKey read FNivel3 write FNivel3;
    property Nivel4: TPrimaryKey read FNivel4 write FNivel4;
    property Nivel5: TPrimaryKey read FNivel5 write FNivel5;
    property Nivel6: TPrimaryKey read FNivel6 write FNivel6;
    property Nivel7: TPrimaryKey read FNivel7 write FNivel7;
    property Nivel8: TPrimaryKey read FNivel8 write FNivel8;
    property Nivel9: TPrimaryKey read FNivel9 write FNivel9;
{$ifdef ACS}
    property Nivel10: TPrimaryKey read FNivel10 write FNivel10;
    property Nivel11: TPrimaryKey read FNivel11 write FNivel11;
    property Nivel12: TPrimaryKey read FNivel12 write FNivel12;
{$endif}
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosKardexCambioContrato = class( TDatosKardex )
  private
    { Private declarations }
    FTipoContrato: TPrimaryKey;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    property TipoContrato: TPrimaryKey read FTipoContrato write FTipoContrato;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosKardexCambioTablaPrestaciones = class( TDatosKardex )
  private
    { Private declarations }
    FTablaPrestaciones: TPrimaryKey;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    property TablaPrestaciones: TPrimaryKey read FTablaPrestaciones write FTablaPrestaciones;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosKardexCambioMultiple = class( TDatosKardex )
  private
    { Private declarations }
    FCambioArea: TDatosKardexCambioArea;
    FCambioPuesto: TDatosKardexCambioPuesto;
    FCambioTurno: TDatosKardexCambioTurno;
    FCambioContrato: TDatosKardexCambioContrato;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property CambioTurno: TDatosKardexCambioTurno read FCambioTurno;
    property CambioPuesto: TDatosKardexCambioPuesto read FCambioPuesto;
    property CambioArea: TDatosKardexCambioArea read FCambioArea;
    property CambioContrato: TDatosKardexCambioContrato read FCambioContrato;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosIncidencia = class( TDatosEmpleado )
  private
    { Private declarations }
    FInicio: TDateKey;
    FInicioAjustado: TDate;
    FRegresoAjustado: TDate;
    FStatusEmpleado: eStatusEmpleado;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Inicio: TDateKey read FInicio;
    property StatusEmpleado: eStatusEmpleado read FStatusEmpleado write FStatusEmpleado;
    property InicioAjustado: TDate read FInicioAjustado write FInicioAjustado;
    property RegresoAjustado: TDate read FRegresoAjustado write FRegresoAjustado;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosVacacionMovimiento = class( TDatosIncidencia )
  private
    { Private declarations }
    FPeriodoTrabajado: TTexto;
    FObservaciones: TTexto;
    FDiasGozo: Extended;
    FDiasPago: Extended;
    FRegreso: TDate;
    FPeriodoPago: TDatosPeriodo;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Regreso: TDate read FRegreso write FRegreso;
    property DiasGozo: Extended read FDiasGozo write FDiasGozo;
    property DiasPago: Extended read FDiasPago write FDiasPago;
    property Observaciones: String read FObservaciones write FObservaciones;
    property PeriodoTrabajado: String read FPeriodoTrabajado write FPeriodoTrabajado;
    property PeriodoPago: TDatosPeriodo read FPeriodoPago;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosVacacionCierre = class( TDatosEmpleado )
  private
    { Private declarations }
    FDiasDerechoGozo: Extended;
    FDiasDerechoPago: Extended;
    FFecha: TDate;
    FObservaciones: TTexto;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property Fecha: TDate read FFecha write FFecha;
    property DiasDerechoGozo: Extended read FDiasDerechoGozo write FDiasDerechoGozo;
    property DiasDerechoPago: Extended read FDiasDerechoPago write FDiasDerechoPago;
    property Observaciones: String read FObservaciones write FObservaciones;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosIncapacidad = class( TDatosIncidencia )
  private
    { Private declarations }
    FObservaciones: TTexto;
    FDias: Extended;
    FTipo: TPrimaryKey;
    FFinIncapacidad: eFinIncapacidad;
    FMotivoMedico: eMotivoIncapacidad;
    FNumero: TMoney;
    FTasaPermanente: TTasa;
    FPeriodoPago: TDatosPeriodo;
    FDiasSubsidio: Extended;
    FFechaCaptura: TDate;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Dias: Extended read FDias write FDias;
    property FechaCaptura: TDate read FFechaCaptura write FFechaCaptura;
    property Observaciones: String read FObservaciones write FObservaciones;
    property Tipo: TPrimaryKey read FTipo write FTipo;
    property Numero: TMoney read FNumero write FNumero;
    property MotivoMedico: eMotivoIncapacidad read FMotivoMedico write FMotivoMedico;
    property FinIncapacidad: eFinIncapacidad read FFinIncapacidad write FFinIncapacidad;
    property DiasSubsidio: Extended read FDiasSubsidio write FDiasSubsidio;
    property PeriodoPago: TDatosPeriodo read FPeriodoPago;
    property TasaPermanente: TTasa read FTasaPermanente write FTasaPermanente;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosPermiso = class( TDatosIncidencia )
  private
    { Private declarations }
    FObservaciones: TTexto;
    FDias: Extended;
    FTipo: TPrimaryKey;
    FReferencia: TTexto;
    FClase: eTipoPermiso;
    FFechaCaptura: TDate;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property Dias: Extended read FDias write FDias;
    property FechaCaptura: TDate read FFechaCaptura write FFechaCaptura;
    property Observaciones: String read FObservaciones write FObservaciones;
    property Clase: eTipoPermiso read FClase write FClase;
    property Tipo: TPrimaryKey read FTipo write FTipo;
    property Referencia: String read FReferencia write FReferencia;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosAhorroBase = class( TDatosEmpleado )
  private
    { Private declarations }
    FTipo: TStringKey;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Tipo: TStringKey read FTipo;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosAhorro = class( TDatosAhorroBase )
  private
    { Private declarations }
    FFormula: TTexto;
    FStatus: eStatusAhorro;
    FInicio: TDate;
    FSaldoInicial: TMoney;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property Inicio: TDate read FInicio write FInicio;
    property Status: eStatusAhorro read FStatus write FStatus;
    property Formula: String read FFormula write FFormula;
    property SaldoInicial: TMoney read FSaldoInicial write FSaldoInicial;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosAhorroCargoAbono = class( TDatosAhorroBase )
  private
    { Private declarations }
    FObservaciones: TTexto;
    FFecha: TDateKey;
    FFechaCaptura: TDate;
    FAbono: TMoney;
    FCargo: TMoney;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property Fecha: TDateKey read FFecha;
    property FechaCaptura: TDate read FFechaCaptura write FFechaCaptura;
    property Cargo: TMoney read FCargo write FCargo;
    property Abono: TMoney read FAbono write FAbono;
    property Observaciones: String read FObservaciones write FObservaciones;
    constructor Create;
    destructor Destroy; override;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosPrestamoBase = class( TDatosEmpleado )
  private
    { Private declarations }
    FTipo: TStringKey;
    FReferencia: TStringKey;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property Tipo: TStringKey read FTipo;
    property Referencia: TStringKey read FReferencia;
    constructor Create;
    destructor Destroy; override;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosPrestamo = class( TDatosPrestamoBase )
  private
    { Private declarations }
    FFormula: TTexto;
    FStatus: eStatusPrestamo;
    FInicio: TDate;
    FSaldoInicial: TMoney;
    FMonto: TMoney;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property Inicio: TDate read FInicio write FInicio;
    property Status: eStatusPrestamo read FStatus write FStatus;
    property Formula: String read FFormula write FFormula;
    property SaldoInicial: TMoney read FSaldoInicial write FSaldoInicial;
    property Monto: TMoney read FMonto write FMonto;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TDatosPrestamoCargoAbono = class( TDatosPrestamoBase )
  private
    { Private declarations }
    FFecha: TDateKey;
    FFechaCaptura: TDate;
    FObservaciones: TTexto;
    FAbono: TMoney;
    FCargo: TMoney;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property Fecha: TDateKey read FFecha;
    property FechaCaptura: TDate read FFechaCaptura write FFechaCaptura;
    property Cargo: TMoney read FCargo write FCargo;
    property Abono: TMoney read FAbono write FAbono;
    property Observaciones: String read FObservaciones write FObservaciones;
    constructor Create;
    destructor Destroy; override;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TParametrosProceso = class( TDataTransport )
  private
    { Private declarations }
    FOk: Boolean;
    FResultado: TProcessResult;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Ok: Boolean read FOk write FOk;
    property Resultado: TProcessResult read FResultado;
    procedure CargaZetaParams( Parametros: TZetaParams ); virtual;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
    {$ifndef DOTNET}
    procedure LoadException( Error: Exception );
    {$endif}
  end;
  TParametrosProcesoFiltro = class( TParametrosProceso )
  private
    { Private declarations }
    FFiltro: TProcessFilter;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Filtro: TProcessFilter read FFiltro;
    procedure CargaZetaParams( Parametros: TZetaParams ); override;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TParametrosNomina = class( TParametrosProcesoFiltro )
  private
    { Private declarations }
    FPeriodo: TDatosPeriodo;
    FIMSS: TDatosIMSS;
    FValoresActivos: TDatosActivos;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property ValoresActivos: TDatosActivos read FValoresActivos;
    property Periodo: TDatosPeriodo read FPeriodo;
    property IMSS: TDatosIMSS read FIMSS;
    procedure CargaZetaParams(Parametros: TZetaParams); override;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TParametrosNominaCalcular = class( TParametrosNomina )
  private
    { Private declarations }
    FMostrarTiempos: Boolean;
    FMostrarScript: Boolean;
    FTipo: eTipoCalculo;
  public
    { Public declarations }
    property MostrarScript: Boolean read FMostrarScript write FMostrarScript;
    property MostrarTiempos: Boolean read FMostrarTiempos write FMostrarTiempos;
    property Tipo: eTipoCalculo read FTipo write FTipo;
    procedure CargaZetaParams(Parametros: TZetaParams); override;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TParametrosNominaAfectar = class( TParametrosNomina )
  private
    { Private declarations }
  public
    { Public declarations }
  end;
  TParametrosNominaDesafectar = class( TParametrosNomina )
  private
    { Private declarations }
  public
    { Public declarations }
  end;
  TParametrosNominaGenerarPoliza = class( TParametrosNomina )
  private
    { Private declarations }
    FRangoPeriodos: TTexto;
    FFormato: Integer;
  public
    { Public declarations }
    property Formato: Integer read FFormato write FFormato;
    property RangoPeriodos: String read FRangoPeriodos write FRangoPeriodos;
    procedure CargaZetaParams(Parametros: TZetaParams); override;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TParametrosNominaRastrear = class( TParametrosNomina )
  private
    { Private declarations }
    FConceptoNombre: TTexto;
    FConceptoFormula: TTexto;
    FEmpleadoNombre: TTexto;
    FMostrarParametros: Boolean;
    FConcepto: Integer;
    FDatos: String;
    FEmpleado: Integer;
  public
    { Public declarations }
    property Concepto: Integer read FConcepto write FConcepto;
    property ConceptoNombre: String read FConceptoNombre write FConceptoNombre;
    property ConceptoFormula: String read FConceptoFormula write FConceptoFormula;
    property Datos: String read FDatos write FDatos;
    property Empleado: Integer read FEmpleado write FEmpleado;
    property EmpleadoNombre: String read FEmpleadoNombre write FEmpleadoNombre;
    property MostrarParametros: Boolean read FMostrarParametros write FMostrarParametros;
    procedure CargaZetaParams(Parametros: TZetaParams); override;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TParametrosNominaNetoBruto = class( TParametrosNomina )
  private
    { Private declarations }
    FZonaGeografica: TTexto;
    FTablaPrestaciones: TTexto;
    FOtrasPercepciones: TTexto;
    FOtrasPercepcionesMonto: Extended;
    FCalcularBruto: Boolean;
    FSubsidioAcreditable: Extended;
    FRedondeo: Extended;
    FIMSS: Extended;
    FISPT: Extended;
    FSalario: Extended;
    FSalarioBruto: Extended;
    FSalarioIntegrado: Extended;
    FSalarioMinimo: Extended;
    FAntiguedad: Integer;
    FPeriodo: Integer;
    FFactorIntegracion: Extended;
    FFecha: TDate;
    function GetSalarioNeto: Extended;
  public
    { Public declarations }
    property Antiguedad: Integer read FAntiguedad write FAntiguedad;
    property CalcularBruto: Boolean read FCalcularBruto write FCalcularBruto;
    property FactorIntegracion: Extended read FFactorIntegracion write FFactorIntegracion;
    property Fecha: TDate read FFecha write FFecha;
    property IMSS: Extended read FIMSS write FIMSS;
    property ISPT: Extended read FISPT write FISPT;
    property OtrasPercepciones: String read FOtrasPercepciones write FOtrasPercepciones;
    property OtrasPercepcionesMonto: Extended read FOtrasPercepcionesMonto write FOtrasPercepcionesMonto;
    property Periodo: Integer read FPeriodo write FPeriodo;
    property Redondeo: Extended read FRedondeo write FRedondeo;
    property Salario: Extended read FSalario write FSalario;
    property SalarioBruto: Extended read FSalarioBruto write FSalarioBruto;
    property SalarioIntegrado: Extended read FSalarioIntegrado write FSalarioIntegrado;
    property SalarioMinimo: Extended read FSalarioMinimo write FSalarioMinimo;
    property SalarioNeto: Extended read GetSalarioNeto;
    property SubsidioAcreditable: Extended read FSubsidioAcreditable write FSubsidioAcreditable;
    property TablaPrestaciones: String read FTablaPrestaciones write FTablaPrestaciones;
    property ZonaGeografica: String read FZonaGeografica write FZonaGeografica;
    function Convertir( const rValue: Extended; const ePeriodo: eTipoPeriodo ): Extended;
    procedure CargaZetaParams(Parametros: TZetaParams); override;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TParametrosIMSS = class( TParametrosProcesoFiltro )
  private
    { Private declarations }
    FPeriodo: TDatosPeriodo;
    FIMSS: TDatosIMSS;
    FValoresActivos: TDatosActivos;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property IMSS: TDatosIMSS read FIMSS;
    property Periodo: TDatosPeriodo read FPeriodo;
    property ValoresActivos: TDatosActivos read FValoresActivos;
    procedure CargaZetaParams(Parametros: TZetaParams); override;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;
  TParametrosIMSSRevisarSUA = class( TParametrosIMSS )
  private
    { Private declarations }
  public
    { Public declarations }
  end;
  TParametrosIMSSExportarSUA = class( TParametrosIMSS )
  private
    { Private declarations }
    FNumeroRegistro: TTexto;
    FArchivoEmpleados: TTexto;
    FArchivoMovimientos: TTexto;
    FPatron: TTexto;
    FBorrarMovimientos: Boolean;
    FEnviarCuotaFija: Boolean;
    FIncluirHomoclave: Boolean;
    FEnviarMovimientos: Boolean;
    FEnviarEmpleados: Boolean;
    FBorrarEmpleados: Boolean;
    FEnviarFaltaBaja: Boolean;
    FYear: Integer;
    FMes: Integer;
  public
    { Public declarations }
    property Patron: String read FPatron write FPatron;
    property NumeroRegistro: String read FNumeroRegistro write FNumeroRegistro;
    property Year: Integer read FYear write FYear;
    property Mes: Integer read FMes write FMes;
    property ArchivoEmpleados: String read FArchivoEmpleados write FArchivoEmpleados;
    property ArchivoMovimientos: String read FArchivoMovimientos write FArchivoMovimientos;
    property EnviarEmpleados: Boolean read FEnviarEmpleados write FEnviarEmpleados;
    property BorrarEmpleados: Boolean read FBorrarEmpleados write FBorrarEmpleados;
    property IncluirHomoclave: Boolean read FIncluirHomoclave write FIncluirHomoclave;
    property EnviarCuotaFija: Boolean read FEnviarCuotaFija write FEnviarCuotaFija;
    property EnviarMovimientos: Boolean read FEnviarMovimientos write FEnviarMovimientos;
    property BorrarMovimientos: Boolean read FBorrarMovimientos write FBorrarMovimientos;
    property EnviarFaltaBaja: Boolean read FEnviarFaltaBaja write FEnviarFaltaBaja;
    procedure CargaZetaParams(Parametros: TZetaParams); override;
    procedure CargaParametros( Lista: TDataTransport ); override;
    procedure DescargaParametros( Lista: TDataTransport ); override;
  end;

  TDatosReporte = class( TDataTransport )
  private
    { Private declarations }
    FDatosImss: TDatosImss;
    FDatosPeriodo: TDatosPeriodo;
    FDatosActivos: TDatosActivos;
    FReporte: integer;
    FNombre: string;
    FPath: string;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property Reporte: integer read FReporte write FReporte;
    property Path: string read FPath write FPath;
    property Nombre: string read FNombre write FNombre;
    property DatosActivos: TDatosActivos read FDatosActivos;
    property DatosImss: TDatosImss read FDatosImss;
    property DatosPeriodo: TDatosPeriodo read FDatosPeriodo;
    constructor Create;
    destructor Destroy; override;
    procedure CargaParametros( Parametros: TDataTransport );override;
    procedure DescargaParametros( Parametros: TDataTransport );override;
  end;

implementation

uses ZetaCommonTools;

const
     K_OLD_SUFFIX = 'OLD';
     
{ ******* TEmpresaActiva ********* }

function TEmpresaActiva.GetValue: Variant;
begin
     Result := VarArrayOf( [ BaseDeDatos, UserName, UserPassword, Usuario, Confidencialidad, Codigo, 1 ] );
end;

procedure TEmpresaActiva.SetValue(const Value: Variant);
begin
     Self.BaseDeDatos := Value[ P_DATABASE ];
     Self.UserName := Value[ P_USER_NAME ];
     Self.UserPassword := Value[ P_PASSWORD ];
     Self.Usuario := Value[ P_USUARIO ];
     Self.Confidencialidad := Value[ P_NIVEL_0 ];
     Self.Codigo := Value[ P_CODIGO ];
     Self.Grupo := Value[ P_GRUPO ];
end;

procedure TEmpresaActiva.CargaParametros( Parametros: TDataTransport );
begin
     with Parametros do
     begin
          AddString( 'Empresa.BaseDatos', Self.BaseDeDatos );
          AddString( 'Empresa.UserName', Self.UserName );
          AddString( 'Empresa.UserPassword', Self.UserPassword );
          AddString( 'Empresa.Confidencialidad', Trim( Self.Confidencialidad ) );
          AddString( 'Empresa.Codigo', Self.Codigo );
          AddInteger( 'Empresa.Usuario', Self.Usuario );
          AddInteger( 'Empresa.Grupo', Self.Grupo );
     end;
end;

procedure TEmpresaActiva.DescargaParametros( Parametros: TDataTransport );
begin
     with Parametros do
     begin
          Self.BaseDeDatos := ParamAsString( 'Empresa.BaseDatos' );
          Self.UserName := ParamAsString( 'Empresa.UserName' );
          Self.UserPassword := ParamAsString( 'Empresa.UserPassword' );
          Self.Confidencialidad := Trim( ParamAsString( 'Empresa.Confidencialidad' ) );
          Self.Codigo := ParamAsString( 'Empresa.Codigo' );
          Self.Usuario := ParamAsInteger( 'Empresa.Usuario' );
          Self.Grupo := ParamAsInteger( 'Empresa.Grupo' );
     end;
end;

{ ******** TDatosPeriodo *********** }

procedure TDatosPeriodo.CargaZetaParams( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddInteger( 'Year', Year );
          AddInteger( 'Tipo', Ord( Tipo ) );
          AddInteger( 'Numero', Numero );
     end;
end;

procedure TDatosPeriodo.CargaParametros( Parametros: TDataTransport );
begin
     with Parametros do
     begin
          AddInteger( 'Periodo.Year', Year );
          AddInteger( 'Periodo.Tipo', Ord( Tipo ) );
          AddInteger( 'Periodo.Numero', Numero );
     end;
end;

procedure TDatosPeriodo.DescargaParametros( Parametros: TDataTransport );
begin
     with Parametros do
     begin
          Self.Year := ParamByName( 'Periodo.Year' ).AsInteger;
          Self.Tipo := eTipoPeriodo( ParamByName( 'Periodo.Tipo' ).AsInteger );
          Self.Numero := ParamByName( 'Periodo.Numero' ).AsInteger;
     end;
end;

{ ******** TDatosPeriodo *********** }

procedure TDatosImss.CargaZetaParams(Parametros: TZetaParams);
begin
     with Parametros do
     begin
          AddString( 'RegistroPatronal', Patron );
          AddInteger( 'IMSSYear', Self.Year );
          AddInteger( 'IMSSMes', Self.Mes );
          AddInteger( 'IMSSTipo', Ord( Self.Tipo ) );
     end;
end;

procedure TDatosIMSS.CargaParametros( Parametros: TDataTransport );
begin
     with Parametros do
     begin
          AddString( 'IMSS.RegistroPatronal', Patron );
          AddInteger( 'IMSS.Year', Year );
          AddInteger( 'IMSS.Mes', Mes );
          AddInteger( 'IMSS.Tipo', Ord( Tipo ) );
     end;
end;

procedure TDatosIMSS.DescargaParametros( Parametros: TDataTransport );
begin
     with Parametros do
     begin
          Self.Patron := ParamByName( 'IMSS.RegistroPatronal' ).AsString;
          Self.Year := ParamByName( 'IMSS.Year' ).AsInteger;
          Self.Mes := ParamByName( 'IMSS.Mes' ).AsInteger;
          Self.Tipo := eTipoLiqIMSS( ParamByName( 'IMSS.Tipo' ).AsInteger );
     end;
end;

{ ******** TDatosActivos *********** }

constructor TDatosActivos.Create;
begin
     inherited Create;
     FechaDefault := Now;
     FechaAsistencia := Now;
     YearDefault := ZetaCommonTools.TheYear( Now );
end;

procedure TDatosActivos.CargaZetaParams(Parametros: TZetaParams);
begin
     with Parametros do
     begin
          AddDate( 'FechaAsistencia', FechaAsistencia );
          AddDate( 'FechaDefault', FechaDefault );
          AddInteger( 'YearDefault', YearDefault );
          AddInteger( 'EmpleadoActivo', EmpleadoActivo );
          AddString( 'NombreUsuario', NombreUsuario );
          AddString( 'CodigoEmpresa', CodigoEmpresa );
     end;
end;

procedure TDatosActivos.CargaParametros( Parametros: TDataTransport );
begin
     with Parametros do
     begin
          AddDate( 'Activos.FechaAsistencia', FechaAsistencia );
          AddDate( 'Activos.FechaDefault', FechaDefault );
          AddInteger( 'Activos.YearDefault', YearDefault );
          AddInteger( 'Activos.EmpleadoActivo', EmpleadoActivo );
          AddString( 'Activos.NombreUsuario', NombreUsuario );
          AddString( 'Activos.CodigoEmpresa', CodigoEmpresa );
     end;
end;

procedure TDatosActivos.DescargaParametros( Parametros: TDataTransport );
begin
     with Parametros do
     begin
          Self.FechaAsistencia := ParamByName( 'Activos.FechaAsistencia' ).AsDate;
          Self.FechaDefault := ParamByName( 'Activos.FechaDefault' ).AsDate;
          Self.YearDefault := ParamByName( 'Activos.YearDefault' ).AsInteger;
          Self.EmpleadoActivo := ParamByName( 'Activos.EmpleadoActivo' ).AsInteger;
          Self.NombreUsuario := ParamByName( 'Activos.NombreUsuario' ).AsString;
          Self.CodigoEmpresa := ParamByName( 'Activos.CodigoEmpresa' ).AsString;
     end;
end;

{ ********* TProcessFilter ******** }

constructor TProcessFilter.Create;
const
     K_CODIGO_EMPLEADO = 'CB_CODIGO';
begin
     inherited Create;
     FRangoCampo := Format( '%s.%s', [ ZetaCommonClasses.ARROBA_TABLA, K_CODIGO_EMPLEADO ] );
end;

function TProcessFilter.GetRangeExpression: String;
begin
     Result := '';
     case Self.TipoRango of
          raRango:
          begin
               if ZetaCommonTools.StrLleno( Self.RangoInicio ) and ZetaCommonTools.StrLleno( Self.RangoFin ) then
                  Result := ZetaCommonTools.ConcatFiltros( ZetaCommonTools.Parentesis( Self.RangoCampo +  '>= ' + Self.RangoInicio ),
                                                           ZetaCommonTools.Parentesis( Self.RangoCampo + '<= ' + Self.RangoFin ) )
               else
                   if ZetaCommonTools.StrLleno( Self.RangoInicio ) then
                      Result := ZetaCommonTools.Parentesis( Self.RangoCampo + '>= ' + Self.RangoInicio )
                   else
                       if ZetaCommonTools.StrLleno( Self.RangoFin ) then
                          Result := ZetaCommonTools.Parentesis( Self.RangoCampo + '<= ' + Self.RangoFin );
          end;
          raLista:
          begin
               if ZetaCommonTools.StrLleno( Self.RangoLista ) then
                  Result := ZetaCommonTools.Parentesis( Format( '%s in ( %s )', [ Self.RangoCampo, Self.RangoLista ] ) );
          end;
     end;
end;

procedure TProcessFilter.CargaZetaParams( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddString( 'RangoLista', Self.GetRangeExpression );
          AddString( 'Condicion', Self.Condicion );
          AddString( 'Filtro', Self.Filtro );
     end;
end;

procedure TProcessFilter.CargaParametros( Parametros: TDataTransport );
begin
     with Parametros do
     begin
          AddInteger( 'Filtro.TipoRango', Ord( Self.TipoRango ) );
          AddString( 'Filtro.RangoInicio', Self.RangoInicio );
          AddString( 'Filtro.RangoFin', Self.RangoFin );
          AddString( 'Filtro.RangoLista', Self.RangoLista );
          AddString( 'Filtro.Condicion', Self.Condicion );
          AddString( 'Filtro.Filtro', Self.Filtro );
     end;
end;

procedure TProcessFilter.DescargaParametros( Parametros: TDataTransport );
begin
     with Parametros do
     begin
          Self.TipoRango := eTipoRangoActivo( ParamByName( 'Filtro.TipoRango' ).AsInteger );
          Self.RangoInicio := ParamByName( 'Filtro.RangoInicio' ).AsString;
          Self.RangoFin := ParamByName( 'Filtro.RangoFin' ).AsString;
          Self.RangoLista := ParamByName( 'Filtro.RangoLista' ).AsString;
          Self.Condicion := ParamByName( 'Filtro.Condicion' ).AsString;
          Self.Filtro := ParamByName( 'Filtro.Filtro' ).AsString;
     end;
end;

{ ******** TIntegerKey ******** }

procedure TIntegerKey.CargaParametros(Parametros: TDataTransport; const sTag: String);
begin
     with Parametros do
     begin
          AddInteger( sTag, Self.Value );
          AddInteger( sTag + K_OLD_SUFFIX, Self.OldValue );
     end;
end;

procedure TIntegerKey.DescargaParametros(Parametros: TDataTransport; const sTag: String);
begin
     with Parametros do
     begin
          Self.Value := ParamAsInteger( sTag );
          Self.OldValue := ParamAsInteger( sTag + K_OLD_SUFFIX );
     end;
end;

{ ******* TFaltaDiasKey ******* }

procedure TFaltaDiasKey.CargaParametros(Parametros: TDataTransport; const sTag: String);
begin
     with Parametros do
     begin
          AddInteger( sTag, Ord( Self.Value ) );
          AddInteger( sTag + K_OLD_SUFFIX, Ord( Self.OldValue ) );
     end;
end;

procedure TFaltaDiasKey.DescargaParametros(Parametros: TDataTransport; const sTag: String);
begin
     with Parametros do
     begin
          Self.Value := eMotivoFaltaDias( ParamAsInteger( sTag ) );
          Self.OldValue := eMotivoFaltaDias( ParamAsInteger( sTag + K_OLD_SUFFIX ) );
     end;
end;

{ ******* TFaltaHorasKey ******* }

procedure TFaltaHorasKey.CargaParametros(Parametros: TDataTransport; const sTag: String);
begin
     with Parametros do
     begin
          AddInteger( sTag, Ord( Self.Value ) );
          AddInteger( sTag + K_OLD_SUFFIX, Ord( Self.OldValue ) );
     end;
end;

procedure TFaltaHorasKey.DescargaParametros(Parametros: TDataTransport; const sTag: String);
begin
     with Parametros do
     begin
          Self.Value := eMotivoFaltaHoras( ParamAsInteger( sTag ) );
          Self.OldValue := eMotivoFaltaHoras( ParamAsInteger( sTag + K_OLD_SUFFIX ) );
     end;
end;

{ ***** TDateKey **** }

procedure TDateKey.CargaParametros(Parametros: TDataTransport; const sTag: String);
begin
     with Parametros do
     begin
          AddDate( sTag, Self.Value );
          AddDate( sTag + K_OLD_SUFFIX, Self.OldValue );
     end;
end;

procedure TDateKey.DescargaParametros(Parametros: TDataTransport; const sTag: String);
begin
     with Parametros do
     begin
          Self.Value := ParamAsDate( sTag );
          Self.OldValue := ParamAsDate( sTag + K_OLD_SUFFIX );
     end;
end;

{ ****** TStringKey ***** }

procedure TStringKey.CargaParametros(Parametros: TDataTransport; const sTag: String);
begin
     with Parametros do
     begin
          AddString( sTag, Self.Value );
          AddString( sTag + K_OLD_SUFFIX, Self.OldValue );
     end;
end;

procedure TStringKey.DescargaParametros(Parametros: TDataTransport; const sTag: String);
begin
     with Parametros do
     begin
          Self.Value := ParamAsString( sTag );
          Self.OldValue := ParamAsString( sTag + K_OLD_SUFFIX );
     end;
end;

{ ********** TDataTransport *********** }

constructor TDataTransport.Create;
begin
     inherited Create;
     FEmpresaActiva := TEmpresaActiva.Create;
end;

destructor TDataTransport.Destroy;
begin
     FreeAndNil( FEmpresaActiva );
     inherited Destroy;
end;

function TDataTransport.GetVarValues: OleVariant;
begin
     CargaParametros( Self );
     Result := inherited VarValues;
end;

procedure TDataTransport.SetVarValues( Value: OleVariant );
begin
     inherited VarValues := Value;
     DescargaParametros( Self );
end;

function TDataTransport.GetTextValue: String;
var
   i, iHigh: Integer;
   sValue: String;
begin
     CargaParametros( Self );
     iHigh := Count - 1;
     Result := '';
     for i := 0 to iHigh do
     begin
          with Items[ i ] do
          begin
               case DataType of
                    ftInteger: sValue := IntToStr( AsInteger );
                    ftFloat: sValue := Format( '%f', [ AsFloat ] );
                    ftDate: sValue := FormatDateTime( 'yyyymmdd', AsDate );
                    ftBoolean: sValue := ZetaCommonTools.zBoolToStr( AsBoolean );
                    ftMemo: sValue := AsString;
                    ftString: sValue := AsString;
               else
                   sValue := '';
               end;
               sValue := Format( '%2.2d%s=%s|', [ Ord( DataType ), Name, sValue ] );
          end;
          Result := Result + sValue;
     end;
end;

procedure TDataTransport.SetTextValue( const Value: String );
var
   iPos, iDelimitador: Integer;
   sValue, sValues, sNombre: String;
   eTipo: TFieldType;
begin
     sValues := Value;
     repeat
           iPos := Pos( '|', sValues );
           if ( iPos > 0 ) then
           begin
                sValue := Copy( sValues, 1, ( iPos - 1 ) );
                sValues := Copy( sValues, ( iPos + 1 ), ( Length( sValues ) - iPos ) );
                eTipo := TFieldType( StrToIntDef( Copy( sValue, 1, 2 ), 0 ) );
                sValue := Copy( sValue, 3, ( Length( sValue ) - 2 ) );
                iDelimitador := Pos( '=', sValue );
                if ( iDelimitador > 0 ) then
                begin
                     sNombre := Copy( sValue, 1, ( iDelimitador - 1 ) );
                     sValue := Copy( sValue, ( iDelimitador + 1 ), ( Length( sValue ) - iDelimitador ) );
                     case eTipo of
                          ftInteger: Self.AddInteger( sNombre, StrToIntDef( sValue, 0 ) );
                          ftFloat: Self.AddFloat( sNombre, StrToFloat( sValue ) );
                          ftDate: Self.AddDate( sNombre, ZetaCommonTools.StrToFecha( sValue ) );
                          ftBoolean: Self.AddBoolean( sNombre, ZetaCommonTools.zStrToBool( sValue ) );
                          ftMemo: Self.AddMemo( sNombre, sValue );
                          ftString: Self.AddString( sNombre, sValue );
                     end;
                end;
           end;
     until ( iPos = 0 ) or ZetaCommonTools.StrVacio( sValues );
     DescargaParametros( Self );
end;

function TDataTransport.ParamAsBlob(const sName: String): TDatosBlob;
begin
     Result := ParamByName( sName ).AsBlob;
end;

function TDataTransport.ParamAsBoolean(const sName: String): Boolean;
begin
     Result := ParamByName( sName ).AsBoolean;
end;

function TDataTransport.ParamAsDate(const sName: String): TDate;
begin
     Result := ParamByName( sName ).AsDate;
end;

function TDataTransport.ParamAsFloat(const sName: String): Extended;
begin
     Result := ParamByName( sName ).AsFloat;
end;

function TDataTransport.ParamAsInteger(const sName: String): Integer;
begin
     Result := ParamByName( sName ).AsInteger;
end;

function TDataTransport.ParamAsMemo(const sName: String): String;
begin
     Result := ParamByName( sName ).AsMemo;
end;

function TDataTransport.ParamAsString(const sName: String): String;
begin
     Result := Trim( ParamByName( sName ).AsString );
end;

procedure TDataTransport.CargaParametros( Lista: TDataTransport );
begin
     with Lista do
     begin
          AddString( 'StatusMsg', Self.Mensaje );
     end;
     Self.EmpresaActiva.CargaParametros( Lista );
end;

procedure TDataTransport.DescargaParametros( Lista: TDataTransport );
begin
     with Lista do
     begin
          Self.Mensaje := ParamAsString( 'StatusMsg' );
     end;
     Self.EmpresaActiva.DescargaParametros( Lista );
end;

{ ****** TDatosEmpleado ****** }

constructor TDatosEmpleado.Create;
begin
     inherited Create;
end;

destructor TDatosEmpleado.Destroy;
begin
     inherited Destroy;
end;

procedure TDatosEmpleado.CargaParametros(Lista: TDataTransport);
begin
     with Lista do
     begin
          AddInteger( 'Empleado', Self.Empleado );
     end;
     inherited CargaParametros( Lista );
end;

procedure TDatosEmpleado.DescargaParametros(Lista: TDataTransport);
begin
     inherited DescargaParametros( Lista );
     with Lista do
     begin
          Self.Empleado := ParamAsInteger( 'Empleado' );
     end;
end;

{ ********* TDatosExcepcion ********* }

constructor TDatosExcepcion.Create;
begin
     inherited Create;
     FDatosPeriodo := TDatosPeriodo.Create;
end;

destructor TDatosExcepcion.Destroy;
begin
     FreeAndNil( FDatosPeriodo );
     inherited Destroy;
end;

procedure TDatosExcepcion.CargaParametros( Lista: TDataTransport );
begin
     Self.DatosPeriodo.CargaParametros( Lista );
     inherited CargaParametros( Lista );
end;

procedure TDatosExcepcion.DescargaParametros(Lista: TDataTransport);
begin
     inherited DescargaParametros( Lista );
     Self.DatosPeriodo.DescargaParametros( Lista );
end;

{ ******** TDatosExcepcionMonto ********* }

constructor TDatosExcepcionMonto.Create;
begin
     inherited Create;
     FConcepto := TIntegerKey.Create;
     FReferencia := TStringKey.Create;
end;

destructor TDatosExcepcionMonto.Destroy;
begin
     FreeAndNil( FConcepto );
     FreeAndNil( FReferencia );
     inherited Destroy;
end;

procedure TDatosExcepcionMonto.CargaParametros( Lista: TDataTransport );
begin
     Self.Concepto.CargaParametros( Lista, 'Concepto' );
     Self.Referencia.CargaParametros( Lista, 'Referencia' );
     with Lista do
     begin
          AddFloat( 'Monto', Self.Monto );
          AddBoolean( 'Activo', Self.Activo );
          AddBoolean( 'EsPercepcion', Self.EsPercepcion );
          AddFloat( 'ISPTIndividual', Self.ISPTIndividual );
          AddBoolean( 'Individual', Self.Individual );
          AddFloat( 'ParteExenta', Self.ParteExenta );
     end;
     inherited CargaParametros( Lista );
end;

procedure TDatosExcepcionMonto.DescargaParametros(Lista: TDataTransport);
begin
     inherited DescargaParametros( Lista );
     Self.Concepto.DescargaParametros( Lista, 'Concepto' );
     Self.Referencia.DescargaParametros( Lista, 'Referencia' );
     with Lista do
     begin
          Self.Monto := ParamAsFloat( 'Monto' );
          Self.Activo := ParamAsBoolean( 'Activo' );
          Self.EsPercepcion := ParamAsBoolean( 'EsPercepcion' );
          Self.ISPTIndividual := ParamAsFloat( 'ISPTIndividual' );
          Self.Individual := ParamAsBoolean( 'Individual' );
          Self.ParteExenta := ParamAsFloat( 'ParteExenta' );
     end;
end;

{ ******** TDatosExcepcionDiaHora ******** }

constructor TDatosExcepcionDiaHora.Create;
begin
     inherited Create;
     FFecha := TDateKey.Create;
end;

destructor TDatosExcepcionDiaHora.Destroy;
begin
     FreeAndNil( FFecha );
     inherited Destroy;
end;

procedure TDatosExcepcionDiaHora.CargaParametros(Lista: TDataTransport);
begin
     Self.Fecha.CargaParametros( Lista, 'Fecha' );
     inherited CargaParametros( Lista );
end;

procedure TDatosExcepcionDiaHora.DescargaParametros(Lista: TDataTransport);
begin
     inherited DescargaParametros( Lista );
     Self.Fecha.DescargaParametros( Lista, 'Fecha' );
end;

{ ******** TDatosExcepcionDia ******** }

constructor TDatosExcepcionDia.Create;
begin
     inherited Create;
     FTipo := TFaltaDiasKey.Create;
end;

destructor TDatosExcepcionDia.Destroy;
begin
     inherited Destroy;
     FreeAndNil( FTipo );
end;

procedure TDatosExcepcionDia.CargaParametros( Lista: TDataTransport );
begin
     Self.Tipo.CargaParametros( Lista, 'Tipo' );
     with Lista do
     begin
          AddFloat( 'Dias', Self.Dias );
     end;
     inherited CargaParametros( Lista );
end;

procedure TDatosExcepcionDia.DescargaParametros(Lista: TDataTransport);
begin
     inherited DescargaParametros( Lista );
     Self.Tipo.DescargaParametros( Lista, 'Tipo' );
     with Lista do
     begin
          Self.Dias := ParamAsFloat( 'Dias' );
     end;
end;

{ ******** TDatosExcepcionHora ********* }

constructor TDatosExcepcionHora.Create;
begin
     inherited Create;
     FTipo := TFaltaHorasKey.Create;
end;

destructor TDatosExcepcionHora.Destroy;
begin
     FreeAndNil( FTipo );
     inherited Destroy;
end;

procedure TDatosExcepcionHora.CargaParametros( Lista: TDataTransport );
begin
     Self.Tipo.CargaParametros( Lista, 'Tipo' );
     with Lista do
     begin
          AddFloat( 'Horas', Self.Horas );
     end;
     inherited CargaParametros( Lista );
end;

procedure TDatosExcepcionHora.DescargaParametros(Lista: TDataTransport);
begin
     inherited DescargaParametros( Lista );
     Self.Tipo.DescargaParametros( Lista, 'Tipo' );
     with Lista do
     begin
          Self.Horas := ParamAsFloat( 'Horas' );
     end;
end;

{ ********* TDatosNominaClasificacion ********** }

procedure TDatosNominaClasificacion.CargaParametros(Lista: TDataTransport);
begin
     with Lista do
     begin
          AddString( 'ZonaGeografica', Self.ZonaGeografica );
          AddString( 'Turno', Self.Turno );
          AddString( 'Puesto', Self.Puesto );
          AddString( 'Clasificacion', Self.Clasificacion );
          AddString( 'Nivel1', Self.Nivel1 );
          AddString( 'Nivel2', Self.Nivel2 );
          AddString( 'Nivel3', Self.Nivel3 );
          AddString( 'Nivel4', Self.Nivel4 );
          AddString( 'Nivel5', Self.Nivel5 );
          AddString( 'Nivel6', Self.Nivel6 );
          AddString( 'Nivel7', Self.Nivel7 );
          AddString( 'Nivel8', Self.Nivel8 );
          AddString( 'Nivel9', Self.Nivel9 );
{$ifdef ACS}
          AddString( 'Nivel10', Self.Nivel10 );
          AddString( 'Nivel11', Self.Nivel11 );
          AddString( 'Nivel12', Self.Nivel12 );
{$endif}
          AddString( 'Observaciones', Self.Observaciones );
          AddString( 'RegistroPatronal', Self.RegistroPatronal );
          AddBoolean( 'PagoPorFuera', Self.PagoPorFuera );
          AddDate( 'PagadoEn', Self.PagadoEn );
          AddInteger( 'Folio1', Self.Folio1 );
          AddInteger( 'Folio2', Self.Folio2 );
          AddInteger( 'Folio3', Self.Folio3 );
          AddInteger( 'Folio4', Self.Folio4 );
          AddInteger( 'Folio5', Self.Folio5 );
     end;
     inherited CargaParametros( Lista );
end;

procedure TDatosNominaClasificacion.DescargaParametros( Lista: TDataTransport);
begin
     inherited DescargaParametros( Lista );
     with Lista do
     begin
          Self.ZonaGeografica := ParamAsString( 'ZonaGeografica' );
          Self.Turno := ParamAsString( 'Turno' );
          Self.Puesto := ParamAsString( 'Puesto' );
          Self.Clasificacion := ParamAsString( 'Clasificacion' );
          Self.Nivel1 := ParamAsString( 'Nivel1' );
          Self.Nivel2 := ParamAsString( 'Nivel2' );
          Self.Nivel3 := ParamAsString( 'Nivel3' );
          Self.Nivel4 := ParamAsString( 'Nivel4' );
          Self.Nivel5 := ParamAsString( 'Nivel5' );
          Self.Nivel6 := ParamAsString( 'Nivel6' );
          Self.Nivel7 := ParamAsString( 'Nivel7' );
          Self.Nivel8 := ParamAsString( 'Nivel8' );
          Self.Nivel9 := ParamAsString( 'Nivel9' );
{$ifdef ACS}
          Self.Nivel10 := ParamAsString( 'Nivel10' );
          Self.Nivel11 := ParamAsString( 'Nivel11' );
          Self.Nivel12 := ParamAsString( 'Nivel12' );
{$endif}
          Self.Observaciones := ParamAsString( 'Observaciones' );
          Self.RegistroPatronal := ParamAsString( 'RegistroPatronal' );
          Self.PagoPorFuera := ParamAsBoolean( 'PagoPorFuera' );
          Self.PagadoEn := ParamAsDate( 'PagadoEn' );
          Self.Folio1 := ParamAsInteger( 'Folio1' );
          Self.Folio2 := ParamAsInteger( 'Folio2' );
          Self.Folio3 := ParamAsInteger( 'Folio3' );
          Self.Folio4 := ParamAsInteger( 'Folio4' );
          Self.Folio5 := ParamAsInteger( 'Folio5' );
     end;
end;

{ ******** TDatosEmpleadoInformacion ******* }

procedure TDatosEmpleadoInformacion.CargaParametros( Lista: TDataTransport );
begin
     inherited CargaParametros( Lista );
     with Lista do
     begin
          AddString( 'ApellidoPaterno', Self.ApellidoPaterno );
          AddString( 'ApellidoMaterno', Self.ApellidoMaterno );
          AddString( 'Nombres', Self.Nombres );
          AddString( 'Sexo', Self.Sexo );
          AddDate( 'NacimientoFecha', Self.NacimientoFecha );
          AddString( 'NacimientoLugar', Self.NacimientoLugar );
          AddString( 'NacimientoEntidad', Self.NacimientoEntidad );
          AddString( 'RFC', Self.RFC );
          AddString( 'IMSSAfiliacion', Self.IMSSAfiliacion );
          AddString( 'IMSSClinica', Self.IMSSClinica );
          AddString( 'CURP', Self.CURP );
          AddString( 'Nacionalidad', Self.Nacionalidad );
          AddBoolean( 'TienePasaporte', Self.FTienePasaporte );
          AddString( 'EstadoCivil', Self.EstadoCivil );
          AddString( 'DatosMatrimonio', Self.DatosMatrimonio );
          AddString( 'Transporte', Self.Transporte );
          AddString( 'ResidenciaCalle', Self.ResidenciaCalle );
          AddString( 'ResidenciaColoniaCodigo', Self.ResidenciaColoniaCodigo );
          AddString( 'ResidenciaColoniaNombre', Self.ResidenciaColoniaNombre );
          AddString( 'ResidenciaCodigoPostal', Self.ResidenciaCodigoPostal );
          AddString( 'ResidenciaCiudad', Self.ResidenciaCiudad );
          AddString( 'ResidenciaCiudadZona', Self.ResidenciaCiudadZona );
          AddString( 'ResidenciaEstado', Self.ResidenciaEstado );
          AddString( 'ResidenciaTelefono', Self.ResidenciaTelefono );
          AddFloat( 'ResidenciaYears', Self.ResidenciaYears );
          AddFloat( 'ResidenciaMonths', Self.ResidenciaMonths );
          AddString( 'ViveCon', Self.ViveCon );
          AddString( 'ViveEn', Self.ViveEn );
          AddString( 'GradoEstudios', Self.GradoEstudios );
          AddString( 'CarreraTerminada', Self.CarreraTerminada );
          AddBoolean( 'EstudiaActualmente', Self.EstudiaActualmente );
          AddString( 'CarreraHorario', Self.CarreraHorario );
          AddBoolean( 'HablaOtroIdioma', Self.HablaOtroIdioma );
          AddString( 'IdiomaDominio', Self.IdiomaDominio );
          AddString( 'MaquinasConocidas', Self.MaquinasConocidas );
          AddString( 'Experiencia', Self.Experiencia );
          AddInteger( 'INFONAVITTipoPrestamo', Ord( Self.INFONAVITTipoPrestamo ) );
          AddString( 'INFONAVITCredito', Self.INFONAVITCredito );
          AddFloat( 'INFONAVITAmortizacion', Self.INFONAVITAmortizacion );
          AddFloat( 'INFONAVITTasaAnterior', Self.INFONAVITTasaAnterior );
          AddBoolean( 'INFONAVITMantenimiento', Self.INFONAVITMantenimiento );
          AddDate( 'INFONAVITInicioCredito', Self.INFONAVITInicioCredito );
          AddDate( 'EvaluacionFecha', Self.EvaluacionFecha );
          AddFloat( 'EvaluacionResultado', Self.EvaluacionResultado );
          AddDate( 'EvaluacionProxima', Self.EvaluacionProxima );
          AddBoolean( 'AsistenciaCheca', Self.AsistenciaCheca );
          AddString( 'AsistenciaLetraCredencial', Self.AsistenciaLetraCredencial );
          AddString( 'AsistenciaNumeroTarjeta', Self.AsistenciaNumeroTarjeta );
          AddString( 'BancaElectronica', Self.BancaElectronica );
          AddString( 'Confidencialidad', Self.Confidencialidad );
          AddDate( 'ExtraFecha1', Self.ExtraFecha1 );
          AddDate( 'ExtraFecha2', Self.ExtraFecha2 );
          AddDate( 'ExtraFecha3', Self.ExtraFecha3 );
          AddDate( 'ExtraFecha4', Self.ExtraFecha4 );
          AddDate( 'ExtraFecha5', Self.ExtraFecha5 );
          AddDate( 'ExtraFecha6', Self.ExtraFecha6 );
          AddDate( 'ExtraFecha7', Self.ExtraFecha7 );
          AddDate( 'ExtraFecha8', Self.ExtraFecha8 );
          AddBoolean( 'ExtraLogico1', Self.ExtraLogico1 );
          AddBoolean( 'ExtraLogico2', Self.ExtraLogico2 );
          AddBoolean( 'ExtraLogico3', Self.ExtraLogico3 );
          AddBoolean( 'ExtraLogico4', Self.ExtraLogico4 );
          AddBoolean( 'ExtraLogico5', Self.ExtraLogico5 );
          AddBoolean( 'ExtraLogico6', Self.ExtraLogico6 );
          AddBoolean( 'ExtraLogico7', Self.ExtraLogico7 );
          AddBoolean( 'ExtraLogico8', Self.ExtraLogico8 );
          AddFloat( 'ExtraNumero1', Self.ExtraNumero1 );
          AddFloat( 'ExtraNumero2', Self.ExtraNumero2 );
          AddFloat( 'ExtraNumero3', Self.ExtraNumero3 );
          AddFloat( 'ExtraNumero4', Self.ExtraNumero4 );
          AddFloat( 'ExtraNumero5', Self.ExtraNumero5 );
          AddFloat( 'ExtraNumero6', Self.ExtraNumero6 );
          AddFloat( 'ExtraNumero7', Self.ExtraNumero7 );
          AddFloat( 'ExtraNumero8', Self.ExtraNumero8 );
          AddFloat( 'ExtraNumero9', Self.ExtraNumero9 );
          AddFloat( 'ExtraNumero10', Self.ExtraNumero10 );
          AddFloat( 'ExtraNumero11', Self.ExtraNumero11 );
          AddFloat( 'ExtraNumero12', Self.ExtraNumero12 );
          AddFloat( 'ExtraNumero13', Self.ExtraNumero13 );
          AddFloat( 'ExtraNumero14', Self.ExtraNumero14 );
          AddFloat( 'ExtraNumero15', Self.ExtraNumero15 );
          AddFloat( 'ExtraNumero16', Self.ExtraNumero16 );
          AddFloat( 'ExtraNumero17', Self.ExtraNumero17 );
          AddFloat( 'ExtraNumero18', Self.ExtraNumero18 );
          AddString( 'ExtraTabla1', Self.ExtraTabla1 );
          AddString( 'ExtraTabla2', Self.ExtraTabla2 );
          AddString( 'ExtraTabla3', Self.ExtraTabla3 );
          AddString( 'ExtraTabla4', Self.ExtraTabla4 );
          AddString( 'ExtraTabla5', Self.ExtraTabla5 );
          AddString( 'ExtraTabla6', Self.ExtraTabla6 );
          AddString( 'ExtraTabla7', Self.ExtraTabla7 );
          AddString( 'ExtraTabla8', Self.ExtraTabla8 );
          AddString( 'ExtraTabla9', Self.ExtraTabla9 );
          AddString( 'ExtraTabla10', Self.ExtraTabla10 );
          AddString( 'ExtraTabla11', Self.ExtraTabla11 );
          AddString( 'ExtraTabla12', Self.ExtraTabla12 );
          AddString( 'ExtraTabla13', Self.ExtraTabla13 );
          AddString( 'ExtraTabla14', Self.ExtraTabla14 );
          AddString( 'ExtraTexto1', Self.ExtraTexto1 );
          AddString( 'ExtraTexto2', Self.ExtraTexto2 );
          AddString( 'ExtraTexto3', Self.ExtraTexto3 );
          AddString( 'ExtraTexto4', Self.ExtraTexto4 );
          AddString( 'ExtraTexto5', Self.ExtraTexto5 );
          AddString( 'ExtraTexto6', Self.ExtraTexto6 );
          AddString( 'ExtraTexto7', Self.ExtraTexto7 );
          AddString( 'ExtraTexto8', Self.ExtraTexto8 );
          AddString( 'ExtraTexto9', Self.ExtraTexto9 );
          AddString( 'ExtraTexto10', Self.ExtraTexto10 );
          AddString( 'ExtraTexto11', Self.ExtraTexto11 );
          AddString( 'ExtraTexto12', Self.ExtraTexto12 );
          AddString( 'ExtraTexto13', Self.ExtraTexto13 );
          AddString( 'ExtraTexto14', Self.ExtraTexto14 );
          AddString( 'ExtraTexto15', Self.ExtraTexto15 );
          AddString( 'ExtraTexto16', Self.ExtraTexto16 );
          AddString( 'ExtraTexto17', Self.ExtraTexto17 );
          AddString( 'ExtraTexto18', Self.ExtraTexto18 );
          AddString( 'ExtraTexto19', Self.ExtraTexto19 );
     end;
end;

procedure TDatosEmpleadoInformacion.DescargaParametros( Lista: TDataTransport );
begin
     inherited DescargaParametros( Lista );
     with Lista do
     begin
          Self.ApellidoPaterno := ParamAsString( 'ApellidoPaterno' );
          Self.ApellidoMaterno := ParamAsString( 'ApellidoMaterno' );
          Self.Nombres := ParamAsString( 'Nombres' );
          Self.Sexo := ParamAsString( 'Sexo' );
          Self.NacimientoFecha := ParamAsDate( 'NacimientoFecha' );
          Self.NacimientoLugar := ParamAsString( 'NacimientoLugar' );
          Self.NacimientoEntidad := ParamAsString( 'NacimientoEntidad' );
          Self.RFC := ParamAsString( 'RFC' );
          Self.IMSSAfiliacion := ParamAsString( 'IMSSAfiliacion' );
          Self.IMSSClinica := ParamAsString( 'IMSSClinica' );
          Self.CURP := ParamAsString( 'CURP' );
          Self.Nacionalidad := ParamAsString( 'Nacionalidad' );
          Self.TienePasaporte := ParamAsBoolean( 'TienePasaporte' );
          Self.EstadoCivil := ParamAsString( 'EstadoCivil' );
          Self.DatosMatrimonio := ParamAsString( 'DatosMatrimonio' );
          Self.Transporte := ParamAsString( 'Transporte' );
          Self.ResidenciaCalle := ParamAsString( 'ResidenciaCalle' );
          Self.ResidenciaColoniaCodigo := ParamAsString( 'ResidenciaColoniaCodigo' );
          Self.ResidenciaColoniaNombre := ParamAsString( 'ResidenciaColoniaNombre' );
          Self.ResidenciaCodigoPostal := ParamAsString( 'ResidenciaCodigoPostal' );
          Self.ResidenciaCiudad := ParamAsString( 'ResidenciaCiudad' );
          Self.ResidenciaCiudadZona := ParamAsString( 'ResidenciaCiudadZona' );
          Self.ResidenciaEstado := ParamAsString( 'ResidenciaEstado' );
          Self.ResidenciaTelefono := ParamAsString( 'ResidenciaTelefono' );
          Self.ResidenciaYears := ParamAsFloat( 'ResidenciaYears' );
          Self.ResidenciaMonths := ParamAsFloat( 'ResidenciaMonths' );
          Self.ViveCon := ParamAsString( 'ViveCon' );
          Self.ViveEn := ParamAsString( 'ViveEn' );
          Self.GradoEstudios := ParamAsString( 'GradoEstudios' );
          Self.CarreraTerminada := ParamAsString( 'CarreraTerminada' );
          Self.EstudiaActualmente := ParamAsBoolean( 'EstudiaActualmente' );
          Self.CarreraHorario := ParamAsString( 'CarreraHorario' );
          Self.HablaOtroIdioma := ParamAsBoolean( 'HablaOtroIdioma' );
          Self.IdiomaDominio := ParamAsString( 'IdiomaDominio' );
          Self.MaquinasConocidas := ParamAsString( 'MaquinasConocidas' );
          Self.Experiencia := ParamAsString( 'Experiencia' );
          Self.INFONAVITTipoPrestamo := eTipoInfonavit( ParamAsInteger( 'INFONAVITTipoPrestamo' ) );
          Self.INFONAVITCredito := ParamAsString( 'INFONAVITCredito' );
          Self.INFONAVITAmortizacion := ParamAsFloat( 'INFONAVITAmortizacion' );
          Self.INFONAVITTasaAnterior := ParamAsFloat( 'INFONAVITTasaAnterior' );
          Self.INFONAVITMantenimiento := ParamAsBoolean( 'INFONAVITMantenimiento' );
          Self.INFONAVITInicioCredito := ParamAsDate( 'INFONAVITInicioCredito' );
          Self.EvaluacionFecha := ParamAsDate( 'EvaluacionFecha' );
          Self.EvaluacionResultado := ParamAsFloat( 'EvaluacionResultado' );
          Self.EvaluacionProxima := ParamAsDate( 'EvaluacionProxima' );
          Self.AsistenciaCheca := ParamAsBoolean( 'AsistenciaCheca' );
          Self.AsistenciaLetraCredencial := ParamAsString( 'AsistenciaLetraCredencial' );
          Self.AsistenciaNumeroTarjeta := ParamAsString( 'AsistenciaNumeroTarjeta' );
          Self.BancaElectronica := ParamAsString( 'BancaElectronica' );
          Self.Confidencialidad := ParamAsString( 'Confidencialidad' );
          Self.ExtraFecha1 := ParamAsDate( 'ExtraFecha1' );
          Self.ExtraFecha2 := ParamAsDate( 'ExtraFecha2' );
          Self.ExtraFecha3 := ParamAsDate( 'ExtraFecha3' );
          Self.ExtraFecha4 := ParamAsDate( 'ExtraFecha4' );
          Self.ExtraFecha5 := ParamAsDate( 'ExtraFecha5' );
          Self.ExtraFecha6 := ParamAsDate( 'ExtraFecha6' );
          Self.ExtraFecha7 := ParamAsDate( 'ExtraFecha7' );
          Self.ExtraFecha8 := ParamAsDate( 'ExtraFecha8' );
          Self.ExtraLogico1 := ParamAsBoolean( 'ExtraLogico1' );
          Self.ExtraLogico2 := ParamAsBoolean( 'ExtraLogico2' );
          Self.ExtraLogico3 := ParamAsBoolean( 'ExtraLogico3' );
          Self.ExtraLogico4 := ParamAsBoolean( 'ExtraLogico4' );
          Self.ExtraLogico5 := ParamAsBoolean( 'ExtraLogico5' );
          Self.ExtraLogico6 := ParamAsBoolean( 'ExtraLogico6' );
          Self.ExtraLogico7 := ParamAsBoolean( 'ExtraLogico7' );
          Self.ExtraLogico8 := ParamAsBoolean( 'ExtraLogico8' );
          Self.ExtraNumero1 := ParamAsFloat( 'ExtraNumero1' );
          Self.ExtraNumero2 := ParamAsFloat( 'ExtraNumero2' );
          Self.ExtraNumero3 := ParamAsFloat( 'ExtraNumero3' );
          Self.ExtraNumero4 := ParamAsFloat( 'ExtraNumero4' );
          Self.ExtraNumero5 := ParamAsFloat( 'ExtraNumero5' );
          Self.ExtraNumero6 := ParamAsFloat( 'ExtraNumero6' );
          Self.ExtraNumero7 := ParamAsFloat( 'ExtraNumero7' );
          Self.ExtraNumero8 := ParamAsFloat( 'ExtraNumero8' );
          Self.ExtraNumero9 := ParamAsFloat( 'ExtraNumero9' );
          Self.ExtraNumero10 := ParamAsFloat( 'ExtraNumero10' );
          Self.ExtraNumero11 := ParamAsFloat( 'ExtraNumero11' );
          Self.ExtraNumero12 := ParamAsFloat( 'ExtraNumero12' );
          Self.ExtraNumero13 := ParamAsFloat( 'ExtraNumero13' );
          Self.ExtraNumero14 := ParamAsFloat( 'ExtraNumero14' );
          Self.ExtraNumero15 := ParamAsFloat( 'ExtraNumero15' );
          Self.ExtraNumero16 := ParamAsFloat( 'ExtraNumero16' );
          Self.ExtraNumero17 := ParamAsFloat( 'ExtraNumero17' );
          Self.ExtraNumero18 := ParamAsFloat( 'ExtraNumero18' );
          Self.ExtraTabla1 := ParamAsString( 'ExtraTabla1' );
          Self.ExtraTabla2 := ParamAsString( 'ExtraTabla2' );
          Self.ExtraTabla3 := ParamAsString( 'ExtraTabla3' );
          Self.ExtraTabla4 := ParamAsString( 'ExtraTabla4' );
          Self.ExtraTabla5 := ParamAsString( 'ExtraTabla5' );
          Self.ExtraTabla6 := ParamAsString( 'ExtraTabla6' );
          Self.ExtraTabla7 := ParamAsString( 'ExtraTabla7' );
          Self.ExtraTabla8 := ParamAsString( 'ExtraTabla8' );
          Self.ExtraTabla9 := ParamAsString( 'ExtraTabla9' );
          Self.ExtraTabla10 := ParamAsString( 'ExtraTabla10' );
          Self.ExtraTabla11 := ParamAsString( 'ExtraTabla11' );
          Self.ExtraTabla12 := ParamAsString( 'ExtraTabla12' );
          Self.ExtraTabla13 := ParamAsString( 'ExtraTabla13' );
          Self.ExtraTabla14 := ParamAsString( 'ExtraTabla14' );
          Self.ExtraTexto1 := ParamAsString( 'ExtraTexto1' );
          Self.ExtraTexto2 := ParamAsString( 'ExtraTexto2' );
          Self.ExtraTexto3 := ParamAsString( 'ExtraTexto3' );
          Self.ExtraTexto4 := ParamAsString( 'ExtraTexto4' );
          Self.ExtraTexto5 := ParamAsString( 'ExtraTexto5' );
          Self.ExtraTexto6 := ParamAsString( 'ExtraTexto6' );
          Self.ExtraTexto7 := ParamAsString( 'ExtraTexto7' );
          Self.ExtraTexto8 := ParamAsString( 'ExtraTexto8' );
          Self.ExtraTexto9 := ParamAsString( 'ExtraTexto9' );
          Self.ExtraTexto10 := ParamAsString( 'ExtraTexto10' );
          Self.ExtraTexto11 := ParamAsString( 'ExtraTexto11' );
          Self.ExtraTexto12 := ParamAsString( 'ExtraTexto12' );
          Self.ExtraTexto13 := ParamAsString( 'ExtraTexto13' );
          Self.ExtraTexto14 := ParamAsString( 'ExtraTexto14' );
          Self.ExtraTexto15 := ParamAsString( 'ExtraTexto15' );
          Self.ExtraTexto16 := ParamAsString( 'ExtraTexto16' );
          Self.ExtraTexto17 := ParamAsString( 'ExtraTexto17' );
          Self.ExtraTexto18 := ParamAsString( 'ExtraTexto18' );
          Self.ExtraTexto19 := ParamAsString( 'ExtraTexto19' );
     end;
end;

{ ****** TDatosEmpleadoAlta ******* }

procedure TDatosEmpleadoAlta.CargaParametros( Lista: TDataTransport );
begin
     inherited CargaParametros( Lista );
     with Lista do
     begin
          AddBoolean( 'UsaTabulador', Self.UsaTabulador );
          AddFloat( 'SalarioDiario', Self.SalarioDiario );
          AddFloat( 'PromedioVariables', Self.PromedioVariables );
          AddString( 'ZonaGeografica', Self.ZonaGeografica );
          AddDate( 'AvisoIMSS', Self.AvisoIMSS );
          AddDate( 'FechaAntiguedad', Self.FechaAntiguedad );
          AddDate( 'FechaIngreso', Self.FechaIngreso );
          AddString( 'Turno', Self.Turno );
          AddString( 'Puesto', Self.Puesto );
          AddString( 'Clasificacion', Self.Clasificacion );
          AddString( 'Nivel1', Self.Nivel1 );
          AddString( 'Nivel2', Self.Nivel2 );
          AddString( 'Nivel3', Self.Nivel3 );
          AddString( 'Nivel4', Self.Nivel4 );
          AddString( 'Nivel5', Self.Nivel5 );
          AddString( 'Nivel6', Self.Nivel6 );
          AddString( 'Nivel7', Self.Nivel7 );
          AddString( 'Nivel8', Self.Nivel8 );
          AddString( 'Nivel9', Self.Nivel9 );
{$ifdef ACS}
          AddString( 'Nivel10', Self.Nivel10 );
          AddString( 'Nivel11', Self.Nivel11 );
          AddString( 'Nivel12', Self.Nivel12 );
{$endif}
          AddString( 'ContratoTipo', Self.ContratoTipo );
          AddDate( 'ContratoInicio', Self.ContratoInicio );
          AddString( 'TablaPrestaciones', Self.TablaPrestaciones );
          AddString( 'RegistroPatronal', Self.RegistroPatronal );
          AddString( 'PercepFijas', Self.PercepFijas );
          AddString( 'Padre', Self.Padre );
          AddString( 'Madre', Self.Madre );
          AddDate( 'VacacionCierre', Self.VacacionCierre );
          AddFloat( 'VacacionDiasPagar', Self.VacacionDiasPagar );
          AddFloat( 'VacacionDiasGozar', Self.VacacionDiasGozar );
     end;
end;

procedure TDatosEmpleadoAlta.DescargaParametros( Lista: TDataTransport );
begin
     with Lista do
     begin
          Self.UsaTabulador := ParamAsBoolean( 'UsaTabulador' );
          Self.SalarioDiario := ParamAsFloat( 'SalarioDiario' );
          Self.PromedioVariables := ParamAsFloat( 'PromedioVariables' );
          Self.ZonaGeografica := ParamAsString( 'ZonaGeografica' );
          Self.AvisoIMSS := ParamAsDate( 'AvisoIMSS' );
          Self.FechaAntiguedad := ParamAsDate( 'FechaAntiguedad' );
          Self.FechaIngreso := ParamAsDate( 'FechaIngreso' );
          Self.Turno := ParamAsString( 'Turno' );
          Self.Puesto := ParamAsString( 'Puesto' );
          Self.Clasificacion := ParamAsString( 'Clasificacion' );
          Self.Nivel1 := ParamAsString( 'Nivel1' );
          Self.Nivel2 := ParamAsString( 'Nivel2' );
          Self.Nivel3 := ParamAsString( 'Nivel3' );
          Self.Nivel4 := ParamAsString( 'Nivel4' );
          Self.Nivel5 := ParamAsString( 'Nivel5' );
          Self.Nivel6 := ParamAsString( 'Nivel6' );
          Self.Nivel7 := ParamAsString( 'Nivel7' );
          Self.Nivel8 := ParamAsString( 'Nivel8' );
          Self.Nivel9 := ParamAsString( 'Nivel9' );
{$ifdef ACS}
          Self.Nivel10 := ParamAsString( 'Nivel10' );
          Self.Nivel11 := ParamAsString( 'Nivel11' );
          Self.Nivel12 := ParamAsString( 'Nivel12' );
{$endif}
          Self.ContratoTipo := ParamAsString( 'ContratoTipo' );
          Self.ContratoInicio := ParamAsDate( 'ContratoInicio' );
          Self.TablaPrestaciones := ParamAsString( 'TablaPrestaciones' );
          Self.RegistroPatronal := ParamAsString( 'RegistroPatronal' );
          Self.PercepFijas := ParamAsString( 'PercepFijas' );
          Self.Padre := ParamAsString( 'Padre' );
          Self.Madre := ParamAsString( 'Madre' );
          Self.VacacionCierre := ParamAsDate( 'VacacionCierre' );
          Self.VacacionDiasPagar := ParamAsFloat( 'VacacionDiasPagar' );
          Self.VacacionDiasGozar := ParamAsFloat( 'VacacionDiasGozar' );
     end;
     inherited DescargaParametros( Lista );
end;

{ ****** TDatosImagen ***** }

procedure TDatosImagen.CargaParametros(Lista: TDataTransport);
begin
     with Lista do
     begin
          AddString( 'Tipo', Self.Tipo );
          AddString( 'Observaciones', Self.Observaciones );
          AddBlob( 'Imagen', Self.Imagen );
     end;
     inherited CargaParametros( Lista );
end;

procedure TDatosImagen.DescargaParametros(Lista: TDataTransport);
begin
     inherited DescargaParametros( Lista );
     with Lista do
     begin
          Self.Tipo := ParamAsString( 'Tipo' );
          Self.Observaciones := ParamAsString( 'Observaciones' );
          Self.Imagen := ParamAsBlob( 'Imagen' );
     end;
end;

{ ******* TDatosKardex ***** }

constructor TDatosKardex.Create;
begin
     inherited Create;
     FFecha := TDateKey.Create;
     FTipo := ZetaCommonClasses.VACIO;
end;

destructor TDatosKardex.Destroy;
begin
     FreeAndNil( FFecha );
     inherited Destroy;
end;

procedure TDatosKardex.CargaParametros(Lista: TDataTransport);
begin
     Self.Fecha.CargaParametros( Lista, 'Fecha' );
     with Lista do
     begin
          AddString( 'Notas', Self.Notas );
          AddString( 'Observaciones', Self.Observaciones );
          AddBoolean( 'Editando', Self.Editando );
     end;
     inherited CargaParametros( Lista );
end;

procedure TDatosKardex.DescargaParametros(Lista: TDataTransport);
begin
     inherited DescargaParametros( Lista );
     Self.Fecha.DescargaParametros( Lista, 'Fecha' );
     with Lista do
     begin
          Self.Notas := ParamAsString( 'Notas' );
          Self.Observaciones := ParamAsString( 'Observaciones' );
          Self.Editando := ParamAsBoolean( 'Editando' );
     end;
end;

{ ******* TDatosKardexGeneral ***** }

constructor TDatosKardexGeneral.Create;
begin
     inherited Create;
     FTipoGral := TStringKey.Create;
end;

destructor TDatosKardexGeneral.Destroy;
begin
     FreeAndNil( FTipoGral );
     inherited Destroy;
end;

procedure TDatosKardexGeneral.CargaParametros( Lista: TDataTransport );
begin
     Self.TipoGral.CargaParametros( Lista, 'TipoGral' );
     Lista.AddFloat( 'Monto', Self.Monto );
     inherited CargaParametros( Lista );
end;

procedure TDatosKardexGeneral.DescargaParametros( Lista: TDataTransport );
begin
     inherited DescargaParametros( Lista );
     Self.TipoGral.DesCargaParametros( Lista, 'TipoGral' );
     FTipo := Self.TipoGral.OldValue;
     Self.Monto := Lista.ParamAsFloat( 'Monto' );
end;

{ ****** TDatosKardexBaja ****** }

constructor TDatosKardexBaja.Create;
begin
     inherited Create;
     FPeriodoPago := TDatosPeriodo.Create;
     FTipo := ZetaCommonClasses.K_T_BAJA;
end;

destructor TDatosKardexBaja.Destroy;
begin
     FreeAndNil( FPeriodoPago );
     inherited Destroy;
end;

procedure TDatosKardexBaja.CargaParametros(Lista: TDataTransport);
begin
     with Lista do
     begin
          AddString( 'Motivo', Self.Motivo );
          AddDate( 'BajaIMSS', Self.BajaIMSS );
     end;
     PeriodoPago.CargaParametros( Lista );
     inherited CargaParametros( Lista );
end;

procedure TDatosKardexBaja.DescargaParametros(Lista: TDataTransport);
begin
     inherited DescargaParametros( Lista );
     with Lista do
     begin
          Self.Motivo := ParamAsString( 'Motivo' );
          Self.BajaIMSS :=  ParamAsDate( 'BajaIMSS' );
     end;
     PeriodoPago.DescargaParametros( Lista );
end;

{ ********* TDatosKardexReingreso ********** }

constructor TDatosKardexReingreso.Create;
begin
     inherited Create;
     FDatosEmpleado := TDatosEmpleadoAlta.Create;
     FTipo := ZetaCommonClasses.K_T_ALTA;
end;

destructor TDatosKardexReingreso.Destroy;
begin
     FreeAndNil( FDatosEmpleado );
     inherited Destroy;
end;

procedure TDatosKardexReingreso.CargaParametros( Lista: TDataTransport );
begin
     DatosEmpleado.CargaParametros( Lista );
     inherited CargaParametros( Lista );
end;

procedure TDatosKardexReingreso.DescargaParametros( Lista: TDataTransport );
begin
     inherited DescargaParametros( Lista );
     DatosEmpleado.DescargaParametros( Lista );
end;

{ ******* TDatosKardexCambioSalario ****** }

constructor TDatosKardexCambioSalario.Create;
begin
     inherited Create;
     FTipo := ZetaCommonClasses.K_T_CAMBIO;
end;

procedure TDatosKardexCambioSalario.CargaParametros( Lista: TDataTransport );
begin
     with Lista do
     begin
          AddBoolean( 'UsaTabulador', Self.UsaTabulador );
          AddFloat( 'SalarioDiario', Self.SalarioDiario );
          AddFloat( 'PromedioVariables', Self.PromedioVariables );
          AddString( 'ZonaGeografica', Self.ZonaGeografica );
          AddDate( 'AvisoIMSS', Self.AvisoIMSS );
          AddString( 'PercepFijas', Self.PercepFijas );
     end;
     inherited CargaParametros( Lista );
end;

procedure TDatosKardexCambioSalario.DescargaParametros( Lista: TDataTransport );
begin
     inherited DescargaParametros( Lista );
     with Lista do
     begin
          Self.UsaTabulador := ParamAsBoolean( 'UsaTabulador' );
          Self.SalarioDiario := ParamAsFloat( 'SalarioDiario' );
          Self.PromedioVariables := ParamAsFloat( 'PromedioVariables' );
          Self.ZonaGeografica := ParamAsString( 'ZonaGeografica' );
          Self.AvisoIMSS := ParamAsDate( 'AvisoIMSS' );
          Self.PercepFijas := ParamAsString( 'PercepFijas' );
     end;
end;

{ ******* TDatosKardexCambioTurno ****** }

constructor TDatosKardexCambioTurno.Create;
begin
     inherited Create;
     FTipo := ZetaCommonClasses.K_T_TURNO;
end;

procedure TDatosKardexCambioTurno.CargaParametros( Lista: TDataTransport );
begin
     with Lista do
     begin
          AddString( 'Turno', Self.Turno );
     end;
     inherited CargaParametros( Lista );
end;

procedure TDatosKardexCambioTurno.DescargaParametros( Lista: TDataTransport );
begin
     inherited DescargaParametros( Lista );
     with Lista do
     begin
          Self.Turno := ParamAsString( 'Turno' );
     end;
end;

{ ******* TDatosKardexCambioPuesto ****** }

constructor TDatosKardexCambioPuesto.Create;
begin
     inherited Create;
     FTipo := ZetaCommonClasses.K_T_PUESTO;
end;

procedure TDatosKardexCambioPuesto.CargaParametros( Lista: TDataTransport );
begin
     with Lista do
     begin
          AddString( 'Puesto', Self.Puesto );
          AddString( 'Clasificacion', Self.Clasificacion );
     end;
     inherited CargaParametros( Lista );
end;

procedure TDatosKardexCambioPuesto.DescargaParametros( Lista: TDataTransport );
begin
     inherited DescargaParametros( Lista );
     with Lista do
     begin
          Self.Puesto := ParamAsString( 'Puesto' );
          Self.Clasificacion := ParamAsString( 'Clasificacion' );
     end;
end;

{ ******* TDatosKardexCambioArea ****** }

constructor TDatosKardexCambioArea.Create;
begin
     inherited Create;
     FTipo := ZetaCommonClasses.K_T_AREA;
end;

procedure TDatosKardexCambioArea.CargaParametros( Lista: TDataTransport );
begin
     with Lista do
     begin
          AddString( 'Nivel1', Self.Nivel1 );
          AddString( 'Nivel2', Self.Nivel2 );
          AddString( 'Nivel3', Self.Nivel3 );
          AddString( 'Nivel4', Self.Nivel4 );
          AddString( 'Nivel5', Self.Nivel5 );
          AddString( 'Nivel6', Self.Nivel6 );
          AddString( 'Nivel7', Self.Nivel7 );
          AddString( 'Nivel8', Self.Nivel8 );
          AddString( 'Nivel9', Self.Nivel9 );
{$ifdef ACS}
          AddString( 'Nivel10', Self.Nivel10 );
          AddString( 'Nivel11', Self.Nivel11 );
          AddString( 'Nivel12', Self.Nivel12 );
{$endif}
     end;
     inherited CargaParametros( Lista );
end;

procedure TDatosKardexCambioArea.DescargaParametros( Lista: TDataTransport );
begin
     inherited DescargaParametros( Lista );
     with Lista do
     begin
          Self.Nivel1 := ParamAsString( 'Nivel1' );
          Self.Nivel2 := ParamAsString( 'Nivel2' );
          Self.Nivel3 := ParamAsString( 'Nivel3' );
          Self.Nivel4 := ParamAsString( 'Nivel4' );
          Self.Nivel5 := ParamAsString( 'Nivel5' );
          Self.Nivel6 := ParamAsString( 'Nivel6' );
          Self.Nivel7 := ParamAsString( 'Nivel7' );
          Self.Nivel8 := ParamAsString( 'Nivel8' );
          Self.Nivel9 := ParamAsString( 'Nivel9' );
{$ifdef ACS}
          Self.Nivel10 := ParamAsString( 'Nivel10' );
          Self.Nivel11 := ParamAsString( 'Nivel11' );
          Self.Nivel12 := ParamAsString( 'Nivel12' );
{$endif}
     end;
end;

{ ******* TDatosKardexCambioContrato ****** }

constructor TDatosKardexCambioContrato.Create;
begin
     inherited Create;
     FTipo := ZetaCommonClasses.K_T_RENOVA;
end;

procedure TDatosKardexCambioContrato.CargaParametros( Lista: TDataTransport );
begin
     with Lista do
     begin
          AddString( 'TipoContrato', TipoContrato );
     end;
     inherited CargaParametros( Lista );
end;

procedure TDatosKardexCambioContrato.DescargaParametros( Lista: TDataTransport );
begin
     inherited DescargaParametros( Lista );
     with Lista do
     begin
          TipoContrato := ParamAsString( 'TipoContrato' );
     end;
end;

{ ******* TDatosKardexCambioTablaPrestaciones ****** }

constructor TDatosKardexCambioTablaPrestaciones.Create;
begin
     inherited Create;
     FTipo := ZetaCommonClasses.K_T_PRESTA;
end;

procedure TDatosKardexCambioTablaPrestaciones.CargaParametros( Lista: TDataTransport );
begin
     with Lista do
     begin
          AddString( 'TablaPrestaciones', TablaPrestaciones );
     end;
     inherited CargaParametros( Lista );
end;

procedure TDatosKardexCambioTablaPrestaciones.DescargaParametros( Lista: TDataTransport );
begin
     inherited DescargaParametros( Lista );
     with Lista do
     begin
          TablaPrestaciones := ParamAsString( 'TablaPrestaciones' );
     end;
end;

{ ******* TDatosKardexCambioMultiple ****** }

constructor TDatosKardexCambioMultiple.Create;
begin
     inherited Create;
     FCambioTurno := TDatosKardexCambioTurno.Create;
     FCambioPuesto := TDatosKardexCambioPuesto.Create;
     FCambioArea := TDatosKardexCambioArea.Create;
     FCambioContrato := TDatosKardexCambioContrato.Create;
end;

destructor TDatosKardexCambioMultiple.Destroy;
begin
     FreeAndNil( FCambioContrato );
     FreeAndNil( FCambioArea );
     FreeAndNil( FCambioPuesto );
     FreeAndNil( FCambioTurno );
     inherited Destroy;
end;

procedure TDatosKardexCambioMultiple.CargaParametros( Lista: TDataTransport );
begin
     CambioTurno.CargaParametros( Lista );
     CambioPuesto.CargaParametros( Lista );
     CambioArea.CargaParametros( Lista );
     CambioContrato.CargaParametros( Lista );
     inherited CargaParametros( Lista );
end;

procedure TDatosKardexCambioMultiple.DescargaParametros( Lista: TDataTransport );
begin
     inherited DescargaParametros( Lista );
     CambioTurno.DescargaParametros( Lista );
     CambioPuesto.DescargaParametros( Lista );
     CambioArea.DescargaParametros( Lista );
     CambioContrato.DescargaParametros( Lista );
end;

{ ********** TDatosIncidencia ********* }

constructor TDatosIncidencia.Create;
begin
     inherited Create;
     FInicio := TDateKey.Create;
end;

destructor TDatosIncidencia.Destroy;
begin
     FreeAndNil( FInicio );
     inherited Destroy;
end;

procedure TDatosIncidencia.CargaParametros(Lista: TDataTransport);
begin
     Self.Inicio.CargaParametros( Lista, 'Inicio' );
     with Lista do
     begin
          AddDate( 'InicioAjustado', Self.InicioAjustado );
          AddDate( 'RegresoAjustado', Self.RegresoAjustado );
          AddInteger( 'StatusEmpleado', Ord( Self.StatusEmpleado ) );
     end;
     inherited CargaParametros( Lista );
end;

procedure TDatosIncidencia.DescargaParametros(Lista: TDataTransport);
begin
     inherited DescargaParametros( Lista );
     Self.Inicio.DescargaParametros( Lista, 'Inicio' );
     with Lista do
     begin
          Self.StatusEmpleado := eStatusEmpleado( ParamAsInteger( 'StatusEmpleado' ) );
          Self.InicioAjustado := ParamAsDate( 'InicioAjustado' );
          Self.RegresoAjustado := ParamAsDate( 'RegresoAjustado' );
     end;
end;

{ ******* TDatosVacacionMovimiento ******* }

constructor TDatosVacacionMovimiento.Create;
begin
     inherited Create;
     FPeriodoPago := TDatosPeriodo.Create;
end;

destructor TDatosVacacionMovimiento.Destroy;
begin
     FreeAndNil( FPeriodoPago );
     inherited Destroy;
end;

procedure TDatosVacacionMovimiento.CargaParametros( Lista: TDataTransport );
begin
     with Lista do
     begin
          AddDate( 'Regreso', Self.Regreso );
          AddFloat( 'DiasGozo', Self.DiasGozo );
          AddFloat( 'DiasPago', Self.DiasPago );
          AddString( 'Observaciones', Self.Observaciones );
          AddString( 'PeriodoTrabajado', Self.PeriodoTrabajado );
     end;
     PeriodoPago.CargaParametros( Lista );
     inherited CargaParametros( Lista );
end;

procedure TDatosVacacionMovimiento.DescargaParametros( Lista: TDataTransport );
begin
     inherited DescargaParametros( Lista );
     with Lista do
     begin
          Self.Regreso := ParamAsDate( 'Regreso' );
          Self.DiasGozo := ParamAsFloat( 'DiasGozo' );
          Self.DiasPago := ParamAsFloat( 'DiasPago' );
          Self.Observaciones := ParamAsString( 'Observaciones' );
          Self.PeriodoTrabajado := ParamAsString( 'PeriodoTrabajado' );
     end;
     PeriodoPago.DescargaParametros( Lista );
end;

{ ******* TDatosVacacionCierre ******* }

procedure TDatosVacacionCierre.CargaParametros( Lista: TDataTransport );
begin
     with Lista do
     begin
          AddDate( 'Fecha', Self.Fecha );
          AddFloat( 'DiasDerechoGozo', Self.DiasDerechoGozo );
          AddFloat( 'DiasDerechoPago', Self.DiasDerechoPago );
          AddString( 'Observaciones', Self.Observaciones );
     end;
     inherited CargaParametros( Lista );
end;

procedure TDatosVacacionCierre.DescargaParametros( Lista: TDataTransport );
begin
     inherited DescargaParametros( Lista );
     with Lista do
     begin
          Self.Fecha := ParamAsDate( 'Fecha' );
          Self.DiasDerechoGozo := ParamAsFloat( 'DiasDerechoGozo' );
          Self.DiasDerechoPago := ParamAsFloat( 'DiasDerechoPago' );
          Self.Observaciones := ParamAsString( 'Observaciones' );
     end;
end;

{ ****** TDatosIncapacidad ******** }

constructor TDatosIncapacidad.Create;
begin
     inherited Create;
     FPeriodoPago := TDatosPeriodo.Create;
end;

destructor TDatosIncapacidad.Destroy;
begin
     FreeAndNil( FPeriodoPago );
     inherited Destroy;
end;

procedure TDatosIncapacidad.CargaParametros( Lista: TDataTransport );
begin
     with Lista do
     begin
          AddFloat( 'Dias', Self.Dias );
          AddDate( 'FechaCaptura', Self.FechaCaptura );
          AddString( 'Observaciones', Self.Observaciones );
          AddString( 'Tipo', Self.Tipo );
          AddFloat( 'Numero', Self.Numero );
          AddInteger( 'MotivoMedico', Ord( Self.MotivoMedico ) );
          AddInteger( 'FinIncapacidad', Ord( Self.FinIncapacidad ) );
          AddFloat( 'TasaPermanente', Self.TasaPermanente );
          AddFloat( 'DiasSubsidio', Self.DiasSubsidio );
     end;
     PeriodoPago.CargaParametros( Lista );
     inherited CargaParametros( Lista );
end;

procedure TDatosIncapacidad.DescargaParametros( Lista: TDataTransport );
begin
     inherited DescargaParametros( Lista );
     with Lista do
     begin
          Self.Dias := ParamAsFloat( 'Dias' );
          Self.FechaCaptura := ParamAsDate( 'FechaCaptura' );
          Self.Observaciones := ParamAsString( 'Observaciones' );
          Self.Tipo := ParamAsString( 'Tipo' );
          Self.Numero := ParamAsFloat( 'Numero' );
          Self.MotivoMedico := eMotivoIncapacidad( ParamAsInteger( 'MotivoMedico' ) );
          Self.FinIncapacidad := eFinIncapacidad( ParamAsInteger( 'FinIncapacidad' ) );
          Self.TasaPermanente := ParamAsFloat( 'TasaPermanente' );
          Self.DiasSubsidio := ParamAsFloat( 'DiasSubsidio' );
     end;
     PeriodoPago.DescargaParametros( Lista );
end;

{ ******** TDatosPermiso ***** }

procedure TDatosPermiso.CargaParametros( Lista: TDataTransport );
begin
     with Lista do
     begin
          AddFloat( 'Dias', Self.Dias );
          AddDate( 'FechaCaptura', Self.FechaCaptura );
          AddString( 'Observaciones', Self.Observaciones );
          AddInteger( 'Clase', Ord( Self.Clase ) );
          AddString( 'Tipo', Self.Tipo );
          AddString( 'Referencia', Self.Referencia );
     end;
     inherited CargaParametros( Lista );
end;

procedure TDatosPermiso.DescargaParametros( Lista: TDataTransport );
begin
     inherited DescargaParametros( Lista );
     with Lista do
     begin
          Self.Dias := ParamAsFloat( 'Dias' );
          Self.FechaCaptura := ParamAsDate( 'FechaCaptura' );
          Self.Observaciones := ParamAsString( 'Observaciones' );
          Self.Clase := eTipoPermiso( ParamAsInteger( 'Clase' ) );
          Self.Tipo := ParamAsString( 'Tipo' );
          Self.Referencia := ParamAsString( 'Referencia' );
     end;
end;

{ *******  TDatosAhorroBase ***** }

constructor TDatosAhorroBase.Create;
begin
     inherited Create;
     FTipo := TStringKey.Create;
end;

destructor TDatosAhorroBase.Destroy;
begin
     FreeAndNil( FTipo );
     inherited Destroy;
end;

procedure TDatosAhorroBase.CargaParametros(Lista: TDataTransport);
begin
     Self.Tipo.CargaParametros( Lista, 'Tipo' );
     inherited CargaParametros( Lista );
end;

procedure TDatosAhorroBase.DescargaParametros(Lista: TDataTransport);
begin
     inherited DescargaParametros( Lista );
     Self.Tipo.DescargaParametros( Lista, 'Tipo' );
end;

{ ******* TDatosAhorro ******* }

procedure TDatosAhorro.CargaParametros( Lista: TDataTransport );
begin
     with Lista do
     begin
          AddDate( 'Inicio', Self.Inicio );
          AddInteger( 'Status', Ord( Status ) );
          AddString( 'Formula', Formula );
          AddFloat( 'SaldoInicial', SaldoInicial );
     end;
     inherited CargaParametros( Lista );
end;

procedure TDatosAhorro.DescargaParametros( Lista: TDataTransport );
begin
     inherited DescargaParametros( Lista );
     with Lista do
     begin
          Self.Inicio := ParamAsDate( 'Inicio' );
          Self.Status := eStatusAhorro( ParamAsInteger( 'Status' ) );
          Self.Formula := ParamAsString( 'Formula' );
          Self.SaldoInicial := ParamAsFloat( 'SaldoInicial' );
     end;
end;

{ ****** TDatosAhorroCargoAbono ***** }

constructor TDatosAhorroCargoAbono.Create;
begin
     inherited Create;
     FFecha := TDateKey.Create;
end;

destructor TDatosAhorroCargoAbono.Destroy;
begin
     FreeAndNil( FFecha );
     inherited Destroy;
end;

procedure TDatosAhorroCargoAbono.CargaParametros( Lista: TDataTransport );
begin
     Self.Fecha.CargaParametros( Lista, 'Fecha' );
     with Lista do
     begin
          AddDate( 'FechaCaptura', Self.FechaCaptura );
          AddFloat( 'Cargo', Cargo );
          AddFloat( 'Abono', Abono );
          AddString( 'Observaciones', Observaciones );
     end;
     inherited CargaParametros( Lista );
end;

procedure TDatosAhorroCargoAbono.DescargaParametros( Lista: TDataTransport );
begin
     inherited DescargaParametros( Lista );
     Self.Fecha.DescargaParametros( Lista, 'Fecha' );
     with Lista do
     begin
          Self.FechaCaptura := ParamAsDate( 'FechaCaptura' );
          Self.Cargo := ParamAsFloat( 'Cargo' );
          Self.Abono := ParamAsFloat( 'Abono' );
          Self.Observaciones := ParamAsString( 'Observaciones' );
     end;
end;

{ ******* TDatosPrestamoBase ***** }

constructor TDatosPrestamoBase.Create;
begin
     inherited Create;
     FTipo := TStringKey.Create;
     FReferencia := TStringKey.Create;
end;

destructor TDatosPrestamoBase.Destroy;
begin
     FreeAndNil( FReferencia );
     FreeAndNil( FTipo );
     inherited Destroy;
end;

procedure TDatosPrestamoBase.CargaParametros(Lista: TDataTransport);
begin
     Self.Tipo.CargaParametros( Lista, 'Tipo' );
     Self.Referencia.CargaParametros( Lista, 'Referencia' );
     inherited CargaParametros( Lista );
end;

procedure TDatosPrestamoBase.DescargaParametros(Lista: TDataTransport);
begin
     inherited DescargaParametros( Lista );
     Self.Tipo.DescargaParametros( Lista, 'Tipo' );
     Self.Referencia.DescargaParametros( Lista, 'Referencia' );
end;

{ ****** TDatosPrestamo ***** }

procedure TDatosPrestamo.CargaParametros( Lista: TDataTransport );
begin
     with Lista do
     begin
          AddDate( 'Inicio', Self.Inicio );
          AddInteger( 'Status', Ord( Status ) );
          AddString( 'Formula', Formula );
          AddFloat( 'SaldoInicial', SaldoInicial );
          AddFloat( 'Monto', Monto );
     end;
     inherited CargaParametros( Lista );
end;

procedure TDatosPrestamo.DescargaParametros( Lista: TDataTransport );
begin
     inherited DescargaParametros( Lista );
     with Lista do
     begin
          Self.Inicio := ParamAsDate( 'Inicio' );
          Self.Status := eStatusPrestamo( ParamAsInteger( 'Status' ) );
          Self.Formula := ParamAsString( 'Formula' );
          Self.SaldoInicial := ParamAsFloat( 'SaldoInicial' );
          Self.Monto := ParamAsFloat( 'Monto' );
     end;
end;

{ ***** TDatosPrestamoCargoAbono ******* }

constructor TDatosPrestamoCargoAbono.Create;
begin
     inherited Create;
     FFecha := TDateKey.Create;
end;

destructor TDatosPrestamoCargoAbono.Destroy;
begin
     FreeAndNil( FFecha );
     inherited Destroy;
end;

procedure TDatosPrestamoCargoAbono.CargaParametros( Lista: TDataTransport );
begin
     Self.Fecha.CargaParametros( Lista, 'Fecha' );
     with Lista do
     begin
          AddDate( 'FechaCaptura', Self.FechaCaptura );
          AddFloat( 'Cargo', Cargo );
          AddFloat( 'Abono', Abono );
          AddString( 'Observaciones', Observaciones );
     end;
     inherited CargaParametros( Lista );
end;

procedure TDatosPrestamoCargoAbono.DescargaParametros( Lista: TDataTransport );
begin
     inherited DescargaParametros( Lista );
     Self.Fecha.DescargaParametros( Lista, 'Fecha' );
     with Lista do
     begin
          Self.FechaCaptura := ParamAsDate( 'FechaCaptura' );
          Self.Cargo := ParamAsFloat( 'Cargo' );
          Self.Abono := ParamAsFloat( 'Abono' );
          Self.Observaciones := ParamAsString( 'Observaciones' );
     end;
end;

{ *********** TIntervalo *********** }

procedure TIntervalo.SetLapso(const Value: TDateTime);
var
   iHoras, iMinutos, iSegundos, iMiliSecs: Word;
begin
     FDias := Trunc( Value );
     DecodeTime( Value, iHoras, iMinutos, iSegundos, iMiliSecs );
     FHoras := iHoras;
     FMinutos := iMinutos;
     FSegundos := iSegundos;
end;

procedure TIntervalo.CargaParametros( Parametros: TDataTransport );
begin
     with Parametros do
     begin
          AddInteger( 'Intervalo.Dias', Self.Dias );
          AddInteger( 'Intervalo.Horas', Self.Horas );
          AddInteger( 'Intervalo.Minutos', Self.Minutos );
          AddInteger( 'Intervalo.Segundos', Self.Segundos );
     end;
end;

procedure TIntervalo.DescargaParametros( Parametros: TDataTransport );
begin
     with Parametros do
     begin
          Self.FDias := ParamAsInteger( 'Intervalo.Dias' );
          Self.FHoras := ParamAsInteger( 'Intervalo.Horas' );
          Self.FMinutos := ParamAsInteger( 'Intervalo.Minutos' );
          Self.FSegundos := ParamAsInteger( 'Intervalo.Segundos' );
     end;
end;

{ ********** TProcessResult ********** }

constructor TProcessResult.Create;
begin
     inherited Create;
     Self.Inicio := Now;
     Self.Final := Now;
     FDuracion := TIntervalo.Create;
end;

destructor TProcessResult.Destroy;
begin
     FreeAndNil( FDuracion );
     inherited;
end;

function TProcessResult.GetDateTimeAsText( const dValue: TDateTime ): TTexto;
begin
     Result := FormatDateTime( 'yyyymmddhhnnss', dValue );
end;

function TProcessResult.GetTextAsDateTime( const sValue: TTexto ): TDateTime;
begin
     Result := EncodeDate( StrToIntDef( Copy( sValue, 1, 4 ), 0 ),
                           StrToIntDef( Copy( sValue, 5, 2 ), 0 ),
                           StrToIntDef( Copy( sValue, 7, 2 ), 0 ) ) +
               EncodeTime( StrToIntDef( Copy( sValue, 9, 2 ), 0 ),
                           StrToIntDef( Copy( sValue, 11, 2 ), 0 ),
                           StrToIntDef( Copy( sValue, 13, 2 ), 0 ),
                           0 );
end;

function TProcessResult.GetAsTexto: TTexto;
const
     K_FORMATO = '%10.10d%3.3d%3.3d%10.10d%10.10d%10.10d%12.12s%12.12s%5.5d%5.5d%5.5d';
begin
     Result := Format( K_FORMATO, [ Self.Folio,                         // K_PROCESO_FOLIO = 0;
                                    Ord( Self.Status ),                 // K_PROCESO_STATUS = 1;
                                    Ord( Self.ProcessID ),              // K_PROCESO_ID = 2;
                                    Self.MaxStep,                       // K_PROCESO_MAXIMO = 3;
                                    Self.Procesados,                    // K_PROCESO_PROCESADOS = 4;
                                    Self.UltimoEmpleado,                // K_PROCESO_ULTIMO_EMPLEADO = 5;
                                    GetDateTimeAsText( Self.Inicio ),   // K_PROCESO_INICIO = 6;
                                    GetDateTimeAsText( Self.Final ),    // K_PROCESO_FIN = 7;
                                    Self.Errores,                       // K_PROCESO_ERRORES = 8;
                                    Self.Advertencias,                  // K_PROCESO_ADVERTENCIAS = 9;
                                    Self.Eventos ] );                   // K_PROCESO_EVENTOS = 10;
end;

function TProcessResult.GetAsVariant: OleVariant;
begin
     Result := VarArrayOf( [ Self.Folio,             // K_PROCESO_FOLIO = 0;
                             Ord( Self.Status ),     // K_PROCESO_STATUS = 1;
                             Ord( Self.ProcessID ),  // K_PROCESO_ID = 2;
                             Self.MaxStep,           // K_PROCESO_MAXIMO = 3;
                             Self.Procesados,        // K_PROCESO_PROCESADOS = 4;
                             Self.UltimoEmpleado,    // K_PROCESO_ULTIMO_EMPLEADO = 5;
                             Self.Inicio,            // K_PROCESO_INICIO = 6;
                             Self.Final,             // K_PROCESO_FIN = 7;
                             Self.Errores,           // K_PROCESO_ERRORES = 8;
                             Self.Advertencias,      // K_PROCESO_ADVERTENCIAS = 9;
                             Self.Eventos ] );       // K_PROCESO_EVENTOS = 10;
end;

procedure TProcessResult.SetAsTexto(const Value: TTexto);
begin
     Self.FFolio := StrToIntDef( Copy( Value, 1, 10 ), 0 );
     Self.FStatus := eProcessStatus( StrToIntDef( Copy( Value, 11, 3 ), 0 ) );
     Self.FProcessID := Procesos( StrToIntDef( Copy( Value, 14, 3 ), 0 ) );
     Self.FMaxStep := StrToIntDef( Copy( Value, 17, 10 ), 0 );
     Self.FProcesados := StrToIntDef( Copy( Value, 27, 10 ), 0 );
     Self.FUltimoEmpleado := StrToIntDef( Copy( Value, 37, 10 ), 0 );
     Self.FInicio := GetTextAsDateTime( Copy( Value, 47, 12 ) );
     Self.FFinal := GetTextAsDateTime( Copy( Value, 59, 12 ) );
     Self.FErrores := StrToIntDef( Copy( Value, 71, 5 ), 0 );
     Self.FAdvertencias := StrToIntDef( Copy( Value, 76, 5 ), 0 );
     Self.FEventos := StrToIntDef( Copy( Value, 81, 5 ), 0 );
     Self.Duracion.Lapso := Self.Final - Self.Inicio;
end;

procedure TProcessResult.SetAsVariant(const Value: OleVariant);
begin
     Self.FFolio := Value[ K_PROCESO_FOLIO ];
     Self.FStatus := eProcessStatus( Value[ K_PROCESO_STATUS ] );
     Self.FProcessID := Procesos( Value[ K_PROCESO_ID ] );
     Self.FMaxStep := Value[ K_PROCESO_MAXIMO ];
     Self.FProcesados := Value[ K_PROCESO_PROCESADOS ];
     Self.FUltimoEmpleado := Value[ K_PROCESO_ULTIMO_EMPLEADO ];
     Self.FInicio := Value[ K_PROCESO_INICIO ];
     Self.FFinal := Value[ K_PROCESO_FIN ];
     Self.FErrores := Value[ K_PROCESO_ERRORES ];
     Self.FAdvertencias := Value[ K_PROCESO_ADVERTENCIAS ];
     Self.FEventos := Value[ K_PROCESO_EVENTOS ];
     Self.Duracion.Lapso := Self.Final - Self.Inicio;
end;

procedure TProcessResult.CargaParametros(Parametros: TDataTransport);
begin
     with Parametros do
     begin
          AddInteger( 'Resultado.Folio', Self.Folio );
          AddInteger( 'Resultado.Status', Ord( Self.Status ) );
          AddInteger( 'Resultado.Proceso', Ord( Self.ProcessID ) );
          AddInteger( 'Resultado.MaxStep', Self.MaxStep );
          AddInteger( 'Resultado.Procesados', Self.Procesados );
          AddInteger( 'Resultado.UltimoEmpleado', Self.UltimoEmpleado );
          AddString( 'Resultado.Inicio', GetDateTimeAsText( Self.Inicio ) );
          AddString( 'Resultado.Final', GetDateTimeAsText( Self.Final ) );
          AddInteger( 'Resultado.Errores', Self.Errores );
          AddInteger( 'Resultado.Advertencias', Self.Advertencias );
          AddInteger( 'Resultado.Eventos', Self.Eventos );
     end;
     Self.Duracion.CargaParametros( Parametros );
end;

procedure TProcessResult.DescargaParametros(Parametros: TDataTransport);
begin
     with Parametros do
     begin
          Self.FFolio := ParamAsInteger( 'Resultado.Folio' );
          Self.FStatus := eProcessStatus( ParamAsInteger( 'Resultado.Status' ) );
          Self.FProcessID := Procesos( ParamAsInteger( 'Resultado.Proceso' ) );
          Self.FMaxStep := ParamAsInteger( 'Resultado.MaxStep' );
          Self.FProcesados := ParamAsInteger( 'Resultado.Procesados' );
          Self.FUltimoEmpleado := ParamAsInteger( 'Resultado.UltimoEmpleado' );
          Self.FInicio := GetTextAsDateTime( ParamAsString( 'Resultado.Inicio' ) );
          Self.FFinal :=  GetTextAsDateTime( ParamAsString( 'Resultado.Final' ) );
          Self.FErrores := ParamAsInteger( 'Resultado.Errores' );
          Self.FAdvertencias := ParamAsInteger( 'Resultado.Advertencias' );
          Self.FEventos := ParamAsInteger( 'Resultado.Eventos' );
     end;
     Self.Duracion.DescargaParametros( Parametros );
end;

{$ifndef DOTNET}
procedure TProcessResult.LoadException( Error: Exception );
begin
     Self.FFolio := 0;
     Self.FStatus := epsCatastrofico;
     Self.FProcessID := Procesos( 0 );
     Self.FMaxStep := 0;
     Self.FProcesados := 0;
     Self.FUltimoEmpleado := 0;
     Self.FInicio := Self.FInicio;
     Self.FFinal := Now;
     Self.FErrores := 0;
     Self.FAdvertencias := 0;
     Self.FEventos := 0;
     Self.Duracion.Lapso := Self.Final - Self.Inicio;
end;
{$endif}

{ ******* TParametrosProceso ********* }

constructor TParametrosProceso.Create;
begin
     inherited Create;
     FOk := True;
     FResultado := TProcessResult.Create;
end;

destructor TParametrosProceso.Destroy;
begin
     FreeAndNil( FResultado );
     inherited Destroy;
end;

procedure TParametrosProceso.CargaZetaParams( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddBoolean( 'Ok', Self.Ok );
     end;
end;

procedure TParametrosProceso.CargaParametros(Lista: TDataTransport);
begin
     Resultado.CargaParametros( Lista );
     with Lista do
     begin
          AddBoolean( 'Ok', Self.Ok );
     end;
     inherited CargaParametros( Lista );
end;

procedure TParametrosProceso.DescargaParametros(Lista: TDataTransport);
begin
     inherited DescargaParametros( Lista );
     with Lista do
     begin
          Self.Ok := ParamAsBoolean( 'Ok' );
     end;
     Resultado.DescargaParametros( Lista );
end;

{$ifndef DOTNET}
procedure TParametrosProceso.LoadException( Error: Exception );
begin
     Self.Ok := False;
     Self.Mensaje := Error.Message;
     Self.Resultado.LoadException( Error );
end;
{$endif}

{ ******* TParametrosProcesoFiltro ********* }

constructor TParametrosProcesoFiltro.Create;
begin
     inherited Create;
     FFiltro := TProcessFilter.Create;
end;

destructor TParametrosProcesoFiltro.Destroy;
begin
     FreeAndNil( FFiltro );
     inherited Destroy;
end;

procedure TParametrosProcesoFiltro.CargaZetaParams( Parametros: TZetaParams );
begin
     Self.Filtro.CargaZetaParams( Parametros );
     inherited CargaZetaParams( Parametros );
end;

procedure TParametrosProcesoFiltro.CargaParametros(Lista: TDataTransport);
begin
     Filtro.CargaParametros( Lista );
     inherited CargaParametros( Lista );
end;

procedure TParametrosProcesoFiltro.DescargaParametros(Lista: TDataTransport);
begin
     inherited DescargaParametros( Lista );
     Filtro.DescargaParametros( Lista );
end;

{ *********** TParametrosNomina ********* }

constructor TParametrosNomina.Create;
begin
     inherited Create;
     FPeriodo := TDatosPeriodo.Create;
     FIMSS := TDatosIMSS.Create;
     FValoresActivos := TDatosActivos.Create;
end;

destructor TParametrosNomina.Destroy;
begin
     FreeAndNil( FValoresActivos );
     FreeAndNil( FIMSS );
     FreeAndNil( FPeriodo );
     inherited Destroy;
end;

procedure TParametrosNomina.CargaZetaParams( Parametros: TZetaParams );
begin
     Periodo.CargaZetaParams( Parametros );
     IMSS.CargaZetaParams( Parametros );
     ValoresActivos.CargaZetaParams( Parametros );
     inherited CargaZetaParams( Parametros );
end;

procedure TParametrosNomina.CargaParametros(Lista: TDataTransport);
begin
     Periodo.CargaParametros( Lista );
     IMSS.CargaParametros( Lista );
     ValoresActivos.CargaParametros( Lista );
     inherited CargaParametros( Lista );
end;

procedure TParametrosNomina.DescargaParametros(Lista: TDataTransport);
begin
     inherited DescargaParametros( Lista );
     Periodo.DescargaParametros( Lista );
     IMSS.DescargaParametros( Lista );
     ValoresActivos.DescargaParametros( Lista );
end;

{ *********** TParametrosNominaCalcular *********** }

procedure TParametrosNominaCalcular.CargaZetaParams( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddBoolean( 'MuestraScript', MostrarScript );
          AddBoolean( 'MuestraTiempos', MostrarTiempos );
          AddInteger( 'TipoCalculo', Ord( Tipo ) );
     end;
     inherited CargaZetaParams( Parametros );
end;

procedure TParametrosNominaCalcular.CargaParametros( Lista: TDataTransport );
begin
     with Lista do
     begin
          AddBoolean( 'MostrarScript', MostrarScript );
          AddBoolean( 'MostrarTiempos', MostrarTiempos );
          AddInteger( 'TipoCalculo', Ord( Tipo ) );
     end;
     inherited CargaParametros( Lista );
end;

procedure TParametrosNominaCalcular.DescargaParametros( Lista: TDataTransport );
begin
     inherited DescargaParametros( Lista );
     with Lista do
     begin
          MostrarScript := ParamAsBoolean( 'MostrarScript' );
          MostrarTiempos := ParamAsBoolean( 'MostrarTiempos' );
          Tipo := eTipoCalculo( ParamAsInteger( 'TipoCalculo' ) );
     end;
end;

{ ********* TParametrosNominaRastrear *********** }

procedure TParametrosNominaRastrear.CargaZetaParams( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddInteger( 'NumeroConcepto', Self.Concepto );
          AddString( 'DesConcepto', Self.ConceptoNombre );
          AddString( 'Formula', Self.ConceptoFormula );
          AddBoolean( 'MostrarNomParam', Self.MostrarParametros );
          AddInteger( 'Empleado', Self.Empleado );
          AddString( 'Nombre', Self.EmpleadoNombre );
          AddBoolean('MuestraScript', False );
          AddString( 'Archivo', '' );
     end;
     inherited CargaZetaParams( Parametros );
end;

procedure TParametrosNominaRastrear.CargaParametros( Lista: TDataTransport );
begin
     with Lista do
     begin
          AddInteger( 'NumeroConcepto', Self.Concepto );
          AddString( 'DesConcepto', Self.ConceptoNombre );
          AddString( 'Formula', Self.ConceptoFormula );
          AddBoolean( 'MostrarNomParam', Self.MostrarParametros );
          AddInteger( 'Empleado', Self.Empleado );
          AddString( 'Nombre', Self.EmpleadoNombre );
          AddBoolean('MuestraScript', False );
          AddString( 'Archivo', '' );
          AddString( 'Datos', Self.Datos );
     end;
     inherited CargaParametros( Lista );
end;

procedure TParametrosNominaRastrear.DescargaParametros( Lista: TDataTransport );
begin
     inherited DescargaParametros( Lista );
     with Lista do
     begin
          Self.Concepto := ParamAsInteger( 'NumeroConcepto' );
          Self.ConceptoNombre := ParamAsString( 'DesConcepto' );
          Self.ConceptoFormula := ParamAsString( 'Formula' );
          Self.MostrarParametros := ParamAsBoolean( 'MostrarNomParam' );
          Self.Empleado := ParamAsInteger( 'Empleado' );
          Self.EmpleadoNombre := ParamAsString( 'Nombre' );
          Self.Datos := ParamAsString( 'Datos' );
     end;
end;

{ ************ TParametrosNominaGenerarPoliza *********** }

procedure TParametrosNominaGenerarPoliza.CargaZetaParams( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddInteger( 'CodigoReporte', Self.Formato );
          AddString( 'RangoNominas', RangoPeriodos );
     end;
     inherited CargaZetaParams( Parametros );
end;

procedure TParametrosNominaGenerarPoliza.CargaParametros( Lista: TDataTransport );
begin
     with Lista do
     begin
          AddInteger( 'CodigoReporte', Self.Formato );
          AddString( 'RangoNominas', RangoPeriodos );
     end;
     inherited CargaParametros( Lista );
end;

procedure TParametrosNominaGenerarPoliza.DescargaParametros( Lista: TDataTransport );
begin
     inherited DescargaParametros( Lista );
     with Lista do
     begin
          Self.Formato := ParamAsInteger( 'CodigoReporte' );
          Self.RangoPeriodos := ParamAsString( 'RangoNominas' );
     end;
end;

{ ********* TParametrosNominaNetoBruto *********** }

procedure TParametrosNominaNetoBruto.CargaZetaParams( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddBoolean( 'CalcularBruto', Self.CalcularBruto );
          AddFloat( 'Salario', Self.Salario );
          AddFloat( 'Subsidio', Self.SubsidioAcreditable );
          AddFloat( 'Antiguedad', Self.Antiguedad );
          AddString( 'ZonaGeografica', Self.ZonaGeografica );
          AddString( 'TablaPrestaciones', Self.TablaPrestaciones );
          AddString( 'OtrasPercepciones', Self.OtrasPercepciones );
          AddDate( 'Fecha', Self.Fecha );
          AddInteger( 'Periodo', Self.Periodo );
          AddFloat( 'Redondeo', Self.Redondeo );
     end;
     inherited CargaZetaParams( Parametros );
end;

procedure TParametrosNominaNetoBruto.CargaParametros( Lista: TDataTransport );
begin
     with Lista do
     begin
          AddBoolean( 'CalcularBruto', Self.CalcularBruto );
          AddFloat( 'Salario', Self.Salario );
          AddFloat( 'SalarioBruto', Self.SalarioBruto );
          AddFloat( 'SalarioMinimo', Self.SalarioMinimo );
          AddFloat( 'SalarioIntegrado', Self.SalarioIntegrado );
          AddFloat( 'FactorIntegracion', Self.FactorIntegracion );
          AddFloat( 'IMSS', Self.IMSS );
          AddFloat( 'ISPT', Self.ISPT );
          AddFloat( 'Subsidio', Self.SubsidioAcreditable );
          AddFloat( 'Antiguedad', Self.Antiguedad );
          AddString( 'ZonaGeografica', Self.ZonaGeografica );
          AddString( 'TablaPrestaciones', Self.TablaPrestaciones );
          AddString( 'OtrasPercepciones', Self.OtrasPercepciones );
          AddFloat( 'OtrasPercepcionesMonto', Self.OtrasPercepcionesMonto );
          AddDate( 'Fecha', Self.Fecha );
          AddInteger( 'Periodo', Self.Periodo );
          AddFloat( 'Redondeo', Self.Redondeo );
     end;
     inherited CargaParametros( Lista );
end;

procedure TParametrosNominaNetoBruto.DescargaParametros( Lista: TDataTransport );
begin
     inherited DescargaParametros( Lista );
     with Lista do
     begin
          Self.CalcularBruto := ParamAsBoolean( 'CalcularBruto' );
          Self.Salario := ParamAsFloat( 'Salario' );
          Self.SalarioBruto := ParamAsFloat( 'SalarioBruto' );
          Self.SalarioMinimo := ParamAsFloat( 'SalarioMinimo' );
          Self.SalarioIntegrado := ParamAsFloat( 'SalarioIntegrado' );
          Self.FactorIntegracion := ParamAsFloat( 'FactorIntegracion' );
          Self.IMSS := ParamAsFloat( 'IMSS' );
          Self.ISPT := ParamAsFloat( 'ISPT' );
          Self.SubsidioAcreditable := ParamAsFloat( 'Subsidio' );
          Self.Antiguedad := ParamAsInteger( 'Antiguedad' );
          Self.ZonaGeografica := ParamAsString( 'ZonaGeografica' );
          Self.TablaPrestaciones := ParamAsString( 'TablaPrestaciones' );
          Self.OtrasPercepciones := ParamAsString( 'OtrasPercepciones' );
          Self.OtrasPercepcionesMonto := ParamAsFloat( 'OtrasPercepcionesMonto' );
          Self.Fecha := ParamAsDate( 'Fecha' );
          Self.Periodo := ParamAsInteger( 'Periodo' );
          Self.Redondeo := ParamAsFloat( 'Redondeo' );
     end;
end;

function TParametrosNominaNetoBruto.GetSalarioNeto: Extended;
begin
     Result := SalarioBruto + OtrasPercepcionesMonto - ISPT - IMSS;
end;

function TParametrosNominaNetoBruto.Convertir( const rValue: Extended; const ePeriodo: eTipoPeriodo ): Extended;
begin
     Result := rValue / K_FACTOR_MENSUAL * ZetaCommonTools.Dias7Tipo( ePeriodo );
end;

{ *********** TParametrosIMSS *********** }

constructor TParametrosIMSS.Create;
begin
     FIMSS := TDatosIMSS.Create;
     FPeriodo := TDatosPeriodo.Create;
     FValoresActivos := TDatosActivos.Create;
     inherited Create;
end;

destructor TParametrosIMSS.Destroy;
begin
     FreeAndNil( FValoresActivos );
     FreeAndNil( FPeriodo );
     FreeAndNil( FIMSS );
     inherited Destroy;
end;

procedure TParametrosIMSS.CargaZetaParams( Parametros: TZetaParams );
begin
     Periodo.CargaZetaParams( Parametros );
     IMSS.CargaZetaParams( Parametros );
     ValoresActivos.CargaZetaParams( Parametros );
     inherited CargaZetaParams( Parametros );
end;

procedure TParametrosIMSS.CargaParametros(Lista: TDataTransport);
begin
     IMSS.CargaParametros( Lista );
     Periodo.CargaParametros( Lista );
     ValoresActivos.CargaParametros( Lista );
     inherited CargaParametros( Lista );
end;

procedure TParametrosIMSS.DescargaParametros(Lista: TDataTransport);
begin
     inherited DescargaParametros( Lista );
     IMSS.DescargaParametros( Lista );
     Periodo.DescargaParametros( Lista );
     ValoresActivos.DescargaParametros( Lista );
end;

{ ********* TParametrosIMSSExportarSUA *********** }

procedure TParametrosIMSSExportarSUA.CargaZetaParams(Parametros: TZetaParams);
begin
     inherited CargaZetaParams( Parametros );
     with Parametros do
     begin
          AddString( 'Patron', Self.Patron );
          AddString( 'NumeroRegistro', Self.NumeroRegistro );
          AddInteger( 'Year', Self.Year );
          AddInteger( 'Mes', Self.Mes );
          AddString( 'ArchivoEmpleados', Self.ArchivoEmpleados );
          AddString( 'ArchivoMovimientos', Self.ArchivoMovimientos );
          AddBoolean( 'EnviarEmpleados', Self.EnviarEmpleados );
          AddBoolean( 'BorrarEmpleados', Self.BorrarEmpleados );
          AddBoolean( 'IncluirHomoclave', Self.IncluirHomoclave );
          AddBoolean( 'EnviarCuotaFija', Self.EnviarCuotaFija );
          AddBoolean( 'EnviarMovimientos', Self.EnviarMovimientos );
          AddBoolean( 'BorrarMovimientos', Self.BorrarMovimientos );
          AddBoolean( 'EnviarFaltaBaja', Self.EnviarFaltaBaja );
     end;
end;

procedure TParametrosIMSSExportarSUA.CargaParametros( Lista: TDataTransport );
begin
     with Lista do
     begin
          AddString( 'Patron', Self.Patron );
          AddString( 'NumeroRegistro', Self.NumeroRegistro );
          AddInteger( 'Year', Self.Year );
          AddInteger( 'Mes', Self.Mes );
          AddString( 'ArchivoEmpleados', Self.ArchivoEmpleados );
          AddString( 'ArchivoMovimientos', Self.ArchivoMovimientos );
          AddBoolean( 'EnviarEmpleados', Self.EnviarEmpleados );
          AddBoolean( 'BorrarEmpleados', Self.BorrarEmpleados );
          AddBoolean( 'IncluirHomoclave', Self.IncluirHomoclave );
          AddBoolean( 'EnviarCuotaFija', Self.EnviarCuotaFija );
          AddBoolean( 'EnviarMovimientos', Self.EnviarMovimientos );
          AddBoolean( 'BorrarMovimientos', Self.BorrarMovimientos );
          AddBoolean( 'EnviarFaltaBaja', Self.EnviarFaltaBaja );
     end;
     inherited CargaParametros( Lista );
end;

procedure TParametrosIMSSExportarSUA.DescargaParametros( Lista: TDataTransport );
begin
     inherited DescargaParametros( Lista );
     with Lista do
     begin
          Self.Patron := ParamAsString( 'Patron' );
          Self.NumeroRegistro := ParamAsString( 'NumeroRegistro' );
          Self.Year := ParamAsInteger( 'Year' );
          Self.Mes := ParamAsInteger( 'Mes' );
          Self.ArchivoEmpleados := ParamAsString( 'ArchivoEmpleados' );
          Self.ArchivoMovimientos := ParamAsString( 'ArchivoMovimientos' );
          Self.EnviarEmpleados := ParamAsBoolean( 'EnviarEmpleados' );
          Self.BorrarEmpleados := ParamAsBoolean( 'BorrarEmpleados' );
          Self.IncluirHomoclave := ParamAsBoolean( 'IncluirHomoclave' );
          Self.EnviarCuotaFija := ParamAsBoolean( 'EnviarCuotaFija' );
          Self.EnviarMovimientos := ParamAsBoolean( 'EnviarMovimientos' );
          Self.BorrarMovimientos := ParamAsBoolean( 'BorrarMovimientos' );
          Self.EnviarFaltaBaja := ParamAsBoolean( 'EnviarFaltaBaja' );
     end;
end;

{ TDatosReporte }

constructor TDatosReporte.Create;
begin
     inherited Create;
     FDatosPeriodo := TDatosPeriodo.Create;
     FDatosImss := TDatosImss.Create;
     FDatosActivos := TDatosActivos.Create;
end;

destructor TDatosReporte.Destroy;
begin
     FreeAndNil( FDatosActivos );
     FreeAndNil( FDatosImss );
     FreeAndNil( FDatosPeriodo );
     inherited;
end;

procedure TDatosReporte.CargaParametros(Parametros: TDataTransport);
begin
     Parametros.AddInteger( 'Reporte', Reporte );
     Parametros.AddString( 'Path', Path );
     Parametros.AddString( 'Nombre', Nombre );


     FDatosActivos.CargaParametros( Parametros );
     FDatosImss.CargaParametros( Parametros );
     FDatosPeriodo.CargaParametros( Parametros );

     inherited CargaParametros( Parametros );
end;

procedure TDatosReporte.DescargaParametros(Parametros: TDataTransport);
begin
     inherited DescargaParametros( Parametros );

     FDatosActivos.DescargaParametros( Parametros );
     FDatosImss.DescargaParametros( Parametros );
     FDatosPeriodo.DescargaParametros( Parametros );

     Reporte := ParamAsInteger( 'Reporte' );
     Path := ParamAsString( 'Path' );
     Nombre := ParamAsString( 'Nombre' );
end;

end.
