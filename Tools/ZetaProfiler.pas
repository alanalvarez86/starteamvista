unit ZetaProfiler;

interface

uses Windows, SysUtils, Consts, Classes, Contnrs;

type
  TZetaProfilerItem = class( TObject )
  private
    { Private declarations }
    FNombre: String;
    FTiempo: TDateTime;
    function GetDuracion: String;
  public
    { Public declarations }
    constructor Create( const sNombre: String );
    property Nombre: String read FNombre;
    property Duracion: String read GetDuracion;
    property Tiempo: TDateTime read FTiempo;
    function Check( const dElapsed: TDateTime ): Extended;
    function GetPorcentaje( const dTotal: TDateTime ): String;
  end;
  TZetaProfiler = class( TObjectList )
  private
    { Private declarations }
    FStart: TDateTime;
    FTotal: TDateTime;
    function GetElemento(Index: Integer): TZetaProfilerItem;
    function Locate( const sMessage: String ): Integer;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property Elemento[ Index: Integer ]: TZetaProfilerItem read GetElemento;
    procedure Check( const sMessage: String );
    procedure Report(const sFileName: String);
    procedure Start;
    procedure Reset;
  end;

var
   Profiler: TZetaProfiler;

implementation

const
     K_MINUTOS = 60;
     K_HORAS = 3600;
     K_DIAS = 86400;

function ComparaTiempos( Item1, Item2: Pointer ): Integer;
begin
     if ( TZetaProfilerItem( Item1 ).Tiempo = TZetaProfilerItem( Item2 ).Tiempo ) then
        Result := 0
     else
         if ( TZetaProfilerItem( Item1 ).Tiempo < TZetaProfilerItem( Item2 ).Tiempo ) then
            Result := 1
         else
             Result := -1;
end;

function GetTiempoAsStr( const rTiempo: TDateTime ): String;
var
   iDias, iHoras, iMinutos, iSegundos, iMilisegundos: Word;
   rSegundos: Extended;
begin
     iDias := Trunc( rTiempo );
     DecodeTime( rTiempo, iHoras, iMinutos, iSegundos, iMilisegundos );
     rSegundos := iSegundos + iMilisegundos / 1000;
     if ( iDias > 0 ) then
        Result := Format( '%d Dias ', [ iDias ] )
     else
         Result := '';
     if ( iHoras > 0 ) then
        Result := Result + Format( '%d Horas ', [ iHoras ] );
     if ( iMinutos > 0 ) then
        Result := Result + Format( '%d Minutos ', [ iMinutos ] );
     if ( rSegundos > 0 ) then
        Result := Result + Trim( Format( '%2.6n Segundos', [ rSegundos ] ) );
     Result := Trim( Result );
end;

{ TZetaProfilerItem }

constructor TZetaProfilerItem.Create(const sNombre: String);
begin
     FNombre := sNombre;
     FTiempo := 0;
end;

function TZetaProfilerItem.Check( const dElapsed: TDateTime ): Extended;
begin
     Result := dElapsed;
     FTiempo := FTiempo + Result;
end;

function TZetaProfilerItem.GetDuracion: String;
begin
     Result := GetTiempoAsStr( Tiempo );
end;

function TZetaProfilerItem.GetPorcentaje(const dTotal: TDateTime): String;
begin
     if ( dTotal = 0 ) then
        Result := 'n/a'
     else
         Result := Trim( Format( '%5.2n', [ 100 * Tiempo / dTotal ] ) ) + ' %';
end;

{ ********** TZetaProfiler ************ }

function TZetaProfiler.GetElemento(Index: Integer): TZetaProfilerItem;
begin
     if ( Index < Count ) then
        Result := TZetaProfilerItem( Items[ Index ] )
     else
         Result := nil;
end;

function TZetaProfiler.Locate(const sMessage: String): Integer;
var
   i: Integer;
begin
     Result := -1;
     for i := 0 to ( Count - 1 ) do
     begin
          if ( Elemento[ i ].Nombre = sMessage ) then
          begin
               Result := i;
               Break;
          end;
     end;
end;

procedure TZetaProfiler.Check(const sMessage: String);
var
   i: Integer;
begin
     i := Locate( sMessage );
     if ( i < 0 ) then
     begin
          Add( TZetaProfilerItem.Create( sMessage ) );
          i := Count - 1;
     end;
     FTotal := FTotal + Elemento[ i ].Check( Now - FStart );
     FStart := Now;
end;

procedure TZetaProfiler.Start;
begin
     Clear;
     FStart := Now;
     FTotal := 0;
end;

procedure TZetaProfiler.Reset;
begin
end;

procedure TZetaProfiler.Report( const sFileName: String );
var
   i: Integer;
   FTexto: TStrings;
begin
     Sort( ComparaTiempos );
     FTexto := TStringList.Create;
     try
        for i := 0 to ( Count - 1 ) do
        begin
             with Elemento[ i ] do
             begin
                  FTexto.Add( Format( '%s = %s ( %s )', [ Nombre, Duracion, GetPorcentaje( FTotal ) ] ) );
             end;
        end;
        with FTexto do
        begin
             Add( '' );
             Add( Format( 'Tiempo Total: %s', [ GetTiempoAsStr( FTotal ) ] ) );
             SaveToFile( sFileName );
        end;
     finally
            FTexto.Free;
     end;
end;

initialization
begin
     Profiler := TZetaProfiler.Create;
end;

finalization
begin
     FreeAndNil( Profiler );
end;

end.
