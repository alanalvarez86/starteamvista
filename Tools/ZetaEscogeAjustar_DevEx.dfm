inherited EscogeAjustar_DevEx: TEscogeAjustar_DevEx
  Caption = 'Error al Registrar Incapacidad'
  ClientHeight = 163
  ClientWidth = 451
  PixelsPerInch = 96
  TextHeight = 13
  object lMensajeAjustar: TLabel [0]
    Left = 75
    Top = 40
    Width = 43
    Height = 13
    Caption = 'Mensaje:'
  end
  object lAdvertencia: TLabel [1]
    Left = 75
    Top = 16
    Width = 263
    Height = 16
    Caption = 'Advertencia al Registrar Incapacidad.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Image1: TImage [2]
    Left = 24
    Top = 18
    Width = 32
    Height = 32
    AutoSize = True
    Picture.Data = {
      0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
      000000200806000000737A7AF4000000017352474200AECE1CE9000000046741
      4D410000B18F0BFC6105000000206348524D00007A26000080840000FA000000
      80E8000075300000EA6000003A98000017709CBA513C00000009704859730000
      0EC100000EC101B8916BED00000171494441545847D5973D8EC2301046BF8282
      822350700C8A2DF620141C0589A350208156AB900350525072000A8A102E4019
      C6849FC4D89971B08D40B24024F1BC387EFE1CE0DB3FC51A9D8FDC43BE402F5F
      619327D81DFED08F0E91A7981240716BB3A800D93F0674E7E70A40714C308C06
      4185E7D5E2B7DF9B280054ECC750FCFA28681446C121A8D0D60640FF1FF66B74
      83416409C60DC5CB0999601204406947439C0900CE41B4D4B4BBEB67FBF6ABA5
      493B7D98F591F1AAA5493B0E40AD925EE6824D3B01801F2D6DDA4900DED6B249
      3B21407B2D39ED1C00DA69C96927062813D34D4B9376BA668E006E696949BBDA
      A2E30A20D6B229EDAAA3D00240A6259376DC12CC1D6F4E4B51DA3DB7615C31F3
      715B5A72DABDA42075A4A2F7D1A460B49533A625A7DDBB1668D7D7B59468E719
      A0AEA5443B1D4041571BBB51797D44655A4AB56B51809DA4D74D2C759C86E85C
      D427BD55E1B4C25274B27496BB9DB7857AB9548F214BF11BBB05DDC24BB76317
      EF8AE332643996160000000049454E44AE426082}
    Stretch = True
  end
  inherited PanelBotones: TPanel
    Top = 127
    Width = 451
    inherited OK_DevEx: TcxButton
      Left = 282
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 362
      Cancel = True
    end
  end
  object rbAjustar: TRadioButton [4]
    Left = 83
    Top = 71
    Width = 273
    Height = 17
    Caption = 'Ajustar Incapacidad del:'
    Checked = True
    TabOrder = 1
    TabStop = True
  end
  object rbAjustarNueva: TRadioButton [5]
    Left = 83
    Top = 103
    Width = 281
    Height = 17
    Caption = 'Ajustar Incapacidad Nueva del:'
    TabOrder = 2
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
