unit ZGlobalTress;

interface

uses ZetaCommonLists,
     ZetaCommonClasses;

{$define CAMBIOS_SENDA}
{.$undefine CAMBIOS_SENDA}

const
     K_NUM_GLOBALES = 330;
     K_NUM_DEFAULTS = 80;
     K_TOT_GLOBALES = K_NUM_GLOBALES + K_NUM_DEFAULTS ;
     K_BASE_DEFAULTS = 2000;
     K_TOT_DESCRIPCIONES_GV = 13;

     {Constantes para Globales}

     {Boton Identificacion}
     K_GLOBAL_RAZON_EMPRESA            =  1; {Raz�n Social de Empresa}
     K_GLOBAL_CALLE_EMPRESA            =  2; {Calle de Empresa}
     K_GLOBAL_COLONIA_EMPRESA          =  3; { Colonia de Empresa}
     K_GLOBAL_CIUDAD_EMPRESA           =  4; {Ciudad de Empresa}
     K_GLOBAL_ENTIDAD_EMPRESA          =  5; {Entidad de Empresa}
     K_GLOBAL_CP_EMPRESA               =  6; {C�digo Postal Empresa}
     K_GLOBAL_TEL_EMPRESA              =  7; {Tel�fono de Empresa}
     K_GLOBAL_RFC_EMPRESA              =  8; {R.F.C. de Empresa}
     K_GLOBAL_INFONAVIT_EMPRESA        =  9; { # de INFONAVIT de Empresa}
     K_GLOBAL_DIGITO_EMPRESA           = 10; {D�gito de Empresa}
     K_GLOBAL_REPRESENTANTE            = 12; {Representante Legal}
     K_GLOBAL_NUM_EXT_EMPRESA          = 208; { # Exterior del domicilio fiscal }
     K_GLOBAL_NUM_INT_EMPRESA          = 209; { # Interior del domicilio fiscal }


     {Niveles de Organigrama}
     {Boton de Areas}
     K_GLOBAL_NIVEL_BASE =  12;    { Facilita OFFSET de Niveles }
     K_GLOBAL_NIVEL_BASE2=  224;   { Facilita OFFSET de Niveles del 10 al 12 }
     K_GLOBAL_NIVEL_MAX  =  {$ifdef ACS}12{$else}9{$endif};    { M�ximo 12 Niveles de Organigrama }
     K_GLOBAL_NIVEL1  = 13; { Nombre de Nivel #1}
     K_GLOBAL_NIVEL2  = 14; {Nombre de Nivel #2}
     K_GLOBAL_NIVEL3  = 15; {Nombre de Nivel #3}
     K_GLOBAL_NIVEL4  = 16; {Nombre de Nivel #4}
     K_GLOBAL_NIVEL5  = 17; {Nombre de Nivel #5}
     K_GLOBAL_NIVEL6  = 18; {Nombre de Nivel #6}
     K_GLOBAL_NIVEL7  = 19; {Nombre de Nivel #7}
     K_GLOBAL_NIVEL8  = 20; {Nombre de Nivel #8}
     K_GLOBAL_NIVEL9  = 21; {Nombre de Nivel #9}
     {Se agregaron 3 niveles del 10 al 12}
{$ifdef ACS}
     K_GLOBAL_NIVEL10  = 234; {Nombre de Nivel #10}
     K_GLOBAL_NIVEL11  = 235; {Nombre de Nivel #11}
     K_GLOBAL_NIVEL12  = 236; {Nombre de Nivel #12}
{$endif}
{$ifdef DENSO}
     K_GLOBAL_NUM_EMP_SUGIERE_ESPECIAL = 234; { SI/NO Sugiere # de Empleado en programacion especial }
{$endif}

     {Campos Adicionales}
     {Boton Adicionales}
     K_GLOBAL_FECHA_BASE = 21;
     K_GLOBAL_FECHA_MAX = 3;
     K_GLOBAL_FECHA1  = 22; { Nombre de Fecha #1}
     K_GLOBAL_FECHA2  = 23; { Nombre de Fecha #2}
     K_GLOBAL_FECHA3  = 24; { Nombre de Fecha #3}
     K_GLOBAL_TEXTO_BASE = 24;
     K_GLOBAL_TEXTO_MAX = 4;
     K_GLOBAL_TEXTO1  = 25; { Nombre de Texto #1}
     K_GLOBAL_TEXTO2  = 26; { Nombre de Texto #2}
     K_GLOBAL_TEXTO3  = 27; { Nombre de Texto #3}
     K_GLOBAL_TEXTO4  = 28; { Nombre de Texto #4}
     K_GLOBAL_NUM_BASE = 28;
     K_GLOBAL_NUM_MAX = 3;
     K_GLOBAL_NUM1    = 29; {Nombre de N�mero #1}
     K_GLOBAL_NUM2    = 30; {Nombre de N�mero #2}
     K_GLOBAL_NUM3    = 31; {Nombre de N�mero #3}
     K_GLOBAL_LOG_BASE = 31;
     K_GLOBAL_LOG_MAX  = 3;
     K_GLOBAL_LOG1    = 32; {Nombre de L�gico #1}
     K_GLOBAL_LOG2    = 33; {Nombre de L�gico #2}
     K_GLOBAL_LOG3    = 34; {Nombre de L�gico #3}
     K_GLOBAL_TAB_BASE = 34;
     K_GLOBAL_TAB_MAX  = 4;
     K_GLOBAL_TAB1    = 35; {Nombre de Tabla #1}
     K_GLOBAL_TAB2    = 36; {Nombre de Tabla #2}
     K_GLOBAL_TAB3    = 37; {Nombre de Tabla #3}
     K_GLOBAL_TAB4    = 38; {Nombre de Tabla #4}

     K_GLOBAL_PRIMER_ADICIONAL = K_GLOBAL_FECHA1;
     K_GLOBAL_LAST_ADICIONAL = K_GLOBAL_TAB4;

     {Boton Asistencia}
     K_INCIDENCIA_FI                   = 117;{ Incidencia que se asigna cuando es FI, cuando se modifica la prenomina manualmente}
     K_GLOBAL_PRIMER_DIA               = 40;  {Primer d�a de la semana}
     K_GLOBAL_GRACIA_2CHECK            = 73; {Gracia Checadas Repetidas}
     K_GLOBAL_AUT_X                    = 74; {Autorizar Horas Extras}
     K_GLOBAL_AUT_FESTIVO_TRAB         = 75; {Autorizar Festivo Trabajado}
     K_GLOBAL_TRATO_X_SAB              = 76; {Trato Extras en S�bado}
     K_GLOBAL_TRATO_X_DESC             = 77; {Trato Extras en Descanso}
     K_GLOBAL_TRATO_X_FESTIVO          = 78; {Trato Extras en Festivo}
     K_GLOBAL_SUMAR_JORNADAS           = 79; {Sumar Jornadas de Horarios}
     K_EXTRAS_NO_CHECAN                = 121;
     // Nuevo Global. Permisos d�as h�biles
     K_GLOBAL_PERMISOS_DIAS_HABILES    = 305;
     // ----- -----
     K_GLOBAL_NO_CHECA_USAR_JORNADAS   = 160;
     K_GLOBAL_TAB_REDON_ORD            = 71; {Tabla Redondeo Ordinarias}
     K_GLOBAL_TAB_REDON_X              = 72; {Tabla Redondeo Extras}
     K_GLOBAL_HRS_EXENTAS_IMSS         = 166;
     K_GLOBAL_VALIDA_PERM_CGOCE        = 169;
     K_GLOBAL_VALIDA_PERM_SGOCE        = 170;
     K_GLOBAL_VALIDA_HRS_EXTRAS        = 171;
     K_GLOBAL_VALIDA_DESC_TRAB         = 172;
     K_GLOBAL_VALIDA_PERM_CGENT        = 173;
     K_GLOBAL_VALIDA_PERM_SGENT        = 174;
     K_GLOBAL_DESCONTAR_COMIDAS        = 178;
     K_GLOBAL_APROBAR_AUTORIZACIONES   = 180;
     K_GLOBAL_TOPE_SEMANAL_DOBLES      = 188;
     K_GLOBAL_TRATAMIENTO_TIEMPO_EXTRA = 196;
     K_GLOBAL_DIAS_APLICA_EXENTO       = 197;
     K_GLOBAL_PAGO_PRIMA_VACA_ANIV     = 201;
     K_GLOBAL_EXTRA_DESCANSO           = 207;
     K_GLOBAL_PERMITIR_CAPTURA         = 243;
     K_GLOBAL_CAPTURA_OBLIGATORIA      = 244;
     {Global para Poliza contable para Cardinal Health}
     //K_GLOBAL_CARDINAL_USA_HRS_NOMINA   = 245;

     {Boton N�mina}
     K_GLOBAL_REDONDEO_NETO            = 39; {Redondeo de Neto}
     K_GLOBAL_LLEVAR_SALDO_REDONDEO    = 41; {Llevar Saldo de Redondeo}
     K_GLOBAL_PROMEDIAR_SALARIOS       = 42; {Promediar Salarios}
     K_GLOBAL_PORCEN_SUBSIDIO          = 43; {Proporcional del Subsidio}
     K_GLOBAL_FORMATO_POLIZA           = 45; {Formato de P�liza Contable}
     K_GLOBAL_FORMATO_BANCA            = 46; {Formato Banca Electr�nica}
     K_GLOBAL_PRIMA_DOMINICAL_COMPLETA = 129;{Prima Dominical Considera S�bados y Lunes}
     K_GLOBAL_PRIMA_DOMINICAL_TIPO_DIA = 189;{Prima Dominical se Calcula sin Importar el Tipo de Dia}

     K_GLOBAL_ISPT                     = 50; {Concepto para ISPT}
     K_GLOBAL_CREDITO                  = 51; {Concepto Cr�dito al Salario}
     K_GLOBAL_AJUSTE                   = 52; {Concepto Ajuste de Moneda}
     K_GLOBAL_INCOBRABLES              = 53; {Concepto Deduc. Incobrables}
     K_GLOBAL_SEPARAR_CRED_SAL         = 54; {Separar Cr�dito al Salario}
     K_GLOBAL_PROM_SAL_MIN             = 55; {Promediar Salarios M�nimos}
     K_DEFAULT_RECIBOS_PAGADOS	       = 122;
     K_TITULO_MONTO_ADICIONAL_RECIBO   = 123;
     K_FORMULA_MONTO_ADICIONAL_RECIBO  = 124;

     K_GLOBAL_PIRAMIDA_TOLERANCIA      = 194;
     K_GLOBAL_PIRAMIDA_CONCEPTO        = 195;
     K_GLOBAL_PIRAMIDA_PRESTAMOS       = 205;
     K_GLOBAL_SIMULACION_FINIQUITOS    = 202;
     K_GLOBAL_SIM_FINIQ_APROBACION     = 242;
     K_GLOBAL_TOPE_PRESTAMOS           = 206;
     K_GLOBAL_FONACOT_PRESTAMO         = 211;
     K_GLOBAL_FONACOT_PRESTAMO_CONCILIA= 328;

     K_GLOBAL_NOMINA_METODO_DEFAULT     = 310;

     {Boton Variables Globales}
     K_GLOBAL_NUM_GLOBAL1              = 47; {N�mero Global #1}
     K_GLOBAL_NUM_GLOBAL2              = 48; {N�mero Global #2}
     K_GLOBAL_NUM_GLOBAL3              = 49; {N�mero Global #3}
     K_GLOBAL_NUM_GLOBAL4              = 56; {N�mero Global #4}
     K_GLOBAL_NUM_GLOBAL5              = 57; {N�mero Global #5}
     K_GLOBAL_NUM_GLOBAL6              = 58; {N�mero Global #6}
     K_GLOBAL_NUM_GLOBAL7              = 59; {N�mero Global #7}
     K_GLOBAL_NUMERO_GLOBAL8           = 60; {N�mero Global #8}
     K_GLOBAL_NUM_GLOBAL9              = 84; {N�mero Global #9}
     K_GLOBAL_NUM_GLOBAL10             = 85; {N�mero Global #10}
     K_GLOBAL_TEXT_GLOBAL1             = 86; {Texto Global #1 }
     K_GLOBAL_TEXT_GLOBAL2             = 87; {Texto Global #2}
     K_GLOBAL_TEXT_GLOBAL3             = 88; {Texto Global #3}

     {Boton Recursos}
     K_GLOBAL_NUM_IMSS                 = 11; {N�mero de Gu�a IMSS}
     K_GLOBAL_DIR_PLANT                = 61; {Directorio Plantillas}
     K_GLOBAL_ZONAS_GEOGRAFICAS        = 44; {Zonas Geogr�ficas}
     K_GLOBAL_DIAS_VACACIONES          = 127;
     K_GLOBAL_VALIDA_ALTA_EMP          = 168;
     K_GLOBAL_RH_VALIDA_ORGANIGRAMA    = 128;
     K_GLOBAL_EMPRESA_SELECCION        = 179; {C�digo de Empresa de Selecci�n}
     K_GLOBAL_NO_VALIDAR_BAJAS_EXT     = 227; {No Validar Bajas Extempor�neas}
     K_GLOBAL_CONSERVAR_TAM_FOTOS      = 229;{OP: 04/06/08}
     K_GLOBAL_VALIDAR_PRESTAMOS        = 246; {ACL: 26/06/09}
     K_GLOBAL_USAR_VALIDACION_STATUS_TABLAS = 291; { AV: 16/nov/2010: Usar validaci�n de estatus de tablas y cat�logos }

     {IMSS/INFONAVIT}
     K_GLOBAL_ADICIONAL_RETIRO         = 62; {Concepto Adicional Retiro}
     K_GLOBAL_FORM_PERCEP              = 63; {F�rmula M�d. 18 Percepciones}
     K_GLOBAL_FORM_DIAS                = 64; {F�rmula M�d. 18 D�as}
     K_GLOBAL_FILE_EMP_SUA             = 65; {SUA: Archivo Empleados}
     K_GLOBAL_FILE_MOVS_SUA            = 66; {SUA: Archivo Movimientos}
     K_GLOBAL_DIR_DATOS_SUA            = 67; { SUA: Directorio de Datos}
     K_GLOBAL_FILE_AFIL_SUA            = 198;{SUA: Archivo Datos afiliatorios}
     K_GLOBAL_FILE_INFO_SUA            = 199;{SUA: Archivo Infonavit}
     K_GLOBAL_FILE_INCA_SUA            = 200;{SUA: Archivo Incapacidades}
     K_GLOBAL_FEC_1_EXP_SUA            = 68; {SUA: Fecha 1a. Exportaci�n}
     K_GLOBAL_DELETE_FILE_SUA          = 69; {SUA: Bborrar Archivos}
     K_GLOBAL_TOPE_INFONAVIT           = 70; {Tope Amortizaci�n INFONAVIT}
     K_GLOBAL_INICIO_AMORTIZACION      = 159;      //MV: Se uso este lugar para el siguiente Global 22/Jul/2002
     K_GLOBAL_USAR_ACUM_AMORTIZACION   = 203;{Usar monto acumulado de amortizaci�n}
     K_GLOBAL_CONCEPTO_AMORTIZACION    = 204;{Concepto de Amortizaci�n de INFONAVIT}
     K_GLOBAL_INCLUYE_FI_SUA           = 214; {SUA: Incluir Faltas Injustificadas?)}
     K_GLOBAL_INFO_DESCUENTO_INFONAVIT = 292; {IMSS: AV Tipo de Informaci�n de la Conciliaci�n Infonavit 2011/03/16}
     K_GLOBAL_CREDITO_UMA              = 327; {Valor del Tipo de Cr�dito UMA}
     {US #13392 Global para indicar tipo de tope de amortizaci�n INFONAVIT}
     K_GLOBAL_TIPO_TOPE_AMORTIZACION_INFONAVIT = 326; {Tipo de tope de amortizaci�n INFONAVIT (SMGDF o UMA}

     {Capacitaci�n}
     K_GLOBAL_GIRO_EMPRESA             = 80; {Giro de la Empresa}
     K_GLOBAL_REGISTRO_STPS            = 81; {# de Registro ante STPS}
     K_GLOBAL_REPRESENTA_STPS_EMPRESA  = 82; {Representa Empresa STPS}
     K_GLOBAL_REPRESENTA_STPS_EMPLEADO = 83; {Representa Empleados STPS}
     K_NIVEL_PROGRAMACION_CURSOS       = 125;
     K_NIVEL_PROGRAMACION_CERTIFIC     = 228;{Criterio de Programaci�n de Certificaciones}


     {Defaults de Captura de Empleados}
     {Boton Empleados}
     K_GLOBAL_DEF_CHECA_TARJETA        = 89; { Default: Checa Tarjeta }
     K_GLOBAL_DEF_NACIONALIDAD         = 90; { Default: Nacionalidad }
     K_GLOBAL_DEF_SEXO                 = 91; { Default: Sexo }
     K_GLOBAL_DEF_SALARIO_TAB          = 92; { Default: Salario por Tabulador }
     K_GLOBAL_DEF_SALARIO_DIARIO       = 93; { Default: Salario Diario }
     K_GLOBAL_DEF_LETRA_GAFETE         = 94; { Default: Letra de Gafete }
     K_GLOBAL_DEF_ESTADO_CIVIL         = 95; { Default: C�digo de Estado Civil }
     K_GLOBAL_DEF_VIVE_EN              = 96; { Default: C�digo de Vive En }
     K_GLOBAL_DEF_VIVE_CON             = 97; { Default: C�digo de Vive Con }
     K_GLOBAL_DEF_MEDIO_TRANSPORTE     = 98; { Default: C�digo de Medio de Transporte }
     K_GLOBAL_DEF_ESTUDIOS             = 99; { Default: C�digo de Grado de Estudios }
     K_GLOBAL_DEF_CONTRATO             = 100;{ Default: Tipo de Contrato }
     K_GLOBAL_DEF_PUESTO               = 101;{ Default: C�digo de Puesto }
     K_GLOBAL_DEF_CLASIFICACION        = 102;{ Default: Clasificaci�n / Tabulador }
     K_GLOBAL_DEF_TURNO                = 103;{ Default: C�digo de Turno }
     K_GLOBAL_DEF_PRESTACIONES         = 104;{ Default: C�digo Tabla Prestaciones }
     K_GLOBAL_DEF_REGISTRO_PATRONAL    = 105;{ Default: C�digo Registro Patronal }
     K_GLOBAL_DEF_NIVEL_1              = 106;{ Default: C�digo de Nivel #1 }
     K_GLOBAL_DEF_NIVEL_2              = 107;{ Default: C�digo de Nivel #2 }
     K_GLOBAL_DEF_NIVEL_3              = 108;{ Default: C�digo de Nivel #3 }
     K_GLOBAL_DEF_NIVEL_4              = 109;{ Default: C�digo de Nivel #4 }
     K_GLOBAL_DEF_NIVEL_5              = 110;{ Default: C�digo de Nivel #5 }
     K_GLOBAL_DEF_NIVEL_6              = 111;{ Default: C�digo de Nivel #6 }
     K_GLOBAL_DEF_NIVEL_7              = 112;{ Default: C�digo de Nivel #7 }
     K_GLOBAL_DEF_NIVEL_8              = 113;{ Default: C�digo de Nivel #8 }
     K_GLOBAL_DEF_NIVEL_9              = 114;{ Default: C�digo de Nivel #9 }
{$ifdef ACS}
     K_GLOBAL_DEF_NIVEL_10              = 237;{ Default: C�digo de Nivel #10 }
     K_GLOBAL_DEF_NIVEL_11              = 238;{ Default: C�digo de Nivel #11 }
     K_GLOBAL_DEF_NIVEL_12              = 239;{ Default: C�digo de Nivel #12 }
{$endif}
     K_GLOBAL_DEF_MOTIVO_BAJA          = 115;{ Default: C�digo de Motivo de Baja }
     K_GLOBAL_NUM_EMP_AUTOMATICO       = 116; { SI/NO se desea generar el # de Empleado autom�ticamente}
     K_GLOBAL_DEF_MUNICIPIO            = 247;

     K_GLOBAL_DEF_BANCO                =  311;
     K_GLOBAL_DEF_REGIMEN              =  312;

{$ifdef COMMSCOPE}
     K_GLOBAL_FECHA_EVALUACION_DIARIA  = 316;
{$endif}


     {Cafeter�a}
     K_GLOBAL_LETRERO                  = 118;
     K_GLOBAL_AGREGAR_CON_ERROR        = 119;


     {Supervisores}
     K_GLOBAL_NIVEL_SUPERVISOR         = 120;
     K_SUPER_SEGUNDOS                  = 131;
     K_GLOBAL_VERSION_DATOS            = 133;


     {Globales de Labor}
     {Boton de Labor}
     K_GLOBAL_LABOR_ORDEN              = 134;
     K_GLOBAL_LABOR_ORDENES            = 135;
     K_GLOBAL_LABOR_PARTE              = 136;
     K_GLOBAL_LABOR_PARTES             = 137;
     K_GLOBAL_LABOR_AREA               = 138;
     K_GLOBAL_LABOR_AREAS              = 139;
     K_GLOBAL_LABOR_OPERACION          = 140;
     K_GLOBAL_LABOR_OPERACIONES        = 141;
     K_GLOBAL_LABOR_TMUERTO            = 142;
     K_GLOBAL_LABOR_MODULA1            = 143;
     K_GLOBAL_LABOR_MODULA2            = 144;
     K_GLOBAL_LABOR_MODULA3            = 145;
     K_GLOBAL_LABOR_USA_DEFSTEPS       = 146;
     K_GLOBAL_LABOR_VALIDASTEPS        = 147;
     K_GLOBAL_LABOR_TIEMPO             = 148;
     K_GLOBAL_LABOR_PIEZAS             = 149;
     K_GLOBAL_MATSUSHITA               = 150;
     K_GLOBAL_SUPER_PENALIZACION       = 151;
     K_GLOBAL_LABOR_TDEFECTO           = 175;
     K_GLOBAL_LABOR_TDEFECTOS          = 176;
     K_GLOBAL_LABOR_TIPO_TMUERTO       = 158;
     K_GLOBAL_LABOR_MULTILOTES         = 167;
     K_GLOBAL_LABOR_FECHACORTE         = 130;
     K_GLOBAL_LABOR_VALIDA_FECHACORTE  = 177;
     K_GLOBAL_LABOR_MAQUINA            = 283;
     K_GLOBAL_LABOR_MAQUINAS           = 284;
     K_GLOBAL_LABOR_PLANTILLA          = 285;
     K_GLOBAL_LABOR_PLANTILLAS         = 286;

     //CONSTANTES PARA PROGRAMA REPORTES VIA EMAIL
     {Boton Reportes Email}
     K_GLOBAL_EMAIL_HOST               = 152;
     K_GLOBAL_EMAIL_USERID             = 153;
     K_GLOBAL_EMAIL_PSWD               = 302;
     K_GLOBAL_EMAIL_PORT               = 303;
     K_GLOBAL_EMAIL_FROMADRESS         = 154;
     K_GLOBAL_EMAIL_FROMNAME           = 155;
     K_GLOBAL_EMAIL_MSGERROR           = 156;
     K_GLOBAL_EMAIL_PLANTILLA          = 157;
     K_GLOBAL_EMAIL_AUTH               = 308;

     {Presupuesto de Personal}
     K_GLOBAL_CONTEO_NIVEL1            = 161;
     K_GLOBAL_CONTEO_NIVEL2            = 162;
     K_GLOBAL_CONTEO_NIVEL3            = 163;
     K_GLOBAL_CONTEO_NIVEL4            = 164;
     K_GLOBAL_CONTEO_NIVEL5            = 165;

     {Boton bitacora}
     K_GLOBAL_GRABA_BITACORA           = 126;

     {Boton Bloqueo de Asistencia}
     K_LIMITE_MODIFICAR_ASISTENCIA     = 132;
     K_GLOBAL_TIPO_NOMINA              = 181;
     K_GLOBAL_FECHA_LIMITE             = 182;
     K_GLOBAL_BLOQUEO_X_STATUS         = 187;

     { Gafetes de Proximidad }
     K_GLOBAL_PROXIMITY_TOTAL_BYTES    = 183;
     K_GLOBAL_PROXIMITY_FACILITY_CODE  = 184;

     { Globales para Plan Carrera }
     K_GLOBAL_CARRERA_FECHA_EVAL_INICIO = 185;
     K_GLOBAL_CARRERA_FECHA_EVAL_FINAL  = 186;

     {Globales presentar cb_activo al d�a}
     K_GLOBAL_CB_ACTIVO_AL_DIA          = 190;
     K_GLOBAL_FORMULA_STATUS_ACTIVO     = 191;

     { Plan de vacaciones }
     K_GLOBAL_PLAN_VACA_DIAS_PAGO       = 192;
     K_GLOBAL_PLAN_VACA_PRENOMINA       = 193;
     K_GLOBAL_PAGO_PERIODO_ANTC         = 210;

     { Global Exclusivo para AVENT-HONDURAS }
     K_GLOBAL_AVENT                     = 212;
     K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS = 213;

     { Enrolamiento de usuarios }
     K_GLOBAL_ENROLL_AUTO_USER          = 215;
     K_GLOBAL_ENROLL_TIPO_LOGIN         = 216;
     K_GLOBAL_ENROLL_DOMINIO_AD         = 217;
     K_GLOBAL_ENROLL_FORMULA_USER       = 218;
     K_GLOBAL_ENROLL_FORMULA_PWD        = 219;
     K_GLOBAL_ENROLL_FORMULA_MAIL       = 220;
     K_GLOBAL_ENROLL_FORMULA_AD         = 221;
     K_GLOBAL_ENROLL_GRUPO_DEFAULT      = 222;
     K_GLOBAL_ENROLL_ROLES_DEFAULT      = 223;
     K_GLOBAL_ENROLL_ROLES_DEFAULT2     = 224;
     K_GLOBAL_ENROLL_CAMBIAR_PWD        = 225;

     K_GLOBAL_VERSION_RDD               = 226;

     { Conceptos de Infonavit }
     K_GLOBAL_CONCEPTOS_INFONAVIT       = 230;
     K_GLOBAL_PROVISION_INFONAVIT       = 231;
     K_GLOBAL_AJUSINFO_TCOMPARA         = 232;
     K_GLOBAL_AJUSINFO_TPRESTA          = 233;

     K_GLOBAL_EXTRA_DESCANSO_SAB        = 240;
     K_GLOBAL_EXTRA_DESCANSO_DES        = 241;

     { Globales Disponibles }
     K_GLOBAL_NOTIFICACION_TIMBRADO_DIAS = 315;
     K_GLOBAL_NOTIFICACION_TIMBRADO_ACTIVO  = 314;

     {GLOBALES ESPECIALES}
     K_GLOBAL_ESPECIAL_NUM_GLOBAL_2        = 248;
     K_GLOBAL_ESPECIAL_NUM_GLOBAL_3        = 249;
     K_GLOBAL_ESPECIAL_NUM_GLOBAL_4        = 250;
     K_GLOBAL_ESPECIAL_NUM_GLOBAL_5        = 251;
     K_GLOBAL_ESPECIAL_NUM_ENTERO_GLOBAL_1 = 252;
     K_GLOBAL_ESPECIAL_NUM_ENTERO_GLOBAL_2 = 253;
     K_GLOBAL_ESPECIAL_NUM_ENTERO_GLOBAL_3 = 254;
     K_GLOBAL_ESPECIAL_NUM_ENTERO_GLOBAL_4 = 255;
     K_GLOBAL_ESPECIAL_NUM_ENTERO_GLOBAL_5 = 256;
     K_GLOBAL_ESPECIAL_TEXTO_GLOBAL_1      = 257;
     K_GLOBAL_ESPECIAL_TEXTO_GLOBAL_2      = 258;
     K_GLOBAL_ESPECIAL_TEXTO_GLOBAL_3      = 259;
     K_GLOBAL_ESPECIAL_TEXTO_GLOBAL_4      = 260;
     K_GLOBAL_ESPECIAL_TEXTO_GLOBAL_5      = 261;
     {$IFDEF CALSONIC}
     K_GLOBAL_ESPECIAL_LOG_GLOBAL_1        = 262; //Constante para Calsonic
     {$ENDIF}
     K_GLOBAL_TIMBRADO_USAR_PERIODICIDAD_2 = 263;
     K_GLOBAL_TIMBRADO_ENVIAR_TIEMPO_EXTRA_3    = 264;
     K_GLOBAL_TIMBRADO_ENVIAR_INCAPACIDADES_4   = 265;
     K_GLOBAL_TIMBRADO_DETALLE_SUBCONTRATO_6    = 329;
     K_GLOBAL_TIMBRADO_ENVIAR_DETALLE_5    = 266;
     K_GLOBAL_ESPECIAL_FECHA_GLOBAL_1      = 267;
     K_GLOBAL_ESPECIAL_FECHA_GLOBAL_2      = 268;
     K_GLOBAL_ESPECIAL_FECHA_GLOBAL_3      = 269;
     K_GLOBAL_ESPECIAL_FECHA_GLOBAL_4      = 270;
     K_GLOBAL_ESPECIAL_FECHA_GLOBAL_5      = 271;
     K_GLOBAL_DIASNT_HABILES               = 272;
     K_GLOBAL_ESPECIAL_NUM_ENTERO_GLOBAL_6 = 273;
     K_GLOBAL_ESPECIAL_NUM_ENTERO_GLOBAL_7 = 274;
     K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1  = 275;

     K_GLOBAL_LABOR_AREA_DEFAULT        = 245; {Labor/Costeo, indica el area default que se le asigna al empleado, cuando no tiene area.}
     K_GLOBAL_LABOR_T_PRODUCTIVO        = 280; {Labor/Costeo, indica si el tiempo no registrado lo marca como productivo.}
     K_GLOBAL_LABOR_NO_ADMOS            = 276; {Labor/Costeo, indica si a los empleados que no checan tarjeta se les considerar� el c�lculo de Tiempo.}
     K_GLOBAL_LABOR_PERMISO_GOCE        = 277; {Labor/Costeo, indica si la checada especial es permiso con goce.}
     K_GLOBAL_LABOR_FESTIVOS            = 278; {Labor/Costeo, indica si la checada especial es Festivo.}
     K_GLOBAL_LABOR_DESCANSO            = 281; {Labor/Costeo, indica si la checada especial es Festivo.}
     K_GLOBAL_LABOR_SABADO              = 282;
    {Desarrollo especial de Visteon}
     K_GLOBAL_VISTEON                   = 279;

     K_GLOBAL_SOBREGIRO_CONCEPTO        = 287;
     K_GLOBAL_ENROLAMIENTO_SUPERVISORES = 288;

     K_GLOBAL_BLOQUEO_PRENOM_AUT        = 289; {Version 2011}

     {JB: Credenciales Invalidas}
     K_GLOBAL_PROCESA_CRED_INVALIDAS    = 290;

     {CV: Este global es para manejar las distintas programaciones especiales
     Se maneja un bit por cada prog. especial
     La relacion de que posicion le corresponden a cada Bit esta en ZToolsPE.eProyectoEspecial}
     K_GLOBAL_PROYECTOS_ESPECIALES      = 293;
     K_GLOBAL_NIVEL_COSTEO              = 294;
     K_GLOBAL_CALC_SAL_INTG             = 298;
     K_GLOBAL_VER_SALDO_VACACION        = 296;
     K_GLOBAL_MES_VENC_VAC              = 297;

     K_GLOBAL_EMPLEADO_CON_AJUSTE       = 295;
     K_GLOBAL_IMPRIMIR_CONCEPTOS_APOYO  = 299;
     K_GLOBAL_DIAS_COTIZADOS_FV         = 300;
     K_GLOBAL_SALARIO_TEMP              = 304;
     K_GLOBAL_INCAPACIDAD_GENERAL       = 306;
      {V2013 Mejoras configuraci�n conceptos de n�mina}
     K_GLOBAL_FESTIVO_EN_DESCANSOS      = 307;

     {***US 13038: Timbrado - Enviar notificaci�n a un grupo de usuarios cuando una nomina afectada tiene pendiente el timbrado ***}
     K_GLOBAL_NOTIFICACION_TIMBRADO_SOLO_ORDINARIAS = 325;
     {*** FIN ***}

     { ��� OJO !!!

     CUANDO SE AGREGUE UNA CONSTANTE NUEVA

     0) Primero asegurarse que no hay huecos( constantes de Globales que no se usen y estan ya declaradas ) en el arreglo
     1) SE TIENE QUE AGREGAR EN EL METODO DGLOBAL.GETTIPOGLOBAL CUANDO EL GLOBAL NO SEA TEXTO
     2) Se debe aumentar en 1 la constante K_NUM_GLOBALES ( est� al principio de esta unidad ),
        K_NUM_GLOBALES debe ser igual al valor mas alto de todos los globales.
     3) Se debe considerar la constante en la funci�n GetTipoGlobal ( est� al final de esta unidad )
     4) Agregar en el Metodo getGlobalDescripcion la constante con una descripci�n de la misma no Mayor a 50 Caracteres
     }

     {Default de Wizards}
     K_GLOBAL_DEF_PROGRAMA_POLL                 = 2001;{ Default: Programa Poll }
     K_GLOBAL_DEF_PARAMETROS_POLL               = 2002;{ Default: Par�metros Poll}
     K_GLOBAL_DEF_ARCHIVO_POLL                  = 2003;{ Default: Archivo Poll}
     K_GLOBAL_DEF_LISTADO_NOMINA                = 2004;{ Default: Listado de N�mina}
     K_GLOBAL_DEF_RECIBOS_NOMINA                = 2005;{ Default: Recibos de N�mina}
     K_GLOBAL_DEF_POLIZA                        = 2006;{ Default: Listado Poliza Contable}
     K_GLOBAL_DEF_ASCII_POLIZA                  = 2007;{ Default: Ascii Poliza Contable}
     K_GLOBAL_DEF_IMP_MOVS                      = 2008;{ Default: Archivo Importacion Mov de Nomina}
     K_GLOBAL_DEF_PROM_CONCEPTO                 = 2009;{ Default: Promediar Variables : Conceptos }
     K_GLOBAL_DEF_PROM_DIAS                     = 2010;{ Default: Promediar Variables : Dias Considerados }
     K_GLOBAL_DEF_AGUINALDO_FALTAS              = 2011;{ Default: F�rmula Para Faltas En Aguinaldo }
     K_GLOBAL_DEF_AGUINALDO_INCAPACIDADES       = 2012;{ Default: F�rmula Para Incapacidades En Aguinaldo }
     K_GLOBAL_DEF_AGUINALDO_ANTICIPOS           = 2013;{ Default: F�rmula Para Anticipos En Aguinaldo }
     K_GLOBAL_DEF_REPARTIR_AHORROS_TIPO         = 2014;{ Default: Tipo De Ahorro para Repartir Anualmente }
     K_GLOBAL_DEF_REPARTIR_AHORROS_MONTO        = 2015;{ Default: Monto Ahorrado a Repartir }
     K_GLOBAL_DEF_REPARTIR_AHORROS_PROPORCIONAL = 2016;{ Default: Proporcionar Ahorro }
     K_GLOBAL_DEF_REPARTIR_AHORROS_CONCEPTO     = 2017;{ Default: Concepto Reparto Ahorro }
     K_GLOBAL_DEF_AGUINALDO_REPORTE             = 2018;{ Default: Reporte de Aguinaldos }
     K_GLOBAL_DEF_REPARTIR_AHORROS_REPORTE      = 2019;{ Default: Reporte de Reparto de Ahorros }

     K_GLOBAL_DEF_COMP_ANUAL_INGRESO_BRUTO      = 2020;{ Default: Comp Anual Ingreso Bruto }
     K_GLOBAL_DEF_COMP_ANUAL_INGRESO_EXENTO     = 2021;{ Default: Comp Anual Ingreso Exento }
     K_GLOBAL_DEF_COMP_ANUAL_IMPUESTO_RETENIDO  = 2022;{ Default: Comp Anual Impuesto Retenido }
     K_GLOBAL_DEF_COMP_ANUAL_CREDITO_PAGADO     = 2023;{ Default: Comp Anual Credito Pagado }
     K_GLOBAL_DEF_COMP_ANUAL_CALCULO_IMPUESTO   = 2024;{ Default: Comp Anual Calculo Impuesto }
     K_GLOBAL_DEF_COMP_ANUAL_REPORTE            = 2025;{ Default: Reporte Comp Anual }
     K_GLOBAL_DEF_COMP_ANUAL_EMPLEADOS_CALCULAR = 2071;{Default: Comparativo y Declaracion Anual, default de empleados a calcular}
     K_GLOBAL_DEF_COMP_ANUAL_CONDICION          = 2072;{Default: Comparativo y Declaracion Anual, default de condicion}
     K_GLOBAL_DEF_COMP_ANUAL_FILTROS            = 2073;{Default: Comparativo y Declaracion Anual, default de filtros}

     K_GLOBAL_DEF_CRED_SAL_REPORTE              = 2026;{ Default: Reporte Decl Crd Sal }
     K_GLOBAL_DEF_CRED_SAL_ING_BRUTO            = 2027;{ Default: Decl Crd Sal Ing Bruto }
     K_GLOBAL_DEF_CRED_SAL_ING_EXENTO           = 2028;{ Default: Decl Crd Sal Ing Exento }
     K_GLOBAL_DEF_CRED_SAL_PAGADO               = 2029;{ Default: Decl Crd Sal Crd Pagado }

     K_GLOBAL_DEF_PTU_REPORTE                   = 2030;{ Default: Reporte de PTU }
     K_GLOBAL_DEF_PTU_UTILIDAD_REPARTIR         = 2031;{ Default: PTU Utilidad Repartir }
     K_GLOBAL_DEF_PTU_SUMA_DIAS                 = 2032;{ Default: PTU Suma de D�as }
     K_GLOBAL_DEF_PTU_PERCEPCIONES              = 2033;{ Default: PTU Percepciones }
     K_GLOBAL_DEF_PTU_FILTRO_DIRECTOR           = 2034;{ Default: PTU Filtro Director }
     K_GLOBAL_DEF_PTU_FILTRO_PLANTA             = 2035;{ Default: PTU Filtro Planta }
     K_GLOBAL_DEF_PTU_FILTRO_EVENTUAL           = 2036;{ Default: PTU Filtro Eventual }
     K_GLOBAL_DEF_PTU_SALARIO_TOPE              = 2037;{ Default: PTU Salario Tope }

     K_GLOBAL_DEF_PAGO_RECIBOS                  = 2038;{ Default: PTU Salario Tope }

     K_GLOBAL_DEF_CONCEPTO_X_RASTREAR           = 2039;{ Default: Concepto por Rastrear }

     K_GLOBAL_DEF_LAB_IMP_ORD_PROGRAMA          = 2040;{ Default: Programa Labor Importar Ordenes }
     K_GLOBAL_DEF_LAB_IMP_ORD_PARAMETROS        = 2041;{ Default: Par�metros Labor Importar Ordenes }
     K_GLOBAL_DEF_LAB_IMP_ORD_ARCHIVO           = 2042;{ Default: Archivo Labor Importar Ordenes }

     K_GLOBAL_DEF_LAB_IMP_PAR_PROGRAMA          = 2043;{ Default: Programa Labor Importar Partes }
     K_GLOBAL_DEF_LAB_IMP_PAR_PARAMETROS        = 2044;{ Default: Par�metros Labor Importar Partes }
     K_GLOBAL_DEF_LAB_IMP_PAR_ARCHIVO           = 2045;{ Default: Archivo Labor Importar Partes }

     K_GLOBAL_DEF_LAB_IMP_CED_PROGRAMA          = 2063;{ Default: Programa Labor Importar Partes }
     K_GLOBAL_DEF_LAB_IMP_CED_PARAMETROS        = 2064;{ Default: Par�metros Labor Importar Partes }
     K_GLOBAL_DEF_LAB_IMP_CED_ARCHIVO           = 2065;{ Default: Archivo Labor Importar Partes }

     K_GLOBAL_DEF_EMP_IMP_ALTAS_EMP             = 2046;{ Default: Archivo Importar Empleados }

     K_GLOBAL_DEF_NOM_CREDITO_AP_CONCEPTO       = 2047;{ Default: Cr�dito Aplicado Mensual }
     K_GLOBAL_DEF_NOM_CREDITO_AP_INGRESO_BRUTO  = 2048;{ Default: Cr�dito Aplicado Mensual }
     K_GLOBAL_DEF_NOM_CREDITO_AP_INGRESO_EXENTO = 2049;{ Default: Cr�dito Aplicado Mensual }
     K_GLOBAL_DEF_NOM_CREDITO_AP_DIAS           = 2050;{ Default: Cr�dito Aplicado Mensual }
     K_GLOBAL_DEF_NOM_CREDITO_AP_FACTOR_MENSUAL = 2051;{ Default: Cr�dito Aplicado Mensual }
     K_GLOBAL_DEF_NOM_CREDITO_AP_TABLA          = 2052;{ Default: Cr�dito Aplicado Mensual }
     K_GLOBAL_DEF_NOM_CREDITO_AP_RASTREAR       = 2053;{ Default: Cr�dito Aplicado Mensual }

     K_GLOBAL_DEF_EMP_RENUMERA                  = 2054;{ Default: Archivo para el wizard de Renumera }
     K_GLOBAL_RETROACT_CONCEPTOS                = 2055;{ Lista de Conceptos para el calculo de Retroactivos }
     K_GLOBAL_RETROACT_CONCEPTO_PAGAR           = 2056;{ Default: Para el concepto a pagar }

     K_GLOBAL_DEF_LAB_IMP_COM_PROGRAMA          = 2057;{ Default: Programa Labor Importar Componentes }
     K_GLOBAL_DEF_LAB_IMP_COM_PARAMETROS        = 2058;{ Default: Par�metros Labor Importar Componentes }
     K_GLOBAL_DEF_LAB_IMP_COM_ARCHIVO           = 2059;{ Default: Archivo Labor Importar Componentes }

     K_GLOBAL_DEF_DECL_ANUAL_REPORTE            = 2060;{ Default: Reporte Declaracion Anual }

     K_GLOBAL_DEF_COMENTA_SALDAR_VACA           = 2061;{ Default: Saldar Vacaciones Globales }
     K_GLOBAL_DEF_MONTH_SALDAR_VACA             = 2062;{ Default: Saldar Vacaciones Globales }

     K_GLOBAL_DEF_EMP_IMP_AHO_PRE               = 2066;{ Default: Archivo Importar Prestamos y Ahorros}
     K_GLOBAL_DEF_NOM_TIPOPRESTAAJUSTE          = 2067;{ Default: Tipo Pr�stamo para Ajuste para Ajuste de Retenci�n Fonacot}
     K_GLOBAL_DEF_NOM_TIPOPRESTAJUSTE_CALCULO   = 2068;{ Default: Tipo Pr�stamo para Ajuste para C�lculo de Pago Fonacot }

     K_GLOBAL_DEF_RDD_ARCHIVO_EXPORTAR          = 2069;{ Default: Nombre de archivo a exportar para el diccionario de datos }
     K_GLOBAL_DEF_RDD_ARCHIVO_IMPORTAR          = 2070;{ Default: Nombre de archivo a importar para el diccionario de datos }
     K_GLOBAL_DEF_IMSS_PAGO_SUGERIDODANOS       = 2074;{ Default: Valor sugerido da�os a la vivienda (JB) }

     K_GLOBAL_DEF_PREVIO_ISR_INGRESO_BRUTO      = 2075;{ Default: ISR Previo Ingreso Bruto }
     K_GLOBAL_DEF_PREVIO_ISR_INGRESO_EXENTO     = 2076;{ Default: ISR Previo Ingreso Exento }
     K_GLOBAL_DEF_PREVIO_ISR_IMPUESTO_RETENIDO  = 2077;{ Default: ISR Previo Impuesto Retenido }
     K_GLOBAL_DEF_PREVIO_ISR_SUBE_APLICADO      = 2078;{ Default: ISR Previo Credito Pagado }
     K_GLOBAL_DEF_PREVIO_ISR_CALCULO_IMPUESTO   = 2079;{ Default: ISR Previo Calculo Impuesto }
     K_GLOBAL_DEF_PREVIO_ISR_REPORTE            = 2080;{ Default: Reporte ISR Previo }



     { ******************************************************** }
     { *************** O J O*********************************** }
     {No usar globales arriba de 10000, esos globales se van a
     utilizar para el proceso de Declaracion Anual
     Los globales de la declacion anual, no se cargan junto con
     el arreglo de globales.
     Se estan filtrando para que no viajen en DSuperGlobal.ProcesaLectura }
     //Globales para las f�rmulas del Proceso de Declaracion Anual
     {Del 10001 al 100102- son campos numericos}
     K_GLOBAL_DECANUAL_PRIMER_CAMPO             = 10001;
     K_GLOBAL_DECANUAL_ULTIMO_CAMPO             = 10102; //Ultimo campo numerico

     K_GLOBAL_LABOR_SIN_EXCEPCIONES             = 16000;
     K_GLOBAL_LABOR_EXCEPCIONES_AREA            = 16001;

     { ��� OJO !!!

     CUANDO SE AGREGUE UNA CONSTANTE NUEVA

     1) Se debe aumentar en 1 la constante K_NUM_DEFAULTS ( est� al principio de esta unidad )
     2) Se debe considerar la constante en la funci�n GetTipoGlobal ( est� al final de esta unidad )

     }

     {$ifdef CAMBIOS_SENDA}
     NO_VALIDAR_REPETIDOS_ALTA          = 'N';
     VALIDAR_REPETIDOS_ALTA_ADVIRTIENDO = 'S';
     VALIDAR_REPETIDOS_ALTA_IMPIDIENDO  = 'P';
     {$endif}

type
{$IFDEF TRESS_DELPHIXE5_UP}
  TBooleano = String;
{$ELSE}
  TBooleano = String[ 1 ];
{$ENDIF}
  TEmpDefault = record
    Ciudad : String;
    Estado : String;
    Municipio : String;
    Checa : Boolean;
    Nacion : String;
    Sexo  : TBooleano;
    Credencial : TBooleano;
    EstadoCivil : TCodigo;
    ViveEn : TCodigo;
    ViveCon : TCodigo;
    MedioTransporte : TCodigo;
    Estudio : TCodigo;
    AutoSal : Boolean;
    ZonaGeografica : TBooleano;
    Patron : TCodigo;
    Salario : TPesos;
    Contrato : TCodigo;
    Puesto : TCodigo;
    Clasifi : TCodigo;
    Turno : TCodigo;
    TablaSS : TCodigo;
    Nivel1 : TCodigo;
    Nivel2 : TCodigo;
    Nivel3 : TCodigo;
    Nivel4 : TCodigo;
    Nivel5 : TCodigo;
    Nivel6 : TCodigo;
    Nivel7 : TCodigo;
    Nivel8 : TCodigo;
    Nivel9 : TCodigo;
{$ifdef ACS}
    Nivel10 : TCodigo;
    Nivel11 : TCodigo;
    Nivel12 : TCodigo;
{$endif}
    Siguiente : Boolean;
    Banco  : TCodigo;
    Regimen : integer; 
  end;


function GetTipoGlobal( const iCodigo : Integer ) : eTipoGlobal;
function GlobalToPos( const iCodigo : Integer ) : Integer;
function PosToGlobal( const iPos : Integer ) : Integer;
function GetDescripcionGlobal( const iCodigo : integer ): string;


implementation


{ Permite compactar el arreglo de Globales }
{ Los globales arriba de 2000, les resta un offset }
function GlobalToPos( const iCodigo : Integer ) : Integer;
begin
     if ( iCodigo <= K_NUM_GLOBALES ) then
        Result := iCodigo
     else
         Result := iCodigo - K_BASE_DEFAULTS + K_NUM_GLOBALES;
end;

function PosToGlobal( const iPos : Integer ) : Integer;
begin
     if ( iPos <= K_NUM_GLOBALES ) then
        Result := iPos
     else
         Result := iPos + K_BASE_DEFAULTS - K_NUM_GLOBALES;
end;

function GetTipoGlobal( const iCodigo : Integer ) : eTipoGlobal;
begin
     if ( iCodigo in [ K_GLOBAL_LLEVAR_SALDO_REDONDEO,
                       K_GLOBAL_PROMEDIAR_SALARIOS,
                       K_GLOBAL_SEPARAR_CRED_SAL,
                       K_GLOBAL_PROM_SAL_MIN,
                       K_GLOBAL_DELETE_FILE_SUA,
                       K_GLOBAL_USAR_ACUM_AMORTIZACION,
                       K_GLOBAL_AUT_X,
                       K_GLOBAL_AUT_FESTIVO_TRAB,
                       K_GLOBAL_SUMAR_JORNADAS,
                       K_GLOBAL_DEF_CHECA_TARJETA,
                       K_GLOBAL_DEF_SALARIO_TAB,
                       K_GLOBAL_NUM_EMP_AUTOMATICO,
{$ifdef DENSO}
                       K_GLOBAL_NUM_EMP_SUGIERE_ESPECIAL,
{$endif}
                       K_GLOBAL_AGREGAR_CON_ERROR,
                       K_EXTRAS_NO_CHECAN,
                       K_DEFAULT_RECIBOS_PAGADOS,
                       K_GLOBAL_LABOR_USA_DEFSTEPS,
                       K_GLOBAL_LABOR_VALIDASTEPS,
                       K_GLOBAL_MATSUSHITA,
                       {$ifndef CAMBIOS_SENDA}
                       K_GLOBAL_VALIDA_ALTA_EMP,
                       {$endif}
                       K_GLOBAL_VALIDA_HRS_EXTRAS,
                       K_GLOBAL_VALIDA_DESC_TRAB,
                       K_GLOBAL_VALIDA_PERM_CGOCE,
                       K_GLOBAL_VALIDA_PERM_CGENT,
                       K_GLOBAL_VALIDA_PERM_SGOCE,
                       K_GLOBAL_VALIDA_PERM_SGENT,
                       K_GLOBAL_CB_ACTIVO_AL_DIA] ) or
          ( iCodigo = K_GLOBAL_NO_CHECA_USAR_JORNADAS ) or
          ( iCodigo = K_GLOBAL_LABOR_MULTILOTES ) or
          ( iCodigo = K_GLOBAL_DEF_NOM_CREDITO_AP_RASTREAR ) or
          ( iCodigo = K_GLOBAL_PRIMA_DOMINICAL_COMPLETA ) or
          ( iCodigo = K_GLOBAL_PRIMA_DOMINICAL_TIPO_DIA ) or
          ( iCodigo = K_GLOBAL_RH_VALIDA_ORGANIGRAMA ) or
          ( iCodigo = K_GLOBAL_NO_VALIDAR_BAJAS_EXT ) or
          ( iCodigo = K_GLOBAL_CONSERVAR_TAM_FOTOS ) or {OP:04/06/08}
          ( iCodigo = K_GLOBAL_LABOR_VALIDA_FECHACORTE ) or
          ( iCodigo = K_GLOBAL_DESCONTAR_COMIDAS ) or
          ( iCodigo = K_GLOBAL_APROBAR_AUTORIZACIONES ) or
          ( iCodigo = K_GLOBAL_TOPE_SEMANAL_DOBLES ) or
          ( iCodigo = K_GLOBAL_BLOQUEO_X_STATUS ) or
          ( iCodigo = K_GLOBAL_PLAN_VACA_PRENOMINA ) or
          ( iCodigo = K_GLOBAL_PAGO_PRIMA_VACA_ANIV ) or
          ( iCodigo = K_GLOBAL_EXTRA_DESCANSO ) or
          ( iCodigo = K_GLOBAL_AVENT ) or
          ( iCodigo = K_GLOBAL_INCLUYE_FI_SUA ) or
           //Enrolamiento de Usuarios
          ( iCodigo = K_GLOBAL_ENROLL_CAMBIAR_PWD ) or
          ( iCodigo = K_GLOBAL_EXTRA_DESCANSO_SAB ) or
          ( iCodigo = K_GLOBAL_EXTRA_DESCANSO_DES ) or
          ( iCodigo = K_GLOBAL_PERMITIR_CAPTURA ) or
          ( iCodigo = K_GLOBAL_CAPTURA_OBLIGATORIA )or
          ( iCodigo = K_GLOBAL_SIM_FINIQ_APROBACION ) or
          ( iCodigo = K_GLOBAL_VALIDAR_PRESTAMOS)or{ACL: 26/06/09}
          ( iCodigo = K_GLOBAL_BLOQUEO_PRENOM_AUT ) or
          ( iCodigo = K_GLOBAL_PROCESA_CRED_INVALIDAS  ){JB: 20101108 Procesamiento de credenciales invalidas} or
          ( iCodigo = K_GLOBAL_USAR_VALIDACION_STATUS_TABLAS) {AV: 2010-11-16 }  or
          ( iCodigo = K_GLOBAL_ENROLAMIENTO_SUPERVISORES )    {AV: 2011-01-24 }  or
          ( iCodigo = K_GLOBAL_CALC_SAL_INTG ) or
          ( iCodigo = K_GLOBAL_VER_SALDO_VACACION ) or   {EZ }
          ( iCodigo = K_GLOBAL_DIAS_COTIZADOS_FV ) or {ER: 2012-03-16 }
          ( iCodigo = K_GLOBAL_FESTIVO_EN_DESCANSOS ) or {ER: 2012-03-16 }
          ( iCodigo = K_GLOBAL_SALARIO_TEMP ) or {ER: 2012-03-16 }
          ( iCodigo = K_GLOBAL_IMPRIMIR_CONCEPTOS_APOYO ) or
          ( iCodigo = K_GLOBAL_PERMISOS_DIAS_HABILES)  or
          ( iCodigo = K_GLOBAL_TIMBRADO_ENVIAR_DETALLE_5 ) or
          ( iCodigo = K_GLOBAL_TIMBRADO_ENVIAR_INCAPACIDADES_4 ) or
          ( iCodigo = K_GLOBAL_TIMBRADO_ENVIAR_TIEMPO_EXTRA_3 ) or
          ( iCodigo = K_GLOBAL_TIMBRADO_USAR_PERIODICIDAD_2 ) or
          ( iCodigo = K_GLOBAL_TIMBRADO_DETALLE_SUBCONTRATO_6 ) or
          {$IFDEF CALSONIC}
          ( iCodigo = K_GLOBAL_ESPECIAL_LOG_GLOBAL_1 )  or
          {$ENDIF}
          ( iCodigo =  K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1  ) or
          {***US 13038: Timbrado - Enviar notificaci�n a un grupo de usuarios cuando una nomina afectada tiene pendiente el timbrado***}
          ( iCodigo =  K_GLOBAL_NOTIFICACION_TIMBRADO_SOLO_ORDINARIAS  ) or
          ( iCodigo =  K_GLOBAL_NOTIFICACION_TIMBRADO_ACTIVO  ) or
          ( iCodigo = K_GLOBAL_FONACOT_PRESTAMO_CONCILIA )
          then
     begin
          Result := tgBooleano;
     end
     else
     if ( iCodigo in [ K_GLOBAL_REDONDEO_NETO,
                     K_GLOBAL_PORCEN_SUBSIDIO,
                     K_GLOBAL_NUM_GLOBAL1..K_GLOBAL_NUM_GLOBAL3,
                     K_GLOBAL_NUM_GLOBAL4..K_GLOBAL_NUM_GLOBAL7,
                     K_GLOBAL_NUMERO_GLOBAL8,
                     K_GLOBAL_TOPE_INFONAVIT,
                     K_GLOBAL_NUM_GLOBAL9..K_GLOBAL_NUM_GLOBAL10,
                     K_GLOBAL_DEF_SALARIO_DIARIO ] ) or
         ( iCodigo = K_GLOBAL_DEF_REPARTIR_AHORROS_MONTO ) or
         ( iCodigo = K_GLOBAL_DEF_PTU_UTILIDAD_REPARTIR ) or
         ( iCodigo = K_GLOBAL_DEF_PTU_SALARIO_TOPE ) or
         ( iCodigo = K_GLOBAL_PIRAMIDA_TOLERANCIA ) or
         ( iCodigo = K_GLOBAL_DEF_IMSS_PAGO_SUGERIDODANOS ) then
     begin
          Result := tgFloat;
     end
     else
     if ( iCodigo in [ K_GLOBAL_PRIMER_DIA,
                       K_GLOBAL_FORMATO_POLIZA,
                       K_GLOBAL_FORMATO_BANCA,
                       K_GLOBAL_ADICIONAL_RETIRO,
                       K_GLOBAL_CONCEPTO_AMORTIZACION,
                       K_GLOBAL_NIVEL_SUPERVISOR ] ) or
          ( iCodigo =  K_GLOBAL_DEF_LISTADO_NOMINA ) or
          ( iCodigo =  K_GLOBAL_DEF_RECIBOS_NOMINA ) or
          ( iCodigo =  K_GLOBAL_DEF_ASCII_POLIZA ) or
          ( iCodigo =  K_GLOBAL_DEF_REPARTIR_AHORROS_CONCEPTO ) or
          ( iCodigo =  K_GLOBAL_DEF_AGUINALDO_REPORTE ) or
          ( iCodigo =  K_GLOBAL_DEF_REPARTIR_AHORROS_REPORTE ) or
          ( iCodigo =  K_GLOBAL_DEF_COMP_ANUAL_REPORTE ) or
          ( iCodigo =  K_GLOBAL_DEF_CRED_SAL_REPORTE ) or
          ( iCodigo =  K_GLOBAL_DEF_PTU_REPORTE ) or
          ( iCodigo in [ K_LIMITE_MODIFICAR_ASISTENCIA,
                         K_NIVEL_PROGRAMACION_CURSOS,
                         K_NIVEL_PROGRAMACION_CERTIFIC,
                         K_SUPER_SEGUNDOS ] ) or
          ( iCodigo in [ K_GLOBAL_TAB_REDON_ORD..K_GLOBAL_GRACIA_2CHECK ] ) or
          ( iCodigo in [ K_GLOBAL_ISPT..K_GLOBAL_INCOBRABLES] ) or
          ( iCodigo in [ K_GLOBAL_TRATO_X_SAB..K_GLOBAL_TRATO_X_FESTIVO ] ) or
          ( iCodigo = K_GLOBAL_SUPER_PENALIZACION ) or
          ( iCodigo in [ K_GLOBAL_CONTEO_NIVEL1..K_GLOBAL_CONTEO_NIVEL5 ] ) or
          ( iCodigo = K_GLOBAL_HRS_EXENTAS_IMSS ) or
          ( iCodigo = K_GLOBAL_INICIO_AMORTIZACION ) or
          ( iCodigo = K_GLOBAL_DEF_NOM_CREDITO_AP_CONCEPTO ) or
          ( iCodigo = K_GLOBAL_DEF_NOM_CREDITO_AP_TABLA ) or
          ( iCodigo = K_GLOBAL_DIAS_VACACIONES ) or
          ( iCodigo = K_GLOBAL_DEF_DECL_ANUAL_REPORTE ) or
          ( iCodigo = K_GLOBAL_DEF_MONTH_SALDAR_VACA ) or
          ( iCodigo = K_GLOBAL_TIPO_NOMINA ) or
          ( iCodigo = K_GLOBAL_PROXIMITY_TOTAL_BYTES ) or
          ( iCodigo = K_GLOBAL_PLAN_VACA_DIAS_PAGO ) or
          ( iCodigo = K_GLOBAL_PIRAMIDA_CONCEPTO ) or
          ( iCodigo = K_GLOBAL_TRATAMIENTO_TIEMPO_EXTRA ) or
          ( iCodigo = K_GLOBAL_DIAS_APLICA_EXENTO )or
          ( iCodigo = K_GLOBAL_SIMULACION_FINIQUITOS)or
          ( iCodigo = K_GLOBAL_PIRAMIDA_PRESTAMOS)or
          ( iCodigo = K_GLOBAL_TOPE_PRESTAMOS ) or
          ( iCodigo = K_GLOBAL_PAGO_PERIODO_ANTC ) or
          ( iCodigo = K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ) or
          //Enrolamiento de Usuarios
          ( iCodigo = K_GLOBAL_ENROLL_AUTO_USER ) or
          ( iCodigo = K_GLOBAL_ENROLL_TIPO_LOGIN ) or
          ( iCodigo = K_GLOBAL_ENROLL_GRUPO_DEFAULT )or
          //Patch RDD 
          ( iCodigo = K_GLOBAL_VERSION_RDD ) or
          ( iCodigo = K_GLOBAL_AJUSINFO_TCOMPARA ) or
          ( iCodigo = K_GLOBAL_INFO_DESCUENTO_INFONAVIT ) or
          ( iCodigo = K_GLOBAL_NIVEL_COSTEO ) or
          ( iCodigo = K_GLOBAL_EMAIL_PORT )  or
          ( iCodigo = K_GLOBAL_EMAIL_AUTH ) or
          ( iCodigo = K_GLOBAL_MES_VENC_VAC ) or
          ( iCodigo = K_GLOBAL_DEF_PREVIO_ISR_REPORTE ) or
          ( iCodigo = K_GLOBAL_NOMINA_METODO_DEFAULT ) or
          ( iCodigo = K_GLOBAL_DEF_REGIMEN ) or
          {***US 13038: Timbrado - Enviar notificaci�n a un grupo de usuarios cuando una nomina afectada tiene pendiente el timbrado ***}
          ( iCodigo = K_GLOBAL_NOTIFICACION_TIMBRADO_DIAS ) or
          ( iCodigo = K_GLOBAL_CREDITO_UMA ) or
          {US #13392 Global para indicar tipo de tope de amortizaci�n INFONAVIT}
          ( iCodigo = K_GLOBAL_TIPO_TOPE_AMORTIZACION_INFONAVIT )
           then
     begin
          Result := tgNumero;
     end
     else
     if ( iCodigo = K_GLOBAL_FEC_1_EXP_SUA ) or
        ( iCodigo = K_GLOBAL_LABOR_FECHACORTE )or
        ( iCodigo = K_GLOBAL_FECHA_LIMITE ) or
        ( iCodigo = K_GLOBAL_CARRERA_FECHA_EVAL_INICIO ) or
        ( iCodigo = K_GLOBAL_CARRERA_FECHA_EVAL_FINAL )
        {$ifdef COMMSCOPE}
        or ( iCodigo = K_GLOBAL_FECHA_EVALUACION_DIARIA )
        {$endif}
         then
         Result := tgFecha
     else
         Result := tgTexto;
end;

function GetDescripcionGlobal( const iCodigo : integer ): string;
begin
     if iCodigo = K_GLOBAL_SALARIO_TEMP then
        Result := 'Tarjetas con Salario Temporal'
     else
     begin
     case iCodigo of
          {Boton Identificacion}
          K_GLOBAL_RAZON_EMPRESA    :  Result := 'Raz�n Social de Empresa';
          K_GLOBAL_CALLE_EMPRESA    :  Result := 'Calle de Empresa';
          K_GLOBAL_COLONIA_EMPRESA  :  Result := 'Colonia de Empresa';
          K_GLOBAL_CIUDAD_EMPRESA   :  Result := 'Ciudad de Empresa';
          K_GLOBAL_ENTIDAD_EMPRESA  :  Result := 'Entidad de Empresa';
          K_GLOBAL_CP_EMPRESA       :  Result := 'C�digo Postal Empresa';
          K_GLOBAL_TEL_EMPRESA      :  Result := 'Tel�fono de Empresa';
          K_GLOBAL_RFC_EMPRESA      :  Result := 'R.F.C. de Empresa';
          K_GLOBAL_INFONAVIT_EMPRESA:  Result := '# de INFONAVIT de Empresa';
          K_GLOBAL_REPRESENTANTE    :  Result := 'Representante Legal';
          K_GLOBAL_NUM_EXT_EMPRESA  :  Result := '# Exterior de Domicilio Fiscal';
          K_GLOBAL_NUM_INT_EMPRESA  :  Result := '# Interior de Domicilio Fiscal';

          {Boton de Areas- Niveles de Organigrama }
          K_GLOBAL_NIVEL1: Result := 'Nombre de Nivel #1';
          K_GLOBAL_NIVEL2: Result := 'Nombre de Nivel #2';
          K_GLOBAL_NIVEL3: Result := 'Nombre de Nivel #3';
          K_GLOBAL_NIVEL4: Result := 'Nombre de Nivel #4';
          K_GLOBAL_NIVEL5: Result := 'Nombre de Nivel #5';
          K_GLOBAL_NIVEL6: Result := 'Nombre de Nivel #6';
          K_GLOBAL_NIVEL7: Result := 'Nombre de Nivel #7';
          K_GLOBAL_NIVEL8: Result := 'Nombre de Nivel #8';
          K_GLOBAL_NIVEL9: Result := 'Nombre de Nivel #9';
{$ifdef ACS}
          K_GLOBAL_NIVEL10: Result := 'Nombre de Nivel #10';
          K_GLOBAL_NIVEL11: Result := 'Nombre de Nivel #11';
          K_GLOBAL_NIVEL12: Result := 'Nombre de Nivel #12';
{$endif}
          {Boton Adicionales - Campos Adicionales }

          K_GLOBAL_FECHA1: Result := 'Nombre de Fecha #1';
          K_GLOBAL_FECHA2: Result := 'Nombre de Fecha #2';
          K_GLOBAL_FECHA3: Result := 'Nombre de Fecha #3';

          K_GLOBAL_TEXTO1: Result := 'Nombre de Texto #1';
          K_GLOBAL_TEXTO2: Result := 'Nombre de Texto #2';
          K_GLOBAL_TEXTO3: Result := 'Nombre de Texto #3';
          K_GLOBAL_TEXTO4: Result := 'Nombre de Texto #4';

          K_GLOBAL_NUM1: Result := 'Nombre de N�mero #1';
          K_GLOBAL_NUM2: Result := 'Nombre de N�mero #2';
          K_GLOBAL_NUM3: Result := 'Nombre de N�mero #3';

          K_GLOBAL_LOG1: Result := 'Nombre de L�gico #1';
          K_GLOBAL_LOG2: Result := 'Nombre de L�gico #2';
          K_GLOBAL_LOG3: Result := 'Nombre de L�gico #3';

          K_GLOBAL_TAB1: Result := 'Nombre de Tabla #1';
          K_GLOBAL_TAB2: Result := 'Nombre de Tabla #2';
          K_GLOBAL_TAB3: Result := 'Nombre de Tabla #3';
          K_GLOBAL_TAB4: Result := 'Nombre de Tabla #4';

          {Boton Asistencia}
          K_INCIDENCIA_FI                 : Result := 'Incidencia Falta Injustificada';
          K_GLOBAL_PRIMER_DIA             : Result := 'Primer d�a de la semana';
          K_GLOBAL_GRACIA_2CHECK          : Result := 'Gracia Checadas Repetidas';
          K_GLOBAL_AUT_X                  : Result := 'Autorizaci�n para Pagar Horas Extras';
          K_GLOBAL_AUT_FESTIVO_TRAB       : Result := 'Autorizaci�n para Pagar Festivo Trabajado';
          K_GLOBAL_TRATO_X_SAB            : Result := 'Trato Extras en S�bado';
          K_GLOBAL_TRATO_X_DESC           : Result := 'Trato Extras en Descanso';
          K_GLOBAL_TRATO_X_FESTIVO        : Result := 'Trato Extras en Festivo';
          K_GLOBAL_SUMAR_JORNADAS         : Result := 'Sumar Jornadas de Horarios';
          K_EXTRAS_NO_CHECAN              : Result := 'Extras para Empleados que no Checan';
          K_GLOBAL_PERMISOS_DIAS_HABILES  : Result := 'Considerar D�as de Permiso como D�as H�biles';
          K_GLOBAL_NO_CHECA_USAR_JORNADAS : Result := 'Jornadas para Empleados que no Checan';
          K_GLOBAL_TAB_REDON_ORD          : Result := 'Tabla Redondeo Hrs. Ordinarias';
          K_GLOBAL_TAB_REDON_X            : Result := 'Tabla Redondeo Hrs. Extras';
          K_GLOBAL_HRS_EXENTAS_IMSS       : Result := 'Horas Exentas para IMSS (Regla &3x3)';
          K_GLOBAL_VALIDA_HRS_EXTRAS      : Result := 'Mot. Aut. Obligatorio Hrs. Extras';
          K_GLOBAL_VALIDA_DESC_TRAB       : Result := 'Mot. Aut. Obligatorio Desc. Trabajado';
          K_GLOBAL_VALIDA_PERM_CGOCE      : Result := 'Mot. Aut. Obligatorio Permiso CG';
          K_GLOBAL_VALIDA_PERM_CGENT      : Result := 'Mot. Aut. Obligatorio Permiso CG\Ent';
          K_GLOBAL_VALIDA_PERM_SGOCE      : Result := 'Mot. Aut. Obligatorio Permiso SG';
          K_GLOBAL_VALIDA_PERM_SGENT      : Result := 'Mot. Aut. Obligatorio Permiso SG\Ent';
          K_GLOBAL_DESCONTAR_COMIDAS      : Result := 'Autorizar Descuento de Comidas';
          K_GLOBAL_APROBAR_AUTORIZACIONES : Result := 'Aprobar Autorizaciones';
          K_GLOBAL_TOPE_SEMANAL_DOBLES    : Result := 'Tope de dobles corte semanal';
          K_GLOBAL_TRATAMIENTO_TIEMPO_EXTRA : Result := 'Tratamiento de tiempo extra';
          K_GLOBAL_DIAS_APLICA_EXENTO     : Result := 'Dias en que aplica horas exentas para IMSS';
          K_GLOBAL_EXTRA_DESCANSO         : Result := 'Festivo: Aut. como extra o descanso';
          K_GLOBAL_EXTRA_DESCANSO_SAB     : Result := 'S�bado: Aut. como extra o descanso';
          K_GLOBAL_EXTRA_DESCANSO_DES     : Result := 'Descanso: Aut. como extra o descanso';
          K_GLOBAL_PERMITIR_CAPTURA       : Result := 'Permitir Captura';
          K_GLOBAL_CAPTURA_OBLIGATORIA   : Result := 'Captura Obligatoria';

          {Boton N�mina}
          K_GLOBAL_REDONDEO_NETO            : Result := 'Redondeo de Neto';
          K_GLOBAL_LLEVAR_SALDO_REDONDEO    : Result := 'Llevar Saldo de Redondeo';
          K_GLOBAL_PROMEDIAR_SALARIOS       : Result := 'Promediar Salarios en Cambios';
          K_GLOBAL_PORCEN_SUBSIDIO          : Result := 'Proporci�n del Subsidio';//'% Subsidio Acreditable';
          K_GLOBAL_FORMATO_POLIZA           : Result := 'Formato P�liza Contable';
          K_GLOBAL_FORMATO_BANCA            : Result := 'Banca Electr�nica';
          K_GLOBAL_PRIMA_DOMINICAL_COMPLETA : Result := 'Prima Dominical Solo Considera Horas de Domingo';
          K_GLOBAL_PRIMA_DOMINICAL_TIPO_DIA : Result := 'Prima Dominical Se Calcula Para Todos los Tipos de D�a';
          K_GLOBAL_ISPT                     : Result := 'Concepto para ISR';
     {$ifdef ANTES}
          K_GLOBAL_CREDITO                  : Result := 'Concepto Cr�dito al Salario';
     {$else}
          K_GLOBAL_CREDITO                  : Result := 'Concepto Subsidio al Empleo';
     {$endif}
          K_GLOBAL_AJUSTE                   : Result := 'Concepto Ajuste de Moneda';
          K_GLOBAL_INCOBRABLES              : Result := 'Concepto Deduc. Incobrables';
     {$ifdef ANTES}
          K_GLOBAL_SEPARAR_CRED_SAL         : Result := 'Separar Cr�dito al Salario';
     {$else}
          K_GLOBAL_SEPARAR_CRED_SAL         : Result := 'Separar Subsidio al Empleo';
     {$endif}
          K_GLOBAL_PROM_SAL_MIN             : Result := 'Promediar Salarios M�nimos';
          K_DEFAULT_RECIBOS_PAGADOS	    : Result := 'Default para Recibo Pagados';
          K_TITULO_MONTO_ADICIONAL_RECIBO   : Result := 'T�tulo Monto Adicional';
          K_FORMULA_MONTO_ADICIONAL_RECIBO  : Result := 'F�rmula Monto Adicional';
          K_GLOBAL_PIRAMIDA_TOLERANCIA      : Result := 'Tolerancia para piramidaci�n';
          K_GLOBAL_PIRAMIDA_CONCEPTO        : Result := 'Concepto de piramidaci�n';
          K_GLOBAL_PIRAMIDA_PRESTAMOS       : Result := 'Descuento de Ahorros y Prestamos para Piramidaci�n';
          K_GLOBAL_SIMULACION_FINIQUITOS    : Result := 'Simulaci�n de finiquitos de Recursos Humanos';
          K_GLOBAL_SIM_FINIQ_APROBACION     : Result := 'Simulaci�n de Finiquitos Requiere Aprobaci�n ';
          K_GLOBAL_TOPE_PRESTAMOS           : Result := 'Concepto Tope de Pr�stamos y Ahorros';
          K_GLOBAL_FONACOT_PRESTAMO         : Result := 'Tipo de pr�stamo de FONACOT';

          K_GLOBAL_NOMINA_METODO_DEFAULT    : Result := 'M�todo predeterminado de pago de la n�mina';

          {Boton Variables Globales}
          K_GLOBAL_NUM_GLOBAL1    : Result := 'N�mero Global #1';
          K_GLOBAL_NUM_GLOBAL2    : Result := 'N�mero Global #2';
          K_GLOBAL_NUM_GLOBAL3    : Result := 'N�mero Global #3';
          K_GLOBAL_NUM_GLOBAL4    : Result := 'N�mero Global #4';
          K_GLOBAL_NUM_GLOBAL5    : Result := 'N�mero Global #5';
          K_GLOBAL_NUM_GLOBAL6    : Result := 'N�mero Global #6';
          K_GLOBAL_NUM_GLOBAL7    : Result := 'N�mero Global #7';
          K_GLOBAL_NUMERO_GLOBAL8 : Result := 'N�mero Global #8';
          K_GLOBAL_NUM_GLOBAL9    : Result := 'N�mero Global #9';
          K_GLOBAL_NUM_GLOBAL10   : Result := 'N�mero Global #10';

          K_GLOBAL_TEXT_GLOBAL1 : Result := 'Texto Global #1';
          K_GLOBAL_TEXT_GLOBAL2 : Result := 'Texto Global #2';
          K_GLOBAL_TEXT_GLOBAL3 : Result := 'Texto Global #3';

          {Boton Recursos}
          K_GLOBAL_NUM_IMSS              : Result := '# de Gu�a IMSS';
          K_GLOBAL_ZONAS_GEOGRAFICAS     : Result := 'Zonas Geogr�ficas';
          K_GLOBAL_DIR_PLANT             : Result := 'Directorio Plantillas';
          K_GLOBAL_DIAS_VACACIONES       : Result := 'Valor de D�as en Vacaciones';
          K_GLOBAL_VALIDA_ALTA_EMP       : Result := 'Validar Repetidos en Alta de Empleados';
          K_GLOBAL_RH_VALIDA_ORGANIGRAMA : Result := 'Usar Validaciones de Organigrama';
          K_GLOBAL_NO_VALIDAR_BAJAS_EXT  : Result := 'No Validar Bajas Extempor�neas';
          K_GLOBAL_CONSERVAR_TAM_FOTOS   : Result := 'Mantener el tama�o original de las fotos'; {OP: 04/06/08}
          K_GLOBAL_EMPRESA_SELECCION     : Result := 'Empresa de Selecci�n de Personal';
          K_GLOBAL_PAGO_PRIMA_VACA_ANIV  : Result := 'Pago autom�tico de prima vacacional';
          K_GLOBAL_VALIDAR_PRESTAMOS     : Result := 'Validar P�liticas para otorgar Pr�stamos';{ACL: 26/06/09}
          K_GLOBAL_PROCESA_CRED_INVALIDAS: Result := 'Procesar Credenciales Inv�lidas';{JB: 20101108 Procesamiento de credenciales invalidas}
          K_GLOBAL_USAR_VALIDACION_STATUS_TABLAS : Result := 'Usar validaci�n de estatus de tablas y cat�logos'; {AV: 20101116}
          K_GLOBAL_VER_SALDO_VACACION    : Result := 'Mostrar Saldo de Vacaciones en Expediente'; {AV: 20101116}
          {IMSS/INFONAVIT}
          K_GLOBAL_ADICIONAL_RETIRO    : Result := 'Concepto Adicional Retiro';
          K_GLOBAL_TOPE_INFONAVIT      : Result := 'Tope Amortizaci�n INFONAVIT';
          K_GLOBAL_FORM_PERCEP         : Result := 'F�rmula M�d. 18 Percepciones';
          K_GLOBAL_FORM_DIAS           : Result := 'F�rmula M�d. 18 D�as';
          K_GLOBAL_FILE_EMP_SUA        : Result := 'SUA: Archivo Empleados';
          K_GLOBAL_FILE_MOVS_SUA       : Result := 'SUA: Archivo Movimientos';
          K_GLOBAL_FILE_AFIL_SUA       : Result := 'SUA: Archivo Datos Afiliatorios';
          K_GLOBAL_FILE_INFO_SUA       : Result := 'SUA: Archivo Infonavit';
          K_GLOBAL_FILE_INCA_SUA       : Result := 'SUA: Archivo Incapacidades';
          K_GLOBAL_DIR_DATOS_SUA       : Result := 'SUA: Directorio de Datos';
          K_GLOBAL_FEC_1_EXP_SUA       : Result := 'SUA: Fecha 1a. Exportaci�n';
          K_GLOBAL_DELETE_FILE_SUA     : Result := 'SUA: Borrar Archivos';
          K_GLOBAL_INICIO_AMORTIZACION : Result := 'Amortizaci�n Cuota Fija o SMGDF';
          K_GLOBAL_USAR_ACUM_AMORTIZACION: Result := 'Usar Acumulado de Amortizaci�n';
          K_GLOBAL_CONCEPTO_AMORTIZACION: Result := 'Concepto de Amortizaci�n';
          K_GLOBAL_CONCEPTOS_INFONAVIT : Result := 'Conceptos de Infonavit';
          K_GLOBAL_INFO_DESCUENTO_INFONAVIT : Result := 'Tipo de Informaci�n de la Conciliaci�n Infonavit';
          K_GLOBAL_CREDITO_UMA         : Result := 'Valor de Tipo de Cr�dito UMA';
          K_GLOBAL_FONACOT_PRESTAMO_CONCILIA: Result := 'Administrar pr�stamos sin conciliaci�n';
          {US #13392 Global para indicar tipo de tope de amortizaci�n INFONAVIT}
          K_GLOBAL_TIPO_TOPE_AMORTIZACION_INFONAVIT : Result := 'Tipo de tope de amortizaci�n INFONAVIT';


          {Boton Capacitaci�n}
          K_GLOBAL_GIRO_EMPRESA             : Result := 'Giro de la Empresa';
          K_GLOBAL_REGISTRO_STPS            : Result := '# de Registro ante STPS';
          K_GLOBAL_REPRESENTA_STPS_EMPRESA  : Result := 'Representa Empresa STPS';
          K_GLOBAL_REPRESENTA_STPS_EMPLEADO : Result := 'Representa Empleados STPS';
          K_NIVEL_PROGRAMACION_CURSOS       : Result := 'Criterio Adicional de Programaci�n de Cursos';
          K_NIVEL_PROGRAMACION_CERTIFIC     : Result := 'Criterio Adicional de Programaci�n de Certificaciones';

          {Boton Empleados-Defaults de Captura de Empleados}
          K_GLOBAL_DEF_CHECA_TARJETA        : Result := 'Default: Checa Tarjeta';
          K_GLOBAL_DEF_NACIONALIDAD         : Result := 'Default: Nacionalidad';
          K_GLOBAL_DEF_SEXO                 : Result := 'Default: Sexo';
          K_GLOBAL_DEF_SALARIO_TAB          : Result := 'Default: Salario por Tabulador';
          K_GLOBAL_DEF_SALARIO_DIARIO       : Result := 'Default: Salario Diario';
          K_GLOBAL_DEF_LETRA_GAFETE         : Result := 'Default: Letra de Gafete';
          K_GLOBAL_DEF_ESTADO_CIVIL         : Result := 'Default: C�digo de Estado Civil';
          K_GLOBAL_DEF_VIVE_EN              : Result := 'Default: C�digo de Vive En';
          K_GLOBAL_DEF_VIVE_CON             : Result := 'Default: C�digo de Vive Con';
          K_GLOBAL_DEF_MEDIO_TRANSPORTE     : Result := 'Default: C�digo de Medio de Transporte';
          K_GLOBAL_DEF_ESTUDIOS             : Result := 'Default: C�digo de Grado de Estudios';
          K_GLOBAL_DEF_CONTRATO             : Result := 'Default: Tipo de Contrato';
          K_GLOBAL_DEF_PUESTO               : Result := 'Default: C�digo de Puesto';
          K_GLOBAL_DEF_CLASIFICACION        : Result := 'Default: Clasificaci�n\Tabulador';
          K_GLOBAL_DEF_TURNO                : Result := 'Default: C�digo de Turno';
          K_GLOBAL_DEF_PRESTACIONES         : Result := 'Default: C�digo Tabla Prestaciones';
          K_GLOBAL_DEF_REGISTRO_PATRONAL    : Result := 'Default: C�digo Registro Patronal';
          K_GLOBAL_DEF_NIVEL_1              : Result := 'Default: C�digo de Nivel #1';
          K_GLOBAL_DEF_NIVEL_2              : Result := 'Default: C�digo de Nivel #2';
          K_GLOBAL_DEF_NIVEL_3              : Result := 'Default: C�digo de Nivel #3';
          K_GLOBAL_DEF_NIVEL_4              : Result := 'Default: C�digo de Nivel #4';
          K_GLOBAL_DEF_NIVEL_5              : Result := 'Default: C�digo de Nivel #5';
          K_GLOBAL_DEF_NIVEL_6              : Result := 'Default: C�digo de Nivel #6';
          K_GLOBAL_DEF_NIVEL_7              : Result := 'Default: C�digo de Nivel #7';
          K_GLOBAL_DEF_NIVEL_8              : Result := 'Default: C�digo de Nivel #8';
          K_GLOBAL_DEF_NIVEL_9              : Result := 'Default: C�digo de Nivel #9';
{$ifdef ACS}
          K_GLOBAL_DEF_NIVEL_10              : Result := 'Default: C�digo de Nivel #10';
          K_GLOBAL_DEF_NIVEL_11              : Result := 'Default: C�digo de Nivel #11';
          K_GLOBAL_DEF_NIVEL_12              : Result := 'Default: C�digo de Nivel #12';
{$endif}
          K_GLOBAL_DEF_MOTIVO_BAJA          : Result := 'Default: C�digo de Motivo de Baja';
          K_GLOBAL_NUM_EMP_AUTOMATICO       : Result := 'SI/NO se desea generar el # de Empleado autom�ticamente';
{$ifdef DENSO}
          K_GLOBAL_NUM_EMP_SUGIERE_ESPECIAL : Result := 'SI/NO se desea generar el # de Empleado de forma especial';
{$endif}

          K_GLOBAL_DEF_BANCO                : Result := 'Default: C�digo de Banco';
          K_GLOBAL_DEF_REGIMEN              : Result := 'Default: R�gimen Fiscal de Empleado';

{$ifdef COMMSCOPE}
          K_GLOBAL_FECHA_EVALUACION_DIARIA  : Result := 'Fecha de evaluaci�n diaria';
{$endif}

          {Boton de Cafeter�a}
          K_GLOBAL_LETRERO                  : Result := 'Letrero con Exito';
          K_GLOBAL_AGREGAR_CON_ERROR        : Result := 'Agregar Con Error al Importar';

          {Globales de Labor - Boton de Labor }
          K_GLOBAL_LABOR_ORDEN              : Result := 'Nombre del Cat�logo de Orden de Trabajo- Singular';
          K_GLOBAL_LABOR_ORDENES            : Result := 'Nombre del Catalogo de Orden de Trabajo- Plural';
          K_GLOBAL_LABOR_PARTE              : Result := 'Nombre del Cat�logo de Producto- Singular';
          K_GLOBAL_LABOR_PARTES             : Result := 'Nombre del Cat�logo de Producto- Plural';
          K_GLOBAL_LABOR_AREA               : Result := 'Nombre del Cat�logo de Area- Singular';
          K_GLOBAL_LABOR_AREAS              : Result := 'Nombre del Cat�logo de Area- Plural';
          K_GLOBAL_LABOR_OPERACION          : Result := 'Nombre del Cat�logo de Operaci�n- Singular';
          K_GLOBAL_LABOR_OPERACIONES        : Result := 'Nombre del Cat�logo de Operaci�n- Plural';
          K_GLOBAL_LABOR_TDEFECTO           : Result := 'Nombre del Cat�logo de Tipo Defecto- Singular';
          K_GLOBAL_LABOR_TDEFECTOS          : Result := 'Nombre del Cat�logo de Tipo Defecto- Plural';
          K_GLOBAL_LABOR_TMUERTO            : Result := 'Nombre del Cat�logo de Tiempo Muerto';
          K_GLOBAL_LABOR_MODULA1            : Result := 'Nombre de Modulador #1';
          K_GLOBAL_LABOR_MODULA2            : Result := 'Nombre de Modulador #2';
          K_GLOBAL_LABOR_MODULA3            : Result := 'Nombre de Modulador #3';
          K_GLOBAL_LABOR_USA_DEFSTEPS       : Result := '�Usar Pasos (Operaciones) Predefinidos?';
          K_GLOBAL_LABOR_VALIDASTEPS        : Result := '�Se requiere Validaci�n al dar de Alta los Pasos?';
          K_GLOBAL_LABOR_TIEMPO             : Result := 'Unidad de Tiempo';
          K_GLOBAL_LABOR_PIEZAS             : Result := 'Piezas';
          K_GLOBAL_MATSUSHITA               : Result := 'Matsuchita';
          K_GLOBAL_LABOR_TIPO_TMUERTO       : Result := 'Tiempo Muerto NO Registrado';
          K_GLOBAL_LABOR_MULTILOTES         : Result := '�Trabaja con Multilotes?';
          K_GLOBAL_LABOR_FECHACORTE         : Result := 'Fecha de Corte para Cambios en Labor';
          K_GLOBAL_LABOR_VALIDA_FECHACORTE  : Result := '�Usar Fecha de Corte Para Cambios?';

          {Boton de Supervisores}
          K_GLOBAL_NIVEL_SUPERVISOR         : Result := 'Nivel de Organigrama de Supervisores';
          K_GLOBAL_SUPER_PENALIZACION       : Result := 'Penalizaci�n en Captura Manual';
          K_SUPER_SEGUNDOS                  : Result := 'L�mite de Inactividad';

          {CONSTANTES PARA PROGRAMA REPORTES VIA EMAIL}
          {Boton Reportes Email}
          K_GLOBAL_EMAIL_HOST       : Result := 'Servidor de Correos';
          K_GLOBAL_EMAIL_USERID     : Result := 'ID. Usuario';
          K_GLOBAL_EMAIL_FROMADRESS : Result := 'Direcci�n del Email';
          K_GLOBAL_EMAIL_FROMNAME   : Result := 'Descripci�n del Email';
          K_GLOBAL_EMAIL_MSGERROR   : Result := 'Direcci�n para Errores';
          K_GLOBAL_EMAIL_PLANTILLA  : Result := 'Plantilla Default';
          K_GLOBAL_EMAIL_AUTH       : Result := 'M�todo de Autenticaci�n';


          {Presupuesto de Personal}
          K_GLOBAL_CONTEO_NIVEL1  : Result := 'Presupuesto de Personal Citerio #1';
          K_GLOBAL_CONTEO_NIVEL2  : Result := 'Presupuesto de Personal Citerio #2';
          K_GLOBAL_CONTEO_NIVEL3  : Result := 'Presupuesto de Personal Citerio #3';
          K_GLOBAL_CONTEO_NIVEL4  : Result := 'Presupuesto de Personal Citerio #4';
          K_GLOBAL_CONTEO_NIVEL5  : Result := 'Presupuesto de Personal Citerio #5';

          {Boton bitacora}
          K_GLOBAL_GRABA_BITACORA : Result := 'Movimientos que se registran en Bit�cora';

          {Bot�n de Bloqueo de Asistencia}
          K_LIMITE_MODIFICAR_ASISTENCIA   : Result := 'Status L�mite para Ajustar Asistencia';
          K_GLOBAL_TIPO_NOMINA            : Result := 'Tipo De N�mina Para Modificar Tarjetas';
          K_GLOBAL_FECHA_LIMITE           : Result := 'Fecha L�mite De Cambios A Tarjetas';
          K_GLOBAL_BLOQUEO_X_STATUS       : Result := 'Bloqueo por Status de la N�mina';

          {Version de Datos}
          K_GLOBAL_VERSION_DATOS    : Result := 'Versi�n de Datos';

          {Default de Wizards}
          K_GLOBAL_DEF_PROGRAMA_POLL                 : Result :=  'Proceso Poll Relojes: Programa Poll';
          K_GLOBAL_DEF_PARAMETROS_POLL               : Result :=  'Proceso Poll Relojes: Par�metros Poll';
          K_GLOBAL_DEF_ARCHIVO_POLL                  : Result :=  'Proceso Poll Relojes: Archivo Poll';
          K_GLOBAL_DEF_LISTADO_NOMINA                : Result :=  'Listado de N�mina';
          K_GLOBAL_DEF_RECIBOS_NOMINA                : Result :=  'Recibos de N�mina';
          K_GLOBAL_DEF_POLIZA                        : Result :=  'Listado Poliza Contable';
          K_GLOBAL_DEF_ASCII_POLIZA                  : Result :=  'Ascii Poliza Contable';
          K_GLOBAL_DEF_IMP_MOVS                      : Result :=  'Proceso Importaci�n Movtos de Nomina: Archivo ';
          K_GLOBAL_DEF_PROM_CONCEPTO                 : Result :=  'Proceso Promediar Variables : Conceptos';
          K_GLOBAL_DEF_PROM_DIAS                     : Result :=  'Proceso Promediar Variables : Dias Considerados';
          K_GLOBAL_DEF_AGUINALDO_FALTAS              : Result :=  'Proceso Aguinaldo: F�rmula Para Faltas';
          K_GLOBAL_DEF_AGUINALDO_INCAPACIDADES       : Result :=  'Proceso Aguinaldo: F�rmula Para Incapacidades';
          K_GLOBAL_DEF_AGUINALDO_ANTICIPOS           : Result :=  'Proceso Aguinaldo: F�rmula Para Anticipos';
          K_GLOBAL_DEF_REPARTIR_AHORROS_TIPO         : Result :=  'Proceso Reparto de Ahorros: Tipo De Ahorro';
          K_GLOBAL_DEF_REPARTIR_AHORROS_MONTO        : Result :=  'Proceso Reparto de Ahorros: Monto a Repartir';
          K_GLOBAL_DEF_REPARTIR_AHORROS_PROPORCIONAL : Result :=  'Proceso Reparto de Ahorros: F�rmula Proporcional';
          K_GLOBAL_DEF_REPARTIR_AHORROS_CONCEPTO     : Result :=  'Proceso Reparto de Ahorros: Concepto Reparto Ahorro';
          K_GLOBAL_DEF_AGUINALDO_REPORTE             : Result :=  'Reporte de Aguinaldos';
          K_GLOBAL_DEF_REPARTIR_AHORROS_REPORTE      : Result :=  'Reporte de Reparto de Ahorros';

          K_GLOBAL_DEF_COMP_ANUAL_INGRESO_BRUTO      : Result :=  'Proceso Comparativo Anual: Ingreso Bruto';
          K_GLOBAL_DEF_COMP_ANUAL_INGRESO_EXENTO     : Result :=  'Proceso Comparativo Anual: Ingreso Exento';
          K_GLOBAL_DEF_COMP_ANUAL_IMPUESTO_RETENIDO  : Result :=  'Proceso Comparativo Anual: Impuesto Retenido';
          K_GLOBAL_DEF_COMP_ANUAL_CREDITO_PAGADO     : Result :=  'Proceso Comparativo Anual: Credito Pagado';
          K_GLOBAL_DEF_COMP_ANUAL_CALCULO_IMPUESTO   : Result :=  'Proceso Comparativo Anual: Calculo Impuesto';
          K_GLOBAL_DEF_COMP_ANUAL_EMPLEADOS_CALCULAR : Result :=  'Proceso Comparativo Anual: Empleados a Calcular';
          K_GLOBAL_DEF_COMP_ANUAL_REPORTE            : Result :=  'Reporte Comparativo Anual';

     {$ifdef ANTES}
          K_GLOBAL_DEF_CRED_SAL_REPORTE              : Result :=  'Reporte Declaraci�n Cr�dito Salario';
          K_GLOBAL_DEF_CRED_SAL_ING_BRUTO            : Result :=  'Proceso Declaraci�n Cr�dito Salario: Ingreso Bruto';
          K_GLOBAL_DEF_CRED_SAL_ING_EXENTO           : Result :=  'Proceso Declaraci�n Cr�dito Salario: Exento';
          K_GLOBAL_DEF_CRED_SAL_PAGADO               : Result :=  'Proceso Declaraci�n Cr�dito Salario: Pagado';
     {$else}
          K_GLOBAL_DEF_CRED_SAL_REPORTE              : Result :=  'Reporte Declaraci�n Subsidio Empleo';
          K_GLOBAL_DEF_CRED_SAL_ING_BRUTO            : Result :=  'Proceso Declaraci�n Subsidio Empleo: Ingreso Bruto';
          K_GLOBAL_DEF_CRED_SAL_ING_EXENTO           : Result :=  'Proceso Declaraci�n Subsidio Empleo: Exento';
          K_GLOBAL_DEF_CRED_SAL_PAGADO               : Result :=  'Proceso Declaraci�n Subsidio Empleo: Pagado';
     {$endif}

          K_GLOBAL_DEF_PTU_REPORTE                   : Result :=  'Reporte de PTU';
          K_GLOBAL_DEF_PTU_UTILIDAD_REPARTIR         : Result :=  'Proceso PTU: Utilidad Repartir';
          K_GLOBAL_DEF_PTU_SUMA_DIAS                 : Result :=  'Proceso PTU: Suma de D�as';
          K_GLOBAL_DEF_PTU_PERCEPCIONES              : Result :=  'Proceso PTU: Percepciones';
          K_GLOBAL_DEF_PTU_FILTRO_DIRECTOR           : Result :=  'Proceso PTU: Filtro Director';
          K_GLOBAL_DEF_PTU_FILTRO_PLANTA             : Result :=  'Proceso PTU: Filtro Planta';
          K_GLOBAL_DEF_PTU_FILTRO_EVENTUAL           : Result :=  'Proceso PTU: Filtro Eventual';
          K_GLOBAL_DEF_PTU_SALARIO_TOPE              : Result :=  'Proceso PTU: Salario Tope';

          K_GLOBAL_DEF_PAGO_RECIBOS                  : Result :=  'Proceso Importar Pago de Recibos: Archivo';

          K_GLOBAL_DEF_CONCEPTO_X_RASTREAR           : Result :=  'Proceso Rastreo: Concepto por Rastrear';

          K_GLOBAL_DEF_LAB_IMP_ORD_PROGRAMA          : Result :=  'Proceso Labor Importar Ordenes: Programa';
          K_GLOBAL_DEF_LAB_IMP_ORD_PARAMETROS        : Result :=  'Proceso Labor Importar Ordenes: Par�metros';
          K_GLOBAL_DEF_LAB_IMP_ORD_ARCHIVO           : Result :=  'Proceso Labor Importar Ordenes: Archivo';

          K_GLOBAL_DEF_LAB_IMP_PAR_PROGRAMA          : Result :=  'Proceso Labor Imp Productos: Programa';
          K_GLOBAL_DEF_LAB_IMP_PAR_PARAMETROS        : Result :=  'Proceso Labor Imp Productos: Par�metros';
          K_GLOBAL_DEF_LAB_IMP_PAR_ARCHIVO           : Result :=  'Proceso Labor Imp Productos: Archivo P/ Importar Partes';
          K_GLOBAL_DEF_EMP_IMP_ALTAS_EMP             : Result :=  'Proceso Labor Imp Productos: Archivo P/ Importar Empleados';

          K_GLOBAL_DEF_NOM_CREDITO_AP_CONCEPTO       : Result :=  'Proceso Cred. Aplicado Mensual: Concepto';
          K_GLOBAL_DEF_NOM_CREDITO_AP_INGRESO_BRUTO  : Result :=  'Proceso Cred. Aplicado Mensual: Ingreso Bruto';
          K_GLOBAL_DEF_NOM_CREDITO_AP_INGRESO_EXENTO : Result :=  'Proceso Cred. Aplicado Mensual: Ingreso Exento';
          K_GLOBAL_DEF_NOM_CREDITO_AP_DIAS           : Result :=  'Proceso Cred. Aplicado Mensual: D�as Trabajados';
          K_GLOBAL_DEF_NOM_CREDITO_AP_FACTOR_MENSUAL : Result :=  'Proceso Cred. Aplicado Mensual: Facto Mensual';
          K_GLOBAL_DEF_NOM_CREDITO_AP_TABLA          : Result :=  'Proceso Cred. Aplicado Mensual: Tabla';
          K_GLOBAL_DEF_NOM_CREDITO_AP_RASTREAR       : Result :=  'Proceso Cred. Aplicado Mensual: Rastrear Calculos';

          K_GLOBAL_DEF_EMP_RENUMERA                  : Result :=  'Proceso Renumera: Archivo';
          K_GLOBAL_RETROACT_CONCEPTOS                : Result :=  'Procesos Retroactivos: Lista de Conceptos';
          K_GLOBAL_RETROACT_CONCEPTO_PAGAR           : Result :=  'Procesos Retroactivos: Concepto a pagar';

          K_GLOBAL_DEF_COMENTA_SALDAR_VACA           : Result :=  'Proceso Saldar Vacaciones Globales: Observaciones';
          K_GLOBAL_DEF_MONTH_SALDAR_VACA              : Result :=  'Proceso Saldar Vacaciones Globales: Vencimiento';

          K_GLOBAL_PROXIMITY_TOTAL_BYTES             : Result :=  'Total De Caracteres En C�digo De Proximidad';
          K_GLOBAL_PROXIMITY_FACILITY_CODE           : Result :=  'Prefijo ( Facility Code ) En Hexadecimal';

          K_GLOBAL_CARRERA_FECHA_EVAL_INICIO         : Result := 'Fecha de Inicio del Periodo de Evaluaci�n';
          K_GLOBAL_CARRERA_FECHA_EVAL_FINAL          : Result := 'Fecha de Termino del Periodo de Evaluaci�n';

          K_GLOBAL_CB_ACTIVO_AL_DIA                  : Result := 'Mostrar Status del Empleado al D�a';
          K_GLOBAL_FORMULA_STATUS_ACTIVO             : Result := 'F�rmula para Empleados Activos';

          K_GLOBAL_DEF_EMP_IMP_AHO_PRE               : Result := 'Proceso Importar Ahorros y Pr�stamos: Archivo';

          K_GLOBAL_PLAN_VACA_PRENOMINA               : Result := 'Aplicar plan de vacaciones';
          K_GLOBAL_PLAN_VACA_DIAS_PAGO               : Result := 'Plan de vacaciones (D�as de pago)';
          K_GLOBAL_PAGO_PERIODO_ANTC                 : Result := 'Per�odo de pago anticipado en plan de vacaciones';
          K_GLOBAL_DEF_NOM_TIPOPRESTAAJUSTE          : Result := 'Proceso: Ajuste de Retenci�n Fonacot';
          K_GLOBAL_DEF_NOM_TIPOPRESTAJUSTE_CALCULO   : Result := 'Proceso: C�lculo de Pago Fonacot';
          K_GLOBAL_DEF_IMSS_PAGO_SUGERIDODANOS       : Result := 'Valor Sugerido de Da�os a Vivienda';

          { Global exclusivo para AVENT-HONDURAS }
          K_GLOBAL_AVENT                             : Result := 'Empleados AVENT-HONDURAS';

          //Global para indicar limite de nominas ordinarias
          K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS         : Result := 'N�mero l�mite de n�minas ordinarias';

          K_GLOBAL_INCLUYE_FI_SUA                    : Result := 'SUA: Incluir FI';

          //Enrolamiento de Usuarios
          K_GLOBAL_ENROLL_AUTO_USER                  : Result := 'Crear usuarios autom�ticamente al dar de alta o reingresar un empleado';
          K_GLOBAL_ENROLL_TIPO_LOGIN                 : Result := 'Tipo de login de usuarios';
          K_GLOBAL_ENROLL_DOMINIO_AD                 : Result := 'Dominio default de active directory';
          K_GLOBAL_ENROLL_FORMULA_USER               : Result := 'F�rmula de usuario del empleado';
          K_GLOBAL_ENROLL_FORMULA_PWD                : Result := 'F�rmula de contrase�a del empleado';
          K_GLOBAL_ENROLL_FORMULA_MAIL               : Result := 'F�rmula del correo del empleado';
          K_GLOBAL_ENROLL_FORMULA_AD                 : Result := 'F�rmula de active directory del empleado';
          K_GLOBAL_ENROLL_GRUPO_DEFAULT              : Result := 'Grupo default del empleado';
          K_GLOBAL_ENROLL_ROLES_DEFAULT              : Result := 'Roles default del empleado';
          K_GLOBAL_ENROLL_ROLES_DEFAULT2             : Result := 'Roles default del empleado';
          K_GLOBAL_ENROLL_CAMBIAR_PWD                : Result := 'Cambiar contrase�a despu�s de ingresar por primera vez';

          K_GLOBAL_PROVISION_INFONAVIT               : Result := 'Concepto de Prov. Infonavit';
          K_GLOBAL_AJUSINFO_TCOMPARA                 : Result := 'T. Comparaci�n Ajus. Infonavit';
          K_GLOBAL_AJUSINFO_TPRESTA                  : Result := 'T. Pr�stamo Ajuste Infonavit';
          K_GLOBAL_BLOQUEO_PRENOM_AUT                : Result := 'Bloqueo de Pre-Nominas Autorizadas';
          K_GLOBAL_DEF_MUNICIPIO                     : Result := 'Default del Municipio';
          K_GLOBAL_LABOR_MAQUINA                     : Result := 'Etiqueta Singular de M�quinas';
          K_GLOBAL_LABOR_MAQUINAS                    : Result := 'Etiqueta Plural de M�quinas';
          K_GLOBAL_LABOR_PLANTILLA                   : Result := 'Etiqueta Singular de Plantillas de Producci�n';
          K_GLOBAL_LABOR_PLANTILLAS                  : Result := 'Etiqueta Plural de Plantillas de Producci�n';
          K_GLOBAL_ENROLAMIENTO_SUPERVISORES         : Result := 'Usar Enrolamiento de Supervisores';
          K_GLOBAL_NIVEL_COSTEO                      : Result := 'Nivel de Organigrama para Costeo';
          K_GLOBAL_CALC_SAL_INTG                     : Result := 'Actualizar Factor Integraci�n Salarios Topados';
          K_GLOBAL_MES_VENC_VAC                      : Result := 'Vencimiento de Vacaciones en Meses';
          K_GLOBAL_DIAS_COTIZADOS_FV                 : Result := 'Calcular d�as cotizados sin faltas x Vac.';
          K_GLOBAL_INCAPACIDAD_GENERAL               : Result := 'Incidencia de Incapacidad General';
          K_GLOBAL_FESTIVO_EN_DESCANSOS              : Result := 'Incidencia Festivo en Descansos';
          K_GLOBAL_TIMBRADO_ENVIAR_DETALLE_5         : Result := 'Timbrado de  N�mina - Envio de Detalle de Montos';
          K_GLOBAL_TIMBRADO_ENVIAR_INCAPACIDADES_4   : Result := 'Timbrado de  N�mina - Envio de Incapacidades';
          K_GLOBAL_TIMBRADO_ENVIAR_TIEMPO_EXTRA_3    : Result := 'Timbrado de  N�mina - Envio de Tiempo Extra';
          K_GLOBAL_TIMBRADO_USAR_PERIODICIDAD_2      : Result := 'Timbrado de  N�mina - Periodicidad de Pago';
          K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1       : Result := 'Timbrado de  N�mina - Considerar Transferidos';
          K_GLOBAL_TIMBRADO_DETALLE_SUBCONTRATO_6    : Result := 'Timbrado de  N�mina - Enviar detalle de Subcontrato a timbrado';
          {*** Notificaciones de Timbrado y Advertencias ***}
          K_GLOBAL_NOTIFICACION_TIMBRADO_SOLO_ORDINARIAS : Result := 'Considerar s�lo n�minas ordinarias';
          K_GLOBAL_NOTIFICACION_TIMBRADO_ACTIVO          : Result := 'Notificaciones de n�minas pendientes a timbrar';
          K_GLOBAL_NOTIFICACION_TIMBRADO_DIAS            : Result := 'D�as de notificaci�n despu�s de la fecha de pago';
//          K_GLOBAL_SALARIO_TEMP                      : Result := 'Tarjetas con Salario Temporal';
          {$IFDEF CALSONIC}
          K_GLOBAL_ESPECIAL_LOG_GLOBAL_1               : Result := 'Incluir sentencia SQL en bit�cora';
          {$ENDIF}
          else
              Result := 'Descripci�n NO Disponible';
     end;
     end;
end;

end.
