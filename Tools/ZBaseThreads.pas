unit ZBaseThreads;

interface

{$ifndef DOS_CAPAS}
{$define HTTP_CONNECTION}
{$endif}

uses Classes, SysUtils, Forms, Windows, ComObj,Variants,
     {$ifdef TRESS_DELPHIXE5_UP}System.Types,{$endif}
     {$ifdef HTTP_CONNECTION}SConnect,{$endif}
     {$ifdef HTTP_CONNECTION}ZetaRegistryCliente,{$endif}
     ZetaClientTools,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses;

type
  TSetResultadoEspecial = procedure(Resultado :OleVariant) of object;
  TProcessList = class;
  TProcessInfo = class( TObject )
  private
    { Private declarations }
    FHandle: HWND;
    FLista: TProcessList;
    FCanceladoPor: Integer;
    FInicio: TDateTime;
    FFin: TDateTime;
    FMaximo: Integer;
    FProcesados: Integer;
    FEmpleado: TNumEmp;
    FFolio: Integer;
    FIndex: Integer;
    FProceso: Procesos;
    FStatus: eProcessStatus;
    FError: String;
    FCargo: OleVariant;
    FWarningCount: Word;
    FErrorCount: Word;
    FEventCount: Word;
    FFeedBack:Boolean;
    FSetResultadoEsp : TSetResultadoEspecial;
    procedure SetCargo( const Valor: OleVariant );
  public
    { Public declarations }
    constructor Create( Lista: TProcessList );
    destructor Destroy; override;
    property CanceladoPor: Integer read FCanceladoPor;
    property Cargo: OleVariant read FCargo write SetCargo;
    property Empleado: Integer read FEmpleado;
    property Error: String read FError;
    property Folio: Integer read FFolio;
    property Fin: TDateTime read FFin;
    property Inicio: TDateTime read FInicio;
    property Maximo: Integer read FMaximo;
    property Proceso: Procesos read FProceso write FProceso;
    property Procesados: Integer read FProcesados;
    property Status: eProcessStatus read FStatus;
    property TotalErrores: Word read FErrorCount;
    property TotalEventos: Word read FEventCount;
    property TotalAdvertencias: Word read FWarningCount;
    property FeedBack:Boolean read FFeedBack write FFeedBack;
    property Handle: HWND read FHandle write FHandle;
    property SetResultadoEsp : TSetResultadoEspecial read FSetResultadoEsp write FSetResultadoEsp;
    procedure Assign( Value: TProcessInfo );
    procedure Excepcion( Error: Exception );
    procedure Inicializar;
    procedure Finalizar( Resultado: OleVariant );
    procedure FinalizarSilent( Resultado: OleVariant );
    procedure SetResultado( Valor: OleVariant );
  end;
  TProcessList = class( TThreadList )
  private
    { Private declarations }
  public
    { Public declarations }
    destructor Destroy; override;
    function Count: Integer;
  end;
{$ifdef DOS_CAPAS}
  TProcessThread = class( TObject )
  private
    { Private declarations }
    FFreeOnTerminate: Boolean;
    FSilentExecute: Boolean;
    FPriority: TThreadPriority;
  protected
    { Protected declarations }
    procedure Execute; virtual; abstract;

  public
    { Public declarations }
    constructor Create(CreateSuspended: Boolean);
    property Priority: TThreadPriority read FPriority write FPriority;
    property FreeOnTerminate: Boolean read FFreeOnTerminate write FFreeOnTerminate;
    property SilentExecute: Boolean read FSilentExecute write FSilentExecute default FALSE;
    procedure Resume;
  end;
  TBaseThread = class(TProcessThread)
{$else}
  TBaseThread = class(TThread)
{$endif}
  private
    { Private declarations }
    FLista: TProcessList;
    FInfo: TProcessInfo;
    FComputerName: String;
    {$ifdef HTTP_CONNECTION}
    FURL: String;
    FTipoConexion: TTipoConexion;
    FUserName: String;
    FPassword: String;
    FWebConnection: TWebConnection;
    {$endif}
    FEmpresa: OleVariant;
    FEmployees: OleVariant;
    FUsuario: Integer;
    FParametros: TZetaParams;
    FSilentExecute : Boolean;
  protected
    { Protected declarations }
    property Lista: TProcessList read FLista;
    property ComputerName: String read FComputerName;
    {$ifdef HTTP_CONNECTION}
    property URL: String read FURL write FURL;
    property TipoConexion: TTipoConexion read FTipoConexion write FTipoConexion;
    property UserName: String read FUserName write FUserName;
    property Password: String read FPassword write FPassword;
    {$endif}
    property Empresa: OleVariant read FEmpresa;
    property Parametros: TZetaParams read FParametros;
    property Usuario: Integer read FUsuario;
    function CreateServer( const ClassID: TGUID ): IDispatch;
    function CreateAppServer( const ClassID: TGUID ) : IDispatch;
    function GetProceso: Procesos;
    function Procesar: OleVariant; virtual; abstract;
    procedure Execute; override;
    procedure Init; virtual;
    procedure SetCargo( const Valor: OleVariant );
    procedure SetProceso( const eProceso: Procesos );
    procedure SetEmployees( const Valor: OleVariant );
  public
    { Public declarations }
    constructor Create( Lista: TProcessList );
    destructor Destroy; override;
    property ProcessInfo: TProcessInfo read FInfo;
    property Proceso: Procesos read GetProceso write SetProceso;
    property Employees: OleVariant read FEmployees write SetEmployees;
    property SilentExecute: Boolean read FSilentExecute write FSilentExecute default FALSE;
    procedure CargaParametros( Fuente: TZetaParams );
  end;

implementation

uses ZetaMessages,
    {$ifndef HTTP_CONNECTION}
     ZetaRegistryCliente,
     {$endif}
     DCliente;

{ ******** TProcessInfo ******* }

constructor TProcessInfo.Create( Lista: TProcessList );
begin
     FLista := Lista;
     if Assigned( FLista ) then
     begin
          with FLista do
          begin
               Add( Self );
               FIndex := ( Count - 1 );
          end;
     end;
     FInicio := Now;
     FFin := Now;
     FMaximo := 0;
     FProcesados := 0;
     FEmpleado := 0;
     FFolio := 0;
     FProceso := prNinguno;
     FCanceladoPor := 0;
     with Application do
     begin
          if Assigned( MainForm ) then
             FHandle := MainForm.Handle
          else
              FHandle := 0;
     end;
     FWarningCount := 0;
     FErrorCount := 0;
     FEventCount := 0;
     VarClear( FCargo );
     FFeedBack := True;
end;

destructor TProcessInfo.Destroy;
begin
     if Assigned( FLista ) then
     begin
          with FLista do
          begin
               Remove( Self );
          end;
          FLista := nil;
     end;
     inherited Destroy;
end;

procedure TProcessInfo.Assign( Value: TProcessInfo );
begin
     if Assigned( Value ) then
     begin
          FInicio := Value.Inicio;
          FFin := Value.Fin;
          FMaximo := Value.Maximo;
          FProcesados := Value.Procesados;
          FEmpleado := Value.Empleado;
          FFolio := Value.Folio;
          FProceso := Value.Proceso;
          FStatus := Value.Status;
          FError := Value.Error;
          FCanceladoPor := Value.CanceladoPor;
          FWarningCount := Value.TotalAdvertencias;
          FErrorCount := Value.TotalErrores;
          FEventCount := Value.TotalEventos;
          FFeedBack := Value.FeedBack;
          SetCargo( Value.Cargo );
     end;
end;

procedure TProcessInfo.SetCargo( const Valor: OleVariant );
begin
     VarCopy( FCargo, Valor );
end;

procedure TProcessInfo.SetResultado( Valor: OleVariant );
begin
     FFolio := Valor[ K_PROCESO_FOLIO ];
     FStatus := eProcessStatus( Valor[ K_PROCESO_STATUS ] );
     FProceso := Procesos( Valor[ K_PROCESO_ID ] );
     FMaximo := Valor[ K_PROCESO_MAXIMO ];
     FProcesados := Valor[ K_PROCESO_PROCESADOS ];
     FEmpleado := Valor[ K_PROCESO_ULTIMO_EMPLEADO ];
     FInicio := Valor[ K_PROCESO_INICIO ];
     FFin := Valor[ K_PROCESO_FIN ];
     FError := '';
     FErrorCount := Valor[ K_PROCESO_ERRORES ];
     FEventCount := Valor[ K_PROCESO_EVENTOS ];
     FWarningCount := Valor[ K_PROCESO_ADVERTENCIAS ];
end;

procedure TProcessInfo.Inicializar;
begin
     FInicio := Now;
     FStatus := epsEjecutando;
end;

procedure TProcessInfo.Finalizar( Resultado: OleVariant );
begin
     FFin := Now;
     SetResultado( Resultado );
     if ( FHandle > 0 ) then
        Windows.PostMessage( FHandle, WM_WIZARD_END, FIndex, 0 );
end;

procedure TProcessInfo.FinalizarSilent( Resultado: OleVariant );
begin
     FFin := Now;
     If FFeedBack then
        SetResultado( Resultado )
     else if (Assigned(FSetResultadoEsp)) then
             FSetResultadoEsp (Resultado);

     if ( FHandle > 0 ) and FFeedBack then
        Windows.PostMessage( FHandle, WM_WIZARD_SILENT, FIndex, 0 );
end;


procedure TProcessInfo.Excepcion(Error: Exception);
begin
     FFin := Now;
     FStatus := epsCatastrofico;
     FError := Error.Message;
     if ( FHandle > 0 ) then
        Windows.PostMessage( FHandle, WM_WIZARD_END, FIndex, 0 );
end;

{ ******** TProcessList ******* }

destructor TProcessList.Destroy;
var
   i: Integer;
begin
     try
        with LockList do
        begin
             for i := ( Count - 1 ) downto 0 do
             begin
                  TProcessInfo( Items[ i ] ).Free;
             end;
        end;
     finally
            UnlockList;
     end;
     inherited Destroy;
end;

function TProcessList.Count: Integer;
begin
     try
        Result := LockList.Count;
     finally
            UnlockList;
     end;
end;

{$ifdef DOS_CAPAS}
{ ********** TProcessThread *********** }

constructor TProcessThread.Create(CreateSuspended: Boolean);
begin
end;

procedure TProcessThread.Resume;
begin
     Execute;
end;



{$endif}

{ ******** TBaseThread ******* }

constructor TBaseThread.Create( Lista: TProcessList );
begin
     FLista := Lista;
     FInfo := TProcessInfo.Create( FLista );
     Init;
     FParametros := TZetaParams.Create;
     {$ifdef VER130}
     Priority := tpLowest;
     {$endif}
     FreeOnTerminate := True;
     {$ifdef HTTP_CONNECTION}
     {FWebConnection := TWebConnection.Create( nil );
     with FWebConnection do
     begin
          LoginPrompt := False;
          Agent := 'GTI';
     end;}
     {$endif}
     inherited Create( True );
end;

destructor TBaseThread.Destroy;
begin
     {$ifdef HTTP_CONNECTION}
     FreeAndNil( FWebConnection );
     {$endif}
     FreeAndNil( FParametros );
     inherited Destroy;
end;

procedure TBaseThread.Init;
begin
     FComputerName := ClientRegistry.ComputerName;
     {$ifdef HTTP_CONNECTION}
     FURL := ClientRegistry.URL; //'http://www.tress.com.mx/TressRemoto/httpsrvr.dll';
     FUsername := ClientRegistry.UserName;
     FPassword := ClientRegistry.Password;
     FTipoConexion := ClientRegistry.TipoConexion;
     {$endif}
     with dmCliente do
     begin
          FEmpresa := Empresa;
          FUsuario := Usuario;
     end;
end;

procedure TBaseThread.Execute;
var
   Resultado: OleVariant;
begin
     with FLista do
     begin
          try
             LockList;
             FInfo.Inicializar;
          finally
                 UnlockList;
          end;
          try
             Resultado := Procesar;
             try
                LockList;
                if SilentExecute then
                begin
                     if NOT VarIsNull( Resultado ) then
                     begin
                          //if eProcessStatus( Resultado[ K_PROCESO_STATUS ] ) in[epsEjecutando, epsOK] then
                             FInfo.FinalizarSilent( Resultado )
                          {else
                              FInfo.Finalizar( Resultado );}
                     end;
                end
                else
                    if NOT VarIsNull( Resultado ) then
                       FInfo.Finalizar( Resultado );
             finally
                    UnlockList;
             end;
          except
                on Error: Exception do
                begin
                     try
                        LockList;
                        FInfo.Excepcion( Error );
                     finally
                            UnlockList;
                     end;
                end;
          end;
     end;
end;

procedure TBaseThread.CargaParametros( Fuente: TZetaParams );
begin
     if ( Fuente <> NIL ) then
       with FParametros do
       begin
            Assign( Fuente );
            AssignValues( Fuente );
       end;
end;

function TBaseThread.CreateServer( const ClassID: TGUID ): IDispatch;
begin
     ZetaClientTools.InitDCOM( False );
     {$ifdef HTTP_CONNECTION}
     case Self.TipoConexion of
          conxHTTP:
          begin
               if (not Assigned (FWebConnection)) then
               begin
                    FWebConnection := TWebConnection.Create( nil );
                    FWebConnection.LoginPrompt := False;
                    FWebConnection.Agent :=  'GTI';
               end;
               with FWebConnection do
               begin
                    Connected := False;
                    URL := Self.URL; //'http://www.tress.com.mx/TressRemoto/httpsrvr.dll';
                    Username := Self.UserName;
                    Password := Self.Password;
                    ServerGUID := GUIDToString( ClassID );
                    Connected := True;
                    Result := AppServer;
               end;
          end
          else
          begin
               Result := CreateRemoteComObject( ComputerName, ClassID ) as IDispatch;
          end;
     end;
     {$else}
     Result := ComObj.CreateRemoteComObject( ComputerName, ClassID ) as IDispatch;
     {$endif}
end;

function TBaseThread.CreateAppServer(const ClassID: TGUID): IDispatch;
begin
     ZetaClientTools.InitDCOM( False );
     {$ifdef HTTP_CONNECTION}
     case ClientRegistry.TipoConexion of
          conxHTTP:
          begin
               with FWebConnection do
               begin
                    Connected := False;
                    URL := ClientRegistry.URL; //'http://www.tress.com.mx/TressRemoto/httpsrvr.dll';
                    Username := ClientRegistry.UserName;
                    Password := ClientRegistry.Password;
                    ServerGUID := GUIDToString( ClassID );
                    Connected := True;
                    Result := AppServer;
               end;
          end
          else
          begin
               if ZetaCommonTools.StrVacio( ComputerName ) then
                  Result := CreateComObject( ClassID ) as IDispatch
               else
                   Result := CreateRemoteComObject( ComputerName, ClassID ) as IDispatch;
          end;
     end;
     {$else}
     if ZetaCommonTools.StrVacio( ComputerName ) then
        Result := CreateComObject( ClassID ) as IDispatch
     else
         Result := CreateRemoteComObject( ComputerName, ClassID ) as IDispatch;
     {$endif}
end;

procedure TBaseThread.SetEmployees( const Valor: OleVariant );
begin
     VarCopy( FEmployees, Valor );
end;

procedure TBaseThread.SetCargo( const Valor: OleVariant );
begin
     with FLista do
     begin
          try
             LockList;
             FInfo.Cargo := Valor;
          finally
                 UnlockList;
          end;
     end;
end;

function TBaseThread.GetProceso: Procesos;
begin
     with FLista do
     begin
          try
             LockList;
             Result := FInfo.Proceso;
          finally
                 UnlockList;
          end;
     end;
end;

procedure TBaseThread.SetProceso(const eProceso: Procesos);
begin
     with FLista do
     begin
          try
             LockList;
             FInfo.Proceso := eProceso;
          finally
                 UnlockList;
          end;
     end;
end;

end.
