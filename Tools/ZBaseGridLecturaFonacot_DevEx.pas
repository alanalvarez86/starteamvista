unit ZBaseGridLecturaFonacot_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid, StdCtrls, Menus, ActnList, ImgList, TressMorado2013, ComCtrls,
  ZetaStateComboBox, ZBaseGridLectura_DevEx,ftressshell,ZetaCommonTools,ZetaCommonLists,
  System.Actions;

type
  TBaseGridLecturaFonacot_DevEx = class(TBaseGridLectura_DevEx)
    IMSSPanel: TPanel;
    IMSSMesLBL: TLabel;
    IMSSYearLBL: TLabel;
    IMSSPatronLBL: TLabel;
    IMSSTipoLBL: TLabel;
    IMSSMesCB: TStateComboBox;
    IMSSAnioCB: TStateComboBox;
    IMSSPatronCB: TStateComboBox;
    IMSSTipoCB: TStateComboBox;
    IMSSYearUpDown: TUpDown;
    procedure IMSSTipoCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure IMSSYearUpDownClick(Sender: TObject; Button: TUDBtnType);
    procedure IMSSAnioCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure IMSSMesCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure IMSSPatronCBLookUp(Sender: TObject; var lOk: Boolean);

  private
    { Private declarations }
  protected
    { Protected declarations }
    //DevEx (by ame): Proceso Agregado para asignar el MinWidth Ideal
    procedure LlenaRazonSocialAcumulados;


  public
   { Public declarations }
  end;

var
  BaseGridLecturaFonacot_DevEx: TBaseGridLecturaFonacot_DevEx;

implementation

uses DCliente, DRecursos, DCatalogos, ZetaCommonClasses;

{$R *.dfm}

{ TCtasBancarias }


procedure TBaseGridLecturaFonacot_DevEx.IMSSTipoCBLookUp(Sender: TObject; var lOk: Boolean);
begin
     inherited;
     dmCliente.IMSSTipo := eTipoLiqIMSS( IMSSTipoCB.Indice );
     TressShell.RefrescaIMSSActivoPublico;
end;

procedure TBaseGridLecturaFonacot_DevEx.IMSSYearUpDownClick(Sender: TObject; Button: TUDBtnType);
begin
     inherited;
     with IMSSAnioCB do
     begin
          case Button of
               btNext: AsignaValorEntero( ValorEntero + 1 );
               btPrev: AsignaValorEntero( ValorEntero - 1 );
          end;
     end;
end;

procedure TBaseGridLecturaFonacot_DevEx.IMSSAnioCBLookUp(Sender: TObject; var lOk: Boolean);
begin
     inherited;
     dmCliente.IMSSYear := IMSSAnioCB.ValorEntero;
     TressSHell.RefrescaIMSSActivoPublico;
end;

procedure TBaseGridLecturaFonacot_DevEx.IMSSMesCBLookUp(Sender: TObject; var lOk: Boolean);
begin
     inherited;
     dmCliente.IMSSMes := IMSSMesCB.Indice;
     TressSHell.RefrescaIMSSActivoPublico;
end;

procedure TBaseGridLecturaFonacot_DevEx.IMSSPatronCBLookUp(Sender: TObject; var lOk: Boolean);
begin
     inherited;
     dmCliente.RazonSocial := IMSSPatronCB.Llave;
     TressSHell.RefrescaIMSSActivoPublico;
end;

procedure TBaseGridLecturaFonacot_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     IMSSPatronCB.Llave := dmCliente.RazonSocial;
     IMSSMesCB.Indice := dmCliente.IMSSMes;
     IMSSAnioCB.ValorEntero := dmCliente.IMSSYear;
     IMSSTipoCB.Indice := Ord( dmCliente.IMSSTipo );
end;

procedure TBaseGridLecturaFonacot_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     LlenaRazonSocialAcumulados;
     IMSSPatronCB.Llave := dmCliente.RazonSocial;
     TressSHell.RefrescaIMSSActivoPublico;
     with self.IMSSPatronCB do
     begin
          DropDownCount := iMin( Items.Count, 12 );   // M�ximo muestra 12 sin que aparezca el Scroll
     end;
     IMSSMesCB.Indice := dmCliente.IMSSMes;
     IMSSAnioCB.ValorEntero := dmCliente.IMSSYear;
     IMSSTipoCB.Indice := Ord( dmCliente.IMSSTipo );
end;


procedure TBaseGridLecturaFonacot_DevEx.LlenaRazonSocialAcumulados;
const
     K_RAZON_TODOS_TEXTO  = 'Todas';
begin
     with IMSSPatronCB.Lista do
     begin
          BeginUpdate;
          IMSSPatronCB.Sorted := False;
          try
             Clear;
             with dmCatalogos.cdsRSocial do
             begin
                  Conectar;
                  First;
                  Add( VACIO + ' = ' + K_RAZON_TODOS_TEXTO );

                  while ( not EOF ) do
                  begin
                       Add( FieldByName('RS_CODIGO').AsString + ' = ' + FieldByName('RS_NOMBRE').AsString );
                       Next;
                  end;
                  First;
             end;
          finally
                 EndUpdate;
          end;

          if dmCliente.RazonSocial <> VACIO then
          begin
                IMSSPatronCB.Llave := dmCliente.RazonSocial;
          end;

          if IMSSPatronCB.ItemIndex = -1 then
          begin
               IMSSPatronCB.ItemIndex := 0;
               dmCliente.RazonSocial := IMSSPatronCB.Llave;
          end;
     end;
end;

end.
