inherited ServerRegistryEditor: TServerRegistryEditor
  ActiveControl = Database
  Caption = 'Especificar Base De Datos'
  ClientHeight = 129
  ClientWidth = 339
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object UserNameLBL: TLabel [0]
    Left = 49
    Top = 31
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = '&Usuario:'
    FocusControl = UserName
  end
  object PasswordLBL: TLabel [1]
    Left = 4
    Top = 51
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = '&Clave de Acceso:'
    FocusControl = Password
  end
  object PasswordCheckLBL: TLabel [2]
    Left = 24
    Top = 74
    Width = 64
    Height = 13
    Alignment = taRightJustify
    Caption = 'C&onfirmación:'
    FocusControl = Password
  end
  object DatabaseLBL: TLabel [3]
    Left = 15
    Top = 8
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = '&Base de Datos:'
    FocusControl = Database
  end
  inherited PanelBotones: TPanel
    Top = 96
    Width = 339
    Height = 33
    BevelOuter = bvNone
    TabOrder = 4
    inherited OK: TBitBtn
      Left = 171
      Top = 5
      Anchors = [akRight, akBottom]
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 256
      Top = 5
      Anchors = [akRight, akBottom]
      OnClick = CancelarClick
    end
  end
  object UserName: TEdit
    Left = 90
    Top = 26
    Width = 123
    Height = 21
    TabOrder = 1
  end
  object Password: TEdit
    Left = 90
    Top = 48
    Width = 123
    Height = 21
    PasswordChar = '*'
    TabOrder = 2
    OnChange = PasswordChange
  end
  object PasswordCheck: TEdit
    Left = 90
    Top = 70
    Width = 123
    Height = 21
    PasswordChar = '*'
    TabOrder = 3
  end
  object Database: TEdit
    Left = 90
    Top = 4
    Width = 244
    Height = 21
    TabOrder = 0
  end
end
