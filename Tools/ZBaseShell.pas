unit ZBaseShell;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      ZBaseShell.pas                             ::
  :: Descripci�n: Clase base del programa principal de Tress ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

{$ifndef DOS_CAPAS}
{$define HTTP_CONNECTION}
{$endif}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, dbGrids, Db, ExtCtrls,
     ZetaTipoEntidad,
     ZetaClientDataset,
     ZetaCommonLists,
     FHelpManager,
     cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, dxStatusBar, dxRibbonStatusBar,
     cxClasses, dxSkinsForm, cxStyles, dxRibbonSkins{$ifdef TRESS_DELPHIXE5_UP}, dxSkinsdxRibbonPainter{$endif},
  dxRibbon{$ifdef TRESS_DELPHIXE5_UP}, dxRibbonCustomizationForm{$endif};

type
  TBaseShell = class( TForm )
    StatusBar_DevEx: TdxRibbonStatusBar;
    LogoffTimer: TTimer;
    DevEx_SkinController: TdxSkinController;
    cxStyleRepository1: TcxStyleRepository;
    DevEx_ShellRibbon: TdxRibbon;
    StatusBar: TStatusBar;

    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure MuestraPista(Sender: TObject);
    procedure ManejaExcepcion( Sender: TObject; Error: Exception );
    procedure ShellOnIdle(Sender: TObject; var Done: Boolean);
    procedure ShellOnMessage(var Msg: TMsg; var Handled: Boolean);
    procedure LogoffTimerTimer(Sender: TObject);
{$ifndef DOS_CAPAS}
    function GetCredenciales(var Usuario, Contrasena: String): Boolean;
{$ENDIF}
  private
    { Private declarations }
    FTimerOn: Boolean;
{$ifndef DOS_CAPAS}
    FListaParametros: TStrings;
{$ENDIF}
    //DevEx(@am): Variable que guarda si la version de la BD es la actual, ants de ejecutar una accion sobre ella (por ej. una actualizacion)
    FVersionBD: Boolean;
    //US# 11757: Proteccion funcionalidad HTTP por licencia sentinel
    FTerminaAplicacion: Boolean; //Indica cuando la aplicacion ya Finalizo
    FModoSistema: Boolean; //Indica si la aplicacion esta en modo sistema
    FEmpresaAbiertaAux: Boolean;
    //FIN
    function CheckVersion(sServer: String): Boolean;
    function GetEmpresaAbierta: Boolean;
    procedure AbreTodo;
    procedure AbreSistema;
    procedure SetEmpresaAbierta( const Value: Boolean );
    procedure SuspendeSistema;
    procedure MueveFormas;
    procedure RestableceFormas;
    //DevEx(by am): Metodos para nuevo LogIn
    function LoginTress_DevEx(const lAllowReentry:Boolean;var eReply: eLogReply): Boolean;
    //
    function MostrarMensajeReply( eReply:eLogReply): Boolean;overload;
    function MostrarMensajeReply( eReply:eLogReply; var lOtroIntento:Boolean): Boolean; overload;
    function EsWindows7: Boolean;
    procedure ShowLoginDenied(const sTitulo, sMsg: String);
{$ifndef DOS_CAPAS}
    procedure GetListaParametros; //CONSOLA
{$ENDIF}

  protected
    { Protected declarations }
    FOldHelpEvent: THelpEvent;
    {$IFDEF TRESS_DELPHIXE5_UP}
    function HTMLHelpHook( Command: Word; Data: THelpEventData; var CallHelp: Boolean ): Boolean;
    {$ELSE}
    function HTMLHelpHook( Command: Word; Data: Longint; var CallHelp: Boolean ): Boolean;
    {$ENDIF}
    property EmpresaAbierta: Boolean read GetEmpresaAbierta write SetEmpresaAbierta;
    function CambiaClaveUsuario: Boolean;
    {***DevEx (by am): Se sobrecargo el metodo para que siga funcionando con los viejos ejecutables mientras se hace
    el cambio de LogIn para todos.
    Se agrego el parametro ShowDevExForm para saber cuando la llamada proviene del LogIn o vista Actual,
    pues cuando sea asi debe llamarse a la forma nueva.***}
    function DesConectaUsuario: Boolean;
    {***}
    function ProcesaDialogo(oLookUp: TZetaLookupDataset; const eEntidad: TipoEntidad; const sFilter: String;
             var sKey, sDescription: String; var lEncontrado: Boolean ): Boolean; virtual;
    procedure AbreEmpresa(const lDefault: Boolean); dynamic;
    procedure CierraTodo;
    {***US# 11757: Proteccion funcionalidad HTTP por licencia sentinel: Se modifico para poder realizar el Cambio de Servidor
    Sin mostrar la Forma: FindServidor_DevEx que realiza la busqueda del Servidor COM Remoto
    ***}
    procedure CambiaServidor( lCambiaServidorHTTP: Boolean = False );
    procedure DoOpenAll; virtual;
    procedure DoOpenSistema; virtual;
    procedure DoCloseAll; virtual;
    procedure HabilitaControles; virtual;
    procedure InitCaption(const sValue,sTipoApp: String);virtual;
    procedure Logout;
    procedure LogOff; virtual;
    procedure CloseShell; virtual;
    procedure SetTimerInfo;
    procedure StatusBarMsg( const sMsg: String; const iPanel: Integer );
    procedure CargaDerechos; dynamic;
    function CheckVersionDatos:Boolean;virtual;
    function EscogeEmpresa(const lDefault: Boolean): Boolean;
    procedure MuestraDatosUsuarioActivo; dynamic;
    {***}
  public
    { Public declarations }
    function BuscaDialogo( const eEntidad: TipoEntidad; const sFilter: String; var sKey, sDescription: String ): Boolean;virtual;
    function GetLookUpDataSetGeneral(  const eEntidad: TipoEntidad ): TZetaLookUpDataSet;
    function GetLookUpDataSet( const eEntidad: TipoEntidad ): TZetaLookUpDataSet;virtual;
    //DevEx: Metodo agregado para el nuevo LogIn
    function Login( const lAllowReentry: Boolean ): Boolean;
    //
    procedure BeforeRun;
    procedure PrintScreen; virtual;
    procedure ReconectaMenu;virtual;
    {***DevEx(@am): Metodos virtuales para poder compilar formas que invocan procedimientos desde el TressShell ***}
    procedure CreaNavBar; virtual;
    procedure LimpiaGruposNavBar; virtual;
    procedure LLenaOpcionesGrupos; virtual;
    //function GetSkinController: TdxSkinController; virtual; //BORRAR
    function GetNavBarImageList: TcxImageList; virtual;
    procedure AsistenciaFechavalid(Valor:TDate); virtual;
    procedure RefrescaIMSSActivoPublico; virtual;
    //US# 11757: Proteccion funcionalidad HTTP por licencia sentinel
    function ValidaConexionHTTP: Boolean;
    {***}
{$ifndef DOS_CAPAS}
    property ListaParametros: TStrings read FListaParametros; //CONSOLA
{$ENDIF}
    property EsVersionActualBD: Boolean read FVersionBD write FVersionBD;
  end;

var
  BaseShell: TBaseShell;

implementation

uses ZAccesosMgr,
     ZetaUserLogin_DevEx,
     ZetaEscogeCia_DevEx,
     ZetaCambiaClave_DevEx,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaDialogo,
     ZetaRegistryCliente,
     ZetaSystemWorking,
     ZetaBusqueda_DevEx,
     {$ifdef DOS_CAPAS}
     ZetaRegistryServerEditor,
     {$else}
     FBuscaServidor_DevEx,
     {$endif}
     FAutoClasses,
     FEntradaDemoKit,
     ZetaUserSuspend_DevEx,
     DCliente, DDiccionario,ZGlobalTress,
     DGlobal,ZetaServerTools,
     FBienvenida_DevEx;

const
     K_PANEL_USER = 0;
     K_PANEL_HINT = 4;
     K_MESSAGE_PRO_MSSQL = 'La licencia Profesional de Sistema TRESS requiere MSSQL Server Express.'+CR_LF+
                           'Si usted cuenta con servidor de MSSQL distinto favor de contactar a su asesor '+CR_LF+
                           ' el cual podr� indicarle los pasos necesarios para actualizar a Sistema TRESS Corporativo';

{$R *.DFM}

procedure TBaseShell.FormCreate(Sender: TObject);
begin

     ZetaCommonTools.Environment;
     with Application do
     begin
          OnHint := MuestraPista;
          OnException := ManejaExcepcion;
          FOldHelpEvent := OnHelp;
          OnHelp := HTMLHelpHook;
          {$IFDEF TRESS_DELPHIXE5_UP}
          StatusBar.Visible := FALSE;
          StatusBar_DevEx.Visible := TRUE;
          {$ELSE}
          StatusBar.Visible := TRUE;
          StatusBar_DevEx.Visible := FALSE;
          {$ENDIF}
     end;
     EmpresaAbierta := False;
     ZetaRegistryCliente.InitClientRegistry;
     with Application do
     begin
          OnIdle := ShellOnIdle;
          OnMessage := ShellOnMessage;
          UpdateFormatSettings := FALSE;
     end;
{$ifndef DOS_CAPAS}
     FListaParametros := TStringList.Create;
     GetListaParametros;
{$ENDIF}

     DevEx_ShellRibbon.ShowTabHeaders := False;

end;

procedure TBaseShell.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     with Application do
     begin
          CanClose := ZetaDialogo.ZConfirm( 'Salir de ' + Title, '� Desea salir de ' + Title + ' ?', 0, mbYes );
     end;
end;

procedure TBaseShell.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     CierraTodo;
end;

procedure TBaseShell.FormDestroy(Sender: TObject);
begin
{$ifndef DOS_CAPAS}
     FreeAndNil( FListaPArametros );
{$ENDIF}
     Application.OnHelp := FOldHelpEvent;
     Logout;
     ZetaRegistryCliente.ClearClientRegistry;
end;

{$IFDEF TRESS_DELPHIXE5_UP}
function TBaseShell.HTMLHelpHook(Command: Word; Data: THelpEventData; var CallHelp: Boolean): Boolean;
{$ELSE}
function TBaseShell.HTMLHelpHook(Command: Word; Data: Integer; var CallHelp: Boolean): Boolean;
{$ENDIF}
begin
    Result := FHelpManager.HtmlHelpHook( Application.HelpFile, Command, Data, CallHelp );
end;

{ **********  Funciones de Entrada al Sistema ********** }

procedure TBaseShell.BeforeRun;
begin
     AbreEmpresa( False );
end;

function TBaseShell.MostrarMensajeReply(eReply:eLogReply):Boolean;
var
   lIntento:Boolean;
begin
     Result := MostrarMensajeReply(eReply,lIntento);
end;


function TBaseShell.MostrarMensajeReply(eReply:eLogReply;var lOtroIntento:Boolean):Boolean;
begin
     Result := False;
     lOtroIntento := True;
     case eReply of
          lrLoggedIn:
                     begin
                          lOtroIntento := False;
                          ShowLoginDenied( '� Usuario Ya Entr� Al Sistema !', 'Ya Hay Otro Usuario Con Esta Clave En El Sistema' );
                     end;
          lrLockedOut:
                      begin
                          lOtroIntento := False;
                          ShowLoginDenied( '� Usuario Bloqueado !', 'El Administrador Ha Bloqueado Su Acceso Al Sistema');
                      end;
          lrOK: Result := True;
          lrChangePassword: Result := True;
          lrExpiredPassword: Result := True;
          lrInactiveBlock:
                          begin                                                
                               lOtroIntento := False;
                               ShowLoginDenied( '� Usuario Bloqueado !', 'Usuario Ha Sido Bloqueado Por No Entrar Al Sistema Durante Mucho Tiempo' );
                          end;
          lrSystemBlock:
                        begin
                          lOtroIntento := False;
                          ShowLoginDenied( '� Sistema Bloqueado !', 'El Administrador Ha Bloqueado el Acceso Al Sistema' );
                        end;

          lrUserInactive:
                         begin
                              lOtroIntento := False;
                              ShowLoginDenied( '� Usuario Desactivado !', 'El Administrador Ha Desactivado el Acceso Al Sistema');
                         end;
          lrDBServerInvalid:
                         begin
                              lOtroIntento := False;
                              ShowLoginDenied( '� Servidor de Base Datos Inv�lido  !',K_MESSAGE_PRO_MSSQL);
                         end;

     else
         Beep;
     end;
end;
//DevEx: Nuevo metodo de LogIn
function TBaseShell.Login( const lAllowReentry: Boolean ): Boolean;
var
   eReply: eLogReply;
   iTipo : Integer;
   eTipo : eTipoLogin;
begin
     {$ifdef DOS_CAPAS}
     Result := ZetaRegistryServerEditor.InitComparte;
     {$else}
     Result := ClientRegistry.InitComputerName;
     {$endif}
      //Nuevo
     if Result then
     begin
          //Este metodo lo pasamos a la forma ZetaUserLogin_DevEx y lo invocamos en el FormShow
          eReply := dmCliente.LoginActiveDirectory(lAllowReentry,iTipo);
          eTipo := eTipoLogin(iTipo);
          //Global para accesar el TipoLogIn desde la forma ZetaUserLogIn_DevEx
          case eTipo of
               tlAD:   //iTipo=1
                    begin
                         Result := MostrarMensajeReply(eReply);
                         if (eReply = lrNotFound )then
                         begin
                              ShowLoginDenied('Usuario Inv�lido',Format('El usuario: %s no tiene acceso al Sistema TRESS',[dmCliente.LoggedOnUserNameEx]));
                         end;
                    end;
               tlLoginTress: //iTipo=0
                            begin
                                 Result:= LoginTress_DevEx(lAllowReentry, eReply);
                            end;
               tlADLoginTress: //iTipo=2
                              begin
                                   Result := MostrarMensajeReply(eReply);
                              end;
          else
              Result:= LoginTress_DevEx( lAllowReentry,eReply);
          end;
     end;
     //Validacion de la version
     {$ifndef INTERFAZ_TRESS}
     if Result then  { Verifica Modo Demo o Kit de Destribuidor }
               begin
                    with Autorizacion do
                    begin
                         { Verifica Versi�n del Cliente vs Versi�n del Servidor }
                         //Si la version del servidor es valida, continua con las validaciones posteriores. De lo contrario cerrara la aplicacion.
                         if CheckVersion( VersionArchivo ) then
                         begin
                              {//**DevEx(@am): Este codigo ya no es necesario, pues se hace dentro de la nueva forma para Escoger Empresa.***
                              if EsDemo then
                                 EntradaVersionDemoKit( ES_DEMO );
                              if EsKit then
                                 EntradaVersionDemoKit( ES_KIT ); }
                                 Result := TRUE;
                         end
                         else
                         begin
                              Logout;
                              Result := False;
                         end;
                    end;
               end;
     //
     {$endif}
     if Result then
          begin
               if ( eReply = lrExpiredPassword ) then
                  ZetaDialogo.ZInformation( '� Clave Vencida !',
                                            'Su Clave de Acceso Anterior Ha Expirado' + CR_LF +
                                            'Favor De Proporcionar Una Nueva', 0 );
               if ( eReply in [ lrChangePassword, lrExpiredPassword ] ) then
                  Result := CambiaClaveUsuario;
               if not Result then      // Se neg� a cambiar Password
                  Logout;
          end;
     if Result then
     begin
          //US# 11757: Proteccion funcionalidad HTTP por licencia sentinel
          Result := ValidaConexionHTTP;
          StatusBarMsg( dmCliente.GetDatosUsuarioActivo.NombreCorto, K_PANEL_USER );
          MuestraDatosUsuarioActivo;
     end
     else
         StatusBarMsg( '', K_PANEL_USER );
end;
//

procedure TBaseShell.ShowLoginDenied( const sTitulo, sMsg: String );
begin
     ZetaDialogo.zError( sTitulo, sMsg, 0 );
end;
//DevEx: Metodo agregado para el nuevo LogIn

function TBaseShell.LoginTress_DevEx(const lAllowReentry:Boolean;var eReply: eLogReply): Boolean;
var
   wIntentos, wTope: Integer;
   oCursor: TCursor;
   lLoop : Boolean;
   sUser,sPassword : string;


   function GetTope: Word; //Indent Fix
   begin
        if ( wTope = 0 ) then
           Result := 3
        else
           Result := wTope;
   end;

begin
     lLoop := True;
     eReply := lrNotFound;
     wIntentos := 1;
     wTope := wIntentos;
     Result := False;

     {$ifndef DOS_CAPAS}
     if ( GetCredenciales( sUser, sPassword ) ) then //Metodo de Parametros al llamar al Ejecutable
     begin
          with dmCliente do
          begin
               eReply := GetUsuario( sUser, ZetaServerTools.Decrypt(sPassword), False );
               Result := MostrarMensajeReply(eReply,lLoop);
               wTope := DatosSeguridad.PasswordIntentos;
               if not Result then
                    ShowLoginDenied( '� Contrase�a Incorrecta !', 'La contrase�a es incorrecta.' );
          end;
     end
     else
     begin
     {$ENDIF}
          repeat
                ZetaUserLogin_DevEx.InitUserLogin;
                //Se descarta el uso de registro para obtener credenciales.
                with ZUserLogin_DevEx do
                begin
                     NumeroIntentosLogin := wIntentos;
                     RespuestaLogin := eReply;
                     ShowModal;
                     if ( ModalResult = mrOK ) then
                     begin
                          oCursor := Screen.Cursor;
                          Screen.Cursor := crHourglass;
                          Application.ProcessMessages;
                          sUser := UserName;
                          sPassword := UserPassword;
                          with dmCliente do
                          begin
                               eReply := GetUsuario( sUser, sPassword, lAllowReentry );
                               Result := MostrarMensajeReply(eReply,lLoop);
                               wTope := DatosSeguridad.PasswordIntentos;
                          end;
                          Screen.Cursor := oCursor;
                     end
                     else
                          lLoop := FALSE;
                end;

                Inc( wIntentos );
                if not Result and lLoop and ( wIntentos > GetTope ) then
                begin
                     lLoop := False;
                     if ( wTope > 0 ) then
                        dmCliente.UsuarioBloquea( sUser, wIntentos );
                end;
                ZUserLogin_DevEx.Free;
          until Result or not lLoop;
     {$ifndef DOS_CAPAS}
     end;
     {$ENDIF}
end;
//
function TBaseShell.CheckVersion( sServer: String ): Boolean;
var
   FServer: VersionInfo;
   sClient, sInfo: String;

function GetVersionNumber( var sVersion: String ): Word;
var
   i: Integer;
begin
     i := Pos( '.', sVersion );
     if ( i > 0 ) then
     begin
          Result := StrToIntDef( Copy( sVersion, 1, ( i - 1 ) ), 0 );
          sVersion := Copy( sVersion, ( i + 1 ), ( Length( sVersion ) - i ) );
     end
     else
     begin
          Result := StrToIntDef( sVersion, 0 );
          sVersion := '';
     end;
end;

begin
     if ZetaCommonTools.StrLleno( sServer ) then
     begin
          with FServer do
          begin
               MajorVersion := GetVersionNumber( sServer );
               MinorVersion := GetVersionNumber( sServer );
               Release := GetVersionNumber( sServer );
               Build := GetVersionNumber( sServer );
          end;
          ZetaClientTools.GetApplicationVersionNumber;
          Result := ( FServer.MajorVersion = ZetaClientTools.VersionArchivo.MajorVersion ) and
                    ( FServer.MinorVersion = ZetaClientTools.VersionArchivo.MinorVersion );
     end
     else
         Result := True; { No Hay Datos Con Los Cuales Comparar Versiones }
     if not Result then
     begin
          with FServer do
          begin
               sServer := Format( '%d.%d', [ MajorVersion,
                                             MinorVersion ] );
          end;
          with ZetaClientTools.VersionArchivo do
          begin
               sClient := Format( '%d.%d', [ MajorVersion,
                                             MinorVersion ] );
               if ( FServer.MajorVersion < MajorVersion ) or
                  ( FServer.MinorVersion < MinorVersion ) then
               begin
                    sInfo := Format( 'Actualice el servidor de procesos a la versi�n %s', [ sClient ] );
               end
               else
               begin
                    sInfo := Format( 'Actualice este programa a la versi�n %s', [ sServer ] );
               end;
          end;
          Result := ZetaDialogo.zErrorConfirm( '� Conflicto Entre Versiones !',
                                               Format( 'La versi�n del servidor de procesos de Sistema Tress ( %s )', [ sServer ] ) + CR_LF +
                                               Format( 'es diferente de la versi�n de este programa ( %s )', [ sClient ] ) + CR_LF +
                                               CR_LF +
                                               sInfo +
                                               CR_LF +
                                               '� Desea continuar ?', 0, mbCancel );
     end;
end;

function TBaseShell.CambiaClaveUsuario: Boolean;
begin
     ZCambiaClave_DevEx := TZCambiaClave_DevEx.Create( Self ); { No Vale la Pena reutilizar ya que solo se puede crear una vez }
     with ZCambiaClave_DevEx DO
     begin
          try
             ShowModal;
             Result := ( ModalResult = mrOK );
          finally
                 Free;
                ZCambiaClave_DevEx := nil;
          end;
     end
end;

procedure TBaseShell.Logout;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        with dmCliente do
        begin
             UsuarioLogout;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBaseShell.AbreEmpresa( const lDefault: Boolean );

   procedure MuestraBienvenida;
   begin
        {***Acontinuacion se verifica si el usuario esta utilizando la nueva imagen y si la variable del Registry indica que el usuario desea ver la presentacion***}
        {***Se valida el valor asignado a la variable del Regisgry, si no tiene uno se le asigna el default.
            La variable del Registry se crea al dar clic en el checkbox de no volverlo a mostrar.***}
        if ( ZetaCommonTools.StrToReal( VERSION_COMPILACION ) > ZetaCommonTools.StrToReal( ClientRegistry.VSPresentacion ) ) then
        begin
             if EsVersionActualBD then   //(@am):Solo se valida en TRESS, por lo tanto para otros ejecutables siempre sera FALSE.
                FBienvenida_DevEx.EntradaBienvenida_DevEx;
        end;
   end;
begin
     if EscogeEmpresa( lDefault ) then
     begin
          Application.ProcessMessages;
          CierraTodo;
          InitAnimation( 'Conectando Empresa...' );
          try
             dmCliente.SetCompany;
             dmCliente.SetCacheLookup;
             if dmCliente.CheckDBServer then
             begin
                  ClientRegistry.SetLastEmpresa( dmCliente.Usuario, dmCliente.Compania );
                  AbreTodo;
                  //DevEx( @am ): La presentacion de Bienvenida solo sale para proyectos de la nueva imagen
                  {$ifdef TRESS_DELPHIXE5_UP}
                          {***DevEx( @am ): Anteriormente se valido que la version del cliente sea la misma del servidor y que la version de la BD sea la mas nueva.
                                    En este momento de la ejecucion ya se conecto la empresa, solo hay que verificar si al iniciar el programa la BD
                                    tenia version 440 (si no la tenia y se eligio actualizar no aparecera la presentacion hasta la proxima vez que el usuario entre al programa)***}
                          {***DevEx( @am ): Otra condicion para que la presentacion pueda ser desplegada es que el usuario tenga permiso de Escritura en el registry. La validacion se
                                            define en el Get de la llave en la unidad ZetaRegistryCliente.*** }
                             MuestraBienvenida;

                 {$endif}
             end
             else
             begin
                  EndAnimation;
                  ZError('Sistema TRESS',K_MESSAGE_PRO_MSSQL,0);
             end;
          finally
                 EndAnimation;
          end;
     end
     else
     begin
          if not EmpresaAbierta then
          begin
               Application.ProcessMessages;
               CierraTodo;
               AbreSistema;
               FModoSistema := TRUE;
          end;
     end;
end;

function TBaseShell.EscogeEmpresa( const lDefault: Boolean ): Boolean;
var
   oCursor: TCursor;
   iCompanys: Integer;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        iCompanys := dmCliente.InitCompany;
     finally
            Screen.Cursor := oCursor;
     end;
     case iCompanys of
          0:
          begin
               zError( '� No Hay Compa��as !', 'No Hay Empresas En El Sistema', 0 );
               Result := False;
          end;
          1:
          begin
               with Autorizacion do
               begin
                    if EsDemo or EsKit then
                                 Result := EscogeUnaEmpresa_DevEx ( EsDemo, EsKit, ClientRegistry.GetLastEmpresa( dmCliente.Usuario ) )
                    else
                        Result := True; //Si es una sola empresa y No EsDemo, pasa directamente a conectar
               end;
          end;
          else
          begin
               with Autorizacion do
                    Result := EscogeUnaEmpresa_DevEx ( EsDemo, EsKit, ClientRegistry.GetLastEmpresa( dmCliente.Usuario ) );
          end;
     end;
     //Validar Version de Datos
     with dmCliente do
     begin
          if Result  then //Se escogio una empresa
          begin
                If iCompanys = 1 then
                begin
                     CheckVersion;
                     Result := ( ( UltimaVersion <> 0 ) and ( VersionDatos <> -1 ) ); // Exception al conectarse unica empresa
                end;
                if Result then
                   Result := CheckVersionDatos; //Se Actualiz� un BD
          end;
     end;
end;


function TBaseShell.CheckVersionDatos:Boolean;
begin
     Result := True;
end;

//US# 11757: Proteccion funcionalidad HTTP por licencia sentinel
procedure TBaseShell.CambiaServidor( lCambiaServidorHTTP: Boolean = False );
var
   lOk: Boolean;
   {$ifndef DOS_CAPAS}
   sComputerName: String;
   sWebPage,sWebUser,sWebPassword: string;
   eTipoConexion: TTipoConexion;
   {$endif}
begin
     inherited;
     {$ifdef DOS_CAPAS}
     lOk := ZetaRegistryServerEditor.EspecificaComparte;
     {$else}
     sComputerName := ClientRegistry.ComputerName;
     with TFindServidor_DevEx.Create( Application ) do
     begin
          try
             Servidor := sComputerName;

             {$ifdef HTTP_CONNECTION}
             eTipoConexion := ClientRegistry.TipoConexion;
             ConnectionType := ClientRegistry.TipoConexion;
             WebPage := ClientRegistry.URL;
             WebUser := ClientRegistry.UserName;
             WebPassword := ClientRegistry.Password;
             {$endif}

             if lCambiaServidorHTTP then
                ModalResult := mrOk
             else
                 ShowModal;
             if ( ModalResult = mrOk ) then
             begin
                  lOk := True;
                  sComputerName := Servidor;
                  {$ifdef HTTP_CONNECTION}
                  eTipoConexion := ConnectionType;
                  sWebPAge := WebPage;
                  sWebUser := WebUser;
                  sWebPassword := WebPassword;
                  {$endif}

             end
             else
                 lOk := False;
          finally
                 Free;
          end;
     end;
     {$endif}
     if lOk then
     begin
          if EmpresaAbierta then
             FEmpresaAbiertaAux := TRUE;
          CierraTodo; { Cierra Empresa Activa }
          if not lCambiaServidorHTTP then
             Logout;     { Logout al Usuario Activo }
          Application.ProcessMessages;
          if ClientRegistry.CanWrite then
          begin
               {$ifndef DOS_CAPAS}
               ClientRegistry.ComputerName := sComputerName;

               {$ifdef HTTP_CONNECTION}
               ClientRegistry.TipoConexion := eTipoConexion;
               ClientRegistry.URL := sWebPage;
               ClientRegistry.UserName := sWebUser;
               ClientRegistry.Password := sWebPassword;
               {$endif}

               {$endif}
          end;
          if Login( False ) then
          begin
               if not lCambiaServidorHTTP then
                  AbreEmpresa( False );
          end
          else
          begin
               //US# 11757: Proteccion funcionalidad HTTP por licencia sentinel
               //Solo mostrar el dialogo cuando se esta en modo sistema o una empresa este seleccionada(abierta)
               if FEmpresaAbiertaAux or FModoSistema then
                  ZetaDialogo.ZInformation( '� Atenci�n !',
                                         'Para Conectarse Al Nuevo Servidor' +
                                         CR_LF +
                                         'Es Necesario Volver A Entrar Al Sistema', 0 );
               FTerminaAplicacion := TRUE;
               Application.Terminate;
          end;
     end;
end;

procedure TBaseShell.MuestraPista(Sender: TObject);
begin
     StatusBarMsg( Application.Hint, K_PANEL_HINT );
end;

procedure TBaseShell.StatusBarMsg( const sMsg: String; const iPanel: Integer );
begin
    {$IFDEF TRESS_DELPHIXE5_UP}
      StatusBar_DevEx.Panels[ iPanel ].Text := sMsg;
    {$ELSE}
      StatusBar.Panels[ iPanel ].Text := sMsg;
    {$ENDIF}
end;

procedure TBaseShell.ManejaExcepcion(Sender: TObject; Error: Exception);
begin
     ZExcepcion( 'Error en ' + Application.Title, '� Se Encontr� un Error !', Error, 0 );
end;

procedure TBaseShell.InitCaption( const sValue,sTipoApp: String );
const
     {.$ifdef DOS_CAPAS
     K_TITULO = '%s Profesional: %s';
     {.$else
     K_TITULO = '%s Corporativa: %s';
     {.$endif.}

     K_TITULO = '%s %s: %s';
begin
     Caption := Format( K_TITULO, [ Application.Title,sTipoApp ,sValue ] );
end;

procedure TBaseShell.AbreTodo;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        DoOpenAll;
        SetTimerInfo;
     finally
        Screen.Cursor := oCursor;
     end;
     Application.ProcessMessages;
end;

procedure TBaseShell.AbreSistema;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        DoOpenSistema;
        SetTimerInfo;
     finally
        Screen.Cursor := oCursor;
     end;
     Application.ProcessMessages;
end;

procedure TBaseShell.SetTimerInfo;
begin
     FTimerOn := True;
     LogOffTimer.Interval:= dmCliente.GetSegundosTimeout * 1000;
end;

function TBaseShell.GetEmpresaAbierta: Boolean;
begin
     Result := dmCliente.EmpresaAbierta;
end;


procedure TBaseShell.SetEmpresaAbierta( const Value: Boolean );
begin
     with dmCliente do
     begin
          EmpresaAbierta := Value;
{$ifndef AES_MERIDA}
          if Value then
             InitCaption( GetDatosEmpresaActiva.Nombre ,TipoApplicacionStr  )
          else
              InitCaption( '< NO HAY EMPRESA SELECCIONADA >',VACIO );
{$endif}
     end;
     HabilitaControles;
end;

procedure TBaseShell.HabilitaControles;
begin
end;

procedure TBaseShell.CargaDerechos;
begin
     ZAccesosMgr.LoadDerechos;
end;

procedure TBaseShell.MuestraDatosUsuarioActivo;
begin
end;

procedure TBaseShell.DoOpenAll;
begin
     CargaDerechos;
     EmpresaAbierta := True;
end;

procedure TBaseShell.DoOpenSistema;
begin
     CargaDerechos;
end;

procedure TBaseShell.CierraTodo;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        DoCloseAll;
     finally
            Screen.Cursor := oCursor;
     end;

     Application.ProcessMessages;
end;

procedure TBaseShell.DoCloseAll;
begin
     EmpresaAbierta := False;
end;

procedure TBaseShell.PrintScreen;
begin
     Print;
end;

procedure TBaseShell.ReconectaMenu;
begin

end;

procedure TBaseShell.CreaNavBar;
begin

end;

procedure TBaseShell.LimpiaGruposNavBar;
begin

end;

procedure TBaseShell.LLenaOpcionesGrupos;
begin

end;

{function TBaseShell.GetSkinController: TdxSkinController;
begin
     Result:=nil;
end; } //BORRAR

function TBaseShell.GetNavBarImageList: TcxImageList;
begin
     Result:=nil;
end;

procedure TBaseShell.AsistenciaFechavalid(Valor:TDate);
begin
     
end;

procedure TBaseShell.RefrescaIMSSActivoPublico;
begin

end;

function TBaseShell.GetLookUpDataSetGeneral(  const eEntidad: TipoEntidad ): TZetaLookUpDataSet;
begin
     with dmDiccionario do
     begin
          if ( cdsLookupGeneral.Tag <> Ord( eEntidad ) ) then
          begin
               cdsLookupGeneral.Tag := Ord( eEntidad );
               cdsLookupGeneral.Close;
          end;
          Result := cdsLookupGeneral;
     end;
end;

function TBaseShell.GetLookUpDataSet(  const eEntidad: TipoEntidad): TZetaLookUpDataSet;
begin
     Result := NIL;
end;

function TBaseShell.ProcesaDialogo(oLookUp: TZetaLookupDataset; const eEntidad: TipoEntidad;
         const sFilter: String; var sKey, sDescription: String; var lEncontrado: Boolean ): Boolean;
begin
     Result := FALSE;
end;

function TBaseShell.BuscaDialogo(const eEntidad: TipoEntidad; const sFilter: String;
         var sKey, sDescription: String): Boolean;
const
     K_TP_TIPO_ORDER_BY = 'TP_TIPO';
var
   oLookUp: TZetaLookupDataset;
   lShowModificar, lActivo, lUsarConfidencialidad : Boolean;
begin
     oLookUp := GetLookUpDataSet( eEntidad );

     if (oLookup = NIL) then
        oLookUp := GetLookUpDataSetGeneral( eEntidad );

     if ( oLookUp <> nil ) then
     begin
          oLookUp.Conectar;
          if NOT ProcesaDialogo(oLookUp, eEntidad, sFilter, sKey, SDescription, Result ) then
          //DevEx
          begin
               {$IF (not Defined(VISITANTES) ) and ( not Defined(VISITANTES_MGR) ) and ( not Defined(SELECCION) ) and ( not Defined(WORKFLOWCFG) ) and ( not Defined(KIOSCOCFG) ) }
               if eEntidad = enTPeriodo then
               begin
                    lShowModificar := False;
                    lActivo := True;
                    lUsarConfidencialidad := True;
                    oLookup.IndexFieldNames := K_TP_TIPO_ORDER_BY;
                    Result := ZetaBusqueda_DevEx.ShowSearchForm( oLookup,  sFilter, sKey, sDescription, lShowModificar, lActivo, lUsarConfidencialidad );
               end
               else
                   Result := ZetaBusqueda_DevEx.ShowSearchForm( oLookup, sFilter, sKey, sDescription );
               {$else}
               Result := ZetaBusqueda_DevEx.ShowSearchForm( oLookup, sFilter, sKey, sDescription );
               {$ifend}
          end
     end
     else
     begin
          ZetaDialogo.ZWarning( Caption, 'B�squeda No Disponible', 0, mbOk );
          Result := False;
     end;
end;

{ Limite de Inactividad }

procedure TBaseShell.LogoffTimerTimer(Sender: TObject);
begin
     LogoffTimer.Enabled := False;
     FTimerOn := False;
     SuspendeSistema;
end;

procedure TBaseShell.ShellOnIdle(Sender: TObject; var Done: Boolean);
begin
     LogoffTimer.Enabled := FTimerOn;
end;

procedure TBaseShell.ShellOnMessage(var Msg: TMsg; var Handled: Boolean);
begin
     if FTimerOn then
     begin
          with Msg do
          begin
               if ( ( Message >= WM_KEYFIRST ) and ( Message <= WM_KEYLAST ) ) or ( ( Message >= WM_MOUSEFIRST ) and ( Message <= WM_MOUSELAST ) ) then
                  LogoffTimer.Enabled := False;
          end;
     end;
     {
     ER: No se implementa para V.2017 por el riesgo al cerrar versi�n, pero se sugiere evaluar para que funcione el ENTER
     En todos los controles editables de las formas de consulta (normalmente son controles de filtros)
     Se debe evaluar si limitar a los controles de las formas de consulta o incluir tambien controles del shell (ribbon,
     valores activos, etc.
     if ( Msg.Message = WM_CHAR ) then
     begin
          case Msg.wParam of
                VK_RETURN:
                          begin
                               // enter as tab on CheckBox
                               if Assigned( Screen.ActiveControl ) then
                               begin
                                    Msg.wParam :=  word(#0);//VK_TAB
                                    GetParentForm( Self ).Perform( WM_NEXTDLGCTL, 0, 0 );
                               end;
                          end;
          end;
     end;
     }
end;

function TBaseShell.EsWindows7: Boolean;
begin
     Result := ( SysUtils.Win32Platform = VER_PLATFORM_WIN32_NT ) and
               ( SysUtils.Win32MajorVersion >= 6);
end;

procedure TBaseShell.SuspendeSistema;
begin
     if EsWindows7 then
        Application.Restore;
     MueveFormas;
     if ( ZUserSuspend_DevEx = nil ) then
        ZUserSuspend_DevEx := TZUserSuspend_DevEx.Create( Application );
     with ZUserSuspend_DevEx do
     begin
          Segundos := Trunc( LogOffTimer.Interval / 1000 );
          ShowModal;
          case ModalResult of
               mrYes :
               begin
                    RestableceFormas;
                    FTimerOn := True;
               end;
               mrNo :
               begin
                    //RestableceFormas;
                    Logoff;
               end;
               mrAbort, mrCancel :
               begin
                    if DesconectaUsuario then
                    begin
                         CloseShell;
                         Application.Terminate;
                    end
                    else
                        RestableceFormas;
               end;
          end;
     end;
//Termina la invocacion
end;

procedure TBaseShell.MueveFormas;
var
   i : Integer;
begin
     with Screen do
     begin
          for i := FormCount-1 downto 0 do
          begin
               if ( Forms[i] = self ) or
                  ( ( Forms[i].Parent = nil ) and ( Forms[i].Visible ) and
                  ( ( fsModal in Forms[i].FormState ) or ( fsVisible in Forms[i].FormState ) ) ) then
               try
                  Forms[i].Tag := Forms[i].Top;
                  Forms[i].Top := Forms[i].Monitor.Height;        // Esto hace que se esconda la Forma
               except
               end;
          end;
          Application.ProcessMessages;
     end;
     dmCliente.FormsMoved := TRUE;
end;

procedure TBaseShell.RestableceFormas;
var
   i : Integer;
begin
     with Screen do
     begin
          for i := FormCount-1 downto 0 do
          begin
               if ( Forms[i] = self ) or
                  ( ( Forms[i].Parent = nil ) and ( Forms[i].Visible ) and
                  ( ( fsModal in Forms[i].FormState ) or ( fsVisible in Forms[i].FormState ) )
                  and ( Forms[i].Tag <> 0 ) ) then
               try
                  Forms[i].Top := Forms[i].Tag;  // Esto hace que Regrese la forma a su posici�n
                  Forms[i].Tag := 0;
               except
               end;
          end;
          Application.ProcessMessages;
     end;
     dmCliente.FormsMoved := FALSE;
end;

procedure TBaseShell.LogOff;
begin
     { SobreEscribir en Aplicaciones con Capacidad para Cambiar de Usuario }
     if DesconectaUsuario then
     begin
          CloseShell;
          Application.Terminate;
     end
     else
         RestableceFormas;
end;

procedure TBaseShell.CloseShell;
begin
     CierraTodo;
end;

function TBaseShell.DesConectaUsuario: Boolean;
begin
     try
        Logout;
        Result := TRUE;
     except
        Result := FALSE;
     end;
end;

{$ifndef DOS_CAPAS}
procedure TBaseShell.GetListaParametros;
var
   i: integer;
begin
     with FListaParametros do
     begin
          Clear;
          for i := 1 to ParamCount do
          begin
               Add( ParamStr( i ) );
          end;
     end;
end;
{$ENDIF}

{$ifndef DOS_CAPAS}
//Obtiene credenciales a partir de un usuario y contrasena para enviarla al programa ConsolaTress.exe
function TBaseShell.GetCredenciales(var Usuario, Contrasena: String): Boolean;
var
   ValoresCredenciales : TStringList;
begin
     result := false;
     if ( ParamCount = 1 ) then
     begin
          ValoresCredenciales := TStringList.Create;
          try
             try
                with ValoresCredenciales do
                begin
                     Delimiter := K_PIPE;
                     DelimitedText := ZetaServerTools.Decrypt( ParamStr(1) );
                end;
                Usuario := ValoresCredenciales[0];
                Contrasena := ValoresCredenciales[2];
                result := ( strLleno( Usuario ) and strLleno( Contrasena ) );
             except
                   result := false;
             end;
          finally
                 FreeAndNil(ValoresCredenciales);
          end;
     end;
end;
{$ENDIF}

//US# 11757: Proteccion funcionalidad HTTP por licencia sentinel
function TBaseShell.ValidaConexionHTTP: Boolean;
var
   lSalirTress: Boolean;
   sMensajeError: String;
   lLoop: Boolean;
   lCambiarServidor: Boolean;
begin
     Result := True;
     lCambiarServidor := True;
     repeat
           dmCliente.ValidarConexionHTTP( lLoop, lSalirTress, sMensajeError );
           if lSalirTress then
           begin
                Result := False;
           end;
           if lLoop then
           begin
                CambiaServidor( lCambiarServidor );
                if FTerminaAplicacion then
                begin
                     Result := False;
                     break;
                end;
           end;
     until not lLoop;
end;
//FIN

end.
