unit ZetaWizardFeedBackAbrir_DevEx;

interface

{$IFDEF TRESS_DELPHIXE5_UP}
uses
Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ZetaWizardFeedBack_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore,
  TressMorado2013, cxControls, cxContainer, cxEdit, Vcl.ImgList, cxTextEdit,
  cxMemo, Vcl.StdCtrls, cxButtons, Vcl.ExtCtrls, ZetaDBTextBox;
{$ELSE}
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, ZetaWizardFeedBack_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, cxControls, cxContainer, cxEdit, ImgList, cxTextEdit,
  cxMemo, StdCtrls, cxButtons, ExtCtrls, ZetaDBTextBox;
{$ENDIF}

type
  TWizardFeedbackAbrir_DevEx = class(TWizardFeedback_DevEx)
    AbreUbicacion: TcxButton;
    procedure AbreUbicacionClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  WizardFeedbackAbrir_DevEx: TWizardFeedbackAbrir_DevEx;

implementation

{$R *.dfm}

uses ZetaFilesTools, ZetaCommonTools, ZetaDialogo, dProcesos;

procedure TWizardFeedbackAbrir_DevEx.AbreUbicacionClick(Sender: TObject);
var sPath : String;
begin
     inherited;
     sPath := dmProcesos.RutaArchivoGenerado;
     if ( FileExists( sPath ) )then
         ZetaFilesTools.AbreArchivoEnExplorer( dmProcesos.RutaArchivoGenerado )
     else
         ZetaDialogo.zError( '� Error al abrir archivo !',Format( 'No existe el archivo en la ruta: %s',[ sPath ] ), 0 );
end;

procedure TWizardFeedbackAbrir_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
//     if StrLleno (dmCatalogos.CPArchivo) then
//     begin
//          AbreUbicacion.SetFocus;
//          AbreUbicacion.Visible := True;
//     end
//     else
//     begin
//          AbreUbicacion.Visible := False;
//          OK_DevEx.SetFocus;
//     end;
end;

end.
