unit ZBaseUtileriaShell;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseShell, ExtCtrls, ComCtrls, StdCtrls, Buttons, Psock, NMsmtp,
  ImgList, Menus, ActnList, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, TressMorado2013, dxSkinsDefaultPainters,
  dxStatusBar, dxRibbonStatusBar;

type
  TBaseUtileriaShell = class(TBaseShell)
    ActionList: TActionList;
    _A_OtraEmpresa: TAction;
    _A_Servidor: TAction;
    _A_SalirSistema: TAction;
    _H_AcercaDe: TAction;
    OpenDialog: TOpenDialog;
    ArbolImages: TImageList;
    procedure _A_OtraEmpresaExecute(Sender: TObject);
    procedure _A_ServidorExecute(Sender: TObject);
    procedure _A_SalirSistemaExecute(Sender: TObject);
    procedure _H_AcercaDeExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure _Per_GenerarPolizaExecute(Sender: TObject);
  private
    { Private declarations }
    FModoBatch: Boolean;
    FEnviarCorreo: boolean;    
    FBatchLog: TStrings;
    FMinParamsBatch : Integer;
    FSintaxisBatch  : String;
    FLogNameBatch   : String;
  protected
    procedure InitBitacora;
    procedure EscribeBitacoras; virtual;
    procedure ActivaControles( const lProcesando: Boolean ); virtual;
    procedure SetControlesBatch; virtual;
    procedure ProcesarArchivo; virtual;
    procedure CancelarProceso; virtual;
    procedure DoProcesar; virtual; abstract;
    procedure ReportaErrorDialogo(const sTitulo, sMensaje: String);
  public
    { Public declarations }
    property MinParamsBatch : Integer read FMinParamsBatch write FMinParamsBatch;
    property SintaxisBatch : String read FSintaxisBatch write FSintaxisBatch;
    property LogNameBatch : String read FLogNameBatch write FLogNameBatch;
    property ModoBatch : Boolean read FModoBatch;
    property EnviarCorreo : Boolean write FEnviarCorreo;
    procedure EscribeBitacora( const sMensaje: String );
    procedure EscribeError( const sMensaje: String );
    procedure ProcesarBatch; virtual;
  end;

const
     K_PARAM_EMPRESA  = 1;
     K_PARAM_USUARIO  = 2;
     K_PARAM_PASSWORD = 3;
     K_PARAM_ARCHIVO  = 4;
     K_BATCH_LOG = 'Interfase.LOG';
     K_BATCH_SINTAXIS = '%s EMPRESA USUARIO PASSWORD ARCHIVO';

var
  BaseUtileriaShell: TBaseUtileriaShell;

implementation

uses dGlobal, dCliente, dDiccionario,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaCommonLists,
     ZetaMessages,
     ZGlobalTress,
     ZAsciiTools,
     ZetaDialogo,
     ZetaAcercaDe_DevEx;

{$R *.DFM}

procedure TBaseUtileriaShell.FormCreate(Sender: TObject);
begin
     FEnviarCorreo := TRUE;
     FMinParamsBatch := K_PARAM_ARCHIVO;
     FSintaxisBatch := Format( K_BATCH_SINTAXIS, [ ExtractFileName( Application.ExeName ) ] );
     FLogNameBatch  := K_BATCH_LOG;
     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( Self );
     {$ifdef TIMBRADO}
     dmCliente.ModoSuper := True;
     {$endif}
     dmDiccionario := TdmDiccionario.Create( Self );
     FBatchLog := TStringList.Create;
     inherited;
end;

procedure TBaseUtileriaShell.FormDestroy(Sender: TObject);
begin
     inherited;
   {  dmCliente.Free;
     Global.Free;}
end;

procedure TBaseUtileriaShell.FormShow(Sender: TObject);
begin
     inherited;
     ActivaControles( FALSE );
end;

procedure TBaseUtileriaShell.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     if FModoBatch then
        ZAsciiTools.GrabaBitacora( FBatchLog, ZetaMessages.SetFileNameDefaultPath( FLogNameBatch ) );
end;

procedure TBaseUtileriaShell.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     CanClose := TRUE;
end;



{ Procesar Batch }

procedure TBaseUtileriaShell.ProcesarBatch;
begin
end;

procedure TBaseUtileriaShell.SetControlesBatch;
begin
end;



{ Procesos Generales del Shell }

procedure TBaseUtileriaShell.ProcesarArchivo;
begin
end;

procedure TBaseUtileriaShell.CancelarProceso;
begin
     // No hace nada en la base
end;

procedure TBaseUtileriaShell.InitBitacora;
begin
end;

procedure TBaseUtileriaShell.EscribeBitacoras;
begin
end;

procedure TBaseUtileriaShell.ActivaControles( const lProcesando: Boolean );
begin
end;



procedure TBaseUtileriaShell.ReportaErrorDialogo( const sTitulo, sMensaje: String );
begin
end;



procedure TBaseUtileriaShell.EscribeBitacora(const sMensaje: String);
begin
end;

procedure TBaseUtileriaShell.EscribeError(const sMensaje: String);
begin
end;

{ Action List }

procedure TBaseUtileriaShell._A_OtraEmpresaExecute(Sender: TObject);
begin
     AbreEmpresa( False );
end;

procedure TBaseUtileriaShell._A_ServidorExecute(Sender: TObject);
begin
     CambiaServidor;
end;

procedure TBaseUtileriaShell._A_SalirSistemaExecute(Sender: TObject);
begin
     Close;
end;

procedure TBaseUtileriaShell._H_AcercaDeExecute(Sender: TObject);
begin
     with TZAcercaDe_DevEx.Create( Self ) do
     begin
          try
             ShowModal;
          finally
             Free;
          end;
     end;
end;

{ Eventos del Shell }

procedure TBaseUtileriaShell._Per_GenerarPolizaExecute(Sender: TObject);
begin
     inherited;
     { No se hace nada } 
end;

end.
