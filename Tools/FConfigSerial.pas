unit FConfigSerial;

interface

uses ZBaseTerminalShell, Forms, ZBaseDlgModal, StdCtrls, Mask, ZetaNumero, Controls, Buttons, Classes, ExtCtrls,
  ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, Vcl.ImgList, cxButtons;

type
  TConfigSerial = class(TZetaDlgModal_DevEx)
    LabelBaudrate: TLabel;
    ComboBaudrate: TcxComboBox;
    LabelDataBits: TLabel;
    ComboDatabits: TcxComboBox;
    LabelStopbits: TLabel;
    ComboStopbits: TcxComboBox;
    LabelParity: TLabel;
    ComboParity: TcxComboBox;
    LabelDTRControl: TLabel;
    ComboDTRControl: TcxComboBox;
    LabelFlowControl: TLabel;
    ComboFlowControl: TcxComboBox;
    LabelRTSControl: TLabel;
    ComboRTSControl: TcxComboBox;
    LabelRxChar: TLabel;
    RxChar: TZetaNumero;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    FInfoPuerto: TInfoPuerto;
  public
    { Public declarations }
    property InfoPuerto: TInfoPuerto read FInfoPuerto write FInfoPuerto;
  end;

var
  ConfigSerial: TConfigSerial;

implementation

uses
  VaComm, VaPrst;

{$R *.DFM}

procedure TConfigSerial.FormShow(Sender: TObject);
begin
     inherited;
     with FInfoPuerto do
     begin
          ComboBaudrate.ItemIndex    := Ord( BaudRate ) - 1 ;
          ComboDataBits.ItemIndex    := Ord( Databits );
          ComboParity.ItemIndex      := Ord( Parity );
          ComboStopbits.ItemIndex    := Ord( StopBits );
          ComboDTRControl.ItemIndex  := Ord( DTRControl );
          ComboFlowControl.ItemIndex := FlowControl; // Ord( FlowControl );
          ComboRTSControl.ItemIndex  := Ord( RTSControl );
          RxChar.Valor               := Ord( EventChar );
     end;
     ActiveControl := ComboBaudrate;
end;

procedure TConfigSerial.OKClick(Sender: TObject);
begin
     inherited;
     with FInfoPuerto do
     begin
          BaudRate    := TVaBaudrate( ComboBaudrate.ItemIndex + 1);
          Databits    := TVaDataBits( ComboDatabits.ItemIndex );
          Parity      := TVaParity( ComboParity.ItemIndex );
          StopBits    := TVaStopBits( ComboStopbits.ItemIndex );
          DTRControl  := TVaControlDTR( ComboDTRControl.ItemIndex );
          FlowControl := ComboFlowControl.ItemIndex; // TVaFlowControl( ComboFlowControl.ItemIndex );
          RTSControl  := TVaControlRTS( ComboRTSControl.ItemIndex );
          EventChar   := AnsiChar( RxChar.ValorEntero );
     end;
     ModalResult := mrOk;
end;

end.
