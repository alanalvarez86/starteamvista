unit ZetaAliasManager;

interface

uses SysUtils, Classes, DBTables;

const
     K_INTERBASE = 'INTRBASE';
     K_SERVER_NAME = 'SERVER NAME';
     K_USER_NAME = 'USER NAME';
type
  {TAliasEditor class declaration}
  TZetaAliasManager = class(TObject)
  private
    { Private declarations }
    FParams: TStringList;
    FConfigMode: TConfigMode;
    FOldConfigMode: TConfigMode;
    function GetValue( const Alias, ValueName: String ): String;  //  Get the type of an alias
  protected
    { Protected declarations }
    procedure InitBDE;
    procedure RestoreConfigMode;
    procedure SetConfigMode;
    procedure SetParams( const sName, sValue: String );
  public
    { Public declarations }
    property Params: TStringList read FParams write FParams;
    property ConfigMode: TConfigMode read FConfigMode write FConfigMode;
    constructor Create;
    destructor Destroy; override;
    function CreateAlias( const sAliasName, sDatabase, sUserName: String; var sError: String ): Boolean;
    function Exists( const Alias: String ): Boolean;
    function GetDriver( const Alias: String ): String;
    function GetPath( const Alias: String ): String;
    function GetType( const Alias: String ): String;
    function GetServerName( const Alias: String ): String;
    procedure GetAliasList(Values: TStrings );
    procedure GetDriverParams( const sDriverName: String; Values: TStrings );
    procedure GetDriverList( Values: TStrings );
    procedure GetParams( const Alias: String; Values: TStrings );
    procedure PrepareAliasList( Values: TStrings ); //  Get a list of available aliases
    procedure Add( const Alias, sDriver: String; Values: TStrings);
    procedure Delete( const Alias: String );
    procedure Modify( const Alias: String; Values: TStrings );
  end;

function BuildFullDatabaseName( const sDatabaseFile, sComputerName: String ): String;
function GetDatabaseComputerName( const sValue: String ): String;
function GetDatabaseBkupFileName( const sBkupFolder, sDatabase: String ): String;
function GetDatabaseFileName( const sValue: String ): String;

implementation

uses ZetaWinAPITools,
     ZetaCommonTools;

function GetDatabaseComputerName( const sValue: String ): String;
var
   iPos: Integer;
begin
     iPos := Pos( ':\', sValue );
     if ( iPos > 3 ) then
        Result := Copy( sValue, 1, ( iPos - 3 ) )
     else
         Result := ZetaWinAPITools.GetComputerName;
end;

function GetDatabaseFileName( const sValue: String ): String;
var
   iPos: Integer;
begin
     iPos := Pos( ':\', sValue );
     if ( iPos > 1 ) then
        Result := Copy( sValue, ( iPos - 1 ), ( Length( sValue ) - ( iPos - 2 ) ) )
     else
         Result := sValue;
end;
                              
function GetDatabaseBkupFileName( const sBkupFolder, sDatabase: String ): String;
begin
     Result := GetDatabaseFileName( sDatabase );
     Result := ExtractFileName( ZetaCommonTools.StrTransform( Result, ExtractFileExt( Result ), '.gbk' ) );
     Result := ZetaCommonTools.VerificaDir( sBkupFolder ) + Result;
end;

function BuildFullDatabaseName( const sDatabaseFile, sComputerName: String ): String;
begin
     Result := sDatabaseFile;
     if ( Pos( sComputerName, Result ) = 0 ) and StrLleno( sComputerName ) then
     begin
          Result := Format( '%s:%s', [ sComputerName, Result ] );
     end;
end;

{ ************ TZetaAliasManager ************* }

constructor TZetaAliasManager.Create;
begin
     inherited Create;
     FParams := TStringList.Create;
     FConfigMode := [cfmPersistent];
end;

destructor TZetaAliasManager.Destroy;
begin
     FParams.Free;
     inherited Destroy;
end;

procedure TZetaAliasManager.InitBDE;
begin
     if not( Session.Active ) then
        Session.Active := True;
end;

procedure TZetaAliasManager.RestoreConfigMode;
begin
     Session.ConfigMode := FOldConfigMode;
end;

procedure TZetaAliasManager.SetConfigMode;
begin
     FOldConfigMode := Session.ConfigMode;
     Session.ConfigMode := FConfigMode;
end;

procedure TZetaAliasManager.Add( const Alias, sDriver: String; Values: TStrings ); // Add a new alias
begin
     InitBDE;
     SetConfigMode;
     try
        Session.AddAlias( Alias, sDriver, Values);
        Session.SaveConfigFile;
     finally
            RestoreConfigMode;
     end;
end;

procedure TZetaAliasManager.Delete( const Alias: String );  //  Delete an existing alias
begin
     InitBDE;
     SetConfigMode;
     try
        Session.DeleteAlias( Alias );
        Session.SaveConfigFile;
     finally
            RestoreConfigMode;
     end;
end;

function TZetaAliasManager.Exists( const Alias: String ): Boolean; // Check if an alias exists
begin
     InitBDE;
     SetConfigMode;
     try
        Result := Session.IsAlias( Alias );
     finally
            RestoreConfigMode;
     end;
end;

procedure TZetaAliasManager.Modify( const Alias: String; Values: TStrings ); //  Modify an existing alias
begin
     InitBDE;
     SetConfigMode;
     try
        Session.ModifyAlias( Alias, Values );
        Session.SaveConfigFile;
     finally
            RestoreConfigMode;
     end;
end;

procedure TZetaAliasManager.PrepareAliasList( Values: TStrings ); //  Get a list of available aliases
var
   i: Word;
begin
     GetAliasList( Values );
     with Values do
     begin
          try
             BeginUpdate;
             for i := 0 to ( Count - 1 ) do
                 Strings[ i ] := Strings[ i ] + '=' + Strings[ i ];
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TZetaAliasManager.GetAliasList( Values: TStrings ); //  Get a list of available aliases
begin
     InitBDE;
     SetConfigMode;
     try
        Session.GetAliasNames( Values );
     finally
            RestoreConfigMode;
     end;
end;

function TZetaAliasManager.GetDriver( const Alias: String ): String; //  Get the driver name for an alias.
begin
     InitBDE;
     SetConfigMode;
     try
        Result := Session.GetAliasDriverName( Alias );
     finally
            RestoreConfigMode;
     end;
end;

procedure TZetaAliasManager.GetDriverParams( const sDriverName: String; Values: TStrings );  //  Get the default parameters for specified driver
begin
     InitBDE;
     Session.GetDriverParams( sDriverName, Values );
end;

procedure TZetaAliasManager.GetDriverList(Values: TStrings);  // Get a list of currently installed drivers
begin
     InitBDE;
     Session.GetDriverNames( Values );
end;

procedure TZetaAliasManager.GetParams( const Alias: String; Values: TStrings );  //  Get the parameters for an alias
begin
     InitBDE;
     SetConfigMode;
     try
        Session.GetAliasParams( Alias, Values );
     finally
            RestoreConfigMode;
     end;
end;

procedure TZetaAliasManager.SetParams( const sName, sValue: String );
var
   i: Integer;
begin
     with FParams do
     begin
          i := IndexOfName( sName );
          if ( i >= 0 ) then
             Strings[ i ] := sName + '=' + sValue;
     end;
end;

function TZetaAliasManager.GetPath( const Alias: String ): String; //  Get the path for an alias
begin
     Result := GetValue( Alias, 'PATH' );
end;

function TZetaAliasManager.GetServerName( const Alias: String ): String;  //  Get the type of an alias
begin
     Result := GetValue( Alias, K_SERVER_NAME );
end;

function TZetaAliasManager.GetType( const Alias: String ): String;  //  Get the type of an alias
begin
     Result := GetValue( Alias, 'TYPE' );
end;

function TZetaAliasManager.GetValue( const Alias, ValueName: String ): String;  //  Get the type of an alias
var
   Values: TStringList;
begin
     Values := TStringList.Create;
     try
        GetParams( Alias,Values );
        Result := Values.Values[ ValueName ];
     finally
            Values.Free;
     end;
end;

function TZetaAliasManager.CreateAlias( const sAliasName, sDatabase, sUserName: String; var sError: String ): Boolean;
begin
     try
        GetDriverParams( K_INTERBASE, FParams );  //  Get the default parameters for specified driver
        SetParams( K_SERVER_NAME, sDatabase );
        SetParams( K_USER_NAME, sUserName );
        if Exists( sAliasName ) then
           Modify( sAliasName, FParams )
        else
            Add( sAliasName, K_INTERBASE, FParams );
        Result := True;
     except
           on Error: Exception do
           begin
                sError := Error.Message;
                Result := False;
           end;
     end;
end;

end.
