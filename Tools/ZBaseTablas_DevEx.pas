unit ZBaseTablas_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Mask, DBCtrls, Db, Buttons, ExtCtrls,
     ZetaKeyCombo,
     ZetaCommonLists,
     ZetaClientDataSet,
     ZetaEdit, ZetaNumero, ZetaSmartLists, ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus,
  
  TressMorado2013,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  dxSkinsCore, dxSkinsDefaultPainters;

type
  TEditTablas_DevEx = class(TBaseEdicion_DevEx)
    PanelDatos: TPanel;
    DBInglesLBL: TLabel;
    DBDescripcionLBL: TLabel;
    DBCodigoLBL: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    TB_INGLES: TDBEdit;
    TB_TEXTO: TDBEdit;
    TB_NUMERO: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FDataset: TZetaClientDataset;
  protected
    { Protected declarations }
    property ZetaDataset: TZetaClientDataSet read FDataset write FDataset;
    procedure AfterCreate; virtual;
    procedure AfterShow; virtual;
    procedure Connect; override;
    procedure DoLookup; override;
    procedure InitForma; virtual;
    procedure ShowLabel( oLabel: TLabel; const sTexto: String );
  public
    { Public declarations }
  end;
  TEditLookup = class(TEditTablas_DevEx)
  private
    { Private declarations }
    procedure SetLookupDataset(const Value: TZetaLookupDataSet);
    function GetLookupDataset: TZetaLookupDataSet;
  protected
    { Protected declarations }
    property LookupDataset: TZetaLookupDataSet read GetLookupDataset write SetLookupDataset;
    procedure Connect; override;
  end;

var
  EditTablas_DevEx: TEditTablas_DevEx;

implementation

{$R *.DFM}

uses ZetaCommonClasses,
     ZetaBuscador_DevEx;

procedure TEditTablas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     InitForma;
     dxBarButton_BuscarBtn.Visible:= ivAlways;
     FirstControl := TB_CODIGO;
     Height := 247;//239;
     AfterCreate;
end;

procedure TEditTablas_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     AfterShow;
end;

procedure TEditTablas_DevEx.AfterCreate;
begin
end;

procedure TEditTablas_DevEx.AfterShow;
begin
end;

procedure TEditTablas_DevEx.Connect;
begin
     ZetaDataset.Conectar;
     DataSource.DataSet := ZetaDataset;
end;

procedure TEditTablas_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', ZetaDataset );        // (icm)
end;

procedure TEditTablas_DevEx.InitForma;
begin
     Height := 275;
end;

procedure TEditTablas_DevEx.ShowLabel( oLabel: TLabel; const sTexto: String );
begin
     with oLabel do
     begin
          Visible := True;
          Caption := sTexto;
     end;
end;

{ ********* TEditLookup ********* }

procedure TEditLookup.Connect;
begin
     inherited Connect;
     Caption := LookupDataset.LookupName;
end;

function TEditLookup.GetLookupDataset: TZetaLookupDataSet;
begin
     Result := TZetaLookupDataset( Self.ZetaDataset );
end;

procedure TEditLookup.SetLookupDataset(const Value: TZetaLookupDataSet);
begin
     Self.ZetaDataset := Value;
end;

end.
