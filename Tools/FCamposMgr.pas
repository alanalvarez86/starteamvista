unit FCamposMgr;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, DB, DBCtrls, comctrls, ExtCtrls,
     ZetaNumero,
     ZetaEdit,
     ZetaFecha,
     ZetaKeyLookup,
     ZetaKeyLookup_DevEx,
     ZetaDBTextBox,
     ZetaKeyCombo,
     ZetacommonLists,
     ZetaTipoEntidad,
     cxControls,
     cxPc,
     cxScrollBar,
     ZetaClientDataSet;

const
     K_MAX_LENGTH_DESCRIP = 30;

type
  //eCampoTipo = ( cdtTexto, cdtNumero, cdtBooleano, cdtBoolStr );
  //eMostrar = ( arSiempre, arNunca, arVacio, arLleno );
  TCheckEntidad = function( const eTipoEntidad: TipoEntidad ): TZetaLookupDataSet of object;
  TClasificacion = class( TObject )
  private
    { Private declarations }
    FCodigo: String;
    FNombre: String;
    FPosicion: Word;
    FDerecho : Integer;
  public
    constructor Create;
    destructor Destroy; override;
    property Codigo: String read FCodigo write FCodigo;
    property Nombre: String read FNombre write FNombre;
    property Posicion: Word read FPosicion write FPosicion;
    property Derecho :Integer read FDerecho write FDerecho; 
    function Comparar(Value: TClasificacion): Integer;
    function PuedeModificar :Boolean;
    function PuedeImprimir: Boolean;

  end;
  TCampo = class( TObject )
  private
    { Private declarations }
    FTipo: eExCampoTipo;
    FLetrero: String;
    FCampo: String;
    FDefault: String;
    FPosicion: Word;
    FClasificacion: String;
    FIndice: Integer;
    FMostrar: eExMostrar;
    FEntidad: TipoEntidad;
    FAnchoTexto: Word;
    FValoresTexto: String;
    FObligatorio :Boolean;
    function GetNombre: String;
    function GetCapturar: Boolean;
    function GetCampoTexto: String;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Clasificacion: String read FClasificacion write FClasificacion;
    property Campo: String read FCampo write FCampo;
    property CampoTexto: String read GetCampoTexto;
    property Capturar: Boolean read GetCapturar;
    property Indice: Integer read FIndice write FIndice;
    property Letrero: String read FLetrero write FLetrero;
    property Nombre: String read GetNombre;
    property Posicion: Word read FPosicion write FPosicion;
    property Mostrar: eExMostrar read FMostrar write FMostrar;
    property Tipo: eExCampoTipo read FTipo write FTipo;
    property ValorDefault: String read FDefault write FDefault;
    property Entidad: TipoEntidad read FEntidad write FEntidad;
    property AnchoTexto: Word read FAnchoTexto write FAnchoTexto;
    property ValoresTexto: String read FValoresTexto write FValoresTexto;
    property Obligatorio: Boolean read FObligatorio write FObligatorio;
    procedure Inicializa;
    function Comparar(Value: TCampo): Integer;
    function TextoDisponible: String;
    function TextoEscogido: String;

  end;
  TListaClasificaciones = class( TObject )
  private
    { Private declarations }
    FClasificaciones: TList;
    function GetClasificacion(Index: Integer): TClasificacion;
    procedure Delete(const Index: Integer);
    procedure Ordena;
    function EsIgual( Grupo: TClasificacion; const sCodigo,sNombre: String ): Boolean;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Clasificacion[ Index: Integer ]: TClasificacion read GetClasificacion;
    function Add( const sCodigo, sNombre: String; const iPosicion: Word;const iDerecho:Integer ): TClasificacion;
    function Count: Integer;
    function Repetido(const sCodigo, sNombre: String; Original: TClasificacion = NIL ):Boolean;
    procedure Assign(Source: TListaClasificaciones);
    procedure Borrar(const sCodigo: String);
    procedure Clear;
    procedure Cargar(Lista: TStrings);
  end;
  TListaCampos = class( TObject )
  private
    { Private declarations }
    FCampos: TList;
    FDatasource: TDatasource;
    FForma: TForm;
    FPaginas: TWinControl;

    FCheckEntidad: TCheckEntidad;
    //FRedraw: Boolean;
    function GetCampo(Index: Integer): TCampo;
    function CuantosHayEnGrupo(const sCodigo: String): Integer;
    procedure Delete(const Index: Integer);
    procedure Ordena;
    procedure Cargar(Lista: TStrings; const sClasificacion: String );
    function ConstruyeGrupo( oGrupo: TClasificacion; const iCuantos: Integer; Padre: TWinControl ): Integer;
    function ConstruyeGrupoConsulta(oGrupo: TClasificacion; const iCuantos: Integer; Padre: TWinControl): Integer;
    function GetCampoPorField(sFieldName:string):TCampo;
  public
    { Public declarations }
    { Public declarations }
    {***DevEx(by am)***}
    //'NUEVO SCROLL' : Descomentar los siguientes metodos para ver funcionalidad del scroll de devexpress.
    {FPrevScrollPos: Integer;
    procedure DoOnScroll(Sender: TObject; ScrollCode: TScrollCode;
      var ScrollPos: Integer); }
    {***}
    constructor Create;
    destructor Destroy; override;
    property Campo[ Index: Integer ]: TCampo read GetCampo;
    property CampoField[ sFieldName: string ]:TCampo read GetCampoPorField;
    property Datasource: TDatasource read FDatasource write FDatasource;
    property Forma: TForm read FForma write FForma;
    //property Paginas: TPageControl read FPaginas write FPaginas;
    //property Redraw: Boolean read FRedraw;
    function Count: Integer;
    function GetLetreroEntidad( const oEntidad: TipoEntidad ): String;
    function ConstruyeForma( Grupos: TListaClasificaciones; const lEdicion, lShowTab: Boolean ): Integer;
    function ContruyeFormaEdicion( Grupos: TListaClasificaciones; oDataSource: TDataSource; oForma: TForm; oPaginas: TWinControl; oCheckEntidad: TCheckEntidad; const lShowTab: Boolean ): Integer;
    function ContruyeFormaConsulta( Grupos: TListaClasificaciones; oDataSource: TDataSource; oForma: TForm; oPaginas: TWinControl; oCheckEntidad: TCheckEntidad ): Integer;
    procedure Add( const eTipo: eExCampoTipo; const sLetrero, sCampo, sDefault, sClasificacion: String; const iPosicion: Word; const eComoMostrar: eExMostrar; const eEntidadLookup: TipoEntidad = enNinguno;
                   const iAnchoTexto: Integer = K_MAX_LENGTH_DESCRIP; const sValores: String = '';const lObligatorio:Boolean = False );
    procedure Assign(Source: TListaCampos);
    procedure BorrarClasificacion(const sCodigo: String);
    procedure Clear;
    procedure CargarDisponibles( Lista: TStrings );
    procedure CargarEscogidos( Lista: TStrings; const sClasificacion: String );
    procedure SetRedraw;
  end;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools;

const
     K_FACTOR_ANCHO = 0.06;
     K_MENOR = -1;
     K_IGUAL = 0;
     K_MAYOR = 1;

function ComparaCampos( Item1, Item2: Pointer ): Integer;
begin
     Result := TCampo( Item1 ).Comparar( TCampo( Item2 ) );
end;

function ComparaClasificaciones( Item1, Item2: Pointer ): Integer;
begin
     Result := TClasificacion( Item1 ).Comparar( TClasificacion( Item2 ) );
end;

{ ******** TClasificacion ********** }

constructor TClasificacion.Create;
begin
     FCodigo := VACIO;
     FNombre := VACIO;
     FPosicion := 0;
     FDerecho := MaxInt; //Este campo es para aquellas clasificaciones que manejan derecho de acceso. Las que no lo hacen, se pone el valor maximo de derecho.  
end;

destructor TClasificacion.Destroy;
begin
     inherited Destroy;
end;

function TClasificacion.Comparar(Value: TClasificacion): Integer;
begin
     if ( Self.Posicion < Value.Posicion ) then
        Result := K_MENOR
     else
         if ( Self.Posicion > Value.Posicion ) then
            Result := K_MAYOR
         else
             Result := AnsiCompareText( Self.Nombre, Value.Nombre );
end;

function TClasificacion.PuedeModificar: Boolean;
begin
     Result := ( FDerecho and K_DERECHO_CAMBIO ) > K_SIN_DERECHOS;
end;

function TClasificacion.PuedeImprimir: Boolean;
begin
     Result := ( FDerecho and K_DERECHO_IMPRESION ) > K_SIN_DERECHOS;
end;


{ ********* TListaClasificaciones *********** }

constructor TListaClasificaciones.Create;
begin
     FClasificaciones := TList.Create;
end;

destructor TListaClasificaciones.Destroy;
begin
     Clear;
     FreeAndNil( FClasificaciones );
     inherited Destroy;
end;

procedure TListaClasificaciones.Assign(Source: TListaClasificaciones);
var
   i: Integer;
begin
     Self.Clear;
     with Source do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with Clasificacion[ i ] do
               begin
                    Self.Add( Codigo, Nombre, Posicion,Derecho );
               end;
          end;
     end;
end;

procedure TListaClasificaciones.Clear;
var
   i: Integer;
begin
     with FClasificaciones do
     begin
          for i := ( Count - 1 ) downto 0 do
          begin
               Self.Delete( i );
          end;
          Clear;
     end;
end;

function TListaClasificaciones.Count: Integer;
begin
     Result := FClasificaciones.Count;
end;

procedure TListaClasificaciones.Delete(const Index: Integer);
begin
     Clasificacion[ Index ].Free;
     FClasificaciones.Delete( Index );
end;

function TListaClasificaciones.GetClasificacion(Index: Integer): TClasificacion;
begin
     with FClasificaciones do
     begin
          if ( Index >= 0 ) and ( Index < Count ) then
             Result := TClasificacion( Items[ Index ] )
          else
              Result := nil;
     end;
end;

procedure TListaClasificaciones.Ordena;
begin
     FClasificaciones.Sort( ComparaClasificaciones );
end;

procedure TListaClasificaciones.Cargar(Lista: TStrings);
var
   i: Integer;
begin
     Ordena;
     with Lista do
     begin
          BeginUpdate;
          try
             Clear;
             for i := 0 to ( Self.Count - 1 ) do
             begin
                  Lista.AddObject( Clasificacion[ i ].Nombre, Clasificacion[ i ] )
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

function TListaClasificaciones.Add( const sCodigo, sNombre: String; const iPosicion: Word;const iDerecho:Integer ): TClasificacion;
begin
     Result := TClasificacion.Create;
     try
        FClasificaciones.Add( Result );
        with Result do
        begin
             Codigo := sCodigo;
             Nombre := sNombre;
             Posicion := iPosicion;
             Derecho := iDerecho;
        end;
     except
           on Error: Exception do
           begin
                FreeAndNil( Result );
                raise;
           end;
     end;
end;

procedure TListaClasificaciones.Borrar(const sCodigo: String );
var
   i: Integer;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          if ( Clasificacion[ i ].Codigo = sCodigo ) then
          begin
               Delete( i );
               Break;
          end;
     end;
end;

function TListaClasificaciones.EsIgual( Grupo :TClasificacion; Const sCodigo,sNombre : String ):Boolean;
begin
     Result := ( AnsiCompareText( Grupo.Codigo,sCodigo ) = 0 ) or ( AnsiCompareText( Grupo.Nombre, sNombre ) = 0 );
end;

function TListaClasificaciones.Repetido(const sCodigo, sNombre: String;Original: TClasificacion): Boolean;
 var
    i: Integer;
begin
     Result := False;
     for i := 0 to ( Count - 1 ) do
     begin
          if not Assigned(Original) or not EsIgual(Clasificacion[i],Original.Codigo,Original.Nombre) then
          begin
               if EsIgual(Clasificacion[i],sCodigo,sNombre) then
               begin
                    Result := True;
                    Break;
               end;
          end;
     end;
end;

{ ******** TCampo ********* }

constructor TCampo.Create;
begin
     FTipo := xctTexto;
     FLetrero := VACIO;
     FCampo := VACIO;
     FEntidad := enNinguno;
end;

destructor TCampo.Destroy;
begin
     inherited Destroy;
end;

function TCampo.GetNombre: String;
begin
     Result := Format( '%s # %d', [ObtieneElemento( lfExCampoTipo, Ord(FTipo) ),FIndice] );
end;

function TCampo.GetCampoTexto: String;
begin
     Result := StringReplace( Campo, '_BOL', '_DES', [ rfReplaceAll, rfIgnoreCase ] );
end;

function TCampo.GetCapturar: Boolean;
begin
     Result := ( FClasificacion <> VACIO );
end;

function TCampo.Comparar( Value: TCampo ): Integer;
begin
     if ( Self.Posicion < Value.Posicion ) then
        Result := K_MENOR
     else
         if ( Self.Posicion > Value.Posicion ) then
            Result := K_MAYOR
         else
             Result := AnsiCompareText( Self.Campo, Value.Campo );
end;

procedure TCampo.Inicializa;
begin
     FClasificacion := VACIO;
     FLetrero := VACIO;
     FDefault := VACIO;
     FMostrar := arSiempre;
     FPosicion := 0;
end;

function TCampo.TextoEscogido: String;
begin
     Result := Format( '%s ( %s )', [ Letrero, Nombre ] );
end;

function TCampo.TextoDisponible: String;
begin
     Result := Format( '%s', [ Nombre ] );
end;

{ ******* TListaCampos ******** }

constructor TListaCampos.Create;
begin
     FCampos := TList.Create;
     SetRedraw;
end;

destructor TListaCampos.Destroy;
begin
     Clear;
     FreeAndNil( FCampos );
     inherited Destroy;
end;

procedure TListaCampos.Assign( Source: TListaCampos );
var
   i: Integer;
begin
     Self.Clear;
     with Source do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with Campo[ i ] do
               begin
                    Self.Add( Tipo, Letrero, Campo, ValorDefault, Clasificacion, Posicion, Mostrar, Entidad, AnchoTexto, ValoresTexto,Obligatorio );
               end;
          end;
     end;
end;

procedure TListaCampos.SetRedraw;
begin
     //FRedraw := True;
     FDatasource := nil;
     FForma := nil;
     FPaginas := nil;
     FCheckEntidad := nil;
end;

function TListaCampos.CuantosHayEnGrupo(const sCodigo: String): Integer;
var
   i: Integer;
begin
     Result := 0;
     for i := 0 to ( Count - 1 ) do
     begin
          if ( Campo[ i ].Clasificacion = sCodigo ) then
          begin
               Inc( Result );
          end;
     end;
end;

function TListaCampos.GetLetreroEntidad( const oEntidad: TipoEntidad ): String;
var
   i: Integer;
begin
     Result := ZetaCommonClasses.VACIO;
     for i := 0 to ( Count - 1 ) do
     begin
          if ( Campo[ i ].Capturar ) and
             ( Campo[ i ].Tipo = xctTabla ) and
             ( Campo[ i ].Entidad = oEntidad ) then
          begin
               Result := Campo[ i ].Letrero;
               Break;
          end;
     end;
end;

procedure TListaCampos.Add( const eTipo: eExCampoTipo; const sLetrero, sCampo, sDefault, sClasificacion: String;
                            const iPosicion: Word; const eComoMostrar: eExMostrar; const eEntidadLookup: TipoEntidad = enNinguno;
                            const iAnchoTexto: Integer = K_MAX_LENGTH_DESCRIP; const sValores: String = '';const lObligatorio:Boolean = False );
var
   Campo: TCampo;
   sIndice: String;
begin
     sIndice := Copy( sCampo, 9, 2 );
     Campo := TCampo.Create;
     try
        FCampos.Add( Campo );
        with Campo do
        begin
             Tipo := eTipo;
             Letrero := sLetrero;
             Campo := sCampo;
             ValorDefault := sDefault;
             Clasificacion := sClasificacion;
             Posicion := iPosicion;
             Indice := StrToIntDef( StrTransform( sIndice, '_', '0' ), 0 );
             Mostrar := eComoMostrar;
             Entidad := eEntidadLookup;
             AnchoTexto := iAnchoTexto;
             ValoresTexto := sValores;
             Obligatorio := lObligatorio;
        end;
     except
           on Error: Exception do
           begin
                Campo.Free;
                raise;
           end;
     end;
end;

procedure TListaCampos.Clear;
var
   i: Integer;
begin
     with FCampos do
     begin
          for i := ( Count - 1 ) downto 0 do
          begin
               Self.Delete( i );
          end;
          Clear;
     end;
end;

function TListaCampos.Count: Integer;
begin
     Result := FCampos.Count;
end;

function TListaCampos.GetCampo(Index: Integer): TCampo;
begin
     with FCampos do
     begin
          if ( Index >= 0 ) and ( Index < Count ) then
             Result := TCampo( Items[ Index ] )
          else
              Result := nil;
     end;
end;

procedure TListaCampos.Delete(const Index: Integer);
begin
     Campo[ Index ].Free;
     FCampos.Delete( Index );
end;

procedure TListaCampos.Ordena;
begin
     FCampos.Sort( ComparaCampos );
end;

procedure TListaCampos.Cargar(Lista: TStrings; const sClasificacion: String );
var
   i: Integer;
begin
     Ordena;
     with Lista do
     begin
          BeginUpdate;
          try
             Clear;
             with FCampos do
             begin
                  for i := 0 to ( Count - 1 ) do
                  begin
                       if ( Campo[ i ].Clasificacion = sClasificacion ) then
                       begin
                            if Campo[ i ].Capturar then
                               Lista.AddObject( Campo[ i ].TextoEscogido, Campo[ i ] )
                            else
                                Lista.AddObject( Campo[ i ].TextoDisponible, Campo[ i ] );
                       end;
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TListaCampos.CargarDisponibles( Lista: TStrings );
begin
     Cargar( Lista, VACIO );
end;

procedure TListaCampos.CargarEscogidos( Lista: TStrings; const sClasificacion: String );
begin
     Cargar( Lista, sClasificacion );
end;

procedure TListaCampos.BorrarClasificacion(const sCodigo: String );
var
   i: Integer;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          if ( Campo[ i ].Clasificacion = sCodigo ) then
          begin
               Campo[ i ].Inicializa;
          end;
     end;
end;

function TListaCampos.ConstruyeGrupo( oGrupo: TClasificacion; const iCuantos: Integer; Padre: TWinControl ): Integer;
const
     K_LONGITUD_COMBO = 280;
     K_HEIGHT_MEMO = 60;  // ( 15 pixeles x 4 renglones )
var
   i, iMargenDerecho, iTop, iMiddle, iHeight, iWidth, iLeftBool, iWidthBool: Integer;

 procedure AsignaTagObligatorio( );
 begin
      with Self.Datasource do
           if ( ( Dataset <> NIL ) AND
              ( DataSet.FieldByName(Campo[i].Campo)  <> NIL ) ) then
                DataSet.FieldByName(Campo[i].Campo).Tag := BoolToInt(Campo[i].Obligatorio);
 end;


begin
     iWidth := Padre.Parent.Width;
     iMiddle := Trunc( iWidth / 3 );
     iMargenDerecho := Trunc( K_FACTOR_ANCHO * iWidth );
     iTop := 2;
     for i := 0 to ( Count - 1 ) do
     begin
          with Campo[ i ] do
          begin
               if ( Clasificacion = oGrupo.Codigo ) then
               begin
                    with TStaticText.Create( FForma ) do
                    begin
                         Name := Format( '%s_LBL', [ Campo ] );
                         Parent := Padre;
                         Alignment := taRightJustify;
                         Height := 13;
                         Top := iTop + 3;
                         Caption := Letrero + ':';
                         Left := iMiddle - 2 - Width;
                    end;
                    case Tipo of
                         xctTexto:
                         begin
                              with TDBEdit.Create( FForma ) do
                              begin
                                   Name := Format( '%s', [ Campo ] );
                                   DataField := Campo;
                                   Parent := Padre;
                                   iHeight := Height;
                                   Top := iTop;
                                   Left := iMiddle + 2;
                                   Width := iWidth - Left - iMargenDerecho;
                                   Enabled := True;
                                   Datasource := Self.FDatasource;
                                   {AP(28/06/08): Se cambi� para que tomara el ancho del campo desde la BD
                                   MaxLength := AnchoTexto;}
                                   MaxLength := 0;
                                   AsignaTagObligatorio;
                              end;
                         end;
                         xctNumero:
                         begin
                              with TZetaDBNumero.Create( FForma ) do
                              begin
                                   Name := Format( '%s', [ Campo ] );
                                   DataField := Campo;
                                   Parent := Padre;
                                   iHeight := Height;
                                   Top := iTop;
                                   Left := iMiddle + 2;
                                   Mascara := mnNumeroGlobal;
                                   Enabled := True;
                                   Datasource := Self.FDatasource;
                                   AsignaTagObligatorio;
                              end;
                         end;
                         xctBooleano, xctBoolStr:
                         begin
                              with TDBCheckBox.Create( FForma ) do
                              begin
                                   Name := Format( '%s', [ Campo ] );
                                   DataField := Campo;
                                   Caption := VACIO;
                                   Parent := Padre;
                                   Top := iTop + 2;
                                   Left := iMiddle + 2;
                                   Width := Height;
                                   AllowGrayed := False;
                                   Enabled := True;
                                   ValueChecked := K_GLOBAL_SI;
                                   ValueUnchecked := K_GLOBAL_NO;
                                   Datasource := Self.FDatasource;
                                   iHeight := Height + 4;
                                   iLeftBool := Left;
                                   iWidthBool := Width;
                                   //Checked := False;
                              end;
                              if ( Tipo = xctBoolStr ) then
                              begin
                                   with TDBEdit.Create( FForma ) do
                                   begin
                                        Name := Format( '%s', [ CampoTexto ] );
                                        DataField := CampoTexto;
                                        Parent := Padre;
                                        iHeight := Height;
                                        Top := iTop;
                                        Left := iLeftBool + iWidthBool; //+ 2;
                                        Width := iWidth - Left - iMargenDerecho;
                                        Enabled := True;
                                        Datasource := Self.FDatasource;
                                        MaxLength := AnchoTexto;
                                   end;
                              end;
                         end;
                         xctFecha :
                         begin
                              with TZetaDBFecha.Create( FForma ) do
                              begin
                                   Name := Format( '%s', [ Campo ] );
                                   DataField := Campo;
                                   Parent := Padre;
                                   iHeight := Height;
                                   Top := iTop;
                                   Left := iMiddle + 2;
                                   Enabled := True;
                                   Opcional := True;
                                   Datasource := Self.FDatasource;
                                   AsignaTagObligatorio;
                              end;
                         end;
                         xctTabla :
                         begin
                                   with TZetaDBKeyLookup_DevEx.Create( FForma ) do
                                   begin
                                        Name := Format( '%s', [ Campo ] );
                                        DataField := Campo;
                                        Parent := Padre;
                                        iHeight := Height;
                                        Top := iTop;
                                        Left := iMiddle + 2;
                                        Width := iWidth - Left - iMargenDerecho;
                                        WidthLlave := 60;
                                        Enabled := True;
                                        Datasource := Self.FDatasource;
                                        if Assigned( FCheckEntidad ) then
                                           LookupDataSet := FCheckEntidad( Entidad );
                                        AsignaTagObligatorio;
                                   end;
                         end;
                         xctMemo :
                         begin
                              with TDBMemo.Create( FForma ) do
                              begin
                                   Name := Format( '%s', [ Campo ] );
                                   DataField := Campo;
                                   Parent := Padre;
                                   Height := K_HEIGHT_MEMO;
                                   iHeight := Height;
                                   Top := iTop;
                                   Left := iMiddle + 2;
                                   Width := iWidth - Left - iMargenDerecho;
                                   Enabled := True;
                                   Datasource := Self.FDatasource;
                                   MaxLength := AnchoTexto;
                                   ScrollBars := ssVertical;
                                   WordWrap := TRUE;
                                   {$ifndef VER130}
                                   WantReturns := TRUE;
                                   {$endif}
                                   WantTabs := FALSE;
                                   AsignaTagObligatorio;
                              end;
                         end;                                             
                         xctCombo :
                         begin
                              with TDBComboBox.Create( FForma ) do
                              begin
                                   Name := Format( '%s', [ Campo ] );
                                   DataField := Campo;
                                   Parent := Padre;
                                   Style := csDropDownList;
                                   Top := iTop;
                                   Left := iMiddle + 2;
                                   Width := iMin( iWidth - Left - iMargenDerecho, K_LONGITUD_COMBO );
                                   Enabled := True;
                                   Datasource := Self.FDatasource;
                                   Items.CommaText := ValoresTexto;
                                   iHeight := Height;
                                   AsignaTagObligatorio;
                              end;
                         end;
                         xctListBox :
                         begin
                              with TDBListBox.Create( FForma ) do
                              begin
                                   Name := Format( '%s', [ Campo ] );
                                   DataField := Campo;
                                   Parent := Padre;
                                   Height := K_HEIGHT_MEMO;  // Presenta hasta 4 opciones, si hay mas requiere un scrollbar
                                   iHeight := Height;
                                   Top := iTop;
                                   Left := iMiddle + 2;
                                   Width := iMin( iWidth - Left - iMargenDerecho, K_LONGITUD_COMBO );
                                   Enabled := True;
                                   Datasource := Self.FDatasource;
                                   Items.CommaText := ValoresTexto;
                                   AsignaTagObligatorio;
                              end;
                         end;
                    else
                        iHeight := 0;
                    end;
                    iTop := iTop + iHeight + 1;
               end;
          end;
     end;
     Result := iTop;
end;


function TListaCampos.ConstruyeGrupoConsulta( oGrupo: TClasificacion; const iCuantos: Integer; Padre: TWinControl ): Integer;
const
     K_LONGITUD_TEXTO = 270;
     K_LONGITUD_LLAVE = 60;
     K_LONGITUD_FECHA = 80;
     K_LONGITUD_NUMERO = 80;
var
   i, iMargenDerecho, iTop, iMiddle, iHeight, iWidth, iLeftBool, iWidthBool: Integer;

begin
     iWidth := Padre.Parent.Width;
     iMiddle := Trunc( iWidth / 3 );
     iMargenDerecho := Trunc( K_FACTOR_ANCHO * iWidth );
     iTop := 2;

     for i := 0 to ( Count - 1 ) do
     begin
          with Campo[ i ] do
          begin
               if ( Clasificacion = oGrupo.Codigo ) then
               begin
                    with TStaticText.Create( FForma ) do
                    begin
                         Name := Format( '%s_LBL', [ Campo ] );
                         Parent := Padre;
                         Alignment := taRightJustify;
                         Height := 13;
                         Top := iTop + 3;
                         Caption := Letrero + ':';
                         Left := iMiddle - 2 - Width;
                    end;
                    case Tipo of
                         xctTexto:
                         begin
                              with TZetaDBTextBox.Create( FForma ) do
                              begin
                                   Name := Format( '%s', [ Campo ] );
                                   DataField := Campo;
                                   Parent := Padre;
                                   iHeight := Height;
                                   Top := iTop;
                                   Left := iMiddle + 2;
                                   Width := iMin( iWidth - Left - iMargenDerecho, K_LONGITUD_TEXTO );
                                   Enabled := True;
                                   Datasource := Self.FDatasource;
                              end;
                         end;
                         xctNumero:
                         begin
                              with TZetaDBTextBox.Create( FForma ) do
                              begin
                                   Name := Format( '%s', [ Campo ] );
                                   DataField := Campo;
                                   Parent := Padre;
                                   Alignment := taRightJustify;
                                   iHeight := Height;
                                   Top := iTop;
                                   Left := iMiddle + 2;
                                   Width := K_LONGITUD_NUMERO;
                                   Enabled := True;
                                   Datasource := Self.FDatasource;
                              end;
                         end;
                         xctBooleano, xctBoolStr:
                         begin
                              with TDBCheckBox.Create( FForma ) do
                              begin
                                   Name := Format( '%s', [ Campo ] );
                                   DataField := Campo;
                                   Caption := VACIO;
                                   Parent := Padre;
                                   Top := iTop + 2;
                                   Left := iMiddle + 2;
                                   Width := Height;
                                   AllowGrayed := False;
                                   Enabled := True;
                                   ValueChecked := K_GLOBAL_SI;
                                   ValueUnchecked := K_GLOBAL_NO;
                                   Datasource := Self.FDatasource;
                                   iHeight := Height + 2;
                                   iLeftBool := Left;
                                   iWidthBool := Width;
                                   ReadOnly := TRUE;
                              end;
                              if ( Tipo = xctBoolStr ) then
                              begin
                                   with TZetaDBTextBox.Create( FForma ) do
                                   begin
                                        Name := Format( '%s', [ CampoTexto ] );
                                        DataField := CampoTexto;
                                        Parent := Padre;
                                        iHeight := Height;
                                        Top := iTop;
                                        Left := iLeftBool + iWidthBool;
                                        Width := iMin( iWidth - Left - iMargenDerecho, K_LONGITUD_TEXTO - iWidthBool );
                                        Enabled := True;
                                        Datasource := Self.FDatasource;
                                   end;
                              end;
                         end;
                         xctFecha :
                         begin
                              with TZetaDBTextBox.Create( FForma ) do
                              begin
                                   Name := Format( '%s', [ Campo ] );
                                   DataField := Campo;
                                   Parent := Padre;
                                   iHeight := Height;
                                   Top := iTop;
                                   Left := iMiddle + 2;
                                   Width := K_LONGITUD_FECHA;
                                   Enabled := True;
                                   Datasource := Self.FDatasource;
                              end;
                         end;
                         xctTabla :
                         begin
                              with TZetaDBTextBox.Create( FForma ) do
                              begin
                                   Name := Format( '%s', [ Campo ] );
                                   DataField := Campo;
                                   Parent := Padre;
                                   Top := iTop;
                                   Left := iMiddle + 2;
                                   Width := K_LONGITUD_LLAVE;
                                   Enabled := True;
                                   Datasource := Self.FDatasource;
                                   iLeftBool := Left;
                                   iWidthBool := Width;
                              end;
                              with TZetaTextBox.Create( FForma ) do
                              begin
                                   Name := Format( 'Z%s', [ Campo ] );
                                   Parent := Padre;
                                   iHeight := Height;
                                   Top := iTop;
                                   Left := iLeftBool + iWidthBool;
                                   Width := iMin( iWidth - Left - iMargenDerecho, K_LONGITUD_TEXTO - iWidthBool );
                                   Enabled := True;
                                   Tag := Ord( Entidad );  // Para obtener la descripci�n de la entidad
                              end;
                              if Assigned( FCheckEntidad ) then
                                 FCheckEntidad( Entidad );  // No se asigna, pero se conecta
                         end;
                    else
                        iHeight := 0;
                    end;
                    iTop := iTop + iHeight + 1;
               end;
          end;
     end;
     Result := iTop;
end;

//'NUEVO SCROLL' : Descomentar el sig. metodo para ver la funcionalidad del scroll
{procedure TListaCampos.DoOnScroll(Sender: TObject; ScrollCode: TScrollCode;
  var ScrollPos: Integer);
var
  ATabSheet: TcxTabSheet;
begin
  ATabSheet := TcxScrollBar(Sender).Parent as TcxTabSheet;
  ATabSheet.ScrollBy(0, FPrevScrollPos - ScrollPos);
  FPrevScrollPos := ScrollPos;
end;}

function TListaCampos.ConstruyeForma( Grupos: TListaClasificaciones; const lEdicion, lShowTab: Boolean ): Integer;
//'NUEVO SCROLL'
{const
     K_WIDTH_SCROLLBAR = 17;}
var
   i, iCuantos: Integer;
   oScrollBar: TScrollBox;
   oScrollBar_DevEx: TScrollBox;
   //oCxScrollBar :TcxScrollBar;  //'NUEVO SCROLL' : Descomentar y comentar linea anterior
   oTab: TTabSheet;
   ocxTab : TcxTabSheet;
   oGrupo: TClasificacion;
   iMaxAltura: Integer;
   Contenedor : TWinControl;

   procedure AddTabClasico;
   begin
        oTab := TTabSheet.Create( FForma );
        with oGrupo do
        begin
             with oTab do
             begin
                  PageControl  := TPageControl(FPaginas);
                  Caption := Nombre;
                  TabVisible := lShowTab;
                  Tag := BoolToInt(PuedeImprimir) + 1;         // Identificar TabSheets agregados por este metodo
             end;

             oScrollBar := TScrollBox.Create( FForma );
             with oScrollBar do
             begin
                  Parent := oTab;
                  Align := alClient;
                  BorderStyle := bsNone;
                  OnDblClick := FForma.OnDblClick;  // PageControl y TabSheet no manejan este evento, debe reasignarse el de la forma de consulta para invocar la edici�n
             end;
        end;
   end;

   {***DevEx(@am): Funcionamiento de Scrollbar de DevExpess.
                   Quedo pendiente arreglar el evento OnScroll para que tope al darle click al boton inferior.
                   Tambien, quedO pendiente codificar el evento OnChange, para actualizar la posicion al navegar con Tab.
                   Para poner a funcionar este codigo descomentar Buscar 'NUEVO SCROLL' y descomentar el codigo correspondiente.
   ***}
   //'NUEVO SCROLL' : Descomentar los siguientes metodos
   {function GetContentHeight(ATabSheet: TcxTabSheet): Integer;
   var AHeight, I: Integer;
   begin
        AHeight := 0;
        for I := 0 to ATabSheet.ControlCount - 1 do
            with ATabSheet.Controls[I] do
                 if (Top + Height > AHeight) then
                    AHeight := Top + Height;
        Result := AHeight;
   end;
   procedure AddTabCx;
   begin
         ocxTab := TCxTabSheet.Create( FForma );
         with oGrupo do
         begin
              with ocxTab do
              begin
                   Parent := FPaginas;
                   Caption := Nombre;
                   TabVisible := lShowTab;
                   Tag := BoolToInt(PuedeImprimir) + 1;
                   OnDblClick := FForma.OnDblClick;        // Identificar TabSheets agregados por este metodo
              end;
              oCxScrollBar := TcxScrollBar.Create( FForma );
              with oCxScrollBar do
              begin
                   Parent := ocxTab;
                   Align := alRight;
                   Kind := sbVertical;
                   //BorderStyle := bsNone;
                   //OnDblClick := FForma.OnDblClick;  // PageControl y TabSheet no manejan este evento, debe reasignarse el de la forma de consulta para invocar la edici�n
              end;
         end;
   end; }
   {**DevEx(@am): Se dejo el mismo scrollbar que en la vista clasica, queda pendiente para VS 2015 poner uno de DevExpress, el codigo arriba es el avance que se tiene.***}
   procedure AddTabCx;
   begin
         ocxTab := TCxTabSheet.Create( FForma );
         with oGrupo do
         begin
              with ocxTab do
              begin
                   Parent := FPaginas;
                   Caption := Nombre;
                   TabVisible := lShowTab;
                   Tag := BoolToInt(PuedeImprimir) + 1;
                   //OnDblClick := FForma.OnDblClick;        // Identificar TabSheets agregados por este metodo
              end;
              oScrollBar_DevEx := TScrollBox.Create( FForma );
              with oScrollBar_DevEx do
              begin
                   {***DevEx(@am): Configuracion agregada para nueva imagen***}
                   Color := clWhite; //Los objetos se meten al scrollbar, asiq ue el color de fondo dependera de este objeto.
                   {***}
                  Parent := ocxTab;
                  Align := alClient;
                  BorderStyle := bsNone;
                  OnDblClick := FForma.OnDblClick;  // PageControl y TabSheet no manejan este evento, debe reasignarse el de la forma de consulta para invocar la edici�n
              end;
         end;
   end;


begin
     iMaxAltura := 0;
     Ordena; // Esta ordenando la lista de Campos
     { Esta ordenando la lista de los grupos }
     { Est� afuera del with para que sea mas legible }
     Grupos.Ordena;
     with Grupos do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               oGrupo := Clasificacion[ i ];
               with oGrupo do
               begin
                    iCuantos := CuantosHayEngrupo( Codigo );
                    if ( iCuantos > 0 ) then
                    begin
                         if  (not lEdicion ) or ( lEdicion and ( PuedeModificar ) )then
                         begin
                              AddTabCx;
                              Contenedor := oScrollBar_DevEx;
                              Application.ProcessMessages;
                              if lEdicion then
                                 iMaxAltura := iMax( ConstruyeGrupo( oGrupo, iCuantos, Contenedor ), iMaxAltura )
                              else
                                  iMaxAltura := iMax( ConstruyeGrupoConsulta( oGrupo, iCuantos, Contenedor ), iMaxAltura );

                              //'NUEVO SCROLL' : Descomentar los siguientes metodos para verlo funcionando
                              {***}
                              {if not FEsVistaClasica then
                              begin
                                   with oCxScrollBar do
                                   begin
                                        Min := 0;
                                        Max := GetContentHeight(ocxTab);
                                        if Max <= ocxTab.ClientHeight then
                                           Visible := false
                                        else
                                        begin
                                             Visible := true;
                                             PageSize := ocxTab.ClientHeight;
                                             FPrevScrollPos := Position;
                                             OnScroll := DoOnScroll;
                                        end;
                                        if not lEdicion then
                                           Width := K_WIDTH_SCROLLBAR;
                                   end;
                              end; }
                              {***}
                         end;
                    end;
               end;
          end;
     end;
     Result := iMaxAltura;
end;

function TListaCampos.ContruyeFormaEdicion( Grupos: TListaClasificaciones; oDataSource: TDataSource;
         oForma: TForm; oPaginas: TWinControl; oCheckEntidad: TCheckEntidad; const lShowTab: Boolean ): Integer;
begin
     FDatasource := oDataSource;
     FForma := oForma;
     FPaginas := oPaginas;
     FCheckEntidad := oCheckEntidad;
     Result := ConstruyeForma( Grupos, TRUE, lShowTab );
end;

function TListaCampos.ContruyeFormaConsulta( Grupos: TListaClasificaciones; oDataSource: TDataSource;
         oForma: TForm; oPaginas: TWinControl; oCheckEntidad: TCheckEntidad ): Integer;
begin
     FDatasource := oDataSource;
     FForma := oForma;
     FPaginas := oPaginas;
     FCheckEntidad := oCheckEntidad;
     Result := ConstruyeForma( Grupos, FALSE, TRUE );
end;

function TListaCampos.GetCampoPorField(sFieldName: string): TCampo;
var
   i:Integer;
begin
     Result := nil;
     if StrLLeno( sFieldName ) then
     begin
          For i := 0 to Count - 1 do
          begin
               If Campo[i].Campo = sFieldName then
               begin
                    Result := Campo[i];
                    Break;
               end;
          end;
     end;
end;


end.
