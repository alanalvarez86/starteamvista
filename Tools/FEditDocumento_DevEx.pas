unit FEditDocumento_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, Mask, DBCtrls, Buttons, StdCtrls, ExtCtrls,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, ImgList, cxButtons;

type
  TCargaDocumento_DevEx = function ( const sPath, sObservaciones : string; const lEncimarArchivo : Boolean ): Boolean of object;
  TEditDocumento_DevEx = class(TZetaDlgModal_DevEx)
    lblArchivo: TLabel;
    edArchivo: TEdit;
    Label5: TLabel;
    OpenDialog: TOpenDialog;
    edObservaciones: TEdit;
    btnArchivo_DevEx: TcxButton;
    procedure btnArchivoClick(Sender: TObject);
    procedure edArchivoChange(Sender: TObject);
    //procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure btnArchivo_DevExClick(Sender: TObject);

  private
    FAgregando : Boolean;
    FNomDocumento: String;
    FCargaDocumento: TCargaDocumento_DevEx;
    { Private declarations }
  public
    { Public declarations }
  end;

function EditarDocumento( const lAgregando: Boolean; const sNomDocumento: String; const iHelpCtx: LongInt; oCargaDocumento: TCargaDocumento_DevEx): Boolean;

var
  EditDocumento_DevEx: TEditDocumento_DevEx;

implementation

uses ZetaCommonClasses;

{$R *.DFM}

function EditarDocumento( const lAgregando: Boolean; const sNomDocumento: String; const iHelpCtx: LongInt; oCargaDocumento: TCargaDocumento_DevEx): Boolean;
begin
     with TEditDocumento_DevEx.Create( Application ) do
     begin
          try
             Result:= True;
             FAgregando:= lAgregando;
             FNomDocumento:= sNomDocumento;
             FCargaDocumento := oCargaDocumento;
             HelpContext:= iHelpCtx;
             ShowModal;
          finally
            Free;
          end;
     end;
end;

procedure TEditDocumento_DevEx.btnArchivoClick(Sender: TObject);
begin
     inherited;
     with OpenDialog do
     begin
          FileName := edArchivo.Text;
          if Execute then
          begin
               edArchivo.Text := FileName;
          end;
     end;
end;

procedure TEditDocumento_DevEx.edArchivoChange(Sender: TObject);
begin
     inherited;
     edObservaciones.Text := Copy( ExtractFileName( edArchivo.Text ),1,Pos( '.', edArchivo.Text )-1 );
end;

{procedure TEditDocumento_DevEx.OKClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if FCargaDocumento( edArchivo.Text, edObservaciones.Text, FAgregando ) then
           Close;
     finally
            Screen.Cursor := oCursor;
     end;
end;}

procedure TEditDocumento_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     edArchivo.Enabled := FAgregando;
     lblArchivo.Enabled := FAgregando;
     btnArchivo_DevEx.Enabled := FAgregando;
     
     edArchivo.Text := VACIO;
     edObservaciones.Text := VACIO;

     if FAgregando then
        edArchivo.SetFocus
     else
     begin
         edObservaciones.SetFocus;
         edObservaciones.Text := FNomDocumento;
     end

end;

procedure TEditDocumento_DevEx.OK_DevExClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if FCargaDocumento( edArchivo.Text, edObservaciones.Text, FAgregando ) then
           Close;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEditDocumento_DevEx.btnArchivo_DevExClick(Sender: TObject);
begin
  inherited;
     with OpenDialog do
     begin
          FileName := edArchivo.Text;
          if Execute then
          begin
               edArchivo.Text := FileName;
          end;
     end;
end;

end.
