unit ZcxBaseWizard;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls,
     ZetaKeyLookup_DevEx, ZcxWizardBasico,
     ZetaCommonClasses, ZetaWizard, cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, dxCustomWizardControl,
     dxWizardControl, ZetaCXWizard, dxSkinsCore, cxContainer, cxEdit,
     ZetaDBTextBox, cxGroupBox, cxLabel, dxGDIPlusClasses, cxImage,
     cxScrollBar, TressMorado2013,
     dxSkinsDefaultPainters;

type
  TcxBaseWizard = class(TCXWizardBasico)
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  //se elimino wizardalcancelar, provocaba que el wizard siempre se saliera 
    procedure WizardAfterMove(Sender: TObject);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
  private
    { Private declarations }
    FParametrosControl: TWinControl;
    FParameterList: TZetaParams;
    FDescripciones: TZetaParams;
    FGlobales: TZetaParams;
    FErrores: Integer;
    FParamInicial:Integer;
    procedure CargarGlobales;
    procedure DescargarGlobales;
    procedure EnfocaParametrosControl;
    procedure ConstruirParametros;
  protected
    { Protected declarations }
    property ParametrosControl: TWinControl read FParametrosControl write FParametrosControl;
    property ParameterList: TZetaParams read FParameterList;
    property Descripciones: TZetaParams read FDescripciones;
    property ParamInicial:Integer read FParamInicial write FParamInicial;
    function CancelarWizard: Boolean; virtual;
    function EjecutarWizard: Boolean; virtual; abstract;
    function Error(const sError: String; oControl: TWinControl): Boolean;
    function EjecutaProgramaArchivo( const sPrograma, sParametros, sArchivo: String; var sMensaje: String ): Boolean;
    procedure CargaParametros; virtual;
    procedure TerminarWizard; dynamic;
    function GetDescripLlave( oLookup: TZetaKeyLookup_DevEx ): string;

  public
    { Public declarations }
  end;

var
  cxBaseWizard: TcxBaseWizard;

implementation

uses DGlobal,
     ZetaDialogo,
     ZetaMessages,
     ZBasicoSelectGrid_DevEx,
     ZetaCommonTools, DB;

{$R *.DFM}

procedure TcxBaseWizard.FormCreate(Sender: TObject);
begin
     inherited;
     FParameterList := TZetaParams.Create;
     FDescripciones := TZetaParams.Create;
     FGlobales := TZetaParams.Create;
     ZBasicoSelectGrid_DevEx.ParametrosGrid := FDescripciones;
     FErrores := 0;

end;

procedure TcxBaseWizard.FormShow(Sender: TObject);

begin
     inherited;
     CargarGlobales;
     EnfocaParametrosControl;
end;

procedure TcxBaseWizard.FormDestroy(Sender: TObject);
begin
     inherited;
     FGlobales.Free;
     FDescripciones.Free;
     FParameterList.Free;
end;

procedure TcxBaseWizard.CargarGlobales;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           Global.CargaControles( FGlobales, Self );
        except
              on Error: Exception do
              begin
                   zExcepcion( Caption, 'Error al leer defaults del wizard', Error, 0 );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TcxBaseWizard.DescargarGlobales;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           Global.DescargaControles( FGlobales, Self );
        except
              on Error: Exception do
              begin
                   zExcepcion( Caption, 'Error al escribir defaults del wizard', Error, 0 );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TcxBaseWizard.CargaParametros;
begin
     
end;

function TcxBaseWizard.Error( const sError: String; oControl: TWinControl ): Boolean;
begin
     ZetaDialogo.ZError( Caption, sError, 0 );
     oControl.SetFocus;
     Result := False;
end;

function TcxBaseWizard.EjecutaProgramaArchivo( const sPrograma, sParametros, sArchivo: String; var sMensaje: String ): Boolean;
const
     K_OK_EXECUTE = 0;
     K_ERROR_EXECUTE = -1;
     K_ERROR_NOT_FOUND = 1;
     K_MESS_ERROR_EXECUTE = 'Error al ejecutar el programa: %s%s';
     K_MESS_ERROR_EXE_NOT_FOUND = 'El programa: %s%s%s no fu� encontrado';
     K_MESS_ERROR_FILE_NOT_FOUND = 'Archivo %s no fu� creado';
var
   oCursor: TCursor;
   iResultado : Integer;
begin
     if ZetaCommonTools.StrLleno( sPrograma ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             iResultado := ZetaMessages.WinExecAndWait32( sPrograma, sParametros, SW_SHOWNORMAL );
          finally
                 Screen.Cursor := oCursor;
          end;
          Result := ( iResultado = K_OK_EXECUTE );
          if Result then
          begin
               Result := FileExists( sArchivo );
               if not Result then
                  sMensaje := Format( K_MESS_ERROR_FILE_NOT_FOUND, [ sArchivo ] );
          end
          else
          begin
               case iResultado of
                    K_ERROR_EXECUTE: sMensaje := Format( K_MESS_ERROR_EXECUTE, [ CR_LF, sPrograma ] );
                    K_ERROR_NOT_FOUND: sMensaje := Format( K_MESS_ERROR_EXE_NOT_FOUND, [ CR_LF, sPrograma, CR_LF ] );
               { else
                     ZetaMessages.WinExecAndWait32 solo puede regresar valores : -1, 0 y 1 }
               end;
          end;
     end
     else
         Result := True;
end;

procedure TcxBaseWizard.EnfocaParametrosControl;
var
   lEnfocar: Boolean;
begin
     {if Wizard.EsPaginaActual( Parametros ) then
     begin}
          if ( ParametrosControl <> nil ) then
          begin
               with ParametrosControl do
               begin
                    lEnfocar := Visible and Enabled and CanFocus;
               end;
               if lEnfocar then
                  self.ActiveControl := ParametrosControl;
               //ActiveControl := ParametrosControl;
         end;
     //end;
end;

function TcxBaseWizard.CancelarWizard: Boolean;
begin
     Close;
     Result := True;
end;

procedure TcxBaseWizard.TerminarWizard;
begin
     DescargarGlobales;
     Close;
     Windows.SetForegroundWindow( Handle ); { Evita que la aplicaci�n pierda el Focus }
end;

procedure TcxBaseWizard.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        Application.ProcessMessages;
           try
              //CargaParametros; EZ : Se comento para que se desplegaran los parametros en la ultima pagina
              EjecutarWizard;
           except
                 on Error: Exception do
                 begin
                      zExcepcion( Caption, 'Error al ejecutar wizard', Error, 0 );
                      lOk := False;
                 end;
           end;
     finally
            Screen.Cursor := oCursor;
     end;
     if lOk then
        TerminarWizard;
end;

procedure TcxBaseWizard.WizardAfterMove(Sender: TObject);
begin
     inherited;
     if WizardControl.ActivePage = Ejecucion then
     begin
          CargaParametros;
          ConstruirParametros;
     end;
     EnfocaParametrosControl;
end;

function TcxBaseWizard.GetDescripLlave( oLookup: TZetaKeyLookup_DevEx ): string;
begin
     Result := VACIO;
     with oLookup do
          if StrLleno(Llave) then
             Result := Llave + ': ' + Descripcion;
end;

procedure TcxBaseWizard.ConstruirParametros();
var i:Integer;
    Etiqueta:TcxLabel;
    ZTexto:TZetaTextBox;

    function GetValueStringParam(Parametro:TParam):string;
    begin
         Result := Parametro.Text;
         if Parametro.DataType = ftBoolean then
         begin
              Result := BoolAsSiNo(Parametro.AsBoolean)
         end;

         if Parametro.DataType = ftDate then
         begin
              Result := DateToStr(Parametro.AsDate)
         end;


    end;
    procedure LimpiarControles;
    begin
         i := 0;
         with GrupoParametros do
         begin
              while (ControlCount > 0 ) do
              begin
                   Controls[i].Free;
              end;
         end;
    end;

begin
     LimpiarControles;
     with Descripciones do
     begin
          if  Count = 0 then
          begin
               Etiqueta := TcxLabel.Create(Self);
               With Etiqueta do
               begin
                    Parent := GrupoParametros;
                    Left := 8;
                    Top := 20;
                    //Width := 140;
                    Properties.Alignment.Horz := taLeftJustify;
                    Caption := 'El proceso no requiere par�metros.';
                    AutoSize := True;
                    Transparent := True;
               end;
          end;
          for i := FParamInicial to Count - 1 do
          begin
               Etiqueta := TcxLabel.Create(Self);
               With Etiqueta do
               begin
                    Parent := GrupoParametros;
                    Left := 135;
                    Top := (i+1-FParamInicial)*22;
                    //Width := 140;
                    Properties.Alignment.Horz := taRightJustify;
                    Caption := Items[i].Name+':';
                    AutoSize := False;
                    Transparent := True;
               end;
               ZTexto := TZetaTextBox.Create(Self);
               with ZTexto do
               begin
                    Parent := GrupoParametros;
                    Caption := GetValueStringParam(Items[i]);
                    Left := 143;
                    Top := (i-FParamInicial+1)*22;
                    Width := 240;
               end;

          end;
     end;
end;

end.
