unit ZetaBuscaEmpleadoNombre_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ExtCtrls, Db, Mask,
     ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
     Menus, dxSkinsCore, TressMorado2013, ImgList, cxButtons,
     dxSkinsDefaultPainters ;

type
  TBuscaEmpleadoNombre = class(TZetaDlgModal_DevEx)

    PanelControles: TPanel;
    ApellidoPaternoLabel: TLabel;
    ApellidoMaternoLabel: TLabel;
    NombresLBL: TLabel;
    ApellidoPaternoEdit: TEdit;
    ApellidoMaternoEdit: TEdit;
    NombresEdit: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FiltraTextoEditChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
    end;


var
  BuscaEmpleadoNombre: TBuscaEmpleadoNombre;



implementation

uses ZetaCommonClasses, ZFiltroSQLTools;


{$R *.DFM}


procedure TBuscaEmpleadoNombre.FormCreate(Sender: TObject);
begin
     HelpContext := H00012_Busqueda_empleados;
end;


procedure TBuscaEmpleadoNombre.FiltraTextoEditChange(Sender: TObject);
var
   OldChange: TNotifyEvent;
   OldStart : Integer;
begin
    with ( Sender as TEdit ) do
    begin
         OldChange := OnChange;
         OnChange  := nil;
         OldStart  := SelStart;
         Text := FiltrarTextoSQL( Text );
         OnChange := OldChange;
         SelStart := OldStart;
    end;
end;

procedure TBuscaEmpleadoNombre.FormShow(Sender: TObject);
begin
     inherited;
     NombresEdit.SetFocus;

end;

end.
