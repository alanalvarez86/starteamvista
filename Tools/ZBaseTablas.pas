unit ZBaseTablas;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Mask, DBCtrls, Db, Buttons, ExtCtrls,
     ZBaseEdicion,
     ZetaKeyCombo,
     ZetaCommonLists,
     ZetaClientDataSet,
     ZetaEdit, ZetaNumero, ZetaSmartLists;

type
  TEditTablas = class(TBaseEdicion)
    PanelDatos: TPanel;
    DBInglesLBL: TLabel;
    DBDescripcionLBL: TLabel;
    DBCodigoLBL: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    TB_INGLES: TDBEdit;
    TB_TEXTO: TDBEdit;
    TB_NUMERO: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FDataset: TZetaClientDataset;
  protected
    { Protected declarations }
    property ZetaDataset: TZetaClientDataSet read FDataset write FDataset;
    procedure AfterCreate; virtual;
    procedure AfterShow; virtual;
    procedure Connect; override;
    procedure DoLookup; override;
    procedure InitForma; virtual;
    procedure ShowLabel( oLabel: TLabel; const sTexto: String );
  public
    { Public declarations }
  end;
  TEditLookup = class(TEditTablas)
  private
    { Private declarations }
    procedure SetLookupDataset(const Value: TZetaLookupDataSet);
    function GetLookupDataset: TZetaLookupDataSet;
  protected
    { Protected declarations }
    property LookupDataset: TZetaLookupDataSet read GetLookupDataset write SetLookupDataset;
    procedure Connect; override;
  end;

var
  EditTablas: TEditTablas;

implementation

{$R *.DFM}

uses ZetaCommonClasses,
     ZetaBuscador;

procedure TEditTablas.FormCreate(Sender: TObject);
begin
     inherited;
     InitForma;
     BuscarBtn.Visible:= True;
     FirstControl := TB_CODIGO;
     Height := 247;//239;
     AfterCreate;
end;

procedure TEditTablas.FormShow(Sender: TObject);
begin
     inherited;
     AfterShow;
end;

procedure TEditTablas.AfterCreate;
begin
end;

procedure TEditTablas.AfterShow;
begin
end;

procedure TEditTablas.Connect;
begin
     ZetaDataset.Conectar;
     DataSource.DataSet := ZetaDataset;
end;

procedure TEditTablas.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', ZetaDataset );
end;

procedure TEditTablas.InitForma;
begin
     Height := 275;
end;

procedure TEditTablas.ShowLabel( oLabel: TLabel; const sTexto: String );
begin
     with oLabel do
     begin
          Visible := True;
          Caption := sTexto;
     end;
end;

{ ********* TEditLookup ********* }

procedure TEditLookup.Connect;
begin
     inherited Connect;
     Caption := LookupDataset.LookupName;
end;

function TEditLookup.GetLookupDataset: TZetaLookupDataSet;
begin
     Result := TZetaLookupDataset( Self.ZetaDataset );
end;

procedure TEditLookup.SetLookupDataset(const Value: TZetaLookupDataSet);
begin
     Self.ZetaDataset := Value;
end;

end.
