unit ZBaseArbolShell;

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, ComCtrls, Menus, ImgList, ActnList, StdCtrls, Buttons, {$ifndef VER130}Variants,{$endif}
     ZetaSmartLists,
     ZBaseShell,
     ZBaseConsulta,
     ZetaCommonLists,
     ZetaClientDataSet,
     ZetaTipoEntidad,
     ZetaDespConsulta, cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, dxSkinsCore, TressMorado2013, dxStatusBar,
     dxRibbonStatusBar, dxSkinsDefaultPainters;

type
  TBaseArbolShell = class;
  TFormManager = class(TObject)
  private
    { Private declarations }
    FShell: TBaseArbolShell;
    FFormas: TList;
    FFormaActiva: TBaseConsulta;
    function BorraUna(Forma: TBaseConsulta): Integer;
    function GetFormas( Index: Integer ): TBaseConsulta;
    procedure SetFormaActiva( Value: TBaseConsulta );
  public
    { Public declarations }
    constructor Create( Shell: TBaseArbolShell );
    destructor Destroy; override;
    property FormaActiva: TBaseConsulta read FFormaActiva write SetFormaActiva;
    property Formas[ Index: Integer ]: TBaseConsulta read GetFormas;
    function BorraFormaActiva: TBaseConsulta;
    function GetForm( InstanceClass: TBaseConsultaClass ): TBaseConsulta;
    function HayFormas: Boolean;
    procedure BorraTodas;
    procedure CambiaFormaActiva( Value: TBaseConsulta );
    procedure DesconectaFormaActiva;
  end;
  TBaseArbolShell = class(TBaseShell)
    PanelArbol: TPanel;
    Arbol: TTreeView;
    PanelBusca: TPanel;
    BuscarFormaBtn: TZetaSpeedButton;
    editBusca: TEdit;
    PanelConsulta: TPanel;
    ClientAreaPG: TPageControl;
    PanelToolBar: TPanel;
    ImprimirFormaBtn: TZetaSpeedButton;
    Imprimir: TZetaSpeedButton;
    Modificar: TZetaSpeedButton;
    Borrar: TZetaSpeedButton;
    Agregar: TZetaSpeedButton;
    MostrarArbol: TZetaSpeedButton;
    BuscarCodigoBtn: TZetaSpeedButton;
    RefrescarBtn: TZetaSpeedButton;
    CerrarVentana: TZetaSpeedButton;
    EscogeImpresora: TPrinterSetupDialog;
    ActionList: TActionList;
    _A_ConfigurarEmpresa: TAction;
    _A_CatalogoUsuarios: TAction;
    _A_ExploradorReportes: TAction;
    _A_Imprimir: TAction;
    _A_ImprimirForma: TAction;
    _A_ConfigurarImpresora: TAction;
    _A_Servidor: TAction;
    _A_SalirSistema: TAction;
    _E_Agregar: TAction;
    _E_Borrar: TAction;
    _E_Modificar: TAction;
    _E_Refrescar: TAction;
    _E_BuscarCodigo: TAction;
    _V_Cerrar: TAction;
    _V_CerrarTodas: TAction;
    _V_MostrarArbol: TAction;
    _V_ExpanderArbol: TAction;
    _V_CompactarArbol: TAction;
    _V_BuscarForma: TAction;
    _V_Calculadora: TAction;
    _H_Contenido: TAction;
    _H_Glosario: TAction;
    _H_UsoTeclado: TAction;
    _H_ComoUsarAyuda: TAction;
    _H_AcercaDe: TAction;
    ArbolImages: TImageList;
    ArbolPopup: TPopupMenu;
    ArbolMostrar: TMenuItem;
    ArbolExpander: TMenuItem;
    ArbolComprimir: TMenuItem;
    N56: TMenuItem;
    ArbolBuscarForma: TMenuItem;
    ClientAreaPopup: TPopupMenu;
    ClientAreaCerrar: TMenuItem;
    ClientAreaCerrarTodas: TMenuItem;
    N57: TMenuItem;
    ClientAreaBuscarCodigo: TMenuItem;
    ShellMenu: TMainMenu;
    Archivo: TMenuItem;
    ArchivoConfigurarEmpresa: TMenuItem;
    ArchivoCatalogoUsuarios: TMenuItem;
    N4: TMenuItem;
    ArchivoExplorardorReportes: TMenuItem;
    N6: TMenuItem;
    ArchivoImprimir: TMenuItem;
    ArchivoImprimirForma: TMenuItem;
    ArchivoImpresora: TMenuItem;
    N40: TMenuItem;
    ArchivoServidor: TMenuItem;
    N7: TMenuItem;
    ArchivoSalir: TMenuItem;
    Editar: TMenuItem;
    EditarAgregar: TMenuItem;
    EditarBorrar: TMenuItem;
    EditarModificar: TMenuItem;
    N8: TMenuItem;
    EditarRefrescar: TMenuItem;
    N9: TMenuItem;
    EditarBuscarCodigo: TMenuItem;
    Ventana: TMenuItem;
    VentanaCerrar: TMenuItem;
    VentanaCerrarTodas: TMenuItem;
    N1: TMenuItem;
    VentanaMostrarArbol: TMenuItem;
    VentanaExpanderArbol: TMenuItem;
    VentanaCompactarArbol: TMenuItem;
    VentanaBuscarForma: TMenuItem;
    N2: TMenuItem;
    VentanaCalculadora: TMenuItem;
    Ayuda: TMenuItem;
    AyudaContenido: TMenuItem;
    AyudaGlosario: TMenuItem;
    AyudaCombinacionesTeclas: TMenuItem;
    AyudaUsandoAyuda: TMenuItem;
    N11: TMenuItem;
    AyudaAcercaDe: TMenuItem;
    ListarProcesos: TZetaSpeedButton;
    _A_ListaProcesos: TAction;
    _A_OtraEmpresa: TAction;
    ArchivoOtraEmpresa: TMenuItem;
    ArchivoListaProcesos: TMenuItem;
    _A_ClaveAcceso: TAction;
    ClaveAcceso: TMenuItem;
    Registro: TMenuItem;
    Procesos: TMenuItem;
    N3: TMenuItem;
    ArbolSplitter: TSplitter;
    _A_Cache: TAction;
    ArchivoCache: TMenuItem;
    _H_NuevoTress: TAction;
    NuevoenTress: TMenuItem;
    Exportar: TZetaSpeedButton;
    _A_Exportar: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormaActivaDefocus(Sender: TObject);
    procedure FormaActivaFocus(Sender: TObject);
    procedure _SinImplementar(Sender: TObject);
    procedure _A_OtraEmpresaExecute(Sender: TObject);
    procedure _A_ConfigurarEmpresaExecute(Sender: TObject);
    procedure _A_ExploradorReportesExecute(Sender: TObject);
    procedure _A_ImprimirExecute(Sender: TObject);
    procedure _A_ImprimirFormaExecute(Sender: TObject);
    procedure _A_ConfigurarImpresoraExecute(Sender: TObject);
    procedure _A_ServidorExecute(Sender: TObject);
    procedure _A_SalirSistemaExecute(Sender: TObject);
    procedure _A_ClaveAccesoExecute(Sender: TObject);
    procedure _A_ListaProcesosExecute(Sender: TObject);
    procedure _A_CacheExecute(Sender: TObject);
    procedure _E_AgregarExecute(Sender: TObject);
    procedure _E_BorrarExecute(Sender: TObject);
    procedure _E_ModificarExecute(Sender: TObject);
    procedure _E_RefrescarExecute(Sender: TObject);
    procedure _E_BuscarCodigoExecute(Sender: TObject);
    procedure _E_BuscarCodigoUpdate(Sender: TObject);
    procedure _V_CerrarExecute(Sender: TObject);
    procedure _V_CerrarTodasExecute(Sender: TObject);
    procedure _V_MostrarArbolExecute(Sender: TObject);
    procedure _V_ExpanderArbolExecute(Sender: TObject);
    procedure _V_CompactarArbolExecute(Sender: TObject);
    procedure _V_BuscarFormaExecute(Sender: TObject);
    procedure _V_CalculadoraExecute(Sender: TObject);
    procedure _H_ContenidoExecute(Sender: TObject);
    procedure _H_GlosarioExecute(Sender: TObject);
    procedure _H_UsoTecladoExecute(Sender: TObject);
    procedure _H_ComoUsarAyudaExecute(Sender: TObject);
    procedure _H_AcercaDeExecute(Sender: TObject);
    procedure _HayEmpresaAbierta(Sender: TObject);
    procedure ArbolDblClick(Sender: TObject);
    procedure ArbolKeyPress(Sender: TObject; var Key: Char);
    procedure ClientAreaPGChange(Sender: TObject);
    procedure editBuscaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure _HayFormaAbierta(Sender: TObject);
    procedure EscogeImpresoraClose(Sender: TObject);
    procedure EscogeImpresoraShow(Sender: TObject);
    procedure _A_CacheUpdate(Sender: TObject);
    procedure _A_ServidorUpdate(Sender: TObject);
    procedure _H_NuevoTressExecute(Sender: TObject);
    procedure _A_ExportarExecute(Sender: TObject);
  private
    { Private declarations }
    FFormManager: TFormManager;
  protected
    { Protected declarations }
    FHelpGlosario: String;
    function HayFormaAbierta: Boolean;
    procedure AbreFormaConsulta( const TipoForma: eFormaConsulta );
    procedure CierraFormaActiva;
    procedure CierraFormaTodas;
    procedure ShowArbol(const lVisible: Boolean);
    procedure DoCloseAll; override;
    procedure HabilitaControles; override;
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado); virtual;
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado); virtual;
    {$endif}
    procedure CambioValoresActivos(const Valor: TipoEstado);
    procedure CargaDerechos; override;

  public
    { Public declarations }
    function FormaActiva: TBaseConsulta;
    procedure ReconectaFormaActiva;
    procedure CreaArbol( const lEjecutar: Boolean );
    procedure CreaArbolSistema( const lEjecutar: Boolean );
    {$ifdef MULTIPLES_ENTIDADES}
    procedure SetDataChange(const Entidades: array of TipoEntidad); virtual;
    {$else}
    procedure SetDataChange(const Entidades: ListaEntidades); virtual;
    {$endif}
  end;

var
  BaseArbolShell: TBaseArbolShell;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaClientTools,
     ZetaMessages,
     ZArbolTools,
     ZArbolTress,
     ZetaBusqueda,
     ZetaDialogo,
     ZAccesosMgr,
     ZetaAcercaDe,
     ZBaseDlgModal,
     ZetaRegistryCliente,
     ZAccesosTress,
     FActivaCache;

{$R *.DFM}

{ ************** TFormaMgr ******************* }

constructor TFormManager.Create( Shell: TBaseArbolShell );
begin
     FShell := Shell;
     FFormas := TList.Create;
     FFormaActiva := nil;
end;

destructor TFormManager.Destroy;
begin
     FFormaActiva := nil;
     FFormas.Free;
     inherited;
end;



function TFormManager.HayFormas: Boolean;
begin
     Result := ( FFormas.Count > 0 );
end;

function TFormManager.GetFormas(Index: Integer): TBaseConsulta;
begin
     Result := TBaseConsulta( FFormas.Items[ Index ] );
end;

function TFormManager.GetForm( InstanceClass: TBaseConsultaClass ): TBaseConsulta;
var
   i: Integer;
begin
     Result := nil;
     with FFormas do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if Formas[ i ].ClassNameIs( InstanceClass.ClassName ) then
               begin
                    Result := Formas[ i ];
                    Break;
               end;
          end;
     end;
end;

procedure TFormManager.DesconectaFormaActiva;
begin
     if ( FFormaActiva <> nil ) then
     begin
          with FFormaActiva do
          begin
               DoDisconnect;
          end;
     end;
end;

procedure TFormManager.CambiaFormaActiva( Value: TBaseConsulta );
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        DesconectaFormaActiva;
        with Value do
        begin
             Reconnect;
        end;
        FormaActiva := Value;
     finally
        Screen.Cursor := oCursor;
     end;
end;

procedure TFormManager.SetFormaActiva( Value: TBaseConsulta );
begin
     with FFormas do
     begin
          if ( Value <> nil ) and ( IndexOf( Value ) < 0 ) then
             Add( Value );
     end;
     FFormaActiva := Value;
     if ( FFormaActiva <> nil ) then
     begin
          with FShell.ClientAreaPG do
          begin
               HelpContext:= FFormaActiva.HelpContext;
               FFormaActiva.Pestania.AbreForma;
               SetFocus;
          end;
     end;
end;

function TFormManager.BorraUna( Forma: TBaseConsulta ): Integer;
begin
     with Forma do
     begin
          DoDisconnect;
          with Pestania do
          begin
               CierraForma;
               Free;
          end;
     end;
     with FFormas do
     begin
          Result := Remove( Forma );
     end;
end;

procedure TFormManager.BorraTodas;
var
   i: Integer;
begin
     with FFormas do
     begin
          for i := ( Count - 1 ) downto 0 do
          begin
               BorraUna( Formas[ i ] );
          end;
     end;
     FFormaActiva := nil;
end;

function TFormManager.BorraFormaActiva: TBaseConsulta;
var
   i: Integer;
begin
     if ( FFormaActiva = nil ) then
        Result := nil
     else
     begin
          i := BorraUna( FFormaActiva );
          FFormaActiva := nil;
          with FFormas do
          begin
               { Determinar candidato a Forma Activa }
               if ( Count = 0 ) then
                  Result := nil
               else
                   if ( Count = i ) then
                      Result := Formas[ i - 1 ]
                   else
                       Result := Formas[ i ];
          end;
     end;
end;

{ ZBaseArbolShell }

procedure TBaseArbolShell.FormCreate(Sender: TObject);
begin
     // Ayuda de Componentes
     Arbol.HelpContext                     := H00003_Explorador;
     ArchivoOtraEmpresa.HelpContext        := H00006_Menu_empresas;
     ArchivoCatalogoUsuarios.HelpContext   := H80813_Usuarios;
     ArchivoImprimir.HelpContext           := H00008_Archivo_imprimir;
     ArchivoImprimirForma.HelpContext      := H00009_Imprimir_forma;
     ArchivoImpresora.HelpContext          := H00010_Impresora;
     Editar.HelpContext                    := H00011_Menu_Editar;
     Ventana.HelpContext                   := H00013_Menu_ventana;
     EscogeImpresora.HelpContext           := H00010_Impresora;
     FHelpGlosario := VACIO;
     //
     FFormManager := TFormManager.Create( Self );
     inherited;
     WindowState:= wsMaximized;
     ShowArbol( True );
end;

procedure TBaseArbolShell.FormDestroy(Sender: TObject);
begin
     FFormManager.Free;
     inherited;
end;
procedure TBaseArbolShell.AbreFormaConsulta( const TipoForma: eFormaConsulta);
var
   oPropConsulta: TPropConsulta;
   oForma: TBaseConsulta;
   InstanceClass: TBaseConsultaClass;
   Pestania: TTabForm;
   lNueva: Boolean;
begin
     oPropConsulta := GetPropConsulta( TipoForma );
     if ZAccesosMgr.CheckDerecho( oPropConsulta.IndexDerechos, K_DERECHO_CONSULTA ) then
     begin
          InstanceClass := oPropConsulta.ClaseForma;
          if ( InstanceClass = nil ) then
             ZetaDialogo.zError( '� Error Al Crear Forma !', '� Clase de Ventana No Implementada !', 0 )
          else
          begin
               with FFormManager do
               begin
                    oForma := GetForm( InstanceClass );
                    if ( oForma = nil ) then
                    begin
                         try
                            oForma := InstanceClass.Create( Self ) as TBaseConsulta;
                            with oForma do
                            begin
                                 IndexDerechos := oPropConsulta.IndexDerechos;
                            end;
                         except
                               on Error: Exception do
                               begin
                                    ZetaDialogo.zError( '� Error Al Crear Forma !', '� Ventana No Pudo Ser Creada !', 0 );
                                    oForma := nil;
                               end;
                         end;
                         lNueva := True;
                    end
                    else
                        lNueva := False;
                    if ( oForma <> nil ) then
                    begin
                         if lNueva then
                         begin
                              DesconectaFormaActiva;
                              if oForma.DoConnect then
                              begin
                                   Pestania := TTabForm.Create( Self );
                                   Pestania.Forma := oForma;
                                   with Pestania do
                                   begin
                                        PageControl := ClientAreaPG;
                                        Visible := True;
                                   end;
                                   ClientAreaPG.ActivePage := Pestania;
                                   oForma.Show;
                                   FormaActiva := oForma;
                              end
                              else
                                  oForma.Free;
                         end
                         else
                             CambiaFormaActiva( oForma );
                    end;
               end;
          end;
     end
     else
         ZetaDialogo.zError( '� Error Al Crear Forma !', '� No Tiene Derechos de Consulta Para Esta Ventana !', 0 );
end;

function TBaseArbolShell.HayFormaAbierta: Boolean;
begin
     Result := FFormManager.HayFormas;
end;

function TBaseArbolShell.FormaActiva: TBaseConsulta;
begin
     Result := FFormManager.FormaActiva;
end;

procedure TBaseArbolShell.ReconectaFormaActiva;
begin
     with FFormManager do
     begin
          if ( FormaActiva <> nil ) then
             FormaActiva.ReConnect;
     end;
end;

procedure TBaseArbolShell.CierraFormaActiva;
var
   oForma: TBaseConsulta;
begin
     with FFormManager do
     begin
          oForma := BorraFormaActiva;
          if ( oForma <> nil ) then
          begin
               oForma.Reconnect;
               FormaActiva := oForma;
          end;
          if not HayFormas then
             ShowArbol( True );
     end;
end;

procedure TBaseArbolShell.CierraFormaTodas;
begin
     with Screen do
     begin
          Cursor := crHourglass;
          try
             with FFormManager do
             begin
                  BorraTodas;
                  if not HayFormas then
                     ShowArbol( True );
             end;
          finally
                 Cursor := crDefault;
          end;
     end;
end;

procedure TBaseArbolShell.DoCloseAll;
begin
     with Arbol.Items do
     begin
          BeginUpdate;
          Clear;
          EndUpdate;
     end;
     inherited DoCloseAll;
end;

procedure TBaseArbolShell.HabilitaControles;
begin
     inherited HabilitaControles;
     Editar.Enabled := TRUE;
     Registro.Enabled := EmpresaAbierta;
     Procesos.Enabled := TRUE;
     Ventana.Enabled := EmpresaAbierta;
     ArchivoConfigurarEmpresa.Enabled := TRUE;
     ArchivoExplorardorReportes.Enabled := EmpresaAbierta;
     ArchivoCatalogoUsuarios.Enabled := EmpresaAbierta;
end;

procedure TBaseArbolShell.CreaArbol(const lEjecutar: Boolean);
var
   oArbolMgr: TArbolMgr;
begin
     oArbolMgr := TArbolMgr.Create( Arbol );
     with oArbolMgr do
     begin
          try
             with Arbol.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     { Aqu� se agrega el arbol definido por el usuario }
                     ZArbolTress.CreaArbolUsuario( oArbolMgr, '' );
                     { Si no hubo selecci�n, selecciona el Primero y lo Abre }
                     if ( Arbol.Selected = nil ) then
                     begin
                          with Arbol.Items[ 0 ] do
                          begin
                               Selected := True;
                          end;
                     end;
                     { Se ejecuta el Nodo default Seleccionado por el Usuario }
                     if lEjecutar then
                     begin
                          if EsFolder( Arbol.Selected ) then
                             Arbol.Selected.Expand( False )
                          else
                             ArbolDblClick( Arbol );
                     end;
                  finally
                     EndUpdate;
                  end;
             end;
          finally
             Free;
          end;
     end;
end;

procedure TBaseArbolShell.CreaArbolSistema(const lEjecutar: Boolean);
var
   oArbolMgr: TArbolMgr;
begin
     oArbolMgr := TArbolMgr.Create( Arbol );
     with oArbolMgr do
     begin
          try
             with Arbol.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     { Aqu� se agrega el arbol definido por el usuario }
                     ZArbolTress.CreaArbolUsuario( oArbolMgr, '' );
                     { Si no hubo selecci�n, selecciona el Primero y lo Abre }
                     if ( Arbol.Selected = nil ) then
                     begin
                          with Arbol.Items[ 0 ] do
                          begin
                               Selected := True;
                          end;
                     end;
                     { Se ejecuta el Nodo default Seleccionado por el Usuario }
                     if lEjecutar then
                     begin
                          if EsFolder( Arbol.Selected ) then
                             Arbol.Selected.Expand( False )
                          else
                             ArbolDblClick( Arbol );
                     end;
                  finally
                     EndUpdate;
                  end;
             end;
          finally
             Free;
          end;
     end;
end;

procedure TBaseArbolShell.ShowArbol(const lVisible: Boolean);
begin
     PanelArbol.Visible := lVisible;
     with _V_MostrarArbol do
     begin
          Checked := lVisible;
          if Checked then
          begin
               Hint := 'Esconder Arbol de Formas';
               Caption := 'Esconde&r Arbol';
          end
          else
          begin
               Hint := 'Mostrar Arbol de Formas';
               Caption := 'Mostra&r Arbol';
          end;
     end;
end;

procedure TBaseArbolShell.CambioValoresActivos(const Valor: TipoEstado);
begin
     NotifyDataChange( [], Valor );
end;


{$ifdef MULTIPLES_ENTIDADES}
procedure TBaseArbolShell.SetDataChange(const Entidades: array of TipoEntidad);
{$else}
procedure TBaseArbolShell.SetDataChange(const Entidades: ListaEntidades); 
{$endif}
begin
     NotifyDataChange( Entidades, stNinguno );
end;

{$ifdef MULTIPLES_ENTIDADES}
procedure TBaseArbolShell.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
{$else}
procedure TBaseArbolShell.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
{$endif}
begin
     ReconectaFormaActiva;
end;

procedure TBaseArbolShell.EscogeImpresoraShow(Sender: TObject);
begin
     inherited;
     LogoffTimer.Enabled := False;
end;

procedure TBaseArbolShell.EscogeImpresoraClose(Sender: TObject);
begin
     inherited;
     LogoffTimer.Enabled := True;
end;

{ Action List }

procedure TBaseArbolShell._SinImplementar(Sender: TObject);
begin
     zInformation( 'Estamos Trabajando...', 'Esta Opci�n No Ha Sido Implementada', 0 );
end;

procedure TBaseArbolShell._A_ServidorUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := ClientRegistry.CanWrite;
end;

procedure TBaseArbolShell._A_CacheUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and ClientRegistry.CanWrite;
end;

procedure TBaseArbolShell._HayEmpresaAbierta(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta;
end;

procedure TBaseArbolShell._HayFormaAbierta(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := HayFormaAbierta;
end;

procedure TBaseArbolShell._E_BuscarCodigoUpdate(Sender: TObject);
begin
     TAction( Sender ).Enabled := HayFormaAbierta and FormaActiva.CanLookup;
end;

procedure TBaseArbolShell._A_OtraEmpresaExecute(Sender: TObject);
begin
     AbreEmpresa( False );
end;

procedure TBaseArbolShell._A_ConfigurarEmpresaExecute(Sender: TObject);
begin
     AbreFormaConsulta( efcSistGlobales );
end;

procedure TBaseArbolShell._A_ListaProcesosExecute(Sender: TObject);
begin
     AbreFormaConsulta( efcSistProcesos );
end;

procedure TBaseArbolShell._A_ExploradorReportesExecute(Sender: TObject);
begin
     AbreFormaConsulta( efcReportes );
end;

procedure TBaseArbolShell._A_ImprimirExecute(Sender: TObject);
begin
     FormaActiva.DoPrint;
end;

procedure TBaseArbolShell._A_ImprimirFormaExecute(Sender: TObject);
begin
     FormaActiva.DoPrintForma;
end;

procedure TBaseArbolShell._A_ExportarExecute(Sender: TObject);
begin
     inherited;
     FormaActiva.DoExportar;
end;

procedure TBaseArbolShell._A_ConfigurarImpresoraExecute(Sender: TObject);
begin
     EscogeImpresora.Execute;
end;

procedure TBaseArbolShell._A_ClaveAccesoExecute(Sender: TObject);
begin
     CambiaClaveUsuario;
end;

procedure TBaseArbolShell._A_ServidorExecute(Sender: TObject);
begin
     CambiaServidor;
end;

procedure TBaseArbolShell._A_CacheExecute(Sender: TObject);
begin
     ZBaseDlgModal.ShowDlgModal( ActivarCache, TActivarCache );
end;

procedure TBaseArbolShell._A_SalirSistemaExecute(Sender: TObject);
begin
     Close;
end;

procedure TBaseArbolShell._E_AgregarExecute(Sender: TObject);
begin
     FormaActiva.DoInsert;
end;

procedure TBaseArbolShell._E_BorrarExecute(Sender: TObject);
begin
     FormaActiva.DoDelete;
end;

procedure TBaseArbolShell._E_ModificarExecute(Sender: TObject);
begin
     FormaActiva.DoEdit;
end;

procedure TBaseArbolShell._E_RefrescarExecute(Sender: TObject);
begin
     if HayFormaAbierta then
        FormaActiva.DoRefresh;
end;

procedure TBaseArbolShell._E_BuscarCodigoExecute(Sender: TObject);
begin
     FormaActiva.DoLookup;
end;

procedure TBaseArbolShell._V_CerrarExecute(Sender: TObject);
begin
     CierraFormaActiva;
end;

procedure TBaseArbolShell._V_CerrarTodasExecute(Sender: TObject);
begin
     CierraFormaTodas;
end;

procedure TBaseArbolShell._V_MostrarArbolExecute(Sender: TObject);
begin
     ShowArbol( not PanelArbol.Visible );
end;

procedure TBaseArbolShell._V_ExpanderArbolExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        Arbol.FullExpand;
     finally
        Screen.Cursor := oCursor;
     end;
end;

procedure TBaseArbolShell._V_CompactarArbolExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        Arbol.FullCollapse;
     finally
        Screen.Cursor := oCursor;
     end;
end;

procedure TBaseArbolShell._V_BuscarFormaExecute(Sender: TObject);
var
    lEncontro: Boolean;
begin
     inherited;
     if (( ActiveControl = Arbol ) or ( ActiveControl = EditBusca ) ) and StrLleno( EditBusca.Text ) then
        lEncontro := ZArbolTools.BuscaNodoTexto( Arbol, 'Explorador de Formas', EditBusca.Text )
     else
         lEncontro := FALSE;
     if not lEncontro and PanelArbol.Visible then
     begin
          with EditBusca do
          begin
               SetFocus;
               SelectAll;
          end;
     end;
end;

procedure TBaseArbolShell._V_CalculadoraExecute(Sender: TObject);
begin
     ExecuteFile( 'CALC.EXE', '', 'WINDOWS', SW_SHOWDEFAULT );
end;

procedure TBaseArbolShell._H_ContenidoExecute(Sender: TObject);
begin
     Application.HelpCommand( HELP_FINDER, 0 );
end;

procedure TBaseArbolShell._H_GlosarioExecute(Sender: TObject);
var
   sHelpFile: String;
begin
     if strLleno( FHelpGlosario ) then  // En el create poner FHelpGlosario indicando el Archivo de Ayuda y Secci�n al cual apuntar
        with Application do
        begin
             sHelpFile := HelpFile;
             try
                HelpFile := FHelpGlosario; //'Tress.hlp>Main';
                HelpContext ( AyudaGlosario.HelpContext );
             finally
                HelpFile := sHelpFile;
             end;
        end;

end;

procedure TBaseArbolShell._H_UsoTecladoExecute(Sender: TObject);
begin
     Application.HelpContext( AyudaCombinacionesTeclas.HelpContext );
end;

procedure TBaseArbolShell._H_ComoUsarAyudaExecute(Sender: TObject);
begin
     Application.HelpContext( AyudaUsandoAyuda.HelpContext );
end;

procedure TBaseArbolShell._H_NuevoTressExecute(Sender: TObject);
const
     K_HELP_CAMBIOS = 'Version.chm';
var
   sHelpFile: String;
begin
     with Application do
     begin
          sHelpFile := HelpFile;
          try
             HelpFile := K_HELP_CAMBIOS;
             HelpCommand( HELP_FINDER, 0 );
          finally
             HelpFile := sHelpFile;
          end;
     end;
end;

procedure TBaseArbolShell._H_AcercaDeExecute(Sender: TObject);
begin
     with TZAcercaDe.Create( Self ) do
     begin
          try
             ShowModal;
          finally
             Free;
          end;
     end;
end;

procedure TBaseArbolShell.ArbolDblClick(Sender: TObject);
var
   oNodo: TTreeNode;
   TipoForma: eFormaConsulta;
begin
     oNodo := Arbol.Selected;
     if ( oNodo <> nil ) then
     begin
          if EsForma( oNodo, TipoForma ) then
             AbreFormaConsulta( TipoForma )
          else if ( Sender = nil ) then  { Es Folder. Si Viene de un Enter, Abrirlo }
             oNodo.Expand( False );
     end;
end;

procedure TBaseArbolShell.ArbolKeyPress(Sender: TObject; var Key: Char);
begin
     case Key of
          Chr( VK_RETURN ):
          begin
               Key := #0;
               ArbolDblClick( NIL );
          end;
     end;
     inherited;
end;

procedure TBaseArbolShell.editBuscaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     if ( Key = 13 ) then
     begin
          Key := 0;
          _V_BuscarFormaExecute(Sender);
     end;
     inherited;
end;

procedure TBaseArbolShell.ClientAreaPGChange(Sender: TObject);
begin
     with ClientAreaPG do
     begin
          FFormManager.CambiaFormaActiva( TTabForm( ActivePage ).Forma );
          SetFocus;
     end;
end;

procedure TBaseArbolShell.FormaActivaFocus(Sender: TObject);
begin
     if HayFormaAbierta then
        Windows.SendMessage( FormaActiva.Handle, WM_FOCUS, 0, 0 )
end;

procedure TBaseArbolShell.FormaActivaDefocus(Sender: TObject);
begin
     if HayFormaAbierta then
        Windows.SendMessage( FormaActiva.Handle, WM_DEFOCUS, 0, 0 )
end;

procedure TBaseArbolShell.CargaDerechos;
begin
     inherited CargaDerechos;
     {$ifdef TRESS}
     {$ifndef DOS_CAPAS}
     ResetDerecho( D_CONS_REPORTES , K_SIN_RESTRICCION );
     {$endif}
    {$endif}
end;

end.
