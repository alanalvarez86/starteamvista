unit ZetaBorradorSUA;
{$define CHANGED}
(* ===========================================================================
 * TbDBF.dcu - TTbDBF : A custom data set which uses a dBase III+ file
 * v 1.10              for single user access only.
 *                     Without index support yet.
 *  version            Freeware for non-commercial use only!
 *  Delphi 4.0         (for commercial use has a fee of USD 15.- only)
 *
 * Author:  Horacio Jamilis
 * E-Mail:  terabyte@iname.com
 * WWW:     http://members.xoom.com/t_byte/
 *  or      http://www.geocities.com/SiliconValley/Sector/2369/
 * Copyright (C) 1998, Terabyte Software
 *
 * ===========================================================================
 * I have many useful components writen. TTbDBF is just one of them.
 * The others are...
 *  - TbPrint ... a Printing and reporting component set that makes VERY easy
 *                and VERY fast to make database reports (less than a minute).
 *                It can print all you want (but only text, no graphics yet), in
 *                Windows (using a printer driver) or Fast modes (writing directly
 *                to the printer port, using the device features).
 *                So, prints very fast.
 *  - TbLang  ... a MultiLanguage system. With it you could make your programs
 *                multilingual VERY easy. You could edit all the text strings while
 *                you are programing (in the Delphi IDE) or later with a stand alone
 *                aplication. It supports UserStrings (used in the code for messages)
 *                and Dialogs (MessageDlg, InputBox and InputQuery) with translated
 *                caption and buttons. You could use all the languages you wish.
 *                You could save the translations in the EXE or in a Flat File.
 *  - LinkEdit .. Just an TEdit with an elipsis button in it's right side only when
 *                the edit is active (with cursor), and alignment options.
 * Enjoy Them.
 * ===========================================================================
 * What comes:
 *  - MultiUser concurrent access support
 *  - Index files support (probably dbase iii+ NDX and clipper NTX)
 * ===========================================================================
 * v 1.10
 * - Fixed the table creation method (now compatible with Excel!)
 * - Fixed the pack method
 * - Modified the GetFieldData and SetFieldData methods to avoid garbage.
 * ===========================================================================
 * v 1.00
 * - Added memo fields support
 * - Changed Records to Packed Records in the Table Fields Definition
 * ===========================================================================
 * v 0.97
 * - Added online help
 * ===========================================================================
 * v 0.96
 * - Added SpecialOffset property
 * - Added Transliterate property
 * ===========================================================================
 * v 0.95
 * - Just clean the code for release
 * - New version for Delphi 4 in the release
 * ===========================================================================
 * v 0.94 (22/7/98)
 * - Fixed field deleting: when deleting the field contents, this was not saved
 *   in the previous version.
 * - Caption for main window of the demo corrected to show the new component's name
 *   (TTbDBF, not TDbf).
 * ===========================================================================
 * v 0.93 (16/7/98)
 * - Fixed filtering: it was showing one record when no records match the filter.
 *         now supports compare one field and a constant
 *         (character, numeric, date or logic).
 * - Removed OnFilterRecord event - not implemented yet.
 * - Fixed deleting: it was showing one record when all records was deleted.
 * - Added CreateTable            - Now writed
 * - Added PackTable              - Now writed
 * - Component renamed from TDbf to TTbDbf (to include my prefix standard)
 * ===========================================================================
 * v 0.92
 * - Clean the code
 * - Removed SortTable            - Never implemented
 * - Removed UnSortTable          - Never implemented
 * - Removed _CompareRecords      - Never used
 * - Removed _SwapRecords         - Never used
 * - Removed CreateTable          - Never really implemented
 * - Removed PackTable            - Never really implemented
 * ===========================================================================
 * v 0.91
 * - Fixed error on deleting records
 * - Added filtering capabilities (work wrong when there are no records within
 *   the filter expresion - Only support expresion with one field like
 *   "NUMFIELD>10" or "TEXTFIELD<='TEST'" or "DATEFIELD=19980626"
 *   (in yyyymmdd format))
 *   the OnFilterRecord event does not work yet.
 * ===========================================================================
 * Especial thanks to Michael Beauregard (Michael_Beauregard@mck.com)
 * for C++ Builder support!
 * Thanks too to all the people that try this component,
 * and special thanks to the people that writes to me,
 * to report bugs, to comment or just for saying hello to me.
 * Again, my email address is jhoracio@cvtci.com.ar
 * ===========================================================================
 *)

interface

uses Dialogs, Windows, Forms, Controls, IniFiles,
{$ifndef VER125}
     DbTables,
{$endif}
     SysUtils, Classes, DB;

type
  EDBFError = class (Exception);

  pDateTime = ^TDateTime;
  pBoolean = ^WordBool;
  pInteger = ^Integer;

  PRecInfo = ^TRecInfo;
  TRecInfo = record
    Bookmark: Longint;
    BookmarkFlag: TBookmarkFlag;
  end;

  TTbDBFHeader = record  { Dbase III + header definition        }
     VersionNumber    :byte;  { version number (03h or 83h ) }
     LastUpdateYear   :byte;  { last update YY MM DD         }
     LastUpdateMonth  :byte;
     LastUpdateDay    :byte;
     NumberOfRecords  :longint; { number of record in database }
     BytesInHeader    :smallint;{ number of bytes in header }
     BytesInRecords   :smallint;{ number of bytes in records }
     ReservedInHeader :array[1..20] of char;   { reserved bytes in header }
  end;

  PDbfField = ^TTbDBFField;
  TTbDBFField = packed record
     FieldName   :array[1..11] of char; { Name of this record             }
     FieldType   :char;           { type of record - C,N,D,L,etc.         }
     fld_addr    :longint;        { not used }
     Width       :byte;           { total field width of this record      }
     Decimals    :byte;           { number of digits to right of decimal  }
     MultiUser   :smallint;       { reserved for multi user }
     WorkAreaID  :byte;           { Work area ID }
     MUser       :smallint;       { reserved for multi_user }
     SetFields   :byte;           { SET_FIELDS flag }
     Reserved    :array[1..8] of byte;      { 8 bytes reserved }
  end;                           { record starts                         }

  pRecordHeader = ^tRecordHeader;
  tRecordHeader = record
    DeletedFlag : char;
  end;

  TTbMemoBlock = Record
    NextBlock    : integer;
    BlockSize    : integer;
    Data         : array[1..504] of Char;
  End;

  TTbDBF = class(TDataSet)
  protected
    FTableStream: TStream; // the physical table
    FTableName: string; // table path and file name
    fDBFHeader : TTbDBFHeader;
    // record data
    fRecordHeaderSize : Integer;   // The size of the record header
    FRecordCount,                  // current number of record
    FRecordSize,                   // the size of the actual data
    FRecordBufferSize,             // data + housekeeping (TRecInfo)
    FRecordInfoOffset,             // offset of RecInfo in record buffer
    FCurrentRecord : integer;      // current record (0 to FRecordCount - 1)
    FIsTableOpen: Boolean;         // status
    FFileWidth,                    // field widths in record
    FFileDecimals,                 // field decimals in record
    FFileOffset: TList;            // field offsets in record
    fReadOnly : Boolean;           // Enhancements
    fStartData : Integer;          // Position in file where data starts
    fActiveFilter : Boolean;       // If the filtering is active
    FSpecialOffset : integer;
    FTransliterate : boolean;
    FEof : boolean;
    FBof : boolean;
    FBlobCount:    Integer;
    FBlobCacheOfs: Word;
    FCacheBlobs: boolean;
    FMemoStream: TFileStream; // the file for memo fields
    function FFieldType(F : char):TFieldType;
    function FFieldSize(FType:char;FWidth:integer):integer;
  protected
    // TDataSet virtual abstract method
    function AllocRecordBuffer: PChar; override;
    procedure FreeRecordBuffer(var Buffer: PChar); override;
    procedure GetBookmarkData(Buffer: PChar; Data: Pointer); override;
    function GetBookmarkFlag(Buffer: PChar): TBookmarkFlag; override;
    function GetRecord(Buffer: PChar; GetMode: TGetMode; DoCheck: Boolean): TGetResult; override;
    function GetRecordSize: Word; override;
    procedure InternalAddRecord(Buffer: Pointer; Append: Boolean); override;
    procedure InternalClose; override;
    procedure InternalDelete; override;
    procedure InternalFirst; override;
    procedure InternalGotoBookmark(Bookmark: Pointer); override;
    procedure InternalHandleException; override;
    procedure InternalInitFieldDefs; override;
    procedure InternalInitRecord(Buffer: PChar); override;
    procedure InternalLast; override;
    procedure InternalOpen; override;
    procedure InternalPost; override;
    procedure InternalSetToRecord(Buffer: PChar); override;
    function IsCursorOpen: Boolean; override;
    procedure SetBookmarkFlag(Buffer: PChar; Value: TBookmarkFlag); override;
    procedure SetBookmarkData(Buffer: PChar; Data: Pointer); override;
    procedure SetFieldData(Field: TField; Buffer: Pointer); override;
    // TDataSet virtual method (optional)
    function GetRecordCount: Integer; override;
    procedure SetRecNo(Value: Integer); override;
    function GetRecNo: Integer; override;
    Procedure WriteHeader;
    // TDataSet - Filter
    function FindRecord(Restart, GoForward: Boolean): Boolean; override;
  private
    Procedure _ReadRecord(Buffer:PChar;IntRecNum:Integer);
    Procedure _WriteRecord(Buffer:PChar;IntRecNum:Integer);
    Procedure _AppendRecord(Buffer:PChar);
    Function _ProcessFilter(Buffer:PChar):boolean;
    function IsEof: Boolean;
    function IsBof: Boolean;
  public
    constructor Create(AOwner:tComponent); override;
    procedure CreateTable(TName : string; FieldDfs:TList);
    procedure PackTable;
    property Bof: Boolean read IsBof;
    property Eof: Boolean read IsEof;
    procedure First;
    procedure Last;
    function CreateBlobStream(Field: TField; Mode: TBlobStreamMode): TStream; override;
    function GetFieldData(Field: TField; Buffer: Pointer): Boolean; override;
  published
    property TableName: string read FTableName write FTableName;
    property ReadOnly : Boolean read fReadOnly write fReadonly default False;
  {$ifndef VER125}
    property DBFHeader : TTbDBFHeader read fDBFHeader;
  {$endif}
    property SpecialOffset : integer read FSpecialOffset write FSpecialOffset default 0;
    property Transliterate : boolean read FTransliterate write FTransliterate default True;
    // redeclared data set properties
    property Active;
    property Filter;
    property Filtered;
    property BeforeOpen;
    property AfterOpen;
    property BeforeClose;
    property AfterClose;
    property BeforeInsert;
    property AfterInsert;
    property BeforeEdit;
    property AfterEdit;
    property BeforePost;
    property AfterPost;
    property BeforeCancel;
    property AfterCancel;
    property BeforeDelete;
    property AfterDelete;
    property BeforeScroll;
    property AfterScroll;
    property OnCalcFields;
    property OnDeleteError;
    property OnEditError;
//    property OnFilterRecord;
    property OnNewRecord;
    property OnPostError;
  end;

  TTbBlobDataArray = array[0..0] of Pointer;
  PTbBlobDataArray = ^TTbBlobDataArray;

  TTbBlobStream = class(TStream)
  private
    FField: TBlobField;
    FDataSet: TTbDBF;
    FMode: TBlobStreamMode;
    FFieldNo: Integer;
    FPosition: Longint;
    FRecord : PChar;
  public
    constructor Create(Field: TBlobField; Mode: TBlobStreamMode);
    function Read(var Buffer; Count: Longint): Longint; override;
    function Write(const Buffer; Count: Longint): Longint; override;
    function Seek(Offset: Longint; Origin: Word): Longint; override;
  end;
  TBorradorSUA = class( TObject )
  private
    { Private declarations }
    FEmpleados: Boolean;
    FMovimientos: Boolean;
    FPatron: String;
    FPath: String;
    FTodos: Boolean;
    FDBF: TTbDBF;
    function NextToken(var sTarget: String): String;
    procedure SetPath( const Value: String );
    procedure BorraTabla(const sTabla, sIndices, sCampoPatron: String);
    procedure SetDBF(const sTabla: String);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent);
    destructor Destroy; override;
    property Empleados: Boolean read FEmpleados write FEmpleados;
    property Movimientos: Boolean read FMovimientos write FMovimientos;
    property Patron: String read FPatron write FPatron;
    property Path: String read FPath write SetPath;
    procedure Borrar;
  end;

implementation

uses ZetaCommonTools;

Const
  dfhVersionNumber = 13;
  dB3     = $03;
  dB4     = $04;
  dB5     = $05;
  dB3Memo = $83;
  dB4Memo = $7B;
  dB5Memo = $8B;

TYPE
  PBufArray = ^BufArray;
  BufArray = Array[0..0] of Char;

// ****************************************************************************
// Low Level Routines for accessing an internal record

// ____________________________________________________________________________
// TTbDBF._ReadRecord
Procedure TTbDBF._ReadRecord(Buffer:PChar;IntRecNum:Integer);
  {-Read a record based on the internal record number (absolute)}
var
  i,j,D : integer;
  Memo : PChar;
  S,S2: string;
  Flag : boolean;
  MemoBuff, MemoTranslated : array [0..511] of char;
  Longitud, L, T : integer;
BEGIN
  FTableStream.Position := FStartData + (FRecordSize * IntRecNum);
 try
  FTableStream.ReadBuffer(Buffer^, FRecordSize);
  // set the memo fields values
  for i := 0 to FieldDefs.Count-1 do
    begin // read the memo contents for each memo field
      if Fields[i] is TMemoField then
        begin
          S := '';
          for j := Integer(FFileOffset[i])+FRecordHeaderSize to Integer(FFileOffset[i])+9+FRecordHeaderSize do
            S := S+Buffer[j];
          S := Trim(S);
          if S<>'' then
            begin
              try
                D := StrToIntDef(S,0);
                if D > 0 then
                  begin
                    S := '';
                    Flag := False;
                    FMemoStream.Seek(Trunc(D*512), soFromBeginning);
                    while not Flag do
                      begin
                        if FMemoStream.Read(MemoBuff,512) < 512 then
                          Flag := True;
                        if fDBFHeader.VersionNumber = dB5Memo then
                          begin
                            if FTransliterate then
                              OemToCharBuff(MemoBuff+8,MemoTranslated,504)
                            else
                              CopyMemory(@MemoTranslated, @MemoBuff[7],504);
                          end
                        else
                          begin
                            if FTransliterate then
                              OemToCharBuff(MemoBuff,MemoTranslated,512)
                            else
                              CopyMemory(@MemoTranslated,@MemoBuff,512);
                          end;
                        if Pos(#26+#26, MemoTranslated) > 0 then
                          begin
                            S := S + Copy(MemoTranslated, 1, Pos(#26+#26, MemoBuff) - 1);
                            Flag := True;
                          end
                        else if Pos(#26, MemoTranslated) > 0 then
                          S := S + Copy(MemoTranslated, 1, Pos(#26, MemoBuff) - 1)
                        else
                          begin
                            Longitud := PInteger(@MemoBuff[4])^-8;
                            if Longitud<512 then L := Longitud
                            else L := 504;
                            S2 := MemoTranslated;
                            SetLength(S2,L);
                            S := S + S2;
                            T := Longitud;
                            Longitud := Longitud - L;
                            While (Longitud > 0) and not Flag do
                              begin
                                if FMemoStream.Read(MemoBuff,512) < 512 then
                                  Flag := True;
                                if FTransliterate then
                                  OemToCharBuff(MemoBuff,MemoTranslated,512)
                                else
                                  CopyMemory(@MemoTranslated,@MemoBuff,512);
                                if Longitud<512 then L := Longitud
                                else L := 512;
                                S2 := MemoTranslated;
                                SetLength(S2,L);
                                S := S + S2;
                                Longitud := Longitud - L;
                              end;
                            Flag := True;
                            SetLength(S,T);
                          end;
                      end;
                    while Pos(#141, S) > 0 do
                      S[Pos(#141, S)] := #13;
                    while pos(#236,S)>0 do
                      S := Copy(S,1,Pos(#236,S)-1)+Copy(S,Pos(#236,S)+1,Length(S)-Pos(#236,S));
                    while (pos(#10,S)>0)and(pos(#10,S)<>(pos(#13+#10,S)+1)) do
                      S := Copy(S,1,Pos(#10,S)-1)+Copy(S,Pos(#10,S)+1,Length(S)-Pos(#10,S));
                  end;
              finally
              end;
            end;
          Memo:=StrAlloc(Length(S)+1);
          StrCopy(Memo,PChar(S));
          PTbBlobDataArray(Buffer + FBlobCacheOfs)[Integer(FFileDecimals[i])*2] := Memo;
          StrCopy(Memo,PChar(S));
          Memo:=StrAlloc(Length(S)+1);
          StrCopy(Memo,PChar(S));
          PTbBlobDataArray(Buffer + FBlobCacheOfs)[Integer(FFileDecimals[i])*2+1] := Memo;
        end;
    end;
 except
 end;
END;

// ____________________________________________________________________________
// TTbDBF._WriteRecord
Procedure TTbDBF._WriteRecord(Buffer:PChar;IntRecNum:Integer);
  {-Write a record based on the internal record number (absolute)}
const
  ChrEof : char = #26;
var
  Block: String;
  i,j : integer;
  MemoHeader : TTbMemoBlock;
  Memo, MemoOriginal : PChar;
  Longitud : integer;
BEGIN
  for i := 0 to FieldDefs.Count-1 do
    begin // read the memo contents for each memo field
      if Fields[i] is TMemoField then
        begin
          Memo := PTbBlobDataArray(Buffer + FBlobCacheOfs)[Integer(FFileDecimals[i])*2];
          MemoOriginal := PTbBlobDataArray(Buffer + FBlobCacheOfs)[Integer(FFileDecimals[i])*2+1];
          if StrComp(Memo,MemoOriginal)<>0 then
            begin
              FMemoStream.Seek(0,soFromBeginning);
              FMemoStream.Read(MemoHeader,SizeOf(MemoHeader));
              Block := IntToStr(MemoHeader.NextBlock);
              Block := TrimLeft(TrimRight(Block));
              While Length(Block)<10 do
                Block := ' '+Block;
              for j := 0 to 9 do
                Buffer[Integer(FFileOffset[i])+j+FRecordHeaderSize] := Block[j+1];
              FMemoStream.Seek(0,soFromEnd);
              Block := Chr(26) + Chr(26);
              FMemoStream.Write(Block,2); // save the new end of file
              FMemoStream.Seek(MemoHeader.NextBlock * 512, soFromBeginning);
              Longitud := StrLen(Memo);
              Block :=Memo;
              if fDBFHeader.VersionNumber <> dB5Memo then
                While Length(Block)mod 512 <> 0 do
                  Block := Block+' ';
              if FTransliterate then
                CharToOemBuff(pchar(Block),pchar(Block),Length(Block));
              if fDBFHeader.VersionNumber = dB5Memo then
                MemoHeader.NextBlock := MemoHeader.NextBlock + (Length(Block)div 512) + 1
              else
                begin
                  if Length(Block) mod 512 = 0 then
                    MemoHeader.NextBlock := MemoHeader.NextBlock + (Length(Block)div 512)
                  else 
                    MemoHeader.NextBlock := MemoHeader.NextBlock + (Length(Block)div 512) + 1;
                end;
              if fDBFHeader.VersionNumber = dB5Memo then
                begin
                  j := 589823;
                  FMemoStream.Write(j,SizeOf(j));
                  j := Longitud+8;
                  FMemoStream.Write(j,SizeOf(j));
                  if Longitud>504 then j := 504
                  else j := Longitud;
                  Longitud := Longitud - j;
                  FMemoStream.Write(Block[1],j);
                  if Length(Block)>504 then
                    Block := Copy(Block,505,Length(Block)-504)
                  else
                    Block := '';
                  While Length(Block)>0 do
                    begin
                      if Longitud>512 then j := 512
                      else j := Longitud;
                      Longitud := Longitud - j;
                      FMemoStream.Write(Block[1],j);
                      if Length(Block)>512 then
                        Block := Copy(Block,513,Length(Block)-512)
                      else
                        Block := '';
                    end;
                end
              else
                FMemoStream.Write(Block,Length(Block));
              FMemoStream.Seek(0,soFromBeginning);
              FMemoStream.Write(MemoHeader,SizeOf(MemoHeader));
            end;
        end;
    end;
  FTableStream.Position := FStartData + (FRecordSize * IntRecNum);
  FTableStream.WriteBuffer (Buffer^, FRecordSize);
END;

// ____________________________________________________________________________
// TTbDBF._AppendRecord
Procedure TTbDBF._AppendRecord(Buffer:PChar);
const
  ChrEof : char = #26;
var
  Block: String;
  i,j : integer;
  MemoHeader : TTbMemoBlock;
  Memo, MemoOriginal : PChar;
  Longitud : integer;
BEGIN
  for i := 0 to FieldDefs.Count-1 do
    begin // read the memo contents for each memo field
      if Fields[i] is TMemoField then
        begin
          Memo := PTbBlobDataArray(Buffer + FBlobCacheOfs)[Integer(FFileDecimals[i])*2];
          MemoOriginal := PTbBlobDataArray(Buffer + FBlobCacheOfs)[Integer(FFileDecimals[i])*2+1];
          if StrComp(Memo,MemoOriginal)<>0 then
            begin
              FMemoStream.Seek(0,soFromBeginning);
              FMemoStream.Read(MemoHeader,SizeOf(MemoHeader));
              Block := IntToStr(MemoHeader.NextBlock);
              Block := TrimLeft(TrimRight(Block));
              While Length(Block)<10 do
                Block := ' '+Block;
              for j := 0 to 9 do
                Buffer[Integer(FFileOffset[i])+j+FRecordHeaderSize] := Block[j+1];
              FMemoStream.Seek(0,soFromEnd);
              Block := Chr(26) + Chr(26);
              FMemoStream.Write(Block,2); // save the new end of file
              FMemoStream.Seek(MemoHeader.NextBlock * 512, soFromBeginning);
              Longitud := StrLen(Memo);
              Block :=Memo+#32+#32;
              if fDBFHeader.VersionNumber = dB5Memo then
                begin
                  Longitud := Longitud + 8;
                  While Length(Block)mod 504 <> 0 do
                    Block := Block+' ';
                end
              else
                While Length(Block)mod 512 <> 0 do
                  Block := Block+' ';
              if FTransliterate then
                CharToOemBuff(pchar(Block),pchar(Block),Length(Block));
              if fDBFHeader.VersionNumber = dB5Memo then
                begin
{                  if Length(Block) mod 504 = 0 then
                    MemoHeader.NextBlock := MemoHeader.NextBlock + (Length(Block)div 504)
                  else }
                    MemoHeader.NextBlock := MemoHeader.NextBlock + (Length(Block)div 512) + 1;
                end
              else
                begin
                  if Length(Block) mod 512 = 0 then
                    MemoHeader.NextBlock := MemoHeader.NextBlock + (Length(Block)div 512)
                  else
                    MemoHeader.NextBlock := MemoHeader.NextBlock + (Length(Block)div 512) + 1;
                end;
              if fDBFHeader.VersionNumber = dB5Memo then
                begin
                  While Length(Block)>0 do
                    begin
                      j := 589823;
                      FMemoStream.Write(j,SizeOf(j));
                      if Longitud>504 then j := 504
                      else j := Longitud;
                      Longitud := Longitud - j;
                      FMemoStream.Write(j,SizeOf(j));
                      FMemoStream.Write(Block[1],j);
                      if Length(Block)>504 then
                        Block := Copy(Block,505,Length(Block)-504)
                      else
                        Block := '';
                    end;
                end
              else
                FMemoStream.Write(Block,Length(Block));
              FMemoStream.Seek(0,soFromBeginning);
              FMemoStream.Write(MemoHeader,SizeOf(MemoHeader));
            end;
        end;
    end;
  FTableStream.Position := FStartData + (FRecordSize * (FRecordCount{+FDeletedCount}));
  FTableStream.WriteBuffer (Buffer^, FRecordSize);
END;

/////////////////////////////////////////////////
////// Part I:
////// Initialization, opening, and closing
/////////////////////////////////////////////////

// ____________________________________________________________________________
// TTbDBF.InternalOpen
// I: open the table/file
procedure TTbDBF.InternalOpen;
var
  Field : TField;
  i,j : integer;
  d : string;
  MemoFileName : string;
begin
  // check if the file exists
  if not FileExists (FTableName) then
    raise eDBFError.Create ('Open: Table file not found');
  // create a stream for the file
  FTableStream := TFileStream.Create (FTableName, fmOpenReadWrite + fmShareExclusive);
  FTableStream.ReadBuffer(fDBFHeader,SizeOf(TTbDBFHeader));
  // opens the memo file
  if (fDBFHeader.VersionNumber = dB3Memo) or (fDBFHeader.VersionNumber = dB4Memo) or
     (fDBFHeader.VersionNumber = dB5Memo) then
    begin // there are a memo file
      if Pos('.',FTableName)>0 then
        MemoFileName := Copy(FTableName,1,Pos('.',FTableName))+'DBT'
      else
        MemoFileName := FTableName+'.DBT';
      FMemoStream := TFileStream.Create(MemoFileName, fmOpenReadWrite + fmShareExclusive);
    end;
  // sets record position
  FCurrentRecord := -1;
  // set the bookmark size
  BookmarkSize := sizeOf (Integer);
  if not (assigned(FFileOffset)) then
    FFileOffset := TList.Create;
  if not (assigned(FFileWidth)) then
    FFileWidth := TList.Create;
  if not (assigned(FFileDecimals)) then
    FFileDecimals := TList.Create;
  // initialize the field definitions
  // (another virtual abstract method of TDataSet)
  InternalInitFieldDefs;
  // if there are no persistent field objects,
  // create the fields dynamically
  if DefaultFields then
    CreateFields;
  // connect the TField objects with the actual fields
  BindFields (True);
  FBlobCacheOfs := FRecordSize + CalcFieldsSize;
  FRecordInfoOffset := FBlobCacheOfs + FBlobCount * SizeOf(Pointer) * 2;
  FRecordBufferSize := FRecordInfoOffset + SizeOf(TRecInfo);
  for i := 0 to FieldCount-1 do
    begin
      Field := Fields[i];
      if (Field.DataType = ftFloat) and (Integer(FFileDecimals[i])>0) then
        begin
          d := '0.';
          for j := 1 to Integer(FFileDecimals[i]) do
            d := d + '0';
          (Field as TFloatField).DisplayFormat := d;
        end;
    end;
  // get the number of records and check size
  fRecordCount := fDBFHeader.NumberOfRecords;
  // everything OK: table is now open
  FIsTableOpen := True;
end;

// Returns the Type of the field
function TTbDBF.FFieldType(F : char):TFieldType;
begin
  if F = 'C' then
    FFieldType := ftString
  else if (F = 'N') or (F = 'F') then
    FFieldType := ftFloat
  else if F = 'L' then
    FFieldType := ftBoolean
  else if F = 'D' then
    FFieldType := ftDate
  else if F = 'M' then
    FFieldType := ftMemo
  else
    FFieldType := ftUnknown;
end;

function TTbDBF.FFieldSize(FType:char;FWidth:integer):integer;
var
  Res : integer;
begin
  if FType = 'C' then
    Res := FWidth
  else if (FType = 'N') or (FType = 'F') then
    Res := 0
  else if FType = 'L' then
    Res := 0
  else if FType = 'D' then
    Res := 0
  else if FType = 'M' then
    Res := 0
  else
    Res := 0;
  FFieldSize := Res;
end;

// ____________________________________________________________________________
// TTbDBF.InternalInitFieldDefs
// I: define the fields
procedure TTbDBF.InternalInitFieldDefs;
var
  Il : Integer;
  TmpFileOffset : Integer;
  NumberOfFields : integer;
  Fld : TTbDBFField;
  FldName : PChar;
{$IFDEF VER103}
  NewFieldDef : TFieldDef;
{$ENDIF}
begin
  FieldDefs.Clear;
  FTableStream.Seek(SizeOf(TTbDBFHeader),soFromBeginning);
  NumberOfFields := ((fDbfHeader.BytesInHeader-sizeof(TTbDbfHeader))div 32);
  if not (assigned(FFileOffset)) then
    FFileOffset := TList.Create;
  FFileOffset.Clear;
  if not (assigned(FFileWidth)) then
    FFileWidth := TList.Create;
  FFileWidth.Clear;
  if not (assigned(FFileDecimals)) then
    FFileDecimals := TList.Create;
  FFileDecimals.Clear;
  TmpFileOffset := 0;
  FBlobCount   := 0;
  if (NumberOfFields>0) then
    begin
      for Il:=0 to NumberOfFields-1 do
        begin
          FTableStream.Read(Fld,SizeOf(Fld));
          FFileOffset.Add(Pointer(TmpFileOffset));
          FFileWidth.Add(Pointer(Fld.Width));
          if Fld.FieldType = 'M' then
            begin
              FFileDecimals.Add(Pointer(FBlobCount));
              inc(FBlobCount);
            end
          else
            FFileDecimals.Add(Pointer(Fld.Decimals));
          GetMem(FldName,Length(Fld.FieldName)+1);
          CharToOem(PChar(@Fld.FieldName),FldName);
{$IFDEF VER103}
          NewFieldDef := TFieldDef.Create(FieldDefs);
          NewFieldDef.DataType := FFieldType(Fld.FieldType);
          NewFieldDef.Size := FFieldSize(Fld.FieldType,Fld.Width);
          NewFieldDef.Name := FldName;
          NewFieldDef.FieldNo := Il+1;
          NewFieldDef.Required := FALSE;
          FreeMem(FldName);
          Inc(tmpFileOffset,Fld.Width);
{$endif}
{$ifdef VER125}
          TFieldDef.Create(FieldDefs, FldName,FFieldType(Fld.FieldType),
                           FFieldSize(Fld.FieldType,Fld.Width),False,Il+1);
          FreeMem(FldName);
          Inc(tmpFileOffset,Fld.Width);
{$endif}
{$ifdef VER100}
          TFieldDef.Create(FieldDefs, FldName,FFieldType(Fld.FieldType),
                           FFieldSize(Fld.FieldType,Fld.Width),False,Il+1);
          FreeMem(FldName);
          Inc(tmpFileOffset,Fld.Width);
{$endif}
{$ifdef VER120}
          TFieldDef.Create(FieldDefs, FldName,FFieldType(Fld.FieldType),
                           FFieldSize(Fld.FieldType,Fld.Width),False,Il+1);
          FreeMem(FldName);
          Inc(tmpFileOffset,Fld.Width);
{$endif}
{$ifdef VER130}
          TFieldDef.Create(FieldDefs, FldName,FFieldType(Fld.FieldType),
                           FFieldSize(Fld.FieldType,Fld.Width),False,Il+1);
          FreeMem(FldName);
          Inc(tmpFileOffset,Fld.Width);
{$endif}
        end;
      fRecordSize := tmpFileOffset+FrecordHeaderSize;
      FStartData := FTableStream.Position+1;
    end;
end;

// ____________________________________________________________________________
// TTbDBF.InternalClose
// I: close the table/file
procedure TTbDBF.InternalClose;
begin
  // if required, save updated header
  if (fDBFHeader.NumberOfRecords <> fRecordCount) or
    (fDBFHeader.BytesInRecords = 0) then
    BEGIN
      fDBFHeader.BytesInRecords := fRecordSize;
      fDBFHeader.NumberOfRecords := fRecordCount;
      WriteHeader;
    END;
  // disconnet field objects
  BindFields(False);
  // destroy field object (if not persistent)
  if DefaultFields then
    DestroyFields;
  // free the internal list field offsets
  if Assigned(FFileOffset) then
    FFileOffset.Free;
  FFileOffset := nil;
  if Assigned(FFileWidth) then
    FFileWidth.Free;
  FFileWidth := nil;
  if Assigned(FFileDecimals) then
    FFileDecimals.Free;
  FFileDecimals := nil;
  FCurrentRecord := -1;
  // close the file
  if (fDBFHeader.VersionNumber = dB3Memo) or (fDBFHeader.VersionNumber = dB4Memo) or
     (fDBFHeader.VersionNumber = dB5Memo) then
    begin
      FMemoStream.Free;
      FMemoStream := nil;
    end;
  FIsTableOpen := False;
  FTableStream.Free;
  FTableStream := nil;
end;

// ____________________________________________________________________________
// TTbDBF.IsCursorOpen
// I: is table open
function TTbDBF.IsCursorOpen: Boolean;
begin
  Result := FIsTableOpen;
end;

// ____________________________________________________________________________
// TTbDBF.WriteHeader
procedure TTbDBF.WriteHeader;
begin
//  Assert(FTableStream<>nil,'FTableStream=Nil');
  if FTableStream <> nil then
    begin
      FTableStream.Seek(0,soFromBeginning);
      FTableStream.WriteBuffer(fDBFHeader,SizeOf(TTbDBFHeader));
    end;
end;

// ____________________________________________________________________________
// TTbDBF.Create
constructor TTbDBF.Create(AOwner:tComponent);
BEGIN
  inherited create(aOwner);
  fRecordHeaderSize := SizeOf(tRecordHeader);
  FEof := False;
  FBof := False;
  FCacheBlobs := True;
  FTransliterate := True;
  FSpecialOffset := 0;
END;

// ____________________________________________________________________________
// TTbDBF.CreateTable
// I: Create a new table/file
procedure TTbDBF.CreateTable(TName : string; FieldDfs:TList);
var
  Offs : Integer;
  Fld : PDbfField;
  i : integer;
  b : byte;
begin
  CheckInactive;
  //  InternalInitFieldDefs;
  // create the new file
  if FileExists (TName) and
    (MessageDlg ('File ' + TName +
      ' already exists. OK to override?',
      mtConfirmation, [mbYes, mbNo], 0) = mrNo) then
    Exit;
  FTableStream := TFileStream.Create (TName,
                 fmCreate or fmShareExclusive);
  try
    FillChar(fDBFHeader,SizeOf(TTbDBFHeader),0);
    fDBFHeader.VersionNumber := 3;
    fDBFHeader.BytesInRecords := 0; // filled later
    fDBFHeader.NumberOfRecords := 0; // empty
    WriteHeader;
    Offs:=0;
    for i:=0 to FieldDfs.Count-1 do
      begin
        Fld := FieldDfs[i];
        Inc(Offs,Fld^.Width);
        FTableStream.Write(Fld^,SizeOf(TTbDBFField));
      end;
    b:=$0D;
    FTableStream.Write(b,1);
    fStartData := FTableStream.Position;
    fDBFHeader.BytesInRecords := Offs;
    FRecordSize := Offs+FRecordHeaderSize;
    fDbfHeader.BytesInHeader := FTableStream.Position;
    WriteHeader;
  finally
    // close the file
    FTableStream.Free;
    FTableStream := nil;
  end;
end;

// ____________________________________________________________________________
// TTbDBF.PackTable
//Enhancement: Remove all deleted items from the table.
procedure TTbDBF.PackTable;
var
  NewStream : tStream;
  NumberOfFields : integer;
  NewTableName : string;
  i : integer;
  Fld : TTbDBFField;
  ActiveRecords : integer;
  Buffer : pchar;
  iRecordSize : integer;
  Error : boolean;
  c : char;
BEGIN
  Error := False;
  CheckInactive;
  if fTableName = '' then
    raise EDBFError.Create('Table name not specified.');
  if not FileExists (FTableName) then
    raise EDBFError.Create('Table '+fTableName+' does not exist.');
  FTableStream := TFileStream.Create (FTableName, fmOpenRead + fmShareExclusive);
  if Pos('.',FTableName)>0 then
    NewTableName := Copy(FTableName,1,Pos('.',FTableName))+'DBP'
  else
    NewTableName := FTableName+'.DBP';
  NewStream := TFileStream.Create (NewTableName, fmCreate or fmShareExclusive);
 try
  FTableStream.ReadBuffer(fDBFHeader,SizeOf(TTbDBFHeader));
  NewStream.WriteBuffer(fDBFHeader,SizeOf(TTbDBFHeader));
  NumberOfFields := ((fDbfHeader.BytesInHeader-sizeof(TTbDbfHeader))div 32);
  iRecordSize := SizeOf(tRecordHeader);
  for i := 1 to NumberOfFields do
    begin
      FTableStream.Read(Fld,SizeOf(Fld));
      NewStream.Write(Fld,SizeOf(Fld));
      Inc(iRecordSize,Fld.Width);
    end;
  FTableStream.Seek(1,soFromCurrent);
  c:= #0;
  NewStream.Write(c,1);
  ActiveRecords := 0;
  GetMem(Buffer,iRecordSize);
  while FTableStream.Position < (FTableStream.Size-2) do
    begin
      FTableStream.ReadBuffer(Buffer^,iRecordSize);
      if Buffer[0] =' ' then
        begin
          NewStream.WriteBuffer(Buffer^,iRecordSize);
          inc(ActiveRecords);
        end;
    end;
  c := #0;
  NewStream.Write(c,1);
  FreeMem(Buffer,iRecordSize);
  NewStream.Position := 0;
  fDbfHeader.NumberOfRecords := ActiveRecords;
  NewStream.WriteBuffer(fDBFHeader,SizeOf(TTbDBFHeader));
 except
  Error := True;
 end;
  FTableStream.Free;
  NewStream.Free;
  if Not Error then
    begin
      CopyFile(PChar(NewTableName),PChar(FTableName),False);
      DeleteFile(Pchar(NewTableName));
    end;
end;

////////////////////////////////////////
////// Part II:
////// Bookmarks management and movement
////////////////////////////////////////

// ____________________________________________________________________________
// TTbDBF.InternalGotoBookmark
// II: set the requested bookmark as current record
procedure TTbDBF.InternalGotoBookmark (Bookmark: Pointer);
var
  ReqBookmark: Integer;
begin
  ReqBookmark := PInteger (Bookmark)^;
  if (ReqBookmark >= 0) and (ReqBookmark < FRecordCount) then
    FCurrentRecord := ReqBookmark
  else
    raise eDBFError.Create ('Bookmark ' +
      IntToStr (ReqBookmark) + ' not found');
end;

// ____________________________________________________________________________
// TTbDBF.InternalSetToRecord
// II: same as above (but passes a buffer)
procedure TTbDBF.InternalSetToRecord (Buffer: PChar);
var
  ReqBookmark: Integer;
begin
  ReqBookmark := PRecInfo(Buffer + FRecordInfoOffset).Bookmark;
  InternalGotoBookmark (@ReqBookmark);
end;

// ____________________________________________________________________________
// TTbDBF.GetBookmarkFlag
// II: retrieve bookmarks flags from buffer
function TTbDBF.GetBookmarkFlag (
  Buffer: PChar): TBookmarkFlag;
begin
  Result := PRecInfo(Buffer + FRecordInfoOffset).BookmarkFlag;
end;

// ____________________________________________________________________________
// TTbDBF.SetBookmarkFlag
// II: change the bookmark flags in the buffer
procedure TTbDBF.SetBookmarkFlag (Buffer: PChar;
  Value: TBookmarkFlag);
begin
  PRecInfo(Buffer + FRecordInfoOffset).BookmarkFlag := Value;
end;

// ____________________________________________________________________________
// TTbDBF.GetBookmarkData
// II: read the bookmark data from record buffer
procedure TTbDBF.GetBookmarkData (
  Buffer: PChar; Data: Pointer);
begin
  PInteger(Data)^ :=
    PRecInfo(Buffer + FRecordInfoOffset).Bookmark;
end;

// ____________________________________________________________________________
// TTbDBF.SetBookmarkData
// II: set the bookmark data in the buffer
procedure TTbDBF.SetBookmarkData (
  Buffer: PChar; Data: Pointer);
begin
  PRecInfo(Buffer + FRecordInfoOffset).Bookmark :=
    PInteger(Data)^;
end;

// ____________________________________________________________________________
// TTbDBF.InternalFirst
// II: Go to a special position before the first record
procedure TTbDBF.InternalFirst;
begin
  FCurrentRecord := -1;
end;

// ____________________________________________________________________________
// TTbDBF.InternalLast
// II: Go to a special position after the last record
procedure TTbDBF.InternalLast;
begin
  FCurrentRecord := FRecordCount;
end;

// ____________________________________________________________________________
// TTbDBF.GetRecordCount
// II (optional): Record count
function TTbDBF.GetRecordCount: Longint;
begin
  CheckActive;
  Result := FRecordCount;
end;

// ____________________________________________________________________________
// TTbDBF.GetRecNo
// II (optional): Get the number of the current record
function TTbDBF.GetRecNo: Longint;
begin
  UpdateCursorPos;
  if FCurrentRecord < 0 then
    Result := 1
  else
    Result := FCurrentRecord + 1;
end;

// ____________________________________________________________________________
// TTbDBF.SetRecNo
// II (optional): Move to the given record number
procedure TTbDBF.SetRecNo(Value: Integer);
begin
  CheckBrowseMode;
  if (Value > 1) and (Value <= (FRecordCount)) then
  begin
    FCurrentRecord := Value - 1;
    Resync([]);
  end;
end;

//////////////////////////////////////////
////// Part III:
////// Record buffers and field management
//////////////////////////////////////////

// ____________________________________________________________________________
// TTbDBF.GetRecordSize
/// III: Determine the size of each record buffer in memory
function TTbDBF.GetRecordSize: Word;
begin
  Result := FRecordSize; // data only
end;

// ____________________________________________________________________________
// TTbDBF.AllocRecordBuffer
/// III: Allocate a buffer for the record
function TTbDBF.AllocRecordBuffer: PChar;
begin
  Result := StrAlloc(FRecordBufferSize+1);
{$ifdef VER100}
  if FBlobCount > 0 then
    Initialize(PTbBlobDataArray(Result + FBlobCacheOfs)[0], FBlobCount*2);
{$endif}
end;

// ____________________________________________________________________________
// TTbDBF.InternalInitRecord
// III: Initialize the record (set to zero)
procedure TTbDBF.InternalInitRecord(Buffer: PChar);
var
  i : integer;
  Memo : pchar;
begin
  FillChar(Buffer^, FRecordBufferSize, 32);
  if FBlobCount>0 then
  for i := 0 to FieldDefs.Count-1 do
    begin
      if Fields[i] is TMemoField then
        begin
          Memo := StrAlloc(1);
          Memo[0] := #0;
          PTbBlobDataArray(Buffer + FBlobCacheOfs)[Integer(FFileDecimals[i])*2] := Memo;
          Memo := StrAlloc(1);
          Memo[0] := #0;
          PTbBlobDataArray(Buffer + FBlobCacheOfs)[Integer(FFileDecimals[i])*2+1] := Memo;
        end;
    end;
end;

// ____________________________________________________________________________
// TTbDBF.FreeRecordBuffer
// III: Free the buffer
procedure TTbDBF.FreeRecordBuffer (var Buffer: PChar);
begin
{$ifdef VER100}
  if FBlobCount > 0 then
    Finalize(PTbBlobDataArray(Buffer + FBlobCacheOfs)[0], FBlobCount*2);
{$endif}
  StrDispose(Buffer);
end;

// TTbDBF.FindRecord
// III: Find a record that matches the Filter constraints
function TTbDBF.FindRecord(Restart, GoForward: Boolean): Boolean;
var
  Status : Boolean;
begin
  CheckBrowseMode;
  DoBeforeScroll;
  UpdateCursorPos;
  CursorPosChanged;
  try
    begin
      if GoForward then
      begin
        if Restart then First;
        if Filtered then fActiveFilter := True;
        Status := GetNextRecord;
      end else
      begin
       if Restart then Last;
       if Filtered then fActiveFilter := True;
       Status := GetPriorRecord;
      end;
    end;
  finally
    if Filtered then fActiveFilter := False;
  end;
  Result := Status;
  if Result then DoAfterScroll;
end;

// ____________________________________________________________________________
// TTbDBF.GetRecord
// III: Retrieve data for current, previous, or next record
// (eventually moving to it) and return the status
function TTbDBF.GetRecord(Buffer: PChar;
  GetMode: TGetMode; DoCheck: Boolean): TGetResult;
var
  Acceptable : Boolean;
begin
  result := grOk;
  if FRecordCount < 1 then
    Result := grEOF
  else
    repeat
      FEof := False;
      FBof := False;
      case GetMode of
        gmCurrent :
          begin
            if (FCurrentRecord >= FRecordCount) or
                (FCurrentRecord < 0) then
              Result := grError;
          end;
        gmNext :
          begin
            if (fCurrentRecord < (fRecordCount)-1) then
              begin
                Inc (FCurrentRecord);
                if fCurrentRecord=FRecordCount then
                  FEof := True;
              end
            else
              begin
                Result := grEOF;
                FEof := True;
              end;
          end;
        gmPrior :
          begin
           if (fCurrentRecord > 0) then
             begin
               Dec(fCurrentRecord);
               if fCurrentRecord=0 then
                 FBof := True;
             end
           else
             begin
               Result := grBOF;
               FBof := True;
             end;
          end;
      end;
      // fill record data area of buffer
      if Result = grOK then
        begin
          _ReadRecord(Buffer, fCurrentRecord );
          ClearCalcFields(Buffer);
          GetCalcFields(Buffer);
          with PRecInfo(Buffer + FRecordInfoOffset)^ do
            begin
              BookmarkFlag := bfCurrent;
              Bookmark := FCurrentRecord;
            end;
        end
      else
        if (Result = grError) and DoCheck then
          raise eDBFError.Create('GetRecord: Invalid record');
      Acceptable := pRecordHeader(Buffer)^.DeletedFlag <> '*';
      if Filtered or fActiveFilter then
        Acceptable := Acceptable and (_ProcessFilter(Buffer));
      if (GetMode=gmCurrent) and Not Acceptable then
        Result := grError;
    until (Result <> grOK) or Acceptable;
  if ((Result=grEOF)or(Result=grBOF)) and (Filtered or fActiveFilter) and not (_ProcessFilter(Buffer)) then
    Result := grError;
end;

// ____________________________________________________________________________
// TTbDBF.InternalPost
// III: Write the current data to the file
procedure TTbDBF.InternalPost;
begin
  CheckActive;
  if State = dsEdit then
    begin
      // replace data with new data
      if not fReadOnly then
        _WriteRecord (ActiveBuffer, fCurrentRecord);
    end
  else
    begin
      // always append
      InternalLast;
      pRecordHeader(ActiveBuffer)^.DeletedFlag := ' ';
      _AppendRecord(ActiveBuffer);
      Inc (FRecordCount);
    end;
end;

// ____________________________________________________________________________
// TTbDBF.InternalAddRecord
// III: Add the current data to the file
procedure TTbDBF.InternalAddRecord(Buffer:Pointer; Append:Boolean);
begin
  // always append
  InternalLast;
  // add record at the end of the file
  pRecordHeader(ActiveBuffer)^.DeletedFlag := ' ';
  _AppendRecord(ActiveBuffer);
  Inc (FRecordCount);
end;

// ____________________________________________________________________________
// TTbDBF.InternalDelete
// III: Delete the current record
procedure TTbDBF.InternalDelete;
begin
  if fReadOnly then Exit;
  CheckActive;
  PChar(ActiveBuffer)^ := '*';
  _WriteRecord(ActiveBuffer,fCurrentRecord);
  Resync([]);
end;

// ____________________________________________________________________________
// TTbDBF.GetFieldData
// III: Move data from record buffer to field
function TTbDBF.GetFieldData(Field:TField; Buffer:Pointer):Boolean;
var
  FieldOffset: Integer;
  S : string;
  Buf2 : PChar;
  i,l : integer;
  D : Double;
  n : integer;
  T : TDateTime;
  j : integer;
  OldDateFormat : string;
begin
  Result := False;
  Buf2 := ActiveBuffer;
  if (not IsEmpty)and(FRecordCount>0) and (Field.FieldNo > 0) and (Assigned(Buffer)) and (Assigned(Buf2))  then
    begin
      FieldOffset := Integer(FFileOffset[Field.FieldNo-1])+FRecordHeaderSize+FSpecialOffset;
      if Field.DataType = ftString then
        begin
          l := Integer(FFileWidth[Field.FieldNo-1]);
          S := '';
{$ifdef CHANGED}
          i := 1;
          While (Buf2[FieldOffset+i] <> #0) and (i<=l) do
{$else}
          i := 0;
          While (Buf2[FieldOffset+i] <> #0) and (i<l) do
{$endif}
            begin
              S := S+pChar(Buf2+FieldOffset+i)^;
              inc(i);
            end;
          S := Trim(S);
          s:=s+#0;
          if FTransliterate then
            CharToOemBuff(PChar(S), Buffer,l+1)
          else
            CopyMemory(Buffer,PChar(S),l+1);
          Result := True;
        end
      else if Field.DataType = ftFloat then
        begin
          n := Integer(FFileWidth[Field.FieldNo-1]);
          S := '';
          for i := FieldOffset to FieldOffset+n-1 do
            S := S+pChar(Buf2+i)^;
          S := Trim(S);
          if S='' then
            Result := False
          else
            begin
			  //(@am)PENDIENTE VALIDAR EL CAMBIO
              if (Pos('.',S) > 0) and ({$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}DecimalSeparator <> '.') then
                S[Pos('.',S)] := {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}DecimalSeparator;
              Result := True;
             try
              D := StrToFloat(S);
             except
              D := 0;
              Result := False;
             end;
              PDouble(Buffer)^ := D;
            end;
        end
      else if Field.DataType = ftDate then
        begin
          S := '';
          for j := 0 to 7 do
            S := S + pChar(Buf2+FieldOffset+j);
          SetLength(S,8);
          if (trim(S) = '') or (S='00000000') then
            Result := false
          else
            begin
			  //(@am)PENDIENTE VALIDAR EL CAMBIO
              S := Copy(S,7,2)+{$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}DateSeparator+Copy(S,5,2)+{$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}DateSeparator+Copy(S,1,4);
              OldDateFormat := {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}ShortDateFormat;
              {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}ShortDateFormat := 'dd/mm/yyyy';
              t := StrToDate(S);
              {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}ShortDateFormat := OldDateFormat; //(@am)PENDIENTE VALIDAR EL CAMBIO
              j := Trunc(pDouble(@t)^)+693594;
              pInteger(Buffer)^ := j;
              result := True;
            end;
        end
      else if Field.DataType = ftBoolean then
        begin
          Result := True;
          if PChar(Buf2+FieldOffset)^ in ['S','T','Y'] then
            pBoolean(Buffer)^ := True
//            pChar(Buffer) := 'T'
          else if PChar(Buf2+FieldOffset)^ in ['N','F'] then
            pBoolean(Buffer)^ := False
//            pChar(Buffer) := 'F'
          else
            Result := False;
        end
      else
        begin
          ShowMessage ('very bad error in get field data');
          Result := False;
        end;
    end;
end;

// ____________________________________________________________________________
// TTbDBF.SetFieldData
// III: Move data from field to record buffer
procedure TTbDBF.SetFieldData(Field: TField; Buffer: Pointer);
var
  FieldOffset: Integer;
  Buf2 : PChar;
  l,i,n:integer;
  S : string;
  D : TDateTime;
  j : integer;
begin
  Buf2 := ActiveBuffer;
  if (Field.FieldNo >= 0) and (Assigned(Buffer)) and (Assigned(Buf2)) then
    begin
      FieldOffset := Integer(FFileOffset[Field.FieldNo-1])+FRecordHeaderSize+FSpecialOffset;
      if Field.DataType = ftString then
        begin
          l := Integer(FFileWidth[Field.FieldNo-1]);
          S := '';
          i := 0;
          SetLength(S,l);
          S:=StringOfChar(#32,l);
          While (PChar(Buffer)[i] <> #0) and (i<l) do
            begin
              S[i+1] := PChar(Buffer)[i];
              inc(i);
            end;
          S[i+1] := #0;
          if FTransliterate then
            OemToCharBuff(PChar(S),PChar(Buf2+FieldOffset),l)
          else
            CopyMemory(PChar(Buf2+FieldOffset),PChar(S),l);
        end
      else if Field.DataType = ftFloat then
        begin
          n := Integer(FFileWidth[Field.FieldNo-1]);
          Str(pDouble(Buffer)^:n:Integer(FFileDecimals[Field.FieldNo-1]),S);
          while Length(S)<n do
            S :=  ' '+S;
          if (Pos({$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}DecimalSeparator,S) > 0) and ({$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}DecimalSeparator <> '.') then
            S[Pos({$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}DecimalSeparator,S)] := '.';
          CopyMemory(Pchar(Buf2+FieldOffset),PChar(S),n);
        end
      else if Field.DataType = ftDate then
        begin
          j := pInteger(Buffer)^-693594;
          pDouble(@d)^ := j;
          S := FormatDateTime('yyyymmdd',d);
          CopyMemory(Pchar(Buf2+FieldOffset),PChar(S),8);
//          StrLCopy(pChar(Buf2+FieldOffset),pChar(S),8);
        end
      else if Field.DataType = ftBoolean then
        begin
          if pBoolean(Buffer)^ then
            PChar(Buf2+FieldOffset)^ := 'T'
          else
            PChar(Buf2+FieldOffset)^ := 'F'; 
        end
      else
        ShowMessage ('very bad error in setfield data');
      DataEvent (deFieldChange, Longint(Field));
    end
  else if (Field.FieldNo >= 0)and(not Assigned(Buffer))and(Assigned(Buf2)) then
    begin // field deleted
      FieldOffset := Integer(FFileOffset[Field.FieldNo-1])+FRecordHeaderSize;
      for i := 0 to Integer(FFileWidth[Field.FieldNo-1])-1 do
        PChar(Buf2+FieldOffset+i)^ := #32;
      PChar(Buf2+FieldOffset)^ := #0;
      SetModified(True);
    end;
end;

// ____________________________________________________________________________
// TTbDBF.InternalHandleException
// default exception handling
procedure TTbDBF.InternalHandleException;
begin
  // standard exception handling
  Application.HandleException(Self);
end;

Function TTbDBF._ProcessFilter(Buffer:PChar):boolean;
var
  FilterExpresion : string;
  PosComp : integer;
  FName : string;
  FieldPos : integer;
  FieldOffset : integer;
  FieldValue : Variant;
  TestValue : Variant;
  FieldText : string;
  OldShortDateFormat : string;
  Res : boolean;
begin
  FilterExpresion := Filter;
  PosComp := Pos('<',FilterExpresion);
  if PosComp=0 then
    PosComp := Pos('>',FilterExpresion);
  if PosComp=0 then
    PosComp := Pos('=',FilterExpresion);
  if PosComp=0 then
    begin
      _ProcessFilter := True;
      Exit;
    end;
  FName := Trim(Copy(FilterExpresion,1,PosComp-1));
  FieldPos := FieldDefs.IndexOf(FName);
  FieldOffset := integer(FFileOffset[FieldPos]);
  if FieldPos < 0 then
    Res := True
  else if FieldDefs.Items[FieldPos].DataType = ftString then
    begin // STRING
     try
      FieldValue := '';
      FieldOffset := FieldOffset+1;
      While (Buffer[FieldOffset]<>#0) and (Length(FieldValue)<integer(FFileWidth[FieldPos])) do
        begin
          FieldValue := FieldValue + Buffer[FieldOffset];
          FieldOffset := FieldOffset+1;
        end;
      FieldValue := Trim(FieldValue);
      TestValue := Trim(Copy(FilterExpresion,PosComp+2,Length(FilterExpresion)-PosComp-2));
      if FilterExpresion[PosComp]='=' then
        Res := (FieldValue=TestValue)
      else if FilterExpresion[PosComp]='>' then
        begin
          if FilterExpresion[PosComp+1]='=' then
            Res := (FieldValue>=Copy(TestValue,2,(Length(TestValue)-1)))
          else
            Res := (FieldValue>TestValue);
        end
      else if FilterExpresion[PosComp]='<' then
        begin
          if FilterExpresion[PosComp+1]='=' then
            Res := (FieldValue<=Copy(TestValue,2,(Length(TestValue)-1)))
          else if FilterExpresion[PosComp+1]='>' then
            Res := (FieldValue<>Copy(TestValue,2,(Length(TestValue)-1)))
          else
            Res := (FieldValue<TestValue);
        end
      else
        Res := False;
     except
       Res := False;
     end;
    end
  else if FieldDefs.Items[FieldPos].DataType = ftFloat then
    begin // FLOAT
     try
      FieldText := '';
      FieldOffset := FieldOffset+1;
      While (Buffer[FieldOffset]<>#0) and (Length(FieldText)<integer(FFileWidth[FieldPos])) do
        begin
          FieldText := FieldText + Buffer[FieldOffset];
          FieldOffset := FieldOffset+1;
        end;
      FieldText := Trim(FieldText);
      if Pos('.',FieldText)>0 then
        FieldText[Pos('.',FieldText)] := {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}DecimalSeparator;
      FieldValue := StrToFloat(FieldText);
      if (FilterExpresion[PosComp+1]='=') or
         (FilterExpresion[PosComp+1]='>') then
        FieldText := Trim(Copy(FilterExpresion,PosComp+2,Length(FilterExpresion)-PosComp-1))
      else
        FieldText := Trim(Copy(FilterExpresion,PosComp+1,Length(FilterExpresion)-PosComp));
      if Pos('.',FieldText)>0 then
        FieldText[Pos('.',FieldText)] := {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}DecimalSeparator;
      TestValue := StrToFloat(FieldText);
      if FilterExpresion[PosComp]='=' then
        Res := (FieldValue=TestValue)
      else if FilterExpresion[PosComp]='>'then
        begin
          if FilterExpresion[PosComp+1]='='then
            Res := (FieldValue>=TestValue)
          else
            Res := (FieldValue>TestValue);
        end
      else if FilterExpresion[PosComp]='<'then
        begin
          if FilterExpresion[PosComp+1]='='then
            Res := (FieldValue<=TestValue)
          else if FilterExpresion[PosComp+1]='>'then
            Res := (FieldValue<>TestValue)
          else
            Res := (FieldValue<TestValue);
        end
      else
        Res := False;
     except
      Res := False;
     end;
    end
  else if FieldDefs.Items[FieldPos].DataType = ftDate then
    begin // DATE
      OldShortDateFormat := {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}ShortDateFormat;
     try
      FieldText := '';
      FieldOffset := FieldOffset+1;
      While (Buffer[FieldOffset]<>#0) and (Length(FieldText)<integer(FFileWidth[FieldPos])) do
        begin
          FieldText := FieldText + Buffer[FieldOffset];
          FieldOffset := FieldOffset+1;
        end;
      FieldText := Trim(FieldText);
      FieldText := Copy(FieldText,1,4)+{$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}DateSeparator+Copy(FieldText,5,2)+{$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}DateSeparator+Copy(FieldText,7,2);
      {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}ShortDateFormat := 'yyyy/mm/dd';
      FieldValue := StrToDate(FieldText);
      if (FilterExpresion[PosComp+1]='=') or
         (FilterExpresion[PosComp+1]='>') then
        FieldText := Trim(Copy(FilterExpresion,PosComp+2,Length(FilterExpresion)-PosComp-1))
      else
        FieldText := Trim(Copy(FilterExpresion,PosComp+1,Length(FilterExpresion)-PosComp));
      FieldText := Copy(FieldText,1,4)+{$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}DateSeparator+Copy(FieldText,5,2)+{$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}DateSeparator+Copy(FieldText,7,2);
      TestValue := StrToDate(FieldText);
      if FilterExpresion[PosComp]='=' then
        begin
          Res := (FieldValue=TestValue);
        end
      else if FilterExpresion[PosComp]='>' then
        begin
          if FilterExpresion[PosComp+1]='='then
            Res := (FieldValue>=TestValue)
          else
            Res := (FieldValue>TestValue);
        end
      else if FilterExpresion[PosComp]='<' then
        begin
          if FilterExpresion[PosComp+1]='='then
            Res := (FieldValue<=TestValue)
          else if FilterExpresion[PosComp+1]='>'then
            Res := (FieldValue<>TestValue)
          else
            Res := (FieldValue<TestValue);
        end
      else
        Res := False;
     except
      Res := False;
     end;
      {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}ShortDateFormat := OldShortDateFormat;
    end
  else if FieldDefs.Items[FieldPos].DataType = ftBoolean then
    begin
     try
      if (Buffer[FieldOffset+1]='T') or (Buffer[FieldOffset+1]='t') or
         (Buffer[FieldOffset+1]='Y') or (Buffer[FieldOffset+1]='y') or
         (Buffer[FieldOffset+1]='S') or (Buffer[FieldOffset+1]='s') then
        FieldValue := True
      else
        FieldValue := False;
      FieldText := Trim(Copy(FilterExpresion,PosComp+1,Length(FilterExpresion)-PosComp));
      if FilterExpresion[PosComp]='=' then
        begin
          if (FieldText='T') or (FieldText='t') or
             (FieldText='Y') or (FieldText='y') or
             (FieldText='S') or (FieldText='s') then
            TestValue := True
          else
            TestValue := False;
          Res := (FieldValue=TestValue)
        end
      else if FilterExpresion[PosComp]='>' then
        begin
          if FilterExpresion[PosComp+1]='=' then
            begin
              if (FieldText[2]='T') or (FieldText[2]='t') or
                 (FieldText[2]='Y') or (FieldText[2]='y') or
                 (FieldText[2]='S') or (FieldText[2]='s') then
                TestValue := True
              else
                TestValue := False;
              Res := (FieldValue>=TestValue);
            end
          else
            begin
              if (FieldText[1]='T') or (FieldText[1]='t') or
                 (FieldText[1]='Y') or (FieldText[1]='y') or
                 (FieldText[1]='S') or (FieldText[1]='s') then
                TestValue := True
              else
                TestValue := False;
              Res := (FieldValue>TestValue);
            end;
        end
      else if FilterExpresion[PosComp]='<' then
        begin
          if FilterExpresion[PosComp+1]='=' then
            begin
              if (FieldText[2]='T') or (FieldText[2]='t') or
                 (FieldText[2]='Y') or (FieldText[2]='y') or
                 (FieldText[2]='S') or (FieldText[2]='s') then
                TestValue := True
              else
                TestValue := False;
              Res := (FieldValue<=TestValue);
            end
          else if FilterExpresion[PosComp+1]='>' then
            begin
              if (FieldText[2]='T') or (FieldText[2]='t') or
                 (FieldText[2]='Y') or (FieldText[2]='y') or
                 (FieldText[2]='S') or (FieldText[2]='s') then
                TestValue := True
              else
                TestValue := False;
              Res := (FieldValue<>TestValue);
            end
          else
            begin
              if (FieldText[1]='T') or (FieldText[1]='t') or
                 (FieldText[1]='Y') or (FieldText[1]='y') or
                 (FieldText[1]='S') or (FieldText[1]='s') then
                TestValue := True
              else
                TestValue := False;
              Res := (FieldValue<TestValue);
            end;
        end
      else
        Res := False;
     except
       Res := False;
     end;
    end
  else
    Res := False;
  _ProcessFilter := Res;
end;

function TTbDBF.IsBof : Boolean;
begin
  if IsEmpty then
    Result := True
  else
    Result := FBof;
end;

function TTbDBF.IsEof : Boolean;
begin
  if IsEmpty then
    Result := True
  else
    Result := FEof;
end;

procedure TTbDBF.First;
begin
  inherited First;
  refresh;
end;

procedure TTbDBF.Last;
begin
  inherited Last;
  refresh;
end;

{******************************************************************************}
{* Start of Memo Field Support                                                *}
{******************************************************************************}

function TTbDBF.CreateBlobStream(Field: TField; Mode: TBlobStreamMode): TStream;
begin
  Result := TTbBlobStream.Create(Field as TBlobField, Mode);
  TTbBlobStream(Result).FRecord := ActiveBuffer;
  TTbBlobStream(Result).FDataSet := self;
  TTbBlobStream(Result).FFieldNo := Integer(FFileDecimals[Field.Index]);
end;

constructor TTbBlobStream.Create(Field: TBlobField; Mode: TBlobStreamMode);
begin
  FField := Field;
  FMode := Mode;
  FPosition := 0;
end;

function TTbBlobStream.Read(var Buffer; Count: LongInt): LongInt;
var
  Memo : PChar;
begin
  Memo := PTbBlobDataArray(FRecord + FDataSet.FBlobCacheOfs)[FFieldNo*2];
  StrLCopy(@Buffer,Memo,Count);
  FPosition := FPosition+Count;
  Result:=Count;
end;

function TTbBlobStream.Write(const Buffer; Count: LongInt): LongInt;
var
  PC:PChar;
  Memo : PChar;
begin
  Memo := PTbBlobDataArray(FRecord + FDataSet.FBlobCacheOfs)[FFieldNo*2];
  StrDispose(Memo);
  PC:=StrAlloc(Count+1);
  StrLCopy(PC,@Buffer,Count);
  PTbBlobDataArray(FRecord + FDataSet.FBlobCacheOfs)[FFieldNo*2]:=PC;
  Result:=Count;
end;

function TTbBlobStream.Seek(Offset: LongInt; Origin: Word): LongInt;
var
  Memo : PChar;
begin
  Memo := PTbBlobDataArray(FRecord + FDataSet.FBlobCacheOfs)[FFieldNo*2];
  case Origin of
       soFromBeginning : FPosition:=0;
       soFromEnd       : FPosition:=StrLen(Memo);
  end;
  FPosition:=FPosition+Offset;
  Result:=FPosition;
end;

{ ************ TBorradorSUA ********** }

constructor TBorradorSUA.Create( AOwner: TComponent );
begin
     FDBF := TTbDBF.Create( AOwner );
end;

destructor TBorradorSUA.Destroy;
begin
     FDBF.Free;
     inherited Destroy;
end;

procedure TBorradorSUA.SetPath( const Value: String );
begin
     FPath := ZetaCommonTools.VerificaDir( Value );
end;

function TBorradorSUA.NextToken( var sTarget: String ): String;
var
   i: Integer;
begin
     i := Pos( ',', sTarget );
     if ( i <> 0 ) then
     begin
          Result := System.Copy( sTarget, 1, ( i - 1 ) );
          System.Delete( sTarget, 1, i );
     end
     else
     begin
          Result := sTarget;
          sTarget := '';
     end;
end;

procedure TBorradorSUA.SetDBF( const sTabla: String );
begin
     with FDBF do
     begin
          Active := False;
          {
          Filtered := False;
          }
          TableName := FPath + sTabla;
     end;
end;

procedure TBorradorSUA.BorraTabla( const sTabla, sIndices, sCampoPatron: String );
var
   sTarget, sFile: String;
begin
     SetDBF( sTabla );
     with FDBF do
     begin
          {
          if not FTodos then
          begin
               Filter := Format( '%s="%s"', [ sCampoPatron, FPatron ] );
               Filtered := True;
          end;
          }
          Active := True;
          if IsEmpty then
             Active := False
          else
          begin
               while not Eof do
               begin
                    if FTodos or ( FieldByName( sCampoPatron ).AsString = FPatron ) then
                       Delete
                    else
                        Next;
               end;
               Active := False;
               PackTable;
          end;
     end;
     sTarget := sIndices;
     while ZetaCommonTools.StrLleno( sTarget ) do
     begin
          sFile := FPath + NextToken( sTarget ) + '.NTX';
          if SysUtils.FileExists( sFile ) then
             SysUtils.DeleteFile( sFile );
     end;
end;

procedure TBorradorSUA.Borrar;
begin
     if FEmpleados or FMovimientos then
     begin
          if StrVacio( FPatron ) then
             FTodos := True
          else
          begin
               SetDBF( 'PATRON.DBF' );
               with FDBF do
               begin
                    Active := True;
                    FTodos := ( RecordCount <= 1 );
                    Active := False;
               end;
          end;
          if FEmpleados then
          begin
               BorraTabla( 'ASEGURA.DBF',
                           'ASG1,ASG2,ASG3,ASG4,ASG5,ASG6',
                           'REG_PATR' );
          end;
          if FMovimientos then
          begin
               BorraTabla( 'MOVTOS.DBF',
                           'MOV1,MOV2,MOV3,MOV4,MOV5,MOV6,MOV7,MOV8',
                           'REG_PATR' );
               BorraTabla( 'AUSENT.DBF',
                           'AUS1.NTX',
                           'REG_PAT' );
          end;
     end;
end;

end.
