Unit FGraficasBase;

interface
  {$INCLUDE TeeChartProVS.INC}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, {Forms,}
  Dialogs,Chart, DBChart, StdCtrls, Buttons,
  ExtCtrls, db, Series, ComCtrls, ToolWin, ImgList,
  DBClient,ZetaCommonLists, DBCtrls, Menus,
  TeeFunci,
  {$ifdef VER130}
  IEditCha,
  {$else}
  TeeEditCha,
  Variants,
  {$endif}
  TeeProcs, TeEngine,
  {$IFDEF TeeChartPro}
  TeeComma, TeeEdit,
  {$ENDIF}
  ZReportTools,
  ZetaClientDataSet, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, dxSkinsdxBarPainter, cxGraphics, dxBar, cxClasses,
  cxDropDownEdit, cxBarEditItem, cxTreeView, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, VclTee.TeeGDIPlus;


type eTipoGrafica = ( tgBarraVertical,tgBarraHorizontal,tgPie,tgLinea,tgArea,tgPuntos);

type
  //DevEx(@am): Guardar el indice y campo correspondiente para cada opcion del combo de Campos
//  TTagItemsCombo = class
//  private
//     ItemIndex: Integer;
//     ItemTag:Integer;
//  public
//     property CampoIndex: Integer read ItemIndex;
//     property CampoTag: Integer read ItemTag;
//
//     //Constructor
//     constructor Create (const ItemIndex: Integer; const TagItem:Integer );
//  end;
//
//  TTagItemsDesc = class
//  private
//     ItemIndex: Integer;
//     ItemTag:Integer;
//  public
//     property CampoIndex: Integer read ItemIndex;
//     property CampoTag: Integer read ItemTag;
//
//     //Constructor
//     constructor Create (const ItemIndex: Integer; const TagItem:Integer );
//  end;

  TGraficaBase_DevEx = class//class(TChart)
    ImageList: TImageList;
    dsDataset: TDataSource;
    Arbol: TcxTreeView;
    Splitter: TSplitter;
    cdsGrupos: TZetaClientDataSet;
    Panel1: TPanel;
    ScrollGrafica: TScrollBar;
    Chart: TDBChart;
    lbDemo: TLabel;
    LblNoGraficar: TLabel;
    BarraNavegacion_DevEx: TdxBarManager;
    BarraNavegacion_DevExBar: TdxBar;
    BImprimir_DevEx: TdxBarSubItem;
    BSeriesOpciones_DevEx: TdxBarSubItem;
    BOpciones_DevEx: TdxBarSubItem;
    BTipoGrafica_DevEx: TdxBarSubItem;
    BDescripcionOpciones_DevEx: TdxBarSubItem;
    BAcercar_DevEx: TdxBarButton;
    BAlejar_DevEx: TdxBarButton;
    meGraficaActual_DevEx: TdxBarButton;
    meTodas_DevEx: TdxBarButton;
    meGrupoActual_DevEx: TdxBarButton;
    meTodoslosGrupos_DevEx: TdxBarButton;
    op3D: TdxBarButton;
    NoMostrarEtiqueta: TdxBarButton;
    Valor: TdxBarButton;
    Porcentaje: TdxBarButton;
    DescPorcentaje: TdxBarButton;
    DescValor: TdxBarButton;
    Leyenda: TdxBarButton;
    PorcentajeTotal: TdxBarButton;
    DescPorcentajeTotal: TdxBarButton;
    meBarraVertical_DevEx: TdxBarButton;
    BarraHorizontal_DevEx: TdxBarButton;
    mePie_DevEx: TdxBarButton;
    meLinea_DevEx: TdxBarButton;
    meArea_DevEx: TdxBarButton;
    mePuntos_DevEx: TdxBarButton;
    cxImageList16: TcxImageList;
    gfg: TcxBarEditItem;
    cbMostrarEtiquetas: TdxBarCombo;
    BSeries_DevEx: TdxBarCombo;
    BSeriesConfig_DevEx: TdxBarButton;
    BDescripcion_DevEx: TdxBarCombo;
//    procedure BAcercarClick(Sender: TObject);
//    procedure BAnteriorClick(Sender: TObject);
//    procedure ScrollGraficaChange(Sender: TObject);
//    procedure BEditarClick(Sender: TObject);
//    procedure BSeriesClick(Sender: TObject);
//    procedure BImprimirClick(Sender: TObject);
//    procedure meGraficaActualClick(Sender: TObject);
//    procedure ArbolChange(Sender: TObject; Node: TTreeNode);
//    procedure DrawItemMenu(Sender: TObject; ACanvas: TCanvas; ARect: TRect;
//      Selected: Boolean);
//    procedure TipoGraficaClick(Sender: TObject);
//    procedure meTodasClick(Sender: TObject);
//    procedure meTodoslosGruposClick(Sender: TObject);
//    procedure meGrupoActualClick(Sender: TObject);
//    procedure MenuItem1Click(Sender: TObject);
//    procedure MenuItem2Click(Sender: TObject);
//    procedure NoMostrarEtiqueta1Click(Sender: TObject);
//    procedure FormShow(Sender: TObject);
//    procedure FormKeyUp(Sender: TObject; var Key: Word;
//      Shift: TShiftState);
//    procedure BarraNavegacion_DevExShowToolbarsPopup(Sender: TdxBarManager;
//      PopupItemLinks: TdxBarItemLinks);
//    procedure cbMostrarEtiquetasChange(Sender: TObject);
//    procedure BAcercar_DevExClick(Sender: TObject);
//    procedure BAlejar_DevExClick(Sender: TObject);
//    procedure meBarraVertical_DevExClick(Sender: TObject);
//    procedure BarraHorizontal_DevExClick(Sender: TObject);
//    procedure mePie_DevExClick(Sender: TObject);
//    procedure meLinea_DevExClick(Sender: TObject);
//    procedure meArea_DevExClick(Sender: TObject);
//    procedure mePuntos_DevExClick(Sender: TObject);
//    procedure BSeriesConfig_DevExClick(Sender: TObject);
//    procedure BSeries_DevExChange(Sender: TObject);
//    procedure FormCreate(Sender: TObject);
//    procedure BDescripcion_DevExChange(Sender: TObject);
  private
    { Private declarations }
    FCampos,FGrupos : TStrings;
    //FDataSet : TDataSet;
    FNombre, FXSeries : string;
    FGroupCount, FColumnCount,FRecordCount,FMaxPoints : Integer;
    FLastSeries : integer;
    FAgregaDesc,FSoloTotales : Boolean;
    FTipo : eTipoGrafica;
//    FMenuDescripcion : String;
    FPuedeGraficar: Boolean;
    FDescIndex: Integer;
    //DevEx(@am): Lista para guardar los Tags para cada item del combo de Campos y el combo de Descripciones
    listaCampos : TList;
    listaDesc: TList;

    {NUEVOS}
    FDataSet : TZetaClientDataSet;
    FChartBase: TChart;

    {FIN}





    //
//    {$IFDEF TeeChartPro}
//    ChartEditor:TChartEditor;
//    {$ENDIF}
    function CreaGrafica: Boolean;
//    function GetDescripcion(const sTitulo : string ) : string;
    procedure CreaSerie( const sTitulo, sX, sY : String;
                         const Tipo : Integer);
    procedure SetDefaultsGrafica;
    procedure AgregaSeries;
    procedure AgregaPie( var oSerie : TPieSeries );
    procedure DefineFXSeries;
//    procedure AgregaGrupos;
    procedure DefineGrupos;
//    procedure FiltraDataSet( const sFiltro : string );
//    procedure SetChartEditorOptions( const sTitulo : string;
//                                     const Tabs: TChartEditorHiddenTabs;
//
//                                     const Options: TChartEditorOptions);
//    procedure ImprimirGrafica(const lTodas: Boolean; const Paginas : integer);
//    procedure ConstruyeArbol;

    function UnaSerie: Boolean;
    procedure SetMaxPoints(const iSeriesCount: integer);
    procedure SetTipoGrafica( const eTipo : eTipoGrafica );

    procedure MuestraSerie(const Posicion: integer);
//    procedure MuestraDesc(const Posicion: integer);

    procedure MuestraTodasSeries;

    //procedure ClickItemMenuTodos(Sender: TObject);

    procedure CreateTempDataset;
    procedure InitTempDataSet;
//    function GetFiltroGrupos(Nodo: TTreeNode): String;
    //procedure SetMenuChecked(oMenu : TMenu; const Posicion: integer);
//    procedure SetDefView3d;
//    procedure AgregaMenuSeries(eTipo: eTipoGrafica);
    function CampoEsSerie(oCampo: TCampoListado): Boolean;
    procedure SetMaxScroll(const iPuntos: integer);
    function ValidaPieSeries( const iPos: Integer ): Boolean;
//    procedure SetLblDemo;
    procedure Desprepara;
//    function AccedeListaCampos: Integer;
//    function AccedeListaDesc:Integer;
  public
    { Public declarations }
//    function GeneraGrafica: Boolean;
    function Prepara( const sTitulo: String; oDataSet: TZetaClientDataSet; var ChartBase: TChart ): Boolean;
  end;


var
  Grafica_DevEx: TGraficaBase_DevEx;


  procedure PreparaGrafica( const sTitulo: String; const oDataSet: TZetaClientDataSet; var ChartBase: TChart);
  procedure DesPreparaGrafica;


implementation

uses ZetaCommonTools,
     ZetaDialogo,
     ZQRReporteGrafica;

//{$R *.DFM}

const K_MAX_POINTS = 30;
      Off_SET = 3;
      K_STR_TOTAL_EMPRESA = 'Total de Empresa';
      K_TODOS = 'Todos';

//DevEx(@am): Constructures de los objetos  para las listas
//constructor  TTagItemsCombo.Create(const ItemIndex: Integer; const TagItem:Integer );
//begin
//     self.ItemIndex := ItemIndex;
//     self.ItemTag := TagItem;
//end;
//
//constructor  TTagItemsDesc.Create(const ItemIndex: Integer; const TagItem:Integer );
//begin
//     self.ItemIndex := ItemIndex;
//     self.ItemTag := TagItem;
//end;
//

procedure PreparaGrafica( const sTitulo: String; const oDataSet: TZetaClientDataSet; var ChartBase: TChart );
begin
     Grafica_DevEx.Prepara( sTitulo, oDataSet, ChartBase);
end;

procedure DesPreparaGrafica;
begin
     Grafica_DevEx.Desprepara;
     FreeAndNil( Grafica_DevEx );
end;

procedure TGraficaBase_DevEx.AgregaPie( var oSerie : TPieSeries );
begin
     with oSerie do
     begin
          with Marks do
          begin
               Visible := TRUE;
               Style := smsLabelPercent;
          end;
          ColorEachPoint := TRUE;
          ExplodeBiggest := 25;
          with OtherSlice do
          begin
               Style := poBelowPercent;
               Text := 'Otros';
               Value := 5;
          end;
     end;
end;

procedure TGraficaBase_DevEx.CreaSerie( const sTitulo, sX, sY : String;
                              const Tipo : Integer );
var
   oSeries : TBarSeries;
begin
     oSeries := TBarSeries.Create( Chart );
     with oSeries do
     begin
          Title := sTitulo;
          Marks.Visible := FALSE;
          Marks.Style := smsValue;
          //BarStyle := bsRectGradient;   //OLD
          BarStyle := bsRectangle;
          ParentChart := Chart;
          DataSource := FDataSet;
          //YValues.Value := 133.5;//esto no se puede
          YValues.ValueSource := sy;  { <-- the Field for Bar Values }
          XLabelsSource := FXSeries;          { <-- the Field for Bar Labels }
          //ColorEachPoint := FColumnCount = 1;
     end;
     Chart.AddSeries( oSeries );
end;

procedure TGraficaBase_DevEx.DefineFXSeries;
 var
    sTitulo : string;
    i : integer;
 procedure SetCamposFXSeries;
 begin
      if FDataSet.RecordCount > 0 then
      begin
           with FDataSet do
           begin
                First;
                while ( not EOF ) do
                begin
                     FXSeries := FieldByName( 'Descripcion' ).AsString;
                     sTitulo := FieldByName( 'Descripcion' ).AsString;
                     Next;
                end;
           end;
      end
      else FXSeries := '< Indefinido >';
 end;
begin
     SetCamposFXSeries;
     //Chart.Foot.Text.Text:= FMenuDescripcion;
end;

procedure TGraficaBase_DevEx.AgregaSeries;
 var oCampo : TCampoListado;
     i, j : integer;
     sCampo : string;
begin
     FChartBase.FreeAllSeries;
     DefineFXSeries;
     {----------------------------}
     {Series a Detalle, Sin Grupos}
     with FDataSet do
     begin
          if FDataSet.RecordCount = 1 then
             FChartBase.Legend.LegendStyle := lsSeries;
               j := 0;
               for i:= 2 to FieldCount - 1 do
               begin
                    sCampo := Fields[j].AsString; FieldByName( 'Descripcion' ).AsString;
                    CreaSerie( oCampo.Titulo,'',sCampo,0 );
                    Inc( j );
               end;
     end;
     {------------------------------}
end;

procedure TGraficaBase_DevEx.InitTempDataSet;
begin
     with cdsGrupos do
     begin
          Active := False;
          while FieldCount > 0 do
          begin
               Fields[ FieldCount - 1 ].DataSet := nil;
          end;
          FieldDefs.Clear;
     end;
end;

procedure TGraficaBase_DevEx.CreateTempDataset;
var
   i: Integer;
begin
     with cdsGrupos do
     begin
          for i := 0 to ( FieldDefs.Count - 1 ) do
          begin
               FieldDefs[ i ].CreateField( nil );
          end;
          CreateDataset;
     end;
end;

//procedure TGraficaBase_DevEx.AgregaGrupos;
//begin
//     InitTempDataSet;
//     DefineGrupos;
//end;

procedure TGraficaBase_DevEx.DefineGrupos;
 var oGrupo : TGrupoOpciones;
     j: integer;

 function EsCorteGrupo : Boolean;
  var i : integer;
 begin
      Result := FALSE;
      for i := 1 to FGrupos.Count -1 do
      begin
           oGrupo := TGrupoOpciones(FGrupos.Objects[i]);
           Result := Result OR
                     (FDataSet.FieldByName(oGrupo.NombreCampo).AsString <>
                     cdsGrupos.FieldByName(oGrupo.NombreCampo).AsString);
           if Result then Break;
      end;
 end;

 procedure AgregaRegistroGrupo;
  var i: integer;
  procedure SetRecord(const sField : string);
  begin
       with FDataSet.FieldByName(sField) do
       begin
            if DataType in [ftDate,ftTime,ftDateTime] then
               cdsGrupos.FieldByName(sField).AsDateTime := AsDateTime
            else cdsGrupos.FieldByName(sField).AsString := AsString;
       end;
  end;
 begin
      cdsGrupos.Append;
      for i := 1 to FGrupos.Count -1 do
      begin
           oGrupo := TGrupoOpciones(FGrupos.Objects[i]);
           SetRecord(oGrupo.NombreCampo);
           with oGrupo.ListaEncabezado do
           begin
                if Count>0 then
                   SetRecord(TCampoListado(Objects[0]).NombreCampo);
                if Count>1 then
                   SetRecord(TCampoListado(Objects[1]).NombreCampo);
           end;
      end;
      cdsGrupos.Post;
 end;

 procedure AgregaGruposDataSet( const sFieldName : string );

   procedure AgregaDefField( const Tipo : TFieldType;
                             const iSize : Integer );
   begin
        with cdsGrupos.FieldDefs.AddFieldDef do
        begin
             Name := sFieldName;
             DisplayName := sFieldName;
             DataType := Tipo;
             Size := iSize;
        end;
   end;
 begin
      if sFieldName = '' then
         with cdsGrupos.FieldDefs.AddFieldDef do
         begin
              Name := '_'+IntToStr(j);
              DataType := ftString;
              Size := 1;
              Inc(j);
         end
      else
          with FDataSet.FieldByName(sFieldName) do
             AgregaDefField( DataType, Size );
 end;

 var i : integer;
     sIndexName : string;
begin
     j:=0;
     for i := 1 to FGrupos.Count -1 do
     begin
          oGrupo := TGrupoOpciones(FGrupos.Objects[i]);
          AgregaGruposDataset( oGrupo.NombreCampo );
          with oGrupo.ListaEncabezado do
          begin
               if Count > 0 then
                  AgregaGruposDataset( TCampoListado(Objects[0]).NombreCampo )
               else AgregaGruposDataset( oGrupo.NombreCampo+IntToStr(i) );
               if Count > 1 then
                  AgregaGruposDataset( TCampoListado(Objects[1]).NombreCampo )
               else AgregaGruposDataset( '' );
          end;
          sIndexName := ConcatString( sIndexName,oGrupo.NombreCampo, ',' );
     end;

     CreateTempDataset;

     with FDataSet do
     begin
          DisableControls;

          {Se Agrega el primer registro al DataSet Temporal}
          First;
          AgregaRegistroGrupo;
          Next;
          while NOT EOF do
          begin
               if EsCorteGrupo then
                  AgregaRegistroGrupo;
               Next;
          end;
          EnableControls;
     end;
     {$ifdef CAROLINA}
     cdsGrupos.SaveToFile(ExtractFileDir((Application.ExeName )) + '\Grupos.cds',dfBinary);
     {$endif}
     {SoloTotales con dos o mas Grupos}
end;

procedure TGraficaBase_DevEx.SetDefaultsGrafica;
 var I : integer;
begin
     FGroupCount := FGrupos.Count - 1;
     FColumnCount := 0;

     for i:= 0 to FCampos.Count -1 do
         if NOT (TCampoListado(FCampos.Objects[i]).OpImp in [ocNinguno,ocAutomatico]) then
            Inc(FColumnCount);

     with Chart do
     begin
          with Title do
          begin
               Text.Text := FNombre;
               Font.Color := clBlack;
          end;
          Legend.LegendStyle := lsSeries;
          Legend.Alignment := laBottom;
     end;
//     SetDefView3d;

     //AgregaMenuSeries(tgBarraVertical);
end;

// DevEx(@am): Encuentra el valor del campo del item seleccionado del combo de campos
//function TGraficaBase_DevEx.AccedeListaCampos: Integer;
//var
//   i, Tag:Integer;
//begin
//  Tag:=0;
//  for i := 0 to listaCampos.Count-1 do
//  begin
//       if TTagItemsCombo(listaCampos[i]).CampoIndex = BSeries_DevEx.ItemIndex then
//       begin
//            Tag:= TTagItemsCombo(listaCampos[i]).CampoTag;
//            break;
//       end;
//  end;
//  Result:=Tag;
//end;

// DevEx(@am): Encuentra el valor del campo del item seleccionado del de Descripciones
//function TGraficaBase_DevEx.AccedeListaDesc: Integer;
//var
//   i, Tag:Integer;
//begin
//  Tag:=0;
//  for i := 0 to listaDesc.Count-1 do
//  begin
//       if TTagItemsDesc(listaDesc[i]).CampoIndex = BDescripcion_DevEx.ItemIndex then
//       begin
//            Tag:= TTagItemsDesc(listaDesc[i]).CampoTag;
//            break;
//       end;
//  end;
//  Result:=Tag;
//end;

//procedure TGraficaBase_DevEx.AgregaMenuSeries(eTipo : eTipoGrafica);
// var i,j: integer;
// procedure AgregaPopupDescr(const sTitulo : string);
// begin
//      {***NUEVO***}
//      with BDescripcion_DevEx,BDescripcion_DevEx.Items do
//      begin
//           Add(sTitulo);
//           //Se agrega la relacion entre index y campo a la lista.
//           listaDesc.Add(TTagItemsDesc.Create(Count-1, i)); //Index del ultimo item agregado, campo
//           //Seleccionar el item indicado
//           if i = 0 then
//              FDescIndex := i;
//      end;
//      {***}
// end;
//
// procedure AgregaPopupMenu(const sTitulo : string; const lEnabled : Boolean = TRUE);
// begin
//      {***NUEVO***}
//      with BSeries_DevEx,BSeries_DevEx.Items do
//      begin
//           Add(sTitulo);
//           //Se agrega la relacion entre index y campo a la lista.
//           listaCampos.Add(TTagItemsCombo.Create(Count-1, j)); //Index del ultimo item agregado, campo
//           Enabled := lEnabled;
//      end;
//      {***}
// end;
// var oCampo : TCampoListado;
//     //Popup : TMenuItem;
//begin
//     {***NUEVO***}
//     //Los combos no tienen la propiedad tag asi que hay que crear una lista de objetos
//     BSeries_DevEx.Items.Clear;
//     BDescripcion_DevEx.Items.Clear;
//     listaCampos.Clear;
//     listaDesc.Clear;
//     {***}
//
//     j:=0;
//     if (FColumnCount > 1) then
//     begin
//          if (eTipo <> tgPie) then
//          begin
//               {***NUEVO***}
//               BSeries_DevEx.Items.Add(K_TODOS);
//               {***}
//          end;
//          FAgregaDesc := NOT FSoloTotales AND (FGrupos.Count>0);
//
//          for i := 0 to FCampos.Count -1 do
//          begin
//               oCampo := TCampoListado(FCampos.Objects[i]);
//               if FAgregaDesc AND (i<=1) then
//                  AgregaPopupDescr(oCampo.Titulo);
//               if CampoEsSerie(oCampo) then
//               begin
//                    AgregaPopupMenu(oCampo.Titulo);
//                    Inc(j);
//               end;
//          end;
//          BDescripcion_DevEx.Enabled := NOT (FSoloTotales AND (FGrupos.Count=0));
//          if NOT FAgregaDesc then
//          begin
//               with FGrupos,TGrupoOpciones(Objects[Count-1]) do
//                    for i := 0 to ListaEncabezado.Count - 1 do
//                        if (i<=1) then
//                           AgregaPopupDescr(TCampoOpciones(ListaEncabezado.Objects[i]).Titulo)
//                        else Break;
//          end;
//     end
//     else
//     begin
//          for i := 0 to FCampos.Count -1 do
//          begin
//               oCampo := TCampoListado(FCampos.Objects[i]);
//               if CampoEsSerie(oCampo) then
//               begin
//                    AgregaPopupMenu(TCampoListado(FCampos.Objects[i]).Titulo, FALSE);
//                    Break;
//               end;
//          end;
//     end;
//end;

function TGraficaBase_DevEx.CampoEsSerie(oCampo : TCampoListado) : Boolean;
begin
     with oCampo do
          Result := (OpImp <> ocNinguno) AND
                    (TipoImp in [tgNumero,tgFloat]) AND
                    (FSoloTotales  OR NOT SQLColumna.EsConstante );
end;

procedure TGraficaBase_DevEx.SetMaxScroll( const iPuntos : integer );
begin
     with ScrollGrafica do
     begin
          Max := Trunc(FRecordCount/iPuntos);
          //if (FRecordCount mod iPuntos) < iPuntos then Max := Max + 1;
          Enabled := Max>=1;
     end;
end;

procedure TGraficaBase_DevEx.SetMaxPoints(const iSeriesCount : integer);
begin
     FMaxPoints := iMin(Trunc(K_MAX_POINTS/iSeriesCount-1)+1,FDataset.RecordCount);
     Chart.MaxPointsPerPage := FMaxPoints;
     SetMaxScroll(FMaxPoints)
end;

function TGraficaBase_DevEx.CreaGrafica: Boolean;
begin
     Result := TRUE;
//     SetDefaultsGrafica;

     if UnaSerie then
     begin {Barras - N Series}
//          Arbol.Visible := FALSE;
//          Splitter.Visible := FALSE;
          AgregaSeries;
     end
     else
     begin
//          if lInit then
//             AgregaGrupos;
          AgregaSeries;
          //ConstruyeArbol;
//          cdsGrupos.First;
          //FiltraDataSet('');
//          Splitter.Visible := TRUE;
     end;
     {****NUEVO***}
//     FPuedeGraficar:= FALSE;
//     if BSeries_DevEx.items.Count > 0 then
//        BSeries_DevEx.itemIndex := 0;
     {***}
     //SetMaxPoints(FColumnCount);
end;

function TGraficaBase_DevEx.Prepara( const sTitulo: String; oDataSet: TZetaClientDataSet; var ChartBase: TChart ): Boolean;
begin
     Result := TRUE;
     FDataSet := oDataSet;
     FNombre := sTitulo;
     FRecordCount := FDataSet.RecordCount;
end;

procedure TGraficaBase_DevEx.Desprepara;
begin
     Chart.FreeAllSeries;
end;

//function TGraficaBase_DevEx.GeneraGrafica: Boolean;
// var i: integer;
//begin
//     Result := FALSE;
//     for i:=0 to FCampos.Count-1 do
//     begin
//          Result := CampoEsSerie(TCampoListado(FCampos.Objects[i]));
//          if Result then
//             Break;
//     end;
//
//     if NOT Result then
//        Raise Exception.Create('No Hay Ninguna Columna para Graficar');
//
//     Result := TRUE;
//     if CreaGrafica then
//     begin
//          //ShowModal;
//          FDataSet.Filter := '';
//          FDataSet.Filtered :=FALSE;
//     end;
//end;

//procedure TGraficaBase_DevEx.BAcercarClick(Sender: TObject);
//begin
//     inherited;
//     with Chart do
//     begin
//          if MaxPointsPerPage > 1 then
//          begin
//               MaxPointsPerPage := MaxPointsPerPage - 1;
//               //ScrollGrafica.Max := FRecordCount-MaxPointsPerPage;
//               SetMaxScroll(MaxPointsPerPage);
//          end;
//          BAcercar_DevEx.Enabled := MaxPointsPerPage > 1;
//          BAlejar_DevEx.Enabled := TRUE;
//     end;
//
//end;

//procedure TGraficaBase_DevEx.BAnteriorClick(Sender: TObject);
//begin
//     inherited;
//     Chart.Page := ScrollGrafica.Position+1;
//end;

//procedure TGraficaBase_DevEx.ScrollGraficaChange(Sender: TObject);
//begin
//     inherited;
//     Chart.Page := ScrollGrafica.Position+1;
//end;

//procedure TGraficaBase_DevEx.FiltraDataSet( const sFiltro : string );
//begin
//     FDataSet.Filter := sFiltro;
//     FDataSet.Filtered := TRUE;
//     if ( FLastSeries > 0 ) then
//        MuestraSerie( FLastSeries - 1 );     // Reevalua la serie activa, para casos de Graficas de PIE
//     //Chart.MaxPointsPerPage := iMin(FMaxPoints,FDataset.RecordCount);
//     if FLastSeries > 0 then SetMaxPoints(1)
//     else SetMaxPoints(FColumnCount);
//     Chart.RefreshData;
//end;

//procedure TGraficaBase_DevEx.BEditarClick(Sender: TObject);
//begin
//     SetChartEditorOptions( 'Editar Gr�fica',
//                            [],
//                            [ceAdd, ceDelete, ceChange, ceClone, ceTitle] );
//end;

//procedure TGraficaBase_DevEx.SetChartEditorOptions( const sTitulo : string;
//                                          const Tabs : TChartEditorHiddenTabs;
//                                          const Options : TChartEditorOptions );
//begin
//    {$IFDEF TeeChartPro}
//     with ChartEditor do
//     begin
//          Title := sTitulo;
//          //HideTabs := Tabs;
//          //Options := Options;
//          Execute;
//     end;
//     {$ENDIF}
//end;

//procedure TGraficaBase_DevEx.BSeriesClick(Sender: TObject);
//begin
//     SetChartEditorOptions( 'Editar Series',
//                            [cetGeneral, cetAxis, cetTitles, cetLegend, cetPanel, cetPaging, cetWalls, cet3D],
//                            [ceAdd, ceDelete, ceChange, ceClone, ceTitle] );
//
//end;

//procedure TGraficaBase_DevEx.BImprimirClick(Sender: TObject);
//begin
//     ImprimirGrafica(FALSE,1);
//end;

function TGraficaBase_DevEx.UnaSerie : Boolean;
var
   iNumeroSeries: Integer;
begin
     iNumeroSeries := FDataSet.FieldCount - 2;
     Result := ( iNUmeroSeries = 1 );
//     Result := ( FGroupCount = 0) OR
//               ((FGroupCount = 1) AND (FColumnCount= 1) AND FSoloTotales )
end;

//procedure TGraficaBase_DevEx.ImprimirGrafica( const lTodas : Boolean; const Paginas : integer );
// var oDataSet : TDataSet;
//begin
//     if ZReportTools.ModuloGraficas then
//     begin
//
//          try
//             if lTodas then
//             begin
//                  if UnaSerie then oDataSet := NIL
//                  else oDataSet := cdsGrupos;
//                  QrReporteGrafica.GeneraVariasGraficas(Chart,FDataSet,oDataset,Paginas);
//             end
//             else {Imprime la Grafica que se esta viendo en ese momento}
//                  QrReporteGrafica.GeneraUnaGrafica(Chart,FDataSet,Paginas);
//          finally
//                 CreaGrafica;
//          end;
//     end;
//end;
//procedure TGraficaBase_DevEx.meGraficaActualClick(Sender: TObject);
//begin
//     ImprimirGrafica(FALSE,1);
//end;

//procedure TGraficaBase_DevEx.meTodasClick(Sender: TObject);
//begin
//     ImprimirGrafica(TRUE,ScrollGrafica.Max+1);
//end;
//
//procedure TGraficaBase_DevEx.meTodoslosGruposClick(Sender: TObject);
//begin
//     ImprimirGrafica(TRUE,ScrollGrafica.Max);
//end;
//
//procedure TGraficaBase_DevEx.meGrupoActualClick(Sender: TObject);
//begin
//     ImprimirGrafica(FALSE,ScrollGrafica.Max);
//end;

{Arbol}
//procedure TGraficaBase_DevEx.ConstruyeArbol;
//var
//    oPadre, oHijo : TTreeNode;
//    sActual : String;
//    aGrupos : Array of String;
//    nCorte, nNivel : Integer;
//
//
//    procedure InicializaArbol;
//    var
//        i : Integer;
//    begin
//        aGrupos := VarArrayCreate( [ 0, FGroupCount-1 ], varOleStr );
//        for i := 0 to FGroupCount-1 do
//            aGrupos[ i ] := '';
//        Arbol.Items.BeginUpdate;
//        Arbol.Items.Clear;
//        oPadre := Arbol.Items.AddChild( NIL, 'Empresa' );
//        with oPadre do
//        begin
//            ImageIndex := 7; //19
//            SelectedIndex := 7;  //19
//            Data := Pointer( 0 );
//        end;
//    end;
//
//    function GrupoCorte : Integer;
//    var
//        i : Integer;
//    begin
//        Result := -1;
//        for i := 0 to FGroupCount-1 do
//        begin
//            if ( cdsGrupos.Fields[ i ].AsString <> aGrupos[ i ] ) then
//            begin
//                Result := i;
//                Exit;
//            end;
//        end;
//    end;
//
//begin   // ConstruyeArbol
//     Arbol.Visible := not (FSoloTotales and (FGroupCount = 1));
//     if Arbol.Visible then
//     begin
//          InicializaArbol;
//          with cdsGrupos do
//          begin
//              First;
//              while not Eof do
//              begin
//                  nCorte := GrupoCorte;
//                  if ( nCorte >= 0 ) then
//                  begin
//                      while oPadre.Level > nCorte do
//                          oPadre := oPadre.Parent;
//
//                      for nNivel := nCorte to FGroupCount-1 do
//                      begin
//                        sActual := Fields[ nNivel*Off_SET ].AsString;
//                        aGrupos[ nNivel ] := sActual;
//                        //pendiente, para cuando haya descripcion:
//                        //oHijo := Arbol.Items.AddChild( oPadre, sActual + Fields[ nNivel + nGrupos ].AsString );
//                        sActual := Fields[ (nNivel*Off_SET)+1 ].AsString;
//                        if StrLleno(Fields[ (nNivel*Off_SET)+2 ].AsString) then
//                           sActual := sActual + ': ' + Fields[ (nNivel*Off_SET)+2 ].AsString;
//                        oHijo := Arbol.Items.AddChild( oPadre, sActual );
//                        with oHijo do
//                        begin
//                            ImageIndex := 8; //14
//                            //pendiente, si se quiere cambiar el bitmap cuando
//                            //esta seleccionado
//                            SelectedIndex := 9; //15
//                            Data := Pointer( RecNo );
//                        end;
//                        oPadre := oHijo;
//                      end;
//                  end;
//                  Next;
//              end;
//          end;
//
//          with Arbol do
//          begin
//              Items[ 0 ].Expand( FALSE );
//              Items.EndUpdate;
//              Selected := Items[ 1 ];
//              FiltraDataset(GetFiltroGrupos( Selected ));
//          end;
//     end;
//end;

//function TGraficaBase_DevEx.GetFiltroGrupos( Nodo : TTreeNode ) : String;
//var
//    nRecord : Integer;
//    i : Integer;
//
//    function FiltroUnCampo( const nPos : Integer ) : String;
//    begin //PENDIENTE SI SE AGRUPA POR TIPO <> STRING
//        // Supone que todos los Grupos son tipo String
//        with cdsGrupos.Fields[ nPos*Off_SET] do
//             if DataType in [ftDate,ftTime,ftDateTime] then
//                Result := FieldName  + ' = ' + EntreComillas(FormatDateTime('dd/mm/yyyy',AsDateTime))
//             else Result := FieldName  + ' = ' + EntreComillas(AsString);
//    end;
//    function GetDescripcion(const iPos : integer): string;
//    begin
//         with cdsGrupos do
//         begin
//              Result := Fields[(iPos*Off_SET)+1].AsString;
//              if StrLleno(Fields[(iPos*Off_SET)+2].AsString) then
//                 Result := Result +': '+Fields[(iPos*Off_SET)+2].AsString;
//         end;
//    end;
//    function GetGrupoDescripcion(const Posicion : integer) : string;
//    begin
//         with TGrupoOpciones(FGrupos.Objects[Posicion]) do
//         begin
//              if ListaEncabezado.Count > 0 then
//                 Result := ListaEncabezado[0]
//              else Result := Titulo;
//         end;
//    end;
//begin   // GetFiltroGrupos
//     Chart.Title.Text.Text := '';
//     nRecord := Integer( Nodo.Data );
//     if ( nRecord = 0 ) then
//         Result := ''
//     else
//     begin
//         cdsGrupos.Recno := nRecord;
//         Result := FiltroUnCampo( 0 );
//         Chart.Title.Text.Add( GetGrupoDescripcion(1) + ': ' + GetDescripcion(0) );
//         for i := 1 to Nodo.Level-1 do
//         begin
//              Result := ConcatFiltros(Result, FiltroUnCampo( i ));
//              Chart.Title.Text.Add( GetGrupoDescripcion(i+1) + ':' + GetDescripcion(i) );
//         end;
//     end;
//end;

//procedure TGraficaBase_DevEx.ArbolChange(Sender: TObject; Node: TTreeNode);
//begin
//    if Node <> NIL then
//       FiltraDataset(GetFiltroGrupos( Node ));
//end;

//procedure TGraficaBase_DevEx.DrawItemMenu(Sender: TObject; ACanvas: TCanvas;
//  ARect: TRect; Selected: Boolean);
//begin
//     {aCanvas.Pen.Color := clRed;
//     with (Sender as TMenuItem).Canvas do  { draw on control canvas, not on the form }
//     {begin
//          Pen.Color := clRed;
//          {FillRect(Rect);       { clear the rectangle }
//          {sIndex := IntToStr( Index + 1  );
//          TextOut(Rect.Left + 18 - TextWidth( sIndex ), Rect.Top, sIndex );
//          TextOut(Rect.Left + 20, Rect.Top, ': '
//              + (Control as TListBox).Items[Index])  { display the text }
//     //end;
//end;

procedure TGraficaBase_DevEx.MuestraTodasSeries;
 var i: integer;
begin
     for i := 0 to FColumnCount-1 do
     begin
          Chart.Series[i].Active := TRUE;
     end;
     SetMaxPoints(FColumnCount);
     FLastSeries := 0;
end;

//procedure TGraficaBase_DevEx.MuestraDesc(const Posicion: integer);
// var i: integer;
//     sTitulo : string;
//begin
//     if FAgregaDesc then
//     begin
//          with TCampoListado(FCampos.Objects[Posicion]) do
//          begin
//               FXSeries := NombreCampo;
//               sTitulo := Titulo;
//          end;
//     end
//     else
//     begin
//          with FGrupos,TGrupoOpciones(Objects[Count-1]) do
//               with TCampoOpciones(ListaEncabezado.Objects[Posicion]) do
//               begin
//                    FXSeries := NombreCampo;
//                    sTitulo := Titulo;
//               end;
//     end;
//     for i := 0 to FColumnCount-1 do
//         Chart.Series[i].XLabelsSource := FXSeries;
//
//     with Chart.Foot.Text do
//     begin
//          Clear;
//          Add(GetDescripcion(sTitulo));
//     end;
//
//end;

procedure TGraficaBase_DevEx.MuestraSerie( const Posicion : integer );
 var i: integer;
begin
     LblNoGraficar.Visible := FALSE;
     //SetLblDemo;
     if Chart.SeriesCount > 0 then
     begin
          for i := 0 to FColumnCount-1 do
              Chart.Series[i].Active := ( i = Posicion ) and
                                        ( ( not ( Chart.Series[i] is TPieSeries ) ) or
                                          ValidaPieSeries(i) );
     end;
     SetMaxPoints(1);
     FLastSeries := Posicion+1;
end;

function TGraficaBase_DevEx.ValidaPieSeries( const iPos: Integer ): Boolean;
const
     K_MESS_NO_GRAFICAR = 'El Campo <%s> No tiene Datos Para Graficar';
var
   oBookMark : TBookMark;
   sCampo : String;
begin
     Result := FALSE;
     with Chart.Series[iPos] do
     begin
          sCampo := YValues.ValueSource;
          with TDataSet( Datasource ) do                                     // Se esta suponiendo que el datasource siempre es un DataSet
               if Active and ( not IsEmpty ) then
               begin
                    oBookMark := GetBookMark;
                    try
                       First;
                       while ( not EOF ) and ( not Result ) do
                       begin
                            Result := ( FieldByName( sCampo ).AsFloat > 0 );    // Es un valor n�merico, con uno solo que no sea cero no habr� problema
                            Next;
                       end;
                       GotoBookMark( oBookMark );
                    finally
                       FreeBookmark( oBookMark );
                    end;
               end;
          LblNoGraficar.Visible := ( not Result );
          if LblNoGraficar.Visible then
          begin
               LbDemo.Visible := FALSE;
               LblNoGraficar.Caption := Format( K_MESS_NO_GRAFICAR, [ Title ] );
               LblNoGraficar.Align := alClient;
          end;
     end;
end;

//procedure TGraficaBase_DevEx.TipoGraficaClick(Sender: TObject);
//begin
//     SetTipoGrafica(eTipoGrafica(TMenuItem(Sender).MenuIndex));
//end;

procedure TGraficaBase_DevEx.SetTipoGrafica( const eTipo : eTipoGrafica );
 var oSerie : TChartSeries;
     SerieClass:TChartSeriesClass;
     i: integer;
begin
     FTipo := eTipo;
     case eTipo of
          tgBarraVertical: SerieClass := TBarSeries;
          tgBarraHorizontal:SerieClass := THorizBarSeries;
          tgPie:SerieClass := TPieSeries;
          tgLinea:SerieClass := TLineSeries;
          tgArea:SerieClass := TAreaSeries;
          tgPuntos:SerieClass := TPointSeries
          else SerieClass := TBarSeries;
     end;

     for i := 0 to FDataSet.FieldCount - 3 do
     begin
          oSerie := FChartBase.Series[i];
          ChangeSeriesType(oSerie,SerieClass);
          with oSerie do
          begin
               with Marks do
               begin
                    Visible := FALSE;
                    Style := smsValue;
               end;
               ColorEachPoint := FALSE;
          end;

          if (SerieClass = TBarSeries) then
               TBarSeries(oSerie).BarStyle := bsRectangle //TBarSeries(oSerie).BarStyle := bsRectGradient //old
          else if (SerieClass = THorizBarSeries) then
               THorizBarSeries(oSerie).BarStyle := bsRectangle //THorizBarSeries(oSerie).BarStyle := bsRectGradient //old
          else if SerieClass = TPieSeries then
               AgregaPie(TPieSeries(oSerie));
     end;

     if SerieClass = TPieSeries then
     begin
          MuestraSerie( 0 ); //Nada mas va a mostrar la primera;
          //AgregaMenuSeries(eTipo);
          {***NUEVO***}
          //DevEx(@am): Seleccionar el item que se desea mostrar al elegir la grafica, pues la opcion TODOS no se agrega
          if BSeries_DevEx.Items.Count > 0 then
             BSeries_DevEx.ItemIndex := 0; //Se selecciona la primera opcion.
          {***}
     end
     else
     begin
          SetMaxPoints( FColumnCount );
          {***NUEVO***} //PROBAR QUE FUNCIONE BIEN.
          FPuedeGraficar:= FALSE;
          //AgregaMenuSeries(eTipo); //Agregar los items de nuevo para que aparezca la opcion de TODOS otravez, esto corrigue un bug de la vs 2013.
          FPuedeGraficar:= TRUE;
          //DevEx(@am): Seleccionar el item que se desea mostrar al elegir la grafica
          if BSeries_DevEx.Items.Count > 2 then
             BSeries_DevEx.ItemIndex := 1; //Si existen por lo menos 2 items se selecciona la opcion despues de TODOS.
          {***}

          //SetDefView3d;
          if FLastSeries > 0 then
             MuestraSerie(FLastSeries-1)
          else MuestraTodasSeries;
     end;

//     BAcercar_DevEx.Enabled := eTipo <> tgPie;
//     BAlejar_DevEx.Enabled := eTipo <> tgPie;
//     ScrollGrafica.Enabled := eTipo <> tgPie;
end;

//procedure TGraficaBase_DevEx.SetDefView3d;
//begin
//     with Chart.View3dOptions do
//     begin
//          Elevation := 0;
//          Perspective := 0;
//          Tilt := 0;
//          Rotation := 350;
//          Orthogonal := FALSE;
//     end;
//end;
//
//procedure TGraficaBase_DevEx.MenuItem1Click(Sender: TObject);
//begin
//     {with TMenuItem(Sender) do
//     begin
//          Chart.View3d := NOT Checked;
//          Checked := NOT Checked;
//     end;}//OLD
//
//     with op3D do
//     begin
//         Chart.View3d := op3D.Down;
//     end;
//
//end;
//
//procedure TGraficaBase_DevEx.MenuItem2Click(Sender: TObject);
//begin
//     with TMenuItem(Sender) do
//     begin
//          Chart.AnimatedZoom := Checked;
//          Checked := NOT Checked;
//     end;
//end;
//
//procedure TGraficaBase_DevEx.NoMostrarEtiqueta1Click(Sender: TObject);
// var i: integer;
//     lVisible : Boolean;
//     oStyle : TSeriesMarksStyle;
//begin
//     with TMenuItem(Sender) do
//     begin
//          lVisible := MenuIndex > 0 ;
//          oStyle := TSeriesMarksStyle(MenuIndex - 1);
//          Checked := NOT Checked;
//     end;
//     for i := 0 to FColumnCount-1 do
//     begin
//          with Chart.Series[i].Marks do
//          begin
//               Visible := lVisible;
//               Style :=  oStyle;
//          end;
//     end;
//end;

//procedure TGraficaBase_DevEx.FormShow(Sender: TObject);
//begin
//     lbDemo.Align := AlClient;
//     SetLblDemo;
//     //DevEx(@am): Se cambian las subopciones de NO mostrar etiquetas a un combo pues no es posible utilizar las opciones del menu de DevEx como RadioButtons.
//     cbMostrarEtiquetas.ItemIndex :=0; //PRIMERA OPCION
//
//     //DevEx(@am): Solo comenzamos a graficar ya que estan todos los items agregados
//     FPuedeGraficar:= TRUE;
//     //DevEx(@am): Asignamos el item seleccionado ya que se agregaron todos a los combos
//     if BSeries_DevEx.Items.Count>2 then
//        if (BSeries_DevEx.ItemIndex = 0) then
//           if Bseries_DevEx.CurText = K_TODOS then
//              BSeries_DevEx.ItemIndex := 1; //Para que no se seleccion Todos aunque sea el primer item
//     if BDescripcion_DevEx.Items.Count>0 then
//        BDescripcion_DevEx.ItemIndex := FDescIndex
//     else
//         BDescripcionOpciones_DevEx.Enabled := FALSE; //Se agrego pues en VS2013 cuando no se tiene ninguna opcion este boton aparece activo y no hace nada.
//end;

//procedure TGraficaBase_DevEx.SetLblDemo;
//begin
//     lbDemo.Visible := not ZReportTools.ModuloGraficas;
//end;

//function TGraficaBase_DevEx.GetDescripcion(const sTitulo: string): string;
//begin
//     if Trim(sTitulo) = ':' then
//        Result := 'Descripci�n'
//     else Result := sTitulo;
//end;

//procedure TGraficaBase_DevEx.FormKeyUp(Sender: TObject; var Key: Word;
//  Shift: TShiftState);
//begin
//     if Key = 27  then
//     begin
//          Key := 0;
//          //Close;
//     end;
//end;

////DevEx (by am): Metodo agregado para que no no salga ningun pop up  al dar clic derecho sobre los toolbar de las formas de edicion.
//procedure TGraficaBase_DevEx.BarraNavegacion_DevExShowToolbarsPopup(
//  Sender: TdxBarManager; PopupItemLinks: TdxBarItemLinks);
//begin
//     inherited;
//     Abort;
//end;

////DevEx(@am): Los submenus de la opcion mostrar etiquetas se cambiaron por un combo.
//procedure TGraficaBase_DevEx.cbMostrarEtiquetasChange(Sender: TObject);
//var i: integer;
//     lVisible : Boolean;
//     oStyle : TSeriesMarksStyle;
//begin
//     with cbMostrarEtiquetas do
//     begin
//          lVisible := ItemIndex > 0;
//          oStyle := TSeriesMarksStyle(ItemIndex - 1);
//     end;
//     for i := 0 to FColumnCount-1 do
//     begin
//          with Chart.Series[i].Marks do
//          begin
//               Visible := lVisible;
//               Style :=  oStyle;
//          end;
//     end;
//
//end;

//procedure TGraficaBase_DevEx.BAcercar_DevExClick(Sender: TObject);
//begin
//     inherited;
//     with Chart do
//     begin
//          if MaxPointsPerPage > 1 then
//          begin
//               MaxPointsPerPage := MaxPointsPerPage - 1;
//               //ScrollGrafica.Max := FRecordCount-MaxPointsPerPage;
//               SetMaxScroll(MaxPointsPerPage);
//          end;
//          BAcercar_DevEx.Enabled := MaxPointsPerPage > 1;
//          BAlejar_DevEx.Enabled := TRUE;
//     end;
//end;
//
//procedure TGraficaBase_DevEx.BAlejar_DevExClick(Sender: TObject);
//begin
//      with Chart do
//     begin
//          if MaxPointsPerPage < FRecordCount then
//          begin
//               MaxPointsPerPage := MaxPointsPerPage + 1;
//               SetMaxScroll(MaxPointsPerPage);
//          end;
//          BAlejar_DevEx.Enabled := MaxPointsPerPage < FRecordCount;
//          BAcercar_DevEx.Enabled := TRUE;
//     end;
//end;

//{***DevEx(@am): Se define un evento para cada opcion y se le manda el valor del TAG para ubicar el enumerado correcto***}
//procedure TGraficaBase_DevEx.meBarraVertical_DevExClick(Sender: TObject);
//begin
//     SetTipoGrafica(eTipoGrafica(meBarraVertical_DevEx.Tag));
//end;
//
//procedure TGraficaBase_DevEx.BarraHorizontal_DevExClick(Sender: TObject);
//begin
//     SetTipoGrafica(eTipoGrafica(BarraHorizontal_DevEx.Tag));
//end;
//
//procedure TGraficaBase_DevEx.mePie_DevExClick(Sender: TObject);
//begin
//     SetTipoGrafica(eTipoGrafica(mePie_DevEx.Tag));
//end;
//
//procedure TGraficaBase_DevEx.meLinea_DevExClick(Sender: TObject);
//begin
//     SetTipoGrafica(eTipoGrafica(meLinea_DevEx.Tag));
//end;
//
//procedure TGraficaBase_DevEx.meArea_DevExClick(Sender: TObject);
//begin
//      SetTipoGrafica(eTipoGrafica(meArea_DevEx.Tag));
//end;
//
//procedure TGraficaBase_DevEx.mePuntos_DevExClick(Sender: TObject);
//begin
//     SetTipoGrafica(eTipoGrafica(mePuntos_DevEx.Tag));
//end;
//
//procedure TGraficaBase_DevEx.BSeriesConfig_DevExClick(Sender: TObject);
//begin
//      SetChartEditorOptions( 'Editar Series',
//                            [cetGeneral, cetAxis, cetTitles, cetLegend, cetPanel, cetPaging, cetWalls, cet3D],
//                            [ceAdd, ceDelete, ceChange, ceClone, ceTitle] );
//end;

//DevEx(@am): Menu Campos
//procedure TGraficaBase_DevEx.BSeries_DevExChange(Sender: TObject);
//begin
//     if (Bseries_DevEx.Items.Count > 0) AND (Bseries_DevEx.ItemIndex> -1) then
//        if( Bseries_DevEx.CurText = K_TODOS)then
//        begin
//             if FPuedeGraficar then
//                MuestraTodasSeries;
//        end
//        else
//             MuestraSerie(accedeListaCampos);
//end;

//DevEx(@am): Menu de Descripciones
//procedure TGraficaBase_DevEx.BDescripcion_DevExChange(Sender: TObject);
//begin
//     if (BDescripcion_DevEx.Items.Count > 0) AND (BDescripcion_DevEx.ItemIndex> -1) then
//        if FPuedeGraficar then
//           MuestraDesc(AccedeListaDesc);
//end;
//
//procedure TGraficaBase_DevEx.FormCreate(Sender: TObject);
//begin
//     //Listas de Campos y Descripciones
//     if listaCampos = NIL then
//        listaCampos := TList.Create;
//     if listaDesc = NIL then
//        listaDesc := TList.Create;
//     FDescIndex := 0;
//
//    {***(@am): Se cambio la creacion del objeto ChartEditor a tiempo de ejecucion porque solo las maquinas
//               con licencia PRO de TeeChar pueden utilizar este componente.***}
//    {$IFDEF TeeChartPro}
//      ChartEditor := TChartEditor.Create(self);
//      ChartEditor.Chart := Chart;
//    {$ENDIF}
//end;


end.
