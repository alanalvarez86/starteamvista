inherited ZCambiaClave_DevEx: TZCambiaClave_DevEx
  Left = 337
  Top = 253
  ActiveControl = OPassWord
  Caption = 'Nueva Clave de Usuario'
  ClientHeight = 121
  ClientWidth = 274
  OldCreateOrder = True
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object NPassWordLBL: TLabel [0]
    Left = 6
    Top = 36
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Caption = '&Nueva Clave:'
    FocusControl = NPassWord
  end
  object NPassWord2LBL: TLabel [1]
    Left = 7
    Top = 62
    Width = 64
    Height = 13
    Alignment = taRightJustify
    Caption = '&Confirmaci'#243'n:'
    FocusControl = NPassWord2
  end
  object Label1: TLabel [2]
    Left = 8
    Top = 10
    Width = 63
    Height = 13
    Alignment = taRightJustify
    Caption = 'Clave &Actual:'
    FocusControl = OPassWord
  end
  inherited PanelBotones: TPanel
    Top = 88
    Width = 274
    Height = 33
    BevelOuter = bvNone
    TabOrder = 3
    inherited OK_DevEx: TcxButton
      Left = 109
      Top = 0
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 189
      Top = 0
    end
  end
  object NPassWord: TMaskEdit
    Left = 73
    Top = 32
    Width = 197
    Height = 21
    Hint = 'Clave Nueva A Usar'
    MaxLength = 14
    ParentShowHint = False
    PasswordChar = '*'
    ShowHint = True
    TabOrder = 1
  end
  object NPassWord2: TMaskEdit
    Left = 73
    Top = 58
    Width = 197
    Height = 21
    Hint = 'Confirmaci'#243'n de la Clave Nueva'
    MaxLength = 14
    ParentShowHint = False
    PasswordChar = '*'
    ShowHint = True
    TabOrder = 2
  end
  object OPassWord: TMaskEdit
    Left = 73
    Top = 6
    Width = 197
    Height = 21
    Hint = 'Clave Actual del Usuario'
    MaxLength = 14
    ParentShowHint = False
    PasswordChar = '*'
    ShowHint = True
    TabOrder = 0
  end
end
