unit ZConfirmaVerificacion;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     ZBaseDlgModal;

type
  TConfirmaVerificacion = class(TZetaDlgModal)
    CrearLista: TBitBtn;
    Mensaje: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ConfirmaVerificacion: TConfirmaVerificacion;

function ConfirmVerification: TModalResult;

implementation

{$R *.DFM}

function ConfirmVerification: TModalResult;
begin
     with TConfirmaVerificacion.Create( Application ) do
     begin
          try
             ShowModal;
             Result := ModalResult;
          finally
                 Free;
          end;
     end;
end;

procedure TConfirmaVerificacion.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ADUANAS}
     Mensaje.Caption := Format(Mensaje.Caption, ['Pedimentos']);
     {$else}
     Mensaje.Caption := Format(Mensaje.Caption, ['Empleados']);
     {$endif}
end;

end.
