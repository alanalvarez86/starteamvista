unit ZBasicoNavBarShell;

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, ComCtrls, Menus, ImgList, ActnList, StdCtrls, Buttons, {$ifndef VER130}Variants,{$endif}
     {$ifdef TRESS_DELPHIXE5_UP}System.Types,{$endif}
     ZBaseShell,
     ZBaseConsulta,
     ZetaCommonLists,
     ZetaClientDataSet,
     ZetaTipoEntidad,
     ZetaDespConsulta,
     dxSkinsCore, 
     dxSkinsdxBarPainter, dxBar, dxRibbon, cxLookAndFeels, dxSkinsForm,
     cxGraphics, cxControls, cxLookAndFeelPainters, dxRibbonSkins,
     dxSkinsdxRibbonPainter, cxClasses, dxSkinsdxNavBarPainter, dxNavBar, dxNavBarCollns,
     dxSkinsDefaultPainters,
     cxLocalization, dxSkinscxPCPainter,
     cxPCdxBarPopupMenu, cxPC, TressMorado2013, cxButtons,
     cxtreeview, dxStatusBar,
     dxRibbonStatusBar, System.Actions, cxStyles, dxSkinsdxStatusBarPainter,
  cxContainer, cxEdit, dxRibbonCustomizationForm, dxBarBuiltInMenu, DGlobal, ZGlobalTress ; //Edit by MP: se incluye el cxTreeview

type
  TBasicoNavBarShell = class;
  TFormManager = class(TObject)
  private
    { Private declarations }
    FShell: TBasicoNavBarShell;
    FFormas: TList;
    FFormaActiva: TBaseConsulta;
    function BorraUna(Forma: TBaseConsulta): Integer;
    function GetFormas( Index: Integer ): TBaseConsulta;
    procedure SetFormaActiva( Value: TBaseConsulta );
  public
    { Public declarations }
    constructor Create( Shell: TBasicoNavBarShell );
    destructor Destroy; override;
    property FormaActiva: TBaseConsulta read FFormaActiva write SetFormaActiva;
    property Formas[ Index: Integer ]: TBaseConsulta read GetFormas;
    function BorraFormaActiva: TBaseConsulta;
    function GetForm( InstanceClass: TBaseConsultaClass ): TBaseConsulta;
    function HayFormas: Boolean;
    procedure BorraTodas;
    procedure CambiaFormaActiva( Value: TBaseConsulta );
    procedure DesconectaFormaActiva;
  end;                                  
  TBasicoNavBarShell = class(TBaseShell)
    PanelConsulta: TPanel;
    EscogeImpresora: TPrinterSetupDialog;
    ActionList: TActionList;
    _A_Imprimir: TAction;
    _A_ImprimirForma: TAction;
    _A_ConfigurarImpresora: TAction;
    _A_Servidor: TAction;
    _A_SalirSistema: TAction;
    _E_Agregar: TAction;
    _E_Borrar: TAction;
    _E_Modificar: TAction;
    _E_Refrescar: TAction;
    _E_BuscarCodigo: TAction;
    _V_Cerrar: TAction;
    _V_CerrarTodas: TAction;
    _V_ExpanderArbol: TAction;
    _V_CompactarArbol: TAction;
    _V_BuscarForma: TAction;
    _V_Calculadora: TAction;
    _H_Contenido: TAction;
    _H_Glosario: TAction;
    _H_UsoTeclado: TAction;
    _H_ComoUsarAyuda: TAction;
    _H_AcercaDe: TAction;
    _A_OtraEmpresa: TAction;
    _A_ClaveAcceso: TAction;
    _H_NuevoTress: TAction;
    _A_Exportar: TAction;
    DevEx_BarManager: TdxBarManager;
    AEmpresas: TdxBar;
    AImprimir: TdxBar;
    AServer: TdxBar;
    ASalir: TdxBar;
    EOperaciones: TdxBar;
    VCerrar: TdxBar;
    VArbolForma: TdxBar;
    VCalculadora: TdxBar;
    HContenido: TdxBar;
    HAcerca: TdxBar;
    TabArchivo_btnOtraEmpresa: TdxBarLargeButton;
    TabArchivo_btnImprimir: TdxBarButton;
    TabArchivo_btnImprimirForma: TdxBarButton;
    TabArchivo_btnImpresora: TdxBarButton;
    TabArchivo_btnClaveAcceso: TdxBarLargeButton;
    TabArchivo_btnSalir: TdxBarLargeButton;
    ERefrescar_btnRefrescar: TdxBarLargeButton;
    VCerrar_btnCerrar: TdxBarLargeButton;
    VCerrar_btnCerrarTodas: TdxBarLargeButton;
    VArbolForma_btnMostrarArbol: TdxBarLargeButton;
    VArbolForma_btnArbolOriginal: TdxBarButton;
    VArbolForma_btnBuscarForma: TdxBarLargeButton;
    VArbolForma_btnExpanderArbol: TdxBarLargeButton;
    VArbolForma_btnCompactarArbol: TdxBarLargeButton;
    VCalculadora_btnCalculadora: TdxBarLargeButton;
    HContenido_btnContenido: TdxBarButton;
    HContenido_NuevoEnTress: TdxBarButton;
    HContenido_Glosario: TdxBarButton;
    HContenido_btnUsoTeclado: TdxBarLargeButton;
    HContenido_btnComoUsarAyuda: TdxBarLargeButton;
    HAcerca_btnAcercaDe: TdxBarLargeButton;
    TabArchivo_btnServidor: TdxBarLargeButton;
    TabArchivo: TdxRibbonTab;
    TabEditar: TdxRibbonTab;
    TabVentana: TdxRibbonTab;
    TabAyuda: TdxRibbonTab;
    _V_MostrarNavBar: TAction;
    _E_Exportar_DevEx: TAction;
    DevEx_cxLocalizer: TcxLocalizer;
    PanelNavBar: TPanel;
    PanelBuscaNavBar: TPanel;
    editBusca_DevEx: TEdit;
    DevEx_NavBar: TdxNavBar;
    ClientAreaPG_DevEx: TcxPageControl;
    Image16: TcxImageList;
    EOperaciones_btnAgregar: TdxBarLargeButton;
    EOperaciones_btnBorrar: TdxBarLargeButton;
    EOperaciones_btnModificar: TdxBarLargeButton;
    EOperaciones_btnExportar: TdxBarLargeButton;
    EOperaciones_btnBuscarCodigo: TdxBarLargeButton;
    BuscarFormaBtn_DevEx: TcxButton;
    ClientAreaPopup: TPopupMenu;
    ClientAreaCerrar: TMenuItem;
    ClientAreaCerrarTodas: TMenuItem;
    N57: TMenuItem;
    ClientAreaBuscarCodigo: TMenuItem;
    ArbolImages: TImageList;
    SmallImageList: TcxImageList;
    DevEx_BandaValActivos: TPanel;
    RibbonSmallImages: TcxImageList;
    RibbonsLargeImages: TcxImageList;
    NavBarLarge: TcxImageList;
    NavBarSmall: TcxImageList;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormaActivaDefocus(Sender: TObject);
    procedure FormaActivaFocus(Sender: TObject);
    procedure _SinImplementar(Sender: TObject);
    procedure _A_OtraEmpresaExecute(Sender: TObject);
    procedure _A_ImprimirExecute(Sender: TObject);
    procedure _A_ImprimirFormaExecute(Sender: TObject);
    procedure _A_ConfigurarImpresoraExecute(Sender: TObject);
    procedure _A_ServidorExecute(Sender: TObject);
    procedure _A_SalirSistemaExecute(Sender: TObject);
    procedure _A_ClaveAccesoExecute(Sender: TObject);
    procedure _E_AgregarExecute(Sender: TObject);
    procedure _E_BorrarExecute(Sender: TObject);
    procedure _E_ModificarExecute(Sender: TObject);
    procedure _E_RefrescarExecute(Sender: TObject);
    procedure _E_BuscarCodigoExecute(Sender: TObject);
    procedure _E_BuscarCodigoUpdate(Sender: TObject);
    procedure _V_CerrarExecute(Sender: TObject);
    procedure _V_CerrarTodasExecute(Sender: TObject);
    procedure _V_ExpanderArbolExecute(Sender: TObject);
    procedure _V_CompactarArbolExecute(Sender: TObject);
    procedure _V_BuscarFormaExecute(Sender: TObject);
    procedure _V_CalculadoraExecute(Sender: TObject);
    procedure _H_ContenidoExecute(Sender: TObject);
    procedure _H_GlosarioExecute(Sender: TObject);
    procedure _H_UsoTecladoExecute(Sender: TObject);
    procedure _H_ComoUsarAyudaExecute(Sender: TObject);
    procedure _H_AcercaDeExecute(Sender: TObject);
    procedure _HayEmpresaAbierta(Sender: TObject);
    procedure editBuscaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure _HayFormaAbierta(Sender: TObject);
    procedure EscogeImpresoraClose(Sender: TObject);
    procedure EscogeImpresoraShow(Sender: TObject);
    procedure _A_ServidorUpdate(Sender: TObject);
    procedure _H_NuevoTressExecute(Sender: TObject);
    procedure _A_ExportarExecute(Sender: TObject);
    procedure RegistroClick(Sender: TObject);
    //DevEx Aroblitos DblClick
    procedure ArbolitoDblClick(Sender: TObject); dynamic;
    procedure ArbolitoKeyPress(Sender: TObject; var Key: Char); dynamic;
    procedure DevEx_NavBarNavigationPaneCollapsed(Sender: TObject);
    procedure DevEx_NavBarNavigationPaneExpanded(Sender: TObject);
    procedure _V_MostrarNavBarExecute(Sender: TObject);
    procedure _E_Exportar_DevExExecute(Sender: TObject);
    procedure ClientAreaPG_DevExChange(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
    { Private declarations }
    FFormManager: TFormManager;

    //DevEx (by am): exponader y contraer arbolito activo
    FCandadoTab: Boolean;
    {*** US 14266: Las gr�ficas se refrescan cada vez que cambia el empleado activo ***}
    FActualizarGraficasInicio: Boolean;
    {*** FIN ***}
    procedure ExpandeArbolito;
    procedure CompactaArbolito;
  protected
    { Protected declarations }
    FHelpGlosario: String;
    FCambiaServidor :Boolean;
    FCerrandoSistema:Boolean;
    property PuedeCambiarServidor : Boolean read FCambiaServidor Write FCambiaServidor;
    function HayFormaAbierta: Boolean;
    //DevEx (by am): Agregado para accesar el valor de HayEmpresaAbierta desde los Shells
    function HayEmpresaAbierta: Boolean;
    procedure SetArbolitosLength(Length:Integer);
    procedure AbreFormaConsulta( const TipoForma: eFormaConsulta );
    procedure CierraFormaActiva;
    procedure CierraFormaTodas;
    procedure ShowNavBar(const lVisible: Boolean);
    procedure DoCloseAll; override;
    procedure HabilitaControles; override;
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado); virtual;
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado); virtual;
    {$endif}
    procedure CambioValoresActivos(const Valor: TipoEstado);
    procedure CargaDerechos; override;
    //DevEx (by am)
    procedure CargaTraducciones; Virtual;
    //
    {*** US 14266: Las gr�ficas se refrescan cada vez que cambia el empleado activo ***}
    procedure SetActualizarGraficasInicio( const lActualizaGraficaInicio: Boolean );
    function GetActualizarGraficasInicio: Boolean;
    {*** FIN ***}
  public
    { Public declarations }
    //DevEx declaradcion de Aroblitos
    Arbolitos : array of TcxTreeView; //Edit by MP: se cambian a TcxTreeview
    ArbolitoUsuario : TcxTreeView;//Edit by MP: se cambian a TcxTreeview
    function FormaActiva: TBaseConsulta;
    procedure ReconectaFormaActiva;
    procedure CreaArbol( const lEjecutar: Boolean );
   //DevEx
    procedure CargaVista; Virtual;
    {$ifdef MULTIPLES_ENTIDADES}
    procedure SetDataChange(const Entidades: array of TipoEntidad); virtual;
    {$else}
    procedure SetDataChange(const Entidades: ListaEntidades); virtual;
    {$endif}
    //DevEx Creacion de NavBar y sus Arbolitos
    //DevEx (by am): Metodo encargado de cargar los nodos a la NavBar
    procedure CreaNavBar; override;
    //DevEx (by am): Metodo encargado de crear en tiempo de ejecucion los arbolitos para cada grupo de la navbar
    procedure CreaArbolitos; virtual;
    //DevEx : Agregado para alpicar BestFit desde los botones fuera del tab principal
    procedure InvocaBestFit ( NombreForma:String );
    procedure LimpiaGruposNavBar; override;
    function GetNavBarImageList: TcxImageList; override;
    {*** US 14266: Las gr�ficas se refrescan cada vez que cambia el empleado activo ***}
    property ActualizarGraficasInicio: Boolean read GetActualizarGraficasInicio write SetActualizarGraficasInicio;
  end;

var
  BasicoNavBarShell: TBasicoNavBarShell;
  sCambioEdit: String;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaClientTools,
     ZetaMessages,
     ZArbolTools,
     ZArbolTress,
     ZetaBusqueda_DevEx,
     ZetaDialogo,
     ZAccesosMgr,
     ZetaAcercaDe_DevEx,
     ZetaRegistryCliente,
     ZAccesosTress,
     ZNavBarTools,
     DCliente;

{$R *.DFM}

{ ************** TFormaMgr ******************* }

constructor TFormManager.Create( Shell: TBasicoNavBarShell );
begin
     FShell := Shell;
     FFormas := TList.Create;
     FFormaActiva := nil;
end;

destructor TFormManager.Destroy;
begin
     FFormaActiva := nil;
     FFormas.Free;
     inherited;
end;



function TFormManager.HayFormas: Boolean;
begin
     Result := ( FFormas.Count > 0 );
end;

function TFormManager.GetFormas(Index: Integer): TBaseConsulta;
begin
     Result := TBaseConsulta( FFormas.Items[ Index ] );
end;

function TFormManager.GetForm( InstanceClass: TBaseConsultaClass ): TBaseConsulta;
var
   i: Integer;
begin
     Result := nil;
     with FFormas do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if Formas[ i ].ClassNameIs( InstanceClass.ClassName ) then
               begin
                    Result := Formas[ i ];
                    Break;
               end;
          end;
     end;
end;

procedure TFormManager.DesconectaFormaActiva;
begin
     if ( FFormaActiva <> nil ) then
     begin
          with FFormaActiva do
          begin
               DoDisconnect;
          end;
     end;
end;

procedure TFormManager.CambiaFormaActiva( Value: TBaseConsulta );
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        {*** US 14266: Las gr�ficas se refrescan cada vez que cambia el empleado activo ***}
        FShell.ActualizarGraficasInicio := True;
        {*** FIN ***}
        DesconectaFormaActiva;
        with Value do
        begin
             Reconnect;
        end;
        {*** US 14266: Las gr�ficas se refrescan cada vez que cambia el empleado activo ***}
        FShell.ActualizarGraficasInicio := False;
        {*** FIN ***}
        FormaActiva := Value;
     finally
        Screen.Cursor := oCursor;
     end;
end;

procedure TFormManager.SetFormaActiva( Value: TBaseConsulta );
begin
     with FFormas do
     begin
          if ( Value <> nil ) and ( IndexOf( Value ) < 0 ) then
             Add( Value );
     end;
     FFormaActiva := Value;
     if ( FFormaActiva <> nil ) then
     begin

               with FShell.ClientAreaPG_DevEx do
               begin
                    HelpContext:= FFormaActiva.HelpContext;
                    FFormaActiva.Pestania_DevEx.AbreForma;
                    SetFocus;
               end;

     end;
end;

function TFormManager.BorraUna( Forma: TBaseConsulta ): Integer;
begin
     with Forma do
     begin
          DoDisconnect;
          //DevEx
          with Pestania_DevEx do
          begin
               CierraForma;
               Free;
          end;
     end;
     with FFormas do
     begin
          Result := Remove( Forma );
     end;
end;

procedure TFormManager.BorraTodas;
var
   i: Integer;
begin
     with FFormas do
     begin
          for i := ( Count - 1 ) downto 0 do
          begin
               BorraUna( Formas[ i ] );
          end;
     end;
     FFormaActiva := nil;
end;

function TFormManager.BorraFormaActiva: TBaseConsulta;
var
   i: Integer;
begin
     if ( FFormaActiva = nil ) then
        Result := nil
     else
     begin
          i := BorraUna( FFormaActiva );
          FFormaActiva := nil;
          with FFormas do
          begin
               { Determinar candidato a Forma Activa }
               if ( Count = 0 ) then
                  Result := nil
               else
                   if ( Count = i ) then
                      Result := Formas[ i - 1 ]
                   else
                       Result := Formas[ i ];
          end;
     end;
end;

{ ZBaseArbolShell }

procedure TBasicoNavBarShell.FormCreate(Sender: TObject);
begin
     // Ayuda de Componentes
//** 
	{Arbol.HelpContext                     := H00003_Explorador;
     ArchivoOtraEmpresa.HelpContext        := H00006_Menu_empresas;
     ArchivoCatalogoUsuarios.HelpContext   := H80813_Usuarios;
     ArchivoImprimir.HelpContext           := H00008_Archivo_imprimir;
     ArchivoImprimirForma.HelpContext      := H00009_Imprimir_forma;
     ArchivoImpresora.HelpContext          := H00010_Impresora;
     Editar.HelpContext                    := H00011_Menu_Editar;
     Ventana.HelpContext                   := H00013_Menu_ventana;
}
    // DevEx
    {  //**pendiente asignarles los helpcontext correctos
     TabArchivo_btnOtraEmpresa.HelpContext        := H00006_Menu_empresas;
     TabArchivo_btnCatalogoUsuarios.HelpContext   := H80813_Usuarios;
     TabArchivo_btnImprimir.HelpContext           := H00008_Archivo_imprimir;
     TabArchivo_btnImprimirForma.HelpContext      := H00009_Imprimir_forma;
     TabArchivo_btnImpresora.HelpContext          := H00010_Impresora;
     }
     EscogeImpresora.HelpContext           := H00010_Impresora;
     FHelpGlosario := VACIO;
     //
     FFormManager := TFormManager.Create( Self );
     inherited;
     WindowState:= wsMaximized;
     FCambiaServidor := true;

     DevEx_ShellRibbon.ShowTabHeaders := True;
     FCerrandoSistema:= False;

end;

procedure TBasicoNavBarShell.FormDestroy(Sender: TObject);
begin
     FFormManager.Free;
     inherited;
end;

//DevEx (by am): Editato para utilizar el nuevo componente de PageControl
procedure TBasicoNavBarShell.AbreFormaConsulta( const TipoForma: eFormaConsulta);
var
   oPropConsulta: TPropConsulta;
   oForma: TBaseConsulta;
   InstanceClass: TBaseConsultaClass;
   Pestania_DevEx: TTabForm_DevEx; //DevEx
   lNueva: Boolean;
begin
     oPropConsulta := GetPropConsulta( TipoForma );
     if ZAccesosMgr.CheckDerecho( oPropConsulta.IndexDerechos, K_DERECHO_CONSULTA ) then
     begin
          //Al abrir una forma de consulta, el tab seleccionado debe ser el de Editar (Caption: Principal)
          FCerrandoSistema:= False;
          DevEx_ShellRibbon.ActiveTab := TabEditar;

          InstanceClass := oPropConsulta.ClaseForma;
          if ( InstanceClass = nil ) then
             ZetaDialogo.zError( '� Error al crear forma !', '� Clase de ventana no implementada !', 0 )
          else
          begin
               with FFormManager do
               begin
                    oForma := GetForm( InstanceClass );
                    if ( oForma = nil ) then
                    begin
                         try
                            oForma := InstanceClass.Create( Self ) as TBaseConsulta;
                            with oForma do
                            begin
                                 IndexDerechos := oPropConsulta.IndexDerechos;
                            end;
                         except
                               on Error: Exception do
                               begin
                                    ZetaDialogo.zError( '� Error al crear forma !', '� Ventana no pudo ser creada !', 0 );
                                    oForma := nil;
                               end;
                         end;
                         lNueva := True;
                    end
                    else
                        lNueva := False;
                    if ( oForma <> nil ) then
                    begin
                         if lNueva then
                         begin
                              DesconectaFormaActiva;
                              if oForma.DoConnect then
                              begin
                                       if (InstanceClass.ClassName = 'TFonacotDatosTot_DevEx')  then
                                       begin
                                             if (Global.GetGlobalString (K_GLOBAL_FONACOT_PRESTAMO) <> VACIO) then
                                             begin
                                                      Pestania_DevEx := TTabForm_DevEx.Create( Self );
                                                      Pestania_DevEx.Forma := oForma;
                                                      with Pestania_DevEx do
                                                      begin
                                                           FCandadoTab := True;
                                                           PageControl := ClientAreaPG_DevEx;
                                                           FCandadoTab := False;
                                                           Visible := True;
                                                      end;
                                                      FCandadoTab := True;
                                                      ClientAreaPG_DevEx.Properties.ActivePage := Pestania_DevEx;
                                                      FCandadoTab := False;
                                                 oForma.Show;
                                                 FormaActiva := oForma;
                                             end;
                                       end
                                       else
                                       begin
                                                //DevEx
                                                Pestania_DevEx := TTabForm_DevEx.Create( Self );
                                                Pestania_DevEx.Forma := oForma;
                                                with Pestania_DevEx do
                                                begin
                                                     {***El candado es para que el evento Change del PageControl no ejecute el
                                                         CambiaForma cuando se le asigna al PageControl un Tab o cuando por codigo
                                                         cambiamos el taba Activo.***}
                                                     FCandadoTab := True; //Cierra candado
                                                     PageControl := ClientAreaPG_DevEx;
                                                     FCandadoTab := False; //Abre candado
                                                     Visible := True;
                                                end;
                                                FCandadoTab := True; //Cierra candado
                                                ClientAreaPG_DevEx.Properties.ActivePage := Pestania_DevEx;
                                                FCandadoTab := False; //Abre candado
                                           oForma.Show;
                                           FormaActiva := oForma;
                                       end;

                              end
                              else
                                  oForma.Free;
                         end
                         else
                             CambiaFormaActiva( oForma );
                    end;
               end;
          end;
     end
     else
         ZetaDialogo.zError( '� Error al crear forma !', '� No tiene derechos de consulta para esta ventana !', 0 );
end;

function TBasicoNavBarShell.HayFormaAbierta: Boolean;
begin
     Result :=  FFormManager.HayFormas;
end;

//DevEx (by am): Agregado para accesar el valor de HayEmpresaAbierta desde los Shells
function TBasicoNavBarShell.HayEmpresaAbierta: Boolean;
begin
      Result := EmpresaAbierta;
end;

function TBasicoNavBarShell.FormaActiva: TBaseConsulta;
begin
     Result := FFormManager.FormaActiva;
end;

procedure TBasicoNavBarShell.ReconectaFormaActiva;
begin
     with FFormManager do
     begin
          if ( FormaActiva <> nil ) then
             FormaActiva.ReConnect;
     end;
end;

//DevEx (by am): Editado para crear el arbol o la navbar segun la vista
procedure TBasicoNavBarShell.CierraFormaActiva;
var
   oForma: TBaseConsulta;
begin
     with FFormManager do
     begin
          oForma := BorraFormaActiva;
          if ( oForma <> nil ) then
          begin
               oForma.Reconnect;
               FormaActiva := oForma;
          end;
          if not HayFormas then
          begin
               ShowNavBar( True );
          end;
     end;
end;

procedure TBasicoNavBarShell.CierraFormaTodas;
begin
     with Screen do
     begin
          Cursor := crHourglass;
          try
             FCerrandoSistema := True;
             with FFormManager do
             begin
                  BorraTodas;
                  if (not HayFormas )and (Assigned(dmcliente.cdsUsuario.findfield('US_CODIGO'))) then
                     ShowNavBar( True );
             end;
          finally
                 Cursor := crDefault;
          end;
     end;
end;

//DevEx (by am): Limpia ambos componentes
procedure TBasicoNavBarShell.DoCloseAll;
begin
     DevEx_NavBar.Groups.Clear;
     inherited DoCloseAll;
end;

procedure TBasicoNavBarShell.HabilitaControles;
begin
     inherited HabilitaControles;
end;

procedure TBasicoNavBarShell.CreaArbol(const lEjecutar: Boolean);
//**PENDIENTE MODIFICACION PARA QUE REFRESQUE ARBOL VISTA NUEVA
{var
       oArbolMgr: TArbolMgr;
}
begin
 {//**
     oArbolMgr := TArbolMgr.Create( Arbol );
     with oArbolMgr do
     begin
          try

             with Arbol.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     // Aqu� se agrega el arbol definido por el usuario
                     ZArbolTress.CreaArbolUsuario( oArbolMgr, '' );
                     // Si no hubo selecci�n, selecciona el Primero y lo Abre
                     if ( Arbol.Selected = nil ) then
                     begin
                          with Arbol.Items[ 0 ] do
                          begin
                               Selected := True;
                          end;
                     end;
                     // Se ejecuta el Nodo default Seleccionado por el Usuario
                     if lEjecutar then
                     begin
                          if EsFolder( Arbol.Selected ) then
                             Arbol.Selected.Expand( False )
                          else
                             ArbolDblClick( Arbol );
                     end;
                  finally
                     EndUpdate;
                  end;
             end;
          finally
             Free;
          end;
     end;
       }
end;
{******NavBar******}
//DevEx (by am): Creacion de Arbolitos para la NavBar
procedure TBasicoNavBarShell.CreaArbolitos;
var
   iArbolito: Integer;
begin
  {
   //Arbolito para NavBar del Usuario
     ArbolitoUsuario := TcxTreeView.Create(nil);////Edit by MP: Creacion de arbol TcxTreeview
     with ArbolitoUsuario do
     begin
          Images := SmallImageList;
          //Asignacion de todos los eventos que maneja el Arbol a los Arbolitos
          //Utilizados solo por tvActual
          OnDblClick := ArbolitoDblClick;
          OnKeyPress := ArbolitoKeyPress;
          //Utilizados para cualquier vista
          OnEnter    := FormaActivaDefocus;
          OnExit     := FormaActivaFocus;
          ReadOnly := True;
          Visible := false;
          //Estilos
          Style.LookAndFeel.SkinName := DevEx_SkinCOntroller.SkinName; //Edit by MP:use de skin//no funciona
     end;
   }
     //Arbolitos para NavBar del Sistema
     for iArbolito := 0 to High(Arbolitos) do
     begin
          Arbolitos[iArbolito] := TcxTreeView.Create(nil);//Edit by MP: se crea el tcxtreeview
          with Arbolitos[iArbolito] do
          begin
               Visible := False;
               Images := SmallImageList;
               {Asignacion de todos los eventos que maneja el Arbol a los Arbolitos}
               //Utilizados solo por tvActual
               OnDblClick := ArbolitoDblClick;
               OnKeyPress := ArbolitoKeyPress;
               //Utilizados para cualquier vista
               Style.LookAndFeel.SkinName := DevEx_SkinCOntroller.SkinName;//Edit by MP:seleccion de skin//no funciona
               OnEnter    := FormaActivaDefocus;
               OnExit     := FormaActivaFocus;
               ReadOnly := True;
               //Estilo
               Style.LookAndFeel.SkinName := DevEx_SkinCOntroller.SkinName;//Edit by MP:seleccion de skin//no funciona
          end;
     end;
end;
procedure TBasicoNavBarShell.CreaNavBar;
var
   IndiceArbolitos:integer;
   oNavBarMgr: TNavBarMgr;
begin
     // Varibale Global que guardara el valor anterior de editBusca para la NavBar
     sCambioEdit := ' ';
     //Creacion de arbolitos para la NavBar
     CreaArbolitos;
     //Creacion NavBar
     oNavBarMgr := TNavBarMgr.Create(DevEx_NavBar);
     with oNavBarMgr do
     begin
          try
             with NavBar.Groups do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     //ZArbolTress.CreaNavBarUsuario(oNavBarMgr, '',Arbolitos, ArbolitoUsuario);
                     ZArbolTress.CreaNavBarDefault(oNavBarMgr, Arbolitos);
                   //**  arbolitoUsuario.Visible:=true;
                     for IndiceArbolitos :=0 to high(Arbolitos) do
                     begin
                         arbolitos[IndiceArbolitos].Visible:=true;
                     end;
                  finally
                         EndUpdate;
                  end;
             end;
          finally
             Free;
          end;
     end;
end;

procedure TBasicoNavBarShell.ShowNavBar(const lVisible: Boolean);
begin
     DevEx_NavBar.OptionsBehavior.NavigationPane.Collapsed := not lVisible;
     with _V_MostrarNavBar do
     begin
          Checked := lVisible;
          if Checked then
          begin
               Hint := 'Esconder Barra de Formas';
               Caption := 'Esconde&r Barra';
          end
          else
          begin
               Hint := 'Mostrar Barra de Formas';
               Caption := 'Mostra&r Barra';
          end;
     end;
end;
procedure TBasicoNavBarShell.CambioValoresActivos(const Valor: TipoEstado);
begin
     NotifyDataChange( [], Valor );
     if HayFormaAbierta then
     begin
          //DevEx: Para que haga el BestFit con el cambio de empleado o TipoAhorro solo para los
          //gris que manejan valores activos.
          if ( FormaActiva.ValorActivo1.Visible or FormaActiva.ValorActivo2.Visible) then
             FormaActiva.DoBestFit;
     end;
end;


{$ifdef MULTIPLES_ENTIDADES}
procedure TBasicoNavBarShell.SetDataChange(const Entidades: array of TipoEntidad);
{$else}
procedure TBaseNavBarShell.SetDataChange(const Entidades: ListaEntidades);
{$endif}
begin
     NotifyDataChange( Entidades, stNinguno );
end;

{$ifdef MULTIPLES_ENTIDADES}
procedure TBasicoNavBarShell.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
{$else}
procedure TBaseNavBarShell.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
{$endif}
begin
     ReconectaFormaActiva;
end;

procedure TBasicoNavBarShell.EscogeImpresoraShow(Sender: TObject);
begin
     inherited;                                 
     LogoffTimer.Enabled := False;
end;

procedure TBasicoNavBarShell.EscogeImpresoraClose(Sender: TObject);
begin
     inherited;
     LogoffTimer.Enabled := True;
end;

{DevEx: Carga Traduccion de textos}
procedure TBasicoNavBarShell.CargaTraducciones;
begin
     //Para que lo herede
end;

//**Carga vista actual
procedure TBasicoNavBarShell.CargaVista;
begin
       // Enable and Disable Actions
     _V_MostrarNavBar.Visible := True;

     //2. Mostrar nuevos controles y activar skin
     DevEx_SkinController.NativeStyle := False; //Al principio para evitar ver controles sin estilo
     //Especificar estilo para el Ribbon
     DevEx_ShellRibbon.ColorSchemeName :=  DevEx_SkinController.SkinName;
     //Mostrar elementos para la vista nueva
     DevEx_ShellRibbon.Visible := True;

     DevEx_ShellRibbon.ActiveTab := TabEditar; //Asigna Tab Default
     PanelNavBar.Visible := True;
     ClientAreaPG_DevEx.Visible := True;
end;
{ Action List }
procedure TBasicoNavBarShell._SinImplementar(Sender: TObject);
begin
     zInformation( 'Estamos trabajando...', 'Esta opci�n no ha sido implementada', 0 );
end;

procedure TBasicoNavBarShell._A_ServidorUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := ClientRegistry.CanWrite and FCambiaServidor;
end;

procedure TBasicoNavBarShell._HayEmpresaAbierta(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta;
end;

procedure TBasicoNavBarShell._HayFormaAbierta(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := HayFormaAbierta;
end;

procedure TBasicoNavBarShell._E_BuscarCodigoUpdate(Sender: TObject);
begin
     TAction( Sender ).Enabled := HayFormaAbierta and FormaActiva.CanLookup;
end;

procedure TBasicoNavBarShell._A_OtraEmpresaExecute(Sender: TObject);
begin
     AbreEmpresa( False );
end;

procedure TBasicoNavBarShell._A_ImprimirExecute(Sender: TObject);
begin
     FormaActiva.DoPrint;
end;

procedure TBasicoNavBarShell._A_ImprimirFormaExecute(Sender: TObject);
begin
     FormaActiva.DoPrintForma;
end;

procedure TBasicoNavBarShell._A_ExportarExecute(Sender: TObject);
begin
     inherited;
     FormaActiva.DoExportar;
end;

procedure TBasicoNavBarShell._A_ConfigurarImpresoraExecute(Sender: TObject);
begin
     EscogeImpresora.Execute;
end;

procedure TBasicoNavBarShell._A_ClaveAccesoExecute(Sender: TObject);
begin
     CambiaClaveUsuario;
end;

procedure TBasicoNavBarShell._A_ServidorExecute(Sender: TObject);
begin
     {DevEx (by am): Dependiendo de la vista se invoca la forma de CambiaServidor.
     Esta llamada solo se hace desde el boton Cambia Servidor/Usuario en el Shell
     no desde el LogIn}
        CambiaServidor;
end;

procedure TBasicoNavBarShell._A_SalirSistemaExecute(Sender: TObject);
begin
     Close;
end;

procedure TBasicoNavBarShell._E_AgregarExecute(Sender: TObject);
begin
     FormaActiva.DoInsert;
end;

procedure TBasicoNavBarShell._E_BorrarExecute(Sender: TObject);
begin
     FormaActiva.DoDelete;
end;

procedure TBasicoNavBarShell._E_ModificarExecute(Sender: TObject);
begin
     FormaActiva.DoEdit;
end;

procedure TBasicoNavBarShell._E_RefrescarExecute(Sender: TObject);
begin
     if HayFormaAbierta then
     begin
          FormaActiva.DoRefresh;
          //DevEx (by am): Para que haga el BestFit con el Refresh para los grids de la vista actual
          FormaActiva.DoBestFit;
     end;
end;

procedure TBasicoNavBarShell._E_BuscarCodigoExecute(Sender: TObject);
begin
     FormaActiva.DoLookup;
end;

procedure TBasicoNavBarShell._V_CerrarExecute(Sender: TObject);
begin
     CierraFormaActiva;
end;

procedure TBasicoNavBarShell._V_CerrarTodasExecute(Sender: TObject);
begin
     CierraFormaTodas;
end;

//DevEx (by am)
procedure TBasicoNavBarShell.ExpandeArbolito;
var
   ANavBarControl: TdxNavBarGroupControl;
   i: Integer;
begin
ANavBarControl := DevEx_NavBar.ActiveGroup.Control;
    for i := 0 to ANavBarControl.ControlCount - 1 do
    begin
        if ANavBarControl.Controls[i] is TTreeView then
           (ANavBarControl.Controls[i] as TTreeView).FullExpand;
        if ANavBarControl.Controls[i] is TcxTreeView then //edit by MP: si el control es una tcxtreeview tambien se expande
           (ANavBarControl.Controls[i] as TcxTreeView).FullExpand;
    end;
end;

//DevEx (by am)
procedure TBasicoNavBarShell.CompactaArbolito;
var
   ANavBarControl: TdxNavBarGroupControl;
   i: Integer;
begin
ANavBarControl := DevEx_NavBar.ActiveGroup.Control;
    for i := 0 to ANavBarControl.ControlCount - 1 do
        begin
        if ANavBarControl.Controls[i] is TTreeView then
           (ANavBarControl.Controls[i] as TTreeView).FullCollapse;
           if ANavBarControl.Controls[i] is TcxTreeView then //Edit by MP: si el arbol es una tcxTreeview tambien se colapsa
           (ANavBarControl.Controls[i] as TcxTreeView).FullCollapse;
        end;
end;

//DevEx (by am)
procedure TBasicoNavBarShell._V_ExpanderArbolExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        ExpandeArbolito;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBasicoNavBarShell._V_CompactarArbolExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        CompactaArbolito;
     finally
        Screen.Cursor := oCursor;
     end;
end;

//DevEx (by am)
procedure TBasicoNavBarShell._V_BuscarFormaExecute(Sender: TObject);
var
    lEncontro, lReiniciar: Boolean;
    Counter: Integer;
    Arbolito: TcxTreeView; //Edit by MP:el arbolito debe ser tcxTreeview
    ANavBarGroup: TdxNavBarGroup;
    sTexto : String;

begin
     inherited;
     begin
         //Inicializa variables
         Counter := 0;
         lEncontro := FALSE;
         lReiniciar := FALSE;

         if(ActiveControl <> nil) then
         if (( ActiveControl = DevEx_NavBar ) or   //El control activo es la NavBar
         ( ActiveControl = EditBusca_DevEx ) or     //El control activo es la caja de busqueda
         ( (ActiveControl.Parent.ClassName = 'TcxTreeView') and (not strLleno(ActiveControl.Parent.Name)) ) and
         StrLleno( EditBusca_DevEx.Text )) then
         begin
         if ( DevEx_NavBar.OptionsBehavior.NavigationPane.Collapsed ) then
            ShowNavBar( DevEx_NavBar.OptionsBehavior.NavigationPane.Collapsed );
         While ( Counter <= DevEx_NavBar.Groups.Count -1 ) do
         begin
              sTexto := UpperCase( ZetaCommonTools.QuitaAcentos( editBusca_DevEx.Text  ));
              //*** Nueva busqueda O Indica seguir buscando en el siguiente grupo***
              if (  sTexto <> ZNavBarTools.FUltimoTextoGrupo ) then
              begin
                   ANavBarGroup := DevEx_NavBar.Groups[Counter];
                   //Limpia los texto y ultimo grupo
                   ZNavBarTools.BuscaNodoReset;
                   // OJO: sCambioEdit se inicializa cada vez que la navbar es creada
                   if ( sCambioEdit <> editBusca_DevEx.Text ) then
                   begin
                        //Resetea FNodosTextoGrupo
                        ZNavBarTools.FNodosTextoGrupo := 0;
                        sCambioEdit := editBusca_DevEx.Text;
                   end;

                   //Guarda Ultimos Valores
                   ZNavBarTools.FUltimoTextoGrupo := sTexto;
                   ZNavBarTools.FUltimoGrupo := Counter;

                   //Valida si hay texto para buscar
                   if  StrLleno( editBusca_DevEx.Text ) then
                   begin
                        //Busca si la palabra esta en el grupo
                        lEncontro := ZNavBarTools.BuscaTextoGrupo(DevEx_NavBar, ANavBarGroup, sTexto);
                        if ( lEncontro ) then
                        begin
                             //***Incrementar lo encontro en el grupo
                             Inc ( ZNavBarTools.FNodosTextoGrupo ) ;
                             Exit; //Abandona la busqueda porque ya lo encontro
                        end
                        else
                        begin
                             //***Busca en el arbolito del grupo
                             if ANavBarGroup.Control.Controls[0] is TcxTreeView then //Edit by MP: el control del navbar tiene que ser  tcxTreeview
                             begin
                                    Arbolito := (ANavBarGroup.Control.Controls[0] as TcxTreeView); //Edit by MP: el control del navbar se tiene que manejar como tcxTreeview
                                    lEncontro := ZArbolTools.BuscaNodoTextoArbolito( DevEx_NavBar, ANavBarGroup, Arbolito, sTexto );
                                    if ( lEncontro ) then
                                    begin
                                       //Sincroniza valores globales
                                       ZNavBarTools.FUltimoTextoGrupo := ZArbolTools.FUltimoTexto;
                                       //***Incrementar si encontro algo
                                       if ( ZArbolTools.FNodosTexto > 0 ) then
                                          Inc ( ZNavBarTools.FNodosTextoGrupo ) ;
                                       //***
                                       Exit; //Abandona la busqueda porque ya lo encontro
                                  end
                                  else
                                      ZArbolTools.FUltimoTexto := '';
                             end;
                             //***
                        end;
                   end;
              end
              else //*** la palabra ya se habia buscado antes ***
              begin
                   //Definir el grupo en el que se quedo y Buscar en el arbol del grupo
                   Counter := ZNavBarTools.FUltimoGrupo;
                   ANavBarGroup := DevEx_NavBar.Groups[Counter];
                   //***Busca en el arbolito del grupo ya se la primera vez o las siguientes incidencias
                   if ANavBarGroup.Control.Controls[0] is TcxTreeView then //Edit by MP: se busca si el control del navbar es una tcxtreeview
                   begin
                        Arbolito := (ANavBarGroup.Control.Controls[0] as TcxTreeView); //Edit by MP: se maneja el arbolito como tcxtreeview
                        lEncontro := ZArbolTools.BuscaNodoTextoArbolito( DevEx_NavBar, ANavBarGroup, Arbolito, sTexto );
                        if ( lEncontro ) then
                        begin
                             ZNavBarTools.FUltimoTextoGrupo := ZArbolTools.FUltimoTexto;
                             //***Incrementar si encontro algo
                             if ( ZArbolTools.FNodosTexto > 0 ) then
                                Inc ( ZNavBarTools.FNodosTextoGrupo ) ;
                                //***
                             Exit; //Abandona la busqueda porque ya lo encontro
                        end
                        else
                        begin
                             //Si ya no encontro nada en el arbol del grupo, se pasa al siguiente
                             ZNavBarTools.FUltimoTextoGrupo := ' ';
                             ZArbolTools.FUltimoTexto := '';
                        end;
                   end;
              end;
              Inc ( Counter ); //Siguiente grupo
         end;  //Fin While
         end
         else
         begin
              lReiniciar := TRUE; //DevEx(@am):El usuario dio ENTER o Doble Click a la opcion
              lEncontro :=  TRUE;
         end;
         {***DevEx(@am):Si llega a este punto, significa que busco en todo el arbol y no econtro nada o que el usuario dio ENTER o Doble Click a la opcion***}
         //Invocar notificacion de fin de busqueda
         if not lEncontro then
         begin
              //El vacio no existe en el arbol, se valida si la caja tiene texto para que el mensaje no aparezca
              if StrLleno( editBusca_DevEx.Text ) then
              begin
                   ZNavBarTools.NotificacionBusqueda(lEncontro,'Explorador de Formas',editBusca_DevEx.Text);
                   lReiniciar:= TRUE; //Para que enfoque la caja de busqueda de nuevo.
              end;
         end;
         //Reset pra ejecutar una nueva busqueda
         ZNavBarTools.BuscaNodoReset;
         ZNavBarTools.FNodosTextoGrupo  := 0 ;
         ZArbolTools.BuscaNodoReset;
         //Enfocar para una nueva busqueda
         if lReiniciar and PanelNavBar.Visible then
         with editBusca_DevEx do
         begin
              SetFocus;
              SelectAll;
         end;
     end; //Fin condicion vista.
end;

procedure TBasicoNavBarShell._V_CalculadoraExecute(Sender: TObject);
begin
     ExecuteFile( 'CALC.EXE', '', 'WINDOWS', SW_SHOWDEFAULT );
end;

procedure TBasicoNavBarShell._H_ContenidoExecute(Sender: TObject);
begin
     Application.HelpCommand( HELP_FINDER, 0 );
end;

procedure TBasicoNavBarShell._H_GlosarioExecute(Sender: TObject);
var
   sHelpFile: String;
begin
     if strLleno( FHelpGlosario ) then  // En el create poner FHelpGlosario indicando el Archivo de Ayuda y Secci�n al cual apuntar
        with Application do
        begin
             sHelpFile := HelpFile;
             try
                HelpFile := FHelpGlosario; //'Tress.hlp>Main';
                HelpContext ( HContenido_Glosario.HelpContext );
             finally
                HelpFile := sHelpFile;
             end;
        end;

end;

procedure TBasicoNavBarShell._H_UsoTecladoExecute(Sender: TObject);
begin
     Application.HelpContext( HContenido_btnUsoTeclado.HelpContext ); 
end;

procedure TBasicoNavBarShell._H_ComoUsarAyudaExecute(Sender: TObject);
begin
     Application.HelpContext( HContenido_btnComoUsarAyuda.HelpContext );
end;

procedure TBasicoNavBarShell._H_NuevoTressExecute(Sender: TObject);
const
     K_HELP_CAMBIOS = 'Version.chm';
var
   sHelpFile: String;
begin
     with Application do
     begin
          sHelpFile := HelpFile;
          try
             HelpFile := K_HELP_CAMBIOS;
             HelpCommand( HELP_FINDER, 0 );
          finally
             HelpFile := sHelpFile;
          end;
     end;
end;

procedure TBasicoNavBarShell._H_AcercaDeExecute(Sender: TObject);
begin
     with TZAcercaDe_DevEx.Create( Self ) do
     begin
          try
             ShowModal;
          finally
             Free;
          end;
     end;
end;

procedure TBasicoNavBarShell.editBuscaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     if ( Key = 13 ) then
     begin
          Key := 0;
          _V_BuscarFormaExecute(Sender);
     end;
     inherited;
end;

//DevEx (by am)
procedure TBasicoNavBarShell.ClientAreaPG_DevExChange(Sender: TObject);
begin
     if not FCandadoTab then
     begin
          //El siguiente If protege que si quiera hacer cambio de tab cuando ya no hay formas
          if  ( ClientAreaPG_DevEx.Properties.ActivePage <> nil) and ( Not FCerrandoSistema ) then
          begin
               with ClientAreaPG_DevEx do
               begin
                    {*** US 14266: Las gr�ficas se refrescan cada vez que cambia el empleado activo ***}
                    ActualizarGraficasInicio := True;
                    FFormManager.CambiaFormaActiva( (Properties.ActivePage as TTabForm_DevEx ).Forma );
                    SetFocus;
                    {*** US 14266: Las gr�ficas se refrescan cada vez que cambia el empleado activo ***}
                    ActualizarGraficasInicio := False;
                    {*** FIN ***}
               end;
          end;
     end;

end;



procedure TBasicoNavBarShell.FormaActivaFocus(Sender: TObject);
begin
     if HayFormaAbierta then
        Windows.SendMessage( FormaActiva.Handle, WM_FOCUS, 0, 0 )
end;

procedure TBasicoNavBarShell.FormaActivaDefocus(Sender: TObject);
begin
     if HayFormaAbierta then
        Windows.SendMessage( FormaActiva.Handle, WM_DEFOCUS, 0, 0 )
end;

procedure TBasicoNavBarShell.CargaDerechos;
begin
     inherited CargaDerechos;
     {$ifdef TRESS}
     {$ifndef DOS_CAPAS}
     ResetDerecho( D_CONS_REPORTES , K_SIN_RESTRICCION );
     {$endif}
    {$endif}
end;

procedure TBasicoNavBarShell.RegistroClick(Sender: TObject);
begin
  inherited;

end;
{***DblClick Arbolitos NavBar***}
//DevEx (by am)
procedure TBasicoNavBarShell.ArbolitoDblClick(Sender: TObject);
var
    oNodo: TTreeNode;
    TipoForma: eFormaConsulta;
    i: Integer;
    ANavBarControl: TdxNavBarGroupControl;
begin
    //Acceder a los controles dentro del grupo y buscar el TTreeView
    oNodo := nil;
    ANavBarControl := DevEx_NavBar.ActiveGroup.Control;
    for i := 0 to ANavBarControl.ControlCount - 1 do
        begin
        if ANavBarControl.Controls[i] is TTreeView then
           oNodo := (ANavBarControl.Controls[i] as TTreeView).Selected;
        if ANavBarControl.Controls[i] is TcxTreeView then //Edit by Mp : si el control del navbar es una tcxTreeview 
           oNodo := (ANavBarControl.Controls[i] as TcxTreeView).Selected;
        end;

    if ( oNodo <> nil ) then
          begin
               if EsForma( oNodo, TipoForma ) then
               begin
                    //Al abrir una forma de consulta, el tab seleccionado debe ser el de Editar (Caption: Principal)
                    //DevEx_ShellRibbon.ActiveTab := TabEditar;
                    AbreFormaConsulta( TipoForma );
               end
               else if ( Sender = nil ) then  { Es Folder. Si Viene de un Enter, Abrirlo }
                    oNodo.Expand( False );
          end;
end;
procedure TBasicoNavBarShell.ArbolitoKeyPress(Sender: TObject; var Key: Char);
begin
     case Key of
          Chr( VK_RETURN ):
          begin
               Key := #0;
               ArbolitoDblClick( NIL );
          end;
     end;
     inherited;
end;

procedure TBasicoNavBarShell.DevEx_NavBarNavigationPaneCollapsed(
  Sender: TObject);
begin
  inherited;
  PanelBuscaNavBar.Visible := False;
  PanelNavBar.Width := DevEx_NavBar.Width;
end;

procedure TBasicoNavBarShell.DevEx_NavBarNavigationPaneExpanded(
  Sender: TObject);
begin
  inherited;
  PanelNavBar.Width := DevEx_NavBar.Width;
  PanelBuscaNavBar.Visible := True;
end;

procedure TBasicoNavBarShell._V_MostrarNavBarExecute(Sender: TObject);
begin
  inherited;
  ShowNavBar( DevEx_NavBar.OptionsBehavior.NavigationPane.Collapsed );
end;

procedure TBasicoNavBarShell._E_Exportar_DevExExecute(Sender: TObject);
begin
  inherited;
  FormaActiva.DoExportar;
end;
//Permite invocar Best Fit para la forma activa que lo necesite
procedure TBasicoNavBarShell.InvocaBestFit ( NombreForma:String );
begin
     if FormaActiva.Name = NombreForma then
        FormaActiva.DoBestFit;
end;
procedure TBasicoNavBarShell.FormShow(Sender: TObject);
begin
  inherited;
  ClientAreaCerrar.ImageIndex:=26;
  ClientAreaCerrartodas.ImageIndex:=27;
  ClientAreaBuscarCodigo.ImageIndex:=28;

end;

procedure TBasicoNavBarShell.SetArbolitosLength(length:Integer);
begin
	SetLength(Arbolitos,Length);
end;

procedure  TBasicoNavBarShell.LimpiaGruposNavBar;
begin
     DevEx_NavBar.Groups.Clear;
end;

function TBasicoNavBarShell.GetNavBarImageList: TcxImageList;
begin
     Result:=SmallImageList;
end;

{*** US 14266: Las gr�ficas se refrescan cada vez que cambia el empleado activo ***}
procedure TBasicoNavBarShell.SetActualizarGraficasInicio( const lActualizaGraficaInicio: Boolean );
begin
     Self.FActualizarGraficasInicio := lActualizaGraficaInicio;
end;

function TBasicoNavBarShell.GetActualizarGraficasInicio: Boolean;
begin
     Result := Self.FActualizarGraficasInicio;
end;
{*** FIN ***}

end.
