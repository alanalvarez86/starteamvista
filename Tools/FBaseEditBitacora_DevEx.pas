unit FBaseEditBitacora_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBCtrls, StdCtrls, Buttons, ExtCtrls,
     ZetaDBTextBox, ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters,
  cxButtons, cxControls, cxNavigator, cxDBNavigator,
  cxContainer, cxEdit, cxTextEdit, cxMemo, cxDBEdit, ImgList;

type
  TBaseEditBitacora_DevEx = class(TZetaDlgModal_DevEx)
    DataSource: TDataSource;
    Panel1: TPanel;
    FechaLBL: TLabel;
    BI_FECHA: TZetaDBTextBox;
    HoraLBL: TLabel;
    BI_HORA: TZetaDBTextBox;
    TipoLBL: TLabel;
    BI_TIPO: TZetaDBTextBox;
    LabFecMov: TLabel;
    B_FEC_MOD: TZetaDBTextBox;
    MensajeLBL: TLabel;
    BI_TEXTO: TZetaDBTextBox;
    Label1: TLabel;
    US_CODIGO: TZetaDBTextBox;
    Label2: TLabel;
    BI_CLASE: TZetaDBTextBox;
    Imagen: TImage;
    DevEx_cxDBNavigatorEdicion: TcxDBNavigator;
    BI_DATA: TcxDBMemo;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Conectar( Dataset: TDataset );
  end;
  TBaseEditBitacoraClass = class of TBaseEditBitacora_DevEx;

var
  BaseEditBitacora_DevEx: TBaseEditBitacora_DevEx;

procedure ShowLogDetail( var Forma; EditBitacoraClass: TBaseEditBitacoraClass; Dataset: TDataset );

implementation
uses ZetaCommonClasses;
{$R *.DFM}

procedure ShowLogDetail( var Forma; EditBitacoraClass: TBaseEditBitacoraClass; Dataset: TDataset );
begin
     try
        TBaseEditBitacora_DevEx( Forma ) := EditBitacoraClass.Create( Application ) as TBaseEditBitacora_DevEx;
        with TBaseEditBitacora_DevEx( Forma ) do
        begin
             Conectar( Dataset );
             ShowModal;
        end;
     finally
            TBaseEditBitacora_DevEx( Forma ).Free;
     end;
end;

{ ********** TFormaBitacora ********** }

procedure TBaseEditBitacora_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     DataSource.DataSet := nil;
end;

procedure TBaseEditBitacora_DevEx.Conectar(Dataset: TDataset);
begin
     DataSource.DataSet := Dataset;
end;

end.
