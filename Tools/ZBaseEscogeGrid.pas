unit ZBaseEscogeGrid;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, Grids, DBGrids,
     StdCtrls, Buttons, ExtCtrls,
     ZetaDBGrid,
     ZetaMessages;

type
  TBaseEscogeGrid = class(TForm)
    Panel1: TPanel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    Panel2: TPanel;
    Grid: TZetaDBGrid;
    DataSource: TDataSource;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FFormula: String;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
  protected
    { Protected declarations }
    function GetFormula: String; virtual;
    procedure Connect; virtual;
  public
    { Public declarations }
    property Formula: String read GetFormula write FFormula;
  end;

var
  BaseEscogeGrid: TBaseEscogeGrid;

implementation

{$R *.DFM}

procedure TBaseEscogeGrid.FormShow(Sender: TObject);
begin
     FFormula:= '';
     Connect;
end;

procedure TBaseEscogeGrid.Connect;
begin
end;

function TBaseEscogeGrid.GetFormula: String;
begin
end;

procedure TBaseEscogeGrid.WMExaminar(var Message: TMessage);
begin
     Close;
     ModalResult := mrOk;
end;

end.
