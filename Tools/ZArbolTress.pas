unit ZArbolTress;

interface

uses  SysUtils, ComCtrls, Dialogs, DB, Controls,
     ZArbolTools, ZNavBarTools, ZetaCommonLists,
     cxtreeview,ZbaseShell; //edit by mp:Agregar CxTreeVIew a los uses

{$define QUINCENALES}

function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
procedure CreaArbolDefault( oArbolMgr: TArbolMgr );
procedure CreaArbolDefaultSistema( oArbolMgr: TArbolMgr );
procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String );
procedure CreaArbolUsuarioSistema( oArbolMgr: TArbolMgr; const sFileName: String );
procedure MuestraArbolOriginal( oArbol: TTreeView ); overload;
procedure MuestraArbolOriginal( oArbol: TcxTreeView ); overload;
procedure CreaArbolitoDefault( oArbolMgr: TArbolMgr ; iAccesoGlobal: Integer );
procedure CreaArbolitoUsuario( oArbolMgr: TArbolMgr; const sFileName: String);
//EDIT BY MP: se modifican los metodos para recivir TcxTreeview en lugar de TTreeview
procedure CreaNavBarUsuario (  oNavBarMgr: TNavBarMgr; const sFileName: String; Arbolitos: array of TcxTreeView ; ArbolitoUsuario : TcxTreeView);
procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr ; Arbolitos: array of TcxTreeView );
//fin edit
procedure GetDatosNavBarUsuario( oNavBarMgr: TNavBarMgr; DataSet: TDataSet; const sFileName: String );
function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
function CheckDerechosTress: Boolean;   
implementation

uses DGlobal,
     DSistema,
     DCliente,
     ZetaDespConsulta,
     ZToolsPe,
     ZGlobalTress,
     ZetaCommonTools,
     ZAccesosMgr,
     ZAccesosTress, 
     ZetaTipoEntidad,//
     FAutoClasses;

const
     K_EMPLEADOS                        = 1;
     K_EMP_DATOS                        = 2;
     K_EMP_DATOS_IDENTIFICA             = 3;
     K_EMP_DATOS_CONTRATA               = 4;
     K_EMP_DATOS_AREA                   = 5;
     K_EMP_DATOS_PERCEP                 = 6;
     K_EMP_DATOS_GAFETE_BIOMETRICO      = 758;
     K_EMP_DATOS_OTROS                  = 7;
     K_EMP_DATOS_ADICION                = 8;
     K_EMP_DATOS_USUARIO                = 508;
     K_EMP_CURRICULUM                   = 9;
     K_EMP_CURR_PERSONALES              = 10;
     K_EMP_CURR_FOTO                    = 11;
     K_EMP_CURR_DOCUMENTO               = 629;
     K_EMP_CURR_PARIENTES               = 12;
     K_EMP_CURR_EXPERIENCIA             = 13;
     K_EMP_CURR_CURSOS                  = 14;
     K_EMP_CURR_PUESTOS                 = 15;
     K_EMP_EXPEDIENTE                   = 16;
     K_EMP_EXP_KARDEX                   = 17;
     K_EMP_EXP_VACA                     = 18;
     K_EMP_EXP_INCA                     = 19;
     K_EMP_EXP_PERM                     = 20;
     K_EMP_EXP_CURSOS_TOMADOS           = 21;
     K_EMP_EXP_CERTIFICACIONES          = 473;
     K_EMP_EXP_CERTIFIC_PROG            = 591;
     K_EMP_EXP_PAGOS_IMSS               = 22;
     K_EMP_EXP_SGM                      = 660;
     K_EMP_PLANCAPACITA                 = 679;
     K_EMP_EVA_COMPETEM                 = 680;
     K_EMP_NOMINA                       = 23;
     K_EMP_NOM_AHORROS                  = 24;
     K_EMP_NOM_PRESTAMOS                = 25;
     K_EMP_NOM_PENSIONES                = 627;
     K_EMP_NOM_ACUMULA                  = 26;
     K_EMP_NOM_ACUMULA_RS               = 29;
     K_EMP_NOM_HISTORIAL                = 27;
     K_EMP_SIM_FINIQUITOS               = 605;
     K_EMP_NOM_SIM_FINIQUITOS           = 507;
     K_EMP_SIM_FINIQUITOS_TOTALES       = 604;
     K_EMP_REGISTRO                     = 28;
     K_EMP_PROCESOS                     = 43;
     {******** Asistencia **********}
     K_ASISTENCIA                       = 53;
     K_ASIS_DATOS                       = 54;
     K_ASIS_DATOS_TARJETA_DIARIA        = 55;
     K_ASIS_DATOS_PRENOMINA             = 56;
     K_ASIS_DATOS_CALENDARIO            = 57;
     K_ASIS_REGISTRO                    = 58;
     K_ASIS_PROCESOS                    = 61;
     {******** N�mina **********}
     K_NOMINA                           = 69;
     K_NOM_DATOS                        = 70;
     K_NOM_DATOS_TOTALES                = 71;
     K_NOM_DATOS_DIAS_HORAS             = 72;
     K_NOM_DATOS_MONTOS                 = 73;
     K_NOM_DATOS_PRENOMINA              = 74;
     K_NOM_DATOS_CLASIFI                = 75;
     K_NOM_DATOS_POLIZAS                = 474;
     K_NOM_EXCEPCIONES                  = 76;
     K_NOM_EXCEP_DIAS_HORAS             = 77;
     K_NOM_EXCEP_MONTOS                 = 78;
     K_NOM_REGISTRO                     = 79;
     K_NOM_REG_NOMINA_NUEVA             = 80;
     K_NOM_REG_BORRA_ACTIVA             = 81;
     K_NOM_REG_EXCEP_DIA_HORA           = 82;
     K_NOM_REG_EXCEP_MONTO              = 83;
     K_NOM_REG_LIQUIDACION              = 84;
     K_NOM_PROCESOS                     = 85;
     K_NOM_PROC_CALCULAR                = 86;
     K_NOM_PROC_AFECTAR                 = 87;
     K_NOM_PROC_FOLIAR_RECIBOS          = 88;
     K_NOM_PROC_IMP_LISTADO             = 89;
     K_NOM_PROC_IMP_RECIBOS             = 90;
     K_NOM_PROC_CALC_POLIZA             = 91;
     K_NOM_PROC_IMP_POLIZA              = 92;
     K_NOM_PROC_EXP_POLIZA              = 93;
     K_NOM_PROC_DESAFECTAR              = 94;
     K_NOM_PROC_LIMPIA_ACUM             = 95;
     K_EMP_REG_CIERRE_VACA              = 96;
     K_CAT_NOMINA_PARAMETROS            = 97;
     K_NOM_PROC_IMP_MOVIMIEN            = 98;
     K_NOM_PROC_EXP_MOVIMIEN            = 99;
     K_NOM_PROC_PAGAR_FUERA             = 100;
     K_NOM_PROC_CANCELA_PASADA          = 101;
     K_NOM_PROC_LIQUIDA_GLOBAL          = 102;
     K_NOM_PROC_NETO_BRUTO              = 103;
     K_NOM_PROC_RASTREO                 = 104;
     K_NOM_ANUALES                      = 105;
     K_NOM_ANUAL_PERIODOS               = 106;
     K_NOM_ANUAL_CALC_AGUINALDO         = 107;
     K_NOM_ANUAL_CALC_AHORRO            = 108;
     K_NOM_ANUAL_CALC_PTU               = 109;
     K_NOM_ANUAL_CREDITO                = 110;
     K_NOM_ANUAL_COMPARA_ISPT           = 111;
     {******** Pagos IMSS **********}
     K_IMSS                             = 112;
     K_IMSS_DATOS                       = 113;
     K_IMSS_DATOS_TOTALES               = 114;
     K_IMSS_DATOS_EMP_MEN               = 115;
     K_IMSS_DATOS_EMP_BIM               = 116;
     K_IMSS_HISTORIA                    = 117;
     K_IMSS_HIST_EMP_MEN                = 118;
     K_IMSS_HIST_EMP_BIM                = 119;
     K_IMSS_REGISTRO                    = 120;
     K_IMSS_REG_NUEVO                   = 121;
     K_IMSS_REG_BORRAR_ACTIVO           = 122;
     K_IMSS_PROCESOS                    = 123;
     K_IMSS_PROC_CALCULAR               = 124;
     K_IMSS_PROC_RECARGOS               = 125;
     K_IMSS_PROC_REVISAR_SUA            = 126;
     K_IMSS_PROC_EXP_SUA                = 127;
     K_IMSS_PROC_CALC_RIESGO            = 128;
     {******** Consultas **********}
     K_CONSULTAS                        = 129;
     K_CONS_REPORTES                    = 130;
     K_CONS_ENVIOS_PROGRAMADOS          = 718;
     K_CONS_CONTEO_PERSONAL             = 131;
     K_CONS_GRAFICA_CONTEO              = 132;
     K_CONS_SQL                         = 133;
     {******** Cat�logos **********}
     K_CATALOGOS                        = 134;
     K_CAT_CONTRATACION                 = 135;
     K_CAT_CONT_PUESTOS                 = 136;
     K_CAT_CONT_CLASIFI                 = 137;
     K_CAT_CONT_PER_FIJAS               = 138;
     K_CAT_CONT_TURNOS                  = 139;
     K_CAT_CONT_HORARIOS                = 140;
     K_CAT_CONT_PRESTA                  = 141;
     K_CAT_CONT_CONTRATO                = 142;
     K_CAT_DESCRIPCION_PUESTOS          = 511;
     K_CAT_DESC_PERF_PUESTOS            = 512;
     K_CAT_DESC_SECC_PERF               = 513;
     K_CAT_DESC_CAMPOS_PERF             = 514;
{$IFNDEF DOS_CAPAS}
     K_CAT_VALUACION_PUESTOS            = 530;
     K_CAT_VALUACIONES                  = 534;
     K_CAT_VAL_PLANTILLAS               = 531;
     K_CAT_VAL_FACTORES                 = 532;
     K_CAT_VAL_SUBFACTORES              = 533;
{$ENDIF}
     K_CAT_NOMINA                       = 143;
     K_CAT_NOMINA_CONCEPTOS             = 144;
     K_CAT_NOMINA_PERIODOS              = 145;
     K_CAT_NOMINA_FOLIOS                = 146;
     K_CAT_NOMINA_POLIZAS               = 475;
     K_CAT_NOMINA_TPENSION              = 626;
{$ifdef QUINCENALES}
     K_CAT_NOMINA_TPERIODOS             = 510;
{$endif}
     K_CAT_GENERALES                    = 147;
     K_CAT_GRALES_SOCIALES              = 562;
     K_CAT_GRALES_PATRONALES            = 148;
     K_CAT_GRALES_FEST_GRAL             = 149;
     K_CAT_GRALES_FEST_TURNO            = 150;
     K_CAT_GRALES_CONDICIONES           = 151;
     K_CAT_GRALES_EVENTOS               = 152;
     K_CAT_GRALES_SGM                   = 659;
     K_CAT_GRALES_TABLA_AMORT           = 661;
     K_CAT_CAPACITACION                 = 153;
     K_CAT_CAPA_CURSOS                  = 154;
     K_CAT_CAPA_MAESTROS                = 155;
     K_CAT_CAPA_PROVEEDORES             = 529;
     K_CAT_CAPA_MATRIZ_CURSO            = 156;
     K_CAT_CAPA_MATRIZ_PUESTO           = 157;
     K_CAT_CAPA_MATRIZ_CERT_PUESTO      = 592;
     K_CAT_CAPA_MATRIZ_PUESTO_CERT      = 593;
     K_CAT_CAPA_CALENDARIO              = 158;
     K_CAT_CERTIFICACIONES              = 472;
     K_CAT_ESTABLECIMIENTOS             = 619;
     K_CAT_COMPETENCIAS                 = 678;
     K_CAT_PERFILES                     = 677;
     K_CAT_MAT_PERFILES_COMP            = 682;
     K_CAT_NODO_COMPETENCIAS            = 683;
     K_TAB_OFI_CAT_NAC_COMP             = 699;

     K_CAT_CONFIGURACION                = 159;
     K_CAT_CONFI_GLOBALES               = 160;
     K_CONS_BITACORA                    = 161;
     K_CAT_CONFI_DICCIONARIO            = 162;
     K_CONS_PROCESOS                    = 163; { GA 22/Feb/2000:ANTES ERA UN HUECO 163 }
     {******** Tablas **********}
     K_TABLAS                           = 164;
     K_TAB_PERSONALES                   = 165;
     K_TAB_PER_EDOCIVIL                 = 166;
     K_TAB_PER_VIVE_EN                  = 167;
     K_TAB_PER_VIVE_CON                 = 168;
     K_TAB_PER_ESTUDIOS                 = 169;
     K_TAB_PER_TRANSPORTE               = 170;
     K_TAB_PER_ESTADOS                  = 171;
     K_TAB_PER_MUNICIPIOS               = 618;
     K_TAB_AREAS                        = 172;
     K_TAB_AREAS_NIVEL1                 = 173;
     K_TAB_AREAS_NIVEL2                 = 174;
     K_TAB_AREAS_NIVEL3                 = 175;
     K_TAB_AREAS_NIVEL4                 = 176;
     K_TAB_AREAS_NIVEL5                 = 177;
     K_TAB_AREAS_NIVEL6                 = 178;
     K_TAB_AREAS_NIVEL7                 = 179;
     K_TAB_AREAS_NIVEL8                 = 180;
     K_TAB_AREAS_NIVEL9                 = 181;
     K_TAB_HIST_CHECA_MANUAL            = 606;
{$ifdef ACS}
     { OJO: NO usar estos n�meros de las constantes de ACS, independientemente de que se compile
       con la directiva de ACS o NO. Respetar los valores 600, 601 y 602 as� como el valor de la
       constante K_TAB_AREAS2 (590) }
     K_TAB_AREAS2                       = 590; { OJO: Esta constante solo se usa como referencia, NO CAMBIAR }
     K_TAB_AREAS_NIVEL10                = 600; { OJO: No quitar o cambiar }
     K_TAB_AREAS_NIVEL11                = 601; { OJO: No quitar o cambiar  }
     K_TAB_AREAS_NIVEL12                = 602; { OJO: No quitar o cambiar  }
{$endif}
     K_TAB_ADICIONALES                  = 182;
     K_TAB_ADICION_TABLA1               = 183;
     K_TAB_ADICION_TABLA2               = 184;
     K_TAB_ADICION_TABLA3               = 185;
     K_TAB_ADICION_TABLA4               = 186;
     K_TAB_ADICION_TABLA5               = 426;
     K_TAB_ADICION_TABLA6               = 427;
     K_TAB_ADICION_TABLA7               = 428;
     K_TAB_ADICION_TABLA8               = 429;
     K_TAB_ADICION_TABLA9               = 430;
     K_TAB_ADICION_TABLA10              = 431;
     K_TAB_ADICION_TABLA11              = 432;
     K_TAB_ADICION_TABLA12              = 433;
     K_TAB_ADICION_TABLA13              = 434;
     K_TAB_ADICION_TABLA14              = 435;
     K_TAB_HISTORIAL                    = 187;
     K_TAB_HIST_TIPO_KARDEX             = 188;
     K_TAB_HIST_MOT_BAJA                = 189;
     K_TAB_HIST_INCIDEN                 = 190;
     K_CAT_CAPA_TIPO_CURSO              = 191;
     K_TAB_NOMINA                       = 192;
     K_TAB_NOM_TIPO_AHORRO              = 193;
     K_TAB_NOM_TIPO_PRESTA              = 194;
     K_TAB_NOM_MONEDAS                  = 195;
     K_TAB_NOM_REGLAS_PRESTA            = 597;
     K_TAB_OFICIALES                    = 196;
     K_TAB_OFI_SAL_MINIMOS              = 197;
     K_TAB_OFI_CUOTAS_IMSS              = 198;
     K_TAB_OFI_ISPT_NUMERICAS           = 199;
     K_TAB_OFI_GRADOS_RIESGO            = 200;
     K_TAB_COLONIAS                     = 293;
     K_TAB_OFI_STPS                     = 615;
     K_TAB_OFI_STPS_CAT_NAC_OCUP        = 616;
     K_TAB_OFI_STPS_ARE_TEM_CUR         = 617;
     K_TAB_CAPACITACION                 = 631;
     K_TAB_TCOMPETENCIAS                = 675;
     K_TAB_TPERFILES                    = 676;
     K_TAB_BANCOS                       = 715;
     K_TAB_VAL_UMA                      = 716;
     K_TAB_OFI_SAT                      = 696;
     K_TAB_OFI_SAT_TIPO_CONTRATO        = 697; // Constante de Tipo de Contrato
     K_TAB_OFI_SAT_TIPO_JORNADA         = 698;
     K_TAB_OFI_SAT_TIPO_RIESGO_PUESTO   = 700;
     K_TAB_OFI_SAT_TIPOS_CONCEPTOS      = 717;

     {******** Sistema **********}
     K_SISTEMA                          = 201;
     K_SIST_DATOS                       = 202;
     K_SIST_DATOS_EMPRESAS              = 203;
     K_SIST_DATOS_USUARIOS              = 204;
     K_SIST_DATOS_GRUPOS                = 205;
     K_SIST_DATOS_ROLES                 = 476;
     K_SIST_DATOS_POLL_PENDIENTES       = 206;
     K_SIST_REGISTRO                    = 207;
     K_SIST_REG_EMPRESA_NUEVA           = 208;
     K_SIST_REG_BORRAR_EMPRESA          = 209;
     K_SIST_PROCESOS                    = 210;
     K_NOM_PROC_CANCELAR_VACA           = 211;
     K_SIST_PROC_BORRAR_BAJAS           = 212;
     K_SIST_PROC_BORRAR_TARJETAS        = 213;
     K_SIST_BITACORA                    = 526;
     K_SIST_BITACORA_REPORTES           = 760;
     K_SIST_LST_DISPOSITIVOS            = 603;
     K_SIST_SOLICITUD_CORREOS           = 635;
     K_SIST_SOLICITUD_ENVIOS            = 766;
     K_SIST_CALENDARIO_REPORTES         = 636;
     K_SIST_LISTA_GRUPOS                = 623; // SYNERGY
     K_SIST_TERM_GRUPOS                 = 624; // SYNERGY
     K_SIST_BITACORA_BIO                = 622; // SYNERGY
     //K_SIST_MENSAJES                    = 631; // SYNERGY
     K_SIST_MENSAJES_TERM               = 632; // SYNERGY
     K_SIST_TERMINALES                  = 633; // SYNERGY
     K_SIST_TERMINALESGTI               = 634; // SYNERGY
     K_SIST_BASEDATOS                   = 669;
     K_SIST_ACTUALIZAR_BDS              = 670;
     K_SIST_REPORTES_EMAIL              = 765;
     { *********** Nuevos ***********}
     K_NOM_PROC_COPIAR_NOMINA           = 214;
     K_EMP_EXP_CURSOS_PROGRAMADOS       = 215;
     K_EMP_REG_CAMBIO_MULTIPLE          = 216;
     {********** Reportes ***********}
     K_REPORTES_EMPLEADOS               = 217;
     K_REPORTES_ASISTENCIA              = 218;
     K_REPORTES_NOMINA                  = 219;
     K_REPORTES_IMSS                    = 220;
     K_REPORTES_CONSULTAS               = 221;
     K_REPORTES_CATALOGOS               = 222;
     K_REPORTES_TABLAS                  = 223;
     { *********** Nuevos ***********}
     K_IMSS_PROC_CALC_TASA_INFO         = 224;
     { PENDIENTE ??? }
     //K_CHECADAS_EMPLEADO  = 225;
     K_NOM_PROC_RECALC_ACUMULADOS       = 226;
     K_EMP_PROC_TRANSFERENCIA           = 227;
     K_NOM_ANUAL_PAGA_AGUINALDO         = 228;
     { PENDIENTE: Repetido con 108? }
     K_NOM_ANUAL_CALC_AHORROS           = 229;
     K_NOM_ANUAL_CIERRE_AHORRO          = 230;
     K_NOM_ANUAL_CIERRE_PRESTA          = 231;
     K_SIST_PROC_BORRA_NOMINAS          = 232;
     K_NOM_ANUAL_IMP_AGUINALDO          = 233;
     K_NOM_ANUAL_IMP_AHORRO             = 234;
     K_NOM_ANUAL_DIF_COMPARA            = 235;
     K_NOM_ANUAL_IMP_COMPARA            = 236;
     K_NOM_ANUAL_IMP_CREDITO            = 237;
     K_NOM_ANUAL_IMP_PTU                = 238;
     K_NOM_ANUAL_PAGA_PTU               = 239;
     {************ Cafeter�a **********}
     K_EMP_CAFETERIA                    = 240;
     K_EMP_CAFE_COMIDAS_DIA             = 241;
     K_EMP_CAFE_COMIDAS_PERIODO         = 242;
     K_EMP_CAFE_INVITACIONES            = 243;
     K_CAT_CAFETERIA                    = 244;
     K_CAT_CAFE_REGLAS                  = 245;
     K_CAT_CAFE_INVITADORES             = 246;
     K_REPORTES_CAFETERIA               = 247;
     K_REPORTES_SUPERVISORES            = 248;
     K_SIST_DATOS_IMPRESORAS            = 249;
     K_NOM_EXCEP_GLOBALES               = 250;
     {************ Accesos **********}
     K_CAT_ACCESOS_REGLAS               = 291;

     { ********* Supervisores ****** }
     K_NOM_REG_EXCEP_GLOBAL             = 251;
     K_SUPER_AJUSTAR_ASISTENCIA         = 252;
     K_SUPER_PERMISOS                   = 253;
     K_SUPER_KARDEX                     = 254;
     K_SUPER_CONSULTAR_PRENOMINA        = 255;
     K_SUPER_BITACORA                   = 256;
     K_SUPER_ASIGNAR_EMPLEADOS          = 257;
     K_SUPER_DATOS_EMPLEADOS            = 258;
     { PENDIENTE: Repetida 251 }
     K_NOM_REG_EXCEP_MONTO_GLOBAL       = 259;
     K_SUPER_AJUSTAR_ASISTENCIA_ESPECIAL= 260;
     { HUECO 261 }
     K_SUPER_AUTORIZACION_COLECTIVA     = 262;
     K_SUPER_TIPO_KARDEX                = 263;
     K_NOM_PROC_CALC_DIFERENCIAS        = 264;
     K_ASIS_DATOS_AUTORIZACIONES        = 265;
     K_TAB_HIST_AUT_EXTRAS              = 266;
     K_TAB_OFI_TIPOS_CAMBIO             = 267;
     K_REPORTES_CONFIDENCIALES          = 268;
     K_NOM_REG_PAGO_RECIBOS             = 269;
     K_NOM_PROC_RE_FOLIAR               = 270;
     K_NOM_PROC_IMP_PAGO_RECIBO         = 271;
     K_SUPER_VER_BAJAS                  = 272;
     K_SIST_NIVEL0                      = 282;
     K_CAT_HERRAMIENTAS                 = 283;
     K_CAT_HERR_TOOLS                   = 284;
     K_TAB_HERR_TALLAS                  = 285;
     K_TAB_HERR_MOTTOOL                 = 286;
     K_EMP_EXP_TOOLS                    = 287;
     K_CLASIFI_CURSO                    = 288;
     K_CONS_ORGANIGRAMA                 = 289;
     K_ASIS_DATOS_CLASIFI_TEMP          = 292;
     K_CAT_CAPA_AULAS 	                = 294;
     K_CAT_CAPA_PREREQUISITOS           = 299;
     K_CONS_PLANVACACION                = 506;
     K_CONS_INICIO                      = 509;
     K_CONS_MOSTRAR_GRAFICAS            = 515;
     K_CONS_TABLEROS                    = 762;
     {******** Capacitacion *******}
     K_CAPACITACION 		        = 471;
     K_EMP_EXP_SESIONES                 = 290;
     K_CAPA_RESERVAS 	                = 296;
     K_CAPA_MATRIZ_HBLDS                = 695;
     K_CAPA_MATRIZ_CURSOS               = 757;

     K_NOM_FONACOT                      = 588;
     K_NOM_FONACOT_TOTALES              = 589;
     K_NOM_FONACOT_EMPLEADO             = 590;

     K_EMP_EXP_INFONAVIT                = 595;
     K_IMSS_CONCILIA_INFONAVIT          = 596;
     {******** Caseta *******}
     K_EMP_CASETA                       = 598;{acl 081008}
     K_EMP_CASETA_DIARIAS               = 599;{acl 081008}

     {******** Pantallas para Costeo ********}
     K_COSTEO_TRANSFER                  = 620;
     K_COSTEO_MOTIVO_TRANSFER           = 621;
     K_CAT_COSTEO_GRUPOS                = 625;
     K_CAT_COSTEO_CRITERIOS             = 628;
     K_CAT_COSTEO_CRITERIOS_CONCEPTO    = 630;
     K_FULL_ARBOL_TRESS                 =9999;//edit by MP: Derecho para el Arbol Clasico

     { OJO: (OP) NO usar los n�meros de las constantes de ACS, independientemente de que se compile
       con la directiva de ACS o NO. Respetar los valores 600, 601 y 602 as� como el valor de la
       constante K_TAB_AREAS2 (590) }

     //MA( Ver 2.7 ) NOTA: Cuando se agreguen constantes que corresponden a nodos tipo FOLDER dentro de el
     //�rbol de formas en Tress, es necesario que el valor de la constante sea igual al valor del derecho
     //de acceso en ZAccesosTress.pas.
     //                   Ej.- K_CAPACITACION = D_CAPACITACION        ( Sugerencia para versiones futuras )
     //

function Forma( const sCaption: String; const eTipo: eFormaConsulta ): TNodoInfo;
begin
     Result.EsForma := TRUE;
     Result.Caption := sCaption;
     Result.TipoForma := eTipo;
end;

function Folder( const sCaption: String; const iDerechos: Integer ): TNodoInfo;
begin
     Result.EsForma := FALSE;
     Result.Caption := sCaption;
     Result.IndexDerechos := iDerechos;
end;
//edit by MP: Funcion para definir Grupos del NavBar
function Grupo( const sCaption: String; const iDerechos: Integer ): TGrupoInfo;
begin
     Result.Caption := sCaption;
     Result.IndexDerechos := iDerechos;
end;
function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
begin
     case iNodo of
          // Empleados
          K_EMPLEADOS               : Result := Folder( 'Empleados', iNodo );
          K_EMP_DATOS               : Result := Folder( 'Datos', iNodo );
          K_EMP_DATOS_IDENTIFICA    : Result := Forma( 'Identificaci�n', efcEmpIdentifica );
          K_EMP_DATOS_CONTRATA      : Result := Forma( 'Contrataci�n', efcEmpContratacion );
          K_EMP_DATOS_AREA          : Result := Forma( '�rea', efcEmpArea );
          K_EMP_DATOS_PERCEP        : Result := Forma( 'Percepciones', efcEmpPercepcion );
          K_EMP_DATOS_GAFETE_BIOMETRICO: Result := Forma( 'Gafete y biom�trico', efcEmpDatosGafeteBiometrico );
          K_EMP_DATOS_OTROS         : Result := Forma( 'Otros', efcEmpOtros );
          K_EMP_DATOS_ADICION       : Result := Forma( 'Adicionales', efcEmpAdicionales );
          K_EMP_DATOS_USUARIO       : Result := Forma( 'Usuario', efcEmpUsuario );
          K_EMP_CURRICULUM          : Result := Folder( 'Curr�culum', iNodo );
          K_EMP_CURR_PERSONALES     : Result := Forma( 'Personales', efcEmpPersonal );
          K_EMP_CURR_FOTO           : Result := Forma( 'Foto e Im�genes', efcEmpFoto ); // (JB) Anexar al expediente del trabajador documentos CR1880 T1107 (Cambia de Nombre)
          K_EMP_CURR_DOCUMENTO      : Result := Forma( 'Documentos', efcEmpDocumento ); // (JB) Anexar al expediente del trabajador documentos CR1880 T1107
          K_EMP_CURR_PARIENTES      : Result := Forma( 'Parientes', efcEmpParientes );
          K_EMP_CURR_EXPERIENCIA    : Result := Forma( 'Experiencia', efcEmpExperiencia  );
          K_EMP_CURR_CURSOS         : Result := Forma( 'Cursos Anteriores', efcEmpAntesCur );
          K_EMP_CURR_PUESTOS        : Result := Forma( 'Puestos Anteriores', efcEmpAntesPto );
          K_EMP_EXPEDIENTE          : Result := Folder( 'Expediente', iNodo );
          K_EMP_EXP_KARDEX          : Result := Forma( 'Kardex', efcHisKardex );
          K_EMP_EXP_VACA            : Result := Forma( 'Vacaciones', efcHisVacacion );
          K_EMP_EXP_INCA            : Result := Forma( 'Incapacidades', efcHisIncapaci  );
          K_EMP_EXP_PERM            : Result := Forma( 'Permisos', efcHisPermisos );
          K_EMP_EXP_CERTIFICACIONES : Result := Forma( 'Certificaciones Tomadas', efcKarCertificaciones );
          K_EMP_EXP_CERTIFIC_PROG   : Result := Forma( 'Certificaciones Programadas', efcKarCertificProgramadas );
          K_EMP_EXP_CURSOS_TOMADOS  : Result := Forma( 'Cursos Tomados', efcHisCursos );
          K_EMP_EXP_PAGOS_IMSS      : Result := Forma( 'Pagos IMSS', efcHisPagosIMSS );
          K_EMP_EXP_INFONAVIT       : Result := Forma( 'Cr�dito Infonavit', efcHisCreInfonavit );
          K_EMP_EXP_SGM             : Result := Forma( 'Seguros de Gastos M�dicos', efcHisSegurosGastosMedicos );
          K_EMP_PLANCAPACITA        : Result := Forma( 'Plan de Capacitaci�n', efcHisPlanCapacitacion );
          K_EMP_EVA_COMPETEM        : Result := Forma( 'Competencias Evaluadas', efcHisEvaluaCompeten );

          K_EMP_NOMINA              : Result := Folder( 'N�mina', iNodo );
          K_EMP_NOM_AHORROS         : Result := Forma( 'Ahorros', efcHisAhorros );
          K_EMP_NOM_PRESTAMOS       : Result := Forma( 'Pr�stamos', efcHisPrestamos );
          K_EMP_NOM_PENSIONES       : Result := Forma( 'Pensiones Alimenticias', efcHisPensiones );
          //K_EMP_NOM_ACUMULA         : Result := Forma( 'Acumulados', efcHisAcumulados );
          K_EMP_NOM_ACUMULA_RS      : Result := Forma( 'Acumulados', efcHisAcumulados_RS );
          K_EMP_NOM_HISTORIAL       : Result := Forma( 'Historial ', efcHisNominas );
          K_EMP_SIM_FINIQUITOS      : Result := Folder( 'Simulaci�n de Finiquitos', iNodo );
          K_EMP_NOM_SIM_FINIQUITOS  : Result := Forma( 'Por Empleado', efcSimulaMontos );
          K_EMP_SIM_FINIQUITOS_TOTALES : Result := Forma( 'Totales', efcSimulaMontoGlobal );
          K_EMP_REGISTRO            : Result := Forma( 'Registro', efcRegEmpleado );
          K_EMP_PROCESOS            : Result := Forma( 'Procesos', efcProcEmpleado  );
          // Asistencia
          K_ASISTENCIA              : Result := Folder( 'Asistencia', iNodo );
          K_ASIS_DATOS              : Result := Folder( 'Datos', iNodo );
          K_ASIS_DATOS_TARJETA_DIARIA: Result := Forma( 'Tarjeta Diaria', efcAsisTarjeta );
          K_ASIS_DATOS_PRENOMINA    : Result := Forma( 'Pre-N�mina', efcAsisPreNomina );
          K_ASIS_DATOS_CALENDARIO   : Result := Forma( 'Calendario', efcAsisCalendario );
          K_ASIS_REGISTRO           : Result := Forma( 'Registro', efcRegAsistencia );
          K_ASIS_PROCESOS           : Result := Forma( 'Procesos', efcProcAsistencia );

          // N�minas
          K_NOMINA                  : Result := Folder( 'N�minas', iNodo );
          K_NOM_DATOS               : Result := Folder( 'Datos', iNodo );
          K_NOM_DATOS_TOTALES       : Result := Forma( 'Totales', efcNomTotales );
          K_NOM_DATOS_DIAS_HORAS    : Result := Forma( 'D�as/Horas', efcNomDiasHoras );
          K_NOM_DATOS_MONTOS        : Result := Forma( 'Montos', efcNomMontos );
          K_NOM_DATOS_PRENOMINA     : Result := Forma( 'Pre-N�mina', efcNomDatosAsist );
          K_NOM_DATOS_CLASIFI       : Result := Forma( 'Clasificaci�n', efcNomDatosClasifi );
          K_NOM_DATOS_POLIZAS       : Result := Forma( 'P�lizas', efcPolizas);
          K_NOM_EXCEPCIONES         : Result := Folder( 'Excepciones', iNodo );
          K_NOM_EXCEP_DIAS_HORAS    : Result := Forma( 'D�as/Horas', efcNomExcepciones );
          K_NOM_EXCEP_MONTOS        : Result := Forma( 'Montos', efcNomExcepMontos );
          K_NOM_REGISTRO            : Result := Forma( 'Registro', efcRegNomina );
          K_NOM_PROCESOS            : Result := Forma( 'Procesos', efcProcNomina );
          K_CAT_NOMINA_PARAMETROS   : Result := Forma( 'Par�metros', efcNomParams );
          K_NOM_ANUALES             : Result := Forma( 'Anuales', efcAnualNomina );

          // Pagos IMSS
          K_IMSS                    : Result := Folder( 'Pagos IMSS', iNodo );
          K_IMSS_DATOS              : Result := Folder( 'Datos', iNodo );
          K_IMSS_DATOS_TOTALES      : Result := Forma( 'Totales', efcIMSSDatosTot );
          K_IMSS_DATOS_EMP_MEN      : Result := Forma( 'Por Empleado Mensual', efcIMSSDatosMensual );
          K_IMSS_DATOS_EMP_BIM      : Result := Forma( 'Por Empleado Bimestral', efcIMSSDatosBimestral );
          K_IMSS_CONCILIA_INFONAVIT : Result := Forma( 'Conciliaci�n Infonavit', efcIMSSConciliaInfonavit );
          K_IMSS_HISTORIA           : Result := Folder( 'Historia', iNodo );
          K_IMSS_HIST_EMP_MEN       : Result := Forma( 'Por Empleado Mensual', efcIMSSHisMensual );
          K_IMSS_HIST_EMP_BIM       : Result := Forma( 'Por Empleado Bimestral', efcIMSSHisBimestral );
          K_IMSS_REGISTRO           : Result := Forma( 'Registro', efcRegIMSS );
          K_IMSS_PROCESOS           : Result := Forma( 'Procesos', efcProcIMSS );

          // Consultas
          K_CONSULTAS               : Result := Folder( 'Consultas', iNodo );
          K_CONS_REPORTES           : Result := Forma( 'Reportes', efcReportes );
          K_CONS_ENVIOS_PROGRAMADOS : Result := Forma( 'Env�os Programados', efcEnviosProgramados );
          K_CONS_CONTEO_PERSONAL    : Result := Forma( 'Presupuesto', efcConteo );
          K_CONS_GRAFICA_CONTEO     : Result := Forma( 'Gr�fica de Conteo', efcNinguna );
          K_CONS_SQL                : Result := Forma( 'SQL', efcQueryGral );
          K_CONS_ORGANIGRAMA        : Result := Forma( 'Organigrama', efcOrganigrama );
          K_CONS_PLANVACACION       : Result := Forma( 'Plan de Vacaciones', efcPlanVacacion );
          K_CONS_INICIO             : Result := Forma( 'Tablero de Inicio', efcInicio );
          K_CONS_MOSTRAR_GRAFICAS   : Result := Forma( 'Tableros de Inicio por Roles de Usuario', efcTablerosPorRoles );
          K_CONS_TABLEROS           : Result := Folder( 'Tableros', iNodo );
     // Cat�logos
          K_CATALOGOS               : Result := Folder( 'Cat�logos', iNodo );
          K_CAT_CONTRATACION        : Result := Folder( 'Contrataci�n', iNodo );
          K_CAT_CONT_PUESTOS        : Result := Forma( 'Puestos', efcCatPuestos );
          K_CAT_CONT_CLASIFI        : Result := Forma( 'Clasificaciones', efcCatClasifi );
          K_CAT_CONT_PER_FIJAS      : Result := Forma( 'Percepciones Fijas', efcCatOtrasPer );
          K_CAT_CONT_TURNOS         : Result := Forma( 'Turnos', efcCatTurnos );
          K_CAT_CONT_HORARIOS       : Result := Forma( 'Horarios', efcCatHorarios );
          K_CAT_CONT_PRESTA         : Result := Forma( 'Prestaciones', efcCatPrestaciones );
          K_CAT_CONT_CONTRATO       : Result := Forma( 'Tipo de Contrato', efcCatContratos );
          //Descripci�n de puestos
          K_CAT_DESCRIPCION_PUESTOS : Result := Folder( 'Descripci�n de Puestos', iNodo );
          K_CAT_DESC_PERF_PUESTOS   : Result := Forma( 'Perfiles de Puestos', efcCatPerfPuestos );
          K_CAT_DESC_SECC_PERF      : Result := Forma( 'Secciones de Perfil', efcCatSeccPerf );
          K_CAT_DESC_CAMPOS_PERF    : Result := Forma( 'Campos de Perfil', efcCatCamposPerf );
{$IFNDEF DOS_CAPAS}
          //Valuaci�n de puestos
          K_CAT_VALUACION_PUESTOS   : Result := Folder( 'Valuaci�n de Puestos', iNodo );
          K_CAT_VALUACIONES         : Result := Forma( 'Valuaciones de Puestos', efcCatValuaciones );
          K_CAT_VAL_PLANTILLAS      : Result := Forma( 'Plantillas de Valuaci�n', efcCatValPlantillas );
          K_CAT_VAL_FACTORES        : Result := Forma( 'Factores de Valuaci�n', efcCatValFactores );
          K_CAT_VAL_SUBFACTORES     : Result := Forma( 'Subfactores de Valuaci�n', efcCatValSubfactores);
{$ENDIF}
          //N�mina
          K_CAT_NOMINA              : Result := Folder( 'N�mina', iNodo );
          K_CAT_NOMINA_CONCEPTOS    : Result := Forma( 'Conceptos', efcCatConceptos );
          K_CAT_NOMINA_PERIODOS     : Result := Forma( 'Per�odos', efcCatPeriodos );
          K_CAT_NOMINA_FOLIOS       : Result := Forma( 'Folios de Recibos', efcCatFolios );
          K_CAT_NOMINA_POLIZAS      : Result := Forma( 'Tipos de P�liza', efcTiposPoliza );
          K_CAT_NOMINA_TPENSION     : Result := Forma( 'Tipos de Pensi�n', efcTiposPension );
{$ifdef QUINCENALES}
          K_CAT_NOMINA_TPERIODOS    : Result := Forma( 'Tipos de Periodos', efcCatTPeriodos );
{$endif}

          K_CAT_GENERALES           : Result := Folder( 'Generales', iNodo );
          K_CAT_GRALES_SOCIALES     : Result := Forma( 'Razones Sociales', efcCatRSocial );
          K_CAT_GRALES_PATRONALES   : Result := Forma( 'Registros Patronales', efcCatRPatron );
          K_CAT_GRALES_FEST_GRAL    : Result := Forma( 'Festivos Generales', efcCatFestGrales );
          K_CAT_GRALES_FEST_TURNO   : Result := Forma( 'Festivos por Turno', efcCatFestTurno );
          K_CAT_GRALES_CONDICIONES  : Result := Forma( 'Condiciones', efcCatCondiciones );
          K_CAT_GRALES_EVENTOS      : Result := Forma( 'Eventos', efcCatEventos );
          K_CAT_GRALES_SGM          : Result := Forma( 'Seguros de Gastos M�dicos', efcCatSegurosGastosMedicos );
          K_CAT_GRALES_TABLA_AMORT  : Result := Forma( 'Tablas de Cotizaci�n SGM', efcCatTabAmortizacion );

          K_CAT_CAPACITACION        : Result := Folder( 'Capacitaci�n', iNodo );
          K_CAT_CAPA_CURSOS         : Result := Forma( 'Cursos',  efcCatCursos );
          K_CAT_CAPA_MAESTROS       : Result := Forma( 'Maestros', efcCatMaestros );
          K_CAT_CAPA_PROVEEDORES    : Result := Forma( 'Proveedores de Capacitaci�n', efcCatProvCap);
          K_CAT_CAPA_MATRIZ_CURSO   : Result := Forma( 'Matriz por Curso', efcCatMatrizCurso );
          K_CAT_CAPA_MATRIZ_PUESTO  : Result := Forma( 'Matriz por  Puesto', efcCatMatrizPuesto );
          K_CAT_CAPA_MATRIZ_CERT_PUESTO : Result := Forma( 'Matriz Certificaciones por Puesto', efcCatMatrizCertsPuesto );
          K_CAT_CAPA_MATRIZ_PUESTO_CERT : Result := Forma( 'Matriz Puestos por Certificaci�n', efcCatMatrizPuestosCert );
          K_CAT_CAPA_CALENDARIO     : Result := Forma( 'Calendario', efcCatCalendario );
          K_CAT_CERTIFICACIONES     : Result := Forma( 'Certificaciones', efcCertificaciones );
          K_CAT_ESTABLECIMIENTOS    : Result := Forma( 'Establecimientos', efcEstablecimientos );
          K_CAT_COMPETENCIAS        : Result := Forma( 'Competencias', efcCatCompetencias );
          K_CAT_PERFILES            : Result := Forma( 'Grupos de Competencias', efcCatPerfiles );
          K_CAT_MAT_PERFILES_COMP   : Result := Forma( 'Matriz de Funciones', efcCatMatPerfilComps );
          K_CAT_NODO_COMPETENCIAS   : Result := Folder( 'Competencias', iNodo );

          K_CAT_CONFIGURACION       : Result := Folder( 'Configuraci�n', iNodo );
          K_CAT_CONFI_GLOBALES      : Result := Forma( 'Globales de Empresa', efcSistGlobales );
          K_CONS_BITACORA           : Result := Forma( 'Bit�cora', efcSistBitacora );
          K_CAT_CONFI_DICCIONARIO   : Result := Forma( 'Diccionario de Datos', efcDiccion );
          K_CONS_PROCESOS           : Result := Forma( 'Procesos', efcSistProcesos );

          //Tablas
          K_TABLAS                  : Result := Folder( 'Tablas', iNodo );
          K_TAB_PERSONALES          : Result := Folder( 'Personales', iNodo );
          K_TAB_PER_EDOCIVIL        : Result := Forma( 'Estado Civil', efcTOEstadoCivil );
          K_TAB_PER_VIVE_EN         : Result := Forma( 'En D�nde Vive', efcTOHabitacion );
          K_TAB_PER_VIVE_CON        : Result := Forma( 'Con Qui�n Vive', efcTOViveCon );
          K_TAB_PER_ESTUDIOS        : Result := Forma( 'Grado de Estudios', efcTOEstudios );
          K_TAB_PER_TRANSPORTE      : Result := Forma( 'Transporte', efcTOTransporte );
          K_TAB_PER_ESTADOS         : Result := Forma( 'Estados del Pa�s', efcTOEstado );
          K_TAB_PER_MUNICIPIOS      : Result := Forma( 'Municipios', efcTOMunicipios );

          K_TAB_AREAS               : Result := Folder( 'Areas', iNodo );
          K_TAB_AREAS_NIVEL1        : Result := Forma( Global.NombreNivel( 1 ), efcTONivel1 );
          K_TAB_AREAS_NIVEL2        : Result := Forma( Global.NombreNivel( 2 ), efcTONivel2 );
          K_TAB_AREAS_NIVEL3        : Result := Forma( Global.NombreNivel( 3 ), efcTONivel3 );
          K_TAB_AREAS_NIVEL4        : Result := Forma( Global.NombreNivel( 4 ), efcTONivel4 );
          K_TAB_AREAS_NIVEL5        : Result := Forma( Global.NombreNivel( 5 ), efcTONivel5 );
          K_TAB_AREAS_NIVEL6        : Result := Forma( Global.NombreNivel( 6 ), efcTONivel6 );
          K_TAB_AREAS_NIVEL7        : Result := Forma( Global.NombreNivel( 7 ), efcTONivel7 );
          K_TAB_AREAS_NIVEL8        : Result := Forma( Global.NombreNivel( 8 ), efcTONivel8 );
          K_TAB_AREAS_NIVEL9        : Result := Forma( Global.NombreNivel( 9 ), efcTONivel9 );
{$ifdef ACS}
          K_TAB_AREAS_NIVEL10       : Result := Forma( Global.NombreNivel( 10 ), efcTONivel10 );
          K_TAB_AREAS_NIVEL11       : Result := Forma( Global.NombreNivel( 11 ), efcTONivel11 );
          K_TAB_AREAS_NIVEL12       : Result := Forma( Global.NombreNivel( 12 ), efcTONivel12 );
{$endif}
          K_TAB_ADICIONALES         : Result := Folder( 'Adicionales', iNodo );
{
          K_TAB_ADICION_TABLA1      : Result := Forma( Global.AdTabla( 1 ), efcTOExtra1 );
          K_TAB_ADICION_TABLA2      : Result := Forma( Global.AdTabla( 2 ), efcTOExtra2 );
          K_TAB_ADICION_TABLA3      : Result := Forma( Global.AdTabla( 3 ), efcTOExtra3 );
          K_TAB_ADICION_TABLA4      : Result := Forma( Global.AdTabla( 4 ), efcTOExtra4 );
}
          K_TAB_ADICION_TABLA1      : Result := Forma( dmSistema.GetNameTabAdic( enExtra1 ), efcTOExtra1 );
          K_TAB_ADICION_TABLA2      : Result := Forma( dmSistema.GetNameTabAdic( enExtra2 ), efcTOExtra2 );
          K_TAB_ADICION_TABLA3      : Result := Forma( dmSistema.GetNameTabAdic( enExtra3 ), efcTOExtra3 );
          K_TAB_ADICION_TABLA4      : Result := Forma( dmSistema.GetNameTabAdic( enExtra4 ), efcTOExtra4 );
          K_TAB_ADICION_TABLA5      : Result := Forma( dmSistema.GetNameTabAdic( enExtra5 ), efcTOExtra5 );
          K_TAB_ADICION_TABLA6      : Result := Forma( dmSistema.GetNameTabAdic( enExtra6 ), efcTOExtra6 );
          K_TAB_ADICION_TABLA7      : Result := Forma( dmSistema.GetNameTabAdic( enExtra7 ), efcTOExtra7 );
          K_TAB_ADICION_TABLA8      : Result := Forma( dmSistema.GetNameTabAdic( enExtra8 ), efcTOExtra8 );
          K_TAB_ADICION_TABLA9      : Result := Forma( dmSistema.GetNameTabAdic( enExtra9 ), efcTOExtra9 );
          K_TAB_ADICION_TABLA10     : Result := Forma( dmSistema.GetNameTabAdic( enExtra10 ), efcTOExtra10 );
          K_TAB_ADICION_TABLA11     : Result := Forma( dmSistema.GetNameTabAdic( enExtra11 ), efcTOExtra11 );
          K_TAB_ADICION_TABLA12     : Result := Forma( dmSistema.GetNameTabAdic( enExtra12 ), efcTOExtra12 );
          K_TAB_ADICION_TABLA13     : Result := Forma( dmSistema.GetNameTabAdic( enExtra13 ), efcTOExtra13 );
          K_TAB_ADICION_TABLA14     : Result := Forma( dmSistema.GetNameTabAdic( enExtra14 ), efcTOExtra14 );

          K_TAB_HISTORIAL           : Result := Folder( 'Historial', iNodo );
          K_TAB_HIST_TIPO_KARDEX    : Result := Forma( 'Tipo de Kardex', efcTOMovKardex );
          K_TAB_HIST_MOT_BAJA       : Result := Forma( 'Motivo de Baja', efcTOMotivoBaja );
          K_TAB_HIST_INCIDEN        : Result := Forma( 'Incidencias', efcTOIncidencias );
          K_CAT_CAPA_TIPO_CURSO     : Result := Forma( 'Tipo de Curso', efcTOTipoCursos );

          K_TAB_NOMINA              : Result := Folder( 'N�mina', iNodo );
          K_TAB_NOM_TIPO_AHORRO     : Result := Forma( 'Tipo de Ahorro', efcTOTAhorro );
          K_TAB_NOM_TIPO_PRESTA     : Result := Forma( 'Tipo de Pr�stamo', efcTOTPresta );
          K_TAB_NOM_MONEDAS         : Result := Forma( 'Monedas', efcMonedas );
          K_TAB_NOM_REGLAS_PRESTA   : Result := Forma( 'Reglas de Pr�stamos', efcReglasPrestamos );


          K_TAB_OFICIALES           : Result := Folder( 'Oficiales', iNodo );
          K_TAB_OFI_SAL_MINIMOS     : Result := Forma( 'Salarios M�nimos', efcSistSalMin );
          K_TAB_OFI_CUOTAS_IMSS     : Result := Forma( 'Cuotas de IMSS', efcSistLeyIMSS );
          K_TAB_OFI_ISPT_NUMERICAS  : Result := Forma( 'ISPT y Num�ricas', efcSistNumerica );
          K_TAB_OFI_GRADOS_RIESGO   : Result := Forma( 'Grados de Riesgo', efcSistRiesgo );
          K_TAB_COLONIAS            : Result := Forma( 'Colonias', efcTTOColonia );
          K_TAB_OFI_STPS            : Result := Folder( 'STPS', iNodo );
          K_TAB_OFI_STPS_CAT_NAC_OCUP : Result := Forma( 'Cat�logo Nacional de Ocupaciones', efcTOcupaNac );
          K_TAB_OFI_STPS_ARE_TEM_CUR  : Result := Forma( 'Areas Tem�ticas de Cursos', efcTAreaTemCur );
          K_TAB_CAPACITACION        : Result := Folder( 'Competencias', iNodo );
          K_TAB_TCOMPETENCIAS       : Result := Forma( 'Tipos de Competencias', efcTabTCompetencias  );
          K_TAB_TPERFILES           : Result := Forma( 'Tipos de Grupos de Competencias', efcTabTPerfiles );
          K_TAB_OFI_CAT_NAC_COMP    : Result := Forma( 'Cat�logo Nacional de Competencias', efcCatNacCompetencias );
          K_TAB_BANCOS              : Result := Forma( 'Cat�logo de Bancos' , efcBancos ) ;
          K_TAB_VAL_UMA              : Result := Forma( 'Unidad de Medida y Actualizaci�n' , efcUma ) ;
          K_TAB_OFI_SAT              : Result := Folder( 'SAT', iNodo );
          K_TAB_OFI_SAT_TIPOS_CONCEPTOS   : Result := Forma( 'Tipos de Conceptos', efcTTiposConceptosSAT );
          K_TAB_OFI_SAT_TIPO_CONTRATO   : Result := Forma( 'Tipos de Contrato', efcTTipoContratoSAT );
          K_TAB_OFI_SAT_TIPO_JORNADA    : Result := Forma( 'Tipos de Jornada', efcTTipoJornadaSAT );
          K_TAB_OFI_SAT_TIPO_RIESGO_PUESTO    : Result := Forma( 'Riesgo de Puesto', efcTRiesgoPuestoSAT );
          // Sistema
          K_SISTEMA                 : Result := Folder( 'Sistema', iNodo );
          K_SIST_DATOS              : Result := Folder( 'Datos', iNodo );
          K_SIST_DATOS_EMPRESAS     : Result := Forma( 'Empresas', efcSistEmpresas );
          K_SIST_DATOS_USUARIOS     : Result := Forma( 'Usuarios', efcSistUsuarios );
          K_SIST_DATOS_GRUPOS       : Result := Forma( 'Grupos de Usuarios', efcSistGrupos );
          K_SIST_BASEDATOS          : Result := Forma( 'Bases de Datos', efcSistBaseDatos);
          K_SIST_ACTUALIZAR_BDS     : Result := Forma( 'Actualizaci�n de Bases de Datos', efcSistActualizarBDs);
          {$ifdef DOS_CAPAS}
          //En la version profesional, no lleva esta forma.
          {$else}
          K_SIST_DATOS_ROLES        : Result := Forma( 'Roles', efcSistRoles );
          {$endif}
          K_SIST_DATOS_POLL_PENDIENTES: Result := Forma( 'Poll Pendientes', efcSistPoll );
          K_SIST_REGISTRO           : Result := Forma( 'Registro', efcRegSist );
          K_SIST_PROCESOS           : Result := Forma( 'Procesos', efcProcSist );
          K_SIST_NIVEL0             : Result := Forma( 'Confidencialidad', efcSistNivel0 );
          K_SIST_BITACORA           : Result := Forma( 'Bit�cora de Sistema', efcBitacoraSistema );
          K_SIST_LST_DISPOSITIVOS   : Result := Forma( 'Lista de Dispositivos', efcSistDispositivos );
{$ifndef DOS_CAPAS}
          K_SIST_TERMINALESGTI      : Result := Folder( 'Terminales GTI', iNodo );
          K_SIST_TERMINALES         : Result := Forma( 'Terminales', efcSistTerminales ); // SYNERGY
          K_SIST_LISTA_GRUPOS       : Result := Forma( 'Grupos', efcSistListaGrupos ); // SYNERGY
          //K_SIST_MENSAJES           : Result := Forma( 'Mensajes', efcSistMensajes ); // SYNERGY
          K_SIST_TERM_GRUPOS        : Result := Forma( 'Terminales por Grupo', efcSistTermPorGrupos ); // SYNERGY
          K_SIST_MENSAJES_TERM      : Result := Forma( 'Mensajes por Terminal', efcSistMensajesPorTerminal ); // SYNERGY
          K_SIST_BITACORA_BIO       : Result := Forma( 'Bit�cora Terminales GTI', efcBitacoraBio ); // SYNERGY
{$endif}
          K_SIST_REPORTES_EMAIL     : Result := Folder('Reportes email', iNodo);
          K_SIST_SOLICITUD_CORREOS  : Result := Forma( 'Bit�cora de correos', efcSistSolicitudCorreo );
          K_SIST_CALENDARIO_REPORTES: Result := Forma( 'Administrador de env�os programados', efcSistCalendarioReportes );
          K_SIST_BITACORA_REPORTES  : Result := Forma( 'Bit�cora de reportes email', efcSistBitacoraReportes );
          K_SIST_SOLICITUD_ENVIOS   : Result := Forma( 'Solicitudes de env�os programados', efcSolicitudEnviosProgramados );

         // Revueltos
          K_EMP_EXP_CURSOS_PROGRAMADOS: Result := Forma( 'Cursos Programados', efcHisCursosProg );
          // Cafeter�a
          K_EMP_CAFETERIA           : Result := Folder( 'Cafeter�a', iNodo );
          K_EMP_CAFE_COMIDAS_DIA    : Result := Forma( 'Comidas Diarias', efcCafDiarias );
          K_EMP_CAFE_COMIDAS_PERIODO: Result := Forma( 'Comidas Per�odo', efcCafPeriodo );
          K_EMP_CAFE_INVITACIONES   : Result := Forma( 'Invitaciones', efcCafInvita );
          K_CAT_CAFETERIA           : Result := Folder( 'Cafeter�a', iNodo );
          K_CAT_CAFE_REGLAS         : Result := Forma( 'Reglas', efcCatReglas );
          K_CAT_CAFE_INVITADORES    : Result := Forma( 'Invitadores', efcCatInvitadores );
          K_CAT_ACCESOS_REGLAS      : Result := Forma( 'Reglas de Caseta', efcCatAccReglas );
          K_SIST_DATOS_IMPRESORAS   : Result := Forma( 'Impresoras', efcImpresoras );
          K_NOM_EXCEP_GLOBALES      : Result := Forma( 'Globales', efcNomExcepGlobales );
          K_ASIS_DATOS_AUTORIZACIONES: Result := Forma( 'Autorizaciones', efcAutorizaciones );
          K_TAB_HIST_AUT_EXTRAS     : Result := Forma( 'Motivos Autorizaciones', efcTOMotAuto );
          K_TAB_OFI_TIPOS_CAMBIO    : Result := Forma( 'Tipos de Cambios', efcTipoCambio );
          K_CAT_HERRAMIENTAS        : Result := Folder( 'Herramientas', iNodo );
          K_CAT_HERR_TOOLS          : Result := Forma( 'Herramientas', efcCatTools );
          K_TAB_HERR_TALLAS         : Result := Forma( 'Tallas', efcTOTallas );
          K_TAB_HERR_MOTTOOL        : Result := Forma( 'Devoluci�n', efcTOMotTool );
          K_EMP_EXP_TOOLS           : Result := Forma( 'Herramientas', efcHisTools );
          K_CLASIFI_CURSO           : Result := Forma( 'Clase de Cursos',  efcTTOClasifiCurso );
          K_ASIS_DATOS_CLASIFI_TEMP : Result := Forma( 'Clasificaciones Temporales', efcAsisClasifiTemp );
          K_CAT_CAPA_AULAS          : Result := Forma( 'Aulas', efcCatAulas );
          K_CAT_CAPA_PREREQUISITOS  : Result := Forma( 'Prerrequisitos por Curso', efcPrerequisitosCurso );
          {*******Capacitaci�n **********}
          K_CAPACITACION            : Result := Folder( 'Capacitaci�n', iNodo );
          K_EMP_EXP_SESIONES        : Result := Forma( 'Grupos', efcCatSesiones );
          K_CAPA_RESERVAS           : Result := Forma( 'Reservaciones de aula', efcReservas );
          K_CAPA_MATRIZ_HBLDS       : Result := Forma( 'Matriz de Habilidades', efcMtzHabilidades );
          K_CAPA_MATRIZ_CURSOS      : Result := Forma( 'Plan de Cursos Programados', efcMtzCursos );

          K_NOM_FONACOT             : Result := Folder( 'Pagos Fonacot', iNodo );
          K_NOM_FONACOT_TOTALES     : Result := Forma( 'Totales', efcNomFonacotTotales );
          K_NOM_FONACOT_EMPLEADO    : Result := Forma( 'Totales por Empleado', efcNomFonacotEmpleado );
          {*******Caseta acl 081008**********}
          K_EMP_CASETA                  : Result := Folder( 'Caseta', iNodo );
          K_EMP_CASETA_DIARIAS          : Result := Forma( 'Checadas Diarias', efcCasDiarias );

          {*******Tablas ACL 140409**********}
          K_TAB_HIST_CHECA_MANUAL   : Result := Forma( 'Motivos Checadas Manuales', efcMotChecaManual );

          {******* Pantallas de Costeo SUG 1904 **********}
{$ifndef DOS_CAPAS}
          K_COSTEO_TRANSFER         : Result := Forma( 'Transferencias', efcCosteoTransferencias );
          K_COSTEO_MOTIVO_TRANSFER  : Result := Forma( 'Motivos de Transferencias', efcCosteoMotTransfer );
          K_CAT_COSTEO_GRUPOS             : Result := Forma( 'Grupos de Costeo', efcCatCosteoGrupos );
          K_CAT_COSTEO_CRITERIOS          : Result := Forma( 'Criterios de Costeo', efcCatCosteoCriterios );
          K_CAT_COSTEO_CRITERIOS_CONCEPTO : Result := Forma( 'Conceptos de Costeo', efcCatCosteoCriteriosPorConcepto ); //JB: Cambio de Nombre
{$endif}   
       else
           Result := Folder( '', 0 );
     end;
end;

procedure CreaArbolDefault( oArbolMgr : TArbolMgr );
var
   i, iNiveles: Integer;

function NodoNivel( const iNivel, iNodo: Integer ): Boolean;
begin
     Result := oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo );
end;

procedure AgregaNodoTabAdic( const oEntidad : TipoEntidad; iNodo: Integer );
begin
     if strLleno( dmSistema.GetNameTabAdic( oEntidad ) ) then
        NodoNivel( 2, iNodo );
end;

begin  // CreaArbolDefault

    if dmCliente.EmpresaAbierta then
    begin
        // ********* Empleados ********
        NodoNivel( 0, K_EMPLEADOS );
          NodoNivel( 1, K_EMP_DATOS );
            NodoNivel( 2, K_EMP_DATOS_IDENTIFICA );
            NodoNivel( 2, K_EMP_DATOS_CONTRATA );
            NodoNivel( 2, K_EMP_DATOS_AREA );
            NodoNivel( 2, K_EMP_DATOS_PERCEP );
            NodoNivel( 2, K_EMP_DATOS_GAFETE_BIOMETRICO );
            NodoNivel( 2, K_EMP_DATOS_OTROS );
           // if Global.HayAdicionales then
               NodoNivel( 2, K_EMP_DATOS_ADICION );
            NodoNivel( 2, K_EMP_DATOS_USUARIO );
          NodoNivel( 1, K_EMP_CURRICULUM );
            NodoNivel( 2, K_EMP_CURR_PERSONALES );
            NodoNivel( 2, K_EMP_CURR_FOTO );
            NodoNivel( 2, K_EMP_CURR_DOCUMENTO ); // (JB) Anexar al expediente del trabajador documentos CR1880 T1107
            NodoNivel( 2, K_EMP_CURR_PARIENTES );
            NodoNivel( 2, K_EMP_CURR_EXPERIENCIA );
            NodoNivel( 2, K_EMP_CURR_CURSOS );
            NodoNivel( 2, K_EMP_CURR_PUESTOS );
          NodoNivel( 1, K_EMP_EXPEDIENTE );
            NodoNivel( 2, K_EMP_EXP_KARDEX );
            NodoNivel( 2, K_EMP_EXP_VACA );
            NodoNivel( 2, K_EMP_EXP_INCA );
            NodoNivel( 2, K_EMP_EXP_PERM );
            NodoNivel( 2, K_EMP_EXP_CURSOS_TOMADOS );
            NodoNivel( 2, K_EMP_EXP_CURSOS_PROGRAMADOS );
            NodoNivel( 2, K_EMP_EXP_CERTIFICACIONES );
            NodoNivel( 2, K_EMP_EXP_CERTIFIC_PROG );
            NodoNivel( 2, K_EMP_EXP_PAGOS_IMSS );
            NodoNivel( 2, K_EMP_EXP_TOOLS );
            NodoNivel( 2, K_EMP_EXP_INFONAVIT );
            NodoNivel( 2, K_EMP_EXP_SGM );
            {$ifndef DOS_CAPAS}
            if ( not dmCliente.Sentinel_EsProfesional_MSSQL )then
            begin
                 NodoNivel( 2, K_EMP_PLANCAPACITA );
                 NodoNivel( 2, K_EMP_EVA_COMPETEM );
            end;
            {$endif}
          NodoNivel( 1, K_EMP_NOMINA );
            NodoNivel( 2, K_EMP_NOM_AHORROS );
            NodoNivel( 2, K_EMP_NOM_PRESTAMOS );
            NodoNivel( 2, K_EMP_NOM_PENSIONES );
            NodoNivel( 2, K_EMP_NOM_ACUMULA );
            NodoNivel( 2, K_EMP_NOM_ACUMULA_RS );
            NodoNivel( 2, K_EMP_NOM_HISTORIAL );
            NodoNivel (2,K_EMP_SIM_FINIQUITOS);
              NodoNivel( 3, K_EMP_SIM_FINIQUITOS_TOTALES );
              NodoNivel( 3, K_EMP_NOM_SIM_FINIQUITOS );
          NodoNivel( 1, K_EMP_CAFETERIA );
            NodoNivel( 2, K_EMP_CAFE_COMIDAS_DIA );
            NodoNivel( 2, K_EMP_CAFE_COMIDAS_PERIODO );
            NodoNivel( 2, K_EMP_CAFE_INVITACIONES );
        // ********* Caseta **********{acl 0811008}
          NodoNivel( 1, K_EMP_CASETA );
            NodoNivel( 2, K_EMP_CASETA_DIARIAS );
        // ********* Registro **********
          NodoNivel( 1, K_EMP_REGISTRO );
        // ********* Proceso **********
          NodoNivel( 1, K_EMP_PROCESOS );
        // ********* Asistencia **********
        if NodoNivel( 0, K_ASISTENCIA ) then
        begin
             if NodoNivel( 1, K_ASIS_DATOS ) then
             begin
                  NodoNivel( 2, K_ASIS_DATOS_TARJETA_DIARIA );
                  NodoNivel( 2, K_ASIS_DATOS_PRENOMINA );
                  NodoNivel( 2, K_ASIS_DATOS_CALENDARIO );
                  NodoNivel( 2, K_ASIS_DATOS_AUTORIZACIONES );
                  NodoNivel( 2, K_ASIS_DATOS_CLASIFI_TEMP );
             end;
             NodoNivel( 1, K_ASIS_REGISTRO );
             NodoNivel( 1, K_ASIS_PROCESOS );
        end;
        // ********* N�minas ************
        if NodoNivel( 0, K_NOMINA ) then
        begin
             if NodoNivel( 1, K_NOM_DATOS ) then
             begin
                  NodoNivel( 2, K_NOM_DATOS_TOTALES );
                  NodoNivel( 2, K_NOM_DATOS_DIAS_HORAS );
                  NodoNivel( 2, K_NOM_DATOS_MONTOS );
                  NodoNivel( 2, K_NOM_DATOS_PRENOMINA );
                  NodoNivel( 2, K_NOM_DATOS_CLASIFI );
                  NodoNivel( 2, K_NOM_DATOS_POLIZAS);
             end;
             if NodoNivel( 1, K_NOM_EXCEPCIONES ) then
             begin
                  NodoNivel( 2, K_NOM_EXCEP_DIAS_HORAS );
                  NodoNivel( 2, K_NOM_EXCEP_MONTOS );
                  NodoNivel( 2, K_NOM_EXCEP_GLOBALES );
                  //JB:
                  if ( dmCliente.ModuloAutorizadoCosteo ) then
                       NodoNivel( 2, K_COSTEO_TRANSFER );
             end;
             if NodoNivel( 1, K_NOM_FONACOT ) then
             begin
                  NodoNivel( 2, K_NOM_FONACOT_TOTALES );
                  NodoNivel( 2, K_NOM_FONACOT_EMPLEADO );
             end;
             NodoNivel( 1, K_NOM_REGISTRO );
             NodoNivel( 1, K_NOM_PROCESOS );
             NodoNivel( 1, K_NOM_ANUALES );
        end;
        // ******** Pagos IMSS ***********
        NodoNivel( 0, K_IMSS );
          NodoNivel( 1, K_IMSS_DATOS );
            NodoNivel( 2, K_IMSS_DATOS_TOTALES );
            NodoNivel( 2, K_IMSS_DATOS_EMP_MEN );
            NodoNivel( 2, K_IMSS_DATOS_EMP_BIM );
            NodoNivel( 2, K_IMSS_CONCILIA_INFONAVIT );
          NodoNivel( 1, K_IMSS_HISTORIA );
            NodoNivel( 2, K_IMSS_HIST_EMP_MEN );
            NodoNivel( 2, K_IMSS_HIST_EMP_BIM );
          NodoNivel( 1, K_IMSS_REGISTRO );
          NodoNivel( 1, K_IMSS_PROCESOS );
        // ******** Capacitaci�n **********
        NodoNivel( 0, K_CAPACITACION );
          NodoNivel( 1, K_EMP_EXP_SESIONES );
          NodoNivel( 1, K_CAPA_RESERVAS );
          {$ifndef DOS_CAPAS}
          if dmCliente.ModuloAutorizadoDerechos(okCursos) then
          begin
               NodoNivel( 1, K_CAPA_MATRIZ_CURSOS );
               if ( not dmCliente.Sentinel_EsProfesional_MSSQL ) then
                  NodoNivel( 1, K_CAPA_MATRIZ_HBLDS );
          end;
          {$endif}
        // ******** Consultas ***********
        NodoNivel( 0, K_CONSULTAS );
          NodoNivel( 1, K_CONS_REPORTES );
          NodoNivel( 1, K_CONS_ENVIOS_PROGRAMADOS );
          NodoNivel( 1, K_CONS_CONTEO_PERSONAL );
          NodoNivel( 1, K_CONS_ORGANIGRAMA );
          NodoNivel( 1, K_CONS_PLANVACACION );
          NodoNivel( 1, K_CONS_GRAFICA_CONTEO );
          NodoNivel( 1, K_CONS_SQL );
          NodoNivel( 1, K_CONS_BITACORA );
          NodoNivel( 1, K_CONS_PROCESOS );
          NodoNivel( 1, K_CONS_TABLEROS );
            NodoNivel( 2, K_CONS_INICIO );
            NodoNivel( 2, K_CONS_MOSTRAR_GRAFICAS );
        // ******* Cat�logos *************
        NodoNivel( 0, K_CATALOGOS );
          NodoNivel( 1, K_CAT_CONTRATACION );
            NodoNivel( 2, K_CAT_CONT_PUESTOS );
            NodoNivel( 2, K_CAT_CONT_CLASIFI );
            NodoNivel( 2, K_CAT_CONT_PER_FIJAS );
            NodoNivel( 2, K_CAT_CONT_TURNOS );
            NodoNivel( 2, K_CAT_CONT_HORARIOS );
            NodoNivel( 2, K_CAT_CONT_PRESTA );
            NodoNivel( 2, K_CAT_CONT_CONTRATO );
          NodoNivel( 1, K_CAT_DESCRIPCION_PUESTOS );
            NodoNivel( 2, K_CAT_DESC_PERF_PUESTOS );
            NodoNivel( 2, K_CAT_DESC_SECC_PERF );
            NodoNivel( 2, K_CAT_DESC_CAMPOS_PERF );
    {$IFNDEF DOS_CAPAS}
          NodoNivel( 1, K_CAT_VALUACION_PUESTOS );
            NodoNivel( 2, K_CAT_VALUACIONES );
            NodoNivel( 2, K_CAT_VAL_PLANTILLAS );
            NodoNivel( 2, K_CAT_VAL_FACTORES );
            NodoNivel( 2, K_CAT_VAL_SUBFACTORES );
    {$ENDIF}
          NodoNivel( 1, K_CAT_NOMINA );
            NodoNivel( 2, K_CAT_NOMINA_CONCEPTOS );
            NodoNivel( 2, K_CAT_NOMINA_PARAMETROS );
            NodoNivel( 2, K_CAT_NOMINA_PERIODOS );
            NodoNivel( 2, K_CAT_NOMINA_FOLIOS );
            NodoNivel( 2, K_CAT_NOMINA_POLIZAS );
    {$ifdef QUINCENALES}
            NodoNivel( 2, K_CAT_NOMINA_TPERIODOS );
    {$endif}
            //Esta revisando si es corporativo y profesionay y si tiene autorizado supervisores
            if  ( dmCliente.ModuloAutorizadoCosteo ) then
            begin
                 NodoNivel( 2, K_CAT_COSTEO_GRUPOS );
                 NodoNivel( 2, K_CAT_COSTEO_CRITERIOS );
                 NodoNivel( 2, K_CAT_COSTEO_CRITERIOS_CONCEPTO );
            end;
            NodoNivel( 2, K_CAT_NOMINA_TPENSION );
          NodoNivel( 1, K_CAT_GENERALES );
            NodoNivel( 2, K_CAT_GRALES_SOCIALES );
            NodoNivel( 2, K_CAT_GRALES_PATRONALES );
            NodoNivel( 2, K_CAT_GRALES_FEST_GRAL );
            NodoNivel( 2, K_CAT_GRALES_FEST_TURNO );
            NodoNivel( 2, K_CAT_GRALES_CONDICIONES );
            NodoNivel( 2, K_CAT_GRALES_EVENTOS );
            NodoNivel( 2, K_CAT_ACCESOS_REGLAS );
            NodoNivel( 2, K_CAT_GRALES_SGM );
            NodoNivel( 2, K_CAT_GRALES_TABLA_AMORT );
          NodoNivel( 1, K_CAT_CAPACITACION );
            NodoNivel( 2, K_CAT_CAPA_TIPO_CURSO );
            NodoNivel( 2, K_CLASIFI_CURSO );
            NodoNivel( 2, K_CAT_CAPA_CURSOS );
            NodoNivel( 2, K_CAT_CAPA_MAESTROS );
            NodoNivel( 2, K_CAT_CAPA_PROVEEDORES );
            NodoNivel( 2, K_CAT_CAPA_MATRIZ_CURSO );
            NodoNivel( 2, K_CAT_CAPA_MATRIZ_PUESTO );
            NodoNivel( 2, K_CAT_CAPA_MATRIZ_CERT_PUESTO );
            NodoNivel( 2, K_CAT_CAPA_MATRIZ_PUESTO_CERT );
            NodoNivel( 2, K_CAT_CAPA_CALENDARIO );
            NodoNivel( 2, K_CAT_CAPA_AULAS );
            NodoNivel( 2, K_CAT_CAPA_PREREQUISITOS );
            NodoNivel( 2, K_CAT_CERTIFICACIONES );
            NodoNivel( 2, K_CAT_ESTABLECIMIENTOS );
            {$IFNDEF DOS_CAPAS}
          NodoNivel( 1, K_CAT_NODO_COMPETENCIAS );
            NodoNivel( 2, K_CAT_COMPETENCIAS  );
            NodoNivel( 2, K_CAT_PERFILES );
            if ( not dmCliente.Sentinel_EsProfesional_MSSQL )then
               NodoNivel( 2, K_CAT_MAT_PERFILES_COMP );
            {$ENDIF}
          NodoNivel( 1, K_CAT_CONFIGURACION );
            NodoNivel( 2, K_CAT_CONFI_GLOBALES );
            {$IFDEF DOS_CAPAS}
            NodoNivel( 2, K_CAT_CONFI_DICCIONARIO );
            {$ENDIF}
          NodoNivel( 1, K_CAT_CAFETERIA );
            NodoNivel( 2, K_CAT_CAFE_REGLAS );
            NodoNivel( 2, K_CAT_CAFE_INVITADORES );
          NodoNivel( 1, K_CAT_HERRAMIENTAS );
            NodoNivel( 2, K_CAT_HERR_TOOLS );
            NodoNivel( 2, K_TAB_HERR_TALLAS );
            NodoNivel( 2, K_TAB_HERR_MOTTOOL );
        // ******* Tablas *************
        NodoNivel( 0, K_TABLAS );
          NodoNivel( 1, K_TAB_PERSONALES );
            NodoNivel( 2, K_TAB_PER_EDOCIVIL );
            NodoNivel( 2, K_TAB_PER_VIVE_EN );
            NodoNivel( 2, K_TAB_PER_VIVE_CON );
            NodoNivel( 2, K_TAB_PER_ESTUDIOS );
            NodoNivel( 2, K_TAB_PER_TRANSPORTE );
            NodoNivel( 2, K_TAB_PER_ESTADOS );
            NodoNivel( 2, K_TAB_PER_MUNICIPIOS );
            NodoNivel( 2, K_TAB_COLONIAS );
          NodoNivel( 1, K_TAB_AREAS );
            iNiveles := Global.NumNiveles;
            for i := 1 to iNiveles do
            begin
                 if( i > 0 ) and ( i < 10 ) then
                 begin
                      NodoNivel( 2, K_TAB_AREAS + i );
                 {$ifdef ACS}
                 end
                 else if ( i >= 10 ) and ( i <= 12 ) then
                 begin
                     NodoNivel( 2, K_TAB_AREAS2 + i );
                 {$endif}
                 end;
            end;
          NodoNivel( 1, K_TAB_ADICIONALES );
    {
            for i := 1 to K_GLOBAL_TAB_MAX do
              if StrLleno( Global.AdTabla( i ) ) then
                 NodoNivel( 2, K_TAB_ADICIONALES + i );
    }
            AgregaNodoTabAdic( enExtra1, K_TAB_ADICION_TABLA1 );
            AgregaNodoTabAdic( enExtra2, K_TAB_ADICION_TABLA2 );
            AgregaNodoTabAdic( enExtra3, K_TAB_ADICION_TABLA3 );
            AgregaNodoTabAdic( enExtra4, K_TAB_ADICION_TABLA4 );
            AgregaNodoTabAdic( enExtra5, K_TAB_ADICION_TABLA5 );
            AgregaNodoTabAdic( enExtra6, K_TAB_ADICION_TABLA6 );
            AgregaNodoTabAdic( enExtra7, K_TAB_ADICION_TABLA7 );
            AgregaNodoTabAdic( enExtra8, K_TAB_ADICION_TABLA8 );
            AgregaNodoTabAdic( enExtra9, K_TAB_ADICION_TABLA9 );
            AgregaNodoTabAdic( enExtra10, K_TAB_ADICION_TABLA10 );
            AgregaNodoTabAdic( enExtra11, K_TAB_ADICION_TABLA11 );
            AgregaNodoTabAdic( enExtra12, K_TAB_ADICION_TABLA12 );
            AgregaNodoTabAdic( enExtra13, K_TAB_ADICION_TABLA13 );
            AgregaNodoTabAdic( enExtra14, K_TAB_ADICION_TABLA14 );
          NodoNivel( 1, K_TAB_HISTORIAL );
            NodoNivel( 2, K_TAB_HIST_TIPO_KARDEX );
            NodoNivel( 2, K_TAB_HIST_MOT_BAJA );
            NodoNivel( 2, K_TAB_HIST_INCIDEN );
            NodoNivel( 2, K_TAB_HIST_AUT_EXTRAS );
            NodoNivel( 2, K_TAB_HIST_CHECA_MANUAL );
            if ( dmCliente.ModuloAutorizadoCosteo ) then
               NodoNivel( 2, K_COSTEO_MOTIVO_TRANSFER );
          NodoNivel( 1, K_TAB_NOMINA );
            NodoNivel( 2, K_TAB_NOM_TIPO_AHORRO );
            NodoNivel( 2, K_TAB_NOM_TIPO_PRESTA );
            NodoNivel( 2, K_TAB_NOM_MONEDAS );
            NodoNivel( 2, K_TAB_NOM_REGLAS_PRESTA );
          NodoNivel( 1, K_TAB_OFICIALES );
            NodoNivel( 2, K_TAB_OFI_SAL_MINIMOS );
            NodoNivel( 2, K_TAB_OFI_CUOTAS_IMSS );
            NodoNivel( 2, K_TAB_OFI_ISPT_NUMERICAS );
            NodoNivel( 2, K_TAB_OFI_GRADOS_RIESGO );
            NodoNivel( 2, K_TAB_OFI_TIPOS_CAMBIO );
            NodoNivel( 2, K_TAB_OFI_STPS  );
                NodoNivel( 3, K_TAB_OFI_STPS_CAT_NAC_OCUP );
                NodoNivel( 3, K_TAB_OFI_STPS_ARE_TEM_CUR  );
            NodoNivel( 2, K_TAB_OFI_SAT  );
                NodoNivel( 3, K_TAB_OFI_SAT_TIPOS_CONCEPTOS);
                NodoNivel( 3, K_TAB_OFI_SAT_TIPO_CONTRATO );
                NodoNivel( 3, K_TAB_OFI_SAT_TIPO_JORNADA );
                NodoNivel( 3, K_TAB_OFI_SAT_TIPO_RIESGO_PUESTO);
    {$IFNDEF DOS_CAPAS}
            NodoNivel( 2, K_TAB_OFI_CAT_NAC_COMP );
            NodoNivel( 2, K_TAB_BANCOS );
            NodoNivel( 2, K_TAB_VAL_UMA );
          NodoNivel( 1, K_TAB_CAPACITACION );
            NodoNivel( 2, K_TAB_TCOMPETENCIAS );
            NodoNivel( 2, K_TAB_TPERFILES );
    {$ENDIF}
    end;
    
    // ******* Sistema *************
    NodoNivel( 0, K_SISTEMA );
      NodoNivel( 1, K_SIST_DATOS );
        NodoNivel( 2, K_SIST_BASEDATOS);
        NodoNivel( 2, K_SIST_ACTUALIZAR_BDS);
        NodoNivel( 2, K_SIST_DATOS_EMPRESAS );
        NodoNivel( 2, K_SIST_DATOS_GRUPOS );
        NodoNivel( 2, K_SIST_DATOS_ROLES );
        NodoNivel( 2, K_SIST_DATOS_USUARIOS );
        NodoNivel( 2, K_SIST_DATOS_POLL_PENDIENTES );
        NodoNivel( 2, K_SIST_DATOS_IMPRESORAS );
        NodoNivel( 2, K_SIST_NIVEL0 );
        NodoNivel( 2, K_SIST_BITACORA );
        NodoNivel( 2, K_SIST_LST_DISPOSITIVOS );
        // NodoNivel( 2, K_SIST_BASEDATOS);
{$ifndef DOS_CAPAS}
      NodoNivel( 1, K_SIST_TERMINALESGTI );
        NodoNivel( 2, K_SIST_TERMINALES ); // SYNERGY
        NodoNivel( 2, K_SIST_LISTA_GRUPOS ); // SYNERGY
        //NodoNivel( 2, K_SIST_MENSAJES ); // SYNERGY
        NodoNivel( 2, K_SIST_TERM_GRUPOS ); // SYNERGY
        NodoNivel( 2, K_SIST_MENSAJES_TERM ); // SYNERGY
        NodoNivel( 2, K_SIST_BITACORA_BIO ); // SYNERGY
{$endif}
     if dmCliente.EmpresaAbierta then
     begin
          NodoNivel( 1, K_SIST_REPORTES_EMAIL );
            NodoNivel( 2, K_SIST_CALENDARIO_REPORTES );
            NodoNivel( 2, K_SIST_SOLICITUD_ENVIOS );
            NodoNivel( 2, K_SIST_BITACORA_REPORTES );
            NodoNivel( 2, K_SIST_SOLICITUD_CORREOS );
     end;

      NodoNivel( 1, K_SIST_REGISTRO );
      NodoNivel( 1, K_SIST_PROCESOS );
end;

procedure GetDatosArbolUsuario( oArbolMgr: TArbolMgr; DataSet: TDataSet; const sFileName: String );
var
   iUsuario: Integer;
   lImportando: Boolean;
begin
     // SUPONE que 'cdsUsuarios' est� posicionado en el usuario correcto
     with Dataset, oArbolMgr.DatosUsuario do
     begin
          iUsuario          := FieldByName( 'US_CODIGO' ).AsInteger;
          NombreNodoInicial := FieldByName( 'US_NOMBRE' ).AsString;
          IncluirSistema    := zStrToBool( FieldByName( 'US_ARBOL' ).AsString );
          NodoDefault       := FieldByName( 'US_FORMA' ).AsInteger;
          TieneArbol        := FALSE;
          if(DmCLiente.EmpresaAbierta) then NombreNodoInicial := 'Sistema';
     end;
     lImportando := StrLleno( sFileName );

     // Se trae los registros de Arbol del Servidor
     with dmSistema, cdsArbol, oArbolMgr.DatosUsuario do
     begin
            if ( lImportando ) then
              cdsArbol.LoadFromFile( sFileName )
            else
               GetArbolUsuario( iUsuario );

        if not IsEmpty then
        begin
             // Busca el Primer registro para obtener nombre del Nodo
             if ( FieldByName( 'AB_ORDEN' ).AsInteger <= 0 ) then
             begin
               // En caso de IMPORTACION
               // En el primer registro va 'IncluirSistema' y 'Forma Default'
               if ( lImportando ) then
               begin
                    IncluirSistema := FieldByName( 'AB_NIVEL' ).AsInteger > 0;
                    NodoDefault    := FieldByName( 'AB_NODO' ).AsInteger;
               end
               else // Si viene de Base de Datos, permite renombrar RAIZ
               begin
                    if StrLleno( FieldByName( 'AB_DESCRIP' ).AsString ) then
                       NombreNodoInicial:= FieldByName('AB_DESCRIP').AsString;
               end;
               Next;
             end;
             TieneArbol := Not Eof;
        end;
        if ( NodoDefault = 0 ) then
           NodoDefault     := K_EMP_REGISTRO;
     end;
end;

procedure CreaArbolDefaultSistema( oArbolMgr : TArbolMgr );

function NodoNivel( const iNivel, iNodo: Integer ): Boolean;
begin
     Result := oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo );
end;

procedure AgregaNodoTabAdic( const oEntidad : TipoEntidad; iNodo: Integer );
begin
     if strLleno( dmSistema.GetNameTabAdic( oEntidad ) ) then
        NodoNivel( 2, iNodo );
end;

begin  // CreaArbolDefaultSistema

    // ******* Sistema *************

        NodoNivel( 0, K_SIST_DATOS );
        NodoNivel( 1, K_SIST_BASEDATOS);
        NodoNivel( 1, K_SIST_ACTUALIZAR_BDS);
        NodoNivel( 1, K_SIST_DATOS_EMPRESAS );
        NodoNivel( 1, K_SIST_DATOS_GRUPOS );
        NodoNivel( 1, K_SIST_DATOS_ROLES );
        NodoNivel( 1, K_SIST_DATOS_USUARIOS );
        NodoNivel( 1, K_SIST_DATOS_POLL_PENDIENTES );
        NodoNivel( 1, K_SIST_DATOS_IMPRESORAS );
        NodoNivel( 1, K_SIST_NIVEL0 );
        NodoNivel( 1, K_SIST_BITACORA );
        NodoNivel( 1, K_SIST_LST_DISPOSITIVOS );
        // NodoNivel( 2, K_SIST_BASEDATOS);
{$ifndef DOS_CAPAS}
      NodoNivel( 0, K_SIST_TERMINALESGTI );
        NodoNivel( 1, K_SIST_TERMINALES ); // SYNERGY
        NodoNivel( 1, K_SIST_LISTA_GRUPOS ); // SYNERGY
        //NodoNivel( 2, K_SIST_MENSAJES ); // SYNERGY
        NodoNivel( 1, K_SIST_TERM_GRUPOS ); // SYNERGY
        NodoNivel( 1, K_SIST_MENSAJES_TERM ); // SYNERGY
        NodoNivel( 1, K_SIST_BITACORA_BIO ); // SYNERGY
{$endif}
     if dmCliente.EmpresaAbierta then
     begin
          NodoNivel( 0, K_SIST_REPORTES_EMAIL );
            NodoNivel( 1, K_SIST_CALENDARIO_REPORTES );
            NodoNivel( 1, K_SIST_SOLICITUD_ENVIOS );
            NodoNivel( 1, K_SIST_BITACORA_REPORTES );
            NodoNivel( 1, K_SIST_SOLICITUD_CORREOS );
     end;

      NodoNivel( 0, K_SIST_REGISTRO );
      NodoNivel( 0, K_SIST_PROCESOS );
end;


procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String);
var
   NodoRaiz: TNodoInfo;
   DataSet: TDataSet;

  procedure NodoNivel( const iNivel, iNodo : Integer; const sCaption : String );
  var
     oNodoInfo : TNodoInfo;
  begin
       oNodoInfo := GetNodoInfo( iNodo );
       oNodoInfo.Caption := sCaption;
       oArbolMgr.NodoNivel( iNivel, oNodoInfo, iNodo );
  end;

begin  // CreaArbolUsuario
       if ( oArbolMgr.Configuracion ) then
          DataSet := dmSistema.cdsUsuarios
       else
           DataSet := dmCliente.cdsUsuario;

        GetDatosArbolUsuario( oArbolMgr, DataSet, sFileName );
        with oArbolMgr.DatosUsuario do
            begin
            oArbolMgr.NumDefault := NodoDefault;

            // Agrega el Nodo Raiz con el Nombre del Usuario
            if ( oArbolMgr.Configuracion ) or ( TieneArbol ) then
               with NodoRaiz do
               begin
                    Caption := NombreNodoInicial;
                    EsForma := FALSE;
                    IndexDerechos := K_SIN_RESTRICCION;
                    oArbolMgr.NodoNivel( 0, NodoRaiz, K_ARBOL_NODO_RAIZ );
               end;

            if TieneArbol then
              with dmSistema.cdsArbol do
                   while not Eof do
                   begin
                        {***DevEx(@am): A diferencia de Caja de Ahorro esta validacion no es necesaria en TRESS, porque no se considero dentro
                                                  del dise�o remover el grupo de CONSULTAS de la NavBar***}

                        {

					              //edit by MP: VistaClasica y Omision de Campos del Navbar
                        if ( dmCliente.GetDatosUsuarioActivo.Vista = tvClasica )then
                          begin
                              NodoNivel( FieldByName( 'AB_NIVEL' ).AsInteger,
                              FieldByName( 'AB_NODO' ).AsInteger,
                              FieldByName( 'AB_DESCRIP' ).AsString );
                          end
                        else
                            begin
                                  //Checamos que no se de los nodos eliminados para la vista actualporque ya se encuentran en el ribbon.
                                  if (( FieldByName( 'AB_NODO' ).AsInteger <> K_CONS_REPORTES ) and
                                  ( FieldByName( 'AB_NODO' ).AsInteger <> K_CONS_SQL      ) and
                                  ( FieldByName( 'AB_NODO' ).AsInteger <> K_CONS_BITACORA ) and
                                  ( FieldByName( 'AB_NODO' ).AsInteger <> K_CONS_PROCESOS ) and
                                  ( FieldByName( 'AB_NODO' ).AsInteger <> K_CONSULTAS     )) then
                                  begin
                                        NodoNivel( FieldByName( 'AB_NIVEL' ).AsInteger,
                                        FieldByName( 'AB_NODO' ).AsInteger,
                                        FieldByName( 'AB_DESCRIP' ).AsString );
                                  end;//End if 2
                            end; //End if 1 } //OLD
                            NodoNivel( FieldByName( 'AB_NIVEL' ).AsInteger,
                              FieldByName( 'AB_NODO' ).AsInteger,
                              FieldByName( 'AB_DESCRIP' ).AsString );
                            Next;
                      end;

            // Agregar el Arbol de Sistema cuando:
            //  Se trate del Shell y
            //     -Usuario tiene su propio Arbol y decidi� Incluir Sistema, o
            //     -Usuario NO tiene su propio Arbol
          if ( not oArbolMgr.Configuracion ) then
          begin
             if ( IncluirSistema or ( not TieneArbol )) then
                ZArbolTress.CreaArbolDefault( oArbolMgr );

          // Si por derechos no tiene nada, agregar nodo
          //Edit By MP : Valido si el arbol TTreeview existe
          if(oArbolMgr.Arbol = nil) then
          begin
             	  if ( oArbolMgr.Arbol_DevEx.Items.Count = 0 ) then //Si no existe utilizo el arbol_Devex
               	begin
               		  with NodoRaiz do
                  	begin
                        Caption := 'No tiene derechos';
                        EsForma := FALSE;
                        IndexDerechos := K_SIN_RESTRICCION;
                        oArbolMgr.NodoNivel( 0, NodoRaiz, K_ARBOL_NODO_RAIZ );
                  	end;
               	end;
          end
          else
          begin
              if ( oArbolMgr.Arbol.Items.Count = 0 ) then
              begin
                with NodoRaiz do
                begin
                  Caption := 'No tiene derechos';
                  EsForma := FALSE;
                  IndexDerechos := K_SIN_RESTRICCION;
                  oArbolMgr.NodoNivel( 0, NodoRaiz, K_ARBOL_NODO_RAIZ );
                end;
          end;
        end;
            end;
          end;
//fin Edit
end;

// Pruebas
procedure CreaArbolUsuarioSistema( oArbolMgr: TArbolMgr; const sFileName: String );
var
   NodoRaiz: TNodoInfo;
   DataSet: TDataSet;

  procedure NodoNivel( const iNivel, iNodo : Integer; const sCaption : String );
  var
     oNodoInfo : TNodoInfo;
  begin
       oNodoInfo := GetNodoInfo( iNodo );
       oNodoInfo.Caption := sCaption;
       oArbolMgr.NodoNivel( iNivel, oNodoInfo, iNodo );
  end;

begin  // CreaArbolUsuario
       if ( oArbolMgr.Configuracion ) then
          DataSet := dmSistema.cdsUsuarios
       else
           DataSet := dmCliente.cdsUsuario;

       GetDatosArbolUsuario( oArbolMgr, DataSet, sFileName );
       with oArbolMgr.DatosUsuario do
       begin
          oArbolMgr.NumDefault := NodoDefault;

          // Agrega el Nodo Raiz con el Nombre del Usuario
          if ( oArbolMgr.Configuracion ) or ( TieneArbol ) then
              with NodoRaiz do
              begin
                   Caption := NombreNodoInicial;
                   EsForma := FALSE;
                   IndexDerechos := K_SIN_RESTRICCION;
                   oArbolMgr.NodoNivel( 0, NodoRaiz, K_ARBOL_NODO_RAIZ );
              end;

          if TieneArbol then
            with dmSistema.cdsArbol do
              while not Eof do
              begin
                   NodoNivel( FieldByName( 'AB_NIVEL' ).AsInteger,
                              FieldByName( 'AB_NODO' ).AsInteger,
                              FieldByName( 'AB_DESCRIP' ).AsString );
                   Next;
              end;

          // Agregar el Arbol de Sistema cuando:
          //  Se trate del Shell y
          //     -Usuario tiene su propio Arbol y decidi� Incluir Sistema, o
          //     -Usuario NO tiene su propio Arbol
          if ( not oArbolMgr.Configuracion ) then
          begin
             if ( IncluirSistema or ( not TieneArbol )) then
                // ZArbolTress.CreaArbolDefault( oArbolMgr );
                ZArbolTress.CreaArbolDefaultSistema( oArbolMgr );

             // Si por derechos no tiene nada, agregar nodo
             if ( oArbolMgr.Arbol.Items.Count = 0 ) then
             begin
                with NodoRaiz do
                begin
                     Caption := 'No tiene derechos';
                     EsForma := FALSE;
                     IndexDerechos := K_SIN_RESTRICCION;
                     oArbolMgr.NodoNivel( 0, NodoRaiz, K_ARBOL_NODO_RAIZ );
                end;
             end;
          end;
        end;

end;
// ----- -----

procedure MuestraArbolOriginal( oArbol: TTreeView );
var
   oNodo: TTreeNode;
   iNodo: Integer;
begin
     oArbol.Items.BeginUpdate;
     oNodo := oArbol.Items.GetFirstNode;
     while ( oNodo <> NIL ) do
     begin
          iNodo := Integer( oNodo.Data );
          if ( iNodo > 0 ) then
             oNodo.Text := GetNodoInfo( iNodo ).Caption;
          oNodo := oNodo.GetNext;
     end;
     oArbol.Items.EndUpdate;
end;

//DevEx(@am): Agregado para ver el nombre original de los nodos del arbol de usuario.
procedure MuestraArbolOriginal( oArbol: TcxTreeView );
var
   oNodo: TTreeNode;
   iNodo: Integer;
begin
     oArbol.Items.BeginUpdate;
     oNodo := oArbol.Items.GetFirstNode;
     while ( oNodo <> NIL ) do
     begin
          iNodo := Integer( oNodo.Data );
          if ( iNodo > 0 ) then
             oNodo.Text := GetNodoInfo( iNodo ).Caption;
          oNodo := oNodo.GetNext;
     end;
     oArbol.Items.EndUpdate;
end;

//Edit by MP: Creacion de Arbolitos para el NavBar
procedure CreaArbolitoDefault( oArbolMgr: TArbolMgr ; iAccesoGlobal: Integer );
var
   i, iNiveles: Integer;

function NodoNivel( const iNivel, iNodo: Integer ): Boolean;
begin
     Result := oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo );
end;

procedure AgregaNodoTabAdic( const oEntidad : TipoEntidad; iNodo: Integer );
begin
     if strLleno( dmSistema.GetNameTabAdic( oEntidad ) ) then
     NodoNivel(1,iNodo);
end;

begin  // CreaArbolDefault
    case iAccesoGlobal of
    // ********* Empleados ********
    K_EMPLEADOS:
    begin
      NodoNivel( 0, K_EMP_DATOS );
        NodoNivel( 1, K_EMP_DATOS_IDENTIFICA );
        NodoNivel( 1, K_EMP_DATOS_CONTRATA );
        NodoNivel( 1, K_EMP_DATOS_AREA );
        NodoNivel( 1, K_EMP_DATOS_PERCEP );
        NodoNivel( 1, K_EMP_DATOS_GAFETE_BIOMETRICO );
        NodoNivel( 1, K_EMP_DATOS_OTROS );
        //if Global.HayAdicionales then
           NodoNivel( 1, K_EMP_DATOS_ADICION );
        NodoNivel( 1, K_EMP_DATOS_USUARIO );
      NodoNivel( 0, K_EMP_CURRICULUM );
        NodoNivel( 1, K_EMP_CURR_PERSONALES );
        NodoNivel( 1, K_EMP_CURR_FOTO );
        NodoNivel( 1, K_EMP_CURR_DOCUMENTO ); // (JB) Anexar al expediente del trabajador documentos CR1880 T1107
        NodoNivel( 1, K_EMP_CURR_PARIENTES );
        NodoNivel( 1, K_EMP_CURR_EXPERIENCIA );
        NodoNivel( 1, K_EMP_CURR_CURSOS );
        NodoNivel( 1, K_EMP_CURR_PUESTOS );
      NodoNivel( 0, K_EMP_EXPEDIENTE );
        NodoNivel( 1, K_EMP_EXP_KARDEX );
        NodoNivel( 1, K_EMP_EXP_VACA );
        NodoNivel( 1, K_EMP_EXP_INCA );
        NodoNivel( 1, K_EMP_EXP_PERM );
        NodoNivel( 1, K_EMP_EXP_CURSOS_TOMADOS );
        NodoNivel( 1, K_EMP_EXP_CURSOS_PROGRAMADOS );
        NodoNivel( 1, K_EMP_EXP_CERTIFICACIONES );
        NodoNivel( 1, K_EMP_EXP_CERTIFIC_PROG );
        NodoNivel( 1, K_EMP_EXP_PAGOS_IMSS );
        NodoNivel( 1, K_EMP_EXP_TOOLS );
        NodoNivel( 1, K_EMP_EXP_INFONAVIT );
        NodoNivel( 1, K_EMP_EXP_SGM );
        {$ifndef DOS_CAPAS}
        if ( not dmCliente.Sentinel_EsProfesional_MSSQL )then
        begin
           NodoNivel( 1, K_EMP_PLANCAPACITA );
           NodoNivel( 1, K_EMP_EVA_COMPETEM );
        end;
        {$endif}
      NodoNivel( 0, K_EMP_NOMINA );
        NodoNivel( 1, K_EMP_NOM_AHORROS );
        NodoNivel( 1, K_EMP_NOM_PRESTAMOS );
        NodoNivel( 1, K_EMP_NOM_PENSIONES );
        NodoNivel( 1, K_EMP_NOM_ACUMULA );
        NodoNivel( 1, K_EMP_NOM_ACUMULA_RS );
        NodoNivel( 1, K_EMP_NOM_HISTORIAL );
        NodoNivel (1,K_EMP_SIM_FINIQUITOS);
          NodoNivel( 2, K_EMP_SIM_FINIQUITOS_TOTALES );
          NodoNivel( 2, K_EMP_NOM_SIM_FINIQUITOS );
      NodoNivel( 0, K_EMP_CAFETERIA );
        NodoNivel( 1, K_EMP_CAFE_COMIDAS_DIA );
        NodoNivel( 1, K_EMP_CAFE_COMIDAS_PERIODO );
        NodoNivel( 1, K_EMP_CAFE_INVITACIONES );
    // ********* Caseta **********{acl 0811008}
      NodoNivel( 0, K_EMP_CASETA );
        NodoNivel( 1, K_EMP_CASETA_DIARIAS );
    // ********* Registro **********
      NodoNivel( 0, K_EMP_REGISTRO );
    // ********* Proceso **********
      NodoNivel( 0, K_EMP_PROCESOS );
    // ********* Asistencia **********
    end;
    K_ASISTENCIA:
    begin
         if NodoNivel( 0, K_ASIS_DATOS ) then
         begin
              NodoNivel( 1, K_ASIS_DATOS_TARJETA_DIARIA );
              NodoNivel( 1, K_ASIS_DATOS_PRENOMINA );
              NodoNivel( 1, K_ASIS_DATOS_CALENDARIO );
              NodoNivel( 1, K_ASIS_DATOS_AUTORIZACIONES );
              NodoNivel( 1, K_ASIS_DATOS_CLASIFI_TEMP );
         end;
         NodoNivel( 0, K_ASIS_REGISTRO );
         NodoNivel( 0, K_ASIS_PROCESOS );
    end;
    // ********* N�minas ************
    K_NOMINA :
    begin
         if NodoNivel( 0, K_NOM_DATOS ) then
         begin
              NodoNivel( 1, K_NOM_DATOS_TOTALES );
              NodoNivel( 1, K_NOM_DATOS_DIAS_HORAS );
              NodoNivel( 1, K_NOM_DATOS_MONTOS );
              NodoNivel( 1, K_NOM_DATOS_PRENOMINA );
              NodoNivel( 1, K_NOM_DATOS_CLASIFI );
              NodoNivel( 1, K_NOM_DATOS_POLIZAS);
         end;
         if NodoNivel( 0, K_NOM_EXCEPCIONES ) then
         begin
              NodoNivel( 1, K_NOM_EXCEP_DIAS_HORAS );
              NodoNivel( 1, K_NOM_EXCEP_MONTOS );
              NodoNivel( 1, K_NOM_EXCEP_GLOBALES );
              //JB:
              if ( dmCliente.ModuloAutorizadoCosteo ) then
                   NodoNivel( 1, K_COSTEO_TRANSFER );
         end;
         if NodoNivel( 0, K_NOM_FONACOT ) then
         begin
              NodoNivel( 1, K_NOM_FONACOT_TOTALES );
              NodoNivel( 1, K_NOM_FONACOT_EMPLEADO );
         end;
         NodoNivel( 0, K_NOM_REGISTRO );
         NodoNivel( 0, K_NOM_PROCESOS );
         NodoNivel( 0, K_NOM_ANUALES );
    end;
    // ******** Pagos IMSS ***********
    K_IMSS :
    begin
      NodoNivel( 0, K_IMSS_DATOS );
        NodoNivel( 1, K_IMSS_DATOS_TOTALES );
        NodoNivel( 1, K_IMSS_DATOS_EMP_MEN );
        NodoNivel( 1, K_IMSS_DATOS_EMP_BIM );
        NodoNivel( 1, K_IMSS_CONCILIA_INFONAVIT );
      NodoNivel( 0, K_IMSS_HISTORIA );
        NodoNivel( 1, K_IMSS_HIST_EMP_MEN );
        NodoNivel( 1, K_IMSS_HIST_EMP_BIM );
        NodoNivel( 0, K_IMSS_REGISTRO );
        NodoNivel( 0, K_IMSS_PROCESOS );
    // ******** Capacitaci�n **********
    end ;
    K_CAPACITACION :
    begin
      NodoNivel( 0, K_EMP_EXP_SESIONES );
      NodoNivel( 0, K_CAPA_RESERVAS );
      {$ifndef DOS_CAPAS}
      if dmCliente.ModuloAutorizadoDerechos(okCursos) then
      begin
           NodoNivel( 0, K_CAPA_MATRIZ_CURSOS );
           if ( not dmCliente.Sentinel_EsProfesional_MSSQL ) then
              NodoNivel( 0, K_CAPA_MATRIZ_HBLDS );
      end;
      {$endif}
    end;
    // ******** Consultas ***********
    K_CONSULTAS :
    begin
      NodoNivel( 0, K_CONS_REPORTES );
      NodoNivel( 0, K_CONS_ENVIOS_PROGRAMADOS );
      NodoNivel( 0, K_CONS_CONTEO_PERSONAL );
      NodoNivel( 0, K_CONS_ORGANIGRAMA );
      NodoNivel( 0, K_CONS_PLANVACACION );
      NodoNivel( 0, K_CONS_GRAFICA_CONTEO );
      NodoNivel( 0, K_CONS_SQL );
      NodoNivel( 0, K_CONS_BITACORA );
      NodoNivel( 0, K_CONS_PROCESOS );
      NodoNivel( 0, K_CONS_TABLEROS );
        NodoNivel( 1, K_CONS_INICIO );
        NodoNivel( 1, K_CONS_MOSTRAR_GRAFICAS );
    end;
    // ******* Cat�logos *************
    K_CATALOGOS :
    begin
      NodoNivel( 0, K_CAT_CONTRATACION );
        NodoNivel( 1, K_CAT_CONT_PUESTOS );
        NodoNivel( 1, K_CAT_CONT_CLASIFI );
        NodoNivel( 1, K_CAT_CONT_PER_FIJAS );
        NodoNivel( 1, K_CAT_CONT_TURNOS );
        NodoNivel( 1, K_CAT_CONT_HORARIOS );
        NodoNivel( 1, K_CAT_CONT_PRESTA );
        NodoNivel( 1, K_CAT_CONT_CONTRATO );
      NodoNivel( 0, K_CAT_DESCRIPCION_PUESTOS );
        NodoNivel( 1, K_CAT_DESC_PERF_PUESTOS );
        NodoNivel( 1, K_CAT_DESC_SECC_PERF );
        NodoNivel( 1, K_CAT_DESC_CAMPOS_PERF );
{$IFNDEF DOS_CAPAS}
      NodoNivel( 0, K_CAT_VALUACION_PUESTOS );
        NodoNivel( 1, K_CAT_VALUACIONES );
        NodoNivel( 1, K_CAT_VAL_PLANTILLAS );
        NodoNivel( 1, K_CAT_VAL_FACTORES );
        NodoNivel( 1, K_CAT_VAL_SUBFACTORES );
{$ENDIF}
      NodoNivel( 0, K_CAT_NOMINA );
        NodoNivel( 1, K_CAT_NOMINA_CONCEPTOS );
        NodoNivel( 1, K_CAT_NOMINA_PARAMETROS );
        NodoNivel( 1, K_CAT_NOMINA_PERIODOS );
        NodoNivel( 1, K_CAT_NOMINA_FOLIOS );
        NodoNivel( 1, K_CAT_NOMINA_POLIZAS );
{$ifdef QUINCENALES}
        NodoNivel( 1, K_CAT_NOMINA_TPERIODOS );
{$endif}
        //Esta revisando si es corporativo y profesionay y si tiene autorizado supervisores
        if  ( dmCliente.ModuloAutorizadoCosteo ) then
        begin
             NodoNivel( 1, K_CAT_COSTEO_GRUPOS );
             NodoNivel( 1, K_CAT_COSTEO_CRITERIOS );
             NodoNivel( 1, K_CAT_COSTEO_CRITERIOS_CONCEPTO );
        end;
        NodoNivel( 1, K_CAT_NOMINA_TPENSION );
      NodoNivel( 0, K_CAT_GENERALES );
        NodoNivel( 1, K_CAT_GRALES_SOCIALES );
        NodoNivel( 1, K_CAT_GRALES_PATRONALES );
        NodoNivel( 1, K_CAT_GRALES_FEST_GRAL );
        NodoNivel( 1, K_CAT_GRALES_FEST_TURNO );
        NodoNivel( 1, K_CAT_GRALES_CONDICIONES );
        NodoNivel( 1, K_CAT_GRALES_EVENTOS );
        NodoNivel( 1, K_CAT_ACCESOS_REGLAS );
        NodoNivel( 1, K_CAT_GRALES_SGM );
        NodoNivel( 1, K_CAT_GRALES_TABLA_AMORT );
      NodoNivel( 0, K_CAT_CAPACITACION );
        NodoNivel( 1, K_CAT_CAPA_TIPO_CURSO );
        NodoNivel( 1, K_CLASIFI_CURSO );
        NodoNivel( 1, K_CAT_CAPA_CURSOS );
        NodoNivel( 1, K_CAT_CAPA_MAESTROS );
        NodoNivel( 1, K_CAT_CAPA_PROVEEDORES );
        NodoNivel( 1, K_CAT_CAPA_MATRIZ_CURSO );
        NodoNivel( 1, K_CAT_CAPA_MATRIZ_PUESTO );
        NodoNivel( 1, K_CAT_CAPA_MATRIZ_CERT_PUESTO );
        NodoNivel( 1, K_CAT_CAPA_MATRIZ_PUESTO_CERT );
        NodoNivel( 1, K_CAT_CAPA_CALENDARIO );
        NodoNivel( 1, K_CAT_CAPA_AULAS );
        NodoNivel( 1, K_CAT_CAPA_PREREQUISITOS );
        NodoNivel( 1, K_CAT_CERTIFICACIONES );
        NodoNivel( 1, K_CAT_ESTABLECIMIENTOS );
        {$IFNDEF DOS_CAPAS}
      NodoNivel( 0, K_CAT_NODO_COMPETENCIAS );
        NodoNivel( 1, K_CAT_COMPETENCIAS  );
        NodoNivel( 1, K_CAT_PERFILES );
        if ( not dmCliente.Sentinel_EsProfesional_MSSQL )then
           NodoNivel( 1, K_CAT_MAT_PERFILES_COMP );
        {$ENDIF}
      NodoNivel( 0, K_CAT_CONFIGURACION );
        NodoNivel( 1, K_CAT_CONFI_GLOBALES );
        {$IFDEF DOS_CAPAS}
        NodoNivel( 1, K_CAT_CONFI_DICCIONARIO );
        {$ENDIF}
      NodoNivel( 0, K_CAT_CAFETERIA );
        NodoNivel( 1, K_CAT_CAFE_REGLAS );
        NodoNivel( 1, K_CAT_CAFE_INVITADORES );
      NodoNivel( 0, K_CAT_HERRAMIENTAS );
        NodoNivel( 1, K_CAT_HERR_TOOLS );
        NodoNivel( 1, K_TAB_HERR_TALLAS );
        NodoNivel( 1, K_TAB_HERR_MOTTOOL );
    // ******* Tablas *************
    end;
    K_TABLAS :
    begin
      NodoNivel( 0, K_TAB_PERSONALES );
        NodoNivel( 1, K_TAB_PER_EDOCIVIL );
        NodoNivel( 1, K_TAB_PER_VIVE_EN );
        NodoNivel( 1, K_TAB_PER_VIVE_CON );
        NodoNivel( 1, K_TAB_PER_ESTUDIOS );
        NodoNivel( 1, K_TAB_PER_TRANSPORTE );
        NodoNivel( 1, K_TAB_PER_ESTADOS );
        NodoNivel( 1, K_TAB_PER_MUNICIPIOS );
        NodoNivel( 1, K_TAB_COLONIAS );
      NodoNivel( 0, K_TAB_AREAS );
        iNiveles := Global.NumNiveles;
        for i := 1 to iNiveles do
        begin
             if( i > 0 ) and ( i < 10 ) then
             begin
                  NodoNivel( 1, K_TAB_AREAS + i );
             {$ifdef ACS}
             end
             else if ( i >= 10 ) and ( i <= 12 ) then
             begin
                 NodoNivel( 1, K_TAB_AREAS2 + i );
             {$endif}
             end;
        end;
      NodoNivel( 0, K_TAB_ADICIONALES );
{
        for i := 1 to K_GLOBAL_TAB_MAX do
          if StrLleno( Global.AdTabla( i ) ) then
             NodoNivel( 2, K_TAB_ADICIONALES + i );
}
        AgregaNodoTabAdic( enExtra1, K_TAB_ADICION_TABLA1 );
        AgregaNodoTabAdic( enExtra2, K_TAB_ADICION_TABLA2 );
        AgregaNodoTabAdic( enExtra3, K_TAB_ADICION_TABLA3 );
        AgregaNodoTabAdic( enExtra4, K_TAB_ADICION_TABLA4 );
        AgregaNodoTabAdic( enExtra5, K_TAB_ADICION_TABLA5 );
        AgregaNodoTabAdic( enExtra6, K_TAB_ADICION_TABLA6 );
        AgregaNodoTabAdic( enExtra7, K_TAB_ADICION_TABLA7 );
        AgregaNodoTabAdic( enExtra8, K_TAB_ADICION_TABLA8 );
        AgregaNodoTabAdic( enExtra9, K_TAB_ADICION_TABLA9 );
        AgregaNodoTabAdic( enExtra10, K_TAB_ADICION_TABLA10 );
        AgregaNodoTabAdic( enExtra11, K_TAB_ADICION_TABLA11 );
        AgregaNodoTabAdic( enExtra12, K_TAB_ADICION_TABLA12 );
        AgregaNodoTabAdic( enExtra13, K_TAB_ADICION_TABLA13 );
        AgregaNodoTabAdic( enExtra14, K_TAB_ADICION_TABLA14 );
      NodoNivel( 0, K_TAB_HISTORIAL );
        NodoNivel( 1, K_TAB_HIST_TIPO_KARDEX );
        NodoNivel( 1, K_TAB_HIST_MOT_BAJA );
        NodoNivel( 1, K_TAB_HIST_INCIDEN );
        NodoNivel( 1, K_TAB_HIST_AUT_EXTRAS );
        NodoNivel( 1, K_TAB_HIST_CHECA_MANUAL );
        if ( dmCliente.ModuloAutorizadoCosteo ) then
           NodoNivel( 1, K_COSTEO_MOTIVO_TRANSFER );
      NodoNivel( 0, K_TAB_NOMINA );
        NodoNivel( 1, K_TAB_NOM_TIPO_AHORRO );
        NodoNivel( 1, K_TAB_NOM_TIPO_PRESTA );
        NodoNivel( 1, K_TAB_NOM_MONEDAS );
        NodoNivel( 1, K_TAB_NOM_REGLAS_PRESTA );
      NodoNivel( 0, K_TAB_OFICIALES );
        NodoNivel( 1, K_TAB_OFI_SAL_MINIMOS );
        NodoNivel( 1, K_TAB_OFI_CUOTAS_IMSS );
        NodoNivel( 1, K_TAB_OFI_ISPT_NUMERICAS );
        NodoNivel( 1, K_TAB_OFI_GRADOS_RIESGO );
        NodoNivel( 1, K_TAB_OFI_TIPOS_CAMBIO );
        NodoNivel( 1, K_TAB_OFI_STPS  );
            NodoNivel( 2, K_TAB_OFI_STPS_CAT_NAC_OCUP );
            NodoNivel( 2, K_TAB_OFI_STPS_ARE_TEM_CUR  );
        NodoNivel( 1, K_TAB_OFI_SAT  );
            NodoNivel( 2, K_TAB_OFI_SAT_TIPOS_CONCEPTOS);
            NodoNivel( 2, K_TAB_OFI_SAT_TIPO_CONTRATO );
            NodoNivel( 2, K_TAB_OFI_SAT_TIPO_JORNADA );
            NodoNivel( 2, K_TAB_OFI_SAT_TIPO_RIESGO_PUESTO );
{$IFNDEF DOS_CAPAS}
        NodoNivel( 1, K_TAB_OFI_CAT_NAC_COMP );
        NodoNivel( 1, K_TAB_BANCOS );
        NodoNivel( 1, K_TAB_VAL_UMA );
      NodoNivel( 0, K_TAB_CAPACITACION );
        NodoNivel( 1, K_TAB_TCOMPETENCIAS );
        NodoNivel( 1, K_TAB_TPERFILES );
{$ENDIF}
    end;
    // ******* Sistema *************
    K_SISTEMA :
    begin
      NodoNivel( 0, K_SIST_DATOS );
        NodoNivel( 1, K_SIST_BASEDATOS);
        NodoNivel( 1, K_SIST_DATOS_EMPRESAS );
        NodoNivel( 1, K_SIST_DATOS_GRUPOS );
        NodoNivel( 1, K_SIST_DATOS_ROLES );
        NodoNivel( 1, K_SIST_DATOS_USUARIOS );
        NodoNivel( 1, K_SIST_DATOS_POLL_PENDIENTES );
        NodoNivel( 1, K_SIST_DATOS_IMPRESORAS );
        NodoNivel( 1, K_SIST_NIVEL0 );
        NodoNivel( 1, K_SIST_BITACORA );
        NodoNivel( 1, K_SIST_LST_DISPOSITIVOS );
{$ifndef DOS_CAPAS}
      NodoNivel( 0, K_SIST_TERMINALESGTI );
        NodoNivel( 1, K_SIST_TERMINALES ); // SYNERGY
        NodoNivel( 1, K_SIST_LISTA_GRUPOS ); // SYNERGY
        //NodoNivel( 2, K_SIST_MENSAJES ); // SYNERGY
        NodoNivel( 1, K_SIST_TERM_GRUPOS ); // SYNERGY
        NodoNivel( 1, K_SIST_MENSAJES_TERM ); // SYNERGY
        NodoNivel( 1, K_SIST_BITACORA_BIO ); // SYNERGY
{$endif}
     if dmCliente.EmpresaAbierta then
     begin
          NodoNivel( 0, K_SIST_REPORTES_EMAIL );
            NodoNivel( 1, K_SIST_CALENDARIO_REPORTES );
            NodoNivel( 1, K_SIST_SOLICITUD_ENVIOS );
            NodoNivel( 1, K_SIST_BITACORA_REPORTES );
            NodoNivel( 1, K_SIST_SOLICITUD_CORREOS );
     end;
      NodoNivel( 0, K_SIST_REGISTRO );
      NodoNivel( 0, K_SIST_PROCESOS );
      end;
    end;
end;

procedure CreaArbolitoUsuario( oArbolMgr: TArbolMgr; const sFileName: String);
var
   NodoRaiz: TNodoInfo;
   DataSet: TDataSet;

  procedure NodoNivel( const iNivel, iNodo : Integer; const sCaption : String );
  var
     oNodoInfo : TNodoInfo;
  begin
       oNodoInfo := GetNodoInfo( iNodo );
       oNodoInfo.Caption := sCaption;
       oArbolMgr.NodoNivel( iNivel, oNodoInfo, iNodo );
  end;

begin  // CreaArbolUsuario
       if ( oArbolMgr.Configuracion ) then
          DataSet := dmSistema.cdsUsuarios
       else
           DataSet := dmCliente.cdsUsuario;

       GetDatosArbolUsuario( oArbolMgr, DataSet, sFileName );
       with oArbolMgr.DatosUsuario do
       begin
          oArbolMgr.NumDefault := NodoDefault;
          {***DevEx(@am): NO SE NECESITA EL PRIMER NODO ES EL GRUPO***}
          // Agrega el Nodo Raiz con el Nombre del Usuario
          {if ( oArbolMgr.Configuracion ) or ( TieneArbol ) then
              with NodoRaiz do
              begin
                   Caption := NombreNodoInicial;
                   EsForma := FALSE;
                   IndexDerechos := K_SIN_RESTRICCION;
                   oArbolMgr.NodoNivel( 0, NodoRaiz, K_ARBOL_NODO_RAIZ );
              end; }
          if TieneArbol then
          begin
            with dmSistema.cdsArbol do
            begin
                while not Eof do
                begin
                     //DevEx(@am):Reducimos el nivel, pues el primer nive de nodos ahora sera un Grupo.
                    NodoNivel( FieldByName( 'AB_NIVEL' ).AsInteger -1,
                               FieldByName( 'AB_NODO' ).AsInteger,
                               FieldByName( 'AB_DESCRIP' ).AsString );
                    Next;
                end;
            end;
          end;
          // Agregar el Arbol de Sistema cuando:
          //  Se trate del Shell y
          //     -Usuario tiene su propio Arbol y decidi� Incluir Sistema, o
          //     -Usuario NO tiene su propio Arbol
          if ( not oArbolMgr.Configuracion ) then
          begin
             if ( IncluirSistema or ( not TieneArbol )) then
             begin
                if (oArbolMgr.Arbol = nil) then
                begin
                   if ( oArbolMgr.Arbol_DevEx.Items.Count = 0 ) then
                   begin
                      with NodoRaiz do
                      begin
                          Caption := 'No tiene derechos';
                          EsForma := FALSE;
                          IndexDerechos := K_SIN_RESTRICCION;
                          oArbolMgr.NodoNivel( 0, NodoRaiz, K_ARBOL_NODO_RAIZ );
                      end;
                   end;
                end
                else
                begin
                  // Si por derechos no tiene nada, agregar nodo
                  if ( oArbolMgr.Arbol.Items.Count = 0 ) then
                  begin
                    with NodoRaiz do
                    begin
                        Caption := 'No tiene derechos';
                        EsForma := FALSE;
                        IndexDerechos := K_SIN_RESTRICCION;
                        oArbolMgr.NodoNivel( 0, NodoRaiz, K_ARBOL_NODO_RAIZ );
                    end;
                  end;
                end;
             end;
          end;
       end;
//Fin Edit
end;
//Edit by MP: Llamada Modificada Para Utilizar el tcxTreeview
procedure CreaNavBarUsuario (  oNavBarMgr: TNavBarMgr; const sFileName: String; Arbolitos: array of TcxtreeView ; ArbolitoUsuario : TcxTreeView);
var
   DataSet: TDataSet;
   NodoRaiz: TGrupoInfo;
//Edit by MP: Metodo Modificado para usar un TcxTreeview
procedure CreaArbolito( Arbolito: TcxTreeView);
var
   oArbolMgr: TArbolMgr;
begin
     with Arbolito do
     begin
          Parent := oNavBarMgr.NavBar.Groups[oNavBarMgr.UltimoGrupo.Index].Control;
          Align := alClient;
     end;
     oArbolMgr := TArbolMgr.Create( Arbolito );
     with oArbolMgr do
     begin
          try
             with Arbolito.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     // Aqu� se agrega el arbol definido por el usuario

                     CreaArbolitoUsuario( oArbolMgr, '' );
                  finally
                     EndUpdate;
                  end;
             end;
          finally
             Free;
          end;
     end;
end;
begin
     if ( oNavBarMgr.Configuracion ) then
       DataSet := dmSistema.cdsUsuarios
     else
         DataSet := dmCliente.cdsUsuario;


     GetDatosNavBarUsuario(oNavBarMgr, DataSet, sFileName );
     //CreaNavBarUsuario
     with oNavBarMgr.DatosUsuario do
     begin
          oNavBarMgr.NumDefault := NodoDefault;
          // Agrega el Nodo Raiz con el Nombre del Usuario
          if ( oNavBarMgr.Configuracion ) or ( TieneArbol ) then
          begin
             with NodoRaiz do
             begin
                  if(dmcliente.EmpresaAbierta) then
                     Caption := NombreNodoInicial
                  else
                      Caption := 'Sistema';
                  //EsForma := FALSE;
                  IndexDerechos := K_SIN_RESTRICCION;
                  if(dmcliente.EmpresaAbierta) then
                  begin
                    if(oNavBarMgr.NodoNivel(NodoRaiz,0, K_ARBOL_NODO_RAIZ)) then
                      begin
                          CreaArbolito( ArbolitoUsuario ); //Pendiente cambiar el indice
                      end;
                    end;
             end;
          end;
          // Agregar el Arbol de Sistema cuando:
          //  Se trate del Shell y
          //     -Usuario tiene su propio Arbol y decidi� Incluir Sistema, o
          //     -Usuario NO tiene su propio Arbol
          if ( not oNavBarMgr.Configuracion ) then
          begin
             if ( IncluirSistema or ( not TieneArbol )) then
             begin
                if (oNavBarMgr.NumDefault = 0 ) then
                    oNavBarMgr.NumDefault :=  K_Empleados; //Por si solo se agregara el arbol del sistema

                CreaNavBarDefault(oNavBarMgr, Arbolitos );
                //PENDIENTE: Implementar FullColapse de la NavBar
             end;

             // Si por derechos no tiene nada, agregar grupo
             if ( oNavBarMgr.NavBar.Groups.Count = 0 ) then
             begin
                with NodoRaiz do
                begin
                     Caption := 'No tiene derechos';
                     //EsForma := FALSE;
                     IndexDerechos := K_SIN_RESTRICCION;
                     oNavBarMgr.NodoNivel(NodoRaiz,0, K_ARBOL_NODO_RAIZ)
                end;
             end;
          end;
     end;
end;
procedure GetDatosNavBarUsuario( oNavBarMgr: TNavBarMgr; DataSet: TDataSet; const sFileName: String );
var
   iUsuario: Integer;
   lImportando: Boolean;
   oDatosUsuario: TDatosArbolUsuario;
begin
     // SUPONE que 'cdsUsuarios' est� posicionado en el usuario correcto
     with Dataset, oDatosUsuario do
     begin
          iUsuario          := FieldByName( 'US_CODIGO' ).AsInteger;
          NombreNodoInicial := FieldByName( 'US_NOMBRE' ).AsString;
          IncluirSistema    := zStrToBool( FieldByName( 'US_ARBOL' ).AsString );
          NodoDefault       := FieldByName( 'US_FORMA' ).AsInteger;
          TieneArbol        := FALSE;
     end;
     lImportando := StrLleno( sFileName );
     // Se trae los registros de Arbol del Servidor
     with dmSistema, cdsArbol, oDatosUsuario do
     begin
            if ( lImportando ) then
              cdsArbol.LoadFromFile( sFileName )
            else
               GetArbolUsuario( iUsuario );
        if not IsEmpty then
        begin
             // Busca el Primer registro para obtener nombre del Nodo
             if ( FieldByName( 'AB_ORDEN' ).AsInteger <= 0 ) then
             begin
               // En caso de IMPORTACION
               // En el primer registro va 'IncluirSistema' y 'Forma Default'
               if ( lImportando ) then
               begin
                    IncluirSistema := FieldByName( 'AB_NIVEL' ).AsInteger > 0;
                    NodoDefault    := FieldByName( 'AB_NODO' ).AsInteger;
               end
               else // Si viene de Base de Datos, permite renombrar RAIZ
               begin
                    if StrLleno( FieldByName( 'AB_DESCRIP' ).AsString ) then
                       NombreNodoInicial:= FieldByName('AB_DESCRIP').AsString;
               end;
               Next;
             end;
             TieneArbol := Not Eof;
        end;
        if ( NodoDefault = 0 ) then
           NodoDefault     := K_Empleados; //K_EMP_REGISTRO;
     end;
     oNavBarMgr.DatosUsuario := oDatosUsuario;
end;

//Edit by MP: LLamada modificada para utilizar el TcxTreeview
procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr; Arbolitos: array of TcxTreeView);
function NodoNivel( iNodo, iGroupIndex: Integer): Boolean;
begin
     Result := oNavBarMgr.NodoNivel( GetGrupoInfo( iNodo ), iGroupIndex, iNodo );
end;
//Edit by MP: Metodo MOdificado para utilizar el TcxTreeview
procedure CreaArbolito( const K_GLOBAL:Integer; Arbolito: TcxTreeView);
var
   oArbolMgr: TArbolMgr;
begin
     with Arbolito do
     begin
          Parent := oNavBarMgr.NavBar.Groups[oNavBarMgr.UltimoGrupo.Index].Control;
          Align := alClient;
     end;
     oArbolMgr := TArbolMgr.Create( Arbolito );
     with oArbolMgr do
     begin
          try
             with Arbolito.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     // Aqu� se agrega el arbol definido por el usuario
                     //CreaArbolitoUsuario( oArbolMgr, '',K_GLOBAL );
                     //Antes metodo: CreaArbolUsuario
                     with oArbolMgr do
                     begin
                          //Configuracion := TRUE;
                          if(dmCliente.EmpresaAbierta) then
                          begin
                            //oArbolMgr.NumDefault := K_GLOBAL ;
                            if ( (K_global = K_FULL_ARBOL_TRESS) and (CheckDerechosTress) ) then      //DChavez: se agrego esta validacion para cuando no tenga derechos cree el grupo "sin derechos"
                              ZArbolTress.CreaArbolDefault( oArbolMgr)
                            else
                              CreaArbolitoDefault( oArbolMgr, K_GLOBAL  )
                          end
                          else
                            CreaArbolDefaultSistema( oArbolMgr);
                            //Edit by MP: si el arbol es nil, utilizar el Arbol_Devex
                          if(arbol = nil) then
                            arbol_Devex.FullCollapse
                          else
                            Arbol.FullCollapse;
                          // Fin Edit
                     end;
                  finally
                         EndUpdate;
                  end;
             end;
          finally
                 Free;
          end;
     end;
end;
begin
     {***OJO***}
     {El index del grupo comienza desde 1 porque el 0 es para el grupo del Arbol del Usuario}
     // ************ Presupuestos *****************
     if not dmCliente.EmpresaAbierta then
     begin
     {   if ( NodoNivel( K_Sistema, 1 )) then
          CreaArbolito ( K_SISTEMA, Arbolitos[1]);
          }
          if ( NodoNivel( K_Sistema, 1 )) then
          CreaArbolito ( K_SISTEMA, Arbolitos[0]);

     end
     else
     begin
        if( NodoNivel( K_Empleados, 1 )) then
        begin
          CreaArbolito ( K_EMPLEADOS, Arbolitos[1]);
        end;
        if( NodoNivel( K_ASISTENCIA, 2 )) then
        begin
          CreaArbolito ( K_ASISTENCIA, Arbolitos[2]);
        end;
        if( NodoNivel( K_NOMINA, 3 )) then
        begin
          CreaArbolito ( K_NOMINA, Arbolitos[3]);
        end;
        if( NodoNivel( K_IMSS, 4 )) then
        begin
          CreaArbolito ( K_IMSS, Arbolitos[4]);
        end;
        if( NodoNivel( K_CAPACITACION, 5 )) then
        begin
          CreaArbolito ( K_CAPACITACION, Arbolitos[5]);
        end;
        if( NodoNivel( K_CONSULTAS, 6 )) then
        begin
          CreaArbolito ( K_CONSULTAS, Arbolitos[6]);
        end;
        if( NodoNivel( K_CATALOGOS, 7 )) then
        begin
          CreaArbolito ( K_CATALOGOS, Arbolitos[7]);
        end;
        if( NodoNivel( K_TABLAS, 8 )) then
        begin
          CreaArbolito ( K_TABLAS, Arbolitos[8]);
        end;
        if( NodoNivel( K_SISTEMA, 9 )) then
        begin
          CreaArbolito ( K_SISTEMA, Arbolitos[9]);
        end;
        if(nodoNivel(K_FULL_ARBOL_TRESS,10))then
        begin
          CreaArbolito(K_Full_Arbol_Tress,Arbolitos[10]);
        end;
     end;
end;
function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
begin
     case iNodo of
          K_EMPLEADOS               : Result := Grupo( 'Empleados', iNodo );
          K_EMP_DATOS               : Result := Grupo( 'Datos', iNodo );
          K_EMP_CURRICULUM          : Result := Grupo( 'Curr�culum', iNodo );
          K_EMP_EXPEDIENTE          : Result := Grupo( 'Expediente', iNodo );
          K_EMP_NOMINA              : Result := Grupo( 'N�mina', iNodo );
          K_EMP_SIM_FINIQUITOS      : Result := Grupo( 'Simulaci�n de Finiquitos', iNodo );
          K_ASISTENCIA              : Result := Grupo( 'Asistencia', iNodo );
          K_ASIS_DATOS              : Result := Grupo( 'Datos', iNodo );
          K_NOMINA                  : Result := Grupo( 'N�minas', iNodo );
          K_NOM_DATOS               : Result := Grupo( 'Datos', iNodo );
          K_NOM_EXCEPCIONES         : Result := Grupo( 'Excepciones', iNodo );
		      K_IMSS                    : Result := Grupo( 'Pagos IMSS', iNodo );
          K_IMSS_DATOS              : Result := Grupo( 'Datos', iNodo );
          K_IMSS_HISTORIA           : Result := Grupo( 'Historia', iNodo );
          K_CONSULTAS               : Result := Grupo( 'Consultas', iNodo );
          K_CATALOGOS               : Result := Grupo( 'Cat�logos', iNodo );
          K_CAT_CONTRATACION        : Result := Grupo( 'Contrataci�n', iNodo );
          K_CAT_DESCRIPCION_PUESTOS : Result := Grupo( 'Descripci�n de Puestos', iNodo );
{$IFNDEF DOS_CAPAS}
          K_CAT_VALUACION_PUESTOS   : Result := Grupo( 'Valuaci�n de Puestos', iNodo );
{$ENDIF}
          K_CAT_NOMINA              : Result := Grupo( 'N�mina', iNodo );
          K_CAT_GENERALES           : Result := Grupo( 'Generales', iNodo );
          K_CAT_CAPACITACION        : Result := Grupo( 'Capacitaci�n', iNodo );
          K_CAT_NODO_COMPETENCIAS   : Result := Grupo( 'Competencias', iNodo );
          K_CAT_CONFIGURACION       : Result := Grupo( 'Configuraci�n', iNodo );
          K_TABLAS                  : Result := Grupo( 'Tablas', iNodo );
          K_TAB_PERSONALES          : Result := Grupo( 'Personales', iNodo );
          K_TAB_AREAS               : Result := Grupo( 'Areas', iNodo );
          K_TAB_ADICIONALES         : Result := Grupo( 'Adicionales', iNodo );
          K_TAB_HISTORIAL           : Result := Grupo( 'Historial', iNodo );
          K_TAB_NOMINA              : Result := Grupo( 'N�mina', iNodo );
          K_TAB_OFICIALES           : Result := Grupo( 'Oficiales', iNodo );
          K_TAB_OFI_STPS            : Result := Grupo( 'STPS', iNodo );
          K_TAB_CAPACITACION        : Result := Grupo( 'Competencias', iNodo );
          K_SISTEMA                 : Result := Grupo( 'Sistema', iNodo );
          K_SIST_DATOS              : Result := Grupo( 'Datos', iNodo );
{$ifndef DOS_CAPAS}
          K_SIST_TERMINALESGTI      : Result := Grupo( 'Terminales GTI', iNodo );
{$endif}


          K_EMP_CAFETERIA           : Result := Grupo( 'Cafeter�a', iNodo );
          K_CAT_CAFETERIA           : Result := Grupo( 'Cafeter�a', iNodo );
          K_CAT_HERRAMIENTAS        : Result := Grupo( 'Herramientas', iNodo );
          K_CAPACITACION            : Result := Grupo( 'Capacitaci�n', iNodo );
		      K_NOM_FONACOT             : Result := Grupo( 'Pagos Fonacot', iNodo );
          K_EMP_CASETA              : Result := Grupo( 'Caseta', iNodo );
          K_FULL_ARBOL_TRESS        : Result := Grupo('�rbol Cl�sico',iNodo);
          K_SIST_REPORTES_EMAIL     : Result := Grupo( 'Reportes email', iNodo );
       else
           Result := Grupo( '', 0 );
     end;
end;
function CheckDerechosTress: Boolean;
begin
     Result := RevisaCualquiera( D_EMPLEADOS ) or
               RevisaCualquiera( D_ASISTENCIA ) or
               RevisaCualquiera( D_NOMINA ) or
               RevisaCualquiera( D_IMSS ) or
               RevisaCualquiera( D_CAPACITACION ) or
               RevisaCualquiera( D_CONSULTAS ) or
               RevisaCualquiera( D_CATALOGOS ) or
               RevisaCualquiera( D_TABLAS ) or
               RevisaCualquiera( D_SISTEMA );
end;


end.
