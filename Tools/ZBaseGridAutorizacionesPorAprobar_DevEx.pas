unit ZBaseGridAutorizacionesPorAprobar_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaDBTextBox, StdCtrls, Db, Grids, DBGrids,
  ZetaDBGrid, DBCtrls, Buttons, ExtCtrls, Mask, ZetaFecha, ZetaKeyCombo,
  ZetaClientDataSet,
  ZetaSmartLists, ZBaseGridEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, ZetaKeyLookup_DevEx, dxSkinsDefaultPainters;

type
  TBaseGridAutorizacionesPorAprobar_DevEx = class(TBaseGridEdicion_DevEx)
    PanelControles: TPanel;
    Label1: TLabel;
    cbTipo: TZetaKeyCombo;
    LabDel: TLabel;
    zFecha: TZetaFecha;
    Supervisores: TZetaKeyLookup_DevEx;
    lblSupervisor: TLabel;
    Label3: TLabel;
    cbAutorizaciones: TZetaKeyCombo;
    bbAutorizarTodas: TcxButton;
    bbBorrarTodas: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbTipoChange(Sender: TObject);
    procedure SupervisoresValidKey(Sender: TObject);
    procedure zFechaValidDate(Sender: TObject);
    procedure bbAutorizarTodasClick(Sender: TObject);
    procedure bbBorrarTodasClick(Sender: TObject);
  private
    { Private declarations }
    FAutorizaciones: string;
    FSupervisores: string;
    FStatus: string;
    FAutoXAprobar: TZetaClientDataSet;
    function ChecaFiltroAutorizaciones: boolean;
    function ChecaFiltroSupervisores: boolean;
    function ChecaFiltroStatus: boolean;
    procedure LlenaTipoAutorizaciones;
    procedure LlenaAprobaciones;
    procedure ApruebaDesaprueba( const lAprueba: boolean );
    procedure ConcatenaFiltro;    
  protected
    { Protected declarations }
    function PuedeModificar: boolean; override;
    function ChecaDerecho: boolean; virtual;
    procedure InicializaControles; Virtual;
    procedure AplicaGridFiltros( const sFiltros: string ); virtual;
    procedure HabilitaControles; override;
    procedure Connect; override;
    procedure Agregar; override;
    procedure Borrar; override;
  public
    { Public declarations }
    procedure ReAjusta;
    property AutoXAprobar: TZetaClientDataSet read FAutoXAprobar write FAutoXAprobar;
    property FiltroSuper: string read FSupervisores write FSupervisores;
  end;

var
  BaseGridAutorizacionesPorAprobar_DevEx: TBaseGridAutorizacionesPorAprobar_DevEx;

implementation

{$R *.DFM}

uses
    DCliente,
    DSistema,
    DGlobal,
    ZGlobalTress,
    ZetaDialogo,
    ZetaCommonClasses,
    ZetaCommonTools,
    ZetaCommonLists;

const
     K_CODIGO_NULO = 0;
     K_CODIGO_TODOS = 'X8G7';
     K_AUTORIZACIONES_SIN_APROBAR = 2;
     aAprobaciones: array[ 1..2 ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}=( '( US_COD_OK > 0 )','( US_COD_OK = 0 )' );

{ TBaseGridAutorizacionesPorAprobar }

procedure TBaseGridAutorizacionesPorAprobar_DevEx.FormCreate(Sender: TObject);
const
     K_COLUMNA_HORAS_APROBADAS = 6;
begin
     inherited;
     PrimerColumna := K_COLUMNA_HORAS_APROBADAS;
     LlenaTipoAutorizaciones;
     LlenaAprobaciones;
end;

procedure TBaseGridAutorizacionesPorAprobar_DevEx.FormShow(Sender: TObject);
const
     K_SUPERVISOR = '%s:';
     COLUMNA_SUPER = 2;
var
   sNivelSuper: string;
begin
     inherited;
     with Global do
          sNivelSuper := NombreNivel( GetGlobalInteger( K_GLOBAL_NIVEL_SUPERVISOR ) );
     lblSupervisor.caption := Format( K_SUPERVISOR, [ sNivelSuper ] );
     ZetaDBGrid.Columns[ COLUMNA_SUPER ].Title.Caption := sNivelSuper;
     AplicaGridFiltros( VACIO );
     InicializaControles;
     self.ActiveControl:= cbTipo;
     SeleccionaPrimerColumna;
     ReAjusta;
end;

procedure TBaseGridAutorizacionesPorAprobar_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     FAutoXAprobar.Refrescar;
     DataSource.DataSet := FAutoXAprobar;
end;

procedure TBaseGridAutorizacionesPorAprobar_DevEx.Agregar;
begin
     { No Hace Nada }
end;

procedure TBaseGridAutorizacionesPorAprobar_DevEx.Borrar;
begin
     { No Hace Nada }
end;

function TBaseGridAutorizacionesPorAprobar_DevEx.PuedeModificar: Boolean;
begin
     Result := ChecaDerecho;
end;

procedure TBaseGridAutorizacionesPorAprobar_DevEx.LlenaTipoAutorizaciones;
var
   eTipo: eAutorizaChecadas;
begin
     with cbTipo.Lista do
     begin
          BeginUpdate;
          try
             Clear;
             Add( K_CODIGO_TODOS + '=Todos' );
             for eTipo := LOW( eAutorizaChecadas ) to HIGH( eAutorizaChecadas ) do
             begin
                  Add( Format( '%d=%s', [ ORD( eTipo ) + K_OFFSET_AUTORIZACION, ObtieneElemento( lfAutorizaChecadas, Ord( eTipo ) ) ] ) );
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TBaseGridAutorizacionesPorAprobar_DevEx.LlenaAprobaciones;
begin
     with cbAutorizaciones.Lista do
     begin
          BeginUpdate;
          try
             Clear;
             Add( K_CODIGO_TODOS + '=Todas' );
             Add( '1=Aprobadas' );
             Add( '2=Sin Aprobar' );
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TBaseGridAutorizacionesPorAprobar_DevEx.ConcatenaFiltro;
var
   sFiltro, sLlave: string;
begin
     sFiltro := VACIO;
     //Combo de Tipos Autorizaciones
     sLlave := cbTipo.Llave;
     if( sLlave <> K_CODIGO_TODOS )then
     begin
          sFiltro := ConcatFiltros( sFiltro, Format( '( CH_TIPO = %s )', [ EntreComillas( sLlave ) ] ) );
          FAutorizaciones := sLlave;
     end
     else
         FAutorizaciones := VACIO;

     //Supervisores
     sLlave := Supervisores.Llave;
     if strLleno( sLlave ) then
     begin
          sFiltro := ConcatFiltros( sFiltro, Format( '( CB_NIVEL = %s )', [ EntreComillas( sLlave ) ] ) );
          FSupervisores := sLlave;
     end
     else
         FSupervisores := VACIO;

     //Combo de Autorizaciones
     sLlave := cbAutorizaciones.Llave;
     if( sLlave <> K_CODIGO_TODOS )then
     begin
          sFiltro := ConcatFiltros( sFiltro, aAprobaciones[ StrToInt( sLlave ) ] );
          FStatus := sLlave
     end
     else
         FStatus := VACIO;
     AplicaGridFiltros( sFiltro );
end;

function TBaseGridAutorizacionesPorAprobar_DevEx.ChecaDerecho: boolean;
begin
     Result := True;
end;

procedure TBaseGridAutorizacionesPorAprobar_DevEx.InicializaControles;
begin
     cbtipo.ItemIndex := 0;
     cbAutorizaciones.ItemIndex := K_AUTORIZACIONES_SIN_APROBAR;
     ZetaDBGrid.ReadOnly := not ChecaDerecho;
     with bbAutorizarTodas do
     begin
          Enabled := ChecaDerecho;
          bbBorrarTodas.Enabled := Enabled;
     end;
end;

procedure TBaseGridAutorizacionesPorAprobar_DevEx.ApruebaDesaprueba( const lAprueba: boolean );
var
   Pos : TBookMark;
   sOldIndex, sOldFiltro: string;
   lComparacion: boolean;
begin
     with FAutoXAprobar do
     begin
          if not IsEmpty then
          begin
               DisableControls;
               Pos:= GetBookMark;
               sOldFiltro := Filter;
               Filtered := FALSE;
               sOldIndex := IndexFieldNames;
               try
                  IndexFieldNames := VACIO;
                  First;
                  while not Eof do
                  begin
                       if ( lAprueba ) then
                          lComparacion := ( FieldByName( 'US_COD_OK' ).AsInteger = K_CODIGO_NULO )
                       else
                           lComparacion := ( FieldByName( 'US_COD_OK' ).AsInteger > K_CODIGO_NULO );
                       if ( lComparacion ) then
                       begin
                            if ChecaFiltroAutorizaciones and ChecaFiltroSupervisores and ChecaFiltroStatus then
                            begin
                                 if ( State <> dsEdit ) then
                                    Edit;
                                 if ( lAPrueba ) then
                                    FieldByName( 'CH_HOR_DES' ).AsFloat := FieldByName( 'CH_HORAS' ).AsFloat
                                 else
                                     FieldByName( 'CH_HOR_DES' ).AsFloat := 0;
                                 Post;
                                 HuboCambios := TRUE;
                            end;
                       end;
                       Next;
                  end;
               finally
                      IndexFieldNames := sOldIndex;
                      Filter := sOldFiltro;
                      Filtered := TRUE;
                      if ( Pos <> nil ) then
                      begin
                           if BookMarkValid( Pos ) then
                              GotoBookMark( Pos );
                           FreeBookMark( Pos );
                      end;
                      EnableControls;
               end;
          end
          else
              ZetaDialogo.ZError( Caption, '� No Hay Autorizaciones Por Aprobar/Desaprobar !', 0 );
     end;
end;

function TBaseGridAutorizacionesPorAprobar_DevEx.ChecaFiltroAutorizaciones: boolean;
begin
     Result := TRUE;
     with FAutoXAprobar do
     begin
          if StrLleno( FAutorizaciones ) then
             Result := ( FieldByName( 'CH_TIPO' ).AsString = FAutorizaciones );
     end;
end;

function TBaseGridAutorizacionesPorAprobar_DevEx.ChecaFiltroSupervisores: boolean;
begin
     Result := TRUE;
     with AutoXAprobar do
     begin
          if StrLleno( FSupervisores ) then
             Result := ( FieldByName( 'CB_NIVEL' ).AsString = FSupervisores );
     end;
end;

function TBaseGridAutorizacionesPorAprobar_DevEx.ChecaFiltroStatus: boolean;
begin
     Result := TRUE;
     with AutoXAprobar do
     begin
          if StrLleno( FStatus ) then
          begin
               if StrToInt( Fstatus ) = K_AUTORIZACIONES_SIN_APROBAR then
                  Result := ( FieldByName( 'US_COD_OK' ).AsInteger = K_CODIGO_NULO )
               else
                  Result := ( FieldByName( 'US_COD_OK' ).AsInteger > K_CODIGO_NULO );
          end;
     end;
end;

procedure TBaseGridAutorizacionesPorAprobar_DevEx.HabilitaControles;
begin
     inherited;
     zFecha.Enabled := not Editing;
end;

procedure TBaseGridAutorizacionesPorAprobar_DevEx.AplicaGridFiltros( const sFiltros: string );
begin
     with FAutoXAprobar do
     begin
          Filtered := strLleno( sFiltros );
          if ( Filtered ) then
             Filter := sFiltros
          else
              Filter := VACIO;
     end;
end;

procedure TBaseGridAutorizacionesPorAprobar_DevEx.zFechaValidDate(Sender: TObject);
begin
     inherited;
     ConcatenaFiltro;
     ReAjusta;
end;

procedure TBaseGridAutorizacionesPorAprobar_DevEx.cbTipoChange(Sender: TObject);
begin
     inherited;
     ConcatenaFiltro;
     ReAjusta;
end;

procedure TBaseGridAutorizacionesPorAprobar_DevEx.SupervisoresValidKey(Sender: TObject);
begin
     inherited;
     ConcatenaFiltro;
     ReAjusta;
end;

procedure TBaseGridAutorizacionesPorAprobar_DevEx.bbAutorizarTodasClick(Sender: TObject);
begin
     ApruebaDesaprueba( TRUE );
end;

procedure TBaseGridAutorizacionesPorAprobar_DevEx.bbBorrarTodasClick(Sender: TObject);
begin
     ApruebaDesaprueba( FALSE );
end;
procedure TBaseGridAutorizacionesPorAprobar_DevEx.ReAjusta;
var
i:integer;
Width :Integer;
begin
  Width:=0;
  for i :=0 to  ZetadbGrid.Columns.Count -1 do
  begin
   Width := ZetaDbgrid.Columns[i].Width + Width;
  end;
  self.Width := width+37;
end;
end.
