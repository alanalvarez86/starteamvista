unit ZBaseDlgModal_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  dxSkinsDefaultPainters, cxButtons, ImgList, TressMorado2013, ZetaCXGrid;

type
  TZetaDlgModal_DevEx = class(TForm)
    PanelBotones: TPanel;
    OK_DevEx: TcxButton;
    Cancelar_DevEx: TcxButton;
    cxImageList24_PanelBotones: TcxImageList;
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
    { Public declarations }
  end;
  TZetaDlgModalClass = class of TZetaDlgModal_DevEx;

var
  ZetaDlgModal_DevEx: TZetaDlgModal_DevEx;

procedure ShowDlgModal( var Dialogo; DialogClass: TZetaDlgModalClass );

implementation

uses ZetaSmartLists;

{$R *.DFM}

procedure ShowDlgModal( var Dialogo; DialogClass: TZetaDlgModalClass );
begin
     if ( TZetaDlgModal_DevEx( Dialogo ) = nil ) then
        TZetaDlgModal_DevEx( Dialogo ) := DialogClass.Create( Application ) as TZetaDlgModal_DevEx;
     with TZetaDlgModal_DevEx( Dialogo ) do
     begin
          ShowModal;
     end;
end;

{ *********** TZetaDlgModal_DevEx ************ }

procedure TZetaDlgModal_DevEx.KeyPress( var Key: Char );
begin
     inherited KeyPress( Key );
     case Key of
          Chr( VK_RETURN ):
          begin
               if ( not ( ActiveControl is TButton ) ) and
                  ( not ( ActiveControl is TZetaSmartListBox ) ) and
                  ( not ( ActiveControl is TCustomMemo ) ) and
                  ( not ( ActiveControl is TZetaCXGrid ) ) then
               begin
                    Key := #0;
                    Perform( WM_NEXTDLGCTL, ( Hi( GetKeyState( VK_SHIFT ) ) and 1 ), 0 );
               end;
          end;
     end;
end;
end.
