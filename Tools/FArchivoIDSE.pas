unit FArchivoIDSE;

interface

uses
  SysUtils, Classes, gtPDFClasses, gtCstPDFDoc, gtExPDFDoc, gtExProPDFDoc,
  gtPDFDoc, gtWidestrings, FileCtrl, ZetaCommonTools, ZetaCommonClasses, RegExpr,
  Db, DBClient,ZetaClientDataSet;

type
  TArchivoIDSE = class(TDataModule)
    gtPDFDocumento: TgtPDFDocument;
  private
    { Private declarations }
    FArchivo : string;
    FNumeroLote : string;
    FFechaTransmision : TDateTime;
    FMovimientosBajasCount : integer;
    FMovimientosCambiosCount : integer;
    FMovimientosReingresosCount : integer;
    FMovimientosTotalCount : integer;
    FMovimientosCargados   : integer;
    FRegistroPatron:string;

    FLecturaPagina : integer;
    FListaLineas : TgtWideStringList;
    FListaIndex  : integer;

    IDSEMovimientosExpr : TRegExpr;
    IDSEMovimientosExpr_TipoMov : TRegExpr;
    IDSEMovimientosExpr_FechaMov: TRegExpr;
    IDSENSSExpr : TRegExpr;

    FDataSetMovimientos : TZetaClientDataSet;
    FCargoArchivo : boolean;

    FVersion : integer;

    function LeerEncabezado : boolean;
    function LeerEncabezadoVersion2 : boolean;
    function LeerEncabezadoVersion3 : boolean;
    function LeerEncabezadoVersion4 : boolean;
    function ParseFechaIDSE ( sDia, sMes, sAnio : string ) : TDateTime;
    function ParsearMovimientos(const sTexto: string): integer;

    function AvanzarPagina : boolean;
    procedure IniciarDataSet;
    procedure AgregarMovimientoADataSet(iTipo : integer; dFecha : TDateTime; lValido : boolean;   sNSS, sMovimiento : string);
  public
    { Public declarations }
    function Cargar(sArchivo : string) : boolean;
    function Iniciar : boolean;
    procedure Terminar;
    function LeerMovimiento(var sMovimiento : string ) : integer;
    function ParsearMovimientosVer2(const sTexto: string): integer;
    function GetLineasMovimientos : TgtWideStringList; virtual;

    property CargoArchivo : boolean read FCargoArchivo;
    property NumeroLote: String read FNumeroLote;
    property FechaTransmision: TDateTime read FFechaTransmision;
    property MovimientosBajas: integer read FMovimientosBajasCount;
    property MovimientosCambios: integer read FMovimientosCambiosCount;
    property MovimientosReingresos: integer read FMovimientosReingresosCount;
    property Movimientos: integer read FMovimientosTotalCount;
    property DataSetMovimientos : TZetaClientDataSet  read FDataSetMovimientos write FDataSetMovimientos;
    property RegistroPatronal: String read FRegistroPatron;

  end;

const
     ShortMonthNamesESP : array[1..12] of string =  ('ENE', 'FEB','MAR','ABR','MAY','JUN','JUL','AGO','SEP','OCT','NOV','DIC');
     ShortMonthNamesENG : array[1..12] of string =  ('JAN', 'FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC');

     K_VERSION_IDSE_1 = 1;
     K_VERSION_IDSE_2 = 2;
     K_VERSION_IDSE_3 = 3;
     K_VERSION_IDSE_4 = 4;


     //Constantes y Expresiones Version 1
     K_TOPE_PAGINA_ENCABEZADO = 2;
     K_INICIA_PAGINA_MOVIMIENTOS = 2;

     K_IDSE_EXPRESION_LOTE = 'LOTE N�MERO\s*(.+)';
     K_IDSE_EXPRESION_FECHA_TRAN = 'FECHA TRANSACCI�N\s*(\d{1,2})/([A-Z]+)/(\d{1,4}).*';
     K_IDSE_EXPRESION_TOTALES = '.*(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})+.*';
     K_IDSE_EXPRESION_NSS = '.*(\d{11}).*';
     K_IDSE_EXPRESION_MOVIMIENTO = '^\s*(\d)\s(\d{11}).*\d+\.\d{2}.*(\d{2})/(.{1,3})/(\d{2})';
     K_IDSE_EXPRESION_REG_PATRON = 'REGISTRO PATRONAL\s*(.+)';

    //Constantes y  Expresiones Version 2
     K_TOPE_PAGINA_ENCABEZADO_VER2 = 2;
     K_INICIA_PAGINA_MOVIMIENTOS_VER2 = 2;

     K_IDSE_EXPRESION_LOTE_VER2 = 'LOTE N�MERO\s*([0-9a-zA-Z]+)';
     K_IDSE_EXPRESION_FECHA_TRAN_VER2 = 'FECHA TRANSACCI�N|FECHA DE TRANSMISI�N\s*(\d{1,2})/([A-Z]+)/(\d{1,4})';
     {$ifdef PDF_TOOLKIT_3}
     K_IDSE_EXPRESION_TOTALES_VER2 = 'O P E R A D O S BAJAS (\d+) MODIF. (\d+) REING. (\d+) TOTAL (\d+)';
     {$else}
     K_IDSE_EXPRESION_TOTALES_VER2 = '(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s*';
     {$endif}
     K_IDSE_EXPRESION_NSS_VER2 = '(\d{11})';
    {$ifdef PDF_TOOLKIT_3}
     K_IDSE_EXPRESION_MOVIMIENTO_VER2 = '(\d)\s(\d{11})\s*.+\$\s*\d+\.\d{2}\s.\s...\s.\s(\d{2})/(.{1,3})/(\d{2,4})';
    {$else}
     K_IDSE_EXPRESION_MOVIMIENTO_VER2 = '(\d)\s(\d{11})\s*.+\s*\d+\.\d{2}\s.\s...\s.\s(\d{2})/(.{1,3})/(\d{2,4})';
     {$endif}
     //'2 09937628429 MENDOZA GONZALEZ GABRIELA 0.00 0 000 0 31/01/2011 1 2'
     K_IDSE_EXPRESION_REG_PATRON_VER2 = 'REGISTRO PATRONAL\s*([0-9a-zA-Z]+)';
     K_IDSE_EXPRESION_REG_PATRON_VER2_1 = 'PATR�N\s*([0-9a-zA-Z]+)';

     //Constantes y  Expresiones Version 3
     K_TOPE_PAGINA_ENCABEZADO_VER3 = 1;
     K_INICIA_PAGINA_MOVIMIENTOS_VER3 = 1;

     K_IDSE_EXPRESION_LOTE_VER3 = 'N�MERO DE LOTE\s*([0-9a-zA-Z]+)';

     K_IDSE_EXPRESION_FECHA_TRAN_VER3 = 'FECHA Y HORA DE RECEPCI�N DEL LOTE\s*(\d{1,4})-(\d{1,2})-(\d{1,2}) (\d{1,2}):(\d{1,2})';

     K_IDSE_EXPRESION_TOTALES_VER3 = '(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s*';
     K_IDSE_EXPRESION_NSS_VER3 = '(\d{11})';

     // DES #12973: Validaci�n de Movimientos IDSE, procesar nuevo formato
     // K_IDSE_EXPRESION_MOVIMIENTO_VER3 = '(\d)\s(\d{11})\s*.+\$\s*\d+\.\d{2}\s.\s...\s.\s(\d{2})/(.{1,3})/(\d{2,4})';
     K_IDSE_EXPRESION_MOVIMIENTO_VER3 = '(\d)\s*(\d{11})\s*.+\$\s*\d+\.\d{2}\s.\s...\s.\s(\d{2})/(.{1,3})/(\d{2,4})';

     K_IDSE_EXPRESION_REG_PATRON_VER3 = 'REGISTRO PATRONAL\s*([0-9a-zA-Z]+)';
     K_IDSE_EXPRESION_REG_PATRON_VER3_1 = 'PATR�N\s*([0-9a-zA-Z]+)';

     // DES #12973: Validaci�n de Movimientos IDSE, procesar nuevo formato
     // K_IDSE_EXPRESION_MOVIMIENTO_VER3_TIPOMOV = '(\d)\s(\d{11})';
     K_IDSE_EXPRESION_MOVIMIENTO_VER3_TIPOMOV = '(\d)\s*(\d{11})';

     K_IDSE_EXPRESION_MOVIMIENTO_VER3_FECHA = '(\d{2})/(.{1,3})/(\d{2,4})';


     // Constantes y  Expresiones Version 4
     K_TOPE_PAGINA_ENCABEZADO_VER4 = 1;
     K_INICIA_PAGINA_MOVIMIENTOS_VER4 = 1;

     // DES #12973: Validaci�n de Movimientos IDSE, procesar nuevo formato
     // Se modifican y agregan expresiones regulares
     K_IDSE_INFORMACION_GENERAL = '\s*INFORMACI�N GENERAL';
     K_IDSE_EXPRESION_LOTE_VER4 = '([0-9]+)';
     K_IDSE_EXPRESION_FECHA_TRAN_VER4 = '(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2})';
     K_IDSE_EXPRESION_TOTALES_VER4 = '(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s+(\d{1,9})\s*';
     K_IDSE_EXPRESION_NSS_VER4 = '(\d{11})';
     K_IDSE_EXPRESION_MOVIMIENTO_VER4 = '(\d)\s*(\d{12})\s*.+\$\s*\d+\.\d{2}\s.\s...\s.\s(\d{2})/(.{1,3})/(\d{2,4})';
     K_IDSE_EXPRESION_REG_PATRON_VER4 = '([0-9a-zA-Z]\d{10}){1}';
     K_IDSE_EXPRESION_MOVIMIENTO_VER4_TIPOMOV = '(\d)\s*(\d{11})';
     K_IDSE_EXPRESION_MOVIMIENTO_VER4_FECHA = '(\d{2})/(.{1,3})/(\d{2,4})';

var
  ArchivoIDSE: TArchivoIDSE;

implementation

{$R *.dfm}

{ TArchivoIDSE }


function TArchivoIDSE.Cargar(sArchivo: string): boolean;
begin
    Result := False;
    FArchivo := sArchivo;
    FNumeroLote := VACIO;
    FFechaTransmision := 0;
    FMovimientosTotalCount := 0;
    FMovimientosBajasCount := 0;
    FMovimientosCambiosCount := 0;
    FMovimientosReingresosCount := 0;
    FCargoArchivo := False;
    FVersion := K_VERSION_IDSE_1;
    try
        if  gtPDFDocumento. LoadFromFile(sArchivo) then
        begin

             Result := LeerEncabezado;
             if not Result then
             begin
                Result :=  LeerEncabezadoVersion2;
                if ( Result ) then
                     FVersion := K_VERSION_IDSE_2
                else
                begin
                    Result :=  LeerEncabezadoVersion3;
                    if (Result) then
                    begin
                         FVersion :=  K_VERSION_IDSE_3;
                    end
                    else
                    begin
                         Result :=  LeerEncabezadoVersion4;
                         FVersion :=  K_VERSION_IDSE_4;
                    end;
                end;

             end;

             FCargoArchivo := Result;
        end;
    except
          on Error: Exception do
          begin
               Result := False;
          end;
    end;

end;

procedure TArchivoIDSE.IniciarDataSet;
begin
     if (   FDataSetMovimientos  <> nil ) then
     begin
        with FDataSetMovimientos do
        begin
             if state in [ dsEdit, dsInsert ] then
                Cancel;
             FDataSetMovimientos.EmptyDataSet;
        end;
     end;
end;

function TArchivoIDSE.Iniciar: boolean;
begin

     IDSEMovimientosExpr := TRegExpr.Create;
     IDSENSSExpr := TRegExpr.Create;
     FMovimientosCargados := 0;
     IDSEMovimientosExpr_TipoMov  := nil;
     IDSEMovimientosExpr_FechaMov  := nil;

     if ( FVersion =  K_VERSION_IDSE_1 ) then
     begin
          FLecturaPagina :=  K_INICIA_PAGINA_MOVIMIENTOS;
          IDSEMovimientosExpr.Expression := K_IDSE_EXPRESION_MOVIMIENTO;
          IDSENSSExpr.Expression := K_IDSE_EXPRESION_NSS;
     end
     else
     if (FVersion =  K_VERSION_IDSE_2 ) then
     begin
          FLecturaPagina :=  K_INICIA_PAGINA_MOVIMIENTOS_VER2;
          IDSEMovimientosExpr.Expression := K_IDSE_EXPRESION_MOVIMIENTO_VER2;
          IDSENSSExpr.Expression := K_IDSE_EXPRESION_NSS_VER2;
     end
     else
     if (FVersion =  K_VERSION_IDSE_3 ) then
     begin
          FLecturaPagina :=  K_INICIA_PAGINA_MOVIMIENTOS_VER3;
          IDSEMovimientosExpr.Expression := K_IDSE_EXPRESION_MOVIMIENTO_VER3;
          IDSENSSExpr.Expression := K_IDSE_EXPRESION_NSS_VER3;
          IDSEMovimientosExpr_TipoMov := TRegExpr.Create;
          IDSEMovimientosExpr_TipoMov.Expression := K_IDSE_EXPRESION_MOVIMIENTO_VER3_TIPOMOV;
          IDSEMovimientosExpr_FechaMov := TRegExpr.Create;
          IDSEMovimientosExpr_FechaMov.Expression := K_IDSE_EXPRESION_MOVIMIENTO_VER3_FECHA;
     end
     else
     if (FVersion =  K_VERSION_IDSE_4 ) then
     begin
          FLecturaPagina :=  K_INICIA_PAGINA_MOVIMIENTOS_VER4;
          IDSEMovimientosExpr.Expression := K_IDSE_EXPRESION_MOVIMIENTO_VER4;
          IDSENSSExpr.Expression := K_IDSE_EXPRESION_NSS_VER4;
          IDSEMovimientosExpr_TipoMov := TRegExpr.Create;
          IDSEMovimientosExpr_TipoMov.Expression := K_IDSE_EXPRESION_MOVIMIENTO_VER4_TIPOMOV;
          IDSEMovimientosExpr_FechaMov := TRegExpr.Create;
          IDSEMovimientosExpr_FechaMov.Expression := K_IDSE_EXPRESION_MOVIMIENTO_VER4_FECHA;
     end;

     FListaIndex := 0;
     FListaLineas := GetLineasMovimientos;

     {$ifdef TEST}
     FListaLineas.SaveToFile('C:\TMP\FListaLineas.txt');
     {$endif}

     IniciarDataSet;

     Result := (FListaLineas.Count > 0);
end;

procedure TArchivoIDSE.Terminar;
begin
     if FDataSetMovimientos <> nil then
     begin
          with FDataSetMovimientos do
          begin
               if State in [ dsEdit, dsInsert ] then
                   Post;
          end;
     end;

     FreeAndNil( IDSEMovimientosExpr ) ;
     FreeAndNil( IDSEMovimientosExpr_TipoMov ) ;
     FreeAndNil( IDSEMovimientosExpr_FechaMov ) ;
     FreeAndNil( IDSENSSExpr ) ;
     FreeAndNil( FListaLineas );
end;


function TArchivoIDSE.ParseFechaIDSE ( sDia, sMes, sAnio : string ) : TDateTime;
var
   iYear, iMonth, iDay : Word;
begin
     iDay := StrToIntDef( sDia, 0);
     iYear := StrToIntDef( sAnio, 0);
     iMonth := StrToIntDef( sMes , 0 ) ;

     if ( iMonth = 0 ) then
     begin
        for iMonth := 1  to 12 do
        begin
             if  ( ShortMonthNamesESP[iMonth] = sMes  ) or  ( ShortMonthNamesENG[iMonth] = sMes ) then
                 Break;
        end;
     end;
     Result := CodificaFecha( iYear, iMonth, iDay);
end;



function TArchivoIDSE.LeerEncabezado: boolean;
var
   List : TgtWideStringList;
   IDSELoteExpr : TRegExpr;
   IDSEFecExpr : TRegExpr;
   IDSETotalesExpr : TRegExpr;
   IDSERegPatronExpr : TRegExpr;
   Pag, LI : integer;
   sTexto : string;

   lHuboLote, lHuboFecha, lHuboCant,lHuboRegPatron : boolean;
begin
    IDSELoteExpr :=   TRegExpr.Create;
    IDSEFecExpr :=  TRegExpr.Create;
    IDSETotalesExpr := TRegExpr.Create;
    IDSERegPatronExpr :=  TRegExpr.Create;

    IDSELoteExpr.Expression := K_IDSE_EXPRESION_LOTE;
    IDSEFecExpr.Expression := K_IDSE_EXPRESION_FECHA_TRAN;
    IDSETotalesExpr.Expression := K_IDSE_EXPRESION_TOTALES;
    IDSERegPatronExpr.Expression :=  K_IDSE_EXPRESION_REG_PATRON;


    lHuboLote := False;
    lHuboFecha := False;
    lHuboCant := False;
    lHuboRegPatron := False;

    try
       for Pag := 1 to K_TOPE_PAGINA_ENCABEZADO do
       begin
            // DES #12973: Validaci�n de Movimientos IDSE, procesar nuevo formato
            // List := gtPDFDocumento.ExtractTextFromPage(Pag, False); //PDFDoc.ExtractTextFormatted( Pag);
            List := gtPDFDocumento.ExtractTextFromPage(Pag, TRUE); //PDFDoc.ExtractTextFormatted( Pag);

            {$ifdef TEST}
            gtPDFDocumento.SaveAsText (Format ('C:\TMP\IDSE_TEXT1_%d.txt', [Pag]), '1');
            {$endif}

            for LI := 0 to List.Count  do
            begin
                 sTexto := String( List[LI] );
                 sTexto := UpperCase( sTexto );

                 if IDSELoteExpr.Exec( sTexto ) then
                 begin
                      FNumeroLote :=  IDSELoteExpr.Match[1];
                      lHuboLote := True;
                 end;

                 if IDSERegPatronExpr.Exec( sTexto ) then
                 begin
                      FRegistroPatron := IDSERegPatronExpr.Match[1];
                      if Pos( '-',FRegistroPatron ) > 0 then
                      begin
                           FRegistroPatron := Copy(FRegistroPatron,0,Pos( '-',FRegistroPatron )- 1 );
                      end;
                      lHuboRegPatron := True;
                 end;

                 if IDSEFecExpr.Exec( sTexto ) then
                 begin
                      FFechaTransmision :=  ParseFechaIDSE( IDSEFecExpr.Match[1],IDSEFecExpr.Match[2], IDSEFecExpr.Match[3]);
                      lHuboFecha :=  True;
                 end;

                 if IDSETotalesExpr.Exec( sTexto ) then
                 begin
                      FMovimientosBajasCount := StrToIntDef( IDSETotalesExpr.Match[5],0 );
                      FMovimientosCambiosCount := StrToIntDef( IDSETotalesExpr.Match[6],0 );
                      FMovimientosReingresosCount := StrToIntDef( IDSETotalesExpr.Match[7],0 );
                      FMovimientosTotalCount := StrToIntDef( IDSETotalesExpr.Match[8],0 );

                      lHuboCant :=  ( FMovimientosTotalCount > 0  );
                 end;
            end;

        end;
     finally
            FreeAndNil(IDSELoteExpr);
            FreeAndNil(IDSEFecExpr);
            FreeAndNil(IDSETotalesExpr);
            FreeAndNil(IDSERegPatronExpr);
            FreeAndNil(List);
     end;

     Result := ( lHuboLote and lHuboFecha and lHuboCant and lHuboRegPatron );

end;

function TArchivoIDSE.AvanzarPagina : boolean;
begin
     Result := False;
     if (  FListaIndex  > FListaLineas.Count  ) then
     begin
          FreeAndNil( FListaLineas );
          FListaIndex := 0;
          Inc( FLecturaPagina );
          if ( FLecturaPagina <= gtPDFDocumento.PageCount ) then
          begin

               // DES #12973: Validaci�n de Movimientos IDSE, procesar nuevo formato
               // FListaLineas := gtPDFDocumento.ExtractTextFromPage(FLecturaPagina, False);
               FListaLineas := gtPDFDocumento.ExtractTextFromPage(FLecturaPagina, TRUE);

               Result := ( FListaLineas.Count > 0 );
          end;
     end;
end;

procedure TArchivoIDSE.AgregarMovimientoADataSet(
   iTipo : integer;
   dFecha : TDateTime;
   lValido : boolean;
   sNSS, sMovimiento : string );
begin
     if FDataSetMovimientos <> nil then
     begin

          if lValido then
          begin
              with FDataSetMovimientos do
              begin
                   if FMovimientosCargados < FMovimientosTotalCount then
                   begin
                      Append;
                      FieldByName( 'TIPO' ).AsInteger := iTipo;
                      FieldByName( 'NSS' ).AsString := sNSS;
                      FieldByName( 'FECHA' ).AsDateTime := dFecha;
                      FieldByName( 'PAGINA' ).AsInteger:= FLecturaPagina;
                      FieldByName( 'VALIDO' ).AsBoolean := lValido;
                      FieldByName( 'TEXTO' ).AsString := sMovimiento;

                   end;
              end;
          end;

    end;

end;

function TArchivoIDSE.ParsearMovimientos(const sTexto: string): integer;
var
   iTipo : integer;
   dFecha : TDateTime;
   lValido : boolean;
   sNSS : string;
begin
  iTipo := 0;
  dFecha := NullDateTime;
  sNSS := VACIO;

  Result := 0;
  if IDSENSSExpr.Exec( sTexto ) then
  begin
     sNSS := IDSENSSExpr.Match[1];

     if IDSEMovimientosExpr.Exec( sTexto ) then
     begin
         iTipo :=  StrToIntDef( IDSEMovimientosExpr.Match[1], 0 );
         dFecha := ParseFechaIDSE(  IDSEMovimientosExpr.Match[3], IDSEMovimientosExpr.Match[4], IDSEMovimientosExpr.Match[5] );
         lValido := True;
     end
     else
     begin

                  lValido := False;

     end;

     AgregarMovimientoADataSet(iTipo, dFecha, lValido, sNSS, sTexto);
     Inc(FMovimientosCargados);
     Result := 1;
  end;
end;

function TArchivoIDSE.ParsearMovimientosVer2(const sTexto: string): integer;
const
     K_SUBTEXTO_RANGO_IZQUIERDA = 3;
     K_SUBTEXTO_RANGO_DERECHA = 100;
var
   iTipo {$ifdef PDF_TOOLKIT_3}, iPos {$endif}: integer;
   dFecha : TDateTime;
   lValido : boolean;
   sSubTexto, sNSS : string;
begin
  iTipo := 0;
  dFecha := NullDateTime;
  sNSS := VACIO;

  Result := 0;

  if ( IDSENSSExpr.Exec( sTexto ) ) then
  repeat
       sNSS:= IDSENSSExpr.Match[1];

       {$ifdef PDF_TOOLKIT_3}
       iPos := IDSENSSExpr.MatchPos[0] - K_SUBTEXTO_RANGO_IZQUIERDA;
       if ( iPos < 0 ) then iPos :=0;
       sSubTexto :=  copy (sTexto, iPos, K_SUBTEXTO_RANGO_DERECHA );
       {$else}
       sSubTexto :=  sTexto;
       {$endif}

       if IDSEMovimientosExpr.Exec( sSubTexto ) then
       begin
            iTipo :=  StrToIntDef( IDSEMovimientosExpr.Match[1], 0 );
            dFecha := ParseFechaIDSE(  IDSEMovimientosExpr.Match[3], IDSEMovimientosExpr.Match[4], IDSEMovimientosExpr.Match[5] );
            lValido := True;
       end
       else
       begin
           lValido := False;

             //2do intento
            if ( IDSEMovimientosExpr_TipoMov <> nil ) and ( IDSEMovimientosExpr_FechaMov <> nil ) then
            begin
                 if IDSEMovimientosExpr_TipoMov.Exec(sSubTexto) then
                 begin
                    iTipo :=  StrToIntDef( IDSEMovimientosExpr_TipoMov.Match[1], 0 );

                    if IDSEMovimientosExpr_FechaMov.Exec(sSubTexto) then
                    begin
                       dFecha :=   ParseFechaIDSE( IDSEMovimientosExpr_FechaMov.Match[1],  IDSEMovimientosExpr_FechaMov.Match[2], IDSEMovimientosExpr_FechaMov.Match[3]);
                       lValido := True;
                    end;

                 end;
            end
       end;

       AgregarMovimientoADataSet(iTipo, dFecha, lValido, sNSS, sSubTexto);
       Inc( Result );
  until  not IDSENSSExpr.ExecNext ;
  {$ifdef TEST}
   if FDataSetMovimientos <> nil then
   begin
   FDataSetMovimientos.SaveToFile(StringReplace(FArchivo, '.', '_',  [rfReplaceAll, rfIgnoreCase])  + '.xml', dfXMLUTF8 );
   end;

  {$endif}

end;


function TArchivoIDSE.LeerMovimiento(var sMovimiento: string): integer;
var
 sTexto: string;
 iLinea : integer;
begin
     Result := 0;
     sMovimiento := VACIO;

     if ( CargoArchivo ) and ( Movimientos > 0 ) then
     begin
          while TRUE do
          begin
              if (  FListaIndex  > FListaLineas.Count  ) then
              begin
              //Condicion de paro  (Termino de archivo )
                 if not AvanzarPagina then
                    Break;
              end;

              if ( FListaLineas <> nil ) and ( FListaLineas.Count > 0 ) then
              begin
                   for iLinea := FListaIndex to FListaLineas.Count do
                   begin
                   //Si encontro un match  Break;
                        sTexto := String( FListaLineas[iLinea] );
                        sTexto := UpperCase( sTexto );

                         if ( FVersion = K_VERSION_IDSE_1 ) then
                           Result := ParsearMovimientos(sTexto)
                         else
                         if ( FVersion = K_VERSION_IDSE_2 ) then
                           Result := ParsearMovimientosVer2(sTexto)
                         else
                         if ( FVersion = K_VERSION_IDSE_3 ) then
                           Result := ParsearMovimientosVer2(sTexto)
                         else
                         if ( FVersion = K_VERSION_IDSE_4 ) then
                           Result := ParsearMovimientosVer2(sTexto);

                         if (Result > 0 ) then
                            break;
                   end;

                   Inc(iLinea);
                   FListaIndex := iLinea;

                   if ( Result > 0  ) then
                        Break; //Condicion dado el match

              end
              else
                  Break; //Condicion de Paro defensivo
          end;


     end;
end;

function TArchivoIDSE.LeerEncabezadoVersion2: boolean;
var
   List : TgtWideStringList;
   IDSELoteExpr : TRegExpr;
   IDSEFecExpr : TRegExpr;
   IDSETotalesExpr : TRegExpr;
   IDSERegPatronExpr : TRegExpr;
   IDSERegPatronExpr_Sig : TRegExpr;

   Pag, LI : integer;
   sTexto : string;
   PatronSig : string; //Registro Patronal encontrado en las siguientes p�ginas

   lHuboLote, lHuboFecha, lHuboCant,lHuboRegPatron : boolean;

begin
    lHuboLote := FALSE;
    lHuboFecha:= FALSE;
    lHuboCant := FALSE;
    lHuboRegPatron := FALSE;

    IDSELoteExpr :=   TRegExpr.Create;
    IDSEFecExpr :=  TRegExpr.Create;
    IDSETotalesExpr := TRegExpr.Create;
    IDSERegPatronExpr := TRegExpr.Create;
    IDSERegPatronExpr_Sig := TRegExpr.Create;

    IDSELoteExpr.Expression := K_IDSE_EXPRESION_LOTE_VER2;
    IDSEFecExpr.Expression := K_IDSE_EXPRESION_FECHA_TRAN_VER2;
    IDSETotalesExpr.Expression := K_IDSE_EXPRESION_TOTALES_VER2;
    IDSERegPatronExpr.Expression := K_IDSE_EXPRESION_REG_PATRON_VER2;
    IDSERegPatronExpr_Sig.Expression := K_IDSE_EXPRESION_REG_PATRON_VER2_1;


    try
       for Pag := 1 to K_TOPE_PAGINA_ENCABEZADO_VER2 do
       begin
            // DES #12973: Validaci�n de Movimientos IDSE, procesar nuevo formato
            // List := gtPDFDocumento.ExtractTextFromPage(Pag, False);
            List := gtPDFDocumento.ExtractTextFromPage(Pag, TRUE);

            for LI := 0 to List.Count  do
            begin
                 sTexto := String( List[LI] );
                 sTexto := UpperCase( sTexto );

                 if IDSELoteExpr.Exec( sTexto ) then
                 begin
                      FNumeroLote :=  IDSELoteExpr.Match[1];
                      lHuboLote := True;
                 end;

                 if IDSERegPatronExpr.Exec( sTexto ) then
                 begin
                      FRegistroPatron := IDSERegPatronExpr.Match[1];
                      if Pos( FRegistroPatron,'-') > 0 then
                      begin
                           FRegistroPatron := Copy(FRegistroPatron,0,Pos( FRegistroPatron,'-')- 1 );
                      end;
                      lHuboRegPatron := True;
                 end;

                 if IDSERegPatronExpr_Sig.Exec( sTexto ) then
                 begin
                      PatronSig := IDSERegPatronExpr_Sig.Match[1];
                      if Pos( PatronSig,'-') > 0 then
                      begin
                           PatronSig := Copy(PatronSig,0,Pos( PatronSig,'-')- 1 );
                      end;

                      if ( lHuboRegPatron ) then
                      begin
                         if (   Length(PatronSig)  > Length(FRegistroPatron) ) then
                              FRegistroPatron := PatronSig;
                      end
                      else
                      begin
                           FRegistroPatron := PatronSig;
                      end;

                      lHuboRegPatron := True;
                 end;


                 if IDSEFecExpr.Exec( sTexto ) then
                 begin
                      FFechaTransmision :=  ParseFechaIDSE( IDSEFecExpr.Match[1],IDSEFecExpr.Match[2], IDSEFecExpr.Match[3]);
                      lHuboFecha := True;
                 end;

                 if IDSETotalesExpr.Exec( sTexto ) then
                 begin
                      {$ifdef PDK_TOOLKIT_3}
                      FMovimientosBajasCount := StrToIntDef( IDSETotalesExpr.Match[1],0 );
                      FMovimientosCambiosCount := StrToIntDef( IDSETotalesExpr.Match[2],0 );
                      FMovimientosReingresosCount := StrToIntDef( IDSETotalesExpr.Match[3],0 );
                      FMovimientosTotalCount := StrToIntDef( IDSETotalesExpr.Match[4],0 );
                      {$else}
                      FMovimientosBajasCount := StrToIntDef( IDSETotalesExpr.Match[5],0 );
                      FMovimientosCambiosCount := StrToIntDef( IDSETotalesExpr.Match[6],0 );
                      FMovimientosReingresosCount := StrToIntDef( IDSETotalesExpr.Match[7],0 );
                      FMovimientosTotalCount := StrToIntDef( IDSETotalesExpr.Match[8],0 );
                      {$endif}
                      lHuboCant :=  ( FMovimientosTotalCount > 0  );
                 end;
            end;

        end;
     finally
            FreeAndNil(IDSELoteExpr);
            FreeAndNil(IDSEFecExpr);
            FreeAndNil(IDSETotalesExpr);
            FreeAndNil(IDSERegPatronExpr);
            FreeAndNil(IDSERegPatronExpr_Sig);
            FreeAndNil(List);
     end;

     Result := ( lHuboLote and lHuboFecha and lHuboCant and lHuboRegPatron);
end;


function TArchivoIDSE.LeerEncabezadoVersion3: boolean;
var
   List : TgtWideStringList;
   IDSELoteExpr : TRegExpr;
   IDSEFecExpr : TRegExpr;
   IDSETotalesExpr : TRegExpr;
   IDSERegPatronExpr : TRegExpr;
   IDSERegPatronExpr_Sig : TRegExpr;

   Pag, LI : integer;
   sTexto : string;
   PatronSig : string; //Registro Patronal encontrado en las siguientes p�ginas

   lHuboLote, lHuboFecha, lHuboCant, lHuboRegPatron : boolean;

begin
    lHuboLote := FALSE;
    lHuboFecha:= FALSE;
    lHuboCant := FALSE;
    lHuboRegPatron := False;
    FRegistroPatron := VACIO;

    IDSELoteExpr :=   TRegExpr.Create;
    IDSEFecExpr :=  TRegExpr.Create;
    IDSETotalesExpr := TRegExpr.Create;
    IDSERegPatronExpr :=  TRegExpr.Create;
    IDSERegPatronExpr_Sig := TRegExpr.Create;


    IDSELoteExpr.Expression := K_IDSE_EXPRESION_LOTE_VER3 ;
    IDSEFecExpr.Expression := K_IDSE_EXPRESION_FECHA_TRAN_VER3 ;
    IDSETotalesExpr.Expression := K_IDSE_EXPRESION_TOTALES_VER3 ;
    IDSERegPatronExpr.Expression := K_IDSE_EXPRESION_REG_PATRON_VER3;
    IDSERegPatronExpr_Sig.Expression := K_IDSE_EXPRESION_REG_PATRON_VER3_1;


    try
       for Pag := 1 to K_TOPE_PAGINA_ENCABEZADO_VER3 do
       begin
            // DES #12973: Validaci�n de Movimientos IDSE, procesar nuevo formato
            // List := gtPDFDocumento.ExtractTextFromPage(Pag, False);
            List := gtPDFDocumento.ExtractTextFromPage(Pag, TRUE);

            {$ifdef TEST}
            gtPDFDocumento.SaveAsText (Format ('C:\TMP\IDSE_TEXT3_%d.txt', [Pag]), '1');
            {$endif}

            for LI := 0 to List.Count  do
            begin
                 sTexto := String( List[LI] );
                 sTexto := UpperCase( sTexto );

                 if IDSELoteExpr.Exec( sTexto ) then
                 begin
                      FNumeroLote :=  IDSELoteExpr.Match[1];
                      lHuboLote := True;
                 end;

                 if IDSERegPatronExpr.Exec( sTexto ) then
                 begin
                      PatronSig := IDSERegPatronExpr.Match[1];
                      if Pos( PatronSig,'-') > 0 then
                      begin
                           PatronSig := Copy(PatronSig,0,Pos( PatronSig,'-')- 1 );
                      end;

                      if ( lHuboRegPatron ) then
                      begin
                         if (   Length(PatronSig)  > Length(FRegistroPatron) ) then
                              FRegistroPatron := PatronSig;
                      end
                      else
                      begin
                           FRegistroPatron := PatronSig;
                      end;

                      lHuboRegPatron := True;
                 end;

                 if IDSERegPatronExpr_Sig.Exec( sTexto ) then
                 begin
                      PatronSig := IDSERegPatronExpr_Sig.Match[1];
                      if Pos( PatronSig,'-') > 0 then
                      begin
                           PatronSig := Copy(PatronSig,0,Pos( PatronSig,'-')- 1 );
                      end;

                      if ( lHuboRegPatron ) then
                      begin
                         if (   Length(PatronSig)  > Length(FRegistroPatron) ) then
                              FRegistroPatron := PatronSig;
                      end
                      else
                      begin
                           FRegistroPatron := PatronSig;
                      end;

                      lHuboRegPatron := True;
                 end;

                 if IDSEFecExpr.Exec( sTexto ) then
                 begin
                      FFechaTransmision :=  ParseFechaIDSE( IDSEFecExpr.Match[3],IDSEFecExpr.Match[2], IDSEFecExpr.Match[1]);
                      lHuboFecha := True;
                 end;

                 if IDSETotalesExpr.Exec( sTexto ) then
                 begin
                      FMovimientosBajasCount := StrToIntDef( IDSETotalesExpr.Match[1],0 );
                      FMovimientosCambiosCount := StrToIntDef( IDSETotalesExpr.Match[2],0 );
                      FMovimientosReingresosCount := StrToIntDef( IDSETotalesExpr.Match[3],0 );
                      FMovimientosTotalCount := StrToIntDef( IDSETotalesExpr.Match[4],0 );
                      lHuboCant :=  ( FMovimientosTotalCount > 0  );
                 end;
            end;

        end;
     finally
            FreeAndNil(IDSELoteExpr);
            FreeAndNil(IDSEFecExpr);
            FreeAndNil(IDSETotalesExpr);
            FreeAndNil(IDSERegPatronExpr);
            FreeAndNil(IDSERegPatronExpr_Sig);
            FreeAndNil(List);
     end;

     Result := ( lHuboLote and lHuboFecha and lHuboCant and lHuboRegPatron );
end;

function TArchivoIDSE.LeerEncabezadoVersion4: boolean;
const
     K_POS_NUM_LOTE = 3;

     // Correcci�n de bug #13356: ELECTRO COMPONENTES DE MEXICO-Herramienta Validaci�n IDSE marca error al intentar validar archivo
     // K_LINEAS_INFORMACION_GENERAL = 7;
     // Se dar�n m�s iteraciones para encontrar los datos del encabezado.
     K_LINEAS_INFORMACION_GENERAL = 9;

     K_REG_PATRONAL_LONGITUD = 11;
     // DES #15008: Validaci�n de Movimientos IDSE- El archivo no pudo ser le�do,
	 // no contiene la informaci�n necesaria para ser procesado.
     K_NOMBRE = 'Nombre:';
     K_REGISTRO_PATRONAL = 'Registro Patronal:';
var
   List : TgtWideStringList;
   ListMovimientos : TgtWideStringList;
   IDSELoteExpr : TRegExpr;
   IDSEFecExpr : TRegExpr;
   IDSETotalesExpr : TRegExpr;
   IDSERegPatronExpr : TRegExpr;
   IDSEInformacion_General : TRegExpr;
   Pag, LI : integer;
   sTexto : string;

   lHuboLote, lHuboFecha, lHuboCant, lHuboRegPatron : boolean;
   LI_INF_GEN: Integer;

   function asignarLote: boolean;
   begin
        Result := FALSE;
        if IDSELoteExpr.Exec( sTexto ) then
        begin
             FNumeroLote :=  IDSELoteExpr.Match[1];
             Result := TRUE;
        end
   end;


begin
    lHuboLote := FALSE;
    lHuboFecha:= FALSE;
    lHuboCant := FALSE;
    lHuboRegPatron := False;
    FRegistroPatron := VACIO;

    IDSELoteExpr :=   TRegExpr.Create;
    IDSEFecExpr :=  TRegExpr.Create;
    IDSETotalesExpr := TRegExpr.Create;
    IDSERegPatronExpr :=  TRegExpr.Create;

    IDSELoteExpr.Expression := K_IDSE_EXPRESION_LOTE_VER4 ;
    IDSEFecExpr.Expression := K_IDSE_EXPRESION_FECHA_TRAN_VER4 ;
    IDSETotalesExpr.Expression := K_IDSE_EXPRESION_TOTALES_VER4 ;
    IDSERegPatronExpr.Expression := K_IDSE_EXPRESION_REG_PATRON_VER4;
    IDSEInformacion_General :=   TRegExpr.Create;
    IDSEInformacion_General.Expression := K_IDSE_INFORMACION_GENERAL;

    try
       for Pag := 1 to K_TOPE_PAGINA_ENCABEZADO_VER4 do
       begin
            List := gtPDFDocumento.ExtractTextFromPage(Pag, FALSE);
            ListMovimientos := gtPDFDocumento.ExtractTextFromPage(Pag, TRUE);

            {$ifdef TEST}
            gtPDFDocumento.SaveAsText (Format ('C:\TMP\IDSE_TEXT4_%d.txt', [Pag]), '1');
            {$endif}

            for LI := 0 to List.Count  do
            begin
                 sTexto := String( List[LI] );
                 sTexto := UpperCase( sTexto );

                 if IDSEInformacion_General.Exec( sTexto ) then
                 begin
                      // Una vez que se encontr� la frase "Informaci�n General",
                      // buscar en las siguientes 7 posiciones los datos del encabezado:
                      // Registro patronal -- Lote -- Fecha
                      // ----------------------------------------------------------------

                      for LI_INF_GEN := LI to (LI + K_LINEAS_INFORMACION_GENERAL)  do
                      begin
                           // Buscar registro patronal.
                           // Se busca que inicie con letra y tenga 10 d�gitos
                           sTexto := String( List[LI_INF_GEN] );
                           sTexto := UpperCase( sTexto );
                           if IDSERegPatronExpr.Exec( sTexto ) and (Length (sTexto) = K_REG_PATRONAL_LONGITUD) then
                           begin
                                FRegistroPatron := IDSERegPatronExpr.Match[1];
                                lHuboRegPatron := True;
                           end;

                           // Encontrar fecha y hora de recepci�n del lote
                           sTexto := String( List[LI_INF_GEN] );
                           sTexto := UpperCase( sTexto );
                           if IDSEFecExpr.Exec( sTexto ) then
                           begin
                                FFechaTransmision :=  ParseFechaIDSE( IDSEFecExpr.Match[3],IDSEFecExpr.Match[2], IDSEFecExpr.Match[1]);
                                lHuboFecha := True;
                           end;

                           // Salir si ya se encontraron el registro patronal y la fecha
                           {if lHuboRegPatron and lHuboFecha then
                              break;}
                      end;

                      sTexto := String( List[LI + K_POS_NUM_LOTE]);
                      sTexto := UpperCase( sTexto );
                      lHuboLote := asignarLote;
                      if not lHuboLote then
                      begin
                           sTexto := String( List[LI + K_POS_NUM_LOTE + 1]);
                           sTexto := UpperCase( sTexto );
                           lHuboLote := asignarLote;
                      end;

                      // Detener ciclo
                      break;
                 end;
            end;

            for LI := 0 to ListMovimientos.Count  do
            begin
                 sTexto := String( ListMovimientos[LI] );
                 sTexto := UpperCase( sTexto );

                 // DES #15008: Validaci�n de Movimientos IDSE- El archivo no pudo ser le�do,
				 // no contiene la informaci�n necesaria para ser procesado.
                 sTexto := StringReplace (sTexto, K_NOMBRE, VACIO, [rfReplaceAll, rfIgnoreCase]);
                 sTexto := StringReplace (sTexto, K_REGISTRO_PATRONAL, VACIO, [rfReplaceAll, rfIgnoreCase]);                 

                 if IDSETotalesExpr.Exec( sTexto ) then
                 begin
                      FMovimientosBajasCount := StrToIntDef( IDSETotalesExpr.Match[1],0 );
                      FMovimientosCambiosCount := StrToIntDef( IDSETotalesExpr.Match[2],0 );
                      FMovimientosReingresosCount := StrToIntDef( IDSETotalesExpr.Match[3],0 );
                      FMovimientosTotalCount := StrToIntDef( IDSETotalesExpr.Match[4],0 );
                      lHuboCant :=  ( FMovimientosTotalCount > 0  );
                 end;
            end;

        end;
     finally
            FreeAndNil(IDSELoteExpr);
            FreeAndNil(IDSEFecExpr);
            FreeAndNil(IDSETotalesExpr);
            FreeAndNil(IDSERegPatronExpr);
            FreeAndNil(IDSEInformacion_General);
            FreeAndNil(List);
     end;

     Result := ( lHuboLote and lHuboFecha and lHuboCant and lHuboRegPatron );
end;

function TArchivoIDSE.GetLineasMovimientos: TgtWideStringList;
begin
     Result := nil;

      if ( FLecturaPagina > 0 ) then
      begin
           // DES #12973: Validaci�n de Movimientos IDSE, procesar nuevo formato
           // FListaLineas := gtPDFDocumento.ExtractTextFromPage(FLecturaPagina, False);
           FListaLineas := gtPDFDocumento.ExtractTextFromPage(FLecturaPagina, TRUE);

           Result := FListaLineas;
      end;

end;

end.



