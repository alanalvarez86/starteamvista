unit ZGridModeTools;

interface

uses Classes, cxFilter, cxCustomData, cxGridDBTableView, cxGridDBDataDefinitions, DB, SysUtils,
     ZetaClientDataSet, ZetaCommonTools, ZetaCommonClasses;

procedure BorrarItemGenerico( AValueList: TcxDataFilterValueList; oKindFiltro: TcxFilterValueItemKind );
procedure BorrarItemGenericoAll( AValueList: TcxDataFilterValueList );
procedure LlenaDataValues( AValueList: TcxDataFilterValueList; oField: TField; oDataSet: TDataSet );
procedure CreaColumnaSumatoria(Columna:TcxGridDBColumn; TipoFormato: Integer ; TextoSumatoria: String ; FooterKind : tcxSummaryKind );
procedure AsignaValorColumnaSummary(ASender: TcxDataSummary);
procedure LimpiaCacheGrid( ZetaDBGridDBTableView: TcxGridDBTableView );
procedure FiltroSetValueLista( ZetaDBGridDBTableView: TcxGridDBTableView; AItemIndex: Integer; AValueList: TcxDataFilterValueList );
const
     {***Formastos para las sumatorias***}
     K_SIN_TIPO = 0;
     K_TIPO_MONEDA = 1;
     K_ESPACIO = ' ';
     K_FORMATO_MONEDA = ',0.00;-,0.00';
     K_FORMATO_DEFAULT ='0';

implementation

procedure BorrarItemGenerico( AValueList: TcxDataFilterValueList; oKindFiltro: TcxFilterValueItemKind );
var
   AIndex: Integer;
begin
     AIndex := AValueList.FindItemByKind(oKindFiltro);
     if ( AIndex <> -1 ) then
          AValueList.Delete( AIndex );
end;

procedure BorrarItemGenericoAll( AValueList: TcxDataFilterValueList );
begin
     BorrarItemGenerico( AValueList, fviCustom );
     BorrarItemGenerico( AValueList, fviBlanks );
     BorrarItemGenerico( AValueList, fviNonBlanks );
end;

procedure CreaColumnaSumatoria( Columna:TcxGridDBColumn; TipoFormato: Integer; TextoSumatoria: String; FooterKind : tcxSummaryKind );

   procedure SeleccionTexto;
   const
        K_VACIO = '';
   begin
        if TextoSumatoria = K_VACIO then
        begin
             case FooterKind of
                  skCount: TextoSumatoria:= 'Registros: ';
                  skSum: TextoSumatoria:= 'Total: ';
                  skAverage: TextoSumatoria:= 'Promedio: ';
                  skMax: TextoSumatoria:= ' M�ximo: ';
                  skMin:  TextoSumatoria:= ' M�nimo: ';
             end;
        end;
   end;

begin
     if Columna.Visible then
     begin
          if ( not Columna.GridView.OptionsView.Footer ) then   //Si la banda no esta visible se muestra
             Columna.GridView.OptionsView.Footer := TRUE;
           with Columna.GridView.DataController.Summary do
           begin
                BeginUpdate;
                try
                   with FooterSummaryItems.Add as TcxGridDBTableSummaryItem do
                   begin
                        Column := Columna;
                        Kind := FooterKind;
                         //***Se aplica un texto Default si el parametro de texto se mando vacio.**
                        SeleccionTexto;
                        //***Tipo de formato que se desee aplicar**
                        //Al agregar el tipo de formato declarando la constante en esta unidad;
                        case TipoFormato of
                             K_TIPO_MONEDA : Format := TextoSumatoria + K_ESPACIO + K_FORMATO_MONEDA;
                             K_SIN_TIPO : Format := TextoSumatoria + K_ESPACIO + K_FORMATO_DEFAULT;
                        end;
                   end;
                finally
                       EndUpdate;
                end;
           end;
     end;
end;

procedure AsignaValorColumnaSummary(ASender: TcxDataSummary);
var
   i, iPosFooter: Integer;
   ADataController: TcxGridDBDataController;
   oListaSumatorias: TStringList;
   cdsClone: TZetaClientDataSet;
   rSumaValor: Double;
begin
     oListaSumatorias := TStringList.Create;
     try
        with ASender do
        begin
             ADataController := TcxGridDBDataController( DataController );
             if Assigned( ADataController.DataSet ) and ADataController.DataSet.Active then
             begin
                  for i := 0 to FooterSummaryItems.Count - 1 do
                  begin
                       if ( FooterSummaryItems[i].Kind = skCount ) then
                       begin
                            FooterSummaryValues[i] := ADataController.DataSet.RecordCount;
                       end
                       else if ( FooterSummaryItems[i].Kind = skSum ) then
                       begin
                            oListaSumatorias.Add( Format( '%d=%g', [ i, 0.0 ] ) );
                       end;
                  end;
                  // Si hay sumatorias se barre el dataset y se obtienen valores sumados de cada columna
                  if ( oListaSumatorias.Count > 0 ) then
                  begin
                       cdsClone := TZetaClientDataSet.Create(nil);
                       try
                          with cdsClone do
                          begin
                               CloneCursor( TZetaClientDataSet( ADataController.DataSet ), FALSE );    // Se conserva el filtro del dataset
                               First;
                               while ( not EOF ) do
                               begin
                                    for i := 0 to oListaSumatorias.Count - 1 do
                                    begin
                                         iPosFooter := StrToIntDef( oListaSumatorias.Names[i], 0 );
                                         rSumaValor := StrToFloatDef( oListaSumatorias.ValueFromIndex[i], 0.0 ) + FieldByName( TcxGridDBTableSummaryItem(ASender.FooterSummaryItems[iPosFooter]).Column.Name ).AsFloat;
                                         oListaSumatorias[i] := Format( '%d=%g', [ iPosFooter, rSumaValor ] );
                                    end;
                                    Next;
                               end;
                               Close;
                          end;
                       finally
                              cdsClone.Free;
                       end;
                       for i := 0 to oListaSumatorias.Count - 1 do
                       begin
                            FooterSummaryValues[ StrToIntDef( oListaSumatorias.Names[i], 0 ) ] := StrToFloatDef( oListaSumatorias.ValueFromIndex[i], 0.0 );
                       end;
                  end;
             end;
        end;
     finally
            oListaSumatorias.Free;
     end;
end;

procedure LimpiaCacheGrid( ZetaDBGridDBTableView: TcxGridDBTableView );
var
   i: Integer;
   oColumna: TcxGridDBColumn;
begin
     for i := 0 to ZetaDBGridDBTableView.ColumnCount -1 do
     begin
          oColumna := ZetaDBGridDBTableView.Columns[ i ];
          if ( oColumna.Tag > 0 ) then
          begin
               try
                  if TObject( oColumna.Tag ).ClassName = 'TStringList' then    // Tiene asociada una lista fija
                  begin
                       TStringList( oColumna.Tag ).Free;
                       oColumna.Tag := 0;
                  end;
               except
                     //Abort;   // excepci�n silenciosa - si no es un objeto TStringList, no lo elimina
               end;
          end;
     end;
end;

procedure LlenaDataValues( AValueList: TcxDataFilterValueList; oField: TField; oDataSet: TDataSet );
var
   sFieldName: String;
   cdsClone: TZetaClientDataSet;
   ValorAnterior, ValorActual: Variant;
begin
     sFieldName := oField.FieldName;
     cdsClone := TZetaClientDataSet.Create(nil);
     try
        with cdsClone do
        begin
             CloneCursor( TZetaClientDataSet( oDataSet ), TRUE, TRUE );
             FieldByName( sFieldName ).OnGetText := oField.OnGetText;
             FieldByName( sFieldName ).Tag := oField.Tag;
             IndexFieldNames := sFieldName;
             SetVariantToNull( ValorAnterior );
             First;
             while ( not EOF ) do
             begin
                  ValorActual := FieldByName( sFieldName ).Value;
                  if ( ValorActual <> ValorAnterior ) then
                  begin
                       AValueList.Add( fviValue, ValorActual, FieldByName( sFieldName ).DisplayText, TRUE );
                       ValorAnterior := ValorActual;
                  end;
                  Next;
             end;
             Close;
        end;
     finally
            cdsClone.Free;
     end;
end;

procedure FiltroSetValueLista( ZetaDBGridDBTableView: TcxGridDBTableView; AItemIndex: Integer;
          AValueList: TcxDataFilterValueList );
var
   oField: TField;
   sFieldName: String;
   cdsClone: TZetaClientDataSet;
   oListaLookup: TStringList;

   procedure StoreLookupValues;
   var
      ValorAnterior, ValorActual: String;
   begin
        sFieldName := oField.KeyFields;
        oListaLookup := TStringList.Create;
        cdsClone := TZetaClientDataSet.Create(nil);
        try
           with cdsClone do
           begin
                CloneCursor( TZetaClientDataSet( ZetaDBGridDBTableView.DataController.DataSet ), TRUE, TRUE );
                IndexFieldNames := sFieldName;
                ValorAnterior := ZetaCommonClasses.VACIO;
                First;
                while ( not EOF ) do
                begin
                     ValorActual := FieldByName( sFieldName ).AsString;
                     if ( ValorActual <> ValorAnterior ) then
                     begin
                          oListaLookup.Add( Format( '%s=%s', [ TZetaLookupDataSet( oField.LookupDataSet ).GetDescripcion( ValorActual ), ValorActual ] ) );
                          ValorAnterior := ValorActual;
                     end;
                     Next;
                end;
                Close;
                oListaLookup.Sort;
           end;
        finally
               ZetaDBGridDBTableView.Columns[ AItemIndex ].Tag := Integer( oListaLookup );
               cdsClone.Free;
        end;
   end;

   procedure LlenaLookupValues;
   var
      i: Integer;
   begin
        if ( ZetaDBGridDBTableView.Columns[ AItemIndex ].Tag = 0 ) then
           StoreLookupValues;
        if ( ZetaDBGridDBTableView.Columns[ AItemIndex ].Tag > 0 ) then
        begin
             try
                oListaLookup := TStringList( ZetaDBGridDBTableView.Columns[ AItemIndex ].Tag );
                for i := 0 to oListaLookup.Count - 1 do
                begin
                     AValueList.Add( fviValue, oListaLookup.ValueFromIndex[i], oListaLookup.Names[i], TRUE );
                end;
             except
                   //Abort;   // excepci�n silenciosa - No continua con el llenado
             end;
        end;
   end;

begin
     oField := ZetaDBGridDBTableView.Columns[ AItemIndex ].DataBinding.Field;
     if Assigned( oField ) then
     begin
          if ( oField.FieldKind = fkLookup ) then
             LlenaLookupValues
          else if ( oField.FieldKind = fkData ) and ( oField.DataType <> ftMemo ) then         // Los campos memo no se pueden ordenar
             LlenaDataValues( AValueList, oField, ZetaDBGridDBTableView.DataController.DataSet );
     end;
end;

end.
