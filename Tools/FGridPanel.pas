unit FGridPanel;

interface

uses
   Controls,
   Classes,
   ExtCtrls,
   Graphics,
   SysUtils,
   ZetaCommonClasses,
   FIndicadores,
   ZetaCXGrid,
   VCLTee.Chart,
   VCLTee.TeEngine,
   ZetaDialogo,
   ZetaClientDataSet,
   VCLTee.TeCanvas,
   FIndicadorListado,
   FGraficasDashlets;

type
  eTipoDashlet = (tpNingunoTipo, tpListado, tpGrafica, tpIndicador);
  eEstiloDashlet = (tpNingunoEstilo, tpColumnas, tpBarras, tpPastel, tpHorizStack);

  TComponenteGridDashlet = class
  private
    { private declarations here }
    FName: String;
    FControl: TWinControl;
    FColumna: Integer;
    FRenglon: Integer;
    FColumnaWidth: Integer;
    FRenglonHeight: Integer;
    FElemento: Integer;
    FDashletID: Integer;
    FFAOwner: TComponent;
    FFParent: TWinControl;
    function GetColumna: Integer;
    function GetColumnaWidth: Integer;
    function GetElemento: Integer;
    function GetName: String;
    function GetRenglon: Integer;
    function GetRenglonHeight: Integer;
    function GetControl: TWinControl;
    procedure SetColumna(const Value: Integer);
    procedure SetColumnaWidth(const Value: Integer);
    procedure SetElemento(const Value: Integer);
    procedure SetControl(const Value: TWinControl);
    procedure SetName(const Value: String);
    procedure SetRenglon(const Value: Integer);
    procedure SetRenglonHeight(const Value: Integer);
    function GetDashletID: Integer;
    procedure SetDashletID(const Value: Integer);
    function GetFAOwner: TComponent;
    function GetFParent: TWinControl;
    procedure SetFAOwner(const Value: TComponent);
    procedure SetFParent(const Value: TWinControl);
  protected
    { protected declarations here }
  public
    { public declarations here }
    constructor Create(const FDataset: TZetaClientDataSet; ClassTypeComponente: TClass; const AOwner: TComponent; const Parent: TWinControl ); overload;
    constructor Create( const FDataset: TZetaClientDataSet; ClassTypeComponente: TClass; const AOwner: TComponent; const Parent: TWinControl; var BaseChart: TChart ); overload;
    constructor Create( const FDataset: TZetaClientDataSet; ClassTypeComponente: TClass; const AOwner: TComponent; const Parent: TWinControl; var BaseTCXGrid: TZetaCXGrid ); overload;
    property Name: String read GetName write SetName;
    property Control: TWinControl read GetControl write SetControl;
    property Columna: Integer read GetColumna write SetColumna;
    property Renglon: Integer read GetRenglon write SetRenglon;
    property ColumnaWidth: Integer read GetColumnaWidth write SetColumnaWidth;
    property RenglonHeight: Integer read GetRenglonHeight write SetRenglonHeight;
    property Elemento: Integer read GetElemento write SetElemento;
    property DashletID: Integer read GetDashletID write SetDashletID;
    property AOwnerProperty: TComponent read GetFAOwner write SetFAOwner;
    property ParentProperty: TWinControl read GetFParent write SetFParent;
  end;

  TGridPanelLayout = class
  private
    { private declarations here }
    FAnchoGrid: Integer;
    FAlto: Integer;
    FColumnas: Integer;
    FRenglones: Integer;
    FBaseChartBarrasHorizontal: TChart;
    FBaseChartColumnasVertical: TChart;
    FBaseChartPie: TChart;
    FBaseHorizStack: TChart;
    FBaseGridListado: TZetaCXGrid;
    FFAOwner: TComponent;
    FFParent: TWinControl;
    FGridPanelLayout: TGridPanel;
    function GetBaseChartBarrasHorizontal: TChart;
    function GetBaseChartColumnasVertical: TChart;
    function GetBaseChartPie: TChart;
    function GetBaseGridListado: TZetaCXGrid;
    procedure SetBaseChartBarrasHorizontal(const Value: TChart);
    procedure SetBaseChartColumnasVertical(const Value: TChart);
    procedure SetBaseChartPie(const Value: TChart);
    procedure SetBaseGridListado(const Value: TZetaCXGrid);
    function GetFAOwner: TComponent;
    function GetFParent: TWinControl;
    procedure SetFAOwner(const Value: TComponent);
    procedure SetFParent(const Value: TWinControl);
    function GetBaseHorizStack: TChart;
    procedure SetBaseHorizStack(const Value: TChart);
  protected
    { protected declarations here }
  public
    { public declarations here }
    constructor Create( var GridPanelBase: TGridPanel );
    procedure AgregarComponenteGrid( const oGridComponente: TComponenteGridDashlet );
    procedure AgregarComponenteIndicadoresListado( const oGridComponente: TComponenteGridDashlet );
    procedure AgregarComponenteChart( const oGridComponente: TComponenteGridDashlet; const ClassTypeComponente: TClass );
    procedure AgregarComponenteIndicadores( const oGridComponente: TComponenteGridDashlet );
    procedure TamanioComponenteGrid ( const oGridComponente: TComponenteGridDashlet );
    procedure GenerarTablero( const FDataset: TZetaClientDataSet );
    procedure GenerarTableroCusTom( const FDataset: TZetaClientDataSet; var oGridPanel: TGridPanel );
    function CrearComponentesUI( const FDataSet: TZetaClientDataSet; const ClassTypeComponente: TClass; const iNumeroComponente: Integer; eEstiloGrafica: eEstiloDashlet ): TComponenteGridDashlet;
    property AnchoGrid: Integer read FAnchoGrid write FAnchoGrid default 150;
    property AltoGrid: Integer read FAlto write FAlto default 87;
    property Columnas: Integer read FColumnas write FColumnas default 4;
    property Renglones: Integer read FRenglones write FRenglones default 6;
    property BaseChartBarrasHorizontal: TChart read GetBaseChartBarrasHorizontal write SetBaseChartBarrasHorizontal;
    property BaseChartColumnasVertical: TChart read GetBaseChartColumnasVertical write SetBaseChartColumnasVertical;
    property BaseChartPie: TChart read GetBaseChartPie write SetBaseChartPie;
    property BaseHorizStack: TChart read GetBaseHorizStack write SetBaseHorizStack;
    property BaseGridListado: TZetaCXGrid read GetBaseGridListado write SetBaseGridListado;
    property AOwnerProperty: TComponent read GetFAOwner write SetFAOwner;
    property ParentProperty: TWinControl read GetFParent write SetFParent;

end;

Const
     K_SEPARACION = 6;
     K_ANCHO = 150;
     K_ALTO = 87;
     K_CERO = 0;
     K_NAME_INDICADOR = 'INDICADOR_%d';
     K_NAME_GRAFICA = 'GRAFICA_%d';
     K_NAME_LEVEL = 'LEVEL_%d';
     K_NAME_VIEW = 'VIEW_%d';
     K_NAME_LISTADO = 'LISTADO_%d';

var
  GridPanelLayout: TGridPanelLayout;

implementation

constructor TGridPanelLayout.Create( var GridPanelBase: TGridPanel );
begin
     FGridPanelLayout := GridPanelBase;
end;

function TGridPanelLayout.GetFAOwner: TComponent;
begin
     Result := FFAOwner;
end;

function TGridPanelLayout.GetBaseChartBarrasHorizontal: TChart;
begin
     Result := FBaseChartBarrasHorizontal;
end;

function TGridPanelLayout.GetBaseChartColumnasVertical: TChart;
begin
     Result := FBaseChartColumnasVertical;
end;

function TGridPanelLayout.GetBaseChartPie: TChart;
begin
     Result := FBaseChartPie;
end;

function TGridPanelLayout.GetBaseGridListado: TZetaCXGrid;
begin
     Result := FBaseGridListado;
end;

function TGridPanelLayout.GetBaseHorizStack: TChart;
begin
     Result := FBaseHorizStack;
end;

function TGridPanelLayout.GetFParent: TWinControl;
begin
     Result := FFParent;
end;

procedure TGridPanelLayout.SetFAOwner(const Value: TComponent);
begin
     Self.FFAOwner := Value;
end;

procedure TGridPanelLayout.SetBaseChartBarrasHorizontal(const Value: TChart);
begin
     Self.FBaseChartBarrasHorizontal := Value;
end;

procedure TGridPanelLayout.SetBaseChartColumnasVertical(const Value: TChart);
begin
     Self.FBaseChartColumnasVertical := Value;
end;

procedure TGridPanelLayout.SetBaseChartPie(const Value: TChart);
begin
     Self.FBaseChartPie := Value;
end;

procedure TGridPanelLayout.SetBaseGridListado(const Value: TZetaCXGrid);
begin
     Self.FBaseGridListado := Value;
end;

procedure TGridPanelLayout.SetBaseHorizStack(const Value: TChart);
begin
     Self.FBaseHorizStack := Value;
end;

procedure TGridPanelLayout.SetFParent(const Value: TWinControl);
begin
     Self.FFParent := Value;
end;

procedure TGridPanelLayout.AgregarComponenteGrid( const oGridComponente: TComponenteGridDashlet );
begin
     try
     if ( oGridComponente.Control is TWinControl ) then
     begin
          with oGridComponente.Control do
          begin
               if ClassType = TIndicadoresListado then
               begin
                    AgregarComponenteIndicadoresListado( oGridComponente );
               end;

               if ClassType = TChart then
               begin
                    AgregarComponenteChart( oGridComponente, TChart );
               end;

               if ClassType = TGraficasDashletsColumnas then
               begin
                    AgregarComponenteChart( oGridComponente, TGraficasDashletsColumnas );
               end;

               if ClassType = TGraficasDashletsBarras then
               begin
                    AgregarComponenteChart( oGridComponente, TGraficasDashletsBarras );
               end;

               if ClassType = TGraficasDashletsPie then
               begin
                    AgregarComponenteChart( oGridComponente, TGraficasDashletsPie );
               end;

               if ClassType = TIndicadores then
               begin
                    AgregarComponenteIndicadores( oGridComponente )
               end;

               if ClassType = TGraficasDashletsHorizStack then
               begin
                    AgregarComponenteChart( oGridComponente, TGraficasDashletsHorizStack );
               end;
          end;
     end;
     except
           on Error: Exception do
           begin
                ZetaDialogo.ZError( 'Error', Error.Message, 0 );
           end;
     end;
end;

procedure TGridPanelLayout.AgregarComponenteIndicadoresListado( const oGridComponente: TComponenteGridDashlet );
begin
     try
        with FGridPanelLayout do
        begin
             ControlCollection.AddControl(  TIndicadoresListado( oGridComponente.Control ), oGridComponente.Columna, oGridComponente.Renglon );
             TamanioComponenteGrid ( oGridComponente );
        end;
     except
           on Error: Exception do
           begin
                ZetaDialogo.ZError( 'Error', Error.Message, 0 );
           end;
     end;
end;

procedure TGridPanelLayout.AgregarComponenteChart( const oGridComponente: TComponenteGridDashlet; const ClassTypeComponente: TClass );
begin
     try
        with FGridPanelLayout do
        begin
             if TGraficasDashletsColumnas.ClassNameIs( ClassTypeComponente.ClassName ) then
                ControlCollection.AddControl(  TGraficasDashletsColumnas( oGridComponente.Control ), oGridComponente.Columna, oGridComponente.Renglon )
             else if TGraficasDashletsBarras.ClassNameIs( ClassTypeComponente.ClassName ) then
                ControlCollection.AddControl(  TGraficasDashletsBarras( oGridComponente.Control ), oGridComponente.Columna, oGridComponente.Renglon )
             else if TGraficasDashletsPie.ClassNameIs( ClassTypeComponente.ClassName ) then
                ControlCollection.AddControl(  TGraficasDashletsPie( oGridComponente.Control ), oGridComponente.Columna, oGridComponente.Renglon )
             else if TGraficasDashletsHorizStack.ClassNameIs( ClassTypeComponente.ClassName ) then
                ControlCollection.AddControl(  TGraficasDashletsHorizStack( oGridComponente.Control ), oGridComponente.Columna, oGridComponente.Renglon );
        end;
        TamanioComponenteGrid ( oGridComponente );
     except
           on Error: Exception do
           begin
                ZetaDialogo.ZError( 'Error', Error.Message, 0 );
           end;
     end;
end;

procedure TGridPanelLayout.AgregarComponenteIndicadores( const oGridComponente: TComponenteGridDashlet );
begin
     try
        with FGridPanelLayout do
        begin
             ControlCollection.AddControl( TIndicadores( oGridComponente.Control), oGridComponente.Columna, oGridComponente.Renglon );
        end;
        TamanioComponenteGrid ( oGridComponente );
     except
           on Error: Exception do
           begin
                ZetaDialogo.ZError( 'Error', Error.Message, 0 );
           end;
     end;
end;

procedure TGridPanelLayout.TamanioComponenteGrid( const oGridComponente: TComponenteGridDashlet );
begin
     try
        with FGridPanelLayout do
        begin
             ControlCollection.Items[ oGridComponente.Elemento ].Column := oGridComponente.Columna;
             ControlCollection.Items[ oGridComponente.Elemento ].Row := oGridComponente.Renglon;
             ControlCollection.Items[ oGridComponente.Elemento ].ColumnSpan := oGridComponente.ColumnaWidth;
             ControlCollection.Items[ oGridComponente.Elemento ].RowSpan := oGridComponente.RenglonHeight;
             ControlCollection.Items[ oGridComponente.Elemento ].Control.Align := alClient;
        end;
     except
           on Error: Exception do
           begin
                ZetaDialogo.ZError( 'Error', Error.Message, 0 );
           end;
     end;
end;

function TGridPanelLayout.CrearComponentesUI( const FDataSet: TZetaClientDataSet; const ClassTypeComponente: TClass; const iNumeroComponente: Integer; eEstiloGrafica: eEstiloDashlet ): TComponenteGridDashlet;
var
   oGridComponente: TComponenteGridDashlet;
begin
     try
        Result := Nil;
        oGridComponente := Nil;
        if ClassTypeComponente.ClassName = TIndicadores.ClassName then
        begin
             oGridComponente := TComponenteGridDashlet.Create( FDataSet, TIndicadores, FGridPanelLayout, FGridPanelLayout );
             oGridComponente.Elemento := iNumeroComponente;
             Result := oGridComponente;
        end;
        if ( ClassTypeComponente.ClassName = TGraficasDashletsColumnas.ClassName ) or ( ClassTypeComponente.ClassName = TGraficasDashletsBarras.ClassName ) or ( ClassTypeComponente.ClassName = TGraficasDashletsPie.ClassName ) or ( ClassTypeComponente.ClassName = TGraficasDashletsHorizStack.ClassName ) then
        begin
             if tpColumnas = eEstiloGrafica then
                oGridComponente := TComponenteGridDashlet.Create( FDataSet, TGraficasDashletsColumnas, FGridPanelLayout, FGridPanelLayout, Self.FBaseChartColumnasVertical );
             if tpBarras = eEstiloGrafica then
                oGridComponente := TComponenteGridDashlet.Create( FDataSet, TGraficasDashletsBarras, FGridPanelLayout, FGridPanelLayout, Self.FBaseChartBarrasHorizontal );
             if tpPastel = eEstiloGrafica then
                oGridComponente := TComponenteGridDashlet.Create( FDataSet, TGraficasDashletsPie, FGridPanelLayout, FGridPanelLayout, Self.FBaseChartPie );
             if tpHorizStack = eEstiloGrafica then
                oGridComponente := TComponenteGridDashlet.Create( FDataSet, TGraficasDashletsHorizStack, FGridPanelLayout, FGridPanelLayout, Self.FBaseHorizStack );
             oGridComponente.Elemento := iNumeroComponente;
             Result := oGridComponente;
        end;
        if ClassTypeComponente.ClassName = TIndicadoresListado.ClassName then
        begin
             oGridComponente := TComponenteGridDashlet.Create( FDataSet, TIndicadoresListado, FGridPanelLayout, FGridPanelLayout, Self.FBaseGridListado );
             oGridComponente.Elemento := iNumeroComponente;
             Result := oGridComponente;
        end;
     except
           on Error: Exception do
           begin
                Result := nil;
                ZetaDialogo.ZError( 'Error', Error.Message, 0 );
           end;
     end;
end;

procedure TGridPanelLayout.GenerarTablero( const FDataset: TZetaClientDataSet );
var
   iNumeroComponente: Integer;
   oGridComponente: TComponenteGridDashlet;
begin
     iNumeroComponente := 0;
     try
        if FDataset <> nil then
        begin
             with FDataset do
             begin
                  First;
                  while ( not EOF ) do
                  begin
                       if ( eTipoDashlet( FieldByName( 'DashletTipo' ).AsInteger ) = tpListado )  then //Listado
                       begin
                            oGridComponente := CrearComponentesUI( FDataSet, TIndicadoresListado, iNumeroComponente, tpNingunoEstilo );
                            Self.AgregarComponenteGrid( oGridComponente );
                            inc( iNumeroComponente );
                       end;

                       if ( eTipoDashlet( FieldByName( 'DashletTipo' ).AsInteger ) = tpIndicador ) then //Indicadores
                       begin
                            oGridComponente := CrearComponentesUI( FDataSet, TIndicadores, iNumeroComponente, tpNingunoEstilo );
                            Self.AgregarComponenteGrid( oGridComponente );
                            inc( iNumeroComponente );
                       end;

                       if ( eTipoDashlet( FieldByName( 'DashletTipo' ).AsInteger ) = tpGrafica ) and ( eEstiloDashlet( FieldByName( 'DashletEstilo' ).AsInteger ) = tpColumnas ) then //Grafica Columnas
                       begin
                            Self.AgregarComponenteGrid( CrearComponentesUI( FDataSet, TGraficasDashletsColumnas, iNumeroComponente, tpColumnas ) );
                            inc( iNumeroComponente );
                       end;

                       if ( eTipoDashlet( FieldByName( 'DashletTipo' ).AsInteger ) = tpGrafica ) and ( eEstiloDashlet( FieldByName( 'DashletEstilo' ).AsInteger ) = tpBarras ) then //Grafica Barras
                       begin
                            Self.AgregarComponenteGrid( CrearComponentesUI( FDataSet, TGraficasDashletsBarras, iNumeroComponente, tpBarras ) );
                            inc( iNumeroComponente );
                       end;

                       if ( eTipoDashlet( FieldByName( 'DashletTipo' ).AsInteger ) = tpGrafica  ) and ( eEstiloDashlet( FieldByName( 'DashletEstilo' ).AsInteger ) = tpPastel ) then //Grafica Pie
                       begin
                            Self.AgregarComponenteGrid( CrearComponentesUI( FDataSet, TGraficasDashletsPie, iNumeroComponente, tpPastel ) );
                            inc( iNumeroComponente );
                       end;
                       if ( eTipoDashlet( FieldByName( 'DashletTipo' ).AsInteger ) = tpGrafica  ) and ( eEstiloDashlet( FieldByName( 'DashletEstilo' ).AsInteger ) = tpHorizStack ) then //Grafica Pie
                       begin
                            Self.AgregarComponenteGrid( CrearComponentesUI( FDataSet, TGraficasDashletsHorizStack, iNumeroComponente, tpHorizStack ) );
                            inc( iNumeroComponente );
                       end;
                       Next;
                  end;
             end;
        end;
     except
           on Error: Exception do
           begin
                ZetaDialogo.ZError( 'Error', Error.Message, 0 );
           end;
     end;
end;

procedure TGridPanelLayout.GenerarTableroCusTom( const FDataset: TZetaClientDataSet; var oGridPanel: TGridPanel );
var
   iNumeroComponente: Integer;
   oGridComponente: TComponenteGridDashlet;
begin
     iNumeroComponente := 0;
     try
        if FDataset <> nil then
        begin
             with FDataset do
             begin
                  First;
                  while ( not EOF ) do
                  begin
                       if ( eTipoDashlet( FieldByName( 'DashletTipo' ).AsInteger ) = tpListado )  then //Listado
                       begin
                            oGridComponente := CrearComponentesUI( FDataSet, TIndicadoresListado, iNumeroComponente, tpNingunoEstilo );
                            Self.AgregarComponenteGrid( oGridComponente );
                            inc( iNumeroComponente );
                       end;

                       if ( eTipoDashlet( FieldByName( 'DashletTipo' ).AsInteger ) = tpIndicador ) then //Indicadores
                       begin
                            oGridComponente := CrearComponentesUI( FDataSet, TIndicadores, iNumeroComponente, tpNingunoEstilo );
                            Self.AgregarComponenteGrid( oGridComponente );
                            inc( iNumeroComponente );
                       end;

                       if ( eTipoDashlet( FieldByName( 'DashletTipo' ).AsInteger ) = tpGrafica ) and ( eEstiloDashlet( FieldByName( 'DashletEstilo' ).AsInteger ) = tpColumnas ) then //Grafica Columnas
                       begin
                            Self.AgregarComponenteGrid( CrearComponentesUI( FDataSet, TGraficasDashletsColumnas, iNumeroComponente, tpColumnas ) );
                            inc( iNumeroComponente );
                       end;

                       if ( eTipoDashlet( FieldByName( 'DashletTipo' ).AsInteger ) = tpGrafica ) and ( eEstiloDashlet( FieldByName( 'DashletEstilo' ).AsInteger ) = tpBarras ) then //Grafica Barras
                       begin
                            Self.AgregarComponenteGrid( CrearComponentesUI( FDataSet, TGraficasDashletsBarras, iNumeroComponente, tpBarras ) );
                            inc( iNumeroComponente );
                       end;

                       if ( eTipoDashlet( FieldByName( 'DashletTipo' ).AsInteger ) = tpGrafica  ) and ( eEstiloDashlet( FieldByName( 'DashletEstilo' ).AsInteger ) = tpPastel ) then //Grafica Pie
                       begin
                            Self.AgregarComponenteGrid( CrearComponentesUI( FDataSet, TGraficasDashletsPie, iNumeroComponente, tpPastel ) );
                            inc( iNumeroComponente );
                       end;

                       if ( eTipoDashlet( FieldByName( 'DashletTipo' ).AsInteger ) = tpGrafica ) and ( eEstiloDashlet( FieldByName( 'DashletEstilo' ).AsInteger ) = tpHorizStack ) then //Grafica Barras
                       begin
                            Self.AgregarComponenteGrid( CrearComponentesUI( FDataSet, TGraficasDashletsHorizStack, iNumeroComponente, tpHorizStack ) );
                            inc( iNumeroComponente );
                       end;
                       Next;
                  end;
             end;
        end;
     except
           on Error: Exception do
           begin
                ZetaDialogo.ZError( 'Error', Error.Message, 0 );
           end;
     end;
end;

{ TComponenteGridDashlet }

constructor TComponenteGridDashlet.Create( const FDataset: TZetaClientDataSet; ClassTypeComponente: TClass; const AOwner: TComponent; const Parent: TWinControl );
const
     K_POS_COL = 1;
     K_POS_ROW = 1;
var
   Indicador: TIndicadores;
begin
     try
        if FDataset <> nil then
        begin
             with FDataset do
             begin
                  Self.SetColumna( FieldByName( 'TabDashCol' ).AsInteger - K_POS_COL );
                  Self.SetRenglon( FieldByName( 'TabDashRow' ).AsInteger - K_POS_ROW );
                  Self.SetColumnaWidth( FieldByName( 'TabDashWidth' ).AsInteger );
                  Self.SetRenglonHeight( FieldByName( 'TabDashHeight' ).AsInteger );

                  if ClassTypeComponente.ClassName = TIndicadores.ClassName then
                  begin
                       Self.SetName( Format( K_NAME_INDICADOR, [ FieldByName( 'DashletID' ).AsInteger ] ) );
                       Indicador := TIndicadores.Create(Self.GetName, AOwner, Parent);
                       Indicador.Titulo := FieldByName( 'DashletNombre' ).AsString;
                       Indicador.SubTitulo := FieldByName( 'DashletSubTitulo' ).AsString;
                       Indicador.LabelTendencia.Caption := VACIO;
                       Indicador.DashletIconos := FieldByName( 'DashletIconos' ).AsInteger;
                       Indicador.DashletIconosDesc := FieldByName( 'DashletIconosDesc' ).AsString;
                       Indicador.DashletFormato := FieldByName( 'DashletFormato' ).AsString;
                       Indicador.DashletFormatoSub := FieldByName( 'DashletFormatoSub' ).AsString;
                       Indicador.DashletTendenciaDesc := FieldByName( 'DashletTendenciaDesc' ).AsString;
                       Indicador.Parent:= Parent;

                       Self.SetControl( Indicador );
                  end;
             end;
        end;
     except
           on Error: Exception do
           begin
                ZetaDialogo.ZError( 'Error', Error.Message, 0 );
           end;
     end;
end;

constructor TComponenteGridDashlet.Create( const FDataset: TZetaClientDataSet; ClassTypeComponente: TClass; const AOwner: TComponent; const Parent: TWinControl; var BaseChart: TChart );
const
     K_POS_COL = 1;
     K_POS_ROW = 1;
var
   GraficaColumna: TGraficasDashletsColumnas;
   GraficaBarras: TGraficasDashletsBarras;
   GraficaPie: TGraficasDashletsPie;
   GraficaHorzStack: TGraficasDashletsHorizStack;
begin
     try
        if FDataset <> nil then
        begin
             with FDataset do
             begin
                  Self.SetName( Format( K_NAME_GRAFICA, [ FieldByName( 'DashletID' ).AsInteger ] ) );
                  Self.SetColumna( FieldByName( 'TabDashCol' ).AsInteger - K_POS_COL );
                  Self.SetRenglon( FieldByName( 'TabDashRow' ).AsInteger - K_POS_ROW );
                  Self.SetColumnaWidth( FieldByName( 'TabDashWidth' ).AsInteger );
                  Self.SetRenglonHeight( FieldByName( 'TabDashHeight' ).AsInteger );
                  if ClassTypeComponente.ClassName = TGraficasDashletsColumnas.ClassName then
                  begin
                       Self.SetName( Format( K_NAME_GRAFICA, [ FieldByName( 'DashletID' ).AsInteger ] ) );
                       GraficaColumna := TGraficasDashletsColumnas.Create( Self.GetName, AOwner, Parent );
                       CloneChart(GraficaColumna, BaseChart, AOwner, false);
                       GraficaColumna.Parent := Parent;
                       GraficaColumna.DefinirPropiedades;
                       GraficaColumna.Titulo := FieldByName( 'DashletNombre' ).AsString;
                       Self.SetControl( GraficaColumna );
                  end;
                  if ClassTypeComponente.ClassName = TGraficasDashletsBarras.ClassName then
                  begin
                       Self.SetName( Format( K_NAME_GRAFICA, [ FieldByName( 'DashletID' ).AsInteger ] ) );
                       GraficaBarras := TGraficasDashletsBarras.Create( Self.GetName, AOwner, Parent );
                       CloneChart(GraficaBarras, BaseChart, AOwner, false);
                       GraficaBarras.Parent := Parent;
                       GraficaBarras.DefinirPropiedades;
                       GraficaBarras.Height := Parent.Height;
                       GraficaBarras.Titulo := FieldByName( 'DashletNombre' ).AsString;
                       Self.SetControl( GraficaBarras );
                  end;
                  if ClassTypeComponente.ClassName = TGraficasDashletsPie.ClassName then
                  begin
                       Self.SetName( Format( K_NAME_GRAFICA, [ FieldByName( 'DashletID' ).AsInteger ] ) );
                       GraficaPie := TGraficasDashletsPie.Create( Self.GetName, AOwner, Parent );
                       CloneChart(GraficaPie, BaseChart, AOwner, false);
                       GraficaPie.Parent := Parent;
                       GraficaPie.DefinirPropiedades;
                       GraficaPie.Titulo := FieldByName( 'DashletNombre' ).AsString;
                       Self.SetControl( GraficaPie );
                  end;
                  if ClassTypeComponente.ClassName = TGraficasDashletsHorizStack.ClassName then
                  begin
                       Self.SetName( Format( K_NAME_GRAFICA, [ FieldByName( 'DashletID' ).AsInteger ] ) );
                       GraficaHorzStack := TGraficasDashletsHorizStack.Create( Self.GetName, AOwner, Parent );
                       CloneChart(GraficaHorzStack, BaseChart, AOwner, false);
                       GraficaHorzStack.Parent := Parent;
                       GraficaHorzStack.DefinirPropiedades;
                       GraficaHorzStack.Height := Parent.Height;
                       GraficaHorzStack.Titulo := FieldByName( 'DashletNombre' ).AsString;
                       Self.SetControl( GraficaHorzStack );
                  end;
             end;
        end;
     except
           on Error: Exception do
           begin
                ZetaDialogo.ZError( 'Error', Error.Message, 0 );
           end;
     end;
end;

constructor TComponenteGridDashlet.Create(const FDataset: TZetaClientDataSet; ClassTypeComponente: TClass; const AOwner: TComponent; const Parent: TWinControl; var BaseTCXGrid: TZetaCXGrid);
const
     K_POS_COL = 1;
     K_POS_ROW = 1;
var
   ListadoGrid: TIndicadoresListado;
begin
     try
        if FDataset <> nil then
        begin
             with FDataset do
             begin
                  if ClassTypeComponente.ClassName = TIndicadoresListado.ClassName then
                  begin
                       Self.SetName( Format( K_NAME_LISTADO, [ FieldByName( 'DashletID' ).AsInteger ] ) );
                       Self.SetColumna( FieldByName( 'TabDashCol' ).AsInteger - K_POS_COL );
                       Self.SetRenglon( FieldByName( 'TabDashRow' ).AsInteger - K_POS_ROW );
                       Self.SetColumnaWidth( FieldByName( 'TabDashWidth' ).AsInteger );
                       Self.SetRenglonHeight( FieldByName( 'TabDashHeight' ).AsInteger );

                       ListadoGrid := TIndicadoresListado.Create(Self.GetName, AOwner, Parent);
                       ListadoGrid.Titulo := FieldByName( 'DashletNombre' ).AsString;
                       ListadoGrid.Parent := Parent;
                       ListadoGrid.SetBorde;
                       Self.SetControl( ListadoGrid );
                  end
             end;
        end;
     except
           on Error: Exception do
           begin
                ZetaDialogo.ZError( 'Error', Error.Message, 0 );
           end;
     end;
end;

function TComponenteGridDashlet.GetFAOwner: TComponent;
begin
     Result := FFAOwner;
end;

function TComponenteGridDashlet.GetColumna: Integer;
begin
     Result := FColumna;
end;

function TComponenteGridDashlet.GetColumnaWidth: Integer;
begin
     Result := FColumnaWidth;
end;

function TComponenteGridDashlet.GetElemento: Integer;
begin
     Result := FElemento;
end;

function TComponenteGridDashlet.GetName: String;
begin
     Result := FName;
end;

function TComponenteGridDashlet.GetFParent: TWinControl;
begin
     Result := FFParent;
end;

function TComponenteGridDashlet.GetRenglon: Integer;
begin
     Result := FRenglon;
end;

function TComponenteGridDashlet.GetRenglonHeight: Integer;
begin
     Result := FRenglonHeight;
end;

function TComponenteGridDashlet.GetControl: TWinControl;
begin
     Result := FControl;
end;

function TComponenteGridDashlet.GetDashletID: Integer;
begin
     Result := FDashletID;
end;

procedure TComponenteGridDashlet.SetFAOwner(const Value: TComponent);
begin
     Self.FFAOwner := Value;
end;

procedure TComponenteGridDashlet.SetColumna(const Value: Integer);
begin
     Self.FColumna := Value;
end;

procedure TComponenteGridDashlet.SetColumnaWidth(const Value: Integer);
begin
     Self.FColumnaWidth := Value;
end;

procedure TComponenteGridDashlet.SetElemento(const Value: Integer);
begin
     Self.FElemento := Value;
end;

procedure TComponenteGridDashlet.SetControl(const Value: TWinControl);
begin
     Self.FControl := Value;
end;

procedure TComponenteGridDashlet.SetDashletID(const Value: Integer);
begin
     Self.FDashletID := Value;
end;

procedure TComponenteGridDashlet.SetName(const Value: String);
begin
     Self.FName := Value;
end;

procedure TComponenteGridDashlet.SetFParent(const Value: TWinControl);
begin
     Self.FFParent := Value;
end;

procedure TComponenteGridDashlet.SetRenglon(const Value: Integer);
begin
     Self.FRenglon := Value;
end;

procedure TComponenteGridDashlet.SetRenglonHeight(const Value: Integer);
begin
     Self.FRenglonHeight := Value;
end;

end.
