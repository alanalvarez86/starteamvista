unit FToolsRH;

interface

uses Controls, Dialogs, db, dbClient, SysUtils,
     ZetaCommonClasses,
     Variants,
     ZetaCommonLists, ZetaClientDataSet;

type
    TVacaDiasGozar = function( const iEmpleado: Integer; const dInicio, dFinal: TDate ): TPesos of object;
    TVacaFechaRegreso = function( const iEmpleado: Integer; const dInicio: TDate; const rGozo: TPesos; var rDiasRango: Double ): TDate of object;
    TGetDias  = function( const iEmpleado: Integer; const dInicio, dFinal: TDate ): TPesos of object;
    function ConflictoAccion( DataSet: TCustomClientDataSet; var FCancelaAjuste: Boolean; const sPrefijo, sCaption, sMensaje: String ) : TReconcileAction;
    function ConflictoAccionGetDias( DataSet: TCustomClientDataSet; var FCancelaAjuste: Boolean; const sPrefijo, sCaption, sMensaje: String; GetDias : TGetDias = nil) : TReconcileAction;
    function ConflictoIncapacidadAccion( var DataSet: TCustomClientDataSet; var FCancelaAjuste: Boolean; const sPrefijo, sCaption, sMensaje: String;var DatosConflictos:TZetaClientDataSet{$ifdef WF_NUBE};iAjustarNuevaWFN :integer; var sMensajeError: string {$endif} ) : TReconcileAction;
    procedure SetMontosVaca( oSaldosVaca: OleVariant; oDataSet: TClientDataSet );
    procedure RecalculaVacaDiasGozados( DataSet: TClientDataSet; const ValorDias: eValorDiasVacaciones; VacaDiasGozar: TVacaDiasGozar; const sPrefijo: String = 'VA_'; const sCampoDias: String = 'VA_GOZO' );
    procedure RecalculaVacaFechaRegreso( DataSet: TClientDataSet; const ValorDias: eValorDiasVacaciones; VacaFechaRegreso: TVacaFechaRegreso );
    procedure SetStatusPlanVacacion (DataSet: TZetaclientDataSet; const tipoAutorizacion: eStatusPlanVacacion; const lTodos: Boolean );
    procedure RecalculaDiasSaldoVacaciones(DataSet: TZetaClientDataSet; oSaldosVaca: OleVariant)  ;
    procedure SetDiasPrima( oSaldosVaca: OleVariant; oDataSet: TClientDataSet );
    procedure SetDiasSaldosVacaciones( oSaldosVaca: OleVariant; const lInfoAniversario: Boolean; var rGozo, rPago, rPrima: TPesos );
    procedure cdsPlanVacacionBeforePost(DataSet: TDataSet);

const
     K_SUB_INCAPACIDAD = 'IN_';
     K_SUB_PERMISO = 'PM_';
     K_INCIDENCIAS_FILTRO = 'TB_INCIDEN = %d';
     K_MOTIVO_PERMISOS_OFFSET = -1;
     K_AJUSTE_SALDO_REINGRESO = ' AJUSTE DE SALDO AL REINGRESO';

implementation

uses ZetaCommonTools, ZetaClientTools, ZetaDialogo,  ZetaEscogeAjustar_DevEx,ZetaRegistryCliente;//,ZetaEscogeAjustar

const
     ID_FECHA_INI = '@1';
     ID_FECHA_FIN = '@2';
     ID_INC = '@3';
     LONGITUD_FECHAS = 10;
     CAMPO_FECHA_INI = 'FEC_INI';
     CAMPO_FECHA_FIN = 'FEC_FIN';
     CAMPO_SUA_FECHA_INI = 'IN_SUA_INI';
     CAMPO_SUA_FECHA_FIN = 'IN_SUA_FIN';
     CAMPO_DIAS = 'DIAS';

function ConflictoAccion( DataSet: TCustomClientDataSet; var FCancelaAjuste: Boolean; const sPrefijo, sCaption, sMensaje: String ) : TReconcileAction;
var
   dInicio, dFinal : TDate;
   iPosFechaIni : Integer;

   procedure GetFechasConflicto;
   var
      iPosFechaFin : Integer;
   begin
        iPosFechaFin := pos( ID_FECHA_FIN, sMensaje );
        dInicio := StrAsFecha( Copy( sMensaje, iPosFechaIni + 2, LONGITUD_FECHAS ) );
        dFinal := StrAsFecha( Copy( sMensaje, iPosFechaFin + 2, LONGITUD_FECHAS ) );
   end;

   function AjustaFechas: Boolean;
   var
      dAjusteInicio, dAjusteFinal: TDate;
      iDias : Integer;
      sCampoFechaIni, sCampoFechaFin, sCampoDias: String;
   begin
        sCampoFechaIni := sPrefijo + CAMPO_FECHA_INI;
        sCampoFechaFin := sPrefijo + CAMPO_FECHA_FIN;
        sCampoDias := sPrefijo + CAMPO_DIAS;

        with DataSet do
        begin
             dAjusteInicio := ValorCampo( FieldByName( sCampoFechaIni ) );
             dAjusteFinal := ValorCampo( FieldByName( sCampoFechaFin ) );
             if ( dAjusteInicio >= dInicio ) then
                dAjusteInicio := dFinal
             else
                dAjusteFinal := dInicio;
             iDias := Trunc( dAjusteFinal - dAjusteInicio );
             Result := ( iDias > 0 );
             if Result then
             begin
                  FieldByName( sCampoFechaIni ).NewValue := dAjusteInicio;
                  FieldByName( sCampoDias ).NewValue := iDias;
                  FieldByName( sCampoFechaFin ).NewValue := dAjusteInicio + iDias;
                  { AP(01/08/2008): Se modifican al igual las fechas de SUA, si existe el campo }
                  if ( FindField( CAMPO_SUA_FECHA_INI ) <> NIL ) then
                     FieldByName( CAMPO_SUA_FECHA_INI ).NewValue := dAjusteInicio;
             end;
        end;
   end;

begin
{$ifdef INTERFAZ_TRESS}
     Result := raAbort;
     FCancelaAjuste := TRUE;
{$else}
     Result := raAbort;
     FCancelaAjuste := TRUE;
     iPosFechaIni := pos( ID_FECHA_INI, sMensaje );
     if ( iPosFechaIni > 0 ) then
     begin
          if ZetaDialogo.ZErrorConfirm( sCaption, Copy( sMensaje, 1, iPosFechaIni - 1 ) +
                                        CR_LF + CR_LF + '� Desea Ajustar Fechas ?',
                                        0, mbCancel ) then
          begin
               GetFechasConflicto;
               if AjustaFechas then
               begin
                    Result := raCorrect;
                    FCancelaAjuste := FALSE;
               end
               else
                    ZetaDialogo.ZError( sCaption, 'NO se Pudo Ajustar Fechas', 0 );
          end;
     end
     else
          ZetaDialogo.ZError( sCaption, sMensaje, 0 );
{$endif}
end;

function ConflictoAccionGetDias( DataSet: TCustomClientDataSet; var FCancelaAjuste: Boolean; const sPrefijo, sCaption, sMensaje: String; GetDias : TGetDias ) : TReconcileAction;
var
   dInicio, dFinal : TDate;
   iPosFechaIni : Integer;

   procedure GetFechasConflicto;
   var
      iPosFechaFin : Integer;
   begin
        iPosFechaFin := pos( ID_FECHA_FIN, sMensaje );
        dInicio := StrAsFecha( Copy( sMensaje, iPosFechaIni + 2, LONGITUD_FECHAS ) );
        dFinal := StrAsFecha( Copy( sMensaje, iPosFechaFin + 2, LONGITUD_FECHAS ) );
   end;

   function AjustaFechas: Boolean;
   var
      dAjusteInicio, dAjusteFinal: TDate;
      iDias : Integer;
      sCampoFechaIni, sCampoFechaFin, sCampoDias: String;
   begin
        sCampoFechaIni := sPrefijo + CAMPO_FECHA_INI;
        sCampoFechaFin := sPrefijo + CAMPO_FECHA_FIN;
        sCampoDias := sPrefijo + CAMPO_DIAS;

        with DataSet do
        begin
             dAjusteInicio := ValorCampo( FieldByName( sCampoFechaIni ) );
             dAjusteFinal := ValorCampo( FieldByName( sCampoFechaFin ) );
             if ( dAjusteInicio >= dInicio ) then
                dAjusteInicio := dFinal
             else
                dAjusteFinal := dInicio;
             iDias := Trunc( dAjusteFinal - dAjusteInicio );
             Result := ( iDias > 0 );
             if Result then
             begin
                  FieldByName( sCampoFechaIni ).NewValue := dAjusteInicio;
                  FieldByName( sCampoDias ).NewValue := iDias;
                  FieldByName( sCampoFechaFin ).NewValue := dAjusteInicio + iDias;
                  { AP(01/08/2008): Se modifican al igual las fechas de SUA, si existe el campo }
                  if ( FindField( CAMPO_SUA_FECHA_INI ) <> NIL ) then
                     FieldByName( CAMPO_SUA_FECHA_INI ).NewValue := dAjusteInicio;

                  {AV(18.12.2012) Se modifica para el caso de conflicto en fechas  }
                  if ( @GetDias <> nil ) then
                  begin
                        FieldByName( sCampoDias ).NewValue := GetDias( FieldByName('CB_CODIGO').AsInteger,  FieldByName( sCampoFechaIni ).NewValue,  FieldByName( sCampoFechaFin ).NewValue );
                  end;
             end;
        end;
   end;

begin
{$ifdef INTERFAZ_TRESS}
     Result := raAbort;
     FCancelaAjuste := TRUE;
{$else}
     Result := raAbort;
     FCancelaAjuste := TRUE;
     iPosFechaIni := pos( ID_FECHA_INI, sMensaje );
     if ( iPosFechaIni > 0 ) then
     begin
          if ZetaDialogo.ZErrorConfirm( sCaption, Copy( sMensaje, 1, iPosFechaIni - 1 ) +
                                        CR_LF + CR_LF + '� Desea Ajustar Fechas ?',
                                        0, mbCancel ) then
          begin
               GetFechasConflicto;
               if AjustaFechas then
               begin
                    Result := raCorrect;
                    FCancelaAjuste := FALSE;
               end
               else
                    ZetaDialogo.ZError( sCaption, 'NO se Pudo Ajustar Fechas', 0 );
          end;
     end
     else
          ZetaDialogo.ZError( sCaption, sMensaje, 0 );
{$endif}
end;

function ConflictoIncapacidadAccion( var DataSet: TCustomClientDataSet; var FCancelaAjuste: Boolean; const sPrefijo, sCaption, sMensaje: String;var DatosConflictos:TZetaClientDataSet{$ifdef WF_NUBE};iAjustarNuevaWFN :integer; var sMensajeError: string {$endif} ) : TReconcileAction;
{$ifdef WF_NUBE}
const
   K_AJUSTAR_NUEVA_WFN = 1;
{$endif}

var
   dInicio, dFinal : TDate;
   iPosFechaIni : Integer;
   bDialogoResult : Boolean;
   bAjustarNueva : Boolean;
   {$ifdef WF_NUBE}
   lAjustarNuevaWFN : Boolean;
   {$endif}

   procedure GetFechasConflicto;
   var
      iPosFechaFin : Integer;
   begin
        iPosFechaFin := pos( ID_FECHA_FIN, sMensaje );
        dInicio := StrAsFecha( Copy( sMensaje, iPosFechaIni + 2, LONGITUD_FECHAS ) );
        dFinal := StrAsFecha( Copy( sMensaje, iPosFechaFin + 2, LONGITUD_FECHAS ) );
   end;

   function AjustaFechas ( bAjustarNueva : Boolean): Boolean;
   var
      dAjusteInicio, dAjusteFinal, dInicioBusqueda: TDate;
      iDias : Integer;
      sCampoFechaIni, sCampoFechaFin, sCampoDias: String;
   begin
        dInicioBusqueda := dInicio;
        sCampoFechaIni := sPrefijo + CAMPO_FECHA_INI;
        sCampoFechaFin := sPrefijo + CAMPO_FECHA_FIN;
        sCampoDias := sPrefijo + CAMPO_DIAS;

        with DataSet do
        begin
             dAjusteInicio := ValorCampo( FieldByName( sCampoFechaIni ) );
             dAjusteFinal := ValorCampo( FieldByName( sCampoFechaFin ) );

             if (bAjustarNueva) then
             begin
                  if ( dAjusteInicio >= dInicio ) then
                     dAjusteInicio := dFinal
                  else
                      dAjusteFinal := dInicio;
                  iDias := Trunc( dAjusteFinal - dAjusteInicio );
             end
             else
             begin
                  if ( dAjusteInicio >= dInicio ) then
                     dFinal := dAjusteInicio
                  else
                      dInicio := dAjusteFinal;
                  iDias := Trunc( dFinal - dInicio );
             end;
             Result := ( iDias > 0 );
             if Result then
             begin
                  if (bAjustarNueva) then
                  begin
                         FieldByName( sCampoFechaIni ).NewValue := dAjusteInicio;
                         FieldByName( sCampoDias ).NewValue := iDias;
                         FieldByName( sCampoFechaFin ).NewValue := dAjusteInicio + iDias;
                         { AP(01/08/2008): Se modifican al igual las fechas de SUA, si existe el campo }
                         if ( FindField( CAMPO_SUA_FECHA_INI ) <> NIL ) then
                         begin
                            FieldByName( CAMPO_SUA_FECHA_INI ).NewValue := dAjusteInicio;
                            FieldByName( CAMPO_SUA_FECHA_FIN ).NewValue :=  dAjusteInicio + iDias;
                         end
                  end
                  else
                  begin
                       // Ajustar Anterior
                       with DatosConflictos do
                       begin
                            

                            Append;
                            FieldByName('CB_CODIGO').AsInteger := DataSet.FieldByName('CB_CODIGO').AsInteger;
                            FieldByName(sCampoFechaIni).AsDateTime := dInicio;
                            FieldByName(sCampoDias).AsInteger := iDias;
                            FieldByName(sCampoFechaFin).AsDateTime := dInicio + iDias;
                            FieldByName(CAMPO_SUA_FECHA_INI).AsDateTime := dInicio;
                            FieldByName(CAMPO_SUA_FECHA_FIN).AsDateTime := dInicio + iDias;
                            FieldByName(sCampoFechaIni+'_BUS').AsDateTime := dInicioBusqueda;
                            Post;
                       end;
                       {
                       ConflictoParams.AddDate(sCampoFechaIni, dInicio);
                       ConflictoParams.AddInteger(sCampoDias, iDias);
                       ConflictoParams.AddDate(sCampoFechaFin, dInicio + iDias);
                       ConflictoParams.AddDate(CAMPO_SUA_FECHA_INI, dInicio);
                       ConflictoParams.AddDate(CAMPO_SUA_FECHA_FIN, dInicio + iDias);
                       ConflictoParams.AddDate(sCampoFechaIni + '_BUS', dInicioBusqueda);}
                  end;
             end;
        end;
   end;

begin
{$ifdef INTERFAZ_TRESS}
     Result := raAbort;
     FCancelaAjuste := TRUE;
{$else}
     Result := raAbort;
     FCancelaAjuste := TRUE;
     iPosFechaIni := pos( ID_FECHA_INI, sMensaje );
     if ( iPosFechaIni > 0 ) then
     begin
             GetFechasConflicto;
             if (pos( ID_INC, sMensaje ) > 0) and (DataSet.FieldByName('IN_FEC_INI').AsDateTime > dInicio) then
             begin
			 {$IfNDef WF_NUBE}
                  try
                     bDialogoResult := ZetaEscogeAjustar_DevEx.InitEscogeAjustar(sCaption, Copy( sMensaje, 1, iPosFechaIni - 1 ) +
                                        CR_LF + CR_LF, 0, dInicio,
                                        ValorCampo( DataSet.FieldByName( sPrefijo + CAMPO_FECHA_INI ) ), mbCancel);
                     bAjustarNueva := ZetaEscogeAjustar_DevEx.EscogeAjustar_DevEx.rbAjustarNueva.Checked;
                  finally
                         ZetaEscogeAjustar_DevEx.EscogeAjustar_DevEx.Free;
                  end;
			   {$endif}

                  {***DevEx(by am): En esta unidad se encuentra en tools por lo tanto no esta agregado el datamodule de DCliente
                   por lo que leemos el valor asignado al registry para validar la vista del usuario. ***}

                  {$ifdef WF_NUBE}
                  lAjustarNuevaWFN := False;
                  bDialogoResult  := lAjustarNuevaWFN;

                  if( iAjustarNuevaWFN > 0   ) then
                  begin
                       if ( iAjustarNuevaWFN = K_AJUSTAR_NUEVA_WFN ) then
                       begin
                            bAjustarNueva := True;
                       end;

                       if AjustaFechas (bAjustarNueva) then
                       begin
                            Result := raCorrect;
                            FCancelaAjuste := FALSE;
                       end
                       else
                       begin
                           sMensajeError :=  'NO se Pudo Ajustar Fechas';
                       end;
                  end
                  else
                  begin
                       sMensajeError := sMensaje;
                  end;
                  {$endif}
             end
             else
             begin
                  {$ifdef WF_NUBE}
                  lAjustarNuevaWFN := False;
                  bDialogoResult  := lAjustarNuevaWFN;

                  if( iAjustarNuevaWFN > 0   ) then
                  begin
                       if ( iAjustarNuevaWFN = K_AJUSTAR_NUEVA_WFN ) then
                       begin
                            bAjustarNueva := True;
                       end;

                       if AjustaFechas (bAjustarNueva) then
                       begin
                            Result := raCorrect;
                            FCancelaAjuste := FALSE;
                       end
                       else
                       begin
                           sMensajeError :=  'NO se Pudo Ajustar Fechas';
                       end;
                  end
                  else
                  begin
                       sMensajeError := sMensaje;
                  end;
                  {$else}
                  bDialogoResult := ZetaDialogo.ZErrorConfirm( sCaption, Copy( sMensaje, 1, iPosFechaIni - 1 ) +
                                        CR_LF + CR_LF + '� Desea Ajustar Fechas ?',
                                        0, mbCancel );
                  bAjustarNueva := TRUE;
                  {$endif}
             end;
             {$IfNDef WF_NUBE}
             if bDialogoResult then
             begin
                  if AjustaFechas (bAjustarNueva) then
                  begin
                       Result := raCorrect;
                       FCancelaAjuste := FALSE;
                  end
                  else
                      ZetaDialogo.ZError( sCaption, 'NO se Pudo Ajustar Fechas', 0 );
             end;
             {$endif}
     end
     else
         {$ifdef WF_NUBE}
         sMensajeError := sMensaje;
         {$else}
         ZetaDialogo.ZError( sCaption, sMensaje, 0 );
         {$endif}

	{$IfNDef WF_NUBE}
          ZetaDialogo.ZError( sCaption, sMensaje, 0 );
	{$endif}

{$endif}
end;

{$ifdef false}
procedure SetMontosVaca( oSaldosVaca: OleVariant; oDataSet: TClientDataSet );
var
   nMonto, nSeptimoDia, nPrima : TPesos;
begin
     with oDataSet do
     begin
         { S�lo se usa en ALTA y en EDICION cuando cambia VA_PAGO }
         nMonto := Redondea( FieldByName( 'VA_PAGO' ).AsFloat * FieldByName( 'CB_SALARIO' ).AsFloat );
         if ( oSaldosVaca[ K_VACA_SEPTIMO_DIA ] ) then    { Se paga el 7o. Dia? }
            nSeptimoDia := ( nMonto / 6 )
         else
             nSeptimoDia := 0;
         if ( oSaldosVaca[ K_VACA_SEPTIMO_PRIMA ] ) then    { Se paga la prima sobre 7o. Dia? }
            nPrima := ( ( nMonto + nSeptimoDia ) * FieldByName( 'VA_TASA_PR' ).AsFloat / 100 )
         else
             nPrima := ( nMonto * FieldByName( 'VA_TASA_PR' ).AsFloat / 100 );

         FieldByName( 'VA_MONTO' ).AsFloat := nMonto;
         FieldByName( 'VA_SEVEN' ).AsFloat := nSeptimoDia;
         FieldByName( 'VA_PRIMA' ).AsFloat := nPrima;
         FieldByName( 'VA_TOTAL' ).AsFloat := ( nMonto + nSeptimoDia + nPrima + FieldByName( 'VA_OTROS' ).AsFloat );
     end;
end;
{$endif}

procedure SetMontosVaca( oSaldosVaca: OleVariant; oDataSet: TClientDataSet );
var
   nMonto, nSeptimoDia, nPrima : TPesos;
begin
     with oDataSet do
     begin
         { S�lo se usa en ALTA y en EDICION cuando cambia VA_PAGO }
         nMonto := Redondea( FieldByName( 'VA_PAGO' ).AsFloat * FieldByName( 'CB_SALARIO' ).AsFloat );
         if ( oSaldosVaca[ K_VACA_SEPTIMO_DIA ] ) then    { Se paga el 7o. Dia? }
            nSeptimoDia := ( nMonto / 6 )
         else
             nSeptimoDia := 0;
         FieldByName( 'VA_MONTO' ).AsFloat := nMonto;
         FieldByName( 'VA_SEVEN' ).AsFloat := nSeptimoDia;
         nPrima := FieldByName( 'VA_P_PRIMA' ).AsFloat * FieldByName( 'CB_SALARIO' ).AsFloat  ;
         if  ( oSaldosVaca[ K_VACA_SEPTIMO_PRIMA ] ) then
             nPrima := ( nPrima * ( 7 / 6 ) );    // Agrega el 7o. d�a de lo que se paga de prima
         nPrima := Redondea ( nPrima );
         FieldByName( 'VA_PRIMA' ).AsFloat := nPrima;
         FieldByName( 'VA_TOTAL' ).AsFloat := ( nMonto + nSeptimoDia + nPrima + FieldByName( 'VA_OTROS' ).AsFloat );
     end;
end;

procedure SetDiasPrima( oSaldosVaca: OleVariant; oDataSet: TClientDataSet );
begin
     with oDataSet do
     begin
          FieldByName( 'VA_P_PRIMA' ).AsFloat :=  FieldByName( 'VA_PAGO' ).AsFloat * ( FieldByName( 'VA_TASA_PR' ).AsFloat / 100 )
     end;
end;

procedure RecalculaVacaDiasGozados( DataSet: TClientDataSet; const ValorDias: eValorDiasVacaciones;
          VacaDiasGozar: TVacaDiasGozar; const sPrefijo: String = 'VA_'; const sCampoDias: String = 'VA_GOZO' );
var
   dInicio, dFinal: TDate;
   rGozo: TPesos;
begin
     rGozo := 0;                // Default es cero
     with DataSet do
     begin
          dInicio := FieldByName( sPrefijo + 'FEC_INI' ).AsDateTime;
          dFinal  := FieldByName( sPrefijo + 'FEC_FIN' ).AsDateTime;
          if ( dFinal > dInicio ) then
          begin
               if ( ValorDias = dvNoUsar ) then
                  rGozo := ( dFinal - dInicio )
               else
                   rGozo := VacaDiasGozar( FieldByName( 'CB_CODIGO' ).AsInteger, dInicio, dFinal );
          end;
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
          FieldByName( sCampoDias ).AsFloat := rGozo;
     end;
end;

procedure RecalculaVacaFechaRegreso( DataSet: TClientDataSet; const ValorDias: eValorDiasVacaciones; VacaFechaRegreso: TVacaFechaRegreso );
var
   dInicio, dFinal: TDate;
   rGozo: TPesos;
   rDiasRango: Double;
begin
     with DataSet do
     begin
          dInicio := FieldByName( 'VA_FEC_INI' ).AsDateTime;
          rGozo   := FieldByName( 'VA_GOZO' ).AsFloat;
          dFinal  := dInicio;                // Default es la Fecha Inicial
          if ( rGozo > 0 ) then
          begin
               if ( ValorDias = dvNoUsar ) then
               begin
                    rDiasRango := Trunc( rGozo );            // D�as Calendario: Dias Gozo es igual al rango en D�as
                    dFinal := ( dInicio + rDiasRango );
               end
               else
                  dFinal := VacaFechaRegreso( FieldByName( 'CB_CODIGO' ).AsInteger, dInicio, rGozo, rDiasRango );
          end;
          if ( dFinal <> FieldByName( 'VA_FEC_FIN' ).AsDateTime ) then
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'VA_FEC_FIN' ).AsDateTime := dFinal;
          end;
          //if ( rGozo <> rDiasRango ) then
          {$ifndef INTERFAZ_TRESS}
          {$ifndef INTERFAZ_GREATBATCH}
          if ( not PesosIguales( rGozo, rDiasRango ) ) then
             zWarning( 'Rec�lculo de Fecha de Regreso', '� El C�lculo De La Fecha De Regreso No Es Exacto Contra Los D�as Gozados ! ' + CR_LF + CR_LF +
                                                        Format( 'Se Ajust� La Fecha De Regreso Hacia Abajo, Que Corresponde a %3.2f D�as Gozados.', [ rDiasRango ] ) + CR_LF +
                                                        'Verifique Los Valores Del Per�odo De Vacaciones' + CR_LF, ZetaCommonClasses.Configurando_el_valor_de_Cada_Dia_de_Vacaciones, mbOK );
          {$endif}
		  {$endif}
     end;
end;

procedure RecalculaDiasSaldoVacaciones(DataSet: TZetaClientDataSet; oSaldosVaca: OleVariant)  ;
begin
     with DataSet do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
          FieldByName( 'VP_SAL_ANT' ).AsFloat := ( oSaldosVaca[ K_VACA_GOZO_ACTUAL ] - oSaldosVaca[ K_VACA_PAGO_PROP ] );
          FieldByName( 'VP_SAL_PRO' ).AsFloat := oSaldosVaca[ K_VACA_PAGO_PROP ];
     end;
end;

procedure SetStatusPlanVacacion (DataSet: TZetaClientDataSet; const tipoAutorizacion: eStatusPlanVacacion; const lTodos: Boolean );
var
   oStatusAutorizacion : eStatusPlanVacacion;
   Pos: TBookMark;
begin
     with DataSet do
     begin
          if ( not IsEmpty ) then
          begin
               DisableControls;
               Pos := GetBookMark;
               try
                  PostData;
                  if lTodos then
                     First;
                  while ( not EOF ) do
                  begin
                       oStatusAutorizacion :=  eStatusPlanVacacion( FieldByName( 'VP_STATUS' ).AsInteger );
                       if   ( oStatusAutorizacion  <> tipoAutorizacion) and
                            ( not (oStatusAutorizacion in [spvProcesada {$ifdef SUPERVISORES}, spvAutorizada, spvRechazada{$endif}] ) )   then
                       begin
                            Edit;
                            FieldByName( 'VP_STATUS' ).AsInteger :=  Ord ( tipoAutorizacion );
                            Post;
                       End;
                       Next;
                       if ( not lTodos ) then
                          Exit;    // Solo procesa un registro
                  end;
               finally
                      if ( Pos <> nil ) then
                      begin
                           if lTodos then
                              GotoBookMark( Pos );
                           FreeBookMark( Pos );
                      end;
                      EnableControls;
               end;
          end;
     end;
end;

procedure SetDiasSaldosVacaciones( oSaldosVaca: OleVariant; const lInfoAniversario: Boolean;
          var rGozo, rPago, rPrima: TPesos );
begin
     if not VarIsNull( oSaldosVaca ) then
     begin
          if lInfoAniversario then
          begin
               rGozo  := oSaldosVaca[ K_VACA_GOZO_ACTUAL ] - oSaldosVaca[ K_VACA_PAGO_PROP ];
               rPago  := oSaldosVaca[ K_VACA_PAGO_ACTUAL ] - oSaldosVaca[ K_VACA_PAGO_PROP ];
               rPrima := oSaldosVaca[ K_VACA_DPRIMA_ACTUAL ] - oSaldosVaca[ K_VACA_DPRIMA_PROP ];
          end
          else
          begin
               rGozo  := oSaldosVaca[ K_VACA_GOZO_ACTUAL ];
               rPago  := oSaldosVaca[ K_VACA_PAGO_ACTUAL ];
               rPrima := oSaldosVaca[ K_VACA_DPRIMA_ACTUAL ];
          end;
     end;
end;

procedure cdsPlanVacacionBeforePost(DataSet: TDataSet);
begin
     with Dataset do
     begin
          if ( FieldByName( 'CB_CODIGO' ).AsInteger  <= 0 ) then
             DataBaseError( 'El empleado no puede quedar vac�o' );
          if ( FieldByName( 'VP_DIAS' ).AsInteger  <= 0 ) then
             DataBaseError( 'Los dias de vacaciones tienen que ser mayores a cero' );
          if ( FieldByName( 'VP_FEC_INI' ).AsDateTime  > FieldByName( 'VP_FEC_FIN' ).AsDateTime  ) then
             DataBaseError( 'La fecha de inicio no puede ser mayor a la de regreso' );

          if( FieldByName('VP_STATUS').AsInteger <> Ord( spvAutorizada ) )then
          begin
                FieldByName( 'VP_PAGO_US' ).AsString := K_GLOBAL_NO;
          end;
          if ( zStrToBool( FieldByName( 'VP_PAGO_US' ).AsString ) )then
          begin
               if( FieldByName( 'VP_NOMYEAR' ).AsInteger <= 0  )then
               begin
                    DataBaseError( 'El a�o del per�odo de pago debe ser mayor que cero' )
               end;
               if ( FieldByName( 'VP_NOMNUME' ).AsInteger <= 0 )then
               begin
                    DataBaseError( 'El n�mero del per�odo de pago debe ser mayor que cero' )
               end;
               if ( FieldByName( 'VP_NOMTIPO' ).AsInteger <= 0 )then
               begin
                    DataBaseError( 'El tipo del per�odo de pago no esta definido' )
               end;
          end
          else
          begin
               FieldByName( 'VP_NOMYEAR' ).AsInteger := 0;
               FieldByName( 'VP_NOMNUME' ).AsInteger := 0;
               FieldByName( 'VP_NOMTIPO' ).AsInteger := 0;
          end;
     end;
end;


end.
