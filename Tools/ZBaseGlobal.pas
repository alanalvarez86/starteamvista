unit ZBaseGlobal;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, DB, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, FileCtrl,
     ZAccesosMgr,
     ZetaCommonClasses,
     ZBaseDlgModal;

type
  TBaseGlobal = class(TZetaDlgModal)
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FIndexDerechos: Integer;
    FLastAction: Integer;
    FGlobales: TZetaParams;
    FActualizaDiccion: Boolean;
  protected
    { Protected declarations }
    function BuscarDirectorio( const Value: String ): String;
    function CheckDerechos( const iDerecho: Integer ): Boolean;
    function PuedeModificar( var sMensaje: String ): Boolean; dynamic;
    function PuedeImprimir( var sMensaje: String ): Boolean; dynamic;
  public
    { Public declarations }
    property ActualizaDiccion: Boolean read FActualizaDiccion write FActualizaDiccion;
    property IndexDerechos: Integer read FIndexDerechos write FIndexDerechos;
    property LastAction: Integer read FLastAction write FLastAction;
    procedure Cargar; virtual;
    procedure Descargar; virtual;
  end;
  TBaseGlobalClass = class of TBaseGlobal;

var
  BaseGlobal: TBaseGlobal;

implementation

uses DGlobal,
     ZetaCommonTools,
     ZetaDialogo;

{$R *.DFM}

{ *********** TBaseGlobal *********** }

procedure TBaseGlobal.FormCreate(Sender: TObject);
begin
     FIndexDerechos := 0;
     inherited;
     FGlobales := TZetaParams.Create;
     FActualizaDiccion := FALSE;
end;

procedure TBaseGlobal.FormShow(Sender: TObject);
var
   sMsg: String;
   lCanEdit: Boolean;
begin
     inherited;
     lCanEdit := PuedeModificar( sMsg );
     with OK do
     begin
          Enabled := lCanEdit;
          Visible := lCanEdit;
     end;
     with Cancelar do
     begin
          if lCanEdit then
          begin
               Kind := bkCancel;
               Caption := '&Cancelar';
               Hint := 'Cancelar Cambios';
          end
          else
          begin
               Kind := bkClose;
               Caption := '&Salir';
               Hint := 'Cerrar Pantalla y Salir';
          end;
     end;
     Cargar;
end;

procedure TBaseGlobal.FormDestroy(Sender: TObject);
begin
     inherited;
     FGlobales.Free;
end;

procedure TBaseGlobal.Cargar;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           Global.CargaControles( FGlobales, Self );
        except
              on Error: Exception do
              begin
                   zExcepcion( Caption, 'Error Al Leer Valores Globales', Error, 0 );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBaseGlobal.Descargar;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           Global.DescargaControles( FGlobales, Self, ActualizaDiccion );
           FLastAction := K_EDICION_MODIFICACION;
        except
              on Error: Exception do
              begin
                   zExcepcion( Caption, 'Error Al Escribir Valores Globales', Error, 0 );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TBaseGlobal.BuscarDirectorio( const Value: String ): String;
var
   sDirectory: String;
begin
     sDirectory := Value;
     if SelectDirectory( sDirectory, [ sdAllowCreate, sdPerformCreate, sdPrompt ], 0 ) then
        Result := sDirectory
     else
         Result := Value;
end;

function TBaseGlobal.CheckDerechos( const iDerecho: Integer ): Boolean;
begin
     Result := ZAccesosMgr.CheckDerecho( IndexDerechos, iDerecho );
end;

function TBaseGlobal.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result := CheckDerechos( K_DERECHO_CAMBIO );
     if not Result then
        sMensaje := 'No Tiene Permiso Para Modificar Registros';
end;

function TBaseGlobal.PuedeImprimir( var sMensaje: String ): Boolean;
begin
     Result := CheckDerechos( K_DERECHO_IMPRESION );
     if not Result then
        sMensaje := 'No Tiene Permiso Para Imprimir Registros';
end;

procedure TBaseGlobal.OKClick(Sender: TObject);
begin
     inherited;
     Descargar;
     if ( FLastAction = K_EDICION_MODIFICACION ) then
        self.ModalResult := mrOk;
end;

procedure TBaseGlobal.CancelarClick(Sender: TObject);
begin
     inherited;
     FLastAction := K_EDICION_CANCELAR;
end;

end.
