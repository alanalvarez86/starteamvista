unit ZCerrarEdicion;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls;

type
  TCerrarEdicion = class(TForm)
    Mensaje: TLabel;
    Image: TImage;
    Guardar: TBitBtn;
    SalirSinGuardar: TBitBtn;
    NoSalir: TBitBtn;
    Pregunta: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CerrarEdicion: TCerrarEdicion;

function CierraEdicion: TModalResult;

implementation

{$R *.DFM}

uses ZetaCommonClasses;

function CierraEdicion: TModalResult;
begin
     if ( CerrarEdicion = nil ) then
     begin
          CerrarEdicion := TCerrarEdicion.Create( Application ); // Se destruye al Salir del Programa //
     end;
     with CerrarEdicion do
     begin
          ShowModal;
          Result := ModalResult;
     end;
end;

{ ***** TCerrarEdicion ******** }

procedure TCerrarEdicion.FormCreate(Sender: TObject);
begin
     HelpContext := H95001_Confirmacion;
end;

procedure TCerrarEdicion.FormShow(Sender: TObject);
begin
     Guardar.SetFocus;
end;

end.
