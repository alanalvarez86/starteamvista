unit FServidorDesconectado;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ZetaDBTextBox, ZetaCommonClasses, ZetaCommonLists,
  Mask,
  ZetaMessages,
  ZetaNumero, dxSkinsCore, TressMorado2013, cxClasses, cxLookAndFeels,
  dxSkinsForm, cxGraphics, cxLookAndFeelPainters, Vcl.Menus, cxButtons, cxStyles,
  Vcl.ImgList, dxGDIPlusClasses;

type
  eOperacionDesconectado = ( osReintentar, osConfigurar, osIgnorar, osSalir );

  TServidorDesconectado = class(TForm)
    Mensaje: TLabel;
    Image1: TImage;
    ServidorLb: TLabel;
    PuertoLb: TLabel;
    Label5: TLabel;
    PuertoLbl: TLabel;
    ServidorLbl: TLabel;
    ReintentarBtn_DevEx: TcxButton;
    ConfigurarBtn_DevEx: TcxButton;
    IgnorarBtn_DevEx: TcxButton;
    SalirBtn_DevEx: TcxButton;
    cxImageList24_PanelBotones: TcxImageList;

    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    {$ifdef ACCESOS} FCanWrite : Boolean; {$endif}

    procedure KillWindow( var Message: TMessage ); message WM_KILLWINDOW;

  public
    { Public declarations }
  end;

    function ShowDlgDesconectado(  {$ifdef ACCESOS} lCanWrite : boolean; {$endif} sServidor: String; iPuerto: Integer ): eOperacionDesconectado;

var
  ServidorDesconectado: TServidorDesconectado;

implementation

uses ZetaHelpCafe,
{$ifdef ACCESOS}
     ZetaRegistryTerminal;
{$else}
     ZetaRegistryCafe;
{$endif}

{$R *.DFM}

procedure TServidorDesconectado.FormCreate(Sender: TObject);
begin
     HelpContext:= H00011_Servidor_fuera_de_linea;
end;

function ShowDlgDesconectado( {$ifdef ACCESOS} lCanWrite : boolean; {$endif} sServidor: String; iPuerto: Integer ): eOperacionDesconectado;
begin

     if ( ServidorDesconectado = nil ) then
     begin
          ServidorDesconectado := TServidorDesconectado.Create( Application ); // Se destruye al Salir del Programa //
     end;

     with ServidorDesconectado do
     begin
          {$ifdef ACCESOS}
          FCanWrite := lCanWrite;
          {$endif}
          ServidorLb.Caption:= sServidor;
          PuertoLb.Caption:= IntToStr(iPuerto);
          ShowModal;
          case ModalResult of
               mrRetry:  Result:= osReintentar;
               mrOk:     Result:= osConfigurar;
               mrIgnore: Result:= osIgnorar;
          else Result:= osSalir;
          end;
     end;
end;

procedure TServidorDesconectado.FormShow(Sender: TObject);
begin
{$ifdef ACCESOS}
     // ConfigurarBtn.Enabled := FCanWrite;
     ConfigurarBtn_DevEx.Enabled := FCanWrite;
{$else}
     ConfigurarBtn_DevEx.Enabled := CafeRegistry.CanWrite;
{$endif}
     IgnorarBtn_DevEx.SetFocus;
end;

procedure TServidorDesconectado.KillWindow(var Message: TMessage);
begin
     if ( Message.Msg = WM_KILLWINDOW ) then
          ServidorDesconectado.ModalResult := mrIgnore;
end;

end.
