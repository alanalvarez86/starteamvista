unit ZBaseDlgModal;

interface

uses
  Windows, Messages, Forms, StdCtrls, Buttons, Classes, Controls, ExtCtrls;

type
  TZetaDlgModal = class(TForm)
    PanelBotones: TPanel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure KeyPress(var Key: Char); override; { TWinControl }
  public
    { Public declarations }
  end;

  TZetaDlgModalClass = class of TZetaDlgModal;

var
  ZetaDlgModal: TZetaDlgModal;

procedure ShowDlgModal(var Dialogo; DialogClass: TZetaDlgModalClass);

implementation

uses
  ZetaSmartLists;

{$R *.DFM}

procedure ShowDlgModal(var Dialogo; DialogClass: TZetaDlgModalClass);
begin
  if (TZetaDlgModal(Dialogo) = nil) then
    TZetaDlgModal(Dialogo) := DialogClass.Create(Application) as TZetaDlgModal;
  with TZetaDlgModal(Dialogo) do begin
    ShowModal;
  end;
end;

{ *********** TZetaDlgModal ************ }

procedure TZetaDlgModal.KeyPress(var Key: Char);
begin
  inherited KeyPress(Key);
  case Key of
    Chr(VK_RETURN): begin
        if (not(ActiveControl is TButton)) and (not(ActiveControl is TZetaSmartListBox)) and
          (not(ActiveControl is TCustomMemo)) then begin
          Key := #0;
          Perform(WM_NEXTDLGCTL, (Hi(GetKeyState(VK_SHIFT)) and 1), 0);
        end;
      end;
  end;
end;

end.
