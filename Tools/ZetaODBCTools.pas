unit ZetaODBCTools;

interface

uses Windows, SysUtils, Forms, Consts, Registry, ShellAPI,
     WinSvc, Classes;

const
     MSSQL_DRIVER = 'SQL Server';

function GetODBCTraceFile: String;
function GetIntersolvPooling: Integer;
function SetIntersolvPooling( const iTimeOut: Integer; var sError: String ): Boolean;
function SetIntersolvDataSource( const sName, sDescription, sDatabase: String; var sError: String ): Boolean;
function SetMSSQLPooling( const iTimeOut: Integer; var sError: String ): Boolean;
function SetSQLServerDataSource( const sDataSourceName, sDescription, sServer, sDatabase: String; var sError: String ): Boolean;
function SetRegistryStringValue( const sKey, sName, sValue: String ): Boolean;
procedure InvokeODBCAdministrator( hWndParent: HWND );
procedure SetSQLServerAttributes( Atributos: TStrings; const sServer, sDatabase: String );

implementation

uses ZetaWinAPITools;

const
     MAXNULLSTRING = 255;
     INTERSOLV_DRIVER = 'INTERSOLV InterBase ODBC Driver (*.gdb)';
     KEY_ODBC_INI = '\Software\ODBC\ODBC.INI\';
     KEY_ODBC_INST = '\Software\ODBC\ODBCINST.INI\';
     ODBC_INSTALL_DRIVER = 1;
     ODBC_REMOVE_DRIVER	= 2;
     ODBC_CONFIG_DRIVER	= 3;
     ODBC_CONFIG_DRIVER_MAX = 100;
     ODBC_ADD_DSN = 1;         { Add data source }
     ODBC_CONFIG_DSN = 2;      { Configure (edit) data source }
     ODBC_REMOVE_DSN = 3;      { Remove data source }
     ODBC_ADD_SYS_DSN = 4;     { add a system DSN }
     ODBC_CONFIG_SYS_DSN = 5;  { Configure a system DSN }
     ODBC_REMOVE_SYS_DSN = 6;  { remove a system DSN }
     SQL_SUCCESS = 0;
     SQL_SUCCESS_WITH_INFO = 1;
     SQL_NO_DATA = 100;
     SQL_ERROR = -1;
     SQL_INVALID_HANDLE = -2;
     SQL_STILL_EXECUTING = 2;
     SQL_NEED_DATA = 99;
     SQL_MAX_MESSAGE_LENGTH = 512;
type
    NullString = array[ 0..MAXNULLSTRING ] of Char;

function _SQLConfigDataSource( hWndParent: HWND; FRequest: Word; pDriver, pAttributes: NullString ): Bool; stdcall; far; external 'odbccp32.dll' name 'SQLConfigDataSource';
function _SQLManageDataSources( hWndParent: HWND ): Bool; stdcall; far; external 'odbccp32.dll' name 'SQLManageDataSources';
function _SQLValidDSN( pName: pChar ): Bool; stdcall; far; external 'odbccp32.dll' name 'SQLValidDSN';
function _SQLConfigDriver( hWndParent: HWND; FRequest: Word; pDriver, pAttributes, pMsg: NullString; iMsgMax: Word; var iMsgOut: Word ): Bool; stdcall; far; external 'odbccp32.dll' name 'SQLConfigDriver';
function _SQLInstallerError( iError: Word; var iErrorCode: DWord; pErrorMsg: NullString; iErrorMsgMax: Word; var iErrorMsg: Word ): SmallInt; stdcall; far; external 'odbccp32.dll' name 'SQLInstallerError';

procedure StringToNullString( const sValue: String; var sTarget: NullString );
var
   i: Integer;
begin
     i := 0;
     while ( i <= MAXNULLSTRING ) do
     begin
          if ( ( i + 1 ) <= Length( sValue ) ) then
             sTarget[ i ] := sValue[ i + 1 ]
          else
              sTarget[ i ] := Chr( 0 );
          Inc( i );
     end;
end;
function SetRegistryStringValue( const sKey, sName, sValue: String ): Boolean;
begin
     with TRegistry.Create do
     begin
          try
             RootKey := HKEY_LOCAL_MACHINE;
             if OpenKey( sKey, False ) then
             begin
                  WriteString( sName, sValue );
                  Result := True;
             end
             else
             begin
                  Result := False;
             end;
          finally
                 Free;
          end;
     end;
end;
function GetSQLInstallerError: String;
var
   iError, iErrorMsg: Word;
   iErrorCode: DWord;
   pErrorMsg: NullString;
begin
     iError := 1;
     case _SQLInstallerError( iError, iErrorCode, pErrorMsg, SizeOf( pErrorMsg ), iErrorMsg ) of
          SQL_ERROR: Result := Format( 'Error SQL %d', [ iErrorCode ] );
          SQL_INVALID_HANDLE: Result := Format( 'Invalid Handle %d', [ iErrorCode ] );
          SQL_STILL_EXECUTING: Result := Format( 'Still Executing %d', [ iErrorCode ] );
          SQL_NEED_DATA: Result := Format( 'Need Data %d', [ iErrorCode ] );
          SQL_NO_DATA: Result := Format( 'No Data %d', [ iErrorCode ] );
     else
         Result := Format( 'Error %d: %s', [ iErrorCode, StrPas( pErrorMsg ) ] );
     end;
end;

function SetConnectionPooling( const iTimeOut: Integer; const sDriver: String; var sError: String ): Boolean;
var
   pDriver, pAttributes, pMsg: NullString;
   iMsgOut: Word;
begin
     StringToNullString( Format( 'CPTimeout=%d', [ iTimeOut ] ), pAttributes );
     StringToNullString( sDriver, pDriver );
     if _SQLConfigDriver( 0, ODBC_CONFIG_DRIVER, pDriver, pAttributes, pMsg, SizeOf( pMsg ), iMsgOut ) then
        Result := True
     else
     begin
          Result := False;
          sError := 'Configuración de Connection Pooling: ' + GetSQLInstallerError;
     end;
end;

function SetIntersolvPooling( const iTimeOut: Integer; var sError: String ): Boolean;
begin
     Result := SetConnectionPooling( iTimeOut, INTERSOLV_DRIVER, sError );
end;

function GetIntersolvPooling: Integer;
var
   sValue: String;
begin
     if ZetaWinAPITools.GetRegistryStringValue( KEY_ODBC_INST + INTERSOLV_DRIVER, 'CPTimeout', sValue ) then
        Result := StrToIntDef( sValue, 0 )
     else
         Result := 0;
end;

function SetMSSQLPooling( const iTimeOut: Integer; var sError: String ): Boolean;
begin
     Result := SetConnectionPooling( iTimeOut, MSSQL_DRIVER, sError );
end;

function GetODBCTraceFile: String;
var
   sValue: String;
begin
     if ZetaWinAPITools.GetRegistryStringValue( KEY_ODBC_INI + 'ODBC', 'TraceFile', sValue ) then
     begin
          Result := sValue;
          if ( ExtractFileDrive( Result ) = '' ) then
             Result := Format( '%s%s', [ ExtractFileDrive( ZetaWinAPITools.GetWinSysDir ), Result ] );
     end
     else
         Result := '';
end;

function SetIntersolvDataSource( const sName, sDescription, sDatabase: String; var sError: String ): Boolean;
const
     WORKAROUNDS = 'WorkArounds';
     ATRIBUTOS = 'DSN=%s' + #0 +
                 'Driver={%s}' + #0 +
                 'Database=%s' + #0 +
                 'Description=%s' + #0 +
                 'ApplicationUsingThreads=1' + #0 +
                 'LockTimeOut=0' + #0;
var
   sDataSourceName: String;
   pDriver, pAttributes: NullString;
begin
     Result := False;
     sDataSourceName := sName + 'ODBC';
     if _SQLValidDSN( pChar( sDataSourceName ) ) then
     begin
          StringToNullString( Format( ATRIBUTOS, [ sDataSourceName, INTERSOLV_DRIVER, sDatabase, sDescription ] ), pAttributes );
          StringToNullString( INTERSOLV_DRIVER, pDriver );
          if _SQLConfigDataSource( 0, ODBC_ADD_SYS_DSN, pDriver, pAttributes ) then
          begin
               { Es necesario hacer esto manualmente, ya que el incluir }
               { WorkArounds en pAttributes no da resultado }
               if SetRegistryStringValue( KEY_ODBC_INI + sDataSourceName, WORKAROUNDS, '1' ) then
                  Result := SetIntersolvPooling( 650, sError )
               else
                   sError := 'Error Al Escribir WorkArounds';
          end
          else
              sError := 'Configuración de ODBC DSN: ' + GetSQLInstallerError;
     end
     else
         sError := 'Nombre Inválido';
end;

procedure SetSQLServerAttributes( Atributos: TStrings; const sServer, sDatabase: String );
begin
     with Atributos do
     begin
          Clear;
          Add( Format( 'SERVER=%s', [ sServer ] ) );
          Add( Format( 'DATABASE=%s', [ sDatabase ] ) );
          Add( Format( 'Language=%s', [ 'us_english' ] ) );
     end;
end;

function SetSQLServerDataSource( const sDataSourceName, sDescription, sServer, sDatabase: String; var sError: String ): Boolean;
{ El atributo Language sirve para que las fechas se formateen bien }
{ EXEC SP_CONFIG 'DefaultLanguage', 0: Fija el default para todos los usuarios }
{ EXEC SP_DEFAULTLANGUAGE 'sa', 'us_english': Fija el default para el usuario 'sa' }
const
     ATRIBUTOS = 'DSN={%s}' + #0 +
                 'Driver={%s}' + #0 +
                 'Server={%s}' + #0 +
                 'Database={%s}' + #0 +
                 'Language=us_english' + #0 +
                 'Description=%s' + #0;

var
   pDriver, pAttributes: NullString;
begin
     Result := False;
     if _SQLValidDSN( pChar( sDataSourceName ) ) then
     begin
          StringToNullString( Format( ATRIBUTOS, [ sDataSourceName, MSSQL_DRIVER, sServer, sDatabase, sDescription ] ), pAttributes );
          StringToNullString( MSSQL_DRIVER, pDriver );
          if _SQLConfigDataSource( 0, ODBC_ADD_SYS_DSN, pDriver, pAttributes ) then
             Result := True
          else
              sError := 'Configuración de ODBC DSN: ' + GetSQLInstallerError;
     end
     else
         sError := 'Nombre Inválido';
end;

procedure InvokeODBCAdministrator( hWndParent: HWND );
begin
     _SQLManageDataSources( hWndParent );
end;

end.
