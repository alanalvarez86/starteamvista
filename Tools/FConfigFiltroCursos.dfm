inherited ConfigFiltroCursos: TConfigFiltroCursos
  Caption = 'Configurar filtros de vista de Cursos Programados'
  ClientHeight = 300
  ClientWidth = 414
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  ExplicitWidth = 420
  ExplicitHeight = 329
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 264
    Width = 414
    ExplicitTop = 264
    ExplicitWidth = 414
    inherited OK_DevEx: TcxButton
      Left = 247
      ModalResult = 0
      OnClick = OK_DevExClick
      ExplicitLeft = 247
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 327
      OnClick = Cancelar_DevExClick
      ExplicitLeft = 327
    end
  end
  object Advertencia: TMemo [1]
    Left = 0
    Top = 0
    Width = 414
    Height = 73
    Align = alTop
    Alignment = taCenter
    Color = clInfoBk
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Lines.Strings = (
      'Seleccionar hasta 4 tablas y/o cat'#225'logos.'
      ''
      'Se pondr'#225'n disponibles en el plan de cursos programados para '
      
        'aplicar filtros en la informaci'#243'n que proviene de la base de dat' +
        'os.'
      '')
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
  end
  object PanelOpciones: TPanel [2]
    Left = 0
    Top = 73
    Width = 414
    Height = 191
    Align = alClient
    TabOrder = 2
    object CBPuestos: TCheckBox
      Tag = 1
      Left = 19
      Top = 15
      Width = 162
      Height = 17
      Caption = 'Puestos'
      TabOrder = 0
      OnClick = CBOpcionesClick
    end
    object CBTurnos: TCheckBox
      Tag = 2
      Left = 19
      Top = 38
      Width = 162
      Height = 17
      Caption = 'Turnos'
      TabOrder = 1
      OnClick = CBOpcionesClick
    end
    object CBClaseCursos: TCheckBox
      Tag = 4
      Left = 19
      Top = 85
      Width = 162
      Height = 17
      Caption = 'Clase de Cursos'
      TabOrder = 3
      OnClick = CBOpcionesClick
    end
    object CBTipoCursos: TCheckBox
      Tag = 3
      Left = 19
      Top = 61
      Width = 162
      Height = 17
      Caption = 'Tipo de Cursos'
      TabOrder = 2
      OnClick = CBOpcionesClick
    end
    object CBCursos: TCheckBox
      Tag = 5
      Left = 19
      Top = 108
      Width = 162
      Height = 17
      Caption = 'Cursos'
      TabOrder = 4
      OnClick = CBOpcionesClick
    end
    object CBNivel1: TCheckBox
      Tag = 6
      Left = 19
      Top = 131
      Width = 162
      Height = 17
      Caption = 'Nivel1'
      TabOrder = 5
      OnClick = CBOpcionesClick
    end
    object CBNivel2: TCheckBox
      Tag = 7
      Left = 19
      Top = 154
      Width = 162
      Height = 17
      Caption = 'Nivel2'
      TabOrder = 6
      OnClick = CBOpcionesClick
    end
    object CBNivel3: TCheckBox
      Tag = 8
      Left = 203
      Top = 15
      Width = 162
      Height = 17
      Caption = 'Nivel3'
      TabOrder = 7
      OnClick = CBOpcionesClick
    end
    object CBNivel4: TCheckBox
      Tag = 9
      Left = 203
      Top = 38
      Width = 162
      Height = 17
      Caption = 'Nivel4'
      TabOrder = 8
      OnClick = CBOpcionesClick
    end
    object CBNivel5: TCheckBox
      Tag = 10
      Left = 203
      Top = 61
      Width = 162
      Height = 17
      Caption = 'Nivel5'
      TabOrder = 9
      OnClick = CBOpcionesClick
    end
    object CBNivel6: TCheckBox
      Tag = 11
      Left = 203
      Top = 85
      Width = 162
      Height = 17
      Caption = 'Nivel6'
      TabOrder = 10
      OnClick = CBOpcionesClick
    end
    object CBNivel7: TCheckBox
      Tag = 12
      Left = 203
      Top = 108
      Width = 162
      Height = 17
      Caption = 'Nivel7'
      TabOrder = 11
      OnClick = CBOpcionesClick
    end
    object CBNivel8: TCheckBox
      Tag = 13
      Left = 203
      Top = 131
      Width = 162
      Height = 17
      Caption = 'Nivel8'
      TabOrder = 12
      OnClick = CBOpcionesClick
    end
    object CBNivel9: TCheckBox
      Tag = 14
      Left = 203
      Top = 154
      Width = 162
      Height = 17
      Caption = 'Nivel9'
      TabOrder = 13
      OnClick = CBOpcionesClick
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 42467648
  end
end
