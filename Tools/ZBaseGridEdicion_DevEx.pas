unit ZBaseGridEdicion_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, DBCtrls, Buttons, ExtCtrls, StdCtrls, Grids, DBGrids,
     {$ifndef VER130}Variants,{$endif}     
     ZBaseDlgModal_DevEx,
     ZetaClientDataset,
     ZetaCommonLists, ZetaMessages,
     dbClient, ZetaDBGrid, ZetaSmartLists, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, ImgList, cxButtons, cxControls,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, cxNavigator,
  cxDBNavigator;

type
  TBaseGridEdicion_DevEx = class(TZetaDlgModal_DevEx)
    PanelIdentifica: TPanel;
    Splitter: TSplitter;
    ValorActivo1: TPanel;
    ValorActivo2: TPanel;
    DataSource: TDataSource;
    ZetaDBGrid: TZetaDBGrid;
    cxImageList16Edicion: TcxImageList;
    DevEx_cxDBNavigatorEdicion: TcxDBNavigator;
    DevEx_BarManagerEdicion: TdxBarManager;
    BarSuperior: TdxBar;
    dxBarButton_AgregarBtn: TdxBarButton;
    dxBarButton_BorrarBtn: TdxBarButton;
    dxBarButton_ModificarBtn: TdxBarButton;
    dxBarButton_BuscarBtn: TdxBarButton;
    dxBarButton_ImprimirBtn: TdxBarButton;
    dxBarButton_ImprimirFormaBtn: TdxBarButton;
    dxBarButton_ExportarBtn: TdxBarButton;
    dxBarButton_CortarBtn: TdxBarButton;
    dxBarButton_CopiarBtn: TdxBarButton;
    dxBarButton_PegarBtn: TdxBarButton;
    dxBarButton_UndoBtn: TdxBarButton;
    dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem;
    textoValorActivo1: TLabel;
    textoValorActivo2: TLabel;
    procedure AgregarBtnClick(Sender: TObject);
    procedure BorrarBtnClick(Sender: TObject);
    procedure ModificarBtnClick(Sender: TObject);
    procedure BuscarBtnClick(Sender: TObject);
    procedure ImprimirBtnClick(Sender: TObject);
    procedure ImprimirFormaBtnClick(Sender: TObject);
    procedure CortarBtnClick(Sender: TObject);
    procedure CopiarBtnClick(Sender: TObject);
    procedure PegarBtnClick(Sender: TObject);
    procedure UndoBtnClick(Sender: TObject);
    //procedure OKClick(Sender: TObject);
    //procedure CancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSourceStateChange(Sender: TObject);
    procedure ZetaDBGridColEnter(Sender: TObject);
    procedure ExportarBtnClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure dxBarButton_AgregarBtnClick(Sender: TObject);
    procedure dxBarButton_BorrarBtnClick(Sender: TObject);
    procedure dxBarButton_ModificarBtnClick(Sender: TObject);
    procedure dxBarButton_BuscarBtnClick(Sender: TObject);
    procedure dxBarButton_ImprimirBtnClick(Sender: TObject);
    procedure dxBarButton_ImprimirFormaBtnClick(Sender: TObject);
    procedure dxBarButton_ExportarBtnClick(Sender: TObject);
    procedure dxBarButton_CortarBtnClick(Sender: TObject);
    procedure dxBarButton_CopiarBtnClick(Sender: TObject);
    procedure dxBarButton_PegarBtnClick(Sender: TObject);
    procedure dxBarButton_UndoBtnClick(Sender: TObject);
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure MyMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure DevEx_BarManagerEdicionShowToolbarsPopup(
      Sender: TdxBarManager; PopupItemLinks: TdxBarItemLinks);
{    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState); }
  private
    { Private declarations }
    FTipoValorActivo1: TipoEstado;
    FTipoValorActivo2: TipoEstado;
    FPrimerColumna: Integer;
    FHuboCambios: Boolean;
    FHuboErrores : Boolean;
    FInicial: Boolean;
    FGeneral: Variant;
    FIndexDerechos: Integer;
    FPermiteAltas: Boolean;
    procedure AsignaValoresActivos;
    procedure InitValoresActivos;
    procedure InitColumnasActivas;
    procedure SetHuboCambios( const Value: Boolean );
    procedure ImprimirForma;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    procedure BorraCelda;
    procedure SetAccesos;
  protected
    { Protected declarations }
    property TipoValorActivo1: TipoEstado read FTipoValorActivo1 write FTipoValorActivo1;
    property TipoValorActivo2: TipoEstado read FTipoValorActivo2 write FTipoValorActivo2;
    property PrimerColumna: Integer read FPrimerColumna write FPrimerColumna;
    property HuboCambios: Boolean read FHuboCambios write SetHuboCambios;
    property IndexDerechos: Integer read FIndexDerechos write FIndexDerechos;
    property PermiteAltas: Boolean read FPermiteAltas write FPermiteAltas;
    function GridEnfocado: Boolean;
    function PosicionaSiguienteColumna: Integer; virtual;
    function Editing: Boolean; dynamic;
    function Inserting: Boolean;
    function SoloAltas: Boolean; dynamic;
    function ClientDataset: TZetaClientDataset;
    function DoConnect: Boolean;
    function ValoresGrid: Variant; virtual;
    function CheckDerechos(const iDerecho: Integer): Boolean;
    function PuedeAgregar: Boolean; dynamic;
    function PuedeBorrar: Boolean; dynamic;
    function PuedeModificar: Boolean; dynamic;
    function PuedeImprimir: Boolean; dynamic;
    procedure DoDisconnect;
    procedure EscribirCambios; dynamic;
    procedure CancelarCambios; dynamic;
    procedure Connect; virtual; abstract;
    procedure Disconnect; virtual;
    procedure Agregar; virtual;
    procedure Borrar; virtual;
    procedure Modificar; virtual;
    procedure Imprimir; dynamic;
    procedure Exportar;virtual;
    procedure Buscar; virtual;
    procedure HabilitaControles; dynamic;
    procedure SeleccionaPrimerColumna;
    procedure SeleccionaSiguienteRenglon;
    procedure SetCycleControl; dynamic;
    procedure KeyDown( var Key: Word; Shift: TShiftState ); override; { TWinControl }
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    property General: Variant read FGeneral write FGeneral;
  end;
  TBaseGridEdicionClass_DevEx = class of TBaseGridEdicion_DevEx;
  TDBGridHack = class(TDBGrid); //declared
var
  BaseGridEdicion_DevEx: TBaseGridEdicion_DevEx;

procedure ShowGridEdicion( var Forma; EdicionClass: TBaseGridEdicionClass_DevEx; const Parametro: Variant );

implementation

uses DCliente,
     FBaseReportes_DevEx,
     ZetaDialogo,
     ZCerrarEdicion_DevEx,
     ZetaCommonClasses,
     ZAccesosMgr;

{$R *.DFM}

procedure ShowGridEdicion( var Forma; EdicionClass: TBaseGridEdicionClass_DevEx; const Parametro: Variant );
begin
     try
        TBaseGridEdicion_DevEx( Forma ) := EdicionClass.Create( Application ) as TBaseGridEdicion_DevEx;
        with TBaseGridEdicion_DevEx( Forma ) do
        begin
             General := Parametro;
             ShowModal;
        end;
     finally
            TBaseGridEdicion_DevEx( Forma ).Free;
     end;
end;

{ ************ TBaseEdicionGrid *********** }

constructor TBaseGridEdicion_DevEx.Create(AOwner: TComponent);
begin
     FTipoValorActivo1 := stNinguno;
     FTipoValorActivo2 := stNinguno;
     FPrimerColumna := 0;
     FIndexDerechos := 0;
     FPermiteAltas := TRUE;
     inherited Create( AOwner );
     InitValoresActivos;
     InitColumnasActivas;
end;

procedure TBaseGridEdicion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     //DevEx (by am):Colores Base para el grid de Edicion
    ZetaDBGrid.FixedColor := RGB(235,235,235);//Gris
    ZetaDBGrid.TitleFont.Color := RGB(77,59,75);//Gris-Morado tono fuerte
    ZetaDBGrid.Font.Color := RGB(156,129,139);//Gris-Morado
    //DevEx (by am): Asigna el evento OnMouseWheel al grid de edicion
    TDBGridHack( ZetaDBGrid ).OnMouseWheel:= MyMouseWheel;
end;

procedure TBaseGridEdicion_DevEx.MyMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
     Handled := True;
     with ZetaDBGrid.DataSource.DataSet do
     begin
          if WheelDelta > 0 then
             Prior
          else
              Next;
     end;
end;

procedure TBaseGridEdicion_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     FInicial := TRUE;
     FHuboErrores := FALSE;
     if DoConnect then
     begin
          SetAccesos;
          AsignaValoresActivos;
          SeleccionaPrimerColumna;
     end;
end;

procedure TBaseGridEdicion_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     DoDisconnect;
     Action := caHide;
end;

procedure TBaseGridEdicion_DevEx.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     inherited;
     CanClose := True;
     if Editing then
     begin
          case ZCerrarEdicion_DevEx.CierraEdicion of
               mrOk: EscribirCambios;
               mrIgnore: CancelarCambios;
          else
              CanClose := False;
          end;
     end;
end;

function TBaseGridEdicion_DevEx.DoConnect: Boolean;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             try
                Connect;
                Result := True;
             except
                   on Error: Exception do
                   begin
                        zExcepcion( '� Error Al Conectar Forma !', '� Ventana No Pudo Ser Conectada A Sus Datos !', Error, 0 );
                        Result := False;
                   end;
             end;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

procedure TBaseGridEdicion_DevEx.DoDisconnect;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Disconnect;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

procedure TBaseGridEdicion_DevEx.Disconnect;
begin
     Datasource.Dataset := nil;
end;

procedure TBaseGridEdicion_DevEx.EscribirCambios;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with ClientDataset do
        begin
             Enviar;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBaseGridEdicion_DevEx.CancelarCambios;
begin
     with ClientDataset do
     begin
          if State in [ dsEdit, dsInsert ] then
             Cancel;
          CancelUpdates;
          DataSourceStateChange( Self );
     end;
     if SoloAltas then
        Close
     else
        SeleccionaPrimerColumna;
end;

{***DevEx (by am): Se dejo de usar el caption de los paneles para mostrar los valores activos.
Ahora se utilizara una label dentro del panel. ***}
procedure TBaseGridEdicion_DevEx.AsignaValoresActivos;
begin
     with dmCliente do
     begin
          textoValorActivo1.Caption := GetValorActivoStr( TipoValorActivo1 );
          textoValorActivo2.Caption := GetValorActivoStr( TipoValorActivo2 )
     end;

end;

procedure TBaseGridEdicion_DevEx.InitValoresActivos;
begin
     if ( TipoValorActivo1 = stNinguno ) and ( TipoValorActivo2 = stNinguno ) then
     begin
          PanelIdentifica.Visible := False;
          ValorActivo1.Visible := False;
          ValorActivo2.Visible := False;
          Splitter.Visible := False;
     end
     else
     begin
          if ( TipoValorActivo1 = stNinguno ) then
          begin
               ValorActivo1.Align := alNone;
               ValorActivo1.Visible := False;
               ValorActivo2.Align := alClient;
               Splitter.Visible := False;
          end;
          if ( TipoValorActivo2 = stNinguno ) then
          begin
               ValorActivo2.Align := alNone;
               ValorActivo2.Visible := False;
               ValorActivo1.Align := alClient;
               Splitter.Visible := False;
          end;
     end;
end;

procedure TBaseGridEdicion_DevEx.InitColumnasActivas;
var
   i: Integer;
begin
     with ZetaDBGrid do
     begin
          for i := 0 to ( Columns.Count - 1 ) do
          begin
               if Columns[ i ].ReadOnly then
               begin
                    Columns[ i ].Color := clInfoBk;
                    //Columns[ i ].Font.Color := clInfoText;
                    Columns[ i ].Font.Color := clGrayText;
               end
               else
               begin
                    Columns[ i ].Color := clWindow;
                    //Columns[ i ].Font.Color := clWindowText;
                    Columns[ i ].Font.Color := clGrayText;
               end;
          end;
     end;
end;

{procedure TBaseGridEdicion_DevEx.OKClick(Sender: TObject);
begin
     inherited;
     EscribirCambios;
     with ClientDataSet do
     begin
          if ( HuboErrores ) then
          begin
               FHuboErrores := TRUE;
               DisableControls;
               try
                  PosicionaError;
               finally
                  EnableControls;
               end;
          end
          else
              Self.Close;
     end;
end;

procedure TBaseGridEdicion_DevEx.CancelarClick(Sender: TObject);
begin
     if Editing then
        CancelarCambios
     else
        Close;
end; }

function TBaseGridEdicion_DevEx.ClientDataset: TZetaClientDataset;
begin
     Result := TZetaClientDataset( Datasource.Dataset );
end;

function TBaseGridEdicion_DevEx.Editing: Boolean;
begin
     if ( Datasource.State <> dsInactive ) then
        Result := ( ClientDataSet.ChangeCount > 0 ) or ( DataSource.State in [ dsEdit, dsInsert ] )
     else
         Result := False;
end;

function TBaseGridEdicion_DevEx.Inserting: Boolean;
begin
     if ( Datasource.State <> dsInactive ) then
        Result :=( SoloAltas ) and ( ( ClientDataSet.ChangeCount > 0 ) or ( DataSource.State in [ dsEdit, dsInsert ] ) )
     else
         Result := False;
end;

function TBaseGridEdicion_DevEx.SoloAltas: Boolean;
begin
     Result := False;
     if ( VarType( FGeneral ) = varBoolean ) then
        Result := FGeneral;
end;

procedure TBaseGridEdicion_DevEx.Agregar;
begin
     if PuedeAgregar then
     begin
          ClientDataset.Append;
          SeleccionaPrimerColumna;
     end;
end;

procedure TBaseGridEdicion_DevEx.Borrar;
begin
     if PuedeBorrar then
        with ClientDataset do
             if not IsEmpty then
             begin
                  Delete;
                  DataSourceStateChange( Self );
                  SeleccionaPrimerColumna;
             end;
end;

procedure TBaseGridEdicion_DevEx.Modificar;
begin
     if PuedeModificar then
        ClientDataset.Edit;
end;

function TBaseGridEdicion_DevEx.ValoresGrid : Variant;
begin
     Result := VarArrayOf( [ textoValorActivo1.Caption, textoValorActivo2.Caption, TipoValorActivo1, TipoValorActivo2 ] );
end;

procedure TBaseGridEdicion_DevEx.Imprimir;
var
   Valor : Variant;
begin
     if PuedeImprimir then
        if zConfirm( 'Imprimir...', '� Desea Imprimir El Grid De La Pantalla ' + Caption + ' ?', 0, mbYes ) then
        begin
             Valor := ValoresGrid;
             FBaseReportes_DevEx.ImprimirGrid( ZetaDBGrid, ZetaDBGrid.DataSource.DataSet, Caption, 'IM',
                                         Valor[0],Valor[1],Valor[2],Valor[3]);
        end;
end;

procedure TBaseGridEdicion_DevEx.Exportar;
var
   Valor : Variant;
begin
     if PuedeImprimir then
        if zConfirm( 'Exportar...', '� Desea exportar a Excel la informaci�n de ' + Caption + ' ?', 0, mbYes ) then
        begin
             Valor := ValoresGrid;
             FBaseReportes_DevEx.ExportarGrid( ZetaDBGrid,
                                         ZetaDBGrid.DataSource.DataSet,
                                         Caption,
                                         'IM',
                                         Valor[0],
                                         Valor[1],
                                         Valor[2],
                                         Valor[3] );
        end;
end;

procedure TBaseGridEdicion_DevEx.ImprimirForma;
begin
     if zConfirm( 'Imprimir...', '� Desea Imprimir La Pantalla ' + Caption + ' ?', 0, mbYes ) then
        Print;
end;

procedure TBaseGridEdicion_DevEx.Buscar;
begin
     { Para Habilitar Busqueda sobre columnas al presionar CTRL + F }
end;

procedure TBaseGridEdicion_DevEx.SeleccionaPrimerColumna;
begin
     Self.ActiveControl := ZetaDBGrid;
     ZetaDBGrid.SelectedField := ZetaDBGrid.Columns[ FPrimerColumna ].Field;   // Posicionar en la primer columna
end;

procedure TBaseGridEdicion_DevEx.SeleccionaSiguienteRenglon;
begin
     with ClientDataSet do
     begin
          Next;
          if EOF then
             if FPermiteAltas then
                Self.Agregar
             else
                SetCycleControl;
     end;
end;

procedure TBaseGridEdicion_DevEx.SetCycleControl;
begin
     if OK_DevEx.Enabled then
        OK_DevEx.SetFocus
     else
        Cancelar_DevEx.SetFocus;
end;

procedure TBaseGridEdicion_DevEx.BorraCelda;
begin
     if PuedeModificar then
        with ZetaDBGrid do
        begin
             if ( not Columns[ SelectedIndex ].ReadOnly ) then
             begin
                  with ClientDataSet do
                       if not ( State in [ dsEdit, dsInsert ] ) then
                          Edit;
                  with SelectedField do
                       case DataType of
                            ftSmallint, ftInteger, ftWord : AsInteger := 0;
                            ftString : AsString := VACIO;
                            ftFloat, ftCurrency, ftBCD: AsFloat := 0;
                            //ftDate, ftTime, ftDateTime : No se Pueden Borrar Fechas
                       end;
             end;
        end;
end;

{ ************* Consulta de Derechos de Acceso ************* }

function TBaseGridEdicion_DevEx.CheckDerechos(const iDerecho: Integer): Boolean;
begin
     Result := ( FIndexDerechos = 0 ) or ( ZAccesosMgr.CheckDerecho( FIndexDerechos, iDerecho ) );
end;

function TBaseGridEdicion_DevEx.PuedeAgregar: Boolean;
begin
     Result := CheckDerechos( K_DERECHO_ALTA );
end;

function TBaseGridEdicion_DevEx.PuedeModificar: Boolean;
begin
     Result := CheckDerechos( K_DERECHO_CAMBIO );
end;

function TBaseGridEdicion_DevEx.PuedeBorrar: Boolean;
begin
     Result := CheckDerechos( K_DERECHO_BAJA );
end;

function TBaseGridEdicion_DevEx.PuedeImprimir: Boolean;
begin
     Result := CheckDerechos( K_DERECHO_IMPRESION );
end;

procedure TBaseGridEdicion_DevEx.SetAccesos;

   procedure SetAccesoBoton( oControl: TControl; const lEnabled: Boolean );
   begin
        with oControl do
             if Visible then
                Enabled := lEnabled;
   end;
   procedure SetAccesoBoton_DevEx( oControl: TdxBarButton; const lEnabled: Boolean );
   begin
        with oControl do
             if Visible=ivAlways then
                Enabled := lEnabled;
   end;

begin
     {SetAccesoBoton( AgregarBtn, PuedeAgregar );
     SetAccesoBoton( ModificarBtn, PuedeModificar );
     SetAccesoBoton( BorrarBtn, PuedeBorrar );
     SetAccesoBoton( ImprimirBtn, PuedeImprimir );
     SetAccesoBoton( ImprimirFormaBtn, PuedeImprimir );}

     //DevEx
     SetAccesoBoton_DevEx( dxBarButton_AgregarBtn, PuedeAgregar );
     SetAccesoBoton_DevEx( dxBarButton_ModificarBtn, PuedeModificar );
     SetAccesoBoton_DevEx( dxBarButton_BorrarBtn, PuedeBorrar );
     SetAccesoBoton_DevEx( dxBarButton_ImprimirBtn, PuedeImprimir );
     SetAccesoBoton_DevEx( dxBarButton_ImprimirFormaBtn, PuedeImprimir );
     DataSource.AutoEdit := PuedeModificar;
end;

{ ************* Eventos de Controles ************* }

procedure TBaseGridEdicion_DevEx.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     if ( Editing or Inserting ) then
        HuboCambios := True
     else
         HuboCambios := False;
end;

procedure TBaseGridEdicion_DevEx.SetHuboCambios( const Value: Boolean );
begin
     if ( FHuboCambios <> Value ) or ( FInicial ) then
     begin
          FHuboCambios := Value;
          HabilitaControles;
          FInicial := False;
     end;
end;

procedure TBaseGridEdicion_DevEx.HabilitaControles;
begin
     OK_DevEx.Enabled := Editing;
     if Editing then
     begin
          with Cancelar_DevEx do
          begin
               //Kind := bkCancel;
               OptionsImage.ImageIndex := 0;
               ModalResult := mrNone;
               Cancel := False;
               Caption := '&Cancelar';
               Hint := 'Cancelar Cambios';
          end;
     end
     else
     begin
          // En casos donde hubo errores y el usuario decide borrar registros
          // del grid hasta que el ChangeCount llega a 0
          if ( FHuboErrores ) then
          begin
            // ZetaDialogo.ZInformation( Self.Caption, 'Todos los errores han sido corregidos' + CR_LF + 'y los registros fueron grabados', 0 );
            if ZetaDialogo.ZConfirm( Self.Caption, 'Todos los errores han sido corregidos' + CR_LF + 'y los registros fueron grabados.' + CR_LF + '� Desea Salir ?', 0, mbYes ) then
              Close;
            FHuboErrores := FALSE;
          end;
          with Cancelar_DevEx do
          begin
               //Kind := bkClose;
               OptionsImage.ImageIndex := 2;
               ModalResult := mrNone;
               Cancel := True;
               Caption := '&Salir';
               Hint := 'Cerrar Pantalla y Salir';
          end;
     end;
     Self.ActiveControl := ZetaDBGrid;
     Application.ProcessMessages;
end;

procedure TBaseGridEdicion_DevEx.AgregarBtnClick(Sender: TObject);
begin
     inherited;
     Agregar;
end;

procedure TBaseGridEdicion_DevEx.BorrarBtnClick(Sender: TObject);
begin
     inherited;
     Borrar;
end;

procedure TBaseGridEdicion_DevEx.ModificarBtnClick(Sender: TObject);
begin
     inherited;
     Modificar;
end;

procedure TBaseGridEdicion_DevEx.ImprimirBtnClick(Sender: TObject);
begin
     inherited;
     Imprimir;
end;

procedure TBaseGridEdicion_DevEx.ImprimirFormaBtnClick(Sender: TObject);
begin
     inherited;
     ImprimirForma;
end;

procedure TBaseGridEdicion_DevEx.BuscarBtnClick(Sender: TObject);
begin
     inherited;
     Buscar;
end;

procedure TBaseGridEdicion_DevEx.CortarBtnClick(Sender: TObject);
begin
     inherited;
     SendMessage( ActiveControl.Handle, WM_CUT, 0, 0 );
end;

procedure TBaseGridEdicion_DevEx.CopiarBtnClick(Sender: TObject);
begin
     inherited;
     SendMessage( ActiveControl.Handle, WM_COPY, 0, 0 );
end;

procedure TBaseGridEdicion_DevEx.PegarBtnClick(Sender: TObject);
begin
     inherited;
     SendMessage( ActiveControl.Handle, WM_PASTE, 0, 0 );
end;

procedure TBaseGridEdicion_DevEx.UndoBtnClick(Sender: TObject);
begin
     inherited;
     SendMessage( ActiveControl.Handle, WM_UNDO, 0, 0 );
end;

function TBaseGridEdicion_DevEx.PosicionaSiguienteColumna: Integer;
var
   i: Integer;
begin
     with ZetaDBGrid do
     begin
          i := SelectedIndex + 1;
          while ( i < Columns.Count ) and ( Columns[ i ].ReadOnly or not Columns[ i ].Visible ) do
          begin
               i := i + 1;
          end;
          if ( i = Columns.Count ) then
             Result := FPrimerColumna
          else
              Result := i;
     end;
end;

procedure TBaseGridEdicion_DevEx.KeyDown(var Key: Word; Shift: TShiftState);
begin
     if ( GridEnfocado ) then
     begin
          if ( ssShift in Shift ) then { SHIFT }
          begin
               case Key of
                    VK_INSERT:    { INS = Agregar }
                    begin
                         Key := 0;
                         Agregar;
                    end;
                    VK_DELETE:    { DEL = Borrar }
                    begin
                         Key := 0;
                         Borrar;
                    end;
               end;
          end
          else
          begin
              if ( ssCtrl in Shift ) then { CTRL }
              begin
                   case Key of
                        VK_INSERT:   { INS = Modificar }
                        begin
                             Key := 0;
                             Modificar;
                        end;
                        80: { Letra P = Imprimir }
                        begin
                             Key := 0;
                             Imprimir;
                        end;
                        70: { Letra F = Buscar }
                        begin
                             Key := 0;
                             Buscar;
                        end;
                   end;
              end;
          end;
          if ( Key = VK_RETURN ) then
          begin
               Key := 0;
          end;
          if ( Key = VK_DOWN ) then
          begin
               Key := 0;
               SeleccionaSiguienteRenglon;
          end;
          if ( Key = VK_DELETE ) and ( not ZetaDBGrid.EditorMode ) then     // Si est� editando la celda lo deja pasar
          begin
               Key := 0;
               BorraCelda;
          end;
     end
     else
         inherited KeyDown( Key, Shift );
end;

procedure TBaseGridEdicion_DevEx.KeyPress(var Key: Char);
begin
     if ( GridEnfocado ) then
     begin
          if ( Key = Chr( VK_RETURN ) ) then
          begin
               Key := #0;
               ZetaDBGrid.SelectedIndex := PosicionaSiguienteColumna;
               if ( ZetaDBGrid.SelectedIndex = FPrimerColumna ) then
                  SeleccionaSiguienteRenglon;
          end
          else
              if ( Key = Chr( VK_ESCAPE ) ) then
              begin
                   SeleccionaPrimerColumna;
              end;
     end
     else if ActiveControl.TabStop then
          inherited KeyPress( Key );
end;

function TBaseGridEdicion_DevEx.GridEnfocado: Boolean;
begin
     Result := ( ActiveControl = ZetaDBGrid );
end;

procedure TBaseGridEdicion_DevEx.WMExaminar(var Message: TMessage);
begin
     if ( TExaminador(  Message.LParam ) = exEnter ) then
     begin
          ZetaDBGrid.SelectedIndex := PosicionaSiguienteColumna;
          if ( ZetaDBGrid.SelectedIndex = FPrimerColumna ) then
             SeleccionaSiguienteRenglon;
     end;
end;

procedure TBaseGridEdicion_DevEx.ZetaDBGridColEnter(Sender: TObject);
begin
     inherited;
     with ZetaDBGrid do
          with Columns[SelectedIndex] do
               dxBarButton_BuscarBtn.Enabled := ( not ReadOnly ) and ( ButtonStyle = cbsAuto );
              //BuscarBtn.Enabled := ( not ReadOnly ) and ( ButtonStyle = cbsAuto );
end;

procedure TBaseGridEdicion_DevEx.ExportarBtnClick(Sender: TObject);
begin
     inherited;
     Exportar;
end;

procedure TBaseGridEdicion_DevEx.OK_DevExClick(Sender: TObject);
begin
  inherited;
  EscribirCambios;
     with ClientDataSet do
     begin
          if ( HuboErrores ) then
          begin
               FHuboErrores := TRUE;
               DisableControls;
               try
                  PosicionaError;
               finally
                  EnableControls;
               end;
          end
          else
              Self.Close;
     end;
end;

procedure TBaseGridEdicion_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
  if Editing then
        CancelarCambios
     else
        Close;
end;

procedure TBaseGridEdicion_DevEx.dxBarButton_AgregarBtnClick(
  Sender: TObject);
begin
  inherited;
  Agregar;

end;

procedure TBaseGridEdicion_DevEx.dxBarButton_BorrarBtnClick(
  Sender: TObject);
begin
  inherited;
  Borrar;
end;

procedure TBaseGridEdicion_DevEx.dxBarButton_ModificarBtnClick(
  Sender: TObject);
begin
  inherited;
  Modificar;
end;

procedure TBaseGridEdicion_DevEx.dxBarButton_BuscarBtnClick(
  Sender: TObject);
begin
  inherited;
  Buscar;
end;

procedure TBaseGridEdicion_DevEx.dxBarButton_ImprimirBtnClick(
  Sender: TObject);
begin
  inherited;
  Imprimir;
end;

procedure TBaseGridEdicion_DevEx.dxBarButton_ImprimirFormaBtnClick(
  Sender: TObject);
begin
  inherited;
  ImprimirForma;
end;

procedure TBaseGridEdicion_DevEx.dxBarButton_ExportarBtnClick(
  Sender: TObject);
begin
  inherited;
  Exportar;
end;

procedure TBaseGridEdicion_DevEx.dxBarButton_CortarBtnClick(
  Sender: TObject);
begin
  inherited;
  SendMessage( ActiveControl.Handle, WM_CUT, 0, 0 );
end;

procedure TBaseGridEdicion_DevEx.dxBarButton_CopiarBtnClick(
  Sender: TObject);
begin
  inherited;
  SendMessage( ActiveControl.Handle, WM_COPY, 0, 0 );
end;

procedure TBaseGridEdicion_DevEx.dxBarButton_PegarBtnClick(
  Sender: TObject);
begin
  inherited;
  SendMessage( ActiveControl.Handle, WM_PASTE, 0, 0 );
end;

procedure TBaseGridEdicion_DevEx.dxBarButton_UndoBtnClick(Sender: TObject);
begin
  inherited;
  SendMessage( ActiveControl.Handle, WM_UNDO, 0, 0 );
end;

procedure TBaseGridEdicion_DevEx.ZetaDBGridDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
   iCurrentRow, iRecNo: Integer;
begin
  inherited;
  with ZetaDBGrid, Canvas, Brush do
    begin
         if ( State = [] ) then
         begin
              iCurrentRow := Rect.Top div (1+Rect.Bottom-Rect.Top);
              iRecNo:= TDBGridHack( ZetaDBGrid ).Row;
              if iCurrentRow = iRecNo then
              begin
                   //Color:= clBtnShadow;
                   Color:=  RGB(237,230,249);//RGB(156,129,139); //morado row seleccionado
                   Font.Color := clWindowText;
              end;
         end
         else if gdSelected in State then
         begin
              //Color:= clHighlight;
              Color:= RGB(212,188,251);//RGB(136,174,213); //Azul de celda seleccionado
              Font.Color := clHighlightText; //Texto en blanco para celda seleccionada
         end;
         DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;
end;

//DevEx (by am): Metodo agregado para que no no salga ningun pop up  al dar clic derecho sobre los toolbar de las formas de edicion.
procedure TBaseGridEdicion_DevEx.DevEx_BarManagerEdicionShowToolbarsPopup(
  Sender: TdxBarManager; PopupItemLinks: TdxBarItemLinks);
begin
  inherited;
  Abort;
end;

end.

