unit ZetaHayProblemas;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls,
     ZBaseDlgModal;

type
  THayProblemas = class(TZetaDlgModal)
    Switch: TBitBtn;
    PanelResumen: TPanel;
    PanelDescripcion: TPanel;
    Mensajes: TMemo;
    Imagen: TImage;
    procedure FormShow(Sender: TObject);
    procedure SwitchClick(Sender: TObject);
  private
    { Private declarations }
    FResumen: String;
    FArchivo: String;
    FDetails: Boolean;
    function GetMensaje: String;
    procedure SetControls;
    procedure SetMensaje( const sValue: String );
  public
    { Public declarations }
    property Mensaje: String read GetMensaje write SetMensaje;
    property Resumen: String read FResumen write FResumen;
    property Archivo: String read FArchivo write FArchivo;
  end;

var
  HayProblemas: THayProblemas;

function ShowProblemsFoundDialog( const sMensaje, sResumen, sArchivo: String  ): Boolean;

implementation

{$R *.DFM}

function ShowProblemsFoundDialog( const sMensaje, sResumen, sArchivo: String  ): Boolean;
begin
     with THayProblemas.Create( Application ) do
     begin
          try
             Mensaje := sMensaje;
             Resumen := sResumen;
             Archivo := sArchivo;
             ShowModal;
             Result := ( ModalResult = mrOk );
          finally
                 Free;
          end;
     end;
end;

{ *********** THayProblemas ********** }

procedure THayProblemas.FormShow(Sender: TObject);
begin
     inherited;
     FDetails := True;
     SetControls;
     ActiveControl := Switch;
end;

function THayProblemas.GetMensaje: String;
begin
     Result := PanelDescripcion.Caption;
end;

procedure THayProblemas.SetMensaje(const sValue: String);
begin
     PanelDescripcion.Caption := sValue;
end;

procedure THayProblemas.SetControls;
begin
     with Mensajes do
     begin
          with Lines do
          begin
               BeginUpdate;
               Clear;
               if FDetails then
               begin
                    FDetails := False;
                    Add( FResumen );
                    ScrollBars := ssNone;
                    BorderStyle := bsNone;
                    with Switch do
                    begin
                         Caption := 'Ver &Detalles';
                         Hint := 'Mostrar Detalles de los Problemas';
                    end;
               end
               else
               begin
                    FDetails := True;
                    if FileExists( FArchivo ) then
                       LoadFromFile( FArchivo )
                    else
                        Add( 'Archivo ' + FArchivo + 'No Existe' );
                    ScrollBars := ssBoth;
                    BorderStyle := bsSingle;
                    with Switch do
                    begin
                         Caption := 'Ver &Resumen';
                         Hint := 'Mostrar Resumen del Problema';
                    end;
               end;
               EndUpdate;
          end;
     end;
end;

procedure THayProblemas.SwitchClick(Sender: TObject);
begin
     inherited;
     SetControls;
end;

end.
