unit ZetaRegistryCliente;

interface

uses Windows, Messages, SysUtils, Classes,
     {$ifndef DOTNET}Graphics, Controls, Forms, Dialogs,{$endif}
     Registry,ZetaCommonClasses;

{$ifndef DOS_CAPAS}
{$define HTTP_CONNECTION}
{$endif}

type
  {$ifdef HTTP_CONNECTION}
  TTipoConexion = ( conxDCOM, conxHTTP );
  {$endif}
  TZetaClientRegistry = class(TObject)
  private
    { Private declarations }
    FRegistry: TRegIniFile;
    FCompania: String;
    FCanWrite: Boolean;
    {$ifdef TRESS_DELPHIXE5_UP}
    FRegistryUser: TRegIniFile;
    FCanWriteUser: Boolean;
    {$endif}
    function GetRegistry: TRegIniFile;
    function GetCanWrite: Boolean;
    function GetComputerName: String;
    function GetConsolaUsuario: String;
    function GetConsolaContrasena: String;
    function GetInvocaApp: String;
    procedure SetInvocaApp( const Value: String );
    procedure SetComputerName(const Value: String);
    function GetCacheSistemaActive: Boolean;
    procedure SetCacheSistemaActive(const Value: Boolean);
    function GetCacheSistemaFolder: String;
    procedure SetCacheSistemaFolder(const Value: String);
    function GetCacheActive: Boolean;
    procedure SetCacheActive(const Value: Boolean);
    function GetCacheFolder: String;
    procedure SetCacheFolder(const Value: String);
    function GetCacheFecha: String;
    procedure SetCacheFecha(const Value: String);
    procedure SetCompania(const Value: String);
    function GetRegistryKey: String;
    function GetRegistryRoot: HKEY;
    {$ifdef HTTP_CONNECTION}
    function GetPassword: String;
    function GetTipoConexion: TTipoConexion;
    function GetURL: String;
    function GetUserName: String;
    procedure SetPassword(const Value: String);
    procedure SetTipoConexion(const Value: TTipoConexion);
    procedure SetURL(const Value: String);
    procedure SetUserName(const Value: String);
    {$endif}
    {$ifdef TIMBRADO}
    function GetTimbradoURL : String;
    procedure SetTimbradoURL(const Value: String);
    function GetRecibosPath : String;
    procedure SetRecibosPath(const Value: String);
    function GetTimbradoPaqueteSize : integer;
    procedure SetTimbradoPaqueteSize(const Value: integer);
    //Intento de Timbrado de Nomina
    function GetTimbradoPaqueteTimeoutConnect: integer;
    procedure SetTimbradoPaqueteTimeoutConnect(const Value: integer);
    function GetTimbradoPaqueteTimeoutSend: integer;
    procedure SetTimbradoPaqueteTimeoutSend(const Value: integer);
    function GetTimbradoPaqueteTimeoutReceive: integer;
    procedure SetTimbradoPaqueteTimeoutReceive(const Value: integer);
    function GetTimbradoPaqueteIntentos: integer;
    procedure SetTimbradoPaqueteIntentos(const Value: integer);
    function GetTimbradoPaqueteEspera: integer;
    procedure SetTimbradoPaqueteEspera(const Value: integer);
    {$endif}
    function GetRegPatronal: String;
    function GetTipoNomina: Integer;
    procedure SetRegPatronal(const Value: String);
    procedure SetTipoNomina(const Value: integer);
    function GetVSPresentacion:String;                     //DevEx (@am): Lee y Escribe si se desea ver la presentacion de bienvenida
    procedure SetVSPresentacion (Value: String);
    function GetFiltroCursosProgramados: String;
    procedure SetFiltroCursosProgramados(const Value: String);
    function GetHomeSistema: Boolean;
    procedure SetHomeSistema(const Value: Boolean);
  protected
    { Protected declarations }
    property Registry: TRegIniFile read FRegistry;
    {$ifdef TRESS_DELPHIXE5_UP}
    property RegistryUser: TRegIniFile read FRegistryUser;
    {$endif}
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property CanWrite: Boolean read FCanWrite;
    property RegistryKey: String read GetRegistryKey;
    property RegistryRoot: HKEY read GetRegistryRoot;
    property ComputerName: String read GetComputerName write SetComputerName;
    property Compania: String read FCompania write SetCompania;
    property CacheSistemaActive: Boolean read GetCacheSistemaActive write SetCacheSistemaActive;
    property CacheSistemaFolder: String read GetCacheSistemaFolder write SetCacheSistemaFolder;
    property CacheActive: Boolean read GetCacheActive write SetCacheActive;
    property CacheFolder: String read GetCacheFolder write SetCacheFolder;
    property CacheFecha: String read GetCacheFecha write SetCacheFecha;
    {$ifdef HTTP_CONNECTION}
    property TipoConexion: TTipoConexion read GetTipoConexion write SetTipoConexion;
    property URL: String read GetURL write SetURL;
    property UserName: String read GetUserName write SetUserName;
    property Password: String read GetPassword write SetPassword;
    {$endif}
    {$ifdef TIMBRADO}
    property TimbradoURL: String read GetTimbradoURL write SetTimbradoURL;
    property RecibosPath: String read GetRecibosPath write SetRecibosPath;
    property TimbradoPaqueteSize: integer read GetTimbradoPaqueteSize write SetTimbradoPaqueteSize;
    //Intento de Timbrado de Nomina
    property TimbradoPaqueteTimeoutConnect: integer read GetTimbradoPaqueteTimeoutConnect write SetTimbradoPaqueteTimeoutConnect;
    property TimbradoPaqueteTimeoutSend: integer read GetTimbradoPaqueteTimeoutSend write SetTimbradoPaqueteTimeoutSend;
    property TimbradoPaqueteTimeoutReceive: integer read GetTimbradoPaqueteTimeoutReceive write SetTimbradoPaqueteTimeoutReceive;
    property TimbradoPaqueteIntentos: integer read GetTimbradoPaqueteIntentos write SetTimbradoPaqueteIntentos;
    property TimbradoPaqueteEspera: integer read GetTimbradoPaqueteEspera write SetTimbradoPaqueteEspera;
    {$endif}
    property TipoNomina:  Integer read GetTipoNomina write SetTipoNomina;
    property RegPatronal: String read GetRegPatronal write SetRegPatronal;
    property FiltroCursosProgramados: String read GetFiltroCursosProgramados write SetFiltroCursosProgramados;

    function BuscaServidor( const lInitVirtual: Boolean = FALSE ): Boolean;

    {$ifdef TRESS_DELPHIXE5_UP}
    {$ifdef WIN32}
    function LeeVirtualStore: Boolean;
    {$endif}
    {$endif}
    function InitComputerName: Boolean;
    function GetLastEmpresa( const iUsuario: Integer ): String;
    procedure SetLastEmpresa( const iUsuario: Integer; const sEmpresa: String );
    property ConsolaUsuario: string read GetConsolaUsuario;
    property ConsolaContrasena: string read GetConsolaContrasena;
    property InvocaApp: string read GetInvocaApp write SetInvocaApp;
    property VSPresentacion: String read GetVSPresentacion write SetVSPresentacion;  //DevEx (@am): Propiedad para leer y escribir si el usuario desea ver la presentacion de bienvenida al entrar a la aplicacion.
    function GetAccesoDirecto(const numAcceso:Integer): Integer;
    procedure SetAccesosDirecto (const numAcceso : Integer ; const Value: Integer);
    property HomeSistema: Boolean read GetHomeSistema write SetHomeSistema;
  end;

 {$ifdef VISITANTES}
 TClientRegistryVisita = class(TZetaClientRegistry)
  private
    function GetCasetaDefault: string;
    procedure SetCasetaDefault(const Value: string);
    function GetImpresionGafete: integer;
    function GetMostrarGridCitas: Boolean;
    procedure SetImpresionGafete(const Value: integer);
    procedure SetMostrarGridCitas(const Value: Boolean);
    function GetFiltrarTiempo: Boolean;
    function GetMinutosAntes: Integer ;
    function GetMinutosDespues: Integer;
    function GetAutoReg_AsuntoATratar: boolean;
    function GetAutoReg_Identificacion: boolean;
    function GetAutoReg_Observaciones: boolean;
    function GetAutoReg_Vehiculo: boolean;
    function GetImpresoraDefault: string;
    procedure SetAutoReg_AsuntoATratar(const Value: boolean);
    procedure SetAutoReg_Identificacion(const Value: boolean);
    procedure SetAutoReg_Observaciones(const Value: boolean);
    procedure SetAutoReg_Vehiculo(const Value: boolean);
    procedure SetImpresoraDefault(const Value: string);
    procedure SetFiltrarTiempo(const Value: Boolean);
    procedure SetMinutosAntes(const Value: Integer);
    procedure SetMinutosDespues(const Value: Integer);
    function GetMostrarGridAvanzado: Boolean;
    procedure SetMostrarGridAvanzado(const Value: Boolean);
    function GetMostrarRegistroGrupo: Boolean;
    procedure SetMostrarRegistroGrupo(const Value: Boolean);


  public
    property CasetaDefault: string read GetCasetaDefault write SetCasetaDefault;
    property ImpresionGafete: integer read GetImpresionGafete write SetImpresionGafete;
    property MostrarGridCitas: Boolean read GetMostrarGridCitas write SetMostrarGridCitas;
    property MostrarGridAvanzado: Boolean read GetMostrarGridAvanzado write SetMostrarGridAvanzado;
    property MostrarRegistroGrupo: Boolean read GetMostrarRegistroGrupo write SetMostrarRegistroGrupo;
    property ImpresoraDefault: string read GetImpresoraDefault write SetImpresoraDefault;
    property AutoReg_AsuntoATratar: boolean read GetAutoReg_AsuntoATratar write SetAutoReg_AsuntoATratar;
    property AutoReg_Vehiculo: boolean read GetAutoReg_Vehiculo write SetAutoReg_Vehiculo;
    property AutoReg_Identificacion: boolean read GetAutoReg_Identificacion write SetAutoReg_Identificacion;
    property AutoReg_Observaciones: boolean read GetAutoReg_Observaciones write SetAutoReg_Observaciones;
    property FiltrarTiempo: Boolean read GetFiltrarTiempo write SetFiltrarTiempo;
    property MinutosAntes: Integer read GetMinutosAntes write SetMinutosAntes;
    property MinutosDespues: Integer read GetMinutosDespues write SetMinutosDespues;
  end;
  {$endif}

   {$ifdef FONACOT}
   TClientFonacotRegistry = class( TZetaClientRegistry )
    private
      { Private declarations }
      function GetRutaImportarPrestamos: String;
      function GetRutaActualizaPrestamos: String;
      function GetRutaImportarKardex: String;
      function GetRutaUltimaCedula: String;
      function GetRutaUltimaCedulaConfronta: String;
      function GetRazonSocialConciliacion: String;
      function GetRazonSocialConfronta: String;
      procedure SetRutaImportarPrestamos(const Value: String);
      procedure SetRutaActualizaPrestamos(const Value: String);
      procedure SetRutaImportarKardex(const Value: String);
      procedure SetRutaUltimaCedula(const Value: String);
      procedure SetRutaUltimaCedulaConfronta(const Value: String);
      procedure SetRazonSocialConciliacion(const Value: String);
      procedure SetRazonSocialConfronta(const Value: String);
    protected
      { Protected declarations }
    public
      { Public declarations }
      property RutaImportarPrestamos: String read GetRutaImportarPrestamos write SetRutaImportarPrestamos;
      property RutaActualizaPrestamos: String read GetRutaActualizaPrestamos write SetRutaActualizaPrestamos;
      property RutaImportarKardex: String read GetRutaImportarKardex write SetRutaImportarKardex;
      property RutaUltimaCedula: String read GetRutaUltimaCedula write SetRutaUltimaCedula;
      property RutaUltimaCedulaConfronta: String read GetRutaUltimaCedulaConfronta write SetRutaUltimaCedulaConfronta;
      property RazonSocialConciliacion: String read GetRazonSocialConciliacion write SetRazonSocialConciliacion;
      property RazonSocialConfronta: String read GetRazonSocialConfronta write SetRazonSocialConfronta;
    end;
  {$endif}

   {$ifdef CONCILIA}
   TClientIMSSRegistry = class( TZetaClientRegistry )
    private
      { Private declarations }
      function GetConciliadorIMSSPatron: String;
      function GetConciliadorIMSSYear: String;
      function GetConciliadorIMSSMes: String;
      function GetConciliadorIMSSTipo: String;
      function GetConciliadorIMSSTipoConciliacion: String;
      function GetConciliadorIMSSBaseDatosSUA: String;
      function GetConciliadorIMSSDirectorioIMSS: String;
      function GetConciliadorIMSSClaseEmision: String;
      procedure SetConciliadorIMSSPatron(const Value: String);
      procedure SetConciliadorIMSSYear(const Value: String);
      procedure SetConciliadorIMSSMes(const Value: String);
      procedure SetConciliadorIMSSTipo(const Value: String);
      procedure SetConciliadorIMSSTipoConciliacion(const Value: String);
      procedure SetConciliadorIMSSBaseDatosSUA(const Value: String);
      procedure SetConciliadorIMSSDirectorioIMSS(const Value: String);
      procedure SetConciliadorIMSSClaseEmision(const Value: String);
    protected
      { Protected declarations }
    public
      { Public declarations }
        property ConciliadorIMSSPatron: String read GetConciliadorIMSSPatron write SetConciliadorIMSSPatron;
        property ConciliadorIMSSYear: String read GetConciliadorIMSSYear write SetConciliadorIMSSYear;
        property ConciliadorIMSSMes: String read GetConciliadorIMSSMes write SetConciliadorIMSSMes;
        property ConciliadorIMSSTipo: String read GetConciliadorIMSSTipo write SetConciliadorIMSSTipo;
        property ConciliadorIMSSTipoConciliacion: String read GetConciliadorIMSSTipoConciliacion write SetConciliadorIMSSTipoConciliacion;
        property ConciliadorIMSSBaseDatosSUA: String read GetConciliadorIMSSBaseDatosSUA write SetConciliadorIMSSBaseDatosSUA;
        property ConciliadorIMSSDirectorioIMSS: String read GetConciliadorIMSSDirectorioIMSS write SetConciliadorIMSSDirectorioIMSS;
        property ConciliadorIMSSClaseEmision: String read GetConciliadorIMSSClaseEmision write SetConciliadorIMSSClaseEmision;
    end;
  {$endif}

var
   {$ifdef VISITANTES}
   ClientRegistry: TClientRegistryVisita;
   {$ELSE}
   {$ifdef FONACOT}
   ClientRegistry: TClientFonacotRegistry;
   {$else}
   {$ifdef CONCILIA}
   ClientRegistry: TClientIMSSRegistry;
   {$else}
   ClientRegistry: TZetaClientRegistry;
   {$ENDIF}
   {$ENDIF}
   {$ENDIF}

procedure InitClientRegistry;
procedure ClearClientRegistry;

implementation

uses ZetaCommonTools,
{$ifdef TIMBRADO}
     ShlObj,
{$endif}
     ZetaDialogo,
     FBuscaServidor_DevEx;

const
     {$ifdef HTTP_CONNECTION}
     K_KEY = 't0MacEwsky';
     {$endif}
     {$ifdef PORTAL}
     CLIENT_REGISTRY_PATH = 'Software\Grupo Tress\Portal\Client';
     {$else}
            {$ifdef VISITANTES}
            CLIENT_REGISTRY_PATH = 'Software\Grupo Tress\Visitantes';
            {$else}
            //{$ifdef FONACOT}
            //CLIENT_REGISTRY_PATH = 'Software\Grupo Tress\Conciliador Fonacot';
            //{$else}
            CLIENT_REGISTRY_PATH = 'Software\Grupo Tress\TressWin\Client';
            //{$endif}
            {$endif}
     {$endif}
     SERVER_COMPUTER_NAME = 'Server Computer Name';
     CLIENT_REGISTRY_SISTEMA_PATH = 'Sistema';
     FILES_CACHE_ACTIVE   = 'Files Cache Active';
     FILES_CACHE_FOLDER   = 'Files Cache Folder';
     FILES_CACHE_FECHA    = 'Files Cache Fecha';
     CLIENT_LAST_COMPANY  = 'Last Company';
     CLIENT_TIPO_NOMINA   = 'Tipo de Nomina';
     CLIENT_REG_PATRONAL  = 'Registro Patronal';
     CLIENT_FILTRO_CURSOPROG = 'Filtro Cursos Programados';
     CONSOLA_USR = 'ConsolaUsuario';
     CONSOLA_PWD = 'ConsolaClave';
     INVOCA_APP = 'InvocaApp';
     VS_PRESENTACION ='VsPresentacion';    //DevEx (@am): Key Agregado para guardar la seleccion del usuario para continuar viendo la presentacion de bienvenida
     TACCESO_DIRECTO = 'AccesoDirecto';
     THOME_SISTEMA = 'vsHomeSistema';
     CLIENT_ACCESOS_DIRECTOS = 'Accesos Directos';
     {$ifdef HTTP_CONNECTION}
     TIPO_CONEXION = 'TipoConexion';
     SERVER_URL = 'ServerURL';
     SERVER_USERNAME = 'ServerUserName';
     SERVER_PASSWORD = 'ServerPassword';
     {$endif}
{$ifdef TIMBRADO}
     TIMBRADO_URL = 'TimbradoURL';
     TIMBRADO_DEFAULT_URL = 'https://ws.recibodigital.mx';
     TIMBRADO_TEST_KEY = 'TimbradoTestKey';
     TIMBRADO_RECIBOS_PATH = 'TimbradoRecibosPath';
     TIMBRADO_RECIBOS_PATH_DEFAULT = '';
     TIMBRADO_PAQUETE_SIZE = 'TimbradoPaqueteSize';
     TIMBRADO_PAQUETE_SIZE_DEFAULT = 100;
     //Registry Timbrado
     TIMBRADO_PAQUETE_TIMEOUT_CONNECT = 'TimbradoPaqueteTimeoutConnect';
     TIMBRADO_PAQUETE_TIMEOUT_CONNECT_DEFAULT = 30000;
     TIMBRADO_PAQUETE_TIMEOUT_SEND = 'TimbradoPaqueteTimeoutSend';
     TIMBRADO_PAQUETE_TIMEOUT_SEND_DEFAULT = 120000;
     TIMBRADO_PAQUETE_TIMEOUT_RECEIVE = 'TimbradoPaqueteTimeoutReceive';
     TIMBRADO_PAQUETE_TIMEOUT_RECEIVE_DEFAULT = 240000;
     TIMBRADO_PAQUETE_INTENTOS = 'TimbradoPaqueteIntentos';
     TIMBRADO_PAQUETE_INTENTOS_DEFAULT = 5;
     TIMBRADO_PAQUETE_ESPERA = 'TimbradoPaqueteEspera';
     TIMBRADO_PAQUETE_ESPERA_DEFAULT = 60000;
{$endif}

{$ifdef VISITANTES}
 const
     KEY_VISITANTES = 'Visitantes';
     CASETA_DEFAULT = 'Caseta Default';
     IMPRESION_GAFETE = 'Impresion Gafete';
     MOSTRAR_GRID_CITAS = 'Mostrar Grid Citas';
     MOSTRAR_GRID_AVANZADO = 'Mostrar grid Avanzado';
     MOSTRAR_REGISTRO_GRUPO = 'Mostrar registro en grupo';
     IMPRESORA_DEFAULT = 'Impresora Default';
     AUTOREG_ASUNTO = 'Auto Registro - Asunto a Tratar';
     AUTOREG_VEH = 'Auto Registro - Vehiculo';
     AUTOREG_IDENT = 'Auto Registro - Identificacion';
     AUTOREG_OBS = 'Auto Registro - Observaciones';
     FILTRAR_TIEMPO = 'Filtrar por Tiempo';
     MIN_ANTES = 'Minutos antes';
     MIN_DESPUES = 'Minutos despues';
{$endif}

{$ifdef FONACOT}
const
     K_BITACORA = '';
     K_RUTA_BITACORA_IMPORTAR_PRESTAMOS = 'RutaImportarPrestamos';
     K_RUTA_BITACORA_IMPORTAR_KARDEX = 'RutaImportarKardex';
     K_RUTA_BITACORA_ACTUALIZA_PRESTAMOS = 'RutaActualizaPrestamos';
     K_RUTA_ULTIMA_CEDULA = 'RutaUltimaCedula';
     K_RUTA_ULTIMA_CEDULA_CONFRONTA = 'RutaUltimaCedulaConfronta';
     K_RAZON_SOCIAL_CONCILIACION = 'RazonSocialConciliacion';
     K_RAZON_SOCIAL_CONFRONTA = 'RazonSocialConfronta';
{$endif}

{$ifdef CONCILIA}
const
     CONCILIADOR_IMSS_PATRON = 'ConciliadorIMSSPatron';
     CONCILIADOR_IMSS_YEAR = 'ConciliadorIMSSYear';
     CONCILIADOR_IMSS_MES = 'ConciliadorIMSSMes';
     CONCILIADOR_IMSS_TIPO = 'ConciliadorIMSSTipo';
     CONCILIADOR_IMSS_TIPO_CONCILIACION = 'ConciliadorIMSSTipoConciliacion';
     CONCILIADOR_IMSS_BASE_DATOS_SUA = 'ConciliadorIMSSBaseDatosSUA';
     CONCILIADOR_IMSS_DIRECTORIO_IMSS = 'ConciliadorIMSSDirectorioIMSS';
     CONCILIADOR_IMSS_CLASE_EMISION = 'ConciliadorIMSSClaseEmision';
{$endif}

procedure InitClientRegistry;
begin
     {$ifdef VISITANTES}
     ClientRegistry := TClientRegistryVisita.Create;
     {$ELSE}
     {$ifdef FONACOT}
     ClientRegistry := TClientFonacotRegistry.Create;
     {$ELSE}
     {$ifdef CONCILIA}
     ClientRegistry := TClientIMSSRegistry.Create;
     {$ELSE}
     ClientRegistry := TZetaClientRegistry.Create;
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
end;

procedure ClearClientRegistry;
begin
     FreeAndNil( ClientRegistry );
end;

{$ifdef HTTP_CONNECTION}
function Decrypt( const sSource: String ): String;
var
   sKey: String;
   iKeyLen, iKeyPos, iOffset, iSrcPos, iSrcAsc, iTmpSrcAsc: Integer;
begin
     Result := '';
     if StrLleno( sSource ) then
     begin
          {$ifndef DOTNET}
          try
          {$endif}
             sKey := K_KEY;
             iKeyLen := Length( sKey );
             iKeyPos := 0;
             iOffset := StrToInt( '$' + Copy( sSource, 1, 2 ) );
             iSrcPos := 3;
             repeat
                   iSrcAsc := StrToInt( '$' + Copy( sSource, iSrcPos, 2 ) );
                   if ( iKeyPos < iKeyLen ) then
                      iKeyPos := iKeyPos + 1
                   else
                       iKeyPos := 1;
                   iTmpSrcAsc := iSrcAsc xor Ord( sKey[ iKeyPos ] );
                   if ( iTmpSrcAsc <= iOffset ) then
                      iTmpSrcAsc := 255 + iTmpSrcAsc - iOffset
                   else
                       iTmpSrcAsc := iTmpSrcAsc - iOffset;
                   Result := Result + Chr( iTmpSrcAsc );
                   iOffset := iSrcAsc;
                   iSrcPos := iSrcPos + 2;
             until ( iSrcPos >= Length( sSource ) );
          {$ifndef DOTNET}
          except
          end;
          {$endif}
     end;
end;

function Encrypt( const sSource: String ): String;
var
   sKey: String;
   iKeyLen, iKeyPos, iOffset, iSrcPos, iSrcAsc, iRange: Integer;
begin
     if StrLleno( sSource ) then
     begin
          sKey := K_KEY;
          iKeyLen := Length( sKey );
          iKeyPos := 0;
          iRange := 256;
          Randomize;
          iOffset := Random( iRange );
          Result := Format( '%1.2x', [ iOffset ] );
          for iSrcPos := 1 to Length( sSource ) do
          begin
               iSrcAsc := ( Ord( sSource[ iSrcPos ] ) + iOffset ) mod 255;
               if ( iKeyPos < iKeyLen ) then
                  iKeyPos := iKeyPos + 1
               else
                   iKeyPos := 1;
               iSrcAsc := iSrcAsc xor Ord( sKey[ iKeyPos ] );
               Result := Result + Format( '%1.2x',[ iSrcAsc ] );
               iOffset := iSrcAsc;
          end;
     end
     else
         Result := '';
end;
{$endif}

{ ************ TZetaRegistry *********** }

constructor TZetaClientRegistry.Create;
begin
     FRegistry := TRegIniFile.Create;
     with FRegistry do
     begin
          RootKey := HKEY_LOCAL_MACHINE;
          LazyWrite := False;
          FCanWrite := OpenKey( CLIENT_REGISTRY_PATH, TRUE );
          if not FCanWrite then
             OpenKeyReadOnly( CLIENT_REGISTRY_PATH );
     end;
     {$ifdef TRESS_DELPHIXE5_UP}
     // Registry del usuario
     FRegistryUser := TRegIniFile.Create;
     with FRegistryUser do
     begin
          //RootKey := HKEY_LOCAL_MACHINE;    // Al migrar las aplicaciones a Delphi XE5 se cambia a usar el registry del usuario: HKEY_CURRENT_USER que es el default
          LazyWrite := False;
          FCanWriteUser := OpenKey( CLIENT_REGISTRY_PATH, TRUE );
          if not FCanWriteUser then
             OpenKeyReadOnly( CLIENT_REGISTRY_PATH );
     end;
     {$endif}
end;

destructor TZetaClientRegistry.Destroy;
begin
     FRegistry.Free;
     {$ifdef TRESS_DELPHIXE5_UP}
     FRegistryUser.Free;
     {$endif}
     inherited Destroy;
end;

function TZetaClientRegistry.GetRegistry: TRegIniFile;
begin
     {$ifdef TRESS_DELPHIXE5_UP}
     Result := FRegistryUser;
     {$else}
     Result := FRegistry;
     {$endif}
end;

function TZetaClientRegistry.GetCanWrite: Boolean;
begin
     {$ifdef TRESS_DELPHIXE5_UP}
     Result := FCanWriteUser;
     {$else}
     Result := FCanWrite;
     {$endif}
end;

function TZetaClientRegistry.GetRegistryKey: String;
begin
     Result := FRegistry.CurrentPath;
end;

function TZetaClientRegistry.GetRegistryRoot: HKEY;
begin
     Result := FRegistry.RootKey;
end;

function TZetaClientRegistry.GetComputerName: String;
begin
     with FRegistry do
     begin
          Result := ReadString( '', SERVER_COMPUTER_NAME, '' );
     end;
end;

//DevEx: Leer Si el cliente desea ver la presentacion de bienvenida del Registry
function TZetaClientRegistry.GetVSPresentacion:String;
begin
     with GetRegistry do
     begin
          //Si el usuario no puede escribir en el registry, la presentacion no se mostrara
          if GetCanWrite then
             //El metodo ReadString ya cubre el que la llave no exista, en estos casos devolvera el valor por defecto que es el que mandamos en el ultimo parametro.
             Result := ReadString( '', VS_PRESENTACION, '0' )
          else
              Result := VERSION_COMPILACION; //Devolvera el mismo valor de la compilacion por lo tanto la presentacion no sera mostrada.
     end;
end;

//DevEx: Escribe en el Registry Si el cliente desea continuar viendo la presentacion de bienvenida la prox. vez que entre a la aplicacion.
procedure TZetaClientRegistry.SetVSPresentacion( Value: String);
begin
     with GetRegistry do
     begin
           //Se valida si el usuario puede o no escribir en el registry
           if GetCanWrite then
              WriteString( '', VS_PRESENTACION, Value );
     end;
end;

function TZetaClientRegistry.GetConsolaUsuario: String;
begin
     with FRegistry do
          Result := ReadString( '', CONSOLA_USR, '' );
end;

function TZetaClientRegistry.GetConsolaContrasena: String;
begin
     with FRegistry do
          Result := ReadString( '', CONSOLA_PWD, '' );
end;

function TZetaClientRegistry.GetInvocaApp: String;
begin
     with FRegistry do
          Result := ReadString( '', INVOCA_APP, '' );
end;

procedure TZetaClientRegistry.SetInvocaApp( const Value: String );
begin
     with FRegistry do
     begin
          WriteString( '', INVOCA_APP, Value );
     end;
end;

function TZetaClientRegistry.InitComputerName: Boolean;
begin
     with FRegistry do
     begin
          if ValueExists( SERVER_COMPUTER_NAME ) then
             Result := True
          else if FCanWrite then
              Result := BuscaServidor( TRUE )
          else
          begin
               ZetaDialogo.ZError( 'Acceso al Sistema', 'No tiene configurado Acceso' + CR_LF + 'Requiere derechos de escritura en Registry' + CR_LF + 'para realizar esta configuración', 0 );
               Result := FALSE;
          end;
     end;
end;


procedure TZetaClientRegistry.SetComputerName( const Value: String );
begin
     with FRegistry do
     begin
          WriteString( '', SERVER_COMPUTER_NAME, Value );
     end;
end;

{$ifdef TRESS_DELPHIXE5_UP}
{$ifdef WIN32}

function TZetaClientRegistry.LeeVirtualStore: Boolean;
const
     {$ifdef VISITANTES}
     CLIENT_REGISTRY_PATH_VS_32 = 'Software\Classes\VirtualStore\MACHINE\SOFTWARE\Grupo Tress\Visitantes';
     CLIENT_REGISTRY_PATH_VS_64 = 'Software\Classes\VirtualStore\MACHINE\SOFTWARE\Wow6432Node\Grupo Tress\Visitantes';
     {$else}
     CLIENT_REGISTRY_PATH_VS_32 = 'Software\Classes\VirtualStore\MACHINE\SOFTWARE\Grupo Tress\TressWin\Client';
     CLIENT_REGISTRY_PATH_VS_64 = 'Software\Classes\VirtualStore\MACHINE\SOFTWARE\Wow6432Node\Grupo Tress\TressWin\Client';
     {$endif}
{
    OpenKey(HKEY_LOCAL_MACHINE, r'SOFTWARE\Interface Software\ConnMgr', 0, KEY_READ)
    OpenKey(HKEY_CURRENT_USER, r'Software\Classes\VirtualStore\MACHINE\SOFTWARE\Interface Software\ConnMgr', 0, KEY_READ)
}
var
    RegistryVS: TRegIniFile;
    sComputerName: String;
begin
//     Result := FALSE;
     RegistryVS := TRegIniFile.Create;
     try
        with RegistryVS do
        begin
             RootKey := HKEY_CURRENT_USER;
             if ( TOSVersion.Architecture = arIntelX86 ) then
                Result := OpenKeyReadOnly( CLIENT_REGISTRY_PATH_VS_32 )
             else
                 Result := OpenKeyReadOnly( CLIENT_REGISTRY_PATH_VS_64 );
             if Result then
             begin
                  sComputerName := ReadString( '', SERVER_COMPUTER_NAME, '' );
                  Result := strLleno( sComputerName );
                  if Result then
                  begin
                       ComputerName := sComputerName;
                       {$ifdef HTTP_CONNECTION}
                       TipoConexion := TTipoConexion( ReadInteger( VACIO, TIPO_CONEXION, Ord( conxDCOM ) ) );
                       URL := ReadString( VACIO, SERVER_URL, VACIO );
                       UserName := ReadString( VACIO, SERVER_USERNAME, VACIO );
                       Password := Decrypt( ReadString( VACIO, SERVER_PASSWORD, VACIO ) );
                       {$endif}
                       {$ifdef VISITANTES}
                       TClientRegistryVisita(self).CasetaDefault := ReadString( VACIO, CASETA_DEFAULT, VACIO );
                       TClientRegistryVisita(self).ImpresionGafete := ReadInteger( VACIO, IMPRESION_GAFETE, 0 );
                       TClientRegistryVisita(self).MostrarGridCitas := ReadBool( VACIO, MOSTRAR_GRID_CITAS, FALSE );
                       TClientRegistryVisita(self).ImpresoraDefault := ReadString( VACIO, IMPRESORA_DEFAULT, VACIO );
                       TClientRegistryVisita(self).FiltrarTiempo := ReadBool( VACIO, FILTRAR_TIEMPO, FALSE );
                       TClientRegistryVisita(self).MinutosAntes := ReadInteger( VACIO, MIN_ANTES, 20 );
                       TClientRegistryVisita(self).MinutosDespues := ReadInteger( VACIO, MIN_DESPUES, 20 );
                       TClientRegistryVisita(self).AutoReg_AsuntoATratar := ReadBool( VACIO, AUTOREG_ASUNTO, TRUE );
                       TClientRegistryVisita(self).AutoReg_Vehiculo := ReadBool( VACIO, AUTOREG_VEH , TRUE );
                       TClientRegistryVisita(self).AutoReg_Identificacion := ReadBool( VACIO, AUTOREG_IDENT , TRUE );
                       TClientRegistryVisita(self).AutoReg_Observaciones := ReadBool( VACIO, AUTOREG_OBS, TRUE );
                       {$endif}
                  end;
             end;
        end;
     finally
            RegistryVS.Free;
     end;
end;

{$endif}
{$endif}

function TZetaClientRegistry.BuscaServidor( const lInitVirtual: Boolean ): Boolean;
begin
     {$ifdef TRESS_DELPHIXE5_UP}
     {$ifdef WIN32}
     Result := ( lInitVirtual and GetIsWindowsVista and LeeVirtualStore );
     if ( not Result ) then
     {$endif}
     {$endif}
     //DevEX: Al invocar aqui un cambio de servidor el usuario no ha hecho LogIn
     with TFindServidor_DevEx.Create( Application ) do
     begin
          try
             Servidor := ComputerName;
             {$ifdef HTTP_CONNECTION}
             ConnectionType := Self.TipoConexion;
             WebPage := Self.URL;
             WebUser := Self.UserName;
             WebPassword := Self.Password;
             {$endif}
             if ( ShowModal = mrOk ) then
             begin
                  ComputerName := Servidor;
                  {$ifdef HTTP_CONNECTION}
                  Self.TipoConexion := ConnectionType;
                  Self.URL := WebPage;
                  Self.UserName := WebUser;
                  Self.Password := WebPassword;
                  {$endif}
                  Result := True;
             end
             else
                 Result := False;
          finally
                 Free;
          end;
     end;
end;

procedure TZetaClientRegistry.SetCompania( const Value: String );
begin
     FCompania := Value;
end;

function TZetaClientRegistry.GetCacheSistemaActive: Boolean;
begin
     with FRegistry do
     begin
          Result := ReadBool( CLIENT_REGISTRY_SISTEMA_PATH, FILES_CACHE_ACTIVE, FALSE );
     end;
end;

procedure TZetaClientRegistry.SetCacheSistemaActive(const Value: Boolean);
begin
     with FRegistry do
     begin
          WriteBool( CLIENT_REGISTRY_SISTEMA_PATH, FILES_CACHE_ACTIVE, Value );
     end;
end;

function TZetaClientRegistry.GetCacheSistemaFolder: String;
begin
     with FRegistry do
     begin
          Result := ReadString( CLIENT_REGISTRY_SISTEMA_PATH, FILES_CACHE_FOLDER, '' );
     end;
end;

procedure TZetaClientRegistry.SetCacheSistemaFolder(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( CLIENT_REGISTRY_SISTEMA_PATH, FILES_CACHE_FOLDER, Value );
     end;
end;

function TZetaClientRegistry.GetCacheActive: Boolean;
begin
     with FRegistry do
     begin
          Result := ReadBool( FCompania, FILES_CACHE_ACTIVE, FALSE );
     end;
end;

procedure TZetaClientRegistry.SetCacheActive( const Value: Boolean );
begin
     with FRegistry do
     begin
          WriteBool( FCompania, FILES_CACHE_ACTIVE, Value );
     end;
end;

function TZetaClientRegistry.GetCacheFolder: String;
begin
     with FRegistry do
     begin
          Result := ReadString( FCompania, FILES_CACHE_FOLDER, '' );
     end;
end;

procedure TZetaClientRegistry.SetCacheFolder( const Value: String );
begin
     with FRegistry do
     begin
          WriteString( FCompania, FILES_CACHE_FOLDER, Value );
     end;
end;

function TZetaClientRegistry.GetCacheFecha: String;
begin
     with FRegistry do
     begin
          Result := ReadString( FCompania, FILES_CACHE_FECHA, '' );
          if StrVacio( Result ) then
             Result := 'No se Ha Refrescado';
     end;
end;

procedure TZetaClientRegistry.SetCacheFecha( const Value: String );
begin
     with FRegistry do
     begin
          WriteString( FCompania, FILES_CACHE_FECHA, Value );
     end;
end;

function TZetaClientRegistry.GetLastEmpresa( const iUsuario: Integer ): String;
begin
     with GetRegistry do
     begin
          Result := ReadString( CLIENT_LAST_COMPANY, IntToStr( iUsuario ), ZetaCommonClasses.VACIO );
     end;
end;

procedure TZetaClientRegistry.SetLastEmpresa( const iUsuario: Integer; const sEmpresa: String );
begin
     if GetCanWrite then
     begin
 	  with GetRegistry do
          begin
               WriteString( CLIENT_LAST_COMPANY, IntToStr( iUsuario ), sEmpresa );
          end;
     end;
end;

//extraer el registry de tipo de nomina
function TZetaClientRegistry.GetTipoNomina: Integer;
begin
     with GetRegistry do
     begin
          // 1 es semanal, se pone constante para no tener que ligar ZetaCommonLists
          Result := ReadInteger( FCompania, CLIENT_TIPO_NOMINA, 1 );
     end;
end;

procedure TZetaClientRegistry.SetTipoNomina(const Value: integer);
begin
     if GetCanWrite then
     begin
          with GetRegistry do
          begin
               WriteInteger( FCompania, CLIENT_TIPO_NOMINA, Value );
          end;
     end;
end;

// Registro patronal en el registry
function TZetaClientRegistry.GetRegPatronal: String;
begin
     with GetRegistry do
     begin
          Result := ReadString( FCompania, CLIENT_REG_PATRONAL, '' );
     end;
end;

procedure TZetaClientRegistry.SetRegPatronal( const Value: String );
begin
     if GetCanWrite then
     begin
          with GetRegistry do
          begin
               WriteString( FCompania, CLIENT_REG_PATRONAL, Value );
          end;
     end;
end;

// Registro patronal en el registry
function TZetaClientRegistry.GetFiltroCursosProgramados: String;
begin
     with GetRegistry do
     begin
          Result := ReadString( FCompania, CLIENT_FILTRO_CURSOPROG, '1,2,3,4' );
     end;
end;

procedure TZetaClientRegistry.SetFiltroCursosProgramados( const Value: String );
begin
     if GetCanWrite then
     begin
          with GetRegistry do
          begin
               WriteString( FCompania, CLIENT_FILTRO_CURSOPROG, Value );
          end;
     end;
end;

procedure TZetaClientRegistry.SetAccesosDirecto (const numAcceso : Integer ; const Value: Integer);
begin
     if GetCanWrite then
     begin
          with GetRegistry do
          begin
               WriteInteger ( CLIENT_ACCESOS_DIRECTOS, TACCESO_DIRECTO + IntToStr(numAcceso) , Value );
          end;
     end;
end;
function TZetaClientRegistry.GetAccesoDirecto(const numAcceso:Integer): Integer;
begin
     Result := -1; //Regresamos el valor Default
     with GetRegistry do
     begin
          if GetCanWrite then
             Result := ReadInteger( CLIENT_ACCESOS_DIRECTOS, TACCESO_DIRECTO + IntToStr(numAcceso), -1 );
     end;
end;

// Mostrar Pantalla Inicio
procedure TZetaClientRegistry.SetHomeSistema(const Value: Boolean);
begin
     if GetCanWrite then
     begin
          with GetRegistry do
          begin
              WriteBool( VACIO, THOME_SISTEMA, Value );
          end;
     end;
end;

function TZetaClientRegistry.GetHomeSistema: Boolean;
begin
     with GetRegistry do
     begin
          if GetCanWrite then
             Result := ReadBool( VACIO, THOME_SISTEMA, TRUE )
          else
              Result := FALSE;
     end;

end;


{ TClientRegistryVisita }

{$ifdef VISITANTES}
function TClientRegistryVisita.GetAutoReg_AsuntoATratar: boolean;
begin
     with FRegistry do
     begin
          Result := ReadBool( VACIO, AUTOREG_ASUNTO, TRUE );
     end;
end;

function TClientRegistryVisita.GetAutoReg_Identificacion: boolean;
begin
     with FRegistry do
     begin
          Result := ReadBool( VACIO, AUTOREG_IDENT , TRUE );
     end;
end;

function TClientRegistryVisita.GetAutoReg_Observaciones: boolean;
begin
     with FRegistry do
     begin
          Result := ReadBool( VACIO, AUTOREG_OBS, TRUE );
     end;
end;

function TClientRegistryVisita.GetAutoReg_Vehiculo: boolean;
begin
     with FRegistry do
     begin
          Result := ReadBool( VACIO, AUTOREG_VEH , TRUE );
     end;
end;

function TClientRegistryVisita.GetCasetaDefault: string;
begin
     with FRegistry do
     begin
          Result := ReadString( VACIO, CASETA_DEFAULT, VACIO );
     end;
end;

function TClientRegistryVisita.GetImpresionGafete: integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( VACIO, IMPRESION_GAFETE, 0 );
     end;
end;

function TClientRegistryVisita.GetImpresoraDefault: string;
begin
     with FRegistry do
     begin
          Result := ReadString( VACIO, IMPRESORA_DEFAULT, VACIO );
     end;
end;

function TClientRegistryVisita.GetMinutosAntes: Integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( VACIO, MIN_ANTES, 20 );
     end;
end;

function TClientRegistryVisita.GetMinutosDespues: Integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( VACIO, MIN_DESPUES, 20 );
     end;
end;

function TClientRegistryVisita.GetFiltrarTiempo: Boolean;
begin
     with FRegistry do
     begin
          Result := ReadBool( VACIO, FILTRAR_TIEMPO, FALSE );
     end;
end;

function TClientRegistryVisita.GetMostrarGridAvanzado: Boolean;
begin
    with FRegistry do
     begin
          Result := ReadBool( VACIO, MOSTRAR_GRID_AVANZADO, TRUE );
     end;
end;

function TClientRegistryVisita.GetMostrarGridCitas: Boolean;
begin
     with FRegistry do
     begin
          Result := ReadBool( VACIO, MOSTRAR_GRID_CITAS, FALSE );
     end;
end;

function TClientRegistryVisita.GetMostrarRegistroGrupo: Boolean;
begin
     with FRegistry do
     begin
          Result := ReadBool( VACIO, MOSTRAR_REGISTRO_GRUPO, FALSE );
     end;
end;

procedure TClientRegistryVisita.SetAutoReg_AsuntoATratar(const Value: boolean);
begin
     with FRegistry do
     begin
          WriteBool( VACIO, AUTOREG_ASUNTO, Value );
     end;
end;

procedure TClientRegistryVisita.SetAutoReg_Identificacion(const Value: boolean);
begin
     with FRegistry do
     begin
          WriteBool( VACIO, AUTOREG_IDENT, Value );
     end;
end;

procedure TClientRegistryVisita.SetAutoReg_Observaciones(const Value: boolean);
begin
     with FRegistry do
     begin
          WriteBool( VACIO, AUTOREG_OBS, Value );
     end;
end;

procedure TClientRegistryVisita.SetAutoReg_Vehiculo(const Value: boolean);
begin
     with FRegistry do
     begin
          WriteBool( VACIO, AUTOREG_VEH, Value );
     end;
end;

procedure TClientRegistryVisita.SetCasetaDefault(const Value: string);
begin
     with FRegistry do
     begin
          WriteString( VACIO, CASETA_DEFAULT, Value );
     end;
end;

procedure TClientRegistryVisita.SetFiltrarTiempo(const Value: Boolean);
begin
     with FRegistry do
     begin
          WriteBool( VACIO, FILTRAR_TIEMPO, Value );
     end;
end;

procedure TClientRegistryVisita.SetImpresionGafete(const Value: integer);
begin
     with FRegistry do
     begin
          WriteInteger( VACIO, IMPRESION_GAFETE, Value );
     end;
end;

procedure TClientRegistryVisita.SetImpresoraDefault(const Value: string);
begin
     with FRegistry do
     begin
          WriteString( VACIO, IMPRESORA_DEFAULT, Value );
     end;
end;

procedure TClientRegistryVisita.SetMinutosAntes(const Value: Integer);
begin
     with FRegistry do
     begin
          WriteInteger( VACIO, MIN_ANTES, Value );
     end;
end;

procedure TClientRegistryVisita.SetMinutosDespues(const Value: Integer);
begin
     with FRegistry do
     begin
          WriteInteger( VACIO, MIN_DESPUES, Value );
     end;
end;

procedure TClientRegistryVisita.SetMostrarGridAvanzado(const Value: Boolean);
begin
    with FRegistry do
     begin
          WriteBool( VACIO, MOSTRAR_GRID_AVANZADO, Value );
     end;
end;

procedure TClientRegistryVisita.SetMostrarGridCitas(const Value: Boolean);
begin
     with FRegistry do
     begin
          WriteBool( VACIO, MOSTRAR_GRID_CITAS, Value );
     end;
end;

procedure TClientRegistryVisita.SetMostrarRegistroGrupo(const Value: Boolean);
begin
     with FRegistry do
     begin
          WriteBool( VACIO, MOSTRAR_REGISTRO_GRUPO, Value );
     end;
end;

{$endif}

{$ifdef HTTP_CONNECTION}
function TZetaClientRegistry.GetPassword: String;
begin
     with FRegistry do
     begin
          Result := Decrypt( ReadString( VACIO, SERVER_PASSWORD, VACIO ) );
     end;
end;

function TZetaClientRegistry.GetTipoConexion: TTipoConexion;
begin
     with FRegistry do
     begin
          Result := TTipoConexion( ReadInteger( VACIO, TIPO_CONEXION, Ord( conxDCOM ) ) );
     end;
end;

{$ifdef antes}
function TZetaClientRegistry.GetURL: String;
begin
     with FRegistry do
     begin
          Result := ReadString( VACIO, SERVER_URL, VACIO );
     end;
end;
{$endif}

function TZetaClientRegistry.GetURL: String;
const
     TRESS_HOST_NAME = 'TressHTTPSrvr.dll';
var
   iPos: integer;
begin
     with FRegistry do
     begin
          {Alternativa # 2 }
          Result := ReadString( VACIO, SERVER_URL, VACIO );
          iPos := Pos( '.dll', Result );
          if ( iPos > 0 ) then
          begin
               Dec( iPos );
               while ( Result[iPos] <> '/' ) and ( iPos > 0 ) do
                     Dec( iPos );

               Result := Copy( Result, 0, iPos );
          end;
          Result := ZetaCommonTools.VerificaDirURL( Result ) + TRESS_HOST_NAME
     end;
end;

function TZetaClientRegistry.GetUserName: String;
begin
     with FRegistry do
     begin
          Result := ReadString( VACIO, SERVER_USERNAME, VACIO );
     end;
end;

procedure TZetaClientRegistry.SetPassword(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( VACIO, SERVER_PASSWORD, Encrypt( Value ) );
     end;
end;

procedure TZetaClientRegistry.SetTipoConexion(const Value: TTipoConexion);
begin
     with FRegistry do
     begin
          WriteInteger( VACIO, TIPO_CONEXION, Ord( Value ) );
     end;
end;

procedure TZetaClientRegistry.SetURL(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( VACIO, SERVER_URL, Value );
     end;
end;

procedure TZetaClientRegistry.SetUserName(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( VACIO, SERVER_USERNAME, Value );
     end;
end;

{$endif}

{$ifdef FONACOT}

{ ******** TClientFonacotRegistry ********* }

function TClientFonacotRegistry.GetRutaActualizaPrestamos: String;
begin
     Result := GetRegistry.ReadString( K_BITACORA, K_RUTA_BITACORA_ACTUALIZA_PRESTAMOS, '' );
end;

function TClientFonacotRegistry.GetRutaImportarKardex: String;
begin
     Result := GetRegistry.ReadString( K_BITACORA, K_RUTA_BITACORA_IMPORTAR_KARDEX, '' );
end;

function TClientFonacotRegistry.GetRutaImportarPrestamos: String;
begin
    Result := GetRegistry.ReadString( K_BITACORA, K_RUTA_BITACORA_IMPORTAR_PRESTAMOS, '' );
end;

function TClientFonacotRegistry.GetRutaUltimaCedula: String;
begin
     Result := GetRegistry.ReadString( FCompania, K_RUTA_ULTIMA_CEDULA, '' );
end;

function TClientFonacotRegistry.GetRutaUltimaCedulaConfronta: String;
begin
     Result := GetRegistry.ReadString( FCompania, K_RUTA_ULTIMA_CEDULA_CONFRONTA, '' );
end;

function TClientFonacotRegistry.GetRazonSocialConciliacion: String;
begin
     Result := GetRegistry.ReadString( FCompania, K_RAZON_SOCIAL_CONCILIACION, '' );
end;

function TClientFonacotRegistry.GetRazonSocialConfronta: String;
begin
     Result := GetRegistry.ReadString( FCompania, K_RAZON_SOCIAL_CONFRONTA, '' );
end;

procedure TClientFonacotRegistry.SetRutaActualizaPrestamos(const Value: String);
begin
     GetRegistry.WriteString( K_BITACORA, K_RUTA_BITACORA_ACTUALIZA_PRESTAMOS, Value );
end;

procedure TClientFonacotRegistry.SetRutaImportarKardex(const Value: String);
begin
     GetRegistry.WriteString( K_BITACORA, K_RUTA_BITACORA_IMPORTAR_KARDEX, Value );
end;

procedure TClientFonacotRegistry.SetRutaImportarPrestamos(const Value: String);
begin
     GetRegistry.WriteString( K_BITACORA, K_RUTA_BITACORA_IMPORTAR_PRESTAMOS, Value );
end;

procedure TClientFonacotRegistry.SetRutaUltimaCedula(const Value: String);
begin
     GetRegistry.WriteString( FCompania, K_RUTA_ULTIMA_CEDULA, Value );
end;

procedure TClientFonacotRegistry.SetRutaUltimaCedulaConfronta(const Value: String);
begin
     GetRegistry.WriteString( FCompania, K_RUTA_ULTIMA_CEDULA_CONFRONTA, Value );
end;

procedure TClientFonacotRegistry.SetRazonSocialConciliacion(const Value: String);
begin
     GetRegistry.WriteString( FCompania, K_RAZON_SOCIAL_CONCILIACION, Value );
end;

procedure TClientFonacotRegistry.SetRazonSocialConfronta(const Value: String);
begin
     GetRegistry.WriteString( FCompania, K_RAZON_SOCIAL_CONFRONTA, Value );
end;

{$endif}

{$ifdef CONCILIA}

{ ******** TClientIMSSRegistry ********* }

function TClientIMSSRegistry.GetConciliadorIMSSPatron: String;
begin
     Result := GetRegistry.ReadString( FCompania, CONCILIADOR_IMSS_PATRON, VACIO);
end;

function TClientIMSSRegistry.GetConciliadorIMSSYear: String;
begin
     Result := GetRegistry.ReadString( FCompania, CONCILIADOR_IMSS_YEAR ,VACIO);
end;

function TClientIMSSRegistry.GetConciliadorIMSSMes: String;
begin
     Result := GetRegistry.ReadString( FCompania, CONCILIADOR_IMSS_MES, VACIO);
end;

function TClientIMSSRegistry.GetConciliadorIMSSTipo: String;
begin
     Result := GetRegistry.ReadString( FCompania, CONCILIADOR_IMSS_TIPO, VACIO);
end;

function TClientIMSSRegistry.GetConciliadorIMSSTipoConciliacion: String;
begin
     Result := GetRegistry.ReadString( FCompania, CONCILIADOR_IMSS_TIPO_CONCILIACION, VACIO);
end;

function TClientIMSSRegistry.GetConciliadorIMSSBaseDatosSUA: String;
begin
     Result := GetRegistry.ReadString( FCompania, CONCILIADOR_IMSS_BASE_DATOS_SUA, VACIO);
end;

function TClientIMSSRegistry.GetConciliadorIMSSDirectorioIMSS: String;
begin
     Result := GetRegistry.ReadString( FCompania, CONCILIADOR_IMSS_DIRECTORIO_IMSS, VACIO);
end;

function TClientIMSSRegistry.GetConciliadorIMSSClaseEmision: String;
begin
     Result := GetRegistry.ReadString( FCompania, CONCILIADOR_IMSS_CLASE_EMISION, VACIO);
end;

procedure TClientIMSSRegistry.SetConciliadorIMSSPatron(const Value: String);
begin
     GetRegistry.WriteString( FCompania, CONCILIADOR_IMSS_PATRON, Value);
end;
procedure TClientIMSSRegistry.SetConciliadorIMSSYear(const Value: String);
begin
     GetRegistry.WriteString( FCompania, CONCILIADOR_IMSS_YEAR, Value);
end;
procedure TClientIMSSRegistry.SetConciliadorIMSSMes(const Value: String);
begin
     GetRegistry.WriteString( FCompania, CONCILIADOR_IMSS_MES, Value);
end;

procedure TClientIMSSRegistry.SetConciliadorIMSSTipo(const Value: String);
begin
     GetRegistry.WriteString( FCompania, CONCILIADOR_IMSS_TIPO, Value);
end;
procedure TClientIMSSRegistry.SetConciliadorIMSSTipoConciliacion(const Value: String);
begin
     GetRegistry.WriteString( FCompania, CONCILIADOR_IMSS_TIPO_CONCILIACION, Value);
end;

procedure TClientIMSSRegistry.SetConciliadorIMSSBaseDatosSUA(const Value: String);
begin
     GetRegistry.WriteString( FCompania, CONCILIADOR_IMSS_BASE_DATOS_SUA, Value);
end;

procedure TClientIMSSRegistry.SetConciliadorIMSSDirectorioIMSS(const Value: String);
begin
     GetRegistry.WriteString( FCompania, CONCILIADOR_IMSS_DIRECTORIO_IMSS, Value);
end;

procedure TClientIMSSRegistry.SetConciliadorIMSSClaseEmision(const Value: String);
begin
     GetRegistry.WriteString( FCompania, CONCILIADOR_IMSS_CLASE_EMISION, Value);
end;
{$endif}

{$ifdef TIMBRADO}
function TZetaClientRegistry.GetTimbradoURL: String;
begin
     with FRegistry do
     begin
          Result := ReadString( VACIO, TIMBRADO_URL , TIMBRADO_DEFAULT_URL );
     end;
end;

procedure TZetaClientRegistry.SetTimbradoURL(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( VACIO, TIMBRADO_URL, Value );
     end;
end;

function TZetaClientRegistry.GetRecibosPath: String;


 function GetMyDocumentsDir: string;
 var
    r: Bool;
    path: array[0..Max_Path] of Char;
 begin
    r := ShGetSpecialFolderPath(0, path, CSIDL_Personal, False) ;
    if r then
       Result := Path
    else
       Result := TIMBRADO_RECIBOS_PATH_DEFAULT;

 end;

begin
    with FRegistry do
     begin
          Result := ReadString( VACIO, TIMBRADO_RECIBOS_PATH , GetMyDocumentsDir );
     end;
end;

procedure TZetaClientRegistry.SetRecibosPath(const Value: String);
begin
 with FRegistry do
     begin
          WriteString( VACIO, TIMBRADO_RECIBOS_PATH , Value );
     end;
end;

function TZetaClientRegistry.GetTimbradoPaqueteSize: integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( VACIO, TIMBRADO_PAQUETE_SIZE, TIMBRADO_PAQUETE_SIZE_DEFAULT );
          if ( Result <= 0 ) then
             Result := TIMBRADO_PAQUETE_SIZE_DEFAULT;
     end;
end;

procedure TZetaClientRegistry.SetTimbradoPaqueteSize(const Value: integer);
begin
     if FcanWrite then
     begin
          with FRegistry do
          begin
               WriteInteger( VACIO, TIMBRADO_PAQUETE_SIZE, Value );
          end;
     end;
end;

//Intento de Timbrado de Nomina
function TZetaClientRegistry.GetTimbradoPaqueteTimeoutConnect: integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( VACIO, TIMBRADO_PAQUETE_TIMEOUT_CONNECT, TIMBRADO_PAQUETE_TIMEOUT_CONNECT_DEFAULT );
          if ( Result <= 0 ) then
             Result := TIMBRADO_PAQUETE_TIMEOUT_CONNECT_DEFAULT;
     end;
end;

procedure TZetaClientRegistry.SetTimbradoPaqueteTimeoutConnect(const Value: integer);
begin
     if FcanWrite then
     begin
          with FRegistry do
          begin
               WriteInteger( VACIO, TIMBRADO_PAQUETE_TIMEOUT_CONNECT, Value );
          end;
     end;
end;

function TZetaClientRegistry.GetTimbradoPaqueteTimeoutSend: integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( VACIO, TIMBRADO_PAQUETE_TIMEOUT_SEND, TIMBRADO_PAQUETE_TIMEOUT_SEND_DEFAULT );
          if ( Result <= 0 ) then
             Result := TIMBRADO_PAQUETE_TIMEOUT_SEND_DEFAULT;
     end;
end;

procedure TZetaClientRegistry.SetTimbradoPaqueteTimeoutSend(const Value: integer);
begin
     if FcanWrite then
     begin
          with FRegistry do
          begin
               WriteInteger( VACIO, TIMBRADO_PAQUETE_TIMEOUT_SEND, Value );
          end;
     end;
end;

function TZetaClientRegistry.GetTimbradoPaqueteTimeoutReceive: integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( VACIO, TIMBRADO_PAQUETE_TIMEOUT_RECEIVE, TIMBRADO_PAQUETE_TIMEOUT_RECEIVE_DEFAULT );
          if ( Result <= 0 ) then
             Result := TIMBRADO_PAQUETE_TIMEOUT_RECEIVE_DEFAULT;
     end;
end;

procedure TZetaClientRegistry.SetTimbradoPaqueteTimeoutReceive(const Value: integer);
begin
     if FcanWrite then
     begin
          with FRegistry do
          begin
               WriteInteger( VACIO, TIMBRADO_PAQUETE_TIMEOUT_RECEIVE, Value );
          end;
     end;
end;

function TZetaClientRegistry.GetTimbradoPaqueteIntentos: integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( VACIO, TIMBRADO_PAQUETE_INTENTOS, TIMBRADO_PAQUETE_INTENTOS_DEFAULT );
          if ( Result <= 0 ) then
             Result := TIMBRADO_PAQUETE_INTENTOS_DEFAULT;
     end;
end;

procedure TZetaClientRegistry.SetTimbradoPaqueteIntentos(const Value: integer);
begin
     if FcanWrite then
     begin
          with FRegistry do
          begin
               WriteInteger( VACIO, TIMBRADO_PAQUETE_INTENTOS, Value );
          end;
     end;
end;

function TZetaClientRegistry.GetTimbradoPaqueteEspera: integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( VACIO, TIMBRADO_PAQUETE_ESPERA, TIMBRADO_PAQUETE_ESPERA_DEFAULT );
          if ( Result <= 0 ) then
             Result := TIMBRADO_PAQUETE_ESPERA_DEFAULT;
     end;
end;

procedure TZetaClientRegistry.SetTimbradoPaqueteEspera(const Value: integer);
begin
     if FcanWrite then
     begin
          with FRegistry do
          begin
               WriteInteger( VACIO, TIMBRADO_PAQUETE_ESPERA, Value );
          end;
     end;
end;

{$endif}

end.

