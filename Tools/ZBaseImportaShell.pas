unit ZBaseImportaShell;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseShell, ExtCtrls, ComCtrls, StdCtrls, Buttons, Psock, NMsmtp,
  ImgList, Menus, ActnList;

type
  TBaseImportaShell = class(TBaseShell)
    ActionList: TActionList;
    _A_OtraEmpresa: TAction;
    _A_Servidor: TAction;
    _A_SalirSistema: TAction;
    _P_Procesar: TAction;
    _P_Cancelar: TAction;
    _H_AcercaDe: TAction;
    ShellMenu: TMainMenu;
    Archivo1: TMenuItem;
    ArchivoOtraEmpresa: TMenuItem;
    ArchivoServidor: TMenuItem;
    N2: TMenuItem;
    CancelarProceso1: TMenuItem;
    CancelarProceso2: TMenuItem;
    N1: TMenuItem;
    ArchivoSalir: TMenuItem;
    Ayuda: TMenuItem;
    AcercaDe1: TMenuItem;
    OpenDialog: TOpenDialog;
    ArbolImages: TImageList;
    Email: TNMSMTP;
    PageControl: TPageControl;
    TabBitacora: TTabSheet;
    MemBitacora: TMemo;
    PanelBottom: TPanel;
    BitacoraSeek: TSpeedButton;
    LblGuardarBIT: TLabel;
    ArchBitacora: TEdit;
    TabErrores: TTabSheet;
    Panel1: TPanel;
    ErroresSeek: TSpeedButton;
    LblGuardarERR: TLabel;
    ArchErrores: TEdit;
    MemErrores: TMemo;
    PanelBotones: TPanel;
    BtnProcesar: TBitBtn;
    BtnDetener: TBitBtn;
    BtnSalir: TBitBtn;
    PanelEncabezado: TPanel;
    LblArchivo: TLabel;
    ArchivoSeek: TSpeedButton;
    SistemaFechaDia: TLabel;
    ArchOrigen: TEdit;
    _Per_GenerarPoliza: TAction;
    procedure _A_OtraEmpresaExecute(Sender: TObject);
    procedure _A_ServidorExecute(Sender: TObject);
    procedure _A_SalirSistemaExecute(Sender: TObject);
    procedure _P_ProcesarExecute(Sender: TObject);
    procedure _P_CancelarExecute(Sender: TObject);
    procedure _H_AcercaDeExecute(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure BitacoraSeekClick(Sender: TObject);
    procedure ErroresSeekClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure _Per_GenerarPolizaExecute(Sender: TObject);
  private
    { Private declarations }
    FModoBatch: Boolean;
    FEnviarCorreo: boolean;    
    FBatchLog: TStrings;
    FMinParamsBatch : Integer;
    FSintaxisBatch  : String;
    FLogNameBatch   : String;
    function LoginBatch: Boolean;
    function AbreEmpresaBatch: Boolean;
    procedure ManejaExcepcionLog(Sender: TObject; Error: Exception);
    procedure EnviarCorreoBitacoras;
    procedure GrabaBitacora(oMemo: TMemo; const sArchivo: String);
    procedure AgregaBitacoraBatch(const sMensaje: String);
  protected
    procedure InitBitacora;
    procedure EscribeBitacoras; virtual;
    procedure ActivaControles( const lProcesando: Boolean ); virtual;
    procedure SetControlesBatch; virtual;
    procedure ProcesarArchivo; virtual;
    procedure CancelarProceso; virtual;
    procedure DoProcesar; virtual; abstract;
    procedure ReportaErrorDialogo(const sTitulo, sMensaje: String);
  public
    { Public declarations }
    property MinParamsBatch : Integer read FMinParamsBatch write FMinParamsBatch;
    property SintaxisBatch : String read FSintaxisBatch write FSintaxisBatch;
    property LogNameBatch : String read FLogNameBatch write FLogNameBatch;
    property ModoBatch : Boolean read FModoBatch;
    property EnviarCorreo : Boolean write FEnviarCorreo;
    procedure EscribeBitacora( const sMensaje: String );
    procedure EscribeError( const sMensaje: String );
    procedure ProcesarBatch; virtual;
  end;

const
     K_PARAM_EMPRESA  = 1;
     K_PARAM_USUARIO  = 2;
     K_PARAM_PASSWORD = 3;
     K_PARAM_ARCHIVO  = 4;
     K_BATCH_LOG = 'Interfase.LOG';
     K_BATCH_SINTAXIS = '%s EMPRESA USUARIO PASSWORD ARCHIVO';

var
  BaseImportaShell: TBaseImportaShell;

implementation

uses dGlobal, dCliente, dDiccionario,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaCommonLists,
     ZetaMessages,
     ZGlobalTress,
     ZAsciiTools,
     ZetaDialogo,
     ZetaAcercaDe;

{$R *.DFM}

procedure TBaseImportaShell.FormCreate(Sender: TObject);
begin
     FEnviarCorreo := TRUE;
     FMinParamsBatch := K_PARAM_ARCHIVO;
     FSintaxisBatch := Format( K_BATCH_SINTAXIS, [ ExtractFileName( Application.ExeName ) ] );
     FLogNameBatch  := K_BATCH_LOG;

     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( Self );
     dmDiccionario := TdmDiccionario.Create( Self );
     FBatchLog := TStringList.Create;
     inherited;
end;

procedure TBaseImportaShell.FormDestroy(Sender: TObject);
begin
     inherited;
     FBatchLog.Free;
     dmCliente.Free;
     Global.Free;
end;

procedure TBaseImportaShell.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := TabBitacora;
     ActivaControles( FALSE );
end;

procedure TBaseImportaShell.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     if FModoBatch then
        ZAsciiTools.GrabaBitacora( FBatchLog, ZetaMessages.SetFileNameDefaultPath( FLogNameBatch ) );
end;

procedure TBaseImportaShell.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     CanClose := TRUE;
end;

procedure TBaseImportaShell.ManejaExcepcionLog(Sender: TObject; Error: Exception);
begin
     AgregaBitacoraBatch( Error.Message );
end;

{ Procesar Batch }

procedure TBaseImportaShell.ProcesarBatch;
var
   sNombreEmpresa: String;
begin
     Application.OnException := ManejaExcepcionLog;
     FModoBatch := TRUE;
     try
        if ( ParamCount >= FMinParamsBatch ) then   // Deben incluirse todos los parametros
        begin
             if LoginBatch and AbreEmpresaBatch then
             begin
                  SetControlesBatch;
                  sNombreEmpresa := dmCliente.GetDatosEmpresaActiva.Nombre;
                  AgregaBitacoraBatch( Format( 'Inici� Proceso = Empresa: %s - Archivo: %s - Fecha Default: %s', [ sNombreEmpresa, ArchOrigen.Text, FechaCorta( dmCliente.FechaDefault ) ] ) );
                  ProcesarArchivo;
                  if FEnviarCorreo then
                     EnviarCorreoBitacoras;
                  AgregaBitacoraBatch( Format( 'Termin� Proceso = Empresa: %s - Bit�cora de Cambios: %s - Errores: %s', [ sNombreEmpresa, ArchBitacora.Text, ArchErrores.Text ] ) );
             end;
        end
        else
            AgregaBitacoraBatch( Format( '� Faltan Par�metros ! - Sintaxis: %s', [ FSintaxisBatch ] ) );
     except
        on Error: Exception do
        begin
             Application.HandleException( Error );
        end;
     end;
     Close;         // Salir del Programa
end;

function TBaseImportaShell.LoginBatch: Boolean;
var
   eReply: eLogReply;
   lLoop, lSalirTress: Boolean;
   sMensajeErrorHTTP: String;
begin
     Result := FALSE;
     with dmCliente do
     begin
          eReply := GetUsuario( ParamStr( K_PARAM_USUARIO ), ParamStr( K_PARAM_PASSWORD ), TRUE );
          case eReply of
               lrOK: Result := TRUE;
               lrLoggedIn: AgregaBitacoraBatch( '� Usuario Ya Entr� Al Sistema !' + ' - Ya Hay Otro Usuario Con Esta Clave En El Sistema' );
               lrLockedOut: AgregaBitacoraBatch( '� Usuario Bloqueado !' + ' - El Administrador Ha Bloqueado Su Acceso Al Sistema' );
               lrChangePassword: AgregaBitacoraBatch( '� Debe Cambiar Clave de Usuario !' + ' - El Administrador Ha forzado que Cambie su Password' );
               lrExpiredPassword: AgregaBitacoraBatch( '� Clave de Usuario Vencida !' + ' - Su Clave de Acceso Anterior Ha Expirado' );
               lrInactiveBlock: AgregaBitacoraBatch( '� Usuario Bloqueado !' + ' - Usuario Ha Sido Bloqueado Por No Entrar Al Sistema Durante Mucho Tiempo' );
               lrNotFound: AgregaBitacoraBatch( '� No se Encontr� el Usuario !' + ' - Verifique que sea Correcto el Nombre del Usuario' );
               lrAccessDenied: AgregaBitacoraBatch( '� Password Incorrecto de Usuario !' + ' - Verifique el Password del Usuario' );
               lrSystemBlock: AgregaBitacoraBatch( '� Sistema Bloqueado !' +  ' - El Administrador Ha Bloqueado el Acceso Al Sistema' );
          else
               AgregaBitacoraBatch( '� Error General al Verificar Usuario !' + ' - Intentar con otro Usuario' );
          end;
          {$ifndef INTERFAZORDENES}
          //US# 11757:  Proteccion funcionalidad HTTP por licencia sentinel
          ValidarConexionHTTP( lLoop, lSalirTress, sMensajeErrorHTTP, True );
          if not StrVacio( sMensajeErrorHTTP ) then
          begin
               Result := FALSE;
               AgregaBitacoraBatch( sMensajeErrorHTTP );
          end;
          //FIN
          {$endif}
     end;
end;

function TBaseImportaShell.AbreEmpresaBatch: Boolean;
var
   iCompanys: Integer;
   sEmpresa : String;
begin
     Result := FALSE;
     sEmpresa := ParamStr( K_PARAM_EMPRESA );
     // Localiza Empresa
     iCompanys := dmCliente.InitCompany;
     if ( iCompanys = 0 ) then
        AgregaBitacoraBatch( '� No Hay Compa��as !' + ' - No Hay Empresas En El Sistema' )
     else
     begin
          Result := dmCliente.FindCompany( sEmpresa );
          if not Result then
             AgregaBitacoraBatch( '� No se Encontr� Empresa !' + ' - Verifique el C�digo: ' + sEmpresa );
     end;
     // Abre Empresa
     if Result then
     begin
          Application.ProcessMessages;
          CierraTodo;
          with dmCliente do
          begin
               SetCompany;
               SetCacheLookup;
          end;
          DoOpenAll;
     end;
end;

procedure TBaseImportaShell.SetControlesBatch;
begin
     ArchOrigen.Text := ParamStr( K_PARAM_ARCHIVO );
end;

procedure TBaseImportaShell.EnviarCorreoBitacoras;
const
     K_EMAIL_TIMEOUT = 5000;
var
   sHost: String;
   Puerto:Integer;

   procedure AgregaFileBitacora( const sArchivo: String );
   begin
        if strLleno( sArchivo ) and FileExists( sArchivo ) then
           Email.PostMessage.Attachments.Add( sArchivo );
   end;

begin
     sHost := Global.GetGlobalString( K_GLOBAL_EMAIL_HOST );
     if strVacio( sHost ) then
        AgregaBitacoraBatch( '� No se Pudo Enviar Email ! - No est� Configurado el Servidor de Correos en Globales de Empresa' )
     else
     begin
          with Email do
          begin
               Host := sHost;
               Puerto := Global.GetGlobalInteger( K_GLOBAL_EMAIL_PORT );
               if Puerto > 0 then
                  Port := Puerto;
               UserID := StrDef( Global.GetGlobalString( K_GLOBAL_EMAIL_USERID ), Application.Title );
               Password := Global.GetGlobalString( K_GLOBAL_EMAIL_PSWD );
               TimeOut := K_EMAIL_TIMEOUT;
               with PostMessage do
               begin
                    FromAddress := StrDef( Global.GetGlobalString( K_GLOBAL_EMAIL_FROMADRESS ), 'Interfase@dominio.com' );
                    FromName := StrDef( Global.GetGlobalString( K_GLOBAL_EMAIL_FROMNAME ), 'Interfase - Sistema TRESS' );
               end;
               try
                  Connect;
               except
                  on Error: Exception do
                  begin
                       AgregaBitacoraBatch( '� No se Pudo Enviar Email ! - No se Pudo Conectar el Servidor de Correos - ' + Error.Message );
                  end;
               end;
               if Connected then
               begin
                    with PostMessage do
                    begin
                         ToAddress.Clear;
                         Body.Clear;
                         Attachments.Clear;
                         Subject := Application.Title + ' - ' + dmCliente.GetDatosEmpresaActiva.Nombre;
                         ToAddress.CommaText := dmCliente.cdsUsuario.FieldByName( 'US_EMAIL' ).AsString;
                         Body.Text := 'Bit�coras de Proceso: ' + Subject;
                         AgregaFileBitacora( ArchBitacora.Text );
                         AgregaFileBitacora( ArchErrores.Text );
                    end;
                    try
                       SendMail;
                    except
                       on Error: Exception do
                       begin
                            AgregaBitacoraBatch( '� No se Pudo Enviar Email ! - Error al Enviar Correo - ' + Error.Message );
                       end;
                    end;
                    try
                       Disconnect;
                    except          // Que mensaje de error enviar ? ya se envi� el correo, la falla es al desconectar el cliente, lo cual se forza al terminar el programa.
                    end;
               end;
          end;
     end
end;

{ Procesos Generales del Shell }

procedure TBaseImportaShell.ProcesarArchivo;
begin
     if strLleno( ArchOrigen.Text ) then
     begin
          if FileExists( ArchOrigen.Text ) then
          begin
               ActivaControles( TRUE );
               try
                  InitBitacora;
                  DoProcesar;
               finally
                  EscribeBitacoras;
                  ActivaControles( FALSE );
               end;
          end
          else
          begin
               ReportaErrorDialogo( self.Caption, 'No se Encontr� el Archivo de Origen Especificado' );
               ActiveControl := ArchOrigen;
          end;
     end
     else
     begin
          ReportaErrorDialogo( self.Caption, 'Falta Especificar Archivo de Origen' );
          ActiveControl := ArchOrigen;
     end;
end;

procedure TBaseImportaShell.CancelarProceso;
begin
     // No hace nada en la base
end;

procedure TBaseImportaShell.InitBitacora;
begin
     MemBitacora.Clear;
     MemErrores.Clear;
     Application.ProcessMessages;
end;

procedure TBaseImportaShell.EscribeBitacoras;
begin
     self.GrabaBitacora( MemBitacora, ArchBitacora.Text );
     self.GrabaBitacora( MemErrores, ArchErrores.Text );
end;

procedure TBaseImportaShell.ActivaControles( const lProcesando: Boolean );
begin
     with dmCliente do
     begin
          LblArchivo.Enabled := EmpresaAbierta and ( not lProcesando );
          ArchOrigen.Enabled := EmpresaAbierta and ( not lProcesando );
          ArchivoSeek.Enabled := EmpresaAbierta and ( not lProcesando );
          _P_Procesar.Enabled := EmpresaAbierta and ( not lProcesando );
          _A_SalirSistema.Enabled := ( not lProcesando );
          _P_Cancelar.Enabled := EmpresaAbierta and lProcesando;
     end;
end;

procedure TBaseImportaShell.AgregaBitacoraBatch(const sMensaje: String);
begin
     FBatchLog.Add( FormatDateTime( 'dd/mm/yyyy-hh:mm:ss', now ) + ': ' + sMensaje );
end;

procedure TBaseImportaShell.ReportaErrorDialogo( const sTitulo, sMensaje: String );
begin
     if FModoBatch then
        AgregaBitacoraBatch( strTransAll( sMensaje, CR_LF, ZetaCommonClasses.VACIO ) )
     else
        ZetaDialogo.ZError( sTitulo, sMensaje, 0 );
end;

procedure TBaseImportaShell.GrabaBitacora(oMemo: TMemo; const sArchivo: String);
begin
     try
        oMemo.Lines.SaveToFile( sArchivo );
     except
        on Error : Exception do
        begin
             ReportaErrorDialogo( self.Caption, 'Error al Grabar ' + oMemo.Hint + ' al Archivo ' + CR_LF + sArchivo );
        end;
     end;
end;

procedure TBaseImportaShell.EscribeBitacora(const sMensaje: String);
begin
     MemBitacora.Lines.Add( sMensaje );
end;

procedure TBaseImportaShell.EscribeError(const sMensaje: String);
begin
     MemErrores.Lines.Add( sMensaje );
end;

{ Action List }

procedure TBaseImportaShell._A_OtraEmpresaExecute(Sender: TObject);
begin
     AbreEmpresa( False );
end;

procedure TBaseImportaShell._A_ServidorExecute(Sender: TObject);
begin
     CambiaServidor;
end;

procedure TBaseImportaShell._A_SalirSistemaExecute(Sender: TObject);
begin
     Close;
end;

procedure TBaseImportaShell._P_ProcesarExecute(Sender: TObject);
begin
     inherited;
     ProcesarArchivo;
end;

procedure TBaseImportaShell._P_CancelarExecute(Sender: TObject);
begin
     inherited;
     CancelarProceso;
end;

procedure TBaseImportaShell._H_AcercaDeExecute(Sender: TObject);
begin
     with TZAcercaDe.Create( Self ) do
     begin
          try
             ShowModal;
          finally
             Free;
          end;
     end;
end;

{ Eventos del Shell }

procedure TBaseImportaShell.ArchivoSeekClick(Sender: TObject);
begin
     ArchOrigen.Text := ZetaClientTools.AbreDialogo( OpenDialog, ArchOrigen.Text, 'Dat' );
end;

procedure TBaseImportaShell.BitacoraSeekClick(Sender: TObject);
begin
     ArchBitacora.Text := ZetaClientTools.AbreDialogo( OpenDialog, ArchBitacora.Text, 'Log' );
end;

procedure TBaseImportaShell.ErroresSeekClick(Sender: TObject);
begin
     ArchErrores.Text := ZetaClientTools.AbreDialogo( OpenDialog, ArchErrores.Text, 'Log' );
end;

procedure TBaseImportaShell._Per_GenerarPolizaExecute(Sender: TObject);
begin
     inherited;
     { No se hace nada } 
end;

end.
