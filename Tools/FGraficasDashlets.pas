unit FGraficasDashlets;

interface

uses
   Controls,
   Classes,
   ExtCtrls,
   Graphics,
   SysUtils,
   ZetaCommonClasses,
   FIndicadores,
   ZetaCXGrid,
   VCLTee.Chart,
   VCLTee.TeEngine,
   ZetaDialogo,
   ZetaClientDataSet,
   VCLTee.TeCanvas,
   VCLTee.Series,
   VCLTee.TeeProcs,
   Data.DB;



type
  TGraficasDashletsBase = class(TChart)
  private
    FTitulo: String;
    FControlName: String;
    FMensajeChart: String;
    FPanelMensajeThread: TPanel;
    function GetTitulo: String;
    procedure SetTitulo(const Value: String);
    procedure ChartBeforeDrawAxes(Sender: TObject);
    { private declarations here }
  protected
    { protected declarations here }
    FDataSet: TZetaClientDataSet;
    function CrearSerieHorizBar: THorizBarSeries;
    function CrearSerieBar: TBarSeries;
    function CrearSeriePie: TPieSeries;
    function CrearSerieHorizBarStack: THorizBarSeries;
    function EsUnaSerie: Boolean;
    procedure GenerarDatos(  const ClassTypeSerie: TClass  );
    procedure CrearSerie( const ClassTypeSerie: TClass );
    procedure CrearSeries( const ClassTypeSerie: TClass );
    procedure AddDatos( const ClassTypeSerie: TClass; Const iSerie: Integer; const dValor: Double; const sDescripcion: String; const Color: Longint );
    procedure SetDescripcionesSerie( const ClassTypeSerie: TClass; const iSerie: Integer; const oDataSet: TZetaClientDataSet; const Color: Longint );
    procedure Graficar( const ClassTypeSerie: TClass );
  public
    { public declarations here }
    constructor Create( AOwner : TComponent); overload; override;
    constructor Create(const ControlName: string; AOwner : TComponent; Parent: TWinControl ); reintroduce; overload;
    procedure DefinirPropiedades;
    procedure BorrarSeries;
    procedure PropiedadesChart( const lDisable: Boolean );
    procedure InitChart;
    procedure MensajeEspera;
    procedure ValoresDefaulPanel;
  published
    { published declarations here }
    property Titulo: String read GetTitulo write SetTitulo;
    property ControlName: String read FControlName write FControlName;
    property DataSet: TZetaClientDataSet read FDataSet write FDataSet;
end;

type
  TGraficasDashletsBarras = class(TGraficasDashletsBase)
  private
    { private declarations here }
  protected
    { protected declarations here }
  public
    { public declarations here }
    constructor Create( AOwner : TComponent); overload; override;
    constructor Create(const ControlName: string; AOwner : TComponent; Parent: TWinControl ); reintroduce; overload;
    procedure Graficar( const ClassTypeSerie: TClass );
  published
    { published declarations here }
end;

type
  TGraficasDashletsColumnas = class(TGraficasDashletsBase)
  private
    { private declarations here }
  protected
    { protected declarations here }
  public
    { public declarations here }
    constructor Create( AOwner : TComponent); overload; override;
    constructor Create(const ControlName: string; AOwner : TComponent; Parent: TWinControl ); reintroduce; overload;
    procedure Graficar( const ClassTypeSerie: TClass );
  published
    { published declarations here }
end;

type
  TGraficasDashletsPie = class(TGraficasDashletsBase)
  private
    { private declarations here }
  protected
    { protected declarations here }
  public
    { public declarations here }
    constructor Create( AOwner : TComponent); overload; override;
    constructor Create(const ControlName: string; AOwner : TComponent; Parent: TWinControl ); reintroduce; overload;
    procedure Graficar( const ClassTypeSerie: TClass );
  published
    { published declarations here }
end;

type
  TGraficasDashletsHorizStack = class(TGraficasDashletsBase)
  private
    procedure RemoverNegativoTexto(var LabelText: String);
    { private declarations here }
  protected
    { protected declarations here }
  public
    { public declarations here }
    constructor Create( AOwner : TComponent); overload; override;
    constructor Create(const ControlName: string; AOwner : TComponent; Parent: TWinControl ); reintroduce; overload;
    procedure Graficar( const ClassTypeSerie: TClass );
    procedure OnGetAxisLabelCustom( Sender: TChartAxis; Series: TChartSeries; ValueIndex: Integer; var LabelText: string );
    procedure OnBeforeDrawSeriesCustom(Sender: TObject);
    procedure SeriesGetMarkText( Sender: TChartSeries; ValueIndex: Integer; var MarkText: String );
  published
    { published declarations here }
end;

const
  tColoresGrafica : array [0..15] of string = (
                  '$00D8AA7F',
                  '$0000B2E9',
                  '$004858CC',
                  '$0050D293',
                  '$003399FF',
                  '$005D4E64',
                  '$00DCAB00',
                  '$00738D2E',
                  '$004B3B4D',
                  '$008162F5',
                  '$00566FF3',
                  '$008B818C',
                  '$003F21C3',
                  '$00D1D600',
                  '$00CDD768',
                  '$00A494C2'
                );

  tColoresGraficaColumnas : array [0..15] of string = (
                  '$00738D2E',
                  '$0050D293',
                  '$005D4E64',
                  '$00DCAB00',
                  '$00D8AA7F',
                  '$0000B2E9',
                  '$004858CC',
                  '$003399FF',
                  '$004B3B4D',
                  '$008162F5',
                  '$00566FF3',
                  '$008B818C',
                  '$003F21C3',
                  '$00D1D600',
                  '$00CDD768',
                  '$00A494C2'
                );

  tColoresGraficaBarras : array [0..15] of string = (
                  '$005D4E64',
                  '$00DCAB00',
                  '$00D8AA7F',
                  '$0000B2E9',
                  '$004858CC',
                  '$0050D293',
                  '$003399FF',
                  '$00738D2E',
                  '$004B3B4D',
                  '$008162F5',
                  '$00566FF3',
                  '$008B818C',
                  '$003F21C3',
                  '$00D1D600',
                  '$00CDD768',
                  '$00A494C2'
                );

  tColoresGraficaBarraStack : array [0..15] of string = (
                  '$00DCAB00',
                  '$004858CC',
                  '$005D4E64',
                  '$00D8AA7F',
                  '$0000B2E9',
                  '$0050D293',
                  '$003399FF',
                  '$00738D2E',
                  '$004B3B4D',
                  '$008162F5',
                  '$00566FF3',
                  '$008B818C',
                  '$003F21C3',
                  '$00D1D600',
                  '$00CDD768',
                  '$00A494C2'
                );


  K_MARGIN = 5;
  K_MENSJAE_THREAD = 'REFRESCANDO...';

implementation
{ TGraficasDashletsBase }

function TGraficasDashletsBase.CrearSerieBar: TBarSeries;
begin
     Result := TBarSeries.Create( Owner );
     with Result do
     begin
          Marks.Visible := True;
          Marks.Style := smsValue;
          BarPen.Visible := False;
          Marks.Style := smsValue;
          Marks.Font.Name := 'Segoe UI';
          Marks.Font.Quality := fqClearType;
          Marks.Frame.Visible := False;
          Marks.Transparent := True;
     end;
     Self.AddSeries( Result );
end;

function TGraficasDashletsBase.CrearSerieHorizBar: THorizBarSeries;
begin
     Result := THorizBarSeries.Create( Owner );
     with Result do
     begin
          Marks.Visible := True;
          Marks.Style := smsValue;
          BarPen.Visible := False;
          Marks.Style := smsValue;
          Marks.Font.Name := 'Segoe UI';
          Marks.Font.Quality := fqClearType;
          Marks.Frame.Visible := False;
          Marks.Transparent := True;
     end;
     Self.AddSeries( Result );
end;

function TGraficasDashletsBase.CrearSeriePie: TPieSeries;
begin
     Result := TPieSeries.Create( Owner );
     with Result do
     begin
          Marks.Visible := True;
          PiePen.Visible := False;
          Marks.Style := smsPercent;
          Marks.Font.Name := 'Segoe UI';
          Marks.Font.Quality := fqClearType;
          Marks.Frame.Visible := False;
          Marks.Transparent := True;
     end;
     Self.AddSeries( Result );
end;

function TGraficasDashletsBase.CrearSerieHorizBarStack: THorizBarSeries;
begin
     Result := THorizBarSeries.Create( Owner );
     with Result do
     begin
          Marks.Visible := True;
          Marks.Style := smsValue;
          BarPen.Visible := False;
          Marks.Style := smsValue;
          Marks.Font.Name := 'Segoe UI';
          Marks.Font.Quality := fqClearType;
          Marks.Frame.Visible := False;
          Marks.Transparent := True;
          MultiBar := mbStacked;
     end;
     Self.AddSeries( Result );
end;

constructor TGraficasDashletsBase.Create(const ControlName: string; AOwner: TComponent; Parent: TWinControl );
begin
     inherited Create( AOwner );
     FPanelMensajeThread := TPanel.Create( Self );
     with FPanelMensajeThread do
     begin
          Parent := Self;
          ParentBackground:= False;
          ParentColor := False;
          BevelEdges := [];
          BevelInner := bvNone;
          BevelKind := bkNone;
          BevelOuter := bvNone;
          Align := alClient;
          Color := $00F4F4F4;
          Font.Name := 'Segoe UI';
          Font.Size := 10;
          Font.Color := $00A6A6A6;
          Font.Style := [fsBold];
          Caption := '';
          Alignment := taCenter;
     end;
end;

procedure TGraficasDashletsBase.DefinirPropiedades;
begin
     Self.Top := 0;
     Self.Left := 0;
     Self.Visible := true;
     Self.Parent := Parent;
     Self.Align := alClient;
     Self.Title.Caption := '';
     Self.Zoom.Allow := False;
     Self.BottomAxis.Grid.Color := clNone;
     Self.BottomAxis.Grid.Style := psClear;
     Self.Panning.MouseWheel := pmwNone;
     //Border
     Self.Border.Visible := True;
     Self.Title.Font.Name := 'Segoe UI';
     Self.Title.Font.Size := 10;
     Self.Title.Font.Style := [fsBold];
     Self.Title.Font.Color := $003E3E3E;
end;

constructor TGraficasDashletsBase.Create(AOwner: TComponent);
begin
     inherited Create( AOwner );
end;

function TGraficasDashletsBase.GetTitulo: String;
begin
     Result := FTitulo;
end;

procedure TGraficasDashletsBase.SetTitulo(const Value: String);
begin
     Self.FTitulo := Value;
     Self.Title.Caption := Value;
end;

function TGraficasDashletsBase.EsUnaSerie: Boolean;
begin
     Result := True;
     if FDataSet.FieldCount - 2 > 1 then
        Result := False;
end;

procedure TGraficasDashletsBase.BorrarSeries;
begin
     FMensajeChart := '<No hay datos>';
     PropiedadesChart( False );
     ValoresDefaulPanel;
end;

procedure TGraficasDashletsBase.InitChart;
begin
     FMensajeChart := '';
     PropiedadesChart( True );
     ValoresDefaulPanel;
end;

procedure TGraficasDashletsBase.ChartBeforeDrawAxes(Sender: TObject);
begin
     with Self.Canvas do
     begin
          Font.Size := 8;
          Font.Name := 'Tahoma';
          Font.Style := [];
          Font.Color := $00989898;
          Font.Quality := fqClearType;
          Brush.Style:=bsClear;
          TextOut( trunc( Self.Width / 2 ) - trunc( TextWidth( FMensajeChart ) / 2 ), trunc( Self.Height / 2 ), FMensajeChart );
          Alignment := taCenter;
     end;
end;

procedure TGraficasDashletsBase.ValoresDefaulPanel;
begin
     FPanelMensajeThread.Hide;
     FPanelMensajeThread.Caption := '';
end;

procedure TGraficasDashletsBase.MensajeEspera;
begin
     PropiedadesChart( False );
     FPanelMensajeThread.Visible := True;
     FPanelMensajeThread.Caption := K_MENSJAE_THREAD;
     with Self.Canvas do
     begin
          Padding.Left := K_MARGIN;
          Padding.Right := K_MARGIN;
          Padding.Top := 25;
          Padding.Bottom := K_MARGIN;
          Alignment := taCenter;
     end;
end;

procedure TGraficasDashletsBase.PropiedadesChart( const lDisable: Boolean );
begin
    Self.Enabled := lDisable;
    Self.RemoveAllSeries;
    Self.BottomAxis.Automatic := lDisable;
    Self.BottomAxis.AutomaticMaximum := lDisable;
    Self.BottomAxis.AutomaticMinimum := lDisable;
    Self.BottomAxis.Visible := lDisable;
    Self.DepthAxis.Automatic := lDisable;
    Self.DepthAxis.AutomaticMaximum := lDisable;
    Self.DepthAxis.AutomaticMinimum := lDisable;
    Self.DepthTopAxis.Automatic := lDisable;
    Self.DepthTopAxis.AutomaticMaximum := lDisable;
    Self.DepthTopAxis.AutomaticMinimum := lDisable;
    Self.LeftAxis.Automatic := lDisable;
    Self.LeftAxis.AutomaticMaximum := lDisable;
    Self.LeftAxis.AutomaticMinimum := lDisable;
    Self.LeftAxis.Visible := lDisable;
    Self.RightAxis.Automatic := lDisable;
    Self.RightAxis.AutomaticMaximum := lDisable;
    Self.RightAxis.AutomaticMinimum := lDisable;
    Self.RightAxis.Visible := lDisable;
    Self.TopAxis.Automatic := lDisable;
    Self.TopAxis.AutomaticMaximum := lDisable;
    Self.TopAxis.AutomaticMinimum := lDisable;
    Self.TopAxis.Visible := lDisable;
    Self.OnBeforeDrawAxes := ChartBeforeDrawAxes;
    Self.Refresh;
end;

procedure TGraficasDashletsBase.CrearSerie( const ClassTypeSerie: TClass );
begin
     if Self.ClassType.ClassNameIs( TGraficasDashletsColumnas.ClassName ) and  ClassTypeSerie.ClassNameIs( TBarSeries.ClassName ) then //Columnas
        CrearSerieBar
     else if  Self.ClassType.ClassNameIs( TGraficasDashletsBarras.ClassName ) and ClassTypeSerie.ClassNameIs( THorizBarSeries.ClassName ) then //Barras
        CrearSerieHorizBar
     else if Self.ClassType.ClassNameIs( TGraficasDashletsPie.ClassName ) and ClassTypeSerie.ClassNameIs( TPieSeries.ClassName ) then //Pie
        CrearSeriePie
     else if Self.ClassType.ClassNameIs( TGraficasDashletsHorizStack.ClassName ) and ClassTypeSerie.ClassNameIs( THorizBarSeries.ClassName ) then //BarrasStack
        CrearSerieHorizBarStack;
end;

procedure TGraficasDashletsBase.CrearSeries( const ClassTypeSerie: TClass );
var
   iSerie: Integer;
begin
     for iSerie := 2 to FDataSet.FieldCount - 1 do
     begin
          CrearSerie( ClassTypeSerie );
     end;
end;

procedure TGraficasDashletsBase.AddDatos( const ClassTypeSerie: TClass; Const iSerie: Integer; const dValor: Double; const sDescripcion: String; const Color: Longint );
begin
     if ( Self.ClassType.ClassNameIs( TGraficasDashletsColumnas.ClassName ) and ClassTypeSerie.ClassNameIs( TBarSeries.ClassName ) ) then //Columnas
     begin
          TBarSeries( Self.Series[ iSerie ] ).AddBar( dValor, sDescripcion, ColorToRGB( StringToColor( tColoresGraficaColumnas[ Color ] ) ) );
     end
     else if ( Self.ClassType.ClassNameIs( TGraficasDashletsBarras.ClassName ) and ClassTypeSerie.ClassNameIs( THorizBarSeries.ClassName ) ) then //Barras
     begin
          THorizBarSeries( Self.Series[ iSerie ] ).AddBar( dValor, sDescripcion, ColorToRGB( StringToColor( tColoresGraficaBarras[ Color ] ) ) );
     end
     else if ( Self.ClassType.ClassNameIs( TGraficasDashletsPie.ClassName ) and ClassTypeSerie.ClassNameIs( TPieSeries.ClassName ) ) then //Pie
     begin
          TPieSeries( Self.Series[ iSerie ] ).AddPie( dValor, sDescripcion, ColorToRGB( StringToColor( tColoresGrafica[ Color ] ) ) );
     end
     else if ( Self.ClassType.ClassNameIs( TGraficasDashletsHorizStack.ClassName ) and ClassTypeSerie.ClassNameIs( THorizBarSeries.ClassName ) ) then //Barras Horiz Stack
     begin
          if dValor = 0 then
          begin
               THorizBarSeries( Self.Series[ iSerie ] ).AddNullXY( 0, 0, sDescripcion );
          end
          else
          begin
               THorizBarSeries( Self.Series[ iSerie ] ).AddBar( dValor, sDescripcion, ColorToRGB( StringToColor( tColoresGraficaBarraStack[ Color ] ) ) );
          end;

     end
end;

procedure TGraficasDashletsBase.SetDescripcionesSerie( const ClassTypeSerie: TClass; const iSerie: Integer; const oDataSet: TZetaClientDataSet; const Color: Longint );
begin
     if ( Self.ClassType.ClassNameIs( TGraficasDashletsColumnas.ClassName ) and ClassTypeSerie.ClassNameIs( TBarSeries.ClassName ) ) then //Columnas
     begin
          TBarSeries( Self.Series[ iSerie ] ).LegendTitle := oDataSet.Fields[ iSerie + 2 ].FieldName;
          TBarSeries( Self.Series[ iSerie ] ).Color := ColorToRGB( StringToColor( tColoresGraficaColumnas[ Color ] ) );
     end
     else if ( Self.ClassType.ClassNameIs( TGraficasDashletsBarras.ClassName ) and ClassTypeSerie.ClassNameIs( THorizBarSeries.ClassName ) ) then //Barras
     begin
          THorizBarSeries( Self.Series[ iSerie ] ).LegendTitle := oDataSet.Fields[ iSerie + 2 ].FieldName;
          THorizBarSeries( Self.Series[ iSerie ] ).Color :=  ColorToRGB( StringToColor( tColoresGraficaBarras[ Color ] ) );
     end
     else if ( Self.ClassType.ClassNameIs( TGraficasDashletsHorizStack.ClassName ) and ClassTypeSerie.ClassNameIs( THorizBarSeries.ClassName ) ) then //Barras Hoz Stack
     begin
          THorizBarSeries( Self.Series[ iSerie ] ).LegendTitle := oDataSet.Fields[ iSerie + 2 ].FieldName;
          THorizBarSeries( Self.Series[ iSerie ] ).Color :=  ColorToRGB( StringToColor( tColoresGraficaBarraStack[ Color ] ) );
     end;
end;

procedure TGraficasDashletsBase.GenerarDatos(  const ClassTypeSerie: TClass  );
var
   j: Integer;
   iSerie: Integer;

   procedure ReiniciarPaletaColores;
   begin
        if j = High( tColoresGrafica ) then
           j:=0;
   end;
begin
     if ( FDataSet <> nil ) and not ( FDataSet.EOF ) then
     begin
          iSerie := 0;
          j := 0;
          if EsUnaSerie then
          begin
               with FDataSet do
               begin
                    First;
                    while ( not EOF ) do
                    begin
                         with Self.Series[ iSerie ] do
                         begin
                              ColorEachPoint := True;
                              ReiniciarPaletaColores;
                              AddDatos( ClassTypeSerie, iSerie, FDataSet.FieldByName( 'Cantidad' ).AsFloat, FDataSet.FieldByName( 'Descripcion' ).AsString, j );
                         end;
                         inc(j);
                         Next;
                    end;
               end;
          end
          else
          begin
               with FDataSet do
               begin
                    First;
                    while ( not EOF ) do
                    begin
                         try
                            for iSerie := 0 to Self.SeriesCount - 1 do
                            begin
                                 AddDatos( ClassTypeSerie, iSerie, FDataSet.Fields[ iSerie + 2 ].AsFloat, FDataSet.FieldByName( 'Descripcion' ).AsString,  iSerie );
                            end;

                         except
                               on Error: Exception do
                               begin
                                    ZetaDialogo.ZError( 'Error', Error.Message, 0 );
                               end;
                         end;
                         Next;
                    end;
               end;
               with FDataSet do
               begin
                    First;
                    try
                       for iSerie := 0 to Self.SeriesCount - 1 do
                       begin
                            SetDescripcionesSerie( ClassTypeSerie, iSerie, FDataSet, iSerie );
                       end;
                    except
                         on Error: Exception do
                         begin
                              ZetaDialogo.ZError( 'Error', Error.Message, 0 );
                         end;
                    end;
              end;
          end;
     end;
end;


procedure TGraficasDashletsBase.Graficar( const ClassTypeSerie: TClass );
begin
     if ( FDataSet <> nil ) and not ( FDataSet.EOF ) then
     begin
          Self.FreeAllSeries;
          if EsunaSerie then
          begin
               CrearSerie( ClassTypeSerie );
               GenerarDatos( ClassTypeSerie );
          end
          else
          begin
               CrearSeries( ClassTypeSerie );
               GenerarDatos( ClassTypeSerie );
          end;
     end;
end;

{ TGraficasDashletsBarras }

constructor TGraficasDashletsBarras.Create(AOwner: TComponent);
begin
     inherited Create( AOwner );
end;

constructor TGraficasDashletsBarras.Create(const ControlName: string; AOwner: TComponent; Parent: TWinControl );
begin
     inherited Create( ControlName, AOwner, Parent );
     Self.Name := ControlName;
end;

procedure TGraficasDashletsBarras.Graficar(const ClassTypeSerie: TClass);
begin
     inherited Graficar( ClassTypeSerie );
end;

{ TGraficasDashletsColumnas }

constructor TGraficasDashletsColumnas.Create(AOwner: TComponent);
begin
     inherited Create( AOwner );
end;

constructor TGraficasDashletsColumnas.Create(const ControlName: string; AOwner: TComponent; Parent: TWinControl );
begin
     inherited Create( ControlName, AOwner, Parent );
     Self.Name := ControlName;
end;

procedure TGraficasDashletsColumnas.Graficar( const ClassTypeSerie: TClass );
begin
     inherited Graficar( ClassTypeSerie );
end;

{ TGraficasDashletsPie }

constructor TGraficasDashletsPie.Create(AOwner: TComponent);
begin
     inherited Create( AOwner );
end;

constructor TGraficasDashletsPie.Create(const ControlName: string; AOwner: TComponent; Parent: TWinControl );
begin
     inherited Create( ControlName, AOwner, Parent );
     Self.Name := ControlName;
end;

procedure TGraficasDashletsPie.Graficar(const ClassTypeSerie: TClass);
begin
     inherited Graficar( ClassTypeSerie );
end;

{ TGraficasDashletsHorizStack }

constructor TGraficasDashletsHorizStack.Create(AOwner: TComponent);
begin
     inherited Create( AOwner );
end;

constructor TGraficasDashletsHorizStack.Create(const ControlName: string; AOwner: TComponent; Parent: TWinControl);
begin
     inherited Create( ControlName, AOwner, Parent );
     Self.Name := ControlName;
     Self.OnGetAxisLabel := OnGetAxisLabelCustom;
     Self.OnBeforeDrawSeries := OnBeforeDrawSeriesCustom;
end;

procedure TGraficasDashletsHorizStack.SeriesGetMarkText( Sender: TChartSeries; ValueIndex: Integer; var MarkText: String );
begin
     RemoverNegativoTexto( MarkText );
end;

procedure TGraficasDashletsHorizStack.OnBeforeDrawSeriesCustom(Sender: TObject);
var
   i: Integer;
begin
     for i:=0 to TChart(Sender).SeriesCount -1 do
     begin
          TChart(Sender).Series[i].OnGetMarkText := SeriesGetMarkText;
     end;
end;

procedure TGraficasDashletsHorizStack.OnGetAxisLabelCustom( Sender: TChartAxis; Series: TChartSeries; ValueIndex: Integer; var LabelText: string );
begin
     RemoverNegativoTexto( LabelText );
end;

procedure TGraficasDashletsHorizStack.RemoverNegativoTexto( var LabelText: String );
var
   dVal: Double;
   iVal: Integer;
begin
     if TryStrToFloat( LabelText, dVal ) then
     begin
          if dVal < 0 then
          begin
               LabelText := StringReplace( LabelText, '-', '', [rfReplaceAll] );
          end;
     end
     else if TryStrToInt( LabelText, iVal ) then
     begin
          if iVal < 0 then
          begin
               LabelText := StringReplace( LabelText, '-', '', [rfReplaceAll] );
          end;
     end;
end;

procedure TGraficasDashletsHorizStack.Graficar(const ClassTypeSerie: TClass);
begin
     inherited Graficar( ClassTypeSerie );
end;

end.

