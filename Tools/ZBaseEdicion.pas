unit ZBaseEdicion;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls, DBCtrls, Db, DBGrids,
     {$ifndef ver130}
     variants,
     {$endif}
     ZetaClientDataset,
     ZetaCommonLists,
     ZBaseDlgModal, ZetaSmartLists;

type
  TBaseEdicion = class(TZetaDlgModal)
    PanelSuperior: TPanel;
    PanelIdentifica: TPanel;
    Splitter: TSplitter;
    ValorActivo1: TPanel;
    ValorActivo2: TPanel;
    DataSource: TDataSource;
    DBNavigator: TDBNavigator;
    AgregarBtn: TSpeedButton;
    BorrarBtn: TSpeedButton;
    ModificarBtn: TSpeedButton;
    BuscarBtn: TSpeedButton;
    ImprimirBtn: TSpeedButton;
    ImprimirFormaBtn: TSpeedButton;
    CortarBtn: TSpeedButton;
    CopiarBtn: TSpeedButton;
    PegarBtn: TSpeedButton;
    UndoBtn: TSpeedButton;
    ExportarBtn: TZetaSpeedButton;
    textoValorActivo1: TLabel;
    textoValorActivo2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure DataSourceStateChange(Sender: TObject);
    procedure AgregarBtnClick(Sender: TObject);
    procedure BorrarBtnClick(Sender: TObject);
    procedure ModificarBtnClick(Sender: TObject);
    procedure ImprimirBtnClick(Sender: TObject);
    procedure CortarBtnClick(Sender: TObject);
    procedure CopiarBtnClick(Sender: TObject);
    procedure PegarBtnClick(Sender: TObject);
    procedure UndoBtnClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure BuscarBtnClick(Sender: TObject);
    procedure ImprimirFormaBtnClick(Sender: TObject);
    procedure ExportarBtnClick(Sender: TObject);
  private
    { Private declarations }
    FFirstControl: TWinControl;
    FIndexDerechos: Integer;
    FModo: TDatasetState;
    FTipoValorActivo1: TipoEstado;
    FTipoValorActivo2: TipoEstado;
    FInsertInicial: Boolean;
    function DoConnect: Boolean;
    function NoHayDatos: Boolean;
    procedure DoDisconnect;
    procedure InitValoresActivos;
    procedure CancelarCambios;
    procedure SetModo( const Value: TDatasetState );
    procedure DoExportar;
    procedure Exportar;
    procedure InitExcelBtn;
  protected
    { Protected declarations }
    property IndexDerechos: Integer read FIndexDerechos write FIndexDerechos;
    property InsertInicial: Boolean read FInsertInicial;
    property Modo: TDatasetState read FModo write SetModo;
    property FirstControl: TWinControl read FFirstControl write FFirstControl;
    property TipoValorActivo1: TipoEstado read FTipoValorActivo1 write FTipoValorActivo1;
    property TipoValorActivo2: TipoEstado read FTipoValorActivo2 write FTipoValorActivo2;
    function CheckDerechos(const iDerecho: Integer): Boolean;dynamic;
    function Editing: Boolean;
    function Inserting: Boolean;
    function BotonesActivos: Boolean; dynamic;
    function ClientDataset: TZetaClientDataset;
    function NoData( Fuente: TDatasource ): Boolean;
    function PuedeAgregar(var sMensaje: String): Boolean; dynamic;
    function PuedeBorrar(var sMensaje: String): Boolean; dynamic;
    function PuedeModificar(var sMensaje: String): Boolean; dynamic;
    function PuedeImprimir(var sMensaje: String): Boolean; dynamic;
    function ValoresGrid: Variant;virtual;
    function CanLookup: Boolean; virtual;
    procedure AsignaValoresActivos;
    procedure Connect; virtual; abstract;
    procedure Disconnect; virtual;
    procedure Agregar; virtual;
    procedure Borrar; virtual;
    procedure Modificar; virtual;
    procedure Imprimir; dynamic;
    procedure ImprimirForma;virtual;
    procedure DoCancelChanges; dynamic;
    procedure DoDelete;
    procedure DoEdit;
    procedure DoInsert;
    procedure DoLookup; virtual;
    procedure DoPrint;
    procedure DoPrintForma;
    procedure FocusFirstControl; dynamic;
    procedure HabilitaControles; dynamic;
    procedure KeyDown( var Key: Word; Shift: TShiftState ); override; { TWinControl }
    procedure EscribirCambios; dynamic;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
  end;
  TBaseEdicionClass = class of TBaseEdicion;

var
  BaseEdicion: TBaseEdicion;

procedure ShowFormaEdicion( var Forma; EdicionClass: TBaseEdicionClass );
procedure ShowFormaEdicionAndFree( var Forma; EdicionClass: TBaseEdicionClass );

implementation

uses ZetaDialogo,
     ZetaCommonClasses,
     ZAccesosMgr,
     ZCerrarEdicion,
     FBaseReportes,
     DCliente,
     DBClient;

{$R *.DFM}

procedure ShowFormaEdicion( var Forma; EdicionClass: TBaseEdicionClass );
begin
     ZBaseDlgModal.ShowDlgModal( Forma, EdicionClass );
end;

procedure ShowFormaEdicionAndFree( var Forma; EdicionClass: TBaseEdicionClass );
begin
     ZBaseDlgModal.ShowDlgModal( Forma, EdicionClass );
     if ( TBaseEdicion(Forma) <> nil ) then
        FreeAndNil( Forma );
end;

{ ************ TZetaBaseEdicion *********** }

constructor TBaseEdicion.Create( AOwner: TComponent );
begin
     FTipoValorActivo1 := stNinguno;
     FTipoValorActivo2 := stNinguno;
     inherited Create( AOwner );
     InitValoresActivos;

     InitExcelBtn;
end;

{La razon de este metodo, es para no tener que revisar TODAS las herencias
de ZbaseEdicion.
El boton de excel entra a la mitad del panel, lo que se requiere que las descencias
se revisen y que no se encime el boton.
Con este metodo, si la forma NO tiene grid, el boton NO aparece}

procedure TBaseEdicion.InitExcelBtn;
 var
    lVisible: boolean;
    i: integer;
begin
     lVisible := FALSE;
     for i := 0 to ( ComponentCount - 1 ) do
     begin
          if ( Components[ i ] is TDBGrid ) then
          begin
               lVisible := TRUE;
               Break;
          end;
     end;
     ExportarBtn.Visible := lVisible;
end;

procedure TBaseEdicion.FormCreate(Sender: TObject);
begin
     inherited;
     { Para permitir a descendientes invocar este evento }

end;

procedure TBaseEdicion.FormShow(Sender: TObject);
begin
     inherited;
     if DoConnect then
     begin
          { Impide que usuarios sin derechos de Modificaci�n }
          { puedan llegar a modo de Edici�n simplemente cambiando }
          { el contenido de un control }
          Datasource.AutoEdit := CheckDerechos( K_DERECHO_CAMBIO );
          AsignaValoresActivos;
          FocusFirstControl;
          FInsertInicial := Inserting;
     end;
end;

procedure TBaseEdicion.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     inherited;
     if Editing then
     begin
          case ZCerrarEdicion.CierraEdicion of
               mrOk:
               begin
                    EscribirCambios;
                    CanClose := not Editing;
               end;
               mrIgnore:
               begin
                    CancelarCambios;
                    CanClose := not Editing;
               end;
          else
              CanClose := False;
          end;
     end
     else
         CanClose := True;
end;

procedure TBaseEdicion.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     DoDisconnect;
     Action := caHide;
end;

function TBaseEdicion.ClientDataset: TZetaClientDataset;
begin
     Result := TZetaClientDataset( Datasource.Dataset );
end;

function TBaseEdicion.CanLookup: Boolean;
begin
     with DataSource do
     begin
          Result := ( Dataset <> nil ) and ( Dataset is TZetaLookupDataset );
     end;
end;

function TBaseEdicion.NoData( Fuente: TDatasource ): Boolean;
begin
     with Fuente do
     begin
          if ( Dataset = nil ) then
             Result := False
          else
              Result := Dataset.IsEmpty;
     end;
end;

function TBaseEdicion.NoHayDatos: Boolean;
begin
     with Datasource do
     begin
          if ( Dataset = nil ) then
             Result := False
          else
              Result := Dataset.IsEmpty;
     end;
end;

function TBaseEdicion.DoConnect: Boolean;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             try
                Connect;
                Result := True;
             except
                   on Error: Exception do
                   begin
                        zExcepcion( '� Error Al Conectar Forma !', '� Ventana No Pudo Ser Conectada A Sus Datos !', Error, 0 );
                        Result := False;
                   end;
             end;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

procedure TBaseEdicion.DoDisconnect;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Disconnect;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

procedure TBaseEdicion.Disconnect;
begin
     Datasource.Dataset := nil;
end;

procedure TBaseEdicion.DoLookup;
{
var
   sKey, sDescripcion: String;
}
begin
     {
     if CanLookup then
        TZetaLookupDataset( ClientDataset ).Search( '', sKey, sDescripcion );
     }
end;

procedure TBaseEdicion.DoCancelChanges;
begin
     with ClientDataset do
     begin
          CancelUpdates;
     end;
end;

procedure TBaseEdicion.InitValoresActivos;
begin
     if ( TipoValorActivo1 = stNinguno ) and ( TipoValorActivo2 = stNinguno ) then
     begin
          PanelIdentifica.Visible := False;
          ValorActivo1.Visible := False;
          ValorActivo2.Visible := False;
          Splitter.Visible := False;
     end
     else
     begin
          if ( TipoValorActivo1 = stNinguno ) then
          begin
               ValorActivo1.Align := alNone;
               ValorActivo1.Visible := False;
               ValorActivo2.Align := alClient;
               Splitter.Visible := False;
          end;
          if ( TipoValorActivo2 = stNinguno ) then
          begin
               ValorActivo2.Align := alNone;
               ValorActivo2.Visible := False;
               ValorActivo1.Align := alClient;
               Splitter.Visible := False;
          end;
     end;
end;

{***DevEx (by am): Se dejo de usar el caption de los paneles para mostrar los valores activos.
Ahora se utilizara una label dentro del panel. ***}
procedure TBaseEdicion.AsignaValoresActivos;
begin
     with dmCliente do
     begin
          textoValorActivo1.Caption := GetValorActivoStr( TipoValorActivo1 );
          textoValorActivo2.Caption := GetValorActivoStr( TipoValorActivo2 )
     end;

end;

procedure TBaseEdicion.HabilitaControles;
begin
     DBNavigator.Enabled := not Editing;
     AgregarBtn.Enabled := not Editing;
     BorrarBtn.Enabled := not Editing and not NoHayDatos;
     ModificarBtn.Enabled := not Editing and not NoHayDatos;
     BuscarBtn.Enabled := CanLookup and not Editing;
     ImprimirBtn.Enabled := not Editing;
     ImprimirFormaBtn.Enabled := not Editing;
     CortarBtn.Enabled := True;
     CopiarBtn.Enabled := True;
     PegarBtn.Enabled := True;
     UndoBtn.Enabled := Editing;
     OK.Enabled := Editing;
     if Editing then
     begin
          with Cancelar do
          begin
               Kind := bkCancel;
               ModalResult := mrNone;
               Cancel := False;
               Caption := '&Cancelar';
               Hint := 'Cancelar Cambios';
          end;
          if Inserting then
             FocusFirstControl;
     end
     else
     begin
          with Cancelar do
          begin
               Kind := bkClose;
               ModalResult := mrNone;
               Cancel := True;
               Caption := '&Salir';
               Hint := 'Cerrar Pantalla y Salir';
               if Self.Active then  // EZM: S�lo si la forma es visible
                 SetFocus;
          end;
     end;
     Application.ProcessMessages;
end;

procedure TBaseEdicion.FocusFirstControl;
begin
     if Visible and ( FirstControl <> nil ) then
     begin
          with FirstControl do
          begin
               if Visible and Enabled and CanFocus then
                  SetFocus;
          end;
     end;
end;

function TBaseEdicion.Editing: Boolean;
begin
     Result := ( Modo in [ dsInsert, dsEdit ] );
end;

function TBaseEdicion.Inserting: Boolean;
begin
     Result := ( Modo in [ dsInsert ] );
end;

function TBaseEdicion.BotonesActivos: Boolean;
begin
     Result := ( not Editing );
end;

procedure TBaseEdicion.SetModo(const Value: TDatasetState);
begin
     if ( FModo <> Value ) then
     begin
          FModo := Value;
          HabilitaControles;
     end;
end;

function TBaseEdicion.CheckDerechos( const iDerecho: Integer ): Boolean;
begin
     Result := ZAccesosMgr.CheckDerecho( IndexDerechos, iDerecho );
end;

procedure TBaseEdicion.KeyDown( var Key: Word; Shift: TShiftState );
begin
     inherited KeyDown( Key, Shift );
     if ( Key <> 0 ) and BotonesActivos then
     begin
          if ( ssShift in Shift ) then { SHIFT }
          begin
               case Key of
                    VK_INSERT:    { INS = Agregar }
                    begin
                         Key := 0;
                         DoInsert;
                    end;
                    VK_DELETE:    { DEL = Borrar }
                    begin
                         Key := 0;
                         DoDelete;
                    end;
               end;
          end
          else
              if ( ssCtrl in Shift ) then { CTRL }
              begin
                   case Key of
                        VK_INSERT:   { INS = Modificar }
                        begin
                             Key := 0;
                             DoEdit;
                        end;
                        80: { Letra P = Imprimir }
                        begin
                             Key := 0;
                             DoPrint;
                        end;
                        79: { Letra 0 = Imprimir Forma }
                        begin
                             Key := 0;
                             {
                             if ImprimirFormaBtn.Enabled then
                                ImprimirForma;
                             }
                        end;
                        66:  { Letra F = Buscar }
                        begin
                             Key := 0;
                             if CanLookup then
                                DoLookup;
                        end;
                   end;
              end;
     end;
end;

{ ************* Eventos de Controles ************* }

procedure TBaseEdicion.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     with Datasource do
     begin
          if ( Dataset = nil ) then
             Modo := dsInactive
          else
              Modo := Dataset.State;
     end;
end;

procedure TBaseEdicion.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if NoHayDatos then
        Close;
end;

procedure TBaseEdicion.AgregarBtnClick(Sender: TObject);
begin
     inherited;
     DoInsert;
end;

procedure TBaseEdicion.BorrarBtnClick(Sender: TObject);
begin
     inherited;
     DoDelete;
end;

procedure TBaseEdicion.ModificarBtnClick(Sender: TObject);
begin
     inherited;
     DoEdit;
     FocusFirstControl;
end;

procedure TBaseEdicion.BuscarBtnClick(Sender: TObject);
begin
     inherited;
     DoLookup;
end;

procedure TBaseEdicion.ImprimirBtnClick(Sender: TObject);
begin
     inherited;
     DoPrint;
end;

procedure TBaseEdicion.CortarBtnClick(Sender: TObject);
begin
     inherited;
     SendMessage( ActiveControl.Handle, WM_CUT, 0, 0 );
end;

procedure TBaseEdicion.CopiarBtnClick(Sender: TObject);
begin
     inherited;
     SendMessage( ActiveControl.Handle, WM_COPY, 0, 0 );
end;

procedure TBaseEdicion.PegarBtnClick(Sender: TObject);
begin
     inherited;
     SendMessage( ActiveControl.Handle, WM_PASTE, 0, 0 );
end;

procedure TBaseEdicion.UndoBtnClick(Sender: TObject);
begin
     inherited;
     SendMessage( ActiveControl.Handle, WM_UNDO, 0, 0 );
end;

procedure TBaseEdicion.OKClick(Sender: TObject);
begin
     inherited;
     EscribirCambios;
end;

procedure TBaseEdicion.CancelarClick(Sender: TObject);
begin
     inherited;
     if Editing then
        CancelarCambios
     else
         Close;
end;

{ ******** Manejo de Altas, Bajas, Cambios e Impresi�n ********** }

function TBaseEdicion.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     Result := CheckDerechos( K_DERECHO_ALTA );
     if not Result then
        sMensaje := Format( 'No Tiene Permiso Para %s Registros', [ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord(ukInsert) )] );
end;

procedure TBaseEdicion.Agregar;
begin
     ClientDataset.Append;
end;

procedure TBaseEdicion.DoInsert;
var
   sMensaje: String;
begin
     if PuedeAgregar( sMensaje ) then
        Agregar
     else
         ZetaDialogo.zInformation( Caption, sMensaje, 0 );
end;

function TBaseEdicion.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result := CheckDerechos( K_DERECHO_BAJA );
     if not Result then
        sMensaje := Format( 'No Tiene Permiso Para %s Registros', [ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord(ukDelete) )] ) ;
end;

procedure TBaseEdicion.Borrar;
begin
     ClientDataset.Borrar;
end;

procedure TBaseEdicion.DoDelete;
var
   sMensaje: String;
begin
     if NoHayDatos then
        ZetaDialogo.zInformation( Caption, Format('No Hay Datos Para %s', [ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord(ukDelete) )] ), 0 )
     else
         if PuedeBorrar( sMensaje ) then
            Borrar
         else
             ZetaDialogo.zInformation( Caption, sMensaje, 0 );
end;

function TBaseEdicion.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result := CheckDerechos( K_DERECHO_CAMBIO );
     if not Result then
        sMensaje := Format('No Tiene Permiso Para %s Registros', [ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord(ukModify) )] );
end;

procedure TBaseEdicion.Modificar;
begin
     ClientDataset.Edit;
end;

procedure TBaseEdicion.DoEdit;
var
   sMensaje: String;
begin
     if NoHayDatos then
        ZetaDialogo.zInformation( Caption, Format('No Hay Datos Para %s', [ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord(ukModify) )] ), 0 )
     else
         if PuedeModificar( sMensaje ) then
            Modificar
         else
             ZetaDialogo.zInformation( Caption, sMensaje, 0 );
end;

function TBaseEdicion.PuedeImprimir( var sMensaje: String ): Boolean;
begin
     Result := CheckDerechos( K_DERECHO_IMPRESION );
     if not Result then
        sMensaje := 'No Tiene Permiso Para Imprimir Registros';
end;

function TBaseEdicion.ValoresGrid : Variant;
begin
     Result := VarArrayOf( [ textoValorActivo1.Caption,
                             textoValorActivo2.Caption,
                             TipoValorActivo1,
                             TipoValorActivo2 ] ); 
end;

procedure TBaseEdicion.Imprimir;
var
   lImprime: Boolean;
   i: integer;
   Valor : Variant;
begin
     lImprime := False;
     for i := 0 to ( ComponentCount - 1 ) do
     begin
          if ( Components[ i ] is TDBGrid ) then
          begin
               lImprime := True;
               if ZetaDialogo.zConfirm( 'Imprimir...',
                                        '� Desea Imprimir El Grid De La Pantalla ' + Caption + ' ?',
                                        0,
                                        mbYes ) then
               begin
                    Valor := ValoresGrid;
                    FBaseReportes.ImprimirGrid( TDBGrid( Components[ i ] ),
                                                TDBGrid( Components[ i ] ).DataSource.DataSet,
                                                Caption,
                                                'IM',
                                                Valor[0],
                                                Valor[1],
                                                Valor[2],
                                                Valor[3] );

               end;
               Break;
          end;
     end;
     if not lImprime and ZetaDialogo.zConfirm( 'Imprimir...', '� Desea Imprimir La Pantalla ' + Caption + ' ?', 0, mbYes ) then
        Print;
end;

procedure TBaseEdicion.DoPrint;
var
   sMensaje: String;
begin
     if PuedeImprimir( sMensaje ) then
        Imprimir
     else
         ZetaDialogo.zInformation( Caption, sMensaje, 0 );
end;

procedure TBaseEdicion.CancelarCambios;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             DoCancelChanges;
          finally
                 Cursor := oCursor;
          end;
     end;
     if FInsertInicial then
        Close;
end;

procedure TBaseEdicion.EscribirCambios;
var
   oCursor: TCursor;
begin
     with ClientDataset do
     begin
          with Screen do
          begin
               oCursor := Cursor;
               Cursor := crHourglass;
               try
                  Enviar;
               finally
                      Cursor := oCursor;
               end;
          end;
          if ( ChangeCount = 0 ) then
            FInsertInicial := FALSE
          else
              Edit;
     end;
end;

procedure TBaseEdicion.ImprimirForma;
begin
     ZetaDialogo.ZInformation( Caption, 'Esta Pantalla no Cuenta con Impresi�n de Formas', 0 );
end;

procedure TBaseEdicion.DoPrintForma;
var
   sMensaje: String;
begin
     if PuedeImprimir( sMensaje ) then
        ImprimirForma
     else
         ZetaDialogo.zInformation( Caption, sMensaje, 0 );
end;

procedure TBaseEdicion.ImprimirFormaBtnClick(Sender: TObject);
begin
     inherited;
     DoPrintForma;
end;

procedure TBaseEdicion.ExportarBtnClick(Sender: TObject);
begin
     inherited;
     DoExportar;
end;


procedure TBaseEdicion.DoExportar;
var
   sMensaje: String;
begin
     if PuedeImprimir( sMensaje ) then
        Exportar
     else
         ZetaDialogo.zInformation( Caption, 'No Tiene Permiso Para Exportar Registros', 0 );
end;

procedure TBaseEdicion.Exportar;
var
   lExporta: Boolean;
   i: integer;
   Valor : Variant;
begin
     lExporta:= False;
     for i := 0 to ( ComponentCount - 1 ) do
     begin
          if ( Components[ i ] is TDBGrid ) then
          begin
               lExporta:= True;
               if ZetaDialogo.zConfirm( 'Exportar...',
                                        '� Desea exportar a Excel la informaci�n de ' + Caption + ' ?',
                                        0,
                                        mbYes ) then
               begin
                    Valor := ValoresGrid;
                    FBaseReportes.ExportarGrid( TDBGrid( Components[ i ] ),
                                                TDBGrid( Components[ i ] ).DataSource.DataSet,
                                                Caption,
                                                'IM',
                                                Valor[0],
                                                Valor[1],
                                                Valor[2],
                                                Valor[3] );

               end;
               Break;
          end;
     end;
     if not lExporta then
        zInformation( 'Exportar...',Format( 'La Pantalla %s no Contiene Ning�n Grid para Exportar',[Caption] ), 0 );
end;

end.
