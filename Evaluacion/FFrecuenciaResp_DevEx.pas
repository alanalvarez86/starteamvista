unit FFrecuenciaResp_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, ZetaKeyLookup, StdCtrls, Buttons,
  ZetaDBTextBox, TeEngine, Series, TeeProcs, Chart, DBChart, Grids,
  DBGrids, ZetaDBGrid, ComCtrls, Mask, ZetaNumero, ZetaKeyCombo,
  VclTee.TeeGDIPlus, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList,
  Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
  cxSplitter, cxContainer, cxTextEdit, cxMemo, cxDBEdit, cxButtons, cxGroupBox;

type
  TFrecuenciaResp_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    EP_NUMERO1: TZetaKeyLookup;
    Grafica: TDBChart;
    Series1: THorizBarSeries;
    Series2: TPieSeries;
    gbPregunta: TGroupBox;
    lblFolio: TLabel;
    EP_NUMERO: TZetaNumero;
    btnBuscaPregunta: TcxButton;
    lblTexto: TLabel;
    btnFiltrar: TcxButton;
    EV_RELACIO: TZetaKeyCombo;
    EL_NOMBRE: TcxGridDBColumn;
    CUANTOS: TcxGridDBColumn;
    cxSplitter1: TcxSplitter;
    GroupBox1: TGroupBox;
    EP_DESCRIP: TcxMemo;
    procedure FormCreate(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure ZetaDBGrid1TitleClick(Column: TColumn);
    procedure GraficaDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure btnBuscaPreguntaClick(Sender: TObject);
    procedure DoLookupPreguntas;
    procedure EP_NUMEROKeyPress(Sender: TObject; var Key: Char);
    procedure EP_NUMEROExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure CuantosRegistros;
    procedure CreaGrafica;
    procedure CargaFiltros;
    procedure ChecaValoresDefault;
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
  end;

var
  FrecuenciaResp_DevEx: TFrecuenciaResp_DevEx;

implementation

{$R *.DFM}
uses DEvaluacion,
     ZHelpContext,
     ZetaClientTools,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists;

{ TFrecuenciaResp }
procedure TFrecuenciaResp_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEvaluacion;
     with dmEvaluacion do
     begin
          cdsPreguntasEnc.IndexFieldNames := 'EP_NUMERO';
          cdsPreguntasEnc.LookupKeyField := 'EP_NUMERO';
          //EP_NUMERO.LookupDataSet := cdsPreguntasEnc;
     end;
     HelpContext := H_RESULTADOS_ENCUESTA_FRECUENCIA_RESPUESTAS;
end;

procedure TFrecuenciaResp_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[1], 0 ,'' , SkCount );
  //ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE;
  //ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Muestra la caja de agrupamiento
  ApplyMinWidth;
  ZetaDBGridDBTableView.ApplyBestFit();
  //EL_NOMBRE.Options.Grouping:= FALSE;
  //CUANTOS.Options.Grouping:= TRUE;
end;

procedure TFrecuenciaResp_DevEx.Agregar;
begin
     dmEvaluacion.cdsFrecuenciaResp.Agregar;
end;

procedure TFrecuenciaResp_DevEx.Borrar;
begin
     dmEvaluacion.cdsFrecuenciaResp.Borrar;
end;

procedure TFrecuenciaResp_DevEx.Connect;
begin
     with dmEvaluacion do
     begin
          if not cdsFrecuenciaResp.Active or cdsFrecuenciaResp.HayQueRefrescar then
          begin
               cdsPreguntasEnc.Conectar;
               ChecaValoresDefault;
               CargaFiltros;
               CreaGrafica;
               cdsFrecuenciaResp.ResetDataChange;
          end;
          DataSource.DataSet := cdsFrecuenciaResp;
          LlenaPerfilEvaluadores( EV_RELACIO.Lista, TRUE );
          EV_RELACIO.ItemIndex := 0;
     end;

end;

procedure TFrecuenciaResp_DevEx.ChecaValoresDefault;
var
   iEP_NUMERO: Integer;
begin
     iEP_NUMERO := dmEvaluacion.cdsPreguntasEnc.FieldByName('EP_NUMERO').AsInteger;
     if( iEP_NUMERO > 0 )then
     begin
          EP_NUMERO.Valor := iEP_NUMERO;
          EP_DESCRIP.Text := dmEvaluacion.cdsPreguntasEnc.FieldByName('EP_DESCRIP').AsString;
     end;
end;

procedure TFrecuenciaResp_DevEx.CuantosRegistros;
begin
     //StatusBar1.Panels[0].Text := 'Cantidad : ' + inttoStr( dmEvaluacion.cdsFrecuenciaResp.RecordCount );
end;

procedure TFrecuenciaResp_DevEx.ImprimirForma;
begin
  inherited;
end;

procedure TFrecuenciaResp_DevEx.Modificar;
begin
     dmEvaluacion.cdsFrecuenciaResp.Modificar;
end;

function TFrecuenciaResp_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede agregar el registro desde esta forma';
     Result := False;
end;

function TFrecuenciaResp_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede borrar el registro desde esta forma';
     Result := False;
end;

function TFrecuenciaResp_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede modificar el registro desde esta forma';
     Result := False;
end;

procedure TFrecuenciaResp_DevEx.Refresh;
begin
     CargaFiltros;
end;

procedure TFrecuenciaResp_DevEx.CreaGrafica;
const
     K_MAXIMO_COLORES = 16000000;
var
   sNombre: String;
begin
     Randomize;
     with Series2 do
     begin
          with dmEvaluacion.cdsFrecuenciaResp do
          begin
               Clear;
               DisableControls;
               try
                  Last;
                  while not BOF do
                  begin
                       sNombre := FieldByName('EL_NOMBRE').AsString;
                       if( strVacio( sNombre ) )then
                           sNombre := '* Sin respuesta *';
                       AddPie( FieldByName('CUANTOS').AsFloat, sNombre, clDefault {Random( K_MAXIMO_COLORES )}  );
                       Prior;
                  end;
               finally
                      EnableControls;
               end;
          end;
     end;
end;

procedure TFrecuenciaResp_DevEx.btnFiltrarClick(Sender: TObject);
begin
     CargaFiltros;
end;

procedure TFrecuenciaResp_DevEx.CargaFiltros;
var
   iRelacion, iPregunta: Integer;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmEvaluacion.cdsFrecuenciaResp do
        begin
             DisableControls;
             try
                iPregunta := EP_NUMERO.ValorEntero;
                iRelacion := EV_RELACIO.ItemIndex;
                dmEvaluacion.ObtenerFrecuenciaResp( iPregunta, ( iRelacion - 1 ) );
                CreaGrafica;
             finally
                    EnableControls;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     CuantosRegistros;
end;

procedure TFrecuenciaResp_DevEx.ZetaDBGrid1TitleClick(Column: TColumn);
var
   sColumna: String;
begin
     sColumna := Column.FieldName;
end;

procedure TFrecuenciaResp_DevEx.GraficaDragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
     inherited;
     Grafica.View3DOptions.Rotation := 360 + X;
end;

procedure TFrecuenciaResp_DevEx.btnBuscaPreguntaClick(Sender: TObject);
var
   sKey, sDescripcion: String;
begin
     with dmEvaluacion.cdsPreguntasEnc do
     begin
          Search( '', sKey, sDescripcion );
          if( strLleno( sKey ) )then
          begin
               EP_NUMERO.Valor := StrtoInt( sKey );
               EP_DESCRIP.Text := sDescripcion;
          end;
     end;
end;

procedure TFrecuenciaResp_DevEx.DoLookupPreguntas;
begin
     with dmEvaluacion.cdsPreguntasEnc do
     begin
          if( Locate( 'EP_NUMERO', EP_NUMERO.ValorEntero, [] ) )then
              EP_DESCRIP.Text := FieldByName('EP_DESCRIP').AsString
          else
              EP_DESCRIP.Clear;
     end;
end;


procedure TFrecuenciaResp_DevEx.EP_NUMEROKeyPress(Sender: TObject; var Key: Char);
begin
     inherited;
     if ( ( Key = Chr(9) ) or ( Key = Chr( VK_RETURN ) ) ) then
     begin
          DoLookupPreguntas;
     end;
end;

procedure TFrecuenciaResp_DevEx.EP_NUMEROExit(Sender: TObject);
begin
     inherited;
     DoLookupPreguntas;
end;

end.
