unit FCatEscalas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Vcl.StdCtrls,ZBaseGridLectura_DevEx,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxTextEdit, cxClasses, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList,
  cxGridLevel, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TCatEscalas = class(TBaseGridLectura_DevEx)
    SC_CODIGO: TcxGridDBColumn;
    SC_NOMBRE: TcxGridDBColumn;
    SC_NIVELES: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
 private
    procedure ConfigAgrupamiento;
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  CatEscalas: TCatEscalas;

implementation

{$R *.DFM}
uses DEvaluacion,
     ZHelpContext,
     ZetaBuscador_DevEx,
     ZetaCommonLists,
     ZAccesosTress,
     ZetaCommonClasses;

{ TCatEscalas }
procedure TCatEscalas.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_CATALOGOS_ESCALAS;
end;

procedure TCatEscalas.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('SC_CODIGO'), K_SIN_TIPO , '', skCount);
     ConfigAgrupamiento;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatEscalas.Agregar;
begin
     dmEvaluacion.cdsEscalas.Agregar;
     DoBestFit;
end;

procedure TCatEscalas.Borrar;
begin
     dmEvaluacion.cdsEscalas.Borrar;
     DoBestFit;
end;

procedure TCatEscalas.Connect;
begin
     with dmEvaluacion do
     begin
          cdsEscalas.Conectar;
          DataSource.DataSet := cdsEscalas;
     end;
end;

procedure TCatEscalas.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Escala', 'escala', 'SC_CODIGO', dmEvaluacion.cdsEscalas );
end;

procedure TCatEscalas.ImprimirForma;
begin
  inherited;
end;

procedure TCatEscalas.Modificar;
begin
     dmEvaluacion.cdsEscalas.Modificar;
     DoBestFit;
end;

procedure TCatEscalas.Refresh;
begin
     dmEvaluacion.cdsEscalas.Refrescar;
     DoBestFit;
end;

procedure TCatEscalas.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := FALSE; //activa la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := FALSE; //Muestra la caja de agrupamiento
end;

end.
