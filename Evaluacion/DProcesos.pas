unit DProcesos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient, Variants,
{$ifdef DOS_CAPAS}
     DServerEvaluacion,
{$else}
     Evaluacion_TLB,
{$endif}
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaAsciiFile,
     ZetaClientDataSet,
     ZBaseThreads;

type
  TdmProcesos = class(TDataModule)
    cdsASCII: TZetaClientDataSet;
    cdsASCIIRenglon: TStringField;
    cdsDeclaraGlobales: TZetaClientDataSet;
    cdsDataSet: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
{$ifndef DOS_CAPAS}
    FServidorEvaluacion: IdmServerEvaluacionDisp;
{$endif}                             
    FErrorCount: Integer;
    function CorreThreadEvaluacion(const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams): Boolean;
    function GetLista( ZetaDataset: TZetaClientDataset ): OLEVariant;
{$ifdef DOS_CAPAS}
    function GetServerEvalucion: TdmServerEvaluacion;
{$else}
    function GetServerEvaluacion: IdmServerEvaluacionDisp;
{$endif}
    function ProcesarResultado( Info: TProcessInfo ): String;
  protected
    { Protected declarations }
  public
    { Public declarations }
    FListaProcesos: TProcessList;
    property ErrorCount: Integer read FErrorCount;
{$ifdef DOS_CAPAS}
    property ServerEvaluacion: TdmServerEvaluacion read GetServerEvaluacion;
{$else}
    property ServerEvaluacion: IdmServerEvaluacionDisp read GetServerEvaluacion;
{$endif}
    procedure HandleProcessEnd(const iIndice, iValor: Integer);
    //WizAgregaEmpEvaluarGlobal
    function AgregaEmpEvaluarGlobal(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure AgregaEmpEvaluarGlobalGetLista(Parametros: TZetaParams);
    //WizPrepararEvaluaciones
    function PrepararEvaluaciones(Parametros: TZetaParams): Boolean;
    //WizCalcularResultados
    function CalcularResultados( Parametros: TZetaParams ): Boolean;
    //WizRecalcularPromedios
    function RecalcularPromedios( Parametros: TZetaParams ):Boolean;
    //WizCierraEvaluaciones
    function CierraEvaluaciones(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure CierraEvaluacionesGetLista(Parametros: TZetaParams);
    //WizCorreosInvitacion
    function CorreosInvitacion(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure CorreosInvitacionGetLista(Parametros: TZetaParams);
    //WizCorreosNotificacion
    function CorreosNotificacion( Parametros: TZetaParams ): Boolean;
    //WizAsignaRelaciones
    function AsignaRelaciones(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure AsignaRelacionesGetLista(Parametros: TZetaParams);
    //WizCreaEncuestaSimple
    function CreaEncuestaSimple( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    procedure CreaEncuestaSimpleGetLista( Parametros: TZetaParams );
    //WizAgregaEncuesta
    function AgregaEncuesta( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
  end;

var
  dmProcesos: TdmProcesos;

procedure ShowWizard( const eProceso: Procesos );

implementation

uses ZWizardBasico,
     ZcxWizardBasico,
     ZGlobalTress,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaClientTools,
     ZAsciiTools,
     ZetaHayProblemas,
     ZetaDialogo,
     ZetaRegistryCliente,
     ZetaMessages,
     ZetaWizardFeedBack_DevEx,
     //ZetaBorradorSUA, DChavez: no se necesita en evaluacion
     ZetaSystemWorking,
     FTressShell,
     DThreads,
     DGlobal,
     DCliente,
     //MA: Uses para Eval. Desempe�o
     FWizAgregaEmpEvaluar_DevEx,
     FWizPrepararEvaluaciones_DevEx,
     FWizCalcularResultados_DevEx,
     FWizRecalcularPromedios_DevEx,
    // FWizCierraEvaluaciones,   DChavez: No se migro ya no se utiliza
     FWizCorreoInvitacion_DevEx,
     FWizAsignaRelaciones_DevEx,
     FWizCorreosNotificacion_DevEx,
     FWizCreaEncuesta_DevEx,
     ComObj,
     DConsultas;
     
const
     K_FORMATO = '#,###,###,###,##0';

{$R *.DFM}

{ ********** Funciones Globales ************* }

procedure SetDataChange( const Proceso: Procesos );
begin
     with TressShell do
     begin
          case Proceso of
               //MA: Procesos para Eval. Desempe�o
               prDEAgregaEmpEvaluarGlobal: SetDataChange( [ enSujeto, enEvalua ] );
               prDEPrepararEvaluaciones:  SetDataChange( [ enEncuesta ] );
               prDECalcularResultados: SetDatachange( [ enSujComp, enSujPreg ] );
               prDERecalcularPromedios: SetDatachange( [ enEncuesta, enVEvaComp, enVEvaPreg ] );
               prDECierraEvaluaciones: SetDataChange( [ enEncuesta ] );
               prDEAsignaRelaciones: SetDataChange( [ enEvalua, enSujeto ] );
               prDECreaEncuestaSimple: begin
                                            SetDataChange( [ enEncuesta ] );
                                            SetUltimaEncuesta;
                                       end;
          end;
     end;
end;

function GetWizardClass( const eProceso: Procesos ): TWizardBasicoClass;
begin
   { case eProceso of
          //MA: Procesos para Eval. Desempe�o
         // prDEAgregaEmpEvaluarGlobal: Result := TWizAgregaEmpEvaluar;
        //  prDEPrepararEvaluaciones:   Result := TWizPrepararEvaluaciones;
       //   prDECalcularResultados: Result := TWizCalcularResultados;
        // prDERecalcularPromedios: Result := TWizRecalcularPromedios;
          prDECierraEvaluaciones: Result := TWizCierraEvaluaciones;
       //  prDECorreosInvitacion: Result := TWizCorreosInvitacion;
          //prDECorreosNotificacion: Result := TWizCorreosNotificacion;
         // prDEAsignaRelaciones: Result := TWizAsignaRelaciones;
        //  prDECreaEncuestaSimple: Result := TWizCreaEncuesta;

     else
         Result := nil;
     end;    }
end;

function GetCxWizardClass( const eProceso: Procesos ): TCXWizardBasicoClass;
begin
     case eProceso of
          //MA: Procesos para Eval. Desempe�o
          prDEAgregaEmpEvaluarGlobal: Result := TWizAgregaEmpEvaluar_DevEx;
          prDEPrepararEvaluaciones:   Result := TWizPrepararEvaluaciones_DevEx;
          prDECalcularResultados: Result := TWizCalcularResultados_DevEx;
          prDERecalcularPromedios: Result := TWizRecalcularPromedios_DevEx;
          //prDECierraEvaluaciones: Result := TWizCierraEvaluaciones;
          prDECorreosInvitacion: Result := TWizCorreosInvitacion_DevEx;
          prDECorreosNotificacion: Result := TWizCorreosNotificacion_DevEx;
          prDEAsignaRelaciones: Result := TWizAsignaRelaciones_DevEx;
          prDECreaEncuestaSimple: Result := TWizCreaEncuesta_DEvEx;
     else
         Result := nil;
     end;
end;

procedure ShowWizard( const eProceso: Procesos );
var
   WizardClass: TWizardBasicoClass;
   WizardCXClass:TcxWizardBasicoClass;
begin
     dmProcesos.cdsDataSet.Init;
     WizardCXClass := nil;
     WizardCXClass :=  GetCxWizardClass ( eProceso );
     if ( ( WizardClass = nil ) and (WizardCXClass = nil ) ) then { Este IF debe ser Temporal }
        ZetaDialogo.zInformation( 'Estamos trabajando...', 'Esta opci�n no ha sido implementada', 0 )
     else
     begin
          ZCXWizardBasico.ShowWizard( WizardCXClass);
     end;

     TressShell.ReconectaMenu;
     Application.ProcessMessages;
end;

{ *********** TdmProcesos ************ }

procedure TdmProcesos.DataModuleCreate(Sender: TObject);
begin
     FListaProcesos := TProcessList.Create;
end;

procedure TdmProcesos.DataModuleDestroy(Sender: TObject);
begin
     FListaProcesos.Free;
end;

procedure TdmProcesos.HandleProcessEnd( const iIndice, iValor: Integer );
begin
     with TWizardFeedback_DevEx.Create( Application ) do
     begin
          try
             OcultarForma := dmCliente.FormsMoved;
             with FListaProcesos do
             begin
                  try
                     with LockList do
                     begin
                          if ( iIndice >= 0 ) and ( iIndice < Count ) then
                             ProcessData.Assign( TProcessInfo( Items[ iIndice ] ) )
                          else
                              raise Exception.Create( 'Indice De Lista De Procesos Fuera De Rango ( ' + IntToStr( iIndice ) + ' )' );
                     end;
                  finally
                         UnlockList;
                  end;
             end;
             if ProcessOK then
                SetMensajes( Self.ProcesarResultado( ProcessData ) );
             dmConsultas.LeerUnProceso( ShowProcessInfo( True ) );
             if ProcessOK then
                SetDataChange( ProcessData.Proceso );
          finally
                 Free;
          end;
     end;
     TressShell.ReconectaMenu;
     Application.ProcessMessages;
end;

function TdmProcesos.ProcesarResultado( Info: TProcessInfo ): String;
begin
     with Info do
     begin
          case Proceso of
               prIMSSExportarSua:
               begin
                    Result := '';
                    //Result := ExportarSUAEnd( Cargo[ 0 ], Cargo[ 1 ], Cargo[ 2 ] );
               end;
          else
              Result := '';
          end;
     end;
end;

{$ifdef DOS_CAPAS}
function TdmProcesos.GetServerEvaluacion: TdmServerEvaluacion;
begin
     Result := DCliente.dmCliente.ServerEvaluacion;
end;
{$else}
function TdmProcesos.GetServerEvaluacion: IdmServerEvaluacionDisp;
begin
     Result := IdmServerEvaluacionDisp( dmCliente.CreaServidor( CLASS_dmServerEvaluacion, FServidorEvaluacion ) );
end;
{$endif}

function TdmProcesos.GetLista( ZetaDataset: TZetaClientDataset ): OLEVariant;
begin
     with ZetaDataset do
     begin
          MergeChangeLog;  // Elimina Registros Borrados //
          Result := Data;
     end;
end;

{ *********** Procesos de Sistemas ********* }

{function TdmProcesos.Transferencia(Parametros: TZetaParams; const EmpresaDestino: Variant): Boolean;
begin
     Result := CorreThreadCalcNomina( prRHTranferencia, EmpresaDestino, Parametros );
     {
     with TSistemaThread.Create( FListaProcesos ) do
     begin
          Proceso := prRHTranferencia;
          Employees := EmpresaDestino;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
     }
  {   if Result then
        dmCliente.PosicionaSiguienteEmpleado;
end;

function TdmProcesos.CorreThreadSistema(const eProceso: Procesos;  const Lista: OleVariant; Parametros: TZetaParams): Boolean;
begin
     with TSistemaThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.BorrarBitacora(Parametros: TZetaParams): Boolean;
begin
    Result := CorreThreadSistema( prSISTDepurar, NULL, Parametros );
     {
     with TSistemaThread.Create( FListaProcesos ) do
     begin
          Proceso := prSISTDepurar;
          Employees := NULL;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
     }
{end;

function TdmProcesos.NumeroTarjeta: Boolean;
begin
    Result := CorreThreadSistema( prSISTNumeroTarjeta, NULL, NIL );
end;

function TdmProcesos.NumeroTarjetaEnd( const Bitacora: OleVariant ): String;
var
   sArchivo: String;
begin
     sArchivo := Format( '%sActualizarTarjeta.log', [ ExtractFilePath( Application.ExeName ) ] );
     with TStringList.Create do
     begin
          try
             Text := Bitacora;
             SaveToFile( sArchivo );
          finally
                 Free;
          end;
     end;
     ZetaClientTools.ExecuteFile( 'NOTEPAD.EXE', sArchivo, ExtractFileDir( sArchivo ), SW_SHOWDEFAULT );
end;

{ ********** Fin de Procesos de Sistemas ********* }


{ ******** Inicio de Procesos para Eval. Desempe�o ******** }
function TdmProcesos.CorreThreadEvaluacion( const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams ): Boolean;
begin
     with TEvaluacionThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.AgregaEmpEvaluarGlobal(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadEvaluacion( prDEAgregaEmpEvaluarGlobal, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadEvaluacion( prDEAgregaEmpEvaluarGlobal, NULL, Parametros );
end;

procedure TdmProcesos.AgregaEmpEvaluarGlobalGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerEvaluacion.AgregaEmpEvaluarGlobalGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.PrepararEvaluaciones( Parametros: TZetaParams ): Boolean;
begin
     Result := CorreThreadEvaluacion( prDEPrepararEvaluaciones, Null, Parametros );
end;

function TdmProcesos.CalcularResultados( Parametros: TZetaParams ): Boolean;
begin
     Result := CorreThreadEvaluacion( prDECalcularResultados, Null, Parametros );
end;

function TdmProcesos.RecalcularPromedios( Parametros: TZetaParams ):Boolean;
begin
     Result := CorreThreadEvaluacion( prDERecalcularPromedios, Null, Parametros );
end;

function TdmProcesos.CierraEvaluaciones(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadEvaluacion( prDECierraEvaluaciones, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadEvaluacion( prDECierraEvaluaciones, NULL, Parametros );
end;

procedure TdmProcesos.CierraEvaluacionesGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerEvaluacion.CierraEvaluacionesGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.CorreosInvitacion(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadEvaluacion( prDECorreosInvitacion, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadEvaluacion( prDECorreosInvitacion, NULL, Parametros );
end;

procedure TdmProcesos.CorreosInvitacionGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerEvaluacion.CorreosInvitacionGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.CorreosNotificacion( Parametros: TZetaParams ): Boolean;
begin
     Result := CorreThreadEvaluacion( prDECorreosNotificacion, Null, Parametros );
end;

function TdmProcesos.AsignaRelaciones(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadEvaluacion( prDEAsignaRelaciones, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadEvaluacion( prDEAsignaRelaciones, NULL, Parametros );
end;

procedure TdmProcesos.AsignaRelacionesGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerEvaluacion.AsignaRelacionesGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.CreaEncuestaSimple( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadEvaluacion( prDECreaEncuestaSimple, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadEvaluacion( prDECreaEncuestaSimple, NULL, Parametros );
end;

procedure TdmProcesos.CreaEncuestaSimpleGetLista( Parametros: TZetaParams );
begin
     with cdsDataset do
     begin
          Data := ServerEvaluacion.CreaEncuestaSimpleGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.AgregaEncuesta( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadEvaluacion( prDEAgregarEncuesta, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadEvaluacion( prDEAgregarEncuesta, NULL, Parametros );
end;

{ ********** Fin de Procesos para Eval. Desempe�o ********* }

end.



