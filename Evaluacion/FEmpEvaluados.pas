unit FEmpEvaluados;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx,
  Db, ExtCtrls, StdCtrls, Buttons, Mask, ZetaNumero,
  ZetaDBTextBox, Grids, DBGrids, ZetaDBGrid, ComCtrls, ZetaKeyLookup,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxTextEdit, cxClasses, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList,
  cxGridLevel, cxGridCustomView, cxGrid, ZetaCXGrid, ZetaKeyLookup_DevEx;

type
  TEmpEvaluados = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Label3: TLabel;
    CB_CODIGO_GRID: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    SJ_NUM_EVA: TcxGridDBColumn;
    SJ_FIN_EVA: TcxGridDBColumn;
    SJ_STATUS_GRID: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    CB_CODIGO: TZetaKeyLookup_DevEx;
    procedure btnFiltrarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CB_CODIGOValidKey(Sender: TObject);
  private
    { Private declarations }
    procedure ConfigAgrupamiento;
    procedure CargaFiltros;
  protected
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
  end;

var
  EmpEvaluados: TEmpEvaluados;

implementation

{$R *.DFM}

uses DEvaluacion,
     DCliente,
     ZHelpContext,
     ZetaBuscador_DevEx,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaCommonLists;

{ TEmplEvaluados }
procedure TEmpEvaluados.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEvaluacion;
     CB_CODIGO.LookupDataSet := dmCliente.cdsEmpleadoLookup;
     HelpContext := H_RESULTADOS_ENCUESTA_EMPLEADOS_EVALUADOS;
end;

procedure TEmpEvaluados.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO'), K_SIN_TIPO , '', skCount);
     ConfigAgrupamiento;
     Refresh;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TEmpEvaluados.Agregar;
begin
     dmEvaluacion.cdsSujeto.Agregar;
end;

procedure TEmpEvaluados.Borrar;
begin
     dmEvaluacion.cdsSujeto.Borrar;
end;

procedure TEmpEvaluados.Connect;
begin
     with dmEvaluacion do
     begin
          if not cdsSujeto.Active or cdsSujeto.HayQueRefrescar then
          begin
               CargaFiltros;
               cdsSujeto.ResetDataChange;
          end;
          DataSource.DataSet:= cdsSujeto;
     end;
end;

procedure TEmpEvaluados.ImprimirForma;
begin
  inherited;
end;

procedure TEmpEvaluados.Modificar;
begin
     dmEvaluacion.cdsSujeto.Modificar;
     DoBestFit;
end;

procedure TEmpEvaluados.Refresh;
begin
     CargaFiltros;
     DoBestFit;
end;



procedure TEmpEvaluados.btnFiltrarClick(Sender: TObject);
begin
     CargaFiltros;
     DoBestFit;
end;

procedure TEmpEvaluados.CargaFiltros;
var
   iValor: Integer;
   iIndice: Integer;
   sFiltro: String;
   oCursor: TCursor;
begin
     sFiltro := VACIO;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmEvaluacion.cdsSujeto do
        begin
             DisableControls;
             try
                //iValor := znEmpleado.ValorEntero;
                iValor := CB_CODIGO.Valor;
               // iIndice := SJ_STATUS.ItemIndex;
                dmEvaluacion.ObtieneEmpleadosEvaluados( iValor,  - 1  ); //DChavez: se utiliza el menos uno para que busque en todos los campos
             finally
                    EnableControls;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;


procedure TEmpEvaluados.CB_CODIGOValidKey(Sender: TObject);
begin
     inherited;
     CargaFiltros;
     if(dmEvaluacion.cdsSujeto.RecordCount = 0) then
     begin
          zInformation( 'Administrador evaluación', 'No hay datos para mostrar para el empleado seleccionado.',  0 )
     end;
     DoBestFit;
end;


function TEmpEvaluados.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se pueden agregar empleados desde esta forma';
     Result := False;
end;

function TEmpEvaluados.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se pueden borrar empleados desde esta forma';
     Result := False;
end;

function TEmpEvaluados.PuedeModificar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se pueden modificar empleados desde esta forma';
     Result := False;
end;


procedure TEmpEvaluados.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //activa la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Muestra la caja de agrupamiento
     CB_CODIGO_GRID.Options.Grouping  := FALSE;
     PRETTYNAME.Options.Grouping:= FALSE;
     SJ_NUM_EVA.Options.Grouping:= FALSE;
     SJ_FIN_EVA.Options.Grouping:= FALSE;
     SJ_STATUS_GRID.Options.Grouping:= TRUE;
end;

end.
