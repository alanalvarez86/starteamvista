inherited EmpEvaluados: TEmpEvaluados
  Left = 416
  Top = 252
  Caption = 'Empleados evaluados'
  ClientHeight = 279
  ClientWidth = 995
  ExplicitWidth = 995
  ExplicitHeight = 279
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 995
    ExplicitWidth = 995
    inherited ValorActivo2: TPanel
      Width = 736
      ExplicitWidth = 736
      inherited textoValorActivo2: TLabel
        Width = 730
        ExplicitLeft = 650
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 995
    Height = 62
    Align = alTop
    TabOrder = 2
    object Label3: TLabel
      Left = 14
      Top = 24
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empleado:'
    end
    object CB_CODIGO: TZetaKeyLookup_DevEx
      Left = 70
      Top = 19
      Width = 356
      Height = 21
      Hint = 'Busca empleado'
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      ShowHint = True
      TabOrder = 0
      TabStop = True
      WidthLlave = 80
      OnValidKey = CB_CODIGOValidKey
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 81
    Width = 995
    Height = 198
    ExplicitTop = 81
    ExplicitWidth = 995
    ExplicitHeight = 198
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      Styles.Content = cxStyle1
      Styles.Header = cxStyle2
      Styles.Indicator = cxStyle2
      object CB_CODIGO_GRID: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'CB_CODIGO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        MinWidth = 80
        Styles.Content = cxStyle3
        Width = 100
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        MinWidth = 100
        Styles.Content = cxStyle4
        Width = 250
      end
      object SJ_NUM_EVA: TcxGridDBColumn
        Caption = 'Evaluaciones'
        DataBinding.FieldName = 'SJ_NUM_EVA'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        MinWidth = 80
        Styles.Content = cxStyle5
        Width = 100
      end
      object SJ_FIN_EVA: TcxGridDBColumn
        Caption = 'Terminadas'
        DataBinding.FieldName = 'SJ_FIN_EVA'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        MinWidth = 80
        Styles.Content = cxStyle6
        Width = 100
      end
      object SJ_STATUS_GRID: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'SJ_STATUS'
        MinWidth = 80
        Styles.Content = cxStyle7
        Width = 100
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 376
    Top = 8
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 616
    Top = 16
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWindow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
  end
end
