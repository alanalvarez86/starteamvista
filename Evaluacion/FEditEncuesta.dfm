�
 TEDITENCUESTA 0A  TPF0�
TEditEncuestaEditEncuestaLeft�Top� CaptionEncuestaClientHeight�ClientWidth�
ExplicitWidth�ExplicitHeight�
PixelsPerInch`
TextHeight
 �TPanelPanelBotonesTop�Width�ExplicitTop�
ExplicitWidth� �	TcxButtonOK_DevExLeft5ExplicitLeft5  �	TcxButtonCancelar_DevExLeft�ExplicitLeft�   �TPanelPanelIdentificaWidth�
ExplicitWidth� �TPanelValorActivo2Width� 
ExplicitWidth�  �TLabeltextoValorActivo2Width� ExplicitLeft<    �TPanelPanel1Left Top2Width�HeightNAlignalTopTabOrder TLabelLabel1LeftTop
Width(Height
	AlignmenttaRightJustifyCaption   Número:  TLabelET_NOMBRElblLeftTop9Width(Height
	AlignmenttaRightJustifyCaptionNombre:  TZetaTextBox	ztbTituloLeft<TopWidthaHeight	AlignmenttaRightJustifyAutoSizeCaption	ztbTitulo
ShowAccelCharBrush.Color	clBtnFaceBorder	  TLabelET_STATUSlblLeftTop Width!Height
	AlignmenttaRightJustifyCaptionStatus:  TDBEdit	ET_NOMBRELeft<Top4Width�Height	DataField	ET_NOMBRE
DataSource
DataSourceTabOrder  TZetaDBKeyCombo	ET_STATUSLeft<TopWidthbHeight	BevelKindbkFlatStylecsDropDownListCtl3DParentCtl3DTabOrder OnChangeET_STATUSChange	ListaFijalfStatusEncuesta
ListaVariablelvPuestoOffset OpcionalEsconderVacios	DataField	ET_STATUS
DataSource
DataSource
LlaveNumerica	   �TcxPageControlPageControlLeft Top� Width�HeightAlignalClientTabOrderProperties.ActivePagetsGenerales Properties.CustomButtons.Buttons ClientRectBottomClientRectLeftClientRectRight�
ClientRectTop TcxTabSheettsGeneralesCaption	GeneralesFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.Name
MS Sans Serif
Font.Style 
ParentFont TLabel
ET_FEC_INIlblLeftATop~WidthHeight
	AlignmenttaRightJustifyCaptionInicio:  TLabel
ET_FEC_FINlblLeftDTop� WidthHeight
	AlignmenttaRightJustifyCaptionFinal:  TLabel
ET_USR_ADMlblLeftTop� WidthAHeight
	AlignmenttaRightJustifyCaptionResponsable:  TLabel
ET_DOCUMENlblLeftTop� WidthNHeight
	AlignmenttaRightJustifyCaption   Documentación:  TLabel
ET_CONSOLAlblLeft�Top� WidthaHeight	AlignmenttaRightJustifyAutoSizeCaptionMostrar en tablero de avance:WordWrap	  	TcxButtonET_DOCUMENSeekLeft�Top� WidthHeightHint/   Buscar archivo de documentación de la encuestaOptionsImage.Glyph.Data
�  �  BM�      6   (                �                   ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� �������������������������������������������������� ��� ��� ��� ��� ��� ��������������������������������������������������?��� ��� ��� ��� ��� ��� ���?�����������������������������������������������j��� ��� ��� ��� ��� ��� ���j��������������������������������������������������� ��� ��� ��� ��� ��� ������������������������������������������������������� ��� ��� ��� ��� ��� ������������������������������������������������������� ��� ��� ��� ��� ��� ���S���_���_���_���_���_���_���_���_���_���_���_���_��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ����������������������������������������������� ��� ��� ��� ��� ��� ��� ��� �����������������������~���~���~���~���~���~��� ��� ��� ��� ��� ��� ��� ��� �������������������n��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���ParentShowHintShowHint	TabOrderFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.Name
MS Sans Serif
Font.Style 
ParentFontOnClickET_DOCUMENSeekClick  TcxGroupBoxET_DESCRIPgbLeft Top AlignalTopCaption    Descripción: TabOrder HeightuWidth� TDBMemo
ET_DESCRIPLeftTopWidth�Height\AlignalClient	DataField
ET_DESCRIP
DataSource
DataSourceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.Name
MS Sans Serif
Font.Style 
ParentFont
ScrollBars
ssVerticalTabOrder    TZetaDBFecha
ET_FEC_FINLeft_Top� WidthgHeightCursorcrArrowTabOrderText	22-Oct-04Valor      ��@	DataField
ET_FEC_FIN
DataSource
DataSource  TZetaDBFecha
ET_FEC_INILeft_TopyWidthgHeightCursorcrArrowTabOrderText	22-Oct-04Valor      ��@	DataField
ET_FEC_INI
DataSource
DataSource  TZetaDBKeyLookup_DevEx
ET_USR_ADMLeft_Top� WidthoHeightEditarSoloActivosIgnorarConfidencialidadTabOrderTabStop	
WidthLlaveF	DataField
ET_USR_ADM
DataSource
DataSource  TDBEdit
ET_DOCUMENLeft_Top� WidthVHeight	DataField
ET_DOCUMEN
DataSource
DataSourceTabOrder  TDBCheckBox
ET_CONSOLALeft`Top� WidthHeight	Alignment
taLeftJustify	DataField
ET_CONSOLA
DataSource
DataSourceTabOrderValueCheckedSValueUncheckedN   TcxTabSheet	tsEscalasCaptionEscalas
ImageIndex TLabel
ET_ESC_DEFlblLeftTopWidthiHeight
	AlignmenttaRightJustifyCaption   Escala de evaluación:  TLabelET_ESCALAlblLeft/Top-WidthYHeight
	AlignmenttaRightJustifyCaptionNombre de escala:  TLabelET_ESTILOlblLeftTopEWidth� Height
	AlignmenttaRightJustifyCaption   Presentación de respuestas:  TLabel
ET_ESC_PESlblLeftTop^WidthkHeight
	AlignmenttaRightJustifyCaptionEscala de importancia:  TLabel
ET_TXT_COMlblLeftkTop� WidthHeight
	AlignmenttaRightJustifyCaptionTitulo:  TLabel
ET_PTS_CALlblLeftTopxWidthrHeight
	AlignmenttaRightJustifyCaption   Puntos calificación final:  TLabel
ET_ESC_CALlblLeftTop� WidthqHeight
	AlignmenttaRightJustifyCaption   Escala calificación final:  TZetaDBKeyLookup_DevEx
ET_ESC_DEFLeft� TopWidth:HeightEditarSoloActivosIgnorarConfidencialidadTabOrder TabStop	
WidthLlaveF	DataField
ET_ESC_DEF
DataSource
DataSource  TDBEdit	ET_ESCALALeft� Top(Width9Height	DataField	ET_ESCALA
DataSource
DataSourceTabOrder  TZetaDBKeyCombo	ET_ESTILOLeft� TopAWidth� Height	BevelKindbkFlatStylecsDropDownListCtl3DParentCtl3DTabOrder	ListaFijalfEstiloRespuesta
ListaVariablelvPuestoOffset OpcionalEsconderVacios	DataField	ET_ESTILO
DataSource
DataSource
LlaveNumerica	  TZetaDBKeyLookup_DevEx
ET_ESC_PESLeft� TopZWidth:HeightEditarSoloActivosIgnorarConfidencialidadTabOrderTabStop	
WidthLlaveF	DataField
ET_ESC_PES
DataSource
DataSource  TDBEdit
ET_TXT_COMLeft� Top� Width9Height	DataField
ET_TXT_COM
DataSource
DataSourceTabOrder  TDBCheckBox
ET_GET_COMLeft/Top� WidthkHeight	Alignment
taLeftJustifyCaptionPedir comentarios:    	DataField
ET_GET_COM
DataSource
DataSourceTabOrderValueCheckedSValueUncheckedNOnClickET_GET_COMClick  TZetaDBKeyCombo
ET_PTS_CALLeft� ToptWidth� Height	BevelKindbkFlatStylecsDropDownListCtl3DParentCtl3DTabOrderOnClickET_PTS_CALClick	ListaFija	lfNinguna
ListaVariablelvPuestoOffset OpcionalEsconderVacios	DataField
ET_PTS_CAL
DataSource
DataSource
LlaveNumerica	  TZetaDBKeyLookup_DevEx
ET_ESC_CALLeft� Top� Width:HeightEditarSoloActivosIgnorarConfidencialidadTabOrderTabStop	
WidthLlaveF	DataField
ET_ESC_CAL
DataSource
DataSource   TcxTabSheet
tsMensajesCaption
Bienvenida
ImageIndex TcxGroupBoxET_MSG_INIgbLeft Top AlignalClientCaption	 Mensaje TabOrder Height� Width� TDBMemo
ET_MSG_INILeftTopWidth�Height� AlignalClient	DataField
ET_MSG_INI
DataSource
DataSourceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.Name
MS Sans Serif
Font.Style 
ParentFont
ScrollBars
ssVerticalTabOrder     TcxTabSheettsDespedidaCaption	Despedida
ImageIndex TcxGroupBoxET_MSG_FINgbLeft Top AlignalClientCaption	 Mensaje TabOrder Height� Width� TDBMemo
ET_MSG_FINLeftTopWidth�Height� AlignalClient	DataField
ET_MSG_FIN
DataSource
DataSource
ScrollBars
ssVerticalTabOrder      �TDataSource
DataSourceLeft�Top  �TcxImageListcxImageList24_PanelBotones
FormatVersion
DesignInfo� 8 	ImageInfo
Image.Data
:	  6	  BM6	      6   (                 	                  GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��������������JZ��GW��GW��GW��GW��GW��GW��GW��GW��JZ��������������GW��GW��GW��GW��GW��GW��GW��GW������������������JZ��GW��GW��GW��GW��GW��GW��JZ������������������GW��GW��GW��GW��GW��GW��GW��GW����������������������JZ��GW��GW��GW��GW��JZ����������������������GW��GW��GW��GW��GW��GW��GW��GW��GW����������������������JZ��GW��GW��JZ����������������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW����������������������JZ��JZ����������������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW����������������������������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��������������������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��JZ��������������������������JZ��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��JZ����������������������������������JZ��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��JZ������������������������������������������JZ��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��JZ����������������������GW��GW����������������������JZ��GW��GW��GW��GW��GW��GW��GW��GW��GW��JZ����������������������GW��GW��GW��GW����������������������JZ��GW��GW��GW��GW��GW��GW��GW��GW����������������������GW��GW��GW��GW��GW��GW����������������������GW��GW��GW��GW��GW��GW��GW��GW������������������GW��GW��GW��GW��GW��GW��GW��GW������������������GW��GW��GW��GW��GW��GW��GW��GW��������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��	Mask.Data
�   �   BM�       >   (               `                      ���                                                                                                  
Image.Data
:	  6	  BM6	      6   (                 	                  Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�eء�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ��������TӖ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�eء�������������Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Rӕ�����������������pڧ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�������������������������Rӕ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�xݬ�����������������������������Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Z՚���������������������������������bן�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ����������������Oғ�TӖ�����������������Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ��������j٤�Oғ�Oғ�Oғ�_֝����������������Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Rӕ�Oғ�Oғ�Oғ�Oғ�Oғ�eء�������������hآ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�j٤�������������Rӕ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�xݬ�������������Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�pڧ�������������Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�eء������������Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�WԘ������������Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ��߱����Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�	Mask.Data
�   �   BM�       >   (               `                      ���                                                                                                  
Image.Data
:	  6	  BM6	      6   (                 	                  GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��Ra��ly����������������������o|��u���dq��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW������dq������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW����������������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW��o|������������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW������GW��iv��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��Ud��u�������������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��	Mask.Data
�   �   BM�       >   (               `                      ���                                                                                                     �
TdxBarManagerDevEx_BarManagerEdicionDockControlHeights      �TcxImageListcxImageList16Edicion
FormatVersion  TOpenDialog
OpenDialogFilteroAdobe Acrobat (*.pdf)|*.pdf|Microsoft Word (*.doc)|*.doc|Paginas Web (*.htm)|*.htm|Todos los archivos (*.*)|*.*FilterIndex Title   Buscar documentaciiónLeft� Top�    