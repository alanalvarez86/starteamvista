inherited GlobalEvaluacion: TGlobalEvaluacion
  Left = 350
  Top = 300
  Caption = 'Configurar Fechas de Evaluación'
  ClientHeight = 151
  ClientWidth = 298
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 115
    Width = 298
    inherited OK: TBitBtn
      Left = 130
    end
    inherited Cancelar: TBitBtn
      Left = 215
    end
  end
  object GroupBox1: TGroupBox
    Left = 48
    Top = 16
    Width = 209
    Height = 89
    Caption = ' Rango de Evaluación: '
    TabOrder = 1
    object lblDe: TLabel
      Left = 35
      Top = 26
      Width = 17
      Height = 13
      Alignment = taRightJustify
      Caption = 'De:'
    end
    object lblHasta: TLabel
      Left = 21
      Top = 53
      Width = 31
      Height = 13
      Alignment = taRightJustify
      Caption = 'Hasta:'
    end
    object zfFin: TZetaFecha
      Left = 56
      Top = 48
      Width = 129
      Height = 22
      Cursor = crArrow
      TabOrder = 0
      Text = '29/Jul/04'
      UseEnterKey = True
      Valor = 38197
    end
    object zfInicio: TZetaFecha
      Left = 56
      Top = 21
      Width = 129
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '29/Jul/04'
      UseEnterKey = True
      Valor = 38197
    end
  end
end
