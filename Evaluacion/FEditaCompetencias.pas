unit FEditaCompetencias;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls, DBCtrls, ZetaSmartLists, Buttons, StdCtrls,
  ZetaKeyLookup, ZetaEdit, ZetaNumero, Mask, ComCtrls, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, cxControls, dxBarExtItems,
  dxBar, cxClasses, Vcl.ImgList, cxNavigator, cxDBNavigator, cxButtons,
  ZetaKeyLookup_DevEx, dxBarBuiltInMenu, cxPC;

type
  TEditCompetencias = class(TBaseEdicion_DevEx)
    PageControl1: TcxPageControl;
    tsGeneral: TcxTabSheet;
    Label3: TLabel;
    CM_INGLES: TDBEdit;
    CM_NUMERO: TZetaDBNumero;
    Label5: TLabel;
    Label6: TLabel;
    CM_TEXTO: TDBEdit;
    Label4: TLabel;
    tsDetalle: TcxTabSheet;
    CM_DETALLE: TDBMemo;
    Panel1: TPanel;
    Label1: TLabel;
    CM_CODIGO: TZetaDBEdit;
    Label2: TLabel;
    CM_DESCRIP: TDBEdit;
    TC_CODIGO: TZetaDBKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditCompetencias: TEditCompetencias;

implementation

{$R *.DFM}

uses DEvaluacion,
     ZetaBuscador,
     ZetaCommonClasses,
     ZHelpContext,
     ZAccesosTress;

procedure TEditCompetencias.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_EVAL_COMPETEN;
     FirstControl := CM_CODIGO;
     TC_CODIGO.LookupDataset := dmEvaluacion.cdsTCompetencia;
     HelpContext:= H_CATALOGOS_COMPETENCIAS_AGREGAR_REGISTRO;
end;

procedure TEditCompetencias.FormShow(Sender: TObject);
begin
     inherited;
     PageControl1.ActivePageIndex := 0;
end;

procedure TEditCompetencias.Connect;
begin
     with dmEvaluacion do
     begin
          cdsTCompetencia.Conectar;
          DataSource.DataSet := cdsCompetencia;
     end;
end;

procedure TEditCompetencias.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Competencia', 'COMPETEN', 'CM_CODIGO', dmEvaluacion.cdsCompetencia );
end;

end.
