unit FModificaEmpleado_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, StdCtrls, Buttons, ExtCtrls, ZetaKeyCombo, ZetaDBTextBox,
  ZBaseDlgModal, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  Vcl.ImgList, cxButtons;

type
  TModificaEmpleado_DevEx = class(TZetaDlgModal_DevEx)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    ztbTitulo: TZetaTextBox;
    ztbEmpleado: TZetaTextBox;
    ztbEvaluador: TZetaTextBox;
    zkcRelacion: TZetaKeyCombo;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FTipoRelacion: Integer;
    FNombreEvaluador: String;
  public
    { Public declarations }
    property NombreEvaluador : String read FNombreEvaluador write FNombreEvaluador;
    property TipoRelacion: Integer read FTipoRelacion write FTipoRelacion;
  end;

var
  ModificaEmpleado_DevEx: TModificaEmpleado_DevEx;

  procedure AbreModificarEmpleadoEvaluar;

implementation

uses DSistema,
     DCliente,
     DEvaluacion,
     ZetaDialogo;

{$R *.DFM}


procedure AbreModificarEmpleadoEvaluar;
var
   iRelacion: Integer;
   sNomEvaluador: String;
   oModificaEmpleado: TModificaEmpleado_DevEx;
begin
     oModificaEmpleado := TModificaEmpleado_DevEx.Create( Application );
     try
        with oModificaEmpleado do
        begin
             if( dmEvaluacion.ObtieneEvaluador( sNomEvaluador, iRelacion ) )then
             begin
                  NombreEvaluador := sNomEvaluador;
                  TipoRelacion := iRelacion;
                  ShowModal;
             end
             else
                 ZetaDialogo.ZInformation( 'Empleados a Evaluar', 'Se debe de seleccionar un usuario', 0 );
        end;
     finally
            FreeAndNil( oModificaEmpleado );
     end;
end;

procedure TModificaEmpleado_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmEvaluacion do
     begin
          ztbTitulo.Caption := InttoStr( dmCliente.Encuesta ) + ' = ' + dmCliente.Nombre;
          ztbEmpleado.Caption := InttoStr( cdsEmpEvaluar.FieldByName('CB_CODIGO').AsInteger ) + ' = ' +
                                            cdsEmpEvaluar.FieldByName('PRETTYNAME').AsString;
          ztbEvaluador.Caption := FNombreEvaluador;
          LlenaPerfilEvaluadores( zkcRelacion.Lista );
          zkcRelacion.LlaveEntero := TipoRelacion;
     end;
end;

procedure TModificaEmpleado_DevEx.OK_DevExClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmEvaluacion do
        begin
             AgregaSujetoArbol( dmSistema.cdsUsuarios.FieldByName('US_CODIGO').AsInteger, zkcRelacion.LlaveEntero, cdsEmpEvaluar.FieldByName('CB_CODIGO').AsInteger, False );
        end;
     finally
         Screen.Cursor := oCursor;
     end;
end;

procedure TModificaEmpleado_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     dmSistema.cdsUsuarios.Filtered := False;
end;

end.
