CREATE FUNCTION DBO.LISTA_EVALUADORES( @ET_CODIGO FolioGrande )
RETURNS @TABLE_RESULT
TABLE  ( US_CODIGO  Smallint,
         EV_TOTAL   Integer,
         EV_NUEVO   Integer,
         EV_PROCESO Integer,
         EV_TERMINA Integer )
AS 
BEGIN 
	INSERT INTO @TABLE_RESULT
	SELECT US_CODIGO, COUNT(*), 0, 0, 0
	FROM   EVALUA
	WHERE  ET_CODIGO = @ET_CODIGO
	GROUP BY US_CODIGO

	DECLARE  @US_CODIGO Usuario;
	DECLARE  @EV_STATUS Status;
	DECLARE  @Cuantos   Integer;

	DECLARE  CursorEval CURSOR for
	SELECT   US_CODIGO, EV_STATUS, COUNT(*)
	FROM     EVALUA
	WHERE    ET_CODIGO = @ET_CODIGO
	GROUP BY US_CODIGO, EV_STATUS

	OPEN     CursorEval
	FETCH NEXT from CursorEval
	INTO  @US_CODIGO, @EV_STATUS, @Cuantos;
	while ( @@Fetch_Status = 0 )
	BEGIN
		IF ( @EV_STATUS = 4 ) -- Terminados
			UPDATE @TABLE_RESULT
			SET    EV_TERMINA = @Cuantos
			WHERE  US_CODIGO = @US_CODIGO
		else if ( @EV_STATUS = 3 ) -- En proceso
			UPDATE @TABLE_RESULT
			SET    EV_PROCESO = @Cuantos
			WHERE  US_CODIGO = @US_CODIGO
		else
			UPDATE @TABLE_RESULT
			SET    EV_NUEVO = @Cuantos
			WHERE  US_CODIGO = @US_CODIGO
		
		

		FETCH NEXT from CursorEval
		INTO @US_CODIGO, @EV_STATUS, @Cuantos;
	END
	CLOSE CursorEval;
	DEALLOCATE CursorEval;

	RETURN  
END 
GO

CREATE FUNCTION SP_STATUS_EVALUADORES (
  @ET_CODIGO FolioGrande
)
RETURNS @StatusEvaluadores TABLE
( US_CODIGO  Smallint,
  SE_TOTAL   Integer,
  SE_NUEVA   Integer,
  SE_TERMINA Integer,
  SE_PROCESO Integer,
  SE_STATUS  Smallint
) AS     
BEGIN
            INSERT INTO @StatusEvaluadores
            SELECT US_CODIGO, COUNT(*), 
               SUM( CASE WHEN EV_STATUS <= 2 THEN 1 ELSE 0 END ),
               SUM( CASE WHEN EV_STATUS >= 4 THEN 1 ELSE 0 END ),
               SUM( CASE WHEN EV_STATUS = 3 THEN 1 ELSE 0 END ),
               0
            FROM   EVALUA
            WHERE  ET_CODIGO = @ET_CODIGO
            GROUP BY US_CODIGO
 
            UPDATE @StatusEvaluadores
            SET    SE_STATUS = CASE WHEN SE_NUEVA = SE_TOTAL THEN 2
                                WHEN SE_TERMINA = SE_TOTAL THEN 4 ELSE 3 END
 
 
            RETURN
END

