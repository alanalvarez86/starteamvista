CREATE PROCEDURE SP_CUENTA_RESPUESTAS( @EV_FOLIO GUID )
AS
BEGIN
       SET NOCOUNT ON
       
       DECLARE @Respuestas   FolioChico
       DECLARE @Status       Status
       DECLARE @Preguntas    FolioChico
       DECLARE @StatusActual Status
 
       -- Obtiene el n�mero de preguntas
       SELECT @Preguntas = ET_NUM_PRE, @StatusActual = ET_STATUS
       FROM   EVALUA join ENCUESTA on EVALUA.ET_CODIGO = ENCUESTA.ET_CODIGO
       WHERE  EV_FOLIO = @EV_FOLIO
 
       -- Cuenta el n�mero de respuestas ya contestadas por el usuario
       SELECT @Respuestas = COUNT(*)
       FROM   EVA_PREG
       WHERE  EV_FOLIO = @EV_FOLIO AND VP_CUAL > 0 

        if ( @StatusActual <= 1 ) and ( @Respuestas = 0 )
              SET @Status = @StatusActual -- No cambia el status Nuevo
       else if ( @Respuestas = 0 )
              SET @Status = 2  -- Preparada
       else if ( @Respuestas >= @Preguntas )
              SET @Status = 4  -- Terminada
       else
              SET @Status = 3  -- En Proceso 

       UPDATE EVALUA
       SET    EV_FIN_PRE = @Respuestas,
              EV_STATUS  = @Status
       WHERE  EV_FOLIO = @EV_FOLIO

END
