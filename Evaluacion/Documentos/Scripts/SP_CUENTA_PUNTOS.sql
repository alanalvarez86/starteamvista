CREATE PROCEDURE SP_CUENTA_PUNTOS( @EV_FOLIO GUID )
AS
BEGIN
	SET NOCOUNT ON
	-- Anota el valor de las respuestas seg�n su escala sin modificar el status de la evaluaci�n
	UPDATE EVA_PREG
	SET    EVA_PREG.VP_RESPUES = 
		( select EL_VALOR
		  from   V_EVA_PREG
		  where  V_EVA_PREG.EV_FOLIO = @EV_FOLIO and
                         V_EVA_PREG.VC_ORDEN = EVA_PREG.VC_ORDEN and
			 V_EVA_PREG.VP_ORDEN = EVA_PREG.VP_ORDEN )
	WHERE( EV_FOLIO = @EV_FOLIO and VP_CUAL > 0 )
END