SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE PROCEDURE DBO.INVESTIGA_STATUS_ENC( @ET_CODIGO FolioGrande )
BEGIN 	
	DECLARE  @EV_FOLIO GUID;
	DECLARE  @EV_FIN_PRE FolioChico;
	DECLARE  @Cuantos INTEGER;
	DECLARE  @Respuestas Booleano;
	DECLARE  @NumEval INTEGER;
	DECLARE  @STATUS Status;
	DECLARE  @EncCompleta Booleano;
	
	DECLARE  CursorStatusEncuesta CURSOR for
	SELECT EV_FOLIO, EV_FIN_PRE
	FROM EVALUA
	WHERE ET_CODIGO = @ET_CODIGO
	
	SELECT @NumEval = 0;
	SELECT @EncCompleta = 'N';	

	OPEN   CursorStatusEncuesta
	FETCH NEXT from CursorStatusEncuesta
	INTO   @EV_FOLIO, @EV_FIN_PRE;
	WHILE ( @@Fetch_Status = 0 )
	BEGIN		
		SELECT @NumEval = @NumEval + 1

		SELECT @CUANTOS = COUNT(*) 
		FROM   EVA_PREG
		WHERE  EV_FOLIO = @EV_FOLIO AND VP_CUAL > 0
	
		IF( @CUANTOS = 0 )
		BEGIN
	     	     SELECT @RESPUESTAS = 'N'
	     	     BREAK;
		END
		ELSE
		BEGIN	
	     	     IF( @EV_FIN_PRE = @CUANTOS )
			SELECT @ENCCOMPLETA = 'S'
	     	     ELSE	   
	     	     BEGIN
		  	  SELECT @ENCCOMPLETA = 'N'	
	     	  	  SELECT @RESPUESTAS = 'S'		
		  	  BREAK
	             END
	        END				

		FETCH NEXT from CursorStatusEncuesta
		INTO @EV_FOLIO, @EV_FIN_PRE;
	END
	CLOSE CursorStatusEncuesta;
	DEALLOCATE CursorStatusEncuesta;

	if( @NumEval = 0 )
	   SELECT @STATUS = 0 /*Nueva*/
	else
	begin
	     if( ( @Respuestas = 'N' ) and ( @EncCompleta ='N' ) )
		SELECT @STATUS = 1 /*Preparada*/
	     else
	     begin
		  if( @EncCompleta ='N' ) 
		  	SELECT @STATUS = 2 /*En Proceso*/
		  else
			SELECT @STATUS = 3 /*Terminada*/
             end
	end

	UPDATE ENCUESTA
	SET ET_CODIGO = @STATUS
	WHERE ET_CODIGO = @ET_CODIGO
END 


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

