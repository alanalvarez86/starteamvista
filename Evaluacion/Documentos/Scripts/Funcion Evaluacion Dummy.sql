CREATE FUNCTION dbo.SP_EVALUACION_DUMMY(
  @ET_CODIGO  FolioGrande,
  @US_CODIGO  Usuario,
  @CB_CODIGO  NumeroEmpleado )
RETURNS TABLE
AS
RETURN
  (SELECT
         CAST( '00000000-1111-2222-3333-444444444444' AS uniqueidentifier ) EV_FOLIO,
         @US_CODIGO US_CODIGO,
         @ET_CODIGO ET_CODIGO, 
         @CB_CODIGO CB_CODIGO, 
         0 EV_RELACIO, 
         2 EV_STATUS,
 
         ENC_COMP.EC_ORDEN VC_ORDEN,
         'S' VC_INCLUYE,
         ENC_COMP.EC_ORDEN,
 
         ENC_COMP.CM_CODIGO,
         ENC_COMP.EC_PESO,
         ENC_COMP.EC_NOMBRE,
         ENC_COMP.EC_TOT_PES,
         ENC_COMP.EC_NUM_PRE,
 
          ENC_PREG.EP_ORDEN VP_ORDEN,
          0.0 VP_RESPUES,
         0 VP_CUAL,
         ENC_NIVE.EL_NOMBRE,
         ENC_NIVE.EL_VALOR,
          CAST( ' ' AS Varchar(255)) VP_COMENTA,
          ENC_PREG.EP_NUMERO EP_NUMERO,
 
          ENC_PREG.EP_ORDEN,
          ENC_PREG.EP_DESCRIP,
         ENC_PREG.PG_FOLIO,
          ENC_PREG.EP_PESO,
          ENC_PREG.EP_TIPO,
          ENC_PREG.EP_GET_COM,
 
          ENC_RELA.ER_NOMBRE,
          ENC_RELA.ER_TIPO
          
             
  FROM
          ENC_PREG 
              left join ENC_COMP on ENC_PREG.ET_CODIGO = ENC_COMP.ET_CODIGO AND ENC_PREG.EC_ORDEN = ENC_COMP.EC_ORDEN
               left join ENC_RELA on ENC_PREG.ET_CODIGO = ENC_RELA.ET_CODIGO AND ENC_RELA.ER_TIPO = 0
               left join ENC_NIVE on ENC_PREG.ET_CODIGO = ENC_NIVE.ET_CODIGO AND ENC_PREG.EE_CODIGO = ENC_NIVE.EE_CODIGO AND
                                     ENC_NIVE.EL_ORDEN = 0
  WHERE
         ENC_PREG.ET_CODIGO = @ET_CODIGO)
GO
