CREATE PROCEDURE SP_CONTADORES_ENCUESTA( @ET_CODIGO FolioGrande )
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @Niveles   FolioChico
	DECLARE @Pesos     FolioChico
	DECLARE @TotPesos  Numerico
	DECLARE @NumCom    FolioChico
	DECLARE @Preguntas FolioChico
	
	-- Asegura que no hay ponderaciones con valor 0
	UPDATE ENC_PREG
	SET    EP_PESO = 1
	WHERE  EP_PESO <= 0

	UPDATE ENC_COMP
	SET    EC_PESO = 1
	WHERE  EC_PESO <= 0

	-- Actualiza los totales en las competencias a evaluar
	UPDATE ENC_COMP
	SET    EC_NUM_PRE = ( SELECT COUNT(*) 
        	              FROM ENC_PREG 
                	      WHERE ENC_PREG.ET_CODIGO = ENC_COMP.ET_CODIGO AND ENC_PREG.EC_ORDEN = ENC_COMP.EC_ORDEN ),
	       EC_TOT_PES = COALESCE( 
                            ( SELECT SUM(EP_PESO) 
        	              FROM ENC_PREG 
                	      WHERE ENC_PREG.ET_CODIGO = ENC_COMP.ET_CODIGO AND ENC_PREG.EC_ORDEN = ENC_COMP.EC_ORDEN ), 1 )
	WHERE  ET_CODIGO = @ET_CODIGO


	-- Cuenta los Niveles de la escala de evaluaci�n
	SELECT @Niveles = COUNT(*)
	FROM   ENC_NIVE
	WHERE  ET_CODIGO = @ET_CODIGO AND EE_CODIGO = ( SELECT ET_ESC_DEF FROM ENCUESTA WHERE ET_CODIGO = @ET_CODIGO )

	-- Cuenta los Niveles de la escala de importancia
	SELECT @Pesos = COUNT(*)
	FROM   ENC_NIVE
	WHERE  ET_CODIGO = @ET_CODIGO AND EE_CODIGO = ( SELECT ET_ESC_PES FROM ENCUESTA WHERE ET_CODIGO = @ET_CODIGO )

	-- Suma las ponderaciones y las competencias a evaluar. Cuenta las preguntas
	SELECT @TotPesos = SUM(EC_PESO), @NumCom = COUNT(*), @Preguntas = SUM(EC_NUM_PRE)
	FROM   ENC_COMP
	WHERE  ET_CODIGO = @ET_CODIGO

	-- Actualiza los contadores
	-- Se necesita el COALESCE porque los SUM() pueden regresar NULL y campos son NOT NULL
	UPDATE ENCUESTA
	SET    ET_NIVELES = @Niveles,
	       ET_PESOS   = @Pesos,
               ET_TOT_PES = COALESCE( @TotPesos, 1 ),
	       ET_NUM_COM = @NumCom,
	       ET_NUM_PRE = COALESCE( @Preguntas, 0 )
	WHERE  ET_CODIGO  = @ET_CODIGO

	-- Creo que estos no se necesitan dentro de este SP
	-- Solo se deben invocar cuando se agreguen Sujetos o Evaluaciones
	-- EXEC SP_CUENTA_SUJETOS @ET_CODIGO
	-- EXEC SP_CUENTA_EVALUACIONES @ET_CODIGO
END
GO

CREATE PROCEDURE SP_BORRA_ENC_COMP( @ET_CODIGO FolioGrande, @Actual FolioChico )
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM ENC_COMP
	WHERE  ET_CODIGO = @ET_CODIGO and EC_ORDEN = @Actual

	IF ( @@ROWCOUNT = 0 )
		RETURN 

	UPDATE ENC_COMP
	SET    EC_ORDEN = EC_ORDEN-1
	WHERE  ET_CODIGO = @ET_CODIGO and EC_ORDEN > @Actual

	EXEC SP_CONTADORES_ENCUESTA @ET_CODIGO
END
GO

CREATE PROCEDURE SP_BORRA_ENC_PREG( @ET_CODIGO FolioGrande, @EC_ORDEN FolioChico, @Actual FolioChico )
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM ENC_PREG
	WHERE  ET_CODIGO = @ET_CODIGO and EC_ORDEN = @EC_ORDEN and EP_ORDEN = @Actual

	IF ( @@ROWCOUNT = 0 )
		RETURN 

	UPDATE ENC_PREG
	SET    EP_ORDEN = EP_ORDEN-1
	WHERE  ET_CODIGO = @ET_CODIGO and EC_ORDEN = @EC_ORDEN and EP_ORDEN > @Actual
END
GO

CREATE PROCEDURE SP_BORRA_PREGUNTA( @CM_CODIGO Codigo, @Actual FolioChico )
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM PREGUNTA
	WHERE  CM_CODIGO = @CM_CODIGO and PG_ORDEN = @Actual

	IF ( @@ROWCOUNT = 0 )
		RETURN 

	UPDATE PREGUNTA
	SET    PG_ORDEN = PG_ORDEN-1
	WHERE  CM_CODIGO = @CM_CODIGO and PG_ORDEN > @Actual
END
GO

CREATE PROCEDURE SP_CAMBIA_ENC_COMP( @ET_CODIGO FolioGrande, @Actual FolioChico, @Nuevo FolioChico )
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Maximo FolioChico

	IF ( @Nuevo < 1 ) or ( @Actual < 1 ) or ( @Actual = @Nuevo )
		RETURN 

	select @Maximo = MAX( EC_ORDEN )
	from   ENC_COMP
	where  ET_CODIGO = @ET_CODIGO

	if ( @Nuevo > @Maximo ) or ( @Actual > @Maximo )
		RETURN

	UPDATE ENC_COMP
	SET    EC_ORDEN = 0
	WHERE  ET_CODIGO = @ET_CODIGO and EC_ORDEN = @Actual

	UPDATE ENC_COMP
	SET    EC_ORDEN = @Actual
	WHERE  ET_CODIGO = @ET_CODIGO and EC_ORDEN = @Nuevo

	UPDATE ENC_COMP
	SET    EC_ORDEN = @Nuevo
	WHERE  ET_CODIGO = @ET_CODIGO and EC_ORDEN = 0
END
GO


CREATE PROCEDURE SP_CAMBIA_ENC_PREG( @ET_CODIGO FolioGrande, @EC_ORDEN FolioChico, @Actual FolioChico, @Nuevo FolioChico )
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Maximo FolioChico

	IF ( @Nuevo < 1 ) or ( @Actual < 1 ) or ( @Actual = @Nuevo )
		RETURN 

	select @Maximo = MAX( EP_ORDEN )
	from   ENC_PREG
	where  ET_CODIGO = @ET_CODIGO and EC_ORDEN = @EC_ORDEN

	if ( @Nuevo > @Maximo ) or ( @Actual > @Maximo )
		RETURN

	UPDATE ENC_PREG
	SET    EP_ORDEN = 0
	WHERE  ET_CODIGO = @ET_CODIGO and EC_ORDEN = @EC_ORDEN and EP_ORDEN = @Actual

	UPDATE ENC_PREG
	SET    EP_ORDEN = @Actual
	WHERE  ET_CODIGO = @ET_CODIGO and EC_ORDEN = @EC_ORDEN and EP_ORDEN = @Nuevo

	UPDATE ENC_PREG
	SET    EP_ORDEN = @Nuevo
	WHERE  ET_CODIGO = @ET_CODIGO and EC_ORDEN = @EC_ORDEN and EP_ORDEN = 0
END
GO

CREATE PROCEDURE SP_CAMBIA_PREGUNTA( @CM_CODIGO Codigo, @Actual FolioChico, @Nuevo FolioChico )
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Maximo FolioChico

	IF ( @Nuevo < 1 ) or ( @Actual < 1 ) or ( @Actual = @Nuevo )
		RETURN 

	select @Maximo = MAX( PG_ORDEN )
	from   PREGUNTA
	where  CM_CODIGO = @CM_CODIGO

	if ( @Nuevo > @Maximo ) or ( @Actual > @Maximo )
		RETURN

	UPDATE PREGUNTA
	SET    PG_ORDEN = 0
	WHERE  CM_CODIGO = @CM_CODIGO and PG_ORDEN = @Actual

	UPDATE PREGUNTA
	SET    PG_ORDEN = @Actual
	WHERE  CM_CODIGO = @CM_CODIGO and PG_ORDEN = @Nuevo

	UPDATE PREGUNTA
	SET    PG_ORDEN = @Nuevo
	WHERE  CM_CODIGO = @CM_CODIGO and PG_ORDEN = 0
END
GO

CREATE PROCEDURE SP_TOTALIZA_SUJETO( @ET_CODIGO FolioGrande, @CB_CODIGO NumeroEmpleado )
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @FinEval      FolioGrande
	DECLARE @Promedio     CuatroDecimales
	DECLARE @NumEval      FolioGrande
	DECLARE @Status       Status
	DECLARE @StatusActual Status

	-- Por lo pronto hago el promedio sin poderar el peso de cada evaluador
	-- Ya existe el campo ENC_RELA.ER_PESO
	-- Falta considerar que puede haber 0 o varios evaluadores de la misma relaci�n

	SELECT  @FinEval = COUNT(*)
	FROM    EVALUA
	WHERE   ET_CODIGO = @ET_CODIGO and CB_CODIGO = @CB_CODIGO and
		EV_STATUS = 4

	SELECT  @Promedio = ROUND( AVG(EV_TOTAL), 3 )
	FROM    EVALUA
	WHERE   ET_CODIGO = @ET_CODIGO and CB_CODIGO = @CB_CODIGO and
		EV_STATUS = 4 and EV_TOTAL > 0


	-- Obtiene valores actuales de Status y Total de Evaluaciones
	SELECT  @NumEval = SJ_NUM_EVA,
		@StatusActual  = SJ_STATUS
	FROM    SUJETO
	WHERE   ET_CODIGO = @ET_CODIGO and CB_CODIGO = @CB_CODIGO


        if ( @StatusActual <= 1 )
		SET @Status = @StatusActual	-- Respeta Nuevo y Listo
	else if ( @FinEval = 0 )
		SET @Status = 2  -- Sin evaluar
	else if ( @FinEval >= @NumEval )
		SET @Status = 4  -- Terminado
	else
		SET @Status = 3  -- En Proceso

	UPDATE  SUJETO
	SET     SJ_FIN_EVA = @FinEval,
		SJ_TOTAL   = COALESCE( @Promedio, 0 ),
		SJ_STATUS  = @Status
	WHERE   ET_CODIGO = @ET_CODIGO and @CB_CODIGO = CB_CODIGO
END
GO

CREATE PROCEDURE SP_TOTALIZA_ENCUESTA( @ET_CODIGO FolioGrande )
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Sujetos FolioGrande
	DECLARE @Evaluas FolioGrande
	DECLARE @FinSuj  FolioGrande
	DECLARE @FinEval FolioGrande

	-- Cuenta los Sujetos a evaluar
	SELECT @Sujetos = COUNT(*)
	FROM   SUJETO
	WHERE  ET_CODIGO = @ET_CODIGO

	-- Cuenta las evaluaciones
	SELECT @Evaluas = COUNT(*)
	FROM   EVALUA
	WHERE  ET_CODIGO = @ET_CODIGO

	-- Cuenta los Sujetos que ya terminaron
	SELECT @FinSuj = COUNT(*)
	FROM   SUJETO
	WHERE  ET_CODIGO = @ET_CODIGO and SJ_STATUS = 4

	-- Cuenta las evaluaciones que ya terminaron
	SELECT @FinEval = COUNT(*)
	FROM   EVALUA
	WHERE  ET_CODIGO = @ET_CODIGO and EV_STATUS = 4
	
	UPDATE ENCUESTA
	SET    ET_NUM_SUJ = @Sujetos,
               ET_NUM_EVA = @Evaluas,
               ET_FIN_SUJ = @FinSuj,
               ET_FIN_EVA = @FinEval
	WHERE  ET_CODIGO = @ET_CODIGO
END
GO

CREATE PROCEDURE SP_CONTADORES_SUJETO( @ET_CODIGO FolioGrande, @CB_CODIGO NumeroEmpleado )
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE SUJETO
	SET    SJ_NUM_EVA = ( SELECT COUNT(*)
                              FROM   EVALUA
                              WHERE  EVALUA.ET_CODIGO = SUJETO.ET_CODIGO and
                                     EVALUA.CB_CODIGO = SUJETO.CB_CODIGO )
	WHERE  ET_CODIGO = @ET_CODIGO and CB_CODIGO = @CB_CODIGO

	EXEC SP_TOTALIZA_SUJETO @ET_CODIGO, @CB_CODIGO

	-- Actualiza los Totales de la Encuesta general
	EXEC SP_TOTALIZA_ENCUESTA @ET_CODIGO
END
GO

CREATE PROCEDURE SP_CUENTA_EVALUACIONES( @ET_CODIGO FolioGrande )
AS
BEGIN
	SET NOCOUNT ON

	-- Cuenta los Sujetos a evaluar
	UPDATE ENCUESTA
	SET    ET_NUM_EVA = ( SELECT COUNT(*)
			      FROM  EVALUA
			      WHERE ET_CODIGO = ENCUESTA.ET_CODIGO )
	WHERE  ET_CODIGO = @ET_CODIGO
END
GO

CREATE PROCEDURE SP_COPIA_EVALUADORES( @Anterior FolioGrande, @Nueva FolioGrande )
AS
BEGIN
	SET NOCOUNT ON

	-- Copia las evaluaciones, sin competencias ni respuestas
	-- Deja status 0 = Nueva
	INSERT INTO EVALUA
	  ( EV_FOLIO,   US_CODIGO, ET_CODIGO, CB_CODIGO, EV_RELACIO, EV_STATUS )
	SELECT NEWID(), US_CODIGO, @Nueva,    CB_CODIGO, EV_RELACIO, 0
	FROM   EVALUA
	WHERE  ET_CODIGO = @Anterior

	EXEC SP_CUENTA_EVALUACIONES @Nueva
END
GO

CREATE PROCEDURE SP_CUENTA_SUJETOS( @ET_CODIGO FolioGrande )
AS
BEGIN
	SET NOCOUNT ON

	-- Cuenta los Sujetos a evaluar
	UPDATE ENCUESTA
	SET    ET_NUM_SUJ = ( SELECT COUNT(*)
			      FROM SUJETO
			      WHERE ET_CODIGO = ENCUESTA.ET_CODIGO )
	WHERE  ET_CODIGO = @ET_CODIGO
END
GO

CREATE PROCEDURE SP_COPIA_SUJETOS( @Anterior FolioGrande, @Nueva FolioGrande, @MarcaListos Booleano )
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Tope Status
	if ( @MarcaListos = 'S' ) 
		set @Tope = 1
	else
		set @Tope = 0

	-- Copia los sujetos a evaluar, sin totales
	-- Emulando la funci�n de MIN( SJ_STATUS, @Tope )
	INSERT INTO SUJETO
	  ( ET_CODIGO, CB_CODIGO, SJ_STATUS )
	SELECT @Nueva, CB_CODIGO, CASE WHEN SJ_STATUS >= @Tope THEN @Tope ELSE SJ_STATUS END
	FROM   SUJETO
	WHERE  ET_CODIGO = @Anterior

	EXEC SP_CUENTA_SUJETOS @Nueva
END
GO


CREATE PROCEDURE SP_COPIA_ENCUESTA( @Anterior FolioGrande, @Nueva FolioGrande, @Nombre DescLarga, @Sujetos Booleano, @Evaluadores Booleano )
AS
BEGIN
	SET NOCOUNT ON

	-- Copia la tabla principal ENCUESTA
	INSERT INTO ENCUESTA
	  ( ET_CODIGO, ET_NOMBRE, ET_STATUS, ET_FEC_INI, ET_FEC_FIN, ET_DESCRIP, ET_USR_ADM, ET_ESCALA, ET_MSG_INI, ET_MSG_FIN, ET_GET_COM, ET_TXT_COM, ET_ESTILO, ET_ESC_DEF, ET_ESC_PES )
	SELECT @Nueva, @Nombre, 0,           ET_FEC_INI, ET_FEC_FIN, ET_DESCRIP, ET_USR_ADM, ET_ESCALA, ET_MSG_INI, ET_MSG_FIN, ET_GET_COM, ET_TXT_COM, ET_ESTILO, ET_ESC_DEF, ET_ESC_PES
	FROM   ENCUESTA
	WHERE  ET_CODIGO = @Anterior

	-- Copia la tabla de Escalas
	INSERT INTO ENC_ESCA
	  ( ET_CODIGO, EE_CODIGO, EE_NOMBRE, EE_NIVELES )
	SELECT @Nueva, EE_CODIGO, EE_NOMBRE, EE_NIVELES
	FROM   ENC_ESCA
	WHERE  ET_CODIGO = @Anterior

	-- Copia los valores de las Escalas
	INSERT INTO ENC_NIVE
	  ( ET_CODIGO, EE_CODIGO, EL_ORDEN, EL_NOMBRE, EL_CORTO, EL_DESCRIP, EL_VALOR )
	SELECT @Nueva, EE_CODIGO, EL_ORDEN, EL_NOMBRE, EL_CORTO, EL_DESCRIP, EL_VALOR
	FROM   ENC_NIVE
	WHERE  ET_CODIGO = @Anterior

	-- Copia tabla de Relaciones con Evaluador
	INSERT INTO ENC_RELA
	  ( ET_CODIGO, ER_TIPO, ER_NUM_MIN, ER_NUM_MAX, ER_NOMBRE, ER_PESO, ER_POS_PES )
	SELECT @Nueva, ER_TIPO, ER_NUM_MIN, ER_NUM_MAX, ER_NOMBRE, ER_PESO, ER_POS_PES
	FROM   ENC_RELA
	WHERE  ET_CODIGO = @Anterior

	-- Copia la tabla de Competencias
	INSERT INTO ENC_COMP
	  ( ET_CODIGO, EC_ORDEN, CM_CODIGO, EC_NOMBRE, EC_DETALLE, EC_NUM_PRE, EC_GET_COM, EC_PESO, EC_TOT_PES, EC_POS_PES )
	SELECT @Nueva, EC_ORDEN, CM_CODIGO, EC_NOMBRE, EC_DETALLE, EC_NUM_PRE, EC_GET_COM, EC_PESO, EC_TOT_PES, EC_POS_PES
	FROM   ENC_COMP
	WHERE  ET_CODIGO = @Anterior

	-- Copia la tabla de Preguntas por competencia
	INSERT INTO ENC_PREG
	  ( ET_CODIGO, EC_ORDEN, EP_ORDEN, EP_NUMERO, EP_DESCRIP, PG_FOLIO, EP_TIPO, EP_GET_COM, EP_PESO, EP_GET_NA, EP_POS_PES, EE_CODIGO )
	SELECT @Nueva, EC_ORDEN, EP_ORDEN, EP_NUMERO, EP_DESCRIP, PG_FOLIO, EP_TIPO, EP_GET_COM, EP_PESO, EP_GET_NA, EP_POS_PES, EE_CODIGO
	FROM   ENC_PREG
	WHERE  ET_CODIGO = @Anterior

	-- Calcula los contadores de la encuesta Nueva
	EXEC SP_CONTADORES_ENCUESTA @Nueva

	if ( @Sujetos = 'S' )
		EXEC SP_COPIA_SUJETOS @Anterior, @Nueva, @Evaluadores

	if ( @Evaluadores = 'S' )
		EXEC SP_COPIA_EVALUADORES @Anterior, @Nueva
END
GO

CREATE PROCEDURE SP_CREA_ENC_PREG( @ET_CODIGO FolioGrande, @EC_ORDEN FolioChico, @CM_CODIGO Codigo )
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO ENC_PREG
	  ( ET_CODIGO, EC_ORDEN, EP_ORDEN, PG_FOLIO, EP_DESCRIP, EP_TIPO, EP_PESO, EP_POS_PES, EE_CODIGO )
	SELECT @ET_CODIGO, @EC_ORDEN, PG_ORDEN, PG_FOLIO, PG_DESCRIP, PG_TIPO, 1, 1, ET_ESC_DEF
	FROM   PREGUNTA, ENCUESTA
	WHERE  CM_CODIGO = @CM_CODIGO AND ENCUESTA.ET_CODIGO = @ET_CODIGO
END
GO

CREATE PROCEDURE SP_CREA_ENC_COMP( @ET_CODIGO FolioGrande, @EC_ORDEN FolioChico, @CM_CODIGO Codigo )
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO ENC_COMP
	  ( ET_CODIGO, EC_ORDEN, CM_CODIGO, EC_NOMBRE, EC_DETALLE, EC_PESO, EC_POS_PES )
	SELECT @ET_CODIGO, @EC_ORDEN, CM_CODIGO, CM_DESCRIP, CM_DETALLE, 1, 1
	FROM   COMPETEN
	WHERE  CM_CODIGO = @CM_CODIGO

	EXEC SP_CREA_ENC_PREG @ET_CODIGO, @EC_ORDEN, @CM_CODIGO


END
GO

CREATE PROCEDURE SP_CREA_ENC_ESCA( @ET_CODIGO FolioGrande, @SC_CODIGO Codigo )
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO ENC_ESCA ( ET_CODIGO, EE_CODIGO, EE_NOMBRE, EE_NIVELES )
	SELECT @ET_CODIGO, SC_CODIGO, SC_NOMBRE, SC_NIVELES
	FROM   ESCALA
	WHERE SC_CODIGO = @SC_CODIGO


	INSERT INTO ENC_NIVE ( ET_CODIGO, EE_CODIGO, EL_ORDEN, EL_NOMBRE, EL_CORTO, EL_DESCRIP, EL_VALOR )
	SELECT @ET_CODIGO, SC_CODIGO, SN_ORDEN, SN_NOMBRE, SN_CORTO, SN_DESCRIP, SN_VALOR
	FROM   ESCNIVEL
	WHERE  SC_CODIGO = @SC_CODIGO
END
GO

CREATE PROCEDURE SP_CREA_ENC_PESO( @ET_CODIGO FolioGrande, @SC_CODIGO Codigo )
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO ENC_PESO
	  ( ET_CODIGO, ES_ORDEN, ES_NOMBRE, ES_CORTO )
	SELECT @ET_CODIGO, SN_ORDEN, SN_NOMBRE, SN_CORTO
	FROM   ESCNIVEL
	WHERE  SC_CODIGO = @SC_CODIGO
END
GO

CREATE PROCEDURE SP_CREA_ENC_RELA( @ET_CODIGO FolioGrande, @ER_ORDEN FolioChico, @ER_TIPO Status, @ER_NOMBRE Descripcion )
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO ENC_RELA
	  ( ET_CODIGO, ER_TIPO, ER_NOMBRE, ER_NUM_MIN, ER_NUM_MAX, ER_PESO, ER_POS_PES )
	VALUES
	  ( @ET_CODIGO, @ER_TIPO, @ER_NOMBRE, 1, 1, 1, 1 )

END
GO

CREATE PROCEDURE SP_CREA_ENCNIVEL( @ET_CODIGO FolioGrande, @SC_CODIGO Codigo )
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO ENCNIVEL
	  ( ET_CODIGO, EL_ORDEN, EL_NOMBRE, EL_CORTO, EL_VALOR )
	SELECT @ET_CODIGO, SN_ORDEN, SN_NOMBRE, SN_CORTO, SN_ORDEN
	FROM   ESCNIVEL
	WHERE  SC_CODIGO = @SC_CODIGO
END
GO

CREATE PROCEDURE SP_CUENTA_RESPUESTAS( @EV_FOLIO GUID )
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @Respuestas   FolioChico
	DECLARE @Status       Status
	DECLARE @Preguntas    FolioChico
        DECLARE @StatusActual Status

	-- Obtiene el n�mero de preguntas
	SELECT @Preguntas = ET_NUM_PRE, @StatusActual = ET_STATUS
	FROM   EVALUA join ENCUESTA on EVALUA.ET_CODIGO = ENCUESTA.ET_CODIGO
	WHERE  EV_FOLIO = @EV_FOLIO

	-- Cuenta el n�mero de respuestas ya contestadas por el usuario
	SELECT @Respuestas = COUNT(*)
        FROM   EVA_PREG
        WHERE  EV_FOLIO = @EV_FOLIO AND
               VP_CUAL > 0

        if ( @StatusActual <= 1 )
		SET @Status = @StatusActual -- No cambia el status Nuevo
	else if ( @Respuestas = 0 )
		SET @Status = 2  -- Preparada
	else if ( @Respuestas >= @Preguntas )
		SET @Status = 4  -- Terminada
	else
		SET @Status = 3  -- En Proceso

	UPDATE EVALUA
	SET    EV_FIN_PRE = @Respuestas,
               EV_STATUS  = @Status
	WHERE  EV_FOLIO = @EV_FOLIO
END
GO

CREATE PROCEDURE SP_NUMERA_PREGUNTAS( @ET_CODIGO FolioGrande )
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Posicion FolioChico
	DECLARE @Numero   FolioChico
	SET @Posicion = 0;
	

	DECLARE  CursorPreguntas CURSOR for
	SELECT   EP_NUMERO
	FROM     ENC_PREG
	WHERE    ET_CODIGO = @ET_CODIGO
	ORDER BY EC_ORDEN, EP_ORDEN

	OPEN     CursorPreguntas
	FETCH NEXT from CursorPreguntas
	INTO  @Numero
	while ( @@Fetch_Status = 0 )
	BEGIN
		SET @Posicion = @Posicion + 1

		UPDATE ENC_PREG
		SET    EP_NUMERO = @Posicion
		WHERE CURRENT OF CursorPreguntas

		FETCH NEXT from CursorPreguntas
		INTO @Numero
	END
	CLOSE CursorPreguntas;
	DEALLOCATE CursorPreguntas;
END
GO

CREATE PROCEDURE SP_PREPARA_EVALUACION( @EV_FOLIO GUID )
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ET_CODIGO FolioGrande

	select  @ET_CODIGO = ET_CODIGO
	from    EVALUA
	where   EV_FOLIO = @EV_FOLIO

	-- Limpia tablas hijas por si es la segunda vez que se prepara
	-- Por 'cascade' se hace el delete en EVA_PREG
	DELETE FROM EVA_COMP WHERE EV_FOLIO = @EV_FOLIO

	-- Agrega competencias a evaluar
	INSERT INTO EVA_COMP ( EV_FOLIO, VC_ORDEN, EC_ORDEN, VC_INCLUYE )
	SELECT @EV_FOLIO, EC_ORDEN, EC_ORDEN, 'S'
	FROM   ENC_COMP
	WHERE  ET_CODIGO = @ET_CODIGO

	-- Agrega preguntas a evaluar. VP_CUAL = 0 indica que no se ha contestado, VP_RESPUES = 0 indica N/A
	INSERT INTO EVA_PREG ( EV_FOLIO, VC_ORDEN, VP_ORDEN, EP_NUMERO, VP_RESPUES, VP_CUAL )
	SELECT @EV_FOLIO, EC_ORDEN, EP_ORDEN, EP_NUMERO, 0, 0
	FROM   ENC_PREG
	WHERE  ET_CODIGO = @ET_CODIGO

	-- Marca como Status = 2 Preparada, e inicializa totales
	UPDATE EVALUA
	SET    EV_STATUS = 2, 
               EV_TOTAL = 0, 
               EV_COMENTA = NULL, 
               EV_FEC_INI = '12/30/1899', 
               EV_FEC_FIN = '12/30/1899', 
               EV_FIN_PRE = 0
	WHERE  EV_FOLIO = @EV_FOLIO
END
GO

CREATE PROCEDURE SP_PREPARA_EVALUACIONES( @ET_CODIGO FolioGrande )
AS
BEGIN
	SET NOCOUNT ON

	EXEC SP_NUMERA_PREGUNTAS @ET_CODIGO

	DECLARE @EV_FOLIO GUID

	DECLARE  CursorEval CURSOR for
	SELECT   EV_FOLIO
	FROM     EVALUA
	WHERE    ET_CODIGO = @ET_CODIGO AND EV_STATUS <= 2

	OPEN     CursorEval
	FETCH NEXT from CursorEval
	INTO  @EV_FOLIO;
	while ( @@Fetch_Status = 0 )
	BEGIN
		EXEC SP_PREPARA_EVALUACION @EV_FOLIO

		FETCH NEXT from CursorEval
		INTO @EV_FOLIO;
	END
	CLOSE CursorEval;
	DEALLOCATE CursorEval;

	/* 16/Jun/05. Ya no hay cambio de ET_STATUS
	-- Pasa Status de 0=Nueva a 2 = Preparada
	UPDATE ENCUESTA
	SET    ET_STATUS = 2
	WHERE  ET_CODIGO = @ET_CODIGO and ET_STATUS <= 1
	*/
END
GO

CREATE PROCEDURE SP_PREPARA_SUJETO( @ET_CODIGO FolioGrande, @CB_CODIGO NumeroEmpleado )
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @EV_FOLIO GUID

	DECLARE  CursorEval CURSOR for
	SELECT   EV_FOLIO
	FROM     EVALUA
	WHERE    ET_CODIGO = @ET_CODIGO and CB_CODIGO = @CB_CODIGO and EV_STATUS <= 1

	OPEN     CursorEval
	FETCH NEXT from CursorEval
	INTO  @EV_FOLIO;
	while ( @@Fetch_Status = 0 )
	BEGIN
		EXEC SP_PREPARA_EVALUACION @EV_FOLIO

		FETCH NEXT from CursorEval
		INTO @EV_FOLIO;
	END
	CLOSE CursorEval;
	DEALLOCATE CursorEval;


	EXEC SP_CONTADORES_SUJETO @ET_CODIGO, @CB_CODIGO

	-- Pasa status de 1=Listo para evaluar a 2=Preparado
	UPDATE SUJETO
	SET    SJ_STATUS = 2 -- Preparado
	WHERE  ET_CODIGO = @ET_CODIGO and @CB_CODIGO = CB_CODIGO and SJ_STATUS = 1
END
GO

CREATE PROCEDURE SP_PREPARA_SUJETOS( @ET_CODIGO FolioGrande )
AS
BEGIN
	SET NOCOUNT ON

	EXECUTE SP_NUMERA_PREGUNTAS @ET_CODIGO

	DECLARE @CB_CODIGO NumeroEmpleado

	DECLARE  CursorSujeto CURSOR for
	SELECT   CB_CODIGO
	FROM     SUJETO
	WHERE    ET_CODIGO = @ET_CODIGO AND SJ_STATUS >= 1

	OPEN     CursorSujeto
	FETCH NEXT from CursorSujeto
	INTO  @CB_CODIGO;
	while ( @@Fetch_Status = 0 )
	BEGIN
		EXEC SP_PREPARA_SUJETO @ET_CODIGO, @CB_CODIGO

		FETCH NEXT from CursorSujeto
		INTO @CB_CODIGO;
	END
	CLOSE CursorSujeto;
	DEALLOCATE CursorSujeto;

	/* 16/Jun/05. Ya no hay cambio de ET_STATUS
	-- Pasa Status de 0=Nueva a 2 = Preparada
	UPDATE ENCUESTA
	SET    ET_STATUS = 2
	WHERE  ET_CODIGO = @ET_CODIGO and ET_STATUS <= 1
	*/
	
END
GO

CREATE PROCEDURE SP_RESUMEN_COMP( @ET_CODIGO FolioGrande, @CB_CODIGO NumeroEmpleado, @EC_ORDEN FolioChico, 
				  @EV_RELACIO Status, @Cuantos NumeroEmpleado, @Suma CuatroDecimales )
AS
BEGIN
	if ( @EV_RELACIO = 0 )
		UPDATE SUJ_COMP
		SET    SO_NUM_R0  = SO_NUM_R0  + @Cuantos,
		       SO_SUM_R0  = SO_SUM_R0  + @Suma,
                       SO_NUM_TOT = SO_NUM_TOT + @Cuantos,
                       SO_SUM_TOT = SO_SUM_TOT + @Suma
		WHERE  ET_CODIGO = @ET_CODIGO AND CB_CODIGO = @CB_CODIGO AND EC_ORDEN = @EC_ORDEN
	else if ( @EV_RELACIO = 1)
		UPDATE SUJ_COMP
		SET    SO_NUM_R1  = SO_NUM_R1  + @Cuantos,
		       SO_SUM_R1  = SO_SUM_R1  + @Suma,
                       SO_NUM_TOT = SO_NUM_TOT + @Cuantos,
                       SO_SUM_TOT = SO_SUM_TOT + @Suma
		WHERE  ET_CODIGO = @ET_CODIGO AND CB_CODIGO = @CB_CODIGO AND EC_ORDEN = @EC_ORDEN
	else if ( @EV_RELACIO = 2 )
		UPDATE SUJ_COMP
		SET    SO_NUM_R2  = SO_NUM_R2  + @Cuantos,
		       SO_SUM_R2  = SO_SUM_R2  + @Suma,
                       SO_NUM_TOT = SO_NUM_TOT + @Cuantos,
                       SO_SUM_TOT = SO_SUM_TOT + @Suma
		WHERE  ET_CODIGO = @ET_CODIGO AND CB_CODIGO = @CB_CODIGO AND EC_ORDEN = @EC_ORDEN
	else if ( @EV_RELACIO = 3 )
		UPDATE SUJ_COMP
		SET    SO_NUM_R3  = SO_NUM_R3  + @Cuantos,
		       SO_SUM_R3  = SO_SUM_R3  + @Suma,
                       SO_NUM_TOT = SO_NUM_TOT + @Cuantos,
                       SO_SUM_TOT = SO_SUM_TOT + @Suma
		WHERE  ET_CODIGO = @ET_CODIGO AND CB_CODIGO = @CB_CODIGO AND EC_ORDEN = @EC_ORDEN
	else if ( @EV_RELACIO = 4 )
		UPDATE SUJ_COMP
		SET    SO_NUM_R4  = SO_NUM_R4  + @Cuantos,
		       SO_SUM_R4  = SO_SUM_R4  + @Suma,
                       SO_NUM_TOT = SO_NUM_TOT + @Cuantos,
                       SO_SUM_TOT = SO_SUM_TOT + @Suma
		WHERE  ET_CODIGO = @ET_CODIGO AND CB_CODIGO = @CB_CODIGO AND EC_ORDEN = @EC_ORDEN
	else 
		UPDATE SUJ_COMP
		SET    SO_NUM_R5  = SO_NUM_R5  + @Cuantos,
		       SO_SUM_R5  = SO_SUM_R5  + @Suma,
                       SO_NUM_TOT = SO_NUM_TOT + @Cuantos,
                       SO_SUM_TOT = SO_SUM_TOT + @Suma
		WHERE  ET_CODIGO = @ET_CODIGO AND CB_CODIGO = @CB_CODIGO AND EC_ORDEN = @EC_ORDEN
END
GO


CREATE PROCEDURE SP_RESUMEN_PREG( @ET_CODIGO FolioGrande, @CB_CODIGO NumeroEmpleado, @EC_ORDEN FolioChico, @EP_ORDEN FolioChico,
				  @EV_RELACIO Status, @Cuantos NumeroEmpleado, @Suma CuatroDecimales )
AS
BEGIN
	if ( @EV_RELACIO = 0 )
		UPDATE SUJ_PREG
		SET    SP_NUM_R0  = SP_NUM_R0  + @Cuantos,
		       SP_SUM_R0  = SP_SUM_R0  + @Suma,
                       SP_NUM_TOT = SP_NUM_TOT + @Cuantos,
                       SP_SUM_TOT = SP_SUM_TOT + @Suma
		WHERE  ET_CODIGO = @ET_CODIGO AND CB_CODIGO = @CB_CODIGO AND EC_ORDEN = @EC_ORDEN AND EP_ORDEN = @EP_ORDEN
	else if ( @EV_RELACIO = 1)
		UPDATE SUJ_PREG
		SET    SP_NUM_R1  = SP_NUM_R1  + @Cuantos,
		       SP_SUM_R1  = SP_SUM_R1  + @Suma,
                       SP_NUM_TOT = SP_NUM_TOT + @Cuantos,
                       SP_SUM_TOT = SP_SUM_TOT + @Suma
		WHERE  ET_CODIGO = @ET_CODIGO AND CB_CODIGO = @CB_CODIGO AND EC_ORDEN = @EC_ORDEN AND EP_ORDEN = @EP_ORDEN
	else if ( @EV_RELACIO = 2 )
		UPDATE SUJ_PREG
		SET    SP_NUM_R2  = SP_NUM_R2  + @Cuantos,
		       SP_SUM_R2  = SP_SUM_R2  + @Suma,
                       SP_NUM_TOT = SP_NUM_TOT + @Cuantos,
                       SP_SUM_TOT = SP_SUM_TOT + @Suma
		WHERE  ET_CODIGO = @ET_CODIGO AND CB_CODIGO = @CB_CODIGO AND EC_ORDEN = @EC_ORDEN AND EP_ORDEN = @EP_ORDEN
	else if ( @EV_RELACIO = 3 )
		UPDATE SUJ_PREG
		SET    SP_NUM_R3  = SP_NUM_R3  + @Cuantos,
		       SP_SUM_R3  = SP_SUM_R3  + @Suma,
                       SP_NUM_TOT = SP_NUM_TOT + @Cuantos,
                       SP_SUM_TOT = SP_SUM_TOT + @Suma
		WHERE  ET_CODIGO = @ET_CODIGO AND CB_CODIGO = @CB_CODIGO AND EC_ORDEN = @EC_ORDEN AND EP_ORDEN = @EP_ORDEN
	else if ( @EV_RELACIO = 4 )
		UPDATE SUJ_PREG
		SET    SP_NUM_R4  = SP_NUM_R4  + @Cuantos,
		       SP_SUM_R4  = SP_SUM_R4  + @Suma,
                       SP_NUM_TOT = SP_NUM_TOT + @Cuantos,
                       SP_SUM_TOT = SP_SUM_TOT + @Suma
		WHERE  ET_CODIGO = @ET_CODIGO AND CB_CODIGO = @CB_CODIGO AND EC_ORDEN = @EC_ORDEN AND EP_ORDEN = @EP_ORDEN
	else 
		UPDATE SUJ_PREG
		SET    SP_NUM_R5  = SP_NUM_R5  + @Cuantos,
		       SP_SUM_R5  = SP_SUM_R5  + @Suma,
                       SP_NUM_TOT = SP_NUM_TOT + @Cuantos,
                       SP_SUM_TOT = SP_SUM_TOT + @Suma
		WHERE  ET_CODIGO = @ET_CODIGO AND CB_CODIGO = @CB_CODIGO AND EC_ORDEN = @EC_ORDEN AND EP_ORDEN = @EP_ORDEN
END
GO

CREATE PROCEDURE SP_RESUMEN_SUJETO( @ET_CODIGO FolioGrande, @CB_CODIGO NumeroEmpleado )
AS
BEGIN
	SET NOCOUNT ON

	-- Borra registros anteriores
	DELETE FROM SUJ_COMP
	WHERE  ET_CODIGO = @ET_CODIGO AND CB_CODIGO = @CB_CODIGO

	-- Crea registros para todos los criterios (competencias)
	INSERT INTO SUJ_COMP ( ET_CODIGO, CB_CODIGO, EC_ORDEN )
	SELECT ET_CODIGO, @CB_CODIGO, EC_ORDEN
	FROM   ENC_COMP
	WHERE  ET_CODIGO = @ET_CODIGO

	DECLARE @EC_ORDEN   FolioChico
	DECLARE @EV_RELACIO Status
	DECLARE @EP_ORDEN   FolioChico
	DECLARE @Cuantos    NumeroEmpleado
	DECLARE @Suma	    CuatroDecimales

	DECLARE  CursorComp CURSOR for
	SELECT EC_ORDEN, EV_RELACIO, COUNT(*), SUM(VC_TOTAL)
	FROM   V_EVA_COMP
	WHERE  ET_CODIGO = @ET_CODIGO AND CB_CODIGO = @CB_CODIGO and VC_TOTAL > 0
	GROUP BY EC_ORDEN, EV_RELACIO

	OPEN     CursorComp
	FETCH NEXT from CursorComp
	INTO  @EC_ORDEN, @EV_RELACIO, @Cuantos, @Suma;
	while ( @@Fetch_Status = 0 )
	BEGIN
		EXEC SP_RESUMEN_COMP @ET_CODIGO, @CB_CODIGO, @EC_ORDEN, @EV_RELACIO, @Cuantos, @Suma

		FETCH NEXT from CursorComp
		INTO  @EC_ORDEN, @EV_RELACIO, @Cuantos, @Suma;
	END
	CLOSE CursorComp;
	DEALLOCATE CursorComp;

	-- Calcula Promedio por Competencia por sujeto
	UPDATE SUJ_COMP
	SET    SO_TOTAL = ROUND( SO_SUM_TOT / SO_NUM_TOT, 3 ),
               SO_FINAL = ROUND( SO_SUM_TOT / SO_NUM_TOT, 3 )
	WHERE  ET_CODIGO = @ET_CODIGO AND CB_CODIGO = @CB_CODIGO AND SO_NUM_TOT > 0
	

	-- Crea registros para todas las preguntas
	INSERT INTO SUJ_PREG ( ET_CODIGO, CB_CODIGO, EC_ORDEN, EP_ORDEN, EP_NUMERO )
	SELECT ET_CODIGO, @CB_CODIGO, EC_ORDEN, EP_ORDEN, EP_NUMERO
	FROM   ENC_PREG
	WHERE  ET_CODIGO = @ET_CODIGO


	DECLARE  CursorPreg CURSOR for

	SELECT EC_ORDEN, EP_ORDEN, EV_RELACIO, COUNT(*), SUM(VP_RESPUES)
	FROM   V_EVA_PREG
	WHERE  ET_CODIGO = @ET_CODIGO AND CB_CODIGO = @CB_CODIGO and VP_RESPUES > 0
	GROUP BY EC_ORDEN, EP_ORDEN, EV_RELACIO


	OPEN     CursorPreg
	FETCH NEXT from CursorPreg
	INTO  @EC_ORDEN, @EP_ORDEN, @EV_RELACIO, @Cuantos, @Suma;
	while ( @@Fetch_Status = 0 )
	BEGIN
		EXEC SP_RESUMEN_PREG @ET_CODIGO, @CB_CODIGO, @EC_ORDEN, @EP_ORDEN, @EV_RELACIO, @Cuantos, @Suma

		FETCH NEXT from CursorPreg
		INTO  @EC_ORDEN, @EP_ORDEN, @EV_RELACIO, @Cuantos, @Suma;
	END
	CLOSE CursorPreg;
	DEALLOCATE CursorPreg;

	-- Calcula Promedio por Pregunta por sujeto
	UPDATE SUJ_PREG
	SET    SP_TOTAL = ROUND( SP_SUM_TOT / SP_NUM_TOT, 3 ),
               SP_FINAL = ROUND( SP_SUM_TOT / SP_NUM_TOT, 3 )
	WHERE  ET_CODIGO = @ET_CODIGO AND CB_CODIGO = @CB_CODIGO AND SP_NUM_TOT > 0
END
GO



CREATE PROCEDURE SP_RESUMEN_SUJETOS( @ET_CODIGO FolioGrande )
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @CB_CODIGO NumeroEmpleado

	-- POR SI SE EJECUTA VARIAS VECES. Aunque borra los comentarios
	DELETE FROM SUJ_COMP
	WHERE ET_CODIGO = @ET_CODIGO

	DECLARE  CursorSujeto CURSOR for
	SELECT   CB_CODIGO
	FROM     SUJETO
	WHERE    ET_CODIGO = @ET_CODIGO

	OPEN     CursorSujeto
	FETCH NEXT from CursorSujeto
	INTO  @CB_CODIGO;
	while ( @@Fetch_Status = 0 )
	BEGIN
		EXEC SP_RESUMEN_SUJETO @ET_CODIGO, @CB_CODIGO

		FETCH NEXT from CursorSujeto
		INTO @CB_CODIGO;

	END
	CLOSE CursorSujeto;
	DEALLOCATE CursorSujeto;
END
GO

CREATE PROCEDURE SP_TOTALIZA_EVALUACION( @EV_FOLIO GUID )
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @ET_CODIGO FolioGrande
	DECLARE @CB_CODIGO NumeroEmpleado

	-- Limpia el valor de las respuestas, por si se modificaron
	UPDATE EVA_PREG
	SET    VP_RESPUES = 0
	WHERE  EV_FOLIO = @EV_FOLIO

	-- Anota el valor de las respuestas seg�n su escala
	UPDATE EVA_PREG
	SET    EVA_PREG.VP_RESPUES = 
		( select EL_VALOR
		  from   V_EVA_PREG
		  where  V_EVA_PREG.EV_FOLIO = @EV_FOLIO and
                         V_EVA_PREG.VC_ORDEN = EVA_PREG.VC_ORDEN and
			 V_EVA_PREG.VP_ORDEN = EVA_PREG.VP_ORDEN )
	WHERE  EV_FOLIO = @EV_FOLIO and VP_CUAL > 0

	-- Calcula el promedio ponderado de las respuestas de cada competencia evaluada
	-- En dos pasos. 
	-- 1) Calcula el 'EC_TOT_PES', como la SUMA(EP_PESO) de las preguntas que SI respondieron
	-- 2) Divide VP_RESPUES * EP_PESO entre EC_TOT_PES
	UPDATE EVA_COMP
	SET    VC_TOTAL = COALESCE(
                          ( SELECT SUM(EP_PESO)
                            FROM   V_EVA_PREG
			    WHERE V_EVA_PREG.EV_FOLIO = EVA_COMP.EV_FOLIO AND
                                  V_EVA_PREG.VC_ORDEN = EVA_COMP.VC_ORDEN AND
                                  V_EVA_PREG.VP_RESPUES > 0 ), 0 )
	WHERE  EV_FOLIO = @EV_FOLIO;

	UPDATE EVA_COMP
	SET    VC_TOTAL = ROUND( 
                          COALESCE(
                          ( SELECT SUM(VP_RESPUES * EP_PESO)
                            FROM   V_EVA_PREG
			    WHERE V_EVA_PREG.EV_FOLIO = EVA_COMP.EV_FOLIO AND
                                  V_EVA_PREG.VC_ORDEN = EVA_COMP.VC_ORDEN AND
                                  V_EVA_PREG.VP_RESPUES > 0 ), 0 ) / VC_TOTAL, 3 )
	WHERE  EV_FOLIO = @EV_FOLIO and VC_TOTAL > 0


	-- Calcula el promedio ponderado de las competencias evaluadas, en dos pasos
	-- 1) Calcula el 'ET_TOT_PES', como la SUMA(EC_PESO) de los criterios que SI respondieron
	-- 2) Divide VP_TOTAL * EC_PESO entre ET_TOT_PES
	UPDATE EVALUA
	SET    EV_TOTAL = COALESCE( 
			  ( SELECT SUM(EC_PESO)
                            FROM   V_EVA_COMP
                            WHERE  V_EVA_COMP.EV_FOLIO = EVALUA.EV_FOLIO AND VC_TOTAL > 0 ), 0 )
	WHERE  EV_FOLIO = @EV_FOLIO

	UPDATE EVALUA
	SET    EV_TOTAL = ROUND(
                          COALESCE( 
			  ( SELECT SUM(VC_TOTAL * EC_PESO)
                            FROM   V_EVA_COMP
                            WHERE  V_EVA_COMP.EV_FOLIO = EVALUA.EV_FOLIO AND VC_TOTAL > 0 ), 0 ) / EV_TOTAL, 3 )
	WHERE  EV_FOLIO = @EV_FOLIO and EV_TOTAL > 0


	/* Cuenta las respuestas y marca el status de la Evaluaci�n */
	EXEC SP_CUENTA_RESPUESTAS @EV_FOLIO

	SELECT @ET_CODIGO = ET_CODIGO,
	       @CB_CODIGO = CB_CODIGO
	FROM   EVALUA
	WHERE  EV_FOLIO = @EV_FOLIO

	-- Si es una Encuesta relacionada con un Sujeto (360)
	-- Entonces actualiza los totales y status del Sujeto
	if ( @CB_CODIGO > 0 )
		EXEC SP_TOTALIZA_SUJETO @ET_CODIGO, @CB_CODIGO

	-- Actualiza los Totales de la Encuesta general
	EXEC SP_TOTALIZA_ENCUESTA @ET_CODIGO
END
GO

CREATE PROCEDURE SP_TOTALIZA_EVALUACIONES( @ET_CODIGO FolioGrande )
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @EV_FOLIO GUID

	DECLARE  CursorEval CURSOR for
	SELECT   EV_FOLIO
	FROM     EVALUA
	WHERE    ET_CODIGO = @ET_CODIGO

	OPEN     CursorEval
	FETCH NEXT from CursorEval
	INTO  @EV_FOLIO;
	while ( @@Fetch_Status = 0 )
	BEGIN
		EXEC SP_TOTALIZA_EVALUACION @EV_FOLIO

		FETCH NEXT from CursorEval
		INTO @EV_FOLIO;
	END
	CLOSE CursorEval;
	DEALLOCATE CursorEval;
END
GO

CREATE PROCEDURE SP_CIERRA_EVALUACION( @EV_FOLIO GUID, @VP_CUAL FolioChico )
AS
BEGIN
	SET NOCOUNT ON
	UPDATE EVA_PREG
	SET    VP_CUAL = @VP_CUAL
	WHERE  EV_FOLIO = @EV_FOLIO and VP_CUAL = 0

	EXEC SP_TOTALIZA_EVALUACION @EV_FOLIO
END
GO

CREATE PROCEDURE SP_BORRA_RESPUESTAS( @ET_CODIGO FolioGrande )
AS
BEGIN
	SET NOCOUNT ON

	/* Borra Respuestas */
	DELETE FROM EVA_COMP
	WHERE EV_FOLIO IN ( SELECT EV_FOLIO FROM EVALUA WHERE ET_CODIGO = @ET_CODIGO )

	/* Borra Resumen */
	DELETE FROM SUJ_COMP
	WHERE ET_CODIGO = @ET_CODIGO

	/* Limpia contadores de SUJETO */
	UPDATE  SUJETO
	SET     SJ_FIN_EVA = 0,
		SJ_TOTAL   = 0
	WHERE   ET_CODIGO = @ET_CODIGO

	/* Regresa Status de SUJETO a Listo */
	UPDATE  SUJETO
	SET     SJ_STATUS  = 1 -- Listo
	WHERE   ET_CODIGO = @ET_CODIGO and SJ_STATUS > 1

	/* Limpia contadores de EVALUACION */
	UPDATE  EVALUA
	SET     EV_FIN_PRE = 0,
		EV_TOTAL   = 0
	WHERE   ET_CODIGO = @ET_CODIGO

	/* Regresa Status de EVALUACION a NUEVA */
	UPDATE  EVALUA
	SET     EV_STATUS = 0  -- Nueva
	WHERE   ET_CODIGO = @ET_CODIGO
                
	-- Actualiza los Totales de la Encuesta general
	EXEC SP_TOTALIZA_ENCUESTA @ET_CODIGO

END
GO
