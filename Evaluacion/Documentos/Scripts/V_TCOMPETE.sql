CREATE VIEW V_TCOMPETE ( 
	  TC_CODIGO, 
	  TC_DESCRIP,
	  TC_INGLES,
          TC_NUMERO, 
          TC_TEXTO,
          TC_TIPO)
AS
   SELECT TC_CODIGO, 
	  TC_DESCRIP,
	  TC_INGLES,
          TC_NUMERO, 
          TC_TEXTO,
          TC_TIPO
   FROM	  TCOMPETE
   