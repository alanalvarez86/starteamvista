unit FSistEditUsuarios;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, StdCtrls, DBCtrls, Mask, ComCtrls, ExtCtrls, Buttons,
     FSistBaseEditUsuarios,
     ZetaDBTextBox,
     ZetaKeyCombo,
     ZetaKeyLookup,
     ZetaNumero,
     ZetaSmartLists;

type
  TSistEditUsuarios = class(TSistBaseEditUsuarios)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SistEditUsuarios: TSistEditUsuarios;

implementation

{$R *.DFM}

end.
