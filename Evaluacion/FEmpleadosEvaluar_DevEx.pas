unit FEmpleadosEvaluar_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, ExtCtrls, StdCtrls, Mask, Buttons, Grids, DBGrids, ComCtrls,
     ZBaseConsulta,
     ZetaNumero,
     ZetaDBTextBox,
     ZetaKeyCombo,
     ZetaDBGrid,
     ZetaKeyLookup,
     ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, Vcl.Menus, System.Actions,
  Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid,
  ZetaKeyLookup_DevEx;

type
  TEmpleadosEvaluar_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Label2: TLabel;
    ztbCuantos: TZetaTextBox;
    Label3: TLabel;
    CB_CODIGO: TZetaKeyLookup_DevEx;
    CB_CODIGO_GRID: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    SJ_STATUS_GRID: TcxGridDBColumn;
    SJ_NUM_EVA: TcxGridDBColumn;
    procedure btnBuscaEmpClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CB_CODIGOValidKey(Sender: TObject);
  private
    { Private declarations }
    procedure LlenaListaStatus;
    procedure CuantosRegistros;
    procedure CargarFiltros;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  EmpleadosEvaluar_DevEx: TEmpleadosEvaluar_DevEx;

implementation

{$R *.DFM}

uses DEvaluacion,
     ZHelpContext,
     ZetaMsgDlg,
     ZetaDialogo,
     ZetaTipoEntidad,
     ZetaBuscador_DevEx,
     ZImprimeForma,
     ZetaCommonTools,
     ZetaBuscaEmpleado,
     ZetaCommonLists,
     DCliente;

{ TEmpleadosEvaluar_DevEx }

procedure TEmpleadosEvaluar_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEvaluacion;
     CB_CODIGO.LookupDataSet := dmCliente.cdsEmpleadoLookup;
     HelpContext := H_DISENO_ENCUESTA_EMPLEADOS_EVALUAR;

     ZetaDBGridDBTableView.OptionsSelection.MultiSelect := True;
end;

procedure TEmpleadosEvaluar_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     CB_CODIGO.Valor := 0;
     CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0], 0 ,'' , SkCount );
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Muestra la caja de agrupamiento
     ApplyMinWidth;
     ZetaDBGridDBTableView.ApplyBestFit();
     CB_CODIGO_GRID.Options.Grouping:= FALSE;
     PRETTYNAME.Options.Grouping:= FALSE;
     SJ_NUM_EVA.Options.Grouping:= FALSE;
     SJ_STATUS_GRID.Options.Grouping:= TRUE;
end;

procedure TEmpleadosEvaluar_DevEx.Connect;
begin
     with dmEvaluacion do
     begin
          if not cdsEmpEvaluar.Active or cdsEmpEvaluar.HayQueRefrescar then
          begin
               CargarFiltros;
               cdsEmpEvaluar.ResetDataChange;
          end;
          DataSource.DataSet := cdsEmpEvaluar;
     end;
end;

procedure TEmpleadosEvaluar_DevEx.Agregar;
begin
     dmEvaluacion.cdsEmpEvaluar.Agregar;
     DoBestFit;
end;

procedure TEmpleadosEvaluar_DevEx.Borrar;
var
  I,iRecordIndex, iFieldIndex: Integer;
  Codigos:TStringList;
begin
       with  ZetaDBGridDBTableView.Controller do
       begin
                 if ( SelectedRecordCount > 1 ) then
                 begin
                      Codigos := TStringList.Create;
                      try
                        if ZetaMsgDlg.ConfirmaCambio( Format( '� Desea borrar estos %d empleados ?', [ SelectedRecordCount ] ) ) then
                        begin

                             for I := 0 to SelectedRecordCount-1 do
                             begin
                                iRecordIndex :=  ZetaDBGridDBTableView.Controller.SelectedRecords[I].RecordIndex;
                                iFieldIndex :=   ZetaDBGridDBTableView.DataController.GetItemByFieldName('CB_CODIGO').Index;

                                Codigos.Add(ZetaDBGridDBTableView.DataController.Values[iRecordIndex,iFieldIndex])
                             end;
                             if Codigos.Count>0 then
                                dmEvaluacion.BorrarEmpleadosPorEvaluar( Codigos );
                        end;

                      finally
                             FreeAndNil(Codigos);
                      end;

                 end
                 else
                     if ( SelectedRecordCount = 1 ) then
                     begin
                          with dmEvaluacion do
                          begin
                               BorrarEmpleadoPorEvaluar( cdsEmpEvaluar.FieldByName( 'CB_CODIGO' ).AsInteger );
                          end;
                     end
                     else
                         ZetaDialogo.zWarning( '� Atenci�n !', 'No se escogi� ning�n empleado para borrar', 0, mbOk );
       end;
     DoBestFit;
end;

procedure TEmpleadosEvaluar_DevEx.LlenaListaStatus;
begin
     {ZetaCommonLists.LlenaLista( lfStatusSujeto, SJ_STATUS.Items );
     SJ_STATUS.Items.Insert( 0, 'Todos' );
     SJ_STATUS.ItemIndex := 0;}
end;

procedure TEmpleadosEvaluar_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( self.Caption, 'SUJETO', 'CB_CODIGO', dmEvaluacion.cdsEmpEvaluar );
     CargarFiltros;
end;

procedure TEmpleadosEvaluar_DevEx.ImprimirForma;
begin
     inherited;
     ZImprimeForma.ImprimeUnaForma( enEvalua, dmEvaluacion.cdsEmpEvaluar );
end;

procedure TEmpleadosEvaluar_DevEx.Modificar;
begin
     dmEvaluacion.cdsEmpEvaluar.Modificar;
     DoBestFit;
end;

procedure TEmpleadosEvaluar_DevEx.Refresh;
begin
     CargarFiltros;
     DoBestFit;
end;

procedure TEmpleadosEvaluar_DevEx.CuantosRegistros;
begin
     //StatusBar1.Panels[0].Text := 'Cantidad : ' + inttoStr( dmEvaluacion.cdsEmpEvaluar.RecordCount );
end;

procedure TEmpleadosEvaluar_DevEx.btnBuscaEmpClick(Sender: TObject);
var
   sKey, sDescription: String;
begin
     inherited;
     {if ZetaBuscaEmpleado.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
        znEmpleado.Valor :=  StrToIntDef( sKey, 0 );  }
end;

procedure TEmpleadosEvaluar_DevEx.btnFiltrarClick(Sender: TObject);
begin
     //CargarFiltros;
end;

procedure TEmpleadosEvaluar_DevEx.CargarFiltros;
var
   iValor: Integer;
   iIndice: Integer;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmEvaluacion do
        begin
             iValor := CB_CODIGO.Valor;
             iIndice := 0;//SJ_STATUS.ItemIndex;
             ObtieneSujetosEvaluar( iValor, ( iIndice - 1 ), cdsEmpEvaluar );
             EmpleadoEvaluar := iValor;
             StatusEmpEvaluar := ( iIndice - 1 );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     //CuantosRegistros;
end;

procedure TEmpleadosEvaluar_DevEx.CB_CODIGOValidKey(Sender: TObject);
begin
  inherited;
  CargarFiltros;
  DoBestFit;
end;

end.
