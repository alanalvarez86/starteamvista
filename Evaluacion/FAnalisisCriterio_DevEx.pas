unit FAnalisisCriterio_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, StdCtrls, Buttons, Mask, ZetaNumero,
  ZetaDBTextBox, Grids, DBGrids, ZetaDBGrid, TeeProcs, TeEngine, Chart,
  DBChart, Series, ComCtrls, ZBaseGridLectura_DevEx, VclTee.TeeGDIPlus,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxSplitter, cxButtons, cxContainer,
  cxGroupBox;

type
  TAnalisisCriterio_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Label2: TLabel;
    ztbCuantos: TZetaTextBox;
    lblPrimeros: TLabel;
    edPrimeros: TZetaNumero;
    znPromedios: TComboBox;
    btnFiltrar: TcxButton;
    EV_RELACIO: TComboBox;
    Grafica: TDBChart;
    Series1: THorizBarSeries;
    EC_ORDEN: TcxGridDBColumn;
    EC_NOMBRE: TcxGridDBColumn;
    CUANTAS: TcxGridDBColumn;
    MINIMO: TcxGridDBColumn;
    PROMEDIO: TcxGridDBColumn;
    MAXIMO: TcxGridDBColumn;
    cxSplitter1: TcxSplitter;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    procedure znPromediosChange(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Grid1TitleClick(Column: TColumn);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure LlenaListaRelacion;
    procedure CuantosRegistros;
    procedure InicializaValores;
    //procedure OrdenaGrid( const iOrdenarPor: Integer );
    procedure CargaFiltros;
    procedure CreaGrafica;
    procedure SetControles;

  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
  end;

var
  AnalisisCriterio_DevEx: TAnalisisCriterio_DevEx;

implementation

{$R *.DFM}
uses DEvaluacion,
     ZHelpContext,
     ZetaClientTools,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists;

{ TAnalisisCriterio }
procedure TAnalisisCriterio_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEvaluacion;
     HelpContext := H_RESULTADOS_ENCUESTA_ANALISIS_CRITERIO;
end;

procedure TAnalisisCriterio_DevEx.FormShow(Sender: TObject);
begin
    inherited;
    InicializaValores;
    SetControles;
    LlenaListaRelacion;
    CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0], 0 ,'' , SkCount );
    ApplyMinWidth;
    ZetaDBGridDBTableView.ApplyBestFit();
    DoBestFit;
end;

procedure TAnalisisCriterio_DevEx.Agregar;
begin
     dmEvaluacion.cdsAnalisisCriterio.Agregar;
end;

procedure TAnalisisCriterio_DevEx.Borrar;
begin
     dmEvaluacion.cdsAnalisisCriterio.Borrar;
end;

procedure TAnalisisCriterio_DevEx.Connect;
begin
     with dmEvaluacion do
     begin
          if not cdsAnalisisCriterio.Active or cdsAnalisisCriterio.HayQueRefrescar then
          begin
               CargaFiltros;
               CreaGrafica;
               cdsAnalisisCriterio.ResetDataChange;
          end;
          DataSource.DataSet := cdsAnalisisCriterio;
     end;
end;

procedure TAnalisisCriterio_DevEx.ImprimirForma;
begin
  inherited;
end;

procedure TAnalisisCriterio_DevEx.Modificar;
begin
     dmEvaluacion.cdsAnalisisCriterio.Modificar;
end;

function TAnalisisCriterio_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede agregar el registro desde esta forma';
     Result := False;
end;

function TAnalisisCriterio_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede borrar el registro desde esta forma';
     Result := False;
end;

function TAnalisisCriterio_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede modificar el registro desde esta forma';
     Result := False;
end;

procedure TAnalisisCriterio_DevEx.Refresh;
begin
     //dmEvaluacion.cdsAnalisisCriterio.Refrescar;
     CargaFiltros;
end;

procedure TAnalisisCriterio_DevEx.LlenaListaRelacion;
begin
     with dmEvaluacion.cdsPerfilEvaluador do
     begin
          EV_RELACIO.Clear;
          Conectar;
          First;
          DisableControls;
          try
             EV_RELACIO.Items.AddObject( 'Todas', TObject( 0 ) );
             while not EOF do
             begin
                  EV_RELACIO.Items.AddObject( FieldByName('ER_NOMBRE').AsString, TObject( FieldByName('ER_TIPO').AsInteger ) );
                  Next;
             end;
          finally
                 EnableControls;
          end;
     end;
     EV_RELACIO.ItemIndex := 0;
end;

procedure TAnalisisCriterio_DevEx.InicializaValores;
begin
     znPromedios.ItemIndex := 0;
     edPrimeros.Valor := 5;
end;

procedure TAnalisisCriterio_DevEx.CuantosRegistros;
begin
     //ztbCuantos.Caption := inttoStr( dmEvaluacion.cdsAnalisisCriterio.RecordCount );
     //StatusBar1.Panels[0].Text := 'Cantidad : ' + inttoStr( dmEvaluacion.cdsAnalisisCriterio.RecordCount );
end;

{procedure TAnalisisCriterio.OrdenaGrid( const iOrdenarPor: Integer );
begin
     with dmEvaluacion.cdsAnalisisCriterio do
     begin
          case( iOrdenarPor ) of
                0: IndexName := 'EC_ORDENIndex';
                1: IndexName := 'PROMEDIOIndexD';
                2: IndexName := 'PROMEDIOIndex';
          else
              IndexName := VACIO;
          end;
     end;
end;}

procedure TAnalisisCriterio_DevEx.CargaFiltros;
var
   iMostrarProm, iRelacion, iMostrar: Integer;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmEvaluacion.cdsAnalisisCriterio do
        begin
             DisableControls;
             try
                iMostrarProm := znPromedios.ItemIndex;
                iMostrar := edPrimeros.ValorEntero;
                if EV_RELACIO.ItemIndex > 0 then
                   iRelacion := Integer( EV_RELACIO.Items.Objects[ EV_RELACIO.ItemIndex ] ) //EV_RELACIO.ItemIndex;
                else
                    iRelacion := -1;
                //OrdenaGrid( iMostrarProm );
                //dmEvaluacion.ObtenerAnalisisCriterio( iMostrar, ( iRelacion - 1 ), iMostrarProm );
                dmEvaluacion.ObtenerAnalisisCriterio( iMostrar, ( iRelacion ), iMostrarProm );
                CreaGrafica;
             finally
                    EnableControls;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     CuantosRegistros;
     DoBestFit;
end;

procedure TAnalisisCriterio_DevEx.CreaGrafica;
begin
     with Series1 do
     begin
          with dmEvaluacion.cdsAnalisisCriterio do
          begin
               Clear;
               DisableControls;
               try
                  Last;
                  while not BOF do
                  begin
                       AddBar( FieldByName('PROMEDIO').AsFloat, FieldByName('EC_NOMBRE').AsString, clDefault );
                       Prior;
                  end;
               finally
                      EnableControls;
               end;
          end;
     end;
end;

procedure TAnalisisCriterio_DevEx.znPromediosChange(Sender: TObject);
begin
     inherited;
     SetControles;
end;

procedure TAnalisisCriterio_DevEx.SetControles;
begin
     edPrimeros.Enabled := ( znPromedios.ItemIndex <> 0 );
     lblPrimeros.Enabled := edPrimeros.Enabled;
     if( edPrimeros.Enabled ) then
     begin
          if( edPrimeros.Valor  = 0 )then
              edPrimeros.Valor := 5;
     end
     else
         edPrimeros.Valor := 0;
end;

procedure TAnalisisCriterio_DevEx.btnFiltrarClick(Sender: TObject);
begin
     CargaFiltros;
end;

procedure TAnalisisCriterio_DevEx.Grid1TitleClick(Column: TColumn);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        //Grid1.OrdenarPor( Column );
        CreaGrafica;
     finally
            Screen.Cursor := oCursor;
     end;
end;

end.
