unit ZHelpContext;

interface

const

   // Help context number para la ventana principal

   H_SHELL                                                          = 1; { Ventana principal - descripci�n de ventana principal }
   H_INTERFAZ_PRICIPAL                                              = 1; {Al posicionarse en la busqueda de opciones del �rbol }

   // Help context numbers para ventanas que se acceden en "Dise�o de Encuesta"

   H_DISENO_ENCUESTA_CRITERIOS_EVALUAR                              = 21100; {Dise�o de encuesta | Criterios a evaluar }
   H_DISENO_ENCUESTA_CRITERIOS_EVALUAR_AGREGAR_COMPETENCIAS         = 21100; {Dise�o de encuesta | Criterios a evaluar, Agregar competencias}
   H_DISENO_ENCUESTA_CRITERIOS_EVALUAR_AGREGAR_REGISTRO             = 21100; {Dise�o de encuesta | Criterios a evaluar, Agregar registro}

   H_DISENO_ENCUESTA_PREGUNTAS_CRITERIO                             = 91000; {Dise�o de encuesta | Preguntas por criterio}
   H_DISENO_ENCUESTA_PREGUNTAS_CRITERIO_AGREGAR_REGISTRO            = 91000; {Dise�o de encuesta | Preguntas por criterio, Agregar registro}

   H_DISENO_ENCUESTA_ESCALAS_ENCUESTA                               = 21300; {Dise�o de encuesta | Escalas de Encuesta}
   H_DISENO_ENCUESTA_ESCALAS_ENCUESTA_COPIAR_ESCALAS                = 21300; {Dise�o de encuesta | Escalas de Encuesta, Copiar escalas}
   H_DISENO_ENCUESTA_ESCALAS_ENCUESTA_AGREGAR_REGISTRO              = 21300; {Dise�o de encuesta | Escalas de Encuesta, Agregar registro}

   H_DISENO_ENCUESTA_PERFIL_EVALUADORES                             = 94000; {Dise�o de encuesta | Perfil de Evaluadores}
   H_DISENO_ENCUESTA_PERFIL_EVALUADORES_AGREGAR_REGISTRO            = 94000; {Dise�o de encuesta | Perfil de Evaluadores, Agregar registro}

   H_DISENO_ENCUESTA_EMPLEADOS_EVALUAR                              = 95000; {Dise�o de encuesta | Empleados a Evaluar}
   H_DISENO_ENCUESTA_EMPLEADOS_EVALUAR_ASIGNAR_EVALUADORES          = 95000; {Dise�o de encuesta | Empleados a Evaluar, Asignar evaluadores}
   H_DISENO_ENCUESTA_EMPLEADOS_EVALUAR_AGREGAR_REGISTRO             = 95000; {Dise�o de encuesta | Empleados a Evaluar, Agregar registro}

   H_DISENO_ENCUESTA_REGISTROS                                      = 21550; {Dise�o de encuesta | Registros}

   H_DISENO_ENCUESTA_PROCESOS                                       = 21600; {Dise�o de encuesta | Procesos}
   H_DISENO_ENCUESTA_PROCESOS_GENERAR_LISTA_EMPLEADOS_EVALUAR       = 43300; {Dise�o de encuesta | Procesos, Gr lista emp a eva (wizard)}
   H_DISENO_ENCUESTA_PROCESOS_PREPARAR_EVALUACIONES                 = 45100; {Dise�o de encuesta | Procesos, Preparar evaluaciones (wizard)}
   H_DISENO_ENCUESTA_PROCESOS_CALCULAR_RESULTADOS                   = 47100; {Dise�o de encuesta | Procesos, Calcular resultados (wizard)}
   H_DISENO_ENCUESTA_PROCESOS_RECALCULAR_PROMEDIOS                  = 47200; {Dise�o de encuesta | Procesos, Recalcular promedios (wizard)}
   H_DISENO_ENCUESTA_PROCESOS_ENVIA_CORREOS_INVITACION              = 21620; {Dise�o de encuesta | Procesos, Env�a correos de inv (wizard)}
   H_DISENO_ENCUESTA_PROCESOS_ENVIA_CORREOS_NOTIFICACION            = 21610; {Dise�o de encuesta | Procesos, Env�a correos de not (wizard)}
   H_DISENO_ENCUESTA_PROCESOS_ASIGNACION_RELACIONES                 = 43400; {Dise�o de encuesta | Procesos, Asignaci�n de realacio (wizard)}
   H_DISENO_ENCUESTA_PROCESOS_ENCUESTA_SIMPLE                       = 21630; {Dise�o de encuesta | Procesos, Crear encuesta simple (wizard)}

   // Help context numbers para ventanas que se acceden en "Resultados de Encuesta"

   H_RESULTADOS_ENCUESTA_TOTALES_ENCUESTA                           = 96000; {Resultados de Encuesta | Totales de enc (pesta�a Evaluaciones)}
   H_RESULTADOS_ENCUESTA_TOTALES_ENCUESTA_EVALUACIONES              = 96000; {Resultados de Encuesta | Totales de enc (pesta�a Evaluaciones)}
   H_RESULTADOS_ENCUESTA_TOTALES_ENCUESTA_EVALUADORES               = 96000; {Resultados de Encuesta | Totales de enc (pesta�a Evaluadores) }
   H_RESULTADOS_ENCUESTA_TOTALES_ENCUESTA_EVALUADOS                 = 96000; {Resultados de Encuesta | Totales de enc (pesta�a Evaluados)}

   H_RESULTADOS_ENCUESTA_EMPLEADOS_EVALUADOS                        = 97000; {Resultados de Encuesta | Empleados evaluados}
   H_RESULTADOS_ENCUESTA_EVALUADORES                                = 98000; {Resultados de Encuesta | Evaluadores}
   H_RESULTADOS_ENCUESTA_EVALUACIONES                               = 99000; {Resultados de Encuesta | Evaluaciones}
   H_RESULTADOS_ENCUESTA_ANALISIS_CRITERIO                          = 100000; {Resultados de Encuesta | An�lisis por criterio}
   H_RESULTADOS_ENCUESTA_ANALISIS_PREGUNTA                          = 101000; {Resultados de Encuesta | An�lisis por pregunta}
   H_RESULTADOS_ENCUESTA_FRECUENCIA_RESPUESTAS                      = 102000; {Resultados de Encuesta | Frecuencia de respuetas}

   // Help context numbers para ventanas que se acceden en "Resultados Individuales"

   H_RESULTADOS_INDIVIDUALES_RESULTADOS_CRITERIO                    = 103000; {Resultados individuales | Resultados por criterio}
   H_RESULTADOS_INDIVIDUALES_RESULTADOS_PREGUNTA                    = 104000; {Resultados individuales | Resultados por pregunta}

   //Todos los apartados de consultas conducir�n a una ventana que har� referencia a la ayuda en l�nea de Tress.

   H_CONSULTAS                                                      = 50; {Consultas}

   H_CONSULTAS_CORREOS                                              = 105000; {Consultas}
   H_HISTORIAL_CORREOS                                              = 105000; {Consultas | Correos}
   // Help context numbers para ventanas que se acceden en "Cat�logos"

   H_CATALOGOS_TIPOS_COMPETENCIA                                    = 24100; {Cat�logos | Tipos de competencia}
   H_CATALOGOS_TIPOS_COMPETENCIA_AGREGAR_REGISTRO                   = 24100; {Cat�logos | Tipos de competencia, Agregar registro}

   H_CATALOGOS_COMPETENCIAS 	                                    = 24200; {Cat�logos | Competencias}
   H_CATALOGOS_COMPETENCIAS_AGREGAR_REGISTRO                        = 24200; {Cat�logos | Competencias, Agregar registro}

   H_CATALOGOS_PREGUNTAS                                            = 24300; {Cat�logos | Preguntas}
   H_CATALOGOS_PREGUNTAS_AGREGAR_REGISTRO                           = 24300; {Cat�logos | Preguntas, Agregar registro}

   H_CATALOGOS_ESCALAS                                              = 24400; {Cat�logos | Escalas}
   H_CATALOGOS_ESCALAS_AGREGAR_REGISTRO                             = 24400; {Cat�logos | Escalas, Agregar registro}

   H_CATALOGOS_ENCUESTAS                                            = 24500; {Cat�logos | Encuestas}
   H_CATALOGOS_ENCUESTAS_AGREGAR_REGISTRO                           = 24500; {Cat�logos | Encuestas, Agregar registro (wizard)}
   H_CATALOGOS_ENCUESTAS_MODIFICAR_REGISTRO                         = 24500; {Cat�logos | Encuestas, Modificar registro}

   // Help context numbers para ventanas que se acceden en "Sistema"

   H_SISTEMA_USUARIOS                                               = 92000; {Sistema | Usuarios}
   H_SISTEMA_USUARIOS_MODIFICAR_REGISTROS                           = 92000; {Sistema | Usuarios, Modificar registro}

   H_SISTEMA_GLOBALES_EMPRESA_CONFIGURAR_SERVIDOR_WEB               = 106000; {Sistema | Globales de Empresa, Configurar servidor de web}
   H_SISTEMA_GLOBALES_EMPRESA                                       = 106000; {Sistema | Globales de Empresa}

   H_SISTEMA_DICCIONARIO_DATOS                                      = 107000; {Sistema | Diccionario de Datos}
   H_SISTEMA_DICCIONARIO_DATOS_MODIFICAR_REGISTRO                   = 107000; { Sistema | Diccionario de Datos, Modificar registro}

   {$ifdef FALSE}
   {$endif}

   {******** Consultas **********}

  { H_CONSULTAS                        = 5;
   H_CONS_REPORTES                    = 6;
   H_REPORTES_CONFIDENCIALES          = 9;
   H_REPORTES_WORKFLOW                = 30;
   H_REPORTES_PORTAL                  = 31;
   H_REPORTES_SISTEMA                 = 32;
   H_REPORTES_FAVORITOS               = 33;
   H_CONS_SQL                         = 7;
   H_CONS_BITACORA                    = 8;
   H_TOOLS_CONSTRUYE_FORMULA          = 34;
   }
   {******** Cat�logos **********}

   {H_CATALOGOS                        = 10;
   H_CAT_MODELOS                      = 11;
   H_CAT_PASOS_MODELOS                = 12;
   H_CAT_USUARIOS                     = 13;
   H_CAT_ROLES                        = 14;
   }
   H_CAT_ACCIONES                     = 15;
   {
   H_CATALOGOS_GENERALES              = 16;
   H_CAT_GRALES_CONDICIONES           = 17;
   H_CATALOGOS_CONFIGURACION          = 18;
   }
   H_CAT_CONFI_DICCIONARIO            = 19;
   H_CAT_CONFI_GLOBALES               = 20;

   {******** Sistema **********}
   {
   Estos DEBEN de ser iguales en todas las aplicaciones
   que usan COMPARTE - ver D:\3Win_20\Tools\zAccesosTress.pas
   }
   H_SISTEMA                          = 21;
   {
   H_SIST_DATOS                       = 22;
   H_SIST_DATOS_EMPRESAS              = 23;
   H_SIST_DATOS_USUARIOS              = 24;
   H_SIST_DATOS_GRUPOS                = 25;
   H_SIST_DATOS_GRUPOS_ACCESOS        = 26;
   H_SIST_DATOS_IMPRESORAS            = 27;
   H_SIST_PRENDE_USUARIOS             = 28;
   H_SIST_APAGA_USUARIOS              = 29;
   }
implementation

end.
