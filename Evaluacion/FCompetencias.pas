unit FCompetencias;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx,ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls,
  Vcl.StdCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxClasses, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList,
  cxGridLevel, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TCompetencias = class(TBaseGridLectura_DevEx)
    CM_CODIGO: TcxGridDBColumn;
    CM_DESCRIP: TcxGridDBColumn;
    CM_INGLES: TcxGridDBColumn;
    TC_DESCRIP: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ConfigAgrupamiento;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Competencias: TCompetencias;

implementation

{$R *.DFM}

uses DEvaluacion,
     ZHelpContext,
     ZetaBuscador_DevEx,
     ZetaCommonLists,
     ZAccesosTress,
     ZetaCommonClasses;

{ TCompetencias }
procedure TCompetencias.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_CATALOGOS_COMPETENCIAS;
end;

procedure TCompetencias.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('CM_CODIGO'), K_SIN_TIPO , '', skCount);
     inherited;
     ConfigAgrupamiento;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCompetencias.Agregar;
begin
     dmEvaluacion.cdsCompetencia.Agregar;
     DoBestFit;
end;

procedure TCompetencias.Borrar;
begin
     dmEvaluacion.cdsCompetencia.Borrar;
     DoBestFit;
end;

procedure TCompetencias.Connect;
begin
     with dmEvaluacion do
     begin
          cdsTCompetencia.Conectar;
          cdsCompetencia.Conectar;
          DataSource.DataSet := cdsCompetencia;
     end;
end;

procedure TCompetencias.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Competencia', 'competencia', 'CM_CODIGO', dmEvaluacion.cdsCompetencia );
end;

procedure TCompetencias.ImprimirForma;
begin
     inherited;
end;

procedure TCompetencias.Modificar;
begin
     dmEvaluacion.cdsCompetencia.Modificar;
     DoBestFit;
end;

procedure TCompetencias.Refresh;
begin
     dmEvaluacion.cdsCompetencia.Refrescar;
     DoBestFit;
end;

procedure TCompetencias.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False; //activa la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := False; //Muestra la caja de agrupamiento
end;

end.
