object dmEvaluacion: TdmEvaluacion
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object cdsCompetencia: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'CM_CODIGO'
    Params = <>
    BeforePost = cdsCompetenciaBeforePost
    AfterDelete = cdsCompetenciaAfterDelete
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsCompetenciaAlAdquirirDatos
    AlEnviarDatos = cdsCompetenciaAlEnviarDatos
    AlCrearCampos = cdsCompetenciaAlCrearCampos
    AlModificar = cdsCompetenciaAlModificar
    UsaCache = True
    LookupName = 'Cat'#225'logo de Competencias / Habilidades'
    LookupDescriptionField = 'CM_DESCRIP'
    LookupKeyField = 'CM_CODIGO'
    Left = 40
    Top = 16
  end
  object cdsTCompetencia: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'TC_CODIGO'
    Params = <>
    BeforePost = cdsTCompetenciaBeforePost
    AfterDelete = cdsTCompetenciaAfterDelete
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsTCompetenciaAlAdquirirDatos
    AlEnviarDatos = cdsTCompetenciaAlEnviarDatos
    AlCrearCampos = cdsTCompetenciaAlCrearCampos
    AlModificar = cdsTCompetenciaAlModificar
    UsaCache = True
    LookupName = 'Cat'#225'logo Tipo de Competencia'
    LookupDescriptionField = 'TC_DESCRIP'
    LookupKeyField = 'TC_CODIGO'
    Left = 136
    Top = 16
  end
  object cdsPreguntas: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'PG_FOLIO'
    Params = <>
    BeforePost = cdsPreguntasBeforePost
    AfterDelete = cdsPreguntasAfterDelete
    OnNewRecord = cdsPreguntasNewRecord
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsPreguntasAlAdquirirDatos
    AlEnviarDatos = cdsPreguntasAlEnviarDatos
    AlBorrar = cdsPreguntasAlBorrar
    AlModificar = cdsPreguntasAlModificar
    UsaCache = True
    LookupName = 'Cat'#225'logo de Preguntas'
    LookupDescriptionField = 'PG_DESCRIP'
    LookupKeyField = 'PG_FOLIO'
    Left = 224
    Top = 16
  end
  object cdsEscalas: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'SC_CODIGO'
    Params = <>
    AfterDelete = cdsEscalasAfterDelete
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsEscalasAlAdquirirDatos
    AlAgregar = cdsEscalasAlAgregar
    AlModificar = cdsEscalasAlModificar
    UsaCache = True
    LookupName = 'Cat'#225'logo de Escalas'
    LookupDescriptionField = 'SC_NOMBRE'
    LookupKeyField = 'SC_CODIGO'
    OnGetRights = cdsEscalasGetRights
    Left = 312
    Top = 16
  end
  object cdsEditEscala: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforeInsert = cdsEditEscalaBeforeInsert
    BeforePost = cdsEditEscalaBeforePost
    BeforeDelete = cdsEditEscalaBeforeDelete
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsEditEscalaAlAdquirirDatos
    AlEnviarDatos = cdsEditEscalaAlEnviarDatos
    AlAgregar = cdsEditEscalaAlAgregar
    Left = 40
    Top = 88
  end
  object cdsCriteriosEval: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'EC_ORDEN'
    Params = <>
    BeforePost = cdsCriteriosEvalBeforePost
    AfterDelete = cdsCriteriosEvalAfterDelete
    OnNewRecord = cdsCriteriosEvalNewRecord
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsCriteriosEvalAlAdquirirDatos
    AlEnviarDatos = cdsCriteriosEvalAlEnviarDatos
    AlCrearCampos = cdsCriteriosEvalAlCrearCampos
    AlBorrar = cdsCriteriosEvalAlBorrar
    AlModificar = cdsCriteriosEvalAlModificar
    Left = 224
    Top = 88
  end
  object cdsPregCriterio: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsPregCriterioBeforePost
    OnNewRecord = cdsPregCriterioNewRecord
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsPregCriterioAlAdquirirDatos
    AlEnviarDatos = cdsPregCriterioAlEnviarDatos
    AlCrearCampos = cdsPregCriterioAlCrearCampos
    AlBorrar = cdsPregCriterioAlBorrar
    AlModificar = cdsPregCriterioAlModificar
    Left = 312
    Top = 88
  end
  object cdsCriteriosxPreg: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsCriteriosxPregAlAdquirirDatos
    Left = 400
    Top = 88
  end
  object cdsEscalasEncuesta: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforeInsert = cdsEscalasEncuestaBeforeInsert
    BeforePost = cdsEscalasEncuestaBeforePost
    AfterDelete = cdsEscalasEncuestaAfterDelete
    OnNewRecord = cdsEscalasEncuestaNewRecord
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsEscalasEncuestaAlAdquirirDatos
    AlEnviarDatos = cdsEscalasEncuestaAlEnviarDatos
    AlAgregar = cdsEscalasEncuestaAlAgregar
    AlModificar = cdsEscalasEncuestaAlModificar
    Left = 504
    Top = 88
  end
  object cdsPerfilEvaluador: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsPerfilEvaluadorBeforePost
    AfterDelete = cdsPerfilEvaluadorAfterDelete
    OnNewRecord = cdsPerfilEvaluadorNewRecord
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsPerfilEvaluadorAlAdquirirDatos
    AlEnviarDatos = cdsPerfilEvaluadorAlEnviarDatos
    AlCrearCampos = cdsPerfilEvaluadorAlCrearCampos
    AlBorrar = cdsPerfilEvaluadorAlBorrar
    AlModificar = cdsPerfilEvaluadorAlModificar
    Left = 40
    Top = 144
  end
  object cdsEmpEvaluar: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsReconcileError
    AlCrearCampos = cdsEmpEvaluarAlCrearCampos
    AlAgregar = cdsEmpEvaluarAlAgregar
    AlModificar = cdsEmpEvaluarAlModificar
    Left = 128
    Top = 144
  end
  object cdsEncuestas: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'ET_CODIGO'
    Params = <>
    BeforePost = cdsEncuestasBeforePost
    AfterDelete = cdsEncuestasAfterDelete
    OnNewRecord = cdsEncuestasNewRecord
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsEncuestasAlAdquirirDatos
    AlEnviarDatos = cdsEncuestasAlEnviarDatos
    AlCrearCampos = cdsEncuestasAlCrearCampos
    AlAgregar = cdsEncuestasAlAgregar
    AlModificar = cdsEncuestasAlModificar
    UsaCache = True
    LookupName = 'Encuestas'
    LookupDescriptionField = 'ET_NOMBRE'
    LookupKeyField = 'ET_CODIGO'
    Left = 224
    Top = 144
  end
  object cdsCriterios: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'CM_DESCRIP'
    Params = <>
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsCriteriosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Competencias / Habilidades'
    LookupDescriptionField = 'CM_DESCRIP'
    LookupKeyField = 'CM_CODIGO'
    Left = 400
    Top = 16
  end
  object cdsAgregaCriterios: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 312
    Top = 144
  end
  object cdsAgregaEvaluador: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 408
    Top = 144
  end
  object cdsNivelesEnc: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'EL_ORDEN'
    Params = <>
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsNivelesEncAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Niveles de Encuesta'
    LookupDescriptionField = 'EL_NOMBRE'
    LookupKeyField = 'EL_ORDEN'
    OnGetRights = cdsNivelesEncGetRights
    Left = 496
    Top = 16
  end
  object cdsEscalaNivEnc: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforeInsert = cdsEscalaNivEncBeforeInsert
    AfterInsert = cdsEscalaNivEncAfterInsert
    AfterEdit = cdsEscalaNivEncAfterInsert
    AfterDelete = cdsEscalaNivEncAfterInsert
    OnNewRecord = cdsEscalaNivEncNewRecord
    OnReconcileError = cdsReconcileError
    AlCrearCampos = cdsEscalaNivEncAlCrearCampos
    Left = 512
    Top = 144
  end
  object cdsEscalasEnc: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'EE_CODIGO'
    Params = <>
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsEscalasEncAlAdquirirDatos
    UsaCache = True
    LookupName = 'Escalas de Encuesta'
    LookupDescriptionField = 'EE_NOMBRE'
    LookupKeyField = 'EE_CODIGO'
    OnGetRights = cdsEscalasEncGetRights
    Left = 40
    Top = 208
  end
  object cdsEvalua: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'EV_FOLIO'
    Params = <>
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsEvaluaAlAdquirirDatos
    Left = 128
    Top = 208
  end
  object cdsTotalesEncuesta: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsTotalesEncuestaAlAdquirirDatos
    AlCrearCampos = cdsTotalesEncuestaAlCrearCampos
    Left = 216
    Top = 208
  end
  object cdsSujeto: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsSujetoAlAdquirirDatos
    AlCrearCampos = cdsSujetoAlCrearCampos
    Left = 312
    Top = 208
  end
  object cdsEvaluadores: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsReconcileError
    AlCrearCampos = cdsEvaluadoresAlCrearCampos
    Left = 384
    Top = 208
  end
  object cdsUsuarios: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsReconcileError
    OnGetRights = cdsUsuariosGetRights
    Left = 40
    Top = 272
  end
  object cdsEvaluaciones: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsReconcileError
    AlCrearCampos = cdsEvaluacionesAlCrearCampos
    Left = 480
    Top = 208
  end
  object cdsAnalisisCriterio: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsReconcileError
    Left = 128
    Top = 272
  end
  object cdsAnalisisPregunta: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsAnalisisPreguntaAlAdquirirDatos
    Left = 232
    Top = 272
  end
  object cdsCriteriosEnc: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'EC_ORDEN'
    Params = <>
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsCriteriosEncAlAdquirirDatos
    UsaCache = True
    LookupName = 'Criterios por Encuesta'
    LookupDescriptionField = 'EC_NOMBRE'
    LookupKeyField = 'EC_ORDEN'
    OnGetRights = cdsCriteriosEncGetRights
    Left = 328
    Top = 272
  end
  object cdsFrecuenciaResp: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsReconcileError
    AlCrearCampos = cdsFrecuenciaRespAlCrearCampos
    Left = 424
    Top = 272
  end
  object cdsPreguntasEnc: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'PG_FOLIO'
    Params = <>
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsPreguntasEncAlAdquirirDatos
    UsaCache = True
    LookupName = 'Preguntas por Encuesta'
    LookupDescriptionField = 'EP_DESCRIP'
    LookupKeyField = 'PG_FOLIO'
    OnGetRights = cdsPreguntasEncGetRights
    Left = 520
    Top = 272
  end
  object cdsResultadoCriterio: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsResultadoCriterioAlAdquirirDatos
    Left = 40
    Top = 336
  end
  object cdsResultadoPregunta: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsResultadoPreguntaAlAdquirirDatos
    Left = 152
    Top = 336
  end
  object cdsAgregaEmpEvaluar: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsAgregaEmpEvaluarReconcileError
    AlAdquirirDatos = cdsAgregaEmpEvaluarAlAdquirirDatos
    AlEnviarDatos = cdsAgregaEmpEvaluarAlEnviarDatos
    AlCrearCampos = cdsAgregaEmpEvaluarAlCrearCampos
    Left = 272
    Top = 336
  end
  object cdsNivelesEscala: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'SN_ORDEN'
    Params = <>
    BeforeInsert = cdsNivelesEscalaBeforeInsert
    AfterInsert = cdsNivelesEscalaAfterInsert
    AfterEdit = cdsNivelesEscalaAfterInsert
    AfterDelete = cdsNivelesEscalaAfterInsert
    OnNewRecord = cdsNivelesEscalaNewRecord
    OnReconcileError = cdsReconcileError
    AlCrearCampos = cdsNivelesEscalaAlCrearCampos
    UsaCache = True
    LookupName = 'Niveles de Escala'
    LookupDescriptionField = 'SN_NOMBRE'
    LookupKeyField = 'SN_ORDEN'
    OnGetRights = cdsNivelesEscalaGetRights
    Left = 128
    Top = 86
  end
  object cdsTotalesEvaluacion: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsResultadoPreguntaAlAdquirirDatos
    Left = 392
    Top = 336
  end
  object cdsTotalesEvaluador: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsResultadoPreguntaAlAdquirirDatos
    Left = 504
    Top = 336
  end
  object cdsTotalesEvaluado: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsReconcileError
    Left = 40
    Top = 392
  end
  object cdsDatosxEvaluado: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsReconcileError
    Left = 152
    Top = 392
  end
  object cdsEvaluadosLookup: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'CB_CODIGO'
    Params = <>
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsEvaluadosLookupAlAdquirirDatos
    LookupName = 'Empleados a evaluar'
    LookupDescriptionField = 'PRETTYNAME'
    LookupKeyField = 'CB_CODIGO'
    OnGetRights = cdsEvaluadosLookupGetRights
    Left = 272
    Top = 392
  end
  object cdsEvaluadoresLookup: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'US_CODIGO'
    Params = <>
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsEvaluadoresLookupAlAdquirirDatos
    AlCrearCampos = cdsEvaluadoresLookupAlCrearCampos
    LookupName = 'Evaluadores'
    LookupDescriptionField = 'US_NOMBRE'
    LookupKeyField = 'US_CODIGO'
    OnLookupSearch = cdsEvaluadoresLookupLookupSearch
    OnGetRights = cdsEvaluadoresLookupGetRights
    Left = 392
    Top = 392
  end
  object cdsCorreos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnCalcFields = cdsCorreosCalcFields
    AlCrearCampos = cdsCorreosAlCrearCampos
    AlModificar = cdsCorreosAlModificar
    Left = 504
    Top = 392
  end
  object cdsUsuariosRol: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterCancel = cdsUsuariosRolAfterCancel
    AfterScroll = cdsUsuariosRolAfterScroll
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsUsuariosRolAlAdquirirDatos
    AlEnviarDatos = cdsUsuariosRolAlEnviarDatos
    AlCrearCampos = cdsUsuariosRolAlCrearCampos
    AlModificar = cdsUsuariosRolAlModificar
    Left = 40
    Top = 456
  end
end
