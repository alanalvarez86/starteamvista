unit ZArbolTress;

interface

uses ComCtrls, Dialogs, SysUtils, DB, Controls,
     ZArbolTools, ZNavBarTools, ZetaCommonLists,cxTreeView;

function  GetNodoInfo( const iNodo: Integer ): TNodoInfo;
procedure CreaArbolDefault( oArbolMgr: TArbolMgr );
procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr ; Arbolitos: array of TcxTreeView );
function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
procedure CreaArbolitoDefault( oArbolMgr: TArbolMgr ; iAccesoGlobal: Integer );
procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String );

implementation

uses ZetaDespConsulta,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZAccesosMgr,
     ZAccesosTress,
     ZGlobalTress,
     FCatTCompetencias,
     DGlobal,
     DSistema,
     DCliente;
     
const
      K_EVALUACION_ADMIN    = 900;
      K_DISENO_ENCUESTA     = 901;
        K_CRITERIOS_EVAL    = 902;
        K_PREG_CRITERIO     = 903;
        K_ESCALAS           = 904;
        K_PERFIL_EVAL       = 905;
        K_EMP_EVAL          = 906;
        K_ENCUESTAS         = 907;
        K_DISENO_REGISTRO   = 908;
        K_DISENO_PROCESOS   = 910;
      K_RESULTADOS_ENC      = 911;
        K_TOTALES_ENC       = 912;
        K_EMP_EVALUADOS     = 913;
        K_EVALUADORES       = 914;
        K_EVALUACIONES      = 915;
        K_ANALISIS_CRITERIO = 916;
        K_ANALISIS_PREGUNTA = 917;
        K_FRECUENCIA_RESP   = 918;
      K_RESULTADOS_INDIVID  = 919;
        K_RESULTADO_CRITERIO= 920;
        K_RESULTADO_PREGUNTA= 921;
      K_CONSULTAS           = 922;
        K_REPORTES          = 923;
        K_CONS_SQL          = 924;
        K_PROCESOS          = 925;
        K_CORREOS           = 933;
        K_REPORTA           = 934;
      K_CATALOGOS           = 926;
        K_CAT_EVA_COMPETE   = 927;
        K_CAT_EVA_PREGUNTA  = 928;
        K_CAT_EVA_TCOMPETE  = 929;
        K_CAT_EVA_ESCALAS   = 930;
      K_SISTEMA             = 931;
        K_GLOBAL_EVALUACION = 932;
        K_DICCIONARIO       = 935;


function Forma( const sCaption: String; const eTipo: eFormaConsulta ): TNodoInfo;
begin
     Result.EsForma   := TRUE;
     Result.Caption   := sCaption;
     Result.TipoForma := eTipo;
end;

function Folder( const sCaption: String; const iDerechos: Integer ): TNodoInfo;
begin
     Result.EsForma   := FALSE;
     Result.Caption   := sCaption;
     Result.IndexDerechos  := iDerechos;
end;
function Grupo( const sCaption: String; const iDerechos: Integer ): TGrupoInfo;
begin
     Result.Caption := sCaption;
     Result.IndexDerechos := iDerechos;
end;
function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
begin
     with Global do
     begin
          case iNodo of
               //K_EVALUACION_ADMIN : Result := Folder( 'Evaluaci�n/Encuestas', D_EVALUACION );
               K_DISENO_ENCUESTA  : Result := Folder( 'Dise�o de encuesta', D_EVAL_DISENO_ENCUESTA );
                 K_CRITERIOS_EVAL : Result := Forma( 'Criterios a evaluar', efcCriteriosEval );
                 K_PREG_CRITERIO  : Result := Forma( 'Preguntas por criterio', efcPregxCriterio );
                 K_ESCALAS        : Result := Forma( 'Escalas de encuesta', efcEscalasEncuesta );
                 K_PERFIL_EVAL    : Result := Forma( 'Perfil de evaluadores', efcPerfilEval );
                 K_EMP_EVAL       : Result := Forma( 'Empleados a evaluar', efcEmpleadosEval );
                 K_DISENO_REGISTRO: Result := Forma( 'Registros', efcRegistrosDisenoEnc );
                 K_DISENO_PROCESOS : Result := Forma( 'Procesos', efcProcesosDisenoEnc );
               K_RESULTADOS_ENC   : Result := Folder( 'Resultados de encuesta', D_EVAL_RESULTADOS_ENC );
                 K_TOTALES_ENC    : Result := Forma( 'Totales de encuesta', efcTotalesEncuesta );
                 K_EMP_EVALUADOS  : Result := Forma( 'Empleados evaluados', efcSujeto );
                 K_EVALUADORES    : Result := Forma( 'Evaluadores', efcEvaluadores );
                 K_EVALUACIONES   : Result := Forma( 'Evaluaciones', efcEvaluaciones );
                 K_ANALISIS_CRITERIO : Result := Forma( 'An�lisis por criterio', efcAnalisisCriterio );
                 K_ANALISIS_PREGUNTA : Result := Forma( 'An�lisis por pregunta', efcAnalisisPregunta );
                 K_FRECUENCIA_RESP: Result := Forma( 'Frecuencia de respuestas', efcFrecuenciaResp );
               K_RESULTADOS_INDIVID: Result := Folder( 'Resultados individuales', D_EVA_RESULTADOS_INDIV );
                 K_RESULTADO_CRITERIO: Result := Forma( 'Resultados por criterio', efcResultadosCriterio );
                 K_RESULTADO_PREGUNTA: Result := Forma( 'Resultados por pregunta', efcResultadosPregunta );
               // Consultas
               K_CONSULTAS       : Result := Folder( 'Consultas', D_EVALUACION_CONSULTAS );
                 K_REPORTES      : Result := Forma( 'Reportes' , efcReportes );
                 K_CONS_SQL      : Result := Forma( 'SQL', efcQueryGral );
                 K_PROCESOS      : Result := Forma( 'Procesos', efcSistProcesos );
                 K_CORREOS       : Result := Forma( 'Correos', efcCorreos );
              K_CATALOGOS        : Result := Folder( 'Cat�logos', D_EVALUACION_CATALOGOS );
                K_ENCUESTAS      : Result := Forma( 'Encuestas', efcEncuestas );
                K_CAT_EVA_TCOMPETE : Result := Forma( 'Tipos de competencia' , efcCatTCompeten );
                K_CAT_EVA_COMPETE : Result := Forma( 'Competencias' , efcCatCompeten );
                K_CAT_EVA_PREGUNTA : Result := Forma( 'Preguntas' , efcCatPreguntas );
                K_CAT_EVA_ESCALAS  : Result := Forma( 'Escalas' , efcCatEscalas );
               K_SISTEMA         : Result := Folder( 'Sistema', D_EVALUACION_SISTEMA );
                 K_REPORTA       : Result := Forma( 'Usuarios', efcReportaA );
                 K_GLOBAL_EVALUACION: Result := Forma( 'Globales de Empresa', efcSistGlobales );
                 K_DICCIONARIO   : Result := Forma( 'Diccionario de Datos', efcDiccion );
            else
                Result := Folder( VACIO, 0 );
            end;
     end;
end;

function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
begin
     case iNodo of
          // Registros
          D_EVAL_DISENO_ENCUESTA              : Result := Grupo( 'Dise�o de Encuesta', iNodo );
          // Resultado
          D_EVAL_RESULTADOS_ENC            : Result := Grupo( 'Resultados de Encuesta', iNodo );
          //Resultados individuales
          D_EVA_RESULTADOS_INDIV           :  Result := Grupo ('Resultados individuales',iNodo );
          // Consultas
          D_EVALUACION_CONSULTAS           : Result := Grupo( 'Consultas', iNodo );
          // Cat�logos
          D_EVALUACION_CATALOGOS           : Result := Grupo( 'Cat�logos', iNodo );
		  //Sistema
		  D_EVALUACION_SISTEMA		       : Result := Grupo( 'Sistema', iNodo );
     else
          Result := Grupo( '', 0 );
     end;
end;
procedure CreaArbolDefault( oArbolMgr: TArbolMgr );
{var
   i : Integer;}

   procedure NodoNivel( const iNivel, iNodo: Integer );
   begin
        oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo );
   end;

begin
     NodoNivel( 0, K_DISENO_ENCUESTA );
        NodoNivel( 1,K_CRITERIOS_EVAL );
        NodoNivel( 1,K_PREG_CRITERIO );
        NodoNivel( 1,K_ESCALAS );
        NodoNivel( 1,K_PERFIL_EVAL );
        NodoNivel( 1,K_EMP_EVAL );
        NodoNivel( 1,K_DISENO_REGISTRO );
        NodoNivel( 1,K_DISENO_PROCESOS );
     NodoNivel( 0, K_RESULTADOS_ENC );
        NodoNivel( 1,K_TOTALES_ENC );
        NodoNivel( 1,K_EMP_EVALUADOS );
        NodoNivel( 1,K_EVALUADORES );
        NodoNivel( 1,K_EVALUACIONES );
        NodoNivel( 1,K_ANALISIS_CRITERIO );
        NodoNivel( 1,K_ANALISIS_PREGUNTA );
        NodoNivel( 1,K_FRECUENCIA_RESP );
     NodoNivel( 0, K_RESULTADOS_INDIVID );
        NodoNivel( 1,K_RESULTADO_CRITERIO );
        NodoNivel( 1,K_RESULTADO_PREGUNTA );
     NodoNivel( 0, K_CONSULTAS );
        NodoNivel( 1,K_REPORTES );
        NodoNivel( 1,K_CONS_SQL );
        NodoNivel( 1,K_PROCESOS );
        NodoNivel( 1,K_CORREOS );
     NodoNivel( 0, K_CATALOGOS );
        NodoNivel( 1,K_CAT_EVA_TCOMPETE );
        NodoNivel( 1,K_CAT_EVA_COMPETE );
        NodoNivel( 1,K_CAT_EVA_PREGUNTA );
        NodoNivel( 1,K_CAT_EVA_ESCALAS );
        NodoNivel( 1,K_ENCUESTAS );
     NodoNivel( 0, K_SISTEMA );
        NodoNivel( 1, K_REPORTA );
        NodoNivel( 1, K_GLOBAL_EVALUACION );
        NodoNivel( 1, K_DICCIONARIO );
end;

procedure CreaArbolitoDefault( oArbolMgr : TArbolMgr; iAccesoGlobal: Integer );
function NodoNivel( const iNivel, iNodo: Integer ): Boolean;
begin
     Result := oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo); //Siempre mandar 0 en iNodo
end;
begin
      case iAccesoGlobal of
            //Registros
            K_DISENO_ENCUESTA		:
            begin
                 NodoNivel( 0, K_CRITERIOS_EVAL );
                 NodoNivel( 0, K_PREG_CRITERIO );
                 NodoNivel( 0, K_ESCALAS );
                 NodoNivel( 0, K_PERFIL_EVAL );
                 NodoNivel( 0, K_EMP_EVAL );
            end;
            //Consultas
           K_RESULTADOS_ENC      :
           begin
                 NodoNivel( 0, K_TOTALES_ENC );
                 NodoNivel( 0, K_EMP_EVALUADOS );
                 NodoNivel( 0, K_EVALUADORES );
                 NodoNivel( 0, K_EVALUACIONES );
                 NodoNivel( 0, K_ANALISIS_CRITERIO );
                 NodoNivel( 0, K_ANALISIS_PREGUNTA );
                 NodoNivel( 0, K_FRECUENCIA_RESP );
            end;
            //Cat�logos
            K_RESULTADOS_INDIVID               :
            begin
                 NodoNivel( 0, K_RESULTADO_CRITERIO );
                 NodoNivel( 0, K_RESULTADO_PREGUNTA );
            end;
            //Tablas
            K_CONSULTAS               :
            begin
                 NodoNivel( 0, K_REPORTES );
                 NodoNivel( 0, K_CONS_SQL );
                 NodoNivel( 0, K_PROCESOS );
                 NodoNivel( 0, K_CORREOS );
            end;
            //Catalogos
            K_CATALOGOS               :
            begin
                 NodoNivel( 0, K_CAT_EVA_TCOMPETE );
                 NodoNivel( 0, K_CAT_EVA_COMPETE );
                 NodoNivel( 0, K_CAT_EVA_PREGUNTA );
                 NodoNivel( 0, K_CAT_EVA_ESCALAS );
                 NodoNivel( 0, K_ENCUESTAS );
            end;
            //Sistema
            K_SISTEMA               :
            begin
                 NodoNivel( 0, K_REPORTA );
                 NodoNivel( 0, K_GLOBAL_EVALUACION );
                 NodoNivel( 0, K_DICCIONARIO );
            end;
      end;
end;

procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr; Arbolitos: array of TcxTreeView);
var NodoRaiz: TGrupoInfo;
function NodoNivel( iNodo, iGroupIndex: Integer): Boolean;
begin
     Result := oNavBarMgr.NodoNivel( GetGrupoInfo( iNodo ), iGroupIndex, iNodo );
end;
procedure CreaArbolito( const K_GLOBAL:Integer; Arbolito: TcxTreeView);
var
   oArbolMgr: TArbolMgr;

begin
     with Arbolito do
     begin
          Parent := oNavBarMgr.NavBar.Groups[oNavBarMgr.UltimoGrupo.Index].Control;
          Align := alClient;
     end;
     oArbolMgr := TArbolMgr.Create( Arbolito );
     with oArbolMgr do
     begin
          try
             with Arbolito.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     // Aqu� se agrega el arbol definido por el usuario
                     //CreaArbolitoUsuario( oArbolMgr, '',K_GLOBAL );
                     //Antes metodo: CreaArbolUsuario
                     with oArbolMgr do
                     begin
                           //Configuracion := TRUE;
                           //oArbolMgr.NumDefault := K_GLOBAL ;
                           CreaArbolitoDefault( oArbolMgr, K_GLOBAL  );
                           Arbol_DevEx.FullCollapse;//edit by mp: el arbol dentro del navbar es el arbol_DevEx
                     end;
                  finally
                     EndUpdate;
                  end;
             end;
          finally
             Free;
          end;


     end;
end;
begin
     {***OJO***}
     {El index del grupo comienza desde 1 porque el 0 es para el grupo del Arbol del Usuario. En VisitantesMGR inica desde
      cero porque no hay Arbol de Usuario.}


           // ******** Dise�o de Encuesta ***********
           if( NodoNivel( D_EVAL_DISENO_ENCUESTA, 0 )) then
                      CreaArbolito ( K_DISENO_ENCUESTA, Arbolitos[0]);
           // ******** Resultados de Encuesta ***********
           if( NodoNivel( D_EVAL_RESULTADOS_ENC, 1 )) then
                      CreaArbolito ( K_RESULTADOS_ENC, Arbolitos[1]);
            // ******** Resultados de Encuesta ***********
           if( NodoNivel( D_EVA_RESULTADOS_INDIV, 2 )) then
                      CreaArbolito ( K_RESULTADOS_INDIVID, Arbolitos[2]);
           // ******** Consultas ***********
           if( NodoNivel( D_EVALUACION_CONSULTAS, 3 )) then
                      CreaArbolito ( K_CONSULTAS, Arbolitos[3]);
           // ******* Cat�logos *************
           if( NodoNivel( D_EVALUACION_CATALOGOS, 4 ) ) then
                      CreaArbolito ( K_CATALOGOS, Arbolitos[4]);
           // ******* Cat�logos *************
           if( NodoNivel( D_EVALUACION_SISTEMA, 5 ) ) then
                      CreaArbolito ( K_SISTEMA, Arbolitos[5]);
           // ******* Sin Derechos *************
           if ( oNavBarMgr.NavBar.Groups.Count = 0 ) then
           begin
                with NodoRaiz do
                begin
                     Caption := 'No tiene derechos';
                     IndexDerechos := K_SIN_RESTRICCION;
                     oNavBarMgr.NodoNivel(NodoRaiz,0, K_ARBOL_NODO_RAIZ);
                end;
           end;
end;

procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String );
begin
     with oArbolMgr do
     begin
          Configuracion := TRUE;
          oArbolMgr.NumDefault := K_EVALUACION_ADMIN;
          CreaArbolDefault( oArbolMgr );
          Arbol.FullExpand;
     end;
end;

end.
