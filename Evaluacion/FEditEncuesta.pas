unit FEditEncuesta;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask, ComCtrls,
     ZBaseEdicion_DevEx,
     ZetaSmartLists,
     ZetaDBTextBox,
     ZetaKeyLookup,
     ZetaFecha,
     ZetaKeyCombo,
     ZetaCommonLists, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, cxControls, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList,
  cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx, dxBarBuiltInMenu,
  cxPC, cxContainer, cxEdit, cxGroupBox;

type
  TEditEncuesta = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    ET_NOMBRElbl: TLabel;
    ztbTitulo: TZetaTextBox;
    ET_NOMBRE: TDBEdit;
    PageControl: TcxPageControl;
    tsGenerales: TcxTabSheet;
    ET_FEC_INIlbl: TLabel;
    ET_DESCRIPgb: TcxGroupBox;
    ET_DESCRIP: TDBMemo;
    ET_FEC_FINlbl: TLabel;
    ET_USR_ADMlbl: TLabel;
    ET_FEC_FIN: TZetaDBFecha;
    ET_FEC_INI: TZetaDBFecha;
    ET_USR_ADM: TZetaDBKeyLookup_DevEx;
    tsEscalas: TcxTabSheet;
    ET_ESC_DEFlbl: TLabel;
    ET_ESC_DEF: TZetaDBKeyLookup_DevEx;
    ET_ESCALAlbl: TLabel;
    ET_ESCALA: TDBEdit;
    ET_ESTILOlbl: TLabel;
    ET_ESTILO: TZetaDBKeyCombo;
    ET_ESC_PESlbl: TLabel;
    ET_ESC_PES: TZetaDBKeyLookup_DevEx;
    ET_TXT_COMlbl: TLabel;
    ET_TXT_COM: TDBEdit;
    tsMensajes: TcxTabSheet;
    ET_MSG_INIgb: TcxGroupBox;
    ET_MSG_INI: TDBMemo;
    tsDespedida: TcxTabSheet;
    ET_MSG_FINgb: TcxGroupBox;
    ET_MSG_FIN: TDBMemo;
    ET_GET_COM: TDBCheckBox;
    ET_STATUSlbl: TLabel;
    ET_STATUS: TZetaDBKeyCombo;
    ET_DOCUMENlbl: TLabel;
    ET_DOCUMEN: TDBEdit;
    ET_DOCUMENSeek: TcxButton;
    OpenDialog: TOpenDialog;
    ET_PTS_CALlbl: TLabel;
    ET_PTS_CAL: TZetaDBKeyCombo;
    ET_ESC_CAL: TZetaDBKeyLookup_DevEx;
    ET_ESC_CALlbl: TLabel;
    ET_CONSOLA: TDBCheckBox;
    ET_CONSOLAlbl: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure ET_GET_COMClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ET_STATUSChange(Sender: TObject);
    procedure ET_DOCUMENSeekClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure ET_PTS_CALClick(Sender: TObject);
  private
    { Private declarations }
    {$ifdef ANTES}
    FStatusFueModificado: Boolean;
    FStatusOriginal: eStatusEncuesta;
    procedure ContinuaGrabando;
    {$endif}
    procedure SetControls;
    procedure VerificaHTML( const sMSG: String; lMSGInicial: Boolean );
  protected
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure DoLookup; override;
    procedure EscribirCambios; override;
  public
    { Public declarations }
  end;

var
  EditEncuesta: TEditEncuesta;

implementation

{$R *.DFM}

uses DEvaluacion,
     DCliente,
     DSistema,
     ZHelpContext,
     ZAccesosTress,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaBuscador,
     FTressShell;

{ TEditEncuesta }

procedure TEditEncuesta.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_ENCUESTAS;
     ET_STATUS.ListaFija := lfStatusEncuesta;
     ET_PTS_CAL.ListaFija := lfComoCalificar;
     ET_ESTILO.ListaFija := lfEstiloRespuesta;
     ET_USR_ADM.LookupDataSet := dmSistema.cdsUsuarios;
     with dmEvaluacion do
     begin
          ET_ESC_DEF.LookupDataSet := cdsEscalasEnc;
          ET_ESC_PES.LookupDataSet := cdsEscalasEnc;
          ET_ESC_CAL.LookupDataSet := cdsEscalasEnc;
     end;
     ActiveControl := ET_NOMBRE;
     HelpContext := H_CATALOGOS_ENCUESTAS_MODIFICAR_REGISTRO;
end;

procedure TEditEncuesta.FormShow(Sender: TObject);
begin
     inherited;
     {$ifdef ANTES}
     FStatusFueModificado := False;
     FStatusOriginal := eStatusEncuesta( ET_STATUS.ItemIndex );
     {$endif}
     ztbTitulo.Caption := InttoStr( dmEvaluacion.cdsEncuestas.FieldByName('ET_CODIGO').AsInteger );
     FirstControl := ET_NOMBRE;
     PageControl.ActivePageIndex := 0;
     SetControls;
end;

procedure TEditEncuesta.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmEvaluacion do
     begin
          ObtieneEscalasEncuesta( cdsEncuestas.FieldByName('ET_CODIGO').AsInteger );
          DataSource.DataSet := cdsEncuestas;
     end;
end;

procedure TEditEncuesta.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Encuesta', 'ENCUESTA', 'ET_CODIGO', dmEvaluacion.cdsEncuestas );
end;

function TEditEncuesta.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede agregar encuestas desde esta forma';
     Result := False;
end;

function TEditEncuesta.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede borrar encuestas desde esta forma';
     Result := False;
end;

procedure TEditEncuesta.SetControls;
var
   lPuedeEditar: Boolean;
begin
     case eStatusEncuesta( dmEvaluacion.cdsEncuestas.FieldByName( 'ET_STATUS' ).AsInteger ) of
          stenDiseno: lPuedeEditar := True;
     else
         lPuedeEditar := False;
     end;
     ET_NOMBRElbl.Enabled := lPuedeEditar;
     ET_NOMBRE.Enabled := lPuedeEditar;
     ET_FEC_INIlbl.Enabled := lPuedeEditar;
     ET_FEC_INI.Enabled := lPuedeEditar;
     ET_DESCRIPgb.Enabled := lPuedeEditar;
     ET_DESCRIP.Enabled := lPuedeEditar;
     {
     ET_FEC_FINlbl.Enabled := lPuedeEditar;
     ET_FEC_FIN.Enabled := lPuedeEditar;
     ET_CONSOLAlbl.Enabled := lPuedeEditar;
     ET_CONSOLA.Enabled := lPuedeEditar;
     }
     ET_USR_ADMlbl.Enabled := lPuedeEditar;
     ET_USR_ADM.Enabled := lPuedeEditar;
     ET_ESC_DEFlbl.Enabled := lPuedeEditar;
     ET_ESC_DEF.Enabled := lPuedeEditar;
     ET_ESCALAlbl.Enabled := lPuedeEditar;
     ET_ESCALA.Enabled := lPuedeEditar;
     ET_ESTILOlbl.Enabled := lPuedeEditar;
     ET_ESTILO.Enabled := lPuedeEditar;
     ET_ESC_PESlbl.Enabled := lPuedeEditar;
     ET_ESC_PES.Enabled := lPuedeEditar;
     ET_MSG_INIgb.Enabled := lPuedeEditar;
     ET_MSG_INI.Enabled := lPuedeEditar;
     ET_MSG_FINgb.Enabled := lPuedeEditar;
     ET_MSG_FIN.Enabled := lPuedeEditar;
     ET_GET_COM.Enabled := lPuedeEditar;
     ET_DOCUMENlbl.Enabled := lPuedeEditar;
     ET_DOCUMEN.Enabled := lPuedeEditar;
     ET_DOCUMENSeek.Enabled := lPuedeEditar;
     ET_TXT_COMlbl.Enabled := lPuedeEditar and ET_GET_COM.Checked;
     ET_TXT_COM.Enabled := ET_TXT_COMlbl.Enabled;
     ET_PTS_CALlbl.Enabled := lPuedeEditar;
     ET_PTS_CAL.Enabled := ET_PTS_CALlbl.Enabled;
     ET_ESC_CALlbl.Enabled := lPuedeEditar and ( eComoCalificar( ET_PTS_CAL.Valor ) <> eccaNoUsar );
     ET_ESC_CAL.Enabled := ET_ESC_CALlbl.Enabled;
end;

procedure TEditEncuesta.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if Assigned( Field ) then
     begin
          SetControls;
     end;
end;

procedure TEditEncuesta.ET_GET_COMClick(Sender: TObject);
begin
     inherited;
     SetControls;
end;

procedure TEditEncuesta.EscribirCambios;
begin
     with dmEvaluacion do
     begin
          {$ifdef ANTES}
          if FStatusFueModificado then
          begin
               if ValidaCambioStatusEncuesta( eStatusEncuesta( ET_STATUS.ItemIndex ) ) then
                  ContinuaGrabando;
          end
          else
          begin
               if ModificaDisenoEncuesta( False, FStatusOriginal ) then
               begin
                    if ValidaCambioStatusEncuesta( eStatusEncuesta( ET_STATUS.ItemIndex ) ) then
                       ContinuaGrabando;
               end;
          end;
          {$else}
          if ValidaCambioStatusEncuesta( eStatusEncuesta( ET_STATUS.ItemIndex ) ) then
          begin
               //VerificaHTML( ET_MSG_INI.Lines.Text, True );{OP: 04.11.08}
               //VerificaHTML( ET_MSG_FIN.Lines.Text, False );{OP: 04.11.08}
               GrabaEncuesta( False );
               dmCliente.RefrescaEncuesta; //Para que refresque el status
               TressShell.RefrescaEncuestasActivas;
          end;
          {$endif}
     end;
end;

{$ifdef ANTES}
procedure TEditEncuesta.ContinuaGrabando;
begin
     with dmEvaluacion do
     begin
          //VerificaHTML( ET_MSG_INI.Lines.Text, True );{OP: 04.11.08}
          //VerificaHTML( ET_MSG_FIN.Lines.Text, False );{OP: 04.11.08}
          GrabaEncuesta( False );
          dmCliente.RefrescaEncuesta; //Para que refresque el status
          TressShell.RefrescaEncuestasActivas;
     end;
end;
{$endif}

procedure TEditEncuesta.VerificaHTML( const sMSG: String; lMSGInicial: Boolean );
const
     K_ESPACIO = ' ';
     K_DELIMITADOR = #$D#$A;

  procedure QuitaCaracteresASCII( const iIndice: Integer; var sMensaje:String; lInicial: Boolean );
  var
     i: Integer;
  begin
       if ( iIndice > 0 ) then
       begin
            for i := iIndice to ( iIndice + 1 ) do
            begin
                 sMensaje[ i ] := K_ESPACIO
            end;
            if lInicial then
               ET_MSG_INI.Lines.Text := sMensaje
            else
                ET_MSG_FIN.Lines.Text := sMensaje;
            VerificaHTML( sMensaje, lInicial );
       end;
  end;

var
   sMensaje: String;
begin
     sMensaje := sMSG;
     if lMSGInicial then
     begin
          if StrVacio( sMensaje ) then
             ET_MSG_INI.Text := K_ESPACIO
          else
              QuitaCaracteresASCII( Pos( K_DELIMITADOR, sMensaje ), sMensaje, lMSGInicial );
     end
     else
     begin
          if StrVacio( sMensaje ) then
             ET_MSG_FIN.Text := K_ESPACIO
          else
              QuitaCaracteresASCII( Pos( K_DELIMITADOR, sMensaje ), sMensaje, lMSGInicial );
     end;
end;

procedure TEditEncuesta.ET_PTS_CALClick(Sender: TObject);
begin
     inherited;
     SetControls;
end;

procedure TEditEncuesta.ET_STATUSChange(Sender: TObject);
{$ifdef ANTES}
var
   eNewStatus: eStatusEncuesta;
{$endif}
begin
     inherited;
     {$ifdef ANTES}
     eNewStatus := eStatusEncuesta( ET_STATUS.ItemIndex );
     FStatusFueModificado := ( eNewStatus <> FStatusOriginal ) and ( eNewStatus <> eStatusEncuesta( dmEvaluacion.cdsEncuestas.FieldByName( 'ET_STATUS' ).AsInteger ) );
     if FStatusFueModificado then
     begin
          FStatusOriginal := eNewStatus;
     end;
     {$endif}
end;

procedure TEditEncuesta.ET_DOCUMENSeekClick(Sender: TObject);
begin
     inherited;
     with ET_DOCUMEN do
     begin
          with OpenDialog do
          begin
               FileName := Text;
               if Execute then
               begin
                    with dmEvaluacion do
                    begin
                         CambiarDocumentacion( cdsEncuestas, 'ET_DOCUMEN', ExtractFileName( FileName ) );
                    end;
               end;
          end;
     end;
end;

end.
