inherited AgregarEmpleado_DevEx: TAgregarEmpleado_DevEx
  Left = 437
  Top = 144
  Caption = 'Agregar Evaluador'
  ClientHeight = 390
  ClientWidth = 429
  OldCreateOrder = True
  Position = poDesigned
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  ExplicitWidth = 435
  ExplicitHeight = 419
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 354
    Width = 429
    ExplicitTop = 354
    ExplicitWidth = 429
    inherited OK_DevEx: TcxButton
      Left = 256
      Width = 79
      Caption = '  &Agregar'
      ParentFont = False
      OnClick = OK_DevExClick
      ExplicitLeft = 256
      ExplicitWidth = 79
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 340
      ParentFont = False
      ExplicitLeft = 340
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 0
    Width = 429
    Height = 57
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 48
      Height = 13
      Alignment = taRightJustify
      Caption = 'Encuesta:'
    end
    object ztbTitulo: TZetaTextBox
      Left = 60
      Top = 6
      Width = 357
      Height = 17
      AutoSize = False
      Caption = 'ztbTitulo'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object Label2: TLabel
      Left = 6
      Top = 32
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empleado:'
    end
    object ztbEmpleado: TZetaTextBox
      Left = 60
      Top = 30
      Width = 357
      Height = 17
      AutoSize = False
      Caption = 'ztbEmpleado'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
  end
  object Panel2: TPanel [2]
    Left = 0
    Top = 57
    Width = 429
    Height = 32
    Align = alTop
    TabOrder = 2
    object Label3: TLabel
      Left = 20
      Top = 8
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'Buscar:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object edBuscar: TEdit
      Left = 60
      Top = 5
      Width = 250
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 30
      ParentFont = False
      TabOrder = 0
      OnKeyUp = edBuscarKeyUp
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [3]
    Left = 0
    Top = 89
    Width = 429
    Height = 231
    Align = alClient
    DataSource = dsAgregaEmp
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = ZetaDBGrid1DblClick
    Columns = <
      item
        Alignment = taRightJustify
        Color = clInfoBk
        Expanded = False
        FieldName = 'US_CODIGO'
        Title.Alignment = taRightJustify
        Title.Caption = 'N'#250'mero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 330
        Visible = True
      end>
  end
  object Panel3: TPanel [4]
    Left = 0
    Top = 320
    Width = 429
    Height = 34
    Align = alBottom
    TabOrder = 4
    object Label4: TLabel
      Left = 9
      Top = 9
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Caption = 'Relaci'#243'n:'
    end
    object zkcRelacion: TZetaKeyCombo
      Left = 60
      Top = 6
      Width = 250
      Height = 21
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  object dsAgregaEmp: TDataSource
    Left = 376
    Top = 57
  end
end
