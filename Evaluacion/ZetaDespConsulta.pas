unit ZetaDespConsulta;

interface

uses Forms, Controls, Sysutils, Dialogs,
     ZBaseConsulta;

type
  eFormaConsulta = ( efcSistGlobales,
                     efcReportes,
                     efcQueryGral,
                     efcSistProcesos,
                     efcCatCompeten,
                     efcCatPreguntas,
                     efcCatTCompeten,
                     efcCatEscalas,
                     efcCriteriosEval,
                     efcPregxCriterio,
                     efcDisenoEscalas,
                     efcPerfilEval,
                     efcEmpleadosEval,
                     efcEncuestas,
                     efcEscalasEncuesta,
                     efcTotalesEncuesta,
                     efcSujeto,
                     efcEvaluadores,
                     efcEvaluaciones,
                     efcAnalisisCriterio,
                     efcAnalisisPregunta,
                     efcFrecuenciaResp,
                     efcResultadosCriterio,
                     efcResultadosPregunta,
                     efcRegistrosDisenoEnc,
                     efcProcesosDisenoEnc,
                     efcCorreos,
                     efcReportaA,
                     efcDiccion );

  TPropConsulta = record
    IndexDerechos: Integer;
    ClaseForma: TBaseConsultaClass;
  end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;

implementation

uses FCriteriosEvaluar,
     FPreguntasCriterio,
     FEscalasEncuesta,
     FPerfilEvaluadores,
     FEmpleadosEvaluar_DevEx,
     FTotalesEncuesta_DevEx,
     FEmpEvaluados,
     FEvaluadores_DevEx,
     FEvaluaciones,
     FAnalisisCriterio_DevEx,
     FAnalisisPregunta_DevEx,
     FFrecuenciaResp_DevEx,
     FResultadosCriterio,
     FResultadosPregunta,
     FEncuestas,
     FCompetencias,
     FCatTCompetencias,
     FCatPreguntas,
     FCatEscalas,
     FQueryGral_DevEx,
     FReportes_DevEx,
     FSistProcesos_DevEx,
     FGlobalesEvaluacion,
     FRegistroDisenoEnc,
     FProcesoDisenoEnc,
     FHistCorreos,
     FCatUsuarios,
     FDiccion_DevEx,
     ZAccesosMgr,
     ZAccesosTress;

function Consulta( const iDerechos: Integer; const Forma: TBaseConsultaClass ): TPropConsulta;
begin
     with Result do
     begin
          IndexDerechos := iDerechos;
          ClaseForma := Forma;
     end;
end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;
begin
     case Forma of
          efcCatCompeten              : Result := Consulta( D_CAT_EVAL_COMPETEN, TCompetencias );
          efcCatPreguntas             : Result := Consulta( D_CAT_EVAL_PREGUNTAS, TCatPreguntas );
          efcCatTCompeten             : Result := Consulta( D_CAT_EVAL_TCOMPETEN, TTipoCompetencias );
          efcCatEscalas               : Result := Consulta( D_CAT_EVAL_ESCALAS, TCatEscalas );
          efcReportes                 : Result := Consulta( D_REPORTES_EVALUACION, TReportes_DevEx );
          efcQueryGral                : Result := Consulta( D_EVAL_CONSULTAS, TQueryGral_DevEx );
          efcSistProcesos             : Result := Consulta( D_EVAL_PROCESOS, TSistProcesos_DevEx );
          efcSistGlobales             : Result := Consulta( D_GLOBALES_EVALUACION, TGlobalesEvaluacion );
          efcCriteriosEval            : Result := Consulta( D_CRITERIOS_EVAL, TCriteriosEvaluar );
          efcPregxCriterio            : Result := Consulta( D_PREG_CRITERIO, TPreguntasCriterio );
          efcEscalasEncuesta          : Result := Consulta( D_ESCALAS, TEscalasEncuesta );
          efcPerfilEval               : Result := Consulta( D_PERFIL_EVAL, TPerfilEvaluadores );
          efcEmpleadosEval            : Result := Consulta( D_EMPLEADOS_EVAL, TEmpleadosEvaluar_DevEx );
          efcEncuestas                : Result := Consulta( D_ENCUESTAS, TEncuestas );
          efcTotalesEncuesta          : Result := Consulta( D_TOTALES_ENCUESTA, TTotalesEncuesta_DevEx );
          efcSujeto                   : Result := Consulta( D_EMPLEADOS_EVALUADOS, TEmpEvaluados );
          efcEvaluadores              : Result := Consulta( D_EVALUADORES, TEvaluadores_DevEx );
          efcEvaluaciones             : Result := Consulta( D_EVALUACIONES, TEvaluaciones );
          efcAnalisisCriterio         : Result := Consulta( D_ANALISIS_CRITERIO, TAnalisisCriterio_DevEx );
          efcAnalisisPregunta         : Result := Consulta( D_ANALISIS_PREGUNTA, TAnalisisPregunta_DevEx );
          efcFrecuenciaResp           : Result := Consulta( D_FRECUENCIA_RESP, TFrecuenciaResp_DevEx );
          efcResultadosCriterio       : Result := Consulta( D_RESULTADO_CRITERIO, TResutadosCriterio );
          efcResultadosPregunta       : Result := Consulta( D_RESULTADO_PREGUNTA, TResultadosPregunta );
          efcRegistrosDisenoEnc       : Result := Consulta( D_DISENO_REG, TRegistroDisenoEnc );
          efcProcesosDisenoEnc        : Result := Consulta( D_DISENO_PROC, TProcesoDisenoEnc );
          efcCorreos                  : Result := Consulta( D_EVAL_CORREOS, THistCorreos );
          efcReportaA                 : Result := Consulta( D_EVAL_REPORTA, TCatUsuarios );
          efcDiccion                  : Result := Consulta( D_EVAL_DICCIONARIO, TDiccion_DevEx );
     else
         Result := Consulta( 0, nil );
     end;
end;

end.
