unit FGlobalesEvaluacion;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, ImgList, ComCtrls, ToolWin, ZetaDBTextBox,
  ZBaseGlobal_DevEx, StdCtrls, dxBar, cxClasses, cxGraphics;

type
  TGlobalesEvaluacion = class(TBaseConsulta)
    PanelTitulos: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    RazonLbl: TZetaDBTextBox;
    RfcLbl: TZetaDBTextBox;
    InfonavitLbl: TZetaDBTextBox;
    ImageList2: TImageList;
    PanelIdentifica2: TPanel;
    ValorActivo3: TPanel;
    DataSource2: TDataSource;
    cxImageList1: TcxImageList;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    btnCorreos_DevEx: TdxBarLargeButton;
    dxBarDockControl1: TdxBarDockControl;
    procedure FormCreate(Sender: TObject);
    procedure btnCorreosClick(Sender: TObject);

  private
    { Private declarations }
    function AbrirGlobal( GlobalClass: TBaseGlobalClass_DevEx ): Integer;
  protected
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
  end;

var
  GlobalesEvaluacion: TGlobalesEvaluacion;

implementation

uses DGlobal,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaDialogo,
     ZetaCommonClasses,
     ZGlobalTress,
     FGlobalCorreosEdit,
     ZHelpContext;

{$R *.DFM}

procedure TGlobalesEvaluacion.FormCreate(Sender: TObject);
begin
     inherited;
     EsConsulta := False;
     HelpContext := H_SISTEMA_GLOBALES_EMPRESA;
end;

procedure TGlobalesEvaluacion.Connect;
begin
     with Global do
     begin
          RFCLbl.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_RFC_EMPRESA );
          InfonavitLbl.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_INFONAVIT_EMPRESA );
          RazonLbl.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_RAZON_EMPRESA );
     end;
end;

procedure TGlobalesEvaluacion.Refresh;
begin
end;

function TGlobalesEvaluacion.AbrirGlobal( GlobalClass: TBaseGlobalClass_DevEx ): Integer;
var
   Forma: TBaseGlobal_DevEx;
begin
     Forma := GlobalClass.Create( Application );
     with Forma do
     begin
          try
             ShowModal;
             Result := LastAction;
          finally
                 Free;
          end;
     end;
end;

procedure TGlobalesEvaluacion.btnCorreosClick(Sender: TObject);
begin
     inherited;
     if ZAccesosMgr.CheckDerecho(  D_GLOBALES_EVALUACION, K_DERECHO_CAMBIO ) then
         AbrirGlobal( TGlobalCorreosEdit )
     else
         ZetaDialogo.ZInformation('Operaci�n no v�lida', 'No se tiene permiso para modificar globales', 0 );
end;

end.
