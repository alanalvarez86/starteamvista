inherited WizAgregaEmpEvaluar: TWizAgregaEmpEvaluar
  Left = 458
  Top = 325
  Caption = 'Agregar empleados a evaluar'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    BeforeMove = WizardBeforeMove
  end
  inherited PageControl: TPageControl
    ActivePage = Parametros
    inherited Parametros: TTabSheet
      object Label1: TLabel
        Left = 7
        Top = 48
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = 'Encuesta:'
      end
      object ztbEncuesta: TZetaTextBox
        Left = 59
        Top = 46
        Width = 353
        Height = 17
        AutoSize = False
        Caption = 'ztbEncuesta'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Label2: TLabel
        Left = 4
        Top = 118
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Evaluador:'
      end
      object Label3: TLabel
        Left = 10
        Top = 199
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Relaci�n:'
      end
      object US_CODIGO: TZetaKeyLookup
        Left = 59
        Top = 114
        Width = 355
        Height = 21
        TabOrder = 0
        TabStop = True
        WidthLlave = 70
      end
      object zkcRelacion: TZetaKeyCombo
        Left = 59
        Top = 195
        Width = 250
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
      end
    end
    inherited FiltrosCondiciones: TTabSheet
      inherited FiltrosGB: TGroupBox
        inherited Seleccionar: TBitBtn
          Visible = True
        end
      end
    end
    inherited Ejecucion: TTabSheet
      inherited Advertencia: TMemo
        Lines.Strings = (
          'Se agregar�n empleados a evaluar de acuerdo al evaluador, '
          'la encuesta y la relaci�n seleccionada.'
          ''
          'Presione el bot�n '#39'Ejecutar'#39' para iniciar el proceso.')
      end
      inherited ProgressPanel: TPanel
        inherited ProgressBar: TProgressBar
          Left = 40
        end
      end
    end
  end
end
