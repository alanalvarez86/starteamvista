inherited WizAsignaRelaciones_DevEx: TWizAsignaRelaciones_DevEx
  Left = 438
  Top = 331
  Caption = 'Asignaci'#243'n de relaciones'
  ClientHeight = 472
  ClientWidth = 464
  ExplicitWidth = 470
  ExplicitHeight = 501
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 464
    Height = 472
    Header.AssignedValues = [wchvGlyph, wchvTitleFont]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EC400000EC401952B
      0E1B00000557494441545847AD965D685C4514C7EFAE8D2D26B6B6A2F6A168A1
      7DA87D691F839F955850A8316A9A74BF62A4501115EA93606D6F9362BAD9BBBB
      F7EE57524B63637CD067D107A550D0071F8388143F40A882F6C1A752444AEFF8
      3B73E7DEECDEBD6962D2813F33BB73E7FCFF73CE993363F9B59CA56AB98DBE97
      DBA366F2B63F333EEABB23C77C67D0F25BCCD5B3967233413F3766A9F9714B5D
      7C3582DFCA0736945A1364710A0147553DF703BDA2175CF55B856A44DE844008
      E79788C3DF7EB3808D7CA2F1D54004EC84F086218E634ACD401E276E13A01AA3
      965F5F9F07DE4D200E50CBFDADCEE3F60543D6258070CC8E121E4410060D4F42
      924514DE6B206C05C8C7C544728107AAD9ADEA4389FD3202A49F09BCA021028C
      8824C23864D1CB90FDDB450E98FB4BD50FA795184B48C04004F848F2C008883C
      21BD88B8BD1059B4197C9520E02792EB39BF898BBD114E0A863E4E1020FFCD91
      8CA1800E4F40B28227CC82ECC31016185F03D7897D0DF52FAA5AFE717E6F544D
      048831081DDB79CB9970BE2B4F388B8E3DFDBE6DDB1B542D63C55B940F9127BA
      C90542AE3F60410A6C83701F0B7B949BDB84900155CF4FAB064771B66055261D
      0762D589D2A7DA50ACE98D85F9E02180DF7172BD4E2B4D80DE713DBB8B858B88
      18ABD895DD10DEEC16E028BC72C0F0464D0B10883D4F36A74FC65D90A685D817
      30DF452CD00528401A3C431C17BDD3D3EF25916B4C96670D6FD4B4AD40C4FD78
      612F7D1D11C390F733DE05C4E3DD02DAC843A4943BBAA13AE15C4C24D7285D36
      BC5133F624A49FE1C52BF461727FCFF80BE6764AF8572217B73197B16497C9E4
      8EAA4F16BF36BC51C35E1A50CE73370D7107B0BB08EE5D965C13EB713E257DD9
      2E8D27910B6A674A5386376AACD99144DC0E78B9440C6112B91650CBA6FDF363
      698EDB2667A2742541C09F45BBB8C3F046CD6F640E2591B68350BC131052AD42
      2FB4936BEFD0AB663E257325BBB4BD5D04E35FCA76798F5F2F70A4F2F7F16DDA
      6F14F6AB46A187B54F937089C421F8E654442257AA3EB3865C7999602E161E11
      01F9EFE03719FB6EEE1E551A7A9B356798BF4AFF89F20ABB5539DB0BC9CF71D2
      0E78997D183544C60B11A97B441790E837E452F54C2E483DF80701C7BF9C3ABE
      59B9239FB3F30F943736805B73544E4ECE88AC3B92485CCFDD626E8ED395C675
      4B3BD52274E5C2A552BD5C6E3923826233AC5DDE197F9D0322E49BB36F6C35E1
      D74D350F8BE81EBC7188F5DFEAC74E2D2BE43FF2FB047848EC23405C4BA9954A
      250242113A2419AB6217FB21598C91264184E40D7F60533652958BAAB09D2B7D
      2FB6DF44D40390F7CAA68C00064684FC1994C84084D9F5F504B26521DED0025C
      AEEF0A37A5C78D7901706DFBE78253A7793A05C44564D9F9F2B57F25C8DDA0AA
      AF610F62AEEAF031B382002322B819ADCA6D4BEF4A285D562D04C87BA1ED25B5
      0A014644ECE14866F7B68D71EB110C61ACCA3B61F2594BD94FE9B57E0B920AC9
      E70E05C46DE4FF430093F24A6A64FBF828AD89DD8CDC5CFBC9E0076551041152
      7EC552A70F48B10AD6B748BC05B3FB185613821E12F02002CED197F9E820E843
      C036AED26308E803723B2E09108F94D8B1106B01905C2001572D60C9B5F25818
      C2E01F202C18D7A80303E06E6E44A96C01A15913413CE0E2FA861030BF46015B
      C0BC21D6E023909DC61B69399ADA789280534F920B0384012FAC43402F715F60
      179100700B149923343A37BA05C8EE4F22E0C463EB16C0F92FBC84AB7FC59090
      DFA07C5EF21BF9477D7913E05EFA4044482E6389BF0838F9C41D10501FDBA2A6
      07FB31F43A97C951557AE111B23CA5AA72B4E45EE082E27524E30895E13B2AC0
      52679F97DDA6557130A5E32A0421E4C80924FB4301F27BCD0246ADFF0003FF83
      DCE49A318D0000000049454E44AE426082}
    ExplicitWidth = 464
    inherited Parametros: TdxWizardControlPage
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'Proceso que genera las relaciones para los responsables de la en' +
        'cuesta.'
      Header.Title = 'Asignaci'#243'n de relaciones.'
      ParentFont = False
      ExplicitWidth = 442
      ExplicitHeight = 332
      object GroupBox1: TcxGroupBox
        Left = 27
        Top = 30
        Caption = ' Datos de Encuesta: '
        TabOrder = 1
        Height = 119
        Width = 388
        object Label1: TLabel
          Left = 16
          Top = 28
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object Label2: TLabel
          Left = 16
          Top = 66
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
        end
        object ET_CODIGO: TZetaTextBox
          Left = 60
          Top = 26
          Width = 65
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ET_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object ET_NOMBRE: TZetaTextBox
          Left = 60
          Top = 64
          Width = 320
          Height = 17
          AutoSize = False
          Caption = 'ET_NOMBRE'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
      end
      object gbRelaciones: TcxGroupBox
        Left = 27
        Top = 163
        Caption = ' Elija las relaciones: '
        TabOrder = 0
        Height = 148
        Width = 388
        object cblRelaciones: TcxCheckListBox
          Left = 3
          Top = 15
          Width = 382
          Height = 123
          Align = alClient
          Items = <>
          ParentFont = False
          TabOrder = 0
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      ParentFont = False
      ExplicitWidth = 442
      ExplicitHeight = 332
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 442
        Height = 237
        Width = 442
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 442
        Width = 442
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          ExplicitWidth = 374
          Width = 374
          AnchorY = 57
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      ParentFont = False
      ExplicitWidth = 442
      ExplicitHeight = 332
      inherited sCondicionLBl: TLabel
        Left = 15
        ExplicitLeft = 15
      end
      inherited sFiltroLBL: TLabel
        Left = 38
        Top = 184
        ExplicitLeft = 38
        ExplicitTop = 184
      end
      inherited Seleccionar: TcxButton
        Left = 143
        Visible = True
        ExplicitLeft = 143
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 67
        ExplicitLeft = 67
      end
      inherited sFiltro: TcxMemo
        Left = 67
        Style.IsFontAssigned = True
        ExplicitLeft = 67
      end
      inherited GBRango: TGroupBox
        Left = 67
        ParentFont = False
        ExplicitLeft = 67
      end
      inherited bAjusteISR: TcxButton
        Left = 381
        ExplicitLeft = 381
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 24
    Top = 354
  end
end
