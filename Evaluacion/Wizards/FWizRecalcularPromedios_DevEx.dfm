inherited WizRecalcularPromedios_DevEx: TWizRecalcularPromedios_DevEx
  Left = 352
  Top = 301
  Caption = 'Recalcular promedios'
  ClientHeight = 297
  ClientWidth = 479
  ExplicitWidth = 485
  ExplicitHeight = 326
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 479
    Height = 297
    Header.AssignedValues = [wchvDescriptionFont, wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EC400000EC401952B
      0E1B00000411494441545847AD573D68535114EE2F4A6945C1C1A188420727A9
      E0D0A183A083430707070507410707050707A1496E938AD4BC84162A3874E8A0
      D2BC17A5481105C50C0A82454AB1E6DDD8A1430705870E1D3A7488E7BBEFDCEB
      7B793F498C1F1C12CE3DF79C73CF3D3FF775B50BE16C0C8AB27B2E55AA5E4997
      E45D50AA242F09A7368A3516FBBF108B5B07D3B67B2BE3C87769C7DDA3DF7A02
      5552B6BC863DBCBD33A41CF73A19DF8E309448695BEE64CA725A542A7DACAA3D
      50388F91A24A58B1FB91E81EAE0057A1AEC373722A4E5EBCFC31CC6A5B03293D
      EB3F359D663F63CBF956144106CED09E5DB3DF717F0B478EB3483284BD31A2C2
      6736CBCFE0F172CB104BEE09DA6F220287C40B798A97A3812C2683DFF4A64CB9
      B610778796B04E5B596BA190B54A9678342E84E0952048C7ACCF894DF15C1E05
      1F7A274BDF2F28210D0AF3A231EEC815668750CC591364B81EA4FC1D5E0E8192
      71C9384111CD38EE534484AECA65119CBE364ACC7D25044F636ABA288AFD6470
      2FEC8055A7A81C66310351AA8EA11AB4EE067AC26238BDFB462FA0B1303B8482
      C88F451957942B5C653105AE8C46A38650494A1099AB99E4E99A62C6A098B56E
      441A57949F65318324274C55A5EDEA6DCD24AF6E2A660C70CA68E38AA6592C00
      B46BBF6110E5C2162F53F829E11493EE4967691CE80A46220C7B942B9C67B110
      424E50C2F3122220373D077C591903949B95B3DE4638B03E23667A592C1229DB
      BDAC9311DD93D9CA01DDB52ACC8A0579DEFDE0F1AB3E2B9B7FAD8D533FF8F070
      CE19A06CEF66B1586827024D49D53F51B3FBA74D7DE4640FFEC30932FE85E8D3
      D4B3AF87887F805A6EBF126C82495B5EE4BF7F813EC07F6391B16B4364A817D7
      C0B9807EB09317F933E0D15A3775BEC428607879B3C29D12CB5B5EDFF0E6BDDC
      45781423062473249F9B3B4EE17F8FD037D03A1CA1EC4E7400ED9D1C55898889
      AB98985460E06EE29C288A99C142B66047186E24387292B785407654C5819845
      2773AAF735134EA0647849815AEC309DFA5784B1584234787B00147A35E60315
      E76FC3C6117622A9F737A3C6D9A023CDE4CD018C45DCBF6FC150BAE4A6925B6F
      330AB666AAB479ADDB8C6264BFDF6888689E2B411F684F0F291BA230F6D5EB75
      DC6B0F65F7002F47821F3A3C6DDD6DF3CE88EAD38A6C5946B73299DA21324E6D
      59EB0E7441BD40DEAD81FC0EB048C7C0556ABD646395D91ED41B9EC7221A03B2
      D3381111FE7681EEFA37F4ADBC0B498004CDA39468A595D77023D0DCFC4D0794
      5AAA4EF07232502E7E27F01F6F86B8679A1F48AEC68F1944A0D99C0901CF6ADA
      B8AA952845F82CA3072694A18C90D990C3BB0FCF389C18DF00813DB6FC897556
      DB1E701AE4410BDF8321C2A9B1D70C9C4E805712CA35F0CD10432AF430FC0F1F
      332D018A914CFC8EB4D0DD50665ECF683ED283E8EAFA033A765625FC05B58900
      00000049454E44AE426082}
    ExplicitTop = 8
    ExplicitWidth = 435
    ExplicitHeight = 398
    inherited Parametros: TdxWizardControlPage
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'Proceso para el realizar nuevamente el c'#225'lculo de los resultados' +
        ' obtenidos de la encuesta seleccionada.'
      Header.Title = 'Recalculo de resultados.'
      ParentFont = False
      ExplicitWidth = 457
      ExplicitHeight = 157
      object GroupBox1: TcxGroupBox
        Left = 11
        Top = 20
        Caption = ' Datos de Encuesta: '
        TabOrder = 0
        Height = 105
        Width = 426
        object Label1: TLabel
          Left = 16
          Top = 28
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object Label2: TLabel
          Left = 16
          Top = 66
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
        end
        object ET_CODIGO: TZetaTextBox
          Left = 60
          Top = 26
          Width = 65
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ET_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object ET_NOMBRE: TZetaTextBox
          Left = 60
          Top = 64
          Width = 357
          Height = 17
          AutoSize = False
          Caption = 'ET_NOMBRE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      ExplicitWidth = 457
      ExplicitHeight = 157
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 413
        Height = 62
        Width = 457
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 413
        Width = 457
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          ExplicitWidth = 345
          Width = 389
          AnchorY = 57
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
  end
end
