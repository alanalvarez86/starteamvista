inherited WizAgregaEmpEvaluar_DevEx: TWizAgregaEmpEvaluar_DevEx
  Left = 458
  Top = 325
  Caption = 'Agregar empleados a evaluar'
  ClientHeight = 427
  ClientWidth = 458
  ExplicitWidth = 464
  ExplicitHeight = 456
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 458
    Height = 427
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F40000000467414D410000B18F0BFC6105000000097048597300000EC300000E
      C301C76FA86400000263494441545847ED943F68144114C6D71841D1229D8D56
      5A8420A25D0A219616723B174C974210C4420BB151737B63276291EC6E2CC422
      A070EEAEC255560A87D8A9B07F2EDCAE368A8D82450A0B8B2BD6F766DF79337B
      7BC4E4F62484FCE0E3F666DEBCEFCDBE99D5F6D891709E4EF0A63F3576AD7D39
      48962A8697DCAE7B493A6ED5DCF83759AAD4DD78B968C13844962ABBA60078C5
      8F6EBADF0E5D7DFCF1C0B0B692A54A5905706FFD08A5D416BC743FF63C1F43D3
      2A3BAA0058F486BBEB33A093856A24A76B5EFC494E2AAD1DBD0535277E5A682C
      A9E676DECB49B722B25429A305FC65C2F0B5534AF171BBEB7466F36DA0699591
      0B7092054A35001621C7D2B00A568FBDC700C34D7ED4BDF846DD4DAE14CA896F
      E577056D99A15403DC694447E5581A1E429AEEE3ADD6243EE2EF30195EDC9493
      1A6EFC4A7EFD7F817C7830E5589AD99C5AE3F35979E166329CE42BB662E9797C
      1EC55F742E8279988FA3F41966583D6686954B76A8DFB37CB668B6D9099A126C
      B5887F9148CC5B7393686A47AC0B4A1505BAFD30583C2C020138A00F8A126D57
      222918AD0D18F7D5B503F65A04024BCDB7C78B126D579A19542F1498666A57EF
      AF04D55366A07F8722AE61019C6B13861BCDF57A3B8AC46DB1DB956768660695
      5F66C03E9079D70AF5CB686845F3D358801555DEE1FFD2811EC768BE12B073CB
      BE3E6586AC85CF38D733EF152816948D15323FDBE1FC340D096473D2064D95CB
      6AC49E643BEC1751600EB7A17F104B05EEFE2C1888EB9715A1B301739C83EF03
      2D291F3CED794345705029747CE00EAD88FDCC996FC0D8750AF93F887B0FC5AC
      FAFA19FC42D2F01EBB154DFB03ADAD9759190EB28F0000000049454E44AE4260
      82}
    ExplicitWidth = 458
    ExplicitHeight = 427
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso para asignar empleados a evaluar tomando en cuenta el ev' +
        'aluador y su relaci'#243'n.'
      Header.Title = 'Informaci'#243'n de empleados a evaluar'
      ExplicitWidth = 436
      ExplicitHeight = 285
      object Label1: TLabel
        Left = 18
        Top = 93
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = 'Encuesta:'
      end
      object ztbEncuesta: TZetaTextBox
        Left = 70
        Top = 91
        Width = 353
        Height = 17
        AutoSize = False
        Caption = 'ztbEncuesta'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Label2: TLabel
        Left = 15
        Top = 139
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Evaluador:'
      end
      object Label3: TLabel
        Left = 19
        Top = 184
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Relaci'#243'n:'
      end
      object US_CODIGO: TZetaKeyLookup_DevEx
        Left = 70
        Top = 136
        Width = 355
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 70
      end
      object zkcRelacion: TZetaKeyCombo
        Left = 70
        Top = 181
        Width = 250
        Height = 21
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 1
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      ExplicitWidth = 436
      ExplicitHeight = 285
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 436
        ExplicitHeight = 190
        Height = 190
        Width = 436
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 436
        Width = 436
        inherited Advertencia: TcxLabel
          Caption = 
            'Al aplicar el proceso se asignar'#225' la evaluaci'#243'n a los empleados ' +
            'de acuerdo con los par'#225'metros especificados'
          Style.IsFontAssigned = True
          ExplicitLeft = 70
          ExplicitWidth = 364
          Width = 364
          AnchorY = 49
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      ExplicitWidth = 436
      ExplicitHeight = 285
      inherited sCondicionLBl: TLabel
        Left = 8
        Top = 137
        ExplicitLeft = 8
        ExplicitTop = 137
      end
      inherited sFiltroLBL: TLabel
        Left = 33
        Top = 165
        ExplicitLeft = 33
        ExplicitTop = 165
      end
      inherited Seleccionar: TcxButton
        Left = 134
        Top = 253
        Visible = True
        ExplicitLeft = 134
        ExplicitTop = 253
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 62
        Top = 133
        ExplicitLeft = 62
        ExplicitTop = 133
      end
      inherited sFiltro: TcxMemo
        Left = 62
        Top = 164
        Style.IsFontAssigned = True
        ExplicitLeft = 62
        ExplicitTop = 164
      end
      inherited GBRango: TGroupBox
        Left = 62
        Top = 3
        ExplicitLeft = 62
        ExplicitTop = 3
      end
      inherited bAjusteISR: TcxButton
        Left = 379
        Top = 164
        ExplicitLeft = 379
        ExplicitTop = 164
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 64
    Top = 442
  end
end
