unit FCopiaEncuesta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, StdCtrls, Buttons, ExtCtrls, ZetaDBTextBox, ZBaseDlgModal,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, Vcl.ImgList,
  cxButtons, cxControls, cxContainer, cxEdit, cxGroupBox;

type
  TCopiaEncuesta = class(TZetaDlgModal_DevEx)
    GroupBox1: TcxGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    ztbNombre: TZetaTextBox;
    ET_NOMBRE: TEdit;
    cbEmpEvaluar: TCheckBox;
    gbStatusSujeto: TcxGroupBox;
    cbEvaluadores: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure cbEmpEvaluarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CopiaEncuesta: TCopiaEncuesta;

implementation

{$R *.DFM}
uses DEvaluacion, FTressShell;

procedure TCopiaEncuesta.FormShow(Sender: TObject);
var
   sNombre: String;
begin
     inherited;
     ET_NOMBRE.SetFocus;
     with dmEvaluacion.cdsEncuestas do
     begin
          Conectar;
          ztbNombre.Caption := FieldByName('ET_CODIGO').AsString + ' = ' + FieldByName('ET_NOMBRE').AsString;
          sNombre := 'Copia de ' + FieldByName('ET_NOMBRE').AsString;
          ET_NOMBRE.Text := Copy( sNombre, 1, ET_NOMBRE.MaxLength );
     end;
end;

procedure TCopiaEncuesta.OKClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             inherited;
             dmEvaluacion.CopiaEncuesta( ET_NOMBRE.Text, cbEmpEvaluar.Checked , cbEvaluadores.Checked );
             TressShell.SetUltimaEncuesta;
          finally
                 Cursor := oCursor;
          end;
     end;
     Close;
end;

procedure TCopiaEncuesta.cbEmpEvaluarClick(Sender: TObject);
begin
     inherited;
     cbEvaluadores.Enabled := cbEmpEvaluar.Checked;
end;

end.
