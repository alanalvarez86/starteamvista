unit FWizCorreoInvitacion_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ZetaKeyLookup, ZetaEdit, StdCtrls, Buttons,
  ZetaWizard, ExtCtrls, ZetaDBTextBox, ZetaKeyCombo, ZcxBaseWizardFiltro,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer,
  cxEdit, Vcl.Menus, ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo,
  ZetaKeyLookup_DevEx, cxButtons, dxGDIPlusClasses, cxImage, cxLabel,
  cxGroupBox, dxCustomWizardControl, dxWizardControl;

type
  eTipoCorreo = ( tpPlantilla, tpTexto );
  eFormatoCorreo = ( fcTexto, fcHTML );
  
  TWizCorreosInvitacion_DevEx = class(TBaseCXWizardFiltro)
    OpenDialog1: TOpenDialog;
    OpenDialog2: TOpenDialog;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    ET_CODIGO: TZetaTextBox;
    ET_NOMBRE: TZetaTextBox;
    lblRelacion: TLabel;
    ENC_RELA: TZetaKeyCombo;
    gbEvaluadores: TGroupBox;
    lblLista: TLabel;
    tsTipoTexto: TdxWizardControlPage;
    tsTipoPlantilla: TdxWizardControlPage;
    tsTipoCorreo: TdxWizardControlPage;
    rgEstiloCorreo: TcxRadioGroup;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    ET_CODIGO2: TZetaTextBox;
    ET_NOMBRE2: TZetaTextBox;
    GroupBox3: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    ET_CODIGO3: TZetaTextBox;
    ET_NOMBRE3: TZetaTextBox;
    lblTema: TLabel;
    edTema: TEdit;
    lblPlantilla: TLabel;
    edPlantilla: TEdit;
    btnDialogo: TcxButton;
    pnDatos: TPanel;
    lblTemaTexto: TLabel;
    edTemaTexto: TEdit;
    rgFormatoCorreo: TcxRadioGroup;
    gbMensaje: TGroupBox;
    Mensaje: TMemo;
    pnBoton: TPanel;
    btnArchivo: TcxButton;
    btnLista: TcxButton;
    edLista: TZetaEdit;
    procedure FormShow(Sender: TObject);
    procedure BListaClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BInicialClick(Sender: TObject);
    procedure BFinalClick(Sender: TObject);
    procedure btnListaClick(Sender: TObject);
    procedure rgEstiloCorreoClick(Sender: TObject);
    procedure btnDialogoClick(Sender: TObject);
    procedure btnArchivoClick(Sender: TObject);
  private
    { Private declarations }
    lChecaLista: Boolean;
    TipoDeCorreo: eTipoCorreo;
    FEvaluadoCodigo: String;
    procedure CargaDescripcionFinal;
    function BuscaEvaluado( const lConcatena: Boolean; const sLlave: String ): String;
    function BuscaEvaluador( const lConcatena: Boolean; const sLlave: String ): String;
    function ValidaSettingsTipoPlantilla: String;
    function ValidaSettingsTipoTexto: String;
  protected
    procedure CargaParametros; override;
    procedure CargaListaVerificacion; override;
    function Verificar: Boolean; override;
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizCorreosInvitacion_DevEx: TWizCorreosInvitacion_DevEx;

implementation

uses DCliente,
     DSistema,
     DProcesos,
     DEvaluacion,
     ZHelpContext,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZBaseSelectGrid_DevEx,
     FCorreosInvitacionGridSelect_DevEx;

{$R *.DFM}
procedure TWizCorreosInvitacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FEvaluadoCodigo := 'SUJETO.CB_CODIGO';
     HelpContext := H_DISENO_ENCUESTA_PROCESOS_ENVIA_CORREOS_INVITACION;
end;

procedure TWizCorreosInvitacion_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     lChecaLista := False;
     with dmCliente do
     begin
          ET_CODIGO.Caption := InttoStr( Encuesta );
          ET_NOMBRE.Caption := Nombre;
          ET_CODIGO2.Caption := ET_CODIGO.Caption;
          ET_NOMBRE2.Caption := ET_NOMBRE.Caption;
          ET_CODIGO3.Caption := ET_CODIGO.Caption;
          ET_NOMBRE3.Caption := ET_NOMBRE.Caption;
     end;
     with ENC_RELA do
     begin
          dmEvaluacion.LlenaPerfilEvaluadores( Lista, True );
          ItemIndex := 0;
     end;
     with dmEvaluacion do
     begin
          cdsEvaluadosLookup.Refrescar;
          cdsEvaluadoresLookup.Refrescar;
     end;
     rgEstiloCorreo.ItemIndex := Ord( tpPlantilla );
     edTema.Text := ET_NOMBRE.Caption;
     edTemaTexto.Text := ET_NOMBRE.Caption;

     WizardControl.ActivePage := Parametros;
     ActiveControl := ENC_RELA;
end;

procedure TWizCorreosInvitacion_DevEx.BListaClick(Sender: TObject);
begin
     with ELista do
     begin
          Text := BuscaEvaluado( True, Text );
     end;
end;

function TWizCorreosInvitacion_DevEx.BuscaEvaluado( const lConcatena: Boolean; const sLlave: String ): String;
var
   sKey, sDescripcion: String;
begin
     with dmEvaluacion.cdsEvaluadosLookup do
     begin
          if( Search( '', sKey, sDescripcion ) )then
          begin
               if lConcatena and ZetaCommonTools.StrLleno( Text ) then
               begin
                    if( strLleno( sLlave ) )then
                        Result := sLlave + ',' + sKey
                    else
                        Result := sKey;
               end
               else
                   Result := sKey;
          end;
     end;
end;

procedure TWizCorreosInvitacion_DevEx.WizardAfterMove(Sender: TObject);
begin
     inherited;
     if (WizardControl.IsDestroying = False ) then
        begin
            if ( WizardControl.ActivePage = Parametros ) then
                ActiveControl := ENC_RELA
            else if ( WizardControl.ActivePage = tsTipoCorreo ) then
                ActiveControl := rgEstiloCorreo
            else if ( WizardControl.ActivePage = tsTipoTexto ) then
                ActiveControl := edTemaTexto
            else if ( WizardControl.ActivePage = tsTipoPlantilla ) then
                ActiveControl := edTema
     end;
end;


procedure TWizCorreosInvitacion_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   sMensaje: String;
begin
     inherited;
     if Wizard.Adelante then
     begin
          if ( WizardControl.ActivePage = Parametros ) then
          begin
               dmEvaluacion.cdsPerfilEvaluador.Conectar;
               if( dmEvaluacion.cdsPerfilEvaluador.RecordCount <= 0 )then
                  CanMove := Error( '� No se tienen registrados perfiles de evaluadores !', Parametros );
          end
          else if( WizardControl.ActivePage = tsTipoCorreo )then
               begin
               {
                    tsTipoPlantilla.Enabled := ( rgEstiloCorreo.ItemIndex = Ord( tpPlantilla ) );
                    tsTipoTexto.Enabled := not tsTipoPlantilla.Enabled;
               }
                 if( rgEstiloCorreo.ItemIndex = 0) then
                 begin
                      tsTipoTexto.PageVisible:=FALSE;
                      tsTipoPlantilla.PageVisible:= TRUE;
                 end
                 else
                 begin
                      tsTipoPlantilla.PageVisible := FALSE;
                      tsTipoTexto.PageVisible := TRUE;
                 end;
               end
          else if( WizardControl.ActivePage = tsTipoPlantilla )then
               begin
                    sMensaje := ValidaSettingsTipoPlantilla;
                    if( strLleno( sMensaje ) )then
                        CanMove := Error( sMensaje, tsTipoPlantilla )
                    else
                        CargaDescripcionFinal;
               end
          else if( WizardControl.ActivePage = tsTipoTexto )then
               begin
                    sMensaje := ValidaSettingsTipoTexto;
                    if( strLleno( sMensaje ) )then
                        CanMove := Error( sMensaje, tsTipoTexto )
                    else
                        CargaDescripcionFinal;
               end;
     end;
end;

procedure TWizCorreosInvitacion_DevEx.CargaDescripcionFinal;
var
    oCursor: TCursor;
begin
     if not( lChecaLista )then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             CargaParametros;
             dmProcesos.CorreosInvitacionGetLista( ParameterList );
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
     //Advertencia.Clear;
     //Advertencia.Caption := Format( 'Advertencia' + CR_LF + 'Se enviar�n %d correos de invitaci�n para los evaluadores seleccionados en la encuesta activa ' + CR_LF + 'Presione el bot�n Ejecutar para iniciar el proceso ', [ dmProcesos.cdsDataset.RecordCount ] );
end;

function TWizCorreosInvitacion_DevEx.ValidaSettingsTipoTexto: String;
begin
     Result := VACIO;
     if( strVacio( edTemaTexto.Text ) )then
         Result := 'El tema no puede quedar vac�o'
     else
         if( strVacio( Mensaje.Text ) )then
             Result := 'Es necesario que el mensaje tenga contenido';
end;

function TWizCorreosInvitacion_DevEx.ValidaSettingsTipoPlantilla: String;
begin
     Result := VACIO;
     if( strVacio( edTema.Text ) )then
         Result := 'El tema no puede quedar vac�o'
     else
         if( strVacio( edPlantilla.Text ) )then
             Result := 'Es necesario seleccionar la plantilla a utilizar'
         else
             if( not FileExists( edPlantilla.Text ) )then
                 Result := 'Archivo de plantilla no existe';
end;

procedure TWizCorreosInvitacion_DevEx.CargaParametros;
begin
     inherited CargaParametros;

     with Descripciones do
     begin
          AddString( 'Encuesta', ET_CODIGO.Caption + ' = ' + ET_NOMBRE.Caption );
          if( ENC_RELA.LlaveEntero > -1 )then
              AddString( 'Relacion', ENC_RELA.Descripcion )
          else
              AddString( 'Relacion', 'Todas' );
          if( strLleno(( edLista.Text )) )then
              AddString( 'Evaluadores', edLista.Text );
          if( rgEstiloCorreo.ItemIndex = Ord( tpPlantilla ) )then
              AddString( 'Tipo de Correo', 'Plantilla' )
          else
              AddString( 'Tipo de Correo', 'Texto fijo' );

          {Descripciones para correos tipo plantilla}
          AddString( 'Tema Plantilla', edTema.Text );
          AddString( 'Plantilla', edPlantilla.Text );

          {Descripciones para correos tipo texto}
          AddString( 'Tema Texto', edTemaTexto.Text );
          if( rgFormatoCorreo.ItemIndex = Ord( fcTexto ) )then
              AddString( 'Formato', 'Texto fijo' )
          else
              AddString( 'Formato', 'HTML' );
          AddMemo( 'Mensaje', Mensaje.Text );
     end;


     with ParameterList do
     begin
          AddInteger( 'Encuesta', dmCliente.Encuesta );
          AddInteger( 'Relacion', ENC_RELA.LlaveEntero );
          if( ENC_RELA.LlaveEntero > -1 )then
              AddString( 'RelacionDesc', ENC_RELA.Descripcion )
          else
              AddString( 'RelacionDesc', 'Todas' );
          AddString( 'Evaluadores', edLista.Text  );
          AddInteger( 'TipoCorreo', rgEstiloCorreo.ItemIndex );
          AddInteger( 'Responsable', dmCliente.Responsable );

          {Parametros para tipo de correo plantilla}
          AddString( 'TemaPlantilla', edTema.Text );
          AddString( 'Plantilla', edPlantilla.Text );

          {Parametros para tipo de correo texto fijo}
          AddString( 'TemaTexto', edTemaTexto.Text );
          if( rgEstiloCorreo.ItemIndex = Ord( tpPlantilla ) )then
              AddInteger( 'FormatoCorreo', Ord( fcHTML ) )
          else
              AddInteger( 'FormatoCorreo', rgFormatoCorreo.ItemIndex );
          AddMemo( 'Mensaje', Mensaje.Lines.Text );
     end;
end;

procedure TWizCorreosInvitacion_DevEx.CargaListaVerificacion;
begin
     dmProcesos.CorreosInvitacionGetLista( ParameterList );
end;

function TWizCorreosInvitacion_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CorreosInvitacion( ParameterList, Verificacion );
end;

procedure TWizCorreosInvitacion_DevEx.BInicialClick(Sender: TObject);
begin
     with EInicial do
     begin
          Text := BuscaEvaluado( False, Text );
     end;
end;

procedure TWizCorreosInvitacion_DevEx.BFinalClick(Sender: TObject);
begin
     with EFinal do
     begin
          Text := BuscaEvaluado( False, Text );
     end;
end;

procedure TWizCorreosInvitacion_DevEx.btnListaClick(Sender: TObject);
begin
     inherited;
     with edLista do
     begin
          Text := BuscaEvaluador( True, Text );
     end;   //@gbeltran
end;

function TWizCorreosInvitacion_DevEx.BuscaEvaluador( const lConcatena: Boolean; const sLlave: String ): String;
var
   sKey, sDescripcion: String;
begin
     with dmEvaluacion.cdsEvaluadoresLookup do
     begin
          Search( '', sKey, sDescripcion );
          {
          if( Search( '', sKey, sDescripcion ) )then
          begin
          }
               if lConcatena and ZetaCommonTools.StrLleno( Text ) then
               begin
                    if( strLleno( sLlave ) )then
                        Result := sLlave + ',' + sKey
                    else
                        Result := sKey;
               end
               else
                   Result := sKey;
          //end;
     end;
end;

function TWizCorreosInvitacion_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TCorreosInvitacionGridSelect_DevEx );
end;

procedure TWizCorreosInvitacion_DevEx.rgEstiloCorreoClick(Sender: TObject);
begin
     inherited;
     TipoDeCorreo := eTipoCorreo( rgEstiloCorreo.ItemIndex );
end;

procedure TWizCorreosInvitacion_DevEx.btnDialogoClick(Sender: TObject);
begin
     inherited;
     OpenDialog1.Title := 'Seleccionar plantilla';
     edPlantilla.Text := ZetaClientTools.AbreDialogo( OpenDialog1, edPlantilla.Text, 'xsl' );
end;

procedure TWizCorreosInvitacion_DevEx.btnArchivoClick(Sender: TObject);
var
   sFileName: String;
begin
     inherited;
     OpenDialog2.Title := 'Seleccionar archivo';
     sFileName := ZetaClientTools.AbreDialogo( OpenDialog2, sFileName, 'txt' );
     if( strLleno( sFileName ) )then
     begin
          Mensaje.Lines.LoadFromFile( sFileName );
     end;
end;

end.
