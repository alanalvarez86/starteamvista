inherited CorreosInvitacionGridSelect: TCorreosInvitacionGridSelect
  Width = 695
  Caption = 'Seleccionar evaluados'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 687
    inherited OK: TBitBtn
      Left = 530
    end
    inherited Cancelar: TBitBtn
      Left = 608
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 687
    Columns = <
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        PickList.Strings = ()
        Title.Caption = 'Evaluado'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRETTYNAME'
        PickList.Strings = ()
        Title.Caption = 'Nombre'
        Width = 250
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_CODIGO'
        PickList.Strings = ()
        Title.Caption = 'Evaluador'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_NOMBRE'
        PickList.Strings = ()
        Title.Caption = 'Nombre'
        Width = 250
        Visible = True
      end>
  end
  inherited PanelSuperior: TPanel
    Width = 687
    inherited PistaLBL: TLabel
      Left = 8
      Width = 81
      Caption = '&Busca Evaluado:'
    end
  end
end
