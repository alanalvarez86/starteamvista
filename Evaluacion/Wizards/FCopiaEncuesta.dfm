inherited CopiaEncuesta: TCopiaEncuesta
  Left = 424
  Top = 267
  Caption = 'Copiar encuesta'
  ClientHeight = 192
  ClientWidth = 481
  OldCreateOrder = True
  OnShow = FormShow
  ExplicitWidth = 487
  ExplicitHeight = 221
  PixelsPerInch = 96
  TextHeight = 13
  object gbStatusSujeto: TcxGroupBox [0]
    Left = 4
    Top = 102
    Caption = '                                                     '
    ParentFont = False
    TabOrder = 2
    Height = 49
    Width = 193
    object cbEvaluadores: TCheckBox
      Left = 34
      Top = 20
      Width = 119
      Height = 17
      Caption = 'Copiar evaluadores'
      Enabled = False
      TabOrder = 0
    end
  end
  inherited PanelBotones: TPanel
    Top = 156
    Width = 481
    TabOrder = 3
    ExplicitTop = 156
    ExplicitWidth = 481
    inherited OK_DevEx: TcxButton
      Left = 315
      Top = 5
      ParentFont = False
      OnClick = OKClick
      ExplicitLeft = 315
      ExplicitTop = 5
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 395
      Top = 5
      ParentFont = False
      ExplicitLeft = 395
      ExplicitTop = 5
    end
  end
  object GroupBox1: TcxGroupBox [2]
    Left = 3
    Top = 8
    Caption = ' Encuesta: '
    ParentFont = False
    TabOrder = 0
    Height = 89
    Width = 473
    object Label1: TLabel
      Left = 22
      Top = 24
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'Original:'
    end
    object Label2: TLabel
      Left = 27
      Top = 52
      Width = 35
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nueva:'
    end
    object ztbNombre: TZetaTextBox
      Left = 66
      Top = 22
      Width = 385
      Height = 17
      AutoSize = False
      Caption = 'ztbNombre'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object ET_NOMBRE: TEdit
      Left = 66
      Top = 48
      Width = 385
      Height = 21
      MaxLength = 40
      TabOrder = 0
    end
  end
  object cbEmpEvaluar: TCheckBox [3]
    Left = 20
    Top = 106
    Width = 155
    Height = 17
    Caption = 'Copiar empleados a evaluar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnClick = cbEmpEvaluarClick
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
