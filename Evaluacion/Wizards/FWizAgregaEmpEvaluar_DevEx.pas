unit FWizAgregaEmpEvaluar_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ZetaKeyLookup, ZetaEdit, StdCtrls, Buttons,
  ZetaWizard, ExtCtrls, ZetaDBTextBox, ZetaKeyCombo,
  ZCXBaseWizardFiltro, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus, ZetaCXWizard,
  cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl;

type
  TWizAgregaEmpEvaluar_DevEx = class(TBaseCXWizardFiltro)
    Label1: TLabel;
    ztbEncuesta: TZetaTextBox;
    Label2: TLabel;
    Label3: TLabel;
    US_CODIGO: TZetaKeyLookup_DevEx;
    zkcRelacion: TZetaKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizAgregaEmpEvaluar_DevEx: TWizAgregaEmpEvaluar_DevEx;

implementation

{$R *.DFM}

uses DEvaluacion,
     DSistema,
     DCliente,
     DProcesos,
     ZHelpContext,
     ZetaCommonTools,
     ZetaCommonLists,
     ZBaseSelectGrid_DevEx,
     FEvalAgregaempEvaluadorGridSelect_DevEx;

procedure TWizAgregaEmpEvaluar_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := US_CODIGO;
     HelpContext := H_DISENO_ENCUESTA_PROCESOS_GENERAR_LISTA_EMPLEADOS_EVALUAR;
     dmSistema.cdsUsuarios.LookupName := 'evaluador';
     US_CODIGO.LookupDataSet := dmSistema.cdsUsuarios;
end;

procedure TWizAgregaEmpEvaluar_DevEx.FormShow(Sender: TObject);
{
var
   i: Integer;
}
begin
     inherited;
     with dmEvaluacion do
     begin
          with zkcRelacion do
          begin
               LlenaPerfilEvaluadores( Lista, [ tevMismo ] );
               {
               Items.BeginUpdate;
               try
                  i := 0;
                  while i < ( Items.Count ) do
                  begin
                       if( strtoInt( zkcRelacion.Llaves[ i ] ) = Ord( tevMismo ) )then
                           Items.Delete( i );
                       i := i + 1;
                  end;
               finally
                      Items.EndUpdate;
               end;
               }
               if ( Items.Count > 0 ) then
                  zkcRelacion.ItemIndex := 0;
          end;
     end;
     dmSistema.cdsUsuarios.Conectar;
     ztbEncuesta.Caption := InttoStr( dmCliente.Encuesta ) + ' = ' + dmCliente.Nombre;
end;

procedure TWizAgregaEmpEvaluar_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with Descripciones do
     begin
          AddString( 'Encuesta', ztbEncuesta.Caption );
          AddString( 'Evaluador', GetDescripLlave( US_CODIGO ) );
          AddString('Relacion', zkcRelacion.Descripcion );
     end;


     with ParameterList do
     begin
          AddInteger( 'Encuesta', dmCliente.Encuesta );
          AddInteger( 'Evaluador', US_CODIGO.Valor );
          AddInteger( 'Relacion', strtoInt( zkcRelacion.Llaves[ zkcRelacion.ItemIndex ] ) );
     end;
end;

procedure TWizAgregaEmpEvaluar_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if Wizard.Adelante then
     begin
          if ( WizardControl.ActivePage = Parametros ) then
          begin
               if StrVacio( US_CODIGO.Llave ) then
                  CanMove := Error( '� No se ha seleccionado evaluador !', US_CODIGO )
               else if( dmEvaluacion.cdsPerfilEvaluador.RecordCount <= 0 )then
                    CanMove := Error( '� No se tienen registrados perfiles de evaluadores !', zkcRelacion )
               else if( strVacio( zkcRelacion.Descripcion ) )then
                    CanMove := Error( '� Es necesario seleccionar un perfil de evaluador !', zkcRelacion )
          end;
     end;
end;

procedure TWizAgregaEmpEvaluar_DevEx.CargaListaVerificacion;
begin
     dmProcesos.AgregaEmpEvaluarGlobalGetLista( ParameterList );
end;

function TWizAgregaEmpEvaluar_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEvalAgregaEmpEvaluadorGridSelect_DevEx );
end;

function TWizAgregaEmpEvaluar_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.AgregaEmpEvaluarGlobal( ParameterList, Verificacion );
end;

end.
