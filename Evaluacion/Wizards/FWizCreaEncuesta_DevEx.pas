unit FWizCreaEncuesta_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComCtrls, StdCtrls, Buttons, ExtCtrls, DBCtrls, Mask, Grids, DBGrids, Db,
     ZetaWizard,
     ZetaKeyLookup,
     ZetaFecha,
     ZetaNumero,
     ZetaDBGrid,
     ZetaCommonClasses,
     ZetaEdit,
     ZcxBaseWizardFiltro, cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus,
     ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
     cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
     dxCustomWizardControl, dxWizardControl;

type
  TWizCreaEncuesta_DevEx = class(TBaseCXWizardFiltro)
    OpenDialog: TOpenDialog;
    tsPregx: TdxWizardControlPage;
    tsBienvenida: TdxWizardControlPage;
    EP_DESCRIP: TMemo;
    EL_NOMBRE: TMemo;
    ET_MSG_FIN: TMemo;
    ET_MSG_INI: TMemo;
    ET_NUM_PRE: TZetaNumero;
    ET_DESCRIP: TMemo;
    ET_NOMBRE: TEdit;
    ET_FEC_INI: TZetaFecha;
    ET_DOCUMEN: TEdit;
    ET_FEC_FIN: TZetaFecha;
    ET_USR_ADM: TZetaKeyLookup_DevEx;
    ET_DOCUMENlbl: TLabel;
    ET_NUM_PRElbl: TLabel;
    ET_DESCRIPlbl: TLabel;
    ET_NOMBRElbl: TLabel;
    ET_FEC_INIlbl: TLabel;
    ET_DOCUMENFind: TcxButton;
    ET_FEC_FINlbl: TLabel;
    ET_USR_ADMlbl: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure ET_NUM_PREExit(Sender: TObject);
    procedure ET_DOCUMENFindClick(Sender: TObject);
  private
    { Private declarations }
    FTotalPaginas: Integer;
    FNumPregunta: Integer;
    FPreguntas: Integer;
    function VerificaExistenDatos: Boolean;
    function RegresaMemos( const oTabSheet: TdxWizardControlCustomPage; var oMemoEscalas: TMemo ): TMemo;
    procedure DoConnect;
    procedure Connect;
    procedure SetControles;
    procedure CreaPaginasPreguntas;
    procedure LiberaPaginas;
    //procedure EscribePanelTitulo;
    procedure InicializaRespuestas;
    procedure RecolectaPreguntasRespuestas( const oParametros: TZetaParams );
    procedure CargaPaginas;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaParametros; override;
    procedure CargaListaVerificacion; override;
  public
    { Public declarations }
  end;

var
  WizCreaEncuesta_DevEx: TWizCreaEncuesta_DevEx;

implementation

uses DEvaluacion,
     DSistema,
     DCliente,
     DProcesos,
     ZHelpContext,
     ZBaseSelectGrid_DevEx,
     ZetaDialogo,
     ZetaCommonTools,
     FCreaEncuestaSimpleGridSelect_DevEx,
     FTressShell;

const
     K_OFFSET = 2;
     K_PREGUNTA = ' Pregunta # %d';
     K_PREGUNTA_NO = ' #%d';
     K_TAB_PREGUNTA = 'tsPregunta';
     K_MEMO_PREGUNTAS = 'EP_DESCRIP';
     K_MEMO_RESPUESTAS = 'Respuestas';

{$R *.DFM}

procedure TWizCreaEncuesta_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := ET_NOMBRE;
     HelpContext := H_DISENO_ENCUESTA_PROCESOS_ENCUESTA_SIMPLE;
     WizardControl.ActivePage := Parametros;
     ET_USR_ADM.LookupDataset := dmSistema.cdsUsuarios;

     Parametros.PageIndex := 0;
     tsPregx.PageIndex := 1;
     FiltrosCondiciones.PageIndex := 2;
     tsBienvenida.PageIndex := 3;
     Ejecucion.PageIndex := 4;
end;

procedure TWizCreaEncuesta_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     DoConnect;
     SetControles;
     Parametros.PageIndex := 0;
     tsPregx.PageIndex := 1;
     FiltrosCondiciones.PageIndex := 2;
     tsBienvenida.PageIndex := 3;
     Ejecucion.PageIndex := 4;

     CreaPaginasPreguntas;
     WizardControl.ActivePage := Parametros;
     ActiveControl := ET_NOMBRE;

end;

procedure TWizCreaEncuesta_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     LiberaPaginas;
end;

procedure TWizCreaEncuesta_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
end;

procedure TWizCreaEncuesta_DevEx.DoConnect;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             try
                Connect;
             except
                   on Error: Exception do
                      zExcepcion( '� Error al conectar forma !', '� Ventana no pudo ser conectada a sus datos !', Error, 0 );
             end;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

procedure TWizCreaEncuesta_DevEx.CargaParametros;
const
     K_TODOS = 0;
     K_RANGO = 1;
     K_LISTA = 2;
begin
     inherited CargaParametros;

     with Descripciones do
     begin
          AddString( 'Nombre', ET_NOMBRE.Text );
          AddString( 'Fecha inicial', ZetaCommonTools.FechaCorta( ET_FEC_INI.Valor ) );
          AddString( 'Fecha final', ZetaCommonTools.FechaCorta( ET_FEC_FIN.Valor ) );
          AddInteger( 'No de preguntas', FPreguntas );
          AddInteger( 'Administrador', ET_USR_ADM.Valor );
          {$ifdef FALSE}
          if( rbTodos.Checked )then
              AddString( 'Evaluadores', 'Todos' )
          else if( rbRango.Checked )then
                   AddString( 'Evaluadores', 'Del: ' + edInicial.ValorAsText + ' al: ' + edFinal.ValorAsText )
               else
                   AddString( 'Evaluadores', edLista.Text );
          {$endif}
     end;

     with ParameterList do
     begin
          AddString( 'ET_NOMBRE', ET_NOMBRE.Text );
          AddString( 'ET_DOCUMEN', ET_DOCUMEN.Text );
          AddMemo( 'ET_DESCRIP', ET_DESCRIP.Text );
          AddDate( 'ET_FEC_INI', ET_FEC_INI.Valor );
          AddDate( 'ET_FEC_FIN', ET_FEC_FIN.Valor );
          AddInteger( 'ET_USR_ADM', ET_USR_ADM.Valor );
          AddInteger( 'ET_NUM_PRE', FPreguntas ); // GA: Antes era FNumPreguntas �xq?
          AddMemo( 'ET_MSG_INI', ET_MSG_INI.Text );
          AddMemo( 'ET_MSG_FIN', ET_MSG_FIN.Text );
          {$ifdef FALSE}
          if( rbTodos.Checked )then
              AddInteger( 'EVALUADORES', K_TODOS )
          else if( rbRango.Checked )then
               begin
                    AddInteger( 'EVALUADORES', K_RANGO );
                    AddInteger( 'RANGO_INI', edInicial.ValorEntero );
                    AddInteger( 'RANGO_FIN', edFinal.ValorEntero );
               end
               else
               begin
                    AddInteger( 'EVALUADORES', K_LISTA );
                    AddString( 'EVAL_LISTA', edLista.Text );
               end;
          {$endif}
          AddMemo( K_MEMO_PREGUNTAS + 'x', EP_DESCRIP.Text );
          AddMemo( K_MEMO_RESPUESTAS + 'x', EL_NOMBRE.Text );
     end;
     RecolectaPreguntasRespuestas( ParameterList );
end;

procedure TWizCreaEncuesta_DevEx.RecolectaPreguntasRespuestas( const oParametros: TZetaParams );
var
   i: Integer;
   otsPregunta: TdxWizardControlCustomPage;
   oPregunta, oRespuestas: TMemo;
begin
     with oParametros do
     begin
          if( FPreguntas > 1 )then
          begin
               for i := 0 to ( FPreguntas - K_OFFSET ) do
               begin
                    otsPregunta := WizardControl.Pages[ i + K_OFFSET ];
                    oPregunta := RegresaMemos( otsPregunta, oRespuestas );
                    AddMemo( Format( K_MEMO_PREGUNTAS + '%d', [ i ] ), oPregunta.Text );
                    AddMemo( Format( K_MEMO_RESPUESTAS + '%d', [ i ] ), oRespuestas.Text );
               end;
          end;
     end;
end;

procedure TWizCreaEncuesta_DevEx.SetControles;
const
     K_VALOR_INICIAL = 1;
begin
     FNumPregunta := 0;
     FPreguntas := K_VALOR_INICIAL;
     ET_NUM_PRE.Valor := K_VALOR_INICIAL;
     ET_FEC_INI.Valor := Date;
     ET_FEC_FIN.Valor := ET_FEC_INI.Valor;
     ET_USR_ADM.Valor := dmCliente.Usuario;
     InicializaRespuestas;
end;

procedure TWizCreaEncuesta_DevEx.CargaListaVerificacion;
begin
     dmProcesos.CreaEncuestaSimpleGetLista( ParameterList );
end;

function TWizCreaEncuesta_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TCreaEncuestaSimpleGridSelect_DevEx );
end;

function TWizCreaEncuesta_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CreaEncuestaSimple( ParameterList, Verificacion );
end;

procedure TWizCreaEncuesta_DevEx.CreaPaginasPreguntas;

   procedure CreaGroupBoxPregunta( const oPadre: TdxWizardControlCustomPage; const iValor: Integer );
   var
      oMemo: TMemo;
      oLabel : TLabel;
   begin
        oMemo := TMemo.Create( Self );
        with oMemo do
        begin
             Align := alCustom;
             Name := Format( K_MEMO_PREGUNTAS + '%d', [ iValor ] );
             Text := '';
             ScrollBars := ssVertical;
             MaxLength := 255;
             Parent := oPadre;
             Left := 3;
             Top := 22;
             Width := 235;
             Height:= 250;
        end;

        oLabel := TLabel.Create( Self );
        with oLabel do
        begin
            Align := alCustom;
            Caption := 'Capture el texto de la pregunta:';
            Parent := oPadre;
            Left := 3;
            Top := 3;
        end;
   end;

   procedure CreaGroupBoxEscalas( const oPadre: TdxWizardControlCustomPage; const iValor: Integer );
   var
      oMemo: TMemo;
      oLabel : TLabel;
   begin
        oMemo := TMemo.Create( Self );
        with oMemo do
        begin
             Align := alCustom;
             Name := Format( 'EL_NOMBRE%d', [ iValor ] );
             Left := 257;
             Top := 22;
             Text := VACIO;//EL_NOMBRE.Text;
             ScrollBars := ssVertical;
             //Font.Color := clBlue;
             Parent := oPadre;
             Width := 235;
             Height:= 250;
        end;

        oLabel := TLabel.Create( Self );
        with oLabel do
        begin
            Align := alCustom;
            Caption := 'Respuestas:';
            Parent := oPadre;
            Left := 257;
            Top := 3;
        end;
   end;

var
   i: Integer;
   PaginaActiva, PaginaNueva: TdxWizardControlCustomPage;
begin
     if ( WizardControl.PageCount < FTotalPaginas ) then
     begin
          PaginaActiva := WizardControl.ActivePage;
          for i := 0 to ( FPreguntas - K_OFFSET ) do
          begin
               PaginaNueva := TdxWizardControlCustomPage.Create( Self );
               with PaginaNueva do
               begin
                    WizardControl := Self.WizardControl;
                    PageIndex := i + K_OFFSET;
                    Name := Format( K_TAB_PREGUNTA + '%d', [ i + K_OFFSET ] );
                    PageVisible := True;
                    Header.Title := 'Crear Pregunta ' + Format( K_PREGUNTA_NO, [ i + K_OFFSET ] );
                    Header.TitleFont.Style := [fsBold];
                    Header.Description := 'Elaborar el texto de cada una de pregunta con su respectiva respuesta.';
               end;

               CreaGroupBoxPregunta( PaginaNueva, ( i + K_OFFSET ) );
               CreaGroupBoxEscalas( PaginaNueva, ( i + K_OFFSET ) );
          end;
          WizardControl.ActivePage := PaginaActiva;
     end;
end;

procedure TWizCreaEncuesta_DevEx.CargaPaginas;
begin
     FNumPregunta := 1;
     FPreguntas := ET_NUM_PRE.ValorEntero;
     FTotalPaginas := WizardControl.PageCount + ( FPreguntas - 1 );
     EP_DESCRIP.Clear;
     InicializaRespuestas;
     LiberaPaginas;
     CreaPaginasPreguntas;
end;

function TWizCreaEncuesta_DevEx.RegresaMemos( const oTabSheet: TdxWizardControlCustomPage; var oMemoEscalas: TMemo ): TMemo;
var
   i: Integer;
   oMemo: TMemo;
begin
     Result := nil;
     oMemoEscalas := nil;
     with oTabSheet do
     begin
          i := 0;
          while ( i < ControlCount ) do
          begin
               if ( Pos( 'EP_DESCRIP', Controls[ i ].Name ) > 0 )then
               begin
                    Result := TMemo( Controls[ i ] );
                    //Result :=  TMemo( oGroupBox.Controls[ 0 ] );
               end;
               if ( Pos( 'EL_NOMBRE', Controls[ i ].Name ) > 0 ) then
               begin
                    oMemo := TMemo( Controls[ i ] );
                    oMemoEscalas := oMemo;// TMemo( oGroupBox.Controls[ 0 ] );
                    Break;
               end;
               i := i + 1;
          end;
     end;
end;

procedure TWizCreaEncuesta_DevEx.LiberaPaginas;
var
   i, j: Integer;
   sTabNombre: String;
   PaginaActiva: TdxWizardControlCustomPage;
begin
     i := 0;
     PaginaActiva := WizardControl.ActivePage;
     while( i < ( WizardControl.PageCount - 1 ) )do
     begin
          sTabNombre := WizardControl.Pages[i].Name;
          if( Pos( 'tsPregunta', sTabNombre ) > 0 )then
          begin
               j := 0;
               while( j < ( WizardControl.Pages[i].ControlCount - 1 ) )do
               begin
                    WizardControl.Pages[i].Controls[j].Free;
                    j := j + 1;
               end;
               WizardControl.Pages[i].Free;
               i := i - 1 ;
          end;
          i := i + 1;
     end;

     WizardControl.ActivePage := PaginaActiva;
end;

function TWizCreaEncuesta_DevEx.VerificaExistenDatos: Boolean;
const
     K_PAGINAS_INICIO = 5;
var
   i, j: Integer;
   iTotalPaginas: Integer;
   oMemoPreg, oMemoEscalas: TMemo;
begin
     j := 2;
     Result := False;
     iTotalPaginas := WizardControl.PageCount;
     if ( iTotalPaginas > K_PAGINAS_INICIO ) then
     begin
          for i := K_PAGINAS_INICIO to iTotalPaginas do
          begin
               oMemoPreg := RegresaMemos( WizardControl.Pages[ j ], oMemoEscalas );
               Result := ( strLleno( oMemoPreg.Text ) or strLleno( oMemoEscalas.Text ) );
               if Result then
                  Break;
               j := j + 1;
          end;
     end;
end;

procedure TWizCreaEncuesta_DevEx.InicializaRespuestas;
begin
     with EL_NOMBRE.Lines do
     begin
          Clear;
          Add( 'Respuesta#1' );
          Add( 'Respuesta#2' );
          Add( 'Respuesta#3' );
     end;
end;

procedure TWizCreaEncuesta_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
const
     K_ERROR_PREGUNTAS = '� El texto de la pregunta no puede quedar vac�o !';
     K_ERROR_RESPUESTAS = '� La escala de respuestas para la pregunta no puede quedar vac�a !';
var
   oPregunta, oRespuesta: TMemo;
begin
     inherited;
     if Wizard.Adelante then
     begin
          if ( WizardControl.ActivePage = Parametros ) then
          begin
               if ( dmCliente.Encuesta <= 0 )then
                  CanMove := Error( '� No se han encontraso Encuestas v�lidas !', Parametros )
               else
                 if  not StrLleno(ET_NOMBRE.Text ) then
                      begin
                        CanMove := Error( '� El nombre de la encuesta no puede quedar vac�o !', Parametros );
                        ActiveControl := ET_NOMBRE;
                      end
                 else
                      if ( ET_NUM_PRE.ValorEntero <= 0 ) then
                        begin
                          CanMove := Error( '� El n�mero de preguntas en la encuesta debe de ser mayor que 0 !', Parametros );
                          ActiveControl := ET_NUM_PRE;
                        end
                      else
                        if StrVacio( ET_USR_ADM.Llave ) then
                          begin
                              CanMove := Error( 'Se debe asignar un responsable de Encuesta.', Parametros );
                              ActiveControl := ET_USR_ADM;
                          end;

               if CanMove then
               begin
                    CargaPaginas;
               end;
          end;

          if ( WizardControl.ActivePage = tsPregx ) then
          begin
               if StrVacio( EP_DESCRIP.Text ) then
                  begin
                    CanMove := Error( K_ERROR_PREGUNTAS, tsPregx );
                    ActiveControl := EP_DESCRIP;
                  end
               else
                   if StrVacio( EL_NOMBRE.Text ) then
                   begin
                      CanMove := Error( K_ERROR_RESPUESTAS, tsPregx );
                      ActiveControl := EL_NOMBRE;
                   end;
          end;

          if ( Pos( K_TAB_PREGUNTA, WizardControl.ActivePage.Name ) > 0 ) then
          begin
               oPregunta := RegresaMemos( WizardControl.ActivePage, oRespuesta );
               if StrVacio( oPregunta.Text ) then
                  begin
                    CanMove := Error( K_ERROR_PREGUNTAS, WizardControl.ActivePage );
                    ActiveControl := oPregunta;
                  end
               else
                   if StrVacio( oRespuesta.Text ) then
                   begin
                      CanMove := Error( K_ERROR_RESPUESTAS, WizardControl.ActivePage );
                      Activecontrol := oRespuesta;
                   end;
          end;
     end;
end;

procedure TWizCreaEncuesta_DevEx.WizardAfterMove(Sender: TObject);
var
   oPregunta, oRespuestas: TMemo;
begin
     inherited;
     if (WizardControl.IsDestroying = False ) then
     begin
     if ( WizardControl.ActivePage = Parametros ) then
        ActiveControl := ET_NOMBRE
     else
         if ( WizardControl.ActivePage = tsBienvenida ) then
            ActiveControl := ET_MSG_INI
         else
             if ( WizardControl.ActivePage = tsPregx ) then
                 ActiveControl := EP_DESCRIP
             else
                 if ( Pos( K_TAB_PREGUNTA, WizardControl.ActivePage.Name ) > 0 ) then
                    begin
                        oPregunta := RegresaMemos( WizardControl.ActivePage, oRespuestas );
                        ActiveControl := oPregunta;
                end;
     end;

end;

procedure TWizCreaEncuesta_DevEx.ET_NUM_PREExit(Sender: TObject);
const
     K_PREGUNTA = 'Se perder� el contenido de las preguntas previamente creadas' + CR_LF + '� Desea cambiar el n�mero de preguntas ?';
begin
     inherited;
     if ( FPreguntas <> ET_NUM_PRE.ValorEntero ) and VerificaExistenDatos then
     begin
          if not zConfirm( Self.Caption, K_PREGUNTA, 0, mbNo ) then
          begin
               ET_NUM_PRE.Valor := FPreguntas;
          end;
     end;
end;

procedure TWizCreaEncuesta_DevEx.ET_DOCUMENFindClick(Sender: TObject);
begin
     inherited;
     with ET_DOCUMEN do
     begin
          with OpenDialog do
          begin
               FileName := Text;
               if Execute then
               begin
                    Text := ExtractFileName( FileName );
               end;
          end;
     end;
end;

end.
