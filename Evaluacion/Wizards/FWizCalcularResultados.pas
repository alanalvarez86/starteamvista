unit FWizCalcularResultados;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseWizard, ZetaDBTextBox, StdCtrls, ComCtrls, Buttons, ZetaWizard,
  ExtCtrls;

type
  TWizCalcularResultados = class(TBaseWizard)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    ET_CODIGO: TZetaTextBox;
    ET_NOMBRE: TZetaTextBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
  private
    { Private declarations }
  protected
    procedure CargaParametros; override;
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizCalcularResultados: TWizCalcularResultados;

implementation

{$R *.DFM}
uses ZHelpContext,
     DCliente,
     DProcesos;

procedure TWizCalcularResultados.FormCreate(Sender: TObject);
begin
     inherited;
      //ParametrosControl := US_CODIGO;
     HelpContext := H_DISENO_ENCUESTA_PROCESOS_CALCULAR_RESULTADOS;
end;

procedure TWizCalcularResultados.FormShow(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          ET_CODIGO.Caption := InttoStr( Encuesta );
          ET_NOMBRE.Caption := Nombre;
     end;
end;

procedure TWizCalcularResultados.CargaParametros;
begin
     with Descripciones do
     begin
          AddString( 'Encuesta', ET_CODIGO.Caption + ' = ' + ET_NOMBRE.Caption );
     end;

     inherited CargaParametros;

     with ParameterList do
     begin
          AddInteger( 'Encuesta', dmCliente.Encuesta );
     end;
end;

procedure TWizCalcularResultados.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if Wizard.Adelante then
     begin
          if ( PageControl.ActivePage = Parametros ) then
          begin
               if( dmCliente.Encuesta <= 0 )then
                  CanMove := Error( '� No se han encontraso Encuestas v�lidas !', Siguiente );
          end;
     end;
end;

function TWizCalcularResultados.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CalcularResultados( ParameterList );
end;

end.
