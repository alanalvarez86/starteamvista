inherited WizCreaEncuesta: TWizCreaEncuesta
  Left = 504
  Top = 339
  Caption = 'Crear Encuesta'
  ClientHeight = 360
  ClientWidth = 516
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    Top = 324
    Width = 516
    BeforeMove = WizardBeforeMove
    inherited Anterior: TZetaWizardButton
      Enabled = False
    end
    inherited Salir: TZetaWizardButton
      Left = 428
    end
    inherited Ejecutar: TZetaWizardButton
      Left = 345
    end
  end
  inherited PageControl: TPageControl
    Top = 33
    Width = 516
    Height = 291
    ActivePage = Parametros
    inherited Parametros: TTabSheet
      object ET_NOMBRElbl: TLabel
        Left = 43
        Top = 9
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nombre:'
      end
      object ET_DESCRIPlbl: TLabel
        Left = 24
        Top = 32
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci�n:'
      end
      object ET_NUM_PRElbl: TLabel
        Left = 12
        Top = 161
        Width = 71
        Height = 13
        Alignment = taRightJustify
        Caption = 'No. Preguntas:'
      end
      object ET_FEC_INIlbl: TLabel
        Left = 55
        Top = 185
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Inicio:'
      end
      object ET_USR_ADMlbl: TLabel
        Left = 18
        Top = 231
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = 'Responsable:'
      end
      object ET_FEC_FINlbl: TLabel
        Left = 58
        Top = 207
        Width = 25
        Height = 13
        Alignment = taRightJustify
        Caption = 'Final:'
      end
      object ET_DOCUMENFind: TSpeedButton
        Left = 467
        Top = 251
        Width = 23
        Height = 22
        Hint = 'Buscar archivo de documentaci�n de la encuesta'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
          300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
          330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
          333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
          339977FF777777773377000BFB03333333337773FF733333333F333000333333
          3300333777333333337733333333333333003333333333333377333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = ET_DOCUMENFindClick
      end
      object ET_DOCUMENlbl: TLabel
        Left = 5
        Top = 254
        Width = 78
        Height = 13
        Alignment = taRightJustify
        Caption = 'Documentaci�n:'
      end
      object ET_NOMBRE: TEdit
        Left = 86
        Top = 6
        Width = 401
        Height = 21
        MaxLength = 40
        TabOrder = 0
      end
      object ET_DESCRIP: TMemo
        Left = 86
        Top = 30
        Width = 401
        Height = 124
        MaxLength = 255
        ScrollBars = ssVertical
        TabOrder = 1
      end
      object ET_NUM_PRE: TZetaNumero
        Left = 86
        Top = 156
        Width = 49
        Height = 21
        Mascara = mnDias
        MaxLength = 2
        TabOrder = 2
        Text = '0'
        OnExit = ET_NUM_PREExit
      end
      object ET_FEC_INI: TZetaFecha
        Left = 86
        Top = 179
        Width = 105
        Height = 22
        Cursor = crArrow
        TabOrder = 3
        Text = '18/Aug/05'
        Valor = 38582
      end
      object ET_USR_ADM: TZetaKeyLookup
        Left = 86
        Top = 227
        Width = 404
        Height = 21
        TabOrder = 5
        TabStop = True
        WidthLlave = 70
      end
      object ET_FEC_FIN: TZetaFecha
        Left = 86
        Top = 203
        Width = 105
        Height = 22
        Cursor = crArrow
        TabOrder = 4
        Text = '18/Aug/05'
        Valor = 38582
      end
      object ET_DOCUMEN: TEdit
        Left = 86
        Top = 251
        Width = 379
        Height = 21
        TabOrder = 6
      end
    end
    object tsPregx: TTabSheet [1]
      Caption = 'tsPregx'
      ImageIndex = 3
      TabVisible = False
      object gbPreguntas: TGroupBox
        Left = 0
        Top = 0
        Width = 508
        Height = 105
        Align = alTop
        Caption = ' Capture aqu� el texto de la pregunta: '
        TabOrder = 0
        object EP_DESCRIP: TMemo
          Left = 2
          Top = 15
          Width = 504
          Height = 88
          Align = alClient
          MaxLength = 255
          ScrollBars = ssVertical
          TabOrder = 0
        end
      end
      object pnLeyenda: TPanel
        Left = 0
        Top = 105
        Width = 508
        Height = 21
        Align = alTop
        BevelOuter = bvNone
        Caption = 
          'Escribir una respuesta por rengl�n, con una longitud m�xima de 3' +
          '0 caracteres'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
      object pnIzq: TPanel
        Left = 0
        Top = 126
        Width = 145
        Height = 155
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 2
      end
      object gbRespuestas: TGroupBox
        Left = 145
        Top = 126
        Width = 218
        Height = 155
        Align = alClient
        Caption = ' Respuestas: '
        TabOrder = 3
        object EL_NOMBRE: TMemo
          Left = 2
          Top = 15
          Width = 214
          Height = 138
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 0
        end
      end
      object pnDer: TPanel
        Left = 363
        Top = 126
        Width = 145
        Height = 155
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 4
      end
    end
    inherited FiltrosCondiciones: TTabSheet
      inherited FiltrosGB: TGroupBox
        Left = 0
        Top = 0
        Width = 508
        Align = alClient
        inherited sFiltroLBL: TLabel
          Left = 70
        end
        inherited sCondicionLBl: TLabel
          Left = 45
        end
        inherited sFiltro: TMemo
          Left = 98
        end
        inherited GBRango: TGroupBox
          Left = 98
        end
        inherited ECondicion: TZetaKeyLookup
          Left = 98
        end
        inherited BAgregaCampo: TBitBtn
          Left = 413
        end
        inherited Seleccionar: TBitBtn
          Left = 166
          Visible = True
        end
      end
    end
    object tsBienvenida: TTabSheet [3]
      Caption = 'tsBienvenida'
      ImageIndex = 4
      TabVisible = False
      object ET_MSG_INI: TMemo
        Left = 0
        Top = 0
        Width = 508
        Height = 281
        Align = alClient
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object tsDespedida: TTabSheet [4]
      Caption = 'tsDespedida'
      ImageIndex = 5
      TabVisible = False
      object ET_MSG_FIN: TMemo
        Left = 0
        Top = 0
        Width = 508
        Height = 281
        Align = alClient
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    inherited Ejecucion: TTabSheet
      inherited Advertencia: TMemo
        Width = 508
        Lines.Strings = (
          ''
          'Advertencia'
          
            'Se crear� una encuesta simple con los valores especificados, as�' +
            ' como las'
          'preguntas y las respuestas anteriormente descritas.'
          ''
          'Presione el bot�n '#39'Ejecutar'#39' para iniciar el proceso.')
      end
      inherited ProgressPanel: TPanel
        Top = 214
        Width = 508
        inherited ProgressBar: TProgressBar
          Width = 457
        end
      end
    end
  end
  object PanelTitulo1: TPanel
    Left = 0
    Top = 0
    Width = 516
    Height = 33
    Align = alTop
    Alignment = taLeftJustify
    BevelOuter = bvNone
    Caption = 'Generales'
    Color = clGray
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Trebuchet MS'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
  end
  object OpenDialog: TOpenDialog
    Filter = 
      'Adobe Acrobat (*.pdf)|*.pdf|Microsoft Word (*.doc)|*.doc|Paginas' +
      ' Web (*.htm)|*.htm|Todos los archivos (*.*)|*.*'
    FilterIndex = 0
    Title = 'Buscar documentacii�n'
    Left = 244
    Top = 209
  end
end
