inherited WizPrepararEvaluaciones_DevEx: TWizPrepararEvaluaciones_DevEx
  Left = 345
  Top = 311
  Caption = 'Preparar Evaluaciones'
  ClientHeight = 299
  ClientWidth = 479
  ExplicitWidth = 485
  ExplicitHeight = 328
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 479
    Height = 299
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EC400000EC401952B
      0E1B00000213494441545847CD96AF4EC35014C659B71104028140201008248F
      C00621481E0143428221BCC0A52080B6CBCC3CF02CF000380402814020108889
      89F27DCB3DE3F4AEEDBAB16E9CE4979D3FED3DDFEE3DEDB610C7F15C494DCE92
      D4E42C194E286B9BF64A68C29D32C915C00B223F8CCBE47F0B28EB0802139C15
      12509651C4408036E71B372CDA9F04B9BF0976D987B95401BA50166E9F7E63B1
      B90B5047205B27E838AF960A86EEBC90009A3B9445C042158BC74FB78E86FB6C
      46D82321A0E5877753E201DCEB5C646E0F433FA8B8DF38118B4370F1B78E4157
      F93D271E49E887179109967277401C9CD3016700FE33E821DE8E4CB40AFF1D74
      E16FDD989B7588FCE2F545B0023C346C482E5340CBB436FB453F7842DC858035
      1BBFB269C774968D314BA87DC83DA3B00216D1704F72FD35D304B0097851F127
      9B4B0CD8F84DC523B1026A68D8945CA600C0C5FF0C85E3530BE0108E3E021668
      F0FB8FD1A4868647B22605D8C7F0B7212C57006F40ECD9706C9B8A80B9EE00FC
      3AB1E1D8360D0155D901BD1379BBC29AD433042486B06DAE375823994790065E
      AF35E0592A8C9DBA0701C7B2261A5C32EF0A48985B809FF831612CB831A84A7E
      70BD094E91130157AC8F25C059AC4A74CEE62BA00E6AD61F88807F226B42808F
      78E8559CB0CCC28486868919604E0F9D08156622400F5DA6000C90FE6D9F18BC
      8A1F5D01DA320594413101DCB292E07F0ADB77604901F1C20F802EB31FA1683A
      570000000049454E44AE426082}
    ExplicitWidth = 503
    ExplicitHeight = 414
    inherited Parametros: TdxWizardControlPage
      Header.AssignedValues = [wchvTitleFont]
      Header.Description = 
        'Proceso para preparar la informaci'#243'n de las evaluaciones obtenid' +
        'as de la encuesta actual.'
      Header.Title = 'Informaci'#243'n general de las evaluaciones.'
      ParentFont = False
      ExplicitWidth = 457
      ExplicitHeight = 159
      object GroupBox1: TGroupBox
        Left = 14
        Top = 21
        Width = 426
        Height = 105
        Caption = ' Datos de Encuesta: '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Label1: TLabel
          Left = 16
          Top = 30
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object Label2: TLabel
          Left = 16
          Top = 68
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
        end
        object ET_CODIGO: TZetaTextBox
          Left = 60
          Top = 28
          Width = 69
          Height = 17
          AutoSize = False
          Caption = 'ET_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object ET_NOMBRE: TZetaTextBox
          Left = 60
          Top = 66
          Width = 357
          Height = 17
          AutoSize = False
          Caption = 'ET_NOMBRE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      Header.AssignedValues = [wchvTitleFont]
      ParentFont = False
      ExplicitWidth = 457
      ExplicitHeight = 159
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 481
        ExplicitHeight = 179
        Height = 64
        Width = 457
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 481
        Width = 457
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          ExplicitWidth = 413
          Width = 389
          AnchorY = 57
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
  end
end
