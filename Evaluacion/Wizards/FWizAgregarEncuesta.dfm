inherited AgregarEncuesta: TAgregarEncuesta
  Left = 297
  Top = 238
  Width = 545
  Height = 353
  Caption = 'Encuesta nueva'
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    Top = 283
    Width = 537
    TabOrder = 1
    AlEjecutar = WizardAlEjecutar
    AlCancelar = WizardAlCancelar
    AfterMove = WizardAfterMove
    BeforeMove = WizardBeforeMove
    inherited Salir: TZetaWizardButton
      Left = 449
      Kind = bkCancel
    end
    inherited Ejecutar: TZetaWizardButton
      Left = 366
    end
  end
  inherited PageControl: TPageControl
    Top = 33
    Width = 537
    Height = 250
    ActivePage = tsGenerales
    TabOrder = 0
    object tsGenerales: TTabSheet
      Caption = 'tsGenerales'
      TabVisible = False
      object ET_NOMBRElbl: TLabel
        Left = 50
        Top = 4
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nombre:'
      end
      object ET_DESCRIPlbl: TLabel
        Left = 31
        Top = 22
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripción:'
      end
      object ET_FEC_INIlbl: TLabel
        Left = 62
        Top = 125
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Inicio:'
      end
      object ET_FEC_FINlbl: TLabel
        Left = 62
        Top = 148
        Width = 25
        Height = 13
        Alignment = taRightJustify
        Caption = 'Final:'
      end
      object ET_USR_ADMlbl: TLabel
        Left = 25
        Top = 169
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = 'Responsable:'
      end
      object ET_DOCUMENlbl: TLabel
        Left = 12
        Top = 191
        Width = 78
        Height = 13
        Alignment = taRightJustify
        Caption = 'Documentación:'
      end
      object ET_DOCUMENFind: TSpeedButton
        Left = 504
        Top = 188
        Width = 22
        Height = 22
        Hint = 'Buscar archivo de documentación de la encuesta'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
          300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
          330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
          333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
          339977FF777777773377000BFB03333333337773FF733333333F333000333333
          3300333777333333337733333333333333003333333333333377333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = ET_DOCUMENFindClick
      end
      object ET_CONSOLAlbl: TLabel
        Left = 2
        Top = 213
        Width = 87
        Height = 28
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Mostrar en tablero de avance:'
        WordWrap = True
      end
      object ET_NOMBRE: TDBEdit
        Left = 92
        Top = 1
        Width = 434
        Height = 21
        DataField = 'ET_NOMBRE'
        DataSource = DataSource
        TabOrder = 0
      end
      object ET_DESCRIP: TDBMemo
        Left = 92
        Top = 22
        Width = 434
        Height = 98
        DataField = 'ET_DESCRIP'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 1
      end
      object ET_FEC_FIN: TZetaDBFecha
        Left = 92
        Top = 144
        Width = 105
        Height = 22
        Cursor = crArrow
        TabOrder = 3
        Text = '13/Oct/04'
        Valor = 38273
        DataField = 'ET_FEC_FIN'
        DataSource = DataSource
      end
      object ET_FEC_INI: TZetaDBFecha
        Left = 92
        Top = 121
        Width = 105
        Height = 22
        Cursor = crArrow
        TabOrder = 2
        Text = '13/Oct/04'
        Valor = 38273
        DataField = 'ET_FEC_INI'
        DataSource = DataSource
      end
      object ET_USR_ADM: TZetaDBKeyLookup
        Left = 92
        Top = 166
        Width = 435
        Height = 21
        TabOrder = 4
        TabStop = True
        WidthLlave = 70
        DataField = 'ET_USR_ADM'
        DataSource = DataSource
      end
      object ET_DOCUMEN: TDBEdit
        Left = 92
        Top = 187
        Width = 410
        Height = 21
        DataField = 'ET_DOCUMEN'
        DataSource = DataSource
        TabOrder = 5
      end
      object ET_CONSOLA: TDBCheckBox
        Left = 90
        Top = 219
        Width = 16
        Height = 17
        Alignment = taLeftJustify
        DataField = 'ET_CONSOLA'
        DataSource = DataSource
        TabOrder = 6
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
    end
    object tsEscalas: TTabSheet
      Caption = 'tsEscalas'
      ImageIndex = 1
      TabVisible = False
      object ET_ESC_DEFlbl: TLabel
        Left = 29
        Top = 16
        Width = 110
        Height = 13
        Alignment = taRightJustify
        Caption = 'Escalas de evaluación:'
      end
      object ET_ESC_PESlbl: TLabel
        Left = 32
        Top = 103
        Width = 107
        Height = 13
        Alignment = taRightJustify
        Caption = 'Escala de importancia:'
      end
      object ET_TXT_COMlbl: TLabel
        Left = 108
        Top = 216
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Título:'
        Enabled = False
      end
      object ET_PTS_CALlbl: TLabel
        Left = 24
        Top = 131
        Width = 114
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puntos calificación final:'
      end
      object ET_ESC_CALlbl: TLabel
        Left = 25
        Top = 160
        Width = 113
        Height = 13
        Alignment = taRightJustify
        Caption = 'Escala calificación final:'
      end
      object ET_ESCALAlbl: TLabel
        Left = 50
        Top = 45
        Width = 89
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nombre de escala:'
      end
      object ET_ESTILOlbl: TLabel
        Left = 5
        Top = 74
        Width = 134
        Height = 13
        Alignment = taRightJustify
        Caption = 'Presentación de respuestas:'
      end
      object ET_GET_COM: TDBCheckBox
        Left = 50
        Top = 186
        Width = 105
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Pedir comentarios:'
        DataField = 'ET_GET_COM'
        DataSource = DataSource
        TabOrder = 6
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = ET_GET_COMClick
      end
      object ET_TXT_COM: TDBEdit
        Left = 142
        Top = 212
        Width = 349
        Height = 21
        DataField = 'ET_TXT_COM'
        DataSource = DataSource
        Enabled = False
        TabOrder = 7
      end
      object ET_ESC_DEF: TZetaDBKeyLookup
        Left = 142
        Top = 12
        Width = 375
        Height = 21
        TabOrder = 0
        TabStop = True
        WidthLlave = 70
        DataField = 'ET_ESC_DEF'
        DataSource = DataSource
      end
      object ET_ESC_PES: TZetaDBKeyLookup
        Left = 142
        Top = 99
        Width = 375
        Height = 21
        TabOrder = 3
        TabStop = True
        WidthLlave = 70
        DataField = 'ET_ESC_PES'
        DataSource = DataSource
      end
      object ET_PTS_CAL: TZetaDBKeyCombo
        Left = 142
        Top = 128
        Width = 193
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 4
        OnClick = ET_PTS_CALClick
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        DataField = 'ET_PTS_CAL'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object ET_ESC_CAL: TZetaDBKeyLookup
        Left = 142
        Top = 157
        Width = 375
        Height = 21
        TabOrder = 5
        TabStop = True
        WidthLlave = 70
        DataField = 'ET_ESC_CAL'
        DataSource = DataSource
      end
      object ET_ESCALA: TDBEdit
        Left = 142
        Top = 41
        Width = 349
        Height = 21
        DataField = 'ET_ESCALA'
        DataSource = DataSource
        TabOrder = 1
      end
      object ET_ESTILO: TZetaDBKeyCombo
        Left = 142
        Top = 70
        Width = 193
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 2
        ListaFija = lfEstiloRespuesta
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        DataField = 'ET_ESTILO'
        DataSource = DataSource
        LlaveNumerica = True
      end
    end
    object tsCriterios: TTabSheet
      Caption = 'tsCriterios'
      ImageIndex = 2
      TabVisible = False
      object Panel1: TPanel
        Left = 496
        Top = 26
        Width = 33
        Height = 214
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object btnSubir: TZetaSmartListsButton
          Left = 5
          Top = 88
          Width = 25
          Height = 25
          Hint = 'Subir criterio'
          Enabled = False
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
            3333333333090333333333333309033333333333330903333333333333090333
            3333333333090333333333300009000033333330999999903333333309999903
            3333333309999903333333333099903333333333309990333333333333090333
            3333333333090333333333333330333333333333333033333333}
          ParentShowHint = False
          ShowHint = True
          Tipo = bsSubir
          SmartLists = ZSLCriterios
        end
        object btnBajar: TZetaSmartListsButton
          Left = 5
          Top = 118
          Width = 25
          Height = 25
          Hint = 'Bajar criterio'
          Enabled = False
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
            3333333333303333333333333309033333333333330903333333333330999033
            3333333330999033333333330999990333333333099999033333333099999990
            3333333000090000333333333309033333333333330903333333333333090333
            3333333333090333333333333309033333333333330003333333}
          ParentShowHint = False
          ShowHint = True
          Tipo = bsBajar
          SmartLists = ZSLCriterios
        end
      end
      object gbSeleccionados: TGroupBox
        Left = 266
        Top = 26
        Width = 230
        Height = 214
        Align = alLeft
        Caption = ' Seleccionados: '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        object zmlDestino: TZetaSmartListBox
          Left = 2
          Top = 15
          Width = 226
          Height = 197
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          TabOrder = 0
        end
      end
      object Panel2: TPanel
        Left = 230
        Top = 26
        Width = 36
        Height = 214
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 2
        object btnEscogeCriterio: TZetaSmartListsButton
          Left = 5
          Top = 88
          Width = 25
          Height = 25
          Hint = 'Seleccionar criterio'
          Enabled = False
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333333333333333333333333333333333333333333333333003333
            33333333330B003333333333330BBB0033330000000BBBBB00330BBBBBBBBBBB
            BB000000000BBBBB00333333330BBB0033333333330B00333333333333003333
            3333333333333333333333333333333333333333333333333333}
          ParentShowHint = False
          ShowHint = True
          OnClick = btnEscogeCriterioClick
          Tipo = bsEscoger
          SmartLists = ZSLCriterios
        end
        object btnRechazaCriterioOld: TZetaSmartListsButton
          Left = 5
          Top = 22
          Width = 25
          Height = 25
          Hint = 'Rechazar criterio'
          Enabled = False
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333333333333333333333333333330033333333333300B033
            3333333300BBB03333333300BBBBB000000000BBBBBBBBBBBBB03300BBBBB000
            0000333300BBB033333333333300B03333333333333300333333333333333333
            3333333333333333333333333333333333333333333333333333}
          ParentShowHint = False
          ShowHint = True
          Visible = False
          Tipo = bsRechazar
          SmartLists = ZSLCriterios
        end
        object btnSeleccionaTodos: TSpeedButton
          Left = 5
          Top = 58
          Width = 25
          Height = 25
          Hint = 'Seleccionar todos los criterios'
          Enabled = False
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777777777777777777777777777777777777707707777777777770070077
            7777777770E00E077777700000EE0EE0777770EEEEEEE0EE077770EEEEEEEE0E
            E07770EEEEEEE0EE0777700000EE0EE07777777770E00E077777777770070077
            7777777770770777777777777777777777777777777777777777}
          ParentShowHint = False
          ShowHint = True
          OnClick = btnSeleccionaTodosClick
        end
        object btnRechazaTodos: TSpeedButton
          Left = 5
          Top = 148
          Width = 25
          Height = 25
          Hint = 'Rechazar todos los criterios'
          Enabled = False
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777777777777777777777777777777777777777077077777777777007007
            7777777770E00E07777777770EE0EE0000777770EE0EEEEEEE77770EE0EEEEEE
            EE777770EE0EEEEEEE7777770EE0EE000077777770E00E077777777777007007
            7777777777707707777777777777777777777777777777777777}
          OnClick = btnRechazaTodosClick
        end
        object btnRechazaCriterio: TSpeedButton
          Left = 5
          Top = 118
          Width = 25
          Height = 25
          Enabled = False
          OnClick = btnRechazaCriterioClick
        end
      end
      object gbDisponibles: TGroupBox
        Left = 0
        Top = 26
        Width = 230
        Height = 214
        Align = alLeft
        Caption = ' Disponibles: '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        object zmlFuente: TZetaSmartListBox
          Left = 2
          Top = 15
          Width = 226
          Height = 197
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          TabOrder = 0
        end
      end
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 529
        Height = 26
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 4
        object Label10: TLabel
          Left = 36
          Top = 7
          Width = 113
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipos de competencias:'
        end
        object zkcTipos: TZetaKeyCombo
          Left = 152
          Top = 3
          Width = 225
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 0
          OnChange = zkcTiposChange
          ListaFija = lfNinguna
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = True
        end
      end
    end
    object tsEvaluadores: TTabSheet
      Caption = 'tsEvaluadores'
      ImageIndex = 3
      TabVisible = False
      object gbAvailable: TGroupBox
        Left = 0
        Top = 0
        Width = 230
        Height = 240
        Align = alLeft
        Caption = ' Disponibles: '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        object lbEvalDisponibles: TZetaSmartListBox
          Left = 2
          Top = 15
          Width = 226
          Height = 223
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          TabOrder = 0
        end
      end
      object Panel3: TPanel
        Left = 230
        Top = 0
        Width = 36
        Height = 240
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object btnSelect: TZetaSmartListsButton
          Left = 5
          Top = 88
          Width = 25
          Height = 25
          Hint = 'Seleccionar perfil de evaluador'
          Enabled = False
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333333333333333333333333333333333333333333333333003333
            33333333330B003333333333330BBB0033330000000BBBBB00330BBBBBBBBBBB
            BB000000000BBBBB00333333330BBB0033333333330B00333333333333003333
            3333333333333333333333333333333333333333333333333333}
          ParentShowHint = False
          ShowHint = True
          Tipo = bsEscoger
          SmartLists = ZSLPerfiles
        end
        object btnDeselect: TZetaSmartListsButton
          Left = 5
          Top = 118
          Width = 25
          Height = 25
          Hint = 'Rechazar perfil de evaluador'
          Enabled = False
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333333333333333333333333333330033333333333300B033
            3333333300BBB03333333300BBBBB000000000BBBBBBBBBBBBB03300BBBBB000
            0000333300BBB033333333333300B03333333333333300333333333333333333
            3333333333333333333333333333333333333333333333333333}
          ParentShowHint = False
          ShowHint = True
          Tipo = bsRechazar
          SmartLists = ZSLPerfiles
        end
      end
      object gbSelected: TGroupBox
        Left = 266
        Top = 0
        Width = 230
        Height = 240
        Align = alLeft
        Caption = ' Seleccionados: '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        object lbEvalSelecc: TZetaSmartListBox
          Left = 2
          Top = 15
          Width = 226
          Height = 223
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ItemHeight = 13
          ParentFont = False
          TabOrder = 0
        end
      end
      object Panel4: TPanel
        Left = 496
        Top = 0
        Width = 33
        Height = 240
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 3
        object btnRaise: TZetaSmartListsButton
          Left = 6
          Top = 88
          Width = 25
          Height = 25
          Hint = 'Subir perfil de evaluador'
          Enabled = False
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
            3333333333090333333333333309033333333333330903333333333333090333
            3333333333090333333333300009000033333330999999903333333309999903
            3333333309999903333333333099903333333333309990333333333333090333
            3333333333090333333333333330333333333333333033333333}
          ParentShowHint = False
          ShowHint = True
          Tipo = bsSubir
          SmartLists = ZSLPerfiles
        end
        object btnLower: TZetaSmartListsButton
          Left = 6
          Top = 118
          Width = 25
          Height = 25
          Hint = 'Bajar perfil de evaluador'
          Enabled = False
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
            3333333333303333333333333309033333333333330903333333333330999033
            3333333330999033333333330999990333333333099999033333333099999990
            3333333000090000333333333309033333333333330903333333333333090333
            3333333333090333333333333309033333333333330003333333}
          ParentShowHint = False
          ShowHint = True
          Tipo = bsBajar
          SmartLists = ZSLPerfiles
        end
      end
    end
    object tsBienvenida: TTabSheet
      Caption = 'tsBienvenida'
      ImageIndex = 4
      TabVisible = False
      object ET_MSG_INI: TDBMemo
        Left = 0
        Top = 0
        Width = 529
        Height = 240
        Align = alClient
        DataField = 'ET_MSG_INI'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object tsDespedida: TTabSheet
      Caption = 'tsDespedida'
      ImageIndex = 5
      TabVisible = False
      object ET_MSG_FIN: TDBMemo
        Left = 0
        Top = 0
        Width = 529
        Height = 240
        Align = alClient
        DataField = 'ET_MSG_FIN'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
  end
  object PanelTitulo1: TPanel
    Left = 0
    Top = 0
    Width = 537
    Height = 33
    Align = alTop
    Alignment = taLeftJustify
    BevelOuter = bvNone
    Caption = 'Generales'
    Color = clGray
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Trebuchet MS'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
  end
  object DataSource: TDataSource
    Left = 324
    Top = 1
  end
  object ZSLCriterios: TZetaSmartLists
    BorrarAlCopiar = True
    CopiarObjetos = True
    ListaDisponibles = zmlFuente
    ListaEscogidos = zmlDestino
    AlBajar = ZSLCriteriosAlBajar
    AlEscoger = ZSLCriteriosAlEscoger
    AlRechazar = ZSLCriteriosAlRechazar
    AlSubir = ZSLCriteriosAlSubir
    Left = 188
    Top = 65535
  end
  object ZSLPerfiles: TZetaSmartLists
    BorrarAlCopiar = True
    CopiarObjetos = True
    ListaDisponibles = lbEvalDisponibles
    ListaEscogidos = lbEvalSelecc
    AlBajar = ZSLCriteriosAlBajar
    AlEscoger = ZSLPerfilesAlEscoger
    AlRechazar = ZSLPerfilesAlRechazar
    AlSubir = ZSLCriteriosAlSubir
    Left = 224
  end
  object OpenDialog: TOpenDialog
    Filter = 
      'Adobe Acrobat (*.pdf)|*.pdf|Microsoft Word (*.doc)|*.doc|Paginas' +
      ' Web (*.htm)|*.htm|Todos los archivos (*.*)|*.*'
    FilterIndex = 0
    Title = 'Buscar documentaciión'
    Left = 276
    Top = 241
  end
end
