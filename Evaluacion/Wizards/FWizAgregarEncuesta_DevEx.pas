unit FWizAgregarEncuesta_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComCtrls, StdCtrls, Buttons, ExtCtrls, DBCtrls, Mask, Grids, DBGrids, Db,
     ZetaWizard,
     ZetaKeyLookup,
     ZetaFecha,
     ZetaNumero,
     ZetaDBGrid,
     ZetaCommonClasses,
     ZetaEdit,
     ZcxBaseWizardFiltro, cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus,
     ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
     cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
     dxCustomWizardControl, dxWizardControl, ZWizardBasico, ZetaSmartLists,
     ZetaKeyCombo, DEvaluacion, ZetaSmartLists_DevEx, cxListBox;

type
  TAgregarEncuesta_DevEx = class(TBaseCXWizardFiltro)
    DataSource: TDataSource;
    OpenDialog: TOpenDialog;
    ET_NOMBRElbl: TLabel;
    ET_NOMBRE: TDBEdit;
    ET_DESCRIPlbl: TLabel;
    ET_DESCRIP: TDBMemo;
    ET_FEC_INIlbl: TLabel;
    ET_FEC_INI: TZetaDBFecha;
    ET_FEC_FINlbl: TLabel;
    ET_FEC_FIN: TZetaDBFecha;
    ET_USR_ADMlbl: TLabel;
    ET_USR_ADM: TZetaDBKeyLookup_DevEx;
    ET_DOCUMENlbl: TLabel;
    ET_DOCUMEN: TDBEdit;
    ET_CONSOLAlbl: TLabel;
    ET_CONSOLA: TDBCheckBox;
    Escalas: TdxWizardControlPage;
    Criterios: TdxWizardControlPage;
    Evaluadores: TdxWizardControlPage;
    ET_ESC_DEFlbl: TLabel;
    ET_ESC_DEF: TZetaDBKeyLookup_DevEx;
    ET_ESCALAlbl: TLabel;
    ET_ESCALA: TDBEdit;
    ET_ESTILOlbl: TLabel;
    ET_ESTILO: TZetaDBKeyCombo;
    ET_ESC_PESlbl: TLabel;
    ET_ESC_PES: TZetaDBKeyLookup_DevEx;
    ET_PTS_CALlbl: TLabel;
    ET_PTS_CAL: TZetaDBKeyCombo;
    ET_ESC_CALlbl: TLabel;
    ET_ESC_CAL: TZetaDBKeyLookup_DevEx;
    ET_GET_COM: TDBCheckBox;
    ET_TXT_COM: TDBEdit;
    ET_TXT_COMlbl: TLabel;
    gbDisponibles: TGroupBox;
    gbSeleccionados: TGroupBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel7: TPanel;
    Label10: TLabel;
    zkcTipos: TZetaKeyCombo;
    Panel3: TPanel;
    gbSelected: TGroupBox;
    Panel4: TPanel;
    Bienvenida: TdxWizardControlPage;
    ET_MSG_FIN: TMemo;
    ET_MSG_INI: TMemo;
    ET_DOCUMENFind: TcxButton;
    zmlFuente: TZetaSmartListBox_DevEx;
    zmlDestino: TZetaSmartListBox_DevEx;
    btnRechazaCriterio: TZetaSmartListsButton_DevEx;
    btnEscogeCriterio: TZetaSmartListsButton_DevEx;
    ZSLCriterios: TZetaSmartLists_DevEx;
    btnSubir: TZetaSmartListsButton_DevEx;
    btnBajar: TZetaSmartListsButton_DevEx;
    btnSeleccionaTodos: TcxButton;
    btnRechazaTodos: TcxButton;
    gbAvailable: TGroupBox;
    lbEvalDisponibles: TZetaSmartListBox_DevEx;
    lbEvalSelecc: TZetaSmartListBox_DevEx;
    btnSubirEvaluador: TZetaSmartListsButton_DevEx;
    btnBajarEvaluador: TZetaSmartListsButton_DevEx;
    btnEscogerEvaluador: TZetaSmartListsButton_DevEx;
    btnRechazarEvaluador: TZetaSmartListsButton_DevEx;
    ZSLPerfiles: TZetaSmartLists_DevEx;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure ZSLCriteriosAlEscoger(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure ZSLCriteriosAlRechazar(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure ZSLCriteriosAlBajar(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure ZSLCriteriosAlSubir(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure ZSLPerfilesAlEscoger(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure ZSLPerfilesAlRechazar(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure ET_GET_COMClick(Sender: TObject);
    procedure zkcTiposChange(Sender: TObject);
    procedure btnEscogeCriterioClick(Sender: TObject);
    procedure btnSeleccionaTodosClick(Sender: TObject);
    procedure btnRechazaTodosClick(Sender: TObject);
    procedure btnRechazaCriterioClick(Sender: TObject);
    procedure ET_DOCUMENFindClick(Sender: TObject);
    procedure ET_PTS_CALClick(Sender: TObject);
   private
    { Private declarations }
    FTotalPaginas: Integer;
    FNumPregunta: Integer;
    FPreguntas: Integer;
    procedure DoConnect;
    procedure Connect;
    procedure SetControles;
    procedure SetControlesEscalas;
    procedure SetEscogieronCriterio;
    procedure LlenaDisponibles ( const sCodigoTipo: String );
    function VerificaCompetenciaEscogida( const sCodigoCompetencia: String ): Boolean;

  protected
    { Protected declarations }
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  AgregarEncuesta_DevEx: TAgregarEncuesta_DevEx;

implementation

{$R *.DFM}

uses DSistema,
     ZHelpContext,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaDialogo,
     FTressShell,
     DCliente,
     DProcesos;

{ TAgregarEncuesta }

const
     K_TCRITERIOS_EVAL = 0;

var FDisponibles : TCriteriosEval;

procedure TAgregarEncuesta_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FDisponibles := TCriteriosEval.Create;
     WizardControl.ActivePage := Parametros;
     ET_USR_ADM.LookupDataset := dmSistema.cdsUsuarios;
     ET_PTS_CAL.ListaFija := lfComoCalificar;
     ET_ESTILO.ListaFija := lfEstiloRespuesta;
     ET_FEC_INI.Valor := Date;
     ET_FEC_FIN.Valor := ET_FEC_INI.Valor;
     with dmEvaluacion do
     begin
          ET_ESC_DEF.LookupDataset := cdsEscalas;
          ET_ESC_PES.LookupDataset := cdsEscalas;
          ET_ESC_CAL.LookupDataSet := cdsEscalas;
     end;

     HelpContext := H_CATALOGOS_ENCUESTAS_AGREGAR_REGISTRO;
     btnEscogeCriterio.Hint := 'Seleccionar criterio';
     btnRechazaCriterio.Hint := 'Rechazar criterio';
     btnSubir.Hint := 'Subir criterio';
     btnBajar.Hint := 'Bajar criterio';
     btnEscogerEvaluador.Hint := 'Seleccionar evaluador';
     btnRechazarEvaluador.Hint := 'Rechazar evaluador';
     btnSubirEvaluador.Hint := 'Subir evaluador';
     btnBajarEvaluador.Hint := 'Bajar evaluador';
end;

procedure TAgregarEncuesta_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     DoConnect;
     ParametrosControl := ET_NOMBRE;
     zmlFuente.ItemIndex := 0;
     lbEvalDisponibles.ItemIndex := 0;
     zkcTipos.ItemIndex := 0;
     LlenaDisponibles( zkcTipos.Llaves[ zkcTipos.ItemIndex ] );
     SetControles;
     SetControlesEscalas;
end;

procedure TAgregarEncuesta_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     //DoDisconnect;
     Action := caHide;
end;

procedure TAgregarEncuesta_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FDisponibles );
end;

procedure TAgregarEncuesta_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmEvaluacion do
     begin
          cdsEscalas.Refrescar;
          cdsEncuestas.Conectar;
          DataSource.DataSet:= cdsEncuestas;
          LlenaTiposCompetencia( zkcTipos.Lista, TRUE, TRUE );
          LlenaCriterios( FDisponibles );
          LlenaEvaluadores( lbEvalDisponibles.Items );
     end;
end;

procedure TAgregarEncuesta_DevEx.DoConnect;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             try
                Connect;
             except
                   on Error: Exception do
                   begin
                        zExcepcion( '� Error Al Conectar Forma !', '� Ventana No Pudo Ser Conectada A Sus Datos !', Error, 0 );
                   end;
             end;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

{procedure TAgregarEncuesta_DevEx.Disconnect;
begin
     //Datasource.Dataset := nil; // @Gbeltran
end;      }

{ procedure TAgregarEncuesta_DevEx.DoDisconnect;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Disconnect;
          finally
                 Cursor := oCursor;
          end;
     end;
end; }

{ function TAgregarEncuesta_DevEx.Error( const sError: String; oControl: TWinControl ): Boolean;
begin
     ZetaDialogo.ZError( Caption, sError, 0 );
     oControl.SetFocus;
     Result := False;
end; }

procedure TAgregarEncuesta_DevEx.SetEscogieronCriterio;
begin
     with dmEvaluacion.cdsEncuestas do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
     end;
end;

procedure TAgregarEncuesta_DevEx.LLenaDisponibles( const sCodigoTipo: String );
var
   i: Integer;
   Elemento: TCriterioEval;
begin
     with zmlFuente.Items do
     begin
          Clear;
          BeginUpdate;
          try
             for i := 0 to ( FDisponibles.Count - 1 ) do
             begin
                  Elemento := FDisponibles.Criterio[ i ];
                  if ( sCodigoTipo = K_TODOS_CODIGO ) or ( sCodigoTipo = Elemento.TipoCriterio ) then
                  begin
                       if not VerificaCompetenciaEscogida( Elemento.Codigo ) then
                          AddObject( Elemento.Descripcion, Elemento );
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;
     btnEscogeCriterio.SetState;
end;

function TAgregarEncuesta_DevEx.VerificaCompetenciaEscogida( const sCodigoCompetencia: String ): Boolean;
var
   i: Integer;
begin
     Result := False;
     with zmlDestino.Items do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if ( TCriterioEval( Objects[ i ] ).Codigo = sCodigoCompetencia ) then
               begin
                    Result := True;
                    Break;
               end;
          end;
     end;
end;

procedure TAgregarEncuesta_DevEx.SetControles;
begin
     with ZSLCriterios do
     begin
          btnSeleccionaTodos.Enabled := HayElementosDisponibles;
          btnRechazaTodos.Enabled := HayElementosEscogidos;
          btnRechazaCriterio.Enabled := HayElementosEscogidos;
     end;
end;

procedure TAgregarEncuesta_DevEx.SetControlesEscalas;
begin
     ET_TXT_COMlbl.Enabled := ET_GET_COM.Checked;
     ET_TXT_COM.Enabled := ET_TXT_COMlbl.Enabled;
     ET_ESC_CALlbl.Enabled := ( eComoCalificar( ET_PTS_CAL.Valor ) <> eccaNoUsar );
     ET_ESC_CAL.Enabled := ET_ESC_CALlbl.Enabled;
end;

{ ********* Eventos del Wizard ************** }

procedure TAgregarEncuesta_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
          if CanMove and Adelante then
          begin
               if EsPaginaActual( Parametros ) then
               begin
                    if StrVacio( ET_NOMBRE.Text ) then
                       CanMove := Error( 'El nombre de la Encuesta no puede quedar vac�o', ET_NOMBRE )
                    else
                        if ( ET_FEC_INI.Valor > ET_FEC_FIN.Valor ) then
                           CanMove := Error( 'La fecha de inicio debe de ser menor que la fecha de fin de la Encuesta', ET_FEC_INI )
                    else
                        if StrVacio( ET_USR_ADM.Llave ) then
                           CanMove := Error( 'Se debe asignar un responsable de Encuesta', ET_USR_ADM )
               end;
               if  EsPaginaActual( Escalas ) then
               begin
                    if StrVacio( ET_ESC_DEF.Llave ) then
                       CanMove := Error( 'Se debe de asignar una escala a la encuesta', ET_ESC_DEF )
               end;
          end;
     end;
end;

procedure TAgregarEncuesta_DevEx.WizardAfterMove(Sender: TObject);
var
   sTitulo: String;
begin
     inherited;
     case WizardControl.ActivePageIndex of
          0:
          begin
               ParametrosControl := ET_NOMBRE;
          end;
          1:
          begin
               ParametrosControl:= ET_ESC_DEF;
               SetControlesEscalas;
          end;
          2:
          begin
               ParametrosControl := zmlFuente;
          end;
          3:
          begin
               ParametrosControl := lbEvalDisponibles;
          end;
          4:
          begin
               ParametrosControl := ET_MSG_INI;
          end;
     end;
end;

procedure TAgregarEncuesta_DevEx.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
   ErrorCount: Integer;
   Result : Boolean;
   ComentarioCheck : String;
begin
//     inherited;
    ComentarioCheck := 'N';
    if ET_GET_COM.Checked then
        ComentarioCheck := 'S';

     Result := dmProcesos.AgregaEncuesta( ParameterList, Verificacion );
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        Application.ProcessMessages;
        try
           Datasource.Dataset := nil; //Para que al Grabar no muestre la forma con Datos antes de cerrarse
           with dmEvaluacion do
           begin

                cdsEncuestas.FieldByName('ET_GET_COM').AsString := ComentarioCheck;

                if ComentarioCheck = 'N' then
                    cdsEncuestas.FieldByName('ET_TXT_COM').AsString := K_ESPACIO;

                cdsEncuestas.FieldByName('ET_MSG_INI').AsString := ET_MSG_INI.Text;
                cdsEncuestas.FieldByName('ET_MSG_FIN').AsString := ET_MSG_FIN.Text;

                CreaServerAgregaCriterios;
                CreaServerAgregaEvaluador;
                LlenaDataSetCriterios( zmlDestino.Items );
                LlenaDataSetEvaluadores( lbEvalSelecc.Items );

                cdsEncuestas.Enviar;
                cdsEncuestas.Refrescar;
                TressShell.SetUltimaEncuesta;
           end;
           lOk := True;
        except
              on Error: Exception do
              begin
                   zExcepcion( Caption, 'Error Al Ejecutar Wizard', Error, 0 );
                   lOk := False;
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     if lOk then     begin

        TerminarWizard;

        end;
end;


procedure TAgregarEncuesta_DevEx.WizardAlCancelar(Sender: TObject; var lOk: Boolean);
begin
     inherited;
     with dmEvaluacion do
     begin
          cdsEncuestas.Cancel;
     end;
     Close;
     lOk := True;
end;


//procedure TAgregarEncuesta_DevEx.EjecutarWizard;
//begin
///     if not Wizard.Reejecutar then
//     begin
//          Close;
//          Windows.SetForegroundWindow( Handle ); { Evita que la aplicaci�n pierda el Focus }
//     end;
//end;


procedure TAgregarEncuesta_DevEx.CargaParametros;
begin
     inherited CargaParametros;

     with Descripciones do
     begin
          AddString( 'Nombre', ET_NOMBRE.Text );
          AddString( 'Fecha inicial', ZetaCommonTools.FechaCorta( ET_FEC_INI.Valor ) );
          AddString( 'Fecha final', ZetaCommonTools.FechaCorta( ET_FEC_FIN.Valor ) );
          AddInteger( 'Administrador', ET_USR_ADM.Valor );
     end;

     with ParameterList do
     begin
      AddString( 'ET_NOMBRE', ET_NOMBRE.Text );
          AddString( 'ET_DOCUMEN', ET_DOCUMEN.Text );
          AddMemo( 'ET_DESCRIP', ET_DESCRIP.Text );
          AddDate( 'ET_FEC_INI', ET_FEC_INI.Valor );
          AddDate( 'ET_FEC_FIN', ET_FEC_FIN.Valor );
          AddInteger( 'ET_USR_ADM', ET_USR_ADM.Valor );
          AddInteger( 'ET_NUM_PRE', 0 ); // GA: Antes era FNumPreguntas �xq?
          AddMemo( 'ET_MSG_INI', ET_MSG_INI.Text );
          AddMemo( 'ET_MSG_FIN', ET_MSG_FIN.Text );
     end;
end;

{ ************ Eventos de Controles ************* }

procedure TAgregarEncuesta_DevEx.ZSLCriteriosAlEscoger(Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     SetEscogieronCriterio;
     SetControles;
end;

procedure TAgregarEncuesta_DevEx.ZSLCriteriosAlRechazar(Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     SetEscogieronCriterio;
     SetControles;
end;

procedure TAgregarEncuesta_DevEx.ZSLCriteriosAlBajar(Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     SetEscogieronCriterio;
end;

procedure TAgregarEncuesta_DevEx.ZSLCriteriosAlSubir(Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     SetEscogieronCriterio;
end;

procedure TAgregarEncuesta_DevEx.ET_GET_COMClick(Sender: TObject);
begin
     inherited;
     SetControlesEscalas;

end;

procedure TAgregarEncuesta_DevEx.ET_PTS_CALClick(Sender: TObject);
begin
     inherited;
     SetControlesEscalas;
end;

procedure TAgregarEncuesta_DevEx.zkcTiposChange(Sender: TObject);
begin
     inherited;
     LlenaDisponibles( zkcTipos.Llaves[ zkcTipos.ItemIndex ] );
     SetControles;
end;

procedure TAgregarEncuesta_DevEx.btnEscogeCriterioClick(Sender: TObject);
begin
     inherited;
     SetControles;
end;

procedure TAgregarEncuesta_DevEx.btnSeleccionaTodosClick(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     zmlFuente.Items.BeginUpdate;
     try
        with ZSLCriterios do
        begin
             for i:= 0 to ( zmlFuente.Items.Count - 1 ) do
             begin
                  SelectDisponible( i );
                  Escoger;
             end;
             ListaEscogidos.Sorted := True;
        end;
     finally
            zmlFuente.Items.EndUpdate;
     end;
     SetControles;
end;

procedure TAgregarEncuesta_DevEx.btnRechazaTodosClick(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     zmlDestino.Items.BeginUpdate;
     try
        with ZSLCriterios do
        begin
             for i :=0 to ( zmlDestino.Items.Count - 1 ) do
             begin
                  SelectEscogido( i );
                  Rechazar;
             end;
             ListaDisponibles.Sorted := True;
        end;
        LlenaDisponibles( zkcTipos.Llaves[ zkcTipos.ItemIndex ] );
     finally
            zmlDestino.Items.EndUpdate;
     end;
     SetControles;
end;

procedure TAgregarEncuesta_DevEx.ZSLPerfilesAlEscoger(Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     SetEscogieronCriterio;
end;

procedure TAgregarEncuesta_DevEx.ZSLPerfilesAlRechazar(Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     SetEscogieronCriterio;
end;

procedure TAgregarEncuesta_DevEx.btnRechazaCriterioClick(Sender: TObject);
begin
     inherited;
     zmlDestino.Items.BeginUpdate;
     try
        with ZSLCriterios do
        begin
             SelectEscogido( zmlDestino.ItemIndex );
             Rechazar;
             LlenaDisponibles( zkcTipos.Llaves[ zkcTipos.ItemIndex ] );
             SetControles;
             SetEscogieronCriterio;
        end;
     finally
            zmlDestino.Items.EndUpdate;
     end;
end;

procedure TAgregarEncuesta_DevEx.ET_DOCUMENFindClick(Sender: TObject);
begin
     inherited;
     with ET_DOCUMEN do
     begin
          with OpenDialog do
          begin
               FileName := Text;
               if Execute then
               begin
                    Text := ExtractFileName( FileName );
                    with dmEvaluacion do
                    begin
                         CambiarDocumentacion( cdsEncuestas, 'ET_DOCUMEN', ExtractFileName( FileName ) );
                    end;
               end;
          end;
     end;
end;

end.
