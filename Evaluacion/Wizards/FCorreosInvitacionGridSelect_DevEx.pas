unit FCorreosInvitacionGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseSelectGrid_DevEx, Db, StdCtrls, Grids, DBGrids, ZetaDBGrid, Buttons,
  ExtCtrls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, cxControls, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Vcl.ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons;

type
  TCorreosInvitacionGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    US_CODIGO: TcxGridDBColumn;
    US_NOMBRE: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CorreosInvitacionGridSelect_DevEx: TCorreosInvitacionGridSelect_DevEx;

implementation

{$R *.DFM}

end.
