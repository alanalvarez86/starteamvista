unit FWizPrepararEvaluaciones_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZcxBaseWizard,
  ZetaDBTextBox, StdCtrls, ComCtrls, Buttons, ZetaWizard,
  ExtCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel,
  cxGroupBox, dxCustomWizardControl, dxWizardControl;

type
  TWizPrepararEvaluaciones_DevEx = class(TcxBaseWizard)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    ET_CODIGO: TZetaTextBox;
    ET_NOMBRE: TZetaTextBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
  private
    { Private declarations }
  protected
    procedure CargaParametros; override;
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizPrepararEvaluaciones_DevEx: TWizPrepararEvaluaciones_DevEx;

implementation

{$R *.DFM}

uses ZHelpContext,
     ZetaDialogo,
     DCliente,
     DProcesos;

procedure TWizPrepararEvaluaciones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     //ParametrosControl := US_CODIGO;
     HelpContext := H_DISENO_ENCUESTA_PROCESOS_PREPARAR_EVALUACIONES;
     Parametros.PageIndex := 0;
     Ejecucion.PageIndex := 1;
end;

procedure TWizPrepararEvaluaciones_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          ET_CODIGO.Caption := InttoStr( Encuesta );
          ET_NOMBRE.Caption := Nombre;
     end;
     Advertencia.Caption := 'Se va a preparar la evaluaci'#243'n de acuerdo al valor activo seleccionado.'
end;

procedure TWizPrepararEvaluaciones_DevEx.CargaParametros;
begin
     inherited;
     with Descripciones do
     begin
          AddString( 'Encuesta', ET_CODIGO.Caption + ' = ' + ET_NOMBRE.Caption );
     end;



     with ParameterList do
     begin
          AddInteger( 'Encuesta', dmCliente.Encuesta );
     end;
end;

procedure TWizPrepararEvaluaciones_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if Wizard.EsPaginaActual( Parametros ) and Wizard.Adelante then
     begin
          if( dmCliente.Encuesta <= 0 )then
          begin
               CanMove := FALSE;
               ZetaDialogo.ZError( 'Preparar evaluaciones', '� No se han encontraron encuestas v�lidas !', 0 );
          end;
     end;
end;

function TWizPrepararEvaluaciones_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.PrepararEvaluaciones( ParameterList );
end;

end.
