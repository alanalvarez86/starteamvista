inherited WizCorreosInvitacion: TWizCorreosInvitacion
  Left = 306
  Top = 343
  Caption = 'Enviar invitaciones'
  ClientWidth = 448
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    Width = 448
    BeforeMove = WizardBeforeMove
    inherited Anterior: TZetaWizardButton
      Enabled = False
    end
    inherited Salir: TZetaWizardButton
      Left = 360
    end
    inherited Ejecutar: TZetaWizardButton
      Left = 277
    end
  end
  inherited PageControl: TPageControl
    Width = 448
    ActivePage = Parametros
    inherited Parametros: TTabSheet
      object lblRelacion: TLabel
        Left = 18
        Top = 144
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Relaci'#243'n:'
      end
      object GroupBox1: TGroupBox
        Left = 8
        Top = 9
        Width = 425
        Height = 105
        Caption = ' Datos de Encuesta: '
        TabOrder = 2
        object Label1: TLabel
          Left = 16
          Top = 28
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object Label2: TLabel
          Left = 16
          Top = 66
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
        end
        object ET_CODIGO: TZetaTextBox
          Left = 60
          Top = 26
          Width = 65
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ET_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object ET_NOMBRE: TZetaTextBox
          Left = 60
          Top = 64
          Width = 357
          Height = 17
          AutoSize = False
          Caption = 'ET_NOMBRE'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
      end
      object ENC_RELA: TZetaKeyCombo
        Left = 67
        Top = 140
        Width = 209
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
      end
      object gbEvaluadores: TGroupBox
        Left = 8
        Top = 188
        Width = 425
        Height = 56
        Caption = ' Selecciona evaluadores: '
        TabOrder = 1
        object lblLista: TLabel
          Left = 30
          Top = 27
          Width = 25
          Height = 13
          Alignment = taRightJustify
          Caption = 'Lista:'
        end
        object btnLista: TSpeedButton
          Left = 360
          Top = 21
          Width = 25
          Height = 25
          Hint = 'Invocar B'#250'squeda de Empleados'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33033333333333333F7F3333333333333000333333333333F777333333333333
            000333333333333F777333333333333000333333333333F77733333333333300
            033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
            33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
            3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
            33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
            333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
            333333773FF77333333333370007333333333333777333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = btnListaClick
        end
        object edLista: TZetaEdit
          Left = 60
          Top = 23
          Width = 296
          Height = 21
          LookUpBtn = btnLista
          TabOrder = 0
        end
      end
    end
    inherited FiltrosCondiciones: TTabSheet
      inherited FiltrosGB: TGroupBox
        Caption = ' Para Generar Lista de Evaluados '
        inherited GBRango: TGroupBox
          inherited BInicial: TSpeedButton
            Top = 48
            Height = 23
          end
        end
        inherited Seleccionar: TBitBtn
          Visible = True
        end
      end
    end
    object tsTipoCorreo: TTabSheet [2]
      ImageIndex = 3
      TabVisible = False
      object GroupBox2: TGroupBox
        Left = 8
        Top = 9
        Width = 425
        Height = 105
        Caption = ' Datos de Encuesta: '
        TabOrder = 0
        object Label3: TLabel
          Left = 16
          Top = 28
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object Label4: TLabel
          Left = 16
          Top = 66
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
        end
        object ET_CODIGO2: TZetaTextBox
          Left = 60
          Top = 26
          Width = 65
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ET_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object ET_NOMBRE2: TZetaTextBox
          Left = 60
          Top = 64
          Width = 357
          Height = 17
          AutoSize = False
          Caption = 'ET_NOMBRE'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
      end
      object rgEstiloCorreo: TRadioGroup
        Left = 136
        Top = 136
        Width = 161
        Height = 105
        Caption = ' Elegir estilo de correo: '
        ItemIndex = 0
        Items.Strings = (
          'Plantilla de correo'
          'Texto fijo')
        TabOrder = 1
        OnClick = rgEstiloCorreoClick
      end
    end
    object tsTipoPlantilla: TTabSheet [3]
      ImageIndex = 4
      TabVisible = False
      object lblTema: TLabel
        Left = 34
        Top = 132
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tema:'
      end
      object lblPlantilla: TLabel
        Left = 25
        Top = 172
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Plantilla:'
      end
      object btnDialogo: TSpeedButton
        Left = 399
        Top = 166
        Width = 25
        Height = 25
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          55555555FFFFFFFFFF55555000000000055555577777777775F55500B8B8B8B8
          B05555775F555555575F550F0B8B8B8B8B05557F75F555555575550BF0B8B8B8
          B8B0557F575FFFFFFFF7550FBF0000000000557F557777777777500BFBFBFBFB
          0555577F555555557F550B0FBFBFBFBF05557F7F555555FF75550F0BFBFBF000
          55557F75F555577755550BF0BFBF0B0555557F575FFF757F55550FB700007F05
          55557F557777557F55550BFBFBFBFB0555557F555555557F55550FBFBFBFBF05
          55557FFFFFFFFF7555550000000000555555777777777755555550FBFB055555
          5555575FFF755555555557000075555555555577775555555555}
        NumGlyphs = 2
        OnClick = btnDialogoClick
      end
      object GroupBox3: TGroupBox
        Left = 8
        Top = 9
        Width = 425
        Height = 105
        Caption = ' Datos de Encuesta: '
        TabOrder = 0
        object Label5: TLabel
          Left = 16
          Top = 28
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object Label6: TLabel
          Left = 16
          Top = 66
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
        end
        object ET_CODIGO3: TZetaTextBox
          Left = 60
          Top = 26
          Width = 65
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ET_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object ET_NOMBRE3: TZetaTextBox
          Left = 60
          Top = 64
          Width = 357
          Height = 17
          AutoSize = False
          Caption = 'ET_NOMBRE'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
      end
      object edTema: TEdit
        Left = 68
        Top = 128
        Width = 356
        Height = 21
        MaxLength = 255
        TabOrder = 1
      end
      object edPlantilla: TEdit
        Left = 68
        Top = 168
        Width = 328
        Height = 21
        MaxLength = 255
        TabOrder = 2
      end
    end
    object tsTipoTexto: TTabSheet [4]
      ImageIndex = 5
      TabVisible = False
      object gbMensaje: TGroupBox
        Left = 0
        Top = 97
        Width = 440
        Height = 185
        Align = alClient
        Caption = ' Mensaje: '
        TabOrder = 0
        object Mensaje: TMemo
          Left = 2
          Top = 15
          Width = 405
          Height = 168
          Align = alClient
          ScrollBars = ssVertical
          TabOrder = 0
        end
        object pnBoton: TPanel
          Left = 407
          Top = 15
          Width = 31
          Height = 168
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object btnArchivo: TSpeedButton
            Left = 4
            Top = 3
            Width = 25
            Height = 25
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000120B0000120B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
              55555555FFFFFFFFFF55555000000000055555577777777775F55500B8B8B8B8
              B05555775F555555575F550F0B8B8B8B8B05557F75F555555575550BF0B8B8B8
              B8B0557F575FFFFFFFF7550FBF0000000000557F557777777777500BFBFBFBFB
              0555577F555555557F550B0FBFBFBFBF05557F7F555555FF75550F0BFBFBF000
              55557F75F555577755550BF0BFBF0B0555557F575FFF757F55550FB700007F05
              55557F557777557F55550BFBFBFBFB0555557F555555557F55550FBFBFBFBF05
              55557FFFFFFFFF7555550000000000555555777777777755555550FBFB055555
              5555575FFF755555555557000075555555555577775555555555}
            NumGlyphs = 2
            OnClick = btnArchivoClick
          end
        end
      end
      object pnDatos: TPanel
        Left = 0
        Top = 0
        Width = 440
        Height = 97
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object lblTemaTexto: TLabel
          Left = 26
          Top = 12
          Width = 30
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tema:'
        end
        object edTemaTexto: TEdit
          Left = 60
          Top = 8
          Width = 356
          Height = 21
          MaxLength = 255
          TabOrder = 0
        end
        object rgFormatoCorreo: TRadioGroup
          Left = 61
          Top = 36
          Width = 161
          Height = 57
          Caption = ' Formato del correo: '
          ItemIndex = 0
          Items.Strings = (
            'Texto'
            'HTML')
          TabOrder = 1
          OnClick = rgEstiloCorreoClick
        end
      end
    end
    inherited Ejecucion: TTabSheet
      inherited Advertencia: TMemo
        Width = 440
      end
      inherited ProgressPanel: TPanel
        Width = 440
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Archivos XSL|*.xsl|Todos|*.*'
    Options = [ofReadOnly, ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 372
    Top = 48
  end
  object OpenDialog2: TOpenDialog
    Filter = 'Archivo Texto|*.txt|Archivo HTML|*.html|Todos|*.*'
    Options = [ofReadOnly, ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 404
    Top = 48
  end
end
