unit FWizCorreoInvitacion;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseWizardFiltro, ComCtrls, ZetaKeyLookup, ZetaEdit, StdCtrls, Buttons,
  ZetaWizard, ExtCtrls, ZetaDBTextBox, ZetaKeyCombo, ZBasicoSelectGrid;

type
  eTipoCorreo = ( tpPlantilla, tpTexto );
  eFormatoCorreo = ( fcTexto, fcHTML );
  
  TWizCorreosInvitacion = class(TBaseWizardFiltro)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    ET_CODIGO: TZetaTextBox;
    ET_NOMBRE: TZetaTextBox;
    lblRelacion: TLabel;
    ENC_RELA: TZetaKeyCombo;
    gbEvaluadores: TGroupBox;
    lblLista: TLabel;
    edLista: TZetaEdit;
    btnLista: TSpeedButton;
    tsTipoCorreo: TTabSheet;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    ET_CODIGO2: TZetaTextBox;
    ET_NOMBRE2: TZetaTextBox;
    rgEstiloCorreo: TRadioGroup;
    tsTipoPlantilla: TTabSheet;
    GroupBox3: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    ET_CODIGO3: TZetaTextBox;
    ET_NOMBRE3: TZetaTextBox;
    lblTema: TLabel;
    edTema: TEdit;
    lblPlantilla: TLabel;
    edPlantilla: TEdit;
    btnDialogo: TSpeedButton;
    OpenDialog1: TOpenDialog;
    tsTipoTexto: TTabSheet;
    gbMensaje: TGroupBox;
    pnDatos: TPanel;
    lblTemaTexto: TLabel;
    edTemaTexto: TEdit;
    rgFormatoCorreo: TRadioGroup;
    Mensaje: TMemo;
    OpenDialog2: TOpenDialog;
    pnBoton: TPanel;
    btnArchivo: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure BListaClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure BInicialClick(Sender: TObject);
    procedure BFinalClick(Sender: TObject);
    procedure btnListaClick(Sender: TObject);
    procedure rgEstiloCorreoClick(Sender: TObject);
    procedure btnDialogoClick(Sender: TObject);
    procedure btnArchivoClick(Sender: TObject);
    procedure SeleccionarClick(Sender: TObject);
  private
    { Private declarations }
    lChecaLista: Boolean;
    TipoDeCorreo: eTipoCorreo;
    FEvaluadoCodigo: String;
    procedure CargaDescripcionFinal;
    function BuscaEvaluado( const lConcatena: Boolean; const sLlave: String ): String;
    function BuscaEvaluador( const lConcatena: Boolean; const sLlave: String ): String;
    function ValidaSettingsTipoPlantilla: String;
    function ValidaSettingsTipoTexto: String;
  protected
    procedure CargaParametros; override;
    procedure CargaListaVerificacion; override;
    function Verificar: Boolean; override;
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizCorreosInvitacion: TWizCorreosInvitacion;

implementation

uses DCliente,
     DSistema,
     DProcesos,
     DEvaluacion,
     ZHelpContext,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZBaseSelectGrid,
     FCorreosInvitacionGridSelect;

{$R *.DFM}
procedure TWizCorreosInvitacion.FormCreate(Sender: TObject);
begin
     inherited;
     FEvaluadoCodigo := 'SUJETO.CB_CODIGO';
     HelpContext := H_DISENO_ENCUESTA_PROCESOS_ENVIA_CORREOS_INVITACION;
end;

procedure TWizCorreosInvitacion.FormShow(Sender: TObject);
begin
     inherited;
     lChecaLista := False;
     with dmCliente do
     begin
          ET_CODIGO.Caption := InttoStr( Encuesta );
          ET_NOMBRE.Caption := Nombre;
          ET_CODIGO2.Caption := ET_CODIGO.Caption;
          ET_NOMBRE2.Caption := ET_NOMBRE.Caption;
          ET_CODIGO3.Caption := ET_CODIGO.Caption;
          ET_NOMBRE3.Caption := ET_NOMBRE.Caption;
     end;
     with ENC_RELA do
     begin
          dmEvaluacion.LlenaPerfilEvaluadores( Lista, True );
          ItemIndex := 0;
     end;
     with dmEvaluacion do
     begin
          cdsEvaluadosLookup.Refrescar;
          cdsEvaluadoresLookup.Refrescar;
     end;
     rgEstiloCorreo.ItemIndex := Ord( tpPlantilla );
     edTema.Text := ET_NOMBRE.Caption;
     edTemaTexto.Text := ET_NOMBRE.Caption;
end;

procedure TWizCorreosInvitacion.BListaClick(Sender: TObject);
begin
     with ELista do
     begin
          Text := BuscaEvaluado( True, Text );
     end;
end;

function TWizCorreosInvitacion.BuscaEvaluado( const lConcatena: Boolean; const sLlave: String ): String;
var
   sKey, sDescripcion: String;
begin
     with dmEvaluacion.cdsEvaluadosLookup do
     begin
          if( Search( '', sKey, sDescripcion ) )then
          begin
               if lConcatena and ZetaCommonTools.StrLleno( Text ) then
               begin
                    if( strLleno( sLlave ) )then
                        Result := sLlave + ',' + sKey
                    else
                        Result := sKey;
               end
               else
                   Result := sKey;
          end;
     end;
end;

procedure TWizCorreosInvitacion.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   sMensaje: String;
begin
     inherited;
     if Wizard.Adelante then
     begin
          if ( PageControl.ActivePage = Parametros ) then
          begin
               dmEvaluacion.cdsPerfilEvaluador.Conectar;
               if( dmEvaluacion.cdsPerfilEvaluador.RecordCount <= 0 )then
                  CanMove := Error( '� No se tienen registrados perfiles de evaluadores !', Siguiente );
          end
          else if( PageControl.ActivePage = tsTipoCorreo )then
               begin
                    tsTipoPlantilla.Enabled := ( rgEstiloCorreo.ItemIndex = Ord( tpPlantilla ) );
                    tsTipoTexto.Enabled := not tsTipoPlantilla.Enabled;
               end
          else if( PageControl.ActivePage = tsTipoPlantilla )then
               begin
                    sMensaje := ValidaSettingsTipoPlantilla;
                    if( strLleno( sMensaje ) )then
                        CanMove := Error( sMensaje, Siguiente )
                    else
                        CargaDescripcionFinal;
               end
          else if( PageControl.ActivePage = tsTipoTexto )then
               begin
                    sMensaje := ValidaSettingsTipoTexto;
                    if( strLleno( sMensaje ) )then
                        CanMove := Error( sMensaje, Siguiente )
                    else
                        CargaDescripcionFinal;
               end;
     end;
end;

procedure TWizCorreosInvitacion.CargaDescripcionFinal;
var
    oCursor: TCursor;
begin
     if not( lChecaLista )then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             CargaParametros;
             dmProcesos.CorreosInvitacionGetLista( ParameterList );
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
     Advertencia.Clear;
     Advertencia.Lines.Add( Format( 'Advertencia'+ CR_LF+
                                    'Se enviar�n %d correos de invitaci�n para los' + CR_LF+
                                    'evaluadores seleccionados en la encuesta activa ' +CR_LF+
                                    CR_LF+'Presione el bot�n ''Ejecutar'' para iniciar el proceso ', [ dmProcesos.cdsDataset.RecordCount ] ) );
end;

function TWizCorreosInvitacion.ValidaSettingsTipoTexto: String;
begin
     Result := VACIO;
     if( strVacio( edTemaTexto.Text ) )then
         Result := 'El tema no puede quedar vac�o'
     else
         if( strVacio( Mensaje.Text ) )then
             Result := 'Es necesario que el mensaje tenga contenido';
end;

function TWizCorreosInvitacion.ValidaSettingsTipoPlantilla: String;
begin
     Result := VACIO;
     if( strVacio( edTema.Text ) )then
         Result := 'El tema no puede quedar vac�o'
     else
         if( strVacio( edPlantilla.Text ) )then
             Result := 'Es necesario seleccionar la plantilla a utilizar'
         else
             if( not FileExists( edPlantilla.Text ) )then
                 Result := 'Archivo de plantilla no existe';
end;

procedure TWizCorreosInvitacion.CargaParametros;
begin
     with Descripciones do
     begin
          AddString( 'Encuesta', ET_CODIGO.Caption + ' = ' + ET_NOMBRE.Caption );
          if( ENC_RELA.LlaveEntero > -1 )then
              AddString( 'Relacion', ENC_RELA.Descripcion )
          else
              AddString( 'Relacion', 'Todas' );
          if( strLleno( edLista.Text ) )then
              AddString( 'Evaluadores', edLista.Text );
          if( rgEstiloCorreo.ItemIndex = Ord( tpPlantilla ) )then
              AddString( 'Tipo de Correo', 'Plantilla' )
          else
              AddString( 'Tipo de Correo', 'Texto fijo' );

          {Descripciones para correos tipo plantilla}
          AddString( 'Tema Plantilla', edTema.Text );
          AddString( 'Plantilla', edPlantilla.Text );

          {Descripciones para correos tipo texto}
          AddString( 'Tema Texto', edTemaTexto.Text );
          if( rgFormatoCorreo.ItemIndex = Ord( fcTexto ) )then
              AddString( 'Formato', 'Texto fijo' )
          else
              AddString( 'Formato', 'HTML' );
          AddMemo( 'Mensaje', Mensaje.Text );
     end;

     inherited CargaParametros;

     with ParameterList do
     begin
          AddInteger( 'Encuesta', dmCliente.Encuesta );
          AddInteger( 'Relacion', ENC_RELA.LlaveEntero );
          if( ENC_RELA.LlaveEntero > -1 )then
              AddString( 'RelacionDesc', ENC_RELA.Descripcion )
          else
              AddString( 'RelacionDesc', 'Todas' );
          AddString( 'Evaluadores', edLista.Text );
          AddInteger( 'TipoCorreo', rgEstiloCorreo.ItemIndex );
          AddInteger( 'Responsable', dmCliente.Responsable );

          {Parametros para tipo de correo plantilla}
          AddString( 'TemaPlantilla', edTema.Text );
          AddString( 'Plantilla', edPlantilla.Text );

          {Parametros para tipo de correo texto fijo}
          AddString( 'TemaTexto', edTemaTexto.Text );
          if( rgEstiloCorreo.ItemIndex = Ord( tpPlantilla ) )then
              AddInteger( 'FormatoCorreo', Ord( fcHTML ) )
          else
              AddInteger( 'FormatoCorreo', rgFormatoCorreo.ItemIndex );
          AddMemo( 'Mensaje', Mensaje.Lines.Text );
     end;
end;

procedure TWizCorreosInvitacion.CargaListaVerificacion;
begin
     dmProcesos.CorreosInvitacionGetLista( ParameterList );
end;

function TWizCorreosInvitacion.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CorreosInvitacion( ParameterList, Verificacion );
end;

procedure TWizCorreosInvitacion.BInicialClick(Sender: TObject);
begin
     with EInicial do
     begin
          Text := BuscaEvaluado( False, Text );
     end;
end;

procedure TWizCorreosInvitacion.BFinalClick(Sender: TObject);
begin
     with EFinal do
     begin
          Text := BuscaEvaluado( False, Text );
     end;
end;

procedure TWizCorreosInvitacion.btnListaClick(Sender: TObject);
begin
     inherited;
     with edLista do
     begin
          Text := BuscaEvaluador( True, Text );
     end;
end;

function TWizCorreosInvitacion.BuscaEvaluador( const lConcatena: Boolean; const sLlave: String ): String;
var
   sKey, sDescripcion: String;
begin
     with dmEvaluacion.cdsEvaluadoresLookup do
     begin
          Search( '', sKey, sDescripcion );
          {
          if( Search( '', sKey, sDescripcion ) )then
          begin
          }
               if lConcatena and ZetaCommonTools.StrLleno( Text ) then
               begin
                    if( strLleno( sLlave ) )then
                        Result := sLlave + ',' + sKey
                    else
                        Result := sKey;
               end
               else
                   Result := sKey;
          //end;
     end;
end;

function TWizCorreosInvitacion.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid.GridSelect( dmProcesos.cdsDataset, TCorreosInvitacionGridSelect );
end;

procedure TWizCorreosInvitacion.rgEstiloCorreoClick(Sender: TObject);
begin
     inherited;
     TipoDeCorreo := eTipoCorreo( rgEstiloCorreo.ItemIndex );
end;

procedure TWizCorreosInvitacion.btnDialogoClick(Sender: TObject);
begin
     inherited;
     OpenDialog1.Title := 'Seleccionar plantilla';
     edPlantilla.Text := ZetaClientTools.AbreDialogo( OpenDialog1, edPlantilla.Text, 'xsl' );
end;

procedure TWizCorreosInvitacion.btnArchivoClick(Sender: TObject);
var
   sFileName: String;
begin
     inherited;
     OpenDialog2.Title := 'Seleccionar archivo';
     sFileName := ZetaClientTools.AbreDialogo( OpenDialog2, sFileName, 'txt' );
     if( strLleno( sFileName ) )then
     begin
          Mensaje.Lines.LoadFromFile( sFileName );
     end;
end;

procedure TWizCorreosInvitacion.SeleccionarClick(Sender: TObject);
begin
     inherited;
     lChecaLista := True;
end;

end.
