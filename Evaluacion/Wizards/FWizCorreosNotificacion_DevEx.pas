unit FWizCorreosNotificacion_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls,
     ZcxBaseWizard,
     ZetaWizard,
     ZetaKeyLookup,
     ZetaEdit,
     ZetaKeyCombo,
     ZetaDBTextBox, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, ZetaCXWizard, dxGDIPlusClasses,
  cxImage, cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl,
  Vcl.Menus, cxButtons, cxRadioGroup, ZetaKeyLookup_DevEx;

type
  TWizCorreosNotificacion_DevEx = class(TcxBaseWizard)
    GroupBox1: TcxGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    ET_CODIGO: TZetaTextBox;
    ET_NOMBRE: TZetaTextBox;
    gbEvaluadores: TcxGroupBox;
    lblLista: TLabel;
    GroupBox2: TcxGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    ET_CODIGO2: TZetaTextBox;
    ET_NOMBRE2: TZetaTextBox;
    rgEstiloCorreo: TcxRadioGroup;
    GroupBox3: TcxGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    ET_CODIGO3: TZetaTextBox;
    ET_NOMBRE3: TZetaTextBox;
    lblTema: TLabel;
    edTema: TEdit;
    edPlantilla: TEdit;
    lblPlantilla: TLabel;
    btnDialogo: TcxButton;
    pnDatos: TPanel;
    lblTemaTexto: TLabel;
    edTemaTexto: TEdit;
    rgFormatoCorreo: TcxRadioGroup;
    gbMensaje: TcxGroupBox;
    Mensaje: TMemo;
    pnBoton: TPanel;
    btnArchivo: TcxButton;
    OpenDialog1: TOpenDialog;
    OpenDialog2: TOpenDialog;
    ENC_RELA: TZetaKeyCombo;
    ENC_RELAlbl: TLabel;
    lblStatus: TLabel;
    lbStatus: TZetaKeyCombo;
    PorRelacionStatus: TcxRadioButton;
    PorLista: TcxRadioButton;
    tsTipoCorreo: TdxWizardControlPage;
    tsTipoPlantilla: TdxWizardControlPage;
    tsTipoTexto: TdxWizardControlPage;
    edLista: TZetaEdit;
    btnLista: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure btnDialogoClick(Sender: TObject);
    procedure btnArchivoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PorRelacionStatusClick(Sender: TObject);
    procedure PorListaClick(Sender: TObject);
    procedure btnListaClick(Sender: TObject);

  private
    procedure CargaDescripcionFinal;
    procedure ActivaControlesEvaluadores;{OP: 22.10.08}
    function BuscaEvaluador(const lConcatena: Boolean; const sLlave: String): String;
    function ValidaSettingsTipoPlantilla: String;
    function ValidaSettingsTipoTexto: String;
  protected
    procedure CargaParametros; override;
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizCorreosNotificacion_DevEx: TWizCorreosNotificacion_DevEx;

implementation

uses DCliente,
     DEvaluacion,
     DProcesos,
     DSistema,
     ZetaEvaluacionTools,
     ZHelpContext,
     ZetaDialogo,
     ZetaClientTools,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses;

{$R *.DFM}

procedure TWizCorreosNotificacion_DevEx.FormCreate(Sender: TObject);
var
 oLista : TStrings;
begin
     try
         inherited;
           //ZetaCommonLists.LlenaLista( lfeMailType, rgFormatoCorreo.Items );  Para lograr esto se creo el OLista y se le asignaron los valores del radiogroup.
         oLista := TStringList.Create;
         with oLista do
         begin
              BeginUpdate;
              oLista.Add( rgFormatoCorreo.Properties.Items.Items[0].Caption  );
              oLista.Add( rgFormatoCorreo.Properties.Items.Items[1].Caption  );
              EndUpdate;
         end;
         ZetaCommonLists.LlenaLista( lfeMailType, oLista); // DChavez : Verificar para que se usa

         HelpContext := H_DISENO_ENCUESTA_PROCESOS_ENVIA_CORREOS_NOTIFICACION;
         tsTipoTexto.PageVisible := False;
         tsTipoPlantilla.PageVisible:= False;
         Parametros.PageIndex := 0;
         tsTipoCorreo.PageIndex := 1;
         tstipoplantilla.PageIndex := 2;
         tstipotexto.PageIndex:=3;
         Ejecucion.PageIndex := 4;
     finally
           oLista.Free;
     end;
end;

procedure TWizCorreosNotificacion_DevEx.FormShow(Sender: TObject);
var
   eStatus: eStatusEvaluador;
begin
     inherited;
     with dmCliente do
     begin
          ET_CODIGO.Caption := InttoStr( Encuesta );
          ET_NOMBRE.Caption := Nombre;
          ET_CODIGO2.Caption := ET_CODIGO.Caption;
          ET_NOMBRE2.Caption := ET_NOMBRE.Caption;
          ET_CODIGO3.Caption := ET_CODIGO.Caption;
          ET_NOMBRE3.Caption := ET_NOMBRE.Caption;
     end;
     with ENC_RELA do
     begin
          dmEvaluacion.LlenaPerfilEvaluadores( Lista, True );
          ItemIndex := 0;
     end;
     with dmEvaluacion do
     begin
          cdsEvaluadoresLookup.Refrescar;
     end;
     rgEstiloCorreo.ItemIndex := Ord( tpPlantilla );
     with lbStatus do
     begin
          with Items do
          begin
               BeginUpdate;
               try
                  for eStatus := Low( eStatusEvaluador ) to High( eStatusEvaluador ) do
                  begin
                       Add( ZetaEvaluacionTools.GetStatusEvaluador( eStatus ) );
                  end;
               finally
                      EndUpdate;
               end;
          end;
          ItemIndex := 0;
     end;
     edTema.Text := ET_NOMBRE.Caption;
     edTemaTexto.Text := ET_NOMBRE.Caption;

     {OP : 22.10.08}
     {Se inicializan los controles de "Selecciona Evaluadores".}
     ActivaControlesEvaluadores;
end;

procedure TWizCorreosNotificacion_DevEx.CargaParametros;
const
     aCriterio: array[ False..True ] of String = ( 'Por Lista', 'Por Relacion' );
var
   eEstilo: eTipoCorreo;
   eFormato: eEmailType;
   sRelacion: String;
   lPorRelacion: Boolean;
begin
     inherited CargaParametros;
     lPorRelacion := PorRelacionStatus.Checked;
     eEstilo := eTipoCorreo( rgEstiloCorreo.ItemIndex );
     case eEstilo of
          tpPlantilla: eFormato := emtHTML;
     else
         eFormato := eEmailType( rgFormatoCorreo.ItemIndex );
     end;

     with Descripciones do
     begin
          Descripciones.Clear;
          AddString( 'Encuesta', ET_CODIGO.Caption + ' = ' + ET_NOMBRE.Caption );
          AddString( 'Criterio Seleccion', aCriterio[ lPorRelacion ] );
          if ( ENC_RELA.LlaveEntero < 0 ) then
             sRelacion := K_TODAS_DESCRIPCION
          else
              sRelacion := ENC_RELA.Descripcion;
          AddString( 'Relacion', sRelacion );
          AddString( 'Status', lbStatus.Items[ lbStatus.ItemIndex ] );
          if ( PorLista.Checked and Strlleno ( edLista.Text )  ) then
             AddString( 'Evaluadores', edLista.Text);
          AddString( 'Tipo de Correo', rgEstiloCorreo.Properties.Items.Items[ rgEstiloCorreo.ItemIndex ].Caption );
          if ( rgEstiloCorreo.ItemIndex = 0 ) then
          begin
               {Descripciones para correos tipo plantilla}
               AddString( 'Tema Plantilla', edTema.Text );
               AddString( 'Plantilla', edPlantilla.Text );
          end
          else
          begin
               {Descripciones para correos tipo texto}
               AddString( 'Tema Texto', edTemaTexto.Text );
               AddString( 'Formato', ZetaCommonLists.ObtieneElemento( lfeMailType, Ord( eFormato ) ) );
              //AddMemo( 'Mensaje', Mensaje.Text );
          end;
     end;


     with ParameterList do
     begin
          AddInteger( 'Encuesta', dmCliente.Encuesta );


          AddBoolean( 'PorRelacion', lPorRelacion );
          AddInteger( 'Relacion', ENC_RELA.LlaveEntero );
          AddString( 'RelacionDesc', sRelacion );

          AddInteger( 'Status', lbStatus.Valor );
          AddString( 'Evaluadores',  edLista.Text );

          AddInteger( 'TipoCorreo', Ord( eEstilo ) );
          AddInteger( 'Responsable', dmCliente.Responsable );

          {Parametros para tipo de correo plantilla}
          AddString( 'TemaPlantilla', edTema.Text );
          AddString( 'Plantilla', edPlantilla.Text );

          {Parametros para tipo de correo texto fijo}
          AddString( 'TemaTexto', edTemaTexto.Text );
          AddInteger( 'FormatoCorreo', Ord( eFormato ) );
          AddMemo( 'Mensaje', Mensaje.Lines.Text );
     end;
end;

function TWizCorreosNotificacion_DevEx.BuscaEvaluador( const lConcatena: Boolean; const sLlave: String ): String;
var
   sKey, sDescripcion: String;
begin
     with dmEvaluacion.cdsEvaluadoresLookup do
     begin
          Search( '', sKey, sDescripcion );
          {
          if ( Search( '', sKey, sDescripcion ) ) then
          begin
          }
               if lConcatena and ZetaCommonTools.StrLleno( Text ) then
               begin
                    if StrLleno( sLlave ) then
                       Result := sLlave + ',' + sKey
                    else
                        Result := sKey;
               end
               else
                   Result := sKey;
          //end;
     end;
end;



procedure TWizCorreosNotificacion_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
const
   K_TITULO = 'Enviar notificaciones';
var
   sMensaje: String;
begin
     inherited;
     if Wizard.Adelante then
     begin
          if ( Wizard.EsPaginaActual(  Parametros ) )then
          begin
               dmEvaluacion.cdsPerfilEvaluador.Conectar;
               if ( dmEvaluacion.cdsPerfilEvaluador.RecordCount <= 0 ) then
               begin
                    CanMove := FALSE;
                    ZetaDialogo.zError( K_TITULO, '� No se tienen registrados perfiles de evaluadores !', 0 );
               end;
               if PorLista.Checked and StrVacio(  edLista.Text ) then
               begin
                    CanMove := FALSE;
                    ZetaDialogo.zError( K_TITULO,'� No se captur� la lista de evaluadores !', 0 );
               end;
          end
          else
          if ( Wizard.EsPaginaActual( tsTipoCorreo ) ) then
          begin
               //tsTipoPlantilla.Enabled := ( rgEstiloCorreo.ItemIndex = Ord( tpPlantilla ) );
               //tsTipoTexto.Enabled := not tsTipoPlantilla.Visible:=FALSE;

               if( rgEstiloCorreo.ItemIndex = 0) then
               begin
                    tsTipoTexto.PageVisible:=FALSE;
                    tsTipoPlantilla.PageVisible:= TRUE;
                  //WizardControl.ActivePage :=  tsTipoPlantilla ;
               end
               else
               begin
                    tsTipoPlantilla.PageVisible := FALSE;
                    tsTipoTexto.PageVisible := TRUE;
                  //WizardControl.ActivePage :=  tsTipoTexto ;
               end;
          end
          else
          if ( Wizard.EsPaginaActual( tsTipoPlantilla ) ) then
          begin
               sMensaje := ValidaSettingsTipoPlantilla;
               if StrLleno( sMensaje ) then
               begin
                  CanMove := FALSE;
                  ZetaDialogo.zError( K_TITULO, sMensaje, 0 );
               end
               else
                   CargaDescripcionFinal;
          end
          else
          if ( Wizard.EsPaginaActual( tsTipoTexto ) ) then
          begin
               sMensaje := ValidaSettingsTipoTexto;
               if StrLleno( sMensaje ) then
               begin
                    CanMove := FALSE;
                    ZetaDialogo.zError( K_TITULO, sMensaje, 0 );
               end
               else
                   CargaDescripcionFinal;
          end;
     end;
end;

function TWizCorreosNotificacion_DevEx.ValidaSettingsTipoPlantilla: String;
begin
     Result := VACIO;
     if( strVacio( edTema.Text ) )then
         Result := '� El tema no puede quedar vac�o !'
     else
         if( strVacio( edPlantilla.Text ) )then
             Result := '� Es necesario seleccionar la plantilla a utilizar !'
         else
             if( not FileExists( edPlantilla.Text ) )then
                 Result := '� Archivo de plantilla no existe !';
end;

function TWizCorreosNotificacion_DevEx.ValidaSettingsTipoTexto: String;
begin
     Result := VACIO;
     if( strVacio( edTemaTexto.Text ) )then
         Result := '� El tema no puede quedar vac�o !'
     else
         if( strVacio( Mensaje.Text ) )then
             Result := '� Es necesario que el mensaje tenga contenido !';
end;

procedure TWizCorreosNotificacion_DevEx.CargaDescripcionFinal;
begin
     Advertencia.Clear;
     Advertencia.Caption:= 'Se enviar�n los correos de notificaci�n para los' + CR_LF+
                            'evaluadores seleccionados en la encuesta activa ';
end;

function TWizCorreosNotificacion_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CorreosNotificacion( ParameterList );
end;

procedure TWizCorreosNotificacion_DevEx.btnDialogoClick(Sender: TObject);
begin
     inherited;
     OpenDialog1.Title := 'Seleccionar plantilla';
     edPlantilla.Text := ZetaClientTools.AbreDialogo( OpenDialog1, edPlantilla.Text, 'xsl' );
end;

procedure TWizCorreosNotificacion_DevEx.btnArchivoClick(Sender: TObject);
var
   sFileName: String;
begin
     inherited;
     OpenDialog2.Title := 'Seleccionar archivo';
     sFileName := ZetaClientTools.AbreDialogo( OpenDialog2, sFileName, 'txt' );
     if( strLleno( sFileName ) )then
     begin
          Mensaje.Lines.LoadFromFile( sFileName );
     end;
end;

procedure TWizCorreosNotificacion_DevEx.PorRelacionStatusClick(Sender: TObject);
begin
     inherited;
     PorLista.Checked := False;
     ActivaControlesEvaluadores; {OP: 22.10.08}
end;

procedure TWizCorreosNotificacion_DevEx.PorListaClick(Sender: TObject);
begin
     inherited;
     PorRelacionStatus.Checked := False;
     ActivaControlesEvaluadores; {OP: 22.10.08}
end;

{OP: 22.10.08}
{ Se activan/desactivan controles en la secci�n de selecci�n de evaluador. }
{ Se utiliza como referencia el control de PorRelacionStatus para manejar
  el los siguientes casos: verdadero en el caso de seleccionar "Por Relaci�n
  y Status" y falso en caso de seleccionar "Por Lista".}
procedure TWizCorreosNotificacion_DevEx.ActivaControlesEvaluadores;
var
   lBandera: Boolean;
begin
     lBandera := PorRelacionStatus.Checked;
     lblStatus.Enabled := lBandera;
     lbStatus.Enabled := lBandera;
     ENC_RELAlbl.Enabled := lBandera;
     ENC_RELA.Enabled := lBandera;
     lblLista.Enabled := Not lBandera;
     edLista.Enabled := Not lBandera;
     btnLista.Enabled := Not lBandera;

     {Limpia el lookup de evaluadores, en el caso de ser verdadera la bandera}
     if lBandera then
        edLista.Valor := VACIO;
end;

procedure TWizCorreosNotificacion_DevEx.btnListaClick(Sender: TObject);
begin
     inherited;
     with edLista do
     begin
          Text := BuscaEvaluador( True, Text );
     end;
end;



end.
