unit FWizAgregarEncuesta;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComCtrls, StdCtrls, Buttons, ExtCtrls, Db, DBCtrls, Mask,
     DEvaluacion,
     ZWizardBasico,
     ZetaWizard,
     ZetaKeyLookup,
     ZetaFecha,
     ZetaKeyCombo,
     ZetaSmartLists;

type
  TAgregarEncuesta = class(TWizardBasico)
    tsGenerales: TTabSheet;
    ET_NOMBRElbl: TLabel;
    ET_DESCRIPlbl: TLabel;
    ET_FEC_INIlbl: TLabel;
    ET_FEC_FINlbl: TLabel;
    ET_USR_ADMlbl: TLabel;
    PanelTitulo1: TPanel;
    ET_NOMBRE: TDBEdit;
    ET_DESCRIP: TDBMemo;
    ET_FEC_FIN: TZetaDBFecha;
    ET_FEC_INI: TZetaDBFecha;
    ET_USR_ADM: TZetaDBKeyLookup;
    DataSource: TDataSource;
    tsEscalas: TTabSheet;
    ET_ESC_DEFlbl: TLabel;
    ET_ESC_PESlbl: TLabel;
    ET_GET_COM: TDBCheckBox;
    ET_TXT_COMlbl: TLabel;
    ET_TXT_COM: TDBEdit;
    ET_ESC_DEF: TZetaDBKeyLookup;
    ET_ESC_PES: TZetaDBKeyLookup;
    tsCriterios: TTabSheet;
    ZSLCriterios: TZetaSmartLists;
    tsEvaluadores: TTabSheet;
    ZSLPerfiles: TZetaSmartLists;
    tsBienvenida: TTabSheet;
    tsDespedida: TTabSheet;
    ET_MSG_FIN: TDBMemo;
    ET_MSG_INI: TDBMemo;
    Panel1: TPanel;
    btnSubir: TZetaSmartListsButton;
    btnBajar: TZetaSmartListsButton;
    gbSeleccionados: TGroupBox;
    zmlDestino: TZetaSmartListBox;
    Panel2: TPanel;
    btnEscogeCriterio: TZetaSmartListsButton;
    btnRechazaCriterioOld: TZetaSmartListsButton;
    gbDisponibles: TGroupBox;
    zmlFuente: TZetaSmartListBox;
    gbAvailable: TGroupBox;
    lbEvalDisponibles: TZetaSmartListBox;
    Panel3: TPanel;
    btnSelect: TZetaSmartListsButton;
    btnDeselect: TZetaSmartListsButton;
    gbSelected: TGroupBox;
    lbEvalSelecc: TZetaSmartListBox;
    Panel4: TPanel;
    btnRaise: TZetaSmartListsButton;
    btnLower: TZetaSmartListsButton;
    Panel7: TPanel;
    Label10: TLabel;
    zkcTipos: TZetaKeyCombo;
    btnSeleccionaTodos: TSpeedButton;
    btnRechazaTodos: TSpeedButton;
    btnRechazaCriterio: TSpeedButton;
    ET_DOCUMENlbl: TLabel;
    ET_DOCUMEN: TDBEdit;
    ET_DOCUMENFind: TSpeedButton;
    OpenDialog: TOpenDialog;
    ET_PTS_CALlbl: TLabel;
    ET_PTS_CAL: TZetaDBKeyCombo;
    ET_ESC_CAL: TZetaDBKeyLookup;
    ET_ESC_CALlbl: TLabel;
    ET_ESCALAlbl: TLabel;
    ET_ESCALA: TDBEdit;
    ET_ESTILOlbl: TLabel;
    ET_ESTILO: TZetaDBKeyCombo;
    ET_CONSOLA: TDBCheckBox;
    ET_CONSOLAlbl: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure ZSLCriteriosAlEscoger(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure ZSLCriteriosAlRechazar(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure ZSLCriteriosAlBajar(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure ZSLCriteriosAlSubir(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure ZSLPerfilesAlEscoger(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure ZSLPerfilesAlRechazar(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure ET_GET_COMClick(Sender: TObject);
    procedure zkcTiposChange(Sender: TObject);
    procedure btnEscogeCriterioClick(Sender: TObject);
    procedure btnSeleccionaTodosClick(Sender: TObject);
    procedure btnRechazaTodosClick(Sender: TObject);
    procedure btnRechazaCriterioClick(Sender: TObject);
    procedure ET_DOCUMENFindClick(Sender: TObject);
    procedure ET_PTS_CALClick(Sender: TObject);
  private
    { Private declarations }
    FDisponibles: TCriteriosEval;
    function VerificaCompetenciaEscogida( const sCodigoCompetencia: String ): Boolean;
    function Error( const sError: String; oControl: TWinControl ): Boolean;
    procedure Connect;
    procedure DoConnect;
    procedure Disconnect;
    procedure DoDisconnect;
    procedure SetEscogieronCriterio;
    procedure TerminarWizard;
    procedure LLenaDisponibles( const sCodigoTipo: String );
    procedure SetControles;
    procedure SetControlesEscalas;
  public
    { Public declarations }
  end;

var
  AgregarEncuesta: TAgregarEncuesta;

implementation

{$R *.DFM}

uses DSistema,
     ZHelpContext,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaDialogo,
     FTressShell,
     DCliente;

{ TAgregarEncuesta }

const
     K_TCRITERIOS_EVAL = 0;

procedure TAgregarEncuesta.FormCreate(Sender: TObject);
begin
     inherited;
     FDisponibles := TCriteriosEval.Create;
     PageControl.ActivePage := tsGenerales;
     ET_USR_ADM.LookupDataset := dmSistema.cdsUsuarios;
     ET_PTS_CAL.ListaFija := lfComoCalificar;
     ET_ESTILO.ListaFija := lfEstiloRespuesta;
     with dmEvaluacion do
     begin
          ET_ESC_DEF.LookupDataset := cdsEscalas;
          ET_ESC_PES.LookupDataset := cdsEscalas;
          ET_ESC_CAL.LookupDataSet := cdsEscalas;
     end;
     btnRechazaCriterio.Glyph :=  btnRechazaCriterioold.Glyph;
     HelpContext := H_CATALOGOS_ENCUESTAS_AGREGAR_REGISTRO;
end;

procedure TAgregarEncuesta.FormShow(Sender: TObject);
begin
     inherited;
     DoConnect;
     ET_NOMBRE.SetFocus;
     zmlFuente.ItemIndex := 0;
     lbEvalDisponibles.ItemIndex := 0;
     zkcTipos.ItemIndex := 0;
     LlenaDisponibles( zkcTipos.Llaves[ zkcTipos.ItemIndex ] );
     SetControles;
     SetControlesEscalas;
end;

procedure TAgregarEncuesta.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     DoDisconnect;
     Action := caHide;
end;

procedure TAgregarEncuesta.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FDisponibles );
end;

procedure TAgregarEncuesta.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmEvaluacion do
     begin
          cdsEscalas.Refrescar;
          cdsEncuestas.Conectar;
          DataSource.DataSet:= cdsEncuestas;
          LlenaTiposCompetencia( zkcTipos.Lista, TRUE, TRUE );
          LlenaCriterios( FDisponibles );
          LlenaEvaluadores( lbEvalDisponibles.Items );
     end;
end;

procedure TAgregarEncuesta.DoConnect;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             try
                Connect;
             except
                   on Error: Exception do
                   begin
                        zExcepcion( '� Error Al Conectar Forma !', '� Ventana No Pudo Ser Conectada A Sus Datos !', Error, 0 );
                   end;
             end;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

procedure TAgregarEncuesta.Disconnect;
begin
     Datasource.Dataset := nil;
end;

procedure TAgregarEncuesta.DoDisconnect;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Disconnect;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

function TAgregarEncuesta.Error( const sError: String; oControl: TWinControl ): Boolean;
begin
     ZetaDialogo.ZError( Caption, sError, 0 );
     oControl.SetFocus;
     Result := False;
end;

procedure TAgregarEncuesta.SetEscogieronCriterio;
begin
     with dmEvaluacion.cdsEncuestas do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
     end;
end;

procedure TAgregarEncuesta.LLenaDisponibles( const sCodigoTipo: String );
var
   i: Integer;
   Elemento: TCriterioEval;
begin
     with zmlFuente.Items do
     begin
          Clear;
          BeginUpdate;
          try
             for i := 0 to ( FDisponibles.Count - 1 ) do
             begin
                  Elemento := FDisponibles.Criterio[ i ];
                  if ( sCodigoTipo = K_TODOS_CODIGO ) or ( sCodigoTipo = Elemento.TipoCriterio ) then
                  begin
                       if not VerificaCompetenciaEscogida( Elemento.Codigo ) then
                          AddObject( Elemento.Descripcion, Elemento );
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;
     btnEscogeCriterio.SetState;
end;

function TAgregarEncuesta.VerificaCompetenciaEscogida( const sCodigoCompetencia: String ): Boolean;
var
   i: Integer;
begin
     Result := False;
     with zmlDestino.Items do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if ( TCriterioEval( Objects[ i ] ).Codigo = sCodigoCompetencia ) then
               begin
                    Result := True;
                    Break;
               end;
          end;
     end;
end;

procedure TAgregarEncuesta.SetControles;
begin
     with ZSLCriterios do
     begin
          btnSeleccionaTodos.Enabled := HayElementosDisponibles;
          btnRechazaTodos.Enabled := HayElementosEscogidos;
          btnRechazaCriterio.Enabled := HayElementosEscogidos;
     end;
end;

procedure TAgregarEncuesta.SetControlesEscalas;
begin
     ET_TXT_COMlbl.Enabled := ET_GET_COM.Checked;
     ET_TXT_COM.Enabled := ET_TXT_COMlbl.Enabled;
     ET_ESC_CALlbl.Enabled := ( eComoCalificar( ET_PTS_CAL.Valor ) <> eccaNoUsar );
     ET_ESC_CAL.Enabled := ET_ESC_CALlbl.Enabled;
end;

{ ********* Eventos del Wizard ************** }

procedure TAgregarEncuesta.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
          if CanMove and Adelante then
          begin
               if EsPaginaActual( tsGenerales ) then
               begin
                    if StrVacio( ET_NOMBRE.Text ) then
                       CanMove := Error( 'El nombre de la Encuesta no puede quedar vac�o', ET_NOMBRE )
                    else
                        if ( ET_FEC_INI.Valor > ET_FEC_FIN.Valor ) then
                           CanMove := Error( 'La fecha de inicio debe de ser menor que la fecha de fin de la Encuesta', ET_FEC_INI )
                    else
                        if StrVacio( ET_USR_ADM.Llave ) then
                           CanMove := Error( 'Se debe asignar un responsable de Encuesta', ET_USR_ADM )
               end;
               if  EsPaginaActual( tsEscalas ) then
               begin
                    if StrVacio( ET_ESC_DEF.Llave ) then
                       CanMove := Error( 'Se debe de asignar una escala a la encuesta', ET_ESC_DEF )
               end;
          end;
     end;
end;

procedure TAgregarEncuesta.WizardAfterMove(Sender: TObject);
var
   sTitulo: String;
begin
     inherited;
     case PageControl.ActivePageIndex of
          0:
          begin
               sTitulo := 'Generales';
               ET_NOMBRE.SetFocus;
          end;
          1:
          begin
               sTitulo := 'Escalas';
               ET_ESC_DEF.SetFocus;
               SetControlesEscalas;
          end;
          2:
          begin
               sTitulo := 'Criterios a evaluar';
               zmlFuente.SetFocus;
          end;
          3:
          begin
               sTitulo := 'Evaluadores';
               lbEvalDisponibles.SetFocus;
          end;
          4:
          begin
               sTitulo := 'Bienvenida';
               ET_MSG_INI.SetFocus;
          end;
          5:
          begin
               sTitulo := 'Despedida';
               ET_MSG_FIN.SetFocus;
          end;
     else
         sTitulo := VACIO;
     end;
     PanelTitulo1.Caption := sTitulo;
end;

procedure TAgregarEncuesta.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        Application.ProcessMessages;
        try
           Datasource.Dataset := nil; //Para que al Grabar no muestre la forma con Datos antes de cerrarse
           with dmEvaluacion do
           begin
                CreaServerAgregaCriterios;
                CreaServerAgregaEvaluador;
                LlenaDataSetCriterios( zmlDestino.Items );
                LlenaDataSetEvaluadores( lbEvalSelecc.Items );
                cdsEncuestas.Enviar;
                cdsEncuestas.Refrescar;
                TressShell.SetUltimaEncuesta;
           end;
           lOk := True;
        except
              on Error: Exception do
              begin
                   zExcepcion( Caption, 'Error Al Ejecutar Wizard', Error, 0 );
                   lOk := False;
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     if lOk then
        TerminarWizard;
end;

procedure TAgregarEncuesta.WizardAlCancelar(Sender: TObject; var lOk: Boolean);
begin
     inherited;
     with dmEvaluacion do
     begin
          cdsEncuestas.Cancel;
     end;
     Close;
     lOk := True;
end;

procedure TAgregarEncuesta.TerminarWizard;
begin
     if not Wizard.Reejecutar then
     begin
          Close;
          Windows.SetForegroundWindow( Handle ); { Evita que la aplicaci�n pierda el Focus }
     end;
end;

{ ************ Eventos de Controles ************* }

procedure TAgregarEncuesta.ZSLCriteriosAlEscoger(Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     SetEscogieronCriterio;
     SetControles;
end;

procedure TAgregarEncuesta.ZSLCriteriosAlRechazar(Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     SetEscogieronCriterio;
     SetControles;
end;

procedure TAgregarEncuesta.ZSLCriteriosAlBajar(Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     SetEscogieronCriterio;
end;

procedure TAgregarEncuesta.ZSLCriteriosAlSubir(Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     SetEscogieronCriterio;
end;

procedure TAgregarEncuesta.ET_GET_COMClick(Sender: TObject);
begin
     inherited;
     SetControlesEscalas;
end;

procedure TAgregarEncuesta.ET_PTS_CALClick(Sender: TObject);
begin
     inherited;
     SetControlesEscalas;
end;

procedure TAgregarEncuesta.zkcTiposChange(Sender: TObject);
begin
     inherited;
     LlenaDisponibles( zkcTipos.Llaves[ zkcTipos.ItemIndex ] );
     SetControles;
end;

procedure TAgregarEncuesta.btnEscogeCriterioClick(Sender: TObject);
begin
     inherited;
     SetControles;
end;

procedure TAgregarEncuesta.btnSeleccionaTodosClick(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     zmlFuente.Items.BeginUpdate;
     try
        with ZSLCriterios do
        begin
             for i:= 0 to ( zmlFuente.Items.Count - 1 ) do
             begin
                  SelectDisponible( i );
                  Escoger;
             end;
             ListaEscogidos.Sorted := True;
        end;
     finally
            zmlFuente.Items.EndUpdate;
     end;
     SetControles;
end;

procedure TAgregarEncuesta.btnRechazaTodosClick(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     zmlDestino.Items.BeginUpdate;
     try
        with ZSLCriterios do
        begin
             for i :=0 to ( zmlDestino.Items.Count - 1 ) do
             begin
                  SelectEscogido( i );
                  Rechazar;
             end;
             ListaDisponibles.Sorted := True;
        end;
        LlenaDisponibles( zkcTipos.Llaves[ zkcTipos.ItemIndex ] );
     finally
            zmlDestino.Items.EndUpdate;
     end;
     SetControles;
end;

procedure TAgregarEncuesta.ZSLPerfilesAlEscoger(Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     SetEscogieronCriterio;
end;

procedure TAgregarEncuesta.ZSLPerfilesAlRechazar(Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     SetEscogieronCriterio;
end;

procedure TAgregarEncuesta.btnRechazaCriterioClick(Sender: TObject);
begin
     inherited;
     zmlDestino.Items.BeginUpdate;
     try
        with ZSLCriterios do
        begin
             SelectEscogido( zmlDestino.ItemIndex );
             Rechazar;
             LlenaDisponibles( zkcTipos.Llaves[ zkcTipos.ItemIndex ] );
             SetControles;
             SetEscogieronCriterio;
        end;
     finally
            zmlDestino.Items.EndUpdate;
     end;
end;

procedure TAgregarEncuesta.ET_DOCUMENFindClick(Sender: TObject);
begin
     inherited;
     with ET_DOCUMEN do
     begin
          with OpenDialog do
          begin
               FileName := Text;
               if Execute then
               begin
                    with dmEvaluacion do
                    begin
                         CambiarDocumentacion( cdsEncuestas, 'ET_DOCUMEN', ExtractFileName( FileName ) );
                    end;
                    ET_DOCUMEN.SetFocus;
               end;
          end;
     end;
end;

end.
