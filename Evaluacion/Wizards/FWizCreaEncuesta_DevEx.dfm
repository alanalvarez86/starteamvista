inherited WizCreaEncuesta_DevEx: TWizCreaEncuesta_DevEx
  Left = 504
  Top = 339
  Caption = 'Crear Encuesta'
  ClientHeight = 422
  ClientWidth = 517
  ExplicitWidth = 523
  ExplicitHeight = 451
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 517
    Height = 422
    Header.AssignedValues = [wchvDescriptionFont, wchvGlyph, wchvTitleFont]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F40000000467414D410000B18F0BFC6105000000097048597300000EC300000E
      C301C76FA8640000021D494441545847ED5631481C4114FD1763AE884969BC26
      285A499A4084040C182CB4B015B44B61616F6337B706D1DDD9237745A2428860
      235CF0DCDD421102290E116C441B110B95A87BA782658A149BFFD73FE770B922
      70DE82641F3CB8F9F3FFBCB77F66670FFE1B945DE82AB9B0E4BBD0C1A16881C2
      1FCA1E04443272B5064F792A3A5CAC400A8D7C5746CE1DE8E6A9C6819EB45C80
      5E1E86B870E02576E1F8DC831E0E350ED46E6E7BF1CC83E71C8620800491878D
      011D3CD56EC592039FFC0D78CC298D033D9DEFC06EB501E2A50B139C069049CB
      C5BBA3B5806816423C2879D05F4B1CB7E23A5880669607B00D19DC2551FCD15A
      6E304942350DA03196BE8156BC9311B3AF6D618E556253F69C14F2859D369755
      0CC723B6B07BF1F7CF4A9EC65C2E97F49DA67768E0A05ADC77137B7F1D3C5528
      8539CE21B00C739F6296B0DA681C9AA0B1616D850988CC9494AA5667763AFB8C
      E6E9C6F3BD44160FDD2F65E0A490EC0C8B75A8425AFCA398690F9F9063244226
      A8131CFB8D1D1A2243987FA8F2746E7C7ED39ACF0F37D159A0F5B113A3F8366C
      97BCC417DC9E87A1A88E5A8BD4C3D3D5643F8A1ED336A87693A1F5F98114CD87
      A23AB4625F1AF2A85E9E7C6B795BD9737C0DE9D6334DF389D261D95BA8096C6B
      1F87EA827EF960279636BFBEEA511A9118A0C3577660527DED6C61BD8FD44035
      6203B181D8406CE01E1930E47AEDBF59F5113FDBC57F32101559F616B661FE88
      922C1B8301F007F89CBA05186775CB0000000049454E44AE426082}
    ParentFont = False
    ExplicitWidth = 517
    ExplicitHeight = 422
    inherited Parametros: TdxWizardControlPage
      Header.AssignedValues = [wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'Proceso que crea la encuesta con el n'#250'mero de preguntas indicada' +
        's en las fechas que se hayan seleccionado.'
      Header.Title = 'Informaci'#243'n General de la Encuesta'
      ParentFont = False
      ExplicitWidth = 495
      ExplicitHeight = 280
      object ET_DOCUMENlbl: TLabel
        Left = 5
        Top = 254
        Width = 78
        Height = 13
        Alignment = taRightJustify
        Caption = 'Documentaci'#243'n:'
      end
      object ET_NUM_PRElbl: TLabel
        Left = 12
        Top = 161
        Width = 71
        Height = 13
        Alignment = taRightJustify
        Caption = 'No. Preguntas:'
      end
      object ET_DESCRIPlbl: TLabel
        Left = 24
        Top = 32
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n:'
      end
      object ET_NOMBRElbl: TLabel
        Left = 43
        Top = 9
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nombre:'
      end
      object ET_FEC_INIlbl: TLabel
        Left = 55
        Top = 185
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Inicio:'
      end
      object ET_FEC_FINlbl: TLabel
        Left = 58
        Top = 207
        Width = 25
        Height = 13
        Alignment = taRightJustify
        Caption = 'Final:'
      end
      object ET_USR_ADMlbl: TLabel
        Left = 18
        Top = 231
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = 'Responsable:'
      end
      object ET_DOCUMENFind: TcxButton
        Left = 467
        Top = 251
        Width = 21
        Height = 21
        Hint = 'Buscar archivo de documentaci'#243'n de la encuesta'
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A40500000000000000000000000000000000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFDFF5
          FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
          FCFFDFF5FCFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF10B7EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF40C5EFFF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF40C5EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6CD3
          F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF6CD3F2FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF93DEF6FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF97E0F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFEBF9FDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF54CB
          F0FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CF
          F1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF80D9F4FF80D9
          F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC3EDFAFFDFF5FCFFDFF5FCFFDFF5
          FCFF70D4F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        OnClick = ET_DOCUMENFindClick
      end
      object ET_DESCRIP: TMemo
        Left = 86
        Top = 30
        Width = 401
        Height = 124
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 255
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 1
      end
      object ET_NOMBRE: TEdit
        Left = 86
        Top = 6
        Width = 401
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 40
        ParentFont = False
        TabOrder = 0
      end
      object ET_FEC_INI: TZetaFecha
        Left = 86
        Top = 179
        Width = 105
        Height = 22
        Cursor = crArrow
        TabOrder = 3
        Text = '18/ago./05'
        Valor = 38582.000000000000000000
      end
      object ET_DOCUMEN: TEdit
        Left = 86
        Top = 251
        Width = 379
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
      end
      object ET_FEC_FIN: TZetaFecha
        Left = 86
        Top = 203
        Width = 105
        Height = 22
        Cursor = crArrow
        TabOrder = 4
        Text = '18/ago./05'
        Valor = 38582.000000000000000000
      end
      object ET_USR_ADM: TZetaKeyLookup_DevEx
        Left = 86
        Top = 227
        Width = 404
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 70
      end
      object ET_NUM_PRE: TZetaNumero
        Left = 86
        Top = 156
        Width = 49
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Mascara = mnDias
        MaxLength = 2
        ParentFont = False
        TabOrder = 2
        Text = '0'
        OnExit = ET_NUM_PREExit
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      Header.AssignedValues = [wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      ParentFont = False
      ExplicitWidth = 495
      ExplicitHeight = 280
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 495
        ExplicitHeight = 185
        Height = 185
        Width = 495
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 495
        Width = 495
        inherited Advertencia: TcxLabel
          Caption = 
            'Al aplicar el proceso se crear'#225' la encuesta de acuerdo con los p' +
            'ar'#225'metros que se especificaron.'
          Style.IsFontAssigned = True
          ExplicitLeft = 70
          ExplicitWidth = 423
          Width = 423
          AnchorY = 49
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      Header.Title = 'Participantes'
      ParentFont = False
      ExplicitWidth = 495
      ExplicitHeight = 280
      inherited sCondicionLBl: TLabel
        Left = 40
        Top = 133
        ExplicitLeft = 40
        ExplicitTop = 133
      end
      inherited sFiltroLBL: TLabel
        Left = 65
        Top = 162
        ExplicitLeft = 65
        ExplicitTop = 162
      end
      inherited Seleccionar: TcxButton
        Left = 163
        Top = 250
        Visible = True
        ExplicitLeft = 163
        ExplicitTop = 250
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 91
        Top = 129
        ExplicitLeft = 91
        ExplicitTop = 129
      end
      inherited sFiltro: TcxMemo
        Left = 90
        Top = 161
        Style.IsFontAssigned = True
        ExplicitLeft = 90
        ExplicitTop = 161
        ExplicitWidth = 315
        Width = 315
      end
      inherited GBRango: TGroupBox
        Left = 91
        Top = 5
        Width = 313
        ExplicitLeft = 91
        ExplicitTop = 5
        ExplicitWidth = 313
      end
      inherited bAjusteISR: TcxButton
        Left = 408
        Top = 161
        ExplicitLeft = 408
        ExplicitTop = 161
      end
    end
    object tsBienvenida: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.DescriptionFont.Charset = DEFAULT_CHARSET
      Header.DescriptionFont.Color = clDefault
      Header.DescriptionFont.Height = -11
      Header.DescriptionFont.Name = 'Tahoma'
      Header.DescriptionFont.Style = []
      Header.TitleFont.Charset = DEFAULT_CHARSET
      Header.TitleFont.Color = clDefault
      Header.TitleFont.Height = -11
      Header.TitleFont.Name = 'Tahoma'
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'Redactar el texto que ser'#225' utilizado para presentarse al momento' +
        ' de iniciar y terminar la encuesta.'
      Header.Title = 'Captura de textos de Bienvenida y Despedida'
      ParentFont = False
      object Label1: TLabel
        Left = 3
        Top = 3
        Width = 56
        Height = 13
        Caption = 'Bienvenida:'
      end
      object Label2: TLabel
        Left = 257
        Top = 3
        Width = 54
        Height = 13
        Caption = 'Despedida:'
      end
      object ET_MSG_FIN: TMemo
        Left = 257
        Top = 22
        Width = 235
        Height = 250
        Align = alCustom
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 1
      end
      object ET_MSG_INI: TMemo
        Left = 3
        Top = 22
        Width = 235
        Height = 250
        Align = alCustom
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object tsPregx: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.DescriptionFont.Charset = DEFAULT_CHARSET
      Header.DescriptionFont.Color = clDefault
      Header.DescriptionFont.Height = -11
      Header.DescriptionFont.Name = 'Tahoma'
      Header.DescriptionFont.Style = []
      Header.TitleFont.Charset = DEFAULT_CHARSET
      Header.TitleFont.Color = clDefault
      Header.TitleFont.Height = -11
      Header.TitleFont.Name = 'Tahoma'
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'Elaborar el texto de cada una de pregunta con su respectiva resp' +
        'uesta.'
      Header.Title = 'Crear Pregunta #1'
      ParentFont = False
      object Label3: TLabel
        Left = 3
        Top = 3
        Width = 148
        Height = 13
        Caption = 'Capture el texto de la pregunta:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 257
        Top = 3
        Width = 59
        Height = 13
        Caption = 'Respuestas:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object EP_DESCRIP: TMemo
        Left = 3
        Top = 22
        Width = 235
        Height = 250
        Align = alCustom
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 255
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 0
      end
      object EL_NOMBRE: TMemo
        Left = 257
        Top = 22
        Width = 235
        Height = 250
        Align = alCustom
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 1
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 16
    Top = 412
  end
  object OpenDialog: TOpenDialog
    Filter = 
      'Adobe Acrobat (*.pdf)|*.pdf|Microsoft Word (*.doc)|*.doc|Paginas' +
      ' Web (*.htm)|*.htm|Todos los archivos (*.*)|*.*'
    FilterIndex = 0
    Title = 'Buscar documentacii'#243'n'
    Left = 52
    Top = 409
  end
end
