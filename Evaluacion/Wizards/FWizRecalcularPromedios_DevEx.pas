unit FWizRecalcularPromedios_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZcxBaseWizard,
  ComCtrls, StdCtrls, Buttons, ZetaWizard, ExtCtrls,
  ZetaDBTextBox, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel,
  cxGroupBox, dxCustomWizardControl, dxWizardControl;

type
  TWizRecalcularPromedios_DevEx = class(TcxBaseWizard)
    GroupBox1: TcxGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    ET_CODIGO: TZetaTextBox;
    ET_NOMBRE: TZetaTextBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
  private
    { Private declarations }
  protected
    procedure CargaParametros; override;
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizRecalcularPromedios_DevEx: TWizRecalcularPromedios_DevEx;

implementation

{$R *.DFM}

uses ZHelpContext,
     DCliente,
     ZetaDialogo,
     DProcesos;

procedure TWizRecalcularPromedios_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     //ParametrosControl := US_CODIGO;
     HelpContext := H_DISENO_ENCUESTA_PROCESOS_RECALCULAR_PROMEDIOS;
end;

procedure TWizRecalcularPromedios_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          ET_CODIGO.Caption := InttoStr( Encuesta );
          ET_NOMBRE.Caption := Nombre;
     end;
     Advertencia.Caption:= 'Se calcular� el resultado final de la encuesta en base la informaci�n obtenida por los evaluadores.'
end;

procedure TWizRecalcularPromedios_DevEx.CargaParametros;
begin
     inherited CargaParametros;

     with Descripciones do
     begin
          AddString( 'Encuesta', ET_CODIGO.Caption + ' = ' + ET_NOMBRE.Caption );
     end;

     with ParameterList do
     begin
          AddInteger( 'Encuesta', dmCliente.Encuesta );
     end;
end;

procedure TWizRecalcularPromedios_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if Wizard.EsPaginaActual( Parametros ) and Wizard.Adelante then
     begin
          if( dmCliente.Encuesta <= 0 )then
          begin
               CanMove := FALSE;
               ZetaDialogo.ZError( 'Calcular resultados', '� No se han encontrado encuestas v�lidas !', 0 );
          end;
     end;
end;

function TWizRecalcularPromedios_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.RecalcularPromedios( ParameterList );
end;

end.
