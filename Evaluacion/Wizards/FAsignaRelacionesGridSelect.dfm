inherited AsignaRelacionesGridSelect: TAsignaRelacionesGridSelect
  Width = 523
  Caption = 'Selecciona relaciones'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 515
    inherited OK: TBitBtn
      Left = 358
      Anchors = [akTop, akRight]
    end
    inherited Cancelar: TBitBtn
      Left = 436
      Anchors = [akTop, akRight]
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 515
    Columns = <
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        PickList.Strings = ()
        Title.Caption = 'N�mero'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRETTYNAME'
        PickList.Strings = ()
        Title.Caption = 'Nombre'
        Width = 350
        Visible = True
      end>
  end
  inherited PanelSuperior: TPanel
    Width = 515
  end
end
