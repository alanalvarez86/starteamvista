inherited WizCierraEvaluaciones: TWizCierraEvaluaciones
  Left = 374
  Top = 319
  Caption = 'Cierra Evaluaciones/Encuestas'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    inherited Siguiente: TZetaWizardButton
      Enabled = False
    end
    inherited Ejecutar: TZetaWizardButton
      Enabled = True
    end
  end
  inherited PageControl: TPageControl
    ActivePage = Ejecucion
    Visible = False
    inherited Ejecucion: TTabSheet
      object btnVerifica: TBitBtn
        Left = 129
        Top = 252
        Width = 169
        Height = 25
        Hint = 'Invocar Pantalla de Selecci�n de Empleados'
        Caption = '&Verificar Lista de Encuestas'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        Visible = False
        OnClick = SeleccionarClick
        Glyph.Data = {
          42010000424D4201000000000000760000002800000011000000110000000100
          040000000000CC00000000000000000000001000000010000000000000000000
          BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
          77777000000070000000000007777000000070FFFFFFFFFF07777000000070FC
          CFCCCCCF07777000000070FFFFFFFFFF07777000000070FCCFCCCCCF07777000
          000070FFFFFFFFFF07777000000070FFFFFFF0FF07777000000070F00FFF0B0F
          07770000000070F0F0F0B0F000700000000070FF0B0B0F0FBF00000000007000
          00F0F0FBFBF0000000007777770B0FBFBFB00000000077777770FBFBFB000000
          0000777777770000007000000000777777777777777770000000777777777777
          777770000000}
      end
    end
  end
end
