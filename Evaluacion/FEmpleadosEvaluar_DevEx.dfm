inherited EmpleadosEvaluar_DevEx: TEmpleadosEvaluar_DevEx
  Left = 423
  Top = 450
  Caption = 'Empleados a evaluar'
  ClientHeight = 270
  ClientWidth = 674
  ExplicitWidth = 674
  ExplicitHeight = 270
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 674
    ExplicitWidth = 674
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 415
      ExplicitWidth = 415
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 409
        ExplicitLeft = 3
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 674
    Height = 38
    Align = alTop
    TabOrder = 2
    DesignSize = (
      674
      38)
    object Label2: TLabel
      Left = 561
      Top = 12
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Cantidad:'
      Visible = False
      ExplicitLeft = 448
    end
    object ztbCuantos: TZetaTextBox
      Left = 609
      Top = 10
      Width = 60
      Height = 17
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      AutoSize = False
      ShowAccelChar = False
      Visible = False
      Brush.Color = clBtnFace
      Border = True
      ExplicitLeft = 496
    end
    object Label3: TLabel
      Left = 14
      Top = 12
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empleado:'
    end
    object CB_CODIGO: TZetaKeyLookup_DevEx
      Left = 70
      Top = 9
      Width = 300
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 70
      OnValidKey = CB_CODIGOValidKey
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 57
    Width = 674
    Height = 213
    ExplicitTop = 57
    ExplicitWidth = 674
    ExplicitHeight = 213
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsSelection.MultiSelect = True
      object CB_CODIGO_GRID: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'CB_CODIGO'
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
      end
      object SJ_STATUS_GRID: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'SJ_STATUS'
      end
      object SJ_NUM_EVA: TcxGridDBColumn
        Caption = 'Evaluadores'
        DataBinding.FieldName = 'SJ_NUM_EVA'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 520
    Top = 0
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
