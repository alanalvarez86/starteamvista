inherited TotalesEncuesta: TTotalesEncuesta
  Left = 362
  Top = 222
  Caption = 'Totales de Encuesta'
  ClientHeight = 505
  ClientWidth = 586
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 586
    inherited ValorActivo2: TPanel
      Width = 327
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 586
    Height = 147
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 26
      Top = 5
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'N�mero:'
    end
    object ET_CODIGO: TZetaDBTextBox
      Left = 69
      Top = 3
      Width = 89
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'ET_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'ET_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object ET_NOMBRE: TZetaDBTextBox
      Left = 69
      Top = 22
      Width = 358
      Height = 17
      AutoSize = False
      Caption = 'ET_NOMBRE'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'ET_NOMBRE'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label2: TLabel
      Left = 26
      Top = 24
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nombre:'
    end
    object Label3: TLabel
      Left = 7
      Top = 42
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci�n:'
    end
    object Label6: TLabel
      Left = 459
      Top = 5
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Status:'
    end
    object DBText1: TDBText
      Left = 495
      Top = 4
      Width = 91
      Height = 13
      DataField = 'ET_STATUS'
      DataSource = DataSource
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clActiveCaption
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 1
      Top = 86
      Width = 65
      Height = 13
      Alignment = taRightJustify
      Caption = 'Responsable:'
    end
    object ztbResp: TZetaTextBox
      Left = 69
      Top = 83
      Width = 257
      Height = 17
      AutoSize = False
      Caption = 'ztbResp'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object ET_FEC_INI: TZetaDBTextBox
      Left = 69
      Top = 103
      Width = 89
      Height = 17
      AutoSize = False
      Caption = 'ET_FEC_INI'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'ET_FEC_INI'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label4: TLabel
      Left = 38
      Top = 105
      Width = 28
      Height = 13
      Alignment = taRightJustify
      Caption = 'Inicio:'
    end
    object Label8: TLabel
      Left = 26
      Top = 125
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'Criterios:'
    end
    object ET_NUM_COM: TZetaDBTextBox
      Left = 69
      Top = 123
      Width = 89
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'ET_NUM_COM'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'ET_NUM_COM'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label9: TLabel
      Left = 183
      Top = 125
      Width = 51
      Height = 13
      Alignment = taRightJustify
      Caption = 'Preguntas:'
    end
    object Label5: TLabel
      Left = 209
      Top = 105
      Width = 25
      Height = 13
      Alignment = taRightJustify
      Caption = 'Final:'
    end
    object ET_FEC_FIN: TZetaDBTextBox
      Left = 237
      Top = 103
      Width = 89
      Height = 17
      AutoSize = False
      Caption = 'ET_FEC_FIN'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'ET_FEC_FIN'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object ET_NUM_PRE: TZetaDBTextBox
      Left = 237
      Top = 123
      Width = 89
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'ET_NUM_PRE'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'ET_NUM_PRE'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object ET_DESCRIP: TDBMemo
      Left = 69
      Top = 41
      Width = 507
      Height = 39
      Color = clBtnFace
      DataField = 'ET_DESCRIP'
      DataSource = DataSource
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 0
    end
    object GroupBox1: TGroupBox
      Left = 402
      Top = 81
      Width = 172
      Height = 61
      Caption = ' Escalas: '
      TabOrder = 1
      object Label10: TLabel
        Left = 9
        Top = 17
        Width = 72
        Height = 13
        Alignment = taRightJustify
        Caption = 'De evaluaci�n:'
      end
      object ET_NIVELES: TZetaDBTextBox
        Left = 84
        Top = 15
        Width = 80
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'ET_NIVELES'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'ET_NIVELES'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label11: TLabel
        Left = 7
        Top = 37
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'De importancia:'
      end
      object ET_PESOS: TZetaDBTextBox
        Left = 84
        Top = 35
        Width = 80
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'ET_PESOS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'ET_PESOS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
  end
  object pcGraficas: TPageControl [2]
    Left = 0
    Top = 166
    Width = 586
    Height = 339
    ActivePage = tsEvaluados
    Align = alClient
    TabOrder = 2
    object tsEvaluaciones: TTabSheet
      Caption = 'Evaluaciones'
      object GraficaEvaluaciones: TChart
        Left = 139
        Top = 0
        Width = 439
        Height = 311
        AllowPanning = pmNone
        AllowZoom = False
        AnimatedZoom = True
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        BackWall.Pen.Visible = False
        Title.Text.Strings = (
          '')
        AxisVisible = False
        Chart3DPercent = 35
        ClipPoints = False
        Frame.Visible = False
        View3DOptions.Elevation = 315
        View3DOptions.Orthogonal = False
        View3DOptions.Perspective = 0
        View3DOptions.Rotation = 360
        View3DOptions.Zoom = 96
        View3DWalls = False
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Series1: TBarSeries
          Active = False
          Marks.ArrowLength = 20
          Marks.Style = smsPercentTotal
          Marks.Visible = False
          SeriesColor = clNavy
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Bar'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
        object Series2: TPieSeries
          Marks.ArrowLength = 8
          Marks.Style = smsPercent
          Marks.Visible = True
          SeriesColor = clRed
          Title = 'SeriesEvaluaciones'
          OtherSlice.Text = 'Other'
          PieValues.DateTime = False
          PieValues.Name = 'Pie'
          PieValues.Multiplier = 1
          PieValues.Order = loNone
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 139
        Height = 311
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object lblEvaluaciones: TLabel
          Left = 3
          Top = 38
          Width = 97
          Height = 13
          Caption = '%d Evaluaciones'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object TablaEvaluaciones: TStringGrid
          Left = 5
          Top = 56
          Width = 134
          Height = 129
          ColCount = 2
          DefaultColWidth = 80
          DefaultRowHeight = 20
          FixedCols = 0
          RowCount = 6
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ScrollBars = ssNone
          TabOrder = 0
          ColWidths = (
            80
            49)
          RowHeights = (
            20
            20
            20
            20
            20
            20)
        end
      end
    end
    object tsEvaluadores: TTabSheet
      Caption = 'Evaluadores'
      ImageIndex = 1
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 167
        Height = 311
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object lblEvaluadores: TLabel
          Left = 3
          Top = 38
          Width = 80
          Height = 13
          Caption = '%d Evaluados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object TablaEvaluadores: TStringGrid
          Left = 5
          Top = 56
          Width = 162
          Height = 87
          ColCount = 2
          DefaultColWidth = 80
          DefaultRowHeight = 20
          FixedCols = 0
          RowCount = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ScrollBars = ssNone
          TabOrder = 0
          ColWidths = (
            109
            48)
          RowHeights = (
            20
            20
            20
            20)
        end
      end
      object GraficaEvaluadores: TChart
        Left = 167
        Top = 0
        Width = 411
        Height = 311
        AllowPanning = pmNone
        AllowZoom = False
        AnimatedZoom = True
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        BackWall.Pen.Visible = False
        Title.Text.Strings = (
          '')
        AxisVisible = False
        Chart3DPercent = 35
        ClipPoints = False
        Frame.Visible = False
        View3DOptions.Elevation = 315
        View3DOptions.Orthogonal = False
        View3DOptions.Perspective = 0
        View3DOptions.Rotation = 360
        View3DOptions.Zoom = 96
        View3DWalls = False
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object BarSeries2: TBarSeries
          Active = False
          Marks.ArrowLength = 20
          Marks.Style = smsPercentTotal
          Marks.Visible = False
          SeriesColor = clNavy
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Bar'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
        object PieSeries2: TPieSeries
          Marks.ArrowLength = 8
          Marks.Style = smsPercent
          Marks.Visible = True
          SeriesColor = clRed
          Title = 'SeriesEvaluados'
          OtherSlice.Text = 'Other'
          PieValues.DateTime = False
          PieValues.Name = 'Pie'
          PieValues.Multiplier = 1
          PieValues.Order = loNone
        end
      end
    end
    object tsEvaluados: TTabSheet
      Caption = 'Evaluados'
      ImageIndex = 2
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 138
        Height = 311
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object lblEvaluados: TLabel
          Left = 3
          Top = 38
          Width = 80
          Height = 13
          Caption = '%d Evaluados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object TablaEvaluados: TStringGrid
          Left = 5
          Top = 56
          Width = 133
          Height = 130
          ColCount = 2
          DefaultColWidth = 80
          DefaultRowHeight = 20
          FixedCols = 0
          RowCount = 6
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ScrollBars = ssNone
          TabOrder = 0
          ColWidths = (
            80
            47)
          RowHeights = (
            20
            20
            20
            20
            20
            20)
        end
      end
      object GraficaEvaluados: TChart
        Left = 138
        Top = 0
        Width = 440
        Height = 311
        AllowPanning = pmNone
        AllowZoom = False
        AnimatedZoom = True
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        BackWall.Pen.Visible = False
        Title.Text.Strings = (
          '')
        AxisVisible = False
        Chart3DPercent = 35
        ClipPoints = False
        Frame.Visible = False
        View3DOptions.Elevation = 315
        View3DOptions.Orthogonal = False
        View3DOptions.Perspective = 0
        View3DOptions.Rotation = 360
        View3DOptions.Zoom = 96
        View3DWalls = False
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object BarSeries1: TBarSeries
          Active = False
          Marks.ArrowLength = 20
          Marks.Style = smsPercentTotal
          Marks.Visible = False
          SeriesColor = clNavy
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Bar'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
        object PieSeries1: TPieSeries
          Marks.ArrowLength = 8
          Marks.Style = smsPercent
          Marks.Visible = True
          SeriesColor = clRed
          Title = 'SeriesEvaluados'
          OtherSlice.Text = 'Other'
          PieValues.DateTime = False
          PieValues.Name = 'Pie'
          PieValues.Multiplier = 1
          PieValues.Order = loNone
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 544
    Top = 8
  end
end
