unit FEditaPreguntas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, DBCtrls, ZetaSmartLists, Buttons, StdCtrls,
  Mask, ZetaKeyLookup, ZetaDBTextBox,ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, cxControls, dxBarExtItems,
  dxBar, cxClasses, Vcl.ImgList, cxNavigator, cxDBNavigator, cxButtons,
  cxContainer, cxEdit, cxGroupBox;

type
  TEditaPreguntas = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    GroupBox1: TcxGroupBox;
    GroupBox2: TcxGroupBox;
    PG_DESCRIP: TDBMemo;
    PG_CONSEJO: TDBMemo;
    PG_ORDEN: TZetaDBTextBox;
    PG_FOLIO: TZetaDBTextBox;
    CM_NOMBRE: TZetaTextBox;
    btnCreaEscala: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnCreaEscalaClick(Sender: TObject);
    procedure DataSourceStateChange(Sender: TObject);
  private
    { Private declarations }
    procedure SetControles;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  EditaPreguntas: TEditaPreguntas;

implementation

{$R *.DFM}
uses DEvaluacion,
     ZHelpContext,
     ZetaBuscador,
     ZetaCommonClasses,
     ZAccesosMgr,
     ZAccesosTress;

procedure TEditaPreguntas.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_EVAL_PREGUNTAS;
     FirstControl := PG_DESCRIP;
     HelpContext := H_CATALOGOS_PREGUNTAS_AGREGAR_REGISTRO;
     {$ifdef BRAZEWAY}
     PG_DESCRIP.MaxLength := 1024;
     {$endif}
end;

procedure TEditaPreguntas.FormShow(Sender: TObject);
begin
     inherited;
     with dmEvaluacion.cdsCompetencia do
     begin
          CM_NOMBRE.Caption := FieldByName('CM_CODIGO').AsString +' = '+ FieldByName('CM_DESCRIP').AsString;
     end;
     SetControles;
     ActiveControl := Cancelar_DevEx;
end;

procedure TEditaPreguntas.SetControles;
begin
     btnCreaEscala.Enabled := ZAccesosMgr.CheckDerecho( D_CAT_EVAL_ESCALAS, K_DERECHO_ALTA ) and
                              ( not ( dmEvaluacion.cdsPreguntas.State in [ dsEdit, dsInsert ] ) );
end;

procedure TEditaPreguntas.Connect;
begin
     with dmEvaluacion do
     begin
          cdsCompetencia.Conectar;
          DataSource.DataSet := cdsPreguntas;
     end;
end;

procedure TEditaPreguntas.Agregar;
begin
     inherited;
end;

procedure TEditaPreguntas.Borrar;
begin
     inherited;
end;

procedure TEditaPreguntas.Modificar;
begin
     inherited;
end;

procedure TEditaPreguntas.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Pregunta', 'PREGUNTA', 'PG_FOLIO', dmEvaluacion.cdsPreguntas );
end;

procedure TEditaPreguntas.btnCreaEscalaClick(Sender: TObject);
begin
     inherited;
     dmEvaluacion.cdsEscalas.Agregar;
end;

procedure TEditaPreguntas.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     SetControles;
end;

end.
