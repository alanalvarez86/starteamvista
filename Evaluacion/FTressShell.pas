unit FTressShell;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, ImgList, ActnList, ExtCtrls, ComCtrls, StdCtrls,
  Buttons, ZetaSmartLists, ShellApi,
  ZetaClientDataSet,
  ZetaCommonLists,
  ZetaTipoEntidad,
  ZetaMessages,
  ZBaseShell,
  ZetaStateComboBox, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxStatusBar, dxRibbonStatusBar,
  dxRibbonSkins, dxRibbonCustomizationForm, System.Actions, cxStyles, cxClasses,
  dxSkinsForm, dxRibbon, ZBasicoNavBarShell, dxBarBuiltInMenu, cxLocalization,
  dxBar, dxNavBar, dxNavBarCollns,cxButtons, cxPC, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, ZetaCXStateComboBox, cxImage,dxGDIPlusClasses, cxLabel,
  cxBarEditItem,cxTreeView;

type
  TTressShell = class(TBasicoNavBarShell)
    _Enc_Primera: TAction;
    _Enc_Anterior: TAction;
    _Enc_Siguiente: TAction;
    _Enc_Ultima: TAction;
    _Emp_Primero: TAction;
    _Emp_Anterior: TAction;
    _Emp_Siguiente: TAction;
    _Emp_Ultimo: TAction;
    _Emp_Reg_Agrega_EmpEvaluar: TAction;
    EncuestaMenu: TMainMenu;
    EncuestaRegistro: TMenuItem;
    EncuestaProcesos: TMenuItem;
    AgregarEmpleadosaevaluar1: TMenuItem;
    _Emp_Proc_Emp_Evaluar_Global: TAction;
    EmpleadoMenu: TMainMenu;
    MenuItem1: TMenuItem;
    MenuItem3: TMenuItem;
    _V_Empleados: TAction;
    _V_Encuesta: TAction;
    _E_Cambiar_Empleado: TAction;
    _E_Cambiar_Encuesta: TAction;
    Empleadosaevaluarglobal1: TMenuItem;
    _Enc_Proc_Preparar_Evaluacion: TAction;
    PrepararEvaluaciones1: TMenuItem;
    _Enc_Proc_Calcular_Resultados: TAction;
    Calcularresultados1: TMenuItem;
    _Enc_Proc_Recalcular_Pomedios: TAction;
    Recalcularpromedios1: TMenuItem;
    Envainvitaciones1: TMenuItem;
    _Enc_Envia_Invitaciones: TAction;
    _Enc_Envia_Notificaciones: TAction;
    Envacorreosdenotificacin1: TMenuItem;
    _Enc_Asigna_Relaciones: TAction;
    Asignacinderelaciones1: TMenuItem;
    _A_Preliminar: TAction;
    _Enc_Crear_Encuesta: TAction;
    Crearencuestasimple1: TMenuItem;
    N5: TMenuItem;
    N10: TMenuItem;
    N12: TMenuItem;
    PanelControlesEncuesta: TPanel;
    PeriodoTipoLBL_DevEx: TLabel;
    EncuestaPrimero_DevEx: TcxButton;
    EncuestaAnterior_DevEx: TcxButton;
    EncuestaSiguiente_DevEx: TcxButton;
    EncuestaUltimo_DevEx: TcxButton;
    StatusEncuestaPanel_DevEx: TPanel;
    PanelNombreEncuesta: TPanel;
    TabEvaluacionCFG: TdxRibbonTab;
    DevEx_BarManagerBar1: TdxBar;
    TabEvaluacionCFG_Agregar: TdxBarLargeButton;
    DevEx_BarManagerBar2: TdxBar;
    TabEvaluacionCFG_Crear: TdxBarLargeButton;
    TabEvaluacionCFG_Asigna: TdxBarLargeButton;
    DevEx_BarManagerBar3: TdxBar;
    TabEvaluacionCFG_Evaluar: TdxBarLargeButton;
    TabEvaluacionCFG_Preparar: TdxBarLargeButton;
    DevEx_BarManagerBar4: TdxBar;
    TabEvaluacionCFG_Invitacion: TdxBarLargeButton;
    TabEvaluacionCFG_Notificacion: TdxBarLargeButton;
    DevEx_BarManagerBar5: TdxBar;
    TabEvaluacionCFG_Calcular: TdxBarLargeButton;
    TabEvaluacionCFG_Recalcular: TdxBarLargeButton;
    EncuestaNumeroCB_DevEx: TcxStateComboBox;
    PanelValorAEmpleado_2: TPanel;
    EmpleadoPrettyName_DevEx: TLabel;
    ImagenStatus_DevEx: TcxImage;
    PanelValorEmpleado_1: TPanel;
    EmpleadoLBL_DevEx: TLabel;
    btnBuscarEmp_DevEx: TcxButton;
    btnEmplPrimero_DevEx: TcxButton;
    btnEmpAnterior_DevEx: TcxButton;
    btnEmpSiguiente_DevEx: TcxButton;
    btnEmpUltimo_DevEx: TcxButton;
    EmpleadoNumeroCB_DevEx: TcxStateComboBox;
    Image24_StatusEmpleado: TcxImageList;
    DevEx_BarManagerBar6: TdxBar;
    TabArchivo_btnExplorardorReportes: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    TabArchivo_btnSQL: TdxBarLargeButton;
    _A_ExploradorReportes: TAction;
    _A_SQL_DevEx: TAction;
    TabArchivo_btnConfigurarEmpresa: TdxBarLargeButton;
    TabArchivo_btnCatalogoUsuarios: TdxBarLargeButton;
    _A_ConfigurarEmpresa: TAction;
    _A_CatalogoUsuarios: TAction;
    cxBarEditItem1: TcxBarEditItem;
    HintEncuesta: TcxBarEditItem;
    TabEvaluacionCFG_VistaPrevia: TdxBarLargeButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure _Enc_PrimeraExecute(Sender: TObject);
    procedure _Enc_AnteriorExecute(Sender: TObject);
    procedure _Enc_SiguienteExecute(Sender: TObject);
    procedure _Enc_UltimaExecute(Sender: TObject);
    procedure _Enc_PrimeraUpdate(Sender: TObject);
    procedure _Enc_UltimaUpdate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure _Emp_PrimeroExecute(Sender: TObject);
    procedure _Emp_PrimeroUpdate(Sender: TObject);
    procedure _Emp_AnteriorExecute(Sender: TObject);
    procedure _Emp_SiguienteExecute(Sender: TObject);
    procedure _Emp_UltimoExecute(Sender: TObject);
    procedure _Emp_UltimoUpdate(Sender: TObject);
    procedure EmpleadoBuscaBtnClick(Sender: TObject);
    procedure _Emp_Reg_Agrega_EmpEvaluarExecute(Sender: TObject);
    procedure _Emp_Proc_Emp_Evaluar_GlobalExecute(Sender: TObject);
    procedure _V_EmpleadosExecute(Sender: TObject);
    procedure _E_Cambiar_EmpleadoExecute(Sender: TObject);
    procedure _E_Cambiar_EncuestaExecute(Sender: TObject);
    procedure _V_EncuestaExecute(Sender: TObject);
    procedure _Enc_Proc_Preparar_EvaluacionExecute(Sender: TObject);
    procedure _Enc_Proc_Calcular_ResultadosExecute(Sender: TObject);
    procedure _Enc_Proc_Recalcular_PomediosExecute(Sender: TObject);
    procedure _Enc_Envia_InvitacionesExecute(Sender: TObject);
    procedure _Enc_Envia_NotificacionesExecute(Sender: TObject);
    procedure _Enc_Asigna_RelacionesExecute(Sender: TObject);
    procedure _A_PreliminarExecute(Sender: TObject);
    procedure _Enc_Crear_EncuestaExecute(Sender: TObject);
    procedure ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
      Sender: TObject);
    procedure CargaTraducciones; override;
    procedure EncuestaNumeroCB_DevExLookUp(Sender: TObject; var lOk: Boolean);
    procedure btnBuscarEmp_DevExClick(Sender: TObject);
    procedure _A_ExploradorReportesExecute(Sender: TObject);
    procedure _A_SQL_DevExExecute(Sender: TObject);
    procedure _A_CatalogoUsuariosExecute(Sender: TObject);
    procedure _A_ConfigurarEmpresaExecute(Sender: TObject);
    procedure EmpleadoNumeroCB_DevExLookUp(Sender: TObject; var lOk: Boolean);
    procedure FormResize(Sender: TObject);
  protected
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
    procedure HabilitaControles; override;
    procedure NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado); override; 
    function ProcesaDialogo(oLookUp: TZetaLookupDataset; const eEntidad: TipoEntidad; const sFilter: String; var sKey,
      sDescription: String; var lEncontrado: Boolean): Boolean;override;
    procedure ArbolitoDblClick(Sender: TObject); override;
  private
    { Private declarations }
    FTempEncuesta :String; //Variable donde se almacena el nombre de la encuesta de manera temporal

//    function AlInvocarAyuda(Command: Word; Data: Longint; var CallHelp: Boolean): Boolean;
    procedure RevisaDerechos;
    procedure CambioEncuestaActiva;
    //procedure CambiaEmpleadoActivos;
    procedure RefrescaEmpleadoActivos;
    procedure CargaEncuestaActiva;
    procedure CargaEmpleadoActivo;
    procedure RefrescaEncuestaInfo;
    procedure RefrescaEmpleadoInfo;
    //procedure RefrescaEmpleado;
    procedure CambioEmpleadoActivo;
    procedure WMWizardEnd(var Message: TMessage); message WM_WIZARD_END;
    procedure AsignacionTabOrder_DevEx;
    function truncaTexto(Texto:String):String;
  public
    { Public declarations }
    procedure LLenaOpcionesGrupos;
    function GetLookUpDataSet(const eEntidad: TipoEntidad): TZetaLookUpDataSet;override;
    procedure SetDataChange(const Entidades: array of TipoEntidad); override;
    procedure SetUltimaEncuesta;
    procedure RefrescaEncuestasActivas;
    procedure CargaVista; override;
    property TempEncuesta: String  read FTempEncuesta write FTempEncuesta;
  end;

var
  TressShell: TTressShell;
  //Arreglo para guardar los status del empleado
  StatusEmpleado: array [0..3] of string = ('Activo', 'Reingreso','Baja','Antes ingreso');
const
     H50051_Explorador_reportes = 23100; {Consulta > Reportes}
     
implementation

uses DCliente,
     DGlobal,
     DSistema,
     DCatalogos,
     DReportes,
     DDiccionario,
     DEvaluacion,
     DProcesos,
     ZHelpContext,
     ZetaDialogo,
     ZAccesosMgr,
     ZAccesosTress,
     ZetacommonClasses,
     ZetaClientTools,
     ZetaCommonTools,
     ZetaBuscaEmpleado_DevEx,
     DConsultas,
     ZetaDespConsulta,
     ZArbolTools;

{$R *.DFM}

const
     K_PANEL_ENCUESTA = 1;
     K_PANEL_EMPLEADO = 2;
     K_PRINCIPAL_CONTEXT = 0;//H00001_Usando_Evaluacion_Desempeno;
     K_MIN_WIDTH = 1165;
     K_EXTRA_WIDTH = 5;
     K_PANEL_WIDTH = 120;
     K_LIMITE_ENCUESTA = 15;
procedure TTressShell.DoCloseAll;
begin
     {CierraFormaTodas;
     inherited DoCloseAll;}
     CierraFormaTodas;
     if EmpresaAbierta then
     begin
          dmCliente.CierraEmpresa;
          CierraDatasets( dmDiccionario );
          CierraDatasets( dmConsultas );
          CierraDatasets( dmReportes );
          CierraDatasets( dmEvaluacion );
          CierraDatasets( dmSistema );
          CierraDatasets( dmCatalogos );
     end;
     inherited DoCloseAll;
end;

procedure TTressShell.DoOpenAll;
const
     K_TAMANO_ARBOLITOS = 6;
begin
     try
        inherited DoOpenAll;
        Global.Conectar;
        dmCliente.InitActivosEncuesta;
        dmCliente.InitActivosEmpleado;
        CargaEncuestaActiva;
        CargaEmpleadoActivo;
        SetArbolitosLength( K_TAMANO_ARBOLITOS );
        CreaNavBar;
        LLenaOpcionesGrupos;
        RevisaDerechos;
        _V_Encuesta.Execute;
     except
        on Error : Exception do
        begin
             ZetaDialogo.zError( 'Error al Conectar', 'Empresa: ' + dmCliente.Compania + CR_LF + Error.Message, 0 );
             DoCloseAll;
        end;
     end;
end;

procedure TTressShell.FormCreate(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
       DZetaServerProvider.InitAll;
     {$endif}
     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( Self );
     dmConsultas := TdmConsultas.Create( Self );
     dmSistema := TdmSistema.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     dmReportes := TdmReportes.Create( Self );
     dmDiccionario := TdmDiccionario.Create( Self );
     dmProcesos := TdmProcesos.Create( Self );
     dmEvaluacion := TdmEvaluacion.Create( Self );
     inherited;
     HelpContext := H_SHELL;
     FHelpGlosario := 'Evaluaci�n de desempe�o.chm';
     EmpleadoNumeroCB_DevEx.HelpContext                  := H_INTERFAZ_PRICIPAL;
     {
     Arbol.HelpContext                     := K_PRINCIPAL_CONTEXT;
     ArchivoOtraEmpresa.HelpContext        := K_PRINCIPAL_CONTEXT;
     ArchivoImprimir.HelpContext           := K_PRINCIPAL_CONTEXT;
     ArchivoImprimirForma.HelpContext      := K_PRINCIPAL_CONTEXT;
     ArchivoImpresora.HelpContext          := K_PRINCIPAL_CONTEXT;
     EscogeImpresora.HelpContext           := K_PRINCIPAL_CONTEXT;
     }
    // Application.OnHelp := AlInvocarAyuda;{OP: 04.11.08}
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     inherited;
     dmEvaluacion.Free;
     dmProcesos.Free;
     dmDiccionario.Free;
     dmReportes.Free;
     dmCatalogos.Free;
     dmSistema.Free;
     dmConsultas.Free;
     dmCliente.Free;
     Global.Free;
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll;
     DZetaServerProvider.FreeAll;
     {$endif}
end;

procedure TTressShell.FormResize(Sender: TObject);
var
sTemporal:string;
begin
     inherited;
     if (Self.Width <= K_MIN_WIDTH) then
     begin
          PanelNombreEncuesta.Caption:=VACIO;
          PanelNombreEncuesta.Width := 1;
     end
     else
     begin
          sTemporal := truncaTexto( StatusEncuestaPanel_DevEx.Hint );
          PanelNombreEncuesta.Caption:=sTemporal  ;
          PanelNombreEncuesta.Width := K_PANEL_WIDTH;
     end;
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
     inherited;
     WindowState := wsMaximized;
     CargaTraducciones;
end;

procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
begin
     if Dentro( enEncuesta, Entidades ) then
     begin
          RefrescaEncuestaInfo;
          RefrescaEncuestasActivas;
     end;
     if Dentro( enEmpleado, Entidades ) then
     begin
          RefrescaEmpleadoInfo;
          RefrescaEmpleadoActivos;
     end;
     { Cambios a otros datasets }
     inherited SetDataChange( Entidades );
     //NotifyDataChange( Entidades, stNinguno );
end;

procedure TTressShell.HabilitaControles;
begin
     inherited HabilitaControles;
     with dmCliente do
     begin
          TabEvaluacionCFG_Agregar.Enabled :=  EmpresaAbierta;
          TabEvaluacionCFG_Crear.Enabled := EmpresaAbierta;
          TabEvaluacionCFG_Asigna.Enabled := EmpresaAbierta;
          TabEvaluacionCFG_Evaluar.Enabled := EmpresaAbierta;
          TabEvaluacionCFG_Preparar.Enabled := EmpresaAbierta;
          TabEvaluacionCFG_Invitacion.Enabled := EmpresaAbierta;
          TabEvaluacionCFG_Notificacion.Enabled := EmpresaAbierta;
          TabEvaluacionCFG_Calcular.Enabled := EmpresaAbierta;
          TabEvaluacionCFG_Recalcular.Enabled := EmpresaAbierta;
          TabEvaluacionCFG_VistaPrevia.Enabled := EmpresaAbierta;
          TabArchivo_btnExplorardorReportes.Enabled := EmpresaAbierta;
          TabArchivo_btnSQL.Enabled := EmpresaAbierta;
          TabArchivo_btnConfigurarEmpresa.Enabled := EmpresaAbierta;
          TabArchivo_btnCatalogoUsuarios.Enabled := EmpresaAbierta;


          _Emp_Siguiente.Enabled := EmpresaAbierta;
          _Emp_Anterior.Enabled := EmpresaAbierta;
          _Enc_Anterior.Enabled := EmpresaAbierta;
          _Enc_Siguiente.Enabled := EmpresaAbierta;
          EncuestaNumeroCB_DevEx.Enabled := EmpresaAbierta;
          EmpleadoNumeroCB_DevEx.Enabled := EmpresaAbierta;
     end;


end;

procedure TTressShell.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
begin
     dmEvaluacion.NotifyDataChange( Entidades, Estado );
     inherited NotifyDataChange( Entidades, Estado );
end;

function TTressShell.ProcesaDialogo(oLookUp: TZetaLookupDataset; const eEntidad: TipoEntidad; const sFilter: String; var sKey, sDescription: String; var lEncontrado: Boolean ): Boolean;
begin
     Result := ( eEntidad = enEmpleado );
     if Result then
        lEncontrado := ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( VACIO, sKey, sDescription )
end;

function TTressShell.GetLookUpDataSet( const eEntidad: TipoEntidad ): TZetaLookUpDataSet;
begin
     with dmEvaluacion do
     begin
          case eEntidad of
               enEmpleado:     Result := dmCliente.cdsEmpleadoLookUp;
               enEncuesta:     Result := cdsEncuestas;
               enTCompetencia: Result := cdsTCompetencia;
               enCompetencias: Result := cdsCompetencia;
               enPreguntas:    Result := cdsPreguntas;
               enEscalas:      Result := cdsEscalas;
               enUsuarios:     Result := dmSistema.cdsUsuarios;
          else
              Result := NIL;
	end {case}
     end;
end;

procedure TTressShell.RevisaDerechos;

{
function Revisa( const iPos: Integer ): Boolean;
begin
     Result := ZAccesosMgr.CheckDerecho( iPos, K_DERECHO_CONSULTA );
end;
}

begin
     _A_CatalogoUsuarios.Enabled := Revisa( D_EVAL_REPORTA ) and dmcliente.EmpresaAbierta;
     _A_ConfigurarEmpresa.Enabled := Revisa( D_GLOBALES_EVALUACION ) and dmcliente.EmpresaAbierta;
     _A_SQL_DevEx.Enabled := Revisa( D_EVAL_CONSULTAS ) and dmcliente.EmpresaAbierta;
     _A_ExploradorReportes.Enabled := Revisa( D_REPORTES_EVALUACION ) and dmcliente.EmpresaAbierta;
     EncuestaRegistro.Enabled := Revisa( D_DISENO_REG ) and dmcliente.EmpresaAbierta;
     _Emp_Reg_Agrega_EmpEvaluar.Enabled := Revisa( D_AGREGAR_EMP_EVAL )and dmcliente.EmpresaAbierta;
     EncuestaProcesos.Enabled := Revisa( D_DISENO_PROC ) and dmcliente.EmpresaAbierta ;
     _Emp_Proc_Emp_Evaluar_Global.Enabled := Revisa( D_GENERAR_LISTA ) and dmcliente.EmpresaAbierta;
     _Enc_Proc_Preparar_Evaluacion.Enabled := Revisa( D_PREPARA_EVAL ) and dmcliente.EmpresaAbierta;
     _Enc_Proc_Calcular_Resultados.Enabled := Revisa( D_CALCULA_RESUL ) and dmcliente.EmpresaAbierta;
     _Enc_Proc_Recalcular_Pomedios.Enabled := Revisa( D_RECALCULA_PROM ) and dmcliente.EmpresaAbierta;
     _Enc_Envia_Invitaciones.Enabled := Revisa( D_CORREOS_INVITACION ) and dmcliente.EmpresaAbierta;
     _Enc_Envia_Notificaciones.Enabled := Revisa( D_CORREOS_NOTIFICACION ) and dmcliente.EmpresaAbierta;
     _Enc_Asigna_Relaciones.Enabled := Revisa( D_ASIGNA_RELACIONES ) and dmcliente.EmpresaAbierta;
     _Enc_Crear_Encuesta.Enabled := Revisa( D_CREA_ENCUESTA_SIMPLE ) and dmcliente.EmpresaAbierta;
     _A_Preliminar.Enabled := ( dmCliente.Encuesta > 0 );
end;

procedure TTressShell.CargaEncuestaActiva;
begin
     with dmCliente do
     begin
          EncuestaNumeroCB_DevEx.ValorEntero :=  Encuesta;
     end;
     SetUltimaEncuesta;
end;

procedure TTressShell.CargaEmpleadoActivo;
begin
     with dmCliente do
     begin
          EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
     end;
     RefrescaEmpleadoActivos;
end;

procedure TTressShell._Enc_PrimeraExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEncuestaPrimera then
             begin
                  EncuestaNumeroCB_DevEx.ValorEntero := Encuesta ;
                  CambioEncuestaActiva;
                  EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Enc_AnteriorExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEncuestaAnterior then
             begin
                  EncuestaNumeroCB_DevEx.ValorEntero :=  Encuesta ;
                  CambioEncuestaActiva;
                  EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Enc_SiguienteExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEncuestaSiguiente then
             begin
                  EncuestaNumeroCB_DevEx.ValorEntero :=  Encuesta;
                  CambioEncuestaActiva;
                  EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Enc_UltimaExecute(Sender: TObject);
begin
     SetUltimaEncuesta;
end;

procedure TTressShell.SetUltimaEncuesta;
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEncuestaUltima then
             begin
                  EncuestaNumeroCB_DevEx.ValorEntero :=  Encuesta ;
                  CambioEncuestaActiva;
                  EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end
             else
             begin
                  PanelNombreEncuesta.Caption := VACIO;
                  HintEncuesta.Caption :=VACIO;
                  EmpleadoPrettyName_DevEx.Caption := VACIO;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.CambioEncuestaActiva;
begin
     RefrescaEncuestasActivas;
     CambioValoresActivos( stEvaluacion );
end;

procedure TTressShell.RefrescaEncuestaInfo;
begin
     dmCliente.RefrescaEncuesta;
end;

procedure TTressShell.RefrescaEncuestasActivas;
var
 sTemp: string;
begin
     with dmCliente do
     begin
          with GetDatosEncuestaActiva do
          begin
               StatusBarMsg( 'Encuesta: '+ InttoStr( Encuesta )+' '+ Nombre, K_PANEL_ENCUESTA );
               sTemp := truncaTexto(  Nombre );
               PanelNombreEncuesta.Caption := sTemp;
               HintEncuesta.Caption := Nombre;
               TempEncuesta := Nombre;
               StatusEncuestaPanel_DevEx.Hint :=  Nombre;
               with StatusEncuestaPanel_DevEx do
               begin
                    if( Status = stenCerrada )then
                        Font.Color := clRed;
                    if Status = stenAbierta then
                        Font.Color := clGreen;
                    if Status = stenDiseno then
                        Font.Color := RGB(77,59,75) ; // RGB(156,129,139); grismorado
                    Caption := ObtieneElemento( lfStatusEncuesta, Ord( Status ) );
               end;
          end;
     end;
end;

procedure TTressShell._Enc_PrimeraUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.EncuestaUpEnabled;
end;

procedure TTressShell._Enc_UltimaUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.EncuestaDownEnabled;
end;


procedure TTressShell.EncuestaNumeroCB_DevExLookUp(Sender: TObject;var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        lOk := dmCliente.SetEncuestaNumero( EncuestaNumeroCB_DevEx.ValorEntero );
        if lOk then
           CambioEncuestaActiva
        else
        begin
             if( dmCliente.Encuesta <> EncuestaNumeroCB_DevEx.ValorEntero )then
                 ZetaDialogo.zInformation( 'Error', '! Encuesta no encontrada !', 0 );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Emp_PrimeroExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoPrimero then
             begin
                  EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Emp_PrimeroUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.EmpleadoDownEnabled;
end;

procedure TTressShell._Emp_AnteriorExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoAnterior then
             begin
                  EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Emp_SiguienteExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoSiguiente then
             begin
                  EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Emp_UltimoExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoUltimo then
             begin
                  EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Emp_UltimoUpdate(Sender: TObject);
begin
     inherited;
      TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.EmpleadoUpEnabled;
end;



procedure TTressShell.EmpleadoBuscaBtnClick(Sender: TObject);
const
     K_SIN_STATUS = -1;
var
   sKey, sDescription: String;
   iEmpleado: TNumEmp;
begin
     inherited;
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
     begin
          iEmpleado := StrToIntDef( sKey, 0 );
          with dmEvaluacion do
          begin
               ObtieneSujetosEvaluar( 0, K_SIN_STATUS, cdsSujeto );
               if( cdsSujeto.Locate( 'CB_CODIGO', iEmpleado, [] ) )then
               begin
                    EmpleadoNumeroCB_DevEx.AsignaValorEntero( iEmpleado );
               end
               else
                   ZetaDialogo.ZInformation( 'Informaci�n', 'El empleado seleccionado no pertenece a la encuesta activa', 0 );
          end;
     end;
end;

procedure TTressShell.EmpleadoNumeroCB_DevExLookUp(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        lOk := dmCliente.SetEmpleadoNumero( EmpleadoNumeroCB_DevEx.ValorEntero );
        if lOk then
           CambioEmpleadoActivo
        else
            ZetaDialogo.zInformation( 'Error', '! Empleado no encontrado !', 0 );
     finally
            Screen.Cursor := oCursor;
     end;
end;


procedure TTressShell.CambioEmpleadoActivo;
begin
     RefrescaEmpleadoActivos;
     CambioValoresActivos( stEmpleado );
end;


procedure TTressShell.RefrescaEmpleadoActivos;
var
   oColor : TColor;
   i:Integer;
   sCaption, sHint : String;

   APngImage: TdxPNGImage;
   ABitmap: TcxAlphaBitmap;
begin
     {with dmCliente do
     begin
           StatusBarMsg( 'Empleado: '+ IntToStr( Empleado ), K_PANEL_EMPLEADO );
           with GetDatosEmpleadoActivo do
           begin
                EmpleadoPrettyName.Caption := Nombre;
                dmCliente.GetStatusEmpleado(GetDatosEmpleadoActivo, sCaption, sHint, oColor);
                with EmpleadoActivoPanel do
                begin
                     Font.Color := oColor;
                     Caption := sCaption;
                     Hint := sHint;
                end;
           end;
     end;}   //OLD

     with dmCliente do
     begin
          StatusBarMsg( 'Empleado: '+ IntToStr( Empleado ), K_PANEL_EMPLEADO );
          with GetDatosEmpleadoActivo do
          begin
               EmpleadoPrettyName_DevEx.Caption := Nombre;
               GetStatusEmpleado(GetDatosEmpleadoActivo, sCaption, sHint, oColor);
               //DevEx (by am): Inicializando Bitmat y PNG
               ABitmap := TcxAlphaBitmap.CreateSize(24, 24);
               APngImage := nil;
               for i := Low(StatusEmpleado) to High(StatusEmpleado) do
               begin
                    if (sCaption = StatusEmpleado[i]) then
                    begin
                         try
                               //DevEx (by am): Creando el Bitmap
                               ABitmap.Clear;
                               Image24_StatusEmpleado.GetBitmap(i, ABitmap);
                               //DevEx (by am): Creando el PNG
                               APngImage := TdxPNGImage.CreateFromBitmap(ABitmap);
                               ImagenStatus_DevEx.Picture.Graphic := APngImage;
                               ImagenStatus_DevEx.Hint := sHint;
                         finally
                                APngImage.Free;
                                ABitmap.Free;
                         end;
                         Break;
                    end;
               end;
          end;
     end;
end;

procedure TTressShell.RefrescaEmpleadoInfo;
begin
     dmCliente.RefrescaEmpleado;
end;

procedure TTressShell._Emp_Reg_Agrega_EmpEvaluarExecute(Sender: TObject);
begin
     inherited;
     dmEvaluacion.RegistraEmpleadosEvaluar;
end;

{ ******* Mensajes de Wizards ejecutados con Threads ****** }
procedure TTressShell.WMWizardEnd(var Message: TMessage);
begin
     with Message do
     begin
          dmProcesos.HandleProcessEnd( WParam, LParam );
     end;
end;

procedure TTressShell._Emp_Proc_Emp_Evaluar_GlobalExecute(Sender: TObject);
begin
     inherited;
     //if dmCliente.ModuloAutorizado( okCursos ) then
        DProcesos.ShowWizard( prDEAgregaEmpEvaluarGlobal );
end;

procedure TTressShell._V_EmpleadosExecute(Sender: TObject);
begin
     inherited;
     _E_Cambiar_EmpleadoExecute( Sender );
end;

procedure TTressShell._E_Cambiar_EmpleadoExecute(Sender: TObject);
begin
     inherited;
     EmpleadoNumeroCB_DevEx.SetFocus;
     //VentanaEmpleados.Checked := True;
end;

procedure TTressShell._E_Cambiar_EncuestaExecute(Sender: TObject);
begin
     inherited;
     EncuestaNumeroCB_DevEx.SetFocus;
     //VentanaEncuesta.Checked := True;
end;

procedure TTressShell._V_EncuestaExecute(Sender: TObject);
begin
     inherited;
     _E_Cambiar_EncuestaExecute( Sender );
end;


procedure TTressShell._Enc_Proc_Preparar_EvaluacionExecute(Sender: TObject);
begin
     inherited;
     //if dmCliente.ModuloAutorizado( okCursos ) then
     DProcesos.ShowWizard( prDEPrepararEvaluaciones );
end;

procedure TTressShell._Enc_Proc_Calcular_ResultadosExecute(Sender: TObject);
begin
     inherited;
     //if dmCliente.ModuloAutorizado( okCursos ) then
     DProcesos.ShowWizard( prDECalcularResultados );
end;

procedure TTressShell._Enc_Proc_Recalcular_PomediosExecute(Sender: TObject);
begin
     inherited;
     //if dmCliente.ModuloAutorizado( okCursos ) then
     DProcesos.ShowWizard( prDERecalcularPromedios );
end;

procedure TTressShell._Enc_Envia_InvitacionesExecute(Sender: TObject);
begin
     inherited;
     //if dmCliente.ModuloAutorizado( okCursos ) then
     DProcesos.ShowWizard( prDECorreosInvitacion );
end;

procedure TTressShell._Enc_Envia_NotificacionesExecute(Sender: TObject);
begin
     inherited;
     //if dmCliente.ModuloAutorizado( okCursos ) then
     DProcesos.ShowWizard( prDECorreosNotificacion );
end;

procedure TTressShell._Enc_Asigna_RelacionesExecute(Sender: TObject);
begin
     inherited;
     //if dmCliente.ModuloAutorizado( okCursos ) then
     DProcesos.ShowWizard( prDEAsignaRelaciones );
end;

procedure TTressShell._A_CatalogoUsuariosExecute(Sender: TObject);
begin
     inherited;
     AbreFormaConsulta( efcReportaA );
end;

procedure TTressShell._A_ConfigurarEmpresaExecute(Sender: TObject);
begin
     AbreFormaConsulta( efcSistGlobales );

end;


procedure TTressShell._A_ExploradorReportesExecute(Sender: TObject);
begin
      AbreFormaConsulta( efcReportes );
end;

procedure TTressShell._A_PreliminarExecute(Sender: TObject);
const
     K_NOMBRE_FORMA = 'Encuestas';
var
   pDirVirtual: array[ 0..255 ] of Char;
   sDirVirtual: WideString;
   sParametros: String;
   lUtilizaEncuestaActiva: Boolean;
begin
     inherited;
     lUtilizaEncuestaActiva := True;
     if ( FormaActiva <> nil ) then
        lUtilizaEncuestaActiva := ( FormaActiva.Name <> K_NOMBRE_FORMA );
     sParametros := dmEvaluacion.ParametrosRutaVirtual( lUtilizaEncuestaActiva, sDirVirtual  );
     if StrVacio( sDirVirtual ) then
     begin
          ZetaDialogo.zError( '� Atenci�n !', 'No se ha especificado la ruta virtual del servidor WEB', 0 );
     end
     else
     begin
          ShellExecute( Application.Handle, 'open', StrPCopy( pDirVirtual, sDirVirtual + sParametros ), nil, nil, SW_SHOWNORMAL );
     end;
end;

procedure TTressShell._A_SQL_DevExExecute(Sender: TObject);
begin
     inherited;
     AbreFormaConsulta( efcQueryGral );
end;

procedure TTressShell._Enc_Crear_EncuestaExecute(Sender: TObject);
begin
     inherited;
     //if dmCliente.ModuloAutorizado( okCursos ) then
     DProcesos.ShowWizard( prDECreaEncuestaSimple );
end;

Procedure TTressShell.LLenaOpcionesGrupos;
var
I:integer;
begin
  if(dmCliente.EmpresaAbierta) then
  begin
        for I:=0 to DevEx_NavBar.Groups.Count-1 do
        begin
              if (DevEx_NavBar.Groups[I].Caption = 'Dise�o de Encuesta')  then
              begin
                    DevEx_NavBar.Groups[I].LargeImageIndex:=0;
                    DevEx_NavBar.Groups[I].SmallImageIndex:=0;
              end;
              if (DevEx_NavBar.Groups[I].Caption = 'Resultados de Encuesta')  then
              begin
                    DevEx_NavBar.Groups[I].LargeImageIndex:=1;
                    DevEx_NavBar.Groups[I].SmallImageIndex:=1;
              end;
              if (DevEx_NavBar.Groups[I].Caption = 'Resultados individuales')  then
              begin
                    DevEx_NavBar.Groups[I].LargeImageIndex:=2;
                    DevEx_NavBar.Groups[I].SmallImageIndex:=2;
              end;
              if (DevEx_NavBar.Groups[I].Caption = 'Consultas')  then
              begin
                    DevEx_NavBar.Groups[I].LargeImageIndex:=3;
                    DevEx_NavBar.Groups[I].SmallImageIndex:=3;
              end;
              if (DevEx_NavBar.Groups[I].Caption = 'Cat�logos')  then
              begin
                    DevEx_NavBar.Groups[I].LargeImageIndex:=4;
                    DevEx_NavBar.Groups[I].SmallImageIndex:=4;
              end;
              if (DevEx_NavBar.Groups[I].Caption = 'Sistema')  then
              begin
                    DevEx_NavBar.Groups[I].LargeImageIndex:=5;
                    DevEx_NavBar.Groups[I].SmallImageIndex:=5;
              end;
              if (I = 0) then
              begin
                    if(DevEx_NavBar.Groups.Count = 1) and ( DevEx_NavBar.Groups[I].Caption = 'No tiene derechos' )then
                    begin
                          DevEx_NavBar.Groups[I].LargeImageIndex:=6;
                          DevEx_NavBar.Groups[I].SmallImageIndex:=6;
                    end
                    else
                    begin
                         if(DevEx_NavBar.Groups.Count = 1) then
                         begin
                              if ( DevEx_NavBar.Groups[I].Caption = 'Dise�o de Encuesta' )  then
                              begin
                                   DevEx_NavBar.Groups[I].LargeImageIndex:=0;
                                   DevEx_NavBar.Groups[I].SmallImageIndex:=0;
                              end;
                         end;
                    end;
              end;
              DevEx_NavBar.Groups[I].UseSmallImages:=false;
        end;
  end
  else
  begin
        if(DevEx_NavBar.Groups.Count >0) then
        begin
              DevEx_NavBar.Groups[0].UseSmallImages:=false;
              DevEx_NavBar.Groups[0].LargeImageIndex:=4;
              DevEx_NavBar.Groups[0].SmallImageIndex:=4;
        end;
  end;
end;

procedure TTressShell.ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
  Sender: TObject);
begin
  inherited;
  _V_Cerrar.Execute;
end;

//Carga los textos a traducir
procedure TTressShell.CargaTraducciones;
begin
    DevEx_cxLocalizer.Active := True;
    DevEx_cxLocalizer.Locale := 2058;  // Carinal para Espanol (Mexico)

end;

procedure TTressShell.AsignacionTabOrder_DevEx;
begin
     DevEx_ShellRibbon.TabOrder:= 1;
     PanelNavBar.TabOrder := 2;
     DevEx_BandaValActivos.TabOrder :=3;
     PanelConsulta.TabOrder :=4;
end;

procedure TTressShell.btnBuscarEmp_DevExClick(Sender: TObject);
const
     K_SIN_STATUS = -1;
var
   sKey, sDescription: String;
   iEmpleado: TNumEmp;
begin
     inherited;
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
     begin
          iEmpleado := StrToIntDef( sKey, 0 );
          with dmEvaluacion do
          begin
               ObtieneSujetosEvaluar( 0, K_SIN_STATUS, cdsSujeto );
               if( cdsSujeto.Locate( 'CB_CODIGO', iEmpleado, [] ) )then
               begin
                   EmpleadoNumeroCB_DevEx.AsignaValorEntero( iEmpleado );
               end
               else
                   ZetaDialogo.ZInformation( 'Informaci�n', 'El empleado seleccionado no pertenece a la encuesta activa', 0 );
          end;
     end;
end;

procedure TTressShell.CargaVista;
begin
     inherited;
     //Mostar Banda Valores activos
     DevEx_BandaValActivos.Visible := TRUE;
     //Asignacion de TabOrders para el shell
     AsignacionTabOrder_DevEx;
     //Para avisar a los controles que hubo movimientos
     Application.ProcessMessages;
end;

function TTressShell.truncaTexto(Texto:String):String;
var
sTemp : String;
begin
      if ( Texto.Length > K_LIMITE_ENCUESTA ) then
      begin
            sTemp := copy(Texto,1,K_LIMITE_ENCUESTA) ;
            sTemp := sTemp +'...';
      end
      else
          sTemp := Texto;

     Result := sTemp ;

end;

procedure TTressShell.ArbolitoDblClick(Sender: TObject);
var
    oNodo: TTreeNode;
    TipoForma: eFormaConsulta;
    i: Integer;
    ANavBarControl: TdxNavBarGroupControl;
begin
    //Acceder a los controles dentro del grupo y buscar el TTreeView
    oNodo := nil;
    ANavBarControl := DevEx_NavBar.ActiveGroup.Control;
    for i := 0 to ANavBarControl.ControlCount - 1 do
        begin
        if ANavBarControl.Controls[i] is TTreeView then
           oNodo := (ANavBarControl.Controls[i] as TTreeView).Selected;
        if ANavBarControl.Controls[i] is TcxTreeView then //si el control del navbar es una tcxTreeview
           oNodo := (ANavBarControl.Controls[i] as TcxTreeView).Selected;
        end;

    if ( oNodo <> nil ) then
          begin
               if EsForma( oNodo, TipoForma ) then
               begin
                     if not( TipoForma in [ efcCatCompeten, efcCatPreguntas, efcCatTCompeten,
                                      efcCatEscalas, efcEncuestas, efcReportes,
                                      efcQueryGral, efcSistProcesos, efcSistGlobales,
                                      efcCorreos, efcReportaA ] )then
                     begin
                          if( dmCliente.Encuesta <> 0 )then      //DChavez: Si no hay encuestas creadas no se muestra las formas
                              AbreFormaConsulta( TipoForma )
                          else
                              ZetaDialogo.ZInformation( 'Informaci�n', 'Debe existir por lo menos 1 encuesta creada', 0 );
                     end
                     else
                         //Al abrir una forma de consulta, el tab seleccionado debe ser el de Editar (Caption: Principal)
                         //DevEx_ShellRibbon.ActiveTab := TabEditar;
                         AbreFormaConsulta( TipoForma );
               end
               else if ( Sender = nil ) then  { Es Folder. Si Viene de un Enter, Abrirlo }
                    oNodo.Expand( False );
          end;
end;


end.
