unit FCriteriosEvaluar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Db, ExtCtrls, Buttons, ZetaSmartLists, StdCtrls, Grids,
  DBGrids, ZetaDBGrid, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxTextEdit, cxClasses,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxButtons,ZetaClientDataSet;

type
    eSubeBaja = ( eSubir, eBajar );
    
  TCriteriosEvaluar = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    btnAgregaCompetencias: TcxButton;
    pnFlechas: TPanel;
    zmlArriba: TcxButton;
    zmlAbajo: TcxButton;
    EC_ORDEN: TcxGridDBColumn;
    EC_NOMBRE: TcxGridDBColumn;
    CM_DESCRIP: TcxGridDBColumn;
    EC_PESO: TcxGridDBColumn;
    EL_NOMBRE: TcxGridDBColumn;
    EC_NUM_PRE: TcxGridDBColumn;
    EC_GET_COM: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    procedure zmlArribaClick(Sender: TObject);
    procedure zmlAbajoClick(Sender: TObject);
    procedure btnAgregaCompetenciasClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure SubeBajaCriterio(eAccion: eSubeBaja);
    procedure RefrescaGrid( iPosicion: Integer; DataSet: TZetaClientDataset );
    procedure SetControles;
    procedure ConfigAgrupamiento;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  CriteriosEvaluar: TCriteriosEvaluar;

implementation

{$R *.DFM}

uses DEvaluacion,
     FAgregarCompetencias,
     ZImprimeForma,
     ZHelpContext,
     ZetaDialogo,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaTipoEntidad,
     ZetaBuscador_DevEx,
     ZetaCommonLists,
     ZetaCommonClasses;

{ TCriteriosEvaluar }
procedure TCriteriosEvaluar.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEvaluacion;
    // btnAgregaCompetencias.Caption := 'Agregar' + CR_LF + '  competencias';
     HelpContext := H_DISENO_ENCUESTA_CRITERIOS_EVALUAR;
end;

procedure TCriteriosEvaluar.Agregar;
begin
     with dmEvaluacion do
     begin
          if ModificaDisenoEncuesta then
             cdsCriteriosEval.Agregar;
     end;
     DoBestFit;
end;

procedure TCriteriosEvaluar.Borrar;
begin
     dmEvaluacion.cdsCriteriosEval.Borrar;
     DoBestFit;
end;

procedure TCriteriosEvaluar.Connect;
begin
     with dmEvaluacion do
     begin
          cdsCriteriosEval.Conectar;
          DataSource.DataSet := cdsCriteriosEval;
          Orden := cdsCriteriosEval.RecordCount;
          SetControles;
     end;
end;

procedure TCriteriosEvaluar.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( self.Caption, 'ENC_COMP', 'EC_ORDEN', dmEvaluacion.cdsCriteriosEval );
end;

procedure TCriteriosEvaluar.ImprimirForma;
begin
     inherited;
     ZImprimeForma.ImprimeUnaForma( enEvaComp, dmEvaluacion.cdsCriteriosEval );
end;

procedure TCriteriosEvaluar.Modificar;
begin
     dmEvaluacion.cdsCriteriosEval.Modificar;
     DoBestFit;
end;

procedure TCriteriosEvaluar.Refresh;
begin
     dmEvaluacion.cdsCriteriosEval.Refrescar;
     DoBestFit;
end;

procedure TCriteriosEvaluar.zmlArribaClick(Sender: TObject);
begin
     inherited;
     SubeBajaCriterio( eSubir );
end;

procedure TCriteriosEvaluar.zmlAbajoClick(Sender: TObject);
begin
     inherited;
     SubeBajaCriterio( eBajar );
end;

procedure TCriteriosEvaluar.SubeBajaCriterio(eAccion: eSubeBaja);
const
     K_PRIMER_CRITERIO = 1;
var
   iPosActual, iNuevaPos: Integer;
   oCursor: TCursor;
begin
     if( dmEvaluacion.ModificaDisenoEncuesta )then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             with dmEvaluacion do
             begin
                  iPosActual := cdsCriteriosEval.FieldByName('EC_ORDEN').AsInteger;
                  case( eAccion ) of
                        eSubir: begin
                                     if( iPosActual = K_PRIMER_CRITERIO )then
                                         ZetaDialogo.ZError( self.Caption, '� Es el inicio de la lista de criterios !', 0 )
                                     else
                                     begin
                                          iNuevaPos := iPosActual - 1;
                                          CambiaOrdenCriterios( iPosActual, iNuevaPos );
                                          RefrescaGrid( iNuevaPos, TZetaClientDataset(dmEvaluacion.cdsCriteriosEval) );
                                     end;
                                end;
                        eBajar: begin
                                     if( iPosActual = dmEvaluacion.cdsCriteriosEval.RecordCount  )then
                                         ZetaDialogo.ZError( self.Caption, '� Es el final de la lista de criterios !', 0 )
                                     else
                                     begin
                                          iNuevaPos := iPosActual + 1;
                                          CambiaOrdenCriterios( iPosActual, iNuevaPos );
                                          RefrescaGrid( iNuevaPos, TZetaClientDataset(dmEvaluacion.cdsCriteriosEval) );
                                     end;
                                end;
                  else
                      ZetaDialogo.ZError( self.Caption, '� Operaci�n no v�lida !', 0 )
                  end;
             end;
          finally
                  Screen.Cursor := oCursor;
          end;
          Application.ProcessMessages;
     end;
end;

procedure TCriteriosEvaluar.RefrescaGrid( iPosicion: Integer; DataSet: TZetaClientDataset );
var
   sField, sIndex, sFieldName: String;
   lDescendente: TIndexOptions;
begin
     with DataSet do
     begin
          lDescendente := [];
          AddIndex( sField, sFieldName, lDescendente);  //DChavez: se agrega un index vacio para que siempre ordene de manera descendente
          IndexDefs.Update; //Es necesario para que refleje que se agrego un Index.
          IndexName := sField;
     end;
     Refresh;
     dmEvaluacion.cdsCriteriosEval.Locate( 'EC_ORDEN', iPosicion, [] );
     DoBestFit;
end;

procedure TCriteriosEvaluar.btnAgregaCompetenciasClick(Sender: TObject);
begin
     if( dmEvaluacion.ModificaDisenoEncuesta )then
     begin
          if( ShowAgregarCompetencias )then
              dmEvaluacion.cdsCriteriosEval.Refrescar;
     end;
     DoBestFit;
end;

procedure TCriteriosEvaluar.SetControles;
begin
     zmlArriba.Enabled := ( dmEvaluacion.cdsCriteriosEval.RecordCount > 0 );
     zmlAbajo.Enabled := zmlArriba.Enabled;
     btnAgregaCompetencias.Enabled := ZAccesosMgr.CheckDerecho( D_CRITERIOS_EVAL, K_DERECHO_SIST_KARDEX );
end;

procedure TCriteriosEvaluar.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     //btnCopiaEscalas.Enabled := ZAccesosMgr.CheckDerecho( D_ESCALAS, K_DERECHO_SIST_KARDEX );
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('EC_ORDEN'), K_SIN_TIPO , '', skCount);
     ConfigAgrupamiento;
     ZetaDBGridDBTableView.ApplyBestFit();
     SetControles;
end;

procedure TCriteriosEvaluar.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //activa la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Muestra la caja de agrupamiento
     ZetaDBGridDBTableView.OptionsCustomize.ColumnFiltering := FALSE; //desactiva el filtrado
     EC_ORDEN.Options.Grouping := FALSE;
     EC_NOMBRE.Options.Grouping := FALSE;
     CM_DESCRIP.Options.Grouping := FALSE;
     EC_PESO.Options.Grouping := FALSE;
     EL_NOMBRE.Options.Grouping := TRUE;
     EC_NUM_PRE.Options.Grouping := FALSE;
     EC_GET_COM.Options.Grouping := FALSE;
end;



end.
