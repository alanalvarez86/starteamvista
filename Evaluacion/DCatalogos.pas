unit DCatalogos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient,
{$ifdef DOS_CAPAS}
     DServerCatalogos,
{$else}
     Catalogos_TLB,
{$endif}
  ZetaClientDataSet;

type
  TdmCatalogos = class(TDataModule)
    cdsCursos: TZetaLookupDataSet;
    cdsConceptosLookup: TZetaLookupDataSet;
    cdsCondiciones: TZetaLookupDataSet;
    cdsNomParamLookUp: TZetaLookupDataSet;
    procedure cdsCursosAlAdquirirDatos(Sender: TObject);
    procedure cdsCondicionesAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }
{$ifdef DOS_CAPAS}
    function GetServerCatalogos: TdmServerCatalogos;
    property ServerCatalogo: TdmServerCatalogos read GetServerCatalogos;
{$else}
    FServidor: IdmServerCatalogosDisp;
    function GetServerCatalogos: IdmServerCatalogosDisp;
    property ServerCatalogo: IdmServerCatalogosDisp read GetServerCatalogos;
{$endif}
  public
    { Public declarations }
  end;

var
  dmCatalogos: TdmCatalogos;

implementation
{$R *.DFM}

uses
    DCliente;
    
{$ifdef DOS_CAPAS}
function TdmCatalogos.GetServerCatalogos: TdmServerCatalogos;
begin
     Result := DCliente.dmCliente.ServerCatalogos;
end;
{$else}
function TdmCatalogos.GetServerCatalogos: IdmServerCatalogosDisp;
begin
     Result := IdmServerCatalogosDisp( dmCliente.CreaServidor( CLASS_dmServerCatalogos, FServidor ) );
end;
{$endif}

procedure TdmCatalogos.cdsCursosAlAdquirirDatos(Sender: TObject);
begin
     cdsCursos.Data := ServerCatalogo.GetCursos( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsCondicionesAlAdquirirDatos(Sender: TObject);
begin
     cdsCondiciones.Data := ServerCatalogo.GetCondiciones( dmCliente.Empresa );
end;

end.
