object dmCatalogos: TdmCatalogos
  OldCreateOrder = False
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object cdsCursos: TZetaLookupDataSet
    Tag = 2
    Aggregates = <>
    IndexFieldNames = 'CU_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsCursosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat�logo de Cursos'
    LookupDescriptionField = 'CU_NOMBRE'
    LookupKeyField = 'CU_CODIGO'
    Left = 24
    Top = 8
  end
  object cdsConceptosLookup: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    Left = 88
    Top = 8
  end
  object cdsCondiciones: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'QU_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsCondicionesAlAdquirirDatos
    LookupName = 'Cat�logo de Condiciones'
    LookupDescriptionField = 'QU_DESCRIP'
    LookupKeyField = 'QU_CODIGO'
    Left = 92
    Top = 76
  end
  object cdsNomParamLookUp: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    Left = 20
    Top = 72
  end
end
