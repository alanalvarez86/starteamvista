unit FEvaluadores_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Db, ExtCtrls, StdCtrls, Buttons, Mask, ZetaNumero,
  ZetaDBTextBox, Grids, DBGrids, ZetaDBGrid, ZetaKeyLookup, ComCtrls,
  ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, ZetaKeyLookup_DevEx, cxButtons;

type
  TEvaluadores_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Label2: TLabel;
    ztbCuantos: TZetaTextBox;
    Label3: TLabel;
    btnBuscaEmp: TSpeedButton;
    znUsuario: TZetaNumero;
    btnFiltrar: TcxButton;
    US_CODIGO: TZetaKeyLookup_DevEx;
    Label1: TLabel;
    EV_STATUS: TComboBox;
    US_CODIGO_GRID: TcxGridDBColumn;
    US_NOMBRE: TcxGridDBColumn;
    SE_TOTAL: TcxGridDBColumn;
    SE_NUEVA: TcxGridDBColumn;
    SE_PROCESO: TcxGridDBColumn;
    SE_TERMINA: TcxGridDBColumn;
    SE_STATUS: TcxGridDBColumn;
    procedure btnBuscaEmpClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    //procedure Grid1DrawColumnCell(Sender: TObject; const Rect: TRect;
    //  DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ZetaDBGridDBTableViewCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
  private
    { Private declarations }
    procedure LlenaListaStatus;
    procedure CuantosRegistros;
    procedure CargaFiltro;
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    //procedure DoLookup; override;
  end;

var
  Evaluadores_DevEx: TEvaluadores_DevEx;

implementation

{$R *.DFM}
uses DEvaluacion,
     DSistema,
     ZHelpContext,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaCommonLists;

{ TEvaluadores }
procedure TEvaluadores_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEvaluacion;
     US_CODIGO.LookupDataSet := dmSistema.cdsUsuarios;
     HelpContext := H_RESULTADOS_ENCUESTA_EVALUADORES;
end;

procedure TEvaluadores_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     US_CODIGO.Valor :=  0;
     LLenaListaStatus;
     CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0], 0 ,'' , SkCount );
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Muestra la caja de agrupamiento
     ApplyMinWidth;
     ZetaDBGridDBTableView.ApplyBestFit();
     US_CODIGO_GRID.Options.Grouping:= FALSE;
     US_NOMBRE.Options.Grouping:= FALSE;
     SE_TOTAL.Options.Grouping:= FALSE;
     SE_NUEVA.Options.Grouping:= FALSE;
     SE_PROCESO.Options.Grouping:= FALSE;
     SE_TERMINA.Options.Grouping:= FALSE;
     SE_STATUS.Options.Grouping:= TRUE;
end;

procedure TEvaluadores_DevEx.Agregar;
begin
     dmEvaluacion.cdsEvaluadores.Agregar;
end;

procedure TEvaluadores_DevEx.Borrar;
begin
     dmEvaluacion.cdsEvaluadores.Borrar;
end;

procedure TEvaluadores_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmEvaluacion do
     begin
          if not cdsEvaluadores.Active or cdsEvaluadores.HayQueRefrescar then
          begin
               cdsEvaluadores.Filtered := False;
               CargaFiltro;
               cdsEvaluadores.ResetDataChange;
          end;
          DataSource.DataSet := cdsEvaluadores;
     end;
end;

{procedure TEvaluadores.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( self.Caption, '', 'CB_CODIGO', dmEvaluacion.cdsEmpEvaluar );
end;}

procedure TEvaluadores_DevEx.ImprimirForma;
begin
  inherited;
end;

procedure TEvaluadores_DevEx.Modificar;
begin
     dmEvaluacion.cdsEvaluadores.Modificar;
end;

procedure TEvaluadores_DevEx.Refresh;
begin
     CargaFiltro;
end;

procedure TEvaluadores_DevEx.ZetaDBGridDBTableViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   iTermina: String;
   iTotal: String;
   INueva: String;
begin
    inherited;
    iTermina :=   AViewInfo.GridRecord.DisplayTexts[SE_TERMINA.Index];
    iTotal :=  AViewInfo.GridRecord.DisplayTexts[SE_TOTAL.Index];
    iNueva :=  AViewInfo.GridRecord.DisplayTexts[SE_NUEVA.Index];

    if ( iTermina = iTotal ) then
        ACanvas.Font.Color := clRed
    else
    begin
      if ( iNueva = iTotal ) then
           ACanvas.Font.Color := clBlack
      else
           ACanvas.Font.Color := clBlue;

   end;

end;

procedure TEvaluadores_DevEx.LlenaListaStatus;
begin
     with EV_STATUS do
     begin
          ZetaCommonLists.LlenaLista( lfStatusEvaluaciones, Items );
          Items.Insert( 0, 'Todas' );
          ItemIndex := 0;
     end;
end;

procedure TEvaluadores_DevEx.CuantosRegistros;
begin
     //ztbCuantos.Caption := inttoStr( dmEvaluacion.cdsEvaluadores.RecordCount );
     //StatusBar1.Panels[0].Text := 'Cantidad : ' + inttoStr( dmEvaluacion.cdsEvaluadores.RecordCount );
end;

procedure TEvaluadores_DevEx.btnBuscaEmpClick(Sender: TObject);
var
   sUsuario, sNombre: String;
begin
     inherited;
     if dmSistema.cdsUsuarios.Search( VACIO, sUsuario, sNombre ) then
     begin
          znUsuario.Valor := strtoint( sUsuario );
     end;

end;

procedure TEvaluadores_DevEx.btnFiltrarClick(Sender: TObject);
begin
     CargaFiltro;
     DoBestFit;
end;

procedure TEvaluadores_DevEx.CargaFiltro;
var
   iValor: Integer;
   sFiltro, sFiltros: String;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmEvaluacion.cdsEvaluadores do
        begin
             DisableControls;
             try
                iValor := US_CODIGO.Valor;
                case( EV_STATUS.ItemIndex )of
                      1: sFiltro := '(SE_TERMINA < SE_TOTAL)';
                      2: sFiltro := '(SE_NUEVA > 0)';
                      3: sFiltro := 'SE_PROCESO > 0';
                      4: sFiltro := 'SE_TERMINA > 0';
                      5: sFiltro := 'SE_NUEVA >= SE_TOTAL';
                      6: sFiltro := 'SE_PROCESO >= SE_TOTAL';
                      7: sFiltro := 'SE_TERMINA >= SE_TOTAL';
                else
                    sFiltro := VACIO;
                end;
                if( ( sFiltro = VACIO ) and ( iValor = 0 ) )then
                    sFiltros := VACIO
                else
                begin
                     if( iValor = 0 )then
                         sFiltros := sFiltro
                     else
                         sFiltros := ConcatFiltros( Format( 'US_CODIGO=%d',[ iValor ] ), sFiltro );
                end;
                dmEvaluacion.ObtieneEvaluadores( sFiltros );
             finally
                    EnableControls;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     CuantosRegistros;
end;

function TEvaluadores_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se pueden agregar evaluadores desde esta forma';
     Result := False;
end;

function TEvaluadores_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se pueden borrar evaluadores desde esta forma';
     Result := False;
end;

function TEvaluadores_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se pueden modificar evaluadores desde esta forma';
     Result := False;
end;


{procedure TEvaluadores_DevEx.Grid1DrawColumnCell( Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
                                            State: TGridDrawState);
begin
     with Grid1 do
     begin
          if ( gdSelected in State )  then
          begin
               with Canvas do
               begin
                    Font.Color := clWhite;
                    Brush.Color := clNavy;
               end;
          end
          else
          begin
               with Canvas do
               begin
                    Brush.Color := clWhite;
                    with Font do
                    begin
                         with dmEvaluacion.cdsEvaluadores do
                         begin
                              if( FieldByName('SE_TERMINA').AsInteger = FieldByName('SE_TOTAL').AsInteger )then
                                  Color := clRed
                              else
                              begin
                                   if( FieldByName('SE_NUEVA').AsInteger = FieldByName('SE_TOTAL').AsInteger )then
                                       Color := Grid1.Font.Color
                                   else
                                       Color := clBlue;
                              end;
                         end;
                    end;
               end;
          end;
          DefaultDrawDataCell( Rect, Column.Field, State );
     end;
end;
}

end.
