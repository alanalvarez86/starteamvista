unit FBuscaTexto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, DB, 
  ZetaClientDataset, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, cxButtons, Vcl.ImgList;

type
  TZetaBuscaTexto = class(TForm)
    lblPista: TLabel;
    edPista: TEdit;
    lblError: TLabel;
    cxImageList24_PanelBotones: TcxImageList;
    PanelBotones: TPanel;
    OK_DevEx: TcxButton;
    Cancelar_DevEx: TcxButton;
    procedure edPistaChange(Sender: TObject);
    procedure edPistaKeyPress(Sender: TObject; var Key: Char);
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FEncontrado: boolean;
    FCampo: String;
    FDataset: TZetaClientDataset;
    function Buscar: Boolean;
    procedure SetCampo( const Value: String );
    procedure SetNombreLlave( const Value: String );
    procedure SetNombreTabla(const Value: String);
  public
    { Public declarations }
    property Campo: String read FCampo write SetCampo;
    property Dataset: TZetaClientDataset read FDataset write FDataset;
    property NombreLlave: String write SetNombreLlave;
    property NombreTabla: String write SetNombreTabla;
    property Encontrado: boolean read FEncontrado;
  end;

procedure BuscarTexto( const sLlave, sTabla, sCampo: String; Tabla: TZetaClientDataset );

var
  ZetaBuscaTexto: TZetaBuscaTexto;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo;

{$R *.DFM}

procedure BuscarTexto( const sLlave, sTabla, sCampo: String; Tabla: TZetaClientDataset );
begin
     if Tabla.IsEmpty then
        ZetaDialogo.zInformation( 'B�squeda de ' + sLlave, 'No hay datos para buscar', 0 )
     else
     begin
          with TZetaBuscaTexto.Create( Application ) do
          begin
               try
                  Campo := sCampo;
                  Dataset := Tabla;
                  NombreLlave := sLlave;
                  NombreTabla := sTabla;
                  ShowModal;
               finally
                      Free;
               end;
          end;
     end;
end;

procedure TZetaBuscaTexto.OKClick(Sender: TObject);
begin
     inherited;
     FEncontrado := Buscar;
     if FEncontrado then
        Close
     else
     begin
          ZetaCommonTools.Chicharra;
          lblError.Visible := True;
          ActiveControl := edPista;
     end;
end;

procedure TZetaBuscaTexto.SetCampo(const Value: String);
begin
     FCampo := Value;
end;

procedure TZetaBuscaTexto.SetNombreLlave(const Value: String);
begin
     lblPista.Caption := Value + ':';
end;

procedure TZetaBuscaTexto.SetNombreTabla(const Value: String);
begin
     Self.Caption := 'Buscar ' + Value;
end;

function TZetaBuscaTexto.Buscar: Boolean;
var
   oRegistroActual: TBookmark;
begin
     Result := False;
     with DataSet do
     begin
          DisableControls;
          oRegistroActual := GetBookmark;
          First;
          try
             while not EOF do
             begin
                  if( Pos( UpperCase( edPista.Text ), UpperCase( FieldbyName( FCampo ).AsString ) ) <> 0 ) then
                  begin
                       Result := True;
                       FreeBookmark( oRegistroActual );
                       Abort;
                  end;
                  Next;
             end;
          finally
                 EnableControls;
          end;
          if not Result then
          begin
               GotoBookmark( oRegistroActual );
               FreeBookmark( oRegistroActual );
          end;
     end;
end;

procedure TZetaBuscaTexto.FormShow(Sender: TObject);
begin
     edPista.SetFocus;
end;

procedure TZetaBuscaTexto.edPistaChange(Sender: TObject);
begin
     lblError.Visible := False;
end;

procedure TZetaBuscaTexto.edPistaKeyPress(Sender: TObject; var Key: Char);
begin
     if ( Key = ZetaCommonClasses.UnaCOMILLA ) then
        Key := Chr( 0 );
     inherited KeyPress( Key );
end;

end.
