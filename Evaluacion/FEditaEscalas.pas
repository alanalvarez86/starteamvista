unit FEditaEscalas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicionRenglon_DevEx, Db, Grids, DBGrids, ZetaDBGrid, ComCtrls, ExtCtrls,
  DBCtrls, ZetaSmartLists, Buttons, StdCtrls, ZetaEdit, Mask, dbclient,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, cxControls,
  dxBarBuiltInMenu, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList, cxPC,
  cxNavigator, cxDBNavigator, cxButtons;

type
  TEditaEscalas = class(TBaseEdicionRenglon_DevEx)
    SC_NOMBRE: TDBEdit;
    Label2: TLabel;
    SC_CODIGO: TZetaDBEdit;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect;override;
    procedure DoLookup; override;
    procedure HabilitaControles; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure DoCancelChanges; override;
    procedure EscribirCambios; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
  public
    { Public declarations }
  end;

var
  EditaEscalas: TEditaEscalas;

implementation

{$R *.DFM}
uses DEvaluacion,
     ZHelpContext,
     ZetaBuscador,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaDialogo,
     ZBaseDlgModal,
     ZAccesosTress;

procedure TEditaEscalas.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_EVAL_ESCALAS;
     FirstControl := SC_CODIGO;
     HelpContext:= H_CATALOGOS_ESCALAS_AGREGAR_REGISTRO;
end;

procedure TEditaEscalas.FormShow(Sender: TObject);
begin
     inherited;
     Datos.TabVisible := False;
     Tabla.TabVisible := False;
     PageControl.ActivePage := Tabla;
     self.Height := 440;
     self.Width := 794;
end;

procedure TEditaEscalas.Connect;
begin
     with dmEvaluacion do
     begin
          DataSource.DataSet := cdsEditEscala;
          dsRenglon.DataSet := cdsNivelesEscala;
     end;
end;

procedure TEditaEscalas.Agregar;
begin
     inherited;
end;

procedure TEditaEscalas.Borrar;
begin
     inherited;
end;

procedure TEditaEscalas.Modificar;
begin
     inherited;
end;

procedure TEditaEscalas.DoCancelChanges;
begin
     dmEvaluacion.cdsNivelesEscala.CancelUpdates;
     inherited DoCancelChanges;
end;

procedure TEditaEscalas.DoLookup;
begin
  inherited;
end;

procedure TEditaEscalas.HabilitaControles;
begin
      inherited HabilitaControles;
end;

procedure TEditaEscalas.BBAgregarClick(Sender: TObject);
begin
     inherited;
     GridRenglones.OrdenarPor( GridRenglones.Columns[0] );
end;

procedure TEditaEscalas.BBBorrarClick(Sender: TObject);
begin
     if not( dmEvaluacion.cdsNivelesEscala.IsEmpty )then
     begin
          if ( Zetadialogo.ZWarningConfirm( 'Escalas', '� Desea borrar este rengl�n ?', 0, mbCancel ) ) then
             inherited;
     end;
end;

procedure TEditaEscalas.OKClick(Sender: TObject);
begin
     inherited;
     with DataSource do
     begin
          if ( TClientDataSet( DataSet ).ChangeCount = 0 ) and         //Si se aplicaron los cambios
             ( not ( DataSet.State in [ dsEdit, dsInsert ] ) ) then    //y no se tienen cambios pendientes
             Close;
     end;
end;

procedure TEditaEscalas.OK_DevExClick(Sender: TObject);
begin
     inherited;
     with DataSource do
     begin
          if ( TClientDataSet( DataSet ).ChangeCount = 0 ) and         //Si se aplicaron los cambios
             ( not ( DataSet.State in [ dsEdit, dsInsert ] ) ) then    //y no se tienen cambios pendientes
             Close;
     end;
end;

function TEditaEscalas.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede agregar escala desde esta forma';
     Result := False;
end;

function TEditaEscalas.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede borrar escala desde esta forma';
     Result := False;
end;

procedure TEditaEscalas.EscribirCambios;
begin
     with dmEvaluacion do
     begin
          cdsEditEscala.FieldByName('SC_NIVELES').AsInteger := cdsNivelesEscala.RecordCount;
     end;
     inherited;
end;

end.
