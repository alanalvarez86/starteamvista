inherited dmSistema: TdmSistema
  OldCreateOrder = True
  inherited cdsGrupos: TZetaLookupDataSet
    Aggregates = <
      item
        Active = True
        Expression = 'Max( GR_CODIGO ) + 0'
        Visible = False
      end>
  end
  object cdsAdicionalesAccesos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = GenericReconcileError
    Left = 384
    Top = 352
  end
  object cdsGruposAdic: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = GenericReconcileError
    LookupName = 'Agrupaciones Adicionales'
    LookupDescriptionField = 'GX_TITULO'
    LookupKeyField = 'GX_CODIGO'
    Left = 128
    Top = 256
  end
end
