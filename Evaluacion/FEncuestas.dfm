inherited Encuestas: TEncuestas
  Left = 340
  Top = 329
  Caption = 'Encuestas'
  ClientHeight = 287
  ClientWidth = 935
  ExplicitWidth = 935
  ExplicitHeight = 287
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 935
    ExplicitWidth = 935
    inherited ValorActivo2: TPanel
      Width = 676
      ExplicitWidth = 676
      inherited textoValorActivo2: TLabel
        Width = 670
        ExplicitLeft = 590
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 935
    Height = 36
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    DesignSize = (
      935
      36)
    object btnExportar: TcxButton
      Left = 872
      Top = 4
      Width = 26
      Height = 26
      Hint = 'Exportar encuesta'
      Anchors = [akTop, akRight]
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        20000000000000090000000000000000000000000000000000004858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF8D97DFFF8D97DFFF8D97DFFF8D97DFFF8D97DFFF8D97
        DFFF8D97DFFF8D97DFFF8D97DFFF8D97DFFF7C87DAFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFFFFFFFFFFC3C8EEFFBAC0ECFFBAC0ECFFBAC0ECFFBAC0
        ECFFBAC0ECFFBAC0ECFFBAC0ECFFD4D8F3FFD1D5F2FF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFFFFFFFFFF5F6DD2FF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF7C87DAFFAFB6E9FF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFFFFFFFFFF5F6DD2FF6572D4FF4B5BCDFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF6A77D6FF737FD8FF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFFFFFFFFFF5F6DD2FF6A77D6FFBAC0ECFF5362CFFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF7682D9FFF9FAFDFF848F
        DDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFFFFFFFFFF5F6DD2FF4858CCFFCED2F1FFF1F2FBFFC8CD
        F0FFACB3E8FF9099E0FF8D97DFFF939CE1FFACB3E8FFD7DAF4FFFFFFFFFFFCFC
        FEFF98A1E2FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFFFFFFFFFF5F6DD2FF4858CCFF5C6AD2FFEEEFFAFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFAFB6E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFFFFFFFFFF5F6DD2FF4858CCFF4858CCFF5C6AD2FFD1D5
        F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFAFB6E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFFFFFFFFFF5F6DD2FF4858CCFF4858CCFF4858CCFF4858
        CCFF737FD8FFB4BBEAFFE8EAF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFAFB6E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFFFFFFFFFF5F6DD2FF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4B5BCDFF5F6DD2FF8792DEFFFFFFFFFFAFB6
        E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFFFFFFFFFF5F6DD2FF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF7682D9FFAFB6E9FF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFFFFFFFFFF5F6DD2FF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF7C87DAFFAFB6E9FF5160CEFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFFFFFFFFFFD7DAF4FFD1D5F2FFD1D5F2FFD1D5F2FFD1D5
        F2FFD1D5F2FFD1D5F2FFD1D5F2FFE2E5F7FFD1D5F2FF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF8D97DFFF8D97DFFF8D97DFFF8D97DFFF8D97DFFF8D97
        DFFF8D97DFFF8D97DFFF8D97DFFF8D97DFFF7C87DAFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
      OptionsImage.Margin = 1
      PaintStyle = bpsGlyph
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = btnExportarClick
    end
    object btnImportar: TcxButton
      Left = 901
      Top = 4
      Width = 26
      Height = 26
      Hint = 'Importar encuesta'
      Anchors = [akTop, akRight]
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        200000000000000900000000000000000000000000000000000050D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3
        BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF81DFB1FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFFFFFFFFFC5F0DBFFBDEED6FFBDEED6FFBDEED6FFBDEE
        D6FFBDEED6FFBDEED6FFBDEED6FFD6F4E6FFD3F4E4FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFFFFFFFFF66D8A1FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF81DFB1FFB2EBD0FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFFFFFFFFF66D8A1FF50D293FF50D293FF50D293FF50D2
        93FF92E3BCFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF71DAA7FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFFFFFFFFF66D8A1FF50D293FF50D293FF53D395FFBDEE
        D6FFE9F9F1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF74DBA9FFC5F0DBFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFFFFFFFFF66D8A1FF50D293FF58D498FFD0F3E2FFFFFF
        FFFFFAFEFCFFC8F1DDFFA5E8C8FF92E3BCFF92E3BCFF9AE5C1FFB5ECD1FFD3F4
        E4FFFCFEFDFF8FE2BAFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFFFFFFFFF66D8A1FF63D79FFFE1F7ECFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFC3EFDAFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFFFFFFFFF66D8A1FF60D69DFFE4F8EEFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFEFCFFAAE9
        CAFF53D395FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFFFFFFFFF66D8A1FF50D293FF60D69DFFE4F8EEFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFEFDFFD6F4E6FFA5E8C8FF60D69DFF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFFFFFFFFF66D8A1FF50D293FF50D293FF60D69DFFE4F8
        EEFFECFAF3FF66D8A1FF60D69DFF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFFFFFFFFF66D8A1FF50D293FF50D293FF50D293FF60D6
        9DFFCEF2E1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFFFFFFFFF66D8A1FF50D293FF50D293FF50D293FF50D2
        93FF58D498FF50D293FF50D293FF81DFB1FFB2EBD0FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFFFFFFFFFD9F5E7FFD3F4E4FFD3F4E4FFD3F4E4FFD3F4
        E4FFD3F4E4FFD3F4E4FFD3F4E4FFE4F8EEFFD3F4E4FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3
        BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF81DFB1FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
      OptionsImage.Margin = 1
      PaintStyle = bpsGlyph
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = btnImportarClick
    end
    object btnCopiarEncuesta: TcxButton
      Left = 16
      Top = 4
      Width = 113
      Height = 26
      Hint = 'Copiar encuesta'
      Caption = 'Copiar encuesta'
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        2000000000000009000000000000000000000000000000000000D8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0DFCFFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFFFFFFFFFF3E4D7FFE7CAAFFFE7CAAFFFE7CAAFFFE7CA
        AFFFE7CAAFFFE7CAAFFFE7CAAFFFE7CAAFFFF9F2EBFFF0DFCFFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFFFFFFFFFECD5BFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFF5EADFFFF0DFCFFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFFFFFFFFFECD5BFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFF5EADFFFF0DFCFFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFFFFFFFFFECD5BFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFF5EADFFFF0DFCFFFD8AA7FFFD8AA
        7FFFE1BE9DFFF0DFCFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFFFFFFFFFECD5BFFFD8AA7FFFD8AA7FFFD8AA7FFFDCB2
        8BFFEAD2BBFFD8AA7FFFD8AA7FFFD8AA7FFFEEDAC7FFEAD2BBFFD8AA7FFFD8AA
        7FFFE7CAAFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFFFFFFFFFECD5BFFFD8AA7FFFD8AA7FFFD8AA7FFFE7CA
        AFFFFFFFFFFFEEDAC7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFE7CAAFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFFFFFFFFFECD5BFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFE9CFB7FFFFFFFFFFEEDAC7FFD8AA7FFFD8AA7FFFE3C2A3FFF5EADFFFDCB2
        8BFFE7CAAFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFFFFFFFFFFFFEFEFFFDFCFBFFFEFDFCFFFEFDFBFFD8AA
        7FFFD8AA7FFFE9CFB7FFFFFFFFFFEEDAC7FFD8AA7FFFE7CAAFFFFFFFFFFFDDB5
        8FFFE7CAAFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFFFFFFFFFFEFDFDFFFEFDFDFFFFFEFDFFFFFFFFFFFEFE
        FEFFD8AA7FFFD8AA7FFFE9CFB7FFFFFFFFFFEEDAC7FFE7CAAFFFFFFFFFFFDDB5
        8FFFE7CAAFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFFFFFFFFFFDFBFAFFFBF8F5FFFCF8F6FFFAF6F6FFFCF9
        F6FFD8AA7FFFD8AA7FFFD8AA7FFFE9CFB7FFFFFFFFFFF9F2EBFFFFFFFFFFDDB5
        8FFFE7CAAFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFF0DFCFFFF0DFCFFFF0DFCFFFF0DFCFFFF0DFCFFFEDD8
        C5FFD8AA7FFFE3C2A3FFE7CAAFFFE7CAAFFFF6EBE1FFFFFFFFFFFFFFFFFFDDB5
        8FFFE7CAAFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFF5EADFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDB5
        8FFFE7CAAFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFDCB28BFFDDB58FFFDDB58FFFDDB58FFFDDB58FFFDDB58FFFD9AB
        81FFE7CAAFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE4C5A7FFECD5
        BFFFECD5BFFFECD5BFFFECD5BFFFECD5BFFFECD5BFFFECD5BFFFECD5BFFFECD5
        BFFFF3E4D7FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFF0DFCFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
      OptionsImage.Margin = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = btnCopiarEncuestaClick
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 55
    Width = 935
    Height = 232
    ExplicitTop = 55
    ExplicitWidth = 935
    ExplicitHeight = 232
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      Styles.Content = cxStyle1
      Styles.Header = cxStyle2
      Styles.Indicator = cxStyle2
      object ET_CODIGO: TcxGridDBColumn
        Caption = '#'
        DataBinding.FieldName = 'ET_CODIGO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        Styles.Content = cxStyle3
        Width = 70
      end
      object ET_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'ET_NOMBRE'
        Styles.Content = cxStyle4
        Width = 200
      end
      object ET_FEC_INI: TcxGridDBColumn
        Caption = 'Inicio'
        DataBinding.FieldName = 'ET_FEC_INI'
        Styles.Content = cxStyle5
        Width = 70
      end
      object ET_FEC_FIN: TcxGridDBColumn
        Caption = 'Final'
        DataBinding.FieldName = 'ET_FEC_FIN'
        Styles.Content = cxStyle6
        Width = 70
      end
      object ET_STATUS: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'ET_STATUS'
        Styles.Content = cxStyle7
        Width = 70
      end
      object ET_NUM_COM: TcxGridDBColumn
        Caption = 'Criterios'
        DataBinding.FieldName = 'ET_NUM_COM'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        Styles.Content = cxStyle8
        Width = 70
      end
      object ET_NUM_SUJ: TcxGridDBColumn
        Caption = 'Evaluados'
        DataBinding.FieldName = 'ET_NUM_SUJ'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        Styles.Content = cxStyle9
        Width = 70
      end
      object ET_NUM_EVA: TcxGridDBColumn
        Caption = 'Evaluaciones'
        DataBinding.FieldName = 'ET_NUM_EVA'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        Styles.Content = cxStyle10
        Width = 80
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 464
    Top = 8
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object SaveDialog: TSaveDialog
    Filter = 'Encuestas (*.en1)|*.en1|Todos (*.*)|*.*'
    FilterIndex = 0
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 528
    Top = 27
  end
  object OpenDialog: TOpenDialog
    Filter = 'Encuestas (*.en1)|*.en1|Todos (*.*)|*.*'
    FilterIndex = 0
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 560
    Top = 27
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 208
    Top = 8
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWindow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
  end
end
