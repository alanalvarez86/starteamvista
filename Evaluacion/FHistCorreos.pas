unit FHistCorreos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids, Buttons, StdCtrls, Mask,
     ZetaCommonClasses,
     ZBaseGridLectura_DevEx,
     ZetaDBGrid,
     ZetaKeyLookup,
     ZetaFecha,
     ZetaNumero,
     ZetaKeyCombo,
     ZetaEdit, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxClasses, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList,
  cxGridLevel, cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons, cxContainer,
  cxGroupBox;

type
  THistCorreos = class( TBaseGridLectura_DevEx)
    Panel: TPanel;
    StatusLBL: TLabel;
    Status: TZetaKeyCombo;
    Filtrar: TcxButton;
    gbFechas: TcxGroupBox;
    TipoFecha: TComboBox;
    FechaInicioLBL: TLabel;
    FechaInicio: TZetaFecha;
    FechaFinalLBL: TLabel;
    FechaFinal: TZetaFecha;
    ZetaDBGridDBTableViewWO_FROM_NA: TcxGridDBColumn;
    ZetaDBGridDBTableViewWO_TO: TcxGridDBColumn;
    ZetaDBGridDBTableViewWO_SUBJECT: TcxGridDBColumn;
    ZetaDBGridDBTableViewWO_FEC_IN: TcxGridDBColumn;
    ZetaDBGridDBTableViewWO_ENVIADO: TcxGridDBColumn;
    ZetaDBGridDBTableViewWO_FEC_OUT: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FiltrarClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FParametros: TZetaParams;
    procedure ConfigAgrupamiento;
  protected
    { Protected declarations }
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeImprimir(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Modificar; override;
  public
    { Public declarations }
    property Parametros: TZetaParams read FParametros;
  end;

var
  HistCorreos: THistCorreos;

implementation

{$R *.DFM}

uses DEvaluacion,
     ZHelpContext,
     ZAccesosTress,
     ZetaCommonTools;

const
     K_TODOS_CHAR = 'T';
     K_TODOS_TXT = 'Todos';

{ *********** THistCorreos *********** }

procedure THistCorreos.FormCreate(Sender: TObject);
begin
     inherited;
     FParametros := TZetaParams.Create;
     HelpContext := H_CONSULTAS_CORREOS;
     IndexDerechos := D_EVAL_CORREOS;
     with Self.Status do
     begin
          with Lista do
          begin
               Clear;
               Add( Format( '%s=%s', [ K_TODOS_CHAR, K_TODOS_TXT ] ) );
               Add( Format( '%s=%s', [ K_GLOBAL_SI, 'Enviados' ] ) );
               Add( Format( '%s=%s', [ K_GLOBAL_NO, 'Sin Enviar' ] ) );
          end;
          ItemIndex := 0;
     end;
     Self.TipoFecha.ItemIndex := 0;
end;

procedure THistCorreos.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('WO_FROM_NA'), K_SIN_TIPO , '', skCount);
     ConfigAgrupamiento;
     ZetaDBGridDBTableView.ApplyBestFit();
     FechaInicio.Valor := Date;
     FechaFinal.Valor := Date;
end;

procedure THistCorreos.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FParametros );
     inherited;
end;

procedure THistCorreos.Connect;
begin
     with dmEvaluacion do
     begin
          DataSource.DataSet:= cdsCorreos;
     end;
end;



procedure THistCorreos.Refresh;
begin
     Filtrar.Click;
     DoBestFit;
end;

function THistCorreos.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No es posible agregar correos';
end;

function THistCorreos.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No es posible borrar correos';
end;

function THistCorreos.PuedeModificar(var sMensaje: String): Boolean;
begin
     {
     Se verifica El Derecho De Alta debido a
     que en el �rbol de Derecho no Aparecen
     Los Derechos De Alta y De Baja
     }
     Result := CheckDerechos( K_DERECHO_CAMBIO );
     if not Result then
        sMensaje := 'No est� permitido modificar correos';
end;

function THistCorreos.PuedeImprimir(var sMensaje: String): Boolean;
begin
     {
     Se verifica El Derecho De Baja debido a
     que en el �rbol de Derecho no Aparecen
     Los Derechos De Alta y De Baja
     }
     Result := CheckDerechos( K_DERECHO_BAJA );
     if not Result then
        sMensaje := 'No est� permitido imprimir correos';
end;

procedure THistCorreos.Modificar;
begin
     dmEvaluacion.cdsCorreos.Modificar;
     DoBestFit;
end;

procedure THistCorreos.FiltrarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with Parametros do
        begin
             Clear;
             AddDate( 'Inicio', FechaInicio.Valor );
             AddDate( 'Final', FechaFinal.Valor );
             AddString( 'Remitente', VACIO );
             AddString( 'Tema', VACIO );
             AddBoolean( 'FiltrarStatus', ( Status.Llave <> K_TODOS_CHAR ) );
             AddBoolean( 'Enviado', ( Status.Llave = K_GLOBAL_SI ) );
             AddBoolean( 'FechaGeneracion', ( Self.TipoFecha.ItemIndex = 0 ) );
        end;
        dmEvaluacion.GetListaCorreos( Parametros );
        DoBestFit;
     finally
            Screen.Cursor := oCursor;
     end;
end;


procedure THistCorreos.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //activa la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Muestra la caja de agrupamiento
end;

end.


