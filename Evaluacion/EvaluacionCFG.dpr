program EvaluacionCFG;

uses
  MidasLib,
  Forms,
  ZetaClientTools,
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DBaseDiccionario in '..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DBaseReportes in '..\DataModules\DBaseReportes.pas' {dmBaseReportes: TDataModule},
  DBaseSistema in '..\DataModules\DBaseSistema.pas' {dmBaseSistema: TDataModule},
  dBaseTressDiccionario in '..\DataModules\dBaseTressDiccionario.pas',
  ZBaseConsulta in '..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  ZBaseEditAccesos in '..\Tools\ZBaseEditAccesos.pas' {ZetaEditAccesos},
  dBaseGlobal in '..\DataModules\dBaseGlobal.pas',
  DBaseCliente in '..\DataModules\DBaseCliente.pas' {BaseCliente: TDataModule},
  ZBaseConsultaBotones in '..\Tools\ZBaseConsultaBotones.pas' {BaseBotones},
  ZWizardBasico in '..\Tools\ZWizardBasico.pas' {WizardBasico},
  FTressShell in 'FTressShell.pas' {TressShell},
  ZBaseGlobal in '..\Tools\ZBaseGlobal.pas' {BaseGlobal},
  ZBasicoNavBarShell in '..\Tools\ZBasicoNavBarShell.pas' {BasicoNavBarShell},
  DCatalogos in 'DCatalogos.pas' {dmCatalogos: TDataModule};

{$R *.RES}
{$R WindowsXP.res}
{***DevEx (by am): Spanish.RES es el recurso con todas las traducciones de textos para los componentes de
DevExpress. El recruso es controlado por los componentes cxLocalizer que se encuentran en el Shell y en la forma
de la Matriz de habilidades. Puede haber varios cxLocalizer en el proyecto, pero debe haber un solo recurso de traduccion.
Estos recursos son generados desde un proyecto el cual se encuentra en StartTeam (Traducciones\Spanish.ini),. Cuando se
requiera agregar un texto el Spanish.ini debe ser modificado, y el Spanish.RES debe ser construido y cambiado en los folders
de los proyectos.

La siguiente directiva es la que indica que se utilizara este recurso el cual debe estar presente en el folder del proyecto.
***}

{$R ..\Traducciones\Spanish.RES}

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  Application.Title := 'Administrador Evaluaci�n/Encuestas';
  Application.HelpFile := 'Evaluaci�n de desempe�o.chm';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login( False ) then
       begin
            Show;
            Update;
            BeforeRun;
            Application.Run;
       end
       else
       begin
            Free;
       end;
  end;
end.

