inherited CatUsuarios: TCatUsuarios
  Left = 327
  Top = 273
  Caption = 'Usuarios'
  ClientHeight = 257
  ClientWidth = 885
  ExplicitWidth = 885
  ExplicitHeight = 257
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 885
    ExplicitWidth = 885
    inherited Slider: TSplitter
      Left = 357
      ExplicitLeft = 357
    end
    inherited ValorActivo1: TPanel
      Width = 341
      ExplicitWidth = 341
      inherited textoValorActivo1: TLabel
        Width = 335
      end
    end
    inherited ValorActivo2: TPanel
      Left = 360
      Width = 525
      ExplicitLeft = 360
      ExplicitWidth = 525
      inherited textoValorActivo2: TLabel
        Width = 519
        ExplicitLeft = 439
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 885
    Height = 41
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 17
      Top = 14
      Width = 33
      Height = 13
      Caption = 'Busca:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object BBusca: TcxButton
      Left = 323
      Top = 10
      Width = 21
      Height = 21
      Hint = 'Buscar usuario'
      OptionsImage.Glyph.Data = {
        DA050000424DDA05000000000000360000002800000013000000130000000100
        200000000000A4050000000000000000000000000000000000008B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFFD2CED2FFD0CCD0FF908791FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFC2BCC2FFFFFF
        FFFFFFFFFFFFD9D5D9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFFB1ABB2FFFFFFFFFFFFFFFFFFF4F3F4FF988F99FF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFFA49DA5FFFAF9FAFFFFFFFFFFFAF9FAFFA39BA3FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFFAFA9B0FFDEDBDFFFF4F3F4FFFBFBFBFFE2DFE2FFF4F3F4FFFFFFFFFFFDFD
        FDFFAFA9B0FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFFC5C0C6FFFFFFFFFFFFFFFFFFF8F7F8FFF2F1F2FFFFFF
        FFFFFFFFFFFFFFFFFFFFC0BAC0FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFFA39BA3FFFFFFFFFFF8F7F8FFBEB8
        BFFF8F8590FF8B818CFFA39BA3FFEDEBEDFFFFFFFFFFC2BCC2FF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFD2CE
        D2FFFFFFFFFFBEB8BFFF8B818CFF8B818CFF8B818CFF8B818CFFA69FA7FFFFFF
        FFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFFE0DDE0FFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B81
        8CFF8B818CFF908791FFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCCC8CCFFFFFFFFFFC5C0
        C6FF8B818CFF8B818CFF8B818CFF8B818CFFAFA9B0FFFFFFFFFFEDEBEDFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFFA39BA3FFFDFDFDFFFDFDFDFFBCB6BDFF948B95FF8D838EFFAFA9B0FFF4F3
        F4FFFFFFFFFFBCB6BDFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFFBAB4BBFFFFFFFFFFFFFFFFFFFDFD
        FDFFFAF9FAFFFFFFFFFFFFFFFFFFD0CCD0FF8D838EFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFFA199A2FFD7D4D7FFEDEBEDFFEDEBEDFFD9D5D9FFAEA7AEFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
      ParentShowHint = False
      ShowHint = True
      SpeedButtonOptions.GroupIndex = 1
      SpeedButtonOptions.AllowAllUp = True
      TabOrder = 1
      OnClick = BBuscaClick
    end
    object EBuscaNombre: TEdit
      Left = 52
      Top = 10
      Width = 268
      Height = 21
      TabOrder = 0
      OnChange = EBuscaNombreChange
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 60
    Width = 885
    Height = 197
    TabOrder = 2
    ExplicitTop = 60
    ExplicitWidth = 885
    ExplicitHeight = 197
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsData.DeletingConfirmation = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      Styles.Content = cxStyle1
      Styles.Header = cxStyle2
      Styles.Indicator = cxStyle2
      object US_CODIGO_GRID: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'US_CODIGO'
        Styles.Content = cxStyle3
        Width = 47
      end
      object US_NOMBRE_GRID: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'US_NOMBRE'
        Styles.Content = cxStyle4
        Width = 214
      end
      object US_EMAIL_GRID: TcxGridDBColumn
        Caption = 'E-mail'
        DataBinding.FieldName = 'US_EMAIL'
        Styles.Content = cxStyle5
        Width = 179
      end
      object US_FORMATO_GRID: TcxGridDBColumn
        Caption = 'Formato'
        DataBinding.FieldName = 'US_FORMATO'
        Styles.Content = cxStyle6
        Width = 94
      end
      object US_JEFE_GRID: TcxGridDBColumn
        Caption = 'Reporta a'
        DataBinding.FieldName = 'US_JEFE'
        Styles.Content = cxStyle7
        Width = 56
      end
      object NOM_JEFE_GRID: TcxGridDBColumn
        Caption = 'Nombre de a quien reporta'
        DataBinding.FieldName = 'NOM_JEFE'
        Styles.Content = cxStyle8
        Width = 229
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 424
    Top = 0
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
    DesignInfo = 1049232
  end
  inherited ActionList: TActionList
    Left = 698
    Top = 24
  end
  inherited PopupMenu1: TPopupMenu
    Left = 624
    Top = 24
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 496
    Top = 8
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWindow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
  end
end
