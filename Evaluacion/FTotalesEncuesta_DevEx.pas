unit FTotalesEncuesta_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, StdCtrls, DBCtrls, ZetaDBTextBox, Grids,
  TeeProcs, TeEngine, Chart, Series, ComCtrls, VclTee.TeeGDIPlus, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  cxTextEdit, cxMemo, cxDBEdit, dxBarBuiltInMenu, cxPC, dxGDIPlusClasses,
  Vcl.ImgList;

type
  TTotalesEncuesta_DevEx = class(TBaseConsulta)
    Panel1: TPanel;
    Label1: TLabel;
    ET_CODIGO: TZetaDBTextBox;
    ET_NOMBRE: TZetaDBTextBox;
    Label2: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    DBText1: TDBText;
    GroupBox1: TGroupBox;
    Label10: TLabel;
    ET_NIVELES: TZetaDBTextBox;
    Label11: TLabel;
    ET_PESOS: TZetaDBTextBox;
    Label7: TLabel;
    ztbResp: TZetaTextBox;
    ET_FEC_INI: TZetaDBTextBox;
    Label4: TLabel;
    Label8: TLabel;
    ET_NUM_COM: TZetaDBTextBox;
    Label9: TLabel;
    Label5: TLabel;
    ET_FEC_FIN: TZetaDBTextBox;
    ET_NUM_PRE: TZetaDBTextBox;
    pcGraficas: TcxPageControl;
    GraficaEvaluaciones: TChart;
    Series1: TBarSeries;
    Series2: TPieSeries;
    Panel2: TPanel;
    TablaEvaluaciones: TStringGrid;
    lblEvaluaciones: TLabel;
    Panel3: TPanel;
    lblEvaluados: TLabel;
    TablaEvaluados: TStringGrid;
    GraficaEvaluados: TChart;
    BarSeries1: TBarSeries;
    PieSeries1: TPieSeries;
    Panel4: TPanel;
    lblEvaluadores: TLabel;
    TablaEvaluadores: TStringGrid;
    GraficaEvaluadores: TChart;
    BarSeries2: TBarSeries;
    PieSeries2: TPieSeries;
    ET_DESCRIP: TcxDBMemo;
    Evaluaciones: TcxTabSheet;
    Evaluadores: TcxTabSheet;
    Evaluados: TcxTabSheet;
    imgStatus: TImage;
    Panel5: TPanel;
    statusImage: TcxImageList;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
    procedure ObtieneResponsable;
    procedure CambiaImagenStatus;
    procedure ConstruyeTablas;
    procedure ConstruyeTablaEvaluaciones;
    procedure CreaGraficaEvaluaciones;
    procedure ConstruyeTablaEvaluados;
    procedure CreaGraficaEvaluados;
    procedure ConstruyeTablaEvaluadores;
    procedure CreaGraficaEvaluadores;
  public
    { Public declarations }
  end;

const
     K_NO_EMPEZADO = 'NO_EMPEZADO';
     K_CONTESTARON_ALGUNAS = 'CONTEST_ALGUNAS';
     K_TERMINARON = 'TERMINARON';
     Q_NO_EMPEZADO = 'No han iniciado';
     Q_CONTESTARON_ALGUNAS = 'Contestaron algunas';
     Q_TERMINARON = 'Ya terminaron';

var
  TotalesEncuesta_DevEx: TTotalesEncuesta_DevEx;

implementation

{$R *.DFM}
uses DEvaluacion,
     DSistema,
     ZHelpContext,
     ZetaBuscador,
     ZetaCommonLists,
     ZetaCommonClasses,
     DCliente;

{ TTotalesEncuesta }
procedure TTotalesEncuesta_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEvaluacion;
     pcGraficas.ActivePage := Evaluaciones;
     HelpContext := H_RESULTADOS_ENCUESTA_TOTALES_ENCUESTA;
     { GA: No tiene efecto, dado que lo que se utiliza es el HelpContext de la forma
     tsEvaluaciones.HelpContext := H_RESULTADOS_ENCUESTA_TOTALES_ENCUESTA_EVALUACIONES;
     tsEvaluadores.HelpContext := H_RESULTADOS_ENCUESTA_TOTALES_ENCUESTA_EVALUADORES;
     tsEvaluados.HelpContext := H_RESULTADOS_ENCUESTA_TOTALES_ENCUESTA_EVALUADOS;
     }
end;

procedure TTotalesEncuesta_DevEx.Agregar;
begin
     dmEvaluacion.cdsTotalesEncuesta.Agregar;
end;

procedure TTotalesEncuesta_DevEx.Borrar;
begin
     dmEvaluacion.cdsTotalesEncuesta.Borrar;
end;

procedure TTotalesEncuesta_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmEvaluacion do
     begin
          //MA: Se est� utilizando el 'Refrescar' y no el 'Conectar' debido
          // a que est� ligado el m�todo SetDatachange cuando cambie la Encuesta
          // como valor activo.
          cdsTotalesEncuesta.Refrescar;
          DataSource.DataSet:= cdsTotalesEncuesta;
     end;
     ObtieneResponsable;
     ConstruyeTablas;
     CambiaImagenStatus;
end;

procedure TTotalesEncuesta_DevEx.CreaGraficaEvaluaciones;
begin
     with Series2 do
     begin
          Clear;
          with dmEvaluacion.TotalesEvaluaciones do
          begin
               AddPie( Items[ Ord(sevNueva) ].AsInteger, ObtieneElemento( lfStatusEvaluacion, Ord(sevNueva) ), clDefault );
               AddPie( Items[ Ord(sevLista) ].AsInteger, ObtieneElemento( lfStatusEvaluacion, Ord(sevLista) ), clDefault );
               AddPie( Items[ Ord(sevPreparada) ].AsInteger, ObtieneElemento( lfStatusEvaluacion, Ord(sevPreparada) ), clDefault );
               AddPie( Items[ Ord(sevEnProceso) ].AsInteger, ObtieneElemento( lfStatusEvaluacion, Ord(sevEnProceso) ), clDefault );
               AddPie( Items[ Ord(sevTerminada) ].AsInteger, ObtieneElemento( lfStatusEvaluacion, Ord(sevTerminada) ), clDefault );
          end;
     end;
end;

procedure TTotalesEncuesta_DevEx.CreaGraficaEvaluados;
begin
     with PieSeries1 do
     begin
          Clear;
          with dmEvaluacion.TotalesEvaluados do
          begin
               AddPie( Items[ Ord(ssNuevo) ].AsInteger, ObtieneElemento( lfStatusSujeto, Ord(ssNuevo) )+'s', clDefault );
               AddPie( Items[ Ord(ssListo) ].AsInteger, ObtieneElemento( lfStatusSujeto, Ord(ssListo) )+'s', clDefault );
               AddPie( Items[ Ord(ssPreparado) ].AsInteger, ObtieneElemento( lfStatusSujeto, Ord(ssPreparado) ), clDefault );
               AddPie( Items[ Ord(ssEnProceso) ].AsInteger, ObtieneElemento( lfStatusSujeto, Ord(ssEnProceso) ), clDefault );
               AddPie( Items[ Ord(ssTerminado) ].AsInteger, ObtieneElemento( lfStatusSujeto, Ord(ssTerminado) )+'s', clDefault );
          end;
     end;
end;

procedure TTotalesEncuesta_DevEx.CreaGraficaEvaluadores;
begin
     with PieSeries2 do
     begin
          Clear;
          with dmEvaluacion.TotalesEvaluadores do
          begin
               AddPie( ParamByName( K_NO_EMPEZADO ).AsInteger, Q_NO_EMPEZADO, clDefault );
               AddPie( ParamByName( K_CONTESTARON_ALGUNAS ).AsInteger, Q_CONTESTARON_ALGUNAS, clDefault );
               AddPie( ParamByName( K_TERMINARON ).AsInteger, Q_TERMINARON, clDefault );
          end;
     end;
end;


procedure TTotalesEncuesta_DevEx.ConstruyeTablas;
begin
     ConstruyeTablaEvaluaciones;
     ConstruyeTablaEvaluadores;
     ConstruyeTablaEvaluados;
end;

procedure TTotalesEncuesta_DevEx.ConstruyeTablaEvaluaciones;
var
   eStEvaluacion: eStatusEvaluacion;
   iNueva, iLista, iPreparada, iEnProceso, iTerminada, iTotal: Integer;
begin
     with TablaEvaluaciones do
     begin
          with dmEvaluacion do
          begin
               Cols[0].Add('   Status ');
               Cols[1].Add('Cantidad');
               for eStEvaluacion := Low( eStatusEvaluacion ) to High( eStatusEvaluacion ) do
               begin
                    Rows[ Ord( eStEvaluacion ) + 1 ].Add( ObtieneElemento( lfStatusEvaluacion, Ord(eStEvaluacion) ) );
               end;
               with TotalesEvaluaciones do
               begin
                    iNueva := Items[ Ord(sevNueva) ].AsInteger;
                    iLista := Items[ Ord(sevLista) ].AsInteger;
                    iPreparada := Items[ Ord(sevPreparada) ].AsInteger;
                    iEnProceso := Items[ Ord(sevEnProceso) ].AsInteger;
                    iTerminada := Items[ Ord(sevTerminada) ].AsInteger;
               end;
               Cells[1,1] := InttoStr( iNueva );
               Cells[1,2] := InttoStr( iLista );
               Cells[1,3] := InttoStr( iPreparada );
               Cells[1,4] := InttoStr( iEnProceso );
               Cells[1,5] := InttoStr( iTerminada );
               iTotal := iNueva + iLista + iPreparada + iEnProceso + iTerminada;
               lblEvaluaciones.Caption := Format ( 'Evaluaciones: %d', [ iTotal ] );
          end;
     end;
     CreaGraficaEvaluaciones;
end;

procedure TTotalesEncuesta_DevEx.ConstruyeTablaEvaluadores;
var
   iNoEmpezaron, iEnProceso, iTerminaron, iTotal: Integer;
begin
     with TablaEvaluadores do
     begin
          with dmEvaluacion do
          begin
               Cols[0].Add('   Status ');
               Cols[1].Add('Cantidad');
               Rows[ 1 ].Add( Q_NO_EMPEZADO );
               Rows[ 2 ].Add( Q_CONTESTARON_ALGUNAS );
               Rows[ 3 ].Add( Q_TERMINARON );

               with TotalesEvaluadores do
               begin
                    iNoEmpezaron := ParamByName( K_NO_EMPEZADO ).AsInteger;
                    iEnProceso := ParamByName( K_CONTESTARON_ALGUNAS ).AsInteger;
                    iTerminaron := ParamByName( K_TERMINARON ).AsInteger;
               end;
               Cells[1,1] := InttoStr( iNoEmpezaron );
               Cells[1,2] := InttoStr( iEnProceso );
               Cells[1,3] := InttoStr( iTerminaron );
               iTotal := iNoEmpezaron + iEnProceso + iTerminaron;
               lblEvaluadores.Caption := Format ( 'Evaluadores: %d', [ iTotal ] );
          end;
     end;
     CreaGraficaEvaluadores;
end;

procedure TTotalesEncuesta_DevEx.ConstruyeTablaEvaluados;
var
   sNombre: String;
   eStSujeto: eStatusSujeto;
   iNuevo, iListo, iPreparado, iEnProceso, iTerminado, iTotal: Integer;
begin
     with TablaEvaluados do
     begin
          with dmEvaluacion do
          begin
               Cols[0].Add('   Status ');
               Cols[1].Add('Cantidad');
               for eStSujeto := Low( eStatusSujeto ) to High( eStatusSujeto ) do
               begin
                    sNombre :=  ObtieneElemento( lfStatusSujeto, Ord(eStSujeto) );
                    if( ( Ord( eStSujeto ) <> Ord( ssPreparado )  ) and ( Ord( eStSujeto ) <> Ord( ssEnProceso ) ) )then
                        sNombre := sNombre + 's';
                    Rows[ Ord( eStSujeto ) + 1 ].Add( sNombre );
               end;
               with TotalesEvaluados do
               begin
                    iNuevo := Items[ Ord(ssNuevo) ].AsInteger;
                    iListo := Items[ Ord(ssListo) ].AsInteger;
                    iPreparado := Items[ Ord(ssPreparado) ].AsInteger;
                    iEnProceso := Items[ Ord(ssEnProceso) ].AsInteger;
                    iTerminado := Items[ Ord(ssTerminado) ].AsInteger;
               end;
               Cells[1,1] := InttoStr( iNuevo );
               Cells[1,2] := InttoStr( iListo );
               Cells[1,3] := InttoStr( iPreparado );
               Cells[1,4] := InttoStr( iEnProceso );
               Cells[1,5] := InttoStr( iTerminado );
               iTotal := iNuevo + iListo + iPreparado + iEnProceso + iTerminado;
               lblEvaluados.Caption := Format ( 'Evaluados: %d', [ iTotal ] );
          end;
     end;
     CreaGraficaEvaluados;
end;


procedure TTotalesEncuesta_DevEx.ObtieneResponsable;
var
   sResponsable: String;
begin
     with dmSistema.cdsUsuarios do
     begin
          if( Locate( 'US_CODIGO', dmEvaluacion.cdsTotalesEncuesta.FieldbyName('ET_USR_ADM').AsInteger, [] ) )then
              sResponsable := InttoStr( FieldByName('US_CODIGO').AsInteger ) + ' = ' + FieldByName('US_NOMBRE').AsString
          else
              sResponsable := ' No tiene un responsable';
     end;
     ztbResp.Caption := sResponsable;
end;

procedure TTotalesEncuesta_DevEx.CambiaImagenStatus;
var
  ABitmap: TcxAlphaBitmap;
  APngImage: TdxPNGImage;
  i:Integer;
begin
     with dmCliente.GetDatosEncuestaActiva do
     begin
          ABitmap := TcxAlphaBitmap.CreateSize(128, 128);
          APngImage := nil;
          ABitmap.Clear;
          if( Status = stenCerrada )then
              i := 1;
          if Status = stenAbierta then
              i := 2;
          if Status = stenDiseno then
              i := 0;

          try
              ABitmap.Clear;
              statusImage.GetBitmap(i, ABitmap);
              APngImage := TdxPNGImage.CreateFromBitmap(ABitmap);
              imgStatus.Picture.Graphic := APngImage;
              imgStatus.Hint := 'Encuesta ' +  LowerCase(DBText1.Caption);
          finally
              APngImage.Free;
              ABitmap.Free;
          end;

     end;
end;

procedure TTotalesEncuesta_DevEx.ImprimirForma;
begin
  inherited;

end;

procedure TTotalesEncuesta_DevEx.Modificar;
begin
     dmEvaluacion.cdsTotalesEncuesta.Modificar;
end;

procedure TTotalesEncuesta_DevEx.Refresh;
begin
     dmEvaluacion.cdsTotalesEncuesta.Refrescar;
end;

function TTotalesEncuesta_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se pueden agregar encuestas desde esta forma';
     Result := False;
end;

function TTotalesEncuesta_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se pueden borrar encuestas desde esta forma';
     Result := False;
end;

function TTotalesEncuesta_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se pueden modificar encuestas desde esta forma';
     Result := False;
end;


end.
