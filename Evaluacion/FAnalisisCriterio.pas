unit FAnalisisCriterio;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, StdCtrls, Buttons, Mask, ZetaNumero,
  ZetaDBTextBox, Grids, DBGrids, ZetaDBGrid, TeeProcs, TeEngine, Chart,
  DBChart, Series, ComCtrls;

type
  TAnalisisCriterio = class(TBaseConsulta)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    ztbCuantos: TZetaTextBox;
    lblPrimeros: TLabel;
    edPrimeros: TZetaNumero;
    znPromedios: TComboBox;
    btnFiltrar: TBitBtn;
    Label5: TLabel;
    EV_RELACIO: TComboBox;
    Grafica: TDBChart;
    Series1: THorizBarSeries;
    Panel2: TPanel;
    Grid1: TZetaDBGrid;
    Splitter1: TSplitter;
    StatusBar1: TStatusBar;
    procedure znPromediosChange(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Grid1TitleClick(Column: TColumn);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure LlenaListaRelacion;
    procedure CuantosRegistros;
    procedure InicializaValores;
    //procedure OrdenaGrid( const iOrdenarPor: Integer );
    procedure CargaFiltros;
    procedure CreaGrafica;
    procedure SetControles;

  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
  end;

var
  AnalisisCriterio: TAnalisisCriterio;

implementation

{$R *.DFM}
uses DEvaluacion,
     ZHelpContext,
     ZetaClientTools,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists;

{ TAnalisisCriterio }
procedure TAnalisisCriterio.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEvaluacion;
     HelpContext := H_RESULTADOS_ENCUESTA_ANALISIS_CRITERIO;
end;

procedure TAnalisisCriterio.FormShow(Sender: TObject);
begin
     inherited;
     InicializaValores;
     SetControles;
     LlenaListaRelacion;
end;

procedure TAnalisisCriterio.Agregar;
begin
     dmEvaluacion.cdsAnalisisCriterio.Agregar;
end;

procedure TAnalisisCriterio.Borrar;
begin
     dmEvaluacion.cdsAnalisisCriterio.Borrar;
end;

procedure TAnalisisCriterio.Connect;
begin
     with dmEvaluacion do
     begin
          if not cdsAnalisisCriterio.Active or cdsAnalisisCriterio.HayQueRefrescar then
          begin
               CargaFiltros;
               CreaGrafica;
               cdsAnalisisCriterio.ResetDataChange;
          end;
          DataSource.DataSet := cdsAnalisisCriterio;
     end;
end;

procedure TAnalisisCriterio.ImprimirForma;
begin
  inherited;
end;

procedure TAnalisisCriterio.Modificar;
begin
     dmEvaluacion.cdsAnalisisCriterio.Modificar;
end;

function TAnalisisCriterio.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede agregar el registro desde esta forma';
     Result := False;
end;

function TAnalisisCriterio.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede borrar el registro desde esta forma';
     Result := False;
end;

procedure TAnalisisCriterio.Refresh;
begin
     //dmEvaluacion.cdsAnalisisCriterio.Refrescar;
     CargaFiltros;
end;

procedure TAnalisisCriterio.LlenaListaRelacion;
begin
     with dmEvaluacion.cdsPerfilEvaluador do
     begin
          EV_RELACIO.Clear;
          Conectar;
          First;
          DisableControls;
          try
             EV_RELACIO.Items.AddObject( 'Todas', TObject( 0 ) );
             while not EOF do
             begin
                  EV_RELACIO.Items.AddObject( FieldByName('ER_NOMBRE').AsString, TObject( FieldByName('ER_TIPO').AsInteger ) );
                  Next;
             end;
          finally
                 EnableControls;
          end;
     end;
     EV_RELACIO.ItemIndex := 0;
end;

procedure TAnalisisCriterio.InicializaValores;
begin
     znPromedios.ItemIndex := 0;
     edPrimeros.Valor := 5;
end;

procedure TAnalisisCriterio.CuantosRegistros;
begin
     //ztbCuantos.Caption := inttoStr( dmEvaluacion.cdsAnalisisCriterio.RecordCount );
     StatusBar1.Panels[0].Text := 'Cantidad : ' + inttoStr( dmEvaluacion.cdsAnalisisCriterio.RecordCount );
end;

{procedure TAnalisisCriterio.OrdenaGrid( const iOrdenarPor: Integer );
begin
     with dmEvaluacion.cdsAnalisisCriterio do
     begin
          case( iOrdenarPor ) of
                0: IndexName := 'EC_ORDENIndex';
                1: IndexName := 'PROMEDIOIndexD';
                2: IndexName := 'PROMEDIOIndex';
          else
              IndexName := VACIO;
          end;
     end;
end;}

procedure TAnalisisCriterio.CargaFiltros;
var
   iMostrarProm, iRelacion, iMostrar: Integer;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmEvaluacion.cdsAnalisisCriterio do
        begin
             DisableControls;
             try
                iMostrarProm := znPromedios.ItemIndex;
                iMostrar := edPrimeros.ValorEntero;
                if EV_RELACIO.ItemIndex > 0 then
                   iRelacion := Integer( EV_RELACIO.Items.Objects[ EV_RELACIO.ItemIndex ] ) //EV_RELACIO.ItemIndex;
                else
                    iRelacion := -1;
                //OrdenaGrid( iMostrarProm );
                //dmEvaluacion.ObtenerAnalisisCriterio( iMostrar, ( iRelacion - 1 ), iMostrarProm );
                dmEvaluacion.ObtenerAnalisisCriterio( iMostrar, ( iRelacion ), iMostrarProm );
                CreaGrafica;
             finally
                    EnableControls;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     CuantosRegistros;
end;

procedure TAnalisisCriterio.CreaGrafica;
begin
     with Series1 do
     begin
          with dmEvaluacion.cdsAnalisisCriterio do
          begin
               Clear;
               DisableControls;
               try
                  Last;
                  while not BOF do
                  begin
                       AddBar( FieldByName('PROMEDIO').AsFloat, FieldByName('EC_NOMBRE').AsString, clFuchsia );
                       Prior;
                  end;
               finally
                      EnableControls;
               end;
          end;
     end;
end;

procedure TAnalisisCriterio.znPromediosChange(Sender: TObject);
begin
     inherited;
     SetControles;
end;

procedure TAnalisisCriterio.SetControles;
begin
     edPrimeros.Enabled := ( znPromedios.ItemIndex <> 0 );
     lblPrimeros.Enabled := edPrimeros.Enabled;
     if( edPrimeros.Enabled ) then
     begin
          if( edPrimeros.Valor  = 0 )then
              edPrimeros.Valor := 5;
     end
     else
         edPrimeros.Valor := 0;
end;

procedure TAnalisisCriterio.btnFiltrarClick(Sender: TObject);
begin
     CargaFiltros;
end;

procedure TAnalisisCriterio.Grid1TitleClick(Column: TColumn);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        Grid1.OrdenarPor( Column );
        CreaGrafica;
     finally
            Screen.Cursor := oCursor;
     end;
end;

end.
