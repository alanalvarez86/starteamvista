inherited ModificaEmpleado_DevEx: TModificaEmpleado_DevEx
  Left = 312
  Top = 317
  Caption = 'Modificar Evaluador'
  ClientHeight = 141
  ClientWidth = 437
  OldCreateOrder = True
  OnClose = FormClose
  OnShow = FormShow
  ExplicitWidth = 443
  ExplicitHeight = 170
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 16
    Top = 8
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Encuesta:'
  end
  object Label2: TLabel [1]
    Left = 14
    Top = 32
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empleado:'
  end
  object Label3: TLabel [2]
    Left = 13
    Top = 56
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = 'Evaluador:'
  end
  object Label4: TLabel [3]
    Left = 19
    Top = 80
    Width = 45
    Height = 13
    Alignment = taRightJustify
    Caption = 'Relaci'#243'n:'
  end
  object ztbTitulo: TZetaTextBox [4]
    Left = 68
    Top = 6
    Width = 357
    Height = 17
    AutoSize = False
    Caption = 'ztbTitulo'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object ztbEmpleado: TZetaTextBox [5]
    Left = 68
    Top = 30
    Width = 357
    Height = 17
    AutoSize = False
    Caption = 'ztbEmpleado'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object ztbEvaluador: TZetaTextBox [6]
    Left = 68
    Top = 54
    Width = 357
    Height = 17
    AutoSize = False
    Caption = 'ztbEvaluador'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  inherited PanelBotones: TPanel
    Top = 105
    Width = 437
    ExplicitTop = 105
    ExplicitWidth = 437
    inherited OK_DevEx: TcxButton
      Left = 268
      OnClick = OK_DevExClick
      ExplicitLeft = 268
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 348
      ParentFont = False
      ExplicitLeft = 348
    end
  end
  object zkcRelacion: TZetaKeyCombo [8]
    Left = 68
    Top = 76
    Width = 250
    Height = 21
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 1
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
