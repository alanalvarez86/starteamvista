unit FEditTCompetencias;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaKeyCombo,
  ZetaNumero, Mask, ZetaEdit, ZetaSmartLists, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, cxControls, dxBarExtItems,
  dxBar, cxClasses, Vcl.ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditTCompetencias = class(TBaseEdicion_DevEx)
    TC_CODIGO: TZetaDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    TC_DESCRIP: TDBEdit;
    TC_INGLES: TDBEdit;
    Label3: TLabel;
    Label5: TLabel;
    TC_NUMERO: TZetaDBNumero;
    TC_TEXTO: TDBEdit;
    Label6: TLabel;
    TC_TIPO: TZetaDBKeyCombo;
    Label4: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditTCompetencias: TEditTCompetencias;

implementation

{$R *.DFM}

uses DEvaluacion,
     ZHelpContext,
     ZetaBuscador_DevEx,
     ZetaCommonClasses,
     ZAccesosTress;

{ TEditTCompetencias }

procedure TEditTCompetencias.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_EVAL_TCOMPETEN;
     FirstControl := TC_CODIGO;
     HelpContext:= H_CATALOGOS_TIPOS_COMPETENCIA_AGREGAR_REGISTRO;
end;

procedure TEditTCompetencias.Connect;
begin
     DataSource.DataSet := dmEvaluacion.cdsTCompetencia;
end;

procedure TEditTCompetencias.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'TCOMPETE', 'TC_CODIGO', dmEvaluacion.cdsTCompetencia );
end;

end.
