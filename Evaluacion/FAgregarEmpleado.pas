unit FAgregarEmpleado;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, Buttons, ExtCtrls, ZetaDBTextBox, ZetaKeyCombo,
  Grids, DBGrids, ZetaDBGrid, Db;

type
  TAgregarEmpleado = class(TZetaDlgModal)
    Panel1: TPanel;
    Label1: TLabel;
    ztbTitulo: TZetaTextBox;
    Label2: TLabel;
    ztbEmpleado: TZetaTextBox;
    Panel2: TPanel;
    Label3: TLabel;
    edBuscar: TEdit;
    ZetaDBGrid1: TZetaDBGrid;
    Panel3: TPanel;
    Label4: TLabel;
    zkcRelacion: TZetaKeyCombo;
    dsAgregaEmp: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure edBuscarKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure OKClick(Sender: TObject);
    procedure ZetaDBGrid1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    FEmpleado: Integer;
    FTipoRelacion: Integer;
    FEsAutoevaluacion: Boolean;
    procedure Conectaforma;
    function AgregaNodo: Boolean;
  public
    { Public declarations }
    property TipoRelacion: Integer read FTipoRelacion write FTipoRelacion;
    property EsAutoevaluacion: Boolean read FEsAutoevaluacion write FEsAutoevaluacion;
  end;

var
  AgregarEmpleado: TAgregarEmpleado;
  procedure AbreAgregaEmpleadoEvaluar( const iRelacion: Integer; const lAutoevaluacion: Boolean );

implementation

uses DEvaluacion,
     DSistema,
     DCliente,
     ZHelpContext,
     ZetaCommonTools,
     FEmpleadosEvaluar,
     FTressShell;

{$R *.DFM}

procedure AbreAgregaEmpleadoEvaluar( const iRelacion: Integer; const lAutoevaluacion: Boolean );
const
     K_POS_X = 437;
     K_POS_Y = 144;
var
   oAgregarEmpleado: TAgregarEmpleado;
begin
     oAgregarEmpleado := TAgregarEmpleado.Create( Application );
     try
        with oAgregarEmpleado do
        begin
             Left := K_POS_X;
             Top := K_POS_Y;
             TipoRelacion := iRelacion;
             EsAutoevaluacion := lAutoevaluacion;
             ShowModal;
        end;
     finally
            FreeAndNil( oAgregarEmpleado );
     end;
end;

procedure TAgregarEmpleado.FormCreate(Sender: TObject);
begin
     inherited;
     dmSistema.cdsUsuarios.Filter := '';
     HelpContext := H_DISENO_ENCUESTA_EMPLEADOS_EVALUAR_ASIGNAR_EVALUADORES;
end;

procedure TAgregarEmpleado.FormShow(Sender: TObject);
begin
     inherited;
     with dmEvaluacion do
     begin
          FEmpleado := cdsEmpEvaluar.FieldByName('CB_CODIGO').AsInteger;
          ztbTitulo.Caption := InttoStr( dmCliente.Encuesta ) + ' = ' + dmCliente.Nombre;
          ztbEmpleado.Caption := InttoStr( FEmpleado ) + ' = ' +
                                           cdsEmpEvaluar.FieldByName('PRETTYNAME').AsString;
          LlenaPerfilEvaluadores( zkcRelacion.Lista );
     end;
     zkcRelacion.LlaveEntero := TipoRelacion;
     edBuscar.SetFocus;
     ConectaForma;
end;

procedure TAgregarEmpleado.Conectaforma;
begin
     with dmSistema do
     begin
          with cdsUsuarios do
          begin
               Filtered := False;
               Conectar;
               Filter := ' US_BLOQUEA = ''N'' ';
               Filtered := True;
          end;
          dsAgregaEmp.DataSet := cdsUsuarios;
          //MA: Sugiere el mismo empleado al darle doble click en Autoevaluación
          if( EsAutoevaluacion )then
              cdsUsuarios.Locate( 'CB_CODIGO', FEmpleado, [] );
     end;
end;

procedure TAgregarEmpleado.edBuscarKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     inherited;
     with dmSistema.cdsUsuarios do
     begin
          DisableControls;
          try
             Filtered := False;
             Filter := ' US_BLOQUEA = ''N'' and UPPER(US_NOMBRE) like ' + EntreComillas( '%' + UpperCase(edBuscar.Text)  + '%' );
             Filtered := True;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TAgregarEmpleado.OKClick(Sender: TObject);
begin
     inherited;
     if( AgregaNodo )then
         ModalResult := mrOk
     else
         ModalResult := mrNone;
end;

function TAgregarEmpleado.AgregaNodo: Boolean;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmEvaluacion do
        begin
             Result := AgregaSujetoArbol( dmSistema.cdsUsuarios.FieldByName('US_CODIGO').AsInteger, zkcRelacion.LlaveEntero, cdsEmpEvaluar.FieldByName('CB_CODIGO').AsInteger, True );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TAgregarEmpleado.ZetaDBGrid1DblClick(Sender: TObject);
begin
     inherited;
     AgregaNodo;
end;

procedure TAgregarEmpleado.FormKeyPress(Sender: TObject; var Key: Char);
begin
     inherited;
     if( Key = #27 )then
         Self.Close;
end;

end.
