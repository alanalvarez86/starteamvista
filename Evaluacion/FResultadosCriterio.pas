unit FResultadosCriterio;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx,
  Db, ExtCtrls, ZetaKeyLookup, StdCtrls, Buttons,
  ZetaDBTextBox, Mask, ZetaNumero, Grids, DBGrids, ZetaDBGrid, ComCtrls,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxTextEdit, cxClasses, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList,
  cxGridLevel, cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons;

type
  TResutadosCriterio = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    btnFiltrar: TcxButton;
    Label3: TLabel;
    znPromedios: TComboBox;
    lblPrimeros: TLabel;
    edPrimeros: TZetaNumero;
    EC_NOMBRE_GRID: TcxGridDBColumn;
    SO_NUM_TOT_GRID: TcxGridDBColumn;
    SO_TOTAL_GRID: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    procedure znPromediosChange(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ConfigAgrupamiento;
    procedure InicializaValores;
    procedure SetControles;
    procedure CargarFiltros;
    //procedure OrdenaGrid( const iOrdenarPor: Integer );
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function Puedemodificar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
  end;

var
  ResutadosCriterio: TResutadosCriterio;

implementation

{$R *.DFM}
uses DEvaluacion,
     ZHelpContext,
     ZetaClientTools,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists;

{ TResutadosCriterio }
procedure TResutadosCriterio.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext := H_RESULTADOS_INDIVIDUALES_RESULTADOS_CRITERIO;
end;

procedure TResutadosCriterio.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('SO_TOTAL'), K_SIN_TIPO , '', skCount);
     ConfigAgrupamiento;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TResutadosCriterio.Agregar;
begin
     dmEvaluacion.cdsResultadoCriterio.Agregar;
end;

procedure TResutadosCriterio.Borrar;
begin
     dmEvaluacion.cdsResultadoCriterio.Borrar;
end;

procedure TResutadosCriterio.Connect;
begin
     InicializaValores;
     SetControles;
     with dmEvaluacion do
     begin
          //cdsResultadoCriterio.Conectar;
          CargarFiltros;
          DataSource.DataSet := cdsResultadoCriterio;
     end;
end;

procedure TResutadosCriterio.SetControles;
begin
     edPrimeros.Enabled := ( znPromedios.ItemIndex <> 0 );
     lblPrimeros.Enabled := edPrimeros.Enabled;
     if( edPrimeros.Enabled )then
     begin
          if( edPrimeros.Valor  = 0 )then
              edPrimeros.Valor := 5;
     end
     else
         edPrimeros.Valor := 0;
end;

procedure TResutadosCriterio.InicializaValores;
begin
     znPromedios.ItemIndex := 0;
     edPrimeros.Valor := 5;
end;

procedure TResutadosCriterio.ImprimirForma;
begin
  inherited;
end;

procedure TResutadosCriterio.Modificar;
begin
     dmEvaluacion.cdsResultadoCriterio.Modificar;
end;

function TResutadosCriterio.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede agregar el registro desde esta forma';
     Result := False;
end;

function TResutadosCriterio.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede borrar el registro desde esta forma';
     Result := False;
end;

function TResutadosCriterio.PuedeModificar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede modificar el registro desde esta forma';
     Result := False;
end;

procedure TResutadosCriterio.Refresh;
begin
     //dmEvaluacion.cdsResultadoCriterio.Refrescar;
     CargarFiltros;
     DoBestFit;
end;

procedure TResutadosCriterio.znPromediosChange(Sender: TObject);
begin
     inherited;
     SetControles;
end;


procedure TResutadosCriterio.btnFiltrarClick(Sender: TObject);
begin
     CargarFiltros;
     DoBestFit;
end;

procedure TResutadosCriterio.CargarFiltros;
var
   iMostrarProm, iMostrar: Integer;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmEvaluacion.cdsResultadoCriterio do
        begin
             DisableControls;
             try
                iMostrarProm := znPromedios.ItemIndex;
                iMostrar := edPrimeros.ValorEntero;
                //OrdenaGrid( iMostrarProm );
                dmEvaluacion.ObtenerResultadosCriterio( iMostrar, iMostrarProm );
             finally
                    EnableControls;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

{procedure TResutadosCriterio.OrdenaGrid( const iOrdenarPor: Integer );
begin
     with dmEvaluacion.cdsResultadoCriterio do
     begin
          case( iOrdenarPor ) of
                0: IndexName := 'EC_ORDENIndex';
                1: IndexName := 'SO_TOTALIndexD';
                2: IndexName := 'SO_TOTALIndex';
          else
              IndexName := VACIO;
          end;
     end;
end;}


procedure TResutadosCriterio.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //activa la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Muestra la caja de agrupamiento
     EC_NOMBRE_GRID.Options.Filtering:=TRUE;
     SO_NUM_TOT_GRID.Options.Filtering:=TRUE;
     SO_TOTAL_GRID.Options.Filtering:=FALSE;
     EC_NOMBRE_GRID.Options.Grouping:=FALSE;
     SO_NUM_TOT_GRID.Options.Grouping:=TRUE;
     SO_TOTAL_GRID.Options.Grouping:=FALSE;
end;

end.
