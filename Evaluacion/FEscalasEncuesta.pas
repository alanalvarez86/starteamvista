unit FEscalasEncuesta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, StdCtrls, ZBaseGridLectura_DevEx,
  Buttons, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxTextEdit, cxClasses, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList,
  cxGridLevel, cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons;

type
  TEscalasEncuesta = class(TBaseGridLectura_DevEx)
    pnBoton: TPanel;
    btnCopiaEscalas: TcxButton;
    ZetaDBGridDBTableView1: TcxGridDBTableView;
    ZetaDBGridDBTableView1EE_CODIGO: TcxGridDBColumn;
    ZetaDBGridDBTableView1EE_NOMBRE: TcxGridDBColumn;
    ZetaDBGridDBTableView1EE_NIVELES: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    EE_CODIGO: TcxGridDBColumn;
    EE_NOMBRE: TcxGridDBColumn;
    EE_NIVELES: TcxGridDBColumn;
    cxStyleRepository2: TcxStyleRepository;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure btnCopiaEscalasClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ConfigAgrupamiento;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
    //function PuedeBorrar( var sMensaje: String ): Boolean; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  EscalasEncuesta: TEscalasEncuesta;

implementation

{$R *.DFM}

uses DEvaluacion,
     ZHelpContext,
     ZetaBuscador_DevEx,
     ZImprimeForma,
     ZetaCommonLists,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaCommonClasses,
     ZetaTipoEntidad;

{ TEscalasEncuesta }
procedure TEscalasEncuesta.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEvaluacion;
     HelpContext := H_DISENO_ENCUESTA_ESCALAS_ENCUESTA;
end;

procedure TEscalasEncuesta.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     btnCopiaEscalas.Enabled := ZAccesosMgr.CheckDerecho( D_ESCALAS, K_DERECHO_SIST_KARDEX );
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('EE_CODIGO'), K_SIN_TIPO , '', skCount);
     ConfigAgrupamiento;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TEscalasEncuesta.Connect;
begin
     with dmEvaluacion do
     begin
          cdsEscalasEncuesta.Conectar;
          DataSource.DataSet := cdsEscalasEncuesta;
     end;
end;

procedure TEscalasEncuesta.Agregar;
begin
     dmEvaluacion.cdsEscalasEncuesta.Agregar;
     DoBestFit;
end;

procedure TEscalasEncuesta.Borrar;
begin
     with dmEvaluacion do
     begin
          if( ModificaDisenoEncuesta )then
              cdsEscalasEncuesta.Borrar;
     end;
     DoBestFit;
end;

procedure TEscalasEncuesta.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( self.Caption, 'ENC_ESCA', 'EE_CODIGO', dmEvaluacion.cdsEscalasEncuesta );
end;

procedure TEscalasEncuesta.ImprimirForma;
begin
     inherited;
     ZImprimeForma.ImprimeUnaForma( enEncEsca, dmEvaluacion.cdsEscalasEncuesta );
end;

procedure TEscalasEncuesta.Modificar;
begin
     dmEvaluacion.cdsEscalasEncuesta.Modificar;
     DoBestFit;
end;

procedure TEscalasEncuesta.Refresh;
begin
     dmEvaluacion.cdsEscalasEncuesta.Refrescar;
     DoBestFit;
end;

procedure TEscalasEncuesta.btnCopiaEscalasClick(Sender: TObject);
begin
     inherited;
     with dmEvaluacion do
     begin
          if( ModificaDisenoEncuesta )then
              CopiaEscalasEncuesta;
     end;
     DoBestFit;
end;

procedure TEscalasEncuesta.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //activa la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Muestra la caja de agrupamiento
     EE_NIVELES.Options.Grouping := TRUE;
     EE_NOMBRE.Options.Grouping := TRUE;
     EE_CODIGO.Options.Grouping := FALSE;
end;

{function TEscalasEncuesta.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     with dmEvaluacion do
     begin
          cdsPregCriterio.Conectar;
          Result := cdsPregCriterio.Locate( 'EE_CODIGO', cdsEscalasEncuesta.FieldByName('EE_CODIGO').AsString, [] );
          if Result then
          begin
               sMensaje := 'La escala que quiere borrar est� ligada a una competencia.' + CR_LF +
                         'Elimine la(s) competencia(s) relacionada(s) con esta escala e int�ntelo de nuevo.';
               Result := False;
          end
          else
              Result := inherited PuedeBorrar( sMensaje );
     end;
end;}

end.
