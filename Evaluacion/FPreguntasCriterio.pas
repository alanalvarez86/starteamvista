unit FPreguntasCriterio;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ZBaseConsulta,ZBaseGridLectura_DevEx,
     Db, ExtCtrls, StdCtrls, DBCtrls, Grids, DBGrids, Buttons,
     ZetaKeyCombo,
     ZetaDBGrid,
     ZetaSmartLists, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxTextEdit, cxClasses,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxContainer, cxMemo, cxButtons,ZetaClientDataSet;

type
  eSubeBaja = ( eSubir, eBajar );
  TPreguntasCriterio = class(TBaseGridLectura_DevEx)
    pnControles: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    EC_ORDEN: TZetaKeyCombo;
    Panel5: TPanel;
    pnFlechas: TPanel;
    zmlArriba: TcxButton;
    zmlAbajo: TcxButton;
    Splitter1: TSplitter;
    ZetaDBGridDBTableView1: TcxGridDBTableView;
    ZetaDBGridDBTableView1EP_ORDEN: TcxGridDBColumn;
    ZetaDBGridDBTableView1EP_DESCRIP: TcxGridDBColumn;
    ZetaDBGridDBTableView1EP_PESO: TcxGridDBColumn;
    ZetaDBGridDBTableView1EL_NOMBRE: TcxGridDBColumn;
    ZetaDBGridDBTableView1EP_NUMERO: TcxGridDBColumn;
    ZetaDBGridDBTableView1EP_GET_COM: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    EP_ORDEN: TcxGridDBColumn;
    EP_DESCRIP: TcxGridDBColumn;
    EP_PESO: TcxGridDBColumn;
    EL_NOMBRE: TcxGridDBColumn;
    EP_NUMERO: TcxGridDBColumn;
    EP_GET_COM: TcxGridDBColumn;
    cxStyleRepository2: TcxStyleRepository;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    EC_DETALLE: TcxMemo;
    Panel1: TPanel;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EC_ORDENChange(Sender: TObject);
    procedure zmlAbajoClick(Sender: TObject);
    procedure zmlArribaClick(Sender: TObject);
  private
    { Private declarations }
    procedure CargaCriterios;
    procedure ObtenerPreguntasPorCriterio;
    procedure SetControles;
    procedure SubeBajaPregunta(eAccion: eSubeBaja);
    procedure RefrescaGrid( iPosicion: Integer; DataSet: TZetaClientDataset  );
    Procedure ConfigAgrupamiento;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  PreguntasCriterio: TPreguntasCriterio;

implementation

{$R *.DFM}

uses DEvaluacion,
     FBuscaTexto,
     ZImprimeForma,
     ZHelpContext,
     ZetaDialogo,
     ZetaTipoEntidad,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaCommonTools;

{ TPreguntasCriterio }

procedure TPreguntasCriterio.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     with dmEvaluacion do
     begin
          cdsPreguntasEnc.IndexFieldNames := 'PG_FOLIO';
          cdsPreguntasEnc.LookupKeyField := 'PG_FOLIO';
     end;
     TipoValorActivo1 := stEvaluacion;
     HelpContext := H_DISENO_ENCUESTA_PREGUNTAS_CRITERIO;
end;

procedure TPreguntasCriterio.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('EP_ORDEN'), K_SIN_TIPO , '', skCount);
     ConfigAgrupamiento;
     ZetaDBGridDBTableView.ApplyBestFit();
     if( EC_DETALLE.Text = VACIO )then
     begin
          CargaCriterios;
          EC_ORDEN.ItemIndex := 0;
          ObtenerPreguntasPorCriterio;
     end;
     SetControles;
     DoBestFit;
end;

procedure TPreguntasCriterio.Connect;
var
   iIndice: Integer;
begin
     with dmEvaluacion do
     begin
          iIndice := EC_ORDEN.ItemIndex;
          CargaCriterios;
          if( iIndice < 0 )then
              iIndice := 0;
          EC_ORDEN.ItemIndex := iIndice;
          if not cdsPregCriterio.Active or cdsPregCriterio.HayQueRefrescar then
          begin
               ObtenerPreguntasPorCriterio;
               cdsPregCriterio.ResetDataChange;
          end;
          DataSource.DataSet := cdsPregCriterio;
     end;
end;

procedure TPreguntasCriterio.Agregar;
begin
     if( dmEvaluacion.ModificaDisenoEncuesta )then
     begin
          dmEvaluacion.cdsPregCriterio.Agregar;
     end;
     DoBestFit;
end;

procedure TPreguntasCriterio.Borrar;
begin
     dmEvaluacion.cdsPregCriterio.Borrar;
     ObtenerPreguntasPorCriterio;
     DoBestFit;
end;

procedure TPreguntasCriterio.CargaCriterios;
var
   sCriteriosDesc: String;
begin
      with EC_ORDEN.Lista do
      begin
           BeginUpdate;
           try
              Clear;
              with dmEvaluacion.cdsCriteriosxPreg do
              begin
                   Refrescar;
                   First;
                   while not EOF do
                   begin
                        sCriteriosDesc := FieldByName('EC_ORDEN').AsString + ' = ' + FieldByName('EC_NOMBRE').AsString;
                        Add( inttoStr( FieldByName('EC_ORDEN').AsInteger ) + '=' + sCriteriosDesc );
                        Next;
                   end;
              end;
           finally
                  EndUpdate;
           end;
      end;
end;

procedure TPreguntasCriterio.DoLookup;
begin
     inherited;
     FBuscaTexto.BuscarTexto( 'Texto', 'pregunta', 'EP_DESCRIP', dmEvaluacion.cdsPregCriterio );
end;

procedure TPreguntasCriterio.ImprimirForma;
begin
     inherited;
     ZImprimeForma.ImprimeUnaForma( enEncPreg, dmEvaluacion.cdsPregCriterio );
end;

procedure TPreguntasCriterio.Modificar;
begin
     dmEvaluacion.cdsPregCriterio.Modificar;
     DoBestFit;
end;

procedure TPreguntasCriterio.Refresh;
var
   iPos : integer;
begin
     iPos := EC_ORDEN.LlaveEntero;
     dmEvaluacion.cdsPregCriterio.Refrescar;
     if( iPos >0 )  then
         EC_ORDEN.LlaveEntero:= iPos;
end;

procedure TPreguntasCriterio.ObtenerPreguntasPorCriterio;
var
   iCodCriterio: Integer;
   oCursor: TCursor;
begin
     iCodCriterio := EC_ORDEN.LlaveEntero;
     EC_DETALLE.Clear;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmEvaluacion do
        begin
             cdsCriteriosxPreg.Locate('EC_ORDEN', iCodCriterio, [] );
             EC_DETALLE.Text := dmEvaluacion.cdsCriteriosxPreg.FieldbyName('EC_DETALLE').AsString;
             GetPreguntasPorCriterio( iCodCriterio );
            // RefrescaGrid( cdsPregCriterio.FieldByName('EP_ORDEN').AsInteger, TZetaClientDataset(dmEvaluacion.cdsPregCriterio) );
            // GridPreg.OrdenarPor( GridPreg.Columns[0] );   TEMPORAL DCHAVEZ
             SetControles;
        end;
     finally
         Screen.Cursor := oCursor;
     end;
end;

procedure TPreguntasCriterio.SetControles;
const
     K_PANEL_CON_TEXTO = 110;
     K_PANEL_SIN_TEXTO = 60;
     K_TAMANO_LINEA =13;
     K_LINEAS_MAXIMAS = 6;
begin
     with dmEvaluacion do
     begin
          zmlArriba.Enabled := ( cdsPregCriterio.RecordCount > 0 );
          zmlAbajo.Enabled := zmlArriba.Enabled;
          if( strLleno( cdsCriteriosxPreg.FieldByName('EC_DETALLE').AsString ) )then
          begin
               if ( EC_DETALLE.Lines.Count > K_LINEAS_MAXIMAS ) then
               begin
                   pnControles.Height := K_PANEL_SIN_TEXTO +( K_TAMANO_LINEA * ( K_LINEAS_MAXIMAS -1 ));
               end
               else
               begin
                   pnControles.Height := K_PANEL_SIN_TEXTO +( K_TAMANO_LINEA * (EC_DETALLE.Lines.Count-1 ));
               end;
          end
          else
              pnControles.Height := K_PANEL_SIN_TEXTO;
     end;
end;

procedure TPreguntasCriterio.EC_ORDENChange(Sender: TObject);
begin
     inherited;
     ObtenerPreguntasPorCriterio;
     DoBestFit;
end;

procedure TPreguntasCriterio.SubeBajaPregunta(eAccion: eSubeBaja);
const
     K_PRIMER_CRITERIO = 1;
var
   iPosActual, iNuevaPos: Integer;
   oCursor: TCursor;
begin
     if( dmEvaluacion.ModificaDisenoEncuesta )then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             with dmEvaluacion do
             begin
                  iPosActual := cdsPregCriterio.FieldByName('EP_ORDEN').AsInteger;
                  case( eAccion ) of
                        eSubir: begin
                                     if( iPosActual = K_PRIMER_CRITERIO )then
                                         ZetaDialogo.ZError( self.Caption, '� Es El Inicio de la Lista de Preguntas !', 0 )
                                     else
                                     begin
                                          iNuevaPos := iPosActual - 1;
                                          CambiaOrdenPreguntasCriterio( EC_ORDEN.LlaveEntero, iPosActual, iNuevaPos );
                                          RefrescaGrid( iNuevaPos, TZetaClientDataset(dmEvaluacion.cdsPregCriterio) );
                                     end;
                                end;
                        eBajar: begin
                                     if( iPosActual = dmEvaluacion.cdsPregCriterio.RecordCount  )then
                                         ZetaDialogo.ZError( self.Caption, '� Es El Final de la Lista de Preguntas !', 0 )
                                     else
                                     begin
                                          iNuevaPos := iPosActual + 1;
                                          CambiaOrdenPreguntasCriterio( EC_ORDEN.LlaveEntero, iPosActual, iNuevaPos );
                                          RefrescaGrid( iNuevaPos, TZetaClientDataset(dmEvaluacion.cdsPregCriterio) );
                                     end;
                                end;
                  else
                      ZetaDialogo.ZError( self.Caption, '� Operaci�n No V�lida !', 0 )
                  end;
             end;
          finally
                  Screen.Cursor := oCursor;
          end;
          Application.ProcessMessages;
     end;
end;


procedure TPreguntasCriterio.zmlAbajoClick(Sender: TObject);
begin
     inherited;
     SubeBajaPregunta( eBajar );
end;

procedure TPreguntasCriterio.zmlArribaClick(Sender: TObject);
begin
     inherited;
     SubeBajaPregunta( eSubir );
end;

procedure TPreguntasCriterio.RefrescaGrid( iPosicion: Integer; DataSet: TZetaClientDataset );
var
   sField, sIndex, sFieldName: String;
   lDescendente: TIndexOptions;
begin
     with DataSet do
     begin
          lDescendente := [];
          AddIndex( sField, sFieldName, lDescendente);  //DChavez: se agrega un index vacio para que siempre ordene de manera descendente
          IndexDefs.Update; //Es necesario para que refleje que se agrego un Index.
          IndexName := sField;
     end;
     //GridPreg.OrdenarPor( GridPreg.Columns[0] );  OLD
     Refresh;
     dmEvaluacion.cdsPregCriterio.Locate( 'EP_ORDEN', iPosicion, [] );
     DoBestFit;
end;

function TPreguntasCriterio.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeAgregar( sMensaje );
     if Result then
     begin
          Result := ( dmEvaluacion.cdsCriteriosxPreg.RecordCount > 0 );
          if not Result then
             sMensaje := 'Es necesario agregar criterios a la Encuesta/Evaluaci�n';
     end;
end;

procedure TPreguntasCriterio.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //activa la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Muestra la caja de agrupamiento
     EP_ORDEN.Options.Grouping  := FALSE;
     EP_DESCRIP.Options.Grouping:= FALSE;
     EP_PESO.Options.Grouping:= FALSE;
     EL_NOMBRE.Options.Grouping:= TRUE;
     EP_NUMERO.Options.Grouping:= FALSE;
     EP_GET_COM.Options.Grouping:= FALSE;
end;

end.
