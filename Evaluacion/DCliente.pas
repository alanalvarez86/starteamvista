unit DCliente;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBaseCliente, Db, DBClient,
{$ifdef DOS_CAPAS}
     DServerCatalogos,
     DServerGlobal,
     DServerSistema,
     DServerReportes,
     DServerEvaluacion,
{$else}
     Evaluacion_TLB,
{$endif}
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaClientDataSet,
     DBasicoCliente;

type TInfoEncuesta = record
    Encuesta: Integer;
    Nombre: String;
    Status: eStatusEncuesta;
    PesoEsc: String;
    EscalaDef: String;
    Responsable: Integer;
end;

type
  TdmCliente = class(TBaseCliente)
    cdsEncuesta: TZetaClientDataSet;
    cdsEmpleado: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsEmpleadoAlAdquirirDatos(Sender: TObject);
    procedure cdsEncuestaAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }
    FEncuesta: Integer;
    FNombre: String;
    FPesoEsc: String;
    FEscalaDef: String;
    FResponsable: Integer;
    FEncuestaBOF: Boolean;
    FEncuestaEOF: Boolean;
    FEmpleadoBOF: Boolean;
    FEmpleadoEOF: Boolean;
    {$ifdef DOS_CAPAS}
    FServerCatalogos: TdmServerCatalogos;
    FServerConsultas: TdmServerConsultas;
    FServerGlobal: TdmServerGlobal;
    FServerSistema: TdmServerSistema;
    FServerReportes : TdmServerReportes;
    FServerEvaluacion : TdmServerEvaluacion;
    {$endif}
    function FetchNextEncuesta( const iEncuesta: Integer ): Boolean;
    function FetchPreviousEncuesta( const iEncuesta: Integer ): Boolean;
    function FetchEncuesta( const iEncuesta: Integer ): Boolean;
    function FetchEmployee( const iEmpleado: TNumEmp ): Boolean;
    function FetchPreviousEmployee( const iEmpleado: TNumEmp ): Boolean;
    function FetchNextEmployee( const iEmpleado: TNumEmp ): Boolean;
    function GetEncuestaDescripcion: String;
    function GetEmpleadoDescripcion: String;
  protected
    { Protected declarations }
    procedure InitCacheModules; override;
  public
    { Public declarations }
{$ifdef DOS_CAPAS}
    function GetServerEvaluacion: TdmServerEvaluacion;
    property ServerCatalogos: TdmServerCatalogos read FServerCatalogos;
    property ServerConsultas: TdmServerConsultas read FServerConsultas;
    property ServerGlobal: TdmServerGlobal read FServerGlobal;
    property ServerSistema: TdmServerSistema read FServerSistema;
    property ServerReportes: TdmServerReportes read FServerReportes;
    property ServerEvaluacion: TdmServerEvaluacion read FServerEvaluacion;
{$else}
    FServidorEvaluacion: IdmServerEvaluacionDisp;
    function GetServerEvaluacion: IdmServerEvaluacionDisp;
    property ServerEvaluacion: IdmServerEvaluacionDisp read GetServerEvaluacion;
{$endif}
    property Encuesta: Integer read FEncuesta;
    property Nombre: String read FNombre;
    property PesoEsc: String read FPesoEsc;
    property EscalaDef: String read FEscalaDef;
    property Responsable: Integer read FResponsable;
    procedure InitActivosEncuesta;
    procedure InitActivosEmpleado;
    procedure RefrescarEvaluacion;
    procedure RefrescaEmpleado;
    procedure RefrescaEncuesta;
    procedure CargaActivosIMSS(Parametros: TZetaParams);
    procedure CargaActivosPeriodo(Parametros: TZetaParams);
    procedure CargaActivosSistema(Parametros: TZetaParams);
    procedure CargaActivosTodos(Parametros: TZetaParams); override;
    //procedure GetEmpleadosBuscados(const sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca: String; const oEmpresa: OleVariant; const oDataSet: TZetaClientDataSet);
    function EmpleadoDownEnabled: Boolean;
    function EmpleadoUpEnabled: Boolean;
    function GetEmpresas: OleVariant;
    function GetValorActivoStr(const eTipo: TipoEstado): String;override;
    function GetDatosPeriodoActivo: TDatosPeriodo;
    function GetDatosEncuestaActiva: TInfoEncuesta;
    function GetDatosEmpleadoActivo: TInfoEmpleado;
    function GetEncuestaInicial: Boolean;
    function GetEncuestaPrimera: Boolean;
    function GetEncuestaAnterior: Boolean;
    function GetEncuestaSiguiente: Boolean;
    function GetEncuestaUltima: Boolean;
    function EncuestaUpEnabled: Boolean;
    function EncuestaDownEnabled: Boolean;
    function SetEncuestaNumero( const Value: Integer ): Boolean;
    function GetEmpleadoPrimero: Boolean;
    function GetEmpleadoAnterior: Boolean;
    function GetEmpleadoSiguiente: Boolean;
    function GetEmpleadoUltimo: Boolean;
    function GetEmpleadoInicial: Boolean;
    function SetEmpleadoNumero( const Value: TNumEmp ): Boolean;
  end;

var
  dmCliente: TdmCliente;

implementation

uses DCatalogos, DSistema, DDiccionario, DEvaluacion, ZetaCommonTools;

{$R *.DFM}



procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     inherited;
     //ModoEvaluacion := TRUE;
     TipoCompany := tc3Datos;

{$ifdef DOS_CAPAS}
     FServerCatalogos := TdmServerCatalogos.Create( Self );
     FServerConsultas := TdmServerConsultas.Create( Self );
     FServerGlobal := TdmServerGlobal.Create( Self );
     FServerSistema := TdmServerSistema.Create( Self );
     FServerReportes := TdmServerReportes.Create( Self );
     FServerEvaluacion := TdmServerEvaluacion.Create(Self);
{$endif}
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
{$ifdef DOS_CAPAS}
     FreeAndNil( FServerEvaluacion );
     FreeAndNil( FServerReportes );
     //FreeAndNil( FServerTablas );
     FreeAndNil( FServerSistema );
     FreeAndNil( FServerGlobal );
     FreeAndNil( FServerCatalogos );
{$endif}
  inherited;
end;

{$ifdef DOS_CAPAS}
function TdmCliente.GetServerEvaluacion: TdmServerEvaluacion;
begin
     Result := ServerEvaluacion;
end;
{$else}
function TdmCliente.GetServerEvaluacion: IdmServerEvaluacionDisp;
begin
     Result := IdmServerEvaluacionDisp( CreaServidor( CLASS_dmServerEvaluacion, FServidorEvaluacion ) );
end;
{$endif}

procedure TdmCliente.InitCacheModules;
begin
     inherited;
     //DataCache.AddObject( 'Tabla', dmTablas );
     DataCache.AddObject( 'Cat�logo', dmCatalogos );
     DataCache.AddObject( 'Diccionario', dmDiccionario );
     DataCache.AddObject( 'Evaluaci�n de Desempe�o', dmEvaluacion );
end;

{procedure TdmCliente.GetEmpleadosBuscados(const sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca: String; const oEmpresa: OleVariant; const oDataSet: TZetaClientDataSet);
begin
     with oDataSet do
     begin
          if ( FServidorEvaluacion = nil ) then
             Data := ServerEvaluacion.GetEmpleadosBuscados( Empresa, sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca, FEncuesta )
          else
             Data := FServidorEvaluacion.GetEmpleadosBuscados( Empresa, sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca, FEncuesta );
     end;
end;}

function TdmCliente.GetEmpresas: OleVariant;
begin
     Result := Servidor.GetCompanys( 0, Ord( tc3Datos ) );
end;

function TdmCliente.GetDatosPeriodoActivo: TDatosPeriodo;
begin
end;

procedure TdmCliente.CargaActivosIMSS( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddString( 'RegistroPatronal', VACIO );
          AddInteger( 'IMSSYear', 0 );
          AddInteger( 'IMSSMes', 0 );
          AddInteger( 'IMSSTipo', 0 );
     end;
end;

procedure TdmCliente.CargaActivosPeriodo( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddInteger( 'Year', TheYear( FechaDefault ) );
          AddInteger( 'Tipo', 0 );
          AddInteger( 'Numero', 0 );
     end;
end;

procedure TdmCliente.CargaActivosSistema( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddDate( 'FechaAsistencia', FechaDefault );
          AddDate( 'FechaDefault', FechaDefault );
          AddInteger( 'YearDefault', TheYear( FechaDefault ) );
          AddInteger( 'EmpleadoActivo', Empleado );
          AddString( 'NombreUsuario', GetDatosUsuarioActivo.Nombre );
          AddString( 'CodigoEmpresa', GetDatosEmpresaActiva.Codigo );
     end;
end;

procedure TdmCliente.CargaActivosTodos(Parametros: TZetaParams);
begin
     CargaActivosIMSS( Parametros );
     CargaActivosPeriodo( Parametros );
     CargaActivosSistema( Parametros );
end;

procedure TdmCliente.InitActivosEncuesta;
begin
     FEncuestaBOF := False;
     FEncuestaEOF := False;
     FEncuesta := 0;
     FEmpleado := 0;
     FPesoEsc := VACIO;
     FNombre := VACIO;
     FEscalaDef := VACIO;
     FResponsable := 0;
     //FFechaDefault := Date;
     //GetEncuestaInicial;
end;

procedure TdmCliente.InitActivosEmpleado;
begin
     FEmpleadoBOF := False;
     FEmpleadoEOF := False;
     FEmpleado := 0;
     //GetEmpleadoInicial;
end;

function TdmCliente.GetDatosEncuestaActiva: TInfoEncuesta;
begin
     with Result do
     begin
          Encuesta := Self.Encuesta;
          with cdsEncuesta do
          begin
               FNombre := FieldByName( 'ET_NOMBRE' ).AsString;
               Status := eStatusEncuesta( FieldByName( 'ET_STATUS' ).AsInteger );
               FPesoEsc := FieldByName( 'ET_ESC_PES' ).AsString;
               FEscalaDef := FieldByName( 'ET_ESC_DEF' ).AsString;
               FResponsable := FieldByName( 'ET_USR_ADM').AsInteger;
               Nombre := FNombre;
               PesoEsc := FPesoEsc;
               EscalaDef := FEscalaDef;
               Responsable := FResponsable;
          end;
     end;
end;

function TdmCliente.SetEncuestaNumero( const Value: Integer ): Boolean;
begin
     if ( FEncuesta <> Value ) then
     begin
          Result := FetchEncuesta( Value );
          if Result then
          begin
               with cdsEncuesta do
               begin
                    FEncuesta := FieldByName( 'ET_CODIGO' ).AsInteger;
               end;
               FEncuestaBOF := False;
               FEncuestaEOF := False;
          end;
     end
     else
         Result := False;
end;

procedure TdmCliente.RefrescarEvaluacion;
begin
     with cdsEncuesta do
     begin
          //Data := ServerEvaluacion.GetEncuesta( Empresa, iValue );
          FEncuesta := FieldByName( 'ET_CODIGO' ).AsInteger;
          FNombre := FieldByName( 'ET_NOMBRE' ).AsString;
          FEscalaDef := FieldByName( 'ET_ESC_DEF' ).AsString;
          FPesoEsc := FieldByName( 'ET_ESC_PES' ).AsString;
          FResponsable := FieldByName( 'ET_USR_ADM').AsInteger;
     end;
end;

procedure TdmCliente.RefrescaEncuesta;
begin
     cdsEncuesta.Refrescar;
     RefrescarEvaluacion;
end;

function TdmCliente.GetValorActivoStr(const eTipo: TipoEstado): String;
begin
     case eTipo of
          stEvaluacion: Result := GetEncuestaDescripcion;
          stEmpleado: Result := GetEmpleadoDescripcion;
          {stFecha: Result := GetAsistenciaDescripcion;}
     end;
end;

function TdmCliente.GetEncuestaDescripcion: String;
begin
     with cdsEncuesta do
     begin
          Result := 'Encuesta ' + InttoStr(FEncuesta) + ': '+ FieldByName( 'ET_NOMBRE' ).AsString;
     end;
end;

function TdmCliente.GetEncuestaInicial: Boolean;
var
   Datos: OleVariant;
begin
     Result := ServerEvaluacion.EncuestaSiguiente( Empresa, 0, Datos );
     with cdsEncuesta do
     begin
          Data := Datos;
          FEncuesta := FieldByName( 'ET_CODIGO' ).AsInteger;
          FNombre := FieldByName( 'ET_NOMBRE' ).AsString;
          FPesoEsc := FieldByName( 'ET_ESC_PES' ).AsString;
          FEscalaDef := FieldByName( 'ET_ESC_DEF' ).AsString;
          FResponsable := FieldByName( 'ET_USR_ADM').AsInteger;
     end;
     FEncuestaBOF := True;
end;

function TdmCliente.GetEncuestaPrimera: Boolean;
var
   iNewEnc: Integer;
   lCargaEmpleados: Boolean;
begin
     Result := FetchNextEncuesta( 0 );
     if Result then
     begin
          with cdsEncuesta do
          begin
               iNewEnc := FieldByName( 'ET_CODIGO' ).AsInteger;
               lCargaEmpleados := ( FEncuesta <> iNewEnc );
               FEncuesta := iNewEnc;
               FNombre := FieldByName( 'ET_NOMBRE' ).AsString;
               FPesoEsc := FieldByName( 'ET_ESC_PES' ).AsString;
               FEscalaDef := FieldByName( 'ET_ESC_DEF' ).AsString;
               FEncuestaBOF := ( iNewEnc = FEncuesta );
               FEncuestaEOF := False;
               FResponsable := FieldByName( 'ET_USR_ADM').AsInteger;
          end;
          if( lCargaEmpleados )then
              GetEmpleadoInicial;
     end;
end;

function TdmCliente.FetchEncuesta( const iEncuesta: Integer ): Boolean;
begin
     with cdsEncuesta do
     begin
          Data := ServerEvaluacion.GetEncuesta( Empresa, iEncuesta );
          Result := not IsEmpty;
     end;
end;

function TdmCliente.FetchNextEncuesta( const iEncuesta: Integer ): Boolean;
var
   Datos: OleVariant;
begin
     Result := ServerEvaluacion.EncuestaSiguiente( Empresa, iEncuesta, Datos );
     if Result then
     begin
          with cdsEncuesta do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmCliente.GetEncuestaAnterior: Boolean;
var
   iNewEnc: Integer;
   lCargaEmpleados: Boolean;
begin
     Result := FetchPreviousEncuesta( FEncuesta );
     if Result then
     begin
          with cdsEncuesta do
          begin
               iNewEnc := FieldByName( 'ET_CODIGO' ).AsInteger;
               lCargaEmpleados := ( FEncuesta <> iNewEnc );
               FEncuesta := iNewEnc;
               FNombre := FieldByName( 'ET_NOMBRE' ).AsString;
               FPesoEsc := FieldByName( 'ET_ESC_PES' ).AsString;
               FEscalaDef := FieldByName( 'ET_ESC_DEF' ).AsString;
               FResponsable := FieldByName( 'ET_USR_ADM').AsInteger;
          end;
          FEncuestaEOF := False;
          if( lCargaEmpleados )then
              GetEmpleadoInicial;
     end
     else
         FEncuestaBOF := True;
end;

function TdmCliente.FetchPreviousEncuesta( const iEncuesta: Integer ): Boolean;
var
   Datos: OleVariant;
begin
     Result := ServerEvaluacion.EncuestaAnterior( Empresa, iEncuesta, Datos );
     if Result then
     begin
          with cdsEncuesta do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmCliente.GetEncuestaSiguiente: Boolean;
var
   iNewEnc: Integer;
   lCargaEmpleados: Boolean;
begin
     Result := FetchNextEncuesta( FEncuesta );
     if Result then
     begin
          with cdsEncuesta do
          begin
               iNewEnc := FieldByName( 'ET_CODIGO' ).AsInteger;
               lCargaEmpleados :=  ( FEncuesta <> iNewEnc );
               FEncuesta := iNewEnc;
               FNombre := FieldByName( 'ET_NOMBRE' ).AsString;
               FPesoEsc := FieldByName( 'ET_ESC_PES' ).AsString;
               FEscalaDef := FieldByName( 'ET_ESC_DEF' ).AsString;
               FResponsable := FieldByName( 'ET_USR_ADM').AsInteger;
          end;
          FEncuestaBOF := False;
          if( lCargaEmpleados )then
              GetEmpleadoInicial;
     end
     else
         FEncuestaEOF := True;
end;

function TdmCliente.GetEncuestaUltima: Boolean;
{const
     K_MAX_ENC = 'ZZZZZZZ';}
var
   iNewEnc: Integer;
   lCargaEmpleados: Boolean;
begin
     Result := FetchPreviousEncuesta(  MAXINT );
     if Result then
     begin
          with cdsEncuesta do
          begin
               iNewEnc := FieldByName( 'ET_CODIGO' ).AsInteger;
               FEncuestaBOF := False;
               lCargaEmpleados := ( FEncuesta <> iNewEnc );
               FEncuesta := iNewEnc;
               FNombre := FieldByName( 'ET_NOMBRE' ).AsString;
               FPesoEsc := FieldByName( 'ET_ESC_PES' ).AsString;
               FEscalaDef := FieldByName( 'ET_ESC_DEF' ).AsString;
               FEncuestaEOF := ( iNewEnc = FEncuesta );
               FResponsable := FieldByName( 'ET_USR_ADM').AsInteger;
          end;
          if( lCargaEmpleados )then
              GetEmpleadoInicial;
     end;
end;

function TdmCliente.EncuestaDownEnabled: Boolean;
begin
     with cdsEncuesta do
     begin
          Result := Active and not IsEmpty and not FEncuestaBOF;
     end;
end;

function TdmCliente.EncuestaUpEnabled: Boolean;
begin
     with cdsEncuesta do
     begin
          Result := Active and not IsEmpty and not FEncuestaEOF;
     end;
end;

function TdmCliente.GetEmpleadoPrimero: Boolean;
var
   iNewEmp: TNumEmp;
   iEmpleado: TNumEmp;
begin
     //Result := FetchNextEmployee( 0 );
     iEmpleado := ServerEvaluacion.GetEmpleadoSiguiente( Empresa, 0, FEncuesta );
     Result := FetchNextEmployee( iEmpleado );
     if Result then
     begin
          with cdsEmpleado do
          begin
               iNewEmp := FieldByName( 'CB_CODIGO' ).AsInteger;
               FEmpleado := iNewEmp;
               FEmpleadoBOF := ( iNewEmp = FEmpleado );
               FEmpleadoEOF := False;
          end;
     end;
end;

function TdmCliente.GetEmpleadoAnterior: Boolean;
var
   iEmpleado: TNumEmp;
begin
     //Result := FetchPreviousEmployee( FEmpleado );
     iEmpleado := ServerEvaluacion.GetEmpleadoAnterior( Empresa, FEmpleado, FEncuesta );
     Result := FetchPreviousEmployee( iEmpleado );
     if Result then
     begin
          with cdsEmpleado do
          begin
               FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
          end;
          FEmpleadoEOF := False;
     end
     else
         FEmpleadoBOF := True;
end;

function TdmCliente.GetEmpleadoSiguiente: Boolean;
var
   iEmpleado: TNumEmp;
begin
     iEmpleado := ServerEvaluacion.GetEmpleadoSiguiente( Empresa, FEmpleado, FEncuesta );
     if( iEmpleado = 0 )then
         iEmpleado := FEmpleado - 1;
     Result := FetchNextEmployee( iEmpleado );
     if Result then
     begin
          with cdsEmpleado do
          begin
               FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
          end;
          FEmpleadoBOF := False;
     end
     else
         FEmpleadoEOF := True;
end;

function TdmCliente.GetEmpleadoUltimo: Boolean;
var
   iNewEmp: TNumEmp;
begin
     //Result := FetchPreviousEmployee( MAXINT );
     Result := FetchPreviousEmployee( ServerEvaluacion.GetEmpleadoAnterior( Empresa, MAXINT, FEncuesta ) );
     if Result then
     begin
          with cdsEmpleado do
          begin
               iNewEmp := FieldByName( 'CB_CODIGO' ).AsInteger;
               FEmpleadoBOF := False;
               FEmpleado := iNewEmp;
               FEmpleadoEOF := ( iNewEmp = FEmpleado );
          end;
     end;
end;

function TdmCliente.FetchEmployee( const iEmpleado: TNumEmp ): Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.GetEmpleado( Empresa, iEmpleado, Datos );
     if Result then
     begin
          with cdsEmpleado do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmCliente.FetchPreviousEmployee( const iEmpleado: TNumEmp ): Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.GetEmpleadoAnterior( Empresa, iEmpleado, VACIO, Datos );
     if Result then
     begin
          with cdsEmpleado do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmCliente.FetchNextEmployee( const iEmpleado: TNumEmp ): Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.GetEmpleadoSiguiente( Empresa, iEmpleado, VACIO, Datos );
     if Result then
     begin
          with cdsEmpleado do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmCliente.EmpleadoDownEnabled: Boolean;
begin
     with cdsEmpleado do
     begin
          Result := Active and not IsEmpty and not FEmpleadoBOF;
     end;
end;

function TdmCliente.EmpleadoUpEnabled: Boolean;
begin
     with cdsEmpleado do
     begin
          Result := Active and not IsEmpty and not FEmpleadoEOF;
     end;
end;

function TdmCliente.GetDatosEmpleadoActivo: TInfoEmpleado;
begin
     with Result do
     begin
          Numero := Self.Empleado;
          with cdsEmpleado do
          begin
               if( not cdsEmpleado.IsEmpty )then
               begin
                    Nombre := FieldByName( 'PRETTYNAME' ).AsString;
                    Activo := zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString );
                    Baja := FieldByName( 'CB_FEC_BAJ' ).AsDateTime;
                    Ingreso := FieldByName( 'CB_FEC_ING' ).AsDateTime;
               end;
          end;
     end;
end;

function TdmCliente.SetEmpleadoNumero( const Value: TNumEmp ): Boolean;
var
   iEmpleado: TNumEmp;
begin
     if ( FEmpleado <> Value ) then
     begin
          iEmpleado := ServerEvaluacion.GetEmpleado( Empresa, Value, FEncuesta );
          //Result := FetchEmployee( Value );
          Result := FetchEmployee( iEmpleado );
          if Result then
          begin
               with cdsEmpleado do
               begin
                    FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
               end;
               FEmpleadoBOF := False;
               FEmpleadoEOF := False;
          end;
     end
     else
         Result := True;
end;

procedure TdmCliente.RefrescaEmpleado;
begin
     cdsEmpleado.Refrescar;
end;

function TdmCliente.GetEmpleadoInicial: Boolean;
var
   Datos: OleVariant;
   iEmpleado: TNumEmp;
begin
     iEmpleado := ServerEvaluacion.GetEmpleadoSiguiente( Empresa, 0, FEncuesta );
     Result := Servidor.GetEmpleadoSiguiente( Empresa, iEmpleado, VACIO, Datos );
     with cdsEmpleado do
     begin
          Data := Datos;
          FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
     end;
     FEmpleadoBOF := True;
end;

function TdmCliente.GetEmpleadoDescripcion: String;
begin
     with cdsEmpleado do
     begin
          Result := IntToStr( FEmpleado ) + ': '+ FieldByName( 'PRETTYNAME' ).AsString;
     end;
end;

{  cdsEmpleado  }
procedure TdmCliente.cdsEmpleadoAlAdquirirDatos(Sender: TObject);
begin
     FetchEmployee( ServerEvaluacion.GetEmpleado( Empresa, FEmpleado, FEncuesta ) );
end;

{ cdsEncuesta }
procedure TdmCliente.cdsEncuestaAlAdquirirDatos(Sender: TObject);
begin
     FetchEncuesta( FEncuesta );
end;

end.
