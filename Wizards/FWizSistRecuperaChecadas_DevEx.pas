unit FWizSistRecuperaChecadas_DevEx;

interface

uses
    Windows, Messages, SysUtils, Variants, Classes, Graphics,
    Controls, Forms, Dialogs, FWizSistBaseFiltro, cxGraphics,
    cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,ZcxBaseWizard,
    TressMorado2013, cxContainer, cxEdit, Menus, StdCtrls, ZetaCXWizard,
    ZetaEdit, cxRadioGroup, cxTextEdit, cxMemo, ExtCtrls, ZetaKeyLookup_DevEx,
    cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
    dxCustomWizardControl, dxWizardControl, ExtDlgs, cxClasses,cxShellBrowserDialog;

type
  TWizSistRecuperaChecadas_DevEx = class(TcxBaseWizard)
    OpenDialog: TOpenDialog;
    Label1: TLabel;
    txtTerminales: TEdit;
    BBuscarArchivo: TcxButton;
    BGuardaArchivo: TcxButton;
    txtSalida: TEdit;
    Label5: TLabel;
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure BBuscarArchivoClick(Sender: TObject);
    procedure BGuardaArchivoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    function GetArchivo: String;
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizSistRecuperaChecadas_DevEx: TWizSistRecuperaChecadas_DevEx;
Const
K_DAT_FILE='RELOJ.DAT';

implementation

uses ZetaClientTools,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo,
     dSistema,
     dprocesos;

{$R *.dfm}

procedure TWizSistRecuperaChecadas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_TRESS_SISTEMA_TERMINALESGTI_RECUPERAR_CHECADAS;
     txtSalida.Text:= ExtractFilePath( Application.ExeName )+ K_DAT_FILE ;
     txtTerminales.Text:= ExtractFilePath( Application.ExeName ) ;
end;

procedure TWizSistRecuperaChecadas_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     {***Asignacion de numeracion a las paginas de wizard***}
     Parametros.PageIndex := 0;
     Ejecucion.PageIndex := 1;
     {***Se determina el control enfocado de inicio***}
     parametrosControl := txtTerminales;
     txtTerminales.SetFocus;
     {***Texto de Advertencia***}
     Advertencia.Caption:= 'Al aplicar el proceso se transformará la información del archivo que contiene las checadas al formato Reloj.DAT.';
end;

procedure TWizSistRecuperaChecadas_DevEx.BBuscarArchivoClick(Sender: TObject);
begin
     inherited;
     txtTerminales.Text := ZetaClientTools.AbreDialogo( OpenDialog, txtTerminales.Text, 'txt' );
end;

procedure TWizSistRecuperaChecadas_DevEx.BGuardaArchivoClick(Sender: TObject);
begin
     inherited;
     txtSalida.Text := ZetaClientTools.AbreDialogo( OpenDialog, txtSalida.Text, '.dat' );
end;

procedure TWizSistRecuperaChecadas_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddString( 'RutaArchivoFuente' , txtTerminales.Text );
          AddString( 'RutaSalidaArchivo' , GetArchivo );
     end;
     with Descripciones do
     begin
          AddString( 'Archivo de terminales', txtTerminales.Text );
          AddString( 'Salida de archivo POLL', GetArchivo );
     end;
end;

function TWizSistRecuperaChecadas_DevEx.GetArchivo: String;
begin
     Result := txtSalida.Text;
end;

procedure TWizSistRecuperaChecadas_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   sFileName: String;
begin
     parametrosControl := nil;
     inherited;
     with Wizard do
     begin
          if Adelante and EsPaginaActual (Parametros) then
          begin
               if not FileExists( Trim( txtTerminales.Text ) )then
               begin
                    CanMove := Error( '¡ Error ! Es necesario especificar un archivo de terminales.', txtTerminales );
                    txtTerminales.SetFocus;
               end
               else
               begin
                    if FileExists( GetArchivo ) and not ZetaDialogo.zConfirm( Caption, 'El archivo ' + GetArchivo + ' ya existe' + CR_LF + '¿ Desea reemplazarlo ? ', 0, mbNo ) then
                    begin
                         CanMove := False;
                         txtTerminales.SetFocus;
                    end
               end;
               sFileName := ExtractFileExt(txtSalida.Text);
               if( sFileName = '' )   then
               begin
                    CanMove := Error( '¡ Error ! Es necesario especificar un nombre al archivo de salida.', txtSalida );
               end;
          end;
     end;
end;

function TWizSistRecuperaChecadas_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmprocesos.RecuperaChecadas(ParameterList);
end;


end.
