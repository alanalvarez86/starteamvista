inherited EmpImportarOrganigramaGridSelect_DevEx: TEmpImportarOrganigramaGridSelect_DevEx
  Left = 131
  Top = 226
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Seleccione Los Empleados Deseados'
  ClientWidth = 1142
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 1142
    inherited OK_DevEx: TcxButton
      Left = 978
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 1057
    end
  end
  inherited PanelSuperior: TPanel
    Width = 1142
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 1142
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.Indicator = True
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre Empleado'
        DataBinding.FieldName = 'PRETTYNAME'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object CB_JEFE: TcxGridDBColumn
        Caption = 'Jefe'
        DataBinding.FieldName = 'CB_JEFE'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object NOMBRE_JEFE: TcxGridDBColumn
        Caption = 'Nombre Jefe'
        DataBinding.FieldName = 'NOMBRE_JEFE'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object PLAZA_JEF: TcxGridDBColumn
        Caption = 'Plaza Jefe'
        DataBinding.FieldName = 'PLAZA_JEF'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object NOMBRE_PLJEF: TcxGridDBColumn
        Caption = 'Nombre Plaza Jefe'
        DataBinding.FieldName = 'NOMBRE_PLJEF'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object PL_NOMBRE: TcxGridDBColumn
        Caption = 'Plaza'
        DataBinding.FieldName = 'PL_NOMBRE'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object PL_CODIGO: TcxGridDBColumn
        Caption = 'Folio'
        DataBinding.FieldName = 'PL_CODIGO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object PL_TIPO_DESC: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'PL_TIPO_DESC'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object PL_TIREP_DESC: TcxGridDBColumn
        Caption = 'Relaci'#243'n'
        DataBinding.FieldName = 'PL_TIREP_DESC'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object PL_FEC_INI: TcxGridDBColumn
        Caption = 'Inicia'
        DataBinding.FieldName = 'PL_FEC_INI'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
        Width = 64
      end
      object PL_FEC_FIN: TcxGridDBColumn
        Caption = 'Termina'
        DataBinding.FieldName = 'PL_FEC_FIN'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
        Width = 64
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
