inherited WizNomSimAplicarFinGlobal_DevEx: TWizNomSimAplicarFinGlobal_DevEx
  Left = 437
  Top = 188
  Caption = 'Aplicar Finiquito Global'
  ClientHeight = 458
  ClientWidth = 442
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 442
    Height = 458
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Se indicar'#225' al sistema que copie los resultados de la simulaci'#243'n' +
        ' a la n'#243'mina real.'
      Header.Title = 'Aplicar Finiquito Global'
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 221
        Width = 420
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 420
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 348
          AnchorX = 244
          AnchorY = 49
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 14
        Top = 137
      end
      inherited sFiltroLBL: TLabel
        Left = 39
        Top = 160
      end
      inherited Seleccionar: TcxButton
        Left = 142
        Top = 254
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 66
        Top = 134
        TabOrder = 4
      end
      inherited sFiltro: TcxMemo
        Left = 66
        Top = 160
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 66
        Top = 4
      end
      object chReEnviar: TcxCheckBox
        Left = 8
        Top = 289
        Caption = 'Reenviar simulaciones aplicadas'
        TabOrder = 1
        Transparent = True
        Width = 185
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 544
    Top = 181
  end
end
