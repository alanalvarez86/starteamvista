inherited WizSistNumeroTarjeta_DevEx: TWizSistNumeroTarjeta_DevEx
  Left = 482
  Top = 187
  Caption = 'N'#250'mero de Tarjeta'
  ClientHeight = 373
  ClientWidth = 442
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 442
    Height = 373
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 138
        Width = 420
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 420
        inherited Advertencia: TcxLabel
          Caption = 
            'Se realizar'#225' la Actualizaci'#243'n del N'#250'mero de Tarjeta para cada Em' +
            'pleado de la lista de Empresas que se tienen en la Base de Datos' +
            ' Comparte.'
          Style.IsFontAssigned = True
          Width = 352
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sFiltro: TcxMemo
        Style.IsFontAssigned = True
      end
    end
  end
end
