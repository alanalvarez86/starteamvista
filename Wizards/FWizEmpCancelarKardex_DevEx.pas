unit FWizEmpCancelarKardex_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Mask, Buttons, ComCtrls, ExtCtrls,
     ZetaFecha, ZetaEdit, ZetaKeyCombo, ZetaCommonClasses,
     FWizEmpBaseFiltro, cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
      TressMorado2013, dxSkinsDefaultPainters,
     cxContainer, cxEdit, Menus, ZetaCXWizard, cxRadioGroup, cxTextEdit,
     cxMemo, ZetaKeyLookup_DevEx, cxButtons, dxGDIPlusClasses, cxImage,
     cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl;

type
  TWizEmpCancelarKardex_DevEx = class(TWizEmpBaseFiltro)
    dReferenciaLBL: TLabel;
    dReferencia: TZetaFecha;
    Label2: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    iTipoFecha: TZetaKeyCombo;
    iCancelaMovimiento: TZetaKeyCombo;
    sMovimiento: TZetaKeyCombo;
    LblGenerales: TLabel;
    RegGenerales: TZetaKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    //DevEx     //Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
    procedure sMovimientoChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    function EsRegistroGeneral: Boolean;
    function GetTipoMovimiento: String;
    procedure InitTipoMovimiento;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  end;

const
     IND_MOVIM_CAMBIO = 2;
     MAX_MOVIM_SISTEMA = 8;
     IND_MOVIM_GENERALES = 9;
     FILTRO_LOOKUP = 'TB_SISTEMA=%s';
     aTipoMovimiento: array[ 0..MAX_MOVIM_SISTEMA ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( K_T_ALTA, K_T_BAJA, K_T_CAMBIO, K_T_TURNO, K_T_PUESTO,
                                                                 K_T_RENOVA, K_T_AREA, K_T_PRESTA, K_T_PLAZA );
var
  WizEmpCancelarKardex_DevEx: TWizEmpCancelarKardex_DevEx;

implementation

uses DCliente,
     DProcesos,
     dTablas,
     ZBaseSelectGrid_DevEx,
     ZetaCommonLists,
     ZetaCommonTools,
     FEmpCancelarKardexGridSelect_DevEx, ZcxWizardBasico;

{$R *.DFM}

{ TWizEmpCancelarKardex }

procedure TWizEmpCancelarKardex_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := dReferencia;
     dReferencia.Valor := dmCliente.FechaDefault;
     iTipoFecha.ItemIndex := 0;
     iCancelaMovimiento.ItemIndex := 0;
     InitTipoMovimiento;
     with RegGenerales do
     begin
          LookupDataset := dmTablas.cdsMovKardex;
          Filtro := Format( FILTRO_LOOKUP, [ EntreComillas( K_GLOBAL_NO ) ] );
     end;
     HelpContext:= H10168_Cancelar_kardex;

     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;
end;

procedure TWizEmpCancelarKardex_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     dmTablas.cdsMovKardex.Conectar;

     Advertencia.Caption := 'Al ejecutar este proceso se borrarán los movimientos de kardex que se especifiquen.';   //DevEx
end;

procedure TWizEmpCancelarKardex_DevEx.CargaParametros;
begin
     inherited CargaParametros;

     with ParameterList do
     begin
          AddDate( 'FechaReferencia', dReferencia.Valor );
          AddInteger( 'TipoFecha', iTipoFecha.ItemIndex );
          AddInteger( 'TipoModificacion', iCancelaMovimiento.ItemIndex );
          AddString( 'Movimiento', GetTipoMovimiento );
     end;
     with Descripciones do
     begin
          AddDate( 'Fecha referencia', dReferencia.Valor );
          AddString( 'Tipo de fecha', ObtieneElemento( lfTipoKarFecha, iTipoFecha.ItemIndex ) );
          AddString( 'Cancelar modificación', ObtieneElemento( lfCancelaMod, iCancelaMovimiento.ItemIndex ) );
          if EsRegistroGeneral then
          begin
             AddString( 'Tipo de movimiento', sMovimiento.Items[ sMovimiento.itemIndex ] );
             AddString( 'Tipo de registro gral.', RegGenerales.Llave + ': ' + RegGenerales.Descripcion );
          end
          else
             AddString( 'Tipo de movimiento', GetTipoMovimiento + ': ' + sMovimiento.Items[ sMovimiento.itemIndex ] );
     end;
end;

procedure TWizEmpCancelarKardex_DevEx.CargaListaVerificacion;
begin
     dmProcesos.CancelarKardexGetLista( ParameterList );
end;

function TWizEmpCancelarKardex_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpCancelarKardexGridSelect_DevEx );
end;

function TWizEmpCancelarKardex_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CancelarKardex( ParameterList, Verificacion );
end;

procedure TWizEmpCancelarKardex_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     ParametrosControl := nil;  //DevEx
     if Wizard.Adelante then
     begin
          //if ( PageControl.ActivePage = Parametros ) then
          if ( WizardControl.ActivePage = Parametros ) then  //DevEx
          begin
               if EsRegistroGeneral and StrVacio( RegGenerales.Llave ) then
                  CanMove := Error( 'No se ha especificado el registro general de Kardex', RegGenerales );
          end;
     end;
end;

function TWizEmpCancelarKardex_DevEx.EsRegistroGeneral: Boolean;
begin
     Result := ( sMovimiento.ItemIndex = IND_MOVIM_GENERALES );
end;

function TWizEmpCancelarKardex_DevEx.GetTipoMovimiento: string;
begin
     if EsRegistroGeneral then
        Result := RegGenerales.Llave
     else
        Result := aTipoMovimiento[ sMovimiento.ItemIndex ];
end;

procedure TWizEmpCancelarKardex_DevEx.InitTipoMovimiento;
begin
     sMovimiento.ItemIndex := IND_MOVIM_CAMBIO;
     sMovimientoChange( self );
end;

procedure TWizEmpCancelarKardex_DevEx.sMovimientoChange(Sender: TObject);
begin
     inherited;
     with RegGenerales do
     begin
          Enabled := EsRegistroGeneral;
          LblGenerales.Enabled := Enabled;
     end;
end;

//DevEx     //Agregado para enfocar un control
procedure TWizEmpCancelarKardex_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
     ParametrosControl := dReferencia
     else
         ParametrosControl := nil;
     inherited;
end;

end.
