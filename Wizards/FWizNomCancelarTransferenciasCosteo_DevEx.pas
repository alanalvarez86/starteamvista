unit FWizNomCancelarTransferenciasCosteo_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls,
     ZCXBaseWizardFiltro, ZetaKeyLookup_DevEx, ZetaEdit,
     ZetaDBTextBox, ZetaCommonLists, ZetaKeyCombo,
     ZetaNumero, Mask, ZetaFecha, cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, cxGroupBox,
     cxRadioGroup, ZetaCXWizard, cxTextEdit, cxMemo, cxButtons,
     dxGDIPlusClasses, cxImage, cxLabel, dxCustomWizardControl,
     dxWizardControl;

type
  TWizNomCancelarTransferenciasCosteo_DevEx = class(TBaseCXWizardFiltro)
    GroupBox2: TcxGroupBox;
    Label5: TLabel;
    lbDestino: TLabel;
    MotivoLbl: TLabel;
    Label6: TLabel;
    TipoAutLbl: TLabel;
    AU_FECHA: TZetaFecha;
    CC_DESTINO: TZetaKeyLookup_DevEx;
    TR_MOTIVO: TZetaKeyLookup_DevEx;
    TR_TEXTO: TZetaEdit;
    lbOriginal: TLabel;
    CC_ORIGEN: TZetaKeyLookup_DevEx;
    Label7: TLabel;
    Label4: TLabel;
    TR_TIPO: TcxRadioGroup;
    TR_GLOBAL: TcxRadioGroup;
    TR_STATUS: TZetaKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    //DevEx Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FNivel: string;
    procedure LlenaStatusTransferencias;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
    procedure CargaListaVerificacion;override;
    function Verificar: Boolean;override;
  public
    { Public declarations }
  end;

var
  WizNomCancelarTransferenciasCosteo_DevEx: TWizNomCancelarTransferenciasCosteo_DevEx;

implementation

uses DProcesos,
     DCliente,
     DGlobal,
     dTablas,
     DSistema,
     ZGlobalTress,
     ZAccesosMgr,
     ZAccesosTress,
     ZBaseSelectGrid_DevEx,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     FTressShell,
     FNomCancelaTransferenciasCosteoGridSelect_DevEx;

{$R *.DFM}

const
      K_INDICE_TODAS = 0;
      K_INDIVIDUALES = 1;
      K_GLOBALES     = 2;
      K_TEXTO_TODAS = 'Todos';

procedure TWizNomCancelarTransferenciasCosteo_DevEx.FormCreate(Sender: TObject);

begin
     inherited;
     ParametrosControl := AU_FECHA;
     HelpContext:= H_Cancelar_Transferencias;

     LlenaStatusTransferencias;

     with Global do
     begin
          FNivel := Global.NombreCosteo;
          //lbDestino.Caption := 'Transferir hacia:';
          //lbOriginal.Caption := FNivel + ' Original:';
     end;

     AU_FECHA.Valor := dmCliente.GetDatosPeriodoActivo.Inicio;
     TR_TIPO.ItemIndex := K_INDICE_TODAS;
     TR_STATUS.ItemIndex := K_INDICE_TODAS;
     TR_GLOBAL.ItemIndex := K_INDICE_TODAS;
     TR_TEXTO.MaxLength := K_ANCHO_DESCRIPCION;

     TR_MOTIVO.LookupDataset := dmTablas.cdsMotivoTransfer;
     CC_DESTINO.LookupDataset := dmTablas.GetDataSetTransferencia;
     CC_ORIGEN.LookupDataset := dmTablas.GetDataSetTransferencia(FALSE);

     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;

end;

procedure TWizNomCancelarTransferenciasCosteo_DevEx.CargaParametros;
 function ObtieneElementoeSpecial( const eLista: ListasFijas; const Index: integer ): string;
 begin
      if ( Index = K_INDICE_TODAS ) then
         Result := K_TEXTO_TODAS
      else
          Result := ObtieneElemento( eLista, Index -1 )

 end;

 function GetTextoCaptura( const Index: integer ): string;
 begin
      case Index of
           K_INDICE_TODAS : Result := K_TEXTO_TODAS;
           K_INDIVIDUALES : Result := 'Individuales';
           K_GLOBALES : Result := 'Globales';
      end;
 end;


 var
    sGlobal: string;
begin
     inherited CargaParametros;

     with ParameterList do
     begin
          AddDate( 'FechaInicial', AU_FECHA.Valor );
          AddString( 'CCDestino', CC_DESTINO.LLave );
          AddString( 'CCOriginal', CC_ORIGEN.LLave );
          AddString( 'Motivo', TR_MOTIVO.Llave );
          AddString( 'Observa', TR_TEXTO.Text );
          AddInteger ('Tipo', TR_TIPO.ItemIndex );
          AddInteger ('Status', TR_STATUS.ItemIndex );

          case TR_GLOBAL.ItemIndex of
               K_INDICE_TODAS : sGlobal := VACIO;
               K_INDIVIDUALES : sGlobal := K_GLOBAL_NO;
               K_GLOBALES : sGlobal := K_GLOBAL_SI;
          end;

          AddString('Captura', sGlobal );

          AddBoolean( 'PuedeCambiarTarjeta', dmCliente.PuedeModificarTarjetasAnteriores );
     end;

     with Descripciones do
     begin
          //Clear;      //DevEx
          AddString ( 'Fecha' , FechaCorta( AU_FECHA.Valor ) );
          if ( Trim(CC_ORIGEN.Descripcion) = ''  ) then
             AddString ( Format( '%s Original ',  [ FNivel] ), StrDef( CC_ORIGEN.LLave, K_TEXTO_TODAS ) )
          else
              AddString ( Format( '%s Original ',  [ FNivel] ), StrDef( CC_ORIGEN.LLave, K_TEXTO_TODAS ) + ': ' + CC_ORIGEN.Descripcion);
          if ( Trim(CC_DESTINO.Descripcion) = '' ) then
             AddString ( Format( '%s Destino',  [ FNivel] ),  StrDef( CC_DESTINO.LLave, K_TEXTO_TODAS ) )
          else
              AddString ( Format( '%s Destino',  [ FNivel] ),  StrDef( CC_DESTINO.LLave, K_TEXTO_TODAS ) + ': ' + CC_DESTINO.Descripcion);
          AddString ( 'Tipo' , ObtieneElementoeSpecial( lfTipoHoraTransferenciaCosteo, TR_TIPO.ItemIndex ) );
          if ( Trim(TR_MOTIVO.Descripcion) = '' ) then
             AddString ( 'Motivo' ,  StrDef( TR_MOTIVO.Llave, K_TEXTO_TODAS ) )
          else
              AddString ( 'Motivo' ,  StrDef( TR_MOTIVO.Llave, K_TEXTO_TODAS ) + ': ' + TR_MOTIVO.Descripcion);
          AddString ( 'Status' , ObtieneElementoeSpecial( lfStatusTransCosteo, TR_STATUS.ItemIndex ) );
          AddString ( 'Captura' , GetTextoCaptura( TR_GLOBAL.ItemIndex ) );
          AddString ( 'Observaciones/Comentarios' , StrDef( TR_TEXTO.Text, K_TEXTO_TODAS ) );
     end;
end;

procedure TWizNomCancelarTransferenciasCosteo_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     ParametrosControl := nil;
     inherited;
     with Wizard do
     begin
          if Adelante and EsPaginaActual( FiltrosCondiciones ) then
          begin
               if ( AU_FECHA.Valor = NULLDateTime ) and
                    StrVacio( CC_DESTINO.LLave ) and
                    StrVacio( CC_ORIGEN.LLave ) and
                    StrVacio( TR_MOTIVO.Llave ) and
                    StrVacio( TR_TEXTO.Text ) and
                   ( TR_TIPO.ItemIndex = K_INDICE_TODAS ) and
                   ( TR_STATUS.ItemIndex = K_INDICE_TODAS ) and
                   ( TR_GLOBAL.ItemIndex = K_INDICE_TODAS ) and
                   StrVacio( EInicial.Text )  and
                   StrVacio( EFinal.Text ) and
                   StrVacio( ELista.Text ) and
                   StrVacio( sFiltro.Text ) and
                   StrVacio( ECondicion.LLave )  then
               begin
                    CanMove:= FALSE;
                    ZetaDialogo.ZError(Caption, 'Es necesario indicar por lo menos uno de los filtros', 0 );
               end;

          end;
     end;//with
end;

function TWizNomCancelarTransferenciasCosteo_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CancelaTransferenciaCosteo(ParameterList, Verificacion );
end;

procedure TWizNomCancelarTransferenciasCosteo_DevEx.CargaListaVerificacion;
begin
     dmProcesos.CancelaTransferenciasCosteoGetLista( ParameterList );
end;

function TWizNomCancelarTransferenciasCosteo_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TNomCancelaTransferenciasCosteoGridSelect_DevEx );
end;


procedure TWizNomCancelarTransferenciasCosteo_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmTablas do
     begin
          cdsMotivoTransfer.Conectar;
     end;
     if CC_ORIGEN.LookupDataset <> NIL then
        CC_ORIGEN.LookupDataset.Conectar;

     if CC_DESTINO.LookupDataset <> NIL then
        CC_DESTINO.LookupDataset.Conectar;
     //DevEx
     Advertencia.Caption := 'Al ejecutar el proceso quedan eliminadas las transferencias de los empleados seleccionados.';

end;

procedure TWizNomCancelarTransferenciasCosteo_DevEx.LlenaStatusTransferencias;
var
   eTipo: eStatusTransCosteo;
begin
     with TR_STATUS.Lista do
     begin
          BeginUpdate;
          try
             Clear;
             Add( '0=Todas' );
             for eTipo := low( eStatusTransCosteo ) to high( eStatusTransCosteo ) do
             begin
                  Add( Format( '%d=%s', [ ord( eTipo )+1, ObtieneElemento( lfStatusTransCosteo, Ord( eTipo ) ) ] ) );
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TWizNomCancelarTransferenciasCosteo_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
          ParametrosControl := AU_FECHA
     else
         ParametrosControl := nil;
     inherited;
end;


end.
