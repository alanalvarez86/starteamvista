unit FNomImportarMovAcuRazonSocialGridShow_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, StdCtrls, ExtCtrls,
     ZBaseGridShow_DevEx,
     ZetaDBGrid, cxGraphics, cxLookAndFeels,
     cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxControls, cxStyles, dxSkinscxPCPainter,
     cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
     cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
     ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
     cxButtons;

type
  TNomImportarMovAcuRazonSocialGridShow_DevEx = class(TBaseGridShow_DevEx)
    RENGLON: TcxGridDBColumn;
    CB_CODIGO: TcxGridDBColumn;
    CO_NUMERO: TcxGridDBColumn;
    MO_MONTO: TcxGridDBColumn;
    NUM_ERROR: TcxGridDBColumn;
    DESC_ERROR: TcxGridDBColumn;
    RS_CODIGO: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  NomImportarMovAcuRazonSocialGridShow_DevEx: TNomImportarMovAcuRazonSocialGridShow_DevEx;

implementation

{$R *.DFM}

end.
