unit FAsiImportacionClasificacionesGridShow_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Grids, DBGrids, ZetaDBGrid, StdCtrls,
  Buttons, ExtCtrls, ZBaseGridShow_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, cxControls, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons,
  cxCalendar;

type
  TAsiImportacionClasificacionesGridShow_DevEx = class(TBaseGridShow_DevEx)
    procedure FormShow(Sender: TObject);
  private
    procedure Connect;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AsiImportacionClasificacionesGridShow_DevEx: TAsiImportacionClasificacionesGridShow_DevEx;

implementation

{$R *.dfm}

uses
  DGlobal,ZetaCommonLists, ZetaCommonTools, ZetaCommonClasses;

procedure TAsiImportacionClasificacionesGridShow_DevEx.FormShow(Sender: TObject);
begin
  Connect;
  inherited;
end;

procedure TAsiImportacionClasificacionesGridShow_DevEx.Connect;
begin
     ZetaDbGridDbTableView.Columns[6].Visible := Global.NumNiveles >= 1;
     ZetaDbGridDbTableView.Columns[7].Visible := ( Global.NumNiveles >= 2 );
     ZetaDbGridDbTableView.Columns[8].Visible := ( Global.NumNiveles >= 3 );
     ZetaDbGridDbTableView.Columns[9].Visible := ( Global.NumNiveles >= 4 );
     ZetaDbGridDbTableView.Columns[10].Visible := ( Global.NumNiveles >= 5 );
     ZetaDbGridDbTableView.Columns[11].Visible := ( Global.NumNiveles >= 6 );
     ZetaDbGridDbTableView.Columns[12].Visible := ( Global.NumNiveles >= 7 );
     ZetaDbGridDbTableView.Columns[13].Visible := ( Global.NumNiveles >= 8 );
     ZetaDbGridDbTableView.Columns[14].Visible := ( Global.NumNiveles >= 9 );
{$IFDEF ACS}
     ZetaDbGridDbTableView.Columns[15].Visible := ( Global.NumNiveles >= 10 );
     ZetaDbGridDbTableView.Columns[16].Visible := ( Global.NumNiveles >= 11 );
     ZetaDbGridDbTableView.Columns[17].Visible := ( Global.NumNiveles >= 12 );
{$ENDIF}

     ZetaDbGridDbTableView.Columns[6].Caption := Global.NombreNivel( 1 );
     ZetaDbGridDbTableView.Columns[7].Caption := Global.NombreNivel( 2 );
     ZetaDbGridDbTableView.Columns[8].Caption := Global.NombreNivel( 3 );
     ZetaDbGridDbTableView.Columns[9].Caption := Global.NombreNivel( 4 );
     ZetaDbGridDbTableView.Columns[10].Caption := Global.NombreNivel( 5 );
     ZetaDbGridDbTableView.Columns[11].Caption := Global.NombreNivel( 6 );
     ZetaDbGridDbTableView.Columns[12].Caption := Global.NombreNivel( 7 );
     ZetaDbGridDbTableView.Columns[13].Caption := Global.NombreNivel( 8 );
     ZetaDbGridDbTableView.Columns[14].Caption := Global.NombreNivel( 9 );
{$IFDEF ACS}
     ZetaDbGridDbTableView.Columns[15].Caption := Global.NombreNivel( 10 );
     ZetaDbGridDbTableView.Columns[16].Caption := Global.NombreNivel( 11 );
     ZetaDbGridDbTableView.Columns[17].Caption := Global.NombreNivel( 12 );
{$ENDIF}
     Width := 500 + ( 75 * Global.NumNiveles );
     Resize;
   
end;

end.
