inherited WizNomImportarMovAcumuladosRS_DevEx: TWizNomImportarMovAcumuladosRS_DevEx
  Left = 583
  Top = 192
  Caption = 'Importar Acumulados'
  ClientHeight = 386
  ClientWidth = 489
  ExplicitWidth = 495
  ExplicitHeight = 415
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 489
    Height = 386
    Header.AssignedValues = [wchvDescriptionFont, wchvGlyph, wchvTitleFont]
    ExplicitWidth = 489
    ExplicitHeight = 416
    inherited Parametros: TdxWizardControlPage
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        ' Proceso que importa los acumulados mensuales de los conceptos q' +
        'ue se indiquen.'
      Header.Title = 'Importar acumulados'
      ExplicitWidth = 467
      ExplicitHeight = 246
      inherited GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 467
        Height = 201
        Align = alCustom
        Visible = False
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 467
        ExplicitHeight = 201
      end
      object GroupBox2: TcxGroupBox
        Left = 0
        Top = 0
        Align = alCustom
        Caption = ' Mes Activo '
        TabOrder = 1
        Height = 75
        Width = 467
        object Label6: TLabel
          Left = 35
          Top = 27
          Width = 23
          Height = 13
          Alignment = taRightJustify
          Caption = 'Mes:'
          Transparent = True
        end
        object iMesActivo: TZetaTextBox
          Left = 65
          Top = 25
          Width = 70
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      ExplicitWidth = 467
      ExplicitHeight = 246
      inherited GrupoParametros: TcxGroupBox
        Top = 70
        ExplicitTop = 70
        ExplicitWidth = 467
        ExplicitHeight = 206
        Height = 176
        Width = 467
      end
      inherited cxGroupBox1: TcxGroupBox
        Anchors = []
        ExplicitWidth = 467
        ExplicitHeight = 70
        Height = 70
        Width = 467
        inherited Advertencia: TcxLabel
          Left = 66
          Top = 0
          Align = alCustom
          Anchors = []
          Style.IsFontAssigned = True
          ExplicitLeft = 66
          ExplicitTop = 0
          ExplicitWidth = 402
          ExplicitHeight = 68
          Height = 68
          Width = 402
          AnchorY = 34
        end
        inherited cxImage1: TcxImage
          Left = 0
          Top = 0
          Align = alNone
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitHeight = 68
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      PageVisible = False
      ExplicitWidth = 467
      ExplicitHeight = 246
      inherited sCondicionLBl: TLabel
        Left = 6
        Top = 137
        ExplicitLeft = 6
        ExplicitTop = 137
      end
      inherited sFiltroLBL: TLabel
        Left = 31
        Top = 166
        ExplicitLeft = 31
        ExplicitTop = 166
      end
      inherited Seleccionar: TcxButton
        Left = 134
        Top = 254
        ExplicitLeft = 134
        ExplicitTop = 254
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 58
        Top = 134
        ExplicitLeft = 58
        ExplicitTop = 134
      end
      inherited sFiltro: TcxMemo
        Left = 58
        Top = 165
        Style.IsFontAssigned = True
        ExplicitLeft = 58
        ExplicitTop = 165
      end
      inherited GBRango: TGroupBox
        Left = 58
        Top = 4
        ExplicitLeft = 58
        ExplicitTop = 4
      end
      inherited bAjusteISR: TcxButton
        Left = 380
        Top = 166
        ExplicitLeft = 380
        ExplicitTop = 166
      end
    end
    object Archivo: TdxWizardControlPage
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.DescriptionFont.Charset = DEFAULT_CHARSET
      Header.DescriptionFont.Color = clDefault
      Header.DescriptionFont.Height = -11
      Header.DescriptionFont.Name = 'Tahoma'
      Header.DescriptionFont.Style = []
      Header.TitleFont.Charset = DEFAULT_CHARSET
      Header.TitleFont.Color = clDefault
      Header.TitleFont.Height = -11
      Header.TitleFont.Name = 'Tahoma'
      Header.TitleFont.Style = [fsBold]
      Header.Description = 'Seleccione los par'#225'metros de carga de archivo.'
      Header.Title = 'Archivo'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 467
        Height = 144
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object Label5: TLabel
          Left = 86
          Top = 65
          Width = 39
          Height = 13
          Alignment = taRightJustify
          Caption = 'A&rchivo:'
          FocusControl = sArchivo
        end
        object Label7: TLabel
          Left = 14
          Top = 90
          Width = 111
          Height = 13
          Alignment = taRightJustify
          Caption = 'Operaci'#243'n con &Montos:'
          FocusControl = iOperacion
        end
        object Label8: TLabel
          Left = 45
          Top = 115
          Width = 80
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de &Formato:'
          FocusControl = iFormato
        end
        object LblYear: TLabel
          Left = 101
          Top = 15
          Width = 22
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'&o:'
          Enabled = False
          FocusControl = YearAcum
          Transparent = True
        end
        object LblMes: TLabel
          Left = 100
          Top = 40
          Width = 23
          Height = 13
          Alignment = taRightJustify
          Caption = 'M&es:'
          Enabled = False
          FocusControl = MesAcum
          Transparent = True
        end
        object BGuardaArchivo: TcxButton
          Left = 436
          Top = 61
          Width = 21
          Height = 21
          Hint = 'Buscar archivo a importar'
          OptionsImage.Glyph.Data = {
            DA050000424DDA05000000000000360000002800000013000000130000000100
            200000000000A40500000000000000000000000000000000000000B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFDFF5
            FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
            FCFFDFF5FCFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF10B7EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF40C5EFFF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF40C5EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6CD3
            F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF6CD3F2FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFF93DEF6FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF97E0F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFEBF9FDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF54CB
            F0FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CF
            F1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF80D9F4FF80D9
            F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC3EDFAFFDFF5FCFFDFF5FCFFDFF5
            FCFF70D4F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
          OptionsImage.Margin = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BGuardaArchivoClick
        end
        object sArchivo: TEdit
          Left = 129
          Top = 61
          Width = 300
          Height = 21
          TabOrder = 2
        end
        object iOperacion: TZetaKeyCombo
          Left = 129
          Top = 86
          Width = 159
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 4
          ListaFija = lfOperacionMontos
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object iFormato: TZetaKeyCombo
          Left = 129
          Top = 111
          Width = 159
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 5
          ListaFija = lfFormatoASCII
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object YearAcum: TZetaNumero
          Left = 129
          Top = 11
          Width = 57
          Height = 21
          Enabled = False
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
        end
        object MesAcum: TZetaKeyCombo
          Left = 129
          Top = 36
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          Enabled = False
          ParentCtl3D = False
          TabOrder = 1
          ListaFija = lfMes13
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object CBLimpiarAcumulados: TCheckBox
          Left = 340
          Top = 111
          Width = 116
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Limpiar Acumulados:'
          TabOrder = 6
          Visible = False
        end
      end
      object PanelAnimation: TPanel
        Left = 0
        Top = 144
        Width = 467
        Height = 102
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitHeight = 132
        object Animate: TAnimate
          Left = 0
          Top = 60
          Width = 467
          Height = 25
          Align = alBottom
          Active = True
          AutoSize = False
          CommonAVI = aviCopyFiles
          StopFrame = 34
          ExplicitTop = 90
        end
        object PanelMensaje: TPanel
          Left = 0
          Top = 85
          Width = 467
          Height = 17
          Align = alBottom
          BevelOuter = bvNone
          Caption = 'Verificando Archivo ...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          ExplicitTop = 115
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 504
    Top = 4
  end
  object OpenDialog: TOpenDialog
    Filter = 'ASCII Movimientos (*.dat)|*.dat|Todos (*.*)|*.*'
    Options = [ofHideReadOnly, ofAllowMultiSelect]
    Left = 348
    Top = 65534
  end
  object SaveDialog: TSaveDialog
    DefaultExt = 'txt'
    FileName = 'ImpError'
    Filter = 
      'Archivos Texto ( *.txt )|*.txt|Archivos Datos ( *.dat )|*.dat|Bi' +
      't'#225'coras (*.log)|*.log|Todos |*.*'
    FilterIndex = 0
    Title = 'Grabar Errores En'
    Left = 377
    Top = 65534
  end
end
