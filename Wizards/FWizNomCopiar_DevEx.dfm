inherited WizNomCopiar_DevEx: TWizNomCopiar_DevEx
  Left = 518
  Top = 140
  Caption = 'Copiar una N'#243'mina hacia otro per'#237'odo'
  ClientHeight = 400
  ClientWidth = 467
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 467
    Height = 400
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que realiza la copia de un periodo de n'#243'mina hacia otro.' +
        ' Se utiliza para el pago de retroactivos por medio de la diferen' +
        'cia entre dos periodos de n'#243'mina.'
      Header.Title = 'Copiar n'#243'mina'
      object TipoLBL: TLabel
        Left = 87
        Top = 36
        Width = 78
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de N'#243'mina:'
        Transparent = True
      end
      object TipoNomina: TZetaKeyCombo
        Left = 167
        Top = 32
        Width = 190
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 0
        ListaFija = lfTipoPeriodo
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = True
      end
      object NominaOriginal: TcxGroupBox
        Left = 87
        Top = 59
        Caption = ' N'#243'mina Original '
        TabOrder = 1
        Height = 80
        Width = 270
        object YearOriginalLBL: TLabel
          Left = 54
          Top = 20
          Width = 22
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'o:'
          Transparent = True
        end
        object NumeroOriginalLBL: TLabel
          Left = 36
          Top = 44
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
          Transparent = True
        end
        object YearOriginal: TZetaNumero
          Left = 80
          Top = 16
          Width = 57
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
        end
        object NumeroOriginal: TZetaNumero
          Left = 80
          Top = 40
          Width = 57
          Height = 21
          Mascara = mnDias
          TabOrder = 1
          Text = '0'
        end
      end
      object NominaNueva: TcxGroupBox
        Left = 87
        Top = 149
        Caption = ' N'#243'mina Nueva '
        TabOrder = 2
        Height = 80
        Width = 270
        object YearNuevoLBL: TLabel
          Left = 56
          Top = 20
          Width = 22
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'o:'
          Transparent = True
        end
        object NumeroNuevoLBL: TLabel
          Left = 38
          Top = 44
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
          Transparent = True
        end
        object YearNuevo: TZetaNumero
          Left = 81
          Top = 16
          Width = 57
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
        end
        object NumeroNuevo: TZetaNumero
          Left = 81
          Top = 40
          Width = 57
          Height = 21
          Mascara = mnDias
          TabOrder = 1
          Text = '0'
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 165
        Width = 445
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 445
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 377
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 27
        Top = 138
      end
      inherited sFiltroLBL: TLabel
        Left = 52
        Top = 167
      end
      inherited Seleccionar: TcxButton
        Left = 155
        Top = 255
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 79
        Top = 135
      end
      inherited sFiltro: TcxMemo
        Left = 79
        Top = 166
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 79
        Top = 5
      end
      inherited bAjusteISR: TcxButton
        Left = 393
        Top = 165
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Top = 297
  end
end
