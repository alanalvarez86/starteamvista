unit FWizNomReFoliarRecibos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ComCtrls, ExtCtrls, Mask,
     FWizNomBase_DevEx,
     ZetaEdit,
     ZetaDBTextBox,
     ZetaWizard,
     ZetaNumero,  
     cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl;

type
  TWizNomReFoliarRecibos_DevEx = class(TWizNomBase_DevEx)
    Panel2: TPanel;
    Label5: TLabel;
    nFolio: TZetaKeyLookup_DevEx;
    Folios: TdxWizardControlPage;
    GroupBox2: TGroupBox;
    Label10: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    nRenumeraDel: TZetaNumero;
    nRenumeraAl: TZetaNumero;
    NumeroInicial: TZetaNumero;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);//DevEx(by am): Agregado para enfocar correctamente los controles.
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean;override;
    procedure CargaParametros;override;
  public
    { Public declarations }
  end;

var
  WizNomReFoliarRecibos_DevEx: TWizNomReFoliarRecibos_DevEx;

implementation

uses ZetaDialogo,
     ZetaCommonClasses,
     DCatalogos,
     DProcesos;

{$R *.DFM}

procedure TWizNomReFoliarRecibos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H33422_Refoliar_recibos;
     ParametrosControl := nFolio;
     nFolio.LookUpDataset :=  dmCatalogos.cdsFolios;
     //DevEx(by am): Es necesario especificar las paginas del wizard.
     Parametros.PageIndex := WizardControl.PageCount - 3;
     Folios.PageIndex := WizardControl.PageCount - 2; //3
     Ejecucion.PageIndex := WizardControl.PageCount - 1;
end;

procedure TWizNomReFoliarRecibos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmCatalogos.cdsFolios do
     begin
          Refrescar;
          First;
          with nFolio do
          begin
               if Eof then
                  Llave := '1'
               else
                   Llave := FieldByName( 'FL_CODIGO' ).AsString;
          end;
     end;
     Advertencia.Caption := 'Al aplicar el proceso se re-foliar�n los recibos de n�mina indicados.';
end;

procedure TWizNomReFoliarRecibos_DevEx.CargaParametros;
begin
     inherited;
     with Descripciones do
     begin
          AddString( 'Folio', nFolio.Llave );
          AddInteger( 'Renumerar del', nRenumeraDel.ValorEntero );
          AddInteger( 'Renumerar al', nRenumeraAl.ValorEntero );
     end;
     with ParameterList do
     begin
          AddString( 'Folio', nFolio.Llave );
          AddInteger( 'RenumerarDel', nRenumeraDel.ValorEntero );
          AddInteger( 'RenumerarAl', nRenumeraAl.ValorEntero );
          AddInteger( 'FolioInicial', NumeroInicial.ValorEntero );
     end;
end;

procedure TWizNomReFoliarRecibos_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     ParametrosControl := nil;
     if CanMove and Wizard.Adelante then
     begin
          if Wizard.EsPaginaActual( Parametros ) then
          begin
               CanMove := nFolio.Valor in [ 1..5 ];
               if CanMove then
               begin
                    with nFolio.LookUpDataset do
                    begin
                         nRenumeraDel.Valor := FieldByName( 'FL_INICIAL' ).AsInteger;
                         nRenumeraAl.Valor := FieldByName( 'FL_FINAL' ).AsInteger;
                         NumeroInicial.Valor := nRenumeraAl.Valor + 1;
                    end;
               end
               else
               begin
                    ZetaDialogo.zError(Caption, 'El N�mero de Folio Debe Estar Entre Uno y Cinco', 0 );
                    nFolio.SetFocus;
               end;
          end;
     end;
     inherited;
end;

procedure TWizNomReFoliarRecibos_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.Adelante then
          if Wizard.EsPaginaActual( Parametros ) then
             ParametrosControl := nFolio;
     inherited;
end;

function TWizNomReFoliarRecibos_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.ReFoliarRecibos(ParameterList);
end;

end.
