inherited GridAsistenciaSesiones_DevEx: TGridAsistenciaSesiones_DevEx
  Width = 531
  Caption = 'Seleccione Los Empleados Deseados'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 515
    inherited OK_DevEx: TcxButton
      Left = 350
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 428
    end
  end
  inherited PanelSuperior: TPanel
    Width = 515
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 515
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
      end
      object SE_FOLIO: TcxGridDBColumn
        Caption = 'Grupo'
        DataBinding.FieldName = 'SE_FOLIO'
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
