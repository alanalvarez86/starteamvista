unit FWizEmpAplicarTabulador_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ExtCtrls, Mask, Buttons, ComCtrls,
     ZetaFecha, ZetaEdit, FWizEmpBaseFiltro,
     cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, dxSkinsCore, 
     TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
     ZetaCXWizard, ZetaKeyLookup_DevEx, cxButtons, cxRadioGroup, cxTextEdit,
     cxMemo, cxGroupBox, cxLabel, dxGDIPlusClasses, cxImage,
     dxCustomWizardControl, dxWizardControl, cxCheckBox;

type
  TWizEmpAplicarTabulador_DevEx = class(TWizEmpBaseFiltro)
    dReferenciaLBL: TLabel;
    dReferencia: TZetaFecha;
    RGRevisar: TcxRadioGroup;
    sDescripcionLBL: TLabel;
    sDescripcion: TEdit;
    sObserva: TcxMemo;
    sObservaLBL: TLabel;
    lIncapacitados: TcxCheckBox;
    dIMSS: TZetaFecha;
    Label1: TLabel;

    procedure FormCreate(Sender: TObject);
    procedure dReferenciaValidDate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizEmpAplicarTabulador_DevEx: TWizEmpAplicarTabulador_DevEx;

implementation

uses DCliente,
     DProcesos,
     ZetaCommonClasses,
     ZBaseSelectGrid_DevEx,
     FEmpAplicaTabuladorGridSelect_DevEx;

{$R *.DFM}

procedure TWizEmpAplicarTabulador_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := dReferencia;
     dReferencia.Valor := dmCliente.FechaDefault;
     HelpContext := H10165_Aplicar_tabulador;
end;

procedure TWizEmpAplicarTabulador_DevEx.CargaParametros;
begin

     inherited CargaParametros;

     with ParameterList do
     begin
          AddDate( 'Fecha', dReferencia.Valor );
          AddString( 'Descripcion', sDescripcion.Text );
          AddString( 'Observaciones', sObserva.Text );
          AddBoolean( 'RevisarDiferencias', ( RGRevisar.ItemIndex = 1 ) );
          AddBoolean( 'NoIncluirIncapacitados', lIncapacitados.Checked );
          AddDate( 'Fecha2', dIMSS.Valor );
     end;
     with Descripciones do
     begin
          AddDate( 'Fecha de cambio', dReferencia.Valor );
          AddDate( 'Fecha de aviso a IMSS', dImss.Valor );
          if RGRevisar.ItemIndex = 0 then
             AddString( 'Revisar', 'Todo el tabulador' )
          else
              AddString( 'Revisar', 'Diferencias de salario diario' );
          AddString( 'Descripci�n', sDescripcion.Text );
          AddString( 'Observaciones', sObserva.Text );
          AddBoolean( 'No inclu�r incapacitados?', lIncapacitados.Checked );
     end;

end;

procedure TWizEmpAplicarTabulador_DevEx.CargaListaVerificacion;
begin
     dmProcesos.AplicarTabuladorGetList( ParameterList );
end;

function TWizEmpAplicarTabulador_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpAplicaTabuladorGridSelect_DevEx );
end;

function TWizEmpAplicarTabulador_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.AplicarTabulador( ParameterList, Verificacion );
end;

procedure TWizEmpAplicarTabulador_DevEx.dReferenciaValidDate(Sender: TObject);
begin
     inherited;
     dIMSS.Valor := dReferencia.Valor;
end;

procedure TWizEmpAplicarTabulador_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'Se actualizar� el salario de los empleados seleccionados, '
                      + 'gener�ndose el movimiento de Kardex respectivo.';
end;

procedure TWizEmpAplicarTabulador_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := dReferencia
     else
         ParametrosControl := nil;
     inherited;
end;
procedure TWizEmpAplicarTabulador_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     ParametrosControl := nil;
end;


end.
