unit FWizSistImportarTablas_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms, Dialogs, ComCtrls,
  StdCtrls, ZetaKeyCombo, ExtCtrls, ZetaEdit, ZcxBaseWizard,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, TressMorado2013, dxSkinsDefaultPainters,
  cxContainer, cxEdit, ZetaCXWizard, dxGDIPlusClasses, cxImage,
  cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl, Menus, cxRadioGroup, cxButtons, cxListBox;

type
  TWizSistImportarTablas_DevEx = class(TcxBaseWizard)
    OpenDialog: TOpenDialog;
    gbArchivo: TcxGroupBox;
    ArchivoSeek: TcxButton;
    Archivo: TEdit;
    gbBaseDatos: TcxGroupBox;
    lblDescripcion: TLabel;
    lblCodigo: TLabel;
    lblTipo: TLabel;
    DB_CODIGO: TZetaEdit;
    DB_DESCRIP: TZetaEdit;
    DB_TIPO: TZetaEdit;
    gbTablaDestino: TcxGroupBox;
    gbModo: TcxGroupBox;
    TablaDestino: TcxListBox;
    rbNuevo: TcxRadioButton;
    rbEncimar: TcxRadioButton;
    procedure ArchivoSeekClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TablaDestinoClick(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
    ArchivoVerif : String;
    function LeerArchivo: Boolean;
    function GetArchivo: String;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }

  end;

var
  WizSistImportarTablas_DevEx: TWizSistImportarTablas_DevEx;

implementation

{$R *.DFM}

uses DProcesos,
     DSistema,
     DCliente,
     DConsultas,
     ZetaDialogo,
     ZetaMessages,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaClientTools,
     ZAsciiTools,
     DGlobal,
     ZGlobalTress,
     ZetaClientDataSet, DXMLTools;

procedure TWizSistImportarTablas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := Archivo;
     ArchivoVerif:= VACIO;
     HelpContext := H_SIST_BASE_DATOS;

     //DevEx
     Parametros.PageIndex := 0;
     Ejecucion.PageIndex := 1;
end;

procedure TWizSistImportarTablas_DevEx.CargaParametros;
var cdsXML: TZetaClientDataSet;
begin
     cdsXML := TZetaClientDataSet.Create(Self);   
     
     with cdsXML do
     begin
          Active:= False;
          Filtered := False;
          Filter := '';
          IndexFieldNames := '';
          LoadFromFile(Archivo.Text);
     end;
     inherited CargaParametros;

     with ParameterList do
     begin                                      
          AddString ( 'Tabla', TablaDestino.Items[TablaDestino.ItemIndex] );
          AddString ( 'Archivo', Archivo.Text );
          AddBoolean('Encimar', rbEncimar.Checked);
          if rbNuevo.Checked then
             AddString( 'Modo', rbNuevo.Caption )
          else
              AddString( 'Modo', rbEncimar.Caption );  
          AddString ('BaseDatosUbicacion', dmSistema.cdsSistBaseDatos.FieldByName('DB_DATOS').AsString);
          AddString ('Usuario', dmSistema.cdsSistBaseDatos.FieldByName('DB_USRNAME').AsString);
          AddString ('Password', dmSistema.cdsSistBaseDatos.FieldByName('DB_PASSWRD').AsString);
          AddVariant ('DatosTabla', cdsXML.Data);
     end;

     with Descripciones do
     begin
          AddString ('Base Datos C�digo', DB_CODIGO.Text );
          AddString ('Base Datos Descripci�n', DB_DESCRIP.Text );
          AddString ('Base Datos Tipo', DB_TIPO.Text );
          AddString ( 'Tabla destino', TablaDestino.Items[TablaDestino.ItemIndex] );
          AddString ( 'Archivo a importar', Archivo.Text );
          if rbNuevo.Checked then
             AddString( 'Modo de importaci�n', rbNuevo.Caption )
          else
              AddString( 'Modo de importaci�n', rbEncimar.Caption ); 
     end;
end;

function TWizSistImportarTablas_DevEx.GetArchivo: String;
begin
     Result := Archivo.Text;
end;

function TWizSistImportarTablas_DevEx.LeerArchivo: Boolean;
begin
     Application.ProcessMessages;
     CargaParametros;
     Result := TRUE;
        ArchivoVerif := GetArchivo
end;
procedure TWizSistImportarTablas_DevEx.ArchivoSeekClick(
  Sender: TObject);
begin
     inherited;
     Archivo.Text := ZetaClientTools.AbreDialogo( OpenDialog, Archivo.Text, 'xml' );
end;

procedure TWizSistImportarTablas_DevEx.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     ParametrosControl := nil;
     inherited;
     with Wizard do
     begin
          if Adelante then
          begin
               if EsPaginaActual( Parametros ) then
               begin

                    if TablaDestino.ItemIndex < 0 then
                       CanMove := Error( 'Debe seleccionar la tabla destino', TablaDestino )
                    else if ZetaCommonTools.StrVacio( GetArchivo ) then
                       CanMove := Error( 'Debe especificarse un archivo a importar', Parametros )
                    else
                       if not FileExists( GetArchivo ) then
                          CanMove := Error( 'El archivo ' + GetArchivo + ' no existe', Parametros )
                       else
                       begin
                            if ( ArchivoVerif <> GetArchivo ) or
                               ( ZetaDialogo.ZConfirm( Caption, 'El archivo ' + GetArchivo + ' ya fu� verificado !' + CR_LF +
                                                       'Volver a verificar ?', 0, mbYes ) ) then
                               CanMove := LeerArchivo
                            else
                               CanMove := TRUE;
                       end;
               end;
          end;
     end;
end;

function TWizSistImportarTablas_DevEx.EjecutarWizard: Boolean;
begin 
     dmConsultas.BaseDatosImportacionXMLCSV := dmSistema.GetBaseDatosImportacion;
     Result := dmProcesos.ImportarTablas( ParameterList );
end;

procedure TWizSistImportarTablas_DevEx.FormShow(Sender: TObject);
begin
     //PageControl.ActivePage := Parametros;       //DevEx
     // Localizar Base de Datos con la que se est� trabajando (empresa actual)
     // y mostrar en campos
     DB_CODIGO.Text := dmSistema.cdsSistBaseDatos.FieldByName ('DB_CODIGO').AsString;
     DB_DESCRIP.Text := dmSistema.cdsSistBaseDatos.FieldByName ('DB_DESCRIP').AsString;
     DB_TIPO.Text := ObtieneElemento (lfTipoCompanyName, dmSistema.cdsSistBaseDatos.FieldByName ('DB_TIPO').AsInteger);

     // Llenar lista de tablas destino
     dmSistema.GetTablasBD(dmSistema.cdsSistBaseDatos.FieldByName ('DB_DATOS').AsString,
             dmSistema.cdsSistBaseDatos.FieldByName ('DB_USRNAME').AsString,
             dmSistema.cdsSistBaseDatos.FieldByName ('DB_PASSWRD').AsString);
     dmSistema.cdsTablasBD.First;
     while not dmSistema.cdsTablasBD.Eof do
     begin 
           TablaDestino.Items.Add(dmSistema.cdsTablasBD.FieldByName ('TABLE_NAME').AsString);
           dmSistema.cdsTablasBD.Next;
     end;
     // ----- -----

     // Mostrar archivo xml default
     Archivo.Text := ExtractFilePath( Application.ExeName ) + 'Archivo.xml';

     // ----- -----

     Advertencia.Caption := 'Al aplicar el proceso se reemplazar� y/o a�adir� la informaci�n contenida en el archivo ' +
     ExtractFileName(Archivo.Text) +' en la base de datos seleccionada. ';
end;

procedure TWizSistImportarTablas_DevEx.TablaDestinoClick(Sender: TObject);
begin
     Archivo.Text := ExtractFilePath(Archivo.Text) + TablaDestino.Items[TablaDestino.ItemIndex] + '.xml';
end;

procedure TWizSistImportarTablas_DevEx.WizardAfterMove(Sender: TObject);
begin
     //DevEx
     if Wizard.EsPaginaActual(Parametros) then
          ParametrosControl := Archivo
     else
         ParametrosControl := nil;
     inherited;

     with Wizard do
     begin
          if Adelante then
          begin
               if EsPaginaActual( Ejecucion ) then
               begin
               {     Advertencia.Caption := 'Se importar�n los registros del archivo ' + ExtractFileName(Archivo.Text);

                    Advertencia.Clear;
                    Advertencia.Lines.Append('');
                    Advertencia.Lines.Append('Se importar�n los registros del archivo ' + ExtractFileName(Archivo.Text));
                    if rbNuevo.Checked then
                       Advertencia.Lines.Append('- ' + rbNuevo.Caption)
                    else
                        Advertencia.Lines.Append('- ' + rbEncimar.Caption);

                    Advertencia.Lines.Append('');
                    Advertencia.Lines.Append('Presione el bot�n Aplicar para iniciar el proceso.');
               }
                Advertencia.Caption := 'Al aplicar el proceso se reemplazar� y/o a�adir� la informaci�n contenida en el archivo ' +
     ExtractFileName(Archivo.Text) +' en la base de datos seleccionada. ';
               end
          end
     end
end;

end.
