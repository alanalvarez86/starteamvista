inherited EmpRecalculaSaldosVacaGridSelect_DevEx: TEmpRecalculaSaldosVacaGridSelect_DevEx
  Left = 232
  Top = 247
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Seleccione los Empleados a Recalcular Saldos de Vacaciones'
  ClientHeight = 301
  ClientWidth = 463
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 265
    Width = 463
    inherited OK_DevEx: TcxButton
      Left = 294
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 372
    end
  end
  inherited PanelSuperior: TPanel
    Width = 463
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 463
    Height = 230
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
      end
      object CB_FEC_ANT: TcxGridDBColumn
        Caption = 'Fecha antig'#252'edad'
        DataBinding.FieldName = 'CB_FEC_ANT'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
      end
      object ZetaDBGridDBTableViewColumn4: TcxGridDBColumn
        Caption = 'A Pagar'
        Visible = False
      end
      object ZetaDBGridDBTableViewColumn5: TcxGridDBColumn
        Caption = 'D'#237'as prima vac.'
        Visible = False
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
