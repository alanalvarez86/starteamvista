unit FWizNomCalcularRepartoAhorro_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, Mask,
     ZetaEdit,
     ZetaWizard,
     ZetaNumero, ZCXBaseWizardFiltro, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl;

type
  TWizNomCalcularRepartoAhorro_DevEx = class(TBaseCXWizardFiltro)
    Year: TZetaNumero;
    YearLBL: TLabel;
    TipoAhorroLBL: TLabel;
    TipoAhorro: TZetaKeyLookup_DevEx;
    Monto: TZetaNumero;
    MontoLBL: TLabel;
    ProporcionalLBL: TLabel;
    Proporcional: TcxMemo;
    ProporcionalBuild: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure ProporcionalBuildClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure CargaParametros;override;
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizNomCalcularRepartoAhorro_DevEx: TWizNomCalcularRepartoAhorro_DevEx;

implementation

uses ZConstruyeFormula,
     ZGlobalTress,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaDialogo,
     DCliente,
     DProcesos, dTablas;

{$R *.DFM}

procedure TWizNomCalcularRepartoAhorro_DevEx.FormCreate(Sender: TObject);
begin
     Year.Valor := dmCliente.YearDefault;
     TipoAhorro.Tag := K_GLOBAL_DEF_REPARTIR_AHORROS_TIPO;
     Monto.Tag := K_GLOBAL_DEF_REPARTIR_AHORROS_MONTO;
     Proporcional.Tag := K_GLOBAL_DEF_REPARTIR_AHORROS_PROPORCIONAL;
     inherited;
     ParametrosControl := Year;
     HelpContext := H30355_Calcular_reparto_de_ahorros;
     TipoAhorro.LookupDataset := dmTablas.cdsTAhorro;
end;

procedure TWizNomCalcularRepartoAhorro_DevEx.FormShow(Sender: TObject);
begin
     dmTablas.cdsTAhorro.Conectar;
     inherited;
end;

procedure TWizNomCalcularRepartoAhorro_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
ParametrosControl := nil;
     inherited;
     if CanMove then
     begin
          with Wizard do
          begin
               if EsPaginaActual( Parametros ) and Adelante then
               begin
                    if StrVacio( Proporcional.Lines.Text ) then
                    begin
                         zError( Caption, '� F�rmula Para Proporcionar No Fu� Especificada !', 0 );
                         ActiveControl := Proporcional;
                         CanMove := False;
                    end
                    else
                        if ( Monto.Valor = 0 ) then
                        begin
                             zError( Caption, '� Monto A Repartir No Fu� Especificado !', 0 );
                             ActiveControl := Monto;
                             CanMove := False;
                        end;
               end;
          end;
     end;
end;

procedure TWizNomCalcularRepartoAhorro_DevEx.CargaParametros;
begin
     inherited;
     with ParameterList do
     begin
          AddInteger( 'Year', Year.ValorEntero );
          AddFloat( 'Monto', Monto.Valor );
          AddString( 'TipoAhorro' , TipoAhorro.Llave );
          AddString( 'Proporcional', Proporcional.Text );
     end;
       with Descripciones do
     begin
          AddInteger( 'A�o', Year.ValorEntero );
          AddFloat( 'Monto', Monto.Valor );
          AddString( 'Tipo de Ahorro' , TipoAhorro.Llave );
          AddString( 'Proporcional a ', Proporcional.Text );
     end;
end;

procedure TWizNomCalcularRepartoAhorro_DevEx.ProporcionalBuildClick(Sender: TObject);
begin
     Proporcional.Text:= GetFormulaConst( enEmpleado , Proporcional.Text, Proporcional.SelStart, evBase );
end;

function TWizNomCalcularRepartoAhorro_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CalcularRepartoAhorro(ParameterList);
end;

procedure TWizNomCalcularRepartoAhorro_DevEx.WizardAfterMove(
  Sender: TObject);
begin
  inherited;
  if Wizard.EsPaginaActual(Parametros) then
ParametrosControl := Year;
end;

end.



