inherited AsiImportacionClasificacionesGridShow_DevEx: TAsiImportacionClasificacionesGridShow_DevEx
  Left = 371
  Top = 160
  Width = 971
  Height = 282
  Caption = 'Lista de Errores Detectados'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 205
    Width = 955
    Height = 39
    inherited OK_DevEx: TcxButton
      Left = 787
      Top = 6
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 866
      Top = 6
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 955
    Height = 205
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object RENGLON: TcxGridDBColumn
        Caption = 'Rengl'#243'n'
        DataBinding.FieldName = 'RENGLON'
        MinWidth = 65
        Width = 65
      end
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
        MinWidth = 75
        Width = 75
      end
      object AU_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'AU_FECHA'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.AssignedValues.EditFormat = True
        Properties.DisplayFormat = 'dd/mmm/yyyy'
        MinWidth = 74
      end
      object CB_PUESTO: TcxGridDBColumn
        Caption = 'Puesto'
        DataBinding.FieldName = 'CB_PUESTO'
        MinWidth = 65
      end
      object CB_CLASIFI: TcxGridDBColumn
        Caption = 'Clasificaci'#243'n'
        DataBinding.FieldName = 'CB_CLASIFI'
        MinWidth = 85
      end
      object CB_TURNO: TcxGridDBColumn
        Caption = 'Turno'
        DataBinding.FieldName = 'CB_TURNO'
        MinWidth = 65
      end
      object CB_NIVEL1: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL1'
        MinWidth = 81
      end
      object CB_NIVEL2: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL2'
        Visible = False
        Width = 20
      end
      object CB_NIVEL3: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL3'
        Visible = False
        Width = 20
      end
      object CB_NIVEL4: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL4'
        Visible = False
        Width = 20
      end
      object CB_NIVEL5: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL5'
        Visible = False
        Width = 20
      end
      object CB_NIVEL6: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL6'
        Visible = False
        Width = 20
      end
      object CB_NIVEL7: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL7'
        Visible = False
        Width = 20
      end
      object CB_NIVEL8: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL8'
        Visible = False
        Width = 20
      end
      object CB_NIVEL9: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL9'
        Visible = False
        Width = 20
      end
      object CB_NIVEL10: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL10'
        Visible = False
        Width = 20
      end
      object CB_NIVEL11: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL11'
        Visible = False
        Width = 20
      end
      object CB_NIVEL12: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL12'
        Visible = False
        Width = 20
      end
      object OPERACION: TcxGridDBColumn
        Caption = 'Operaci'#243'n'
        DataBinding.FieldName = 'OPERACION'
        Visible = False
        Width = 20
      end
      object FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'FECHA'
        Visible = False
        Width = 20
      end
      object CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CODIGO'
        Visible = False
        Width = 20
      end
      object ADICIONALES: TcxGridDBColumn
        Caption = 'Adicionales'
        DataBinding.FieldName = 'ADICIONALES'
        Visible = False
        Width = 20
      end
      object COMENTARIO: TcxGridDBColumn
        Caption = 'Comentarios'
        DataBinding.FieldName = 'COMENTARIO'
        Visible = False
        Width = 20
      end
      object NUM_ERROR: TcxGridDBColumn
        Caption = 'Errores'
        DataBinding.FieldName = 'NUM_ERROR'
        MinWidth = 60
        Width = 60
      end
      object DESC_ERROR: TcxGridDBColumn
        Caption = 'Descripci'#243'n de Errores'
        DataBinding.FieldName = 'DESC_ERROR'
        MinWidth = 150
        Width = 450
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DataSource: TDataSource
    Left = 80
    Top = 48
  end
end
