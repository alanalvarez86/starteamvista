unit FEmpRenumeraGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, ExtCtrls, ZBaseSelectGrid_DevEx,
  cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxTextEdit, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons;

type
  TEmpRenumeraGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO_OLD: TcxGridDBColumn;
    CB_CODIGO_NEW: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpRenumeraGridSelect_DevEx: TEmpRenumeraGridSelect_DevEx;

implementation

{$R *.DFM}

procedure TEmpRenumeraGridSelect_DevEx.FormShow(Sender: TObject);
begin
  inherited;
    //DevEx
   (ZetaDBGridDBTableView.GetColumnByFieldName('PrettyName')).Caption := 'Nombre del Empleado';
end;

end.
