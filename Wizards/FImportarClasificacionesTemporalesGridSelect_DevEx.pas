unit FImportarClasificacionesTemporalesGridSelect_DevEx;

interface
                                   
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, Grids, DBGrids, ZetaDBGrid,
  Buttons, ExtCtrls, ZBaseSelectGrid_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, cxControls, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons,
  cxCalendar;

type

  eOpEvento = ( eoNinguno, eoPuesto, eoClasificacion, eoTurno,
                  eoNivel1, eoNivel2, eoNivel3, eoNivel4, eoNivel5, eoNivel6, eoNivel7, eoNivel8, eoNivel9
                  {$ifdef ACS}, eoNivel10, eoNivel11, eoNivel12{$endif} );

  ListaOperaciones = Set of eOpEvento;
  TDatosOperacion = record
  FieldCheck : String;
  ExisteCampo : Boolean;
  MostrarColumnas : Boolean;

   end;

  TImportarClasificacionesTemporalesGridSelect_DevEx = class(TBaseGridSelect_DevEx)

    procedure FormShow(Sender: TObject);

  private
    procedure Conectar;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  ImportarClasificacionesTemporalesGridSelect_DevEx: TImportarClasificacionesTemporalesGridSelect_DevEx;

implementation

uses dGlobal,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo,
     FBaseReportes_DevEx,
     ZBasicoSelectGrid_DevEx,
     ZGlobalTress;

{$R *.dfm}

procedure TImportarClasificacionesTemporalesGridSelect_DevEx.FormShow(
  Sender: TObject);
begin
     Conectar;
     inherited;


end;

procedure TImportarClasificacionesTemporalesGridSelect_DevEx.Conectar;
var
     i : Integer;
begin
     with ZetaDBGriddbtableview do
     begin
          for i := 1 to K_GLOBAL_NIVEL_MAX do
          begin
               Columns[ i + 7 ].Visible := Global.NumNiveles >= i;
               Columns[ i + 7 ].Caption := Global.NombreNivel(i);
          end;

     end;
end;



end.
