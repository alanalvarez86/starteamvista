unit FEmpVerificaTablasGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, ExtCtrls, ZetaMessages, ZBasicoSelectGrid_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, ImgList, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid, cxButtons, cxTextEdit;

type
  TEmpVerificaTablasGridSelect_DevEx = class(TBasicoGridSelect_DevEx)
    DESC_ENTIDAD: TcxGridDBColumn;
    CODIGO: TcxGridDBColumn;
    DESCRIPCION: TcxGridDBColumn;
  private
    { Private declarations }
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
  public
    { Public declarations }
  end;

var
  EmpVerificaTablasGridSelect_DevEx: TEmpVerificaTablasGridSelect_DevEx;

implementation
uses dProcesos;

{$R *.DFM}

{ TEmpVerificaTablasGridSelect }

procedure TEmpVerificaTablasGridSelect_DevEx.WMExaminar(var Message: TMessage);
begin
     //No se hace nada para que al darle doble-click al grid no realice el excluir.
     //inherited;
end;

end.
