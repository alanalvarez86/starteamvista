inherited WizNomCalcularPagoFonacot_DevEx: TWizNomCalcularPagoFonacot_DevEx
  Left = 409
  Top = 213
  Caption = 'Calcular pago de Fonacot'
  ClientHeight = 576
  ClientWidth = 454
  ExplicitWidth = 460
  ExplicitHeight = 605
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 454
    Height = 576
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F40000000467414D410000B18F0BFC6105000000097048597300000EBC00000E
      BC0195BC72490000032A494441545847ED562D6C1441143E8140542010880A04
      028140542048402010E56696D004410202814020100812482A1025B9DEEE2508
      C40A64050281A8A8A8A828C9EDEE3541549C40549CA840204EC0F7BD793B377B
      B7D78683ABBA97BCECBD37EF6FDEDF5C63010B9807243D735B7F9E1DA4FDC7E7
      DBB9799AE4F67B52D8DF9B99BDA947F385F7DF562F26B9791BE7F6888E3DE6F6
      B38A343A076B97E2C26EE0BBA4AC7F87CEC1BD2B709CC0F1AF8AE30029F366E7
      D639FCEE9346865EA8FAECC0D4F276A1A3A98800A91367F695F2FA0C480CFD0D
      50A99D371FA0B93EC1482A06613C294CCF3BAB416687256A75CD8538373F859F
      450FD5ECE9C09A316DA85F17069E244573456F93B2A6A4D9F113F50F11FD21B6
      0ADB220DD97D317E12B069925EF40E0AC7BC45A76BAE938E7BD10722F87BA3DA
      9BF5B8B87F95B28EAE2283E384200B97410F853F6D4CC5116F57692C3AB0CF95
      4E698C72345CCA70FC6404BD4E157946FBF8BD25BC604204DA597417075F4B85
      10DB79B48CAFDC8EB5C4B74E6EA872E37C87D80DF42325531E27449C837051D5
      A0BB65A8B4B61417CD3B92119624B7875E3EB3ABEC154F8F23CEE90F323BA459
      4A09804D3621AC0807FBD2FD9E6776A519618C379005137437BE7B23D92AD2B1
      0BC058A1D95BD0974E07635A0341080BA7E66C9456B7644E6A44C86E97192050
      D7F1DD84E0F1908E9F542472CC346D825006BD819ADF70A329BC6DD25EC6E190
      7B830D2B4E000C12E3F89117533DB984DBD595CE0FD1F4340B03A5775936695C
      17F860338BAEE15BA6FF58035C16E3005E02CE82ED697E249979C9A656114965
      3A1298C05483A08C4B33D3880038DFBC158D32B0D2A8BC01E80B04E31B93BFE3
      AE7D54BB8E75078C3BF6880C1DC100F64173459C523EB3CF70B64547A55106E0
      1A1BB72C750BF365EAF20901C2B5BBA01699D2C02853CED4E34C32240D8CCCB0
      3C2A723AE842AA77082C8DB29954C565CE3D526ECD4A00665D466C16086B16E0
      8023131AD5C5E3A703C11DB2445CD52A321BF8D152A3AC7369D4FFEDAA3CC358
      4E582E33BDF37520DD8BFAD2A8B2E46F1782790D2C1FA0216538FB2A325F2827
      447BA0E51F92B3048E5A65712C6001FF051A8D3FA6D93DEE37EA8F1800000000
      49454E44AE426082}
    ExplicitWidth = 454
    ExplicitHeight = 576
    inherited Parametros: TdxWizardControlPage
      Header.AssignedValues = [wchvDescriptionFont]
      Header.Description = 
        'En este paso deber'#225' seleccionar las n'#243'minas que comprende el pag' +
        'o de Fonacot del mes seleccionado.'
      Header.Title = 'N'#243'minas a pagar'
      ParentFont = False
      ExplicitWidth = 432
      ExplicitHeight = 436
      object GBTipoPrestamo: TGroupBox
        Left = 0
        Top = 46
        Width = 432
        Height = 168
        Align = alCustom
        TabOrder = 0
        object Label1: TLabel
          Left = 9
          Top = 31
          Width = 147
          Height = 13
          Caption = 'Tipo de pr'#233'stamos de Fonacot:'
          Transparent = True
        end
        object Label2: TLabel
          Left = 133
          Top = 53
          Width = 22
          Height = 13
          Caption = 'A'#241'o:'
          Transparent = True
        end
        object Label3: TLabel
          Left = 133
          Top = 77
          Width = 23
          Height = 13
          Caption = 'Mes:'
          Transparent = True
        end
        object NoTipoPrestamo: TZetaTextBox
          Left = 159
          Top = 30
          Width = 50
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object TipoPrestamo: TZetaTextBox
          Left = 213
          Top = 30
          Width = 212
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object MesDefault: TZetaKeyCombo
          Left = 159
          Top = 73
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 1
          ListaFija = lfMeses
          ListaVariable = lvPuesto
          Offset = 1
          Opcional = False
          EsconderVacios = False
        end
        object RGFiltro: TRadioGroup
          Left = 159
          Top = 96
          Width = 265
          Height = 39
          Caption = 'Filtrar por:'
          Columns = 2
          Items.Strings = (
            'Mes'
            'Rango de n'#243'minas')
          TabOrder = 2
          OnClick = RGFiltroClick
        end
        object YearDefaultAct: TZetaNumero
          Left = 159
          Top = 49
          Width = 50
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      Header.AssignedValues = [wchvDescriptionFont]
      ExplicitWidth = 432
      ExplicitHeight = 436
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 432
        ExplicitHeight = 341
        Height = 341
        Width = 432
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 432
        Width = 432
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          ExplicitWidth = 364
          Width = 364
          AnchorY = 57
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      Header.AssignedValues = [wchvDescriptionFont]
      PageVisible = False
      ExplicitWidth = 432
      ExplicitHeight = 436
      inherited sCondicionLBl: TLabel
        Left = 22
        ExplicitLeft = 22
      end
      inherited sFiltroLBL: TLabel
        Left = 47
        ExplicitLeft = 47
      end
      inherited Seleccionar: TcxButton
        Left = 150
        ExplicitLeft = 150
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 74
        ExplicitLeft = 74
      end
      inherited sFiltro: TcxMemo
        Left = 74
        Style.IsFontAssigned = True
        ExplicitLeft = 74
      end
      inherited GBRango: TGroupBox
        Left = 74
        ExplicitLeft = 74
      end
      inherited bAjusteISR: TcxButton
        Left = 388
        ExplicitLeft = 388
      end
    end
    object RangosDNominas: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.AssignedValues = [wchvDescriptionFont]
      Header.DescriptionFont.Charset = DEFAULT_CHARSET
      Header.DescriptionFont.Color = clDefault
      Header.DescriptionFont.Height = -11
      Header.DescriptionFont.Name = 'Tahoma'
      Header.DescriptionFont.Style = []
      Header.Description = 'Complete los campos para indicar los par'#225'metros del pago. '
      Header.Title = ' Par'#225'metros para realizar el pago'
      ParentFont = False
      object GBRangosNominas: TGroupBox
        Left = 0
        Top = 0
        Width = 432
        Height = 436
        Align = alClient
        Caption = 'Rangos de N'#243'minas:'
        TabOrder = 0
      end
    end
    object Ajuste: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.Description = 
        'Deber'#225' indicar el tipo de pr'#233'stamo de ajuste y si desea reportar' +
        ' las incapacidades.'
      Header.Title = 'Definici'#243'n de par'#225'metros'
      ParentFont = False
      object Label4: TLabel
        Left = 5
        Top = 92
        Width = 141
        Height = 13
        Caption = 'Tipo de pr'#233'stamo para &Ajuste:'
        Transparent = True
      end
      object TipoPrestamoAjus: TZetaKeyLookup_DevEx
        Left = 150
        Top = 88
        Width = 265
        Height = 21
        LookupDataset = dmTablas.cdsTPresta
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 50
      end
      object ReportarIncapa: TCheckBox
        Left = 29
        Top = 112
        Width = 134
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Reportar incapacidades:'
        Checked = True
        State = cbChecked
        TabOrder = 1
      end
      object RGReportar: TRadioGroup
        Left = 150
        Top = 132
        Width = 265
        Height = 61
        Align = alCustom
        BiDiMode = bdLeftToRight
        Caption = 'En caso de existir varias incapacidades'
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Reportar la primera'
          'Reportar la mayor')
        ParentBiDiMode = False
        TabOrder = 2
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Top = 397
  end
end
