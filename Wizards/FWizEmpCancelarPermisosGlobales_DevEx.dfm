inherited WizEmpCancelarPermisosGlobales_DevEx: TWizEmpCancelarPermisosGlobales_DevEx
  Left = 289
  Top = 194
  Caption = 'Cancelar Permisos Globales'
  ClientHeight = 423
  ClientWidth = 434
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 434
    Height = 423
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que cancela los permisos a empleados que se les registro' +
        ' el proceso global.'
      Header.Title = 'Cancelar Permisos Globales'
      object FechaLBL: TLabel
        Left = 37
        Top = 21
        Width = 103
        Height = 13
        Alignment = taRightJustify
        Caption = '&Fecha de Referencia:'
        FocusControl = Fecha
        Transparent = True
      end
      object TipoFechaLBL: TLabel
        Left = 68
        Top = 47
        Width = 72
        Height = 13
        Alignment = taRightJustify
        Caption = '&Tipo de Fecha:'
        FocusControl = TipoFecha
        Transparent = True
      end
      object TipoMovimientoLBL: TLabel
        Left = 32
        Top = 73
        Width = 108
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cancelar &Modificaci'#243'n:'
        FocusControl = TipoMovimiento
        Transparent = True
      end
      object Fecha: TZetaFecha
        Left = 142
        Top = 16
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '02/ene/98'
        Valor = 35797.000000000000000000
      end
      object TipoFecha: TZetaKeyCombo
        Left = 142
        Top = 43
        Width = 170
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 1
        ListaFija = lfTipoKarFecha
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object TipoMovimiento: TZetaKeyCombo
        Left = 142
        Top = 69
        Width = 170
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 2
        ListaFija = lfCancelaMod
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object GroupBox1: TcxGroupBox
        Left = 0
        Top = 95
        Caption = 'Filtrar por'
        TabOrder = 3
        Height = 114
        Width = 409
        object PM_CLASIFI: TZetaKeyCombo
          Left = 103
          Top = 17
          Width = 170
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 1
          ListaFija = lfTipoPermiso
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object PM_TIPO: TZetaKeyLookup_DevEx
          Left = 103
          Top = 41
          Width = 300
          Height = 21
          LookupDataset = dmTablas.cdsIncidencias
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 3
          TabStop = True
          WidthLlave = 60
        end
        object PorTipo: TcxCheckBox
          Left = 16
          Top = 42
          Caption = '            T&ipo:'
          Properties.Alignment = taRightJustify
          TabOrder = 2
          Transparent = True
          OnClick = CheckBoxClick
          Width = 84
        end
        object PorReferencia: TcxCheckBox
          Left = 8
          Top = 64
          Caption = '    &Referencia: '
          Properties.Alignment = taRightJustify
          TabOrder = 4
          Transparent = True
          OnClick = CheckBoxClick
          Width = 92
        end
        object PorClase: TcxCheckBox
          Left = 18
          Top = 18
          Caption = '          C&lase:'
          Properties.Alignment = taRightJustify
          TabOrder = 0
          Transparent = True
          OnClick = CheckBoxClick
          Width = 82
        end
        object PM_NUMERO: TEdit
          Left = 103
          Top = 65
          Width = 130
          Height = 21
          MaxLength = 8
          TabOrder = 5
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 188
        Width = 412
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 412
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 344
          AnchorX = 239
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 6
        Top = 137
      end
      inherited sFiltroLBL: TLabel
        Left = 31
        Top = 160
      end
      inherited Seleccionar: TcxButton
        Left = 134
        Top = 254
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 58
        Top = 134
      end
      inherited sFiltro: TcxMemo
        Left = 58
        Top = 160
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 58
        Top = 4
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 352
    Top = 36
  end
end
