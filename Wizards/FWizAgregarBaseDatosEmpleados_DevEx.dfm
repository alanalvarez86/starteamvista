inherited WizAgregarBaseDatosEmpleados_DevEx: TWizAgregarBaseDatosEmpleados_DevEx
  Left = 986
  Top = 231
  Caption = 'Agregar Base de Datos de tipo Tress'
  ClientHeight = 484
  ClientWidth = 514
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 514
    Height = 484
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que te permite agregar una base de datos de tipo Tress d' +
        'esde cero o bas'#225'ndose en una existente.'
      Header.Title = 'Agregar base de datos de tipo Tress'
      inherited lblSeleccionarBD: TLabel
        Left = 113
        Top = 188
      end
      inherited lbNombreBD: TLabel
        Left = 106
        Top = 124
      end
      inherited cbBasesDatos: TComboBox
        Left = 248
        Top = 184
      end
      inherited txtBaseDatos: TEdit
        Left = 248
        Top = 120
      end
      inherited rbBDNueva: TcxRadioButton
        Left = 80
        Top = 101
      end
      inherited rbBDExistente: TcxRadioButton
        Left = 80
        Top = 165
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 143
        Width = 492
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 492
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 424
          AnchorY = 36
        end
      end
      inherited GrupoAvance: TcxGroupBox
        Top = 208
        Height = 136
        Width = 492
        inherited cxMemoStatus: TcxMemo
          Left = 265
          Height = 111
          Width = 224
        end
        inherited cxMemoPasos: TcxMemo
          Height = 111
          Width = 262
        end
      end
    end
    inherited NuevaBD2: TdxWizardControlPage
      PageVisible = False
      inherited lblUbicacion: TLabel
        Left = 73
        Top = 82
      end
      inherited GBUsuariosMSSQL: TcxGroupBox
        Left = 72
        Top = 104
        inherited lblClave: TLabel
          Left = 104
        end
        inherited lblUsuario: TLabel
          Left = 96
        end
        inherited txtUsuario: TEdit
          Left = 136
        end
        inherited txtClave: TMaskEdit
          Left = 136
        end
      end
      inherited lblDatoUbicacion: TcxLabel
        Left = 128
        Top = 80
      end
    end
    inherited Estructura: TdxWizardControlPage
      inherited lblDescripcion: TLabel
        Left = 98
        Top = 148
      end
      inherited lblCodigo: TLabel
        Left = 121
        Top = 124
      end
      inherited txtDescripcion: TZetaEdit
        Left = 160
        Top = 144
      end
      inherited txtCodigo: TZetaEdit
        Left = 160
        Top = 120
        OnExit = txtCodigoExit
      end
    end
    inherited NuevaBD1: TdxWizardControlPage
      Header.Description = 
        'Seleccione los par'#225'metros de cantidad de empleados y de document' +
        'os digitales.'
      Header.Title = 'Cantidad de empleados y documentos digitales'
      inherited lblCantidadEmpleados: TLabel
        Left = 108
        Top = 90
        Width = 55
        Caption = 'Empleados:'
      end
      inherited lblCantEmpSeleccion: TLabel
        Left = 383
        Top = 88
      end
      inherited lblDocumentos: TLabel
        Left = 60
        Top = 136
      end
      inherited lblDocSeleccion: TLabel
        Left = 383
        Top = 134
      end
      inherited lblBaseDatos: TLabel
        Left = 90
        Top = 200
      end
      inherited lblTamanoSugerido: TLabel
        Left = 78
        Top = 216
      end
      inherited lblLogTransacciones: TLabel
        Left = 58
        Top = 232
      end
      inherited lblBaseDatosValor: TLabel
        Left = 176
        Top = 200
      end
      inherited lblTamanoSugeridoValor: TLabel
        Left = 176
        Top = 216
      end
      inherited lblLogTransaccionesValor: TLabel
        Left = 176
        Top = 232
      end
      inherited tbEmpleados: TcxTrackBar
        Left = 166
        Top = 74
        Style.IsFontAssigned = True
      end
      inherited tbDocumentos: TcxTrackBar
        Left = 166
        Top = 120
      end
    end
    object DefinirEstructura: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 
        ' Indique de donde se tomar'#225'n los datos iniciales para crear la b' +
        'ase de datos.'
      Header.Title = 'Datos iniciales '
      object cbBasesDatosBase: TZetaKeyCombo
        Left = 108
        Top = 152
        Width = 257
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        Enabled = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 0
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object rbValoresDefault: TcxRadioButton
        Left = 88
        Top = 109
        Width = 201
        Height = 17
        Caption = 'Valores default de Sistema TRESS'
        Checked = True
        TabOrder = 1
        TabStop = True
        OnClick = rbValoresDefaultClick
        Transparent = True
      end
      object rbOtraBaseDatos: TcxRadioButton
        Left = 88
        Top = 130
        Width = 361
        Height = 17
        Caption = 
          'Iniciar con informaci'#243'n de una base de datos (de tipo Tress) exi' +
          'stente'
        TabOrder = 2
        OnClick = rbOtraBaseDatosClick
        Transparent = True
      end
      object chbEspeciales: TcxCheckBox
        Left = 88
        Top = 176
        Caption = 'Aplicar script de programaci'#243'n especial'
        TabOrder = 3
        Transparent = True
        Width = 217
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
  end
end
