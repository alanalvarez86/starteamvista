inherited SistImportarEnrolamientoMasivoGridShow_DevEx: TSistImportarEnrolamientoMasivoGridShow_DevEx
  Left = 189
  Top = 216
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Lista de Errores Detectados'
  ClientWidth = 900
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 900
    inherited OK_DevEx: TcxButton
      Left = 733
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 812
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 900
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object RENGLON: TcxGridDBColumn
        Caption = 'Rengl'#243'n'
        DataBinding.FieldName = 'RENGLON'
        MinWidth = 65
        Width = 65
      end
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
        MinWidth = 115
        Width = 115
      end
      object US_CORTO: TcxGridDBColumn
        Caption = 'Usuario'
        DataBinding.FieldName = 'US_CORTO'
        MinWidth = 86
        Width = 86
      end
      object US_DOMAIN: TcxGridDBColumn
        Caption = 'Usuario del dominio'
        DataBinding.FieldName = 'US_DOMAIN'
        MinWidth = 118
        Width = 118
      end
      object US_JEFE: TcxGridDBColumn
        Caption = 'Reporta a'
        DataBinding.FieldName = 'US_JEFE'
        MinWidth = 85
      end
      object GR_CODIGO: TcxGridDBColumn
        Caption = 'Grupo '
        DataBinding.FieldName = 'GR_CODIGO'
        MinWidth = 68
      end
      object US_ROLES: TcxGridDBColumn
        Caption = 'Roles'
        DataBinding.FieldName = 'US_ROLES'
        MinWidth = 66
        Width = 66
      end
      object NUM_ERROR: TcxGridDBColumn
        Caption = 'Errores'
        DataBinding.FieldName = 'NUM_ERROR'
        MinWidth = 80
        Width = 80
      end
      object DESC_ERROR: TcxGridDBColumn
        Caption = 'Descripci'#243'n de Errores'
        DataBinding.FieldName = 'DESC_ERROR'
        MinWidth = 135
        Width = 135
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DataSource: TDataSource
    Left = 112
    Top = 64
  end
end
