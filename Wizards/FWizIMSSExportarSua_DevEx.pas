unit FWizIMSSExportarSua_DevEx;

{$define SUA_14}

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs,  FileCtrl, ExtCtrls, Mask, StdCtrls, ComCtrls,
     ZetaNumero, ZetaKeyCombo, ZetaKeyLookup_DevEx,
     ZetaEdit, cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
     ZCXBaseWizardFiltro, ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo,
     cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
     dxCustomWizardControl, dxWizardControl;

type
  TWizIMSSExportarSUA_DevEx = class(TBaseCXWizardFiltro)
    EmpleadosGB: TcxGroupBox;
    ArchivoEmpleadosLBL: TLabel;
    BuscarArchivoEmpleado: TcxButton;
    lEmpleados: TCheckBox;
    ArchivoEmpleados: TEdit;
    lBorrarEmpleados: TCheckBox;
    lHomoclave: TCheckBox;
    MovimientosGB: TcxGroupBox;
    ArchivoMovimientosLBL: TLabel;
    BuscarArchivoMovimientos: TcxButton;
    lMovimientos: TCheckBox;
    ArchivoMovimientos: TEdit;
    OpenDialog: TOpenDialog;
    PeriodoGB: TcxGroupBox;
    Month: TZetaKeyCombo;
    Label6: TLabel;
    Label5: TLabel;
    Year: TZetaNumero;
    lEnviarFaltas: TCheckBox;
    PatronLbl: TLabel;
    Patron: TZetaKeyLookup_DevEx;
    lCuotaFija: TCheckBox;
    FormatoSUAGB: TcxGroupBox;
    Label2: TLabel;
    VersionSUA: TZetaKeyCombo;
    ZKBorrarMovimientos: TZetaKeyCombo;
    BorrarMovimientosLBL: TLabel;
    ArchivoDatosAfiliatoriosLBL: TLabel;
    ArchivoDatosAfiliatorios: TEdit;
    BuscarArchivoDatosAfiliatorios: TcxButton;
    ArchivoIncapacidadLBL: TLabel;
    ArchivoIncapacidad: TEdit;
    BuscarArchivoIncapacidad: TcxButton;
    ArchivoInfonavitLBL: TLabel;
    ArchivoInfonavit: TEdit;
    BuscarArchivoInfonavit: TcxButton;
    lEnviarSusFinBim: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BuscarArchivoEmpleadoClick(Sender: TObject);
    procedure BuscarArchivoMovimientosClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    //DevEx Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
    procedure lEmpleadosClick(Sender: TObject);
    procedure PatronValidLookup(Sender: TObject);
    procedure BuscarArchivoDatosAfiliatoriosClick(Sender: TObject);
    procedure BuscarArchivoInfonavitClick(Sender: TObject);
    procedure BuscarArchivoIncapacidadClick(Sender: TObject);
  private
    { Private declarations }
    FPatron: String;
    FRegistro: String;
    procedure LlenaVersionesSUA;
    procedure CargaPatron;
    procedure SetControls;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizIMSSExportarSUA_DevEx: TWizIMSSExportarSUA_DevEx;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaClientTools,
     ZetaDialogo,
     ZGlobalTress,
     DCatalogos,
     DProcesos,
     DGlobal,
     DCliente, ZcxWizardBasico;

{$R *.DFM}

procedure TWizIMSSExportarSUA_DevEx.FormCreate(Sender: TObject);
begin
     ArchivoEmpleados.Tag := K_GLOBAL_FILE_EMP_SUA;
     ArchivoMovimientos.Tag := K_GLOBAL_FILE_MOVS_SUA;
     ArchivoDatosAfiliatorios.Tag := K_GLOBAL_FILE_AFIL_SUA;
     ArchivoInfonavit.Tag := K_GLOBAL_FILE_INFO_SUA;
     ArchivoIncapacidad.Tag := K_GLOBAL_FILE_INCA_SUA;

{$ifdef SUA_14}
     lEnviarFaltas.Checked := TRUE;
     lCuotaFija.Visible := FALSE;
{$endif}
     lEnviarSusFinBim.Checked := FALSE;
     inherited;
     ParametrosControl := Year;
     SetControls;
     HelpContext := H40444_Exportar_SUA;
     Patron.LookupDataset := dmCatalogos.cdsRPatron;
     LlenaVersionesSUA;

     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;
end;

procedure TWizIMSSExportarSUA_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmCatalogos.cdsRPatron do
     begin
          Conectar;
          Locate( 'TB_CODIGO', dmCliente.IMSSPatron, [] );
     end;
     CargaPatron;
     with dmCliente do
     begin
          Year.Valor := IMSSYear;
          Month.Valor := IMSSMes;
          Patron.Llave := FPatron;
     end;
     VersionSUA.ItemIndex := High( dProcesos.aVersionSUA );    // Sugiere la versi�n mas reciente

     //DevEx
     Advertencia.Caption :=
        'Al ejecutar el proceso se generan los archivos que requiere la aplicaci�n SUA para realizar el c�lculo de los pagos mensuales y bimestrales.';
end;

procedure TWizIMSSExportarSUA_DevEx.LlenaVersionesSUA;
var
   i: Integer;
begin
     with VersionSUA.Items do
     begin
          Clear;
          for i := Low( dProcesos.aVersionSUA ) to High( dProcesos.aVersionSUA ) do
          begin
	       Add( dProcesos.aVersionSUA[i] );
          end;
      end;
end;

procedure TWizIMSSExportarSUA_DevEx.CargaPatron;
begin
     with dmCatalogos.cdsRPatron do
     begin
          FPatron := FieldByName( 'TB_CODIGO' ).AsString;
          FRegistro := FieldByName( 'TB_NUMREG' ).AsString;
     end;
end;

procedure TWizIMSSExportarSUA_DevEx.SetControls;
var
   lEnabled: Boolean;
begin
     // Exportar Empleados
     lEnabled := lEmpleados.Checked;
     ArchivoEmpleadosLBL.Enabled := lEnabled;
     ArchivoEmpleados.Enabled := lEnabled;
     ArchivoDatosAfiliatoriosLBL.Enabled := lEnabled;
     ArchivoDatosAfiliatorios.Enabled := lEnabled;

     BuscarArchivoEmpleado.Enabled := lEnabled;
     lBorrarEmpleados.Enabled := lEnabled;
     lBorrarEmpleados.Checked := lEnabled and Global.GetGlobalBooleano( K_GLOBAL_DELETE_FILE_SUA );
     lHomoclave.Enabled := lEnabled;
     lCuotaFija.Enabled := lEnabled;
     // Exportar Movimientos
     lEnabled := lMovimientos.Checked;
     ArchivoInfonavitLBL.Enabled := lEnabled;
     ArchivoInfonavit.Enabled := lEnabled;

     ArchivoMovimientos.Enabled := lEnabled;
     ArchivoMovimientosLBL.Enabled := lEnabled;
     ArchivoIncapacidad.Enabled := lEnabled;
     ArchivoIncapacidadLBL.Enabled := lEnabled;
     BuscarArchivoMovimientos.Enabled := lEnabled;
     BorrarMovimientosLBL.Enabled := lEnabled;
     ZKBorrarMovimientos.Enabled := lEnabled;
     ZKBorrarMovimientos.ItemIndex := BoolToInt( lEnabled and Global.GetGlobalBooleano( K_GLOBAL_DELETE_FILE_SUA ) );
     lEnviarFaltas.Enabled := lEnabled;
     lEnviarSusFinBim.Enabled:= lEnabled;
end;

procedure TWizIMSSExportarSUA_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);

function ValidaArchivo( Control: TEdit; const sNombre: String ): Boolean;
var
   sArchivo, sPath: String;
begin
     sArchivo := Control.Text;
     Result := False;
     if ( ZetaCommonTools.StrVacio( sArchivo ) ) then
     begin
          ZetaDialogo.ZError( Caption, 'El Archivo de ' + sNombre + ' no puede quedar vac�o', 0 );
          ActiveControl := Control;
     end
     else
     begin
          sPath := ExtractFilePath( sArchivo );
          if ZetaCommonTools.StrLleno( sPath ) and not DirectoryExists( sPath ) then
          begin
               ZetaDialogo.ZError( Caption, 'El directorio del archivo de ' + sNombre + ' NO existe ', 0 );
               ActiveControl := Control;
          end
          else
              if FileExists( sArchivo ) and not ZetaDialogo.zConfirm( Caption,
                                                                      'El archivo de ' + sNombre + ' YA existe' +
                                                                      CR_LF +
                                                                      sArchivo +
                                                                      CR_LF +
                                                                      '� Desea reemplazarlo ?', 0, mbOk ) then
                 ActiveControl := Control
              else
                  Result := True;
     end;
end;

begin
     ParametrosControl := nil;  //DevEx
     inherited;
     with Wizard do
     begin
          if CanMove and EsPaginaActual( Parametros ) and Adelante then
          begin
               if not lEmpleados.Checked and not lMovimientos.Checked then
               begin
                    CanMove := Error( 'No est� seleccionado para enviar nada', lEmpleados );
               end
               else
               begin
                    if CanMove and ( not lBorrarEmpleados.Checked ) and
                       ( eBorrarMovimSUA( ZKBorrarMovimientos.ItemIndex ) = bsTodo ) then
                       CanMove := Error( 'Si no se borran empleados, no se pueden borrar todos los movimientos.' + CR_LF +
                              'Quedar�a incompleto el historial de movimientos en SUA', ZKBorrarMovimientos );
                    if CanMove and lEmpleados.Checked then
                       CanMove := ValidaArchivo( ArchivoEmpleados, 'Empleados' );
                    if CanMove and lEmpleados.Checked then
                       CanMove := ValidaArchivo( ArchivoDatosAfiliatorios, 'Datos Afiliatorios' );
                    if CanMove and lMovimientos.Checked then
                       CanMove := ValidaArchivo( ArchivoInfonavit, 'Infonavit' );
                    if CanMove and lMovimientos.Checked then
                       CanMove := ValidaArchivo( ArchivoMovimientos, 'Movimientos' );
                    if CanMove and lMovimientos.Checked then
                       CanMove := ValidaArchivo( ArchivoIncapacidad, 'Incapacidades' );
               end;
          end;
     end;
end;

procedure TWizIMSSExportarSUA_DevEx.BuscarArchivoEmpleadoClick(Sender: TObject);
begin
     inherited;
     ArchivoEmpleados.Text := AbreDialogo( OpenDialog, ArchivoEmpleados.Text, 'sua' );
end;

procedure TWizIMSSExportarSUA_DevEx.BuscarArchivoMovimientosClick(Sender: TObject);
begin
     inherited;
     ArchivoMovimientos.Text := AbreDialogo( OpenDialog, ArchivoMovimientos.Text, 'sua' );
end;

procedure TWizIMSSExportarSUA_DevEx.lEmpleadosClick(Sender: TObject);
begin
     inherited;
     SetControls;
end;

procedure TWizIMSSExportarSUA_DevEx.PatronValidLookup(Sender: TObject);
begin
     inherited;
     CargaPatron;
end;

procedure TWizIMSSExportarSUA_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddString( 'Patron', FPatron );
          AddString( 'NumeroRegistro', FRegistro );
          AddInteger( 'Year', Year.ValorEntero );
          AddInteger( 'Mes', Month.Valor );
          AddString( 'ArchivoEmpleados', ArchivoEmpleados.Text );
          AddString( 'ArchivoDatosAfiliatorios', ArchivoDatosAfiliatorios.Text );
          AddString( 'ArchivoInfonavit', ArchivoInfonavit.Text );
          AddString( 'ArchivoMovimientos', ArchivoMovimientos.Text );
          AddString( 'ArchivoIncapacidad', ArchivoIncapacidad.Text );
          AddBoolean( 'EnviarEmpleados', lEmpleados.Checked );
          AddBoolean( 'BorrarEmpleados', lBorrarEmpleados.Checked );
          AddBoolean( 'IncluirHomoclave', lHomoclave.Checked );
          AddBoolean( 'EnviarCuotaFija', lCuotaFija.Checked );
          AddBoolean( 'EnviarMovimientos', lMovimientos.Checked );
          AddInteger( 'BorrarMovimientos', ZKBorrarMovimientos.ItemIndex );
          AddBoolean( 'EnviarFaltaBaja', lEnviarFaltas.Checked );
          AddInteger( 'VersionSUA', VersionSUA.ItemIndex );
          AddBoolean( 'ValidarCURPvsRFC', ( VersionSUA.ItemIndex = K_VERSION_SUA_2000 ) );
          AddBoolean( 'SuspenFinBim', lEnviarSusFinBim.Checked );
     end;

     //DevEx
     with Descripciones do
     begin
          //periodo
          AddString( 'Mes',  ObtieneElemento( lfMeses, Month.ItemIndex )  );
          AddInteger( 'A�o', Year.ValorEntero );
          AddString( 'Registro Patronal', FPatron + ': ' + FRegistro);

          //empleados
          AddBoolean( 'Enviar empleados', lEmpleados.Checked );
          AddString( 'Archivo empleados', ArchivoEmpleados.Text );
          AddString( 'Archivo datos afiliatorios', ArchivoDatosAfiliatorios.Text );
          AddBoolean( 'Borrar empleados en SUA', lBorrarEmpleados.Checked );
          AddBoolean( 'Inclu�r Homoclave', lHomoclave.Checked );
          AddBoolean( 'Enviar cuota fija', lCuotaFija.Checked );

          //movimientos
          AddBoolean( 'Enviar movimientos', lMovimientos.Checked );
          AddString( 'Archivo movimientos', ArchivoMovimientos.Text );
          AddString( 'Archivo incapacidad', ArchivoIncapacidad.Text );
          AddString( 'Archivo INFONAVIT', ArchivoInfonavit.Text );
          AddString( 'Borrar movimientos en SUA', ObtieneElemento( lfBorrarMovimSUA, ZKBorrarMovimientos.ItemIndex ) );
          AddBoolean( 'Mov. con fecha baja', lEnviarFaltas.Checked );
          AddBoolean( 'Suspen. al final bimestre', lEnviarSusFinBim.Checked );
     end;
end;

function TWizIMSSExportarSUA_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.ExportarSUA( ParameterList );
end;

procedure TWizIMSSExportarSUA_DevEx.BuscarArchivoDatosAfiliatoriosClick(
  Sender: TObject);
begin
    inherited;
    ArchivoDatosAfiliatorios.Text := AbreDialogo( OpenDialog, ArchivoDatosAfiliatorios.Text, 'sua' );
end;

procedure TWizIMSSExportarSUA_DevEx.BuscarArchivoInfonavitClick(Sender: TObject);
begin
     inherited;
     ArchivoInfonavit.Text := AbreDialogo( OpenDialog, ArchivoInfonavit.Text, 'sua' );
end;

procedure TWizIMSSExportarSUA_DevEx.BuscarArchivoIncapacidadClick(Sender: TObject);
begin
     inherited;
     ArchivoIncapacidad.Text := AbreDialogo( OpenDialog, ArchivoIncapacidad.Text, 'sua' );
end;



procedure TWizIMSSExportarSUA_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := Year
     else
         ParametrosControl := nil;
     inherited;
end;

end.



