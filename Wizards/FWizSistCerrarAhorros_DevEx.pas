unit FWizSistCerrarAhorros_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask, ZetaFecha, StdCtrls, ExtCtrls, ComCtrls,
  ZetaEdit, Buttons, ZetaWizard, ZCXBaseWizardFiltro,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, Menus, ZetaCXWizard, cxRadioGroup, cxTextEdit,
  cxMemo, ZetaKeyLookup_DevEx, cxButtons, dxGDIPlusClasses, cxImage,
  cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters;
                                                                                                 
type
  TWizSISTCerrarAhorros_DevEx = class(TBaseCXWizardFiltro)
    TipoAhorroLBL: TLabel;
    TipoAhorro: TZetaKeyLookup_DevEx;
    Operacion: TcxRadioGroup;
    lFecha: TCheckBox;
    FechaCierreLBL: TLabel;
    FechaCierre: TZetaFecha;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OperacionClick(Sender: TObject);
    procedure lFechaClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
    procedure SetControls;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizSISTCerrarAhorros_DevEx: TWizSISTCerrarAhorros_DevEx;

implementation

uses DProcesos,
     DTablas,
     DCliente,
     ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonClasses;

{$R *.DFM}

procedure TWizSISTCerrarAhorros_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := TipoAhorro;
     HelpContext := H33516_Cierre_de_ahorros;
     FechaCierre.Valor := dmCliente.FechaDefault;
     SetControls;
     TipoAhorro.LookupDataset := dmTablas.cdsTAhorro;
end;

procedure TWizSISTCerrarAhorros_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     FechaCierre.Valor := dmCliente.FechaDefault;
     dmTablas.cdsTAhorro.Conectar;
end;

procedure TWizSISTCerrarAhorros_DevEx.SetControls;
begin
     with lFecha do
     begin
          Enabled := ( Operacion.ItemIndex < 2 );
          FechaCierre.Enabled := Enabled and Checked;
          FechaCierreLBL.Enabled := FechaCierre.Enabled;
     end;
end;

procedure TWizSISTCerrarAhorros_DevEx.OperacionClick(Sender: TObject);
begin
     inherited;
     SetControls;
end;

procedure TWizSISTCerrarAhorros_DevEx.lFechaClick(Sender: TObject);
begin
     inherited;
     SetControls;
end;

procedure TWizSISTCerrarAhorros_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
ParametrosControl := nil;
     inherited;
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros ) then
          begin
               if StrVacio( TipoAhorro.Llave ) then
               begin
                    ZetaDialogo.zError( Caption, '� Tipo De Ahorro No Fu� Especificado !', 0 );
                    ActiveControl := TipoAhorro;
                    CanMove := False;
               end;
          end;
     end;
end;

procedure TWizSISTCerrarAhorros_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddString( 'TipoAhorro', TipoAhorro.Llave );
          AddInteger( 'Operacion', Operacion.ItemIndex );
          AddBoolean( 'PonerFechaCierre', lFecha.Checked );
          AddDate( 'FechaCierre', FechaCierre.Valor );
     end;
        with Descripciones do
     begin
          AddString( 'Tipo de Ahorro', TipoAhorro.Llave );
          AddString( 'Operaci�n', Operacion.Properties.Items[Operacion.ItemIndex].caption );
          AddBoolean( 'Poner Fecha de Cierre', lFecha.Checked );
          if(lFecha.Checked ) then
          AddDate( 'Fecha de Cierre', FechaCierre.Valor );
     end;
end;

function TWizSISTCerrarAhorros_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CerrarAhorros( ParameterList );
end;

procedure TWizSISTCerrarAhorros_DevEx.WizardAfterMove(Sender: TObject);
begin
  inherited;
  if Wizard.EsPaginaActual(Parametros) then
      ParametrosControl := TipoAhorro;
end;

end.



