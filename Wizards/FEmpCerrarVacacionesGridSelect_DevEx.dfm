inherited EmpCierreVacacionesGridSelect_DevEx: TEmpCierreVacacionesGridSelect_DevEx
  Left = 193
  Top = 312
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Seleccione los Registros de Cierre de Vacaciones a Agregar'
  ClientHeight = 301
  ClientWidth = 577
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 265
    Width = 577
    inherited OK_DevEx: TcxButton
      Left = 410
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 488
    end
  end
  inherited PanelSuperior: TPanel
    Width = 577
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 577
    Height = 230
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
      end
      object FECHA_CIERRE: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'FECHA_CIERRE'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
      end
      object D_GOZO: TcxGridDBColumn
        Caption = 'Gozo'
        DataBinding.FieldName = 'D_GOZO'
      end
      object D_PAGO: TcxGridDBColumn
        Caption = 'Pago'
        DataBinding.FieldName = 'D_PAGO'
      end
      object D_PRIMA: TcxGridDBColumn
        Caption = 'Prima vac.'
        DataBinding.FieldName = 'D_PRIMA'
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
