unit FEmpImportarPCarAboGridShow_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, ExtCtrls, ZBaseGridShow_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons, cxCalendar;

type
  TEmpImportarPCarAboGridShow_DevEx = class(TBaseGridShow_DevEx)
    RENGLON: TcxGridDBColumn;
    CB_CODIGO: TcxGridDBColumn;
    PR_TIPO: TcxGridDBColumn;
    PR_REFEREN: TcxGridDBColumn;
    CR_FECHA: TcxGridDBColumn;
    TIPO_MOV: TcxGridDBColumn;
    MONTO: TcxGridDBColumn;
    NUM_ERROR: TcxGridDBColumn;
    DESC_ERROR: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpImportarPCarAboGridShow_DevEx: TEmpImportarPCarAboGridShow_DevEx;

implementation

{$R *.DFM}

procedure TEmpImportarPCarAboGridShow_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     DataSet.MaskPesos( 'MONTO' );
end;

end.
