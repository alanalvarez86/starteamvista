unit FWizNomCreditoAplicado_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ZetaEdit, StdCtrls, ExtCtrls, ZetaKeyCombo,
  Mask, ZetaNumero, FWizNomBase_DevEx,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
  cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, ZetaDBTextBox,
  dxCustomWizardControl, dxWizardControl, cxCheckBox;

type
  TWizNomCreditoAplicado_DevEx = class(TWizNomBase_DevEx)
    iYearLBL: TLabel;
    zcbMes: TZetaKeyCombo;
    iMesLBL: TLabel;
    ConceptoLBL: TLabel;
    zklConcepto: TZetaKeyLookup_DevEx;
    mIngresoBruto: TcxMemo;
    mIngresoExento: TcxMemo;
    mDiasTrab: TcxMemo;
    zmFactorMensual: TZetaNumero;
    zklTabla: TZetaKeyLookup_DevEx;
    btnIngresoBruto: TcxButton;
    btnIngresoExento: TcxButton;
    btnDiasTrab: TcxButton;
    ckRastrear: TCheckBox;
    zmYear: TZetaNumero;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    //DevEx Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
    procedure btnIngresoBrutoClick(Sender: TObject);
    procedure btnIngresoExentoClick(Sender: TObject);
    procedure btnDiasTrabClick(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaParametros;override;
    procedure CargaListaVerificacion; override;
  public
    { Public declarations }
  end;

var
  WizNomCreditoAplicado_DevEx: TWizNomCreditoAplicado_DevEx;

implementation
uses DCliente,
     DProcesos,
     DCatalogos,
     DTablas,
     ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaTipoEntidad,
     ZGlobalTress,
     ZBaseSelectGrid_DevEx,
     ZConstruyeFormula,
     FNomCredAplicadoGridSelect_DevEx, ZcxWizardBasico;

{$R *.DFM}

procedure TWizNomCreditoAplicado_DevEx.FormCreate(Sender: TObject);
const
     K_FACTOR_MENSUAL = 30.4; //esta constante debe estar en ZetaCommonClasses por que esta repetida en DCalculoNomina

begin
     zklConcepto.Tag := K_GLOBAL_DEF_NOM_CREDITO_AP_CONCEPTO;
     mIngresoBruto.Tag := K_GLOBAL_DEF_NOM_CREDITO_AP_INGRESO_BRUTO;
     mIngresoExento.Tag := K_GLOBAL_DEF_NOM_CREDITO_AP_INGRESO_EXENTO;
     mDiasTrab.Tag := K_GLOBAL_DEF_NOM_CREDITO_AP_DIAS;
     zmFactorMensual.Tag := K_GLOBAL_DEF_NOM_CREDITO_AP_FACTOR_MENSUAL;
     zklTabla.Tag := K_GLOBAL_DEF_NOM_CREDITO_AP_TABLA;
     //ckRastrear.Tag := K_GLOBAL_DEF_NOM_CREDITO_AP_RASTREAR;
     inherited;
     with dmCliente.GetDatosPeriodoActivo do
     begin
          zmYear.Valor := Year;
          zcbMes.Valor := ( Mes - 1 );
     end;
     zmFactorMensual.Valor := K_FACTOR_MENSUAL;
     ParametrosControl := zmYear;
     HelpContext := H30344_Credito_aplicado_mensual;
     zklConcepto.LookupDataset := dmCatalogos.cdsConceptos;
     zklTabla.LookupDataset := dmTablas.cdsNumericas;

     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;
end;

procedure TWizNomCreditoAplicado_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     dmCatalogos.cdsConceptos.Conectar;
     zklConcepto.Valor := zklConcepto.Valor;
     dmTablas.cdsNumericas.Conectar;
     zklTabla.Valor := zklTabla.Valor;
     ckRastrear.Checked := False;
     if ( dmCliente.YearDefault >= ZetaCommonClasses.K_REFORMA_FISCAL_2008 ) then
     begin
        self.Caption := 'C�lculo del SUBE Aplicado Mensual';
        Parametros.Header.Title := 'C�lculo del SUBE Aplicado Mensual'; //DevEx
     end
     else
     begin
         self.Caption := 'C�lculo del Cr�dito Aplicado Mensual';
         Parametros.Header.Title := 'C�lculo del Cr�dito Aplicado Mensual'; //DevEx
     end;
     Advertencia.Caption :=
          'Al aplicar el proceso calcula el cr�dito aplicado de un mes con base en las n�minas afectadas del mes que se indic�.';
end;

procedure TWizNomCreditoAplicado_DevEx.CargaParametros;
begin
     inherited;

     with ParameterList do
     begin
          AddInteger( 'Year', zmYear.ValorEntero );
          AddInteger( 'Mes', zcbMes.ItemIndex + GetOffset( lfMeses ) );
          AddInteger( 'Concepto', zklConcepto.Valor );
          AddInteger( 'Tabla', zklTabla.Valor );
          AddString( 'DescConcepto', zklConcepto.Descripcion );
          AddString( 'DescTabla', zklTabla.Descripcion );
          AddString( 'IngresoBruto', mIngresoBruto.Text );
          AddString( 'IngresoExento', mIngresoExento.Text );
          AddString( 'Dias', mDiasTrab.Text );
          AddString( 'Factor', zmFactorMensual.ValorAsText );
          AddBoolean( 'Rastreo', ckRastrear.Checked );
     end;

     with Descripciones do
     begin
          AddInteger( 'A�o', zmYear.ValorEntero );
          AddString( 'Mes', ObtieneElemento( lfMeses, zcbMes.ItemIndex ) );
          AddString( 'Concepto', GetDescripLlave( zklConcepto ) );
          AddString( 'Ingreso bruto', mIngresoBruto.Text );
          AddString( 'Ingreso exento', mIngresoExento.Text );
          AddString( 'D�as trabajados', mDiasTrab.Text );
          AddString( 'Factor mensual', zmFactorMensual.ValorAsText );
          AddString( 'Tabla', GetDescripLlave( zklTabla ) );
          AddString( 'Rastrear c�lculos', BoolToSiNo( ckRastrear.Checked ) );
     end;
end;

function TWizNomCreditoAplicado_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TNomCredAplicadoGridSelect_DevEx );
end;

procedure TWizNomCreditoAplicado_DevEx.CargaListaVerificacion;
begin
     dmProcesos.CreditoAplicadoGetLista( ParameterList );
end;

function TWizNomCreditoAplicado_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CreditoAplicado(ParameterList, Verificacion);
end;

procedure TWizNomCreditoAplicado_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     ParametrosControl := nil;
     inherited;
     if CanMove then
     begin
          if Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
          begin
               if ( zmYear.ValorEntero = 0) then
               begin
                    zError( Caption, 'El a�o no puede ser igual a cero', 0 );
                    ActiveControl := zmYear;
                    CanMove := False;
               end
               else if strVacio( zklconcepto.Llave ) then
               begin
                    zError( Caption, 'Debe especificarse C�digo del Concepto', 0 );
                    ActiveControl := zklconcepto;
                    CanMove := False;
               end
               else if strVacio( mIngresoBruto.Text ) then
               begin
                    zError( Caption, 'Debe especificarse F�rmula de Ingreso Bruto', 0 );
                    ActiveControl := mIngresoBruto;
                    CanMove := False;
               end
               else if strVacio( mIngresoExento.Text ) then
               begin
                    zError( Caption, 'Debe especificarse F�rmula de Ingreso Exento', 0 );
                    ActiveControl := mIngresoExento;
                    CanMove := False;
               end
               else if strVacio( mDiasTrab.Text ) then
               begin
                    zError( Caption, 'Debe especificarse F�rmula de D�as Trabajados', 0 );
                    ActiveControl := mDiasTrab;
                    CanMove := False;
               end
               else if ( Trim( mDiasTrab.Text ) = '0' ) then
               begin
                    zError( Caption, 'D�as Trabajados no puede ser igual a cero', 0 );
                    ActiveControl := mDiasTrab;
                    CanMove := False;
               end
               else if strVacio( zklTabla.Llave ) then
               begin
                    zError( Caption, 'Debe especificarse C�digo de la tabla', 0 );
                    ActiveControl := zklTabla;
                    CanMove := False;
               end
               else if ( zmFactorMensual.Valor = 0 ) then
               begin
                    zError( Caption, 'Factor Mensual no puede ser igual a cero', 0 );
                    ActiveControl := zmFactorMensual;
                    CanMove := False;
               end;
          end;
     end;
end;

procedure TWizNomCreditoAplicado_DevEx.btnIngresoBrutoClick(Sender: TObject);
begin
     mIngresoBruto.Text:= GetFormulaConst( enEmpleado, mIngresoBruto.Text, mIngresoBruto.SelStart, evBase );
end;

procedure TWizNomCreditoAplicado_DevEx.btnIngresoExentoClick(Sender: TObject);
begin
     mIngresoExento.Text:= GetFormulaConst( enEmpleado, mIngresoExento.Text, mIngresoExento.SelStart, evBase );
end;

procedure TWizNomCreditoAplicado_DevEx.btnDiasTrabClick(Sender: TObject);
begin
     mDiasTrab.Text:= GetFormulaConst( enEmpleado, mDiasTrab.Text, mDiasTrab.SelStart, evBase );
end;


procedure TWizNomCreditoAplicado_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
          ParametrosControl := zmYear
     else
         ParametrosControl := nil;
     inherited;
end;

end.



