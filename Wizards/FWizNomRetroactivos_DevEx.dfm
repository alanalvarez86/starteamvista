inherited WizNomRetroactivos_DevEx: TWizNomRetroactivos_DevEx
  Left = 664
  Top = 205
  Caption = 'Calcular Retroactivos'
  ClientHeight = 533
  ClientWidth = 467
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 467
    Height = 533
    inherited Parametros: TdxWizardControlPage
      Header.Description = 'Seleccione los par'#225'metros del concepto retroactivo.'
      Header.Title = 'Pago de Retroactivo '
      inherited GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 445
        Height = 273
        Align = alCustom
        Caption = ''
        inherited PeriodoTipoLbl: TLabel
          Left = 52
        end
        inherited iTipoNomina: TZetaTextBox
          Left = 81
        end
        inherited iNumeroNomina: TZetaTextBox
          Left = 81
        end
        inherited iMesNomina: TZetaTextBox
          Left = 81
        end
        inherited sDescripcion: TZetaTextBox
          Left = 155
        end
        inherited FechaInicial: TZetaTextBox
          Left = 81
        end
        inherited FechaFinal: TZetaTextBox
          Left = 81
        end
        inherited sStatusNomina: TZetaTextBox
          Left = 81
        end
        inherited Label4: TLabel
          Left = 43
        end
        inherited Label2: TLabel
          Left = 64
        end
        inherited Label3: TLabel
          Left = 53
        end
        inherited PeriodoNumeroLBL: TLabel
          Left = 36
        end
        inherited Label1: TLabel
          Left = 57
        end
        object Label5: TLabel
          Left = 27
          Top = 174
          Width = 49
          Height = 13
          Alignment = taRightJustify
          Caption = 'Concepto:'
        end
        object Label6: TLabel
          Left = 22
          Top = 202
          Width = 127
          Height = 13
          Caption = 'Si Ya Existe La Excepci'#243'n:'
        end
        object lkpConcepto: TZetaKeyLookup_DevEx
          Left = 81
          Top = 170
          Width = 322
          Height = 21
          Filtro = 'CO_NUMERO < 1000'
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 70
        end
        object cbExisteConcepto: TZetaKeyCombo
          Left = 153
          Top = 198
          Width = 136
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 1
          Items.Strings = (
            'Sustituir'
            'Sumar')
          ListaFija = lfNinguna
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 298
        Width = 445
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 445
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 377
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 21
        Top = 191
      end
      inherited sFiltroLBL: TLabel
        Left = 46
        Top = 220
      end
      inherited Seleccionar: TcxButton
        Left = 149
        Top = 308
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 73
        Top = 188
      end
      inherited sFiltro: TcxMemo
        Left = 73
        Top = 219
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 73
        Top = 58
      end
      inherited bAjusteISR: TcxButton
        Left = 403
        Top = 220
      end
    end
    object tsCalculaRetroactivos: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 
        'Proceso que es utilizado para pagar en la n'#243'mina actual la aplic' +
        'aci'#243'n del nuevo salario a trav'#233's de un rango de periodos de n'#243'mi' +
        'na.'
      Header.Title = 'Calcular retroactivos'
      object lblNuevoSalario: TLabel
        Left = 29
        Top = 21
        Width = 120
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha del Nuevo Salario:'
        Transparent = True
      end
      object zfNuevoSalario: TZetaFecha
        Left = 154
        Top = 16
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '08/may/03'
        Valor = 37749.000000000000000000
      end
      object GroupBox2: TcxGroupBox
        Left = 6
        Top = 56
        Caption = ' Rango de Periodos a Calcular '
        TabOrder = 1
        Height = 265
        Width = 435
        object lblAnio: TLabel
          Left = 124
          Top = 23
          Width = 22
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'o:'
          Transparent = True
        end
        object lblDesde: TLabel
          Left = 60
          Top = 50
          Width = 86
          Height = 13
          Alignment = taRightJustify
          Caption = 'Desde Catorcena:'
          Transparent = True
        end
        object lblHasta: TLabel
          Left = 63
          Top = 78
          Width = 83
          Height = 13
          Alignment = taRightJustify
          Caption = 'Hasta Catorcena:'
          Transparent = True
        end
        object LkpDesdeSem: TZetaKeyLookup_DevEx
          Left = 151
          Top = 46
          Width = 247
          Height = 21
          Filtro = 'PE_STATUS >= 4'
          LookupDataset = dmCatalogos.cdsPeriodo
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 60
        end
        object lkpHastaSem: TZetaKeyLookup_DevEx
          Left = 151
          Top = 74
          Width = 248
          Height = 21
          Filtro = 'PE_STATUS >= 4'
          LookupDataset = dmCatalogos.cdsPeriodo
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 2
          TabStop = True
          WidthLlave = 60
        end
        object znAnio: TZetaNumero
          Left = 151
          Top = 18
          Width = 60
          Height = 21
          Mascara = mnDias
          MaxLength = 4
          TabOrder = 0
          Text = '0'
          OnExit = znAnioExit
        end
        object GroupBox3: TcxGroupBox
          Left = 8
          Top = 120
          Caption = '                                        '
          TabOrder = 3
          Height = 121
          Width = 417
          object lblAnioAd: TLabel
            Left = 116
            Top = 22
            Width = 22
            Height = 13
            Alignment = taRightJustify
            Caption = 'A'#241'o:'
            Transparent = True
          end
          object lblDesdeAd: TLabel
            Left = 52
            Top = 49
            Width = 86
            Height = 13
            Alignment = taRightJustify
            Caption = 'Desde Catorcena:'
            Transparent = True
          end
          object lblHastaAd: TLabel
            Left = 55
            Top = 78
            Width = 83
            Height = 13
            Alignment = taRightJustify
            Caption = 'Hasta Catorcena:'
            Transparent = True
          end
          object chbIncluirAnio: TCheckBox
            Left = 9
            Top = -1
            Width = 118
            Height = 17
            Caption = 'Incluir A'#241'o adicional'
            TabOrder = 0
            OnClick = chbIncluirAnioClick
          end
          object znAnioAd: TZetaNumero
            Left = 144
            Top = 17
            Width = 60
            Height = 21
            Mascara = mnDias
            MaxLength = 4
            TabOrder = 1
            Text = '0'
            OnExit = znAnioAdExit
          end
          object LkpDesdeSemAd: TZetaKeyLookup_DevEx
            Left = 144
            Top = 45
            Width = 247
            Height = 21
            Hint = 'Buscar un Periodo de N'#243'mina'
            Filtro = 'PE_STATUS >= 4'
            LookupDataset = dmCatalogos.cdsPeriodoAd
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 2
            TabStop = True
            WidthLlave = 60
          end
          object lkpHastaSemAd: TZetaKeyLookup_DevEx
            Left = 144
            Top = 74
            Width = 248
            Height = 21
            Hint = 'Buscar un Periodo de N'#243'mina'
            Filtro = 'PE_STATUS >= 4'
            LookupDataset = dmCatalogos.cdsPeriodoAd
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 3
            TabStop = True
            WidthLlave = 60
          end
        end
      end
    end
    object tsConceptos: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 'Seleccione los par'#225'metros de Conceptos.'
      Header.Title = 'Conceptos a considerar'
      object ListaConceptos: TcxCheckListBox
        Left = 34
        Top = 30
        Width = 377
        Height = 329
        Align = alCustom
        EditValueFormat = cvfStatesString
        Items = <>
        TabOrder = 0
      end
    end
  end
  object edGlobalRetroactivo: TEdit [1]
    Left = 152
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 1
    Visible = False
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 384
    Top = 42
  end
end
