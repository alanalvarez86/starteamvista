inherited WizAgregarBaseDatosSeleccion_DevEx: TWizAgregarBaseDatosSeleccion_DevEx
  Left = 385
  Top = 243
  Caption = 'Agregar BD de tipo Selecci'#243'n'
  ClientHeight = 576
  ClientWidth = 476
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 476
    Height = 576
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que le permite agregar una base de datos de tipo Selecci' +
        #243'n con la opci'#243'n para crearla o tomando una existente.'
      Header.Title = 'Agregar base de datos de tipo Selecci'#243'n'
      inherited lblSeleccionarBD: TLabel
        Left = 88
        Top = 178
      end
      inherited lbNombreBD: TLabel
        Left = 81
        Top = 106
      end
      inherited cbBasesDatos: TComboBox
        Left = 223
        Top = 174
      end
      inherited txtBaseDatos: TEdit
        Left = 223
        Top = 102
      end
      inherited rbBDNueva: TcxRadioButton
        Left = 55
        Top = 75
      end
      inherited rbBDExistente: TcxRadioButton
        Left = 55
        Top = 155
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 237
        Width = 454
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 454
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 386
          AnchorY = 36
        end
      end
      inherited GrupoAvance: TcxGroupBox
        Top = 302
        Height = 134
        Width = 454
        inherited cxMemoStatus: TcxMemo
          Left = 273
          Height = 109
          Width = 178
        end
        inherited cxMemoPasos: TcxMemo
          Height = 109
          Width = 270
        end
      end
    end
    inherited NuevaBD2: TdxWizardControlPage
      inherited lblUbicacion: TLabel
        Left = 48
        Top = 66
      end
      inherited GBUsuariosMSSQL: TcxGroupBox
        Left = 47
        Top = 99
      end
      inherited lblDatoUbicacion: TcxLabel
        Left = 103
        Top = 64
      end
    end
    inherited Estructura: TdxWizardControlPage
      inherited lblDescripcion: TLabel
        Left = 63
        Top = 120
      end
      inherited lblCodigo: TLabel
        Left = 86
        Top = 80
      end
      inherited txtDescripcion: TZetaEdit
        Left = 133
        Top = 116
      end
      inherited txtCodigo: TZetaEdit
        Left = 133
        Top = 76
      end
    end
    inherited NuevaBD1: TdxWizardControlPage
      inherited lblCantidadEmpleados: TLabel
        Left = 70
        Top = 53
      end
      inherited lblCantEmpSeleccion: TLabel
        Left = 364
        Top = 51
      end
      inherited lblDocumentos: TLabel
        Left = 23
        Top = 131
      end
      inherited lblDocSeleccion: TLabel
        Left = 364
        Top = 129
      end
      inherited lblBaseDatos: TLabel
        Left = 54
        Top = 203
      end
      inherited lblTamanoSugerido: TLabel
        Left = 42
        Top = 235
      end
      inherited lblLogTransacciones: TLabel
        Left = 22
        Top = 267
      end
      inherited lblBaseDatosValor: TLabel
        Left = 144
        Top = 203
      end
      inherited lblTamanoSugeridoValor: TLabel
        Left = 144
        Top = 235
      end
      inherited lblLogTransaccionesValor: TLabel
        Left = 144
        Top = 267
      end
      inherited tbEmpleados: TcxTrackBar
        Left = 144
        Top = 37
        Style.IsFontAssigned = True
      end
      inherited tbDocumentos: TcxTrackBar
        Left = 144
        Top = 115
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Top = 256
  end
end
