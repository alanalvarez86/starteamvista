unit FWizEmpSalarioIntegrado_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, ExtCtrls, Mask, ComCtrls,
     ZetaFecha, ZetaEdit, FWizEmpBaseFiltro, cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
      TressMorado2013, dxSkinsDefaultPainters,
     cxContainer, cxEdit, Menus,
     ZetaCXWizard, ZetaKeyLookup_DevEx, cxButtons, cxRadioGroup, cxTextEdit,
     cxMemo, cxGroupBox, cxLabel, dxGDIPlusClasses, cxImage,
     dxCustomWizardControl, dxWizardControl, cxCheckBox;

type
  TWizEmpSalarioIntegrado_DevEx = class(TWizEmpBaseFiltro)
    dReferencia: TZetaFecha;
    dReferenciaLBL: TLabel;
    RGFecha: TcxRadioGroup;
    RGIMSS: TcxRadioGroup;
    sDescripcion: TEdit;
    sDescripcionLBL: TLabel;
    sObservaLBL: TLabel;
    sObserva: TcxMemo;
    lIncapacitados: TcxCheckBox;
    dIMSS: TZetaFecha;
    procedure FormCreate(Sender: TObject);
    procedure RGIMSSClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    //Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizEmpSalarioIntegrado_DevEx: TWizEmpSalarioIntegrado_DevEx;

implementation

uses DCliente,
     DProcesos,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZBaseSelectGrid_DevEx,
     FEmpSalarioIntegradoGridSelect_DevEx, ZcxWizardBasico;

{$R *.DFM}

procedure TWizEmpSalarioIntegrado_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := dReferencia;
     dReferencia.Valor := dmCliente.FechaDefault;
     sDescripcion.Text := 'Rec�lculo de Salarios Integrados';
     RgFecha.ItemIndex := 0;
     RGIMSS.ItemIndex := 0;
     dIMSS.Enabled := false;
     dIMSS.Valor :=  dmCliente.FechaDefault;
     HelpContext := H10161_Recalcular_integrados;
end;

procedure TWizEmpSalarioIntegrado_DevEx.CargaParametros;
begin


     inherited CargaParametros;

     with ParameterList do
     begin
          AddDate( 'Fecha', dReferencia.Valor );
          AddString( 'Descripcion', sDescripcion.Text );
          AddMemo( 'Observaciones', sObserva.Text );
          AddBoolean( 'Referencia', ( RGFecha.ItemIndex = 0 ) );
          AddBoolean( 'NoIncluirIncapacitados', lIncapacitados.Checked );
          AddBoolean( 'AvisoIMSSFechaCambio', ( RGIMSS.ItemIndex = 0 ) );
          AddDate( 'Fecha2', dImss.Valor );
     end;

     with Descripciones do
     begin
          AddDate( 'Fecha de referencia', dReferencia.Valor );
          if ( RGFecha.ItemIndex = 0 ) then
             AddString( 'Fecha de cambio', 'Referencia' )
          else
              AddString( 'Fecha de cambio', 'Aniversario' );
          if (RGIMSS.ItemIndex = 0) then
             AddString( 'Fecha de aviso a IMSS', 'Misma fecha del cambio')
          else
              AddDate( 'Fecha de aviso a IMSS', dImss.Valor );
          AddString( 'Descripci�n', sDescripcion.Text );
          AddMemo( 'Observaciones', sObserva.Text );
          AddBoolean( 'No inclu�r incapacitados', lIncapacitados.Checked );

     end;
end;

procedure TWizEmpSalarioIntegrado_DevEx.CargaListaVerificacion;
begin
     dmProcesos.SalarioIntegradoGetLista( ParameterList );
end;

function TWizEmpSalarioIntegrado_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpSalarioIntegradoGridSelect_DevEx );
end;

function TWizEmpSalarioIntegrado_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.SalarioIntegrado( ParameterList, Verificacion );
end;



procedure TWizEmpSalarioIntegrado_DevEx.RGIMSSClick(Sender: TObject);
begin
     inherited;
     dIMSS.Enabled := RGIMSS.ItemIndex = 1;
end;


procedure TWizEmpSalarioIntegrado_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'El salario integrado de los empleados seleccionados '
                      + 'ser� actualizado, gener�ndose un movimiento de kardex '
                      + 'por cada actualizaci�n.';
end;

procedure TWizEmpSalarioIntegrado_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := dReferencia
     else
         ParametrosControl := nil;
     inherited;
end;
procedure TWizEmpSalarioIntegrado_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     ParametrosControl := nil;
end;

end.
