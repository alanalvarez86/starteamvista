unit FWizEmpVacaciones_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FWizEmpBaseFiltro, ZetaNumero, Spin, StdCtrls, Mask, ZetaFecha,
  {$ifdef TRESS_DELPHIXE5_UP}Data.DB,{$endif}
  ZetaEdit, Buttons, ComCtrls, {ZetaWizard,} ExtCtrls,
  ZetaCommonLists,
  ZetaKeyCombo, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, ZetaKeyLookup_DevEx, cxButtons, cxRadioGroup, cxTextEdit,
  cxMemo, cxGroupBox, cxLabel, dxGDIPlusClasses, cxImage,
  dxCustomWizardControl, dxWizardControl;

type
  TWizEmpVacaciones_DevEx = class(TWizEmpBaseFiltro)
    ParamVacaciones: TdxWizardControlPage;
    ParamSaldar: TdxWizardControlPage;
    RBProceso: TRadioGroup;
    GBDiasPrima: TGroupBox;
    lbSaldarPrima: TLabel;
    lSaldarPrima: TCheckBox;
    PrimaVaca: TZetaNumero;
    GBParametros: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    FechaInicial: TZetaFecha;
    FechaFinal: TZetaFecha;
    sPeriodo: TEdit;
    GBNominaVaca: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    TipoNomina: TZetaKeyCombo;
    NumeroNomina: TZetaKeyLookup_DevEx;
    YearNomina: TSpinEdit;
    FiltroTipoNomina: TCheckBox;
    GBDiasPagar: TGroupBox;
    lbSaldarPagar: TLabel;
    Label7: TLabel;
    lSaldarPagados: TCheckBox;
    Otros: TZetaNumero;
    Pagados: TZetaNumero;
    GBDiasGozados: TGroupBox;
    lbSaldarGozar: TLabel;
    lSaldarGozados: TCheckBox;
    Gozados: TZetaNumero;
    GBPeriodoSaldadas: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    TipoSaldadas: TZetaKeyCombo;
    NumeroSaldadas: TZetaKeyLookup_DevEx;
    YearSaldadas: TSpinEdit;
    SaldoFiltroTipoNomina: TCheckBox;
    GBVacacionesSaldadas: TGroupBox;
    Label14: TLabel;
    Label3: TLabel;
    Label13: TLabel;
    lblAniosMeses: TLabel;
    FechaReferencia: TZetaFecha;
    VencimientoMonths: TSpinEdit;
    sObservacion: TcxMemo;
    ObservacionesSaldadas: TcxMemo;
    procedure FormCreate(Sender: TObject);
    procedure lSaldarGozadosClick(Sender: TObject);
    procedure lSaldarPagadosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure YearNominaChange(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure YearSaldadasChange(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
    procedure lSaldarPrimaClick(Sender: TObject);
    procedure ChangeVencimientoMonths(Sender: TObject);
    procedure NumeroNominaValidKey(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure NumeroSaldadasValidKey(Sender: TObject);
    procedure RBProcesoClick(Sender: TObject);
    procedure WizardControlButtonClick(Sender: TObject;
      AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
  private
    function EsProcesoVaca: Boolean;
    function GetObservacionesSaldadas: String;
    procedure SetFiltroPeriodo( const iYear: Integer; const TipoPeriodo: eTipoPeriodo);
    function GetMesesAnios: string;
  protected
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
  end;

var
  WizEmpVacaciones_DevEx: TWizEmpVacaciones_DevEx;

implementation

uses DCliente,
     DCatalogos,
     DProcesos,
     ZetaCommonClasses,
     ZetaCommonTools,
     DGlobal,
     ZGlobalTress,
     ZBaseSelectGrid_DevEx,
     FEmpVacacionesGridSelect_DevEx,
     FEmpSaldarVacacionesGridSelect_DevEx,
     ZAccesosTress,
     ZAccesosMgr,
     Math;

{$R *.DFM}

const
     //Constantes radiobutton
     K_PROCESO_VACA = 0;
     K_PROCESO_SALDA = 1;
     //Constantes Paginas
     K_PAGE_PARAMETROS =0;
     K_PAGE_VACA = 1;
     K_PAGE_SALDA = 2;

     K_OBSERVA_SALDADAS_DEFAULT = 'Vacaciones No Tomadas';
     K_MAX_VENCIMIENTO_MONTHS = 108; //9 a�os
     K_NUM_MESES = 12;

procedure TWizEmpVacaciones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ObservacionesSaldadas.Tag := K_GLOBAL_DEF_COMENTA_SALDAR_VACA;
     with VencimientoMonths do
     begin
          Tag := K_GLOBAL_DEF_MONTH_SALDAR_VACA;
          MaxValue := K_MAX_VENCIMIENTO_MONTHS;
     end;
     with dmCatalogos do
     begin
          NumeroNomina.LookupDataset := cdsPeriodo;
          NumeroSaldadas.LookupDataset := cdsPeriodo;
     end;
     RBProceso.ItemIndex := K_PROCESO_VACA;
     sObservacion.Properties.MaxLength := K_ANCHO_TITULO;
     ObservacionesSaldadas.Properties.MaxLength := K_ANCHO_TITULO;
     HelpContext:= H10164_Vacaciones_globales;
     //DevEx(by am): Es necesario especificar que Ejecucion sera la ultima pagina del wizard.
     Parametros.PageIndex := K_PAGE_PARAMETROS;
     ParamVacaciones.PageIndex := K_PAGE_VACA;
     ParamSaldar.PageIndex := K_PAGE_SALDA;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 2; //3
     Ejecucion.PageIndex := WizardControl.PageCount - 1;          //4

end;

procedure TWizEmpVacaciones_DevEx.FormShow(Sender: TObject);
var
   PeriodoValido :Boolean;
begin
     inherited;
     PeriodoValido := True;
     dmCatalogos.cdsPeriodo.Conectar;
     with dmCliente do
     begin
          { Vacaciones }
          FechaInicial.Valor := FechaDefault;
          FechaFinal.Valor := FechaDefault;
          TipoNomina.Llave := IntToStr( Ord( PeriodoTipo ) ); //acl
          YearNomina.Value := YearDefault;
          if( not ZAccesosMgr.CheckDerecho( D_EMP_EXP_VACA, K_DERECHO_SIST_KARDEX ) )then
          begin
               PeriodoValido :=  not (cdsPeriodo.FieldByName('PE_STATUS').AsInteger >= Ord(spAfectadaParcial));
          end;
          if PeriodoValido then
             NumeroNomina.Valor := PeriodoNumero
          else
              NumeroNomina.Valor := 0;
          Otros.Valor := 0;
          { Saldar Vacaciones }
          FechaReferencia.Valor := FechaDefault;
          TipoSaldadas.Llave := IntToStr(  Ord( PeriodoTipo ) ) ;//acl
          YearSaldadas.Value := YearDefault;
          if PeriodoValido then
             NumeroSaldadas.Valor := PeriodoNumero
          else
              NumeroSaldadas.Valor := 0;
     end;
     if ZetaCommonTools.StrVacio( GetObservacionesSaldadas ) then
        ObservacionesSaldadas.Text := K_OBSERVA_SALDADAS_DEFAULT;
        
     with VencimientoMonths do
     begin
          if ( Value <= 0 ) then
             Value := K_NUM_MESES;
     end;
     lblAniosMeses.Caption := GetMesesAnios;
     with dmCatalogos do
     begin
          AplicaFiltroStatusPeriodo(NumeroNomina,True);
          AplicaFiltroStatusPeriodo(NumeroSaldadas,True);
     end;
     with TipoNomina do
     begin
          ListaFija:=lfTipoPeriodoConfidencial; //acl
          LlaveEntero:= Ord( dmCliente.GetDatosPeriodoActivo.Tipo );
     end;
     with TipoSaldadas do
     begin
          ListaFija:=lfTipoPeriodoConfidencial; //acl
          LlaveEntero:= Ord( dmCliente.GetDatosPeriodoActivo.Tipo );
     end;
     //DevEx(by am): Agregamos el mensaje para la advertencia del wizard.
     Advertencia.Caption := 'El proceso registrar� un movimiento de vacaciones a cada empleado en su historial de vacaciones.';
end;

procedure TWizEmpVacaciones_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     ParamInicial := 3;
     with Descripciones do
     begin
          Clear;   // Borra Parametros Anteriores - Es un wizard compuesto
          if EsProcesoVaca then
          begin
               AddString( 'Tipo de proceso', 'Agregar vacaciones' );
               AddDate( 'Fecha de inicio', FechaInicial.Valor );
               AddDate( 'Fecha de regreso', FechaFinal.Valor );
               AddString( 'Per�odo', sPeriodo.Text );
               AddString( 'Observaciones', sObservacion.Text );
               if lSaldarGozados.Checked then
                  AddString( 'D�as gozados', 'Saldar a la fecha' )
               else
                   AddString( 'D�as gozados', Gozados.Text );
               if lSaldarPagados.Checked then
                  AddString( 'D�as a pagar', 'Saldar a la fecha' )
               else
                   AddString( 'D�as a pagar', Pagados.Text );
               if lSaldarPrima.Checked then
                  AddString( 'D�as PV a pagar', 'Saldar a la fecha' )
               else
                   AddString( 'D�as PV a pagar', PrimaVaca.Text );
               AddFloat( 'Otros pagos', Otros.Valor );
               AddString( 'N�mina a pagar', ShowNomina( YearNomina.Value, TipoNomina.LlaveEntero , NumeroNomina.Valor ) );//acl
               AddBoolean( 'Filtro por tipo n�mina', FiltroTipoNomina.Checked );
          end
          else
          begin
               AddString( 'Tipo de proceso', 'Saldar vacaciones no tomadas' );
               AddDate( 'Fecha de referencia', FechaReferencia.Valor );
               AddInteger( 'Meses de vencimiento', VencimientoMonths.Value );
               AddString( 'Observaciones', GetObservacionesSaldadas );
               AddString( 'N�mina a Pagar', ShowNomina( YearSaldadas.Value, TipoSaldadas.LlaveEntero, NumeroSaldadas.Valor ) );
               AddBoolean( 'Filtro por tipo n�mina', SaldoFiltroTipoNomina.Checked );
          end;
     end;

     with ParameterList do
     begin
          AddBoolean( 'ProcesoVaca', EsProcesoVaca );
          if EsProcesoVaca then
          begin
               AddDate( 'FechaInicial', FechaInicial.Valor );
               AddDate( 'FechaFinal', FechaFinal.Valor );
               AddBoolean( 'SaldarGozados', lSaldarGozados.Checked );
               AddFloat( 'Gozados', Gozados.Valor );
               AddBoolean( 'SaldarPagados', lSaldarPagados.Checked );
               AddFloat( 'Pagados', Pagados.Valor );
               AddBoolean( 'SaldarPrima', lSaldarPrima.Checked );
               AddFloat( 'PrimaPagados', PrimaVaca.Valor );
               AddFloat( 'Otros', Otros.Valor );
               AddInteger( 'TipoNomina', TipoNomina.LlaveEntero );//acl
               AddInteger( 'NumeroNomina', NumeroNomina.Valor );
               AddInteger( 'YearNomina', YearNomina.Value );
               AddString( 'Periodo', sPeriodo.Text );
               AddString( 'Observaciones', sObservacion.Text );
               AddBoolean( 'FiltroEmpNom', FiltroTipoNomina.Checked );
          end
          else
          begin
               AddDate( 'FechaReferencia', FechaReferencia.Valor );
               AddInteger( 'VencimientoMonths', VencimientoMonths.Value );
               AddInteger( 'TipoNomina', TipoSaldadas.LlaveEntero ); //acl
               AddInteger( 'NumeroNomina', NumeroSaldadas.Valor );
               AddInteger( 'YearNomina', YearSaldadas.Value );
               AddString( 'Observaciones', GetObservacionesSaldadas );
               AddBoolean( 'FiltroEmpNom', SaldoFiltroTipoNomina.Checked );
          end;
     end;
end;

procedure TWizEmpVacaciones_DevEx.CargaListaVerificacion;
begin
     dmProcesos.VacacionesGetLista( ParameterList );
end;

function TWizEmpVacaciones_DevEx.Verificar: Boolean;
begin
     with dmProcesos do
     begin
          if EsProcesoVaca then
             Result := ZBaseSelectGrid_DevEx.GridSelect( cdsDataset, TEmpVacacionesGridSelect_DevEx )
          else
             Result := ZBaseSelectGrid_DevEx.GridSelect( cdsDataset, TEmpSaldarVacacionesGridSelect_DevEx );
     end;
end;

function TWizEmpVacaciones_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.Vacaciones( ParameterList, Verificacion );
end;

procedure TWizEmpVacaciones_DevEx.lSaldarGozadosClick(Sender: TObject);
begin
     with lSaldarGozados do
     begin
          lbSaldarGozar.Enabled := not Checked;
          Gozados.Enabled := not Checked;
     end;
end;

procedure TWizEmpVacaciones_DevEx.lSaldarPagadosClick(Sender: TObject);
begin
     with lSaldarPagados do
     begin
          lbSaldarPagar.Enabled := not Checked;
          Pagados.Enabled := not Checked;
     end;
end;

procedure TWizEmpVacaciones_DevEx.lSaldarPrimaClick(Sender: TObject);
begin
     inherited;
     with lSaldarPrima do
     begin
          lbSaldarPrima.Enabled := not Checked;
          PrimaVaca.Enabled := not Checked;
     end;
end;

procedure TWizEmpVacaciones_DevEx.SetFiltroPeriodo( const iYear: Integer; const TipoPeriodo: eTipoPeriodo);
begin
     with dmCatalogos do
     begin
          if ( not cdsPeriodo.IsEmpty ) then
          begin
               if ( ( iYear <> cdsPeriodo.FieldByName( 'PE_YEAR' ).AsInteger ) or
                    ( Ord( TipoPeriodo ) <> cdsPeriodo.FieldByName( 'PE_TIPO' ).AsInteger ) ) then
                  GetDatosPeriodo( iYear, TipoPeriodo );
          end
          else
              GetDatosPeriodo( iYear, TipoPeriodo );

          AplicaFiltroStatusPeriodo(NumeroNomina,True);
     end;
end;

procedure TWizEmpVacaciones_DevEx.YearNominaChange(Sender: TObject);
begin
     inherited;
     SetFiltroPeriodo( YearNomina.Value, eTipoPeriodo( StrAsInteger( TipoNomina.Llave ) ) );//acl
end;

procedure TWizEmpVacaciones_DevEx.YearSaldadasChange(Sender: TObject);
begin
     inherited;
     SetFiltroPeriodo( YearSaldadas.Value, eTipoPeriodo( StrAsInteger( TipoSaldadas.Llave ) ) );//acl
end;

function TWizEmpVacaciones_DevEx.EsProcesoVaca: Boolean;
begin
     Result := ( RBProceso.ItemIndex = K_PROCESO_VACA );
end;

procedure TWizEmpVacaciones_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
          var CanMove: Boolean);
const
     aPageOffSet: array[ FALSE..TRUE ] of Integer = ( -1, 1 );
begin
     ParametrosControl := nil;
     if Wizard.Adelante then
     begin
          if( ( Wizard.EsPaginaActual( ParamVacaciones ) ) and ( FechaInicial.Valor > FechaFinal.Valor ) ) then
              CanMove := Error(  'La fecha de inicio debe de ser menor � igual a la de fecha de regreso', FechaInicial );
          if( ( Wizard.EsPaginaActual( ParamSaldar ) ) and ZetaCommonTools.StrVacio( GetObservacionesSaldadas ) )then
              CanMove := Error( 'Deben especificarse las observaciones de saldar vacaciones', ObservacionesSaldadas );
          if CanMove then
          begin
             if  Wizard.SaltoEspecial then
             begin
                  if Wizard.EsPaginaActual( Parametros ) then
                  begin
                     if EsProcesoVaca then
                        iNewPage := ParamVacaciones.PageIndex  //Saltar a ParamVacaciones
                     else
                        iNewPage := ParamSaldar.PageIndex; //Saltar a ParamSaldar
                  end;
                  //Navegando hacia adelante si la pagina actual es ParamVacaciones o ParamSaltar se mueve a FiltrosCondiciones
                  if ((Wizard.EsPaginaActual( ParamVacaciones )) or  (Wizard.EsPaginaActual( ParamSaldar )))then
                     iNewPage := FiltrosCondiciones.PageIndex;
             end;
          end;
     end
     else
     begin
          //if CanMove then
         // begin
               if  Wizard.SaltoEspecial then
               begin
                    if Wizard.EsPaginaActual(FiltrosCondiciones) then
                    begin
                       if EsProcesoVaca then
                          iNewPage := ParamVacaciones.PageIndex  //Saltar a ParamVacaciones
                       else
                          iNewPage := ParamSaldar.PageIndex; //Saltar a ParamVacaciones
                    end;
                    if ((Wizard.EsPaginaActual( ParamVacaciones )) or  (Wizard.EsPaginaActual( ParamSaldar )))then
                         iNewPage := Parametros.PageIndex;
               end;
         // end;
     end;
     inherited;
end;

procedure TWizEmpVacaciones_DevEx.WizardAfterMove(Sender: TObject);
begin
     // Para Enfocar el primer control dependiendo del tipo de proceso
     if Wizard.Adelante then
          begin
             if ((Wizard.EsPaginaActual( ParamVacaciones )) or  (Wizard.EsPaginaActual( ParamSaldar )))then
             begin
                  if  (RBProceso.ItemIndex = K_PROCESO_VACA) then
                      ParametrosControl := FechaInicial
                  else if (RBProceso.ItemIndex = K_PROCESO_SALDA) then
                       ParametrosControl := FechaReferencia
                  else
                      ParametrosControl := nil;
             end;
          end
          else
              ParametrosControl := nil; //Cuanda vaya hacia atras no se quiere enfocar nada.
     inherited;
     { Puede que ya no coincidan los valores de cdsPeriodo con los controles de a�o y Tipo
       Ya que lo pudo haber cambiado cuando se especific� el otro proceso: Vacaciones o Saldar }
     if ( Wizard.EsPaginaActual( ParamVacaciones )) then
        YearNominaChange( Sender )
     else if ( Wizard.EsPaginaActual( ParamSaldar )) then
        YearSaldadasChange( Sender );
end;

function TWizEmpVacaciones_DevEx.GetObservacionesSaldadas: String;
begin
     Result := ObservacionesSaldadas.Text;
end;

procedure TWizEmpVacaciones_DevEx.ChangeVencimientoMonths(Sender: TObject);
begin
     inherited;
     lblAniosMeses.Caption := GetMesesAnios;
end;

function TWizEmpVacaciones_DevEx.GetMesesAnios:string;
var
   iAnios,iMeses:integer;
begin
     Result := VACIO;
     if ( VencimientoMonths.Text <> VACIO )then
     begin
          iAnios := VencimientoMonths.Value div K_NUM_MESES;
          iMeses := VencimientoMonths.Value mod K_NUM_MESES;
          Result := Format('%d a�o(s) %d mes(es)',[iAnios,iMeses] );
     end;

end;

procedure TWizEmpVacaciones_DevEx.NumeroNominaValidKey(Sender: TObject);
begin
     dmCatalogos.AplicaFiltroStatusPeriodo(NumeroNomina,True);
     inherited;

end;

procedure TWizEmpVacaciones_DevEx.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     dmCatalogos.AplicaFiltroStatusPeriodo(NumeroNomina,False);
     inherited;

end;

procedure TWizEmpVacaciones_DevEx.NumeroSaldadasValidKey(Sender: TObject);
begin
     dmCatalogos.AplicaFiltroStatusPeriodo(NumeroSaldadas,True);
     inherited;

end;

procedure TWizEmpVacaciones_DevEx.RBProcesoClick(Sender: TObject);
begin
  inherited;
  //Wizard.SaltoEspecial := ( RBProceso.ItemIndex = K_PROCESO_SALDA );
end;

procedure TWizEmpVacaciones_DevEx.WizardControlButtonClick(Sender: TObject;
  AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
begin
     if Wizard.EsPaginaActual(Ejecucion ) then
           Wizard.SaltoEspecial := FALSE
     else if Wizard.EsPaginaActual(FiltrosCondiciones) then
     begin
          //Wizard.SaltoEspecial := (AKind = wcbkNext);
          if AKind = wcbkNext then
             Wizard.SaltoEspecial := FALSE
          else
              Wizard.SaltoEspecial := TRUE;
     end
     else
          Wizard.SaltoEspecial := TRUE;

  inherited;
end;

end.

