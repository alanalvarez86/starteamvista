inherited WizEmpValidarDC4_DevEx: TWizEmpValidarDC4_DevEx
  Left = 705
  Top = 198
  Caption = 'Revisar datos para STPS'
  ClientHeight = 401
  ClientWidth = 433
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 433
    Height = 401
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que verifica los datos obligatorios para la declaraci'#243'n ' +
        'de capacitaci'#243'n ante la Secretar'#237'a del Trabajo y Previsi'#243'n Socia' +
        'l.'
      Header.Title = 'Revisar datos de STPS'
      object ArchivoLBL: TLabel
        Left = 19
        Top = 74
        Width = 110
        Height = 13
        Alignment = taRightJustify
        Caption = 'Archi&vo de Resultados:'
        FocusControl = Archivo
        Transparent = True
      end
      object AnioLbl: TLabel
        Left = 108
        Top = 32
        Width = 22
        Height = 13
        Caption = '&A'#241'o:'
        FocusControl = Anio
        Transparent = True
      end
      object Archivo: TEdit
        Left = 131
        Top = 70
        Width = 255
        Height = 21
        Hint = 'Archivo de Resultados de Revisi'#243'n de datos para STPS'
        TabOrder = 2
      end
      object Establecimiento: TCheckBox
        Left = 7
        Top = 51
        Width = 137
        Height = 17
        Hint = #191'Revisar Establecimientos?'
        Alignment = taLeftJustify
        Caption = 'Revisar &Establecimientos:'
        TabOrder = 1
      end
      object Anio: TZetaNumero
        Left = 131
        Top = 30
        Width = 65
        Height = 21
        Hint = 'A'#241'o'
        Mascara = mnDias
        TabOrder = 0
        Text = '0'
      end
      object ArchivoSeek: TcxButton
        Left = 387
        Top = 70
        Width = 21
        Height = 21
        Hint = 'Buscar Archivo de Importaci'#243'n'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = ArchivoSeekClick
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A40500000000000000000000000000000000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFDFF5
          FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
          FCFFDFF5FCFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF10B7EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF40C5EFFF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF40C5EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6CD3
          F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF6CD3F2FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF93DEF6FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF97E0F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFEBF9FDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF54CB
          F0FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CF
          F1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF80D9F4FF80D9
          F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC3EDFAFFDFF5FCFFDFF5FCFFDFF5
          FCFF70D4F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
        PaintStyle = bpsGlyph
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 166
        Width = 411
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 411
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 343
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 14
        Top = 137
      end
      inherited sFiltroLBL: TLabel
        Left = 39
        Top = 166
      end
      inherited Seleccionar: TcxButton
        Left = 142
        Top = 254
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 66
        Top = 134
      end
      inherited sFiltro: TcxMemo
        Left = 66
        Top = 165
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 66
        Top = 4
      end
      inherited bAjusteISR: TcxButton
        Left = 380
        Top = 166
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'txt'
    Filter = 'Archivos CSV (*.csv)|*.csv|Todos los Archivos|*.*'
    FilterIndex = 0
    Title = 
      'Escriba el Nombre del Archivo de Resultados de Revisi'#243'n de Datos' +
      ' para STPS'
    Left = 135
    Top = 154
  end
end
