inherited WizAgregarBaseDatosVisitantes_DevEx: TWizAgregarBaseDatosVisitantes_DevEx
  Left = 406
  Top = 221
  Hint = '608'
  Caption = 'Agregar BD de tipo Visitantes'
  ClientHeight = 580
  ClientWidth = 476
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 476
    Height = 580
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que permite agregar una base de datos de tipo Visitantes' +
        ' con la opci'#243'n para crearla o utilizar una existente.'
      Header.Title = 'Agregar base de datos de tipo Visitantes'
      inherited lblSeleccionarBD: TLabel
        Left = 88
        Top = 172
      end
      inherited lbNombreBD: TLabel
        Left = 81
        Top = 116
      end
      inherited cbBasesDatos: TComboBox
        Left = 223
        Top = 168
      end
      inherited txtBaseDatos: TEdit
        Left = 223
        Top = 112
      end
      inherited rbBDNueva: TcxRadioButton
        Left = 55
        Top = 85
      end
      inherited rbBDExistente: TcxRadioButton
        Left = 55
        Top = 149
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 233
        Width = 454
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 454
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 386
          AnchorY = 36
        end
      end
      inherited GrupoAvance: TcxGroupBox
        Top = 298
        Height = 142
        Width = 454
        inherited cxMemoStatus: TcxMemo
          Left = 273
          Height = 117
          Width = 178
        end
        inherited cxMemoPasos: TcxMemo
          Height = 117
          Width = 270
        end
      end
    end
    inherited NuevaBD2: TdxWizardControlPage
      inherited lblUbicacion: TLabel
        Left = 48
        Top = 90
      end
      inherited GBUsuariosMSSQL: TcxGroupBox
        Left = 47
        Top = 120
      end
      inherited lblDatoUbicacion: TcxLabel
        Left = 103
        Top = 88
      end
    end
    inherited Estructura: TdxWizardControlPage
      inherited lblDescripcion: TLabel
        Left = 68
      end
      inherited lblCodigo: TLabel
        Left = 91
      end
      inherited txtDescripcion: TZetaEdit
        Left = 130
      end
      inherited txtCodigo: TZetaEdit
        Left = 130
        OnExit = txtCodigoExit
      end
    end
    inherited NuevaBD1: TdxWizardControlPage
      Header.Description = 
        'Seleccione los par'#225'metros de cantidad de visitantes y de documen' +
        'tos digitales.'
      Header.Title = 'Cantidad de visitantes y documentos digitales'
      inherited lblCantidadEmpleados: TLabel
        Left = 80
        Width = 48
        Caption = 'Visitantes:'
      end
      inherited lblCantEmpSeleccion: TLabel
        Left = 367
      end
      inherited lblDocumentos: TLabel
        Left = 33
        Top = 136
      end
      inherited lblDocSeleccion: TLabel
        Left = 367
        Top = 134
      end
      inherited lblBaseDatos: TLabel
        Left = 64
        Top = 192
      end
      inherited lblTamanoSugerido: TLabel
        Left = 52
        Top = 224
      end
      inherited lblLogTransacciones: TLabel
        Left = 32
        Top = 256
      end
      inherited lblBaseDatosValor: TLabel
        Left = 144
        Top = 192
        Width = 20
      end
      inherited lblTamanoSugeridoValor: TLabel
        Left = 144
        Top = 224
        Width = 20
      end
      inherited lblLogTransaccionesValor: TLabel
        Left = 144
        Top = 256
        Width = 20
      end
      inherited tbEmpleados: TcxTrackBar
        Left = 144
        Style.IsFontAssigned = True
      end
      inherited tbDocumentos: TcxTrackBar
        Left = 144
        Top = 120
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Top = 253
  end
end
