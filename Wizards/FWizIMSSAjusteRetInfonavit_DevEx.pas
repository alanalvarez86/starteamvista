unit FWizIMSSAjusteRetInfonavit_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ZetaEdit, StdCtrls,
  ExtCtrls, ZetaDBTextBox, ZetaKeyCombo, Mask,
  ZetaNumero, ZCXBaseWizardFiltro, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
  cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl;

type
  TWizIMSSAjusteRetInfonavit_DevEx = class(TBaseCXWizardFiltro)
    GBTipoPrestamo: TcxGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    RGAnterior: TcxRadioGroup;
    NoTipoPrestamo: TZetaTextBox;
    TipoPrestamo: TZetaTextBox;
    BimestreDefault: TZetaKeyCombo;
    Label6: TLabel;
    YearDefaultAct: TZetaNumero;
    Label3: TLabel;
    Tolerancia: TZetaNumero;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    //DevEx Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);


  private
    { Private declarations }
  protected
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaParametros;override;
    procedure CargaListaVerificacion; override;
  public
    { Public declarations }
  end;

const
     K_REEMPLAZAR = 0;
     K_ANTERIOR   = 1;
     K_MIN_TOLERANCIA = 1;

var
  WizIMSSAjusteRetInfonavit_DevEx: TWizIMSSAjusteRetInfonavit_DevEx;

implementation

uses DCliente,
     DProcesos,
     DTablas,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaTipoEntidad,
     ZGlobalTress,
     DBaseGlobal,
     ZBaseSelectGrid_DevEx,
     DGlobal,
     FIMSSAjusteInfonavitGridSelect_DevEx;

{$R *.dfm}



procedure TWizIMSSAjusteRetInfonavit_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := YearDefaultAct;
     HelpContext:= H_WIZ_AJUSTE_RET_INFONAVIT;

     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;
end;

procedure TWizIMSSAjusteRetInfonavit_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     dmTablas.cdsTPresta.Conectar;
     NoTipoPrestamo.Caption:= Global.GetGlobalString(K_GLOBAL_AJUSINFO_TPRESTA);
     TipoPrestamo.Caption:= dmTablas.cdsTPresta.GetDescripcion(NoTipoPrestamo.Caption);
     Tolerancia.Valor:= K_MIN_TOLERANCIA;
     with dmCliente do
     begin
          YearDefaultAct.Valor := GetDatosPeriodoActivo.Year;
          BimestreDefault.Valor:= NumeroBimestre(ImssMes);
     end;

     //DevEx
     Advertencia.Caption :=
        'Al aplicar el proceso se depositar�n las diferencias en contra o a favor en un concepto ' +
        'de pr�stamo para que se puedan pagar o cobrar al empleado posteriormente.'
end;



procedure TWizIMSSAjusteRetInfonavit_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     ParametrosControl := nil;
     inherited;
     if CanMove then
     begin
          if Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
          begin
               if ( StrVacio( NoTipoPrestamo.Caption )  ) then
               begin
                    CanMove:= Error( 'Debe especificarse un tipo de Pr�stamo para Ajuste', YearDefaultAct );
               end;
          end;
     end;
end;


function TWizIMSSAjusteRetInfonavit_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TIMSSAjusteInfonavitGridSelect_DevEx );
end;

procedure TWizIMSSAjusteRetInfonavit_DevEx.CargaListaVerificacion;
begin
     inherited;
     dmProcesos.AjusteRetInfonavitGetLista( ParameterList );
end;

function TWizIMSSAjusteRetInfonavit_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.AjusteRetInfonavit(ParameterList, Verificacion);
end;

procedure TWizIMSSAjusteRetInfonavit_DevEx.CargaParametros;
const
     K_SIN_VALOR = -1;
var
   lReemplazar: Boolean;
begin
     lReemplazar:= ( RGAnterior.ItemIndex = K_REEMPLAZAR );
     inherited;

     with ParameterList do
     begin
          AddInteger('Year', YearDefaultAct.ValorEntero );
          AddInteger('Bimestre', BimestreDefault.Valor );
          AddBoolean('AjusAnt', lReemplazar );
          AddFloat( 'Tolerancia', Tolerancia.Valor );
     end;

     with Descripciones do
     begin
          AddString( 'Tipo pr�stamo para Ajuste', NoTipoPrestamo.Caption + ': ' + TipoPrestamo.Caption);
          AddInteger('A�o', YearDefaultAct.ValorEntero );
          AddString('Bimestre', ObtieneElemento( lfBimestres, BimestreDefault.Valor - 1 ) );
          AddBoolean('Reemplazar Ajuste anterior', lReemplazar );
          AddFloat( 'Tolerancia', Tolerancia.Valor );
     end;


end;

procedure TWizIMSSAjusteRetInfonavit_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := YearDefaultAct
     else
         ParametrosControl := nil;
     inherited;
end;

end.
