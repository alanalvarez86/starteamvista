unit FSistEnrolamientoMasivoGridSelect_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, StdCtrls, Buttons, ExtCtrls, Db,
     ZetaDBGrid, ZBaseSelectGrid_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, cxControls, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxTextEdit, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
  cxButtons;

type
  TSistEnrolamientoMasivoGridSelect_DevEx = class(TBaseGridSelect_DevEx)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SistEnrolamientoMasivoGridSelect_DevEx: TSistEnrolamientoMasivoGridSelect_DevEx;

implementation

{$R *.DFM}

end.
