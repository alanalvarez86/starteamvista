inherited WizEmpCorregirFechasCafe_DevEx: TWizEmpCorregirFechasCafe_DevEx
  Left = 355
  Top = 256
  Caption = 'Corregir Fechas Globales de Cafeter'#237'a'
  ClientHeight = 421
  ClientWidth = 449
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 449
    Height = 421
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que ofrece el mecanismo de correcci'#243'n a las fechas de lo' +
        's consumos de cafeter'#237'a.'
      Header.Title = 'Corregir fechas globales de cafeter'#237'a'
      object rbCorregir: TcxRadioGroup
        Left = 113
        Top = 108
        Align = alCustom
        Caption = ' Corregir  '
        Properties.Items = <
          item
            Caption = 'Comidas e Invitaciones'
            Value = '0'
          end
          item
            Caption = #218'nicamente Comidas'
            Value = '1'
          end
          item
            Caption = #218'nicamente Invitaciones'
            Value = '2'
          end>
        ItemIndex = 0
        TabOrder = 1
        OnClick = rbCorregirClick
        Height = 117
        Width = 200
      end
      object gbFechas: TcxGroupBox
        Left = 113
        Top = 0
        Align = alCustom
        Caption = ' Fechas '
        TabOrder = 0
        Height = 100
        Width = 200
        object Label1: TLabel
          Left = 13
          Top = 20
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = '&Actual:'
          FocusControl = FechaActual
          Transparent = True
        end
        object Label2: TLabel
          Left = 11
          Top = 53
          Width = 35
          Height = 13
          Alignment = taRightJustify
          Caption = '&Nueva:'
          FocusControl = FechaNueva
          Transparent = True
        end
        object FechaActual: TZetaFecha
          Left = 48
          Top = 17
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '19/ene/98'
          Valor = 35814.000000000000000000
        end
        object FechaNueva: TZetaFecha
          Left = 48
          Top = 49
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 1
          Text = '19/ene/98'
          Valor = 35814.000000000000000000
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 186
        Width = 427
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 427
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 359
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 14
        Top = 137
      end
      inherited sFiltroLBL: TLabel
        Left = 39
        Top = 160
      end
      inherited Seleccionar: TcxButton
        Left = 142
        Top = 254
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 66
        Top = 134
      end
      inherited sFiltro: TcxMemo
        Left = 66
        Top = 160
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 66
        Top = 4
      end
      inherited bAjusteISR: TcxButton
        Top = 190
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 544
    Top = 26
  end
end
