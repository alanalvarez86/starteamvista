unit FWizEmpImportarSGM_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Buttons, StdCtrls, ComCtrls, ExtCtrls,
     ZetaKeyCombo,
     ZetaWizard, ZetaDBTextBox, ZcxBaseWizard, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl, Menus, ZetaKeyLookup_DevEx,
  cxButtons;

type
  TWizEmpImportarSGM_DevEx = class(TcxBaseWizard)
    OpenDialog: TOpenDialog;
    Panel1: TPanel;
    PanelAnimation: TPanel;
    Animate: TAnimate;
    Panel2: TPanel;
    cbFFecha: TZetaKeyCombo;
    Label4: TLabel;
    Label2: TLabel;
    Formato: TZetaKeyCombo;
    Archivo: TEdit;
    Label1: TLabel;
    ArchivoSeek: TcxButton;
    Panel3: TPanel;
    Label3: TLabel;
    txtPM_NUMERO: TZetaTextBox;
    Label5: TLabel;
    Label6: TLabel;
    PV_REFEREN: TZetaKeyCombo;
    txtPV_FEC_INI: TZetaTextBox;
    Label7: TLabel;
    Label8: TLabel;
    txtPV_FEC_FIN: TZetaTextBox;
    txtPV_CONDIC: TZetaTextBox;
    Label9: TLabel;
    PM_CODIGO: TZetaKeyLookup_DevEx;
    PanelMensaje: TPanel;
    procedure ArchivoSeekClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure PM_CODIGOExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PV_REFERENChange(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
    FVerificaASCII: Boolean;
    FVerificacion: Boolean;
    ArchivoVerif : String;
    function LeerArchivo: Boolean;
    procedure SetVerificacion( const Value: Boolean );
    procedure CargaListaVerificaASCII;
    function GetArchivo: String;
    procedure LLenarListaReferencias;
    procedure MostrarDatosReferencia;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean;
    procedure CargaListaVerificacion;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizEmpImportarSGM_DevEx: TWizEmpImportarSGM_DevEx;

implementation

uses DProcesos,
     ZetaDialogo,
     ZetaMessages,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaClientTools,
     ZAsciiTools,
     ZBaseSelectGrid_DevEx,
     ZBaseGridShow_DevEx,
     FEmpImportarSGMGridShow_DevEx,
     Variants,
     FEmpSGMGridSelect_DevEx, dCatalogos;

{$R *.DFM}

procedure TWizEmpImportarSGM_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     PanelAnimation.Visible := FALSE;
     ParametrosControl := PM_CODIGO;
     Archivo.Text := ZetaMessages.SetFileNameDefaultPath( 'SGM.dat' );
     Formato.ItemIndex := Ord( faASCIIDel );
     cbFFecha.ItemIndex := Ord( ifDDMMYYYYs );
     ArchivoVerif:= VACIO;
     HelpContext:= H_Importar_SGM;
end;

procedure TWizEmpImportarSGM_DevEx.CargaParametros;
begin
     inherited CargaParametros;

     with ParameterList do
     begin
          AddString( 'PM_CODIGO', PM_CODIGO.Llave );
          AddString( 'Descripcion', PM_CODIGO.Descripcion );
          AddString( 'PV_REFEREN', PV_REFEREN.Text );
          AddString( 'FechaInicio', txtPV_FEC_INI.Caption );
          AddString( 'FechaFin', txtPV_FEC_FIN.Caption );

          AddInteger( 'Formato', Formato.Valor );
          AddInteger( 'FormatoImpFecha', cbFFecha.Valor );
          AddString( 'Archivo', Archivo.Text );
     end;
        with Descripciones do
     begin
          AddString( 'C�digo', PM_CODIGO.Llave );
          AddString( 'Descripci�n', PM_CODIGO.Descripcion );
          AddString( 'Referencia', PV_REFEREN.Text );
          AddString( 'Fecha Inicial', txtPV_FEC_INI.Caption );
          AddString( 'Fecha Final', txtPV_FEC_FIN.Caption );
          AddString('Condiciones', txtPV_CONDIC.Caption);
          AddString(  'Archivo', Archivo.Text );
          AddString( 'Formato ASCII', Formato.Text );
          AddString( 'Formato de Fecha', cbFFecha.text );

     end;
end;

procedure TWizEmpImportarSGM_DevEx.CargaListaVerificaASCII;
begin
     with dmProcesos do
     begin
          ImportarSGMGetListaASCII( ParameterList );
          if ( ErrorCount > 0 ) then                 // Hubo Errores
          begin
               with dmProcesos do
               begin
                    ZAsciiTools.FiltraASCII( cdsDataSet, True );
                    FVerificaASCII := ZBaseGridShow_DevEx.GridShow( cdsDataset, TEmpImportarSGMGridShow_DevEx );
                    ZAsciiTools.FiltraASCII( cdsDataSet, False );
               end;
          end
          else
              FVerificaASCII := True;
     end;
end;

procedure TWizEmpImportarSGM_DevEx.CargaListaVerificacion;
begin
     with dmProcesos do
     begin
          ImportarSGMGetLista( ParameterList );
     end;
end;

function TWizEmpImportarSGM_DevEx.Verificar: Boolean;
begin
     dmProcesos.cdsDataSet.First;
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpSGMGridSelect_DevEx, FALSE );
end;

procedure TWizEmpImportarSGM_DevEx.SetVerificacion(const Value: Boolean);
begin
     FVerificacion := Value;
end;

function TWizEmpImportarSGM_DevEx.GetArchivo: String;
begin
     Result := Archivo.Text;
end;

function TWizEmpImportarSGM_DevEx.LeerArchivo: Boolean;
begin
     Result := FALSE;
     PanelAnimation.Visible := TRUE;
     Application.ProcessMessages;
        CargaParametros;
        CargaListaVerificaASCII;
     PanelAnimation.Visible := FALSE;
     if FVerificaASCII then
     begin
          CargaListaVerificacion;
          SetVerificacion( Verificar );
          Result := FVerificacion;
     end;
     if Result then
        ArchivoVerif := GetArchivo
     else
        ArchivoVerif := VACIO;
end;

procedure TWizEmpImportarSGM_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
   ParametrosControl := nil;
     inherited;
     if Wizard.Adelante then
     begin
          if ( wizardControl.ActivePage = Parametros ) then
          begin
               if StrVacio(PM_CODIGO.Llave) then
                  CanMove := Error( 'Debe Especificarse C�digo de Seguro de Gastos M�dicos', PM_CODIGO )
               else
               if StrVacio(PV_REFEREN.Text) then
                  CanMove := Error( 'Debe Especificarse Referencia', PV_REFEREN )
               else
               if ZetaCommonTools.StrVacio( GetArchivo ) then
                  CanMove := Error( 'Debe Especificarse un Archivo a Importar', Archivo )
               else
                  if not FileExists( GetArchivo ) then
                     CanMove := Error( 'El Archivo ' + GetArchivo + ' No Existe', Archivo )
                  else
                  begin
                       if ( ArchivoVerif <> GetArchivo ) or
                          ( ZetaDialogo.ZConfirm( Caption, 'El Archivo ' + GetArchivo + ' Ya Fu� Verificado !' + CR_LF +
                                                  'Volver a Verificar ?', 0, mbYes ) ) then
                          CanMove := LeerArchivo
                       else
                          CanMove := TRUE;
                  end;
          end;
     end;
end;

function TWizEmpImportarSGM_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.ImportarSGM( ParameterList );
end;

procedure TWizEmpImportarSGM_DevEx.ArchivoSeekClick(Sender: TObject);
begin
     Archivo.Text := ZetaClientTools.AbreDialogo( OpenDialog, Archivo.Text, 'dat' );
{     with OpenDialog do
     begin
          FileName := ExtractFileName( Archivo.Text );
          InitialDir := ExtractFilePath( Archivo.Text );
          if Execute then
             Archivo.Text := FileName;
     end; }
end;

procedure TWizEmpImportarSGM_DevEx.PM_CODIGOExit(Sender: TObject);
begin
     LLenarListaReferencias;
     txtPM_NUMERO.Caption := dmCatalogos.cdsSegGastosMed.FieldByName('PM_NUMERO').AsString;
     inherited;
end;

procedure TWizEmpImportarSGM_DevEx.LLenarListaReferencias;
begin
     PV_REFEREN.Items.Clear;
     with dmCatalogos.cdsVigenciasSGM do
     begin
          Filtered := False;
          Filter := Format('PM_CODIGO = ''%s''',[PM_CODIGO.Llave]);
          Filtered := True;
          while not Eof do
          begin
               PV_REFEREN.Items.Add(FieldByName('PV_REFEREN').AsString);
               Next;
          end;
          if PV_REFEREN.Items.Count > 0 then
          begin
               PV_REFEREN.ItemIndex := PV_REFEREN.Items.Count -1;
               MostrarDatosReferencia;
          end
          else
          begin
              //Mostrar Mensaje
          end;
     end;
end;

procedure TWizEmpImportarSGM_DevEx.MostrarDatosReferencia;
begin
     with dmCatalogos.cdsVigenciasSGM do
     begin
          Locate('PM_CODIGO;PV_REFEREN',VarArrayOf([PM_CODIGO.Llave,PV_REFEREN.Text]),[]);
          txtPV_FEC_INI.Caption := FieldByName('PV_FEC_INI').AsString;
          txtPV_FEC_FIN.Caption := FieldByName('PV_FEC_FIN').AsString;
          txtPV_CONDIC.Caption := FieldByName('PV_CONDIC').AsString;
     end;
end;

procedure TWizEmpImportarSGM_DevEx.FormShow(Sender: TObject);
begin
     with dmCatalogos do
     begin
          cdsSegGastosMed.Conectar;
          cdsSegGastosMed.Open;
          PM_CODIGO.LookupDataset := cdsSegGastosMed;
     end;
     inherited;
end;

procedure TWizEmpImportarSGM_DevEx.PV_REFERENChange(Sender: TObject);
begin
     inherited;
     MostrarDatosReferencia;
end;

procedure TWizEmpImportarSGM_DevEx.WizardAfterMove(Sender: TObject);
begin
    inherited;
  if Wizard.EsPaginaActual(Parametros) then
   ParametrosControl := PM_CODIGO;
end;

end.
