unit FEmpCancelarCierreVacacionesGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseSelectGrid_DevEx, Db, StdCtrls, Grids, DBGrids, ZetaDBGrid, Buttons,
  ExtCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid, cxButtons, cxCalendar;

type
  TEmpCancelarCierreVacacionesGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    VA_FEC_INI: TcxGridDBColumn;
    VA_D_GOZO: TcxGridDBColumn;
    VA_D_PAGO: TcxGridDBColumn;
    VA_D_PRIMA: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpCancelarCierreVacacionesGridSelect_DevEx: TEmpCancelarCierreVacacionesGridSelect_DevEx;

implementation

{$R *.DFM}

procedure TEmpCancelarCierreVacacionesGridSelect_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with DataSet do
     begin
          MaskPesos( 'VA_D_GOZO' );
          MaskPesos( 'VA_D_PAGO' );
          MaskPesos( 'VA_D_PRIMA' );
     end;
end;

end.
