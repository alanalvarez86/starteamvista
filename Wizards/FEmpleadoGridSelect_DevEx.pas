unit FEmpleadoGridSelect_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ExtCtrls, Db, ZBaseSelectGrid_DevEx, cxGraphics, cxLookAndFeels,
     cxLookAndFeelPainters, Menus, dxSkinsCore, 
     TressMorado2013, cxControls, cxStyles, dxSkinscxPCPainter, cxCustomData,
     cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, ImgList,
     cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
     cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid, cxButtons,
     dxSkinsDefaultPainters, cxTextEdit;

type
  TEmpleadoGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpleadoGridSelect_DevEx: TEmpleadoGridSelect_DevEx;

implementation

{$R *.DFM}

end.
