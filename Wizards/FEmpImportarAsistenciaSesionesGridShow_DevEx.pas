unit FEmpImportarAsistenciaSesionesGridShow_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridShow_DevEx, Db, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons,
  ExtCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxButtons;

type
  TEmpImportarAsistenciaSesionesGridShow_DevEx = class(TBaseGridShow_DevEx)
    RENGLON: TcxGridDBColumn;
    CB_CODIGO: TcxGridDBColumn;
    SE_SESION: TcxGridDBColumn;
    NUM_ERROR: TcxGridDBColumn;
    DESC_ERROR: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpImportarAsistenciaSesionesGridShow_DevEx: TEmpImportarAsistenciaSesionesGridShow_DevEx;

implementation

{$R *.DFM}

end.
