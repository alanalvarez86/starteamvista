unit FEmpFoliarCapacitacionesSTPSGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseSelectGrid_DevEx, DB, StdCtrls, Grids, DBGrids, ZetaDBGrid,
  Buttons, ExtCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxButtons, cxCalendar;

type
  TEmpFoliarCapacitacionesSTPSGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CU_CODIGO: TcxGridDBColumn;
    KC_FEC_TOM: TcxGridDBColumn;
    KC_REVISIO: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpFoliarCapacitacionesSTPSGridSelect_DevEx: TEmpFoliarCapacitacionesSTPSGridSelect_DevEx;

implementation

{$R *.dfm}

end.
