inherited WizEmpBaseFiltro: TWizEmpBaseFiltro
  Left = 451
  Top = 185
  Caption = 'WizEmpBaseCXWizardFiltro'
  ClientHeight = 476
  ClientWidth = 626
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 626
    Height = 476
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EC200000EC2011528
      4A80000003A5494441545847C5975D48536118C767ADB00F08428824F4C6BE56
      04690569174D4C8D6A5B107611815144415110147E6CAF67735FE73DDB740B22
      22FA40D12DA2ECEBA2920CC36AE2DCE6C711BCF0B24B832EBAF0623DCF7ACF3C
      9BAFA6C9E6033F8E7B9F87F7FF3FCFFBC1519348245614EE602EE10EE612EE60
      2EF9EFB84BEE6E1405D7716A73551242F2D970F603C4B4D426BA2481CE000984
      0AE22F6A755A5849764312C42E4538136AA54E56969DA0841EE509AB98F1104F
      092B5F7AD43B5EE6598263E596D0C40373B7FC820465FD79E79B3C96D64856C9
      C6114D034C5E63E5C9F00D198BDAA3061A881B5F0662A67AD7B75A2D4BCD0D10
      2624349150630ECA3E7826F360E01E4F3485550403EEE66431445BE45445206E
      4AA8F1C74CF15B3DFBD7B292D920DDE39B32C5159A9F0E6FC51A98FC0A5758C1
      0A10B7096BAFDE3BAC05B1E94C03081D3C5E87356961E91EDBC113472CDDA315
      582311A90076FC34571C80DCA47224EF3C3FA2E38923BEC8C936AC490B73C748
      214F1C697A1CDEC9CAD0C491794CFC80DC2E56A6B1F41CDBCC1347A470ED6556
      361BE4D194C61294DF668AC3BE1868F2BD4EAD19DE5E2EE2DA86470E8EE42710
      FE4005DA82DD51F24AC01EB8C131F0FBF6ABD24256921EA44BD682099B4ABCBD
      C1D7939C180345708DF1347804FA48010C3C10897813BB8317152BD708BD95AB
      0231639DB217E0F9B1E97DB98EA5D3838446F3C833790309C92570FC742438BE
      AFB123B6B7C571BF18F3207C096FBC8CB6F3185696C2DE57951F889E2EF20D1A
      7607A2C683701CB70B9FF55B1ADE95AFC67C2A1A3B6305F0B65FD4ADFFDB0179
      DADEEA2FA644347384E682A7009E49A360A2BDEFDCC18CF62B4C7A23278A92E2
      0D8FC3EB5028531C69EE1839230A9E52983475EF2F165896AFB01C6BA0EDBD1C
      0349C4707581C61C926FF2C411ABF7D91E782B1F4F20057B6B1E54B4EAFC51C3
      599E38024BF20476FE442F4F1C696B7D980FEDECE74DBE183C5ECB75EFA0F100
      4F1C81EEFC84CDC717873DD18F4B04134D654EBC683C56D1FBDDA0E3892B2C68
      009ECB33005F2B6D43869D3C6185EC1A70DB9DEEBE93653C6185EC1A7039ECCB
      EAC0B2F7001A881877F084153470E3E9CD5DA335E6AEB1EAA6CEB8093F4280A3
      E6CEB1434B3600DF0369BFC1407BD8B0DE1F35EAE138EA7D43276AFCC3864AFC
      DB35505587BFB1030B0213FDDB807217A80DE098C3D98A2FB160F044D5C027D6
      198988F573B0392F4A2DEE0B4904F64454796AB7973199F943FD4FC24AC01DCC
      25DCC15CC21DCC1D09CD1FBF1FA3CB22C0F0220000000049454E44AE426082}
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 241
        Width = 604
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 604
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 536
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      Header.Description = 'Generar la Lista de Empleados que requiere aplicar el proceso.'
      inherited sFiltro: TcxMemo
        Style.IsFontAssigned = True
      end
    end
  end
end
