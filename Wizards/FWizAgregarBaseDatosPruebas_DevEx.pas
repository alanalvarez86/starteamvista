unit FWizAgregarBaseDatosPruebas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,  ComCtrls, StdCtrls, Buttons, ZetaWizard, ExtCtrls,
  ZetaEdit, ZetaDialogo, DB, ADODB, Mask, DBClient, ZetaClientDataSet,
  FWizBasicoAgregarBaseDatos_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit,
  ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl, cxRadioGroup, ZcxBaseWizard,
  cxTextEdit, cxMemo, cxCheckBox, ZetaKeyCombo;

type
  TSQLConnection = record
    ServerName : widestring;
    DatabaseName : wideString;
    UserName : widestring;
    Password  : widestring;
    end;

  TWizAgregarBaseDatosPruebas_DevEx = class(TWizBasicoAgregarBaseDatos_DevEx)
    CrearEmpresa: TdxWizardControlPage;
    chDepurarDigitales: TcxCheckBox;
    chDepurarAsistencia: TcxCheckBox;
    chDepurarBitacoras: TcxCheckBox;
    cbEmpresas: TZetaKeyCombo;
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure rbBDExistenteClick(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure rbBDNuevaClick(Sender: TObject);
    //procedure btnProbarClick(Sender: TObject);
    procedure rbOtroUserClick(Sender: TObject);
    procedure rbSameUserClick(Sender: TObject);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure txtCodigoExit(Sender: TObject);
  private
    { Private declarations }
    SC : TSQLConnection;
    function GetConnStr: widestring;
    property ConnStr : widestring read GetConnStr;
  public
    { Public declarations }
  protected
   { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
    procedure HabilitaControlesUsuario(lHabilita: Boolean);
    procedure LimpiaControlesUsuario;        
  end;

var
  WizAgregarBaseDatosPruebas_DevEx: TWizAgregarBaseDatosPruebas_DevEx;

implementation

uses DProcesos,
    DSistema,
    DBaseSistema,
    ZetaCommonClasses,
    ZetaCommonLists,
    ZetaServerTools;

{$R *.dfm}

function TWizAgregarBaseDatosPruebas_DevEx.GetConnStr: widestring;
begin
  SC.ServerName := dmSistema.GetServer;
  SC.UserName := txtUsuario.Text;
  SC.Password := txtClave.Text;

  Result := 'Provider=SQLOLEDB.1;';
  Result := Result + 'Data Source=' + SC.ServerName + ';';
  if SC.DatabaseName <> '' then
    Result := Result + 'Initial Catalog=' + SC.DatabaseName + ';';

    Result := Result + 'uid=' + SC.UserName + ';';
    Result := Result + 'pwd=' + SC.Password + ';';

end;


procedure TWizAgregarBaseDatosPruebas_DevEx.CargaParametros;
var sServer, sDataBase: String;
begin
     inherited;
     ParamInicial:=0;
     Descripciones.Clear;
     with Descripciones do
     begin
          if (rbBDNueva.Checked) then
          begin
               AddString( 'Base de datos', txtBaseDatos.Text );
               AddString('Ubicaci�n', lblDatoUbicacion.Caption);
               GetServerDatabase (dmSistema.GetBDFromDB_INFO(cbEmpresas.Llaves[cbEmpresas.ItemIndex]), sServer, sDataBase);
               AddString('Tama�o',format('BD origen %s MB, Log: %s MB', [ dmSistema.GetDBSize(cbEmpresas.Llaves[cbEmpresas.ItemIndex],sDataBase, TRUE),dmSistema.GetDataLogSize(cbEmpresas.Llaves[cbEmpresas.ItemIndex], sDataBase, TRUE)]));
          end
          else
          begin
               AddString( 'Base de datos existente', cbBasesDatos.Text );
               AddString( 'Ubicaci�n:',format('%s.%s', [dmSistema.GetServer, cbBasesDatos.Text ]));
          end;
           AddString( 'Registro de BD', UpperCase(txtCodigo.Text)+':'+ UpperCase(txtDescripcion.Text));

     end;
     with ParameterList do
     begin
          AddString( 'BaseDatos', cbBasesDatos.Text );
          AddString( 'Ubicacion', cbBasesDatos.Text );
          AddString( 'Codigo', UpperCase(txtCodigo.Text) );
          AddString( 'Descripcion', txtDescripcion.Text );
          AddInteger( 'Tipo', ord (tc3Prueba));
          AddString( 'Usuario', Trim(txtUsuario.Text) );
          AddString( 'Clave', ZetaServerTools.Encrypt(txtClave.Text));
     end;
end;

function TWizAgregarBaseDatosPruebas_DevEx.EjecutarWizard: Boolean;
Const
     K_TERMINADO='  Terminado';
     K_ERROR='  Error';
var
PathSQL, DataName, LogName, sServer, sDataBase: String;
lOk : Boolean;
procedure SetStatus (lTerminado: Boolean);
begin
     if lTerminado then
          cxMemoStatus.Lines.Append( K_TERMINADO)
     else
          cxMemoStatus.Lines.Append( K_ERROR);
end;
begin
     {***DevEx(by am):Mostar Grupo de Avance y Limpiar memos de avance***}
     GrupoAvance.Visible := TRUE;
     cxMemoPasos.Lines.Clear;
     cxMemoStatus.Lines.Clear;
    try 
	{***DevEx(by am):Ejecucion al Agregar y Crear una base de datos ***}
     if rbBDNueva.Checked then
     begin
          PathSQL :=dmSistema.GetPathSQLServer;
          dmSistema.cdsSistBaseDatos.Locate('DB_CODIGO', cbEmpresas.Llaves[cbEmpresas.ItemIndex], []);
          dmSistema.GetConexion(dmSistema.GetServer, dmSistema.cdsSistBaseDatos.FieldByName('DB_DATOS').AsString, dmSistema.GetServer, txtUsuario.Text, txtClave.Text );
          {***DevEx(by am): Paso1***}
          cxMemoPasos.Lines.Append('Respaldando base de datos... '); //Paso1
          GetServerDatabase (dmSistema.GetBDFromDB_INFO(cbEmpresas.Llaves[cbEmpresas.ItemIndex]), sServer, sDataBase);
          lOk := dmSistema.BackupDB(ConnStr, sDataBase, QuotedStr(PathSQL+txtBaseDatos.Text+'.bak'), VACIO);
          SetStatus(lOk); //Paso1
          if lOk then
          begin
               {***DevEx(by am): Paso2***}
               cxMemoPasos.Lines.Append('Restaurando base de datos... '); //Paso2
               DataName := dmSistema.GetLogicalName(ConnStr, sDataBase, 0);
               LogName := dmSistema.GetLogicalName(ConnStr, sDataBase, 1);
               lOk := dmSistema.RestoreDB(ConnStr, txtBaseDatos.Text, QuotedStr(PathSQL+txtBaseDatos.Text+'.bak'), QuotedStr(DataName ) , QuotedStr(PathSQL+txtBaseDatos.Text+'.mdf'), QuotedStr(LogName), QuotedStr(PathSQL+txtBaseDatos.Text+'_log.ldf'));
               SetStatus(lOk); //Paso2
               if lOk then
               begin
                    {***DevEx(by am): Paso3***}
                    if (chDepurarBitacoras.Checked) then
                    begin
                         cxMemoPasos.Lines.Append('Depurando bit�cora... '); //Paso3
                         setStatus(dmSistema.DepurarBitacora(ConnStr, txtBaseDatos.Text)); //Paso3
                    end;
                    {***DevEx(by am): Paso4***}
                    if  (chDepurarAsistencia.Checked) then
                    begin
                         cxMemoPasos.Lines.Append('Depurando asistencia... '); //Paso4
                         setStatus(dmSistema.DepurarAsistencia(ConnStr, txtBaseDatos.Text)); //Paso4
                    end;
                    {***DevEx(by am): Paso5***}
                    if (chDepurarDigitales.Checked) then
                    begin
                         cxMemoPasos.Lines.Append('Depurando documentos digitales... '); //Paso5
                         setStatus(dmSistema.DepurarDigitales(ConnStr, txtBaseDatos.Text)); //Paso5
                    end;
                    {***DevEx(by am): Paso6***}
                    cxMemoPasos.Lines.Append('Compactando base de datos... ');//Linea6
                    setStatus(dmSistema.ShrinkDB(ConnStr,txtBaseDatos.Text)); //Linea6
                    {***DevEx(by am): Paso7***}
                    with ParameterList do
                    begin
                         AddString( 'BaseDatos', txtBaseDatos.Text );
                         AddString( 'Ubicacion', txtBaseDatos.Text );
                         AddString( 'Codigo', UpperCase(txtCodigo.Text) );
                         AddString( 'Descripcion', txtDescripcion.Text );
                         AddInteger( 'Tipo', ord (tc3Prueba) );
                    end;
                    cxMemoPasos.Lines.Append('Registrando base de datos en sistema... '); //Paso7
                    lOk := dmSistema.GrabaDB_INFO(ParameterList.VarValues);
                    setStatus(lOk);//Paso7
                    if lOk then
                         ZInformation(Caption, 'La base de datos fue agregada correctamente.', 0)
                    else
                         ZError(Caption, 'Error al agregar base de datos.', 0);
               end;
          end;
    end
    else
    begin
         {***DevEx(by am):Ejecucion al agregar una BD Existente***}
         {***DevEx(by am): Paso1***}
         cxMemoPasos.Lines.Append('Registrando base de datos en sistema... '); //Paso1
         lOk := dmSistema.GrabaDB_INFO(ParameterList.VarValues);
         setStatus(lOk);//Paso1
         if lOk then
              ZInformation(Caption, 'La base de datos fue agregada correctamente.', 0)
         else
              ZError(Caption, 'Error al agregar base de datos.', 0);
    end;
    Result := TRUE;
        except
            on Error: Exception do
            begin
                  ZError(Caption, 'Error al agregar base de datos:' + CR_LF + Error.Message, 0);
                  Result := TRUE;
            end;
     end;
end;

procedure TWizAgregarBaseDatosPruebas_DevEx.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
var
   sServerOrigen, sDataBaseOrigen: string;
   mensaje : WideString;
begin
     ParametrosControl:= nil;
     if Wizard.Adelante then
     begin
          if Wizard.EsPaginaActual( Parametros ) then  //EN PAGINA DE PARAMETROS
          begin
               {***DevEx(by am): Las siguientes pantallas solo son necesarias para la nevegacion al crear una BD nueva***}
                     NuevaBD2.PageVisible := (rbBDNueva.Checked  = TRUE);
                     CrearEmpresa.PageVisible := (rbBDNueva.Checked  = TRUE);
               {***}
               if (rbBDNueva.Checked  = TRUE) then  //SI SE SELECCIONO CREAR UNA BD NUEVA
               begin
                     if ( txtBaseDatos.Text = VACIO ) then  //SI NO SE CAPTURO UN NOMBRE PARA LA BD
                        CanMove := Warning ('El nombre de la Base de Datos no puede quedar vac�o.', txtBaseDatos)
                     else
                        if (dmSistema.EsBaseDatosAgregada(dmSistema.GetServer + '.' + txtBaseDatos.Text)) then //SI EL NOMBRE PERTENECE A UNA BD YA AGREGADA
                              CanMove := Warning (Format ('La base de datos ''%s'' ya se encuentra agregada al sistema.', [txtBaseDatos.Text]), txtBaseDatos)
                        else
                              CanMove := TRUE;
                end;
                if(rbBDExistente.Checked = TRUE) then  //SI SE AGREGA UNA BD YA CREADA
                begin
                     if (cbBasesDatos.ItemIndex >= 0) then  //VALIDA SI SE SELECCIONO UNA BD
                     begin
                          if (dmSistema.EsBaseDatosAgregada(dmSistema.GetServer + '.' + cbBasesDatos.Text)) then  //Es Base de Datos ya agregada a BD_INFO?
                               CanMove := Warning (Format ('La base de datos ''%s'' ya se encuentra agregada al sistema.', [cbBasesDatos.Text]), cbBasesDatos)
                          else
                          begin
                               CanMove := TRUE;
                               if not (dmSistema.EsBaseDatosEmpleados(cbBasesDatos.Text)) then
                                  CanMove := Warning ('No se pudo configurar la base de datos para ser utilizada para pruebas ya que la estructura no es de empleados. Seleccione otra base de datos.', cbBasesDatos);
                          end;
                     end
                     else
                         CanMove := Warning ('Seleccione una base de datos existente para continuar.', cbBasesDatos);
                end;
          end
          else if Wizard.EsPaginaActual( Estructura) then  //EN PAGINA ESTRUCTURA
               if (trim (txtCodigo.Text) = VACIO) then  //SI NO SE CAPTURO UN CODIGO
                    CanMove := Warning ('El campo C�digo no puede quedar vac�o.', txtCodigo)
               else if (Trim (txtDescripcion.Text) = VACIO) then  //SI NO SE CAPTURO UNA DESCRIPCION
                    CanMove := Warning ('El campo Descripci�n no puede quedar vac�o.', txtDescripcion)
               else if (dmSistema.ExisteBD_DBINFO(trim(txtCodigo.Text))) then   //SI EL CODIGO YA EXISTE EN DB_INFO
                    CanMove := Warning (Format ('El c�digo  ''%s'' ya se encuentra agregado al sistema.', [txtCodigo.Text]), txtCodigo)
               else
                    CanMove := TRUE
          else if Wizard.EsPaginaActual( CrearEmpresa) then
          begin
               if not (cbEmpresas.Text = VACIO) then
               begin
                    GetServerDatabase (dmSistema.GetBDFromDB_INFO(cbEmpresas.Llaves[cbEmpresas.ItemIndex]), sServerOrigen, sDataBaseOrigen);
                    if not(sServerOrigen = dmSistema.GetServer) then
                         CanMove := Warning ('No se puede crear BD de Pruebas.' + CR_LF + 'La base de datos de pruebas debe crearse en el mismo servidor de BD de la empresa de Tress.', cbEmpresas)
                    else
                         CanMove := TRUE;
               end
               else
                    CanMove := Warning ('Seleccione una base de datos existente para continuar.', cbEmpresas);
          end
          else if Wizard.EsPaginaActual( NuevaBD2) then
          begin
               if rbOtroUser.Checked then
                    if not ProbarConexion(mensaje) then
                         CanMove := Warning (mensaje, txtUsuario)
                    else
                         CanMove := TRUE;
          end;
     end;
     inherited;
end;

procedure TWizAgregarBaseDatosPruebas_DevEx.rbBDExistenteClick(Sender: TObject);
begin
     txtBaseDatos.Enabled := false;
     cbBasesDatos.Enabled := true; 

     if (cbBasesDatos.Items.Count = 0) then
         begin
             // Obtener Empresas
             dmSistema.cdsBasesDatos.Conectar;
             dmSistema.cdsBasesDatos.First;
             while not dmSistema.cdsBasesDatos.Eof do
             begin
                     cbBasesDatos.AddItem(dmSistema.cdsBasesDatos.FieldByName ('NAME').AsString, nil);
                  dmSistema.cdsBasesDatos.Next;
             end;
         end; 
     cbBasesDatos.setFocus;
end;

procedure TWizAgregarBaseDatosPruebas_DevEx.WizardAfterMove(Sender: TObject);
begin
  inherited;
     if  Wizard.EsPaginaActual( Parametros ) then
          ParametrosControl:= txtBaseDatos
     else if  Wizard.EsPaginaActual( Estructura ) then
          ParametrosControl:= txtCodigo
     else if  Wizard.EsPaginaActual( CrearEmpresa ) then
     begin
         if (cbEmpresas.Items.Count = 0) then
         begin
             // Obtener Empresas
             dmSistema.cdsEmpresas.Conectar;
             dmSistema.cdsEmpresas.First;
             while not dmSistema.cdsEmpresas.Eof do
             begin
                  if (dmSistema.cdsEmpresas.FieldByName ('CM_CONTROL').AsString = ObtieneElemento( lfTipoCompany, Ord( tc3Datos ) )) then
                     cbEmpresas.AddItem(dmSistema.cdsEmpresas.FieldByName ('CM_NOMBRE').AsString, nil);
                  dmSistema.cdsEmpresas.Next;
             end;
         end;
     end;
     inherited;
end;

procedure TWizAgregarBaseDatosPruebas_DevEx.FormCreate(Sender: TObject);
begin
          inherited;
          {***DevEx(by am): Definicion de la secuencia de las paginas***}
          Parametros.PageIndex := 0;   //implementacion
          Estructura.PageIndex := 1;
          CrearEmpresa.PageIndex := 2;
          NuevaBD2.PageIndex := 3;
          Ejecucion.PageIndex := 4;
          {***}

          HelpContext := H_SIST_PROC_AGREGAR_BASE_DATOS; //DevEx(by am): Todos los Wizards que agregan BD comparten el mismo HelpContext

          with dmSistema do
          begin
               cdsEmpresasLookUp.Conectar;
          end;
end;

procedure TWizAgregarBaseDatosPruebas_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     rbSameUser.Checked := TRUE;
     HabilitaControlesUsuario(FALSE);
     dmSistema.GetBasesDatos(dmSistema.GetServer, VACIO, VACIO);
     if (cbEmpresas.Items.Count = 0) then
     begin
         try
             cbEmpresas.Lista.BeginUpdate;
             // Obtener Bases de Datos de Tipo Tress.
             with dmSistema.cdsSistBaseDatos do
             begin
                 Conectar;
                 First;
                 while not Eof do
                 begin
                      if (FieldByName ('DB_TIPO').AsInteger = Ord( tc3Datos )) or (FieldByName ('DB_TIPO').AsInteger = Ord( tc3Prueba )) then
                         cbEmpresas.Lista.Add(FieldByName ('DB_CODIGO').AsString + '=' + FieldByName ('DB_DESCRIP').AsString);
                      Next;
                 end;
             end;
         finally
                cbEmpresas.Lista.EndUpdate;
         end;
     end;
     ParametrosControl:= txtBaseDatos;
     Advertencia.Caption := 'Se proceder� a preparar la base de datos de tipo Pruebas. Presione Aplicar para iniciar el proceso.';
end;

procedure TWizAgregarBaseDatosPruebas_DevEx.rbBDNuevaClick(Sender: TObject);
begin
  inherited;
     txtBaseDatos.Enabled := true;
     txtBaseDatos.SetFocus;
     cbBasesDatos.Enabled := false;
end;

procedure TWizAgregarBaseDatosPruebas_DevEx.rbSameUserClick(Sender: TObject);
begin
  inherited;
    LimpiaControlesUsuario;
    HabilitaControlesUsuario(FALSE);
end;

procedure TWizAgregarBaseDatosPruebas_DevEx.rbOtroUserClick(Sender: TObject);
begin
  inherited;
    HabilitaControlesUsuario(TRUE);
end;

procedure TWizAgregarBaseDatosPruebas_DevEx.HabilitaControlesUsuario(lHabilita: Boolean);
begin
    txtUsuario.Enabled := lHabilita;
    txtClave.Enabled := lHabilita;
end;

procedure TWizAgregarBaseDatosPruebas_DevEx.LimpiaControlesUsuario;
begin
    txtUsuario.Text := VACIO;
    txtClave.Text := VACIO;
end;

procedure TWizAgregarBaseDatosPruebas_DevEx.WizardAlEjecutar(Sender: TObject;
  var lOk: Boolean);
begin
     inherited;
     if lOk then
         dmSistema.cdsSistBaseDatos.Refrescar;
end;

procedure TWizAgregarBaseDatosPruebas_DevEx.txtCodigoExit(Sender: TObject);
begin
      txtCodigo.Text :=  Copy(txtCodigo.Text, 0, 10);
end;

end.
