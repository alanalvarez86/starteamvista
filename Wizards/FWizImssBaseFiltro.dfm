inherited WizIMSSBaseFiltro: TWizIMSSBaseFiltro
  Left = 301
  Top = 130
  Caption = 'WizIMSSBaseFiltro'
  ClientHeight = 444
  ClientWidth = 492
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 492
    Height = 444
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
      F8000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EBC00000EBC0195BC
      7249000002B149444154484BB5954D68134114C75789B5156951B4A6205A45B0
      952A3914D45345450A86EC46F02678123C78F0A0F88160AF5241B21BF0EB1044
      B056F0926E62DAAD59DBD49662DBCD6EC48A1513DB6A2D057BA8506CACEB7BE9
      4C58DCD9B8517CF063775EE6FDDFE4CD9B59CE34CDFF8AA3DDD2F87A3113BC2D
      1A822619421E98970C3E25A685CB91ECE94A32EDEF4C4A0B67455D580451139E
      13A2C1772130CE529F64049AC9F4F22C940EB6A288A40BE352863F44DC458344
      C24A027EEAE6887F1371BB370C4481F0EB935EE2B219FE860B80F2B513973BC3
      BA174A60048E1297A36189707FC8B0B489C68906108EC0AA1452FB881B449D5F
      A0EF448A6DB0A97E9864FE0BD7D5160F91B31B760D2BA81CB0BC44CE6E92CEB7
      B182CA21A4070F1039BBC104AC2333D02DD8DE44CE6EB8B9AC2097CC43377584
      35DE47E4EC06BD9F6104FE897C48E7CFCF46390EB9337C78CBDD91631BE99842
      120873BF059764A53D03CDD8397DEAAECE69D9B30C6226322557E49FF737DE28
      26C08B8B25520A7AE1E56295DFA9F0E7AED53F113A3694DA8942027A7ADD82D7
      08AE3CADD40E52B12FD155E660727BA7D2DF782D17AF5AA4FEEE81A62B1C5E68
      2C2127C286701F574645900175C7BDD84B9F5F53BCA96CACEA1BF54FCA154B9C
      A809A758424E40C79C7BF2EAE03E6B82D1DEBAC4A3D123DB3EC4D77DB5FA1138
      C5FC05969013D8390FC75A9AB02C5621A367F33024A9799BA8C95AFD70D109ED
      2C2127B0E721909BB16CE8BB44F5DC78F7865CF245C345B56FF725EAFF247B7E
      60820E969013B0C933986028591FA74263BD7599A743FB8FCB03BE3393B1B54B
      D4DF93DA731513A82CA1924059B193DE3F5BBF40C560B5CB081DA715EF1B7816
      0E59E13B5B2679FC28619294BAF3F1B4BCA628FC11FE4122B5B70DC50B09E016
      DC8A67A15CF0B3494590075A6B351E3EAB6F36CA71BF000786687A3B9F27F400
      00000049454E44AE426082}
    inherited Parametros: TdxWizardControlPage
      object PeriodoGB: TcxGroupBox
        Left = 0
        Top = 0
        Align = alTop
        TabOrder = 0
        Height = 145
        Width = 470
        object Label1: TLabel
          Left = 121
          Top = 88
          Width = 22
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'&o:'
          Transparent = True
        end
        object Label2: TLabel
          Left = 120
          Top = 57
          Width = 23
          Height = 13
          Alignment = taRightJustify
          Caption = '&Mes:'
          FocusControl = Mes
          Transparent = True
        end
        object PatronLbl: TLabel
          Left = 59
          Top = 26
          Width = 84
          Height = 13
          Alignment = taRightJustify
          Caption = 'Re&gistro Patronal:'
          FocusControl = Patron
          Transparent = True
        end
        object Label3: TLabel
          Left = 47
          Top = 119
          Width = 96
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de Liquidaci'#243'n:'
          Transparent = True
        end
        object Mes: TZetaKeyCombo
          Left = 152
          Top = 53
          Width = 158
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 0
          ListaFija = lfMeses
          ListaVariable = lvPuesto
          Offset = 1
          Opcional = False
          EsconderVacios = False
        end
        object Patron: TZetaKeyLookup_DevEx
          Left = 152
          Top = 22
          Width = 278
          Height = 21
          LookupDataset = dmCatalogos.cdsRPatron
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 60
        end
        object Tipo: TZetaKeyCombo
          Left = 152
          Top = 115
          Width = 158
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 2
          ListaFija = lfTipoLiqIMSS
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object Anio: TZetaNumero
          Left = 152
          Top = 84
          Width = 121
          Height = 21
          Mascara = mnDias
          MaxLength = 4
          TabOrder = 3
          Text = '0'
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 209
        Width = 470
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 470
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 402
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sFiltro: TcxMemo
        Style.IsFontAssigned = True
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 392
    Top = 10
  end
end
