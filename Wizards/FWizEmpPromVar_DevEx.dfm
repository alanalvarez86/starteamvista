inherited WizEmpPromVar_DevEx: TWizEmpPromVar_DevEx
  Left = 509
  Top = 109
  Caption = 'Promediar Percepciones Variables'
  ClientHeight = 441
  ClientWidth = 479
  ExplicitWidth = 485
  ExplicitHeight = 470
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 479
    Height = 441
    ExplicitWidth = 479
    ExplicitHeight = 441
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Este proceso se realiza sumando las percepciones variables corre' +
        'spondientes a un periodo utilizando el bimestre de acumulados.'
      Header.Title = 'Promediar Percepciones Variables'
      ExplicitWidth = 457
      ExplicitHeight = 301
      object nYearLBL: TLabel
        Left = 29
        Top = 14
        Width = 98
        Height = 13
        Alignment = taRightJustify
        Caption = 'A'#241'&o de Acumulados:'
        FocusControl = iYear
        Transparent = True
      end
      object nMonthLBL: TLabel
        Left = 84
        Top = 38
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = '&Bimestre:'
        FocusControl = iMonth
        Transparent = True
      end
      object Label1: TLabel
        Left = 12
        Top = 62
        Width = 115
        Height = 13
        Alignment = taRightJustify
        Caption = '&Percepciones Promedio:'
        FocusControl = sFormula
        Transparent = True
      end
      object sObservaLBL: TLabel
        Left = 53
        Top = 210
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'Obser&vaciones:'
        FocusControl = sObserva
        Transparent = True
      end
      object sDescripcionLBL: TLabel
        Left = 68
        Top = 186
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = '&Descripci'#243'n:'
        FocusControl = sDescripcion
        Transparent = True
      end
      object Label2: TLabel
        Left = 13
        Top = 163
        Width = 114
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha de aviso a &IMSS:'
        FocusControl = dIMSS
        Transparent = True
      end
      object dReferenciaLBL: TLabel
        Left = 41
        Top = 138
        Width = 86
        Height = 13
        Alignment = taRightJustify
        Caption = '&Fecha de Cambio:'
        FocusControl = dReferencia
        Transparent = True
      end
      object lbBimestre: TLabel
        Left = 277
        Top = 38
        Width = 48
        Height = 13
        Caption = 'lbBimestre'
        Transparent = True
      end
      object iYear: TSpinEdit
        Left = 130
        Top = 9
        Width = 65
        Height = 22
        MaxValue = 3000
        MinValue = 1900
        TabOrder = 0
        Value = 1900
        OnChange = iYearChange
      end
      object iMonth: TZetaKeyCombo
        Left = 130
        Top = 34
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 1
        OnChange = iYearChange
        ListaFija = lfBimestres
        ListaVariable = lvPuesto
        Offset = 1
        Opcional = False
        EsconderVacios = False
      end
      object sFormula: TMemo
        Left = 130
        Top = 58
        Width = 291
        Height = 72
        TabOrder = 2
      end
      object FormulaBuild: TcxButton
        Left = 424
        Top = 59
        Width = 25
        Height = 25
        Hint = 'Constructor de F'#243'rmulas'
        OptionsImage.Glyph.Data = {
          7A080000424D7A08000000000000360000002800000017000000170000000100
          2000000000004408000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFF000000000000000000000000D9AB81FFD8AA7FFF0000000000000000D8AA
          7FFFD8AA7FFF000000002E241B36D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
          0000DCB38DFF0000000000000000D8AA7FFFDAAF87FF00000000000000000000
          00000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD9AD83FF00000000DAAE85FFD8AA7FFFD8AA7FFF00000000000000000302
          0203D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFF00000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
          000000000000D8AA7FFFD8AA7FFF010000010000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF000000000000
          0000D8AA7FFF0000000000000000D8AA7FFF0000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000DCB3
          8DFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFF0000000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFDCB28BFF00000000D9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFF00000000000000000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = FormulaBuildClick
      end
      object dReferencia: TZetaFecha
        Left = 130
        Top = 133
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 4
        Text = '02/Jan/98'
        Valor = 35797.000000000000000000
        OnValidDate = dReferenciaValidDate
      end
      object dIMSS: TZetaFecha
        Left = 130
        Top = 158
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 5
        Text = '02/Jan/98'
        Valor = 35797.000000000000000000
      end
      object sDescripcion: TEdit
        Left = 130
        Top = 182
        Width = 291
        Height = 21
        TabOrder = 6
        Text = 'Promedio de Percepciones Variables'
      end
      object sObserva: TcxMemo
        Left = 129
        Top = 206
        Properties.ScrollBars = ssVertical
        TabOrder = 7
        Height = 65
        Width = 292
      end
      object lIncapacitados: TcxCheckBox
        Left = -3
        Top = 273
        Caption = '         Incluir &Incapacitados:'
        Properties.Alignment = taRightJustify
        TabOrder = 8
        Transparent = True
        Width = 149
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      ExplicitWidth = 457
      ExplicitHeight = 301
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 457
        ExplicitHeight = 206
        Height = 206
        Width = 457
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 457
        Width = 457
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          ExplicitLeft = 67
          ExplicitTop = 20
          ExplicitWidth = 389
          ExplicitHeight = 74
          Width = 389
          AnchorY = 57
        end
        inherited cxImage1: TcxImage
          ExplicitLeft = 1
          ExplicitTop = 20
          ExplicitHeight = 74
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      ExplicitWidth = 457
      ExplicitHeight = 301
      inherited sCondicionLBl: TLabel
        Left = 30
        Top = 145
        ExplicitLeft = 30
        ExplicitTop = 145
      end
      inherited sFiltroLBL: TLabel
        Left = 55
        Top = 168
        ExplicitLeft = 55
        ExplicitTop = 168
      end
      inherited Seleccionar: TcxButton
        Left = 158
        Top = 262
        Visible = True
        ExplicitLeft = 158
        ExplicitTop = 262
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 82
        Top = 142
        ExplicitLeft = 82
        ExplicitTop = 142
      end
      inherited sFiltro: TcxMemo
        Left = 82
        Top = 168
        Style.IsFontAssigned = True
        ExplicitLeft = 82
        ExplicitTop = 168
      end
      inherited GBRango: TGroupBox
        Left = 82
        Top = 12
        ExplicitLeft = 82
        ExplicitTop = 12
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Top = 326
  end
end
