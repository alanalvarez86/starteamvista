inherited WizNomCalcularSimulacion_DevEx: TWizNomCalcularSimulacion_DevEx
  Left = 396
  Top = 216
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Buttons.Back.Enabled = False
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que permite visualizar los montos que se le tendr'#237'an que' +
        ' entregar a un empleado si es despedido o renunciar'#225'.'
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited cxGroupBox1: TcxGroupBox
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          AnchorX = 243
          AnchorY = 49
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      PageVisible = False
      inherited sFiltro: TcxMemo
        Style.IsFontAssigned = True
      end
    end
    inherited TipoCalculo: TdxWizardControlPage
      PageVisible = False
    end
  end
end
