unit FEmpSGMGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons,
  ExtCtrls, dbClient, ZBaseSelectGrid_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, cxControls, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons,
  cxCalendar;

type

  TEmpSGMGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    procedure FormShow(Sender: TObject);

  private
    procedure MontosGetText(Sender: TField; var Text: String; DisplayText: Boolean);
  public
  end;

var
  EmpSGMGridSelect_DevEx: TEmpSGMGridSelect_DevEx;

implementation

uses dGlobal,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo,
     ZGlobalTress;

{$R *.DFM}

procedure TEmpSGMGridSelect_DevEx.FormShow(Sender: TObject);
begin
     //AgregaColumnasGrid;// No Habr� problema con siempre Agregar Columnas por que se esta haciendo un free de la forma al terminar
     DataSet.FieldByName( 'EP_CFIJO' ).onGetText := MontosGetText;
     DataSet.FieldByName( 'EP_CTITULA' ).onGetText := MontosGetText;
     DataSet.FieldByName( 'EP_CPOLIZA' ).onGetText := MontosGetText;
     DataSet.FieldByName( 'EP_CEMPRES' ).onGetText := MontosGetText;
     DataSet.FieldByName( 'EP_CTOTAL' ).onGetText := MontosGetText;

     ZetaDBGriddbtableview.Columns[13].Visible := not (DataSet.FieldByName( 'EP_OBSERVA' ).AsString = 'BoRrAnDo');
     inherited;
end;

procedure TEmpSGMGridSelect_DevEx.MontosGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     //MostrarCampoTexto( Sender, Text, DisplayText );
     if DisplayText then
        Text := FormatFloat( '#,0.00', Sender.AsFloat );
end;

end.
