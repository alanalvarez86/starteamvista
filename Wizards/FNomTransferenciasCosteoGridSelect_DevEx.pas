unit FNomTransferenciasCosteoGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, ExtCtrls, ZBaseSelectGrid_DevEx,
  cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, cxControls, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ZetaCXGrid, cxButtons, cxTextEdit;

type
  TNomTransferenciasCosteoGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CC_CODIGO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  NomTransferenciasCosteoGridSelect_DevEx: TNomTransferenciasCosteoGridSelect_DevEx;

implementation

uses
    ZGlobalTress,
    DGlobal, ZBasicoSelectGrid_DevEx;

{$R *.DFM}

procedure TNomTransferenciasCosteoGridSelect_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     with Global do
          //ZetaDBGrid.Columns[2].Title.Caption := Global.NombreCosteo + ' Origen';
          ZetaDBGridDBTableView.Columns[2].Caption := Global.NombreCosteo + ' Origen';  //DevEx
end;

procedure TNomTransferenciasCosteoGridSelect_DevEx.FormShow(
  Sender: TObject);
begin
  inherited;
   (ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO')).Caption := 'Empleado';
   (ZetaDBGridDBTableView.GetColumnByFieldName('PrettyName')).Caption := 'Nombre Completo';
   (ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO')).MinWidth := 75;
end;

end.
