inherited EmpCancelarVacacionesGridSelect_DevEx: TEmpCancelarVacacionesGridSelect_DevEx
  Left = 182
  Top = 209
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Seleccione Los Registros de Vacaciones a Cancelar'
  ClientHeight = 268
  ClientWidth = 682
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 232
    Width = 682
    inherited OK_DevEx: TcxButton
      Left = 518
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 596
    end
  end
  inherited PanelSuperior: TPanel
    Width = 682
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 682
    Height = 197
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
      end
      object VA_GOZO: TcxGridDBColumn
        Caption = 'Gozados'
        DataBinding.FieldName = 'VA_GOZO'
      end
      object VA_PAGO: TcxGridDBColumn
        Caption = 'Pagados'
        DataBinding.FieldName = 'VA_PAGO'
      end
      object VA_P_PRIMA: TcxGridDBColumn
        Caption = 'Prima vac.'
        DataBinding.FieldName = 'VA_P_PRIMA'
      end
      object VA_FEC_INI: TcxGridDBColumn
        Caption = 'Fecha Registro'
        DataBinding.FieldName = 'VA_FEC_INI'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
      end
      object VA_CAPTURA: TcxGridDBColumn
        Caption = 'Fecha Captura'
        DataBinding.FieldName = 'VA_CAPTURA'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
      end
      object US_CODIGO: TcxGridDBColumn
        Caption = 'Usuario'
        DataBinding.FieldName = 'US_CODIGO'
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
