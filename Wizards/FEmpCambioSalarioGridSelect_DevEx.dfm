inherited EmpCambioSalarioGridSelect_DevEx: TEmpCambioSalarioGridSelect_DevEx
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Seleccione Los Empleados Deseados'
  ClientHeight = 216
  ClientWidth = 492
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 180
    Width = 492
    inherited OK_DevEx: TcxButton
      Left = 328
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 407
      Cancel = True
    end
  end
  inherited PanelSuperior: TPanel
    Width = 492
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 492
    Height = 145
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsCustomize.ColumnHorzSizing = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.Indicator = True
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object CB_SALARIO: TcxGridDBColumn
        Caption = 'Salario Actual'
        DataBinding.FieldName = 'CB_SALARIO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 75
      end
      object NUEVO_SAL: TcxGridDBColumn
        Caption = 'Salario Nuevo'
        DataBinding.FieldName = 'NUEVO_SAL'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 85
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
