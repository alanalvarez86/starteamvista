inherited EmpImportarACarAboGridShow_DevEx: TEmpImportarACarAboGridShow_DevEx
  Left = 234
  Top = 362
  Width = 684
  Caption = 'Lista de Errores Detectados'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 668
    inherited OK_DevEx: TcxButton
      Left = 504
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 583
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 668
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object RENGLON: TcxGridDBColumn
        Caption = 'Rengl'#243'n'
        DataBinding.FieldName = 'RENGLON'
        Width = 64
      end
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
        Width = 64
      end
      object AH_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'AH_TIPO'
        Width = 64
      end
      object CR_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'CR_FECHA'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
      end
      object TIPO_MOV: TcxGridDBColumn
        Caption = 'Movimiento'
        DataBinding.FieldName = 'TIPO_MOV'
      end
      object MONTO: TcxGridDBColumn
        Caption = 'Monto'
        DataBinding.FieldName = 'MONTO'
      end
      object NUM_ERROR: TcxGridDBColumn
        Caption = 'Errores'
        DataBinding.FieldName = 'NUM_ERROR'
        Width = 64
      end
      object DESC_ERROR: TcxGridDBColumn
        Caption = 'Descripci'#243'n de Errores'
        DataBinding.FieldName = 'DESC_ERROR'
        Width = 64
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
