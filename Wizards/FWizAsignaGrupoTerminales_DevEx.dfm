inherited WizAsignaGrupoTerminales_DevEx: TWizAsignaGrupoTerminales_DevEx
  Left = 369
  Top = 251
  Caption = 'Asigna Grupo de Terminales'
  ClientHeight = 429
  ClientWidth = 442
  ExplicitWidth = 448
  ExplicitHeight = 458
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 442
    Height = 429
    ExplicitWidth = 442
    ExplicitHeight = 429
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que permite asignar un grupo de terminales a un conjunto' +
        ' de empleados con n'#250'mero biom'#233'trico asignado.'
      Header.Title = 'Asignaci'#243'n de grupos de terminales'
      ExplicitWidth = 420
      ExplicitHeight = 289
      object Label1: TLabel
        Left = 15
        Top = 118
        Width = 97
        Height = 13
        Caption = 'Grupo de terminales:'
      end
      object luGrupo: TZetaKeyLookup_DevEx
        Left = 115
        Top = 113
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      ExplicitWidth = 420
      ExplicitHeight = 289
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 420
        ExplicitHeight = 194
        Height = 194
        Width = 420
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 420
        Width = 420
        inherited Advertencia: TcxLabel
          Caption = 
            'Al aplicar el proceso ser'#225' asignado el grupo de terminal en los ' +
            'empleados indicados.'
          Style.IsFontAssigned = True
          ExplicitLeft = 67
          ExplicitTop = 20
          ExplicitWidth = 352
          ExplicitHeight = 74
          Width = 352
          AnchorY = 57
        end
        inherited cxImage1: TcxImage
          ExplicitLeft = 1
          ExplicitTop = 20
          ExplicitHeight = 74
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      ExplicitWidth = 420
      ExplicitHeight = 289
      inherited sCondicionLBl: TLabel
        Left = 6
        Top = 137
        ExplicitLeft = 6
        ExplicitTop = 137
      end
      inherited sFiltroLBL: TLabel
        Left = 31
        Top = 164
        ExplicitLeft = 31
        ExplicitTop = 164
      end
      inherited Seleccionar: TcxButton
        Left = 136
        Top = 262
        Visible = True
        ExplicitLeft = 136
        ExplicitTop = 262
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 58
        Top = 134
        ExplicitLeft = 58
        ExplicitTop = 134
      end
      inherited sFiltro: TcxMemo
        Left = 58
        Top = 163
        Style.IsFontAssigned = True
        ExplicitLeft = 58
        ExplicitTop = 163
      end
      inherited GBRango: TGroupBox
        Left = 58
        Top = 2
        ExplicitLeft = 58
        ExplicitTop = 2
      end
      inherited bAjusteISR: TcxButton
        Left = 373
        Top = 164
        ExplicitLeft = 373
        ExplicitTop = 164
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
  end
end
