unit FWizAgregarBaseDatosSeleccion_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, ZetaDialogo,
  ZetaEdit, Mask, ZetaNumero, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxTrackBar, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, FWizBaseAgregarBaseDatos_DevEx,
  ZetaCXWizard, cxTextEdit, cxMemo, dxGDIPlusClasses, cxImage, cxLabel,
  cxGroupBox, cxRadioGroup, dxCustomWizardControl, dxWizardControl;

type
  TSQLConnection = record
    ServerName : widestring;
    DatabaseName : wideString;
    UserName : widestring;
    Password  : widestring;
    end;

type
  TWizAgregarBaseDatosSeleccion_DevEx = class(TWizBaseAgregarBaseDatos_DevEx)
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
    procedure txtCodigoExit(Sender: TObject);
   private
    { Private declarations }
    FTieneEstructura: Boolean;
   public
    { Public declarations }
  Protected
    { Protected declarations }
     procedure CargaParametros; override;
    function EjecutarWizard: Boolean; override;
     function ProcesarBD: Boolean;
  end;

var
  WizAgregarBaseDatosSeleccion_DevEx: TWizAgregarBaseDatosSeleccion_DevEx;

implementation
uses DSistema,
    DBaseSistema,
    ZetaCommonClasses,
    ZetaCommonLists,
    ZetaServerTools, ZcxWizardBasico, FWizBasicoAgregarBaseDatos_DevEx,
  ZcxBaseWizard;
{$R *.dfm}

procedure TWizAgregarBaseDatosSeleccion_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
    FTieneEstructura := FALSE;
     with dmSistema do
     begin
          cdsEmpresasLookUp.Conectar;
     end;

     WizardControl.ActivePage := Parametros;
     //DevEx
     Parametros.PageIndex := 0;
     NuevaBD1.PageIndex := 1;
     NuevaBD2.PageIndex := 2;
     Estructura.PageIndex := 3;
     Ejecucion.PageIndex := 4;
     {***Se define el tipo de salto a utilizar en los wizards***}
     Wizard.SaltoEspecial := TRUE;
end;

procedure TWizAgregarBaseDatosSeleccion_DevEx.FormShow(Sender: TObject);
begin
     rbSameUser.Checked := TRUE;
     HabilitaControlesUsuario(FALSE);
     dmSistema.GetBasesDatos(dmSistema.GetServer, VACIO, VACIO);
     //txtBaseDatos.SetFocus;
     ParametrosControl := txtBaseDatos;     //DevEx
     if (cbBasesDatos.Items.Count = 0) then
     begin
         while not dmSistema.cdsBasesDatos.Eof do
         begin
              cbBasesDatos.AddItem(dmSistema.cdsBasesDatos.FieldByName ('NAME').AsString, nil);
              dmSistema.cdsBasesDatos.Next;
         end;
     end;

     cxMemoStatus.Lines.Clear;
     cxMemoPasos.Lines.Clear;
end;
procedure TWizAgregarBaseDatosSeleccion_DevEx.CargaParametros;
begin
     inherited;
     with ParameterList do
     begin
          AddString( 'Codigo', UpperCase(txtCodigo.Text) );
          AddString( 'Descripcion', txtDescripcion.Text );
          AddInteger( 'Tipo', ord (tcRecluta));
          AddBoolean('DefinirEstructura', NOT FTieneEstructura);
          AddBoolean('ValoresDefault', TRUE);
          AddBoolean('NuevaBD', rbBDNueva.Checked);

          if rbBDNueva.Checked then
          begin
               AddString( 'NombreBDSQL', Trim(txtBaseDatos.Text));
               AddString( 'BaseDatos', Trim(txtBaseDatos.Text) );
               AddString( 'Usuario', Trim(txtUsuario.Text) );
               AddString( 'Clave', ZetaServerTools.Encrypt(txtClave.Text));
               AddInteger ('MaxSize', Round ((((tbEmpleados.Position*2048)/1024) + ((tbEmpleados.Position*tbDocumentos.Position*100)/1024))));
          end
          else
          begin
               AddString( 'BaseDatos', cbBasesDatos.Text );
               AddString( 'Ubicacion', cbBasesDatos.Text );
               AddString( 'Usuario', VACIO );
               AddString( 'Clave', VACIO);
          end;
    end;

    Descripciones.Clear;
    with Descripciones do
     begin
          if rbBDNueva.Checked then
          begin
               CreaParametroTamanio;
               AddString( 'Crear Base Datos nueva', Trim(txtBaseDatos.Text));
               AddString( 'Ubicaci�n', Trim(lblDatoUbicacion.Caption) );
                AddString( 'Tama�o de archivo de datos' , tamanio );
                AddString( 'Tama�o de archivo log' , log );
               if (rbSameUser.Checked) then
                   AddBoolean( 'Usuario y clave default', rbSameUser.Checked );
               if ( rbOtroUser.Checked ) then
               begin
                    AddBoolean( 'Otro usuario/clave acceso' , rbOtroUser.Checked );
                    AddString( 'Usuario', Trim(txtUsuario.Text) );
                end
          end
          else
          begin
               AddString( 'Base de Datos existente', cbBasesDatos.Text );
               AddString( 'Ubicacion', dmSistema.GetServer + '.' + cbBasesDatos.Text );
               AddBoolean( 'Usuario y clave default', rbSameUser.Checked );
          end;

          AddString( 'C�digo', UpperCase(txtCodigo.Text) );
          AddString( 'Descripci�n', txtDescripcion.Text );
          AddString( 'Datos iniciales', 'Valores default' );

    end;

end;

procedure TWizAgregarBaseDatosSeleccion_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
const K_ESPACIO = ' ';  
var mensaje : WideString;
begin
     ParametrosControl := nil;  //DevEx
     mensaje := VACIO;
     if Wizard.Adelante then
     begin
          if WizardControl.ActivePage = Parametros then
          begin
              if rbBDNueva.Checked then
              begin
                   if Trim (txtBaseDatos.Text) = VACIO then
                      CanMove := Warning ('El campo nombre de base de datos no puede quedar vac�o.', txtBaseDatos)
                   else if ExisteBD then
                      CanMove := Warning (Format('La base de datos: %s ya existe. Proporcione otro nombre.',[ Trim (txtBaseDatos.Text)]), txtBaseDatos)
                   else if Pos (K_ESPACIO, txtBaseDatos.Text) > 0 then
                        CanMove := Warning ('El nombre de la base de datos no puede contener espacios', txtBaseDatos)
                   else
                   begin
                       iNewPage := NuevaBD1.PageIndex;
                        CanMove := TRUE;
                   end;
              end
              else if rbBDExistente.Checked then
              begin
                   if (cbBasesDatos.ItemIndex >= 0) then
                   begin
                        // Es Base de Datos ya agregada a DB_INFO
                        if (dmSistema.EsBaseDatosAgregada(dmSistema.GetServer + '.' + cbBasesDatos.Text)) then
                        begin
                             CanMove := Warning (Format ('La base de datos ''%s'' ya se encuentra agregada al sistema.', [cbBasesDatos.Text]), cbBasesDatos)
                        end
                        else
                        begin
                             // Averiguar si la Base de Datos seleccionada no es de Tress o Visitantes
                             if (dmSistema.NoEsBDTressVisitantes(cbBasesDatos.Text)) then
                             begin
                                 // Averiguar si la Base de Datos seleccionada tiene la estructura de Seleccion (Tabla SOLICITA)
                                 if (dmSistema.EsBaseDatosSeleccion(cbBasesDatos.Text)) then  //SoloSeGuarda
                                 begin
                                      FTieneEstructura := TRUE;
                                      //NuevaBD1.Enabled :=FALSE;
                                      //NuevaBD2.Enabled :=FALSE;
                                      iNewPage := Estructura.PageIndex;     //DevEx
                                      CanMove := TRUE;
                                 end
                                 else
                                 begin
                                      FTieneEstructura := FALSE;
                                      //NuevaBD1.Enabled :=FALSE;
                                      //NuevaBD2.Enabled :=FALSE;
                                      iNewPage := Estructura.PageIndex;     //DevEx
                                      CanMove := TRUE;
                                 end;
                             end
                             else
                             begin
                                  CanMove := Warning ('La base de datos seleccionada no puede configurarse.' + CR_LF + 'Tiene definida una estructura diferente a Selecci�n.', cbBasesDatos);
                             end;
                        end;
                  end
                  else
                      CanMove := Warning ('Seleccionar una base de datos para continuar.', cbBasesDatos);
              end;
        end    //if parametros

        else if (WizardControl.ActivePage = NuevaBD1) then
           iNewPage := NuevaBD2.PageIndex     //DevEx

        else if (WizardControl.ActivePage = NuevaBD2) then
        begin
             if rbOtroUser.Checked then
             begin
                  if not ProbarConexion(mensaje) then
                  begin
                       CanMove := Warning (mensaje, txtUsuario);
                  end;
              end;
              iNewPage := Estructura.PageIndex;     //DevEx
        end   //if nuevabd2
        else if(WizardControl.ActivePage = Estructura) then
          begin
               if (trim (txtCodigo.Text) = VACIO) then
               begin
                    CanMove := Warning ('El campo C�digo no puede quedar vac�o.', txtCodigo);
               end
               else if (Trim (txtDescripcion.Text) = VACIO) then
               begin
                    CanMove := Warning ('El campo Descripci�n no puede quedar vac�o.', txtDescripcion);
               end
               else if not (dmSistema.ExisteBD_DBINFO(trim(txtCodigo.Text))) then
               begin
                   CanMove := TRUE;
                   Ejecucion.Enabled := TRUE;
              end
              else
              begin
                    CanMove := Warning (Format ('El c�digo  ''%s'' ya se encuentra agregado al sistema.', [txtCodigo.Text]), txtCodigo)
              end;
              iNewPage := Ejecucion.PageIndex;     //DevEx
          end;   //if estrcutura
    end;  //if adelante

    //hacia atras
    if ( not Wizard.Adelante )then
    begin
       if ( WizardControl.ActivePage = Ejecucion ) then
          iNewPage := Estructura.PageIndex;
       if ( WizardControl.ActivePage = Estructura ) then
       begin
            if ( rbBDNueva.Checked ) then
               iNewPage := NuevaBD2.PageIndex
            else
                iNewPage := Parametros.PageIndex
       end;
       if ( WizardControl.ActivePage = NuevaBD2 ) then
          iNewPage := NuevaBD1.PageIndex;
       if ( WizardControl.ActivePage = NuevaBD1 ) then
          iNewPage := Parametros.PageIndex;
    end;

end;
function TWizAgregarBaseDatosSeleccion_DevEx.ProcesarBD: Boolean;
var
   lOk : Boolean;
begin
     lOk := FALSE;
     try
     //se crea estructura
     cxMemoPasos.Lines.Append('Definiendo estructura... ');
     lOk := dmSistema.CrearEstructuraBD(ParameterList);
     SetStatus(lOk);
     if lOk  then
     begin
         cxMemoPasos.Lines.Append('Valores iniciales... ');
         lOk := dmSistema.CreaInicialesTabla(ParameterList);
         SetStatus(lOk);
     end;
     if lOk then
     begin
          cxMemoPasos.Lines.Append('Diccionario de datos... ');
          lOk := dmSistema.CreaDiccionarioTabla(ParameterList);
          SetStatus(lOk);
     end;

     if lOk then
     begin
          cxMemoPasos.Lines.Append('Registrando base de datos en sistema... ');
           lOk := dmSistema.GrabaDB_INFO(ParameterList.VarValues);
          SetStatus(lOk);
     end;

     if lOk then
     begin
          cxMemoPasos.Lines.Append('Proceso terminado.');
          ZInformation(Caption, 'La base de datos fue agregada correctamente.', 0);
          lOk := TRUE;
     end
     else
     begin
          cxMemoPasos.Lines.Append('Proceso con errores.');
          Anterior.Enabled := FALSE;
          Ejecutar.Enabled := FALSE;
          Salir.Enabled := TRUE;
          Siguiente.Enabled := FALSE;
          ZError(Caption, 'Error al agregar base de datos.', 0);
          lOk := TRUE;
     end;
     except
           on Error: Exception do
           begin
               SetStatus(lOk);
               cxMemoPasos.Lines.Append('Proceso con errores.');

               if (  Pos( 'PRIMARY KEY', UpperCase( Error.Message )) > 0) then
               begin
                  ZError (Caption, Format('Error al agregar base de datos:' + CR_LF + 'El c�digo de base de datos: %s ya se encuentra dado de alta',[ txtCodigo.Text]), 0);
               end
               else if (  Pos( 'ALREADY EXISTS', UpperCase( Error.Message )) > 0) then
               begin
                  ZError (Caption, Format('Error al agregar base de datos:' + CR_LF + 'Base de datos: %s ya existe. Proporcione otro nombre.',[ Trim (txtBaseDatos.Text)]), 0);
               end
               else
               begin
                   ZError(Caption, 'Error al agregar base de datos:' + CR_LF + Error.Message, 0);
               end;
           end;
     end;
     Result := lOk;

end;

function TWizAgregarBaseDatosSeleccion_DevEx.EjecutarWizard: Boolean;
var
   lOk : Boolean;
begin
     GrupoAvance.Visible := True;
     cxMemoPasos.Lines.Clear;
     cxMemoStatus.Lines.Clear;
     if rbBDNueva.Checked then
     begin
          Advertencia.Caption := 'Se proceder� a preparar la base de datos de tipo Selecci�n de Personal.';
          cxMemoPasos.Lines.Append('Creando base de datos... ');
          lOk := dmSistema.CrearBDEmpleados(ParameterList);
          SetStatus(lOk);
          if lOk then
          begin
               ProcesarBD;
          end;
     end
     else if rbBDExistente.Checked then
     begin
          Advertencia.Caption := 'Se proceder� a preparar la base de datos de tipo Selecci�n de Personal.';
          if NOT FTieneEstructura then
          begin
               ProcesarBD;
          end
          else
          begin
               lOk := dmSistema.GrabaDB_INFO(ParameterList.VarValues);
               if lOk then
               begin
                    ZInformation(Caption, 'La base de datos fue agregada correctamente.', 0);
               end
               else
               begin
                    ZError(Caption, 'Error al agregar base de datos.', 0);
               end;
          end;
     end;
     Result := TRUE;
end;

procedure TWizAgregarBaseDatosSeleccion_DevEx.WizardAfterMove(Sender: TObject);
Const K_2_MB = 2048;
      K_1024 = 1024;
      K_KB_DOC = 100;
begin
     //DevEx
     if Wizard.EsPaginaActual(Parametros) then
          ParametrosControl := txtBaseDatos
     else
         ParametrosControl := nil;
     inherited;

     if WizardControl.ActivePage = NuevaBD1 then
     begin
          CalcularTamanyos;
          lblCantEmpSeleccion.Caption := IntToStr(tbEmpleados.Position);
          lblDocSeleccion.Caption := IntToStr(tbDocumentos.Position);
     end
  {   else if WizardControl.ActivePage = NuevaBD2 then
     begin
          //*txtUbicacion.Text := dmSistema.GetServer + '.' + Trim (txtBaseDatos.Text);
          lblDatoUbicacion.Caption := dmSistema.GetServer + '.' + Trim (txtBaseDatos.Text);
     end
     } //se actualiza en la base
     else if WizardControl.ActivePage = Ejecucion then
     begin
          //*Advertencia.Lines.Clear;
          cxMemoPasos.Lines.Clear;
          cxMemoStatus.Clear;
          Advertencia.Caption := 'Se proceder� a preparar la base de datos de tipo Selecci�n de Personal.'; 
     end;

end;
procedure TWizAgregarBaseDatosSeleccion_DevEx.txtCodigoExit(
  Sender: TObject);
begin
      txtCodigo.Text :=  Copy(txtCodigo.Text, 0, 10);
end;

end.
