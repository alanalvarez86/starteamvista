inherited NomImportarPagoRecibosGridShow_DevEx: TNomImportarPagoRecibosGridShow_DevEx
  Left = 204
  Top = 186
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Lista de Errores Detectados'
  ClientWidth = 567
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 567
    inherited OK_DevEx: TcxButton
      Left = 403
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 482
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 567
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object RENGLON: TcxGridDBColumn
        Caption = 'Rengl'#243'n'
        DataBinding.FieldName = 'RENGLON'
      end
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
      end
      object CB_DIGITO: TcxGridDBColumn
        Caption = 'Empresa'
        DataBinding.FieldName = 'CB_DIGITO'
      end
      object PE_TIPO: TcxGridDBColumn
        Caption = 'Tipo Periodo'
        DataBinding.FieldName = 'PE_TIPO'
      end
      object PE_NUMERO: TcxGridDBColumn
        Caption = 'Periodo'
        DataBinding.FieldName = 'PE_NUMERO'
      end
      object NUM_ERROR: TcxGridDBColumn
        Caption = 'Errores'
        DataBinding.FieldName = 'NUM_ERROR'
      end
      object DESC_ERROR: TcxGridDBColumn
        Caption = 'Descripci'#243'n de Errores'
        DataBinding.FieldName = 'DESC_ERROR'
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
