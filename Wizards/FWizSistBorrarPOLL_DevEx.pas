unit FWizSistBorrarPOLL_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls, Mask,
     ZetaFecha, ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxContainer, cxEdit, ZetaCXWizard,
     dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
     dxWizardControl, cxRadioGroup;

type
  TWizSistBorrarPoll_DevEx = class(TcxBaseWizard)
    Opciones: TcxRadioGroup;
    FechasGB: TcxGroupBox;
    FechaInicialLBL: TLabel;
    FechaFinalLBL: TLabel;
    FechaInicial: TZetaFecha;
    FechaFinal: TZetaFecha;
    procedure FormCreate(Sender: TObject);
    procedure OpcionesClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure EnableControls;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizSistBorrarPoll_DevEx: TWizSistBorrarPoll_DevEx;

implementation


uses DCliente,
     DProcesos,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo, ZcxWizardBasico;

{$R *.DFM}

procedure TWizSistBorrarPoll_DevEx.FormCreate(Sender: TObject);
begin
     //ParametrosControl := Opciones;
     with dmCliente do
     begin
          FechaInicial.Valor := FechaDefault;
          FechaFinal.Valor := FechaDefault;
     end;
     inherited;
     HelpContext := H80836_Borrar_Poll;
     EnableControls;

     //DevEx
     Parametros.PageIndex := 0;
     Ejecucion.PageIndex := 1;
end;

procedure TWizSistBorrarPoll_DevEx.EnableControls;
var
   lEnabled: Boolean;
begin
     lEnabled := ( Opciones.ItemIndex > 0 );
     FechasGB.Enabled := lEnabled;
     FechaInicial.Enabled := lEnabled;
     FechaInicialLBL.Enabled := lEnabled;
     FechaFinal.Enabled := lEnabled;
     FechaFinalLBL.Enabled := lEnabled;
end;

procedure TWizSistBorrarPoll_DevEx.OpcionesClick(Sender: TObject);
begin
     inherited;
     EnableControls;
end;

procedure TWizSistBorrarPoll_DevEx.CargaParametros;
begin
     with ParameterList do
     begin
          AddInteger( 'Operacion', Opciones.ItemIndex );
          AddDate( 'FechaInicial', FechaInicial.Valor );
          AddDate( 'FechaFinal', FechaFinal.Valor );
     end;
     //DevEx
     with Descripciones do
     begin
          AddString( 'Registros a borrar', String ( Opciones.Properties.Items.Items [ Opciones.ItemIndex  ].Value)  );
          if ( Opciones.ItemIndex = 1  ) then
          begin
                AddDate( 'Fecha inicial', FechaInicial.Valor );
                AddDate( 'Fecha final', FechaFinal.Valor );
          end;

     end;
end;

function TWizSistBorrarPoll_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.BorrarPoll( ParameterList );
end;

procedure TWizSistBorrarPoll_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption :=
     'Al aplicar el proceso se eliminarán las tarjetas de reloj pendientes en las fechas indicadas.';
end;

end.



