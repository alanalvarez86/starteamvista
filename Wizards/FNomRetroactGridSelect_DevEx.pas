unit FNomRetroactGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseSelectGrid_DevEx, Db, StdCtrls,
  ExtCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, cxControls, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
  cxButtons;

type
  TGridCalculaRetoactivos_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CB_SALARIO: TcxGridDBColumn;
    CB_SAL_INT: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GridCalculaRetoactivos_DevEx: TGridCalculaRetoactivos_DevEx;

implementation

{$R *.DFM}

procedure TGridCalculaRetoactivos_DevEx.FormShow(Sender: TObject);
begin

     with DataSet do
     begin
          MaskPesos( 'CB_SALARIO' );
          MaskPesos( 'CB_SAL_INT' );
     end;
     inherited;
end;

end.
