unit FEmpImportarPrestamosGridShow_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, ExtCtrls,
  ZBaseGridShow_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons, cxCalendar;

type
  TEmpImportarPrestamosGridShow_DevEx = class(TBaseGridShow_DevEx)
    RENGLON: TcxGridDBColumn;
    CB_CODIGO: TcxGridDBColumn;
    PR_TIPO: TcxGridDBColumn;
    PR_REFEREN: TcxGridDBColumn;
    PR_FECHA: TcxGridDBColumn;
    PR_MONTO: TcxGridDBColumn;
    NUM_ERROR: TcxGridDBColumn;
    DESC_ERROR: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpImportarPrestamosGridShow_DevEx: TEmpImportarPrestamosGridShow_DevEx;

implementation

{$R *.DFM}

procedure TEmpImportarPrestamosGridShow_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     DataSet.MaskPesos( 'PR_MONTO' );
end;

end.
