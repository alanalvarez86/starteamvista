unit FEmpleadoGridSelectPermisoGlobal_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ExtCtrls, Db, ZGlobalTress, DGlobal,
     ZBaseSelectGrid_DevEx, cxGraphics,
     cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
      TressMorado2013, dxSkinsDefaultPainters,
     cxControls, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
     cxDataStorage, cxEdit, cxNavigator, cxDBData, ImgList, cxGridLevel,
     cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
     cxGridDBTableView, cxGrid, ZetaCXGrid, cxButtons, cxTextEdit,
     cxCalendar;

type
  TEmpleadoGridSelectPermisoGlobal_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    PM_FEC_INI: TcxGridDBColumn;
    PM_DIAS: TcxGridDBColumn;
    PM_FEC_FIN: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpleadoGridSelectPermisoGlobal_DevEx: TEmpleadoGridSelectPermisoGlobal_DevEx;

implementation

uses ZBasicoSelectGrid_DevEx;


{$R *.DFM}

procedure TEmpleadoGridSelectPermisoGlobal_DevEx.FormShow(Sender: TObject);
var bPermisoGlobalDiasHabiles : boolean;
begin
  inherited;
  bPermisoGlobalDiasHabiles := Global.GetGlobalBooleano(K_GLOBAL_PERMISOS_DIAS_HABILES);
  //ZetaDBGrid.Columns[2].Visible := bPermisoGlobalDiasHabiles;
  //ZetaDBGrid.Columns[3].Visible := bPermisoGlobalDiasHabiles;
  //ZetaDBGrid.Columns[4].Visible := bPermisoGlobalDiasHabiles;
  //devex
  ZetaDBGridDBTableView.Columns[2].Visible := bPermisoGlobalDiasHabiles;
  ZetaDBGridDBTableView.Columns[3].Visible := bPermisoGlobalDiasHabiles;
  ZetaDBGridDBTableView.Columns[4].Visible := bPermisoGlobalDiasHabiles;

end;

end.
