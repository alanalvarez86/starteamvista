inherited WizAsistRegistrarExcepcionesFestivo_DevEx: TWizAsistRegistrarExcepcionesFestivo_DevEx
  Left = 459
  Top = 159
  Caption = 'Registro Excepciones de Festivos'
  ClientHeight = 427
  ClientWidth = 423
  ExplicitWidth = 429
  ExplicitHeight = 456
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 423
    Height = 427
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EBC00000EBC0195BC
      72490000040C494441545847C557494C144114E56094834671815150D091805B
      C025D1831A0F1E3C70F0608C070F9298E8C1188D0AA30E16030466BA67C47D89
      07BD7920D1840B070E1A4D34C41817446318511C05590C21A88808ED7F65D558
      74578F4B067CC9CB7455FFFAAFEAFFAADF35299665FD576A3B279249817579F5
      18B6FAD2B3DFB3B9FEDEEA9C8681485EAC2FE46DEA0864996D4767AFB5DB2605
      AAC3F7E59981918B2B47E9D9D2B1AB2ABBFEDEFE8C547A4EFE043E06BD4D1001
      074F2FFBF2B166E1EDCE40A64991A81F38B9A457BEFB7432BF3F5A32230D6392
      02387A579E1580F3D14BAB2C12BCF174DFE4F82A25DF96794A86CF178CC08E6C
      3059E1E137608CA51ACCD84C3C1C0998574D16F29BCCDC847EBC6F2EF164CBB0
      F7542FBC01C76F8EA71751CEA7E15965FBF13945F4CB23F1D69F5ECC051221C2
      82EBCC80F1221C302D0799E1834D8C65FAE110617FB26F0A5F39ADB0F1CBA9A5
      FDD1D299F968AB14ABE751E0226EA095EE25A16187B0240B6D855D77B5B7010E
      917355A8AB72FEB5EF170A47DAFC9EED6A7FAC2CE320FDF2BDC0857408B3703E
      890C3A4455920D6CFB234B6370D8513ECF5485C0767FC6CE914BABADE7A59E74
      D9D7E69B89E3C8D3C0C574A0B0DFD58AAA64E1F5B0ED09E6CA90D64B111BADA8
      6F8E57B62902C5E8FB7A66F91017B3A396D5CED00ADA489B7227ECA315795839
      42DA25456CB45A7C59F10974572DC046B5A840B570413BB0E375824E1A75B0BF
      5BB6311ED2F61373F7482185D663DF623E013AFF5E7962A85A9A5CD00EDADDBB
      F4820E0ED2643D70DC5593CB37E2B7F385DF5FFA7370D4E21378E95FC4DBAD47
      A6675369EE801DFDF63EDB9F3A49488E458885D668C45C68D4C179C3A182D4CF
      74ECE01CC5A8BBC6DBD87E22B338EA9BE5A58D584415F18A2C427432465F95A6
      ADC0382D5060C8B9FBF1B391A27000CEEE1CDB90D61DCA7B081137D23EE995E2
      A02B50F174626E44CD904E9F94AF29FE10CC6B1AA85DC623327476C550BF99DB
      82EF02C22EED40579C63E7A692E3D77621170E236D6268F220F642C24950BD18
      A0EFC2363124F9E091A830CE92509F4D7CD80C9837C765E56E88B0C862AC96D7
      09169E2DBAC70F2454088AA62BF05504514145D7BF838EE0241C2B0A79CFCF50
      1BD7C5AB94200B66E11E20EE02F12242B6ADB0459A3056DE13FE1AC837E5F5BE
      926730265EF314509BBE90C62DD145DF8D9A1C9B3DD16886AD30F933604570EC
      74665A10116629E18A70252E29A229EF0C8E31C44E444C98258610AFD338E1A4
      90EF10A624686E53739D681CF1819A2A57D0CD66B766F02F56842F0A5312341B
      29CF87F10CE79AE33996F461E303DD800D43869D8E8163F908B64ABE5F431CA9
      506CB4C406E5426EF8EDEA0569D55B1009A57D406D27A4B8BA694139BC4E4628
      B7E346750F39A0FBC33891D4764E1CAD941F494657DDDCD9B26B000000004945
      4E44AE426082}
    ExplicitWidth = 423
    ExplicitHeight = 427
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso para registro excepciones de d'#237'as festivos en el calenda' +
        'rio del empleado.'
      Header.Title = 'Registrar Excepciones de Festivos'
      ExplicitWidth = 401
      ExplicitHeight = 287
      object GroupBox1: TGroupBox
        Left = 80
        Top = 69
        Width = 249
        Height = 65
        Caption = ' Asignaci'#243'n de Festivo '
        Color = clWhite
        ParentColor = False
        TabOrder = 0
        object Label1: TLabel
          Left = 16
          Top = 29
          Width = 84
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha de &Tarjeta:'
          FocusControl = FechaTarjeta
        end
        object FechaTarjeta: TZetaFecha
          Left = 102
          Top = 25
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '19/Jan/98'
          Valor = 35814.000000000000000000
        end
      end
      object ckFestivoTrabajado: TCheckBox
        Left = 89
        Top = 162
        Width = 238
        Height = 17
        Caption = 'Asignar horario de festivo trabajado del turno'
        Checked = True
        State = cbChecked
        TabOrder = 1
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      ExplicitWidth = 401
      ExplicitHeight = 287
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 401
        ExplicitHeight = 192
        Height = 192
        Width = 401
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 401
        Width = 401
        inherited Advertencia: TcxLabel
          Caption = 
            'Al aplicar el proceso se registrar'#225'n las excepciones de festivos' +
            ' en el periodo indicado a los empleados seleccionados.'
          Style.IsFontAssigned = True
          ExplicitLeft = 67
          ExplicitTop = 20
          ExplicitWidth = 333
          ExplicitHeight = 74
          Width = 333
          AnchorY = 57
        end
        inherited cxImage1: TcxImage
          ExplicitLeft = 1
          ExplicitTop = 20
          ExplicitHeight = 74
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      ExplicitWidth = 401
      ExplicitHeight = 287
      inherited sCondicionLBl: TLabel
        Left = 0
        Top = 143
        ExplicitLeft = 0
        ExplicitTop = 143
      end
      inherited sFiltroLBL: TLabel
        Left = 25
        Top = 166
        ExplicitLeft = 25
        ExplicitTop = 166
      end
      inherited Seleccionar: TcxButton
        Left = 128
        Top = 260
        Visible = True
        ExplicitLeft = 128
        ExplicitTop = 260
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 52
        Top = 140
        ExplicitLeft = 52
        ExplicitTop = 140
      end
      inherited sFiltro: TcxMemo
        Left = 52
        Top = 166
        Style.IsFontAssigned = True
        ExplicitLeft = 52
        ExplicitTop = 166
      end
      inherited GBRango: TGroupBox
        Left = 52
        Top = 10
        ExplicitLeft = 52
        ExplicitTop = 10
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
  end
end
