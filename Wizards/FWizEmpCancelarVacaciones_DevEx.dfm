inherited WizEmpCancelarVacaciones_DevEx: TWizEmpCancelarVacaciones_DevEx
  Left = 257
  Top = 210
  Caption = 'Cancelar Vacaciones'
  ClientHeight = 431
  ClientWidth = 425
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 425
    Height = 431
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Este proceso cancela o borra los movimientos de vacaciones a un ' +
        'grupo de empleados que cumplan con una condici'#243'n.'
      Header.Title = 'Cancelar vacaciones'
      object TipoMovimientoLBL: TLabel
        Left = 61
        Top = 145
        Width = 108
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cancelar &Modificaci'#243'n:'
        Transparent = True
      end
      object TipoFechaLBL: TLabel
        Left = 97
        Top = 119
        Width = 72
        Height = 13
        Alignment = taRightJustify
        Caption = '&Tipo de Fecha:'
        Transparent = True
      end
      object FechaLBL: TLabel
        Left = 66
        Top = 93
        Width = 103
        Height = 13
        Alignment = taRightJustify
        Caption = '&Fecha de Referencia:'
        FocusControl = Fecha
        Transparent = True
      end
      object TipoMovimiento: TZetaDBKeyCombo
        Left = 171
        Top = 141
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 2
        ListaFija = lfCancelaMod
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        LlaveNumerica = True
      end
      object TipoFecha: TZetaDBKeyCombo
        Left = 171
        Top = 115
        Width = 170
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 1
        ListaFija = lfTipoKarFecha
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        LlaveNumerica = True
      end
      object Fecha: TZetaFecha
        Left = 171
        Top = 88
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '02/ene/98'
        Valor = 35797.000000000000000000
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 196
        Width = 403
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 403
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 335
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 6
        Top = 145
      end
      inherited sFiltroLBL: TLabel
        Left = 31
        Top = 168
      end
      inherited Seleccionar: TcxButton
        Left = 134
        Top = 262
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 58
        Top = 142
      end
      inherited sFiltro: TcxMemo
        Left = 58
        Top = 168
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 58
        Top = 12
      end
      inherited bAjusteISR: TcxButton
        Left = 372
        Top = 169
      end
    end
  end
end
