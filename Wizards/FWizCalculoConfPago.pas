unit FWizCalculoConfPago;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, TressMorado2013, cxContainer, cxEdit, ZetaCXWizard,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl, Vcl.Menus, Vcl.ExtCtrls, ZetaKeyLookup_DevEx, Vcl.StdCtrls,
  cxTextEdit, cxButtons, cxProgressBar, ZetaKeyCombo, Vcl.Mask, ZetaNumero,
  cxDropDownEdit, ZetaCXStateComboBox, cxMaskEdit, cxSpinEdit,
  FWizNomBasico_DevEx, ZetaEdit, cxRadioGroup, cxMemo, cxCheckBox, ZetaDBTextBox, DB,
  ZetaKeyLookup;

type
  TWizCalculoConfPago = class(TcxBaseWizard)
    gbArchivoCedula: TcxGroupBox;
    cxLabel1: TcxLabel;
    btnCedulaFonacot: TcxButton;
    ArchivoCedulaFonacot: TcxTextEdit;
    DialogoCedula: TOpenDialog;
    PanelProgressBar: TPanel;
    ProgressBar: TcxProgressBar;
    lblAnio: TcxLabel;
    lblMes: TcxLabel;
    txtAnio: TcxSpinEdit;
    zcboMes: TcxStateComboBox;
    lbRazonsocial: TLabel;
    gbIncapacidades: TGroupBox;
    chbIgnorarIncapacidades: TcxCheckBox;
    lblTipoCedula: TLabel;
    tbTipoCedulaDesc: TZetaTextBox;
    ListaRSCedulasImportadasCB_DevEx: TZetaKeyCombo;
    Button1: TButton;
    procedure btnCedulaFonacotClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure txtAnioClick(Sender: TObject);
    procedure zcboMesClick(Sender: TObject);
    procedure RS_CODIGOValidKey(Sender: TObject);
    procedure ListaRSCedulasImportadasCB_DevExSelect(Sender: TObject);
    procedure Button1Click(Sender: TObject);

  private
    { Private declarations }
    iTipoCedula : Integer;
    procedure HabilitaFiltroRazonSocial;
    procedure TipoCedula;
    procedure LlenaListaCampos( DataSet: TDataSet );
    procedure CargarComboRSCedulas( const sYear: string; const sMonth: string);
    procedure LimpiarComboRSCedulas;
  protected
    function EjecutarWizard: Boolean;override;
    procedure CargaParametros;override;
    procedure Connect;
  public
    { Public declarations }
  end;

var
  WizCalculoConfPago: TWizCalculoConfPago;

  sAnioFonacot, sMesFonacot : string;

implementation

{$R *.dfm}

uses
    ZetaCommonTools, ZetaDialogo, FTressShell, DCliente,
    ZetaClientTools, ZetaCommonLists,
    ZetaCommonClasses,
    DCatalogos, DProcesos,
    DNomina;

procedure TWizCalculoConfPago.btnCedulaFonacotClick(Sender: TObject);
var
   sNombreCedulaPago: String;
begin
     inherited;

     sNombreCedulaPago := 'Cedula_' + ObtieneElemento( lfMeses, zcboMes.Indice - 1 ) + '_Pago.csv';
     ArchivoCedulaFonacot.Text := sNombreCedulaPago;
     ArchivoCedulaFonacot.Text := AbreDialogo( DialogoCedula, ArchivoCedulaFonacot.Text,'csv' );
end;

procedure TWizCalculoConfPago.WizardAfterMove(Sender: TObject);
begin
     inherited;
     if WizardControl.ActivePage = Parametros then
        ActiveControl := btnCedulaFonacot;
end;

procedure TWizCalculoConfPago.WizardAlCancelar(Sender: TObject;
  var lOk: Boolean);
begin
     inherited;
     // dmCliente.CancelaConciliacion := lOk;
end;

procedure TWizCalculoConfPago.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros ) then
            begin
                 if iTipoCedula < 0 then
                 begin
                      if StrLleno(ListaRSCedulasImportadasCB_DevEx.llave) then
                         ZetaDialogo.ZError( Caption, 'No existe c�dula de Fonacot importada para esa Raz�n Social y mes correspondiente' , 0 )
                      else
                          ZetaDialogo.ZError( Caption, 'No existe c�dula de Fonacot importada para el mes correspondiente' , 0 );

                      CanMove := False;
                 end
                 else if ( StrVacio (ArchivoCedulaFonacot.Text) ) then
                 begin
                      ZetaDialogo.ZError( 'Generar c�dula Fonacot', 'El nombre de archivo de c�dula de pago est� vac�o', 0 );
                      CanMove := False;
                 end
                 else if FileExists(ArchivoCedulaFonacot.Text) then
                 begin
                      CanMove := ZConfirm('Generar c�dula Fonacot','El archivo de c�dula de Fonacot ya existe, �Desea reemplazarlo?',0,mbYes);
                 end;
            end;
     end;
end;

procedure TWizCalculoConfPago.zcboMesClick(Sender: TObject);
begin
     inherited;
     CargarComboRSCedulas(sAnioFonacot,sMesFonacot);
     TipoCedula;// ESTE METODO SIEMPRE VA DESPUES DE LA CARGA DE RAZONES SOCIALES DEL COMBO
end;

function TWizCalculoConfPago.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.GenerarCedulaFonacot (ParameterList);
end;

procedure TWizCalculoConfPago.CargarComboRSCedulas( const sYear: string; const sMonth: string);
var
   iConteoRS : integer;
begin
      try
           //dmCatalogos.cdsRSCedulasImportadas.Conectar;
           sAnioFonacot := txtAnio.Value;
           sMesFonacot := IntToStr(zcboMes.Indice);
           if ((sAnioFonacot <>  VACIO) and (sMesFonacot <>  VACIO)) then
           begin
                 ListaRSCedulasImportadasCB_DevEx.Enabled := true;
                 dmCatalogos.GetDatosRSCedulasImportadas( sAnioFonacot, sMesFonacot );
                 iConteoRS := dmCatalogos.cdsRSCedulasImportadas.RecordCount;
                 if iConteoRS > 0 then
                 begin
                      LlenaListaCampos( dmCatalogos.cdsRSCedulasImportadas );
                      ListaRSCedulasImportadasCB_DevEx.ItemIndex := 0;
                 end
                 else
                 begin
                       ListaRSCedulasImportadasCB_DevEx.Enabled := false;
                       ListaRSCedulasImportadasCB_DevEx.Items.Clear;
                       ListaRSCedulasImportadasCB_DevEx.Clear;
                 end;
           end
           else
           begin
                ListaRSCedulasImportadasCB_DevEx.Enabled := false;
                ListaRSCedulasImportadasCB_DevEx.Items.Clear;
                ListaRSCedulasImportadasCB_DevEx.Clear;
           end;
      finally

      end;
end;

procedure TWizCalculoConfPago.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_WIZ_CALCULA_PAGO_FONACOT;

     with dmCatalogos.cdsRSocial do
     begin
          Conectar;
     end;

     LimpiarComboRSCedulas;
     CargarComboRSCedulas(sAnioFonacot,sMesFonacot);


     HabilitaFiltroRazonSocial;

     Parametros.PageIndex := 0;
     Ejecucion.PageIndex := 1;

end;

procedure TWizCalculoConfPago.ListaRSCedulasImportadasCB_DevExSelect(
  Sender: TObject);
  var
     sSeleccion, sDescripcion : string;

begin
  inherited;
        sSeleccion := VACIO;
        sDescripcion := VACIO;
        sSeleccion := ListaRSCedulasImportadasCB_DevEx.Llave;
        sDescripcion := ListaRSCedulasImportadasCB_DevEx.Text;
        TipoCedula; // ESTE METODO SIEMPRE VA DESPUES DE LA CARGA DE RAZONES SOCIALES DEL COMBO

end;

procedure TWizCalculoConfPago.LlenaListaCampos( DataSet: TDataSet );
var
   Pos : TBookMark;
   iContador : integer;
   sContador, sDescripcion : string;
begin
     ListaRSCedulasImportadasCB_DevEx.Items.Clear;
     with ListaRSCedulasImportadasCB_DevEx do
     begin
          Lista.BeginUpdate;
          Clear;
          ListaRSCedulasImportadasCB_DevEx.Lista.Clear;
     end;
     iContador := 1;
     sContador := IntToStr(iContador);
     with DataSet do
     begin
          DisableControls;
          try
             ListaRSCedulasImportadasCB_DevEx.Items.Clear;
             ListaRSCedulasImportadasCB_DevEx.Clear;

             Pos:= GetBookMark;
             First;
             while ( not EOF ) do
             begin
                 with ListaRSCedulasImportadasCB_DevEx.Lista do
                 begin
                      sDescripcion := VACIO;
                      if (FieldByName( 'CODIGO' ).AsString <> VACIO) then
                      BEGIN
                           sDescripcion := FieldByName( 'CODIGO'  ).AsString +' - '+ FieldByName( 'NOMBRE' ).AsString;
                           Add( FieldByName( 'CODIGO'  ).AsString +'='+ sDescripcion  )
                      END
                      ELSE
                      BEGIN
                           Add( sContador +'='+ 'Todas'  )
                      END;
                 end;
                 iContador := iContador + 1;
                 sContador := IntToStr(iContador);
                 Next;
             end;
             if ( Pos <> nil ) then
             begin
                  GotoBookMark( Pos );
                  FreeBookMark( Pos );
             end;
          finally
                EnableControls;
                ListaRSCedulasImportadasCB_DevEx.Lista.EndUpdate;
          end;
     end;
end;

procedure TWizCalculoConfPago.FormShow(Sender: TObject);
begin
     inherited;
     Advertencia.Caption := 'Al aplicar este proceso se generar� la c�dula de pago con los par�metros indicados.';

     with dmCliente do
     begin
          txtAnio.Value := GetDatosPeriodoActivo.Year;
          zcboMes.ItemIndex := GetDatosPeriodoActivo.Mes - 1;
          if zcboMes.ItemIndex = -1 then
             zcboMes.ItemIndex := ord(emEnero);
     end;

     ActiveControl := txtAnio;

     LimpiarComboRSCedulas;
     CargarComboRSCedulas(sAnioFonacot,sMesFonacot);
     TipoCedula; // ESTE METODO SIEMPRE VA DESPUES DE LA CARGA DE RAZONES SOCIALES DEL COMBO
end;

procedure TWizCalculoConfPago.Button1Click(Sender: TObject);
begin
  inherited;
           ListaRSCedulasImportadasCB_DevEx.Lista.Clear;
           TipoCedula;
end;

procedure TWizCalculoConfPago.LimpiarComboRSCedulas;
begin
     try
           ListaRSCedulasImportadasCB_DevEx.Items.Clear;
           ListaRSCedulasImportadasCB_DevEx.Clear;
     except
     end;
end;

procedure TWizCalculoConfPago.CargaParametros;
var
    sValueComboRSCedulas : string;
begin
         sValueComboRSCedulas := '-1';
         if (ListaRSCedulasImportadasCB_DevEx.Text = 'Todas') then
         begin
              sValueComboRSCedulas := VACIO;
         end
         else
         begin
              sValueComboRSCedulas := ListaRSCedulasImportadasCB_DevEx.Llave;
         end;

     with ParameterList do
     begin
          AddString ('CedulaPago', ArchivoCedulaFonacot.Text);
          AddInteger ('Year' , txtAnio.Value);
          AddInteger ('Mes' , zcboMes.Indice);
          AddString ('RazonSocial', sValueComboRSCedulas);
          AddBoolean ('IgnorarIncapacidades', chbIgnorarIncapacidades.Checked);

          AddInteger ('TipoCedula' , dmNomina.FonacotTipoCedula(txtAnio.Value, zcboMes.Indice, sValueComboRSCedulas));
     end;
     with Descripciones do
     begin
          AddString ('C�dula de pago', ArchivoCedulaFonacot.Text);
          AddInteger ('A�o' , txtAnio.Value);
          AddString ('Mes', ObtieneElemento( lfMeses, zcboMes.Indice - 1));
          AddString ('Raz�n social', sValueComboRSCedulas);
          AddBoolean ('Ignorar incapacidades', chbIgnorarIncapacidades.Checked);
          AddString ('Formato' , ObtieneElemento (lfTipoArchivoCedula, dmNomina.FonacotTipoCedula(txtAnio.Value, zcboMes.Indice, sValueComboRSCedulas)));
     end;
     inherited;
end;

procedure TWizCalculoConfPago.HabilitaFiltroRazonSocial;
var
   lHabilitarFiltro : Boolean;
begin
     lHabilitarFiltro := False;
     with dmCatalogos.cdsRSocial do
     begin
          lHabilitarFiltro := (RecordCount > 1)
     end;


     ListaRSCedulasImportadasCB_DevEx.Enabled := lHabilitarFiltro;
     lbRazonSocial.Enabled := lHabilitarFiltro;
end;

procedure TWizCalculoConfPago.Connect;
begin
     with dmCatalogos do
     begin
          cdsRSocial.Conectar;
     end;
end;

procedure TWizCalculoConfPago.RS_CODIGOValidKey(Sender: TObject);
begin
     inherited;
     TipoCedula;
end;

procedure TWizCalculoConfPago.TipoCedula;
var
    sValueComboRSCedulas : string;
begin
    try
         // Tipo de c�dula cargada
         sValueComboRSCedulas := '-1';

         if (ListaRSCedulasImportadasCB_DevEx.Text = 'Todas') then
         begin
              sValueComboRSCedulas := VACIO;
         end
         else
         begin
              sValueComboRSCedulas := ListaRSCedulasImportadasCB_DevEx.Llave;
         end;

         iTipoCedula := dmNomina.FonacotTipoCedula(txtAnio.Value, zcboMes.Indice, sValueComboRSCedulas);
         if iTipoCedula <> -1 then
         begin
             tbTipoCedulaDesc.Caption := ObtieneElemento (lfTipoArchivoCedula, iTipoCedula);
         end
         else
         begin
              tbTipoCedulaDesc.Caption := VACIO;
         end;

         if iTipoCedula < 0 then
            tbTipoCedulaDesc.Caption := VACIO;
    finally

    end;

end;

procedure TWizCalculoConfPago.txtAnioClick(Sender: TObject);
begin
     inherited;
     LimpiarComboRSCedulas;
     CargarComboRSCedulas(sAnioFonacot,sMesFonacot);
     TipoCedula; // ESTE METODO SIEMPRE VA DESPUES DE LA CARGA DE RAZONES SOCIALES DEL COMBO
end;

end.
