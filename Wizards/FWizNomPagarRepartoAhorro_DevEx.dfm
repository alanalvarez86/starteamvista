inherited WizNomPagarRepartoAhorro_DevEx: TWizNomPagarRepartoAhorro_DevEx
  Left = 204
  Top = 195
  Caption = 'Pagar Reparto de Fondo/Caja de Ahorros'
  ClientHeight = 417
  ClientWidth = 436
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 436
    Height = 417
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que utiliza los resultados del proceso "Calcular reparto' +
        ' de ahorros" para generar excepciones en la n'#243'mina  en la que se' +
        ' pagar'#225' el ahorro.'
      Header.Title = ' Pagar reparto de Fondo/Caja de Ahorros'
      inherited GroupBox1: TGroupBox
        Top = 89
        Color = clWhite
        ParentColor = False
        TabOrder = 1
      end
      object PanelParametros: TPanel
        Left = 0
        Top = 0
        Width = 414
        Height = 89
        Align = alTop
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 0
        object YearLBL: TLabel
          Left = 56
          Top = 16
          Width = 22
          Height = 13
          Caption = 'A&'#241'o:'
        end
        object ConceptoLBL: TLabel
          Left = 29
          Top = 64
          Width = 49
          Height = 13
          Alignment = taRightJustify
          Caption = 'C&oncepto:'
        end
        object TipoAhorroLBL: TLabel
          Left = 5
          Top = 40
          Width = 73
          Height = 13
          Caption = '&Tipo de Ahorro:'
        end
        object Year: TZetaNumero
          Left = 81
          Top = 12
          Width = 59
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
        end
        object Concepto: TZetaKeyLookup_DevEx
          Left = 81
          Top = 60
          Width = 300
          Height = 21
          Filtro = 'CO_NUMERO < 1000'
          LookupDataset = dmCatalogos.cdsConceptos
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 2
          TabStop = True
          WidthLlave = 60
        end
        object TipoAhorro: TZetaKeyLookup_DevEx
          Left = 81
          Top = 36
          Width = 300
          Height = 21
          LookupDataset = dmTablas.cdsTAhorro
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 60
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 182
        Width = 414
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 414
        inherited Advertencia: TcxLabel
          Caption = 
            'Al aplicar el proceso en la n'#243'mina activa se crear'#225'n excepciones' +
            ' de d'#237'as/horas para los empleados que se incluyeron en el c'#225'lcul' +
            'o.'
          Style.IsFontAssigned = True
          Width = 346
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 8
        Top = 133
      end
      inherited sFiltroLBL: TLabel
        Left = 33
        Top = 162
      end
      inherited Seleccionar: TcxButton
        Left = 136
        Top = 250
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 60
        Top = 130
      end
      inherited sFiltro: TcxMemo
        Left = 60
        Top = 161
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 60
        Top = 2
      end
      inherited bAjusteISR: TcxButton
        Left = 375
        Top = 162
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
  end
end
