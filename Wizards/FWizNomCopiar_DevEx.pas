unit FWizNomCopiar_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, Mask,
     ZetaEdit, ZetaNumero, ZetaKeyCombo, FWizNomBasico_DevEx,
     cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
     dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
     cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons,
     dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
     dxWizardControl;

type
  TWizNomCopiar_DevEx = class(TWizNomBasico_DevEx)
    TipoLBL: TLabel;
    TipoNomina: TZetaKeyCombo;
    NominaOriginal: TcxGroupBox;
    YearOriginalLBL: TLabel;
    NumeroOriginalLBL: TLabel;
    NominaNueva: TcxGroupBox;
    YearNuevoLBL: TLabel;
    NumeroNuevoLBL: TLabel;
    YearOriginal: TZetaNumero;
    NumeroOriginal: TZetaNumero;
    YearNuevo: TZetaNumero;
    NumeroNuevo: TZetaNumero;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    //DevEx Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros;override;
  public
    { Public declarations }
  end;

var
  WizNomCopiar_DevEx: TWizNomCopiar_DevEx;

implementation

uses DCliente,
     DProcesos,
     FTressShell,
     ZetaDialogo,
     ZetaCommonLists,
     ZetaCommonClasses;

{$R *.DFM}

procedure TWizNomCopiar_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     with dmCliente.GetDatosPeriodoActivo do
     begin
//          TipoNomina.ItemIndex := Ord( Tipo );
//          TipoNomina.Llave :=  IntToStr( Ord( Tipo ) );   //acl
          YearOriginal.Valor := Year;
          YearNuevo.Valor := Year;
          NumeroOriginal.Valor := Numero;
     end;
     ParametrosControl := TipoNomina;
     HelpContext := H33419_Copiar_nomina;

     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;
end;

procedure TWizNomCopiar_DevEx.CargaParametros;
begin
     inherited;
     with ParameterList do
     begin
          AddInteger( 'Tipo', TipoNomina.LlaveEntero); //acl
          AddInteger( 'YearOriginal' , YearOriginal.ValorEntero);
          AddInteger( 'NumeroOriginal',NumeroOriginal.ValorEntero);
          AddInteger( 'YearNuevo', YearNuevo.ValorEntero);
          AddInteger( 'NumeroNuevo', NumeroNuevo.ValorEntero);
          AddBoolean( 'CancelarNomina', False );
     end;
     //DevEx
     with Descripciones do
     begin
          AddString( 'Tipo de N�mina', ObtieneElemento( lfTipoPeriodo, TipoNomina.LlaveEntero ) );
          AddInteger( 'N�mina Original a�o' , YearOriginal.ValorEntero);
          AddInteger( 'N�mina Original n�mero',NumeroOriginal.ValorEntero);
          AddInteger( 'N�mina Nueva a�o', YearNuevo.ValorEntero);
          AddInteger( 'N�mina Nueva n�mero', NumeroNuevo.ValorEntero);
     end;
end;

procedure TWizNomCopiar_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     ParametrosControl := nil;
     inherited;
     with Wizard do
     begin
          if Adelante and EsPaginaActual( Parametros ) then
          begin
               if TipoNomina.ItemIndex > K_PERIODO_VACIO then
               begin
                    if ( YearOriginal.ValorEntero = YearNuevo.ValorEntero ) and ( NumeroOriginal.ValorEntero = NumeroNuevo.ValorEntero ) then
                    begin
                         zError( Caption, '� La N�mina Nueva debe ser diferente a la N�mina Original !', 0 );
                         CanMove := False;
                         ActiveControl := NumeroNuevo;
                    end
               end
               else
               begin
                    zError( Caption, 'El tipo de n�mina es invalido', 0 );
                    CanMove := False;
               end
          end;
     end;
end;

function TWizNomCopiar_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CopiarNomina(ParameterList);
end;

procedure TWizNomCopiar_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with  TipoNomina do
     begin
          ListaFija:=lfTipoPeriodoConfidencial; //acl
          LlaveEntero:= Ord( dmCliente.GetDatosPeriodoActivo.Tipo );
     end;

     Advertencia.Caption :=
        'Al aplicar el proceso se borrar�n todos los registros de la n�mina destino.';
end;


procedure TWizNomCopiar_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
          ParametrosControl := TipoNomina
     else
         ParametrosControl := nil;
     inherited;
end;

end.
