inherited AsistenciaGridSelect: TAsistenciaGridSelect
  Left = 215
  Top = 328
  Width = 600
  Caption = 'Seleccione las tarjetas de Asistencia'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 592
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 592
    Columns = <
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRETTYNAME'
        Title.Caption = 'Nombre'
        Width = 230
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AU_FECHA'
        Title.Caption = 'Fecha'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AU_TIPO'
        Title.Caption = 'Incidencia'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TB_ELEMENT'
        Title.Caption = 'Tipo Incapacidad'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'IN_FEC_INI'
        Title.Caption = 'Fecha Inicial'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'IN_FEC_FIN'
        Title.Caption = 'Fecha Final'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'IN_NUMERO'
        Title.Caption = '# de Incapacidad'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        Title.Caption = 'D'#237'a'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AU_TIPODIA'
        Title.Caption = 'Tipo'
        Width = 60
        Visible = True
      end>
  end
  inherited PanelSuperior: TPanel
    Width = 592
  end
end
