unit FWizEmpImportarAsistenciaSesiones_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Buttons, StdCtrls, ComCtrls, ExtCtrls,
     FWizEmpBaseFiltro,
     ZetaKeyCombo,
     ZetaWizard, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, ZetaEdit, cxRadioGroup, cxTextEdit, cxMemo,
  ZetaKeyLookup_DevEx, cxButtons, dxGDIPlusClasses, cxImage, cxLabel,
  cxGroupBox, dxCustomWizardControl, dxWizardControl;

type
  TWizEmpImportarAsistenciaSesiones_DevEx = class(TWizEmpBaseFiltro)
    OpenDialog: TOpenDialog;
    PanelAnimation: TPanel;
    Animate: TAnimate;
    PanelMensaje: TPanel;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Archivo: TEdit;
    Formato: TZetaKeyCombo;
    ArchivoSeek: TcxButton;
    procedure ArchivoSeekClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject); //DevEx(by am): Agregado para enfocar correctamente los controles
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FVerificaASCII: Boolean;
    ArchivoVerif : String;
    function LeerArchivo: Boolean;
    procedure CargaListaVerificaASCII;
    function GetArchivo: String;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizEmpImportarAsistenciaSesiones_DevEx: TWizEmpImportarAsistenciaSesiones_DevEx;

implementation

uses DProcesos,
     ZetaDialogo,
     ZetaMessages,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaClientTools,
     ZAsciiTools,
     ZBaseSelectGrid_DevEx,
     ZBaseGridShow_DevEx,
     FEmpImportarAsistenciaSesionesGridShow_DevEx,
     FEmpAsistenciaSesionesGridSelect_DevEx;

{$R *.DFM}

procedure TWizEmpImportarAsistenciaSesiones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     PanelAnimation.Visible := FALSE;
     ParametrosControl := Archivo;
     Archivo.Text := ZetaMessages.SetFileNameDefaultPath( 'GRUPOS.DAT' );
     Formato.ItemIndex := Ord( faASCIIFijo );
     ArchivoVerif:= VACIO;
     HelpContext:= H_Importar_Asistencia_Sesiones;
end;

procedure TWizEmpImportarAsistenciaSesiones_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with Descripciones do
     begin
          AddString( 'Archivo', Archivo.Text );
          AddString( 'Formato ASCII', ObtieneElemento( lfFormatoASCII, Formato.Valor ) );
     end;
     with ParameterList do
     begin
          AddInteger( 'Formato', Formato.Valor );
          AddString( 'Archivo', Archivo.Text );
     end;
end;

procedure TWizEmpImportarAsistenciaSesiones_DevEx.CargaListaVerificaASCII;
begin
     with dmProcesos do
     begin
          ImportarAsistenciaSesionesGetListaASCII( ParameterList );
          if ( ErrorCount > 0 ) then                 // Hubo Errores
          begin
               with dmProcesos do
               begin
                    ZAsciiTools.FiltraASCII( cdsDataSet, True );
                    FVerificaASCII := ZBaseGridShow_DevEx.GridShow( cdsDataset, TEmpImportarAsistenciaSesionesGridShow_DevEx );
                    ZAsciiTools.FiltraASCII( cdsDataSet, False );
               end;
          end
          else
              FVerificaASCII := True;
     end;
end;

procedure TWizEmpImportarAsistenciaSesiones_DevEx.CargaListaVerificacion;
begin
     with dmProcesos do
     begin
          ImportarAsistenciaSesionesGetLista( ParameterList );
     end;
end;

function TWizEmpImportarAsistenciaSesiones_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TGridAsistenciaSesiones_DevEx, FALSE );
end;

function TWizEmpImportarAsistenciaSesiones_DevEx.GetArchivo: String;
begin
     Result := Archivo.Text;
end;

function TWizEmpImportarAsistenciaSesiones_DevEx.LeerArchivo: Boolean;
begin
     Result := FALSE;
     PanelAnimation.Visible := TRUE;
     Application.ProcessMessages;
     CargaParametros;
     CargaListaVerificaASCII;
     PanelAnimation.Visible := FALSE;
     if FVerificaASCII then
     begin
          CargaListaVerificacion;
          Result := Verificar;
     end;
     if Result then
        ArchivoVerif := GetArchivo
     else
        ArchivoVerif := VACIO;
end;

procedure TWizEmpImportarAsistenciaSesiones_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     ParametrosControl := nil;
     if Wizard.Adelante then
     begin
          if ( Wizard.EsPaginaActual (Parametros) ) then
          begin
               if ZetaCommonTools.StrVacio( GetArchivo ) then
                  CanMove := Error( 'Debe especificarse un archivo a importar', Archivo )
               else
                  if not FileExists( GetArchivo ) then
                     CanMove := Error( 'El archivo ' + GetArchivo + ' no existe', Archivo )
                  else
                  begin
                       if ( ArchivoVerif <> GetArchivo ) or
                          ( ZetaDialogo.ZConfirm( Caption, 'El archivo ' + GetArchivo + ' ya fu� verificado !' + CR_LF +'volver a verificar ?', 0, mbYes ) ) then
                          CanMove := LeerArchivo
                       else
                          CanMove := TRUE;
                  end;
          end;
     end;
end;

procedure TWizEmpImportarAsistenciaSesiones_DevEx.WizardAfterMove(Sender: TObject);
begin
     // Para Enfocar el primer control dependiendo del tipo de proceso
     if Wizard.EsPaginaActual( Parametros ) then
        ParametrosControl := Archivo;
     inherited;
end;

function TWizEmpImportarAsistenciaSesiones_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.ImportarAsistenciaSesiones( ParameterList ); //PENDIENTE: Talvez se deba cambiar el parameterlist aqui.
end;

procedure TWizEmpImportarAsistenciaSesiones_DevEx.ArchivoSeekClick(Sender: TObject);
begin
     Archivo.Text := ZetaClientTools.AbreDialogo( OpenDialog, Archivo.Text, 'dat' );
end;

procedure TWizEmpImportarAsistenciaSesiones_DevEx.FormShow(
  Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'Al ejecutar el proceso se registrar�n los cursos tomados al expediente de cada uno de los empleados en el archivo.'; 
end;

end.
