inherited WizNomDesafectar_DevEx: TWizNomDesafectar_DevEx
  Left = 275
  Top = 122
  Caption = 'Desafectar N'#243'mina'
  ClientHeight = 433
  ClientWidth = 439
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 439
    Height = 433
    ExplicitWidth = 439
    ExplicitHeight = 433
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'El proceso resta de los acumulados, los montos del per'#237'odo previ' +
        'amente afectado.'
      Header.Title = 'Desafectar n'#243'mina'
      ExplicitWidth = 417
      ExplicitHeight = 293
      inherited GroupBox1: TGroupBox
        Top = 49
        ExplicitTop = 49
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      ExplicitWidth = 417
      ExplicitHeight = 293
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 417
        ExplicitHeight = 198
        Height = 198
        Width = 417
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 417
        Width = 417
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          ExplicitWidth = 349
          Width = 349
          AnchorY = 57
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      ExplicitWidth = 417
      ExplicitHeight = 293
      inherited sCondicionLBl: TLabel
        Left = 14
        ExplicitLeft = 14
      end
      inherited sFiltroLBL: TLabel
        Left = 39
        ExplicitLeft = 39
      end
      inherited Seleccionar: TcxButton
        Left = 142
        ExplicitLeft = 142
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 66
        ExplicitLeft = 66
      end
      inherited sFiltro: TcxMemo
        Left = 66
        Style.IsFontAssigned = True
        ExplicitLeft = 66
      end
      inherited GBRango: TGroupBox
        Left = 66
        ExplicitLeft = 66
      end
      inherited bAjusteISR: TcxButton
        Left = 380
        ExplicitLeft = 380
      end
    end
  end
end
