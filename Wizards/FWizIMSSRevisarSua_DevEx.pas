unit FWizIMSSRevisarSua_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, ComCtrls, StdCtrls,
     ExtCtrls, Db, ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxContainer, cxEdit, ZetaCXWizard,
     dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
     dxWizardControl;

type
  TWizIMSSRevisarSUA_DevEx = class(TcxBaseWizard)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizIMSSRevisarSUA_DevEx: TWizIMSSRevisarSUA_DevEx;

implementation

uses DProcesos,
     ZetaCommonClasses, ZcxWizardBasico;

{$R *.DFM}

procedure TWizIMSSRevisarSUA_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     //DevEx
      Ejecucion.PageIndex := 0;
      ParametrosControl := nil;
     //Wizard.Saltar( Ejecucion.PageIndex );
     HelpContext := H40443_Revisar_datos_para_SUA;
end;

function TWizIMSSRevisarSUA_DevEx.EjecutarWizard: Boolean;
begin
     Result:= dmProcesos.RevisarSUA( ParameterList );
end;

procedure TWizIMSSRevisarSUA_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'Al ejecutar el proceso genera un reporte detallado con los problemas detectados en los datos de los empleados.';
end;

procedure TWizIMSSRevisarSUA_DevEx.WizardAfterMove(Sender: TObject);
begin
    //Sobreescrito para que no ejecute las acciones del clase padre
end;

end.
