unit FNomAjusteFonacotCancelacionGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseSelectGrid_DevEx, DB, StdCtrls, Grids, DBGrids, ZetaDBGrid,
  Buttons, ExtCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxButtons;

type
  TNomAjusteFonacotCancelacionGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    K_PR_REFEREN: TcxGridDBColumn;
    K_CARGO_CANCELAR: TcxGridDBColumn;
    K_ABONO_CANCELAR: TcxGridDBColumn;
    K_SALDO_DESPUES: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    FModalidad : TModalResult;
  public
    { Public declarations }
    property Modalidad : TModalResult read FModalidad;
  end;

var
  NomAjusteFonacotCancelacionGridSelect_DevEx: TNomAjusteFonacotCancelacionGridSelect_DevEx;

implementation

uses ZGridModeTools;
{$R *.dfm}

procedure TNomAjusteFonacotCancelacionGridSelect_DevEx.Cancelar_DevExClick(
  Sender: TObject);
begin
    inherited;
    FModalidad := mrCancel;
end;

procedure TNomAjusteFonacotCancelacionGridSelect_DevEx.FormShow(Sender: TObject);
begin
    inherited;
    with DataSet do
    begin
         MaskPesos( 'K_ABONO_CANCELAR' );
         MaskPesos( 'K_CARGO_CANCELAR' );
         MaskPesos( 'K_SALDO_DESPUES' );
    end;

    PRETTYNAME.Options.HorzSizing := TRUE;
    PRETTYNAME.MinWidth := 150;

end;

procedure TNomAjusteFonacotCancelacionGridSelect_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     FModalidad := mrOk;
end;

end.

