unit FWizNomAjusteRetFonacotCancelacion_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  {$ifdef TRESS_DELPHIXE5_UP}Data.DB,{$endif}
  Dialogs, FWizNomBasico_DevEx, ComCtrls, ZetaEdit, StdCtrls,
  Buttons, ZetaWizard, ExtCtrls, ZetaDBTextBox, ZetaKeyCombo, Mask,
  ZetaNumero, ZetaCommonLists, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters,
  cxContainer, cxEdit, Menus, ZetaCXWizard, ZetaKeyLookup_DevEx, cxButtons,
  cxRadioGroup, cxTextEdit, cxMemo, cxGroupBox, cxLabel, dxGDIPlusClasses,
  cxImage, dxCustomWizardControl, dxWizardControl, TressMorado2013, cxCheckBox;

type
  TWizNomAjusteRetFonacotCancelacion_DevEx = class(TWizNomBasico_DevEx)
    GBTipoPrestamo: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    NoTipoPrestamo: TZetaTextBox;
    TipoPrestamo: TZetaTextBox;
    Label6: TLabel;
    MesDefault: TZetaKeyCombo;
    YearDefaultAct: TZetaNumero;
    TipoPrestamoAjus: TZetaKeyLookup_DevEx;
    Label4: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject); //DevEx(by am): Agregado para enfocar correctamente los controles
  private
    { Private declarations }
    procedure SetFiltroPeriodo;

  protected
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaParametros;override;
    procedure CargaListaVerificacion; override;
    procedure AjustarLabelLeft(lblLabel: TLabel; tpOrdinal: eTipoPeriodo); //acl
  public
    { Public declarations }
  end;

const
     K_REEMPLAZAR = 0;
     K_ANTERIOR   = 1;
     K_RANGO_NOM = 1;
     K_FILTRO_MES = 0;
     K_FILTRO_NOMINA = 0;
     K_SIN_VALOR = 0;
     K_TOLERANCIA_DEFAULT = 1.00;
     K_MAX_NOMINAS = High ( eTipoPeriodo );
     K_FINAL = -1;

var
  WizNomAjusteRetFonacotCancelacion_DevEx: TWizNomAjusteRetFonacotCancelacion_DevEx;

implementation

uses DCliente,
     DProcesos,
     DTablas,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaTipoEntidad,
     ZGlobalTress,
     DBaseGlobal,
     ZBaseSelectGrid_DevEx,
     DGlobal,
     FNomAjusteFonacotCancelacionGridSelect_DevEx,
     DCatalogos,
     DNomina;

{$R *.dfm}



procedure TWizNomAjusteRetFonacotCancelacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     dmTablas.cdsTPresta.Conectar;
     SetFiltroPeriodo;
     TipoPrestamoAjus.Tag:= K_GLOBAL_DEF_NOM_TIPOPRESTAAJUSTE;
     ParametrosControl := YearDefaultAct;
     HelpContext:= H_WIZ_AJUSTE_RET_FONACOT;

//acl
     {***Se define la seceunta de las paginas***}
     Ejecucion.PageIndex := WizardControl.PageCount-1;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount-2;
     Parametros.PageIndex := WizardControl.PageCount-3;
     {***Se define el tipo de salto a utilizar en los wizards***}
     Wizard.SaltoEspecial := TRUE;

//     Parametros.header.InstanceSize.Size.
end;

{acl 7/10/08 Para ajustar dinamicamente la posicion de los labels} 
procedure TWizNomAjusteRetFonacotCancelacion_DevEx.AjustarLabelLeft(lblLabel: TLabel; tpOrdinal: eTipoPeriodo);
const
     K_WIDTH_MAX = 120;
     K_WIDTH_2PUNTOS = 3;
var
   iWidthO, iDif: integer;
begin
     iWidthO:=lblLabel.Width;
     
     lblLabel.Caption:= ObtieneElemento( lfTipoPeriodo, ord( tpOrdinal ) ) + ':';
     if ( lblLabel.Width > K_WIDTH_MAX ) then
     begin
          lblLabel.Width:= K_WIDTH_MAX;
     end;     

     if( lblLabel.Width > ( iWidthO - K_WIDTH_2PUNTOS ) ) then
     begin
          iDif:= lblLabel.Width - iWidthO;
          lblLabel.Left:=lblLabel.Left - iDif;
     end;
end;

procedure TWizNomAjusteRetFonacotCancelacion_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     NoTipoPrestamo.Caption:= Global.GetGlobalString(K_GLOBAL_FONACOT_PRESTAMO);
     TipoPrestamo.Caption:= dmTablas.cdsTPresta.GetDescripcion(NoTipoPrestamo.Caption);
     with dmCliente do
     begin
          YearDefaultAct.Valor := GetDatosPeriodoActivo.Year;
          MesDefault.Valor:= ImssMes;
     end;
end;

procedure TWizNomAjusteRetFonacotCancelacion_DevEx.SetFiltroPeriodo;
var
   iYear : Integer;
begin
     iYear := YearDefaultAct.ValorEntero;
     with dmCatalogos do
     begin
          with cdsPeriodoOtro do
          begin
               if ( IsEmpty ) or
                  ( iYear <> FieldByName( 'PE_YEAR' ).AsInteger ) then
               begin
                    GetDatosPeriodoOtro( iYear );
               end;
          end;
     end;
end;

procedure TWizNomAjusteRetFonacotCancelacion_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
const
     K_MENSAJE_VALOR_NOM = 'Debe especificarse un valor v�lido de rango de n�minas';
var
   iYear, iMes: Integer;
   oLookup: TZetaKeyLookup_DevEx;

   function CargoRangoNominas: Boolean;
   begin
        //FRangosNomina:= VarArrayCreate( [ 0, FPanelesVisible ], varVariant );
        Result:= TRUE;
        oLookup:= NIL;
   end;


begin
     ParametrosControl := nil;
     iYear:= YearDefaultAct.ValorEntero;
     iMes:= MesDefault.Valor;
     {***Validar si se puede Mover hacia adelante***}
     if CanMove then
     begin
          if Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
          begin
               SetFiltroPeriodo;
               if ( dmNomina.GetStatusFonacot( iMes, iYear ) = slAfectadaTotal ) then
                   begin
                        CanMove:= Error( 'El c�lculo de Fonacot se encuentra cerrado', MesDefault );
                   end
               else
                   if ( NoTipoPrestamo.Caption = TipoPrestamoAjus.Llave ) then
                     begin
                        CanMove:= Error( 'El Tipo de Pr�stamo para Ajuste no puede ser igual al Tipo de Pr�stamo de Fonacot', TipoPrestamoAjus );
                     end
                   else
                     if ( TipoPrestamoAjus.Llave = VACIO ) then
                         begin
                             CanMove:= Error( 'Debe Especificarse un Tipo de Pr�stamo para Ajuste', TipoPrestamoAjus );
                         end
                     else if ( dmCatalogos.cdsPeriodoOtro.IsEmpty ) then
                         begin
                               CanMove:= Error( Format( 'No existen periodos definidos para el a�o %d', [ iYear ] ), YearDefaultAct );
                         end;
          end
     end;



     {***Validar hacia que pagina queremos que se mueva***}
     if Wizard.Adelante then
     begin
          if ( WizardControl.ActivePage = FiltrosCondiciones ) then
          begin
               SeleccionarClick( Sender );
               CanMove := Verificacion;
          end;
          if CanMove then
          begin
               if  Wizard.EsPaginaActual( Parametros) then
                       iNewPage := FiltrosCondiciones.PageIndex
               else if Wizard.EsPaginaActual( FiltrosCondiciones ) then
                    iNewPage := Ejecucion.PageIndex;
          end;
     end
     else
     begin
        if  Wizard.EsPaginaActual( Ejecucion ) then
         begin
             iNewPage := FiltrosCondiciones.PageIndex;
             Verificacion := False;
         end
         else if Wizard.EsPaginaActual (FiltrosCondiciones ) then
              iNewPage := Parametros.PageIndex;
     end;
     inherited;
end;

function TWizNomAjusteRetFonacotCancelacion_DevEx.Verificar: Boolean;
begin
     NomAjusteFonacotCancelacionGridSelect_DevEx := TNomAjusteFonacotCancelacionGridSelect_DevEx.Create( Self );
     try
          //Self.Hide;
          with NomAjusteFonacotCancelacionGridSelect_DevEx do
             begin
                  Dataset := dmProcesos.cdsDataSet;

                  if Dataset.RecordCount = 0 then
                      OK_DevEx.Enabled := False;

                  ShowModal;

                  Result := ( Modalidad = mrOk );
                  if Result = False  then
                       Verificacion := False

             end;
          finally
             FreeAndNil( NomAjusteFonacotCancelacionGridSelect_DevEx );
     end;
end;

procedure TWizNomAjusteRetFonacotCancelacion_DevEx.CargaListaVerificacion;
begin
     inherited;
     dmProcesos.AjusteRetFonacotCancelacionGetLista( ParameterList );
end;

function TWizNomAjusteRetFonacotCancelacion_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.AjusteRetFonacotCancelacion(ParameterList, Verificacion);
end;

procedure TWizNomAjusteRetFonacotCancelacion_DevEx.CargaParametros;
const
     K_SIN_VALOR = -1;
begin
     inherited;

     with Descripciones do
     begin
          AddInteger('A�o', YearDefaultAct.ValorEntero );
          AddString('Mes', ObtieneElemento( lfMeses, MesDefault.Valor  - GetOffSet( lfMeses ) ) );
          AddString('Tipo de Pr�stamo Fonacot', NoTipoPrestamo.Caption );
          AddString('Tipo de Pr�stamo de Ajuste', TipoPrestamoAjus.Llave );
     end;
    // PreparaArreglo;

     with ParameterList do
     begin
          AddInteger('Year', YearDefaultAct.ValorEntero );
          AddInteger('Mes', MesDefault.Valor );
          AddString('TPrestamo', NoTipoPrestamo.Caption );
          AddString('TPrestamoAjus', TipoPrestamoAjus.Llave );
          // AP(11/10/2007): Se Agreg� �ste par�metro para ahorrarnos el trabajo de ir al servidor e investigar el dato
          AddInteger('NoConcepto', dmTablas.GetConcepto( NoTipoPrestamo.Caption,TRUE,TRUE ) );
     end;
end;

procedure TWizNomAjusteRetFonacotCancelacion_DevEx.WizardAfterMove(Sender: TObject);
begin
     // Para Enfocar el primer control dependiendo del tipo de proceso
     if Wizard.EsPaginaActual( Parametros ) then
        ParametrosControl := YearDefaultAct
     else
         ParametrosControl := nil;
     inherited;
end;

end.
