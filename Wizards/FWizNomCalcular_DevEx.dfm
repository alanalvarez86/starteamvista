inherited WizNomCalcular_DevEx: TWizNomCalcular_DevEx
  Caption = 'Calcular N'#243'mina'
  ClientHeight = 422
  ClientWidth = 440
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 440
    Height = 422
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que calcula para cada empleado, las horas de asistencia ' +
        'y excepciones registradas en el periodo.'
      Header.Title = 'Calcular N'#243'mina'
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 187
        Width = 418
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 418
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 350
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 6
        Top = 137
      end
      inherited sFiltroLBL: TLabel
        Left = 31
        Top = 160
      end
      inherited Seleccionar: TcxButton
        Left = 134
        Top = 254
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 58
        Top = 134
      end
      inherited sFiltro: TcxMemo
        Left = 58
        Top = 160
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 58
        Top = 4
      end
      inherited bAjusteISR: TcxButton
        Left = 372
        Top = 166
      end
    end
    object TipoCalculo: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 'Seleccione los par'#225'metros para el tipo de c'#225'lculo.'
      Header.Title = 'Tipo de C'#225'lculo'
      object GroupBox: TcxGroupBox
        Left = 108
        Top = 23
        Caption = ' &Tipo de C'#225'lculo '
        TabOrder = 0
        Height = 130
        Width = 221
        object rbTipoNueva: TcxRadioButton
          Left = 25
          Top = 28
          Width = 113
          Height = 17
          Caption = 'rbTipoNueva'
          TabOrder = 0
          OnClick = rbTipoNuevaClick
          Transparent = True
        end
        object rbTipoTodos: TcxRadioButton
          Left = 25
          Top = 49
          Width = 113
          Height = 17
          Caption = 'rbTipoTodos'
          TabOrder = 1
          OnClick = rbTipoTodosClick
          Transparent = True
        end
        object rbTipoRango: TcxRadioButton
          Left = 25
          Top = 71
          Width = 113
          Height = 17
          Caption = 'rbTipoRango'
          TabOrder = 2
          OnClick = rbTipoRangoClick
          Transparent = True
        end
        object rbTipoPendientes: TcxRadioButton
          Left = 25
          Top = 92
          Width = 113
          Height = 17
          Caption = 'rbTipoPendientes'
          TabOrder = 3
          OnClick = rbTipoPendientesClick
          Transparent = True
        end
      end
      object CBMuestraTiempos: TcxCheckBox
        Left = 133
        Top = 161
        Caption = '&Mostrar Tiempos de C'#225'lculo'
        TabOrder = 1
        Transparent = True
        Width = 177
      end
      object CBScript: TcxCheckBox
        Left = 133
        Top = 185
        Caption = '&Incluir Sentencia de SQL en Bit'#225'cora'
        TabOrder = 2
        Transparent = True
        Width = 213
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
  end
end
