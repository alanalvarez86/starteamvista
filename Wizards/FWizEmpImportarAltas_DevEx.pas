unit FWizEmpImportarAltas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {$ifdef TRESS_DELPHIXE5_UP}Data.DB,{$endif}
  ComCtrls, StdCtrls, ExtCtrls, Mask, ZetaFecha, ZetaKeyCombo,
  ZcxBaseWizard, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxContainer, cxEdit, ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel,
  cxGroupBox, dxCustomWizardControl, dxWizardControl, Menus, cxCheckBox,
  cxButtons;

type
  TWizEmpImportarAltas_DevEx = class(TcxBaseWizard)
    Panel2: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    ArchivoSeek: TcxButton;
    Archivo: TEdit;
    Formato: TZetaKeyCombo;
    Observaciones: TEdit;
    PanelAnimation: TPanel;
    Animate: TAnimate;
    PanelMensaje: TPanel;
    OpenDialog: TOpenDialog;
    btnValidar: TcxButton;
    GroupBox1: TcxGroupBox;
    cbRegKardex: TcxCheckBox;
    Label8: TLabel;
    Fecha: TZetaFecha;
    Label4: TLabel;
    cbFFecha: TZetaKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    //DevEx     //Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
    procedure btnValidarClick(Sender: TObject);
    procedure cbRegKardexClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FVerificaASCII: Boolean;
    FVerificacion: Boolean;
    ArchivoVerif : String;
    procedure SetVerificacion( const Value: Boolean );
    procedure CargaListaVerificaASCII;
    function GetArchivo: String;
    function LeerArchivo: Boolean;
  protected
   procedure CargaParametros; override;
   function CargaListaVerificacion: Boolean;
   procedure CargaListaVerificaCatalogos;
   //function Verificar: Boolean;
   function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizEmpImportarAltas_DevEx: TWizEmpImportarAltas_DevEx;

implementation

uses DProcesos,
     DCliente,
     ZetaDialogo,
     ZetaMessages,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaClientTools,
     ZAsciiTools,
     ZBaseSelectGrid_DevEx,
     ZBasicoSelectGrid_DevEx,
     ZBaseGridShow_DevEx,
     ZGlobalTress,
     //FEmpImportarAltasGridShow,
     FListaEmpGridSelect_DevEx,
     FEmpVerificaTablasGridSelect_DevEx, ZcxWizardBasico;

{$R *.DFM}

procedure TWizEmpImportarAltas_DevEx.FormCreate(Sender: TObject);
begin
     Archivo.Tag := K_GLOBAL_DEF_EMP_IMP_ALTAS_EMP;
     Fecha.Valor := Date;
     ParametrosControl := Archivo;
     PanelAnimation.Visible := FALSE;
     {V2013}
     Formato.ItemIndex := Ord( faASCIIDel );
     
     cbFFecha.ItemIndex := Ord( ifDDMMYYYYs );
     ArchivoVerif:= VACIO;
     inherited;
     HelpContext:= H10174_Importar_altas;
end;

function TWizEmpImportarAltas_DevEx.EjecutarWizard: Boolean;
begin
     Result := ( not dmCliente.EsModoDemo ) and dmProcesos.ImportarAltas( ParameterList );
end;

procedure TWizEmpImportarAltas_DevEx.ArchivoSeekClick(Sender: TObject);
begin
     Archivo.Text := ZetaClientTools.AbreDialogo( OpenDialog, Archivo.Text, 'dat' );
end;

procedure TWizEmpImportarAltas_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     ParametrosControl := nil;  //DevEx
     if Wizard.Adelante then
     begin
          //if ( PageControl.ActivePage = Parametros ) then
          if ( WizardControl.ActivePage = Parametros ) then
          begin
               if ZetaCommonTools.StrVacio( GetArchivo ) then
                  CanMove := Error( 'Debe especificarse un archivo a importar', Archivo )
               else
                  if not FileExists( GetArchivo ) then
                     CanMove := Error( 'El archivo ' + GetArchivo + ' no existe', Archivo )
                  else
                  begin
                       if ( ArchivoVerif <> GetArchivo ) or
                          ( ZetaDialogo.ZConfirm( Caption, 'El archivo ' + GetArchivo + ' ya fu� verificado !' + CR_LF +
                                                  'Volver a verificar ?', 0, mbYes ) ) then
                          CanMove := LeerArchivo
                       else
                          CanMove := TRUE;
                  end;
          end;
     end;
end;

function TWizEmpImportarAltas_DevEx.GetArchivo: String;
begin
     Result := Archivo.Text;
end;

procedure TWizEmpImportarAltas_DevEx.CargaParametros;
begin
     inherited CargaParametros;

     with ParameterList do
     begin
          AddDate( 'Fecha', Fecha.Valor );
          AddInteger( 'Formato', Formato.Valor );
          AddInteger( 'FormatoImpFecha', cbFFecha.Valor );
          AddString( 'Archivo', Archivo.Text );
          AddMemo( 'Observaciones', Observaciones.Text );
          AddBoolean( 'lRegKardex', cbRegKardex.Checked );
     end;

     with Descripciones do
     begin
          //Clear;      //DevEx
          AddString( 'Archivo', Archivo.Text );
          AddString( 'Formato ASCII', ObtieneElemento( lfFormatoASCII, Formato.Valor ) );
          AddString( 'Formato fechas', ObtieneElemento( lfFormatoImpFecha, cbFFecha.Valor ) );
          if ( cbRegKardex.Checked ) then
             AddDate( 'Agregar registros de Kardex con fecha', Fecha.Valor );
          AddMemo( 'Observaciones', Observaciones.Text );
     end;

end;

procedure TWizEmpImportarAltas_DevEx.CargaListaVerificaCatalogos;
begin
     with dmProcesos do
     begin
          ImportarAltasGetASCIICatalogos( ParameterList );
          if (cdsDataset.IsEmpty) then
             ZetaDialogo.zInformation( '� Atenci�n !', 'Los Cat�logos y Tablas verificados est�n correctos.', 0 )
          else
          begin
               ZBasicoSelectGrid_DevEx.GridSelectBasico( cdsDataset, TEmpVerificaTablasGridSelect_DevEx, FALSE );
                  //GrabaVerificaCatalogos;
          end;
     end;
end;


procedure TWizEmpImportarAltas_DevEx.CargaListaVerificaASCII;
begin
     with dmProcesos do
     begin
          ImportarAltasGetListaASCII( ParameterList );
          {if ( ErrorCount > 0 ) then                 // Hubo Errores
          begin
               with dmProcesos do
               begin
                    ZAsciiTools.FiltraASCII( cdsDataSet, True );
                    FVerificaASCII := ZBaseGridShow.GridShow( cdsDataset, TEmpImportarAltasGridShow );
                    ZAsciiTools.FiltraASCII( cdsDataSet, False );
               end;
          end
          else
              FVerificaASCII := True;}
          FVerificaASCII := True;    
     end;
end;

function TWizEmpImportarAltas_DevEx.CargaListaVerificacion: Boolean;
begin
     with dmProcesos do
     begin
          //ImportarAltasGetLista( ParameterList );
          Result := ZBaseSelectGrid_DevEx.GridSelect( cdsDataset, TListaEmpGridSelect_DevEx, FALSE );
     end;
end;

procedure TWizEmpImportarAltas_DevEx.SetVerificacion(const Value: Boolean);
begin
     FVerificacion := Value;
end;

function TWizEmpImportarAltas_DevEx.LeerArchivo: Boolean;
begin
     Result := FALSE;
     PanelAnimation.Visible := TRUE;
     Application.ProcessMessages;
     try
        CargaParametros;
        CargaListaVerificaASCII;
     finally
            PanelAnimation.Visible := FALSE;
     end;
     if FVerificaASCII then
     begin
          //CargaListaVerificacion;
          SetVerificacion( CargaListaVerificacion );
          Result := FVerificacion;
     end;
     if Result then
        ArchivoVerif := GetArchivo
     else
        ArchivoVerif := VACIO;
end;


procedure TWizEmpImportarAltas_DevEx.btnValidarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        CargaParametros;
        CargaListaVerificaCatalogos;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TWizEmpImportarAltas_DevEx.cbRegKardexClick(Sender: TObject);
begin
     Fecha.Enabled := not Fecha.Enabled;
end;

procedure TWizEmpImportarAltas_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'Al ejecutar este proceso se dar�n de alta los empleados contenidos en el archivo de importaci�n.';
end;


//DevEx     //Agregado para enfocar un control
procedure TWizEmpImportarAltas_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := Archivo
     else
         ParametrosControl := nil;
     inherited;
end;

end.
