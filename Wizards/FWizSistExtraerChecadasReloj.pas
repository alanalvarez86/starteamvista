unit FWizSistExtraerChecadasReloj;

interface

uses
    Windows, Messages, SysUtils, Variants, Classes, Graphics,
    Controls, Forms, Dialogs, ZcxBaseWizard, cxGraphics, cxControls,
    cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
    cxContainer, cxEdit, ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel,
    cxGroupBox, dxCustomWizardControl, dxWizardControl, Menus, StdCtrls,
    ZetaKeyCombo, Mask, ZetaFecha, cxButtons, ExtCtrls, ZetaKeyLookup_DevEx,DB;

type
  TWizSistExtraerChecadasReloj = class(TcxBaseWizard)
    Label5: TLabel;
    sArchivo: TEdit;
    BGuardaArchivo: TcxButton;
    OpenDialog: TOpenDialog;
    dInicialLBL: TLabel;
    FechaInicial: TZetaFecha;
    dFinalLBL: TLabel;
    FechaFinal: TZetaFecha;
    lblTerminal: TLabel;
    lkuTerminalGTI: TZetaDBKeyLookup_DevEx;
    DataSource: TDataSource;
    procedure BGuardaArchivoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    function GetArchivo: String;
  public
    { Public declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  end;

var
   WizSistExtraerChecadasReloj: TWizSistExtraerChecadasReloj;

implementation

uses ZetaClientTools,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaCommonClasses,
     DSistema,
     DCliente,
     DProcesos;

{$R *.dfm}

procedure TWizSistExtraerChecadasReloj.FormCreate(Sender: TObject);
begin
    inherited;
    HelpContext := H_TRESS_SISTEMA_TERMINALESGTI_EXTRAER_CHECADAS;
    sArchivo.Text := 'Reloj.Dat';
    // Terminales.
    dmSistema.cdsTerminales.Conectar;
    DataSource.DataSet := dmSistema.cdsTerminales;
    lkuTerminalGTI.LookupDataset := dmSistema.cdsTerminales;
    // Fechas.
    with dmCliente.GetDatosPeriodoActivo do
    begin
         FechaInicial.Valor := Inicio;
         FechaFinal.Valor := Fin;
    end;
end;

procedure TWizSistExtraerChecadasReloj.FormShow(Sender: TObject);
begin
    inherited;
    sArchivo.SetFocus;
end;

procedure TWizSistExtraerChecadasReloj.BGuardaArchivoClick(Sender: TObject);
begin
     inherited;
     sArchivo.Text := AbreDialogo( OpenDialog, GetArchivo, 'dat' );
end;

function TWizSistExtraerChecadasReloj.GetArchivo: String;
begin
     Result := sArchivo.Text;
end;

procedure TWizSistExtraerChecadasReloj.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
          if Adelante and EsPaginaActual ( Parametros ) then
          begin
               if ( ZetaCommonTools.StrVacio ( GetArchivo ) ) or
                  ( ZetaCommonTools.StrVacio ( ExtractFileName( GetArchivo ) ) ) then
               begin
                    ZetaDialogo.zError( Caption, 'El nombre del archivo no puede quedar vac�o', 0 );
                    CanMove := False;
                    sArchivo.SetFocus;
               end
               else
               begin
                    if FileExists( GetArchivo ) and not ZetaDialogo.zConfirm( Caption, 'El archivo ' + GetArchivo + ' ya existe' + CR_LF + '� Desea reemplazarlo ? ', 0, mbNo ) then
                    begin
                         CanMove := False;
                         sArchivo.SetFocus;
                    end
                    else
                    begin
                         if FechaFinal.Valor < FechaInicial.Valor then
                         begin
                              ZetaDialogo.zError( Caption, 'La fecha inicial debe ser menor o igual a la fecha final', 0);
                              CanMove := False;
                              FechaInicial.SetFocus;
                         end
                         else
                         begin
                              if StrLleno ( lkuTerminalGTI.Llave ) then
                              begin
                                   Advertencia.Caption := Format ('Al aplicar el proceso se efectuar� la exportaci�n ' + 'de las checadas de la terminal %s: %s para las fechas seleccionadas.', [lkuTerminalGTI.Llave, lkuTerminalGTI.Descripcion]);
                              end
                              else
                              begin
                                   Advertencia.Caption := 'Al aplicar el proceso se efectuar� la exportaci�n ' +
                                                          'de las checadas de todas las terminales para las fechas seleccionadas.';
                              end;
                         end;
                    end;
               end;
          end;
     end;
end;

procedure TWizSistExtraerChecadasReloj.CargaParametros;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddString( 'Archivo', GetArchivo );
          if strLleno (lkuTerminalGTI.Llave) then
            AddInteger ('Terminal', StrToInt ( lkuTerminalGTI.Llave ) )
          else
            AddInteger ('Terminal', 0);
          AddDate('Inicio', FechaInicial.Valor);
          AddDate('Fin', FechaFinal.Valor);
     end;
     with Descripciones do
     begin
          AddString( 'Archivo', GetArchivo );
          if strLleno ( lkuTerminalGTI.Llave ) then
            AddInteger ( 'Terminal', StrToInt ( lkuTerminalGTI.Llave ) )
          else
            AddInteger ( 'Terminal', 0 );
          AddDate( 'Fecha inicial', FechaInicial.Valor );
          AddDate( 'Fecha final', FechaFinal.Valor );
     end;
end;

function TWizSistExtraerChecadasReloj.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.ExtraerChecadasBitTerminales( ParameterList );
end;

end.
