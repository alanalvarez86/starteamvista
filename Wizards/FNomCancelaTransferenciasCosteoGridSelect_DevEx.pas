unit FNomCancelaTransferenciasCosteoGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, ExtCtrls, ZBaseSelectGrid_DevEx,
  cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, cxControls, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
  cxButtons;

type
  TNomCancelaTransferenciasCosteoGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CC_ORIGEN: TcxGridDBColumn;
    CC_DESTINO: TcxGridDBColumn;
    TR_HORAS: TcxGridDBColumn;
    TR_TIPO: TcxGridDBColumn;
    TR_MOTIVO: TcxGridDBColumn;
    TR_STATUS: TcxGridDBColumn;
    US_CODIGO: TcxGridDBColumn;
    TR_APRUEBA: TcxGridDBColumn;
    TR_GLOBAL: TcxGridDBColumn;
    TR_TEXTO: TcxGridDBColumn;
    TR_TXT_APR: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  NomCancelaTransferenciasCosteoGridSelect_DevEx: TNomCancelaTransferenciasCosteoGridSelect_DevEx;

implementation

uses
    ZetaCommonLists,
    DAsistencia  , ZBasicoSelectGrid_DevEx;

{$R *.DFM}

procedure TNomCancelaTransferenciasCosteoGridSelect_DevEx.FormShow(Sender: TObject);
begin


     with DataSet do
     begin
          ListaFija( 'TR_TIPO', lfTipoHoraTransferenciaCosteo );
          ListaFija( 'TR_STATUS', lfStatusTransCosteo );
          with FieldByName( 'TR_MOTIVO' ) do
          begin
               Alignment := taLeftJustify;
               OnGetText := dmAsistencia.TR_MOTIVOGetText;
          end;

          with FieldByName( 'US_CODIGO' ) do
          begin
               Alignment := taLeftJustify;
               OnGetText := dmAsistencia.US_CODIGOGetText;
          end;

          with FieldByName( 'TR_APRUEBA' ) do
          begin
               Alignment := taLeftJustify;
               OnGetText := dmAsistencia.US_CODIGOGetText;
          end;
     end;
       inherited;
      //DevEx  , ajustar tamano de campos modificados despues del minwidth
      (ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO')).Caption := 'Empleado';
      (ZetaDBGridDBTableView.GetColumnByFieldName('PrettyName')).Caption := 'Nombre Completo';
      (ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO')).MinWidth := 75;

      (ZetaDBGridDBTableView.GetColumnByFieldName('TR_MOTIVO')).ApplyBestFit();
      (ZetaDBGridDBTableView.GetColumnByFieldName('TR_MOTIVO')).MinWidth := 60;
      (ZetaDBGridDBTableView.GetColumnByFieldName('US_CODIGO')).ApplyBestFit();
      (ZetaDBGridDBTableView.GetColumnByFieldName('US_CODIGO')).MinWidth := 70;
      (ZetaDBGridDBTableView.GetColumnByFieldName('TR_APRUEBA')).ApplyBestFit();
      (ZetaDBGridDBTableView.GetColumnByFieldName('TR_APRUEBA')).MinWidth := 65;


end;

end.
