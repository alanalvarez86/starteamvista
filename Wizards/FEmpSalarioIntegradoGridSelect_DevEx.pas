unit FEmpSalarioIntegradoGridSelect_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ExtCtrls, Db,
     ZBaseSelectGrid_DevEx, cxGraphics, cxLookAndFeels,
     cxLookAndFeelPainters, Menus, dxSkinsCore, 
     TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
     dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
     cxEdit, cxNavigator, cxDBData, ImgList, cxGridLevel, cxClasses,
     cxGridCustomView, cxGridCustomTableView, cxGridTableView,
     cxGridDBTableView, cxGrid, ZetaCXGrid, cxButtons, cxTextEdit;

type
  TEmpSalarioIntegradoGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CB_SAL_INT: TcxGridDBColumn;
    NUEVO_INT: TcxGridDBColumn;
    DIFERENCIA: TcxGridDBColumn;
    PORCENTAJE: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpSalarioIntegradoGridSelect_DevEx: TEmpSalarioIntegradoGridSelect_DevEx;

implementation

uses ZBasicoSelectGrid_DevEx;

{$R *.DFM}

procedure TEmpSalarioIntegradoGridSelect_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with DataSet do
     begin
          MaskPesos( 'CB_SAL_INT' );
          MaskPesos( 'NUEVO_INT' );
          MaskPesos( 'DIFERENCIA' );
          //MaskTasa( 'PORCENTAJE' );
          MaskNumerico( 'PORCENTAJE', '#,##0.00 %' );
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

end.
