unit FWizNomSalNetoBruto_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,Forms,
     Dialogs, ComCtrls, StdCtrls, ExtCtrls, Buttons, Grids,DB,Mask,DBGrids,
	{$ifdef TRESS_DELPHIXE5_UP}System.UITypes,{$endif}
     FBaseReportes_DevEx,
     ZetaClientDataSet,
     ZcxBaseWizard,
     ZetaWizard,
     ZetaKeyCombo,
     ZetaFecha,
     ZetaNumero,
     ZetaSmartLists, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit,
  ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl, ZetaKeyLookup_DevEx, Menus,
  cxButtons;

type
  TWizNomSalNetoBruto_DevEx = class(TcxBaseWizard)
    Resultados: TdxWizardControlPage;
    sZonaGe: TComboBox;
    sOtrasPer: TEdit;
    nAntig: TZetaNumero;
    rRedondeo: TZetaNumero;
    SubsidioAcreditable: TZetaNumero;
    rSalario: TZetaNumero;
    sTablaPresta: TZetaKeyLookup_DevEx;
    dReferencia: TZetaFecha;
    QueCalcular: TRadioGroup;
    nPeriodo: TZetaKeyCombo;
    Label1: TLabel;
    dReferenciaLBL: TLabel;
    sListaOtrasLBL: TLabel;
    sZonaGeLBL: TLabel;
    nAntigLBL: TLabel;
    sTablaPrestaLBL: TLabel;
    nSubsidioLBL: TLabel;
    nPeriodoLBL: TLabel;
    nSalarioLBL: TLabel;
    cxGroupBox2: TcxGroupBox;
    StringGrid: TStringGrid;
    PanelInfo: TPanel;
    SalarioMinimoLBL: TLabel;
    FactorIntegracionLBL: TLabel;
    SalarioIntegradoLBL: TLabel;
    SalarioMinimo: TLabel;
    FactorIntegracion: TLabel;
    SalarioIntegrado: TLabel;
    Panel1: TPanel;
    Imprimir: TcxButton;
    Exportar: TcxButton;
    cxButton1: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure QueCalcularClick(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);//DevEx(by am): Agregado para enfocar correctamente los controles
    procedure FormShow(Sender: TObject);
    //procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);  //DevEx(by am): Ahora se ejecutara en un boton
    procedure FormDestroy(Sender: TObject);
    procedure ImprimirClick(Sender: TObject);
    procedure ExportarClick(Sender: TObject);
    procedure WizardControlButtonClick(Sender: TObject;
      AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
    procedure cxButton1Click(Sender: TObject);
    procedure StringGridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
  private
    { Private declarations }
    cdsResultados:TZetaClientDataSet;
    dsResultados: TDataSource;
    dbGrid :      TDBGrid;

    function CalcularBruto: Boolean;
    procedure SetControls;

    procedure InicializaDatos;
    procedure PasarDatos;
    procedure Imprimiendo(const lImprimiendo: Boolean);

    {$ifdef TEST_COMPLETE}
    procedure GeneraASCIICompare;
    {$endif}
  protected
    { Protected declarations }
    //function EjecutarWizard: Boolean; override; //DevEx(by am): Ahora se ejecutara en un boton
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizNomSalNetoBruto_DevEx: TWizNomSalNetoBruto_DevEx;

implementation

uses
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZGlobalTress,
     ZetaDialogo,
     DProcesos,
     DCliente,
     DGlobal,
     DCatalogos;

{$R *.DFM}

procedure TWizNomSalNetoBruto_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := QueCalcular;
     SetControls;
     rSalario.Text := '0';
     nPeriodo.Llave := IntToStr( Ord( tpMensual ) ); //acl
     dReferencia.Valor := dmCliente.FechaDefault;
     nAntig.Valor := 1;
     with Global do
     begin
          SubsidioAcreditable.Valor := GetGlobalReal( K_GLOBAL_PORCEN_SUBSIDIO ); //* 100;
          sZonaGe.ItemIndex := 0;
          rRedondeo.Valor := GetGlobalReal( K_GLOBAL_REDONDEO_NETO ) ;
     end;
     ParametrosControl := rSalario;
     HelpContext:= H33416_Calcular_salario_neto_bruto;
     sTablaPresta.LookupDataset := dmCatalogos.cdsSSocial;
     Imprimir.Enabled := False;
     Exportar.Enabled := False;

     {***Se define la seceunta de las paginas***}
     Resultados.PageIndex := WizardControl.PageCount-1;
     Parametros.PageIndex := WizardControl.PageCount-2;
     {***Se define el tipo de salto a utilizar en los wizards***}
     Wizard.SaltoEspecial := TRUE;

     //DevEx (by am):Colores Base para el grid de Edicion
    StringGrid.FixedColor := RGB(235,235,235);//Gris
    StringGrid.Font.Color := RGB(156,129,139);//Gris-Morado
end;

procedure TWizNomSalNetoBruto_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmCatalogos.cdsSSocial do
     begin
          Conectar;
          sTablaPresta.Llave := FieldByName( LookupKeyField ).AsString;
     end;

     with nPeriodo do
     begin
          ListaFija:=lfTipoPeriodoConfidencial; //acl
          LlaveEntero:= Ord( dmCliente.GetDatosPeriodoActivo.Tipo );
     end;
     //DevEx(by am): Texto final antes de ejecutar el w
     Advertencia.Caption := 'Al ejecutar el proceso se mostrar�n los calculos de acuerdo a los par�metros establecidos.';
end;

function TWizNomSalNetoBruto_DevEx.CalcularBruto: Boolean;
begin
     Result := ( QueCalcular.ItemIndex = 0 );
end;

procedure TWizNomSalNetoBruto_DevEx.SetControls;
begin
     with StringGrid do
     begin
          if CalcularBruto then
          begin
               nSalarioLBL.Caption := 'Salario Neto:';
               Caption := 'Calcular Salario Bruto';
          end
          else
          begin
               nSalarioLBL.Caption := 'Salario Bruto:';
               Caption := 'Calcular Salario Neto';
          end;
          StringGrid.ColWidths[0] := 130; {acl}
          Cells[ 1, 0 ] := 'Salario ';
          Cells[ 5, 0 ] := '= Neto';
          Cells[ 2, 0 ] := '    + Otros   ';
          Cells[ 3, 0 ] := '    - ISPT    ';
          Cells[ 4, 0 ] := '    - IMSS    ';
          Cells[ 0, 1 ] := ObtieneElemento(lfClasifiPeriodoConfidencial, Ord( tpDiario) );//acl
          Cells[ 0, 2 ] := ObtieneElemento(lfClasifiPeriodoConfidencial, Ord( tpSemanal) );
          Cells[ 0, 3 ] := ObtieneElemento(lfClasifiPeriodoConfidencial, Ord( tpMensual) );
          Cells[ 0, 4 ] := ObtieneElemento(lfClasifiPeriodoConfidencial, Ord( tpCatorcenal ) );
          Cells[ 0, 5 ] := ObtieneElemento(lfClasifiPeriodoConfidencial, Ord( tpQuincenal ) );
          Cells[ 0, 6 ] := ObtieneElemento(lfClasifiPeriodoConfidencial, Ord( tpDecenal ) );
     end;
end;

procedure TWizNomSalNetoBruto_DevEx.QueCalcularClick(Sender: TObject);
begin
     inherited;
     SetControls;
end;

procedure TWizNomSalNetoBruto_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
{$ifdef FALSE}
var
   rSubsidio: TTasa;
{$endif}
begin
     ParametrosControl := nil;
     if Wizard.Adelante then
     begin
          if Wizard.EsPaginaActual( Parametros ) then
          begin
               if ( rSalario.Valor > 0 ) then
               begin
                    if nPeriodo.ItemIndex > K_PERIODO_VACIO then
                    begin
                         iNewPage := Resultados.PageIndex;
                         {$ifdef FALSE}
                         rSubsidio := SubsidioAcreditable.Valor;
                         if ( rSubsidio < 0 ) or ( rSubsidio > 1 ) then
                         begin
                              CanMove := Error( '� La Proporci�n del Subsidio ' +
                                           'Acreditable debe de der un valor entre 0 - 1',
                                           SubsidioAcreditable );
                         end;
                         {$endif}
                    end
                    else
                    begin
                         zError( Caption, 'El tipo de n�mina es invalido', 0 );
                         CanMove := False;
                    end
               end
               else
               begin
                    zError( Caption, '� Salario Inv�lido !', 0 );
                    CanMove := False;
               end;
          end;
     end
     else
         if Wizard.EsPaginaActual( Resultados ) then
         begin
              iNewPage := Parametros.PageIndex;
              Imprimir.Enabled := False;
              Exportar.Enabled := False;
         end;
     inherited;
end;

procedure TWizNomSalNetoBruto_DevEx.WizardAfterMove(Sender: TObject);
var
   i, j: Integer;
begin
     //inherited;
     if Wizard.EsPaginaActual( Resultados ) then
     begin
          SalarioMinimo.Caption := '';
          FactorIntegracion.Caption := '';
          SalarioIntegrado.Caption := '';
          for i := 1 to 5 do
          begin
               for j := 1 to 6 do
               begin
                    StringGrid.Cells[ i, j ] := '';
               end;
          end;
     end
     else if Wizard.EsPaginaActual( Parametros ) then   // Para Enfocar el primer control dependiendo del tipo de proceso
         ParametrosControl := QueCalcular;
     inherited;
end;

procedure TWizNomSalNetoBruto_DevEx.CargaParametros;
begin
     inherited;
     with ParameterList do
     begin
          Clear;
          AddBoolean( 'CalcularBruto', CalcularBruto);
          AddFloat( 'Salario', rSalario.Valor);
          AddFloat( 'Subsidio', SubsidioAcreditable.Valor ); /// 100);
          AddFloat( 'Antiguedad' , nAntig.Valor);
          AddString( 'ZonaGeografica', sZonaGe.Items[ sZonaGe.ItemIndex ] );
          AddString( 'TablaPrestaciones', sTablaPresta.Llave);
          AddString( 'OtrasPercepciones', sOtrasPer.Text);
          AddDate( 'Fecha', dReferencia.Valor);
          AddInteger( 'Periodo', nPeriodo.LlaveEntero); //acl
          AddFloat( 'Redondeo', rRedondeo.Valor);
          AddInteger( 'Clasifi', Ord(GetClasificacionPeriodo(FListaTiposPeriodoConfidencialidad, nPeriodo.LlaveEntero)));
     end;
end;

{function TWizNomSalNetoBruto_DevEx.EjecutarWizard: Boolean;

    function FormatReal( const Value: Real ): String;
    begin
         Result := Format( '%14.2n', [ Redondea( Value ) ] );
    end;

    function FormatTasa( const Value: Real ): String;
    begin
         Result := Format( '%14.5n', [ Value ] );
    end;

    procedure GrabaValores( const nValor : Real ; i : Integer );
    begin
         with StringGrid do
         begin
              Cells[ i, 1 ] := FormatReal( nValor / K_FACTOR_MENSUAL );
              Cells[ i, 2 ] := FormatReal( nValor / K_FACTOR_MENSUAL * ZetaCommonTools.Dias7Tipo( tpSemanal ) );
              Cells[ i, 3 ] := FormatReal( nValor );
              Cells[ i, 4 ] := FormatReal( nValor / K_FACTOR_MENSUAL * ZetaCommonTools.Dias7Tipo( tpCatorcenal ) ) ;
              Cells[ i, 5 ] := FormatReal( nValor / K_FACTOR_MENSUAL * ZetaCommonTools.Dias7Tipo( tpQuincenal ) ) ;
              Cells[ i, 6 ] := FormatReal( nValor / K_FACTOR_MENSUAL * ZetaCommonTools.Dias7Tipo( tpDecenal ) ) ;
         end;
    end;

begin

     with ParameterList do
     begin
          Clear;
          AddBoolean( 'CalcularBruto', CalcularBruto);
          AddFloat( 'Salario', rSalario.Valor);
          AddFloat( 'Subsidio', SubsidioAcreditable.Valor ); /// 100);
          AddFloat( 'Antiguedad' , nAntig.Valor);
          AddString( 'ZonaGeografica', sZonaGe.Items[ sZonaGe.ItemIndex ] );
          AddString( 'TablaPrestaciones', sTablaPresta.Llave);
          AddString( 'OtrasPercepciones', sOtrasPer.Text);
          AddDate( 'Fecha', dReferencia.Valor);
          AddInteger( 'Periodo', nPeriodo.LlaveEntero); //acl
          AddFloat( 'Redondeo', rRedondeo.Valor);
     end;

     Result := dmProcesos.SalNetoBruto( ParameterList );
     if Result then
     begin
         with ParameterList do
         begin
              SalarioMinimo.Caption     := FormatReal( ParamByName( 'SalMin' ).AsFloat );
              FactorIntegracion.Caption := FormatTasa( ParamByName( 'Factor' ).AsFloat );
              SalarioIntegrado.Caption  := FormatReal( ParamByName( 'Integrado' ).AsFloat );
              GrabaValores( ParamByName( 'Bruto' ).AsFloat, 1 );
              GrabaValores( ParamByName( 'MontoOtrasP' ).AsFloat, 2 );
              GrabaValores( ParamByName( 'ISPT' ).AsFloat, 3 );
              GrabaValores( ParamByName( 'IMSS' ).AsFloat, 4 );
              GrabaValores( Redondea( ParamByName( 'Bruto' ).AsFloat + ParamByName( 'MontoOtrasP' ).AsFloat -
                            ParamByName( 'ISPT' ).AsFloat - ParamByName( 'IMSS' ).AsFloat ), 5);
         end;}
        //{$ifdef TEST_COMPLETE}
         //GeneraASCIICompare;
         //{$endif}

     {end;
end; }

{$ifdef TEST_COMPLETE}
procedure TWizNomSalNetoBruto_DevEx.GeneraASCIICompare;
var
   iCol, iRow: Integer;
   sTexto : String;
   oLista : TStrings;
begin
     oLista := TStringList.Create;
     try
        with oLista do
        begin
             BeginUpdate;
             try
                Clear;
                with StringGrid do
                begin
                     for iRow := 0 to RowCount - 1 do
                     begin
                          sTexto := VACIO;
                          for iCol := 0 to ColCount - 1 do
                              sTexto := sTexto + Cells[ iCol, iRow ] + K_PIPE;
                          Add( sTexto );
                     end;
                end;
             finally
                EndUpdate;
             end;
             SaveToFile( 'C:\QA_TEST_TEMP\SAL_NETO.TXT' );        // Este Folder debe estar disponible en la Computadora donde se ejecute el programa
        end;
     finally
        FreeAndNil( oLista );
     end;
end;
{$endif}

procedure TWizNomSalNetoBruto_DevEx.PasarDatos;
var
   iCol:Integer;
   iRen:Integer;
begin
    for iRen := 1 to StringGrid.RowCount - 1 do
    begin
         with cdsResultados do
         begin
              Append;
              Fields[0].AsString := StringGrid.Cells[0,iRen];//Periodo
              for iCol := 1 to StringGrid.ColCount - 1 do
              begin
                   Fields[iCol].AsFloat := StrToFloat(StrTransform(StringGrid.Cells[iCol,iRen],',',''));
              end;
              Post;
         end;
    end;
end;

{procedure TWizNomSalNetoBruto_DevEx.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
begin
     inherited;
     with Descripciones do
     begin
          Clear;
          AddString( 'Salario m�nimo',SalarioMinimo.Caption);
          AddString( 'Factor de integraci�n', FactorIntegracion.Caption);
          AddString( 'Salario integrado', SalarioIntegrado.Caption );}
          //{$ifdef FALSE}
          //AddFloat( 'Proporci�n de subsidio', SubsidioAcreditable.Valor );
          //{$endif}
          {AddString( 'Prestaciones', sTablaPresta.Llave +' '+ sTablaPresta.Descripcion );
          AddString( 'Zona geogr�fica', sZonaGe.Items[ sZonaGe.ItemIndex ] );
          if(sOtrasPer.Text <> '') then
          begin
               AddString( 'Otras percepciones', sOtrasPer.Text);
          end;
          AddFloat( 'Antig�edad' , nAntig.Valor);
          AddDate( 'Fecha', dReferencia.Valor);
          AddInteger( 'Periodo', nPeriodo.LlaveEntero); //acl
          AddFloat( 'Redondeo', rRedondeo.Valor);
     end;
     Imprimir.Enabled := True;
     Exportar.Enabled := True;
     lok:=FALSE; //DevEx(by am): Con esta linea evitamos que se cierre el wizard
end;  }

procedure TWizNomSalNetoBruto_DevEx.InicializaDatos;
var
   iIndice: Integer;
begin
     //creaci�n
     if cdsResultados = nil then
     begin
          cdsResultados := TZetaClientDataSet.Create(Self);
          //definici�n de campos
          with cdsResultados do
          begin
               AddStringField('Periodo',20);

               AddFloatField('Salario');
               MaskPesos('Salario');

               AddFloatField('Otros');
               MaskPesos('Otros');

               AddFloatField('ISPT');
               MaskPesos('ISPT');

               AddFloatField('IMSS');
               MaskPesos('IMSS');

               AddFloatField('Neto');
               MaskPesos('Neto');

               CreateTempDataset;
          end;
     end
     else
     begin
          //limpiar data set
          cdsResultados.EmptyDataSet;
     end;

     if dsResultados = nil then
     begin
          dsResultados := TDataSource.Create(Self);
     end;

     if dbGrid = nil then
     begin
          dbGrid := TDBGrid.Create(self);//crear las columnas
          with dbGrid do
          begin
               Columns.Add.Title.Caption := 'Per�odo';
               Columns.Items[0].FieldName := 'Periodo';
               for  iIndice := 1 to StringGrid.ColCount-1 do
               begin
                    Columns.Add.Title.Caption := StringGrid.Cells[iIndice,0];
                    Columns.Items[iIndice].FieldName := cdsResultados.Fields[iIndice].FieldName;
               end;
          end;
     end;
     //asignaci�n
     dsResultados.DataSet := cdsResultados;
     dbGrid.DataSource := dsResultados;
end;

procedure TWizNomSalNetoBruto_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil(cdsResultados);
     FreeAndNil(dsResultados);
     FreeAndNil(dbGrid);
end;

procedure TWizNomSalNetoBruto_DevEx.Imprimiendo (const lImprimiendo: Boolean);
Begin
      InicializaDatos;
      PasarDatos;
      if(lImprimiendo) then
      begin
           FBaseReportes_DevEx.ImprimirGridParams( dbGrid,cdsResultados,Self.Caption,'',Descripciones);
      end
      else
      begin
           FBaseReportes_DevEx.ExportarGridParams( dbGrid,cdsResultados,Descripciones,Self.Caption,'');
      end;
end;

procedure TWizNomSalNetoBruto_DevEx.ImprimirClick(Sender: TObject);
begin
     inherited;
     Imprimiendo(True);
end;

procedure TWizNomSalNetoBruto_DevEx.ExportarClick(Sender: TObject);
begin
     inherited;
     Imprimiendo(False);
end;

procedure TWizNomSalNetoBruto_DevEx.WizardControlButtonClick(
  Sender: TObject; AKind: TdxWizardControlButtonKind;
  var AHandled: Boolean);
begin
  //inherited; DevEx(by am): Para este wizard No utilizamos el comportamiento habitual de este evento
  {if AKind = wcbkFinish  then
     begin

     end; }

     if AKind = wcbkNext  then
     begin
          AHandled := Wizard.SaltarAdelanteEspecial( wcbkNext )
     end;

     if AKind = wcbkBack then
     begin
          AHandled:= Wizard.SaltarAtrasEspecial(wcbkBack)
     end;

     if AKind = wcbkCancel  then
     begin
          Self.Close;
     end;

end;

procedure TWizNomSalNetoBruto_DevEx.cxButton1Click(Sender: TObject);
function FormatReal( const Value: Real ): String;
    begin
         Result := Format( '%14.2n', [ Redondea( Value ) ] );
    end;

    function FormatTasa( const Value: Real ): String;
    begin
         Result := Format( '%14.5n', [ Value ] );
    end;

    procedure GrabaValores( const nValor : Real ; i : Integer );
    begin
         with StringGrid do
         begin
              Cells[ i, 1 ] := FormatReal( nValor / K_FACTOR_MENSUAL );
              Cells[ i, 2 ] := FormatReal( nValor / K_FACTOR_MENSUAL * ZetaCommonTools.Dias7Tipo( tpSemanal ) );
              Cells[ i, 3 ] := FormatReal( nValor );
              Cells[ i, 4 ] := FormatReal( nValor / K_FACTOR_MENSUAL * ZetaCommonTools.Dias7Tipo( tpCatorcenal ) ) ;
              Cells[ i, 5 ] := FormatReal( nValor / K_FACTOR_MENSUAL * ZetaCommonTools.Dias7Tipo( tpQuincenal ) ) ;
              Cells[ i, 6 ] := FormatReal( nValor / K_FACTOR_MENSUAL * ZetaCommonTools.Dias7Tipo( tpDecenal ) ) ;
         end;
    end;
begin
    {***DevEx(by am): Como la ejecucion de CargaParametros se cambio de lugar es necesario agregarlos aqui para poder calcular la informacion en el StringGrid ***}
     with ParameterList do
     begin
          Clear;
          AddBoolean( 'CalcularBruto', CalcularBruto);
          AddFloat( 'Salario', rSalario.Valor);
          AddFloat( 'Subsidio', SubsidioAcreditable.Valor ); /// 100);
          AddFloat( 'Antiguedad' , nAntig.Valor);
          AddString( 'ZonaGeografica', sZonaGe.Items[ sZonaGe.ItemIndex ] );
          AddString( 'TablaPrestaciones', sTablaPresta.Llave);
          AddString( 'OtrasPercepciones', sOtrasPer.Text);
          AddDate( 'Fecha', dReferencia.Valor);
          AddInteger( 'Periodo', nPeriodo.LlaveEntero); //acl
          AddFloat( 'Redondeo', rRedondeo.Valor);
          AddInteger( 'Clasifi', Ord(GetClasificacionPeriodo(FListaTiposPeriodoConfidencialidad, nPeriodo.LlaveEntero)));
     end;
     {***}
     if dmProcesos.SalNetoBruto( ParameterList ) then
     begin
         with ParameterList do
         begin
              SalarioMinimo.Caption     := FormatReal( ParamByName( 'SalMin' ).AsFloat );
              FactorIntegracion.Caption := FormatTasa( ParamByName( 'Factor' ).AsFloat );
              SalarioIntegrado.Caption  := FormatReal( ParamByName( 'Integrado' ).AsFloat );
              GrabaValores( ParamByName( 'Bruto' ).AsFloat, 1 );
              GrabaValores( ParamByName( 'MontoOtrasP' ).AsFloat, 2 );
              GrabaValores( ParamByName( 'ISPT' ).AsFloat, 3 );
              GrabaValores( ParamByName( 'IMSS' ).AsFloat, 4 );
              GrabaValores( Redondea( ParamByName( 'Bruto' ).AsFloat + ParamByName( 'MontoOtrasP' ).AsFloat -
                            ParamByName( 'ISPT' ).AsFloat - ParamByName( 'IMSS' ).AsFloat ), 5);
         end;
         {$ifdef TEST_COMPLETE}
         GeneraASCIICompare;
         {$endif}

     end;

     with Descripciones do
     begin
          Clear;
          AddString( 'Salario m�nimo',SalarioMinimo.Caption);
          AddString( 'Factor de integraci�n', FactorIntegracion.Caption);
          AddString( 'Salario integrado', SalarioIntegrado.Caption );
          {$ifdef FALSE}
          AddFloat( 'Proporci�n de subsidio', SubsidioAcreditable.Valor );
          {$endif}
          AddString( 'Prestaciones', sTablaPresta.Llave +' '+ sTablaPresta.Descripcion );
          AddString( 'Zona geogr�fica', sZonaGe.Items[ sZonaGe.ItemIndex ] );
          if(sOtrasPer.Text <> '') then
          begin
               AddString( 'Otras percepciones', sOtrasPer.Text);
          end;
          AddFloat( 'Antig�edad' , nAntig.Valor);
          AddDate( 'Fecha', dReferencia.Valor);
          AddInteger( 'Periodo', nPeriodo.LlaveEntero); //acl
          AddFloat( 'Redondeo', rRedondeo.Valor);
     end;
     Imprimir.Enabled := True;
     Exportar.Enabled := True;
end;

procedure TWizNomSalNetoBruto_DevEx.StringGridDrawCell(Sender: TObject;
  ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  inherited;
  with StringGrid, Canvas, Brush do
    begin
         if gdSelected in State then
         begin
              Color:= RGB(212,188,251);//RGB(136,174,213); //Azul de celda seleccionado
              Font.Color := clHighlightText; //Texto en blanco para celda seleccionada
         end
         else
         begin
              Color:= Color;
              Font.Color :=  Font.Color;
         end;
         //DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;

end;

end.

