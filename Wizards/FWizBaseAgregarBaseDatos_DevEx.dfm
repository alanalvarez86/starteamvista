inherited WizBaseAgregarBaseDatos_DevEx: TWizBaseAgregarBaseDatos_DevEx
  Left = 486
  Top = 248
  Caption = 'WizBaseAgregarBaseDatos_DevEx'
  ClientHeight = 409
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Height = 409
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 21
      end
      inherited cxGroupBox1: TcxGroupBox
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          AnchorY = 36
        end
      end
      inherited GrupoAvance: TcxGroupBox
        Top = 86
      end
    end
    inherited NuevaBD2: TdxWizardControlPage
      inherited lblUbicacion: TLabel
        Left = 97
        Top = 50
      end
      inherited GBUsuariosMSSQL: TcxGroupBox
        Left = 96
        Top = 72
        Width = 361
        inherited txtUsuario: TEdit
          Left = 104
        end
        inherited txtClave: TMaskEdit
          Left = 104
        end
        inherited rbSameUser: TcxRadioButton
          Left = 56
        end
        inherited rbOtroUser: TcxRadioButton
          Left = 56
        end
      end
      inherited lblDatoUbicacion: TcxLabel
        Left = 152
        Top = 48
        Width = 305
      end
    end
    object NuevaBD1: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 
        'Seleccione los par'#225'metros de cantidad de solicitantes y de docum' +
        'entos digitales.'
      Header.Title = 'Cantidad de solicitantes y documentos digitales'
      object lblCantidadEmpleados: TLabel
        Left = 116
        Top = 58
        Width = 57
        Height = 13
        Caption = 'Solicitantes:'
        Transparent = True
      end
      object lblCantEmpSeleccion: TLabel
        Left = 391
        Top = 56
        Width = 100
        Height = 13
        Caption = 'lblCantEmpSeleccion'
        Transparent = True
      end
      object lblDocumentos: TLabel
        Left = 68
        Top = 104
        Width = 104
        Height = 13
        Caption = 'Documentos digitales:'
        Transparent = True
      end
      object lblDocSeleccion: TLabel
        Left = 391
        Top = 102
        Width = 77
        Height = 13
        Caption = 'lblDocSeleccion'
        Transparent = True
      end
      object lblBaseDatos: TLabel
        Left = 98
        Top = 168
        Width = 73
        Height = 13
        Caption = 'Base de Datos:'
        Transparent = True
      end
      object lblTamanoSugerido: TLabel
        Left = 86
        Top = 184
        Width = 85
        Height = 13
        Caption = 'Tama'#241'o sugerido:'
        Transparent = True
      end
      object lblLogTransacciones: TLabel
        Left = 66
        Top = 200
        Width = 105
        Height = 13
        Caption = 'Log de transacciones:'
        Transparent = True
      end
      object lblBaseDatosValor: TLabel
        Left = 184
        Top = 168
        Width = 3
        Height = 13
        Transparent = True
      end
      object lblTamanoSugeridoValor: TLabel
        Left = 184
        Top = 184
        Width = 3
        Height = 13
        Transparent = True
      end
      object lblLogTransaccionesValor: TLabel
        Left = 184
        Top = 200
        Width = 3
        Height = 13
        Transparent = True
      end
      object tbEmpleados: TcxTrackBar
        Left = 174
        Top = 42
        ParentFont = False
        Position = 300
        Properties.Frequency = 1200
        Properties.Max = 5000
        Properties.Min = 300
        Properties.TickType = tbttTicksAndNumbers
        Properties.OnChange = tbEmpleadosPropertiesChange
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = []
        Style.TextStyle = []
        Style.IsFontAssigned = True
        TabOrder = 0
        Height = 49
        Width = 214
      end
      object tbDocumentos: TcxTrackBar
        Left = 174
        Top = 88
        Position = 1
        Properties.Frequency = 3
        Properties.Max = 12
        Properties.Min = 1
        Properties.TickType = tbttTicksAndNumbers
        Properties.OnChange = tbDocumentosPropertiesChange
        TabOrder = 1
        Height = 49
        Width = 214
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    Left = 504
    Top = 35
  end
end
