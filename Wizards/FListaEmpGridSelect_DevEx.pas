unit FListaEmpGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, ExtCtrls, dbClient, ZBaseSelectGrid_DevEx,
  cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxButtons, cxTextEdit, cxCalendar;

type

  TListaEmpGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    gberrores: TGroupBox;
    dsErrores: TDataSource;
    CB_CODIGO: TcxGridDBColumn;
    CB_APE_PAT: TcxGridDBColumn;
    CB_APE_MAT: TcxGridDBColumn;
    CB_NOMBRES: TcxGridDBColumn;
    ZetaDBGridDBTableView_griderrores: TcxGridDBTableView;
    griderroresLevel1: TcxGridLevel;
    griderrores: TZetaCXGrid;
    RENGLON: TcxGridDBColumn;
    CB_CODIGO_GRID: TcxGridDBColumn;
    CB_APE_PAT_GRID: TcxGridDBColumn;
    CB_APE_MAT_GRID: TcxGridDBColumn;
    CB_NOMBRES_GRID: TcxGridDBColumn;
    CB_FEC_ING: TcxGridDBColumn;
    CB_SALARIO: TcxGridDBColumn;
    NUM_ERROR: TcxGridDBColumn;
    DESC_ERROR: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ZetaDBGridDBTableView_griderroresCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
  private
    { Private declarations }
    cdsErrores: TClientDataSet;
    Filtro_Actual: String;
    Filtrado_Actual: Boolean;
    procedure AgregaColumnasGrid;
    procedure SetControles( const lEnabled: Boolean );
    procedure SetFiltro( const sFiltro: String; oDataSet: TDataSet );
  protected
    //DevEx
    procedure ApplyMinWidthGrid( ZetaDBGridDBTableView: TcxGridDBTableView);
  public
    { Public declarations }
  end;

var
  ListaEmpGridSelect_DevEx: TListaEmpGridSelect_DevEx;

implementation

uses  dGlobal, ZetaCommonLists, ZetaCommonTools, ZetaCommonClasses, ZetaDialogo, //FBaseReportes,
  ZBasicoSelectGrid_DevEx, ZetaClientDataset;
{$R *.DFM}

const
     K_FILTRO_ERRORES = 'NUM_ERROR > 0';
     K_FILTRO_DATASET = 'NUM_ERROR = 0';

{ TListaEmpGridSelect }
procedure TListaEmpGridSelect_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     cdsErrores := TClientDataSet.Create( nil );
end;

procedure TListaEmpGridSelect_DevEx.FormShow(Sender: TObject);
begin
     with cdsErrores do
     begin
          Data := DataSet.Data;
          SetFiltro( K_FILTRO_ERRORES, cdsErrores );
          gbErrores.Visible := not IsEmpty;
          dsErrores.DataSet := cdsErrores;
     end;
     with DataSet do
     begin
          Filtro_Actual := Filter;
          Filtrado_Actual := Filtered;
     end;
     SetFiltro( K_FILTRO_DATASET, DataSet );
     SetControles( not DataSet.IsEmpty );
     AgregaColumnasGrid;  // No Habr� problema con siempre Agregar Columnas por que se esta haciendo un free de la forma al terminar
     inherited;
     ApplyMinWidthGrid( ZetaDBGridDBTableView_griderrores );      //DevEx
     ZetaDBGridDBTableView_griderrores.ApplyBestFit();
     ZetaDBGridDBTableView_griderrores.OptionsCustomize.ColumnHorzSizing := False;

end;

procedure TListaEmpGridSelect_DevEx.SetFiltro( const sFiltro: String; oDataSet: TDataSet );
begin
     with oDataSet do
     begin
          Filtered := False;
          if (strLleno(sFiltro)) then
          begin
               Filter := sFiltro;
               Filtered := True;
          end;
     end;
end;

procedure TListaEmpGridSelect_DevEx.ZetaDBGridDBTableView_griderroresCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  With ACanvas do
      Font.Color := clRed;
end;

procedure TListaEmpGridSelect_DevEx.SetControles( const lEnabled: Boolean );
begin
     Excluir.Enabled := lEnabled;
     Revertir.Enabled := lEnabled;
     Ok_DevEx.Enabled := lEnabled;
     ZetaDbGrid.Visible := lEnabled;
     if not lEnabled then
          gbErrores.Align := alClient
     else
          gbErrores.Align := alTop;
end;

procedure TListaEmpGridSelect_DevEx.AgregaColumnasGrid;
var
   i : Integer;

   //procedure AgregarColumna( const sCampo: String; const iAncho: Integer; const sTitulo: String );
   procedure AgregarColumna( const sCampo: String; const iAncho: Integer; const sTitulo: String; const bEsFecha: Boolean = False );   //DevEx , agregado para dar formato a fechas
   begin
        //with ZetaDBGrid.Columns.Add do
        with ZetaDBGridDBTableView.CreateColumn do    //DevEx
        begin
             //FieldName:= sCampo;
             DataBinding.FieldName:= sCampo;    //DevEx
             Width := iAncho;
             //Title.Caption:= sTitulo;
             Caption:= sTitulo;  //DevEx

             //DevExpara dar formato a fecha
             if ( bEsFecha ) then
             begin
                  PropertiesClass := TcxDateEditProperties;
                  (Properties as TcxDateEditProperties).Kind := ckDateTime;
                  (Properties as TcxDateEditProperties).DisplayFormat := 'dd/mmm/yyyy';
             end;
        end;
   end;

begin
     AgregarColumna( 'CB_RFC', 110, 'R.F.C.' );
     AgregarColumna( 'CB_SEGSOC', 90, 'I.M.S.S.' );
     AgregarColumna( 'CB_CURP', 110, 'C.U.R.P.' );
     AgregarColumna( 'CB_BAN_ELE', 110, 'Banca Electr�nica' );
     AgregarColumna( 'CB_FEC_ING', 70, 'Ingreso', True );
     AgregarColumna( 'CB_FEC_ANT', 70, 'Antig. desde', True );
     AgregarColumna( 'CB_CONTRAT', 45, 'Contrato' );
     AgregarColumna( 'CB_FEC_CON', 75, 'Inicio Contrato', True );
     AgregarColumna( 'CB_PUESTO', 43, 'Puesto' );
     AgregarColumna( 'CB_CLASIFI', 40, 'Clasif.' );
     AgregarColumna( 'CB_TURNO', 42, 'Turno' );
     for i := 1 to Global.NumNiveles do
     begin
          AgregarColumna( 'CB_NIVEL' + IntToStr( i ), 60, Global.NombreNivel( i ) );
     end;
     AgregarColumna( 'CB_AUTOSAL', 25, 'Tab' );
     AgregarColumna( 'CB_SALARIO', 65, 'Salario' );
     AgregarColumna( 'CB_PER_VAR', 100, 'Percep. Variables' );
     AgregarColumna( 'COD_PERCEPS', 80, 'Percep. Fijas' );
     AgregarColumna( 'CB_ZONA_GE', 30, 'Zona' );
     AgregarColumna( 'CB_TABLASS', 54, 'Tabla Ley' );
     AgregarColumna( 'CB_NOMINA', 45, 'N�mina' );
end;

procedure TListaEmpGridSelect_DevEx.FormDestroy(Sender: TObject);
begin
     FreeAndNil( cdsErrores );
end;

procedure TListaEmpGridSelect_DevEx.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     inherited;
     with DataSet do
     begin
          Filtered := False;
          Filter := Filtro_Actual;
          Filtered := Filtrado_Actual;
     end;
end;

//DevEx
procedure TListaEmpGridSelect_DevEx.ApplyMinWidthGrid( ZetaDBGridDBTableView: TcxGridDBTableView);
var
   i: Integer;
const
     widthColumnOptions =30;
begin
     with  ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;

end;

end.
