unit FNomImportarPagoRecibosGridSelect_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, StdCtrls, Buttons, ExtCtrls, Db,
     ZBaseSelectGrid_DevEx,
     ZetaDBGrid, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxButtons, cxCalendar;

type
  TNomImportarPagoRecibosGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    NO_FEC_PAG: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure NO_FEC_PAGGetText(Sender: TField; var Text: String; DisplayText: Boolean);
  public
    { Public declarations }
  end;

var
  NomImportarPagoRecibosGridSelect_DevEx: TNomImportarPagoRecibosGridSelect_DevEx;

implementation

uses ZetaCommonTools, ZetaCommonClasses;

{$R *.DFM}

procedure TNomImportarPagoRecibosGridSelect_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with DataSet do
          FieldByName( 'NO_FEC_PAG' ).OnGetText := NO_FEC_PAGGetText;   //PENDIENTE REVISAR QUE FUNCIONE
end;

procedure TNomImportarPagoRecibosGridSelect_DevEx.NO_FEC_PAGGetText(
          Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if ( Sender.AsDateTime = NullDateTime ) then
        Text := 'Sin Pagar'
     else
        Text := ZetaCommonTools.FechaCorta( Sender.AsDateTime );
end;

end.
