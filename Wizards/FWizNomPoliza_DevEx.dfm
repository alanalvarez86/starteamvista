inherited WizNomPoliza_DevEx: TWizNomPoliza_DevEx
  Left = 334
  Top = 268
  Caption = 'Generar P'#243'liza Contable'
  ClientHeight = 403
  ClientWidth = 494
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 494
    Height = 403
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Este proceso genera la p'#243'liza contable para el per'#237'odo de n'#243'mina' +
        ' activo en base a un formato prestablecido.'
      Header.Title = 'Generar p'#243'liza contable'
      object FormatoLBL: TLabel [0]
        Left = 26
        Top = 29
        Width = 70
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de &P'#243'liza:'
        Transparent = True
      end
      object lbRangonominas: TLabel [1]
        Left = 5
        Top = 245
        Width = 91
        Height = 13
        Alignment = taRightJustify
        Caption = 'Sumar las n'#243'minas:'
        Transparent = True
      end
      object bRangoPeriodo: TcxButton [2]
        Left = 376
        Top = 241
        Width = 21
        Height = 21
        Hint = 'Buscar periodo de n'#243'mina'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = bRangoPeriodoClick
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A4050000000000000000000000000000000000008B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF968D97FFE9E7E9FFAAA3ABFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8D838EFFE2DFE2FFFFFF
          FFFFFDFDFDFF9B939CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFCAC6CBFFFFFFFFFFFFFFFFFFC8C4C9FF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFB1ABB2FFFFFFFFFFFFFFFFFFE0DDE0FF8D838EFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF9B939CFFD0CCD0FFF2F1F2FFFBFBFBFFE2DFE2FFFAF9FAFFFFFFFFFFF0EF
          F1FF948B95FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF9B939CFFF4F3F4FFFFFFFFFFFBFBFBFFF2F1F2FFFFFF
          FFFFFFFFFFFFFBFBFBFFA39BA3FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFDBD7DBFFFFFFFFFFD5D2
          D6FF968D97FF8B818CFFA39BA3FFF4F3F4FFFFFFFFFFA199A2FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF968D
          97FFFFFFFFFFE9E7E9FF8B818CFF8B818CFF8B818CFF8B818CFFB5AEB5FFFFFF
          FFFFCAC6CBFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF9A919AFFFFFFFFFFD3D0D4FF8B818CFF8B818CFF8B81
          8CFF8B818CFF9A919AFFFFFFFFFFD3D0D4FF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF908791FFFBFBFBFFEFED
          EFFF8D838EFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFC7C2C7FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFD7D4D7FFFFFFFFFFD2CED2FF928993FF8F8590FFBEB8BFFFF6F5
          F6FFFDFDFDFF9B939CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF928993FFEDEBEDFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFBEB8BFFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF928993FFC5C0C6FFF0EFF1FFF0EFF1FFDBD7DBFFAAA3ABFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
        OptionsImage.Margin = 1
      end
      inherited GroupBox1: TGroupBox
        Left = 35
        Top = 48
        TabOrder = 1
      end
      object Formato: TZetaKeyLookup_DevEx
        Left = 99
        Top = 1
        Width = 300
        Height = 21
        Filtro = 'RE_TIPO=3 or RE_TIPO=4'
        LookupDataset = dmReportes.cdsLookupReportes
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidKey = FormatoValidKey
      end
      object eRangoNominas: TEdit
        Left = 99
        Top = 241
        Width = 274
        Height = 21
        TabOrder = 2
      end
      object TipoPoliza: TZetaKeyLookup_DevEx
        Left = 99
        Top = 25
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsTiposPoliza
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        OnValidKey = TipoPolizaValidKey
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 168
        Width = 472
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 472
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 404
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      PageVisible = False
      inherited sCondicionLBl: TLabel
        Top = 134
      end
      inherited sFiltroLBL: TLabel
        Top = 163
      end
      inherited Seleccionar: TcxButton
        Top = 251
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Top = 131
      end
      inherited sFiltro: TcxMemo
        Top = 162
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Top = 1
      end
      inherited bAjusteISR: TcxButton
        Top = 163
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 416
    Top = 26
  end
end
