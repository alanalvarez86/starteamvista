unit FEmpPromVarGridSelect_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
      StdCtrls, ExtCtrls, Db,
      ZBaseSelectGrid_DevEx, cxGraphics, cxLookAndFeels,
      cxLookAndFeelPainters, Menus, dxSkinsCore, 
      TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
      dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
      cxEdit, cxNavigator, cxDBData, cxTextEdit, cxGridCustomTableView,
      cxGridTableView, cxGridDBTableView, ImgList, cxGridLevel, cxClasses,
      cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons;

type
  TEmpPromVarGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CB_PER_VAR: TcxGridDBColumn;
    NUEVA_PERC: TcxGridDBColumn;
    DIFERENCIA: TcxGridDBColumn;
    PORCENTAJE: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpPromVarGridSelect_DevEx: TEmpPromVarGridSelect_DevEx;

implementation

uses ZBasicoSelectGrid_DevEx;

{$R *.DFM}

procedure TEmpPromVarGridSelect_DevEx.FormShow(Sender: TObject);
begin
     ZetaDBGridDBTableView.ApplyBestFit();
     inherited;
     with DataSet do
     begin
          MaskPesos( 'CB_PER_VAR' );
          MaskPesos( 'NUEVA_PERC' );
          MaskPesos( 'DIFERENCIA' );
          //MaskTasa( 'PORCENTAJE' );
          MaskNumerico( 'PORCENTAJE', '#,##0.00 %' );
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

end.
