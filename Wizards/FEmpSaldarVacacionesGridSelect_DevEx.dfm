inherited EmpSaldarVacacionesGridSelect_DevEx: TEmpSaldarVacacionesGridSelect_DevEx
  Left = 439
  Top = 296
  Caption = 'Seleccione los Registros de Saldar Vacaciones'
  ClientWidth = 719
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 719
    inherited OK_DevEx: TcxButton
      Left = 552
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 630
    end
  end
  inherited PanelSuperior: TPanel
    Width = 719
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 719
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object ANIV_VENCE: TcxGridDBColumn [2]
        Caption = 'Aniv. Vencido'
        DataBinding.FieldName = 'ANIV_VENCE'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
      end
      object FEC_LIMITE: TcxGridDBColumn [3]
        Caption = 'Vencimiento'
        DataBinding.FieldName = 'FEC_LIMITE'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
      end
      inherited CB_TOT_GOZ: TcxGridDBColumn
        Caption = 'Saldo Gozo'
      end
      inherited CB_TOT_PAG: TcxGridDBColumn
        Caption = 'Saldo Pago'
      end
      inherited CB_TOT_PV: TcxGridDBColumn
        Caption = 'Saldo prima vac.'
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
