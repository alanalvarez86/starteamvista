unit FWizNomDeclaracionAnual_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FWizEmpBaseFiltro, ComCtrls, ZetaEdit, StdCtrls, Buttons,
  ZetaCommonLists,ZetaWizard, ExtCtrls, Mask, ZetaNumero, ZetaKeyCombo,
  cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl;

type
  TWizNomDeclaracionAnual_DevEx = class(TWizEmpBaseFiltro)
    GroupBox1: TGroupBox;
    SubsidioAcreditableLBL: TLabel;
    Label1: TLabel;
    iYearLBL: TLabel;
    lbStatus: TLabel;
    SubsidioAcreditable: TZetaNumero;
    cbRepetidos: TZetaKeyCombo;
    zmYear: TZetaNumero;
    chbRedonEnteros: TCheckBox;
    btnConfiguraFormulas: TcxButton;
    procedure btnConfiguraFormulasClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure zmYearExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);//DevEx(by am): Agregado para enfocar correctamente los controles.
  private
    { Private declarations }
    FStatus : eStatusDeclaracionAnual;
    function GetStatusDeclaraText: string;
    procedure MensajeStatus;
    function SetStatusDeclara: Boolean;
  public
    { Public declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaParametros;override;
    procedure CargaListaVerificacion; override;

  end;

var
  WizNomDeclaracionAnual_DevEx: TWizNomDeclaracionAnual_DevEx;


implementation

uses
     DProcesos,
     DCliente,
     DGlobal,
     DNomina,
     ZetaCommonClasses,
     ZBaseSelectGrid_DevEx,
     ZetaDialogo,
     ZGlobalTress,
     FNomDeclaracionAnualFormulas_DevEx,
     FNomDeclaracionAnualGridSelect_DevEx;

{$R *.DFM}

{ TWizNomDeclaracionAnual }

procedure TWizNomDeclaracionAnual_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H33518_Declaracion_anual;
     ECondicion.Tag := K_GLOBAL_DEF_COMP_ANUAL_CONDICION;
     sFiltro.Tag := K_GLOBAL_DEF_COMP_ANUAL_FILTROS;

     ParametrosControl := zmYear;
     cbRepetidos.ItemIndex := 0;

     with dmCliente.GetDatosPeriodoActivo do
     begin
          zmYear.Valor := Year - 1;
     end;

     dmNomina.cdsDeclaracionesAnuales.Conectar;
     MensajeStatus;
end;

procedure TWizNomDeclaracionAnual_DevEx.CargaListaVerificacion;
begin
     dmProcesos.DeclaracionAnualGetLista( ParameterList );
end;

procedure TWizNomDeclaracionAnual_DevEx.CargaParametros;
begin
     inherited;
     with Descripciones do
     begin
          AddInteger( 'A�o', zmYear.ValorEntero );
          AddString( 'Empleados repetidos', ObtieneElemento( lfEmpleadoRepetido, cbRepetidos.ItemIndex ) );
          AddFloat( 'Proporci�n de subsidio', SubsidioAcreditable.Valor );
          AddBoolean( 'Redondear a enteros', chbRedonEnteros.Checked);
     end;
     with ParameterList do
     begin
          AddInteger( 'Year', zmYear.ValorEntero );
          AddInteger( 'Repetidos', cbRepetidos.ItemIndex );
          AddFloat( 'Subsidio', SubsidioAcreditable.Valor );
          AddBoolean( 'Redondear', chbRedonEnteros.Checked);
     end;
end;

function TWizNomDeclaracionAnual_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.DeclaracionAnual(ParameterList, Verificacion);
end;

function TWizNomDeclaracionAnual_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TNomDeclaracionAnualGridSelect_DevEx );
end;

procedure TWizNomDeclaracionAnual_DevEx.btnConfiguraFormulasClick(Sender: TObject);
begin
     inherited;
     Self.Tag := zmYear.ValorEntero;  // validacion Reforma Fiscal 2008
     if NomDeclaracionAnualFormulas_DevEx = NIL then
        NomDeclaracionAnualFormulas_DevEx := TNomDeclaracionAnualFormulas_DevEx.Create( Self ); // validacion Reforma Fiscal 2008

     NomDeclaracionAnualFormulas_DevEx.ShowModal;
     FreeAndNil( NomDeclaracionAnualFormulas_DevEx );
end;

procedure TWizNomDeclaracionAnual_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   rSubsidio: TTasa;
begin
     ParametrosControl := nil;
     if Wizard.Adelante then
     begin
          if Wizard.EsPaginaActual( Parametros ) then
          begin
               if ( FStatus =esdCerrada ) then
                  CanMove := Error( 'No es posible calcular una Declaraci�n que ya est� cerrada',zmYear );

               rSubsidio := SubsidioAcreditable.Valor;
               if ( rSubsidio < 0 ) or ( rSubsidio > 1 ) then
               begin
                    CanMove := Error( '� La Proporci�n Del Subsidio ' +
                                      'Acreditable Debe De Ser Un Valor Entre 0 - 1',
                                      SubsidioAcreditable );
               end;
          end;
     end;
     inherited;
end;

procedure TWizNomDeclaracionAnual_DevEx.zmYearExit(Sender: TObject);
begin
     inherited;
     MensajeStatus
end;

function TWizNomDeclaracionAnual_DevEx.SetStatusDeclara: Boolean;
begin
     Result := dmNomina.cdsDeclaracionesAnuales.Locate( 'TD_YEAR', zmYear.ValorEntero, [] );
     if Result then
        FStatus := eStatusDeclaracionAnual( dmNomina.cdsDeclaracionesAnuales.FieldByName( 'TD_STATUS' ).AsInteger )
     else
         FStatus := eStatusDeclaracionAnual( 0 );
end;


procedure TWizNomDeclaracionAnual_DevEx.MensajeStatus;
begin
     SetStatusDeclara;
     lbStatus.Caption := 'Status: ' +GetStatusDeclaraText;
end;

function TWizNomDeclaracionAnual_DevEx.GetStatusDeclaraText: string;
begin
     Result := ObtieneElemento( lfStatusDeclaracionAnual, Integer( FStatus ) )
end;

procedure TWizNomDeclaracionAnual_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     eCondicion.Llave := Global.GetGlobalString( K_GLOBAL_DEF_COMP_ANUAL_CONDICION );
     sFiltro.Text := Global.GetGlobalString( K_GLOBAL_DEF_COMP_ANUAL_FILTROS );
     Advertencia.Caption := 'El proceso calcula y almacena los datos para posteriormente obtener los reportes que permiten presentar la Declaraci�n Anual de Sueldos y Salarios.';
end;

procedure TWizNomDeclaracionAnual_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.Adelante then
          if Wizard.EsPaginaActual( Parametros ) then
             ParametrosControl := zmYear;
     inherited;
end;

end.

