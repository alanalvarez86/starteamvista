unit FImportarTablaCSVGridShow_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, ExtCtrls, ZBaseGridShow_DevEx,
  cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, cxControls, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ZetaCXGrid, cxButtons;

type
  TImportarTablaCSVGridShow_DevEx = class(TBaseGridShow_DevEx)
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }   
    procedure AgregaColumnasGrid;      
    procedure AgregarColumna( const sCampo: String; const iAncho: Integer; const sTitulo: String);
  public
    { Public declarations }
  end;

var
  ImportarTablaCSVGridShow_DevEx: TImportarTablaCSVGridShow_DevEx;

implementation

{$R *.DFM}

procedure TImportarTablaCSVGridShow_DevEx.FormShow(Sender: TObject);
begin
     AgregarColumna('RENGLON', 58, 'Rengl�n');
     AgregaColumnasGrid;
     AgregarColumna('NUM_ERROR', 50, 'Errores');
     AgregarColumna('DESC_ERROR', 450, 'Descripci�n de errores');

     ApplyMinWidth;
     ZetaDBGridDBTableView.ApplyBestFit();
     ZetaDBGridDBTableView.OptionsCustomize.ColumnHorzSizing := False;
end;

procedure TImportarTablaCSVGridShow_DevEx.AgregaColumnasGrid;
const K_ANCHO_DEFAULT = 100;
var
   i : Integer;
begin
     // Agregar las columnas del DataSet
     for i:=0 to (DataSet.FieldCount-4) do
     begin
          AgregarColumna(DataSet.Fields[i].FieldName, K_ANCHO_DEFAULT, DataSet.Fields[i].FieldName);
     end
end;

procedure TImportarTablaCSVGridShow_DevEx.AgregarColumna( const sCampo: String; const iAncho: Integer; const sTitulo: String);
begin
   //with ZetaDBGrid.Columns.Add do
   with ZetaDBGridDBTableView.CreateColumn do   //DevEx
   begin
        //FieldName:= sCampo;
        DataBinding.FieldName:= sCampo;   //DevEx
        Width := iAncho;
        //Title.Caption:= sTitulo;
        Caption:= sTitulo;                //DevEx

   end;
end;

end.
