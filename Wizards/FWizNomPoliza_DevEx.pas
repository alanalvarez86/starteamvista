unit FWizNomPoliza_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, ExtCtrls,
     {$ifndef VER130}Variants,{$endif}     
     FWizNomBase_DevEx,
     ZetaKeyLookup_DevEx,
     ZetaDBTextBox, ZetaTipoEntidad,
     ZetaEdit, cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
     cxRadioGroup, cxTextEdit, cxMemo, cxButtons, dxGDIPlusClasses,
     cxImage, cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl;

type
  TWizNomPoliza_DevEx = class(TWizNomBase_DevEx)
    Formato: TZetaKeyLookup_DevEx;
    FormatoLBL: TLabel;
    lbRangonominas: TLabel;
    eRangoNominas: TEdit;
    bRangoPeriodo: TcxButton;
    TipoPoliza: TZetaKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    //DevEx Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
    procedure FormatoValidKey(Sender: TObject);
    procedure bRangoPeriodoClick(Sender: TObject);
    procedure TipoPolizaValidKey(Sender: TObject);
  private
    procedure HabilitaControles;
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros;override;
  public
    { Public declarations }
  end;

var
  WizNomPoliza_DevEx: TWizNomPoliza_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     zReportTools,
     ZGlobalTress,
     ZetaDialogo,
     DCatalogos,
     DCliente,
     DReportes,
     DProcesos,
     FTressShell, ZcxWizardBasico, DB;

{$R *.DFM}

//PENDIENTE, en el servidor se tiene que validar que si la poliza
//no tiene cuentas (CAMPOREP.COUNT=0), debe marcar un error

procedure TWizNomPoliza_DevEx.FormCreate(Sender: TObject);
begin
     TipoPoliza.Tag := K_GLOBAL_DEF_POLIZA;
     inherited;
     ParametrosControl := TipoPoliza;
     HelpContext:= H30346_Generar_poliza_contable;
     Formato.LookupDataset := dmReportes.cdsLookupReportes;
     TipoPoliza.LookupDataSet := dmCatalogos.cdsTiposPoliza;

     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;
end;

procedure TWizNomPoliza_DevEx.FormShow(Sender: TObject);
begin
     dmCatalogos.cdsTiposPoliza.Refrescar;
     dmReportes.cdsLookupReportes.Conectar;
     inherited;
     dmCatalogos.cdsPeriodo.Conectar;
     with dmCatalogos.cdsTiposPoliza do
     begin
          Formato.Llave := IntToStr(FieldByName('PT_REPORTE').AsInteger);
     end;
     eRangoNominas.Text := Vacio;
     HabilitaControles;

     Advertencia.Caption :=  'Al ejecutar el proceso se obtiene la p�liza contable del periodo de n�mina activa.'

end;

procedure TWizNomPoliza_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
 var
    oLista: TStrings;
    i, iNumero: integer;
begin
     ParametrosControl := nil;
     inherited;
     with Wizard do
     begin
          if Adelante then
          begin
               if EsPaginaActual( Parametros ) then
               begin
                    if ( ZetaCommonTools.StrVacio( TipoPoliza.LLave )  ) then
                    begin
                         CanMove := False;
                         ZetaDialogo.ZError( Caption, '� Tipo de P�liza no puede quedar vac�o !', 0 );
                         TipoPoliza.SetFocus;
                    end;
                    if ( ( Formato.Valor = 0 ) or ( ZetaCommonTools.StrVacio( Formato.Llave ) ) or ZetaCommonTools.StrVacio( Formato.Descripcion ) )then
                    begin
                         CanMove := False;
                         ZetaDialogo.ZError( Caption, '� El Tipo de P�liza no tiene un formato de P�liza v�lido !', 0 );
                         TipoPoliza.SetFocus;
                    end;

                    if ZetaCommonTools.StrLleno( eRangoNominas.Text ) then
                    begin
                         try
                            oLista := TStringList.Create;
                            oLista.CommaText := eRangoNominas.Text;

                            for i:= 0 to oLista.Count - 1 do
                            begin
                                 iNumero := StrToIntDef(oLista[i],-1);
                                 if iNumero < 0 then
                                 begin
                                      CanMove := False;
                                      ZetaDialogo.ZError( Caption, '� Los n�meros de per�odo no son v�lidos !', 0 );
                                      eRangoNominas.SetFocus;
                                      Break;
                                 end;
                            end;
                         finally
                                FreeAndNil( oLista );
                         end;
                    end;
               end;
          end;
     end;
end;

procedure TWizNomPoliza_DevEx.CargaParametros;
begin
     inherited;
     with ParameterList do
     begin
          AddString ('CodigoTipo',TipoPoliza.Llave);
          AddInteger( 'CodigoReporte', Formato.Valor );
          if ( TipoEntidad( dmReportes.cdsLookupReportes.FieldByName('RE_ENTIDAD').AsInteger ) = enNomina ) then
              AddString( 'RangoNominas', eRangoNominas.Text )
          else
              AddString( 'RangoNominas', VACIO );
          AddInteger( 'TipoPolizaReporte', dmReportes.cdsLookupReportes.FieldByName('RE_TIPO').AsInteger );
     end;

     //DevEx
     with Descripciones do
     begin
          AddString ('Tipo de p�liza',TipoPoliza.Llave + ': ' + TipoPoliza.Descripcion);
          if ( TipoEntidad( dmReportes.cdsLookupReportes.FieldByName('RE_ENTIDAD').AsInteger ) = enNomina ) then
             if not ( Trim(eRangoNominas.Text) = '') then
              AddString( 'Sumar las n�minas', eRangoNominas.Text );
     end;


end;

function TWizNomPoliza_DevEx.EjecutarWizard: Boolean;
var
   aFiltros,aFiltro: OleVariant;
   sFiltro: String;
   i: Integer;
   lDiccion: Boolean;
   Entidad: TipoEntidad;
begin
     i := 1;
     with Formato.LookUpDataSet do
     begin
         if LookupKey( Formato.Llave, '', sFiltro ) then
         begin
              with ParameterList do
              begin
                   AddString( 'Condicion', FieldBYName( 'QU_FILTRO' ).AsString );
                   AddString( 'Filtro', FieldByName( 'RE_FILTRO' ).AsString );
              end;
         end
         else
             Exception.Create( 'Error al posicionar LookUp de P�liza' );
     end;
     dmReportes.CodigoReporte := Formato.Valor;
     with dmReportes.cdsCampoRepFiltros do
     begin
          Active := FALSE;
          Conectar;
          if RecordCount > 0 then
             aFiltros := VarArrayCreate( [ 1, RecordCount ], varVariant )
          else
              SetOLEVariantToNull( aFiltros );
          while NOT EOF do
          begin
               sFiltro := zReportTools.GetCampoRepFiltro( dmReportes.cdsCampoRepFiltros,
                                                          lDiccion,
                                                          Entidad );
               aFiltro := VarArrayCreate( [ 1, 3 ], varVariant );
               aFiltro[ 1 ] := sFiltro;
               aFiltro[ 2 ] := lDiccion;
               aFiltro[ 3 ] := Ord( Entidad );
               aFiltros[ i ] := aFiltro;
               Inc( i );
               Next;
          end;
     end;
     Result := dmProcesos.Poliza(ParameterList, aFiltros);
end;

procedure TWizNomPoliza_DevEx.HabilitaControles;
 var
    oEntidad : TipoEntidad;
begin
     inherited;
     oEntidad := TipoEntidad( dmReportes.cdsLookupReportes.FieldByName('RE_ENTIDAD').AsInteger );
     lbRangoNominas.Visible := oEntidad = enNomina;
     eRangoNominas.Visible := oEntidad = enNomina;
     bRangoPeriodo.Visible := oEntidad = enNomina;
end;

procedure TWizNomPoliza_DevEx.FormatoValidKey(Sender: TObject);
begin
     inherited;
     HabilitaControles;
end;

procedure TWizNomPoliza_DevEx.bRangoPeriodoClick(Sender: TObject);
 var
    sLlave, sDescripcion: string;
begin
     inherited;
     if TressShell.BuscaDialogo( enPeriodo, '', sLlave, sDescripcion ) then
     begin
          if StrLleno( eRangoNominas.Text ) then
             eRangoNominas.Text := eRangoNominas.Text + ','+ sLlave
          else eRangoNominas.Text := sLlave;
     end;
end;

procedure TWizNomPoliza_DevEx.TipoPolizaValidKey(Sender: TObject);
begin
     inherited;
     Formato.Llave := IntToStr(dmCatalogos.cdsTiposPoliza.FieldByName('PT_REPORTE').AsInteger);
end;

procedure TWizNomPoliza_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := TipoPoliza
     else
         ParametrosControl := nil;
     inherited;
end;

end.

