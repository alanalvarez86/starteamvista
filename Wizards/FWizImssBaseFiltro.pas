unit FWizImssBaseFiltro;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, ExtCtrls,
     ZetaDBTextBox, ZetaEdit, ZCXBaseWizardFiltro,
     cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
     cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons,
     dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
     dxWizardControl, Mask, ZetaNumero, ZetaKeyCombo;

type
  TWizIMSSBaseFiltro = class(TBaseCXWizardFiltro)
    PeriodoGB: TcxGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    PatronLbl: TLabel;
    Mes: TZetaKeyCombo;
    Patron: TZetaKeyLookup_DevEx;
    Tipo: TZetaKeyCombo;
    Label3: TLabel;
    Anio: TZetaNumero;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
  private
    { Private declarations }
  protected
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizIMSSBaseFiltro: TWizIMSSBaseFiltro;

implementation

uses DCliente,
     ZetaCommonLists, dCatalogos, ZetaDialogo, ZetaCommonClasses, ZetaCommonTools;

{$R *.DFM}

procedure TWizIMSSBaseFiltro.FormCreate(Sender: TObject);
begin
     inherited;

     Patron.LookupDataset := dmCatalogos.cdsRPatron;
     with dmCatalogos.cdsRPatron do
     begin
          Conectar;
     end;

     with dmCliente do
     begin
          Patron.Llave := IMSSPatron;
          Tipo.ItemIndex := Ord( IMSSTipo ) ;
          Anio.Valor := IMSSYear;
          Mes.Valor := IMSSMes;
     end;

     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;
end;


procedure TWizIMSSBaseFiltro.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
   with Wizard do
     begin
        //DevEx
       if CanMove and Adelante and EsPaginaActual( Parametros ) then
       begin
          if ( StrVacio(Patron.Llave) ) then
          begin
               ZetaDialogo.zError( Caption, 'El valor del Registro Patronal no puede quedar vac�o', 0 );
               CanMove := False;
          end 
          else if ( Anio.Valor = 0 ) then
          begin
               ZetaDialogo.zError( Caption, 'El valor del A�o no puede quedar vac�o', 0 );
               CanMove := False;
          end
          else
          if not( (Anio.Valor > 1995) and (Anio.Valor < 3000)  ) then
          begin
                   ZetaDialogo.zError( Caption, 'El valor del a�o no es v�lido', 0 );
                   CanMove := False;
          end;
       end;  //validaciones

       if ( CanMove ) then
          with dmCliente do
          begin
               IMSSPatron := Patron.Llave;
               IMSSYear := Anio.ValorEntero ;
               IMSSMes := Mes.Valor;
               IMSSTipo := eTipoLiqIMSS( Tipo.Valor );
          end
       end;  //end canmove

 end;


procedure TWizIMSSBaseFiltro.CargaParametros;
begin
       inherited CargaParametros;
        with Descripciones do
        begin
             AddString( 'Registro patronal', Patron.Llave + ': ' + Patron.Descripcion );
             AddString( 'Mes', ObtieneElemento( lfMeses, Mes.ItemIndex ) );
             AddInteger( 'A�o', Anio.ValorEntero );
             AddString( 'Tipo de liquidaci�n', ObtieneElemento( lfTipoLiqIMSS, Ord( dmCliente.IMSSTipo ) ) );
        end;
end;


end.
