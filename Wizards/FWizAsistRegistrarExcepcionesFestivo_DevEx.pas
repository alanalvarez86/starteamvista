unit FWizAsistRegistrarExcepcionesFestivo_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ZetaEdit, StdCtrls,
  Buttons, ZetaWizard, ExtCtrls, Mask, ZetaFecha, ZCXBaseWizardFiltro,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, Menus, ZetaCXWizard, cxRadioGroup, cxTextEdit,
  cxMemo, ZetaKeyLookup_DevEx, cxButtons, dxGDIPlusClasses, cxImage,
  cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl, dxSkinsCore,
  TressMorado2013;

type
  TWizAsistRegistrarExcepcionesFestivo_DevEx = class(TBaseCXWizardFiltro)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    FechaTarjeta: TZetaFecha;
    ckFestivoTrabajado: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
  protected
    Procedure CargaParametros; override;
    procedure CargaListaVerificacion; override;
    function  Verificar: Boolean; override;
    function EjecutarWizard:Boolean; override;
  public
    { Public declarations }
  end;

var
  WizAsistRegistrarExcepcionesFestivo_DevEx: TWizAsistRegistrarExcepcionesFestivo_DevEx;
implementation

{$R *.dfm}
Uses DCliente,
     DProcesos,
     ZGlobalTress,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     ZBaseSelectGrid_DevEx,
     ZAccesosMgr,
     ZAccesosTress,
     DGlobal,
     FEmpleadoGridSelect_DevEx;

procedure TWizAsistRegistrarExcepcionesFestivo_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := FechaTarjeta;
     with dmCliente do
     begin
          FechaTarjeta.Valor := FechaAsistencia;
     end;
     HelpContext := H_Registrar_Excepciones_Festivo;
end;

Procedure TWizAsistRegistrarExcepcionesFestivo_DevEx.CargaParametros;
begin

     inherited CargaParametros;

     with ParameterList do
     begin
          AddDate('Fecha', FechaTarjeta.Valor);
          AddBoolean( 'PuedeCambiarTarjeta', ZAccesosMgr.CheckDerecho( D_ASIS_BLOQUEO_CAMBIOS, K_DERECHO_MODIFICAR_TARJETAS_ANTERIORES ) );
          AddBoolean ( 'FestivoTrabajado', ckFestivoTrabajado.Checked );
     end;
      with Descripciones do
     begin
          AddDate('Fecha de Tarjeta', FechaTarjeta.Valor);
          if ckFestivoTrabajado.Checked then
            AddString( 'Horario de Festivo trabajado', 'S' )
          else
            AddString( 'Horario de Festivo trabajado', 'N' )
     end;
end;

function TWizAsistRegistrarExcepcionesFestivo_DevEx.EjecutarWizard:Boolean;
begin
     Result := dmProcesos.RegistrarExcepcionesFestivos( ParameterList, Verificacion );
end;

procedure TWizAsistRegistrarExcepcionesFestivo_DevEx.CargaListaVerificacion;
begin
     dmProcesos.RegistrarExcepcionesFestivosGetLista( ParameterList );
end;

function TWizAsistRegistrarExcepcionesFestivo_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpleadoGridSelect_DevEx );
end;                                            

procedure TWizAsistRegistrarExcepcionesFestivo_DevEx.WizardBeforeMove(
  Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
ParametrosControl := nil;
     inherited;
     if CanMove and Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
        CanMove := dmCliente.PuedeCambiarTarjetaDlg(FechaTarjeta.Valor,FechaTarjeta.Valor);
end;

procedure TWizAsistRegistrarExcepcionesFestivo_DevEx.WizardAfterMove(
  Sender: TObject);
begin
  inherited;
  if Wizard.EsPaginaActual(Parametros) then
    ParametrosControl := FechaTarjeta;
end;

end.
