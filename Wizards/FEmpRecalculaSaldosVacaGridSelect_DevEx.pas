unit FEmpRecalculaSaldosVacaGridSelect_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, StdCtrls, Buttons, ExtCtrls, Db,
     ZBaseSelectGrid_DevEx,
     cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxButtons, cxCalendar;

type
  TEmpRecalculaSaldosVacaGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CB_FEC_ANT: TcxGridDBColumn;
    ZetaDBGridDBTableViewColumn4: TcxGridDBColumn;
    ZetaDBGridDBTableViewColumn5: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpRecalculaSaldosVacaGridSelect_DevEx: TEmpRecalculaSaldosVacaGridSelect_DevEx;

implementation

{$R *.DFM}

end.
