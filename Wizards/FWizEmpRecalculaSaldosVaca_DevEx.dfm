inherited WizEmpRecalculaSaldosVaca_DevEx: TWizEmpRecalculaSaldosVaca_DevEx
  Left = 394
  Top = 336
  Caption = 'Recalcular Saldos de Vacaciones'
  ClientHeight = 443
  ClientWidth = 434
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 434
    Height = 443
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Este proceso elimina todos los cierres de vacaciones y los vuelv' +
        'e a generar, lo cual sirve para corregir errores de a'#241'os atr'#225's.'
      Header.Title = 'Recalcular saldos de vacaciones'
      object Label1: TLabel
        Left = 93
        Top = 136
        Width = 98
        Height = 13
        Caption = '&Fecha de referencia:'
        FocusControl = FechaRef
        Transparent = True
      end
      object FechaRef: TZetaFecha
        Left = 197
        Top = 132
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '24/may/06'
        Valor = 38861.000000000000000000
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 208
        Width = 412
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 412
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 344
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 14
      end
      inherited sFiltroLBL: TLabel
        Left = 39
      end
      inherited Seleccionar: TcxButton
        Left = 142
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 66
      end
      inherited sFiltro: TcxMemo
        Left = 66
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 66
      end
      inherited bAjusteISR: TcxButton
        Left = 380
      end
    end
  end
end
