inherited WizNomCalculaCosteo_DevEx: TWizNomCalculaCosteo_DevEx
  Left = 586
  Top = 279
  Caption = 'C'#225'lculo de Costeo'
  ClientHeight = 352
  ClientWidth = 542
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 542
    Height = 352
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Este proceso prorratea los conceptos de n'#243'mina configurados para' +
        ' costeo en proporci'#243'n de las transferencias que tuvo el empleado' +
        ' durante el rango de fechas que cubre el periodo de n'#243'mina.'
      Header.Title = 'Calcular Costeo'
      inherited GroupBox1: TGroupBox
        Left = 55
        Top = 14
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 117
        Width = 520
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 520
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 452
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      PageVisible = False
      inherited sCondicionLBl: TLabel
        Top = 137
      end
      inherited sFiltroLBL: TLabel
        Top = 166
      end
      inherited Seleccionar: TcxButton
        Top = 254
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Top = 134
      end
      inherited sFiltro: TcxMemo
        Top = 165
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Top = 4
      end
      inherited bAjusteISR: TcxButton
        Left = 372
        Top = 166
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    Left = 352
    Top = 30
  end
end
