inherited WizEmpBorrarCursoTomado_DevEx: TWizEmpBorrarCursoTomado_DevEx
  Left = 406
  Top = 219
  Caption = 'Cancelar Curso Tomado Global'
  ClientHeight = 432
  ClientWidth = 451
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 451
    Height = 432
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        ' Proceso que elimina cualquier error que se haya registrado de m' +
        'anera global.'
      Header.Title = 'Cancelar curso tomado global'
      object KC_FEC_FINLbl: TLabel
        Left = 61
        Top = 154
        Width = 25
        Height = 13
        Alignment = taRightJustify
        Caption = '&Final:'
        FocusControl = dFinal
        Transparent = True
      end
      object Label2: TLabel
        Left = 60
        Top = 126
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = '&Inicio:'
        FocusControl = dInicio
        Transparent = True
      end
      object Label3: TLabel
        Left = 47
        Top = 98
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = '&Maestro:'
        FocusControl = sMaestro
        Transparent = True
      end
      object Label1: TLabel
        Left = 58
        Top = 70
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'C&urso:'
        FocusControl = sCurso
        Transparent = True
      end
      object dFinal: TZetaFecha
        Left = 90
        Top = 151
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 3
        Text = '26/ene/00'
        Valor = 36551.000000000000000000
      end
      object dInicio: TZetaFecha
        Left = 90
        Top = 123
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 2
        Text = '14/ene/98'
        Valor = 35809.000000000000000000
      end
      object sMaestro: TZetaKeyLookup_DevEx
        Left = 90
        Top = 95
        Width = 315
        Height = 21
        LookupDataset = dmCatalogos.cdsMaestros
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
      end
      object sCurso: TZetaKeyLookup_DevEx
        Left = 90
        Top = 67
        Width = 315
        Height = 21
        LookupDataset = dmCatalogos.cdsCursos
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 197
        Width = 429
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 429
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 361
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 22
        Top = 145
      end
      inherited sFiltroLBL: TLabel
        Left = 47
        Top = 168
      end
      inherited Seleccionar: TcxButton
        Left = 150
        Top = 262
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 74
        Top = 142
      end
      inherited sFiltro: TcxMemo
        Left = 74
        Top = 173
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 74
        Top = 12
      end
      inherited bAjusteISR: TcxButton
        Left = 388
        Top = 174
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
  end
end
