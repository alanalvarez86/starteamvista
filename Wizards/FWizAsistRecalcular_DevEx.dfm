inherited WizAsistRecalcular_DevEx: TWizAsistRecalcular_DevEx
  Left = 957
  Top = 90
  Caption = 'Rec'#225'lculo Tarjetas de Asistencia'
  ClientHeight = 567
  ClientWidth = 435
  ExplicitWidth = 441
  ExplicitHeight = 596
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 435
    Height = 567
    ExplicitWidth = 435
    ExplicitHeight = 600
    inherited Parametros: TdxWizardControlPage
      Header.AssignedValues = [wchvGlyph]
      Header.Glyph.Data = {
        89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
        F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
        6000003A980000176F925FC546000000097048597300000EBC00000EBC0195BC
        72490000040C494441545847C557494C144114E56094834671815150D091805B
        C025D1831A0F1E3C70F0608C070F9298E8C1188D0AA30E16030466BA67C47D89
        07BD7920D1840B070E1A4D34C41817446318511C05590C21A88808ED7F65D558
        74578F4B067CC9CB7455FFFAAFEAFFAADF35299665FD576A3B279249817579F5
        18B6FAD2B3DFB3B9FEDEEA9C8681485EAC2FE46DEA0864996D4767AFB5DB2605
        AAC3F7E59981918B2B47E9D9D2B1AB2ABBFEDEFE8C547A4EFE043E06BD4D1001
        074F2FFBF2B166E1EDCE40A64991A81F38B9A457BEFB7432BF3F5A32230D6392
        02387A579E1580F3D14BAB2C12BCF174DFE4F82A25DF96794A86CF178CC08E6C
        3059E1E137608CA51ACCD84C3C1C0998574D16F29BCCDC847EBC6F2EF164CBB0
        F7542FBC01C76F8EA71751CEA7E15965FBF13945F4CB23F1D69F5ECC051221C2
        82EBCC80F1221C302D0799E1834D8C65FAE110617FB26F0A5F39ADB0F1CBA9A5
        FDD1D299F968AB14ABE751E0226EA095EE25A16187B0240B6D855D77B5B7010E
        917355A8AB72FEB5EF170A47DAFC9EED6A7FAC2CE320FDF2BDC0857408B3703E
        890C3A4455920D6CFB234B6370D8513ECF5485C0767FC6CE914BABADE7A59E74
        D9D7E69B89E3C8D3C0C574A0B0DFD58AAA64E1F5B0ED09E6CA90D64B111BADA8
        6F8E57B62902C5E8FB7A66F91017B3A396D5CED00ADA489B7227ECA315795839
        42DA25456CB45A7C59F10974572DC046B5A840B570413BB0E375824E1A75B0BF
        5BB6311ED2F61373F7482185D663DF623E013AFF5E7962A85A9A5CD00EDADDBB
        F4820E0ED2643D70DC5593CB37E2B7F385DF5FFA7370D4E21378E95FC4DBAD47
        A6675369EE801DFDF63EDB9F3A49488E458885D668C45C68D4C179C3A182D4CF
        74ECE01CC5A8BBC6DBD87E22B338EA9BE5A58D584415F18A2C427432465F95A6
        ADC0382D5060C8B9FBF1B391A27000CEEE1CDB90D61DCA7B081137D23EE995E2
        A02B50F174626E44CD904E9F94AF29FE10CC6B1AA85DC623327476C550BF99DB
        82EF02C22EED40579C63E7A692E3D77621170E236D6268F220F642C24950BD18
        A0EFC2363124F9E091A830CE92509F4D7CD80C9837C765E56E88B0C862AC96D7
        09169E2DBAC70F2454088AA62BF05504514145D7BF838EE0241C2B0A79CFCF50
        1BD7C5AB94200B66E11E20EE02F12242B6ADB0459A3056DE13FE1AC837E5F5BE
        926730265EF314509BBE90C62DD145DF8D9A1C9B3DD16886AD30F933604570EC
        74665A10116629E18A70252E29A229EF0C8E31C44E444C98258610AFD338E1A4
        90EF10A624686E53739D681CF1819A2A57D0CD66B766F02F56842F0A5312341B
        29CF87F10CE79AE33996F461E303DD800D43869D8E8163F908B64ABE5F431CA9
        506CB4C406E5426EF8EDEA0569D55B1009A57D406D27A4B8BA694139BC4E4628
        B7E346750F39A0FBC33891D4764E1CAD941F494657DDDCD9B26B000000004945
        4E44AE426082}
      Header.Description = 'Se realizar'#225' la correcci'#243'n sobre las tarjetas.'
      Header.Title = 'Recalcular Tarjetas de Asistencia'
      ExplicitWidth = 413
      ExplicitHeight = 425
      object gbFechasAsistencia: TcxGroupBox
        Left = 38
        Top = 66
        Caption = 'Fechas de asistencia'
        TabOrder = 0
        Height = 103
        Width = 338
        object dInicialLBL: TLabel
          Left = 76
          Top = 30
          Width = 63
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha &Inicial:'
          Color = clWhite
          FocusControl = FechaInicial
          ParentColor = False
        end
        object dFinalLBL: TLabel
          Left = 81
          Top = 59
          Width = 58
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha &Final:'
          Color = clWhite
          FocusControl = FechaFinal
          ParentColor = False
        end
        object FechaInicial: TZetaFecha
          Left = 147
          Top = 26
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '02/Jan/98'
          Valor = 35797.000000000000000000
        end
        object FechaFinal: TZetaFecha
          Left = 147
          Top = 55
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 1
          Text = '02/Jan/98'
          Valor = 35797.000000000000000000
        end
      end
      object gbClasificacionTarjeta: TcxGroupBox
        Left = 38
        Top = 176
        Caption = 'Clasificaci'#243'n de tarjeta'
        TabOrder = 1
        Height = 105
        Width = 338
        object lRevisa: TCheckBox
          Left = 66
          Top = 26
          Width = 145
          Height = 17
          Caption = 'Revisar &Horarios Actuales'
          Checked = True
          Color = clWhite
          ParentColor = False
          State = cbChecked
          TabOrder = 0
          OnClick = lRevisaClick
        end
        object Reclasificar: TCheckBox
          Left = 66
          Top = 51
          Width = 206
          Height = 28
          Caption = '&Revisar Puesto, Area y Tipo de n'#243'mina'
          Color = clWhite
          ParentColor = False
          TabOrder = 1
          WordWrap = True
          OnClick = lRevisaClick
        end
      end
      object grReprocesarTarjetas: TcxGroupBox
        Left = 38
        Top = 288
        Caption = 'Reprocesar'
        TabOrder = 2
        Height = 64
        Width = 338
        object lReprocesarTarjetas: TCheckBox
          Left = 66
          Top = 27
          Width = 145
          Height = 17
          Caption = '&Reprocesar Tar&jetas'
          Color = clWhite
          ParentColor = False
          TabOrder = 0
          OnClick = lRevisaClick
        end
      end
    end
    object Cancelaciones: TdxWizardControlPage [1]
      Header.Description = 
        'Seleccione las opciones deseadas para efectuar cancelaciones de ' +
        'horarios temporales o de autorizaciones.'
      Header.Title = 'Efectuar cancelaciones'
      object gbCancelaciones: TcxGroupBox
        Left = 107
        Top = 102
        Caption = 'Efectuar cancelaciones de:'
        TabOrder = 0
        Height = 180
        Width = 202
        object GroupBox1: TGroupBox
          Left = 19
          Top = 47
          Width = 161
          Height = 114
          Caption = ' Autorizaciones '
          TabOrder = 1
          object lAutExtras: TCheckBox
            Left = 8
            Top = 16
            Width = 140
            Height = 17
            Caption = 'Horas E&xtras'
            TabOrder = 0
          end
          object lAutDescanso: TCheckBox
            Left = 8
            Top = 34
            Width = 140
            Height = 17
            Caption = '&Descanso Trabajado'
            TabOrder = 1
          end
          object lAutConGoce: TCheckBox
            Left = 8
            Top = 52
            Width = 140
            Height = 17
            Caption = 'Permisos C&on Goce'
            TabOrder = 2
          end
          object lAutSinGoce: TCheckBox
            Left = 8
            Top = 70
            Width = 140
            Height = 17
            Caption = 'Permisos S&in Goce'
            TabOrder = 3
          end
          object lAutHorasPrep: TCheckBox
            Left = 8
            Top = 88
            Width = 140
            Height = 17
            Caption = 'Horas Prepagadas'
            TabOrder = 4
          end
        end
        object lTemporales: TCheckBox
          Left = 27
          Top = 21
          Width = 119
          Height = 17
          Caption = 'Horarios &Temporales'
          TabOrder = 0
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage [2]
      ExplicitWidth = 413
      ExplicitHeight = 425
      inherited sCondicionLBl: TLabel
        Left = 7
        Top = 193
        ExplicitLeft = 7
        ExplicitTop = 193
      end
      inherited sFiltroLBL: TLabel
        Left = 32
        Top = 215
        ExplicitLeft = 32
        ExplicitTop = 215
      end
      inherited Seleccionar: TcxButton
        Left = 123
        Top = 345
        ExplicitLeft = 123
        ExplicitTop = 345
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 59
        Top = 189
        ExplicitLeft = 59
        ExplicitTop = 189
      end
      inherited sFiltro: TcxMemo
        Left = 59
        Top = 215
        Style.IsFontAssigned = True
        ExplicitLeft = 59
        ExplicitTop = 215
        ExplicitHeight = 123
        Height = 123
      end
      inherited GBRango: TGroupBox
        Left = 59
        Top = 53
        Height = 125
        ExplicitLeft = 59
        ExplicitTop = 53
        ExplicitHeight = 125
      end
      inherited bAjusteISR: TcxButton
        Left = 376
        Top = 214
        ExplicitLeft = 376
        ExplicitTop = 214
      end
    end
    inherited Ejecucion: TdxWizardControlPage [3]
      ExplicitWidth = 413
      ExplicitHeight = 425
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 413
        ExplicitHeight = 199
        Height = 330
        Width = 413
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 413
        Width = 413
        inherited Advertencia: TcxLabel
          Caption = 
            'Se recalcular'#225'n las tarjetas de los empleados seleccionados en e' +
            'l rango de fechas especificado.'
          Style.IsFontAssigned = True
          ExplicitLeft = 70
          ExplicitWidth = 341
          Width = 341
          AnchorY = 49
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 16
    Top = 69
  end
end
