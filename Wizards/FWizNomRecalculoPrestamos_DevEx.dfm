inherited WizNomRecalculoPrestamos_DevEx: TWizNomRecalculoPrestamos_DevEx
  Caption = 'Recalcular Pr'#233'stamos'
  ClientHeight = 412
  ClientWidth = 428
  ExplicitWidth = 434
  ExplicitHeight = 441
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 428
    Height = 412
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F40000000467414D410000B18F0BFC6105000000097048597300000EBC00000E
      BC0195BC72490000032A494441545847ED562D6C1441143E8140542010880A04
      028140542048402010E56696D004410202814020100812482A1025B9DEEE2508
      C40A64050281A8A8A8A828C9EDEE3541549C40549CA840204EC0F7BD793B377B
      B7D78683ABBA97BCECBD37EF6FDEDF5C63010B9807243D735B7F9E1DA4FDC7E7
      DBB9799AE4F67B52D8DF9B99BDA947F385F7DF562F26B9791BE7F6888E3DE6F6
      B38A343A076B97E2C26EE0BBA4AC7F87CEC1BD2B709CC0F1AF8AE30029F366E7
      D639FCEE9346865EA8FAECC0D4F276A1A3A98800A91367F695F2FA0C480CFD0D
      50A99D371FA0B93EC1482A06613C294CCF3BAB416687256A75CD8538373F859F
      450FD5ECE9C09A316DA85F17069E244573456F93B2A6A4D9F113F50F11FD21B6
      0ADB220DD97D317E12B069925EF40E0AC7BC45A76BAE938E7BD10722F87BA3DA
      9BF5B8B87F95B28EAE2283E384200B97410F853F6D4CC5116F57692C3AB0CF95
      4E698C72345CCA70FC6404BD4E157946FBF8BD25BC604204DA597417075F4B85
      10DB79B48CAFDC8EB5C4B74E6EA872E37C87D80DF42325531E27449C837051D5
      A0BB65A8B4B61417CD3B92119624B7875E3EB3ABEC154F8F23CEE90F323BA459
      4A09804D3621AC0807FBD2FD9E6776A519618C379005137437BE7B23D92AD2B1
      0BC058A1D95BD0974E07635A0341080BA7E66C9456B7644E6A44C86E97192050
      D7F1DD84E0F1908E9F542472CC346D825006BD819ADF70A329BC6DD25EC6E190
      7B830D2B4E000C12E3F89117533DB984DBD595CE0FD1F4340B03A5775936695C
      17F860338BAEE15BA6FF58035C16E3005E02CE82ED697E249979C9A656114965
      3A1298C05483A08C4B33D3880038DFBC158D32B0D2A8BC01E80B04E31B93BFE3
      AE7D54BB8E75078C3BF6880C1DC100F64173459C523EB3CF70B64547A55106E0
      1A1BB72C750BF365EAF20901C2B5BBA01699D2C02853CED4E34C32240D8CCCB0
      3C2A723AE842AA77082C8DB29954C565CE3D526ECD4A00665D466C16086B16E0
      8023131AD5C5E3A703C11DB2445CD52A321BF8D152A3AC7369D4FFEDAA3CC358
      4E582E33BDF37520DD8BFAD2A8B2E46F1782790D2C1FA0216538FB2A325F2827
      447BA0E51F92B3048E5A65712C6001FF051A8D3FA6D93DEE37EA8F1800000000
      49454E44AE426082}
    ExplicitWidth = 428
    ExplicitHeight = 412
    inherited Parametros: TdxWizardControlPage
      Header.AssignedValues = [wchvDescriptionFont]
      Header.Description = 
        'Este proceso recalcula los pr'#233'stamos considerando las n'#243'minas af' +
        'ectadas de los empleados seleccionados.'
      Header.Title = 'Recalcular Pr'#233'stamos'
      ExplicitWidth = 406
      ExplicitHeight = 272
      object RecalcularPrestamos: TGroupBox
        Left = 6
        Top = 19
        Width = 394
        Height = 101
        TabOrder = 0
        object TodosPrestamos: TRadioButton
          Left = 18
          Top = 23
          Width = 120
          Height = 17
          Caption = 'Todos los pr'#233'stamos'
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = TodosPrestamosClick
        end
        object SoloUnPrestamo: TRadioButton
          Left = 18
          Top = 43
          Width = 140
          Height = 17
          Caption = 'S'#243'lo un tipo de pr'#233'stamo:'
          TabOrder = 1
          OnClick = SoloUnPrestamoClick
        end
        object PR_TIPO: TZetaKeyLookup_DevEx
          Left = 38
          Top = 61
          Width = 340
          Height = 21
          LookupDataset = dmTablas.cdsTPresta
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 2
          TabStop = True
          WidthLlave = 60
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      ExplicitWidth = 406
      ExplicitHeight = 272
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 406
        ExplicitHeight = 177
        Height = 177
        Width = 406
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 406
        Width = 406
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          ExplicitWidth = 338
          Width = 338
          AnchorY = 57
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      ExplicitWidth = 406
      ExplicitHeight = 272
      inherited sCondicionLBl: TLabel
        Left = 6
        Top = 145
        ExplicitLeft = 6
        ExplicitTop = 145
      end
      inherited sFiltroLBL: TLabel
        Left = 31
        Top = 174
        ExplicitLeft = 31
        ExplicitTop = 174
      end
      inherited Seleccionar: TcxButton
        Left = 134
        Top = 262
        ExplicitLeft = 134
        ExplicitTop = 262
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 58
        Top = 142
        ExplicitLeft = 58
        ExplicitTop = 142
      end
      inherited sFiltro: TcxMemo
        Left = 58
        Top = 173
        Style.IsFontAssigned = True
        ExplicitLeft = 58
        ExplicitTop = 173
      end
      inherited GBRango: TGroupBox
        Left = 58
        Top = 12
        ExplicitLeft = 58
        ExplicitTop = 12
      end
      inherited bAjusteISR: TcxButton
        Left = 372
        Top = 174
        ExplicitLeft = 372
        ExplicitTop = 174
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
  end
end
