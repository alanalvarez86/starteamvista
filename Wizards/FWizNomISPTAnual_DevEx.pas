unit FWizNomISPTAnual_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, Mask,
     FWizEmpBaseFiltro,
     ZetaEdit,
     ZetaWizard,
     ZetaNumero,
     ZetaKeyCombo, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxContainer, cxEdit, Menus, ZetaCXWizard, cxRadioGroup, cxTextEdit,
  cxMemo, ZetaKeyLookup_DevEx, cxButtons, dxGDIPlusClasses, cxImage,
  cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl;

type
  TWizNomISPTAnual_DevEX = class(TWizEmpBaseFiltro)
    rgRepetidos: TRadioGroup;
    Rastrear: TCheckBox;
    SubsidioAcreditable: TZetaNumero;
    Year: TZetaNumero;
    lbEmpleadosACalcular: TLabel;
    lbEmpleadosRepetidos: TLabel;
    SubsidioAcreditableLBL: TLabel;
    CalculoImpuestoLBL: TLabel;
    CreditoPagadoLBL: TLabel;
    ImpuestoRetenidoLBL: TLabel;
    IngresoExentoLBL: TLabel;
    IngresoBrutoLBL: TLabel;
    YearLBL: TLabel;
    IngresoBruto: TcxMemo;
    BitBtn1: TcxButton;
    IngresoExento: TcxMemo;
    BitBtn2: TcxButton;
    ImpuestoRetenido: TcxMemo;
    BitBtn3: TcxButton;
    CreditoPagado: TcxMemo;
    BitBtn4: TcxButton;
    CalculoImpuesto: TcxMemo;
    BitBtn5: TcxButton;
    EmpleadosACalcular: TcxMemo;
    BitBtn6: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure YearExit(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
    procedure ValidaFechaReformaFiscal;
  protected
    { Protected declarations }
    function EjecutarWizard : boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizNomISPTAnual_DevEX: TWizNomISPTAnual_DevEX;

implementation

uses ZetaDialogo,
     ZConstruyeFormula,
     ZGlobalTress,
     ZetaTipoEntidad,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists,
     DGlobal,
     DCliente,
     DProcesos,
     DCatalogos;

{$R *.DFM}

procedure TWizNomISPTAnual_DevEX.FormCreate(Sender: TObject);
begin
     inherited;
     Year.Valor := ( dmCliente.YearDefault - 1 );
     IngresoBruto.Tag := K_GLOBAL_DEF_COMP_ANUAL_INGRESO_BRUTO;
     IngresoExento.Tag := K_GLOBAL_DEF_COMP_ANUAL_INGRESO_EXENTO;
     ImpuestoRetenido.Tag := K_GLOBAL_DEF_COMP_ANUAL_IMPUESTO_RETENIDO;
     CreditoPagado.Tag := K_GLOBAL_DEF_COMP_ANUAL_CREDITO_PAGADO;
     CalculoImpuesto.Tag := K_GLOBAL_DEF_COMP_ANUAL_CALCULO_IMPUESTO;
     SubsidioAcreditable.Tag := K_GLOBAL_PORCEN_SUBSIDIO;
     EmpleadosACalcular.Tag := K_GLOBAL_DEF_COMP_ANUAL_EMPLEADOS_CALCULAR;
     ECondicion.Tag := K_GLOBAL_DEF_COMP_ANUAL_CONDICION;
     sFiltro.Tag := K_GLOBAL_DEF_COMP_ANUAL_FILTROS;


     ParametrosControl := Year;
     ValidaFechaReformaFiscal;
     HelpContext := H33513_Comparar_ISTP_anual;
     rgRepetidos.ItemIndex := 0;
     // �no existe en 2.0?    Mensaje := '';
     SubsidioAcreditable.Tag := 0; { Para Que No SobreEscriba El Default }

      //DevEx(by am): Es necesario especificar que Ejecucion sera la ultima pagina del wizard.
     Parametros.PageIndex := WizardControl.PageCount - 3;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 2; //3
     Ejecucion.PageIndex := WizardControl.PageCount - 1;
end;

procedure TWizNomISPTAnual_DevEX.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   rSubsidio: TTasa;
begin
     ParametrosControl := nil;
     if CanMove and Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
     begin
          with Wizard do
          begin
               if StrVacio( IngresoBruto.Text ) then
               begin
                    zError( Caption, '� F�rmula de Ingreso Bruto no fu� especificada !', 0 );
                    CanMove := False;
                    ActiveControl := IngresoBruto;
               end
               else
               if StrVacio( IngresoExento.Text ) then
               begin
                    zError( Caption, '� F�rmula de Ingreso Exento no fu� especificada !', 0 );
                    CanMove := False;
                    ActiveControl := IngresoExento;
               end
               else
               if StrVacio( ImpuestoRetenido.Text ) then
               begin
                    zError( Caption, '� F�rmula de Impuesto Retenidono fu� especificada !', 0 );
                    CanMove := False;
                    ActiveControl := ImpuestoRetenido;
               end
               else
               if StrVacio( CreditoPagado.Text ) then
               begin
                    if ( Year.Valor >= ZetaCommonClasses.K_REFORMA_FISCAL_2008 ) then
                       zError( Caption, '� F�rmula de Subsidio al Empleo Pagado no fu� especificada !', 0 )
                    else
                        zError( Caption, '� F�rmula de Cr�dito Al Salario Pagado no fu� especificada !', 0 );
                    CanMove := False;
                    ActiveControl := CreditoPagado;
               end
               else
               if StrVacio( CalculoImpuesto.Text ) then
               begin
                    zError( Caption, '� F�rmula de C�lculo de Impuesto no fu� especificada !', 0 );
                    CanMove := False;
                    ActiveControl := CalculoImpuesto;
               end
               else
               if ( Year.ValorEntero <= 0 ) then
               begin
                    zError( Caption, '� A�o no fu� especificado !', 0 );
                    CanMove := False;
                    ActiveControl := Year;
               end
               else
               begin
                    rSubsidio := SubsidioAcreditable.Valor;
                    if ( rSubsidio < 0 ) or ( rSubsidio > 1 ) then
                    begin
                         CanMove := Error( '� La Proporci�n del Subsidio ' +
                                           'Acreditable debe de ser un valor Entre 0 - 1',
                                           SubsidioAcreditable );
                    end;
               end;
          end;
     end;
     inherited;
end;

procedure TWizNomISPTAnual_DevEX.BitBtn1Click(Sender: TObject);
begin
     IngresoBruto.Text:= GetFormulaConst( enEmpleado , IngresoBruto.Text, IngresoBruto.SelStart, evISPTAnual );
end;

procedure TWizNomISPTAnual_DevEX.BitBtn2Click(Sender: TObject);
begin
     IngresoExento.Text:= GetFormulaConst( enEmpleado , IngresoExento.Text, IngresoExento.SelStart, evISPTAnual );
end;

procedure TWizNomISPTAnual_DevEX.BitBtn3Click(Sender: TObject);
begin
     ImpuestoRetenido.Text:= GetFormulaConst( enEmpleado , ImpuestoRetenido.Text, ImpuestoRetenido.SelStart, evISPTAnual );
end;

procedure TWizNomISPTAnual_DevEX.BitBtn4Click(Sender: TObject);
begin
     CreditoPagado.Text:= GetFormulaConst( enEmpleado , CreditoPagado.Text, CreditoPagado.SelStart, evISPTAnual );
end;

procedure TWizNomISPTAnual_DevEX.BitBtn5Click(Sender: TObject);
begin
     CalculoImpuesto.Text:= GetFormulaConst( enEmpleado , CalculoImpuesto.Text, CalculoImpuesto.SelStart, evISPTAnual );
end;

procedure TWizNomISPTAnual_DevEX.CargaParametros;
 function GetRepetidoEnum( const iRepetido: integer ): integer;
 begin
      case rgRepetidos.ItemIndex of
           1: Result := ord( erIncluirTodos );
           2: Result := ord( erNoIncluir )
           else Result := ord( erSumarUltimo )
      end;
 end;
begin
     inherited CargaParametros;
     //DevEx(by am): Se agregaron las descripciones para mostrarlas antes de ejecutar el wizard.
     with Descripciones do
     begin
          AddInteger( 'A�o', Year.ValorEntero );
          AddString( 'Rastrear C�lculos', zBoolToStr(Rastrear.Checked) );
     end;
     with ParameterList do
     begin
          AddInteger( 'Year', Year.ValorEntero );
          AddString( 'IngresoBruto', IngresoBruto.Text );
          AddString( 'IngresoExento', IngresoExento.Text );
          AddString( 'CreditoPagado', CreditoPagado.Text );
          AddString( 'ImpuestoRetenido', ImpuestoRetenido.Text );
          AddSTring( 'CalculoImpuesto', CalculoImpuesto.Text );
          AddSTring( 'EmpleadosACalcular', EmpleadosACalcular.Text );
          AddFloat( 'Subsidio', SubsidioAcreditable.Valor );
          AddBoolean( 'Rastrear', Rastrear.Checked );
          AddInteger( 'Repetidos', GetRepetidoEnum( rgRepetidos.ItemIndex ) );
     end;
end;

function TWizNomISPTAnual_DevEX.EjecutarWizard: boolean;
begin
     Result := dmProcesos.ISPTAnual( ParameterList );
end;

{* Cambio por reforma fiscal del 2008. Si es igual o mayor a la constante
 K_REFORMA_FISCAL_2008 se hacen cambio en cierto textos *}
procedure TWizNomISPTAnual_DevEX.ValidaFechaReformaFiscal;
begin
     if ( Year.Valor >= ZetaCommonClasses.K_REFORMA_FISCAL_2008 ) then
        CreditoPagadoLBL.Caption := 'Su&bsidio al Empleo Aplicado:'
     else
         CreditoPagadoLBL.Caption := 'C&r�dito al Salario Aplicado:';
end;

{* Cambio por reforma fiscal del 2008. Si es igual o mayor a la constante
 K_REFORMA_FISCAL_2008 se hacen cambio en cierto textos *}
procedure TWizNomISPTAnual_DevEX.YearExit(Sender: TObject);
begin
     inherited;
     ValidaFechaReformaFiscal; // Validacion Reforma Fiscal 2008
end;

procedure TWizNomISPTAnual_DevEX.BitBtn6Click(Sender: TObject);
begin
     inherited;
     EmpleadosACalcular.Text:= GetFormulaConst( enEmpleado , EmpleadosACalcular.Text, EmpleadosACalcular.SelStart, evISPTAnual );

end;

procedure TWizNomISPTAnual_DevEX.FormShow(Sender: TObject);
begin
     inherited;
     eCondicion.Llave := Global.GetGlobalString( K_GLOBAL_DEF_COMP_ANUAL_CONDICION );
     sFiltro.Text := Global.GetGlobalString( K_GLOBAL_DEF_COMP_ANUAL_FILTROS );
     Advertencia.Caption := ' Al aplicar el proceso se mostrar�n los montos calculados de la comparaci�n a favor o en contra.';
end;

procedure TWizNomISPTAnual_DevEX.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual( Parametros )then
         ParametrosControl := Year;
  inherited;
end;

end.
