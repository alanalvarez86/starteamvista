inherited WizNomCancelar_DevEx: TWizNomCancelar_DevEx
  Left = 447
  Top = 175
  Caption = 'Cancelar N'#243'minas Pasadas'
  ClientHeight = 420
  ClientWidth = 423
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 423
    Height = 420
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000017352474200AECE1CE90000000467414D410000B18F0BFC61050000
      00097048597300000EBD00000EBD0147FB90AD0000032A494441545847ED562D
      6C1441143E8140542010880A04028140542048402010E56696D0044102028140
      20100812482A1025B9DEEE2508C40A64050281A8A8A8A828C9EDEE3541549C40
      549CA840204EC0F7BD793B377BB7D78683ABBA97BCECBD37EF6FDEDF5C63010B
      9807243D735B7F9E1DA4FDC7E7DBB9799AE4F67B52D8DF9B99BDA947F385F7DF
      562F26B9791BE7F6888E3DE6F6B38A343A076B97E2C26EE0BBA4AC7F87CEC1BD
      2B709CC0F1AF8AE30029F366E7D639FCEE9346865EA8FAECC0D4F276A1A3A988
      00A91367F695F2FA0C480CFD0D50A99D371FA0B93EC1482A06613C294CCF3BAB
      416687256A75CD8538373F859F450FD5ECE9C09A316DA85F17069E244573456F
      93B2A6A4D9F113F50F11FD21B60ADB220DD97D317E12B069925EF40E0AC7BC45
      A76BAE938E7BD10722F87BA3DA9BF5B8B87F95B28EAE2283E384200B97410F85
      3F6D4CC5116F57692C3AB0CF954E698C72345CCA70FC6404BD4E157946FBF8BD
      25BC604204DA597417075F4B8510DB79B48CAFDC8EB5C4B74E6EA872E37C87D8
      0DF42325531E27449C837051D5A0BB65A8B4B61417CD3B92119624B7875E3EB3
      ABEC154F8F23CEE90F323BA4594A09804D3621AC0807FBD2FD9E6776A519618C
      379005137437BE7B23D92AD2B10BC058A1D95BD0974E07635A0341080BA7E66C
      9456B7644E6A44C86E97192050D7F1DD84E0F1908E9F542472CC346D825006BD
      819ADF70A329BC6DD25EC6E1907B830D2B4E000C12E3F89117533DB984DBD595
      CE0FD1F4340B03A5775936695C17F860338BAEE15BA6FF58035C16E3005E02CE
      82ED697E249979C9A6561149653A1298C05483A08C4B33D3880038DFBC158D32
      B0D2A8BC01E80B04E31B93BFE3AE7D54BB8E75078C3BF6880C1DC100F6417345
      9C523EB3CF70B64547A55106E01A1BB72C750BF365EAF20901C2B5BBA01699D2
      C02853CED4E34C32240D8CCCB03C2A723AE842AA77082C8DB29954C565CE3D52
      6ECD4A00665D466C16086B16E08023131AD5C5E3A703C11DB2445CD52A321BF8
      D152A3AC7369D4FFEDAA3CC3584E582E33BDF37520DD8BFAD2A8B2E46F178279
      0D2C1FA0216538FB2A325F2827447BA0E51F92B3048E5A65712C6001FF051A8D
      3FA6D93DEE37EA8F180000000049454E44AE426082}
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Este proceso cancela los montos de una n'#243'mina afectada para un g' +
        'rupo de empleados.'
      Header.Title = 'Cancelar n'#243'minas pasadas'
      object TipoLBL: TLabel
        Left = 67
        Top = 52
        Width = 78
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de N'#243'mina:'
        Transparent = True
      end
      object NominaNueva: TGroupBox
        Left = 68
        Top = 152
        Width = 268
        Height = 73
        Caption = ' N'#243'mina Nueva '
        TabOrder = 2
        object YearNuevoLBL: TLabel
          Left = 56
          Top = 20
          Width = 22
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'o:'
        end
        object NumeroNuevoLBL: TLabel
          Left = 38
          Top = 44
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object YearNuevo: TZetaNumero
          Left = 81
          Top = 16
          Width = 57
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
        end
        object NumeroNuevo: TZetaNumero
          Left = 81
          Top = 40
          Width = 57
          Height = 21
          Mascara = mnDias
          TabOrder = 1
          Text = '0'
        end
      end
      object NominaOriginal: TGroupBox
        Left = 68
        Top = 75
        Width = 268
        Height = 73
        Caption = ' N'#243'mina Original '
        TabOrder = 1
        object YearOriginalLBL: TLabel
          Left = 54
          Top = 20
          Width = 22
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'o:'
        end
        object NumeroOriginalLBL: TLabel
          Left = 36
          Top = 44
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object YearOriginal: TZetaNumero
          Left = 80
          Top = 16
          Width = 57
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
        end
        object NumeroOriginal: TZetaNumero
          Left = 80
          Top = 40
          Width = 57
          Height = 21
          Mascara = mnDias
          TabOrder = 1
          Text = '0'
        end
      end
      object TipoNomina: TZetaKeyCombo
        Left = 147
        Top = 48
        Width = 190
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 0
        ListaFija = lfTipoPeriodo
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = True
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 185
        Width = 401
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 401
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 333
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 6
      end
      inherited sFiltroLBL: TLabel
        Left = 31
      end
      inherited Seleccionar: TcxButton
        Left = 134
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 58
      end
      inherited sFiltro: TcxMemo
        Left = 58
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 58
      end
      inherited bAjusteISR: TcxButton
        Left = 372
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Top = 297
  end
end
