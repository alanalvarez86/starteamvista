inherited EmpCancelarKardexGridSelect_DevEx: TEmpCancelarKardexGridSelect_DevEx
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Seleccione Los Movimientos de Kardex a Cancelar'
  ClientHeight = 268
  ClientWidth = 579
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 232
    Width = 579
    inherited OK_DevEx: TcxButton
      Left = 415
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 494
    end
  end
  inherited PanelSuperior: TPanel
    Width = 579
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 579
    Height = 197
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.Indicator = True
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 65
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 227
      end
      object CB_FECHA: TcxGridDBColumn
        Caption = 'Fecha Registro'
        DataBinding.FieldName = 'CB_FECHA'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
        Width = 88
      end
      object CB_FEC_CAP: TcxGridDBColumn
        Caption = 'Fecha Captura'
        DataBinding.FieldName = 'CB_FEC_CAP'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
        Width = 80
      end
      object CB_TIPO: TcxGridDBColumn
        Caption = 'Cambio'
        DataBinding.FieldName = 'CB_TIPO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 55
      end
      object US_CODIGO: TcxGridDBColumn
        Caption = 'Usuario'
        DataBinding.FieldName = 'US_CODIGO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 43
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
