unit FEmpImportarOrganigramaGridSelect_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, Db,
     ZBaseSelectGrid_DevEx, cxGraphics, cxLookAndFeels,
     cxLookAndFeelPainters, Menus, dxSkinsCore, 
     TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
     dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
     cxEdit, cxNavigator, cxDBData, cxTextEdit, cxGridCustomTableView,
     cxGridTableView, cxGridDBTableView, ImgList, cxGridLevel, cxClasses,
     cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons, cxCalendar;

type
  TEmpImportarOrganigramaGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CB_JEFE: TcxGridDBColumn;
    NOMBRE_JEFE: TcxGridDBColumn;
    PLAZA_JEF: TcxGridDBColumn;
    NOMBRE_PLJEF: TcxGridDBColumn;
    PL_NOMBRE: TcxGridDBColumn;
    PL_CODIGO: TcxGridDBColumn;
    PL_TIPO_DESC: TcxGridDBColumn;
    PL_TIREP_DESC: TcxGridDBColumn;
    PL_FEC_INI: TcxGridDBColumn;
    PL_FEC_FIN: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpImportarOrganigramaGridSelect_DevEx: TEmpImportarOrganigramaGridSelect_DevEx;

implementation

{$R *.DFM}

procedure TEmpImportarOrganigramaGridSelect_DevEx.FormShow(Sender: TObject);
begin

     with DataSet do
     begin
          MaskFecha('PL_FEC_INI');
          MaskFecha('PL_FEC_FIN');
     end;
     inherited;
end;

end.
