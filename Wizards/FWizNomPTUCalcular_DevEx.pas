unit FWizNomPTUCalcular_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, Mask,
     FWizEmpBaseFiltro,
     ZetaEdit,
     ZetaNumero, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl;

type
  TWizNomPTUCalcular_DevEx = class(TWizEmpBaseFiltro)
    Utilidad: TZetaNumero;
    SalarioTope: TZetaNumero;
    Year: TZetaNumero;
    SalarioTopeLBL: TLabel;
    FiltroPlantaLBL: TLabel;
    FiltroEventualLBL: TLabel;
    FiltroDirectorLBL: TLabel;
    PercepcionesLBL: TLabel;
    SumaDiasLBL: TLabel;
    UtilidadLBL: TLabel;
    YearLBL: TLabel;
    SumaDias: TcxMemo;
    Percepciones: TcxMemo;
    FiltroDirector: TcxMemo;
    FiltroPlanta: TcxMemo;
    FiltroEventual: TcxMemo;
    FiltroEventualBuild: TcxButton;
    FiltroPlantaBuild: TcxButton;
    FiltroDirectorBuild: TcxButton;
    PercepcionesBuild: TcxButton;
    SumaBuild: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure SumaBuildClick(Sender: TObject);
    procedure PercepcionesBuildClick(Sender: TObject);
    procedure FiltroDirectorBuildClick(Sender: TObject);
    procedure FiltroPlantaBuildClick(Sender: TObject);
    procedure FiltroEventualBuildClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject); //DevEx(by am): Agregado para enfocar correctamente
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros;override;
  public
    { Public declarations }
  end;

var
  WizNomPTUCalcular_DevEx: TWizNomPTUCalcular_DevEx;

implementation

uses DCliente,
     DProcesos,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaDialogo,
     ZConstruyeFormula,
     ZGlobalTress;

{$R *.DFM}

procedure TWizNomPTUCalcular_DevEx.FormCreate(Sender: TObject);
begin
     Year.Valor := ( dmCliente.YearDefault - 1 );
     Utilidad.Tag := K_GLOBAL_DEF_PTU_UTILIDAD_REPARTIR;
     SumaDias.Tag := K_GLOBAL_DEF_PTU_SUMA_DIAS;
     Percepciones.Tag := K_GLOBAL_DEF_PTU_PERCEPCIONES;
     FiltroDirector.Tag := K_GLOBAL_DEF_PTU_FILTRO_DIRECTOR;
     FiltroPlanta.Tag := K_GLOBAL_DEF_PTU_FILTRO_PLANTA;
     FiltroEventual.Tag := K_GLOBAL_DEF_PTU_FILTRO_EVENTUAL;
     SalarioTope.Tag := K_GLOBAL_DEF_PTU_SALARIO_TOPE;
     inherited;
     ParametrosControl := Year;
     HelpContext := H30358_Calcular_PTU;

     //DevEx(by am): Es necesario especificar las paginas del wizard.
     Parametros.PageIndex := WizardControl.PageCount - 3;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 2; //3
     Ejecucion.PageIndex := WizardControl.PageCount - 1;
end;

procedure TWizNomPTUCalcular_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     ParametrosControl := nil;
     if CanMove then
     begin
          with Wizard do
          begin
               if EsPaginaActual( Parametros ) and Adelante then
               begin
                    if ( Utilidad.Valor <= 0 ) then
                    begin
                         ZetaDialogo.zError( Caption, '� Utilidad a repartir debe ser mayor que cero !', 0 );
                         CanMove := False;
                         ActiveControl := Utilidad;
                    end
                    else
                    if ZetaCommonTools.StrVacio( SumaDias.Text ) then
                    begin
                         ZetaDialogo.zError( Caption, '� F�rmula de Suma de D�as no fu� especificada !', 0 );
                         CanMove := False;
                         ActiveControl := SumaDias;
                    end
                    else
                    if ZetaCommonTools.StrVacio( Percepciones.Text ) then
                    begin
                         ZetaDialogo.zError( Caption, '� F�rmula de Percepciones no fu� especificada !', 0 );
                         CanMove := False;
                         ActiveControl := Percepciones;
                    end
                    else
                    if ( Year.ValorEntero <= 0 ) then
                    begin
                         ZetaDialogo.zError( Caption, '� A�o no fu� especificado !', 0 );
                         CanMove := False;
                         ActiveControl := Year;
                    end;
               end;
          end;
     end;
     inherited;
end;

procedure TWizNomPTUCalcular_DevEx.WizardAfterMove(Sender: TObject);
begin
     // Para Enfocar el primer control dependiendo del tipo de proceso
     with Wizard do
     begin
     if Adelante then
        if EsPaginaActual( Parametros ) then
           ParametrosControl := Year;
     end;
     inherited;
end;

procedure TWizNomPTUCalcular_DevEx.SumaBuildClick(Sender: TObject);
begin
     SumaDias.Text := ZConstruyeFormula.GetFormulaConst( enEmpleado , SumaDias.Text, SumaDias.SelStart, evBase );
end;

procedure TWizNomPTUCalcular_DevEx.PercepcionesBuildClick(Sender: TObject);
begin
     Percepciones.Text := ZConstruyeFormula.GetFormulaConst( enEmpleado , Percepciones.Text, Percepciones.SelStart, evBase );
end;

procedure TWizNomPTUCalcular_DevEx.FiltroDirectorBuildClick(Sender: TObject);
begin
     FiltroDirector.Text := ZConstruyeFormula.GetFormulaConst( enEmpleado , FiltroDirector.Text, FiltroDirector.SelStart, evBase );
end;

procedure TWizNomPTUCalcular_DevEx.FiltroPlantaBuildClick(Sender: TObject);
begin
     FiltroPlanta.Text := ZConstruyeFormula.GetFormulaConst( enEmpleado , FiltroPlanta.Text, FiltroPlanta.SelStart, evBase );
end;

procedure TWizNomPTUCalcular_DevEx.FiltroEventualBuildClick(Sender: TObject);
begin
     FiltroEventual.Text := ZConstruyeFormula.GetFormulaConst( enEmpleado , FiltroEventual.Text, FiltroEventual.SelStart, evBase );
end;

procedure TWizNomPTUCalcular_DevEx.CargaParametros;
begin
     inherited;
     with descripciones do
     begin
          AddInteger( 'A�o', Year.ValorEntero );
     end;
     with ParameterList do
     begin
          AddInteger( 'Year', Year.ValorEntero );
          AddFloat( 'Utilidad', Utilidad.Valor );
          AddFloat( 'SalarioTope', SalarioTope.Valor );
          AddString( 'Percepciones', Percepciones.Text );
          AddString( 'SumaDias', SumaDias.Text );
          AddString( 'FiltroDirector', FiltroDirector.Text );
          AddString( 'FiltroPlanta', FiltroPlanta.Text );
          AddString( 'FiltroEventual', FiltroEventual.Text );
     end;
end;

function TWizNomPTUCalcular_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.PTUCalcular(ParameterList, Verificacion);
end;

procedure TWizNomPTUCalcular_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'Al aplicar el proceso se guardar� el detalle del reparto en una tabla temporal, a la que podr� acceder para revisar los montos.'; 
end;

end.
