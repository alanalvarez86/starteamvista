inherited EmpImportarAsistenciaSesionesGridShow_DevEx: TEmpImportarAsistenciaSesionesGridShow_DevEx
  Left = 204
  Top = 186
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Lista de Errores Detectados'
  ClientWidth = 569
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 569
    inherited OK_DevEx: TcxButton
      Left = 403
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 482
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 569
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object RENGLON: TcxGridDBColumn
        Caption = 'Rengl'#243'n'
        DataBinding.FieldName = 'RENGLON'
      end
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
      end
      object SE_SESION: TcxGridDBColumn
        Caption = 'Grupo'
        DataBinding.FieldName = 'SE_SESION'
      end
      object NUM_ERROR: TcxGridDBColumn
        Caption = 'Errores'
        DataBinding.FieldName = 'NUM_ERROR'
      end
      object DESC_ERROR: TcxGridDBColumn
        Caption = 'Descripci'#243'n de Errores'
        DataBinding.FieldName = 'DESC_ERROR'
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
