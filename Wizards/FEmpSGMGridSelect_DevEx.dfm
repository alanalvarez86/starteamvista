inherited EmpSGMGridSelect_DevEx: TEmpSGMGridSelect_DevEx
  Left = 92
  Top = 269
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Aplicar Eventos'
  ClientHeight = 255
  ClientWidth = 1296
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 219
    Width = 1296
    inherited OK_DevEx: TcxButton
      Left = 1130
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 1208
    end
  end
  inherited PanelSuperior: TPanel
    Width = 1296
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 1296
    Height = 184
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
        Width = 65
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        Width = 213
      end
      object EP_TIPO_DESC: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'EP_TIPO_DESC'
        Width = 65
      end
      object DEPENDIENT: TcxGridDBColumn
        Caption = 'Pariente'
        DataBinding.FieldName = 'DEPENDIENT'
        Width = 207
      end
      object PA_DESC_REL: TcxGridDBColumn
        Caption = 'Parentesco'
        DataBinding.FieldName = 'PA_DESC_REL'
        Width = 62
      end
      object EP_CERTIFI: TcxGridDBColumn
        Caption = 'Certificado'
        DataBinding.FieldName = 'EP_CERTIFI'
        Width = 73
      end
      object RN_FEC_INI: TcxGridDBColumn
        Caption = 'Inicio'
        DataBinding.FieldName = 'RN_FEC_INI'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.AssignedValues.EditFormat = True
        Properties.DisplayFormat = 'dd/mmm/yyyy'
        Width = 72
      end
      object RN_FEC_FIN: TcxGridDBColumn
        Caption = 'Final'
        DataBinding.FieldName = 'RN_FEC_FIN'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
        Width = 72
      end
      object EP_CFIJO: TcxGridDBColumn
        Caption = 'Costo Fijo'
        DataBinding.FieldName = 'EP_CFIJO'
        Width = 80
      end
      object EP_CPOLIZA: TcxGridDBColumn
        Caption = 'Costo P'#243'liza'
        DataBinding.FieldName = 'EP_CPOLIZA'
        Width = 80
      end
      object EP_CTOTAL: TcxGridDBColumn
        Caption = 'Total'
        DataBinding.FieldName = 'EP_CTOTAL'
      end
      object EP_CTITULA: TcxGridDBColumn
        Caption = 'Costo Empleado'
        DataBinding.FieldName = 'EP_CTITULA'
        Width = 83
      end
      object EP_CEMPRES: TcxGridDBColumn
        Caption = 'Costo Empresa'
        DataBinding.FieldName = 'EP_CEMPRES'
        Width = 82
      end
      object EP_OBSERVA: TcxGridDBColumn
        Caption = 'Observaciones'
        DataBinding.FieldName = 'EP_OBSERVA'
        Width = 123
      end
      object PA_APE_MAT: TcxGridDBColumn
        Caption = 'Apellido Materno'
        DataBinding.FieldName = 'PA_APE_MAT'
        Visible = False
        Width = 20
      end
      object PA_NOMBRES: TcxGridDBColumn
        Caption = 'Nombres'
        DataBinding.FieldName = 'PA_NOMBRES'
        Visible = False
        Width = 20
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
