inherited WizIMSSCalcular_DevEx: TWizIMSSCalcular_DevEx
  Left = 196
  Top = 111
  Caption = 'Calcular Pago IMSS'
  ClientHeight = 417
  ClientWidth = 528
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 528
    Height = 417
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Este proceso calcula las cuotas de IMSS, Retiro e INFONAVIT de c' +
        'ada empleado para el mes correspondiente. Esta informaci'#243'n se ut' +
        'iliza para conciliar contra los montos calculados en SUA.'
      Header.Title = 'Calcular pago'
      inherited PeriodoGB: TcxGroupBox
        Height = 153
        Width = 506
        inherited Label1: TLabel
          Left = 161
        end
        inherited Label2: TLabel
          Left = 160
        end
        inherited PatronLbl: TLabel
          Left = 99
        end
        inherited Label3: TLabel
          Left = 87
        end
        inherited Mes: TZetaKeyCombo
          Left = 192
        end
        inherited Patron: TZetaKeyLookup_DevEx
          Left = 192
        end
        inherited Tipo: TZetaKeyCombo
          Left = 192
        end
        inherited Anio: TZetaNumero
          Left = 192
        end
      end
      object GroupBox1: TcxGroupBox
        Left = 0
        Top = 164
        Align = alBottom
        TabOrder = 1
        Height = 113
        Width = 506
        object LblSeguroVivienda: TLabel
          Left = 50
          Top = 40
          Width = 136
          Height = 13
          Alignment = taRightJustify
          Caption = 'Seguro de &da'#241'os a vivienda:'
          Transparent = True
        end
        object lCuotaFija: TcxCheckBox
          Left = 72
          Top = 64
          Caption = 'Enviar Como Cuota &Fija:'
          Properties.Alignment = taRightJustify
          TabOrder = 0
          Transparent = True
          Width = 136
        end
        object SeguroVivienda: TZetaNumero
          Left = 192
          Top = 36
          Width = 60
          Height = 21
          Mascara = mnPesos
          TabOrder = 1
          Text = '0.00'
        end
        object lAusentismos: TcxCheckBox
          Left = 30
          Top = 8
          Caption = 'Ausentismos con Fecha de &Baja:   '
          Properties.Alignment = taRightJustify
          TabOrder = 2
          Transparent = True
          Width = 178
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 182
        Width = 506
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 506
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 438
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      PageVisible = False
      inherited sCondicionLBl: TLabel
        Left = 6
        Top = 137
      end
      inherited sFiltroLBL: TLabel
        Left = 31
        Top = 166
      end
      inherited Seleccionar: TcxButton
        Left = 134
        Top = 254
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 58
        Top = 134
      end
      inherited sFiltro: TcxMemo
        Left = 58
        Top = 165
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 58
        Top = 4
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    Left = 336
    Top = 2
  end
end
