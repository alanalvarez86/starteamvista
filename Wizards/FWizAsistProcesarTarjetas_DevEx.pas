unit FWizAsistProcesarTarjetas_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, ExtCtrls, Buttons,
     ZetaWizard, ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, ZetaCXWizard, cxGroupBox,
  cxLabel, dxGDIPlusClasses, cxImage, dxCustomWizardControl,
  dxWizardControl, dxSkinsCore, TressMorado2013, dxSkinsDefaultPainters;
                                                    
type
  TWizAsistProcesarTarjetas_DevEx = class(TcxBaseWizard)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure CargaParametros;override;
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizAsistProcesarTarjetas_DevEx: TWizAsistProcesarTarjetas_DevEx;

implementation

{$R *.DFM}

uses DProcesos,
     DCliente,
     ZetaCommonClasses;

procedure TWizAsistProcesarTarjetas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     Wizard.Saltar( Ejecucion.PageIndex );
     HelpContext := H20232_Procesar_tarjetas;
end;

procedure TWizAsistProcesarTarjetas_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddBoolean( 'PuedeCambiarTarjeta', dmCliente.PuedeModificarTarjetasAnteriores );
     end;
end;

function TWizAsistProcesarTarjetas_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.ProcesarTarjetasPendientes( ParameterList );
end;

procedure TWizAsistProcesarTarjetas_DevEx.FormShow(Sender: TObject);
begin
  inherited;
parametros.PageVisible := false;
end;

end.
