unit FWizNomFoliarRecibos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, Buttons, StdCtrls, ExtCtrls, Mask,
     FWizNomBase_DevEx,
     ZetaDBTextBox,
     ZetaWizard,
     ZetaNumero,
     ZetaEdit,  
     cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl;

type
  TWizNomFoliarRecibos_DevEx = class(TWizNomBase_DevEx)
    Panel2: TPanel;
    Label5: TLabel;
    nFolio: TZetaKeyLookup_DevEx;
    Parametros2: TdxWizardControlPage;
    Panel1: TPanel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    sFolio: TZetaTextBox;
    sCorrida: TZetaTextBox;
    rMoneda: TZetaTextBox;
    sFormula: TZetaTextBox;
    Label10: TLabel;
    sOrden: TZetaTextBox;
    Label11: TLabel;
    lCeros: TCheckBox;
    lRepite: TCheckBox;
    nFolioInicial: TZetaNumero;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
    FOrdenCampos: String;
    function GetOrdenDescripcion: String;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizNomFoliarRecibos_DevEx: TWizNomFoliarRecibos_DevEx;

implementation

uses ZetaDialogo,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     DCatalogos,
     DProcesos;

{$R *.DFM}

procedure TWizNomFoliarRecibos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H30343_Foliar_recibos;
     ParametrosControl := nFolio;
     nFolio.LookupDataset := dmCatalogos.cdsFolios;
     //DevEx(by am): Es necesario especificar las paginas del wizard.
     Parametros.PageIndex := WizardControl.PageCount - 4;
     Parametros2.PageIndex := WizardControl.PageCount - 3;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 2; //3
     Ejecucion.PageIndex := WizardControl.PageCount - 1;
end;

procedure TWizNomFoliarRecibos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmCatalogos.cdsFolios do
     begin
          Refrescar;
          First;
          with nFolio do
          begin
               if Eof then
                  Llave := '1'
               else
                   Llave := FieldByName( 'FL_CODIGO' ).AsString;
          end;
     end;
     Advertencia.Caption := 'Al aplicar el proceso se enumerar�n los recibos o cheques de un grupo de empleados.'; 
end;

function TWizNomFoliarRecibos_DevEx.GetOrdenDescripcion: String;
begin
     Result := '';
     FordenCampos := '';
     with dmCatalogos.cdsOrdFolios do
     begin
          First;
          while not Eof do
          begin
               if ( Result > '' ) then
               begin
                    Result := Result + ', ';
                    FOrdenCampos := FOrdenCampos + ',';
               end;
               FOrdenCampos := FOrdenCampos + FieldByName( 'OF_CAMPO' ).AsString;
               Result := Result + FieldByName( 'OF_TITULO' ).AsString;
               if ( FieldByName( 'OF_DESCEND' ).AsString = K_GLOBAL_SI ) then
               begin
                    Result := Result + ' DESC ';
                    FOrdenCampos := FOrdenCampos + ' DESC ';
               end;
               Next;
          end;
     end;
end;

procedure TWizNomFoliarRecibos_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     ParametrosControl := nil;
     if Wizard.Adelante then
     begin
          if Wizard.EsPaginaActual( Parametros ) then
          begin
               CanMove := nFolio.Valor in [ 1..5 ];
               if not CanMove then
               begin
                    ZetaDialogo.zError( Caption, 'El N�mero De Folio Debe Estar Entre Uno Y Cinco', 0 );
                    nFolio.SetFocus;
                    Exit;
               end;

               with dmCatalogos.cdsFolios do
               begin
                    sFolio.Caption := FieldByName( 'FL_CODIGO' ).AsString + ' = ' +
                                      FieldByName( 'FL_DESCRIP' ).AsString;
                    sFormula.Caption := FieldByName( 'FL_MONTO' ).AsString;
                    lCeros.Checked := ( FieldByName( 'FL_CEROS' ).AsString = K_GLOBAL_SI );
                    lRepite.Checked := ( FieldByName( 'FL_REPITE' ).AsString = K_GLOBAL_SI );
                    rMoneda.Caption := FieldByName( 'FL_MONEDA' ).AsString;
                    sCorrida.Caption := 'Del ' + FieldByName( 'FL_INICIAL' ).AsString + ' al '+
                                        FieldByName( 'FL_FINAL' ).AsString;

                    sFiltro.Text := FieldByName( 'FL_FILTRO' ).AsString;
                    eCondicion.Llave := FieldByName( 'QU_CODIGO' ).AsString;
                    EnabledBotones( raRango );
                    nFolioInicial.Valor := FieldByName( 'FL_FINAL' ).AsInteger + 1;
                    sOrden.Caption := GetOrdenDescripcion;
               end;
          end //Pagina = Parametros
          else
              if Wizard.EsPaginaActual( Parametros2 ) then
              begin
                   CanMove := ( nFolioInicial.Valor >= 0 );
                   if not CanMove then
                   begin
                        ZetaDialogo.zError( Caption, 'El N�mero Inicial De Folio Debe Ser Mayor O Igual A Cero', 0 );
                        nFolioInicial.SetFocus;
                        Exit;
                   end;
              end
     end;//Adelante
     inherited;
end;

procedure TWizNomFoliarRecibos_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual( FiltrosCondiciones ) then
     begin
          with nFolio do
          begin
               if ZetaCommonTools.StrLleno( Descripcion ) then
               begin
                    with LookupDataset do
                    begin
                         ECondicion.Llave := FieldByName( 'QU_CODIGO' ).AsString;
                         sFiltro.Lines.Text := FieldByName( 'FL_FILTRO' ).AsString;
                    end;
               end;
          end
     end;
     if Wizard.Adelante then
          if Wizard.EsPaginaActual( Parametros ) then
             ParametrosControl := nFolio;
     inherited;
end;

procedure TWizNomFoliarRecibos_DevEx.CargaParametros;
begin
     inherited;
     with Descripciones do
     begin
          AddInteger('Folio', nFolio.Valor );
          AddInteger('Folio Inicial', nFolioInicial.ValorEntero );
     end;
     with ParameterList do
     begin
          AddInteger('Folio', nFolio.Valor );
          AddInteger('FolioInicial', nFolioInicial.ValorEntero );
          AddString('Formula', sFormula.Caption );
          AddString('Orden', FOrdenCampos );
     end;
end;

function TWizNomFoliarRecibos_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.FoliarRecibos(ParameterList);
end;

end.

