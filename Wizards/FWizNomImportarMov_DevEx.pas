unit FWizNomImportarMov_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ComCtrls, Buttons, ExtCtrls,
     ZetaDBTextBox,
     ZetaCommonLists,
     ZetaKeyCombo, Mask, ZetaNumero, FWizNomBase_DevEx, cxGraphics,
     cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
     TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
     ZetaCXWizard, ZetaEdit, cxRadioGroup, cxTextEdit, cxMemo,
     ZetaKeyLookup_DevEx, cxButtons, dxGDIPlusClasses, cxImage, cxLabel,
     cxGroupBox, dxCustomWizardControl, dxWizardControl;

type
  TWizNomImportarMov_DevEx = class(TWizNomBase_DevEx)
    Archivo: TdxWizardControlPage;
    Panel1: TPanel;
    Label5: TLabel;
    Label7: TLabel;
    BGuardaArchivo: TcxButton;
    Label8: TLabel;
    sArchivo: TEdit;
    iOperacion: TZetaKeyCombo;
    iFormato: TZetaKeyCombo;
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    PanelAnimation: TPanel;
    Animate: TAnimate;
    PanelMensaje: TPanel;
    CBNoIncapacitados: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure BGuardaArchivoClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure RBImportarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    ArchivoVerif : String;
    function GetArchivo: String;
    function LeerArchivo: Boolean;
    function GetTipoImportacion: eImportacion;
    procedure ActualizaNoIncapacitados;{OP: 12/06/08}
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizNomImportarMov_DevEx: TWizNomImportarMov_DevEx;

implementation

uses DCliente,
     DProcesos,
     ZGlobalTress,
     ZBaseGridShow_DevEx,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     ZAsciiTools,
     ZetaDialogo,
     FNomImportarMovGridShow_DevEx,
     FTressShell,
     ZAccesosMgr,
     ZAccesosTress;

{$R *.DFM}

procedure TWizNomImportarMov_DevEx.FormCreate(Sender: TObject);
begin
     PanelAnimation.Visible := FALSE;
     with sArchivo do
     begin
          Tag := K_GLOBAL_DEF_IMP_MOVS;
     end;
     inherited;
     with dmCliente do
     begin
          sDescripcion.Caption := GetPeriodoCampoDescripcion;
          with GetDatosPeriodoActivo do
          begin
               iNumeroNomina.Caption := IntToStr( Numero );
               iTipoNomina.Caption := ZetaCommonLists.ObtieneElemento( lfTipoPeriodo, Ord( Tipo ) );
               iMesNomina.Caption := ZetaCommonLists.ObtieneElemento( lfMeses, ( Mes - 1  ) );
               sStatusNomina.Caption := ZetaCommonTools.GetDescripcionStatusPeriodo( Status, StatusTimbrado );
               //sStatusNomina.Caption := ZetaCommonLists.ObtieneElemento( lfStatusPeriodo, Ord( Status ) );
               FechaInicial.Caption := FormatDateTime( {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}LongDateFormat, Inicio );
               FechaFinal.Caption := FormatDateTime( {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}LongDateFormat, Fin );
               // Parametros
          end;
     end;
     iOperacion.ItemIndex := Ord( omSustituir );
     iFormato.ItemIndex := Ord( faASCIIFijo );
     HelpContext := H33411_Importar_movimientos;

     //DevEx
     Parametros.PageIndex := 0;
     Archivo.PageIndex := 1;
     FiltrosCondiciones.PageIndex := 2;
     Ejecucion.PageIndex := 3;
end;

procedure TWizNomImportarMov_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     ActualizaNoIncapacitados;{OP: 12/06/08}

     //DevEx
     Advertencia.Caption :=
        'Al aplicar el proceso se realizar� el registro de los movimientos que contiene el archivo de texto importado.';
end;


function TWizNomImportarMov_DevEx.GetArchivo: String;
begin
     Result := sArchivo.Text;
end;

procedure TWizNomImportarMov_DevEx.CargaParametros;
const
     K_MENSAJE_HACIA = 'Excepciones';
begin
     inherited;
     with ParameterList do
     begin
          AddString( 'Archivo', GetArchivo );
          AddInteger( 'Operacion', iOperacion.Valor );
          AddInteger( 'Importacion', Ord( GetTipoImportacion ) );
          AddInteger( 'Formato', iFormato.Valor );
          //AddInteger( 'YearAcum', YearAcum.ValorEntero );
          AddInteger( 'YearAcum', dmCliente.YearDefault );
          //AddInteger( 'MesAcum', MesAcum.Valor + 1 );
          AddInteger( 'MesAcum', 0 );
          AddBoolean( 'NoIncapacitados', CBNoIncapacitados.Checked );{OP: 12/06/08}
          //AddBoolean( 'ExcederLimites', Revisa( D_NOM_EXCEPCIONES_LIM_MONTO) );
          AddBoolean( 'ExcederLimites', False );

     end;

     with Descripciones do
     begin
          AddInteger( 'Importar hacia', Ord( GetTipoImportacion ) );
          AddString( 'Importar hacia', K_MENSAJE_HACIA );
          AddString( 'Archivo', GetArchivo );
          AddString( 'Operaci�n con montos', ObtieneElemento( lfOperacionMontos, iOperacion.valor )  );
          AddString( 'Tipo de formato', ObtieneElemento( lfFormatoASCII, iFormato.valor )  );
          AddBoolean( 'No importar incapacitados', CBNoIncapacitados.Checked );{OP: 12/06/08}

     end;
end;

function TWizNomImportarMov_DevEx.LeerArchivo: Boolean;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmProcesos do
        begin
             PanelAnimation.Visible := TRUE;
             Application.ProcessMessages;
             CargaParametros;
             ImportarMovGetListaASCII( ParameterList );
             PanelAnimation.Visible := FALSE;
             if ( ErrorCount > 0 ) then                 // Hubo Errores
             begin
                  ZAsciiTools.FiltraASCII( cdsDataSet, True );
                  Result := ZBaseGridShow_DevEx.GridShow( cdsDataset, TNomImportarMovGridShow_DevEx );
                  ZAsciiTools.FiltraASCII( cdsDataSet, False );
             end
             else
                  Result := True;
        end;
        if Result then
           ArchivoVerif := GetArchivo
        else
           ArchivoVerif := VACIO;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TWizNomImportarMov_DevEx.BGuardaArchivoClick(Sender: TObject);
begin
     inherited;
     sArchivo.Text := ZetaClientTools.AbreDialogo( OpenDialog, sArchivo.Text, 'dat' );
end;

procedure TWizNomImportarMov_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
          if Adelante then
          begin
               if EsPaginaActual( Archivo ) then
               begin
                    if ZetaCommonTools.StrVacio( GetArchivo ) then
                       CanMove := Error( 'Debe especificarse un archivo a importar', Archivo )
                    else
                       if not FileExists( GetArchivo ) then
                          CanMove := Error( 'El archivo ' + GetArchivo + ' no existe', Archivo )
                       else
                       begin
                            if ( ArchivoVerif <> GetArchivo ) or
                               ( ZetaDialogo.ZConfirm( Caption, 'El archivo ' + GetArchivo + ' ya fu� verificado !' + CR_LF +
                                                       'Volver a verificar ?', 0, mbYes ) ) then
                               CanMove := LeerArchivo
                            else
                               CanMove := TRUE;
                       end;
               end;
          end;
     end;
end;

function TWizNomImportarMov_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.ImportarMov( ParameterList );
end;

function TWizNomImportarMov_DevEx.GetTipoImportacion: eImportacion;
begin
     Result := eiExcepciones
end;

procedure TWizNomImportarMov_DevEx.RBImportarClick(Sender: TObject);
begin
     inherited;
     ArchivoVerif := VACIO;
     ActualizaNoIncapacitados;{OP: 12/06/08}
end;

{OP: 12/06/08}
procedure TWizNomImportarMov_DevEx.ActualizaNoIncapacitados;
begin
     CBNoIncapacitados.Enabled := TRUE;
end;

end.
