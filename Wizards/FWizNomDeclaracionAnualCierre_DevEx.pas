unit FWizNomDeclaracionAnualCierre_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ZetaEdit, StdCtrls, Buttons,
  ZetaWizard, ExtCtrls, Mask, ZetaNumero, ZetaKeyCombo,
  ZetaCommonLists,
  ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit,
  ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl;

type
  TWizNomDeclaracionAnualCierre_DevEx = class(TcxBaseWizard)
    GroupBox1: TGroupBox;
    lbMensaje1: TLabel;
    lbMensaje2: TLabel;
    lbMensaje4: TLabel;
    lbMensaje3: TLabel;
    lbMensaje5: TLabel;
    Panel1: TPanel;
    iYearLBL: TLabel;
    zmYear: TZetaNumero;
    lbStatus: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure zmYearExit(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);//DevEx(by am): Agregado para enfocar correctamente los controles.
  private
    FStatus : eStatusDeclaracionAnual;
    function GetStatusDeclaraText: string;
    procedure MensajeStatus;
    function SetStatusDeclara: Boolean;
    { Private declarations }

  public
    { Public declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros;override;

  end;

var
  WizNomDeclaracionAnualCierre_DevEx: TWizNomDeclaracionAnualCierre_DevEx;


implementation

uses
     DProcesos,
     DCliente,
     DNomina,
     ZetaCommonClasses,
     ZetaDialogo;

{$R *.DFM}

{ TWizNomDeclaracionAnual }

procedure TWizNomDeclaracionAnualCierre_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_Declaracion_anual_Cierre;

     ParametrosControl := zmYear;

     with dmCliente.GetDatosPeriodoActivo do
     begin
          zmYear.Valor := Year - 1;
     end;
     
     dmNomina.cdsDeclaracionesAnuales.Conectar;
     MensajeStatus;
end;


procedure TWizNomDeclaracionAnualCierre_DevEx.CargaParametros;
 var
    eStatusFinal: eStatusDeclaracionAnual;
begin
     if FStatus = esdAbierta then
        eStatusFinal:= esdCerrada
     else if FStatus = esdCerrada then
        eStatusFinal:= esdAbierta
     else  eStatusFinal:= esdSinCalcular;

     inherited;
     ParamInicial:=0;
     with Descripciones do
     begin
          AddInteger( 'A�o', zmYear.ValorEntero );
          AddString( 'Status Inicial', ObtieneElemento( lfStatusDeclaracionAnual, Integer( FStatus ) ) );
          AddString( 'Status Final', ObtieneElemento( lfStatusDeclaracionAnual, Integer( eStatusFinal ) ) );
     end;
     with ParameterList do
     begin
          AddInteger( 'Year', zmYear.ValorEntero );
          AddInteger( 'StatusInicial', Ord( FStatus ) );
          AddInteger( 'StatusFinal', Ord( eStatusFinal ) );
     end;
end;

function TWizNomDeclaracionAnualCierre_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.DeclaracionAnualCierre(ParameterList);
end;

function TWizNomDeclaracionAnualCierre_DevEx.SetStatusDeclara: Boolean;
begin
     Result := dmNomina.cdsDeclaracionesAnuales.Locate( 'TD_YEAR', zmYear.ValorEntero, [] );
     if Result then
        FStatus := eStatusDeclaracionAnual( dmNomina.cdsDeclaracionesAnuales.FieldByName( 'TD_STATUS' ).AsInteger )
     else
         FStatus := eStatusDeclaracionAnual( 0 );
end;

function  TWizNomDeclaracionAnualCierre_DevEx.GetStatusDeclaraText: string;
begin
     Result := ObtieneElemento( lfStatusDeclaracionAnual, Integer( FStatus ) )
end;

procedure TWizNomDeclaracionAnualCierre_DevEx.zmYearExit(Sender: TObject);
begin
     inherited;
     MensajeStatus;
end;

procedure TWizNomDeclaracionAnualCierre_DevEx.MensajeStatus;
begin
     SetStatusDeclara;

     lbMensaje1.Caption := Format( 'La Declaraci�n del a�o %s tiene status:', [zmYear.ValorAsText] );
     lbMensaje2.Caption := GetStatusDeclaraText;
     lbStatus.Caption := 'Status: ' +GetStatusDeclaraText;

     case FStatus of
          esdSinCalcular:
          begin
               lbMensaje3.Caption := 'El proceso de Cierre de Declaraci�n Anual no puede cerrar una declaraci�n:'; //Se elimino el fianl de la leyenda pues no alcanzaba a salir en VS2013
               lbMensaje4.Caption := 'Sin Calcular';
               lbMensaje5.Caption := 'realice el c�lculo de la Declaraci�n Anual antes de ejecutar este proceso';
          end;

          esdAbierta:
          begin
               lbMensaje3.Caption := 'Al ejecutar el proceso la declaraci�n quedar� marcada como:';
               lbMensaje4.Caption := 'Cerrada';
               lbMensaje5.Caption := 'una vez cerrada, ya no ser� posible volver a realizar el c�lculo de la Declaraci�n Anual';
          end;

          esdCerrada:
          begin
               lbMensaje3.Caption := 'Al ejecutar el proceso la declaraci�n quedar� marcada como:';
               lbMensaje4.Caption := 'Abierta';
               lbMensaje5.Caption := 'una vez abierta, es posible volver a realizar el c�lculo de la Declaraci�n Anual';
          end;
     end;

     Advertencia.Caption := lbMensaje3.Caption+' '+ lbMensaje4.Caption+' '+ lbMensaje5.Caption +' '+ 'presione el bot�n ''Aplicar'' para iniciar el proceso.';
     {with Advertencia do
     begin
          Text := '';
          Lines.Append( Text );
          Lines.Append( string( lbMensaje3.Caption ) );
          Lines.Append( string( lbMensaje4.Caption ) );
          Lines.Append( string( lbMensaje5.Caption ) );
          Lines.Append( '' );
          Lines.Append( 'Presione el bot�n ''Aplicar'' para iniciar el proceso.' );
     end;}
end;

procedure TWizNomDeclaracionAnualCierre_DevEx.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin

     ParametrosControl := nil;
     with Wizard do
     begin
          if Adelante and EsPaginaActual( Parametros) and ( FStatus =esdSinCalcular ) then
             CanMove := Error( Format( 'El a�o %d no cuenta con una Declaraci�n calculada, realice el c�lculo de la Declaraci�n Anual para ese a�o antes de ejecutar este proceso',[zmYear.ValorEntero] ),
                                      zmYear );
     end;
     inherited;
end;

procedure TWizNomDeclaracionAnualCierre_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.Adelante then
          if Wizard.EsPaginaActual( Parametros ) then
             ParametrosControl := zmYear;
     inherited;
end;

procedure TWizNomDeclaracionAnualCierre_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  //Advertencia.Caption :='';//Se defne en el metodo MensajeStatus
end;

end.
