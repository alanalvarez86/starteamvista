inherited NomTransferenciasCosteoGridSelect_DevEx: TNomTransferenciasCosteoGridSelect_DevEx
  Left = 570
  Top = 379
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Seleccione Los Empleados Deseados'
  ClientWidth = 501
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 501
    inherited OK_DevEx: TcxButton
      Left = 337
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 416
      Cancel = True
    end
  end
  inherited PanelSuperior: TPanel
    Width = 501
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 501
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.Indicator = True
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 65
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre Completo'
        DataBinding.FieldName = 'PRETTYNAME'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 220
      end
      object CC_CODIGO: TcxGridDBColumn
        DataBinding.FieldName = 'CC_CODIGO'
        Width = 70
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
