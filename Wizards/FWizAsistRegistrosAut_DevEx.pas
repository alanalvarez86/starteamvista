unit FWizAsistRegistrosAut_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, Mask,
     ZetaWizard,
     ZetaFecha,
     ZetaEdit, ZCXBaseWizardFiltro, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
  cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl, dxSkinsCore, TressMorado2013, dxSkinsDefaultPainters;

type
  TWizAsistRegistrosAut_DevEx = class(TBaseCXWizardFiltro)
    FechaInicial: TZetaFecha;
    FechaFinal: TZetaFecha;
    Label1: TLabel;
    Label2: TLabel;
    CH_MOTIVOlbl: TLabel;
    CH_MOTIVO: TZetaKeyLookup_DevEx;
    lEntrada: TCheckBox;
    lSalida: TCheckBox;
    lSabados: TCheckBox;
    lDescanso: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizAsistRegistrosAut_DevEx: TWizAsistRegistrosAut_DevEx;

implementation

uses DCliente,
     DProcesos,
     ZetaDialogo,
     ZetaCommonClasses,
     FToolsAsistencia,
     DTablas,
     ZetaCommonTools;

{$R *.DFM}

procedure TWizAsistRegistrosAut_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := FechaInicial;
     with dmCliente do
     begin
          FechaInicial.Valor := FechaAsistencia;
          FechaFinal.Valor := FechaAsistencia;
     end;
     HelpContext := H20235_Registros_automaticos;
end;

procedure TWizAsistRegistrosAut_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
        ParametrosControl := nil;

     inherited;
     inherited;
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros ) then
          begin
               if not ( ( lEntrada.Checked ) or ( lSalida.Checked ) ) then
               begin
                    ZError( Caption, 'Tiene Que Seleccionarse Una Opci�n De Agregar Checadas', 0 );
                    ActiveControl := lEntrada;
                    CanMove := False;
               end;
               if ( CapturaObligatoriaMotCH ) and (  StrVacio(CH_MOTIVO.Llave) ) then
               begin
                    ZError( Caption, 'Motivo no puede quedar vac�o', 0 );
                    ActiveControl := CH_MOTIVO;
                    CanMove := False;
               end;
               if CanMove then
                    CanMove := dmCliente.PuedeCambiarTarjetaDlg( FechaInicial.Valor, FechaFinal.Valor );
          end;
     end;
end;

procedure TWizAsistRegistrosAut_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddDate( 'FechaInicial', FechaInicial.Valor );
          AddDate( 'FechaFinal', FechaFinal.Valor );
          AddBoolean( 'AgregarEntrada', lEntrada.Checked );
          AddBoolean( 'AgregarSalida', lSalida.Checked );
          AddBoolean( 'AgregarSabados', lSabados.Checked );
          AddBoolean( 'AgregarDescanso', lDescanso.Checked );
          AddString( 'Motivo', CH_MOTIVO.Llave );
          AddBoolean( 'PuedeCambiarTarjeta', dmCliente.PuedeModificarTarjetasAnteriores );
     end;
          with Descripciones do
     begin
          AddDate( 'Fecha Inicial', FechaInicial.Valor );
          AddDate( 'Fecha Final', FechaFinal.Valor );
          AddBoolean( 'Agregar Entrada', lEntrada.Checked );
          AddBoolean( 'Agregar Salida', lSalida.Checked );
          AddBoolean( 'Considerar S�bados', lSabados.Checked );
          AddBoolean( 'Considerar Descanso', lDescanso.Checked );
          if(CH_MOTIVO.visible)then
          AddString( 'Motivo', CH_MOTIVO.Llave );
     end;
end;

function TWizAsistRegistrosAut_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.RegistrosAutomaticos( ParameterList );
end;

{ACL230409}
procedure TWizAsistRegistrosAut_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmTablas do
     begin
          CH_MOTIVO.LookupDataset := cdsMotCheca;
          cdsMotCheca.Conectar;
     end;
     CH_MOTIVO.Visible:= FToolsAsistencia.PermitirCapturaMotivoCH;
     CH_MOTIVOlbl.Visible := CH_MOTIVO.Visible;
end;

procedure TWizAsistRegistrosAut_DevEx.WizardAfterMove(Sender: TObject);
begin
    if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := FechaInicial;

     inherited;
end;

end.