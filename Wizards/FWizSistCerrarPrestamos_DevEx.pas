unit FWizSistCerrarPrestamos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZCXBaseWizardFiltro, Mask, ZetaFecha, StdCtrls, ExtCtrls, ComCtrls,
  ZetaEdit, Buttons, ZetaCXWizard, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Menus,
  ZetaKeyLookup_DevEx, cxButtons, cxRadioGroup, cxTextEdit, cxMemo,
  dxCustomWizardControl, dxWizardControl, dxSkinsCore, cxGroupBox, cxLabel,
  dxGDIPlusClasses, cxImage;

type
  TWizSISTCerrarPrestamos_DevEx = class(TBaseCXWizardFiltro)
    TipoPrestamoLBL: TLabel;
    TipoPrestamo: TZetaKeyLookup_DevEx;
    chbUtilizaRef: TCheckBox;
    lblReferencia: TLabel;
    edReferencia: TZetaEdit;
    Operacion: TcxRadioGroup;
    PonerFecha: TCheckBox;
    FechaCierreLBL: TLabel;
    FechaCierre: TZetaFecha;

    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure OperacionClick(Sender: TObject);
    procedure PonerFechaClick(Sender: TObject);
    procedure chbUtilizaRefClick(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
    procedure OperacionPropertiesChange(Sender: TObject);
  private
    { Private declarations }
    procedure SetControls;
    procedure SetControlsReferen;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizSISTCerrarPrestamos_DevEx: TWizSISTCerrarPrestamos_DevEx;

implementation

uses DProcesos,
     DCliente,
     DTablas,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo;

{$R *.DFM}

procedure TWizSISTCerrarPrestamos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := TipoPrestamo;
     HelpContext := H33517_Cierre_de_prestamos;
     SetControls;
     TipoPrestamo.LookupDataset := dmTablas.cdsTPresta;
end;

procedure TWizSISTCerrarPrestamos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     FechaCierre.Valor := dmCliente.FechaDefault;
     dmTablas.cdsTPresta.Conectar;
end;

procedure TWizSISTCerrarPrestamos_DevEx.SetControls;
var
   lEnabled: Boolean;
begin
     with PonerFecha do
     begin
          Enabled := ( Operacion.ItemIndex = 1 );
          lEnabled := Enabled and Checked;
     end;
     FechaCierre.Enabled := lEnabled;
     FechaCierreLBL.Enabled := lEnabled;
end;

procedure TWizSISTCerrarPrestamos_DevEx.SetControlsReferen;
begin
     with edReferencia do
     begin
          Enabled := chbUtilizaRef.Checked;
          lblReferencia.Enabled := Enabled;
          Clear;
     end;
end;

procedure TWizSISTCerrarPrestamos_DevEx.OperacionClick(Sender: TObject);
begin
     inherited;
     SetControls;
end;

procedure TWizSISTCerrarPrestamos_DevEx.PonerFechaClick(Sender: TObject);
begin
     inherited;
     SetControls;
end;

procedure TWizSISTCerrarPrestamos_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
ParametrosControl := nil;
     inherited;
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros ) then
          begin
               if ZetaCommonTools.StrVacio( TipoPrestamo.Llave ) then
               begin
                    ZetaDialogo.zError( Caption, '� Tipo De Pr�stamo No Fu� Especificado !', 0 );
                    ActiveControl := TipoPrestamo;
                    CanMove := False;
               end
               else
               begin
                    if( ( chbUtilizaRef.Checked ) and ( ZetaCommonTools.StrVacio( edReferencia.Text ) ) )then
                        CanMove := Error('� Referencia De Pr�stamo No Fu� Especificada !', self.edReferencia );
               end;
          end;
     end;
end;

procedure TWizSISTCerrarPrestamos_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddString( 'TipoPrestamo', TipoPrestamo.Llave );
          AddInteger( 'Operacion', Operacion.ItemIndex );
          AddBoolean( 'PonerFechaCierre', PonerFecha.Checked );
          AddDate( 'FechaCierre', FechaCierre.Valor );
          AddString( 'RefPrestamo', edReferencia.Text );
          AddBoolean( 'UtilizaReferencia', chbUtilizaRef.Checked );
     end;

     with Descripciones do
     begin
          AddString( 'Tipo de Pr�stamo', TipoPrestamo.Llave );
          AddBoolean( 'Utilizar Referencia', chbUtilizaRef.Checked );
          If(chbUtilizaRef.Checked)then
          AddString( 'Referencia Pr�stamo', edReferencia.Text );
          AddString( 'Operaci�n', StringReplace(Operacion.Properties.Items[Operacion.ItemIndex].caption,'&','',[rfReplaceAll, rfIgnoreCase] ));
          AddBoolean( 'Poner Fecha de Cierre', PonerFecha.Checked );
          if(PonerFecha.Checked) then
          AddDate( 'Fecha de Cierre', FechaCierre.Valor );

     end;
end;

function TWizSISTCerrarPrestamos_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CerrarPrestamos( ParameterList );
end;

procedure TWizSISTCerrarPrestamos_DevEx.chbUtilizaRefClick(Sender: TObject);
begin
     inherited;
     SetControlsReferen;
end;

procedure TWizSISTCerrarPrestamos_DevEx.WizardAfterMove(Sender: TObject);
begin
  inherited;
if Wizard.EsPaginaActual(Parametros) then
  ParametrosControl := TipoPrestamo;
end;

procedure TWizSISTCerrarPrestamos_DevEx.OperacionPropertiesChange(
  Sender: TObject);
begin
  inherited;
    SetControls;
end;

end.



