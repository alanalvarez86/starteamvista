inherited EmpRenumeraGridSelect_DevEx: TEmpRenumeraGridSelect_DevEx
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Empleados a Procesar'
  ClientWidth = 501
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 501
    inherited OK_DevEx: TcxButton
      Left = 337
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 416
    end
  end
  inherited PanelSuperior: TPanel
    Width = 501
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 501
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.Indicator = True
      object CB_CODIGO_OLD: TcxGridDBColumn
        Caption = 'C'#243'digo Actual'
        DataBinding.FieldName = 'CB_CODIGO_OLD'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 91
      end
      object CB_CODIGO_NEW: TcxGridDBColumn
        Caption = 'C'#243'digo Nuevo'
        DataBinding.FieldName = 'CB_CODIGO_NEW'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 87
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre del Empleado'
        DataBinding.FieldName = 'PRETTYNAME'
        Width = 114
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DataSource: TDataSource
    Left = 224
    Top = 56
  end
end
