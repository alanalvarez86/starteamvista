unit FWizEmpCancelarVacaciones_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ComCtrls, ExtCtrls, Mask,
     FWizEmpBaseFiltro,
     ZetaEdit,
     ZetaWizard,
     ZetaFecha, ZetaKeyCombo, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxContainer, cxEdit, Menus, ZetaCXWizard, ZetaKeyLookup_DevEx, cxButtons,
  cxRadioGroup, cxTextEdit, cxMemo, cxGroupBox, cxLabel, dxGDIPlusClasses,
  cxImage, dxCustomWizardControl, dxWizardControl;

type
  TWizEmpCancelarVacaciones_DevEx = class(TWizEmpBaseFiltro)
    TipoMovimiento: TZetaDBKeyCombo;
    TipoFecha: TZetaDBKeyCombo;
    Fecha: TZetaFecha;
    TipoMovimientoLBL: TLabel;
    TipoFechaLBL: TLabel;
    FechaLBL: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject); //DevEx(by am): Se agrega para validar el enfoque de controles.
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizEmpCancelarVacaciones_DevEx: TWizEmpCancelarVacaciones_DevEx;

const
     //Constantes Paginas
     K_PAGE_PARAMETROS =0;

implementation

uses DCliente,
     DProcesos,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZBaseSelectGrid_DevEx,
     FEmpCancelarVacacionesGridSelect_DevEx;

{$R *.DFM}

{ TWizEmpCancelarVacaciones }

procedure TWizEmpCancelarVacaciones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := Fecha;
     Fecha.Valor := dmCliente.FechaDefault;
     TipoFecha.ItemIndex := 0;
     TipoMovimiento.ItemIndex := 0;
     HelpContext:= H11611_Cancelar_Vacaciones;
     //DevEx(by am): Es necesario especificar que Ejecucion sera la ultima pagina del wizard.
     Parametros.PageIndex := K_PAGE_PARAMETROS;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 2; //3
     Ejecucion.PageIndex := WizardControl.PageCount - 1;

end;

procedure TWizEmpCancelarVacaciones_DevEx.CargaParametros;
begin

     inherited CargaParametros;
     with Descripciones do
     begin
          AddDate( 'Fecha', Fecha.Valor );
          AddString( 'Tipo de Fecha', ObtieneElemento( lfTipoKarFecha, TipoFecha.ItemIndex ) );
          AddString( 'Cancelar Modificación', ObtieneElemento( lfCancelaMod, TipoMovimiento.ItemIndex ) );
     end;
     with ParameterList do
     begin
          AddDate( 'Fecha', Fecha.Valor );
          AddInteger( 'TipoFecha', TipoFecha.ItemIndex );
          AddInteger( 'TipoMovimiento', TipoMovimiento.ItemIndex );
     end;
end;

procedure TWizEmpCancelarVacaciones_DevEx.CargaListaVerificacion;
begin
     dmProcesos.CancelarVacacionesGetLista( ParameterList );
end;

function TWizEmpCancelarVacaciones_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpCancelarVacacionesGridSelect_DevEx );
end;

function TWizEmpCancelarVacaciones_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CancelarVacaciones( ParameterList, Verificacion );
end;

procedure TWizEmpCancelarVacaciones_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption :='Este proceso cancelará las vacaciones de los empleados que se indicaron.';
end;

procedure TWizEmpCancelarVacaciones_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual( Parametros )then
         ParametrosControl := Fecha
     else
         ParametrosControl := nil;// Si la pagina es distinta no se necesita enfocar nada.
     inherited;

end;

end.
