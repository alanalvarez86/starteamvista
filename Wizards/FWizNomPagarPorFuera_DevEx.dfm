inherited WizNomPagarPorFuera_DevEx: TWizNomPagarPorFuera_DevEx
  Left = 244
  Top = 173
  Caption = 'Pagar Por Fuera'
  ClientHeight = 424
  ClientWidth = 445
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 445
    Height = 424
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que pone una marca en los registros de n'#243'mina de los emp' +
        'leados seleccionados para que puedan ser filtrados en recibos y ' +
        'listados.'
      Header.Title = 'Pagar por fuera'
      inherited GroupBox1: TGroupBox
        Left = 7
        Top = 50
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 189
        Width = 423
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 423
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 355
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 16
        Top = 137
      end
      inherited sFiltroLBL: TLabel
        Left = 41
        Top = 166
      end
      inherited Seleccionar: TcxButton
        Left = 144
        Top = 254
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 68
        Top = 134
      end
      inherited sFiltro: TcxMemo
        Left = 68
        Top = 165
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 68
        Top = 4
      end
      inherited bAjusteISR: TcxButton
        Left = 382
        Top = 166
      end
    end
  end
end
