unit FWizAsisBaseFiltro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZCXBaseWizardFiltro, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
  ExtCtrls, ZetaKeyLookup_DevEx, StdCtrls, cxButtons, ZetaEdit,
  cxRadioGroup, cxTextEdit, cxMemo, cxGroupBox, cxLabel, dxGDIPlusClasses,
  cxImage, dxCustomWizardControl, dxWizardControl;

type
  TWizAsisBaseFiltro = class(TBaseCXWizardFiltro)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  WizAsisBaseFiltro: TWizAsisBaseFiltro;

implementation

{$R *.dfm}

end.
