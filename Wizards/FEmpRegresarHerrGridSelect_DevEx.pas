unit FEmpRegresarHerrGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, ExtCtrls, ZBaseSelectGrid_DevEx,
  cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxTextEdit, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons, cxCalendar;

type
  TEmpRegresarHerrGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    TO_CODIGO: TcxGridDBColumn;
    KT_FEC_INI: TcxGridDBColumn;
    KT_REFEREN: TcxGridDBColumn;
    KT_TALLA: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpRegresarHerrGridSelect_DevEx: TEmpRegresarHerrGridSelect_DevEx;

implementation

uses ZBasicoSelectGrid_DevEx;

{$R *.DFM}

procedure TEmpRegresarHerrGridSelect_DevEx.FormShow(Sender: TObject);
begin
  inherited;

  (ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO')).Caption := 'Empleado';
  (ZetaDBGridDBTableView.GetColumnByFieldName('PrettyName')).Caption := 'Nombre Completo';
  (ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO')).MinWidth := 75;
 

end;

end.
