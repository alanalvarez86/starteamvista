inherited WizEmpSalarioIntegrado_DevEx: TWizEmpSalarioIntegrado_DevEx
  Left = 714
  Top = 233
  Caption = 'Recalcular Salarios Integrados'
  ClientHeight = 530
  ClientWidth = 462
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 462
    Height = 530
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'El rec'#225'lculo de salarios integrados actualiza el factor de integ' +
        'raci'#243'n por aumento en la antig'#252'edad del empleado o por aumento d' +
        'el salario m'#237'nimo.'
      Header.Title = 'Rec'#225'lculo de Salarios Integrados'
      object dReferenciaLBL: TLabel
        Left = 21
        Top = 5
        Width = 103
        Height = 13
        Alignment = taRightJustify
        Caption = '&Fecha de Referencia:'
        FocusControl = dReferencia
        Transparent = True
      end
      object sDescripcionLBL: TLabel
        Left = 65
        Top = 153
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = '&Descripci'#243'n:'
        FocusControl = sDescripcion
        Transparent = True
      end
      object sObservaLBL: TLabel
        Left = 50
        Top = 184
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = '&Observaciones:'
        FocusControl = sObserva
        Transparent = True
      end
      object dReferencia: TZetaFecha
        Left = 128
        Top = 0
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '02/Jan/98'
        Valor = 35797.000000000000000000
      end
      object RGFecha: TcxRadioGroup
        Left = 128
        Top = 23
        Caption = ' Fecha de Cambio '
        Properties.Columns = 2
        Properties.Items = <
          item
            Caption = 'Referencia'
            Value = '0'
          end
          item
            Caption = 'Aniversario'
            Value = '1'
          end>
        ItemIndex = 0
        TabOrder = 1
        Height = 50
        Width = 290
      end
      object RGIMSS: TcxRadioGroup
        Left = 128
        Top = 74
        Caption = ' Aviso a IMSS '
        Properties.Items = <
          item
            Caption = 'Misma fecha del cambio'
            Value = '0'
          end
          item
            Caption = 'Fecha espec'#237'fica'
            Value = '1'
          end>
        ItemIndex = 0
        TabOrder = 2
        OnClick = RGIMSSClick
        Height = 66
        Width = 290
      end
      object sDescripcion: TEdit
        Left = 128
        Top = 150
        Width = 290
        Height = 21
        TabOrder = 3
        Text = 'Rec'#225'lculo de Salarios Integrados'
      end
      object sObserva: TcxMemo
        Left = 128
        Top = 181
        Properties.ScrollBars = ssVertical
        TabOrder = 4
        Height = 177
        Width = 290
      end
      object lIncapacitados: TcxCheckBox
        Left = 5
        Top = 363
        Caption = 'No Inclu'#237'r &Incapacitados:'
        Properties.Alignment = taRightJustify
        State = cbsChecked
        TabOrder = 5
        Transparent = True
        Width = 140
      end
      object dIMSS: TZetaFecha
        Left = 295
        Top = 114
        Width = 115
        Height = 22
        Cursor = crArrow
        Enabled = False
        TabOrder = 6
        Text = '02/Jan/98'
        Valor = 35797.000000000000000000
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 295
        Width = 440
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 440
        inherited Advertencia: TcxLabel
          Caption = ''
          Style.IsFontAssigned = True
          Width = 372
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 14
      end
      inherited sFiltroLBL: TLabel
        Left = 39
        Top = 183
      end
      inherited Seleccionar: TcxButton
        Left = 142
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 66
      end
      inherited sFiltro: TcxMemo
        Left = 66
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 66
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    Left = 544
    Top = 7
  end
end
