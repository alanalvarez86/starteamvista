unit FWizNomCalcularSimulacion_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FWizNomCalcular_DevEx, ZetaWizard, ComCtrls, ExtCtrls, StdCtrls,
  ZetaEdit, ZetaDBTextBox, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxContainer, cxEdit, Menus, ZetaCXWizard, cxLabel, dxGDIPlusClasses,
  cxImage, cxCheckBox, cxGroupBox, ZetaKeyLookup_DevEx, cxButtons,
  cxRadioGroup, cxTextEdit, cxMemo, dxCustomWizardControl, dxWizardControl;

type
  TWizNomCalcularSimulacion_DevEx = class(TWizNomCalcular_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
    { Private declarations }
  protected
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizNomCalcularSimulacion_DevEx: TWizNomCalcularSimulacion_DevEx;

implementation
uses
    ZGlobalTress, ZetaCommonClasses, DProcesos, DGlobal, ZcxWizardBasico;

{$R *.dfm}

procedure TWizNomCalcularSimulacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_Calcular_Simulacion_Finiquitos_Global;
end;

function TWizNomCalcularSimulacion_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CalcularSimulacionGlobal( Global.GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS ) );
end;

procedure TWizNomCalcularSimulacion_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'Se efectuar� el c�lculo de n�mina de finiquito para los empleados seleccionados.';
end;


end.
