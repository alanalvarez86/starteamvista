unit FWizEmpImportarOrganigrama_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ZetaEdit, StdCtrls,
  ExtCtrls, ZetaKeyCombo, ZcxBaseWizard,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxContainer, cxEdit, ZetaCXWizard,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl, Menus, cxButtons;
  
type
  TWizImportarOrganigrama_DevEx = class(TcxBaseWizard)
    PanleFiltros: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    ArchivoSeek: TcxButton;
    Label4: TLabel;
    Archivo: TEdit;
    Formato: TZetaKeyCombo;
    Observaciones: TEdit;
    cbFFecha: TZetaKeyCombo;
    OpenDialog: TOpenDialog;
    PanelAnimation: TPanel;
    Animate: TAnimate;
    PanelMensaje: TPanel;
    chkCopiar: TCheckBox;
    chkBorrarOrg: TCheckBox;
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    //DevEx     //Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
    FVerificaASCII: Boolean;
    FVerificacion: Boolean;
    ArchivoVerif : String;

    function LeerArchivo: Boolean;
    function GetArchivo: String;
    function Verificar: Boolean;

    procedure SetVerificacion(const Value: Boolean);
    procedure CargaListaVerificaASCII; overload;
    procedure CargaListaVerificacion; overload;
  protected
    procedure CargaParametros; override;
    function EjecutarWizard: Boolean; override;
  public
  end;

var
  WizImportarOrganigrama_DevEx: TWizImportarOrganigrama_DevEx;

implementation

uses DProcesos,
     ZetaMessages,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZAsciiTools,
     ZetaClientTools,
     ZetaDialogo,
     ZBaseGridShow_DevEx,
     FEmpImportarOrganigramaGridShow_DevEx,
     FEmpImportarOrganigramaGridSelect_DevEx,
     ZBaseSelectGrid_Devex, ZcxWizardBasico;

{$R *.dfm}

procedure TWizImportarOrganigrama_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     PanelAnimation.Visible := FALSE;
     ParametrosControl := Archivo;
     Archivo.Text := ZetaMessages.SetFileNameDefaultPath( 'Organigrama.DAT' );

     Formato.ItemIndex := Ord( faASCIIFijo );
     cbFFecha.ItemIndex := Ord( ifDDMMYYYYs );
     HelpContext:= H_Importacion_Organigrama;
end;

procedure TWizImportarOrganigrama_DevEx.CargaParametros;
begin
     inherited CargaParametros;

     with ParameterList do
     begin
          AddInteger( 'Formato', Formato.Valor );
          AddInteger( 'FormatoImpFecha', cbFFecha.Valor );
          AddString( 'Archivo', Archivo.Text );
          AddMemo( 'Observaciones', Observaciones.Text );
          AddBoolean('CopiarContrato', chkCopiar.Checked  );
          AddBoolean('BorrarOrg', chkBorrarOrg.Checked  );

     end;
     with Descripciones do
     begin
          AddString( 'Archivo', Archivo.Text );
          AddString( 'Formato ASCII', ObtieneElemento( lfFormatoASCII, Formato.Valor ) );
          AddString( 'Formato fechas', ObtieneElemento( lfFormatoImpFecha, cbFFecha.Valor ) );
          AddBoolean('Copiar contrato a Plaza',chkCopiar.Checked );
          AddBoolean('Borrar organigrama actual',chkBorrarOrg.Checked );
          AddMemo( 'Observaciones', Observaciones.Text );
     end;

end;

procedure TWizImportarOrganigrama_DevEx.CargaListaVerificaASCII;
begin
     with dmProcesos do
     begin
          ImportarOrganigramaGetListaASCII( ParameterList );
          if ( ErrorCount > 0 ) then                 // Hubo Errores
          begin
               with dmProcesos do
               begin
                    ZAsciiTools.FiltraASCII( cdsDataSet, True );
                    FVerificaASCII := ZBaseGridShow_DevEx.GridShow( cdsDataset, TEmpImportarOrganigramaGridShow_DevEx );
                    ZAsciiTools.FiltraASCII( cdsDataSet, False );
               end;
          end
          else
              FVerificaASCII := True;
     end;
end;

procedure TWizImportarOrganigrama_DevEx.CargaListaVerificacion;
begin
     dmProcesos.ImportarOrganigramaGetLista( ParameterList );
end;

function TWizImportarOrganigrama_DevEx.Verificar: Boolean;
begin
     dmProcesos.cdsDataSet.First;
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpImportarOrganigramaGridSelect_DevEx, FALSE );
end;

procedure TWizImportarOrganigrama_DevEx.SetVerificacion(const Value: Boolean);
begin
     FVerificacion := Value;
end;

function TWizImportarOrganigrama_DevEx.GetArchivo: String;
begin
     Result := Archivo.Text;
end;


function TWizImportarOrganigrama_DevEx.LeerArchivo: Boolean;
begin
     Result := False;
     PanelAnimation.Visible := TRUE;
     Application.ProcessMessages;
     CargaParametros;
     CargaListaVerificaASCII;
     PanelAnimation.Visible := FALSE;
     if FVerificaASCII then
     begin
          CargaListaVerificacion;
          SetVerificacion( Verificar );
          Result := FVerificacion;
     end;
     if Result then
        ArchivoVerif := GetArchivo
     else
        ArchivoVerif := VACIO;
end;

procedure TWizImportarOrganigrama_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     ParametrosControl := nil;
     if Wizard.Adelante then
     begin
          //if ( PageControl.ActivePage = Parametros ) then
          if ( WizardControl.ActivePage = Parametros ) then
          begin
               if StrVacio( GetArchivo ) then
                  CanMove := Error( 'Debe Especificarse un Archivo a Importar', Archivo )
               else
                  if not FileExists( GetArchivo ) then
                     CanMove := Error( 'El Archivo ' + GetArchivo + ' No Existe', Archivo )
                  else
                  begin
                       if ( ArchivoVerif <> GetArchivo ) or
                          ( ZConfirm( Caption, 'El Archivo ' + GetArchivo + ' Ya Fu� Verificado !' + CR_LF +
                                                  'Volver a Verificar ?', 0, mbYes ) ) then
                          CanMove := LeerArchivo
                       else
                          CanMove := TRUE;
                  end;
          end;
     end;
end;

procedure TWizImportarOrganigrama_DevEx.ArchivoSeekClick(Sender: TObject);
begin
     Archivo.Text := AbreDialogo( OpenDialog, Archivo.Text, 'dat' );
end;

function TWizImportarOrganigrama_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.ImportarOrganigrama( ParameterList );
end;

procedure TWizImportarOrganigrama_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'Al ejecutar el proceso se inicializar� el organigrama con los empleados y puestos indicados.';
end;


//DevEx     //Agregado para enfocar un control
procedure TWizImportarOrganigrama_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := Archivo
     else
         ParametrosControl := nil;
     inherited;
end;


end.
