inherited EmpImportarKardexGridShow_DevEx: TEmpImportarKardexGridShow_DevEx
  Left = 204
  Top = 186
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Lista de Errores Detectados'
  ClientWidth = 569
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 569
    inherited OK_DevEx: TcxButton
      Left = 405
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 484
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 569
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object RENGLON: TcxGridDBColumn
        Caption = 'Rengl'#243'n'
        DataBinding.FieldName = 'RENGLON'
        Width = 45
      end
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
        Width = 65
      end
      object OPERACION: TcxGridDBColumn
        Caption = 'Operaci'#243'n'
        DataBinding.FieldName = 'OPERACION'
        Width = 55
      end
      object FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'FECHA'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
        Width = 75
      end
      object CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CODIGO'
        Width = 50
      end
      object ADICIONALES: TcxGridDBColumn
        Caption = 'Adicionales'
        DataBinding.FieldName = 'ADICIONALES'
        Width = 100
      end
      object COMENTARIO: TcxGridDBColumn
        Caption = 'Comentarios'
        DataBinding.FieldName = 'COMENTARIO'
        Width = 80
      end
      object NUM_ERROR: TcxGridDBColumn
        Caption = 'Errores'
        DataBinding.FieldName = 'NUM_ERROR'
        Width = 50
      end
      object DESC_ERROR: TcxGridDBColumn
        Caption = 'Descripci'#243'n de Errores'
        DataBinding.FieldName = 'DESC_ERROR'
        Width = 450
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
