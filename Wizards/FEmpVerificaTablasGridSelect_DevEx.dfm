inherited EmpVerificaTablasGridSelect_DevEx: TEmpVerificaTablasGridSelect_DevEx
  Width = 521
  Caption = 'Lista de Tablas y Cat'#225'logos No Existentes'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 505
    inherited OK_DevEx: TcxButton
      Left = 341
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 420
      Cancel = True
    end
    inherited Excluir: TcxButton
      Enabled = False
    end
    inherited Revertir: TcxButton
      Enabled = False
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 505
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsView.Indicator = True
      object DESC_ENTIDAD: TcxGridDBColumn
        Caption = 'Nombre de la Tabla'
        DataBinding.FieldName = 'DESC_ENTIDAD'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 200
      end
      object CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CODIGO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 70
      end
      object DESCRIPCION: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'DESCRIPCION'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 200
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DataSource: TDataSource
    Left = 144
    Top = 40
  end
end
