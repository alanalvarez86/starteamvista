unit FEmpCancelarVacacionesGridSelect_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, StdCtrls, Buttons, ExtCtrls, Db,
     ZBaseSelectGrid_DevEx,
     ZetaDBGrid, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxButtons, cxCalendar;

type
  TEmpCancelarVacacionesGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    VA_GOZO: TcxGridDBColumn;
    VA_PAGO: TcxGridDBColumn;
    VA_P_PRIMA: TcxGridDBColumn;
    VA_FEC_INI: TcxGridDBColumn;
    VA_CAPTURA: TcxGridDBColumn;
    US_CODIGO: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpCancelarVacacionesGridSelect_DevEx: TEmpCancelarVacacionesGridSelect_DevEx;

implementation

{$R *.DFM}

procedure TEmpCancelarVacacionesGridSelect_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with DataSet do
     begin
          MaskPesos( 'VA_GOZO' );
          MaskPesos( 'VA_PAGO' );
          MaskPesos( 'VA_P_PRIMA' );
     end;
end;

end.
