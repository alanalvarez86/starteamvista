unit FWizEmpImportarAhorrosPrestamos_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ZetaKeyCombo,
  ExtCtrls, ZcxBaseWizard, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxContainer, cxEdit, ZetaCXWizard, cxGroupBox, cxLabel, dxGDIPlusClasses,
  cxImage, dxCustomWizardControl, dxWizardControl, Menus, cxRadioGroup,
  cxButtons;

type
  TWizEmpImportarAhorrosPrestamos_DevEx = class(TcxBaseWizard)
    Panel1: TPanel;
    grImportar: TcxRadioGroup;
    Archivo: TEdit;
    Formato: TZetaKeyCombo;
    FormatoFecha: TZetaKeyCombo;
    ArchivoSeek: TcxButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    OpenDialog: TOpenDialog;
    PanelAnimation: TPanel;
    Animate: TAnimate;
    PanelMensaje: TPanel;
    procedure ArchivoSeekClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    //DevEx     //Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FVerificaASCII: Boolean;
    ArchivoVerif : String;
    function LeerArchivo: Boolean;
    procedure CargaListaVerificaASCII;
    function GetArchivo: String;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }

  end;

var
  WizEmpImportarAhorrosPrestamos_DevEx: TWizEmpImportarAhorrosPrestamos_DevEx;

implementation

{$R *.DFM}

uses DProcesos,
     DCliente,
     ZetaDialogo,
     ZetaMessages,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaClientTools,
     ZAsciiTools,
     DGlobal,
     ZGlobalTress,
     ZBaseGridShow_DevEx,
     FEmpImportarAhorrosGridShow_DevEx,
     FEmpImportarPrestamosGridShow_DevEx,
     FEmpImportarACarAboGridShow_DevEx,
     ZAccesosTress,
     ZAccesosMgr,
     FEmpImportarPCarAboGridShow_DevEx,
     ZcxWizardBasico;

procedure TWizEmpImportarAhorrosPrestamos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     PanelAnimation.Visible := FALSE;
     ParametrosControl := Archivo;
     with Archivo do
     begin
          Tag := K_GLOBAL_DEF_EMP_IMP_AHO_PRE;
     end;
     Formato.ItemIndex := Ord( faASCIIDel );
     ArchivoVerif:= VACIO;
     FormatoFecha.ItemIndex := Ord( ifDDMMYYYYs);
     HelpContext := H_Importar_Ahorros_Prestamos;
end;

procedure TWizEmpImportarAhorrosPrestamos_DevEx.CargaParametros;
const
    aTipoImportar : array [ 0 .. 3] of ZetaPChar = ( 'Pr�stamos', 'Ahorros', 'Cargo/Abono Pr�stamo', 'Cargo/Abono Ahorro');
begin
     inherited CargaParametros;

     with ParameterList do
     begin
          AddInteger( 'TipoImportar', grImportar.ItemIndex );
          AddInteger( 'Formato', Formato.Valor );
          AddString( 'Archivo', Archivo.Text );
          AddInteger( 'FormatoFecha', FormatoFecha.Valor);
          AddBoolean( 'PuedeActualizar', FALSE );
          AddBoolean( 'TieneDerechoReglas', CheckDerecho( D_EMP_NOM_PRESTAMOS,K_DERECHO_SIST_KARDEX ));
     end;

     with Descripciones do
     begin
          AddString( 'Tipo importaci�n', aTipoImportar[grImportar.ItemIndex] );
          AddString( 'Archivo', Archivo.Text );
          AddString( 'Formato ASCII', ObtieneElemento( lfFormatoASCII, Formato.Valor ) );
          AddString( 'Formato Fecha', ObtieneElemento( lfFormatoImpFecha, FormatoFecha.Valor ) );
     end;

     with dmCliente do
     begin
          CargaActivosIMSS( ParameterList );
          CargaActivosPeriodo( ParameterList );
          CargaActivosSistema( ParameterList );
     end;
end;

procedure TWizEmpImportarAhorrosPrestamos_DevEx.CargaListaVerificaASCII;
begin
     with dmProcesos do
     begin
          ImportarAhorrosPrestamosGetListaASCII( ParameterList );
          if ( ErrorCount > 0 ) then                 // Hubo Errores
          begin
               with dmProcesos do
               begin
                    ZAsciiTools.FiltraASCII( cdsDataSet, True );
                    case grImportar.ItemIndex of
                         0 : FVerificaASCII := ZBaseGridShow_DevEx.GridShow( cdsDataset, TEmpImportarPrestamosGridShow_DevEx );
                         1 : FVerificaASCII := ZBaseGridShow_DevEx.GridShow( cdsDataset, TEmpImportarAhorrosGridShow_DevEx );
                         2 : FVerificaASCII := ZBaseGridShow_DevEx.GridShow( cdsDataset, TEmpImportarPCarAboGridShow_DevEx );
                         3 : FVerificaASCII := ZBaseGridShow_DevEx.GridShow( cdsDataset, TEmpImportarACarAboGridShow_DevEx );
                    end;
                    ZAsciiTools.FiltraASCII( cdsDataSet, False );
               end;
          end
          else
              FVerificaASCII := True;
     end;
end;

function TWizEmpImportarAhorrosPrestamos_DevEx.GetArchivo: String;
begin
     Result := Archivo.Text;
end;

function TWizEmpImportarAhorrosPrestamos_DevEx.LeerArchivo: Boolean;
begin
     PanelAnimation.Visible := TRUE;
     Application.ProcessMessages;
     CargaParametros;
     CargaListaVerificaASCII;
     Result := FVerificaASCII;
     PanelAnimation.Visible := FALSE;
     if Result then
        ArchivoVerif := GetArchivo
     else
        ArchivoVerif := VACIO;
end;
procedure TWizEmpImportarAhorrosPrestamos_DevEx.ArchivoSeekClick(
  Sender: TObject);
begin
     inherited;
     Archivo.Text := ZetaClientTools.AbreDialogo( OpenDialog, Archivo.Text, 'dat' );
end;

procedure TWizEmpImportarAhorrosPrestamos_DevEx.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     ParametrosControl := nil;        //DevEx
     with Wizard do
     begin
          if Adelante then
          begin
               if EsPaginaActual( Parametros ) then
               begin
                    if ZetaCommonTools.StrVacio( GetArchivo ) then
                       CanMove := Error( 'Debe Especificarse un Archivo a Importar', Parametros )
                    else
                       if not FileExists( GetArchivo ) then
                          CanMove := Error( 'El Archivo ' + GetArchivo + ' No Existe', Parametros )
                       else
                       begin
                            if ( ArchivoVerif <> GetArchivo ) or
                               ( ZetaDialogo.ZConfirm( Caption, 'El Archivo ' + GetArchivo + ' Ya Fu� Verificado !' + CR_LF +
                                                       'Volver a Verificar ?', 0, mbYes ) ) then
                               CanMove := LeerArchivo
                            else
                               CanMove := TRUE;
                       end;
               end;
          end;
     end;
end;

function TWizEmpImportarAhorrosPrestamos_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.ImportarAhorrosPrestamos( ParameterList );
end;

procedure TWizEmpImportarAhorrosPrestamos_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption :=  'Al ejecutar el proceso se generar�n movimientos en la bit�cora de registros importados.';
end;

//DevEx     //Agregado para enfocar un control
procedure TWizEmpImportarAhorrosPrestamos_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := Archivo
     else
         ParametrosControl := nil;
     inherited;
end;


end.
