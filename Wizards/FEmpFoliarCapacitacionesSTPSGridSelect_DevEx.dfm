inherited EmpFoliarCapacitacionesSTPSGridSelect_DevEx: TEmpFoliarCapacitacionesSTPSGridSelect_DevEx
  Left = 262
  Top = 384
  Width = 575
  Caption = 'Seleccione Los Empleados Deseados'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 559
    inherited OK_DevEx: TcxButton
      Left = 390
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 468
    end
  end
  inherited PanelSuperior: TPanel
    Width = 559
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 559
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
      end
      object CU_CODIGO: TcxGridDBColumn
        Caption = 'Curso'
        DataBinding.FieldName = 'CU_CODIGO'
      end
      object KC_FEC_TOM: TcxGridDBColumn
        Caption = 'Inicio'
        DataBinding.FieldName = 'KC_FEC_TOM'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
      end
      object KC_REVISIO: TcxGridDBColumn
        Caption = 'Revisi'#243'n'
        DataBinding.FieldName = 'KC_REVISIO'
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DataSource: TDataSource
    Left = 368
    Top = 8
  end
end
