inherited WizEmpCancelarProgCursoGlobal_DevEx: TWizEmpCancelarProgCursoGlobal_DevEx
  Left = 536
  Top = 228
  Caption = 'Cancelar Curso Programado'
  ClientHeight = 442
  ClientWidth = 434
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 434
    Height = 442
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        ' Proceso que permite eliminar los cursos que se hayan programado' +
        ' de manera global.'
      Header.Title = 'Cancelar curso programado global'
      object Label2: TLabel
        Left = 42
        Top = 51
        Width = 33
        Height = 13
        Caption = '&Fecha:'
        FocusControl = dFecha
        Transparent = True
      end
      object Label1: TLabel
        Left = 45
        Top = 27
        Width = 30
        Height = 13
        Caption = 'C&urso:'
        FocusControl = CodigoCurso
        Transparent = True
      end
      object dFecha: TZetaFecha
        Left = 78
        Top = 49
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '24/feb/09'
        Valor = 39868.000000000000000000
      end
      object CodigoCurso: TZetaKeyLookup_DevEx
        Left = 78
        Top = 25
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsCursos
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 207
        Width = 412
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 412
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 344
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 14
      end
      inherited sFiltroLBL: TLabel
        Left = 39
      end
      inherited Seleccionar: TcxButton
        Left = 142
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 66
      end
      inherited sFiltro: TcxMemo
        Left = 66
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 66
      end
      inherited bAjusteISR: TcxButton
        Left = 380
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
  end
end
