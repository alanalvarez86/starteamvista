inherited WizAsistAutorizarHoras_DevEx: TWizAsistAutorizarHoras_DevEx
  Left = 1515
  Top = 182
  Caption = 'Autorizaci'#243'n de Extras y Permisos Globales'
  ClientHeight = 566
  ClientWidth = 448
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 448
    Height = 566
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EBC00000EBC0195BC
      72490000040C494441545847C557494C144114E56094834671815150D091805B
      C025D1831A0F1E3C70F0608C070F9298E8C1188D0AA30E16030466BA67C47D89
      07BD7920D1840B070E1A4D34C41817446318511C05590C21A88808ED7F65D558
      74578F4B067CC9CB7455FFFAAFEAFFAADF35299665FD576A3B279249817579F5
      18B6FAD2B3DFB3B9FEDEEA9C8681485EAC2FE46DEA0864996D4767AFB5DB2605
      AAC3F7E59981918B2B47E9D9D2B1AB2ABBFEDEFE8C547A4EFE043E06BD4D1001
      074F2FFBF2B166E1EDCE40A64991A81F38B9A457BEFB7432BF3F5A32230D6392
      02387A579E1580F3D14BAB2C12BCF174DFE4F82A25DF96794A86CF178CC08E6C
      3059E1E137608CA51ACCD84C3C1C0998574D16F29BCCDC847EBC6F2EF164CBB0
      F7542FBC01C76F8EA71751CEA7E15965FBF13945F4CB23F1D69F5ECC051221C2
      82EBCC80F1221C302D0799E1834D8C65FAE110617FB26F0A5F39ADB0F1CBA9A5
      FDD1D299F968AB14ABE751E0226EA095EE25A16187B0240B6D855D77B5B7010E
      917355A8AB72FEB5EF170A47DAFC9EED6A7FAC2CE320FDF2BDC0857408B3703E
      890C3A4455920D6CFB234B6370D8513ECF5485C0767FC6CE914BABADE7A59E74
      D9D7E69B89E3C8D3C0C574A0B0DFD58AAA64E1F5B0ED09E6CA90D64B111BADA8
      6F8E57B62902C5E8FB7A66F91017B3A396D5CED00ADA489B7227ECA315795839
      42DA25456CB45A7C59F10974572DC046B5A840B570413BB0E375824E1A75B0BF
      5BB6311ED2F61373F7482185D663DF623E013AFF5E7962A85A9A5CD00EDADDBB
      F4820E0ED2643D70DC5593CB37E2B7F385DF5FFA7370D4E21378E95FC4DBAD47
      A6675369EE801DFDF63EDB9F3A49488E458885D668C45C68D4C179C3A182D4CF
      74ECE01CC5A8BBC6DBD87E22B338EA9BE5A58D584415F18A2C427432465F95A6
      ADC0382D5060C8B9FBF1B391A27000CEEE1CDB90D61DCA7B081137D23EE995E2
      A02B50F174626E44CD904E9F94AF29FE10CC6B1AA85DC623327476C550BF99DB
      82EF02C22EED40579C63E7A692E3D77621170E236D6268F220F642C24950BD18
      A0EFC2363124F9E091A830CE92509F4D7CD80C9837C765E56E88B0C862AC96D7
      09169E2DBAC70F2454088AA62BF05504514145D7BF838EE0241C2B0A79CFCF50
      1BD7C5AB94200B66E11E20EE02F12242B6ADB0459A3056DE13FE1AC837E5F5BE
      926730265EF314509BBE90C62DD145DF8D9A1C9B3DD16886AD30F933604570EC
      74665A10116629E18A70252E29A229EF0C8E31C44E444C98258610AFD338E1A4
      90EF10A624686E53739D681CF1819A2A57D0CD66B766F02F56842F0A5312341B
      29CF87F10CE79AE33996F461E303DD800D43869D8E8163F908B64ABE5F431CA9
      506CB4C406E5426EF8EDEA0569D55B1009A57D406D27A4B8BA694139BC4E4628
      B7E346750F39A0FBC33891D4764E1CAD941F494657DDDCD9B26B000000004945
      4E44AE426082}
    inherited Parametros: TdxWizardControlPage
      Header.Description = 'Registros de autorizaciones de Horas Extras y Permisos.'
      Header.Title = 'Autorizar Extras y Permisos Globales'
      object dInicialLBL: TLabel
        Left = 20
        Top = 9
        Width = 63
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha &Inicial:'
        Color = clWhite
        FocusControl = FechaInicial
        ParentColor = False
      end
      object dFinalLBL: TLabel
        Left = 217
        Top = 9
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha &Final:'
        Color = clWhite
        FocusControl = FechaFinal
        ParentColor = False
      end
      object FechaInicial: TZetaFecha
        Left = 92
        Top = 4
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '02-Jan-98'
        Valor = 35797.000000000000000000
      end
      object FechaFinal: TZetaFecha
        Left = 282
        Top = 4
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '02-Jan-98'
        Valor = 35797.000000000000000000
      end
      object Cancelar: TGroupBox
        Left = 3
        Top = 335
        Width = 423
        Height = 92
        Caption = ' Cancelar '
        Color = clWhite
        ParentColor = False
        TabOrder = 4
        object lTemporales: TCheckBox
          Left = 11
          Top = 28
          Width = 119
          Height = 17
          Caption = 'Horarios Tem&porales'
          TabOrder = 0
        end
        object GroupBox1: TGroupBox
          Left = 136
          Top = 8
          Width = 277
          Height = 70
          Caption = ' Autori&zaciones de '
          TabOrder = 1
          object lAutExtras: TCheckBox
            Left = 8
            Top = 15
            Width = 120
            Height = 17
            Caption = 'Horas Extras'
            TabOrder = 0
          end
          object lAutDescanso: TCheckBox
            Left = 8
            Top = 31
            Width = 120
            Height = 17
            Caption = 'Descanso Trabajado'
            TabOrder = 1
          end
          object lAutConGoce: TCheckBox
            Left = 135
            Top = 15
            Width = 120
            Height = 17
            Caption = 'Permisos Con Goce'
            TabOrder = 2
          end
          object lAutSinGoce: TCheckBox
            Left = 135
            Top = 31
            Width = 120
            Height = 17
            Caption = 'Permisos Sin Goce'
            TabOrder = 3
          end
          object lAutHorasPrep: TCheckBox
            Left = 8
            Top = 47
            Width = 120
            Height = 17
            Caption = 'Horas Prepagadas'
            TabOrder = 4
          end
        end
      end
      object Cambiar: TGroupBox
        Left = 3
        Top = 26
        Width = 423
        Height = 61
        Caption = ' Cambiar '
        Color = clWhite
        ParentColor = False
        TabOrder = 2
        object lHorario: TCheckBox
          Tag = 1
          Left = 101
          Top = 11
          Width = 55
          Height = 17
          Alignment = taLeftJustify
          Caption = '&Horario:'
          TabOrder = 0
          OnClick = CheckBoxClick
        end
        object lTipoDia: TCheckBox
          Left = 78
          Top = 36
          Width = 78
          Height = 17
          Alignment = taLeftJustify
          Caption = '&Tipo de D'#237'a:'
          TabOrder = 2
          OnClick = CheckBoxClick
        end
        object iTipoDia: TZetaKeyCombo
          Left = 161
          Top = 32
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 3
          ListaFija = lfStatusAusencia
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object Horario: TZetaKeyLookup_DevEx
          Left = 161
          Top = 9
          Width = 233
          Height = 21
          LookupDataset = dmCatalogos.cdsHorarios
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 60
        end
      end
      object Autorizar: TGroupBox
        Left = 3
        Top = 90
        Width = 423
        Height = 243
        Caption = ' Autorizar '
        Color = clWhite
        ParentColor = False
        TabOrder = 3
        object lExtras: TCheckBox
          Tag = 2
          Left = 75
          Top = 12
          Width = 81
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Horas E&xtras:'
          TabOrder = 0
          OnClick = CheckBoxClick
        end
        object lDescanso: TCheckBox
          Tag = 3
          Left = 36
          Top = 34
          Width = 120
          Height = 17
          Alignment = taLeftJustify
          Caption = '&Descanso Trabajado:'
          TabOrder = 3
          OnClick = CheckBoxClick
        end
        object lConGoce: TCheckBox
          Tag = 4
          Left = 46
          Top = 57
          Width = 110
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Permiso Con &Goce:'
          TabOrder = 6
          OnClick = CheckBoxClick
        end
        object lSinGoce: TCheckBox
          Tag = 5
          Left = 50
          Top = 79
          Width = 106
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Permiso Sin G&oce:'
          TabOrder = 9
          OnClick = CheckBoxClick
        end
        object lConGoceE: TCheckBox
          Tag = 6
          Left = 17
          Top = 103
          Width = 139
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Permiso c/Goce E&ntrada:'
          TabOrder = 12
          OnClick = CheckBoxClick
        end
        object lSinGoceE: TCheckBox
          Tag = 7
          Left = 18
          Top = 125
          Width = 138
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Permiso s/Goce Ent&rada:'
          TabOrder = 15
          OnClick = CheckBoxClick
        end
        object rExtras: TZetaNumero
          Left = 161
          Top = 10
          Width = 40
          Height = 21
          Mascara = mnHoras
          TabOrder = 1
          Text = '0.00'
        end
        object rDescanso: TZetaNumero
          Left = 161
          Top = 32
          Width = 40
          Height = 21
          Mascara = mnHoras
          TabOrder = 4
          Text = '0.00'
        end
        object rConGoce: TZetaNumero
          Left = 161
          Top = 54
          Width = 40
          Height = 21
          Mascara = mnHoras
          TabOrder = 7
          Text = '0.00'
        end
        object rSinGoce: TZetaNumero
          Left = 161
          Top = 77
          Width = 40
          Height = 21
          Mascara = mnHoras
          TabOrder = 10
          Text = '0.00'
        end
        object rConGoceE: TZetaNumero
          Left = 161
          Top = 101
          Width = 40
          Height = 21
          Mascara = mnHoras
          TabOrder = 13
          Text = '0.00'
        end
        object rSinGoceE: TZetaNumero
          Left = 161
          Top = 123
          Width = 40
          Height = 21
          Mascara = mnHoras
          TabOrder = 16
          Text = '0.00'
        end
        object D_ANTERIOR: TcxRadioGroup
          Left = 2
          Top = 186
          Align = alBottom
          Caption = ' En caso de Existir &Autorizaci'#243'n '
          Ctl3D = True
          ParentCtl3D = False
          Properties.Columns = 3
          Properties.Items = <
            item
              Caption = 'Dejar Anterior'
            end
            item
              Caption = 'Sumar'
            end
            item
              Caption = 'Sustituir'
            end>
          TabOrder = 24
          Height = 55
          Width = 419
        end
        object mExtras: TZetaKeyLookup_DevEx
          Left = 208
          Top = 10
          Width = 210
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 2
          TabStop = True
          WidthLlave = 40
        end
        object mDescanso: TZetaKeyLookup_DevEx
          Left = 208
          Top = 32
          Width = 210
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 5
          TabStop = True
          WidthLlave = 40
        end
        object mConGoce: TZetaKeyLookup_DevEx
          Left = 208
          Top = 54
          Width = 210
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 8
          TabStop = True
          WidthLlave = 40
        end
        object mSinGoce: TZetaKeyLookup_DevEx
          Left = 208
          Top = 77
          Width = 210
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 11
          TabStop = True
          WidthLlave = 40
        end
        object mConGoceE: TZetaKeyLookup_DevEx
          Left = 208
          Top = 100
          Width = 210
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 14
          TabStop = True
          WidthLlave = 40
        end
        object mSinGoceE: TZetaKeyLookup_DevEx
          Left = 208
          Top = 123
          Width = 210
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 17
          TabStop = True
          WidthLlave = 40
        end
        object rPreFueraJor: TZetaNumero
          Left = 161
          Top = 145
          Width = 40
          Height = 21
          Mascara = mnHoras
          TabOrder = 19
          Text = '0.00'
        end
        object mPreFueraJor: TZetaKeyLookup_DevEx
          Left = 208
          Top = 145
          Width = 210
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 20
          TabStop = True
          WidthLlave = 40
        end
        object rPreDentroJor: TZetaNumero
          Left = 161
          Top = 167
          Width = 40
          Height = 21
          Mascara = mnHoras
          TabOrder = 22
          Text = '0.00'
        end
        object mPreDentroJor: TZetaKeyLookup_DevEx
          Left = 208
          Top = 167
          Width = 210
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 23
          TabStop = True
          WidthLlave = 40
        end
        object lPreFueraJor: TCheckBox
          Tag = 8
          Left = 6
          Top = 147
          Width = 150
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Prepagadas &Fuera Jornada:'
          TabOrder = 18
          OnClick = CheckBoxClick
        end
        object lPreDentroJor: TCheckBox
          Tag = 9
          Left = 1
          Top = 169
          Width = 155
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Prepagadas Dentro &Jornada:'
          TabOrder = 21
          OnClick = CheckBoxClick
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 331
        Width = 426
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 426
        inherited Advertencia: TcxLabel
          Caption = 
            'Se registrar'#225'n las autorizaciones y cambios en el rango de fecha' +
            's a los empleados seleccionados.'
          Style.IsFontAssigned = True
          Width = 358
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 18
        Top = 181
      end
      inherited sFiltroLBL: TLabel
        Left = 43
        Top = 204
      end
      inherited Seleccionar: TcxButton
        Left = 146
        Top = 298
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 70
        Top = 178
      end
      inherited sFiltro: TcxMemo
        Left = 70
        Top = 204
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 70
        Top = 52
      end
      inherited bAjusteISR: TcxButton
        Left = 384
        Top = 204
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 36
    Top = 265
  end
end
