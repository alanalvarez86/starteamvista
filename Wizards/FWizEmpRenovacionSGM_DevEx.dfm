inherited WizEmpRenovacionSGM_DevEx: TWizEmpRenovacionSGM_DevEx
  Left = 340
  Top = 79
  Caption = 'Renovar Seguro de Gastos M'#233'dicos'
  ClientHeight = 615
  ClientWidth = 453
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 453
    Height = 615
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Este proceso renueva las p'#243'lizas de seguro de gastos m'#233'dicos par' +
        'a titulares y dependientes.'
      Header.Title = 'Renovar seguro de gastos m'#233'dicos'
      object PanelSuperior: TPanel
        Left = 0
        Top = 0
        Width = 431
        Height = 153
        Align = alTop
        Color = clWhite
        TabOrder = 0
        object LblCodigo: TLabel
          Left = 78
          Top = 5
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = '&C'#243'digo:'
        end
        object LblPoliza: TLabel
          Left = 83
          Top = 28
          Width = 31
          Height = 13
          Alignment = taRightJustify
          Caption = 'P'#243'liza:'
        end
        object LblReferencia: TLabel
          Left = 24
          Top = 50
          Width = 90
          Height = 13
          Alignment = taRightJustify
          Caption = '&Nueva Referencia:'
          FocusControl = ReferenciaDropDown
        end
        object LblVigencia: TLabel
          Left = 70
          Top = 72
          Width = 44
          Height = 13
          Alignment = taRightJustify
          Caption = 'Vigencia:'
        end
        object LblCondiciones: TLabel
          Left = 53
          Top = 92
          Width = 61
          Height = 13
          Alignment = taRightJustify
          Caption = 'Condiciones:'
        end
        object Poliza: TZetaTextBox
          Left = 118
          Top = 26
          Width = 121
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object LblVigenciaA: TLabel
          Left = 241
          Top = 72
          Width = 11
          Height = 13
          Alignment = taRightJustify
          Caption = 'al:'
        end
        object VigenciaInicio: TZetaTextBox
          Left = 118
          Top = 70
          Width = 121
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object VigenciaFin: TZetaTextBox
          Left = 254
          Top = 70
          Width = 121
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object CodigoPolizaLookup: TZetaKeyLookup_DevEx
          Left = 118
          Top = 2
          Width = 300
          Height = 21
          Hint = 'Seleccione un C'#243'digo de P'#243'liza'
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          ShowHint = True
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
          OnExit = CodigoPolizaLookupExit
          OnValidKey = CodigoPolizaLookupValidKey
        end
        object ReferenciaDropDown: TZetaKeyCombo
          Left = 118
          Top = 46
          Width = 121
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          Enabled = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 1
          OnChange = ReferenciaDropDownChange
          ListaFija = lfNinguna
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object Memo1: TcxMemo
          Left = 118
          Top = 90
          Enabled = False
          TabOrder = 2
          Height = 57
          Width = 295
        end
      end
      object PolizaActualGroup: TGroupBox
        Left = 0
        Top = 153
        Width = 431
        Height = 322
        Align = alClient
        Caption = 'P'#243'liza actual:'
        Color = clWhite
        ParentColor = False
        TabOrder = 1
        object LblReferenciaActual: TLabel
          Left = 29
          Top = 20
          Width = 88
          Height = 13
          Alignment = taRightJustify
          Caption = 'R&eferencia Actual:'
          FocusControl = ReferenciaActualDropDown
        end
        object LblRenovarDependientes: TLabel
          Left = 24
          Top = 43
          Width = 93
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de Asegurado:'
        end
        object LblObservaciones: TLabel
          Left = 43
          Top = 246
          Width = 74
          Height = 13
          Alignment = taRightJustify
          Caption = '&Observaciones:'
          FocusControl = Observaciones
        end
        object Label1: TLabel
          Left = 52
          Top = 61
          Width = 65
          Height = 13
          Alignment = taRightJustify
          Caption = 'Aplicar Tabla:'
          FocusControl = chkAplicaTabla
        end
        object lblTablaAmort: TLabel
          Left = 86
          Top = 107
          Width = 30
          Height = 13
          Alignment = taRightJustify
          Caption = '&Tabla:'
          FocusControl = lookupTablasAmortizacion
        end
        object lblLstDiferencia: TLabel
          Left = 2
          Top = 223
          Width = 115
          Height = 13
          Alignment = taRightJustify
          Caption = 'Cargar Diferencia Costo:'
        end
        object lblFechaRef: TLabel
          Left = 31
          Top = 84
          Width = 86
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha Ref. Tabla:'
        end
        object ReferenciaActualDropDown: TZetaKeyCombo
          Left = 120
          Top = 16
          Width = 121
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 0
          ListaFija = lfNinguna
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object Observaciones: TcxMemo
          Left = 120
          Top = 244
          Properties.ScrollBars = ssVertical
          TabOrder = 8
          Height = 69
          Width = 297
        end
        object chkAplicaTabla: TCheckBox
          Left = 120
          Top = 61
          Width = 14
          Height = 17
          Checked = True
          State = cbChecked
          TabOrder = 2
          OnClick = chkAplicaTablaClick
        end
        object lookupTablasAmortizacion: TZetaKeyLookup_DevEx
          Left = 120
          Top = 104
          Width = 298
          Height = 21
          Hint = 'Seleccione un C'#243'digo de P'#243'liza'
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          ShowHint = True
          TabOrder = 4
          TabStop = True
          WidthLlave = 60
        end
        object grpPorcentaje: TGroupBox
          Left = 120
          Top = 126
          Width = 297
          Height = 45
          Caption = 'Porcentaje de Aumento'
          TabOrder = 5
          object lblTitulares: TLabel
            Left = 10
            Top = 22
            Width = 43
            Height = 13
            Alignment = taRightJustify
            Caption = 'Titulares:'
            FocusControl = chkAplicaTabla
          end
          object lblDependientes: TLabel
            Left = 147
            Top = 22
            Width = 69
            Height = 13
            Alignment = taRightJustify
            Caption = 'Dependientes:'
            FocusControl = chkAplicaTabla
          end
          object txtPorcTitulares: TZetaNumero
            Left = 56
            Top = 18
            Width = 57
            Height = 21
            Mascara = mnTasa
            TabOrder = 0
            Text = '0.0 %'
          end
          object txtPorcDependiente: TZetaNumero
            Left = 219
            Top = 18
            Width = 57
            Height = 21
            Mascara = mnTasa
            TabOrder = 1
            Text = '0.0 %'
          end
        end
        object lstRenovar: TZetaKeyCombo
          Left = 120
          Top = 39
          Width = 121
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ItemIndex = 2
          ParentCtl3D = False
          TabOrder = 1
          Text = 'Todos'
          Items.Strings = (
            'Empleados'
            'Parientes'
            'Todos')
          ListaFija = lfNinguna
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object gbTopes: TGroupBox
          Left = 120
          Top = 172
          Width = 297
          Height = 45
          Caption = ' Topes de Costo '
          TabOrder = 6
          object lblTopeEmpresa: TLabel
            Left = 9
            Top = 20
            Width = 44
            Height = 13
            Alignment = taRightJustify
            Caption = 'Empresa:'
            FocusControl = chkAplicaTabla
          end
          object lblTopeEmpleado: TLabel
            Left = 145
            Top = 21
            Width = 50
            Height = 13
            Alignment = taRightJustify
            Caption = 'Empleado:'
            FocusControl = chkAplicaTabla
          end
          object TopeEmpresa: TZetaNumero
            Left = 56
            Top = 18
            Width = 81
            Height = 21
            Mascara = mnPesos
            TabOrder = 0
            Text = '0.00'
            OnExit = TopeEmpresaExit
          end
          object TopeEmpleado: TZetaNumero
            Left = 198
            Top = 18
            Width = 81
            Height = 21
            Mascara = mnPesos
            TabOrder = 1
            Text = '0.00'
            OnExit = TopeEmpleadoExit
          end
        end
        object lstDiferencia: TZetaKeyCombo
          Left = 120
          Top = 219
          Width = 121
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ItemIndex = 0
          ParentCtl3D = False
          TabOrder = 7
          Text = 'Empresa'
          Items.Strings = (
            'Empresa'
            'Empleados')
          ListaFija = lfNinguna
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object FechaRef: TZetaFecha
          Left = 120
          Top = 79
          Width = 119
          Height = 22
          Cursor = crArrow
          TabOrder = 3
          Text = '23-Jul-12'
          Valor = 41113.000000000000000000
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 380
        Width = 431
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 431
        inherited Advertencia: TcxLabel
          Caption = 
            'Al aplicar este proceso se renovar'#225'n las polizas para asegurados' +
            ' indicados.'
          Style.IsFontAssigned = True
          Width = 363
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 18
        Top = 225
      end
      inherited sFiltroLBL: TLabel
        Left = 43
        Top = 248
      end
      inherited Seleccionar: TcxButton
        Left = 146
        Top = 342
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 70
        Top = 222
      end
      inherited sFiltro: TcxMemo
        Left = 70
        Top = 248
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 70
        Top = 92
      end
      inherited bAjusteISR: TcxButton
        Left = 385
        Top = 249
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Top = 492
  end
end
