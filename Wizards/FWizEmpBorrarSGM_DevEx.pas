unit FWizEmpBorrarSGM_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ZetaEdit, StdCtrls,
  Buttons, ZetaWizard, ExtCtrls, ZetaKeyCombo, ZetaDBTextBox, Mask,
  ZetaFecha, ZCXBaseWizardFiltro, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
  cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl, dxSkinsCore;

type
  TWizEmpBorrarSGM_DevEx = class(TBaseCXWizardFiltro)
    PanelSuperior: TPanel;
    LblCodigo: TLabel;
    LblPoliza: TLabel;
    LblReferencia: TLabel;
    LblVigencia: TLabel;
    LblCondiciones: TLabel;
    Poliza: TZetaTextBox;
    LblVigenciaA: TLabel;
    VigenciaInicio: TZetaTextBox;
    VigenciaFin: TZetaTextBox;
    CodigoPolizaLookup: TZetaKeyLookup_DevEx;
    ReferenciaDropDown: TZetaKeyCombo;
    CondicionesMemo: TcxMemo;
    PolizaActualGroup: TGroupBox;
    LblReferenciaActual: TLabel;
    LblRenovarDependientes: TLabel;
    LblFechaDeRegistro: TLabel;
    ReferenciaActualDropDown: TZetaKeyCombo;
    LblPolizasVigentes: TLabel;
    FechaDeRegistroCheckBox: TCheckBox;
    PolizasVigentesCheckBox: TCheckBox;
    TipoAsegurado: TZetaKeyCombo;
    FechaPolizasVigentes: TZetaFecha;
    FechaDeRegistro: TZetaFecha;
    FechaRegistro: TZetaFecha;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure CodigoPolizaLookupExit(Sender: TObject);
    procedure CodigoPolizaLookupValidKey(Sender: TObject);
    procedure ReferenciaDropDownChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
  private
    procedure LimpiaComponentes;
  protected
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizEmpBorrarSGM_DevEx: TWizEmpBorrarSGM_DevEx;

implementation

uses
    ZetaCommonClasses,
    ZetaCommonTools,
    dCatalogos,
    DProcesos,
    ZBaseSelectGrid_DevEx,
    FEmpSGMGridSelect_DevEx,
    ZetaClientDataSet,
    DateUtils;

{$R *.dfm}

{ TWizEmpBorrarSGM }

procedure TWizEmpBorrarSGM_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_BORRAR_SGM;
     ParametrosControl := CodigoPolizaLookup;
     WizardControl.ActivePage := Parametros;
end;

procedure TWizEmpBorrarSGM_DevEx.FormShow(Sender: TObject);
begin
     with dmCatalogos do
     begin
          cdsSegGastosMed.Conectar;
          cdsVigenciasSGM.Conectar;
          CodigoPolizaLookup.LookupDataset := cdsSegGastosMed;
          ReferenciaActualDropDown.ItemIndex := 0;
          FechaRegistro.Valor := Date;
          FechaPolizasVigentes.Valor := Date;
     end;
end;

procedure TWizEmpBorrarSGM_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   sMensaje : String;
begin
ParametrosControl := nil;
     inherited;
     sMensaje := VACIO;
     if Wizard.Adelante then
     begin
          //PARAMETROS
          if WizardControl.ActivePage = Parametros then
          begin
               if StrVacio( Poliza.Caption ) then
                    CanMove := Error(' � No hay P�liza Seleccionada ! ', CodigoPolizaLookup )
               else
                   if StrVacio(VigenciaInicio.Caption) or StrVacio(VigenciaFin.Caption) then
                      CanMove := Error(' � No hay Referencia Seleccionada ! ', CodigoPolizaLookup );
          end;
     end;
end;

procedure TWizEmpBorrarSGM_DevEx.LimpiaComponentes;
begin
     with ReferenciaDropDown do
     begin
          Items.Clear;
          Enabled := False;
     end;
     VigenciaInicio.Caption := VACIO;
     VigenciaFin.Caption := VACIO;
     CondicionesMemo.Lines.Clear;
end;

procedure TWizEmpBorrarSGM_DevEx.CodigoPolizaLookupValidKey(Sender: TObject);
     procedure LlenaCombo;
     begin
          LimpiaComponentes;
          with dmCatalogos.cdsVigenciasSGM do
          begin
               Filter := Format( 'PM_CODIGO = %s', [ EntreComillas( dmCatalogos.cdsSegGastosMed.FieldByName('PM_CODIGO').AsString ) ] );
               Filtered := True;
               ReferenciaDropDown.Enabled := True;
               ReferenciaDropDown.Items.Clear;
               First;
               while ( not eof ) do
               begin
                    ReferenciaDropDown.Items.Insert(0, FieldByName('PV_REFEREN').AsString);
                    Next;
               end;
          end;
     end;
begin
     inherited;
     with dmCatalogos do
     begin
          cdsVigenciasSGM.Filtered := False;
          if StrLleno( CodigoPolizaLookup.Llave ) then
          begin
               Poliza.Caption := cdsSegGastosMed.FieldByName('PM_NUMERO').AsString;
               if cdsVigenciasSGM.Locate( 'PM_CODIGO', cdsSegGastosMed.FieldByName('PM_CODIGO').AsString, [] ) then
                    LlenaCombo()
               else
               begin
                    Error( '� No Hay Referencias en la P�liza Seleccionada !', CodigoPolizaLookup );
                    LimpiaComponentes;
                    FocusControl(CodigoPolizaLookup);
               end;
          end;
     end;
end;

procedure TWizEmpBorrarSGM_DevEx.ReferenciaDropDownChange(Sender: TObject);
var
   sReferenciaSeleccionada : String;
begin
     inherited;
     sReferenciaSeleccionada := ReferenciaDropDown.Items[ReferenciaDropDown.ItemIndex];
     with dmCatalogos.cdsVigenciasSGM, ReferenciaDropDown do
          if ( (ItemIndex >= 0) and
             (dmCatalogos.cdsVigenciasSGM.Locate( 'PV_REFEREN',  sReferenciaSeleccionada, [] ) )) then
          begin
               VigenciaInicio.Caption := FieldByName( 'PV_FEC_INI' ).AsString;
               VigenciaFin.Caption := FieldByName( 'PV_FEC_FIN' ).AsString;
               CondicionesMemo.Text := FieldByName( 'PV_CONDIC' ).AsString;
          end;
end;

procedure TWizEmpBorrarSGM_DevEx.CodigoPolizaLookupExit(Sender: TObject);
begin
     inherited;
     If ( Trim( CodigoPolizaLookup.Llave ) = VACIO ) then
          LimpiaComponentes;
end;


procedure TWizEmpBorrarSGM_DevEx.CargaParametros;
begin
     inherited;
     with ParameterList, dmCatalogos do
     begin
          AddString( 'PM_CODIGO', CodigoPolizaLookup.Llave );
          AddString( 'PM_DESCRIP', CodigoPolizaLookup.Descripcion );
          AddString( 'Numero', Poliza.Caption );

          AddString( 'PV_REFEREN', ReferenciaDropDown.Text );
          AddDate( 'FechaInicio', cdsVigenciasSGM.FieldByName( 'PV_FEC_INI' ).AsDateTime ); //Nuevas Fechas
          AddDate( 'FechaFin', cdsVigenciasSGM.FieldByName( 'PV_FEC_FIN' ).AsDateTime ); //Nuevas Fechas

          AddInteger( 'Registros', ReferenciaActualDropDown.ItemIndex ); //DropDown ()
          AddInteger( 'TipoAseg', TipoAsegurado.ItemIndex ); //DropDown ()
          
          AddBoolean( 'FechaRegistroCheck', FechaDeRegistroCheckBox.Checked ); //Activo?
          AddDate( 'FechaRegistro', FechaRegistro.Valor );
          
          AddBoolean( 'FechaVigenciaCheck', PolizasVigentesCheckBox.Checked ); //Activo?
          AddDate( 'FechaVigencia', FechaPolizasVigentes.Valor );
     end;
     with Descripciones do
     begin
          AddString( 'C�digo P�liza', CodigoPolizaLookup.Llave );
          AddString( 'Descripci�n P�liza', CodigoPolizaLookup.Descripcion );
          AddString( 'N�mero', Poliza.Caption );

          AddString( 'Referencia', ReferenciaDropDown.Text );

          AddInteger( 'Registros', ReferenciaActualDropDown.ItemIndex ); //DropDown ()
          AddInteger( 'Tipo Asegurado', TipoAsegurado.ItemIndex ); //DropDown ()

          if(FechaDeRegistroCheckBox.Checked) then
          AddDate( 'Fecha de Registro', FechaRegistro.Valor );
          
          if PolizasVigentesCheckBox.Checked then
          AddDate( 'Vigencia al ', FechaPolizasVigentes.Valor );
     end;
end;

function TWizEmpBorrarSGM_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpSGMGridSelect_DevEx );
end;

procedure TWizEmpBorrarSGM_DevEx.CargaListaVerificacion;
begin
     inherited;
     dmProcesos.BorrarSGMGetLista( ParameterList );
end;

function TWizEmpBorrarSGM_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.BorrarSGM( ParameterList, Verificacion );
end;

procedure TWizEmpBorrarSGM_DevEx.WizardAfterMove(Sender: TObject);
begin
  inherited;
  if Wizard.EsPaginaActual(Parametros) then
  ParametrosControl := CodigoPolizaLookup;
end;

end.
