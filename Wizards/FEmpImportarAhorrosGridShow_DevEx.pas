unit FEmpImportarAhorrosGridShow_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, ExtCtrls, ZBaseGridShow_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxButtons, cxCalendar;

type
  TEmpImportarAhorrosGridShow_DevEx = class(TBaseGridShow_DevEx)
    RENGLON: TcxGridDBColumn;
    CB_CODIGO: TcxGridDBColumn;
    AH_TIPO: TcxGridDBColumn;
    AH_FECHA: TcxGridDBColumn;
    AH_SALDO_I: TcxGridDBColumn;
    NUM_ERROR: TcxGridDBColumn;
    DESC_ERROR: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpImportarAhorrosGridShow_DevEx: TEmpImportarAhorrosGridShow_DevEx;

implementation

{$R *.DFM}

procedure TEmpImportarAhorrosGridShow_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     DataSet.MaskPesos( 'AH_SALDO_I' );
end;

end.
