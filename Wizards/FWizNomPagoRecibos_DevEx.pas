unit FWizNomPagoRecibos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Mask, StdCtrls, ExtCtrls, Buttons, ComCtrls,
     FWizNomBase_DevEx,
     ZetaFecha,
     ZetaKeyCombo,
     ZetaEdit,
     ZetaDBTextBox,
     ZetaWizard, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl;

type
  TWizNomPagoRecibos_DevEx = class(TWizNomBase_DevEx)
    OpenDialog: TOpenDialog;
    Parametros2: TdxWizardControlPage;
    PanelAnimation: TPanel;
    Animate: TAnimate;
    PanelMensaje: TPanel;
    Panel1: TPanel;
    Label5: TLabel;
    Label8: TLabel;
    Label6: TLabel;
    Archivo: TEdit;
    nPagados: TRadioGroup;
    iFormato: TZetaKeyCombo;
    fPagados: TZetaFecha;
    BGuardaArchivo: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure BGuardaArchivoClick(Sender: TObject);
    procedure nPagadosClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure ParametrosChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FVerificaASCII: Boolean;
    FVerificacion: Boolean;
    ArchivoVerif : String;
    function GetArchivo: String;
    function LeerArchivo: Boolean;
    procedure SetVerificacion( const Value: Boolean );
    procedure CargaListaVerificaASCII;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean;override;
    function Verificar: Boolean;override;
    procedure CargaListaVerificacion;override;
    procedure CargaParametros;override;
  public
    { Public declarations }
  end;

var
  WizNomPagoRecibos_DevEx: TWizNomPagoRecibos_DevEx;

implementation

uses DGlobal,
     DCliente,
     DProcesos,
     ZGlobalTress,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaCommonLists,
     ZAsciiTools,
     ZetaDialogo,
     ZBaseSelectGrid_DevEx,
     ZBaseGridShow_DevEx,
     FNomImportarPagoRecibosGridShow_DevEx,
     FNomImportarPagoRecibosGridSelect_DevEx;

{$R *.DFM}

procedure TWizNomPagoRecibos_DevEx.FormCreate(Sender: TObject);
begin
     PanelAnimation.Visible := FALSE;
     Archivo.Tag := K_GLOBAL_DEF_PAGO_RECIBOS;
     inherited;
     if StrVacio( GetArchivo ) then
        Archivo.Text := 'RECIBOS.DAT';
     FPagados.Valor := dmCliente.FechaDefault;
     iFormato.ItemIndex := 0;
     nPagados.ItemIndex := 0;
     nPagadosClick(Sender);
     ArchivoVerif:= VACIO;
     HelpContext := H33421_Importar_pago_de_recibos;

     //DevEx(by am): Es necesario especificar las paginas del wizard.
     Parametros.PageIndex := WizardControl.PageCount - 4;
     Parametros2.PageIndex := WizardControl.PageCount - 3;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 2; //3
     Ejecucion.PageIndex := WizardControl.PageCount - 1;
end;

procedure TWizNomPagoRecibos_DevEx.CargaParametros;
begin
     inherited;
     with Descripciones do
     begin
          Clear;
          AddString( 'Archivo', GetArchivo );
          AddString( 'Tipo de formato', OBtieneElemento( lfFormatoASCII, iFormato.ItemIndex ) );
          AddBoolean( 'Marcar pagados', ( nPagados.ItemIndex = 0 ) );
          if ( nPagados.ItemIndex = 0 ) then
             AddDate( 'Fecha de pago', fPagados.Valor );
          //AddString( 'D�gito de empresa', Global.GetGlobalString( K_GLOBAL_DIGITO_EMPRESA ) );
     end;
     ParamInicial:=0;
     //inherited;

     with ParameterList do
     begin
          if nPagados.ItemIndex = 0 then
          begin
               AddDate( 'Fecha', fPagados.Valor );
               AddBoolean( 'Pagado', TRUE );
          end
          else
          begin
               AddDate( 'Fecha', 0 );
               AddBoolean( 'Pagado', FALSE );
          end;
          AddInteger( 'Formato', iFormato.ItemIndex );
          AddString( 'Archivo', GetArchivo );
          {$ifdef ANTES}
          AddString( 'DigitoEmpresa', Global.GetGlobalString( K_GLOBAL_DIGITO_EMPRESA ) );
          {$else}
          AddString( 'DigitoEmpresa', dmCliente.cdsCompany.FieldByName( 'CM_DIGITO' ).AsString );
          {$endif}
     end;
end;

procedure TWizNomPagoRecibos_DevEx.CargaListaVerificaASCII;
begin
     with dmProcesos do
     begin
          ImportarPagoRecibosGetListaASCII( ParameterList );
          if ( ErrorCount > 0 ) then                 // Hubo Errores
          begin
               ZAsciiTools.FiltraASCII( cdsDataSet, TRUE );
               FVerificaASCII := ZBaseGridShow_DevEx.GridShow( dmProcesos.cdsDataset, TNomImportarPagoRecibosGridShow_DevEx );
               ZAsciiTools.FiltraASCII( cdsDataSet, FALSE );
          end
          else
               FVerificaASCII := TRUE;
     end;
end;

procedure TWizNomPagoRecibos_DevEx.CargaListaVerificacion;
begin
     with dmProcesos do
     begin
          ImportarPagoRecibosGetLista( ParameterList );
     end;
end;

function TWizNomPagoRecibos_DevEx.Verificar: Boolean;
begin
     dmProcesos.cdsDataSet.First;
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TNomImportarPagoRecibosGridSelect_DevEx, FALSE );
end;

procedure TWizNomPagoRecibos_DevEx.SetVerificacion(const Value: Boolean);
begin
     FVerificacion := Value;
end;

function TWizNomPagoRecibos_DevEx.GetArchivo: String;
begin
     Result := Archivo.Text;
end;

function TWizNomPagoRecibos_DevEx.LeerArchivo: Boolean;
begin
     Result := FALSE;
     PanelAnimation.Visible := TRUE;
     Application.ProcessMessages;
     CargaParametros;
     CargaListaVerificaASCII;
     PanelAnimation.Visible := FALSE;
     if FVerificaASCII then
     begin
          CargaListaVerificacion;
          SetVerificacion( Verificar );
          Result := FVerificacion;
     end;
     if Result then
        ArchivoVerif := GetArchivo
     else
        ArchivoVerif := VACIO;
end;

procedure TWizNomPagoRecibos_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     if Wizard.Adelante then
     begin
          if Wizard.EsPaginaActual( Parametros2 ) then
          begin
               if ZetaCommonTools.StrVacio( GetArchivo ) then
                  CanMove := Error( 'Debe Especificarse un Archivo a Importar.', Archivo )
               else
                  if not FileExists( GetArchivo ) then
                     CanMove := Error( 'El Archivo ' + GetArchivo + ' no existe.', Archivo )
                  else
                  begin
                       if ( ArchivoVerif <> GetArchivo ) or
                          ( ZetaDialogo.ZConfirm( Caption, 'El Archivo ' + GetArchivo + ' ya fu� verificado !' + CR_LF +
                                                  'volver a verificar ?', 0, mbYes ) ) then
                          CanMove := LeerArchivo
                       else
                          CanMove := TRUE;
                  end;
          end;
     end;
     inherited;
end;

function TWizNomPagoRecibos_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.ImportarPagoRecibos( ParameterList );
end;

procedure TWizNomPagoRecibos_DevEx.BGuardaArchivoClick(Sender: TObject);
begin
     Archivo.Text := ZetaClientTools.AbreDialogo( OpenDialog, GetArchivo, 'dat' );
{     with OpenDialog do
     begin
          FileName := ExtractFileName( GetArchivo );
          InitialDir := ExtractFilePath( GetArchivo );
          if Execute then
             Archivo.Text := FileName;
     end; }
end;

procedure TWizNomPagoRecibos_DevEx.nPagadosClick(Sender: TObject);
begin
     inherited;
     fPagados.Enabled := ( nPagados.ItemIndex = 0 );
     if ( fPagados.Enabled ) and ( fPagados.Valor = 0 ) then
        fPagados.Valor := Date;
     ParametrosChange( Sender );
end;

procedure TWizNomPagoRecibos_DevEx.ParametrosChange(Sender: TObject);
begin
     inherited;
     ArchivoVerif := VACIO;
end;

procedure TWizNomPagoRecibos_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption :='Al aplicar el proceso se modificar� el estatus de los recibos indicados.'; 
end;

end.
