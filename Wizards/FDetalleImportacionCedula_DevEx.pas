unit FDetalleImportacionCedula_DevEx;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ZBaseDlgModal_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore,
  TressMorado2013, Vcl.Grids, Vcl.DBGrids, ZetaDBGrid, Vcl.StdCtrls,
  Vcl.ImgList, cxButtons, Vcl.ExtCtrls, Data.DB, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDBData, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, ZetaClientDataSet;

type
  TDetalleImportacionCedula_DevEx = class(TZetaDlgModal_DevEx)
    pnlInformacion: TPanel;
    Label1: TLabel;
    DataSource: TDataSource;
    ZetaDBGrid: TZetaCXGrid;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    NFonacot: TcxGridDBColumn;
    ZetaDBGridLevel: TcxGridLevel;
    Nombre: TcxGridDBColumn;
    RFC: TcxGridDBColumn;
    Credito: TcxGridDBColumn;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    DetalleImportacion: TMemo;
    Imagen: TImage;
    cxImageList_ImagenMensaje: TcxImageList;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure ZetaDBGridDBTableViewDataControllerFilterGetValueList(Sender: TcxFilterCriteria; AItemIndex: Integer; AValueList: TcxDataFilterValueList);
    procedure ZetaDBGridDBTableViewDataControllerSortingChanged(
      Sender: TObject);
    procedure ZetaDBGridDBTableViewColumnHeaderClick(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
  private
    { Private declarations }
    iTipoCedula : Integer;
  protected
    AColumn: TcxGridDBColumn;
    procedure ApplyMinWidth;
  public
    { Public declarations }
  end;

var
  DetalleImportacionCedula_DevEx: TDetalleImportacionCedula_DevEx;

implementation

{$R *.DFM}

uses dNomina, ZGridModeTools, ZetaCommonClasses, ZetaCommonLists;


procedure TDetalleImportacionCedula_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     DataSource.Dataset := nil;
     dmNomina.cdsDetalleImportacion.SetDataChange;
end;

procedure TDetalleImportacionCedula_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= TRUE;
     ZetaDBGridDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
end;

procedure TDetalleImportacionCedula_DevEx.ZetaDBGridDBTableViewColumnHeaderClick(Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
inherited;
     self.AColumn := TcxGridDBColumn(AColumn);
end;

procedure TDetalleImportacionCedula_DevEx.ZetaDBGridDBTableViewDataControllerFilterGetValueList(Sender: TcxFilterCriteria; AItemIndex: Integer; AValueList: TcxDataFilterValueList);
var
   AIndex: Integer;
begin
     inherited;
     AIndex := AValueList.FindItemByKind(fviCustom);
     if AIndex <> -1 then
        AValueList.Delete(AIndex);

     ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if ZetaDBGridDBTableView.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( ZetaDBGridDBTableView, AItemIndex, AValueList );
end;

procedure TDetalleImportacionCedula_DevEx.ZetaDBGridDBTableViewDataControllerSortingChanged(  Sender: TObject);
begin
     inherited;
     ZetaDBGrid.OrdenarPor( AColumn , TZetaClientDataset(DataSource.DataSet) );
end;

procedure TDetalleImportacionCedula_DevEx.FormShow(Sender: TObject);
var sDetalle : String;
begin
     inherited;
     with dmNomina do
     begin
          cdsDetalleImportacion.Conectar;
          DataSource.Dataset := cdsDetalleImportacion;
     end;

     sDetalle := dmNomina.DetalleImportacion;

     sDetalle  := stringreplace(sDetalle, 'Nuevos', '     Nuevos', [rfReplaceAll, rfIgnoreCase]);
     sDetalle  := stringreplace(sDetalle, 'Existentes', '     Existentes', [rfReplaceAll, rfIgnoreCase]);
     sDetalle  := stringreplace(sDetalle, 'No asignados', '     No asignados', [rfReplaceAll, rfIgnoreCase]);
     iTipoCedula := ord(etacPorTrabajador);
     if AnsiPos('Total de cr�ditos', sDetalle) > 0 then
     begin
          iTipoCedula := ord(etacPorCredito);
          sDetalle  := stringreplace(sDetalle, 'Total de cr�ditos', CR_LF + ' Total de cr�ditos', [rfReplaceAll, rfIgnoreCase]);
     end;
     sDetalle  := stringreplace(sDetalle, 'Pr�stamos afectados', CR_LF + ' Pr�stamos afectados', [rfReplaceAll, rfIgnoreCase]);
     sDetalle  := stringreplace(sDetalle, 'Reactivados', '     Reactivados', [rfReplaceAll, rfIgnoreCase]);

     DetalleImportacion.Text := sDetalle;

     cxImageList_ImagenMensaje.GetBitmap( 1, Imagen.Picture.Bitmap);

     //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
     //Esconde la caja de agrupamiento
     ZetaDBGridDBTableView.OptionsView.GroupByBox := False;

     //Para que nunca muestre el filterbox inferior
     ZetaDBGridDBTableView.FilterBox.Visible := fvNever;
     //Para que no aparezca el Custom Dialog
     ZetaDBGridDBTableView.FilterBox.CustomizeDialog := False;

     //DevEx: Para que ponga el MinWidth ideal al titulo de las columnas. Posteriormente se aplica el BestFit.
     ApplyMinWidth;
     ZetaDBGridDBTableView.ApplyBestFit();


     Credito.Visible := (iTipoCedula = ord(etacPorCredito));
end;

procedure TDetalleImportacionCedula_DevEx.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =40;
begin
     with ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;
end;

end.
