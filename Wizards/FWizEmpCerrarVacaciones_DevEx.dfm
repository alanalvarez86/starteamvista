inherited WizEmpCerrarVacaciones_DevEx: TWizEmpCerrarVacaciones_DevEx
  Left = 468
  Top = 199
  Caption = 'Cierre Global de Vacaciones'
  ClientHeight = 487
  ClientWidth = 455
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 455
    Height = 487
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Este proceso realiza un '#39'balance'#39' de d'#237'as gozados y pagados a un' +
        ' grupo de empleados, para presentar un saldo de d'#237'as a pagar.'
      Header.Title = 'Cierre global de vacaciones'
      object ObservacionesLBL: TLabel
        Left = 0
        Top = 309
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'Obser&vaciones:'
        Transparent = True
      end
      object PagarGB: TGroupBox
        Left = 0
        Top = 72
        Width = 433
        Height = 72
        Align = alTop
        Caption = ' Cierre de D'#237'as por Pagar  '
        TabOrder = 1
        object PagarSaldo: TRadioButton
          Left = 136
          Top = 19
          Width = 145
          Height = 17
          Caption = 'Sal&do a la Fecha'
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = SetControls
        end
        object PagarFijo: TRadioButton
          Left = 136
          Top = 42
          Width = 97
          Height = 17
          Caption = 'Est&os D'#237'as:'
          TabOrder = 1
          OnClick = SetControls
        end
        object PagarDias: TZetaNumero
          Left = 214
          Top = 40
          Width = 67
          Height = 21
          Mascara = mnDiasFraccion
          TabOrder = 2
          Text = '0.00'
        end
      end
      object PrimaGB: TGroupBox
        Left = 0
        Top = 144
        Width = 433
        Height = 72
        Align = alTop
        Caption = ' Cierre de d'#237'as de Prima Vacacional '
        TabOrder = 2
        object PrimaSaldo: TRadioButton
          Left = 136
          Top = 27
          Width = 145
          Height = 17
          Caption = 'Saldo a &la Fecha'
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = SetControls
        end
        object PrimaFijo: TRadioButton
          Left = 136
          Top = 50
          Width = 97
          Height = 17
          Caption = 'Es&tos D'#237'as:'
          TabOrder = 1
          OnClick = SetControls
        end
        object PrimaDias: TZetaNumero
          Left = 214
          Top = 48
          Width = 67
          Height = 21
          Mascara = mnDiasFraccion
          TabOrder = 2
          Text = '0.00'
        end
      end
      object GozarGB: TGroupBox
        Left = 0
        Top = 216
        Width = 433
        Height = 72
        Align = alTop
        Caption = ' Cierre de D'#237'as por Gozar '
        TabOrder = 3
        object GozarSaldo: TRadioButton
          Left = 136
          Top = 20
          Width = 145
          Height = 17
          Caption = 'Saldo a la Fec&ha'
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = SetControls
        end
        object GozarFijo: TRadioButton
          Left = 136
          Top = 43
          Width = 97
          Height = 17
          Caption = '&Estos D'#237'as:'
          TabOrder = 1
          OnClick = SetControls
        end
        object GozarDias: TZetaNumero
          Left = 214
          Top = 41
          Width = 67
          Height = 21
          Mascara = mnDiasFraccion
          TabOrder = 2
          Text = '0.00'
        end
      end
      object FechaGB: TGroupBox
        Left = 0
        Top = 0
        Width = 433
        Height = 72
        Align = alTop
        Caption = ' Fecha de Cierre '
        TabOrder = 0
        object PorAniversario: TRadioButton
          Left = 136
          Top = 16
          Width = 145
          Height = 17
          Caption = '&Aniversario del Empleado'
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = SetControls
        end
        object PorFecha: TRadioButton
          Left = 136
          Top = 39
          Width = 97
          Height = 17
          Caption = 'A esta &Fecha:'
          TabOrder = 1
          OnClick = SetControls
        end
        object Fecha: TZetaFecha
          Left = 234
          Top = 37
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 2
          Text = '07/nov/00'
          Valor = 36837.000000000000000000
        end
      end
      object Observaciones: TcxMemo
        Left = 80
        Top = 290
        Properties.ScrollBars = ssVertical
        TabOrder = 4
        Height = 57
        Width = 353
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 252
        Width = 433
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 433
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 365
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 22
      end
      inherited sFiltroLBL: TLabel
        Left = 47
      end
      inherited Seleccionar: TcxButton
        Left = 150
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 74
      end
      inherited sFiltro: TcxMemo
        Left = 74
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 74
      end
      inherited bAjusteISR: TcxButton
        Left = 388
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    Top = 369
  end
end
