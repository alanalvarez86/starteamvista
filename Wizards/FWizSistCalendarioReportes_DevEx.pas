unit FWizSistCalendarioReportes_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls, Mask,
     ZetaCommonLists, ZetaFecha, CheckLst, ZcxBaseWizard,
     cxGraphics, cxControls, ZetaClientDataSet, cxLookAndFeels,
     cxLookAndFeelPainters,
     dxSkinsCore, TressMorado2013,
     cxContainer, cxEdit, ZetaCXWizard,
     dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
     dxCustomWizardControl, dxWizardControl, Menus, cxButtons, cxRadioGroup,
     cxCheckListBox, DReportes, cxCheckGroup, dxCheckGroupBox, cxTextEdit,
     ZetaKeyCombo, ZetaNumero, ZetaHora, cxMemo, cxClasses,
     cxShellBrowserDialog, ZetaKeyLookup_DevEx, System.MaskUtils, Data.DB,
     Vcl.DBCtrls, FileCtrl, cxCheckBox, cxDBEdit, FWizReportesBaseFiltro;

type
    TWizSistCalendarioReportes_DevEx = class(TWizReportesBaseFiltro)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    NombreTarea: TDBEdit;
    rdRolesUsuario: TcxRadioButton;
    rdListaEmpleados: TcxRadioButton;
    rgListaEmpleados: TGroupBox;
    lListaEmpleados: TLabel;
    chkGuardarCopia: TdxCheckGroupBox;
    Label8: TLabel;
    DescripcionTarea: TDBMemo;
    ReporteLista: TZetaDBKeyLookup_DevEx;
    Empresa: TZetaDBKeyLookup_DevEx;
    DataSource: TDataSource;
    CA_FOLIO: TDBEdit;
    ArchivoSeek: TcxButton;
    Archivo: TEdit;
    OpenDialog1: TOpenDialog;
    OpenDialog: TcxShellBrowserDialog;
    chkActivo: TcxDBCheckBox;
    Label25: TLabel;
    Reporte: TZetaDBKeyLookup_DevEx;
    cxGroupBox2: TcxGroupBox;
    procedure FormCreate(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure EmpresaValidKey(Sender: TObject);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure rdListaEmpleadosClick(Sender: TObject);
    procedure rdRolesUsuarioClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    sEmpresa : String;
    procedure SetEmpresa;
    procedure RolesReportes;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
    procedure CargarInformacion;
    procedure CargarInformacionXEmpresa;
  end;

var
   WizSistCalendarioReportes_DevEx: TWizSistCalendarioReportes_DevEx;
   StoredListItemIndex: Integer = -1;

implementation

uses ZetaCommonClasses, DCliente, DSistema, FLookupReporte_DevEx,
     ZetaDialogo, ZFuncsCliente, ZetaCommonTools, ZReportConst,
     ZetaClientTools;

{$R *.DFM}

procedure TWizSistCalendarioReportes_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
           if  dmSistema.EnvioProgramadoXEmpresa = true then
           begin
                dmSistema.cdsEnviosProgramadosEmpresa.CancelUpdates;

           end
           else
           begin
                dmSistema.cdsSistTareaCalendario.CancelUpdates;
           end;

end;

procedure TWizSistCalendarioReportes_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     Parametros.PageIndex := 0;
     ResetPaginas;
     Ejecucion.PageIndex := 1;

     Empresa.LookUpDataset := dmSistema.cdsEmpresas;
     sEmpresa := Empresa.Llave;

     Reporte.LookupDataset := dmReportes.cdsLookupReportesEmpresa;
     dmReportes.cdsLookupReportesEmpresa.Conectar;
     ReporteLista.LookUpDataSet := dmReportes.cdsLookupReportesEmpresa;

     with dmSistema do
     begin
          cdsEmpresas.Conectar;
           if  dmSistema.EnvioProgramadoXEmpresa = true then
           begin
                cdsEnviosProgramadosEmpresa.Conectar;
                DataSource.DataSet := cdsEnviosProgramadosEmpresa;
           end
           else
           begin
                cdsSistTareaCalendario.Conectar;
                DataSource.DataSet := cdsSistTareaCalendario;
           end;
     end;

     sEmpresa := Empresa.Llave;
     SetEmpresa;
     if  dmSistema.EnvioProgramadoXEmpresa = true then
     begin
          CargarInformacionXEmpresa;
     end
     else
     begin
          CargarInformacion;
     end;

     SetEmpresa;
end;

procedure TWizSistCalendarioReportes_DevEx.FormShow(Sender: TObject);
const
     K_TITULO_A = 'Registro de Administrador de env�os programados';
     K_TITULO_B = 'Crear env�o programado';
begin
     inherited;
     if dmSistema.TituloProceso = true then
     begin
          Caption := K_TITULO_A;
          Parametros.Header.Title := K_TITULO_A;
     end
     else
     begin
          Caption := K_TITULO_B;
          Parametros.Header.Title := K_TITULO_B;
     end;

     if  dmSistema.EnvioProgramadoXEmpresa = true then
     begin
          Empresa.Enabled := FALSE;
     end
     else
     begin
          Empresa.Enabled := TRUE;
     end;
end;

procedure TWizSistCalendarioReportes_DevEx.ArchivoSeekClick(Sender: TObject);
var
   sDirectory: String;
begin
     inherited;
     with Archivo do
     begin
          sDirectory := Text;
          if SelectDirectory( sDirectory, [ sdAllowCreate ], 0 ) then
             Text := sDirectory;
     end;
end;

procedure TWizSistCalendarioReportes_DevEx.CargaParametros;
var
   sFecha, sHora, sDias, sDiasMes, sSemanaMes, sMeses, sReporte: String;
   dFechaInicio, dFechaEspecial: TDateTime;
   iRecurrencia, iTipoMes: Integer;
begin
     sReporte := Reporte.Llave;
     with Descripciones do
     begin
          AddString('Nombre del env�o', NombreTarea.Text);
          AddString('Descripci�n', DescripcionTarea.Text);
          AddString('Empresa', Empresa.Descripcion );
          AddString('Reporte', sReporte);
     end;

     with ParameterList do
     begin
          if StrLleno (CA_FOLIO.Text) then
             AddInteger('Folio', StrToInt (CA_FOLIO.Text))
          else
              AddInteger('Folio', 0);
          AddString('Nombre', NombreTarea.Text);
          AddString('Descripcion', DescripcionTarea.Text);
          AddString('Empresa', Empresa.Llave);
          AddString('Activo', zBoolToStr (chkActivo.Checked) );

          if rdListaEmpleados.Checked then
             AddInteger('ReporteEmpleados', ReporteLista.Valor)
          else
              AddInteger('ReporteEmpleados', 0);

          AddString('Reportes', sReporte);

          if chkGuardarCopia.CheckBox.Checked then
             AddString('SalidaArchivo', Trim (Archivo.Text))
          else
              AddString('SalidaArchivo', VACIO);

          if chkGuardarCopia.CheckBox.Checked then
             AddInteger('Salida', 1)
          else
              AddInteger('Salida', 0);

          AddString ('Evaluar', K_GLOBAL_SI);

          AddInteger('Usuario', dmCliente.Usuario);

          AddInteger('SuscribirUsuario', 0);

          AddDate( 'FechaModifico', Date() );
     end;

     inherited CargaParametros;
end;

function TWizSistCalendarioReportes_DevEx.EjecutarWizard: Boolean;
var iFolio: Integer;
begin
     Result := dmSistema.AgregarTareaCalendarioReportes(ParameterList);

     if  dmSistema.EnvioProgramadoXEmpresa = true then
     begin
         with dmSistema.cdsEnviosProgramadosEmpresa do
         begin
              iFolio:= FieldByName( 'CA_FOLIO' ).AsInteger;
              Refrescar;

              if iFolio = 0 then
              begin
                   Last;
                   iFolio:= FieldByName( 'CA_FOLIO' ).AsInteger;
              end;

              Locate( 'CA_FOLIO', iFolio, [] );
         end;
     end
     else
     begin
           with dmSistema.cdsSistTareaCalendario do
           begin
                iFolio:= FieldByName( 'CA_FOLIO' ).AsInteger;
                Refrescar;

                if iFolio = 0 then
                begin
                     Last;
                     iFolio:= FieldByName( 'CA_FOLIO' ).AsInteger;
                end;

                Locate( 'CA_FOLIO', iFolio, [] );
           end;
     end;
end;

procedure TWizSistCalendarioReportes_DevEx.EmpresaValidKey(Sender: TObject);
begin
     inherited;
     if StrVacio(sEmpresa) or ( sEmpresa <> Empresa.Llave) then
     begin
          sEmpresa := Empresa.Llave;
          ReporteLista.Llave := VACIO;
          Reporte.Llave := VACIO;
          SetEmpresa;
          dmReportes.cdsLookupReportesEmpresa.Refrescar;
          dmReportes.cdsSuscripReportesLookup.Refrescar;
          Reporte.ResetMemory;
          ReporteLista.ResetMemory;
     end;
end;

procedure TWizSistCalendarioReportes_DevEx.RolesReportes;
begin
     rgListaEmpleados.Enabled := rdListaEmpleados.Checked;
     lListaEmpleados.Enabled := rdListaEmpleados.Checked;
     ReporteLista.Enabled := rdListaEmpleados.Checked;
end;

procedure TWizSistCalendarioReportes_DevEx.rdListaEmpleadosClick(Sender: TObject);
begin
     inherited;
     RolesReportes;
     rdRolesUsuario.TabStop := True;
end;

procedure TWizSistCalendarioReportes_DevEx.rdRolesUsuarioClick(Sender: TObject);
begin
     inherited;
     RolesReportes;
     rdListaEmpleados.TabStop := True;
end;

procedure TWizSistCalendarioReportes_DevEx.WizardAfterMove(Sender: TObject);
begin
     inherited;
     if (WizardControl.ActivePage = Parametros) then
        ActiveControl := NombreTarea;
end;

procedure TWizSistCalendarioReportes_DevEx.WizardAlCancelar(Sender: TObject; var lOk: Boolean);
begin
     inherited;
     if  dmSistema.EnvioProgramadoXEmpresa = true then
     begin
         if ( lOk ) then
            dmSistema.cdsEnviosProgramadosEmpresa.CancelUpdates;
     end
     else
     begin
         if ( lOk ) then
            dmSistema.cdsSistTareaCalendario.CancelUpdates;
     end;
end;

procedure TWizSistCalendarioReportes_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if CanMove then
     begin
          if Wizard.Adelante and Wizard.EsPaginaActual(Parametros) then
          begin
               if StrVacio(NombreTarea.Text) then
                  CanMove := Error('El nombre del env�o no puede ir vac�o.', NombreTarea)
               else
               begin
                    if StrVacio(Empresa.Llave) then
                       CanMove := Error('La empresa no puede ir vac�a.', Empresa)
                    else
                    begin
                         if Reporte.Llave = VACIO then
                            CanMove := Error('No hay ning�n reporte seleccionado.', Reporte)
                         else
                         begin
                              if rdListaEmpleados.Checked and StrVacio(ReporteLista.Llave) then
                                 CanMove := Error('La lista de reportes para empleados esta vac�o.', ReporteLista)
                              else
                              begin
                                   if chkGuardarCopia.CheckBox.Checked and StrVacio(Archivo.Text) then
                                      CanMove := Error('El directorio para guardar copia de reportes esta vac�o.', Archivo)
                                   else
                                   begin
                                        if chkGuardarCopia.CheckBox.Checked and not DirectoryExists (Archivo.Text) then
                                           CanMove := Error('El directorio para guardar copia de reportes no existe.', Archivo)
                                   end;
                              end
                         end
                    end
               end;
          end;
     end;

     if Wizard.Adelante then
     begin
          if CanMove then
          begin
               if Wizard.EsPaginaActual(Parametros) then
               begin
                    ResetPaginas;
                    case cbFrecuencia.ItemIndex of
                         ord(frDiario):
                         begin
                              pDiario.PageVisible := True;
                              pDiario.PageIndex := 1;
                              iNewPage := pDiario.PageIndex;
                         end;
                         ord(frSemanal):
                         begin
                              pSemanal.PageVisible := True;
                              pSemanal.PageIndex := 1;
                              iNewPage := pDiario.PageIndex;
                         end;
                         ord(frMensual):
                         begin
                              pMensual.PageVisible := True;
                              pMensual.PageIndex := 1;
                              iNewPage := pMensual.PageIndex;
                         end;
                         ord(frCadaHora):
                         begin
                              pHora.PageVisible := True;
                              pHora.PageIndex := 1;
                              iNewPage := pHora.PageIndex;
                         end;
                         ord(frEspecial):
                         begin
                              pEspecial.PageVisible := True;
                              pEspecial.PageIndex := 1;
                              iNewPage := pEspecial.PageIndex;
                         end;
                    end
               end;
          end;
     end
end;

procedure TWizSistCalendarioReportes_DevEx.CargarInformacion;
var
   i, iTSalida, iRepEmpleado: integer;
   sLista, sDesc : String;
begin
     Archivo.Text := dmSistema.cdsSistTareaCalendario.FieldByName('CA_FSALIDA').AsString;
     iTSalida := dmSistema.cdsSistTareaCalendario.FieldByName('CA_TSALIDA').AsInteger;
     iRepEmpleado := dmSistema.cdsSistTareaCalendario.FieldByName('CA_REPEMP').AsInteger;

     if dmSistema.cdsSistTareaCalendario.FieldByName('CA_FOLIO').AsInteger > 0 then
     begin
          Reporte.Llave := VACIO;
          Reporte.Valor := dmSistema.cdsSistTareaCalendario.FieldByName('CA_REPORT').AsInteger;

          if iRepEmpleado > 0 then
          begin
               ReporteLista.Llave := VACIO;
               ReporteLista.Valor := iRepEmpleado;
          end;
     end;

     if iTSalida = 1 then
        chkGuardarCopia.CheckBox.Checked := True;

     if iRepEmpleado > 0 then
        rdListaEmpleados.Checked := True;

     if cbFrecuencia.ItemIndex = ord(frSemanal) then
     begin
          sDesc := dmSistema.cdsSistTareaCalendario.FieldByName('CA_DOWS').AsString;
          MarcarCasillas( chksFrecuenciaDias, sDesc);
     end
     else if cbFrecuencia.ItemIndex = ord(frMensual) then
     begin
          if StrToInt (dmSistema.cdsSistTareaCalendario.FieldByName('CA_MES_ON').AsString) = 1 then
             rDiasMes.Checked := True
          else
              rDiasSemana.Checked := True;

          sDesc := dmSistema.cdsSistTareaCalendario.FieldByName('CA_DOWS').AsString;
          MarcarCasillas( chkMDias, sDesc);

          sDesc := dmSistema.cdsSistTareaCalendario.FieldByName('CA_MESWEEK').AsString;
          MarcarCasillas( chkMNumero, sDesc);

          sDesc := dmSistema.cdsSistTareaCalendario.FieldByName('CA_MESES').AsString;
          MarcarCasillas( chkMMeses, sDesc);

          sDesc := dmSistema.cdsSistTareaCalendario.FieldByName('CA_MESDIAS').AsString;
          MarcarCasillas( chkMDiasMes, sDesc);
     end;
end;

procedure TWizSistCalendarioReportes_DevEx.CargarInformacionXEmpresa;
var
   i, iTSalida, iRepEmpleado: integer;
   sLista, sDesc : String;
begin
     Archivo.Text := dmSistema.cdsEnviosProgramadosEmpresa.FieldByName('CA_FSALIDA').AsString;
     iTSalida := dmSistema.cdsEnviosProgramadosEmpresa.FieldByName('CA_TSALIDA').AsInteger;
     iRepEmpleado := dmSistema.cdsEnviosProgramadosEmpresa.FieldByName('CA_REPEMP').AsInteger;

     if iTSalida = 1 then
        chkGuardarCopia.CheckBox.Checked := True;

     if iRepEmpleado > 0 then
        rdListaEmpleados.Checked := True;

     if cbFrecuencia.ItemIndex = ord(frSemanal) then
     begin
          sDesc := dmSistema.cdsEnviosProgramadosEmpresa.FieldByName('CA_DOWS').AsString;
          MarcarCasillas( chksFrecuenciaDias, sDesc);
     end
     else if cbFrecuencia.ItemIndex = ord(frMensual) then
     begin
          if StrToInt (dmSistema.cdsEnviosProgramadosEmpresa.FieldByName('CA_MES_ON').AsString) = 1 then
             rDiasMes.Checked := True
          else
              rDiasSemana.Checked := True;

          sDesc := dmSistema.cdsEnviosProgramadosEmpresa.FieldByName('CA_DOWS').AsString;
          MarcarCasillas( chkMDias, sDesc);

          sDesc := dmSistema.cdsEnviosProgramadosEmpresa.FieldByName('CA_MESWEEK').AsString;
          MarcarCasillas( chkMNumero, sDesc);

          sDesc := dmSistema.cdsEnviosProgramadosEmpresa.FieldByName('CA_MESES').AsString;
          MarcarCasillas( chkMMeses, sDesc);

          sDesc := dmSistema.cdsEnviosProgramadosEmpresa.FieldByName('CA_MESDIAS').AsString;
          MarcarCasillas( chkMDiasMes, sDesc);
     end;
end;

procedure TWizSistCalendarioReportes_DevEx.SetEmpresa;
begin
     dmReportes.EmpresaReportes := dmSistema.BuildEmpresa(Empresa.Llave);
     dmReportes.cdsLookupReportesEmpresa.Conectar;
     dmReportes.cdsSuscripReportesLookup.Conectar;
     Reporte.ResetMemory;
     ReporteLista.ResetMemory;
end;

end.
