unit FEmpBiometricoGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, Grids, DBGrids, ZetaDBGrid,
  Buttons, ExtCtrls, ZBaseSelectGrid_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, cxControls, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons;

type
  TEmpBiometricoGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure CB_GP_CODGetText(Sender: TField; var Text: String; DisplayText: Boolean); // SYNERGY
  public
    { Public declarations }
  end;

var
  EmpBiometricoGridSelect_DevEx: TEmpBiometricoGridSelect_DevEx;

implementation

uses dSistema,
     ZetaCommonTools;

{$R *.dfm}

procedure TEmpBiometricoGridSelect_DevEx.CB_GP_CODGetText(Sender: TField; var Text: String; DisplayText: Boolean); // SYNERGY
begin
     dmSistema.cdsListaGrupos.Conectar;
     Text := dmSistema.cdsListaGrupos.GetDescripcion( Sender.AsString );
     if StrVacio( Text )then
        Text := Sender.AsString;
end;

procedure TEmpBiometricoGridSelect_DevEx.FormShow(Sender: TObject);
begin
     with DataSet do
     begin
          FieldByName( 'CB_GP_COD' ).OnGetText := CB_GP_CODGetText;
     end;
     inherited;

end;

end.
