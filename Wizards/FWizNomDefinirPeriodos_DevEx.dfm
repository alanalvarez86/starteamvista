inherited WizNomDefinirPeriodos_DevEx: TWizNomDefinirPeriodos_DevEx
  Left = 512
  Top = 163
  Caption = 'Definir Periodos'
  ClientHeight = 393
  ClientWidth = 442
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 442
    Height = 393
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Este proceso genera los periodos de n'#243'mina que se utilizar'#225'n par' +
        'a el a'#241'o.'
      Header.Title = 'Definir Periodos'
      inherited GroupBox1: TGroupBox
        Left = 10
        Top = 25
        TabOrder = 1
        Visible = False
      end
      object Panel1: TPanel
        Left = 39
        Top = 39
        Width = 329
        Height = 145
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 0
        object iTipoPeriodoLBL: TLabel
          Left = 70
          Top = 15
          Width = 78
          Height = 13
          Alignment = taRightJustify
          Caption = '&Tipo de N'#243'mina:'
          FocusControl = iTipoPeriodo
        end
        object dReferenciaLBL: TLabel
          Left = 30
          Top = 42
          Width = 118
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha Inicial Periodo #&1:'
          FocusControl = dReferencia
        end
        object iDiasLBL: TLabel
          Left = 74
          Top = 67
          Width = 74
          Height = 13
          Alignment = taRightJustify
          Caption = '&D'#237'as de Fondo:'
          FocusControl = iDias
        end
        object iPerIniLBL: TLabel
          Left = 78
          Top = 92
          Width = 71
          Height = 13
          Alignment = taRightJustify
          Caption = 'Per'#237'odo &Inicial:'
          FocusControl = iPerIni
        end
        object iPerFinLBL: TLabel
          Left = 83
          Top = 118
          Width = 66
          Height = 13
          Alignment = taRightJustify
          Caption = 'Per'#237'odo &Final:'
          FocusControl = iPerFin
        end
        object iTipoPeriodo: TZetaKeyCombo
          Left = 153
          Top = 11
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 0
          ListaFija = lfTipoPeriodoConfidencial
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = True
        end
        object dReferencia: TZetaFecha
          Left = 153
          Top = 37
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 1
          Text = '23-Jan-98'
          Valor = 35818.000000000000000000
        end
        object iDias: TZetaNumero
          Left = 153
          Top = 63
          Width = 44
          Height = 21
          Mascara = mnMinutos
          TabOrder = 2
          Text = '0'
        end
        object iPerIni: TZetaNumero
          Left = 154
          Top = 88
          Width = 44
          Height = 21
          Mascara = mnMinutos
          TabOrder = 3
          Text = '0'
        end
        object iPerFin: TZetaNumero
          Left = 154
          Top = 114
          Width = 44
          Height = 21
          Mascara = mnMinutos
          TabOrder = 4
          Text = '0'
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 158
        Width = 420
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 420
        inherited Advertencia: TcxLabel
          Caption = 
            'Al aplicar el proceso se crear'#225'n los periodos del tipo de n'#243'mina' +
            ' indicado.'
          Style.IsFontAssigned = True
          Width = 352
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      PageVisible = False
      inherited sCondicionLBl: TLabel
        Left = 16
        Top = 133
      end
      inherited sFiltroLBL: TLabel
        Left = 41
        Top = 162
      end
      inherited Seleccionar: TcxButton
        Left = 144
        Top = 250
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 68
        Top = 130
      end
      inherited sFiltro: TcxMemo
        Left = 68
        Top = 161
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 68
        Top = 0
      end
      inherited bAjusteISR: TcxButton
        Left = 382
        Top = 162
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
  end
end
