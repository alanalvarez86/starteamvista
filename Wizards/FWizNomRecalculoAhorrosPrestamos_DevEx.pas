unit FWizNomRecalculoAhorrosPrestamos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Mask, ComCtrls, Buttons, ExtCtrls,
     FWizEmpBaseFiltro,
     ZetaWizard,
     ZetaKeyCombo,
     ZetaNumero,
     ZetaEdit, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl;

type
  TWizNomRecalculoAhorrosPrestamos_DevEx = class(TWizEmpBaseFiltro)
    RecalcularAhorros: TGroupBox;
    RecalcularPrestamos: TGroupBox;
    lPrestamos: TCheckBox;
    lAhorros: TCheckBox;
    TodosAhorros: TRadioButton;
    SoloUnAhorro: TRadioButton;
    TodosPrestamos: TRadioButton;
    SoloUnPrestamo: TRadioButton;
    AH_TIPO: TZetaKeyLookup_DevEx;
    PR_TIPO: TZetaKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
    procedure lAhorrosClick(Sender: TObject);
    procedure SoloUnAhorroClick(Sender: TObject);
    procedure TodosAhorrosClick(Sender: TObject);
    procedure lPrestamosClick(Sender: TObject);
    procedure TodosPrestamosClick(Sender: TObject);
    procedure SoloUnPrestamoClick(Sender: TObject);//DevEx(by am): Agregado para enfocar correctamente los controles.
    procedure ControlesVerificar;
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean;override;
    procedure CargaParametros;override;
  public
    { Public declarations }
  end;

var
  WizNomRecalculoAhorrosPrestamos_DevEx: TWizNomRecalculoAhorrosPrestamos_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaCommonLists,
     ZetaDialogo,
     DCliente,
     DProcesos,
     dTablas,
     ZetaCommonTools;

{$R *.DFM}

procedure TWizNomRecalculoAhorrosPrestamos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H33425_Recalcular_prestamos_ahorros;
     ParametrosControl := lAhorros;

     //DevEx(by am): Es necesario especificar que Ejecucion sera la ultima pagina del wizard.
     Parametros.PageIndex := WizardControl.PageCount - 3;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 2;
     Ejecucion.PageIndex := WizardControl.PageCount - 1;

     //Conectar LookUp
     dmTablas.cdsTAhorro.Conectar;
     dmTablas.cdsTPresta.Conectar;

     AH_TIPO.LookupDataset := dmTablas.cdsTAhorro;
     PR_TIPO.LookupDataset := dmTablas.cdsTPresta;

     ControlesVerificar;
end;

procedure TWizNomRecalculoAhorrosPrestamos_DevEx.CargaParametros;
begin
     inherited;
     with Descripciones do
     begin
           AddBoolean( 'Recalcula ahorros', lAhorros.Checked );
           if lAhorros.Checked then
           begin
                if TodosAhorros.Checked then
                   AddBoolean( 'Todos los ahorros', TodosAhorros.Checked );
                if SoloUnAhorro.Checked then
                   AddString('S�lo un tipo de ahorro', AH_TIPO.Descripcion );
           end;
           AddBoolean( 'Recalcula pr�stamos', lPrestamos.Checked );
           if lPrestamos.Checked then
           begin
                if TodosPrestamos.Checked then
                   AddBoolean( 'Todos los pr�stamos', TodosPrestamos.Checked );
                if SoloUnPrestamo.Checked then
                   AddString('S�lo un tipo de pr�stamo', PR_TIPO.Descripcion );
           end;
     end;
     with ParameterList do
     begin
          AddInteger( 'Mes', 0 );
          AddInteger( 'Year', 0 );
          AddBoolean( 'Acumulados', False );
          AddBoolean( 'Ahorros', lAhorros.Checked );
          if lAhorros.Checked then
          begin
               if TodosAhorros.Checked then
                  AddString( 'TipoAhorro', VACIO );
               if SoloUnAhorro.Checked then
               begin
                    AddString('TipoAhorroDescripcion', AH_TIPO.Descripcion );
                    AddString('TipoAhorro', AH_TIPO.Llave );
               end;
          end;
          AddBoolean( 'Prestamos', lPrestamos.Checked );
          if lPrestamos.Checked then
          begin
               if TodosPrestamos.Checked then
                  AddString( 'TipoPrestamo', VACIO );
               if SoloUnPrestamo.Checked then
               begin
                    AddString('TipoPrestamoDescripcion', Trim( PR_TIPO.Descripcion ) );
                    AddString('TipoPrestamo', PR_TIPO.Llave );
               end;
          end;
     end;
end;

procedure TWizNomRecalculoAhorrosPrestamos_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin

     ParametrosControl := nil;
     if CanMove and Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
     begin
          if not lAhorros.Checked and not lPrestamos.Checked then
          begin
               ZetaDialogo.ZError( Caption, 'Debe seleccionar al menos una opci�n de Rec�lculo', 0 );
               CanMove := FALSE;
          end;
          //Validar Prestamos y Ahorros
          if CanMove and lAhorros.Checked and SoloUnAhorro.Enabled and SoloUnAhorro.Checked then
          begin
               if StrVacio( AH_TIPO.Llave ) then
               begin
                    ZetaDialogo.ZError( Caption, 'Debe seleccionar un tipo de ahorro', 0 );
                    CanMove := FALSE;
               end;
          end;
          if CanMove and lPrestamos.Checked and SoloUnPrestamo.Enabled and SoloUnPrestamo.Checked then
          begin
               if StrVacio( PR_TIPO.Llave ) then
               begin
                    ZetaDialogo.ZError( Caption, 'Debe seleccionar un tipo de pr�stamo', 0 );
                    CanMove := FALSE;
               end;
          end;
     end;
     inherited;
end;

function TWizNomRecalculoAhorrosPrestamos_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.RecalculoAhorrosPrestamos(ParameterList);
end;

procedure TWizNomRecalculoAhorrosPrestamos_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'El proceso recalcula los saldos de ahorros y/o pr�stamos considerando las n�minas afectadas para el grupo de empleados seleccionados.';
end;

procedure TWizNomRecalculoAhorrosPrestamos_DevEx.lAhorrosClick(Sender: TObject);
begin
     inherited;
     TodosAhorros.Enabled := lAhorros.Checked;
     SoloUnAhorro.Enabled := lAhorros.Checked;
     if not lAhorros.Checked then
        AH_TIPO.Enabled := lAhorros.Checked
     else
         AH_TIPO.Enabled := SoloUnAhorro.Checked;
end;

procedure TWizNomRecalculoAhorrosPrestamos_DevEx.lPrestamosClick( Sender: TObject );
begin
     inherited;
     TodosPrestamos.Enabled := lPrestamos.Checked;
     SoloUnPrestamo.Enabled := lPrestamos.Checked;
     if not lPrestamos.Checked then
        PR_TIPO.Enabled := lPrestamos.Checked
     else
         PR_TIPO.Enabled := SoloUnPrestamo.Checked;
end;

procedure TWizNomRecalculoAhorrosPrestamos_DevEx.SoloUnAhorroClick( Sender: TObject );
begin
     inherited;
     TodosAhorros.Checked := False;
     AH_TIPO.Enabled := SoloUnAhorro.Checked;
end;

procedure TWizNomRecalculoAhorrosPrestamos_DevEx.SoloUnPrestamoClick( Sender: TObject );
begin
     inherited;
     TodosPrestamos.Checked := not SoloUnPrestamo.Checked;
     PR_TIPO.Enabled := SoloUnPrestamo.Checked;
end;

procedure TWizNomRecalculoAhorrosPrestamos_DevEx.TodosAhorrosClick( Sender: TObject );
begin
     inherited;
     AH_TIPO.Enabled := not TodosAhorros.Checked;
     SoloUnAhorro.Checked := not TodosAhorros.Checked;
end;

procedure TWizNomRecalculoAhorrosPrestamos_DevEx.TodosPrestamosClick( Sender: TObject );
begin
     inherited;
     SoloUnPrestamo.Checked := not TodosPrestamos.Checked;
     PR_TIPO.Enabled := not TodosPrestamos.Checked;
end;

procedure TWizNomRecalculoAhorrosPrestamos_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.Adelante then
          if Wizard.EsPaginaActual( Parametros ) then
             ParametrosControl := lAhorros;
     inherited;
end;

procedure TWizNomRecalculoAhorrosPrestamos_DevEx.ControlesVerificar;
begin
     TodosAhorros.Enabled   := lAhorros.Checked;
     SoloUnAhorro.Enabled   := lAhorros.Checked;
     AH_TIPO.Enabled        := lAhorros.Checked;
     TodosPrestamos.Enabled := lPrestamos.Checked;
     SoloUnPrestamo.Enabled := lPrestamos.Checked;
     PR_TIPO.Enabled        := lPrestamos.Checked;
end;

end.
