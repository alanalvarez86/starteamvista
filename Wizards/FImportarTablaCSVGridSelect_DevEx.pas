unit FImportarTablaCSVGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Db, StdCtrls, ExtCtrls, dbClient,
  ZBasicoSelectGrid_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, cxControls, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ZetaCXGrid, cxButtons;

type

  TImportarTablaCSVGridSelect_DevEx = class(TBasicoGridSelect_DevEx)
    procedure FormShow(Sender: TObject);
  private
    procedure AgregaColumnasGrid;
  public
  end;

var
  ImportarTablaCSVGridSelect_DevEx: TImportarTablaCSVGridSelect_DevEx;

implementation

uses dGlobal,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo,
     ZGlobalTress;

{$R *.DFM}

procedure TImportarTablaCSVGridSelect_DevEx.FormShow(Sender: TObject);
begin
     AgregaColumnasGrid;
     ApplyMinWidth;
     ZetaDBGridDBTableView.OptionsCustomize.ColumnHorzSizing := True;
     ZetaDBGridDBTableView.ApplyBestFit();
     ZetaDBGridDBTableView.OptionsCustomize.ColumnHorzSizing := False;
     {***(am):Trabaja CON GridMode***}
     if ZetaDBGridDBTableView.DataController.DataModeController.GridMode then
        ZetaDBGridDBTableView.OptionsCustomize.ColumnFiltering := False;
     {***}
     // inherited;  //DevEx
end;

procedure TImportarTablaCSVGridSelect_DevEx.AgregaColumnasGrid;
const K_ANCHO_DEFAULT = 100;
var
   i : Integer;

    procedure AgregarColumna( const sCampo: String; const iAncho: Integer; const sTitulo: String);
    begin
       //with ZetaDBGrid.Columns.Add do
       with ZetaDBGridDBTableView.CreateColumn  do        //DevEx
       begin
            //FieldName:= sCampo;
            DataBinding.FieldName := sCampo;              //DevEx
            Width := iAncho;
            //Title.Caption:= sTitulo;
            Caption:= sTitulo;                      //DevEx
       end;
    end;
begin
     for i:=0 to (DataSet.FieldCount-4) do
          AgregarColumna(DataSet.Fields[i].FieldName, K_ANCHO_DEFAULT, DataSet.Fields[i].FieldName);
end;


end.
