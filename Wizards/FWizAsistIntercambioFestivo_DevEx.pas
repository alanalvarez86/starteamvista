unit FWizAsistIntercambioFestivo_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ZetaEdit, StdCtrls,
  Buttons, ZetaWizard, ExtCtrls, Mask, ZetaFecha, ZCXBaseWizardFiltro,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, Menus, ZetaCXWizard, cxRadioGroup, cxTextEdit,
  cxMemo, ZetaKeyLookup_DevEx, cxButtons, dxGDIPlusClasses, cxImage,
  cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl, dxSkinsCore,
  TressMorado2013;

type
  TWizAsistIntercambioFestivo_DevEx = class(TBaseCXWizardFiltro)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    FechaNueva: TZetaFecha;
    FechaOriginal: TZetaFecha;
    ckFestivoTrabajado: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
  protected
    Procedure CargaParametros; override;
    procedure CargaListaVerificacion; override;
    function Verificar: Boolean;  override;
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizAsistIntercambioFestivo_DevEx: TWizAsistIntercambioFestivo_DevEx;

implementation

{$R *.dfm}

Uses DCliente,
     DProcesos,
     ZGlobalTress,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     ZBaseSelectGrid_DevEx,
     FEmpleadoGridSelect_DevEx;

procedure TWizAsistIntercambioFestivo_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := FechaOriginal;
     with dmCliente do
     begin
          FechaOriginal.Valor := FechaAsistencia;
          FechaNueva.Valor := FechaAsistencia;
     end;
     HelpContext:= H_ASIS_WIZ_INTERCAMBIO_FESTIVO; //act. HC
end;

Procedure TWizAsistIntercambioFestivo_DevEx.CargaParametros;
begin

     inherited CargaParametros;

     with ParameterList do
     begin
          AddDate('FechaOriginal', FechaOriginal.Valor);
          AddDate('FechaNueva', FechaNueva.Valor);
          AddBoolean( 'PuedeCambiarTarjeta', dmCliente.PuedeModificarTarjetasAnteriores );
          AddBoolean ( 'FestivoTrabajado', ckFestivoTrabajado.Checked );
     end;
         with Descripciones do
     begin
          AddDate('Fecha Original', FechaOriginal.Valor);
          AddDate('Fecha Nueva', FechaNueva.Valor);
          if ckFestivoTrabajado.Checked then
            AddString( 'Horario de Festivo trabajado', 'S' )
          else
            AddString( 'Horario de Festivo trabajado', 'N' )
     end;
end;

procedure TWizAsistIntercambioFestivo_DevEx.CargaListaVerificacion;
begin
     dmProcesos.IntercambioFestivoGetLista( ParameterList );
end;

function TWizAsistIntercambioFestivo_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpleadoGridSelect_DevEx );
end;

function TWizAsistIntercambioFestivo_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.IntercambioFestivo( ParameterList, Verificacion );
end;

procedure TWizAsistIntercambioFestivo_DevEx.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
  ParametrosControl := nil;
     inherited;
     if CanMove and Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
     begin
        if ( FechaOriginal.Valor = FechaNueva.Valor ) then
           CanMove := Error('Las fechas original y nueva deben ser diferentes', FechaNueva);
        if CanMove then
           CanMove := dmCliente.PuedeCambiarTarjetaDlg(FechaNueva.Valor,FechaNueva.Valor);
     end;
end;

procedure TWizAsistIntercambioFestivo_DevEx.WizardAfterMove(
  Sender: TObject);
begin
  inherited;
  if Wizard.EsPaginaActual(Parametros) then
    ParametrosControl := FechaOriginal;

end;

end.
