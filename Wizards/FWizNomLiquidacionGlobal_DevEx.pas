unit FWizNomLiquidacionGlobal_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, ExtCtrls, StdCtrls,
     FWizNomBase_DevEx, ZetaDBTextBox, ZetaEdit, ZetaKeyLookup_DevEx,
     cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
     cxRadioGroup, cxTextEdit, cxMemo, cxButtons, dxGDIPlusClasses,
     cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
     dxWizardControl;

type
  TWizNomLiquidacionGlobal_DevEx = class(TWizNomBase_DevEx)
    GRTipo: TcxRadioGroup;
    Label5: TLabel;
    Observaciones: TZetaEdit;
    procedure FormCreate(Sender: TObject);
    procedure GRTipoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean;override;
    function Verificar: Boolean;override;
    procedure CargaListaVerificacion;override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizNomLiquidacionGlobal_DevEx: TWizNomLiquidacionGlobal_DevEx;

implementation

uses ZetaCommonClasses,
     ZBaseSelectGrid_DevEx,
     FEmpleadoGridSelect_DevEx,
     DProcesos;

{$R *.DFM}

procedure TWizNomLiquidacionGlobal_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H33415_Liquidacion_global;
     GrTipo.ItemIndex := 0;
     Observaciones.MaxLength := ZetaCommonClasses.K_ANCHO_DESCRIPCION;
     //Observaciones.Text := GRTipo.Items.Strings[GRTipo.ItemIndex];
     Observaciones.Text := String( GRTipo.Properties.Items.Items[ GRTipo.ItemIndex ].Value );    //DevEx

     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;

end;

procedure TWizNomLiquidacionGlobal_DevEx.CargaListaVerificacion;
begin
     dmProcesos.LiquidacionGlobalLista( ParameterList );
end;

function TWizNomLiquidacionGlobal_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpleadoGridSelect_DevEx );
end;

function TWizNomLiquidacionGlobal_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.LiquidacionGlobal(ParameterList, Verificacion);
end;

procedure TWizNomLiquidacionGlobal_DevEx.CargaParametros;
begin
     inherited CargaParametros;

     with ParameterList do
     begin
          AddInteger( 'TipoLiquidacion', GRTipo.ItemIndex + 1 );
          AddString( 'Observaciones', Observaciones.Text );
          AddString( 'Simulacion', K_GLOBAL_NO );

     end;
     with Descripciones do
     begin
          //AddString( 'Tipo Liquidaci�n', GRTipo.Items.Strings[GRTipo.ItemIndex] );
          AddString( 'Tipo Liquidaci�n', String( GRTipo.Properties.Items.Items[ GRTipo.ItemIndex ].Value ) );    //DevEx
          AddString( 'Observaciones', Observaciones.Text );
     end;

end;

procedure TWizNomLiquidacionGlobal_DevEx.GRTipoClick(Sender: TObject);
begin
     inherited;
     //Observaciones.Text := GRTipo.Items.Strings[GRTipo.ItemIndex];;
     Observaciones.Text := String( GRTipo.Properties.Items.Items[ GRTipo.ItemIndex ].Value );    //DevEx
end;

procedure TWizNomLiquidacionGlobal_DevEx.FormShow(Sender: TObject);
begin
  inherited;

  Advertencia.Caption :=
     'Al aplicar el proceso se realizar� el c�lculo de liquidaciones de los empleados indicados.';
end;

end.
