unit FNomCredAplicadoGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, ExtCtrls, ZBaseSelectGrid_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxTextEdit, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons;

type
  TNomCredAplicadoGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    ACUMULADO: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  NomCredAplicadoGridSelect_DevEx: TNomCredAplicadoGridSelect_DevEx;

implementation

{$R *.DFM}

procedure TNomCredAplicadoGridSelect_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  (ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO')).Caption := 'Empleado';
  (ZetaDBGridDBTableView.GetColumnByFieldName('PrettyName')).Caption := 'Nombre Completo';
  (ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO')).MinWidth := 75;
end;

end.
