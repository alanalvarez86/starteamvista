inherited WizNomLiquidacionGlobal_DevEx: TWizNomLiquidacionGlobal_DevEx
  Left = 265
  Top = 162
  Caption = 'Liquidaci'#243'n Global'
  ClientHeight = 423
  ClientWidth = 474
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 474
    Height = 423
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que facilita la labor de liquidar o indemnizar a un conj' +
        'unto de empleados.'
      Header.Title = 'Liquidaci'#243'n global'
      object Label5: TLabel [0]
        Left = 31
        Top = 64
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'Observaciones:'
        Transparent = True
      end
      inherited GroupBox1: TGroupBox
        Left = 30
        Top = 89
        TabOrder = 1
      end
      object GRTipo: TcxRadioGroup
        Left = 30
        Top = 8
        Caption = 'Tipo'
        Properties.Columns = 2
        Properties.Items = <
          item
            Caption = 'Liquidaci'#243'n'
            Value = 'Liquidaci'#243'n'
          end
          item
            Caption = 'Indemnizaci'#243'n'
            Value = 'Indemnizaci'#243'n'
          end>
        TabOrder = 0
        OnClick = GRTipoClick
        Height = 41
        Width = 409
      end
      object Observaciones: TZetaEdit
        Left = 119
        Top = 60
        Width = 321
        Height = 21
        TabOrder = 2
        Text = 'Observaciones'
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 188
        Width = 452
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 452
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 384
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 31
        Top = 137
      end
      inherited sFiltroLBL: TLabel
        Left = 56
        Top = 166
      end
      inherited Seleccionar: TcxButton
        Left = 159
        Top = 254
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 83
        Top = 134
      end
      inherited sFiltro: TcxMemo
        Left = 83
        Top = 165
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 83
        Top = 4
      end
      inherited bAjusteISR: TcxButton
        Left = 372
        Top = 174
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    Top = 308
  end
end
