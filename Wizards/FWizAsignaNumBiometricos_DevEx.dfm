inherited WizAsignaNumBiometricos_DevEx: TWizAsignaNumBiometricos_DevEx
  Left = 308
  Top = 174
  Caption = 'Asignaci'#243'n de N'#250'meros Biom'#233'tricos'
  ClientHeight = 417
  ClientWidth = 437
  ExplicitWidth = 443
  ExplicitHeight = 446
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 437
    Height = 417
    ExplicitWidth = 437
    ExplicitHeight = 417
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que asigna el n'#250'mero de biom'#233'trico y grupo de terminales' +
        ' a un grupo de empleados.'
      Header.Title = 'Asignaci'#243'n de n'#250'meros biom'#233'tricos'
      ExplicitWidth = 415
      ExplicitHeight = 277
      object Label1: TLabel
        Left = 10
        Top = 90
        Width = 97
        Height = 13
        Caption = 'Grupo de terminales:'
      end
      object luGrupo: TZetaKeyLookup_DevEx
        Left = 113
        Top = 87
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      ExplicitWidth = 415
      ExplicitHeight = 277
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 415
        ExplicitHeight = 182
        Height = 182
        Width = 415
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 415
        Width = 415
        inherited Advertencia: TcxLabel
          Caption = 
            'Al aplicar el proceso ser'#225'n asignados los n'#250'meros biom'#233'tricos al' +
            ' grupo de empleados indicados.'
          Style.IsFontAssigned = True
          ExplicitLeft = 67
          ExplicitTop = 20
          ExplicitWidth = 347
          ExplicitHeight = 74
          Width = 347
          AnchorY = 57
        end
        inherited cxImage1: TcxImage
          ExplicitLeft = 1
          ExplicitTop = 20
          ExplicitHeight = 74
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      ExplicitWidth = 415
      ExplicitHeight = 277
      inherited sCondicionLBl: TLabel
        Left = 14
        Top = 133
        ExplicitLeft = 14
        ExplicitTop = 133
      end
      inherited sFiltroLBL: TLabel
        Left = 39
        Top = 162
        ExplicitLeft = 39
        ExplicitTop = 162
      end
      inherited Seleccionar: TcxButton
        Left = 142
        Top = 250
        Visible = True
        ExplicitLeft = 142
        ExplicitTop = 250
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 66
        Top = 130
        ExplicitLeft = 66
        ExplicitTop = 130
      end
      inherited sFiltro: TcxMemo
        Left = 66
        Top = 161
        Style.IsFontAssigned = True
        ExplicitLeft = 66
        ExplicitTop = 161
      end
      inherited GBRango: TGroupBox
        Left = 66
        Top = 0
        ExplicitLeft = 66
        ExplicitTop = 0
      end
      inherited bAjusteISR: TcxButton
        Left = 380
        Top = 162
        ExplicitLeft = 380
        ExplicitTop = 162
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 220
    Top = 372
  end
end
