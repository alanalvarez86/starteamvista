inherited WizIMSSValidacionIDSE_DevEx: TWizIMSSValidacionIDSE_DevEx
  Left = 358
  Top = 333
  Caption = 'Validaci'#243'n Movimientos IDSE'
  ClientHeight = 388
  ClientWidth = 509
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 509
    Height = 388
    inherited Parametros: TdxWizardControlPage
      Header.AssignedValues = [wchvGlyph]
      Header.Glyph.Data = {
        89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
        F8000000206348524D00007A25000080830000F9FF000080E9000075300000EA
        6000003A980000176F925FC546000000097048597300000EBC00000EBC0195BC
        7249000002B149444154484BB5954D68134114C75789B5156951B4A6205A45B0
        952A3914D45345450A86EC46F02678123C78F0A0F88160AF5241B21BF0EB1044
        B056F0926E62DAAD59DBD49662DBCD6EC48A1513DB6A2D057BA8506CACEB7BE9
        4C58DCD9B8517CF063775EE6FDDFE4CD9B59CE34CDFF8AA3DDD2F87A3113BC2D
        1A822619421E98970C3E25A685CB91ECE94A32EDEF4C4A0B67455D580451139E
        13A2C1772130CE529F64049AC9F4F22C940EB6A288A40BE352863F44DC458344
        C24A027EEAE6887F1371BB370C4481F0EB935EE2B219FE860B80F2B513973BC3
        BA174A60048E1297A36189707FC8B0B489C68906108EC0AA1452FB881B449D5F
        A0EF448A6DB0A97E9864FE0BD7D5160F91B31B760D2BA81CB0BC44CE6E92CEB7
        B182CA21A4070F1039BBC104AC2333D02DD8DE44CE6EB8B9AC2097CC43377584
        35DE47E4EC06BD9F6104FE897C48E7CFCF46390EB9337C78CBDD91631BE99842
        120873BF059764A53D03CDD8397DEAAECE69D9B30C6226322557E49FF737DE28
        26C08B8B25520A7AE1E56295DFA9F0E7AED53F113A3694DA8942027A7ADD82D7
        08AE3CADD40E52B12FD155E660727BA7D2DF782D17AF5AA4FEEE81A62B1C5E68
        2C2127C286701F574645900175C7BDD84B9F5F53BCA96CACEA1BF54FCA154B9C
        A809A758424E40C79C7BF2EAE03E6B82D1DEBAC4A3D123DB3EC4D77DB5FA1138
        C5FC05969013D8390FC75A9AB02C5621A367F33024A9799BA8C95AFD70D109ED
        2C2127B0E721909BB16CE8BB44F5DC78F7865CF245C345B56FF725EAFF247B7E
        60820E969013B0C933986028591FA74263BD7599A743FB8FCB03BE3393B1B54B
        D4DF93DA731513A82CA1924059B193DE3F5BBF40C560B5CB081DA715EF1B7816
        0E59E13B5B2679FC28619294BAF3F1B4BCA628FC11FE4122B5B70DC50B09E016
        DC8A67A15CF0B3494590075A6B351E3EAB6F36CA71BF000786687A3B9F27F400
        00000049454E44AE426082}
      Header.Description = 
        ' Este proceso utiliza la Constancia de transmisi'#243'n de movimiento' +
        's afiliatorios para certificar en el kardex de los empleados los' +
        ' movimientos que fueron recibidos por el IMSS.'
      Header.Title = 'Validaci'#243'n de  movimientos IDSE'
      object Label5: TLabel
        Left = 22
        Top = 32
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'A&rchivo:'
        FocusControl = sArchivo
        Transparent = True
      end
      object BCargaArchivo: TcxButton
        Left = 407
        Top = 28
        Width = 21
        Height = 21
        Hint = 'Buscar Archivo a Importar'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BCargaArchivoClick
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A40500000000000000000000000000000000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFDFF5
          FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
          FCFFDFF5FCFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF10B7EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF40C5EFFF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF40C5EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6CD3
          F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF6CD3F2FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF93DEF6FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF97E0F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFEBF9FDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF54CB
          F0FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CF
          F1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF80D9F4FF80D9
          F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC3EDFAFFDFF5FCFFDFF5FCFFDFF5
          FCFF70D4F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
        OptionsImage.Margin = 1
      end
      object sArchivo: TEdit
        Left = 66
        Top = 28
        Width = 336
        Height = 21
        TabOrder = 0
        OnChange = sArchivoChange
      end
      object GroupBox1: TcxGroupBox
        Left = 0
        Top = 72
        Align = alBottom
        Caption = ' Informaci'#243'n del Documento:  '
        TabOrder = 1
        Height = 176
        Width = 487
        object Label1: TLabel
          Left = 62
          Top = 44
          Width = 79
          Height = 13
          Caption = 'N'#250'mero de Lote:'
          Transparent = True
        end
        object Label2: TLabel
          Left = 34
          Top = 81
          Width = 107
          Height = 13
          Caption = 'Fecha de Transmisi'#243'n:'
          Transparent = True
        end
        object tbNumeroLote: TZetaTextBox
          Left = 144
          Top = 42
          Width = 150
          Height = 17
          AutoSize = False
          Caption = 'tbNumeroLote'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object tbFechaTransmision: TZetaTextBox
          Left = 144
          Top = 79
          Width = 100
          Height = 17
          AutoSize = False
          Caption = 'tbFechaTransmision'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object Label4: TLabel
          Left = 102
          Top = 24
          Width = 39
          Height = 13
          Alignment = taRightJustify
          Caption = 'Archivo:'
          Transparent = True
        end
        object tbArchivoCargado: TZetaTextBox
          Left = 144
          Top = 22
          Width = 257
          Height = 17
          AutoSize = False
          Caption = 'tbArchivoCargado'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object Label9: TLabel
          Left = 57
          Top = 62
          Width = 84
          Height = 13
          Caption = 'Registro Patronal:'
          Transparent = True
        end
        object tbRegistroPatron: TZetaTextBox
          Left = 144
          Top = 60
          Width = 100
          Height = 17
          AutoSize = False
          Caption = 'tbFechaTransmision'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object gpResumenMovimientos: TcxGroupBox
          Left = 33
          Top = 100
          Caption = ' Movimientos Operados: '
          TabOrder = 0
          Height = 65
          Width = 425
          object Label3: TLabel
            Left = 357
            Top = 18
            Width = 24
            Height = 13
            Caption = 'Total'
            Transparent = True
          end
          object tbMovimientosOperados: TZetaTextBox
            Left = 319
            Top = 35
            Width = 100
            Height = 17
            AutoSize = False
            Caption = 'tbMovimientosOperados'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
          end
          object tbMovimientosAltas: TZetaTextBox
            Left = 215
            Top = 35
            Width = 100
            Height = 17
            AutoSize = False
            Caption = 'tbMovimientosAltas'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
          end
          object Label6: TLabel
            Left = 254
            Top = 18
            Width = 23
            Height = 13
            Caption = 'Altas'
            Transparent = True
          end
          object tbMovimientosCambios: TZetaTextBox
            Left = 111
            Top = 35
            Width = 100
            Height = 17
            AutoSize = False
            Caption = 'tbMovimientosCambios'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
          end
          object Label7: TLabel
            Left = 141
            Top = 18
            Width = 40
            Height = 13
            Caption = 'Cambios'
            Transparent = True
          end
          object tbMovimientosBajas: TZetaTextBox
            Left = 8
            Top = 35
            Width = 100
            Height = 17
            AutoSize = False
            Caption = 'tbMovimientosBajas'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
          end
          object Label8: TLabel
            Left = 45
            Top = 18
            Width = 26
            Height = 13
            Caption = 'Bajas'
            Transparent = True
          end
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 153
        Width = 487
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 487
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 419
          AnchorY = 51
        end
      end
    end
    object Validacion: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 'Validacion del archivo'
      Header.Title = 'Validacion'
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 487
        Height = 78
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object btCancelarCarga: TcxButton
          Left = 372
          Top = 47
          Width = 80
          Height = 26
          Hint = 'Cancelar carga de archivo'
          Caption = 'Cancelar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = btCancelarCargaClick
          OptionsImage.Glyph.Data = {
            36090000424D3609000000000000360000002800000018000000180000000100
            20000000000000090000000000000000000000000000000000004858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF98A1E2FFF6F7FDFFB7BEEBFF4B5BCDFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFB1B8
            E9FFF6F7FDFFACB3E8FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFFE2E5F7FFFFFFFFFFFFFFFFFFC3C8EEFF4B5BCDFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFF
            FFFFFFFFFFFFF6F7FDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF9BA4E3FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4B5B
            CDFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFF
            FFFFFFFFFFFFA6AEE6FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
            EEFF4B5BCDFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
            FFFFAFB6E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
            FFFFC3C8EEFF4B5BCDFF4B5BCDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
            E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFF
            FFFFFFFFFFFFC3C8EEFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6
            E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8
            EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4B5BCDFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4B5BCDFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFF
            FFFFFFFFFFFFAFB6E9FFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4B5B
            CDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
            FFFFAFB6E9FF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
            EEFF4B5BCDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
            E9FF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
            FFFFC3C8EEFF4B5BCDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFFA9B0E7FFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFF
            FFFFFFFFFFFFB7BEEBFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFFE2E5F7FFFFFFFFFFFFFFFFFFAFB6E9FF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFF
            FFFFFFFFFFFFF6F7FDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF8792DEFFDFE2F6FFA1A9E5FF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF9BA4
            E3FFDFE2F6FF98A1E2FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
          OptionsImage.Margin = 1
        end
        object PanelNombreArchivo: TPanel
          Left = 0
          Top = 11
          Width = 452
          Height = 17
          BevelOuter = bvNone
          Caption = 'ARCHIVO ARCHIVO ARCHIVO ARCHIVO.pdf'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object PanelMensaje: TPanel
          Left = 0
          Top = 26
          Width = 452
          Height = 17
          BevelOuter = bvNone
          Caption = 'Cargando documento ...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object barraProgreso: TProgressBar
          Left = 5
          Top = 47
          Width = 362
          Height = 17
          ParentShowHint = False
          Step = 1
          ShowHint = True
          TabOrder = 2
        end
      end
      object PanelMovimientosError: TPanel
        Left = 0
        Top = 78
        Width = 487
        Height = 170
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        Visible = False
        object lbMovNoReconocidos: TLabel
          Left = 0
          Top = 0
          Width = 487
          Height = 13
          Align = alTop
          Caption = 'Movimientos con error de lectura:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object ZetaCXGrid: TZetaCXGrid
          Left = 0
          Top = 13
          Width = 487
          Height = 157
          Align = alClient
          TabOrder = 0
          object gridNoReconocidos: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = DataSourceRegistrosIDSE
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.FocusCellOnTab = True
            OptionsCustomize.ColumnFiltering = False
            OptionsCustomize.ColumnGrouping = False
            OptionsCustomize.ColumnHidingOnGrouping = False
            OptionsCustomize.ColumnHorzSizing = False
            OptionsCustomize.ColumnMoving = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsSelection.CellSelect = False
            OptionsSelection.MultiSelect = True
            OptionsView.GroupByBox = False
            OptionsView.Indicator = True
            Styles.Content = cxStyle1
            object PAGINA: TcxGridDBColumn
              Caption = 'P'#225'gina'
              DataBinding.FieldName = 'PAGINA'
            end
            object NSS: TcxGridDBColumn
              Caption = 'N.S.S.'
              DataBinding.FieldName = 'NSS'
              Width = 115
            end
            object TEXTO: TcxGridDBColumn
              Caption = 'Texto de Referencia'
              DataBinding.FieldName = 'TEXTO'
              Width = 279
            end
          end
          object ZetaCXGridLevel: TcxGridLevel
            GridView = gridNoReconocidos
          end
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 320
    Top = 65531
  end
  object OpenDialog: TOpenDialog
    Filter = 'Acrobat PDF (*.pdf)|*.pdf'
    Options = [ofHideReadOnly, ofAllowMultiSelect]
    Left = 468
    Top = 65534
  end
  object DataSourceRegistrosIDSE: TDataSource
    Left = 388
    Top = 65521
  end
  object cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = clBtnFace
      TextColor = clWindowText
    end
  end
end
