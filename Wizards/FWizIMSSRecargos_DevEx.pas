unit FWizIMSSRecargos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes,
     Graphics, Controls, Forms, Dialogs,
     ComCtrls, StdCtrls, ExtCtrls, Mask,
     ZetaDBTextBox, FWizImssBaseFiltro,
     ZetaNumero, ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxContainer, cxEdit, ZetaCXWizard,
     dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
     dxWizardControl, Menus, ZetaEdit, cxRadioGroup, cxTextEdit, cxMemo,
     cxButtons, ZetaKeyLookup_DevEx, ZetaKeyCombo;

type
  TWizIMSSRecargos_DevEx = class(TWizIMSSBaseFiltro)
   //Label3: TLabel;
    Label4: TLabel;
    ZTasa: TZetaNumero;
    ZFactor: TZetaNumero;
  { Panel1: TPanel;
    Patron: TZetaDBTextBox;
    Mes: TZetaDBTextBox;
    Anio: TZetaDBTextBox;
    Tipo: TZetaDBTextBox;
    Label8: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    }   //DevEx, se heredan de nueva base
    
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizIMSSRecargos_DevEx: TWizIMSSRecargos_DevEx;

implementation

uses ZetaDialogo,
     ZetaCommonClasses,
     ZetaCommonLists,
     DProcesos,
     DCliente;

{$R *.DFM}

procedure TWizIMSSRecargos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := ZFactor;
     //DevEx , se heredan de nueva base
 {    with dmCliente do
     begin
          Patron.Caption := IMSSPatron;
          Mes.Caption := ObtieneElemento( lfMeses, ( IMSSMes - 1 ) );
          Anio.Caption := IntToStr( IMSSYear );
          Tipo.Caption := ObtieneElemento( lfTipoLiqIMSS, Ord( IMSSTipo ) );
     end;
     }
     HelpContext := H40442_Calcular_recargos;
     ZFactor.Valor := 1;

     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;
end;

procedure TWizIMSSRecargos_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     ParametrosControl := nil;
     inherited;
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros ) then
          begin
               if ( ZFactor.Valor < 1 ) then
               begin
                    ZetaDialogo.zError( Caption, '� Factor debe ser igual � mayor a 1 !', 0 );
                    ActiveControl := ZFactor;
                    CanMove := False;
               end;
          end;
     end;
end;

procedure TWizIMSSRecargos_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddFloat( 'Tasa', ZTasa.Valor );
          AddFloat( 'Factor', ZFactor.Valor );
     end;

     //DevEx
     with Descripciones do
     begin
          AddFloat( 'Tasa de recargos', ZTasa.Valor );
          AddFloat( 'Factor de actualizaci�n', ZFactor.Valor );
     end;
end;

function TWizIMSSRecargos_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CalcularRecargosIMSS( ParameterList );
end;


procedure TWizIMSSRecargos_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := ZFactor
     else
         ParametrosControl := nil;
     inherited;
end;

procedure TWizIMSSRecargos_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption :=
    'Al aplicar el proceso se generan las actualizaciones y recargos de IMSS/INFONAVIT del mes correspondiente.';
end;

end.
