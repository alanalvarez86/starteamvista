unit FWizAsistAutorizacionPrenomina_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, Mask,
     ZetaWizard,
     ZetaFecha,
     ZetaEdit, ZetaDBTextBox, FWizNomBase_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, ZetaKeyLookup_DevEx, cxButtons, cxRadioGroup, cxTextEdit,
  cxMemo, cxGroupBox, cxLabel, dxGDIPlusClasses, cxImage,
  dxCustomWizardControl, dxWizardControl;

type
  TAutorizacionPrenomina_DevEx = class(TWizNomBase_DevEx)
    GroupBox2: TGroupBox;
    chkAutorizar: TRadioButton;
    chkDesAutorizar: TRadioButton;
    procedure FormCreate(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
    procedure chkAutorizarClick(Sender: TObject);
    procedure chkDesAutorizarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FAutorizacion:Integer;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
  public
    { Public declarations }
  end;

var
  AutorizacionPrenomina_DevEx: TAutorizacionPrenomina_DevEx;

implementation

uses DCliente,
     DProcesos,
     ZetaDialogo,
     ZetaCommonLists,
     ZAccesosMgr,
     ZAccesosTress,
     ZBaseSelectGrid_DevEx,
     FAsisAutorizacionPreNominaGridSelect_DevEx,
     ZetaCommonClasses;

{$R *.DFM}

procedure TAutorizacionPrenomina_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_AutorizacionPrenomina;
     chkAutorizar.Enabled := ZAccesosMgr.CheckDerecho( D_ASIS_PROC_AUTORIZ_PRENOM, K_DERECHO_CONSULTA );
     chkDesautorizar.Enabled := ZAccesosMgr.CheckDerecho( D_ASIS_PROC_AUTORIZ_PRENOM, K_DERECHO_ALTA );
     if (not chkAutorizar.Enabled ) and chkDesautorizar.Enabled  then
     begin
          chkAutorizar.Checked := False;
          chkDesautorizar.Checked := True;
     end
     else
         chkAutorizar.Checked := True;
end;

procedure TAutorizacionPrenomina_DevEx.CargaParametros;
var sAccion:string;
begin
inherited CargaParametros;
     with Descripciones do
     begin
          sAccion := 'Autorización';
          if chkDesAutorizar.Checked then
             sAccion := 'Desautorización';
          AddString( 'Acción', sAccion );
     end;

     with ParameterList do
     begin
          with dmCliente.GetDatosPeriodoActivo do
          begin
               AddInteger( 'Anio', Year );
               AddInteger( 'Tipo', Ord(Tipo) );
               AddInteger( 'Periodo', Numero );
          end;
          AddInteger( 'Autorizacion', FAutorizacion );//0:Autorizar , 1:Desautorizar
     end;
end;

function TAutorizacionPrenomina_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.AutorizacionPreNomina(ParameterList,Verificacion );
end;

procedure TAutorizacionPrenomina_DevEx.CargaListaVerificacion;
begin
     inherited;
     dmProcesos.AutorizacionPrenominaGetLista( ParameterList );
end;

function TAutorizacionPrenomina_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TAsisAutoricacionPreNominaGridSelect_DevEx );
end;

procedure TAutorizacionPrenomina_DevEx.WizardAfterMove(Sender: TObject);
var sAutoriza:string;
begin
     inherited;
     sAutoriza := 'Autorización';
     if chkDesAutorizar.Checked then
     begin
          sAutoriza := 'Desautorización';
     end;
     if WizardControl.ActivePage = Ejecucion then
     begin
          Advertencia.Caption :='';
          Advertencia.Caption := format('Se realizará la %s de la prenómina activa para los empleados seleccionados.',[sAutoriza]);

     end;
end;

procedure TAutorizacionPrenomina_DevEx.chkAutorizarClick(Sender: TObject);
begin
     inherited;
     chkDesAutorizar.Checked := false;
     FAutorizacion := 0;
end;

procedure TAutorizacionPrenomina_DevEx.chkDesAutorizarClick(Sender: TObject);
begin
     inherited;
     chkAutorizar.Checked := false;
     FAutorizacion := 1;
end;

procedure TAutorizacionPrenomina_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Parametros.PageIndex :=0;
  FiltrosCondiciones.pageIndex :=1;
  Ejecucion.PageIndex :=2;
end;

end.
