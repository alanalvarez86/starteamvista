unit FNomAjusteFonacotGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseSelectGrid_DevEx, DB, StdCtrls, Grids, DBGrids, ZetaDBGrid,
  Buttons, ExtCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxButtons;

type
  TNomAjusteFonacotGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    K_PRETTYNAME: TcxGridDBColumn;
    K_PR_REFEREN: TcxGridDBColumn;
    K_PR_PAG_PER: TcxGridDBColumn;
    K_RETENCION: TcxGridDBColumn;
    K_AJUSTE: TcxGridDBColumn;
    K_MONTO_AJUSTE: TcxGridDBColumn;
    K_SALDO: TcxGridDBColumn;
    K_NO_DIAS_VA: TcxGridDBColumn;
    K_NO_DIAS_FV: TcxGridDBColumn;
    K_NO_DIAS_IN: TcxGridDBColumn;
    K_AUSENTISMOS: TcxGridDBColumn;
    K_PR_FECHA: TcxGridDBColumn;
    K_AJUSTE_CARGO: TcxGridDBColumn;
    K_AJUSTE_FAVOR: TcxGridDBColumn;
    K_PAGO_FONACOT: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure ZetaDBGridDBTableViewColumnHeaderClick(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure FormCreate(Sender: TObject);
  private
    FModalidad : TModalResult;
  public
    { Public declarations }
    property Modalidad : TModalResult read FModalidad;
  end;

var
  NomAjusteFonacotGridSelect_DevEx: TNomAjusteFonacotGridSelect_DevEx;

implementation

uses ZGridModeTools;
{$R *.dfm}

procedure TNomAjusteFonacotGridSelect_DevEx.Cancelar_DevExClick(
  Sender: TObject);
begin
    inherited;
    FModalidad := mrCancel;
end;

procedure TNomAjusteFonacotGridSelect_DevEx.FormCreate(Sender: TObject);
begin
  inherited;

    ZetaDBGridDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
end;

procedure TNomAjusteFonacotGridSelect_DevEx.FormShow(Sender: TObject);
begin
    inherited;
    with DataSet do
    begin
         MaskPesos( 'K_PR_PAG_PER' );
         MaskPesos( 'K_RETENCION' );
         MaskPesos( 'K_AJUSTE' );
         MaskPesos( 'K_MONTO_AJUSTE' );
         MaskPesos( 'K_SALDO');
         MaskPesos( 'K_NO_DIAS_VA');
         MaskPesos( 'K_NO_DIAS_FV');
         MaskPesos( 'K_NO_DIAS_IN');
         MaskPesos( 'K_AUSENTISMOS');
         MaskPesos( 'K_PAGO_FONACOT');
         MaskPesos( 'K_AJUSTE_CARGO');
         MaskPesos( 'K_AJUSTE_FAVOR');
    end;

    //DevEx(by am): Se especifica el HorzSizing porque el nombre de la columna no es estandar
    K_PRETTYNAME.Options.HorzSizing := TRUE;
    K_PRETTYNAME.MinWidth := 150;

    //ApplyMinWidth;
    ZetaDbGridDbTableView.OptionsCustomize.ColumnFiltering := True;
    ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
    ZetaDBGridDBTableView.OptionsView.GroupByBox := False;
    ZetaDBGridDBTableView.FilterBox.Visible := fvNever;
    ZetaDBGridDBTableView.FilterBox.CustomizeDialog := False;
end;

procedure TNomAjusteFonacotGridSelect_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     FModalidad := mrOk;
end;

procedure TNomAjusteFonacotGridSelect_DevEx.ZetaDBGridDBTableViewColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  inherited;
  Self.AColumn := TcxGridDBColumn(AColumn);
end;

procedure TNomAjusteFonacotGridSelect_DevEx.ZetaDBGridDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
  AIndex: Integer;
begin
  inherited;
  AIndex := AValueList.FindItemByKind(fviCustom);
    if AIndex <> -1 then
      AValueList.Delete(AIndex);

   ZGridModeTools.BorrarItemGenericoAll( AValueList );
   if ZetaDBGridDBTableView.DataController.IsGridMode then
      ZGridModeTools.FiltroSetValueLista( ZetaDBGridDBTableView, AItemIndex, AValueList );

end;

end.

