unit FWizNomDeclaracionCreditoSalario_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, Mask,
     FWizEmpBaseFiltro,
     ZetaEdit,
     ZetaWizard,
     ZetaNumero, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl;

type
  TWizNomDeclaracionCreditoSalario_DevEx = class(TWizEmpBaseFiltro)
    Year: TZetaNumero;
    CreditoPagadoLBL: TLabel;
    IngresoExentoLBL: TLabel;
    IngresoBrutoLBL: TLabel;
    YearLBL: TLabel;
    BitBtn3: TcxButton;
    BitBtn2: TcxButton;
    BitBtn1: TcxButton;
    IngresoBruto: TcxMemo;
    IngresoExento: TcxMemo;
    CreditoPagado: TcxMemo;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure YearExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);//DevEx(by am): Agregado para enfocar correctamente los controles.
  private
    { Private declarations }
    procedure ValidaFechaReformaFiscal;
  protected
    { Protected declarations }
    function EjecutarWizard : boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizNomDeclaracionCreditoSalario_DevEx: TWizNomDeclaracionCreditoSalario_DevEx;

implementation

uses FTressShell,
     ZetaDialogo,
     ZConstruyeFormula,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaTipoEntidad,
     ZetaCommonLists,
     DCliente,
     DCatalogos,
     DProcesos;

{$R *.DFM}

procedure TWizNomDeclaracionCreditoSalario_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     Year.Valor := ( dmCliente.YearDefault - 1 );
     IngresoBruto.Tag := K_GLOBAL_DEF_CRED_SAL_ING_BRUTO;
     IngresoExento.Tag := K_GLOBAL_DEF_CRED_SAL_ING_EXENTO;
     CreditoPagado.Tag := K_GLOBAL_DEF_CRED_SAL_PAGADO;
     ParametrosControl := Year;
     ValidaFechaReformaFiscal;
     HelpContext := H33511_Declaracion_de_credito_al_salario;

     //DevEx(by am): Es necesario especificar que Ejecucion sera la ultima pagina del wizard.
     Parametros.PageIndex := WizardControl.PageCount - 3;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 2;
     Ejecucion.PageIndex := WizardControl.PageCount - 1;
end;

procedure TWizNomDeclaracionCreditoSalario_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     ParametrosControl := nil;
     if CanMove then
     begin
          with Wizard do
          begin
               if EsPaginaActual( Parametros ) and Adelante then
               begin
                    if StrVacio( IngresoBruto.Text ) then
                    begin
                         zError( Caption, '� F�rmula de Ingreso Bruto no fu� especificada !', 0 );
                         CanMove := False;
                         ActiveControl := IngresoBruto;
                    end
                    else
                    if StrVacio( IngresoExento.Text ) then
                    begin
                         zError( Caption, '� F�rmula de Ingreso Exento no fu� especificada !', 0 );
                         CanMove := False;
                         ActiveControl := IngresoExento;
                    end
                    else
                    if StrVacio( CreditoPagado.Text ) then
                    begin
                         if ( Year.Valor >= ZetaCommonClasses.K_REFORMA_FISCAL_2008 ) then
                            zError( Caption, '� F�rmula de Subsidio al Empleo Pagado no fu� especificada !', 0 )
                         else
                             zError( Caption, '� F�rmula de Cr�dito al Salario Pagado no fu� especificada !', 0 );
                         CanMove := False;
                         ActiveControl := CreditoPagado;
                    end
                    else
                    if ( Year.ValorEntero <= 0 ) then
                    begin
                         zError( Caption, '� A�o no fu� especificado !', 0 );
                         CanMove := False;
                         ActiveControl := Year;
                    end;
               end;
          end;
     end;
     inherited;
end;

procedure TWizNomDeclaracionCreditoSalario_DevEx.BitBtn1Click(Sender: TObject);
begin
     IngresoBruto.Text:= GetFormulaConst( enEmpleado , IngresoBruto.Text, IngresoBruto.SelStart, evBase );
end;

procedure TWizNomDeclaracionCreditoSalario_DevEx.BitBtn2Click(Sender: TObject);
begin
     IngresoExento.Text:= GetFormulaConst( enEmpleado , IngresoExento.Text, IngresoExento.SelStart,evBase );
end;

procedure TWizNomDeclaracionCreditoSalario_DevEx.BitBtn3Click(Sender: TObject);
begin
     CreditoPagado.Text:= GetFormulaConst( enEmpleado , CreditoPagado.Text, CreditoPagado.SelStart, evBase );
end;

procedure TWizNomDeclaracionCreditoSalario_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with Descripciones do
          AddInteger( 'A�o', Year.ValorEntero );

     with ParameterList do
     begin
          AddInteger( 'Year', Year.ValorEntero );
          AddString( 'IngresoBruto', IngresoBruto.Text );
          AddString( 'IngresoExento', IngresoExento.Text );
          AddString( 'CreditoPagado', CreditoPagado.Text );
     end;
end;

function TWizNomDeclaracionCreditoSalario_DevEx.EjecutarWizard;
begin
     Result := dmProcesos.DeclaraCredito( ParameterList );
end;

{* Cambio por reforma fiscal del 2008. Si es igual o mayor a la constante
 K_REFORMA_FISCAL_2008 se hacen cambio en cierto textos *}
procedure TWizNomDeclaracionCreditoSalario_DevEx.ValidaFechaReformaFiscal;
begin
     if ( Year.Valor >= ZetaCommonClasses.K_REFORMA_FISCAL_2008 ) then
     begin
          self.Caption := 'Calcular Declaraci�n de Subsidio al Empleo';
          CreditoPagadoLBL.Caption := 'S&ubsidio al Empleo Pagado:';
          Parametros.Header.Title := 'Declaraci�n de subsidio al empleo';
     end
     else
     begin
          self.Caption := 'Calcular Declaraci�n de Cr�dito al Salario';
          CreditoPagadoLBL.Caption := 'C&r�dito al Salario Pagado:';
          Parametros.Header.Title := 'Declaraci�n de Cr�dito al Salario';
     end;
end;

procedure TWizNomDeclaracionCreditoSalario_DevEx.YearExit(Sender: TObject);
begin
     inherited;
     ValidaFechaReformaFiscal; // Validacion Reforma Fiscal 2008
end;

procedure TWizNomDeclaracionCreditoSalario_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'El proceso genera la relaci�n de empleados y el monto que recibieron por subsidio al empleo en el a�o fiscal.';
end;

procedure TWizNomDeclaracionCreditoSalario_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.Adelante then
          if Wizard.EsPaginaActual( Parametros ) then
             ParametrosControl := Year;
     inherited;
end;

end.
