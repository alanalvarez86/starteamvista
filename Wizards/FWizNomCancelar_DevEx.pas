unit FWizNomCancelar_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, Mask,
     FWizEmpBaseFiltro,
     ZetaEdit,
     ZetaWizard,
     ZetaNumero,
     ZetaKeyCombo, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxContainer, cxEdit, Menus, ZetaCXWizard, cxRadioGroup, cxTextEdit,
  cxMemo, ZetaKeyLookup_DevEx, cxButtons, dxGDIPlusClasses, cxImage,
  cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl;

type
  TWizNomCancelar_DevEx = class(TWizEmpBaseFiltro)
    NominaNueva: TGroupBox;
    YearNuevoLBL: TLabel;
    NumeroNuevoLBL: TLabel;
    YearNuevo: TZetaNumero;
    NumeroNuevo: TZetaNumero;
    NominaOriginal: TGroupBox;
    YearOriginalLBL: TLabel;
    NumeroOriginalLBL: TLabel;
    YearOriginal: TZetaNumero;
    NumeroOriginal: TZetaNumero;
    TipoNomina: TZetaKeyCombo;
    TipoLBL: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);//DevEx(by am): Agregado para enfocar correctamente los controles.
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure CargaParametros;override;
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizNomCancelar_DevEx: TWizNomCancelar_DevEx;

implementation

uses DCliente,
     DProcesos,
     FTressShell,
     ZetaDialogo,
     ZetaCommonLists,
     ZetaCommonClasses;

{$R *.DFM}

procedure TWizNomCancelar_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     with dmCliente.GetDatosPeriodoActivo do
     begin
       //   TipoNomina.ItemIndex := Ord( Tipo );
          TipoNomina.Llave := IntToStr( ord( Tipo ) );  //acl
          YearOriginal.Valor := Year;
          YearNuevo.Valor := Year;
          NumeroOriginal.Valor := Numero;
     end;
     ParametrosControl := TipoNomina;
     HelpContext:= H33414_Cancelar_nominas_pasadas;

     //DevEx(by am): Es necesario especificar que Ejecucion sera la ultima pagina del wizard.
     Parametros.PageIndex := WizardControl.PageCount - 3;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 2;
     Ejecucion.PageIndex := WizardControl.PageCount - 1;
end;

procedure TWizNomCancelar_DevEx.CargaParametros;
begin
     inherited;
     with Descripciones do
     begin
          AddInteger( 'Tipo', TipoNomina.LlaveEntero);//acl
          AddInteger( 'A�o Original' , YearOriginal.ValorEntero);
          AddInteger( 'N�mero Original',NumeroOriginal.ValorEntero);
          AddInteger( 'A�o Nuevo', YearNuevo.ValorEntero);
          AddInteger( 'N�mero Nuevo', NumeroNuevo.ValorEntero);
     end;
     with ParameterList do
     begin
//          AddInteger( 'Tipo', TipoNomina.ItemIndex);
          AddInteger( 'Tipo', TipoNomina.LlaveEntero);//acl
          AddInteger( 'YearOriginal' , YearOriginal.ValorEntero);
          AddInteger( 'NumeroOriginal',NumeroOriginal.ValorEntero);
          AddInteger( 'YearNuevo', YearNuevo.ValorEntero);
          AddInteger( 'NumeroNuevo', NumeroNuevo.ValorEntero);
          AddBoolean( 'CancelarNomina', TRUE );
     end;
end;

procedure TWizNomCancelar_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     ParametrosControl := nil;
     with Wizard do
     begin
          if Adelante and EsPaginaActual( Parametros ) then
          begin
               if TipoNomina.ItemIndex > K_PERIODO_VACIO then
               begin
                    if ( YearOriginal.ValorEntero = YearNuevo.ValorEntero ) and ( NumeroOriginal.ValorEntero = NumeroNuevo.ValorEntero ) then
                    begin
                         zError( Caption, '� La N�mina Nueva Debe Ser Diferente A La N�mina Original !', 0 );
                         CanMove := False;
                         ActiveControl := NumeroNuevo;
                    end
               end
               else
               begin
                    zError( Caption, 'El tipo de n�mina es invalido', 0 );
                    CanMove := False;
               end
          end;
     end;
     inherited;
end;

function TWizNomCancelar_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CopiarNomina(ParameterList);
end;

procedure TWizNomCancelar_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with TipoNomina do
     begin
          ListaFija:=lfTipoPeriodoConfidencial; //acl
          LlaveEntero := Ord( dmCliente.GetDatosPeriodoActivo.Tipo );
     end;
     Advertencia.Caption := 'El proceso depositar� las excepciones en la n�mina de cancelaci�n para los empleados seleccionados. Para que tenga efecto en los acumulados, esta n�mina se deber� afectar.';
end;

procedure TWizNomCancelar_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.Adelante then
          if Wizard.EsPaginaActual( Parametros ) then
             ParametrosControl := TipoNomina;
     inherited;
end;

end.
