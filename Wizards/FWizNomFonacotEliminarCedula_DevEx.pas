unit FWizNomFonacotEliminarCedula_DevEx;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ZcxBaseWizard, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
  cxContainer, cxEdit, ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel,
  cxGroupBox, dxCustomWizardControl, dxWizardControl, Vcl.ExtCtrls,
  ZetaKeyLookup_DevEx, cxDropDownEdit, ZetaCXStateComboBox, cxTextEdit,
  cxMaskEdit, cxSpinEdit, Vcl.StdCtrls, ZetaKeyCombo, DB;

type
  TWizNomFonacotEliminarCedula_DevEx = class(TcxBaseWizard)
    Label2: TLabel;
    txtAnio: TcxSpinEdit;
    Label3: TLabel;
    zcboMes: TcxStateComboBox;
    lbRazonsocial: TLabel;
    ListaRSCedulasImportadasCB_DevEx: TZetaKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure zcboMesClick(Sender: TObject);
    procedure txtAnioClick(Sender: TObject);
  private
    { Private declarations }
    procedure HabilitaFiltroRazonSocial;
    procedure LlenaListaCampos( DataSet: TDataSet );
    procedure CargarComboRSCedulas( const sYear: string; const sMonth: string);
    procedure LimpiarComboRSCedulas;
  protected
    { Protected declarations }
    procedure CargaParametros; override;
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizNomFonacotEliminarCedula_DevEx: TWizNomFonacotEliminarCedula_DevEx;

  sAnioFonacot, sMesFonacot : string;

implementation

{$R *.dfm}

uses dCatalogos, DNomina, dCliente, dProcesos, ZetaDialogo,
     ZetaCommonClasses,
     ZetaCommonLists;

procedure TWizNomFonacotEliminarCedula_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := txtAnio;
     HelpContext:= H_WIZ_ELIMINAR_CEDULA_FONACOT;

     dmCatalogos.cdsRSocial.Conectar;
     LimpiarComboRSCedulas;
     CargarComboRSCedulas(sAnioFonacot,sMesFonacot);

     HabilitaFiltroRazonSocial;
end;

procedure TWizNomFonacotEliminarCedula_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          txtAnio.Value := GetDatosPeriodoActivo.Year;
          zcboMes.ItemIndex := GetDatosPeriodoActivo.Mes - 1;
          if zcboMes.ItemIndex = -1 then
             zcboMes.ItemIndex := ord(emEnero);
     end;

     LimpiarComboRSCedulas;
     CargarComboRSCedulas(sAnioFonacot,sMesFonacot);
end;

procedure TWizNomFonacotEliminarCedula_DevEx.HabilitaFiltroRazonSocial;
var
   lHabilitarFiltro : Boolean;
begin
     lHabilitarFiltro := False;
     with dmCatalogos.cdsRSocial do
     begin
          lHabilitarFiltro := (RecordCount > 1)
     end;

     ListaRSCedulasImportadasCB_DevEx.Enabled := lHabilitarFiltro;
     lbRazonSocial.Enabled := lHabilitarFiltro;
end;

procedure TWizNomFonacotEliminarCedula_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   sRazon : String;
begin
     inherited;
     sRazon := Vacio;
     with Wizard do
     begin
          if Adelante then
          begin
               if EsPaginaActual( Parametros ) then
               begin
                    if Not (ListaRSCedulasImportadasCB_DevEx.Descripcion = 'Todas') then
                       sRazon := ListaRSCedulasImportadasCB_DevEx.Llave;

                    if dmProcesos.GetConteoCedula( IntToStr (txtAnio.Value) , IntToStr(zcboMes.ItemIndex + 1), sRazon  ) > 0 then
                    begin
                         if dmProcesos.GetConteoCedulaAfectada( IntToStr (txtAnio.Value) , IntToStr(zcboMes.ItemIndex + 1), sRazon ) > 0 then
                         begin
                              CanMove := False;
                              ZetaDialogo.zWarning( Caption, 'No es posible eliminar registros para la ' + CR_LF + 'c�dula seleccionada debido a que cuenta con deducciones.', 0, mbOK );
                         end
                         else
                             CanMove := TRUE;
                    end
                    else
                    begin
                         CanMove := False;
                         ZetaDialogo.zWarning( Caption, 'No hay registros por eliminar.', 0, mbOK );
                    end;
               end
          end
     end;
end;

procedure TWizNomFonacotEliminarCedula_DevEx.zcboMesClick(Sender: TObject);
begin
     inherited;
     CargarComboRSCedulas(sAnioFonacot,sMesFonacot);
end;

procedure TWizNomFonacotEliminarCedula_DevEx.CargarComboRSCedulas( const sYear: string; const sMonth: string);
var
   iConteoRS : integer;
begin
      try
           sAnioFonacot := txtAnio.Value;
           sMesFonacot := IntToStr(zcboMes.Indice);
           if ((sAnioFonacot <>  VACIO) and (sMesFonacot <>  VACIO)) then
           begin
                 ListaRSCedulasImportadasCB_DevEx.Enabled := true;
                 dmCatalogos.GetDatosRSCedulasImportadas( sAnioFonacot, sMesFonacot );
                 iConteoRS := dmCatalogos.cdsRSCedulasImportadas.RecordCount;
                 if iConteoRS > 0 then
                 begin
                      LlenaListaCampos( dmCatalogos.cdsRSCedulasImportadas );
                      ListaRSCedulasImportadasCB_DevEx.ItemIndex := 0;
                 end
                 else
                 begin
                       ListaRSCedulasImportadasCB_DevEx.Enabled := false;
                       ListaRSCedulasImportadasCB_DevEx.Items.Clear;
                       ListaRSCedulasImportadasCB_DevEx.Clear;
                 end;
           end
           else
           begin
                ListaRSCedulasImportadasCB_DevEx.Enabled := false;
                ListaRSCedulasImportadasCB_DevEx.Items.Clear;
                ListaRSCedulasImportadasCB_DevEx.Clear;
           end;
      finally

      end;
end;

procedure TWizNomFonacotEliminarCedula_DevEx.txtAnioClick(Sender: TObject);
begin
     inherited;
     LimpiarComboRSCedulas;
     CargarComboRSCedulas(sAnioFonacot,sMesFonacot);
end;

function TWizNomFonacotEliminarCedula_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.EliminarCedulaFonacot ( ParameterList );
end;

procedure TWizNomFonacotEliminarCedula_DevEx.LimpiarComboRSCedulas;
begin
     try
           ListaRSCedulasImportadasCB_DevEx.Items.Clear;
           ListaRSCedulasImportadasCB_DevEx.Clear;
     except
     end;
end;

procedure TWizNomFonacotEliminarCedula_DevEx.LlenaListaCampos( DataSet: TDataSet );
var
   Pos : TBookMark;
   iContador : integer;
   sContador, sDescripcion : string;
begin
     ListaRSCedulasImportadasCB_DevEx.Items.Clear;
     with ListaRSCedulasImportadasCB_DevEx do
     begin
          Lista.BeginUpdate;
          Clear;
          ListaRSCedulasImportadasCB_DevEx.Lista.Clear;
     end;
     iContador := 1;
     sContador := IntToStr(iContador);
     with DataSet do
     begin
          DisableControls;
          try
             ListaRSCedulasImportadasCB_DevEx.Items.Clear;
             ListaRSCedulasImportadasCB_DevEx.Clear;

             Pos:= GetBookMark;
             First;
             while ( not EOF ) do
             begin
                 with ListaRSCedulasImportadasCB_DevEx.Lista do
                 begin
                      sDescripcion := VACIO;
                      if (FieldByName( 'CODIGO' ).AsString <> VACIO) then
                      BEGIN
                           sDescripcion := FieldByName( 'CODIGO'  ).AsString +' - '+ FieldByName( 'NOMBRE' ).AsString;
                           Add( FieldByName( 'CODIGO'  ).AsString +'='+ sDescripcion  )
                      END
                      ELSE
                      BEGIN
                           Add( sContador +'='+ 'Todas'  )
                      END;
                 end;
                 iContador := iContador + 1;
                 sContador := IntToStr(iContador);
                 Next;
             end;
             if ( Pos <> nil ) then
             begin
                  GotoBookMark( Pos );
                  FreeBookMark( Pos );
             end;
          finally
                EnableControls;
                ListaRSCedulasImportadasCB_DevEx.Lista.EndUpdate;
          end;
     end;
end;

procedure TWizNomFonacotEliminarCedula_DevEx.CargaParametros;
var
    sValueComboRSCedulas : string;
begin
     sValueComboRSCedulas := '-1';
     if (ListaRSCedulasImportadasCB_DevEx.Text = 'Todas') then
     begin
          sValueComboRSCedulas := VACIO;
     end
     else
     begin
          sValueComboRSCedulas := ListaRSCedulasImportadasCB_DevEx.Llave;
     end;

     with ParameterList do
     begin
          AddInteger ('Year', txtAnio.Value);
          AddInteger ('Mes', zcboMes.ItemIndex + 1 );
          AddString( 'RazonSocial', sValueComboRSCedulas );
     end;

     with Descripciones do
     begin
          AddInteger ('A�o', txtAnio.Value);
          AddString ('Mes', ObtieneElemento( lfMeses, zcboMes.ItemIndex ));
          AddString( 'Raz�n social', sValueComboRSCedulas );
     end;
end;

end.
