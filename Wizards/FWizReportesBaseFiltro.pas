unit FWizReportesBaseFiltro;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, ExtCtrls,
     ZetaDBTextBox, ZetaEdit, ZCXBaseWizardFiltro,
     cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
     cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons,
     dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
     dxWizardControl, Mask, ZetaNumero, ZetaKeyCombo, ZcxBaseWizard, ZetaHora,
     ZetaFecha, cxCheckGroup, System.DateUtils, System.MaskUtils;

type
  TWizReportesBaseFiltro = class(TcxBaseWizard)
    pDiario: TdxWizardControlPage;
    pEspecial: TdxWizardControlPage;
    pHora: TdxWizardControlPage;
    pMensual: TdxWizardControlPage;
    pSemanal: TdxWizardControlPage;
    fdFechaInicio: TZetaDBFecha;
    fdHora: TZetaDBHora;
    fdRepeticion: TZetaDBNumero;
    Label13: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label22: TLabel;
    lfdHora: TLabel;
    Label20: TLabel;
    feFechaInicio: TZetaDBFecha;
    feHora: TZetaDBHora;
    Label21: TLabel;
    Label24: TLabel;
    Label14: TLabel;
    fhFechaInicio: TZetaDBFecha;
    Label17: TLabel;
    fhHora: TZetaDBHora;
    Label18: TLabel;
    fhRepeticion: TZetaDBNumero;
    Label19: TLabel;
    Label23: TLabel;
    chkMDias: TcxCheckGroup;
    chkMDiasMes: TcxCheckGroup;
    chkMMeses: TcxCheckGroup;
    chkMNumero: TcxCheckGroup;
    fmFechaInicio: TZetaDBFecha;
    fmHora: TZetaDBHora;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label6: TLabel;
    chksFrecuenciaDias: TcxCheckGroup;
    fsFechaInicio: TZetaDBFecha;
    fsHora: TZetaDBHora;
    fsRepeticion: TZetaDBNumero;
    Label41: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    lfsFechaInicio: TLabel;
    lfsHora: TLabel;
    lfsRepetirse: TLabel;
    Label5: TLabel;
    cbFrecuencia: TZetaDBKeyCombo;
    rDiasMes: TcxRadioButton;
    rDiasSemana: TcxRadioButton;
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure rDiasMesClick(Sender: TObject);
    procedure rDiasSemanaClick(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
    procedure cbFrecuenciaChange(Sender: TObject);
  private
    { Private declarations }
  protected
    function VerificarTotalCasillas(var chkGrupo : TcxCheckGroup): Integer;
    function ValidarFechasMensual: Boolean;
    procedure ResetPaginas;
    function RegresarTotales(var chkGrupo: TcxCheckGroup): String;
    procedure MarcarCasillas(var chkGrupo: TcxCheckGroup; var sCadena: string);
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizReportesBaseFiltro: TWizReportesBaseFiltro;

implementation

uses DCliente,
     ZetaCommonLists, dCatalogos, ZetaDialogo, ZetaCommonClasses, ZetaCommonTools;

{$R *.DFM}

procedure TWizReportesBaseFiltro.cbFrecuenciaChange(Sender: TObject);
begin
     inherited;
     // Frecuencia Semanal
     fsFechaInicio.Valor := Date();
     fsHora.Text := FormatDateTime('hhnn', Now);
     fsRepeticion.Valor := 1;

     // Frecuencia Diario
     fdFechaInicio.Valor := Date();
     fdHora.Text := FormatDateTime('hhnn', Now);
     fdRepeticion.Valor := 1;

     // Frecuencia Mensual
     fmFechaInicio.Valor := Date();
     fmHora.Text := FormatDateTime('hhnn', Now);

     // Frecuencia Especial
     feFechaInicio.Valor := Date();
     // feHora.Text := VACIO;
     feHora.Text := FormatDateTime('hhnn', Now);

     // Frecuencia Hora
     fhFechaInicio.Valor := Date();
     fhHora.Text := FormatDateTime('hhnn', Now);
     fhRepeticion.Valor := 8;
end;

procedure TWizReportesBaseFiltro.rDiasMesClick(Sender: TObject);
begin
     inherited;
     chkMDiasMes.Enabled := rDiasMes.Checked;
     chkMNumero.Enabled := Not rDiasMes.Checked;
     chkMDias.Enabled := Not rDiasMes.Checked;
end;

procedure TWizReportesBaseFiltro.rDiasSemanaClick(Sender: TObject);
begin
     chkMDiasMes.Enabled := Not rDiasSemana.Checked;
     chkMNumero.Enabled := rDiasSemana.Checked;
     chkMDias.Enabled := rDiasSemana.Checked;
end;

procedure TWizReportesBaseFiltro.WizardAfterMove(Sender: TObject);
begin
     inherited;
     if (WizardControl.ActivePage = pSemanal) then
             ActiveControl := fsFechaInicio
     else
     begin
          if (WizardControl.ActivePage = pMensual) then
             ActiveControl := fmFechaInicio
          else
          begin
               if (WizardControl.ActivePage = pDiario) then
                  ActiveControl := fdFechaInicio
               else
               begin
                    if (WizardControl.ActivePage = pHora) then
                       ActiveControl := fhFechaInicio
                    else
                    begin
                         if (WizardControl.ActivePage = pEspecial) then
                            ActiveControl := feFechaInicio
                    end
               end
          end
     end;
end;

procedure TWizReportesBaseFiltro.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     if CanMove then
     begin
          if Wizard.Adelante and Wizard.EsPaginaActual(pSemanal) then
          begin
               if StrVacio(fsHora.Text) then
                  CanMove := Error('La hora no puede ir vac�a.', fsHora)
               else
               begin
                    if VerificarTotalCasillas(chksFrecuenciaDias) = 0 then
                       CanMove := Error('Se debe indicar al menos 1 d�a para realizar el env�o.', chksFrecuenciaDias)
                    else
                    begin
                         if (fsRepeticion.Valor < 1) or (fsRepeticion.Valor > 99) then
                            CanMove := Error('El valor de la repetici�n debe ser entre 1 y 99.', fsRepeticion)
                    end
               end;
          end
          else if Wizard.Adelante and Wizard.EsPaginaActual(pHora) then
          begin
               if StrVacio(fhHora.Text) then
                  CanMove := Error('La hora no puede ir vac�a.', fhHora)
               else
               begin
                    if (fhRepeticion.Valor < 1) or (fhRepeticion.Valor > 99) then
                       CanMove := Error('El valor de la repetici�n debe ser entre 1 y 99.', fhRepeticion)
               end;
          end
          else if Wizard.Adelante and Wizard.EsPaginaActual(pDiario) then
          begin
               if StrVacio(fdHora.Text) then
                  CanMove := Error('La hora no puede ir vac�a.', fdHora)
               else
               begin
                    if (fdRepeticion.Valor < 1) or (fdRepeticion.Valor > 99) then
                       CanMove := Error('El valor de la repetici�n debe ser entre 1 y 99.', fdRepeticion)
               end;
          end
          else if Wizard.Adelante and Wizard.EsPaginaActual(pEspecial) then
          begin
               if StrVacio(feHora.Text) then
                  CanMove := Error('La hora no puede ir vac�a.', feHora)
               else if feFechaInicio.Valor < Date() then
                    CanMove := Error('No se puede programar el env�o con una fecha menor a la de hoy.', feFechaInicio)
               else
               begin
                    if ((MinutesBetween (StrToTime(feHora.EditText), StrToTime(FormatDateTime('hh:nn', Now))) < 60) and (feFechaInicio.Valor = Date()) or
                       (feHora.Horas < StrToInt(FormatDateTime('hh', Now)) ) and (feFechaInicio.Valor = Date())) then
                       CanMove := Error('La hora indicada debe ser una hora mayor que la hora actual del sistema' + CR_LF + 'para que le sea posible al mecanismo programar el env�o.', feHora)
               end;
          end
          else if Wizard.Adelante and Wizard.EsPaginaActual(pMensual) then
          begin
               if StrVacio(fmHora.Text) then
                  CanMove := Error('La hora no puede ir vac�a.', fmHora)
               else
               begin
                    if VerificarTotalCasillas(chkMMeses) = 0 then
                       CanMove := Error('Se debe indicar al menos 1 mes para realizar el env�o.', chkMMeses)
                    else
                    begin
                         if (rDiasMes.Checked) and (VerificarTotalCasillas(chkMDiasMes) = 0) then
                            CanMove := Error('Se debe indicar al menos 1 d�a al mes para realizar el env�o.', chkMDiasMes)
                         else
                         begin
                              if (rDiasSemana.Checked) and (VerificarTotalCasillas(chkMNumero) = 0) then
                                 CanMove := Error('Se debe indicar al menos 1 d�a por semana para realizar el env�o.', chkMNumero)
                              else
                              begin
                                   if (rDiasSemana.Checked) and (VerificarTotalCasillas(chkMDias) = 0) then
                                      CanMove := Error('Se debe indicar al menos 1 d�a por semana para realizar el env�o.', chkMDias)
                                   else
                                   begin
                                        if ((rDiasMes.Checked)) and not ValidarFechasMensual then
                                           CanMove := Error('Has seleccionado un d�a que no es parte de los meses seleccionados.'
                                           + CR_LF + 'Debes incluir al menos 1 mes que tenga todos los d�as seleccionados.', chkMDiasMes)
                                   end;
                              end;
                         end;
                    end
               end
          end
     end;

     if Wizard.Adelante then
     begin
          if CanMove then
          begin
               if Wizard.EsPaginaActual(Parametros) then
               begin
                    ResetPaginas;
                    case cbFrecuencia.ItemIndex of
                         ord(frDiario):
                         begin
                              pDiario.PageVisible := True;
                              pDiario.PageIndex := 1;
                              iNewPage := pDiario.PageIndex;
                         end;
                         ord(frSemanal):
                         begin
                              pSemanal.PageVisible := True;
                              pSemanal.PageIndex := 1;
                              iNewPage := pDiario.PageIndex;
                         end;
                         ord(frMensual):
                         begin
                              pMensual.PageVisible := True;
                              pMensual.PageIndex := 1;
                              iNewPage := pMensual.PageIndex;
                         end;
                         ord(frCadaHora):
                         begin
                              pHora.PageVisible := True;
                              pHora.PageIndex := 1;
                              iNewPage := pHora.PageIndex;
                         end;
                         ord(frEspecial):
                         begin
                              pEspecial.PageVisible := True;
                              pEspecial.PageIndex := 1;
                              iNewPage := pEspecial.PageIndex;
                         end;
                    end
               end;
          end;
     end;
 end;


procedure TWizReportesBaseFiltro.CargaParametros;
var
   sFecha, sHora, sDias, sDiasMes, sSemanaMes, sMeses: String;
   dFechaInicio, dFechaEspecial: TDateTime;
   iRecurrencia, iTipoMes: Integer;
begin
     inherited CargaParametros;
     iRecurrencia := 0;
     dFechaInicio := Date();
     iTipoMes := 0;

     case cbFrecuencia.ItemIndex of
          ord(frDiario):
          begin
               sHora := fdHora.Text;
               sFecha := fdFechaInicio.Texto;
               dFechaInicio := fdFechaInicio.Valor;
               iRecurrencia := fdRepeticion.ValorEntero;
          end;
          ord(frSemanal):
          begin
               sHora := fsHora.Text;
               sFecha := fsFechaInicio.Texto;
               dFechaInicio := fsFechaInicio.Valor;
               iRecurrencia := fsRepeticion.ValorEntero;
               sDias := RegresarTotales(chksFrecuenciaDias);
          end;
          ord(frMensual):
          begin
               sHora := fmHora.Text;
               sFecha := fmFechaInicio.Texto;
               dFechaInicio := fmFechaInicio.Valor;
               sMeses := RegresarTotales(chkMMeses);
               if rDiasMes.Checked then
               begin
                    sDiasMes := RegresarTotales(chkMDiasMes);
                    iTipoMes := 1;
               end
               else
               begin
                    sDias := RegresarTotales(chkMDias);
                    sSemanaMes := RegresarTotales(chkMNumero);
                    iTipoMes := 2;
               end;
          end;
          ord(frCadaHora):
          begin
               sHora := fhHora.Text;
               sFecha := fhFechaInicio.Texto;
               dFechaInicio := fhFechaInicio.Valor;
               iRecurrencia := fhRepeticion.ValorEntero;
          end;
          ord(frEspecial):
          begin
               sHora := feHora.Text;
               sFecha := feFechaInicio.Texto;
               dFechaInicio := feFechaInicio.Valor;
          end;
     end;

     with Descripciones do
     begin
          AddString('Frecuencia', ObtieneElemento(lfEmailFrecuencia, cbFrecuencia.ItemIndex));
          AddString('Fecha de inicio del env�o', sFecha);
          AddString('Hora', FormatMaskText('99:99;0', shora));
     end;

     with ParameterList do
     begin
          // AddDate('FechaInicio', dFechaInicio);
          AddDateTime('FechaInicio', StrToDateTime(fdFechaInicio.ValorAsText + MaskHora (fdHora.Valor)));
          AddString('Hora', sHora);
          AddInteger('Frecuencia', cbFrecuencia.ItemIndex);
          AddInteger('Recurrencia', iRecurrencia);
          AddString('Meses', sMeses);
          AddString('DiasMes', sDiasMes);
          AddString('SemanaMes', sSemanaMes);
          AddString('Dias', sDias);
          AddInteger('TipoMes', iTipoMes);

          if cbFrecuencia.ItemIndex = ord(frEspecial) then
             AddDate('FechaEspecial', dFechaInicio)
          else
              AddDate('FechaEspecial', dFechaEspecial);
     end;
end;


function TWizReportesBaseFiltro.VerificarTotalCasillas (var chkGrupo: TcxCheckGroup): Integer;
var
   i: Integer;
begin
     Result := 0;
     with chkGrupo do
     begin
          for i := 0 to Properties.Items.Count - 1 do
              if States[i] = cbsChecked then
                 Result := Result + 1;
     end;
end;

function TWizReportesBaseFiltro.RegresarTotales(var chkGrupo: TcxCheckGroup): String;
var
   i: Integer;
begin
     with chkGrupo do
     begin
          for i := 0 to Properties.Items.Count - 1 do
              if States[i] = cbsChecked then
              begin
                   if StrVacio(Result) then
                      Result := IntToStr(Properties.Items[i].Tag)
                   else
                       Result := Result + ',' + IntToStr(Properties.Items[i].Tag);
              end;
     end;
end;

procedure TWizReportesBaseFiltro.MarcarCasillas(var chkGrupo: TcxCheckGroup; var sCadena : string);
var
   i,j : Integer;
   Lista : TStringList;
begin
     Lista := TStringList.Create;
     try
        Lista.CommaText  := sCadena;

        with chkGrupo do
        begin
             for i := 0 to Properties.Items.Count - 1 do
             begin
                  for j := 0 to Lista.Count - 1 do
                       if ( Properties.Items[i].Tag ) = StrToInt( Lista[j] ) then
                          States[i] := cbsChecked ;
             end;
        end;
     finally
            Lista.Free;
     end;
end;

procedure TWizReportesBaseFiltro.ResetPaginas;
begin
     pSemanal.PageVisible := False;
     pMensual.PageVisible := False;
     pDiario.PageVisible := False;
     pHora.PageVisible := False;
     pEspecial.PageVisible := False;
end;


function TWizReportesBaseFiltro.ValidarFechasMensual: Boolean;
// DIA_LIMITE.
// Si un dia es duperior a DIA_LIMITE, debe validarse.
const K_DIA_LIMITE = 29;
      K_CERO = 0;
var iDias, iMeses: Integer;
begin
     // Funci�n para validar d�as 30 y 31.
     // Esto porque estos n�meros de d�a podr�an ocasionar problema para generar fechas.
     // Validar que los d�as marcados existan para los meses seleccionados.

     Result := TRUE;
     for iDias := 0 to chkMDiasMes.Properties.Items.Count - 1 do
     begin
          if chkMDiasMes.States[iDias] = cbsChecked then
          begin
               if chkMDiasMes.Properties.Items[iDias].Tag > K_DIA_LIMITE then
               begin
                    Result := FALSE;
                    for iMeses := 0 to chkMMeses.Properties.Items.Count - 1 do
                    begin
                         if chkMMeses.States[iMeses] = cbsChecked then
                         begin
                              if (CodificaFecha (dmCliente.YearDefault,
                                 chkMMeses.Properties.Items[iMeses].Tag, chkMDiasMes.Properties.Items[iDias].Tag) <> NullDateTime) then
                              begin
                                   Result := TRUE;
                                   break;
                              end;
                         end;
                    end;
               end
               else
               begin
                    if (chkMDiasMes.Properties.Items[iDias].Tag = K_CERO) then
                    begin
                          Result := TRUE;
                          break;
                    end;
               end;
          end;
     end;
end;


end.
