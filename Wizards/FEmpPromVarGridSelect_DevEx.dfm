inherited EmpPromVarGridSelect_DevEx: TEmpPromVarGridSelect_DevEx
  Left = 718
  Top = 320
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Seleccione Los Empleados Deseados'
  ClientWidth = 492
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 492
    inherited OK_DevEx: TcxButton
      Left = 328
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 407
      Cancel = True
    end
  end
  inherited PanelSuperior: TPanel
    Width = 492
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 492
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsCustomize.ColumnHorzSizing = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.Indicator = True
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object CB_PER_VAR: TcxGridDBColumn
        Caption = 'Prom. Actual'
        DataBinding.FieldName = 'CB_PER_VAR'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 73
      end
      object NUEVA_PERC: TcxGridDBColumn
        Caption = 'Prom. Nuevo'
        DataBinding.FieldName = 'NUEVA_PERC'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 76
      end
      object DIFERENCIA: TcxGridDBColumn
        Caption = 'Diferencia'
        DataBinding.FieldName = 'DIFERENCIA'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object PORCENTAJE: TcxGridDBColumn
        Caption = 'Porcentaje'
        DataBinding.FieldName = 'PORCENTAJE'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
