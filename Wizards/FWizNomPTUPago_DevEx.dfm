inherited WizNomPTUPago_DevEx: TWizNomPTUPago_DevEx
  Left = 229
  Top = 110
  Caption = 'Pagar PTU'
  ClientHeight = 423
  ClientWidth = 439
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 439
    Height = 423
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Este proceso utiliza los montos calculados del proceso Calcular ' +
        'PTU para crear las excepciones en la n'#243'mina activa.'
      Header.Title = 'Pagar PTU'
      object YearLBL: TLabel [0]
        Left = 44
        Top = 16
        Width = 22
        Height = 13
        Alignment = taRightJustify
        Caption = '&A'#241'o:'
        FocusControl = Year
        Transparent = True
      end
      object ConceptoLBL: TLabel [1]
        Left = 17
        Top = 41
        Width = 49
        Height = 13
        Alignment = taRightJustify
        Caption = '&Concepto:'
        FocusControl = Concepto
        Transparent = True
      end
      inherited GroupBox1: TGroupBox
        Top = 73
        TabOrder = 2
      end
      object Year: TZetaNumero
        Left = 70
        Top = 12
        Width = 60
        Height = 21
        Mascara = mnDias
        TabOrder = 0
        Text = '0'
      end
      object Concepto: TZetaKeyLookup_DevEx
        Left = 70
        Top = 37
        Width = 300
        Height = 21
        Filtro = 'CO_NUMERO < 1000'
        LookupDataset = dmCatalogos.cdsConceptos
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 188
        Width = 417
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 417
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 349
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 14
        Top = 137
      end
      inherited sFiltroLBL: TLabel
        Left = 39
        Top = 166
      end
      inherited Seleccionar: TcxButton
        Left = 142
        Top = 254
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 66
        Top = 134
      end
      inherited sFiltro: TcxMemo
        Left = 66
        Top = 165
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 66
        Top = 4
      end
      inherited bAjusteISR: TcxButton
        Left = 380
        Top = 166
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
  end
end
