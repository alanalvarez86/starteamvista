unit FWizNomPrevioISR_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, ExtCtrls, Mask,
     ZetaKeyLookup_DevEx, ZetaEdit, ZetaNumero, ZetaKeyCombo,
     FWizNomBase_DevEx, cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
     TressMorado2013, dxSkinsDefaultPainters,
     cxContainer, cxEdit, Menus, ZetaCXWizard, cxRadioGroup, cxTextEdit,
     cxMemo, cxButtons, dxGDIPlusClasses, cxImage,
     cxLabel, cxGroupBox, ZetaDBTextBox, dxCustomWizardControl,
     dxWizardControl, cxCheckBox;

type
  TWizNomPrevioISR_DevEx = class(TWizNomBase_DevEx)
    YearLBL: TLabel;
    IngresoBrutoLBL: TLabel;
    IngresoExentoLBL: TLabel;
    ImpuestoRetenidoLBL: TLabel;
    CreditoPagadoLBL: TLabel;
    CalculoImpuestoLBL: TLabel;
    Year: TZetaNumero;
    IngresoBruto: TcxMemo;
    IngresoExento: TcxMemo;
    ImpuestoRetenido: TcxMemo;
    SUBEAplicado: TcxMemo;
    CalculoImpuesto: TcxMemo;
    BitBtn1: TcxButton;
    BitBtn2: TcxButton;
    BitBtn3: TcxButton;
    BitBtn4: TcxButton;
    BitBtn5: TcxButton;
    Rastrear: TcxCheckBox;
    lbEmpleadosRepetidos: TLabel;
    rgRepetidos: TcxRadioGroup;
    lbEmpleadosACalcular: TLabel;
    EmpleadosACalcular: TcxMemo;
    BitBtn6: TcxButton;
    Month: TZetaKeyCombo;
    nMonthLBL: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    //DevEx     //Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard : boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizNomPrevioISR_DevEx: TWizNomPrevioISR_DevEx;

implementation

uses ZetaDialogo,
     ZConstruyeFormula,
     ZGlobalTress,
     ZetaTipoEntidad,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists,
     DGlobal,
     DCliente,
     DProcesos,
     DCatalogos, ZcxWizardBasico;

{$R *.DFM}

procedure TWizNomPrevioISR_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     Year.Valor := ( dmCliente.YearDefault );
     IngresoBruto.Tag       := K_GLOBAL_DEF_PREVIO_ISR_INGRESO_BRUTO;
     IngresoExento.Tag      := K_GLOBAL_DEF_PREVIO_ISR_INGRESO_EXENTO;
     ImpuestoRetenido.Tag   := K_GLOBAL_DEF_PREVIO_ISR_IMPUESTO_RETENIDO;
     SUBEAplicado.Tag       := K_GLOBAL_DEF_PREVIO_ISR_SUBE_APLICADO;
     CalculoImpuesto.Tag    := K_GLOBAL_DEF_PREVIO_ISR_CALCULO_IMPUESTO;
     EmpleadosACalcular.Lines.Text := Global.GetGlobalString( K_GLOBAL_EMPLEADO_CON_AJUSTE );


     ParametrosControl := Year;
     Month.ItemIndex := (dmCliente.GetDatosPeriodoActivo.Mes-1);
     HelpContext := H_Calculo_Previo_ISR;
     rgRepetidos.ItemIndex := 0;
 
     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;
end;

procedure TWizNomPrevioISR_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     ParametrosControl := nil;  //DevEx
     inherited;                        
     if CanMove and Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
     begin
          with Wizard do
          begin
               if StrVacio( IngresoBruto.Text ) then
               begin
                    zError( Caption, '� F�rmula de Ingreso Bruto no fu� especificada !', 0 );
                    CanMove := False;
                    ActiveControl := IngresoBruto;
               end
               else
               if StrVacio( IngresoExento.Text ) then
               begin
                    zError( Caption, '� F�rmula de Ingreso Exento no fu� especificada !', 0 );
                    CanMove := False;
                    ActiveControl := IngresoExento;
               end
               else
               if StrVacio( ImpuestoRetenido.Text ) then
               begin
                    zError( Caption, '� F�rmula de Impuesto Retenido no fu� especificada !', 0 );
                    CanMove := False;
                    ActiveControl := ImpuestoRetenido;
               end
               else
               if StrVacio( SUBEAplicado.Text ) then
               begin
                    zError( Caption, '� F�rmula de Subsidio al Empleo Pagado no fu� especificada !', 0 );
                    CanMove := False;
                    ActiveControl := SUBEAplicado;
               end
               else
               if StrVacio( CalculoImpuesto.Text ) then
               begin
                    zError( Caption, '� F�rmula de C�lculo de Impuesto no fu� especificada !', 0 );
                    CanMove := False;
                    ActiveControl := CalculoImpuesto;
               end
               else
               if ( Year.ValorEntero <= 0 ) then
               begin
                    zError( Caption, '� A�o no fu� especificado !', 0 );
                    CanMove := False;
                    ActiveControl := Year;
               end
               else
               if ( Month.Valor <= 0 ) then
               begin
                    zError( Caption, '� Mes no fu� especificado !', 0 );
                    CanMove := False;
                    ActiveControl := Month;
               end
          end;
     end;
end;

procedure TWizNomPrevioISR_DevEx.BitBtn1Click(Sender: TObject);
begin
     IngresoBruto.Text:= GetFormulaConst( enEmpleado , IngresoBruto.Text, IngresoBruto.SelStart, evISPTAnual );
end;

procedure TWizNomPrevioISR_DevEx.BitBtn2Click(Sender: TObject);
begin
     IngresoExento.Text:= GetFormulaConst( enEmpleado , IngresoExento.Text, IngresoExento.SelStart, evISPTAnual );
end;

procedure TWizNomPrevioISR_DevEx.BitBtn3Click(Sender: TObject);
begin
     ImpuestoRetenido.Text:= GetFormulaConst( enEmpleado , ImpuestoRetenido.Text, ImpuestoRetenido.SelStart, evISPTAnual );
end;

procedure TWizNomPrevioISR_DevEx.BitBtn4Click(Sender: TObject);
begin
     SUBEAplicado.Text:= GetFormulaConst( enEmpleado , SUBEAplicado.Text, SUBEAplicado.SelStart, evISPTAnual );
end;

procedure TWizNomPrevioISR_DevEx.BitBtn5Click(Sender: TObject);
begin
     CalculoImpuesto.Text:= GetFormulaConst( enEmpleado , CalculoImpuesto.Text, CalculoImpuesto.SelStart, evISPTAnual );
end;

procedure TWizNomPrevioISR_DevEx.CargaParametros;
 function GetRepetidoEnum( const iRepetido: integer ): integer;
 begin
      case rgRepetidos.ItemIndex of
           1: Result := ord( erIncluirTodos );
           2: Result := ord( erNoIncluir )
           else Result := ord( erSumarUltimo )
      end;
 end;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddInteger( 'Year', Year.ValorEntero );
          AddInteger( 'Mes', Month.Valor );
          AddString( 'IngresoBruto', IngresoBruto.Text );
          AddString( 'IngresoExento', IngresoExento.Text );
          AddString( 'SUBEAplicado', SUBEAplicado.Text );
          AddString( 'ImpuestoRetenido', ImpuestoRetenido.Text );
          AddSTring( 'CalculoImpuesto', CalculoImpuesto.Text );
          AddSTring( 'EmpleadosACalcular', EmpleadosACalcular.Text );
          AddBoolean( 'Rastrear', Rastrear.Checked );
          AddInteger( 'Repetidos', GetRepetidoEnum( rgRepetidos.ItemIndex ) );
     end;

     with Descripciones do
     begin
          AddInteger( 'A�o', Year.ValorEntero );
          AddString( 'Mes',  ObtieneElemento( lfMeses, Month.ItemIndex) );
          AddString( 'Ingreso bruto', IngresoBruto.Text );
          AddString( 'Ingreso exento', IngresoExento.Text );
          AddString( 'Impuesto retenido', ImpuestoRetenido.Text );
          AddString( 'SUBE aplicado', SUBEAplicado.Text );
          AddSTring( 'C�lculo impuesto', CalculoImpuesto.Text );
          AddSTring( 'Empleados a calcular', EmpleadosACalcular.Text );
          AddBoolean( 'Rastrear c�lculos', Rastrear.Checked );
          AddString( 'Empleados repetidos', rgRepetidos.Properties.Items.Items[ rgRepetidos.ItemIndex].Value );
     end;
end;

function TWizNomPrevioISR_DevEx.EjecutarWizard: boolean;
begin
     Result := dmProcesos.PrevioISR( ParameterList );
end;

procedure TWizNomPrevioISR_DevEx.BitBtn6Click(Sender: TObject);
begin
     inherited;
     EmpleadosACalcular.Text:= GetFormulaConst( enEmpleado , EmpleadosACalcular.Text, EmpleadosACalcular.SelStart, evISPTAnual );
end;

procedure TWizNomPrevioISR_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  //DevEx
  Advertencia.Caption :=
            'Este proceso calcula el ISR de los empleados que cumplan con' +
            ' la expresi�n "Empleados a Calcular" y los filtros seleccionados.'
            + CR_LF +
            'Antes de aplicar este proceso, verifique que se cumpla con los siguientes requisitos:'
            + CR_LF +
            '1) Las n�minas ordinarias y especiales que afectan desde Enero hasta el mes seleccionado deben estar afectadas.'
            + CR_LF +
            '2) Haber calculado el proceso SUBE Aplicado Mensual de Enero hasta el mes seleccionado.'
            + CR_LF;  
end;

//DevEx     //Agregado para enfocar un control
procedure TWizNomPrevioISR_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := Year
     else
         ParametrosControl := nil;
     inherited;
end;


end.
