unit FWizAsistCalculoPreNomina_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls,
     ZetaDBTextBox,
     ZetaWizard,
     ZetaEdit,
     ZetaCommonLists,
     FWizNomBase_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, ZetaKeyLookup_DevEx, cxButtons, cxRadioGroup, cxTextEdit,
  cxMemo, cxGroupBox, cxLabel, dxGDIPlusClasses, cxImage,
  dxCustomWizardControl, dxWizardControl;

type
  TWizAsistCalculoPrenomina_DevEx = class(TWizNomBase_DevEx)
    TipoCalculo: TdxWizardControlPage;
    GroupBox: TGroupBox;
    rbTipoNueva: TRadioButton;
    rbTipoTodos: TRadioButton;
    procedure FormCreate(Sender: TObject);
    procedure rbTipoNuevaClick(Sender: TObject);
    procedure rbTipoTodosClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure SetControls( const eTipo: eTipoCalculo );
    function GetTipoCalculo: eTipoCalculo;
    function ValidaTipoCalculo: Boolean;
    function GetPosDerecho(const eTipo: eTipoCalculo): Integer;
    function GetControlTipo(const eTipo: eTipoCalculo): TWinControl;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizAsistCalculoPrenomina_DevEx: TWizAsistCalculoPrenomina_DevEx;

implementation

uses ZetaCommonClasses,
     ZAccesosMgr,
     ZAccesosTress,
     DCliente,
     DProcesos;

{$R *.DFM}

procedure TWizAsistCalculoPrenomina_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H20233_Calcular_prenomina;
     rbTipoNueva.Caption := ZetaCommonLists.ObtieneElemento( lfTipoCalculo, Ord( tcNueva ) );
     rbTipoTodos.Caption := ZetaCommonLists.ObtieneElemento( lfTipoCalculo, Ord( tcTodos ) );
     case dmCliente.GetDatosPeriodoActivo.Status of
          spNueva:
          begin
               SetControls( tcNueva );
               rbTipoTodos.Enabled := False;
          end;
     else
         begin
              SetControls( tcTodos );
              rbTipoTodos.Enabled := True;
         end;
     end;
end;

procedure TWizAsistCalculoPrenomina_DevEx.SetControls( const eTipo: eTipoCalculo );
begin
     rbTipoNueva.Checked := ( eTipo = tcNueva );
     rbTipoTodos.Checked := ( eTipo = tcTodos );
end;

procedure TWizAsistCalculoPrenomina_DevEx.rbTipoNuevaClick(Sender: TObject);
begin
     inherited;
     SetControls( tcNueva );
end;

procedure TWizAsistCalculoPrenomina_DevEx.rbTipoTodosClick(Sender: TObject);
begin
     inherited;
     SetControls( tcTodos );
end;

procedure TWizAsistCalculoPrenomina_DevEx.CargaParametros;
begin
inherited CargaParametros;




     with ParameterList do
     begin
          AddInteger( 'TipoCalculo', Ord( GetTipoCalculo ) );
          AddBoolean( 'PuedeCambiarTarjeta', dmCliente.PuedeModificarTarjetasAnteriores ); {No mostrar en la Impresi�n}
     end;
     with Descripciones do
     begin
          AddString( 'Tipo de C�lculo', ZetaCommonLists.ObtieneElemento( lfTipoCalculo, Ord( Ord( GetTipoCalculo )) ));
     end;
end;

function TWizAsistCalculoPrenomina_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CalcularPrenomina( ParameterList );
end;

procedure TWizAsistCalculoPrenomina_DevEx.WizardBeforeMove(Sender: TObject;
          var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if CanMove and Wizard.Adelante then
     begin
          if Wizard.EsPaginaActual( Parametros ) then
             CanMove := dmCliente.ValidaTarjetaStatusDlg( dmCliente.GetDatosPeriodoActivo.Status )
          else if Wizard.EsPaginaActual( TipoCalculo ) then
               CanMove := ValidaTipoCalculo;
     end;
end;

function TWizAsistCalculoPrenomina_DevEx.ValidaTipoCalculo: Boolean;
var
   eTipo: eTipoCalculo;
begin
     eTipo := GetTipoCalculo;
     Result := ZAccesosMgr.CheckDerecho( D_ASIS_PROC_CALC_PRENOMINA, GetPosDerecho( eTipo ) );
     if not Result then
     begin
          Error( Format( 'No Tiene Derecho para Calcular Pre-N�mina como %s', [
                 ZetaCommonLists.ObtieneElemento( lfTipoCalculo, Ord( eTipo ) ) ] ),
                 GetControlTipo( eTipo ) );
     end;
end;

function TWizAsistCalculoPrenomina_DevEx.GetTipoCalculo: eTipoCalculo;
begin
     if rbTipoNueva.Checked then
        Result := tcNueva
     else {if rbTipoTodos.Checked then}
        Result := tcTodos;
end;

function TWizAsistCalculoPrenomina_DevEx.GetPosDerecho( const eTipo: eTipoCalculo ): Integer;
begin
     if ( eTipo = tcNueva ) then
        Result := K_DERECHO_CONSULTA
     else
        Result := K_DERECHO_ALTA;
end;

function TWizAsistCalculoPrenomina_DevEx.GetControlTipo( const eTipo: eTipoCalculo ): TWinControl;
begin
     if ( eTipo = tcNueva ) then
        Result := rbTipoNueva
     else
        Result := rbTipoTodos;
end;

procedure TWizAsistCalculoPrenomina_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'Se realizar� el c�lculo de totales de d�as y hora sumando las tarjetas diarias de los empleados.';
  Parametros.PageIndex := 0;
  FiltrosCondiciones.PageIndex := 1;
  TipoCalculo.PageIndex := 2;
  Ejecucion.pageIndex := 3;
end;

end.



