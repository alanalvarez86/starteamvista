unit FWizNomExportarMov_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls,
     ZetaEdit, ZetaDBTextBox, ZetaKeyCombo,
     ZetaWizard, FWizNomBase_DevEx, cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
     cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons,
     dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
     dxWizardControl;

type
  TWizNomExportarMov_DevEx = class(TWizNomBase_DevEx)
    Parametros2: TdxWizardControlPage;
    OpenDialog: TOpenDialog;
    Panel1: TPanel;
    Label5: TLabel;
    Label7: TLabel;
    BGuardaArchivo: TcxButton;
    Label6: TLabel;
    sArchivo: TEdit;
    lDiasHoras: TCheckBox;
    iMontos: TZetaKeyCombo;
    iTipoFormato: TZetaKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure BGuardaArchivoClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    function GetArchivo: String;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizNomExportarMov_DevEx: TWizNomExportarMov_DevEx;

implementation

uses DCliente,
     DProcesos,
     ZetaDialogo,
     ZetaClientTools,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaCommonLists;

{$R *.DFM}

procedure TWizNomExportarMov_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H33412_Exportar_movimientos;
     sArchivo.Text := 'Movimien.Dat';
     iMontos.ItemIndex := Ord( hdExcepcion );
     iTipoFormato.ItemIndex := Ord( faASCIIFijo );

     //DevEx
     Parametros.PageIndex := 0;
     Parametros2.PageIndex := 1;
     FiltrosCondiciones.PageIndex := 2;
     Ejecucion.PageIndex := 3;
end;

function TWizNomExportarMov_DevEx.GetArchivo: String;
begin
     Result := sArchivo.Text;
end;

procedure TWizNomExportarMov_DevEx.BGuardaArchivoClick(Sender: TObject);
begin
     inherited;
     sArchivo.Text := AbreDialogo( OpenDialog, GetArchivo, 'dat' );
end;

procedure TWizNomExportarMov_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
          if Adelante and EsPaginaActual( Parametros2 ) then
          begin
               if ZetaCommonTools.StrVacio( GetArchivo ) then
               begin
                    ZetaDialogo.zError( Caption, 'El nombre del archivo no puede quedar vac�o', 0 );
                    CanMove := False;
                    sArchivo.SetFocus;
               end
               else
                   if FileExists( GetArchivo ) and not ZetaDialogo.zConfirm( Caption, 'El archivo ' + GetArchivo + ' ya xiste' + CR_LF + '� Desea reemplazarlo ? ', 0, mbNo ) then
                   begin
                        CanMove := False;
                        sArchivo.SetFocus;
                   end
                   else
                       if not lDiasHoras.Checked and ( eHorasDias( iMontos.ItemIndex ) = hdNinguno ) then
                       begin
                            ZetaDialogo.zError( Caption, 'Excepciones de Dias/Horas tiene que estar checado o Montos diferente de ninguno', 0 );
                            CanMove := False;
                            lDiasHoras.SetFocus;
                       end;
          end;
     end;
end;

procedure TWizNomExportarMov_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddString( 'Archivo', GetArchivo );
          AddBoolean( 'DiasHoras', lDiasHoras.Checked );
          AddInteger( 'TipoMonto', iMontos.ItemIndex );
          AddInteger( 'Formato', iTipoFormato.ItemIndex );
     end;
     with Descripciones do
     begin
          AddString( 'Archivo', GetArchivo );
          AddBoolean( 'Excepciones D�as/Horas:', lDiasHoras.Checked );
          AddString( 'Tipo de Montos', ObtieneElemento( lfHorasDias, iMontos.ItemIndex ) );
          AddString( 'Tipo de formato', ObtieneElemento( lfFormatoASCII, iTipoFormato.ItemIndex ) );
     end;
end;

function TWizNomExportarMov_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.ExportarMov( ParameterList );
end;

procedure TWizNomExportarMov_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption :=
     'Al aplicar el proceso se efectuar� la exportaci�n ' +
     'de todos los montos de la n�mina hacia el archivo especificado.';
end;

end.
