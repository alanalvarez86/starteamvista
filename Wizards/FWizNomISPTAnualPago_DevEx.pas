unit FWizNomISPTAnualPago_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, Mask,
     FWizNomBase_DevEx,                                                                                                      
     ZetaEdit,
     ZetaDBTextBox,
     ZetaWizard,
     ZetaNumero,
     ZetaFecha,
     cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl;

type
  TWizNomISPTAnualPago_DevEx = class(TWizNomBase_DevEx)
    Inicio: TdxWizardControlPage;
    DatosPrestamo: TdxWizardControlPage;
    Metodo: TRadioGroup;
    Year: TZetaNumero;
    Label5: TLabel;
    CreditoSalario: TZetaKeyLookup_DevEx;
    ISPTNeto: TZetaKeyLookup_DevEx;
    ACargo: TZetaKeyLookup_DevEx;
    AFavor: TZetaKeyLookup_DevEx;
    CreditoSalarioLBL: TLabel;
    ISPTNetoLBL: TLabel;
    ACargoLBL: TLabel;
    AFavorLBL: TLabel;
    PCreditoSalario: TZetaKeyLookup_DevEx;
    PISPTNeto: TZetaKeyLookup_DevEx;
    PACargo: TZetaKeyLookup_DevEx;
    PAFavor: TZetaKeyLookup_DevEx;
    PReferencia: TZetaEdit;
    PInicio: TZetaFecha;
    Label12: TLabel;
    Label11: TLabel;
    Label10: TLabel;
    Label9: TLabel;
    Label8: TLabel;
    Label7: TLabel;
    Label6: TLabel;
    PFormulaBtn: TcxButton;
    PFormula: TcxMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure YearChange(Sender: TObject);
    procedure MetodoClick(Sender: TObject);
    procedure PFormulaBtnClick(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject); //DevEx(by am): Agregado para enfocar correctamente
  private
    { Private declarations }
    function EnviaNomina: Boolean;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizNomISPTAnualPago_DevEx: TWizNomISPTAnualPago_DevEx;

implementation

uses ZetaDialogo,
     ZConstruyeFormula,
     ZetaCommonClasses,
     ZetaTipoEntidad,
     ZetaCommonLists,
     ZBaseSelectGrid_DevEx,
     FEmpleadoGridSelect_DevEx,
     DCliente,
     DProcesos,
     DTablas,
     DCatalogos,
     ZetaCommonTools;

{$R *.DFM}

procedure TWizNomISPTAnualPago_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     Year.Valor := dmCliente.YearDefault-1;
     PInicio.Valor := Date;
     PReferencia.Text := IntToStr( Year.ValorEntero );
     HelpContext := H33514_Diferencias_de_ISPT_a_nomina;
     ParametrosControl := Year;
     AFavor.LookupDataset := dmCatalogos.cdsConceptos;
     ACargo.LookupDataset := dmCatalogos.cdsConceptos;
     ISPTNeto.LookupDataset := dmCatalogos.cdsConceptos;
     CreditoSalario.LookupDataset := dmCatalogos.cdsConceptos;
     PAFavor.LookupDataset := dmTablas.cdsTPresta;
     PACargo.LookupDataset := dmTablas.cdsTPresta;
     PISPTNeto.LookupDataset := dmTablas.cdsTPresta;
     PCreditoSalario.LookupDataset := dmTablas.cdsTPresta;

      //DevEx(by am): Es necesario especificar que Ejecucion sera la ultima pagina del wizard.
     Inicio.PageIndex := WizardControl.PageCount - 5;
     Parametros.PageIndex := WizardControl.PageCount - 4;
     DatosPrestamo.PageIndex := WizardControl.PageCount - 3;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 2; //3
     Ejecucion.PageIndex := WizardControl.PageCount - 1;

     //Para determinar la visibilidad de las paginas desde el principio.
     Parametros.PageVisible := EnviaNomina;
     DatosPrestamo.PageVisible := not EnviaNomina;
end;

procedure TWizNomISPTAnualPago_DevEx.FormShow(Sender: TObject);
begin
     dmCatalogos.cdsConceptos.Conectar;
     dmTablas.cdsTPresta.Conectar;
     inherited;
     Advertencia.Caption := ' Al aplicar el proceso se enviar�n las diferencias (a favor o en contra) hacia un per�odo de n�mina o un pr�stamo al empleado.';
end;

function TWizNomISPTAnualPago_DevEx.EnviaNomina: Boolean;
begin
     Result := ( Metodo.ItemIndex = 0 );
end;

procedure TWizNomISPTAnualPago_DevEx.YearChange(Sender: TObject);
begin
     inherited;
     PReferencia.Text := IntToStr( Year.ValorEntero );
end;

procedure TWizNomISPTAnualPago_DevEx.MetodoClick(Sender: TObject);
begin
     inherited;
     with Metodo do
     begin
          Parametros.PageVisible := EnviaNomina;
          DatosPrestamo.PageVisible := not EnviaNomina;
     end;
end;

procedure TWizNomISPTAnualPago_DevEx.PFormulaBtnClick(Sender: TObject);
begin
     inherited;
     with PFormula do
     begin
          Text:= ZConstruyeFormula.GetFormulaConst( enNomina, Text, SelStart, evBase );
     end;
end;

procedure TWizNomISPTAnualPago_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     ParametrosControl := nil;
     with Wizard do
     begin
          if CanMove and Adelante then
          begin
               if EsPaginaActual( Inicio ) then
               begin
                    if ( Year.ValorEntero <= 0 ) then
                    begin
                         ZetaDialogo.zError( Caption, '� A�o no fu� especificado !', 0 );
                         CanMove := False;
                         //ActiveControl := Year;
                    end
               end
               else
                   if EsPaginaActual( Parametros ) then
                   begin
                        if ( ( AFavor.Valor + ACargo.Valor + ISPTNeto.Valor + CreditoSalario.Valor ) = 0 ) then
                        begin
                             ZetaDialogo.zError( Caption, '� No se especific� ning�n concepto !', 0 );
                             CanMove := False;
                             //ActiveControl := AFavor;
                        end;
                   end
                   else
                       if EsPaginaActual( DatosPrestamo ) then
                       begin
                            if ( ( PAFavor.Llave + PACargo.Llave + PISPTNeto.Llave + PCreditoSalario.Llave ) = '' ) then
                            begin
                                 ZetaDialogo.zError( Caption, '� No se especific� ning�n Tipo de Pr�stamo !', 0 );
                                 CanMove := False;
                                 //ActiveControl := PAFavor;
                            end{OP: 28/05/08}
                            else if ZetaCommonTools.StrVacio( PReferencia.Text ) then
                                 CanMove := Error( '�No se especific� una Referencia!', PReferencia );

                       end;
          end;
     end;
     inherited;
end;
procedure TWizNomISPTAnualPago_DevEx.WizardAfterMove(Sender: TObject);
begin
     // Para Enfocar el primer control dependiendo del tipo de proceso
     with Wizard do
     begin
          if Adelante then
        if EsPaginaActual( Inicio ) then
           ParametrosControl := Year
        else if EsPaginaActual( Parametros ) then
             ParametrosControl := AFavor
        else if EsPaginaActual( DatosPrestamo ) then
             ParametrosControl := PAFavor;
     end;
     inherited;
end;

procedure TWizNomISPTAnualPago_DevEx.CargaParametros;
var
   lReformaFiscal2008: Boolean;
begin
     inherited CargaParametros;
     with Descripciones do
     begin
          Clear;
          AddInteger( 'A�o', Year.ValorEntero );
          lReformaFiscal2008 := ( Year.ValorEntero >= ZetaCommonClasses.K_REFORMA_FISCAL_2008 );
          if EnviaNomina then
          begin
               AddString( 'Enviar diferencias a', 'N�mina' );
               AddString( 'Concepto a favor', GetDescripLlave( AFavor ) );
               AddString( 'Concepto a cargo', GetDescripLlave( ACargo ) );
               AddString( 'Concepto ISR neto', GetDescripLlave( ISPTNeto ) );
               if lReformaFiscal2008 then
                  AddString( 'Concepto subsidio empleo', GetDescripLlave( CreditoSalario ) )
               else
                   AddString( 'Concepto cr�dito salario', GetDescripLlave( CreditoSalario ) );
          end
          else
          begin
               AddString( 'Enviar diferencias a', 'Pr�stamos' );
               AddString( 'A favor', GetDescripLlave( PAFavor ) );
               AddString( 'A cargo', GetDescripLlave( PACargo ) );
               AddString( 'ISR neto', GetDescripLlave( PISPTNeto ) );
               if lReformaFiscal2008 then
                  AddString( 'Subsidio al empleo', GetDescripLlave( PCreditoSalario ) )
               else
                   AddString( 'Cr�dito al salario', GetDescripLlave( PCreditoSalario ) );
               AddDate( 'Compensar desde', PInicio.Valor );
               AddString( 'Referencia', PReferencia.Text );
               AddString( 'F�rmula', PFormula.Text );
          end;
     end;
     ParamInicial := 0;//DevEx(by am): Como se limpian las descripciones,  se inicializa el parametro inicial en 0.
     //inherited CargaParametros;

     with ParameterList do
     begin
          AddInteger( 'YearActivo', Year.ValorEntero );
          AddBoolean( 'EnviarNomina', EnviaNomina );

          AddInteger( 'ExcepcionAFavor', AFavor.Valor );
          AddInteger( 'ExcepcionACargo', ACargo.Valor );
          AddInteger( 'ExcepcionISPTNeto', ISPTNeto.Valor );
          AddInteger( 'ExcepcionCredito', CreditoSalario.Valor );

          AddString( 'PrestamoAFavor', PAFavor.Llave );
          AddString( 'PrestamoACargo', PACargo.Llave );
          AddString( 'PrestamoISPTNeto', PISPTNeto.Llave );
          AddString( 'PrestamoCredito', PCreditoSalario.Llave );
          AddString( 'PrestamoReferencia', PReferencia.Text );
          AddString( 'PrestamoFormula', PFormula.Text );
          AddDate( 'PrestamoInicio', PInicio.Valor );
     end;
end;

procedure TWizNomISPTAnualPago_DevEx.CargaListaVerificacion;
begin
     dmProcesos.ISPTAnualPagoGetLista( ParameterList );
end;

function TWizNomISPTAnualPago_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpleadoGridSelect_DevEx );
end;

function TWizNomISPTAnualPago_DevEx.EjecutarWizard: boolean;
begin
     Result := dmProcesos.ISPTAnualPago( ParameterList, Verificacion );
end;

end.





