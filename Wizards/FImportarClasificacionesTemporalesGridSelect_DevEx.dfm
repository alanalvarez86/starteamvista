inherited ImportarClasificacionesTemporalesGridSelect_DevEx: TImportarClasificacionesTemporalesGridSelect_DevEx
  Left = 468
  Top = 442
  Width = 591
  Height = 266
  Caption = 'Clasificaciones a Importar'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 192
    Width = 575
    inherited OK_DevEx: TcxButton
      Left = 408
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 486
    end
  end
  inherited PanelSuperior: TPanel
    Width = 575
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 575
    Height = 157
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
        Width = 40
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        Width = 150
      end
      object EV_CODIGO: TcxGridDBColumn
        Caption = 'Evento'
        DataBinding.FieldName = 'EV_CODIGO'
        Visible = False
        Width = 20
      end
      object TV_FECHA: TcxGridDBColumn
        Caption = 'Fecha Reg.'
        DataBinding.FieldName = 'TV_FECHA'
        Visible = False
        Width = 20
      end
      object AU_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'AU_FECHA'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
        Width = 85
      end
      object CB_PUESTO: TcxGridDBColumn
        Caption = 'Puesto'
        DataBinding.FieldName = 'CB_PUESTO'
      end
      object CB_CLASIFI: TcxGridDBColumn
        Caption = 'Clasificaci'#243'n'
        DataBinding.FieldName = 'CB_CLASIFI'
      end
      object CB_TURNO: TcxGridDBColumn
        Caption = 'Turno'
        DataBinding.FieldName = 'CB_TURNO'
      end
      object CB_NIVEL1: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL1'
      end
      object CB_NIVEL2: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL2'
        Visible = False
        Width = 20
      end
      object CB_NIVEL3: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL3'
        Visible = False
        Width = 20
      end
      object CB_NIVEL4: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL4'
        Visible = False
        Width = 20
      end
      object CB_NIVEL5: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL5'
        Visible = False
        Width = 20
      end
      object CB_NIVEL6: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL6'
        Visible = False
        Width = 20
      end
      object CB_NIVEL7: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL7'
        Visible = False
        Width = 20
      end
      object CB_NIVEL8: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL8'
        Visible = False
        Width = 20
      end
      object CB_NIVEL9: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL9'
        Visible = False
        Width = 20
      end
      object CB_NIVEL10: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL10'
        Visible = False
        Width = 20
      end
      object CB_NIVEL11: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL11'
        Visible = False
        Width = 20
      end
      object CB_NIVEL12: TcxGridDBColumn
        DataBinding.FieldName = 'CB_NIVEL12'
        Visible = False
        Width = 20
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
