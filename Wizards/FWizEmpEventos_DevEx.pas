unit FWizEmpEventos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ComCtrls, ExtCtrls, CheckLst, Mask,
     ZetaEdit, ZetaFecha, ZetaKeyCombo, FWizEmpBaseFiltro,
     cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
      TressMorado2013, dxSkinsDefaultPainters,
     cxContainer, cxEdit, Menus, ZetaCXWizard, cxRadioGroup, cxTextEdit,
     cxMemo, ZetaKeyLookup_DevEx, cxButtons, dxGDIPlusClasses, cxImage,
     cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl;

type
  TWizEmpEventos_DevEx = class(TWizEmpBaseFiltro)
    dReferenciaLBL: TLabel;
    dReferencia: TZetaFecha;
    dRegistroLBL: TLabel;
    dRegistro: TZetaFecha;
    EventosLBL: TLabel;
    sDescripcionLBL: TLabel;
    sDescripcion: TEdit;
    sObservaLBL: TLabel;
    sObserva: TcxMemo;
    Eventos: TCheckListBox;
    PrendeBtn: TcxButton;
    ApagaBtn: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    //DevEx     //Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PrendeApagaBtnClick(Sender: TObject);
  private
    procedure InicializaEventos(const lState: Boolean);
    function GetListaEventos: String;
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizEmpEventos_DevEx: TWizEmpEventos_DevEx;

implementation

uses DCliente,
     DCatalogos,
     DProcesos,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     ZBaseSelectGrid_DevEx,
     FEmpEventosGridSelect_DevEx,
     ZcxWizardBasico;

{$R *.DFM}

procedure TWizEmpEventos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := dReferencia;
     with dmCliente do
     begin
          dReferencia.Valor := FechaDefault;
          dRegistro.Valor := FechaDefault;
     end;
     HelpContext:= H10166_Aplicar_eventos;

     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;
end;

procedure TWizEmpEventos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmCatalogos do
     begin
          cdsEventos.Conectar;
          CargaListaEventos( Eventos.Items );
     end;
     if ( Eventos.Items.Count = 0 ) then
        Error( '� No Hay Eventos Activos en el Cat�logo de Eventos !', TWinControl(Salir) );

     Advertencia.Caption :=
         'Al ejecutar el proceso se generar� un movimiento de kardex o de historial correspondiente.';
end;

procedure TWizEmpEventos_DevEx.CargaParametros;
begin
     inherited CargaParametros;

     with ParameterList do
     begin
          AddDate( 'FechaReferencia', dReferencia.Valor );
          AddDate( 'FechaRegistro', dRegistro.Valor );
          AddString( 'Descripcion', sDescripcion.Text );
          AddMemo( 'Observaciones', sObserva.Text );
          AddString( 'ListaEventos', GetListaEventos );
     end;

     with Descripciones do
     begin
          AddDate( 'Fecha referencia', dReferencia.Valor );
          AddDate( 'Fecha registro', dRegistro.Valor );
          AddString( 'Eventos', GetListaEventos );
          AddString( 'Descripci�n', sDescripcion.Text );
          AddMemo( 'Observaciones', sObserva.Text );
     end;
end;

procedure TWizEmpEventos_DevEx.CargaListaVerificacion;
begin
     dmProcesos.EventosGetLista( ParameterList );
end;

function TWizEmpEventos_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpEventosGridSelect_DevEx );
end;

procedure TWizEmpEventos_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   i : Integer;
begin
     inherited;
     ParametrosControl := nil;  //DevEx
     if Wizard.Adelante then
     begin
          //if ( PageControl.ActivePage = Parametros ) then
          if ( WizardControl.ActivePage = Parametros ) then
          begin
               if ( dRegistro.Valor < dReferencia.Valor ) then
                  CanMove := Error( 'La Fecha de Registro Debe Ser Mayor o Igual a la Fecha de Referencia', dRegistro )
               else
               begin
                    CanMove := False;
                    with Eventos do
                    begin
                         for i := 0 to ( Items.Count - 1 ) do
                         begin
                              if Checked[ i ] then
                              begin
                                   CanMove := True;
                                   Break;
                              end;
                         end;
                    end;
                    if not CanMove then
                       Error( '� No Hay Eventos Seleccionados !', Eventos )
               end;
          end;
     end;
end;

function TWizEmpEventos_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.Eventos( ParameterList, Verificacion );
end;

procedure TWizEmpEventos_DevEx.PrendeApagaBtnClick(Sender: TObject);
begin
     InicializaEventos( ( TBitbtn( Sender ).Tag = 1 ) );
end;

procedure TWizEmpEventos_DevEx.InicializaEventos( const lState: Boolean );
var
   i: Integer;
begin
     with Eventos do
     begin
          with Items do
          begin
               try
                  BeginUpdate;
                  for i := 0 to ( Count - 1 ) do
                      Checked[ i ] := lState;
               finally
                      EndUpdate;
               end;
          end;
     end;
end;

function TWizEmpEventos_DevEx.GetListaEventos: String;
var
   i : Integer;
begin
     Result := VACIO;
     with Eventos do
     begin
          for i := 0 to ( Items.Count - 1 ) do
          begin
               if Checked[ i ] then
                  Result := ZetaCommonTools.ConcatString( Result, EntreComillas( Items.Names[i] ), ',' );
          end;
     end;
end;

//DevEx     //Agregado para enfocar un control
procedure TWizEmpEventos_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
             ParametrosControl := dReferencia
     else
         ParametrosControl := nil;
     inherited;
end;

end.
