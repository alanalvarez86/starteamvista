unit FWizEmpCancelarCierreVacaciones_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FWizEmpBaseFiltro, ComCtrls, ZetaEdit, StdCtrls, Buttons,
  ZetaWizard, ExtCtrls, Mask, ZetaFecha, ZetaKeyCombo,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
  ZetaKeyLookup_DevEx, cxButtons, cxRadioGroup, cxTextEdit, cxMemo,
  cxGroupBox, cxLabel, dxGDIPlusClasses, cxImage, dxCustomWizardControl,
  dxWizardControl;

type
  TWizEmpCancelarCierreVacaciones_DevEx = class(TWizEmpBaseFiltro)
    zFechaCierre: TZetaFecha;
    rbFechaCierre: TRadioGroup;
    zcbModificacion: TZetaKeyCombo;
    lblCanModifica: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure rbFechaCierreClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject); //DevEx(by am): Se agrega para validar el enfoque de controles.
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizEmpCancelarCierreVacaciones_DevEx: TWizEmpCancelarCierreVacaciones_DevEx;

const
     //Constantes Paginas
     K_PAGE_PARAMETROS =0;

implementation

uses DCliente,
     DProcesos,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZBaseSelectGrid_DevEx,
     FEmpCancelarCierreVacacionesGridSelect_DevEx;

{$R *.DFM}
procedure TWizEmpCancelarCierreVacaciones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := rbFechaCierre;
     zFechaCierre.Valor := dmCliente.FechaDefault;
     zcbModificacion.ItemIndex := Ord( cmGlobales );
     HelpContext:= H_Cancelar_Cierre_Vacaciones;
     //DevEx(by am): Es necesario especificar que Ejecucion sera la ultima pagina del wizard.
     Parametros.PageIndex := K_PAGE_PARAMETROS;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 2; //3
     Ejecucion.PageIndex := WizardControl.PageCount - 1;
end;

procedure TWizEmpCancelarCierreVacaciones_DevEx.CargaListaVerificacion;
begin
     dmProcesos.CancelarCierreVacacionesGetLista( ParameterList );
end;

procedure TWizEmpCancelarCierreVacaciones_DevEx.CargaParametros;
var
   lUltimoCierre: Boolean;
begin
     inherited CargaParametros;
     lUltimoCierre := (rbFechaCierre.ItemIndex = 0 );
     with Descripciones do
     begin

	  if( lUltimoCierre )then
              AddString( 'Fecha de cierre', 'Ultimo cierre de cada empleado' )
          else
              AddDate( 'Fecha de cierre', zFechaCierre.Valor );
          AddString( 'Cancelar modificación', ObtieneElemento( lfCancelaMod, zcbModificacion.ItemIndex ) );
     end;

     with ParameterList do
     begin
	  AddBoolean( 'UltimoCierre', lUltimoCierre );
	  AddDate( 'UnaFecha', zFechaCierre.Valor );
          AddInteger( 'TipoModificacion', zcbModificacion.ItemIndex );
     end;
end;

function TWizEmpCancelarCierreVacaciones_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CancelarCierreVacaciones( ParameterList, Verificacion );
end;

function TWizEmpCancelarCierreVacaciones_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpCancelarCierreVacacionesGridSelect_DevEx );
end;

procedure TWizEmpCancelarCierreVacaciones_DevEx.rbFechaCierreClick(
  Sender: TObject);
begin
     inherited;
     zFechaCierre.Enabled := ( rbFechaCierre.ItemIndex <> 0 );
end;

procedure TWizEmpCancelarCierreVacaciones_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'Este proceso cancelará los movimientos de tipo Cierre de los empleados indicados.';
end;

procedure TWizEmpCancelarCierreVacaciones_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual( Parametros )then
         ParametrosControl := rbFechaCierre
     else
         ParametrosControl := nil;// Si la pagina es distinta no se necesita enfocar nada.
     inherited;
end;

end.
