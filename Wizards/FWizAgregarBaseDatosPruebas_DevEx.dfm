inherited WizAgregarBaseDatosPruebas_DevEx: TWizAgregarBaseDatosPruebas_DevEx
  Left = 858
  Top = 215
  Caption = 'Agregar Base de Datos de tipo Pruebas'
  ClientHeight = 477
  ClientWidth = 462
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 462
    Height = 477
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        ' Proceso que permite agregar una base de datos de tipo Pruebas d' +
        'esde cero o bas'#225'ndose en una existente.'
      Header.Title = 'Agregar base de datos de tipo Pruebas'
      inherited lblSeleccionarBD: TLabel
        Left = 89
        Top = 172
      end
      inherited lbNombreBD: TLabel
        Left = 82
        Top = 116
      end
      inherited cbBasesDatos: TComboBox
        Left = 224
        Top = 168
      end
      inherited txtBaseDatos: TEdit
        Left = 224
        Top = 112
      end
      inherited rbBDNueva: TcxRadioButton
        Left = 56
        Top = 85
      end
      inherited rbBDExistente: TcxRadioButton
        Left = 56
        Top = 149
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 127
        Width = 440
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 440
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 372
          AnchorY = 36
        end
      end
      inherited GrupoAvance: TcxGroupBox
        Top = 192
        Height = 145
        Width = 440
        inherited cxMemoStatus: TcxMemo
          Left = 249
          ParentFont = False
          Style.IsFontAssigned = True
          Height = 120
          Width = 188
        end
        inherited cxMemoPasos: TcxMemo
          ParentFont = False
          Style.IsFontAssigned = True
          Height = 120
          Width = 246
        end
      end
    end
    inherited NuevaBD2: TdxWizardControlPage
      inherited lblUbicacion: TLabel
        Left = 41
      end
      inherited GBUsuariosMSSQL: TcxGroupBox
        Left = 40
        inherited lblClave: TLabel
          Left = 96
        end
        inherited lblUsuario: TLabel
          Left = 88
        end
        inherited txtUsuario: TEdit
          Left = 136
        end
        inherited txtClave: TMaskEdit
          Left = 136
        end
        inherited rbSameUser: TcxRadioButton
          Left = 56
          Width = 273
        end
        inherited rbOtroUser: TcxRadioButton
          Left = 56
          Width = 217
        end
      end
      inherited lblDatoUbicacion: TcxLabel
        Left = 96
        Width = 313
      end
    end
    inherited Estructura: TdxWizardControlPage
      inherited lblDescripcion: TLabel
        Left = 66
        Top = 148
      end
      inherited lblCodigo: TLabel
        Left = 89
        Top = 124
      end
      inherited txtDescripcion: TZetaEdit
        Left = 128
        Top = 144
      end
      inherited txtCodigo: TZetaEdit
        Left = 128
        Top = 120
        OnExit = txtCodigoExit
      end
    end
    object CrearEmpresa: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 
        'Indique la base de datos existente de tipo Tress que se usar'#225' pa' +
        'ra crear la nueva base de datos.'
      Header.Title = 'Seleccionar base de datos para la operaci'#243'n'
      object chDepurarDigitales: TcxCheckBox
        Left = 108
        Top = 176
        Caption = #191'Depurar documentos digitales?'
        State = cbsChecked
        TabOrder = 3
        Transparent = True
        Width = 185
      end
      object chDepurarAsistencia: TcxCheckBox
        Left = 108
        Top = 152
        Caption = #191'Depurar asistencia?'
        State = cbsChecked
        TabOrder = 2
        Transparent = True
        Width = 145
      end
      object chDepurarBitacoras: TcxCheckBox
        Left = 108
        Top = 128
        Caption = #191'Depurar bit'#225'coras?'
        State = cbsChecked
        TabOrder = 1
        Transparent = True
        Width = 121
      end
      object cbEmpresas: TZetaKeyCombo
        Left = 108
        Top = 104
        Width = 257
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 0
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 64
    Top = 91
  end
end
