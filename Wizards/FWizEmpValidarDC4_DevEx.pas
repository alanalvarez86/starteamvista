unit FWizEmpValidarDC4_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FWizEmpBaseFiltro, ComCtrls, StdCtrls, Buttons, ZetaWizard, ExtCtrls,
  Mask, ZetaNumero, ZetaEdit, 
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
  cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl;

type
  TWizEmpValidarDC4_DevEx = class(TWizEmpBaseFiltro)
    OpenDialog: TOpenDialog;
    Archivo: TEdit;
    Establecimiento: TCheckBox;
    Anio: TZetaNumero;
    ArchivoLBL: TLabel;
    AnioLbl: TLabel;
    ArchivoSeek: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject); //DevEx(by am): Agregado para enfocar correctamente los controles
  private
    { Private declarations }
  protected
    { Private declarations }
    function EjecutarWizard: Boolean;override;
    procedure CargaParametros;override;
  public
    { Public declarations }
  end;

var
  WizEmpValidarDC4_DevEx: TWizEmpValidarDC4_DevEx;

implementation

uses DCliente, ZetaCommonClasses, DProcesos, ZetaMessages, ZetaDialogo, SHFolder;

{$R *.dfm}

procedure TWizEmpValidarDC4_DevEx.FormCreate(Sender: TObject);
const
     K_ESPACIO = ' ';
     K_GUION = '_';
begin
     // Archivo.Text := GetEnvironmentVariable('TEMP')
     //              + '\RES-'
     //              + StringReplace( dmCliente.GetDatosEmpresaActiva.Nombre, K_ESPACIO, K_GUION, [ rfReplaceAll ] )
     //              + FormatDateTime('ddmmyy-hhnnss',Now)
     //              + '.CSV';
                       
     Archivo.Text := ZetaMessages.SetFileNameDefaultPath( 'RES-'
                         + StringReplace( dmCliente.GetDatosEmpresaActiva.Nombre, K_ESPACIO, K_GUION, [ rfReplaceAll ] )
                         + FormatDateTime('ddmmyy-hhnnss',Now) + '.CSV');

     inherited;
     Anio.Valor := dmCliente.YearDefault-1;
     ParametrosControl := Anio;
     HelpContext := H488000_Revisar_Datos_STPS;
end;

procedure TWizEmpValidarDC4_DevEx.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     ParametrosControl := nil;
     if Wizard.Adelante then
     begin
          if ( Wizard.EsPaginaActual (Parametros) ) then
          begin
               if Anio.Valor <= 1900 then
               begin
                    // CanMove := Error( 'El a�o no es v�lido', Anio )
                    ZetaDialogo.zError( Self.Caption, '� El a�o no es v�lido !', 0 );
                    ActiveControl := Anio;
                    CanMove := False;
               end
               else
               begin
                   if ( Trim (ExtractFileName( Archivo.Text )) = '' ) then
                   begin
                       ZetaDialogo.zError( Self.Caption, '� Nombre de Archivo no puede quedar vac�o !', 0 );
                       ActiveControl := Archivo;
                       CanMove := False;
                   end;
               end;
          end
     end;
     inherited;
end;

procedure TWizEmpValidarDC4_DevEx.WizardAfterMove(Sender: TObject);
begin
     // Para Enfocar el primer control dependiendo del tipo de proceso
     if Wizard.EsPaginaActual( Parametros ) then
        ParametrosControl := Anio;
     inherited;
end;

function TWizEmpValidarDC4_DevEx.EjecutarWizard: Boolean;
begin
     Result:= dmProcesos.RevisarDatosSTPS (ParameterList );
end;

procedure TWizEmpValidarDC4_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with Descripciones do
     begin
          AddInteger('A�o', Anio.ValorEntero);
          AddBoolean('Revisar Establecimientos', Establecimiento.Checked);
     end;

     with ParameterList do
     begin
          AddInteger('Year', Anio.ValorEntero );
          AddBoolean('Establecimiento', Establecimiento.Checked);
          AddString( 'Archivo', Archivo.Text );
     end;
end;

procedure TWizEmpValidarDC4_DevEx.ArchivoSeekClick(Sender: TObject);
begin
     inherited;
     with Archivo do
     begin
          with OpenDialog do
          begin
               FileName := ExtractFileName( Text );
               InitialDir := ExtractFileDir( Text );
               if Execute then
                  Text := FileName;
          end;
     end;
end;

procedure TWizEmpValidarDC4_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := ' Al dar clic en aplicar se llevar� a cabo el proceso de revision de empleados y cursos correspondientes al a�o indicado.'; 
end;

end.
