unit FWizSistImportaTerminales_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZcxBaseWizard, ComCtrls, StdCtrls, Buttons, ZetaWizard, ExtCtrls,
  cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit,
  ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl, Menus, cxButtons;

type
  TWizSistImportaTerminales_DevEx = class(TcxBaseWizard)
    txtTerminales: TEdit;
    Label1: TLabel;
    SpeedButton: TcxButton;
    OpenDialog: TOpenDialog;
    procedure SpeedButtonClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
    FListaTerminales: String;
    procedure ObtenListaTerminales;
  public
    { Public declarations }
  protected
    { Protected declarations }
    procedure CargaParametros; override;
    function EjecutarWizard: Boolean; override;
  end;

var
  WizSistImportaTerminales_DevEx: TWizSistImportaTerminales_DevEx;

implementation

uses ZetaClientTools,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaServerTools,
     DProcesos;

{$R *.dfm}

procedure TWizSistImportaTerminales_DevEx.SpeedButtonClick(Sender: TObject);
begin
     inherited;
     txtTerminales.Text := ZetaClientTools.AbreDialogo( OpenDialog, txtTerminales.Text, 'txt' );
end;

procedure TWizSistImportaTerminales_DevEx.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     parametrosControl := nil;
     if Wizard.Adelante then
     begin
          if( Wizard.EsPaginaActual (Parametros) )then
          begin
               if not FileExists( Trim( txtTerminales.Text ) )then
                  CanMove := Error( '� Error ! Es necesario especificar un archivo de terminales', txtTerminales );
          end;
     end;
end;

procedure TWizSistImportaTerminales_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     //SetFocusedControl( txtTerminales ); //Se utiliza parametrosControl
end;

procedure TWizSistImportaTerminales_DevEx.CargaParametros;
begin
     inherited;
     Self.ObtenListaTerminales;
     with ParameterList do
          AddString( 'ListaTerminales', FListaTerminales );
     
end;

procedure TWizSistImportaTerminales_DevEx.ObtenListaTerminales;
var
   oArchivo, oRenglon: TStringList;
   iCont: Integer;
begin
     FListaTerminales := VACIO;
     if FileExists( txtTerminales.Text )then
     begin
          oArchivo := TStringList.Create;
          try
             oArchivo.LoadFromFile( Trim( txtTerminales.Text ) );
             oRenglon := TStringList.Create;
             try
                oRenglon.Delimiter := '|';
                for iCont := 0 to (oArchivo.Count -1)do
                begin
                     oRenglon.Clear;
                     oRenglon.DelimitedText := Decrypt( Trim( oArchivo[ iCont ] ) );
                     if( oRenglon.Count > 2 )then
                     begin
                          if( Trim( oRenglon[ 2 ] ) = '1' )then
                          begin
                               if( StrLleno( Trim( oRenglon[ 1 ] ) ) ) then
                               begin
                                    if oRenglon.Count > 3 then
                                       FListaTerminales := ConcatString( FListaTerminales, Trim( oRenglon[ 1 ] ) + '|' + Trim( oRenglon[ 3 ] ), ',' )
                                    else
                                        FListaTerminales := ConcatString( FListaTerminales, Trim( oRenglon[ 1 ] ), ',' );
                               end;
                          end;
                     end
                end;
             finally
                    FreeAndNil( oRenglon );
             end;
          finally
                 FreeAndNil( oArchivo );
          end;
     end;
end;

function TWizSistImportaTerminales_DevEx.EjecutarWizard: Boolean;
begin
     inherited;
     Result := dmProcesos.ImportaTerminales( ParameterList );
end;

procedure TWizSistImportaTerminales_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  {***Asignacion de numeracion a las paginas de wizard***}
  Parametros.PageIndex := 0;
  Ejecucion.PageIndex := 1;
  {***Se denermina el control enfocado de inicio***}
  parametrosControl := txtTerminales;
  {***Texto de Advertencia***}
  Advertencia.Caption:= 'Al aplicar el proceso ser�n importadas las terminales definidas en el archivo de importaci�n.';
end;

procedure TWizSistImportaTerminales_DevEx.WizardAfterMove(Sender: TObject);
begin
  inherited;
  if( Wizard.EsPaginaActual (Parametros) )then
  begin
       {***Se denermina el control enfocado de inicio***}
       parametrosControl := txtTerminales;
  end;
end;

end.
