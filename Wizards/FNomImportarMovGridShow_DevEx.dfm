inherited NomImportarMovGridShow_DevEx: TNomImportarMovGridShow_DevEx
  Left = 205
  Top = 186
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Lista de Errores Detectados'
  ClientWidth = 566
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 566
    inherited OK_DevEx: TcxButton
      Left = 402
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 481
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 566
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object RENGLON: TcxGridDBColumn
        Caption = 'Rengl'#243'n'
        DataBinding.FieldName = 'RENGLON'
        Width = 45
      end
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
        Width = 65
      end
      object CO_NUMERO: TcxGridDBColumn
        Caption = 'Concepto'
        DataBinding.FieldName = 'CO_NUMERO'
        Width = 55
      end
      object MO_MONTO: TcxGridDBColumn
        Caption = 'Monto'
        DataBinding.FieldName = 'MO_MONTO'
        Width = 75
      end
      object MO_REFEREN: TcxGridDBColumn
        Caption = 'Referencia'
        DataBinding.FieldName = 'MO_REFEREN'
        Width = 89
      end
      object NUM_ERROR: TcxGridDBColumn
        Caption = 'Errores'
        DataBinding.FieldName = 'NUM_ERROR'
        Width = 40
      end
      object DESC_ERROR: TcxGridDBColumn
        Caption = 'Descripci'#243'n de Errores'
        DataBinding.FieldName = 'DESC_ERROR'
        Width = 450
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DataSource: TDataSource
    Left = 264
    Top = 168
  end
end
