unit FWizSistBorrarTarjetas_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ComCtrls, Buttons, ExtCtrls, Mask,
     ZetaEdit, ZetaFecha, FWizSistBaseFiltro,
     cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
     cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons,
     dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
     dxWizardControl;

type
  TWizSistBorrarTarjetas_DevEx = class(TWizSistBaseFiltro)
    iTipo: TcxRadioGroup;
    Periodo: TcxGroupBox;
    dInicial: TZetaFecha;
    dInicialLBL: TLabel;
    dFinalLBL: TLabel;
    dFinal: TZetaFecha;
    GRTiposChecadas: TcxRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure iTipoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
    //DevEx Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);

  public
    { Public declarations }
  end;

var
  WizSistBorrarTarjetas_DevEx: TWizSistBorrarTarjetas_DevEx;

implementation

uses DCliente,
     DProcesos,
     ZetaCommonClasses, ZcxWizardBasico;

{$R *.DFM}

procedure TWizSistBorrarTarjetas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          dInicial.Valor := FechaDefault;
          dFinal.Valor := FechaDefault;
     end;
     ParametrosControl := dInicial;
     HelpContext := H80834_Borrar_tarjetas;
     GRTiposChecadas.Enabled := false;

     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;
end;

procedure TWizSistBorrarTarjetas_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddDate( 'FechaInicial', dInicial.Valor );
          AddDate( 'FechaFinal', dFinal.Valor );
          AddInteger( 'Operacion',  iTipo.ItemIndex );
          AddInteger( 'TipoChecada', GRTiposChecadas.ItemIndex);
     end;
     //DevEx
     with Descripciones do
     begin
          AddDate( 'Fecha inicial', dInicial.Valor );
          AddDate( 'Fecha final', dFinal.Valor );
          AddString( 'Datos a borrar', String ( iTipo.Properties.Items.Items [ iTipo.ItemIndex ].Value) );
          AddString( 'Tipos de checadas', String ( GRTiposChecadas.Properties.Items.Items [ GRTiposChecadas.ItemIndex ].Value ) );
     end;
end;

function TWizSistBorrarTarjetas_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.BorrarTarjetas( ParameterList );
end;

procedure TWizSistBorrarTarjetas_DevEx.iTipoClick(Sender: TObject);
begin
     inherited;
     GRTiposChecadas.Enabled := ( iTipo.ItemIndex <> 0 );
end;

procedure TWizSistBorrarTarjetas_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'Al aplicar este proceso se borrar�n todas las tarjetas de asistencia de las fechas indicadas.'
end;

procedure TWizSistBorrarTarjetas_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := dInicial
     else
         ParametrosControl := nil;
     inherited;
end;
     
end.
