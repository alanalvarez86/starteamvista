inherited WizEmpProgCursoGlobal_DevEx: TWizEmpProgCursoGlobal_DevEx
  Left = 272
  Top = 233
  Caption = 'Curso Programado Global'
  ClientHeight = 432
  ClientWidth = 420
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 420
    Height = 432
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que permite asignar un curso programado a un grupo de em' +
        'pleados  con una fecha de referencia.'
      Header.Title = 'Curso programado global'
      object Label2: TLabel
        Left = 42
        Top = 51
        Width = 33
        Height = 13
        Caption = '&Fecha:'
        FocusControl = dFecha
        Transparent = True
      end
      object Label1: TLabel
        Left = 45
        Top = 27
        Width = 30
        Height = 13
        Caption = 'C&urso:'
        FocusControl = CodigoCurso
        Transparent = True
      end
      object dFecha: TZetaFecha
        Left = 78
        Top = 49
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '24/feb/09'
        Valor = 39868.000000000000000000
      end
      object chbOpcional: TCheckBox
        Left = 29
        Top = 73
        Width = 62
        Height = 17
        Alignment = taLeftJustify
        Caption = '&Opcional:'
        TabOrder = 2
      end
      object CodigoCurso: TZetaKeyLookup_DevEx
        Left = 78
        Top = 25
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsCursos
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 197
        Width = 398
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 398
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 330
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 6
        Top = 145
      end
      inherited sFiltroLBL: TLabel
        Left = 31
        Top = 174
      end
      inherited Seleccionar: TcxButton
        Left = 134
        Top = 262
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 58
        Top = 142
      end
      inherited sFiltro: TcxMemo
        Left = 58
        Top = 173
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 58
        Top = 12
      end
      inherited bAjusteISR: TcxButton
        Left = 372
        Top = 174
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
  end
end
