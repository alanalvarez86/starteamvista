unit FAsisAjusIncapaCalGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, Grids, DBGrids, ZetaDBGrid,
  Buttons, ExtCtrls, ZBaseSelectGrid_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, cxControls, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxTextEdit, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
  cxButtons, cxCalendar;

type
  TAsisAjusIncapaCalGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AsisAjusIncapaCalGridSelect_DevEx: TAsisAjusIncapaCalGridSelect_DevEx;

implementation

uses ZetaCommonLists;

{$R *.dfm}

procedure TAsisAjusIncapaCalGridSelect_DevEx.FormShow(Sender: TObject);
begin
 with DataSet do
     begin
          ListaFija( 'AU_STATUS',lfStatusAusencia );
          ListaFija( 'AU_TIPODIA', lfTipoDiaAusencia );
     end;
     inherited;

end;

procedure TAsisAjusIncapaCalGridSelect_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     Excluir.Hint := 'Excluir Los Movimientos Seleccionados';
end;

end.
