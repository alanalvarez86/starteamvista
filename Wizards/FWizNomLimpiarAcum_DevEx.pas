unit FWizNomLimpiarAcum_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, Mask,
     FWizEmpBaseFiltro,
     ZetaWizard,
     ZetaDBTextBox,
     ZetaCommonLists,
     ZetaEdit,
     ZetaKeyCombo,
     ZetaNumero, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl, ZetaEdit_DevEx;

type
  TWizNomLimpiarAcum_DevEx = class(TWizEmpBaseFiltro)
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label1: TLabel;
    iMesInicial: TZetaKeyCombo;
    iMesFinal: TZetaKeyCombo;
    GroupBox1: TGroupBox;
    lbInicialConcepto: TLabel;
    lbFinalConcepto: TLabel;
    RBTodosConcepto: TRadioButton;
    RBRangoConcepto: TRadioButton;
    RBListaConcepto: TRadioButton;
    EInicialConcepto: TZetaEdit_DevEx;
    EFinalConcepto: TZetaEdit_DevEx;
    EListaConcepto: TZetaEdit_DevEx;
    BInicialConcepto: TcxButton;
    BFinalConcepto: TcxButton;
    BListaConcepto: TcxButton;
    Panel1: TPanel;
    YearLBL: TLabel;
    Year: TZetaNumero;
    procedure FormCreate(Sender: TObject);
    procedure RBTodosConceptoClick(Sender: TObject);
    procedure RBRangoConceptoClick(Sender: TObject);
    procedure RBListaConceptoClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure BInicialConceptoClick(Sender: TObject);
    procedure BFinalConceptoClick(Sender: TObject);
    procedure BListaConceptoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);//DevEx(by am): Agregado para enfocar correctamente los controles.
  private
    { Private declarations }
    FRangoConcepto: eTipoRangoActivo;
    procedure EnabledBotonesConcepto( eTipo: eTipoRangoActivo );
    procedure BuscaDialogo(oEdit: TZetaEdit_DevEx; const lConcatena: Boolean);
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function GetConceptos: String;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizNomLimpiarAcum_DevEx: TWizNomLimpiarAcum_DevEx;

implementation

uses FTressShell,
     ZetaClientTools,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo,
     DProcesos,
     DCliente,
     DRecursos;

{$R *.DFM}

procedure TWizNomLimpiarAcum_DevEx.FormCreate(Sender: TObject);
var
   iMes: integer;
begin
     inherited;
     HelpContext:= H33410_Limpiar_acumulados;
     ParametrosControl := Year;
     iMes := ( dmCliente.GetDatosPeriodoActivo.Mes - 1 );
     iMesInicial.Valor := iMes;
     iMesFinal.Valor := iMes;
     Year.Valor := dmCliente.YearDefault;
     EnabledBotonesConcepto( raTodos );

     //DevEx(by am): Es necesario especificar que Ejecucion sera la ultima pagina del wizard.
     Parametros.PageIndex := WizardControl.PageCount - 3;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 2;
     Ejecucion.PageIndex := WizardControl.PageCount - 1;
end;

function TWizNomLimpiarAcum_DevEx.GetConceptos : string;
begin
     case FRangoConcepto of
          raRango: Result := ZetaClientTools.GetFiltroRango( 'CO_NUMERO', Trim( EInicialConcepto.Text ), Trim( EFinalConcepto.Text ) );
          raLista: Result := ZetaClientTools.GetFiltroLista( 'CO_NUMERO', Trim( EListaConcepto.Text ) );
     end;
end;

procedure TWizNomLimpiarAcum_DevEx.EnabledBotonesConcepto( eTipo : eTipoRangoActivo );
var
   lLista, lRango: Boolean;
begin
     FRangoConcepto := eTipo;
     lRango := ( eTipo = raRango );
     lLista := ( eTipo = raLista );
     lbInicialConcepto.Enabled := lRango;
     lbFinalConcepto.Enabled := lRango;
     EInicialConcepto.Enabled := lRango;
     EFinalConcepto.Enabled := lRango;
     bInicialConcepto.Enabled := lRango;
     bFinalConcepto.Enabled := lRango;
     EListaConcepto.Enabled := lLista;
     bListaConcepto.Enabled := lLista;
end;

procedure TWizNomLimpiarAcum_DevEx.BInicialConceptoClick(Sender: TObject);
begin
     inherited;
     BuscaDialogo( EInicialConcepto, FALSE );
end;

procedure TWizNomLimpiarAcum_DevEx.BFinalConceptoClick(Sender: TObject);
begin
     inherited;
     BuscaDialogo( EFinalConcepto, FALSE );
end;

procedure TWizNomLimpiarAcum_DevEx.BListaConceptoClick(Sender: TObject);
begin
     inherited;
     BuscaDialogo( EListaConcepto, TRUE );
end;

procedure TWizNomLimpiarAcum_DevEx.BuscaDialogo( oEdit: TZetaEdit_DevEx; const lConcatena: Boolean );
var
   sKey, sDescripcion, sLlave: String;
begin
     sLlave := oEdit.Text;
     TressShell.BuscaDialogo( enConcepto, '', sKey, sDescripcion );
     if lConcatena and ZetaCommonTools.StrLleno( sLlave ) then
     begin
          if strLleno( sKey ) then
              oEdit.Text := sLlave + ',' + sKey
          else
              oEdit.Text := sLlave
     end
     else if strLleno( sKey ) then
          oEdit.Text := sKey;
end;

procedure TWizNomLimpiarAcum_DevEx.RBTodosConceptoClick(Sender: TObject);
begin
     inherited;
     EnabledBotonesConcepto( raTodos );
end;

procedure TWizNomLimpiarAcum_DevEx.RBRangoConceptoClick(Sender: TObject);
begin
     inherited;
     EnabledBotonesConcepto( raRango );
end;

procedure TWizNomLimpiarAcum_DevEx.RBListaConceptoClick(Sender: TObject);
begin
     inherited;
     EnabledBotonesConcepto( raLista );
end;

procedure TWizNomLimpiarAcum_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin

     ParametrosControl := nil;
     if Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
     begin
          CanMove := ( iMesInicial.Valor <= iMesFinal.Valor );
          if not CanMove then
          begin
               ZError( Caption, 'El Mes Inicial Debe Ser Menor o Igual al Mes Final', 0 );
               ActiveControl := iMesInicial;
          end;
     end;
     inherited;
end;

procedure TWizNomLimpiarAcum_DevEx.CargaParametros;
begin
     inherited;
     with Descripciones do
     begin
          AddString( 'Mes Inicial' , ObtieneElemento(lfMeses, iMesInicial.Valor));
          AddString( 'Mes Final', ObtieneElemento(lfMeses, iMesFinal.Valor));
          AddInteger( 'A�o' , Year.ValorEntero );
     end;
     with ParameterList do
     begin
          AddInteger( 'MesInicial' , iMesInicial.Valor + 1 );
          AddInteger( 'MesFinal', iMesFinal.Valor + 1 );
          AddInteger( 'Year' , Year.ValorEntero );
          AddString( 'Conceptos', GetConceptos );
     end;
end;

function TWizNomLimpiarAcum_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.LimpiarAcum(ParameterList);
end;

procedure TWizNomLimpiarAcum_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption :='El proceso borrar� los registros de acumulados para el rango de meses y grupo de empleados seleccionados.';
end;

procedure TWizNomLimpiarAcum_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.Adelante then
          if Wizard.EsPaginaActual( Parametros ) then
             ParametrosControl := Year;
     inherited;
end;

end.
