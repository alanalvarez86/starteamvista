inherited WizNomReFoliarRecibos_DevEx: TWizNomReFoliarRecibos_DevEx
  Caption = 'Re-Foliar Recibos'
  ClientHeight = 370
  ClientWidth = 434
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 434
    Height = 370
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso para re-foliar los recibos de n'#243'mina debido a que alg'#250'n ' +
        'recibo de n'#243'mina no se folio correctamente.'
      Header.Title = 'Re-foliar recibos'
      inherited GroupBox1: TGroupBox
        Left = 0
        Top = 45
        Width = 412
        Align = alBottom
        TabOrder = 1
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 412
        Height = 41
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object Label5: TLabel
          Left = 31
          Top = 14
          Width = 25
          Height = 13
          Alignment = taRightJustify
          Caption = '&Folio:'
          FocusControl = nFolio
          Transparent = True
        end
        object nFolio: TZetaKeyLookup_DevEx
          Left = 64
          Top = 10
          Width = 337
          Height = 21
          LookupDataset = dmCatalogos.cdsFolios
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 135
        Width = 412
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 412
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 344
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      PageVisible = False
      inherited sFiltro: TcxMemo
        Style.IsFontAssigned = True
      end
    end
    object Folios: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = ' Complete los par'#225'metros para indicar el rango de la numeraci'#243'n.'
      Header.Title = 'Par'#225'metros de numeraci'#243'n'
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 412
        Height = 121
        Align = alTop
        Caption = ' Renumeraci'#243'n '
        TabOrder = 0
        object Label10: TLabel
          Left = 102
          Top = 30
          Width = 72
          Height = 13
          Alignment = taRightJustify
          Caption = 'Renumerar &del:'
          FocusControl = nRenumeraDel
        end
        object Label6: TLabel
          Left = 108
          Top = 54
          Width = 66
          Height = 13
          Alignment = taRightJustify
          Caption = 'Renumerar &al:'
          FocusControl = nRenumeraAl
        end
        object Label7: TLabel
          Left = 104
          Top = 78
          Width = 70
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero &Inicial:'
          FocusControl = NumeroInicial
        end
        object nRenumeraDel: TZetaNumero
          Left = 179
          Top = 26
          Width = 121
          Height = 21
          Mascara = mnEmpleado
          TabOrder = 0
        end
        object nRenumeraAl: TZetaNumero
          Left = 179
          Top = 50
          Width = 121
          Height = 21
          Mascara = mnEmpleado
          TabOrder = 1
        end
        object NumeroInicial: TZetaNumero
          Left = 179
          Top = 74
          Width = 121
          Height = 21
          Mascara = mnEmpleado
          TabOrder = 2
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Top = 297
  end
end
