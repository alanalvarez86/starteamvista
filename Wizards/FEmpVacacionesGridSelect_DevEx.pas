unit FEmpVacacionesGridSelect_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, StdCtrls, Buttons, ExtCtrls, Db,
     ZBaseSelectGrid_DevEx,
     ZetaDBGrid, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxButtons;

type
  TEmpVacacionesGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CB_TOT_GOZ: TcxGridDBColumn;
    CB_TOT_PAG: TcxGridDBColumn;
    CB_TOT_PV: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpVacacionesGridSelect_DevEx: TEmpVacacionesGridSelect_DevEx;

implementation

{$R *.DFM}

procedure TEmpVacacionesGridSelect_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with DataSet do
     begin
          MaskPesos( 'CB_TOT_GOZ' );
          MaskPesos( 'CB_TOT_PAG' );
          MaskPesos( 'CB_TOT_PV' );
     end;
end;

end.
