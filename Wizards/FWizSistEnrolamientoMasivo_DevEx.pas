unit FWizSistEnrolamientoMasivo_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, ExtCtrls, Mask, Buttons, ComCtrls,
     ZetaFecha,     
     ZetaEdit,
     ZetaWizard, CheckLst, FWizEmpBaseFiltro, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl, cxCheckListBox,
  FWizSistBaseFiltro, dxSkinsCore;

type
  TWizSistEnrolamientoMasivo_DevEx = class(TWizSistBaseFiltro)
    FormulaUsuario: TcxMemo;
    Label1: TLabel;
    BtnFormulaUsuario: TcxButton;
    BtnFormulaPwd: TcxButton;
    FormulaPwd: TcxMemo;
    Label2: TLabel;
    Label3: TLabel;
    FormulaMail: TcxMemo;
    BtnFormulaMail: TcxButton;
    Label8: TLabel;
    GrupoDefault: TZetaKeyLookup_DevEx;
    RolesDefault: TcxCheckListBox;
    lblRoles: TLabel;
    chUsaPortal: TCheckBox;
    US_JEFE: TZetaKeyLookup_DevEx;
    Label16: TLabel;
    BtnFormulaActiveDirectory: TcxButton;
    FormulaActiveDirectory: TcxMemo;
    Label4: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtnFormulaUsuarioClick(Sender: TObject);
    procedure BtnFormulaPwdClick(Sender: TObject);
    procedure BtnFormulaMailClick(Sender: TObject);
    procedure BtnFormulaActiveDirectoryClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
    procedure CargarListaDefault;
    function DesCargarListaRoles: string;

  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
    function Verificar: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizSistEnrolamientoMasivo_DevEx: TWizSistEnrolamientoMasivo_DevEx;

implementation

uses DCliente,
     DProcesos,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZBaseSelectGrid_DevEx,
     ZGlobalTress,
     DGlobal,
     ZConstruyeFormula,
     ZetaCommonLists,
     ZAccesosTress,
     ZetaTipoEntidad,
     ZetaDialogo,
     FAutoClasses,
     DSistema,
     DBaseSistema,
     FSistEnrolamientoMasivoGridSelect_DevEx;


{$R *.DFM}

procedure TWizSistEnrolamientoMasivo_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := FormulaUsuario;
     US_JEFE.LookupDataset := dmSistema.cdsUsuariosLookup;
     HelpContext := H_Enrolamiento_Masivo;
end;

procedure TWizSistEnrolamientoMasivo_DevEx.CargaParametros;
begin


     inherited CargaParametros;

     with ParameterList do
     begin
          AddString( 'Usuario', FormulaUsuario.Text );
          AddString( 'Clave', FormulaPwd.Text );
          AddString( 'CorreoElectrónico', FormulaMail.Text );
          AddInteger( 'GrupoUsuarios', GrupoDefault.Valor );
          AddBoolean( 'UsaPortal', chUsaPortal.Checked );
          AddString( 'RolesAsignados',DesCargarListaRoles );
          if ( US_JEFE.Visible ) then
             AddInteger( 'ReportaA', US_JEFE.Valor )
          else
              AddInteger( 'ReportaA', 0);

          AddString( 'FormulaAD',FormulaActiveDirectory.Text  );
     end;
        with Descripciones do
     begin
          AddString( 'Usuario', FormulaUsuario.Text );
          AddString( 'Clave', FormulaPwd.Text );
          AddString( 'Correo Electrónico', FormulaMail.Text );
           AddString('Usuario del Dominio',  FormulaActiveDirectory.text);
          AddInteger( 'Grupo Usuarios', GrupoDefault.Valor );
          AddBoolean( 'Usa Portal', chUsaPortal.Checked );
          AddString( 'Roles asignados', DesCargarListaRoles );
          if ( US_JEFE.Visible ) then
             AddInteger( 'Reporta a', US_JEFE.Valor )
          else
             AddInteger( 'Reporta a', 0 );

     end;
end;


function TWizSistEnrolamientoMasivo_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.EnrolarUsuarios(ParameterList);
end;

procedure TWizSistEnrolamientoMasivo_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with Global do
     begin
          with dmSistema do
          begin
               GrupoDefault.LookupDataset := cdsGrupos;
               US_JEFE.LookupDataset := cdsUsuariosLookup;
               cdsUsuarios.Conectar;
               cdsGrupos.Conectar;
          end;
          FormulaUsuario.Text := GetGlobalString(K_GLOBAL_ENROLL_FORMULA_USER);
          FormulaPwd.Text := GetGlobalString(K_GLOBAL_ENROLL_FORMULA_PWD);
          FormulaMail.Text := GetGlobalString(K_GLOBAL_ENROLL_FORMULA_MAIL);
          GrupoDefault.Valor := GetGlobalInteger(K_GLOBAL_ENROLL_GRUPO_DEFAULT );
          FormulaActiveDirectory.Text := GetGlobalString(K_GLOBAL_ENROLL_FORMULA_AD );
          chUsaPortal.Checked := dmCliente.ModuloAutorizadoDerechos( okPortal );
          CargarListaDefault;

          US_JEFE.Visible := not GetGlobalBooleano( K_GLOBAL_ENROLAMIENTO_SUPERVISORES);
          Label16.Visible := US_JEFE.Visible;

     end;
end;

procedure TWizSistEnrolamientoMasivo_DevEx.BtnFormulaUsuarioClick(
  Sender: TObject);
begin
     inherited;
     FormulaUsuario.Text := GetFormulaConst( enEmpleado, FormulaUsuario.Lines.Text, FormulaUsuario.SelStart, evBase  );
end;

procedure TWizSistEnrolamientoMasivo_DevEx.BtnFormulaPwdClick(Sender: TObject);
begin
  inherited;
   FormulaPwd.Text := GetFormulaConst( enEmpleado, FormulaPwd.Lines.Text, FormulaPwd.SelStart, evBase  );
end;

procedure TWizSistEnrolamientoMasivo_DevEx.BtnFormulaMailClick(Sender: TObject);
begin
     inherited;
     FormulaMail.Text := GetFormulaConst( enEmpleado, FormulaMail.Lines.Text, FormulaMail.SelStart, evBase  );
end;

procedure TWizSistEnrolamientoMasivo_DevEx.CargarListaDefault;
{$ifndef DOS_CAPAS}
var
   sCodigo, sNombre, sRoles,sRegistro : string;
{$endif}
begin
     inherited;
     {$ifndef DOS_CAPAS}
     sRoles := Global.GetGlobalString(ZGlobalTress.K_GLOBAL_ENROLL_ROLES_DEFAULT) +  Global.GetGlobalString(ZGlobalTress.K_GLOBAL_ENROLL_ROLES_DEFAULT2);
     //Cargar los roles en el CheckListBox
     with dmSistema.cdsRoles do
     begin
          Conectar;
          First;
          RolesDefault.Clear;
          while not Eof do
          begin
               sCodigo := FieldByName('RO_CODIGO').AsString;
               sNombre := FieldByName('RO_NOMBRE').AsString;
               sRegistro := sCodigo + ' : ' + sNombre;
               with RolesDefault.Items.Add do
               begin
                  text := sRegistro;
                  if ( pos( sCodigo + ',', sRoles ) > 0 ) then
                  begin
                    Checked := True;
                  end;
               end;
               Next;
          end;
     end;
     {$endif}
end;

function TWizSistEnrolamientoMasivo_DevEx.DesCargarListaRoles:string;
{$ifndef DOS_CAPAS}
var
   sRolesSeleccionados,sCodigo : string;
   i:Integer;
{$endif}
begin
     Result := VACIO;
     {$ifndef DOS_CAPAS}
     sRolesSeleccionados := VACIO;
     for i := 0 to RolesDefault.Items.Count - 1 do
     begin
          if RolesDefault.Items[i].Checked then
          begin
               sCodigo := RolesDefault.Items[ i ].Text;
               sCodigo := Trim( copy( sCodigo, 1, pos( ' : ', sCodigo ) ) ) + ',';
               //Acumular codigos
               sRolesSeleccionados := sRolesSeleccionados + sCodigo;
          end;
     end;
     Result := sRolesSeleccionados;
     {$endif}
end;


procedure TWizSistEnrolamientoMasivo_DevEx.BtnFormulaActiveDirectoryClick(
  Sender: TObject);
begin
     inherited;
     FormulaActiveDirectory.Text := GetFormulaConst( enEmpleado, FormulaActiveDirectory.Lines.Text, FormulaActiveDirectory.SelStart, evBase  );
end;

procedure TWizSistEnrolamientoMasivo_DevEx.CargaListaVerificacion;
begin
     inherited;
     dmProcesos.EnrolarUsuariosGetLista(ParameterList);
end;


function TWizSistEnrolamientoMasivo_DevEx.Verificar: Boolean;
begin
      Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TSistEnrolamientoMasivoGridSelect_DevEx );
end;

procedure TWizSistEnrolamientoMasivo_DevEx.WizardBeforeMove(
  Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
  inherited;
ParametrosControl := nil;
end;

procedure TWizSistEnrolamientoMasivo_DevEx.WizardAfterMove(
  Sender: TObject);
begin
  inherited;
   if Wizard.EsPaginaActual(Parametros) then
    ParametrosControl := FormulaUsuario;
end;

end.
