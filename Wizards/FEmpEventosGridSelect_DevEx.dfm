inherited EmpEventosGridSelect_DevEx: TEmpEventosGridSelect_DevEx
  Left = 323
  Top = 318
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Aplicar Eventos'
  ClientHeight = 255
  ClientWidth = 622
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 219
    Width = 622
    inherited OK_DevEx: TcxButton
      Left = 458
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 537
    end
  end
  inherited PanelSuperior: TPanel
    Width = 622
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 622
    Height = 184
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.Indicator = True
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
        Width = 65
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        Width = 96
      end
      object EV_CODIGO: TcxGridDBColumn
        Caption = 'Evento'
        DataBinding.FieldName = 'EV_CODIGO'
        Width = 60
      end
      object TV_FECHA: TcxGridDBColumn
        Caption = 'Fecha Reg.'
        DataBinding.FieldName = 'TV_FECHA'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
        Width = 75
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
