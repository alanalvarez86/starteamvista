inherited WizEmpAplicarTabulador_DevEx: TWizEmpAplicarTabulador_DevEx
  Left = 399
  Top = 173
  Caption = 'Aplicar Tabulador de Salario'
  ClientHeight = 479
  ClientWidth = 518
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 518
    Height = 479
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Este proceso permite igualar el salario de los empleados al nuev' +
        'o salario de su clasificaci'#243'n correspondiente, de acuerdo a los ' +
        'cambios que se hayan hecho en el tabulador de salarios.'
      Header.Title = 'Aplicar Tabulador de Salario'
      object dReferenciaLBL: TLabel
        Left = 60
        Top = 11
        Width = 86
        Height = 13
        Alignment = taRightJustify
        Caption = '&Fecha de Cambio:'
        FocusControl = dReferencia
        Transparent = True
      end
      object sDescripcionLBL: TLabel
        Left = 87
        Top = 138
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = '&Descripci'#243'n:'
        FocusControl = sDescripcion
        Transparent = True
      end
      object sObservaLBL: TLabel
        Left = 72
        Top = 164
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = '&Observaciones:'
        FocusControl = sObserva
        Transparent = True
      end
      object Label1: TLabel
        Left = 32
        Top = 35
        Width = 114
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha de aviso a &IMSS:'
        FocusControl = dIMSS
        Transparent = True
      end
      object dReferencia: TZetaFecha
        Left = 150
        Top = 6
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '02/ene/98'
        Valor = 35797.000000000000000000
        OnValidDate = dReferenciaValidDate
      end
      object RGRevisar: TcxRadioGroup
        Left = 150
        Top = 52
        Caption = ' Revisar '
        Properties.Items = <
          item
            Caption = 'Todo el Tabulador'
            Value = '0'
          end
          item
            Caption = 'Solo diferencias de salario diario'
            Value = '1'
          end>
        ItemIndex = 0
        TabOrder = 2
        Height = 70
        Width = 290
      end
      object sDescripcion: TEdit
        Left = 150
        Top = 132
        Width = 290
        Height = 21
        TabOrder = 3
        Text = 'Modificaci'#243'n de Tabulador'
      end
      object sObserva: TcxMemo
        Left = 150
        Top = 160
        Lines.Strings = (
          '')
        TabOrder = 4
        Height = 153
        Width = 290
      end
      object lIncapacitados: TcxCheckBox
        Left = 28
        Top = 318
        Caption = 'No Incluir Incapacitados:'
        Properties.Alignment = taRightJustify
        State = cbsChecked
        TabOrder = 5
        Transparent = True
        Width = 139
      end
      object dIMSS: TZetaFecha
        Left = 150
        Top = 30
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '02/ene/98'
        Valor = 35797.000000000000000000
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 244
        Width = 496
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 496
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 428
          AnchorX = 281
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited Seleccionar: TcxButton
        Visible = True
      end
      inherited sFiltro: TcxMemo
        Style.IsFontAssigned = True
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    Top = 327
  end
end
