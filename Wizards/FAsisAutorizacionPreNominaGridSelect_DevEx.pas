unit FAsisAutorizacionPreNominaGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons,
  ExtCtrls, ZBaseSelectGrid_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, cxControls, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons,ZetaDialogo,FBaseReportes_DevEx;

type
  TAsisAutoricacionPreNominaGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    procedure FormShow(Sender: TObject);
    procedure ExportarClick(Sender: TObject);
    procedure ImprimirClick(Sender: TObject);
  private
    { Private declarations }
    procedure NO_SUP_OK_GetText( Sender: TField; var Text: String; DisplayText: Boolean);
    procedure SetColumnas;
    procedure FormatoColumnas;
  public
    { Public declarations }
  end;

var
  AsisAutoricacionPreNominaGridSelect_DevEx: TAsisAutoricacionPreNominaGridSelect_DevEx;

implementation

uses ZBasicoSelectGrid_DevEx,DSistema,ZetaClientDataSet,ZetaCommonClasses,DCliente,DBClient,
  DBaseSistema;

{$R *.DFM}

procedure TAsisAutoricacionPreNominaGridSelect_DevEx.FormShow(Sender: TObject);
begin
     SetColumnas;

        SetControls;
        inherited ;
        {
     //ZetadbGridDBTableView.DataController.CreateAllItems();
     ZetaDbGridDbTableView.ApplyBestFit();
     for I:=0 to ZetadbGridDbtableview.ColumnCount -1 do
     begin
  //   Zetadbgriddbtableview.Columns[i].Width:= Zetadbgriddbtableview.Columns[i].Width + 25;
  if(zetadbgriddbtableview.Columns[i].DataBinding.FieldName='CB_CODIGO') then  zetadbgriddbtableview.Columns[i].Caption:='Codigo' ;
  if(zetadbgriddbtableview.Columns[i].DataBinding.FieldName='PRETTYNAME') then zetadbgriddbtableview.Columns[i].Caption:='Nombre';
     end;
     }
     FormatoColumnas;
     applyminwidth;
end;


procedure TAsisAutoricacionPreNominaGridSelect_DevEx.FormatoColumnas;
begin
     with Dataset do
     begin
          MaskPesos( 'NO_HORAS' );
          MaskPesos( 'NO_DOBLES' );
          MaskPesos( 'NO_TRIPLES' );
          MaskPesos( 'NO_HORA_CG' );
          MaskPesos( 'NO_HORA_SG' );
          MaskPesos( 'NO_DIAS_IN' );
          MaskPesos( 'NO_DIAS_VA' );
          MaskPesos( 'NO_DES_TRA' );
     end;
end;

procedure TAsisAutoricacionPreNominaGridSelect_DevEx.SetColumnas;
begin
     dmSistema.cdsUsuarios.Conectar;
     if DataSet.FieldByName('NO_SUP_OK').AsInteger = 0 then
     begin
          ZetaDBGridDbTableView.Columns[10].Caption := 'Autorizará';
     end
     else
     begin
         ZetaDBGridDbTableView.Columns[10].Caption := 'Autorizado por';
     end;
     DataSet.FieldByName('NO_SUP_OK').OnGetText := NO_SUP_OK_GetText;

end;

procedure TAsisAutoricacionPreNominaGridSelect_DevEx.NO_SUP_OK_GetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     If Sender.AsInteger = 0 then
        Text := dmSistema.cdsUsuariosLookup.GetDescripcion(IntToStr(dmCliente.Usuario))
     else
         Text := dmSistema.cdsUsuariosLookup.GetDescripcion(Sender.AsString);
end;

procedure TAsisAutoricacionPreNominaGridSelect_DevEx.ExportarClick(
  Sender: TObject);
begin
   if ZetaDialogo.zConfirm( 'Exportar...', '¿ Desea exportar a Excel la lista mostrada ?', 0, mbYes ) then
        FBaseReportes_DevEx.ExportarGridParams( ZetaDBGridDBTableView, ZetaDBGridDBTableView.DataController.DataSource.DataSet, ParametrosGrid, Caption, 'IM');

end;

procedure TAsisAutoricacionPreNominaGridSelect_DevEx.ImprimirClick(
  Sender: TObject);
begin
 if ZetaDialogo.zConfirm( 'Imprimir...', '¿ Desea Imprimir La Lista Mostrada ?', 0, mbYes ) then
        FBaseReportes_DevEx.ImprimirGridParams( ZetaDBGridDBTableView, ZetaDBGridDBTableView.DataController.DataSource.DataSet, Caption, 'IM', ParametrosGrid);
end;

end.
