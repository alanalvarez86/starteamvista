unit FWizNomFonacotImportarCedula_DevEx;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FWizNomBasico_DevEx, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
  TressMorado2013, cxContainer, cxEdit, Vcl.Menus, Vcl.StdCtrls, ZetaKeyCombo,
  Vcl.Mask, ZetaNumero, ZetaCXWizard, ZetaEdit, cxRadioGroup, cxTextEdit,
  cxMemo, Vcl.ExtCtrls, ZetaKeyLookup_DevEx, cxButtons, dxGDIPlusClasses,
  cxImage, cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl,
  dxCheckGroupBox, Vcl.ComCtrls, cxSpinEdit, cxMaskEdit, cxDropDownEdit,
  ZetaCXStateComboBox, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, System.Actions, Vcl.ActnList,
  Vcl.ImgList, ZetaCommonClasses, ZetaDBTextBox, StrUtils, IOUtils;

  type
   TMenuItemExtended = class(TMenuItem)
   private
     fValue: string;
   published
     property Value : string read fValue write fValue;
   end;

type
  TWizNomFonacotImportarCedula_DevEx = class(TWizNomBasico_DevEx)
    Label2: TLabel;
    Label3: TLabel;
    grbArchivoCedulaFonacot: TcxGroupBox;
    Label5: TLabel;
    sArchivo: TEdit;
    ArchivoSeek: TcxButton;
    OpenDialog: TOpenDialog;
    PanelAnimation: TPanel;
    PanelMensaje: TPanel;
    Opciones: TdxWizardControlPage;
    chbNumeroFonacot: TCheckBox;
    grbCedulaAnterior: TcxGroupBox;
    rbNoSustituir: TcxRadioButton;
    rbSustituirAlgunos: TcxRadioButton;
    rbSustituirTodos: TcxRadioButton;
    zcboMes: TcxStateComboBox;
    txtAnio: TcxSpinEdit;
    lbRazonsocial: TLabel;
    EmpleadosNoEncontrados: TdxWizardControlPage;
    ZetaDBGrid: TZetaCXGrid;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    NFonacot: TcxGridDBColumn;
    NEmpleado: TcxGridDBColumn;
    RFC: TcxGridDBColumn;
    ZetaDBGridLevel: TcxGridLevel;
    dsEmpleadosNoEncontrados: TDataSource;
    btnBuscarEmp_DevEx: TcxButton;
    Nombre: TcxGridDBColumn;
    Elegido: TcxGridDBColumn;
    ActionList: TActionList;
    PopupMenu1: TPopupMenu;
    _BuscarEmpleado: TAction;
    cxImage16: TcxImageList;
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    btnDesmarcarTodas: TcxButton;
    btnSeleccionarTodos: TcxButton;
    cxImageList1: TcxImageList;
    pAvance: TProgressBar;
    OcultarAsignados: TCheckBox;
    Label4: TLabel;
    chkPrestamosActivos: TCheckBox;
    zTipoArchivo: TZetaTextBox;
    RS_CODIGO: TZetaKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;var CanMove: Boolean);
    procedure NEmpleadoCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure btnBuscarEmp_DevExClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewFocusedItemChanged( Sender: TcxCustomGridTableView; APrevFocusedItem, AFocusedItem: TcxCustomGridTableItem);
    procedure mModificarClick(Sender: TObject);
    procedure QuitarEmpleadoClick(Sender: TObject);
    procedure btnSeleccionarTodosClick(Sender: TObject);
    procedure btnDesmarcarTodasClick(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
    procedure OcultarAsignadosClick(Sender: TObject);
  private
    { Private declarations }
    ArchivoVerif : String;
    iTipoArchivoACargar : Integer;
    sMes : String;
    sAnio : String;
    iTipoCedula : Integer;
    function GetArchivo: String;
    function LeerArchivo: Boolean;
    procedure HabilitaFiltroRazonSocial;
    function ExisteCedula (sRS_CODIGO: String): Boolean;
    function ExisteCedulaPorRazon: Boolean;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function VerificarTipoCedula : Integer;
    procedure CargaParametros; override;
    procedure EscogerEmpleado(Sender: TObject);
    procedure ActualizarMismoFonacot(sFonacot, sNombre, sEmpleado : String);
    procedure AsignarEmpleado(sNombre, sEmpleado : String);
    procedure CargaEmpleados;
    procedure cambiarSeleccion (elegir: Boolean);
  public
    { Public declarations }
  end;

const
     K_ERROR_FORMATO = 'El archivo no corresponde a los formatos esperados, verifique que coincidan' + CR_LF + 'con los publicados por Fonacot.';

var
  WizNomFonacotImportarCedula_DevEx: TWizNomFonacotImportarCedula_DevEx;

implementation

uses DCliente,
     DConsultas,
     ZetaClientTools,
     ZetaCommonTools,
     ZetaCommonLists,
     DProcesos,
     ZAsciiTools,
     ZBaseGridShow_DevEx,
     ZetaDialogo,
     dCatalogos,
     ZetaBuscaEmpleado_DevEx,
     FWizNomFonacotImportarCedulaGridShow_DevEx,
     DNomina;

{$R *.dfm}

procedure TWizNomFonacotImportarCedula_DevEx.ArchivoSeekClick(Sender: TObject);
const K_ERROR = 'CANNOT OPEN FILE';
var lEnabled, lError : Boolean;
    iMes : Integer;
   sArchivoSinRuta : string;
   sExtensionArchivo : string;
begin
     inherited;
     sArchivo.Text := ZetaClientTools.AbreDialogo( OpenDialog, sArchivo.Text, 'csv' );
     lError := False;

     if StrLleno ( sArchivo.Text) then
     begin
          try
             sArchivoSinRuta := VACIO;
             iTipoArchivoACargar := VerificarTipoCedula;
          except
                on Error: Exception do
                begin
                     if Pos (K_ERROR, UpperCase (Error.Message)) > 0 then
                     begin
                          sExtensionArchivo := TPath.GetExtension (sArchivo.Text);
                          sArchivoSinRuta := TPath.GetFileNameWithoutExtension (sArchivo.Text);
                          lError := True;
                          sArchivo.Text := VACIO;
                          sArchivoSinRuta := sArchivoSinRuta + sExtensionArchivo;
                          DatabaseError (Format ('El archivo "%s" est� abierto en otra aplicaci�n, cierre e intente de nuevo', [sArchivoSinRuta]));
                     end;
                end;
          end;

          if Not ( lError ) then
          begin
               if (iTipoArchivoACargar > 0) then
               begin
                    iTipoArchivoACargar := iTipoArchivoACargar - 1;
                    zTipoArchivo.Caption := ObtieneElemento ( lfTipoArchivoCedula, iTipoArchivoACargar );
                    lEnabled := True;
                    if iTipoArchivoACargar = ord ( etacPorTrabajador ) then
                    begin
                         lEnabled := False;
                         txtAnio.Text := sAnio;
                         if strLleno ( sMes ) then
                         begin
                              iMes := StrToInt ( sMes );
                              if ( ( iMes < 1 ) or ( iMes > 12 ) ) then
                                 iMes := 1;
                         end
                         else
                             iMes := 1;
                         zcboMes.Indice := iMes;
                    end;

                    txtAnio.Enabled := lEnabled;
                    zcboMes.Enabled := lEnabled;

                    iTipoArchivoACargar := iTipoArchivoACargar + 1;
               end
               else if (sArchivoSinRuta = VACIO) then
               begin
                    zTipoArchivo.Caption := '(Favor de seleccionar archivo)';
                    ZetaDialogo.ZWarning( self.Caption, K_ERROR_FORMATO, 0, mbOk);
                    sArchivo.Text := VACIO;
               end;
          end;
     end;
end;

function TWizNomFonacotImportarCedula_DevEx.VerificarTipoCedula: Integer;
const
     K_POR_CREDITO = 'No_FONACOT';
     K_POR_TRABAJADOR = 'NO_CT';
var
   sEncabezado, sLinea : String;
   Lista, ListaEncabezado: TStringList;

   function ObtenerEncabezado (sFile: String): String;
   var FArchivo : TStrings;
   begin
        FArchivo := TStringList.Create;
        try
           FArchivo.LoadFromFile( sFile );

           if FArchivo.Count > 0 then
           begin
                Result := FArchivo[0];
                sLinea := FArchivo[1];
           end;
        finally
               FArchivo.Free;
        end;
   end;

   procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ;
   begin
         ListOfStrings.Clear;
         ListOfStrings.Delimiter       := Delimiter;
         ListOfStrings.StrictDelimiter := True; // Requires D2006 or newer.
         ListOfStrings.DelimitedText   := Str;
   end;
begin
     ListaEncabezado := TStringList.Create;
     try
        sLinea := VACIO;
        sEncabezado :=  ObtenerEncabezado(sArchivo.Text);
        Result := 0;

        Split(',', sEncabezado, ListaEncabezado) ;
        if ListaEncabezado[0] = K_POR_CREDITO then
           Result := 1
        else if ListaEncabezado[0] = K_POR_TRABAJADOR then
             Result := 2;

        if StrLleno( sLinea ) and ( ListaEncabezado[0] = K_POR_TRABAJADOR )  then
        begin
             Lista := TStringList.Create;
             try
                Split(',', sLinea, Lista) ;
                sAnio := Lista [1];
                sMes := Lista [2];
             finally
                    Lista.Free;
             end;
        end;
     finally
            ListaEncabezado.Free;
     end;
end;

procedure TWizNomFonacotImportarCedula_DevEx.FormCreate(Sender: TObject);
begin
     inherited;

     ParametrosControl := txtAnio;
     HelpContext:= H_WIZ_IMPORTAR_CEDULA_FONACOT;

     with dmCatalogos.cdsRSocial do
     begin
          Conectar;
     end;
     HabilitaFiltroRazonSocial;
     RS_CODIGO.LookupDataset := dmCatalogos.cdsRSocial;

     //DevEx
     Parametros.PageIndex := 0;
     Opciones.PageIndex := 1;
     FiltrosCondiciones.PageIndex := 2;
     EmpleadosNoEncontrados.PageIndex := 3;
     Ejecucion.PageIndex := 4;

     PanelAnimation.Visible := FALSE;

     RS_CODIGO.LookupDataSet:= dmCatalogos.cdsRSocial;
end;

procedure TWizNomFonacotImportarCedula_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     chbNumeroFonacot.Checked := TRUE;
     with dmCliente do
     begin
          txtAnio.Value := GetDatosPeriodoActivo.Year;
          zcboMes.ItemIndex := GetDatosPeriodoActivo.Mes - 1;
          if zcboMes.ItemIndex = -1 then
             zcboMes.ItemIndex := ord(emEnero);
     end;

     rbSustituirAlgunos.Checked := TRUE;

     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := FALSE; //activa la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := FALSE; //Muestra la caja de agrupamiento

     ActiveControl := ArchivoSeek;
end;

function TWizNomFonacotImportarCedula_DevEx.GetArchivo: String;
begin
     Result := sArchivo.Text;
end;

procedure TWizNomFonacotImportarCedula_DevEx.cambiarSeleccion (elegir: Boolean);
var sTablaActual: String;
begin
     with dsEmpleadosNoEncontrados.DataSet do
     begin
          DisableControls;
          sTablaActual := FieldByName('NFonacotAntes').AsString;
          ZetaDBGridDBTableView.OptionsView.ScrollBars := ssNone;
          First;
          while not Eof do
          begin
               Edit;
               FieldByName ('Elegido').AsBoolean := elegir;
               Post;
               Next;
          end;
          EnableControls;
          First;
          ZetaDBGridDBTableView.OptionsView.ScrollBars := ssBoth;
          Locate('NFonacotAntes', sTablaActual, []);
     end;
end;

procedure TWizNomFonacotImportarCedula_DevEx.WizardAfterMove(Sender: TObject);
const K_ADVERTENCIA = 'La c�dula de pago del mes %s del a�o %s ya fue importada previamente';
      K_ADVERTENCIA_RAZON = 'La c�dula de pago del mes %s del a�o %s ya fue importada previamente' + CR_LF + ' para la raz�n social %s';
begin
     inherited;
     with Wizard do
     begin
          if EsPaginaActual( Opciones ) then
          begin
               PanelAnimation.Visible := FALSE;
               if RS_CODIGO.Llave = VACIO then
                  ZetaDialogo.zWarning( Caption, Format( K_ADVERTENCIA, [ ObtieneElemento(lfMeses, zcboMes.ItemIndex), IntToStr(txtAnio.Value) ]), 0, mbOK )
               else
                   ZetaDialogo.zWarning( Caption, Format( K_ADVERTENCIA_RAZON, [ ObtieneElemento(lfMeses, zcboMes.ItemIndex), IntToStr(txtAnio.Value), RS_CODIGO.Descripcion ]), 0, mbOK );
               ActiveControl := rbSustituirAlgunos;
          end
          else if EsPaginaActual( EmpleadosNoEncontrados ) then
          begin
                ZetaDialogo.ZInformation( Caption, 'Existe(n) ' +  IntToStr(dmProcesos.cdsEmpleadosCedula.RecordCount) + '  acreditado(s) de Fonacot sin empleado asignado, '
                                          + CR_LF + ' revise las sugerencias y ajuste seg�n corresponda.', 0 );
          end
          else
              PanelAnimation.Visible := FALSE;
     end;
end;

procedure TWizNomFonacotImportarCedula_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
const K_ADVERTENCIA = 'Ya fue importada previamente una c�dula para el mes %s del a�o %s para todas las razones sociales';
      K_ADVERTENCIA_CEDULA_OTRO_TIPO = 'Ya fue importada previamente una c�dula ' + CR_LF +
                                     'para el mes %s del a�o %s para todas las razones sociales con el formato %s';
      K_ADVERTENCIA_CEDULA_POR_RAZON = 'Ya fue importada previamente una c�dula ' + CR_LF +
                                     'para el mes de %s del a�o %s por raz�n social';
var lEnabled : Boolean;
begin
     inherited;
     with Wizard do
     begin
          if Adelante then
          begin
               if EsPaginaActual( Parametros ) then
               begin
                    if ZetaCommonTools.StrVacio( GetArchivo ) then
                       CanMove := Error( 'Debe especificarse un archivo a importar', Parametros )
                    else
                    begin
                         if not FileExists( GetArchivo ) then
                            CanMove := Error( 'El archivo ' + GetArchivo + ' no existe', Parametros )
                         else
                         begin
                              if dmProcesos.GetNominaAfectada( IntToStr (txtAnio.Value) , IntToStr(zcboMes.ItemIndex + 1), RS_CODIGO.Llave) = 3 then
                              begin
                                   CanMove := False;
                                   ZetaDialogo.ZInformation( Caption, 'No es posible realizar proceso ya que el pago Fonacot se encuentra ' + CR_LF + 'cerrado (estatus de Afectada Total).', 0 );
                              end
                              else
                              begin
                                   // Revisar si se seleccion� una raz�n social y ya existe c�dula cargada para todas las
                                   // razones sociales (RS_CODIGO = VACIO).
                                   if (RS_CODIGO.Llave <> VACIO) and (ExisteCedula (VACIO)) then
                                   begin
                                        CanMove := Error (Format(K_ADVERTENCIA, [ObtieneElemento(lfMeses, zcboMes.ItemIndex), IntToStr(txtAnio.Value)]), Parametros )
                                   end
                                   // Si se seleccion� raz�n social
                                   // revisar si ya existe una c�dula para esa raz�n social
                                   // y del mismo tipo de formato.
                                   else if (ExisteCedula (RS_CODIGO.Llave)) and ((iTipoArchivoACargar-1) <> iTipoCedula) then
                                   begin
                                        CanMove := Error (Format(K_ADVERTENCIA_CEDULA_OTRO_TIPO,
                                        [ObtieneElemento(lfMeses, zcboMes.ItemIndex), IntToStr(txtAnio.Value),
                                         ObtieneElemento ( lfTipoArchivoCedula, iTipoCedula)]), Parametros )
                                   end
                                   else if (RS_CODIGO.Llave = VACIO) and (ExisteCedulaPorRazon) then
                                   begin
                                        CanMove := Error (Format(K_ADVERTENCIA_CEDULA_POR_RAZON,
                                        [ObtieneElemento(lfMeses, zcboMes.ItemIndex), IntToStr(txtAnio.Value)]), Parametros )
                                   end
                                   else if iTipoArchivoACargar > 0 then
                                   begin
                                        if ( ArchivoVerif <> GetArchivo ) or
                                           ( ZetaDialogo.ZConfirm( Caption, 'El archivo ' + GetArchivo + ' ya fu� verificado.' + CR_LF +
                                                                   '�Volver a verificar?', 0, mbYes ) ) then
                                           CanMove := LeerArchivo
                                        else
                                           CanMove := TRUE;
                                   end
                                   else
                                   begin
                                        ZetaDialogo.ZWarning( self.Caption, K_ERROR_FORMATO, 0, mbOk);
                                        CanMove := False;
                                   end;
                              end;
                         end;
                    end;

                    if dmProcesos.GetConteoCedula( IntToStr (txtAnio.Value) , IntToStr(zcboMes.ItemIndex + 1), RS_CODIGO.Llave) > 0 then
                         lEnabled := True
                    else
                         lEnabled := False;

                    Opciones.PageVisible := lEnabled;
               end
          end
     end;
end;

procedure TWizNomFonacotImportarCedula_DevEx.CargaEmpleados;
var
   oCursor: TCursor;
begin
     Screen.Cursor := crHourglass;
     try
        dmProcesos.VerificarEmpleadosCedulaFonacot;
        dmProcesos.AplicarFiltroRFC(OcultarAsignados.Checked);
        dsEmpleadosNoEncontrados.DataSet := dmProcesos.cdsEmpleadosCedula;
        ZetaDBGridDBTableView.ApplyBestFit();
        NEmpleado.Width := NEmpleado.Width + 20;
     finally
            Screen.Cursor := oCursor;
     end;

     EmpleadosNoEncontrados.PageVisible := True;

     if dmProcesos.cdsEmpleadosCedula.RecordCount = 0 then
        EmpleadosNoEncontrados.PageVisible := False;
end;

procedure TWizNomFonacotImportarCedula_DevEx.ZetaDBGridDBTableViewFocusedItemChanged(Sender: TcxCustomGridTableView; APrevFocusedItem, AFocusedItem: TcxCustomGridTableItem);
begin
     inherited;
     if ( AFocusedItem <> nil ) then
     begin
          if ( AFocusedItem.Name <> 'NEmpleado' ) and ( btnBuscarEmp_DevEx.Visible ) then
            btnBuscarEmp_DevEx.Visible:= False;
     end;
end;

function TWizNomFonacotImportarCedula_DevEx.LeerArchivo: Boolean;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmProcesos do
        begin
             pAvance.Position := 45;
             PanelAnimation.Visible := TRUE;
             Application.ProcessMessages;
             pAvance.Position := 65;
             CargaParametros;
             ImportarCedulasFonacotGetListaASCII( ParameterList );
             pAvance.Position := 75;
             CargaEmpleados;
             pAvance.Position := 99;
             pAvance.Update;
             if ( ErrorCount > 0 ) then                 // Hubo Errores
             begin
                  ZAsciiTools.FiltraASCII( cdsDataSet, True );
                  Result := ZBaseGridShow_DevEx.GridShow( cdsDataset, TWizNomFonacotImportarCedulaGridShow_DevEx);
                  ZAsciiTools.FiltraASCII( cdsDataSet, False );
             end
             else
                  Result := True;
        end;
        if Result then
           ArchivoVerif := GetArchivo
        else
           ArchivoVerif := VACIO;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TWizNomFonacotImportarCedula_DevEx.AsignarEmpleado(sNombre, sEmpleado : String);
var sEmpleadoCursor : String;
begin
     with dmProcesos.cdsEmpleadosCedula do
     begin
          sEmpleadoCursor := FieldByName('NFonacotAntes').AsString;

          if FieldByName('NEmpleadoActual').AsString <> sEmpleado then
          begin
               ZetaDBGridDBTableView.OptionsView.ScrollBars := ssNone;
               if Not ( Locate( 'NEmpleadoActual', VarArrayOf( [ sEmpleado ]), [] )) then
                  ActualizarMismoFonacot(sEmpleadoCursor, sEmpleado, sNombre )
               else
                   ZetaDialogo.zWarning( Caption, 'El empleado ya tiene n�mero Fonacot asignado.', 0, mbOK );

               Locate( 'NFonacotAntes', VarArrayOf( [ sEmpleadoCursor ]), [] );
               ZetaDBGridDBTableView.OptionsView.ScrollBars := ssBoth;
          end;
     end;
end;

procedure TWizNomFonacotImportarCedula_DevEx.mModificarClick(Sender: TObject);
var
   sKey,sNombre:string;
begin
     inherited;
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo(VACIO,sKey,sNombre) then
     begin
          AsignarEmpleado(sKey + ' - ' + sNombre, sKey);
     end;
end;

procedure TWizNomFonacotImportarCedula_DevEx.QuitarEmpleadoClick(Sender: TObject);
begin
     with dmProcesos.cdsEmpleadosCedula do
     begin
          if FieldByName('NombreActual').AsString <> 'No encontrado' then
          begin
               Edit;
               FieldByName('NEmpleadoActual').AsString := VACIO;
               FieldByName('NombreActual').AsString := 'No encontrado';
               FieldByName('Elegido').AsBoolean := False;
               Post;
          end;
     end;
end;

procedure TWizNomFonacotImportarCedula_DevEx.EscogerEmpleado(Sender: TObject);
var iEmpleado : Integer;
begin
     if Sender is TMenuItemExtended then
     begin
          iEmpleado := TMenuItemExtended(Sender).Tag;
          AsignarEmpleado( TMenuItemExtended(Sender).Value, IntToStr (iEmpleado ) );
     end;
end;

procedure TWizNomFonacotImportarCedula_DevEx.ActualizarMismoFonacot(sFonacot, sNombre, sEmpleado : String);
begin
     try
        with dmProcesos.cdsEmpleadosCedula do
        begin
             Edit;
             FieldByName('NEmpleadoActual').AsString := sNombre;
             FieldByName('NombreActual').AsString := sEmpleado;
             FieldByName('Elegido').AsBoolean := True;
             Post;
             Next;
        end;
     except on E: Exception do
            begin
                 ZetaDialogo.ZError( Caption, 'Hubo un problema al asignar el empleado, verificar el registro en la c�dula.', 0 );
            end;
     end;

     ZetaDBGridDBTableView.ApplyBestFit();
     NEmpleado.Width := NEmpleado.Width + 20;
end;

procedure TWizNomFonacotImportarCedula_DevEx.NEmpleadoCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
     inherited;
     if AViewInfo.GridRecord.Focused and AViewInfo.Item.Focused then
     begin
           with btnBuscarEmp_DevEx do
           begin
                Left := AViewInfo.RealBounds.Left + AViewInfo.RealBounds.Width - Width;
                Top := AViewInfo.RealBounds.Top + ZetaDBGrid.Top;
                Visible := True;
           end;
     end;
end;

procedure TWizNomFonacotImportarCedula_DevEx.OcultarAsignadosClick(Sender: TObject);
var sTablaActual: String;
begin
     inherited;
     sTablaActual := dmProcesos.cdsEmpleadosCedula.FieldByName('NFonacotAntes').AsString;
     ZetaDBGridDBTableView.OptionsView.ScrollBars := ssNone;
     dmProcesos.AplicarFiltroRFC(OcultarAsignados.Checked);
     ZetaDBGridDBTableView.ApplyBestFit();
     NEmpleado.Width := NEmpleado.Width + 20;
     ZetaDBGridDBTableView.OptionsView.ScrollBars := ssBoth;
     dmProcesos.cdsEmpleadosCedula.Locate('NFonacotAntes', sTablaActual, []);
end;

procedure TWizNomFonacotImportarCedula_DevEx.btnBuscarEmp_DevExClick(Sender: TObject);
const K_EMPLEADO = '%s - %s';
      K_EMPLEADO_RFC = '%s - %s (%s)';
var
   mItem : TMenuItemExtended;
   i : integer;
   sEmpleados, sTempEmpleado : TStringList;

   procedure LimpiarItems;
   begin
        while PopupMenu1.items.count > 0 do
        begin
             PopupMenu1.items[0].free;
        end;
   end;

   procedure CrearDefaults;
   begin
        mItem := TMenuItemExtended.Create(PopupMenu1) ;
        mItem.Caption := 'Buscar empleado';
        mItem.ImageIndex := 1;
        mItem.OnClick := mModificarClick;
        PopupMenu1.Items.Add(mItem);

        mItem := TMenuItemExtended.Create(PopupMenu1) ;
        mItem.Caption := '-';
        PopupMenu1.Items.Add(mItem);

        mItem := TMenuItemExtended.Create(PopupMenu1) ;
        mItem.Caption := 'Cerrar';
        PopupMenu1.Items.Add(mItem);
   end;

   procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ;
   begin
        ListOfStrings.Clear;
        ListOfStrings.Delimiter     := Delimiter;
        ListOfStrings.StrictDelimiter := True;
        ListOfStrings.DelimitedText := Str;
   end;

   procedure MostrarMenu;
      var pnt: TPoint;
   begin
        if GetCursorPos(pnt) then
           PopupMenu1.Popup(pnt.X, pnt.Y);
   end;
 begin
      LimpiarItems;

      sEmpleados :=  TStringList.Create;

      mItem := TMenuItemExtended.Create(PopupMenu1) ;
      mItem.Caption := 'No asignar empleado';
      mItem.OnClick := QuitarEmpleadoClick;
      PopupMenu1.Items.Add(mItem);

      try
         Split('*', dmProcesos.cdsEmpleadosCedula.FieldByName('EmpleadosEncontrados').AsString, sEmpleados);
         for i := 0 to sEmpleados.Count - 1 do
         begin
              if StrLleno (sEmpleados[i]) then
              begin
                   sTempEmpleado := TStringList.Create;
                   try
                      Split('|', sEmpleados[i], sTempEmpleado);
                      mItem := TMenuItemExtended.Create(PopupMenu1) ;
                      mItem.Caption := Format ( K_EMPLEADO_RFC, [ sTempEmpleado[0], sTempEmpleado[1], sTempEmpleado[2] ] );
                      mItem.OnClick := EscogerEmpleado;
                      mItem.Tag := StrToInt ( sTempEmpleado[0]);
                      mItem.Value := Format ( K_EMPLEADO, [ sTempEmpleado[0], sTempEmpleado[1] ] );
                      PopupMenu1.Items.Add(mItem);
                   finally
                          FreeAndNil(sTempEmpleado);
                   end;
              end;
         end;

      finally
             FreeAndNil(sEmpleados);
      end;

      CrearDefaults;
      MostrarMenu;
end;

procedure TWizNomFonacotImportarCedula_DevEx.btnDesmarcarTodasClick(Sender: TObject);
begin
     inherited;
     cambiarSeleccion(FALSE);
end;

procedure TWizNomFonacotImportarCedula_DevEx.btnSeleccionarTodosClick(Sender: TObject);
begin
     inherited;
     cambiarSeleccion(TRUE);
end;

procedure TWizNomFonacotImportarCedula_DevEx.CargaParametros;
begin
     inherited;
     with ParameterList do
     begin
          AddInteger ('Year', txtAnio.Value);
          AddInteger ('Mes', zcboMes.ItemIndex + 1);
          AddString ('Archivo', GetArchivo);
          AddBoolean ('NoSustituir', rbNoSustituir.Checked);
          AddBoolean ('SustituirRegistrosSinDeducciones', rbSustituirAlgunos.Checked);
          AddBoolean ('SustituirTodos', rbSustituirTodos.Checked);
          AddString( 'RazonSocial', RS_CODIGO.Llave );
          AddVariant('EmpleadosEncontrados', dmProcesos.cdsEmpleadosCedula.Data );
          AddInteger ('TipoArchivo', iTipoArchivoACargar );
          AddBoolean ('EmpleadosBaja', chkPrestamosActivos.Checked);
     end;

     with Descripciones do
     begin
          AddInteger ('A�o', txtAnio.Value);
          AddInteger ('Mes', zcboMes.ItemIndex + 1);
          if ( RS_CODIGO.Visible ) then
            AddString( 'Raz�n social', RS_CODIGO.Llave );
          AddString( 'Archivo', GetArchivo );
          AddBoolean ('No sustituir', rbNoSustituir.Checked);
          AddBoolean ('Sustituir sin deducciones', rbSustituirAlgunos.Checked);
          AddBoolean ('Sustituir todos los registros', rbSustituirTodos.Checked);
          AddString ( 'Formato', ObtieneElemento ( lfTipoArchivoCedula, iTipoArchivoACargar - 1) );
     end;
end;

function TWizNomFonacotImportarCedula_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.ImportarCedulasFonacot ( ParameterList );
end;


procedure TWizNomFonacotImportarCedula_DevEx.HabilitaFiltroRazonSocial;
var
   lHabilitarFiltro : Boolean;
begin
     lHabilitarFiltro := False;
     with dmCatalogos.cdsRSocial do
     begin
          lHabilitarFiltro := (RecordCount > 1)
     end;

     RS_CODIGO.LookupDataset := dmCatalogos.cdsRSocial;
     RS_CODIGO.Enabled := lHabilitarFiltro;
     lbRazonSocial.Enabled := lHabilitarFiltro;

     if not lHabilitarFiltro then
     begin
          RS_CODIGO.Llave := VACIO;
     end;
end;

function TWizNomFonacotImportarCedula_DevEx.ExisteCedula (sRS_CODIGO: String): Boolean;
begin
     // Revisar si existe una c�dula para la raz�n social que se recibe de par�metro.
     iTipoCedula := dmNomina.FonacotTipoCedula(txtAnio.Value, zcboMes.Indice, sRS_CODIGO);
     Result := iTipoCedula >= ord (etacPorCredito);
end;

function TWizNomFonacotImportarCedula_DevEx.ExisteCedulaPorRazon: Boolean;
var i: Integer;
begin
     Result := FALSE;
     // Revisar si existe una c�dula para la raz�n social que se recibe de par�metro.

     if dmCatalogos.cdsRSocial.RecordCount > 0 then
     begin
          dmCatalogos.cdsRSocial.First;
          for i := 0 to dmCatalogos.cdsRSocial.RecordCount - 1 do
          begin
                if dmNomina.FonacotTipoCedula(txtAnio.Value, zcboMes.Indice, dmCatalogos.cdsRSocial.FieldByName('RS_CODIGO').AsString)
                   >= ord (etacPorCredito) then
                begin
                     Result := TRUE;
                     break;
                end;
                dmCatalogos.cdsRSocial.Next;
          end;
     end;


end;


end.
