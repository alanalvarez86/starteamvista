inherited WizSISTBorrarBajas_DevEx: TWizSISTBorrarBajas_DevEx
  Caption = 'Borrar Empleados Dados De Baja'
  ClientHeight = 403
  ClientWidth = 425
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 425
    Height = 403
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Este proceso ofrece una manera autom'#225'tica de eliminar los regist' +
        'ros de los empleados dados de baja.'
      Header.Title = 'Borrar bajas'
      object FechaLBL: TLabel
        Left = 60
        Top = 52
        Width = 118
        Height = 13
        Caption = '&Borrar Bajas anteriores a:'
        FocusControl = dFecha
        Transparent = True
      end
      object dFecha: TZetaFecha
        Left = 184
        Top = 48
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '02/nov/98'
        Valor = 36101.000000000000000000
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 168
        Width = 403
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 403
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 335
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 6
        Top = 136
      end
      inherited sFiltroLBL: TLabel
        Left = 31
        Top = 165
      end
      inherited Seleccionar: TcxButton
        Left = 134
        Top = 253
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 58
        Top = 133
      end
      inherited sFiltro: TcxMemo
        Left = 58
        Top = 164
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 58
        Top = 3
      end
      inherited bAjusteISR: TcxButton
        Left = 380
        Top = 166
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    Left = 200
    Top = 2
  end
end
