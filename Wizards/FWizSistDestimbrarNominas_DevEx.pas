unit FWizSistDestimbrarNominas_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, ExtCtrls, Mask,
     ZetaFecha, ZetaKeyCombo, ZetaNumero, ZcxBaseWizard,
     cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxContainer, cxEdit, ZetaCXWizard,
     dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
     dxWizardControl;

type
  TWizSISTDestimbrarNominas_DevEx = class(TcxBaseWizard)
    PeriodoGB: TcxGroupBox;
    YearLBL: TLabel;
    TipoNominaLBL: TLabel;
    InicialLBL: TLabel;
    FinalLBL: TLabel;
    Year: TZetaNumero;
    TipoNomina: TZetaKeyCombo;
    Inicial: TZetaNumero;
    Final: TZetaNumero;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
    //DevEx Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
  public
    { Public declarations }
  end;

var
  WizSISTDestimbrarNominas_DevEx: TWizSISTDestimbrarNominas_DevEx;

implementation

uses DCliente,
     DProcesos,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaCommonClasses,
     ZetaCommonLists, ZcxWizardBasico;

{$R *.DFM}

procedure TWizSISTDestimbrarNominas_DevEx.FormCreate(Sender: TObject);
begin
     ParametrosControl := Year;
     inherited;
     HelpContext := H_Borrar_Timbrado_nominas;
     with dmCliente.GetDatosPeriodoActivo do
     begin
          Self.Year.Valor := ( Year - 1 );
          Self.TipoNomina.Llave := IntToStr(  Ord( Tipo ) ); //acl
          Self.Inicial.Valor := 1;
          Self.Final.Valor := 999;
     end;

     //DevEx
     Parametros.PageIndex := 0;
     Ejecucion.PageIndex := 1;
end;

procedure TWizSISTDestimbrarNominas_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     ParametrosControl := nil;
     inherited;
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros ) then
          begin
               if ( Year.ValorEntero <= 0 ) then
               begin
                    zError( Caption, '� A�o no fu� especificado !', 0 );
                    ActiveControl := Year;
                    CanMove := False;
               end
               else
               if ( Inicial.ValorEntero <= 0 ) then
               begin
                    zError( Caption, '� N�mero de N�mina Inicial no fu� especificado !', 0 );
                    ActiveControl := Inicial;
                    CanMove := False;
               end
               else
               if ( Final.ValorEntero <= 0 ) then
               begin
                    zError( Caption, '� N�mero de N�mina Final no fu� especificado !', 0 );
                    ActiveControl := Final;
                    CanMove := False;
               end
               else
               if ( Inicial.ValorEntero > Final.ValorEntero ) then
               begin
                    zError( Caption, '� N�mero de N�mina Inicial es mayor' + CR_LF + 'que el n�mero de N�mina Final !', 0 );
                    ActiveControl := Inicial;
                    CanMove := False;
               end;
          end;
     end;
end;

procedure TWizSISTDestimbrarNominas_DevEx.CargaParametros;
begin
     with ParameterList do
     begin
          AddInteger( 'Year', Year.ValorEntero );
          AddInteger( 'Tipo', TipoNomina.LlaveEntero );//acl
          AddInteger( 'NumeroInicial', Inicial.ValorEntero );
          AddInteger( 'NumeroFinal', Final.ValorEntero );
          AddInteger( 'Clasifi', Ord(GetClasificacionPeriodo(FListaTiposPeriodoConfidencialidad,TipoNomina.LlaveEntero )));
     end;

     //DevEx
     with Descripciones do
     begin
          AddInteger( 'A�o', Year.ValorEntero );
          AddString( 'Tipo', ObtieneElemento( lfTipoPeriodo, TipoNomina.LlaveEntero ) );//acl     //DevEx
          AddInteger( 'N�mero Inicial', Inicial.ValorEntero );
          AddInteger( 'N�mero Final', Final.ValorEntero );
     end;
end;

function TWizSISTDestimbrarNominas_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.BorrarTimbradoNominas( ParameterList );
end;

procedure TWizSISTDestimbrarNominas_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with TipoNomina do    //acl
     begin
          ListaFija:=lfTipoPeriodoConfidencial; //acl
          LlaveEntero:= Ord( dmCliente.GetDatosPeriodoActivo.Tipo );
     end;

     Advertencia.Caption := 'Al aplicar el proceso se borrar� el status de Timbrado en los periodos y en todo registro de las n�minas indicadas.';
end;


procedure TWizSISTDestimbrarNominas_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := Year
     else
         ParametrosControl := nil;
     inherited;
end;


end.

