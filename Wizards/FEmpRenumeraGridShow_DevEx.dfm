inherited EmpRenumeraGridShow_DevEx: TEmpRenumeraGridShow_DevEx
  Left = 204
  Top = 186
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Lista de Errores Detectados'
  ClientHeight = 205
  ClientWidth = 572
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 169
    Width = 572
    inherited OK_DevEx: TcxButton
      Left = 408
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 487
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 572
    Height = 169
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object RENGLON: TcxGridDBColumn
        Caption = 'Rengl'#243'n'
        DataBinding.FieldName = 'RENGLON'
        Width = 45
      end
      object CB_CODIGO_OLD: TcxGridDBColumn
        Caption = 'C'#243'digo Actual'
        DataBinding.FieldName = 'CB_CODIGO_OLD'
        Width = 91
      end
      object CB_CODIGO_NEW: TcxGridDBColumn
        Caption = 'C'#243'digo Nuevo'
        DataBinding.FieldName = 'CB_CODIGO_NEW'
        Width = 75
      end
      object NUM_ERROR: TcxGridDBColumn
        Caption = 'Errores'
        DataBinding.FieldName = 'NUM_ERROR'
        Width = 50
      end
      object DESC_ERROR: TcxGridDBColumn
        Caption = 'Descripci'#243'n de Errores'
        DataBinding.FieldName = 'DESC_ERROR'
        Width = 148
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DataSource: TDataSource
    Left = 112
    Top = 64
  end
end
