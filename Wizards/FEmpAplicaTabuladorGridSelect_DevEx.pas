unit FEmpAplicaTabuladorGridSelect_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, Db,
     ZBaseSelectGrid_DevEx, cxGraphics, cxLookAndFeels,
     cxLookAndFeelPainters, Menus, dxSkinsCore, 
     TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
     dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
     cxEdit, cxNavigator, cxDBData, ImgList, cxGridLevel, cxClasses,
     cxGridCustomView, cxGridCustomTableView, cxGridTableView,
     cxGridDBTableView, cxGrid, ZetaCXGrid, cxButtons, cxTextEdit;

type
  TEmpAplicaTabuladorGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CB_SALARIO: TcxGridDBColumn;
    SAL_CLASIF: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpAplicaTabuladorGridSelect_DevEx: TEmpAplicaTabuladorGridSelect_DevEx;

implementation

uses ZBasicoSelectGrid_DevEx;

{$R *.DFM}

procedure TEmpAplicaTabuladorGridSelect_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with DataSet do
     begin
          MaskPesos( 'CB_SALARIO' );
          MaskPesos( 'SAL_CLASIF' );
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

end.
