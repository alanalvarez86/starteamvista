unit FWizSistBorrarHerramientas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaEdit, StdCtrls, ComCtrls, ExtCtrls, Mask, ZetaFecha,
  FWizSistBaseFiltro, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl;

type
  TWizSistBorrarHerramientas_DevEx = class(TWizSistBaseFiltro)
    GBFechas: TcxGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    FechaIni: TZetaFecha;
    FechaFin: TZetaFecha;
    GBFiltros: TcxGroupBox;
    CBTool: TCheckBox;
    TO_CODIGO: TZetaKeyLookup_DevEx;
    KT_TALLA: TZetaKeyLookup_DevEx;
    KT_MOT_FIN: TZetaKeyLookup_DevEx;
    CBTalla: TCheckBox;
    CBMotivo: TCheckBox;
    RGActivos: TcxRadioGroup;
    procedure CheckBoxClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    //DevEx Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);

  private
    { Private declarations }
    procedure SetControls;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizSistBorrarHerramientas_DevEx: TWizSistBorrarHerramientas_DevEx;

implementation

uses dCliente, dCatalogos, dTablas, dProcesos,
     ZetaCommonTools, ZetaCommonClasses, ZetaDialogo;

{$R *.DFM}

{ TWizSistBorrarHerramientas }

procedure TWizSistBorrarHerramientas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := FechaIni;
     with dmCliente do
     begin
          FechaIni.Valor := FechaDefault;
          FechaFin.Valor := FechaDefault;
     end;
     RGActivos.ItemIndex := 1;
     SetControls;
     HelpContext := H11616_Borrar_herramientas;
     TO_CODIGO.LookupDataset := dmCatalogos.cdsTools;
     KT_TALLA.LookupDataset := dmTablas.cdsTallas;
     KT_MOT_FIN.LookupDataset := dmTablas.cdsMotTool;

     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;
end;

procedure TWizSistBorrarHerramientas_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     dmCatalogos.cdsTools.Conectar;
     with dmTablas do
     begin
          cdsTallas.Conectar;
          cdsMotTool.Conectar;
     end;

     Advertencia.Caption :=
        'Al aplicar el proceso se eliminarán los registros de las herramientas indicadas en los parámetros.';
end;

procedure TWizSistBorrarHerramientas_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddBoolean( 'PrestamosActivos', ( RGActivos.ItemIndex = 0 ) );
          AddDate( 'FechaIni', FechaIni.Valor );
          AddDate( 'FechaFin', FechaFin.Valor );

          AddBoolean( 'FiltrarTool', CBTool.Checked );
          AddString( 'Tool', TO_CODIGO.Llave );
          AddBoolean( 'FiltrarTalla', CBTalla.Checked );
          AddString( 'Talla', KT_TALLA.Llave );
          AddBoolean( 'FiltrarMotivo', CBMotivo.Checked );
          AddString( 'Motivo', KT_MOT_FIN.Llave );
     end;
     //DevEx
     with Descripciones do
     begin
          AddBoolean( 'Préstamos Activos', ( RGActivos.ItemIndex = 0 ) );
          AddDate( 'Fecha inicial', FechaIni.Valor );
          AddDate( 'Fecha final', FechaFin.Valor );
          if ( CBTool.Checked ) then
             AddString( 'Filtrar por Herramienta', TO_CODIGO.Llave + ': ' + TO_CODIGO.Descripcion );
          if ( CBTalla.Checked ) then
             AddString( 'Filtrar por Talla', KT_TALLA.Llave  + ': ' + KT_TALLA.Descripcion );
          if ( CBMotivo.Checked and CBMotivo.Enabled ) then
             AddString( 'Filtrar por Motivo', KT_MOT_FIN.Llave  + ': ' + KT_MOT_FIN.Descripcion );
     end;
end;

procedure TWizSistBorrarHerramientas_DevEx.CheckBoxClick(Sender: TObject);
begin
     SetControls;
end;

procedure TWizSistBorrarHerramientas_DevEx.SetControls;
begin
     TO_CODIGO.Enabled := CBTool.Checked;
     KT_TALLA.Enabled := CBTalla.Checked;
     CBMotivo.Enabled := ( RGActivos.ItemIndex = 1 );
     KT_MOT_FIN.Enabled := CBMotivo.Checked;
end;

function TWizSistBorrarHerramientas_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.BorrarHerramientas( ParameterList );
end;

procedure TWizSistBorrarHerramientas_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
          var CanMove: Boolean);
begin
     ParametrosControl := nil;
     inherited;
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros ) then
          begin
               if ( CBTool.Checked ) and strVacio( TO_CODIGO.Llave ) then
               begin
                    ZetaDialogo.zError( Caption, 'ˇ Debe especificarse la Herramienta !', 0 );
                    ActiveControl := TO_CODIGO;
                    CanMove := False;
               end
               else if ( CBTalla.Checked ) and strVacio( KT_TALLA.Llave ) then
               begin
                    ZetaDialogo.zError( Caption, 'ˇ Debe especificarse la Talla !', 0 );
                    ActiveControl := KT_TALLA;
                    CanMove := False;
               end
               else if ( CBMotivo.Checked ) and strVacio( KT_MOT_FIN.Llave ) then
               begin
                    ZetaDialogo.zError( Caption, 'ˇ Debe especificarse el Motivo de Devolución !', 0 );
                    ActiveControl := KT_TALLA;
                    CanMove := False;
               end;
          end;
     end;
end;

procedure TWizSistBorrarHerramientas_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
          ParametrosControl := FechaIni
     else
         ParametrosControl := nil;
     inherited;
end;


end.



