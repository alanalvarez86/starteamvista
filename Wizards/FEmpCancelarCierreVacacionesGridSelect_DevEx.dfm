inherited EmpCancelarCierreVacacionesGridSelect_DevEx: TEmpCancelarCierreVacacionesGridSelect_DevEx
  Left = 181
  Top = 376
  Width = 683
  Height = 312
  Caption = 'Seleccione los cierres de vacaciones a cancelar'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 238
    Width = 667
    inherited OK_DevEx: TcxButton
      Left = 499
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 577
    end
  end
  inherited PanelSuperior: TPanel
    Width = 667
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 667
    Height = 203
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
      end
      object VA_FEC_INI: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'VA_FEC_INI'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
      end
      object VA_D_GOZO: TcxGridDBColumn
        Caption = 'Derecho Gozo'
        DataBinding.FieldName = 'VA_D_GOZO'
      end
      object VA_D_PAGO: TcxGridDBColumn
        Caption = 'Derecho Pago'
        DataBinding.FieldName = 'VA_D_PAGO'
      end
      object VA_D_PRIMA: TcxGridDBColumn
        Caption = 'Derecho prima vac.'
        DataBinding.FieldName = 'VA_D_PRIMA'
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
