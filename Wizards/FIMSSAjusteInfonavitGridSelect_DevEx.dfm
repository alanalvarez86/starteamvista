inherited IMSSAjusteInfonavitGridSelect_DevEx: TIMSSAjusteInfonavitGridSelect_DevEx
  Left = 177
  Top = 349
  Width = 862
  Height = 232
  Caption = 'Seleccione los cr'#233'ditos deseados'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 158
    Width = 846
    inherited OK_DevEx: TcxButton
      Left = 682
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 761
      Cancel = True
    end
  end
  inherited PanelSuperior: TPanel
    Width = 846
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 846
    Height = 123
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.Indicator = True
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
      end
      object K_PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre Completo'
        DataBinding.FieldName = 'PRETTYNAME'
        Width = 182
      end
      object CB_INFCRED: TcxGridDBColumn
        Caption = '# Cr'#233'dito'
        DataBinding.FieldName = 'CB_INFCRED'
        Width = 90
      end
      object K_AMORTIZA: TcxGridDBColumn
        Caption = 'Amortizaci'#243'n'
        DataBinding.FieldName = 'K_AMORTIZA'
      end
      object K_ACUMULA: TcxGridDBColumn
        Caption = 'Retenci'#243'n'
        DataBinding.FieldName = 'K_ACUMULA'
      end
      object K_OTROS_AJUSTES: TcxGridDBColumn
        Caption = 'Otros Ajustes'
        DataBinding.FieldName = 'K_OTROS_AJUSTES'
        Width = 73
      end
      object K_MONTO_AJUSTE: TcxGridDBColumn
        Caption = 'Monto de Ajuste'
        DataBinding.FieldName = 'K_MONTO_AJUSTE'
        Width = 89
      end
      object K_SALDO: TcxGridDBColumn
        Caption = 'Saldo'
        DataBinding.FieldName = 'K_SALDO'
      end
      object LE_DIAS_BM: TcxGridDBColumn
        Caption = 'D'#237'as Cotizados'
        DataBinding.FieldName = 'LE_DIAS_BM'
        Width = 100
      end
      object LE_PROV: TcxGridDBColumn
        Caption = 'Prov. Vaca.'
        DataBinding.FieldName = 'LE_PROV'
        Width = 100
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 6815928
  end
end
