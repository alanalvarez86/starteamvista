unit FWizAgregarBaseDatosPresupuestos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FWizBasicoAgregarBaseDatos_DevEx, Mask, ZetaFecha, StdCtrls, ComCtrls, Buttons,
  ZetaWizard, ExtCtrls,  
  ZetaDialogo, ZetaEdit, FileCtrl, ZetaNumero,
  ZetaKeyCombo, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit,
  ZetaCXWizard, cxTextEdit, cxMemo, dxGDIPlusClasses, cxImage, cxLabel,
  cxGroupBox, cxRadioGroup, dxCustomWizardControl, dxWizardControl,
  cxCheckBox;

type
  TSQLConnection = record
    ServerName : widestring;
    DatabaseName : wideString;
    UserName : widestring;
    Password  : widestring;
    end;

  TWizAgregarBaseDatosPresupuestos_DevEx = class(TWizBasicoAgregarBaseDatos_DevEx)
    FechaAlta: TdxWizardControlPage;
    FechaBaja: TdxWizardControlPage;
    NominaMensual: TdxWizardControlPage;
    ConfNomina: TdxWizardControlPage;
    BaseAnio: TdxWizardControlPage;
    txtAnio: TZetaNumero;
    cbBasesDatosBase: TZetaKeyCombo;
    Label3: TLabel;
    lblAnyo: TLabel;
    fecAlta: TZetaFecha;
    lblFechaAlta: TLabel;
    lblFechaBaja: TLabel;
    fecBaja: TZetaFecha;
    chbEspeciales: TcxCheckBox;
    OpenDialog: TOpenDialog;
    chbNominaMensual: TcxCheckBox;
    chbConfNomina: TcxCheckBox;
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    //procedure rbBDExistenteClick(Sender: TObject);
    procedure rbBDNuevaClick(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
    procedure rbOtraBaseDatosClick(Sender: TObject);
    procedure rbValoresDefaultClick(Sender: TObject);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure rbSameUserClick(Sender: TObject);
    procedure rbOtroUserClick(Sender: TObject);
    procedure txtCodigoExit(Sender: TObject);
  private
    { Private declarations }
    ParamTamanioDB,ParamLog: String;
    SC : TSQLConnection;
    function GetConnStr: widestring;
    property ConnStr : widestring read GetConnStr;
    function ExisteBD: boolean;
  public
    { Public declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  end;

var
  WizAgregarBaseDatosPresupuestos_DevEx: TWizAgregarBaseDatosPresupuestos_DevEx;



implementation

uses
    DProcesos,
    DSistema,
    DCatalogos,
    DBaseSistema,
    ZetaCommonClasses,
    ZetaCommonLists,
    ZetaClientTools,
    ZetaServerTools,
    DCliente,
    ZetaCommonTools;

{$R *.dfm}

procedure TWizAgregarBaseDatosPresupuestos_DevEx.CargaParametros;
Const
     K_GB='GB';
     K_MB='MB';
     K_ESPACIO=' ';
     K_1024 = 1024;
var
   tamanioBD,log:String;
begin
     inherited;
     ParamInicial:=0;
     Descripciones.Clear;
     with Descripciones do
     begin
          if BaseAnio.PageVisible then
          begin
               AddString('Base de datos', txtBaseDatos.Text );
               AddString('Ubicaci�n' , dmSistema.GetServer + '.' + txtBaseDatos.Text);
               //AddString('Datos iniciales' + cbBasesDatosBase.Text );  //Se elimino porque son muchos parametros
               {$ifndef PRESUPUESTOS}
				{***DevEx(by am): Validacion para agregar el parametros de Tamano***}
               if (cbBasesDatosBase.Items.Count>0) and (cbBasesDatosBase.ItemIndex<>-1) then
               begin

                    {***DevEx(by am): Formato de los datos***}
                    if StrToFloat (ParamTamanioDB) > K_1024 then
                         tamanioBD := FloatToStr(StrToFloat (ParamTamanioDB)/K_1024)+K_ESPACIO+K_GB
                    else
                        tamanioBD := ParamTamanioDB+K_ESPACIO+K_MB;
                    if StrToFloat (ParamLog) > K_1024 then
                       log := FloatToStr (StrToFloat (ParamLog)/K_1024)+K_ESPACIO+K_GB
                    else
                        log := log+K_ESPACIO+K_MB;
                    AddString('Tama�o',Format ('BD origen %s, log %s',[tamanioBD, log]));
               end;
 				{$endif}
               //DevEx(by am): Se muestra el texto para que las fechas se muestren con la mascara.
               AddString('Contrataciones anteriores a' , fecAlta.Text);
               AddString('Baja Empleados desde' , fecBaja.Text );
               AddBoolean('Cambiar a n�mina mensual', chbNominaMensual.Checked);
               {$ifdef PRESUPUESTOS}
               AddBoolean('Conservar config. de n�mina', chbConfNomina.Checked);
               AddString('Datos iniciales', cbBasesDatosBase.Items[ cbBasesDatosBase.ItemIndex ] );
               AddString('A�o a presupuestar', txtAnio.Text);
               {$endif}
          end
          else
          begin
               AddString( 'Base de datos existente', cbBasesDatos.Text );
               AddString('Ubicaci�n' , dmSistema.GetServer + '.' + cbBasesDatos.Text);
          end;
     end;
     with ParameterList do
     begin
          AddInteger('Tipo', Ord (tcPresupuesto));
          AddString( 'Codigo', txtCodigo.Text );
          AddString( 'Descripcion', txtDescripcion.Text );
          AddBoolean('DefinirEstructura', BaseAnio.Enabled);
          AddBoolean('NuevaBD', rbBDNueva.Checked); 
          AddBoolean('Especiales', chbEspeciales.Checked);

          if rbBDNueva.Checked then
          begin     
               AddString( 'BaseDatosBase', cbBasesDatosBase.Llaves[cbBasesDatosBase.ItemIndex]);
               AddString( 'NombreBDSQL', Trim(txtBaseDatos.Text));
               AddString( 'BaseDatos', Trim(txtBaseDatos.Text) );

               // Par�metros para proceso de preparaci�n de Presupuestos
               AddInteger('anioPresupuestar', txtAnio.ValorEntero);
               AddDate('FechaAlta', fecAlta.Valor);
               AddDate('FechaBaja', fecBaja.Valor);
                                  
               AddBoolean('ConservarConfNomina', chbConfNomina.Checked);
               {$ifdef PRESUPUESTOS}
               AddString( 'Usuario', VACIO );
               AddString( 'Clave', VACIO);
               {$else}
               AddString( 'Usuario', Trim(txtUsuario.Text) );
               AddString( 'Clave', ZetaServerTools.Encrypt(txtClave.Text));
               AddInteger ('MaxSize', Round (StrToFloat(ParamTamanioDB)));
               {$endif}
               AddBoolean('NominaMensual', chbNominaMensual.Checked);
               // ----- -----
          end
          else
          begin
              AddString( 'BaseDatos', cbBasesDatos.Text ); 
              AddString( 'Usuario', VACIO );
              AddString( 'Clave', VACIO );
          end;
     end;
end;

function TWizAgregarBaseDatosPresupuestos_DevEx.EjecutarWizard: Boolean;
Const
     K_TERMINADO='  Terminado';
     K_ERROR='  Error';
var
resultado : Boolean;

procedure SetStatus (lTerminado: Boolean);
begin
     if lTerminado then
          cxMemoStatus.Lines.Append( K_TERMINADO)
     else
          cxMemoStatus.Lines.Append( K_ERROR);
end;

begin
     {***DevEx(by am):Mostar Grupo de Avance y Limpiar memos de avance***}
     GrupoAvance.Visible := TRUE;
     cxMemoPasos.Lines.Clear;
     cxMemoStatus.Lines.Clear;
     if (BaseAnio.PageVisible) then
     begin
         try
              {$ifndef PRESUPUESTOS}
              {***DevEx(by am): Paso1***}
              cxMemoPasos.Lines.Append('Creando base de datos... ');
              resultado := dmSistema.CrearBDEmpleados(ParameterList);
              SetStatus(resultado); //Paso1
              {***DevEx(by am): Paso2***}
              if resultado then
              begin
                   cxMemoPasos.Lines.Append('Definiendo estructura... ');
                   resultado := dmSistema.CrearEstructuraBD(ParameterList);
                   SetStatus(resultado); //Paso2
              end;
              {***DevEx(by am): Paso3***}
              if (resultado) and (chbEspeciales.Checked) then
              begin
                  cxMemoPasos.Lines.Append('Scripts especiales... ');
                  resultado := dmSistema.CreaEspeciales(ParameterList);
                  SetStatus(resultado); //Paso3
              end;
              {$else}
              resultado := TRUE;
              {$endif}
              {***DevEx(by am): Paso1 o Paso4***}
              if resultado then
              begin
                  cxMemoPasos.Lines.Append('Preparar presupuestos... ');
                  resultado := dmSistema.PrepararPresupuestos(ParameterList);
                  SetStatus(resultado); //Paso1 o Paso4
              end;
              {***DevEx(by am): Paso2 o Paso5***}
              if resultado then
              begin
                  cxMemoPasos.Lines.Append('Cat�logos de presupuestos... ');
                  resultado := dmSistema.CatalogosPresupuestos(ParameterList);
                  SetStatus(resultado); //Paso2 o Paso5
              end;
              {***DevEx(by am): Paso3 o Paso6***}
              if resultado then
              begin
                  cxMemoPasos.Lines.Append('Transferencia de empleados... ');
                  resultado := dmSistema.EmpleadosPresupuestos(ParameterList);
                  SetStatus(resultado); //Paso3 o Paso6
              end;
              {***DevEx(by am): Paso4 o Paso8***}
              if resultado then
              begin
                  cxMemoPasos.Lines.Append('Optimizar base de datos... ');
                  resultado := dmSistema.OptimizarPresupuestos(ParameterList);
                  SetStatus(resultado); //Paso4 o Paso8
              end;
              {***DevEx(by am): Paso5 o Paso10***}
              if resultado then
              begin
                  cxMemoPasos.Lines.Append('Compactando base de datos... ');
                  resultado := dmSistema.ShrinkDB(ConnStr,txtBaseDatos.Text);
                  SetStatus(resultado); //Paso5 o Paso10
              end;
              {***DevEx(by am): Paso11***}
              {$ifndef PRESUPUESTOS}
              if resultado then
              begin
                  cxMemoPasos.Lines.Append('Registrando base de datos en sistema... ');
                  resultado := dmSistema.GrabaDB_INFO(ParameterList.VarValues);
                  SetStatus(resultado); //Paso11
              end;
              {$endif}

              if resultado then
                   ZInformation(Caption, 'La base de datos fue agregada correctamente.', 0)
              else
                   ZError(Caption, 'Error al agregar base de datos.', 0);
              Result := TRUE;
         except
               on Error: Exception do
               begin
                   if (  Pos( 'PRIMARY KEY', UpperCase( Error.Message )) > 0) then
                      ZError (Caption, Format('Error al agregar base de datos:' + CR_LF + 'El c�digo de base de datos: %s ya se encuentra dado de alta',[ txtCodigo.Text]), 0)
                   else if (  Pos( 'ALREADY EXISTS', UpperCase( Error.Message )) > 0) then
                      ZError (Caption, Format('Error al agregar base de datos:' + CR_LF + 'Base de datos: %s ya existe. Proporcione otro nombre.',[ Trim (txtBaseDatos.Text)]), 0)
                   else
                       ZError(Caption, 'Error al agregar base de datos:' + CR_LF + Error.Message, 0);
                   Result := TRUE;
               end;

         end;
     end
     else
     begin
         try
            {***DevEx(by am): Paso1***}
             cxMemoPasos.Lines.Append('Agregando base de datos al sistema... ');
             Result := dmSistema.GrabaDB_INFO(ParameterList.VarValues);
             setStatus(Result); //Paso1
             if Result then
                ZInformation(Caption, 'La base de datos fue agregada correctamente.', 0)
             else
             begin
                 ZError(Caption, 'Error al agregar base de datos.', 0);
                 Result := TRUE;
             end;
         except
               on Error: Exception do
               begin
                   if (  Pos( 'PRIMARY KEY', UpperCase( Error.Message )) > 0) then
                   begin
                      ZError (Caption, Format('Error al agregar base de datos:' + CR_LF + 'El c�digo de base de datos: %s ya se encuentra dado de alta',[ txtCodigo.Text]), 0);
                      {***DevEx(by am): El componente wizard de DevExpress no se puede quedar abierto despues de que termine el proceso del boton aplicar. Por lo tanto se cerrara el wizard si existio algun error***}
                      {Wizard.Anterior;
                      txtCodigo.SetFocus;
                      Result := FALSE }
                   end
                   else
                       ZError(Caption, 'Error al agregar base de datos:' + CR_LF + Error.Message, 0);
                   Result := TRUE;
               end;
         end
     end;
end;

procedure TWizAgregarBaseDatosPresupuestos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_SIST_PROC_AGREGAR_BASE_DATOS;
     with dmSistema do
     begin
          cdsEmpresasLookUp.Conectar;
     end;
     with dmCatalogos do
     begin
          cdsTPeriodos.Conectar;
     end;
     {***DevEx(by am): Definicion de la secuencia de las paginas***}
          Parametros.PageIndex := 0;
          BaseAnio.PageIndex := 1;
          FechaAlta.PageIndex := 2;
          FechaBaja.PageIndex := 3;
          NominaMensual.PageIndex := 4;
          ConfNomina.PageIndex := 5;
          NuevaBD2.PageIndex := 6;
          Estructura.PageIndex := 7;
          Ejecucion.PageIndex := 8;
     {***}
     //PageControl.ActivePage := Parametros;  {***CODIGO NO NECESARIO***}
end;

procedure TWizAgregarBaseDatosPresupuestos_DevEx.FormShow(Sender: TObject);
{$ifdef PRESUPUESTOS}
var sServer, sDataBase: String;
{$endif}
begin
     inherited;
     dmSistema.GetBasesDatos(dmSistema.GetServer, VACIO, VACIO);
     txtBaseDatos.SetFocus;
     if dmCliente.YearDefault > 0 then
        txtAnio.Valor := dmCliente.YearDefault + 1;

     if (cbBasesDatos.Items.Count = 0) then
     begin
         while not dmSistema.cdsBasesDatos.Eof do
         begin
              cbBasesDatos.AddItem(dmSistema.cdsBasesDatos.FieldByName ('NAME').AsString, nil);
              dmSistema.cdsBasesDatos.Next;
         end;
     end;

     ConfNomina.PageVisible := FALSE;
     chbConfNomina.Checked := FALSE;
     {$ifdef PRESUPUESTOS}
     Self.Caption := 'Preparar base de datos de tipo Presupuestos';
     HelpContext := H_SIST_PROC_AGREGAR_BASE_DATOS_PRES;
     // P�ginas del Wizard que no se usan
     Parametros.PageVisible := FALSE;
     NuevaBD2.PageVisible := FALSE;
     Estructura.PageVisible := FALSE;
     // =================================
     ConfNomina.PageVisible := TRUE;
     chbConfNomina.Checked := TRUE;
     //PageControl.ActivePage := BaseAnio; {***CODIGO NO NECESARIO***} //Con poner Parametros invisible la primera pag. sera BaseAnio pues tiene el sig. Index.
     chbEspeciales.Visible := FALSE;
     chbEspeciales.Checked := FALSE;

     dmSistema.cdsEmpresas.Conectar;
     dmSistema.cdsEmpresas.Locate('CM_CODIGO', dmCliente.Compania, []);
     GetServerDatabase(dmSistema.cdsEmpresas.FieldByName ('CM_DATOS').AsString, sServer, sDataBase);

     txtBaseDatos.Text := sDataBase;

     {***CODIGO NO NECESARIO***}
     //memBaseAnio.Lines.Clear; {***CODIGO NO NECESARIO***}
     //memBaseAnio.Lines.Add(format('Indique la base de datos existente de tipo Tress que se usar� para preparar la base de datos %s', [sDataBase] ));

     
     if (cbBasesDatosBase.Items.Count = 0) then
     begin
         try
             cbBasesDatosBase.Lista.BeginUpdate;
             // Obtener Bases de Datos de Tipo Tress.
             with dmSistema.cdsSistBaseDatos do
             begin
                 Conectar;
                 First;
                 while not Eof do
                 begin
                      if (FieldByName ('DB_TIPO').AsInteger = Ord( tc3Datos )) or (FieldByName ('DB_TIPO').AsInteger = Ord( tc3Prueba )) then
                         cbBasesDatosBase.Lista.Add(FieldByName ('DB_CODIGO').AsString + '=' + FieldByName ('DB_DESCRIP').AsString);
                      Next;
                 end;
             end;
         finally
                cbBasesDatosBase.Lista.EndUpdate;
         end;
     end;
     cbBasesDatosBase.SetFocus;
     {$else}     
     chbEspeciales.Enabled := dmSistema.ExistenEspeciales;
     {$endif}

     Advertencia.Caption := 'Se proceder� a preparar la base de datos de tipo Presupuestos. Presione Aplicar para iniciar el proceso.';
end;

//Se paso a la clase padre
{procedure TWizAgregarBaseDatosPresupuestos_DevEx.rbBDExistenteClick(
  Sender: TObject);
begin
     txtBaseDatos.Enabled := false;
     cbBasesDatos.Enabled := true;
     cbBasesDatos.SetFocus;
end;}

procedure TWizAgregarBaseDatosPresupuestos_DevEx.rbBDNuevaClick(Sender: TObject);
begin
     txtBaseDatos.Enabled := true;
     txtBaseDatos.SetFocus;
     cbBasesDatos.Enabled := false;
end;

procedure TWizAgregarBaseDatosPresupuestos_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
const K_ESPACIO = ' ';  
var mensaje : WideString;
begin
     ParametrosControl:= nil;
     if Wizard.Adelante then
     begin
          if Wizard.EsPaginaActual( Parametros ) then
          begin
              // Habilitaci�n de pasos.
              BaseAnio.PageVisible := rbBDNueva.Checked;
              FechaAlta.PageVisible := rbBDNueva.Checked;
              FechaBaja.PageVisible := rbBDNueva.Checked;
              // ConfNomina.Enabled := rbBDNueva.Checked;
              NominaMensual.PageVisible := rbBDNueva.Checked;
              NuevaBD2.PageVisible := rbBDNueva.Checked;
              if rbBDNueva.Checked then
              begin
                   if Trim (txtBaseDatos.Text) = VACIO then
                      CanMove := Warning ('El campo nombre de base de datos no puede quedar vac�o.', txtBaseDatos)
                   else if ExisteBD then
                      CanMove := Warning (Format('La base de datos: %s ya existe. Proporcione otro nombre.',[ Trim (txtBaseDatos.Text)]), txtBaseDatos)
                   else if Pos (K_ESPACIO, txtBaseDatos.Text) > 0 then
                        CanMove := Warning ('El nombre de la base de datos no puede contener espacios', txtBaseDatos)
                   else begin
                        CanMove := TRUE;
                   end;
              end
              else if rbBDExistente.Checked then
              begin
                   if (cbBasesDatos.ItemIndex >= 0) and (txtAnio.ValorEntero > 0)  then
                   begin
                        // Es Base deDatos ya agregada a DB_INFO?
                        if (dmSistema.EsBaseDatosAgregada(dmSistema.GetServer + '.' + cbBasesDatos.Text)) then
                        begin
                             CanMove := Warning (Format ('La base de datos ''%s'' ya se encuentra agregada al sistema.', [cbBasesDatos.Text]), cbBasesDatos)
                        end
                        else
                        begin
                             if (dmSistema.EsBaseDatosEmpleados(cbBasesDatos.Text)) then
                             begin
                                 CanMove := TRUE;
                             end
                             else 
                                  CanMove := Warning ('No se pudo configurar la base de datos para ser utilizada para '
                                          + CR_LF + 'presupuestos de n�mina ya que la estructura no es de tipo Presupuestos.'
                                          + CR_LF + CR_LF + 'Seleccione otra base de datos.', cbBasesDatos);
                        end;
                   end
                   else if (cbBasesDatos.ItemIndex < 0) then
                       CanMove := Warning ('Seleccionar una base de datos para continuar.', cbBasesDatos)
                   else    
                       CanMove := Warning ('Proporcione a�o a presupuestar para continuar.', cbBasesDatos)
              end;
          end
          else if Wizard.EsPaginaActual( Estructura ) then
          begin
               if (trim (txtCodigo.Text) = VACIO) then
               begin
                    CanMove := Warning ('El campo C�digo no puede quedar vac�o.', txtCodigo);
               end         
               else if Pos (K_ESPACIO, txtCodigo.Text) > 0 then
               begin
                    CanMove := Warning ('El campo c�digo no puede contener espacios', txtCodigo);
               end 
               else if (dmSistema.ExisteBD_DBINFO(trim(txtCodigo.Text))) then
               begin
                    CanMove := Warning (Format ('El c�digo  ''%s'' ya se encuentra agregado al sistema.', [txtCodigo.Text]), txtCodigo)
               end
               else if (trim (txtDescripcion.Text) = VACIO) then
               begin
                    CanMove := Warning ('El campo Descripci�n no puede quedar vac�o.', txtDescripcion);
               end
               else
                   CanMove := TRUE;
          end
          else if Wizard.EsPaginaActual( BaseAnio ) then
          begin
               if (cbBasesDatosBase.ItemIndex < 0) then
               begin
                    CanMove := Warning ('Seleccionar una base de datos para continuar.', cbBasesDatosBase);
               end
               else
               begin
                   CanMove := TRUE;
               end;
          end
          else if Wizard.EsPaginaActual( NuevaBD2 ) then
          begin
               if rbOtroUser.Checked then
               begin
                    if not ProbarConexion(mensaje) then
                    begin
                         CanMove := Warning (mensaje, txtUsuario);
                    end
                end;
           end
          else if Wizard.EsPaginaActual( NominaMensual ) then
          begin
               if chbNominaMensual.Checked then
               begin
                    if not dmCatalogos.UsaClasifiPeriodo( tpMensual ) then
                       CanMove := Warning ( 'No se tiene configurado un tipo de n�mina mensual' + CR_LF +
                                            'Para cambiar a los empleados a n�mina mensual requiere' + CR_LF +
                                            'agregar ese tipo de periodo.', chbNominaMensual);
                end;
           end;
     end;
     inherited;
end;

procedure TWizAgregarBaseDatosPresupuestos_DevEx.WizardAfterMove(Sender: TObject);
var sServer, sDataBase: String;
begin
     inherited;
     if Wizard.Adelante then
     begin
        if Wizard.EsPaginaActual( BaseAnio )then
        begin
             {***CODIGO NO NECESARIO***}
            {memBaseAnio.Lines.Clear;
            memBaseAnio.Lines.Add(format('Indique la base de datos existente de tipo Tress que se usar� para crear la nueva base de datos %s', [txtBaseDatos.Text] ));}
            {$ifndef PRESUPUESTOS}
            if (cbBasesDatosBase.Items.Count = 0) then
            begin
                try
                    cbBasesDatosBase.Lista.BeginUpdate;
                    // Obtener Bases de Datos de Tipo Tress.
                    with dmSistema.cdsSistBaseDatos do
                    begin
                        Conectar;
                        First;
                        while not Eof do
                        begin
                             if (FieldByName ('DB_TIPO').AsInteger = Ord( tc3Datos )) or (FieldByName ('DB_TIPO').AsInteger = Ord( tc3Prueba )) then
                                cbBasesDatosBase.Lista.Add(FieldByName ('DB_CODIGO').AsString + '=' + FieldByName ('DB_DESCRIP').AsString);
                             Next;
                        end;
                    end;
                finally
                       cbBasesDatosBase.Lista.EndUpdate;
                end;
            end;
            ParametrosControl:= cbBasesDatosBase;
            {$endif}
        end
       { else if Wizard.EsPaginaActual( Ejecucion ) then
        begin
             Advertencia.Lines.Clear; //CAMBIAR TODO LO DE ADVERTENCIA A  LOS MEMOS DEL GRUPO DE AVANCE Y POSIBLEMENTE MOVERLO AL EJECUTAR PARA QUE SE HAGA EL EJECTO DE QUE VA AVANZANDO
             Advertencia.Alignment := taLeftJustify; //CAMBIAR

             if (BaseAnio.PageVisible) then
             begin

             Se proceder� a crear la Base de datos de Presupuestos de N�mina:
             \\BDSERVER\DATOSPRESUPUESTO
             - Tama�o de BD origen 512MB, Log = 128MB
             - Incluir contrataciones anteriores a: 01/Ene/2014
             - Incluir empleados dados de baja desde: 01/Ene/2013
             - Cambiar empleados a N�mina mensual: S
             - Conservar configuraci�n de N�mina: S

                 Advertencia.Lines.Add ('Se proceder� a preparar la base de datos de tipo Presupuestos: ');
                 //***CODIGO NO NECESARIO***//El else nunca se ejecuta
                 //if BaseAnio.Enabled then
                    //Advertencia.Lines.Add ('Ubicaci�n: ' + dmSistema.GetServer + '.' + txtBaseDatos.Text)
                // else
                     //Advertencia.Lines.Add ('Ubicaci�n: ' + dmSistema.GetServer + '.' + cbBasesDatos.Text);
                 Advertencia.Lines.Add ('Ubicaci�n: ' + dmSistema.GetServer + '.' + txtBaseDatos.Text); //CAMBIAR A PARAMETROS

                 Advertencia.Lines.Add ('');
                 Advertencia.Lines.Add('Datos iniciales: ' + cbBasesDatosBase.Text );
                 //Advertencia.Lines.Add(Format ('Tama�o de BD origen %s, log %s',[txtTamanyo.Valor + lblMB1.Caption, txtLog.Valor + lblMB2.Caption]) );  //CAMBIAR DE DONE SE OBTIENE EL DATO
                 Advertencia.Lines.Add('Incluir contrataciones anteriores a: ' + fecAlta.ValorAsText);
                 Advertencia.Lines.Add('Incluir empleados dados de baja desde: ' + fecBaja.ValorAsText );
                 if chbNominaMensual.Checked then
                    Advertencia.Lines.Add('Cambiar empleados a n�mina mensual: S�')
                 else
                     Advertencia.Lines.Add('Cambiar empleados a n�mina mensual: No');
                 //$ifdef PRESUPUESTOS
                 if chbConfNomina.Checked then
                    Advertencia.Lines.Add('Conservar configuraci�n de n�mina: S�')
                 else
                     Advertencia.Lines.Add('Conservar configuraci�n de n�mina: No');
                 //$endif
             end
             else
             begin
                Advertencia.Lines.Add(format('Se agregar� la base de datos ''%s'' al sistema.', [cbBasesDatos.Text]));
                Advertencia.Lines.Add ('La base de datos cuenta con la estructura para Presupuestos.');
             end;

             // Advertencia.Lines.Add(' ');
             Advertencia.Lines.Add('Presione el bot�n Ejecutar para iniciar el proceso.');
             lblProceso.Caption := '';
        end}
        else if  Wizard.EsPaginaActual( Estructura ) then
             ParametrosControl:= txtCodigo
        else if Wizard.EsPaginaActual( NuevaBD2 ) then
        begin
             {$ifndef PRESUPUESTOS}
             {***CODIGO NO NECESARIO***}
             {lblMB1.Caption := 'MB';
             lblMB2.Caption := 'MB';}

             {***DevEx(by am): Es necesario hacer estos calculos para que se agregue a ParameterList el MaxSize***}
             GetServerDatabase (dmSistema.GetBDFromDB_INFO(cbBasesDatosBase.Llaves[cbBasesDatosBase.ItemIndex]), sServer, sDataBase);
             ParamTamanioDB := dmSistema.GetDBSize(cbBasesDatosBase.Llaves[cbBasesDatosBase.ItemIndex], sDataBase, TRUE);
             ParamLog:= dmSistema.GetDataLogSize(cbBasesDatosBase.Llaves[cbBasesDatosBase.ItemIndex], sDataBase, TRUE);
             lblDatoUbicacion.Caption := dmSistema.GetServer + '.' + Trim (txtBaseDatos.Text);
             {$endif}
        end

        else if Wizard.EsPaginaActual( FechaAlta ) then
        begin
             fecAlta.Valor := ZetaCommonTools.FirstDayOfYear(txtAnio.ValorEntero);
        end

        else if Wizard.EsPaginaActual( FechaBaja ) then
        begin
             fecBaja.Valor := ZetaCommonTools.FirstDayOfYear(txtAnio.ValorEntero-1);
        end
     end;
     inherited;
end;

procedure TWizAgregarBaseDatosPresupuestos_DevEx.rbOtraBaseDatosClick(
  Sender: TObject);
begin
     cbBasesDatosBase.Enabled := TRUE;
end;

procedure TWizAgregarBaseDatosPresupuestos_DevEx.rbValoresDefaultClick(
  Sender: TObject);
begin
     cbBasesDatosBase.Enabled := FALSE;
end;

procedure TWizAgregarBaseDatosPresupuestos_DevEx.WizardAlEjecutar(Sender: TObject;
  var lOk: Boolean);
begin
     inherited;
     if lOk then
         dmSistema.cdsSistBaseDatos.Refrescar;
end;

function TWizAgregarBaseDatosPresupuestos_DevEx.GetConnStr: widestring;
begin
  SC.ServerName := dmSistema.GetServer;
  SC.UserName := txtUsuario.Text;
  SC.Password := txtClave.Text;

  Result := 'Provider=SQLOLEDB.1;';
  Result := Result + 'Data Source=' + SC.ServerName + ';';
  if SC.DatabaseName <> '' then
    Result := Result + 'Initial Catalog=' + SC.DatabaseName + ';';

    Result := Result + 'uid=' + SC.UserName + ';';
    Result := Result + 'pwd=' + SC.Password + ';';

end;

procedure TWizAgregarBaseDatosPresupuestos_DevEx.rbSameUserClick(
  Sender: TObject);
begin
     txtUsuario.Enabled := FALSE;
     txtClave.Enabled := FALSE;
end;

procedure TWizAgregarBaseDatosPresupuestos_DevEx.rbOtroUserClick(
  Sender: TObject);
begin
     txtUsuario.Enabled := TRUE;
     txtUsuario.SetFocus;
     txtClave.Enabled := TRUE;
end;        

function TWizAgregarBaseDatosPresupuestos_DevEx.ExisteBD: boolean;
var i: integer;
begin
     Result := FALSE;

     for i:=0 to cbBasesDatos.Items.Count do
     begin
          if UpperCase (txtBaseDatos.Text) = UpperCase (cbBasesDatos.Items.Strings[i]) then
             Result := TRUE;
     end
end;

procedure TWizAgregarBaseDatosPresupuestos_DevEx.txtCodigoExit(
  Sender: TObject);
begin
      txtCodigo.Text :=  Copy(txtCodigo.Text, 0, 10);
end;

end.
