inherited AsisAutoricacionPreNominaGridSelect_DevEx: TAsisAutoricacionPreNominaGridSelect_DevEx
  Left = 147
  Top = 278
  Width = 693
  Height = 331
  Caption = 'Seleccione Los Empleados Deseados'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 257
    Width = 677
    inherited OK_DevEx: TcxButton
      Left = 514
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 592
    end
  end
  inherited PanelSuperior: TPanel
    Width = 677
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 677
    Height = 222
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
        MinWidth = 65
        Width = 65
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        MinWidth = 150
        Width = 150
      end
      object NO_HORAS: TcxGridDBColumn
        Caption = 'Ordinarias'
        DataBinding.FieldName = 'NO_HORAS'
        MinWidth = 75
        Width = 75
      end
      object NO_DOBLES: TcxGridDBColumn
        Caption = 'Dobles'
        DataBinding.FieldName = 'NO_DOBLES'
        MinWidth = 75
        Width = 75
      end
      object NO_TRIPLES: TcxGridDBColumn
        Caption = 'Triples'
        DataBinding.FieldName = 'NO_TRIPLES'
        MinWidth = 75
        Width = 75
      end
      object NO_HORA_CG: TcxGridDBColumn
        Caption = 'Horas CG'
        DataBinding.FieldName = 'NO_HORA_CG'
        MinWidth = 75
      end
      object NO_HORA_SG: TcxGridDBColumn
        Caption = 'Horas SG'
        DataBinding.FieldName = 'NO_HORA_SG'
        MinWidth = 75
      end
      object NO_DES_TRA: TcxGridDBColumn
        Caption = 'Desc. Trab.'
        DataBinding.FieldName = 'NO_DES_TRA'
        MinWidth = 75
      end
      object NO_DIAS_IN: TcxGridDBColumn
        Caption = 'Incapacidades'
        DataBinding.FieldName = 'NO_DIAS_IN'
        MinWidth = 98
        Width = 98
      end
      object NO_DIAS_VA: TcxGridDBColumn
        Caption = 'Vacaciones'
        DataBinding.FieldName = 'NO_DIAS_VA'
        MinWidth = 75
      end
      object NO_SUP_OK: TcxGridDBColumn
        Caption = 'Autorizado por '
        DataBinding.FieldName = 'NO_SUP_OK'
        MinWidth = 85
        Width = 85
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
