inherited WizNomAjusteRetFonacotCancelacion_DevEx: TWizNomAjusteRetFonacotCancelacion_DevEx
  Left = 745
  Top = 198
  Caption = 'Cancelar Ajuste de Retenci'#243'n Fonacot'
  ClientHeight = 437
  ClientWidth = 524
  ExplicitWidth = 530
  ExplicitHeight = 466
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 524
    Height = 437
    ParentFont = False
    ExplicitWidth = 524
    ExplicitHeight = 437
    inherited Parametros: TdxWizardControlPage
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'Este proceso cancela el ajuste de Fonacot de un mes espec'#237'fico, ' +
        'impacta en el saldo a favor o a cargo del pr'#233'stamo de ajuste de ' +
        'Fonacot.'
      Header.Title = 'Cancelar Ajuste de Retenci'#243'n Fonacot'
      ParentFont = False
      ExplicitWidth = 502
      ExplicitHeight = 297
      object GBTipoPrestamo: TGroupBox
        Left = 3
        Top = 64
        Width = 497
        Height = 139
        Align = alCustom
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Label2: TLabel
          Left = 152
          Top = 76
          Width = 22
          Height = 13
          Caption = 'A'#241'o:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object Label6: TLabel
          Left = 151
          Top = 102
          Width = 23
          Height = 13
          Caption = 'Mes:'
          Transparent = True
        end
        object Label1: TLabel
          Left = 27
          Top = 23
          Width = 147
          Height = 13
          Caption = 'Tipo de pr'#233'stamos de Fonacot:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object NoTipoPrestamo: TZetaTextBox
          Left = 178
          Top = 21
          Width = 50
          Height = 17
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object TipoPrestamo: TZetaTextBox
          Left = 234
          Top = 21
          Width = 241
          Height = 17
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object Label4: TLabel
          Left = 34
          Top = 48
          Width = 140
          Height = 13
          Caption = 'Tipo de pr'#233'stamo para &ajuste:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object MesDefault: TZetaKeyCombo
          Left = 178
          Top = 98
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          ListaFija = lfMeses
          ListaVariable = lvPuesto
          Offset = 1
          Opcional = False
          EsconderVacios = False
        end
        object YearDefaultAct: TZetaNumero
          Left = 178
          Top = 71
          Width = 50
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Mascara = mnDias
          ParentFont = False
          TabOrder = 0
          Text = '0'
        end
        object TipoPrestamoAjus: TZetaKeyLookup_DevEx
          Left = 178
          Top = 44
          Width = 297
          Height = 21
          LookupDataset = dmTablas.cdsTPresta
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 2
          TabStop = True
          WidthLlave = 50
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage [1]
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      ParentFont = False
      ExplicitWidth = 502
      ExplicitHeight = 297
      inherited sCondicionLBl: TLabel
        Left = 45
        Top = 149
        ExplicitLeft = 45
        ExplicitTop = 149
      end
      inherited sFiltroLBL: TLabel
        Left = 70
        Top = 178
        ExplicitLeft = 70
        ExplicitTop = 178
      end
      inherited Seleccionar: TcxButton
        Top = 266
        ExplicitTop = 266
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 97
        Top = 146
        ExplicitLeft = 97
        ExplicitTop = 146
      end
      inherited sFiltro: TcxMemo
        Left = 97
        Top = 177
        Style.IsFontAssigned = True
        ExplicitLeft = 97
        ExplicitTop = 177
      end
      inherited GBRango: TGroupBox
        Left = 97
        Top = 16
        ExplicitLeft = 97
        ExplicitTop = 16
      end
      inherited bAjusteISR: TcxButton
        Left = 411
        Top = 178
        ExplicitLeft = 411
        ExplicitTop = 178
      end
    end
    inherited Ejecucion: TdxWizardControlPage [2]
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      ParentFont = False
      ExplicitWidth = 502
      ExplicitHeight = 297
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 502
        ExplicitHeight = 202
        Height = 202
        Width = 502
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 502
        Width = 502
        inherited Advertencia: TcxLabel
          Caption = 
            'Al ejecutar este proceso se cancelar'#225' el cargo o abono a los pr'#233 +
            'stamos de ajuste seleccionados.'
          Style.IsFontAssigned = True
          ExplicitLeft = 67
          ExplicitTop = 20
          ExplicitWidth = 434
          ExplicitHeight = 74
          Width = 434
          AnchorY = 57
        end
        inherited cxImage1: TcxImage
          ExplicitLeft = 1
          ExplicitTop = 20
          ExplicitHeight = 74
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 40
    Top = 469
  end
end
