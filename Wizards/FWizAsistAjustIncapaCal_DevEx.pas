unit FWizAsistAjustIncapaCal_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ZetaEdit, StdCtrls,
  Buttons, ZetaWizard, ExtCtrls, Mask, ZetaFecha, ZCXBaseWizardFiltro,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, Menus, ZetaCXWizard, cxRadioGroup, cxTextEdit,
  cxMemo, ZetaKeyLookup_DevEx, cxButtons, dxGDIPlusClasses, cxImage,
  cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl;

type
  TWizAsistAjustIncapaCal_DevEx = class(TBaseCXWizardFiltro)
    GroupBox: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    zFechaFinal: TZetaFecha;
    zFechaInicial: TZetaFecha;
    chbBajas: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    { Protected declarations}
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;    
  end;

var
  WizAsistAjustIncapaCal_DevEx: TWizAsistAjustIncapaCal_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     DCliente,
     dProcesos,
     ZBaseSelectGrid_DevEx,
     FAsisAjusIncapaCalGridSelect_DevEx,
     ZetaDialogo;

{$R *.dfm}

procedure TWizAsistAjustIncapaCal_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := zFechaInicial;
     HelpContext := H_ASIST_AJUST_INCAPA_CAL;     
     with dmCliente do
     begin
          zFechaInicial.Valor := FechaAsistencia;
          zFechaFinal.Valor := FechaAsistencia;
     end;
end;

function TWizAsistAjustIncapaCal_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TAsisAjusIncapaCalGridSelect_DevEx );
end;

procedure TWizAsistAjustIncapaCal_DevEx.CargaListaVerificacion;
begin
     inherited;
     dmProcesos.AjusteIncapaCalGetLista( ParameterList );
end;

procedure TWizAsistAjustIncapaCal_DevEx.CargaParametros;
begin
     inherited;


     inherited CargaParametros;

     with ParameterList do
     begin
          AddDate( 'dInicial', zFechaInicial.Valor );
          AddDate( 'dFinal', zFechaFinal.Valor );
          AddString( 'Bajas', zBoolToStr( chbBajas.Checked ) );
     end;
     with Descripciones do
     begin
          AddDate( 'Fecha Inicial', zFechaInicial.Valor );
          AddDate( 'Fecha Final', zFechaFinal.Valor );
          AddBoolean( 'Incluir Bajas',  chbBajas.Checked  );
     end;
end;

function TWizAsistAjustIncapaCal_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.AjusteIncapaCal( ParameterList, Verificacion );
end;

procedure TWizAsistAjustIncapaCal_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
   ParametrosControl := nil;
     inherited;
     if Wizard.Adelante then
     begin
          if ( WizardControl.ActivePage = Parametros ) then
          begin
               if zFechaInicial.Valor > zFechaFinal.Valor then
                 CanMove := Error( '� Error En Rango De Fechas. ! La Fecha Final Debe Ser Mayor o Igual A La Fecha Inicial', zFechaInicial );
          end;
     end;
end;

procedure TWizAsistAjustIncapaCal_DevEx.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
begin
     if NOT (ZetaDialogo.ZConfirm('� Atenci�n !', 'Los cambios realizados por este proceso no se pueden deshacer '+CR_LF+' autom�ticamente, en caso de requerirlo, deber� hacerse manualmente. '+CR_LF+' �Desea continuar con el proceso? ',0, mbNo ) ) then
        CancelarWizard
     else
        inherited;
end;

procedure TWizAsistAjustIncapaCal_DevEx.WizardAfterMove(Sender: TObject);
begin
  inherited;
  if Wizard.EsPaginaActual(Parametros) then
    ParametrosControl := zFechaInicial;
end;

end.
