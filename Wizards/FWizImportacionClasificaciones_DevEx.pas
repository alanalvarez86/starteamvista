unit FWizImportacionClasificaciones_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ZetaEdit, StdCtrls,
  Buttons, ZetaWizard, ExtCtrls, ZetaKeyCombo, ZcxBaseWizard,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel,
  cxGroupBox, dxCustomWizardControl, dxWizardControl, Menus, cxButtons;
  
type
  TImportacionClasificaciones_DevEx = class(TcxBaseWizard)
    PanleFiltros: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    ArchivoSeek: TcxButton;
    Label4: TLabel;
    Archivo: TEdit;
    Formato: TZetaKeyCombo;
    Observaciones: TEdit;
    cbFFecha: TZetaKeyCombo;
    OpenDialog: TOpenDialog;
    PanelAnimation: TPanel;
    Animate: TAnimate;
    PanelMensaje: TPanel;
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);

  private
    FVerificaASCII: Boolean;
    FVerificacion: Boolean;
    ArchivoVerif : String;

    function LeerArchivo: Boolean;
    function GetArchivo: String;
    function Verificar: Boolean;

    procedure SetVerificacion(const Value: Boolean);
    procedure CargaListaVerificaASCII; overload;
    procedure CargaListaVerificacion; overload;
  protected
    procedure CargaParametros; override;
    function EjecutarWizard: Boolean; override;
  public
  end;

var
  ImportacionClasificaciones_DevEx: TImportacionClasificaciones_DevEx;

implementation

uses DProcesos,
     ZetaMessages,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZAsciiTools,
     ZetaClientTools,
     ZetaDialogo,
     ZBaseGridShow_DevEx,
     FAsiImportacionClasificacionesGridShow_DevEx,
     FImportarClasificacionesTemporalesGridSelect_DevEx,
     ZBaseSelectGrid_DevEx;

{$R *.dfm}

procedure TImportacionClasificaciones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     PanelAnimation.Visible := FALSE;
     ParametrosControl := Archivo;
     Archivo.Text := ZetaMessages.SetFileNameDefaultPath( 'CLASIFICACIONES.DAT' );

     Formato.ItemIndex := Ord( faASCIIFijo );
     cbFFecha.ItemIndex := Ord( ifDDMMYYYYs );
     HelpContext:= H_Importacion_Clasificaciones;
end;

procedure TImportacionClasificaciones_DevEx.CargaParametros;
begin
  inherited CargaParametros;
     with Descripciones do
     begin
          AddString( 'Archivo', Archivo.Text );
          AddString( 'Formato ASCII', ObtieneElemento( lfFormatoASCII, Formato.Valor ) );
          AddString( 'Formato fechas', ObtieneElemento( lfFormatoImpFecha, cbFFecha.Valor ) );
          AddMemo( 'Observaciones', Observaciones.Text );
     end;



     with ParameterList do
     begin
          AddInteger( 'Formato', Formato.Valor );
          AddInteger( 'FormatoImpFecha', cbFFecha.Valor );
          AddString( 'Archivo', Archivo.Text );
          AddMemo( 'Observaciones', Observaciones.Text );
     end;
end;

procedure TImportacionClasificaciones_DevEx.CargaListaVerificaASCII;
begin
     with dmProcesos do
     begin
          ImportarClasificacionesTemporalesGetListaASCII( ParameterList );
          if ( ErrorCount > 0 ) then                 // Hubo Errores
          begin
               with dmProcesos do
               begin
                    ZAsciiTools.FiltraASCII( cdsDataSet, True );
                    FVerificaASCII := ZBaseGridShow_DevEx.GridShow( cdsDataset, TAsiImportacionClasificacionesGridShow_DevEx );
                    ZAsciiTools.FiltraASCII( cdsDataSet, False );
               end;
          end
          else
              FVerificaASCII := True;
     end;
end;

procedure TImportacionClasificaciones_DevEx.CargaListaVerificacion;
begin
     dmProcesos.ImportarClasificacionesTemporalesGetLista( ParameterList );
end;

function TImportacionClasificaciones_DevEx.Verificar: Boolean;
begin
     dmProcesos.cdsDataSet.First;
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TImportarClasificacionesTemporalesGridSelect_DevEx, FALSE );
end;

procedure TImportacionClasificaciones_DevEx.SetVerificacion(const Value: Boolean);
begin
     FVerificacion := Value;
end;

function TImportacionClasificaciones_DevEx.GetArchivo: String;
begin
     Result := Archivo.Text;
end;


function TImportacionClasificaciones_DevEx.LeerArchivo: Boolean;
begin
     Result := False;
     PanelAnimation.Visible := TRUE;
     Application.ProcessMessages;
     CargaParametros;
     CargaListaVerificaASCII;
     PanelAnimation.Visible := FALSE;
     if FVerificaASCII then
     begin
          CargaListaVerificacion;
          SetVerificacion( Verificar );
          Result := FVerificacion;
     end;
     if Result then
        ArchivoVerif := GetArchivo
     else
        ArchivoVerif := VACIO;
end;

procedure TImportacionClasificaciones_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
ParametrosControl := nil;
     inherited;
     if Wizard.Adelante then
     begin
          if ( WizardControl.ActivePage = Parametros ) then
          begin
               if StrVacio( GetArchivo ) then
                  CanMove := Error( 'Debe Especificarse un Archivo a Importar', Archivo )
               else
                  if not FileExists( GetArchivo ) then
                     CanMove := Error( 'El Archivo ' + GetArchivo + ' No Existe', Archivo )
                  else
                  begin
                       if ( ArchivoVerif <> GetArchivo ) or
                          ( ZConfirm( Caption, 'El Archivo ' + GetArchivo + ' Ya Fu� Verificado !' + CR_LF +
                                                  'Volver a Verificar ?', 0, mbYes ) ) then
                          CanMove := LeerArchivo
                       else
                          CanMove := TRUE;
                  end;
          end;
     end;
end;

procedure TImportacionClasificaciones_DevEx.ArchivoSeekClick(Sender: TObject);
begin
     Archivo.Text := AbreDialogo( OpenDialog, Archivo.Text, 'dat' );
end;

function TImportacionClasificaciones_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.ImportarClasificacionesTemporales( ParameterList );
end;

procedure TImportacionClasificaciones_DevEx.WizardAfterMove(
  Sender: TObject);
begin
    if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := Archivo;

     inherited;

end;

end.
