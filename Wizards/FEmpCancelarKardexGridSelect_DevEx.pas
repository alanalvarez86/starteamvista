unit FEmpCancelarKardexGridSelect_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ExtCtrls, Db, ZBaseSelectGrid_DevEx,
     cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
     Menus, dxSkinsCore, 
     TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
     dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
     cxEdit, cxNavigator, cxDBData, cxTextEdit, cxCalendar,
     cxGridCustomTableView, cxGridTableView, cxGridDBTableView, ImgList,
     cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons;

type
  TEmpCancelarKardexGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CB_FECHA: TcxGridDBColumn;
    CB_FEC_CAP: TcxGridDBColumn;
    CB_TIPO: TcxGridDBColumn;
    US_CODIGO: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpCancelarKardexGridSelect_DevEx: TEmpCancelarKardexGridSelect_DevEx;

implementation

{$R *.DFM}

end.
