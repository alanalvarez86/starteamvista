unit FWizEmpComidasGrupales_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ZetaEdit, StdCtrls, ExtCtrls, ZetaHora,
  Mask, ZetaFecha, ZetaNumero,
  FWizEmpBaseFiltro, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, ZetaKeyLookup_DevEx, cxButtons, cxRadioGroup, cxTextEdit,
  cxMemo, cxGroupBox, cxLabel, dxGDIPlusClasses, cxImage,
  dxCustomWizardControl, dxWizardControl;

type
  TWizEmpComidasGrupales_DevEx = class(TWizEmpBaseFiltro)
    GroupBox1: TcxGroupBox;
    lblNomArchivo: TLabel;
    edArchivo: TEdit;
    ArchivoSeek: TcxButton;
    GroupBox2: TcxGroupBox;
    lblFechaReg: TLabel;
    zFecReg: TZetaFecha;
    zHoraReg: TZetaHora;
    cbTipoComida: TComboBox;
    znCantidad: TZetaNumero;
    lblCantidad: TLabel;
    lblTipoComida: TLabel;
    lblHora: TLabel;
    OpenDialog: TOpenDialog;
    rbGeneraChecada: TcxRadioButton;
    rbImpArchivo: TcxRadioButton;
    Label1: TLabel;
    cbEstacion: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    //DevEx     //Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure rbGeneraChecadaClick(Sender: TObject);
    procedure rbImpArchivoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure SetControlsChecadas;
    function Generar: Boolean;
    procedure SetControls(const lGeneraChecada: Boolean);
    procedure Cargar;
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaParametros; override;
    procedure CargaListaVerificacion; override;
  public
    { Public declarations }
  end;

var
  WizEmpComidasGrupales_DevEx: TWizEmpComidasGrupales_DevEx;

implementation
uses
    DProcesos,
    DCliente,
    ZBaseSelectGrid_DevEx,
    ZetaClientTools,
    ZetaCommonTools,
    ZetaCommonClasses,
    ZetaCommonLists,
    ZetaDialogo,
    ZAsciiTools,
    ZGlobalTress,
    FEmpleadoGridSelect_DevEx, ZcxWizardBasico,
    DSistema;


{$R *.DFM}
procedure TWizEmpComidasGrupales_DevEx.FormCreate(Sender: TObject);
begin
     ParametrosControl := zFecReg;
     inherited;
     HelpContext := H11623_Registro_grupal_de_comidas;

     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;

     {***Se define el tipo de salto a utilizar en los wizards***}
     Wizard.SaltoEspecial := TRUE;
end;

procedure TWizEmpComidasGrupales_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     SetControls(True);
     cbTipoComida.ItemIndex := 0;
     zFecReg.Valor := dmCliente.FechaDefault;
     zHoraReg.Valor := FormatDateTime( 'hhnn', Now );
     Cargar;

     Advertencia.Caption := 'Al ejecutar el proceso se registrará la comida al grupo de empleados indicado.';
end;

procedure TWizEmpComidasGrupales_DevEx.CargaParametros;
var
   lGeneraChecada: Boolean;
begin
     lGeneraChecada := Generar;

     inherited CargaParametros;

     with ParameterList do
     begin
          AddBoolean( 'GenerarChecadas', lGeneraChecada );
          if( lGeneraChecada )then
          begin
               AddDate( 'Fecha', zFecReg.Valor );
               AddString( 'Hora', zHoraReg.Text  );
               AddInteger( 'TipoComida', ( cbTipoComida.ItemIndex + 1 ) );
               AddInteger( 'Cantidad', znCantidad.ValorEntero );
               if cbEstacion.ItemIndex > -1 then
                  AddString( 'Reloj',  cbEstacion.Text )
               else
                   AddString( 'Reloj',  VACIO );
          end
          else
          begin
               AddString( 'Archivo', edArchivo.Text )
          end;
     end;


     with Descripciones do
     begin
     //     Clear;   //DevEx
          if lGeneraChecada then
             AddBoolean( 'Generar checadas', lGeneraChecada )
          else
              AddBoolean( 'Importar archivo', ( not lGeneraChecada ) );

          if( lGeneraChecada )then
          begin
               AddDate( 'Registro', zFecReg.Valor );
               AddString( 'Hora', MaskHora( zHoraReg.Text  ) );
               AddInteger( 'Tipo de comida', ( cbTipoComida.ItemIndex + 1 ) );
               AddInteger( 'Cantidad', znCantidad.ValorEntero );
               if cbEstacion.ItemIndex > -1 then
                  AddString( 'Estación',  cbEstacion.Text );
          end
          else
          begin
               AddString( 'Archivo de checadas', edArchivo.Text )
          end;
     end;

end;

procedure TWizEmpComidasGrupales_DevEx.CargaListaVerificacion;
begin
     dmProcesos.ComidasGrupalesGetLista( ParameterList );
end;

function TWizEmpComidasGrupales_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpleadoGridSelect_DevEx );
end;

function TWizEmpComidasGrupales_DevEx.EjecutarWizard: Boolean;
begin
     if( Generar )then
         Result := dmProcesos.ComidasGrupales( ParameterList, Verificacion )
     else
         Result := dmProcesos.ComidasGrupalesListaASCII( ParameterList );
end;

//DevEx, modificado por uso de nuevos controles wizard
procedure TWizEmpComidasGrupales_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   sArchivo: String;
 begin

     ParametrosControl := nil;
     sArchivo := edArchivo.Text;
     with Wizard do
     begin
          //hacia adelante
          if CanMove and Adelante then
          begin
            if EsPaginaActual( Parametros ) then   //Si partimos de primera pag.
               begin
               if rbImpArchivo.Checked then
               begin
                    if ( strLleno( sArchivo ) )then
                    begin
                         if ( FileExists( sArchivo ) ) then
                         begin
                            iNewPage := Ejecucion.PageIndex;
                         end
                         else
                            CanMove := Error( 'El archivo especificado no existe', edArchivo );
                    end
                    else
                        CanMove := Error( 'No se ha especificado archivo', edArchivo );
               end  //endif selecciono archivo
               else
               begin
                    if ( znCantidad.ValorEntero <= 0 ) then
                       CanMove := Error( 'La cantidad debe de ser mayor que cero', znCantidad )
                    else
                        iNewPage := FiltrosCondiciones.PageIndex;
               end;
               end   //end selecciono checadas

               else if EsPaginaActual( FiltrosCondiciones ) then   //Si partimos de segunda pag.
                    iNewPage := Ejecucion.PageIndex;
          end;

          //hacia atras
          if ( not Adelante )then
          begin
               if EsPaginaActual( Ejecucion) then      //Si partimos de pag. final
               begin
                    if rbImpArchivo.Checked then
                       iNewPage := Parametros.PageIndex       //si selecciono archivo
                    else
                        iNewPage := FiltrosCondiciones.PageIndex   //si selecciono checadas
               end
               else if EsPaginaActual( FiltrosCondiciones ) then  //Si partimos de segunda pag.
                    iNewPage := Parametros.PageIndex;
          end;
     end;
     inherited;
end;

procedure TWizEmpComidasGrupales_DevEx.ArchivoSeekClick(Sender: TObject);
begin
     inherited;
     with OpenDialog do
     begin
          Filter := 'Archivos DAT ( *.dat )|*.dat|Archivos TXT ( *.txt )|*.txt|Todos ( *.* )|*.*';
          FilterIndex := 0;
          Title := 'Seleccione el archivo deseado';
     end;
     edArchivo.Text := ZetaClientTools.AbreDialogo( OpenDialog, edArchivo.Text, 'DAT' );
end;

procedure TWizEmpComidasGrupales_DevEx.rbGeneraChecadaClick(Sender: TObject);
begin
     inherited;
     SetControls( True );
end;

function TWizEmpComidasGrupales_DevEx.Generar: Boolean;
begin
     Result := rbGeneraChecada.Checked;
end;

procedure TWizEmpComidasGrupales_DevEx.SetControls( const lGeneraChecada: Boolean );
begin
     rbGeneraChecada.Checked := lGeneraChecada;
     rbImpArchivo.Checked := not lGeneraChecada;
     SetControlsChecadas;
end;

procedure TWizEmpComidasGrupales_DevEx.SetControlsChecadas;
begin
     //Generar Checadas
     zFecReg.Enabled := Generar;
     lblFechaReg.Enabled := Generar;
     zHoraReg.Enabled := Generar;
     lblHora.Enabled := Generar;
     cbTipoComida.Enabled := Generar;
     lblTipoComida.Enabled := Generar;
     znCantidad.Enabled := Generar;
     lblCantidad.Enabled := Generar;

     //Importa Archivo
     lblNomArchivo.Enabled := not Generar;
     edArchivo.Enabled := not Generar;
     ArchivoSeek.Enabled := not Generar;
end;

procedure TWizEmpComidasGrupales_DevEx.rbImpArchivoClick(Sender: TObject);
begin
     inherited;
     SetControls( False );
end;


//DevEx     //Agregado para enfocar un control
procedure TWizEmpComidasGrupales_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := zFecReg
     else
         ParametrosControl := nil;
     inherited;
end;

procedure TWizEmpComidasGrupales_DevEx.Cargar;
var
   i: integer;
   lstEstacionesCafe : TStrings;
begin
     lstEstacionesCafe := TStringList.Create;
     try
        dmSistema.CargaEstacionesCafeteria( lstEstacionesCafe );
        with cbEstacion do
        begin
             Items.BeginUpdate;
             try
                Clear;
                with lstEstacionesCafe do
                begin
                     for i := 0 to ( Count - 1 ) do
                         Items.Add( lstEstacionesCafe[i]);
                end;
             finally
                    Items.EndUpdate;
             end;
        end;

     finally
            lstEstacionesCafe.Free;
     end;
end;


end.
