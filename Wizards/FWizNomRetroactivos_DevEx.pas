unit FWizNomRetroactivos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ZetaEdit, StdCtrls, ZetaDBTextBox, ExtCtrls,
  Mask, ZetaFecha, CheckLst, ZetaNumero, ZetaKeyCombo,
  FWizNomBasico_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
  cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl, FWizNomBase_DevEx, cxCheckListBox;

type
  TWizNomRetroactivos_DevEx = class(TWizNomBase_DevEx)
    tsCalculaRetroactivos: TdxWizardControlPage;
    lblNuevoSalario: TLabel;
    zfNuevoSalario: TZetaFecha;
    GroupBox2: TcxGroupBox;
    lblAnio: TLabel;
    lblDesde: TLabel;
    lblHasta: TLabel;
    LkpDesdeSem: TZetaKeyLookup_DevEx;
    lkpHastaSem: TZetaKeyLookup_DevEx;
    tsConceptos: TdxWizardControlPage;
    znAnio: TZetaNumero;
    edGlobalRetroactivo: TEdit;
    ListaConceptos: TcxCheckListBox;
    Label5: TLabel;
    lkpConcepto: TZetaKeyLookup_DevEx;
    Label6: TLabel;
    cbExisteConcepto: TZetaKeyCombo;
    GroupBox3: TcxGroupBox;
    chbIncluirAnio: TCheckBox;
    lblAnioAd: TLabel;
    znAnioAd: TZetaNumero;
    lblDesdeAd: TLabel;
    LkpDesdeSemAd: TZetaKeyLookup_DevEx;
    lblHastaAd: TLabel;
    lkpHastaSemAd: TZetaKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    //DevEx Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
    procedure znAnioExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure chbIncluirAnioClick(Sender: TObject);
    procedure znAnioAdExit(Sender: TObject);
    //procedure ProporcionalBuildClick(Sender: TObject);
  private
    lCambioAnio: Boolean;
    procedure LlenaListaConceptos;
    procedure HabilitaControles( lIncluye: Boolean );
    procedure HabilitaControlesTipoNomina( lActiva : Boolean );
    procedure ValidaControles( Anio: TZetaNumero; DesdeSem, HastaSem: TZetaKeyLookup_DevEx; var CanMove: Boolean);
    procedure GetPeriodoAd;
    function LlenaGlobalConceptos: Boolean;
    { Private declarations }
  protected
     function EjecutarWizard: Boolean; override;
     function Verificar: Boolean; override;
     procedure CargaParametros;override;
     procedure CargaListaVerificacion; override;
  public
    { Public declarations }
  end;

var
  WizNomRetroactivos_DevEx: TWizNomRetroactivos_DevEx;

implementation

uses DCliente,
     DProcesos,
     DCatalogos,
     ZGlobalTress,
     ZetaCommonLists,
     ZetaCommontools,
     ZetaCommonClasses,
     ZetaDialogo,
     ZBaseSelectGrid_DevEx,
     FNomRetroactGridSelect_DevEx;

{$R *.DFM}

procedure TWizNomRetroactivos_DevEx.FormCreate(Sender: TObject);
var
   sTipoNomina: String;
begin
     inherited;
     lCambioAnio := False;
     with dmCliente do
     begin
          sTipoNomina := ObtieneElemento( lfTipoNomina, Ord( PeriodoTipo ) );
          zfNuevoSalario.Valor := FechaDefault;
          znAnio.Valor := YearDefault;
          edGlobalRetroactivo.Tag := K_GLOBAL_RETROACT_CONCEPTOS;
          lblDesde.Caption := Format( 'Desde %s:', [ sTipoNomina ] );
          lblHasta.Caption := Format( 'Hasta %s:', [ sTipoNomina ] );
          lblDesdeAd.Caption := Format( 'Desde %s:', [ sTipoNomina ] );
          lblHastaAd.Caption := Format( 'Hasta %s:', [ sTipoNomina ] );
          if ( sTipoNomina = VACIO ) or ( Ord( PeriodoTipo ) = K_PERIODO_VACIO ) then
             HabilitaControlesTipoNomina( FALSE );
          //iTipoNomina.Caption := InttoStr( PeriodoNumero );
     end;
     cbExisteConcepto.ItemIndex := Ord( omSustituir );
     with dmCatalogos do
     begin
          cdsPeriodo.Conectar;
          cdsConceptos.Conectar;
          lkpConcepto.LookupDataset := cdsConceptos;
          lkpConcepto.Tag           := K_GLOBAL_RETROACT_CONCEPTO_PAGAR;
          lkpDesdeSem.LookupDataset := cdsPeriodo;
          lkpHastaSem.LookupDataset := cdsPeriodo;
     end;
     lkpDesdeSem.Valor := ( dmCliente.PeriodoNumero - 1 );
     lkpHastaSem.Valor := lkpDesdeSem.Valor;
     ParametrosControl := lkpConcepto;
     HelpContext := H33423_Calcular_retroactivos;

     //DevEx
     tsCalculaRetroactivos.PageIndex := 0;
     tsConceptos.PageIndex := 1;
     Parametros.PageIndex := 2;
     FiltrosCondiciones.PageIndex := 3;
     Ejecucion.PageIndex := 4;
end;

procedure TWizNomRetroactivos_DevEx.LlenaListaConceptos;
var
   oLista: TStrings;
   sGlobalConceptos, sIndice: String;
   iConcepto : integer;
begin
     sGlobalConceptos := edGlobalRetroactivo.Text;
     oLista := TStringList.Create;
     try
        oLista.CommaText := sGlobalConceptos;
        with dmCatalogos.cdsConceptos do
        begin
             DisableControls;
             sIndice := IndexFieldNames;
             try
                IndexFieldNames := 'CO_NUMERO';
                if not Active then
                   Conectar;
                First;
                while not Eof do
                begin
                     if ( FieldByName('CO_NUMERO').AsInteger <= K_MAX_CONCEPTO ) then
                     begin
                          //ListaConceptos.Items.Add( FieldByName( 'CO_NUMERO' ).AsString + ' = ' + FieldByName( 'CO_DESCRIP' ).AsString );
                          ListaConceptos.Items.Add.Text := FieldByName( 'CO_NUMERO' ).AsString + ' = ' + FieldByName( 'CO_DESCRIP' ).AsString ;  //DevEx
                          if ( oLista.Count > 0 ) then
                          begin
                               iConcepto := StrToIntDef( oLista[ 0 ], 0 );
                               if( FieldByName( 'CO_NUMERO' ).AsInteger > iConcepto )then
                                   oLista.Delete( 0 );

                               iConcepto := StrToIntDef( oLista[ 0 ], 0 );
                               if( FieldByName( 'CO_NUMERO' ).AsInteger = iConcepto )then
                               begin
                                    //ListaConceptos.Checked[ ( ListaConceptos.Items.Count - 1 ) ] := True;
                                    ListaConceptos.Items[ ( ListaConceptos.Items.Count - 1 ) ].Checked := True;  //DevEx
                                    oLista.Delete( 0 );
                               end;
                          end;
                     end;
                     Next;
                end;
             finally
                    IndexFieldNames := sIndice;
                    EnableControls;
             end;
        end;
     finally
            FreeAndNil( oLista );
     end;
end;


procedure TWizNomRetroactivos_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     ParametrosControl := nil;
     ActiveControl := nil;
     inherited;
     if CanMove then
     begin
          with Wizard do
          begin
               if EsPaginaActual( tsCalculaRetroactivos ) and Adelante then
               begin
                    ValidaControles( znAnio, lkpDesdeSem, lkpHastaSem, CanMove );
                    if CanMove AND chbIncluirAnio.Checked then
                          ValidaControles( znAnioAd, lkpDesdeSemAd, lkpHastaSemAd, CanMove );
               end
               else
               begin
                    if( EsPaginaActual( tsConceptos ) ) and Adelante then
                    begin
                         if not LlenaGlobalConceptos then
                         begin
                              zError( Caption, 'Se debe de seleccionar por lo menos un Concepto', 0 );
                              ActiveControl := ListaConceptos;
                              CanMove := False;
                         end;
                    end
                    else
                    begin
                         if( EsPaginaActual(  Parametros ) ) and Adelante then
                         begin
                              if strVacio( lkpConcepto.Llave ) then
                              begin
                                   zError( Caption, 'El Concepto a pagar no puede ser vac�o', 0 );
                                   ActiveControl := lkpConcepto;
                                   CanMove := False;
                              end;
                         end
                    end;
               end;
          end;
     end;
end;


procedure TWizNomRetroactivos_DevEx.CargaParametros;
var
   sTipoNomina: string;
begin
     sTipoNomina := ObtieneElemento( lfTipoNomina, Ord( dmCliente.PeriodoTipo ) );

     inherited;

     with ParameterList do
     begin
          AddDate('CambioSalario', zfNuevoSalario.Valor );
          AddInteger( 'NominaYear', znAnio.ValorEntero );
          AddInteger( 'NominaDesde', lkpDesdeSem.Valor );
          AddInteger( 'NominaHasta', lkpHastaSem.Valor );
          AddString( 'ListaConceptos', edGlobalRetroactivo.Text );
          AddInteger( 'ConceptoRetroactivo', lkpConcepto.Valor );
          AddInteger( 'NominaAPagar', dmCliente.PeriodoNumero );
          AddInteger( 'ExcepcionOp', cbExisteConcepto.Valor );
          AddBoolean( 'MuestraTiempos', FALSE );
          AddBoolean( 'MuestraScript', FALSE );
          AddInteger( 'TipoCalculo', Ord( tcTodos ) );
          AddBoolean ( 'IncluyeAnio', chbIncluirAnio.Checked );
          AddInteger( 'NominaYearAd', znAnioAd.ValorEntero );
          AddInteger( 'NominaDesdeAd', lkpDesdeSemAd.Valor );
          AddInteger( 'NominaHastaAd', lkpHastaSemAd.Valor );
     end;

     //DevEx
     with Descripciones do
     begin
          AddDate( 'Fecha del nuevo salario', zfNuevoSalario.Valor );
          AddInteger( 'A�o a calcular', znAnio.ValorEntero );
          AddString( Format( 'Desde %s', [ sTipoNomina ] ), GetDescripLlave( lkpDesdeSem ) );
          AddString( Format( 'Hasta %s', [ sTipoNomina ] ), GetDescripLlave( lkpHastaSem ) );
          AddBoolean ( 'Incluye A�o Adicional', chbIncluirAnio.Checked );
          if ( chbIncluirAnio.Checked ) then
          begin
               AddInteger( 'A�o Adicional a calcular', znAnioAd.ValorEntero );
               AddString( Format( 'Adicional Desde %s', [ sTipoNomina ] ), GetDescripLlave( lkpDesdeSemAd ) );
               AddString( Format( 'Adicional Hasta %s', [ sTipoNomina ] ), GetDescripLlave( lkpHastaSemAd ) );
          end;
          AddString( 'Conceptos a considerar', edGlobalRetroactivo.Text );
          AddString( 'Concepto retroactivo', GetDescripLlave( lkpConcepto ) );
          AddString( 'Si ya existe la excepci�n', cbExisteConcepto.Items.Strings[ cbExisteConcepto.Valor ] );

     end;
end;

function TWizNomRetroactivos_DevEx.LlenaGlobalConceptos: Boolean;
var
   iMaxConceptos, i: Integer;
   sValorConcepto, sConceptos: String;

begin
     Result := False;
     sConceptos := VACIO;

     with ListaConceptos do
     begin
          iMaxConceptos := Items.Count;
          for i := 0 to ( iMaxConceptos - 1 ) do
          begin
               //if Checked[ i ] then
               if Items[ i ].Checked then  //DevEx
               begin
                    Result := True;
                    //sValorConcepto := Trim( Items.Names[ i ] );
                   sValorConcepto := Trim ( (TCheckListBox(InnerCheckListBox)).Items.Names[i]) ;    //DevEx
                    if StrLleno( sConceptos ) then
                         sConceptos := sConceptos + ',' + sValorConcepto
                    else
                        sConceptos := sValorConcepto;
               end;
          end;
     end;
     edGlobalRetroactivo.Text := sConceptos;
end;

procedure TWizNomRetroactivos_DevEx.znAnioExit(Sender: TObject);
begin
     inherited;
     dmCatalogos.GetDatosPeriodo( znAnio.ValorEntero, dmCliente.PeriodoTipo );
end;

function TWizNomRetroactivos_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TGridCalculaRetoactivos_DevEx );
end;

procedure TWizNomRetroactivos_DevEx.CargaListaVerificacion;
begin
     dmProcesos.CalculaRetroactivoGetLista( ParameterList );
end;

function TWizNomRetroactivos_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CalculaRetroactivos( ParameterList, Verificacion );
end;

procedure TWizNomRetroactivos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     ActiveControl := zfNuevoSalario;
     LlenaListaConceptos;
     HabilitaControles( chbIncluirAnio.Checked );

     //DevEx
     Advertencia.Caption :=
        'Al aplicar el proceso se efectuar� el c�lculo de retroactivos para los empleados ' +
        'seleccionados de acuerdo a los par�metros de n�mina que se especificaron.';
end;

procedure TWizNomRetroactivos_DevEx.HabilitaControles( lIncluye: Boolean );
begin
     znAnioAd.Enabled:= lIncluye;
     LkpDesdeSemAd.Enabled:= lIncluye;
     lkpHastaSemAd.Enabled:= lIncluye;
     lblAnioAd.Enabled:= lIncluye;
     lblDesdeAd.Enabled:= lIncluye;
     lblHastaAd.Enabled:= lIncluye;
end;

procedure TWizNomRetroactivos_DevEx.chbIncluirAnioClick(Sender: TObject);
begin
     inherited;
     HabilitaControles( chbIncluirAnio.Checked );
     znAnioAd.Valor := (znAnio.Valor - 1);
     GetPeriodoAd;
end;

procedure TWizNomRetroactivos_DevEx.znAnioAdExit(Sender: TObject);
begin
     inherited;
     GetPeriodoAd;
end;

procedure TWizNomRetroactivos_DevEx.ValidaControles(Anio: TZetaNumero; DesdeSem, HastaSem: TZetaKeyLookup_DevEx; var CanMove: Boolean);
begin
     if strVacio( Anio.ValorAsText ) or ( Anio.Valor = 0 ) then
     begin
          zError( Caption, 'El A�o no puede ser cero � vac�o', 0 );
          ActiveControl := Anio;
          CanMove := False;
     end
     else
     begin
          if strLleno( DesdeSem.Llave ) and strLleno( HastaSem.Llave ) then
          begin
               if ( DesdeSem.Valor > HastaSem.Valor ) then
               begin
                    zError( Caption, 'El Periodo Inicial no puede ser mayor al Periodo Final', 0 );
                    ActiveControl := DesdeSem;
                    CanMove := False;
               end
               else
               begin
                    if ( lkpHastaSem.Valor >= dmCliente.PeriodoNumero ) then
                    begin
                         zError( Caption, 'El Periodo Final debe ser menor al Periodo Activo', 0 );
                         ActiveControl := HastaSem;
                         CanMove := False;
                    end;
               end;
               end
          else
          begin
               zError( Caption, 'Los Periodos no pueden quedar vac�os', 0 );
               if strVacio( DesdeSem.Llave )then
                  ActiveControl := DesdeSem
               else
                  ActiveControl := HastaSem;
               CanMove := False;
          end;
     end;
end;

procedure TWizNomRetroactivos_DevEx.GetPeriodoAd;
begin
     with dmCatalogos.cdsPeriodoAd do
     begin
          try
             Refrescar;
             dmCatalogos.GetDatosPeriodoAd( znAnioAd.ValorEntero, dmCliente.PeriodoTipo );                     
             Filtered := False;
             Filter := 'PE_USO = 0 and PE_STATUS = 6';
             Filtered := True;
             Last;
             LkpDesdeSemAd.Valor := FieldByName('PE_NUMERO').AsInteger;
             lkpHastaSemAd.Valor := LkpDesdeSemAd.Valor;
             if ( Ord( dmCliente.PeriodoTipo ) = K_PERIODO_VACIO ) then
                HabilitaControlesTipoNomina( FALSE );
          finally
                 Filtered := False;
                 Filter := VACIO;
          end;
     end;
end;


procedure TWizNomRetroactivos_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := lkpConcepto
     else
         ParametrosControl := nil;

     if Wizard.EsPaginaActual(tsCalculaRetroactivos) then
        ActiveControl := zfNuevoSalario
     else
         ActiveControl := nil;
     inherited;
end;

procedure TWizNomRetroactivos_DevEx.HabilitaControlesTipoNomina( lActiva: Boolean );
begin
     LkpDesdeSem.Enabled := lActiva;
     lkpHastaSem.Enabled := lActiva;
     LkpDesdeSemAd.Enabled:= lActiva;
     lkpHastaSemAd.Enabled:= lActiva;
     LkpDesdeSemAd.Llave := VACIO;
     lkpHastaSemAd.Llave := VACIO;
end;

end.
