unit FWizNomCalcularDiferencias_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, ExtCtrls, Mask,
     ZetaWizard, ZetaKeyCombo, ZetaFecha, ZetaNumero, ZetaEdit,
     ZetaCommonLists, FWizNomBasico_DevEx, cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
     cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons,
     dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
     dxWizardControl;

type
  TWizNomCalcularDiferencias_DevEx = class(TWizNomBasico_DevEx)
    TipoNomina: TZetaKeyCombo;
    TipoLBL: TLabel;
    NominaOriginal: TcxGroupBox;
    YearOriginalLBL: TLabel;
    NumeroOriginalLBL: TLabel;
    YearOriginal: TZetaNumero;
    NumeroOriginal: TZetaNumero;
    NominaNueva: TcxGroupBox;
    YearNuevoLBL: TLabel;
    NumeroNuevoLBL: TLabel;
    YearNuevo: TZetaNumero;
    NumeroNuevo: TZetaNumero;
    NominaDiferencias: TcxGroupBox;
    YearDiferenciasLBL: TLabel;
    NumeroDiferenciasLBL: TLabel;
    YearDiferencias: TZetaNumero;
    NumeroDiferencias: TZetaNumero;
    Conceptos: TdxWizardControlPage;
    GroupBox1: TcxGroupBox;
    lbInicialConcepto: TLabel;
    lbFinalConcepto: TLabel;
    BFinalConcepto: TcxButton;
    BInicialConcepto: TcxButton;
    BListaConcepto: TcxButton;
    RBTodosConcepto: TcxRadioButton;
    RBRangoConcepto: TcxRadioButton;
    RBListaConcepto: TcxRadioButton;
    EInicialConcepto: TZetaEdit;
    EFinalConcepto: TZetaEdit;
    EListaConcepto: TZetaEdit;
    procedure FormCreate(Sender: TObject);
    procedure RBTodosConceptoClick(Sender: TObject);
    procedure RBRangoConceptoClick(Sender: TObject);
    procedure RBListaConceptoClick(Sender: TObject);
    procedure BInicialConceptoClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    //DevEx Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
    procedure BFinalConceptoClick(Sender: TObject);
    procedure BListaConceptoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FRangoConcepto: eTipoRangoActivo;
    function GetConceptos: String;
    function ValidaPeriodos: Boolean;
    procedure EnabledBotonesConcepto( eTipo: eTipoRangoActivo );
    procedure BuscaDialogo(oEdit: TZetaEdit; const lConcatena: Boolean);
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros;override;
  end;

var
  WizNomCalcularDiferencias_DevEx: TWizNomCalcularDiferencias_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaClientTools,
     FTressShell,
     DCliente,
     DProcesos,
     DCatalogos, ZcxWizardBasico;

{$R *.DFM}

procedure TWizNomCalcularDiferencias_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     with dmCliente.GetDatosPeriodoActivo do
     begin
//          TipoNomina.ItemIndex := Ord( Tipo );//acl
          TipoNomina.Llave := IntToStr( Ord( Tipo ) );//acl
          YearOriginal.Valor := Year;
          YearNuevo.Valor := Year;
          YearDiferencias.Valor := Year;
          NumeroOriginal.Valor := Numero;
     end;
     ParametrosControl := TipoNomina;
     HelpContext := H33420_Calcular_diferencias;

      //DevEx
     Parametros.PageIndex := 0;
     Conceptos.PageIndex := 1;
     FiltrosCondiciones.PageIndex := 2;
     Ejecucion.PageIndex := 3;
end;

procedure TWizNomCalcularDiferencias_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     dmCatalogos.cdsConceptos.Conectar;
     with TipoNomina do
     begin
          ListaFija:=lfTipoPeriodoConfidencial; //acl
          LlaveEntero:= Ord( dmCliente.GetDatosPeriodoActivo.Tipo );//acl
     end;

     //DevEx
     Advertencia.Caption :=
        'El proceso calcula las diferencias de los conceptos para los empleados seleccionados y las registra como una excepci�n en la n�mina de diferencias.'
end;

procedure TWizNomCalcularDiferencias_DevEx.CargaParametros;
begin
     inherited;
     with ParameterList do
     begin
          AddInteger( 'Tipo', TipoNomina.LlaveEntero ); //acl
          AddInteger( 'YearOriginal', YearOriginal.ValorEntero );
          AddInteger( 'NumeroOriginal', NumeroOriginal.ValorEntero );
          AddInteger( 'YearNuevo', YearNuevo.ValorEntero );
          AddInteger( 'NumeroNuevo', NumeroNuevo.ValorEntero );
          AddInteger( 'YearDiferencias', YearDiferencias.ValorEntero );
          AddInteger( 'NumeroDiferencias' , NumeroDiferencias.ValorEntero );
          AddString( 'Conceptos' , GetConceptos );
     end;
     //DevEx
     with Descripciones do
     begin
          AddString( 'Tipo de N�mina', ObtieneElemento( lfTipoPeriodo, TipoNomina.LlaveEntero ) );
          AddInteger( 'N�mina Original a�o', YearOriginal.ValorEntero );
          AddInteger( 'N�mina Original n�mero', NumeroOriginal.ValorEntero );
          AddInteger( 'N�mina Nueva a�o', YearNuevo.ValorEntero );
          AddInteger( 'N�mina Nueva n�mero', NumeroNuevo.ValorEntero );
          AddInteger( 'N�mina Diferencias a�o', YearDiferencias.ValorEntero );
          AddInteger( 'N�mina Diferencias n�mero' , NumeroDiferencias.ValorEntero );
          if not ( Trim(GetConceptos) = '') then
             AddString( 'Conceptos' , GetConceptos );
     end;
end;

function TWizNomCalcularDiferencias_DevEx.GetConceptos: String;
begin
     case FRangoConcepto of
          raRango: Result := ZetaClientTools.GetFiltroRango( 'M1.CO_NUMERO', Trim( EInicialConcepto.Text ), Trim( EFinalConcepto.Text ) );
          raLista: Result := ZetaClientTools.GetFiltroLista( 'M1.CO_NUMERO', Trim( EListaConcepto.Text ) );
     end;
     if StrLleno( Result ) then
        Result := Parentesis( Result );
end;

procedure TWizNomCalcularDiferencias_DevEx.EnabledBotonesConcepto( eTipo: eTipoRangoActivo );
var
   lLista, lRango: Boolean;
begin
     FRangoConcepto := eTipo;
     lRango := ( eTipo = raRango );
     lLista := ( eTipo = raLista );
     lbInicialConcepto.Enabled := lRango;
     lbFinalConcepto.Enabled := lRango;
     EInicialConcepto.Enabled := lRango;
     EFinalConcepto.Enabled := lRango;
     bInicialConcepto.Enabled := lRango;
     bFinalConcepto.Enabled := lRango;
     EListaConcepto.Enabled := lLista;
     bListaConcepto.Enabled := lLista;
end;

procedure TWizNomCalcularDiferencias_DevEx.RBTodosConceptoClick(Sender: TObject);
begin
     inherited;
     EnabledBotonesConcepto( raTodos );
end;

procedure TWizNomCalcularDiferencias_DevEx.RBRangoConceptoClick(Sender: TObject);
begin
     inherited;
     EnabledBotonesConcepto( raRango );
end;

procedure TWizNomCalcularDiferencias_DevEx.RBListaConceptoClick(Sender: TObject);
begin
     inherited;
     EnabledBotonesConcepto( raLista );
end;

procedure TWizNomCalcularDiferencias_DevEx.BInicialConceptoClick( Sender: TObject );
begin
     inherited;
     BuscaDialogo( EInicialConcepto, FALSE );
end;

procedure TWizNomCalcularDiferencias_DevEx.BFinalConceptoClick(Sender: TObject);
begin
     inherited;
     BuscaDialogo( EFinalConcepto, FALSE );
end;

procedure TWizNomCalcularDiferencias_DevEx.BListaConceptoClick(Sender: TObject);
begin
     inherited;
     BuscaDialogo( EListaConcepto, TRUE );
end;

procedure TWizNomCalcularDiferencias_DevEx.BuscaDialogo( oEdit : TZetaEdit; const lConcatena : Boolean );
 var sKey, sDescripcion : string;
begin
     sKey := oEdit.Text;
     TressShell.BuscaDialogo( enConcepto, '', sKey, sDescripcion );
     if lConcatena AND ( oEdit.Text > '' ) then
        oEdit.Text := oEdit.Text + ',' +sKey
     else oEdit.Text := sKey;
end;

function TWizNomCalcularDiferencias_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CalcularDiferencias( ParameterList );
end;

function TWizNomCalcularDiferencias_DevEx.ValidaPeriodos: Boolean;
var
   eTipo: eTipoPeriodo;
   eStatus: eStatusPeriodo;
   oCursor: TCursor;
begin
     Result := True;
     oCursor := Screen.Cursor;
     Screen.Cursor := oCursor;
     try
        if ( YearOriginal.ValorEntero = YearNuevo.ValorEntero ) and
           ( NumeroOriginal.ValorEntero = NumeroNuevo.ValorEntero ) then
        begin
             Result := Error( '� La N�mina Nueva debe ser diferente a la N�mina Original !', YearNuevo )
        end
        else
        if ( YearOriginal.ValorEntero = YearDiferencias.ValorEntero ) and
           ( NumeroOriginal.ValorEntero = NumeroDiferencias.ValorEntero ) then
        begin
             Result := Error( '� La N�mina de Diferencias debe ser diferente a la N�mina Original !', YearDiferencias );
        end
        else
        if ( YearNuevo.ValorEntero = YearDiferencias.ValorEntero ) and
           ( NumeroNuevo.ValorEntero = NumeroDiferencias.ValorEntero ) then
        begin
             Result := Error( '� La N�mina de Diferencias debe ser diferente a la N�mina Nueva !', YearDiferencias );
        end
        else
        begin
             eTipo := eTipoPeriodo( StrAsInteger( TipoNomina.Llave ) );  //acl
             if not dmCatalogos.BuscaPeriodo( YearOriginal.ValorEntero, NumeroOriginal.ValorEntero, eTipo, eStatus ) then
                Result := Error( '� El Per�odo de la N�mina Original no est� definido !', NumeroOriginal )
             else
                 if ( eStatus < spCalculadaTotal ) then
                    Result := Error( '� La N�mina Original debe estar por lo menos calculada totalmente !', NumeroOriginal )
                 else
                     if not dmCatalogos.BuscaPeriodo( YearNuevo.ValorEntero, NumeroNuevo.ValorEntero, eTipo, eStatus ) then
                        Result := Error( '� El Per�odo de la N�mina Nueva no Est� definido !', NumeroNuevo )
                     else
                         if ( eStatus < spCalculadaTotal ) then
                            Result := Error( '� La N�mina Nueva debe estar por lo menos calculada totalmente !', NumeroNuevo )
                         else
                             if not dmCatalogos.BuscaPeriodo( YearDiferencias.ValorEntero, NumeroDiferencias.ValorEntero, eTipo, eStatus ) then
                                Result := Error( '� El Per�odo de la N�mina de Diferencias no est� definido !', NumeroDiferencias )
                             else
                                 if ( eStatus >= spCalculadaTotal ) then
                                    Result := Error( '� La N�mina de Diferencias no debe estar afectada !', NumeroDiferencias );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TWizNomCalcularDiferencias_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     ParametrosControl := nil;
     inherited;
     with Wizard do
     begin
          if Adelante and EsPaginaActual( Parametros ) then
          begin
               if TipoNomina.ItemIndex > K_PERIODO_VACIO then
                  CanMove := ValidaPeriodos
               else
               begin
                    zError( Caption, 'El tipo de n�mina es invalido', 0 );
                    CanMove := False;
               end
          end
     end;
end;


procedure TWizNomCalcularDiferencias_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
          ParametrosControl := TipoNomina
     else
         ParametrosControl := nil;
     inherited;
end;


end.
