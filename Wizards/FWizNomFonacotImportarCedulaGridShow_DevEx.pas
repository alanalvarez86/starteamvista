unit FWizNomFonacotImportarCedulaGridShow_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, StdCtrls, ExtCtrls,
     ZBaseGridShow_DevEx,
     ZetaDBGrid, cxGraphics, cxLookAndFeels,
     cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxControls, cxStyles, dxSkinscxPCPainter,
     cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
     cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
     ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
     cxButtons;

type
  TWizNomFonacotImportarCedulaGridShow_DevEx = class(TBaseGridShow_DevEx)
    RENGLON: TcxGridDBColumn;
    NO_FONACOT: TcxGridDBColumn;
    RFC: TcxGridDBColumn;
    NUM_ERROR: TcxGridDBColumn;
    DESC_ERROR: TcxGridDBColumn;
    NOMBRE: TcxGridDBColumn;
    NO_CREDITO: TcxGridDBColumn;
    RETENCION_MENSUAL: TcxGridDBColumn;
    CLAVE_EMPLEADO: TcxGridDBColumn;
    PLAZO: TcxGridDBColumn;
    CUOTAS_PAGADAS: TcxGridDBColumn;
    REUBICADO: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  WizNomFonacotImportarCedulaGridShow_DevEx: TWizNomFonacotImportarCedulaGridShow_DevEx;

implementation

{$R *.DFM}

end.
