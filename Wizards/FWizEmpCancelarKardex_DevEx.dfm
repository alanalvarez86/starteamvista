inherited WizEmpCancelarKardex_DevEx: TWizEmpCancelarKardex_DevEx
  Left = 337
  Top = 90
  Caption = 'Cancelar Kardex'
  ClientHeight = 427
  ClientWidth = 441
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 441
    Height = 427
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso para cancelar los movimientos de kardex de determinado t' +
        'ipo a un grupo de empleados.'
      Header.Title = 'Cancelar Kardex'
      object dReferenciaLBL: TLabel
        Left = 24
        Top = 54
        Width = 103
        Height = 13
        Alignment = taRightJustify
        Caption = '&Fecha de Referencia:'
        FocusControl = dReferencia
        Transparent = True
      end
      object Label2: TLabel
        Left = 55
        Top = 79
        Width = 72
        Height = 13
        Alignment = taRightJustify
        Caption = '&Tipo de Fecha:'
        Transparent = True
      end
      object Label1: TLabel
        Left = 19
        Top = 105
        Width = 108
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ca&ncelar Modificaci'#243'n:'
        Transparent = True
      end
      object Label3: TLabel
        Left = 31
        Top = 131
        Width = 96
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de &Movimiento:'
        Transparent = True
      end
      object LblGenerales: TLabel
        Left = 6
        Top = 158
        Width = 121
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de Registro General:'
        Transparent = True
      end
      object dReferencia: TZetaFecha
        Left = 130
        Top = 49
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '02/ene/98'
        Valor = 35797.000000000000000000
      end
      object iTipoFecha: TZetaKeyCombo
        Left = 130
        Top = 75
        Width = 170
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 1
        ListaFija = lfTipoKarFecha
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object iCancelaMovimiento: TZetaKeyCombo
        Left = 130
        Top = 101
        Width = 170
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 2
        ListaFija = lfCancelaMod
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object sMovimiento: TZetaKeyCombo
        Left = 130
        Top = 127
        Width = 170
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        DropDownCount = 9
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 3
        OnChange = sMovimientoChange
        Items.Strings = (
          'Alta de Empleado'
          'Baja de Empleado'
          'Cambio de Salario'
          'Cambio de Turno'
          'Cambio de Puesto'
          'Renovaci'#243'n de Contrato'
          'Cambio de Area'
          'Cambio Prestaci'#243'n'
          'Cambio de Plaza'
          'Registro General')
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object RegGenerales: TZetaKeyLookup_DevEx
        Left = 130
        Top = 153
        Width = 285
        Height = 21
        Filtro = 'TB_SISTEMA='#39'N'#39
        LookupDataset = dmTablas.cdsMovKardex
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 75
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage [1]
      inherited sCondicionLBl: TLabel
        Left = 14
        Top = 137
      end
      inherited sFiltroLBL: TLabel
        Left = 39
        Top = 160
      end
      inherited Seleccionar: TcxButton
        Left = 142
        Top = 254
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 66
        Top = 134
      end
      inherited sFiltro: TcxMemo
        Left = 66
        Top = 160
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 66
        Top = 4
      end
    end
    inherited Ejecucion: TdxWizardControlPage [2]
      inherited GrupoParametros: TcxGroupBox
        Height = 192
        Width = 419
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 419
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 351
          AnchorX = 243
          AnchorY = 51
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 512
    Top = 90
  end
end
