inherited WizIMSSRecargos_DevEx: TWizIMSSRecargos_DevEx
  Caption = 'Calcular Recargos'
  ClientHeight = 399
  ClientWidth = 431
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 431
    Height = 399
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Este proceso calcula las actualizaciones y recargos cuando el pa' +
        'go de IMSS/INFONAVIT no fue presentado dentro del plazo.'
      Header.Title = 'Calcular recargos'
      object Label4: TLabel [0]
        Left = 26
        Top = 200
        Width = 91
        Height = 13
        Alignment = taRightJustify
        Caption = '&Tasa de Recargos:'
        FocusControl = ZTasa
        Transparent = True
      end
      object Label5: TLabel [1]
        Left = 3
        Top = 169
        Width = 114
        Height = 13
        Alignment = taRightJustify
        Caption = '&Factor de Actualizaci'#243'n:'
        FocusControl = ZFactor
        Transparent = True
      end
      inherited PeriodoGB: TcxGroupBox
        TabOrder = 2
        Width = 409
        inherited Label1: TLabel
          Left = 89
        end
        inherited Label2: TLabel
          Left = 88
        end
        inherited PatronLbl: TLabel
          Left = 27
        end
        inherited Label3: TLabel
          Left = 15
        end
        inherited Mes: TZetaKeyCombo
          Left = 120
        end
        inherited Patron: TZetaKeyLookup_DevEx
          Left = 120
        end
        inherited Tipo: TZetaKeyCombo
          Left = 120
        end
        inherited Anio: TZetaNumero
          Left = 120
        end
      end
      object ZTasa: TZetaNumero
        Left = 120
        Top = 196
        Width = 55
        Height = 21
        Mascara = mnTasa
        TabOrder = 1
        Text = '0.0 %'
      end
      object ZFactor: TZetaNumero
        Left = 120
        Top = 165
        Width = 55
        Height = 21
        Mascara = mnNumeroGlobal
        TabOrder = 0
        Text = '0.00'
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 164
        Width = 409
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 409
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 341
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      PageVisible = False
      inherited sFiltro: TcxMemo
        Style.IsFontAssigned = True
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    Left = 368
    Top = 1
  end
end
