unit FNomDeclaracionAnualFormulas_DevEx;

interface

{$INCLUDE DEFINES.INC}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, ComCtrls, StdCtrls, Buttons, ExtCtrls,
  {$ifndef VER130}Variants,{$endif}  
  ImgList, ActnList, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxButtons, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxControls, cxPC, cxContainer, cxEdit, cxTextEdit,
  cxMemo;

type
{$IFDEF TRESS_DELPHIXE5_UP}
  TDescripTexto = String;
{$ELSE}
  TDescripTexto = String[ 30 ];
{$ENDIF}

  TNomDeclaracionAnualFormulas_DevEx = class(TZetaDlgModal_DevEx)
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    btnExportar: TcxButton;
    btmImportar: TcxButton;
    ImageList_DevEx: TcxImageList;
    PageFormulas: TcxPageControl;
    procedure ConstruyeFormulaClick(Sender: TObject);
    procedure btnExportarClick(Sender: TObject);
    procedure btmImportarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FYearSeleccionado : integer; // validacion de Reforma fiscal 2008
    FTabOrder: integer;
    FParent : TWinControl;
    FGlobal: integer;
    procedure AgregaTitulos( const iPrimerPagina: integer );
    procedure CreaControles;
    procedure CargaDescripciones;
    procedure CreaPaginas(const iPaginas: integer; const sNombre, sCaption: string; const lCuantos: Boolean);
    procedure CreaControlTotalExento( oParent: TWinControl; const iPosicion: integer;const sDescripcion: string);
    procedure CreaControlFormula( oParent: TWinControl;const iPosicion: integer; const sDescripcion: string  );
    procedure SetBotonProperties(oBoton: TcxButton; const iPosicion,iLeft: integer; const sNombre: string);
    procedure SetLabelProperties(oLabel: TLabel; const iPosicion, iLeft, iAncho: integer;const sDescripcion: string);
    procedure SetMemoProperties(oMemo: TcxMemo; const iPosicion, iLeft: integer; const sNombre: string);
    function GetMemo( sName : string; oParent : TWinControl ) : TcxMemo;
    function GetTop(const iPosicion: integer): integer;
    procedure Exportar;
    procedure Importar;
    function NombreCampo(const iPosicion: integer): string;
    function ResultCampo(const iPosicion: integer): integer;
    function ComponenteValido(Componente: TComponent): Boolean;
    function GetDiccionDesc(const sCampo: string): string;
    procedure ValidaFechaReformaFiscal;
  protected

  public
    { Public declarations }
    property YearSeleccionado: integer read FYearSeleccionado write FYearSeleccionado; // validacion de Reforma fiscal 2008

  end;

var
  NomDeclaracionAnualFormulas_DevEx: TNomDeclaracionAnualFormulas_DevEx;


implementation
uses
    DProcesos,
    DDiccionario,
    ZGlobalTress,
    ZetaDialogo,
    ZetaCommonTools,
    ZetaCommonClasses,
    ZetaCommonLists,
    ZConstruyeFormula,
    ZetaTipoEntidad;

{$R *.DFM}

type eTipoControl = ( eExento, eFormulas ); 
const
     K_SALTO = 35;

     K_TOP_LABEL = 35;
     K_LEFT_LABEL1 = 0;
     K_LEFT_LABEL2 = 411;
     K_ANCHO_LABEL1 = 190;
     K_ANCHO_LABEL2 = 27;

     K_TOP_MEMO = 24;
     K_LEFT_MEMO1 = 192;
     K_LEFT_MEMO2 = 439;
     K_HEIGHT_MEMO = 34;
     K_ANCHO_MEMO = 185;

     K_TOP_BOTON = 25;
     K_LEFT_BOTON1 = 380;
     K_LEFT_BOTON2 = 627;
     //K_LEFT_BOTON2 = 625;
     //K_HEIGHT_BOTON = 25;
     //K_ANCHO_BOTON = 25;
     K_HEIGHT_BOTON = 26;
     K_ANCHO_BOTON = 26;

     K_NOMBRE_LABEL = 'oLabel';



  //Es char de 30, porque la descripcion va a ir a parar a Diccionario.
     aParametros : array[1..3] of TDescripTexto = (
     'Salario M�nimo',
     'A�os de Servicio',                   
     'Realiz� C�lculo Anual?'
     );

     aTotalExento: array[1..32] of TDescripTexto = (
     'Sueldos Y Salarios',                //2
     'Gratificaci�n Anual',               //3
     'Vi�ticos y Gastos de Viaje',        //4
     'Tiempo Extraordinario',             //5
     'Prima Vacacional',                  //6
     'Prima Dominical',                   //7
     'Reparto de Utilidades(PTU)',        //8
     'Reembolso Gastos M�dicos',          //9
     'Seguro Gastos Med. Mayores',        //10
     'Prima Seguro de Vida',              //11
     'Fondo de Ahorro',                   //12
     'Caja Ahorro',                       //13
     'Contribuciones Patronales',         //14
     'Premios X Puntualidad',             //15
     'Vales de Despensa',                 //16
     'Vales para Restaurante',            //17
     'Vales para Gasolina',               //18
     'Vales para Ropa',                   //19
     'Ayuda para Renta',                  //20
     'Ayuda Art�culos Escolares',         //21
     'Ayuda para Lentes',                 //22
     'Ayuda para Transporte',             //23
     'Ayuda Gastos de Funeral',           //24
     'Cuotas Sindicales',                 //25
     'Subsidios X Incapacidad',           //26
     'Becas para Trabajador',             //27
     'Pagos X Separaci�n',                //28
     'Pago Efec X Otro Patron',           //29
     'Otros Ingresos',                    //30
     'Suma Ingresos',                     //31
     'Ingreso X Prestaciones',            //32
     'Suma Ingreso(Sueldo y Salario)'     //31
      );

     aFormulas: array[1..23] of TDescripTexto = (
     'Total Sueldos y Salarios',                  //1
     'Total Pagos x Separaci�n',                  //2
     'Ingresos Exentos',                           //3/
     'Ingresos Acumulables',                      //4/
     'Ingresos No Acumulables',                   //5/
     'Ingresos Acumulable Sal. Mens.',            //6/
     'ISPT Retenido',                             //7/
     'ISPT Retenido Otro Patron',                 //8/
     'ISPT a Favor',                              //9/
     'ISPT en Contra',                            //10
     'ISPT Ult. Sueldo Mensual',                  //11
     'ISPT Pagos X Separaci�n',                   //12
     'Impuesto Tarifa Anual',                     //13
     'Imp. s/Ingreso Acumulable',                 //14
     'Imp. s/Ingreso NO Acumulable',              //15
     'Imp. Ret. Ejercicio Declara',               //16
     'ISR del Ejercicio que Declara',             //17
     'Cr�dito al Salario Aplicado',               //18
     'Cr�dito al Salario Pagado',                 //19 
     'Subsidio Acreditable',                      //20
     'Subsidio NO Acreditable',                   //21
     'Subsidio Acreditable Fracc.III',            //22
     'Subsidio Acreditable Fracc.IV'              //23
      );

      aAdicionales: array[1..10] of TDescripTexto = (
      'F�rmula Adicional  1',
      'F�rmula Adicional  2',
      'F�rmula Adicional  3',
      'F�rmula Adicional  4',
      'F�rmula Adicional  5',
      'F�rmula Adicional  6',
      'F�rmula Adicional  7',
      'F�rmula Adicional  8',
      'F�rmula Adicional  9',
      'F�rmula Adicional 10'
      );

      aGenerales : array[1..2] of TDescripTexto = (
      'Mes Inicial',
      'Mes Final' );


procedure TNomDeclaracionAnualFormulas_DevEx.FormShow(Sender: TObject);
 var
    i: integer;
    Componente : TComponent;
begin
     inherited;
     dmProcesos.GetDeclaracionAnualGlobales;
     for i:=0 to ComponentCount - 1 do
     begin
          Componente := Components[i];
          if ComponenteValido( Componente ) then
          begin
               with dmProcesos do
               begin
                    if cdsDeclaraGlobales.Locate('GL_CODIGO', Componente.Tag, [] ) then
                       (Componente as TcxMemo).Text := cdsDeclaraGlobales.FieldByName('GL_FORMULA').AsString
                    else
                        (Componente as TcxMemo).Text := VACIO;
               end;
          end;
     end;
end;

procedure TNomDeclaracionAnualFormulas_DevEx.FormClose(Sender: TObject;var Action: TCloseAction);
 var
    i: integer;
    Componente : TComponent;
begin
     inherited;
     if ModalResult = mrOk then
     begin
          with dmProcesos.cdsDeclaraGlobales do
          begin
               EmptyDataset;
               MergeChangeLog;
          end;

          for i:=0 to ComponentCount - 1 do
          begin
               Componente := Components[i];
               if ComponenteValido( Componente ) then
               begin
                    with dmProcesos.cdsDeclaraGlobales do
                    begin
                         Append;
                         FieldByName('GL_CODIGO').AsInteger := Componente.Tag;
                         FieldByName('GL_FORMULA').AsString := (Componente as TcxMemo).Text;
                         Post;
                    end;
               end;
          end;
          dmProcesos.GrabaDeclaracionAnualGlobales;
     end;
end;

//DevEx(by am): Se modifico este metodo para que se crearan los nuevos TabSheets.
procedure TNomDeclaracionAnualFormulas_DevEx.CreaPaginas( const iPaginas: integer; const sNombre, sCaption: string; const lCuantos: Boolean );
 var
    oTab : TcxTabSheet;
    i: integer;
begin
     for i:= 1 to iPaginas do
     begin
          oTab := TcxTabSheet.Create( Self );
          with oTab do
          begin
               PageControl := PageFormulas;
               Name := sNombre + IntToStr( i );
               Caption := sCaption;
               if lCuantos then
                  Caption := Caption + IntToStr( i );
          end;
     end;
end;


procedure TNomDeclaracionAnualFormulas_DevEx.AgregaTitulos( const iPrimerPagina: integer );

 var
    i: integer;
    oLabel : TLabel;

 procedure CreaLabel( const sCaption: string; const iLeft, iAncho: integer );
 begin
      with oLabel do
      begin
           Parent := PageFormulas.Pages[ i ];
           Top := 8;
           Width := iAncho;
           Height := 13;
           Left := Trunc( iLeft + ( K_ANCHO_MEMO / 2 ) - ( Width / 2 ) );
           Caption := sCaption;
           Font.Color := clNavy;
           Font.Style := [fsBold];
           ParentFont := False;
      end;
 end;

begin
     for i:= iPrimerPagina to PageFormulas.PageCount - 1 do
     begin
          oLabel := TLabel.Create( Self );
          CreaLabel( 'Gravado', K_LEFT_MEMO1, 30 );

          oLabel := TLabel.Create( Self );
          CreaLabel( 'Exento', K_LEFT_MEMO2, 40 );
     end;
end;

procedure TNomDeclaracionAnualFormulas_DevEx.CreaControles;

 procedure PoblaPageControl( Arreglo: array of TDescripTexto;
                             const eTipo: eTipoControl;
                             const sNombre, sCaption: string;
                             const lCuantos: Boolean = TRUE );
  const
       K_CONTROLES = 11;
  var
     i, iIndice, iPagina, iPaginas, iOffSet, iElementos : integer;
 begin
      iOffSet := PageFormulas.PageCount;
      iElementos := High( Arreglo ) + 1;
      iPaginas :=  iElementos Mod K_CONTROLES;
      if (iPaginas > 0) then iPaginas := 1;
      iPaginas := ( iElementos div K_CONTROLES ) + iPaginas;

      CreaPaginas( iPaginas, sNombre, sCaption, lCuantos );

      for i:= Low(Arreglo) to High(Arreglo) do
      begin
           iIndice := i + 1;
           iPagina := ( iIndice DIV K_CONTROLES );
           if ( ( iIndice MOD K_CONTROLES ) <> 0 ) then
              Inc( iPagina );

           case eTipo of
                eExento: CreaControlTotalExento( PageFormulas.Pages[ iOffSet + iPagina - 1] , iIndice  - ( (iPagina * K_CONTROLES) -  K_CONTROLES ), Arreglo[i] );
                eFormulas: CreaControlFormula( PageFormulas.Pages[ iOffSet + iPagina - 1] , iIndice - ( (iPagina * K_CONTROLES) - K_CONTROLES ), Arreglo[i] );
           end;
      end;

 end;
 var iPaginas: integer;
begin
     PoblaPageControl( aParametros, eFormulas, 'tsParametros', 'Par�metros', FALSE );
     iPaginas := PageFormulas.PageCount;
     PoblaPageControl( aTotalExento, eExento, 'tsTotalesExentos', 'Gravados/Exentos:' );
     AgregaTitulos( iPaginas );
     PoblaPageControl( aFormulas, eFormulas, 'tsFormulas', 'F�rmulas:' );
     PoblaPageControl( aAdicionales, eFormulas, 'tsAdicionales', 'Adicionales', FALSE );
     PoblaPageControl( aGenerales, eFormulas, 'tsGenerales', 'Generales', FALSE );
end;


function TNomDeclaracionAnualFormulas_DevEx.NombreCampo( const iPosicion: integer ): String;
begin
     Result := DeclaracionAnualNombreCampo( iPosicion, K_GLOBAL_DECANUAL_PRIMER_CAMPO );
end;

function TNomDeclaracionAnualFormulas_DevEx.ResultCampo( const iPosicion: integer): integer;
begin
     Result := iPosicion - K_GLOBAL_DECANUAL_PRIMER_CAMPO + 1
end;

//DevEx (by am): Metodo modificado para utilizar los memos de DevExpress.
procedure TNomDeclaracionAnualFormulas_DevEx.SetMemoProperties( oMemo: TcxMemo; const iPosicion, iLeft: integer; const sNombre: string );
begin
     with oMemo do
     begin
          Name := sNombre + IntToStr(iPosicion) + FParent.Name;
          Parent := FParent;
          Style.Font.Name := 'Courier New';
          Style.Font.Size := 8;
          Properties.ScrollBars := ssVertical;
          Properties.WordWrap := True;
          Properties.MaxLength := 255;

          ShowHint := TRUE;
          Hint := Format( 'Campo "%s" ' + CR_LF + 'Result( %d )' ,
                          [ NombreCampo( FGlobal ),
                            ResultCampo( FGlobal ) ] );
          Top := K_TOP_MEMO  + GetTop( iPosicion );
          Left := iLeft;

          Height := K_HEIGHT_MEMO;
          Width := K_ANCHO_MEMO;

          Lines.Text := VACIO;

          TabOrder := FTabOrder;
          Inc( FTabOrder );
     end;
     oMemo.Tag := FGlobal;
     Inc( FGlobal );
end;

//DevEx (by am): Metodo modificado para utilizar botones de DevExpress. Se agrego la propiedad PaintSyle y un Hint para los botones.
procedure TNomDeclaracionAnualFormulas_DevEx.SetBotonProperties( oBoton: TcxButton; const iPosicion, iLeft: integer; const sNombre: string );
const
     K_BOTON_HINT = 'Abrir constructor de f�rmulas';
begin
     with oBoton do
     begin
          Parent := FParent;

          Name := sNombre + IntToStr(iPosicion) + FParent.Name;
          Caption := VACIO;
          Top := K_TOP_BOTON +  GetTop( iPosicion );
          Left := iLeft;

          Height := K_HEIGHT_BOTON;
          Width := K_ANCHO_BOTON;

          TabOrder := FTabOrder;
          Inc( FTabOrder );

          OnClick := ConstruyeFormulaClick;
          PaintStyle := bpsGlyph;
          ImageList_DevEx.GetBitmap( 0 , Glyph );
          Hint := K_BOTON_HINT;
          ShowHint := TRUE;
     end;
end;

function TNomDeclaracionAnualFormulas_DevEx.GetTop(const iPosicion: integer): integer;
begin
     Result := ( K_SALTO * (iPosicion - 1) );
end;

procedure TNomDeclaracionAnualFormulas_DevEx.SetLabelProperties( oLabel: TLabel; const iPosicion, iLeft, iAncho: integer; const sDescripcion : string );
begin
     with oLabel do
     begin
          //Name := K_NOMBRE_LABEL + IntToStr(iPosicion) + FParent.Name;
          Parent := FParent;
          Autosize := FALSE;
          Caption := sDescripcion + Format( ' (#%2.d):', [ResultCampo( FGlobal )]);
          Top := K_TOP_LABEL + GetTop( iPosicion );
          Left := iLeft;
          Width := iAncho;
          Alignment := taRightJustify;
          Tag := iPosicion;
     end;
end;

procedure TNomDeclaracionAnualFormulas_DevEx.CreaControlFormula( oParent: TWinControl;
                                                           const iPosicion: integer;
                                                           const sDescripcion: string  );
var
    oLabel : TLabel;
    oMemo1 : TcxMemo;
    oBoton1 : TcxButton;
begin
     FParent := oParent;
     oLabel := TLabel.Create( Self );
     SetLabelProperties( oLabel, iPosicion, K_LEFT_LABEL1, K_ANCHO_LABEL1, sDescripcion );

     oMemo1 := TcxMemo.Create( Self );
     SetMemoProperties( oMemo1, iPosicion, K_LEFT_MEMO1, 'oMemo_Primero' );

     oBoton1 := TcxButton.Create( Self );
     SetBotonProperties( oBoton1, iPosicion, K_LEFT_BOTON1, 'oBoton_Primero' );
end;

procedure TNomDeclaracionAnualFormulas_DevEx.CreaControlTotalExento( oParent: TWinControl;
                                                               const iPosicion: integer;
                                                               const sDescripcion: string  );
 var
    oLabel : TLabel;
    oMemo2 : TcxMemo;
    oBoton2 : TcxButton;
begin
     CreaControlFormula( oParent, iPosicion, sDescripcion );

     oLabel := TLabel.Create( self );
     SetLabelProperties( oLabel, iPosicion, K_LEFT_LABEL2, K_ANCHO_LABEL2, VACIO );

     oMemo2 := TcxMemo.Create( self );
     SetMemoProperties( oMemo2, iPosicion, K_LEFT_MEMO2, 'oMemo_Segundo' );

     oBoton2 := TcxButton.Create( self );
     SetBotonProperties( oBoton2, iPosicion, K_LEFT_BOTON2, 'oBoton_Segundo' );
end;


function TNomDeclaracionAnualFormulas_DevEx.GetMemo( sName : string; oParent : TWinControl ) : TcxMemo;
 var
    iPos: integer;
begin
     Result := NIL;
     if ( oParent <> NIL ) then
     begin
          iPos := Pos( 'oBoton', sName );
          if  iPos > 0 then
          begin
               sName := Copy( sName, 8, 100 );
               Result := TcxMemo( oParent.FindChildControl( 'oMemo_' + sName ) );
          end;
     end;
end;

procedure TNomDeclaracionAnualFormulas_DevEx.ConstruyeFormulaClick(Sender: TObject);
 var
    oMemo: TcxMemo;
begin
     inherited;
     with TcxButton(Sender) do
          oMemo := GetMemo( Name, Parent );
     if oMemo <> NIL then
        oMemo.Lines.Text := GetFormulaConst( enEmpleado , oMemo.Lines.Text, oMemo.SelStart, evNomina )
end;

procedure TNomDeclaracionAnualFormulas_DevEx.btnExportarClick(Sender: TObject);
begin
     inherited;
     Exportar;
end;

procedure TNomDeclaracionAnualFormulas_DevEx.btmImportarClick(Sender: TObject);
begin
     inherited;
     Importar;
end;

procedure TNomDeclaracionAnualFormulas_DevEx.Exportar;
 var
    oLista : TStrings;
    i: integer;
    Componente : TComponent;
begin
     if StrVacio( SaveDialog.InitialDir ) then
        SaveDialog.InitialDir :=  ExtractFileDir( Application.ExeName );

     if StrVacio( SaveDialog.FileName ) then
        SaveDialog.FileName :=  'Declaracion.txt';

     if SaveDialog.Execute then
     begin
          oLista := TStringList.Create;
          oLista.Add( 'F�rmulas Para el Wizard de la Declaraci�n Anual' );
          try
             for i:=0 to ComponentCount - 1 do
             begin
                  Componente := Components[i];
                  if ComponenteValido( Componente ) then
                     oLista.Add( NombreCampo( Componente.Tag ) + '=' + StrTransAll( (Componente as TcxMemo).Text, CR_LF, VACIO) );
             end;
             oLista.SaveToFile( SaveDialog.FileName );
          finally
                 FreeAndNil( oLista );
                 ZInformation( Caption, 'Archivo Exportado con Exito', 0 );
          end;
     end;
end;

function TNomDeclaracionAnualFormulas_DevEx.ComponenteValido( Componente: TComponent ): Boolean;
begin
     Result := ( Componente.Tag <> 0 ) and
               ( Componente is TcxMemo );
end;

procedure TNomDeclaracionAnualFormulas_DevEx.Importar;
var
    oLista : TStrings;
    i: integer;
    Componente : TComponent;
begin
     if StrVacio( OpenDialog.InitialDir ) then
        OpenDialog.InitialDir :=  ExtractFileDir( Application.ExeName );

     if StrVacio( OpenDialog.FileName ) then
        OpenDialog.FileName :=  'Declaracion.txt';

     if OpenDialog.Execute then
     begin
          oLista := TStringList.Create;
          oLista.LoadFromFile( OpenDialog.FileName );
          //Name=Value
          try
             for i:=0 to ComponentCount - 1 do
             begin
                  Componente := Components[i];
                  if ComponenteValido( Componente ) then
                  begin
                       (Componente as TcxMemo).Text := oLista.Values[ NombreCampo( Componente.Tag ) ];
                  end;
             end;
          finally
                 FreeAndNil( oLista );
                 ZInformation( Caption, 'Archivo Importado con Exito', 0 );
          end;
     end;
end;




function TNomDeclaracionAnualFormulas_DevEx.GetDiccionDesc( const sCampo: string ): string;
 var
    sFields, sTitulo: string;

begin
     {$ifdef RDD}
     sFields := 'EN_CODIGO;AT_CAMPO';
     sTitulo := 'AT_TITULO';
     {$else}
     sFields := 'DI_CLASIFI;DI_NOMBRE';
     sTitulo := 'DI_TITULO';
     {$endif}

     {$ifdef RDD}
     with dmDiccionario.cdsCamposPorTabla do
     {$else}
     with dmDiccionario.cdsDiccion do
     {$endif}
     begin
          if Locate(sFields, VarArrayof([Ord(enDeclaraAnual),sCampo]), [] ) then
          begin
               Result := FieldByName(sTitulo).AsString;
          end;
     end;
end;

procedure TNomDeclaracionAnualFormulas_DevEx.CargaDescripciones;

 procedure Carga( var Arreglo: array of TDescripTexto; const iOffset: integer );
  var i: integer;
 begin
      for i:= Low(Arreglo) to High(Arreglo) do
      begin
           Arreglo[i] := GetDiccionDesc( NombreCampo( K_GLOBAL_DECANUAL_PRIMER_CAMPO + i + iOffset ) );
      end;
 end;
  var i, iTotal: integer;
      oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        dmDiccionario.GetListaDatosTablas( enDeclaraAnual, TRUE );

        Carga( aParametros, 0 );

        iTotal := High( aParametros );
        for i:= Low(aTotalExento) to High(aTotalExento) do
        begin
             aTotalExento[i] := GetDiccionDesc( NombreCampo( K_GLOBAL_DECANUAL_PRIMER_CAMPO + ( (i * 2) - 2 ) + iTotal ) );
        end;

        iTotal := iTotal + ( High( aTotalExento )* 2);
        Carga( aFormulas, iTotal );

        iTotal := iTotal + High(aFormulas);
        Carga( aAdicionales, iTotal );

     finally
            Screen.Cursor := oCursor;
     end;
end;

{* Cambio por reforma fiscal del 2008. Si es igual o mayor a la constante
 K_REFORMA_FISCAL_2008 se hacen cambio en cierto textos *}
procedure TNomDeclaracionAnualFormulas_DevEx.ValidaFechaReformaFiscal;
begin

     if YearSeleccionado >= ZetaCommonClasses.K_REFORMA_FISCAL_2008 then
     begin
          aFormulas[18] := 'Subsidio al Empleo Aplicado';
          aFormulas[19] := 'Subsidio al Empleo Pagado';
     end
     else
     begin
          aFormulas[18] := 'Cr�dito al Salario Aplicado';
          aFormulas[19] := 'Cr�dito al Salario Pagado';
     end;

end;

procedure TNomDeclaracionAnualFormulas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FTabOrder := 0;
     FGlobal := K_GLOBAL_DECANUAL_PRIMER_CAMPO;
     YearSeleccionado := TForm( owner ).Tag; // Validacion por Reforma Fiscal 2008
     CargaDescripciones;
     ValidaFechaReformaFiscal(); // Validacion por Reforma Fiscal 2008
     CreaControles;

     with PageFormulas do
          ActivePage := Pages[1];
end;

end.






