unit FWizSuscripcionReportes_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls, Mask,
     ZetaCommonLists, ZetaFecha, CheckLst, ZcxBaseWizard,
     cxGraphics, cxControls, ZetaClientDataSet, cxLookAndFeels,
     cxLookAndFeelPainters,
     dxSkinsCore, TressMorado2013,
     cxContainer, cxEdit, ZetaCXWizard,
     dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
     dxCustomWizardControl, dxWizardControl, Menus, cxButtons, cxRadioGroup,
     cxCheckListBox, DReportes, cxCheckGroup, dxCheckGroupBox, cxTextEdit,
     ZetaKeyCombo, ZetaNumero, ZetaHora, cxMemo, cxClasses,
     cxShellBrowserDialog, ZetaKeyLookup_DevEx, System.MaskUtils, Data.DB,
  Vcl.DBCtrls, FileCtrl, cxCheckBox, cxDBEdit, FWizReportesBaseFiltro;

type
    TWizSuscripcionReportes_DevEx = class(TWizReportesBaseFiltro)
    DataSource: TDataSource;
    Label3: TLabel;
    Usuario: TZetaDBKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
  private
    { Private declarations }
    FNombreTarea: String;
    FNombreReporte: String;
    FReporte : String;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
    property NombreTarea: String read FNombreTarea write FNombreTarea;
    property NombreReporte: String read FNombreReporte write FNombreReporte;
    property Reporte: String read FReporte write FReporte;
    procedure CargarInformacion;

  end;

var
   WizSuscripcionReportes_DevEx: TWizSuscripcionReportes_DevEx;

implementation

uses DSistema, DCliente,  ZetaCommonClasses, ZAccesosMgr, ZAccesosTress;

{$R *.DFM}

procedure TWizSuscripcionReportes_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     Parametros.PageIndex := 0;
     ResetPaginas;
     Ejecucion.PageIndex := 1;

     Usuario.Enabled := ZAccesosMgr.CheckDerecho(D_SUSCRIPCION_USUARIOS,K_DERECHO_CONSULTA);
     Usuario.LookUpDataset := dmSistema.cdsUsuarios;

     with dmSistema do
     begin
          cdsUsuarios.Conectar;
          cdsSistTareaCalendario.Conectar;
          DataSource.DataSet := cdsSistTareaCalendario;
     end;


     if dmSistema.EditarCalendario then
     begin
          Reporte := dmSistema.CodigoReporte;
          NombreReporte := dmSistema.NombreReporte;
          NombreTarea := VACIO;
          dmSistema.cdsSistTareaCalendario.Locate('CA_FOLIO', dmSistema.FolioCalendario, []);
          Usuario.Llave := '0';
          Usuario.Visible := False;
          Label3.Visible := False;
          Self.Caption := Format('Env�o programado del reporte ''%s'' ', [NombreReporte]);

          CargarInformacion;
     end
     else
     begin
           Reporte := dmSistema.CodigoReporte;
           NombreReporte := dmSistema.NombreReporte;
           NombreTarea := VACIO;
           Usuario.Llave := IntToStr(dmCliente.GetDatosUsuarioActivo.Codigo);
           Self.Caption := Format('Registro de env�o programado del reporte ''%s'' ', [NombreReporte]);
     end;
end;

procedure TWizSuscripcionReportes_DevEx.CargarInformacion;
var
   i, iTSalida, iRepEmpleado: integer;
   sLista, sDesc : String;
begin
     iTSalida := dmSistema.cdsSistTareaCalendario.FieldByName('CA_TSALIDA').AsInteger;
     iRepEmpleado := dmSistema.cdsSistTareaCalendario.FieldByName('CA_REPEMP').AsInteger;

     if cbFrecuencia.ItemIndex = ord(frSemanal) then
     begin
          sDesc := dmSistema.cdsSistTareaCalendario.FieldByName('CA_DOWS').AsString;
          MarcarCasillas( chksFrecuenciaDias, sDesc);
     end
     else if cbFrecuencia.ItemIndex = ord(frMensual) then
     begin
          if StrToInt (dmSistema.cdsSistTareaCalendario.FieldByName('CA_MES_ON').AsString) = 1 then
             rDiasMes.Checked := True
          else
              rDiasSemana.Checked := True;

          sDesc := dmSistema.cdsSistTareaCalendario.FieldByName('CA_DOWS').AsString;
          MarcarCasillas( chkMDias, sDesc);

          sDesc := dmSistema.cdsSistTareaCalendario.FieldByName('CA_MESWEEK').AsString;
          MarcarCasillas( chkMNumero, sDesc);

          sDesc := dmSistema.cdsSistTareaCalendario.FieldByName('CA_MESES').AsString;
          MarcarCasillas( chkMMeses, sDesc);

          sDesc := dmSistema.cdsSistTareaCalendario.FieldByName('CA_MESDIAS').AsString;
          MarcarCasillas( chkMDiasMes, sDesc);
     end;
end;


procedure TWizSuscripcionReportes_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if CanMove then
     begin
          if Wizard.Adelante and Wizard.EsPaginaActual(Parametros) then
          begin
               if Usuario.Llave = VACIO then
                  CanMove := Error('No hay ning�n usuario seleccionado.', Usuario)
          end;
     end;
end;

function TWizSuscripcionReportes_DevEx.EjecutarWizard: Boolean;
begin
           Result := dmSistema.AgregarTareaCalendarioReportes(ParameterList);
           dmSistema.cdsSistTareaCalendario.Refrescar;
end;

procedure TWizSuscripcionReportes_DevEx.CargaParametros;
var sNombre, sUsuario, sReporte, sFrecuencia : String;
begin
     sFrecuencia := VACIO;
     sNombre := Format('Env�o ''%s'' de ''%s''',[ZetaCommonLists.ObtieneElemento( lfEmailFrecuencia, cbFrecuencia.ItemIndex ), NombreReporte]);
     sFrecuencia := ZetaCommonLists.ObtieneElemento( lfEmailFrecuencia, cbFrecuencia.ItemIndex );
     NombreTarea := 'Env�o de '+ NombreReporte;
     sUsuario := Usuario.Llave + ' - ' + Usuario.Descripcion;
     sReporte := Reporte + ' - ' + NombreReporte;

     if dmSistema.EditarCalendario then
     begin

           with Descripciones do
           begin
                AddString('Nombre del env�o', NombreTarea);
                AddString('Descripci�n', NombreTarea);
                AddString('Empresa', dmCliente.Compania );
                AddString('Reporte', sReporte );
           end;

           with ParameterList do
           begin
                AddInteger('Folio', dmSistema.FolioCalendario);
                AddString('Nombre', NombreTarea);
                AddString('Descripcion', NombreTarea );
                AddString('Empresa', dmCliente.Compania );
                AddString('Activo', K_GLOBAL_SI );
                AddInteger('ReporteEmpleados', 0);
                AddString('Reportes', Reporte);
                AddString('SalidaArchivo', VACIO);
                AddInteger('Salida', 0);
                AddString ('Evaluar', K_GLOBAL_SI);
                AddInteger('Usuario', dmCliente.Usuario);
                AddInteger('SuscribirUsuario', 0);
                AddDate( 'FechaModifico', Date() );
           end;
     end
     else
     begin
           with Descripciones do
           begin
                AddString('Nombre del env�o', NombreTarea);
                AddString('Descripci�n', NombreTarea);
                AddString('Empresa', dmCliente.Compania );
                AddString('Reporte', sReporte );
                AddString('Usuario', sUsuario);
           end;
           with ParameterList do
           begin
                AddInteger('Folio', dmSistema.FolioCalendario);
                AddString('Nombre', NombreTarea);
                AddString('Descripcion', NombreTarea );
                AddString('Empresa', dmCliente.Compania );
                AddString('Activo', K_GLOBAL_SI );
                AddInteger('ReporteEmpleados', 0);
                AddString('Reportes', Reporte);
                AddString('SalidaArchivo', VACIO);
                AddInteger('Salida', 0);
                AddString ('Evaluar', K_GLOBAL_SI);
                AddInteger('Usuario', dmCliente.Usuario);
                AddInteger('SuscribirUsuario', StrToInt(Usuario.Llave));
                AddDate( 'FechaModifico', Date() );
           end;
     end;


     inherited CargaParametros;
end;

end.
