inherited EmpleadoGridSelectPermisoGlobal_DevEx: TEmpleadoGridSelectPermisoGlobal_DevEx
  Left = 353
  Top = 293
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Seleccione Los Empleados Deseados'
  ClientWidth = 530
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 530
    inherited OK_DevEx: TcxButton
      Left = 366
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 445
      Cancel = True
    end
  end
  inherited PanelSuperior: TPanel
    Width = 530
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 530
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.Indicator = True
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object PM_FEC_INI: TcxGridDBColumn
        Caption = 'Fecha Inicio'
        DataBinding.FieldName = 'PM_FEC_INI'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
      end
      object PM_DIAS: TcxGridDBColumn
        Caption = 'D'#237'as'
        DataBinding.FieldName = 'PM_DIAS'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
      end
      object PM_FEC_FIN: TcxGridDBColumn
        Caption = 'Regresa'
        DataBinding.FieldName = 'PM_FEC_FIN'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
        Width = 64
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
