unit FWizNomAfectar_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, ExtCtrls, Buttons,
     FWizNomBase_DevEx,
     ZetaWizard,
     ZetaDBTextBox,
     ZetaEdit,  
     cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx,
  cxButtons, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl;

type
  TWizNomAfectar_DevEx = class(TWizNomBase_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean;override;
  public
    { Public declarations }
  end;

var
  WizNomAfectar_DevEx: TWizNomAfectar_DevEx;

implementation

{$R *.DFM}

uses ZetaCommonClasses,
     DProcesos,
     FTressShell;

procedure TWizNomAfectar_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H30342_Afectar_nomina;
     //DevEx(by am): Es necesario especificar que Ejecucion sera la ultima pagina del wizard.
     Parametros.PageIndex := WizardControl.PageCount - 3;
     FiltrosCondiciones.PageIndex := WizardControl.PageCount - 2;
     Ejecucion.PageIndex := WizardControl.PageCount - 1;
end;

function TWizNomAfectar_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.AfectarNomina(ParameterList);
end;

procedure TWizNomAfectar_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption := 'Al aplicar este proceso se afectar�n los acumulados con los totales de la n�mina, y la n�mina quedar� con estatus de afectada.'; 
end;

end.
