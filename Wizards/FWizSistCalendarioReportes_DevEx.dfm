inherited WizSistCalendarioReportes_DevEx: TWizSistCalendarioReportes_DevEx
  Left = 568
  Top = 150
  Caption = 'Crear env'#237'o programado'
  ClientHeight = 558
  ClientWidth = 614
  OnClose = FormClose
  ExplicitWidth = 620
  ExplicitHeight = 586
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 614
    Height = 558
    ExplicitWidth = 614
    ExplicitHeight = 558
    inherited Parametros: TdxWizardControlPage
      Header.AssignedValues = [wchvGlyph]
      Header.Glyph.Data = {
        89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
        F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
        6000003A980000176F925FC546000000097048597300000EBC00000EBC0195BC
        7249000003CF494441545847B5963F68137114C76F70C8D0A1430707073B0876
        7013914E824B06070727117470D04D5CB248AEA2B56A6D93264DD2FB97CBE572
        97FBD3186A5B8B58A8884B2705B577AD60411011840E0A452AC6F7E25DFCE5EE
        97988BF5C187DF71F77BDFF7EEDDFBFD7EC7A0B19A3BC4569DC3FB86F5E66053
        B817634D672C69BA7BACE536F693A4E54E7A21BA5BD272765B4E9008386E9342
        FD825A5E88EED672C0446A5B87F05ED2DC9C20C5FAA519E06FE64F4E9ACE8A77
        8B618D8D93A450BF7872DDCD9FDCEC03D38D6343266DB74A0AF58B17A2BB41E0
        4F34E77F057477BC10DD8DB59C4B507E079B6FDF30DD77B0BAAE7821FECD1A8D
        C67F8561E5ED186BB867B11291B19DE35E9EFD5BD2DAACD3BE61377E37AC33C6
        AEAD1D78707392F11172A5F5ECFDDC77B86EA4C6D33FE7A6B92F1541BF4ECE09
        D25A05BDE2053F17145AB49731994610295B5C87B16D2E49E40460A95EA00969
        821E87319400546457961E0EC175C8078994007478019DCAC5DA113E2D7C5079
        3D6194EC13425E7D0AF743C17DD213991FF029129A581D56B9CA1DA55056E17E
        B4047069DD365E0E88423DC6A7B8CF64807EA870EA0D18B109DDD7B480210CF7
        1A3A94382D4D0AF5CBD4ADA9862E19C30C2E252C2DBCA1DC0DB6BE3D98139FC6
        66BD2EA7313755D8A9F05AC290AA71E8898B50EA85A95BD3D4B948794EE5BCC5
        D89BF1F2F2199A1022CF16D7E1FBC6E0BAADC974B13A9A9998F9817382E4EECF
        EE32F866B86D36D775276CE71426004E8CCA6B77A6A17CA450F65E76CF281A1D
        3BBD2A55AFC2D8EE7337B307F7E37018392BD46F4EE2EDEB2886C879E51929A6
        E44B0BFE331A589960D255C9C06AF6B80A60ED9309701969CB174274414BF8CF
        3A917F50F80663CBC792CDD378BFB7048C8DCB640262B6F8C21742A0998AFE33
        1AB3E26A2CD88CA66C454800FAC04F40158D44B09C1CEC0B8BE662DBB9405216
        CDF330B6F9881969C3D41E0DF69400FC2FAA9800272F86847C3A5521AFAE1E84
        A5BB0BD7219FB914FF11FF885ED282928C996FBFCE48CF63BAF678303D9EFA49
        1343E0ADB654C93AA3E92B838A5C3F221417AEA6EF66F7687311A5A0180C3BEF
        1E4DDA9BA9E0C643326EBC9245F5C9517062A0E36B41A17E9957EC11AC6CCF06
        4EB0A68D21DC4082625181B3A079204532744024B116CF4EF33B65AE928684CE
        E316EC0BD3C0A62DE5959A2EEAD7E127E5A33023BE5FB6979A4D1BC9D081C612
        FC8C940B0A366128787E32F7CD56E647E0BA351FCE8BD6961DC948912070001D
        833194809C6BF60CD507896434019F3A9414F6F7D0A1A3F195E6B9DF89484613
        08A24AF6107CEB51AB648DD8256B8036E70F93CC2FADD62E283A1FEEC1000000
        0049454E44AE426082}
      Header.Description = 
        'Indicar los par'#225'metros generales de la solicitud de env'#237'os para ' +
        'generar reportes.'
      Header.Title = 'Registro de administrador de env'#237'os programados'
      ParentFont = False
      ExplicitWidth = 592
      ExplicitHeight = 418
      inherited Label5: TLabel
        Left = 118
        Top = 372
        ExplicitLeft = 118
        ExplicitTop = 372
      end
      object Label1: TLabel [1]
        Left = 86
        Top = 25
        Width = 88
        Height = 13
        Caption = '&Nombre del env'#237'o:'
        FocusControl = NombreTarea
      end
      object Label2: TLabel [2]
        Left = 115
        Top = 55
        Width = 59
        Height = 13
        Caption = '&Descripci'#243'n:'
        FocusControl = DescripcionTarea
      end
      object Label3: TLabel [3]
        Left = 130
        Top = 106
        Width = 44
        Height = 13
        Caption = 'E&mpresa:'
        FocusControl = Empresa
      end
      object Label25: TLabel [4]
        Left = 133
        Top = 133
        Width = 41
        Height = 13
        Caption = '&Reporte:'
        FocusControl = Reporte
      end
      inherited cbFrecuencia: TZetaDBKeyCombo
        Left = 180
        Top = 368
        TabOrder = 8
        DataField = 'CA_FREC'
        DataSource = DataSource
        ExplicitLeft = 180
        ExplicitTop = 368
      end
      object CA_FOLIO: TDBEdit
        Left = 180
        Top = 22
        Width = 117
        Height = 21
        DataField = 'CA_FOLIO'
        DataSource = DataSource
        Enabled = False
        TabOrder = 9
        Visible = False
      end
      object rgListaEmpleados: TGroupBox
        Left = 180
        Top = 246
        Width = 317
        Height = 44
        Enabled = False
        TabOrder = 6
        object lListaEmpleados: TLabel
          Left = 5
          Top = 13
          Width = 41
          Height = 13
          Caption = 'Reporte:'
          Enabled = False
        end
        object ReporteLista: TZetaDBKeyLookup_DevEx
          Left = 52
          Top = 10
          Width = 262
          Height = 21
          Enabled = False
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
          DataField = 'CA_REPEMP'
          DataSource = DataSource
        end
      end
      object NombreTarea: TDBEdit
        Left = 180
        Top = 22
        Width = 255
        Height = 21
        DataField = 'CA_NOMBRE'
        DataSource = DataSource
        TabOrder = 0
      end
      object chkGuardarCopia: TdxCheckGroupBox
        Left = 180
        Top = 296
        Caption = '&Guardar copia de reportes en disco'
        CheckBox.Checked = False
        TabOrder = 7
        Height = 66
        Width = 317
        object Label8: TLabel
          Left = 8
          Top = 24
          Width = 48
          Height = 13
          Caption = 'Directorio:'
        end
        object ArchivoSeek: TcxButton
          Left = 290
          Top = 21
          Width = 21
          Height = 21
          Hint = 'Buscar directorio'
          OptionsImage.Glyph.Data = {
            DA050000424DDA05000000000000360000002800000013000000130000000100
            200000000000A40500000000000000000000000000000000000000B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFDFF5
            FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
            FCFFDFF5FCFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF10B7EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF40C5EFFF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF40C5EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6CD3
            F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF6CD3F2FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFF93DEF6FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF97E0F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFEBF9FDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF54CB
            F0FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CF
            F1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF80D9F4FF80D9
            F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC3EDFAFFDFF5FCFFDFF5FCFFDFF5
            FCFF70D4F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          TabStop = False
          OnClick = ArchivoSeekClick
        end
        object Archivo: TEdit
          Left = 62
          Top = 21
          Width = 222
          Height = 21
          TabOrder = 0
        end
      end
      object DescripcionTarea: TDBMemo
        Left = 180
        Top = 49
        Width = 317
        Height = 51
        DataField = 'CA_DESCRIP'
        DataSource = DataSource
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 2
      end
      object Empresa: TZetaDBKeyLookup_DevEx
        Left = 180
        Top = 106
        Width = 319
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        OnValidKey = EmpresaValidKey
        DataField = 'CM_CODIGO'
        DataSource = DataSource
      end
      object chkActivo: TcxDBCheckBox
        Left = 444
        Top = 24
        Caption = 'Acti&va'
        DataBinding.DataField = 'CA_ACTIVO'
        DataBinding.DataSource = DataSource
        ParentBackground = False
        ParentColor = False
        Properties.ValueChecked = 'S'
        Properties.ValueUnchecked = 'N'
        Style.Color = clWhite
        TabOrder = 1
        Width = 58
      end
      object Reporte: TZetaDBKeyLookup_DevEx
        Left = 180
        Top = 133
        Width = 319
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        DataField = 'CA_REPORT'
        DataSource = DataSource
      end
      object cxGroupBox2: TcxGroupBox
        Left = 180
        Top = 157
        TabStop = True
        Caption = 'Tipo de env'#237'o: '
        TabOrder = 5
        Height = 79
        Width = 317
        object rdListaEmpleados: TcxRadioButton
          Left = 20
          Top = 40
          Width = 197
          Height = 17
          Caption = 'Enviar a lista de &empleados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = rdListaEmpleadosClick
          Transparent = True
        end
        object rdRolesUsuario: TcxRadioButton
          Left = 20
          Top = 20
          Width = 234
          Height = 17
          Caption = 'Enviar a r&oles y/o usuarios'
          Checked = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          TabStop = True
          OnClick = rdRolesUsuarioClick
          Transparent = True
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      Header.AssignedValues = [wchvDescriptionFont, wchvGlyph]
      Header.Glyph.Data = {
        89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
        F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
        6000003A980000176F925FC546000000097048597300000EBC00000EBC0195BC
        7249000003CF494441545847B5963F68137114C76F70C8D0A1430707073B0876
        7013914E824B06070727117470D04D5CB248AEA2B56A6D93264DD2FB97CBE572
        97FBD3186A5B8B58A8884B2705B577AD60411011840E0A452AC6F7E25DFCE5EE
        97988BF5C187DF71F77BDFF7EEDDFBFD7EC7A0B19A3BC4569DC3FB86F5E66053
        B817634D672C69BA7BACE536F693A4E54E7A21BA5BD272765B4E9008386E9342
        FD825A5E88EED672C0446A5B87F05ED2DC9C20C5FAA519E06FE64F4E9ACE8A77
        8B618D8D93A450BF7872DDCD9FDCEC03D38D6343266DB74A0AF58B17A2BB41E0
        4F34E77F057477BC10DD8DB59C4B507E079B6FDF30DD77B0BAAE7821FECD1A8D
        C67F8561E5ED186BB867B11291B19DE35E9EFD5BD2DAACD3BE61377E37AC33C6
        AEAD1D78707392F11172A5F5ECFDDC77B86EA4C6D33FE7A6B92F1541BF4ECE09
        D25A05BDE2053F17145AB49731994610295B5C87B16D2E49E40460A95EA00969
        821E87319400546457961E0EC175C8078994007478019DCAC5DA113E2D7C5079
        3D6194EC13425E7D0AF743C17DD213991FF029129A581D56B9CA1DA55056E17E
        B4047069DD365E0E88423DC6A7B8CF64807EA870EA0D18B109DDD7B480210CF7
        1A3A94382D4D0AF5CBD4ADA9862E19C30C2E252C2DBCA1DC0DB6BE3D98139FC6
        66BD2EA7313755D8A9F05AC290AA71E8898B50EA85A95BD3D4B948794EE5BCC5
        D89BF1F2F2199A1022CF16D7E1FBC6E0BAADC974B13A9A9998F9817382E4EECF
        EE32F866B86D36D775276CE71426004E8CCA6B77A6A17CA450F65E76CF281A1D
        3BBD2A55AFC2D8EE7337B307F7E37018392BD46F4EE2EDEB2886C879E51929A6
        E44B0BFE331A589960D255C9C06AF6B80A60ED9309701969CB174274414BF8CF
        3A917F50F80663CBC792CDD378BFB7048C8DCB640262B6F8C21742A0998AFE33
        1AB3E26A2CD88CA66C454800FAC04F40158D44B09C1CEC0B8BE662DBB9405216
        CDF330B6F9881969C3D41E0DF69400FC2FAA9800272F86847C3A5521AFAE1E84
        A5BB0BD7219FB914FF11FF885ED282928C996FBFCE48CF63BAF678303D9EFA49
        1343E0ADB654C93AA3E92B838A5C3F221417AEA6EF66F7687311A5A0180C3BEF
        1E4DDA9BA9E0C643326EBC9245F5C9517062A0E36B41A17E9957EC11AC6CCF06
        4EB0A68D21DC4082625181B3A079204532744024B116CF4EF33B65AE928684CE
        E316EC0BD3C0A62DE5959A2EEAD7E127E5A33023BE5FB6979A4D1BC9D081C612
        FC8C940B0A366128787E32F7CD56E647E0BA351FCE8BD6961DC948912070001D
        833194809C6BF60CD507896434019F3A9414F6F7D0A1A3F195E6B9DF89484613
        08A24AF6107CEB51AB648DD8256B8036E70F93CC2FADD62E283A1FEEC1000000
        0049454E44AE426082}
      Header.Description = 
        'Al aplicar el registro se agregar'#225' el env'#237'o al administrador de ' +
        'env'#237'os programados.'
      Header.Title = 'Presione aplicar para iniciar el proceso'
      ParentFont = False
      ExplicitWidth = 592
      ExplicitHeight = 418
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 592
        ExplicitHeight = 323
        Height = 323
        Width = 592
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 592
        Width = 592
        inherited Advertencia: TcxLabel
          Caption = 
            'El env'#237'o de reporte ser'#225#160'registrado y programado seg'#250'n la config' +
            'uraci'#243'n de par'#225'metros indicados en los pasos anteriores.'
          Style.IsFontAssigned = True
          ExplicitWidth = 524
          Width = 524
          AnchorY = 57
        end
      end
    end
    inherited pDiario: TdxWizardControlPage
      Header.Title = 'Env'#237'o con frecuencia diaria'
      ParentFont = False
      ExplicitWidth = 592
      ExplicitHeight = 418
      inherited Label13: TLabel
        Left = 165
        Width = 123
        Caption = '&Fecha de inicio del env'#237'o:'
        ExplicitLeft = 165
        ExplicitWidth = 123
      end
      inherited fdFechaInicio: TZetaDBFecha
        DataSource = DataSource
      end
      inherited fdHora: TZetaDBHora
        DataSource = DataSource
      end
      inherited fdRepeticion: TZetaDBNumero
        DataSource = DataSource
      end
    end
    inherited pMensual: TdxWizardControlPage
      Header.Title = 'Env'#237'o con frecuencia mensual'
      ParentFont = False
      ExplicitWidth = 592
      ExplicitHeight = 418
      inherited fmFechaInicio: TZetaDBFecha
        DataSource = DataSource
      end
      inherited fmHora: TZetaDBHora
        DataSource = DataSource
      end
      inherited rDiasSemana: TcxRadioButton
        Left = 46
        ExplicitLeft = 46
      end
    end
    inherited pSemanal: TdxWizardControlPage
      Header.Title = 'Env'#237'o con frecuencia semanal'
      ParentFont = False
      ExplicitWidth = 592
      ExplicitHeight = 418
      inherited fsFechaInicio: TZetaDBFecha
        DataSource = DataSource
      end
      inherited fsHora: TZetaDBHora
        DataSource = DataSource
      end
      inherited fsRepeticion: TZetaDBNumero
        DataSource = DataSource
      end
    end
    inherited pHora: TdxWizardControlPage
      Header.Title = 'Env'#237'o con frecuencia por hora'
      ExplicitWidth = 592
      ExplicitHeight = 418
      inherited fhFechaInicio: TZetaDBFecha
        DataSource = DataSource
      end
      inherited fhHora: TZetaDBHora
        DataSource = DataSource
      end
      inherited fhRepeticion: TZetaDBNumero
        DataSource = DataSource
      end
    end
    inherited pEspecial: TdxWizardControlPage
      Header.Title = 'Env'#237'o con frecuencia especial'
      ExplicitWidth = 592
      ExplicitHeight = 418
      inherited feFechaInicio: TZetaDBFecha
        DataSource = DataSource
      end
      inherited feHora: TZetaDBHora
        DataSource = DataSource
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    Left = 72
    Top = 509
  end
  object DataSource: TDataSource
    Left = 19
    Top = 507
  end
  object OpenDialog1: TOpenDialog
    Filter = 
      'Archivos de Datos (*.dat)|*.dat|Archivos de Texto (*.txt)|*.txt|' +
      'Todos (*.*)|*.*'
    Options = [ofHideReadOnly]
    Title = 'Seleccione el folder'
    Left = 130
    Top = 506
  end
  object OpenDialog: TcxShellBrowserDialog
    FolderLabelCaption = 'Registro de Calendario de Reportes'
    Left = 192
    Top = 512
  end
end
