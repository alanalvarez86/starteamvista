unit FWizEmpCancelarProgCursoGlobal_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FWizEmpBaseFiltro, ComCtrls, ZetaEdit, StdCtrls,
  Buttons, ZetaWizard, ExtCtrls, Mask, ZetaFecha, 
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
  cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl;

type
  TWizEmpCancelarProgCursoGlobal_DevEx = class(TWizEmpBaseFiltro)
    dFecha: TZetaFecha;
    CodigoCurso: TZetaKeyLookup_DevEx;
    Label2: TLabel;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject); //DevEx(by am): Agregado para enfocar correctamente los controles.
    procedure SetEditarSoloActivos;
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    { Protected declarations}
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;    
  end;

var
  WizEmpCancelarProgCursoGlobal_DevEx: TWizEmpCancelarProgCursoGlobal_DevEx;

implementation

uses ZetaCommonLists,
     ZetaCommonClasses,
     ZetaCommonTools,
     DCliente,
     DCatalogos,
     DProcesos,
     DGlobal,
     ZGlobalTress,
     ZBaseSelectGrid_DevEx,
     FEmpleadoGridSelect_DevEx;

{$R *.dfm}

procedure TWizEmpCancelarProgCursoGlobal_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ParametrosControl := CodigoCurso;
     HelpContext := H_EMP_CANC_CURSO_PROG_GLOBAL;
     dFecha.Valor := dmCliente.FechaDefault;
     CodigoCurso.LookupDataSet := dmCatalogos.cdsCursos;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TWizEmpCancelarProgCursoGlobal_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     dmCatalogos.cdsCursos.Conectar;
     Advertencia.Caption :='Al aplicar el proceso se eliminar�n los registros de los cursos programados que se realizaron globalmente.';
end;

function TWizEmpCancelarProgCursoGlobal_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpleadoGridSelect_DevEx );
end;

procedure TWizEmpCancelarProgCursoGlobal_DevEx.CargaListaVerificacion;
begin
  inherited;
     dmProcesos.CancelarCursoProgGlobalGetLista( ParameterList );
end;

procedure TWizEmpCancelarProgCursoGlobal_DevEx.CargaParametros;
begin
  //inherited;
  inherited CargaParametros;
     with Descripciones do
     begin
          AddString( 'C�digo Curso', GetDescripLlave( CodigoCurso ) );
          AddDate( 'Inicio', dFecha.Valor );
     end;
     with ParameterList do
     begin
          AddString( 'CodigoCurso', CodigoCurso.Llave );
          AddDate( 'dFecha', dFecha.Valor );
     end;
end;

function TWizEmpCancelarProgCursoGlobal_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CancelarCursoProgGlobal( ParameterList, Verificacion );
end;

procedure TWizEmpCancelarProgCursoGlobal_DevEx.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
Const
K_CODIGO_INVALIDO = 'C�digo Inactivo!!!';
begin
     ParametrosControl := nil;
     if Wizard.Adelante then
     begin
          if ( wizard.EsPaginaActual( Parametros ) ) then
          begin
               if StrVacio( CodigoCurso.Llave ) then
                 CanMove := Error( '� No se ha Especificado Curso !', CodigoCurso );
                //Validaci�n de catalogos inactivos   @DACP
               if  Global.GetGlobalBooleano( K_GLOBAL_USAR_VALIDACION_STATUS_TABLAS ) then
               begin
                    if CodigoCurso.Descripcion = K_CODIGO_INVALIDO then
                       CanMove := FALSE;
               end;
          end;
     end;
     inherited;
end;

procedure TWizEmpCancelarProgCursoGlobal_DevEx.WizardAfterMove(Sender: TObject);
begin
     // Para Enfocar el primer control dependiendo del tipo de proceso
     if Wizard.EsPaginaActual( Parametros ) then
        ParametrosControl := CodigoCurso;
     inherited;
end;

procedure TWizEmpCancelarProgCursoGlobal_DevEx.SetEditarSoloActivos;
begin
     CodigoCurso.EditarSoloActivos := TRUE;
end;

end.
