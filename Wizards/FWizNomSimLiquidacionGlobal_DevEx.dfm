inherited LiquidacionGlobal_DevEx: TLiquidacionGlobal_DevEx
  Left = 484
  Top = 246
  Caption = 'Liquidaci'#243'n Global de Finiquitos'
  ClientHeight = 441
  ClientWidth = 458
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 458
    Height = 441
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Liquidaci'#243'n global de finiquitos detecta a los empleados que fue' +
        'ron dados de baja en la n'#243'mina activa y c'#225'lcula la liquidaci'#243'n d' +
        'e cada uno de ellos.'
      Header.Title = 'Liquidaci'#243'n Global de Finiquitos'
      object Label1: TLabel
        Left = 11
        Top = 77
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'Observaciones:'
        Transparent = True
      end
      object GroupBox1: TcxGroupBox
        Left = 0
        Top = 100
        Caption = ' N'#243'mina Activa '
        TabOrder = 0
        Visible = False
        Height = 183
        Width = 431
        object iNumeroNomina: TZetaTextBox
          Left = 65
          Top = 28
          Width = 65
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object iMesNomina: TZetaTextBox
          Left = 65
          Top = 52
          Width = 65
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object sDescripcion: TZetaTextBox
          Left = 139
          Top = 28
          Width = 255
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object FechaInicial: TZetaTextBox
          Left = 65
          Top = 76
          Width = 200
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object FechaFinal: TZetaTextBox
          Left = 65
          Top = 100
          Width = 200
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object sStatusNomina: TZetaTextBox
          Left = 65
          Top = 124
          Width = 200
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object Label4: TLabel
          Left = 27
          Top = 126
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Status:'
          Transparent = True
        end
        object Label2: TLabel
          Left = 48
          Top = 102
          Width = 12
          Height = 13
          Alignment = taRightJustify
          Caption = 'Al:'
          Transparent = True
        end
        object Label3: TLabel
          Left = 37
          Top = 54
          Width = 23
          Height = 13
          Alignment = taRightJustify
          Caption = 'Mes:'
          Transparent = True
        end
        object PeriodoNumeroLBL: TLabel
          Left = 20
          Top = 30
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
          Transparent = True
        end
        object Label5: TLabel
          Left = 41
          Top = 78
          Width = 19
          Height = 13
          Alignment = taRightJustify
          Caption = 'Del:'
          Transparent = True
        end
      end
      object GRTipo: TcxRadioGroup
        Left = 0
        Top = 0
        Align = alTop
        Caption = 'Tipo'
        Properties.Columns = 2
        Properties.Items = <
          item
            Caption = 'Liquidaci'#243'n'
          end
          item
            Caption = 'Indemnizaci'#243'n'
          end>
        ItemIndex = 0
        TabOrder = 1
        OnClick = GRTipoClick
        Height = 65
        Width = 436
      end
      object Observaciones: TZetaEdit
        Left = 91
        Top = 73
        Width = 321
        Height = 21
        TabOrder = 2
        Text = 'Observaciones'
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 204
        Width = 436
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 436
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 364
          AnchorX = 252
          AnchorY = 49
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 22
      end
      inherited sFiltroLBL: TLabel
        Left = 47
      end
      inherited Seleccionar: TcxButton
        Left = 150
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 74
      end
      inherited sFiltro: TcxMemo
        Left = 74
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 74
      end
    end
    object SimulacionesPrevias: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 'Seleccione los par'#225'metros para simulaciones previas.'
      Header.Title = 'Simulaciones previas'
      object GrSimAprobadas: TcxRadioGroup
        Left = 0
        Top = 0
        Align = alTop
        Caption = ' Si ya existen simulaciones aprobadas en el '
        Properties.Items = <
          item
            Caption = 'Dejar la Anterior'
            Value = 'No'
          end
          item
            Caption = 'Encimar Nueva'
            Value = 'S'#237
          end>
        ItemIndex = 0
        TabOrder = 0
        Height = 82
        Width = 436
      end
      object GrSimAplicadas: TcxRadioGroup
        Left = 0
        Top = 82
        Align = alTop
        Caption = ' Si ya existen simulaciones aplicadas en el '
        Properties.Items = <
          item
            Caption = 'Dejar la Anterior'
          end
          item
            Caption = 'Encimar Nueva'
          end>
        ItemIndex = 0
        TabOrder = 1
        Height = 80
        Width = 436
      end
      object Panel1: TPanel
        Left = 0
        Top = 162
        Width = 436
        Height = 137
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 2
        object Label6: TLabel
          Left = 87
          Top = 32
          Width = 105
          Height = 13
          Caption = 'Fecha de Liquidaci'#243'n:'
        end
        object FechaLiquidacion: TZetaFecha
          Left = 194
          Top = 27
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '12/mar/09'
          Valor = 39884.000000000000000000
        end
        object IncluirBajas: TcxCheckBox
          Left = 80
          Top = 56
          Caption = 'Incluir Bajas desde: '
          Properties.Alignment = taLeftJustify
          TabOrder = 1
          Transparent = True
          OnClick = IncluirBajasClick
          Width = 115
        end
        object FechaBaja: TZetaFecha
          Left = 194
          Top = 53
          Width = 115
          Height = 22
          Cursor = crArrow
          Enabled = False
          TabOrder = 2
          Text = '12/mar/09'
          Valor = 39884.000000000000000000
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    Top = 301
  end
end
