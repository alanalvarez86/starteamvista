inherited WizAsistIntercambioFestivo_DevEx: TWizAsistIntercambioFestivo_DevEx
  Left = 645
  Top = 230
  Caption = 'Intercambio de festivos'
  ClientHeight = 423
  ClientWidth = 428
  ExplicitWidth = 434
  ExplicitHeight = 452
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 428
    Height = 423
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EBD00000EBD0147FB
      90AD0000040C494441545847C557494C144114E56094834671815150D091805B
      C025D1831A0F1E3C70F0608C070F9298E8C1188D0AA30E16030466BA67C47D89
      07BD7920D1840B070E1A4D34C41817446318511C05590C21A88808ED7F65D558
      74578F4B067CC9CB7455FFFAAFEAFFAADF35299665FD576A3B279249817579F5
      18B6FAD2B3DFB3B9FEDEEA9C8681485EAC2FE46DEA0864996D4767AFB5DB2605
      AAC3F7E59981918B2B47E9D9D2B1AB2ABBFEDEFE8C547A4EFE043E06BD4D1001
      074F2FFBF2B166E1EDCE40A64991A81F38B9A457BEFB7432BF3F5A32230D6392
      02387A579E1580F3D14BAB2C12BCF174DFE4F82A25DF96794A86CF178CC08E6C
      3059E1E137608CA51ACCD84C3C1C0998574D16F29BCCDC847EBC6F2EF164CBB0
      F7542FBC01C76F8EA71751CEA7E15965FBF13945F4CB23F1D69F5ECC051221C2
      82EBCC80F1221C302D0799E1834D8C65FAE110617FB26F0A5F39ADB0F1CBA9A5
      FDD1D299F968AB14ABE751E0226EA095EE25A16187B0240B6D855D77B5B7010E
      917355A8AB72FEB5EF170A47DAFC9EED6A7FAC2CE320FDF2BDC0857408B3703E
      890C3A4455920D6CFB234B6370D8513ECF5485C0767FC6CE914BABADE7A59E74
      D9D7E69B89E3C8D3C0C574A0B0DFD58AAA64E1F5B0ED09E6CA90D64B111BADA8
      6F8E57B62902C5E8FB7A66F91017B3A396D5CED00ADA489B7227ECA315795839
      42DA25456CB45A7C59F10974572DC046B5A840B570413BB0E375824E1A75B0BF
      5BB6311ED2F61373F7482185D663DF623E013AFF5E7962A85A9A5CD00EDADDBB
      F4820E0ED2643D70DC5593CB37E2B7F385DF5FFA7370D4E21378E95FC4DBAD47
      A6675369EE801DFDF63EDB9F3A49488E458885D668C45C68D4C179C3A182D4CF
      74ECE01CC5A8BBC6DBD87E22B338EA9BE5A58D584415F18A2C427432465F95A6
      ADC0382D5060C8B9FBF1B391A27000CEEE1CDB90D61DCA7B081137D23EE995E2
      A02B50F174626E44CD904E9F94AF29FE10CC6B1AA85DC623327476C550BF99DB
      82EF02C22EED40579C63E7A692E3D77621170E236D6268F220F642C24950BD18
      A0EFC2363124F9E091A830CE92509F4D7CD80C9837C765E56E88B0C862AC96D7
      09169E2DBAC70F2454088AA62BF05504514145D7BF838EE0241C2B0A79CFCF50
      1BD7C5AB94200B66E11E20EE02F12242B6ADB0459A3056DE13FE1AC837E5F5BE
      926730265EF314509BBE90C62DD145DF8D9A1C9B3DD16886AD30F933604570EC
      74665A10116629E18A70252E29A229EF0C8E31C44E444C98258610AFD338E1A4
      90EF10A624686E53739D681CF1819A2A57D0CD66B766F02F56842F0A5312341B
      29CF87F10CE79AE33996F461E303DD800D43869D8E8163F908B64ABE5F431CA9
      506CB4C406E5426EF8EDEA0569D55B1009A57D406D27A4B8BA694139BC4E4628
      B7E346750F39A0FBC33891D4764E1CAD941F494657DDDCD9B26B000000004945
      4E44AE426082}
    ExplicitWidth = 428
    ExplicitHeight = 423
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso para intercambio de D'#237'as Festivos en el calendario del e' +
        'mpleado.'
      Header.Title = 'Intercambiar Dias Festivos'
      ExplicitWidth = 406
      ExplicitHeight = 283
      object GroupBox1: TGroupBox
        Left = 118
        Top = 75
        Width = 185
        Height = 73
        Caption = ' Fechas Intercambio '
        TabOrder = 0
        object Label1: TLabel
          Left = 14
          Top = 24
          Width = 38
          Height = 13
          Alignment = taRightJustify
          Caption = '&Original:'
        end
        object Label2: TLabel
          Left = 17
          Top = 48
          Width = 35
          Height = 13
          Alignment = taRightJustify
          Caption = '&Nueva:'
          FocusControl = FechaNueva
        end
        object FechaNueva: TZetaFecha
          Left = 54
          Top = 44
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 1
          Text = '19/Jan/98'
          Valor = 35814.000000000000000000
        end
        object FechaOriginal: TZetaFecha
          Left = 54
          Top = 20
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '19/Jan/98'
          Valor = 35814.000000000000000000
        end
      end
      object ckFestivoTrabajado: TCheckBox
        Left = 118
        Top = 163
        Width = 194
        Height = 17
        Caption = 'Intercambiar horarios de las tarjetas'
        Checked = True
        State = cbChecked
        TabOrder = 1
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      ExplicitWidth = 406
      ExplicitHeight = 283
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 406
        ExplicitHeight = 188
        Height = 188
        Width = 406
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 406
        Width = 406
        inherited Advertencia: TcxLabel
          Caption = 
            'Se registrar'#225' el intercambio de festivo para las fechas indicada' +
            's a los empleados seleccionados.'
          Style.IsFontAssigned = True
          ExplicitLeft = 67
          ExplicitTop = 20
          ExplicitWidth = 338
          ExplicitHeight = 74
          Width = 338
          AnchorY = 57
        end
        inherited cxImage1: TcxImage
          ExplicitLeft = 1
          ExplicitTop = 20
          ExplicitHeight = 74
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      ExplicitWidth = 406
      ExplicitHeight = 283
      inherited sCondicionLBl: TLabel
        Left = 4
        Top = 137
        ExplicitLeft = 4
        ExplicitTop = 137
      end
      inherited sFiltroLBL: TLabel
        Left = 37
        Top = 162
        ExplicitLeft = 37
        ExplicitTop = 162
      end
      inherited Seleccionar: TcxButton
        Left = 140
        Top = 256
        Visible = True
        ExplicitLeft = 140
        ExplicitTop = 256
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 64
        Top = 136
        ExplicitLeft = 64
        ExplicitTop = 136
      end
      inherited sFiltro: TcxMemo
        Left = 64
        Top = 162
        Style.IsFontAssigned = True
        ExplicitLeft = 64
        ExplicitTop = 162
      end
      inherited GBRango: TGroupBox
        Left = 64
        Top = 6
        ExplicitLeft = 64
        ExplicitTop = 6
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
  end
end
