inherited EmpBiometricoGridSelect_DevEx: TEmpBiometricoGridSelect_DevEx
  Width = 548
  Height = 322
  Caption = 'Lista de empleados a procesar'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 248
    Width = 532
    inherited OK_DevEx: TcxButton
      Left = 368
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 446
    end
  end
  inherited PanelSuperior: TPanel
    Width = 532
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 532
    Height = 213
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
        Width = 65
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        Width = 200
      end
      object CB_ID_BIO: TcxGridDBColumn
        Caption = 'N'#250'mero biom'#233'trico'
        DataBinding.FieldName = 'CB_ID_BIO'
        Width = 100
      end
      object CB_GP_COD: TcxGridDBColumn
        Caption = 'Grupo de terminales'
        DataBinding.FieldName = 'CB_GP_COD'
        Width = 120
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
