inherited WizEmpReiniciarCapacitacionesSTPS_DevEx: TWizEmpReiniciarCapacitacionesSTPS_DevEx
  Left = 522
  Top = 152
  Caption = 'Reiniciar Capacitaciones de STPS'
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        ' Proceso que permite reiniciar el folio de capacitaciones de la ' +
        'Secretar'#237'a del Trabajo y Previsi'#243'n Social.'
      Header.Title = 'Reinciar capacitaciones de STPS'
      inherited FolioInicialLbl: TLabel
        Visible = False
      end
      inherited FolioInicial: TZetaNumero
        Visible = False
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited cxGroupBox1: TcxGroupBox
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sFiltro: TcxMemo
        Style.IsFontAssigned = True
      end
    end
  end
end
