unit FWizIMSSValidacionIDSE_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls,
     ZetaDBTextBox, ZetaCommonLists, ZetaKeyCombo, Mask, ZetaNumero,
     DB, ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxContainer, cxEdit, ZetaCXWizard,
     dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
     dxWizardControl, Menus, cxButtons, cxStyles, dxSkinscxPCPainter,
     cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, cxDBData,
     cxClasses, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
     cxGridLevel, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TWizIMSSValidacionIDSE_DevEx = class(TcxBaseWizard)
    Label5: TLabel;
    BCargaArchivo: TcxButton;
    sArchivo: TEdit;
    OpenDialog: TOpenDialog;
    GroupBox1: TcxGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    tbNumeroLote: TZetaTextBox;
    tbFechaTransmision: TZetaTextBox;
    tbMovimientosOperados: TZetaTextBox;
    barraProgreso: TProgressBar;
    Label4: TLabel;
    tbArchivoCargado: TZetaTextBox;
    PanelMensaje: TPanel;
    Validacion: TdxWizardControlPage;
    lbMovNoReconocidos: TLabel;
    DataSourceRegistrosIDSE: TDataSource;
    PanelNombreArchivo: TPanel;
    gpResumenMovimientos: TcxGroupBox;
    tbMovimientosAltas: TZetaTextBox;
    Label6: TLabel;
    tbMovimientosCambios: TZetaTextBox;
    Label7: TLabel;
    tbMovimientosBajas: TZetaTextBox;
    Label8: TLabel;
    PanelMovimientosError: TPanel;
    Panel3: TPanel;
    btCancelarCarga: TcxButton;
    Label9: TLabel;
    tbRegistroPatron: TZetaTextBox;
    gridNoReconocidos: TcxGridDBTableView;
    ZetaCXGridLevel: TcxGridLevel;
    ZetaCXGrid: TZetaCXGrid;
    PAGINA: TcxGridDBColumn;
    NSS: TcxGridDBColumn;
    TEXTO: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure BCargaArchivoClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure sArchivoChange(Sender: TObject);
    procedure btCancelarCargaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    ArchivoVerif : String;
    ArchivoCargado : String;
    iCountMov : integer; 
    lCargar : boolean;
    function GetArchivo: String;
    function LeerArchivo: Boolean;
    function CargarMovimientosArchivo: Boolean;
    function MostrarAdvertencia( const sError: String; oControl: TWinControl ): Boolean;
    procedure LimpiarDatos;
    procedure ActualizarDatos;
    procedure IniciarProgreso;
    procedure AvanzarProgreso( i : integer);
    procedure TerminarProgreso;

  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizIMSSValidacionIDSE_DevEx: TWizIMSSValidacionIDSE_DevEx;

implementation

uses DCliente,
     DIMSS,
     DProcesos,
     ZGlobalTress,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     ZAsciiTools,
     ZetaDialogo,
     FTressShell,
     ZAccesosMgr,
     ZAccesosTress,
     FArchivoIDSE, ZcxWizardBasico;

{$R *.DFM}

procedure TWizIMSSValidacionIDSE_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     PanelMensaje.Visible := False;
     barraProgreso.Visible := False;

     LimpiarDatos;

     HelpContext := H_Validacion_Movimientos_IDSE;

     //DevEx
     Parametros.PageIndex := 0;
     Validacion.PageIndex := 1;
     Ejecucion.PageIndex := 2;
end;

function TWizIMSSValidacionIDSE_DevEx.MostrarAdvertencia( const sError: String; oControl: TWinControl ): Boolean;
begin
     ZetaDialogo.ZWarning( Caption, sError, 0, mbOK  );
     oControl.SetFocus;
     Result := False;
end;


function TWizIMSSValidacionIDSE_DevEx.GetArchivo: String;
begin
     Result := sArchivo.Text;
end;

procedure TWizIMSSValidacionIDSE_DevEx.CargaParametros;
begin
     inherited;
     with ParameterList do
     begin
          AddString( 'Archivo', GetArchivo );
          AddString( 'LOTE_IDSE',ArchivoIDSE.NumeroLote );
          AddString( 'REG_PATRON',ArchivoIDSE.RegistroPatronal );
          AddDate( 'FECHA_IDSE', ArchivoIDSE.FechaTransmision  );
     end;
     //DevEx
     with Descripciones do
     begin
          AddString( 'Archivo', GetArchivo );
          AddString( 'N�mero de Lote IDSE', ArchivoIDSE.NumeroLote );
          AddString( 'Registro patronal', ArchivoIDSE.RegistroPatronal );
          AddDate( 'Fecha de Transmisi�n IDSE', ArchivoIDSE.FechaTransmision );
     end;
end;

function TWizIMSSValidacionIDSE_DevEx.CargarMovimientosArchivo: Boolean;
var
   sMovimiento : string;
   nMovimientos : integer;
begin
    lCargar := True;

    ArchivoIDSE.DataSetMovimientos := dmImss.DataSetMovimientosIDSE;
    ArchivoIDSE.DataSetMovimientos.Filter := 'VALIDO = false';
    ArchivoIDSE.DataSetMovimientos.Filtered := true;
    ArchivoIDSE.DataSetMovimientos.DisableControls;

    DataSourceRegistrosIDSE.DataSet := ArchivoIDSE.DataSetMovimientos;
    if ( ArchivoIDSE.Iniciar ) and ( ArchivoIDSE.Movimientos > 0 )  then
    begin
       ActualizarDatos;
       Application.ProcessMessages;
       IniciarProgreso;
       while ( lCargar )  do
       begin
            nMovimientos := ArchivoIDSE.LeerMovimiento( sMovimiento );
            if nMovimientos > 0 then
            begin
               AvanzarProgreso( nMovimientos );

               Application.ProcessMessages;
            end
            else
                Break;
       end;
       ArchivoIDSE.Terminar;
       TerminarProgreso;

       ArchivoIDSE.DataSetMovimientos.EnableControls;
       Application.ProcessMessages;
    end;

    Result := lCargar;

end;


function TWizIMSSValidacionIDSE_DevEx.LeerArchivo: Boolean;
var
   oCursor: TCursor;
begin

     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     lCargar := TRUE;
     LimpiarDatos;

     try
        with dmProcesos do
        begin
             Application.ProcessMessages;
             CargaParametros;

             if  ArchivoIDSE.Cargar( GetArchivo ) then
             begin
                  ActualizarDatos;
                  Result := True;
             end
             else
             begin
                  sArchivo.Enabled := True;
                  BCargaArchivo.Enabled := True;
                  Error( 'El archivo ' +  GetArchivo + ' no pudo ser le�do, no contiene la informaci�n necesaria para ser procesado ', sArchivo);
                  Result := False;
                  LimpiarDatos;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
            TerminarProgreso;
     end;
end;

procedure TWizIMSSValidacionIDSE_DevEx.BCargaArchivoClick(Sender: TObject);
begin
     inherited;
     sArchivo.Text := ZetaClientTools.AbreDialogo( OpenDialog, sArchivo.Text, 'pdf' );
end;

procedure TWizIMSSValidacionIDSE_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
          if Adelante then
          begin
               if EsPaginaActual( Parametros ) then
               begin
                        if ZetaCommonTools.StrVacio( GetArchivo ) then
                           CanMove := Error( 'Debe especificarse un archivo a importar', sArchivo )
                        else
                           if not FileExists( GetArchivo ) then
                              CanMove := Error( 'El archivo ' + GetArchivo + ' no existe', sArchivo )
                           else
                           begin
                                 if ( ArchivoIDSE.Movimientos  <= 0 ) then
                                    CanMove:= MostrarAdvertencia('No es posible continuar con el proceso de validaci�n, el archivo ' +  GetArchivo + ' del Lote '+ ArchivoIDSE.NumeroLote +' no contiene movimientos operados por IMSS' ,sArchivo)
                                 else
                                   CanMove := ArchivoVerif <> VACIO;
                           end;
               end;
               if EsPaginaActual( Validacion ) then
               begin
                    CanMove :=  ( ArchivoCargado = ArchivoVerif );
               end;
          end;

     end;
end;

function TWizIMSSValidacionIDSE_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.ValidarMovimientosIDSE( ParameterList ) ;
end;

procedure TWizIMSSValidacionIDSE_DevEx.sArchivoChange(Sender: TObject);
begin
  inherited;
  if FileExists( sArchivo.Text ) then
  begin
       sArchivo.Enabled := False;
       BCargaArchivo.Enabled := False;
       LeerArchivo;
       sArchivo.Enabled := True;
       BCargaArchivo.Enabled := True;
  end
  else
  begin
     LimpiarDatos;
  end;
end;

procedure TWizIMSSValidacionIDSE_DevEx.LimpiarDatos;
begin
     tbArchivoCargado.Caption := VACIO;
     tbNumeroLote.Caption := VACIO;
     tbFechaTransmision.Caption := VACIO;
     tbMovimientosBajas.Caption := VACIO;
     tbMovimientosCambios.Caption := VACIO;
     tbMovimientosAltas.Caption := VACIO;
     tbMovimientosOperados.Caption := VACIO;
     tbRegistroPatron.Caption := VACIO; 
     ArchivoVerif := VACIO;
     ArchivoCargado := VACIO;
     PanelMovimientosError.Visible := False;
end;

procedure TWizIMSSValidacionIDSE_DevEx.ActualizarDatos;
begin
     LimpiarDatos;

     if ArchivoIDSE <> nil then
     begin
          if  ArchivoIDSE.CargoArchivo then
          begin
               tbArchivoCargado.Caption := ExtractFileName( GetArchivo );
               tbNumeroLote.Caption := ArchivoIDSE.NumeroLote;
               tbRegistroPatron.Caption := ArchivoIDSE.RegistroPatronal;
               tbFechaTransmision.Caption := FechaAsStr( ArchivoIDSE.FechaTransmision );
               tbMovimientosBajas.Caption := IntToStr( ArchivoIDSE.MovimientosBajas );
               tbMovimientosCambios.Caption := IntToStr( ArchivoIDSE.MovimientosCambios );
               tbMovimientosAltas.Caption := IntToStr( ArchivoIDSE.MovimientosReingresos );
               tbMovimientosOperados.Caption := IntToStr( ArchivoIDSE.Movimientos );
               ArchivoVerif := GetArchivo;
          end;
     end;
end;

procedure TWizIMSSValidacionIDSE_DevEx.AvanzarProgreso( i : integer);
begin
     iCountMov := iCountMov + i;
     PanelMensaje.Caption :=  Format ( 'Leyendo movimientos (%d de %d) ... ', [iCountMov, ArchivoIDSE.Movimientos] );
     barraProgreso.StepBy( i );
end;

procedure TWizIMSSValidacionIDSE_DevEx.IniciarProgreso;
begin
     btCancelarCarga.Enabled := false;
     PanelNombreArchivo.Caption :=  'Archivo:' + tbArchivoCargado.Caption;
     PanelMensaje.Caption := 'Leyendo movimientos ... ';
     barraProgreso.Min := 1;
     iCountMov := 0;
     barraProgreso.Max := ArchivoIDSE.Movimientos;
     barraProgreso.Position := 0;
     barraProgreso.Visible := True;
     PanelMensaje.Visible := True;
     lbMovNoReconocidos.Enabled := False;
     //gridNoReconocidos.Enabled := False;
     ZetaCXGrid.Enabled := False;   //DevEx
     //gridNoReconocidos.Color := clBtnFace;  //DevEx
     gridNoReconocidos.Styles.Content.Color  := clBtnFace;  //DevEx
     PanelMovimientosError.Visible := False;
     btCancelarCarga.Enabled := true;
end;

procedure TWizIMSSValidacionIDSE_DevEx.TerminarProgreso;
begin
  btCancelarCarga.Enabled := false;
  if ( lCargar ) then
  begin
    if ( iCountMov < ArchivoIDSE.Movimientos ) then
    begin
        PanelMensaje.Caption := 'Documento cargado con problemas';
        lbMovNoReconocidos.Enabled := True;
        //gridNoReconocidos.Enabled := True;
        ZetaCXGrid.Enabled := True;       //DevEx
        //gridNoReconocidos.Color := clWindow;    //DevEx
        gridNoReconocidos.Styles.Content.Color := clWindow;
        PanelMovimientosError.Visible := True;
    end
    else
    begin
        PanelMensaje.Caption := 'Documento cargado con �xito';
        lbMovNoReconocidos.Enabled := False;
        //gridNoReconocidos.Enabled := False;
        ZetaCXGrid.Enabled := False;  //DevEx
        //gridNoReconocidos.Color := clBtnFace;    //DevEx
        gridNoReconocidos.Styles.Content.Color := clBtnFace;
        PanelMovimientosError.Visible := False;
    end;

    barraProgreso.Visible := True;
    PanelMensaje.Visible := True;
  end;
end;

procedure TWizIMSSValidacionIDSE_DevEx.WizardAfterMove(Sender: TObject);
begin
     inherited;
     with Wizard do
     begin
        if EsPaginaActual( Parametros ) then
        begin
          lCargar := False;
        end;
        if EsPaginaActual( Validacion ) then
        begin
             if ( ArchivoCargado <> ArchivoVerif ) then
             begin
                  if ( CargarMovimientosArchivo )then
                  begin
                     ArchivoCargado := ArchivoVerif;
                     Siguiente;
                  end
                  else
                  begin
                      //Mensaje de Error
                      ArchivoCargado := VACIO;
                  end;
             end;
        end;
     end;
end;

procedure TWizIMSSValidacionIDSE_DevEx.btCancelarCargaClick(Sender: TObject);
begin
  inherited;
  lCargar := False;
  Wizard.Anterior;
  WizardControl.ActivePage := Parametros;
end;

procedure TWizIMSSValidacionIDSE_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Advertencia.Caption :=
     'Al aplicar el proceso se deposita el n�mero de lote y fecha que viene en la '
     + 'Constancia de transmisi�n de movimientos afiliatorios en cada movimiento de '
     + 'kardex del empleado correspondiente para certificar que fue recibido por el IMSS.';

     gridNoReconocidos.OptionsCustomize.ColumnHorzSizing := True;
     gridNoReconocidos.ApplyBestFit();
     gridNoReconocidos.OptionsCustomize.ColumnHorzSizing := True;
end;

end.
