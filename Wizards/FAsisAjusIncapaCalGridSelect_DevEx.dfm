inherited AsisAjusIncapaCalGridSelect_DevEx: TAsisAjusIncapaCalGridSelect_DevEx
  Left = 215
  Top = 328
  Width = 945
  Caption = 'Seleccione las tarjetas de Asistencia'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 929
    inherited OK_DevEx: TcxButton
      Left = 765
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 843
    end
  end
  inherited PanelSuperior: TPanel
    Width = 929
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 929
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        Width = 60
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        Width = 230
      end
      object AU_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'AU_FECHA'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
        Width = 70
      end
      object AU_TIPO: TcxGridDBColumn
        Caption = 'Incidencia'
        DataBinding.FieldName = 'AU_TIPO'
        Width = 55
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Tipo Incapacidad'
        DataBinding.FieldName = 'TB_ELEMENT'
        Width = 150
      end
      object IN_SUA_INI: TcxGridDBColumn
        Caption = 'Fecha Inicial'
        DataBinding.FieldName = 'IN_SUA_INI'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
        Width = 70
      end
      object IN_SUA_FIN: TcxGridDBColumn
        Caption = 'Fecha Final'
        DataBinding.FieldName = 'IN_SUA_FIN'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
        Width = 70
      end
      object IN_NUMERO: TcxGridDBColumn
        Caption = '# de Incapacidad'
        DataBinding.FieldName = 'IN_NUMERO'
        Width = 90
      end
      object AU_STATUS: TcxGridDBColumn
        Caption = 'D'#237'a'
        DataBinding.FieldName = 'AU_STATUS'
        Width = 60
      end
      object AU_TIPODIA: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'AU_TIPODIA'
        Width = 60
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
