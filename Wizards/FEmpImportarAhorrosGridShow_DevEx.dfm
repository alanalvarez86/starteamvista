inherited EmpImportarAhorrosGridShow_DevEx: TEmpImportarAhorrosGridShow_DevEx
  Left = 385
  Top = 295
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Lista de Errores Detectados'
  ClientWidth = 569
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 569
    inherited OK_DevEx: TcxButton
      Left = 405
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 484
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 569
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object RENGLON: TcxGridDBColumn
        Caption = 'Rengl'#243'n'
        DataBinding.FieldName = 'RENGLON'
        Width = 64
      end
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
        Width = 64
      end
      object AH_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'AH_TIPO'
        Width = 64
      end
      object AH_FECHA: TcxGridDBColumn
        Caption = 'Fecha Inicio'
        DataBinding.FieldName = 'AH_FECHA'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
      end
      object AH_SALDO_I: TcxGridDBColumn
        Caption = 'Saldo Inicial'
        DataBinding.FieldName = 'AH_SALDO_I'
      end
      object NUM_ERROR: TcxGridDBColumn
        Caption = 'Errores'
        DataBinding.FieldName = 'NUM_ERROR'
        Width = 64
      end
      object DESC_ERROR: TcxGridDBColumn
        Caption = 'Descripci'#243'n de Errores'
        DataBinding.FieldName = 'DESC_ERROR'
        Width = 64
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
