unit FEMailTest;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, ComCtrls,
     DEMailMgr,
     ZetaDBTextBox, cxStyles, cxClasses, cxLookAndFeels, dxSkinsForm,
  cxGraphics, cxLookAndFeelPainters, Vcl.Menus, cxButtons, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxMemo, TressMorado2013, dxSkinsCore;

type
  TEMailTester = class(TForm)
    LogGB: TGroupBox;
    PanelBotones: TPanel;
    Start: TcxButton;
    Stop: TcxButton;
    Log: TcxMemo;
    MensajesLBL: TStaticText;
    Mensajes: TZetaTextBox;
    GreenLED: TcxButton;
    Configurar: TcxButton;
    DevEx_SkinController: TdxSkinController;
    cxStyleRepository1: TcxStyleRepository;
    cxStyleStatusBarLabel: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure StartClick(Sender: TObject);
    procedure StopClick(Sender: TObject);
    procedure ConfigurarClick(Sender: TObject);
  private
    { Private declarations }
    FEMailer: TTressCorreosServicio;
    FCursor: TCursor;
    procedure SetControls(const lStarted: Boolean);
  public
    { Public declarations }
    procedure UpdateStatus(const Loops, Actions: Integer);
  end;

var
  EMailTester: TEMailTester;

implementation

// uses FWorkFlowConfig;
// uses FGlobalNotificacionAdvertecia_DevEx, DSistema;

{$R *.DFM}

procedure TEMailTester.FormCreate(Sender: TObject);
begin
     FEMailer := TTressCorreosServicio.Create;
     with FEMailer do
     begin
          ServiceCreate(Self);
          OnFeedBack := UpdateStatus;
          Log := Self.Log.Lines;
     end;
end;

procedure TEMailTester.FormShow(Sender: TObject);
begin
     FCursor := Screen.Cursor;
     SetControls( False );

     Start.SetFocus;
end;

procedure TEMailTester.FormDestroy(Sender: TObject);
begin
     FEMailer.ServiceDestroy(Self);
     FreeAndNil( FEMailer );
end;

procedure TEMailTester.SetControls( const lStarted: Boolean );
begin
     Stop.Enabled := lStarted;
     Start.Enabled := not lStarted;
     Configurar.Enabled := not lStarted;
     Mensajes.Visible := lStarted;
     MensajesLBL.Visible := lStarted;
     GreenLED.Visible := lStarted;
     if lStarted then
     begin
          GreenLED.Enabled := True;
          Screen.Cursor := crHourglass;
     end
     else
     begin
          Screen.Cursor := FCursor;
     end;
end;

procedure TEMailTester.UpdateStatus( const Loops, Actions: Integer);
begin
     GreenLED.Enabled := not GreenLED.Enabled;
     Mensajes.Caption := Format( '%n', [ Actions / 1 ] );
     Forms.Application.ProcessMessages;
end;

procedure TEMailTester.StartClick(Sender: TObject);
begin
     SetControls( True );
     try
        try
           FEMailer.ServiceExecute(FEMailer);
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            SetControls( False );
     end;
end;

procedure TEMailTester.StopClick(Sender: TObject);
begin
     try
        FEMailer.Terminated := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

procedure TEMailTester.ConfigurarClick(Sender: TObject);
begin
     Screen.Cursor := crHourglass;
     try
        // FWorkFlowConfig.UpdateWorkFlowRegistry( FEMailer.EmailService, FEMailer.oZetaProvider );
     finally
            Screen.Cursor := FCursor;
     end;

      {if ( GlobalNotificacionAdvertencia_DevEx = nil ) then
             GlobalNotificacionAdvertencia_DevEx := TGlobalNotificacionAdvertencia_DevEx.Create( Application );

          with GlobalNotificacionAdvertencia_DevEx do
          begin
               ShowModal;
          end;}

end;

end.
