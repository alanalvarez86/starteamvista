unit FEMailSendTest;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ComCtrls, ExtCtrls,
     DEmailService;

type
  TEMailTest = class(TForm)
    DestinatarioLBL: TLabel;
    Ejecutar: TBitBtn;
    Destinatario: TEdit;
    TemaLBL: TLabel;
    Tema: TEdit;
    MensajeLBL: TLabel;
    Mensaje: TEdit;
    Salir: TBitBtn;
    procedure EjecutarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FEmailMgr: TdmEmailService;
    procedure SendEmail;
  public
    { Public declarations }
    property EmailMgr: TdmEmailService read FEmailMgr write FEmailMgr;
  end;

procedure ProbarEnvioCorreos( Manager: TdmEMailService );

implementation

uses ZetaDialogo,
     ZetaCommonClasses,
     ZetaCommonTools;

var
  EMailTest: TEMailTest;

{$R *.DFM}

procedure ProbarEnvioCorreos( Manager: TdmEMailService );
begin
     if not Assigned( EMailTest ) then
     begin
          EMailTest := TEMailTest.Create( Application );
     end;
     with EMailTest do
     begin
          EMailMgr := Manager;
          ShowModal;
     end;
end;

{ ******** TEMailTest ************ }

procedure TEMailTest.SendEmail;
var
   sSendTo, sSubject, sBody, sMsg: String;
begin
     sSendTo := Destinatario.Text;
     sSubject := Tema.Text;
     sBody := Mensaje.Text;
     if ZetaCommonTools.StrLleno( sSendTo ) then
     begin
          try
             with EMailMgr do
             begin
                  NewBodyMessage;
                  ToAddress.Add( sSendTo );
                  Subject := sSubject;
                  MessageText.Add( sBody );
                  if SendEMail( sMsg ) then
                  begin
                       ZetaDialogo.zInformation( '� Exito !', Format( 'El Correo Fu� Enviado%0:s%0:sPor Favor Revise El Buz�n Del Destinatario', [ CR_LF ] ), 0 );
                  end
                  else
                  begin
                       ZetaDialogo.zError( '� Problema Al Enviar Correo !', Format( 'Problema Al Enviar Correo: %s', [ sMsg ] ), 0 );
                  end;
             end;
          except
                on Error: Exception do
                begin
                     ZetaDialogo.zExcepcion( '� Error Al Enviar Correo !', 'Error Al Enviar Correo', Error, 0 );
                end;
          end;
     end;
end;

procedure TEMailTest.EjecutarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           SendEMail;
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEMailTest.FormShow(Sender: TObject);
begin
     ActiveControl := Destinatario;
end;

end.
