object EMailTest: TEMailTest
  Left = 363
  Top = 232
  Width = 384
  Height = 186
  ActiveControl = Destinatario
  Caption = 'Prueba de EMail'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  DesignSize = (
    376
    152)
  PixelsPerInch = 96
  TextHeight = 13
  object DestinatarioLBL: TLabel
    Left = 14
    Top = 17
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = '&Destinatario:'
  end
  object TemaLBL: TLabel
    Left = 43
    Top = 41
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = '&Tema:'
  end
  object MensajeLBL: TLabel
    Left = 30
    Top = 65
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = '&Mensaje:'
  end
  object Ejecutar: TBitBtn
    Left = 106
    Top = 109
    Width = 75
    Height = 27
    Hint = 'Enviar Un eMail de Prueba'
    Anchors = [akTop, akRight]
    Caption = '&Enviar'
    Default = True
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    OnClick = EjecutarClick
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333333333333333333FFFFFFFFFFFFFFF000000000000
      000077777777777777770FFFFFFFFFFFFFF07F3333FFF33333370FFFF777FFFF
      FFF07F333777333333370FFFFFFFFFFFFFF07F3333FFFFFF33370FFFF777777F
      FFF07F33377777733FF70FFFFFFFFFFF99907F3FFF33333377770F777FFFFFFF
      9CA07F77733333337F370FFFFFFFFFFF9A907FFFFFFFFFFF7FF7000000000000
      0000777777777777777733333333333333333333333333333333333333333333
      3333333333333333333333333333333333333333333333333333333333333333
      3333333333333333333333333333333333333333333333333333}
    NumGlyphs = 2
  end
  object Destinatario: TEdit
    Left = 76
    Top = 14
    Width = 292
    Height = 21
    TabOrder = 0
  end
  object Tema: TEdit
    Left = 76
    Top = 38
    Width = 292
    Height = 21
    TabOrder = 1
    Text = 'Prueba de Correos de WorkFlow'
  end
  object Mensaje: TEdit
    Left = 76
    Top = 62
    Width = 292
    Height = 21
    TabOrder = 2
    Text = 'Esta Es Una Prueba Del Env'#237'o de Correos de WorkFlow'
  end
  object Salir: TBitBtn
    Left = 188
    Top = 111
    Width = 75
    Height = 25
    Hint = 'Abandonar Esta Prueba'
    Caption = '&Salir'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
    Kind = bkClose
  end
end
