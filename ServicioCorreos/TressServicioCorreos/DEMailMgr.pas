unit DEMailMgr;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Dialogs, ComObj, DB,
     {$ifndef EMAILTEST}
     SvcMgr,
     {$endif}
     ZetaCommonLists,
     DEmailServiceReporte,
     DZetaServerProvider,
     DServicioCorreoTipos,
     Winapi.WinSvc;

type
  {$ifdef SERVICIOCORREOSTEST}
  TService = class( TObject )
  end;
  {$endif}
  TTressCorreosServicio = class(TService)
    procedure ServiceCreate(Sender: TObject);
    procedure ServiceDestroy(Sender: TObject);
    procedure ServiceExecute(Sender: TService);
    procedure EliminarArchivos (iFolioCorreo: Integer);
    procedure EliminarImagenes;
  private
    { Private declarations }
    FZetaProvider: TdmZetaServicioCorreosProvider;
    FRegistry: TSCRegistry;
    FPendientes: TZetaCursor;
    FEnviado: TZetaCursor;
    FProblema: TZetaCursor;
    FPendienteUno: TZetaCursor;
    FEmailService: TdmEmailServiceReporte;
    {$ifdef SERVICIOCORREOSTEST}
    FLog: TStrings;
    FOnFeedBack: TFeedBack;
    FLoops: Integer;
    FEMailCtr: Integer;
    FTerminated: Boolean;
    {$endif}
    function Start: Boolean;
    function Stop: Boolean;
    function Validar(var Mensaje: String): Boolean;
    procedure AddTo(Lista: TStrings; const sValue: String);
    {$ifdef SERVICIOCORREOSTEST}
    procedure DoFeedBack;
    {$endif}
    procedure MarcarEnviado(const idMsg: Integer);
    procedure MarcarProblema(const idMsg: Integer; const sLogMsg: String);
  protected
    { Protected declarations }
    procedure EscribeBitacora(const sMensaje: String);
    procedure EscribeExcepcion(const sMensaje: String; Error1: Exception );
  public
    { Public declarations }
    property EmailService: TdmEmailServiceReporte read FEmailService;
    property oZetaProvider: TdmZetaServicioCorreosProvider read FZetaProvider;
    {$ifdef SERVICIOCORREOSTEST}
    property Log: TStrings read FLog write FLog;
    property OnFeedBack: TFeedBack read FOnFeedBack write FOnFeedBack;
    property Terminated: Boolean read FTerminated write FTerminated;
    procedure LogMessage( const sTexto: String; const iValue1, iValue2, iValue3: Integer );
    {$else}
    function GetServiceController: TServiceController; override;
    {$endif}
  end;

var
  TressCorreosServicio: TTressCorreosServicio;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaWinAPITools,
     ZetaServerTools;

{$ifndef SERVICIOCORREOSTEST}
{$R *.DFM}
{$endif}

const
     Q_PENDIENTES_LEE = 'select TOP 100 EE_FOLIO, EE_FEC_IN '+
                        'from ENVIOEMAIL where ( EE_STATUS = %d ) order by EE_FOLIO ASC';

     Q_MARCA_ENVIADO = 'update ENVIOEMAIL set EE_ENVIADO = ''%s'', EE_STATUS = %d, EE_FEC_OUT = GetDate() where ( EE_FOLIO = :FOLIO )';

     Q_MARCA_PROBLEMA = 'update ENVIOEMAIL set EE_ENVIADO = ''%s'', EE_STATUS = %d, EE_FEC_OUT = GetDate(), EE_LOG = :LOG where ( EE_FOLIO = :FOLIO )';

     Q_PENDIENTE_LEE_UNO = 'select EE_FOLIO, EE_TO, EE_CC, EE_SUBJECT, EE_BODY, '+
                        'EE_FROM_NA, EE_FROM_AD, EE_SUBTYPE, EE_ENVIADO, EE_MOD_ADJ, EE_FEC_IN, '+
                        'EE_FEC_OUT, EE_ATTACH, EE_BORRAR, EE_ENCRIPT '+
                        'from ENVIOEMAIL where ( EE_FOLIO = :FOLIO )';


{ ** Bug #18814 Hosting - Error "Canvas does not allow drawing en reportes email" en ReportesEmail. **}
function GetServiceStatus(const AMachine, AService: PChar; out AStatus: SERVICE_STATUS_PROCESS): Boolean;
var
   scman_handle, svc_handle: SC_HANDLE;
   bytesNeeded             : DWORD;
begin
     result := FALSE;
     scman_handle := OpenSCManager(AMachine, nil, SC_MANAGER_CONNECT);
     if scman_handle <> 0 then
     try
        svc_handle := OpenService(scman_handle, AService, SERVICE_QUERY_STATUS);
        if svc_handle <> 0 then
        try
          result := QueryServiceStatusEx(svc_handle, SC_STATUS_PROCESS_INFO, @AStatus, SizeOf(SERVICE_STATUS_PROCESS), bytesNeeded);
        finally
          CloseServiceHandle(svc_handle);
        end;
     finally
            CloseServiceHandle(scman_handle);
     end;
end;

{$ifndef SERVICIOCORREOSTEST}
procedure ServiceController(CtrlCode: DWord); stdcall;
begin
     TressCorreosServicio.Controller(CtrlCode);
end;
{$endif}

{ ******* TServicioCorreosManager ******** }

{$ifndef SERVICIOCORREOSTEST}
function TTressCorreosServicio.GetServiceController: TServiceController;
begin
     Result := ServiceController;
end;
{$endif}

procedure TTressCorreosServicio.EscribeBitacora(const sMensaje: String);
var sDescripcion, sTexto: String;
begin
     try
        MarcarProblema(FPendienteUno.FieldByName( 'EE_FOLIO' ).AsInteger, sMensaje);
     except
           on Error: Exception do
           begin
                sDescripcion := 'TRESS Correos: Error en bit�cora del servicio.';
                sTexto := sMensaje
                          + CR_LF + Error.Message;
                LogMessage( sDescripcion + CR_LF + sTexto, EVENTLOG_ERROR_TYPE, 0, 0 );
                oZetaProvider.EscribeBitacora (tbErrorGrave, clbNinguno, 0, Date, sDescripcion, sTexto);
           end;
     end;
end;

procedure TTressCorreosServicio.EscribeExcepcion(const sMensaje: String; Error1: Exception );
var sDescripcion, sTexto: String;
begin
     try
        MarcarProblema(FPendienteUno.FieldByName( 'EE_FOLIO' ).AsInteger, sMensaje + CR_LF + Error1.Message);
     except
           on Error2: Exception do
           begin
                sDescripcion := 'TRESS Correos: Error en bit�cora del servicio.';
                sTexto := sMensaje
                            + CR_LF + Error1.Message
                            + CR_LF + Error2.Message;

                LogMessage( sDescripcion + CR_LF + sTexto, EVENTLOG_ERROR_TYPE, 0, 0 );
                oZetaProvider.EscribeBitacora (tbErrorGrave, clbNinguno, 0, Date, sDescripcion, sTexto);
           end;
     end;
end;

{$ifdef SERVICIOCORREOSTEST}
procedure TTressCorreosServicio.LogMessage( const sTexto: String; const iValue1, iValue2, iValue3: Integer );
const
     K_LOG_MAX = 100;
var
   i: Integer;
begin
     with Log do
     begin
          BeginUpdate;
          try
             if ( Count > K_LOG_MAX ) then
             begin
                  for i := ( Count - 1 ) downto ( K_LOG_MAX - 1 ) do
                  begin
                       Delete( i );
                  end;
             end;
             Insert( 0, Format( '[ %s ] %s', [ FormatDateTime( 'hh:nn:ss', Now ), sTexto ] ) );
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TTressCorreosServicio.DoFeedBack;
begin
     if Assigned( FOnFeedBack ) then
        FOnFeedBack( FLoops, FEMailCtr );
end;
{$endif}

procedure TTressCorreosServicio.ServiceCreate(Sender: TObject);
{$ifndef SERVICIOCORREOSTEST}
 var
    FRegistryServer : TServicioCorreosRegistryServer;
    sDependency: string;
{$endif}
begin
     FZetaProvider := TdmZetaServicioCorreosProvider.Create( nil );
     FEmailService := TdmEmailServiceReporte.Create( nil );
     FRegistry := TSCRegistry.Create( False );
     FRegistry.oZetaProvider := Self.oZetaProvider;

{$ifndef SERVICIOCORREOSTEST}
     {$ifdef MSSQL}
     FRegistryServer := TServicioCorreosRegistryServer.Create( False );
     try
        sDependency := FRegistryServer.ServicioCorreosServerDependency;
     finally
            FreeAndNil( FRegistryServer );
     end;

     ZetaWinAPITools.OpenServiceManager;

     if ZetaCommonTools.StrLleno( sDependency ) and ZetaWinAPITools.FindService( sDependency ) then
     begin
          with TDependency.Create( Dependencies ) do
          begin
               IsGroup := False;
               Name := sDependency;
          end;
     end;

     ZetaWinAPITools.CloseServiceManager;
     {$endif}
{$endif}
end;

procedure TTressCorreosServicio.ServiceDestroy(Sender: TObject);
begin
     FreeAndNil( FRegistry );
     FreeAndNil( FEmailService );
     FreeAndNil( FZetaProvider );
end;

function TTressCorreosServicio.Start: Boolean;
begin
     Result := False;
     try
        with oZetaProvider do
        begin
             Start;
        end;

        with FEmailService do
        begin
             NewEMail;
             with FRegistry do
             begin
                  MailServer := EMailServer;
                  Port := EMailPort;
                  User := EMailUser;
                  PassWord := EMailPassword;
                  AuthMethod := EMailAuth;
                  DirectorioReportes := EmailDirectorioReportes;
                  DirectorioImagenes := EmailDirectorioImagenes;
                  URLImagenes := EmailURLImagenes;
                  ExpiracionImagenes := EmailExpiracionImagenes;
             end;
             SubType := emtTexto;
        end;
        {$ifdef SERVICIOCORREOSTEST}
        Log.Clear;
        FLoops := 0;
        FEMailCtr := 0;
        Terminated := False;
        {$endif}
        Result := True;
     except
           on Error: Exception do
           begin
                EscribeExcepcion( 'Error al iniciar servicio', Error );
                {$ifdef SERVICIOCORREOSTEST}
                Terminated := True;
                {$endif}
           end;
     end;
end;

function TTressCorreosServicio.Validar( var Mensaje: String ): Boolean;
begin
     Result := False;
     Mensaje := '';
     with FPendienteUno do
     begin
          if ZetaCommonTools.StrVacio( FieldByName( 'EE_FROM_NA' ).AsString ) then
             Mensaje := 'Nombre del remitente vac�o'
          else
              if ZetaCommonTools.StrVacio( FieldByName( 'EE_FROM_AD' ).AsString ) then
                 Mensaje := 'Direcci�n de correo electr�nico del remitente vac�a'
              else
                  if ZetaCommonTools.StrVacio( FieldByName( 'EE_TO' ).AsString ) then
                     Mensaje := 'Direcci�n de correo electr�nico del destinatario vac�a'
                  else
                      if ZetaCommonTools.StrVacio( FieldByName( 'EE_SUBJECT' ).AsString ) then
                         Mensaje := 'Tema del correo no fu� especificado'
                      else
                          if ZetaCommonTools.StrVacio( FieldByName( 'EE_BODY' ).AsString ) then
                             Mensaje := 'Texto del correo no fu� especificado'
                          else
                              Result := True;
     end;
end;

procedure TTressCorreosServicio.ServiceExecute(Sender: TService);
const
     K_PAUSA = 15000;
     { ** Bug #18814 Hosting - Error "Canvas does not allow drawing en reportes email" en ReportesEmail. **}
     K_TRESS_REPORTES = 'TressReportesServicio';
     K_20_PAUSAS = 20;
var
   idMsg: Integer;
   lEnviado: Boolean;
   sLogMsg: String;
   bCambiarNombreAdjunto: Boolean;
   { ** Bug #18814 Hosting - Error "Canvas does not allow drawing en reportes email" en ReportesEmail. **}
   ss: SERVICE_STATUS_PROCESS;
   iContadorPausas: Integer;
begin

     { ** Bug #18814 Hosting - Error "Canvas does not allow drawing en reportes email" en ReportesEmail. **}
     iContadorPausas := 0;

     if Start then
     begin
          while not Terminated do
          begin
               {$ifdef SERVICIOCORREOSTEST}
               Inc( FLoops );
               {$endif}
               try
                  FPendientes := oZetaProvider.CreateQuery( Format( Q_PENDIENTES_LEE, [ Ord( ocsSinEnviar ) ] ) );
                  try
                     with FPendientes do
                     begin
                          Active := True;
                          try
                             try
                                First;
                                while not Eof do
                                begin
                                     idMsg := FieldByName( 'EE_FOLIO' ).AsInteger;

                                     with oZetaProvider do
                                     begin
                                          FPendienteUno := CreateQuery( Q_PENDIENTE_LEE_UNO );
                                          try
                                             ParamAsInteger( FPendienteUno, 'FOLIO', idMsg );
                                             EmpiezaTransaccion;
                                             try
                                                Ejecuta( FPendienteUno );

                                                if( not FPendienteUno.Eof )then
                                                begin
                                                     lEnviado := False;
                                                     sLogMsg := VACIO;
                                                     if Validar( sLogMsg ) then
                                                     begin
                                                          try
                                                             with FEMailService do
                                                             begin
                                                                  NewBodyMessage;
                                                                  FromName := FPendienteUno.FieldByName( 'EE_FROM_NA' ).AsString;
                                                                  FromAddress := FPendienteUno.FieldByName( 'EE_FROM_AD' ).AsString;
                                                                  AddTo( ToAddress, FPendienteUno.FieldByName( 'EE_TO' ).AsString );
                                                                  AddTo( ToCarbonCopy, FPendienteUno.FieldByName( 'EE_CC' ).AsString );
                                                                  Subject := FPendienteUno.FieldByName( 'EE_SUBJECT' ).AsString;

                                                                  // Revisar si el mensaje est� encriptado. Desencriptar si
                                                                  // el campo EE_ENCRIPT es 'S'.
                                                                  if FPendienteUno.FieldByName( 'EE_ENCRIPT' ).AsString = K_GLOBAL_SI then
                                                                      AddTo( MessageText, Decrypt (FPendienteUno.FieldByName( 'EE_BODY' ).AsString))
                                                                  else
                                                                      AddTo( MessageText, FPendienteUno.FieldByName( 'EE_BODY' ).AsString );

                                                                  SubType := eEMailType( FPendienteUno.FieldByName( 'EE_SUBTYPE' ).AsInteger );

                                                                  Attachments.StrictDelimiter := TRUE;
                                                                  Attachments.Delimiter := ',';
                                                                  Attachments.DelimitedText := FPendienteUno.FieldByName( 'EE_ATTACH' ).AsString;
                                                                  ReceiptRecipientName := '';
                                                                  ReceiptRecipientAddress := '';

                                                                  bCambiarNombreAdjunto := FPendienteUno.FieldByName( 'EE_MOD_ADJ' ).AsString = K_GLOBAL_SI;
                                                                  if SendEMail (sLogMsg, bCambiarNombreAdjunto, idMsg) then
                                                                  begin
                                                                       lEnviado := True;
                                                                  end
                                                                  else
                                                                  begin
                                                                       Self.EscribeBitacora (Format ('Problema al enviar correo: %s', [sLogMsg]));
                                                                  end;
                                                             end;
                                                          except
                                                                on Error: Exception do
                                                                begin
                                                                     Self.EscribeExcepcion ('Error al enviar correo' , Error );
                                                                end;
                                                          end;
                                                     end
                                                     else
                                                     begin
                                                          Self.EscribeBitacora( Format( 'Correo incompleto {%d}: %s', [ idMsg, sLogMsg ] ) );
                                                     end;

                                                     if lEnviado then
                                                     begin
                                                          {$ifdef SERVICIOCORREOSTEST}
                                                          Inc( FEMailCtr );
                                                          {$endif}

                                                          MarcarEnviado( idMsg );

                                                          // Eliminar archivos que se enviaron como adjuntos
                                                          // de la carpeta global Notificaciones/E-Mail/[Directorio para servicio de reportes].
                                                          // Eliminar solo si as� se indica a trav�s del campo EE_BORRAR.
                                                          if FPendienteUno.FieldByName( 'EE_BORRAR' ).AsString = K_GLOBAL_SI then
                                                          begin
                                                               EliminarArchivos(idMsg);
                                                          end;
                                                     end
                                                end;
                                                TerminaTransaccion( True );
                                             except
                                                   on Error: Exception do
                                                   begin
                                                        EscribeExcepcion( Format( 'Error al consultar registro de correo {%d}.', [  idMsg  ] ), Error );
                                                   end;
                                             end;
                                          finally
                                                 FreeAndNil (FPendienteUno);
                                          end;
                                     end;

                                     Next;
                                end;

                             except
                                   on Error: Exception do
                                   begin
                                        EscribeExcepcion( 'Error al procesar mensajes.', Error );
                                   end;
                             end;
                          finally
                                 Active := False;
                          end;
                     end;
                  finally
                         FreeAndNil (FPendientes);
                  end;
               except
                     on Error: Exception do
                     begin
                          EscribeExcepcion( 'Error al leer buz�n', Error );
                     end;
               end;

               // Revisar im�genes y borrar las que cumplen la cantidad
               // de d�as indicado en el global.
               EliminarImagenes;

               { Esperar 1 Segundo Antes De Empezar El Nuevo Query }
               {$ifdef SERVICIOCORREOSTEST}
               Sleep( K_PAUSA );
               DoFeedBack;
               {$else}
               ServiceThread.ProcessRequests( False );
               Sleep( K_PAUSA );
               {$endif}

               { ** Bug #18814 Hosting - Error "Canvas does not allow drawing en reportes email" en ReportesEmail. **}
               // Si est� detenido el servicio de TressReportes, iniciar.

               // Si han pasado 20 pausas, es decir, 300 segundos  (5 minutos),
               // revisar el servicio de reportes para iniciarlo si est� detenido.
               if iContadorPausas = K_20_PAUSAS then
               begin
                    iContadorPausas := 0;
                    if GetServiceStatus (nil, K_TRESS_REPORTES, ss) then
                    begin
                          if ss.dwCurrentState = SERVICE_STOPPED then
                          begin
                               // Iniciar servicio de Tress Correos.
                               ZetaWinAPITools.InitService (K_TRESS_REPORTES, '', FALSE)
                          end;
                    end
               end
               else
               begin
                    iContadorPausas := iContadorPausas + 1;
               end;
          end;
     end;
     Stop;
end;

procedure TTressCorreosServicio.EliminarArchivos (iFolioCorreo: Integer);
const
     K_SE_PUEDE_ELIMINAR = 'SELECT COUNT(EE_FOLIO) AS CANTIDAD FROM ENVIOEMAIL WHERE CONVERT(VARCHAR, EE_ATTACH) = %s AND EE_STATUS =  %d';
var
   iAttach : integer;
   bSePuedeEliminar: Boolean;
   SePuedeEliminarCursor: TZetaCursor;
begin
     try
        for iAttach := 0  to  FEMailService.Attachments.Count - 1 do
        begin
             // De cada archivo extraer solo el nombre, no considerar path ya que todos deben estar
             // depositados en el directorio indicado en globales:
             // CLAVE_DIRECTORIO_SERVICIO_REPORTES.

             SePuedeEliminarCursor := oZetaProvider.CreateQuery( Format( K_SE_PUEDE_ELIMINAR,
                                   [EntreComillas (FEMailService.DirectorioReportes + '\' + SysUtils.ExtractFileName(FEMailService.Attachments[iAttach])),
                                   Ord( ocsSinEnviar ) ] ) );
             try
                SePuedeEliminarCursor.Active := TRUE;
                SePuedeEliminarCursor.First;

                // Agregar validaci�n.
                // Solo eliminar si todos los correos que incluyan el mismo attachment se encuentran procesados.
                if SePuedeEliminarCursor.FieldByName ('CANTIDAD').AsInteger = 0 then
                begin
                     if FileExists( FEMailService.DirectorioReportes + '\' + SysUtils.ExtractFileName(FEMailService.Attachments[iAttach])) then
                     begin
                          if not (DeleteFile(FEMailService.DirectorioReportes + '\' + SysUtils.ExtractFileName(FEMailService.Attachments[iAttach]))) then
                          begin
                               // Validar mensaje con �rea de documentaci�n.
                               MarcarProblema (iFolioCorreo, Format ('No es posible eliminar archivo: %s del directorio global de reportes.',
                                               [SysUtils.ExtractFileName(FEMailService.Attachments[iAttach])]));
                          end;
                     end
                     else
                     begin
                          MarcarProblema (iFolioCorreo, Format ('No existe archivo %s.', [FEMailService.DirectorioReportes + '\' + SysUtils.ExtractFileName(FEMailService.Attachments[iAttach])]));
                     end;
                end;

                SePuedeEliminarCursor.Active := FALSE;

             finally
                    FreeAndNil (SePuedeEliminarCursor);
             end;
        end;

     except
           on Error: Exception do
           begin
                EscribeExcepcion (Format ('No es posible eliminar archivo: %s del directorio global de reportes.',
                               [SysUtils.ExtractFileName(FEMailService.Attachments[iAttach])]), Error);

                if Assigned (SePuedeEliminarCursor) then
                   FreeAndNil (SePuedeEliminarCursor);
           end;
     end;
end;

procedure TTressCorreosServicio.EliminarImagenes;
const
     K_IMAGEN_PLANTILLA_CORREO = 'LOGOCORREO.JPG';
var
   archivos: TSearchRec;
begin
     // Leer directorio de im�genes.
     // Por cada archivo que se encuentre,
     // comparar su antig�edad con el tiempo de expiraci�n indicado en el global.
     // Si el tiempo de expiraci�n es superado por la antig�edad del archivo, eliminarlo.
     if FindFirst(FEMailService.DirectorioImagenes + '\*.jpg', faAnyFile, archivos) = 0 then
     begin
          try
             repeat
                   if (archivos.Attr<>faDirectory) and (archivos.Name<>'.') and (archivos.Name<>'..')
                   and (UpperCase (archivos.Name) <> K_IMAGEN_PLANTILLA_CORREO)
                   then
                   begin
                        if (Now - archivos.TimeStamp >= FEMailService.ExpiracionImagenes) then
                        begin
                             DeleteFile(FEmailService.DirectorioImagenes + '\' + archivos.Name);
                        end;
                   end;
             until FindNext(archivos) <> 0;

         finally
                FindClose(archivos);
         end;
     end;
end;

function TTressCorreosServicio.Stop: Boolean;
begin
     Result := False;
     try
        with oZetaProvider do
        begin
             FreeAndNil( FPendienteUno );
             FreeAndNil( FEnviado );
             FreeAndNil( FProblema );
             FreeAndNil( FPendientes );
             Stop;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                EscribeExcepcion( 'Error al detener servicio', Error );
           end;
     end;
end;

procedure TTressCorreosServicio.AddTo( Lista: TStrings; const sValue: String );
begin
     if ZetaCommonTools.StrLleno( sValue ) then
     begin
          Lista.Add( sValue );
     end;
end;

procedure TTressCorreosServicio.MarcarEnviado( const idMsg: Integer );
begin
     with oZetaProvider do
     begin
          FEnviado := CreateQuery( Format( Q_MARCA_ENVIADO, [ K_GLOBAL_SI, Ord( ocsEnviadoOK ) ] ) );
          try
             ParamAsInteger( FEnviado, 'FOLIO', idMsg );
             EmpiezaTransaccion;
             try
                Ejecuta( FEnviado );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        EscribeExcepcion( Format( 'Error al marcar env�o de correo {%d}', [ idMsg] ), Error );
                   end;
             end;
          finally
                 FreeAndNil (FEnviado);
          end;
     end;
end;

procedure TTressCorreosServicio.MarcarProblema( const idMsg: Integer; const sLogMsg: String );
begin
     with oZetaProvider do
     begin
          FProblema := CreateQuery( Format( Q_MARCA_PROBLEMA, [ K_GLOBAL_NO, Ord( ocsErrorAlEnviar ) ] ) );

          try
             ParamAsInteger( FProblema, 'FOLIO', idMsg );
             ParamAsString( FProblema, 'LOG', sLogMsg );
             EmpiezaTransaccion;
             try
                Ejecuta( FProblema );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        EscribeExcepcion( Format( 'Error al marcar problema en env�o de correo {%d}', [  idMsg  ] ), Error );
                   end;
             end;
          finally
                 FreeAndNil (FProblema);
          end;
     end;
end;

end.
