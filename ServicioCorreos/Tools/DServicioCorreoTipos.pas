unit DServicioCorreoTipos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs,
     ComObj, DB, Registry,
     ZetaRegistryServer,
     ZetaCommonLists,
     DZetaServerProvider
     , ZetaServerTools
     ;

type
  TFeedBack = procedure( const Loops, Actions: Integer ) of object;
  TUpdateStatus = procedure( const sTexto: String ) of object;
  TSCIdentifier = TGUID;
  TUsuario = Integer;
  TNotas = String;
  TSCFecha = TDateTime;
  TSCData = String;
  TLlave = Integer;
  { Herencia Temporal De Provider }
  TdmZetaServicioCorreosProvider = class(TdmZetaServerProvider)
  private
    { Private Declarations }
    FGlobalUpdate: TZetaCursor;
    FGlobalInsert: TZetaCursor;
    function LeerClaves( const iNumero: Integer ): String;
  public
    { Public Declarations }
    procedure Start;
    procedure Stop;
  end;  
  TRegistryBase = class( TObject )
  private
    { Private Declarations }
    FReadWrite: Boolean;
    FZetaProvider: TdmZetaServicioCorreosProvider;    
  protected
    { Protected Declarations }
  public
    { Public Declarations }
    constructor Create( const lReadWrite: Boolean = TRUE );
    property oZetaProvider: TdmZetaServicioCorreosProvider read FZetaProvider write FZetaProvider;
    property ReadWrite: Boolean read FReadWrite;
    destructor Destroy; override;
  end;
  TSCRegistry = class( TRegistryBase )
  private
    { Private Declarations }
    function GetEMailServer: String;
    function GetEMailPort: Integer;
    // function GetEMailAuth: Integer;
    function GetEMailAuth: eAuthTressEmail;
    function GetEMailUser: String;
    function GetEMailPassWord: String;
    function getDirectorioReportes: String;
    function getDirectorioImagenes: String;
    function getURLImagenes: String;
    function getExpiracionImagenes: Integer;
  public
    { Public Declarations }
    property EMailServer: String read GetEMailServer;
    property EMailPort: Integer read GetEMailPort;
    // property EMailAuth: Integer read GetEMailAuth; eAuthTressEmail
    property EMailAuth: eAuthTressEmail read GetEMailAuth;
    property EMailUser: String read GetEMailUser;
    property EMailPassword: String read GetEMailPassWord;

    property EmailDirectorioReportes: String read GetDirectorioReportes;
    property EmailDirectorioImagenes: String read getDirectorioImagenes;
    property EmailURLImagenes: String read getURLImagenes;
    property EmailExpiracionImagenes: Integer read getExpiracionImagenes;
  end;

  TServicioCorreosRegistryServer = class( TZetaRegistryServer )
  private
    { Private declarations }
    function GetServicioCorreosServerDependency: string;
    procedure SetServicioCorreosServerDependency(const Value: string);
  public
    { Public declarations }
    property ServicioCorreosServerDependency: string read GetServicioCorreosServerDependency write SetServicioCorreosServerDependency;
  end;

implementation


uses ZetaCommonTools,
     ZetaCommonClasses,
     ZGlobalTress;

const
     ServicioCorreos_SERVER_DEPENDENCY = 'ServicioCorreosServerDependency';

{ ******** DZetaServicioCorreosProvider ******** }

procedure TdmZetaServicioCorreosProvider.Start;
begin
     Activate;

     { ***** REVISAR ESTA INSTRUCCI�N ***** }
     {
         Es necesaria.
         No se ejecutaba en la versi�n anterior de este servicio,
         en el WorkFlowEmail.exe.
         IMPORTANTE validar.
     }
     {$IFNDEF SERVICIOCORREOSTEST}
     CoInitializeEx (nil, 0);
     {$ENDIF}

     EmpresaActiva := Comparte;
     InitGlobales;
end;

procedure TdmZetaServicioCorreosProvider.Stop;
begin
     Deactivate;
end;

function TdmZetaServicioCorreosProvider.LeerClaves( const iNumero: Integer ): String;
var
   FDataset: TZetaCursor;
begin
     EmpresaActiva := Comparte;
     FDataset := CreateQuery( Format( 'select CL_TEXTO from CLAVES where ( CL_NUMERO = %d )' , [ iNumero ] ) );
     try
        with FDataset do
        begin
             Active := True;
             if EOF then
                Result := ''
             else
                 Result := FieldByName( 'CL_TEXTO' ).AsString;
             Active := False;
        end;
     finally
            FreeAndNil( FDataset );
     end;
end;

{ ******** TRegistryBase ******** }

constructor TRegistryBase.Create(const lReadWrite: Boolean );
begin
     FReadWrite := lReadWrite;
end;

destructor TRegistryBase.Destroy;
begin
     inherited Destroy;
end;

{ ******** TSCRegistry ******** }

function TSCRegistry.GetEMailPassWord: String;
begin
     Result := oZetaProvider.LeerClaves(CLAVE_EMAIL_PSWD);
end;

function TSCRegistry.GetEMailPort: Integer;
begin
     Result := StrToIntDef (oZetaProvider.LeerClaves(CLAVE_PUERTO_SMTP), 25);
end;

// function TSCRegistry.GetEMailAuth: Integer;
function TSCRegistry.GetEMailAuth: eAuthTressEmail;
begin
     // Result := StrToIntDef (oZetaProvider.LeerClaves(CLAVE_AUTENTIFICACION_CORREO), 1);
     Result := eAuthTressEmail ( StrToIntDef (oZetaProvider.LeerClaves(CLAVE_AUTENTIFICACION_CORREO), 1) - 1);
end;

function TSCRegistry.GetEMailServer: String;
begin
     Result := oZetaProvider.LeerClaves(CLAVE_SERVIDOR_CORREOS);
end;

function TSCRegistry.GetEMailUser: String;
begin
     Result := oZetaProvider.LeerClaves(CLAVE_USER_ID);
end;

function TSCRegistry.GetDirectorioReportes: String;
begin
     Result := oZetaProvider.LeerClaves(CLAVE_DIRECTORIO_SERVICIO_REPORTES);
end;

function TSCRegistry.GetDirectorioImagenes: String;
begin
     Result := oZetaProvider.LeerClaves(CLAVE_DIRECTORIO_IMAGENES);
end;

function TSCRegistry.GetURLImagenes: String;
begin
     Result := oZetaProvider.LeerClaves(CLAVE_URL_IMAGENES);
end;

function TSCRegistry.GetExpiracionImagenes: Integer;
begin
     Result := StrToIntDef (oZetaProvider.LeerClaves(CLAVE_EXPIRACION_IMAGENES), 30); // �Default 30 aqu�? �O es mejor cero?
end;

{ ***** TServicioCorreosRegistryServer **** }

function TServicioCorreosRegistryServer.GetServicioCorreosServerDependency: string;
begin
     Result := Registry.ReadString( '', ServicioCorreos_SERVER_DEPENDENCY, MSSQL_DEFAULT_DEPENDENCY );
end;

procedure TServicioCorreosRegistryServer.SetServicioCorreosServerDependency( const Value: string);
begin
     Registry.WriteString( '', ServicioCorreos_SERVER_DEPENDENCY, Value );
end;

end.
