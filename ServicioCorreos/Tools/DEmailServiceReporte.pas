unit DEmailServiceReporte;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
        IniFiles, Registry,
        IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
        IdMessageClient, IdSMTP, IdMessage, IdEMailAddress,
        ZetaCommonLists, IdExplicitTLSClientServerBase, IdSMTPBase,
     IdAttachmentFile, IdAttachment, IdMessageParts, IdText
     {$ifdef PROFILE},ZetaProfiler{$endif};

const
     K_EMAIL_PORT = 25;

type
  TdmEmailServiceReporte = class(TDataModule)
          procedure DataModuleCreate(Sender: TObject);
          procedure DataModuleDestroy(Sender: TObject);
  private
          { Private declarations }
          FIniFile: TIniFile;
          FRegistry: TRegistry;
          FLista: TStrings;
          FSubType: eEmailType;
          FPuerto: Integer;
          FAuthMethod : eAuthTressEmail;
          FTimeOut: Integer;
          FMailServer: String;
          FReplyToName: String;
          FReplyToAddress: String;
          FUser: String;
          FPassWord: String;
          FSubject: String;
          FFromName: String;
          FFromAddress: String;
          FMessageText: TStrings;
          FAttachments: TStrings;
          // FAttachments: String;
          FToCarbonCopy: TStrings;
          FToBlindCarbonCopy: TStrings;
          FToAddress: TStrings;
          FMensaje: TIDMessage;
          FReceiptRecipientAddress: String;
          FReceiptRecipientName: String;
          FRegistryKey: String;
          FDirectorioReportes: String;
          FDirectorioImagenes: String;
          FURLImagenes: String;
          FExpiracionImagenes: Integer;
          {$ifdef PROFILE}
          emailProfiler : TZetaProfiler;
          {$endif}
          FEMailSMTP: TIdSMTP;
  protected
          { Protected declaractions }
          function GetFileName(const sFileName: String): String;
          function GetKeyName(const sKeyName: String): String;
          function MailServerConnect(var sErrorMsg: String): Boolean;
          function MailServerDisconnect(var sErrorMsg: String): Boolean;
          {procedure BuildAddressList(Lista: TStrings;
            Direcciones: TIdEmailAddressList);}
          procedure InitRegistry;
          procedure ReadAddressSection(const sSeccion: String;
            Direcciones: TStrings);
          procedure ReadAddressKey(const sKey, sSubKey: String;
            Direcciones: TStrings);
          procedure ReadSectionFromRegistry(const sSectionKey,
            sSectionName, sKey: String; Lista: TStrings);
          procedure WriteAddressSection(const sSeccion,
            sSubSeccion: String; Direcciones: TStrings);
          procedure WriteAddressKey(const sKey, sSubKey: String;
            Direcciones: TStrings);
          procedure WriteSectionToRegistry(const sSectionKey,
            sSectionName, sKey: String; Lista: TStrings);
          procedure ConfiguracionSMTP;
  public
          { Public declarations }
          property Attachments: TStrings read FAttachments;
          // property Attachments: String read FAttachments write FAttachments;
          property FromAddress: String read FFromAddress
            write FFromAddress;
          property FromName: String read FFromName write FFromName;
          property MailServer: String read FMailServer write FMailServer;
          property MessageText: TStrings read FMessageText;
          property Password: String read FPassWord write FPassWord;
          property Port: Integer read FPuerto write FPuerto
            default K_EMAIL_PORT;
          property AuthMethod : eAuthTressEmail read FAuthMethod write FAuthMethod;
          property ReplyToName: String read FReplyToName
            write FReplyToName;
          property ReplyToAddress: String read FReplyToAddress
            write FReplyToAddress;
          property ReceiptRecipientName: String read FReceiptRecipientName
            write FReceiptRecipientName;
          property ReceiptRecipientAddress: String
            read FReceiptRecipientAddress write FReceiptRecipientAddress;
          property RegistryKey: String read FRegistryKey
            write FRegistryKey;
          property Subject: String read FSubject write FSubject;
          property SubType: eEmailType read FSubType write FSubType;
          property TimeOut: Integer read FTimeOut write FTimeOut;
          property ToAddress: TStrings read FToAddress;
          property ToBlindCarbonCopy: TStrings read FToBlindCarbonCopy;
          property ToCarbonCopy: TStrings read FToCarbonCopy;
          property User: String read FUser write FUser;
          property DirectorioReportes: String read FDirectorioReportes
            write FDirectorioReportes;
          property DirectorioImagenes: String read FDirectorioImagenes
            write FDirectorioImagenes;
          property URLImagenes: String read FURLImagenes
            write FURLImagenes;
          property ExpiracionImagenes: Integer read FExpiracionImagenes
            write FExpiracionImagenes;
          function ReadFromIniFile(const sFileName: String): Boolean;
          function ReadFromRegistry(const sKeyName: String): Boolean;
          function SendEmail( var sErrorMsg : string; bCambiarNombreAdjunto: boolean; idMsg: Integer = 0): Boolean;
          function Validate(var sErrorMsg: String): Boolean;
          function VerifyEmailAddresses(var sErrorMsg: String): Boolean;
          function WriteToIniFile(const sFileName: String): Boolean;
          function WriteToRegistry(const sKeyName: String): Boolean;
          procedure NewBodyMessage;
          procedure NewEmail;
          procedure CreateObjetoEMailSMTP;
          procedure DestroyObjetoEMailSMTP;
  end;

implementation

uses ZetaCommonTools, ZetaCommonClasses, IOUTils, Types, ZetaServerTools;

{$R *.DFM}

const
        K_SECCION = 'EmailParameters';
        K_SECCION_TO_ADDRESS = 'EmailToAddress';
        K_SECCION_CARBON_ADDRESS = 'EmailCarbonAddress';
        K_SECCION_BLIND_ADDRESS = 'EmailBlindAddress';

        K_TO_ADDRESS = 'ToAddress%d';
        K_CARBON_ADDRESS = 'CarbonAdresss%d';
        K_BLIND_ADDRESS = 'BlindAdress%d';

        K_MAILSERVER = 'Host';
        K_PORT = 'Port';
        K_SUBTYPE = 'Type';
        K_USER = 'UserId';
        K_PASSWORD = 'PassWord';
        K_FROMADDRESS = 'Remitente';
        K_FROMNAME = 'FromName';
        K_REPLYTO = 'ReplyTo';
        K_RECEIPT = 'ReceiptRecipient';
        K_TIMEOUT = 'TimeOut';

        K_DEF_MAILSERVER = '255.255.255.255';
        K_DEF_PORT = K_EMAIL_PORT;
        K_DEF_SUBTYPE = Ord(emtTexto);
        K_DEF_USER = 'Usuario';
        K_DEF_PASSWORD = 'PassWord';
        K_DEF_FROMADDRESS = 'usuario@dominio.com';
        K_DEF_FROMNAME = 'Nombre del Usuario';
        K_DEF_REPLYTO = 'usuario@dominio.com';
        K_DEF_RECEIPT = '';
        K_DEF_TIMEOUT = 0;
        K_DEFAULT_ENTERO = 0;

        { ******** TdmEmailService ******* }

procedure TdmEmailServiceReporte.CreateObjetoEMailSMTP;
begin
     try
        if not Assigned( FEMailSMTP ) then
        begin
             FEMailSMTP := TIdSMTP.Create(Self);
             ConfiguracionSMTP;
        end;
     except
           on Error: Exception do
           begin
                FEMailSMTP := TIdSMTP.Create(Self);
                ConfiguracionSMTP;
           end;
     end;
end;

procedure TdmEmailServiceReporte.DestroyObjetoEMailSMTP;
begin
     try
        if Assigned( FEMailSMTP ) then
        begin
             FreeAndNil(FEMailSMTP);
        end;
     except
           on Error: Exception do
           begin
                FEMailSMTP := nil;
           end;
     end;
end;

procedure TdmEmailServiceReporte.ConfiguracionSMTP;
begin
     FEMailSMTP.AuthType := satDefault;
     FEMailSMTP.HeloName := VACIO;
     FEMailSMTP.Host := VACIO;
     FEMailSMTP.MailAgent := VACIO;
     FEMailSMTP.Password := VACIO;
     FEMailSMTP.PipeLine := False;
     FEMailSMTP.Port := K_EMAIL_PORT;
     FEMailSMTP.Tag := K_DEFAULT_ENTERO;
     FEMailSMTP.UseEhlo := True;
     FEMailSMTP.Username := VACIO;
     FEMailSMTP.UseTLS := utNoTLSSupport;
     FEMailSMTP.UseVerp := False;
     FEMailSMTP.ValidateAuthLoginCapability := True;
     FEMailSMTP.VerpDelims := VACIO;
     FEMailSMTP.AuthType := satNone;
end;

procedure TdmEmailServiceReporte.DataModuleCreate(Sender: TObject);
const
     H_KEY_SOFTWARE = 'Software\Grupo Tress\TressWin\';
begin
     CreateObjetoEMailSMTP;
     FMessageText := TStringList.Create;
     FToAddress := TStringList.Create;
     FToCarbonCopy := TStringList.Create;
     FToBlindCarbonCopy := TStringList.Create;
     FAttachments := TStringList.Create;
     // FAttachments := VACIO;
     FLista := TStringList.Create;
     FMensaje := TIDMessage.Create(FEMailSMTP);
     FRegistryKey := H_KEY_SOFTWARE;

     {$ifdef PROFILE}
     emailProfiler :=  TZetaProfiler.Create;
     emailProfiler.Start;
     emailProfiler.Check('TdmEmailServiceReporte.DataModuleCreate');
     {$endif}
end;

procedure TdmEmailServiceReporte.DataModuleDestroy(Sender: TObject);
begin
        if Assigned(FRegistry) then
           FreeAndNil(FRegistry);
        FreeAndNil(FMensaje);
        FreeAndNil(FLista);
        // FreeAndNil( FAttachments );
        FreeAndNil(FToBlindCarbonCopy);
        FreeAndNil(FToCarbonCopy);
        FreeAndNil(FToAddress);
        FreeAndNil(FMessageText);

        {$ifdef PROFILE} FreeAndNil( emailProfiler ); {$endif}
        DestroyObjetoEMailSMTP;
end;

procedure TdmEmailServiceReporte.InitRegistry;
begin
     if not Assigned(FRegistry) then
     begin
          FRegistry := TRegistry.Create;
          with FRegistry do
          begin
               RootKey := HKEY_LOCAL_MACHINE;
          end;
     end;
end;

function TdmEmailServiceReporte.GetKeyName(const sKeyName: String): String;
begin
        Result := FRegistryKey + sKeyName;
end;

function TdmEmailServiceReporte.GetFileName(const sFileName: String): String;
begin
        if ZetaCommonTools.StrVacio(SysUtils.ExtractFileDir(sFileName)) then
                Result := ZetaCommonTools.VerificaDir
                  (ExtractFileDir(Application.ExeName)) + sFileName
        else
                Result := sFileName;
end;

procedure TdmEmailServiceReporte.ReadAddressSection(const sSeccion: String;
  Direcciones: TStrings);
var
        i: Integer;
begin
        with FLista do
        begin
                Clear;
                FIniFile.ReadSectionValues(sSeccion, FLista);
                for i := 0 to (Count - 1) do
                begin
                        Direcciones.Add(Values[Names[i]]);
                end;
        end;
end;

procedure TdmEmailServiceReporte.WriteAddressSection(const sSeccion,
  sSubSeccion: String; Direcciones: TStrings);
var
        i: Integer;
begin
        with FIniFile do
        begin
                EraseSection(sSeccion);
                for i := 0 to (Direcciones.Count - 1) do
                begin
                        WriteString(sSeccion, Format(sSubSeccion, [i]),
                          Direcciones[i]);
                end;
        end;
end;

procedure TdmEmailServiceReporte.WriteAddressKey(const sKey, sSubKey: String; Direcciones: TStrings);
var
   i: Integer;
begin
     with FRegistry do
     begin
          if OpenKey(sKey, True) then
          begin
               for i := 0 to (Direcciones.Count - 1) do
               begin
                    WriteString(Format(sSubKey, [i]), Direcciones[i]);
               end;
          end;
          CloseKey;
     end;
end;

procedure TdmEmailServiceReporte.ReadAddressKey(const sKey, sSubKey: String;
  Direcciones: TStrings);
var
        Llave: TRegKeyInfo;
        i: Integer;
begin
        with FRegistry do
        begin
                if OpenKey(sKey, FALSE) and GetKeyInfo(Llave) then
                begin
                        for i := 0 to (Llave.NumValues - 1) do
                        begin
                                Direcciones.Add
                                  (ReadString(Format(sSubKey, [i])));
                        end;
                        CloseKey;
                end;
        end;
end;

function TdmEmailServiceReporte.ReadFromIniFile(const sFileName
  : String): Boolean;
begin
        NewEmail;
        FIniFile := TIniFile.Create(GetFileName(sFileName));
        try
                with FIniFile do
                begin
                        MailServer := ReadString(K_SECCION, K_MAILSERVER,
                          K_DEF_MAILSERVER);
                        Port := StrToInt(ReadString(K_SECCION, K_PORT,
                          IntToStr(K_DEF_PORT)));
                        SubType :=
                          eEmailType(StrToInt(ReadString(K_SECCION, K_SUBTYPE,
                          IntToStr(K_DEF_SUBTYPE))));
                        User := ReadString(K_SECCION, K_USER, K_DEF_USER);
                        Password := ReadString(K_SECCION, K_PASSWORD,
                          K_DEF_PASSWORD);
                        FromAddress := ReadString(K_SECCION, K_FROMADDRESS,
                          K_DEF_FROMADDRESS);
                        FromName := ReadString(K_SECCION, K_FROMNAME,
                          K_DEF_FROMNAME);
                        ReplyToName := ReadString(K_SECCION, K_REPLYTO,
                          K_DEF_REPLYTO);
                        ReplyToAddress := ReplyToName;
                        ReceiptRecipientName := ReadString(K_SECCION, K_RECEIPT,
                          K_DEF_RECEIPT);
                        ReceiptRecipientAddress := ReceiptRecipientName;
                        TimeOut :=
                          StrToInt(ReadString(K_SECCION, K_TIMEOUT,
                          IntToStr(K_DEF_TIMEOUT)));

                        ReadAddressSection(K_SECCION_TO_ADDRESS, FToAddress);
                        ReadAddressSection(K_SECCION_CARBON_ADDRESS,
                          FToCarbonCopy);
                        ReadAddressSection(K_SECCION_BLIND_ADDRESS,
                          FToBlindCarbonCopy);

                end;
                Result := True;
        finally
                FreeAndNil(FIniFile);
        end;
end;

function TdmEmailServiceReporte.WriteToIniFile(const sFileName: String): Boolean;
begin
     FIniFile := TIniFile.Create(GetFileName(sFileName));
     try
        with FIniFile do
        begin
             WriteString(K_SECCION, K_MAILSERVER, MailServer);
             WriteString(K_SECCION, K_PORT, IntToStr(Port));
             WriteString(K_SECCION, K_SUBTYPE,
               IntToStr(Ord(SubType)));
             WriteString(K_SECCION, K_USER, User);
             WriteString(K_SECCION, K_PASSWORD, Password);
             WriteString(K_SECCION, K_FROMADDRESS, FromAddress);
             WriteString(K_SECCION, K_FROMNAME, FromName);
             WriteString(K_SECCION, K_REPLYTO, ReplyToAddress);
             WriteString(K_SECCION, K_RECEIPT,
               ReceiptRecipientAddress);
             WriteString(K_SECCION, K_TIMEOUT, IntToStr(TimeOut));

             WriteAddressSection(K_SECCION_TO_ADDRESS, K_TO_ADDRESS,
               FToAddress);
             WriteAddressSection(K_SECCION_CARBON_ADDRESS,
               K_CARBON_ADDRESS, FToCarbonCopy);
             WriteAddressSection(K_SECCION_BLIND_ADDRESS,
               K_BLIND_ADDRESS, FToBlindCarbonCopy);
        end;

        Result := True;
     finally
            FreeAndNil(FIniFile);
     end;
end;

procedure TdmEmailServiceReporte.ReadSectionFromRegistry(const sSectionKey,
  sSectionName, sKey: String; Lista: TStrings);
begin
        with FRegistry do
        begin
                ReadAddressKey(sSectionKey + '\' + sSectionName, sKey, Lista);
        end;
end;

procedure TdmEmailServiceReporte.WriteSectionToRegistry(const sSectionKey,
  sSectionName, sKey: String; Lista: TStrings);
begin
        with FRegistry do
        begin
                ReadAddressKey(sSectionKey + '\' + sSectionName, sKey, Lista);
        end;
end;

function TdmEmailServiceReporte.ReadFromRegistry(const sKeyName: String): Boolean;
var
   sKey: String;
begin
     NewEmail;
     InitRegistry;
     with FRegistry do
     begin
          sKey := GetKeyName(sKeyName);
          Result := OpenKey(sKey, FALSE);
          if Result then
          begin
               MailServer := ReadString(K_MAILSERVER);
               Port := ReadInteger(K_PORT);
               SubType := eEmailType(ReadInteger(K_SUBTYPE));
               User := ReadString(K_USER);
               Password := ReadString(K_PASSWORD);
               FromAddress := ReadString(K_FROMADDRESS);
               FromName := ReadString(K_FROMNAME);
               ReplyToName := ReadString(K_REPLYTO);
               ReplyToAddress := ReplyToName;
               ReceiptRecipientName := ReadString(K_RECEIPT);
               ReceiptRecipientAddress := ReceiptRecipientName;
               TimeOut := ReadInteger(K_TIMEOUT);
               ReadSectionFromRegistry(sKey, K_SECCION_TO_ADDRESS,
                 K_TO_ADDRESS, FToAddress);
               ReadSectionFromRegistry(sKey, K_SECCION_CARBON_ADDRESS,
                 K_CARBON_ADDRESS, FToCarbonCopy);
               ReadSectionFromRegistry(sKey, K_SECCION_BLIND_ADDRESS,
                 K_BLIND_ADDRESS, FToBlindCarbonCopy);
          end;
          CloseKey;
     end;
end;

function TdmEmailServiceReporte.WriteToRegistry(const sKeyName: String): Boolean;
var
   sKey: String;
begin
     InitRegistry;
     with FRegistry do
     begin
          sKey := GetKeyName(sKeyName);
          Result := OpenKey(sKey, True);
          if Result then
          begin
               WriteString(K_MAILSERVER, MailServer);
               WriteInteger(K_PORT, Port);
               WriteInteger(K_SUBTYPE, Ord(SubType));
               WriteString(K_USER, User);
               WriteString(K_PASSWORD, Password);
               WriteString(K_FROMADDRESS, FromAddress);
               WriteString(K_FROMNAME, FromName);
               WriteString(K_REPLYTO, ReplyToAddress);
               WriteString(K_RECEIPT, ReceiptRecipientAddress);
               WriteInteger(K_TIMEOUT, TimeOut);
               WriteSectionToRegistry(sKey, K_SECCION_TO_ADDRESS,
                 K_TO_ADDRESS, FToAddress);
               WriteSectionToRegistry(sKey, K_SECCION_CARBON_ADDRESS,
                 K_CARBON_ADDRESS, FToCarbonCopy);
               WriteSectionToRegistry(sKey, K_SECCION_BLIND_ADDRESS,
                 K_BLIND_ADDRESS, FToBlindCarbonCopy);
          end;
          CloseKey;
     end;
end;

function TdmEmailServiceReporte.VerifyEmailAddresses(var sErrorMsg
  : String): Boolean;
begin
        Result := True;
end;

function TdmEmailServiceReporte.Validate(var sErrorMsg: String): Boolean;
begin
        Result := True;
end;

procedure TdmEmailServiceReporte.NewBodyMessage;
begin
     Self.ToAddress.Clear;
     Self.ToCarbonCopy.Clear;
     Self.ToBlindCarbonCopy.Clear;
     Self.MessageText.Clear;
     Self.Attachments.Clear;
     // Self.Attachments := VACIO;
     Subject := VACIO;
end;

procedure TdmEmailServiceReporte.NewEmail;
begin
     MailServer := VACIO;
     Port := K_EMAIL_PORT;
     SubType := emtTexto;
     User := VACIO;
     Password := VACIO;
     FromAddress := VACIO;
     FromName := VACIO;
     ReplyToName := VACIO;
     ReplyToAddress := VACIO;
     ReceiptRecipientName := VACIO;
     ReceiptRecipientAddress := VACIO;
     TimeOut := 0;
     NewBodyMessage;
     AuthMethod   := authSinLogin;
end;

{procedure TdmEmailServiceReporte.BuildAddressList(Lista: TStrings;
  Direcciones: TIdEmailAddressList);
var
   i: Integer;
   sDireccion: String;
begin
      with Lista do
      begin
           for i := 0 to (Count - 1) do
           begin
                sDireccion := Strings[i];
                if ZetaCommonTools.StrLleno(sDireccion) then
                begin
                     with Direcciones.Add do
                     begin
                          Name := sDireccion;
                          Address := sDireccion;
                     end;
                end;
           end;
      end;
end;}

{procedure TdmEmailServiceReporte.ConstruirMensaje;
begin
     with FMensaje do
     begin
          Clear;
          UseNowForDate := True;
          case Self.SubType of
                  emtTexto:
                          ContentType := 'text/plain';
                  emtHTML:
                          ContentType := 'text/html';
          end;
          with From do
          begin
                  Name := Self.FromName;
                  Address := Self.FromAddress;
          end;
          with Sender do
          begin
                  Name := Self.FromName;
                  Address := Self.FromAddress;
          end;
          with ReplyTo.Add do
          begin
                  Name := Self.ReplyToName;
                  Address := Self.ReplyToAddress;
          end;
          with ReceiptRecipient do
          begin
                  Name := Self.ReceiptRecipientName;
                  Address := Self.ReceiptRecipientAddress;
          end;
          Subject := Self.Subject;
          Body.AddStrings(Self.MessageText);
          BuildAddressList(Self.ToAddress, Recipients);
          BuildAddressList(Self.ToBlindCarbonCopy, BCCList);
          BuildAddressList(Self.ToCarbonCopy, CCList);
          BuildAttachmentList( Self.Attachments, MessageParts );
     end;
end;}

function TdmEmailServiceReporte.MailServerDisconnect(var sErrorMsg: String): Boolean;
begin
     {$ifdef PROFILE}emailProfiler.Check('MailServerDisconnect inicio');{$endif}
     Result := FALSE;
     with FEMailSMTP do
     begin
          try
             // **********************************************************
             // No forzar desconexi�n.
             if Connected then
             begin
                  {$ifdef PROFILE}emailProfiler.Check('MailServerDisconnect eMailSMTP Conectado. Iniciar desconexi�n.');{$endif}
                  DisConnect;
                  Result := True;
                  {$ifdef PROFILE}emailProfiler.Check('MailServerDisconnect eMailSMTP Desconectado.');{$endif}
             end
             else
             begin
                  {$ifdef PROFILE}emailProfiler.Check('MailServerDisconnect eMailSMTP ya desconectado.');{$endif}
                  Result := TRUE;
             end;
             // **********************************************************
          except
                on Error: Exception do
                begin
                     Result := FALSE;
                     {$ifdef PROFILE}emailProfiler.Check('MailServerDisconnect Error: ' + Error.Message);{$endif}
                     // sErrorMsg := Error.Message;
                     sErrorMsg := 'Servicio de correos {1} Error al desconectar servidor: ' + CR_LF + Error.Message;
                end;
          end;
     end;
     {$ifdef PROFILE}emailProfiler.Check('MailServerDisconnect fin');{$endif}
end;

function TdmEmailServiceReporte.MailServerConnect(var sErrorMsg: String): Boolean;
begin
     {$ifdef PROFILE}emailProfiler.Check('MailServerConnect inicio');{$endif}
     Result := MailServerDisconnect(sErrorMsg);
     if Result then
     begin
          {$ifdef PROFILE}emailProfiler.Check('eMailSMTP desconectado.');{$endif}
          with FEMailSMTP do
          begin
               {$ifdef PROFILE}emailProfiler.Check('eMailSMTP asignando propiedades.');{$endif}
               Host := Self.MailServer;
               UserName := Self.User;
               // Password := Self.Password;
               Password := ZetaServerTools.Decrypt (Self.Password);
               Port := Self.Port;

               if AuthMethod = authLogin then
                  AuthType := satDefault
               else
                   AuthType := satNone;

               try
                  {$ifdef PROFILE}emailProfiler.Check('eMailSMTP intentando conexi�n.');{$endif}
                  Connect;
                  {$ifdef PROFILE}emailProfiler.Check(Format ('eMailSMTP Result := Connected ---> %s.', [BoolToSiNo(Connected)]));{$endif}
                  Result := Connected;
               except
                     on Error: Exception do
                     begin
                          Result := FALSE;

                          {$ifdef PROFILE}emailProfiler.Check(Format ('eMailSMTP Result := Connected ---> %s.', [BoolToSiNo(Connected)]));{$endif}

                          // Si Error.Message es vac�o, indicar problema en conexi�n al servidor de correos.
                          if Error.Message <> VACIO then
                          begin
                               {$ifdef PROFILE}emailProfiler.Check('MailServerConnect error: ' + Error.Message);{$endif}
                               sErrorMsg := 'Servicio de correos {2} Error al conectar con servidor: ' + CR_LF + Error.Message;
                          end
                          else
                          begin
                               sErrorMsg :=
                                         Format ('No es posible establecer conexi�n con el servidor de correos. Servidor: %s', [Self.MailServer]);
                               {$ifdef PROFILE}emailProfiler.Check('MailServerConnect error: ' + sErrorMsg);{$endif}
                          end;
                     end;
               end;
          end;
     end;
     {$ifdef PROFILE}emailProfiler.Check('MailServerConnect fin');{$endif}
end;

function TdmEmailServiceReporte.SendEmail (var sErrorMsg : string; bCambiarNombreAdjunto: boolean; idMsg: Integer): Boolean;
Const K_CCG = 'Connection closed gracefully';
var
  // idtTextPart:TIdMessage;
  IdPostMessage : TIdMessage;
  idtAttach : TIdAttachment;
  idtEmail : TIdEMailAddressItem;
  lTextPArt : TIdText;
  i : integer;
  bHacerDesc: Boolean;

  procedure AgregarBodyHTML;
  begin
       IdPostMessage.Body.Clear;
       lTextPart := TIdText.Create(IdPostMessage.MessageParts);
       lTextPart.Body.Text := FMessageText.Text;
       lTextPart.ContentType := 'text/html';
       IdPostMessage.Charset := 'UTF-8'
  end;

  procedure AgregarBodyTextPlain;
  begin
       IdPostMessage.Body.Clear;
       IdPostMessage.Body :=  FMessageText;
       IdPostMessage.ContentType := 'text/plain';
       IdPostMessage.Charset := 'UTF-8'
  end;

  procedure AgregarAttachments (bCambiarNombreAdjunto: boolean);
  const K_SEPARADOR = '_';
        K_FORMATO_GRAFICO = '.BMP,.EMF,.JPG,.PNG,.SVG';
  var
     iAttach : integer;
     sExtensi�n: String;
     iIndexReportesImagen: Integer;

     function esReporteEnImagen (sExt: String): boolean;
     begin
          Result := FALSE;
          if Pos (UpperCase (sExt), K_FORMATO_GRAFICO) > 0 then
             Result := TRUE;
     end;

  begin
       iIndexReportesImagen := 0;

       for iAttach := 0  to FAttachments.Count - 1 do
       begin
            if FileExists (DirectorioReportes + '\' + SysUtils.ExtractFileName(FAttachments[iAttach])) then
            begin
                 idtAttach := TIdAttachmentFile.Create(IdPostMessage.MessageParts, DirectorioReportes + '\' + SysUtils.ExtractFileName (FAttachments[iAttach]));

                 // Cambiar el nombre para eliminar los caracteres despu�s
                 // de la �ltima aparici�n de gui�n bajo (incluy�ndolo).
                 // Cambiar nombre del adjunto.
                 if (bCambiarNombreAdjunto) and (SysUtils.ExtractFileName(FAttachments[iAttach]).LastDelimiter (K_SEPARADOR) > 0) then
                 begin
                      // Entonces, renombrar archivo adjunto.
                      sExtensi�n := SysUtils.ExtractFileExt (FAttachments[iAttach]);

                      if esReporteEnImagen (SysUtils.ExtractFileExt(FAttachments[iAttach])) then
                      begin
                           idtAttach.FileName :=  Copy(idtAttach.FileName, 0, idtAttach.FileName.LastDelimiter (K_SEPARADOR) )
                                              + IntToStr (iIndexReportesImagen + 1) + sExtensi�n;
                           iIndexReportesImagen := iIndexReportesImagen + 1;
                      end
                      else
                      begin
                           idtAttach.FileName :=  Copy(idtAttach.FileName, 0, idtAttach.FileName.LastDelimiter (K_SEPARADOR) ) + sExtensi�n;
                      end;
                 end;
            end
            else
            begin
                 sErrorMsg := Format ('No existe archivo %s.', [DirectorioReportes + '\' + SysUtils.ExtractFileName(Attachments[iAttach])]);
            end;
       end;
  end;

begin
     {$ifdef PROFILE}
     emailProfiler.Clear;
     emailProfiler.Check('TdmEmailServiceReporte.SendEmail inicio');
     {$endif}
     Result := FALSE;
     bHacerDesc := FALSE;

     CreateObjetoEMailSMTP;
     with FEMailSMTP do
     begin
          if not MailServerConnect( sErrorMsg ) then
          begin
               {$ifdef PROFILE}emailProfiler.Check('SendEmail eMail SMTP no conectado al servidor.');{$endif}
               Host := FMailServer;
               Username := FUser;
               Password := ZetaServerTools.Decrypt (FPassWord);
               Port := FPuerto;

               if AuthMethod = authLogin then
                  AuthType := satDefault
               else
                   AuthType := satNone;

               {$ifdef PROFILE}emailProfiler.Check('SendEmail eMail SMTP se asignaron propiedades.');{$endif}

               // ************************************************************
               // Intentar conexi�n de nuevo.
               try
                  {$ifdef PROFILE}emailProfiler.Check('SendEmail eMail SMTP intentando conexi�n.');{$endif}
                  Connect;
               except
                     On Error: Exception do
                     begin
                          // sErrorMsg := Error.Message;
                          {$ifdef PROFILE}emailProfiler.Check('SendEmail eMail SMTP error al intentar conexi�n: ' + Error.Message);{$endif}
                     end;
               end;
               // ************************************************************

          end;

          if Connected then
          begin
               {$ifdef PROFILE}emailProfiler.Check('SendEmail eMail SMTP conectado.');{$endif}
               IdPostMessage  := TIdMessage.Create(Self);
               try
                  IdPostMessage.Clear;
                  IdPostMessage.ClearBody;
                  IdPostMessage.ClearHeader;
                  IdPostMessage.Encoding := meMIME;

                  IdPostMessage.Recipients.Clear;
                  IdPostMessage.CCList.Clear;
                  IdPostMessage.BccList.Clear;
                  IdPostMessage.Date := 0;
                  IdPostMessage.From.Address := FFromAddress ;
                  IdPostMessage.From.Name := FFromName;

                  for i:=0 to FToAddress.Count-1 do
                  begin
                       if StrLLeno( FToAddress[i] ) then
                       begin
                            idtEmail := IdPostMessage.Recipients.Add;
                            idtEmail.Address := FToAddress[i];
                       end;
                  end;

                  if IdPostMessage.Recipients.Count > 0 then
                  begin
                       for i:=0 to FToBlindCarbonCopy.Count-1 do
                       begin
                           IdPostMessage.CCList.Add.Address := FToBlindCarbonCopy[i];
                           IdPostMessage.BccList.Add.Address := FToBlindCarbonCopy[i];
                       end;

                       IdPostMessage.Subject := FSubject;

                       case FSubType of
                            emtTexto: AgregarBodyTextPlain;
                            emtHTML: AgregarBodyHTML;
                            emtTextoError: AgregarBodyTextPlain;
                       end;

                       //if FSubType <> emtHTML then
                       AgregarAttachments (bCambiarNombreAdjunto);

                       // if sErrorMsg = VACIO then
                       if (sErrorMsg = VACIO) or
                          (Pos (UpperCase (K_CCG), UpperCase (sErrorMsg)) > 0) then
                       begin
                            {$ifdef PROFILE}emailProfiler.Check('SendEmail no hay error. Se intenta enviar correo.');{$endif}
                            try
                               // Poner el Send en un try/except.
                               try
                                  Send( IdPostMessage );
                                  {$ifdef PROFILE}emailProfiler.Check('SendEmail Correo enviado.');{$endif}
                                  Result := TRUE;
                                  bHacerDesc := TRUE;
                               except
                                     on ErrorSend: Exception do
                                     begin
                                          {$ifdef PROFILE}emailProfiler.Check('    SendEmail Error en m�todo smtp Send: ' + ErrorSend.Message);{$endif}
                                          if Pos (UpperCase (K_CCG), UpperCase (ErrorSend.Message)) = 0 then
                                          begin
                                               Result := FALSE;
                                               sErrorMsg := 'Servicio de correos {3} Error al intentar enviar correo: ' + CR_LF + ErrorSend.Message;
                                          end
                                          else
                                          begin
                                               bHacerDesc := TRUE;
                                          end;
                                     end;
                               end;

                               // Desconectar SMTP
                               // **********************************************************
                               // No forzar desconexi�n.
                               if bHacerDesc then
                               begin
                                    if Connected then
                                    begin
                                         {$ifdef PROFILE}emailProfiler.Check('   SendEmail eMailSMTP Conectado. Iniciar desconexi�n.');{$endif}
                                         DisConnect;
                                         {$ifdef PROFILE}emailProfiler.Check('   SendEmail eMailSMTP Desconectado.');{$endif}
                                    end
                                    else
                                    begin
                                         {$ifdef PROFILE}emailProfiler.Check('   SendEmail eMailSMTP ya desconectado.');{$endif}
                                    end;
                               end;
                               // **********************************************************
                            except
                                  On Error2: Exception do
                                  begin
                                       {$ifdef PROFILE}emailProfiler.Check('SendEmail error al intentar enviar correo: ' + Error2.Message);{$endif}
                                       if Pos (UpperCase (K_CCG), UpperCase (Error2.Message)) = 0 then
                                       begin
                                            Result := FALSE;
                                            sErrorMsg := 'Servicio de correos {4} Error al intentar enviar correo: ' + CR_LF + Error2.Message;
                                       end;
                                  end;
                            end;
                       end;
                  end;
               finally
                      FreeAndNil (IdPostMessage);
                      DestroyObjetoEMailSMTP;
               end;
          end;
     end;

    {$ifdef PROFILE}emailProfiler.Check('TdmEmailServiceReporte.SendEmail fin');{$endif}
    {$ifdef PROFILE}emailProfiler.Report( Format( DirectorioReportes + '\EmailProfile_Correo_%d.LOG', [idMsg]) ); {$endif}
end;

end.
