unit ZetaMsgDlg;

interface

{$R *.RES}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, CommDlg,
     ZetaCommonClasses, ZetaCommonLists, cxButtons, Registry;

{$INCLUDE JEDI.INC}

type
  TZDialogo = class(TComponent)
  private
    { Private declarations }
    {$IFDEF EVENT_LOG}
    FEventLog: TZetaEventLog;
    {$ENDIF}
    FCaption: String;
    FMensaje: String;
    FTipo: TMsgDlgType;
    FBotones: TMsgDlgButtons;
    FHelpCtx: Longint;
    FCol: Integer;
    FRow: Integer;
    FModalResult: TModalResult;
    FDefaultBoton: TMsgDlgBtn;
    function CreaDialogo: TForm;
    function DialogoPos: Integer;
    function GetAveCharSize( Canvas: TCanvas ): TPoint;
    function AsignaCaption( DlgType: TMsgDlgType ): String;
    //DevEx (by am):
    function AsignaName_DevEx(B: TMsgDlgBtn):String;
    function AsignaCaptionBoton_DevEx (B: TMsgDlgBtn):String;
    procedure AsignaImagenBoton (B: TMsgDlgBtn;boton_DevEx:TcxButton);
{$ifdef EVENT_LOG}
    procedure SetEventLog( Value: TZetaEventLog );
{$endif}
    procedure SetHelpCtx( const iValue: Integer );
    procedure HelpButtonClick( Sender: TObject );
  protected
    { Protected declarations }
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    { Public declarations }
    property Botones: TMsgDlgButtons read FBotones write FBotones;
    property Caption: String read FCaption write FCaption;
    property Col: Integer read FCol write FCol;
    property DefaultBoton: TMsgDlgBtn read FDefaultBoton write FDefaultBoton;
    {$IFDEF EVENT_LOG}
    property EventLog: TZetaEventLog read FEventLog write SetEventLog;
    {$ENDIF}
    property HelpCtx: Longint read FHelpCtx write SetHelpCtx;
    property Mensaje: String read FMensaje write FMensaje;
    property ModalResult: TModalResult read FModalResult;
    property Row: Integer read FRow write FRow;
    property Tipo: TMsgDlgType read FTipo write FTipo;

    function Execute: TModalResult;
    procedure Excepcion(const sMensaje: TBitacoraTexto; Problema: Exception);
    function GetStringWidth(textoOrigen:string; anchocaja:integer; fuente:TFont) : string;
  published
    { Published declarations }
  end;
  
function ConfirmaCambio( const sMensaje: String ): Boolean;


implementation

uses
  ZetaCommonTools;

var
  ZDialogo: TZDialogo;

function ConfirmaCambio(const sMensaje: String): Boolean;
begin
     if ( ZDialogo = nil ) then
     begin
          ZDialogo := TZDialogo.Create( Application ); // Application Debe Destruirlo Al Finalizar //
          with ZDialogo do
          begin
               Caption := 'ˇ Atención !';
               Tipo := mtWarning;
               Botones := [ mbOk, mbCancel ];
               DefaultBoton := mbCancel;
               HelpCtx := 0;
          end;
     end;
     with ZDialogo do
     begin
          Mensaje := sMensaje;
          Result := ( Execute = mrOk );
     end;
end;

{ **************** TZDialogo ***************** }

procedure TZDialogo.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  {$IFDEF EVENT_LOG}
  if (Operation = opRemove) then begin
    if (AComponent = EventLog) then
      EventLog := nil;
  end;
  {$ENDIF}
end;

{$IFDEF EVENT_LOG}
procedure TZDialogo.SetEventLog(Value: TZetaEventLog);
begin
  if (FEventLog <> Value) then begin
    FEventLog := Value;
    if (Value <> nil) then
      Value.FreeNotification(Self);
  end;
end;
{$ENDIF}

function TZDialogo.Execute: TModalResult;
begin
  FModalResult := DialogoPos;
  Result := ModalResult;
end;

procedure TZDialogo.Excepcion(const sMensaje: TBitacoraTexto; Problema: Exception);
begin
  {$IFDEF EVENT_LOG}
  if (EventLog <> nil) then begin
    EventLog.Excepcion(0, sMensaje, Problema);
  end;
  {$ENDIF}
end;

function TZDialogo.DialogoPos: Integer;
begin
  with CreaDialogo do begin
    try
      HelpContext := HelpCtx;
      if (FCol >= 0) then begin
        Left := FCol;
      end;
      if (FRow >= 0) then begin
        Top := FRow;
      end;
      Position := poScreenCenter;
      Chicharra;
      Result := ShowModal;
    finally
      Free;
    end;
  end;
end;

procedure TZDialogo.SetHelpCtx(const iValue: Integer);
begin
  FHelpCtx := iValue;
  if (HelpCtx > 0) and not(mbHelp in Botones) then begin
    Botones := Botones + [mbHelp];
  end;
end;

procedure TZDialogo.HelpButtonClick(Sender: TObject);
begin
  Application.HelpContext(FHelpCtx);
end;
function TZDialogo.GetStringWidth(textoOrigen:string; anchocaja:integer; fuente:TFont ) : string;
Var
BM:Tbitmap;
texto:string;
ancho:integer;
anchoborde:integer;
begin
  anchoborde:=0;
  texto:='';
  BM := TBitmap.Create;
  BM.Canvas.Font := Fuente;
  ancho := 25+trunc(BM.Canvas.TextWidth(textoOrigen))+8;
  while  anchoborde+ancho < anchocaja do
  begin
  texto := ' '+texto;
  anchoborde :=(BM.Canvas.TextWidth(texto+' ')*2);
  end;
 BM.Free;
 result:=texto+textoOrigen;
end;
function TZDialogo.CreaDialogo: TForm;
const
  mcHorzMargin    = 8;
  mcVertMargin    = 8;
  mcHorzSpacing   = 10;
  mcVertSpacing   = 10;
  mcButtonWidth   = 50;
  mcButtonHeight  = 16; // 14 //
  mcButtonSpacing = 4;
  Custom          = bkCustom;
  OK              = bkOK;
  Cancel          = bkCancel;
  Help            = bkHelp;
  Yes             = bkYes;
  No              = bkNo;
  Close           = bkClose;
  Abort           = bkAbort;
  Retry           = bkRetry;
  Ignore          = bkIgnore;
  All             = bkAll;
  YesToAll        = bkAll;
  aIconIDs: array [TMsgDlgType] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = (IDI_EXCLAMATION, IDI_HAND, IDI_ASTERISK,
    IDI_QUESTION, nil);
  {$IFNDEF DELPHIXE3_UP}
  aButtonNames: array [TMsgDlgBtn] of String = ('Yes', 'No', 'OK', 'Cancel', 'Abort', 'Retry',
    'Ignore', 'All', 'Help', 'SiATodos', '');
  aButtonCaptions: array [TMsgDlgBtn] of String = ('&Si', '&No', '&OK', '&Cancelar', 'A&bortar',
    '&Reintentar', '&Ignorar', '&Todos', '&Ayuda', 'Si a Todos', '');
  aModalResults: array [TMsgDlgBtn] of Integer = (mrYes, mrNo, mrOk, mrCancel, mrAbort, mrRetry,
    mrIgnore, mrAll, 0, mrYesToAll, 0);
  {$ELSE}
  aButtonNames: array [TMsgDlgBtn] of String = ('Yes', 'No', 'OK', 'Cancel', 'Abort', 'Retry',
    'Ignore', 'All', 'NoToAll', 'SiATodos', 'Help', 'Close');
  aButtonCaptions: array [TMsgDlgBtn] of String = ('&Si', '&No', '&OK', '&Cancelar', 'A&bortar',
    '&Reintentar', '&Ignorar', '&Todos', 'No a Todos', 'Si a Todos', '&Ayuda', 'Cerrar');
  aModalResults: array [TMsgDlgBtn] of Integer = (mrYes, mrNo, mrOk, mrCancel, mrAbort, mrRetry,
    mrIgnore, mrAll, mrNoToAll, mrYesToAll, idHelp, mrClose);
  {$ENDIF}
  aButtonsKinds: array [1 .. 13] of TBitBtnKind = (Retry, Custom, OK, Cancel, Help, Yes, No, Close,
    Abort, Retry, Ignore, All, YesToAll);
var
  DialogUnits: TPoint;
  HorzMargin, VertMargin, HorzSpacing, VertSpacing, ButtonWidth, ButtonHeight, ButtonSpacing,
    ButtonCount, ButtonGroupWidth, IconTextWidth, IconTextHeight, X: Integer;
  B, DefaultButton, CancelButton: TMsgDlgBtn;
  IconID: {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif};
  TextRect: TRect;
   Boton_DevEx: TcxButton;
begin
     Result := TForm.Create( Application );
     with Result do
     begin
          BorderStyle := bsDialog;
          Canvas.Font := Font;
          DialogUnits := GetAveCharSize( Canvas );
          HorzMargin := MulDiv( mcHorzMargin, DialogUnits.X, 3 );
          VertMargin := MulDiv( mcVertMargin, DialogUnits.Y, 8 );
          HorzSpacing := MulDiv( mcHorzSpacing, DialogUnits.X, 3 );
          VertSpacing := MulDiv( mcVertSpacing, DialogUnits.Y, 8 );
          ButtonWidth := MulDiv( mcButtonWidth, DialogUnits.X, 3 );
          ButtonHeight := MulDiv( mcButtonHeight, DialogUnits.Y, 8 );
          ButtonSpacing := MulDiv( mcButtonSpacing, DialogUnits.X, 3 );
          SetRect(TextRect, 0, 0, ( Screen.Width div 2 ), 0);
          DrawText( Canvas.Handle, {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}( FMensaje ), -1, TextRect, DT_CALCRECT OR DT_WORDBREAK );
          IconID := aIconIDs[ FTipo ];
          IconTextWidth := TextRect.Right;
          IconTextHeight := TextRect.Bottom;
          if ( IconID <> nil ) then
          begin
               Inc( IconTextWidth, ( 32 + HorzSpacing ) );
               if ( IconTextHeight < 32 ) then
                  IconTextHeight := 32;
          end;
          ButtonCount := 0;
          for B := Low( TMsgDlgBtn ) to High( TMsgDlgBtn ) do
          begin
               if ( B in FBotones ) then
                  Inc( ButtonCount );
          end;
          ButtonGroupWidth := 0;
          if ( ButtonCount <> 0 ) then
          begin
               ButtonGroupWidth := ButtonWidth * ButtonCount + ButtonSpacing * ( ButtonCount - 1 );
          end;
          ClientWidth := iMax( IconTextWidth, ButtonGroupWidth ) + HorzMargin * 2;
          ClientHeight := ( IconTextHeight + ButtonHeight + VertSpacing + VertMargin * 2 );
          Left := ( Screen.Width div 2 ) - ( Width div 2 );
          Top := ( Screen.Height div 2 ) - ( Height div 2 );
          if ( Length( FCaption ) = 0 ) then
             Caption := AsignaCaption( FTipo )
          else
              Caption := FCaption;
          if ( IconID <> nil ) then
          begin
               with TImage.Create( Result ) do
               begin
                    Name := 'Image';
                    Parent := Result;
                    //Se le suma 1 a FTipo porque en los archivos de recursos el primer indice es 1 no 0
                    Picture.Bitmap.LoadFromResourceID(hInstance, Ord(FTipo)+1 );
                    SetBounds( HorzMargin, VertMargin, 32, 32 );
               end;
          end;
          with TLabel.Create( Result ) do
          begin
               Name := 'Message';
               Parent := Result;
               WordWrap := True;
               Caption := FMensaje;
               Alignment := taCenter;
               BoundsRect := TextRect;
               SetBounds(IconTextWidth - TextRect.Right + HorzMargin, VertMargin,
               TextRect.Right, TextRect.Bottom);
          end;
          {
          if ( mbOk in FBotones ) then
             DefaultButton := mbOk
          else
              if ( mbYes in FBotones ) then
                 DefaultButton := mbYes
              else
                  DefaultButton := mbRetry;
          }
          DefaultButton := FDefaultBoton;
          if ( mbCancel in FBotones ) then
             CancelButton := mbCancel
          else
              if ( mbNo in FBotones ) then
                 CancelButton := mbNo
              else
                  CancelButton := mbOk;
          X := ( ClientWidth - ButtonGroupWidth ) div 2;

          for B := Low( TMsgDlgBtn ) to High( TMsgDlgBtn ) do
          begin
               if ( B in FBotones ) then
               begin
                         Boton_DevEx := TcxButton.Create( Result );
                          with Boton_DevEx do
                          begin
                               //Aplicar Skin
                               boton_DevEx.LookAndFeel.SkinName := 'TressMorado2013';
                               boton_DevEx.LookAndFeel.NativeStyle:= False;
                               boton_DevEx.Kind := cxbkStandard;
                               //boton_DevEx.Name := aButtonNames[ B ];  //old
                               boton_DevEx.Name := AsignaName_DevEx( B );
                               boton_DevEx.Parent := Result;
                               //Boton_DevEx.Caption := aButtonCaptions[ B ]; ///old
                               Boton_DevEx.Caption := AsignaCaptionBoton_DevEx( B );
                               boton_DevEx.OptionsImage.Margin:=1;
                               Boton_devex.Height:=26;
                               boton_DevEx.OptionsImage.Layout:= blGlyphLeft;

                               ModalResult := aModalResults[ B ];
                               //DevEx:Asigna la imagen en base al enumerado recibido
                               AsignaImagenBoton(B, boton_DevEx);

                               boton_DevEx.Caption:=GetStringWidth(boton_DevEx.Caption,buttonWidth,boton_DevEx.Font);
                               boton_DevEx.OptionsImage.Glyph.TransparentColor := clWhite;

                               if ( B = DefaultButton ) then
                               begin
                                    boton_DevEx.Default := True;
                               end;
                               if ( B = CancelButton ) then
                               begin
                                    boton_DevEx.Cancel := True;

                               end;
                               SetBounds( X, IconTextHeight + VertMargin + VertSpacing, ButtonWidth, 25 );
                               Inc( X, ( ButtonWidth + ButtonSpacing ) );
                               if ( B = mbHelp ) then
                               begin
                                    boton_DevEx.OnClick := TZDialogo( Result ).HelpButtonClick;
                                    //boton_DevEx.Caption := '&Ayuda'; //Old
                               end;
                          end;
                          if Boton_DevEx.Default then
                          begin
                               ActiveControl:= Boton_DevEx;
                          end;

               end;//EndIf
          end; //EndFor

     end;
end;

function TZDialogo.GetAveCharSize(Canvas: TCanvas): TPoint;
const
  A_MAYUSCULA = Ord('A');
  A_MINUSCULA = Ord('a');
var
  i: Integer;
  Buffer: array [0 .. 51] of Char;
begin
  for i := 0 to 25 do begin
    Buffer[i] := Chr(i + A_MAYUSCULA);
    Buffer[i + 26] := Chr(i + A_MINUSCULA);
  end;
  GetTextExtentPoint(Canvas.Handle, Buffer, 52, TSize(Result));
  with Result do begin
    X := X div 52;
  end;
end;

function TZDialogo.AsignaCaption(DlgType: TMsgDlgType): String;
begin
  case DlgType of
    mtWarning:
      Result := 'Advertencia';
    mtError:
      Result := 'Error';
    mtInformation:
      Result := 'Información';
    mtConfirmation:
      Result := 'Confirmación';
    else
      Result := '';
  end;
end;

//DevEx (by am): Metodos agregados para llevar un mejro control del nombre, textos e imagenes de los botones
function TZDialogo.AsignaName_DevEx(B: TMsgDlgBtn):String;
begin
     case B of
          mbYes: Result := 'Yes';
          mbNo: Result := 'No';
          mbCancel: Result := 'Cancel';
          mbOk: Result := 'OK';
          mbAbort: Result := 'Abort';
          mbIgnore: Result := 'Ignore';
          mbAll: Result := 'All';
          mbYesToAll : Result := 'SiATodos';
          mbRetry: Result := 'Retry';
          mbHelp: Result := 'Help';
     else
         Result := '';
     end;

end;

function TZDialogo.AsignaCaptionBoton_DevEx (B: TMsgDlgBtn):String;
begin
     case B of
          mbYes: Result := '&Si';
          mbNo: Result := '&No';
          mbCancel: Result := '&Cancelar';
          mbOk: Result := '&OK';
          mbAbort: Result := 'A&bortar';
          mbIgnore: Result := '&Ignorar';
          mbAll: Result := '&Todos';
          mbYesToAll : Result := 'Si a Todos';
          mbRetry: Result := '&Reintentar';
          mbHelp: Result := '&Ayuda';
     else
         Result := '';
     end;
end;

procedure TZDialogo.AsignaImagenBoton (B: TMsgDlgBtn; boton_DevEx:TcxButton);
begin
     case B of
          //mbYes: boton_DevEx.Glyph.LoadFromResourceName( HInstance,'OKGLYPH');
          mbNo,mbCancel,mbAbort:  boton_DevEx.Glyph.LoadFromResourceName( HInstance,'CancelGlyph');
          //mbCancel: boton_DevEx.Glyph.LoadFromResourceName( HInstance,'CancelGlyph');
          mbYes,mbOk, mbAll, mbYesToAll: boton_DevEx.Glyph.LoadFromResourceName( HInstance,'okGlyph');
          //mbAbort: boton_DevEx.Glyph.LoadFromResourceName( HInstance,'CancelGlyph');
          mbIgnore: boton_DevEx.Glyph.LoadFromResourceName( HInstance,'WarningGlyph');
          //mbAll:  boton_DevEx.Glyph.LoadFromResourceName( HInstance,'okGlyph');
          //mbYesToAll :  boton_DevEx.Glyph.LoadFromResourceName( HInstance,'okGlyph');
          mbRetry: boton_DevEx.Glyph.LoadFromResourceName( HInstance,'RefreshGlyph');
          mbHelp:  boton_DevEx.Glyph.LoadFromResourceName( HInstance,'HelpGlyph');
     end;
end;
end.
