unit ZetaTipoEntidadTools;


{$INCLUDE DEFINES.INC}

interface
uses SysUtils, Classes, DB,  ZetaCommonTools, ZetaTipoEntidad;

function EntidadValida( const Index: TipoEntidad ): Boolean;
function Dentro( const Entidad: TipoEntidad; Entidades: Array of TipoEntidad ): Boolean;
{$ifdef RDD|RDDTRESSCFG}
procedure LlenaArregloEntidades( DataSet: TDataset );
{$ELSE}
function ObtieneEntidad( const Index: TipoEntidad ): String;
{$ENDIF}


implementation

uses
  Variants;

function EntidadValida( const Index: TipoEntidad ): Boolean;
begin
     {$ifdef RDD|RDDTRESSCFG}
     //Siempre regresa TRUE, porque ahora hay un constraint de REPORTES hacia R_ENTIDAD
     //que no permite que haya Reportes de tablas que no existen.
     Result :=  TRUE;
     {$else}
{$WARNINGS OFF}
     Result :=  ( Index >=  Low(TipoEntidad) ) and
                ( Index <=  High(TipoEntidad) );
     {$endif}
{$WARNINGS ON}
end;

{$ifdef RDD|RDDTRESSCFG}
procedure LlenaArregloEntidades( DataSet: TDataset );
 var i: TipoEntidad;
begin
     for i:= low( aTipoEntidad ) to High( aTipoEntidad ) do
     aTipoEntidad[i] := Format( 'La Tabla #%d no Existe', [i] );

     with DataSet do
     begin
          try
             DisableControls;
             First;
             while NOT EOF do
             begin
                  with FieldByName('EN_CODIGO') do
                       aTipoEntidad[ AsInteger ] := StrDef( FieldByName('EN_TITULO').AsString, Format( 'Tabla #%d', [AsInteger] ) );
                  Next;
             end;
          finally
                 EnableControls;
          end;
     end;
end;
{$ELSE}
function ObtieneEntidad( const Index: TipoEntidad ): String;
begin
     if EntidadValida( Index ) then
     begin
          Result :=  aTipoEntidad[ TipoEntidad( Index ) ];
     end
     else
     begin
          Result := '< Tabla Para Uso Futuro >';
     end;
end;
{$endif}

function Dentro( const Entidad: TipoEntidad; Entidades : Array of TipoEntidad ): Boolean;
 var i: integer;
begin
     Result := FALSE;
     for i:=  Low(Entidades) to High(Entidades) do
     begin
          Result := ( Entidad = Entidades[i] );
          if Result then
             Break;

     end;
end;



end.
