unit ZetaCXGrid;

interface

uses
  SysUtils, Classes, Controls, DB, cxControls, cxGrid,
  cxGridDBTableView, cxGridCustomView, ZetaMessages ,ZetaClientDataset;

type
  TZetaCXGrid = class(TcxGrid)
  private
   { Propiedades para replicar ordenamiento al dar Refresh a los grids}
    //FDescendente: TIndexOptions;
    //FFieldName: String;
    { Private declarations }
    function GetInformacion: String;
    procedure OrdenaPorColumna(ColumnFieldName, ColumnIndexName: String ; DataSet: TZetaClientDataset);
  protected
    { Protected declarations }
  public
    { Public declarations }
     constructor Create( AOwner: TComponent ); override;
     //procedure LimpiaIndex(DataSet: TZetaClientDataset);
     procedure OrdenarPor(Column: TcxGridDBColumn ; DataSet: TZetaClientDataset); overload;
     procedure OrdenarPor(Column: TcxGridDBColumn ; DataSet: TZetaClientDataset; FieldName:String); overload;
     //procedure OrdenaPostRefresh(DataSet: TZetaClientDataset);
  published
    { Published declarations }
    property Informacion: String read GetInformacion;
  end;

procedure Register;

implementation
uses
{$ifdef TEST_COMPLETE}
     ZetaCommonClasses,
{$endif}
     ZetaCommonTools;

constructor TZetaCXGrid.Create( AOwner: TComponent );
begin
     inherited Create( AOwner );
     {***(@am): Inicializacion de variables***}
     //FDescendente := [];
     //FFieldName := '';
end;

function TZetaCXGrid.GetInformacion: String;
{$ifdef TEST_COMPLETE}
var
AGridView: TcxCustomGridView;
AGridDBTableView: TcxGridDBTableView ;
oCampo: TField;
Pos : TBookMark;
i : Integer;
{$endif}
begin
     Result := '';
{$ifdef TEST_COMPLETE}
    AGridView := Views[0];

    if AGridView is TcxGridDBTableView then
    begin
         AGridDBTableView:=TcxGridDBTableView(AGridView);
         if Assigned( AGridDBTableView.DataController.DataSource ) and Assigned( AGridDBTableView.DataController.DataSource.DataSet) then
         begin
              with AGridDBTableView.DataController.DataSource.DataSet, AGridDBTableView  do
              begin
                   if ( not IsEmpty ) and ( ColumnCount> 0 ) then
                   begin
                        DisableControls;
                        try
                           Pos:= GetBookMark;
                           First;
                           for i := 0 to ( ColumnCount - 1 ) do
                               Result := Result + Columns[i].Caption + K_PIPE; //Items[i].Caption
                           Result := Result + CR_LF;
                           while not Eof do
                           begin
                                for i := 0 to ( ColumnCount - 1 ) do
                                begin
                                     oCampo:= Columns[i].DataBinding.Field;
                                     if Assigned( oCampo ) then
                                        Result := Result + Trim( oCampo.AsString ) + K_PIPE   // Para evitar diferencias por IBObjects que puede regresar strings con espacios a la izquierda ( Ej. Join de Historial de Kardex con Permisos, Incapacidades y Vacaciones
                                     else
                                         Result := Result + K_PIPE;
                                end;
                                Result := Result + CR_LF;
                                Next;
                           end;
                           if ( Pos <> nil ) then
                           begin
                                GotoBookMark( Pos );
                                FreeBookMark( Pos );
                           end;
                        finally
                           EnableControls;
                        end;
                   end;
              end;
         end;
    end;
{$endif}
end;

{***(@am): Este metodo ordenara el dataset con los parametros del ultimo filtro ***}
{procedure TZetaCXGrid.OrdenaPostRefresh(DataSet: TZetaClientDataset);
var
   sField, sFieldName: String;
begin
     LimpiaIndex(Dataset); // (@am): Es necesario si no hubo cambios porque el IndexName no se limpia desde el ClientDataSet.
     if FFieldName <> '' then
     begin
       sField := FFieldName +'Index';
       sFieldName := FFieldName;
       with DataSet do
       begin
            IndexDefs.Update; //agrega 2 por default
            AddIndex( sField, sFieldName, FDescendente);
            IndexName := sField;
            First;
       end;
     end;
end; }

{***@(am): Este metodo oredena por columna despues de dar clic***}
procedure  TZetaCXGrid.OrdenaPorColumna(ColumnFieldName, ColumnIndexName: String; DataSet: TZetaClientDataset);
var
   sField, sIndex, sFieldName: String;
   lDescendente: TIndexOptions;
begin
  sField := ColumnIndexName +'Index';
  sFieldName := ColumnFieldName;
  with DataSet do
     begin
          sIndex := IndexName;
          if sIndex <> '' then
          begin
               GetIndexInfo(sIndex);
               IndexDefs.Update;
               with IndexDefs.Find(sIndex) do
               begin
                    if (sField = sIndex) AND (Options = []) then
                       lDescendente := [ixDescending]
                    else lDescendente := [];
               end;
               DeleteIndex(sIndex);
               IndexDefs.Update; //Es necesario para que refleje que se elimino un Index.
          end
          else
              lDescendente := [];

          AddIndex( sField, sFieldName, lDescendente);
          IndexDefs.Update; //Es necesario para que refleje que se agrego un Index.
          IndexName := sField;
          //First;  // Se comenta para que se conserve el registro seleccionado al ordenar.
          //Llenar estas variables para replicar ordenamiento al dar Refresh
          //FDescendente := lDescendente;
          //FFieldName := sFieldName;
     end;
end;

{procedure TZetaCXGrid.LimpiaIndex(DataSet: TZetaClientDataset);
begin
  DataSet.IndexName := '';
end;}

procedure TZetaCXGrid.OrdenarPor( Column: TcxGridDBColumn ; DataSet: TZetaClientDataset )overload;
begin
     if Column.DataBinding.Field <> NIL then
     begin
          with Column.DataBinding.Field do
          begin
               if ( Dataset is TZetaClientDataset ) then
               begin
                    case FieldKind of
                         fkData: OrdenaPorColumna(Column.DataBinding.FieldName,Column.DataBinding.FieldName, TZetaClientDataset( DataSet)) ;
                         fkLookup: OrdenaPorColumna(KeyFields,KeyFields, TZetaClientDataset( DataSet)) ;
                    end;
               end;
          end;
     end;
end;

procedure TZetaCXGrid.OrdenarPor(Column: TcxGridDBColumn ; DataSet: TZetaClientDataset; FieldName:String)overload;
begin
     if Column.DataBinding.Field <> NIL then
          OrdenaPorColumna(FieldName, Column.DataBinding.FieldName, TZetaClientDataset( DataSet)) ; //Fields, nombre para el Index
end;

procedure Register;
begin
  RegisterComponents('Tress Client', [TZetaCXGrid]);
end;

end.
