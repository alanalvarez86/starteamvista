object Busqueda: TBusqueda
  Left = 514
  Top = 200
  AutoScroll = False
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Busqueda'
  ClientHeight = 304
  ClientWidth = 526
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelBotones: TPanel
    Left = 0
    Top = 271
    Width = 526
    Height = 33
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      526
      33)
    object OK: TBitBtn
      Left = 358
      Top = 4
      Width = 75
      Height = 25
      Hint = 'Escoger Registro Seleccionado'
      Anchors = [akTop, akRight]
      Caption = '&OK'
      ModalResult = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = OKClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object Cancelar: TBitBtn
      Left = 443
      Top = 4
      Width = 75
      Height = 25
      Hint = 'Salir Sin Escoger Registro'
      Anchors = [akTop, akRight]
      Caption = '&Cancelar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Kind = bkCancel
    end
    object Modificar: TBitBtn
      Left = 7
      Top = 4
      Width = 75
      Height = 25
      Hint = 'Editar Estos Registros'
      Caption = '&Modificar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = ModificarClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        5555555500005577777777777777777500005000000000000000007500005088
        80FFFFFF0FFFF0750000508180F4444F0F44F0750000508880FFFFFF0FFFF075
        0000508180F4444F0F44F0750000508880FFFFFF0FFFF0750000508180F4444F
        0F44F0750000508880FF0078088880750000508180F400007844807500005088
        80FF7008007880750000508180F4408FF80080750000508880FFF70FFF800075
        0000500000000008FF803007000050EEEEEEEE70880B43000000500000000000
        00FBB43000005555555555550BFFBB43000055555555555550BFFBB400005555
        55555555550BFFBB0000}
    end
  end
  object PanelSuperior: TPanel
    Left = 0
    Top = 0
    Width = 526
    Height = 35
    Align = alTop
    TabOrder = 1
    DesignSize = (
      526
      35)
    object PistaLBL: TLabel
      Left = 7
      Top = 11
      Width = 59
      Height = 13
      Caption = '&Descripci'#243'n:'
      FocusControl = Pista
    end
    object Refrescar: TSpeedButton
      Left = 495
      Top = 5
      Width = 26
      Height = 25
      Anchors = [akTop, akRight]
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333333333333333333333338083333333333380083333
        3333333300833333333333380833333333333330033333333333333003330000
        0333333008333800033333380083800003333333000000080333333338000833
        0333333333333333333333333333333333333333333333333333}
      ParentShowHint = False
      ShowHint = True
      OnClick = RefrescarClick
    end
    object Pista: TEdit
      Left = 69
      Top = 7
      Width = 176
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      OnChange = PistaChange
      OnKeyDown = PistaKeyDown
      OnKeyPress = PistaKeyPress
    end
    object Filtrar: TBitBtn
      Left = 250
      Top = 5
      Width = 75
      Height = 25
      Hint = 'Filtrar Registros Por Esta Descripci'#243'n'
      Caption = '&Filtrar'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = FiltrarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33033333333333333F7F3333333333333000333333333333F777333333333333
        000333333333333F777333333333333000333333333333F77733333333333300
        033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
        33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
        3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
        33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
        333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
        333333773FF77333333333370007333333333333777333333333}
      NumGlyphs = 2
    end
    object MostrarActivos: TCheckBox
      Left = 370
      Top = 9
      Width = 114
      Height = 17
      Anchors = [akTop, akRight]
      Caption = 'Mostrar s'#243'lo &activos'
      TabOrder = 2
      OnClick = FiltrarClick
    end
  end
  object DBGrid: TZetaDBGrid
    Left = 0
    Top = 35
    Width = 526
    Height = 236
    Align = alClient
    DataSource = Datasource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = OKClick
    Columns = <
      item
        Expanded = False
        Title.Caption = 'C'#243'digo'
        Width = 70
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        Title.Caption = 'Descripci'#243'n'
        Width = 400
        Visible = True
      end>
  end
  object Datasource: TDataSource
    Left = 136
    Top = 120
  end
end
