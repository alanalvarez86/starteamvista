unit ZetaCXStateComboBox;

interface

uses
  Windows, Messages,SysUtils, Classes, Controls, Graphics, Forms, Dialogs, StdCtrls,
  cxControls, cxContainer, cxEdit, cxTextEdit,cxMaskEdit, cxDropDownEdit,
  ZetaCommonLists,
  ZetaCommonClasses,
  ZetaMessages,
  ZetaCommonTools;

type
  TcxStateComboBox = class(TcxComboBox)
  private
    { Private declarations }
    FLlaveNumerica: Boolean;
    FListaFija: ListasFijas; //PENDIENTE
    FListaVariable: ListasVariables; //PENDIENTE
    FLista: TStrings;
    FTempItems: TStrings;//PENDIENTE//acl
    FChanged: Boolean;
    FClickChange: Boolean;
    FEsconderVacios: Boolean; //PENDIENTE{acl Atributo que inicializa los combos de TipoNomina como True para esconder campos vacios.}
    FMaxItems: Integer;
    FOffset: Integer;
    FOnLookUp: TConfirmEvent;
    FOnReload: TNotifyEvent; //PENDIENTE
    function CalculaIndice( const Index: Integer ): Integer;
    function CanLoadList: Boolean;
    function ComboHasValue: Boolean;
    function Editable: Boolean;// Cambiar por propiedad del combo
    function GetComboText: String;                 { Basada en TDBComboBox.GetComboText } //PENDIENTE
    function GetLlave: String;
    function GetLlaves( Index: Integer ): String;
    function GetDescripcion: String; //PENDIENTE
    function GetDescripciones( Index: Integer ): String;
    function GetIndice: Integer; //PENDIENTE
    function GetValorEntero: Integer; //PENDIENTE
    procedure SetIndice( const Value: Integer ); //PENDIENTE
    procedure SetLlave( const Value: String );
    procedure SetLista( Value: TStrings );
    procedure UpdateItems( Sender: TObject );
    procedure SetValorEntero(const Value: Integer); //PENDIENTE
  protected
    { Protected declarations }
    function LookUp: Boolean;
    procedure CMDialogKey( var Message: TCMDialogKey ); message CM_DIALOGKEY; 
    procedure DoEnter; override; { hereda de TWinControl.DoEnter }
    procedure Change(Sender: TObject);
    procedure Click; override; { hereda de TControl.Click }
    procedure DoExit; override; { hereda de TWinControl.DoExit }
    procedure Refresh; //PENDIENTE
    procedure Reload; virtual; //PENDIENTE
    procedure Reaction; //PENDIENTE
    procedure Loaded; override; //PENDIENTE
    procedure LeeListaFija; virtual; //PENDIENTE
    procedure CambiaItems; virtual;
    procedure SetComboText( const sValue: String ); { Basada en TDBComboBox.SetComboText } 
    procedure SetListaFija( Value: ListasFijas ); virtual; //PENDIENTE
  public
    { Public declarations }
    property Descripcion: String read GetDescripcion;  //PENDIENTE
    property Descripciones[ Index: Integer ]: String read GetDescripciones; //PENDIENTE
    property Lista: TStrings read FLista write SetLista;
    property Llave: String read GetLlave write SetLlave;
    property Indice: Integer read GetIndice write SetIndice; //PENDIENTE
    property Llaves[ Index: Integer ]: String read GetLlaves;  //PENDIENTE
    property ValorEntero: Integer read GetValorEntero write SetValorEntero; //PENDIENTE
    //property ClickChange: Boolean read FClickChange write FClickChange; //TEMPORAL
    //property ChangedItem: Boolean read FChanged write FChanged; //TEMPORAL
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    procedure AsignaLlave(const Value: String); { Para Cambiar Llave y llamar Lookup } //PENDIENTE
    procedure AsignaValorEntero(const Value: Integer); { Para Cambiar ValorEntero y llamar Lookup } //PENDIENTE
  published
    { Published declarations }
    property ListaFija: ListasFijas read FListaFija write SetListaFija; //PENDIENTE
    property ListaVariable: ListasVariables read FListaVariable write FListaVariable; //PENDIENTE
    property EsconderVacios: Boolean read FEsconderVacios write FEsconderVacios;//acl //PENDIENTE
    property LlaveNumerica: Boolean read FLlaveNumerica write FLlaveNumerica;
    property MaxItems: Integer read FMaxItems write FMaxItems;
    property Offset: Integer read FOffset write FOffset;
    property OnLookUp: TConfirmEvent read FOnLookUp write FOnLookUp;
    property OnReload: TNotifyEvent read FOnReload write FOnReload;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Tress Client', [TcxStateComboBox]);
end;

{ ************* TcxStateComboBox ****************** }
//DevEx(by am): Adaptacion del ZetaStateComboBox, se cambio la herencia a TcxComboBox

constructor TcxStateComboBox.Create(AOwner: TComponent);
begin
     FMaxItems := 10;
     FLlaveNumerica := True;
     FEsconderVacios := False; //acl
     inherited Create( AOwner );
     FLista := TStringList.Create;
     FTempItems := TStringList.Create; //acl
     TStringList( FLista ).OnChange := UpdateItems;
     FChanged := False;
     FClickChange := False;
     {***Propiedades especiales del control cxComboBox (by am)***}
     self.AutoSize := FALSE; //No se requiere que se ajuste del tama�o del combo al texto
     self.Properties.IncrementalSearch := FALSE;
     self.Properties.ImmediateDropDownWhenKeyPressed := FALSE; //Para no tener que dar doble Enter al querer ejecutar una busqueda
     self.Properties.AutoSelect := TRUE;//Selecciona todo el texto al enfocar el control
     Self.Properties.OnChange := Change; //No se puede hacer override al evento PropertiesOnChange. Pero si se puede ligar el evento a un metodo.
     {NOTA(by am):La propiedad ReadOnly no tiene relevancia, solo se utiliza con el databinding, MaxLength se encuentra dentro de Properties}
end;

destructor TcxStateComboBox.Destroy;
begin
     TStringList( FLista ).OnChange := nil;  //Cambio
     FLista.Free;
     FTempItems.Free;//acl //Cambio
     inherited Destroy;
end;

//Cambio
procedure TcxStateComboBox.Loaded;
begin
     inherited Loaded;
     LeeListaFija;

     //Adaptacion
     with Properties do
          DropDownRows := Items.Count;
     //DropDownCount := Items.Count; //old
end;

function TcxStateComboBox.CanLoadList: Boolean;
begin
     Result := not ( csDesigning in ComponentState );
end;

function TcxStateComboBox.CalculaIndice( const Index: Integer ): Integer;
begin
     Result := ( Index - Offset );
end;

//Cambio
function TcxStateComboBox.GetIndice: Integer;
begin
     Result := ItemIndex + Offset;
end;

//Cambio
procedure TcxStateComboBox.SetIndice(const Value: Integer);
var
   i: Integer;
   sValue: String;
begin
     if FLlaveNumerica then
     begin
          i := CalculaIndice( Value );
          if ( i >= 0 ) and ( i < Properties.Items.Count ) then
               sValue := Properties.Items[ i ]
          else
              sValue := '';
          SetComboText( sValue ); //Necesario para recalcular la llave al deshabilitar borrar descripciones de listas fijas.
     end;
end;

function TcxStateComboBox.GetLlave: String;
begin
     if ( ItemIndex < 0 ) then
        Result := ''
     else
         Result := GetLlaves( ItemIndex );
end;

function TcxStateComboBox.GetLlaves( Index: Integer ): String;
begin
     Result := FLista.Names[ CalculaIndice( Index ) ];
end;

//Cambio
function TcxStateComboBox.GetDescripcion: String;
begin
     if ( ItemIndex < 0 ) then
        Result := ''
     else
         Result := GetDescripciones( ItemIndex );
end;

function TcxStateComboBox.GetDescripciones( Index: Integer ): String;
var
   sLlave: String;
begin
     sLlave := GetLlaves( Index );
     if ( sLlave <> '' ) then
        Result := FLista.Values[ sLlave ]
     else
         Result := '';
end;

procedure TcxStateComboBox.SetLlave(const Value: String);
var
   i: Integer;
begin
     with FLista do
     begin
          //Adaptacion
          if Properties.DropDownListStyle = lsFixedList  then
          //if ( Self.Style = csDropDownList ) then //old
          begin
               i := IndexOfName( Value );
               if ( i >= 0 ) then
                  ItemIndex := i;
          end
          else
              SetComboText( Values[ Value ] );
     end;
end;

//Cambio
procedure TcxStateComboBox.AsignaLlave(const Value: String);
begin
     Llave := Value;
     Lookup;
end;

//Cambio
procedure TcxStateComboBox.LeeListaFija;
var
   i, iMax, iPos: Integer;
begin
     FTempItems.Clear;
     if ( FListaFija <> lfNinguna ) and CanLoadList then
     begin
          with Lista do
          begin
               try
                  BeginUpdate;
                  Clear;
               finally
                      EndUpdate;
               end;
               ZetaCommonLists.LlenaLista( FListaFija, Properties.Items );
{acl. Si el atributo esta vacio se agregan los items de la lista a uno Tstring temporal recorres el ciclo y
almacena los items en el temporal}
               if ( EsconderVacios ) then
               begin
                    iMax:= Properties.Items.Count;
                    iPos:=0;

                    FTempItems.AddStrings( Properties.Items );
                    for i := 0 to ( iMax - 1 ) do
                    begin
                         if (FListaFija = lfTipoPeriodo) or (FListaFija = lfTipoPeriodoConfidencial) or ( FListaFija = lfTipoNomina) then  //Listas que ya vienen en formato Key=Value
                         begin
                              //Valida si hay valor asignado en la cadena 'key=value'
                              iPos := Pos('=', FTempItems[i]);
                              if (iPos > 0) and strLleno(Copy(FTempItems[i], iPos + 1, Length(FTempItems[i]))) then
                                 Add( FTempItems[i] );
                         end
                         else
                         begin
                              if strLleno( FTempItems[i] ) then
                                 Add( IntToStr(i) + '=' + FTempItems[i]  );
                         end;
                    end;
               end;
          end;
     end;
end;

//Cambio
procedure TcxStateComboBox.SetListaFija( Value: ListasFijas );
begin
     FListaFija := Value;
     LeeListaFija;
end;

procedure TcxStateComboBox.SetLista( Value: TStrings );
begin
     if ( FListaFija = lfNinguna ) and CanLoadList then //Actualizacion
     begin
          FLista.Assign( Value );
          UpdateItems( Self );
     end;
end;

//Cambio
function TcxStateComboBox.GetValorEntero: Integer;
begin
     Result := StrToIntDef( Text, 0 );
end;

//Cambio
procedure TcxStateComboBox.SetValorEntero(const Value: Integer);
begin
     Text := IntToStr( Value );
end;

//Cambio
procedure TcxStateComboBox.AsignaValorEntero(const Value: Integer);
begin
     ValorEntero := Value;
     Lookup;
end;

procedure TcxStateComboBox.CambiaItems;
var
   i: Integer;
begin
     with Properties.Items do
     begin
          try
             BeginUpdate;
             Clear;
             for i := 0 to ( FLista.Count - 1 ) do
                 Add( GetDescripciones( i ) );
             finally
                    EndUpdate;
             end;
     end;
end;

procedure TcxStateComboBox.UpdateItems( Sender: TObject );
begin
     CambiaItems;
end;

function TcxStateComboBox.Editable: Boolean;
begin
     Result := ( Properties.DropDownListStyle in [lsEditFixedList, lsEditList] );
end;

//PENDIENTE
procedure TcxStateComboBox.SetComboText( const sValue: String );
var
   //Redraw: Boolean;
   i: Integer;
begin
     {Redraw := ( Style <> csSimple ) and HandleAllocated;
     if Redraw then
        SendMessage( Handle, WM_SETREDRAW, 0, 0 );  }
     try
        if ( sValue = '' ) then
           i := -1
        else
            i := Properties.Items.IndexOf( sValue );
        ItemIndex := i;
     finally
            {if Redraw then
            begin
                 SendMessage( Handle, WM_SETREDRAW, 1, 0 );
                 Invalidate;
            end; }
     end;
end;

function TcxStateComboBox.ComboHasValue: Boolean;
begin
     if Editable then
     begin
          if FLlaveNumerica then
             Result := ( StrToIntDef( Text, 0 ) <> 0 )
          else
              Result := ( Text > '' );
     end
     else
         if FLlaveNumerica then
            Result := ( ItemIndex >= 0 )
         else
             Result := ( Text > '' );
end;

//Cambio
function TcxStateComboBox.GetComboText: string;
var
   i: Integer;
begin
     if Editable then
        Result := Text
     else
     begin
          i := ItemIndex;
          if ( i < 0 ) then
             Result := ''
          else
              Result := Properties.Items[ i ]; //Adaptacion
              //Result := Items[ i ]; //old
     end;
end;

procedure TcxStateComboBox.DoEnter;
begin
     FChanged := False;
     SelectAll;
     inherited DoEnter;
end;

//cambio
procedure TcxStateComboBox.Change(Sender: TObject);
begin
     //inherited Change;
     if (FClickChange) then
     begin
          FClickChange := False;
          LookUp;
          FChanged := False;
     end
     else
         FChanged := True;
end;

procedure TcxStateComboBox.Click;
begin
     inherited Click;
     FClickChange := True;
end;

procedure TcxStateComboBox.DoExit;
begin
     inherited DoExit;
     if Editable then
        Reaction;
end;

//cambio  //SOLO SE UTILIZA EN COMBOS DONDE SE PUEDA CAPTURAR
procedure TcxStateComboBox.CMDialogKey( var Message: TCMDialogKey );
begin
     with Message do
     begin
          if ( CharCode = VK_RETURN ) and Focused and Editable and ( KeyDataToShiftState( Message.KeyData ) = [] ) then
          begin
               Reaction;
               SelectAll;
               Result := 1;
          end
          else
              inherited;
     end;
end;

//Cambio
procedure TcxStateComboBox.Reaction;
begin
     if FChanged then
     begin
          if LookUp then
             Refresh;
          FChanged := False;
     end;
end;

function TcxStateComboBox.LookUp: Boolean;
begin
     if ComboHasValue then
     begin
          Result := True;
          if Assigned( FOnLookUp ) then
             FOnLookUp( Self, Result );
     end
     else
         Result := False;
end;

//Cambio
procedure TcxStateComboBox.Reload;
begin
     if Assigned( FOnReload ) then
        FOnReload( Self );
end;

//Cambio
procedure TcxStateComboBox.Refresh;
var
   i, iPtr, iLast: Integer;
   sText: String;
begin
     if Editable then
     begin
          try
             //Adaptacion
             Properties.Items.BeginUpdate;
             //Items.BeginUpdate; //OLD

             sText := GetComboText;
             //Adaptacion
             iPtr := Properties.Items.IndexOf( sText );
             //iPtr := Items.IndexOf( sText ); //old
             //Adaptacion
             iLast := ( Properties.Items.Count - 1 );
             //iLast := ( Items.Count - 1 ); //old

             if ( iPtr >= 0 ) then
             begin
                  for i := ( iPtr + 1 ) to iLast do
                      //Adaptacion
                      Properties.Items.Strings[ i - 1 ] := Properties.Items.Strings[ i ];
                      //Items.Strings[ i - 1 ] := Items.Strings[ i ];  //old
                  Properties.Items.Delete( iLast );
                  //Items.Delete( iLast );  //old
             end;
             Properties.Items.Insert( 0, sText ); //Adaptacion
             //Items.Insert( 0, sText ); //old
             if ( Properties.Items.Count > FMaxItems ) then  //Adaptacion
             //if ( Items.Count > FMaxItems ) then //old
                  Properties.Items.Delete( FMaxItems ); //Adaptacion
                //Items.Delete( FMaxItems );//old
          finally
                 Properties.Items.EndUpdate; //Adaptacion
                 //Items.EndUpdate; //old
          end;
     end;
end;

end.
