unit ZetaMigrar;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, DB, DBTables,
     ZetaCommonLists,
     ZetaCommonClasses;

const
     DBIERR_KEYVIOL = 9729;
type
  TConfirmEvent = procedure ( Sender: TObject; var lOk: Boolean ) of object;
  TZetaMigrarLog = class;
  TAsciiLog = class( TObject )
  private
    { Private declarations }
    FIsOpen: Boolean;
    FFileName: String;
    FTextFile: TextFile;
    FLogErrors: Boolean;
  public
    { Public Declarations }
    constructor Create;
    destructor Destroy; override;
    property IsOpen: Boolean read FIsOpen;
    property FileName: String read FFileName write FFileName;
    property LogErrors: Boolean read FLogErrors write FLogErrors;
    procedure AgregaError( const sTexto: String );
    procedure AgregaTexto( const sTexto: String );
    procedure Close;
    procedure CopiaLista( Lista: TStrings );
    procedure Open;
    procedure WriteText( const sText: String );
  end;
  TReportEvent = class( TObject )
  private
    { Private declarations }
    FName: String;
    FData: String;
    FCount: Integer;
    FWarning: Boolean;
    FOwner: TList;
  public
    { Public declarations }
    constructor Create( Owner: TList );
    destructor Destroy; override;
    property Name: String read FName write FName;
    property Count: Integer read FCount;
    property Data: String read FData;
    property Warning: Boolean read FWarning write FWarning;
    function WriteDetails: String;
    function WriteSummary: String;
    procedure Increment;
    procedure AddData( const sValue: String );
  end;
  TReportEvents = class( TObject )
  private
    { Private declarations }
    FName: String;
    FWarnings: Boolean;
    FEvents: TList;
    FOwner: TList;
    function GetEvents( Index: Integer ): TReportEvent;
    function IndexOf( const sName: String ): Integer;
    procedure Clear;
  public
    { Public declarations }
    constructor Create( Owner: TList );
    destructor Destroy; override;
    property Name: String read FName write FName;
    property Events[ Index: Integer ]: TReportEvent read GetEvents;
    property Warnings: Boolean read FWarnings;
    procedure AddEvent( const sEvent: String );
    procedure AddWarning( const sEvent: String );
    procedure AddWarningList( const sEvent, sList: String );
    procedure WriteDetails( Lista: TAsciiLog );
    procedure WriteSummary( Bitacora: TZetaMigrarLog );
  end;
  TZetaMigrarLog = class( TObject )
  private
    { Private declarations }
    FAvisos: Integer;
    FErrores: TAsciiLog;
    FResumen: TStrings;
    FLista: TList;
    function GetItems( Index: Integer ): TReportEvents;
    function IndexOf( const sReport: String ): Integer;
    procedure Clear;
    procedure WriteDetails;
    procedure WriteSummary;
    property Items[ Index: Integer ]: TReportEvents read GetItems;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Avisos: Integer read FAvisos;
    procedure AddEvent( const sReport, sEvent: String );
    procedure AddWarning( const sReport, sEvent: String );
    procedure AddWarningList( const sReport, sEvent, sList: String );
    procedure AgregaError( const sMensaje: String );
    procedure AgregaMensaje( const sMensaje: String );
    procedure AgregaResumen( const sMensaje: String );
    procedure AgregaResumenOnly( const sMensaje: String );
    procedure Close;
    procedure HandleException( const sMensaje: String; Error: Exception );
    procedure InitMigracion;
    procedure SetErrores( Value: TAsciiLog );
    procedure SetResumen( Value: TStrings );
  end;
  TZetaMigrar = class( TComponent )
  private
    { Private declarations }
    FBitacora: TZetaMigrarLog;
    FAdditionalRows: Integer;
    FBatchMove: TBatchMove;
    FCounter: Integer;
    FCallBackCount: Integer;
    FEmptyTable: Boolean;
    FEnabled: Boolean;
    FDeleteQuery: TQuery;
    FInsertSQL: TStrings;
    FMessage: String;
    FMultiples: Integer;
    FPoblar: Boolean;
    FProceso: Procesos;
    FSourceTotal: Word;
    FSourcePtr: Word;
    FSourcePath: String;
    FSourceDatabase: TDatabase;
    FSourceRecordNumber: Integer;
    FSourceRecordAccept: Integer;
    FSourceRecordOK: Integer;
    FSourceQuery: TQuery;
    FSourceSQL: TStrings;
    FDefaultSQL: TStrings;
    FTargetDatabase: TDatabase;
    FTargetTableName: String;
    FTargetTable: TTable;
    FTargetQuery: TQuery;
    FTxCount: Integer;
    FHayError: Boolean;
    FCanContinue: Boolean;
    FOnAdditionalRows: TNotifyEvent;
    FOnCallBack: TConfirmEvent;
    FOnCloseDatasets: TNotifyEvent;
    FOnFilterRecord: TFilterRecordEvent;
    FOnMoveExcludedFields: TNotifyEvent;
    FOnMoveOtherTables: TNotifyEvent;
    FOnOpenDatasets: TNotifyEvent;
    FOnPostError: TNotifyEvent;
    function GetSuccessRate: Real;
    function MoreThanOneSourceDataset: Boolean;
    function SourceDatasetsAvailable: Boolean;
    procedure SetBatchMove( Value: TBatchMove );
    procedure SetDefaultSQL( Value: TStrings );
    procedure SetSourceDatabase( Value: TDatabase );
    procedure SetSourceQuery( Value: TQuery );
    procedure SetSourceSQL( Value: TStrings );
    procedure SetTargetDatabase( Value: TDatabase );
    procedure SetTargetTable( Value: TTable );
    procedure SetTargetQuery( Value: TQuery );
    procedure SetDeleteQuery( Value: TQuery );
    procedure SetInsertSQL( Value: TStrings );
    procedure EfectuarTransaccion( const lStartTransaction: Boolean );
    procedure InitCounters;
    procedure CloseDatasetsSource;
    procedure CloseDatasetsTarget;
    procedure DoCloseDatasets;
    procedure FindSourceDatasets;
    procedure OpenDatasetsSource;
    procedure OpenDatasetsTarget;
  protected
    { Protected declarations }
    function CallBack: Boolean;
    procedure Notification( AComponent: TComponent; Operation: TOperation ); override;
  public
    { Public declarations }
    property Counter: Integer read FCounter;
    property SourceRecordNumber: Integer read FSourceRecordNumber write FSourceRecordNumber;
    property SuccessRate: Real read GetSuccessRate;
    property HayError: Boolean read FHayError;
    property CanContinue: Boolean read FCanContinue write FCanContinue;
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    function BorraTabla( Bitacora: TZetaMigrarLog ): Boolean;
    function GetParadoxTable: String;
    function PuedePoblar( const sPath: String ): Boolean;
    procedure PoblarTabla( Bitacora: TZetaMigrarLog );
    procedure PoblarTablaDefault( Bitacora: TZetaMigrarLog );
    procedure Migrar( Bitacora: TZetaMigrarLog );
    procedure AgregaError( const sMensaje: String );
    procedure HandleException( const sMensaje: String; Error: Exception );
    procedure ClearParameters( Query: TQuery );
    procedure CloseDatasets;
    procedure CloseQuery( Query: TQuery );
    procedure DeleteTable( const sTableName: String );
    procedure OpenDatasets;
    procedure PrepareSourceQuery( Query: TQuery; const Script: String );
    procedure PrepareTargetQuery( Query: TQuery; const Script: String );
  published
    { Published declarations }
    property AdditionalRows: Integer read FAdditionalRows write FAdditionalRows default 0;
    property BatchMove: TBatchMove read FBatchMove write SetBatchMove;
    property CallBackCount: Integer read FCallBackCount write FCallBackCount;
    property DefaultSQL: TStrings read FDefaultSQL write SetDefaultSQL;
    property DeleteQuery: TQuery read FDeleteQuery write SetDeleteQuery;
    property EmptyTargetTable: Boolean read FEmptyTable write FEmptyTable;
    property Enabled: Boolean read FEnabled write FEnabled default True;
    property InsertSQL: TStrings read FInsertSQL write SetInsertSQL;
    property Multiples: Integer read FMultiples write FMultiples default 1;
    property Poblar: Boolean read FPoblar write FPoblar default False;
    property Proceso: Procesos read FProceso write FProceso;
    property SourceDatabase: TDatabase read FSourceDatabase write SetSourceDatabase;
    property SourceQuery: TQuery read FSourceQuery write SetSourceQuery;
    property SourceSQL: TStrings read FSourceSQL write SetSourceSQL;
    property StartMessage: String read FMessage write FMessage;
    property TargetDatabase: TDatabase read FTargetDatabase write SetTargetDatabase;
    property TargetTableName: String read FTargetTableName write FTargetTableName;
    property TargetTable: TTable read FTargetTable write SetTargetTable;
    property TargetQuery: TQuery read FTargetQuery write SetTargetQuery;
    property TransactionCount: Integer read FTxCount write FTxCount;
    property OnCallBack: TConfirmEvent read FOnCallBack write FOnCallBack;
    property OnAdditionalRows: TNotifyEvent read FOnAdditionalRows write FOnAdditionalRows;
    property OnPostError: TNotifyEvent read FOnPostError write FOnPostError;
    property OnCloseDatasets: TNotifyEvent read FOnCloseDatasets write FOnCloseDatasets;
    property OnFilterRecord: TFilterRecordEvent read FOnFilterRecord write FOnFilterRecord;
    property OnMoveExcludedFields: TNotifyEvent read FOnMoveExcludedFields write FOnMoveExcludedFields;
    property OnMoveOtherTables: TNotifyEvent read FOnMoveOtherTables write FOnMoveOtherTables;
    property OnOpenDatasets: TNotifyEvent read FOnOpenDatasets write FOnOpenDatasets;
  end;

implementation

uses ZetaCommonTools;

const
     K_TOPE_WARNING = 100000;

procedure SetInsertScript( Tabla: String; Source: TDataset; Target: TQuery );
var
   i: Integer;
   sScript, sCampos: String;
begin
     sScript := 'insert into ' + Tabla + ' ( ';
     sCampos := ' ) values ( ';
     with Source do
     begin
          for i := 0 to ( FieldCount - 1 ) do
          begin
               with Fields[ i ] do
               begin
                    if ( FieldKind = fkData ) then
                    begin
                         sScript := sScript + FieldName + ',';
                         sCampos := sCampos + ':' + FieldName + ',';
                    end;
               end;
          end;
     end;
     sScript := ZetaCommonTools.CortaUltimo( sScript ) + ZetaCommonTools.CortaUltimo( sCampos ) + ' )';
     with Target do
     begin
          Active := False;
          with SQL do
          begin
               Clear;
               Add( sScript );
          end;
     end;
end;

procedure SetToNull( Parametro: TParam; Tipo: TFieldType );
begin
     with Parametro do
     begin
          DataType := Tipo;
          Clear;
          Bound := True;
     end;
end;

procedure TransferDatasets( Source: TDataset; Target: TQuery );
var
   i: Integer;
begin
     with Source do
     begin
          for i := 0 to ( FieldCount - 1 ) do
          begin
               with Fields[ i ] do
               begin
                    if ( FieldKind = fkData ) then
                    begin
                         if ( Fields[ i ] is TBlobField ) and ( TBlobField( Fields[ i ] ).BlobSize = 0 ) then
                            Target.ParamByName( FieldName ).AsBlob := TBlobData( '' )
                         else
                             Target.ParamByName( FieldName ).AssignField( Fields[ i ] );
                    end;
               end;
          end;
     end;
     with Target do
     begin
          for i := 0 to ( Params.Count - 1 ) do
          begin
               with Params[ i ] do
               begin
                    if IsNull then
                    begin
                         case DataType of
                              ftString: AsString := '';
                              ftDate, ftTime, ftDateTime: AsDateTime := NullDateTime;
                              ftInteger, ftSmallInt, ftWord: AsInteger := 0;
                              ftFloat, ftCurrency: AsFloat := 0;
                              ftBlob: AsBlob := TBlobData( '' );
                              ftMemo: AsString := '';
                         end;
                    end;
               end;
          end;
     end;
end;

procedure InsertFromDataset( Source: TDataset; Target: TQuery );
begin
     TransferDatasets( Source, Target );
     Target.ExecSQL;
end;

{ ************ TAsciiData *********** }

constructor TAsciiLog.Create;
begin
     FLogErrors := True;
end;

destructor TAsciiLog.Destroy;
begin
     if IsOpen then
        Close;
end;

procedure TAsciiLog.Open;
begin
     try
        if FileExists( FileName ) then
           SysUtils.DeleteFile( FileName );
        AssignFile( FTextFile, FileName ); { Siempre crea uno nuevo }
        Rewrite( FTextFile );
        FIsOpen := True;
     except
           on Error: Exception do
           begin
                FIsOpen := False;
           end;
     end;
end;

procedure TAsciiLog.WriteText( const sText: String );
begin
     if IsOpen then
        Writeln( FTextFile, sText );
end;

procedure TAsciiLog.CopiaLista( Lista: TStrings );
var
   i: Integer;
begin
     with Lista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               WriteText( Strings[ i ] );
          end;
     end;
end;

procedure TAsciiLog.AgregaTexto( const sTexto: String );
var
   pValor, pStart: {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif};
   sValor: String;
begin
     pValor := Pointer( sTexto );
     if ( pValor <> nil ) then
     begin
          while ( pValor^ <> #0 ) do
          begin
               pStart := pValor;
               while not ( pValor^ in [ #0, #10, #13 ] ) do
               begin
                    Inc( pValor );
               end;
               SetString( sValor, pStart, ( pValor - pStart ) );
               WriteText( sValor );
               if ( pValor^ = #13 ) then
                  Inc( pValor );
               if ( pValor^ = #10 ) then
                  Inc( pValor );
          end;
     end;
end;

procedure TAsciiLog.AgregaError( const sTexto: String );
begin
     if LogErrors then
        AgregaTexto( sTexto );
end;

procedure TAsciiLog.Close;
begin
     CloseFile( FTextFile );
     FIsOpen := False;
end;

{ ********* TReportEvent ************ }

constructor TReportEvent.Create( Owner: TList );
begin
     FOwner := Owner;
     FName := '';
     FData := '';
     FWarning := False;
     FOwner.Add( Self );
end;

destructor TReportEvent.Destroy;
begin
     FOwner.Remove( Self );
     inherited Destroy;
end;

function TReportEvent.WriteDetails: String;
begin
     Result := WriteSummary;
end;

function TReportEvent.WriteSummary: String;
begin
     if ( FData <> '' ) then
     begin
          FData := Trim( FData );
          if ( Copy( FData, Length( FData ), 1 ) = ',' ) then
             FData := ZetaCommonTools.CortaUltimo( FData );
          Result := Format( '%s ( %s )', [ FName, FData ] )
     end
     else
         if ( FCount > 1 ) then
            Result := Format( '%s ( %d veces )', [ FName, FCount ] )
         else
             Result := FName;
end;

procedure TReportEvent.Increment;
begin
     Inc( FCount );
end;

procedure TReportEvent.AddData( const sValue: String );
begin
     if ( Pos( sValue, FData ) = 0 ) then
        FData := FData + sValue;
end;

{ ********* TReportEvents ************ }

constructor TReportEvents.Create( Owner: TList );
begin
     FEvents := TList.Create;
     FOwner := Owner;
     FOwner.Add( Self );
end;

destructor TReportEvents.Destroy;
begin
     Clear;
     FEvents.Free;
     FOwner.Remove( Self );
     inherited Destroy;
end;

function TReportEvents.GetEvents( Index: Integer ): TReportEvent;
begin
     Result := TReportEvent( FEvents.Items[ Index ] );
end;

procedure TReportEvents.Clear;
var
   i: Integer;
begin
     with FEvents do
     begin
          for i := ( Count - 1 ) downto 0 do
          begin
               Self.Events[ i ].Free;
          end;
     end;
end;

function TReportEvents.IndexOf( const sName: String ): Integer;
var
   i: Integer;
begin
     Result := -1;
     with FEvents do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with Self.Events[ i ] do
               begin
                    if ( Name = sName ) then
                    begin
                         Result := i;
                         Break;
                    end;
               end;
          end;
     end;
     if ( Result < 0 ) then
     begin
          with TReportEvent.Create( FEvents ) do
          begin
               Name := sName;
               Result := FEvents.Count - 1;
          end;
     end;
end;

procedure TReportEvents.AddEvent( const sEvent: String );
begin
     with Events[ IndexOf( sEvent ) ] do
     begin
          Increment;
     end;
end;

procedure TReportEvents.AddWarning( const sEvent: String );
begin
     FWarnings := True;
     with Events[ IndexOf( sEvent ) ] do
     begin
          Warning := True;
          Increment;
     end;
end;

procedure TReportEvents.AddWarningList( const sEvent, sList: String );
begin
     FWarnings := True;
     with Events[ IndexOf( sEvent ) ] do
     begin
          Warning := True;
          AddData( sList );
          Increment;
     end;
end;

procedure TReportEvents.WriteDetails( Lista: TAsciiLog );
var
   i: Integer;
begin
     with Lista do
     begin
          WriteText( StringOfChar( '=', 40 ) );
          WriteText( 'Reporte: ' + Self.Name );
     end;
     with FEvents do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with Lista do
               begin
                    WriteText( Events[ i ].WriteDetails );
                    WriteText( '' );
               end;
          end;
     end;
end;

procedure TReportEvents.WriteSummary( Bitacora: TZetaMigrarLog );
var
   i: Integer;
begin
     if Warnings then
     begin
          with Bitacora do
          begin
               AgregaResumenOnly( StringOfChar( '=', 40 ) );
               AgregaResumenOnly( 'Reporte: ' + Self.Name );
          end;
          with FEvents do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    if Events[ i ].Warning then
                    begin
                         with Bitacora do
                         begin
                              AgregaResumenOnly( Events[ i ].WriteSummary );
                              AgregaResumenOnly( '' );
                         end;
                    end;
               end;
          end;
     end;
end;

{ **************** TZetaMigrarLog ********************* }

constructor TZetaMigrarLog.Create;
begin
     FLista := TList.Create;
     FAvisos := 0;
end;

destructor TZetaMigrarLog.Destroy;
begin
     Clear;
     FLista.Free;
     inherited Destroy;
end;

procedure TZetaMigrarLog.InitMigracion;
begin
     FAvisos := 0;
end;

function TZetaMigrarLog.GetItems( Index: Integer ): TReportEvents;
begin
     Result := TReportEvents( FLista.Items[ Index ] );
end;

procedure TZetaMigrarLog.SetErrores( Value: TAsciiLog );
begin
     FErrores := Value;
end;

procedure TZetaMigrarLog.SetResumen( Value: TStrings );
begin
     FResumen := Value;
     if Assigned( FResumen ) then
     begin
          FResumen.Clear;
     end;
end;

procedure TZetaMigrarLog.Clear;
var
   i: Integer;
begin
     with FLista do
     begin
          for i := ( Count - 1 ) downto 0 do
          begin
               Self.Items[ i ].Free;
          end;
     end;
end;

function TZetaMigrarLog.IndexOf( const sReport: String ): Integer;
var
   i: Integer;
begin
     Result := -1;
     with FLista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with Self.Items[ i ] do
               begin
                    if ( Name = sReport ) then
                    begin
                         Result := i;
                         Break;
                    end;
               end;
          end;
     end;
     if ( Result < 0 ) then
     begin
          with TReportEvents.Create( FLista ) do
          begin
               Name := sReport;
               Result := FLista.Count - 1;
          end;
     end;
end;

procedure TZetaMigrarLog.AddEvent( const sReport, sEvent: String );
var
   iPtr: Integer;
begin
     iPtr := IndexOf( sReport );
     if ( iPtr >= 0 ) then
     begin
          with Items[ iPtr ] do
          begin
               AddEvent( sEvent );
          end;
     end;
end;

procedure TZetaMigrarLog.AddWarning( const sReport, sEvent: String );
var
   iPtr: Integer;
begin
     iPtr := IndexOf( sReport );
     if ( iPtr >= 0 ) then
     begin
          with Items[ iPtr ] do
          begin
               AddWarning( sEvent );
          end;
     end;
end;

procedure TZetaMigrarLog.AddWarningList( const sReport, sEvent, sList: String );
var
   iPtr: Integer;
begin
     iPtr := IndexOf( sReport );
     if ( iPtr >= 0 ) then
     begin
          with Items[ iPtr ] do
          begin
               AddWarningList( sEvent, sList );
          end;
     end;
end;

procedure TZetaMigrarLog.WriteDetails;
var
   i: Integer;
begin
     with FLista do
     begin
          if ( Count > 0 ) then
          begin
               with FErrores do
               begin
                    WriteText( '' );
                    WriteText( StringOfChar( '*', 40 ) );
                    WriteText( 'Bit�cora de Reportes' );
                    WriteText( StringOfChar( '*', 40 ) );
               end;
               for i := 0 to ( Count - 1 ) do
               begin
                    with Self.Items[ i ] do
                    begin
                         WriteDetails( FErrores );
                    end;
               end;
          end;
     end;
end;

procedure TZetaMigrarLog.WriteSummary;
var
   i: Integer;
   lWarnings: Boolean;
begin
     lWarnings := False;
     with FLista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with Self.Items[ i ] do
               begin
                    if Warnings then
                    begin
                         lWarnings := True;
                         Break;
                    end;
               end;
          end;
     end;
     if lWarnings then
     begin
          AgregaResumenOnly( '' );
          AgregaResumenOnly( StringOfChar( '*', 40 ) );
          AgregaResumenOnly( 'Bit�cora de Reportes' );
          AgregaResumenOnly( StringOfChar( '*', 40 ) );
          with FLista do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    with Self.Items[ i ] do
                    begin
                         WriteSummary( Self );
                    end;
               end;
          end;
     end;
end;

procedure TZetaMigrarLog.HandleException( const sMensaje: String; Error: Exception );
const
     K_CODIGO_YA_EXISTE = '� C�digo Ya Existe !';
     K_LLAVE_NO_EXISTE = '� Llave For�nea No Existe !';
     DBIERR_FOREIGNKEYERR = 9733;
     DBIERR_UNKNOWNSQL = 13059;
     TRIGGER_EXCEPTION = 'exception';
     SEPARADOR = Chr( 0 );
var
   i, iLastCategory, iLastErrorCode: Integer;
begin
     if Assigned( Error ) then
     begin
          if ( Error is EDBEngineError ) then
          begin
               with Error as EDBEngineError do
               begin
                    i := Pos( TRIGGER_EXCEPTION, Message );
                    if ( i > 0 ) then
                    begin
                         AgregaError( sMensaje );
                         AgregaError( Copy( Message, i + Length( TRIGGER_EXCEPTION ) + 3, MaxInt ) );
                         AgregaError( StringOfChar( '-', 20 ) );
                    end
                    else
                        if ( ErrorCount > 0 ) then
                        begin
                             case Errors[ 0 ].ErrorCode of
                                  DBIERR_KEYVIOL: AgregaError( sMensaje + ': ' + K_CODIGO_YA_EXISTE );
                                  DBIERR_FOREIGNKEYERR: AgregaError(  sMensaje + ': ' + K_LLAVE_NO_EXISTE );
                                  else
                                  begin
                                       iLastCategory := 0;
                                       iLastErrorCode := 0;
                                       AgregaError( sMensaje );
                                       for i := 0 to ( ErrorCount - 1 ) do
                                       begin
                                            with Errors[ i ] do
                                            begin
                                                 if ( iLastCategory = Category ) and ( iLastErrorCode = ErrorCode ) then
                                                    AgregaError( Message )
                                                 else
                                                     AgregaError( IntToStr( i + 1 ) +
                                                                  ': Categor�a = ' +
                                                                  IntToStr( Category ) +
                                                                  ', Error = ' +
                                                                  IntToStr( ErrorCode ) +
                                                                  CR_LF +
                                                                  Message );
                                                 iLastCategory := Category;
                                                 iLastErrorCode := ErrorCode;
                                            end;
                                       end;
                                       AgregaError( StringOfChar( '-', 20 ) );
                                  end;
                             end;
                        end
                        else
                        begin
                             AgregaError( sMensaje );
                             AgregaError( 'Error Tipo EDBEngineError Desconocido: '+ Error.Message );
                             AgregaError( StringOfChar( '-', 20 ) );
                        end;
               end;
          end
          else
          begin
               with Error as Exception do
               begin
                    AgregaError( sMensaje );
                    AgregaError( ClassName + ' = ' + Message );
                    AgregaError( StringOfChar( '-', 20 ) );
               end;
          end;
     end
     else
     begin
          AgregaError( sMensaje );
          AgregaError( StringOfChar( '-', 20 ) );
     end;
end;

procedure TZetaMigrarLog.AgregaResumenOnly( const sMensaje: String );
var
   pValor, pStart: {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif};
   sValor: String;
begin
     Inc( FAvisos );
     if Assigned( FResumen ) then
     begin
          with FResumen do
          begin
               if ( sMensaje = '' ) then
                  Add( '' )
               else
               begin
                    BeginUpdate;
                    try
                       pValor := Pointer( sMensaje );
                       if ( pValor <> nil ) then
                       begin
                            while ( pValor^ <> #0 ) do
                            begin
                                 pStart := pValor;
                                 while not ( pValor^ in [ #0, #10, #13 ] ) do
                                 begin
                                      Inc( pValor );
                                 end;
                                 SetString( sValor, pStart, ( pValor - pStart ) );
                                 Add( sValor );
                                 if ( pValor^ = #13 ) then
                                    Inc( pValor );
                                 if ( pValor^ = #10 ) then
                                    Inc( pValor );
                            end;
                       end;
                    finally
                           EndUpdate;
                    end;
               end;
          end;
     end;
end;

procedure TZetaMigrarLog.AgregaError( const sMensaje: String );
begin
     FErrores.AgregaError( sMensaje );
     Inc( FAvisos );
end;

procedure TZetaMigrarLog.AgregaMensaje( const sMensaje: String );
begin
     FErrores.AgregaTexto( sMensaje );
end;

procedure TZetaMigrarLog.AgregaResumen( const sMensaje: String );
begin
     { Agrega Al Detalle }
     AgregaMensaje( sMensaje );
     { Agrega Al Resumen }
     AgregaResumenOnly( sMensaje );
end;

procedure TZetaMigrarLog.Close;
begin
     WriteDetails;
     WriteSummary;
end;

{ *************** TZetaMigrar ********************* }

constructor TZetaMigrar.Create( AOwner: TComponent );
begin
     FEnabled := True;
     FPoblar := False;
     FCallBackCount := 10;
     FTxCount := 10;
     FAdditionalRows := 0;
     FMultiples := 1;
     inherited Create( AOwner );
     FInsertSQL := TStringList.Create;
     FSourceSQL := TStringList.Create;
     FDefaultSQL := TStringList.Create;
     InitCounters;
end;

destructor TZetaMigrar.Destroy;
begin
     FDefaultSQL.Free;
     FSourceSQL.Free;
     FInsertSQL.Free;
     inherited Destroy;
end;

procedure TZetaMigrar.Notification( AComponent: TComponent; Operation: TOperation );
begin
     inherited Notification( AComponent, Operation );
     if ( Operation = opRemove ) then
     begin
          if ( AComponent = FSourceDatabase ) then
             FSourceDatabase := nil
          else
          if ( AComponent = FSourceQuery ) then
             FSourceQuery := nil
          else
          if ( AComponent = FTargetDatabase ) then
             FTargetDatabase := nil
          else
          if ( AComponent = FTargetQuery ) then
             FTargetQuery := nil
          else
          if ( AComponent = FTargetTable ) then
             FTargetTable := nil
          else
          if ( AComponent = FDeleteQuery ) then
             FDeleteQuery := nil
          else
          if ( AComponent = FBatchMove ) then
             FBatchMove := nil;
     end;
end;

procedure TZetaMigrar.SetSourceDatabase( Value: TDatabase );
begin
     if ( FSourceDatabase <> Value ) then
     begin
          FSourceDatabase := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

procedure TZetaMigrar.SetBatchMove( Value: TBatchMove );
begin
     if ( FBatchMove <> Value ) then
     begin
          FBatchMove := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

procedure TZetaMigrar.SetSourceQuery( Value: TQuery );
begin
     if ( FSourceQuery <> Value ) then
     begin
          FSourceQuery := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

procedure TZetaMigrar.SetSourceSQL( Value: TStrings );
begin
     FSourceSQL.Assign( Value );
end;

procedure TZetaMigrar.SetDefaultSQL( Value: TStrings );
begin
     FDefaultSQL.Assign( Value );
end;

procedure TZetaMigrar.SetInsertSQL( Value: TStrings );
begin
     FInsertSQL.Assign( Value );
end;

procedure TZetaMigrar.SetTargetDatabase( Value: TDatabase );
begin
     if ( FTargetDatabase <> Value ) then
     begin
          FTargetDatabase := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

procedure TZetaMigrar.SetTargetQuery( Value: TQuery );
begin
     if ( FTargetQuery <> Value ) then
     begin
          FTargetQuery := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

procedure TZetaMigrar.SetTargetTable( Value: TTable );
begin
     if ( FTargetTable <> Value ) then
     begin
          FTargetTable := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

procedure TZetaMigrar.SetDeleteQuery( Value: TQuery );
begin
     if ( FDeleteQuery <> Value ) then
     begin
          FDeleteQuery := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

procedure TZetaMigrar.InitCounters;
begin
     FSourceRecordNumber := 0;
     FSourceRecordAccept := 0;
     FSourceRecordOK := 0;
     FSourcePtr := 0;
     FSourceTotal := 0;
     FSourcePath := VACIO;
end;

function TZetaMigrar.GetSuccessRate: Real;
begin
     if ( FSourceRecordAccept <= 0 ) then
        Result := 100
     else
         Result := ( 100 * ( FSourceRecordOK / FSourceRecordAccept ) );
end;

function TZetaMigrar.GetParadoxTable: String;
begin
     Result := Format( '%s.DB', [ TargetTableName ] );
end;

function TZetaMigrar.PuedePoblar( const sPath: String ): Boolean;
begin
     Result := Poblar and FileExists( ZetaCommonTools.VerificaDir( sPath ) + GetParadoxTable ); 
end;

function TZetaMigrar.CallBack: Boolean;
begin
     Result := True;
     if Assigned( FOnCallBack ) then
        FOnCallBack( Self, Result );
end;

procedure TZetaMigrar.PrepareSourceQuery( Query: TQuery; const Script: String );
begin
     with Query do
     begin
          Active := False;
          Filtered := FALSE;
          Filter := '';
          while ( FieldCount > 0 ) do
          begin
               Fields[ FieldCount - 1 ].DataSet := nil;
          end;
          FieldDefs.Clear;
          DatabaseName := FSourceDatabase.DatabaseName;
          RequestLive := False;
          with SQL do
          begin
               Clear;
               if StrLleno( Script ) then
               begin
                    Add( Script );
               end;
          end;
     end;
end;

procedure TZetaMigrar.PrepareTargetQuery( Query: TQuery; const Script: String );
begin
     with Query do
     begin
          Active := False;
          DatabaseName := FTargetDatabase.DatabaseName;
          Filtered := FALSE;
          Filter := '';
          while ( FieldCount > 0 ) do
          begin
               Fields[ FieldCount - 1 ].DataSet := nil;
          end;
          FieldDefs.Clear;
          with SQL do
          begin
               Clear;
               if StrLleno( Script ) then
               begin
                    Add( Script );
                    Prepare;
               end;
          end;
     end;
end;

procedure TZetaMigrar.DeleteTable( const sTableName: String );
begin
     if Assigned( FDeleteQuery ) then
     begin
          PrepareTargetQuery( FDeleteQuery, 'delete from ' + sTableName );
          FDeleteQuery.ExecSQL;
     end;
end;

procedure TZetaMigrar.FindSourceDatasets;
const
     K_SUFIX_PTR = '%0:s.DBF';
var
   sDBFFile, sQuery: String;
   i, iPos: Integer;
begin
     sQuery := SourceSQL.Text;
     iPos := Pos( K_SUFIX_PTR, sQuery );
     { Inicializa los apuntadores }
     FSourcePath := '';
     FSourceTotal := 0;
     FSourcePtr := 0;
     if ( iPos > 0 ) then
     begin
          FSourcePath := ZetaCommonTools.VerificaDir( FSourceDatabase.Params.Values[ 'PATH' ] );
          i := iPos - 1;
          while ( i > 0 ) do
          begin
               if ( sQuery[ i ] = ' ' ) then
                  Break;
               Dec( i );
          end;
          sDBFFile := Trim( Copy( sQuery, i, ( iPos + Length( K_SUFIX_PTR ) - i ) ) );
          i := 0;
          while SysUtils.FileExists( FSourcePath + Format( sDBFFile, [ IntToStr( i + 1 ) ] ) ) do
          begin
               Inc( i );
          end;
          FSourceTotal := i;
          if not MoreThanOneSourceDataset then
          begin
               { Hay que dejar el query sin el K_SUFIX_PTR }
               SourceSQL.Text := Format( sQuery, [ VACIO ] );
          end;
     end;
end;

function TZetaMigrar.MoreThanOneSourceDataset: Boolean;
begin
     Result := ( FSourceTotal > 0 );
end;

function TZetaMigrar.SourceDatasetsAvailable: Boolean;
begin
     if MoreThanOneSourceDataset then
     begin
          Result := ( FSourcePtr < FSourceTotal );
     end
     else
         Result := ( FSourcePtr = 0 );
end;

procedure TZetaMigrar.OpenDatasetsSource;
var
   sQuery: String;
begin
     if FEnabled then
     begin
          if ( SourceQuery <> nil ) then
          begin
               Inc( FSourcePtr );
               sQuery := SourceSQL.Text;
               if MoreThanOneSourceDataset then
               begin
                    sQuery := Format( sQuery, [ IntToStr( FSourcePtr ) ] );
               end;
               PrepareSourceQuery( SourceQuery, sQuery );
               SourceQuery.Active := True;
          end;
     end;
end;

procedure TZetaMigrar.OpenDatasetsTarget;
begin
     if FEnabled then
     begin
          DeleteTable( TargetTableName );
          PrepareTargetQuery( TargetQuery, InsertSQL.Text );
          if Assigned( FOnOpenDatasets ) then
             FOnOpenDatasets( Self );
     end;
end;

procedure TZetaMigrar.OpenDatasets;
begin
     OpenDatasetsSource;
     OpenDatasetsTarget;
end;

procedure TZetaMigrar.CloseQuery( Query: TQuery );
begin
     if ( Query <> nil ) then
     begin
          with Query do
          begin
               Active := False;
               if Prepared then
                  UnPrepare;
          end;
     end;
end;

procedure TZetaMigrar.DoCloseDatasets;
begin
     if Assigned( FOnCloseDatasets ) then
        FOnCloseDatasets( Self );
end;

procedure TZetaMigrar.CloseDatasetsSource;
begin
     if FEnabled then
     begin
          CloseQuery( SourceQuery );
     end;
end;

procedure TZetaMigrar.CloseDatasetsTarget;
begin
     if FEnabled then
     begin
          DoCloseDatasets;
          CloseQuery( TargetQuery );
     end;
end;

procedure TZetaMigrar.CloseDatasets;
begin
     CloseDatasetsTarget;
     CloseDatasetsSource;
end;

function TZetaMigrar.BorraTabla( Bitacora: TZetaMigrarLog ): Boolean;
begin
     FHayError := False;
     FBitacora := Bitacora;
     if EmptyTargetTable then
     begin
          try
             DeleteTable( TargetTableName );
          except
                on Error: Exception do
                   HandleException( 'Error Al Borrar Tabla ' + TargetTableName, Error );
          end;
          Result := not FHayError;
          with FBitacora do
          begin
               if FHayError then
                  AgregaResumen( TargetTableName + ' NO Pudo Ser Borrada' )
               else
                   AgregaMensaje( TargetTableName + ' Fu� Borrada' );
          end;
     end
     else
         Result := True;
end;

procedure TZetaMigrar.PoblarTabla( Bitacora: TZetaMigrarLog );
var
   i, j, k: Integer;
   lOk: Boolean;
   sArchivo, sMsg: String;
begin
     FHayError := False;
     FBitacora := Bitacora;
     lOk := True;
     i := 0;
     j := 0;
     k := 1;
     try
        EfectuarTransaccion( True );
        try
           sArchivo := GetParadoxTable;
           PrepareSourceQuery( SourceQuery, Format( 'select * from %s', [ sArchivo ] ) );
           SourceQuery.Active := True;
           try
              PrepareTargetQuery( TargetQuery, '' );
              SetInsertScript( TargetTableName, SourceQuery, TargetQuery );
              with SourceQuery do
              begin
                   while not Eof and lOk do
                   begin
                        try
                           InsertFromDataset( SourceQuery, TargetQuery );
                           i := i + 1;
                        except
                              on Error: Exception do
                              begin
                                   HandleException( 'Error En Registro ' + IntToStr( j ) + ' De La Tabla ' + sArchivo, Error );
                              end;
                        end;
                        j := j + 1;
                        k := k + 1;
                        if ( k = 10 ) then
                        begin
                             SourceRecordNumber := j;
                             lOk := CallBack;
                             k := 1;
                        end;
                        Next;
                   end;
                   if not FHayError then
                      FHayError := ( i <> j );
              end;
              DoCloseDatasets;
           except
                 on Error: Exception do
                 begin
                      HandleException( 'Error Al Poblar Tabla ' + TargetTableName, Error );
                 end;
           end;
        except
              on Error: Exception do
              begin
                   HandleException( 'Error Al Abrir Tabla ' + sArchivo, Error );
              end;
        end;
        with FBitacora do
        begin
             sMsg := TargetTableName + ' Poblada %s ' + Format( '( %d / %d )', [ i, j ] );
             if FHayError then
                AgregaResumen( Format( sMsg, [ 'CON ERRORES' ] ) )
             else
                 AgregaMensaje( Format( sMsg, [ 'Con Exito' ] ) );
        end;
     finally
            EfectuarTransaccion( False );
     end;
end;

procedure TZetaMigrar.PoblarTablaDefault( Bitacora: TZetaMigrarLog );
begin
     FHayError := False;
     FBitacora := Bitacora;
     try
        PrepareTargetQuery( TargetQuery, DefaultSQL.Text );
        TargetQuery.Active := True;
        try
           with TargetTable do
           begin
                Active := False;
                DatabaseName := FSourceDatabase.DatabaseName;
                TableType := ttDefault;
                TableName := GetParadoxTable;
           end;
           with BatchMove do
           begin
                Source := TargetQuery;
                Destination := TargetTable;
                Mode := batCopy;
                Execute;
           end;
        except
              on Error: Exception do
                 HandleException( 'Error Al Crear Tabla Default ' + GetParadoxTable, Error );
        end;
     except
           on Error: Exception do
              HandleException( 'Error Al Abrir Tabla ' + DefaultSQL.Text, Error );
     end;
     with FBitacora do
     begin
          if FHayError then
             AgregaResumen( 'Tabla Default ' + TargetTableName + '.DB NO Pudo Ser Creada' )
          else
              AgregaMensaje( 'Tabla Default ' + TargetTableName + '.DB Fu� Creada' );
     end;
end;

procedure TZetaMigrar.Migrar( Bitacora: TZetaMigrarLog );
var
   i, iStep, iTxStep: Integer;
   lAccept, lContinue: Boolean;
   sMsg: String;
begin
     FHayError := False;
     FBitacora := Bitacora;
     if FEnabled then
     begin
          try
             InitCounters;
             FBitacora.InitMigracion;
             try
                OpenDataSetsTarget;
                if TargetQuery.Prepared then
                begin
                     FindSourceDatasets;
                     while SourceDatasetsAvailable do
                     begin
                          try
                             OpenDatasetsSource;
                             if ( SourceQuery <> nil ) and SourceQuery.Active then
                             begin
                                  with SourceQuery do
                                  begin
                                       iStep := 1;
                                       iTxStep := 1;
                                       lContinue := True;
                                       EfectuarTransaccion( True );
                                       try
                                          while not Eof and lContinue do
                                          begin
                                               FSourceRecordNumber := FSourceRecordNumber + 1;
                                               for i := 1 to FMultiples do
                                               begin
                                                    FCounter := i;
                                                    lAccept := True;
                                                    try
                                                       if Assigned( FOnFilterRecord ) then
                                                          FOnFilterRecord( SourceQuery, lAccept );
                                                       if lAccept then
                                                       begin
                                                            FSourceRecordAccept := FSourceRecordAccept + 1;
                                                            ClearParameters( TargetQuery );
                                                            with TargetQuery do
                                                            begin
                                                                 FOnMoveExcludedFields( Self );
                                                                 ExecSQL;
                                                                 if Assigned( FOnMoveOtherTables ) then
                                                                    FOnMoveOtherTables( Self );
                                                            end;
                                                            FSourceRecordOK := FSourceRecordOK + 1;
                                                       end;
                                                    except
                                                          on Error: Exception do
                                                             HandleException( TargetTableName + ' @ ' + IntToStr( FSourceRecordNumber ) + ':' + IntToStr( FCounter ), Error );
                                                    end;
                                               end;
                                               Next;
                                               iStep := iStep + 1;
                                               iTxStep := iTxStep + 1;
                                               if ( iTxStep > FTxCount ) then
                                               begin
                                                    EfectuarTransaccion( True );
                                                    iTxStep := 1;
                                               end;
                                               if ( iStep > FCallBackCount ) then
                                               begin
                                                    lContinue := CallBack;
                                                    iStep := 1;
                                               end;
                                          end;
                                       except
                                             on Error: Exception do
                                                HandleException( TargetTableName + ' : ', Error );
                                       end;
                                       EfectuarTransaccion( False );
                                  end;
                             end;
                          except
                                on Error: Exception do
                                begin
                                     HandleException( Format( 'Error Al Abrir Tabla Fuente De %s ( %s )', [ TargetTableName, SourceSQL.Text ] ), Error );
                                end;
                          end;
                          CloseDatasetsSource;
                     end;
                     try
                        if ( FAdditionalRows > 0 ) and Assigned( FOnAdditionalRows ) then
                        begin
                             EfectuarTransaccion( True );
                             try
                                for i := 1 to FAdditionalRows do
                                begin
                                     FCounter := i;
                                     FSourceRecordNumber := FSourceRecordNumber + 1;
                                     try
                                        ClearParameters( TargetQuery );
                                        FSourceRecordAccept := FSourceRecordAccept + 1;
                                        with TargetQuery do
                                        begin
                                             FOnAdditionalRows( Self );
                                             ExecSQL;
                                        end;
                                        FSourceRecordOK := FSourceRecordOK + 1;
                                     except
                                           on Error: Exception do
                                           begin
                                                HandleException( TargetTableName + ' >> ' + IntToStr( FCounter ), Error );
                                           end;
                                     end;
                                end;
                             finally
                                    EfectuarTransaccion( False );
                             end;
                        end;
                     except
                           on Error: Exception do
                           begin
                                HandleException( Format( 'Error Al Migrar Adicionales De %s', [ TargetTableName ] ), Error );
                           end;
                     end;
                end;
             except
                   on Error: Exception do
                   begin
                        HandleException( 'Error Al Abrir Tabla ' + TargetTableName, Error );
                   end;
             end;
             try
                CloseDataSetsTarget;
             except
                   on Error: Exception do
                   begin
                        HandleException( 'Error Al Cerrar Tabla ' + TargetTableName, Error );
                   end;
             end;
             with FBitacora do
             begin
                  sMsg := StartMessage + ' ( ' + TargetTableName + ' ) Migrada %s' + ' Al ' + Trim( Format( '%7.1n', [ SuccessRate ] ) ) + ' %s ( ' + Trim( Format( '%8.0n', [ SourceRecordNumber / 1 ] ) ) + ' Registros )';
                  if FHayError or ( Avisos > 0 ) then
                  begin
                       AgregaMensaje( Format( sMsg, [ 'CON ERRORES', '%' ] ) );
                       AgregaResumenOnly( '' );
                       if ( Avisos > 0 ) then
                          AgregaResumenOnly( Format( sMsg, [ Format( 'Con %d AVISOS', [ Avisos ] ), '%' ] ) )
                       else
                           AgregaResumenOnly( Format( sMsg, [ '', '%' ] ) );
                  end
                  else
                      AgregaMensaje( Format( sMsg, [ '', '%' ] ) );
             end;
          finally
                 EfectuarTransaccion( False );
          end;
     end;
end;

procedure TZetaMigrar.HandleException( const sMensaje: String; Error: Exception );
begin
     FBitacora.HandleException( sMensaje, Error );
     FHayError := True;
end;

procedure TZetaMigrar.AgregaError( const sMensaje: String );
begin
     FBitacora.AgregaError( sMensaje );
     FHayError := True;
end;

procedure TZetaMigrar.EfectuarTransaccion( const lStartTransaction: Boolean );
begin
     with FTargetDatabase do
     begin
          if InTransaction then
             Commit;
          if not InTransaction and lStartTransaction then
             StartTransaction;
     end;
end;

procedure TZetaMigrar.ClearParameters( Query: TQuery );
var
   i: Integer;
begin
     with Query do
     begin
          for i := ( Params.Count - 1 ) downto 0 do
              SetToNull( Params.Items[ i ], Params.Items[ i ].DataType );
     end;
end;

end.
