unit ZetaKeyCombo;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, StdCtrls, DB, DBCtrls,
     ZetaCommonLists;

type
  TZetaKeyCombo = class( TComboBox )
  private
    { Private declarations }
    FLista: TStrings;
    FTempItems: TStrings;//acl
    FListaFija: ListasFijas;
    FListaVariable: ListasVariables;
    FOffset: Integer;
    FOpcional: Boolean;
    FEsconderVacios: Boolean; {acl Atributo que inicializa los combos de TipoNomina como True para esconder campos vacios.}    
    function CanLoadList: Boolean;
    function GetLlave: String;
    function GetLlaveEntero: Integer;
    function GetLlaves( Index: Integer ): String;
    function GetDescripcion: String;
    function GetDescripciones( Index: Integer ): String;
    function GetValor: Integer;
    procedure SetLlave( const Value: String );
    procedure SetLlaveEntero( const Value: Integer );
    procedure SetValor( Value: Integer );
    procedure LeeListaFija;
  protected
    { Protected declarations }
    function CalculaIndice( const Index: Integer ): Integer;
    function CalculaValor( const Valor: Integer ): Integer;
    {$ifdef ADUANAS}
    function GetListaPtr( const sValue: String ): Integer;
    function EsListaNumerica: Boolean;
    {$endif}
    procedure KeyUp( var Key: Word; Shift: TShiftState ); override;
    procedure Loaded; override;
    procedure SetLista( Value: TStrings ); virtual;
    procedure SetListaFija( Value: ListasFijas ); virtual;
    procedure UpdateItems( Sender: TObject ); virtual;
  public
    { Public declarations }
    property Descripcion: String read GetDescripcion;
    property Descripciones[ Index: Integer ]: String read GetDescripciones;
    property Lista: TStrings read FLista write SetLista;
    property Llave: String read GetLlave write SetLlave;
    property LlaveEntero: Integer read GetLlaveEntero write SetLlaveEntero;
    property Llaves[ Index: Integer ]: String read GetLlaves;
    property Valor: Integer read GetValor write SetValor;
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
  published
    { Published declarations }
    property ListaFija: ListasFijas read FListaFija write SetListaFija;
    property ListaVariable: ListasVariables read FListaVariable write FListaVariable;
    property Offset: Integer read FOffset write FOffset;
    property Opcional: Boolean read FOpcional write FOpcional;
    property EsconderVacios: Boolean read FEsconderVacios write FEsconderVacios; //acl    
  end;
  TZetaDBKeyCombo = class( TZetaKeyCombo )
  private
    { Private declarations }
    FLlaveNumerica: Boolean;
    FDataLink: TFieldDataLink;
    function CheckConnection: Boolean;
    function GetComboText: String;                  { Basada en TDBComboBox.GetComboText }
    function GetDataField: String;
    function GetDataSource: TDataSource;
    function GetField: TField;
    function GetReadOnly: Boolean;
    procedure SetComboText;                         { Basada en TDBComboBox.SetComboText }
    procedure SetDataField( const Value: String );
    procedure SetDataSource( Value: TDataSource );
    procedure SetReadOnly( const Value: Boolean );
    procedure CheckFieldType( const Value: String );
    procedure DataChange( Sender: TObject );
    procedure UpdateData( Sender: TObject );
    procedure ActiveChange( Sender: TObject );
    procedure CMExit( var Msg: TCMExit ); message CM_EXIT;
  protected
    { Protected declarations }
    procedure Notification( AComponent: TComponent; Operation: TOperation ); override;
    procedure Change; override;
    procedure Click; override;
    procedure Enfocar;
    procedure KeyDown( var Key: Word; Shift: TShiftState ); override;
    procedure KeyPress( var Key: Char ); override;
    procedure SetStyle( Value: TComboBoxStyle ); override;
    procedure SetItems( const Value: TStrings ); override;
    procedure SetLista( Value: TStrings ); override;
    procedure SetListaFija( Value: ListasFijas ); override;
    procedure UpdateItems( Sender: TObject ); override;
    procedure WndProc(var Message: TMessage); override; { basada en TDBComboBox.WndProc }
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    property Field: TField read GetField;
  published
    { Published declarations }
    property DataField: String read GetDataField write SetDataField;
    property DataSource: TDataSource read GetDataSource write SetDataSource;
    property Items write SetItems;
    property LlaveNumerica: Boolean read FLlaveNumerica write FLlaveNumerica;
    property ReadOnly: Boolean read GetReadOnly write SetReadOnly default False;
  end;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools;

{ ************* TZetaKeyCombo *************** }

constructor TZetaKeyCombo.Create( AOwner: TComponent );
begin
     inherited Create( AOwner );
     FEsconderVacios := False; //acl     
     FLista := TStringList.Create;
     FTempItems := TStringList.Create; //acl     
     TStringList( FLista ).OnChange := UpdateItems;
     FOpcional := False;
     {$ifdef VER150}
     AutoDropDown := False;
     AutoComplete := False;
     {$endif}
     inherited SetStyle( csDropDownList );
     //DevEx: New Code
     Ctl3D:= False;
     ParentCtl3D:= False;
     BevelKind := bkFlat;
end;

destructor TZetaKeyCombo.Destroy;
begin
     TStringList( FLista ).OnChange := nil;
     FLista.Free;
     FTempItems.Free;//acl
     inherited Destroy;
end;

procedure TZetaKeyCombo.Loaded;
begin
     inherited Loaded;
     LeeListaFija;
end;

{$ifdef ADUANAS}
function TZetaKeyCombo.EsListaNumerica: Boolean;
begin
     Result := ( FListaFija = lfNinguna ) and ( Lista.Count > 0 );
end;
{$endif}

function TZetaKeyCombo.CalculaIndice( const Index: Integer ): Integer;
begin
     Result := ( Index - Offset );
end;

function TZetaKeyCombo.CalculaValor( const Valor: Integer ): Integer;
begin
     Result := ( Valor + Offset );
end;

function TZetaKeyCombo.CanLoadList: Boolean;
begin
     Result := not ( csDesigning in ComponentState );
end;

function TZetaKeyCombo.GetLlave: String;
begin
     if ( ItemIndex < 0 ) then
        Result := ''
     else
         Result := GetLlaves( ItemIndex );
end;

{$ifdef ADUANAS}
function TZetaKeyCombo.GetListaPtr( const sValue: String ): Integer;
var
   i: Integer;
begin
     Result := -1;
     with Lista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if ( Trim( Names[ i ] ) = sValue ) then
               begin
                    Result := i;
                    Break;
               end;
          end;
     end;
end;
{$endif}

function TZetaKeyCombo.GetLlaveEntero: Integer;
begin
     Result := StrToIntDef( GetLlave, 0 );
end;

function TZetaKeyCombo.GetLlaves( Index: Integer ): String;
begin
     Result := FLista.Names[ CalculaIndice( Index ) ];
end;

function TZetaKeyCombo.GetDescripcion: String;
begin
     if ( ItemIndex < 0 ) then
        Result := ''
     else
         Result := GetDescripciones( ItemIndex );
end;

function TZetaKeyCombo.GetDescripciones( Index: Integer ): String;
var
   sLlave: String;
begin
     sLlave := GetLlaves( Index );
     if ( sLlave <> '' ) then
        Result := FLista.Values[ sLlave ]
     else
         Result := '';
end;

function TZetaKeyCombo.GetValor: Integer;
begin
     Result := CalculaValor( ItemIndex );
end;

procedure TZetaKeyCombo.SetLista( Value: TStrings );
begin
     if ( FListaFija = lfNinguna ) and CanLoadList then
     begin
          FLista.Assign( Value );
          UpdateItems( Self );
     end;
end;

procedure TZetaKeyCombo.SetLlave( const Value: String );
begin
     SetValor( CalculaValor( FLista.IndexOfName( Value ) ) );
end;

procedure TZetaKeyCombo.SetLlaveEntero( const Value: Integer );
begin
     SetLlave( IntToStr( Value ) );
end;

procedure TZetaKeyCombo.SetValor( Value: Integer );
begin
     Value := CalculaIndice( Value );
     if ( Value >= 0 ) and ( Value < Items.Count ) then
     begin
          Change;
          ItemIndex := Value;
     end;
end;

procedure TZetaKeyCombo.UpdateItems( Sender: TObject );
var
   i: Integer;
begin
     with Items do
     begin
          try
             BeginUpdate;
             Clear;
             for i := 0 to ( FLista.Count - 1 ) do
                 Add( GetDescripciones( i ) );
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TZetaKeyCombo.LeeListaFija;
var
   i, iMax, iPos: Integer;
begin
     FTempItems.Clear;
     if ( FListaFija <> lfNinguna ) and CanLoadList then
     begin
          with Lista do
          begin
               try
                  BeginUpdate;
                  Clear;
               finally
                      EndUpdate;
               end;
          ZetaCommonLists.LlenaLista( FListaFija, Items );
{acl. Si el atributo esta vacio se agregan los items de la lista a uno Tstring temporal recorres el ciclo y
almacena los items en el temporal}
              if ( EsconderVacios ) then
              begin
                   iMax:= Items.Count;
                   iPos:=0;
                   FTempItems.AddStrings( Items );
                   for i := 0 to ( iMax - 1 ) do
                   begin
                        if (FListaFija = lfTipoPeriodo) or (FListaFija = lfTipoPeriodoConfidencial) or ( FListaFija = lfTipoNomina) then  //Listas que ya vienen en formato Key=Value
                        begin
                              //Valida si hay valor asignado en la cadena 'key=value'
                              iPos := Pos('=', FTempItems[i]);
                              if (iPos > 0) and strLleno(Copy(FTempItems[i], iPos + 1, Length(FTempItems[i]))) then
                                 Add( FTempItems[i] );
                        end
                        else
                        begin
                             if StrLleno( FTempItems[i] ) then
                                 Add( IntToStr(i) + '=' + FTempItems[i]  );
                        end;
                   end;
              end;          
          end;   
     end;
end;

procedure TZetaKeyCombo.SetListaFija( Value: ListasFijas );
begin
     FListaFija := Value;
     LeeListaFija;
end;

procedure TZetaKeyCombo.KeyUp( var Key: Word; Shift: TShiftState );
begin
     if FOpcional then
     begin
          if DroppedDown then
          begin
               if ( ( Key = VK_DELETE) or ( Key = VK_BACK ) ) and ( Text = '' ) then
               begin
                    ItemIndex := -1;
                    Change;
               end;
          end
          else
          begin
               if ( ( Key = VK_DELETE ) or ( Key = VK_BACK ) ) and ( Style = csDropDownList ) then
               begin
                    Text := '';
                    ItemIndex := -1;  { 8/22/96}
                    Change;
               end
          end;
     end;
     inherited KeyUp( Key, Shift );
end;

{ ************* TZetaDBKeyCombo *************** }

constructor TZetaDBKeyCombo.Create( AOwner: TComponent );
begin
     inherited Create( AOwner );
     FLlaveNumerica := True;
     FDataLink := TFieldDataLink.Create;
     with FDataLink do
     begin
          Control := Self;
          OnDataChange := DataChange;
          OnUpdateData := UpdateData;
          OnActiveChange := ActiveChange;
     end;
     if not ( csDesigning in ComponentState ) then
        CheckFieldType( DataField );
end;

destructor TZetaDBKeyCombo.Destroy;
begin
     with FDataLink do
     begin
          OnActiveChange := nil;
          OnUpdateData := nil;
          OnDataChange := nil;
          Control := nil;
          Free;
     end;
     FDataLink := nil;
     inherited Destroy;
end;

procedure TZetaDBKeyCombo.Notification( AComponent: TComponent; Operation: TOperation );
begin
     inherited Notification( AComponent, Operation );
     if ( Operation = opRemove ) and
        ( FDataLink <> nil ) and
        ( AComponent = DataSource ) then
        DataSource := nil;
end;

procedure TZetaDBKeyCombo.SetItems(const Value: TStrings );
begin
     Items.Assign( Value );
     FDataLink.OnDataChange( Self );
end;

procedure TZetaDBKeyCombo.SetLista( Value: TStrings );
begin
     inherited SetLista( Value );
     FDataLink.OnDataChange( Self );
end;

procedure TZetaDBKeyCombo.SetListaFija( Value: ListasFijas );
begin
     inherited SetListaFija( Value );
     FDataLink.OnDataChange( Self );
end;

procedure TZetaDBKeyCombo.SetStyle( Value: TComboBoxStyle );
begin
end;

function TZetaDBKeyCombo.GetDataField: String;
begin
     Result := FDataLink.FieldName;
end;

function TZetaDBKeyCombo.GetField: TField;
begin
     Result := FDataLink.Field;
end;

procedure TZetaDBKeyCombo.SetDataField( const Value: String );
begin
     CheckFieldType( Value );
     FDataLink.FieldName := Value;
end;

function TZetaDBKeyCombo.GetDataSource: TDataSource;
begin
     Result := FDataLink.DataSource;
end;

procedure TZetaDBKeyCombo.SetDataSource( Value: TDataSource );
begin
     if ( FDatalink.DataSource <> Value ) then
     begin
          FDataLink.DataSource := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

function TZetaDBKeyCombo.GetReadOnly: Boolean;
begin
     Result := FDataLink.ReadOnly;
end;

function TZetaDBKeyCombo.CheckConnection: Boolean;
begin
     Result := ( FDataLink <> nil ) and
               ( FDataLink.Dataset <> nil ) and
               ( FDataLink.Dataset.Active );
end;

procedure TZetaDBKeyCombo.SetReadOnly( const Value: Boolean );
begin
     FDataLink.ReadOnly := Value;
end;

procedure TZetaDBKeyCombo.CheckFieldType( const Value: String );
var
   FieldType: TFieldType;
begin
     if ( Value <> '' ) and CheckConnection then
     begin
          FieldType := FDataLink.Dataset.FieldByName( Value ).DataType;
          if FLlaveNumerica then
          begin
               if not ( FieldType in [ ftInteger, ftSmallInt, ftWord ] ) then
                  raise EInvalidFieldType.Create( 'TZetaDBKeyComboBox.DataField can only ' +
                                                  'be connected to columns of type ' +
                                                  'Integer, SmallInt, Word' );
          end
          else
              if not ( FieldType in [ ftString ] ) then
                 raise EInvalidFieldType.Create( 'TZetaDBKeyComboBox.DataField can only ' +
                                                 'be connected to columns of type String' );
     end;
end;

procedure TZetaDBKeyCombo.Change;
begin
     FDataLink.Edit;
     inherited Change;
     FDataLink.Modified;
end;

procedure TZetaDBKeyCombo.Click;
begin
     FDataLink.Edit;
     inherited Click;
     FDataLink.Modified;
end;

procedure TZetaDBKeyCombo.KeyDown( var Key: Word; Shift: TShiftState );
begin
     inherited KeyDown( Key, Shift );
     // Need to handle Old Fashioned Cut, Paste, and Delete keystrokes //
     if ( Key = VK_DELETE ) or ( ( Key = VK_INSERT ) and ( ssShift in Shift ) ) then
        FDataLink.Edit;
end;

procedure TZetaDBKeyCombo.KeyPress( var Key: Char );
begin
     inherited KeyPress( Key );
{$ifdef FALSE}
     // Impide que se pueda navegar usando A..Z //
     // The field object referenced by FDataLink will tell us which keys are valid //
     if ( Key in [ #32..#255 ] ) and
        ( FDataLink.Field <> nil ) and
        not FDataLink.Field.IsValidChar( Key ) then
        begin
             MessageBeep( 0 );
             Key := #0;   // Invalid keys are discarded //
        end;
{$endif}
     case Key of
          ^H, ^V, ^X, #32..#255: FDataLink.Edit; // Put corresponding Dataset into Edit mode
          #27:                                   // Escape key pressed
          begin
               FDataLink.Reset;
               SelectAll;
               Key := #0;
          end;
     end;
end;

procedure TZetaDBKeyCombo.DataChange( Sender: TObject );
begin
     if DroppedDown then
        Exit;
     if ( FDataLink.Field <> nil ) then
        SetComboText;
end;

procedure TZetaDBKeyCombo.UpdateData(Sender: TObject);
begin
     if FLlaveNumerica then
    {$ifdef ADUANAS}
     begin
          if EsListaNumerica then
          begin
               FDataLink.Field.AsInteger := LlaveEntero;
          end
          else
          begin
     {$endif}
               if ( EsconderVacios ) then
                  FDataLink.Field.AsInteger := LlaveEntero
               else
                   FDataLink.Field.AsInteger := CalculaValor( ItemIndex )
    {$ifdef ADUANAS}
          end
     end
     {$endif}
     else
         FDataLink.Field.AsString := Llave;
end;

procedure TZetaDBKeyCombo.ActiveChange( Sender: TObject );
begin
     if ( FDataLink <> nil ) and FDataLink.Active then
        CheckFieldType( DataField );
end;

procedure TZetaDBKeyCombo.WndProc( var Message: TMessage );
begin
     if not (csDesigning in ComponentState) then
        case Message.Msg of
             WM_COMMAND:
             if ( TWMCommand( Message ).NotifyCode = CBN_SELCHANGE ) then
                if not FDataLink.Edit then
                begin
                     if ( Style <> csSimple ) then
                        PostMessage( Handle, CB_SHOWDROPDOWN, 0, 0 );
                     Exit;
                end;
             CB_SHOWDROPDOWN:
             if ( Message.WParam <> 0 ) then
                FDataLink.Edit
             else
                 if not FDataLink.Editing then
                    DataChange( Self );
        end;
    inherited WndProc( Message );
end;

procedure TZetaDBKeyCombo.CMExit( var Msg: TCMExit );
begin
     try
        with FDataLink do
        begin
             UpdateRecord;
        end;
     except
           Enfocar;
           raise;
     end;
     inherited;
end;

procedure TZetaDBKeyCombo.Enfocar;
begin
     if CanFocus then
     begin
          SelectAll;
          SetFocus;
     end;
end;

procedure TZetaDBKeyCombo.SetComboText;
var
   Redraw: Boolean;
   Value: String;
   i, iPtr: Integer;
begin
     if FLlaveNumerica then
     begin
          {$ifdef ADUANAS}
          if EsListaNumerica then
          begin
               Value := IntToStr( FDataLink.Field.AsInteger );
               iPtr := GetListaPtr( Value );
               if ( iPtr < 0 ) then
                  Value := ''
               else
                   Value := Items.Strings[ iPtr ];
          end
          else
          begin
          {$endif}

               if ( EsconderVacios ) then
               begin
                    Value := FLista.Values[ FDataLink.Field.AsString ];
               end
               else
               begin
                    i := CalculaIndice( FDataLink.Field.AsInteger );
                    if ( i >= 0 ) then
                       Value := Items[ i ]
                    else
                        Value := '';
               end;
          {$ifdef ADUANAS}
          end;
          {$endif}
     end
     else
     begin
          Value := FDataLink.Field.AsString;
          {$ifdef ADUANAS}
          iPtr := GetListaPtr( Value );
          {$else}
          iPtr := -1;
          with Lista do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    if ( Trim( Names[ i ] ) = Value ) then
                    begin
                         iPtr := i;
                         Break;
                    end;
               end;
          end;
          {$endif}
          if ( iPtr < 0 ) then
             Value := ''
          else
              Value := Items.Strings[ iPtr ];
     end;
     if ( Value <> GetComboText ) then
     begin
          Redraw := ( Style <> csSimple ) and HandleAllocated;
          if Redraw then
             SendMessage( Handle, WM_SETREDRAW, 0, 0 );
          try
             if ( Value = '' ) then
                i := -1
             else
                 i := Items.IndexOf( Value );
             ItemIndex := i;
          finally
                 if Redraw then
                 begin
                      SendMessage( Handle, WM_SETREDRAW, 1, 0 );
                      Invalidate;
                 end;
          end;
     end;
end;

function TZetaDBKeyCombo.GetComboText: String;
var
   i: Integer;
begin
     if Style in [ csDropDown, csSimple ] then
        Result := Text
     else
     begin
          i := ItemIndex;
          if ( i < 0 ) then
             Result := ''
          else
              Result := Items[ i ];
     end;
end;

procedure TZetaDBKeyCombo.UpdateItems(Sender: TObject);
begin
     inherited UpdateItems( Sender );
     if not FLlaveNumerica and CheckConnection then
     begin
          SetComboText;
     end;
end;

end.
