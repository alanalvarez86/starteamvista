unit ZetaGridChart;

interface

uses
  Classes, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, Vcl.Graphics,
  Vcl.Controls, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, Data.DB, cxDBData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxGridLevel,
  cxGridChartView, cxGridDBChartView, cxClasses, cxGridCustomView, cxGrid, Vcl.StdCtrls, ExtCtrls,
  dxSkinsCore, dxSkinscxPCPainter, cxPC, Datasnap.DBClient, ZetaClientDataSet;

type
  TZetaGridChart = class;

  TZetaGridDBChartSeries = class( TcxGridDBChartSeries )
  public
        constructor Create(AOwner: TComponent); override;
  end;

  TZetaStyleTitulo = class( TcxStyle )
  private
  protected
  public
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
  published
  end;

  TZetaGridChartViewStyles = class( TcxGridChartViewStyles )
  private
  protected
  public
    constructor Create( AOwner: TPersistent ); override;
    destructor Destroy; override;
  published
  end;

  TZetaDBChartView = class( TcxGridDBChartView )
  private
    FStyleTitulo: TcxStyle;
    procedure GetValueHintText(Sender: TcxGridChartView; ASeries: TcxGridChartSeries; AValueIndex: Integer; var AHint: String);
  protected
    procedure CreateOptions; override;
  public
    constructor Create( AOwner: TComponent ); override;
    function GetSeriesClass: TcxGridChartSeriesClass; override;
  published
  end;

  //Evento
  TZetaAlAdquirirDatosEvent = procedure( Sender: TZetaGridChart ) of object;

  TZetaGridChart = class( TcxGrid )
  private
    FQuery: String;
    FZetaAlAdquirirDatosEvent: TZetaAlAdquirirDatosEvent;
    function GetQuery: String;
    procedure SetQuery( const Value: String );
    { Private declarations }
  protected
    function GetDefaultViewClass: TcxCustomGridViewClass; override;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    procedure Graficar;
  published
    { Published declarations }
    property Query: String read GetQuery write SetQuery;
    property AlAdquirirDatosChart: TZetaAlAdquirirDatosEvent read FZetaAlAdquirirDatosEvent write FZetaAlAdquirirDatosEvent;
  end;

procedure Register;

implementation

constructor TZetaGridChart.Create( AOwner: TComponent );
begin
     inherited Create( AOwner );
     Self.LevelTabs.CaptionAlignment := taLeftJustify;
     Self.LevelTabs.Slants.Kind := skCutCorner;
     Self.LevelTabs.Style := 6;
     Self.BorderStyle := cxcbsNone;
     Self.Height := 273;
     Self.Width := 433;
end;

procedure Register;
begin
     RegisterComponents( 'Tress Client', [TZetaGridChart] );
end;

destructor TZetaGridChart.Destroy;
begin
    inherited Destroy;
end;

function TZetaGridChart.GetDefaultViewClass: TcxCustomGridViewClass;
begin
     Result := TZetaDBChartView;
end;

function TZetaGridChart.GetQuery: String;
begin
     Result := Self.FQuery;
end;

//Graficar Componente
procedure TZetaGridChart.Graficar;
begin
     if Assigned( AlAdquirirDatosChart ) then
     begin
          AlAdquirirDatosChart( Self );
     end;
end;

procedure TZetaGridChart.SetQuery( const Value: String );
begin
     Self.FQuery := Value;
end;

{ TZetaDBChartView }
constructor TZetaDBChartView.Create( AOwner: TComponent );
begin
     inherited Create( AOwner );
     Self.DragMode := dmAutomatic;
     Self.Categories.DataBinding.FieldName := 'ID';
     Self.Categories.DisplayText := 'Order';
     Self.DataController.DataModeController.SmartRefresh := True;
     Self.DiagramColumn.Active := True;
     Self.DiagramPie.Legend.Position := cppRight;
     Self.Legend.Position := cppNone;
     Self.OptionsCustomize.DataDrillDown := False;
     Self.OptionsCustomize.DataGroupMoving := False;
     Self.OptionsCustomize.OptionsCustomization := False;
     Self.OptionsCustomize.SeriesCustomization := False;
     Self.OptionsView.CategoriesPerPage := 10;
     Self.ToolBox.Border := tbNone;
     Self.ToolBox.DataLevelsInfoVisible := dlivNever;
     Self.ToolBox.DiagramSelector := True;
     Self.Title.Text := '<TITULO DE LA GRAFICA>';
     Self.Title.Alignment := cpaStart;
     Self.Title.Position  := cppDefault;
     FStyleTitulo := TZetaStyleTitulo.Create( self );
     Self.Styles.Title :=FStyleTitulo;
     Self.OnGetValueHint := GetValueHintText;
end;

procedure TZetaDBChartView.CreateOptions;
begin
     inherited;
end;

function TZetaDBChartView.GetSeriesClass: TcxGridChartSeriesClass;
begin
     Result := TZetaGridDBChartSeries;
end;

procedure TZetaDBChartView.GetValueHintText(Sender: TcxGridChartView; ASeries: TcxGridChartSeries; AValueIndex: Integer; var AHint: String);
const
       K_CHART_VALUE_HINT_FORMAT = '%s para %s es %s';  // series display text, category, value
begin
     AHint := Format(cxGetResourceString(K_CHART_VALUE_HINT_FORMAT),
     [ASeries.GetDisplayText, Sender.Categories.VisibleDisplayTexts[AValueIndex],
     ASeries.VisibleDisplayTexts[AValueIndex]]);
end;

constructor TZetaGridDBChartSeries.Create( AOwner: TComponent );
begin
     inherited Create( AOwner );
end;

{ TZetaStyleTitulo }
constructor TZetaStyleTitulo.Create( AOwner: TComponent );
begin
     inherited Create( AOwner );
     with Self do
     begin
          Name := CreateUniqueName( AOwner, nil, Self, '', '' );
          Color := clWhite;
          Font.Charset := DEFAULT_CHARSET;
          Font.Color := clWindowText;
          Font.Height := -27;
          Font.Name := 'Segoe UI';
          Font.Orientation := 0;
          Font.Pitch := fpDefault;
          Font.Size := 14;
          Font.Style := [fsBold];
          TextColor := clWindowText;
     end;
end;

destructor TZetaStyleTitulo.Destroy;
begin
     inherited;
end;

constructor TZetaGridChartViewStyles.Create( AOwner: TPersistent );
begin
     inherited Create( AOwner );
end;

destructor TZetaGridChartViewStyles.Destroy;
begin
     inherited;
end;

initialization
  Classes.RegisterClasses( [TZetaGridDBChartSeries, TZetaStyleTitulo] );
  cxGridRegisteredViews.Register( TZetaDBChartView, 'ZetaDBChartView' );

finalization
  cxGridRegisteredViews.Unregister( TZetaDBChartView );

end.

