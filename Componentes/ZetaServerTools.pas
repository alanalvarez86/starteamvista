unit ZetaServerTools;

interface

{$define IMSS_VACA}   //Corregir d�as IMSS cuando hay vacaciones
{$define CONFIDENCIALIDAD_MULTIPLE}

uses SysUtils, Classes, DB,
     {$ifndef DOTNET}
     Windows, Controls,
     {$ifndef DOS_CAPAS}
     ComObj,
     {$endif}
     {$endif}
     Variants,
     ZetaCommonClasses,
     ZetaCommonLists;

    function Encrypt( const sSource: String ): String;
    function Decrypt( const sSource: String ): String;
    function EncryptDB( const sSource: String ): String;
    function DecryptDB( const sSource: String ): String;

    function dMax( const dValor1, dValor2: TDate ): TDate;
    function dMin( const dValor1, dValor2: TDate ): TDate;

    function ConvierteALista( const sLista: String; const lEntero: Boolean; Lista: TStrings ): String;
    function CampoInLista( const sCampo, sLista : String ) : String;
    function CTOD( const sFecha: String ): TDate;
    function GetImportedDate( const sData: String ): TDate;
    function CampoAsVar( oCampo: TField ): Variant;
    function CampoOldAsVar( oCampo: TField ): Variant;
    function CambiaCampo( oCampo: TField ): Boolean;
    function GetCampoNivelProg( const iNivel : integer ) : String;
    function DiasEnMes( const wYear, wMonth: Word ): Word;
    function Round2Integer( const rValor: Extended ): Integer;
    function Nivel0( const Empresa : OleVariant; const sTabla : string = VACIO ) : String;
    function GetFiltroNivel0( const Empresa : OleVariant; const sTabla : string = VACIO ) : String;
    function GetDiasRango( const dInicio, dFinal, dRangoIni, dRangoFin: TDate ): Integer;
    function GetTipoCompany( const iTipo: Integer ): String;
    function GetTipoCompanyBD( const iTipo: Integer ): String;
    function GetTipoCompanyMasPresupuestos : String;
    function GetTipoCompanyDerechos( const iTipo: Integer ): String;
    function GetTipoCompanyDerechosGrupoUsuarios( const iTipo: Integer ): String;
    function GetTipoCompanyEnum( const sValor: string ): eTipoCompany;
    {$ifndef DOTNET}
    {$ifndef DOS_CAPAS}
    function GetEnablePooling: Boolean;
    function GetThreadingModel: TThreadingModel;
    {$endif}
    {$endif}
    function GetSupportsCOMPlus: Boolean;
    function SetMayusculasSQL( const sTexto: String ): String;       // Util al Comparar Strings con UPPER( Campo ) en Scripts de SQL
    function FormateaHora( const sHora: String ): String;
    function RegistrarBitacora( const eClase : eClaseBitacora; const sBitacora : string ): Boolean;
    function GetExceptionInfo( Error: Exception ): string;

    function CambiaCamposRetroactivos( const sOriginal: string ): string;
    function FormateaFlotanteEvaluador( const rValor: TTasa ): String;
    function GetMsgStatusEmpleado( const iEmpleado: Integer; const dFecha: TDate; const Status: eStatusEmpleado ): String;

    function ProximidadHexToDec( const sHexValue, sFacilityCode: String; const iMaxSize: Integer ): String;
    function ProximidadDecToHex( const sDecValue, sFacilityCode: String; const iMaxSize: Integer ): String;

    function DiasSemanaJornada( const TipoJornada: eTipoJornada ): Word;
    function GetAusentismosJornadaReducida(const TipoJornada: eTipoJornada; const iFaltas: integer ): integer;
    function ColFechaResult( const iColumna: Integer ): String;
    function AnexaParamProceso( const sTexto, sParam, sTitulo: String ): String;

    procedure GetServerDatabase( const sValue: String; var sServer, sDatabase: String );
    procedure GetFiltroEmpleado( const sRangoLista, sCondicion, sFiltro: string; const lFiltro: boolean; var sParametros: string; var sFiltroEmp: string; var sFormula: string );
    procedure RevisaHorasTope24( var dFecha: TDate; var sHora: String );

    function GetCompanyChecksumFromData(  const sCompany, sControlReal, sControl, sDatos : String; const dFecha : TDate ) : string;
    procedure GetCompanyDataFromChecksum( const sCheckSum: String; var sCompany, sControlReal, sControl, sDatos : String; var dFecha : TDate );


    function EvaluaResultadoConflicto( const StatusExp: eStatusConflictoExp; const iEmpleado: Integer; const dInicial, dFinal: TDate;
                                       var sConflicto: String; const lAjuste: Boolean = FALSE ) : Boolean;
    function CampoTexto( const Valor : Variant; const Tipo : TFieldType ): String;

    function NoEsCampoLLavePortal( const sField: string ):Boolean;overload;
    function NoEsCampoLLavePortal( Field: TField ):Boolean;overload;

    function GetTipoDia(const StatusEmpleado: eStatusEmpleado; const lFestivo, lFestivoEnDescanso{$ifdef IMSS_VACA}, lDiasCotizaSinFV{$endif}: Boolean; const StatusDia: eStatusAusencia ): eTipoDiaAusencia;
    function NoEsHabil( const eTipoDia: eStatusAusencia; const eExtrasSabado, eExtrasDescanso: eHorasExtras ): Boolean;

const
     { Tabla CLAVE de Comparte }
     CLAVE_AUTORIZACION            = 1;
     CLAVE_VENCIMIENTO             = 2;
     CLAVE_LIMITE_PASSWORD         = 3;
     CLAVE_INTENTOS                = 4;
     CLAVE_DIAS_INACTIVOS          = 5;
     CLAVE_TIEMPO_INACTIVO         = 6;
     CLAVE_MIN_LETRAS_PASSWORD     = 7;
     CLAVE_MIN_DIGITOS_PASSWORD    = 8;
     CLAVE_MAX_LOG_PASSWORD        = 9;
     CLAVE_SISTEMA_BLOQUEADO       = 10;
     CLAVE_USUARIO_TRESSAUTOMATIZA = 11; // (JB) Se agrega la Configuracion de Tress Automatiza
     CLAVE_MOSTRAR_ADV_LICENCIA    = 12; // US #9666 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
     CLAVE_SERVIDOR_CORREOS        = 13; // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
     CLAVE_PUERTO_SMTP             = 14; // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
     CLAVE_AUTENTIFICACION_CORREO  = 15; // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
     CLAVE_USER_ID                 = 16; // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
     CLAVE_EMAIL_PSWD              = 17; // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
     CLAVE_RECIBIR_ADV_CORREO      = 18; // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
     CLAVE_GRUPO_USUARIO_EMAIL     = 19; // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.

     // US #14275: Servicio de env�o de correos - Modificaci�n a pantallas de globales.
     CLAVE_DIRECTORIO_SERVICIO_REPORTES  = 20;
     CLAVE_DIRECTORIO_IMAGENES           = 21;
     CLAVE_URL_IMAGENES                  = 22;
     CLAVE_EXPIRACION_IMAGENES           = 23;
     CLAVE_RECUPERAR_PWD_SERVICIO_CORREOS = 24;

     K_POS_CONFIDENCIAL = 4;    // C�digo de Nivel de Confidencialidad en OleVariant Empresa
     Q_CURSO_PROG = '( ( CUR_PROG.EN_LISTA = ''N'' ) or ( COLABORA.%s ' +
                    'in ( select ET_CODIGO from ENTNIVEL where ' +
                    '( ENTNIVEL.PU_CODIGO = CUR_PROG.CB_PUESTO ) and ' +
                    '( ENTNIVEL.CU_CODIGO = CUR_PROG.CU_CODIGO ) ) ) )';
     Q_CERTIFIC_PROG = '( ( CER_PROG.PC_LISTA = ''N'' ) or ( COLABORA.%s ' +
                    'in ( select CN_CODIGO from CERNIVEL where ' +
                    '( CERNIVEL.PU_CODIGO = CER_PROG.CB_PUESTO ) and ' +
                    '( CERNIVEL.CI_CODIGO = CER_PROG.CI_CODIGO ) ) ) )';
     Q_V_CURSO_PROG = '( ( CP.EN_LISTA = ''N'' ) or ( C.%s ' +
                      'in ( select ET_CODIGO from ENTNIVEL where ' +
                      '( ENTNIVEL.PU_CODIGO = CP.CB_PUESTO ) and ' +
                      '( ENTNIVEL.CU_CODIGO = CP.CU_CODIGO ) ) ) )';

implementation

uses
    ZetaCommonTools, StrUtils;

const
     K_KEY = 'mAzER0sKy';
     K_KEY_DB = 'jOeMoRgAn';

{$ifndef DOTNET}
{$ifndef DOS_CAPAS}
function GetIsWindows2000: Boolean;
begin
     Result := ( SysUtils.Win32Platform = VER_PLATFORM_WIN32_NT ) and ( SysUtils.Win32MajorVersion >= 5 );
end;

function GetEnablePooling: Boolean;
begin
     {
     GA: 17/Ene/2003 ) Por lo pronto no se usar� el ObjectPooling
     de los objetos COM de Tress dado que no hay beneficios
     palpables y existe el riesgo de problemas en la configuraci�n
     de los paquetes (no se puede automatizar la habilitaci�n del
     ObjectPooling y si se deja a que sea una operaci�n manual
     puede dar problemas)
     }
     Result := False; { GetIsWindows2000 };
end;

function GetThreadingModel: TThreadingModel;
begin
     {
     GA: 17/Ene/2003 ) Es mejor usar tmBoth que tmFree ya
     que la primera permite operar ( Free and Apartment thread )
     de acuerdo a la plataforma en la cual trabajan los paquetes
     y/o los clientes de COM+ mientras la segunda ?�nicamente
     soporta la operaci�n de el modelo FreeThreaded
     }
     if GetIsWindows2000 then
        Result := tmBoth {tmFree}
     else
         Result := tmApartment;
end;
{$endif}
{$endif}

function GetSupportsCOMPlus: Boolean;
begin
     {$ifdef DOTNET}
     Result := False;
     {$else}
     {$ifdef DOS_CAPAS}
     Result := False;
     {$else}
     Result := GetIsWindows2000;
     {$endif}
     {$endif}
end;

function Round2Integer( const rValor: Extended ): Integer;
begin
     Result := Trunc( ZetaCommonTools.MiRound( rValor, 0 ) );
end;

function Decrypt( const sSource: String ): String;
var
   sKey: String;
   iKeyLen, iKeyPos, iOffset, iSrcPos, iSrcAsc, iTmpSrcAsc: Integer;
begin
     Result := '';
     if StrLleno( sSource ) then
     begin
          {$ifndef DOTNET}
          try
          {$endif}
             sKey := K_KEY;
             iKeyLen := Length( sKey );
             iKeyPos := 0;
             iOffset := StrToInt( '$' + Copy( sSource, 1, 2 ) );
             iSrcPos := 3;
             repeat
                   iSrcAsc := StrToInt( '$' + Copy( sSource, iSrcPos, 2 ) );
                   if ( iKeyPos < iKeyLen ) then
                      iKeyPos := iKeyPos + 1
                   else
                       iKeyPos := 1;
                   iTmpSrcAsc := iSrcAsc xor Ord( sKey[ iKeyPos ] );
                   if ( iTmpSrcAsc <= iOffset ) then
                      iTmpSrcAsc := 255 + iTmpSrcAsc - iOffset
                   else
                       iTmpSrcAsc := iTmpSrcAsc - iOffset;
                   Result := Result + Chr( iTmpSrcAsc );
                   iOffset := iSrcAsc;
                   iSrcPos := iSrcPos + 2;
             until ( iSrcPos >= Length( sSource ) );
          {$ifndef DOTNET}
          except
          end;
          {$endif}
     end;
end;

function Encrypt( const sSource: String ): String;
var
   sKey: String;
   iKeyLen, iKeyPos, iOffset, iSrcPos, iSrcAsc, iRange: Integer;
begin
     if StrLleno( sSource ) then
     begin
          sKey := K_KEY;
          iKeyLen := Length( sKey );
          iKeyPos := 0;
          iRange := 256;
          Randomize;
          iOffset := Random( iRange );
          Result := Format( '%1.2x', [ iOffset ] );
          for iSrcPos := 1 to Length( sSource ) do
          begin
               iSrcAsc := ( Ord( sSource[ iSrcPos ] ) + iOffset ) mod 255;
               if ( iKeyPos < iKeyLen ) then
                  iKeyPos := iKeyPos + 1
               else
                   iKeyPos := 1;
               iSrcAsc := iSrcAsc xor Ord( sKey[ iKeyPos ] );
               Result := Result + Format( '%1.2x',[ iSrcAsc ] );
               iOffset := iSrcAsc;
          end;
     end
     else
         Result := '';
end;



function DecryptDB( const sSource: String ): String;
var
   sKey: String;
   iKeyLen, iKeyPos, iOffset, iSrcPos, iSrcAsc, iTmpSrcAsc: Integer;
begin
     Result := '';
     if StrLleno( sSource ) then
     begin
          {$ifndef DOTNET}
          try
          {$endif}
             sKey := K_KEY_DB;
             iKeyLen := Length( sKey );
             iKeyPos := 0;
             iOffset := StrToInt( '$' + Copy( sSource, 1, 2 ) );
             iSrcPos := 3;
             repeat
                   iSrcAsc := StrToInt( '$' + Copy( sSource, iSrcPos, 2 ) );
                   if ( iKeyPos < iKeyLen ) then
                      iKeyPos := iKeyPos + 1
                   else
                       iKeyPos := 1;
                   iTmpSrcAsc := iSrcAsc xor Ord( sKey[ iKeyPos ] );
                   if ( iTmpSrcAsc <= iOffset ) then
                      iTmpSrcAsc := 255 + iTmpSrcAsc - iOffset
                   else
                       iTmpSrcAsc := iTmpSrcAsc - iOffset;
                   Result := Result + Chr( iTmpSrcAsc );
                   iOffset := iSrcAsc;
                   iSrcPos := iSrcPos + 2;
             until ( iSrcPos >= Length( sSource ) );
          {$ifndef DOTNET}
          except
          end;
          {$endif}
     end;
end;

function EncryptDB( const sSource: String ): String;
var
   sKey: String;
   iKeyLen, iKeyPos, iOffset, iSrcPos, iSrcAsc, iRange: Integer;
begin
     if StrLleno( sSource ) then
     begin
          sKey := K_KEY_DB;
          iKeyLen := Length( sKey );
          iKeyPos := 0;
          iRange := 256;
          Randomize;
          iOffset := Random( iRange );
          Result := Format( '%1.2x', [ iOffset ] );
          for iSrcPos := 1 to Length( sSource ) do
          begin
               iSrcAsc := ( Ord( sSource[ iSrcPos ] ) + iOffset ) mod 255;
               if ( iKeyPos < iKeyLen ) then
                  iKeyPos := iKeyPos + 1
               else
                   iKeyPos := 1;
               iSrcAsc := iSrcAsc xor Ord( sKey[ iKeyPos ] );
               Result := Result + Format( '%1.2x',[ iSrcAsc ] );
               iOffset := iSrcAsc;
          end;
     end
     else
         Result := '';
end;
// Traspasada de DEvaluador

function ConvierteALista( const sLista: String; const lEntero: Boolean; Lista: TStrings ): String;
var
   i: Integer;
begin
     Result := '';
     with Lista do
     begin
          CommaText := sLista;
          for i := 0 to ( Count - 1 ) do
          begin
               if ( i > 0 ) then
                  Result := Result + ', ';
              if lEntero then
                 Result := Result + Trim( Strings[ i ] )
              else
                  Result := Result + EntreComillas( Strings[ i ] );
          end;
     end;
end;

function CampoInLista( const sCampo, sLista : String ) : String;
var
   oLista : TStringList;
begin
     oLista := TStringList.Create;
     Result := Parentesis( sCampo + ' IN ' + Parentesis( ConvierteALista( sLista, False, oLista ) ) );
     oLista.Free;
end;

function CTOD( const sFecha: String ): TDate;
var
   iYear: Word;
begin
     if ( Length( sFecha ) = 8 ) then
        iYear := StrAsInteger( Copy( sFecha, 7, 2 ) )
     else
         iYear := StrAsInteger( Copy( sFecha, 7, 4 ) );
     Result := CodificaFecha( iYear,
                              StrAsInteger( Copy( sFecha, 4, 2 ) ),
                              StrAsInteger( Copy( sFecha, 1, 2 ) ) );
end;

function GetImportedDate( const sData: String ): TDate;
begin
     if ( Pos( '/', sData ) > 0 ) then  { Formato DD/MM/YY }
        Result := StrAsFecha( sData )
     else                               { Formato DDMMYYYY }
        Result := CodificaFecha( StrAsInteger( Copy( sData, 5, 4 ) ),
                                 StrAsInteger( Copy( sData, 3, 2 ) ),
                                 StrAsInteger( Copy( sData, 1, 2 ) ) );
end;

function dMax( const dValor1, dValor2: TDate ): TDate;
begin
     if ( dValor1 > dValor2 ) then
        Result := dValor1
     else
         Result := dValor2;
end;

function dMin( const dValor1, dValor2: TDate ): TDate;
begin
     if ( dValor1 < dValor2 ) then
        Result := dValor1
     else
         Result := dValor2;
end;

function CampoAsVar( oCampo: TField ): Variant;
begin
     with oCampo do
     begin
          if IsNull then
          begin
               if not VarIsNull( OldValue ) then
               begin
                    if ( DataType = ftDateTime ) then
                       Result := VarAsType(OldValue, varDate)
                    else
                        Result := OldValue;
               end
               else
               begin
                    if DataType in [ ftString, ftBlob ] then
                       Result := ''
                    else
                       Result := 0;
               end;
          end
          else
               Result := NewValue;
     end;
end;

function CambiaCampo( oCampo: TField ): Boolean;
begin
     with oCampo do
     begin
          if IsNull then
             Result := False
          else
              if ( DataType = ftDateTime ) then
                 Result := ( VarAsType( OldValue, varDate ) <> NewValue )
              else
                  Result := ( OldValue <> NewValue );
     end;
end;

function CampoOldAsVar( oCampo: TField ): Variant;
begin
     with oCampo do
     begin
          if not VarIsNull( OldValue ) then
          begin
               if ( DataType = ftDateTime ) then
                  Result := VarAsType(OldValue, varDate)
               else
                   Result := OldValue;
          end
          else
          begin
               if DataType in [ ftString, ftBlob ] then
                  Result := ''
               else
                  Result := 0;
          end;
     end;
end;

function GetCampoNivelProg( const iNivel : integer ) : string;
begin
     case iNivel of
          0: Result := '';
          1: Result := 'CB_CLASIFI';
          2: Result := 'CB_TURNO'
          else Result := 'CB_NIVEL' + IntToStr(iNivel - 2)
     end;
end;

function DiasEnMes( const wYear, wMonth: Word ): Word; // Cuantos Dias Tiene un Mes //
begin
     Result := Trunc( CodificaFecha( wYear, wMonth, MaxDay( wYear, wMonth ) ) - CodificaFecha( wYear, wMonth, 1 ) ) + 1;
end;

function GetFiltroNivel0( const Empresa : OleVariant; const sTabla : string ) : String;
 var
    sTablaPrefix: string;
begin
    if ( Trim( Empresa[ K_POS_CONFIDENCIAL ] ) <> '' ) then
    begin
         if StrLleno( sTabla ) then
            sTablaPrefix := sTabla + '.'
         else
             sTablaPrefix := VACIO;

{$ifdef CONFIDENCIALIDAD_MULTIPLE}
     Result := Format( '( %sCB_NIVEL0 in %s )', [ sTablaPrefix, ListaComas2InQueryList( Empresa[ K_POS_CONFIDENCIAL ] ) ] )
{$else}
     Result := Format( '( %sCB_NIVEL0 = ''%s'' )', [ sTablaPrefix, Empresa[ K_POS_CONFIDENCIAL ]] )
{$endif}
    end
    else
        Result := '';
end;

function Nivel0( const Empresa : OleVariant; const sTabla : string ) : String;
begin
    Result := GetFiltroNivel0( Empresa, sTabla );
    if ( Result <> '' ) then
        Result := 'AND ' + Result;
end;

function GetDiasRango( const dInicio, dFinal, dRangoIni, dRangoFin: TDate ): Integer;
begin
     Result := iMax( Trunc( rMin( dFinal - 1, dRangoFin ) -
               rMax( dInicio, dRangoIni ) ) + 1, 0 );
end;

procedure GetServerDatabase( const sValue: String; var sServer, sDatabase: String );
var
  iPos: Integer;
begin
     // Estrategia: localizar el �ltimo punto en la cadena y considerarlo como el separador,
     // Esto es dudoso ya que el Servidor, Instancias y Cat�logo puede contener puntos en sus nombres O.o

     // Nomenclatura del nombre de servidor:
     //  <Servidor | nnn.nnn.nnn.nnn>[\Instancia].<Cat�logo>

     iPos := RPos( '.', sValue );
     if ( iPos > 0 ) then
     begin
          sServer := Copy( sValue, 1, ( iPos - 1 ) );
          sDatabase := Copy( sValue, ( iPos + 1 ), ( Length( sValue ) - iPos ) );
     end
     else
     begin
          sServer := sValue;
          sDatabase := ''
     end;
end;

procedure GetFiltroEmpleado( const sRangoLista, sCondicion, sFiltro: string; const lFiltro: boolean; var sParametros: string; var sFiltroEmp: string; var sFormula: string );

   function ChecaLongitud( const sTemp: string ): boolean;
   begin
        if ( Length( sParametros + sTemp ) <= ZetaCommonClasses.K_MAX_VARCHAR ) then
           Result := TRUE
        else
            Result := FALSE;
   end;

   function SetParametros( const sTemp: string ): String;
   begin
        Result := VACIO;
        if ChecaLongitud( sTemp ) then
           sParametros := sParametros + sTemp
        else
            Result := ZetaCommonTools.StrTransAll( copy( sTemp, 2, length( sTemp ) ), K_PIPE, CR_LF );
   end;

begin
     //Formula del Proceso
     if strLleno( sFormula ) then
        sFormula := SetParametros( sFormula );
     if lFiltro then
     begin
          //Rango que trajo el Proceso
          if strLleno( sRangoLista ) then
             sFiltroEmp := K_PIPE + 'Rango: ' + sRangoLista
          else
             sFiltroEmp := K_PIPE + 'Rango: Todos';
          sFiltroEmp := SetParametros( sFiltroEmp );
          //Condici�n del Proceso
          if strLleno( sCondicion ) then
          begin
               if strLleno( sFiltroEmp ) then
                  sFiltroEmp := sFiltroEmp + CR_LF + 'Condici�n: ' + sCondicion
               else
                   sFiltroEmp := Setparametros( K_PIPE + 'Condici�n: ' + sCondicion );
          end;
          //Filtro del Proceso
          if strLleno( sFiltro ) then
          begin
               if strLleno( sFiltroEmp ) then
                  sFiltroEmp := sFiltroEmp + CR_LF + 'Filtro: ' + sFiltro
               else
                   sFiltroEmp := Setparametros( K_PIPE + 'Filtro: ' + sFiltro );
          end;
     end;
end;

function GetTipoCompany( const iTipo: Integer): String;
var
   eCiclo: eTipoCompany;
begin
     Result := VACIO;
     if ( eTipoCompany( iTipo ) <> tc3Datos ) then
        Result := Format( 'CM_CONTROL = %s',
                  [ EntreComillas( ObtieneElemento( lfTipoCompany, iTipo ) ) ] )
     else
        for eCiclo := Low( eTipoCompany ) to High( eTipoCompany ) do
            //(AV) Las BD de Presupuesto no seran administradas en Sistema TRESS
            if ( eCiclo <> tc3Datos ) {and ( eCiclo <> tcPresupuesto )} then
               Result := ConcatFiltros( Result, Format( 'CM_CONTROL <> %s',
                         [ EntreComillas( ObtieneElemento( lfTipoCompany, Ord( eCiclo ) ) ) ] ) );
end;

function GetTipoCompanyBD( const iTipo: Integer): String;
var
   eCiclo: eTipoCompany;
begin
     Result := VACIO;
     if ( eTipoCompany( iTipo ) <> tc3Datos ) then
        Result := Format( 'DB_CONTROL = %s',
                  [ EntreComillas( ObtieneElemento( lfTipoCompany, iTipo ) ) ] )
     else
        for eCiclo := Low( eTipoCompany ) to High( eTipoCompany ) do
            //(AV) Las BD de Presupuesto no seran administradas en Sistema TRESS
            if ( eCiclo <> tc3Datos ) {and ( eCiclo <> tcPresupuesto )} then
               Result := ConcatFiltros( Result, Format( 'DB_CONTROL <> %s',
                         [ EntreComillas( ObtieneElemento( lfTipoCompany, Ord( eCiclo ) ) ) ] ) );
end;

function GetTipoCompanyMasPresupuestos : String;
var
   eCiclo: eTipoCompany;
begin
     Result := VACIO;

     for eCiclo := Low( eTipoCompany ) to High( eTipoCompany ) do
         //(AV) Las BD de Presupuesto no seran administradas en Sistema TRESS
         if ( eCiclo <> tc3Datos ) and ( eCiclo <> tcPresupuesto ) then
            Result := ConcatFiltros( Result, Format( 'CM_CONTROL <> %s',
                      [ EntreComillas( ObtieneElemento( lfTipoCompany, Ord( eCiclo ) ) ) ] ) );
end;


function GetTipoCompanyDerechos( const iTipo: Integer): String;
var
   eCiclo: eTipoCompany;
begin
     Result := VACIO;
     if ( eTipoCompany( iTipo ) <> tc3Datos ) then
        Result := Format( 'CM_CONTROL = %s',
                  [ EntreComillas( ObtieneElemento( lfTipoCompany, iTipo ) ) ] )
     else
        for eCiclo := Low( eTipoCompany ) to High( eTipoCompany ) do
            //(AV) Las BD de Presupuesto no seran administradas en Sistema TRESS
            if ( eCiclo <> tc3Datos ) and ( eCiclo <> tcPresupuesto ) then
               Result := ConcatFiltros( Result, Format( 'CM_CONTROL <> %s',
                         [ EntreComillas( ObtieneElemento( lfTipoCompany, Ord( eCiclo ) ) ) ] ) );
end;

function GetTipoCompanyDerechosGrupoUsuarios( const iTipo: Integer): String;
var
   eCiclo: eTipoCompany;
begin
     Result := VACIO;
     if ( eTipoCompany( iTipo ) <> tc3Datos ) then
        Result := Format( 'CM_CONTROL = %s',
                  [ EntreComillas(EntreComillas( ObtieneElemento( lfTipoCompany, iTipo ) ) ) ] )
     else
        for eCiclo := Low( eTipoCompany ) to High( eTipoCompany ) do
            //(AV) Las BD de Presupuesto no seran administradas en Sistema TRESS
            if ( eCiclo <> tc3Datos ) and ( eCiclo <> tcPresupuesto ) then
               Result := ConcatFiltros( Result, Format( 'CM_CONTROL <> %s',
                         [ EntreComillas( EntreComillas( ObtieneElemento( lfTipoCompany, Ord( eCiclo ) ) ) ) ] ) );
     Result := 'N' + EntreComillas( Result );
end;

function GetTipoCompanyEnum( const sValor: String ): eTipoCompany;
var
   iValor: Integer;
   FTipos: TStringList;
   eCiclo: eTipoCompany;
begin
     Result := tc3Datos;                        // El default siempre es 3Datos

     FTipos:= TStringList.Create;

     for eCiclo := Low( eTipoCompany ) to High( eTipoCompany ) do
     begin
          FTipos.Add( Format( '%s=%s', [ ZetaCommonLists.ObtieneElemento( lfTipoCompany, Ord( eCiclo ) ), ZetaCommonLists.ObtieneElemento( lfTipoCompanyName, Ord( eCiclo ) ) ] ) );
     end;

     if strLleno( sValor ) then
     begin
          iValor := FTipos.IndexOfName( AnsiUpperCase( sValor ) );
          if ( iValor >= 0 ) then
             Result := eTipoCompany( iValor );
     end;

     FreeAndNil ( FTipos );
end;



function FormateaHora( const sHora: String ): String;
begin
     if ZetaCommonTools.StrLleno( sHora ) then
        Result := Copy( sHora, 1, 2 ) + ':' + Copy( sHora, 3, 2 );
end;

function SetMayusculasSQL( const sTexto: String ): String;
begin
{$ifdef INTERBASE}
     Result := UpperCase( sTexto );
{$endif}
{$ifdef MSSQL}
     Result := {$ifdef DOTNET}UpperCase{$else}AnsiUpperCase{$endif}( sTexto );
{$endif}
end;

procedure RevisaHorasTope24( var dFecha: TDate; var sHora: String );
begin
     if ( aMinutos( sHora ) >= K_24HORAS ) then
     begin
          dFecha := dFecha + 1;
          sHora := aHora24( sHora );
     end;
end;

function RegistrarBitacora( const eClase : eClaseBitacora; const sBitacora : string ): Boolean;
 var
    iWhatBit : TBit;
    iPos, iPosOrd, iTope : integer;
    sValue : string;
begin

     Result := StrVacio(sBitacora);

     if NOT Result then
     begin
          iTope := High(TBit)+1;
          iPosOrd := Ord( eClase );

          iPos := ( Trunc((iPosOrd-1)/iTope) + 1 ) * 2;
          iWhatBit := (iPosOrd -1) Mod iTope;

          sValue := '$FF';

          if (iPos-1)< Length(sBitacora) then
          begin
               sValue := '$' + sBitacora[iPos-1];

               if iPos <= Length(sBitacora) then
                  sValue := sValue + sBitacora[iPos];
          end;

          Result := BitSet( StrToInt( sValue ), iWhatBit );
     end;
end;

function GetExceptionInfo( Error: Exception ): string;
const
     K_CODIGO_YA_EXISTE = '� C�digo � Registro Ya Existe !';
begin
     if (  Pos( 'VIOLATION OF PRIMARY', UpperCase( Error.Message )) > 0 ) then
        Result := K_CODIGO_YA_EXISTE
     else
         Result := Error.Message;
end;

function DosComillas( const sTexto: string ): string;
begin
     Result := COMILLA + sTexto + COMILLA;
end;

function AsignaTablaACampo( const sOriginal, sCampo: string ): string;
 const
      K_DUMMY = 'ZSUELDODUMMY';
      K_DUMMY_NOM = 'ZSUELDONOMINADUMMY';
      K_DUMMY_COMILLAS = 'ZSUELDOCOMILLASDUMMY';
      K_DUMMY_COMILLAS_NOM = 'ZSUELDONOMINACOMILLASDUMMY';
      K_DUMMY_2COMILLAS = 'ZSUELDO2COMILLASDUMMY';
      K_DUMMY_2COMILLAS_NOM = 'ZSUELDONOMINA2COMILLASDUMMY';

 var
    sCampoNoCambia, sNominaCampoNoCambia : string;
begin
     Result := sOriginal;

     {Aquellas tablas que tambien contienen el campo CB_SALARIO o CB_SAL_INT no
     se cambian, se mantienen igual si estas tablas son diferentes de NOMINA}
     sCampoNoCambia := '.' + sCampo;
     sNominaCampoNoCambia := 'NOMINA' + sCampoNoCambia;

     Result := StrTransAll( Result, EntreComillas( sCampo ), K_DUMMY_COMILLAS );  //FechaKardex('CB_SALARIO','01/01/01')
     Result := StrTransAll( Result, EntreComillas( sNominaCampoNoCambia ), K_DUMMY_COMILLAS_NOM ); //'NOMINA.CB_SALARIO'

     Result := StrTransAll( Result, DosComillas( sCampo ), K_DUMMY_2COMILLAS );  //FechaKardex("CB_SALARIO",'01/01/01')
     Result := StrTransAll( Result, DosComillas( sNominaCampoNoCambia ), K_DUMMY_2COMILLAS_NOM ); //"NOMINA.CB_SALARIO"

     Result := StrTransAll( Result, sNominaCampoNoCambia, K_DUMMY_NOM );
     Result := StrTransAll( Result, sCampoNoCambia, K_DUMMY );
     Result := StrTransAll( Result, sCampo, 'NOMINA.' + sCampo );

     Result := StrTransAll( Result, K_DUMMY_NOM, 'NOMINA' + sCampoNoCambia);
     Result := StrTransAll( Result, K_DUMMY, sCampoNoCambia );

     Result := StrTransAll( Result, K_DUMMY_2COMILLAS_NOM, DosComillas( sNominaCampoNoCambia ) ); //"NOMINA.CB_SALARIO"
     Result := StrTransAll( Result, K_DUMMY_2COMILLAS, DosComillas( sCampo ) );  //FechaKardex("CB_SALARIO",'01/01/01')

     Result := StrTransAll( Result, K_DUMMY_COMILLAS_NOM, EntreComillas( sNominaCampoNoCambia ) ); //'NOMINA.CB_SALARIO'
     Result := StrTransAll( Result, K_DUMMY_COMILLAS, EntreComillas( sCampo ) );  //FechaKardex('CB_SALARIO','01/01/01')

end;

function CambiaCamposRetroactivos( const sOriginal: string ): string;
begin
     Result := sOriginal;
     Result := AsignaTablaACampo( Result, 'CB_SALARIO' );
     Result := AsignaTablaACampo( Result, 'CB_SAL_INT' );

end;

function FormateaFlotanteEvaluador( const rValor: TTasa ): String;
const
     K_FACTOR_FLOAT = '1.0';
begin
     Result := Format( '( %f * %s )', [ rValor, K_FACTOR_FLOAT ] );
end;

function GetMsgStatusEmpleado( const iEmpleado: Integer; const dFecha: TDate;
         const Status: eStatusEmpleado ): String;
const
     K_MESS_ACTIVO = 'El Empleado %d Est� Activo El %s';
     K_MESS_ANTERIOR = 'Ingreso Del Empleado %d Posterior Al %s';
     K_MESS_BAJA = 'Empleado %d Dado De Baja Antes Del %s';
begin
     case Status of
          steEmpleado : Result := Format( K_MESS_ACTIVO, [ iEmpleado, FechaCorta( dFecha ) ] );
          steAnterior : Result := Format( K_MESS_ANTERIOR, [ iEmpleado, FechaCorta( dFecha ) ] );
          steBaja     : Result := Format( K_MESS_BAJA, [ iEmpleado, FechaCorta( dFecha ) ] );
     else
          Result := VACIO;     // No se ha requerido regresar el Mensaje para otros status
     end;
end;


function DiasSemanaJornada( const TipoJornada: eTipoJornada ): Word;
begin
     case TipoJornada of
          tjReducida_1: Result := 1;
          tjReducida_2: Result := 2;
          tjReducida_3: Result := 3;
          tjReducida_4: Result := 4;
          tjReducida_5: Result := 5;
     else
         Result := 7;
     end;
end;

function GetAusentismosJornadaReducida(const TipoJornada: eTipoJornada; const iFaltas: integer ): integer;
begin
     Result := ZetaCommonTools.iMax(0,  iFaltas );

     if TipoJornada in [tjReducida_1,tjReducida_2,tjReducida_3,tjReducida_4,tjReducida_5 ] then
     begin
          Result := Round2Integer( K_DIAS_SEMANA * ( Result/DiasSemanaJornada( TipoJornada ) ) );
     end;
end;

function StrDecToStrHex( const sSource: String ): String;
begin
     Result := IntToHex( StrToInt64Def( sSource, 0 ), 1 );
end;

function StrHexToStrDec( const sSource: String ): String;
begin
     Result := IntToStr( StrToInt64Def( '$' + sSource, 0 ) );
end;

function ProximidadDecToHex( const sDecValue, sFacilityCode: String; const iMaxSize: Integer ): String;
var
   iMinSize : Integer;
   iValue, iMaxValue: int64;
begin
     if ZetaCommonTools.StrLleno( sFacilityCode ) then
     begin
          iMinSize := ZetaCommonTools.iMax( 0, ( iMaxSize - Length( sFacilityCode ) ) );
          iMaxValue := StrToInt64Def( '$' + StringOfChar( 'F', iMinSize ), 0 );
          iValue := StrToInt64Def( Trim( sDecValue ), 0 );
          if ( iValue > iMaxValue ) then
          begin
               Result := StrDecToStrHex( sDecValue );
          end
          else
          begin
               Result := sFacilityCode + IntToHex( iValue, iMinSize );
          end;
     end
     else
         Result := StrDecToStrHex( sDecValue );
end;

function ProximidadHexToDec( const sHexValue, sFacilityCode: String; const iMaxSize: Integer ): String;
var
   iLen, iFacilityLen: Integer;
begin
     Result := Trim( {$ifdef DOTNET}UpperCase{$else}AnsiUpperCase{$endif}( sHexValue ) );
     if ZetaCommonTools.StrLleno( Result ) then
     begin
          iLen := Length( Result );
          if ( iLen = iMaxSize ) then
          begin
               iFacilityLen := Length( sFacilityCode );
               if ( iFacilityLen > 0 ) and ( Copy( Result, 1, iFacilityLen ) = sFacilityCode ) then
               begin
                    {
                    El String Hexadecimal Si Contiene El Facility Code
                    por lo que hay que regresar el String Decimal Que
                    Corresponda al String Hexadecimal Sin El Facility Code
                    }
                    Result := StrHexToStrDec( Copy( Result, ( iFacilityLen + 1 ), ( iLen - iFacilityLen ) ) );
               end
               else
               begin
                    {
                    El String Hexadecimal No Contiene El Facility Code
                    por lo que hay que regresar el String Decimal Que
                    Corresponda
                    }
                    Result := StrHexToStrDec( Result );
               end;
          end
          else
          begin
               {
               El String Hexadecimal Tiene M�s o Menos D�gitos
               Que el iMaxSize, por lo que hay que regresar
               el String Decimal Que Corresponda, sin quitar el
               Facility Code
               }
               Result := StrHexToStrDec( Result );
          end;
     end;
end;

function ColFechaResult( const iColumna: Integer ): String;
const
     K_FECHA_RESULT = 'CTOD(%s) + INT( RESULT(%d) )';
begin
     { Para Agregar en el Agente el Result de Fechas }
     Result := Format( K_FECHA_RESULT, [ EntreComillas( VACIO ), iColumna ] );
end;

function AnexaParamProceso( const sTexto, sParam, sTitulo: String ): String;
begin
     Result := sTexto;
     if StrLleno( sParam ) then
        Result := ConcatString( Result, sTitulo + sParam, K_PIPE );
end;

function EvaluaResultadoConflicto( const StatusExp: eStatusConflictoExp; const iEmpleado: Integer; const dInicial, dFinal: TDate;
                                   var sConflicto: String; const lAjuste: Boolean = FALSE ) : Boolean;
const
     aMensajesConflicto: array[ eStatusConflictoExp ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} =
                         ( 'Empleado %0:d fue dado de alta ( %1:s ) despu�s de la fecha de inicio', //stcAnterior
                           VACIO,  //stcEmpleado
                           'Empleado %0:d tiene vacaciones desde %1:s hasta %2:s', //stcVacaciones
                           'Empleado %0:d est� incapacitado desde %1:s hasta %2:s', //stcIncapacidad,
                           'Empleado %0:d tiene un permiso con goce desde %1:s hasta %2:s', //stcConGoce,
                           'Empleado %0:d tiene un permiso sin goce desde %1:s hasta %2:s', //stcSinGoce,
                           'Empleado %0:d tiene una falta justificada desde %1:s hasta %2:s', //stcJustificada,
                           'Empleado %0:d est� suspendido desde %1:s hasta %2:s', //stcSuspension,
                           'Empleado %0:d tiene permiso desde %1:s hasta %2:s', //stcOtroPermiso,
                           'Empleado %0:d fue dado de baja ( %1:s ) antes de la fecha de regreso', //stcBaja
                           'Empleado %0:d tiene un plan de vacaciones desde %1:s hasta %2:s' ); //stcPlanVaca
begin
     Result := ( StatusExp <> stcEmpleado );

     if ( StatusExp = stcBaja ) then
        sConflicto := Format( aMensajesConflicto[ StatusExp ], [ iEmpleado, FormatDateTime ('DD/MMM/YYYY', dFinal ) ] )
     else
        sConflicto := Format( aMensajesConflicto[ StatusExp ], [ iEmpleado, FormatDateTime ('DD/MMM/YYYY', dInicial ),
                                                                                           FormatDateTime ('DD/MMM/YYYY', dFinal-1 ) ] );

     if lAjuste and ( not ( StatusExp in [ stcAnterior, stcBaja ] ) ) then
        sConflicto := sConflicto + '@1' + FechaAsStr( dInicial ) + '@2' + FechaAsStr( dFinal );
     if StatusExp in [stcIncapacidad] then
        sConflicto := sConflicto + '@3Incapacidad';
end;

function CampoTexto( const Valor: Variant; const Tipo: TFieldType): String;
begin
  Result := '';
  case Tipo of
    ftSmallint, ftInteger, ftWord, ftAutoInc:
      if (Valor = Null) then
        Result := '0'
      else
        Result := IntToStr(Valor);
    ftString:
      if (Valor = Null) then
        Result := VACIO
      else
        Result := Valor;
    ftFloat, ftCurrency, ftBCD, ftFMTBcd:
      if (Valor = Null) then
        Result := '0.00'
      else
        Result := FormatFloat('#,0.00', Valor);
    ftDate, ftTime, ftDateTime:
      if (Valor = Null) then
        Result := '  /  /    '
      else
        Result := FormatDateTime('dd/mm/yyyy', Valor);
  end;
end;

function NoEsCampoLLavePortal( const sField: string ):Boolean;
begin
     {$ifdef INTERBASE}
     Result := TRUE;
     {$else}
     Result := ( sField <> K_CAMPO_LLAVE_PORTAL );
     {$endif}
end;

function NoEsCampoLLavePortal( Field: TField ):Boolean;
begin
     {$ifdef INTERBASE}
     Result := TRUE;
     {$else}
     Result := FALSE;
     if ( Field <> NIL ) and StrLleno( Field.FieldName ) then
        Result := NoEsCampoLLavePortal( Field.FieldName );
     {$endif}
end;


function GetCompanyChecksumFromData(  const sCompany, sControlReal, sControl, sDatos : String; const dFecha : TDate ) : string;
const
     // CM_CODIGO|CM_CTRL_RL|CM_CONTROL|CM_DATOS|FECHA
     K_TRAMA_CHECKSUM = '%s|%s|%s|%s|%d' ;
var
   sTrama : string;
begin
     sTrama := Format( K_TRAMA_CHECKSUM, [ sCompany, sControlReal, sControl, sDatos, Trunc( Double( dFecha )  ) ] );
     Result := EncryptDB( sTrama );
end;

procedure GetCompanyDataFromChecksum( const sCheckSum:  String; var sCompany, sControlReal, sControl, sDatos : String; var dFecha : TDate );
var
   sTrama : string;
   slItems : TStringList;
   dblFecha : Double;

   procedure ParseDelimited(const sl : TStrings; const value : string; const delimiter : string) ;
   var
      dx : integer;
      ns : string;
      txt : string;
      delta : integer;
   begin
      delta := Length(delimiter) ;
      txt := value + delimiter;
      sl.BeginUpdate;
      sl.Clear;
      try
        while Length(txt) > 0 do
        begin
          dx := Pos(delimiter, txt) ;
          ns := Copy(txt,0,dx-1) ;
          sl.Add(ns) ;
          txt := Copy(txt,dx+delta,MaxInt) ;
        end;
      finally
        sl.EndUpdate;
      end;
   end;

begin
    sTrama  := DecryptDB( sCheckSum );
    slItems := TStringList.Create;
    ParseDelimited( slItems, sTrama, '|');

    sCompany := VACIO;
    sControlReal := VACIO;
    sControl := VACIO;
    sDatos := VACIO;
    dFecha := 0;

    if ( slItems.Count >= 5 ) then
    begin
          sCompany := slItems[0];
          sControlReal := slItems[1];
          sControl := slItems[2];
          sDatos := slItems[3];
          dblFecha := StrToFloatDef( slItems[4], 0);
          dFecha := TDateTime( dblFecha );
    end;

    FreeAndNil ( slItems );
end;

function GetTipoDia(const StatusEmpleado: eStatusEmpleado; const lFestivo, lFestivoEnDescanso{$ifdef IMSS_VACA}, lDiasCotizaSinFV{$endif}: Boolean; const StatusDia: eStatusAusencia ): eTipoDiaAusencia;
begin	 { Nota: Es importante que se respete el orden en que se prueban las condiciones }
     if StatusEmpleado in [ steAnterior, steBaja ] then
     begin
          Result := daNoTrabajado
     end
     else
         if ( StatusEmpleado = steIncapacidad ) then
            Result := daIncapacidad
         else
             if ( StatusDia = auDescanso ) then
{$ifdef IMSS_VACA}
             begin
                  if lDiasCotizaSinFV and ( StatusEmpleado = steVacaciones ) then
                     Result := daVacaciones
                  else if lFestivo and lFestivoEnDescanso then
                     Result := daFestivo
                  else
                      Result := daNormal;
             end
{$else}
                Result := daNormal
{$endif}
             else
                 if lFestivo then
                    Result := daFestivo
                 else
                     if ( StatusDia = auSabado ) then
{$ifdef IMSS_VACA}
                     begin
                          if lDiasCotizaSinFV and ( StatusEmpleado = steVacaciones ) then
                             Result := daVacaciones
                          else
                              Result := daNormal;
                     end
{$else}
                        Result := daNormal
{$endif}
                     else
                     begin
                          case StatusEmpleado of { � Est� en Orden del Ennumerado ? }
                               steEmpleado: Result := daNormal;
                               steVacaciones: Result := daVacaciones;
                               steConGoce: Result := daConGoce;
                               steSinGoce: Result := daSinGoce;
                               steJustificada: Result := daJustificada;
                               steSuspension: Result := daSuspension;
                               steOtroPermiso: Result := daOtroPermiso;
                          else
                              Result := daNormal; { Valor para evitar resultado indefinido }
                          end;
                     end;
end;

function NoEsHabil( const eTipoDia: eStatusAusencia; const eExtrasSabado, eExtrasDescanso: eHorasExtras ): Boolean;
begin
     Result := ( ( eTipoDia = auSabado ) and ( eExtrasSabado in [ heExtrasTopeSemanal..heDescansoTrabajado ] ) ) or
               ( ( eTipoDia = auDescanso ) and ( eExtrasDescanso in [ heExtrasTopeSemanal..heDescansoTrabajado ] ) );
end;

end.
