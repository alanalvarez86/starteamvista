unit ZetaCommonClasses;

interface

uses Classes, SysUtils, DB,      
     {$ifndef DOTNET}Controls, Windows, DBClient,{$endif}
     Variants,
     ZetaCommonLists;

type
  EInvalidFieldType = class( Exception );
  TTiposValidos = set of eTipoGlobal;
  TTipoLookupEmpleado = ( eLookEmpGeneral, eLookEmpNominas, eLookEmpCursos, eLookEmpCedulas, eLookEmpComidas, eLookEmpAsignaArea, eLookEmpMedicos, eLookEmpBancaElec, eLookEmpInfonavit ,eLookEmpPlaza, eLookEmpTransfer);
  TTipoRegTarjeta = ( eRegistroTarjeta, eAgregarTarjeta, eModificarTarjeta );
  // ER,CV eoVacacion se utiliza para proyectos especiales
  eOpEvento = ( eoNinguna, eoSalario, eoPuesto, eoClasifi, eoTurno, eoContrato,
                eoTablaPrestaciones, eoKardexGeneral, eoReingreso, eoBaja, eoAsignaCampo,
                eoNivel1, eoNivel2, eoNivel3, eoNivel4, eoNivel5, eoNivel6, eoNivel7,
                eoNivel8, eoNivel9, eoVacacion, eoTipoNomina {$ifdef ACS},eoVacio1,eoVacio2,
                eoVacio3,eoVacio4,eoVacio5,eoVacio6,eoVacio7,eoVacio8,eoVacio9,eoNivel10,eoNivel11,eoNivel12{$endif});
                {ACS: Se pidi� que se aumentara la lista hasta 30 (22 - 30) y a partir de all� se agreguen los 3 niveles adicionales}

{$ifdef TRESS_DELPHIXE5_UP}
  TBitacoraTexto = String;
{$else}
  TBitacoraTexto = String[ 50 ];
{$endif}
  TBit = 0..7;
  {$ifdef DOTNET}
  TDate = TDateTime;
  {$endif}
  TZetaParams = class( TParams )
  private
    {Private declarations}
    function GetVarValues: OleVariant;
    procedure SetVarValues( Values: OleVariant );
  protected
    {Protected declarations}
  public
    {Public declarations}
    property VarValues: OleVariant read GetVarValues write SetVarValues;
    function IndexOf( const sName: String): Integer;
    function FindCreateParam( FldType: TFieldType; const sName: String ): TParam;
    procedure AsBlob( const sName: String; const bValue: TBlobData );
    procedure AsBoolean( const sName: String; const lValue: Boolean );
    procedure AsDate( const sName: String; const dValue: TDate );
    procedure AsFloat( const sName: String; const rValue: Extended );
    procedure AsInteger( const sName: String; const iValue: Integer );
    procedure AsMemo( const sName, sValue: String );
    procedure AsString( const sName, sValue: String );
    procedure AsVariant( const sName: String; oVariant: Variant );
    procedure AddBlob( const sName: String; const bValue: TBlobData );
    procedure AddBoolean(const sName: String; const lValue: Boolean);
    procedure AddDate( const sName: String; const dValue: TDate );
    procedure AddDateTime( const sName: String; const dValue: TDateTime );
    procedure AddFloat( const sName: String; const rValue: Extended );
    procedure AddInteger( const sName: String; const iValue: Integer );
    procedure AddMemo( const sName, sValue: String );
    procedure AddString( const sName, sValue: String );
    procedure AddVariant( const sName: String; oValue: Variant );
  end;

  eTipoDiccionario = ( eClasifi,
                       eEntidades,
                       eListasFijas,
                       eListasFijasValor,
                       eAccesosClasifi,
                       eAccesosEntidades,
                       eModulos,
                       eModulosPorEntidad,
                       eCamposPorEntidad,
                       eClasifiPorEntidad,
                       eRelacionesPorEntidad,
                       eCamposDefault,
                       eFiltrosDefault,
                       eOrdenDefault );

  {$ifndef DOTNET}
  {$endif}

{$define QUINCENALES}
{$define CAMBIO_TNOM}
{.$undefine QUINCENALES}
const
     VERSION_ARCHIVO = '4.2.0.0';
     VERSION_PRODUCTO = '2018';
     {***DevEx(@am):Actualizar esta varibale cada vez que se saque un build y se requiera
                    utilizar la pantalla de Bienvenida para mostrar los cambios. Conformar
                    la cadena de la siguiente forma: <A�o de la version>.<Numero del build>
                    (Ejemplo: 2014.0,2014.1,...,2015.0)***}
     VERSION_COMPILACION ='2018.0';
     { Contantes para Reportes }
     K_MAX_PARAM = 10;
     K_NoHayRegistros = 'No Hay Registros que Cumplan con los Filtros Especificados';
     K_IMAGEN_MAESTRO = '@IMAGEN_MAESTRO';
     K_IMAGEN_EMPLEADO_X = 'IMAGEN_EMPLEADO_';
     K_IMAGEN_MAESTRO_X = 'IMAGEN_MAESTRO_';
     K_MAESTRO_MA_CODIGO = 'MAESTRO_MA_CODIGO';
     Q_EMPLEADO_FOTO = 'EMPLEADO_IMAGEN';
     K_VISITA_VI_NUMERO = 'VISITA_VI_NUMERO';
     K_VISITA_VI_FOTO = 'VISITA_VI_FOTO';
     {Esta cosntante se utiliza en reportes especiles (Listado de Nomina)
     Se esta suponiendo que ningun empleado va a llegar a tener este numero.
     Si algun empleado alguna vez llega a tener este valor el reporte no va a funcionar correctamente}
     Q_IMAX_EMPLEADO = 999999999;

     // El C�digo de grupo que no tiene restricciones
     D_GRUPO_SIN_RESTRICCION = 1;

     CR_LF = Chr(13) + Chr( 10 );
     {***(@am): Se agregan constantes para los caracteres CR y LF por separado en VS2015, debido que se cambia
     la logica de la funcion BorraCReturn para que busque cada caracter por separado en lugar de buscar la combinacion*** }
     CR = Chr(13);
     LF = Chr( 10 );
     VK3_F = Ord( 'F' );
     K_GRUPO_SISTEMA = 1;
     K_TIPO_DIA = 'D';
     K_TIPO_HORA= 'H';
     K_TOKEN_NIL    = 'NIL';
     //POSICIONES DEL ARREGLO dmCliente.Empresa
     P_ALIAS = 0;
     P_DATABASE = 0;
     P_USER_NAME = 1;
     P_PASSWORD = 2;
     P_USUARIO = 3;
     P_NIVEL_0 = 4;
     P_CODIGO = 5;
     P_GRUPO = 6;
     P_APPID = 7;
     { Array de Datos de Seguridad }
     SEG_VENCIMIENTO             = 0;
     SEG_LIMITE_PASSWORD         = 1;
     SEG_INTENTOS                = 2;
     SEG_DIAS_INACTIVOS          = 3;
     SEG_TIEMPO_INACTIVO         = 4;
     SEG_MIN_LETRAS_PASSWORD     = 5;
     SEG_MIN_DIGITOS_PASSWORD    = 6;
     SEG_MAX_LOG_PASSWORD        = 7;
     SEG_USUARIO_TRESSAUTOMATIZA = 8; // (JB) Global de Usuario de TressAutomatiza

     SEG_MOSTRAR_ADV_LICENCIA    = 0; // US #9666 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
     SEG_SERVIDOR_CORREOS        = 1;// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
     SEG_PUERTO_SMTP             = 2;// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
     SEG_AUTENTIFICACION_CORREO  = 3;// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
     SEG_USER_ID                 = 4;// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
     SEG_EMAIL_PSWD              = 5;// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
     SEG_RECIBIR_ADV_CORREO      = 6;// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.     SEG_GRUPO_USUARIO_EMAIL     = 16;// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
     SEG_GRUPO_USUARIO_EMAIL     = 7;// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
     K_LIMITE_NOM_NORMAL = 200;
     K_MAX_CONCEPTO = 999;
     //
     K_T_ALTA    = 'ALTA';
     K_T_BAJA    = 'BAJA';
     K_T_CAMBIO  = 'CAMBIO';
     K_T_RENOVA  = 'RENOVA';
     K_T_PUESTO  = 'PUESTO';
     K_T_AREA    = 'AREA';
     K_T_TURNO   = 'TURNO';
     K_T_PRESTA  = 'PRESTA';
     K_T_VACA    = 'VACA';
     K_T_PERM    = 'PERM';
     K_T_INCA    = 'INCA';
     K_T_TAS_INF = 'TASINF';
     K_T_EVENTO  = 'EVENTO';
     K_T_CIERRA  = 'CIERRE';
     K_T_PLAZA   = 'PLAZA';
     K_T_TIPNOM = 'TIPNOM';

     K_GLOBAL_SI = 'S';
     K_GLOBAL_NO = 'N';
     K_USAR_GLOBAL = 'G'; // Usa Global en Puestos PU_CHECA y PU_AUTOSAL

     K_FACTOR_MENSUAL = 30.4;

     K_FOTO = 'FOTO';
     K_PIPE = '|';
     K_MAX_VARCHAR = 255;

     K_AJUSTE_SALDO_REINGRESO = ' AJUSTE DE SALDO AL REINGRESO';

     K_MENSAJE_GLOBAL_FONACOT_NO_ACTIVO = 'No est� activo el mecanismo para administrar pr�stamos por c�dula mensual.';
     K_MENSAJE_GLOBAL_FONACOT_NO_TIPO_PRESTAMO = 'No ha definido el Tipo de pr�stamo Fonacot en Globales de N�mina.';

     // Constantes de Mascaras
     K_MASCARA_FLOAT = '#,0.00;-#,0.00';

     //
     TURNO_VACIO       = '!!!';
     K_ANCHO_HORARIO = 6;              // Proviene de DQueries
     K_OFFSET_VECTOR_RITMO = 32;       // Constante de Turno Ritmico que evita Caracteres de Control
     K_DIAS_SEMANA = 7;
     K_DIAS_SEMANA_HABILES = 5;
     K_DIAS_VACACION_HABIL = 1.2;
     K_DIAS_VACACION_DEF = 1;
     K_MESS_TOOL_PRESTADA = 'Prestada';
     K_PREFIJO_INVITADOR = 'IV';
     K_TIME_FORMAT = 'hhnn';

     //
{ Constantes de Array de Saldos de Vacaciones }
     K_VACA_GOZO_ACTUAL = 0;
     K_VACA_PAGO_ACTUAL = 1;
     K_VACA_TABLA_PREST = 2;
     K_VACA_SAL_DIARIO  = 3;
     K_VACA_PRIMA_VACA  = 4;
     K_VACA_SEPTIMO_DIA = 5;
     K_VACA_SEPTIMO_PRIMA = 6;
     K_VACA_PAGO_PROP   = 7;
     K_VACA_DPRIMA_ACTUAL = 8;
     K_VACA_DPRIMA_PROP = 9;
     K_VACA_PAGO_PEN = 10;
     K_VACA_PRIMA_PEN = 11;

{ Constantes de Array de Saldo Vacaciones de Aniv. Anterior }
     K_VACA_ANIV_ANTERIOR  = 0;
     K_VACA_ANIV_SIGUIENTE = 1;
     K_VACA_DERECHO_GOZO   = 2;
     K_VACA_DERECHO_PAGO   = 3;
     K_VACA_GOZADOS        = 4;
     K_VACA_PAGADOS        = 5;
     K_VACA_D_PRIMA        = 6;
     K_VACA_P_PRIMA        = 7;

{ Constantes de Array de Fecha Limite de Bloqueo }
     K_FECHA_LIMITE_DESCRIPCION = 0;
     K_FECHA_LIMITE_FORMULA     = 1;
     K_FECHA_LIMITE_USUARIO     = 2;
     K_FECHA_LIMITE_CAPTURA     = 3;

     NullDateTime: TDateTime = 0;

     K_DIAS_ANNO_PROMEDIO = 365.25;
     K_DIAS_MES_PROMEDIO = 30.4375;

     K_24HORAS = 1440;
     K_CERO_HORAS = '0000';
     K_MASK_HORAS = '00:00;0';

     K_CARGO_REINGRESO = 'R';

     K_PIXELES_PULGADA = 96;

     K_POS_CLAVES_VERSION = 100;       // Posici�n en Tabla CLAVES de Comparte donde se almacena la Versi�n de Datos

{A�o en que cambio la Reforma Fiscal.
Afecta a Proceso de Percepciones Variables,
funcion IMSS_VARIABLE}
     K_YEAR_REFORMA = 2002;
     {
     A�o y Mes de las reformas de la ley del IMSS
     donde se publican los ajustes sobre la cantidad
     de ausentismos a reportar al IMSS para empleados
     con jornadas reducidas
     }     
     K_REFORMA_AJUSTAR_REDUCIDAS_YEAR = 2002;
     K_REFORMA_AJUSTAR_REDUCIDAS_MES = 11;

     {CV:Reforma Fiscal 2008}
     K_REFORMA_FISCAL_2008 = 2008;

{Zonas Geogr�ficas del IMSS }
     K_T_ZONA_A  = 'A';
     K_T_ZONA_B  = 'B';
     K_T_ZONA_C  = 'C';

{Valores Uma}
     K_T_DIARIO  = 1;
     K_T_MENSUAL  = 2;
     K_T_ANUAL  = 3;
     K_T_FDESC  = 4;

{$ifdef INTERBASE}
     {$ifdef TRESS}
     K_PRETTYNAME = 'CB_APE_PAT||'' ''||CB_APE_MAT||'', ''||CB_NOMBRES';
     {$endif}
     {$ifdef TRESSCFG}
     K_PRETTYNAME = 'CB_APE_PAT||'' ''||CB_APE_MAT||'', ''||CB_NOMBRES';
     {$endif}

     {$ifdef SELECCION}
     K_PRETTYNAME = 'SO_APE_PAT||'' ''||SO_APE_MAT||'', ''||SO_NOMBRES';
     {$endif}
     {$ifdef VISITANTES}
     K_PRETTYNAME = '';
     {$endif}
     {$ifdef COMPARTE_MGR}
     K_PRETTYNAME = '';
     {$endif}
     {$ifdef ADUANAS}
     K_PRETTYNAME = '';
     {$endif}
     PRETTY_EXP = 'EX_APE_PAT||'' ''||EX_APE_MAT||'', ''||EX_NOMBRES'; //PrettyName para Servicio Medico
     K_CONCATENA = '||';
{$endif}
{$ifdef MSSQL}
     K_PRETTYNAME = 'PRETTYNAME';
     PRETTY_EXP = 'EX_APE_PAT+'' ''+EX_APE_MAT+'', ''+EX_NOMBRES'; //PrettyName para Servicio Medico
     K_CONCATENA = '+';
{$endif}
     //En interbase este campo no existe.
     K_CAMPO_LLAVE_PORTAL = 'LLAVE';
     K_CAMPO_SINC_CREATED = 'SINC_CREATED';
     K_CAMPO_SINC_UPDATED = 'SINC_UPDATED';

{Constante para Turnos Ritmicos: H-S-D }
     TOKEN_RITMO_HSD = '#';

{Constantes usadas en RELOJ.DAT}
     TOKEN_ASISTENCIA = '@';
     TOKEN_CAFETERIA = '#';
     TOKEN_ACCESOS = '&';
     TOKEN_PROXIMIDAD = '~';
{$ifndef INTERBASE}
     TOKEN_BIOMETRICO = '$';
{$endif}
     TOKEN_ZK = '*';

     K_MAX_LEN_PROXIMIDAD = 10;

     // Parametro para el tipo del dispositivo Biom�trico Revisi�n 2
     K_TIPO_BIOMETRICO = 5;

{Constantes para Derechos de Acceso}
     K_SIN_DERECHOS        =     0;
     K_DERECHO_CONSULTA    =     1;
     K_DERECHO_ALTA        =     2;
     K_DERECHO_BAJA        =     4;
     K_DERECHO_CAMBIO      =     8;
     K_DERECHO_IMPRESION   =    16;
     K_DERECHO_SIST_KARDEX =    32;
     K_DERECHO_BANCA       =    64;
     K_DERECHO_NIVEL0      =   128;
     K_DERECHO_CONFIGURA   =   256;
     K_DERECHO_ADICIONAL9  =   512;
     K_DERECHO_ADICIONAL10 =  1024;
     K_DERECHO_ADICIONAL11 =  2048;
     K_DERECHO_ADICIONAL12 =  4096;
     K_DERECHO_ADICIONAL13 =  8192;
     K_DERECHO_ADICIONAL14 = 16384;
     K_DERECHO_ADICIONAL15 = 32768;

{Constantes para Status Limite de Bloqueo}
     K_DERECHO_MODIFICAR_FECHA_LIMITE        = K_DERECHO_SIST_KARDEX;
     K_DERECHO_MODIFICAR_TARJETAS_ANTERIORES = K_DERECHO_BANCA;

{Constantes para Clasificaciones Temporales}
     K_DERECHO_NIVEL_PUESTO        = K_DERECHO_CONSULTA;
     K_DERECHO_NIVEL_CLASIFICACION = K_DERECHO_ALTA;
     K_DERECHO_NIVEL_TURNO         = K_DERECHO_BAJA;
     K_DERECHO_NIVEL_1             = K_DERECHO_CAMBIO;
     K_DERECHO_NIVEL_2             = K_DERECHO_IMPRESION;
     K_DERECHO_NIVEL_3             = K_DERECHO_SIST_KARDEX;
     K_DERECHO_NIVEL_4             = K_DERECHO_BANCA;
     K_DERECHO_NIVEL_5             = K_DERECHO_NIVEL0;
     K_DERECHO_NIVEL_6             = K_DERECHO_CONFIGURA;
     K_DERECHO_NIVEL_7             = K_DERECHO_ADICIONAL9;
     K_DERECHO_NIVEL_8             = K_DERECHO_ADICIONAL10;
     K_DERECHO_NIVEL_9             = K_DERECHO_ADICIONAL11;
 {$ifdef ACS}
     K_DERECHO_NIVEL_10            = K_DERECHO_ADICIONAL12;
     K_DERECHO_NIVEL_11            = K_DERECHO_ADICIONAL13;
     K_DERECHO_NIVEL_12            = K_DERECHO_ADICIONAL14;
 {$endif}

{Constantes para el Arreglo de Autorizaciones eAutorizaChecadas}
     K_OFFSET_AUTORIZACION = 5;

{Constantes para Prespuesto de Personal}
     K_MAX_CONTEO_NIVELES = 5;

{Constantes para Tress/Seleccion EMail}
     K_PDF = 'PDF';

{Constantes para construccion de QUERYS}
     Y          = ' AND ';
     VACIO      = '';
     COMILLA    = '"';
     UnaCOMILLA = CHAR(39){'};
     PUNTO      = '.';
     ARROBA_TABLA = '@TABLA';

{CONSTANTES BOOLEANAS}

     K_BOOLEANO_SI = 'SI';
     K_BOOLEANO_NO = 'NO';
     K_PROPPERCASE_SI = 'S�';
     K_PROPPERCASE_NO = 'No';

{Constantes para Globales}

     K_GLOBAL_DEF_COMP_ANUAL_INGRESO_BRUTO      = 2020;{ Default: Comp Anual Ingreso Bruto }
     K_GLOBAL_DEF_COMP_ANUAL_INGRESO_EXENTO     = 2021;{ Default: Comp Anual Ingreso Exento }
     K_GLOBAL_DEF_COMP_ANUAL_IMPUESTO_RETENIDO  = 2022;{ Default: Comp Anual Impuesto Retenido }
     K_GLOBAL_DEF_COMP_ANUAL_CREDITO_PAGADO     = 2023;{ Default: Comp Anual Credito Pagado }
     K_GLOBAL_DEF_COMP_ANUAL_CALCULO_IMPUESTO   = 2024;{ Default: Comp Anual Calculo Impuesto }
     K_GLOBAL_DEF_COMP_ANUAL_REPORTE            = 2025;{ Default: Reporte Comp Anual }
     K_GLOBAL_DEF_CRED_SAL_ING_BRUTO            = 2027;{ Default: Decl Crd Sal Ing Bruto }
     K_GLOBAL_DEF_CRED_SAL_ING_EXENTO           = 2028;{ Default: Decl Crd Sal Ing Exento }
     K_GLOBAL_DEF_CRED_SAL_PAGADO               = 2029;{ Default: Decl Crd Sal Crd Pagado }
//
//     {CUANDO SE AGREGUE UNA CONSTANTE NUEVA
//     SE TIENE QUE AGREGAR EN EL METODO DGLOBAL.GETTIPOGLOBAL
//     CUANDO EL GLOBAL NO SEA TEXTO}
//
//     {Resultados de una forma de Edicion}
     K_EDICION_MODIFICACION = 2;
     K_EDICION_CANCELAR     = 4;

{ Constantes de status de Procesos - Wizards }

     B_PROCESO_ABIERTO = 'A';
     B_PROCESO_CANCELADO = 'C';
     B_PROCESO_OK = 'N';
     B_PROCESO_ERROR = 'S';

{ Constantes de elementos del arreglo de TZetaLog.CloseProcess }

     K_PROCESO_FOLIO = 0;
     K_PROCESO_STATUS = 1;
     K_PROCESO_ID = 2;
     K_PROCESO_MAXIMO = 3;
     K_PROCESO_PROCESADOS = 4;
     K_PROCESO_ULTIMO_EMPLEADO = 5;
     K_PROCESO_INICIO = 6;
     K_PROCESO_FIN = 7;
     K_PROCESO_ERRORES = 8;
     K_PROCESO_ADVERTENCIAS = 9;
     K_PROCESO_EVENTOS = 10;

{ Constantes de Longitud de Dominios de Base de Datos }
     K_ANCHO_NUMEROEMPLEADO = 9;
     K_ANCHO_DESCRIPCION = 30;
     K_ANCHO_DESCLARGA = 40;
     K_ANCHO_TITULO = 100;
     K_ANCHO_FECHA = 10;
     K_ANCHO_CODIGO = 6;

     {$ifdef ANCHO_NIVELES_8}
     K_ANCHO_CODIGO_NIVELES= 8;
     {$else}
            {$ifdef ANCHO_NIVELES_10}
             K_ANCHO_CODIGO_NIVELES = 10;
            {$else}
                   {$ifdef ANCHO_NIVELES_12}
                    K_ANCHO_CODIGO_NIVELES= 12;
                   {$else}
                    K_ANCHO_CODIGO_NIVELES = K_ANCHO_CODIGO;
                    {$endif}
            {$endif}
     {$endif}
     K_ANCHO_CODIGO1 = 1;
     K_ANCHO_CODIGO2 = 2;
     K_ANCHO_CODIGO3 = 3;
     K_ANCHO_REGPATRONAL = 1;
     K_ANCHO_BOOLEANO = 1;
     K_ANCHO_PESOS = 15;
     K_ANCHO_PESOSDIARIO = 15;
     K_ANCHO_TASA = 15;
     K_ANCHO_ZONAGEO = 1;
     K_ANCHO_STATUS = 5;
     K_ANCHO_DESCINFO = 15;
     K_ANCHO_DIASFRAC = 15;
     K_ANCHO_REFERENCIA = 8;
     K_ANCHO_PASSWD = 30;
     K_ANCHO_PRETTYNAME = 50;
     K_ANCHO_OBSERVACIONES = 50;
     K_ANCHO_MASCARA = 20;
     K_ANCHO_CODIGOPARTE = 15;
     K_ANCHO_DESCCORTA = 20;
     K_ANCHO_FORMULA = 255;
     K_ANCHO_HORA = 4;
     K_ANCHO_OPERACION = 10;
     K_ANCHO_TIEMPO = 15;
     K_ANCHO_USUARIO = 5;
     K_ANCHO_RELOJLINX = 4;
     K_ANCHO_NOMBRECAMPO = 10;
     K_ANCHO_COMPUTERNAME = 63;

     { Constantes de Valores de Tasa Anterior }
     aArregloTasa: array[0..3] of String = ( 'Ninguna', '20', '25', '30' );

{ Constantes de movimientos exportados a SUA }
     K_TIPO_SUA_BAJA = '02';
     K_TIPO_SUA_CAMBIO = '07';
     K_TIPO_SUA_REINGRESO = '08';
     K_TIPO_SUA_VOLUNTARIA = '09';
     K_TIPO_SUA_AUSENTISMO = '11';
     K_TIPO_SUA_INCAPACIDAD = '12';
     K_TIPO_SUA_INI_CREDITO = '15';
     K_TIPO_SUA_SUS_CREDITO = '16';
     K_TIPO_SUA_REINICIO_CRE = '17';
     K_TIPO_SUA_CAMBIO_TD = '18';
     K_TIPO_SUA_CAMBIO_VD = '19';
     K_TIPO_SUA_CAMBIO_NUM = '20';

{ Constantes de expedientes para evaluar conflicto }
   E_INCAPACI = 1;
   E_VACACION = 2;
   E_PERMISO  = 3;
   E_PLAN_VAC = 4;

{ Constantes Para AVENT-HONDURAS }
   K_DIAS_ANNO_AVENT = 360;
   K_DIAS_ANNO       = 365;

{ Constantes Para Vlaidacion de incapacidades }

  K_MOTIVO_AUTORIZ_OFFSET = -1;

{ Constantes de Proyectos Especiales }

  K_COVIDIEN_INTERFAZ_HR = 1000;
  {$ifdef INTERRUPTORES}
  K_WIDTHLLAVE = 230;
  K_WIDTH_LOOKUP = 500;
  K_LEFT_630 = 630;
  K_WIDTH_CEDULAS = 690;
  K_WIDTH_BASECEDULAS = 660;
  K_ANCHO_PARTE20 = 20;
  {$endif}
  {$ifdef ANCHO_NIVELES_10}
  K_WIDTHLLAVE = 120;
  K_WIDTH_LOOKUP = 360;
  K_WIDTH_TEXTBOX =117;
  K_WIDTH_SMALLFORM = 500;
  K_WIDTH_SMALL_LEFT = 100;
  K_WIDTH_MEDIUM_LEFT = 175;
  K_WIDTH_MEDIUM_TEXTBOX = 240;
  K_WIDTH_MEDIUM_TEXTBOX_LEFT = 298;
  K_WIDTH_FORMCALENDARIO = 219;
  {$endif}

{ Constantes de Mis Datos }

  K_MIS_DATOS = 'MDATOS';
  K_TEMPLATE_HTM = 'DEFAULT.HTM';

  // US #14275: Servicio de env�o de correos - Modificaci�n a pantallas de globales.
  { Constantes para servidor de reportes y servidor de correos }
  K_DIRECTORIO_SERVICIO_REPORTES     = 0;
  K_DIRECTORIO_IMAGENES              = 1;
  K_URL_IMAGENES                     = 2;
  K_EXPIRACION_IMAGENES              = 3;
  K_RECUPERAR_PWD_SERVICIO_CORREOS   = 4;

{ Constantes de Help Context }


      H95001_Confirmacion = 95001;
      H00001_Pantalla_principal = 1;
      H00002_Pestanas_valor_activo = 2;
      H00003_Explorador = 3;
      H00015_busqueda_catalogos = 00015;
      H00022_Acerca_de_Tress = 22;
      H10141_Emp_nom_ahorros = 10141;
      H10151_Alta_empleados = 10151;
      H10162_Promediar_percepciones = 10162;
      H00005_Bitacora_procesos = 5;
      H20211_Tarjeta_diaria = 20211;
      H33131_Edicion_montos = 33131;
      H30311_Totales_nomina = 30311;
      H30313_Montos_nomina = 30313;
      H50511_Generador_reportes = 50511;
      H66211_Evaluador_expresiones  = 66211;
      H66212_Escoge_funcion         = 66212;
      H66213_Escoge_parametro       = 66213;
      H66214_Escoge_concepto        = 66214;
      H66215_Escoge_acumulado       = 66215;
      H66216_Escoge_global          = 66216;
      H60621_Catalogo_conceptos = 60621;
      H60622_Parametros_nomina = 60622;
      H60623_Periodos_nomina = 60623;
      H60644_Matriz_puesto = 60644;
      H80811_Empresas = 80811;
      H40431_Pago_IMSS_nuevo = 40431;
      H50512_Generador_reportes_Generales = 50512;
      H50513_Generador_reportes_Lista_campos = 50513;
      H50514_Generador_reportes_Orden = 50514;
      H50515_Generador_reportes_Grupos = 50515;
      H50516_Generador_reportes_Filtros = 50516;
      H50519_Generador_Cuentas_Contables = 50519;
      H60651_Globales_empresa = 60651;
      H00006_Menu_empresas = 6;
      H80813_Usuarios = 80813;
      H00007_Reportes_archivados = 7;
      H00008_Archivo_imprimir = 8;
      H00009_Imprimir_forma = 9;
      H00010_Impresora = 10;
      H00011_Menu_Editar = 11;
      H00012_Busqueda_empleados = 12;
      H00013_Menu_ventana = 13;
      H00016_Preferencias = 16;
      H00017_Error_funcion              = 17;
      H00018_Lista_reportes             = 18;
      H10111_Datos_identificacion_empleado = 10111;
      H10112_Datos_contratacion_empleado = 10112;
      H10113_Datos_area_empleado = 10113;
      H10114_Datos_percepciones_empleado = 10114;
      H10115_Otros_datos_empleado = 10115;
      H24_Otros_datos_empleado_edicion = 24;
      H10116_Datos_adicionales_empleado = 10116;
      H10121_Expediente_personales_empleado = 10121;
      H10122_Expediente_Foto_empleado = 10122;
      H00000_Expediente_Documento_Empleado = 1000004; // (JB) Anexar al expediente del trabajador documentos CR1880 T1107
      H10123_Expediente_Parientes_empleado = 10123;
      H10124_Expediente_Experiencia_empleado = 10124;
      H10125_Expediente_Cursos_anteriores = 10125;
      H10126_Expediente_Puestos_anteriores = 10126;
      H00014_Calendario = 14;
      H10131_Kardex = 10131;
      H31311_Kardex_Alta = 31311;
      H31312_Kardex_Baja = 31312;
      H31313_Kardex_General = 31313;
      H31314_Kardex_Cambio = 31314;
      H10132_Historial_vacaciones = 10132;
      H10153_Saldar_Vacaciones = 10153;
      H_Kardex_Plaza = 10020500;
      H_Cancelar_Excepciones_Festivos = 225000;
      H_Importar_Asistencia_Sesiones = 172000;
      {
      H11321_Vacaciones = 11321; se us� H11510_Vacaciones
      }
      H11322_Cierre = 11322;
      H10133_Historial_incapacidades = 10133;
      H10134_Historial_permisos = 10134;
      H10135_Historial_cursos_tomados = 10135;
      H10137_Pagos_IMSS = 10137;
      H11411_Agregar_renglon = 11411;
      H10142_Prestamos = 10142;
      H11421_Agregar_renglon = 11421;
      H10143_Acumulados = 10143;
      H10144_Historial = 10144;
      //H10152_Baja = 10152;
      H10152_Tress_verifica_la_existencia_de_nuevos_empleados = 10152;
      H10153_Reingreso = 10153;
      H10155_Cambio_turno = 10155;
      H10156_Cambio_puesto = 10156;
      H10157_Cambio_area = 10157;
      H10158_Cambio_contrato = 10158;
      H10159_Cambio_prestaciones = 10159;
      H11510_Vacaciones = 11510;
      H11511_Cerrar_vacaciones = 363000;   //act. HC, anterior  11511;
      H11512_Incapacidad = 11512;
      H11513_Permisos = 10134;    //act. HC, anterior  ;
      H11515_Curso_tomado = 10135;  //act. HC, anterior 11515;
      H10161_Recalcular_integrados = 10161;
      H10011_Datos_empleado = 10011;
      H10012_Expediente_empleado = 10012;
      H10013_Historia_empleado = 10013;
      H10014_Nomina_empleado = 10014;
      H10015_Registro_Empleados = 10015;
      H20021_Datos_Asistencia = 20021;
      H20022_Registro_Asistencia = 20022;
      H20212_Prenomina = 20212;
      H22121_edicion_prenomina = 22121;
      {
      H22122_Modificar_renglon = 22122;
      }
      H20213_Calendario = 20213;
      H20221_Autorizar_extras_y_permisos = 20221;
      H20222_Horarios_temporales = 20222;
      H20023_Procesos_asistencia = 20023;
      H20231_POLL_relojes = 20231;
      H20232_Procesar_tarjetas = 20232;
      H20233_Calcular_prenomina = 20233;
      H20234_Extras_permisos_globales = 20234;
      H20235_Registros_automaticos = 20235;
      H20236_Corregir_fechas_globales = 20236;
      H20237_Recalcular_tarjetas = 20237;
      H30031_Datos_nomina = 30031;
      H30312_Dias_horas = 30312;
      H33121_Dias_horas = 30321;   //act. HC, anterior  33121;
      {
      H30314_Prenomina = 30314;
      H33141_Edicion_Prenomina = 33141;
      }
      H33142_Modificar_renglon = 33142;
      H30315_Clasificacion = 30315;
      H33151_Clasificacion = 33151;
      H30032_Excepciones = 30032;
      H30321_Dias_horas = 30321;
      H33211_Dias_horas = 33211;
      H30322_Montos = 30322;
      H30323_Excepciones_globales = 30323;
      H33222_Excepciones_montos_todos = 33222;
      H30033_Registro_nominas = 30033;
      H30331_Nomina_nueva = 30331;
      H30332_Borrar_nomina_activa = 30332;
      H30333_Excepcion_dias_horas = 30333;
      H30334_Excepcion_monto = 30334;
      H30335_Liquidacion = 30335;
             H30336_Pago_de_recibos = 30336;
      H30034_Procesos_nomina = 30034;
      H30341_Calcular_nomina = 30341;
      H30035_Anuales_nomina = 30035;
      H30342_Afectar_nomina = 30342;
      H30343_Foliar_recibos = 30343;

      H30346_Generar_poliza_contable = 30346;
      H30344_Credito_aplicado_mensual = 30344;

      H30349_Desafectar_nomina = 30349;
      H33411_Importar_movimientos = 33411;
      H33412_Exportar_movimientos = 33412;
      H33413_Pagar_por_fuera = 33413;
      H33414_Cancelar_nominas_pasadas = 33414;
      H33415_Liquidacion_global = 33415;
      H33416_Calcular_salario_neto_bruto = 33416;
      H33417_Rastrear_calculo = 33417;
      H33418_Recalcular_acumulados = 33418;
      H33425_Recalcular_ahorros = 33425;
      H33424_Importar_acumulados_RS = 33424;
      H33419_Copiar_nomina = 33419;
      H33420_Calcular_diferencias = 33420;
      H33421_Importar_pago_de_recibos = 33421;
      H33422_Refoliar_recibos = 33422;
      H30351_Definir_periodos = 30351;
      H40441_Calcular_pago = 40441;
      H40411_Totales_IMSS = 40411;
      H40412_IMSS_mensual_empleado = 40412;
      H40413_IMSS_bimestral_empleado = 40413;
      H40042_Historia_IMSS = 40042;
      H40422_Hist_IMSS_bimestral_empleado = 40422;
      H40043_Registro_IMSS = 40043;
      H40432_Borrar_pago_IMSS_activo = 40432;
      H40044_Pocesos_IMSS = 40044;
      H40442_Calcular_recargos = 40442;
      H40443_Revisar_datos_para_SUA = 40443;
      H40444_Exportar_SUA = 40444;
      H40445_Calcular_prima_riesgo = 40445;
      H50051_Explorador_reportes = 50051;
      {***Agregados para proyecto NuevaInterfaz***}
      H5005101_Explorador_reportes_Clasica = 5005101;
      H5005101_Explorador_reportes_DevEx   = 5005102;
      {***}
      H50517_Generador_reportes_Impresora = 50517;
      H50518_Generador_reportes_Parametros = 50518;
      H40421_Hist_IMSS_mensual_empleado = 40421;
      H33221_Excepciones_montos =  30322;  //act. HC, anterior  33221;
      H40041_Datos_IMSS = 40041;
      H55121_Generales_Agrega_campo = 55121;
      H10154_Cambio_salario = 10154;
      H55152_Grupos_Modificar_encabezado = 55152;
      H55154_Encabezado_Agrega_campo = 55154;
      H55156_Pie_Agrega_campo = 55156;

      H55161_Filtros_Agrega_filtro = 55161;
      H55162_Filtros_Agrega_campo = 55162;
      H55163_Filtros_Buscar_condicion = 55163;
      H55171_Impresora_Archivo = 55171;
      H55172_Impresora_Plantilla = 55172;
      H50001_Creacion_reporte = 50001;
      H50053_Grafica_conteo = 50053;
      H50054_SQL = 50054;
      {***Agregados para proyecto NuevaInterfaz***}
      H50054_SQL_Clasica = 5005401;
      H50054_SQL_DevEx = 5005402;
      {***}

      {** Agregados por Timbrado}
      H10139_Bancos = 10139;
      H70076_JornadaSat = 70076;
      H70077_ContratoSat = 70077;
      H70078_RiesgoPuesto = 70078;
	    H70079_TiposConceptosSAT = 70079;

      H65116_Globales_de_Configuracion_Timbrado = 65116;

      H80084_CambioServidor = 80084;
      H32130_ActualizarContribuyente = 32130;
      H32131_CancelarContribuyente = 32131;
      H32132_TimbrarNomina = 32132;
      H32133_TomarRecibos = 32133;
      H32134_CancelarTimbrado = 32134;
      H32135_TomarRecibosXEmpleado = 32135;
      H32136_EnvioRecibos = 32136;
      H32137_AdquirirFoliosFiscales = 32137;
      H32138_PeriodosNomina = 32138;
      H32139_EstadoCuentaTimbrado = 32139;
      H32140_CuentasTimbrado = 32140;
      H32141_Contribuyente = 32141;
      H32142_ConciliadorTimbrado = 32142;
      H32143_AuditoriaConceptoTimbrado = 32143;



      {***}

      H60061_Catalogos_Contratacion = 60061;
      H60611_Puestos = 60611;
      H60612_Clasificaciones = 60612;
      H60613_Percepciones_fijas = 60613;
      H60614_Turnos = 60614;
      H60615_Horarios = 60615;
      H60616_Prestaciones = 60616;
      H66161_Agregar_renglon = 60616;   //act. HC, anterior  66161;
      H66162_Modificar_renglon = 66162;
      H60617_Tipo_contrato = 60617;
      H60062_Catalogos_nomina = 60062;
      H60624_Folios_recibos = 60624;
      H60063_Catalogos_generales = 60063;
      H60631_Registros_patronales = 60631;

      H60632_Festivos_General = 60632;
      H3900000_Edit_Festivos_generales = 60632;   //act. HC, anterior  390000;
      H60633_Edit_Festivos_Turno =  391000;  //act. HC, anterior  60633;
      H391000_Festivos_Turno = 391000;


      H60634_Condiciones = 60634;
      H60635_Eventos = 60635;
      H60064_Catalogos_capacitacion = 60064;
      H60641_Cursos = 60641;
      H60642_Maestros = 60642;
      H60643_Matriz_curso = 60643;
      H60645_Calendario = 60645;
      H60065_Catalogos_configuracion = 60065;
      H65101_Identificacion = 65101;
      H65102_Niveles_organigrama = 65102;
      H65103_Campos_adicionales = 65103;
      H65104_Recursos_humanos = 65104;
      H65105_Nomina = 65105;
      H65106_Asistencia = 65106;
      H65107_IMSS_INFONAVIT = 65107;
      H65108_Capacitacion = 65108;
      H65109_Variables_globales = 65109;
      H65110_Empleados = 65110;
      H60652_Bitacora = 60652;
      {***Agregados para proyecto NuevaInterfaz***}
      H60652_Bitacora_Clasica = 6065201;
      H60652_Bitacora_DevEx = 6065202;
      {***}
      H60653_Diccionario_datos = 60653;
      H60654_Comandos_SQL = 60654;
      H60655_Procesos_Abiertos = 60655;
      H70071_Tablas_Personales = 70071;
      H70711_Estado_civil = 70711;
      H70712_En_donde_vive = 70712;
      H70713_Con_quien_vive = 70713;
      H70714_Grado_estudios = 70714;
      H70715_Transporte = 70715;
      H70716_Estados_pais = 70716;
      H70072_Areas = 70072;
      H70073_Adicionales = 70073;
      H70074_Historial = 70074;
      H70741_Tipo_kardex = 70741;
      H70742_Motivo_baja = 70742;
      H70743_Incidencias = 70743;
      H70744_Tipo_curso = 70744;
      H70745_Motivos_de_Autorizacion = 70745;
      H_Motivos_Checada_Manual = 388000;
      H_Motivos_Edit_Checada_Manual = 388000;   //act. HC, anterior  389000;
      H70075_Tablas_nomina = 70075;
      H70751_Tipo_ahorro = 70751;
      H70752_Tipo_prestamo = 70752;
      H70753_Monedas = 70753;
      H70076_Tablas_oficiales = 70076;
      H70761_Salarios_minimos = 70761;
      H70762_Cuotas_IMSS = 70762;
      H70763_ISPT_numericas = 70763;
      H70764_Grados_riesgo = 70764;
      H70765_Tipos_de_cambio = 70765;
      H77632_Agregar_renglon = 77632;
      H77633_Modificar_renglon = 77633;
      H80081_Datos_Sistema = 80081;
      H80812_Grupos_usuarios = 80812;
      H80814_Poll_pendientes = 80814;
      H80082_Sistema_Registro = 80082;
      H80821_Empresa_nueva = 80821;
      H80822_Borrar_empresa_activa = 80822;
      H80083_Procesos_Sistema = 80083;
      H80831_Cierre_anual = 80831;
      H80832_Borrar_bajas = 80832;
      h80833_Borrar_nominas = 80833;
      H80834_Borrar_tarjetas = 80834;
      H80835_Depurar_informacion = 80835;
      H80837_Borrar_bitacora = 80837;
      H20000_Asistencia = 20000;
      H10000_Empleados = 10000;
      H30000_Nominas = 30000;
      H40000_IMSS = 40000;
      H50000_Consultas = 50000;
      H60000_Catalogos = 60000;
      H70000_Tablas = 70000;
      H80000_Sistema = 80000;
      H_Borrar_Timbrado_nominas = 66161;
      // Nuevos

      H10016_Procesos_empleados  = 10016;
      H10163_Cambio_salario_global = 10163;
      H10164_Vacaciones_globales = 10164;
      H10165_Aplicar_tabulador  = 10165;
      H10166_Aplicar_eventos    = 10166;
      H10167_Importar_kardex    = 10167;
      H10168_Cancelar_kardex    = 10168;
      H10174_Importar_altas     = 10174;
      H10169_Captura_de_cursos = 10169;
      H10170_Curso_tomado_global = 10170;
      H10176_Cancelar_curso_tomado_global = 10176;
      H_Foliar_Capacitaciones               = 423000;
      H487000_Reiniciar_Capacitaciones_STPS = 487000;
      H488000_Revisar_Datos_STPS            = 488000;

      H50052_Estadisticas_empleados = 50052;
      H11121_Tipo_Modificacion_contratacion = 11121;
      H11131_Tipo_modif_area_percep = 11131;

      H10136_Cursos_programados     = 10136;
      H33410_Limpiar_acumulados     = 33410;

      H55180_Parametros             = 55180;
      H11521_baja_permanente        = 11521;
      H33351_confirma_baja          = 33351;
      H22111_Tarjeta_diaria_edicion = 22111;
      H11516_cambios_multiples      = 11516;
      H22131_calendario_edicion     = 22131;

      H40446_Calcular_tasa_infonavit= 40446;
      H11610_Transferencia_de_empleado= 11610;
      H11611_Cancelar_Vacaciones = 11611;
      H_Cancelar_Cierre_Vacaciones = 10020510;
      H11612_Registro_de_comidas = 11612;
      H30352_Calcular_aguinaldos      = 30352;
      H30353_Imprimir_aguinaldos      = 30353;
      H30354_Pagar_aguinaldos         = 30354;
      H30355_Calcular_reparto_de_ahorros = 30355;
      H30356_Imprimir_reparto_de_ahorros = 30356;
      H30357_Pagar_ahorros               = 30357;
      H30358_Calcular_PTU                = 30358;
      H30359_Imprimir_PTU                = 30359;
      H33510_Pagar_PTU                   = 33510;
      H33511_Declaracion_de_credito_al_salario = 33511;
      H33512_Imprimir_declaracion_de_credito_al_salario = 33512;
      H33513_Comparar_ISTP_anual = 33513;
      H33514_Diferencias_de_ISPT_a_nomina = 33514;
      H33515_Imprimir_comparativo_anual = 33515;
      H33516_Cierre_de_ahorros          = 33516;
      H33517_Cierre_de_prestamos        = 33517;
      H10171_Comidas_diarias            = 10171;
      H10172_Comidas_periodo            = 10172;
      H10173_Invitaciones               = 10173;

      H60661_Reglas                     = 60661;
      H60662_invitadores                = 60662;
      H65111_globales_de_cafeteria      = 65111;
      H60112_Globales_de_Supervisores   = 60112;
      H33423_Calcular_retroactivos      = 33423;
      HHHHH_Liquidacion_global_Finiquitos = 383000;   //act. HC, anterior  384000;
      HHHHH_Aplicar_Finiquitos_Global     = 0;
      HHHHH_Aprobar_Finiquitos_Global     = 10020550;
      HHHHH_Simulacion_Finiquitos_Global  = 383000;
      HHHHH_Simulacion_Finiquitos_Empleado = 275000;
      H_Calcular_Simulacion_Finiquitos_Global = 10020530;

      H70075_UnidadMedida_Actualizacion = 70075;

//      HHHHH_Simulacion_Finiquitos_Empleado = 289000;
      //MA:Agregados para el build 2.2
      H22211_Existe_autorizacion_extras = 22211;
      H66141_Calendario_del_turno_ritmico = 66141;
      H00051_Kardex_de_areas            = 51;
      H80839_Asignando_areas_de_Labor_a_los_usuarios = 80839;
      H9508_Registro_manual_de_Breaks = 9508;
      H9509_Cancelacion_de_Breaks_Manuales = 9509;
      H_Importar_Ahorros_Prestamos = 10020520;

      //Help Context de Labor
      H95001_Datos_del_empleado	= 10101;
      H9502_Operaciones_del_empleado = 9502;
      H9503_Lecturas_del_empleado = 9503;
      H9504_Ordenes_fijas = 9504;
      H9505_Orden_de_trabajo = 9505;
      H9506_Kardex_de_Orden_de_trabajo = 9506;
      H9507_Historial_de_Ordenes_de_trabajo = 9507;
      H9508_Multilote = 9508;
      H9510_Partes = 9510;
      H9511_Operaciones	= 9511;
      H9515_Moduladores	= 9515;
      H9513_Clasificacion_de_operaciones = 9513;
      H9514_Catalogo_de_Areas = 9514;
      H9512_Clasificacion_de_partes = 9512;
      H9516_Catalogo_Globales = 9516;
      H9518_Catalogo_Breaks = 9518;
      H9519_TiempoMuerto = 9519;
      H30314_Montos_NominaModifica = 30314;
      H80818_Confidencialidad = 80818;
      H60663_Herramientas = 60663;
      H60664_Tallas = 60664;
      H60665_Motivos_Devolucion = 60665;
      H10138_Historial_de_Herramientas = 10138;
      H11613_Entregar_herramienta = 11613;
      H11614_Entrega_global = 11614;
      H11615_Regresar_Herramienta = 11615;
      H11616_Borrar_herramientas = 11616;
      H11617_Entregar_Herramienta = 11617;
      H11618_Regresar_Herramienta = 11618;
      H65112_Globales_de_reportes_email = 65112;
      H65113_Globales_De_Seguridad      = 65113;
      H00001_Labor_Introduccion = 1;
      H00004_Calcular_tiempos = 4;
      H00005_Depuracion = 9531;
      H80836_Borrar_Poll = 80836;
      H00006_Importar_ordenes = 9528;
      H00007_Importar_Productos = 9529;
      H00008_Cedulas_de_captura = 8;
      H50011_Indicando_la_base_de_datos = 50011;
      H50012_Probando_el_acceso = 50012;
      H00053_Clasificacion_de_Defectos = 9524;
      H00052_Cedulas_de_inspeccion = 9526;
      H00054_Afectar_labor = 54;
      H_Enrolamiento_Masivo = 10020600;
      H_Importar_Enrolamiento_Masivo = 10020610;

      H50055_Presupuesto_de_personal = 50055;
      H50551_Detalle_del_presupuesto = 50551;
      H50552_Edicion_del_presupuesto = 50552;
      H50553_Configuracion_del_presupuesto = 50553;
      H5005405_Envios_Programados = 5005405;


      H33620_Copia_Configuracion = 33620;

      //**TRESSPRESUPUESTOS **//
	  // (JB) Escenarios de presupuestos T1060 CR1872
      H00000_Escenarios_de_presupuesto = 151;
      H00001_Escenarios_de_presupuesto = 151;
      H_SIMULACION_PRESUPUESTOS = 153;
      H_CAT_EVENTOS_ALTAS = 150;
      H_SUPUESTOS_RH = 152;

      H00015_Datos_del_sistema = 15;
      H80817_Archivos_temporales = 80817;
      H95002_Lista_de_empleados = 95002;
      H80816_Usuarios_Preferencias = 80816;
      H80081_Especificar_base_de_datos = 80081;
      H80084_Especificar_Servidor = 80084;

      H00055_Cedulas_de_scrap = 9527;
      H00056_Componentes = 9525;
      H00057_Motivos_de_scrap = 9521;


      {Programacion Emerson }

      H_LAB_CAT_MAQUINAS = 1000008;
      H_LAB_CAT_LAYOUT = 98000;
      H_LAB_CAT_EDIT_MAQUINAS = 1000008;
      H_LAB_CAT_EDIT_LAYOUT = 98000;
      H_LAB_CAT_TMAQUINAS =  1000007;
      H_LAB_SUP = 104000;
      H_LAB_KAR_EMP_MAQ = 0;
      H_LAB_KAR_MAQ_EMP = 0;



      {Help Context para Modulo de Seleccion}
      H00072_Entrevista	= 72;

      H00073_Solicitudes = 73;
      H00081_Solicitudes = 81;
      H00076_Solicitud_Examenes =  76;
      H00078_Solicitud_Requisitos = 78;
      H00079_Solicitud_Adicionales = 79;
      H00083_Solicitud_Referencias = 83;

      H00051_Requisicion_General = 51;
      H00052_Requisicion_Perfil	= 52;
      H00053_Requisicion_Candidatos = 53;
      H00054_Requisicion_Otros_datos = 54;
      H00055_Requisicion_Gastos	= 55;
      H00077_Requisicion_Personal = 77;

      H00071_Puestos = 71;
      H00080_Clientes = 80;
      H00070_Catalogos_generales = 70;
      H00079_Tablas_Adicionales=79;
      H00050_Globales_de_empresa = 50;

      H00082_Registro = 82;
      H00084_Servidor_Correos = 84;
      H11514_Registro_de_incapacidades = 11514;
      H11619_Banca_Electronica = 11619;
      H11620_Renumerar_empleados = 11620;
      H65114_Globales_de_Bitacora = 65114;
      H10175_Permisos_globales = 10175;
      H11621_Registro_de_Credito_Infonavit = 11621;
      H80819_Mapa_de_Grupos_de_usuarios = 80819;
      H50016_Desbloquear_administradores = 50016;
      H11622_Registro_de_prestamos = 11622;
      H60646_Clase_de_cursos = 60646;
      H50056_Organigrama_de_la_empresa = 50056;
      H00028_Asignacion_de_licencias_de_empleados = 28;
      H00029_Capturar_clave_de_instalacion = 29;
      Configurando_el_valor_de_Cada_Dia_de_Vacaciones = 11518;
      H00030_Construccion_de_Alias_de_BDE = 30;

      //MA: Agregado para el build 2.4.101
      H80838_Cambiando_su_clave_de_usuario =  80838;

      //MA:Agregado para el Build 2.5.102
      //H60647_Sesiones = 60647;
      H10139_Cursos_programados_individualmente = 10136;  //act. HC, anterior 10139;
      H70754_Prioridad_de_ahorros_y_prestamos= 70754;
      H20223_Autorizaciones_por_aprobar = 20223;
      H20223_Autorizaciones_por_aprobarSuper = 503; //act. HC, anterior 20223, se creo nueva HC;
      H65115_Globales_de_bloqueo_de_Asistencia = 65115;

      H11624_Corregir_fechas_globales_de_Cafeteria = 11624;
      H11623_Registro_grupal_de_comidas = 11623;
      H33518_Declaracion_anual = 33518;
      H_Declaracion_anual_Cierre = 10020555;

      H00010_Importacion_de_Cedulas_de_Produccion = 9530;
      H_Lista_de_Candidatos = 10154;
      H_Document_Dialog = 50520;
      H_Super_Clasificaciones_Temporales = 505;
      H_Asistencia_Clasificaciones_Temporales = 20224;
      H_Cambio_Masivo_Turnos = 506;
      H00504_Vacaciones = 504;
      H11625_Actualizar_numero_de_tarjeta = 11625;

      H_Tabla_Colonias = 140000;
      H_Calendario_del_empleado = 141000;
      //MA:Agregado para el build 2.7.106.0
      H_CATALOGO_AULAS = 151000;
      H_RESERVACION_DE_AULAS = 158000;
      H_RESERVACION_SESION = 161000;
      H_Prerequisitos_curso = 152000;
      H_INSCRITOS = 164000;
      H_ASISTENCIA = 166000;
      H_APROBADOS = 168000;
      H_SESIONES = 155000;
      H_EDIT_EVALUACIONES = 175000;

      // Grid de Autorizar Prenomina
      H_Grid_Autorizar_Prenomina = 501;   //act. HC, anterior  86000;
      H_Cat_Certificaciones = 208000;
      H_Kardex_Certificaciones = 209000;
      H_Cat_TiposPoliza = 249000;
      H_Nom_Datos_Polizas = 248000;
      H_PlanVacacion = 272000;
      H_PlanVacacion_Super = 240000;
      H_CAT_PERFILES_PUESTO = 267000;
      H_CAT_SECC_PERF  = 270000;
      H_CAT_CAMPOS_PERF = 271000;
      H_CAT_PERFILES = 268000;
      H_CAT_PERFIL_NUEVO = 273000;
      H_EDIT_DESC_PERFIL = 269000;
      H_COPIAR_PERFIL = 274000;
      H_CANCELAR_PERMISOS_GLOB = 221000;
      H_EMP_RECALCULA_SALDOS_VACA = 255000;
      H_EMP_EDIT_PLAZA = 256000;
      H_EMP_WIZ_PLAZA =  50056;  //act. HC, anterior  259000;
      H_CertificacionesEmpleado = 320000;
      H_EMP_EDIT_TIPO_NOMINA = 316000;
      H_EmpleadosCertificados = 321000;
      H_FEditReglasPrestamo = 100001;
      H_FReglasPrestamo = 100001;

      H_LAYOUT_PRODUCCION = 10140;  //act. HC, nueva constante

      //Help Context's de caja de Ahorro

      H_SALDOS_EMPLEADO      =  1;
      H_SALDO_CAJA_AHORRO    =  2;
      H_ESTADO_CTA_BANCARIA  =  3;
      H_CAT_TIPO_AHORRO      =  4;
      H_CAT_TIPO_PRESTAMO    =  5;
      H_CAT_CTAS_BANCARIAS   =  6;
      H_CAT_TIPOS_DEP_RET    =  7;
      H_REG_AHORRO           =  8;
      H_REG_CTAS_BANCARIAS   =  6; //Antes 9
      H_REG_MOVIMIENTOS      = 10;
      H_INSCRIPCION          = 11;
      H_REG_PRESTAMO         = 12000; //Antes 12
      H_LIQUID_RET_PARCIAL   = 13;
      H_DEP_RETENCIONES      = 14;
      H_REG_TIPO_AHORRO      = 4; //Antes 15
      H_REG_TIPO_PRESTAMO    = 5; //Antes 16
      H_REG_TIPO_DEP_RET     = 7; //Antes 17

      H_DOCUMENTO_CONCEPTOS  = 344000;
      H_BITACORA_SISTEMA     = 319000;
      H_PROV_CAPACITACION    = 334000;
      H_VALUACION_PLANTILLA  = 338000;
      H_VALUACION_FACTORES   = 339000;
      H_VALUACION_SUBFACTORES = 340000;
      H_VALUACION_NIVELES     = 343000;
      H_VALUACIONES           = 341000;
      H_COPIA_VALUACION       = 341000;   //act. HC, anterior  342000;
      H_TIPOS_D_PERIODO       = 192000;
      H_WIZ_AJUSTE_RET_FONACOT = 347000;
      H_WIZ_CALCULAR_PAGO_FONACOT = 348000;
      H_WIZ_IMPORTAR_CEDULA_FONACOT = 10180;
      H_WIZ_ELIMINAR_CEDULA_FONACOT = 225001;
      H_WIZ_CALCULA_PAGO_FONACOT = 30347;

      H_GRID_AJUSTE_COLECTIVO     = 503;{OP: 26/06/08}
      H_CAT_RAZONES_SOCIALES  = 349000;{OP: 26/06/08}
      H_NOM_FONACOT_TOTALES   = 30311;{OP: 26/06/08}
      H_NOM_FONACOT_EMPLEADO  = 0;{OP: 25/06/08 Esta constante no se usa. Es la misma que H_NOM_FONACOT_TOTALES}
      H_BORRAR_PAGO_FONACOT   = 372000;{OP: 27/06/08}
      H_FONACOT_DATOS_EMPLEADO = 370000;{OP: 26/06/08}
      H_GLOBAL_ENROLL         = 353000;{OP: 26/06/08}
      H_CAT_ROLES             = 368000;{OP: 26/06/08}
      H_EMP_USUARIO           = 371000;{OP: 26/06/08}
      H_MatrizCertificacionesPorPuesto  = 358000;{OP: 25/06/08}
      H_Matriz_PuestosCertificacion     = 359000;{OP: 25/06/08}
      H_Matriz_Edt_PuestoCertifica = 359000;{OP: 25/06/08}
      H_Matriz_Edt_CertificaPuesto = 358000;{OP: 25/06/08}
      H_Certificaciones_Programadas = 361000;{OP: 25/06/08}
      H_RDD_EXPORTAR_DICCION  = 903102;{OP: 04/02/16}
      H_RDD_IMPORTAR_DICCION  = 903101; {OP: 04/02/16}
      H_EMP_EXP_INFONAVIT     = 362000;{OP: 25/06/08}
      H_EMP_EDIT_EXP_INFONAVIT = 36200;   //act. HC, anterior  363000;{OP: 25/06/08}
      H_IMSS_CONCILIA_INFONAVIT = 364000;{OP: 26/06/08}
      H_WIZ_AJUSTE_RET_INFONAVIT = 366000;{OP: 26/06/08}
      H_GRID_AJUSTE_TARJETAS_PERIODO = 367000;{OP: 27/06/08}
      H_GRID_AJUSTE_TARJETAS_PERIODO_SUPER = 501;   //act. HC, anterior 102000;{OP: 27/06/08}

      { Visitantes } //acl
      H_VIS_PRINCIPAL = 3010000;
      H_VIS_REG_ENTRADA = 3020100;
      H_VIS_BUSQ_LIBRO = 3020200;
      H_VIS_CIERRE_CORTE = 3030000;
      H_VIS_MENU_CASETA = 3040000;
      H_VIS_CASETA_ESCOJE = 3040100;
      H_VIS_CASETA_CONFIGURA = 3040200;
      H_VIS_CASETA_REG_ENTRADA = 3040300;
      H_VIS_INF_USUARIOS = 3050000;
      H_VIS_REGISTRO_ENTRADA = 4010000;
      H_VIS_BUS_VISITANTE = 3010100;

      { VisitantesMgr} //acl // HelpContext:= ;
      H_VISMGR_PRINCIPAL = 2010000;
      
      H_VISMGR_CONS_LIBROS = 2020100;
      H_VISMGR_EDIT_LIBROS = 2020200;
      H_VISMGR_CONS_CITAS = 2020300;
      H_VISMGR_EDIT_CITAS = 2020400;
      H_VISMGR_CONS_CORTES = 2020500;
      H_VISMGR_EDIT_CORTES = 2020600;

      H_VISMGR_REPORTES = 2030100;
      H_VISMGR_REPORTES_ASIST = 2030200;
      H_VISMGR_BITACORA = 2030300;
      H_VISMGR_SQL = 2030400;

      H_VISMGR_CONS_ANFITRIONES = 2040100;
      H_VISMGR_EDIT_ANFITRIONES = 2040200;
      H_VISMGR_CONS_VISITANTES = 2040300;
      H_VISMGR_EDIT_VISITANTES = 2040400;
      H_VISMGR_CONS_CASETAS = 2040500;
      H_VISMGR_EDIT_CASETAS = 2040600;
      H_VISMGR_CONS_COMPANIAS = 2040700;
      H_VISMGR_EDIT_COMPANIAS = 2040800;

      H_VISMGR_CONS_DEPARTAMENTO = 2050100;
      H_VISMGR_EDIT_DEPARTAMENTO = 2050200;
      H_VISMGR_CONS_TIDENTIF = 2050300;
      H_VISMGR_EDIT_TIDENTIF = 2050400;
      H_VISMGR_CONS_TVEHICULO = 2050500;
      H_VISMGR_EDIT_TVEHICULO = 2050600;
      H_VISMGR_CONS_TASUNTO = 2050700;
      H_VISMGR_EDIT_TASUNTO = 2050800;
      H_VISMGR_CONS_TVISITANTE = 2050900;
      H_VISMGR_EDIT_TVISITANTE = 2051000;
      H_VISMGR_CONS_TCONDICION = 2051100;
      H_VISMGR_EDIT_TCONDICION = 2051200;

      H_VISMGR_CONS_EMPRESAS = 2060100;
      H_VISMGR_EDIT_EMPRESAS = 2060200;
      H_VISMGR_CONS_GPO_USR = 2060300;
      H_VISMGR_EDIT_GPO_USR = 2060400;
      H_VISMGR_CONS_USUARIOS = 2060500;
      H_VISMGR_EDIT_USUARIOS = 2060600;
      H_VISMGR_CONS_IMPRESORAS = 2060700;
      H_VISMGR_EDIT_IMPRESORAS = 2060800;
      H_VISMGR_GLOB_EMPRESAS = 2060900;
      H_VISMGR_GE_IDENTIF = 2060901;
      H_VISMGR_GE_SEGURIDAD = 2060902;
      H_VISMGR_GE_CORREOS = 2060903;
      H_VISMGR_GE_LIBRO = 2060904;
      H_VISMGR_GE_BITACORA = 2060905;
      H_VISMGR_DICCIONARIO = 2061000;
      H_VISMGR_EDIT_DICCION = 2061001;

      {Caseta}
      H_EMP_CASETA = 10010100;
      H_EMP_CASETA_DIARIAS = 10010110;
      H_EMP_REG_CASETA_DIARIAS = 10010120;
      H_REGLAS_CASETA = 10010130;

      {Lista de Dispositivos}
      H_LISTA_DISPOSIT = 393000;
      H_EDIT_LISTA_DISPOSIT = 394000;
      H_DISPOSIT_GLOBAL = 395000;

      H_ASIST_AJUST_INCAPA_CAL = 396000;
      {Cursos Programador Globales}
      H_EMP_CURSOS_PROG_GLOBAL = 387000;
      H_EMP_CANC_CURSO_PROG_GLOBAL = 392000;

      {Conciliador IMSS}
      H_CONCIL_COMENZAR = 100000;
      H_CONCIL_BIENVENIDO = 101000;
      H_CONCIL_SEGURIDAD =  102000;
      H_CONCIL_ACCESO = 103000;
      H_CONCIL_INTERFAZ = 200000;
      H_CONCIL_SUA = 300000;
      H_CONCIL_IMSS = 400000;
      H_CONCIL_COMO_HACER = 500000;
      H_CONCIL_COMO_HACER_COMPDATOS = 501000;
      H_CONCIL_DIF_TOTAL = 901000;
      H_CONCIL_EM_DIF_DAT_MONT = 902000;
      H_CONCIL_DIF_CURP = 903000;
      H_CONCIL_EMP_NO_REG_IMSS = 904000;
      H_CONCIL_EMP_NO_REG_TRESS = 905000;
      H_CONCIL_MOV_NO_REG_IMSS = 906000;
      H_CONCIL_MOV_NO_REG_TRESS = 907000;
      H_CONCIL_DIF_INFONAVIT = 908000;
      H_CONCIL_CRE_REG_TRESS_NO_INF = 909000;
      H_CONCIL_CRE_NO_TRESS_REG_INF = 910000;
      H_CONCIL_CRE_SUSPENDIDOS_TRESS = 908500;
      H_CONCIL_ARCHIVOS_CONCILIACION = 911000;
      H_CONCIL_WIZ_ACTUALIZAR_DATOS_INFONAVIT = 912000;
      H_CONCIL_WIZ_IMPORTAR_CREDITOS = 913000;
      H_CONCIL_WIZ_IMPORTAR_SUSPENSIONES = 914000;
      H_CONCIL_WIZ_IMPORTAR_REINICIO = 915000;

      {Conciliador FONACOT}
      H_CONCIL_FONACOT_RESUMEN_EMPLEADOS = 301000;
      H_CONCIL_FONACOT_NUEVOS_EMPLEADOS = 303000;
      H_CONCIL_FONACOT_CREDITOS_NUEVOS = 304000;
      H_CONCIL_FONACOT_CREDITOS_DIFERENCIAS = 306000;
      H_CONCIL_FONACOT_CREDITOS_ACTIVOS_NO_CEDULA = 302000;
      H_CONCIL_FONACOT_CREDITOS_SALDADOS_ACTIVOS = 305000;
      H_CONCIL_FONACOT_ERRORES_CEDULA = 307000;
      H_CONCIL_FONACOT_CEDULA_PAGO = 400001;
      H_CONCIL_FONACOT_CALCULAR_CONFRONTA = 400000;
      H_CONCIL_FONACOT_GENERAR_CEDULA = 502000;
      H_CONCIL_FONACOT_CONCILIAR_CREDITOS = 25001;
      H_CONCIL_FONACOT_NUEVOS_EMPLEADOS_CREDITOS = 25002;
      H_CONCIL_FONACOT_IMPORTAR_NUEVOS_CREDITOS = 25003;
      H_CONCIL_FONACOT_ACTUALIZA_DATOS_CREDITOS = 25004;


      {Versi�n 2009}
      H_CAT_EDIT_CONDICIONES = 60634;   //act. HC, anterior  382000;
      H_REP_IMP_SUB = 100003;

      {Versi�n 2010}
      H_Estados_pais_Edicion = 70716;   //act. HC, anterior  415000;
      H_Grado_Estudios_Edicion = 70714;   //act. HC, anterior  430000;
      H_TABLA_OCUPACIONES_NACIONALES = 418000;
      H_TABLA_OCUPACIONES_NACIONALES_EDICION = 418000;   //act. HC, anterior  419000;
      H_TABLA_AREA_TEMATICA_CURSOS = 421000;
      H_TABLA_AREA_TEMATICA_CURSOS_EDICION = 421000;   //act. HC, anterior  422000;
      H_REG_TARJETAS_GASOLINA = 412000;
      H_REG_TARJETAS_DESPENSA = 11513;   //act. HC, anterior  414000;
      H_REG_BANCA_ELECTRONICA = 414000;
      H_TABLA_MUNICIPIOS = 416000;
      H_TABLA_MUNICIPIOS_EDICION = 416000;   //act. HC, anterior  417000;
      H_CAT_ESTABLECIMIENTOS = 431000;
      H_CAT_ESTABLECIMIENTOS_EDICION = 432000;

      {TODO Version 2011}
      H_AutorizacionPrenomina = 437000;
      H_Validacion_Movimientos_IDSE = 1000005;
      H_Registrar_Excepciones_Festivo = 1000006;
      H_Globales_De_MisDatos = 10020206;
      H_Importacion_Clasificaciones = 430000;  //act. HC

      {TODO Version 2012}
      H_Cat_TiposPension = 10020223;
      H_Cat_TiposPension_Edit = 10020223;  //act. HC, anterior  10020224;
      H_EMP_EXP_PENSIONES = 10020219;
      H_EMP_EXP_PENSIONES_EDIT = 10020219;   //act. HC, anterior 10020220;
      H_Cat_SGM = 10020207;
      H_Cat_SGM_EDIT = 10020207;  //act. HC, anterior   10020225;
      H_EMP_EXP_SGM = 10020208;
      H_EMP_EXP_SGM_EDIT = 10020218;
      H_IMPORTAR_SGM = 10020209;
      H_RENOVAR_SGM = 10020210;
      H_BORRAR_SGM = 10020221;
      {Version Costeo 2011}
      H_Registro_Transferencias_Supervisores = 300001;
      H_Transferencias_Supervisores = 300000;
      H_Grid_Aprobar_Costeo_Transferencia = 300100;
      H_Cancelar_Transferencias =10020103;
      H_Agregar_Transferencias = 10020222;
      H_Calcula_Costeo_Transferencias = 10020102;
      H_Transferencias_Tress = 10020000;
      H_Edit_Transferencias_Tress = 10020001;
      H_Tabla_Motivo_Transferencia = 10020300;
      H_Tabla_Motivo_Transferencia_Edit = 10020300;   //act. HC, anterior  10020226;
      // (JB) Costeo
      H00002_Costeo_Grupos = 10020201;
      H00002_Edit_Costeo_Grupos = 10020201;   //act. HC, anterior   10020202;
      H00003_Costeo_Criterios = 10020203;
      H00003_Edit_Costeo_Criterios = 10020203;  //act. HC, anterior  10020204;
      H00003_Costeo_CriteriosPorConcepto = 10020205;
      H00003_Edit_Costeo_CriteriosPorConcepto = 10020205;   //act. HC, anterior   10020206;
      {2da Fase SGM }
      H_Cat_TablasAmortizacion = 484000;
      H_Cat_TablasAmortizacionEdicion = 484000;

      {Proyecto Ajusta_ISR() }
      H_Calculo_Previo_ISR = 443000;

      { WorkFlowCFG }
      H_WORKFLOWCFG_COMENZARCON                 = 8000000;
      H_WORKFLOWCFG_BIENVENIDO                  = 8010000;
      H_WORKFLOWCFG_SEGURIDAD                   = 8020000;
      H_WORKFLOWCFG_ACCESO                      = 8030000;
      H_WORKFLOWCFG_INTERFAZADMIN               = 9010000;
      H_WORKFLOWCFG_HISTORIAL                   = 9020000;
      H_WORKFLOWCFG_HISTORIAL_PROCESOS          = 9020100;
      H_WORKFLOWCFG_HISTORIAL_PROCESOS_EDIC     = 9020200;
      H_WORKFLOWCFG_HISTORIAL_TAREAS            = 9020300;
      H_WORKFLOWCFG_HISTORIAL_TAREAS_EDIC       = 9020400;
      H_WORKFLOWCFG_HISTORIAL_CORREOS           = 9020500;
      H_WORKFLOWCFG_HISTORIAL_CORREOS_CORTES    = 9020600;
      H_WORKFLOWCFG_CONSULTAS                   = 9030000;
      H_WORKFLOWCFG_CONSULTAS_REPORTES          = 9030100;
      H_WORKFLOWCFG_CONSULTAS_REPORTES_GEN      = 9030200;
      H_WORKFLOWCFG_CONSULTAS_BITACORA          = 9030300;
      H_WORKFLOWCFG_CONSULTAS_SQL               = 9030400;
      H_WORKFLOWCFG_CONSULTAS_PROCESOS          = 9030500;
      H_WORKFLOWCFG_CATALOGOS                   = 9040000;
      H_WORKFLOWCFG_CATALOGOS_MODELOS           = 9040100;
      H_WORKFLOWCFG_CATALOGOS_MODELOS_EDIC      = 9040200;
      H_WORKFLOWCFG_CATALOGOS_PASOSMODELOS      = 9040300;
      H_WORKFLOWCFG_CATALOGOS_PASOSMODELOS_EDIC = 9040400;
      H_WORKFLOWCFG_CATALOGOS_USUARIOS          = 9040500;
      H_WORKFLOWCFG_CATALOGOS_USUARIOS_EDIC     = 9040600;
      H_WORKFLOWCFG_CATALOGOS_ROLES             = 9040700;
      H_WORKFLOWCFG_CATALOGOS_ROLES_EDIC        = 9040800;
      H_WORKFLOWCFG_CATALOGOS_ACCIONES          = 9050000;
      H_WORKFLOWCFG_CATALOGOS_ACCIONES_EDIC     = 9050100;
      H_WORKFLOWCFG_CATALOGOS_CONEXIONES        = 9050200;
      H_WORKFLOWCFG_CATALOGOS_CONEXIONES_EDIC   = 9050300;
      H_WORKFLOWCFG_CATALOGOS_CONFIGURACION     = 9050400;
      H_WORKFLOWCFG_CATALOGOS_CONFIGURACION_GLOBAL = 9050500;
      H_WORKFLOWCFG_SISTEMA                     = 9060000;
      H_WORKFLOWCFG_SISTEMA_GPOUSUARIOS         = 9060300;
      H_WORKFLOWCFG_SISTEMA_GPOUSUARIOS_EDIC    = 9060400;
      H_WORKFLOWCFG_SISTEMA_USUARIOS            = 9060500;
      H_WORKFLOWCFG_SISTEMA_USUARIOS_EDIC       = 9060600;
      H_WORKFLOWCFG_SISTEMA_IMPRESORA           = 9060700;
      H_WORKFLOWCFG_SISTEMA_IMPRESORA_EDIC      = 9060800;
      H_SIST_BASE_DATOS                         = 10020220;
      H_SIST_ACTUALIZAR_BDS                     = 1002020;  // act. HC
      H_SIST_PROC_AGREGAR_BASE_DATOS            = 11515;
      H_SIST_PROC_AGREGAR_BASE_DATOS_PRES       = 10020605;

      {TODO Version 2013}
      H_Importacion_Organigrama = 486000;
      H_Tabla_TiposPerfiles = 490000;
      H_Tabla_TiposCompetencias = 489000;
      H_Cat_Competencias = 491000;
      H_Cat_GpoCompetencias = 492000;
      H_PlanCapacitacion =  494000;
      H_MatrizFunciones = 493000;
      H_Empleados_Edit_His_Competen = 495000;
      H_EMP_EVALUA_COMPETEN =495000;
      H_CAP_MATRIZ_HABLDS = 496000;
      H_CAP_MATRIZ_CURSOS = 494500;
      H_CAP_MATRIZ_CURSOS_SUPER = 10137;
      H_CAT_NAC_COMPS = 497000;

      { Terminales GTI }
      H_TRESS_SISTEMA_TERMINALESGTI_TERMINALES            = 467000;
      H_TRESS_SISTEMA_TERMINALESGTI_TERMINALES_EDIC       = 468000;
      H_TRESS_SISTEMA_TERMINALESGTI_GRUPOS                = 469000;
      H_TRESS_SISTEMA_TERMINALESGTI_GRUPOS_EDIC           = 470000;
      H_TRESS_SISTEMA_TERMINALESGTI_TERMINALESXGRUPO      = 471000;
      H_TRESS_SISTEMA_TERMINALESGTI_MSGXTERMINAL          = 472000;
      H_TRESS_SISTEMA_TERMINALESGTI_BITACORATERMINALESGTI = 473000;
      H_TRESS_SISTEMA_PROCESOS_ASIGNUMBIOMETRICO          = 474000;
      H_TRESS_SISTEMA_PROCESOS_ASIGGPOTERMINALES          = 475000;
      H_TRESS_SISTEMA_PROCESOS_BORRARBITTERMINALESGTI     = 476000;
      H_TRESS_SISTEMA_TERMINALESGTI_MSGXTERMINAL_EDIC     = 480000;
      H_TRESS_SISTEMA_TERMINALESGTI_EXTRAER_CHECADAS      = 80850;
      H_TRESS_SISTEMA_TERMINALESGTI_RECUPERAR_CHECADAS    = 80851;

      H_Sup_PlanCapacitacion =  104000;
      H_Sup_EMP_EVALUA_COMPETEN =  105000;
      H_Sup_MATRIZ_HABLDS =  106000;
      H_EMP_IMPORTAR_IMAGENES = 60633; 

      { Nombre DEFAULT de los Servicios que Utilizamos }
      MSSQL_DEFAULT_DEPENDENCY = 'MSSQLSERVER';
      INTERBASE_DEFAULT_DEPENDENCY = 'InterBaseServer';
      INTERBASE_GUARDIAN_DEFAULT_DEPENDENCY = 'InterBaseGuardian';
      FIREBIRD_DEFAULT_DEPENDENCY = 'FirebirdGuardianDefaultInstance';

      {Colores Status de Nomina}
      K_STATUS_COLOR_SINDEFINIR = $0000FF;   //rojo puro = clRed
      K_STATUS_COLOR_NUEVA      = $FF0000;   //azul puro  = clBlue
      K_STATUS_COLOR_AFECTADA   = $008000;   //verde puro = clGreen
      K_STATUS_COLOR_TIMBRADA   = $186FFF;   //naranja TRESS  HexCode FF6F18

      {Actualizacion HelpContext}
      H_ASIS_WIZ_INTERCAMBIO_FESTIVO = 224000;  //act. HC

      {ReporteadorCFG}
      H_Catalogo_Clasificaciones = 903103;
      H_Diccionario_Datos = 903104;
      H_Lista_Valores = 903105;
      H_Catalogo_Modulos = 903106;

      {Plan Carrera}
      H_Catalogo_Competencias = 89000;
      H_Catalogo_Acciones = 90000;
      H_Catalogo_Puestos = 91000;
      H_Tabla_Calificaciones = 92000;
      H_Tipos_Competencia = 93000;
      H_Catalogo_Familias = 94000;
      H_Catalog_Niveles = 95000;
      H_Catalogo_Dimensiones = 96000;
      H_Globales_Carrera = 97000;
      H_Fechas_Carrera = 97000;

      { Notificacion y Advertencias Timbrado }
      H65117_Globales_NotificacionAdvertencia = 65117;

      H394002_Configuracion_Cafeteria_TRESS = 394002;
      H394003_PantallaInicio_TRESS = 394003;
      H394004_PantallaInicioConfiguracion_TRESS = 394004;
      {*** US 15765: Es necesario separar los procesos (wizards) de recalculo de ahorros y pr�stamos para administrar de mejor manera los accesos al proceso y prevenir rec�lculos innecesarios***}
      H394005_Recalcular_prestamos = 394005;

      { Periodo sin confidencialidad }
      K_PERIODO_VACIO = -1;
      K_PERIODO_INICIAL = 0;

      { Reportes Email }
      H5005406_Administrador_Envios_Programados = 5005406;
      H5005407_Solicitudes_Envios_Programados = 5005407;
      H5005408_Bitacora_Reportes_Email = 5005408;
      H5005409_Bitacora_Correos = 5005409;

      { Huellas / Facial }
      K_TERMINAL_HUELLA = 1;
      K_TERMINAL_FACIAL = 2;

type
  TPesos = Currency;           // Cualquier campo relativo a dinero
  TTasa = Double;	       // Cuando se requiere 5 o m�s decimales
  TDiasHoras = Currency;       // Relativo a d�as y horas con 2 decimales
{$ifdef TRESS_DELPHIXE5_UP}
  TCodigo = String;            // C�digos de Tablas y Cat�logos
{$else}
  TCodigo = String[ 10 ];      // C�digos de Tablas y Cat�logos
{$endif}
  TNumEmp = Longint;           // N�mero de Empleado //
  TDatosCompania = record
    Codigo: TCodigo;
    Nombre: String;
  end;
  TDatosUsuario = record
    Codigo: Integer;
    Nombre: String;
  end;
  TDatosSeguridad = record
    PasswordExpiracion: Integer;
    PasswordLongitud: Integer;
    PasswordLetras: Integer;
    PasswordDigitos: Integer;
    PasswordLog: Integer;
    PasswordIntentos: Integer;
    Inactividad: Integer;
    TimeOut: Integer;
    UsuarioTareasAutomaticas: Integer; // (JB) Global de Usuario de TressAutomatiza
    //CafeteraServicio
    CafeteraPuerto: Integer;
    CafeteraHoraReinicio: TDateTime;
    // US #9666 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
    AdvertenciaLicenciaEmpleados: Boolean;
  end;
  {***US:13038 Timbrado - Enviar notificaci�n a un grupo de usuarios cuando una nomina afectada tiene pendiente el timbrado***}
  TDatosNotificacionAdvertencia = record
    UsuarioTareasAutomaticas: Integer; // (JB) Global de Usuario de TressAutomatiza
    // US #9666 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
    AdvertenciaLicenciaEmpleados: Boolean;
    // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo
    ServidorCorreos: String;
    PuertoSMTP: Integer;
    AutentificacionCorreo: Integer;
    UserID: String;
    EmailPSWD: String;
    RecibirAdvertenciaPorEmail: Boolean;
    GrupoUsuariosEmail: Integer;
  end;

  {***US #14275: Servicio de env�o de correos - Modificaci�n a pantallas de globales.***}
  TDatosServicioReportesCorreos = record
    DirectorioReportes: String;
    DirectorioImagenes: String;
    URLImagenes: String;
    ExpiracionImagenes: Integer;
    Recuperarpwsserviciocorreos: Boolean;
  end;

  TDatosClasificacion = record
    ZonaGeografica: TCodigo;
    Puesto: TCodigo;
    Clasificacion: TCodigo;
    Turno: TCodigo;
    Patron: TCodigo;
    Nivel1: TCodigo;
    Nivel2: TCodigo;
    Nivel3: TCodigo;
    Nivel4: TCodigo;
    Nivel5: TCodigo;
    Nivel6: TCodigo;
    Nivel7: TCodigo;
    Nivel8: TCodigo;
    Nivel9: TCodigo;
{$ifdef QUINCENALES}
    Salario: TPesos;
{$endif}
{.$ifdef CAMBIO_TNOM}
    Nomina: Integer;
{.$endif}
{$ifdef ACS}
    Nivel10: TCodigo;
    Nivel11: TCodigo;
    Nivel12: TCodigo;
{$endif}
    TablaPrestaciones: TCodigo;
  end;
  TDatosSalario = record
    Integrado: TPesos;
    Diario: TPesos;
  end;
  TDatosPrestamo = class
    Empleado: Integer;
    Referencia : string;
    Tipo: string;
    OldTipo: string;
    Fecha: TDate;
    Monto: TPesos;
    Status: eStatusPrestamo;
    OldStatus: eStatusPrestamo;
  end;
  TDatosPeriodo = record
   Year: Integer;
   Tipo: eTipoPeriodo;
   Numero: Integer;
   Inicio: TDate;
   Fin: TDate;
   Pago: TDate;
{$ifdef QUINCENALES}
   InicioAsis: TDate;
   FinAsis: TDate;
   TipoDiasBase: eTipoDiasBase;
   DiasBase: TTasa;
   FormulaDias: String;
   SumarJornadaHorarios: Boolean;
   FormulaTotal1: String;
   FormulaTotal2: String;
   FormulaTotal3: String;
   FormulaTotal4: String;
   FormulaTotal5: String;
   HayFuncionesTotales: Boolean;
{$endif}
   Dias: Integer;
   Status: eStatusPeriodo;
   StatusTimbrado : eStatusTimbrado; 
   Uso: eUsoPeriodo;
   Mes: Byte;
   MesFonacot: Byte;
   MesActivo: Byte;
   SoloExcepciones : Boolean;
   IncluyeBajas : Boolean;
   PosMes : Integer;
   PerMes : Integer;
   DiasAcumula : Integer;
   PerAnio : Integer;
   Usuario : Integer;
   DescuentaAhorros : Boolean;
   DescuentaPrestamos : Boolean;
   SoloEx : string;
   Descripcion : string;
   NumeroEmpleados : integer;
   EsCalendario:Boolean;
   EsUltimaNomina:Boolean;
   Clasifi: eClasifiPeriodo;
  end;
  TDatosImss = Record
   Patron: String;
   Mes: Integer;
   Year: Integer;
   Tipo: eTipoLiqIMSS;
   Status: eStatusLiqIMSS;
   Modifico: Integer;
  end;
  TDatosRitmo = record
    Inicio: TDate;
    Patron: String;
    Horario1: TCodigo;
    Horario2: TCodigo;
    Horario3: TCodigo;
  end;
  TStatusHorario = record
    Horario: TCodigo;
    Status: eStatusAusencia;
    DiaVacacion : TDiasHoras;
  end;
  TDatosJornada = record
    Codigo: String;
    TipoTurno: eTipoTurno;
    TipoJornada: eTipoJornada;
    Dias: Word;
    Jornada: TDiasHoras;
    Dobles: TDiasHoras;
    Domingo: TDiasHoras;
    HorarioFestivo: String;
    TieneHorarioFestivo: Boolean;
{$ifdef QUINCENALES}
    DiasBase: TDiasHoras;
{$endif}
{$ifdef TJORNADA_FESTIVOS}
    TipoJorTrabajo: eTipoJornadaTrabajo;
{$endif}
  end;
  TListaObjetos = class( TStringList )
  private
    procedure FreeAll;
  public
    destructor Destroy; override;
    procedure  Empty;
  end;
  TAhorroPresta = class
    Tipo: String;
    Confirmar: Boolean;
    EsAhorro: Boolean;
  end;
  TDatosComida = class
    Empleado: TNumEmp;
    Fecha: TDate;
    Hora: String[4];
    TipoComida: Char;
    NumComidas: Integer;
    EmpresaNombre: String;
    Turno: TCodigo;
    Horario: TCodigo;
    StatusDia: eStatusAusencia;
    StatusEmpleado: eStatusEmpleado;
    TipoChecada: eTipoChecadas;
    Estacion: String[ 4 ]; {Se llena en FCoffeShop cuando se preparan los datos de cafeter�a}
  end;
  TCalendario = class
    CalInicial: string;
    CalFinal: string;
    dCalInicial: TDate;
    dCalFinal: TDate;
    CalFiltro: String;
  end;
  TDatosNomInfo = class
    dInicialRangoNomInfo : TDate;
    dFinalRangoNomInfo : TDate;
    Empleado : TNumEmp;
    CuotaBimestralNomInfo : TPesos;
    DiasCotizadosBimestreNomInfo : TPesos;
    SeguroViviendaNomInfo : TPesos;
    iYearIMSS, iMesIMSS: Integer;
  end;
  TDatosNomm = class
    ScriptNomm: string;
    ScriptNommTipo: string;
    TotalesNomm: string;
    UltimoTipo: eTipoPeriodo;
  end;

  TRitmoDia = class( TObject )
  public
    { Public declarations }
     Horario : TCodigo;
     StatusDia : eStatusAusencia;
     DiaVacacion : TDiasHoras;
  end;
  
  TRitmo = class(TObject)
  private
    {Private Declarations}
    FCodigo: TCodigo;
    FLista: TStrings;
    FInicioRitmo: TDate;
{$ifdef FALSE}
    FVectorDias: String;
    function GetStatus( const dFecha: TDate; var iPosicion: Word ): eStatusAusencia;
{$endif}
  public
    {Public Declarations}
    constructor Create( const sCodigo: TCodigo );
    destructor Destroy; override;
    property Codigo: TCodigo read FCodigo;
    function GetStatusHorario( const dFecha: TDate ): TStatusHorario;
    function GetDiasStatus( const dInicial, dFinal: TDate; const eStatus: eStatusAusencia ): Word;
    procedure LimpiaLista;
{$ifdef FALSE}
    procedure SetPatron( const dInicial: TDate; const sPatron: String; const HorarioHabil, HorarioDescanso: TCodigo );
{$endif}
    procedure SetPatron( const dInicial: TDate; const sPatron: String; const HorarioHabil, Horario2, Horario3: TCodigo );
  end;
  TProcAnimacion = procedure( const lEnabled: Boolean ) of object;
  
   TStringObject = class(TObject)
   private
     FTexto : String;
   public
     property Texto: String read FTexto write FTexto;
   end;
  TFunctionReemplazaVariables = function( var sMensaje, sAttach: string): string;
  TFunctionReemplazaVariablesLogin = function( var sMensaje, sAttach, sUrlImagen: string): string;
  TFunctionReemplazaVariablesError = function( var sMensaje, sAttach, sFecha, sError, sNombreCalendario: string): string;
implementation

{ ************** TZetaParams ****************** }

function TZetaParams.IndexOf( const sName: String ): Integer;
var
   i: Integer;
begin
     Result := -1;
     for i := 0 to ( Count - 1 ) do
     begin
          if ( {$ifdef DOTNET}CompareText{$else}AnsiCompareText{$endif}( Items[ i ].Name, sName ) = 0 ) then
          begin
               Result := i;
               Break;
          end;
     end;
end;

procedure TZetaParams.AsInteger( const sName: String; const iValue: Integer );
var
   i: Word;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          if ( {$ifdef DOTNET}CompareText{$else}AnsiCompareText{$endif}( Items[ i ].Name, sName ) = 0 ) then
             Items[ i ].AsInteger := iValue;
     end;
end;

procedure TZetaParams.AsFloat( const sName: String; const rValue: Extended );
var
   i: Word;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          if ( {$ifdef DOTNET}CompareText{$else}AnsiCompareText{$endif}( Items[ i ].Name, sName ) = 0 ) then
              Items[ i ].AsFloat := rValue;
     end;
end;

procedure TZetaParams.AsDate( const sName: String; const dValue: TDate );
var
   i: Word;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          if ( {$ifdef DOTNET}CompareText{$else}AnsiCompareText{$endif}( Items[ i ].Name, sName ) = 0 ) then
             Items[ i ].AsDate := dValue;
     end;
end;

procedure TZetaParams.AsBlob( const sName: String; const bValue: TBlobData );
var
   i: Word;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          if ( {$ifdef DOTNET}CompareText{$else}AnsiCompareText{$endif}( Items[ i ].Name, sName ) = 0 ) then
             Items[ i ].AsBlob := bValue;
     end;
end;

procedure TZetaParams.AsBoolean( const sName: String; const lValue: Boolean );
var
   i: Word;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          if ( {$ifdef DOTNET}CompareText{$else}AnsiCompareText{$endif}( Items[ i ].Name, sName ) = 0 ) then
             Items[ i ].AsBoolean := lValue;
     end;
end;

procedure TZetaParams.AsString( const sName, sValue: String );
var
   i: Word;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          if ( {$ifdef DOTNET}CompareText{$else}AnsiCompareText{$endif}( Items[ i ].Name, sName ) = 0 ) then
             Items[ i ].AsString := sValue;
     end;
end;

procedure TZetaParams.AsMemo( const sName, sValue: String );
var
   i: Word;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          if ( {$ifdef DOTNET}CompareText{$else}AnsiCompareText{$endif}( Items[ i ].Name, sName ) = 0 ) then
             Items[ i ].AsMemo := sValue;
     end;
end;

procedure TZetaParams.AsVariant(const sName: String; oVariant: Variant);
var
   i: Word;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          if ( {$ifdef DOTNET}CompareText{$else}AnsiCompareText{$endif}( Items[ i ].Name, sName ) = 0 ) then
             Items[ i ].Value := oVariant;
     end;
end;

function TZetaParams.FindCreateParam( FldType: TFieldType; const sName: String ): TParam;
begin
     Result := FindParam( sName );
     if ( Result = nil ) then
        Result := CreateParam( FldType, sName, ptInputOutput );
end;

{$ifdef DOTNET}{$WARNINGS OFF}{$endif}
function TZetaParams.GetVarValues: OleVariant;
var
   i, iHigh: Integer;
begin
     iHigh := Count - 1;
     Result := VarArrayCreate( [ 0, 2, 0, iHigh ], varVariant );
     for i := 0 to iHigh do
     begin
          with Items[ i ] do
          begin
               Result[ 0, i ] := Name;
               Result[ 1, i ] := Ord( DataType );
               Result[ 2, i ] := Value;
          end;
     end;
end;
{$ifdef DOTNET}{$WARNINGS ON}{$endif}

procedure TZetaParams.SetVarValues( Values: OleVariant );
var
   i, iLow, iHigh: Integer;
begin
     iLow := VarArrayLowBound( Values, 1 );
     iHigh := VarArrayHighBound( Values, 2 );
     for i := iLow to iHigh do
     begin
          with FindCreateParam( TFieldType( Values[ 1, i ] ), Values[ 0, i ] ) do
          begin
               Value := Values[ 2, i ];
          end;
     end;
end;

procedure TZetaParams.AddInteger( const sName: String; const iValue: Integer );
begin
     with FindCreateParam( ftInteger, sName ) do
     begin
          AsInteger := iValue;
     end;
end;

procedure TZetaParams.AddFloat( const sName: String; const rValue: Extended );
begin
     with FindCreateParam( ftFloat, sName ) do
     begin
          AsFloat := rValue;
     end;
end;

procedure TZetaParams.AddDate( const sName: String; const dValue: TDate );
begin
     with FindCreateParam( ftDate, sName ) do
     begin
          AsDate := dValue;
     end;
end;

procedure TZetaParams.AddDateTime( const sName: String; const dValue: TDateTime );
begin
     with FindCreateParam( ftDateTime, sName ) do
     begin
          AsDateTime := dValue;
     end;
end;

procedure TZetaParams.AddString( const sName, sValue: String );
begin
     with FindCreateParam( ftString, sName ) do
     begin
          AsString := sValue;
     end;
end;

procedure TZetaParams.AddMemo( const sName, sValue: String );
begin
     with FindCreateParam( ftMemo, sName ) do
     begin
          AsMemo := sValue;
     end;
end;

procedure TZetaParams.AddBoolean( const sName: String; const lValue: Boolean );
begin
     with FindCreateParam( ftBoolean, sName ) do
     begin
          AsBoolean := lValue;
     end;
end;

procedure TZetaParams.AddBlob( const sName: String; const bValue: TBlobData );
begin
     with FindCreateParam( ftBlob, sName ) do
     begin
          AsBlob := bValue;
     end;
end;

procedure TZetaParams.AddVariant( const sName: String; oValue: Variant );
begin
     with FindCreateParam( ftVariant, sName ) do
     begin
          Value:= oValue;
     end;
end;

{$ifndef DOTNET}
{$endif}

{***************** TListaObjetos ******************}

procedure TListaObjetos.FreeAll;
var
   i: Integer;
begin
     for i := 0 to ( Count - 1 ) do
         Objects[ i ].Free;
end;

destructor TListaObjetos.Destroy;
begin
     FreeAll;
     inherited Destroy;
end;

procedure TListaObjetos.Empty;
begin
     FreeAll;
     Clear;
end;

{************************* TRitmo **************************}

constructor TRitmo.Create( const sCodigo: TCodigo );
begin
     {$ifdef DOTNET}
     inherited Create;
     {$endif}
     FCodigo := sCodigo;
{$ifdef FALSE}
     FVectorDias := VACIO;
{$endif}
     FLista := TStringList.Create;
end;

destructor TRitmo.Destroy;
var
   i: Integer;
begin
     with FLista do
     begin
          for i := ( Count - 1 ) downto 0 do
              TRitmoDia( Objects[ i ] ).Free;
          Free;
     end;
     inherited Destroy;
end;

procedure TRitmo.LimpiaLista;
var
   i: Integer;
begin
     with FLista do
     begin
          for i := ( Count - 1 ) downto 0 do
              TRitmoDia( Objects[ i ] ).Free;
          Clear;
     end;
end;

procedure TRitmo.SetPatron( const dInicial: TDate; const sPatron: String; const HorarioHabil,
          Horario2, Horario3: TCodigo );
var
   lUsaSabados : Boolean;
   eStatusDia  : eStatusAusencia;
   oListaTemp  : TStrings;
   sDias, sHorario: String;
   i, j, iPos: Word;
   iDias: Integer;
   oRitmoDia: TRitmoDia;
   rDiaVaca: TDiasHoras;

   function GetNextStatus: eStatusAusencia;
   begin
        case eStatusDia of
             auHabil :
             begin
                  if lUsaSabados then
                     Result := auSabado
                  else
                      Result := auDescanso;
             end;
             auSabado : Result := auDescanso;
             auDescanso : Result := auHabil;
        else
             Result := auHabil;   // En teoria no debe llegar aqu�
        end
   end;

   function GetHorarioDefault: TCodigo;
   begin
        case eStatusDia of
             auHabil : Result := HorarioHabil;
             auSabado : Result := Horario2;
             auDescanso :
             begin
                  if lUsaSabados then
                     Result := Horario3
                  else
                      Result := Horario2;
             end;
        end;
   end;

begin
     FInicioRitmo := dInicial;
     LimpiaLista;

     iPos := Pos( TOKEN_RITMO_HSD, sPatron );
     lUsaSabados := ( iPos > 0 );
     oListaTemp := TStringList.Create;
     try
        with oListaTemp do
        begin
             if lUsaSabados then
                CommaText := Copy( sPatron, iPos + 1, Length( sPatron ) )  // No incluir el signo #
             else
                 CommaText := sPatron;
             eStatusDia := auHabil;   // Comienza siendo dia Habil
             for i := 0 to ( Count - 1 ) do
             begin
                  rDiaVaca := -1;        // Si no est� definido se pone el valor -1 para ser ignorado en procesos posteriores.
                  sDias := Strings[ i ];
                  iPos := Pos( ':', sDias );
                  if ( iPos > 0 ) then
                  begin
                       iDias := StrToIntDef( Trim( Copy( sDias, 1, ( iPos - 1 ) ) ), 0 );
                       sHorario := Copy( sDias, ( iPos + 1 ), MAXINT );
                       iPos := Pos( '%', sHorario );                    // Si se agrega un '%' se incluy� el valor de d�a para Vacaciones
                       if ( iPos > 0 ) then
                       begin
                            rDiaVaca := StrToFloatDef( Trim( Copy( sHorario, ( iPos + 1 ), MAXINT ) ), 0 );
                            sHorario := Copy( sHorario, 1, ( iPos - 1 ) );
                       end;
                  end
                  else
                  begin
                       sHorario := GetHorarioDefault;
                       iPos := Pos( '%', sDias );                    // Si se agrega un '%' se incluy� el valor de d�a para Vacaciones
                       if ( iPos > 0 ) then
                       begin
                            iDias := StrToIntDef( Trim( Copy( sDias, 1, ( iPos - 1 ) ) ), 0 );
                            rDiaVaca := StrToFloatDef( Trim( Copy( sDias, ( iPos + 1 ), MAXINT ) ), 0 );
                       end
                       else
                           iDias := StrToIntDef( Trim( sDias ), 0 );
                  end;
                  for j := 1 to iDias do
                  begin
                       oRitmoDia := TRitmoDia.Create;
                       with oRitmoDia do
                       begin
                            Horario	:= sHorario;
                            StatusDia	:= eStatusDia;
                            DiaVacacion	:= rDiaVaca;
                       end;
                       FLista.AddObject( sHorario, oRitmoDia );
                  end;
                  eStatusDia := GetNextStatus;
             end;
        end;
     finally
            FreeAndNil( oListaTemp );
     end;

     if ( FLista.Count = 0 ) then
     begin
          {$ifdef DOTNET}raise Exception.Create({$else}DatabaseError({$endif} 'Error en Patr�n de Turno R�tmico' );
     end;
end;

function TRitmo.GetStatusHorario( const dFecha: TDate ): TStatusHorario;
var
   iPosRitmo: Integer;
begin
     iPosRitmo := Trunc( dFecha - FInicioRitmo );
     if ( iPosRitmo < 0 ) then
        iPosRitmo := 0;
     iPosRitmo := ( iPosRitmo mod FLista.Count );
     with Result do
     begin
          Status := TRitmoDia( FLista.Objects[ iPosRitmo ] ).StatusDia;
          DiaVacacion := TRitmoDia( FLista.Objects[ iPosRitmo ] ).DiaVacacion;
          Horario := FLista[ iPosRitmo ];
     end;
end;

function TRitmo.GetDiasStatus( const dInicial, dFinal: TDate; const eStatus: eStatusAusencia ): Word;
var
   dFecha : TDate;
begin
     Result := 0;
     dFecha := dInicial;
     while ( dFecha <= dFinal ) do
     begin
          if ( GetStatusHorario( dFecha ).Status = eStatus ) then
             Result := Result + 1;
          dFecha := dFecha + 1;
     end;
end;

{$ifdef FALSE}
procedure TRitmo.SetPatron( const dInicial: TDate; const sPatron: String; const HorarioHabil, HorarioDescanso: TCodigo );
var
   lHabil: Boolean;
   i, j, iPos: Word;
   sDias, sHorario: String;
   iDias: Integer;
   cPosicion : Char;
begin
     FInicioRitmo := dInicial;
     FVectorDias := VACIO;
     lHabil := True;
     with FLista do
     begin
          CommaText := sPatron;
          for i := 0 to ( Count - 1 ) do
          begin
               sDias := Strings[ i ];
               iPos := Pos( ':', sDias );
               if ( iPos > 0 ) then
               begin
                    iDias := StrToIntDef( Trim( Copy( sDias, 1, ( iPos - 1 ) ) ), 0 );
                    sHorario := Copy( sDias, ( iPos + 1 ), MAXINT );
               end
               else
               begin
                    iDias := StrToIntDef( Trim( sDias ), 0 );
                    if lHabil then
                       sHorario := HorarioHabil
                    else
                        sHorario := HorarioDescanso;
               end;
               Strings[ i ] := sHorario;
               { Evita caracteres de control }
               cPosicion := Chr( i + K_OFFSET_VECTOR_RITMO );
               for j := 1 to iDias do
                   FVectorDias := FVectorDias + cPosicion;
               lHabil := not lHabil;
          end;
     end;
     if Length( FVectorDias ) = 0 then
     begin
          {$ifdef DOTNET}raise Exception.Create({$else}DatabaseError({$endif} 'Error en Patr�n de Turno R�tmico' );
     end;
end;

function TRitmo.GetStatus( const dFecha: TDate; var iPosicion: Word ): eStatusAusencia;
var
   iPosRitmo: Integer;
   cPosicion: Char;
begin
     iPosRitmo := Trunc( dFecha - FInicioRitmo );
     if ( iPosRitmo < 0 ) then
        iPosRitmo := 0;
     iPosRitmo := ( iPosRitmo mod Length( FVectorDias ) ) + 1;
     cPosicion := FVectorDias[ iPosRitmo ];
     iPosicion := Ord( cPosicion ) - K_OFFSET_VECTOR_RITMO;
     if ( ( iPosicion mod 2 ) = 0 ) then
        Result := auHabil
     else
        Result := auDescanso;
end;

function TRitmo.GetStatusHorario( const dFecha: TDate ): TStatusHorario;
var
   iPosicion: Word;
begin
     with Result do
     begin
          Status := GetStatus( dFecha, iPosicion );
          Horario := FLista[ iPosicion ];
     end;
end;

function TRitmo.GetDiasStatus( dInicial, dFinal: TDate; const eStatus: eStatusAusencia ): Word;
var
   iPosicion : Word;
begin
     Result := 0;
     while ( dInicial <= dFinal ) do
     begin
         if ( GetStatus( dInicial, iPosicion ) = eStatus ) then
            Result := Result + 1;
         dInicial := dInicial + 1;
     end;
end;
{$endif}

end.

