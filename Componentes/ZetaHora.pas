unit ZetaHora;

interface

uses WinTypes, Classes, StdCtrls, Controls, Messages,
     SysUtils, Forms, Graphics, Menus, Buttons, Dialogs,
     Mask,
     MaskUtils,
     DB, DBCtrls, Comctrls, Clipbrd;

type
  TCheckTimeEvent = procedure( Sender: TObject; var Horas, Minutos, Segundos: Word; var Changed: Boolean ) of object;
  TZetaHora = class( TCustomMaskEdit )
  private
    { Private declarations }
    FCanvas: TControlCanvas;
    FValor: String;
    FUseEnterKey: Boolean;
    FUseSeconds: Boolean;
    FTope: Word;
    FCheck: Boolean;
    FOnCheckTime: TCheckTimeEvent;
    FOnValidTime: TNotifyEvent;
    function GetHoras: Integer;
    function GetHourValue: TDateTime;
    function GetMinutos: Integer;
    function GetSegundos: Integer;
    {
    function GetTextMargins: TPoint;
    }
    function GetValorAsText: String;
    procedure DisplayTime;
    procedure DisplayText( const Value: String );
    {
    procedure WMPaint( var Message: TWMPaint ); message WM_PAINT;
    }
    procedure WMCopy( var Message: TMessage ); message WM_COPY;
    procedure WMCut( var Message: TMessage ); message WM_CUT;
    procedure WMPaste( var Message: TMessage ); message WM_PASTE;
    procedure CMExit( var Message: TCMExit ); message CM_EXIT;
    procedure InitEditMask;
    procedure SetUseSeconds( const Value: Boolean );
    procedure SetTope( const Value: Word );
    procedure SetValor( const Value: String );
  protected
    { Protected declarations }
    function GetValor: String; virtual;
    function GetTimeValue: Boolean;
    function ValidTime( var sTime: String ): Boolean;
    function HoraVacia: Boolean;
    function ValidEdit: Boolean; virtual;
    procedure ChangeValue( const Value: String ); virtual;
    procedure DoEnter; override;
    procedure DoEdit;
    procedure DoOnCheckTime;
    procedure DoOnValidTime;
    procedure Enfocar;
    procedure HayError;
    procedure KeyDown( var Key: Word; Shift: TShiftState); override;
    procedure KeyPress( var Key: Char ); override;
    procedure Loaded; override;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    property Horas: Integer read GetHoras;
    property Minutos: Integer read GetMinutos;
    property Segundos: Integer read GetSegundos;
    property HourValue: TDateTime read GetHourValue;
    function BuildTime(const iHoras, iMinutos, iSegundos: Integer): String;
    procedure CopyToClipboard;
    procedure CutToClipboard;
    procedure PasteFromClipboard;
    procedure SetValorNumerico(const iHoras, iMinutos, iSegundos: Integer);
  published
    { Published declarations }
    property AutoSelect;
    property AutoSize;
    property BorderStyle;
    property CharCase;
    property Color;
    property Ctl3D;
    property DragCursor;
    property DragMode;
    property Enabled;
    property EditMask;
    property Font;
    property ImeMode;
    property ImeName;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PasswordChar;
    property PopupMenu;
    property ReadOnly;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Text;
    property Tope: Word read FTope write SetTope;
    property UseEnterKey: Boolean read FUseEnterKey write FUseEnterKey default False;
    property UseSeconds: Boolean read FUseSeconds write SetUseSeconds default False;
    property Valor: String read GetValor write SetValor;
    property Visible;
    property OnChange;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDrag;
    property OnCheckTime: TCheckTimeEvent read FOnCheckTime write FOnCheckTime;
    property OnValidTime: TNotifyEvent read FOnValidTime write FOnValidTime;
  end;
  TZetaDBHora = class( TZetaHora )
  private
    { Private declarations }
    FDataLink: TFieldDataLink;
    function ReadValor: String;
    function GetDataField: String;
    function GetDataSource: TDataSource;
    function GetField: TField;
    function GetReadOnly: Boolean;
    procedure SetDataField( const Value: string );
    procedure SetDataSource( Value: TDataSource );
    procedure SetReadOnly( const Value: Boolean );
    procedure CheckFieldType( const Value: String );
    procedure DataChange( Sender: TObject );
    procedure EditingChange( Sender: TObject );
    procedure UpdateData( Sender: TObject );
    procedure ActiveChange( Sender: TObject );
    procedure CMGetDataLink( var Message: TMessage ); message CM_GETDATALINK;
  protected
    { Protected declarations }
    function GetValor: String; override;
    function ValidEdit: Boolean; override;
    procedure Change; override;
    procedure ChangeValue( const Value: String ); override;
    procedure Loaded; override;
    procedure Notification( AComponent: TComponent; Operation: TOperation ); override;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    property Field: TField read GetField;
  published
    { Published declarations }
    property DataField: string read GetDataField write SetDataField;
    property DataSource: TDataSource read GetDataSource write SetDataSource;
    property ReadOnly: Boolean read GetReadOnly write SetReadOnly default False;
  end;

implementation

uses Winprocs,
     ZetaCommonClasses,
     ZetaCommonTools;

{ ********* TZetaHora **************** }

constructor TZetaHora.Create( AOwner: TComponent );
begin
     FTope := 24;
     FUseEnterKey := False;
     FUseSeconds := False;
     FCheck := True;
     inherited Create( AOwner );
     Width := 40;
     ControlStyle := ControlStyle - [csSetCaption] + [csReplicatable];
end;

destructor TZetaHora.Destroy;
begin
     FCanvas.Free;
     inherited Destroy;
end;

procedure TZetaHora.Loaded;
begin
     inherited Loaded;
     InitEditMask;
     DisplayTime;
end;

function TZetaHora.GetValor: String;
begin
     Result := FValor;
end;

procedure TZetaHora.SetValor( const Value: String );
begin
     if ( Value <> FValor ) then
     begin
          ChangeValue( Value );
          Modified := True;
          DoOnValidTime;
     end;
     DisplayTime;
end;

function TZetaHora.BuildTime(const iHoras, iMinutos, iSegundos: Integer): String;
begin
     if ( iHoras >= 0 ) and ( iHoras < FTope ) and
        ( iMinutos >= 0 ) and ( iMinutos <= 59 ) and
        ( not FUseSeconds or ( iSegundos >= 0 ) and ( iSegundos <= 59 ) ) then
     begin
          if FUseSeconds then
             Result := Format( '%2.2d%2.2d%2.2d', [ iHoras, iMinutos, iSegundos ] )
          else
              Result := Format( '%2.2d%2.2d', [ iHoras, iMinutos ] );
     end
     else
         Result := '';
end;

procedure TZetaHora.SetValorNumerico(const iHoras, iMinutos, iSegundos: Integer);
begin
     Valor := BuildTime( iHoras, iMinutos, iSegundos );
end;

procedure TZetaHora.SetTope( const Value: Word );
begin
     if ( FTope <> Value ) then
     begin
          if ( Value = 24 ) or ( Value = 48 ) then
             FTope := Value
          else
              raise Exception.Create( 'Tope Puede Ser 24 � 48' );
     end;
end;

procedure TZetaHora.InitEditMask;
begin
     if FUseSeconds then
        EditMask := '99:99:99;0'
     else
         EditMask := '99:99;0';
end;

function TZetaHora.GetHoras: Integer;
begin
     Result := StrToIntDef( Copy( FValor, 1, 2 ), 0 );
end;

function TZetaHora.GetMinutos: Integer;
begin
     Result := StrToIntDef( Copy( FValor, 3, 2 ), 0 );
end;

function TZetaHora.GetSegundos: Integer;
begin
     Result := StrToIntDef( Copy( FValor, 5, 2 ), 0 );
end;

function TZetaHora.HoraVacia: Boolean;
begin
     Result := ZetaCommonTools.StrVacio( Valor );
end;

function TZetaHora.ValidTime( var sTime: String ): Boolean;
begin
     if ( sTime = '' ) then
        Result := True
     else
     begin
          if FUseSeconds then
             Result := ZetaCommonTools.LongTimeIsValid( Tope, sTime )
          else
              Result := ZetaCommonTools.TimeIsValid( Tope, sTime );
     end;
end;

procedure TZetaHora.DoOnCheckTime;
var
   iHoras, iMinutos, iSegundos: Word;
   lChanged: Boolean;
begin
     if Assigned( FOnCheckTime ) then
     begin
          iHoras := GetHoras;
          iMinutos := GetMinutos;
          iSegundos := iSegundos;
          lChanged := False;
          FOnCheckTime( Self, iHoras, iMinutos, iSegundos, lChanged );
          if lChanged then
          begin
               FValor := BuildTime( iHoras, iMinutos, iSegundos );
          end;
     end;
end;

procedure TZetaHora.DoOnValidTime;
begin
     if Assigned( FOnValidTime ) then
        FOnValidTime( Self );
end;

function TZetaHora.GetValorAsText: String;
begin
     if FUseSeconds then
     begin
          case GetTextLen of
               1: Result := '0' + Text + '0000';
               2:
               if ( StrToIntDef( Text, 0 ) < Tope ) then
                  Result := Text + '0000'
               else
                   Result := '00' + Text + '00';
               3: Result := '0' + Text + '00';
               4: Result := Text + '00';
               5: Result := Text + '0';
               6: Result := Text;
          else
              Result := '';
          end;
     end
     else
     begin
          case GetTextLen of
               1: Result := '0' + Text + '00';
               2:
               if ( StrToIntDef( Text, 0 ) < Tope ) then
                  Result := Text + '00'
               else
                   Result := '00' + Text;
               3: Result := '0' + Text;
               4: Result := Text;
          else
              Result := '';
          end;
     end;
end;

function TZetaHora.GetTimeValue: Boolean;
var
   sValor: String;
begin
     if Modified then
     begin
          sValor := GetValorAsText;
          Result := ValidTime( sValor );
          if Result then
             Valor := sValor
          else
          begin
               DisplayText( sValor );
               HayError;
          end;
     end
     else
         Result := True;
end;

function TZetaHora.GetHourValue: TDateTime;
begin
     Result := EncodeTime( Horas, Minutos, Segundos, 0 );
end;

procedure TZetaHora.ChangeValue( const Value: String );
begin
     FValor := Value;
     if FCheck then
     begin
          DoOnCheckTime;
     end;
end;

procedure TZetaHora.SetUseSeconds(const Value: Boolean);
begin
     if ( Value <> FUseSeconds ) then
     begin
          FUseSeconds := Value;
          InitEditMask;
     end;
end;

procedure TZetaHora.DisplayTime;
begin
     DisplayText( Valor );
     if Focused then
        SelectAll;
end;

procedure TZetaHora.DisplayText( const Value: String );
begin
     Text := Value;
end;

procedure TZetaHora.DoEnter;
begin
     inherited DoEnter;
end;

function TZetaHora.ValidEdit: Boolean;
begin
     Result := True;
end;

procedure TZetaHora.DoEdit;
begin
     if ValidEdit then
     begin
          if not Focused then
             Enfocar;
     end;
end;

procedure TZetaHora.KeyDown( var Key: Word; Shift: TShiftState);
begin
     case Key of
          VK_DELETE:
          begin
               if ( Shift = [] ) then
                  DoEdit;
          end;
          VK_RETURN:
          begin
               if UseEnterKey then
               begin
                    Key := 0;
                    if not GetTimeValue then
                       HayError;
               end;
          end;
     end;
     inherited KeyDown( Key, Shift );
end;

procedure TZetaHora.KeyPress( var Key: Char );
begin
     case Key of
          #48..#57: DoEdit;
          Chr( VK_BACK ): DoEdit;
     end;
     inherited KeyPress( Key );
end;

procedure TZetaHora.WMCopy( var Message: TMessage );
begin
     CopyToClipboard;
end;

procedure TZetaHora.WMPaste( var Message: TMessage );
begin
     PasteFromClipboard;
end;

procedure TZetaHora.WMCut( var Message: TMessage );
begin
     CutToClipboard;
end;

{
function TZetaHora.GetTextMargins: TPoint;
var
   DC: HDC;
   SaveFont: HFont;
   I: Integer;
   SysMetrics, Metrics: TTextMetric;
begin
     if NewStyleControls then
     begin
          if ( BorderStyle = bsNone ) then
             I := 0
          else
              if Ctl3D then
                 I := 1
              else
                  I := 2;
          Result.X := SendMessage( Handle, EM_GETMARGINS, 0, 0 ) and $0000FFFF + I;
          Result.Y := I;
     end
     else
     begin
          if ( BorderStyle = bsNone ) then
             I := 0
          else
          begin
               DC := GetDC( 0 );
               GetTextMetrics( DC, SysMetrics );
               SaveFont := SelectObject( DC, Font.Handle );
               GetTextMetrics( DC, Metrics );
               SelectObject( DC, SaveFont );
               ReleaseDC( 0, DC );
               I := SysMetrics.tmHeight;
               if ( I > Metrics.tmHeight ) then
                  I := Metrics.tmHeight;
               I := I div 4;
          end;
          Result.X := I;
          Result.Y := I;
     end;
end;

procedure TZetaHora.WMPaint( var Message: TWMPaint );
var
   Area: TRect;
   DC: HDC;
   PS: TPaintStruct;
begin
     if ( FCanvas = nil ) then
     begin
          FCanvas := TControlCanvas.Create;
          FCanvas.Control := Self;
     end;
     DC := Message.DC;
     if ( DC = 0 ) then
        DC := BeginPaint( Handle, PS );
     FCanvas.Handle := DC;
     try
        FCanvas.Font := Font;
        with FCanvas do
        begin
             Area := ClientRect;
             if not ( NewStyleControls and Ctl3D ) and ( BorderStyle = bsSingle ) then
             begin
                  Brush.Color := clWindowFrame;
                  FrameRect( Area );
                  InflateRect( Area, -1, -1 );
             end;
             Brush.Color := Color;
             with GetTextMargins do
                  TextRect( Area, X, Y, FormatMaskText( EditMask, Valor ) );
        end;
     finally
            FCanvas.Handle := 0;
            if ( Message.DC = 0 ) then
               EndPaint( Handle, PS );
     end;
end;
}

procedure TZetaHora.CMExit( var Message: TCMExit );
begin
     if GetTimeValue then
     begin
          DisplayTime;
          inherited;
     end
     else
         HayError;
end;

procedure TZetaHora.HayError;
begin
     MessageBeep( MB_ICONEXCLAMATION );
     Enfocar;
end;

procedure TZetaHora.Enfocar;
begin
     if CanFocus then
     begin
          SetFocus;
          SelectAll;
     end;
end;

procedure TZetaHora.CopyToClipboard;
begin
     if Focused then
        Clipboard.AsText := GetValorAsText
     else
         Clipboard.AsText := Valor;
end;

procedure TZetaHora.CutToClipboard;
begin
     if not ReadOnly and ValidEdit then
     begin
          CopyToClipboard;
          Valor := '';
     end;
end;

procedure TZetaHora.PasteFromClipboard;
var
   sValue: String;
begin
     with Clipboard do
     begin
          if not ReadOnly and HasFormat( CF_TEXT ) and ValidEdit then
          begin
               sValue := AsText;
               if ValidTime( sValue ) then
               begin
                    Valor := sValue;
               end;
          end;
     end;
end;

{ ************ TZetaDBHora ************** }

constructor TZetaDBHora.Create( AOwner : TComponent );
begin
     inherited Create( AOwner );
     FDataLink := TFieldDataLink.Create;
     with FDataLink do
     begin
          Control := Self;
          OnDataChange := DataChange;
          OnEditingChange := EditingChange;
          OnUpdateData := UpdateData;
          OnActiveChange := ActiveChange;
     end;
     if not ( csDesigning in ComponentState ) then
        CheckFieldType( DataField );
end;

destructor TZetaDBHora.Destroy;
begin
     with FDataLink do
     begin
          OnActiveChange := nil;
          OnUpdateData := nil;
          OnEditingChange := nil;
          OnDataChange := nil;
          Control := nil;
          Free;
     end;
     FDataLink := nil;
     inherited Destroy;
end;

procedure TZetaDBHora.Notification( AComponent: TComponent; Operation: TOperation );
begin
     inherited Notification( AComponent, Operation );
     if ( Operation = opRemove ) then
        if ( FDataLink <> nil ) and ( AComponent = DataSource ) then
           DataSource := nil;
end;

procedure TZetaDBHora.Loaded;
begin
     inherited Loaded;
     if ( csDesigning in ComponentState ) then DataChange( Self );
end;

function TZetaDBHora.GetValor: String;
begin
     if ( csPaintCopy in ControlState ) then
        Result := ReadValor
     else
         Result := inherited GetValor;
end;

function TZetaDBHora.GetDataField: String;
begin
     Result := FDataLink.FieldName;
end;

function TZetaDBHora.GetField: TField;
begin
     Result := FDataLink.Field;
end;

function TZetaDBHora.GetDataSource: TDataSource;
begin
     Result := FDataLink.DataSource;
end;

procedure TZetaDBHora.SetDataField( const Value: String );
begin
     CheckFieldType( Value );
     FDataLink.FieldName := Trim( Value );
end;

procedure TZetaDBHora.SetDataSource( Value: TDataSource );
begin
     if ( FDataLink.DataSource <> Value ) then
     begin
          FDataLink.DataSource := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

function TZetaDBHora.GetReadOnly: Boolean;
begin
     Result := FDataLink.ReadOnly;
end;

procedure TZetaDBHora.SetReadOnly( const Value: Boolean );
begin
     FDataLink.ReadOnly := Value;
end;

procedure TZetaDBHora.CMGetDataLink( var Message: TMessage );
begin
     Message.Result := Integer( FDataLink );
end;

procedure TZetaDBHora.CheckFieldType( const Value: String );
begin
     if ( Value <> '' ) and
        ( FDataLink <> nil ) and
        ( FDataLink.Dataset <> nil ) and
        ( FDataLink.Dataset.Active ) then
     begin
          with FDataLink.Dataset.FieldByName( Value ) do
               if not ( DataType in [ ftString ] ) then
                  raise EInvalidFieldType.Create( 'TZetaDBHora.DataField can only ' +
                                                  'be connected to columns of type ' +
                                                  'String' );
     end;
end;

function TZetaDBHora.ReadValor: String;
const
     NULL_TIME = '    ';
begin
     with FDataLink do
     begin
          if ( Field = nil ) then
             Result := NULL_TIME
          else
              if TStringField( Field ).IsNull then
                 Result := NULL_TIME
              else
                  Result := TStringField( Field ).AsString;
     end;
end;

procedure TZetaDBHora.DataChange( Sender: TObject );
var
   lCheck: Boolean;
begin
     lCheck := FCheck;
     FCheck := False;
     try
        Valor := ReadValor;
     finally
            FCheck := lCheck;
     end;
end;

procedure TZetaDBHora.EditingChange(Sender: TObject);
begin
end;

procedure TZetaDBHora.UpdateData(Sender: TObject);
begin
     if ( FDataLink.Field <> nil ) then
     begin
          with TStringField( FDataLink.Field ) do
          begin
               if ( AsString <> Valor ) then
                  AsString := Valor;
          end;
     end;
end;

procedure TZetaDBHora.ActiveChange( Sender: TObject );
begin
     if ( FDataLink <> nil ) and FDataLink.Active then
        CheckFieldType( DataField );
end;

procedure TZetaDBHora.Change;
begin
     if ( FDataLink <> nil ) then
        FDataLink.Modified;
     inherited Change;
end;

function TZetaDBHora.ValidEdit;
begin
     if ( FDataLink <> nil ) then
     begin
          if FDataLink.Editing then
             Result := True
          else
              Result := FDataLink.Edit;
     end
     else
         Result := True;
     if Result then
        Result := inherited ValidEdit;
end;

procedure TZetaDBHora.ChangeValue( const Value: String );
begin
     inherited ChangeValue( Value );
     if ( FDataLink <> nil ) and FDataLink.Editing then
     begin
          try
             with FDataLink do
             begin
                  UpdateRecord;
             end;
          except
                on Error: Exception do
                   Application.HandleException( Error );
          end;
     end;
end;

end.

