unit ZetaKeyLookup;

interface

{$R *.RES }

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, ExtCtrls, Buttons, DB, DBCtrls, Clipbrd,
     ZetaClientDataset,
     ZetaDBTextBox;

type
  TZetaKeyLookup = class;
  TZetaLlave = class( TEdit )
  private
    { Private declarations }
    function GetKeyLookup: TZetaKeyLookup;
    procedure CNKeyDown(var Message: TWMKeyDown); message CN_KEYDOWN; // De TWinControl.CNKeyDown //
    procedure WMCopy( var Message: TMessage ); message WM_COPY;
    procedure WMCut( var Message: TMessage ); message WM_CUT;
    procedure WMPaste( var Message: TMessage ); message WM_PASTE;
  protected
    { Protected declarations }
    procedure KeyDown( var Key: Word; Shift: TShiftState); override;
    procedure KeyPress( var Key: Char ); override;
  public
    { Public declarations }
    property KeyLookup: TZetaKeyLookup read GetKeyLookup;
  published
    { Published declarations }
  end;
  TZetaKeyLookup = class( TCustomPanel )
  private
    { Private declarations }
    FLlave: TZetaLlave;
    FOldKey: String;
    FOldDescription: String;
    FSetting: Boolean;
    FChanged: Boolean;
    FDescripcion: TZetaTextBox;
    FBuscar: TSpeedButton;
    FLookupDataset: TZetaLookupDataset;
    FFiltro: String;
    FOpcional: Boolean;
    FReadOnly: Boolean;
    FEditarSoloActivos: Boolean;
    FIgnorarConfidencialidad : Boolean;
    FWidthLlave: Integer;
    FOnValidKey: TNotifyEvent;
    FOnValidLookup: TNotifyEvent;
    function DoLookup( const lSetFocus: Boolean ): Boolean;
    function GetLlave: String;
    function GetLlaveValor: String;
    function GetDescripcion: String;
    function GetEditTabStop: Boolean;
    function GetValor: Integer;
    function HayLookupDataset: Boolean;
    procedure SetDescripcion( const Value: String );
    procedure SetEditTabStop(const Value: Boolean);
    procedure SetFiltro( const Value: String );
    procedure SetLlave( const Value: String );
    procedure SetLlaveLookUp( const Value: String; const lLookUp : Boolean);
    procedure SetLookUpDataSet( Value: TZetaLookupDataset );
    procedure SetValor( const Value: Integer );
    procedure SetWidthLlave( const Value: Integer );
    procedure DoSearch( Sender: TObject );
    procedure DoValidKey;
    procedure DoValidLookup;
    procedure WMSize( var Msg: TWMSize ); message WM_SIZE;
    procedure WMSetFocus(var Message: TWMSetFocus); message WM_SETFOCUS;
    procedure CMEnabledChanged(var Message: TMessage); message CM_ENABLEDCHANGED;
    procedure CMFontChanged( var Msg: TMessage ); message CM_FONTCHANGED;
    procedure PositionControls;
  protected
    { Protected declarations }
    FKeySizeSet: Boolean;
    function VerifyData: Boolean; virtual;
    function ValidEdit: Boolean; virtual;
    procedure DoChange( Sender: TObject ); virtual;
    procedure DoEnter; override;
    procedure DoEdit;
    procedure DoExit; override;
    procedure Enfocar;
    procedure Notification( AComponent: TComponent; Operation: TOperation ); override;
    procedure SetKeySize( const iValue: Integer );
    procedure SetTextValue( const Value: String );
    procedure SetReadOnly( const Value: Boolean ); virtual;
  public
    { Public declarations }
    property Descripcion: String read GetDescripcion;
    property Llave: String read GetLlave write SetLlave;
    property Valor: Integer read GetValor write SetValor;
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    procedure CopyToClipboard;
    procedure CutToClipboard;
    procedure PasteFromClipboard;
    procedure ResetMemory;
    procedure Resize; override;
    procedure SetLlaveDescripcion( const sLlave, sDescripcion: String );
  published
    { Published declarations }
    property Align;
    property Enabled;
    property Filtro: String read FFiltro write SetFiltro;
    property LookupDataset: TZetaLookupDataset read FLookupDataset write SetLookupDataset;
    property Opcional: Boolean read FOpcional write FOpcional default True;
    property ReadOnly: Boolean read FReadOnly write SetReadOnly default False;
    property EditarSoloActivos: Boolean read FEditarSoloActivos write FEditarSoloActivos default True;
    property IgnorarConfidencialidad: Boolean read FIgnorarConfidencialidad write FIgnorarConfidencialidad default True;
    property ShowHint;
    property TabOrder;
    property TabStop: Boolean read GetEditTabStop write SetEditTabStop;
    property Visible;
    property WidthLlave: Integer read FWidthLlave write SetWidthLlave;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
    property OnStartDrag;
    property OnValidKey: TNotifyEvent read FOnValidKey write FOnValidKey;
    property OnValidLookup: TNotifyEvent read FOnValidLookup write FOnValidLookup;
  end;
  TZetaDBKeyLookup = class( TZetaKeyLookup )
  private
    { Private declarations }
    FDataLink: TFieldDataLink;
    function GetDataField: String;
    function GetDataSource: TDataSource;
    function GetField: TField;
    procedure SetDataField( Const Value: string );
    procedure SetDataSource( Value: TDataSource );
    procedure CheckFieldType( const Value: String );
    procedure DataChange( Sender: TObject );
    procedure EditingChange( Sender: TObject );
    procedure UpdateData( Sender: TObject );
    procedure ActiveChange( Sender: TObject );
  protected
    { Protected declarations }
    function VerifyData: Boolean; override;
    function ValidEdit: Boolean; override;
    procedure DoChange( Sender: TObject ); override;
    procedure Notification( AComponent: TComponent; Operation: TOperation ); override;
    procedure SetReadOnly( const Value: Boolean ); override;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    property Field: TField read GetField;
  published
    { Published declarations }
    property DataField: string read GetDataField write SetDataField;
    property DataSource: TDataSource read GetDataSource write SetDataSource;
  end;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses;

const
     K_LLAVE_VACIA = '';
     K_LLAVE_CERO = '0';

{ ************ TZetaLlave ************** }

function TZetaLlave.GetKeyLookup: TZetaKeyLookup;
begin
     Result := TZetaKeyLookup( Parent );
end;

procedure TZetaLlave.WMCopy( var Message: TMessage );
begin
     KeyLookup.CopyToClipboard;
end;

procedure TZetaLlave.WMPaste( var Message: TMessage );
begin
     KeyLookup.PasteFromClipboard;
end;

procedure TZetaLlave.WMCut( var Message: TMessage );
begin
     KeyLookup.CutToClipboard;
end;

procedure TZetaLlave.CNKeyDown(var Message: TWMKeyDown);
begin
     with Message do
     begin
          if ( Charcode = VK3_F ) and ( ssCtrl in KeyDataToShiftState( KeyData ) ) then
          begin
               Result := 1;
               KeyLookup.DoSearch( KeyLookup );
          end
          else
              inherited;
     end;
end;

procedure TZetaLlave.KeyDown( var Key: Word; Shift: TShiftState );
begin
     case Key of
          VK_DELETE:
          begin
               if ( Shift = [] ) then
                  KeyLookup.DoEdit;
          end;
     end;
     inherited KeyDown( Key, Shift );
end;

procedure TZetaLlave.KeyPress( var Key: Char );
begin
     case Key of
          #32..#255: KeyLookup.DoEdit;
          Chr( VK_BACK ): KeyLookup.DoEdit;
     end;
     inherited KeyPress( Key );
end;

{ ************ TZetaKeyLookup ************** }

constructor TZetaKeyLookup.Create( AOwner: TComponent );
begin
     inherited Create( AOwner );
     FSetting := False;
     FChanged := False;
     FOpcional := True;
     FOldKey := VACIO;
     FOldDescription := VACIO;
     BorderStyle := bsNone;
     BevelInner := bvNone;
     BevelOuter := bvNone;
     Caption := '';
     ControlStyle := ControlStyle - [ csAcceptsControls, csSetCaption ];
     FLlave := TZetaLlave.Create( Self );
     with FLlave do
     begin
          Parent := Self;
          Name := Self.Name + 'FLlave';
          Text := '';
          CharCase := ecUpperCase;
          OnChange := DoChange;
     end;
     FDescripcion := TZetaTextBox.Create( Self );
     with FDescripcion do
     begin
          Parent := Self;
          Name := Self.Name + 'FDescripcion';
          Color := clInfoBk;
          Caption := '';
     end;
     FBuscar := TSpeedButton.Create( Self );
     with FBuscar do
     begin
          Parent := Self;
          Name := Self.Name + 'FBuscar';
          Glyph.LoadFromResourceName( HInstance, 'ZOOMIN' );
          Visible := True;
          ShowHint := True;
          OnClick := DoSearch;
     end;
     FWidthLlave := 60;
     FKeySizeSet := False;
     Height := 21;
     Width := 300;
end;

destructor TZetaKeyLookup.Destroy;
begin
     inherited Destroy;
end;

procedure TZetaKeyLookup.ResetMemory;
begin
     FOldKey := VACIO;
end;

procedure TZetaKeyLookup.Notification( AComponent: TComponent; Operation: TOperation );
begin
     inherited Notification( AComponent, Operation );
     if ( Operation = opRemove ) then
     begin
          if ( LookupDataset <> nil ) and ( AComponent = LookupDataset ) then
             LookupDataset := nil;
     end;
end;

function TZetaKeyLookup.GetDescripcion: String;
begin
     Result := FDescripcion.Caption;
end;

function TZetaKeyLookup.GetLlaveValor: String;
begin
     Result := FLlave.Text;
     if not ZetaCommonTools.StrLleno( Result ) then
        Result := VACIO;
end;

function TZetaKeyLookup.GetLlave: String;
begin
     if FChanged then
        Result := ''
     else
         Result := GetLlaveValor;
end;

procedure TZetaKeyLookup.SetLookUpDataSet( Value: TZetaLookupDataset );
begin
     if ( FLookupDataset <> Value ) then
     begin
          FLookupDataset := Value;
          if ( Value = nil ) then
             FBuscar.Hint := Hint
          else
          begin
               with FLookupDataset do
               begin
                    FreeNotification( Self );
                    FBuscar.Hint := 'Buscar ' + LookupName + ' ( Ctrl-F )'
               end;
          end;
     end;
end;

function TZetaKeyLookup.GetValor: Integer;
begin
     if ( Llave = VACIO ) then
        Result := 0
     else
         Result := StrAsInteger( Llave );
end;

function TZetaKeyLookup.GetEditTabStop: Boolean;
begin
     Result := FLlave.TabStop;
end;

procedure TZetaKeyLookup.SetEditTabStop(const Value: Boolean);
begin
     FLlave.TabStop := Value;
end;

procedure TZetaKeyLookup.SetWidthLlave( const Value: Integer );
begin
     if ( FWidthLlave <> Value ) then
     begin
          FWidthLlave := Value;
          Resize;
     end;
end;

procedure TZetaKeyLookup.SetDescripcion( const Value: String );
begin
     if ( Value <> FDescripcion.Caption ) then
        FDescripcion.Caption := Value;
end;

procedure TZetaKeyLookup.SetTextValue( const Value: String );
begin
     FSetting := True;
     try
        FLlave.Text := Value;
     finally
            FSetting := False;
     end;
end;

procedure TZetaKeyLookup.SetLlaveDescripcion( const sLlave, sDescripcion: String );
begin
     SetTextValue( sLlave );
     FOldKey := sLlave;
     FOldDescription := sDescripcion;
     SetDescripcion( sDescripcion );
end;

procedure TZetaKeyLookup.SetLlave( const Value: String );
begin
     SetTextValue( Value );
     DoLookup( False );
end;


procedure TZetaKeyLookup.SetLlaveLookUp( const Value: String ; const lLookUp : Boolean);
begin
     SetTextValue( Value );
     DoLookup( lLookUp );
end;

procedure TZetaKeyLookup.SetFiltro( const Value: String );
begin
     if ( FFiltro <> Value ) then
     begin
          FFiltro := Value;
          DoLookup( False );
     end;
end;

procedure TZetaKeyLookup.SetReadOnly( const Value: Boolean );
begin
     if ( FReadOnly <> Value ) then
     begin
          FReadOnly := Value;
          FLlave.ReadOnly := Value;
     end;
end;

procedure TZetaKeyLookup.SetValor( const Value: Integer );
begin
     if ( Value <= 0 ) then
        SetLlave( K_LLAVE_VACIA )
     else
         SetLlave( IntToStr( Value ) );
end;

procedure TZetaKeyLookup.SetKeySize( const iValue: Integer );
begin
     if not FKeySizeSet then
     begin
          FLlave.MaxLength := iValue;
          FKeySizeSet := True;
     end;
end;

function TZetaKeyLookup.VerifyData: Boolean;
begin
     Result := DoLookup( True );
end;

function TZetaKeyLookup.HayLookupDataset: Boolean;
begin
     Result := ( FLookupDataset <> nil );
end;

procedure TZetaKeyLookup.DoSearch( Sender: TObject );
var
   sLlave, sDescripcion: String;
begin
     sLlave := Llave;
     sDescripcion := '';

     if (FLlave.CanFocus) and ( not FLlave.Focused ) then
        FLlave.SetFocus;

     if Enabled and not FReadOnly and HayLookupDataset and FLlave.Focused and
        LookupDataset.Search( FFiltro, sLlave, sDescripcion, TRUE, not IgnorarConfidencialidad ) then
     begin
          DoEdit;
          SetTextValue( sLlave );
          SetDescripcion( sDescripcion );
          if FLlave.Focused then
             GetParentForm( Self ).Perform( WM_NEXTDLGCTL, 0, 0 ) // Para Salir Del Control //
          else
              DoValidKey;
     end;
end;

procedure TZetaKeyLookup.DoEnter;
begin
     if HayLookupDataset then
        SetKeySize( LookupDataset.LookupKeyFieldSize );
     Enfocar;
end;

procedure TZetaKeyLookup.DoEdit;
begin
     if ValidEdit then
     begin
          if not FLlave.Focused then
             Enfocar;
     end;
end;

function TZetaKeyLookup.ValidEdit: Boolean;
begin
     Result := True;
end;

procedure TZetaKeyLookup.DoExit;
begin
     if VerifyData then
     begin
          inherited DoExit;
          DoValidKey;
     end;
end;

procedure TZetaKeyLookup.Enfocar;
begin
     with FLlave do
     begin
          if CanFocus then
          begin
               SelectAll;
               SetFocus;
          end;
     end;
end;

procedure TZetaKeyLookup.DoChange( Sender: TObject );
begin
     if not FSetting then
        FChanged := True;
end;

procedure TZetaKeyLookup.DoValidKey;
begin
     if Assigned( FOnValidKey ) then
        FOnValidKey( Self );
end;

procedure TZetaKeyLookup.DoValidLookup;
begin
     if Assigned( FOnValidLookup ) then
        FOnValidLookup( Self );
end;

function TZetaKeyLookup.DoLookup( const lSetFocus: Boolean ): Boolean;
var
   sLlave, sDescripcion: String;
   lKeyIsActive, lKeyIsConfidential: boolean;
begin
     Result := True;
     sLlave := GetLlaveValor;
     if ( sLlave = K_LLAVE_VACIA ) then
     begin
          SetDescripcion( '' );
          FChanged := False;
     end
     else
         if ( sLlave = FOldKey ) then
         begin
              SetDescripcion( FOldDescription );
              FChanged := False;
         end
         else
             if HayLookupDataset then
             begin
                  Result := LookupDataset.LookupKey( sLlave, Filtro, sDescripcion, lKeyIsActive, lKeyIsConfidential);
                  if Result then
                  begin
                       if not EditarSoloActivos then
                          lKeyIsActive := True;

                       if IgnorarConfidencialidad then
                          lKeyIsConfidential := False;

                       if ( lKeyIsActive and (not lKeyIsConfidential) ) or ( not lSetFocus ) then
                       begin
                          SetDescripcion( sDescripcion );
                          DoValidLookup;
                          FOldKey := sLlave;
                          FOldDescription := sDescripcion;
                          FChanged := False;
                       end
                       else
                       begin
                            Result := False;

                            if lKeyIsConfidential then
                               SetDescripcion( '???')
                            else
                               SetDescripcion( 'C�digo Inactivo!!!');

                            if lSetFocus then
                            begin
                                 MessageBeep( MB_ICONEXCLAMATION );
                                 Enfocar;
                            end;
                       end;
                  end
                  else
                  begin
                       SetDescripcion( '???' );
                       if lSetFocus then
                       begin
                            MessageBeep( MB_ICONEXCLAMATION );
                            Enfocar;
                       end;
                  end;
             end;
end;

procedure TZetaKeyLookup.CMEnabledChanged(var Message: TMessage);
begin
     inherited;
     FLlave.Enabled := Self.Enabled;
     FDescripcion.Enabled := Self.Enabled;
     FBuscar.Enabled := Self.Enabled;
end;

procedure TZetaKeyLookup.CMFontChanged( var Msg: TMessage );
begin
     PositionControls;
     inherited;
end;

procedure TZetaKeyLookup.WMSize( var Msg: TWMSize );
begin
     inherited;
     Resize;
end;

procedure TZetaKeyLookup.WMSetFocus(var Message: TWMSetFocus);
begin
     inherited;
     Enfocar;
end;

procedure TZetaKeyLookup.Resize;
begin
     PositionControls;
     inherited Resize;
end;

procedure TZetaKeyLookup.PositionControls;
var
   iWidth, iLeft: Integer;
begin
     iWidth := ( Self.Width - FWidthLlave - Self.Height - 6 );
     if ( iWidth < 0 ) then
        iWidth := 0;
     iLeft := 0;
     FLlave.SetBounds( iLeft, 0, FWidthLlave, Self.Height );
     iLeft := iLeft + ( FWidthLlave + 2 );
     FDescripcion.SetBounds( iLeft, 0, iWidth, Self.Height );
     iLeft := iLeft + ( iWidth + 2 );
     FBuscar.SetBounds( iLeft, 0, Self.Height, Self.Height );
end;

procedure TZetaKeyLookup.CopyToClipboard;
begin
     Clipboard.AsText := Llave;
end;

procedure TZetaKeyLookup.CutToClipboard;
begin
     if not ReadOnly and ValidEdit then
     begin
          CopyToClipboard;
          Llave := '';
     end;
end;

procedure TZetaKeyLookup.PasteFromClipboard;
begin
     with Clipboard do
     begin
          if not ReadOnly and HasFormat( CF_TEXT ) then
          begin
               if HayLookupDataset and ValidEdit then
                  SetLlaveLookUp( AsText, True); {AV 19/11/2010: Cambio para enfocar y revisar activo al realizar paste}
          end;
     end;
end;

{ ************ TZetaDBKeyLookup ************** }

constructor TZetaDBKeyLookup.Create( AOwner: TComponent );
begin
     inherited Create( AOwner );
     FDataLink := TFieldDataLink.Create;
     with FDataLink do
     begin
          Control := Self;
          OnDataChange := DataChange;
          OnEditingChange := EditingChange;
          OnUpdateData := UpdateData;
          OnActiveChange := ActiveChange;
     end;
     if not ( csDesigning in ComponentState ) then
        CheckFieldType( DataField );
end;

destructor TZetaDBKeyLookup.Destroy;
begin
     with FDataLink do
     begin
          OnActiveChange := nil;
          OnUpdateData := nil;
          OnEditingChange := nil;
          OnDataChange := nil;
          Control := nil;
          Free;
     end;
     FDataLink := nil;
     inherited Destroy;
end;

procedure TZetaDBKeyLookup.Notification( AComponent: TComponent; Operation: TOperation );
begin
     inherited Notification( AComponent, Operation );
     if ( Operation = opRemove ) then
        if ( FDataLink <> nil ) and ( AComponent = DataSource ) then
           DataSource := nil;
end;

function TZetaDBKeyLookup.GetDataField: String;
begin
     Result := FDataLink.FieldName;
end;

function TZetaDBKeyLookup.GetField: TField;
begin
     Result := FDataLink.Field;
end;

function TZetaDBKeyLookup.GetDataSource: TDataSource;
begin
     Result := FDataLink.DataSource;
end;

procedure TZetaDBKeyLookup.SetDataField( Const Value: string );
begin
     CheckFieldType( Value );
     FDataLink.FieldName := Trim( Value );
end;

procedure TZetaDBKeyLookup.SetDataSource(Value: TDataSource);
begin
     if ( FDataLink.DataSource <> Value ) then
     begin
          FDataLink.DataSource := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

procedure TZetaDBKeyLookup.SetReadOnly( const Value: Boolean );
begin
     inherited SetReadOnly( Value );
     FDataLink.ReadOnly := Value;
end;

procedure TZetaDBKeyLookup.CheckFieldType( const Value: String );
begin
     if ( Value <> '' ) and
        ( FDataLink <> nil ) and
        ( FDataLink.Dataset <> nil ) and
        ( FDataLink.Dataset.Active ) then
     begin
          with FDataLink.Dataset.FieldByName( Value ) do
          begin
               if not ( DataType in [ ftInteger, ftSmallInt, ftWord, ftString ] ) then
                  raise EInvalidFieldType.Create( 'TZetaDBKeyLookup.DataField can only ' +
                                                  'be connected to columns of type ' +
                                                  'Integer, SmallInt, Word, String' );
               if ( Datatype = ftString ) then
                  SetKeySize( Size );
          end;
     end;
end;

procedure TZetaDBKeyLookup.DataChange( Sender: TObject );
begin
     with FDataLink do
     begin
          if ( Field = nil ) then
             SetLlave( K_LLAVE_VACIA )
          else
          begin
               with Field do
               begin
                    if IsNull then
                       SetLlave( K_LLAVE_VACIA )
                    else
                        case DataType of
                             ftString: SetLlave( Trim( AsString ) ); { ADO pone espacios al final }
                             ftSmallint, ftInteger, ftWord: SetValor( AsInteger );
                        else
                            SetLlave( K_LLAVE_VACIA );
                        end;
               end;
          end;
     end;
end;

procedure TZetaDBKeyLookup.EditingChange(Sender: TObject);
begin
end;

procedure TZetaDBKeyLookup.UpdateData(Sender: TObject);
begin
     with FDataLink.Field do
     begin
          case DataType of
               ftString: AsString := Llave;
               ftSmallint, ftInteger, ftWord: AsInteger := Valor;
          end;
     end;
end;

procedure TZetaDBKeyLookup.ActiveChange( Sender: TObject );
begin
     if ( FDataLink <> nil ) and FDataLink.Active then
        CheckFieldType( DataField );
end;

procedure TZetaDBKeyLookup.DoChange( Sender: TObject );
begin
     inherited DoChange( Sender );
     if ( FDataLink <> nil ) and FDataLink.Editing then
        FDataLink.Modified;
end;

function TZetaDBKeyLookup.ValidEdit: Boolean;
begin
     if ( FDataLink <> nil ) then
     begin
          if FDataLink.Editing then
             Result := True
          else
              Result := FDataLink.Edit;
     end
     else
         Result := True;
     if Result then
        Result := inherited ValidEdit;
end;

function TZetaDBKeyLookup.VerifyData: Boolean;
begin
     if FDataLink.Editing then
     begin
          Result := False;
          if inherited VerifyData then
          begin
               try
                  with FDataLink do
                  begin
                       Modified;
                       UpdateRecord;
                  end;
                  Result := True;
               except
                     Enfocar;
                     raise;
               end;
          end;
     end
     else
         Result := True;
end;

end.
