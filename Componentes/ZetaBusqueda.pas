unit ZetaBusqueda;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Db, Grids, DBGrids,
     ZetaClientDataset,
     ZetaDBGrid,
     ZetaMessages, DBCtrls;

type
  TBusqueda = class(TForm)
    PanelBotones: TPanel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    PanelSuperior: TPanel;
    PistaLBL: TLabel;
    Pista: TEdit;
    Filtrar: TBitBtn;
    Modificar: TBitBtn;
    DBGrid: TZetaDBGrid;
    Datasource: TDataSource;
    Refrescar: TSpeedButton;
    MostrarActivos: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure PistaChange(Sender: TObject);
    procedure PistaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure PistaKeyPress(Sender: TObject; var Key: Char);
    procedure ModificarClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FiltrarClick(Sender: TObject);
    procedure RefrescarClick(Sender: TObject);
  private
    { Private declarations }
    FDataset: TZetaLookupDataset;
    FFiltro: String;
    FCodigo: String;
    FShowModificar: Boolean;
    FFiltroActivos: Boolean;
    FFiltroConfidencialidad: Boolean;

    function Llave: String;
    procedure Connect;
    procedure Disconnect;
    procedure RemoveFilter;
    procedure SetControls;
    procedure SetDataset( Value: TZetaLookupDataset );
    procedure SetFilter;
    procedure SetFiltro( const Value: String );
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;

  protected
    { Protected declarations }
{$ifdef FALSE}
    procedure KeyPress( var Key: Char ); override; { TWinControl }
{$endif}
    procedure OnFiltrarConfidencialidad( DataSet: TDataSet;
          var Accept: Boolean);
  public
    { Public declarations }
    property Codigo: String read FCodigo write FCodigo;
    property Dataset: TZetaLookupDataset read FDataset write SetDataset;
    property Filtro: String read FFiltro write SetFiltro;
  end;

var
  Busqueda: TBusqueda;

function ShowSearchForm( LookupDataset: TZetaLookupDataset; const sFilter: String; var sKey, sDescription: String; const lShowModificar: Boolean = TRUE; const SoloActivos: Boolean = TRUE;  const lUsarConfidencialidad : Boolean = TRUE): Boolean;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses;

{$R *.DFM}

function ShowSearchForm( LookupDataset: TZetaLookupDataset; const sFilter: String; var sKey, sDescription: String; const lShowModificar: Boolean; const SoloActivos: Boolean; const lUsarConfidencialidad : Boolean): Boolean;
var
   oBusqueda: TBusqueda;
begin
//   if ( Busqueda = nil ) then
//   Se agreg� un objeto TBusqueda para manejar multiples instancias
//   de la forma de busqueda

     oBusqueda := TBusqueda.Create( Application );
     try
        with oBusqueda do
        begin
             Dataset := LookupDataset;
             Filtro := sFilter;
             Codigo := sKey;
             FShowModificar:= lShowModificar;
             FFiltroActivos:= SoloActivos;
             FFiltroConfidencialidad := lUsarConfidencialidad;
             ShowModal;
             Result := ( ModalResult = mrOk );
             if Result then
             begin
                  sKey := Codigo;
                  sDescription := LookupDataset.GetDescription;
             end;
        end;
     finally
            oBusqueda.Free;
     end;
end;

{ *********** TBusqueda ************ }

procedure TBusqueda.FormCreate(Sender: TObject);
begin
     HelpContext:= H00015_busqueda_catalogos;
end;

procedure TBusqueda.FormShow(Sender: TObject);
var
   sDescription: String;
begin
     inherited;
     with Dataset do
     begin
          if strLleno( Codigo ) then
             LookupKey( Codigo, Filtro, sDescription );
          Caption := 'B�squeda de ' + LookupName;
          Self.Refrescar.Hint := 'Refrescar ' + LookupName;
          with DBGrid do
          begin
               with Columns[ 0 ] do
               begin
                    FieldName := LookupKeyField;
                    if ( Width < 64 ) then
                       Width := 64;
               end;
               Columns[ 1 ].FieldName := LookupDescriptionField;
          end;
     end;
     Pista.Clear;
     if ( FShowModificar ) then //FShowModificar indica si viene de un lookup
        ActiveControl := DBGrid
     else
         ActiveControl:= Pista;
     Connect;
end;

procedure TBusqueda.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Disconnect;
     Action := caHide;
end;

procedure TBusqueda.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     with Modificar do
     begin
          if Visible and ( Key = VK_INSERT ) and ( ssShift in Shift ) then
          begin
               Key := 0;
               Click;
          end;
     end;
end;

{$ifdef FALSE}
procedure TBusqueda.KeyPress( var Key: Char );
begin
     inherited KeyPress( Key );
     case Key of
          Chr( VK_RETURN ):
          begin
               if ( not ( ActiveControl is TButton ) ) then
               begin
                    Key := #0;
                    Perform( WM_NEXTDLGCTL, ( Hi( GetKeyState( VK_SHIFT ) ) and 1 ), 0 );
               end;
          end;
     end;
end;
{$endif}

procedure TBusqueda.SetControls;
begin
     with Modificar do
     begin
          with Dataset do
          begin
               if PuedeConsultar and FShowModificar then
               begin
                    if IsEmpty then
                    begin
                         if GetRights( K_DERECHO_ALTA ) then
                         begin
                              Caption := '&Agregar';
                              Hint := 'Agregar ' + LookupName;
                              Visible := True;
                         end
                         else
                             Visible := False;
                    end
                    else
                    begin
                         Caption := '&Modificar';
                         Hint := 'Editar ' + LookupName;
                         Visible := True;
                    end;
               end
               else
               begin
                   Visible := False;
               end;

               with MostrarActivos do
               begin
                    Visible := StrLleno( LookupActivoField ) and ( Pos( LookupActivoField, Filtro ) <= 0 ) ;
                    Checked := FFiltroActivos;
               end;
          end;

     end;
     Self.Refrescar.Visible:= FShowModificar;
end;

procedure TBusqueda.SetFiltro( const Value: String );
begin
     if strVacio( Value ) then
        FFiltro := VACIO
     else
         FFiltro := ZetaCommonTools.Parentesis( Value );
end;

procedure TBusqueda.SetDataset( Value: TZetaLookupDataset );
begin
     if ( FDataset <> Value ) then
     begin
          FDataset := Value;
     end;
end;

procedure TBusqueda.OnFiltrarConfidencialidad( DataSet: TDataSet; var Accept: Boolean);
  var sNivel0 : String;
begin
     Accept := TRUE;
     if (DataSet <> nil ) then
     begin
         if StrLleno(ZetaClientDataSet.GlobalListaConfidencialidad) and StrLleno( TZetaLookupDataSet( Dataset ).LookupConfidenField) then
         begin
              sNivel0 := DataSet.FieldByName(TZetaLookupDataSet( Dataset ).LookupConfidenField).AsString;
              Accept := ZetaCommonTools.ListaIntersectaConfidencialidad( sNivel0 , ZetaClientDataSet.GlobalListaConfidencialidad ) ;
         end;
     end;
end;

procedure TBusqueda.SetFilter;
var
   Pos : TBookMark;

   function GetFiltroActivos: String;
   begin
        with MostrarActivos do
        begin
             if Visible and Checked then
             begin
                  Result:= Parentesis( DataSet.LookupActivoField + '=' + EntreComillas( K_GLOBAL_SI ) );
             end
             else
             begin
                  Result:= VACIO;
             end;
        end;
   end;


   function GetFilterLlave : String;
   var
      iDefTipoDato : String;
      TipoCampo: TFieldType;
      sFiltro: String;

   begin
        with DataSet do
        begin
             TipoCampo := FieldByName( LookupKeyField ).DataType;
             //if ( TipoCampo in [ ftInteger, ftSmallInt, ftWord ] ) then  //OLD
             {***DevEx(@am): En realacion al Bug 5241 del proyecto de nueva imagen. Se agrega el tipo ftAutoInc pues de lo contrario
                             el flujo entraria a la negacion del If. En la cual se intentara hacer un UPPER a un dato numerico, lo cual producira
                             un mensaje de error notificando que los tipos de datos son incompatibles.

                             En la VS. 2013 no se daba esta situacion pues el provider hera compilado en D7, sin embargo para la version 2014 las DLLS
                             comenzaron a ser compioladas en XE5, en donde los tipos de datos Enteros que sean Identity, son detectados como AutoInc.***}
             if ( TipoCampo in [ ftInteger, ftSmallInt, ftWord, ftAutoInc ] ) then
             begin
                  iDefTipoDato:=IntToStr( StrToIntDef( Llave, 0 ));
                  sFiltro:= LookupKeyField + ' = ' + iDefTipoDato ;
             end
             else
             begin
                  sFiltro:= ' UPPER( ' + LookupKeyField + ' ) like ' + EntreComillas(  Llave + '%' );
             end;

             Result:= Parentesis( ConcatString( sFiltro, ' UPPER( ' + LookupDescriptionField + ' ) like ' + EntreComillas( '%' + Llave + '%' ), 'OR' ) );
        end;
   end;

begin
     with Dataset do
     begin
          DisableControls;
          try
             if strLleno( Filtro ) or strLleno( Llave ) or MostrarActivos.Checked  or FFiltroConfidencialidad    then
             begin
                  Pos:= GetBookMark;
                  Filtered := False;
                  Filter := ZetaCommonTools.ConcatFiltros ( GetFilterLlave, GetFiltroActivos );
                  Filter := ZetaCommonTools.ConcatFiltros ( Filtro, Filter );
                  DataSet.OnFilterRecord := OnFiltrarConfidencialidad;
                  Filtered := True;
                  if ( Pos <> nil ) then
                  begin
                       if BookMarkValid( Pos ) then
                          GotoBookMark( Pos );
                       FreeBookMark( Pos );
                  end;
             end
             else if Filtered then
             begin
                  Pos:= GetBookMark;
                  Filtered := False;
                  Filter := VACIO;
                  if ( Pos <> nil ) then
                  begin
                       if BookMarkValid( Pos ) then
                          GotoBookMark( Pos );
                       FreeBookMark( Pos );
                  end;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TBusqueda.RemoveFilter;
begin
     with Dataset do
     begin
          if Filtered then
          begin
               Filtered := False;
               Filter := VACIO;
          end;
     end;
end;

procedure TBusqueda.Connect;
begin
     SetFilter;
     SetControls;
     Datasource.Dataset := Dataset;
end;

procedure TBusqueda.Disconnect;
var
   bAnterior: TBookMark;
begin
     Datasource.Dataset := nil;
     with DataSet do
          if Filtered then
          begin
               bAnterior:= GetBookMark;
               RemoveFilter;
               GotoBookMark( bAnterior );
               FreeBookMark( bAnterior );
          end;
end;

procedure TBusqueda.WMExaminar(var Message: TMessage);
begin
     OK.Click;
end;

function TBusqueda.Llave: String;
begin
     Result := Pista.Text;
end;

{ ********** Eventos de Controles ******** }

procedure TBusqueda.PistaChange(Sender: TObject);
begin
     if strVacio( Pista.Text ) then
     begin
          SetFilter;
          Filtrar.Enabled := False;
     end
     else
          Filtrar.Enabled := True;
end;

procedure TBusqueda.PistaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     with DBGrid do
     begin
          case Key of
               VK_PRIOR: Perform( WM_KEYDOWN, VK_PRIOR, 0 );   // PgUp //
               VK_NEXT: Perform( WM_KEYDOWN, VK_NEXT, 0 );   // PgDn //
               VK_UP: Perform( WM_KEYDOWN, VK_UP, 0 );   //Up Arrow //
               VK_DOWN: Perform( WM_KEYDOWN, VK_DOWN, 0 );   //Down Arrow //
          end;
     end;
end;

procedure TBusqueda.PistaKeyPress(Sender: TObject; var Key: Char);
begin
     case Key of
          Chr( VK_RETURN ):
          begin
               Key := Chr( 0 );
               if Filtrar.Enabled then
                  Filtrar.Click
               else
                   OK.Click;
          end;
     end;
end;

procedure TBusqueda.FiltrarClick(Sender: TObject);
begin
     SetFilter;
end;

procedure TBusqueda.ModificarClick(Sender: TObject);
begin
     Disconnect;
     with Dataset do
     begin
          if IsEmpty then
          begin
               if GetRights( K_DERECHO_ALTA ) then
                  Agregar
               else
                   Beep;
          end
          else
              Modificar;
     end;
     Connect;
end;

procedure TBusqueda.RefrescarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        FFiltroActivos:= MostrarActivos.Checked;
        Disconnect;
        Dataset.Refrescar;
        Connect;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBusqueda.OKClick(Sender: TObject);
begin
     with Dataset do
     begin
          FCodigo := FieldByName( LookupKeyField ).AsString;
     end;
end;

end.
