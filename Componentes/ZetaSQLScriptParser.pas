{* Unidad para analizar y ejecutar un archivo de Script SQL Server.
   @author Ricardo Carrillo Morales (desarrollo original)
   @version 03/Mar/2014 v0.1 Versi�n inicial
}
unit ZetaSQLScriptParser;

interface

uses
  Classes, ADODB;

type

  {* Funci�n que se ejecutar� por cada sentencia SQL
     @param Sender Referencia al objeto TSQLScriptParser que ejecuta la funci�n
     @param Sentences Sentencias a ser ejecutadas
  }
  TExecSQLFunction = function(Sender: TObject; const Sentences: string): Boolean of object;

  {* Clase para analizar un script SQL}
  TZetaSQLScriptParser = class
    function Execute: Boolean;
    procedure LoadFromFile(FileName: string);
  private
    fText: string;
    fEliminarComentarios: Boolean;
    fScript: TStringList;
    fSentences: TStringList;
    fOnExecSQL: TExecSQLFunction;
    fIndex: Integer;
    fQuery: TADOQuery;
//    fExectResult: Boolean;
    procedure SetText(const Value: string);
  public
    constructor Create;
    destructor Destroy; override;
    property Index    : Integer          read fIndex;                      {* Sentencia actual siendo ejecutada}
    property Sentences: TStringList      read fSentences;                  {* Sentencias analizadas}
    property Text     : string           read fText      write SetText;    {* Sentencias originales}
    property EliminarComentarios     : Boolean           read fEliminarComentarios      write fEliminarComentarios;
    property OnExecSQL: TExecSQLFunction read fOnExecSQL write fOnExecSQL; {* Funci�n que se ejecutar� por cada sentencia SQL}
  end;

implementation

uses
  SysUtils;

{ TZetaSQLScriptParser }

{* Constructor de la clase}
constructor TZetaSQLScriptParser.Create;
begin
  inherited;
  fScript := TStringList.Create;
  fSentences := TStringList.Create;
  fQuery := TADOQuery.Create(nil);
  fEliminarComentarios := TRUE;
end;

{* Destructor de la clase}
destructor TZetaSQLScriptParser.Destroy;
begin
  FreeAndNil(fQuery);
  FreeAndNil(fSentences);
  FreeAndNil(fScript);
  inherited;
end;

{* Ejecuta cada sentencia del script.
   @return TRUE si ejecut� todas las sentencias SQL sin errores, FALSE en caso contrario}
function TZetaSQLScriptParser.Execute: Boolean;
var
  I: Integer;
begin
  fIndex := 0;
  // Por cada script separado...
  if Assigned(fOnExecSQL) then
    for I := 0 to fSentences.Count - 1 do begin
      fIndex := I;
      try
        Result := fOnExecSQL(Self, fSentences[I]);
      except
        on e:Exception do
          //Result := False
      end;
      //if not Result then
        //Break;
    end
  else
    Result := False;
end;

{* Carga un archivo de scripts.
   @param FileName Nombre del archivo de scripts}
procedure TZetaSQLScriptParser.LoadFromFile(FileName: string);
var
  FileText: TStringList;
begin
  FileText := TStringList.Create;
  if not FileExists(FileName) then
    Exit;
  FileText.LoadFromFile(FileName);
  Text := FileText.Text;
  FreeAndNil(FileText);
end;

{* Analiza el texto del script para eliminar comentarios y separar cada sentencia.
   @param Value Sentencias SQL de un script}
procedure TZetaSQLScriptParser.SetText(const Value: string);
var
  I, Pos1, Pos2: Integer;
  TheText: string;
begin
  fText   := Value;
  TheText := Value;
  fSentences.Clear;

  // Eliminar comentarios Multilinea   /* */
  Pos1 := Pos('/*', TheText);
  while Pos1 > 0 do begin
    Pos2 := Pos('*/', TheText, Pos1 + 2);
    if Pos2 > Pos1 then // Se cierra el comentario multilinea
      Delete(TheText, Pos1, Pos2 - Pos1 + 2)
    else // Queda abierto, error de sintaxis
      TheText := '';
    Pos1 := Pos('/*', TheText);
  end;
  fScript.Text := TheText;

  // Eliminar comentarios con --
  if fEliminarComentarios then
  begin
      I := 0;
      while I < fScript.Count do begin
        TheText := fScript[I];
        Pos1 := Pos('--', TheText);
        if Pos1 > 0 then
          Delete(TheText, Pos1, Length(TheText));
        fScript[I] := TheText;
        Inc(I);
      end;
  end;

  // (Parse) Separar GOs de archivo scripts
  TheText := '';
  while fScript.Count > 0 do begin
    if UpperCase(Trim(fScript[0])) = 'GO' then begin
      fScript.Delete(0);
      if Trim(TheText) <> '' then
        fSentences.Add(TheText);
      TheText := '';
    end else begin
      TheText := TheText + fScript[0] + sLineBreak;
      fScript.Delete(0);
    end;
  end;
  if Trim(TheText) <> '' then
    fSentences.Add(TheText);
end;

end.
