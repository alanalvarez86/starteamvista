unit ZetaTipoEntidad;

interface
uses SysUtils, Classes,ZetaCommonLists;

{$DEFINE MULTIPLES_ENTIDADES}
{$DEFINE DEFECTO1715}
{$DEFINE CAMBIO_TNOM}
{$INCLUDE DEFINES.INC}

{ Tipo Entidades}
{$ifdef DOS_CAPAS}
   {$INCLUDE ZetaTipoEntidadDosCapas.inc}
{$else}
   {$ifdef NORDD}
       {$INCLUDE ZetaTipoEntidadDosCapas.inc}
   {$else}
       {$INCLUDE ZetaTipoEntidadRDD.inc}
   {$endif}
{$endif}

{$ifndef MULTIPLES_ENTIDADES}

type
    ListaEntidades = set of TipoEntidad;
{$endif}

const
    {*****NOTA IMPORTANTE*****}
    {CV: NOTA IMPORTANTE }
    {Para modificar alguna de las constantes siguientes, ver el documento
    "Cuando se agrega una tabla nueva al Sistema.doc"
    que se encuentra en el Starteam \3win_21\Documentos\Reporteador}

    {CV: Entidades que si muestran parametros de nomina}
    {$ifndef MULTIPLES_ENTIDADES}
    NomParamEntidades : ListaEntidades
    {$else}
    NomParamEntidades
    {$endif} = [ enNomina,enAusencia,enFaltas,enMovimien ];
    {CV: Entidades que pueden tener poliza contable}
    {$ifndef MULTIPLES_ENTIDADES}
    TipoPoliza : ListaEntidades
    {$else}
    TipoPoliza
    {$endif} = [ enNomina,enAusencia,enWorks ];
    {CV: Entidad con la que se evalua las condiciones de la tabla Querys (es la Entidad default del Evaluador) }
    EntidadDefaultCondicion = enEmpleado;
    {CV: Entidades que son para reportes especiales}
    {$ifndef MULTIPLES_ENTIDADES}
    ReporteEspecial :ListaEntidades =[
    {$else}
    var ReporteEspecial: array[1..12] of TipoEntidad =(
    {$endif}         enMovimienBalanza,
                     enCurProg,
                     enCalendario,
                     enMovimienLista,
                     enEstadoAhorro,
                     enEstadoPrestamo,
                     enDemografica,
                     enRotacion,
                     enConcilia,
                     enCalendarioHoras,
                     enListadoNomina,
                     enMovimienBalanzaMensual
    {$ifndef MULTIPLES_ENTIDADES}
           ];
    {$else}
           );
    const
    {$endif}
    {CV: Entidades que si pueden tener condiciones}
    {$ifndef MULTIPLES_ENTIDADES}
    EntidadConCondiciones: ListaEntidades;
    {$else}
      {$IFNDEF SERVERCAFE}
      K_LIMITE_ENTIDAD_CONDICIONES=97;
      {$else}
      K_LIMITE_ENTIDAD_CONDICIONES=96;
      {$endif}
      var EntidadConCondiciones: array[1..K_LIMITE_ENTIDAD_CONDICIONES] of TipoEntidad =(
               enAcumula,
               enAhorro,
               enAusencia,
               enAntesPto,
               enAntesCur,
               enChecadas,
               {$IFNDEF SERVERCAFE}
               enEmpleado,
               {$ENDIF}
               enFaltas,
               enImagen,
               enIncapacidad,
               enKarCurso,
               enKardex,
               enKarfija,
               enLiq_Emp,
               enLiq_IMSS,
               enLiq_Mov,
               enMovimien,
               enNomina,
               enPariente,
               enPCarAbo,
               enPermiso,
               enPrestamo,
               enVacacion,
               enMovimienBalanza,
               enEstadoAhorro,
               enEstadoPrestamo,
               enCurProg,
               //enDemografica,
               enRepAhorro,
               enAguinaldo,
               //enRotacion,
               enCompara,
               enDeclara,
               enRepartoPTU,
               enInvitacion,
               enComida,
               enInvitador,
               enCalendario,
               enMovimienLista,
               enConcilia,
               enCurAsis,
               enOrdenesFijas,
               enWorks,
               enWorder,
               enTemporalNomina,
               enCursoGlobal,
               enCedulaEmpleado,
               enCedulaWorder,
               enAsigna,
               enLecturas,
               enCalendarioHoras,
               enKarTool,
               enListadoNomina,
               enExpediente,
               enConsulta,
               enEmbarazo,
               enAccidente,
               enMedEntregada,
               enKardexArea,
               enEmpleadoCompete,
               enEmpleadoPlan,
               enDeclaraAnual,
               enEmpProg,
               enAccesLog,
               enClasifiTemp
               {$ifdef DEFECTO1715}
               ,
               {CV-Junio2010- Correcion defecto 1715:
               Que todas las tablas que tengan relacion a colabora (directa o indirectamente)
               filtren el nivel de confidencialidad}
               enACarAbo,
               //enBitacora,
               enCerProg,
               enCompara,
               enCtaMovs,
               enEventoAlta,
               enEvalua,
               enFonCre,
               enFonEmp,
               enInscrito,
               enKarCert,
               enKarInf,
               //enPlazas,
               //enProceso,
               enSujComp,
               enSujPreg,
               enSujeto,
               enRPromVar,
               //enBitacoraComparte,
               //enBitacoraCafeteria,
               //enBitacoraCafeteriaEmp,
               //enBitacoraKiosco,
               enEvaComp,
               enEvaPreg,
               enVLiqEmp,
               enPoll,
               //enUsuarios,
               enPlanVacacion,
               enTransferencias,
               enDocumento,
               enMovimienBalanzaMensual,
               enNomMes,
               enHuella,
               enKarEmpSilla,
               enPrevioISR
               {$endif}
               ,enWFCambioSalario
               ,enWFAutorizacion
               ,enWFCambioMultiple
               ,enWFPermisos
               ,enWFVacaciones
               ,enPropCost
               ,enSaldoVacaciones
                );
    const
    {$ENDIF}
    {CV: Entidades que no contienen Orden default al dar de alta un reporte nuevo}
    {$ifndef MULTIPLES_ENTIDADES}
    ReportesSinOrdenDef : ListaEntidades
    {$else}
    ReportesSinOrdenDef
    {$endif}=[enDemografica, enRotacion ];
    {$ifdef CAMBIO_TNOM}
    { Entidades en las cuales se necesita agregar las fechas de n�mina para evaluar}
    EntidadesNomina = [ enNomina, enFaltas, enMovimien, enMovimienBalanza, enMovimienLista, enListadoNomina];
    {$endif}


//function ObtieneEntidad( const Index: TipoEntidad ): String;
function GetConteoEntidad(const eTipo: eCamposConteo):TipoEntidad;
procedure LlenaTipoPoliza( Lista: TStrings );
function Dentro( const Entidad: TipoEntidad; ListaEntidades: Array of TipoEntidad ): Boolean;


implementation
uses
    ZetaTipoEntidadTools;

{function ObtieneEntidad( const Index: TipoEntidad ): String;
begin
     if Index in [ Low(TipoEntidad)..High(TipoEntidad) ] then
     begin
          Result :=  aTipoEntidad[ Index  ];
     end
     else
     begin
          Result := '< Tabla p/ Uso Futuro >';
     end;
end;     }

function Dentro( const Entidad: TipoEntidad; ListaEntidades: Array of TipoEntidad ): Boolean;
begin
     Result := ZetaTipoEntidadTools.Dentro(Entidad,ListaEntidades);
end;

procedure LlenaTipoPoliza( Lista: TStrings );
begin
     { Se puede poner en funci�n al arreglo TipoPoliza }
     with Lista do
     begin
          try
             BeginUpdate;
             Clear;

             Add( Format( '0=%s', [aTipoEntidad[enNomina]] ) );
             Add( Format( '1=%s', [aTipoEntidad[enAusencia]] ) );
             Add( Format( '2=%s', [aTipoEntidad[enWorks]] ) );
          finally
                 EndUpdate;
          end;
     end;
end;

function GetConteoEntidad(const eTipo: eCamposConteo):TipoEntidad;
begin
     case eTipo of
          coPuesto : Result := enPuesto;
          coTurno : Result := enTurno;
          coClasifi : Result := enClasifi;
          coConfidencial : Result := enNivel0;
          coNivel1..coNivel9 : Result := TipoEntidad(Ord(enNivel1) + Ord(eTipo) - Ord(coNivel1))
          else Result := enNinguno;
     end;
end;

end.
