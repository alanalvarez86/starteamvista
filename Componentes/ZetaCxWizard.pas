unit ZetaCXWizard;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ComCtrls, Buttons, ExtCtrls,
     ZetaCommonTools,
     ZetaMessages,cxGraphics, cxLookAndFeelPainters, cxLookAndFeels,
     dxCustomWizardControl, dxWizardControl;

type
  TWizardMove = procedure( Sender: TObject; var iNewPage: Integer; var CanMove: Boolean ) of object;
  WizardState = ( wzDataEntry, wzExecuting, wzExecuted, wzCanceled );

  TZetaCXWizard = class(TComponent)
    private
    FSaltoEspecial:Boolean;
    FAdelante: Boolean;
    FPageControl: TdxWizardControl;
    FAlEjecutar: TConfirmEvent;
    FAlCancelar: TConfirmEvent;
    FAfterMove: TNotifyEvent;
    FBeforeMove: TWizardMove;
    FEstado: WizardState;
    FReejecutar: Boolean;
    function GetFirstPage: Integer;
    function GetLastPage: Integer;
    function GetPrimerPaso: Boolean;
    function GetUltimoPaso: Boolean;
    function GetCapturando: Boolean;
    function GetEjecutando: Boolean;
    function GetEjecutado: Boolean;
    function GetCancelado: Boolean;
    procedure SetEstado( Value: WizardState );

  protected
    { Protected declarations }
    function HayPageControl: Boolean;
    procedure SetPageControl( Value: TdxWizardControl );
    procedure Notification( AComponent: TComponent; Operation: TOperation ); override;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    property Cancelado: Boolean read GetCancelado;
    property Capturando: Boolean read GetCapturando;
    property Ejecutando: Boolean read GetEjecutando;
    property Ejecutado: Boolean read GetEjecutado;
    property Estado: WizardState read FEstado;
    property Preparado: Boolean read HayPageControl;
    property PrimeraPagina: Integer read GetFirstPage;
    property PrimerPaso: Boolean read GetPrimerPaso;
    property UltimaPagina: Integer read GetLastPage;
    property UltimoPaso: Boolean read GetUltimoPaso;
    property Adelante :Boolean read FAdelante;
    function EsPaginaActual( Value: TdxWizardControlPage ): Boolean;
    procedure Reset;
    procedure Primero;
    procedure Anterior;
    function Siguiente:Boolean;
    procedure Ultimo;
    procedure Ejecutar;
    procedure Cancelar;
    procedure Abortar;
    //function SaltarAtras:Boolean;
    function Saltar( AKindButton:TdxWizardControlButtonKind ):Boolean;overload;
    function Saltar( iNewPageIndex:Integer ):Boolean;overload;
    {***DevEx(by am): Metodos agregados para el cambio en la navegacion del wizard***}
    function SaltarAtrasEspecial( AKindButton:TdxWizardControlButtonKind ):Boolean;
    function SaltarAtras( AKindButton:TdxWizardControlButtonKind ):Boolean;
    function SaltarAdelanteEspecial( AKindButton:TdxWizardControlButtonKind ):Boolean;
    function SaltarAdelante( iNewPageIndex:Integer ):Boolean;
    Property SaltoEspecial:Boolean read FSaltoEspecial  write FSaltoEspecial default FALSE;
    {***FIN***}
  published
    { Published declarations }
    property AlEjecutar: TConfirmEvent read FAlEjecutar write FAlEjecutar;
    property AlCancelar: TConfirmEvent read FAlCancelar write FAlCancelar;
    property AfterMove: TNotifyEvent read FAfterMove write FAfterMove;
    property BeforeMove: TWizardMove read FBeforeMove write FBeforeMove;
    property PageControl: TdxWizardControl read FPageControl write SetPageControl;
    property Reejecutar: Boolean read FReejecutar write FReejecutar;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Tress Client', [TZetaCXWizard]);
end;


procedure TZetaCXWizard.Abortar;
begin
     SetEstado( wzCanceled );
end;

procedure TZetaCXWizard.Anterior;
begin
     if HayPageControl then
        //Saltar( wcbkBack );
        SaltarAtras( wcbkBack );
end;

procedure TZetaCXWizard.Cancelar;
var
   lOk: Boolean;
begin
     try
        lOk := True;
        if Assigned( FAlCancelar ) then
           FAlCancelar( Self, lOk );
     finally
            if lOk then
               SetEstado( wzCanceled );
     end;
end;

constructor TZetaCXWizard.Create(AOwner: TComponent);
begin
     inherited Create( AOwner );
     Reset;
end;

procedure TZetaCXWizard.Ejecutar;
var
   lOk: Boolean;
   wzValue: WizardState;
begin
     wzValue := FEstado;
     SetEstado( wzExecuting );
     try
        lOk := True;
        if Assigned( FAlEjecutar ) then
           FAlEjecutar( Self, lOk );
     finally
            if lOk then
               SetEstado( wzExecuted )
            else
                SetEstado( wzValue );
     end;

end;

function TZetaCXWizard.EsPaginaActual(Value: TdxWizardControlPage): Boolean;
begin
      Result := HayPageControl and ( FPageControl.ActivePage.PageIndex = Value.PageIndex );
end;

function TZetaCXWizard.GetCancelado: Boolean;
begin
     Result := ( FEstado = wzCanceled  );
end;

function TZetaCXWizard.GetCapturando: Boolean;
begin
     Result := ( FEstado = wzDataEntry );
end;

function TZetaCXWizard.GetEjecutado: Boolean;
begin
     Result := ( FEstado = wzExecuted  );
end;

function TZetaCXWizard.GetEjecutando: Boolean;
begin
     Result := ( FEstado = wzExecuting );
end;

function TZetaCXWizard.GetFirstPage: Integer;
var
   i: Integer;
begin
     Result := -2;
     if HayPageControl then
     begin
          with FPageControl do
          begin
               for i := 0 to ( PageCount - 1 ) do
               begin
                    if Pages[ i ].Enabled then
                    begin
                         Result := i;
                         Break;
                    end;
               end;
          end;
     end;
end;

function TZetaCXWizard.GetPrimerPaso: Boolean;
begin
     if HayPageControl then
        Result := ( GetFirstPage = FPageControl.ActivePage.PageIndex )
     else
         Result := False;
end;

function TZetaCXWizard.GetLastPage: Integer;
var
   i: Integer;
begin
     Result := -2;
     if HayPageControl then
     begin
          with FPageControl do
          begin
               for i := ( PageCount - 1 ) downto 0 do
               begin
                    if Pages[ i ].Enabled then
                    begin
                         Result := i;
                         Break;
                    end;
               end;
          end;
     end;
end;

function TZetaCXWizard.GetUltimoPaso: Boolean;
begin
     if HayPageControl then
        Result := ( GetLastPage = FPageControl.ActivePage.PageIndex )
     else
         Result := False;
end;

function TZetaCXWizard.HayPageControl: Boolean;
begin
     Result := Assigned( PageControl ) and ( PageControl.PageCount > 0 );
end;

procedure TZetaCXWizard.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;

end;


procedure TZetaCXWizard.Primero;
begin
     if HayPageControl then
        FPageControl.ActivePageIndex := 0; 
end;

procedure TZetaCXWizard.Reset;
begin
     FAdelante := False;
     SetEstado( wzDataEntry );
end;

//DevEx(by am): Saltar hacia atras a una pagina especifica
{function TZetaCXWizard.SaltarAtras:Boolean;
var
   CanMove: Boolean;
   iNewPageIndex:Integer;
const
     K_PRIMERA_PAGINA=0;
begin
   CanMove := False;
   FAdelante := False;
   if Assigned( FBeforeMove ) then
   begin
      FBeforeMove( Self, iNewPageIndex, CanMove );
      if FPageControl.ActivePageIndex > K_PRIMERA_PAGINA  then
         FPageControl.ActivePageIndex := iNewPageIndex;
   end;
   Result := CanMove;
end; }

{***DevEx(by am): MODIFICACION DE ESTURCTURA DE NAVEGACION DEL WIZARD ***}
//Saltoa hacia atras a una pagina especifica
function TZetaCXWizard.SaltarAtrasEspecial( AKindButton:TdxWizardControlButtonKind ):Boolean;
var
   CanMove: Boolean;
   iNewPageIndex:Integer;
const
     K_PRIMERA_PAGINA=0;
begin
   //CanMove := False;
   FAdelante := AKindButton = wcbkNext;
   if Assigned( FBeforeMove ) and not FAdelante then
   begin
      FBeforeMove( Self, iNewPageIndex, CanMove );
      if FPageControl.ActivePageIndex > K_PRIMERA_PAGINA  then //Para que no retroceda al estar en la primera pagina
         FPageControl.ActivePageIndex := iNewPageIndex;
   end;
   Result := TRUE; //Regresamos TRUE al Handled para que no avance una pagina mas a causa del movimiento por defecto de wcbkBack.
end;

//Salto a la pagina anterior
function TZetaCXWizard.SaltarAtras( AKindButton:TdxWizardControlButtonKind ):Boolean;
var
   CanMove: Boolean;
begin
   CanMove := False;
   FAdelante := AKindButton = wcbkNext;
   //Devolverse a la pagina anterior no se valida beforeMove simbrese podra dar bakc excepto cuando nos encontremos en la primera pagina.
   if (AKindButton = wcbkBack)then
   begin
        CanMove := True;
   end;
   Result := CanMove; //Este resultado no afecta el handled revisar la invocacion en ZcxWizardBasico
end;

//DevEx: Salto hacia adelante a una pagina especifica
function TZetaCXWizard.SaltarAdelanteEspecial( AKindButton:TdxWizardControlButtonKind ):Boolean;
var
   CanMove: Boolean;
   iNewPageIndex:Integer; //TODO: Remove
begin
     CanMove := True;
     FAdelante := AKindButton = wcbkNext;
     if Assigned( FBeforeMove ) and FAdelante then
        FBeforeMove( Self, iNewPageIndex, CanMove );
     if CanMove and FAdelante then
     begin
          FPageControl.ActivePageIndex := iNewPageIndex;
          if Assigned( FAfterMove ) and FAdelante then
             FAfterMove( Self );
     end;
     //CanMove := False; //DevEx (by am): Para que no avance una pagina mas.
     Result := TRUE; //Regresamos TRUE al Handled para que no avance una pagina mas a causa del movimiento por defecto de wcbkNext.
end;

//DevEx: Saltar a la pagina siguiente
function TZetaCXWizard.SaltarAdelante( iNewPageIndex:Integer ):Boolean;
var
   CanMove: Boolean;
begin
     {***Mantenemos iNewPageIndex Actualizdo por si se utiliza en el evento BeforeMove, realmente el cambio
     de pagina lo hace el componente por default a menos que se le indique lo contrario en el handled ***}
     CanMove := True;
     FAdelante := iNewPageIndex > FPageControl.ActivePageIndex;
     if Assigned( FBeforeMove ) and FAdelante then
        FBeforeMove( Self, iNewPageIndex, CanMove ); //Para validar correctamente que se pueda cambiar de pagina afectar el valor de CanMove en la implementacion de WizardBeforeMove
     //Si CanMove regresamos al handled un FALSE para que ejecute su funcionamiento por default. Si CanMove=FALSE mandamos TRUE al handled para que no ejecute su funcionamiento por default.
      Result := not CanMove;
end;

{***FIN MODIFICACION***}

//DevEx: Saltar hacia adelante a una pagina especifica y saltar hacia atras normalmente
function TZetaCXWizard.Saltar( AKindButton:TdxWizardControlButtonKind ):Boolean;
var
   CanMove: Boolean;
   iNewPageIndex:Integer; //TODO: Remove
begin
     CanMove := True;
     FAdelante := AKindButton = wcbkNext;

     //Devolverse a la pagina anterior
     if (AKindButton = wcbkBack)then
     begin
          //iNewPageIndex := FPageControl.ActivePage.PageIndex - 1;
          CanMove := True;
     end;

     if Assigned( FBeforeMove ) and FAdelante then
        FBeforeMove( Self, iNewPageIndex, CanMove );
     if CanMove and FAdelante then
     begin
          FPageControl.ActivePageIndex := iNewPageIndex;
          if SaltoEspecial then
             CanMove := False; //DevEx (by am): Para que no avance una pagina mas.
          if Assigned( FAfterMove ) and FAdelante then
             FAfterMove( Self );
     end;
     Result := CanMove;
end;

//DevEx: Saltar hacia adelante normalmente
function TZetaCXWizard.Saltar( iNewPageIndex:Integer ):Boolean;
var
   CanMove: Boolean;
begin
     CanMove := True;
     FAdelante := iNewPageIndex > FPageControl.ActivePageIndex;
     if Assigned( FBeforeMove ) then
        FBeforeMove( Self, iNewPageIndex, CanMove );
     Result := CanMove;
end;


procedure TZetaCXWizard.SetEstado(Value: WizardState);
begin
     FEstado := Value;
end;

procedure TZetaCXWizard.SetPageControl(Value: TdxWizardControl);
begin
     if ( FPageControl <> Value ) then
     begin
          FPageControl := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

Function TZetaCXWizard.Siguiente:Boolean;
begin
      Result := False;
      if HayPageControl then
         //Result := Saltar( wcbkNext );  //OLD
         Result := SaltarAdelanteEspecial( wcbkNext );
end;

procedure TZetaCXWizard.Ultimo;
begin
     if HayPageControl then
       // Saltar( GetLastPage );  TODO: Remover Ultimo ?
end;


end.
 