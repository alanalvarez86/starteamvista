unit ZetaDBGrid;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Grids, DB, DBGrids,
     ZetaMessages;

type
  TExaminador = ( exUsuario, exDblClick, exEnter );
  TGridSelection = ( dbgTodos, dbgNinguno, dbgInversa );
  TZetaDBGrid = class(TDBGrid)
  private
    { Private declarations }
    FDrawDefault: Boolean;
    FFixedCols: Integer;
    function GetFixedCols: Integer;
    function GetInformacion: String;
    procedure CNKeyDown(var Message: TWMKeyDown); message CN_KEYDOWN; { hereda de TWinControl.CNKeyDown }
    procedure SendExaminar( const eSender: TExaminador );
    procedure SetFixedCols( const Value: Integer );
  protected
    { Protected declarations }
    procedure DblClick; override;                    { hereda de TControl.DblClick }
    procedure DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState); override;
    procedure DrawColumnCell(const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override; { hereda de TDBGrid.KeyDown }
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure SetColumnAttributes; override;
    procedure TitleClick( Column: TColumn ); override; { hereda de TDBGrid.TitleClick }
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    function GetFieldColumnIndex( const sFieldName: String ): Integer;
    procedure Examinar;
    procedure OrdenarPor( Column: TColumn );
  published
    { Published declarations }
    property DrawDefault: Boolean read FDrawDefault write FDrawDefault default True;
    property FixedCols: Integer read GetFixedCols write SetFixedCols default 0;
    property Informacion: String read GetInformacion;
  end;

implementation

uses ZetaClientDataset,
{$ifdef TEST_COMPLETE}
     ZetaCommonClasses,
{$endif}
     ZetaCommonTools;

constructor TZetaDBGrid.Create( AOwner: TComponent );
begin
     inherited Create( AOwner );
     FDrawDefault := True;
     FFixedCols := 0;
     Options := Options + [dgRowSelect] - [dgConfirmDelete];
end;

procedure TZetaDBGrid.SetColumnAttributes;
begin
     inherited SetColumnAttributes;
     if ( dgIndicator in Options ) then
        ColWidths[ 0 ] := IndicatorWidth; // WidthOfIndicator;
     SetFixedCols( FFixedCols );
     SelectedIndex := FFixedCols;
end;

function TZetaDBGrid.GetFixedCols: Integer;
begin
     if DataLink.Active then
        Result := inherited FixedCols - IndicatorOffset
     else
         Result := FFixedCols;
end;

procedure TZetaDBGrid.SetFixedCols( const Value: Integer );
var
   i, iFixCount: Integer;
begin
     if ( FFixedCols <> Value ) then
     begin
          inherited LeftCol := Value;
          FFixedCols := Value;
     end;
     iFixCount := ZetaCommonTools.iMax( Value, 0 ) + IndicatorOffset;
     if DataLink.Active and not ( csLoading in ComponentState ) and ( ColCount > IndicatorOffset + 1 ) then
     begin
          iFixCount := ZetaCommonTools.iMin( iFixCount, ( ColCount - 1 ) );
          inherited FixedCols := iFixCount;
          for i := 1 to iMin( FixedCols, ( ColCount - 1 ) ) do
              TabStops[ i ] := False;
     end;
     FFixedCols := iFixCount - IndicatorOffset;
end;

function TZetaDBGrid.GetFieldColumnIndex( const sFieldName: String): Integer;
var
   i: Integer;
begin
     Result := -1;
     with Columns do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if ( Items[ i ].FieldName = sFieldName ) then
               begin
                    Result := i;
                    Break;
               end;
          end;
     end;
end;

procedure TZetaDBGrid.SendExaminar( const eSender: TExaminador );
var
   Msg: TMessage;
begin
     with Msg do
     begin
          Msg := WM_EXAMINAR;
          LParam := Ord( eSender );
          WParam := 0;
          Result := 0;
     end;
     Owner.Dispatch( Msg );
end;

procedure TZetaDBGrid.Examinar;
begin
     SendExaminar( exUsuario );
end;

procedure TZetaDBGrid.DblClick;
begin
     SendExaminar( exDblClick );
     inherited DblClick;
end;

procedure TZetaDBGrid.TitleClick( Column: TColumn );
begin
     if Assigned( OnTitleClick ) then
        inherited TitleClick( Column )
     else
         OrdenarPor( Column );
end;

procedure TZetaDBGrid.OrdenarPor( Column: TColumn );
begin
     if Column.Field <> NIL then
     begin
          with Column.Field do
          begin
               if ( Dataset is TZetaClientDataset ) then
               begin
                    case FieldKind of
                         fkData: TZetaClientDataset( Dataset ).IndexFieldNames := FieldName;
                         fkLookup: TZetaClientDataset( Dataset ).IndexFieldNames := KeyFields;
                    end;
               end;
          end;
     end;
end;

procedure TZetaDBGrid.CNKeyDown(var Message: TWMKeyDown);
begin
     if ( Message.Charcode = VK_RETURN ) then
     begin
          Message.Result := 1;
          SendExaminar( exEnter );
     end
     else
         inherited;
end;

procedure TZetaDBGrid.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     if ( FFixedCols = 0 ) or ( RawToDataColumn( MouseCoord( X, Y ).X ) > ( FFixedCols - 1 ) ) then
     begin
          inherited MouseDown( Button, Shift, X, Y );
     end;
end;

procedure TZetaDBGrid.KeyDown(var Key: Word; Shift: TShiftState);
begin
     case Key of
          VK_HOME:
          begin
               if ( FixedCols > 0 ) then
               begin
                    SelectedIndex := FixedCols;
                    Key := 0;
               end;
          end;
          VK_LEFT:
          begin
               if ( ssCtrl in Shift ) then
               begin
                    if ( FixedCols > 0 ) then
                    begin
                         SelectedIndex := FixedCols;
                         Key := 0;
                    end;
               end
               else
               begin
                    if ( FixedCols > 0 ) and not ( dgRowSelect in Options ) then
                    begin
                         if ( SelectedIndex <= FFixedCols ) then
                         begin
                              Key := 0;
                         end;
                    end;
               end;
          end;
          VK_DELETE:
          begin
               if ( ssCtrl in Shift ) then
                  Key := 0;
          end;
     end;
     inherited KeyDown( Key, Shift );
end;

procedure TZetaDBGrid.DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState);
begin
     if not ( csDesigning in ComponentState ) and
        ( FixedCols > 0 ) and
        ( aRow = 0 ) and
        ( ACol > 0 ) and
        ( ACol <= ( FixedCols ) ) and
        ( ACol <= ( Columns.Count - 1 ) ) then
     begin
          with Canvas do
          begin
               Font.Color := TitleFont.Color;
               FillRect( ARect );
               TextRect( ARect, ARect.Left + 2, ARect.Top + 2, Columns[ ACol - 1 ].Title.Caption );
               Windows.DrawEdge( Handle, ARect, BDR_RAISEDINNER, BF_BOTTOMRIGHT );
               Windows.DrawEdge( Handle, ARect, BDR_RAISEDINNER, BF_TOPLEFT );
          end;
     end
     else
         inherited DrawCell( ACol, ARow, ARect, AState );
end;

procedure TZetaDBGrid.DrawColumnCell(const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     if not ( csDesigning in ComponentState ) and ( FixedCols > 0 ) and ( gdFixed in State ) then
     begin
          with Canvas do
          begin
               Font.Color := TitleFont.Color;
               DefaultDrawColumnCell( Rect, DataCol, Column, State );
          end;
     end
     else
         inherited DrawColumnCell( Rect, DataCol, Column, State );
end;

function TZetaDBGrid.GetInformacion: String;
{$ifdef TEST_COMPLETE}
var
   oCampo: TField;
   Pos : TBookMark;
   i : Integer;
{$endif}
begin
     Result := '';
{$ifdef TEST_COMPLETE}
     if Assigned( DataSource ) and Assigned( DataSource.DataSet ) then
        with DataSource.DataSet, Columns do
        begin
             if ( not IsEmpty ) and ( Count > 0 ) then
             begin
                  DisableControls;
                  try
                     Pos:= GetBookMark;
                     First;
                     for i := 0 to ( Count - 1 ) do
                         Result := Result + Items[ i ].Title.Caption + K_PIPE;
                     Result := Result + CR_LF;
                     while not EOF do
                     begin
                          for i := 0 to ( Count - 1 ) do
                          begin
                               oCampo := Items[ i ].Field;
                               if Assigned( oCampo ) then
                                  Result := Result + Trim( oCampo.AsString ) + K_PIPE   // Para evitar diferencias por IBObjects que puede regresar strings con espacios a la izquierda ( Ej. Join de Historial de Kardex con Permisos, Incapacidades y Vacaciones
                               else
                                  Result := Result + K_PIPE;
                          end;
                          Result := Result + CR_LF;
                          Next;
                     end;
                     if ( Pos <> nil ) then
                     begin
                          GotoBookMark( Pos );
                          FreeBookMark( Pos );
                     end;
                  finally
                     EnableControls;
                  end;
             end;
        end;
{$endif}
end;

end.
