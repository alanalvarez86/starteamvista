unit ZetaNumero;

interface

uses WinTypes, Classes, StdCtrls, Controls, Messages,
     SysUtils, Forms, Graphics, Menus, Buttons, Dialogs,
     Mask,
     MaskUtils,
     DB, DBCtrls, Comctrls, Clipbrd;

type
  TMascaraNumerica = ( mnHoras,
                       mnMinutos,
                       mnPesosDiario,
                       mnPesos,
                       mnPesosEmpresa,
                       mnTasa,
                       mnDias,
                       mnDiasFraccion,
                       mnEmpleado,
                       mnNumeroGlobal,
                       mnVecesSMGDF,
                       mnTasa3,
                       mnMillares,
                       mnMillares5,
                       mnTasa5,
                       mnMillares4, {OP: 28/05/08}
                       mnDiasNoFraccion, {US 13038: 27/05/2016}
                       mnVecesUMA ); {US 12860 : 06/06/2016 }
  TZetaNumero = class( TCustomMaskEdit )
  private
    { Private declarations }
    FAlignment: TAlignment;
    FCanvas: TControlCanvas;
    FValor: Double;
    FUseEnterKey: Boolean;
    FMascara: TMascaraNumerica;
    FEnteros: Byte;
    FDecimales: Byte;
    FFormatString: String;
    FFormatMask: String;
    function GetValor: Double;
    function GetValorEntero: Integer;
    function GetValorAsText: String;
    function GetTextMargins: TPoint;
    procedure SetValor( const Value: Double );
    procedure SetMascara( Value: TMascaraNumerica );
    procedure InitMascara;
    procedure DisplayNumber;
    procedure DisplayText( const Value: String );
    procedure CMExit( var Message: TCMExit ); message CM_EXIT;
    procedure WMPaint( var Message: TWMPaint ); message WM_PAINT;
    procedure WMCopy( var Message: TMessage ); message WM_COPY;
    procedure WMCut( var Message: TMessage ); message WM_CUT;
    procedure WMPaste( var Message: TMessage ); message WM_PASTE;
  protected
    { Protected declarations }
    function GetNumberValue: Boolean;
    function NumeroVacio: Boolean;
    function PadWithSpaces( const sNumber: String ): String;
    function SetNumberValue: Boolean; virtual;
    function ValidEdit: Boolean; virtual;
    function ValidNumber( var sNumber: String ): Boolean;
    procedure SetMask( const lEdicion: Boolean );
    procedure DoEnter; override;
    procedure DoEdit;
    procedure QuitaEspacios;
    procedure Enfocar;
    procedure HayError;
    procedure KeyDown( var Key: Word; Shift: TShiftState); override;
    procedure KeyPress( var Key: Char ); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
  public
    { Public declarations }
    property Valor: Double read GetValor write SetValor;
    property ValorEntero: Integer read GetValorEntero;
    property ValorAsText: String read GetValorAsText;
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    procedure Loaded; override;
    procedure CopyToClipboard;
    procedure CutToClipboard;
    procedure PasteFromClipboard;
  published
    { Published declarations }
    property Anchors;
    property AutoSelect;
    property AutoSize;
    property BorderStyle;
    property CharCase;
    property Color;
    property Ctl3D;
    property DragCursor;
    property DragMode;
    property Enabled;
    property Font;
    property ImeMode;
    property ImeName;
    property Mascara: TMascaraNumerica read FMascara write SetMascara;
    property MaxLength;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PasswordChar;
    property PopupMenu;
    property ReadOnly;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Text;
    property UseEnterKey: Boolean read FUseEnterKey write FUseEnterKey default False;
    property Visible;
    property OnChange;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDrag;
  end;
  TZetaDBNumero = class( TZetaNumero )
  private
    { Private declarations }
    FDataLink: TFieldDataLink;
    FConfirmEdit: Boolean;
    FConfirmMsg: String;
    function GetConfirmMsg: String;
    function GetDataField: String;
    function GetDataSource: TDataSource;
    function GetField: TField;
    function GetReadOnly: Boolean;
    procedure SetDataField( const Value: string );
    procedure SetDataSource( Value: TDataSource );
    procedure SetReadOnly( const Value: Boolean );
    procedure CheckFieldType( const Value: String );
    procedure DataChange( Sender: TObject );
    procedure EditingChange( Sender: TObject );
    procedure UpdateData( Sender: TObject );
    procedure ActiveChange( Sender: TObject );
  protected
    { Protected declarations }
    function SetNumberValue: Boolean; override;
    function ValidEdit: Boolean; override;
    procedure Change; override;
    procedure Notification( AComponent: TComponent; Operation: TOperation ); override;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    property Field: TField read GetField;
  published
    { Published declarations }
    property ConfirmEdit: Boolean read FConfirmEdit write FConfirmEdit default False;
    property ConfirmMsg: String read FConfirmMsg write FConfirmMsg;
    property DataField: String read GetDataField write SetDataField;
    property DataSource: TDataSource read GetDataSource write SetDataSource;
    property ReadOnly: Boolean read GetReadOnly write SetReadOnly default False;
  end;

implementation

uses Winprocs,
     ZetaMsgDlg,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaCommonLists;

const
     NULL_NUMBER = 0;
     aMascarasNumericas: array[ TMascaraNumerica ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( '3|2|!999.99;1; |##0.00;(##0.00)',
                                                                '3|0|!#99;0; |##0;(##0)',
                                                                '5|2|!#9999.99;1; |##,##0.00;(##,##0.00)',
                                                                '10|2|!#999999999.99;1; |#,###,###,##0.00;(#,###,###,##0.00)',
                                                                '13|2|!#999999999999.99;1; |#,###,###,###,##0.00;(#,###,###,###,##0.00)',
                                                                '3|5|!#99.99999;1; |##0.0####" %";(##0.00###)" %"',
                                                                '5|0|!#9999;0; |####0;(####0)',
                                                                '3|2|!999.99;1; |##0.00;(##0.00)',
                                                                '9|0|!999999999;0; |#########',
                                                                '12|5|!#99999999999.99999;1; |###,###,###,##0.00###;(###,###,###,##0.00###)',
                                                                '3|4|!999.9900;1; |##0.00##" SMGDF"',
                                                                '6|3|!999999.999;1; |#,0.000 %',
                                                                '9|3|!999999999.999;1; |#,0.000',
                                                                '9|5|!999999999.99999;1; |#,0.00000',
                                                                '6|5|!999999.99999;1; |#,0.00000 %',
                                                                '9|4|!999999999.9999;1; |#,0.0000', {OP: 28/05/08}
                                                                '5|0|!99999;0; |####0;####0',{US 13038: 27/05/2016}
                                                                '3|4|!999.9900;1; |##0.00##" UMA"');{US 12860: 06/06/2016}

{ ********* TZetaNumero **************** }

constructor TZetaNumero.Create( AOwner: TComponent );
begin
     FAlignment := taRightJustify;
     FUseEnterKey := False;
     inherited Create( AOwner );
     Width := 121;
     ControlStyle := ControlStyle - [csSetCaption];
     InitMascara;
     Valor := 0;
end;

destructor TZetaNumero.Destroy;
begin
     FCanvas.Free;
     inherited Destroy;
end;

procedure TZetaNumero.Loaded;
begin
     inherited Loaded;
     DisplayNumber;
end;

function TZetaNumero.GetValor: Double;
begin
     Result := FValor;
end;

function TZetaNumero.GetValorEntero: Integer;
begin
     Result := Round( Valor );
end;

procedure TZetaNumero.SetValor( const Value: Double );
begin
     if ( Value <> FValor ) then
     begin
          FValor := Value;
     end;
     DisplayNumber;
end;

procedure TZetaNumero.SetMascara( Value: TMascaraNumerica );
begin
     if ( FMascara <> Value ) then
     begin
          FMascara := Value;
          InitMascara;
          DisplayNumber;
     end;
end;

procedure TZetaNumero.InitMascara;
var
   sMascara: String;

function NextToken( var sTarget: String ): String;
var
   i: Integer;
begin
     i := Pos( '|', sTarget );
     if ( i <> 0 ) then
     begin
          Result := System.Copy( sTarget, 1, ( i - 1 ) );
          System.Delete( sTarget, 1, i );
     end
     else
     begin
          Result := sTarget;
          sTarget := '';
     end;
end;

begin
     sMascara := aMascarasNumericas[ Mascara ];
     FEnteros := StrAsInteger( NextToken( sMascara ) );
     FDecimales := StrAsInteger( NextToken( sMascara ) );
     FFormatMask := NextToken( sMascara );
     FFormatString := NextToken( sMascara );
end;

function TZetaNumero.NumeroVacio: Boolean;
begin
     Result := ( Valor = 0 );
end;

function TZetaNumero.ValidNumber( var sNumber: String ): Boolean;
var
   rValor: Double;
   iPos: Integer;
begin
     sNumber := Trim( sNumber );
     iPos := Pos( ' ', sNumber );
     while ( iPos > 0 ) do
     begin
          Delete( sNumber, iPos, 1 );
          iPos := Pos( ' ', sNumber );
     end;
     rValor := 0;
     Result := ( sNumber = '' );
     if not Result then
     begin
          try
             rValor := StrToFloat( sNumber );
             Result := True;
          except
          end;
     end;
     if Result then
        Valor := rValor;
end;

procedure TZetaNumero.QuitaEspacios;
var
   iPos: Word;
   sNumber: String;
begin
     sNumber := Trim( Text );
     iPos := Pos( ' ', sNumber );
     while ( iPos > 0 ) do
     begin
          Delete( sNumber, iPos, 1 );
          iPos := Pos( ' ', sNumber );
     end;
     DisplayText( sNumber );
end;

function TZetaNumero.PadWithSpaces( const sNumber: String ): String;
var
   iPos: Word;
begin
     iPos := Pos( '.', sNumber );
     if ( iPos = 0 ) then
        Result := PadL( sNumber, FEnteros )
     else
         Result := PadL( Copy( sNumber, 1, ( iPos - 1 ) ), FEnteros ) +
                    '.' +
                    PadR( Copy( sNumber, ( iPos + 1 ), ( Length( sNumber ) - iPos ) ), FDecimales );
end;

function TZetaNumero.GetNumberValue: Boolean;
var
   sValor: String;
begin
     if Modified then
     begin
          sValor := Text;
          Result := ValidNumber( sValor );
          if not Result then
          begin
               DisplayText( sValor );
               HayError;
          end;
     end
     else
         Result := True;
end;

procedure TZetaNumero.SetMask( const lEdicion: Boolean );
begin
     if ( lEdicion <> IsMasked ) then
     begin
          if lEdicion then
             EditMask := FFormatMask
          else
              EditMask := '';
          DisplayNumber;
     end;
end;

function TZetaNumero.SetNumberValue: Boolean;
begin
     Result := GetNumberValue;
end;

function TZetaNumero.GetValorAsText: String;
begin
     Result := Format( '%*.*f', [ FEnteros, FDecimales, Valor ] )
end;

procedure TZetaNumero.DisplayNumber;
begin
     if IsMasked then
        DisplayText( ValorAsText )
     else
         DisplayText( FormatFloat( FFormatString, Valor ) );
     if Focused then
        SelectAll;
end;

procedure TZetaNumero.DisplayText( const Value: String );
begin
     if IsMasked then
        Text := PadWithSpaces( Value )
     else
         Text := Value;
end;

procedure TZetaNumero.DoEnter;
begin
     SetMask( True );
     if ( FAlignment <> taLeftJustify ) and not IsMasked then
        Invalidate;
     Enfocar;
     inherited DoEnter;
end;

function TZetaNumero.ValidEdit: Boolean;
begin
     Result := True;
end;

procedure TZetaNumero.DoEdit;
begin
     if not Focused then
        Enfocar;
end;

procedure TZetaNumero.MouseDown( Button: TMouseButton; Shift: TShiftState; X, Y: Integer );
begin
     inherited MouseDown( Button, Shift, X, Y );
     if ( Button = mbLeft ) then
        SelectAll;
end;

procedure TZetaNumero.KeyDown( var Key: Word; Shift: TShiftState);
begin
     case Key of
          VK_DELETE:
          begin
               if ( Shift = [] ) then
               begin
                    if ValidEdit then
                       DoEdit
                    else
                        Key := 0;
               end;
          end;
          VK_RETURN:
          begin
               if UseEnterKey then
               begin
                    Key := 0;
                    if not GetNumberValue then
                       HayError;
               end;
          end;
     end;
     inherited KeyDown( Key, Shift );
end;

procedure TZetaNumero.KeyPress( var Key: Char );

procedure PerformEdit;
begin
     if ValidEdit then
        DoEdit
     else
         Key := Chr( 0 );
end;

begin
     if ( Key <> Chr( VK_RETURN ) ) then { ENTER as TAB }
     begin
          case Key of
               #48..#57: PerformEdit;
               '-': PerformEdit;
               '.':
               begin
                    if ValidEdit then
                    begin
                         DoEdit;
                         QuitaEspacios;
                    end
                    else
                        Key := Chr( 0 );
               end;
               Chr( VK_BACK ): PerformEdit;
          else
              begin
                   MessageBeep( MB_ICONEXCLAMATION );
                   Key := Chr( 0 );
              end;
          end;
     end;
     inherited KeyPress( Key );
end;

procedure TZetaNumero.CMExit( var Message: TCMExit );
begin
     if SetNumberValue then
     begin
          SetMask( False );
          inherited;
     end
     else
         HayError;
end;

procedure TZetaNumero.WMCopy( var Message: TMessage );
begin
     CopyToClipboard;
end;

procedure TZetaNumero.WMPaste( var Message: TMessage );
begin
     PasteFromClipboard;
end;

procedure TZetaNumero.WMCut( var Message: TMessage );
begin
     CutToClipboard;
end;

{ GA: 05/Dic/2001: Se tiene que hacer esto par aque el n�mero salga justificado a la derecha}
procedure TZetaNumero.WMPaint( var Message: TWMPaint );
var
   Left: Integer;
   Margins: TPoint;
   R: TRect;
   DC: HDC;
   PS: TPaintStruct;
   S: string;
begin
     if ( ( FAlignment = taLeftJustify ) or Focused ) and
        not ( csPaintCopy in ControlState ) then
     begin
          inherited;
          Exit;
     end;
     if ( FCanvas = nil ) then
     begin
          FCanvas := TControlCanvas.Create;
          FCanvas.Control := Self;
     end;
     DC := Message.DC;
     if ( DC = 0 ) then
        DC := BeginPaint( Handle, PS );
     FCanvas.Handle := DC;
     try
        FCanvas.Font := Font;
        with FCanvas do
        begin
             R := ClientRect;
             if not ( NewStyleControls and Ctl3D ) and ( BorderStyle = bsSingle ) then
             begin
                  Brush.Color := clWindowFrame;
                  FrameRect( R );
                  InflateRect( R, -1, -1 );
             end;
             Brush.Color := Color;
             if ( csPaintCopy in ControlState )  then
             begin
                  S := Text;
                  case CharCase of
                       ecUpperCase: S := AnsiUpperCase( S );
                       ecLowerCase: S := AnsiLowerCase( S );
                  end;
             end
             else
                 S := EditText;
             if ( PasswordChar <> #0 ) then
                FillChar( S[ 1 ], Length( S ), PasswordChar );
             Margins := GetTextMargins;
             case FAlignment of
                  taLeftJustify: Left := Margins.X;
                  taRightJustify: Left := ClientWidth - TextWidth( S ) - Margins.X - 1;
             else
                 Left := ( ClientWidth - TextWidth( S ) ) div 2;
             end;
             TextRect( R, Left, Margins.Y, S );
        end;
     finally
            FCanvas.Handle := 0;
            if ( Message.DC = 0 ) then
            EndPaint( Handle, PS );
     end;
end;

function TZetaNumero.GetTextMargins: TPoint;
var
   DC: HDC;
   SaveFont: HFont;
   I: Integer;
   SysMetrics, Metrics: TTextMetric;
begin
     if NewStyleControls then
     begin
          if ( BorderStyle = bsNone ) then
             I := 0
          else
              if Ctl3D then
                 I := 1
              else
                  I := 2;
          Result.X := SendMessage( Handle, EM_GETMARGINS, 0, 0 ) and ( $0000FFFF + I );
          Result.Y := I;
     end
     else
     begin
          if ( BorderStyle = bsNone ) then
             I := 0
          else
          begin
               DC := GetDC( 0 );
               GetTextMetrics( DC, SysMetrics );
               SaveFont := SelectObject( DC, Font.Handle );
               GetTextMetrics( DC, Metrics );
               SelectObject( DC, SaveFont );
               ReleaseDC( 0, DC );
               I := SysMetrics.tmHeight;
               if ( I > Metrics.tmHeight ) then
                  I := Metrics.tmHeight;
               I := I div 4;
          end;
          Result.X := I;
          Result.Y := I;
     end;
end;

procedure TZetaNumero.HayError;
begin
     MessageBeep( MB_ICONEXCLAMATION );
     Enfocar;
end;

procedure TZetaNumero.Enfocar;
begin
     if CanFocus then
     begin
          SetFocus;
          SelectAll;
     end;
end;

procedure TZetaNumero.CopyToClipboard;
begin
     if Focused then
        Clipboard.AsText := Text
     else
         Clipboard.AsText := ValorAsText;
end;

procedure TZetaNumero.CutToClipboard;
begin
     if not ReadOnly and ValidEdit then
     begin
          CopyToClipboard;
          Valor := 0;
     end;
end;

procedure TZetaNumero.PasteFromClipboard;
var
   sValue: String;
begin
     with Clipboard do
     begin
          if not ReadOnly and HasFormat( CF_TEXT ) and ValidEdit then
          begin
               sValue := AsText;
               ValidNumber( sValue );
          end;
     end;
end;

{ ************ TZetaDBNumero ************** }

constructor TZetaDBNumero.Create( AOwner : TComponent );
begin
     inherited Create( AOwner );
     FConfirmEdit := False;
     FConfirmMsg := '';
     FDataLink := TFieldDataLink.Create;
     with FDataLink do
     begin
          Control := Self;
          OnDataChange := DataChange;
          OnEditingChange := EditingChange;
          OnUpdateData := UpdateData;
          OnActiveChange := ActiveChange;
     end;
     if not ( csDesigning in ComponentState ) then
        CheckFieldType( DataField );
end;

destructor TZetaDBNumero.Destroy;
begin
     with FDataLink do
     begin
          OnActiveChange := nil;
          OnUpdateData := nil;
          OnEditingChange := nil;
          OnDataChange := nil;
          Control := nil;
          Free;
     end;
     FDataLink := nil;
     inherited Destroy;
end;

procedure TZetaDBNumero.Notification( AComponent: TComponent; Operation: TOperation );
begin
     inherited Notification( AComponent, Operation );
     if ( Operation = opRemove ) then
        if ( FDataLink <> nil ) and ( AComponent = DataSource ) then
           DataSource := nil;
end;

function TZetaDBNumero.GetDataField: String;
begin
     Result := FDataLink.FieldName;
end;

function TZetaDBNumero.GetField: TField;
begin
     Result := FDataLink.Field;
end;

function TZetaDBNumero.GetDataSource: TDataSource;
begin
     Result := FDataLink.DataSource;
end;

procedure TZetaDBNumero.SetDataField( const Value: String );
begin
     CheckFieldType( Value );
     FDataLink.FieldName := Trim( Value );
end;

procedure TZetaDBNumero.SetDataSource( Value: TDataSource );
begin
     if ( FDataLink.DataSource <> Value ) then
     begin
          FDataLink.DataSource := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

function TZetaDBNumero.GetReadOnly: Boolean;
begin
     Result := FDataLink.ReadOnly;
end;

procedure TZetaDBNumero.SetReadOnly( const Value: Boolean );
begin
     FDataLink.ReadOnly := Value;
end;

function TZetaDBNumero.GetConfirmMsg: String;
begin
     if ( FConfirmMsg = VACIO ) then
        Result := '� Desea Cambiar Este C�digo ?'
     else
         Result := FConfirmMsg;
end;

procedure TZetaDBNumero.CheckFieldType(const Value: String);
begin
  if (Value <> '') and (FDataLink <> nil) and (FDataLink.Dataset <> nil) and (FDataLink.Dataset.Active) then
    with FDataLink.Dataset.FieldByName(Value) do
      if not(DataType in [ftInteger, ftSmallInt, ftWord, ftAutoInc, ftFloat, ftBCD, ftFMTBcd]) then
        raise EInvalidFieldType.Create
          ('TZetaDBNumero.DataField can only be connected to columns of type ' +
          'Integer, SmallInt, Word, AutoInc, Float, BCD, FMTBcd.' + sLineBreak +
          Format('Returned field type was %s (Ordinal %d)', [FieldTypeNames[DataType],
          Ord(DataType)]));
end;

procedure TZetaDBNumero.DataChange( Sender: TObject );
begin
     if ( FDataLink.Field = nil ) then
        Valor := NULL_NUMBER
     else
         if FDataLink.Field.IsNull then
            Valor := NULL_NUMBER
         else
             case FDataLink.Field.DataType of
                  ftInteger, ftWord, ftSmallInt: Valor := TIntegerField( FDataLink.Field ).AsInteger;
                  ftFloat, ftBCD, ftFMTBcd: Valor := TFloatField( FDataLink.Field ).AsFloat;
             end;
end;

procedure TZetaDBNumero.EditingChange(Sender: TObject);
begin
     if Focused and FDataLink.Editing then
     begin
          SetMask( True );
          Enfocar;
     end;
end;

procedure TZetaDBNumero.UpdateData(Sender: TObject);
begin
     if ( FDataLink.Field <> nil ) then
     begin
          if GetNumberValue then
          begin
               try
                  case FDataLink.Field.DataType of
                       ftInteger, ftWord, ftSmallInt:
                       begin
                            with TIntegerField( FDataLink.Field ) do
                            begin
                                 if ( AsInteger <> ValorEntero ) then
                                 begin
                                      AsInteger := ValorEntero;
                                 end;
                            end;
                       end;
                       ftFloat, ftBCD, ftFMTBcd:
                       begin
                            with TFloatField( FDataLink.Field ) do
                            begin
                                 if ( AsFloat <> Valor ) then
                                 begin
                                      AsFloat := Valor;
                                 end;
                            end;
                       end;
                  end;
               except
                     on Error: Exception do
                     begin
                          Enfocar;
                          raise;
                     end;
               end;
          end
          else
          begin
               Enfocar;
               raise EDatabaseError.Create( '� N�mero Inv�lido !' );
          end;
     end;
end;

procedure TZetaDBNumero.ActiveChange( Sender: TObject );
begin
     if ( FDataLink <> nil ) and FDataLink.Active then
        CheckFieldType( DataField );
end;

procedure TZetaDBNumero.Change;
begin
     if ( FDataLink <> nil ) then
        FDataLink.Modified;
     inherited Change;
end;

function TZetaDBNumero.ValidEdit;
begin
     if ( FDataLink <> nil ) then
     begin
          if FDataLink.Editing then
             Result := True
          else
          begin
               if not FConfirmEdit or ConfirmaCambio( GetConfirmMsg ) then
                  Result := FDataLink.Edit
               else
                   Result := False;
          end;
     end
     else
         Result := True;
     if Result then
        Result := inherited ValidEdit;
end;

function TZetaDBNumero.SetNumberValue: Boolean;
begin
     if ( FDataLink <> nil ) and FDataLink.Editing then
     begin
          Result := inherited SetNumberValue;
          if Result then
          try
             with FDataLink do
             begin
                  UpdateRecord;
             end;
             Result := True;
          except
                on Error: Exception do
                   Application.HandleException( Error );
          end
          else
              HayError;
     end
     else
         Result := True;
end;

end.
