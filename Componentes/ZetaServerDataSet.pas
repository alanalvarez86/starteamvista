unit ZetaServerDataSet;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Db, DBClient, Mask,
  ActiveX, DSIntf, DBCommon, StdVcl,
  ZetaCommonLists, ZetaCommonClasses;

type
  TServerDataSet = class(TClientDataSet)
  private
    { Private declarations }
    function GetLista: OleVariant;
    procedure SetLista(const Value: OleVariant);
  protected
    { Protected declarations }
    procedure SetActive(Value: Boolean); override;
  public
    { Public declarations }
    procedure AddField(const sNombre: String; const Tipo: TFieldType; const iSize: Integer);
    property Lista: OleVariant read GetLista write SetLista;
  published
    { Published declarations }
    procedure AddFloatField(const sNombre: String);
    procedure AddIntegerField(const sNombre: String);
    procedure AddBooleanField(const sNombre: String);
    procedure AddDateField(const sNombre: String);
    procedure AddDateTimeField(const sNombre: String);
    procedure AddMemoField(const sNombre: String; const iSize: Integer);
    procedure AddBlobField(const sNombre: String; const iSize: Integer);
    procedure AddStringField(const sNombre: String; const iSize: Integer);
    procedure CreateTempDataset;
    procedure InitTempDataset;
  end;

implementation

{ ********* TZetaServerDataSet *************** }

procedure TServerDataSet.SetActive(Value: Boolean);
begin
  inherited SetActive(Value);
  if Active then
    LogChanges := False;
end;

procedure TServerDataSet.InitTempDataset;
begin
  Active := False;
  Filtered := False;
  Filter := '';
  IndexFieldNames := '';
  while FieldCount > 0 do begin
    Fields[FieldCount - 1].DataSet := nil;
  end;
  FieldDefs.Clear;
end;

procedure TServerDataSet.AddField(const sNombre: String; const Tipo: TFieldType;
  const iSize: Integer);
begin
  with FieldDefs.AddFieldDef do begin
    Name := sNombre;
    DisplayName := sNombre;
    DataType := Tipo;
    Size := iSize;
  end;
end;

procedure TServerDataSet.CreateTempDataset;
var
  i: Integer;
begin
  for i := 0 to (FieldDefs.Count - 1) do begin
    FieldDefs[i].CreateField(Self);
  end;
  CreateDataset;
end;

procedure TServerDataSet.AddStringField(const sNombre: String; const iSize: Integer);
begin
  AddField(sNombre, ftString, iSize);
end;

procedure TServerDataSet.AddFloatField(const sNombre: String);
begin
  AddField(sNombre, ftFloat, 0);
end;

procedure TServerDataSet.AddBlobField(const sNombre: String; const iSize: Integer);
begin
  AddField(sNombre, ftBlob, iSize);
end;

procedure TServerDataSet.AddBooleanField(const sNombre: String);
begin
  AddField(sNombre, ftBoolean, 0);
end;

procedure TServerDataSet.AddDateField(const sNombre: String);
begin
  AddField(sNombre, ftDate, 0);
end;

procedure TServerDataSet.AddDateTimeField(const sNombre: String);
begin
  AddField(sNombre, ftDateTime, 0);
end;

procedure TServerDataSet.AddIntegerField(const sNombre: String);
begin
  AddField(sNombre, ftInteger, 0);
end;

procedure TServerDataSet.AddMemoField(const sNombre: String; const iSize: Integer);
begin
  AddField(sNombre, ftMemo, 0);
end;

function TServerDataSet.GetLista: OleVariant;
begin
  Result := Self.Data;
end;

procedure TServerDataSet.SetLista(const Value: OleVariant);
begin
  InitTempDataset;
  Self.Data := Value;
end;

end.

