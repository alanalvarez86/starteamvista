unit ZetasClientReg;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, ComCtrls,
     ZetaDBGrid,
     ZetaDBTextBox,
     ZetaEdit,
     ZetaEdit_DevEx,
     ZetaKeyCombo,
     ZetaFecha,
     ZetaHora,
     ZetaNumero,
     ZetaSmartLists,
     ZetaSmartLists_DevEx,
     ZetaStateComboBox,
     ZetaClientDataset,
     ZetaWizard,
     ZetaKeyLookup,
     ZetaBanner,
     ZetaKeyLookup_DevEx,
     ZetaWizard_DevEx
     {$IFNDEF TRESS_DELPHIXE5_UP};
     {$ELSE},ZetaGridChart;{$ENDIF}

procedure Register;

implementation

procedure Register;
begin
     RegisterComponents( 'Tress Client', [TZetaDBGrid,
                                          TZetaTextBox,
                                          TZetaDBTextBox,
                                          TZetaEdit,
                                          TZetaEdit_DevEx,
                                          TZetaDBEdit,
                                          TZetaDBEdit_DevEx,
                                          TZetaKeyCombo,
                                          TZetaDBKeyCombo,
                                          TZetaFecha,
                                          TZetaDBFecha,
                                          TZetaHora,
                                          TZetaDBHora,
                                          TZetaNumero,
                                          TZetaDBNumero,
                                          TZetaSmartListsButton,
                                          TZetaSmartListsButton_DevEx,
                                          TZetaSmartLists,
                                          TZetaSmartLists_DevEx,
                                          TZetaSmartListBox,
                                          TZetaSmartListBox_DevEx,
                                          TZetaSpeedButton,
                                          TStateComboBox,
                                          TZetaClientDataset,
                                          TZetaLookupDataset,
                                          TZetaKeyLookup,
                                          TZetaDBKeyLookup,
                                          TZetaWizard,
                                          TZetaWizardButton,
                                          TZetaBanner,
                                          TZetakeyLookup_DevEx,
                                          TZetaDbKeyLookup_DevEx,
                                          TZetaWizard_DevEx,
                                          TZetaWizardButton_DevEx
                                          {$IFDEF TRESS_DELPHIXE5_UP},
                                          TZetaGridChart{$ENDIF}
                                          ] );
end;

end.
