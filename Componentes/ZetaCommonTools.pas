unit ZetaCommonTools;

interface

{$INCLUDE JEDI.INC}

uses SysUtils, Classes,
     {$ifndef DOTNET}
     Windows, Controls, Forms, Messages, Math,
     {$endif}
     MaskUtils, Variants,
     ZToolsPE,
     ZetaCommonClasses,
     ZetaCommonLists;

{$ifdef DOTNET}
type
    DWord = Integer;
{$endif}

     function MaxDay( const wYear, wMonth: Word ): Word;
     function DayIsValid( const wYear, wMonth, wDay: Word ): Boolean;
     function DaysToYears( const iDias: Integer ): Real;
     function DiaSemana( const dReferencia: TDateTime ): String ;
     function DiaDelMes( const dReferencia: TDateTime; const lPrimero: Boolean ): TDateTime;
     function FirstDayOfMonth( const dFecha: TDateTime ): TDateTime;
     function FirstDayOfBimestre( const dFecha: TDateTime ): TDateTime;
     function FirstDayOfQuincena( const dFecha: TDateTime ): TDateTime;
     function FirstDayOfLastQuincena( const dFecha: TDateTime ): TDateTime;
     function FirstDayOfYear( const wYear: Word ): TDateTime;

     function TheDay( const dFecha: TDateTime ): Word;
     function TheMonth( const dFecha: TDateTime ): Word;
     function TheYear( const dFecha: TDateTime ): Word;
     function YearToDays( const rYears: Real ): Integer;
     function DaysBetween( const dInicial, dFinal: TDate ): Integer;
     function zYearsBetween( const dInicial, dFinal: TDateTime ): Integer;
     function Years( const dInicial, dFinal: TDateTime ): Real;
     function DateTheYear( const dFecha: TDateTime; const iYear: Integer ): TDateTime;
     function NextDate( const dFecha, dReferencia: TDateTime ): TDateTime;
     function MesConLetraMes( const iValor: Integer ): String;
     function LastMonth( const dFecha: TDateTime ): TDateTime;
     function LastMonday( const dFecha: TDateTime ): TDateTime;
     function LastCorte( const dFecha: TDateTime; const iDOWCorte: Word ): TDateTime;
     function LastDayOfMonth( const dFecha: TDateTime ): TDateTime;
     function LastDayOfQuincena( const dFecha: TDateTime ): TDateTime;
     function LastDayOfLastQuincena( const dFecha: TDateTime ): TDateTime;
     function LastDayOfBimestre( const dFecha: TDateTime ): TDateTime;
     function LastDayOfYear( const wYear: Word ): TDateTime;
     function DateTheMonth( const dFecha: TDateTime; const iMonth: Word ): TDateTime;
     function Replicate( const sValor: String; iVeces: Word ): String;
     //function Tiempo( const dInicial, dFinal: TDateTime; const lDetalle: Boolean ): String;
     function Tiempo( const dInicial, dFinal: TDateTime; Detalle: eTiempoDias ): String;
     //function TiempoDias( rValor: Real; const lDetalle: Boolean ): String;
     function TiempoDias( rValor: Real; Detalle: eTiempoDias ): String;
     function LongTimeIsValid( const iTope: Integer; var sTime: String ): Boolean;
     function TimeIsValid( const iTope: Integer; var sTime: String ): Boolean;
     function FechaCorta( const dReferencia: TDateTime ): String;
     function FechaLarga( const dReferencia: TDateTime ): String;
     function FechaToStr( const dFecha: TDateTime ): String;
     function FechaAsStr( const dFecha: TDateTime ): String; // Formato dd/mm/yyy //
     function BoolToInt( const bValor: Boolean ): Integer;
     function IntToBool(const iNumero:Integer): Boolean;
     function zBoolToStr( const bValor: Boolean ): String;
     function BoolToSiNo( const bValor: Boolean ): String;
     function BoolAsSiNo( const bValor: Boolean ): string;
     function zStrToBool( const sValor: String ): Boolean;
     function CodificaYear( const iYear: Word ): Word;
     function CodificaFecha( const Year, Month, Day: Word ): TDate;
     function DateToStrSQL( const dFecha: TDateTime ): String;
     function DateToStrSQLC( const dFecha: TDateTime ): String;
     function DateToStrSQLClient( const dFecha: TDateTime ): String; // Formato YYYY-MM-DD
     function DateToStrSQLClientC( const dFecha: TDateTime ): String;// Formato YYYY-MM-DD
     function StrToFecha( const sFecha: String ): TDateTime;
     function StrAsFecha( sFecha: String ): TDateTime; // Formato DDMMYYYY � DDMMYY //
     function StrAsDate( sFecha: String ): TDateTime; // Formato MM/DD/YYYY
     function EsFechaValida( const sFecha: String; var dFecha: TDate ): Boolean;
     function MesBimestre( const iBimestre: Integer ): Integer;
     function EsBimestre( const iMes: Word ): Boolean;
     function NumeroBimestre( const iMes : integer ) : integer;
     function FechaDef( const dValor, dDefault: TDateTime): TDateTime;
     function Aniversario( dReferencia, dInicial, dFinal: TDate ): Boolean;
     function StrZero( const sValor: String; const iAncho: Integer ): String;
     function StrToZero( const rMonto: TPesos; const iAncho, iDecimales: Integer ): String;
     function StrToReal( const sValor: String ): Extended;
     function StrAsInteger( const sValor: String ): Integer;
     function StrTransform( sOrigen, sSearch, sReplace: String ): String;
     function StrTransAll( sOrigen, sSearch, sReplace: String; lOriginal: boolean = TRUE ): String;
     function StrCount( sOrigen, sSearch: String ): integer;//Busca el string sSearch en sOrigen y regresa cuantas veces se encontro.
     function StrLleno( const sText: String ): Boolean;
     function StrVacio( const sText: String ): Boolean;
     function StrLeft( const sValor: String; iLen: Integer): String;
     function StrRight( const sValor: String; iLen: Integer): String;
     function StrDef( const sValor, sDefault: string ) : string;
     function StrLlenoDef( const sValor, sDefault: string ) : string;
     function PadR( const sValor: String; iLength: Integer ): String;
     function PadL( const sValor: String; iLength: Integer ): String;
     function PadC( const sValor: String; iLength: Integer ): String;
     function PadRCar( const sValor: String; iLength: Integer; const cCar: Char  ): String;
     function PadLCar( const sValor: String; iLength: Integer; const cCar: Char ): String;

     function PadCCar( const sValor: String; iLength: Integer; const cCar: Char ): String;
     function MemoLine( const sMemo: String; const iAncho, iLinea: Integer ): String;
     function BorraCReturn( const sValor: String ): String;
     function MiRound( const rValor: Double; const iDecimales: Integer ): Currency;
     function Redondea( const rValor: TPesos ): TPesos;
     function iMax( const iValor1, iValor2: Integer ): Integer;
     function iMin( const iValor1, iValor2: Integer ): Integer;
     function rMax( const rValor1, rValor2: Real ): Real;
     function rMin( const rValor1, rValor2: Real ): Real;

     function aMinutos( const sValor: String ): Integer;
     function aHoras( const iValor: Integer ): Real;
     function aHoraString( const Minutos: Integer ): String;
     function aHora24( const sValor: String ): String;
     procedure SetHora24( var sHora: String; var dFecha: TDate; const eTipo: eTipoHorario; const iMaxSalida: Integer );
     function GetEntrada24( const eTipo: eTipoHorario; const sEntrada: TCodigo ): TCodigo;
     function GetSalida24( const eTipo: eTipoHorario; const sEntrada, sSalida: TCodigo ): TCodigo;
     function HoraAsStr( const Time:TDateTime; const sFormat : string = K_TIME_FORMAT ): string;
     function MaskHora( const sHora : string ): string; //Formatea un Texto de 1535 a 15:35
     function FormateaNumero( const iValor, iLength: Integer; const sValor : string ): String;
     function Space( const iVeces: Word ): String;
     function CortaUltimo( const sValor: String ): String;
     function PesosIguales( const rValor1, rValor2: Currency ): Boolean;
     function PesosIgualesP( const rValor1, rValor2: Currency; rPrecision: Currency ): Boolean;
     function ModReal( const rMonto, rEntre: Real ): Real;
     function Comillas( const sExpresion: String ): String;
     function EntreComillas( const sExpresion: String ): String;
     function Parentesis( const sExpresion: String ): String;
     function ConcatFiltros( const sFiltro, sExpresion: String ): String;
     function ConcatString( const sTexto, sExpresion, sSeparador: String ): String;
     function GetFilterRango( const sCampo, sCampo2, sInicial, sFinal: String ): String;
     function VerificaDir( const sDirectory: string; const sDelimitador: String = '\'): String;
     function VerificaDirURL( const sDirectory: String ): String;
     function ValidaHora( var sHora: String; const sTope: String ): Boolean;
     function ConvierteHora( const sHora: String ): String;
     function MontoConLetra( rMonto: TPesos; sMoneda: String; const lCentavos: Boolean ): String;
     procedure Environment;
     procedure Chicharra;
     function SetDateTimeText( const dValue: TDateTime; const lDate: Boolean ): String;
     function EsBajaNominaAnterior( const lActivo: Boolean; const iYear, iTipo, iNumero, iLimiteNomina : Integer; const dBaja: TDate; const oDatosPeriodo: TDatosPeriodo; const lCapturar: Boolean; var sMensaje: String ): Boolean;
     function IncluyeBaja( const iYear, iTipo, iNumero, iLimiteNomina : Integer; const dFecha: TDate; const DatosPeriodo: TDatosPeriodo ): Boolean; overload;
     function IncluyeBaja( const iYear, iTipo, iNumero, iLimiteNomina : Integer; const dFecha: TDate; const DatosPeriodo: TDatosPeriodo; const iTipoNomEmp: Integer; const lHuboCambioTNom: Boolean ): Boolean; overload;
     function RequiereBorrar( const Activo, Baja: TDatosPeriodo; const dIngreso, dBaja: TDate; const lActivo: Boolean; const TipoNomina: eTipoPeriodo; const iLimiteNomina : Integer; var sMensaje: String; const dCambioTNom: TDate ): Boolean;
     function GetExceptionInfo( Error: Exception ): String;
     function ShowNomina( const iYear, iTipo, iNumero: Integer ): String;

     function CalculaDigito( const sValor: String ): String;
     function ValidaDigito( const sValor: String ): Boolean;
     function ValidaCredito( sInfCred: String; var sSignos: String ): Boolean;

     function DatosJornadaToVariant( const Valor: TDatosJornada ): Variant;
     function DatosJornadaFromVariant( const Valor: Variant ): TDatosJornada;
     function DatosSalarioToVariant( const Valor: TDatosSalario ): Variant;
     function DatosSalarioFromVariant( const Valor: Variant ): TDatosSalario;
     function DatosClasificacionFromVariant( const Valor: Variant ): TDatosClasificacion;
     function DatosClasificacionToVariant( const Valor: TDatosClasificacion ): Variant;
     procedure SetVariantToNull( var Value: Variant );
     procedure SetOLEVariantToNull( var Value: OLEVariant );
     function QuitaAcentos( sValue : String ) : String;
     procedure LimpiaLista( oLista : TStrings );
     function GetArchivoCafeTemporal( const lExt: Boolean = TRUE; const sFolder: String = VACIO ) : string;
     procedure GuardaCafeTemporal( oLista : TStrings; const sFolder: String = VACIO );
     function PropperCase( const s : String ) : String;
     function GetCodigosASCII( const sCodigos: String ): String;
     function GetTempFile( const sPrefijo, sExt : string ): string;
     function QuitaComillas( const sTexto :string ): string;
     procedure InitDatosCalendario( oDatosCalendario : TCalendario );

     function CalcRFC( const sFirst, sLast, sNames: String; const dNacimiento: TDate ): String;
     function RFCVerifica( const sOriginal, sFirst, sLast, sNames: String; const dNacimiento: TDate ): String;
     function GetDigitoCURP( const sCurp: String ): String;
     function CalcCURP( const sFirst, sLast, sNames: String; const dNacimiento: TDate; const sSexo, sEntidad: String ): String;
     {*** US 9304: Validacion de RFC y CURP Capturados por el empleado ***}
     function ValidaRFC( sRFC: String; var sError: String ): Boolean;
     function ValidaCURP( sCurp: String; var sError: String ): Boolean;
     function GetDigitoRFC( const sRFC: String ): String;
     function GetNombreRaro( sNombre: String; var sError: String ): Boolean;
     //FIN

     function BitSet( const Value: Byte; const WhatBit: TBit ): Boolean;
     function BitOn( const Value: Byte; const WhatBit: TBit ): DWord;
     function BitOff( const Value: Byte; const WhatBit: TBit ): DWord;
     function BitToggle( const Value: Byte; const WhatBit: TBit ): DWord;

     function EsDigito( const cCaracter: Char ): Boolean;
     function EsLetra( const cCaracter: Char ): Boolean;
     function EsMinuscula( const cCaracter: Char ): Boolean;
     function EsMayuscula( const cCaracter: Char ): Boolean;
     function AMayuscula( const cCaracter: Char ): Char;

     function GetPeriodoInfo( const iYear, iNumero: Integer; const eTipo: eTipoPeriodo ): String;
     function GetMontosInfo(const iConcepto: integer; const sReferencia: string ): string;
     function GetFaltasInfo( const sDiaHora: string; const dFecha : TDate; const iMotivo : integer ): string;

     function GetImportedDateDia( const sData: String ): TDate;
     function GetImportedDateMes( const sData: String ): TDate;
     function GetImportedDateYear( const sData: String ): TDate;
     function GetFechaImportada( const sData: String; FormatoFecha: eFormatoImpFecha ): TDate;
     function DeclaracionAnualNombreCampo( const iPos, iGlobal: integer ): string;
     function PuedeCambiarTarjeta( const dInicial, dFinal, dLimite: TDate; const lTieneDerecho: Boolean; var sMensaje: string ): Boolean;
     function ValidaStatusNomina( const StatusActivo, StatusLimite: eStatusPeriodo ): Boolean;overload;
     function ValidaStatusNomina( const StatusActivo, StatusLimite: eStatusPeriodo; var sMensaje: string ): Boolean;overload;

     function GetDescripcionStatusPeriodo( statusPeriodo : eStatusPeriodo; statusTimbrado : eStatusTimbrado ) : string; overload;
     function GetDescripcionStatusPeriodo( periodo : TDatosPeriodo ) : string; overload;
     function QuitaCerosIzquierda( const sTexto: string ): string;
     function PreparaGafeteProximidad( const sTexto: string ): string;
     function SerialToWiegand( const sSerial: String ): String;
     function ClockAndDataToWiegand( const sClockAndData: String ): String;

     function Dias7Tipo( const eTipo: eClasifiPeriodo ): Currency;
     function ExtractJustExt(const sArchivo: string): string;

     function EsBajaenPeriodo( const dIngreso, dBaja: TDate; const oDatosPeriodo: TDatosPeriodo; var dFechaInicio, dFechaLimite: TDate ): Boolean;
     function EsCambioTNomPeriodo( const dCambioTipoNom: TDate; const oDatosPeriodo: TDatosPeriodo ): Boolean;
     function ValidaListaNumeros( const sLista: string ): Boolean;
     function AplicaRegla3X3( const DiaAplicaExento:eDiasAplicaExento;const StatusAusencia:eStatusAusencia;const TipoDia:eTipoDiaAusencia):Boolean;
     function ProgramacionEspecial( const eProgramacion : eProyectoEspecial; const sValor : string ): Boolean;
     function PosicionPrendida( const iPosicion : integer; const sValor : string ): Boolean;
    {$ifdef AUTOLIV}
    //function EncriptaWMLOCK( pCadena, pLlave: PChar; var lError: Boolean ): String;
    function EncriptaWMLOCK( sCadena, sLlave: string; var lError: Boolean ): String;
    {$endif}

     function ListaComas2TextoComillas( CommaTexto : string ) : string;
     function ListaComas2InQueryList( CommaTexto : string ) : string;
     function EsLista( CommaTexto : string ) : boolean;
     function ListaIntersectaConfidencialidad( sLista1, sLista2 : string ) : boolean;
     function ByteToKiloByte( const number: Int64 ): Int64;
     function KiloByteToMegaByte( const number: Int64 ): Int64;
     function MegaByteToGigaByte( const number: Int64 ): Currency;
     function ByteToGigaByte( const number: Int64 ): Currency;

     function RPos(SubStr: string; S: string): integer;
     function SetFileNameDefaultPath( const sFile: String ): String;     
     function SetFileNamePath( const sPath, sFile: String ): String;

     function scandate(const pattern:string;s:string;startpos:integer=1) : tdatetime;
     function ValidarEmail(email: string): boolean;
     function DentroBimestre(const dReIngreso, dBaja: TDate):Boolean;
     function GetIsWindowsVista: Boolean;
     // function GeneraMensajeHTML( var sAttach:string; ReemplazaVariables:TFunctionReemplazaVariables):String;
     function GeneraMensajeHTML( var sAttach:string; ReemplazaVariables:TFunctionReemplazaVariables; sPlantilla: String = VACIO):String;
     function GeneraMensajeHTMLLogin( var sAttach, sURLImagen:string; ReemplazaVariables:TFunctionReemplazaVariablesLogin; sPlantilla: String = VACIO):String;
     function GeneraMensajeHTMLError( var sAttach, sError, sNombreCalendario: string; ReemplazaVariablesError:TFunctionReemplazaVariablesError; sPlantilla: String = VACIO):String;
     function ParseCSVLine(ALine: string; AFields: TStringList) : TStringList;
{$define QUINCENALES}

implementation

uses
  StrUtils;

const
     C1 = 52845;
     C2 = 22719;
     aCentenares : array [1..9] of ZetaPChar = ( 'Ciento', 'Doscientos', 'Trescientos', 'Cuatrocientos', 'Quinientos', 'Seiscientos', 'Setecientos', 'Ochocientos', 'Novecientos' );
     aDecenas : array [3..9] of ZetaPChar = ('Treinta','Cuarenta','Cincuenta','Sesenta','Setenta','Ochenta','Noventa' );
     aUnidades : array [1..19] of ZetaPChar = ('Un','Dos','Tres','Cuatro','Cinco','Seis','Siete','Ocho','Nueve','Diez','Once','Doce','Trece','Catorce','Quince','Dieciseis','Diecisiete','Dieciocho','Diecinueve' );
     K_DOS_LETRAS = 2;
     K_ESPACIO = ' ';
     K_RELLENO = 'X';
     {V 2013. Actualizaci�n de la lista de palabras de acuerdo al documento InstructivoParaLaCurp_v2008.pdf  Bug SOP-4795}
     { K_OBSCENAS = 'BUEI|CACA|CAGA|CAKA|COGE|COJE|COJO|FETO|JOTO|KACO|KAGO|KOJO|KULO|LOCO|LOKO|MAMO|MEAS|MION|MULA|' +
                  'BUEY|CACO|CAGO|CAKO|COJA|COJI|CULO|GUEY|KACA|KAGA|KOGE|KAKA|LOCA|LOKA|MAME|MEAR|MEON|MOCO|PEDA|' +
                  'PEDO|PUTA|QULO|RUIN|PENE|PUTO|RATA'; }
      K_OBSCENAS = 'BACA|BAKA|BUEI|BUEY|CACA|CACO|CAGA|CAGO|CAKA|CAKO|COGE|COGI|COJA|COJE|COJI|' +
                   'COJO|COLA|CULO|FALO|FETO|GETA|GUEI|GUEY|JETA|JOTO|KACA|KACO|KAGA|KAGO|KAKA|' +
                   'KAKO|KOGE|KOGI|KOJA|KOJE|KOJI|KOJO|KOLA|KULO|LILO|LOCA|LOCO|LOKA|LOKO|MAME|' +
                   'MAMO|MEAR|MEAS|MEON|MIAR|MION|MOCO|MOKO|MULA|MULO|NACA|NACO|PEDA|PEDO|PENE|' +
                   'PIPI|PITO|POPO|PUTA|PUTO|QULO|RATA|ROBA|ROBE|ROBO|RUIN|SENO|TETA|VACA|VAGA|' +
                   'VAGO|VAKA|VUEI|VUEY|WUEI|WUEY';

      whitespace  = [' ',#13,#10];
      hrfactor    = 1/(24);
      minfactor   = 1/(24*60);
      secfactor   = 1/(24*60*60);
      mssecfactor = 1/(24*60*60*1000);

procedure Chicharra;
begin
     {$ifndef DOTNET}SysUtils.{$endif}Beep;
end;

function EntreComillas( const sExpresion: String ): String;
begin
     Result := UnaComilla + sExpresion + UnaComilla;
end;

function Comillas( const sExpresion: String ): String;
begin
     if ( sExpresion <> VACIO ) then
        Result := EntreComillas( sExpresion );
end;

function StrLeft( const sValor: String; iLen: Integer ): String;
begin
     Result:= Copy( sValor, 1, iLen );
end;

function StrRight( const sValor: String; iLen: Integer ): String;
begin
     if ( iLen >= Length( sValor ) ) then
        Result := sValor
     else
         Result:= Copy( sValor, ( Succ( Length( sValor ) ) - iLen ), iLen );
end;

function StrDef( const sValor, sDefault: string ) : string;
begin
     if StrVacio( sValor ) then
        Result := sDefault
     else Result := sValor; 
end;

function StrLlenoDef( const sValor, sDefault: string ) : string;
begin
     if StrLleno( sValor ) then
        Result := sDefault
     else Result := '';
end;

function FormateaNumero( const iValor, iLength: Integer; const sValor : string ): String;
begin
     Result := IntToStr( iValor );
     while ( Length( Result ) < iLength ) do
     begin
          Result := sValor + Result;
     end;
end;

function NumeroToStr( const iValor, iLength: Integer ): String;
begin
     Result := FormateaNumero( iValor, iLength, '0' );
end;

function PadCCar( const sValor: String; iLength: Integer; const cCar: Char ): String;
var
   iLengthValor: Integer;
begin
     iLengthValor := Length( sValor );
     Result := PadRCar( PadLCar( sValor, iLengthValor + Trunc( ( iLength - iLengthValor ) / 2 ), cCar ), iLength, cCar );
end;

function PadC( const sValor: String; iLength: Integer ): String;
begin
     Result := PadCCar( sValor, iLength, ' ' );
end;

function PadL( const sValor: String; iLength: Integer ): String;
begin
     Result := PadLCar( sValor, iLength, ' ' );
end;

function PadR( const sValor: String; iLength: Integer ): String;
begin
     Result := PadRCar( sValor, iLength, ' ' );
end;

function PadRCar( const sValor: String; iLength: Integer; const cCar: Char ): String;
begin
     Result := sValor;
     if ( Length( Result ) < iLength ) then
     begin
          while ( Length( Result ) < iLength ) do
          begin
               Result := Result + cCar;
          end;
     end
     else
         Result := Copy( Result, 1, iLength );
end;

function PadLCar( const sValor: String; iLength: Integer; const cCar: Char ): String;
var
   iLongitud: Integer;
begin
     Result := sValor;
     iLongitud := Length( Result );
     if ( iLongitud < iLength ) then
     begin
          while ( Length( Result ) < iLength ) do
          begin
                Result := cCar + Result;
          end;
     end
     else
         Result := Copy( Result, ( iLongitud - iLength + 1 ), iLength );
end;

function StrTransform( sOrigen, sSearch, sReplace: String ): String;
var
   i: Integer;
begin
     if {$ifdef DOTNET}CompareText{$else}AnsiCompareText{$endif}( sSearch , sReplace ) <> 0 then
     begin
          i := {$ifdef DOTNET}Pos{$else}AnsiPos{$endif}( {$ifdef DOTNET}UpperCase{$else}AnsiUpperCase{$endif}( sSearch ), {$ifdef DOTNET}UpperCase{$else}AnsiUpperCase{$endif}( sOrigen ) );
          if ( i > 0 ) then
          begin
               Delete( sOrigen, i, Length( sSearch ) );
               Insert( sReplace, sOrigen, i );
          end;
     end;
     Result := sOrigen;
end;

function StrTransAll( sOrigen, sSearch, sReplace: String; lOriginal: boolean = TRUE ): String;
var
   iPos, iLast, iSearch: Integer;
   sCopia : String;
begin
     Result  := '';
     if lOriginal then
     begin
          sSearch := {$ifdef DOTNET}UpperCase{$else}AnsiUpperCase{$endif}( sSearch );
          sCopia  := {$ifdef DOTNET}UpperCase{$else}AnsiUpperCase{$endif}( sOrigen );
     end
     else
         sCopia   := sOrigen;
     iSearch := Length( sSearch );
     iPos    := {$ifdef DOTNET}Pos{$else}AnsiPos{$endif}( sSearch, sCopia );
     while ( iPos > 0 ) do
     begin
          Result := Result + Copy( sOrigen, 1, iPos-1 ) + sReplace;
          iLast := iPos + iSearch;
          sCopia := Copy( sCopia, iLast, MAXINT );
          sOrigen := Copy( sOrigen, iLast, MAXINT );
          iPos := {$ifdef DOTNET}Pos{$else}AnsiPos{$endif}( sSearch, sCopia );
     end;
     Result := Result + sOrigen;
end;

function StrCount( sOrigen, sSearch: String ): integer;//Busca el string sSearch en sOrigen y regresa cuantas veces se encontro.
var
   iPos: Integer;

begin
     Result  := 0;
     iPos    := {$ifdef DOTNET}Pos{$else}AnsiPos{$endif}( sSearch, sOrigen );
     while ( iPos > 0 ) do
     begin
          Inc( Result );
          sOrigen := Copy( sOrigen, iPos+Length(sSearch)+1, MAXINT );
          iPos := {$ifdef DOTNET}Pos{$else}AnsiPos{$endif}(  sSearch, sOrigen );
     end;
end;


function TiempoDias( rValor: Real; Detalle: eTiempoDias ): String;
var
   iYears, iMeses, iDias: Integer;
begin
     if ( rValor = 0 ) then
        Result := 'Futuro'
     else
     begin
          iYears := Trunc( rValor / 365.25 );
          rValor := Round( ModReal( rValor, 365.25 ));
          if ( rValor = 365 ) then
          begin
               iYears := iYears + 1;
               rValor := 0;
          end;
          case iYears of
               1: Result := '1 A�o ';
               0: Result := '';
          else
              Result := IntToStr( iYears ) + ' A�os ';
          end;
          if (iYears= 0) OR (Detalle = etMeses) or (Detalle=etDias) then
          begin
               iMeses := Trunc( rValor / 30.4375 );
               if ( iMeses = 1 ) then
                    Result := Result + '1 Mes '
               else if ( iMeses > 1 ) then
                    Result := Result + IntToStr( iMeses ) + ' Meses ';
               if ((iYears = 0) AND (iMeses = 0)) OR (Detalle=etDias) then
               begin
                    iDias := Trunc( ModReal( rValor, 30.4375 ));
                    if ( iDias = 1 ) then
                       Result := Result + '1 D�a'
                    else if ( iDias > 1 ) then
                       Result := Result + IntToStr( iDias ) + ' D�as';
               end;
          end;
     end;
end;

{function TiempoDias( rValor: Real; const lDetalle: Boolean ): String;
var
   iYears, iMeses, iDias: Integer;
begin
     if ( rValor = 0 ) then
        Result := 'Futuro'
     else
     begin
          iYears := Trunc( rValor / 365.25 );
          rValor := Round( ModReal( rValor, 365.25 ));
          if ( rValor = 365 ) then
          begin
               iYears := iYears + 1;
               rValor := 0;
          end;
          case iYears of
               1: Result := '1 A�o ';
               0: Result := '';
          else
              Result := IntToStr( iYears ) + ' A�os ';
          end;
          iMeses := Trunc( rValor / 30.4375 );
          if ( iMeses = 1 ) then
               Result := Result + '1 Mes '
          else
              if ( iMeses > 1 ) then
	                Result := Result + IntToStr( iMeses ) + ' Meses ';
          if lDetalle then
          begin
               iDias := Trunc( ModReal( rValor, 30.4375 ) );
               if ( iDias = 1 ) then
                  Result := Result + '1 D�a'
               else
                   if ( iDias > 1 ) then
                      Result := Result + IntToStr( iDias ) + ' D�as';
          end;
     end;
end;

{function Tiempo( const dInicial, dFinal: TDateTime; const lDetalle: Boolean ): String;
begin
     if ( dInicial = 0 ) then
        Result := '      '
     else
         if ( dInicial > dFinal ) then
            Result := 'Futuro'
         else
             Result := TiempoDias( ( dFinal - dInicial + 1 ), lDetalle );
end; }

function Tiempo( const dInicial, dFinal: TDateTime; Detalle: eTiempoDias ): String;
begin
     if ( dInicial = 0 ) then
        Result := '      '
     else
         if ( dInicial > dFinal ) then
            Result := 'Futuro'
         else
             Result := TiempoDias( ( dFinal - dInicial + 1), Detalle );
end;


function LongTimeIsValid( const iTope: Integer; var sTime: String ): Boolean;
const
     K_VALOR = '%2.2d';
var
   iHoras, iMinutos, iSegundos: Word;
begin
     Result := False;
     iSegundos := StrToIntDef( Copy( sTime, 5, 2 ), 0 );
     if ( iSegundos in [ 0..59 ] ) then
     begin
          iMinutos := StrToIntDef( Copy( sTime, 3, 2 ), 0 );
          if ( iMinutos in [ 0..59 ] ) then
          begin
               iHoras := StrToIntDef( Copy( sTime, 1, 2 ), 0 );
               if ( iHoras < iTope ) then
               begin
                    sTime := Format( K_VALOR, [ iHoras ] ) +
                             Format( K_VALOR, [ iMinutos ] ) +
                             Format( K_VALOR, [ iSegundos ] );
                    Result := True;
               end;
          end;
     end;
end;

function TimeIsValid( const iTope: Integer; var sTime: String ): Boolean;
var
   iValue: Word;
   sValue: String;
begin
     Result := False;
     iValue := StrAsInteger( Copy( sTime, 3, 2 ) );
     if ( iValue in [ 0..59 ] ) then
     begin
          sValue := Copy( sTime, 1, 2 );
          sTime := sValue + NumeroToStr( iValue, 2 );
          iValue := StrAsInteger( sValue );
          if ( iValue < iTope ) then
          begin
               sTime := NumeroToStr( iValue, 2 ) + Copy( sTime, 3, 2 );
               Result := True;
          end;
     end;
end;

function DaysToYears( const iDias: Integer ): Real;
begin
     Result := iDias / 365.25;
end;

function YearToDays( const rYears: Real ): Integer;
begin
     Result := Trunc(rYears * 365.25);
end;

function DaysBetween( const dInicial, dFinal: TDate ): Integer;
begin
     if ( dFinal < dInicial ) then
        Result := 0
     else
        Result := Trunc( dFinal - dInicial + 1 );
end;

function zYearsBetween( const dInicial, dFinal: TDateTime ): Integer;
var
   iYear : Integer;
begin
     {$ifdef FALSE}
     Result := Trunc( DaysBetween( dInicial, dFinal ) / 365.25 );     // No era exacto el calculo en a�os bisiestos
     {$else}
     iYear := TheYear( dInicial ) + 1;
     Result := 0;
     while ( DateTheYear( dInicial, iYear ) <= dFinal ) do
     begin
          Result := Result + 1;
          Inc( iYear );
     end;
     {$endif}
end;

function Years( const dInicial, dFinal: TDateTime ): Real;
begin
     Result := DaysBetween( dInicial, dFinal ) / 365.25;
end;

function BoolToInt( const bValor: Boolean ): Integer;
begin
     if bValor then
        Result := 1
     else
         Result := 0;
end;

{Compara que el 1 es True y el 0 es False}
function IntToBool(const iNumero:Integer): Boolean;
begin
     Result :=( iNumero <> 0 );
end;

function zBoolToStr( const bValor: Boolean ): String;
begin
     if bValor then
        Result := K_GLOBAL_SI
     else
         Result := K_GLOBAL_NO;
end;

function zStrToBool( const sValor: String ): Boolean;
begin
     Result := ( sValor = K_GLOBAL_SI );
end;

function BoolToSiNo( const bValor: Boolean ): String;
begin
     if bValor then
        Result := K_BOOLEANO_SI
     else
         Result := K_BOOLEANO_NO;
end;

function BoolAsSiNo( const bValor: Boolean ): string;
begin
     if bValor then
        Result := K_PROPPERCASE_SI
     else
         Result := K_PROPPERCASE_NO;
end;

function CodificaYear( const iYear: Word ): Word;
const
     K_CENTURY = 1900;
     K_EPOCA = 30;
begin
     Result := iYear;
     if ( Result < K_EPOCA ) then
        Result := 100 + K_CENTURY + Result
     else
         if ( Result >= K_EPOCA ) and ( Result < 100 ) then
            Result := K_CENTURY + Result;
end;

function CodificaFecha( const Year, Month, Day: Word ): TDate;
const
     K_MIN_YEAR = 1900;
     K_MAX_YEAR = 2099;
var
   iYear: Word;
begin
     iYear := CodificaYear( Year );
     if ( Month in [ 1..12 ] ) and ( iYear >= K_MIN_YEAR ) and ( iYear <= K_MAX_YEAR ) and DayIsValid( iYear, Month, Day ) then
        Result := EncodeDate( iYear, Month, Day )
     else
         Result := NullDateTime;
end;

function TheYear( const dFecha: TDateTime ): Word;
var
   wYear, wMonth, wDay: Word;
begin
     DecodeDate( dFecha, wYear, wMonth, wDay );
     Result := wYear;
end;

function TheMonth( const dFecha: TDateTime ): Word;
var
   wYear, wMonth, wDay: Word;
begin
     DecodeDate( dFecha, wYear, wMonth, wDay );
     Result := wMonth;
end;

function TheDay( const dFecha: TDateTime ): Word;
var
   wYear, wMonth, wDay: Word;
begin
     DecodeDate( dFecha, wYear, wMonth, wDay );
     Result := wDay;
end;

function MaxDay( const wYear, wMonth: Word ): Word;
begin
     case wMonth of
          2: if IsLeapYear( wYear ) then
                Result := 29
             else
                 Result := 28;
          4, 6, 9, 11: Result := 30;
     else
         Result := 31;
     end;
end;

function DayIsValid( const wYear, wMonth, wDay: Word ): Boolean;
begin
     Result := ( wDay > 0 ) and ( wDay <= MaxDay( wYear, wMonth ) );
end;

function LastMonth( const dFecha: TDateTime ): TDateTime;
var
   wYear, wMonth, wDay: Word;
begin
     DecodeDate( dFecha, wYear, wMonth, wDay );
     if ( wMonth = 1 ) then
        Result := EncodeDate( ( wYear - 1 ), 12, wDay )
     else
         Result := DateTheMonth( dFecha, ( wMonth - 1 ) );
end;

function LastMonday( const dFecha: TDateTime ): TDateTime;
 var iDayOfWeek : integer;
begin
     iDayOfWeek := DayofWeek( dFecha );
     if ( iDayOfWeek = 1 ) then
        Result :=  ( dFecha - 6 )
     else
         Result := ( dFecha - iDayOfWeek + 2 );
end;

function LastCorte( const dFecha: TDateTime; const iDOWCorte: Word ): TDateTime;
begin
     Result := dFecha;
     while ( DayOfWeek( Result ) <> iDOWCorte ) do
     begin
          Result := ( Result - 1 );
     end;
end;

function FirstDayOfQuincena( const dFecha: TDateTime ): TDateTime;
 var
    wYear, wMonth, wDay: Word;
begin
     DecodeDate( dFecha, wYear, wMonth, wDay );
     if wDay <= 15 then wDay := 1
     else wDay := 16;
     Result := EncodeDate( wYear, wMonth, wDay );
end;

function LastDayOfQuincena( const dFecha: TDateTime ): TDateTime;
 var
    wYear, wMonth, wDay: Word;
begin
     DecodeDate( dFecha, wYear, wMonth, wDay );
     if wDay >= 16 then wDay := MaxDay(wYear,wMonth)
     else wDay := 15;
     Result := EncodeDate( wYear, wMonth, wDay );
end;

function FirstDayOfLastQuincena( const dFecha: TDateTime ): TDateTime;
 var
    wYear, wMonth, wDay: Word;
begin
     DecodeDate( dFecha, wYear, wMonth, wDay );
     if wDay <= 15 then
     begin
          wDay := 16;
          if wMonth = 1 then
          begin
               wMonth := 12;
               wYear := wYear -1;
          end
          else wMonth := wMonth -1;
          Result := EncodeDate( wYear, wMonth, wDay )
     end
     else Result := EncodeDate( wYear, wMonth, 1 );
end;

function LastDayOfLastQuincena( const dFecha: TDateTime ): TDateTime;
 var
    wYear, wMonth, wDay: Word;

begin
     DecodeDate( dFecha, wYear, wMonth, wDay );
     if wDay >= 16 then
        Result := EncodeDate( wYear, wMonth, 15 )
        //wDay := 15;
     else
         Result := LastDayOfMonth(LastMonth(dFecha));
end;

function LastDayOfBimestre( const dFecha: TDateTime ): TDateTime;
 var iMes : integer;
begin
     iMes := TheMonth(dFecha);
     if ( iMes mod 2 ) = 1 then
        Result := LastDayOfMonth(DateTheMonth( dFecha, iMes+1 ) )
     else
         Result := LastDayOfMonth(dFecha);
end;

function FirstDayOfBimestre( const dFecha: TDateTime ): TDateTime;
 var iMes : integer;
begin
     iMes := TheMonth(dFecha);
     if ( iMes mod 2 ) = 1 then
        Result := FirstDayOfMonth(dFecha)
     else
         Result := FirstDayOfMonth(DateTheMonth( dFecha, iMes-1 ) );
end;

function FirstDayOfMonth( const dFecha: TDateTime ): TDateTime;
var
   wYear, wMonth, wDay: Word;
begin
     DecodeDate( dFecha, wYear, wMonth, wDay );
     Result := EncodeDate( wYear, wMonth, 1 );
end;

function LastDayOfMonth( const dFecha: TDateTime ): TDateTime;
var
   wYear, wMonth, wDay: Word;
begin
     DecodeDate( dFecha, wYear, wMonth, wDay );
     Result := EncodeDate( wYear, wMonth, MaxDay( wYear, wMonth ) );
end;

function DateTheMonth( const dFecha: TDateTime; const iMonth: Word ): TDateTime;
var
   wYear, wMonth, wDay, wMaxDay: Word;
begin
     DecodeDate( dFecha, wYear, wMonth, wDay );
     wMaxDay := MaxDay( wYear, iMonth );
     if ( wDay > wMaxDay ) then
        wDay := wMaxDay;
     Result := EncodeDate( wYear, iMonth, wDay );
end;

function FirstDayOfYear( const wYear: Word ): TDateTime;
begin
     Result := EncodeDate( wYear, 1, 1 );
end;

function LastDayOfYear( const wYear: Word ): TDateTime;
begin
     Result := EncodeDate( wYear, 12, 31 );
end;

function DateTheYear( const dFecha: TDateTime; const iYear: Integer ): TDateTime;
var
   wYear, wMonth, wDay: Word;
begin
     if ( iYear > 0 ) then
     begin
          DecodeDate( dFecha, wYear, wMonth, wDay );
          if ( wMonth = 2 ) and ( wDay = 29 ) and not IsLeapYear( iYear ) then
             wDay := 28;
          Result :=  EncodeDate( iYear, wMonth, wDay );
     end
     else
         Result := dFecha;
end;

function NextDate( const dFecha, dReferencia: TDateTime ): TDateTime;
begin
     Result := DateTheYear( dFecha, TheYear( dReferencia ) );
     if ( Result > dReferencia ) then
             Result := DateTheYear( dFecha, TheYear( dReferencia ) - 1 );
end;

function DiaDelMes( const dReferencia: TDateTime; const lPrimero: Boolean ): TDateTime;
begin
     if lPrimero then
        Result := FirstDayOfMonth( dReferencia )
     else
         Result := LastDayOfMonth( dReferencia );
end;

function DiaSemana( const dReferencia: TDateTime ): String;
begin
     if ( dReferencia > 0 ) then
        Result := {$IFDEF DELPHIXE3_UP}FormatSettings.{$ENDIF}LongDayNames[ DayOfWeek( dReferencia ) ];
end;

function MesConLetraMes( const iValor: Integer ): String;
begin
     if ( iValor in [ 1..12 ] ) then
        Result := ZetaCommonLists.ObtieneElemento( lfMeses, ( iValor - 1 ) )
     else
         Result := 'Mes Inv�lido';
end;

function FechaCorta( const dReferencia: TDateTime ): String;
begin
     if ( dReferencia = 0 ) then
        Result := ' '
     else
         Result := FormatDateTime( 'dd/mmm/yy', dReferencia );
end;

function FechaLarga( const dReferencia: TDateTime ): String;
begin
     Result := FormatDateTime( 'dd" de "mmmm" de "yyyy', dReferencia );
end;

function FechaAsStr( const dFecha: TDateTime ): String; // Formato dd/mm/yyy //
begin
     Result := FormatDateTime( 'dd/mm/yyyy', dFecha );
end;

function FechaToStr( const dFecha: TDateTime ): String; // Formato YYYYMMDD //
begin
     Result := FormatDateTime( 'yyyymmdd', dFecha );
end;

function DateToStrSQL( const dFecha: TDateTime ): String;
begin
     Result := FormatDateTime( 'mm/dd/yyyy', dFecha );
end;

function DateToStrSQLC( const dFecha: TDateTime ): String;
begin
     Result := Comillas( DateToStrSQL( dFecha ) );
end;

function DateToStrSQLClient( const dFecha: TDateTime ): String;
begin
     Result := FormatDateTime( 'yyyy-mm-dd', dFecha );
end;

function DateToStrSQLClientC( const dFecha: TDateTime ): String;
begin
     Result := Comillas( DateToStrSQLClient( dFecha ) );
end;

function StrAsFecha( sFecha: String ): TDateTime; // Formato DDMMYYYY � DDMMYY //

function GetSiguiente: String;
var
   iPos: Word;
begin
     iPos := Pos( '/', sFecha );
     Result := Copy( sFecha, 0, ( iPos - 1 ) );
     sFecha := Copy( sFecha, ( iPos + 1 ), MaxInt );
end;

var
   sDia, sMes: String;
begin
     sDia := GetSiguiente;
     sMes := GetSiguiente;
     Result := CodificaFecha( StrAsInteger( sFecha ),
                              StrAsInteger( sMes ),
                              StrAsInteger( sDia ) );
end;


function StrAsDate( sFecha: String ): TDateTime; // Formato MM/DD/YYYY //

function GetSiguiente: String;
var
   iPos: Word;
begin
     iPos := Pos( '/', sFecha );
     Result := Copy( sFecha, 0, ( iPos - 1 ) );
     sFecha := Copy( sFecha, ( iPos + 1 ), MaxInt );
end;

var
   sDia, sMes: String;
begin
     sMes := GetSiguiente;
     sDia := GetSiguiente;
     Result := CodificaFecha( StrAsInteger( sFecha ),
                              StrAsInteger( sMes ),
                              StrAsInteger( sDia ) );
end;

function StrToFecha( const sFecha: String ): TDateTime; // Formato YYYYMMDD //
begin
     try
        Result := CodificaFecha( StrToIntDef( Copy( sFecha, 1, 4 ), 0 ),
                                 StrToIntDef( Copy( sFecha, 5, 2 ), 0 ),
                                 StrToIntDef( Copy( sFecha, 7, 2 ), 0 ) );
     except
           Result := NullDateTime;
     end;
end;

function EsFechaValida( const sFecha: String; var dFecha: TDate ): Boolean;
begin
     dFecha := StrAsFecha( sFecha ); //CV-Se manda llamar de varios lugares.
     Result := ( dFecha <> NullDateTime ); // EM: Ya no es un try..except
end;

function Aniversario( dReferencia, dInicial, dFinal: TDate ): Boolean;
var
   iYear: Word;
begin
     if ( dReferencia >= dFinal ) then
        Result := False
     else
     begin
          iYear := TheYear( dFinal );
          dReferencia := DateTheYear( dReferencia, iYear );
          if ( dReferencia > dFinal ) then
             dReferencia := DateTheYear( dReferencia, ( iYear - 1 ) );
          Result := ( dReferencia >= dInicial ) and ( dReferencia <= dFinal );
     end;
end;

function StrToZero( const rMonto: TPesos; const iAncho, iDecimales: Integer ): String;
begin
     if ( iDecimales > 0 ) then
        Result := FormatFloat( StringOfChar( '0', ( iAncho - iDecimales - 1 ) ) + '.' + StringOfChar( '0', iDecimales ), rMonto )
     else
         Result := FormatFloat( StringOfChar( '0', iAncho ), rMonto );;
     if ( Length( Result ) > iAncho ) then
     begin
          if ( iDecimales > 0 ) then
             Result := StringOfChar( '*', ( iAncho - iDecimales - 1 ) ) + '.' + StringOfChar( '*', iDecimales )
          else
              Result := StringOfChar( '*', iAncho );
     end;
     {
     if ( iDecimales = 0 ) then
        nMonto := Round( nMonto );
     cNumber := FloatToStr( nMonto );
     if ( Length( cNumber ) > nAncho ) then
        Result := Replicate( '*', nAncho )
     else
         Result := Replicate( '0', nAncho - Length( cNumber ) ) + cNumber;
     }
end;

function StrZero( const sValor: String; const iAncho: Integer ): String;
begin
     Result := Trim( sValor );
     while ( Length( Result ) < iAncho ) do
     begin
          Result := '0' + Result ;
     end;
end;

function StrToReal( const sValor: String ): Extended;
begin
     if ( Trim( sValor ) = '' ) then
        Result := 0
     else
     begin
          try
             Result := StrToFloat( Trim( sValor ) );
          except
                Result := 0;
          end;
     end;
end;

function StrAsInteger( const sValor: String ): Integer;
begin
     Result := StrToIntDef( Trim( sValor ), 0 );
end;

function MiRound( const rValor: Double; const iDecimales: Integer ): Currency;
const
     Factores: Array[ 0..5 ] of Integer = ( 1, 10, 100, 1000, 10000, 100000 );
begin
     Result := rValor * Factores[ iDecimales ];
     if ( Result <> 0 ) then
     begin
          if ( Result < 0 ) then
             Result := Result - 0.5
          else
              Result := Result + 0.5;
          Result := Int( Result ) / Factores[ iDecimales ];
     end;
end;

function Redondea( const rValor: TPesos ): TPesos;
begin
     Result := MiRound( rValor, 2 );
end;

function iMax( const iValor1, iValor2: Integer ): Integer;
begin
     if ( iValor1 > iValor2 ) then
        Result := iValor1
     else
         Result := iValor2;
end;

function iMin( const iValor1, iValor2: Integer ): Integer;
begin
     if ( iValor1 < iValor2 ) then
        Result := iValor1
     else
         Result := iValor2;
end;

function rMax( const rValor1, rValor2: Real ): Real;
begin
     if ( rValor1 > rValor2 ) then
        Result := rValor1
     else
         Result := rValor2;
end;

function rMin( const rValor1, rValor2: Real ): Real;
begin
     if ( rValor1 < rValor2 ) then
        Result := rValor1
     else
         Result := rValor2;
end;

function aMinutos( const sValor: String ): Integer;
begin
     Result := 0;
     if ( Length( sValor ) > 0 ) then
     begin
          Result := ( 60 * StrAsInteger( Copy( sValor, 1, 2 ) ) );
          if ( Length( sValor ) >= 4 ) then
             Result:= Result + StrAsInteger( Copy( sValor, 3, 2 ) );
     end;
end;

function aHoras( const iValor: Integer ): Real;
begin
     Result := MiRound( iValor / 60, 2 );
end;

function aHoraString( const Minutos: Integer ): String;
begin
     Result := NumeroToStr( Trunc( Minutos / 60 ), 2 ) + NumeroToStr( ( Minutos mod 60 ), 2 );
end;

function aHora24( const sValor: String ): String;
const
     K_24_HORAS = '24';
begin
     if ( sValor > K_24_HORAS ) then
        Result := aHoraString( aMinutos( sValor ) mod K_24HORAS )
     else
         Result := sValor;
end;

function GetEntrada24( const eTipo: eTipoHorario; const sEntrada: TCodigo ): TCodigo;
begin
     Result := sEntrada;
     if ( eTipo = thMediaNoche ) then
        Result := aHoraString( aMinutos( Result ) + K_24HORAS );
end;

function GetSalida24( const eTipo: eTipoHorario; const sEntrada, sSalida: TCodigo ): TCodigo;
begin
     Result := sSalida;
     if ( eTipo = thMediaNoche ) or ( sSalida < sEntrada ) then
        Result := aHoraString( aMinutos( Result ) + K_24HORAS );
end;

procedure SetHora24( var sHora: String; var dFecha: TDate; const eTipo: eTipoHorario; const iMaxSalida: Integer );
const
     K_24_HORAS = '24';
begin
     if ( sHora > K_24_HORAS ) then
     begin
          sHora:= aHoraString( aMinutos( sHora ) - K_24HORAS );

          if not( eTipo in [thMediaNoche, thMadrugadaSalida] ) then //Cuando es media noche o Entrada F.Salida no se cambia la fecha
             dFecha:= dFecha + 1;
     end
     else if ( eTipo in [thMadrugadaSalida,thMediaNoche] ) then
     begin
          if ( aMinutos(sHora) > iMaxSalida ) then  //Si la hora es mayor a la ultima madrugada: Es la entrada.
             dFecha:= dFecha - 1;
     end;
end;

function HoraAsStr( const Time:TDateTime; const sFormat : string ): string;
begin
     Result := FormatDateTime( sFormat, Time );
end;

function MaskHora( const sHora : string ): string;
begin
     Result := FormatMaskText( K_MASK_HORAS, sHora );
end;

procedure Environment; // Para Ajustar El Ambiente De Ejecucion //
var
   i: Integer;
begin
     {$IFDEF DELPHIXE3_UP}FormatSettings.{$ENDIF}DateSeparator := '/';
     {$IFDEF DELPHIXE3_UP}FormatSettings.{$ENDIF}ShortDateFormat := 'dd/mmm/yyyy';
     {$IFDEF DELPHIXE3_UP}FormatSettings.{$ENDIF}LongDateFormat  := 'dddd, dd MMMM, yyyy';
     for i := 1 to 12 do
     begin
          {$IFDEF DELPHIXE3_UP}FormatSettings.{$ENDIF}LongMonthNames[ i ] := ObtieneElemento( lfMeses, ( i - 1 ) );
          {$IFDEF DELPHIXE3_UP}FormatSettings.{$ENDIF}ShortMonthNames[ i ] := Copy( {$IFDEF DELPHIXE3_UP}FormatSettings.{$ENDIF}LongMonthNames[ i ], 1, 3 );
     end;
     for i := 1 to 7 do
     begin
          {$IFDEF DELPHIXE3_UP}FormatSettings.{$ENDIF}LongDayNames[ i ] := ObtieneElemento( lfDiasSemana, ( i - 1 ) );
          {$IFDEF DELPHIXE3_UP}FormatSettings.{$ENDIF}ShortDayNames[ i ] := Copy( {$IFDEF DELPHIXE3_UP}FormatSettings.{$ENDIF}LongDayNames[ i ], 1, 3 );
     end;
end;

function ModReal( const rMonto, rEntre: Real ): Real;
begin
     Result := rMonto - rEntre * Trunc( rMonto / rEntre );
end;

function StrLleno( const sText: String ): Boolean;
begin
     Result := ( Length( sText ) > 0 ) and ( Length( Trim( sText ) ) > 0 );
end;

function StrVacio( const sText: String ): Boolean;
begin
     Result := ( Length( sText ) = 0 ) or ( Length( Trim( sText ) ) = 0 );
end;

function Parentesis( const sExpresion: String ): String;
begin
     Result := '(' + sExpresion + ')';
end;

function ConcatFiltros( const sFiltro, sExpresion: String ): String;
begin
     Result := ConcatString( sFiltro, sExpresion, ' AND ' );
end;

function ConcatString( const sTexto, sExpresion, sSeparador: String ): String;
begin
     Result := sTexto;
     if StrLleno( sExpresion ) then
     begin
          if StrLleno( Result ) then
             Result := Result + sSeparador + sExpresion
          else
              Result := sExpresion;
     end;
end;

function GetFilterRango( const sCampo, sCampo2, sInicial, sFinal: String ): String;
begin
     if StrLleno( sInicial ) and StrLleno( sFinal ) then
        Result := sCampo +  '>=' + sInicial + ' AND ' +
                  sCampo2 + '<=' + sFinal
     else
         if StrLleno( sInicial ) then
            Result := sCampo + '>=' + sInicial
         else
             if StrLleno( sFinal ) then
                Result := sCampo + '<=' + sFinal
             else
                 Result := '';
end;

function ConvierteHora( const sHora: String ): String;
begin
     Result := Trim( sHora );
     Result := StrTransAll( sHora, K_ESPACIO, VACIO ); //Se quitan los espacios intermedios que pueda trae la hora
     case Length( Result ) of
           0: Result := '0000';
           1: Result := '0' + Result + '00';
           2: Result := Result + '00';
           3: Result := '0' + Result;
     end;
end;

function ValidaHora( var sHora: String; const sTope: String ): Boolean;
begin
     sHora := ConvierteHora( sHora );
     Result := ( Copy( sHora, 1, 2 ) < sTope ) and
               ( Copy( sHora, 3, 2 ) < '60' );
end;

function PesosIguales( const rValor1, rValor2: Currency ): Boolean;
begin
     Result := ( Abs( rValor1 - rValor2 ) < 0.01 );
end;

function PesosIgualesP( const rValor1, rValor2: Currency; rPrecision: Currency ): Boolean;
begin
     //if rPrecision = 0 then rPrecision := 0.01;
     Result := ( Abs( rValor1 - rValor2 ) <= rPrecision );
end;

function SetDateTimeText( const dValue: TDateTime; const lDate: Boolean ): String;
begin
     if ( dValue > 0 ) then
     begin
          if lDate then
             Result := FechaCorta( dValue )
          else
              Result := TimeToStr( dValue );
     end
     else
         Result := '';
end;

function BorraCReturn( const sValor: String ): String;
var  sCadena: String;
begin
     sCadena := StrTransAll( sValor, CR, ' ' );
     Result :=  StrTransAll( sCadena, LF, ' ' );
end;

function Replicate( const sValor: String; iVeces: Word ): String;
begin
     Result := '';
     while ( iVeces > 0 ) do
     begin
          Result := Result + sValor;
          iVeces := iVeces - 1;
     end;
end;

function Space( const iVeces: Word ): String;
begin
     Result := Replicate( ' ', iVeces );
end;

function CortaUltimo( const sValor: String ): String;
begin
     Result := Copy( sValor, 1, ( Length( sValor ) - 1 ) );
end;

function EsBajaNominaAnterior( const lActivo: Boolean; const iYear, iTipo, iNumero, iLimiteNomina : Integer;
                               const dBaja: TDate; const oDatosPeriodo: TDatosPeriodo;
                               const lCapturar: Boolean; var sMensaje: String ): Boolean;
const
     K_MENSAJE_BAJA = 'No es posible %s Excepciones para empleados dados de baja';
     aProcesoExcep: array[ FALSE..TRUE ] of ZetaPChar = ( 'importar', 'capturar' );
begin
     result := ( not lActivo );
     if Result then
     begin
          //oZetaProvider.InitGlobales;
          //if ( oDatosPeriodo.Numero < K_LIMITE_NOM_NORMAL ) then    // Ordinaria
          if ( oDatosPeriodo.Numero < iLimiteNomina ) then    // Ordinaria
             Result := ( not IncluyeBaja( iYear, iTipo, iNumero, iLimiteNomina, dBaja, oDatosPeriodo ) )
          else                               // Especial
             Result := ( not oDatosPeriodo.IncluyeBajas );
     end;
     if Result then
     begin
          sMensaje := Format( K_MENSAJE_BAJA, [ aProcesoExcep[lCapturar] ] );
          //if ( oDatosPeriodo.Numero < K_LIMITE_NOM_NORMAL ) then
          if ( oDatosPeriodo.Numero < iLimiteNomina ) then
             sMensaje := sMensaje + ' en per�odos anteriores';
     end;
end;

function IncluyeBaja( const iYear, iTipo, iNumero, iLimiteNomina : Integer; const dFecha: TDate; const DatosPeriodo: TDatosPeriodo ): Boolean;
begin
     with DatosPeriodo do
     begin
          if ( Uso = upOrdinaria ) then
          begin
               //if ( iNumero <= 0 ) or ( iNumero >= K_LIMITE_NOM_NORMAL ) or ( eTipoPeriodo( iTipo ) <> Tipo ) then
               if ( iNumero <= 0 ) or ( iNumero >= iLimiteNomina ) or ( eTipoPeriodo( iTipo ) <> Tipo ) then
                  Result := False
               else
                  Result := ( iYear > Year ) or ( ( iYear = Year ) and ( iNumero >= Numero ) );
          end
          else  { upEspecial }
              if ( SoloExcepciones ) then
                 Result := True
              else
                  if ( IncluyeBajas ) then { Incluye Bajas del mismo A�o }
                     Result := ( TheYear( dFecha ) = iYear )
                  else  { Solo Incluye los que es su Ultima Nomina }
                      Result := ( iYear = Year ) and ( eTipoPeriodo( iTipo ) = Tipo  ) and ( iNumero = Numero );
     end;
end;

function IncluyeBaja( const iYear, iTipo, iNumero, iLimiteNomina : Integer; const dFecha: TDate; const DatosPeriodo: TDatosPeriodo; const iTipoNomEmp: Integer; const lHuboCambioTNom: Boolean ): Boolean;
begin
     with DatosPeriodo do
     begin
          if ( Uso = upOrdinaria ) then
          begin
               { Si el tipo de n�mina de baja es diferente o no es la n�mina a la que pertenece el empleado no se calcula }
               if ( iNumero <= 0 ) or ( iNumero >= iLimiteNomina ) or ( ( eTipoPeriodo( iTipo ) <> Tipo ) and
                                                                        ( ( eTipoPeriodo(iTipoNomEmp) <> Tipo ) or ( not lHuboCambioTNom ) ) ) then
                  Result := False
               else
                  Result := ( iYear > Year ) or ( ( iYear = Year ) and ( ( iNumero >= Numero ) or ( lHuboCambioTNom ) )  );
          end
          else  { upEspecial }
              if ( SoloExcepciones ) then
                 Result := True
              else
                  if ( IncluyeBajas ) then { Incluye Bajas del mismo A�o }
                     Result := ( TheYear( dFecha ) = iYear )
                  else  { Solo Incluye los que es su Ultima Nomina }
                      Result := ( iYear = Year ) and ( eTipoPeriodo( iTipo ) = Tipo  ) and ( iNumero = Numero );
     end;
end;

function RequiereBorrar( const Activo, Baja: TDatosPeriodo; const dIngreso, dBaja: TDate; const lActivo: Boolean; const TipoNomina: eTipoPeriodo; const iLimiteNomina : Integer; var sMensaje: String; const dCambioTNom: TDate ): Boolean;
var
   lHuboCambioTNom: Boolean;
begin
     Result := True;

     with Activo do
     begin
          { Cambio de Tipo de n�mina a medio periodo: cuando existen reingresos el tipo de n�mina que viene es el de reingreso,
           �ste debe de ser igual al que se esta calculando. }
          lHuboCambioTNom:= ( ( dCambioTNom <> dIngreso ) ) and
                            ( dCambioTNom >= Inicio ) and
                            ( dCambioTNom <= Fin );


          if ( dIngreso > Fin ) then
             sMensaje := 'Fecha De Ingreso Posterior Al Per�odo'
          else {Se cambi� para que validara con ausencia su tipo de n�mina en caso de
                que tenga cambio de tipo de n�mina en el periodo. }

              if ( TipoNomina <> Tipo ) {and ( not lHuboCambioTNom )} then
                 sMensaje := 'Empleado Usa Otro Tipo De N�mina'
              else
                  Result := False;
     end;
     if not Result and not lActivo then
     begin
          with Baja do
          begin
               if not IncluyeBaja( Year, Ord( Tipo ), Numero, iLimiteNomina, dBaja, Activo, Ord(TipoNomina), lHuboCambioTNom  ) then
               begin
                    Result := True;
                    sMensaje := 'Empleado Dado de Baja Fuera del Per�odo';
               end;
          end;
     end;
end;


function MontoConLetra( rMonto: TPesos; sMoneda: String; const lCentavos: Boolean ): String;
var
   sMonto, sCentavos, sSigno, sCantidad, sFraccion, sExp: String;
   iExp, iOrden, iFraccion: Integer;
   lPlural, lMinusculas: Boolean;
begin
     if lCentavos then
     begin
          rMonto := Redondea( rMonto );
          sCentavos := ' ' + StrToZero( Round( ( rMonto - Int( rMonto ) ) * 100 ), 2, 0  ) + '/100';
     end
     else
     begin
          rMonto := Int( rMonto );
          sCentavos := '';
     end;
     if ( Length( sMoneda ) > 0 ) then
        sMoneda := ' ' + sMoneda;
     if ( rMonto < 0 ) then
     begin
          sSigno := 'Menos ';
          rMonto := Abs( rMonto );
     end
     else
          sSigno := '';
     if ( rMonto = 0 ) then
        sCantidad := 'Cero'
     else
     begin
          iExp := 0;
	         sCantidad := '';
          sMonto := IntToStr( Trunc( rMonto ) );
          iOrden := Trunc( ( Length( sMonto ) - 1 ) / 3 );
          if ( iOrden < 2 ) then
             iOrden := 0
          else
              if ( iOrden < 4 ) then
                 iOrden := 2
              else
                  iOrden := 4;
          while ( Length( sMonto ) > 0 ) do
          begin
               iFraccion := StrToInt( Copy( sMonto, ( Length( sMonto ) - 2 ), 3 ) );
               sMonto := Copy( sMonto, 1, ( Length( sMonto ) - 3 ) );
               sFraccion := '';
               lPlural	:= (iFraccion <> 1);
               lMinusculas := False;
               if ( iFraccion = 100 ) then    // 1er. Caso Especial //
               begin
                    sFraccion := 'Cien';
                    iFraccion := 0;
               end
               else
                   if ( iFraccion >= 100 ) then
                   begin
                        sFraccion := aCentenares[ Trunc( iFraccion / 100 ) ];
                        iFraccion := ( iFraccion mod 100 );
                        if ( iFraccion > 0 ) then
                           sFraccion := sFraccion + ' ';
                   end;
               if ( iFraccion >= 30 ) then
               begin
                    sFraccion := sFraccion + aDecenas[ Trunc( iFraccion / 10 ) ];
                    iFraccion := iFraccion mod 10;
                    if ( iFraccion > 0 ) then
                       sFraccion := sFraccion + ' y ';
               end
               else
                   if ( iFraccion >= 20 ) then
                   begin
                        if ( iFraccion = 20 ) then
                        begin
                             sFraccion := sFraccion + 'Veinte';
                             iFraccion := 0;
                        end
                        else
                        begin
                             sFraccion := sFraccion + 'Veinti';
                             iFraccion := iFraccion mod 10;
                             lMinusculas := True;
                        end;
                   end;
               if ( iFraccion > 0 ) then
               begin
                    if lMinusculas then
                       sFraccion := sFraccion + LowerCase( aUnidades[ iFraccion ] )
                    else
                        sFraccion := sFraccion + aUnidades[ iFraccion ];
               end;
               if ( Length( sFraccion ) > 0 ) or ( iExp >= iOrden ) then
               begin
                    if ( iExp > 0 ) then
                    begin
                         case iExp of
                              1, 3, 5: sExp := 'Mil';
                              2:
                              if lPlural then
                                 sExp := 'Millones'
                              else
                                  sExp := 'Mill�n';
                              4:
                              if lPlural then
                                 sExp := 'Billones'
                              else
                                  sExp := 'Bill�n';
                         end;
                         sFraccion := sFraccion + ' ' + sExp;
                    end;
                    sCantidad := sFraccion + ' ' + sCantidad;
               end;
               sCantidad := Trim( sCantidad );
               iExp := iExp + 1;
          end;
     end;
     Result := sSigno + sCantidad + sMoneda + sCentavos;
end;

function MemoLine( const sMemo: String; const iAncho, iLinea: Integer ): String;
begin
     // Temporal. Falta cortar palabras completas
     Result := Copy( sMemo, ( iAncho * ( iLinea - 1 ) ) + 1, iAncho );
end;

function ShowNomina( const iYear, iTipo, iNumero: Integer ): String;
begin
     if ( iTipo >= 0 ) then
     begin
          Result := ZetaCommonLists.ObtieneElemento( lfTipoNomina, iTipo ) + ' # ' + IntToStr( iNumero ) + ' de ' + IntToStr( iYear );
     end
     else
         Result := '';
end;

function CalculaDigito( const sValor: String ): String;
{ Usada en Cliente y Servidor para validar }
{ d�gito del # de Seguro Social }
var
   i, iFactor: Integer;
   rSuma, rDigito, rProducto: Real;
begin
     rSuma := 0;
     iFactor := 1;
     for i := 1 to 10 do
     begin
          rDigito := StrtoInt( Copy( sValor, i, 1 ) );
          rProducto := rDigito * iFactor ;
          if ( rProducto >= 10 ) then
             rProducto := rProducto - 9;
          rSuma := rSuma +  rProducto;
          if ( iFactor = 1 ) then
             iFactor := 2
          else
              iFactor := 1;
     end;
     rDigito := 10 - Trunc( rSuma ) mod 10;
     if ( rDigito = 10 ) then
        Result := '0'
     else
     begin
          {$WARNINGS OFF}
          Str( rDigito:1:0, Result );
          {$WARNINGS ON}
     end;
end;

function ValidaDigito( const sValor: String ): Boolean;
{ Usada en Cliente y Servidor para validar }
{ d�gito del # de Seguro Social }
begin
     Result := ( Copy( sValor, 11, 1 ) = CalculaDigito( sValor ) );
end;

function GetDigitoCredito( const sInfCred: String ): String;
{ Usada en Cliente y Servidor para validar }
{ Cr�dito de INFONAVIT }
var
   i, iPares, iNones, iValCaracter: Integer;
   rDigito: Real;
   lNon: Boolean;
{$ifdef TRESS_DELPHIXE5_UP}
   sDigito: ShortString;
   sCaracter: String;
{$else}
   sCaracter: String[ 1 ];
{$endif}
begin
     iPares  := 0;
     iNones  := 0;
     lNon := True;
     for i := 1 to 9 do
     begin
          sCaracter := Copy( sInfCred, i, 1 );
          iValCaracter := StrToInt( sCaracter );
	         if lNon then
             iNones := iNones + iValCaracter
          else
              iPares := iPares + iValCaracter;
          lNon := not lNon;
     end;
     iNones := iNones mod 10;
     iPares := iPares mod 10;
     rDigito := 2 * iNones + iPares + Trunc( Int( iNones / 5 ) );
     rDigito := Trunc( rDigito) mod 10;
     {$ifdef TRESS_DELPHIXE5_UP}
     Str( rDigito:1:0, sDigito );
     Result := String( sDigito );
     {$else}
     Str( rDigito:1:0, Result );
     {$endif}
end;

function ValidaCredito( sInfCred: String; var sSignos: String ): Boolean;
{ Usada en Cliente y Servidor para validar }
{ Cr�dito de INFONAVIT }
var
   rInfCred: Real;
   iCode: Integer;
begin
     sSignos := '';
     sInfCred := StrZero( sInfCred, 10 );
     Val( sInfCred, rInfCred, iCode );
     if ( iCode > 0 ) then
        sSignos := 'Contiene Caracteres Inv�lidos'
     else
{$ifdef FALSE}
         if ( ( Pos( '000', sInfCred ) = 1 ) or ( rInfCred = 0 ) ) then
            sSignos := 'VACIO o con CEROS'
         else
             if ( ZetaFunciones.CodificaYear( StrToInt( Copy( sInfCred, 1, 2 ) ) ) > ZetaFunciones.TheYear( Date ) ) then
                sSignos := 'Primeros 2 D�gitos > A�o'
             else
                 if ( Copy( sInfCred, 10, 1 ) <> GetDigitoCredito( sInfCred ) ) then
                    sSignos := 'D�gito Verificador';
{$else}
         if ( rInfCred = 0 ) then
            sSignos := 'VACIO o con CEROS'
         else
             if ( Copy( sInfCred, 10, 1 ) <> GetDigitoCredito( sInfCred ) ) then
                sSignos := 'D�gito Verificador';
{$endif}
     Result := StrVacio( sSignos );
end;

function DatosJornadaToVariant( const Valor: TDatosJornada ): Variant;
begin
     with Valor do
     begin
          Result := VarArrayOf( [ Codigo,
                                  Ord( TipoTurno ),
                                  Ord( TipoJornada ),
                                  Dias,
                                  Jornada,
                                  Dobles,
                                  Domingo,
                                  HorarioFestivo,
                                  TieneHorarioFestivo ] );
     end;
end;

function DatosJornadaFromVariant( const Valor: Variant ): TDatosJornada;
begin
     with Result do
     begin
          Codigo := Valor[ 0 ];
          TipoTurno := eTipoTurno( Valor[ 1 ] );
          TipoJornada := eTipoJornada( Valor[ 2 ] );
          Dias := Valor[ 3 ];
          Jornada := Valor[ 4 ];
          Dobles := Valor[ 5 ];
          Domingo := Valor[ 6 ];
          HorarioFestivo := Valor[ 7 ];
          TieneHorarioFestivo := Valor[ 8 ];
     end;
end;

function DatosClasificacionToVariant( const Valor: TDatosClasificacion ): Variant;
begin
     with Valor do
     begin
          Result := VarArrayOf( [ ZonaGeografica,
                                  Puesto,
                                  Clasificacion,
                                  Turno,
                                  Patron,
                                  Nivel1,
                                  Nivel2,
                                  Nivel3,
                                  Nivel4,
                                  Nivel5,
                                  Nivel6,
                                  Nivel7,
                                  Nivel8,
                                  Nivel9
                                  {$ifdef QUINCENALES}
                                  ,Salario
                                  {$endif}
                                  ,Nomina
                                  ,TablaPrestaciones
                                  {$ifdef ACS}
                                  ,Nivel10,Nivel11,Nivel12
                                  {$endif}
                                   ] );
     end;
end;

function DatosClasificacionFromVariant( const Valor: Variant ): TDatosClasificacion;
begin
     {$ifndef DOTNET}
     with Result do
     begin
          ZonaGeografica := Valor[ 0 ];
          Puesto := Valor[ 1 ];
          Clasificacion := Valor[ 2 ];
          Turno := Valor[ 3 ];
          Patron := Valor[ 4 ];
          Nivel1 := Valor[ 5 ];
          Nivel2 := Valor[ 6 ];
          Nivel3 := Valor[ 7 ];             
          Nivel4 := Valor[ 8 ];
          Nivel5 := Valor[ 9 ];
          Nivel6 := Valor[ 10 ];
          Nivel7 := Valor[ 11 ];
          Nivel8 := Valor[ 12 ];
          Nivel9 := Valor[ 13 ];
{$ifdef QUINCENALES}
          Salario := Valor[ 14 ]; 
{$endif}
          Nomina := Valor[ 15 ];
          TablaPrestaciones := Valor[ 16 ];
          {$ifdef ACS}
          Nivel10 := Valor[ 17 ];
          Nivel11 := Valor[ 18 ];
          Nivel12 := Valor[ 19 ];
          {$endif}
     end;
     {$endif}
end;

function DatosSalarioToVariant( const Valor: TDatosSalario ): Variant;
begin
     with Valor do
     begin
          Result := VarArrayOf( [ Integrado, Diario ] );
     end;
end;

function DatosSalarioFromVariant( const Valor: Variant ): TDatosSalario;
begin
     with Result do
     begin
          Integrado := Valor[ 0 ];
          Diario := Valor[ 1 ];
     end;
end;

procedure SetVariantToNull( var Value: Variant );
begin
     {$ifdef DOTNET}
     Value := Null;
     {$else}
     TVarData( Value ).VType := varNull;
     {$endif}
end;

procedure SetOLEVariantToNull( var Value: OLEVariant );
begin
     {$ifdef DOTNET}
     Value := Null;
     {$else}
     TVarData( Value ).VType := varNull;
     {$endif}
end;

function VerificaDir( const sDirectory: string; const sDelimitador: String = '\'): String;
begin
     if StrLleno( sDirectory ) and ( sDirectory[ Length( sDirectory ) ] <> sDelimitador ) then
        Result := sDirectory + sDelimitador
     else
         Result := sDirectory;
end;

function VerificaDirURL( const sDirectory: String ): String;
begin
     Result := VerificaDir( sDirectory, '/' );
end;

{ ******** Cliente ******* }

function GetExceptionInfo( Error: Exception ): String;
begin
     Result := Error.Message;
end;

function QuitaAcentos( sValue : String ) : String;
const
     LETRA_A_MINUSCULA = Ord( '�' );
     LETRA_E_MINUSCULA = Ord( '�' );
     LETRA_I_MINUSCULA = Ord( '�' );
     LETRA_O_MINUSCULA = Ord( '�' );
     LETRA_U_MINUSCULA = Ord( '�' );
     LETRA_ENE_MINUSCULA = Ord( '�' );
     LETRA_ENE_MAYUSCULA = Ord( '�' );
     LETRA_U_DIERESIS = Ord( '�' );
     LETRA_ADMIRACION = Ord( '�' );
     LETRA_PREGUNTA = Ord( '�' );
var
   i, iLen: Integer;
   cValue: Char;
begin
     iLen  := Length( sValue );
     Result := '';
     for i := 1 to iLen do
     begin
        cValue := sValue[ i ];
        case Ord( cValue ) of
               LETRA_A_MINUSCULA: cValue := 'a';
               LETRA_E_MINUSCULA: cValue := 'e';
               LETRA_I_MINUSCULA: cValue := 'i';
               LETRA_O_MINUSCULA: cValue := 'o';
               LETRA_U_MINUSCULA: cValue := 'u';
               LETRA_ENE_MINUSCULA: cValue := 'n';
               LETRA_ENE_MAYUSCULA: cValue := 'N';
               LETRA_U_DIERESIS: cValue := 'u';
               LETRA_ADMIRACION: cValue := '!';
               LETRA_PREGUNTA: cValue := '?';
        end;
        Result := Result + cValue;
     end;
end;

procedure LimpiaLista( oLista : TStrings );
 var i : Integer;
begin
    with oLista do
    begin
         for i := 0 to Count-1 do
             Objects[ i ].Free;
         Clear;
    end;
end;

function GetArchivoCafeTemporal( const lExt: Boolean = TRUE; const sFolder: String = VACIO ) : string;
begin
     Result := sFolder;
     {$ifndef DOTNET}
     if StrVacio( Result ) then
        Result := ExtractFileDir(Application.ExeName);
     {$endif}
     Result := Result + '\CF' + FormatDateTime( 'ddmmyy', Date );
     if lExt then
        Result := Result + '.DAT';
end;

procedure GuardaCafeTemporal( oLista : TStrings; const sFolder: String = VACIO );
var
   FArchivo : TStrings;
   sArchivo : string;
begin
     sArchivo := GetArchivoCafeTemporal( TRUE, sFolder );

     if FileExists(sArchivo) then
     begin
          FArchivo := TStringList.Create;
          try
             FArchivo.LoadFromFile(sArchivo);
             FArchivo.AddStrings(oLista);
             FArchivo.SaveToFile(sArchivo);
          finally
                 FArchivo.Free;
          end;
     end
     else oLista.SaveToFile(sArchivo);
end;

function PropperCase( const s : String ) : String;
var
    iPos, iMax : Integer;
    lEspacio : Boolean;
    cPos : Char;
begin
    // Paso todo a min�sculas
    Result := Trim( LowerCase( s ));
    iMax   := Length( Result );
    lEspacio := TRUE; // Para que la primera sea May�scula
    for iPos := 1 to iMax do
    begin
        cPos := Result[ iPos ];
        if ( cPos = ' ' ) then
            lEspacio := TRUE
        // Si la anterior es espacio, la pongo como May�scula
        else if ( lEspacio ) then
        begin
            Result[ iPos ] := UpperCase( cPos )[ 1 ];
            lEspacio := FALSE;
        end
        else    // Casos especiales donde LowerCase no las convierte
            case cPos of
                '�' : Result[ iPos ] := '�';
                '�' : Result[ iPos ] := '�';
                '�' : Result[ iPos ] := '�';
            end;
    end;
end;

function GetCodigosASCII(const sCodigos: String): String;
var
    Sub : string;
    Cod : Integer;
    P : Integer;
    CONST K_COMA= ',';
begin
     Sub := sCodigos;
     Result:= '';
     while Length(Sub) > 0 do
     begin
          P := Pos( K_COMA ,Sub);
          if P = 0 then // ES EL ULTIMO CODIGO
          begin
            Cod := StrToIntDef(Sub, 0 );
            Result:= Result + chr(Cod);
            Sub := '';
          end
          else
          begin // HAY MAS CODIGOS
            Cod   := StrToIntDef(Copy(Sub,1,P-1), 0 );
            Result:= Result + chr(Cod);
            Sub   := Copy(Sub,P+1,Length(Sub)-3);
          end;
     end;
end;

function GetTempFile( const sPrefijo, sExt : string ): string;
{$ifndef DOTNET}
var
   aDir,aArchivo : array[0..255] of {$ifdef TRESS_DELPHIXE5_UP}WideChar{$else}Char{$endif};
{$endif}
begin
     {$ifdef DOTNET}
     Result := '';
     {$else}
     GetTempPath(255, aDir);
     GetTempFileName( aDir,{$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(sPrefijo), 0,aArchivo );
     Result := {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(ChangeFileExt( aArchivo, '.'+ sExt ));
     DeleteFile( aArchivo );
     {$endif}
end;

function QuitaComillas( const sTexto :string ): string;
begin
     Result := StrTransAll( sTexto, COMILLA, '' );
     Result := StrTransAll( Result, UnaCOMILLA, '' );
end;

function EsBimestre( const iMes: Word ): Boolean;
begin
     Result := ( iMes in [ 2, 4, 6, 8, 10, 12 ] );
end;

function NumeroBimestre( const iMes : integer ) : integer;
begin
     Result := ( ( iMes -1 ) div 2 ) + 1;
end;

function FechaDef( const dValor, dDefault: TDateTime): TDateTime;
begin
     if ( dValor = NullDateTime ) then
          Result:= dDefault
     else
          Result:= dValor;
end;

function MesBimestre( const iBimestre: Integer ): Integer;
begin
     Result:= iBimestre * 2;
end;

procedure InitDatosCalendario( oDatosCalendario : TCalendario );
begin
     with oDatosCalendario do
     begin
          CalInicial:= VACIO;
          CalFinal:= VACIO;
          dCalInicial:= NullDateTime;
          dCalFinal:= NullDateTime;
          CalFiltro:= VACIO;
     end;
end;

function BitSet( const Value: Byte; const WhatBit: TBit ): Boolean;
begin
     Result:= ( ( Value and ( 1 shl WhatBit ) ) <> 0 );
end;

function BitOn( const Value: Byte; const WhatBit: TBit ): DWord;
begin
     Result := Value or ( 1 shl WhatBit );
end;

function BitOff( const Value: Byte; const WhatBit: TBit ): DWord;
begin
     Result := Value and ( ( 1 shl WhatBit ) xor $FF );
end;

function BitToggle( const Value: Byte; const WhatBit: TBit ): DWord;
begin
     Result := Value xor ( 1 shl WhatBit );
end;

function EsDigito( const cCaracter: Char ): Boolean;
begin
     {$ifdef DOTNET}{$WARNINGS OFF}{$endif}
     {$IFNDEF TRESS_DELPHIXE5_UP}
     Result := ( cCaracter in [ '0'..'9' ] );                          //( Caracter >= '0' ) and ( Caracter <= '9' );
     {$ELSE}
     Result := ( CharInSet (cCaracter, [ '0'..'9' ]) );                          //( Caracter >= '0' ) and ( Caracter <= '9' );
     {$ENDIF}
     {$ifdef DOTNET}{$WARNINGS ON}{$endif}
end;

function EsLetra( const cCaracter: Char ): Boolean;
begin
     Result := EsMinuscula( cCaracter ) or EsMayuscula( cCaracter );    //( ( Caracter >= 'a' ) and ( Caracter <= 'z' ) ) or  ( ( Caracter >= 'A' ) and ( Caracter <= 'Z' ) );
end;

function EsMinuscula( const cCaracter: Char ): Boolean;
begin
     {$ifdef DOTNET}{$WARNINGS OFF}{$endif}
     {$IFNDEF TRESS_DELPHIXE5_UP}
     Result := ( cCaracter in [ 'a'..'z' ] );
     {$ELSE}
     Result := ( CharInSet (cCaracter, [ 'a'..'z' ]) );
     {$ENDIF}
     {$ifdef DOTNET}{$WARNINGS ON}{$endif}
end;

function EsMayuscula( const cCaracter: Char ): Boolean;
begin
     {$ifdef DOTNET}{$WARNINGS OFF}{$endif}
     {$IFNDEF TRESS_DELPHIXE5_UP} 
     Result := ( cCaracter in [ 'A'..'Z' ] );
     {$ELSE} 
     Result := ( CharInSet (cCaracter, [ 'A'..'Z' ]) );
     {$ENDIF}
     {$ifdef DOTNET}{$WARNINGS ON}{$endif}
end;

function AMayuscula( const cCaracter: Char ): Char;
begin
     Result := Chr( Ord( cCaracter ) - 32 );
end;

function GetPeriodoInfo( const iYear, iNumero: Integer; const eTipo: eTipoPeriodo ): String;
begin
     Result := Format( '%s # %d del %d', [ ZetaCommonLists.ObtieneElemento( lfTipoPeriodo, Ord( eTipo ) ), iNumero, iYear ] );
end;

function GetMontosInfo(const iConcepto: integer; const sReferencia: string ): string;
begin
     Result := Format( 'Concepto #%d - Referencia: %s', [iConcepto, sReferencia] );
end;

function GetFaltasInfo( const sDiaHora: string; const dFecha : TDate; const iMotivo : integer ): string;
 var eLista : ListasFijas;
begin
     if sDiaHora = K_TIPO_DIA then
        eLista := lfMotivoFaltaDias
     else
         eLista := lfMotivoFaltaHoras;

     Result := Format( 'Motivo #%d: %s   Fecha: %s', [ iMotivo, ZetaCommonLists.ObtieneElemento( eLista, iMotivo ), FormatDateTime( 'dd/mmm/yyyy', dFecha) ] );
end;

{ Calcular RFC }
{
  MV: 08/Dic/2003
  Se Comento esta funci�n por la nueva implementaci�n del m�todo CalcRFC que calcula HomoClave
}
{
function RFC_COMPUESTA( const Nombre: String ): String;
begin
     Result := QuitaAcentos( Nombre );
     Result := Trim( UpperCase( Result ) );
     if ( Result = 'CH' ) or ( Result = 'LL' ) then
     	  Result := Copy( Result, 1, 1 ) + Trim( Copy( Result, 3, 100 ) );
     Result := ' ' + Result;
     Result := StrTransform( Result, ' DE ', ' ');
     Result := StrTransform( Result, ' EL ', ' ');
     Result := StrTransform( Result, ' LA ', ' ');
     Result := StrTransform( Result, ' LOS ', ' ');
     Result := StrTransform( Result, ' LAS ', ' ');
     Result := StrTransform( Result, ' Y ', ' ');
     Result := StrTransform( Result, ' AL ', ' ');
     Result := StrTransform( Result, ' DEL ', ' ');
     Result := StrTransform( Result, ' DELA ', ' ');
     Result := StrTransform( Result, ' DE ', ' ');
     Result := Trim( Result );
end;

function PrimeraVocal( const sValor: String ): String;
var
   i: Integer;
   bValor: Boolean;
   cValor: String[ 1 ];
begin
     i := 2;
     bValor := True;
     Result := Copy( sValor ,1 , 2 );
     while bValor and ( i <= Length( sValor ) )  do
     begin
          cValor := Copy( sValor, i, 1 ) ;
          if ( cValor = 'A' ) or ( cValor = 'E' ) or ( cValor = 'I' ) or ( cValor = 'O' ) or ( cValor = 'U' ) then
          begin
               Result := Copy( sValor, 1, 1 ) + cValor ;
               bValor := False;
          end;
          Inc( i );
     end;
end;

function CalcRFC( sPaterno, sMaterno, sNombre: String; dFecha: TDateTime ): String;
var
   Year, Month, Day: Word;
   sFecha: String;
begin
     if ( dFecha = 0 ) or ( Length( sPaterno ) = 0 ) then
        Result:= '             ';
     DecodeDate( dFecha, Year, Month, Day );
     sFecha := Copy( IntToStr( Year ), 3, 2 );
     if ( Month < 10 ) then
        sFecha := sFecha + '0' + IntToStr( Month )
     else
         sFecha := sFecha + IntToStr( Month );
     if ( Day < 10 ) then
        sFecha := sFecha + '0' + IntToStr( Day )
     else
         sFecha := sFecha + IntToStr( Day );
     sPaterno := RFC_COMPUESTA( sPaterno );
     sMaterno := RFC_COMPUESTA( sMaterno );
     sNombre := RFC_COMPUESTA( sNombre );
     if ( Pos( 'JOSE ', sNombre ) > 0 ) then
        Delete( sNombre, Pos( 'JOSE ', sNombre ), 5 )
     else
         if ( Pos( 'MARIA ',sNombre ) > 0 ) then
            Delete( sNombre, Pos( 'MARIA ', sNombre ), 6 );
     if ( Length( sPaterno ) = 0 ) or ( Length( sMaterno ) = 0 ) then
     begin
          sPaterno := sPaterno + sMaterno ;
          Result := PrimeraVocal( sPaterno ) + Copy( sNombre, 1, 2 ) + sFecha;
     end
     else
         if ( Length( sPaterno ) <= 2 ) then
            Result := Copy( sPaterno, 1 , 1 ) + Copy( sMaterno, 1 ,1 ) + Copy( sNombre, 1, 2 ) + sFecha
         else
             Result := PrimeraVocal( sPaterno ) + Copy( sMaterno, 1 ,1 ) + Copy( sNombre, 1, 1 ) + sFecha;
end;
}

{$ifdef FALSE}
********************************************************************************
*        PROGRAMA QUE GENERA EL RFC, HOMONIMIA Y DIGITO VERIFICADOR
********************************************************************************
PROCEDURE GENERFC
PARAMETERS PATERNO, MATERNO, NOMBRE, FECHA, RFC, LOGOKARF *ENTRADA---> PATERNO APELLIDO
*              MATERNO   "
*              NOMBRE  NOMBRE(S)
*              FECHANAC FECHA DE NACIMIENTO
*SALIDA   ---> RFC RFC DE LA PERSONA CON HOMONIMIA Y DIGITO VERIFICADOR
*  ---> LOGOKARFC  .T. SE GENERA RFC
LOGOKARF = .T.
DO CASE
 CASE LEN(TRIM(PATERNO))=0 .AND. LEN(TRIM(NOMBRE)) = 0
  LOGOKARF = .F.
 CASE LEN(TRIM(MATERNO))=0 .AND. LEN(TRIM(NOMBRE)) = 0
  LOGOKARF = .F.
 CASE LEN(TRIM(NOMBRE)) = 0
  LOGOKARF = .F.
 CASE FECHA = {}
  LOGOKARF = .F.
ENDCASE
IF .NOT. LOGOKARF
  RETURN
ENDIF

PALABRAS = "BUEI/CACA/CAGA/CAKA/COGE/COJE/COJO/FETO/JOTO/KACO/KAGO/KOJO/KULO/LOCO/LOKO/MAMO/MEAS/MION/MULA" +;
           "BUEY/CACO/CAGO/CAKO/COJA/COJI/CULO/GUEY/KACA/KAGA/KOGE/KAKA/LOCA/LOKA/MAME/MEAR/MEON/MOCO/PEDA" +;
           "PEDO/PUTA/QULO RUIN/PENE/PUTO/RATA"
VOCAL = "AEIOU"
TABLA1  = "   0 1 2 3 4 5 6 7 8 9 � � A B C D E F G H I J K L M N O P Q R S T U V W X Y Z &"
TABLA1V = "00000102030405060708091010111213141516171819212223242526272829323334353637383910"
TABLA2  = " 0 1 2 3 4 5 6 7 8 9101112131415161718192021222324252627282930313233"
TABLA2V = " 1 2 3 4 5 6 7 8 9 A B C D E F G H I J K L M N P Q R S T U V W X Y Z"
TABLA3  = " 0 1 2 3 4 5 6 7 8 9 A B C D E F G H I J K L M N � & � O P Q R S T U V W X Y Z  "
TABLA3V = "00010203040506070809101112131415161718192021222324242425262728293031323334353637"

ELIPA = PATERNO
ELIMA = MATERNO
ELINO = NOMBRE
DO ELIMINA WITH ELIPA
DO ELIMINA WITH ELIMA
DO ELIMINA WITH ELINO
STORE "" TO LUNO, LDOS, LTRES, LCUA
DO CARANOM WITH ELINO, LCUA
DO CASE
 CASE LEN(TRIM(ELIPA)) > 2 .AND. LEN(TRIM(ELIMA)) > 2
  LETRA1 = 2
  DO WHILE LETRA1 <= (LEN( TRIM(ELIPA)))
    IF ( AT( (SUBSTR(ELIPA,LETRA1,1)),VOCAL)) > 0
      LDOS = SUBSTR(VOCAL,(AT( (SUBSTR(ELIPA,LETRA1,1)),VOCAL)),1)
      EXIT
    ENDIF
    LETRA1 = LETRA1 + 1
  ENDDO
  LUNO = SUBSTR(ELIPA,1,1)
  LTRES= SUBSTR(ELIMA,1,1)
 CASE (LEN(TRIM(ELIPA)) > 0 .AND. LEN(TRIM(ELIPA)) <= 2 ) .AND. LEN(TRIM(ELIMA)) > 0
  LUNO = SUBSTR(ELIPA,1,1)
  LTRES= SUBSTR(ELIMA,1,1)
  LCUA = SUBSTR(ELINO,1,2)
 CASE LEN(TRIM(ELIPA)) = 0 .AND. LEN(TRIM(ELIMA)) > 0
  LDOS = SUBSTR(ELIMA,1,2)
  LCUA = SUBSTR(ELINO,1,2)
 CASE LEN(TRIM(ELIPA)) > 0 .AND. LEN(TRIM(ELIMA)) = 0
  LUNO = SUBSTR(ELIPA,1,2)
  LCUA = SUBSTR(ELINO,1,2)
ENDCASE
LRFC1 = LUNO + LDOS + LTRES + LCUA
IF MONTH(FECHA) < 10
  LMES = SUBSTR(STR(100+MONTH(FECHA),3,0),2,2)
 ELSE
     LMES = STR(MONTH(FECHA),2,0)
ENDIF
IF DAY(FECHA) < 10
  LDIA = SUBSTR(STR(100+DAY(FECHA),3,0),2,2)
 ELSE
     LDIA = STR(DAY(FECHA),2,0)
ENDIF
LANO = SUBSTR(STR(YEAR(FECHA),4,0),3,2)
LRFC2 = LANO + LMES + LDIA
IF AT(LRFC1,PALABRAS) > 0
  LRFC1 = SUBSTR(LRFC1,1,3) + "X"
ENDIF
LNOM = TRIM(PATERNO) + " " + TRIM(MATERNO) + " " + TRIM(NOMBRE)
LCADENA = "0"
LONG = LEN(LNOM)
LCON = 1
DO WHILE LCON <= LONG
  LAUX = " " + SUBSTR(LNOM,LCON,1)
  IF AT(LAUX,TABLA1) > 0
    LCADENA = LCADENA + SUBSTR(TABLA1V,AT(LAUX,TABLA1),2)
  ENDIF
  LCON = LCON + 1
ENDDO
LHASTA = LEN(LCADENA)
LSUMA = 0
I = 1
DO WHILE I <= (LHASTA-1)
  LSUMA = LSUMA + ( VAL(SUBSTR(LCADENA,I,2)) * VAL(SUBSTR(LCADENA,I+1,1)))
  I = I + 1
ENDDO
LDI = STR(LSUMA,15,0)
LDIVI = VAL( SUBSTR(LDI,13,3))
LCIENTE = INT(LDIVI/34)
LRESIDU = MOD(LDIVI,34)
IF AT(STR(LCIENTE,2,0),TABLA2) > 0
  LAUX = AT(STR(LCIENTE,2,0),TABLA2)
  LHOMO1 = LTRIM(SUBSTR(TABLA2V,LAUX,2))
ENDIF
IF AT(STR(LRESIDU,2,0),TABLA2) > 0
  LAUX = AT(STR(LRESIDU,2,0),TABLA2)
  LHOMO2 = LTRIM(SUBSTR(TABLA2V,LAUX,2))
ENDIF
LRFCX1 = LRFC1 + LRFC2 + TRIM(LHOMO1) + TRIM(LHOMO2)
J = 1
LTRECE = 14
STORE 0 TO LSUMA2
DO WHILE J <= 12
  LVALOR = 0
  LX = " " + SUBSTR(LRFCX1,J,1)
  IF AT(LX,TABLA3) > 0
    LAUX = AT(LX,TABLA3)
    LVALOR = VAL(SUBSTR(TABLA3V,LAUX,2))
  ENDIF
  LSUMA2 = LSUMA2 + ( LVALOR * (LTRECE-J))
  J = J + 1
ENDDO
LDIGITO = MOD(LSUMA2,11)
IF LDIGITO = 0
  LDIG = "0"
 ELSE
     LDIGITO = 11 - LDIGITO
     IF LDIGITO = 10
       LDIG = "A"
      ELSE
          LDIG = STR(LDIGITO,1,0)
     ENDIF
ENDIF
RFC = LRFCX1 + LTRIM(LDIG)
RETURN

**************************************
* File: DIFVER.PRG                   *
* Recovered by Valkyrie Version 2.07 *
**************************************

   clear
   close databases
   set index to
   set scoreboard off
   set color to B/W
   @ 0, 0 say " <ESC>  TERMINAR "
   set color to W+/GB
   @ 1, 0 to 4, 79 double
   @ 2, 1 clear to 3, 78
   @ 2, 11 say "        PROGRAMA PARA OBTENER LA HOMOCLAVE DEL RFC        "
   set color to B/GB
   @ 3, 20 say "CLAVE DIFERENCIADORA - DIGITO VERIFICADOR"
   @ 5, 0 clear to 24, 79
   @ 24, 75 say "JRM "
   set color to /B
   @ 5, 1 clear to 23, 78
   do while .t.
      set color to GB+/B,W+/W
      nom = space(45)
      rfc_var = space(10)
      @ 8, 4 say "RFC :              NOMBRE :"
      @ 8, 10 get rfc_var picture "XXXX999999"
      @ 8, 32 get nom
      read
      if readkey() = 12 .or. readkey() = 268
         set color to
         clear
         quit
      endif
      if rfc_var = space(13) .or. nom = space(45)
         loop
      endif
      rfc_var = ltrim(trim(upper(rfc_var)))
      nom = ltrim(trim(upper(nom)))
      cvedif = ""
      rfc_act = ""
      do clave_dif
      do digito_ver
      @ 12, 25 say "R.F.C.  ACTUAL :"
      set color to W+/B
      @ 12, 42 say rfc_act
      inkey(300)
      @ 12, 25 say space(30)
   enddo

********************************
procedure clave_dif

   i = 1
   do while i < len(nom) + 1
      letra = substr(nom,i,1)
      cve = ""
      do tabla_i
      cvedif = cvedif + cve
      i = i + 1
   enddo
   cvedif = "0" + cvedif
   total = 0
   i = 1
   do while i < len(cvedif)
      total = total + val(substr(cvedif,i,2)) * val(substr(cvedif,i + 1,1))
      i = i + 1
   enddo
   total = mod(total,1000)
   cvedif = int(total / 34)
   cve = ""
   do tabla_ii
   rfc_act = rfc_var + cve
   cvedif = mod(total,34)
   cve = ""
   do tabla_ii
   rfc_act = rfc_act + cve
   return

********************************
procedure digito_ver

   i = 1
   cvedif = ""
   total = 0
   do while i < len(rfc_act) + 1
      letra = substr(rfc_act,i,1)
      cve = 0
      do tabla_iii
      total = total + cve * (14 - i)
      i = i + 1
   enddo
   cve = mod(total,11)
   if cve = 1
      letra = "A"
   else
      if cve > 1
         letra = str(11 - cve,1)
      else
         letra = str(cve,1)
      endif
   endif
   rfc_act = rfc_act + letra
   return

********************************
procedure tabla_i

   do case
   case letra = " "
      cve = "00"
   case letra = "0"
      cve = "00"
   case letra = "1"
      cve = "01"
   case letra = "2"
      cve = "02"
   case letra = "3"
      cve = "03"
   case letra = "4"
      cve = "04"
   case letra = "5"
      cve = "05"
   case letra = "6"
      cve = "06"
   case letra = "7"
      cve = "07"
   case letra = "8"
      cve = "08"
   case letra = "9"
      cve = "09"
   case letra = "�" .or. letra = "�"
      cve = "10"
   case letra = "A" .or. letra = "�"
      cve = "11"
   case letra = "B"
      cve = "12"
   case letra = "C"
      cve = "13"
   case letra = "D"
      cve = "14"
   case letra = "E" .or. letra = "?" .or. letra = "�"
      cve = "15"
   case letra = "F"
      cve = "16"
   case letra = "G"
      cve = "17"
   case letra = "H"
      cve = "18"
   case letra = "I" .or. letra = "�"
      cve = "19"
   case letra = "J"
      cve = "21"
   case letra = "K"
      cve = "22"
   case letra = "L"
      cve = "23"
   case letra = "M"
      cve = "24"
   case letra = "N"
      cve = "25"
   case letra = "O" .or. letra = "�"
      cve = "26"
   case letra = "P"
      cve = "27"
   case letra = "Q"
      cve = "28"
   case letra = "R"
      cve = "29"
   case letra = "S"
      cve = "32"
   case letra = "T"
      cve = "33"
   case letra = "U" .or. letra = "�"
      cve = "34"
   case letra = "V"
      cve = "35"
   case letra = "W"
      cve = "36"
   case letra = "X"
      cve = "37"
   case letra = "Y"
      cve = "38"
   case letra = "Z"
      cve = "39"
   endcase
   return

********************************
procedure tabla_ii

   do case
   case cvedif = 0
      cve = "1"
   case cvedif = 1
      cve = "2"
   case cvedif = 2
      cve = "3"
   case cvedif = 3
      cve = "4"
   case cvedif = 4
      cve = "5"
   case cvedif = 5
      cve = "6"
   case cvedif = 6
      cve = "7"
   case cvedif = 7
      cve = "8"
   case cvedif = 8
      cve = "9"
   case cvedif = 9
      cve = "A"
   case cvedif = 10
      cve = "B"
   case cvedif = 11
      cve = "C"
   case cvedif = 12
      cve = "D"
   case cvedif = 13
      cve = "E"
   case cvedif = 14
      cve = "F"
   case cvedif = 15
      cve = "G"
   case cvedif = 16
      cve = "H"
   case cvedif = 17
      cve = "I"
   case cvedif = 18
      cve = "J"
   case cvedif = 19
      cve = "K"
   case cvedif = 20
      cve = "L"
   case cvedif = 21
      cve = "M"
   case cvedif = 22
      cve = "N"
   case cvedif = 23
      cve = "P"
   case cvedif = 24
      cve = "Q"
   case cvedif = 25
      cve = "R"
   case cvedif = 26
      cve = "S"
   case cvedif = 27
      cve = "T"
   case cvedif = 28
      cve = "U"
   case cvedif = 29
      cve = "V"
   case cvedif = 30
      cve = "W"
   case cvedif = 31
      cve = "X"
   case cvedif = 32
      cve = "Y"
   case cvedif = 33
      cve = "Z"
   endcase
   return

********************************
procedure tabla_iii

   do case
   case letra = "0"
      cve = 0
   case letra = "1"
      cve = 1
   case letra = "2"
      cve = 2
   case letra = "3"
      cve = 3
   case letra = "4"
      cve = 4
   case letra = "5"
      cve = 5
   case letra = "6"
      cve = 6
   case letra = "7"
      cve = 7
   case letra = "8"
      cve = 8
   case letra = "9"
      cve = 9
   case letra = "A"
      cve = 10
   case letra = "B"
      cve = 11
   case letra = "C"
      cve = 12
   case letra = "D"
      cve = 13
   case letra = "E"
      cve = 14
   case letra = "F"
      cve = 15
   case letra = "G"
      cve = 16
   case letra = "H"
      cve = 17
   case letra = "I"
      cve = 18
   case letra = "J"
      cve = 19
   case letra = "K"
      cve = 20
   case letra = "L"
      cve = 21
   case letra = "M"
      cve = 22
   case letra = "N"
      cve = 23
   case letra = "�"
      cve = 24
   case letra = "O"
      cve = 25
   case letra = "P"
      cve = 26
   case letra = "Q"
      cve = 27
   case letra = "R"
      cve = 28
   case letra = "S"
      cve = 29
   case letra = "T"
      cve = 30
   case letra = "U"
      cve = 31
   case letra = "V"
      cve = 32
   case letra = "W"
      cve = 33
   case letra = "X"
      cve = 34
   case letra = "Y"
      cve = 35
   case letra = "Z"
      cve = 36
   endcase
   return

* EOF
{$endif}

{ Funciones de uso interno para calcular RFC y CURP }

   function PrimeraVocal( const sValor: String ): String;
   var
      i: Integer;
      bValor: Boolean;
      {$ifdef TRESS_DELPHIXE5_UP}
      cValor: String;
      {$else}
      cValor: String[ 1 ];
      {$endif}
   begin
        i := 2;
        bValor := True;
        Result := Copy( sValor ,1 , 2 );
        while bValor and ( i <= Length( sValor ) )  do
        begin
             cValor := Copy( sValor, i, 1 ) ;
             if ( cValor = 'A' ) or ( cValor = 'E' ) or ( cValor = 'I' ) or ( cValor = 'O' ) or ( cValor = 'U' ) then
             begin
                  Result := Copy( sValor, 1, 1 ) + cValor ;
                  bValor := False;
             end;
             Inc( i );
        end;
   end;

   function AMayusculas( const sValue: String ): String;
   const
        LETRA_A_MINUSCULA = Ord( '�' );
        LETRA_E_MINUSCULA = Ord( '�' );
        LETRA_I_MINUSCULA = Ord( '�' );
        LETRA_O_MINUSCULA = Ord( '�' );
        LETRA_U_MINUSCULA = Ord( '�' );
        LETRA_U_DIERESIS = Ord( '�' );

        // Correcci�n de bug #11006: Homoclave de RFC diferente cuando se utiliza acentos en may�sculas.
        LETRA_A_MAYUSCULA = Ord( '�' );
        LETRA_E_MAYUSCULA = Ord( '�' );
        LETRA_I_MAYUSCULA = Ord( '�' );
        LETRA_O_MAYUSCULA = Ord( '�' );
        LETRA_U_MAYUSCULA = Ord( '�' );
        LETRA_U_DIERESIS_MAYUS = Ord( '�' );

        PUNTO = '.';
   var
      i, iLen: Integer;
      cValue: Char;
   begin
        iLen  := Length( sValue );
        Result := '';
        for i := 1 to iLen do
        begin
           cValue := sValue[ i ];
           case Ord( cValue ) of
                  LETRA_A_MINUSCULA: cValue := 'a';
                  LETRA_E_MINUSCULA: cValue := 'e';
                  LETRA_I_MINUSCULA: cValue := 'i';
                  LETRA_O_MINUSCULA: cValue := 'o';
                  LETRA_U_MINUSCULA: cValue := 'u';
                  LETRA_U_DIERESIS: cValue := 'u';

                  // Correcci�n de bug #11006: Homoclave de RFC diferente cuando se utiliza acentos en may�sculas.
                  LETRA_A_MAYUSCULA: cValue := 'a';
                  LETRA_E_MAYUSCULA: cValue := 'e';
                  LETRA_I_MAYUSCULA: cValue := 'i';
                  LETRA_O_MAYUSCULA: cValue := 'o';
                  LETRA_U_MAYUSCULA: cValue := 'u';
                  LETRA_U_DIERESIS_MAYUS: cValue := 'u';

           end;
           if ( cValue <> PUNTO ) then
              Result := Result + cValue;
        end;
        Result := {$ifdef DOTNET}UpperCase{$else}AnsiUpperCase{$endif}( Trim( Result ) );
   end;

   function SinMariaJose( const sNombre: String ): String; { QUITA MARIA o JOSE DEL NOMBRE }
   const
        K_JOSE = 'JOSE';
        K_MARIA = 'MARIA';
        K_MARIA_SHORT = 'MA';
   var
      sName, sTemp: String;
      iLength, iPos: Integer;
   begin
        sName := AMayusculas( sNombre );
        iLength := Length( sName );
        if ( ( ( sName = K_JOSE ) and ( iLength = Length( K_JOSE ) ) ) or
           ( ( sName = K_MARIA ) and ( iLength = Length( K_MARIA ) ) ) or
           ( ( sName = K_MARIA_SHORT ) and ( iLength = Length( K_MARIA_SHORT ) ) ) ) then
        begin
             { EL NOMBRE ES SOLO MARIA O JOSE }
             Result := sName;
        end
        else
        begin
             iPos := Pos( K_ESPACIO, sName );
             if ( iPos = 0 ) then
             begin
                  { EL NOMBRE NO ES COMPUESTO Y NO ES MARIA O JOSE }
                  Result := sName;
             end
             else
             begin
                  { Unicamente lo que est� antes del espacio }
                  sTemp := Copy( sName, 1, ( iPos - 1 ) );
                  if ( sTemp = K_JOSE ) or ( sTemp = K_MARIA ) or ( sTemp = K_MARIA_SHORT ) then
                  begin
                       Result := Copy( sName, ( iPos + 1 ), Length( sName ) - iPos );  { Lo que sigue de JOSE o MARIA }
                  end
                  else
                      Result := sTemp; { EL NOMBRE ES COMPUESTO Y LA PARTE INICIAL NO ES MARIA O JOSE }
             end;
        end;
   end;

   function CaraNom( const sNombre: String ): Char; { FORMACION DE LA PRIMERA LETRA DEL NOMBRE PARA EL RFC AUN COMPUESTO EL NOMBRE }
   var
      sName: String;
   begin
        sName := SinMariaJose( sNombre );
        if ( Length( sName ) > 0 ) then
           Result := sName[ 1 ]
        else
            Result := Char( 0 );
   end;

   function Elimina( const sNombre: String ): String; { ELIMINACION DE LOS ARTICULOS,PREPOSICIONES,CONJUNCIONES O CONTARCIONES DE LOS APELLIDOS PATERNO Y MATERNO }
   const
        aPreposiciones: array[ 0..5 ] of String = ( 'DE', 'LA', 'LOS', 'DEL', 'LAS', 'Y' );
   var
      sName, sTemp: String;
      i, iPos, iCtr, iLength: Integer;
      lFound: Boolean;
   begin
        sName := AMayusculas( sNombre );
        iLength := Length( sName );
        iCtr := 1;
        Result := VACIO;
        while ( iCtr < iLength ) do
        begin
             iPos := Pos( K_ESPACIO, sName );
             if ( iPos > 0 ) then
             begin
                  { Copia los caracteres anteriores al espacio }
                  sTemp := Copy( sName, 1, ( iPos - 1 ) );
                  { Elimina los caracteres anteriores al espacio }
                  sName := Copy( sName, ( iPos + 1 ), Length( sName ) - iPos );
                  lFound := False;
                  for i := Low( aPreposiciones ) to High( aPreposiciones ) do
                  begin
                       if ( sTemp = aPreposiciones[ i ] ) then
                       begin
                            lFound := True;
                            Break;
                       end;
                  end;
                  if lFound then
                  begin
                       iCtr := iCtr + Length( sTemp ) + 1;
                       if ( iCtr = iLength ) then
                          Result := sName;
                  end
                  else
                  begin
                       iCtr := iCtr + Length( sTemp );
                       Result := Result + sTemp + K_ESPACIO;
                  end;
             end
             else
             begin
                  if ( iCtr = 1 ) then
                  begin
                       Result := sName;
                       iCtr := iLength;
                  end
                  else
                  begin
                       Result := Result + sName;
                       iCtr := iCtr + Length( sName );
                  end;
             end;
        end;
        Result := Trim( Result );
   end;

   function EliminaCURP( const sNombre: String ): String; { ELIMINACION DE LOS ARTICULOS,PREPOSICIONES,CONJUNCIONES O CONTARCIONES DE LOS APELLIDOS PATERNO Y MATERNO }
   const
        aPreposiciones: array[ 0..9 ] of String = ( 'DE', 'LA', 'LOS', 'DEL', 'LAS', 'Y', 'MC' ,'MAC' ,'VON' ,'VAN' );
   var
      sName, sTemp: String;
      i, iPos, iCtr, iLength: Integer;
      lFound: Boolean;
   begin
        sName := AMayusculas( sNombre );
        iLength := Length( sName );
        iCtr := 1;
        Result := VACIO;
        while ( iCtr < iLength ) do
        begin
             iPos := Pos( K_ESPACIO, sName );
             if ( iPos > 0 ) then
             begin
                  { Copia los caracteres anteriores al espacio }
                  sTemp := Copy( sName, 1, ( iPos - 1 ) );
                  { Elimina los caracteres anteriores al espacio }
                  sName := Copy( sName, ( iPos + 1 ), Length( sName ) - iPos );
                  lFound := False;
                  for i := Low( aPreposiciones ) to High( aPreposiciones ) do
                  begin
                       if ( sTemp = aPreposiciones[ i ] ) then
                       begin
                            lFound := True;
                            Break;
                       end;
                  end;
                  if lFound then
                  begin
                       iCtr := iCtr + Length( sTemp ) + 1;
                       if ( iCtr = iLength ) then
                          Result := sName;
                  end
                  else
                  begin
                       iCtr := iCtr + Length( sTemp );
                       Result := Result + sTemp + K_ESPACIO;
                  end;
             end
             else
             begin
                  if ( iCtr = 1 ) then
                  begin
                       Result := sName;
                       iCtr := iLength;
                  end
                  else
                  begin
                       Result := Result + sName;
                       iCtr := iCtr + Length( sName );
                  end;
             end;
        end;
        Result := Trim( Result );
   end;

function CalcRFC( const sFirst, sLast, sNames: String; const dNacimiento: TDate ): String; { PROGRAMA QUE GENERA EL RFC, HOMONIMIA Y DIGITO VERIFICADOR }
const
     K_LETRA_A = 'A';
     //K_VOCALES = 'AEIOU';
{    ER: 2.6: Se pasaron a la unidad para usarlos en CURP
     K_DOS_LETRAS = 2;
     K_ESPACIO = ' ';
     K_RELLENO = 'X';
     K_OBSCENAS = 'BUEI|CACA|CAGA|CAKA|COGE|COJE|COJO|FETO|JOTO|KACO|KAGO|KOJO|KULO|LOCO|LOKO|MAMO|MEAS|MION|MULA|' +
                  'BUEY|CACO|CAGO|CAKO|COJA|COJI|CULO|GUEY|KACA|KAGA|KOGE|KAKA|LOCA|LOKA|MAME|MEAR|MEON|MOCO|PEDA|' +
                  'PEDO|PUTA|QULO|RUIN|PENE|PUTO|RATA';
}
     K_TABLA1  = '   0 1 2 3 4 5 6 7 8 9 � � A B C D E F G H I J K L M N O P Q R S T U V W X Y Z &';
     K_TABLA1V = '00000102030405060708091010111213141516171819212223242526272829323334353637383910';
     K_CERO = '0';
     K_MIL = 1000;
     K_TREINTA_Y_CUATRO = 34;
     K_CATORCE = 14;
     K_DOCE = 12;
     K_ONCE = 11;
     K_DIEZ = 10;
var
   lOk: Boolean;
   sPaterno, sMaterno, sNombres: String;
   sRFC1, sRFC2, sRFC3: String;
   sLetra1, sLetra2, sLetra3, sLetra4: String; //sLetra,
   iLenPaterno, iLenMaterno: Integer; //, iLetra1

   function TablaI( const sLetra: String ): String;
   begin
        if ( sLetra = ' ' ) then
           Result := '00'
        else if ( sLetra = '0' ) then
           Result := '00'
        else if ( sLetra = '1' ) then
           Result := '01'
        else if ( sLetra = '2' ) then
           Result := '02'
        else if ( sLetra = '3' ) then
           Result := '03'
        else if ( sLetra = '4' ) then
           Result := '04'
        else if ( sLetra = '5' ) then
           Result := '05'
        else if ( sLetra = '6' ) then
           Result := '06'
        else if ( sLetra = '7' ) then
           Result := '07'
        else if ( sLetra = '8' ) then
           Result := '08'
        else if ( sLetra = '9' ) then
           Result := '09'
        else if ( sLetra = '�' ) or ( sLetra = '�' ) then
           Result := '10'
        else if ( sLetra = 'A' ) or ( sLetra = '�' ) or ( sLetra = '�' ) then // Correcci�n de bug #11006: Homoclave de RFC diferente cuando se utiliza acentos en may�sculas.
           Result := '11'
        else if ( sLetra = 'B' ) then
           Result := '12'
        else if ( sLetra = 'C' ) then
           Result := '13'
        else if ( sLetra = 'D' ) then
           Result := '14'
        else if ( sLetra = 'E' ) or ( sLetra = '�' ) or ( sLetra = '�' ) then
           Result := '15'
        else if ( sLetra = 'F' ) then
           Result := '16'
        else if ( sLetra = 'G' ) then
           Result := '17'
        else if ( sLetra = 'H' ) then
           Result := '18'
        else if ( sLetra = 'I' ) or ( sLetra = '�' ) or ( sLetra = '�' ) then // Correcci�n de bug #11006: Homoclave de RFC diferente cuando se utiliza acentos en may�sculas.
           Result := '19'
        else if ( sLetra = 'J' ) then
           Result := '21'
        else if ( sLetra = 'K' ) then
           Result := '22'
        else if ( sLetra = 'L' ) then
           Result := '23'
        else if ( sLetra = 'M' ) then
           Result := '24'
        else if ( sLetra = 'N' ) then
           Result := '25'
        else if ( sLetra = 'O' ) or ( sLetra = '�' ) or ( sLetra = '�' ) then // Correcci�n de bug #11006: Homoclave de RFC diferente cuando se utiliza acentos en may�sculas.
           Result := '26'
        else if ( sLetra = 'P' ) then
           Result := '27'
        else if ( sLetra = 'Q' ) then
           Result := '28'
        else if ( sLetra = 'R' ) then
           Result := '29'
        else if ( sLetra = 'S' ) then
           Result := '32'
        else if ( sLetra = 'T' ) then
           Result := '33'
        else if ( sLetra = 'U' ) or ( sLetra = '�' ) or ( sLetra = '�' ) then // Correcci�n de bug #11006: Homoclave de RFC diferente cuando se utiliza acentos en may�sculas.
           Result := '34'
        else if ( sLetra = 'V' ) then
           Result := '35'
        else if ( sLetra = 'W' ) then
           Result := '36'
        else if ( sLetra = 'X' ) then
           Result := '37'
        else if ( sLetra = 'Y' ) then
           Result := '38'
        else if ( sLetra = 'Z' ) then
           Result := '39';
   end;

   function TablaII( const iClave: Integer ): String;
   begin
        case iClave of
             0: Result := '1';
             1: Result := '2';
             2: Result := '3';
             3: Result := '4';
             4: Result := '5';
             5: Result := '6';
             6: Result := '7';
             7: Result := '8';
             8: Result := '9';
             9: Result := 'A';
            10: Result := 'B';
            11: Result := 'C';
            12: Result := 'D';
            13: Result := 'E';
            14: Result := 'F';
            15: Result := 'G';
            16: Result := 'H';
            17: Result := 'I';
            18: Result := 'J';
            19: Result := 'K';
            20: Result := 'L';
            21: Result := 'M';
            22: Result := 'N';
            23: Result := 'P';
            24: Result := 'Q';
            25: Result := 'R';
            26: Result := 'S';
            27: Result := 'T';
            28: Result := 'U';
            29: Result := 'V';
            30: Result := 'W';
            31: Result := 'X';
            32: Result := 'Y';
            33: Result := 'Z';
        else
            Result := ' ';
        end;
   end;

   function TablaIII( const sLetra: String ): Integer;
   begin
        if ( sLetra = '0' ) then
           Result := 0
        else if ( sLetra = '1' ) then
           Result := 1
        else if ( sLetra = '2' ) then
           Result := 2
        else if ( sLetra = '3' ) then
           Result := 3
        else if ( sLetra = '4' ) then
           Result := 4
        else if ( sLetra = '5' ) then
           Result := 5
        else if ( sLetra = '6' ) then
           Result := 6
        else if ( sLetra = '7' ) then
           Result := 7
        else if ( sLetra = '8' ) then
           Result := 8
        else if ( sLetra = '9' ) then
           Result := 9
        else if ( sLetra = 'A' ) then
           Result := 10
        else if ( sLetra = 'B' ) then
           Result := 11
        else if ( sLetra = 'C' ) then
           Result := 12
        else if ( sLetra = 'D' ) then
           Result := 13
        else if ( sLetra = 'E' ) then
           Result := 14
        else if ( sLetra = 'F' ) then
           Result := 15
        else if ( sLetra = 'G' ) then
           Result := 16
        else if ( sLetra = 'H' ) then
           Result := 17
        else if ( sLetra = 'I' ) then
           Result := 18
        else if ( sLetra = 'J' ) then
           Result := 19
        else if ( sLetra = 'K' ) then
           Result := 20
        else if ( sLetra = 'L' ) then
           Result := 21
        else if ( sLetra = 'M' ) then
           Result := 22
        else if ( sLetra = 'N' ) then
           Result := 23
        else if ( sLetra = '�' ) then
           Result := 24
        else if ( sLetra = 'O' ) then
           Result := 25
        else if ( sLetra = 'P' ) then
           Result := 26
        else if ( sLetra = 'Q' ) then
           Result := 27
        else if ( sLetra = 'R' ) then
           Result := 28
        else if ( sLetra = 'S' ) then
           Result := 29
        else if ( sLetra = 'T' ) then
           Result := 30
        else if ( sLetra = 'U' ) then
           Result := 31
        else if ( sLetra = 'V' ) then
           Result := 32
        else if ( sLetra = 'W' ) then
           Result := 33
        else if ( sLetra = 'X' ) then
           Result := 34
        else if ( sLetra = 'Y' ) then
           Result := 35
        else if ( sLetra = 'Z' ) then
           Result := 36
        else
            Result := 0;
   end;

   function ClaveDif( const sNombre, sRFCParcial: String ): String;
   var
      i, iTope, iTotal: Integer;
      sLetra, sClave, sClaveDif: String;
   begin
        i := 1;
        iTope := Length( sNombre ) + 1;
        sClaveDif := K_CERO;
        while ( i < iTope ) do
        begin
             sLetra := Copy( sNombre, i, 1 );
             sClave := TablaI( sLetra );
             sClaveDif := sClaveDif + sClave;
             Inc( i );
        end;
        iTotal := 0;
        i := 1;
        iTope := Length( sClaveDif );
        while ( i < iTope ) do
        begin
             iTotal := iTotal + ( StrToIntDef( Copy( sClaveDif, i, 2 ), 0 ) * StrToIntDef( Copy( sClaveDif, ( i + 1 ), 1 ), 0 ) );
             Inc( i );
        end;
        iTotal := iTotal mod K_MIL;
        sClave := TablaII( iTotal div K_TREINTA_Y_CUATRO );
        Result := sClave;
        sClave := TablaII( iTotal mod K_TREINTA_Y_CUATRO );
        Result := Result + sClave;
   end;

   function DigitoVerificador( const sRFCParcial, sHomoClaveParcial: String ): String;
   var
      i, iTope, iTotal, iClave: Integer;
      sLetra, sValor: String;
   begin
        sValor := sRFCParcial + sHomoClaveParcial;
        iTotal := 0;
        i := 1;
        iTope := Length( sValor ) + 1;
        while ( i < iTope ) do
        begin
             sLetra := Copy( sValor, i, 1 );
             iClave := TablaIII( sLetra );
             iTotal := iTotal + ( iClave * ( K_CATORCE - i ) );
             Inc( i );
        end;
        iClave := ( iTotal mod K_ONCE );
        if ( iClave = 1 ) then
           sLetra := 'A'
        else
        begin
             if ( iClave > 1 ) then
                sLetra := Format( '%1.1d', [ ( K_ONCE - iClave ) ]  )
             else
                 sLetra := Format( '%1.1d', [ iClave ]  );
        end;
        Result := sHomoClaveParcial + sLetra;
   end;

begin
     Result := VACIO;
     { Validar que se tenga por lo menos el nombre y la fecha de Nacimiento }
     lOk := True;
     if ZetaCommonTools.StrVacio( sFirst ) and ZetaCommonTools.StrVacio( sNames ) then
        lOk := False
     else
         if ZetaCommonTools.StrVacio( sLast ) and ZetaCommonTools.StrVacio( sNames ) then
            lOk := False
         else
             if ZetaCommonTools.StrVacio( sNames ) then
                lOk := False
             else
                 if ( dNacimiento = NullDateTime ) then
                    lOk := False;
     if lOk then
     begin
          { Determinar la primera parte - Iniciales del Nombre }
          sPaterno := Elimina( sFirst );
          sMaterno := Elimina( sLast );
          sNombres := SinMariaJose( Elimina( sNames ) );
          sLetra1 := VACIO;
          sLetra2 := VACIO;
          sLetra3 := VACIO;
          sLetra4 := CaraNom( sNombres );
          iLenPaterno := Length( sPaterno );
          iLenMaterno := Length( sMaterno );
          if ( iLenPaterno > K_DOS_LETRAS ) and ( iLenMaterno > K_DOS_LETRAS ) then
          begin
               {
               iLetra1 := K_DOS_LETRAS;
               sLetra2 := Copy( sPaterno, 2, 1 );
               while ( iLetra1 <= iLenPaterno ) do
               begin
                    sLetra := Copy( sPaterno, iLetra1, 1 );
                    if ( Pos( sLetra, K_VOCALES ) > 0 ) then
                    begin
                         sLetra2 := sLetra;
                         Break;
                    end;
                    Inc( iLetra1 );
               end;
               sLetra1 := Copy( sPaterno, 1, 1 );
               }
               sLetra1 := PrimeraVocal( sPaterno );
               sLetra3 := Copy( sMaterno, 1, 1 );
          end
          else
              if ( iLenPaterno > 0 ) and ( iLenMaterno > 0 ) then  //No tienen mas de 2 letras pero mas de cero
              begin
                   if ( iLenPaterno <= K_DOS_LETRAS ) then
                   begin
                        sLetra1 := Copy( sPaterno, 1, 1 );
                        sLetra3 := Copy( sMaterno, 1, 1 );
                        sLetra4 := Copy( sNombres, 1, 2 );
                   end
                   else
                   begin
                        sLetra1 := PrimeraVocal( sPaterno );
                        sLetra3 := Copy( sMaterno, 1, 1 );
                   end
              end
              else
                  if ( ( iLenPaterno = 0 ) and ( iLenMaterno > 0 ) ) or
                     ( ( iLenPaterno > 0 ) and ( iLenMaterno = 0 ) ) then //Hay Paterno o Materno
                  begin
                       if ( ( iLenPaterno + iLenMaterno ) = 1 ) then //Suposici�n de Tress
                       begin
                            sLetra1 := PrimeraVocal( sPaterno + sMaterno );
                            sLetra2 := sLetra1;
                            sLetra4 := Copy( sNombres, 1, 2 );
                       end
                       else
                       begin
                            sLetra1 := PrimeraVocal( sPaterno + sMaterno );
                            sLetra4 := Copy( sNombres, 1, 2 );
                       end;
                  end;
          sRFC1 := sLetra1 + sLetra2 + sLetra3 + sLetra4;
          if ( Pos( sRFC1, K_OBSCENAS ) > 0 ) then
               sRFC1 := Copy( sRFC1, 1, 3 ) + K_RELLENO;
          { Determinar la parte intermedia - Fecha de Nacimiento }
          sRFC2 := FormatDateTime( 'yymmdd', dNacimiento );
          { Determinar la parte final - Homoclave }
          sNombres := Format( '%s %s %s', [ AMayusculas( sNames ), AMayusculas( sFirst ), AMayusculas( sLast ) ] );
          { Determina los primeros dos caracteres de la homoclave }
          sRFC3 := ClaveDif( sNombres, sRFC1 + sRFC2 );
          { Determina el d�gito verificador de la homoclave }
          sRFC3 := DigitoVerificador( sRFC1 + sRFC2, sRFC3 );
          { Ensambla el RFC Completo }
          Result := sRFC1 + sRFC2 + sRFC3;
     end;
end;

function RFCVerifica( const sOriginal, sFirst, sLast, sNames: String; const dNacimiento: TDate ): String;
const
     K_RFC_CON_HOMOCLAVE = 13;
     K_RFC_SIN_HOMOCLAVE = 10;
var
   sCalculado: String;
begin
     sCalculado := CalcRFC( sFirst, sLast, sNames, dNacimiento );
     { Si No se puede Calcular el RFC, se regresa el Original }
     if ZetaCommonTools.StrVacio( sCalculado ) then
        Result := sOriginal
     else
     begin
          {
          Si el RFC Original no tiene 13 caracteres ( AAAA-999999-XXX ),
          se regresa el Calculado ( el cual siempre la tiene )
          }
          if ( Length( sOriginal ) < K_RFC_CON_HOMOCLAVE ) then
             Result := sCalculado
          else
              {
              Si ambos RFC's coinciden en los primeros 10 caracteres, entonces
              se escoge el original para darle el beneficio del homoclave
              Como el calculado est� protegido contra obscenidades y tiene
              la fecha de nacimiento incorporada, tambi�n se valida esto
              }
              if ( Copy( sOriginal, 1, K_RFC_SIN_HOMOCLAVE ) = Copy ( sCalculado, 1, K_RFC_SIN_HOMOCLAVE ) ) then
                 Result := sOriginal
              else
                  Result := sCalculado;
     end;
end;

{ Calculo de CURP}

function GetDigitoCURP( const sCurp: String ): String;
var
{$ifdef TRESS_DELPHIXE5_UP}
   sCaracter: String;
   sDigito: ShortString;
{$else}
   sCaracter: String[ 1 ];
{$endif}
   i, iCaracter, iValCaracter: Integer;
   rDigito: Integer;
begin
     rDigito := 0;
     if Length(sCurp) > 15 then
     begin
          for i:= 1 to 17 do
          begin
               sCaracter := Copy( sCurp, i, 1 );
               iCaracter := Ord( sCaracter[ 1 ] );
               if ( iCaracter < 65 ) then
                  iValCaracter := StrToInt( sCaracter ) * ( 19 - i )
               else
                   if ( iCaracter >= 79 ) then
                      iValCaracter := ( iCaracter - 54 ) * ( 19 - i )
                   else
                       iValCaracter := ( iCaracter - 55 ) * ( 19 - i );
               rDigito := rDigito + iValCaracter;
          end;
     end;
     rDigito := 10 - ( Trunc( rDigito ) mod 10 );
     if ( rDigito = 10 ) then
        {$ifdef TRESS_DELPHIXE5_UP}sDigito := '0'{$else}Result := '0'{$endif}
     else
     {$ifdef TRESS_DELPHIXE5_UP}
         Str( rDigito:1, sDigito );
     Result := String( sDigito );
     {$else}
         Str( rDigito:1, Result );
     {$endif}
end;

function CalcRFCCURP( const sFirst, sLast, sNames: String; const dNacimiento: TDate ): String;
var
   sRFC: String;
   sPaterno, sMaterno, sNombres: String;
   sLetra1, sLetra2, sLetra3, sLetra4: String; //sLetra,
   iLenPaterno, iLenMaterno: Integer; //, iLetra1
begin
     {
      Si llega a conciliarse el algoritmo de RFC y CURP aqu� se invocaran ambos y se quitar� la homoclave
     }
     { Determinar la primera parte - Iniciales del Nombre }
     sPaterno := EliminaCURP( sFirst );
     sMaterno := EliminaCURP( sLast );
     sNombres := SinMariaJose( EliminaCURP( sNames ) );
     sLetra1 := VACIO;
     sLetra2 := VACIO;
     sLetra3 := VACIO;
     sLetra4 := CaraNom( sNombres );
     iLenPaterno := Length( sPaterno );
     iLenMaterno := Length( sMaterno );

     if ( iLenPaterno > K_DOS_LETRAS ) and ( iLenMaterno > K_DOS_LETRAS ) then
     begin
          sLetra1 := PrimeraVocal( sPaterno );
          sLetra3 := Copy( sMaterno, 1, 1 );
     end
     else if ( iLenPaterno > 0 ) and ( iLenMaterno > 0 ) then  //No tienen mas de 2 letras pero mas de cero
     begin
          if ( iLenPaterno <= K_DOS_LETRAS ) then
          begin
               sLetra1 := Copy( sPaterno, 1, 1 );
               sLetra2 := K_RELLENO;
               sLetra3 := Copy( sMaterno, 1, 1 );
          end
          else
          begin
               sLetra1 := PrimeraVocal( sPaterno );
               sLetra3 := Copy( sMaterno, 1, 1 );
          end
     end
     else if ( ( iLenPaterno = 0 ) and ( iLenMaterno > 0 ) ) or
             ( ( iLenPaterno > 0 ) and ( iLenMaterno = 0 ) ) then //Hay Paterno o Materno
     begin
          if ( ( iLenPaterno + iLenMaterno ) = 1 ) then //Suposici�n de Tress
          begin
               sLetra1 := PrimeraVocal( sPaterno + sMaterno );
               sLetra2 := K_RELLENO;
               sLetra3 := K_RELLENO;
          end
          else
          begin
               sLetra1 := PrimeraVocal( sPaterno + sMaterno );
               sLetra3 := K_RELLENO;
          end;
     end;
     sRFC := sLetra1 + sLetra2 + sLetra3 + sLetra4;

     if ( Pos( sRFC, K_OBSCENAS ) > 0 ) then    // Quitar palabras obscenas
          sRFC := Copy( sRFC, 1, 3 ) + K_RELLENO;

     Result := sRFC + FormatDateTime( 'yymmdd', dNacimiento );
end;

function CalcCURP( const sFirst, sLast, sNames: String; const dNacimiento: TDate;
                   const sSexo, sEntidad: String ): String;
const
     K_HOMOCLAVE = '0';
var
   lOk : Boolean;

   function ConvierteSexo( const sLetraSexo: String ): String; { Recibe 'F' = Femenino; 'M' = Masculino - As� est� almacenado en BD }
   begin
        if ( sLetraSexo = 'M' ) then
           Result := 'H'
        else if ( sLetraSexo = 'F' ) then
           Result := 'M'
        else
           Result := K_RELLENO;       // Si no se puede determinar Sexo regresar 'X'
   end;

   function GetConsonanteInterna( const sValue: String ): String;
   const
        K_VOCALES = 'AEIOU';
   var
      i, iLen: Integer;
      cValue: Char;
   begin
        Result := VACIO;
        iLen  := Length( sValue );
        if ( iLen > K_DOS_LETRAS ) then
        begin
             for i := 2 to iLen do
             begin
                  cValue := sValue[ i ];
                  if ( Pos( cValue, K_VOCALES ) <= 0 ) then
                  begin
                       Result := cValue;
                       Break;
                  end;
             end;
        end;
        if strVacio( Result ) or ( Result = '�' ) then     // La � no es un valor v�lido: Validado con NU�EZ, no se regresa la Z
           Result := K_RELLENO;     // Si no encuentra consonante en esa posici�n regresa 'X'
   end;

begin
     Result := VACIO;

     { Validar que se tenga por lo menos el nombre, la fecha de Nacimiento, Sexo y Entidad de Nacimiento }
     lOk := TRUE;
     if ZetaCommonTools.StrVacio( sFirst ) and ZetaCommonTools.StrVacio( sNames ) then
        lOk := FALSE
     else if ZetaCommonTools.StrVacio( sLast ) and ZetaCommonTools.StrVacio( sNames ) then
        lOk := FALSE
     else if ZetaCommonTools.StrVacio( sNames ) then
        lOk := FALSE
     else if ( dNacimiento = NullDateTime ) then
        lOk := FALSE
     else if strVacio( sSexo ) then
        lOk := FALSE
     else if strVacio( sEntidad ) then
        lOk := FALSE;

     if lOk then
     begin
          Result := CalcRFCCURP( sFirst, sLast, sNames, dNacimiento ) +   // Parte que debiera ser como RFC
                    ConvierteSexo( sSexo ) + PadLCar( sEntidad, 2, K_RELLENO ) +
                    GetConsonanteInterna( EliminaCURP( sFirst ) ) +
                    GetConsonanteInterna( EliminaCURP( sLast ) ) +
                    GetConsonanteInterna( SinMariaJose( EliminaCURP( sNames ) ) ) +
                    K_HOMOCLAVE;
          // Obtener Digito
          Result := Result + GetDigitoCURP( Result );
     end;
end;

{*** US 9304: Validacion de RFC y CURP Capturados por el empleado ***}
function GetNombreRaro( sNombre: String; var sError: String ): Boolean;
var
   i: Integer;
{$ifdef TRESS_DELPHIXE5_UP}
   sLetra: String;
{$else}
   sLetra: String[ 1 ];
{$endif}
begin
     { � Acepta � ? � Acepta ' ' ? }
     sNombre := UpperCase( Trim( sNombre ) );
     sError := '';
     for  i := 1 to Length( sNombre ) do
     begin
          sLetra := Copy( sNombre, i, 1 );
          if not ( ( sLetra >= 'A' )  and ( sLetra <= 'Z' ) or ( sLetra = ' ' ) or ( sLetra = '�' ) ) then
             sError := sError + sLetra;
     end;
     Result := StrLleno( sError );
end;

function GetDigitoRFC( const sRFC: String ): String;
var
   sCaracter: String;
   i, iCaracter, iValCaracter: Integer;
   rDigito: Real;
   {$ifdef TRESS_DELPHIXE5_UP}
   sDigito: ShortString;
   {$endif}
begin
     rDigito := 0;
     for i := 1 to 12 do
     begin
          sCaracter := Copy( sRFC, i , 1 );
          iCaracter := Ord( sCaracter[ 1 ] );
       	  if ( iCaracter < 65 ) then
          begin
               iValCaracter := StrToInt( sCaracter );
               if ( iValCaracter > 0 ) then
                  rDigito := rDigito + ( iValCaracter * ( 14 - i ) )
             else
                 if ( sCaracter = ' ' ) then
                    rDigito := rDigito + ( 37 * ( 14 - i ) )
                 else
                     rDigito := rDigito + ( 99 * ( 14 - i ) );
          end
          else
          begin
               if ( iCaracter > 78 ) then
                  iCaracter := iCaracter - 54
               else
                   iCaracter := iCaracter - 55;
               rDigito := rDigito + ( iCaracter * ( 14 - i ) );
          end;
      end;
      rDigito := 11 - ( Trunc( rDigito ) mod 11 );
      if ( rDigito = 10 ) then
         {$ifdef TRESS_DELPHIXE5_UP}sDigito := 'A'{$else}Result := 'A'{$endif}
      else
          if ( rDigito = 11 ) then
             {$ifdef TRESS_DELPHIXE5_UP}sDigito := '0'{$else}Result := '0'{$endif}
          else
           {$ifdef TRESS_DELPHIXE5_UP}
              STR( rDigito:1:0, sDigito );
              Result := String( sDigito );
           {$else}
              STR( rDigito:1:0, Result );
           {$endif}
end;

function ValidaRFC( sRFC: String; var sError: String ): Boolean;
begin
     sError := '';
     sRFC := Trim( UpperCase( sRFC ) );
     if StrVacio( sRFC ) then
        sError := 'R.F.C. no puede quedar vac�o'
     else
         if Pos( ' ', sRFC ) > 0 then
            sError := 'R.F.C. no puede tener espacios'
         else
             if GetNombreRaro( Copy( sRFC, 1, 4 ), sError ) then
                sError := 'R.F.C. contiene: ' + sError
             else
                 if Length( Copy( sRFC, 5, 6 ) ) < 6 then
                    sError :='R.F.C. fecha no es v�lida'
                 else
                     if ( Pos( ' ', Copy( sRFC, 11, 3 ) ) > 0 ) OR ( Length( Copy( sRFC, 11, 3 ) ) < 3 ) then
                        sError := 'R.F.C. homoclave invalido'
                     else
                         if ( Length( sRFC ) = 13 ) and ( Copy( sRFC, 13, 1 ) <> GetDigitoRFC( sRFC ) ) then
                            sError := 'R.F.C. d�gito verificador invalido';
     Result := StrVacio( sError );
end;

function ValidaCURP( sCurp: String; var sError: String ): Boolean;
begin
     sError := '';
     sCurp := Trim( UpperCase( sCurp ) );
     if StrVacio( sCurp ) then
        sError := 'C.U.R.P. no puede quedar vac�o'
     else
         if Pos( ' ', sCurp ) > 0 then
            sError := 'C.U.R.P. no puede tener espacios'
         else
             if Length( Copy( sCurp, 5, 6 ) ) < 6 then
                sError := 'C.U.R.P. fecha no es v�lida'
             else
                 if not ( ( Copy( sCurp, 11, 1 ) = 'H' ) or ( Copy( sCurp, 11, 1 ) = 'M' ) ) then
                    sError := 'C.U.R.P. caracter 11 debe ser H/M'
                 else
                     if Length( sCurp ) < 18 then
                        sError := 'C.U.R.P. no puede tener espacios'
                     else
                         if ( Length( sCurp ) = 18 ) and ( Copy( sCurp, 18, 1 ) <> ZetaCommonTools.GetDigitoCurp( sCurp ) ) then
                            sError := 'C.U.R.P. d�gito verificador invalido';
     Result := StrVacio( sError );
end;

{ Formato de Importaci�n de Fechas
  En base a enumerado:
  eFormatoImpFecha = ( ifDDMMYYs, ifDDMMYYYYs, ifDDMMYY, ifDDMMYYYY,
                       ifMMDDYYs, ifMMDDYYYYs, ifMMDDYY, ifMMDDYYYY,
                       ifYYMMDDs, ifYYYYMMDDs, ifYYMMDD, ifYYYYMMDD );
 }

function GetFechaItem( const sFecha, sDelimita: String; const item: Integer ): Integer;
var
   iCuantos: Integer;
   sTemp, sItem: String;

   function GetSiguiente: String;
   var
      iPos: Word;
   begin
        iPos := Pos( sDelimita, sTemp );
        if ( iPos > 0 ) then
        begin
             Result := Copy( sTemp, 0, ( iPos - 1 ) );
             sTemp := Copy( sTemp, ( iPos + 1 ), MaxInt );
        end
        else
           Result := sTemp;
   end;

begin
     sTemp := sFecha;
     iCuantos := 0;
     Repeat
           sItem := GetSiguiente;
           Inc( iCuantos );
     Until ( iCuantos >= item );
     Result := StrAsInteger( sItem );
end;

function TieneDelimitador( const sData: String; var sDelimitador: String ): Boolean;

   function TieneCaracter( const sCarac: String ): Boolean;
   begin
        Result := ( Pos( sCarac, sData ) > 0 );
        if Result then
           sDelimitador := sCarac;
   end;

begin
     sDelimitador := VACIO;
     Result := TieneCaracter( '/' ) or TieneCaracter( '-' );
end;

function GetImportedDateDia( const sData: String ): TDate;
var
   sDelimita: String;
begin
     if TieneDelimitador( sData, sDelimita ) then
        Result := CodificaFecha( GetFechaItem( sData, sDelimita, 3 ),
                                 GetFechaItem( sData, sDelimita, 2 ),
                                 GetFechaItem( sData, sDelimita, 1 ) )
     else
        Result := CodificaFecha( StrAsInteger( Copy( sData, 5, 4 ) ),  // No afecta si el a�o viene con dos digitos
                                 StrAsInteger( Copy( sData, 3, 2 ) ),
                                 StrAsInteger( Copy( sData, 1, 2 ) ) );
end;

function GetImportedDateMes( const sData: String ): TDate;
var
   sDelimita: String;
begin
     if TieneDelimitador( sData, sDelimita ) then  { Formato MM/DD/YY }
        Result := CodificaFecha( GetFechaItem( sData, sDelimita, 3 ),
                                 GetFechaItem( sData, sDelimita, 1 ),
                                 GetFechaItem( sData, sDelimita, 2 ) )
     else
        Result := CodificaFecha( StrAsInteger( Copy( sData, 5, 4 ) ),  // No afecta si el a�o viene con dos digitos
                                 StrAsInteger( Copy( sData, 1, 2 ) ),
                                 StrAsInteger( Copy( sData, 3, 2 ) ) );
end;

function GetImportedDateYear( const sData: String ): TDate;
const
     K_LONG_FECHA_YY = 6;
var
   sDelimita: String;
begin
     if TieneDelimitador( sData, sDelimita ) then
        Result := CodificaFecha( GetFechaItem( sData, sDelimita, 1 ),
                                 GetFechaItem( sData, sDelimita, 2 ),
                                 GetFechaItem( sData, sDelimita, 3 ) )
     else
     begin
          if ( Length( sData ) = K_LONG_FECHA_YY ) then
             Result := CodificaFecha( StrAsInteger( Copy( sData, 1, 2 ) ),  // Si longitud es 6 se lee el a�o a 2 d�gitos
                                      StrAsInteger( Copy( sData, 3, 2 ) ),
                                      StrAsInteger( Copy( sData, 5, 2 ) ) )
          else
             Result := CodificaFecha( StrAsInteger( Copy( sData, 1, 4 ) ),
                                      StrAsInteger( Copy( sData, 5, 2 ) ),
                                      StrAsInteger( Copy( sData, 7, 2 ) ) );
     end;
end;

function GetFechaImportada( const sData: String; FormatoFecha: eFormatoImpFecha ): TDate;
begin
     case FormatoFecha of
          ifDDMMYYs, ifDDMMYYYYs, ifDDMMYY, ifDDMMYYYY :
                    Result := GetImportedDateDia( sData );
          ifMMDDYYs, ifMMDDYYYYs, ifMMDDYY, ifMMDDYYYY :
                    Result := GetImportedDateMes( sData );
          ifYYMMDDs, ifYYYYMMDDs, ifYYMMDD, ifYYYYMMDD :
                    Result := GetImportedDateYear( sData );
     else
          Result := NullDateTime;
     end;
end;

function DeclaracionAnualNombreCampo( const iPos, iGlobal: integer ): string;
 var
      iNumero: integer;
begin
     iNumero := iPos - iGlobal + 1;
     if ( iNumero < 100 ) then
         Result := 'TD_DATA_' + StrZero( IntToStr( iNumero ), 2 )
     else
         case iNumero of
              100: Result := 'TD_DATA_A0';
              101: Result := 'TD_MES_INI';
              102: Result := 'TD_MES_FIN';
         end;

end;

function PuedeCambiarTarjeta( const dInicial,dFinal, dLimite: TDate;
                              const lTieneDerecho: Boolean;
                              var sMensaje: string ): Boolean;
begin
     Result := lTieneDerecho;
     if not Result then
     begin
          Result := ( dInicial > dLimite ) and ( dFinal > dLimite );
          if ( not Result ) then
          begin
               sMensaje := Format( 'No se Puede Cambiar Tarjetas Anteriores %s A la Fecha L�mite %s', [ CR_LF, ZetaCommonTools.FechaCorta( dLimite ) ] );
          end;
     end;
end;

function QuitaCerosIzquierda( const sTexto: string ): string;
var
   lCopiar: boolean;
   i, iLen: integer;
   sTemp: string;
begin
     lCopiar := FALSE;
     iLen := Length( sTexto );
     if iLen > 0 then
     begin
          for i := 1 to iLen do
          begin
               sTemp := sTexto[ i ];
               if ( sTemp <> '0' ) then
                  lCopiar := TRUE;
               if lCopiar then
                  Result := Result + sTemp;
          end;
     end
     else
         Result := VACIO;
end;

function PreparaGafeteProximidad( const sTexto: string ): string;
begin
     Result := QuitaCerosIzquierda( sTexto );
end;

//(@am): Se cambia eTipoPeriodo por eClasifiPeriodo para el proyecto de "Configuracion de nomina"
function Dias7Tipo( const eTipo: eClasifiPeriodo ): Currency;
const
     aDias7Periodo: array[ eClasifiPeriodo] of Currency = ( 1, 7, 14, 15.2, 30.4, 10.13 );
begin
     Result := aDias7Periodo[ eTipo ];
end;

function SerialToWiegand( const sSerial: String ): String;
const
     K_MAX_LENGTH = 14;
     K_START_POS = 6;
     K_VALID_DIGITS = K_MAX_LENGTH - K_START_POS - 1;

{$ifdef FALSE}
Interpretation of Data Output from ProxPro Proximity Reader with Serial Interface HID P/N 5352A

Scope
This application note is for users of HID 5352A serial readers. It is intended as an aid to those interpreting the data seen from the serial output of the reader.

Serial Transmission RS232\422:
HID's standard readers output data through a "Wiegand" interface, a communications protocol used in most access control systems. In some instances a serial interface is required instead. A PC based system requires a serial interface. Communication is made through a DB9 or DB25 connector to a serial port on the computer.

HID's 5352A ProxPro(tm) reader is a medium range proximity reader with both RS232 and RS422 outputs. These outputs are selected with an internal dip switch on the ProxPro. There is hardware setup for RS485 output. But no standard protocol for RS485 has been established, so there are no software provisions for RS485 on the reader. Nor does it allow for any handshaking. The reader sends asynchronous data to the host whenever a card is read.

The communications settings for serial ports com1 or com2 are 9600 baud, 8 data bits, 1 stop bit, no parity, and no flow control.

There are a few differences between RS232 and RS422.

RS232 utilizes only one data line (TD). When the voltage on the TD line is above 3vdc, it is considered a "0". If it is lower than -3vdc, it is a "1".

RS422 uses two data lines (TX+ and TX-). Whenever TX+ has a higher voltage than TX- it is considered a "0", if the voltage is lower, it is a "1".
The advantage of RS422 is that the reader can be placed farther from the host, and data can be transmitted faster.

Distance

Speed

RS232 = 50ft

RS232 = 20Kb/s

RS422 = 4000ft

RS422 = 100Kb/s @ 4000ft

Message Sent by the
5352A Serial Reader
The reader sends out a string of characters. This message is 16 ASCII encoded hexadecimal digits. The message is the same whether the reader is in RS232 or RS422 mode. The string is shown below.

CCDDDDDDDDDDXX<CR><LF>

C = HID Specific Code
D = Card Data
(valid numbers are 0000000000 to 1FFFFFFFFF)
X = Checksum. This is calculated by converting the ASCII
characters to hex values and adding the six bytes
CC..DD ..DD together and taking the least significant 8
bits of the result.
CR = 0D Hex (ASCII code for a carriage return).
LF = 0A Hex (ASCII code for a line feed).

The first 3 characters (C C D) total only eight bits with the first C character 3 bits and the first D character only being one bit. All the other characters are four bits each.

Example: 26 Bit
Card Message
Example of a serial output for a standard 26 bit H10301 formatted card:

Format H10301 26 bits, Facility code = 5, Card number = 87.

Message sent by the reader:

C C D D D D D D D D D D X X
0 1 0 0 0 4 0 A 0 0 A E B D

Stripping off the checksum, X, and reducing the data to binary gives.

C C D D D D D D D D D D
000 0001 0 0000 0000 0100 0000 1010 0000 0000 1010 1110
C C Z Z Z zspf f f f f f f fn nnnn nnnn nnnn nnnp

All the Card Data Characters to the left of the 7th can be ignored.

C = HID Specific Code.
Z = Leading zeros.
s = Start sentinel (It is always a 1).
p = Parity odd and even (12 bits each).
f = Facility Code 8 bits (In this example = 5 decimal).
n = Card Number 16 bits (In this example = 87 decimal).

Remove everything to the left of the 26th bit. Then drop both parity bits.
24 bits remain. The most significant 8 bits form the "Facility Code" field.
The least significant 16 bits form the "Card Number" field.

0000 0101 = 8 bit Facility Code = 5 decimal.
0000 0000 0101 0111 = 16 bit Card Number = 87 decimal.
Example: 37 Bit
Card Message

The first 3 characters (C C D) total only eight bits. The first C is three bits and the first D character only one bit. All the other characters are four bits each.

Format H10302 37 bit (it has no Facility Code), Card number = 285.

The serial message seen by the host is as shown below.

C C D D D D D D D D D D X X
0 0 0 0 0 0 0 0 0 2 3 5 3 7

Removing the checksum and reducing the data to binary gives:

C C D D D D D D D D D D
000 0000 0 0000 0000 0000 0000 0000 0000 0010 0011 1010
C C p nnnn nnnn nnnn nnnn nnnn nnnn nnnn nnnn nnnp

C = HID Specific Code.
p = Parity even and odd (18 bits each).
n = Card Number 35 bits.

Remove everything to the left of bit 37. Then drop both parity bits.
This leaves only the 35 bits of Card Number data.

0000 00000 0000 0000 0000 0000 0010 0011 101 = 35 bit Card Number =285 decimal.

K Version of Keypad
Internal Message
The keypad sends an ASCII representation of the key pressed. Key buffering is also supported on the serial reader as in the Wiegand reader. No parity or bit inversion is supported on the serial ProxPro keypad.
{$endif}

   {$ifdef FALSE}
   function ConvierteAHex( const sBinario: String ): String;
   var
      iPos: Integer;
      sNibble: String;
   begin
        Result := '';
        iPos := 1;
        while ( iPos < Length( sBinario ) ) do
        begin
             sNibble := Copy( sBinario, iPos, 4 );
             if ( sNibble = '0000' ) then
                Result := Result + '0'
             else
             if ( sNibble = '0001' ) then
                Result := Result + '1'
             else
             if ( sNibble = '0010' ) then
                Result := Result + '2'
             else
             if ( sNibble = '0011' ) then
                Result := Result + '3'
             else
             if ( sNibble = '0100' ) then
                Result := Result + '4'
             else
             if ( sNibble = '0101' ) then
                Result := Result + '5'
             else
             if ( sNibble = '0110' ) then
                Result := Result + '6'
             else
             if ( sNibble = '0111' ) then
                Result := Result + '7'
             else
             if ( sNibble = '1000' ) then
                Result := Result + '8'
             else
             if ( sNibble = '1001' ) then
                Result := Result + '9'
             else
             if ( sNibble = '1010' ) then
                Result := Result + 'A'
             else
             if ( sNibble = '1011' ) then
                Result := Result + 'B'
             else
             if ( sNibble = '1100' ) then
                Result := Result + 'C'
             else
             if ( sNibble = '1101' ) then
                Result := Result + 'D'
             else
             if ( sNibble = '1110' ) then
                Result := Result + 'E'
             else
             if ( sNibble = '1111' ) then
                Result := Result + 'F'
             else
                 raise Exception.Create( 'No Es Binario' );
             iPos := iPos + 4;
        end;
   end;

   function ConvierteABits( const sHex: String ): String;
   var
      i: Integer;
   begin
        Result := '';
        for i := 1 to Length( sHex ) do
        begin
             case sHex[ i ] of
                 '0' : Result := Result + '0000';
                 '1' : Result := Result + '0001';
                 '2' : Result := Result + '0010';
                 '3' : Result := Result + '0011';
                 '4' : Result := Result + '0100';
                 '5' : Result := Result + '0101';
                 '6' : Result := Result + '0110';
                 '7' : Result := Result + '0111';
                 '8' : Result := Result + '1000';
                 '9' : Result := Result + '1001';
                 'A' : Result := Result + '1010';
                 'B' : Result := Result + '1011';
                 'C' : Result := Result + '1100';
                 'D' : Result := Result + '1101';
                 'E' : Result := Result + '1110';
                 'F' : Result := Result + '1111';
             else
                 raise Exception.Create( 'D�gito No Es V�lido' );
             end;
        end;
   end;
   {$endif}
begin
     if ( Length( sSerial ) <> K_MAX_LENGTH ) then
        raise Exception.Create( 'Longitud No Es V�lida' )
     else
         Result := Format( '%x', [ ( ( StrToInt64Def( '$' + Copy( sSerial, K_START_POS, K_VALID_DIGITS ), 0 ) shr 1 ) and $FFFFFF ) ] );
         {
         Result := ConvierteAHex( Copy( ConvierteABits( Copy( sSerial, 6, 7 ) ), 4, 24 ) );
         }
end;

function ClockAndDataToWiegand( const sClockAndData: String ): String;
var
   iPwr, iValue: Int64;
   i: Word;
begin
     {
     Los datos son le�dos en formato Octal (caracteres van del 0 al 7 )
     Hay que conservar unicamente los primeros 26 bits (los menos significativos),
     quitar los bits de paridad (1 y 26 ) y convertir a hexadecimal los 24 bits
     que quedan para formar un n�mero de 6 digitos HEX

     Dato le�do: 562566575

     5   6   2   5   6   6   5   7   5
     101 110 010 101 110 110 101 111 101 - De Octal a Binario
     01 110 010 101 110 110 101 111 101  - Conserva solo los 26 bits menos significativos
     1 110 010 101 110 110 101 111 10    - Quita bits de paridad, 1 y 26, quedan 24 bits
     1110 0101 0111 0110 1011 1110       - Agrupar en 6 grupos de 4 bits
     E    5    7    6    B    E          - Convertir a Hexadecimal
     }
     iValue := 0;
     iPwr := 1;
     for i := Length( sClockAndData ) downto 1 do
     begin
          iValue := iValue + ( iPwr * StrToIntDef( sClockAndData[ i ], 0 ) );
          iPwr := iPwr * 8;
     end;
     Result := Format( '%x', [ ( iValue shr 1 ) and $FFFFFF ] );
end;

function ExtractJustExt(const sArchivo: string): string;
begin
     Result := ExtractFileExt( sArchivo );
     if StrLleno( Result ) then
     begin
          if Pos( '.', Result ) <> 0 then
          Result := Copy( Result, Pos( '.', Result ) + 1, Length( Result ) ) ;
     end;
end;

function ValidaStatusNomina( const StatusActivo, StatusLimite: eStatusPeriodo ): Boolean;
 var
    sMensaje: string;
begin
     Result := ValidaStatusNomina( StatusActivo, StatusLimite, sMensaje );

end;

function ValidaStatusNomina( const StatusActivo, StatusLimite: eStatusPeriodo;
                             var sMensaje: string ): Boolean;
begin
     Result := ( StatusActivo <= StatusLimite );
     if NOT Result then
        sMensaje := Format( 'No se Puede Modificar Tarjetas de N�mina %s',
                            [ObtieneElemento(lfStatusPeriodo, ord(StatusActivo))] );
end;

function EsBajaenPeriodo( const dIngreso, dBaja: TDate; const oDatosPeriodo: TDatosPeriodo; var dFechaInicio, dFechaLimite: TDate ): Boolean;
begin
     with oDatosPeriodo do
     begin
          dFechaInicio := InicioAsis;
          dFechaLimite := FinAsis;
          Result := ( dBaja <> NullDateTime ) and ( dBaja > dIngreso ) and      // Si hay baja y
                    ( dBaja >= InicioAsis ) and ( dBaja <= Fin );               // la fecha cae en cualquier dia del rango de Asistencia hasta pago
          if Result then
          begin
               if ( dBaja >= Inicio ) then  // Si la baja cae en el periodo de pago se tomar�n los dias hasta el final del periodo de pago
                  dFechaLimite := Fin;
               if ( dIngreso > Inicio ) then  // Si el ingreso es posterior al inicio del periodo de pago, corta desde el Inicio del periodo de pago
                  dFechaInicio := Inicio
               else if ( dIngreso > InicioAsis ) then  // Si el Ingreso cae en el periodo de prenomina comienza desde el Ingreso
                  dFechaInicio := dIngreso;
          end;
     end;
end;

function EsCambioTNomPeriodo( const dCambioTipoNom: TDate; const oDatosPeriodo: TDatosPeriodo ): Boolean;
begin
     with oDatosPeriodo do
     begin
          Result:= ( dCambioTipoNom >= Inicio ) and
                   ( dCambioTipoNom <= Fin );
     end;
end;

function ValidaListaNumeros( const sLista: string ): Boolean;
 var
    oLista: TStrings;
    i: integer;
begin
     Result := TRUE;
     if StrLleno( sLista ) then
     begin
          try
             oLista := TStringList.Create;
             oLista.CommaText := sLista;

             for i:= 0 to oLista.Count - 1 do
             begin
                  try
                     StrToInt( oLista[i] );
                  except
                        Result := FALSE;
                        Break;
                  end;
             end;
          finally
                 FreeAndNil( oLista );
          end;
     end;
end;

function AplicaRegla3X3( const DiaAplicaExento:eDiasAplicaExento;const StatusAusencia:eStatusAusencia;const TipoDia:eTipoDiaAusencia):Boolean;
begin
     case DiaAplicaExento of
          daeHabiles:
          begin
               Result := ( StatusAusencia = auHabil );
          end;
          daeHabilesSinFestivos:
          begin
               Result := ( ( StatusAusencia = auHabil ) and  (  TipoDia <> daFestivo ) );
          end;
          else
          begin
               Result := True;
          end;
      end;
end;

function ProgramacionEspecial( const eProgramacion : eProyectoEspecial; const sValor : string ): Boolean;
begin
     Result := PosicionPrendida( Ord( eProgramacion ), sValor );
end;

function PosicionPrendida( const iPosicion : integer; const sValor : string ): Boolean;
 const K_TOPE = 4;
 var
    iLen: integer;
begin

     Result := StrLleno(sValor);

     if Result then
     begin
          iLen := ( iPosicion div K_TOPE );
          if (((iPosicion+1) mod K_TOPE) <> 0) then
             iLen := iLen +1;

          if iLen <= Length(sValor) then
             Result := ( ( StrToIntDef( '$'+sValor,0 ) and ( 1 shl iPosicion ) ) <> 0 )
          else
              Result := FALSE;
     end;

end;

function ListaComas2TextoComillas( CommaTexto : string ) : string;
var
    lista : TStringList;
begin
  lista := TStringList.Create;
  lista.CommaText :=  Trim( CommaTexto );

  Result  := '';

  if lista.Count > 0 then
  begin
       Result := lista.CommaText;
       Result := StringReplace( Result, ',',''',''',[rfReplaceAll, rfIgnoreCase]);
       Result := '''' + Result + '''';
  end;

  FreeAndNil( lista );

end;

function ListaComas2InQueryList( CommaTexto : string ) : string;
begin
     Result := ListaComas2TextoComillas( CommaTexto );

     if StrLleno( Result )  then
             Result := '(' + Result + ')';
end;

function EsLista( CommaTexto : string ) : boolean;
var
    lista : TStringList;
begin
  lista := TStringList.Create;
  lista.CommaText := Trim( CommaTexto );

  Result := lista.Count > 1;

  FreeAndNil( lista );

end;

function ListaIntersectaConfidencialidad( sLista1, sLista2 : string ) : boolean;
var
   lLista1, lLista2 : TStringList;
   idx, idx2 : integer;
begin
     if StrLLeno( sLista1 ) and StrLLeno( sLista2 ) then
     begin
          lLista1 := TStringList.Create;
          lLista2 := TStringList.Create;

          lLista1.CommaText := Trim( sLista1 );
          lLista2.CommaText := Trim( sLista2 );

          Result := FALSE;
          for idx := 0 to lLista1.Count-1 do
          begin
               if lLista2.Find( lLista1[idx], idx2 ) then
               begin
                    Result := TRUE;
                    break;
               end;
          end;
          FreeAndNil(  lLista1 );
          FreeAndNil(  lLista1 );
     end
     else
         Result := TRUE;
end;

{$ifdef AUTOLIV}
function Encripcion(cadena: LPSTR; llave: LPSTR):  smallint stdcall; External 'WMLOCK.dll' Name 'Encripcion';
function Deencripcion(cadena: LPSTR; llave: LPSTR):  smallint stdcall; External 'WMLOCK.dll' Name 'Deencripcion';

function EncriptaWMLOCK( sCadena, sLlave: string; var lError: Boolean ): String;
var
   sCadenaEncriptada: String;
   pCadena, pllave: PAnsiChar;
begin
     lError := false;
     try
        GetMem( pCadena, 10000 );
        try
           GetMem( pLlave, 5000 );
           try
              pLLave := StrPCopy( pLlave, sLlave );
              pCadena := StrPCopy( pCadena, sCadena );
              if ( Encripcion( pCadena,  pLlave ) > 0 )then
                 SetString( sCadenaEncriptada, pCadena, Length( pCadena ) )
              else
                  sCadenaEncriptada := VACIO;

              Result := sCadenaEncriptada;
           finally
                  FreeMem( pCadena );
           end;
        finally
               FreeMem( pLlave );
        end;
     except
           lError := true;
           Result := VACIO;
     end;
end;
{$endif}

function scandate(const pattern:string;s:string;startpos:integer=1) : tdatetime;
var len ,ind  : integer;
    plen,pind : integer;
    i     : integer;
    pivot,
    yy,mm,dd  : integer;

function scanfixedint:integer;
var c : char;
begin
  result:=0;
  c:=pattern[pind];
  {$IFNDEF TRESS_DELPHIXE5_UP}
  while (pind<=plen) and (pattern[pind]=c) and (ind<=len) and (s[ind] IN ['0'..'9']) do
  {$ELSE}
  while (pind<=plen) and (pattern[pind]=c) and (ind<=len) and (CharInSet (s[ind], ['0'..'9'])) do
  {$ENDIF}
     begin
       result:=result*10+ord(s[ind])-48;
       inc(pind);
       inc(ind);
     end;
  while (pind<=plen) and (pattern[pind]=c) do inc(pind); 
end;

begin
  yy:=0; mm:=0; dd:=0;
  result:=0.0;
  len:=length(s); ind:=startpos;
                       
  {$IFNDEF TRESS_DELPHIXE5_UP}
  while(ind<=len) and (s[ind] in whitespace) do inc(ind);
  {$ELSE}
  while(ind<=len) and ( CharInSet (s[ind], whitespace)) do inc(ind);
  {$ENDIF}
  plen:=length(pattern); pind:=1;

  while (ind<=len) and (pind<=plen) do
     begin
        case pattern[pind] of
           'h':  result:=result+scanfixedint* hrfactor;
           'd':  dd:=scanfixedint;
           'n':  result:=result+scanfixedint* minfactor;
           's':  result:=result+scanfixedint* secfactor;
           'z':  result:=result+scanfixedint* mssecfactor;
           'y':  begin
                   i:=pind;
                   yy:=scanfixedint;
                   i:=pind-i;
                   if i<=2 then
                     begin
                       pivot:=TheYear(now)-{$IFDEF DELPHIXE3_UP}FormatSettings.{$ENDIF}TwoDigitYearCenturyWindow;
                       inc(yy, pivot div 100 * 100);
                       if ({$IFDEF DELPHIXE3_UP}FormatSettings.{$ENDIF}TwoDigitYearCenturyWindow > 0) and (yy < pivot) then
                          inc(yy, 100);
                     end;
                  end;
           'm':  mm:=scanfixedint;

           else
             begin
               inc(pind);
               inc(ind);
             end;
         end;
     end;
 if (yy>0) and (mm>0) and (dd>0) then  
    result:=result+encodedate(yy,mm,dd);
end;


function GetDescripcionStatusPeriodo( statusPeriodo : eStatusPeriodo; statusTimbrado : eStatusTimbrado ) : string;
begin
     Result  := ZetaCommonLists.ObtieneElemento( lfStatusPeriodo, ord( statusPeriodo) );
end;

function GetDescripcionStatusPeriodo( periodo : TDatosPeriodo ) : string; overload;
begin
     Result := GetDescripcionStatusPeriodo( periodo.Status, periodo.StatusTimbrado ) ;
end;


function ValidarEmail(email: string): boolean;
 const
    // Valid characters in an "atom"
    atom_chars = [#33..#255] - ['(', ')', '<', '>', '@', ',', ';', ':',
                                '\', '/', '"', '.', '[', ']', #127];
    // Valid characters in a "quoted-string"
    quoted_string_chars = [#0..#255] - ['"', #13, '\'];
    // Valid characters in a subdomain
    letters = ['A'..'Z', 'a'..'z'];
    letters_digits = ['0'..'9', 'A'..'Z', 'a'..'z'];
    subdomain_chars = ['-', '0'..'9', 'A'..'Z', 'a'..'z'];
  type
    States = (STATE_BEGIN, STATE_ATOM, STATE_QTEXT, STATE_QCHAR,
      STATE_QUOTE, STATE_LOCAL_PERIOD, STATE_EXPECTING_SUBDOMAIN,
      STATE_SUBDOMAIN, STATE_HYPHEN);
  var
    State: States;
    i, n, subdomains: integer;
    c: char;
  begin
    State := STATE_BEGIN;
    n := Length(email);
    i := 1;
    subdomains := 1;

    while (i <= n) do begin
      c := email[i];
      case State of
      STATE_BEGIN:
        {$IFNDEF TRESS_DELPHIXE5_UP}
        if c in atom_chars then
        {$ELSE}
        if CharInSet (c, atom_chars) then
        {$ENDIF}
          State := STATE_ATOM
        else if c = '"' then
          State := STATE_QTEXT
        else
          break;
      STATE_ATOM:
        if c = '@' then
          State := STATE_EXPECTING_SUBDOMAIN
        else if c = '.' then
          State := STATE_LOCAL_PERIOD
        {$IFNDEF TRESS_DELPHIXE5_UP}
        else if not (c in atom_chars) then
        {$ELSE}
        else if not (CharInSet (c, atom_chars)) then
        {$ENDIF}
          break;
      STATE_QTEXT:
        if c = '\' then
          State := STATE_QCHAR
        else if c = '"' then
          State := STATE_QUOTE
        {$IFNDEF TRESS_DELPHIXE5_UP}
        else if not (c in quoted_string_chars) then
        {$ELSE}
        else if not (CharInSet (c, quoted_string_chars)) then
        {$ENDIF}
          break;
      STATE_QCHAR:
        State := STATE_QTEXT;
      STATE_QUOTE:
        if c = '@' then
          State := STATE_EXPECTING_SUBDOMAIN
        else if c = '.' then
          State := STATE_LOCAL_PERIOD
        else
          break;
      STATE_LOCAL_PERIOD:
        {$IFNDEF TRESS_DELPHIXE5_UP}
        if c in atom_chars then
        {$ELSE}
        if CharInSet (c, atom_chars) then
        {$ENDIF}
          State := STATE_ATOM
        else if c = '"' then
          State := STATE_QTEXT
        else
          break;
      STATE_EXPECTING_SUBDOMAIN:
        {$IFNDEF TRESS_DELPHIXE5_UP}
        if c in letters then
        {$ELSE}
        if CharInSet (c, letters) then
        {$ENDIF}
          State := STATE_SUBDOMAIN
        else
          break;
      STATE_SUBDOMAIN:
        if c = '.' then begin
          inc(subdomains);
          State := STATE_EXPECTING_SUBDOMAIN
        end else if c = '-' then
          State := STATE_HYPHEN
        {$IFNDEF TRESS_DELPHIXE5_UP}
        else if not (c in letters_digits) then
        {$ELSE}
        else if not (CharInSet (c, letters_digits)) then
        {$ENDIF}
          break;
      STATE_HYPHEN: 
        {$IFNDEF TRESS_DELPHIXE5_UP}
        if c in letters_digits then
        {$ELSE}
        if CharInSet (c, letters_digits) then
        {$ENDIF}
          State := STATE_SUBDOMAIN
        else if c <> '-' then
          break;
      end;
      inc(i);
    end;
    if i <= n then
      Result := False
    else
      Result := (State = STATE_SUBDOMAIN) and (subdomains >= 2);
  end;

function ByteToKiloByte( const number: Int64 ): Int64;
begin
     Result := number div 1024;
end;

function KiloByteToMegaByte( const number: Int64 ): Int64;
begin
      Result := number div 1024;
end;

function MegaByteToGigaByte( const number: Int64 ): Currency;
begin
      Result := number div 1024;
end;

function ByteToGigaByte( const number: Int64 ): Currency;
begin
     Result := (((number /1024) /1024)/1024);
end;

{* Busca la posicion de SubStr en S de derecha a izquierda. @autor Ricardo Carrillo Morales 01/Mar/2014}
function RPos(SubStr: string; S: string): integer;
var
  I: integer;
begin
  SubStr := ReverseString(SubStr);
  S := ReverseString(S);
  I := Pos(SubStr, S);
  if I <> 0 then
    I := (Length(S) + 1) - (I + Length(SubStr) - 1);
  Result := I;
end;

function SetFileNameDefaultPath( const sFile: String ): String;
begin
     Result := SetFileNamePath( ExtractFileDir( Application.ExeName ), sFile );
end;

function SetFileNamePath( const sPath, sFile: String ): String;
begin
     Result := sPath;
     if ( Result[ Length( Result ) ] <> '\' ) then
        Result := Result + '\';
     Result := Result + sFile;
end;

function DentroBimestre(const dReIngreso, dBaja: TDate):Boolean;
begin
     Result := ( LastDayOfBimestre(dBaja)>= dReIngreso )and ( FirstDayOfBimestre(dBaja)<= dReIngreso ); 
end;

function GetIsWindowsVista: Boolean;
begin
     Result := ( SysUtils.Win32Platform = VER_PLATFORM_WIN32_NT ) and ( SysUtils.Win32MajorVersion >= 6 );
end;

function GeneraMensajeHTML( var sAttach:string; ReemplazaVariables:TFunctionReemplazaVariables; sPlantilla: String = VACIO):string;
const
     K_PLANTILLA_DEFAULT = 'PlantillaNotificacionReporteEmail';
var
     oArchivo : TStrings;
     sMensaje, sArchivo : string;
     RSource:Tresourcestream;
begin
     oArchivo := TStringList.Create;
     sMensaje := VACIO;

     if sPlantilla = VACIO then
        sPlantilla := K_PLANTILLA_DEFAULT;

     try
        RSource := TResourceStream.Create(HInstance,sPlantilla, RT_RCDATA);
        oArchivo.LoadFromStream(RSource);
        sMensaje := oArchivo.Text;
        sMensaje := ReemplazaVariables(sMensaje, sAttach); //Funcion personalizada
     finally
            FreeAndNil( oArchivo );
            FreeAndNil( RSource );
     end;

     Result := sMensaje;
end;

function GeneraMensajeHTMLLogin( var sAttach, sUrlImagen:string; ReemplazaVariables:TFunctionReemplazaVariablesLogin; sPlantilla: String = VACIO):string;
const
     K_PLANTILLA_DEFAULT = 'PlantillaNotificacionReporteEmail';
var
     oArchivo : TStrings;
     sMensaje, sArchivo : string;
     RSource:Tresourcestream;
begin
     oArchivo := TStringList.Create;
     sMensaje := VACIO;

     if sPlantilla = VACIO then
        sPlantilla := K_PLANTILLA_DEFAULT;

     try
        RSource := TResourceStream.Create(HInstance,sPlantilla, RT_RCDATA);
        oArchivo.LoadFromStream(RSource);
        sMensaje := oArchivo.Text;
        sMensaje := ReemplazaVariables(sMensaje, sAttach, sUrlImagen); //Funcion personalizada
     finally
            FreeAndNil( oArchivo );
            FreeAndNil( RSource );
     end;

     Result := sMensaje;
end;

function GeneraMensajeHTMLError( var sAttach, sError, sNombreCalendario:string; ReemplazaVariablesError:TFunctionReemplazaVariablesError; sPlantilla: String = VACIO):string;
const
     K_PLANTILLA_DEFAULT = 'PlantillaNotificacionReporteEmailError';
var
     oArchivo : TStrings;
     sMensaje, sArchivo, sFecha : string;
     RSource:Tresourcestream;
begin
     oArchivo := TStringList.Create;
     sMensaje := VACIO;
     sFecha := DateToStr(Now);

     if sPlantilla = VACIO then
        sPlantilla := K_PLANTILLA_DEFAULT;

     try
        RSource := TResourceStream.Create(HInstance,sPlantilla, RT_RCDATA);
        oArchivo.LoadFromStream(RSource);
        sMensaje := oArchivo.Text;
        sMensaje := ReemplazaVariablesError(sMensaje, sAttach, sFecha, sError, sNombreCalendario); //Funcion personalizada
     finally
            FreeAndNil( oArchivo );
            FreeAndNil( RSource );
     end;

     Result := sMensaje;
end;

function ParseCSVLine(ALine: string; AFields: TStringList): TStringList;
var
   iState:  cardinal;
   i:       cardinal;
   iLength: cardinal;
   sField, sError:  string;
begin
     iLength := Length(ALine);
     if iLength = 0 then
       Exit;
     iState := 0;
     sField := '';

     AFields.Clear;
     for i := 1 to iLength do
     begin
       case iState of
            0:
            begin
                 sField := '';
                 case ALine[i] of
                      '"':
                      begin
                           iState := 2;
                      end;
                      ',':
                      begin
                           AFields.Add(sField);
                      end;
                      else
                      begin
                           sField := ALine[i];
                           iState := 1;

                           if (i = iLength) then
                              AFields.Add(sField);
                      end;
                 end;
            end;
            1:
            begin
                 case ALine[i] of
                   ',':
                   begin
                        AFields.Add(sField);
                        if (i = iLength) then
                        begin
                             AFields.Add('');
                        end
                        else
                        begin
                             iState := 0;
                        end;
                   end;
                   else
                   begin
                        sField := sField + ALine[i];
                        if (i = iLength) then
                           AFields.Add(sField);
                   end;
                 end;
            end;
            2:
            begin
                 case ALine[i] of
                   '"':
                   begin
                        if (i < iLength) then  /////
                        begin
                             if (ALine[i+1] = ',') then
                             begin
                                   iState := 1
                             end
                             else
                             begin
                                  iState := 3;
                             end;
                        end
                        else
                        begin
                              AFields.Add(sField);
                        end;
                   end
                   else
                   begin
                        sField := sField + ALine[i];
                   end;
                 end;
            end;
            3:
            begin
                 case ALine[i] of
                      '"':
                      begin
                           sField := sField + ALine[i];
                           iState := 2;
                      end;
                 end;
            end;
       end;
     end;
     Result := AFields;
end;

end.

