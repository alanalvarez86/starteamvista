unit FCalendario;

interface

{$INCLUDE JEDI.INC}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ExtCtrls, Grids,
  StdCtrls, Buttons, ComCtrls, ZetaCommonClasses, ZetaKeyCombo, Spin;

type
  TCalendario = class(TForm)
    StringGrid: TStringGrid;
    PanelInferior: TPanel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    YearLBL: TLabel;
    MonthLBL: TLabel;
    MonthValue: TZetaKeyCombo;
    PanelFecha: TPanel;
    YearValue: TSpinEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure MonthValueChange(Sender: TObject);
    procedure YearValueChange(Sender: TObject);
    procedure StringGridDrawCell(Sender: TObject; Col, Row: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure StringGridDblClick(Sender: TObject);
    procedure StringGridClick(Sender: TObject);
    procedure StringGridSelectCell(Sender: TObject; Col, Row: Integer; var CanSelect: Boolean);
  private
    { Private declarations }
    FFecha: TDate;
    FChanging: Boolean;
    FStartPos: Word;
    procedure SetFecha(const Value: TDate);
    procedure ResetCalendario;
    procedure ResetFecha(wDay: Word);
  public
    { Public declarations }
    property Fecha: TDate read FFecha write SetFecha;
    procedure Init;
  end;

var
  Calendario: TCalendario;

  function BuscaFecha(var dValor: TDate): Boolean;
  procedure SetDefaultDlgFecha(const dValue: TDate);

implementation

uses
  ZetaCommonTools;

var
  dDefault: TDate;

  {$R *.DFM}

function BuscaFecha(var dValor: TDate): Boolean;
begin
  Result := False;
  if (Calendario = nil) then
    Calendario := TCalendario.Create(Application);
  if (Calendario <> nil) then
    with Calendario do begin
      if (dValor = NullDateTime) then
        Fecha := dDefault
      else
        Fecha := dValor;
      Init;
      ShowModal;
      if (ModalResult = mrOk) and (Fecha <> NullDateTime) then begin
        dValor := Fecha;
        Result := True;
      end;
    end;
end;

procedure SetDefaultDlgFecha(const dValue: TDate);
begin
  dDefault := dValue;
end;

{ *********** TCalendario ************ }

procedure TCalendario.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  with StringGrid do begin
    for i := 1 to 7 do
      Cells[(i - 1), 0] := {$IFDEF DELPHIXE3_UP}FormatSettings.{$ENDIF}ShortDayNames[i];
  end;
  HelpContext := H00014_Calendario;
end;

procedure TCalendario.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caHide;
end;

procedure TCalendario.Init;
begin
  OK.Default := True;
  ActiveControl := OK;
end;

procedure TCalendario.SetFecha(const Value: TDate);
var
  Year, Month, Day: Word;
  Anio, Mes, Dia: Word;
begin
  if (FFecha <> Value) and not FChanging then begin
    FChanging := True;
    try
      DecodeDate(FFecha, Anio, Mes, Dia);
      DecodeDate(Value, Year, Month, Day);
      FFecha := Value;
      if (Anio <> Year) then
        YearValue.Value := Year;
      if (Mes <> Month) or (MonthValue.ItemIndex < 0) then
        MonthValue.ItemIndex := (Month - 1);
      if (Anio <> Year) or (Mes <> Month) then
        ResetCalendario;
      ResetFecha(Day);
    finally
      FChanging := False;
    end;
  end;
end;

procedure TCalendario.ResetCalendario;
var
  i, j, iDay, iFirstDay, iLastDay: Integer;
  sDay: string;
begin
  iFirstDay := DayOfWeek(FirstDayOfMonth(Fecha));
  iLastDay := TheDay(LastDayOfMonth(Fecha));
  iDay := 0;
  with StringGrid do begin
    for j := 2 to RowCount do begin // Empieza en 2 por el Encabezado //
      for i := 1 to ColCount do  begin// Conteo basado en 1 para i, j //
        if (iDay = 0) and (i = iFirstDay) then begin
          iDay := 1;
          sDay := '1';
          FStartPos := ((j - 1) * ColCount) + (i - 1);
        end else if (iDay > 0) and (iDay < iLastDay) then begin
          iDay := iDay + 1;
          sDay := IntToStr(iDay);
        end else
          sDay := '';
        Cells[(i - 1), (j - 1)] := sDay;
      end;
    end;
  end;
end;

procedure TCalendario.ResetFecha(wDay: Word);
begin
  PanelFecha.Caption := FechaLarga(Fecha);
  wDay := FStartPos + wDay - 1;
  with StringGrid do begin
    Row := Trunc(wDay / ColCount);
    Col := (wDay mod ColCount);
  end;
end;

procedure TCalendario.MonthValueChange(Sender: TObject);
begin
  Fecha := DateTheMonth(Fecha, (MonthValue.ItemIndex + 1));
end;

procedure TCalendario.YearValueChange(Sender: TObject);
begin
  if (Length(YearValue.Text) = 4) then
    Fecha := DateTheYear(Fecha, YearValue.Value);
end;

procedure TCalendario.StringGridDrawCell(Sender: TObject; Col, Row: Integer; Rect: TRect;
  State: TGridDrawState);
var
  oColor: TColor;
begin
  if (StringGrid.Cells[Col, Row] = '') then begin
    with StringGrid.Canvas do begin
      oColor := Brush.Color;
      try
        Brush.Color := clInfoBk;
        FillRect(Rect);
      finally
        Brush.Color := oColor;
      end;
    end;
  end;
end;

procedure TCalendario.StringGridDblClick(Sender: TObject);
begin
  OK.Click;
end;

procedure TCalendario.StringGridClick(Sender: TObject);
var
  Year, Month, Day: Word;
begin
  DecodeDate(Fecha, Year, Month, Day);
  with StringGrid do
    Day := StrAsInteger(Cells[Col, Row]);
  if DayIsValid(Year, Month, Day) then
    Fecha := EncodeDate(Year, Month, Day);
end;

procedure TCalendario.StringGridSelectCell(Sender: TObject; Col, Row: Integer;
  var CanSelect: Boolean);
begin
  CanSelect := (StringGrid.Cells[Col, Row] <> '');
end;

end.
