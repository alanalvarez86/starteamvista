type
    TipoEntidad = ( enEntidadVACIA,      {000}
                    enAcumula,           {001}
                    enAhorro,            {002}
                    enAntesCur,          {003}
                    enAntesPto,          {004}
                    enAusencia,          {005}        
                    enCalCurso,          {006}
                    enCampoRep,          {007}
                    enChecadas,          {008}
                    enClasifi,           {009}
                    enEmpleado,          {010}
                    enConcepto,          {011}
                    enContrato,          {012}
                    enCurso,             {013}
                    enEdoCivil,          {014}
                    enEntidad,           {015}
                    enEntrena,           {016}
                    enEstudios,          {017}
                    enEvento,            {018}
                    enExtra1,            {019}
                    enExtra2,            {020}
                    enExtra3,            {021}
                    enExtra4,            {022}
                    enFaltas,            {023}
                    enFestivo,           {024}
                    enFolio,             {025}
                    enGlobal,            {026}
                    enHorario,           {027}
                    enImagen,            {028}
                    enIncapacidad,       {029}
                    enIncidencia,        {030}
                    enKarFija,           {031}
                    enKarCurso,          {032}
                    enKardex,            {033}
                    enLiq_Emp,           {034}
                    enLiq_IMSS,          {035}
                    enLiq_Mov,           {036}
                    enMaestros,          {037}
                    enMoneda,            {038}
                    enMotBaja,           {039}
                    enMovimien,          {040}
                    enNivel1,            {041}
                    enNivel2,            {042}
                    enNivel3,            {043}
                    enNivel4,            {044}
                    enNivel5,            {045}
                    enNivel6,            {046}
                    enNivel7,            {047}
                    enNivel8,            {048}
                    enNivel9,            {049}
                    enNomina,            {050}
                    enOtrasPer,          {051}
                    enPariente,          {052}
                    enPCarAbo,           {053}
                    enPeriodo,           {054}
                    enPermiso,           {055}
                    enPrestacion,        {056}
                    enPrestamo,          {057}
                    enPRiesgo,           {058}
                    enPuesto,            {059}
                    enQuerys,            {060}
                    enReporte,           {061}
                    enRPatron,           {062}
                    enSSocial,           {063}
                    enTAhorro,           {064}
                    enTCurso,            {065}
                    enTKardex,           {066}
                    enTPresta,           {067}
                    enTransporte,        {068}
                    enTurno,             {069}
                    enVacacion,          {070}
                    enViveCon,           {071}
                    enViveEn,            {072}
                    enAcceso,            {073}
                    enArt_80,            {074}
                    enBitacora,          {075}
                    enCompanys,          {076}
                    enDiccion,           {077}
                    enGrupos,            {078}
                    enLeyImss,           {079}
                    enNumerica,          {080}
                    enPoll,              {081}
                    enRiesgo,            {082}
                    enSal_Min,           {083}
                    enEntNivel,          {084}
                    enUsuarios,          {085}
                    { De uso general }
                    enNinguno,           {086}  {entidad ninguno}
                    enRPromVar,          {087}
                    enProvCap,           {088}  
                    enMovimienBalanza,   {089}
                    enBitacoraComparte,  {090}
                    enEstadoAhorro,      {091}
                    enEstadoPrestamo,    {092}
                    enAusenciaRL,        {093}
                    enChecadasRL,        {094}
                    enCurProg,           {095}
                    enPoliza,            {096}
                    enProceso,           {097}
                    enNomParam,          {098}
                    enTFijas,            {099}
                    enMovimienG,         {100}
                    enFormula,           {101}
                    enHorarioTemp,       {102}
                    enChecadasG,         {103}
                    enFunciones,         {104}
                    enArbol,             {105}
                    enDemografica,       {106}
                    enRepAhorro,         {107}
                    enAguinaldo,         {108}
                    enRotacion,          {109}
                    enCompara,           {110}
                    enDeclara,           {111}
                    enRepartoPTU,        {112}
                    enRegla,             {113}
                    enInvitacion,        {114}
                    enComida,            {115}
                    enInvitador,         {116}
                    enImpresora,         {117}
                    enAsigna,            {118}
                    enSuper,             {119}
                    enMovGral,           {120}
                    enFaltasG,           {121}
                    enTCambio,           {122}
                    enMotAuto,           {123}
                    enConteo,            {124}
                    enCorreo,            {125}
                    enUsuarioBitacora,   {126}
                    enNominaPR,          {127}
                    enOrdFolio,          {128}
                    enTPeriodo,          {129}
                    enAutorizaciones,    {130}
                    enCalendario,        {131}
                    enMovimienLista,     {132}
                    enConcilia,          {133}
                    enCurAsis,           {134}
                    {Entidades para Modulo de Labor}
                    enModula1,           {135}
                    enModula2,           {136}
                    enModula3,           {137}
                    enTMuerto,           {138}
                    enTParte,            {139}
                    enTOpera,            {140}
                    enPartes,            {141}
                    enOpera,             {142}
                    enOrdenesFijas,      {143}
                    enWorks,             {144}
                    enLecturas,          {145}
                    enDefSteps,          {146}
                    enSteps,             {147}
                    enArea,              {148}
                    enWorder,            {149}
                    enTemporalNomina,    {150}
                    enCursoGlobal,       {151}
                    enCedula,            {152}
                    enCedulaEmpleado,    {153}
                    enCedulaWorder,      {154}
                    enCalendarioHoras,   {155}
                    enTarjetaXY,         {156}
                    enNivel0,            {157}
                    enTools,             {158}
                    enMotTool,           {159}
                    enTalla,             {160}
                    enKarTool,           {161}
                    enListadoNomina,     {162}
                    enSuscrip,           {163}
                    enBreak,             {164}
                    enBrkHora,           {165}
                    enClaves,            {166}
                    enPrinter,           {167}
                    enFoto,              {168}
                    enExpediente,        {169}
                    enConsulta,          {170}
                    enEmbarazo,          {171}
                    enAccidente,         {172}
                    enMedicina,          {173}
                    enMedEntregada,      {174}
                    enTAccidente,        {175}
                    enCausaAccidente,    {176}
                    enTEstudio,          {177}
                    enTConsulta,         {178}
                    enDiagnostico,       {179}
                    enGrupoEx,           {180}
                    enCampoEx,           {181}
                    enSupArea,           {182}
                    enKardexArea,        {183}
                    enMotivoAcc,         {184}
                    enTDefecto,          {185}
                    enCedulaInspeccion,  {186}
                    enDefecto,           {187}
                    enCCurso,            {188}
                    enPtoFijas,          {189}
                    enPtoTools,          {190}
                    enCedulaScrap,       {191}
                    enComponentes,       {192}
                    enMotivoScrap,       {193}
                    enScrap,             {194}
                    {Entidades para Plan Carrera}
                    enTCompetencia,      {195}
                    enCalifica,          {196}
                    enDimensiones,       {197}
                    enAcciones,          {198}
                    enFamiliasPuesto,    {199}
                    enNivelesPuesto,     {200}
                    enPuestoDimension,   {201}
                    enCompetencias,      {202}
                    enCompeteFamilia,    {203}
                    enCompeteCalifica,   {204}
                    enCompetePuesto,     {205}
                    enEmpleadoCompete,   {206}
                    enEmpleadoPlan,      {207}
                    enCompeteMapa,       {208}
                    enNivelDimension,    {209}
                    enDeclaraAnual,      {210}
                    enSesion,            {211}
                    enMisReportes,       {212}
                    enEmpProg,           {213}
                    enSolicitud,         {214}
                    enTArt_80,           {215}
                    enAccRegla,          {216}
                    enAccesLog,          {217}
                    enClasifiTemp,       {218}
                    enEntidadNac,        {219}
                    enColonia,           {220}
                    enGrupoAdicional,    {221}
                    enCampoAdicional,    {222}
                    enExtra5,            {223}
                    enExtra6,            {224}
                    enExtra7,            {225}
                    enExtra8,            {226}
                    enExtra9,            {227}
                    enExtra10,           {228}
                    enExtra11,           {229}
                    enExtra12,           {230}
                    enExtra13,           {231}
                    enExtra14,           {232}
                    {Entidades para Evaluaci�n Desempe�o}
                    enEscalas,           {233}
                    enEscNivel,          {234}
                    enPreguntas,         {235}
                    enEncNive,           {236}
                    enEncEsca,           {237}
                    enEncuesta,          {238}
                    enEncComp,           {239}
                    enEncPreg,           {240}
                    enEncRela,           {241}
                    enSujeto,            {242}
                    enEvalua,            {243}
                    enSujComp,           {244}
                    enSujPreg,           {245}
                    enEvaComp,           {246}
                    enEvaPreg,           {247}
                    enVEvaComp,          {248}
                    enVEvaPreg,          {249}
                    enVCompetencias,     {250}
                    enVTCompete,         {251}
                    {Termina entidades para Evaluacion de desempe�o}
                    enAula,		           {252}
                    enReserva,           {253}
                    enCursoPre,          {254}
                    enInscrito,          {255}
                    enEventoAlta,        {256}
                    enSupuestosRH,       {257}
                    enCuboPresup,        {258}
                    enCtaBanco,          {259}
                    enCtaMovs,           {260}
                    enTCtaMovs,          {261}
                    enCertific,          {262}
                    enKarCert,           {263}
                    enPolTipo,           {264}
                    enPolHead,           {265}
                    enPlanVacacion,      {266}
                    enPerfilPuesto,      {267}
                    enSeccPerfil,        {268}
                    enDescPerfil,        {269}
                    enCamposPerfil,      {270}
                    enPlazas,            {271}
                    enPermutas,          {272}
                    enValPlant,          {273}
                    enValFact,           {274}
                    enValSubfact,        {275}
                    enValNiveles,        {276}
                    enValuacion,         {277}
                    enValPuntos,         {278}
                    enRSocial,           {279}
                    enKarInf,            {280} //Version 2008
                    enKarPlaza,          {281}
                    enACarAbo,           {282}
                    enFonCre,            {283}
                    enFonEmp,            {284}
                    enFonTot,            {285}
		                enCerProg,           {286} //Version 2008
		                enMatrizCertif,	 {287} //Version 2008
		                enNivelCertif,       {288} //Version 2008
		                enDerechosGrupoAd,   {289} //Version 2008
                    enBitacoraCafeteria, {290} //Version 2008
                    enBitacoraCafeteriaEmp, {291}//Version 2008
                    enVLiqEmp,           {292}
                    enBitacoraKiosco,    {293}
                    {..$ifdef ACS}
                    enNivel10, 	         {294}
                    enNivel11, 	         {295}
                    enNivel12, 	         {296}
                    {..$endif}
                    enReglaPresta,       {297}
                    enPrestaXRegla,      {298}
                    enHistRevision,      {299} //NO BORRAR  //ACL. Proyecto Cursos Fersinsa
                    enAccArbol,          {300} {CV:Proyecto Derechos de Acceso Cooper}
                    enACCDerechos,       {301} {CV:Proyecto Derechos de Acceso Cooper}
                    enAccEmpresa,        {302} {CV:Proyecto Derechos de Acceso Cooper}
                    enAccUsuarios,       {303} {CV:Proyecto Derechos de Acceso Cooper}
                    enOcupaNac,          {304}
                    enAreaTematica,      {305}
                    enEstablecimiento,   {306}
                    enMunicipio,         {307}
                    enDeclaraAnualCierre,{308}
                    enCostos,            {309}
                    enCHoras,            {310}
                    enDispositivos,      {311}
                    enDisXCom,           {312}
                    enMotCheca,          {313}
                    enMovimienBalanzaMensual, {314}
                    enNomMes,            {315}
                    enHueco316,           {316}
                    enTMaquina,           {317}
                    enMaquina ,           {318}
                    enLayout  ,           {319}
                    enLayMaq,             {320}
                    enLaySilla,           {321}
                    enSillaMaq,           {322}
                    enKarEmpSilla,        {323}
                    enMaqCert,            {324}
                    enEscenarios,         {325} // (JB) Escenarios de presupuestos T1060 CR1872
                    enDocumento,          {326} // (JB) Anexar Documento de Expediente de Empleado CR1880 T1107
                    enTransferencias,     {327}
                    enEdoCtaTransfer,     {328}
                    enMotivosTransfer,    {329}
                    enHuella,             {330}
                    enBitMisDatos,        {331}
                    enKiosImpr,           {332}
                    enClasifDoc,          {333}
                    enEncuestasKiosc,     {334}
                    enOpcionesEncuestaKiosc,{335}
                    enVotosEncuesta,      {336}
                    enResultadosEncuesta, {337}
                    enGruposCosteo,       {338}
                    enCriteriosCosteo,    {339}
                    enCriteriosPorConcepto,{340}
                    enSaldoVacaciones,    {341}
                    enPrevioISR,          {342}
                    enTPension,           {343}
                    enPensiones,          {344}
                    enPorcPensiones,      {345}
                    enCCostos,            {346}
                    enHueco347,           {347}
                    enHueco348,           {348}
                    enHueco349,           {349}
                    enHueco350,           {350}
                    enSynelAlarma,        {351}
                    enHueco352,           {352}
                    enCatSegGasMed,       {353}
                    enCatVigenciasSGM,    {354}
                    enHisSegGasMed,       {355}
                    enCatTablasCotSGM,    {356}
                    enCatCotizacionSGM,   {357}
                    enHueco358,           {358}
                    enHueco359,           {359}
                    enHueco360,           {360}
                    enWFCambioSalario,           {361}
                    enWFAutorizacion,           {362}
                    enWFCambioMultiple,           {363}
                    enWFPermisos,           {364}
                    enWFVacaciones,           {365}
                    enWFProcesos,           {366}
                    enHueco367,           {367}
                    enHueco368,           {368}
                    enHueco369,           {369}
                    enHueco370,           {370}
                    enHueco371,           {371}
                    enHueco372,           {372}
                    enHueco373,           {373}
                    enHueco374,           {374}
                    enHueco375,           {375}
                    enHueco376,           {376}
                    enHueco377,           {377}
                    enHueco378,           {378}
                    enHueco379,           {379}
                    enHueco380,           {380}
                    enHueco381,           {381}
                    enPropCost
                    );


var
    aTipoEntidad: array[ TipoEntidad ] of PChar = (
    '',
    'Acumulados Mensuales y Anuales',
    'Registro de Ahorros',
    'Cursos Anteriores',
    'Puestos Anteriores',
    'Asistencia Diaria',
    'Calendario de Cursos',
    'Detalle de Campos de Reportes',
    'Detalle de Checadas',
    'Clasificaciones\Tabulador de Salarios',
    'Empleados y Bajas',
    'Conceptos de N�mina',
    'Tipos de Contrato',
    'Cursos',
    'Estado Civil',
    'Entidades Federativas',
    'Matriz de Entrenamiento',
    'Grados de Estudio',
    'Eventos (Cambios Globales)',
    'Adicional #1',
    'Adicional #2',
    'Adicional #3',
    'Adicional #4',
    'Excepciones de Faltas',
    'D�as Festivos',
    'Folios de N�minas',
    'Variables Globales, Defaults...',
    'Horarios Diarios',
    'Foto e Im�genes',
    'Registros de Incapacidad',
    'Incidencias Diarias',
    'Otras Percepciones Fijas',
    'Kardex de Cursos',
    'Movimientos de Kardex',
    'IMSS/INFO: Totales por Empleado',
    'IMSS/INFO: Totales Empresa',
    'IMSS/INFO: Detalle de Movimientos',
    'Maestros de Cursos',
    'Denominaciones de Moneda',
    'Motivos de Baja',
    'Movimientos de N�mina',
    'Nivel de Organigrama #1',
    'Nivel de Organigrama #2',
    'Nivel de Organigrama #3',
    'Nivel de Organigrama #4',
    'Nivel de Organigrama #5',
    'Nivel de Organigrama #6',
    'Nivel de Organigrama #7',
    'Nivel de Organigrama #8',
    'Nivel de Organigrama #9',
    'Registro de N�minas',
    'Percepciones Fijas',
    'Parientes del Empleado',
    'Cargos\Abonos sobre Pr�stamos',
    'Per�odos de N�mina',
    'Registro de Permisos',
    'Prestaciones por A�o',
    'Registro de Pr�stamos',
    'Prima de Riesgos',
    'Puestos',
    'Condiciones',
    'Datos Generales de Reportes',
    'Registros Patronales',
    'Prestaciones de Ley',
    'Tipos de Ahorro',
    'Tipos de Curso',
    'Tipos de Movimiento de Kardex',
    'Tipos de Pr�stamo',
    'Medio de Transporte',
    'Turnos',
    'Historial de Vacaciones',
    'Personas con Qui�n Vive',
    'Tipo de Habitaci�n',
    'Derechos de Acceso',
    'Impuestos/Num�rica',
    'Bit�cora de Errores',
    'Datos B�sicos de Empresa',
    'Diccionario de Datos',
    'Grupos de Usuarios',
    'Primas y Topes de Ley',
    'ISPT y Num�ricas',
    'Resultados del Poll',
    'Riesgo de Trabajo',
    'Salarios M�nimos',
    'Matriz de Entrenamiento por Nivel',
    'Cat�logo de Usuarios',
    'Ninguno',
    'Rastreo de Prom. de variables',
    'Proveedores de Capacitaci�n',
    'Balanza de Movimientos',
    '',
    'Estado de Cuenta de Ahorro',
    'Estado de Cuenta de Pr�stamo',
    'AusenciaRL',
    'ChecadasRL',
    'Cursos Programados',
    'P�liza Contable',
    'Procesos',
    'Par�metros de N�mina',
    'Tablas Fijas',
    'Movimientos Globales',
    'Formulas',
    'Horarios Temporales',
    'Checadas Globales',
    'Lista de Funciones',
    'Arbol',
    'Estad�stica Demogr�fica',
    'Reparto de Ahorro',
    'Aguinaldo',
    'Rotaci�n',
    'Comparaci�n de ISPT Anual',
    'Declaraci�n de Cr�dito al Salario',
    'Reparto de PTU',
    'Reglas',
    'Invitaciones',
    'Comidas',
    'Invitadores',
    'Impresoras',
    'Asignaci�n de Niveles',
    'Niveles por Supervisor',
    'Movimientos Globales',
    'Excepciones de D�as/Horas Globales',
    'Tipos de Cambio',
    'Autorizaciones Extras y Permisos',
    'Presupuesto de Personal',
    'Correo',
    'Bit�cora de Usuario',
    'Pago de Recibos - N�mina',
    'Criterio de Orden para Folios',
    'Tipos de Periodo',
    'Autorizaciones',
    'Calendario Mensual',
    'Totales de Movimientos',
    'Conciliaci�n de Folios',
    'Lista de Asistencia',
    'Modulador #1',
    'Modulador #2',
    'Modulador #3',
    'Modulador #4',
    'Clasificaci�n de Partes',
    'Clasificaci�n de Operaci�n',
    'Partes',
    'Operaci�n',
    'Ordenes Fijas',
    'Operaciones Realizadas',
    'Lecturas',
    'Pasos Est�ndar',
    'Pasos',
    'Area',
    'Orden de Trabajo',
    'enTemporalNomina',
    'enCursoGlobal',
    'C�dula',
    'Cedula-Empleado',
    'C�dula-Orden Trabajo',
    'Calendario Mensual Horas',
    'Tarjeta XY',
    'Confidencialidad',
    'Herramientas',
    'Motivo de Devoluci�n',
    'Tallas',
    'Historial de Herramientas',
    'Listado de N�mina',
    'Suscripci�n a Reportes',
    'Breaks',
    'Breaks por Hora',
    'Claves',
    'Impresoras',
    'Foto',
    'Expediente',
    'Consulta',
    'Embarazo',
    'Accidente',
    'Medicinas',
    'Medicinas Entregadas',
    'Tipo de Accidente',
    'Causas de Accidente',
    'Tipo de Estudio',
    'Tipo de Consulta',
    'Diagn�stico',
    'Agrupaciones de Expediente',
    'Campos de Expediente',
    'Areas por Supervisor',
    'Kardex de Areas',
    'Motivos de Accidente',
    'Clase de Defectos',
    'C�dula de Inspecci�n',
    'Defectos',
    'Clase de Cursos',
    'Percepciones Fijas por Puesto',
    'Herramientas por Puesto',
    'C�dulas de Scrap',
    'Componentes',
    'Motivos de Scrap',
    'Scrap',
    'Tipos de Competencia',
    'Tabla de Calificaciones',
    'Dimensiones',
    'Acciones',
    'Familias',
    'Niveles',
    'Dimensiones por Puesto',
    'Competencias / Habilidades',
    'Competencias por Familia',
    'Competencias por Calificaci�n',
    'Competencias por Puesto',
    'Competencias por Empleado',
    'Plan por Empleado',
    'Mapa de Competencias',
    'Dimensiones por Nivel',
    'Declaraci�n Anual',
    'Grupo',
    'Mis Reportes Favoritos',
    'Programaci�n Individual Cursos',
    'Solicitud',
    'Vigencia Impuestos/Num�rica',
    'Reglas de Control de Caseta',
    'Checadas de Control de Caseta',
    'Clasificaciones Temporales',
    'Entidad Nacimiento',
    'Colonias',
    'Agrupaciones Adicionales',
    'Contenedor Campos Adicionales',
    'Tabla Adicional #5',
    'Tabla Adicional #6',
    'Tabla Adicional #7',
    'Tabla Adicional #8',
    'Tabla Adicional #9',
    'Tabla Adicional #10',
    'Tabla Adicional #11',
    'Tabla Adicional #12',
    'Tabla Adicional #13',
    'Tabla Adicional #14',
    'Cat�logo de escalas',
    'Respuestas de escala',
    'Cat�logo de preguntas',
    'Niveles de escala por encuesta',
    'Escalas por encuesta',
    'Encuestas',
    'Criterios a evaluar',
    'Preguntas por criterio',
    'Perfil de evaluadores',
    'Empleados a evaluar',
    'Evaluaciones',
    'Resumen de criterios por evaluado',
    'Resumen de preguntas por evaluado',
    'Criterios evaluados',
    'Preguntas por Evaluaci�n',
    'An�lisis por criterio',
    'An�lisis por pregunta',
    'Competencias / Habilidades',
    'Tipos de Competencia',
    'Aulas',
    'Reservaciones de aula',
    'Prerrequisitos por Curso',
    'Empleados Inscritos',
    'Contrataciones de Presupuesto',
    'Supuestos de Empleados',
    'Cubo de Totales de Presupuesto',
    'Cuentas Bancarias',
    'Movtos de Cuentas Bancarias',
    'Tipo Movto Cuenta Bancaria',
    'Certificaciones',
    'Kardex de certificaciones',
    'Tipos de P�liza',
    'Encabezado de P�liza',
    'Plan de vacaciones',
    'Perfil de Puesto',
    'Secciones de Perfil',
    'Descripciones de Perfil',
    'Campos por Secci�n',
    'Plazas',
    'Permutas de Asistencia',
    'Plantilla de Valuaci�n',
    'Factores de Valuaci�n',
    'Subfactores de Valuaci�n',
    'Niveles de Valuaci�n',
    'Valuaci�n de Puestos',
    'Puntos de Valuaci�n',
    'Razones Sociales',
    'Expediente Infonavit',
    'Cambios de Plaza',
    'Cargos\Abonos sobre Ahorros',
    'Totales de cr�dito de Fonacot',
    'Totales de empleado de Fonacot',
    'Totales de empresa de Fonacot',
    'Certificaciones Programadas',
    'Matriz de Certificaciones',
    'Certificaciones X Nivel',
    'Derechos Grupos Adicionales',
    'Bit�cora Cafe (Empresas)',
    'Bit�cora Cafe (Empresa activa)',
    'Conciliaci�n Infonavit',
    'Bit�cora de Kiosco',  
    'enNivel10',
    'enNivel11',
    'enNivel12',
    'Reglas para Pr�stamos ',
    'Reglas por Pr�stamos',
    'Historial de Revisi�',
    'Ocupaci�n Nacional',
    'Areas Tem�ticas',
    'Establecimientos',
    'enHueco303',
    'enHueco304',
    'enHueco305',
    'enHueco306',
    'enHueco307',
    'enHueco308',
    'enHueco309',
    'enHueco310',
    'Lista de Dispositivos',
    'Dispositivos por Empresa ',
    'Motivo de checadas Manuales',
    'Balanza de Movimientos Mensual',
    'Ultima N�mina de Balanza',
    'enHueco316', //En Corporativa se utiliza para V_ENROLA, dejar este hueco en Profesional
    'Clase de M�quinas',
    'M�quinas',
    'Plantillas',
    'M�quinas de Plantillas',
    'Sillas de Plantillas',
    'Sillas Asignadas a M�quinas',
    'Kardex de Empleado-M�quina',
    'Certificaciones de M�quinas',
    'enHueco325', // (JB) Escenarios de presupuestos T1060 CR1872
    'Documento', // (JB) Anexar Documento de Expediente de Empleado CR1880 T1107
    'Transferencias de Empleados',
    'Edo Cta Transferencias',
    'Motivos de Transferencia',
    'Huellas',
    'Bitacora Mis Datos',
    'Impresi�n Kiosco',
    'Expediente Documentos',
    'Encuestas de Kiosco',
    'Opciones de Encuesta de Kiosco',
    'Votos de Encuesta',
    'Resultados de Encuesta',
    'Grupos de Costeo',
    'Criterios de Costeo',
    'Criterios Por Concepto',
    'Saldo de Vacaciones',
    'C�lculo previo de ISR',
    'Tipos de Pensi�n',           {343}
    'Historial de Pensiones',     {344}
    'Porcentajes por Pensi�n',     {345}
    'Centro de Costos',           {346}
    'enHueco347',           {347}
    'enHueco348',           {348}
    'enHueco349',           {349}
    'enHueco350',           {350}
    'Synel Alarmas',        {351}
    'enHueco352',           {352}
    'Seguros de Gastos M�dicos',  {353}
    'Vigencias de SGM',           {354}
    'Historial de Seguros de Gastos M�dicos', {355}
    'Tablas de Cotizaci�n SGM',{356}
    'Costos de Cotizaci�n SGM', {357}
    'enHueco358',           {358}
    'enHueco359',           {359}
    'enHueco360',           {360}
    'enHueco361',           {361}
    'enHueco362',           {362}
    'enHueco363',           {363}
    'enHueco364',           {364}
    'enHueco365',           {365}
    'enHueco366',           {366}
    'enHueco367',           {367}
    'enHueco368',           {368}
    'enHueco369',           {369}
    'enHueco370',           {370}
    'enHueco371',           {371}
    'enHueco372',           {372}
    'enHueco373',           {373}
    'enHueco374',           {374}
    'enHueco375',           {375}
    'enHueco376',           {376}
    'enHueco377',           {377}
    'enHueco378',           {378}
    'enHueco379',           {379}
    'enHueco380',           {380}
    'enHueco381',            {381}
    'enPropCost'
     ) ;
