unit ZetaEdit_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, Menus, DB,
     DBCtrls, Comctrls, Clipbrd,
     ZetaCommonTools ,ZetaCommonLists, cxButtons;

type
  TZetaEdit_DevEx = class(TCustomEdit)
  private
    { Private declarations }
    FLookUpBtn: TcxButton;
    FTrimAfterPaste: Boolean;
    function GetValor: String;
    procedure SetLookUpBtn( Value: TcxButton );
    procedure SetValor( const Value: String );
    procedure WMCopy( var Message: TMessage ); message WM_COPY;
    procedure WMCut( var Message: TMessage ); message WM_CUT;
    procedure WMPaste( var Message: TMessage ); message WM_PASTE;
    procedure CNKeyDown(var Message: TWMKeyDown); message CN_KEYDOWN;
  protected
    { Protected declarations }
    function ValidCharacter(const cValue: Char): Boolean; virtual;
    function ValidEdit: Boolean; virtual;
    procedure DoEnter; override;
    procedure DoEdit;
    procedure Enfocar;
    procedure KeyDown( var Key: Word; Shift: TShiftState); override;
    procedure KeyPress( var Key: Char ); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure Notification( AComponent: TComponent; Operation: TOperation ); override;
  public
    { Public declarations }
    property Valor: String read GetValor write SetValor;
    constructor Create( AOwner: TComponent ); override;
    procedure CopyToClipboard;
    procedure CutToClipboard;
    procedure PasteFromClipboard;
  published
    { Published declarations }
    property AutoSelect;
    property AutoSize;
    property BorderStyle;
    property CharCase;
    property Color;
    property Ctl3D;
    property DragCursor;
    property DragMode;
    property Enabled;
    property Font;
    property HideSelection;
    property ImeMode;
    property ImeName;
    property LookUpBtn: TcxButton read FLookUpBtn write SetLookUpBtn;
    property MaxLength;
    property OEMConvert;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PasswordChar;
    property PopupMenu;
    property ReadOnly;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Text;
    property Visible;
    property OnChange;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDrag;
    property TrimAfterPaste: Boolean read FTrimAfterPaste write FTrimAfterPaste default True;
  end;
  TZetaDBEdit_DevEx = class( TZetaEdit_DevEx )
  private
    { Private declarations }
    FDataLink: TFieldDataLink;
    FConfirmEdit: Boolean;
    FConfirmMsg: String;
    function GetDataField: String;
    function GetDataSource: TDataSource;
    function GetField: TField;
    function GetReadOnly: Boolean;
    function GetConfirmMsg: String;
    procedure SetDataField( const Value: string );
    procedure SetDataSource( Value: TDataSource );
    procedure SetReadOnly( const Value: Boolean );
    procedure CheckFieldType( const Value: String );
    procedure DataChange( Sender: TObject );
    procedure EditingChange( Sender: TObject );
    procedure UpdateData( Sender: TObject );
    procedure ActiveChange( Sender: TObject );
  protected
    { Protected declarations }
    function ValidCharacter(const cValue: Char): Boolean; override;
    function ValidEdit: Boolean; override;
    procedure Notification( AComponent: TComponent; Operation: TOperation ); override;
    procedure Change; override;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    property Field: TField read GetField;
  published
    { Published declarations }
    property ConfirmEdit: Boolean read FConfirmEdit write FConfirmEdit default False;
    property ConfirmMsg: String read FConfirmMsg write FConfirmMsg;
    property DataField: string read GetDataField write SetDataField;
    property DataSource: TDataSource read GetDataSource write SetDataSource;
    property ReadOnly: Boolean read GetReadOnly write SetReadOnly default False;
  end;

implementation

uses ZetaMsgDlg, ZetaCommonClasses;

{ ********* TZetaEdit_DevEx **************** }

constructor TZetaEdit_DevEx.Create( AOwner: TComponent );
begin
     inherited Create( AOwner );
     Valor := VACIO;
     TrimAfterPaste := true;
end;

procedure TZetaEdit_DevEx.Notification( AComponent: TComponent; Operation: TOperation );
begin
     inherited Notification( AComponent, Operation );
     if ( Operation = opRemove ) then
     begin
          if ( AComponent = LookUpBtn ) then
             LookUpBtn := nil;
     end;
end;

function TZetaEdit_DevEx.GetValor: String;
begin
     Result := Text;
end;

procedure TZetaEdit_DevEx.SetValor( const Value: String );
begin
     if ( Value <> Text ) then
     begin
          Text := Value;
     end;
end;

procedure TZetaEdit_DevEx.SetLookUpBtn( Value: TcxButton );
begin
     if ( Value <> nil ) then
        Value.SpeedButtonOptions.CanBeFocused := FALSE; //DevEx(by am): Los botones de DevExpress por defecto pueden ser enfocados.
     if ( FLookUpBtn <> Value ) then
     begin
          FLookUpBtn := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

procedure TZetaEdit_DevEx.DoEnter;
begin
     Enfocar;
     inherited DoEnter;
end;

function TZetaEdit_DevEx.ValidEdit: Boolean;
begin
     Result := True;
end;

procedure TZetaEdit_DevEx.DoEdit;
begin
     if not Focused then
        Enfocar;
end;

procedure TZetaEdit_DevEx.MouseDown( Button: TMouseButton; Shift: TShiftState; X, Y: Integer );
begin
     inherited MouseDown( Button, Shift, X, Y );
     if ( Button = mbLeft ) then
        SelectAll;
end;

procedure TZetaEdit_DevEx.KeyDown( var Key: Word; Shift: TShiftState);
begin
     if ( Key = VK_DELETE ) and ( Shift = [] ) then
     begin
          if ValidEdit then
             DoEdit
          else
              Key := 0;
     end;
     inherited KeyDown( Key, Shift );
end;

function TZetaEdit_DevEx.ValidCharacter( const cValue: Char ): Boolean;
begin
     Result := True;
end;

procedure TZetaEdit_DevEx.KeyPress( var Key: Char );
begin
     if ( Key <> Chr( VK_RETURN ) ) then { ENTER as TAB }
     begin
          if not ValidCharacter( Key ) then
             Key := Chr( 0 )
          else
              if ValidEdit then
                 DoEdit
              else
                  Key := Chr( 0 );
     end;
     inherited KeyPress( Key );
end;

procedure TZetaEdit_DevEx.WMCopy( var Message: TMessage );
begin
     CopyToClipboard;
end;

procedure TZetaEdit_DevEx.WMPaste( var Message: TMessage );
begin
     PasteFromClipboard;
end;

procedure TZetaEdit_DevEx.WMCut( var Message: TMessage );
begin
     CutToClipboard;
end;

procedure TZetaEdit_DevEx.CNKeyDown(var Message: TWMKeyDown);
begin
     with Message do
     begin
          if ( Charcode = VK3_F ) and ( ssCtrl in KeyDataToShiftState( KeyData ) ) then
          begin
               if Assigned( FLookUpBtn ) then
               begin
                    Result := 1;
                    FLookUpBtn.Click;
               end;
          end
          else
              inherited;
     end;
end;

procedure TZetaEdit_DevEx.Enfocar;
begin
     if CanFocus then
     begin
          SetFocus;
          SelectAll;
     end;
end;

procedure TZetaEdit_DevEx.CopyToClipboard;
begin
     Clipboard.AsText := Text;
end;

procedure TZetaEdit_DevEx.CutToClipboard;
begin
     if not ReadOnly and ValidEdit then
     begin
          CopyToClipboard;
          Valor := VACIO;
     end;
end;

procedure TZetaEdit_DevEx.PasteFromClipboard;
begin
     with Clipboard do
     begin
          if not ReadOnly and HasFormat( CF_TEXT ) and ValidEdit then
          begin
               Valor := AsText;
               if TrimAfterPaste then
                  Valor := Trim (Valor);
          end;
     end;
end;

{ ************ TZetaDBEdit_DevEx ************** }

constructor TZetaDBEdit_DevEx.Create( AOwner : TComponent );
begin
     inherited Create( AOwner );
     FConfirmEdit := False;
     FConfirmMsg := '';
     FDataLink := TFieldDataLink.Create;
     with FDataLink do
     begin
          Control := Self;
          OnDataChange := DataChange;
          OnEditingChange := EditingChange;
          OnUpdateData := UpdateData;
          OnActiveChange := ActiveChange;
     end;
     if not ( csDesigning in ComponentState ) then
        CheckFieldType( DataField );
end;

destructor TZetaDBEdit_DevEx.Destroy;
begin
     with FDataLink do
     begin
          OnActiveChange := nil;
          OnUpdateData := nil;
          OnEditingChange := nil;
          OnDataChange := nil;
          Control := nil;
          Free;
     end;
     FDataLink := nil;
     inherited Destroy;
end;

procedure TZetaDBEdit_DevEx.Notification( AComponent: TComponent; Operation: TOperation );
begin
     inherited Notification( AComponent, Operation );
     if ( Operation = opRemove ) then
        if ( FDataLink <> nil ) and ( AComponent = DataSource ) then
           DataSource := nil;
end;

function TZetaDBEdit_DevEx.GetDataField: String;
begin
     Result := FDataLink.FieldName;
end;

function TZetaDBEdit_DevEx.GetField: TField;
begin
     Result := FDataLink.Field;
end;

function TZetaDBEdit_DevEx.GetDataSource: TDataSource;
begin
     Result := FDataLink.DataSource;
end;

procedure TZetaDBEdit_DevEx.SetDataField( const Value: String );
begin
     CheckFieldType( Value );
     FDataLink.FieldName := Trim( Value );
end;

procedure TZetaDBEdit_DevEx.SetDataSource( Value: TDataSource );
begin
     if ( FDataLink.DataSource <> Value ) then
     begin
          FDataLink.DataSource := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

function TZetaDBEdit_DevEx.GetReadOnly: Boolean;
begin
     Result := FDataLink.ReadOnly;
end;

procedure TZetaDBEdit_DevEx.SetReadOnly( const Value: Boolean );
begin
     FDataLink.ReadOnly := Value;
end;

function TZetaDBEdit_DevEx.GetConfirmMsg: String;
begin
     if ( FConfirmMsg = VACIO ) then
        Result := '� Desea Cambiar Este C�digo ?'
     else
         Result := FConfirmMsg;
end;

procedure TZetaDBEdit_DevEx.CheckFieldType( const Value: String );
begin
     if ( Value <> VACIO ) and
        ( FDataLink <> nil ) and
        ( FDataLink.Dataset <> nil ) and
        ( FDataLink.Dataset.Active ) then
     begin
          with FDataLink.Dataset.FieldByName( Value ) do
          begin
               if not ( DataType in [ ftString ] ) then
                  raise EInvalidFieldType.Create( 'TZetaDBEdit.DataField can only ' +
                                                  'be connected to columns of type ' +
                                                  'String' );
               MaxLength := Size;
          end;
     end;
end;

procedure TZetaDBEdit_DevEx.DataChange( Sender: TObject );
begin
     if ( FDataLink.Field = nil ) then
        Valor := VACIO
     else
         if FDataLink.Field.IsNull then
            Valor := VACIO
         else
             case FDataLink.Field.DataType of
                  ftString: Valor := FDataLink.Field.AsString;
             end;
end;

procedure TZetaDBEdit_DevEx.EditingChange(Sender: TObject);
begin
     if Focused and FDataLink.Editing then
     begin
          Enfocar;
     end;
end;

procedure TZetaDBEdit_DevEx.UpdateData(Sender: TObject);
begin
     if ( FDataLink.Field <> nil ) then
     begin
          case FDataLink.Field.DataType of
               ftString:
               begin
                    with FDataLink.Field do
                    begin
                         if ( AsString <> Valor ) then
                            AsString := Valor;
                    end;
               end;
          end;
     end;
end;

procedure TZetaDBEdit_DevEx.ActiveChange( Sender: TObject );
begin
     if ( FDataLink <> nil ) and FDataLink.Active then
        CheckFieldType( DataField );
end;

procedure TZetaDBEdit_DevEx.Change;
begin
     if ( FDataLink <> nil ) then
        FDataLink.Modified;
     inherited Change;
end;

function TZetaDBEdit_DevEx.ValidCharacter( const cValue: Char ): Boolean;
begin
     if ( cValue = UnaComilla ) then
        Result := False
     else
         Result := inherited ValidCharacter( cValue );
end;

function TZetaDBEdit_DevEx.ValidEdit;
begin
     if ( FDataLink <> nil ) then
     begin
          if FDataLink.Editing then
             Result := True
          else
          begin
               if not FConfirmEdit or ConfirmaCambio( GetConfirmMsg) then
                  Result := FDataLink.Edit
               else
                   Result := False;
          end;
     end
     else
         Result := True;
     if Result then
        Result := inherited ValidEdit;
end;

end.
