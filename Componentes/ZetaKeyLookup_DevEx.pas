unit ZetaKeyLookup_DevEx;

interface

{$R *.RES }

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, ExtCtrls, Buttons, DB, DBCtrls, Clipbrd,
     ZetaClientDataset,
     ZetaDBTextBox,
     ZetaSmartLists,cxbuttons,cxLookAndFeels;

type
  TZetaKeyLookup_DevEx = class;
  TZetaLlave_DevEx = class( TEdit )
  private
    { Private declarations }
    function GetKeyLookup: TZetaKeyLookup_DevEx;
    procedure CNKeyDown(var Message: TWMKeyDown); message CN_KEYDOWN; // De TWinControl.CNKeyDown //
    procedure WMCopy( var Message: TMessage ); message WM_COPY;
    procedure WMCut( var Message: TMessage ); message WM_CUT;
    procedure WMPaste( var Message: TMessage ); message WM_PASTE;

  protected
    { Protected declarations }
    procedure KeyDown( var Key: Word; Shift: TShiftState); override;
    procedure KeyPress( var Key: Char ); override;
  public
    { Public declarations }

    property KeyLookup: TZetaKeyLookup_DevEx read GetKeyLookup;
  published
    { Published declarations }
  end;
  TZetaKeyLookup_DevEx = class( TCustomPanel )
  private
    { Private declarations }
    FDevEx:Boolean;
    FLlave: TZetaLlave_DevEx;
    FOldKey: String;
    FOldDescription: String;
    FSetting: Boolean;
    FChanged: Boolean;
    FDescripcion: TZetaTextBox;
    FBuscar: TcxButton;
    FLookupDataset: TZetaLookupDataset;
    FFiltro: String;
    FOpcional: Boolean;
    FReadOnly: Boolean;
    FEditarSoloActivos: Boolean;
    FIgnorarConfidencialidad : Boolean;
    FWidthLlave: Integer;
    FOnValidKey: TNotifyEvent;
    FOnValidLookup: TNotifyEvent;
    function DoLookup( const lSetFocus: Boolean ): Boolean;
    function GetLlave: String;
    function GetLlaveValor: String;
    function GetDescripcion: String;
    function GetEditTabStop: Boolean;
    function GetValor: Integer;
    function HayLookupDataset: Boolean;
    procedure SetDescripcion( const Value: String );
    procedure SetEditTabStop(const Value: Boolean);
    procedure SetFiltro( const Value: String );
    procedure SetLlave( const Value: String );
    procedure SetLlaveLookUp( const Value: String; const lLookUp : Boolean);
    procedure SetLookUpDataSet( Value: TZetaLookupDataset );
    procedure SetValor( const Value: Integer );
    procedure SetWidthLlave( const Value: Integer );
    procedure DoSearch( Sender: TObject );
    procedure DoValidKey;
    procedure DoValidLookup;
    procedure WMSize( var Msg: TWMSize ); message WM_SIZE;
    procedure WMSetFocus(var Message: TWMSetFocus); message WM_SETFOCUS;
    procedure CMEnabledChanged(var Message: TMessage); message CM_ENABLEDCHANGED;
    procedure CMFontChanged( var Msg: TMessage ); message CM_FONTCHANGED;
    procedure PositionControls;
  protected
    { Protected declarations }
    FKeySizeSet: Boolean;
    function VerifyData: Boolean; virtual;
    function ValidEdit: Boolean; virtual;
    procedure DoChange( Sender: TObject ); virtual;
    procedure DoEnter; override;
    procedure DoEdit;
    procedure DoExit; override;
    procedure Enfocar;
    procedure Notification( AComponent: TComponent; Operation: TOperation ); override;
    procedure SetKeySize( const iValue: Integer );
    procedure SetTextValue( const Value: String );
    procedure SetReadOnly( const Value: Boolean ); virtual;
  public
    { Public declarations }
    property Descripcion: String read GetDescripcion;
    property Llave: String read GetLlave write SetLlave;
    property Valor: Integer read GetValor write SetValor;
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    procedure CopyToClipboard;
    procedure CutToClipboard;
    procedure PasteFromClipboard;
    procedure ResetMemory;
    procedure Resize; override;
    procedure SetLlaveDescripcion( const sLlave, sDescripcion: String );

  published
    { Published declarations }
    property Align;
    property Enabled;
    property Filtro: String read FFiltro write SetFiltro;
    property LookupDataset: TZetaLookupDataset read FLookupDataset write SetLookupDataset;
    property Opcional: Boolean read FOpcional write FOpcional default True;
    property ReadOnly: Boolean read FReadOnly write SetReadOnly default False;
    property EditarSoloActivos: Boolean read FEditarSoloActivos write FEditarSoloActivos default True;
    property IgnorarConfidencialidad: Boolean read FIgnorarConfidencialidad write FIgnorarConfidencialidad default True;
    property ShowHint;
    property TabOrder;
    property TabStop: Boolean read GetEditTabStop write SetEditTabStop;
    property Visible;
    property WidthLlave: Integer read FWidthLlave write SetWidthLlave;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
    property OnStartDrag;
    property OnValidKey: TNotifyEvent read FOnValidKey write FOnValidKey;
    property OnValidLookup: TNotifyEvent read FOnValidLookup write FOnValidLookup;
  end;
  TZetaDBKeyLookup_DevEx = class( TZetaKeyLookup_DevEx )
  private
    { Private declarations }
    FDataLink: TFieldDataLink;
    function GetDataField: String;
    function GetDataSource: TDataSource;
    function GetField: TField;
    procedure SetDataField( Const Value: string );
    procedure SetDataSource( Value: TDataSource );
    procedure CheckFieldType( const Value: String );
    procedure DataChange( Sender: TObject );
    procedure EditingChange( Sender: TObject );
    procedure UpdateData( Sender: TObject );
    procedure ActiveChange( Sender: TObject );
  protected
    { Protected declarations }
    function VerifyData: Boolean; override;
    function ValidEdit: Boolean; override;
    procedure DoChange( Sender: TObject ); override;
    procedure Notification( AComponent: TComponent; Operation: TOperation ); override;
    procedure SetReadOnly( const Value: Boolean ); override;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    property Field: TField read GetField;
    procedure PUpdateData();
  published
    { Published declarations }
    property DataField: string read GetDataField write SetDataField;
    property DataSource: TDataSource read GetDataSource write SetDataSource;
  end;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses;

const
     K_LLAVE_VACIA = '';
     K_LLAVE_CERO = '0';

{ ************ TZetaLlave ************** }

function TZetaLlave_DevEx.GetKeyLookup: TZetaKeyLookup_DevEx;
begin
     Result := TZetaKeyLookup_DevEx( Parent );
end;

procedure TZetaLlave_DevEx.WMCopy( var Message: TMessage );
begin
     KeyLookup.CopyToClipboard;
end;

procedure TZetaLlave_DevEx.WMPaste( var Message: TMessage );
begin
     KeyLookup.PasteFromClipboard;
end;

procedure TZetaLlave_DevEx.WMCut( var Message: TMessage );
begin
     KeyLookup.CutToClipboard;
end;

procedure TZetaLlave_DevEx.CNKeyDown(var Message: TWMKeyDown);
begin
     with Message do
     begin
          if ( Charcode = VK3_F ) and ( ssCtrl in KeyDataToShiftState( KeyData ) ) then
          begin
               Result := 1;
               KeyLookup.DoSearch( KeyLookup );
          end
          else
              inherited;
     end;
end;

procedure TZetaLlave_DevEx.KeyDown( var Key: Word; Shift: TShiftState );
begin
     case Key of
          VK_DELETE:
          begin
               if ( Shift = [] ) then
                  KeyLookup.DoEdit;
          end;
     end;
     inherited KeyDown( Key, Shift );
end;

procedure TZetaLlave_DevEx.KeyPress( var Key: Char );
begin
     case Key of
          #32..#255: KeyLookup.DoEdit;
          Chr( VK_BACK ): KeyLookup.DoEdit;
     end;
     inherited KeyPress( Key );
end;

{ ************ TZetaKeyLookup_DevEx ************** }

constructor TZetaKeyLookup_DevEx.Create( AOwner: TComponent );
begin
     inherited Create( AOwner );
     FDevEx :=True;
     FSetting := False;
     FChanged := False;
     FOpcional := True;
     FOldKey := VACIO;
     FOldDescription := VACIO;
     BorderStyle := bsNone;
     BevelInner := bvNone;
     BevelOuter := bvNone;
     Caption := '';
     ControlStyle := ControlStyle - [ csAcceptsControls, csSetCaption ];
     FLlave := TZetaLlave_DevEx.Create( Self );
     with FLlave do
     begin
          Parent := Self;
          Name := Self.Name + 'FLlave';
          Text := '';
          CharCase := ecUpperCase;
          OnChange := DoChange;
     end;
     FDescripcion := TZetaTextBox.Create( Self );
     with FDescripcion do
     begin
          Parent := Self;
          Name := Self.Name + 'FDescripcion';
          Color := clInfoBk;
          Caption := '';
     end;
     FBuscar := TcxButton.Create( Self );
     with FBuscar do
     begin
          SpeedbuttonOptions.CanBeFocused:=false;
          Height:=23;
          Width:=23;
          Parent := Self;
          Name := Self.Name + 'FBuscar';
          Visible := True;
          ShowHint := True;
          onClick := DoSearch;
          LookAndFeel.SkinName :='TressMorado2013';
          fbuscar.Default:=true;
          fbuscar.GroupIndex:=1;
          fbuscar.Down:=true;
          fbuscar.AllowAllUp:=true;
          OptionsImage.Glyph.LoadFromResourceName( HInstance, 'zoom' );
          Caption:='';
     end;
     FWidthLlave := 60;
     FKeySizeSet := False;
     Height := 21;
     Width := 300;
end;

destructor TZetaKeyLookup_DevEx.Destroy;
begin
     inherited Destroy;
end;

procedure TZetaKeyLookup_DevEx.ResetMemory;
begin
     FOldKey := VACIO;
end;

procedure TZetaKeyLookup_DevEx.Notification( AComponent: TComponent; Operation: TOperation );
begin
     inherited Notification( AComponent, Operation );
     if ( Operation = opRemove ) then
     begin
          if ( LookupDataset <> nil ) and ( AComponent = LookupDataset ) then
             LookupDataset := nil;
     end;
end;

function TZetaKeyLookup_DevEx.GetDescripcion: String;
begin
     Result := FDescripcion.Caption;
end;

function TZetaKeyLookup_DevEx.GetLlaveValor: String;
begin
     Result := FLlave.Text;
     if not ZetaCommonTools.StrLleno( Result ) then
        Result := VACIO;
end;

function TZetaKeyLookup_DevEx.GetLlave: String;
begin
     if FChanged then
        Result := ''
     else
         Result := GetLlaveValor;
end;

procedure TZetaKeyLookup_DevEx.SetLookUpDataSet( Value: TZetaLookupDataset );
begin
     if ( FLookupDataset <> Value ) then
     begin
          FLookupDataset := Value;
          if ( Value = nil ) then
             FBuscar.Hint := Hint
          else
          begin
               with FLookupDataset do
               begin
                    FreeNotification( Self );
                    FBuscar.Hint := 'Buscar ' + LookupName + ' ( Ctrl-F )'
               end;
          end;
     end;
end;

function TZetaKeyLookup_DevEx.GetValor: Integer;
begin
     if ( Llave = VACIO ) then
        Result := 0
     else
         Result := StrAsInteger( Llave );
end;

function TZetaKeyLookup_DevEx.GetEditTabStop: Boolean;
begin
     Result := FLlave.TabStop;
end;

procedure TZetaKeyLookup_DevEx.SetEditTabStop(const Value: Boolean);
begin
     FLlave.TabStop := Value;
end;

procedure TZetaKeyLookup_DevEx.SetWidthLlave( const Value: Integer );
begin
     if ( FWidthLlave <> Value ) then
     begin
          FWidthLlave := Value;
          Resize;
     end;
end;

procedure TZetaKeyLookup_DevEx.SetDescripcion( const Value: String );
begin
     if ( Value <> FDescripcion.Caption ) then
        FDescripcion.Caption := Value;
end;

procedure TZetaKeyLookup_DevEx.SetTextValue( const Value: String );
begin
     FSetting := True;
     try
        FLlave.Text := Value;
     finally
            FSetting := False;
     end;
end;

procedure TZetaKeyLookup_DevEx.SetLlaveDescripcion( const sLlave, sDescripcion: String );
begin
     SetTextValue( sLlave );
     FOldKey := sLlave;
     FOldDescription := sDescripcion;
     SetDescripcion( sDescripcion );
end;

procedure TZetaKeyLookup_DevEx.SetLlave( const Value: String );
begin
     SetTextValue( Value );
     DoLookup( False );
end;


procedure TZetaKeyLookup_DevEx.SetLlaveLookUp( const Value: String ; const lLookUp : Boolean);
begin
     SetTextValue( Value );
     DoLookup( lLookUp );
end;

procedure TZetaKeyLookup_DevEx.SetFiltro( const Value: String );
begin
     if ( FFiltro <> Value ) then
     begin
          FFiltro := Value;
          DoLookup( False );
     end;
end;

procedure TZetaKeyLookup_DevEx.SetReadOnly( const Value: Boolean );
begin
     if ( FReadOnly <> Value ) then
     begin
          FReadOnly := Value;
          FLlave.ReadOnly := Value;
     end;
end;

procedure TZetaKeyLookup_DevEx.SetValor( const Value: Integer );
begin
     if ( Value <= 0 ) then
        SetLlave( K_LLAVE_VACIA )
     else
         SetLlave( IntToStr( Value ) );
end;

procedure TZetaKeyLookup_DevEx.SetKeySize( const iValue: Integer );
begin
     if not FKeySizeSet then
     begin
          FLlave.MaxLength := iValue;
          FKeySizeSet := True;
     end;
end;

function TZetaKeyLookup_DevEx.VerifyData: Boolean;
begin
     Result := DoLookup( True );
end;

function TZetaKeyLookup_DevEx.HayLookupDataset: Boolean;
begin
     Result := ( FLookupDataset <> nil );
end;

procedure TZetaKeyLookup_DevEx.DoSearch( Sender: TObject );
var
   sLlave, sDescripcion: String;
begin
     sLlave := Llave;
     sDescripcion := '';

     if (FLlave.CanFocus)  then
        FLlave.SetFocus;

     if Enabled and not FReadOnly and HayLookupDataset and FLlave.Focused and
        LookupDataset.Search_DevEx( FFiltro, sLlave, sDescripcion, TRUE, not IgnorarConfidencialidad ) then
     begin

          DoEdit;
          SetTextValue( sLlave );
          if( sllave<>'')then
          begin
          SetDescripcion( sDescripcion );
          end
          else
          begin
          setDescripcion('');
          end;
          if FLlave.Focused or Fbuscar.Focused then
             GetParentForm( Self ).Perform( WM_NEXTDLGCTL, 0, 0 ) // Para Salir Del Control //
          else
              DoValidKey;
     end;
end;

procedure TZetaKeyLookup_DevEx.DoEnter;
begin
     if HayLookupDataset then
        SetKeySize( LookupDataset.LookupKeyFieldSize );
  //   Enfocar;
end;

procedure TZetaKeyLookup_DevEx.DoEdit;
begin
     if ValidEdit then
     begin
          if not FLlave.Focused then
             Enfocar;
     end;
end;

function TZetaKeyLookup_DevEx.ValidEdit: Boolean;
begin
     Result := True;
end;

procedure TZetaKeyLookup_DevEx.DoExit;
begin
     if VerifyData then
     begin
          inherited DoExit;
          DoValidKey;
     end;
end;

procedure TZetaKeyLookup_DevEx.Enfocar;
begin
     with FLlave do
     begin
          if CanFocus then
          begin
               SelectAll;
               SetFocus;
          end;
     end;
end;

procedure TZetaKeyLookup_DevEx.DoChange( Sender: TObject );
begin
     if not FSetting then
        FChanged := True;
end;

procedure TZetaKeyLookup_DevEx.DoValidKey;
begin
     if Assigned( FOnValidKey ) then
        FOnValidKey( Self );
end;

procedure TZetaKeyLookup_DevEx.DoValidLookup;
begin
     if Assigned( FOnValidLookup ) then
        FOnValidLookup( Self );
end;

function TZetaKeyLookup_DevEx.DoLookup( const lSetFocus: Boolean ): Boolean;
var
   sLlave, sDescripcion: String;
   lKeyIsActive, lKeyIsConfidential: boolean;
begin
     Result := True;
     sLlave := GetLlaveValor;
     if ( sLlave = K_LLAVE_VACIA ) then
     begin
          SetDescripcion( '' );
          FChanged := False;
     end
     else
         if ( sLlave = FOldKey ) then
         begin
              SetDescripcion( FOldDescription );
              FChanged := False;
         end
         else
             if HayLookupDataset then
             begin
                  Result := LookupDataset.LookupKey( sLlave, Filtro, sDescripcion, lKeyIsActive, lKeyIsConfidential);
                  if Result then
                  begin
                       if not EditarSoloActivos then
                          lKeyIsActive := True;

                       if IgnorarConfidencialidad then
                          lKeyIsConfidential := False;

                       if ( lKeyIsActive and (not lKeyIsConfidential) ) or ( not lSetFocus ) then
                       begin
                          SetDescripcion( sDescripcion );
                          DoValidLookup;
                          FOldKey := sLlave;
                          FOldDescription := sDescripcion;
                          FChanged := False;
                       end
                       else
                       begin
                            Result := False;

                            if lKeyIsConfidential then
                               SetDescripcion( '???')
                            else
                               SetDescripcion( 'C�digo Inactivo!!!');

                            if lSetFocus then
                            begin
                                 MessageBeep( MB_ICONEXCLAMATION );
                                 Enfocar;
                            end;
                       end;
                  end
                  else
                  begin
                       SetDescripcion( '???' );
                       if lSetFocus then
                       begin
                            MessageBeep( MB_ICONEXCLAMATION );
                            Enfocar;
                       end;
                  end;
             end;
end;

procedure TZetaKeyLookup_DevEx.CMEnabledChanged(var Message: TMessage);
begin
     inherited;
     FLlave.Enabled := Self.Enabled;
     FDescripcion.Enabled := Self.Enabled;
     FBuscar.Enabled := Self.Enabled;
end;

procedure TZetaKeyLookup_DevEx.CMFontChanged( var Msg: TMessage );
begin
     PositionControls;
     inherited;
end;

procedure TZetaKeyLookup_DevEx.WMSize( var Msg: TWMSize );
begin
     inherited;
     Resize;
end;

procedure TZetaKeyLookup_DevEx.WMSetFocus(var Message: TWMSetFocus);
begin
     inherited;
     Enfocar;
end;

procedure TZetaKeyLookup_DevEx.Resize;
begin
     PositionControls;
     inherited Resize;
end;

procedure TZetaKeyLookup_DevEx.PositionControls;
var
   iWidth, iLeft: Integer;
begin
     iWidth := ( Self.Width - FWidthLlave - Self.Height - 6 );
     if ( iWidth < 0 ) then
        iWidth := 0;
     iLeft := 0;
     FLlave.SetBounds( iLeft, 0, FWidthLlave, Self.Height );
     iLeft := iLeft + ( FWidthLlave + 2 );
     FDescripcion.SetBounds( iLeft, 0, iWidth, Self.Height );
     iLeft := iLeft + ( iWidth + 2 );
     FBuscar.SetBounds( iLeft, 0, Self.Height, Self.Height );
end;

procedure TZetaKeyLookup_DevEx.CopyToClipboard;
begin
     Clipboard.AsText := Llave;
end;

procedure TZetaKeyLookup_DevEx.CutToClipboard;
begin
     if not ReadOnly and ValidEdit then
     begin
          CopyToClipboard;
          Llave := '';
     end;
end;

procedure TZetaKeyLookup_DevEx.PasteFromClipboard;
begin
     with Clipboard do
     begin
          if not ReadOnly and HasFormat( CF_TEXT ) then
          begin
               if HayLookupDataset and ValidEdit then
                  SetLlaveLookUp( AsText, True); {AV 19/11/2010: Cambio para enfocar y revisar activo al realizar paste}
          end;
     end;
end;

{ ************ TZetaDBKeyLookup ************** }

constructor TZetaDBKeyLookup_DevEx.Create( AOwner: TComponent );
begin
     inherited Create( AOwner );
     FDataLink := TFieldDataLink.Create;
     with FDataLink do
     begin
          Control := Self;
          OnDataChange := DataChange;
          OnEditingChange := EditingChange;
          OnUpdateData := UpdateData;
          OnActiveChange := ActiveChange;
     end;
     if not ( csDesigning in ComponentState ) then
        CheckFieldType( DataField );
end;

destructor TZetaDBKeyLookup_DevEx.Destroy;
begin
     with FDataLink do
     begin
          OnActiveChange := nil;
          OnUpdateData := nil;
          OnEditingChange := nil;
          OnDataChange := nil;
          Control := nil;
          Free;
     end;
     FDataLink := nil;
     inherited Destroy;
end;

procedure TZetaDBKeyLookup_DevEx.Notification( AComponent: TComponent; Operation: TOperation );
begin
     inherited Notification( AComponent, Operation );
     if ( Operation = opRemove ) then
        if ( FDataLink <> nil ) and ( AComponent = DataSource ) then
           DataSource := nil;
end;

function TZetaDBKeyLookup_DevEx.GetDataField: String;
begin
     Result := FDataLink.FieldName;
end;

function TZetaDBKeyLookup_DevEx.GetField: TField;
begin
     Result := FDataLink.Field;
end;

function TZetaDBKeyLookup_DevEx.GetDataSource: TDataSource;
begin
     Result := FDataLink.DataSource;
end;

procedure TZetaDBKeyLookup_DevEx.SetDataField( Const Value: string );
begin
     CheckFieldType( Value );
     FDataLink.FieldName := Trim( Value );
end;

procedure TZetaDBKeyLookup_DevEx.SetDataSource(Value: TDataSource);
begin
     if ( FDataLink.DataSource <> Value ) then
     begin
          FDataLink.DataSource := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

procedure TZetaDBKeyLookup_DevEx.SetReadOnly( const Value: Boolean );
begin
     inherited SetReadOnly( Value );
     FDataLink.ReadOnly := Value;
end;

procedure TZetaDBKeyLookup_DevEx.CheckFieldType( const Value: String );
begin
     if ( Value <> '' ) and
        ( FDataLink <> nil ) and
        ( FDataLink.Dataset <> nil ) and
        ( FDataLink.Dataset.Active ) then
     begin
          with FDataLink.Dataset.FieldByName( Value ) do
          begin
               if not ( DataType in [ ftInteger, ftSmallInt, ftWord, ftString ] ) then
                  raise EInvalidFieldType.Create( 'TZetaDBKeyLookup.DataField can only ' +
                                                  'be connected to columns of type ' +
                                                  'Integer, SmallInt, Word, String' );
               if ( Datatype = ftString ) then
                  SetKeySize( Size );
          end;
     end;
end;

procedure TZetaDBKeyLookup_DevEx.DataChange( Sender: TObject );
begin
     with FDataLink do
     begin
          if ( Field = nil ) then
             SetLlave( K_LLAVE_VACIA )
          else
          begin
               with Field do
               begin
                    if IsNull then
                       SetLlave( K_LLAVE_VACIA )
                    else
                        case DataType of
                             ftString: SetLlave( Trim( AsString ) ); { ADO pone espacios al final }
                             ftSmallint, ftInteger, ftWord: SetValor( AsInteger );
                        else
                            SetLlave( K_LLAVE_VACIA );
                        end;
               end;
          end;
     end;
end;

procedure TZetaDBKeyLookup_DevEx.EditingChange(Sender: TObject);
begin
end;
procedure TZetaDBKeyLookup_DevEx.PUpdateData;
begin
 UpdateData(Fllave);
end;
procedure TZetaDBKeyLookup_DevEx.UpdateData(Sender: TObject);
begin
     with FDataLink.Field do
     begin
          case DataType of
               ftString: AsString := Llave;
               ftSmallint, ftInteger, ftWord: AsInteger := Valor;
          end;
     end;
end;

procedure TZetaDBKeyLookup_DevEx.ActiveChange( Sender: TObject );
begin
     if ( FDataLink <> nil ) and FDataLink.Active then
        CheckFieldType( DataField );
end;

procedure TZetaDBKeyLookup_DevEx.DoChange( Sender: TObject );
begin
     inherited DoChange( Sender );
     if ( FDataLink <> nil ) and FDataLink.Editing then
        FDataLink.Modified;
end;

function TZetaDBKeyLookup_DevEx.ValidEdit: Boolean;
begin
     if ( FDataLink <> nil ) then
     begin
          if FDataLink.Editing then
             Result := True
          else
              Result := FDataLink.Edit;
     end
     else
         Result := True;
     if Result then
        Result := inherited ValidEdit;

end;

function TZetaDBKeyLookup_DevEx.VerifyData: Boolean;
begin
     if FDataLink.Editing then
     begin
          Result := False;
          if inherited VerifyData then
          begin
               try
                  with FDataLink do
                  begin
                       Modified;
                       UpdateRecord;
                  end;
                  Result := True;
               except
                     Enfocar;
                     raise;
               end;
          end;
     end
     else
         Result := True;
end;

end.
