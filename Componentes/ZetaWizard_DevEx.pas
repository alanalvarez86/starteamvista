unit ZetaWizard_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ComCtrls, Buttons, ExtCtrls,
     ZetaCommonTools,CxButtons,cxClasses,CXpc,
     ZetaMessages;

type
  TZetaWizard_DevEx = class;
  WizardState = ( wzDataEntry, wzExecuting, wzExecuted, wzCanceled );
  BotonWizard = ( bwAnterior, bwSiguiente, bwEjecutar, bwCancelar );
  TWizardMove = procedure( Sender: TObject; var iNewPage: Integer; var CanMove: Boolean ) of object;
  TZetaWizardButton_DevEx = class( TcxButton )
  private
    { Private declarations }
    FTipo: BotonWizard;
    FWizard: TZetaWizard_devEx;
    FParentForm: TCustomForm;
    procedure SetWizard( Value: TZetaWizard_DevEx );
    procedure SetTipo( const Value: BotonWizard);
    procedure SetState;
    procedure SetStateSalir;
    procedure WMWizardMove( var Message: TMessage ); message WM_WIZARD_MOVE;
    procedure WMWizardFocus( var Message: TMessage ); message WM_WIZARD_FOCUS;
    procedure WMPaint( var Message: TWMPaint ); message WM_PAINT;
    procedure Enfocar;
  protected
    { Protected declarations }
    procedure Notification( AComponent: TComponent; Operation: TOperation ); override;
    procedure Loaded; override;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    procedure Click; override;
  published
    { Published declarations }
    property Tipo: BotonWizard read FTipo write SetTipo;
    property Wizard: TZetaWizard_DevEx read FWizard write SetWizard;
  end;
  TZetaWizard_DevEx = class( TPanel )
  private
    { Private declarations }
    FAdelante: Boolean;
    FPageControl: TPageControl;
    FAlEjecutar: TConfirmEvent;
    FAlCancelar: TConfirmEvent;
    FAfterMove: TNotifyEvent;
    FBeforeMove: TWizardMove;
    FEstado: WizardState;
    FReejecutar: Boolean;
    function GetFirstPage: Integer;
    function GetLastPage: Integer;
    function GetPrimerPaso: Boolean;
    function GetUltimoPaso: Boolean;
    function GetCapturando: Boolean;
    function GetEjecutando: Boolean;
    function GetEjecutado: Boolean;
    function GetCancelado: Boolean;
    procedure SetEstado( Value: WizardState );
    procedure NotifyButtons;
  protected
    { Protected declarations }
    function HayPageControl: Boolean;
    procedure SetPageControl( Value: TPageControl );
    procedure Notification( AComponent: TComponent; Operation: TOperation ); override;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    property Adelante: Boolean read FAdelante;
    property Cancelado: Boolean read GetCancelado;
    property Capturando: Boolean read GetCapturando;
    property Ejecutando: Boolean read GetEjecutando;
    property Ejecutado: Boolean read GetEjecutado;
    property Estado: WizardState read FEstado;
    property Preparado: Boolean read HayPageControl;
    property PrimeraPagina: Integer read GetFirstPage;
    property PrimerPaso: Boolean read GetPrimerPaso;
    property UltimaPagina: Integer read GetLastPage;
    property UltimoPaso: Boolean read GetUltimoPaso;
    function EsPaginaActual( Value: TTabSheet ): Boolean; overload;
    function EsPaginaActual( Value: TcxTabSheet ): Boolean;overload;
    procedure Reset;
    procedure Primero;
    procedure Anterior;
    procedure Siguiente;
    procedure Ultimo;
    procedure Ejecutar;
    procedure Cancelar;
    procedure Abortar;
    procedure Saltar( iNewPageIndex: Integer );
  published
    { Published declarations }
    property AlEjecutar: TConfirmEvent read FAlEjecutar write FAlEjecutar;
    property AlCancelar: TConfirmEvent read FAlCancelar write FAlCancelar;
    property AfterMove: TNotifyEvent read FAfterMove write FAfterMove;
    property BeforeMove: TWizardMove read FBeforeMove write FBeforeMove;
    property PageControl: TPageControl read FPageControl write SetPageControl;
    property Reejecutar: Boolean read FReejecutar write FReejecutar;
  end;

implementation

{ *************** TZetaWizardBtn ***************** }

constructor TZetaWizardButton_DevEx.Create( AOwner: TComponent );
begin
     inherited Create( AOwner );
end;

procedure TZetaWizardButton_DevEx.Notification( AComponent: TComponent; Operation: TOperation );
begin
     inherited Notification( AComponent, Operation );
     if ( Operation = opRemove ) and ( AComponent = Wizard ) then
        Wizard := nil;
end;

procedure TZetaWizardButton_DevEx.Loaded;
begin
     FParentForm := GetParentForm( Self );
end;

procedure TZetaWizardButton_DevEx.SetWizard( Value: TZetaWizard_DevEx );
begin
     if ( FWizard <> Value ) then
     begin
          FWizard := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

procedure TZetaWizardButton_DevEx.SetTipo( const Value: BotonWizard );
begin
     if ( FTipo <> Value ) then
     begin
          FTipo := Value;
          case Tipo of
               bwAnterior: TabOrder := 3;
               bwSiguiente: TabOrder := 0;
               bwEjecutar: TabOrder := 1;
               bwCancelar: TabOrder := 2;
          end;
     end;
end;

procedure TZetaWizardButton_DevEx.WMPaint( var Message: TWMPaint );
begin
     SetState;
     inherited;
end;

procedure TZetaWizardButton_DevEx.WMWizardMove( var Message: TMessage );
begin
     Invalidate;
end;

procedure TZetaWizardButton_DevEx.WMWizardFocus( var Message: TMessage );
begin
     case Tipo of
          bwSiguiente: Enfocar;
          bwEjecutar: Enfocar;
     end;
end;

procedure TZetaWizardButton_DevEx.SetState;
var
   lEnabled: Boolean;
begin
     if Assigned( FWizard ) and FWizard.Preparado then
     begin
          case Tipo of
               bwAnterior:
               begin
                    with FWizard do
                    begin
                         lEnabled := Capturando and not PrimerPaso;
                         if not lEnabled and Reejecutar and Ejecutado then
                            lEnabled := True;
                    end;
                    Enabled := lEnabled;
               end;
               bwSiguiente: Enabled := FWizard.Capturando and not FWizard.UltimoPaso;
               bwEjecutar: Enabled := FWizard.Capturando and FWizard.UltimoPaso;
               bwCancelar: SetStateSalir;
          end;
     end;
end;

procedure TZetaWizardButton_DevEx.Enfocar;
begin
     if Enabled and CanFocus then
        FParentForm.ActiveControl := Self;
end;

procedure TZetaWizardButton_DevEx.SetStateSalir;
begin
     if FWizard.Ejecutado or FWizard.Cancelado then
     begin
     { if ( Kind <> bkClose ) then
          begin
               Kind := bkClose;
               Caption := '&Salir';
          end
     end
     else
         if ( Kind <> bkCancel ) then
         begin
              Kind := bkCancel;
              Caption := '&Cancelar';
         end;                          }
         end;
end;

procedure TZetaWizardButton_DevEx.Click;
begin
     if Assigned( FWizard ) and FWizard.Preparado then
     begin
          case Tipo of
               bwAnterior:
               begin
                    with FWizard do
                    begin
                         if Reejecutar and Ejecutado then
                            Reset;
                         Anterior;
                    end;
               end;
               bwSiguiente: FWizard.Siguiente;
               bwEjecutar: FWizard.Ejecutar;
               bwCancelar: FWizard.Cancelar;
          end;
     end
     else
         inherited Click;
end;

{ *************** TZetaWizard ***************** }

constructor TZetaWizard_DevEx.Create( AOwner: TComponent );
begin
     inherited Create( AOwner );
     ControlStyle := ControlStyle - [ csSetCaption ];
     Reset;
end;

procedure TZetaWizard_DevEx.Reset;
begin
     FAdelante := False;
     SetEstado( wzDataEntry );
end;

procedure TZetaWizard_DevEx.SetPageControl( Value: TPageControl );
begin
     if ( FPageControl <> Value ) then
     begin
          FPageControl := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

function TZetaWizard_DevEx.GetCapturando: Boolean;
begin
     Result := ( FEstado = wzDataEntry );
end;

function TZetaWizard_DevEx.GetEjecutando: Boolean;
begin
     Result := ( FEstado = wzExecuting );
end;

function TZetaWizard_DevEx.GetEjecutado: Boolean;
begin
     Result := ( FEstado = wzExecuted );
end;

function TZetaWizard_DevEx.GetCancelado: Boolean;
begin
     Result := ( FEstado = wzCanceled );
end;

procedure TZetaWizard_DevEx.Notification( AComponent: TComponent; Operation: TOperation );
begin
     inherited Notification( AComponent, Operation );
     if ( Operation = opRemove ) and ( AComponent = PageControl ) then
        PageControl := nil;
end;

function TZetaWizard_DevEx.HayPageControl: Boolean;
begin
     Result := Assigned( FPageControl ) and ( FPageControl.PageCount > 0 );
end;

function TZetaWizard_DevEx.EsPaginaActual( Value: TTabSheet ): Boolean;
begin
     Result := HayPageControl and ( FPageControl.ActivePage.PageIndex = Value.PageIndex );
end;
function TZetaWizard_DevEx.EsPaginaActual( Value: TcxTabSheet ): Boolean;
begin
     Result := HayPageControl and ( FPageControl.ActivePage.PageIndex = Value.PageIndex );
end;

function TZetaWizard_DevEx.GetFirstPage: Integer;
var
   i: Integer;
begin
     Result := -2;
     if HayPageControl then
     begin
          with FPageControl do
          begin
               for i := 0 to ( PageCount - 1 ) do
               begin
                    if Pages[ i ].Enabled then
                    begin
                         Result := i;
                         Break;
                    end;
               end;
          end;
     end;
end;

function TZetaWizard_DevEx.GetPrimerPaso: Boolean;
begin
     if HayPageControl then
        Result := ( GetFirstPage = FPageControl.ActivePage.PageIndex )
     else
         Result := False;
end;

function TZetaWizard_DevEx.GetLastPage: Integer;
var
   i: Integer;
begin
     Result := -2;
     if HayPageControl then
     begin
          with FPageControl do
          begin
               for i := ( PageCount - 1 ) downto 0 do
               begin
                    if Pages[ i ].Enabled then
                    begin
                         Result := i;
                         Break;
                    end;
               end;
          end;
     end;
end;

function TZetaWizard_DevEx.GetUltimoPaso: Boolean;
begin
     if HayPageControl then
        Result := ( GetLastPage = FPageControl.ActivePage.PageIndex )
     else
         Result := False;
end;

procedure TZetaWizard_DevEx.Primero;
begin
     if HayPageControl then
        Saltar( GetFirstPage );
end;

procedure TZetaWizard_DevEx.Anterior;
begin
     if HayPageControl then
        Saltar( FPageControl.ActivePage.PageIndex - 1 );
end;

procedure TZetaWizard_DevEx.Siguiente;
begin
     if HayPageControl then
        Saltar( FPageControl.ActivePage.PageIndex + 1 );
end;

procedure TZetaWizard_DevEx.Ultimo;
begin
     if HayPageControl then
        Saltar( GetLastPage );
end;

procedure TZetaWizard_DevEx.Ejecutar;
var
   lOk: Boolean;
   wzValue: WizardState;
begin
     wzValue := FEstado;
     SetEstado( wzExecuting );
     try
        lOk := True;
        if Assigned( FAlEjecutar ) then
           FAlEjecutar( Self, lOk );
     finally
            if lOk then
               SetEstado( wzExecuted )
            else
                SetEstado( wzValue );
     end;
end;

procedure TZetaWizard_DevEx.Abortar;
begin
     SetEstado( wzCanceled );
end;

procedure TZetaWizard_DevEx.Cancelar;
var
   lOk: Boolean;
begin
     try
        lOk := True;
        if Assigned( FAlCancelar ) then
           FAlCancelar( Self, lOk );
     finally
            if lOk then
               SetEstado( wzCanceled );
     end;
end;

procedure TZetaWizard_DevEx.SetEstado( Value: WizardState );
begin
     FEstado := Value;
     NotifyButtons;
end;

procedure TZetaWizard_DevEx.Saltar( iNewPageIndex: Integer );
var
   CanMove: Boolean;
begin
     CanMove := True;
     FAdelante := HayPageControl and ( FPageControl.ActivePage.PageIndex < iNewPageIndex );
     if Assigned( FBeforeMove ) then
        FBeforeMove( Self, iNewPageIndex, CanMove );
     if CanMove then
     begin
          if HayPageControl and
             ( iNewPageIndex >= 0 ) and
             ( iNewPageIndex < FPageControl.PageCount ) and
             ( iNewPageIndex <> FPageControl.ActivePage.PageIndex ) then
          begin
               with FPageControl do
               begin
                    ActivePage := Pages[ iNewPageIndex ];
                    while not ActivePage.Enabled do
                    begin
                         ActivePage := FindNextPage( ActivePage, FAdelante, False );
                    end;
                    iNewPageIndex := ActivePage.PageIndex;
               end;
               if ( iNewPageIndex = GetFirstPage ) then
                  Reset;
               NotifyButtons;
               if Assigned( FAfterMove ) then
                  FAfterMove( Self );
          end;
     end;
end;

procedure TZetaWizard_DevEx.NotifyButtons;
begin
     if HayPageControl then
     begin
          NotifyControls( WM_WIZARD_MOVE );
          Application.ProcessMessages;
          NotifyControls( WM_WIZARD_FOCUS );
          Application.ProcessMessages;
     end;
end;

end.
