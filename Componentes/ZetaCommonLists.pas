unit ZetaCommonLists;

interface                                                           
uses SysUtils,{$ifndef DOTNET} Windows, Messages, DB, Buttons,{$endif}Classes
{$ifdef TRESS_DELPHIXE5_UP}, Generics.Collections{$endif};

{$Define QUINCENALES}
{.$Undefine QUINCENALES}
{$INCLUDE DEFINES.INC}
type
    {$ifdef ADUANAS}
    TipoEstado = ( stContribuyente, stFecha, stNinguno );
    {$else}
    TipoEstado = ( stEmpleado, stPeriodo, stIMSS, stFecha, stTodos, stNinguno,stSistema, stExpediente, stEvaluacion{$ifdef CAJAAHORRO}, stTipoAhorro{$endif}{$ifdef GUARDIAS}, stTipoContrato{$endif} {$ifdef CONCILIA} ,stCedula, stArchivo{$endif}, stRazonSocial );
    {$endif}

    TiposEstados = set of TipoEstado;
    {$ifdef DOTNET}
    TUpdateKind = (ukModify, ukInsert, ukDelete);
    {$endif}
    TipoApp     = ( taTress, taCajaAhorro, taSuper, taOtherApp);
type
    ListasVariables = ( lvPuesto,
                        lvCurso,
                        lvTurno,
                        lvRPatron,
                        lvCompanysCod,
                        lvEvento,
                        lvHorario );

{$ifdef TRESS_DELPHIXE5_UP}
    ZetaPChar = string;
{$else}
    ZetaPChar = string[50];
{$endif}

    {CV: OJO!!!!!!!!!!!!!!}
    {CV: OJO!!!!!!!!!!!!!!}

    {CV: Si vas a agregar un dato a LISTASFIJAS TIENE que ser al final de la lista.
    Por ninguna razon, debe de agregarse ning�n dato, enmedio de la lista.}
    {CV: Al momento de agregar un elemento nuevo al numerado de ListasFijas
    hay que hacerlo al final de la misma, sin excepci�n alguna.
    Yo se que los elementos no estan ordenados de ninguna manera,
    y la tentaci�n de hacerlo es mucha, pero el mover alguno
    de estos elementos, ocasiona errores dentro del reporteador.
    Pues el reporteador, asume que un numerado esta en una
    posicion, y obtiene informaci�n de esta posici�n, si se mueve de lugar
    alguno de los elementos, la informaci�n ya no
    coincide, estar�a accesando una lista que no es la correcta.
    Tampoco se pueden borrar elementos, pues el borrar alg�n elemento,
    ocasiona que se mueva la lista hacia 'arriba' y
    tendriamos el problema que menciono lineas arriba.}


    ListasFijas = ( lfNinguna,                     {0  }
                    lfNivelUsuario,                {1  }
                    lfTipoBitacora,                {2  }
                    lfSexoDesc,                    {3  }
                    lfCandado,                     {4  }  
                    lfMotivoBaja,                  {5  }  
                    lfTipoOtrasPer,                {6  }
                    lfIMSSOtrasPer,                {7  }  
                    lfISPTOtrasPer,                {8  }  
                    lfIncidencias,                 {9  }
                    lfIncidenciaIncapacidad,       {10 }
                    lfRPatronModulo,               {11 }  
                    lfTipoTurno,                   {12 }
                    lfTipoJornada,                 {13 }  
                    lfTipoPeriodo,                 {14 }
                    lfUsoPeriodo,                  {15 }
                    lfStatusPeriodo,               {16 }  
                    lfTipoNomina,                  {17 }  
                    lfTipoFestivo,                 {18 }
                    lfTipoHorario,                 {19 }
                    lfTipoReporte,                 {20 }  
                    lfTipoRangoActivo,             {21 }
                    lfOperacionConflicto,          {22 }
                    lfStatusLectura,               {23 }  
                    lfClasifiReporte,              {24 }
                    lfTipoCampo,                   {25 }  
                    lfTipoOpera,                   {26 }
                    lfTipoOperacionCampo,          {27 }
                    lfWorkStatus,                  {28 }  
                    lfTipoLectura ,                {29 }  
                    lfTipoCedula,                  {30 }
                    lfRangoFechas,                 {31 }  
                    lfJustificacion,               {32 }  
                    lfIncluyeEvento,               {33 }
                    lfTipoGlobal,                  {34 }
                    lfTipoConcepto,                {35 }  
                    lfMotivoFaltaDias,             {36 }
                    lfMotivoFaltaHoras,            {37 }
                    lfStatusAhorro,                {38 }  
                    lfStatusPrestamo,              {39 }
                    lfStatusAusencia,              {40 }  
                    lfTipoDiaAusencia,             {41 }
                    lfTipoChecadas,                {42 }
                    lfDescripcionChecadas,         {43 }
                    lfTipoPariente,                {44 }  
                    lfTipoInfonavit,               {45 }
                    lfTipoVacacion,                {46 }
                    lfStatusKardex,                {47 }  
                    lfMotivoIncapacidad,           {48 }
                    lfFinIncapacidad,              {49 }  
                    lfTipoPermiso,                 {50 }  
                    lfTipoLiqIMSS,                 {51 }
                    lfCampoCalendario,             {52 }
                    lfTipoLiqMov,                  {53 }
                    lfCafeCalendario,              {54 }  
                    lfTipoAhorro,                  {55 }
                    lfHorasExtras,                 {56 }
                    lfMotTools,                    {57 }
                    lfBancaElectronica,            {58 }  
                    lfYears,                       {59 }
                    lfDescuentoTools,              {60 }
                    lfMeses,                       {61 }
                    lfZonaGeografica,              {62 }  
                    lfSexo,                        {63 }
                    lfCodigoLiqMov,                {64 }
                    lfDiasSemana,                  {65 }
                    lfMes13,                       {66 }  
                    lfTipoRango,                   {67 }
                    lfLiqNomina,                   {68 }  
                    lfAutorizaChecadas,            {69 }
                    lfAltaAhorroPrestamo,          {70 }  
                    lfStatusEmpleado,              {71 }
                    lfTipoFormato,                 {72 }  
                    lfExtFormato,                  {73 }
                    lfFormatoFormas,               {74 }  
                    lfFormatoASCII,                {75 }
                    lfEmailFrecuencia,             {76 }  
                    lfEmailFormato,                {77 }
                    lfEmailNotificacion,           {78 }  
                    lfTipoCompanyName,             {79 }
                    lfTipoCalculo,                 {80 }  
                    lfCamposConteo,                {81 }
                    lfHorasDias,                   {82 }  
                    lfImportacion,                 {83 }
                    lfOperacionMontos ,            {84 }  
                    lfTipoBanda,                   {85 }
                    lfPuertoSerial,                {86 }
                    lfTipoGlobalAuto,              {87 }
                    lfClaseBitacora,               {88 }
                    lfProcesos,                    {89 }
                    lfRegla3x3,                    {90 }
                    lfTipoCompany,                 {91 }
                    lfPrioridad,                   {92 }
                    lfReqStatus,                   {93 }
                    lfMotivoVacante,               {94 }
                    lfEstudios,                    {95 }
                    lfSolStatus,                   {96 }
                    lfEdoCivil,                    {97 }
                    lfEdoCivilDesc,                {98 }
                    lfCanStatus,                   {99 }
                    lfEntStatus,                   {100}
                    lfTipoExamen,                  {101}
                    lfTipoConsulta,                {102}
                    lfTipoEstudio,                 {103}
                    lfTipoAccidente,               {104}
                    lfMotivoAcc,                   {105}
                    lfTipoMedicina,                {106}
                    lfTipoDiagnost,                {107}
                    lfTipoLesion,                  {108}
                    lfAxAtendio,                   {109}
                    lfAxTipo,                      {110}
                    lfExCampoTipo,                 {111}
                    lfExMostrar,                   {112}
                    lfExTipo,                      {113}
                    lfEmTermino,                   {114}
                    lfTurnoLabor,                  {115}
                    lfTipoArea,                    {116}
                    lfTipoKarFecha,                {117}
                    lfCancelaMod,                  {118}
                    lfInicioAmortizacion,          {119}
                    lfClaseAcciones,               {120}
                    lfTipoCompete,                 {121}
                    lfStatusAcciones,              {122}
                    lfStatusCursos,                {123}
                    lfTipoCedInspeccion,           {124}
                    lfResultadoCedInspeccion,      {125}
                    lfValorDiasVacaciones,         {126}
                    lfFiltroEmpOrganigrama,        {127}
                    lfTipoPuesto,                  {128}
                    lfEmailType,                   {129}
                    lfUpdateKind,                  {130}
                    lfOperacionIncremento,         {131}
                    lfFormatoImpFecha,             {132}
                    lfOut2Eat,                     {133}
                    lfEmpleadoRepetido,            {134}
                    lfTipoCorregirFechas,          {135}
                    lfTipoNavegador,               {136}
                    lfOrientacionNavegador,        {137}
                    lfPosicionNavegador,           {138}
                    lfCompressionLevel,            {139}
                    lfEncryptionLevel,             {140}
                    lfFontEncoding,                {141}
                    lfPageLayout,                  {142}
                    lfPageMode,                    {143}
                    lfTransitionEffect,            {144}
                    lfAnfitrionStatus,             {145}
                    lfVisitanteStatus,             {146}
                    lfCasetaStatus,                {147}
                    lfCitaStatus,                  {148}
                    lfCatCompanys,                 {149}
                    lfLibroStatus,                 {150}
                    lfTipoGafete,                  {151}
                    lfCorteStatus,                 {152}
                    lfAccesoRegla,                 {153}
                    lfTipoKind,                    {154}
                    lfBusquedaLibro,               {155}
                    lfImpresionGafete,             {156}
                    lfTipoProximidad,              {157}
                    {$ifdef COMPARTE_MGR}
                    lfWFStatusModelo,
                    lfWFNotificacion,
                    lfWFProcesoStatus,
                    lfWFPasoProcesoStatus,
                    lfWFTareaStatus,
                    lfWFCorreoStatus,                                     // GA 24/Ago/05: Se hizo as� porque se agreg� un ennumerado lfEVCorreoStatus fuera de los IFDEf'S de COMPARTE
                    {$endif}
                    {$ifdef ADUANAS}
                    lfTipoCliente,
                    lfTipoMaterial,
                    lfTipoMedida,
                    lfStatusFactura,
                    lfFacTipoProceso,
                    lfTipoActivo,
                    lfStatusPedimento,
                    lfProgramasTipoProceso,          
                    lfTipoFraccionAutorizada,
                    lfDescargaAlmacen,               
                    lfDescargaTipo,
                    {$endif}                         
                    lfStatusEncuesta,                  {158}
                    lfComentariosMGR,                  {159}
                    lfEstiloRespuesta,                 {160}
                    lfTipoEvaluaEnc,                   {161}
                    lfStatusEvaluaciones,              {162}
                    lfStatusSujeto,                    {163}
                    lfStatusInscrito,                  {164}
                    lfStatusSesiones,                  {165}
                    lfTipoReserva,                     {166}
                    lfStatusEvaluacion,                {167}
                    lfEVCorreoStatus,                  {168}
                    lfTipoSupuestoRH,                  {169}
                    lfTipoPeriodoMes,                  {170}
                    {Lista para Kiosco}
                    lfKJustificacion,                  {171}                  
                    lfAccionBoton,                     {172}
                    lfLugarBoton,                      {173}
                    lfPosicionIcono,                   {174}
                    {Listas para Proyecto Especial Schneider}
                    lfDPEscolar,                       {175}
                    lfDPNivel,                         {176}
                    lfDPExpPuesto,                     {177}
                    lfJITipoPlaza,                     {178}
                    lfJIPlazaReporta,                  {179}
                    lfTipoDiasBase,                    {180}
                    lfComentariosPedir,                {181}
                    lfComoCalificar,                   {182}
                    lfStatusAcuerdo,                   {183}
                    lfBorrarMovimSUA,                  {184}
                    lfStatusPlanVacacion,              {185}
                    lfStatusPoliza,                    {186}
                    lfTiposdePrestamo,                 {187}                   // Listas del proyecto especial Caja de Ahorro
                    lfControles,                       {188}
                    lfStatusFestivos,                  {189}
                    lfPagoPlanVacacion,                {190}
                    lfTratamientoExtras,               {191}
                    lfDiasAplicaExento,                {192}
                    { Listas Fijas para Guardias de COTEMAR }
                    lfRolGuardias,                     {193}
                    lfStatusPlaza,                     {194}
                    lfStatusGuardia,                   {195}
                    lfClaseKarPlaza,                   {196}
                    lfTipoKarPlaza,                    {197}
                    lfEstadosGuardia,                  {198}
                    lfRitmosGuardia,                   {199}
                    {$ifdef COMPARTE_MGR}
                    lfWFNotificacionCuando,          
                    lfWFNotificacionQue,             
                    {$endif}
                    lfTipoABordoGuardia,               {200}                    {AV: 9/Ago/06}
                    lfTipoDiaGuardia,                  {201}                    {AV: 9/Ago/06}
                    lfTipoPagoGuardia,                 {202}                    {AV: 9/Ago/06}
                    lfTipoEventoGuardia,               {203}                    {AV: 1/Sept/06}
                    lfClaseEventoGuardia,              {204}                    {AV: 12/Sept/06}
                    lfStatusAusenciaGuardia,           {205}                    {AV: 27/Sept/06}
                    lfTipoServicioLogistica,           {206}                    {AV: 20/Oct/06}
                    lfViaLogistica,                    {207}                    {AV: 20/Oct/06}
                    lfSentidoLogistica,                {208}                    {AV: 20/Oct/06}
                    lfTipoPuertoLogistica,             {209}                    {AV: 20/Oct/06}
                    lfStatusViaje,                     {210}                    {EA: 23/Oct/2006}
                    lfStatusTarjeta,                   {211}
                    lfTipoTarjeta,                     {212}
                    lfPuEmbarque,                      {213}                    {EA: 01/Nov/2006}
                    lfPuDomicilio,                     {214}                    {EA: 01/Nov/2006}
                    lfPuHabitacion,                    {215}                    {EA: 01/Nov/2006}
                    lfStatusTarea,                     {216}                    {MV: 05/Nov/2006}
                    lfStatusHosp,                      {217}                    {EA: 06/Nov/2006}
                    lfSentidoHosp,                     {218}                    {EA: 06/Nov/2006}
                    lfStatusTrasporte,                 {219}                    {EA: 08/Nov/2006}
                    lfTipoCalEvent,                    {220}                    {EA: 13/Nov/2006}
                    lfTipoRuta,                        {221}                    {EA: 27/Nov/2006}
                    lfStatusDePlaza,                   {222}                    {EA: 27/Nov/2006}
                    lfPrioridadVac,                    {223}                    {EA: 12/Dic/2006}
                    lfStatusVac,                       {224}                    {EA: 12/Dic/2006}
                    lfPasoVac,                         {225}                    {EA: 12/Dic/2006}
                    lfTrabaja,                         {226}                    {EA: 12/Dic/2006}
                    lfPlantillaTipo,                   {227}                    {EA: 13/Dic/2006}
                    lfPrioridadSolicitud,              {228}                    {AV: 13/Dic/2006}
                    lfCicloSolicitud,                  {229}                    {AV: 13/Dic/2006}
                    lfReservacion,                     {230}                    {EA: 14/Dic/2006}
                    lfStatusAutoriza,                  {231}                    {EA: 18/Dic/2006}
                    {Comienza listas de ICUMedical  12/Ene/07}
                    lfSentidoCamion,                   {232}
                    lfStatusCamion,                    {233}
                    lfStatusPasajero,                  {234}
                    lfLogEventos,                      {235}
                    { Lista fija Solvay }
                    lfTipoPermuta,                     {236}    {MV: 21/Feb/2007}
                    lfPiramidacionPrestamos,           {237}    {AP: 28/Feb/2007}
                    lfClaseSistBitacora                {238}    {AP: 29/Mar/2007}
                    {$ifdef COMPARTE_MGR}
                    ,lfWFAutenticacionWebService
                    ,lfWFCodificacionWebService
                    {$endif}
                    ,lfAltaMedica,                     {239}     {EA: 14/03/07 }
                    lfRDDDefaultsTexto,                {240}
                    lfRDDDefaultsFloat,                {241}
                    lfRDDDefaultsLogico,               {242}
                    lfRDDValorActivo,                  {243}
                    lfStatusPlantilla,                 {244}
                    lfStatusValuacion,                 {245}
                    lfStatusLiqIMSS,                   {246}
                    lfReclasificaBool,                 {247}
                    lfReclasificaRelacion,             {248}
                    lfReclasificaTipoVigencia,         {249}
                    lfReclasificaZonaGeo,              {250}

                    { COTEMAR v.2007:Cambios para migrar de v.2006 a v.2007 }
                    lfStatusTEvento,                   {251}     {EA : 17/04/07 }
                    lfTurnoAbordo,                     {252}     {AV : 25/04/07 }
                    lfBitacoraStatus,                  {253}     {EA : 11/05/07 }
                    lfGrupoRecibo,                     {254}     {EA : 21/05/07 }
                    lfSubStatus,                       {255}     {EA : 30/05/07 }
                    lfTipoPuestoContrato,              {256}     {EA : 04/06/07 }
                    lfTipoPlaza,                       {257}     {EA : 04/06/07 }
                    lfTipoCobertura,                   {258}     {EA : 16/07/07 }
                    lfClaseBitacoraCOTEMAR,            {259}     {EA : 07/11/07 }
                    lfClaseBitacoraCOTEMARLogistica,   {260}     {AV : 17/12/07 }

                    //LEPE
                    lfModulosSolTran                   {261}     {ML : 19/12/07 }


                    ,lfViaticoNivelRestriccion,        {262}     {ML : 21/01/08 }
                    lfViaticoTopeConceptoGasto,        {263}     {ML : 22/01/08 }

                    lfAccionBitacora,                  {264}     {AP: 12/02/08}

                    lfTipoLogin,                       {265}     {EZ}
                    lfFiltroStatusEmpleado,            {266}     {EZ}
                    lfAccionRegistros,                 {267}     {EZ}
                    lfTipoInfoExp,                     {268}     {CV}
                    lfConflictoImp,                    {269}     {CV}
                    lfTipoRangoActivo2,                {270}     {CV}
                    lfDiasCafeteria,                   {271}     {OP}
                    lfRequisitoTipo,                   {272}     {OP:16.Abr.08}
                    lfTipoMovInfonavit,                {273}     {AP:16/06/2008}
                    lfBimestres,                       {274}     {AP:16/06/2008}
                    lfTipoDescuentoInfo,               {275}     {AP:16/06/2008}
                    lfStatusReglaPrestamo,             {276}     {CV:12/AGO/2008-ACS}
                    lfTipoJornadaTrabajo               {277}     {CV:12/AGO/2008-ACS}
                    ,lfFasesContingencia               {278}     {AV : 16/07/08 }
                    ,lfStatusPersonaExterna            {279}     {AV : 16/07/08 }
                    ,lfStatusHospedaje                 {280}      {AV : 16/07/08 }
                    ,lfClaseNominaCOTEMAR              {281}      {AV : 16/07/08 }
                    ,lfLstDispositivos                 {282}
                    ,lfStatusSimFiniquitos             {283}
                    ,lfTipoSaldoVacaciones             {284}    {CV:No quitar, se usa en Diccionario de Datos.}
                    ,lfClaseRiesgo                     {285}    {Indica la clase de prima de riesgo}
                    ,lfObjetivoCursoSTPS               {286}      {EZ v2010}
                    ,lfModalidadCursoSTPS              {287}      {EZ v2010}
                    ,lfDiscapacidadSTPS                {288}      {EZ v2010}
                    ,lfTipoInstitucionSTPS             {289}      {EZ v2010}
                    ,lfTipoDocProbatorioSTPS           {290}      {EZ v2010}
                    ,lfStatusDeclaracionAnual          {291}
                    ,lfTipoAgenteSTPS                  {292}      {EZ V2010}
                    ,lfTipoHoraTransferenciaCosteo     {293}      {CV V2011}
                    ,lfTipoDescConciliaInfonavit       {294}      {AV V2011B4}
                    ,lfTiposDerechosAcceso             {295}      {CV V2011B4}
                    ,lfTipoPantallaDerechoAcceso       {296}      {CV V2011B4}
                    ,lfFuenteDerechoAcceso             {297}      {CV V2011B4}
                    ,lfImpactoDerechoAcceso            {298}      {CV V2011B4}
                    ,lfTipoSGM                         {299}      {EZ V2012}
                    ,lfTipoEmpSGM                      {300}      {EZ V2012}
                    ,lfStatusSGM                       {301}      {EZ V2012}
                    ,lfStatusTransCosteo               {302}      {CV Costeo 2011}
                    ,lfStatusTransCosteoConsulta       {303}      {CV Costeo 2011}
                    ,lfStatusEncuestaKiosco            {304}      {EZ v2013}
                    ,lfTipoAsegurado                   {305}      {EZ SGM 2DA FASE v2013}
                    ,lfStatusSincronizacionDiurna      {306}      {AV Cotemar cambio v2013}
                    ,lfTipoBrigada                     {307}      {AL V2013}
                    ,lfUsoConceptoNomina               {308}      {AL V2013}
                    ,lfTipoPeriodoConfidencial         {309}      {JA V2013}
                    ,lfAuthTressEmail                  {AV Cummins Julio 7 2012}
	                ,lfCotemarPorcentajeCantidad       {310}      {AV Cotemar cambio v2013}
	                ,lfCotemarPeriodoTipoBono          {311}      {JB Cotemar cambio v2013}
                    ,lfStatusTimbrado                      {312}
                    ,lfStatusTimbradoPeticion              {313}
                    ,lfClaseConceptosSAT                   {314}
                    ,lfTipoExentoSAT                       {315}
                    ,lfTipoRegimenesSAT                    {316}
                    ,lfMetodoPagoSAT                       {317}
                    ,lfColumnasReporte                     {318}
                    ,lfStatusEmpleadoCedula                {319}
                    ,lfChecadasSimultaneas                 {320}
                    ,lfStatusSistDispositivos              {321}
                    ,lfClasifiPeriodoConfidencial 		   { 320 }
                    ,lfTPDias
                    ,lfTPDias7
                    ,lfTPHoras
                    ,lfTPHorasJo
                    ,lfTPDiasBT
                    ,lfClasifiPeriodo                    {326}
                    ,lfTipoPeriodoKardex                 {327}
                    ,lfSolicitudEnvios                     {328}
                    ,lfPrevisionSocialClasifi              {329}
                    ,lfStatusBajaFonacot                   {330}
                    ,lfMesesFonacot                        {331}
                    ,lfIdTerminales                        {332}
                    ,lfTipoArchivoCedula                   {333}
   					);

                    {CV: OJO: -->> Si vas a agregar un dato a LISTASFIJAS TIENE

                                   que ser al final de la lista.
                                   En la declaraci�n de ListasFijas (Lineas arriba),
                                   Se encuentra la justificaci�n.}

type
    //USUARIO
    eNivelUsuario = ( nuSinPrioridad,
                      nuPrioridadMaxima,
                      nuPrioridadMuyAlta,
                      nuPrioridadAlta,
                      nuPrioridadNormal,
                      nuPrioridadBaja,
                      nuPrioridadMuyBaja,
                      nuPrioridadMinima );
    eLogReply = ( lrNotFound,
                  lrAccessDenied,
                  lrLoggedIn,
                  lrLockedOut,
                  lrOK,
                  lrChangePassword,
                  lrFailure,
                  lrExpiredPassword,
                  lrInactiveBlock,
                  lrSystemBlock,
                  lrUserInactive,
                  lrDBServerInvalid );
    //BITACORA
    eTipoBitacora = ( tbNormal, tbAdvertencia, tbError, tbErrorGrave );
    {$ifdef COMPARTE_MGR}
    eClaseBitacora = ( clbNinguno,
                       clbGlobales,
                       clbModeloSuspender,
                       clbModeloReactivar,
                       clbModeloCancelar,
                       clbModeloReiniciar,
                       clbProcesoSuspender,
                       clbProcesoReiniciar,
                       clbProcesoCancelar,
                       clbTareaReasignar,
                       clbEmailService,
                       clbWorkFlowServicio,
                       clbEvalFormulas );
    {$else}
    {$ifdef KIOSCOS}
    eClaseBitacora = ( clbNinguno,
                       clbContenido,
                       clbMisDatos,
                       clbReporte,
                       clbCambioNIP,
                       clbLogin,
                       clbLogout,
                       clbErrorBrowser );
    {$else}
    {$ifdef ADUANAS}
    eClaseBitacora = ( clbNinguno,
                       clbGlobales,
                       clbCondicion,

                       clbContribuyentes,
                       clbClientesProveedores,
                       clbTransportistas,
                       clbAgentesAduanales,

                       clbMateriales,
                       clbMaterialesKardex,
                       clbMaterialGrupos,
                       clbMarcas,
                       clbEmpaques,
                       clbMedidas,
                       clbBOM,
                       clbBOMCOMP,
                       clbSimilares,

                       clbContenedores,
                       clbChoferes,
                       clbTractocamiones,
                       clbTiposContenedor,
                       clbMediosTransporte,

                       clbAduanas,
                       clbFracciones,
                       clbKardexFracciones,
                       clbContribuciones,
                       clbFormasPago,
                       clbIdentificadores,
                       clbTerminos,
                       clbINPC,
                       clbRecargos,
                       clbMonedas,
                       clbPaises,
                       clbEntidades,
                       clbPruebas,
                       clbRegulaciones,
                       clbSectores,
                       clbTiposTasa,
                       clbTiposValor,
                       clbTratados,
                       clbVinculaciones,
                       clbTiposPermiso,
                       clbGrupoVigencia,

                       clbCuotaMaterial,
                       clbMaterialProsec,
                       clbMaterialRegulaciones,

                       clbOperaciones,
                       clbTiposPedimento,
                       clbClavesPedimento,
                       clbMaterialCategorias,
                       clbOperacionesClaves,
                       clbClavesSalida,

                       clbProgramas,
                       clbPermisos,
                       clbReglaOctava,
                       clbSensibles,
                       clbFraccionesAutorizadas,
                       clbFraccionesProsec,
                       clbCertificadosOrigen,
                       clbDescargas,
                       clbDeshacerDescargas,
                       clbCalcularImpuestos,
                       clbFacturas,
                       clbPartidas,
                       clbCuotasPartida,
                       clbRegulacionesPartida,
                       clbPROSECPartida,
                       clbPedimentos,
                       clbCuotasPedimento,
                       clbEvalFormulas,
                       clbTiposServicios,
                       clbRateDuty,
                       clbSpecialPrograms,
                       clbFraccionesAmericanas,
                       clbPartTypes,
                       clbEntryType );
    {$else}

    {$ifdef VISITANTES}
    eClaseBitacora = ( clbNinguno,
                       clbDepto,
                       clbTipoID,
                       clbTipoCarro,
                       clbTipoAsunto,
                       clbTipoVisit,
                       clbCondiciones,
                       clbLibro,
                       clbCita,
                       clbCorte,
                       clbAnfitrion,
                       clbVisita,
                       clbCaseta,
                       clbCompVisita,
                       clbGlobales,
                       clbNumTarjeta,
                       clbEvalFormulas
                        );
    {$else}
    eClaseBitacora = ( clbNinguno,
                       clbKardexGeneral,
                       clbKardexPuesto,
                       clbKardexTurno,
                       clbKardexArea,
                       clbPermiso,
                       clbAjusteAsistencia,
                       clbCambioHorario,
                       clbAutoHorasExtras,
                       clbAutoDescanso,
                       clbAutoPermisoCG,
                       clbAutoPermisoCGEnt,
                       clbAutoPermisoSG,
                       clbAutoPermisoSGEnt,
                       clbAsignaciones,
                       clbCambioTipoDia,
                       clbTurno,
                       clbHorario,
                       clbCondicion,
                       clbConcepto,
                       clbKardexAlta,
                       clbKardexBaja,
                       clbKardexReingreso,
                       clbKardexCambio,
                       clbKardexRenova,
                       clbKardexPresta,
                       clbIncapacidad,
                       clbVacacion,
                       clbLabor,
                       clbTarjeta,
                       clbInfonavit,
                       clbPeriodoNomina,
                       clbNomina,
                       clbGlobales,
                       clbPuesto,
                       clbCursos,
                       clbCalendario,
                       clbClasifi,
                       clbContratos,
                       clbEventos,
                       clbFestTurno,
                       clbFolios,
                       clbInvitadores,
                       clbMaestros,
                       clbMatrizCurso,
                       clbMatrizPuesto,
                       clbOtrasPer,
                       clbPeriodos,
                       clbPrestaciones,
                       clbReglas,
                       clbRPatron,
                       clbNomParam,
                       clbTools,
                       clbExcepcionMonto,
                       clbExcepcionDias,
                       clbExcepcionGlobal,
                       clbMontoNomina,
                       clbKardexCursos,
                       clbSesiones,
                       clbNumTarjeta,
                       clbAccReglas,
                       clbHisAhorros,
                       clbHisPrestamos,          
                       clbEmpleadoCambios,
                       clbAntesCur,
                       clbAntesPto,
                       clbEvalFormulas,
                       clbAulas,
                       clbPrerequisitosCurso,
                       clbCertificaciones,
                       clbTiposPoliza,
                       clbSeccionesPerfil,
                       clbCamposPerfil,
                       clbPerfiles,
                       clbKardexPlaza,
                       clbKardexTipoNomina,
                       clbMovCajaAhorro,
                       clbSuscripcionReportes,
                       clbSupervisores,
                       clbAreasLabor,
                       clbCambioFestivo,
                       clbCambioTomoComida,
                       clbReservaAula,
                       clbInscripciones,
                       clbProvCap,
                       clbValPlantillas,
                       clbFactores,
                       clbSubfactores,
                       clbValNiveles,
                       clbRSocial,
                       clbAutoPrepFueraJor,
                       clbAutoPrepDentroJor,
                       clbRepCfgClasificaciones,
                       clbRepCfgListaValores,
                       clbRepCfgModulos,
                       clbRepCfgCampos,
                       clbRepCfgCamposDefault,
                       clbRepCfgOrden,
                       clbRepCfgFiltros,
                       clbRepCfgRelaciones,
                       clbRepCfgEntidadClasificacioes,
                       clbRepCfgEntidadModulos,
                       clbRepCfgEntidades,
                       clbRepCfgValores,
                       clbMatrizCertificaPuesto,
                       clbMatrizPuestoCertifica,
                       clbCredencialInvalida,
                       clbReglaPrestamo,
                       clbPrestaXRegla,
                       clbKardexCertificaciones,
                       clbSisBorrarBajas,                       //                         (JB) Se agregan Clases de Bitacora para Tablas de Sistema
                       clbPersEstados,
                       clbPersMunicipios,
                       clbPersArea1,
                       clbPersArea2,
                       clbPersArea3,
                       clbPersArea4,
                       clbPersArea5,
                       clbPersArea6,
                       clbPersArea7,
                       clbPersArea8,
                       clbPersArea9,
                       clbHistTipoKardex,
                       clbHistMotivoBaja,
                       clbHistIncidencias,
                       clbHistMotivosAut,
                       clbHistMotivosManCheck,
                       clbNomTipoAhorro,
                       clbNomTipoPrestamo,
                       clbOficialesSalMin,
                       clbOficialesCuotasIMSS,
                       clbNumericasGeneral,
                       clbNumericasTabla,
                       clbOficialesGradosRiesgo,
                       clbOficialesCatNacOcup,
                       clbOficialesAreasTemat,
                       clbPersAdicionales1,
                       clbPersAdicionales2,
                       clbPersAdicionales3,
                       clbPersAdicionales4,
                       clbPersAdicionales5,
                       clbPersAdicionales6,
                       clbPersAdicionales7,
                       clbPersAdicionales8,
                       clbPersAdicionales9,
                       clbPersAdicionales10,
                       clbPersAdicionales11,
                       clbPersAdicionales12,
                       clbPersAdicionales13,
                       clbPersAdicionales14,
                       clbAsisAutorizaPrenom,
                       clbTransferencias,
                       clbAsisImportaClasifTemp,
                       clbCatNomTPension,
                       clbCatGralSGM,
                       clbCosteoGrupos,
                       clbCosteoCriterios,
                       clbCosteoCriteriosPorConcepto,
                       clbTCompetencias,
                       clbTPerfiles,
                       clbCatCompetencias,
                       clbCatPerfiles,
                       clbPerfilesCompetencias,
                       clbPerfilesPuestos,
                       clbCompentenciaEmp,
                       clbEvaluacionEmp,
                       clbTCatNacComp,
                       clbApruebHorasExtras, // (AB) Clase Bit�cora para Aprobaci�n de autorizaciones
                       clbApruebDescanso,
                       clbApruebPermisoCG,
                       clbApruebPermisoCGEnt,
                       clbApruebPermisoSG,
                       clbApruebPermisoSGEnt,
                       clbApruebPrepFueraJor,
                       clbApruebPrepDentroJor,
                       clbMotorPatchCambioVersion,
                       clbMotorPatchImportarDiccionario,
                       clbMotorPatchImportarReporte,
                       clbMotorPatchScriptEspecial,
                       clbImportarImagenes,
		       clbBancos,
		       clbTipoSAT,
		       clbUma,
		       clbTipoContratoSAT,
                       clbTipoJornadaSAT,
                       clbRiesgoPuestoSAT,
                       clbPensiones,
                       clbCafeHistorial,
                       clbRegimenContratoTrabaja,
                       clbCalendarioReportes, //Pendiente gbeltran
                       clbServicioCorreos
                       );

    {$endif}
    {$endif}
    {$endif}
    {$endif}

    //MOTIVO BAJA
    eMotivoBaja = (mdDesconocido,mbTerminoContrato,mbVoluntaria,mbAbandono,mbDefuncion,mbClausura,mbOtros,mbAusentismo,mbRescision,mbJubiliacion,mbPension);
    //OTRAS PERCEPCIONS
    eTipoOtrasPer = (toCantidad,toPorcentaje);
    eIMSSOtrasPer = (ioExento,ioGravado,ioAsistencia,ioDespensa);
    eISPTOtrasPer = (soExento,soGravado);
    //INCIDENCIAS
    eIncidencias = (eiKardex, eiFaltas, eiIncapacidad, eiVacaciones, eiPermiso, eiRetardo, eiOtros );
    eIncidenciaIncapacidad = (inNoIncapacidad,inGeneral,inMaternidad,inAccidenteTrabajo,inAccidenteTrayecto,inEnfermedadTrabajo);
    //REGISTRO PATRONAL
    eRPatronModulo = (pmDesconocido,pmPermanente,pmEventual,pmConstruccion);
    //TURNO
    eTipoTurno = (ttNormal,ttConJornada,ttSinJornada);
    eTipoJornada = (tjDesconocido,tjSemanaCompleta,tjReducida_1,tjReducida_2,tjReducida_3,tjReducida_4,tjReducida_5,tjMenor);
    //PERIODO
    eTipoPeriodo = SmallInt;
    {$IFNDEF TRESS_DELPHIXE5_UP}
             eTipoPeriodoConfidencial = (tpcDiario,tpcSemanal,tpcCatorcenal,tpcQuincenal,tpcMensual,tpcDecenal,tpcSemanalA,tpcSemanalB,tpcQuincenalA,tpcQuincenalB,tpcCatorcenalA,tpcCatorcenalB);
    {$ENDIF}
    eUsoPeriodo = (upOrdinaria,upEspecial);
    eStatusPeriodo = (spNueva,spPreNomina,spSinCalcular,spCalculadaParcial,spCalculadaTotal,spAfectadaParcial,spAfectadaTotal);
    //Movimien
    eHorasDias = ( hdTodos, hdExcepcion, hdNinguno );
    eImportacion = ( eiExcepciones, eiAcumulados );
    eOperacionMontos = ( omSustituir, omSumar, omRestar );
    //Nomina
    eTipoNomina = ( tnDia,tnSemana,tnCatorcena,tnQuincena,tnMes,tnDecena,tnSemanaA,tnSemanaB,tnQuincenaA,tnQuincenaB,tnCatorcenaA,tnCatorcenaB );
    eRDDDefaultsTexto = (dtDiferentes,dtIguales);
    eRDDDefaultsFloat = (dfDiferentes,dfIguales,dfMayor,dfMenor,dfMayorIgual,dfMenorIgual);
    eRDDDefaultsLogico = (dlSi, dlNo, dlTodos );
    eRDDValorActivo = ( vaSinValor,vaEmpleado,vaNumeroNomina,vaTipoNomina,vaYearNomina,
                        vaImssPatron,vaImssMes,vaImssTipo, vaImssYear, vaCajaAhorro, vaEvaluacion, vaCotemarContrato  );



    {$ifdef SIGUIENTE_VERSION}
        eTipoNomina = eTipoPeriodo;
    {$endif}
    eLiqNomina = (lnNormal,lnLiquidacion,lnIndemnizacion );
    eTipoCalculo = (tcNueva,tcTodos,tcRango,tcPendiente);
    //FESTIVO
    eTipoFestivo = (tfFestivo,tfIntercambio);
    //HORARIO
    eTipoHorario = (thSinHorario,thNormal,thMadrugadaEntrada,thMadrugadaSalida,thMediaNoche);
    //REPORTE
    eTipoReporte = (trListado,trForma,trEtiqueta,trPoliza, trPolizaConcepto, trImpresionRapida, trFuturo);
    ListaReportes = set of eTipoReporte;
    eTipoRangoActivo = (raTodos,raActivo,raRango,raLista);
    eTipoInfoExp = (ieCompleto,ieCambiosOficiales,ieCambiosUsuario);
    eConflictoImp = (ciIgnorar,ciEncimar);
    eTipoRango = (rNinguno,rRangoEntidad,rRangoListas, rFechas, rBool );
    eOrganizacionHojas = (ohPorHoja, ohPorReporte);

    {CV: Diciembre 2002
    Se esta poniendo el IFDEF de Seleccion y luego un ELSE, para no tener que
    poner las directivas de compilacion dentro de todos los proyectos.
    Por que la mayoria de los proyectos hacen uses de ZetaCommonLists}
    {$ifdef COMPARTE_MGR}
    eClasifiReporte = ( crWorkFlow, crPortal, crSistema, crFavoritos, crSuscripciones );
    {$else}
          {$ifdef ADUANAS}
          eClasifiReporte = ( crFacturasPedimentos, crProductos, crCatalogos, crSistema, crFavoritos, crSuscripciones );
          {$else}
                {$ifdef SELECCION}
                eClasifiReporte = ( crSeleccion, crFavoritos, crSuscripciones );
                {$else}
                      {$ifdef VISITANTES}
                      eClasifiReporte = ( crVisitantes, crFavoritos, crSuscripciones );
                      {$else}
                             {$ifdef RDD}
                             eClasifiReporte = integer;
                             {$else}
                             eClasifiReporte = ( crEmpleados, {0}
                                                 crAsistencia,{1}
                                                 crNominas,   {2}
                                                 crPagosIMSS, {3}
                                                 crConsultas, {4}
                                                 crCatalogos, {5}
                                                 crTablas,    {6}
                                                 crSupervisor,{7}
                                                 crCafeteria, {8}
                                                 crMigracion, {9}
                                                 crCursos,    {10}
                                                 crLabor,     {11}
                                                 crMedico,    {12}
                                                 crFavoritos, {13}
                                                 crCarrera,   {14}
                                                 crKiosco,    {15}
                                                 crAccesos,   {16}
                                                 crEvaluacion, {17}
                                                 crCajaAhorro, {18}
                                                 crSuscripciones, {19}
                                                 crEmailEmpleados {20}
                                                 );
                             {$endif}
                      {$endif}
                {$endif}
          {$endif}
    {$endif}
    eTipoBanda = ( tbEncabezado, tbDetalle, tbPiePagina );
    eCandado = ( cnAbierto, cnSoloImprimir, cnCerrado );
    //CAMPOREPORTE
    eTipoCampo =( tcCampos,
                  tcGrupos,
                  tcOrden,
                  tcEncabezado,
                  tcPieGrupo,
                  tcFiltro,
                  tcParametro );
    eTipoOperacionCampo = (ocNinguno,ocCuantos,ocSuma,ocPromedio,ocMin,ocMax,ocAutomatico,ocSobreTotales,ocRepite);
    eTipoFormato =( tfImpresora,
                    tfASCIIFijo,
                    tfASCIIDel,
                    tfHTML,
                    tfCSV,
                    tfQRP,
                    tfTXT,
                    tfXLS,
                    tfRTF,
                    tfWMF,
                    tfMailMerge,
                    tfPDF,
                    tfBMP,
                    tfTIF,
                    tfPNG,
                    tfJPEG,
                    tfEMF,
                    tfSVG,
                    tfHTM,
                    tfWB1,
                    tfWK2,
                    tfDIF,
                    tfSLK,
                    tfXLSQR,
                    tfMailMergeXLS );
    eFormatoFormas =( tffImpresora,
                      tffHTML,
                      tffQRP,
                      tffTXT,
                      tffXLS,
                      tffRTF,
                      tffWMF,
                      tffPDF,
                      tffBMP,
                      tffTIF,
                      tffPNG,
                      tffJPEG,
                      tffEMF,
                      tffSVG,
                      tffHTM,
                      tffWB1,
                      tffWK2,
                      tffDIF,
                      tffSLK );
    eFormatoASCII =(faASCIIFijo,faASCIIDel{$ifdef OSRAM_INTERFAZ}, faASCIIPuntoComa {$endif});
    eExtFormato = eTipoFormato;

    //EVENTO
    eIncluyeEvento =(ieEmpleados,ieBajas,ieTodos);
    //GLOBAL
    eTipoGlobal =(tgBooleano,tgFloat,tgNumero,tgFecha,tgTexto,tgMemo,tgAutomatico, tgBlob);
    //CONCEPTO
    eTipoConcepto =(coAcumulado,coPercepcion,coDeduccion,coObligacion,coPrestacion,coCalculo,coCaptura,coResultados);
    //FALTAS
    eMotivoFaltaDias =( mfdInjustificada,mfdJustificada,mfdSinGoce,mfdConGoce,
    mfdSuspension,mfdOtrosPermisos, mfdVacaciones,mfdIncapacidad,mfdAguinaldo,
    mfdAsistencia,mfdNoTrabajados,mfdRetardo,mfdAjuste,mfdIMSS,mfdEM, mfdPrimaVacacional );
    eMotivoFaltaHoras =( mfhOrdinarias,mfhExtras,mfhDobles,mfhTriples,mfhRetardo,mfhDomingo,mfhAdicionales,
    mfhFestivo,mfhDescanso,mfhVacaciones,mfhConGoce,mfhSinGoce);
    //AHORRO
    eStatusAhorro =(saActivo,saCancelado);
    //PRESTAMO
    eStatusPrestamo =(spActivo,spCancelado,spSaldado);
    //AUSENCIA
    eStatusAusencia =(auHabil,auSabado,auDescanso);
    eTipoDiaAusencia =(daNormal,daIncapacidad,daVacaciones,daConGoce,daSinGoce,daJustificada,daSuspension,daOtroPermiso,daFestivo,daNoTrabajado);
    eAutorizaChecadas =(acConGoce,acSinGoce,acExtras,acDescanso,acConGoceEntrada,acSinGoceEntrada,acPrepagFueraJornada,acPrepagDentroJornada  );
    //CHECADAS
    eTipoChecadas =(chDesconocido,chEntrada,chSalida,chInicio,chFin,chConGoce,chSinGoce,chExtras,chDescanso,chConGoceEntrada,chSinGoceEntrada,chPrepagFueraJornada,chPrepagDentroJornada );
    eDescripcionChecadas =(dcDesconocida,dcOrdinaria,dcRetardo,dcPuntual,dcExtras,dcComida,dcDescanso1,dcDescanso2,dcDescanso,dcEntrada);
    //PARIENTES
    eTipoPariente =(paDesconocido,paPadre,paMadre,paHijo,paEsposo,paHermano,paPrimo,paTio,paSobrino,paAbuelo,paNieto,paOtros);
    //COLABORA
    eTipoInfonavit = (tiNoTiene,tiPorcentaje,tiCuotaFija,tiVeces,tiVecesUMA);
    eStatusEmpleado = ( steAnterior,
                        steEmpleado,
                        steVacaciones,
                        steIncapacidad,
                        steConGoce,
                        steSinGoce,
                        steJustificada,
                        steSuspension,
                        steOtroPermiso,
                        steBaja );
    eStatusEmpleadoCedula = ( steAnteriorCedula,
                        steEmpleadoCedula,
                        steVacacionesCedula,
                        steIncapacidadCedula,
                        steConGoceCedula,
                        steSinGoceCedula,
                        steJustificadaCedula,
                        steSuspensionCedula,
                        steOtroPermisoCedula,
                        steBajaCedula );
    eStatusConflictoExp = ( stcAnterior,
                            stcEmpleado,
                            stcVacaciones,
                            stcIncapacidad,
                            stcConGoce,
                            stcSinGoce,
                            stcJustificada,
                            stcSuspension,
                            stcOtroPermiso,
                            stcBaja,
                            stcPlanVaca );

    //VACACION
    eTipoVacacion =(tvCierre,tvVacaciones);
    //KARDEX
    eStatusKardex =(skCapturado,skImpreso,skExportado,skEntregado,skRecibido);
    //INCAPACIDAD
    eMotivoIncapacidad=(miInicial,miSubsecuente,miRecaida,miNinguno,miUnica,miAltaMedica,miValuacion,miDefuncion,miPrenatal,miPostnatal,miEnlace);
    eFinIncapacidad =(fiNoTermina,fiAltaMedica,fiPermanente,fiMuerte,fiRecaida,fiValuacionProv,fiValuacionDef,fiRevaluacionProv,fiRevaluacionDef);
    //PERMISOS
    eTipoPermiso =(tpConGoce,tpSinGoce,tpFaltaJustificada,tpSuspension,tpOtros);
    //LIQUIDACION IMSS
    eTipoLiqIMSS =(tlOrdinaria,tlExtemporanea,tlComplementaria);
    eStatusLiqIMSS =(slSinCalcular,slCalculadaParcial,slCalculadaTotal,slAfectadaTotal);
    //LIQUIDACION (DETALLE DE MOVIMIENTOS)
    eTipoLiqMov =(tlmMensual,tlmBimestral);
    eCodigoLiqMov = eTipoLiqMov;
    //TIPO DE AHORRO
    eTipoAhorro=( taNoSaldar, taAutomaticamente, taSaldarPreguntando );
    eAltaAhorroPrestamo=(apNoAgregar, apAgregarAutomaticamente, apAgregarPreguntando );
    eAltaUsuario=eAltaAhorroPrestamo;
    //GLOBALES
    eHorasExtras =(heNoPagar,heExtrasTopeSemanal,heExtrasTopeDiario,heDescansoTrabajado,heDescansoExtrasTopeSemanal,heDescansoExtrasTopeDiario);
    eBancaElectronica=(beNotiene,beBancomer,beBancomerSI);
    eYears=( ey94,
             ey95,
             ey96,
             ey97,
             ey98,
             ey99,
             ey00,
             ey01,
             ey02,
             ey03,
             ey04,
             ey05,
             ey06,
             ey07,
             ey08,
             ey09,
             ey10,
             ey11,
             ey12,
             ey13,
             ey14,
             ey15,
             ey16,
             ey17,
             ey18,
             ey19,
             ey20,
             ey21,
             ey22,
             ey23,
             ey24);
    eMeses=( emEnero,
             emFebrero,
             emMarzo,
             emAbril,
             emMayo,
             emJunio,
             emJulio,
             emAgosto,
             emSeptiembre,
             emOctubre,
             emNoviembre,
             emDiciembre );
    eZonaGeografica=(zgA,zgB,zgC);
    eSexo=(esMasculino, esFemenino);
    eDiasSemana=(dsDomingo,dsLunes,dsMartes,dsMiercoles,dsJueves,dsViernes,dsSabado);
    eTiempoDias = (etYear,etMeses,etDias);
    eMes13= (mtEnero,
             mtFebrero,
             mtMarzo,
             mtAbril,
             mtMayo,
             mtJunio,
             mtJulio,
             mtAgosto,
             mtSeptiembre,
             mtOctubre,
             mtNoviembre,
             mtDiciembre,
             mtMes13);
    eProcessStatus = ( epsEjecutando, epsOK, epsCancelado, epsError, epsCatastrofico );
    eCampoCalendario = (ccHoras,ccExtras,ccDobles,ccTriples,ccTardes,ccDesTra,ccPerCG,PerSG,ccStatus,ccTipoDia,ccAuTipo);
    eTipoJornadaTrabajo = ( tjtmatutino, tjtvespertino, tjtnocturno );
    {$ifdef COMPARTE_MGR}
    Procesos = ( prNinguno,
                 prReporteador );
    {$else}
           {$ifdef ADUANAS}
           Procesos = ( prNinguno,
                        prReporteador,
                        prDescargaPedimentos,
                        prDeshacerDescargas,
                        prCalcularImpuestos  );
           {$else}
           Procesos = ( prNinguno,                                          	{0  }
                        prRHSalarioIntegrado,                               	{1  }
                        prRHCambioSalario,                                  	{2  }
                        prRHPromediarVariables,                             	{3  }
                        prRHVacacionesGlobales,                             	{4  }
                        prRHAplicarTabulador,                               	{5  }
                        prRHAplicarEventos,                                 	{6  }
                        prRHImportarKardex,                                 	{7  }
                        prRHCancelarKardex,                                 	{8  }
                        prRHCursoTomado,                                    	{9  }
	
                        prASISPollRelojes,                                  	{10 }
                        prASISProcesarTarjetas,                             	{11 }
                        prASISCalculoPreNomina,                             	{12 }
                        prASISExtrasPer,                                    	{13 }
                        prASISRegistrosAut,                                 	{14 }
                        prASISCorregirFechas,                               	{15 }
                        prASISRecalculoTarjetas,                            	{16 }
                                                    	
                        prNOCalcular,                                       	{17 }
                        prNOAfectar,                                        	{18 }
                        prNOImprimirListado,                                	{19 }
                        prNOImprimirRecibos,                                	{20 }
                        prNOPolizaContable,                                 	{21 }
                        prNODesafectar,                                     	{22 }
                        prNOLimpiarAcum,                                    	{23 }
                        prNOImportarMov,                                    	{24 }
                        prNOExportarMov,                                    	{25 }
                        prNOPagosPorFuera,                                  	{26 }
                        prNOCancelarPasadas,                                	{27 }
                        prNOLiquidacion,                                    	{28 }
                        prNOCalculoNetoBruto,                               	{29 }
                        prNORastrearCalculo,                                	{30 }
                        prNOFoliarRecibos,                                  	{31 }
                                                    	
                        prNODefinirPeriodos,                                	{32 }
                        prNOCalculoAguinaldo,                               	{33 }
                        prNORepartoAhorro,                                  	{34 }
                        prNOCalculoPTU,                                     	{35 }
                        prNOCreditoSalario,                                 	{36 }
                        prNOISPTAnual,                                      	{37 }
                        prNORecalcAcum,                                     	{38 }
                                                    	
                        prIMSSCalculoPagos,                                 	{39 }
                        prIMSSCalculoRecargos,                              	{40 }
                        prIMSSRevisarSUA,                                   	{41 }
                        prIMSSExportarSUA,                                  	{42 }
                        prIMSSCalculoPrima,                                 	{43 }
                        prIMSSTasaINFONAVIT,                                	{44 }
                                                    	
                        prASISAjusteColectivo,                              	{45 }
                        prSISTBorrarBajas,                                  	{46 }
                        prSISTBorrarTarjetas,                               	{47 }
                        prSISTDepurar,                                      	{48 }
                        prSISTMigracion,                                    	{49 }
                        prRHTranferencia,                                   	{50 }
                        prNOPagoAguinaldo,                                  	{51 }
                        prNOPagoRepartoAhorro,                              	{52 }
                        prNOPagoPTU,                                        	{53 }
                                                    	
                        prSISTCierreAhorros,                                	{54 }
                        prSISTCierrePrestamos,                              	{55 }
                        prSISTBorrarNominas,                                	{56 }
                                                    	
                        prNOISPTAnualPago,                                  	{57 }
                        prNOPagoRecibos,                                    	{58 }
                        prNOReFoliarRecibos,                                	{59 }
                        prNOCopiarNominas,                                  	{60 }
                        prNOCalcularDiferencias,                            	{61 }
                        prRHCancVacGlobales,                                	{62 }
                        prSISTBorrarPOLL,                                   	{63 }
                        prLabCalcularTiempos,                               	{64 }
                        prLabDepuracion,                                    	{65 }
                        prLabLecturas,                                      	{66 }
                        prLabImportarOrdenes,                               	{67 }
                        prReporteador,                                      	{68 }
                        prRHCierreVacacionGlobal,                           	{69 }
                        prRHEntregarHerramienta,                            	{70 }
                        prRHRegresarHerramienta,                            	{71 }
                        prSISTBorrarHerramienta,                            	{72 }
                        prLabImportarPartes,                                	{73 }
                        prNOCalcularNueva,                                  	{74 }
                        prDepuraSeleccion,                                  	{75 }
                        prLabCancelarBreaks,                                	{76 }
                        prRHImportarAltas,                                  	{77 }
                        prNOCreditoAplicado,                                	{78 }
                        prRHRenumeraEmpleados,                              	{79 }
                        prRHPermisoGlobal,                                  	{80 }
                        prNOCalculaRetroactivos,                            	{81 }
                        prLabAfectarLabor,                                  	{82 }
                        prLabImportarComponentes,                           	{83 }
                        prRHBorrarCursoTomado,                              	{84 }
                        prNODeclaracionAnual,                               	{85 }
                        prCAFEComidasGrupales,                              	{86 }
                        prCAFECorregirFechas,                               	{87 }
                        prLabImportarCedulas,                               	{88 }
                        prLabImportarCambioAreas,                           	{89 }
                        prLabImportarProduccionIndividual,                  	{90 }
                        prSISTNumeroTarjeta,                                	{91 }
                        prRHCambioTurno,                                    	{92 }
                        // MA: Procesos para Evaluaci�n de Desempe�o        	
                        prDEAgregaEmpEvaluarGlobal,                         	{93 }
                        prDEPrepararEvaluaciones,                           	{94 }
                        prDECalcularResultados,                             	{95 }
                        prDERecalcularPromedios,                            	{96 }
                        prDECierraEvaluaciones,                             	{97 }
                        prRHImportarAsistenciaSesiones,                     	{98 }
                        prRHCancelarCierreVacaciones,                       	{99 }
                        prDECorreosInvitacion,                              	{100}
                        prDECorreosNotificacion,                            	{101}
                        prDEAsignaRelaciones,                               	{102}
                        //Presupuestos                                      	
                        prPREDepuraSupuestosRH,                             	{103}
                        prPREDepuraNominas,                                 	{104}
                        prPRECalculaCubo,                                   	{105}
                        prPRESimulacionPresupuestos,                        	{106}
                        prDECreaEncuestaSimple,                             	{107}
                        prASISIntercambioFestivo,                           	{108}
                        prASISCancelarExcepcionesFestivos,                  	{109}
                        prRHCancelarPermisosGlobales,                       	{110}
                        prRHImportarAhorrosPrestamos,                       	{111}
                        prRHRecalculaSaldosVacaciones,                      	{112}
                        //Procesos Cotemar (Administrador de Guardias)      	
                        prCTCalcularPreNominaGuardias,       {AV: 5-Sept-06}	{113}
                        prCTRegistrarCambiosPlazaVacantes,   {AV: 21-Sept-06	{114}
                        prCTRegistrarCambiosEnvioTransicion, {AV: 21-Sept-06	{115}
                        prCTCrearCalendarioGuardias,         {AV: 25-Sept-06	{116}
                        prCTRegistrarSubieron,               {AV: 25-Sept-06	{117}
                        prCTRegistrarBajaron,                {AV: 25-Sept-06	{118}
                        prCTRegistrarPermanecenABordo,       {AV: 25-Sept-06	{119}
                        prCTRegistrarPermanecenEnTierra,     {AV: 25-Sept-06	{120}
                        prCTProgramarViajes,                 {AV: 24-Oct-06}	{121}
                        prCTAsignarViaje,                    {AV: 30-Oct-06}	{122}
                        prCTLiberarGuardiasALogistica,       {AV: 15-Nov-06}	{123}
                        prCTPollPDA,                         {AV: 30-Nov-06}	{124}
                        prCTCambiarStatusVacantes,                          	{125}
                        prCTGenerarReciboEmpleado,                          	{126}
                        { Procesos de ICU Medical }                         	
                        prICUProgramarViajes,                               	{127}
                        prICUAsignarViaje,                                  	{128}
                        prICUPollPDA,                                       	{129}
                        prICUPDA,                                           	{130}
                        { Procesos Permutas de Solvay }                     	
                        prSolAplicarPermutas,                               	{131}
                        prSolDepurarPermutas,                               	{132}
                        { Alta M�dica Guardias 14/03/07}
                        prCTCambiarStatusAltaMedica,                        	{133}
                        prNOAjusteRetFonacot,                               	{134}
                        prNOCalcularPagoFonacot,                            	{135}
                                                    	
                        prCTGenerarPlantillasOT,   {AV: 30-Abr-06}          	{136}
                        {Sincronizacion}
                        prCTExportar,                                       	{137}
                        prCTRespalda,                                       	{138}
                        prCTComprime,                                       	{139}
                        prCTTransfiere,                                     	{140}
                        prCTDescomprime,                                    	{141}
                        prCTRestaura,                                       	{142}
                        prCTImporta,                                        	{143}
                        prCTImportaHorasExtras,                                 {144}
                        prSistEnrolarUsuario,                                   {145}
                        prSistImportarEnrolamientoMasivo,                       {146}
                        prRDDExportarDiccionario,                               {147}
                        prRDDImportarDiccionario,                               {148}
                        prIMSSAjusteRetInfonavit,                                {149}

                        prCTSolicitudesPDA,                                     {150}{AV: 16.Jul.08}
                        prCTAbrirPreNominaGuardias,                             {151}{AV: 16.Jul.08}
                        prCTCabinasCamas,                                       {152}{AV: 16.Jul.08}
                        prCTCambioRegistroPatronal,                             {153} {AV: 25.Ag.08}
                        prASISReprocesaTarjetas,                                {154}
                        prEmpAplicacionFiniGlobal,                              {155}
                        prEmpLiquidacionSimFinGlobal,                           {156}
						            prRHImportarInfonavit,									{157}
                        prRHCursoProgGlobal,                                    {158}
                        prRHCancelarCursoProgGlobal,                            {159}
                        prASIAjustIncapaCal, {ACL: 06.Abril.09}                 {160}
                        prCTCalculaTarifas,  									                  {161} { Calculo de tarifas - Talleres Estrella 25/06/07 }
                        prRHFoliarCapacitaciones,                               {162}
                        prNoDeclaracionAnualCierre,                             {163}
                        prASISAutorizacionPreNomina,                            {164}
                        prASISRegistrarExcepcionesFestivos,                     {165}
                        prIMSSValidacionMovimientosIDSE,                        {166}
                        prNoTransferenciasCosteo,                               {167}
                        prNoCalculaCosteo,                                      {169}
                        prNoCancelaTransferencias,                              {170}
                        prASISImportacionClasificaciones,                       {168}{JB: 20111031 Importacion de Clasificaciones}
                        prSISTDepuraBitBiometrico,                              {171} // SYNERGY
                        prSISTAsignaNumBiometricos,                             {172} // SYNERGY
                        prSISTAsignaGrupoTerminales,                            {173} // SYNERGY
                        prRHImportarSGM,                                        {174}
                        prRHRenovacionSGM,                                      {175}
                        prRHBorrarSGM,                                          {176}
			            prNOPrevioISR,                                          {177}
                        prRHRevisarDatosSTPS,                                   {178}
                        prRHReiniciarCapacitacionesSTPS,                        {179}
                        prSISTImportaTerminales,                                {180} // SYNERGY
                        prRHImportarOrganigrama,                                {181}
						prNoTimbrarNomina,
                        {Motor de Patch 6/Mar/2014 RCM}
                        prPatchCambioVersion,                                   {182}
                        prPatchImportarDiccionario,                             {183}
                        prPatchImportarReportes,                                {184}
                        prPatchScriptEspecial,                                  {185}
                        {Fin Motor de Patch 6/Mar/2014 RCM}

                        prSISTPrepararPresupuestos,                             {186}
                        prSISTImportarTablas,                                   {187}
                        prRHImportarImagenes,                                   {188}
                        prSISTImportarTablasCSV,                                {189}
                        prSISTAgregarBaseDatosSeleccion,                        {190}
                        prSISTAgregarBaseDatosVisitantes,                       {191}
                        prSISTAgregarBaseDatosPruebas,                          {192}
                        prSISTAgregarBaseDatosEmpleados,                        {193}
                        prSISTBorrarTimbradoNominas,                            {194}
                        prSISTExtraerChecadas,                               	{195}
						prSISTRecuperarChecadas, 					            {196}
                        prNOAjusteRetFonacotCancelacion,                       	{197}
                        prEmpCambioTurnoMasivo,                                 {198}
                        prEmpPensiones,                                         {199}
                        prCAFEhistorial,                                        {200}
                        prNOImportarAcumuladosRS,                            	{201}
                        prNORecalcAhorros,                          	        {202}
                        prSISTImportarCafeteria,                                {203}
                        prDEAgregarEncuesta,                                    {204}
                        prNORecalcPrestamos,                                    {205}
                        prSISTAgregarTareaCalendarioReportes,                   {206}
                        prSISTServicioCorreos,                                  {207}
                        prNOFonacotImportarCedulas,                             {208}
                        prNOFonacotGenerarCedula,                               {209}
                        prNOFonacotEliminarCedula,                               {210}
                        //Conciliacion Timbrado Nomina
                        prIniciarConciliacionTimbrado,                          {211}
                        prAplicarConciliacionTimbrado,                          {212}
                        prNoPendienteNominaEmpleado                             {213}
                        );

           {$endif}
    {$endif}
     ProcesosVersion = set of Procesos;
    TTipoFuente = ( eNegrita,
                    eItalica,
                    eSubrayado,
                    eComprimido,
                    eEject,
                    eAncho10,
                    eAncho12,
                    eExtra1,
                    eExtra2,
                    eReset,
                    eLineas6,
                    eLineas8,
                    eLandscape );

    TFuente = set of TTipoFuente;
    eTipoEvaluador= ( evUsoGeneral, evBase, evReporte, evNomina, evPoliza, evISPTAnual, evAguinaldo, evReglasPrestamo );
    eOperacionConflicto = ( ocReportar, ocIgnorar, ocSumar, ocSustituir );

    eStatusLectura = (slCompleto,slIncompleto);
    eTipoLectura = ( wtChecada,
                     wtFija,
                     wtLibre,
                     wtPrecalculada,
                     wtBreak );

    eTipoOpera = (toDirecta,toIndirecta,toExcepcion,toEntrada,toSalida,toComida,toPermiso);
    eWorkStatus = (wsEnProceso,wsCerrado,wsCancelado,wsSuspendido);
    eTipoWOrder = ( ewUnaOrden, ewHistorial, ewKardex, ewLookup );
    eTipoCedula = (tcOperacion,tcEspeciales,tcTMuerto);
    eRangoFechas = ( rfHoy,
                     {$ifndef ADUANAS}
                     rfNominaActiva,
                     {$ifdef QUINCENALES} rfAsistenciaActiva, {$endif}
                     {$endif}
                     rfSemanaActual,
                     rfQuincenaActual,
                     rfMesActual,
                     rfBimestreActual,
                     rfYearActual,
                     rfAyer,
                     rfLunesPasado,
                     rfSemanaPasada,
                     rfQuincenaPasada,
                     rfMesPasado,
                     rfBimestrePasado,
                     rfYearPasado );
    eCafeCalendario = ( cfComida1, cfComida2, cfComida3, cfComida4, cfComida5, cfComida6,
                        cfComida7, cfComida8, cfComida9, cfNormal, cfProhibe, cfValida, cfNoValida );
    eMotTools = ( mtNoDescontar, mtReposicion, mtBaja );
    eDescuentoTools = ( dtFijo, dtFijoNoUtil, dtProporcional, dtProporcionalRestante );
    eEmailFormato = (efHTML,efAttach);
    eEmailFrecuencia = (frDiario,frSemanal,frMensual,frCadaHora,frEspecial);
    eEmailNotificacion = (enEnviar,enNoEnviar);
    eEmailType = ( emtTexto, emtHTML, emtTextoError );
    eCamposConteo = ( coNinguno, coPuesto, coTurno, coClasifi, coConfidencial, coNivel1, coNivel2,
                      coNivel3, coNivel4, coNivel5, coNivel6, coNivel7, coNivel8, coNivel9 {$ifdef ACS}, coNivel10, coNivel11, coNivel12{$endif} );
    ePuertoSerial = ( psNinguno, psCom1, psCom2, psCom3, psCom4 );
    eRegla3x3 = ( r3x3_Proporcional, r3x3_Cuantas, r3x3_SoloDobles );
    eTipoCompany = ( tc3Datos, tcRecluta, tcVisitas, tcOtro, tc3Prueba, tcPresupuesto );
    eFunciones = ( efGenerales,efTress,efNomina,efReporte,
                   efPoliza,efAnuales,efImss,efISPT,efCafe,efComunes, efMedico, efAguinaldo,efPolizaGral, efReglasPrestamos,
                   efReporteBalanza, efFonacot );

    ePrioridad = ( prAlta, prNormal, prBaja );
    eReqStatus = ( stPendiente, stCubierta, stCancelada );
    eMotivoVacante = ( mvRotacion, mvPlazaNueva, mvTemporal, mvReemplazo, mvOtro );
    eEstudios = ( esNinguno, esPrimaria, esSecundaria, esBachillerato, esTecnico, esPasante, esTitulado, esMaestria, esDoctorado,esEspecialidad );
    eSolStatus = ( stDisponible, stEnTramite, esNoDisponible, esListaNegra, esContratado );{OP:18.Abr.08}
    eEdoCivil = ( ecSoltero, ecCasado, ecDivorciado, ecUnionLibre, ecViudo );
    eCanStatus = ( csSeleccionado, csPorContratar, csRechazado, csContratado, csNoAcepto );
    eEntStatus = ( esContratar, esEvaluarOtro, esRechazar );
    eTipoExamen = ( teMedico, tePsico, teTecnico, teOtro, teVista, teHabilidad, teAntidoping, teIngles );{OP:15.Abr.08}
    eOperacionEnAlta = (opOtraAlta, opEditar, opTerminar );
    //MA: Numerados para Lista Fija de Servicio medico
    eTipoConsulta = ( tcConsGral, tcAccidente, tcEmbarazo );
    eTipoEstudio  = ( teAntiestrol,teCalcemia,teCitologico,teColesterol,teGlucemia,teHematrocito,teOrina,teUremia  );
    eTipoAccidente = ( taLesionPersonal, taDanoPropiedad,taOtro  );
    eMotivoAcc     = ( maFactorPers, maFactorLaboral, maMedioAmb, maOtro );
    eTipoMedicina  = ( tmAntibiotico, tmAntigripal, tmOtro );
    eTipoDiagnost  = ( tdAccidentologia,tdAlteraciondelosliquidos,tdAlteraciondelmedio,tdAntecedentes,tdAntecedentesPsicologicos, tdOtro );
    eTipoLesion    = ( tlEnlaEmpresa, tlEnunaComision, tlEnTrayectoasuTrabajo, tlEnTrayectoasuDomicilio, tlTrabajandoTiempoExtra );
    eAxAtendio     = ( axServicioMedico, axIMSS, axOtro );
    eAxTipo        = ( axAccidente, axEnfermedad, axOtros );
    eExCampoTipo   = ( xctTexto, xctNumero, xctBooleano, xctBoolStr, xctFecha, xctTabla, xctMemo, xctCombo, xctListBox );
    eExMostrar     = ( arSiempre, arNunca, arLleno, arVacio );
    eExTipo        = ( exEmpleado, exCandidato, exPariente, exOtro );
    eEmTermino     = ( emNormal, emPrematuro, emAborto );
    eTurnoLabor    = ( tlOtros, tlMatutino, tlVespertino, tlNocturnoEntrada, tlNocturnoSalida );
    eTipoArea      = ( taDirecto, taIndirecto, taAdministrativo );
    eTipoKarFecha  = ( tkKardex, tkCapturo );
    eCancelaMod    = ( cmGlobales, cmManuales, cmTodas );
    eInicioAmortizacion = ( iaBimestre, iaMes, iaEsteMes );
    eClaseAcciones = ( eaCurso, eaDidactico, eaActividad, eaOtra );
    eTipoCompete   =  ( etcHabilidad, etcCompetencia, etcOtra );
    eStatusAcciones = ( esaAsignado, esaPendiente, esaCumplido );
    eStatusCursos = ( escProgramado, escPendiente, escTomado );
    eTipoCedInspeccion = ( citEnProceso, citFinal, citOtro );
    eResultadoCedInspeccion = ( cirAceptado, cirRechazado, cirSuspendido, cirOtro ) ;
    eValorDiasVacaciones = ( dvNoUsar, dvSinRedondeo, dvRedondearArriba, dvRedondearAbajo );
    eFiltroEmpOrganigrama = ( tpTitulares, tpJefe, tpSubordinados );
    eTipoPuesto = ( ptoEmpleado, ptoDirectivo, ptoGerente, ptoAsistente, ptoExterno, ptoOtro );
    eOperacionIncremento = ( oiSumar, oiRestar );
    eFormatoImpFecha = ( ifDDMMYYs, ifDDMMYYYYs, ifDDMMYY, ifDDMMYYYY,
                         ifMMDDYYs, ifMMDDYYYYs, ifMMDDYY, ifMMDDYYYY,
                         ifYYMMDDs, ifYYYYMMDDs, ifYYMMDD, ifYYYYMMDD );
    eOut2Eat = ( e2Sistema, e2SiComio, e2NoComio );
    eEmpleadoRepetido= ( erSumarUltimo, erSumarPrimero, erUltimo, erPrimero, erNoIncluir, erIncluirTodos );
    eTipoCorregirFechas = ( tcfAmbos, tcfComidas, tcfInvitaciones );
    eTipoNavegador =( tnPantalla, tnPagina );
    eOrientacionNavegador =( onHorizontal, onVertical );
    ePosicionNavegador =( pnTopLeft, pnTopCenter, pnTopRigth,
                          pnCenterLeft, pnCenter, pnCenterRigth,
                          pnBottomLeft, pnBottomCenter, pnBottomRigth );
    eCompressionLevel = (clFastest, clNormal, clMaxCompress);
    eEncryptionLevel = (el40bit, el128bit);
    eFontEncoding = (feWinAnsiEncoding, feMacRomanEncoding, feMacExpertEncoding, feStandardEncoding, fePDFDocEncoding);
    ePageLayout = (plSinglePage, plOneColumn, plTwoColumnLeft, plTwoColumnRight);
    ePageMode = (pmUseNone, pmUseOutlines, pmUseThumbs, pmFullScreen);
    eTransitionEffect = ( teNone, teSplitHorizontalInward,
                          teSplitHorizontalOutward, teSplitVerticalInward, teSplitVerticalOutward,
                          teBlindsHorizontal, teBlindsVertical, teBoxInward, teBoxOutward,
                          teWipeLeftToRight, teWipeBottomToTop, teWipeRightToLeft, teWipeTopToBottom,
                          teDissolve, teGlitterLeftToRight, teGlitterTopToBottom,
                          teGlitterTopLeftToBottomRight);


    eTipoGafete      = ( egTress, egProximidad{$ifndef DOS_CAPAS}, egBiometrico{$endif} );
    eChecadasSimultaneas = (egAndarPreguntando, egAgregarSinPreg, egNoPreguntar);
    eTipoProximidad  = ( epWiegand, epSerial, epClockData );
    eAccesoRegla     = ( arInformativo, arEvalEntrada, arEvalSalida, arEvaluacion );
    eTipoKind        = ( tkNumero, tkNombre, tkEmpresa, tkStatus, tkHorario, tkTurno, tkSuper, tkFoto, tkExtras, tkDescanso, tkPermisoCG, tkPermisoSG, tkPermisoCGEntrada, tkPermisoSGEntrada, tkInfo, tkEval );

    {Numerados de Visitantes}
    eAnfitrionStatus = ( afActivo, afSuspendido );
    eVisitanteStatus = ( vfActivo, vfSuspendido );
    eCasetaStatus    = ( cfActiva, cfCancelada );
    eCitaStatus      = ( csSi, csNo, csCancelada );
    eCatCompanys     = ( ccActiva, ccSuspendida );
    eLibroStatus     = ( lsDentro, lsFuera );
    eCorteStatus     = ( ecAbierto, ecCerrado );
    eBusquedaLibro   = ( blNombre, blEmpresa, blVisitaA, blDepartamento, blPlacas, blGafete );
    eImpresionGafete = ( igNoImprimir, igImprimirPreguntando, igImprimirSinPreguntar );
    {$ifdef COMPARTE_MGR}
    eWFStatusModelo  = ( smwActivo, smwSuspendido, smwCancelado );
    eWFTareaStatus   = ( sttInactiva, sttActiva, sttEsperando, sttTerminada, sttCancelada );
    eWFPasoProcesoStatus = ( ppInactivo, ppActivo, ppEsperando, ppTerminado, ppCancelado, ppSuspendido );
    eWFProcesoStatus = ( prInactivo, prActivo, prTerminado, prCancelado, prSuspendido );
    eWFNotificacion  = ( nvNinguno, nvRol, nvUsuarioEspecifico, nvJefe, nvJefeDelJefe, nvSubordinadosDirectos, nvNivel, nvMismo, nvParticipantes, nvResponsable );
    eWFNotificacionCuando = (ncIniciar, ncTerminar, ncCancelar, ncModificar, ncDespuesIniciar, ncAntesLimite, ncDespuesLimite);
    eWfNotificacionQue = (nqNotificar, nqAutorizar, nqLanzar);
    eWFAutenticacionWebService = ( auDefault, auAnonimo, auUsuarioClave );
    eWFCodificacionWebService = ( coWebServiceDocument, coWebServiceRPC );
    {$endif}
    {$ifdef ADUANAS}
    eTipoCliente     = ( atcCliente, atcProveedor, atcAmbos );
    eTipoMaterial    = ( atmMateriaPrima, atmProductoTerminado, atmActivoFijo, atmOtros );
    eTipoMedida      = ( atuPiezas, atuLongitud, atuArea, atuVolumen, atuPeso, atuOtro );
    eStatusFactura   = ( esfNormal, esfLigado, esfAfectado, esfDescargado, esfRectificado, esfBloqueado );
    eFacTipoProceso  = ( ftpIndustrial, ftpServicio );
    eTipoActivo      = ( taMaquinaria, taHerramienta );
    eStatusPedimento = ( espNormal, espLigado, espAfectado, espDescargado, espRectificado, espBloqueado );
    eProgramasTipoProceso = ( ptpIndustrial, ptpServicio, ptpAmbos );
    eDescargaAlmacen = ( aldMaterial, aldDesperdicio );
    eDescargaTipo = ( edtMaterialInicial, edtMaterialExportar,edtMaterialDesperdicio, edtMaterialMerma, edtDesperdicioEntrada, edtDespercicioExportar );
    {$endif}
    // **** Evaluacion de Desempe�o **********//
    eStatusEncuesta  = ( stenDiseno,
                         stenAbierta,
                         stenCerrada );
    eComentariosMGR  = ( cmDefault, cmNo, cmSi );
    eComentariosPedir = ( cpedNo, cpedOpcional, cpedObligatorio, cpedValorMinimo, cpedValorMaximo, cpedExtremos );
    eEstiloRespuesta = ( erComboBox, erRadioButton, erTextoLibre, erCalculada, erCalculadaRadio );
    eTipoEvaluaEnc = ( tevMismo, tevJefe,tevSubordinado, tevColega, tevCliente, tevOtros );
    eStatusEvaluaciones = ( seNoHanTerminado,
                            seAlgunasNuevas,
                            seAlgunasProceso,
                            seAlgunasTerminadas,
                            seTodasNuevas,
                            seTodasProceso,
                            seTodasTerminadas );
    eStatusSujeto  = ( ssNuevo,
                       ssListo,
                       ssPreparado,
                       ssEnProceso,
                       ssTerminado );
                       
    eStatusInscrito  = ( siActivo, siBaja, siEnEspera, siAprobado );
    eStatusSesiones  = ( ssAbierta, ssCerrada );
    eTipoReserva     = ( rvBloqueo, rvSesion );
    eStatusEvaluacion = ( sevNueva,
                          sevLista,
                          sevPreparada,
                          sevEnProceso,
                          sevTerminada );
    eEVCorreoStatus  = ( ocsSinEnviar, ocsEnviadoOK, ocsErrorAlEnviar );
    eTipoSupuestoRH  = ( srEventoAlta, srEvento );
    eTipoPeriodoMes = ( pmPrimero, pmUltimo );

    eComoCalificar = ( eccaNoUsar, eccaSumar, eccaPromediar );
    eStatusAcuerdo = ( escpSinPreguntar, escpSi, escpNo );

    eDPEscolar = ( dpeNinguna, dpeBachillerato, dpeProfesionalParcial, dpeProfesional, dpeMaestria, dpePostgrado );
    eDPNivel = ( dpnNinguna, dpnConocimientoBasico, dpnConocimientoTecnico, dpnComunicarse, dpnDominio );
    eDPExpPuesto = (dppNinguna, dpp1, dpp2, dpp4, dpp7, dpp10 );
    eJITipoPlaza = ( jitpPlanta, jiEventual );
    eJIPlazaReporta = ( jiprLinea, jiprStaff );

    eKJustificacion = ( ekIzquierda, ekDerecha );
    eAccionBoton = (abContenido, abPantalla, abRegresar, abMisDatos, abOtro, abReporte, abCambiaNIP );
    eLugarBoton  = (lbDefault, lbEspaciado, lbAbajo);
    ePosicionIcono = {$ifdef DOTNET}(blGlyphLeft, blGlyphRight, blGlyphTop, blGlyphBottom){$else}TButtonLayout{$endif};

    eTipoDiasBase = ( dbFijos, dbPeriodo, dbTurno );
    eBorrarMovimSUA = ( bsNinguno, bsTodo, bsMes );
    eStatusPlanVacacion = ( spvPendiente, spvAutorizada, spvRechazada, spvProcesada );
    ePagoPlanVacacion = ( pvGozo, pvNinguno );
    //STATUS POLIZA
    eStatusPoliza = (spNormal,spError);
    //Caja de Ahorro
    eTiposdePrestamo =(tdpIguales, tdpDiferidos);
    //Controles
    eControles = (coTextoCorto, coTextoLargo, coNumero , coFecha, coCombo, coLista );
    //subir bajar de orden
    eSubeBaja = (eSubir, eBajar);
    //Status Festivo
    eStatusFestivo = (esfAutomatico, esfFestivo, esfFestivoTransferido);

    eTratamientoExtras = (eteHorasExtrasTriples, eteRegla3X3);
    eDiasAplicaExento = ( daeTodos, daeHabiles ,daeHabilesSinFestivos );
    eRolGuardias = ( rgRolA, rgRolB, rgRolC, rgRolD );
    eStatusPlaza = ( spOcupada, spVacante );
    eStatusGuardia = ( sgActivo, sgCancelado );
    eClaseKarPlaza = ( ckpRegistro, ckpCancelacion );
    eTipoKarPlaza = ( tkpPermanente, tkpTemporal );
    eEstadosGuardia = ( egProgramado, egLiberado, egReservado, egProcesoEmbarque, egTransitoAbordar, egAbordo, egCancelado );
    eRitmosGuardia = ( erg1414, erg2814, erg2828, erg61);

    eTipoABordoGuardia = (etagABordo, etagEnTierra);
    eTipoDiaGuardia = (etdgTrabajo, etdgDescanso);
    eTipoPagoGuardia = (etpgNoAplica, etpgNormal, etpgDescanso, etpgCompensacion, etpgAdicional, etpgHorasExtras);

    eTipoEventoGuardia = (etegStatusLogistica, etegEmbarco, etegDesembarco, etegAusente, etegCubreTemporalmente, etegPermaneceABordo, etegPermaneceEnTierra, etegPermaneceACubrir, etegCompensacion, etegHorasExtras, etegCambioPlazaAbordo, etegCambioPernocta);
    eClaseEventoGuardia = ( ecvgRegistro, ecvgCancelacion, ecvgCancelado );
    eStatusAusenciaGuardia =(augTrabaja,augSabado,augDescanso);

    eTipoServicioLogistica = ( etslTransporte, etslHospedaje );
    eViaLogistica = ( evlMaritima, evlHelicoptero, evlTerrestre, evlAvion );
    eSentidoLogistica = ( eslEmbarque, eslDesembarque, eslDomicilio, eslRetorno, eslInterplataformas );
    eTipoPuertoLogistica = (etplPuertoMar, etplPuertoBus, etplPuertoAereo);
    eStatusViaje = ( viajePrograma, viajeSalio, viajeLlego, viajeCancelado);
    eStatusTarjeta = ( estNueva,estPendiente,estProceso,estRelacionada,estRechazada,estCancelada );
    eTipoTarjeta = ( ettTitular,ettReposicionTitular,ettAdicional,ettReposicionAdicional );
    ePuEmbarque =  ( emMaritima,emHelicoptero );
    ePuDomicilio = ( doTerrestre,doAvion );
    ePuHabitacion = ( haDoble,haSencilla,haVIP,haTriple );
    eStatusTarea = ( estaPendiente,estaTerminada,estaCancelada );
    eStatusHosp  = ( estaSolicitada, estaTerminadaHos, estaCanceladaHos );
    eSentidoHosp = ( sentiEmbarque, sentiDesembarque, sentiOtro );
    eStatusTrasporte = ( estaCancelado, estaliberado, estaReservado, estaEnProceso, estaEnTransito, estaArribo, estaNoPresento );
    eTipoCalEvent = ( etEmbarque, etDesembarque, etInterplataformas );
    eTipoRuta    = ( etPublica, etConsolidada, etEnlace );
    eStatusDePlaza = ( plazaActiva, plazaCancelada );
    ePrioridadVac = ( PriAlta, PriNormal, PriBaja );
    eStatusVac = ( esDetenida, estaContratar, esCubierta );
    ePasoVac = ( PasPendiente, PasPrecontratado, PasContratado );
    eTrabaja = ( SiDescansa, SiTrabaja );
    ePlantillaTipo = ( esPEMEX, esCOTEMAR, esOtro, esTemporal);
    ePrioridadSolicitud = ( prioridadAlta, prioridadNormal, prioridadBaja );
    eCicloSolicitud = ( cicloSencillo, cicloRedondo ) ;
    eReservacion = ( esElectronica, esManual ) ;
    eStatusAutoriza = ( esPendiente, esautorizado, esRechazado );
    eSentidoCamion = ( eSalida, eLlegada, eMixta);
    eStatusCamion = ( eProgramado, eSalio, eLlego, eCancelado);
    eStatusPasajero = ( eReservado, eLibre );
    eLogEventos   = ( eCorrectos, eAdvertencia, eErrores, eEntrevistados);
    eTipoPermuta = ( tpPorDia, tpPorHora );
    ePiramidacionPrestamos = ( eDespuesPiramidacion, eAntesPiramidacion );
    eClaseSistBitacora     = ( clbNingunoSist, clbCatalogoUsuarios, clbCatalogoGrupoUsuarios, clbDerechosAcceso,clbAsignacionRoles {$ifndef DOS_CAPAS},clbTerminalesGTI, clbBiometrico{$endif},clbCalendarioReportesSist );
    eAltaMedica = ( esIncapacidad, esPermiso );

    eStatusTEvento = ( esPendienteEvento, esExitoEvento, esErrorEvento );
    eTurnoAbordo  = ( eMatutino, eVespertino, eNoAplica  );
    eBitacoraStatus = ( esEnProceso, esEnExito, esEnError );
    eGrupoRecibo = ( esPercepcion, esDeduccion );
    eSubStatus = ( esNoCambio, esSolCambio, esCambioTer, esBoletoEmi );
    eTipoPuestoContrato = ( epcPermanente, epcTemporal );
    eTipoPlaza = ( plPermanente, plTemporal  );
    eTipoCobertura = ( cobTitular, cobAusencia, cobVacante );

    eStatusPlantilla = ( spDiseno, spLiberada, spCancelada );
    eStatusValuacion = ( svProceso, svTerminada, svCancelada );

    eReclasificaBool = (erbSinCambio,erbSi,erbNo);
    eReclasificaRelacion = ( errSinCambio, errLinea, errStaff );
    eReclasificaTipoVigencia = ( ertvSinCambio,ertvPLanta, ertvEventual ) ;
    eReclasificaZonaGeo = ( rzgSinCambio, erzA, erzB, erzC );
    eClaseBitacoraCOTEMAR = ( clbNingunoCote, clbPlaza, clbPuestoXCon, clbPlantillaAuto );
    eClaseBitacoraCOTEMARLogistica = ( clbNingunoCoteLog, clbSolTran, clbSolHosp, clbViaje, clbRuta );

    //LEPE
    eModulosSolTran = ( mstLogistica, mstGuardias, mstWorkflow, mstKiosco );

    eViaticoNivelRestriccion = ( vnrInformativo, vnrAdvertencia, vnrRechazo );
    eViaticoTopeConceptoGasto = ( vtcTodos, vtcConTope, vtcSinTope );

    eAccionBitacora = ( abiNinguno, abiContenido, abiMisDatos, abiReporte, abiCambioNIP, abiLogin, abiLogout, abiErrorBrowser );

    eTipoLogin=( tlLoginTress, tlAD, tlADLoginTress );
    eFiltroStatusEmpleado=( fseActivos, fseInactivos, fseTodos);
    eAccionRegistros = ( arSobreescribir,arIgnorar);
    eDiasCafeteria = ( dcTodaSemana, dcLunes, dcMartes, dcMiercoles, dcJueves, dcViernes, dcSabado, dcDomingo );
    eRequisitoTipo = ( rtDocumento, rtOtro );{OP:16.Abr.08}
    eTipoMovInfonavit = ( infoInicio, infosuspen, inforeinicio, infocambiotd, infocambiovd, infocambionocre );
    eBimestres = ( bimEneFeb, bimMarAbr, bimMayJun, bimJulAgo, bimSepOct, bimNovDic );
    eStatusReglaPrestamo = ( rpActivo, rpTodos );

    eFasesContingencia = ( efcPermaneceABordo, extFase1, extFase2, extFase );
    eStatusPersonaExterna = ( espeDesembarco, espeEnTransito, espePernocta, espeABordo );

    eStatusHospedaje = ( eshEntrada, eshSalida );

    eClaseNominaCOTEMAR = (cncNominasOrdinarias,cncNominasFinales,cncNominasExtraordinarias,cncNominasFiniquitos,cncNominasPrestamos,cncNominasAnuales,cncOtras);
    eLstDispositivos = ( dpRelojChecados, dpCafeteria, dpCaseta, dpOtros, dpKiosco, dpMisDatos );
    eStatusSimFiniquitos = (ssfSinAprobar,ssfAprobada,ssfLiquidada);
    eTipoDifInfonavit = (DifInfNoCredito, DifReinicioDescuento, DifInfTipoDescuento, DifInfValorDesc, DifInfOtorgamiento);
    eClaseRiesgo = ( claseNingun, claseOrdinario, claseBajo, claseMedio, claseAlto, claseMaximo );

    eObjetivoCursoSTPS     = (ocsSinDefinir, ocsActualizaPerfeccConHab, ocsPrevRiesgTrab, ocsIncProductividad, ocsMejorarNivelEduc, ocsPrepOcupVacPuesto );
    eModalidadCursoSTPS    = (modSinDefinir, modPresencial, modEnLinea, modMixta );
    eDiscapacidadSTPS      = (disSinDefinir, disMotriz, disVisual, disMental, disAuditiva,disDeLenguaje );
    eTipoInstitucionSTPS   = (tiSinDefinir, tiPublica, tiPrivada);
    eTipoDocProbatorioSTPS = (tdpSinDefinir, tdpTitulo, tdpCertificado, tdpDiploma,tdpOtro);
    eStatusDeclaracionAnual = (esdSinCalcular, esdAbierta, esdCerrada );
    eTipoAgenteSTPS = (taNinguno, taInterno, taExterno, taOtros );
    eTipoHoraTransferenciaCosteo = ( tcOrdinarias, tcExtras );
    eStatusTransCosteo = ( stcPorAprobar, stcAprobada, stcCancelada );
    eStatusTransCosteoConsulta = (stcPorAprobarMio, stcPorAprobarOtro, stcAprobadaMio, stcAprobadaOtro, stcCanceladaMio, stcCanceladaOtro, stcTodos );

    eTipoDescConciliaInfonavit = (tciBimestral,  tciSoloAcmulado);
    eTipoSGM = (tsGastosMayores,tsGastosMenores,tsOtros);
    eTipoEmpSGM = (tesTitular,tesDependiente);
    eStatusSGM = (ssActiva,ssVencida,ssCancelada);
    eTipoBrigada = (tbNinguna, tbPrimerosAuxilios, tbBusquedaRescate, tbEvacuacion, tbPrevencionCombateIncendios,
                   tbMultifuncional, tbDerrame, tbOtro); {AL. V2013}
    eUsoConceptoNomina = (ucnTodas, ucnOrdinarias, ucnEspeciales); {AL. V2013}
    eTipoAsegurado = (taEmpleado,taPariente);

    eAuthTressEmail = ( authSinLogin,  authLogin );
    eEvaluaTotalEmpleados = ( evIgnorar, evDebajoUmbral, evEnUmbral, evRebasaAdvierte, evRebasaRestringe, evPruebasRestringe, evRebasaAdvierteDemo, evRebasaRestringeDemo );
    eEvaluaOperacion = ( evOpAlta, evOpNomina , evOpLogin );

    eStatusTimbrado = ( estiPendiente, estiTimbradoParcial, estiTimbrado, estiTimbradoPendiente, estiCancelacionPendiente, estiTimbradoPendienteLimpiar, estiCancelacionPendienteLimpiar );
    eStatusTimbradoPeticion = ( estpPendiente, estpProceso, estpTermino, estpError );

    eClaseConceptosSAT = ( esatNoAplica, esatPercepcion, esatDeduccion, esatExento, esatOtrosPagos );
    eTipoExentoSAT  = ( exentoNoAplica, exentoCien, exentoMonto, exentoConcepto, exentoGravaCien, exentoGravaMonto,  exentoGravaConcepto, exentoCancelacion );
    eTipoRegimenesSAT = ( rsatNoAplica, rsatReservado1, rsatSueldos, rsatJubilados, rsatPensionados, rsatAsimiladosMSCP, rsatAsimiladosISAC,rsatAsimiladosDir,rsatAsimiladosActEmp, rsatAsimiliadosHonorarios, rsatAsimiladosAcciones);
    eMetodoPagoSAT = ( msatNoAplica, msatEfectivo, msatDepositoCuenta, msatCheque, msatTarjetaCredito, msatTarjetadebito, msatEfectivoYMonedero,msatDepositoCuentaYMonedero,msatChequeYMonedero,msatTarjectaCreditoyMonedero,msatTarjetaDebitoyMonedero,msatMixto  );

    eCampoTipo = ( cdtTexto, cdtNumero, cdtFecha, cdtBooleano, cdtLookup, cdtNinguno );    //Adicionales de Seleccion - Se reubicaron de
    eTipoColumnaReporte = (tcNombre, tcCodigo,tcClasificacion,tcTablaPrincipal);
    eClasifiPeriodo = (tpDiario, tpSemanal, tpCatorcenal, tpQuincenal, tpMensual,tpDecenal);
    eTPDias = ( vTPDUno, vTPDSeis, vTPDDoce, vTPDTrece, vTPDVeintiseis, vTPDNueve );
    eTPDias7 = ( vTPD7Uno, vTPD7Siete, vTPD7Catorce, vTPD7Quince, vTPD7Treinta, vTPD7Diez );
    eTPHoras = ( vTPHOcho, vTPHCuarenta, vTPHNoventa, vTPHCuatro, vTPHDoscientos, vTPHSetenta  );
    eTPHorasJo = ( vTPHJN1, vTPHJN2, vTPHJN3, vTPHJS4, vTPHJS5, vTPHJS6 );
    eTPDiasBT = ( vTPDBTCero1, vTPDBTCero2, vTPDBTCero3, vTPDBTCero4, vTPDBTCero5, vTPDBTCero6 );
    eClasifiTipoPeriodoConfidencial = ( tcPDiario,tcPSemanal,tcPCatorcenal,tcPQuincenal,tcPMensual,tcPDecenal );
    eStatusSisDispositivos = (tNoAplica, tConfigurada, tNoConfigurada);
    eSolicitudEnvios = (tcSEPendiente, tcSEProcesando, tcSEConerror, tcSEFinalizado, tcSECancelar, tcSECancelado );
    ePrevisionSocialClasifi = (pscNoAplica, pscPrevisioSocial, pscSumaSalarios);
    eStatusBajaFonacot = ( sbfNoAplica, sbfPagoDirectoFonacot, sbfConfirmacionFonacot, sbfAusenteCedula, sbfOtro );
    eMesesFonacot=( emfNoAplica, emfEnero, emfFebrero, emfMarzo, emfAbril, emfMayo, emfJunio, emfJulio, emfAgosto, emfSeptiembre,
                    emfOctubre, emfNoviembre, emfDiciembre );
    eIdTerminales=(eitVacio, eitHuellaD, eitHuellaDProxHID, eitHuellaDProxMIFARE, eitHuellaDCodigoBarras, eitReconocimientoFacial );
    eTipoArchivoCedula=(etacPorCredito, etacPorTrabajador);
var
   { Debe guardar mismo orden que ennumerado ZetaCommonList.Procesos }
   aProcesos: array[ Procesos ] of ZetaPChar = (
              '0', { Todos = Enga�a al TStringList.Sort }
              {$ifdef COMPARTE_MGR}
              'Consultas - Reportes'
              {$else}
              {$ifdef ADUANAS}
              'Consultas - Reportes',
              'Pedimentos - Descargas',
              'Pedimentos - Deshacer descargas',
              'C�lculo de Impuestos'

              {$else}
              'Recursos - Salario Integrado',
              'Recursos - Cambio Salario',
              'Recursos - Promediar Variables',
              'Recursos - Vacaciones Globales',
              'Recursos - Aplicar Tabulador',
              'Recursos - Aplicar Eventos',
              'Recursos - Importar Kardex',
              'Recursos - Cancelar Kardex',
              'Recursos - Curso Tomado',
              'Asistencia - Poll Relojes',
              'Asistencia - Procesar Tarjetas',
              'Asistencia - Calculo PreN�mina',
              'Asistencia - Extras y Permisos',
              'Asistencia - Registros Autom�ticos',
              'Asistencia - Corregir Fechas',
              'Asistencia - Recalcular Tarjetas',
              'N�mina - Calcular',
              'N�mina - Afectar',
              'N�mina - Imprimir Listado',
              'N�mina - Imprimir Recibos',
              'N�mina - P�liza Contable',
              'N�mina - Desafectar',
              'N�mina - Limpiar Acumulados',
              //'N�mina - Importar Movimientos',
              {*** US 13895: Modificar proceso Importar movimientos de n�mina para delimitar su alcance a excepciones de n�mina ***}
              'N�mina - Importar Excepciones de N�mina',
              {*** FIN ***}
              'N�mina - Exportar Movimientos',
              'N�mina - Pagos Por Fuera',
              'N�mina - Cancelar N�minas Pasadas',
              'N�mina - Liquidaci�n',
              'N�mina - Calcular Salario Neto/Bruto',
              'N�mina - Rastrear C�lculo',
              'N�mina - Foliar Recibos',
              'N�mina - Definir Per�odos',
              'N�mina - Calcular Aguinaldo',
              'N�mina - Reparto de Ahorro',
              'N�mina - Calcular PTU',
{$ifdef ANTES}
              'N�mina - Declaraci�n Cr�dito Al Salario',
{$else}
              'N�mina - Declaraci�n Subsidio al Empleo',
{$endif}
              'N�mina - Declaraci�n de ISPT Anual',
              'N�mina - Recalcular Acumulados',
              'IMSS - Calcular Pagos',
              'IMSS - Calcular Recargos',
              'IMSS - Revisar SUA',
              'IMSS - Exportar SUA',
              'IMSS - Calcular Prima de Riesgo',
              'IMSS - Calcular Tasa INFONAVIT',
              'Asistencia - Ajuste Colectivo',
              'Sistema - Borrar Bajas',
              'Sistema - Borrar Tarjetas',
              'Sistema - Depurar',
              'Sistema - Migraci�n',
              'Recursos - Transferencia de Empleados',
              'N�mina - Pagar Aguinaldo',
              'N�mina - Pagar Reparto de Ahorro',
              'N�mina - Pagar PTU',
              'Sistema - Cierre de Ahorros',
              'Sistema - Cierre de Pr�stamos',
              'Sistema - Borrar N�minas',
              'N�mina - Pagar ISPT Anual',
              'N�mina - Pago de Recibos',
              'N�mina - ReFoliar Recibos',
              'N�mina - Copiar N�minas',
              'N�mina - Calcular Diferencias',
              'Recursos - Cancelar Vacaciones Globales',
              'Sistema - Borrar POLL',
              'Labor - Calcular Tiempos',
              'Labor - Depuraci�n',
              'Labor - Procesar Lecturas',
              'Labor - Importar Ordenes',
              'Consultas - Reportes',
              'Recursos - Cierre Global De Vacaciones',
              'Recursos - Entregar Herramienta',
              'Recursos - Regresar Herramienta',
              'Sistema - Borrar Herramienta',
              'Labor - Importar Partes',
              'N�mina - Preparar',
              'Selecci�n - Depurar',
              'Labor - Cancelar Breaks',
              'Recursos - Importar Altas',
{$ifdef ANTES}
              'N�mina - Cr�dito Aplicado Mensual',
{$else}
              'N�mina - SUBE Aplicado Mensual',
{$endif}
              'Recursos - Renumeraci�n de Empleados',
              'Recursos - Permisos Globales',
              'N�mina - Calcular Retroactivos',
              'Labor - Afectar Labor',
              'Labor - Importar Componentes',
              'Recursos - Cancelar Curso Tomado',
              'N�minas - Declaraci�n Anual',
              'Cafeter�a - Registro Grupal de Comidas',
              'Cafeter�a - Corregir Fechas Globales',
              'Labor - Importar C�dulas',
              'Labor - Importar Cambio de Area',
              'Labor - Importar Producci�n Individual',
              'Sistema - Actualizar Numero de Tarjeta',
              'Recursos - Cambio Masivo de Turnos',
              'Evaluaci�n - Agregar Empleados a evaluar global',
              'Evaluaci�n - Preparar Evaluaciones',
              'Evaluaci�n - Calcular resultados',
              'Evaluaci�n - Recalcular promedios',
              'Evaluaci�n - Cerrar Evaluaciones',
              'Recursos - Importar cursos tomados',
              'Recursos - Cancelar cierre de vacaciones',
              'Evaluaci�n - Enviar correos de invitaci�n',
              'Evaluaci�n - Enviar correos de notificaci�n',
              'Evaluaci�n - Asignaci�n de relaciones',
              'Presupuestos - Depuraci�n de Supuestos de Personal',
              'Presupuestos - Depuraci�n de N�minas',
              'Presupuestos - Calculo del Cubo',
              'Presupuestos - Simulaci�n',
              'Evaluaci�n - Crear encuesta simple',
              'Asistencia - Intercambio de festivos',
              'Asistencia - Cancelar excepciones de festivos',
              'Recursos - Cancelar permisos globales',
              'Recursos - Importar ahorros y pr�stamos',
              'Recursos - Recalcular saldos de vacaciones',
              'Adm. de Guardias - C�lculo de Tarjetas',
              'Adm. de Guardias - Cambiar Plazas Vac.',
              'Adm. de Guardias - Enviar a transici�n',
              'Adm. de Guardias - Calendario Guardias',
              'Adm. de Guardias - Subieron',
              'Adm. de Guardias - Bajaron',
              'Adm. de Guardias - Permanecen a bordo',
              'Adm. de Guardias - Permanecen en tierra',
              'Log�stica - Programar Viajes',
              'Log�stica - Asignar Viaje',
              'Adm. de Guardias - Liberar a Log�stica',
              'Log�stica - Poll PDA',
              'Adm. de Guardias - Cambio Status Vacante',
              'Adm. de Guardias - Generar Recibo Empleado',
              'Transporte - Programar viajes',
              'Transporte - Asignar viajes',
              'Transporte - Importar registros de viaje',
              'Transporte - PDA de pasajeros',
              'Aplicar Permutas de Asistencia',
              'Depurar Permutas de Asistencia',
              'Adm. de Guardias - Alta M�dica',
              'N�mina - Ajuste de Retenci�n Fonacot',
              'N�mina - Calcular Pago Fonacot',
              'Adm. de Guardias - Generar Plantillas OT',
              'Sincronizador - Exportar',
              'Sincronizador - Respaldar',
              'Sincronizador - Comprimir',
              'Sincronizador - Transferir',
              'Sincronizador - Descomprimir',
              'Sincronizador - Restaurar',
              'Sincronizador - Importar',
              'Adm. de Guardias - Importar Horas Extras',
              'Sistema - Enrolamiento Masivo',
              'Sistema - Importar Enrolamiento Masivo',
              'Configurador Reportes - Exportar Diccionario',
              'Configurador Reportes - Importar Diccionario',
              'IMSS - Ajuste de Retenci�n INFONAVIT',
 	            'Adm. de Guardias - Generar Solicitudes PDA',
              'Adm. de Guardias = Abrir Tarjetas',
              'Cabinas y Camas - Generar Cabinas y Camas'{OP: 18.May.08} , 
              'Adm. de Guardias - Cambio Registro Patronal' {AV: 5.Ag.08},
              'Asistencia - Reprocesar Tarjetas',
              'Recursos - Aplicar Finiquitos Global',
              'Recursos - Simulaci�n Finiquitos Global',
              'Recursos - Importar Kardex de Infonavit',
              'Recursos - Curso Programado Global',
              'Recursos - Cancelar Curso Programado Global',
              'Asistencia - Ajustar Incapacidades en Calendario',
              'Talleres estrella - C�lculo de tarifas' { Talleres Estrella },
              'Recursos - Foliar Capacitaciones de STPS',
              'N�mina - Cierre Declaraci�n Anual',
              'Asistencia - Autorizaci�n de Pre-N�mina',
              'Asistencia - Registro de Excepciones de Festivo',
              'IMSS - Validaci�n Movimientos IDSE',
              'N�mina - Transferencias',
              'N�mina - C�lculo de Costeo',
              'N�mina - Cancelaci�n de Transferencias',
              'Asistencia - Importaci�n de Clasificaciones',{JB 20111031 Wizard de Importacion de Clasificaciones}
              'Sistema - Borrar Bit�cora Terminales GTI', // SYNERGY
              'Sistema - Asigna N�meros Biom�tricos', // SYNERGY
              'Sistema - Asigna Grupo de Terminales', // SYNERGY
              'Recursos - Importar Seguros de Gastos M�dicos',
              'Recursos - Renovar Seguros de Gastos M�dicos',
              'Recursos - Borrar Seguros de Gastos M�dicos',
			  'N�mina - C�lculo Previo ISR',
              'Recursos - Revisar datos para STPS',
              'Recursos - Reiniciar Folio Capacitaciones de STPS',
              'Sistema - Importaci�n de Terminales', // SYNERGY
              'Recursos - Importar Organigrama',
              'N�mina - Timbrar',
     		  'Sistema - Actualizaci�n de Base de Datos',
              'Sistema - Importar Diccionario',
              'Sistema - Importar Reporte',
              'Sistema - Script Especial',
              'Sistema - Preparar Presupuestos',
              'Sistema - Importar Tablas',
              'Recursos - Importar Imagenes',
              'Sistema - Importar Tablas en CSV',
              'Sistema - Agregar Base de Datos Seleccion',
              'Sistema - Agregar Base de Datos Visitantes',
              'Sistema - Agregar Base de Datos Pruebas',
              'Sistema - Agregar Base de Datos Tress',
              'Sistema - Borrar estatus de Timbrado',
              'Sistema - Extraer checadas Terminales GTI',
              'Sistema - Transformar checadas Terminales GTI',
              'N�mina - Cancelar Ajuste de Retenci�n Fonacot',
              'Recursos - Cambio Masivo de Turnos',
              'Empleados - Pensi�n Alimenticia',
              'Cafeter�a - Historial',
              {*** US 13895: Modificar proceso Importar movimientos de n�mina para delimitar su alcance a excepciones de n�mina ***}
              'N�mina - Importar Acumulados Raz�n social',
              //'N�mina - Recalcular Acumulados Raz�n social'
              {*** US 13909: Modificar proceso Recalcular Acumulados para delimitar el alcance solo a acumulados de n�mina***}
              'N�mina - Recalcular Ahorros',
              {*** FIN ***}
              {*** FIN ***}
              'Sistema - Copiar Configuraci�n',
			  'Evaluaci�n - Agregar encuesta',
              'N�mina - Recalcular Pr�stamos',
              'Sistema - Agregar nuevo registro al Administrador de env�os programados',
              'Sistema - Servicio de Correos',
              'N�mina - Importar c�dula de cr�ditos Fonacot',
              'N�mina - Generaci�n de c�dula de pago Fonacot',
              'N�mina - Eliminar registros de c�dula Fonacot',
              //Conciliacion Timbrado Nomina
              'N�mina - Conciliaci�n de periodos de n�mina',                          {2010}
              'N�mina - Aplicar conciliaci�n a periodos de n�mina',                        {2011}
              'N�mina - Cambiar estatus a pendiente de empleado'                           {2012}
              {$endif}
              {$endif}
              );

			  aProcesosVersion :procesosVersion;

 			  FArregloPeriodo, FArregloTipoNomina, FArregloPeriodoConfidencial, FListaTiposPeriodoConfidencialidad: TStrings;

function GetOffSet( Tipo: ListasFijas ): Integer;
function ObtieneElemento( Tipo: ListasFijas; const Index: Integer ): String;
procedure LlenaLista( Tipo: ListasFijas; Lista: TStrings );

function GetDescripcionLista(FLista: TStrings; iLlave: integer): String;
{*** US 15153: El usuario requiere que los procesos y operaciones sobre Expediente que utilizan el tipo de periodo sigan funcionando ***}
function GetValidoPeriodoKArdex(FLista: TStrings; iLlave: integer): String;
function GetClasificacionPeriodo(FLista: TStrings; const iTipoPeriodo: Integer): eClasifiPeriodo;


{$ifdef COMPARTE_MGR}
function GetNotificacionDebeEspecificar( const eValor: eWFNotificacion ): Boolean;
procedure LlenaNotificacionItems( Lista: TStrings );
{$endif}

implementation

const
     //USUARIO
    aNivelUsuario: array[ eNivelUsuario ] of ZetaPChar = ('Sin Prioridad','Prioridad M�xima','Prioridad Muy Alta','Prioridad Alta','Prioridad Normal','Prioridad Baja','Prioridad Muy Baja','Prioridad M�nima');
    //BITACORA
    aTipoBitacora: array[ eTipoBitacora ] of ZetaPChar = ('Normal','Advertencia','Error','Error Grave');
    {$ifdef COMPARTE_MGR}
    aClaseBitacora: array[ eClaseBitacora ] of ZetaPChar = ( '< Ninguno >',
                                                             'Cat�logos - Globales de Empresa',
                                                             'Modelos - Suspender',
                                                             'Modelos - Reactivar',
                                                             'Modelos - Cancelar',
                                                             'Modelos - Reiniciar',
                                                             'Procesos - Suspender',
                                                             'Procesos - Reiniciar',
                                                             'Procesos - Cancelar',
                                                             'Tareas - Reasignar',
                                                             'Servicio de Workflow Email',
                                                             'Servicio de WorkFlow Manager',
                                                             'Evaluador - F�rmulas'
                                                             );
    {$else}
    {$ifdef KIOSCOS}
    aClaseBitacora: array[ eClaseBitacora ] of ZetaPChar = ( '< Ninguno >',
                                                              'Contenido',
                                                              'Mis Datos',
                                                              'Reporte',
                                                              'Cambio de NIP',
                                                              'LogIn',
                                                              'LogOut',
                                                              'Error al Navegar' );
    {$else}
    {$ifdef ADUANAS}
    aClaseBitacora: array[ eClaseBitacora ] of ZetaPChar = ( '< Ninguno >',
                                                             'Cat�logos - Globales de Empresa',
                                                             'Cat�logos - Condiciones',
                                                             'Cat�logos - Contribuyentes',
                                                             'Cat�logos - Importadores y exportadores',
                                                             'Cat�logos - Transportistas',
                                                             'Cat�logos - Agentes aduanales',
                                                             'Cat�logos - Materiales',
                                                             'Cat�logos - Kardex de materiales',
                                                             'Cat�logos - Grupos de material',
                                                             'Cat�logos - Marcas',
                                                             'Cat�logos - Empaques',
                                                             'Cat�logos - Unidades de medida',
                                                             'Cat�logos - BOM',
                                                             'Cat�logos - BOMCOMP',
                                                             'Cat�logos - Similares',
                                                             'Cat�logos - Contenedores',
                                                             'Cat�logos - Choferes',
                                                             'Cat�logos - Tractocamiones',
                                                             'Cat�logos - Tipos de contenedor',
                                                             'Cat�logos - Medios de transporte',
                                                             'Cat�logos - Aduanas',
                                                             'Cat�logos - Fracciones arancelarias',
                                                             'Cat�logos - Kardex de fracciones arancelarias',
                                                             'Cat�logos - Contribuciones',
                                                             'Cat�logos - Formas de pago',
                                                             'Cat�logos - Identificadores',
                                                             'Cat�logos - Terminos de facturaci�n',
                                                             'Cat�logos - INPC',
                                                             'Cat�logos - Recargos',
                                                             'Cat�logos - Monedas',
                                                             'Cat�logos - Pa�ses',
                                                             'Cat�logos - Entidades',
                                                             'Cat�logos - Pruebas suficientes',
                                                             'Cat�logos - Regulaciones',
                                                             'Cat�logos - Sectores',
                                                             'Cat�logos - Tipos de tasa',
                                                             'Cat�logos - Tipos de valor',
                                                             'Cat�logos - Tratados de libre comercio',
                                                             'Cat�logos - Vinculaciones',
                                                             'Cat�logos - Tipos de permiso',
                                                             'Cat�logos - Grupos de vigencia',
                                                             'Cat�logos - Cuota de material',
                                                             'Cat�logos - Materiales PROSEC',
                                                             'Cat�logos - Materiales de regulaci�n',

                                                             'Cat�logos - Operaciones',
                                                             'Cat�logos - Tipos de pedimento',
                                                             'Cat�logos - Claves de pedimento',
                                                             'Cat�logos - Categor�as de material',
                                                             'Cat�logos - Operaciones y sus claves',
                                                             'Cat�logos - Claves de descargas',
                                                             'Cat�logos - Programas',
                                                             'Cat�logos - Permisos',
                                                             'Cat�logos - Regla octava',
                                                             'Cat�logos - Sensibles',
                                                             'Cat�logos - Fracciones autorizadas',
                                                             'Cat�logos - Fracciones con PROSEC',
                                                             'Cat�logos - Certificados de origen',
                                                             'Proceso de Descargas',
                                                             'Proceso de Deshacer Descargas',
                                                             'Proceso de C�lculo de Impuestos',
                                                             'Facturas',
                                                             'Partidas',
                                                             'Partidas - Cuotas',
                                                             'Partidas - Regulaciones',
                                                             'Partidas - PROSEC',
                                                             'Pedimentos',
                                                             'Pedimentos - Cuotas',

                                                             'Evaluador - F�rmulas',
                                                             'Cat�logos - Tipos de servicios',
                                                             'Cat�logos - Rate of duty',
                                                             'Cat�logos - Special programs',
                                                             'Cat�logos - Fracciones americanas',
                                                             'Cat�logos - Part types',
                                                             'Cat�logos - Entry type'
                                                             );
    {$else}
    {$ifdef VISITANTES}
    aClaseBitacora: array[ eClaseBitacora ] of ZetaPChar = ( '< Ninguno >',
                                                         'Tablas - Departamento',
                                                         'Tablas - Tipo de Identificaci�n',
                                                         'Tablas - Tipo de Veh�culo',
                                                         'Tablas - Tipo de Asunto',
                                                         'Tablas - Tipo de Visitante',
                                                         'Tablas - Condiciones',
                                                         'Registros - Visitas',
                                                         'Registros - Citas',
                                                         'Registros - Cortes',
                                                         'Cat�logos - Anfitriones',
                                                         'Cat�logos - Visitantes',
                                                         'Cat�logos - Casetas',
                                                         'Cat�logos - Compa��as',
                                                         'Cat�logos - Globales de Empresa',
                                                         'Sistema - Actualizaci�n # Tarjeta',
                                                         'Evaluador - F�rmulas' );
    {$else}
    aClaseBitacora: array[ eClaseBitacora ] of ZetaPChar = ( '< Ninguno >',
                                                          'Kardex - Movimiento General',
                                                          'Kardex - Cambio de Puesto',
                                                          'Kardex - Cambio de Turno',
                                                          'Kardex - Cambio de Area',
                                                          'Historial - Permiso',
                                                          'Asistencia - Ajuste',
                                                          'Asistencia - Cambio de Horario',
                                                          'Asistencia - Aut. Horas Extras',
                                                          'Asistencia - Aut. Descanso Trabajado',
                                                          'Asistencia - Aut. Permiso CG',
                                                          'Asistencia - Aut. Permiso CG Entrada',
                                                          'Asistencia - Aut. Permiso SG',
                                                          'Asistencia - Aut. Permiso SG Entrada',
                                                          'Supervisores - Asignaci�n Empleados',
                                                          'Asistencia - Cambio de Tipo de D�a',
                                                          'Cat�logo de Turnos',
                                                          'Cat�logo de Horarios',
                                                          'Cat�logo de Condiciones',
                                                          'Cat�logo de Conceptos',
                                                          'Kardex - Alta del Empleado',
                                                          'Kardex - Baja del Empleado',
                                                          'Kardex - Reingreso del Empleado',
                                                          'Kardex - Cambio de Salario',
                                                          'Kardex - Renovacion de Contrato',
                                                          'Kardex - Cambio de Prestaciones',
                                                          'Historial - Incapacidad',
                                                          'Historial - Vacaciones',
                                                          'Supervisores - Registro de Labor',
                                                          'Asistencia - Modificar/Borrar Tarjeta',
                                                          'Historial - Cr�dito Infonavit',
                                                          'N�mina - Borrar Per�odo',
                                                          'N�mina - Borrar N�mina Del Empleado',
                                                          'Cat�logo de Globales de Empresa',
                                                          'Cat�logo de Puestos',
                                                          'Cat�logo de Cursos',
                                                          'Cat�logo de Calendario de Cursos',
                                                          'Cat�logo de Clasificaciones',
                                                          'Cat�logo de Contratos',
                                                          'Cat�logo de Eventos',
                                                          'Cat�logo de Festivos por Turno',
                                                          'Cat�logo de Folios',
                                                          'Cat�logo de Invitadores',
                                                          'Cat�logo de Maestros',
                                                          'Cat�logo de Matriz por Curso',
                                                          'Cat�logo de Matriz por Puesto',
                                                          'Cat�logo de Percepciones Fijas',
                                                          'Cat�logo de Per�odos',
                                                          'Cat�logo de Prestaciones',
                                                          'Cat�logo de Reglas de Cafeter�a',
                                                          'Cat�logo de Registro Patronal',
                                                          'Cat�logo de Par�metros de N�mina',
                                                          'Cat�logo de Herramientas',
                                                          'N�mina - Excepciones de Monto',
                                                          'N�mina - Excepciones de D�as/Hora',
                                                          'N�mina - Excepciones Globales',
                                                          'N�mina - Montos',
                                                          'Capacitaci�n - Cursos del Empleado',
                                                          'Capacitaci�n - Grupo',
                                                          'Sistema - Actualizaci�n # Tarjeta',
                                                          'Cat�logo de Reglas de Caseta',
                                                          'N�mina - Historial de Ahorros',
                                                          'N�mina - Historial de Pr�stamos',
                                                          'Empleados - Cambios a Datos',
                                                          'Empleados - Cursos Anteriores',
                                                          'Empleados - Puestos Anteriores',
                                                          'Evaluador - F�rmulas',
                                                          'Cat�logo de Aulas',
                                                          'Prerrequisitos para Curso',
                                                          'Cat�logo de Certificaciones',
                                                          'Cat�logo - Tipos de P�liza',
                                                          'Cat�logo - Secciones de Perfil',
                                                          'Cat�logo - Campos de Perfil',
                                                          'Cat�logo - Perfiles',
                                                          'Kardex - Cambio de Plaza',
                                                          'Kardex - Cambio de Tipo de N�mina'
                                                          {$ifdef CAJAAHORRO}
                                                          ,'Movimientos Caja Ahorro'
                                                          {$else}
                                                          ,'<Reservado Sistema TRESS>'
                                                          {$endif}
                                                          ,'Sistema - Suscripciones a Reportes'
                                                          ,'Sistema - Supervisores por Usuario'
                                                          ,'Sistema - Areas de Labor por Usuario'
                                                          ,'Asistencia - Cambio de Festivo'
                                                          ,'Asistencia - Cambio de Tom� de comida'
                                                          ,'Capacitaci�n - Reservaciones Aula'
                                                          ,'Capacitaci�n - Inscripciones'
                                                          ,'Cat�logo de Proveedores de Capacitaci�n'
                                                          ,'Cat�logo de Plantillas de Valuaci�n'
                                                          ,'Cat�logo de Factores de Valuaci�n'
                                                          ,'Cat�logo de Subfactores de Valuaci�n'
                                                          ,'Cat�logo de Niveles de Valuaci�n'
                                                          ,'Cat�logo - Razones Sociales'
                                                          ,'Asistencia - Aut. Horas Prep. fuera de jornada '
                                                          ,'Asistencia - Aut. Horas Prep. dentro de jornada'
                                                          ,'Configurador Reporteador - Clasificaciones'
                                                          ,'Configurador Reporteador - Lista Fijas'
                                                          ,'Configurador Reporteador - M�dulos'
                                                          ,'Configurador Reporteador - Campos de Tablas'
                                                          ,'Configurador Reporteador - Campos Default'
                                                          ,'Configurador Reporteador - Criterios de Orden'
                                                          ,'Configurador Reporteador - Filtros de Tablas'
                                                          ,'Configurador Reporteador - Relaciones de Tablas'
                                                          ,'Configurador Reporteador - Clasificaciones '
                                                          ,'Configurador Reporteador - M�dulos de Tablas'
                                                          ,'Configurador Reporteador - Tablas'
                                                          ,'Configurador Reporteador - Valores de Listas Fijas'
                                                          ,'Cat�logo de Matriz Certificaciones por Puesto'
                                                          ,'Cat�logo de Matriz Puestos por Certificaci�n'
                                                          ,'Asistencia - Credencial Inv�lida'
                                                          ,'Cat�logo de Reglas de Validaci�n de Pr�stamos'
                                                          ,'Lista de Tipos de Pr�stamos en Reglas'                                                          
                                                          ,'Capacitaci�n - Certificaciones del Empleado'
                                                          ,'Sistema - Borrar Bajas'
                                                          //(JB) Se agregan Clases de Bitacora para Tablas de Sistema
                                                          ,'Tabla de Estados del Pa�s'
                                                          ,'Tabla de Municipios'
                                                          ,'Tabla de Nivel #1'
                                                          ,'Tabla de Nivel #2'
                                                          ,'Tabla de Nivel #3'
                                                          ,'Tabla de Nivel #4'
                                                          ,'Tabla de Nivel #5'
                                                          ,'Tabla de Nivel #6'
                                                          ,'Tabla de Nivel #7'
                                                          ,'Tabla de Nivel #8'
                                                          ,'Tabla de Nivel #9'
                                                          ,'Tabla de Tipo Kardex'
                                                          ,'Tabla de Motivo de Baja'
                                                          ,'Tabla de Incidencias'
                                                          ,'Tabla de Motivos de Autorizaciones'
                                                          ,'Tabla de Motivos de Checadas Manuales'
                                                          ,'Tabla de Tipo de Ahorro'
                                                          ,'Tabla de Tipo de Pr�stamo'
                                                          ,'Tabla de Salarios M�nimos'
                                                          ,'Tabla de Cuotas del IMSS'
                                                          ,'Tabla de Num�ricas Generales'
                                                          ,'Tabla de Num�ricas Tablas'
                                                          ,'Tabla de Grados de Riesgo'
                                                          ,'Cat�logo Nacional de Ocupaciones'
                                                          ,'Tabla de �reas Tem�ticas de Cursos'
                                                          ,'Tabla Adicional #1'
                                                          ,'Tabla Adicional #2'
                                                          ,'Tabla Adicional #3'
                                                          ,'Tabla Adicional #4'
                                                          ,'Tabla Adicional #5'
                                                          ,'Tabla Adicional #6'
                                                          ,'Tabla Adicional #7'
                                                          ,'Tabla Adicional #8'
                                                          ,'Tabla Adicional #9'
                                                          ,'Tabla Adicional #10'
                                                          ,'Tabla Adicional #11'
                                                          ,'Tabla Adicional #12'
                                                          ,'Tabla Adicional #13'
                                                          ,'Tabla Adicional #14'
                                                          ,'Asistencia - Autorizaci�n de Pre-N�mina'
                                                          ,'Transferencias de Costeo'
                                                          ,'Asistencia - Importaci�n Clasificaciones Temp.'
                                                          ,'Cat�logo de Tipos de Pensi�n'
                                                          ,'Cat�logo de Seguros de Gastos M�dicos'
                                                          ,'Grupos de Costeo'
                                                          ,'Criterios de Costeo'
                                                          ,'Criterios por Concepto'
                                                          ,'Tabla Tipos de Competencias'
                                                          ,'Tabla Tipos de Perfiles'
                                                          ,'Cat�logo de Competencias'
                                                          ,'Cat�logo de Perfiles'
                                                          ,'Cat�logo de Matriz de Funciones'
                                                          ,'Cat�logo de Puestos - Perfiles'
                                                          ,'Competencias Individuales'
                                                          ,'Competencias Evaluadas'
                                                          ,'Cat�logo Nacional de Competencias'
                                                          ,'Asistencia - Apr. Horas Extras'
                                                          ,'Asistencia - Apr. Descanso Trabajado'
                                                          ,'Asistencia - Apr. Permiso CG'
                                                          ,'Asistencia - Apr. Permiso CG Entrada'
                                                          ,'Asistencia - Apr. Permiso SG'
                                                          ,'Asistencia - Apr. Permiso SG Entrada'
                                                          ,'Asistencia - Apr. Horas Prep. fuera de jornada '
                                                          ,'Asistencia - Apr. Horas Prep. dentro de jornada'
                                                          ,'Sistema - Cambio de versi�n'
                                                          ,'Sistema - Importar Diccionario'
                                                          ,'Sistema - Importar Reporte'
                                                          ,'Sistema - Script Especiales'
                                                          ,'Empleados - Importar Imagenes ' 
							  ,'Cat�logo de Bancos'
							  ,'Timbrado - Tipos de Conceptos SAT'
                										  ,'Unidad de Medida y Actualizaci�n'
                                                          ,'Timbrado - Tipos de Contratos SAT'
                                                          ,'Timbrado - Tipos de Jornada SAT'
                                                          ,'Timbrado - Riesgos de Puestos SAT'
                                                          ,'Empleados - Pensi�n alimenticia'
                                                          ,'Cafeter�a - Historial'
                                                          ,'R�gimen de Contrataci�n del Trabajador'
                                                          ,'Sistema - Agregar nuevo registro al Administrador de env�os programados'
							  							  ,'Sistema - Servicio de correos'							
															);



    {$endif}
    {$endif}
    {$endif}
    {$endif}

    //MOTIVO BAJA
    aMotivoBaja: array[ eMotivoBaja ] of ZetaPChar = ('Desconocido','T�rmino de Contrato','Separaci�n Voluntaria','Abandono','Defunci�n','Clausura','Otros','Ausentismo','Rescisi�n de contrato','Jubilaci�n','Pensi�n');
    //OTRAS PERCEPCIONS
    aTipoOtrasPer: array[ eTipoOtrasPer ] of ZetaPChar = ('Cantidad','Porcentaje');
    aIMSSOtrasPer: array[ eIMSSOtrasPer ] of ZetaPChar = ('Exento','Gravado 100%','Asistencia','Despensa');
    aISPTOtrasPer: array[ eISPTOtrasPer ] of ZetaPChar = ('Exento','Gravado 100%');
    //INCIDENCIAS
    aIncidencias: array[ eIncidencias ] of ZetaPChar = ('Kardex','Faltas','Incapacidad','Vacaciones','Permiso','Retardo','Otros');
    aIncidenciaIncapacidad: array[ eIncidenciaIncapacidad ] of ZetaPChar = ('No es incapacidad','Enfermedad General','Maternidad','Accidente de Trabajo','Accidente en Trayecto','Enfermedad de Trabajo');
    //    //REGISTRO PATRONAL
    aRPatronModulo: array[ eRPatronModulo ] of ZetaPChar = ('Desconocido','Permanente (M�dulo 10)','Eventual (M�dulo 18)','Construcci�n (M�dulo 19)');
    //    //TURNO
    aTipoTurno: array[ eTipoTurno ] of ZetaPChar = ('Normal','Sin Horario, Con Jornada','Sin Horario, Sin Jornada');
    aTipoJornada: array[ eTipoJornada ] of ZetaPChar = ('Desconocido','Semana Completa','Reducida 1 D�a / Sem','Reducida 2 D�as / Sem','Reducida 3 D�as / Sem','Reducida 4 D�as / Sem','Reducida 5 D�as / Sem','Menor a 8 horas');
    {acl Valida que no se obtengan valores vacios en el arreglo de periodo}
    aUsoPeriodo: array[ eUsoPeriodo ] of ZetaPChar = ('Ordinaria','Especial');
    aStatusPeriodo: array[ eStatusPeriodo ] of ZetaPChar = ('Nueva','Pre-N�mina Parcial','Sin Calcular','Calculada Parcial','Calculada Total','Afectada Parcial','Afectada Total');
    //    //MOVIMIEN
    aHorasDias: array[ eHorasDias ] of ZetaPChar = ( 'Todos', 'Solo Excepciones', 'Ninguno' );
    aImportacion: array[ eImportacion ] of ZetaPChar = ( 'Excepciones de N�mina', 'Acumulados del Mes' );
    aOperacionMontos: array[ eOperacionMontos ] of ZetaPChar = ( 'Sustituir', 'Sumar', 'Restar' );
    //    //NOMINA
    { Se cambio a array variable y este convirtio temporal
    aTipoNomina: array[ eTipoNomina ] of ZetaPChar = ('D�a','Semana','Catorcena','Quincena','Mes','Decena','Semana A','Semana B','Quincena A','Quincena B','Catorcena A','Catorcena B'); }
    {acl Valida que no se obtengan valores vacios en el arreglo de Nomina}    
    aRDDDefaultsTexto : array[ eRDDDefaultsTexto ] of ZetaPChar = ( '<>''''','=''''' );
    aRDDDefaultsFloat : array[ eRDDDefaultsFloat ] of ZetaPChar = ( '<>0','=0','>0','<0','>=0','<=0' );
    aRDDDefaultsLogico : array[ eRDDDefaultsLogico ] of ZetaPChar = ('S�', 'No', 'Todos' );
    aRDDValorActivo : array[ eRDDValorActivo ] of ZetaPChar = ( 'Sin Valor', 'Empleado', 'N�mero de N�mina', 'Tipo de N�mina', 'A�o de N�mina',
                                                                'Registro Patronal', 'Mes del Imss', 'Tipo del Imss', 'A�o del Imss' , 'Caja de Ahorro', 'Evaluaci�n de Desempe�o', 'Contrato' );

    aLiqNomina: array[ eLiqNomina ] of ZetaPChar = ( 'Normal', 'Liquidaci�n', 'Indemnizaci�n' );
    aTipoCalculo: array[ eTipoCalculo ] of ZetaPChar = ('Nueva','Todos','Rango','Pendiente');
    //    //FESTIVO
    aTipoFestivo: array[ eTipoFestivo ] of ZetaPChar = ('D�a Festivo','Intercambio de Fecha');
    //    //HORARIO
    aTipoHorario: array[ eTipoHorario ] of ZetaPChar = ('Sin Horario','Normal','Madrugada, Entrada=F.Entrada','Madrugada, Entrada=F.Salida','Media Noche');
    //    //REPORTE
    aTipoReporte: array[ eTipoReporte ] of ZetaPChar = ('Listado','Forma','Etiqueta','P�liza','P-Concepto','I-R�pida','<FUTURO>');
    aTipoRangoActivo: array[ eTipoRangoActivo ] of ZetaPChar = ('Todos','Activo','Rango','Lista');
    aTipoRangoActivo2: array[ eTipoRangoActivo ] of ZetaPChar = ('Todas','Activa','Rango','Lista');
    aTipoRango: array[ eTipoRango ] of ZetaPChar = ( 'Ninguno','Hacia una tabla', 'Hacia una lista', 'Fechas', 'L�gicos');
    aTipoInfoExp : array[ eTipoInfoExp ] of ZetaPChar = ( 'Diccionario Completo','S�lo Cambios Oficiales','S�lo Cambios del Cliente');
    aConflictoImp: array[ eConflictoImp ] of ZetaPChar = ( 'Diccionario actual (Cambios del cliente)', 'El archivo a Importar' );

    {$ifdef COMPARTE_MGR}
    aClasifiReporte: array[ eClasifiReporte ] of ZetaPChar = ( 'WorkFlow',
                                                               'Portal',
                                                               'Sistema',
                                                               'Mis Favoritos',
                                                               'Mis Suscripciones' );
    {$else}
          {$ifdef ADUANAS}
          aClasifiReporte: array[ eClasifiReporte ] of ZetaPChar = ( 'Facturas/Pedimentos',
                                                                     'Productos',
                                                                     'Cat�logos',
                                                                     'Sistema',
                                                                     'Mis Favoritos',
                                                                     'Mis Suscripciones' );
          {$else}
                {$ifdef SELECCION}
                aClasifiReporte: array[ eClasifiReporte ] of ZetaPChar = ( 'Selecci�n',
                                                                           'Mis Favoritos',
                                                                           'Mis Suscripciones' );
                {$else}
                       {$ifdef VISITANTES}
                       aClasifiReporte: array[ eClasifiReporte ] of ZetaPChar = ( 'Visitantes',
                                                                                  'Mis Favoritos',
                                                                                  'Mis Suscripciones' );
                       {$else}

                             {$ifndef RDD}
                             aClasifiReporte: array[ eClasifiReporte ] of ZetaPChar = ( 'Empleados',
                                                                                  'Asistencia',
                                                                                  'N�minas',
                                                                                  'Pagos IMSS',
                                                                                  'Consultas',
                                                                                  'Cat�logos',
                                                                                  'Tablas',
                                                                                  'Supervisores',
                                                                                  'Cafeter�a',
                                                                                  'Migraci�n',
                                                                                  'Cursos',
                                                                                  'Labor',
                                                                                  'Servicio M�dico',
                                                                                  'Mis Favoritos',
                                                                                  'Plan Carrera',
                                                                                  'Kiosco',
                                                                                  'Caseta',
                                                                                  'Evaluaci�n Desempe�o',
                                                                                  'Caja de Ahorro',
                                                                                  'Mis Suscripciones',
                                                                                  'Env�o Email Empleados'
                                                                                  );

                             {$endif}

                       {$endif}
                {$endif}
          {$endif}
    {$endif}

    aTipobanda: array[ eTipoBanda ] of ZetaPChar = ( 'Encabezado', 'Detalle', 'Pie' );
    aCandado: array[ eCandado ] of ZetaPChar = ( 'Imprimir y Cambiar', 'S�lo Imprimir', 'Ni Imprimir Ni Cambiar' );
    //    //CAMPOREPORTE
    aTipoCampo: array[ eTipoCampo ] of ZetaPChar =('Campos','Grupos','Orden','Encabezado','Pie de Grupo','Filtro', 'Par�metro');
    aTipoOperacionCampo: array[ eTipoOperacionCampo ] of ZetaPChar = ( 'Ninguno','Cuantos','Suma','Promedio','Min','Max','Autom�tico','Sobre Totales','Repite Ultimo');
    aTipoFormato: array[ eTipoFormato ] of ZetaPChar= ( 'Impresora',
                                                    'ASCII Longitud Fija (SDF)',
                                                    'ASCII Delimitado',
                                                    'HTML (P�ginas Internet)',
                                                    'CSV  (Hoja de C�lculo)',
                                                    'QRP  (Tipo Preliminar)',
                                                    'TXT  (Archivos Texto)',
                                                    'XLSX  (Excel con Presentaci�n)',
                                                    'RTF  (Texto Enriquecido)',
                                                    'WMF  (Archivos MetaData)',
                                                    'Mail Merge - Microsoft Word',
                                                    'PDF  (Documento de Acrobat)',
                                                    'BMP  (Imagen - Bitmap)',
                                                    'TIF  (Imagen - Tagged Image File)',
                                                    'PNG  (Imagen - Portable Network Graphics)',
                                                    'JPEG (Imagen - JPEG File Interchange Format)',
                                                    'EMF  (Imagen - Enhanced Metafiles)',
                                                    'SVG  (Scalable Vector Graphics)',
                                                    'HTM  (Extended HyperText Markup Language)',
                                                    'WB1  (Quattro Pro para Windows)',
                                                    'WK2  (Lotus 1-2-3)',
                                                    'DIF  (Excel - Data Interchange Format)',
                                                    'SLK  (Excel - Symbolic Link)',
                                                    'XLS  (Excel - Hoja de C�lculo)',
                                                    'Mail Merge - Microsoft Excel'
                                                     );
    aFormatoFormas: array[ eFormatoFormas ] of ZetaPChar= ( 'Impresora',
                                                      'HTML (P�ginas Internet)',
                                                      'QRP (Tipo Preliminar)',
                                                      'TXT (Archivos Texto)',
                                                      'XLSX (Hojas de C�lculo de Excel)',
                                                      'RTF (Texto Enriquecido)',
                                                      'WMF (Archivos MetaData)',
                                                      'PDF (Documento de Acrobat)',
                                                      'BMP  (Imagen - Bitmap)',
                                                      'TIF  (Imagen - Tagged Image File)',
                                                      'PNG  (Imagen - Portable Network Graphics)',
                                                      'JPEG (Imagen - JPEG File Interchange Format)',
                                                      'EMF  (Imagen - Enhanced Metafiles)',
                                                      'SVG  (Scalable Vector Graphics)',
                                                      'HTM  (Extended HyperText Markup Language)',
                                                      'WB1  (Quattro Pro para Windows)',
                                                      'WK2  (Lotus 1-2-3)',
                                                      'DIF  (Excel - Data Interchange Format)',
                                                      'SLK  (Excel - Symbolic Link)'
                                                      );
    aFormatoASCII: array[ eFormatoASCII ] of ZetaPChar= ('ASCII Longitud Fija (SDF)', 'ASCII Delimitado'{$ifdef OSRAM_INTERFAZ},'ASCII Punto y Coma'{$endif} );
    aExtFormato: array[ eExtFormato ] of ZetaPChar= ( '',
                                                  'TXT',
                                                  'TXT',
                                                  'HTML',
                                                  'CSV',
                                                  'QRP',
                                                  'TXT',
                                                  'XLSX',
                                                  'RTF',
                                                  'WMF',
                                                  'TXT',
                                                  'PDF',
                                                  'BMP',
                                                  'TIF',
                                                  'PNG',
                                                  'JPG',
                                                  'EMF',
                                                  'SVG',
                                                  'HTM',
                                                  'WB1',
                                                  'WK2',
                                                  'DIF',
                                                  'SLK',
                                                  'XLS',
                                                  'XLS' );

    aJustificacion: array[ TAlignment ] of ZetaPChar =('Justificaci�n a la Izquierda','Justificaci�n a la Derecha','Justificaci�n Centrado');
    //    //EVENTO
    aIncluyeEvento: array[ eIncluyeEvento ] of ZetaPChar =('Empleados','Bajas','Empleados/Bajas');
    //    //GLOBAL
    aTipoGlobal: array[ eTipoGlobal ] of ZetaPChar =('L�gico','Numero con Decimales','Numero Entero','Fecha','Texto','Imagen', 'Autom�tico', 'Blob');
    //    //CONCEPTO
    aTipoConcepto: array[ eTipoConcepto ] of ZetaPChar =('Acumulado D�as/Horas','Percepci�n','Deducci�n','Obligaci�n Empresa','Prestaci�n','C�lculo Temporal','De Captura', 'Resultados');
    //    //FALTAS
    aMotivoFaltaDias: array[ eMotivoFaltaDias ] of ZetaPChar =( 'D�as Falta Injustificada',
                                                            'D�as Falta Justificada',
                                                            'D�as Permiso s/Goce',
                                                            'D�as Permiso c/Goce',
                                                            'D�as Suspensi�n',
                                                            'D�as Otros Permisos',
                                                            'D�as Vacaciones',
                                                            'D�as Incapacidad',
                                                            'D�as Aguinaldo',
                                                            'D�as Asistencia',
                                                            'D�as No Trabajados',
                                                            'D�as Retardo',
                                                            'D�as Ajuste',
                                                            'D�as IMSS',
                                                            'D�as E y M',
                                                            'D�as Prima Vacacional');
    aMotivoFaltaHoras: array[ eMotivoFaltaHoras ] of ZetaPChar =( 'Horas Ordinarias',
                                                              'Horas Extras',
                                                              'Horas Dobles',
                                                              'Horas Triples',
                                                              'Horas Retardo',
                                                              'Horas Domingo',
                                                              'Horas Adicionales',
                                                              'Horas Festivo Trab.',
                                                              'Horas Descanso Trab.',
                                                              'Horas Vacaciones Trab.',
                                                              'Horas Permiso c/Goce',
                                                              'Horas Permiso s/Goce');
    //AHORRO
    aStatusAhorro: array[ eStatusAhorro ] of ZetaPChar =('Activo','Cancelado');
    //    //PRESTAMO
    aStatusPrestamo: array[ eStatusPrestamo ] of ZetaPChar =('Activo','Cancelado','Saldado');
    //    //AUSENCIA
    aStatusAusencia: array[ eStatusAusencia ] of ZetaPChar =('H�bil','S�bado','Descanso');
    aTipoDiaAusencia: array[ eTipoDiaAusencia ] of ZetaPChar =('Normal','Incapacidad','Vacaciones','Permiso c/Goce','Permiso s/Goce','Falta Justificada','Suspensi�n','Otro Permiso','Festivo','No Trabajado');
    aAutorizaChecadas: array[ eAutorizaChecadas ] of ZetaPChar=('Permiso c/Goce','Permiso s/Goce','Horas Extras', 'Descanso Trabajado','Permiso c/Goce Entrada','Permiso s/Goce Entrada','Prepagadas fuera Jornada','Prepagadas dentro jornada');
    //    //CHECADAS
    aTipoChecadas: array[ eTipoChecadas ] of ZetaPChar =('Desconocido','Entrada','Salida','Inicio','Fin','Autoriza c/Goce','Autoriza s/Goce','Autoriza Extras', 'Autorizaci�n','Autoriza c/Goce Entrada','Autoriza s/Goce Entrada','Autoriza prepagadas F/Jornada', 'Autoriza prepagadas D/Jornada');
    aDescripcionChecadas: array[ eDescripcionChecadas ] of ZetaPChar =('Desconocida','Ordinaria','Retardo','Puntual','Extras','Comida','Descanso #1','Descanso #2','Descanso','Permiso Entrada');
    //    //PARIENTES
    aTipoPariente: array[ eTipoPariente ] of ZetaPChar =('Desconocido','Padre','Madre','Hijo (a)','Esposo (a)','Hermano (a)','Primo (a)','T�o (a)','Sobrino (a)','Abuelo (a)','Nieto (a)','Otros');
    //    //COLABORA
    aTipoInfonavit: array[ eTipoInfonavit ] of ZetaPChar =('No tiene','Porcentaje','Cuota Fija','Veces SMGDF','Veces UMA');
    //    //VACACION
    aTipoVacacion: array[ eTipoVacacion ] of ZetaPChar =('Cierre','Vacaciones');
    //    //KARDEX
    aStatusKardex: array[ eStatusKardex ] of ZetaPChar =('Capturado','Impreso','Exportado','Entregado','Recibido');
    //    //INCAPACIDAD
    aMotivoIncapacidad: array[ eMotivoIncapacidad ] of ZetaPChar =('Inicial','Subsecuente','Reca�da','Ninguno','�nica', 'Alta M�dica', 'Valuaci�n','Defunci�n','Prenatal','Postnatal','Enlace');
    aFinIncapacidad: array[ eFinIncapacidad ] of ZetaPChar =('No ha terminado','Alta m�dica','Incapacidad Permanente','Muerte','Reca�da', 'Valuaci�n Provisional', 'Valuaci�n Definitiva', 'Revaluaci�n Provisional', 'Revaluaci�n Definitiva');
    //    //PERMISOS
    aTipoPermiso: array[ eTipoPermiso ] of ZetaPChar =('Con Goce','Sin Goce','Falta Justificada','Suspensi�n','Otros');
    //    //LIQUIDACION IMSS
    aTipoLiqIMSS: array[ eTipoLiqIMSS ] of ZetaPChar =('Ordinaria','Extempor�nea','Complementaria');
    //    //LIQUIDACION (DETALLE DE MOVIMIENTOS)
    aTipoLiqMov: array[ eTipoLiqMov ] of ZetaPChar =('Mensual','Bimestral');
    aCodigoLiqMov: array[ eCodigoLiqMov ] of ZetaPChar =('M','B');
    //    //TIPO DE AHORRO
    aTipoAhorro: array[ eTipoAhorro ] of ZetaPChar =('No Saldar','Saldar Autom�ticamente', 'Saldar Preguntando' );
    aAltaAhorroPrestamo: array[ eAltaAhorroPrestamo ] of ZetaPChar=('No Agregar', 'Agregar sin Preguntar', 'Agregar Preguntando' );
    //    //GLOBALES
    aHorasExtras: array[ eHorasExtras ] of ZetaPChar =('No Pagar','Extras c/Tope Semanal','Extras c/Tope Diario','Descanso Trabajado','Descanso + Extras c/Tope Semanal','Descanso + Extras c/Tope Diario');
    aBancaElectronica: array[ eBancaElectronica ] of ZetaPChar =('No tiene','Bancomer','Bancomer SI-Empresarial');
    aYears: array[ eYears ] of ZetaPChar =( '1994',
                                        '1995',
                                        '1996',
                                        '1997',
                                        '1998',
                                        '1999',
                                        '2000',
                                        '2001',
                                        '2002',
                                        '2003',
                                        '2004',
                                        '2005',
                                        '2006',
                                        '2007',
                                        '2008',
                                        '2009',
                                        '2010',
                                        '2011',
                                        '2012',
                                        '2013',
                                        '2014',
                                        '2015',
                                        '2016',
                                        '2017',
                                        '2018',
                                        '2019',
                                        '2020',
                                        '2021',
                                        '2022',
                                        '2023',
                                        '2024' );
    aMeses: array[ eMeses ] of ZetaPChar = ( 'Enero',
                                        'Febrero',
                                        'Marzo',
                                        'Abril',
                                        'Mayo',
                                        'Junio',
                                        'Julio',
                                        'Agosto',
                                        'Septiembre',
                                        'Octubre',
                                        'Noviembre',
                                        'Diciembre' );
    aZonaGeografica: array[ eZonaGeografica ] of ZetaPChar= ('A','B','C');
    aSexo: array[ eSexo ] of ZetaPChar= ('M','F');
    aDesSexo: array[ eSexo ] of ZetaPChar= ('Masculino','Femenino');
    aDiasSemana: array[ eDiasSemana ] of ZetaPChar= ( 'Domingo',
                                                  'Lunes',
                                                  'Martes',
                                                  'Mi�rcoles',
                                                  'Jueves',
                                                  'Viernes',
                                                  'S�bado' );
    aMes13: array[ eMes13 ] of ZetaPChar = ('Enero',
                                        'Febrero',
                                        'Marzo',
                                        'Abril',
                                        'Mayo',
                                        'Junio',
                                        'Julio',
                                        'Agosto',
                                        'Septiembre',
                                        'Octubre',
                                        'Noviembre',
                                        'Diciembre',
                                        'Mes 13' );
    aStatusEmpleado: array[ eStatusEmpleado ] of ZetaPChar = ( 'Antes Ingreso',
                                                           'Empleado Activo',
                                                           'Vacaciones',
                                                           'Incapacidad',
                                                           'Permiso c/Goce',
                                                           'Permiso s/Goce',
                                                           'Falta Justificada',
                                                           'Suspensi�n',
                                                           'Permiso Otros',
                                                           'Baja' );
    aStatusEmpleadoCedula: array[ eStatusEmpleadoCedula ] of ZetaPChar = ( 'Antes Ingreso',
                                                           'Activo',
                                                           'Vacaciones',
                                                           'Incapacidad',
                                                           'Permiso c/Goce',
                                                           'Permiso s/Goce',
                                                           'Falta Justificada',
                                                           'Suspensi�n',
                                                           'Permiso Otros',
                                                           'Baja' );
    aOperacionConflicto: array[ eOperacionConflicto ] of ZetaPChar = ( 'Reportar Error',
                                                                    'Dejar Anterior',
                                                                    'Sumar',
                                                                    'Sustituir' );
    aStatusLectura: array[ eStatusLectura ] of ZetaPChar = ( 'Completo',
                                                          'Incompleto');
    aTipoLectura: array[ eTipoLectura ] of ZetaPChar = ( 'Producci�n',
                                                      'Fija',
                                                      'Tiempo Muerto',
                                                      'Pre-Calculada',
                                                      'Break' );

    aTipoOpera: array[eTipoOpera] of ZetaPChar = ( 'Directa',
                                                'Indirecta',
                                                'Excepcion',
                                                'Entrada',
                                                'Salida',
                                                'Comida',
                                                'Permiso S/Goce');

    aWorkStatus: array[eWorkStatus] of ZetaPChar = ( 'En Proceso',
                                                 'Cerrado',
                                                 'Cancelado',
                                                 'Suspendido');

    aTipoCedula: array[eTipoCedula] of ZetaPChar = ( 'Operaciones', 'Horas Especiales', 'Tiempo Muerto' );

    aRangoFechas: array[eRangoFechas] of ZetaPChar = ( 'Hoy',
                                                       {$ifndef ADUANAS}
                                                       'N�mina Activa',
                                                       {$ifdef QUINCENALES}
                                                       'Asistencia Activa',
                                                       {$endif}
                                                       {$endif}
                                                       'Semana Actual',
                                                       'Quincena Actual',
                                                       'Mes Actual',
                                                       'Bimestre Actual',
                                                       'A�o Actual',
                                                       'Ayer',
                                                       'Lunes Pasado',
                                                       'Semana Pasada',
                                                       'Quincena Pasada',
                                                       'Mes Pasado',
                                                       'Bimestre Pasado',
                                                       'A�o Pasado' );
    aCampoCalendario: array[ eCampoCalendario ] of ZetaPChar = ( 'Horas Ordinarias',
                                                             'Horas Extras',
                                                             'Horas Dobles',
                                                             'Horas Triples',
                                                             'Tardes',
                                                             'Descanso Trabajado',
                                                             'Permiso Con Goce',
                                                             'Permiso Sin Goce',
                                                             'Status',
                                                             'Tipo D�a',
                                                             'Incidencia');
    aCafeCalendario: array[ eCafeCalendario ] of ZetaPChar = ( 'Tipo de Comida #1',
                                                           'Tipo de Comida #2',
                                                           'Tipo de Comida #3',
                                                           'Tipo de Comida #4',
                                                           'Tipo de Comida #5',
                                                           'Tipo de Comida #6',
                                                           'Tipo de Comida #7',
                                                           'Tipo de Comida #8',
                                                           'Tipo de Comida #9',
                                                           'Permitir Acceso',
                                                           'Prohibir Acceso',
                                                           'Activar Validaci�n',
                                                           'DesActivar Validaci�n' );
    aMotTools: array[ eMotTools ] of ZetaPChar = ( 'NO Descontar',
                                               'Valor Reposici�n',
                                               'Valor en BAJA' );
    aDescuentoTools: array[ eDescuentoTools ] of ZetaPChar = ( 'Cantidad Fija',
                                                           'Cantidad Fija - NO Descontar al Vencer Vida Util',
                                                           'Cantidad Proporcional a Vida Util',
                                                           'Cantidad Proporcional a Vida Util Restante' );
    aEmailFormato: array[ eEmailFormato ] of ZetaPChar = ( 'Formato HTML', 'Formato HTML Adjunto' );
    aEmailFrecuencia: array[ eEMailFrecuencia ] of ZetaPChar = ('Diario', 'Semanal', 'Mensual', 'Por Hora', 'Especial' );
    aEmailNotificacion: array[ eEmailNotificacion ] of ZetaPChar = ('Enviar Notificaci�n', 'NO Enviar Notificaci�n' );
    aEmailType: array[ eEmailType ] of ZetaPChar = ( 'Texto', 'HTML', 'Texto Error' );
    aCamposConteo: array[ eCamposConteo ] of ZetaPChar = ( '',
                                                       'CB_PUESTO',
                                                       'CB_TURNO',
                                                       'CB_CLASIFI',
                                                       'CB_NIVEL0',
                                                       'CB_NIVEL1',
                                                       'CB_NIVEL2',
                                                       'CB_NIVEL3',
                                                       'CB_NIVEL4',
                                                       'CB_NIVEL5',
                                                       'CB_NIVEL6',
                                                       'CB_NIVEL7',
                                                       'CB_NIVEL8',
                                                       'CB_NIVEL9'{$ifdef ACS},'CB_NIVEL10', 'CB_NIVEL11', 'CB_NIVEL12'{$endif} ); 

    aPuertoSerial: array[ ePuertoSerial ] of ZetaPChar = ( 'Sin Definir', 'Com 1', 'Com 2', 'Com 3', 'Com 4' );
    aRegla3x3: array[ eRegla3x3 ] of ZetaPChar = ( 'Dobles y Triples (Proporcional)',
                                               'Dobles y Triples (Cuantas)',
                                               'S�lo Dobles' );
    aTipoCompany: array[ eTipoCompany ] of ZetaPChar = ( '3DATOS', '3RECLUTA',  '3VISITA', '3OTRO', '3PRUEBA', '3PRESUP' );
    aTipoCompanyName: array[ eTipoCompany ] of ZetaPChar = ( 'Tress', 'Selecci�n', 'Visitantes', 'Otro', 'Tress Prueba', 'Presupuesto' );
    aPrioridad: array[ePrioridad] of ZetaPChar = ( 'Alta', 'Normal', 'Baja' );
    aReqStatus: array[eReqStatus] of ZetaPChar = ( 'Pendiente', 'Cubierta', 'Cancelada' );
    aMotivoVacante: array[eMotivoVacante] of ZetaPChar = ( 'Rotaci�n', 'Plaza Nueva', 'Temporal', 'Reemplazo', 'Otro' );
    aEstudios: array[eEstudios] of ZetaPChar = ( 'Ninguno', 'Primaria', 'Secundaria', 'Bachillerato', 'T�cnico', 'Profesional Pasante', 'Profesional Titulado', 'Maestr�a', 'Doctorado','Especialidad' );
    aSolStatus: array[eSolStatus] of ZetaPChar = ( 'Disponible', 'En Tr�mite', 'No Disponible', 'No Contratable', 'Contratado' );{OP:18.Abr.08}
    aEdoCivil: array[eEdoCivil] of ZetaPChar = ( 'S', 'C', 'D', 'U', 'V' );
    aEdoCivilDesc: array[eEdoCivil] of ZetaPChar = ( 'Soltero', 'Casado', 'Divorciado', 'Uni�n Libre', 'Viudo' );
    aCanStatus: array[eCanStatus] of ZetaPChar = ( 'Seleccionado', 'Por Contratar',
                                               'Rechazado', 'Contratado', 'No Acept�' );
    aEntStatus: array[eEntStatus] of ZetaPChar = ( 'Contratar', 'Evaluar Otros', 'Rechazar' );
    aTipoExamen: array[eTipoExamen] of ZetaPChar = ( 'M�dico', 'Psicom�trico', 'T�cnico', 'Otro', 'Vista', 'Habilidad', 'Antidoping', 'Ingl�s' );{OP:15.Abr.08}
    //MA: Descripciones de los Numerados de Servicio Medico
    aTipoConsulta: array[eTipoConsulta] of ZetaPChar = ( 'General', 'Accidente', 'Embarazo' );
    aTipoEstudio:  array[eTipoEstudio]  of ZetaPChar = ( 'Antiestrol','Calcemia','Citol�gico','Colesterol','Glucemia','Hematrocito','Orina','Uremia' );
    aTipoAccidente:array[eTipoAccidente]of ZetaPChar = ( 'Lesi�n Personal','Da�o a la Propiedad','Otro' );
    aMotivoAcc:    array[eMotivoAcc]    of ZetaPChar = ( 'Factor Personal', 'Factor Laboral', 'Medio Ambiente', 'Otro');
    aTipoMedicina: array[eTipoMedicina] of ZetaPChar = ( 'Antibiotico', 'Antigripal', 'Otros..');
    aTipoDiagnost: array[eTipoDiagnost] of ZetaPChar = ( 'Accidentologia e Intoxicaciones', 'Alteraci�n de los liquidos corporales','Alteracion del medio interno','Antecedentes Perinatales','Antecedentes Psicologicos Fliares.', 'Otro' );
    aTipoLesion:   array[eTipoLesion]   of ZetaPChar = ( 'En la Empresa', 'En una Comisi�n', 'En Trayecto a su Trabajo','En Trayecto a su Domicilio','Trabajando Tiempo Extra' );
    aAxAtendio:    array[eAxAtendio]    of ZetaPChar = ( 'Servicio M�dico','IMSS','Otro' );
    aAxTipo:       array[eAxTipo]       of ZetaPChar = ( 'Accidente','Enfermedad','Otro' );
    aExCampoTipo:  array[eExCampoTipo]  of ZetaPChar = ( 'Texto', 'N�mero', 'Si/No', 'Si/No/Comentario', 'Fecha', 'Tabla','Memo', 'Combo', 'Lista' );
    aExMostrar:    array[eExMostrar]    of ZetaPChar = ( 'Siempre', 'Nunca', 'Si est� Lleno', 'Si es Vac�o' );
    aExTipo:       array[eExTipo]       of ZetaPChar = ( 'Empleado', 'Candidato', 'Pariente', 'Otro' );
    aEmTermino:    array[eEmTermino]    of ZetaPChar = ( 'Normal', 'Prematuro', 'Aborto' );
    //MV: Descripcion del enumerado para Turnos en Labor
    aTurnoLabor:   array[eTurnoLabor]   of ZetaPChar = ( 'Otros', 'Matutino', 'Vespertino', 'Nocturno Entrada', 'Nocturno Salida' );
    aTipoArea:     array[eTipoArea]   of ZetaPChar = ( 'Directo', 'Indirecto', 'Administrativo' );
    aTipoKarFecha: array[eTipoKarFecha]   of ZetaPChar = ( 'Fecha que aparece en Kardex', 'Fecha en que se Captur�' );
    aCancelaMod:   array[eCancelaMod]   of ZetaPChar = ( 'Globales', 'Manuales', 'Todas' );
    aInicioAmortizacion: array[eInicioAmortizacion]   of ZetaPChar = ( 'Usar Inicio del Siguiente Bimestre', 'Usar Inicio del Siguiente Mes', 'Usar Fecha De Inicio Del Cr�dito' );
    //aInicioAmortizacion: array[eInicioAmortizacion]   of ZetaPChar = ( 'Usar Inicio del Siguiente Bimestre', 'Usar Inicio del Siguiente Mes', 'Usar Fecha De Inicio Del Cr�dito' );
    aClaseAcciones: array[eClaseAcciones] of ZetaPChar = ( 'Curso', 'Material Did�ctico', 'Actividad', 'Otra' );
    aTipoCompete: array[eTipoCompete] of ZetaPChar = ( 'Habilidad', 'Competencia', 'Otra' );
    aStatusAcciones: array[eStatusAcciones] of ZetaPChar = ( 'Asignado', 'Pendiente', 'Cumplido' );
    aStatusCursos: array[eStatusCursos] of ZetaPChar = ( 'Programado', 'Pendiente', 'Tomado' );
    aTipoCedInspeccion: Array[eTipoCedInspeccion] of ZetaPChar = ( 'En Proceso', 'Final', 'Otro' );
    aResultadoCedInspeccion: Array[eResultadoCedInspeccion] of ZetaPChar = ( 'Aceptado', 'Rechazado', 'Suspendido', 'Otro' ) ;
    aValorDiasVacaciones: Array[ eValorDiasVacaciones ] of ZetaPChar = ( 'No usar', 'Usar sin redondeo', 'Redondear hacia arriba', 'Redondear hacia abajo' );
    aFiltroEmpOrganigrama: Array[ eFiltroEmpOrganigrama ] of ZetaPChar = ( 'Titulares', 'Jefe', 'Subordinados' );
    aTipoPuesto: Array[ eTipoPuesto ] of ZetaPChar = ( 'Empleado', 'Directivo', 'Gerente', 'Asistente', 'Externo', 'Otro' );
    {DESCRIPCIONES CUANDO SE MODIFICA}
    aUpdateKind: array[ TUpdateKind ] of ZetaPChar = ( 'Modificar', 'Agregar', 'Borrar' );
    aOperacionIncremento: array[ eOperacionIncremento ] of ZetaPChar = ( 'Sumar', 'Restar' );
    aFormatoImpFecha: array[ eFormatoImpFecha ] of ZetaPChar = ( 'dd/mm/yy', 'dd/mm/yyyy', 'ddmmyy', 'ddmmyyyy',
                                                             'mm/dd/yy', 'mm/dd/yyyy', 'mmddyy', 'mmddyyyy',
                                                             'yy/mm/dd', 'yyyy/mm/dd', 'yymmdd', 'yyyymmdd' );
    aOut2Eat: array[ eOut2Eat ] of ZetaPChar = ( 'Determinado por el Sistema', 'S�', 'No' );
    aEmpleadoRepetido: array[ eEmpleadoRepetido ] of ZetaPChar = ( 'Sumar al Ultimo', 'Sumar al Primero', 'Incluir el Ultimo', 'Incluir el Primero', 'No Incluir', 'Incluir Todos' );
    aTipoCorregirFechas: array[ eTipoCorregirFechas ] of ZetaPChar = ( 'Comidas e Invitaciones','�nicamente Comidas','�nicamente Invitaciones' );
    aTipoNavegador : array[ eTipoNavegador ] of ZetaPChar =( 'Ajustado a la Pantalla', 'Ajustado a la Pagina' );
    aOrientacionNavegador : array[ eOrientacionNavegador ] of ZetaPChar =(  'Horizontal', 'Vertical' );
    aPosicionNavegador : array[ ePosicionNavegador ] of ZetaPChar =( 'Arriba a la Izquierda',
                                                                 'Arriba al Centro',
                                                                 'Arriba a la Derecha',
                                                                 'Centro a la Izquierda',
                                                                 'Centro',
                                                                 'Centro a la Derecha',
                                                                 'Abajo a la Izquierda',
                                                                 'Abajo al Centro',
                                                                 'Abajo a la Derecha' );

    aCompressionLevel : array[ eCompressionLevel ] of ZetaPChar = ( 'M�s R�pido', 'Normal', 'M�xima Compresi�n' );
    aEncryptionLevel : array[ eEncryptionLevel ] of ZetaPChar =  ( '40 bits', '128 bits' );
    aFontEncoding : array[ eFontEncoding  ] of ZetaPChar = ('WinAnsiEncoding', 'MacRomanEncoding', 'MacExpertEncoding', 'StandardEncoding', 'PDFDocEncoding' );
    aPageLayout : array[ ePageLayout  ] of ZetaPChar = ( 'Una Sola P�gina', 'Una Columna', 'Dos Columnas a la Izquierda', 'Dos Columnas a la Derecha');
    aPageMode : array[ ePageMode  ] of ZetaPChar = ('Ninguno', 'Usar Contornos (Outlines)', 'Usar Vista Preliminar (Thumbs)', 'Pantalla Completa');
    aTransitionEffect : array[ eTransitionEffect  ] of ZetaPChar = ( 'Ninguno',
                                                                 'Partir Horizontalmente Desde Adentro',
                                                                 'Partir Horizontalmente Desde Afuera',
                                                                 'Partir Verticalmente Desde Adentro',
                                                                 'Partir Verticalmente Desde Afuera',
                                                                 'Persianas Horizontales',
                                                                 'Persianas Verticales',
                                                                 'Rect�ngulo Desde Adentro',
                                                                 'Rect�ngulo Desde Afuera',
                                                                 'Limpiar de Izquierda a Derecha',
                                                                 'Limpiar de Abajo hacia Arriba',
                                                                 'Limpiar de Derecha a Izquierda',
                                                                 'Limpiar de Arriba hacia Abajo',
                                                                 'Disolver',
                                                                 'Brillo de Izquierda a Derecha',
                                                                 'Brillo de Arriba hacia Abajo',
                                                                 'Brillo En Diagonal' );
    aAnfitrionStatus: array[ eAnfitrionStatus ] of ZetaPChar = ( 'Activo', 'Suspendido' );
    aVisitanteStatus: array[ eVisitanteStatus ] of ZetaPChar = ( 'Activo', 'Suspendido' );
    aCasetaStatus: array[ eCasetaStatus ] of ZetaPChar = ( 'Activa', 'Cancelada' );
    aCitaStatus: array[ eCitaStatus ] of ZetaPChar = ( 'Si', 'No', 'Cancelada' );
    aCatCompanys: array[ eCatCompanys ] of ZetaPChar = ( 'Activo', 'Suspendido' );
    aLibroStatus: array[ eLibroStatus ] of ZetaPChar = ( 'Dentro De La Empresa', 'Fuera De La Empresa' );
    aTipoGafete: array[ eTipoGafete ] of ZetaPChar = ( 'C�digo De Barra', 'Proximidad'{$ifndef DOS_CAPAS}, 'Biom�trico'{$endif} );
    aChecasSimultaneas: array [ eChecadasSimultaneas ] of ZetaPChar = ( 'Agregar Preguntando', 'Agregar Sin Preguntar', 'No Agregar' );
    aTipoProximidad: array[ eTipoProximidad ] of ZetaPChar = ( 'Wiegand', 'Serial', 'Clock & Data' );
    aCorteStatus: array[ eCorteStatus ] of ZetaPChar = ( 'Abierto', 'Cerrado' );
    aAccesoRegla: array[ eAccesoRegla ] of ZetaPChar = ( 'Informativo', 'Evaluar Al Entrar', 'Evaluar Al Salir', 'Evaluar Siempre' );
    aBusquedaLibro: array[ eBusquedaLibro ] of ZetaPChar  = ( 'Nombre', 'Empresa', 'Visita a', 'Departamento', 'Placas', 'Gafete' );
    aImpresionGafete: array[ eImpresionGafete ] of ZetaPChar = ( 'No Imprimir', 'Imprimir Preguntando', 'Imprimir Sin Preguntar' );
     {$ifdef COMPARTE_MGR}
    aWFStatusModelo: array[ eWFStatusModelo ] of ZetaPChar = ( 'Activo', 'Suspendido', 'Cancelado' );
    aWFNotificacion: array[ eWFNotificacion ] of ZetaPChar = ( 'Ninguno', 'Rol', 'Usuario Espec�fico', 'Jefe', 'Jefe Del Jefe', 'Subordinados Directos', 'Nivel', 'Solicitante', 'Participantes', 'Responsable' );
    aWFProcesoStatus: array[ eWFProcesoStatus ] of ZetaPChar = ( 'Inactivo', 'Activo', 'Terminado', 'Cancelado', 'Suspendido' );
    aWFTareaStatus: array[ eWFTareaStatus ] of ZetaPChar = ( 'Inactiva', 'Activa', 'Esperando', 'Terminada', 'Cancelada' );
    aWFNotificacionCuando: array[ eWFNotificacionCuando ] of ZetaPChar = ( 'Iniciar', 'Terminar', 'Cancelar', 'Modificar datos', 'Despues de iniciar', 'Antes del l�mite', 'Despu�s del l�mite');
    aWFNotificacionQue: array[ eWFNotificacionQue ] of ZetaPChar = ( 'Notificar', 'Autorizar', 'Lanzar proceso');
    aWFAutenticacionWebService: array [ eWFAutenticacionWebService ] of ZetaPChar = ( 'Default', 'An�nimo', 'Usuario y clave');
    aWFCodificacionWebService: array [ eWFCodificacionWebService ] of ZetaPChar = ( 'Documento', 'RPC' );
    {$endif}
    {$ifdef ADUANAS}
    aTipoCliente: array[ eTipoCliente ] of ZetaPChar = ( 'Cliente', 'Proveedor', 'Cliente y Proveedor' );
    aTipoMaterial: array[ eTipoMaterial ] of ZetaPChar = ( 'Materia prima', 'Producto terminado', 'Activo fijo', 'Otros' );
    aTipoMedida: array[ eTipoMedida ] of ZetaPChar = ( 'Piezas', 'Longitud', 'Area', 'Vol�men', 'Peso', 'Otro' );
    aStatusFactura: array[ eStatusFactura ] of ZetaPChar = ( 'Normal', 'Ligado', 'Afectado', 'Descargado', 'Rectificado', 'Bloqueado' );
    aFacTipoProceso: array[ eFacTipoProceso ] of ZetaPChar = ( 'Industrial', 'De servicio' );
    aTipoActivo: array[ eTipoActivo ] of ZetaPChar = ( 'Maquinaria y equipo', 'Herramienta' );
    aStatusPedimento: array[ eStatusPedimento ] of ZetaPChar = ( 'Normal', 'Ligado', 'Afectado', 'Descargado', 'Rectificado', 'Bloqueado' );
    aProgramasTipoProceso: array[ eProgramasTipoProceso ] of ZetaPChar = ( 'Industrial', 'De servicio', 'Ambos' );
    aDescargaAlmacen: array[ eDescargaAlmacen ] of ZetaPChar = ( 'Material', 'Desperdicio' );
    aDescargaTipo: array[ eDescargaTipo ]of ZetaPChar = ( 'Saldo Inicial','Exportar Material','Desperdicio Producido','Merma Generada','Desperdicio Agregado','Despercicio Exportado' );
    {$endif}
    //***** Evaluaci�n de Desempe�o ********//
    aStatusEncuesta: array[ eStatusEncuesta ] of ZetaPChar = ( 'En dise�o', 'Abierta', 'Cerrada' );
    aComentariosMGR: array[ eComentariosMGR ] of ZetaPChar = ( 'Default', 'No', 'Si' );
    aComentariosPedir: array[ eComentariosPedir ] of ZetaPChar = ( 'No', 'Opcional', 'Obligatorio', 'Solo en el valor m�nimo', 'Solo en el valor m�ximo', 'Solo en los extremos' );
    aEstiloRespuesta: array[ eEstiloRespuesta ] of ZetaPChar = ( 'Lista', 'Bot�n Radio', 'Texto Libre', 'Calculada Lista', 'Calculada Radio' );
    aTipoEvaluaEnc: array[ eTipoEvaluaEnc ] of ZetaPChar = ( 'Mismo', 'Jefe', 'Subordinado', 'Colega', 'Cliente', 'Otros' );
    aStatusEvaluaciones: array[ eStatusEvaluaciones ] of ZetaPChar = ( 'No han terminado', 'Algunas nuevas', 'Algunas en proceso', 'Algunas terminadas', 'Todas nuevas', 'Todas en proceso', 'Todas terminadas' );
    aStatusSujeto: array[ eStatusSujeto ] of ZetaPChar = ( 'Nuevo', 'Listo', 'Preparado', 'En Proceso', 'Terminado' );
    aStatusInscrito : array[ eStatusInscrito ] of ZetaPChar = ( 'Activo', 'Baja', 'En Espera', 'Aprobado' );
    aStatusSesiones : array[ eStatusSesiones ] of ZetaPChar = ( 'Abierto', 'Cerrado' );
    aTipoReserva: array[ eTipoReserva ] of ZetaPChar = ( 'Bloqueo', 'Sesi�n' );
    aStatusEvaluacion: array[ eStatusEvaluacion ] of ZetaPChar = ( 'Nueva', 'Lista', 'Preparada', 'En proceso', 'Terminada' );
    aEVCorreoStatus: array[ eEVCorreoStatus ] of ZetaPChar = ( 'Sin Enviar', 'Enviado', 'Error Al Enviar' );
    aTipoSupuestoRH: array[ eTipoSupuestoRH ] of ZetaPChar = ( 'Contrataciones', 'Cambios y Recortes' );
    aTipoPeriodoMes: array[ eTipoPeriodoMes ] of ZetaPChar = ( 'Primer periodo del mes', 'Ultimo periodo del mes' );
    aComoCalificar: array[ eComoCalificar ] of ZetaPChar = ( 'No Usar', 'Suma', 'Promedio' );
    aStatusAcuerdo: array[ eStatusAcuerdo ] of ZetaPChar = ( 'No se ha preguntado', 'Si', 'No' );

    aDPEscolar: array[ eDPEscolar ] of ZetaPChar = ( 'Ninguna', 'Bachillerato/T�cnico', 'Profesional parcial', 'Profesional', 'Maestr�a', 'Postgrado' );
    aDPNivel: array[ eDPNivel ] of ZetaPChar = ( 'Ninguno', 'Conocimientos b�sicos', 'Conocimientos t�cnicos', 'Comunicarse', 'Dominio' );
    aDPExpPuesto: array[ eDPExpPuesto ] of ZetaPChar = ( 'Ninguna', '1 a�o', '2 a 3 a�os', '4 a 6 a�os', '7 a 9 a�os', '10 a�os o m�s' );
    aJITipoPlaza: array[ eJITipoPlaza ] of ZetaPChar = ( 'Planta', 'Eventual' );
    aJIPlazaReporta: array[ eJIPlazaReporta ] of ZetaPChar = ( 'Linea', 'Staff' );
    aKJustificacion: array[ eKJustificacion ] of ZetaPChar = ('Izquierda','Derecha');
    aAccionBoton: array[ eAccionBoton ] of ZetaPChar = ( 'Contenido', 'Pantalla', 'Regresar', 'Mis Datos', 'Otro', 'Reporte', 'Cambio de NIP' );
    aLugarBoton: array[ eLugarBoton ] of ZetaPchar = ('Default', 'Espaciado', 'Abajo');
    aPosicionIcono: array[ ePosicionIcono ] of ZetaPChar = ( 'Izquierda', 'Derecha', 'Arriba', 'Abajo' );
    aTipoDiasBase: array[ eTipoDiasBase ] of ZetaPChar = ( 'D�as fijos', 'D�as del periodo', 'D�as del turno' );
    aBorrarMovimSUA: array[ eBorrarMovimSUA ] of ZetaPChar = ( 'No', 'Todo', 'Solo lo del mes' );
    aStatusPlanVacacion: array[ eStatusPlanVacacion ] of ZetaPChar = ( 'Pendiente', 'Autorizada', 'Rechazada', 'Procesada' );
    aPagoPlanVacacion: array[ ePagoPlanVacacion ] of ZetaPChar = ( 'Mismos d�as de gozo', 'Dejar en cero' );
    aStatusPoliza: array[ eStatusPoliza ] of ZetaPChar = ( 'Normal', 'Con Error' );
    //CAJA de AHORRO
    aTiposdePrestamo: array[ eTiposdePrestamo ] of ZetaPChar =('Pagos Iguales','Pagos Diferidos');
    aControles: array[ eControles ] of ZetaPChar = ( 'Texto corto', 'Texto largo', 'N�mero','Fecha', 'Combo', 'Lista'  );
    aStatusFestivo: array[eStatusFestivo] of ZetaPChar = ('Autom�tico', 'Festivo', 'No es festivo');
    aTratamientoExtras: array[eTratamientoExtras] of ZetaPChar = ('Triples basadas en turnos/horarios','Regla 3X3');
    aDiasAplicaExento: array[eDiasAplicaExento] of ZetaPChar = ( 'Todos', 'H�biles','H�biles sin festivos' );
    aRolGuardias: array[eRolGuardias] of ZetaPChar = ( 'A', 'B', 'C', 'D' );
    aStatusPlaza: array[eStatusPlaza] of ZetaPChar = ( 'Ocupada', 'Vacante' );
    aStatusGuardia: array[eStatusGuardia] of ZetaPChar = ( 'Activo', 'Cancelado' );
    aClaseKarPlaza: array[eClaseKarPlaza] of ZetaPChar = ( 'Registro', 'Cancelaci�n' );
    aTipoKarPlaza: array[eTipoKarPlaza] of ZetaPChar = ( 'Permanente', 'Temporal' );
    aEstadosGuardia: array[eEstadosGuardia] of ZetaPChar = ( 'Programado', 'Liberado', 'Reservado', 'En proceso', 'En Tr�nsito', 'Arrib�', 'Cancelado' );
    aRitmosGuardia: array[eRitmosGuardia] of ZetaPChar = ( '14 x 14', '28 x 14', '28 x 28', '6 x 1' );

    aTipoABordoGuardia: array[eTipoABordoGuardia] of ZetaPChar = ( 'A bordo', 'En tierra');
    aTipoDiaGuardia: array[eTipoDiaGuardia] of ZetaPChar = ( 'Descanso', 'Trabajo');
    aTipoPagoGuardia: array[eTipoPagoGuardia] of ZetaPChar = ( 'No aplica','Normal', 'Descanso','Compensaci�n','Adicional','Horas Extras');
    aTipoEventoGuardia: array[eTipoEventoGuardia] of ZetaPChar = ('Liberado a log�stica', 'Embarc�', 'Desembarc�', 'Ausente', 'Cubre temporalmente', 'Permanece a bordo', 'Permanece en tierra', 'Permanece a cubrir', 'Compensaci�n', 'Horas Extras', 'Cambio de plaza a bordo', 'Cambio de pernocta');
    aClaseEventoGuardia: array[eClaseEventoGuardia] of ZetaPChar = ( 'Registro', 'Cancelaci�n', 'Cancelado' );
    aStatusAusenciaGuardia: array[ eStatusAusenciaGuardia ] of ZetaPChar =('Trabaja','S�bado','Descansa');

    aTipoServicioLogistica: array[ eTipoServicioLogistica ] of ZetaPChar =('Transporte','Hospedaje');
    aViaLogistica: array[ eViaLogistica ] of ZetaPChar =('Mar�tima','Helic�ptero','Terrestre','Avi�n');
    aSentidoLogistica: array[ eSentidoLogistica ] of ZetaPChar =('Embarque','Desembarque','Domicilio','Retorno','Interplataformas');
    aTipoPuertoLogistica: array[ eTipoPuertoLogistica ] of ZetaPChar =('Mar�timos','Terrestres','Aeropuertos');
    aStatusViaje: array[ eStatusViaje ] of ZetaPChar =('Programado','Sali�','Lleg�','Cancelado');
    aStatusTarjeta: array[ eStatusTarjeta ] of ZetaPChar =('Nueva','Pendiente','Proceso','Relacionada','Rechazada','Cancelada');
    aTipoTarjeta: array[ eTipoTarjeta ] of ZetaPChar =('Titular','Reposici�n Titular','Adicional','Reposici�n Adicional' );
    aPuEmbarque: array[ ePuEmbarque ] of ZetaPChar =('Mar�tima','Helic�ptero');
    aPuDomicilio: array[ ePuDomicilio ] of ZetaPChar =('Terrestre','Avi�n');
    aPuHabitacion: array[ ePuHabitacion ] of ZetaPChar =('Doble','Sencilla','VIP','Triple');
    aStatusTarea: array[ eStatusTarea ] of ZetaPChar =( 'Pendiente', 'Terminada', 'Cancelada' );
    aStatusHosp : array[ eStatusHosp ] of ZetaPChar =( 'Solicitada', 'Terminada', 'Cancelada' );
    aSentidoHosp : array[ eSentidoHosp ] of ZetaPChar =( 'Embarque', 'Desembarque', 'Otro' );
    aStatusTrasporte : array[ eStatusTrasporte ] of ZetaPChar =( 'Cancelado', 'Liberado', 'Reservado', 'En proceso', 'En tr�nsito', 'Arrib�', 'No se present�' );
    aTipoCalEvent : array[ eTipoCalEvent ] of ZetaPChar =( 'Embarque', 'Desembarque', 'Interplataformas');
    aTipoRuta : array[ eTipoRuta ] of ZetaPChar =( 'P�blica', 'Consolidada', 'Enlace');
    aStatusDePlaza :   array[ eStatusDePlaza ] of ZetaPChar =( 'Activa', 'Cancelada' );
    aPrioridadVac :   array[ ePrioridadVac ] of ZetaPChar =( 'Alta', 'Normal', 'Baja' );
    aStatusVac :   array[ eStatusVac ] of ZetaPChar =( 'Detenida', 'Contratar', 'Cubierta' );
    aPasoVac :   array[ ePasoVac ] of ZetaPChar =( 'Pendiente', 'Precontratado', 'Contratado' );
    aTrabaja :   array[ eTrabaja ] of ZetaPChar =( 'Descansa', 'Trabaja' );
    aPlantillaTipo : array[ ePlantillaTipo ] of ZetaPChar =( 'Autorizada', 'Adicional', 'Otros', 'Temporal');
    aPrioridadSolicitud : array[ ePrioridadSolicitud ] of ZetaPChar = ( 'Alta', 'Normal', 'Baja' );
    aCicloSolicitud  : array[ eCicloSolicitud ] of ZetaPChar = ( 'Sencillo', 'Redondo' );
    aReservacion :  array[ eReservacion ] of ZetaPChar = ( 'Electr�nica', 'Manual' );
    aStatusAutoriza: array[ eStatusAutoriza ] of ZetaPChar = ( 'Pendiente', 'Autorizado', 'Rechazado');
    aSentidoCamion: array[eSentidoCamion] of ZetaPChar = ( 'Salida', 'Llegada', 'Mixta' );
    aStatusCamion: array[eStatusCamion] of ZetaPChar = ( 'Programado', 'Sali�', 'Lleg�', 'Cancelado' );
    aStatusPasajero: array[eStatusPasajero] of ZetaPChar = ( 'Reservado', 'Libre' );
    aLogEventos: array[eLogEventos] of ZetaPChar = ( 'Correcto', 'Advertencia', 'Error', 'Entrevistado' );
    aTipoPermuta: array[ eTipoPermuta ] of ZetaPChar = ( 'Por D�a', 'Por Horas' );
    aPiramidacionPrestamos: array[ ePiramidacionPrestamos ] of ZetaPChar = ( 'Despu�s de la piramidaci�n', 'Antes de la piramidaci�n' );
    aClaseSistBitacora: array[ eClaseSistBitacora ] of ZetaPChar = ( '< Ninguno >', 'Sistema - Cat�logo de Usuarios', 'Sistema - C�talogo de Grupos de Usuario', 'Sistema - Derechos de Acceso por Grupo','Sistema - Asignaci�n de Roles a Usuario' {$ifndef DOS_CAPAS}, 'Sistema - Cat�logos Terminales GTI', 'Sistema - Biom�trico'{$endif},'Sistema - Administrador de env�os programados' );
    aAltaMedica: array[ eAltaMedica ] of ZetaPChar = ( 'Incapacidad', 'Permiso' );
    aStatusTEvento: array [ eStatusTEvento ] of ZetaPChar = ( 'Pendiente', '�xito', 'Error' );
    aTurnoAbordo: array [ eTurnoAbordo ] of ZetaPChar = ( 'Matutino', 'Vespertino', 'No aplica' );
    aBitacoraStatus: array [ eBitacoraStatus ] of ZetaPChar = ( 'En proceso', 'Terminado con �xito', 'Terminado con Error' );
    aGrupoRecibo: array [ eGrupoRecibo ] of ZetaPChar = ( 'Percepci�n', 'Deducci�n');
    aSubStatus: array [ eSubStatus ] of ZetaPChar = ( 'Sin cambios', 'Solicitar cambio', 'Cambio terminado', 'Boleto emitido' );
    aTipoPuestoContrato: array [ eTipoPuestoContrato ] of ZetaPChar = ( 'Permanente', 'Temporal' );
    aTipoPlaza : array [ eTipoPlaza ] of ZetaPChar = ( 'Permanente', 'Temporal');
    aTipoCobertura : array [eTipoCobertura ] of ZetaPChar = ( 'Titular', 'Ausencia', 'Vacante');

    aStatusPlantilla: array[ eStatusPlantilla ] of ZetaPChar = ( 'En dise�o', 'Liberada', 'Cancelada' );
    aStatusValuacion: array[ eStatusValuacion ] of ZetaPChar = ( 'En proceso', 'Terminada', 'Cancelada' );
    aReclasificaBool: array[ eReclasificaBool ] of ZetaPChar = ( 'Sin cambio', 'S�', 'No' );
    aReclasificaRelacion: array[ eReclasificaRelacion ] of ZetaPChar = ( 'Sin cambio', 'L�nea', 'Staff' );
    aReclasificaTipoVigencia: array[ eReclasificaTipoVigencia ] of ZetaPChar = ( 'Sin cambio', 'Planta', 'Eventual' );
    aReclasificaZonaGeo: array[ eReclasificaZonaGeo ] of ZetaPChar = ( 'Sin cambio', 'A', 'B', 'C' );
    aClaseBitacoraCOTEMAR: array[ eClaseBitacoraCOTEMAR ] of ZetaPChar = ( '< Ninguno >', 'Plazas', 'Puestos por Contrato', 'Plantilla Autorizada');
    aClaseBitacoraCOTEMARLogistica: array[ eClaseBitacoraCOTEMARLogistica ] of ZetaPChar = ( '< Ninguno >', 'Solicitud de Transporte', 'Solicitud de Hospedaje', 'Viaje', 'Ruta');

    //LEPE
    aModulosSolTran: array[ eModulosSolTran ] of ZetaPChar = ( 'Log�stica', 'Guardias', 'Workflow', 'Kiosco' );

    aViaticoNivelRestriccion: array[ eViaticoNivelRestriccion ] of ZetaPChar = ( 'Informativo', 'Advertencia', 'Rechazo' );
    aViaticoTopeConceptoGasto: array[ eViaticoTopeConceptoGasto ] of ZetaPChar = ( 'Todos', 'Con Tope', 'Sin Tope' );

    aAccionBitacora: array [ eAccionBitacora ] of ZetaPChar = ( '< Ninguno >', 'Contenido', 'Mis Datos', 'Reporte', 'Cambio de NIP', 'LogIn', 'LogOut', 'Error al Navegar' );

    aTipoLogin: array[ eTipoLogin ] of ZetaPChar=('Login a Tress', 'Active Directory', 'Active Directory con Login a Tress' );
    aFiltroStatusEmpleado: array[ eFiltroStatusEmpleado ] of ZetaPChar=('Activos', 'Inactivos', 'Todos' );
    aAccionRegistros: array[ eAccionRegistros ] of ZetaPChar=('Sobreescribir', 'Ignorar');
    aDiasCafeteria: array[ eDiasCafeteria ] of ZetaPChar = ( 'Toda la Semana', 'Lunes', 'Martes', 'Mi�rcoles', 'Jueves', 'Viernes', 'S�bado', 'Domingo' );
    aStatusLiqImss: array[ eStatusLiqImss ] of ZetaPChar = ( 'Sin Calcular', 'Calculada Parcial', 'Calculada Total', 'Afectada Total' );
    aRequisitoTipo: array[eRequisitoTipo] of ZetaPChar = ( 'Documento', 'Otro(s)' );{OP:16.Abr.08}
    aTipoMovInfonavit: array[ eTipoMovInfonavit ] of ZetaPChar = ( 'Inicio de Cr�dito de Vivienda', 'Fecha de Suspensi�n de Descuento', 'Reinicio de Descuento', 'Modificaci�n de Tipo de Descuento',
                                                                   'Modificaci�n de Valor de Descuento', 'Modificaci�n de N�mero de Cr�dito' );
    aBimestres: array[ eBimestres ] of ZetaPChar = ( 'Enero - Febrero','Marzo - Abril','Mayo - Junio','Julio - Agosto','Septiembre - Octubre','Noviembre - Diciembre');

    aTipoDescuentoInfo: array[ eTipoInfonavit ] of ZetaPChar =('Elija Tipo de Descuento','Porcentaje','Cuota Fija','Veces SMGDF','Veces UMA');
    aStatusReglaPrestamo: array[ eStatusReglaPrestamo ] of ZetaPChar = ('Activos', 'Todos');
    aTipoJornadaTrabajo: array[ eTipoJornadaTrabajo ] of ZetaPChar = ('Matutino (8 Horas)', 'Vespertino (7.5 horas)','Nocturno (7 horas)');

    aFasesContingencia: array[ eFasesContingencia ] of ZetaPChar = ( 'Permanece a Bordo', 'Fase 1', 'Fase 2', 'Fase 3' );
    aStatusPersonaExterna: array[ eStatusPersonaExterna ] of ZetaPChar = ( 'Desembarc�', 'En Tr�nsito', 'Pernocta', 'A Bordo' );

    aStatusHospedaje: array[ eStatusHospedaje ] of ZetaPChar = ( 'Salida', 'Entrada' );

    aClaseNominaCOTEMAR: array[ eClaseNominaCOTEMAR ] of ZetaPChar = ('N�minas Ordinarias','N�minas Finales','N�minas Extraordinarias','N�minas Finiquitos','N�minas Pr�stamos','N�minas Anuales','Otras');
    aLstDispositivos: array[ eLstDispositivos ] of ZetaPChar = ('Reloj Checador','Cafeter�a','Caseta','Otros','Kiosco','Mis Datos');

    aStatusSimFiniquitos: array[ eStatusSimFiniquitos ] of ZetaPChar = ( 'Sin Aprobar', 'Aprobada','Aplicada' );
    aClaseRiesgo: array[ eClaseRiesgo ] of ZetaPChar = ( 'Elija una Clase','I - Ordinario', 'II - Bajo', 'III - Medio', 'IV - Alto', 'V - M�ximo');

    aObjetivoCursoSTPS :   array[ eObjetivoCursoSTPS ] of ZetaPChar = ( 'Sin Definir', 'Act. conocimientos, habilidades, nueva tecnolog�a',
                                                                        'Prevenir riesgos de trabajo', 'Incrementar la productividad',
                                                                        'Mejorar el nivel educativo', 'Preparar para ocupar vacantes � puestos nuevos' );
    aModalidadCursoSTPS:   array[ eModalidadCursoSTPS ] of ZetaPChar = ( 'Sin Definir', 'Presencial','En L�nea','Mixta' );
    aDiscapacidadSTPS:     array[ eDiscapacidadSTPS ] of ZetaPChar = ( 'Sin Definir', 'Motriz','Visual','Mental','Auditiva','De Lenguaje');
    aTipoInstitucionSTPS:  array[ eTipoInstitucionSTPS ] of ZetaPChar = ( 'Sin Definir', 'P�blica','Privada' );
    aTipoDocProbatorioSTPS: array[ eTipoDocProbatorioSTPS ] of ZetaPChar = ( 'Sin Definir', 'T�tulo','Certificado','Diploma','Otro' );
    aStatusDeclaracionAnual: array[ eStatusDeclaracionAnual ] of ZetaPChar = ( 'Sin Calcular', 'Abierta', 'Cerrada' );
    aTipoAgenteSTPS: array[ eTipoAgenteSTPS ] of ZetaPChar = ( 'Ninguno', 'Interno', 'Externo', 'Otros' );
    aTipoHoraTransferenciaCosteo: array[ eTipoHoraTransferenciaCosteo ] of ZetaPChar = ( 'Ordinarias', 'Extras' );
    aStatusTransCosteo: array[ eStatusTransCosteo ] of ZetaPChar = ( 'Por aprobar', 'Aprobada', 'Cancelada' );
    aStatusTransCosteoConsulta: array[ eStatusTransCosteoConsulta ] of ZetaPChar = ('Por aprobar por m�', 'Por aprobar por otro Supervisor','Aprobadas por m�','Aprobadas por otro Supervisor','Canceladas por m�','Canceladas por otro Supervisor','< Todas >');
    aTipoDescConciliaInfonavit : array[ eTipoDescConciliaInfonavit ] of ZetaPChar = ( 'Bimestral', 'Acumulado de N�mina' );
    aTipoSGM : array[ eTipoSGM ] of ZetaPChar = ( 'Gastos Medicos Mayores', 'Gastos Medicos Menores','Otros' );
    aTipoEmpSGM : array[ eTipoEmpSGM ] of ZetaPChar = ( 'Titular', 'Dependiente' );
    aStatusSGM : array[ eStatusSGM ] of ZetaPChar = ( 'Activa','Vencida','Cancelada');
    aAuthTressEmail :  array[ eAuthTressEmail ] of ZetaPChar = ( 'Ninguno', 'Login B�sico' );
    aTipoBrigada : array[ eTipoBrigada ] of ZetaPChar = ('Ninguna', 'Primeros auxilios', 'B�squeda y rescate',
                                         'Evacuaci�n', 'Prevenci�n y combate de incendios', 'Multifuncional',
                                         'Brigada de Derrames', 'Otro tipo de brigada'); {AL. V2013}
    aUsoConceptoNomina : array[ eUsoConceptoNomina ] of ZetaPChar = ('Todas', 'Ordinarias', 'Especiales'); {AL. V2013}
    aTipoAsegurado : array[ eTipoAsegurado ] of ZetaPChar = ( 'Empleado', 'Pariente' );

    aStatusTimbrado  : array[ eStatusTimbrado ] of ZetaPChar = ('Pendiente', 'Timbrada Parcial', 'Timbrada', 'Timbrado pendiente', 'Cancelaci�n pendiente', 'Pendiente', 'Timbrada');
    aStatusTimbradoPeticion : array[ eStatusTimbradoPeticion ] of ZetaPChar = ( 'Pendiente', 'Proceso', 'Termin�', 'Error');

    aClaseConceptosSAT  : array[ eClaseConceptosSAT ] of ZetaPChar = ('No aplica', 'Percepci�n', 'Deducci�n', 'Exento', 'Otros Pagos');
    aTipoExentoSAT    : array[ eTipoExentoSAT   ] of ZetaPChar = ('No aplica', 'Exento 100%', 'Exento por Monto', 'Exento por Concepto','Gravado 100%', 'Gravado por Monto', 'Gravado por Concepto', 'Exento + Cancelaci�n');

    aTipoRegimenesSAT : array[ eTipoRegimenesSAT ] of ZetaPChar = ( 'No aplica',  'Reservado para uso futuro',
    'Sueldos y salarios',    'Jubilados',
    'Pensionados',
    'Asimilados a Salarios, Miembros de las Socieda...',
    'Asimilados a Salarios, Integrantes de Sociedad...',
    'Asimilados a Salarios, Miembros de consejos di...',
    'Asimilados a Salarios, Actividad empresarial, ...',
    'Asimilados a Salarios, Honorarios asimilados a...',
    'Asimilados a Salarios, Ingresos acciones o t�t...' );

    aMetodoPagoSAT    : array[ eMetodoPagoSAT   ] of ZetaPChar = ('No aplica','Efectivo', 'Deposito en Cuenta', 'Cheque',  'Tarjeta de Cr�dito', 'Tarjeta de D�bito', 'Efectivo y Monedero Electr�nico','Deposito en Cuenta y Monedero Electr�nico','Cheque y Monedero Electr�nico','Tarjeta de Cr�dito y Monedero Electr�nico','Tarjeta de D�bito y Monedero Electr�nico','Mixto' );

    aTipoColumnaReporte : array[ eTipoColumnaReporte   ] of ZetaPChar = ('Nombre', 'C�digo','Clasificaci�n','Tabla Principal');
  	aClasifiPeriodoConfidencial: array [eClasifiPeriodo] of ZetaPChar = ('Diario','Semanal', 'Catorcenal', 'Quincenal', 'Mensual', 'Decenal');
    aTPDias : array[ eTPDias ] of ZetaPChar = ( '1', '6', '12', '13', '26', '9' );
    aTPDias7 : array[ eTPDias7 ] of ZetaPChar = ( '1', '7', '14', '15.2', '30.4', '10.13' );
    aTPHoras : array[ eTPHoras ] of ZetaPChar = ( '8', '48', '96', '104', '208', '72'  );
    aTPHorasJo : array[ eTPHorasJo ] of ZetaPChar = ( 'N', 'N', 'N', 'S', 'S', 'S' );
    aTPDiasBT : array[ eTPDiasBT ] of ZetaPChar = ( '0', '0', '0', '0', '0', '0' );
    aClasifiTipoPeriodoConfidencial: array [eClasifiTipoPeriodoConfidencial] of ZetaPChar = ('Diario','Semanal', 'Catorcenal', 'Quincenal', 'Mensual', 'Decenal');
    aStatusSistDispositivos : array[ eStatusSisDispositivos ] of ZetaPChar = ('No Aplica', 'Configurada', 'No Configurada');
    aSolicitudEnvios : array [ eSolicitudEnvios ] of ZetaPChar = ('Pendiente', 'Procesando', 'Con error', 'Finalizado', 'Cancelar', 'Cancelado' );
    aPrevisionSocialClasifi : array [ ePrevisionSocialClasifi ] of ZetaPChar = ('No aplica', 'Previsi�n Social', 'Suma a Salarios' );
    aStatusBajaFonacot : array [ eStatusBajaFonacot ] of ZetaPChar = ('No aplica', 'Pago directo en Fonacot', 'Confirmaci�n de Fonacot', 'Ausente en C�dula', 'Otro' );
    aMesesFonacot: array[ eMesesFonacot ] of ZetaPChar = ( 'No aplica', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
                                        'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' );
    aIdTerminales: array[ eIdTerminales ] of ZetaPChar = ( '', 'Huella digital', 'Huella digital y proximidad HID', 'Huella digital y proximidad MIFARE',
                                        'Huella digital y c�digo de barras', 'Reconocimiento facial' );
    aTipoArchivoCedula: array [ eTipoArchivoCedula ] of ZetaPChar = ( 'C�dula por cr�dito', 'C�dula por trabajador');

function GetOffSet( Tipo: ListasFijas ): Integer;
begin
     { Si se agrega algun otro OFFSET,
       revisar la funcion ZFuncsTress.ZetaTf,
       que no vaya a modificar su funcionalidad}

     case Tipo of
          lfMeses, lfMes13: Result := 1;
     else
         Result := 0;
     end;
end;

{$ifdef COMPARTE_MGR}
function GetNotificacionDebeEspecificar( const eValor: eWFNotificacion ): Boolean;
begin
     case eValor of
          nvRol: Result := True;
          nvUsuarioEspecifico: Result := True;
          nvNivel : Result := True;
          {$ifdef RDD}
          {$else}
          {$endif}
     else
         Result := False;
     end;
end;

procedure LlenaNotificacionItems( Lista: TStrings );
var
   eTipo: eWFNotificacion;
begin
     with Lista do
     begin
          Clear;
          for eTipo := Low( eWFNotificacion ) to High( eWFNotificacion ) do
          begin
               Add( ZetaCommonLists.ObtieneElemento( lfWFNotificacion, Ord( eTipo ) ) );
          end;
     end;
end;
{$endif}

procedure LlenaLista( Tipo: ListasFijas; Lista: TStrings );
procedure Llena( Lista: TStrings; Valores: array of ZetaPChar );
var
   i: Integer;
begin
     with Lista do
     begin
          try
             BeginUpdate;
             Clear;
             for i := 0 to High( Valores ) do
                 Add( Valores[ i ] );
          finally
                 EndUpdate;
          end;
     end;

end;

{acl Funcion para validar que no venga el arreglo vacio}
function EstaVacio( aTipoArreglo: array of ZetaPChar ): Boolean;
var
   i: Integer;
begin
     Result:= False;
     for  i := 0 to High(aTipoArreglo) do
     begin
          Result:= aTipoArreglo[ i ] = '';

          if not Result then
             Break;
     end;
end;  

begin
     case Tipo of
          lfNivelUsuario: Llena( Lista, aNivelUsuario );
          lfTipoBitacora: Llena( Lista, aTipoBitacora );
          lfSexoDesc: Llena( Lista, aDesSexo );
          lfCandado: Llena( Lista, aCandado );
          lfMotivoBaja: Llena( Lista, aMotivoBaja );
          lfTipoOtrasPer: Llena( Lista, aTipoOtrasPer );
          lfIMSSOtrasPer: Llena( Lista, aIMSSOtrasPer );
          lfISPTOtrasPer: Llena( Lista, aISPTOtrasPer );
          lfIncidencias: Llena( Lista, aIncidencias );
          lfIncidenciaIncapacidad: Llena( Lista, aIncidenciaIncapacidad );
          lfRPatronModulo: Llena( Lista, aRPatronModulo );
          lfTipoTurno: Llena( Lista, aTipoTurno );
          lfTipoJornada: Llena( Lista, aTipoJornada );
          lfTipoPeriodo: { acl }
          begin
               if Assigned(FArregloPeriodo) then
                  Lista.AddStrings(FArregloPeriodo);
          end;
          lfTipoPeriodoConfidencial:
          begin
               if Assigned(FArregloPeriodoConfidencial) then
                  Lista.AddStrings(FArregloPeriodoConfidencial);
          end;
          lfUsoPeriodo: Llena(Lista, aUsoPeriodo);
          lfStatusPeriodo: Llena(Lista, aStatusPeriodo);
          lfTipoNomina: { acl }
          begin
               if Assigned(FArregloTipoNomina) then
                  Lista.AddStrings(FArregloTipoNomina);
          end;
          lfTipoFestivo: Llena( Lista, aTipoFestivo );
          lfTipoHorario: Llena( Lista, aTipoHorario );
          lfTipoReporte: Llena( Lista, aTipoReporte );
          lfTipoRangoActivo: Llena( Lista, aTipoRangoActivo );
          lfTipoRangoActivo2: Llena( Lista, aTipoRangoActivo2 );
          lfTipoInfoExp: Llena( Lista, aTipoInfoExp );
          lfConflictoImp: Llena( Lista, aConflictoImp );
          lfOperacionConflicto : Llena( Lista, aOperacionConflicto );
          {$ifdef RDD}
          {$else}
          lfClasifiReporte: Llena( Lista, aClasifiReporte );
          {$endif}
          lfTipoCampo: Llena( Lista, aTipoCampo );
          lfTipoOperacionCampo: Llena( Lista, aTipoOperacionCampo );
          lfJustificacion: Llena( Lista, aJustificacion );
          lfIncluyeEvento: Llena( Lista, aIncluyeEvento );
          lfTipoGlobal, lfTipoGlobalAuto:
          begin
               Llena( Lista, aTipoGlobal );
               if ( Tipo = lfTipoGlobal ) then
                  Lista.Delete( Ord( High( eTipoGlobal ) ) );
          end;
          lfTipoConcepto: Llena( Lista, aTipoConcepto );
          lfMotivoFaltaDias: Llena( Lista, aMotivoFaltaDias );
          lfMotivoFaltaHoras: Llena( Lista, aMotivoFaltaHoras );
          lfStatusAhorro: Llena( Lista, aStatusAhorro );
          lfStatusPrestamo: Llena( Lista, aStatusPrestamo );
          lfStatusAusencia: Llena( Lista, aStatusAusencia );
          lfTipoDiaAusencia: Llena( Lista, aTipoDiaAusencia );
          lfTipoChecadas: Llena( Lista, aTipoChecadas );
          lfDescripcionChecadas: Llena( Lista, aDescripcionChecadas );
          lfTipoPariente: Llena( Lista, aTipoPariente );
          lfTipoInfonavit: Llena( Lista, aTipoInfonavit );
          lfTipoVacacion: Llena( Lista, aTipoVacacion );
          lfStatusKardex: Llena( Lista, aStatusKardex );
          lfMotivoIncapacidad: Llena( Lista, aMotivoIncapacidad );
          lfFinIncapacidad: Llena( Lista, aFinIncapacidad );
          lfTipoPermiso: Llena( Lista, aTipoPermiso );
          lfTipoLiqIMSS: Llena( Lista, aTipoLiqIMSS );
          lfCampoCalendario: Llena( Lista, aCampoCalendario );
          lfTipoLiqMov: Llena( Lista, aTipoLiqMov );
          lfTipoAhorro: Llena( Lista, aTipoAhorro );
          lfHorasExtras: Llena( Lista, aHorasExtras );
          lfBancaElectronica: Llena( Lista, aBancaElectronica );
          lfYears: Llena( Lista, aYears );
          lfMeses: Llena( Lista, aMeses );
          lfZonaGeografica: Llena( Lista, aZonaGeografica );
          lfSexo: Llena(Lista, aDesSexo);
          lfCodigoLiqMov: Llena(Lista, aCodigoLiqmov );
          lfDiasSemana: Llena(Lista, aDiasSemana );
          lfMes13: Llena(Lista, aMes13);
          lfTipoRango: Llena(Lista, aTipoRango );
          lfLiqNomina: Llena(Lista, aLiqNomina );
          lfAutorizaChecadas: Llena(Lista, aAutorizaChecadas);
          lfAltaAhorroPrestamo: Llena(Lista, aAltaAhorroPrestamo );
          lfStatusEmpleado: Llena( Lista, aStatusEmpleado );
          lfStatusEmpleadoCedula: Llena( Lista, aStatusEmpleadoCedula );
          lfTipoFormato: Llena( Lista, aTipoFormato );
          lfExtFormato: Llena( Lista, aExtFormato );
          lfFormatoFormas: Llena( Lista, aFormatoFormas );
          lfFormatoASCII: Llena( Lista, aFormatoASCII );
          lfTipoCalculo: Llena( Lista, aTipoCalculo );
          lfHorasDias: Llena( Lista, aHorasDias );
          lfImportacion: Llena( Lista, aImportacion );
          lfOperacionMontos: Llena( Lista, aOperacionMontos );
          lfTipoBanda: Llena( Lista, aTipoBanda );
          lfClaseBitacora: Llena( Lista, aClaseBitacora );
          lfProcesos: Llena(Lista, aProcesos);
          lfStatusLectura : Llena( Lista, aStatusLectura );
          lfTipoLectura : Llena( Lista, aTipoLectura );
          lfTipoOpera : Llena( Lista, aTipoOpera );
          lfWorkStatus : Llena( Lista, aWorkStatus );
          lfTipoCedula : Llena( Lista, aTipoCedula );
          lfRangoFechas : Llena( Lista, aRangoFechas );
          lfCafeCalendario : Llena( Lista, aCafeCalendario );
          lfMotTools : Llena( Lista, aMotTools );
          lfDescuentoTools : Llena( Lista, aDescuentoTools );
          lfEmailFormato : Llena( Lista, aEmailFormato );
          lfEmailFrecuencia : Llena( Lista, aEmailFrecuencia );
          lfEmailNotificacion : Llena( Lista, aEmailNotificacion );
          lfEmailType : Llena( Lista, aEmailType );
          lfTipoCompanyName: Llena( Lista, aTipoCompanyName );
          lfCamposConteo : Llena( Lista, aCamposConteo );
          lfPuertoSerial : Llena( Lista, aPuertoSerial );
          lfRegla3x3: Llena( Lista, aRegla3x3 );
          lfTipoCompany: Llena( Lista, aTipoCompany );
          lfPrioridad: Llena( Lista, aPrioridad );
          lfReqStatus: Llena( Lista, aReqStatus );
          lfMotivoVacante: Llena( Lista, aMotivoVacante );
          lfEstudios: Llena( Lista, aEstudios );
          lfSolStatus: Llena( Lista, aSolStatus );
          lfEdoCivil: Llena( Lista, aEdoCivil );
          lfEdoCivilDesc: Llena( Lista, aEdoCivilDesc );
          lfCanStatus: Llena( Lista, aCanStatus );
          lfEntStatus: Llena( Lista, aEntStatus );
          lfTipoExamen: Llena( Lista, aTipoExamen );
          lfTipoConsulta: Llena( Lista, aTipoConsulta );
          lfTipoEstudio:  Llena ( Lista, aTipoEstudio );
          lfTipoAccidente: Llena ( Lista, aTipoAccidente );
          lfMotivoAcc:     Llena ( Lista, aMotivoAcc );
          lfTipoMedicina:  Llena ( Lista, aTipoMedicina );
          lfTipoDiagnost:  Llena ( Lista, aTipoDiagnost );
          lfTipoLesion:    Llena ( Lista, aTipoLesion );
          lfAxAtendio:     Llena ( Lista, aAxAtendio );
          lfAxTipo:        Llena ( Lista, aAxTipo );
          lfExCampoTipo: Llena( Lista, aExCampoTipo );
          lfExMostrar: Llena( Lista, aExMostrar );
          lfExTipo: Llena( Lista, aExTipo );
          lfEmTermino: Llena ( Lista, aEmTermino );
          lfTurnoLabor: Llena( Lista, aTurnoLabor );
          lfTipoArea: Llena( Lista, aTipoArea );
          lfTipoKarFecha: Llena( Lista, aTipoKarFecha );
          lfCancelaMod: Llena( Lista, aCancelaMod );
          lfInicioAmortizacion: Llena( Lista, aInicioAmortizacion );
          lfClaseAcciones: Llena( Lista, aClaseAcciones );
          lfTipoCompete: Llena( Lista, aTipoCompete );
          lfStatusAcciones: Llena( Lista, aStatusAcciones );
          lfStatusCursos: Llena( Lista, aStatusCursos );
          lfTipoCedInspeccion: Llena( Lista, aTipoCedInspeccion );
          lfResultadoCedInspeccion: Llena( Lista, aResultadoCedInspeccion );
          lfValorDiasVacaciones: Llena( Lista, aValorDiasVacaciones );
          lfFiltroEmpOrganigrama: Llena( Lista, aFiltroEmpOrganigrama );
          lfTipoPuesto: Llena( Lista, aTipoPuesto );
          lfUpdateKind: Llena( Lista, aUpdateKind );
          lfOperacionIncremento: Llena( Lista, aOperacionIncremento );
          lfFormatoImpFecha: Llena( Lista, aFormatoImpFecha );
          lfOut2Eat: Llena(Lista, aOut2Eat);
          lfEmpleadoRepetido: Llena(Lista, aEmpleadoRepetido);
          lfTipoCorregirFechas: Llena(Lista, aTipoCorregirFechas);
          lfTipoNavegador: Llena(Lista, aTipoNavegador);
          lfOrientacionNavegador: Llena(Lista, aOrientacionNavegador);
          lfPosicionNavegador: Llena(Lista, aPosicionNavegador);
          lfCompressionLevel  : Llena(Lista, aCompressionLevel);
          lfEncryptionLevel : Llena(Lista, aEncryptionLevel);
          lfFontEncoding : Llena(Lista, aFontEncoding);
          lfPageLayout : Llena(Lista, aPageLayout);
          lfPageMode  : Llena(Lista, aPageMode);
          lfTransitionEffect  : Llena(Lista, aTransitionEffect);
          lfAnfitrionStatus: Llena( Lista, aAnfitrionStatus );
          lfVisitanteStatus: Llena( Lista, aVisitanteStatus );
          lfCasetaStatus: Llena( Lista, aCasetaStatus );
          lfCitaStatus: Llena( Lista, aCitaStatus );
          lfCatCompanys: Llena( Lista, aCatCompanys );
          lfLibroStatus: Llena( Lista, aLibroStatus );
          lfTipoGafete: Llena( Lista, aTipoGafete );
          lfChecadasSimultaneas: Llena (Lista, aChecasSimultaneas );
          lfCorteStatus: Llena( Lista, aCorteStatus );
          lfAccesoRegla: Llena( Lista, aAccesoRegla );
          lfBusquedaLibro: Llena( Lista, aBusquedaLibro );
          lfImpresionGafete: Llena( Lista, aImpresionGafete );
          lfTipoProximidad: Llena( Lista, aTipoProximidad );
          {$ifdef COMPARTE_MGR}
          lfWFStatusModelo: Llena( Lista, aWFStatusModelo );
          lfWFNotificacion: Llena( Lista, aWFNotificacion );
          lfWFProcesoStatus: Llena( Lista, aWFProcesoStatus );
          lfWFTareaStatus: Llena( Lista, aWFTareaStatus );
          lfWFCorreoStatus: Llena( Lista, aEVCorreoStatus ); // GA 24/Ago/05: Se hizo as� porque se agreg� un ennumerado lfEVCorreoStatus fuera de los IFDEf'S de COMPARTE
          lfWFNotificacionCuando: LLena( Lista, aWFNotificacionCuando);
          lfWFNotificacionQue: LLena( Lista, aWFNotificacionQue);
          lfWFAutenticacionWebService: Llena( Lista, aWFAutenticacionWebService );
          lfWFCodificacionWebService: LLena( Lista, aWFCodificacionWebService );
          {$endif}
          {$ifdef ADUANAS}
          lfTipoCliente: Llena( Lista, aTipoCliente );
          lfTipoMaterial: Llena( Lista, aTipoMaterial );
          lfTipoMedida: Llena( Lista, aTipoMedida );
          lfStatusFactura: Llena( Lista, aStatusFactura );
          lfFacTipoProceso: Llena( Lista, aFacTipoProceso );
          lfTipoActivo: Llena( Lista, aTipoActivo );
          lfStatusPedimento: Llena( Lista, aStatusPedimento );
          lfProgramasTipoProceso: Llena( Lista, aProgramasTipoProceso );
          lfDescargaAlmacen: Llena( Lista, aDescargaAlmacen );
          lfDescargaTipo: Llena( Lista, aDescargaTipo );
          {$endif}
          //******** Evaluacion de Desempe�o *****************//
          lfStatusEncuesta: Llena( Lista, aStatusEncuesta );
          lfComentariosMGR: Llena( Lista, aComentariosMGR );
          lfEstiloRespuesta: Llena( Lista, aEstiloRespuesta );
          lfTipoEvaluaEnc: Llena( Lista, aTipoEvaluaEnc );
          lfStatusEvaluaciones: Llena( Lista, aStatusEvaluaciones );
          lfStatusSujeto: Llena( Lista, aStatusSujeto );
          lfStatusInscrito: Llena( Lista, aStatusInscrito );
          lfStatusSesiones: Llena( Lista, aStatusSesiones );
          lfTipoReserva:    Llena( Lista, aTipoReserva );
          lfStatusEvaluacion: Llena( Lista, aStatusEvaluacion );
          lfEVCorreoStatus: Llena( Lista, aEVCorreoStatus );
          lfTipoSupuestoRH: Llena( Lista, aTipoSupuestoRH );
          lfTipoPeriodoMes: Llena( Lista, aTipoPeriodoMes );
          lfDPEscolar: Llena( Lista, aDPEscolar );
          lfDPNivel: Llena( Lista, aDPNivel );
          lfDPExpPuesto: Llena( Lista, aDPExpPuesto );
          lfJITipoPlaza: Llena( Lista, aJITipoPlaza );
          lfJIPlazaReporta: Llena( Lista, aJIPlazaReporta );
          lfKJustificacion: Llena( Lista, aKJustificacion );
          lfAccionBoton: Llena( Lista, aAccionBoton );
          lfLugarBoton: Llena( Lista, aLugarBoton );
          lfPosicionIcono: Llena( Lista, aPosicionIcono );
          lfTipoDiasBase: Llena( Lista, aTipoDiasBase );
          lfComentariosPedir: Llena( Lista, aComentariosPedir );
          lfComoCalificar: Llena( Lista, aComoCalificar );
          lfStatusAcuerdo: Llena( Lista, aStatusAcuerdo );
          lfBorrarMovimSUA: Llena( Lista, aBorrarMovimSUA );
          lfStatusPlanVacacion: Llena( Lista, aStatusPlanVacacion );
          lfPagoPlanVacacion: Llena( Lista, aPagoPlanVacacion );
          lfStatusPoliza: Llena( Lista, aStatusPoliza );
          lfTiposdePrestamo: Llena( Lista, aTiposdePrestamo );
          lfControles: Llena( Lista, aControles );
          lfStatusFestivos: LLena(Lista, aStatusFestivo);
          lfTratamientoExtras: Llena(Lista, aTratamientoExtras);
          lfDiasAplicaExento: LLena(Lista, aDiasAplicaExento);
          lfRolGuardias: LLena(Lista, aRolGuardias);
          lfStatusPlaza: LLena(Lista, aStatusPlaza);
          lfStatusGuardia: LLena(Lista, aStatusGuardia);
          lfClaseKarPlaza: LLena(Lista, aClaseKarPlaza);
          lfTipoKarPlaza: LLena(Lista, aTipoKarPlaza);
          lfEstadosGuardia: Llena( Lista, aEstadosGuardia );
          lfRitmosGuardia: Llena( Lista, aRitmosGuardia );
          lfTipoABordoGuardia : Llena(Lista, aTipoABordoGuardia);         {AV : 9/Ago/06}
          lfTipoDiaGuardia : Llena(Lista, aTipoDiaGuardia);         {AV : 9/Ago/06}
          lfTipoPagoGuardia : Llena(Lista, aTipoPagoGuardia);         {AV : 9/Ago/06}
          lfTipoEventoGuardia : Llena(Lista, aTipoEventoGuardia);         {AV : 1/Sept/06}
          lfClaseEventoGuardia : LLena(Lista, aClaseEventoGuardia );   {AV : 12/Sept/06}
          lfStatusAusenciaGuardia: Llena( Lista, aStatusAusenciaGuardia ); {AV : 27/Sept/06}
          lfTipoServicioLogistica : Llena( Lista, aTipoServicioLogistica ); {AV: 20/Oct/06}
          lfViaLogistica : Llena( Lista, aViaLogistica );                   {AV: 20/Oct/06}
          lfSentidoLogistica : Llena( Lista, aSentidoLogistica  );          {AV: 20/Oct/06}
          lfTipoPuertoLogistica  : Llena( Lista, aTipoPuertoLogistica );    {AV: 20/Oct/06}
          lfStatusViaje : Llena( Lista, aStatusViaje );    {EA: 23/ Oct/2006}
          lfStatusTarjeta : Llena( Lista, aStatusTarjeta );
          lfTipoTarjeta : Llena( Lista, aTipoTarjeta );
          lfPuEmbarque  : Llena( Lista, aPuEmbarque );  {EA: 01/Nov/2006}
          lfPuDomicilio   : Llena( Lista, aPuDomicilio );   {EA: 01/Nov/2006}
          lfPuHabitacion  : Llena( Lista, aPuHabitacion ); {EA: 01/Nov/2006}
          lfStatusTarea : Llena( Lista, aStatusTarea );
          lfStatusHosp  : Llena( Lista, aStatusHosp );
          lfSentidoHosp  : Llena( Lista, aSentidoHosp );
          lfStatusTrasporte : Llena( Lista, aStatusTrasporte );
          lfTipoCalEvent : Llena( Lista, aTipoCalEvent );
          lfTipoRuta : Llena( Lista, aTipoRuta );
          lfStatusDePlaza : Llena( Lista, aStatusDePlaza );
          lfPrioridadVac : Llena( Lista, aPrioridadVac );
          lfStatusVac : Llena( Lista, aStatusVac );
          lfPasoVac : Llena( Lista, aPasoVac );
          lfTrabaja : Llena( Lista, aTrabaja);
          lfPlantillaTipo : Llena(Lista, aPlantillaTipo);
          lfPrioridadSolicitud : Llena( Lista, aPrioridadSolicitud );
          lfCicloSolicitud : Llena( Lista, aCicloSolicitud );
          lfReservacion :  Llena( Lista, aReservacion );
          lfStatusAutoriza : Llena( Lista, aStatusAutoriza);
          lfSentidoCamion : LLena(Lista, aSentidoCamion );
          lfStatusCamion : Llena(Lista, aStatusCamion );
          lfStatusPasajero : Llena( Lista, aStatusPasajero );
          lfLogEventos     : Llena(Lista, aLogEventos );
          lfTipoPermuta : Llena( Lista, aTipoPermuta);
          lfPiramidacionPrestamos : Llena( Lista, aPiramidacionPrestamos );
          lfClaseSistBitacora : Llena( Lista, aClaseSistBitacora );
          lfAltaMedica     : Llena(Lista, aAltaMedica );{EA: 14/03/07 }
          lfStatusTEvento  : Llena(Lista, aStatusTEvento );{ EA: 17/04/07 }
          lfTurnoAbordo    : Llena(Lista, aTurnoAbordo );{AV : 25/04/07 }
          lfBitacoraStatus : Llena(Lista, aBitacoraStatus );{ EA: 17/04/07 }
          lfGrupoRecibo : Llena(Lista, aGrupoRecibo );{ EA: 21/04/07 }
          lfSubStatus : Llena(Lista, aSubStatus );{ EA: 30/05/07 }
          lfTipoPuestoContrato : Llena(Lista, aTipoPuestoContrato );{ EA: 30/05/07 }
          lfTipoPlaza : Llena(Lista, aTipoPlaza );{ EA: 30/05/07 }
          lfTipoCobertura : Llena( Lista, aTipoCobertura );
          lfRDDDefaultsTexto: Llena( Lista, aRDDDefaultsTexto );
          lfRDDDefaultsFloat: Llena( Lista, aRDDDefaultsFloat );
          lfRDDDefaultsLogico: Llena( Lista, aRDDDefaultsLogico );
          lfRDDValorActivo: Llena( Lista, aRDDValorActivo );
          lfStatusPlantilla: Llena( Lista, aStatusPlantilla);
          lfStatusValuacion: Llena( Lista, aStatusValuacion);
          lfReclasificaBool: Llena( Lista, aReclasificaBool );
          lfReclasificaRelacion: Llena( Lista, aReclasificaRelacion );
          lfReclasificaTipoVigencia: Llena( Lista, aReclasificaTipoVigencia );
          lfReclasificaZonaGeo: Llena( Lista, aReclasificaZonaGeo );
          lfClaseBitacoraCOTEMAR: Llena( Lista, aClaseBitacoraCOTEMAR );
          lfClaseBitacoraCOTEMARLogistica: Llena( Lista, aClaseBitacoraCOTEMARLogistica );

          //LEPE
          lfModulosSolTran: Llena( Lista, aModulosSolTran );

          lfViaticoNivelRestriccion: Llena( Lista, aViaticoNivelRestriccion );
          lfViaticoTopeConceptoGasto: Llena( Lista, aViaticoTopeConceptoGasto );

          lfAccionBitacora: Llena ( Lista, aAccionBitacora );

          lfTipoLogin: Llena(Lista, aTipoLogin );
          lfFiltroStatusEmpleado: Llena(Lista, aFiltroStatusEmpleado );
          lfAccionRegistros: Llena(Lista, aAccionRegistros );
          lfDiasCafeteria: Llena( Lista, aDiasCafeteria );
          lfRequisitoTipo: Llena( Lista, aRequisitoTipo );{OP:16.Abr.08}
          lfTipoMovInfonavit: Llena( Lista, aTipoMovInfonavit );
          lfBimestres: Llena( Lista, aBimestres );
          lfTipoDescuentoInfo: Llena ( Lista, aTipoDescuentoInfo );
          lfStatusReglaPrestamo: Llena ( Lista, aStatusReglaPrestamo );

	      lfFasesContingencia : Llena( Lista, aFasesContingencia );
          lfStatusPersonaExterna: Llena( Lista, aStatusPersonaExterna );
          lfStatusHospedaje: Llena( Lista, aStatusHospedaje );
          lfClaseNominaCOTEMAR: Llena( Lista, aClaseNominaCOTEMAR );
          lfLstDispositivos: Llena( Lista, aLstDispositivos );
	  
          lfTipoJornadaTrabajo: Llena ( Lista, aTipoJornadaTrabajo );

          lfObjetivoCursoSTPS : Llena (Lista,aObjetivoCursoSTPS );
          lfModalidadCursoSTPS : Llena (Lista,aModalidadCursoSTPS );
          lfTipoDocProbatorioSTPS : Llena(Lista,aTipoDocProbatorioSTPS );
          lfTipoInstitucionSTPS : Llena(Lista,aTipoInstitucionSTPS );
          lfDiscapacidadSTPS : Llena(Lista,aDiscapacidadSTPS );
          lfClaseRiesgo: Llena( Lista, aClaseRiesgo );
          lfStatusDeclaracionAnual: Llena( Lista, aStatusDeclaracionAnual );
          lfTipoAgenteSTPS : Llena( Lista, aTipoAgenteSTPS );
          lfTipoHoraTransferenciaCosteo : Llena( Lista, aTipoHoraTransferenciaCosteo );
          lfTipoDescConciliaInfonavit : Llena( Lista, aTipoDescConciliaInfonavit );

          lfAuthTressEmail : Llena(Lista,aAuthTressEmail );
          lfTipoSGM : Llena( Lista, aTipoSGM );
          lfTipoEmpSGM : Llena( Lista, aTipoEmpSGM );
          lfStatusSGM : Llena( Lista, aStatusSGM );
          lfTipoBrigada : Llena( Lista, aTipoBrigada ); {AL V2013}
          lfUsoConceptoNomina : Llena( Lista, aUsoConceptoNomina ); {AL V2013}
          lfStatusTransCosteo : LLena( Lista, aStatusTransCosteo );
          lfStatusTransCosteoConsulta : LLena( Lista, aStatusTransCosteoConsulta );
          lfTipoAsegurado : Llena( Lista, aTipoAsegurado );

          lfStatusTimbrado : Llena( Lista, aStatusTimbrado );
          lfStatusTimbradoPeticion : Llena( Lista, aStatusTimbradoPeticion );

          lfClaseConceptosSAT : Llena( Lista, aClaseConceptosSAT  );
          lfTipoExentoSAT  : Llena( Lista, aTipoExentoSAT );

          lfTipoRegimenesSAT  : Llena( Lista, aTipoRegimenesSAT );
          lfMetodoPagoSAT  : Llena( Lista, aMetodoPagoSAT );

          lfColumnasReporte : Llena( Lista, aTipoColumnaReporte );
          lfClasifiPeriodoConfidencial: Llena(Lista, aClasifiPeriodoConfidencial);

          lfTPDias : Llena( Lista, aTPDias );
          lfTPDias7 : Llena( Lista, aTPDias7 );
          lfTPHoras : Llena( Lista, aTPHoras );
          lfTPHorasJo : Llena( Lista, aTPHorasJo );
          lfTPDiasBT : Llena( Lista, aTPDiasBT );
		  lfStatusSistDispositivos : Llena( Lista, aStatusSistDispositivos);
          lfSolicitudEnvios : Llena ( Lista, aSolicitudEnvios );
          lfPrevisionSocialClasifi : Llena ( Lista, aPrevisionSocialClasifi );
          lfStatusBajaFonacot: Llena( Lista, aStatusBajaFonacot );
          lfMesesFonacot: Llena( Lista, aMesesFonacot );
          lfIdTerminales: Llena( Lista, aIdTerminales );
          lfTipoArchivoCedula: Llena( Lista, aTipoArchivoCedula );
	end;
end;

function GetClasificacionPeriodo(FLista: TStrings; const iTipoPeriodo:Integer): eClasifiPeriodo;
begin
     Result := eClasifiPeriodo(StrToInt(FLista.Values[IntToStr(iTipoPeriodo)])); //14479
end;

function GetDescripcionLista(FLista: TStrings; iLlave: integer): String;
var
   i: integer;
   sDescripcion: string;
begin
     sDescripcion := '';
     try
          for i := 0 to FLista.Count - 1 do
          begin
                if iLlave = StrToInt(FLista.Names[ i ]) then
                begin
                     sDescripcion := FLista.Values[ IntToStr(iLlave) ];
                     Break;
                end;
          end;
     finally
           Result := sDescripcion;
     end;
end;

{*** US 15153: El usuario requiere que los procesos y operaciones sobre Expediente que utilizan el tipo de periodo sigan funcionando ***}
function GetValidoPeriodoKArdex(FLista: TStrings; iLlave: integer): String;
const
     K_ENCONTRADO = 'ENCONTRADO';
var
   i: integer;
begin
     Result := '';
     try
          for i := 0 to FLista.Count - 1 do
          begin
                if iLlave = StrToInt(FLista.Names[ i ]) then
                begin
                     Result := K_ENCONTRADO;
                     Break;
                end;
          end;
      except
           on Error: Exception do
              Result := '';
      end;
end;
{*** FIN ***}


function ObtieneElemento( Tipo: ListasFijas; const Index: Integer ): String;
begin
     try
        case Tipo of
             lfNivelUsuario:          Result :=  aNivelUsuario[ eNivelUsuario( Index ) ];
             lfTipoBitacora:          Result :=  aTipoBitacora[ eTipoBitacora( Index ) ];
             lfSexoDesc:              Result :=  aDesSexo[ eSexo ( Index ) ];
             lfCandado:               Result :=  aCandado[ eCandado( Index ) ];
             lfMotivoBaja:            Result :=  aMotivoBaja[ eMotivoBaja( Index ) ];
             lfTipoOtrasPer:          Result :=  aTipoOtrasPer[ eTipoOtrasPer( Index ) ];
             lfIMSSOtrasPer:          Result :=  aIMSSOtrasPer[ eIMSSOtrasPer( Index ) ];
             lfISPTOtrasPer:          Result :=  aISPTOtrasPer[ eISPTOtrasPer( Index ) ];
             lfIncidencias:           Result :=  aIncidencias[ eIncidencias( Index ) ];
             lfIncidenciaIncapacidad: Result :=  aIncidenciaIncapacidad[ eIncidenciaIncapacidad( Index ) ];
             lfRPatronModulo:         Result :=  aRPatronModulo[ eRPatronModulo( Index ) ];
             lfTipoTurno:             Result :=  aTipoTurno[ eTipoTurno( Index ) ];
             lfTipoJornada:           Result :=  aTipoJornada[ eTipoJornada( Index ) ];
   			 lfTipoPeriodo:           Result :=  GetDescripcionLista(FArregloPeriodo, Index);
             lfTipoPeriodoKardex:     Result :=  GetValidoPeriodoKArdex(FArregloPeriodo, Index);
             lfTipoPeriodoConfidencial: Result := GetDescripcionLista(FArregloPeriodoConfidencial, Index);
             lfUsoPeriodo:            Result :=  aUsoPeriodo[eUsoPeriodo(Index)];
             lfStatusPeriodo:         Result :=  aStatusPeriodo[eStatusPeriodo(Index)];
             lfTipoNomina:            Result :=  GetDescripcionLista(FArregloTipoNomina, Index);
             lfTipoFestivo:           Result :=  aTipoFestivo[ eTipoFestivo( Index ) ];
             lfTipoHorario:           Result :=  aTipoHorario[ eTipoHorario( Index ) ];
             lfTipoReporte:           Result :=  aTipoReporte[ eTipoReporte( Index ) ];
             lfTipoRangoActivo:       Result :=  aTipoRangoActivo[ eTipoRangoActivo( Index ) ];
             lfTipoRangoActivo2:      Result :=  aTipoRangoActivo2[ eTipoRangoActivo( Index ) ];
             lfTipoInfoExp:           Result :=  aTipoInfoExp[ eTipoInfoExp( Index ) ];
             lfConflictoImp:          Result :=  aConflictoImp[ eConflictoImp( Index ) ];
             lfOperacionConflicto:    Result :=  aOperacionConflicto[ eOperacionConflicto( Index ) ];
             {$ifdef RDD}
             {$else}
             lfClasifiReporte:        Result :=  aClasifiReporte[ eClasifiReporte( Index ) ];
             {$endif}
             lfTipoCampo:             Result := aTipoCampo[ eTipoCampo( Index ) ];
             lfTipoOperacionCampo:    Result :=  aTipoOperacionCampo[ eTipoOperacionCampo( Index ) ];
             lfJustificacion:         Result :=  aJustificacion[ TAlignment( Index ) ];
             lfIncluyeEvento:         Result :=  aIncluyeEvento[ eIncluyeEvento( Index ) ];
             lfTipoGlobal, lfTipoGlobalAuto: Result :=  aTipoGlobal[ eTipoGlobal( Index ) ];
             lfTipoConcepto:          Result :=  aTipoConcepto[ eTipoConcepto( Index ) ];
             lfMotivoFaltaDias:       Result :=  aMotivoFaltaDias[ eMotivoFaltaDias( Index ) ];
             lfMotivoFaltaHoras:      Result :=  aMotivoFaltaHoras[ eMotivoFaltaHoras( Index ) ];
             lfStatusAhorro:          Result :=  aStatusAhorro[ eStatusAhorro( Index ) ];
             lfStatusPrestamo:        Result :=  aStatusPrestamo[ eStatusPrestamo( Index ) ];
             lfStatusAusencia:        Result :=  aStatusAusencia[ eStatusAusencia( Index ) ];
             lfTipoDiaAusencia:       Result :=  aTipoDiaAusencia[ eTipoDiaAusencia( Index ) ];
             lfTipoChecadas:          Result :=  aTipoChecadas[ eTipoChecadas( Index ) ];
             lfDescripcionChecadas:   Result :=  aDescripcionChecadas[ eDescripcionChecadas( Index ) ];
             lfTipoPariente:          Result :=  aTipoPariente[ eTipoPariente( Index ) ];
             lfTipoInfonavit:         Result :=  aTipoInfonavit[ eTipoInfonavit( Index ) ];
             lfTipoVacacion:          Result :=  aTipoVacacion[ eTipoVacacion( Index ) ];
             lfStatusKardex:          Result :=  aStatusKardex[ eStatusKardex( Index ) ];
             lfMotivoIncapacidad:     Result :=  aMotivoIncapacidad[ eMotivoIncapacidad( Index ) ];
             lfFinIncapacidad:        Result :=  aFinIncapacidad[ eFinIncapacidad( Index ) ];
             lfTipoPermiso:           Result :=  aTipoPermiso[ eTipoPermiso( Index ) ];
             lfTipoLiqIMSS:           Result :=  aTipoLiqIMSS[ eTipoLiqIMSS( Index ) ];
             lfCampoCalendario:       Result := aCampoCalendario[ eCampoCalendario( Index ) ];
             lfTipoLiqMov:            Result := aCodigoLiqMov[ eTipoLiqMov( Index ) ];
             lfTipoAhorro:            Result := aTipoAhorro[ eTipoAhorro( Index ) ];
             lfHorasExtras:           Result := aHorasExtras[ eHorasExtras( Index ) ];
             lfBancaElectronica:      Result :=  aBancaElectronica[ eBancaElectronica( Index ) ];
             lfYears:                 Result := aYears[ eYears( Index ) ];
             lfMeses:                 Result := aMeses[ eMeses( Index ) ];
             lfZonaGeografica:        Result := aZonaGeografica[ eZonaGeografica( Index ) ];
             lfSexo:                  Result := aSexo[ eSexo( Index ) ];
             lfCodigoLiqMov:          Result := aCodigoLiqmov[ eCodigoLiqMov( Index ) ];
             lfDiasSemana:            Result := aDiasSemana[ eDiasSemana( Index ) ];
             lfMes13:                 Result := aMes13[ eMes13( Index ) ];
             lfTipoRango:             Result := aTipoRango[ eTipoRango( Index ) ];
             lfLiqNomina:             Result := aLiqNomina[ eLiqNomina( Index ) ];
             lfAutorizaChecadas:      Result := aAutorizaChecadas[ eAutorizaChecadas( Index ) ];
             lfAltaAhorroPrestamo:    Result := aAltaAhorroPrestamo[ eAltaAhorroPrestamo( Index ) ];
             lfStatusEmpleado:        Result := aStatusEmpleado[ eStatusEmpleado( Index ) ];
             lfStatusEmpleadoCedula:  Result := aStatusEmpleadoCedula[ eStatusEmpleadoCedula( Index ) ];
             lfTipoFormato:           Result := aTipoFormato[ eTipoFormato( Index ) ];
             lfExtFormato:            Result := aExtFormato[ eExtFormato( Index ) ];
             lfFormatoFormas:         Result := aFormatoFormas[ eFormatoFormas( Index ) ];
             lfFormatoASCII:          Result := aFormatoASCII[ eFormatoASCII( Index ) ];
             lfTipoCalculo:           Result := aTipoCalculo[ eTipoCalculo( Index ) ];
             lfHorasDias:             Result := aHorasDias[ eHorasDias( Index ) ];
             lfImportacion:           Result := aImportacion[ eImportacion( Index ) ];
             lfOperacionMontos:       Result := aOperacionMontos[ eOperacionMontos( Index ) ];
             lfTipoBanda:             Result := aTipoBanda[ eTipoBanda( Index ) ];
             lfClaseBitacora:         Result := aClaseBitacora[ eClaseBitacora( Index ) ];
             lfProcesos:              Result := aProcesos[ Procesos(Index) ];
             lfStatusLectura:         Result := aStatusLectura[ eStatusLectura( Index ) ];
             lfTipoLectura:           Result := aTipoLectura[ eTipoLectura( Index ) ];
             lfTipoOpera:             Result := aTipoOpera[ eTipoOpera( Index ) ];
             lfWorkStatus:            Result := aWorkStatus[ eWorkStatus( Index )];
             lfTipoCedula:            Result := aTipoCedula[ eTipoCedula( Index ) ];
             lfRangoFechas:           Result := aRangoFechas[ eRangoFechas( Index ) ];
             lfCafeCalendario:        Result := aCafeCalendario[ eCafeCalendario( Index ) ];
             lfMotTools:              Result := aMotTools[ eMotTools( Index ) ];
             lfDescuentoTools:        Result := aDescuentoTools[ eDescuentoTools( Index ) ];
             lfEmailFormato:          Result := aEmailFormato[ eEmailFormato( Index ) ];
             lfEmailFrecuencia:       Result := aEmailFrecuencia[ eEMailFrecuencia( Index ) ];
             lfEmailNotificacion:     Result := aEmailNotificacion[ eEmailNotificacion( Index ) ];
             lfEmailType:             Result := aEmailType[ eEmailType( Index ) ];
             lfTipoCompanyName:       Result := aTipoCompanyName[ eTipoCompany( Index ) ];
             lfCamposConteo:          Result := aCamposConteo[ eCamposConteo( Index ) ];
             lfPuertoSerial:          Result := aPuertoSerial[ ePuertoSerial( Index ) ];
             lfRegla3x3:              Result := aRegla3x3[ eRegla3x3( Index ) ];
             lfTipoCompany:           Result := aTipoCompany[ eTipoCompany( Index ) ];
             lfPrioridad:             Result := aPrioridad[ ePrioridad( Index ) ];
             lfReqStatus:             Result := aReqStatus[ eReqStatus( Index ) ];
             lfMotivoVacante:         Result := aMotivoVacante[ eMotivoVacante( Index ) ];
             lfEstudios:              Result := aEstudios[ eEstudios( Index ) ];
             lfSolStatus:             Result := aSolStatus[ eSolStatus( Index ) ];
             lfEdoCivil:              Result := aEdoCivil[ eEdoCivil( Index ) ];
             lfEdoCivilDesc:          Result := aEdoCivilDesc[ eEdoCivil( Index ) ];
             lfCanStatus:             Result := aCanStatus[ eCanStatus( Index ) ];
             lfEntStatus:             Result := aEntStatus[ eEntStatus( Index ) ];
             lfTipoExamen:            Result := aTipoExamen[ eTipoExamen( Index ) ];
             lfTipoConsulta:          Result := aTipoConsulta[ eTipoConsulta( Index ) ];
             lfTipoEstudio:           Result := aTipoEstudio[ eTipoEstudio( Index ) ];
             lfTipoAccidente:         Result := aTipoAccidente[ eTipoAccidente( Index ) ];
             lfMotivoAcc:             Result := aMotivoAcc[ eMotivoAcc( Index ) ];
             lfTipoMedicina:          Result := aTipoMedicina[ eTipoMedicina( Index ) ];
             lfTipoDiagnost:          Result := aTipoDiagnost[ eTipoDiagnost( Index ) ];
             lfTipoLesion:            Result := aTipoLesion[ eTipoLesion( Index ) ];
             lfAxAtendio:             Result := aAxAtendio[ eAxAtendio( Index ) ];
             lfAxTipo:                Result := aAxTipo[ eAxTipo( Index ) ];
             lfExCampoTipo:           Result := aExCampoTipo[ eExCampoTipo( Index ) ];
             lfExMostrar:             Result := aExMostrar[ eExMostrar( Index ) ];
             lfExTipo:                Result := aExTipo[ eExTipo( Index ) ];
             lfEmTermino:             Result := aEmTermino[ eEmTermino( Index ) ];
             lfTurnoLabor:            Result := aTurnoLabor[ eTurnoLabor( Index ) ];
             lfTipoArea:              Result := aTipoArea[ eTipoArea( Index ) ];
             lfTipoKarFecha:          Result := aTipoKarFecha[ eTipoKarFecha( Index ) ];
             lfCancelaMod:            Result := aCancelaMod[ eCancelaMod( Index ) ];
             lfInicioAmortizacion:    Result := aInicioAmortizacion[ eInicioAmortizacion( Index ) ];
             lfClaseAcciones:         Result := aClaseAcciones[ eClaseAcciones( Index ) ];
             lfTipoCompete:           Result := aTipoCompete[ eTipoCompete( Index ) ];
             lfStatusAcciones:        Result := aStatusAcciones[ eStatusAcciones( Index ) ];
             lfStatusCursos:          Result := aStatusCursos[ eStatusCursos( Index ) ];
             lfTipoCedInspeccion:     Result := aTipoCedInspeccion[ eTipoCedInspeccion( Index ) ];
             lfResultadoCedInspeccion:Result := aResultadoCedInspeccion[ eResultadoCedInspeccion( Index ) ];
             lfValorDiasVacaciones:   Result := aValorDiasVacaciones[ eValorDiasVacaciones( Index ) ];
             lfFiltroEmpOrganigrama:  Result := aFiltroEmpOrganigrama[ eFiltroEmpOrganigrama( Index ) ];
             lfTipoPuesto:            Result := aTipoPuesto[ eTipoPuesto( Index ) ];
             lfUpdateKind:            Result := aUpdateKind[ TUpdateKind( Index ) ];
             lfOperacionIncremento:   Result := aOperacionIncremento[ eOperacionIncremento( Index ) ];
             lfFormatoImpFecha:       Result := aFormatoImpFecha[ eFormatoImpFecha( Index ) ];
             lfOut2Eat:               Result := aOut2Eat[ eOut2Eat( Index ) ];
             lfEmpleadoRepetido:      Result := aEmpleadoRepetido[ eEmpleadoRepetido( Index ) ];
             lfTipoCorregirFechas:    Result := aTipoCorregirFechas[ eTipoCorregirFechas( Index ) ];
             lfTipoNavegador:         Result := aTipoNavegador[ eTipoNavegador( Index ) ];
             lfOrientacionNavegador:  Result := aOrientacionNavegador[ eOrientacionNavegador( Index ) ];
             lfPosicionNavegador:     Result := aPosicionNavegador[ ePosicionNavegador( Index ) ];
             lfCompressionLevel:      Result := aCompressionLevel[ eCompressionLevel( Index ) ];
             lfEncryptionLevel:       Result := aEncryptionLevel[ eEncryptionLevel( Index ) ];
             lfFontEncoding:          Result := aFontEncoding[ eFontEncoding( Index ) ];
             lfPageLayout:            Result := aPageLayout[ ePageLayout( Index ) ];
             lfPageMode:              Result := aPageMode[ ePageMode( Index ) ];
             lfTransitionEffect:      Result := aTransitionEffect[ eTransitionEffect( Index ) ];
             lfAnfitrionStatus:       Result := aAnfitrionStatus[ eAnfitrionStatus( Index ) ];
             lfVisitanteStatus:       Result := aVisitanteStatus[ eVisitanteStatus( Index ) ];
             lfCasetaStatus:          Result := aCasetaStatus[ eCasetaStatus( Index ) ];
             lfCitaStatus:            Result := aCitaStatus[ eCitaStatus( Index ) ];
             lfCatCompanys:           Result := aCatCompanys[ eCatCompanys( Index ) ];
             lfLibroStatus:           Result := aLibroStatus[ eLibroStatus( Index ) ];
             lfTipoGafete:            Result := aTipoGafete[ eTipoGafete( Index ) ];
             lfChecadasSimultaneas:   Result := aChecasSimultaneas [ eChecadasSimultaneas ( Index ) ];
             lfCorteStatus:           Result := aCorteStatus[ eCorteStatus( Index ) ];
             lfAccesoRegla:           Result := aAccesoRegla[ eAccesoRegla( Index ) ];
             lfBusquedaLibro:         Result := aBusquedaLibro[ eBusquedaLibro( Index ) ];
             lfImpresionGafete:       Result := aImpresionGafete[ eImpresionGafete( Index ) ];
             lfTipoProximidad:        Result := aTipoProximidad[ eTipoProximidad( Index ) ];
             {$ifdef COMPARTE_MGR}
             lfWFStatusModelo:        Result := aWFStatusModelo[ eWFStatusModelo( Index ) ];
             lfWFNotificacion:        Result := aWFNotificacion[ eWFNotificacion( Index ) ];
             lfWFProcesoStatus:       Result := aWFProcesoStatus[ eWFProcesoStatus( Index ) ];
             lfWFTareaStatus:         Result := aWFTareaStatus[ eWFTareaStatus( Index ) ];
             lfWFCorreoStatus:        Result := aEVCorreoStatus[ eEVCorreoStatus( Index ) ]; // GA 24/Ago/05: Se hizo as� porque se agreg� un ennumerado lfEVCorreoStatus fuera de los IFDEf'S de COMPARTE
             lfWFNotificacionCuando:  Result := aWFNotificacionCuando[ eWFNotificacionCuando( Index ) ];
             lfWFNotificacionQue:     Result := aWFNotificacionQue[ eWFNotificacionQue( Index ) ];
             lfWFAutenticacionWebService: Result := aWFAutenticacionWebService[ eWFAutenticacionWebService( Index ) ];
             lfWFCodificacionWebService: Result := aWFCodificacionWebService[ eWFCodificacionWebService ( Index ) ];
             {$endif}
             {$ifdef ADUANAS}
             lfTipoCliente:           Result := aTipoCliente[ eTipoCliente( Index ) ];
             lfTipoMaterial:          Result := aTipoMaterial[ eTipoMaterial( Index ) ];
             lfTipoMedida:            Result := aTipoMedida[ eTipoMedida( Index ) ];
             lfStatusFactura:         Result := aStatusFactura[ eStatusFactura( Index ) ];
             lfFacTipoProceso:        Result := aFacTipoProceso[ eFacTipoProceso( Index ) ];
             lfTipoActivo:            Result := aTipoActivo[ eTipoActivo( Index ) ];
             lfStatusPedimento:       Result := aStatusPedimento[ eStatusPedimento( Index ) ];
             lfProgramasTipoProceso:  Result := aProgramasTipoProceso[ eProgramasTipoProceso( Index ) ];
             lfDescargaAlmacen:       Result := aDescargaAlmacen[ eDescargaAlmacen ( Index ) ];
             lfDescargaTipo:          Result := aDescargaTipo[ eDescargaTipo  ( Index ) ];
             {$endif}
             //****** Evaluaci�n De Desempe�o ************//
             lfStatusEncuesta:        Result := aStatusEncuesta[ eStatusEncuesta( Index ) ];
             lfComentariosMGR:        Result := aComentariosMGR[ eComentariosMGR( Index ) ];
             lfEstiloRespuesta:       Result := aEstiloRespuesta[ eEstiloRespuesta( Index ) ];
             lfTipoEvaluaEnc:         Result := aTipoEvaluaEnc[ eTipoEvaluaEnc( Index ) ];
             lfStatusEvaluaciones:    Result := aStatusEvaluaciones[ eStatusEvaluaciones( Index ) ];
             lfStatusSujeto:          Result := aStatusSujeto[ eStatusSujeto( Index ) ];
             lfStatusInscrito:        Result := aStatusInscrito[ eStatusInscrito( Index ) ];
             lfStatusSesiones:        Result := aStatusSesiones[ eStatusSesiones( Index ) ];
             lfTipoReserva:           Result := aTipoReserva[ eTipoReserva( Index ) ];
             lfStatusEvaluacion:      Result := aStatusEvaluacion[ eStatusEvaluacion( Index ) ];
             lfEVCorreoStatus:        Result := aEVCorreoStatus[ eEVCorreoStatus( Index ) ];
             lfTipoSupuestoRH:        Result := aTipoSupuestoRH[ eTipoSupuestoRH( Index ) ];
             lfTipoPeriodoMes:        Result := aTipoPeriodoMes[ eTipoPeriodoMes( Index ) ];
             lfDPEscolar:             Result := aDPEscolar[ eDPEscolar( Index ) ];
             lfDPNivel:               Result := aDPNivel[ eDPNivel( Index ) ];
             lfDPExpPuesto:           Result := aDPExpPuesto[ eDPExpPuesto( Index ) ];
             lfJITipoPlaza:           Result := aJITipoPlaza[ eJITipoPlaza( Index ) ];
             lfJIPlazaReporta:        Result := aJIPlazaReporta[ eJIPlazaReporta( Index ) ];
             lfKJustificacion:        Result := aKJustificacion[ eKJustificacion( Index ) ];
             lfAccionBoton:           Result := aAccionBoton[ eAccionBoton( Index ) ];
             lfLugarBoton:            Result := aLugarBoton[ eLugarBoton( Index ) ];
             lfPosicionIcono:         Result := aPosicionIcono[ ePosicionIcono( Index ) ];
             lfTipoDiasBase:          Result := aTipoDiasBase[ eTipoDiasBase( Index ) ];
             lfComentariosPedir:      Result := aComentariosPedir[ eComentariosPedir( Index ) ];
             lfComoCalificar:         Result := aComoCalificar[ eComoCalificar( Index ) ];
             lfStatusAcuerdo:         Result := aStatusAcuerdo[ eStatusAcuerdo( Index ) ];
             lfBorrarMovimSUA:        Result := aBorrarMovimSUA[ eBorrarMovimSUA( Index ) ];
             lfStatusPlanVacacion:    Result := aStatusPlanVacacion[ eStatusPlanVacacion( Index ) ];
             lfPagoPlanVacacion:      Result := aPagoPlanVacacion[ ePagoPlanVacacion( Index ) ];
             lfStatusPoliza:          Result := aStatusPoliza[ eStatusPoliza( Index ) ];
             lfTiposdePrestamo:       Result := aTiposdePrestamo[ eTiposdePrestamo( Index ) ];
             lfControles:             Result := aControles [ eControles( Index ) ];
             lfStatusFestivos:        Result := aStatusFestivo[ eStatusFestivo( Index )];
             lfTratamientoExtras:     Result := aTratamientoExtras[ eTratamientoExtras( Index )];
             lfDiasAplicaExento:      Result := aDiasAplicaExento[ eDiasAplicaExento( Index )];
             lfRolGuardias:           Result := aRolGuardias[ eRolGuardias( Index )];
             lfStatusPlaza:           Result := aStatusPlaza[ eStatusPlaza( Index )];
             lfStatusGuardia:         Result := aStatusGuardia[ eStatusGuardia( Index )];
             lfClaseKarPlaza:         Result := aClaseKarPlaza[ eClaseKarPlaza( Index )];
             lfTipoKarPlaza:          Result := aTipoKarPlaza[ eTipoKarPlaza( Index )];
             lfEstadosGuardia:        Result := aEstadosGuardia[ eEstadosGuardia( Index ) ];
             lfRitmosGuardia:         Result := aRitmosGuardia[ eRitmosGuardia( Index ) ];
             lfTipoABordoGuardia:     Result := aTipoABordoGuardia[ eTipoABordoGuardia( Index ) ];
             lfTipoDiaGuardia:        Result := aTipoDiaGuardia[ eTipoDiaGuardia( Index ) ];
             lfTipoPagoGuardia:       Result := aTipoPagoGuardia[ eTipoPagoGuardia( Index ) ];
             lfTipoEventoGuardia:     Result := aTipoEventoGuardia[ eTipoEventoGuardia( Index ) ];
             lfClaseEventoGuardia:    Result := aClaseEventoGuardia[ eClaseEventoGuardia( Index )];
             lfStatusAusenciaGuardia: Result :=  aStatusAusenciaGuardia[ eStatusAusenciaGuardia( Index ) ];
             lfTipoServicioLogistica: Result :=  aTipoServicioLogistica[ eTipoServicioLogistica( Index ) ];
             lfViaLogistica:          Result :=  aViaLogistica[ eViaLogistica( Index ) ];
             lfSentidoLogistica:      Result :=  aSentidoLogistica[ eSentidoLogistica( Index ) ];
             lfTipoPuertoLogistica:   Result :=  aTipoPuertoLogistica[ eTipoPuertoLogistica( Index ) ];
             lfStatusViaje:           Result := aStatusViaje[ eStatusViaje(Index) ];
             lfStatusTarjeta:         Result := aStatusTarjeta[ eStatusTarjeta(Index) ];
             lfTipoTarjeta:           Result := aTipoTarjeta[ eTipoTarjeta(Index) ];
             lfPuEmbarque:            Result := aPuEmbarque[ ePuEmbarque(Index) ];
             lfPuDomicilio:           Result := aPuDomicilio[ ePuDomicilio(Index) ];
             lfPuHabitacion:          Result := aPuHabitacion[ ePuHabitacion(Index) ];
             lfStatusTarea:           Result := aStatusTarea[ eStatusTarea(Index) ];
             lfStatusHosp:            Result := aStatusHosp[ eStatusHosp(Index) ];
             lfSentidoHosp:           Result := aSentidoHosp[eSentidoHosp(Index) ];
             lfStatusTrasporte:       Result := aStatusTrasporte[eStatusTrasporte(Index) ];
             lfTipoCalEvent:          Result := aTipoCalEvent[eTipoCalEvent(Index) ];
             lfTipoRuta:              Result := aTipoRuta[eTipoRuta(Index) ];
             lfStatusDePlaza:         Result := aStatusDePlaza[ eStatusDePlaza(Index) ];
             lfPrioridadVac :         Result := aPrioridadVac[ ePrioridadVac(Index) ];
             lfStatusVac :            Result := aStatusVac[ eStatusVac(Index) ];
             lfPasoVac :              Result := aPasoVac[ ePasoVac(Index) ];
             lfTrabaja :              Result := aTrabaja[ eTrabaja(Index) ];
             lfPlantillaTipo :        Result := aPlantillaTipo[ ePlantillaTipo(Index) ];
             lfPrioridadSolicitud :   Result := aPrioridadSolicitud[ ePrioridadSolicitud(Index) ];
             lfCicloSolicitud :       Result := aCicloSolicitud[ eCicloSolicitud(Index) ];
             lfReservacion :          Result := aReservacion[ eReservacion(Index) ];
             lfStatusAutoriza :       Result := aStatusAutoriza[ eStatusAutoriza(Index) ];
             lfSentidoCamion:         Result := aSentidoCamion[ eSentidoCamion( Index )];
             lfStatusCamion:          Result := aStatusCamion [ eStatusCamion ( Index )];
             lfStatusPasajero:        Result := aStatusPasajero [ eStatusPasajero ( Index )];
             lfLogEventos:            Result := aLogEventos [ eLogEventos ( Index )];
             lfTipoPermuta :          Result := aTipoPermuta[ eTipoPermuta(Index) ];
             lfPiramidacionPrestamos: Result := aPiramidacionPrestamos [ ePiramidacionPrestamos(Index) ];
             lfClaseSistBitacora:     Result := aClaseSistBitacora [ eClaseSistBitacora(Index) ];
             lfAltaMedica  :          Result := aAltaMedica[ eAltaMedica(Index) ];
             lfStatusTEvento :        Result := aStatusTEvento [ eStatusTEvento(Index) ];
             lfTurnoAbordo :          Result := aTurnoAbordo[ eTurnoAbordo(Index) ];
             lfBitacoraStatus :       Result := aBitacoraStatus[ eBitacoraStatus(Index) ];
             lfGrupoRecibo :          Result := aGrupoRecibo[ eGrupoRecibo(Index) ];
             lfSubStatus :            Result := aSubStatus[ eSubStatus(Index) ];
             lfTipoPuestoContrato:    Result := aTipoPuestoContrato[ eTipoPuestoContrato(Index) ];
             lfTipoPlaza :            Result := aTipoPlaza[ eTipoPlaza(Index) ];
             lfTipoCobertura :        Result := aTipoCobertura [ eTipoCobertura (Index) ];
             lfRDDDefaultsTexto:      Result := aRDDDefaultsTexto[ eRDDDefaultsTexto( Index ) ];
             lfRDDDefaultsFloat:      Result := aRDDDefaultsFloat[ eRDDDefaultsFloat( Index ) ];
             lfRDDDefaultsLogico:     Result := aRDDDefaultsLogico[ eRDDDefaultsLogico( Index ) ];
             lfRDDValorActivo:        Result := aRDDValorActivo[ eRDDValorActivo( Index ) ];
             lfStatusPlantilla:       Result := aStatusPlantilla [ eStatusPlantilla(Index)];
             lfStatusValuacion:       Result := aStatusValuacion [ eStatusValuacion(Index)];
             lfReclasificaBool:       Result := aReclasificaBool [ eReclasificaBool(Index)];
             lfReclasificaRelacion:   Result := aReclasificaRelacion [ eReclasificaRelacion(Index)];
             lfReclasificaTipoVigencia: Result := aReclasificaTipoVigencia [ eReclasificaTipoVigencia(Index)];
             lfReclasificaZonaGeo:    Result := aReclasificaZonaGeo [ eReclasificaZonaGeo(Index)];
             lfClaseBitacoraCOTEMAR : Result := aClaseBitacoraCOTEMAR [ eClaseBitacoraCOTEMAR(Index)];
             lfClaseBitacoraCOTEMARLogistica : Result := aClaseBitacoraCOTEMARLogistica [ eClaseBitacoraCOTEMARLogistica(Index)];

             //LEPE
             lfModulosSolTran : Result := aModulosSolTran [ eModulosSolTran(Index) ];

             lfViaticoNivelRestriccion : Result := aViaticoNivelRestriccion [ eViaticoNivelRestriccion(Index) ];
             lfViaticoTopeConceptoGasto : Result := aViaticoTopeConceptoGasto[ eViaticoTopeConceptoGasto(Index) ];

             lfAccionBitacora:        Result := aAccionBitacora[ eAccionBitacora ( Index ) ];

             lfTipoLogin:             Result := aTipoLogin[ eTipoLogin( Index ) ];
             lfFiltroStatusEmpleado:  Result := aFiltroStatusEmpleado[ eFiltroStatusEmpleado( Index ) ];

             lfDiasCafeteria:         Result := aDiasCafeteria[ eDiasCafeteria( Index ) ];
             lfRequisitoTipo:         Result := aRequisitoTipo[ eRequisitoTipo( Index ) ];{OP:16.Abr.08}             
             lfTipoMovInfonavit:      Result := aTipoMovInfonavit[ eTipoMovInfonavit(Index)];
             lfBimestres:             Result := aBimestres[ eBimestres(Index)];
             lfTipoDescuentoInfo:      Result := aTipoDescuentoInfo [ eTipoInfonavit(Index)];
             lfStatusLiqImss:         Result := aStatusLiqImss[ eStatusLiqImss(Index)];
             lfStatusReglaPrestamo:   Result := aStatusReglaPrestamo[ eStatusReglaPrestamo(Index)];
             lfTipoJornadaTrabajo:     Result := aTipoJornadaTrabajo[ eTipoJornadaTrabajo(Index)];

	           lfFasesContingencia: Result := aFasesContingencia [ eFasesContingencia(Index) ];
             lfStatusPersonaExterna: Result := aStatusPersonaExterna [eStatusPersonaExterna(Index) ];
             lfStatusHospedaje: Result := aStatusHospedaje [ eStatusHospedaje( Index ) ];
             lfClaseNominaCOTEMAR: Result :=  aClaseNominaCOTEMAR[ eClaseNominaCOTEMAR( Index ) ];
             lfLstDispositivos: Result := aLstDispositivos[ eLstDispositivos( Index ) ];
             lfStatusSimFiniquitos : Result :=  aStatusSimFiniquitos [ eStatusSimFiniquitos( Index ) ];
             lfObjetivoCursoSTPS      : Result := aObjetivoCursoSTPS [ eObjetivoCursoSTPS( Index ) ];
             lfModalidadCursoSTPS     : Result := aModalidadCursoSTPS [ eModalidadCursoSTPS( Index ) ];
             lfTipoInstitucionSTPS    : Result := aTipoInstitucionSTPS [ eTipoInstitucionSTPS( Index ) ];
             lfDiscapacidadSTPS       : Result := aDiscapacidadSTPS [ eDiscapacidadSTPS( Index ) ];
             lfTipoDocProbatorioSTPS  : Result := aTipoDocProbatorioSTPS [ eTipoDocProbatorioSTPS( Index ) ];
             lfClaseRiesgo            : Result := aClaseRiesgo [eClaseRiesgo( Index )];
             lfStatusDeclaracionAnual : Result := aStatusDeclaracionAnual[ eStatusDeclaracionAnual( Index ) ];
             lfTipoAgenteSTPS         : Result := aTipoAgenteSTPS[ eTipoAgenteSTPS( Index ) ];
             lfTipoHoraTransferenciaCosteo: Result := aTipoHoraTransferenciaCosteo[ eTipoHoraTransferenciaCosteo( Index ) ];
             lfTipoDescConciliaInfonavit : Result := aTipoDescConciliaInfonavit[ eTipoDescConciliaInfonavit( Index ) ];

             lfAuthTressEmail            : Result := aAuthTressEmail [eAuthTressEmail( Index )];             
             lfTipoSGM : Result := aTipoSGM[ eTipoSGM( Index ) ];
             lfTipoEmpSGM : Result := aTipoEmpSGM[ eTipoEmpSGM( Index ) ];
             lfStatusSGM : Result := aStatusSGM[ eStatusSGM( Index ) ];
             lfTipoBrigada : Result := aTipoBrigada[ eTipoBrigada( Index ) ];
             lfUsoConceptoNomina : Result := aUsoConceptoNomina[ eUsoConceptoNomina( Index ) ];
             lfStatusTransCosteo : Result := aStatusTransCosteo[ eStatusTransCosteo( Index ) ];
             lfStatusTransCosteoConsulta : Result := aStatusTransCosteoConsulta[ eStatusTransCosteoConsulta( Index ) ];
             lfTipoAsegurado : Result := aTipoAsegurado[ eTipoAsegurado( Index ) ];

             lfStatusTimbrado : Result := aStatusTimbrado[ eStatusTimbrado( Index ) ];
             lfStatusTimbradoPeticion : Result := aStatusTimbradoPeticion[ eStatusTimbradoPeticion( Index ) ];

             lfClaseConceptosSAT : Result := aClaseConceptosSAT[ eClaseConceptosSAT( Index ) ];
             lfTipoExentoSAT  : Result := aTipoExentoSAT[ eTipoExentoSAT( Index ) ];
             lfTipoRegimenesSAT  : Result := aTipoRegimenesSAT[ eTipoRegimenesSAT( Index ) ];
             lfMetodoPagoSAT  : Result := aMetodoPagoSAT[ eMetodoPagoSAT( Index ) ];
             lfColumnasReporte : Result := aTipoColumnaReporte [ eTipoColumnaReporte( Index ) ];
             lfClasifiPeriodoConfidencial : Result := aClasifiPeriodoConfidencial [ eClasifiPeriodo( Index ) ];
             lfTPDias : Result := aTPDias [ eTPDias( Index ) ];
             lfTPDias7 : Result := aTPDias7 [ eTPDias7( Index ) ];
             lfTPHoras : Result := aTPHoras [ eTPHoras( Index ) ];
             lfTPHorasJo : Result := aTPHorasJo [ eTPHorasJo( Index ) ];
             lfTPDiasBT : Result := aTPDiasBT [ eTPDiasBT( Index ) ];
             lfClasifiPeriodo: Result := aClasifiTipoPeriodoConfidencial[ eClasifiTipoPeriodoConfidencial( Index ) ];
             lfStatusSistDispositivos : Result := aStatusSistDispositivos[eStatusSisDispositivos( Index)];
             lfSolicitudEnvios : Result := aSolicitudEnvios [ eSolicitudEnvios ( Index ) ];
             lfPrevisionSocialClasifi : Result := aPrevisionSocialClasifi [ ePrevisionSocialClasifi ( Index ) ];
             lfStatusBajaFonacot : Result := aStatusBajaFonacot [ eStatusBajaFonacot ( Index ) ];
             lfMesesFonacot : Result := aMesesFonacot [ eMesesFonacot ( Index ) ];
             lfIdTerminales : Result := aIdTerminales [ eIdTerminales ( Index ) ];
             lfTipoArchivoCedula : Result := aTipoArchivoCedula [ eTipoArchivoCedula ( Index ) ];
        else
            Result := '';
        end;
     except
           Result := IntToStr( Index ) + '=Indefinido';
     end;
end;

{$ifdef FALSE}
procedure InsertListasFijas; 
 var oLista,oLista2, oFijas:TStrings;
     oInventario: TStrings;
     i: ListasFijas;
     j: integer;
begin
     oLista:= TStringList.Create;
     oLista2:= TStringList.Create;
     oFijas:= TStringList.Create;
     oInventario:= TStringList.Create;

     for i:= Low(ListasFijas) to High(ListasFijas) do
     begin
          oLista.Add( Format( 'INSERT R_ListaVal(LV_CODIGO, LV_NOMBRE) ' +
                              'VALUES(%d,''%s'') ', [ Ord(i), TypInfo.GetEnumName( TypeInfo( ListasFijas ), Ord(i) ) ] ) );
          LlenaLista( i, oLista2 );
          for j:= 0 to oLista2.Count - 1 do
          begin
               oLista.Add( Format( 'INSERT R_VALOR(LV_CODIGO,VL_CODIGO,VL_DESCRIP) ' +
                                   'VALUES(%d,%d,''%s'') ', [ Ord(i), j, ObtieneElemento( i, j ) ] ) );
               oFijas.Add( Format( 'INSERT TFIJAS(TF_TABLA,TF_CODIGO,TF_DESCRIP) ' +
                                   'VALUES(%d,%d,''%s'') ', [ Ord(i), j, ObtieneElemento( i, j ) ] ) );
          end;
          oInventario.Add( IntToStr( Ord(i) ) +';'+ TypInfo.GetEnumName( TypeInfo( ListasFijas ), ord(i) ) );
     end;
     oLista.SaveToFile('d:\temp\ListaFijas.txt');
     oFijas.SaveToFile('d:\temp\ListaFijasViejo.txt');
     oInventario.SaveToFile('d:\temp\Inventario2011.txt');
end;
{$ENDIF}

procedure llenaprocVersion();
begin
  {$ifdef COMPARTE_MGR}
    aProcesosVersion := [ prNinguno,
                 prReporteador ];
    {$else}
           {$ifdef ADUANAS}
           aProcesosVersion := [prNinguno,
                        prReporteador,
                        prDescargaPedimentos,
                        prDeshacerDescargas,
                        prCalcularImpuestos  ];
           {$else}
     aProcesosVersion:= [prNinguno,                                          	{0  }
                        prRHSalarioIntegrado,                               	{1  }
                        prRHCambioSalario,                                  	{2  }
                        prRHPromediarVariables,                             	{3  }
                        prRHVacacionesGlobales,                             	{4  }
                        prRHAplicarTabulador,                               	{5  }
                        prRHAplicarEventos,                                 	{6  }
                        prRHImportarKardex,                                 	{7  }
                        prRHCancelarKardex,                                 	{8  }
                        prRHCursoTomado,                                    	{9  }
	
                        prASISPollRelojes,                                  	{10 }
                        prASISProcesarTarjetas,                             	{11 }
                        prASISCalculoPreNomina,                             	{12 }
                        prASISExtrasPer,                                    	{13 }
                        prASISRegistrosAut,                                 	{14 }
                        prASISCorregirFechas,                               	{15 }
                        prASISRecalculoTarjetas,                            	{16 }
                                                    	
                        prNOCalcular,                                       	{17 }
                        prNOAfectar,                                        	{18 }
                        prNOImprimirListado,                                	{19 }
                        prNOImprimirRecibos,                                	{20 }
                        prNOPolizaContable,                                 	{21 }
                        prNODesafectar,                                     	{22 }
                        prNOLimpiarAcum,                                    	{23 }
                        prNOImportarMov,                                    	{24 }
                        prNOExportarMov,                                    	{25 }
                        prNOPagosPorFuera,                                  	{26 }
                        prNOCancelarPasadas,                                	{27 }
                        prNOLiquidacion,                                    	{28 }
                        prNOCalculoNetoBruto,                               	{29 }
                        prNORastrearCalculo,                                	{30 }
                        prNOFoliarRecibos,                                  	{31 }

                        prNODefinirPeriodos,                                	{32 }
                        prNOCalculoAguinaldo,                               	{33 }
                        prNORepartoAhorro,                                  	{34 }
                        prNOCalculoPTU,                                     	{35 }
                        prNOCreditoSalario,                                 	{36 }
                        prNOISPTAnual,                                      	{37 }
                        prNORecalcAcum,                                     	{38 }

                        prIMSSCalculoPagos,                                 	{39 }
                        prIMSSCalculoRecargos,                              	{40 }
                        prIMSSRevisarSUA,                                   	{41 }
                        prIMSSExportarSUA,                                  	{42 }
                        prIMSSCalculoPrima,                                 	{43 }
                        prIMSSTasaINFONAVIT,                                	{44 }
                                                    	
                        prASISAjusteColectivo,                              	{45 }
                        prSISTBorrarBajas,                                  	{46 }
                        prSISTBorrarTarjetas,                               	{47 }
                        prSISTDepurar,                                      	{48 }
                        prSISTMigracion,                                    	{49 }
                        prRHTranferencia,                                   	{50 }
                        prNOPagoAguinaldo,                                  	{51 }
                        prNOPagoRepartoAhorro,                              	{52 }
                        prNOPagoPTU,                                        	{53 }
                                                    	
                        prSISTCierreAhorros,                                	{54 }
                        prSISTCierrePrestamos,                              	{55 }
                        prSISTBorrarNominas,                                	{56 }
                                                    	
                        prNOISPTAnualPago,                                  	{57 }
                        prNOPagoRecibos,                                    	{58 }
                        prNOReFoliarRecibos,                                	{59 }
                        prNOCopiarNominas,                                  	{60 }
                        prNOCalcularDiferencias,                            	{61 }
                        prRHCancVacGlobales,                                	{62 }
                        prSISTBorrarPOLL,                                   	{63 }
                        prLabCalcularTiempos,                               	{64 }
                        prLabDepuracion,                                    	{65 }
                        prLabLecturas,                                      	{66 }
                        prLabImportarOrdenes,                               	{67 }
                        prReporteador,                                      	{68 }
                        prRHCierreVacacionGlobal,                           	{69 }
                        prRHEntregarHerramienta,                            	{70 }
                        prRHRegresarHerramienta,                            	{71 }
                        prSISTBorrarHerramienta,                            	{72 }
                        prLabImportarPartes,                                	{73 }
                        prNOCalcularNueva,                                  	{74 }
                        prDepuraSeleccion,                                  	{75 }
                        prLabCancelarBreaks,                                	{76 }
                        prRHImportarAltas,                                  	{77 }
                        prNOCreditoAplicado,                                	{78 }
                        prRHRenumeraEmpleados,                              	{79 }
                        prRHPermisoGlobal,                                  	{80 }
                        prNOCalculaRetroactivos,                            	{81 }
                        prLabAfectarLabor,                                  	{82 }
                        prLabImportarComponentes,                           	{83 }
                        prRHBorrarCursoTomado,                              	{84 }
                        prNODeclaracionAnual,                               	{85 }
                        prCAFEComidasGrupales,                              	{86 }
                        prCAFECorregirFechas,                               	{87 }
                        prLabImportarCedulas,                               	{88 }
                        prLabImportarCambioAreas,                           	{89 }
                        prLabImportarProduccionIndividual,                  	{90 }
                        prSISTNumeroTarjeta,                                	{91 }
                        prRHCambioTurno,                                    	{92 }
                        // MA: Procesos para Evaluaci�n de Desempe�o        	
                        prDEAgregaEmpEvaluarGlobal,                         	{93 }
                        prDEPrepararEvaluaciones,                           	{94 }
                        prDECalcularResultados,                             	{95 }
                        prDERecalcularPromedios,                            	{96 }
                        prDECierraEvaluaciones,                             	{97 }
                        prRHImportarAsistenciaSesiones,                     	{98 }
                        prRHCancelarCierreVacaciones,                       	{99 }
                        prDECorreosInvitacion,                              	{100}
                        prDECorreosNotificacion,                            	{101}
                        prDEAsignaRelaciones,                               	{102}
                        //Presupuestos                                      	
                        prPREDepuraSupuestosRH,                             	{103}
                        prPREDepuraNominas,                                 	{104}
                        prPRECalculaCubo,                                   	{105}
                        prPRESimulacionPresupuestos,                        	{106}
                        prDECreaEncuestaSimple,                             	{107}
                        prASISIntercambioFestivo,                           	{108}
                        prASISCancelarExcepcionesFestivos,                  	{109}
                        prRHCancelarPermisosGlobales,                       	{110}
                        prRHImportarAhorrosPrestamos,                       	{111}
                        prRHRecalculaSaldosVacaciones,                      	{112}
                                                                                        //Procesos Cotemar (Administrador de Guardias)
                       // prCTCalcularPreNominaGuardias,       {AV: 5-Sept-06}	{113}
                       // prCTRegistrarCambiosPlazaVacantes,   {AV: 21-Sept-06	{114}
                       // prCTRegistrarCambiosEnvioTransicion, {AV: 21-Sept-06	{115}
                       // prCTCrearCalendarioGuardias,         {AV: 25-Sept-06	{116}
                       // prCTRegistrarSubieron,               {AV: 25-Sept-06	{117}
                       // prCTRegistrarBajaron,                {AV: 25-Sept-06	{118}
                       // prCTRegistrarPermanecenABordo,       {AV: 25-Sept-06	{119}
                       // prCTRegistrarPermanecenEnTierra,     {AV: 25-Sept-06	{120}
                       // prCTProgramarViajes,                 {AV: 24-Oct-06}	{121}
                       // prCTAsignarViaje,                    {AV: 30-Oct-06}	{122}
                       // prCTLiberarGuardiasALogistica,       {AV: 15-Nov-06}	{123}
                       // prCTPollPDA,                         {AV: 30-Nov-06}	{124}
                       // prCTCambiarStatusVacantes,                          	{125}
                       // prCTGenerarReciboEmpleado,                          	{126}
                                                                                      { Procesos de ICU Medical }
                       // prICUProgramarViajes,                               	{127}
                       // prICUAsignarViaje,                                  	{128}
                       // prICUPollPDA,                                       	{129}
                       // prICUPDA,                                           	{130}
                                                                                      { Procesos Permutas de Solvay }
                       // prSolAplicarPermutas,                               	{131}
                      //  prSolDepurarPermutas,                               	{132}
                                                                                      { Alta M�dica Guardias 14/03/07}
                       // prCTCambiarStatusAltaMedica,                        	{133}
                          prNOAjusteRetFonacot,                               	{134}
                          prNOCalcularPagoFonacot,                            	{135}

                       // prCTGenerarPlantillasOT,   {AV: 30-Abr-06}          	{136}
                                                                                          {Sincronizacion}
                      //  prCTExportar,                                       	{137}
                       // prCTRespalda,                                       	{138}
                       // prCTComprime,                                       	{139}
                       // prCTTransfiere,                                     	{140}
                       // prCTDescomprime,                                    	{141}
                       // prCTRestaura,                                       	{142}
                       // prCTImporta,                                        	{143}
                       // prCTImportaHorasExtras,                                 {144}
                        prSistEnrolarUsuario,                                   {145}
                        prSistImportarEnrolamientoMasivo,                       {146}
                        prRDDExportarDiccionario,                               {147}
                        prRDDImportarDiccionario,                               {148}
                        prIMSSAjusteRetInfonavit,                                {149}

                       // prCTSolicitudesPDA,                                     {150}{AV: 16.Jul.08}
                      //  prCTAbrirPreNominaGuardias,                             {151}{AV: 16.Jul.08}
                        //prCTCabinasCamas,                                       {152}{AV: 16.Jul.08}
                      //  prCTCambioRegistroPatronal,                             {153} {AV: 25.Ag.08}
                        prASISReprocesaTarjetas,                                {154}
                        prEmpAplicacionFiniGlobal,                              {155}
                        prEmpLiquidacionSimFinGlobal,                           {156}
						            prRHImportarInfonavit,									{157}
                        prRHCursoProgGlobal,                                    {158}
                        prRHCancelarCursoProgGlobal,                            {159}
                        prASIAjustIncapaCal, {ACL: 06.Abril.09}                 {160}
                       // prCTCalculaTarifas,  									                  {161} { Calculo de tarifas - Talleres Estrella 25/06/07 }
                        prRHFoliarCapacitaciones,                               {162}
                        prNoDeclaracionAnualCierre,                             {163}
                        prASISAutorizacionPreNomina,                            {164}
                        prASISRegistrarExcepcionesFestivos,                     {165}
                        prIMSSValidacionMovimientosIDSE,                        {166}
                        prNoTransferenciasCosteo,                               {167}
                        prNoCalculaCosteo,                                      {169}
                        prNoCancelaTransferencias,                              {170}
                        prASISImportacionClasificaciones,                       {168}{JB: 20111031 Importacion de Clasificaciones}
                        prSISTDepuraBitBiometrico,                              {171} // SYNERGY
                        prSISTAsignaNumBiometricos,                             {172} // SYNERGY
                        prSISTAsignaGrupoTerminales,                            {173} // SYNERGY
                        prRHImportarSGM,                                        {174}
                        prRHRenovacionSGM,                                      {175}
                        prRHBorrarSGM,                                          {176}
			                  prNOPrevioISR,                                          {177}
                        prRHRevisarDatosSTPS,                                   {178}
                        prRHReiniciarCapacitacionesSTPS,                        {179}
                        prSISTImportaTerminales,                                {180} // SYNERGY
                        prRHImportarOrganigrama,                                {181}
                        prNoTimbrarNomina,                                      {182}
                        {Motor de Patch 6/Mar/2014 RCM}
                        prPatchCambioVersion,                                   {183}
                        prPatchImportarDiccionario,                             {184}
                        prPatchImportarReportes,                                {185}
                        prPatchScriptEspecial,                                  {186}
                        {Fin Motor de Patch 6/Mar/2014 RCM}

                        prSISTPrepararPresupuestos,                             {187}
                        prSISTImportarTablas,                                   {188}
                        prRHImportarImagenes,                                   {189}
                        prSISTImportarTablasCSV,                                {190}
                        prSISTAgregarBaseDatosSeleccion,                        {191}
                        prSISTAgregarBaseDatosVisitantes,                       {192}
                        prSISTAgregarBaseDatosPruebas,                          {193}
                        prSISTAgregarBaseDatosEmpleados,
                        prSISTBorrarTimbradoNominas,                            {194}
                        prSISTExtraerChecadas,                              	  {195}
                        prSISTRecuperarChecadas,								                {196}
                        prNOAjusteRetFonacotCancelacion,                        {197}
                        prEmpCambioTurnoMasivo,                                 {198}
                        prEmpPensiones,                                         {199}
                        prCAFEhistorial,                                        {200}
                        prNOImportarAcumuladosRS,                               {201}
                        prNORecalcAhorros,                                      {202}
                        prSISTImportarCafeteria,								{203}
						prDEAgregarEncuesta,                                    {204}
                        prNORecalcPrestamos,                                    {205}
                        prSISTAgregarTareaCalendarioReportes,                   {206}
                        prSISTServicioCorreos,                                  {207}
                        prNOFonacotImportarCedulas,                             {208}
                        prNOFonacotGenerarCedula,                               {209}
                        prNOFonacotEliminarCedula,                               {210}
                        //Conciliacion Timbrado Nomina
                        prIniciarConciliacionTimbrado,                          {208}
                        prAplicarConciliacionTimbrado,                          {209}
                        prNoPendienteNominaEmpleado                             {2011}
                        ];
                        {$endif}
    {$endif}
end;

initialization
begin
     {$IFDEF FALSE}
     InsertListasFijas;
     {$ENDIF}
	 llenaprocVersion();
end;

end.













