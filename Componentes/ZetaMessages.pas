unit ZetaMessages;

{$A-}
{$WEAKPACKAGEUNIT}

interface

uses Windows, Messages, SysUtils, Forms;

{ Window Messages }

const
  { NOTE: All Message Numbers below 0x0400 are RESERVED }
  { Private Window Messages Start Here }
  WM_WIZARD_MOVE           = ( WM_USER + 1 );
  WM_EXAMINAR              = ( WM_USER + 2 );
  WM_EMPTY                 = ( WM_USER + 3 );
  WM_NOT_EMPTY             = ( WM_USER + 4 );
  WM_WIZARD_FOCUS          = ( WM_USER + 5 );
  WM_STATE_RELOAD          = ( WM_USER + 6 );
  WM_WIZARD_OK             = ( WM_USER + 7 );
  WM_WIZARD_CANCELADO      = ( WM_USER + 8 );
  WM_WIZARD_ERROR          = ( WM_USER + 9 );
  WM_WIZARD_GRAVE          = ( WM_USER + 10 );
  WM_WIZARD_START          = ( WM_USER + 11 );
  WM_WIZARD_END            = ( WM_USER + 12 );
  //WM_DATE_TRIGGER          = ( WM_USER + 13 ); // Obsoleto, al parecer lo usaba Tress Cafeteria
  WM_FOCUS                 = ( WM_USER + 14 );
  WM_DEFOCUS               = ( WM_USER + 15 );
  WM_KILLWINDOW            = ( WM_USER + 16 );
  WM_WIZARD_SILENT         = ( WM_USER + 17 );

type
  TConfirmEvent = procedure ( Sender: TObject; var lOk: Boolean ) of object;

function SetFileNamePath( const sPath, sFile: String ): String;
function SetFileNameDefaultPath( const sFile: String ): String;
function WinExecAndWait32( const Programa, Parametros: String; Visibility: Integer ): Integer;

implementation

uses ZetaCommonTools;

function SetFileNamePath( const sPath, sFile: String ): String;
begin
     Result := sPath;
     if ( Result[ Length( Result ) ] <> '\' ) then
        Result := Result + '\';
     Result := Result + sFile;
end;

function SetFileNameDefaultPath( const sFile: String ): String;
begin
     Result := SetFileNamePath( ExtractFileDir( Application.ExeName ), sFile );
end;

function WinExecAndWait32( const Programa, Parametros: String; Visibility: Integer ): Integer;
var
   zAppName: array[ 0..512 ] of Char;
   zCurDir: array[ 0..255 ] of Char;
   WorkDir: String;
   StartupInfo: TStartupInfo;
   ProcessInfo: TProcessInformation;
   ExitCode: DWORD;
begin
     if FileExists( Programa ) then
     begin
          if ZetaCommonTools.StrLleno( Parametros ) then
             StrPCopy( zAppName, Programa + ' ' + Parametros )
          else
              StrPCopy( zAppName, Programa );
          GetDir( 0,WorkDir );
          StrPCopy( zCurDir,WorkDir );
          FillChar( StartupInfo, Sizeof( StartupInfo ), #0 );
          with StartupInfo do
          begin
               cb := Sizeof( StartupInfo );
               dwFlags := STARTF_USESHOWWINDOW;
               wShowWindow := Visibility;
          end;
          if not CreateProcess( nil,
                                zAppName, { pointer to command line string }
                                nil,      { pointer to process security attributes }
                                nil,      { pointer to thread security attributes }
                                False,    { handle inheritance flag }
                                CREATE_NEW_CONSOLE or NORMAL_PRIORITY_CLASS,          { creation flags }
                                nil,               { pointer to new environment block }
                                nil,               { pointer to current directory name }
                                StartupInfo,       { pointer to STARTUPINFO }
                                ProcessInfo ) then
          Result := -1 { pointer to PROCESS_INF / FRACASO}
          else
          begin
               with ProcessInfo do
               begin
                    WaitForSingleObject( hProcess, INFINITE );
                    GetExitCodeProcess( hProcess, ExitCode );
                    Result := 0; { SE EJECUTO CON EXITO }
               end;
          end;
     Application.ProcessMessages;
     end
     else
         Result := 1; { NO EXISTE EL ARCHIVO }
end;

end.
