unit ZetaDataSetTools;

interface

uses db, SysUtils;

function GetMaxValue( DataSet: TDataSet; const sCampo: String; const iMinimo: Integer ): Integer;     // El m�ximo siempre se obtiene en Valor Entero
function GetNextEntero( DataSet: TDataSet; const sCampo: String; const iMinimo: Integer = 0 ): Integer;
function GetNextCaracter( DataSet: TDataSet; const sCampo: String; const lAllChars: Boolean = FALSE ): char;
function CheckCambioCampo( oCampo1, oCampo2: TField; const lNulos: Boolean = FALSE ): Boolean;
function ConstruyeFiltroEmpleados( oDataset: TDataset; const sCampoFiltro: String ): String;
procedure RevisaCambioCampo( oCampo1, oCampo2: TField; const lNulos: Boolean = FALSE );

implementation

uses ZetaCommonClasses, ZetaCommonTools;

function GetMaxValue( DataSet: TDataSet; const sCampo: String; const iMinimo: Integer ): Integer;     // El m�ximo siempre se obtiene en Valor Entero
var
   oCampo: TField;
   iValor: Integer;
   Pos : TBookMark;
begin
     Result := iMinimo;   // Debe ser minimo el valor que indican - 1
     with DataSet do
     begin
          if not IsEmpty then
          begin
               if not ( state in [ dsEdit, dsInsert ] ) then
               begin
                    DisableControls;
                    try
                       Pos:= GetBookMark;
                       First;
                       oCampo := FindField( sCampo );
                       if Assigned( oCampo ) then
                       begin
                            while not EOF do
                            begin
                                 with oCampo do
                                 begin
                                      if ( not IsNull ) then
                                      begin
                                           case DataType of
                                                ftString:
                                                begin
                                                     if strLleno( AsString ) then
                                                        iValor := Ord( AsString[1] )
                                                     else
                                                        iValor := 0;
                                                end;
                                                ftSmallint, ftInteger, ftWord: iValor := AsInteger;
                                                ftDate, ftTime, ftDateTime: iValor := Trunc( AsDateTime );   // Se obtiene la parte entera ( date )
                                           else
                                                iValor := Result;  // Para que no se asigne a Result
                                           end;
                                           if ( iValor > Result ) then
                                              Result := iValor;
                                      end;
                                 end;
                                 Next;
                            end;
                       end;
                       if ( Pos <> nil ) then
                       begin
                            GotoBookMark( Pos );
                            FreeBookMark( Pos );
                       end;
                    finally
                           EnableControls;
                    end;
               end
               else
                   raise Exception.Create( Format( 'No se puede Investigar el M�ximo Valor de %s si est� en modo de Edici�n', [ sCampo ] ) );
          end;
     end;
end;

function GetNextCaracter( DataSet: TDataSet; const sCampo: String; const lAllChars: Boolean = FALSE ): char;
var
   iDigito: Integer;
begin
     // Llamar esta funci�n en evento BeforeInsert de Dataset ya que cambia la posici�n del DataSet
     Result := #0;
     if lAllChars then
        iDigito := 32
     else
        iDigito := Ord( '0' );                                        // Valor M�nimo es cero '0'
     iDigito := GetMaxValue( Dataset, sCampo, iDigito - 1 ) + 1;      // Resta 1 ya que si no encuentra otro se incrementar� en 1 y quedar� igual
     if ( ( iDigito >= 32 ) and ( iDigito <= 255 ) ) then
     begin
          Result := Chr( iDigito );
          if ( not lAllChars ) and ( not ( EsDigito( Result ) ) ) and ( not ( EsLetra( Result ) ) ) then   // Solo Letras y D�gitos
          begin
               if ( Result < 'A' ) then              // Puede brincar a la 'A'
                  Result := 'A'
               else if ( Result < 'a' ) then         // Puede brincar a la 'a'
                  Result := 'a'
               else
                  Result := #0;                      // Si no se puede asignar letra se deja vacio
          end;
     end;
end;

function GetNextEntero( DataSet: TDataSet; const sCampo: String; const iMinimo: Integer = 0 ): Integer;
begin
     Result :=  GetMaxValue( Dataset, sCampo, iMinimo ) + 1;
end;

function CheckCambioCampo(oCampo1, oCampo2: TField; const lNulos: Boolean = FALSE): Boolean;
begin
  Result := False;
  with oCampo1 do begin
    if not IsNull then begin
      if (DataType = ftString) then
        Result := (lNulos or strLleno(AsString)) and (oCampo2.AsString <> AsString)
      else if (DataType in [ftInteger, ftSmallint, ftWord, ftAutoInc]) then
        Result := (lNulos or (AsInteger > 0)) and (oCampo2.AsInteger <> AsInteger)
      else if (DataType in [ftFloat, ftCurrency, ftBCD, ftFMTBcd]) then
        Result := (lNulos or (AsFloat > 0)) and (oCampo2.AsFloat <> AsFloat)
      else if (DataType in [ftDate, ftTime, ftDateTime]) then
        Result := (lNulos or (AsDateTime > NullDateTime)) and (oCampo2.AsDateTime <> AsDateTime);
    end;
  end;
end;

procedure RevisaCambioCampo( oCampo1, oCampo2: TField; const lNulos: Boolean = FALSE );
begin
     if CheckCambioCampo( oCampo1, oCampo2, lNulos ) then
     begin
          if ( not ( oCampo2.DataSet.State in [ dsEdit, dsInsert ] ) ) then
             oCampo2.DataSet.Edit;
          oCampo2.Value := oCampo1.Value;
     end;
end;

function ConstruyeFiltroEmpleados( oDataset: TDataset; const sCampoFiltro: String ): String;
const
     K_LIMITE_OBJETOS_FB = 1000; { Corrige defecto de limite de empleados en comando IN() de BD Interbase o Firebird }
     K_FILTRO_LISTA_EMP = '%s IN (%s)';
var
   sListaEmp : String;
   Pos : TBookMark;
begin
     Result := ZetaCommonClasses.VACIO;
     with oDataSet do
     begin
          DisableControls;
          try
             if ( not IsEmpty ) then
             begin
                  Pos:= GetBookMark;
                  First;
                  sListaEmp := ZetaCommonClasses.VACIO;
                  while ( not EOF ) do
                  begin
                       sListaEmp := ConcatString( sListaEmp, FieldByName( 'CB_CODIGO' ).AsString, ',' );
                       if ( RecNo MOD K_LIMITE_OBJETOS_FB ) = 0 then
                       begin
                            Result := ConcatString( Result, Format( K_FILTRO_LISTA_EMP, [ sCampoFiltro, sListaEmp ] ), ' OR ' );
                            sListaEmp := ZetaCommonClasses.VACIO;
                       end;
                       Next;
                  end;
                  if ZetaCommonTools.strLleno( sListaEmp ) then
                     Result := ConcatString( Result, Format( K_FILTRO_LISTA_EMP, [ sCampoFiltro, sListaEmp ] ), ' OR ' );

                  if ( Pos <> nil ) then
                  begin
                       GotoBookMark( Pos );
                       FreeBookMark( Pos );
                  end;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

end.
