unit ZetaSmartLists;

interface

{$R *.RES}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ComCtrls, Buttons, ExtCtrls,cxbuttons;

type
  TZetaSmartLists = class;
  BotonSmartLists = ( bsSubir, bsBajar, bsEscoger, bsRechazar );
  ListBoxSmartLists = ( lbDisponibles, lbEscogidos );
  TSmartListsEvent = procedure( Sender: TObject; var Objeto: TObject; Texto: String ) of object;
  TSmartListsChangeTextEvent = procedure( Objeto: TObject; var Texto: String ) of object;
  TZetaSpeedButton = class( TSpeedButton )
  private
    { Private declarations }
    FCaptionShow: Boolean;
    procedure SetCaptionShow( const Value: Boolean );
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    procedure DefaultHandler( var Message ); override;  { from TControl }
  published
    { Published declarations }
    property CaptionShow: Boolean read FCaptionShow write SetCaptionShow default False;
  end;
  TZetaSmartListsButton = class( TSpeedButton )
  private
    { Private declarations }
    FTipo: BotonSmartLists;
    FSmartLists: TZetaSmartLists;
    procedure SetSmartLists( Value: TZetaSmartLists );
    procedure SetTipo( Value: BotonSmartLists );
    procedure SetUp;
    procedure WMPaint( var Message: TWMPaint ); message WM_PAINT;
  protected
    { Protected declarations }
    procedure Notification( AComponent: TComponent; Operation: TOperation ); override;
    procedure Loaded; override;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    procedure Click; override;
    procedure SetState;
  published
    { Published declarations }
    property Tipo: BotonSmartLists read FTipo write SetTipo;
    property SmartLists: TZetaSmartLists read FSmartLists write SetSmartLists;
  end;
   
  TZetaSmartListBox = class( TListBox )
  private
    { Private declarations }
    FSmartLists: TZetaSmartLists;
    FTipo: ListBoxSmartLists;
    procedure DoRefrescar( Sender: TObject );
    procedure DoEscoger( Sender: TObject );
    procedure DoRechazar( Sender: TObject );
  protected
    { Protected declarations }
    procedure KeyPress( var Key: Char ); override;
    procedure Notification( AComponent: TComponent; Operation: TOperation ); override;
  public
    { Public declarations }
    procedure SetUp( Tipo: ListBoxSmartLists; SmartLists: TZetaSmartLists );
  published
  end;
  TZetaSmartLists = class( TComponent )
  private
    { Private declarations }
    FBotones: TList;
    FControl: TWinControl;
    FBorrarAlCopiar: Boolean;
    FCopiarObjetos: Boolean;
    FListaDisponibles: TZetaSmartListBox;
    FListaEscogidos: TZetaSmartListBox;
    FOk2Move: Boolean;
    FAlBajar: TSmartListsEvent;
    FAlEscoger: TSmartListsEvent;
    FAlRechazar: TSmartListsEvent;
    FAlSeleccionar: TSmartListsEvent;
    FAlSubir: TSmartListsEvent;
    FAlCambiarTexto: TSmartListsChangeTextEvent;
    function HayListas: Boolean;
    function TraspasaItem( const Borrar, Copiar: Boolean; SourceIndex, TargetIndex: Integer; Source, Target: TStrings; Evento: TSmartListsEvent ): Boolean;
    procedure SetListaDisponibles( Value: TZetaSmartListBox );
    procedure SetListaEscogidos( Value: TZetaSmartListBox );
    procedure NotifyButtons;
    procedure SubirBajarEscogidos( const lArriba: Boolean; Evento: TSmartListsEvent );
    procedure TraspasaListas( Borrar, Copiar: Boolean; Source, Target: TListBox; Evento: TSmartListsEvent );
    procedure SelectItem( Index: Integer; ListBox: TListBox );
    procedure DoEvent( const Index: Integer; Lista: TStrings; Evento: TSmartListsEvent );
    procedure DoClick;
  protected
    { Protected declarations }
    procedure Notification( AComponent: TComponent; Operation: TOperation ); override;
    procedure Loaded; override;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    property Ok2Move: Boolean read FOk2Move write FOk2Move;
    property AlCambiarTexto: TSmartListsChangeTextEvent read FAlCambiarTexto write FAlCambiarTexto;
    function HayElementosDisponibles: Boolean;
    function HayElementosEscogidos: Boolean;
    function PrimerEscogido: Boolean;
    function UltimoEscogido: Boolean;
    procedure Subir;
    procedure Bajar;
    procedure Escoger;
    procedure Rechazar;
    procedure Refrescar;
    procedure SelectDisponible( Index: Integer );
    procedure SelectEscogido( Index: Integer );
    procedure Init;
  published
    { Published declarations }
    property BorrarAlCopiar: Boolean read FBorrarAlCopiar write FBorrarAlCopiar;
    property Control: TWinControl read FControl write FControl;
    property CopiarObjetos: Boolean read FCopiarObjetos write FCopiarObjetos;
    property ListaDisponibles: TZetaSmartListBox read FListaDisponibles write SetListaDisponibles;
    property ListaEscogidos: TZetaSmartListBox read FListaEscogidos write SetListaEscogidos;
    property AlBajar: TSmartListsEvent read FAlBajar write FAlBajar;
    property AlEscoger: TSmartListsEvent read FAlEscoger write FAlEscoger;
    property AlRechazar: TSmartListsEvent read FAlRechazar write FAlRechazar;
    property AlSeleccionar: TSmartListsEvent read FAlSeleccionar write FAlSeleccionar;
    property AlSubir: TSmartListsEvent read FAlSubir write FAlSubir;
  end;

implementation

{ *************** TZetaSpeedButton ***************** }

constructor TZetaSpeedButton.Create( AOwner: TComponent );
begin
     FCaptionShow := False;
     inherited Create( AOwner );
end;

procedure TZetaSpeedButton.SetCaptionShow( const Value: Boolean );
begin
     if ( FCaptionShow <> Value ) then
     begin
          FCaptionShow := Value;
          if ( Action <> nil ) then
             ActionChange( Action, False );
          Repaint;
     end;
end;

procedure TZetaSpeedButton.DefaultHandler( var Message );
begin
     with TMessage( Message ) do
     begin
          case Msg of
               WM_GETTEXT: if FCaptionShow then inherited DefaultHandler( Message );
               WM_GETTEXTLENGTH: if FCaptionShow then inherited DefaultHandler( Message );
               WM_SETTEXT: if FCaptionShow then inherited DefaultHandler( Message );
          else
              inherited DefaultHandler( Message );
          end;
     end;
end;

{ *************** TZetaSmartListsBtn ***************** }

constructor TZetaSmartListsButton.Create( AOwner: TComponent );
begin
     inherited Create( AOwner );
     Caption := '';
     ControlStyle := ControlStyle - [ csSetCaption ];
     Height := 25;
     Width := 25;
     if ( csDesigning in ComponentState ) then
        SetUp;
end;

procedure TZetaSmartListsButton.Notification( AComponent: TComponent; Operation: TOperation );
begin
     inherited Notification( AComponent, Operation );
     if ( Operation = opRemove ) and ( AComponent = SmartLists ) then
        SmartLists := nil;
end;

procedure TZetaSmartListsButton.Loaded;
begin
     inherited Loaded;
     SetUp;
end;

procedure TZetaSmartListsButton.SetUp;
var
   Imagen: TBitMap;
begin
     Imagen := TBitMap.Create;
     try
        case Tipo of
             bsSubir: Imagen.LoadFromResourceName( HInstance, 'BTN_SUBIR' );
             bsBajar: Imagen.LoadFromResourceName( HInstance, 'BTN_BAJAR' );
             bsEscoger: Imagen.LoadFromResourceName( HInstance, 'BTN_ESCOGER' );
             bsRechazar: Imagen.LoadFromResourceName( HInstance, 'BTN_RECHAZAR' );
        end;

        Glyph := Imagen;
     finally
            Imagen.Free;
     end;
end;

procedure TZetaSmartListsButton.SetSmartLists( Value: TZetaSmartLists );
begin
     if ( FSmartLists <> Value ) then
     begin
          FSmartLists := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

procedure TZetaSmartListsButton.SetTipo( Value: BotonSmartLists );
begin
     if ( FTipo <> Value ) then
     begin
          FTipo := Value;
          SetUp;
     end;
end;

procedure TZetaSmartListsButton.WMPaint( var Message: TWMPaint );
begin
     SetState;
     inherited;
end;

procedure TZetaSmartListsButton.SetState;
begin
     if Assigned( FSmartLists ) then
     begin
          case Tipo of
               bsSubir: Enabled := FSmartLists.HayElementosEscogidos and not FSmartLists.PrimerEscogido;
               bsBajar: Enabled := FSmartLists.HayElementosEscogidos and not FSmartLists.UltimoEscogido;
               bsEscoger: Enabled := FSmartLists.HayElementosDisponibles;
               bsRechazar: Enabled := FSmartLists.HayElementosEscogidos;
          end;
     end;
end;

procedure TZetaSmartListsButton.Click;
begin
     if Assigned( FSmartLists ) then
     begin
          case Tipo of
               bsSubir: FSmartLists.Subir;
               bsBajar: FSmartLists.Bajar;
               bsEscoger: FSmartLists.Escoger;
               bsRechazar: FSmartLists.Rechazar;
          end;
     end
     else
         inherited Click;
end;



procedure TZetaSmartListBox.Notification( AComponent: TComponent; Operation: TOperation );
begin
     inherited Notification( AComponent, Operation );
     if ( Operation = opRemove ) then
     begin
          if ( AComponent = FSmartLists ) then
             FSmartLists := nil;
     end;
end;

procedure TZetaSmartListBox.SetUp( Tipo: ListBoxSmartLists; SmartLists: TZetaSmartLists );
begin
     if ( FSmartLists <> SmartLists ) then
     begin
          FSmartLists := SmartLists;
          if ( SmartLists <> nil ) then
             SmartLists.FreeNotification( Self );
     end;
     if ( FTipo <> Tipo ) then
        FTipo := Tipo;
     case FTipo of
          lbDisponibles:
          begin
               if not Assigned( OnDblClick ) then
                  OnDblClick := DoEscoger;
          end;
          lbEscogidos:
          begin
               if not Assigned( OnClick ) then
                  OnClick := DoRefrescar;
               if not Assigned( OnDblClick ) then
                  OnDblClick := DoRechazar;
          end;
     end;
end;

procedure TZetaSmartListBox.KeyPress( var Key: Char );
begin
     if ( Key = Chr( VK_RETURN ) ) and ( FTipo = lbDisponibles ) then
     begin
          if ( Items.Count > 0 ) and ( ItemIndex < 0 ) then
             ItemIndex := 0;
          DoEscoger( Self );
          Key := Chr( 0 );
     end
     else
         inherited KeyPress( Key );
end;

procedure TZetaSmartListBox.DoEscoger( Sender: TObject );
begin
     FSmartLists.Escoger;
end;

procedure TZetaSmartListBox.DoRechazar( Sender: TObject );
begin
     FSmartLists.Rechazar;
end;

procedure TZetaSmartListBox.DoRefrescar( Sender: TObject );
begin
     FSmartLists.Refrescar;
end;

{ *************** TZetaSmartLists ***************** }

constructor TZetaSmartLists.Create( AOwner: TComponent );
begin
     inherited Create( AOwner );
     FBotones := TList.Create;
     FBotones.Capacity := ( Ord( High( BotonSmartLists ) ) + 1 );
end;

destructor TZetaSmartLists.Destroy;
begin
     FBotones.Free;
     inherited Destroy;
end;

procedure TZetaSmartLists.Notification( AComponent: TComponent; Operation: TOperation );
begin
     inherited Notification( AComponent, Operation );
     if ( Operation = opRemove ) then
     begin
          if ( AComponent = ListaDisponibles ) then
             ListaDisponibles := nil
          else
          if ( AComponent = ListaEscogidos ) then
             ListaEscogidos := nil;
     end;
end;

procedure TZetaSmartLists.Loaded;
begin
     inherited Loaded;
     ListaDisponibles.SetUp( lbDisponibles, Self );
     ListaEscogidos.SetUp( lbEscogidos, Self );
end;

procedure TZetaSmartLists.SetListaDisponibles( Value: TZetaSmartListBox );
begin
     if ( FListaDisponibles <> Value ) then
     begin
          FListaDisponibles := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

procedure TZetaSmartLists.SetListaEscogidos( Value: TZetaSmartListBox );
begin
     if ( FListaEscogidos <> Value ) then
     begin
          FListaEscogidos := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

function TZetaSmartLists.HayListas: Boolean;
begin
     Result := Assigned( FListaDisponibles ) and Assigned( FListaEscogidos );
end;

function TZetaSmartLists.HayElementosDisponibles: Boolean;
begin
     Result := HayListas and ( ListaDisponibles.Items.Count > 0 );
end;

function TZetaSmartLists.PrimerEscogido: Boolean;
begin
     with ListaEscogidos do
          if MultiSelect then
             Result := ( ( Items.Count > 0 ) and not Selected[ 0 ] )
          else
              Result := ( ItemIndex = 0 );
end;

function TZetaSmartLists.UltimoEscogido: Boolean;
begin
     with ListaEscogidos do
          if MultiSelect then
             Result := ( ( Items.Count > 0 ) and not Selected[ ( Items.Count - 1 ) ] )
          else
              Result := ( ItemIndex >= 0 ) and ( ItemIndex = ( Items.Count - 1 ) );
end;

function TZetaSmartLists.HayElementosEscogidos: Boolean;
begin
     Result := HayListas and ( ListaEscogidos.Items.Count > 0 );
end;

procedure TZetaSmartLists.SubirBajarEscogidos( const lArriba: Boolean; Evento: TSmartListsEvent );
var
   i, j: Integer;
   lOk: Boolean;
begin
     with ListaEscogidos do
     begin
          i := ItemIndex;
          if lArriba then
          begin
               lOk := ( i > 0 );
               j := ( i - 1 );
          end
          else
          begin
               lOk := ( i >= 0 ) and ( i < ( Items.Count - 1 ) );
               j := ( i + 1 );
          end;
          if lOk then
          begin
               with Items do
               begin
                    try
                       BeginUpdate;
                       DoEvent( i, Items, Evento );
                       Exchange( i, j );
                       ItemIndex := j;
                    finally
                           EndUpdate;
                    end;
               end;
               NotifyButtons;
          end;
     end;
end;

function TZetaSmartLists.TraspasaItem( const Borrar, Copiar: Boolean; SourceIndex, TargetIndex: Integer; Source, Target: TStrings; Evento: TSmartListsEvent ): Boolean;
var
   Objeto: TObject;
   Texto: String;
begin
     FOk2Move := True;
     with Source do
     begin
          Texto := '';
          Objeto := nil;
          if ( SourceIndex >= 0 ) then
          begin
               Texto := Strings[ SourceIndex ];
               Objeto := Objects[ SourceIndex ];
          end;
          if Assigned( Evento ) then
             Evento( Self, Objeto, Texto );
          if FOk2Move then
          begin
               if Copiar then
               begin
                    if Assigned( FAlCambiarTexto ) then
                    begin
                         FAlCambiarTexto( Objeto, Texto );
                    end;
                    if CopiarObjetos then
                       Target.InsertObject( TargetIndex, Texto, Objeto )
                    else
                        Target.Insert( TargetIndex, Texto );
               end;
               if Borrar then
                  Delete( SourceIndex );
          end;
     end;
     Result := FOk2Move;
end;

procedure TZetaSmartLists.Init;
begin
     SelectDisponible( 0 );
     SelectEscogido( 0 );
end;

procedure TZetaSmartLists.SelectDisponible( Index: Integer );
begin
     if HayElementosDisponibles then
        SelectItem( Index, ListaDisponibles );
end;

procedure TZetaSmartLists.SelectEscogido( Index: Integer );
begin
     if HayElementosEscogidos then
     begin
          SelectItem( Index, ListaEscogidos );
          DoClick;
     end;
end;

procedure TZetaSmartLists.SelectItem( Index: Integer; ListBox: TListBox );
var
   i: Integer;
begin
     with ListBox do
     begin
          if ( Index >= 0 ) then
          begin
               try
                  Items.BeginUpdate;
                  if ( Index >= Items.Count ) then
                     Index := ( Items.Count - 1 );
                  if MultiSelect then
                  begin
                       for i := 0 to ( Items.Count - 1 ) do
                       begin
                            if Selected[ i ] then
                               Selected[ i ] := False;
                       end;
                       Selected[ Index ] := True;
                  end
                  else
                      ItemIndex := Index;
               finally
                      Items.EndUpdate;
               end;
          end;
     end;
end;

procedure TZetaSmartLists.TraspasaListas( Borrar, Copiar: Boolean; Source, Target: TListBox; Evento: TSmartListsEvent );
var
   i, j, k: Integer;
begin
     j := -1;
     with Source do
     begin
          try
             if Borrar then
                Items.BeginUpdate;
             if Copiar then
                Target.Items.BeginUpdate;
             if ( SelCount > 0 ) then
             begin
                  k := 0;
                  for i := ( Items.Count - 1 ) downto 0 do
                  begin
                       if Selected[ i ] then
                       begin
                            if TraspasaItem( Borrar, Copiar, i, ( Target.Items.Count - k ), Items, Target.Items, Evento ) then
                            begin
                                 k := k + 1;
                                 j := i;
                            end;
                       end;
                  end;
             end
             else
             begin
                  j := ItemIndex;
                  if ( j >= 0 ) then
                  begin
                       if not TraspasaItem( Borrar, Copiar, j, Target.Items.Count, Items, Target.Items, Evento ) then
                          j := -1;
                  end;
             end;
          finally
                 if Copiar then
                    Target.Items.EndUpdate;
                 if Borrar then
                    Items.EndUpdate;
          end;
     end;
     if ( j >= 0 ) then
     begin
          if Borrar then
             SelectItem( j, Source );
          if Copiar then
             SelectItem( Target.Items.Count, Target );
          NotifyButtons;
     end;
end;

procedure TZetaSmartLists.Subir;
begin
     if HayElementosEscogidos then
        SubirBajarEscogidos( True, FAlSubir );
end;

procedure TZetaSmartLists.Bajar;
begin
     if HayElementosEscogidos then
        SubirBajarEscogidos( False, FAlBajar );
end;

procedure TZetaSmartLists.Escoger;
begin
     if HayElementosDisponibles then
     begin
          TraspasaListas( BorrarAlCopiar, True, ListaDisponibles, ListaEscogidos, FAlEscoger );
          if ( Control <> nil ) and Control.CanFocus then
             Control.SetFocus;
          DoClick;
     end;
end;

procedure TZetaSmartLists.Rechazar;
begin
     if HayElementosEscogidos then
     begin
          TraspasaListas( True, BorrarAlCopiar, ListaEscogidos, ListaDisponibles, FAlRechazar );
          DoClick;
     end;
end;

procedure TZetaSmartLists.Refrescar;
begin
     DoEvent( ListaEscogidos.ItemIndex, ListaEscogidos.Items, FAlSeleccionar );
     NotifyButtons;
end;

procedure TZetaSmartLists.DoEvent( const Index: Integer; Lista: TStrings; Evento: TSmartListsEvent );
var
   Objeto: TObject;
   Texto: String;
begin
     if Assigned( Evento ) then
     begin
          with Lista do
          begin
               if ( Index < 0 ) then
               begin
                    Texto := '';
                    Objeto := nil;
               end
               else
               begin
                    Texto := Strings[ Index ];
                    Objeto := Objects[ Index ];
               end;
               Evento( Self, Objeto, Texto );
               if ( Index >= 0 ) then
               begin
                    Strings[ Index ] := Texto;
                    Objects[ Index ] := Objeto;
               end;
          end;
     end;
end;

procedure TZetaSmartLists.DoClick;
begin
     with ListaEscogidos do
     begin
          if Assigned( OnClick ) then;
             OnClick( Self );
     end;
end;

procedure TZetaSmartLists.NotifyButtons;
var
   i: Integer;

begin

     if HayListas then
     begin
          if ( FBotones.Count = 0 ) then
          begin
               with TForm( Owner ) do

                    for i := 0 to ( ComponentCount - 1 ) do
                    begin
                        if ( Components[ i ] is TZetaSmartListsButton)then
                        begin
                           FBotones.Add( Components[ i ] );
                           end;
                    end;
               end;
               with FBotones do
               begin
                    Pack;
                    Capacity := Count;
               end;
          end;
          with FBotones do
          begin
               for i := 0 to ( Count - 1 ) do
               begin

                TZetaSmartListsButton( Items[ i ] ).SetState;


               end;
          end;
     end;

end.

