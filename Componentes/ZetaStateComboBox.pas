unit ZetaStateComboBox;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, DB, DBCtrls,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaMessages,
     ZetaCommonTools;

type
  TStateComboBox = class(TComboBox)
  private
    { Private declarations }
    FLlaveNumerica: Boolean;
    FListaFija: ListasFijas;
    FListaVariable: ListasVariables;
    FLista: TStrings;
    FTempItems: TStrings;//acl
    FChanged: Boolean;
    FClickChange: Boolean;
    FEsconderVacios: Boolean; {acl Atributo que inicializa los combos de TipoNomina como True para esconder campos vacios.}
    FMaxItems: Integer;
    FOffset: Integer;
    FOnLookUp: TConfirmEvent;
    FOnReload: TNotifyEvent;
    function CalculaIndice( const Index: Integer ): Integer;
    function CanLoadList: Boolean;
    function ComboHasValue: Boolean;
    function Editable: Boolean;
    function GetComboText: String;                 { Basada en TDBComboBox.GetComboText }
    function GetLlave: String;
    function GetLlaves( Index: Integer ): String;
    function GetDescripcion: String;
    function GetDescripciones( Index: Integer ): String;
    function GetIndice: Integer;
    function GetValorEntero: Integer;
    procedure SetIndice( const Value: Integer );
    procedure SetLlave( const Value: String );
    procedure SetLista( Value: TStrings );
    procedure UpdateItems( Sender: TObject );
    procedure SetValorEntero(const Value: Integer);
  protected
    { Protected declarations }
    function LookUp: Boolean;
    procedure CMDialogKey( var Message: TCMDialogKey ); message CM_DIALOGKEY;
    procedure DoEnter; override; { hereda de TWinControl.DoEnter }
    procedure Change; override; { hereda de TCustomComboBox.Change }
    procedure Click; override; { hereda de TControl.Click }
    procedure DoExit; override; { hereda de TWinControl.DoExit }
    procedure Refresh;
    procedure Reload; virtual;
    procedure Reaction;
    procedure Loaded; override;
    procedure LeeListaFija; virtual;
    procedure CambiaItems; virtual;
    procedure SetComboText( const sValue: String ); { Basada en TDBComboBox.SetComboText }
    procedure SetListaFija( Value: ListasFijas ); virtual;
  public
    { Public declarations }
    property Descripcion: String read GetDescripcion;
    property Descripciones[ Index: Integer ]: String read GetDescripciones;
    property Lista: TStrings read FLista write SetLista;
    property Llave: String read GetLlave write SetLlave;
    property Indice: Integer read GetIndice write SetIndice;
    property Llaves[ Index: Integer ]: String read GetLlaves;
    property ValorEntero: Integer read GetValorEntero write SetValorEntero;
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    procedure AsignaLlave(const Value: String); { Para Cambiar Llave y llamar Lookup }
    procedure AsignaValorEntero(const Value: Integer); { Para Cambiar ValorEntero y llamar Lookup }
  published
    { Published declarations }
    property ListaFija: ListasFijas read FListaFija write SetListaFija;
    property ListaVariable: ListasVariables read FListaVariable write FListaVariable;
    property EsconderVacios: Boolean read FEsconderVacios write FEsconderVacios;//acl
    property LlaveNumerica: Boolean read FLlaveNumerica write FLlaveNumerica;
    property MaxItems: Integer read FMaxItems write FMaxItems;
    property Offset: Integer read FOffset write FOffset;
    property OnLookUp: TConfirmEvent read FOnLookUp write FOnLookUp;
    property OnReload: TNotifyEvent read FOnReload write FOnReload;
  end;
  TDBStateComboBox = class(TStateComboBox)
  private
    { Private declarations }
    FDataLink: TFieldDataLink;
    function DataLinkActive: Boolean;
    function DataLinkFieldActive: Boolean;
    function GetDataField: String;
    function GetDataSource: TDataSource;
    procedure SetDataField( const Value: string );
    procedure SetDataSource( Value: TDataSource );
    procedure DataChange( Sender: TObject );
    procedure ActiveChange( Sender: TObject );
    procedure CheckFieldType( const Value: String );
  protected
    { Protected declarations }
    procedure Notification( AComponent: TComponent; Operation: TOperation ); override;
    procedure CambiaItems; override;
    procedure Reload; override;
    procedure SetListaFija( Value: ListasFijas ); override;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
  published
    { Published declarations }
    property DataField: string read GetDataField write SetDataField;
    property DataSource: TDataSource read GetDataSource write SetDataSource;
  end;

implementation

{ ************* TStateComboBox ****************** }

constructor TStateComboBox.Create(AOwner: TComponent);
begin
     FMaxItems := 10;
     FLlaveNumerica := True;
     FEsconderVacios := False; //acl
     inherited Create( AOwner );
     FLista := TStringList.Create;
     FTempItems := TStringList.Create; //acl
     TStringList( FLista ).OnChange := UpdateItems;
     FChanged := False;
     FClickChange := False;
     {$ifdef VER150}
     AutoComplete := False;
     AutoDropDown := False;
     {$endif}

     //DevEx: New Code
     Ctl3D:= False;
     ParentCtl3D:= False;
     BevelKind := bkFlat;
end;

destructor TStateComboBox.Destroy;
begin
     TStringList( FLista ).OnChange := nil;
     FLista.Free;
     FTempItems.Free;//acl
     inherited Destroy;
end;

procedure TStateComboBox.Loaded;
begin
     inherited Loaded;
     LeeListaFija;
     DropDownCount := Items.Count;
end;

function TStateComboBox.CanLoadList: Boolean;
begin
     Result := not ( csDesigning in ComponentState );
end;

function TStateComboBox.CalculaIndice( const Index: Integer ): Integer;
begin
     Result := ( Index - Offset );
end;

function TStateComboBox.GetIndice: Integer;
begin
     Result := ItemIndex + Offset;
end;

procedure TStateComboBox.SetIndice(const Value: Integer);
var
   i: Integer;
   sValue: String;
begin
     if FLlaveNumerica then
     begin
          i := CalculaIndice( Value );
          if ( i >= 0 ) and ( i < Items.Count ) then
             sValue := Items[ i ]
          else
              sValue := '';
          SetComboText( sValue );
     end;
end;

function TStateComboBox.GetLlave: String;
begin
     if ( ItemIndex < 0 ) then
        Result := ''
     else
         Result := GetLlaves( ItemIndex );
end;

function TStateComboBox.GetLlaves( Index: Integer ): String;
begin
     Result := FLista.Names[ CalculaIndice( Index ) ];
end;

function TStateComboBox.GetDescripcion: String;
begin
     if ( ItemIndex < 0 ) then
        Result := ''
     else
         Result := GetDescripciones( ItemIndex );
end;

function TStateComboBox.GetDescripciones( Index: Integer ): String;
var
   sLlave: String;
begin
     sLlave := GetLlaves( Index );
     if ( sLlave <> '' ) then
        Result := FLista.Values[ sLlave ]
     else
         Result := '';
end;

procedure TStateComboBox.SetLlave(const Value: String);
var
   i: Integer;
begin
     with FLista do
     begin
          if ( Self.Style = csDropDownList ) then
          begin
               i := IndexOfName( Value );
               if ( i >= 0 ) then
                  ItemIndex := i;
          end
          else
              SetComboText( Values[ Value ] );
     end;
end;

procedure TStateComboBox.AsignaLlave(const Value: String);
begin
     Llave := Value;
     Lookup;
end;

procedure TStateComboBox.LeeListaFija;
var
   i, iMax: Integer;
begin
     FTempItems.Clear;
     if ( FListaFija <> lfNinguna ) and CanLoadList then
     begin
          with Lista do
          begin
               try
                  BeginUpdate;
                  Clear;
               finally
                      EndUpdate;
               end;

          ZetaCommonLists.LlenaLista( FListaFija, Items );
{acl. Si el atributo esta vacio se agregan los items de la lista a uno Tstring temporal recorres el ciclo y
almacena los items en el temporal}
              if ( EsconderVacios ) then
              begin
                   iMax:= Items.Count;
                   FTempItems.AddStrings( Items );
                   for i := 0 to ( iMax - 1 ) do
                   begin
                        if strLleno( FTempItems[i] ) then
                           Add( IntToStr(i) + '=' + FTempItems[i]  );
                   end;
              end;

          end;
     end;
end;


procedure TStateComboBox.SetListaFija( Value: ListasFijas );
begin
     FListaFija := Value;
     LeeListaFija;
end;

procedure TStateComboBox.SetLista( Value: TStrings );
begin
     if ( FListaFija = lfNinguna ) and CanLoadList then
     begin
          FLista.Assign( Value );
          UpdateItems( Self );
     end;
end;

function TStateComboBox.GetValorEntero: Integer;
begin
     Result := StrToIntDef( Text, 0 );
end;

procedure TStateComboBox.SetValorEntero(const Value: Integer);
begin
     Text := IntToStr( Value );
end;

procedure TStateComboBox.AsignaValorEntero(const Value: Integer);
begin
     ValorEntero := Value;
     Lookup;
end;

procedure TStateComboBox.CambiaItems;
var
   i: Integer;
begin
     with Items do
     begin
          try
             BeginUpdate;
             Clear;
             for i := 0 to ( FLista.Count - 1 ) do
                 Add( GetDescripciones( i ) );
             finally
                    EndUpdate;
             end;
     end;
end;

procedure TStateComboBox.UpdateItems( Sender: TObject );
begin
     CambiaItems;
end;

function TStateComboBox.Editable: Boolean;
begin
     Result := ( Style in [csDropDown, csSimple] );
end;

procedure TStateComboBox.SetComboText( const sValue: String );
var
   Redraw: Boolean;
   i: Integer;
begin
     Redraw := ( Style <> csSimple ) and HandleAllocated;
     if Redraw then
        SendMessage( Handle, WM_SETREDRAW, 0, 0 );
     try
        if ( sValue = '' ) then
           i := -1
        else
            i := Items.IndexOf( sValue );
        ItemIndex := i;
     finally
            if Redraw then
            begin
                 SendMessage( Handle, WM_SETREDRAW, 1, 0 );
                 Invalidate;
            end;
     end;
end;

function TStateComboBox.ComboHasValue: Boolean;
begin
     if Editable then
     begin
          if FLlaveNumerica then
             Result := ( StrToIntDef( Text, 0 ) <> 0 )
          else
              Result := ( Text > '' );
     end
     else
         if FLlaveNumerica then
            Result := ( ItemIndex >= 0 )
         else
             Result := ( Text > '' );
end;

function TStateComboBox.GetComboText: string;
var
   i: Integer;
begin
     if Editable then
        Result := Text
     else
     begin
          i := ItemIndex;
          if ( i < 0 ) then
             Result := ''
          else
              Result := Items[ i ];
     end;
end;

procedure TStateComboBox.DoEnter;
begin
     FChanged := False;
     SelectAll;
     inherited DoEnter;
end;

procedure TStateComboBox.Change;
begin
     inherited Change;
     if FClickChange then
     begin
          FClickChange := False;
          LookUp;
          FChanged := False;
     end
     else
         FChanged := True;
end;

procedure TStateComboBox.Click;
begin
     inherited Click;
     FClickChange := True;
end;

procedure TStateComboBox.DoExit;
begin
     inherited DoExit;
     if Editable then
        Reaction;
end;

procedure TStateComboBox.CMDialogKey( var Message: TCMDialogKey );
begin
     with Message do
     begin
          if ( CharCode = VK_RETURN ) and Focused and Editable and ( KeyDataToShiftState( Message.KeyData ) = [] ) then
          begin
               Reaction;
               SelectAll;
               Result := 1;
          end
          else
              inherited;
     end;
end;

procedure TStateComboBox.Reaction;
begin
     if FChanged then
     begin
          if LookUp then
             Refresh;
          FChanged := False;
     end;
end;

function TStateComboBox.LookUp: Boolean;
begin
     if ComboHasValue then
     begin
          Result := True;
          if Assigned( FOnLookUp ) then
             FOnLookUp( Self, Result );
     end
     else
         Result := False;
end;

procedure TStateComboBox.Reload;
begin
     if Assigned( FOnReload ) then
        FOnReload( Self );
end;

procedure TStateComboBox.Refresh;
var
   i, iPtr, iLast: Integer;
   sText: String;
begin
     if Editable then
     begin
          try
             Items.BeginUpdate;
             sText := GetComboText;
             iPtr := Items.IndexOf( sText );
             iLast := ( Items.Count - 1 );
             if ( iPtr >= 0 ) then
             begin
                  for i := ( iPtr + 1 ) to iLast do
                      Items.Strings[ i - 1 ] := Items.Strings[ i ];
                  Items.Delete( iLast );
             end;
             Items.Insert( 0, sText );
             if ( Items.Count > FMaxItems ) then
                Items.Delete( FMaxItems );
          finally
                 Items.EndUpdate;
          end;
     end;
end;

{ ************* TDBStateComboBox ****************** }

constructor TDBStateComboBox.Create(AOwner: TComponent);
begin
     inherited Create( AOwner );
     FDataLink := TFieldDataLink.Create;
     with FDataLink do
     begin
          Control := Self;
          OnDataChange := DataChange;
          OnActiveChange := ActiveChange;
     end;
     if not ( csDesigning in ComponentState ) then
        CheckFieldType( DataField );
end;

destructor TDBStateComboBox.Destroy;
begin
     with FDataLink do
     begin
          OnActiveChange := nil;
          OnDataChange := nil;
          Free;
     end;
     FDataLink := nil;
     inherited Destroy;
end;

procedure TDBStateComboBox.Notification( AComponent: TComponent; Operation: TOperation );
begin
     inherited Notification( AComponent, Operation );
     if ( Operation = opRemove ) and ( FDataLink <> nil ) and ( AComponent = DataSource ) then
        DataSource := nil;
end;

function TDBStateComboBox.GetDataField: String;
begin
     Result := FDataLink.FieldName;
end;

function TDBStateComboBox.GetDataSource: TDataSource;
begin
     Result := FDataLink.DataSource;
end;

procedure TDBStateComboBox.SetDataField( const Value: String );
begin
     CheckFieldType( Value );
     FDataLink.FieldName := Value;
end;

procedure TDBStateComboBox.SetDataSource( Value: TDataSource );
begin
     if ( FDataLink.DataSource <> Value ) then
     begin
          FDataLink.DataSource := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

procedure TDBStateComboBox.SetListaFija( Value: ListasFijas );
begin
     inherited SetListaFija( Value );
     FDataLink.OnDataChange( Self );
end;

procedure TDBStateComboBox.CambiaItems;
begin
     inherited CambiaItems;
     FDataLink.OnDataChange( Self );
end;

procedure TDBStateComboBox.Reload;
begin
     if DataLinkFieldActive then
        DataChange( Self )
     else
         inherited Reload;
end;

function TDBStateComboBox.DataLinkActive: Boolean;
begin
     Result := ( FDataLink <> nil ) and
               ( FDataLink.Dataset <> nil ) and
               ( FDataLink.Dataset.Active );
end;

function TDBStateComboBox.DataLinkFieldActive: Boolean;
begin
     Result := DataLinkActive and ( FDataLink.Field <> nil );
end;

procedure TDBStateComboBox.CheckFieldType( const Value: String );
var
   FieldType: TFieldType;
begin
     if ( Value <> '' ) and DataLinkActive then
     begin
          FieldType := FDataLink.Dataset.FieldByName( Value ).DataType;
          if LlaveNumerica then
          begin
               if not ( FieldType in [ ftInteger, ftSmallInt, ftWord ] ) then
                  raise EInvalidFieldType.Create( 'TDBStateComboBox.DataField can only ' +
                                                  'be connected to columns of type ' +
                                                  'Integer, SmallInt, Word' );
          end
          else
              if not ( FieldType in [ ftString ] ) then
                 raise EInvalidFieldType.Create( 'TDBStateComboBox.DataField can only ' +
                                                 'be connected to columns of type String' );
     end;
end;

procedure TDBStateComboBox.DataChange( Sender: TObject );
begin
     if DataLinkFieldActive then
     begin
          if Editable then
             Text := FDataLink.Field.AsString
          else
              if LlaveNumerica then
                 Indice := FDataLink.Field.AsInteger
              else
                  Llave := FDataLink.Field.AsString;
     end;
end;

procedure TDBStateComboBox.ActiveChange( Sender: TObject );
begin
     if ( FDataLink <> nil ) and FDataLink.Active then
        CheckFieldType( DataField );
end;

end.
