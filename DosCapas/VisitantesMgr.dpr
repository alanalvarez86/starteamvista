program VisitantesMGR;

uses
  Forms,
  ZetaClientTools,
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  ZetaSplash in '..\Tools\ZetaSplash.pas' {SplashScreen},
  ZBaseArbolShell in '..\Tools\ZBaseArbolShell.pas' {BaseArbolShell},
  FTressShell in '..\VisitantesMgr\FTressShell.pas' {TressShell},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DCliente in '..\VisitantesMgr\DCliente.pas' {dmCliente: TDataModule},
  DBaseGlobal in '..\DataModules\dBaseGlobal.pas',
  DBaseDiccionario in '..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DBaseReportes in '..\DataModules\DBaseReportes.pas' {dmBaseReportes: TDataModule},
  DBaseSistema in '..\DataModules\DBaseSistema.pas' {dmBaseSistema: TDataModule},
  FSistBaseEditEmpresas in '..\Sistema\FSistBaseEditEmpresas.pas' {SistBaseEditEmpresas},
  ZBaseEditAccesos in '..\Tools\ZBaseEditAccesos.pas' {ZetaEditAccesos},
  FSistBaseEditUsuarios in '..\Sistema\FSistBaseEditUsuarios.pas' {SistBaseEditUsuarios},
  FSistEditUsuarioSuscribe in '..\Sistema\FSistEditUsuarioSuscribe.pas' {FormaSuscribeUsuario},
  FEditDiccion in '..\Catalogos\FEditDiccion.pas' {EditDiccion},
  ZBaseConsulta in '..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  DConsultas in '..\DataModules\DConsultas.pas' {dmConsultas: TDataModule},
  DEntidades in '..\Evaluador\DEntidades.pas' {dmEntidades: TDataModule},
  ZBaseEdicion in '..\Tools\ZBaseEdicion.pas' {BaseEdicion},
  ZBaseGlobal in '..\Tools\ZBaseGlobal.pas' {BaseGlobal},
  ZBaseTablas in '..\Tools\ZBaseTablas.pas' {EditTablas},
  EditorDResumen in '..\Medicos\EditorDResumen.pas';

{$R *.RES}
{$R WindowsXP.res}



procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  SplashScreen := TSplashScreen.Create( Application );
  with SplashScreen do
  begin
       Show;
       Update;
  end;
  Application.Title := 'Administrador de Visitantes';
  Application.HelpFile := 'Visitantes.chm';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login( False ) then
       begin
            CierraSplash;
            Show;
            Update;
            BeforeRun;
            Application.Run;
       end
       else
       begin
            CierraSplash;
            Free;
       end;
  end;
end.

