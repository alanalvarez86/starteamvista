program TressMedico;

uses
  Forms,
  ZetaClientTools,
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  DBaseCliente in '..\DataModules\DBaseCliente.pas' {BaseCliente: TDataModule},
  ZBaseConsulta in '..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FBaseReportes in '..\Reportes\FBaseReportes.pas' {BaseReportes},
  ZReportTools in '..\Reportes\ZReportTools.pas',
  DGlobal in '..\DataModules\DGlobal.pas',
  DBaseDiccionario in '..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DEntidades in '..\Evaluador\DEntidades.pas' {dmEntidades: TDataModule},
  DDiccionario in '..\Medicos\DDiccionario.pas' {dmDiccionario: TDataModule},
  DReportes in '..\DataModules\DReportes.pas' {dmReportes: TDataModule},
  FEditReportes in '..\Reportes\FEditReportes.pas',
  FEditBaseReportes in '..\Reportes\FEditBaseReportes.pas' {EditBaseReportes},
  ZetaSplash in '..\Tools\ZetaSplash.pas' {SplashScreen},
  ZBaseEdicion in '..\Tools\ZBaseEdicion.pas' {BaseEdicion},
  DTablas in '..\Medicos\DTablas.pas' {dmTablas: TDataModule},
  ZBaseEdicionRenglon in '..\Tools\ZBaseEdicionRenglon.pas' {BaseEdicionRenglon},
  ZBaseConsultaBotones in '..\Tools\ZBaseConsultaBotones.pas' {BaseBotones},
  ZWizardBasico in '..\Tools\ZWizardBasico.pas' {WizardBasico},
  ZetaDespConsulta in '..\Medicos\ZetaDespConsulta.pas',
  ZArbolTress in '..\Medicos\ZArbolTress.pas',
  ZBaseWizard in '..\Tools\ZBaseWizard.pas' {BaseWizard},
  ZBaseWizardFiltro in '..\Tools\ZBaseWizardFiltro.pas' {BaseWizardFiltro},
  ZBaseThreads in '..\Tools\ZBaseThreads.pas',
  DConsultas in '..\DataModules\DConsultas.pas' {dmConsultas: TDataModule},
  ZBaseGlobal in '..\Tools\ZBaseGlobal.pas' {BaseGlobal},
  DCatalogos in '..\Medicos\DCatalogos.pas' {dmCatalogos: TDataModule},
  DCliente in '..\Medicos\DCliente.pas' {dmCliente: TDataModule},
  DSistema in '..\Medicos\DSistema.pas' {dmSistema: TDataModule},
  ZBaseArbolShell in '..\Tools\ZBaseArbolShell.pas' {BaseArbolShell},
  DBaseReportes in '..\DataModules\DBaseReportes.pas' {dmBaseReportes: TDataModule},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DBaseGlobal in '..\DataModules\dBaseGlobal.pas',
  FTressShell in '..\Medicos\FTressShell.pas' {TressShell},
  FEditExpediente in '..\Medicos\FEditExpediente.pas' {EditExpediente},
  DMailMerge in '..\datamodules\dmailmerge.pas' {dmMailMerge: TDataModule},
  DMedico in '..\Medicos\DMedico.pas' {dmMedico: TDataModule},
  DConfigPoliza in '..\DataModules\DConfigPoliza.pas' {dmConfigPoliza: TDataModule},
  DExportEngine in '..\DataModules\DExportEngine.pas' {dmExportEngine: TDataModule},
  dBaseTressDiccionario in '..\DataModules\dBaseTressDiccionario.pas' {dmBaseTressDiccionario: TDataModule},
  DBaseSistema in '..\DataModules\DBaseSistema.pas' {dmBaseSistema: TDataModule};

{$R *.RES}
{$R WindowsXP.res}

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  SplashScreen := TSplashScreen.Create( Application );
  with SplashScreen do
  begin
       Show;
       Update;
  end;
  Application.Title := 'Servicio M�dico';
  Application.HelpFile := 'TressMedico.chm';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login( False ) then
       begin
            CierraSplash;
            Show;
            Update;
            BeforeRun;
            Application.Run;
       end
       else
       begin
            CierraSplash;
            Free;
       end;
  end;

end.
