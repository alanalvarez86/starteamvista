program VisitantesDisena;

uses
  Forms,
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  FTressShell in '..\Designer\FTressShell.pas' {TressShell},
  ZReportTools in '..\Reportes\ZReportTools.pas',
  ZetaCommonClasses in '..\Componentes\ZetaCommonClasses.pas',
  ZetaCommonLists in '..\Componentes\ZetaCommonLists.pas',
  ZetaCommonTools in '..\Componentes\ZetaCommonTools.pas',
  DBaseDiccionario in '..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  ZetaSplash in '..\Tools\ZetaSplash.pas' {SplashScreen},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  DBaseGlobal in '..\DataModules\dBaseGlobal.pas',
  DGlobal in '..\VisitantesMGR\DGlobal.pas',
  DCliente in '..\Designer\dcliente.pas' {dmCliente: TDataModule},
  ZCreator in '..\VisitantesMGR\ZCreator.pas',
  DCatalogos in '..\Visitantes\DCatalogos.pas' {dmCatalogos: TDataModule},
  EditorDResumen in '..\Medicos\EditorDResumen.pas',
  DDiccionario in '..\Designer\Visitantes\DDiccionario.pas' {dmDiccionario: TDataModule};

{$R *.RES}
procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;

begin
     Application.Initialize;
     SplashScreen := TSplashScreen.Create( Application );
     with SplashScreen do
     begin
          Show;
          Update;
     end;
     Application.Title := 'Dise�ador';
  Application.HelpFile := 'Tress.chm';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
     begin
          if Login( TRUE ) then
          begin
               CierraSplash;
               Show;
               Update;
               BeforeRun;
               if ( ParamCount > 0 ) then
                  DesignerRunBatch
               else
                   DesignerRun;
          end
          else
          begin
               CierraSplash;
               Free
          end;
     end;
     Application.Run;
end.
