program Visitantes;

uses
  Forms,
  ZetaClientTools,
  FTressShell in '..\Visitantes\FTressShell.pas' {TressShell},
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FBaseBusquedas in '..\VisitantesMGR\FBaseBusquedas.pas' {BaseBusquedas},
  FBuscaAnfitriones in '..\VisitantesMGR\FBuscaAnfitriones.pas' {BuscaAnfitriones},
  FBuscaVisitantes in '..\VisitantesMGR\FBuscaVisitantes.pas' {BuscaVisitantes},
  ZAccesosTress in '..\VisitantesMGR\ZAccesosTress.pas',
  FEditLibros in '..\Visitantes\FEditLibros.pas' {EditLibros},
  FBusquedaLibro in '..\Visitantes\FBusquedaLibro.pas' {BusquedaLibro},
  FFolioSalida in '..\Visitantes\FFolioSalida.pas' {FolioSalida},
  FGridSalidas in '..\Visitantes\FGridSalidas.pas' {GridSalidas},
  ZetaTipoEntidad in '..\VisitantesMGR\ZetaTipoEntidad.pas',
  DReportes in '..\VisitantesMGR\DReportes.pas' {dmReportes: TDataModule},
  DEntidadesTress in '..\VisitantesMGR\DEntidadesTress.pas' {dmEntidadesTress: TDataModule},
  FConfigura in '..\Visitantes\FConfigura.pas' {Configura},
  FEscogeCaseta in '..\Visitantes\FEscogeCaseta.pas' {EscogeCaseta},
  ZGlobalTress in '..\VisitantesMGR\ZGlobalTress.pas',
  FCierraCorte in '..\Visitantes\FCierraCorte.pas' {CierraCorte},
  ZWizardBasico in '..\Tools\ZWizardBasico.pas' {WizardBasico},
  FAutoRegistroEntrada in '..\Visitantes\FAutoRegistroEntrada.pas' {AutoRegistroEntrada},
  ZetaSplash in '..\Tools\ZetaSplash.pas' {SplashScreen},
  FAlerta in '..\Visitantes\FAlerta.pas' {Alerta},
  EditorDResumen in '..\Medicos\EditorDResumen.pas';

{$R *.RES}
{$R WindowsXP.res}

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  SplashScreen := TSplashScreen.Create( Application );
  with SplashScreen do
  begin
       Show;
       Update;
  end;
  Application.Title := 'Visitantes';
  Application.HelpFile := 'Visitantes.chm';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login( False ) then
       begin
            CierraSplash;
            Show;
            Update;
            BeforeRun;
            Application.Run;
       end
       else
       begin
            CierraSplash;
            Free;
       end;
  end;
end.

