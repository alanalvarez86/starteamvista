program ConciliadorIMSS;

uses
  Forms,
  ZetaSplash,
  ZetaClientTools,
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  FTressShell in '..\IMSS\ConciliadorIMSS\FTressShell.pas' {TressShell},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  DConciliador in '..\IMSS\ConciliadorIMSS\DConciliador.pas' {dmConciliador: TDataModule};

{$R *.RES}
{$R WindowsXP.res}

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  SplashScreen := TSplashScreen.Create( Application );
  with SplashScreen do
  begin
       Show;
       Update;
  end;
  Application.Title := 'Conciliador Tress';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login( True ) then
       begin
            CierraSplash;
            Show;
            Update;
            BeforeRun;
            Application.Run;
       end
       else
       begin
            CierraSplash;
       end;
       Free;
  end;
end.
