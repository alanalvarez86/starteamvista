program TressEmail;

uses
  Forms,
  SysUtils,
  ZetaClientTools,
  ZetaSplash,
  FReportesMailCFG in '..\Email\FReportesMailCFG.pas' {ReportesMailTest},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  DReportesGenerador in '..\DataModules\DReportesGenerador.pas' {dmReportGenerator: TDataModule},
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell};

{$R *.RES}
{$R WindowsXP.res}

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;

begin
     ZetaClientTools.InitDCOM;
     Application.Initialize;
     Application.Title := 'Reportes E-Mail';
     Application.HelpFile := 'TressEmail.chm';
  Application.CreateForm(TReportesMailTest, ReportesMailTest);
  if ( ParamCount > 0 ) then
     begin
          try
             ReportesMailTest.ProcesarBatch
          finally
                 FreeAndNil( ReportesMailTest );
          end
     end
     else
     begin
          SplashScreen := TSplashScreen.Create( Application );
          with SplashScreen do
          begin
               Show;
               Update;
          end;
          with ReportesMailTest do
          begin
               if Login( False ) then
               begin
                    CierraSplash;
                    Show;
                    Update;
                    BeforeRun;
                    Application.Run;
               end
               else
               begin
                    CierraSplash;
                    Free;
               end;
          end;
     end;
end.
