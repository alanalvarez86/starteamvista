program SeleccionEmail;

uses
  Forms,
  SysUtils,
  ZetaClientTools,
  ZetaSplash,
  FReportesMailCFG in '..\Email\FReportesMailCFG.pas' {ReportesMailTest},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  DReportesGenerador in '..\DataModules\DReportesGenerador.pas' {dmReportGenerator: TDataModule},
  ZetaTipoEntidad in '..\Seleccion\ZetaTipoEntidad.pas',
  ZGlobalTress in '..\Seleccion\ZGlobalTress.pas',
  ZReportModulo in '..\Seleccion\ZReportModulo.pas',
  ZBaseCreator in '..\Servidor\ZBaseCreator.pas' {ZetaBaseCreator: TDataModule},
  ZCreator in '..\Seleccion\ZCreator.pas' {ZetaCreator: TDataModule},
  DEntidades in '..\Evaluador\DEntidades.pas' {dmEntidades: TDataModule},
  DEntidadesTress in '..\Seleccion\DEntidadesTress.pas' {dmEntidadesTress: TDataModule},
  DBaseExisteCampo in '..\Evaluador\DBaseExisteCampo.pas' {dmBaseExisteCampo: TDataModule},
  DExisteCampo in '..\Seleccion\DExisteCampo.pas' {dmExisteCampo: TDataModule},
  DGlobal in '..\Seleccion\DGlobal.pas',
  ZetaAdicionalMgr in '..\Seleccion\ZetaAdicionalMgr.pas',
  DBaseDiccionario in '..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DBaseSeleccionDiccionario in '..\Seleccion\DBaseSeleccionDiccionario.pas' {dmBaseSeleccionDiccionario: TDataModule},
  DDiccionario in '..\Email\Seleccion\DDiccionario.pas' {dmDiccionario: TDataModule},
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell};

{$R *.RES}
{$R WindowsXP.res}

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;
begin
     ZetaClientTools.InitDCOM;
     Application.Initialize;
     Application.Title := 'Reportes E-Mail';
     Application.CreateForm(TReportesMailTest, ReportesMailTest);
  if ( ParamCount > 0 ) then
        begin
             try
                ReportesMailTest.ProcesarBatch
             finally
                    FreeAndNil( ReportesMailTest );
             end
        end
     else
        begin
             SplashScreen := TSplashScreen.Create( Application );
             with SplashScreen do
             begin
                  Show;
                  Update;
             end;
             with ReportesMailTest do
             begin
                  if Login( False ) then
                  begin
                       CierraSplash;
                       Show;
                       Update;
                       BeforeRun;
                       Application.Run;
                  end
                  else
                  begin
                       CierraSplash;
                       Free;
                  end;
             end;
        end;
end.
