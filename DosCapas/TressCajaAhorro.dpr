program TressCajaAhorro;

uses
  Forms,
  ZetaClientTools,
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  ZBaseConsulta in '..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZetaSplash in '..\Tools\ZetaSplash.pas' {SplashScreen},
  DDiccionario in '..\CajaAhorro\DDiccionario.pas' {dmDiccionario: TDataModule},
  DGlobal in '..\DataModules\DGlobal.pas',
  DReportes in '..\DataModules\DReportes.pas' {dmReportes: TDataModule},
  ZBaseConsultaBotones in '..\Tools\ZBaseConsultaBotones.pas' {BaseBotones},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  ZBaseEdicion in '..\Tools\ZBaseEdicion.pas' {BaseEdicion},
  ZBaseGlobal in '..\Tools\ZBaseGlobal.pas' {BaseGlobal},
  ZBaseEscogeGrid in '..\Tools\ZBaseEscogeGrid.pas' {BaseEscogeGrid},
  ZWizardBasico in '..\Tools\ZWizardBasico.pas' {WizardBasico},
  ZBaseGridEdicion in '..\Tools\ZBaseGridEdicion.pas' {BaseGridEdicion},
  ZBaseWizard in '..\Tools\ZBaseWizard.pas' {BaseWizard},
  ZBaseWizardFiltro in '..\Tools\ZBaseWizardFiltro.pas' {BaseWizardFiltro},
  ZBaseSelectGrid in '..\Tools\ZBaseSelectGrid.pas' {BaseGridSelect},
  FTressShell in '..\CajaAhorro\FTressShell.pas' {TressShell},
  ZetaWizardFeedBack in '..\Tools\ZetaWizardFeedBack.pas' {WizardFeedback},
  ZetaBuscaEntero in '..\Tools\ZetaBuscaEntero.pas' {BuscaEntero},
  DBaseCliente in '..\DataModules\DBaseCliente.pas' {BaseCliente: TDataModule},
  ZBaseArbolShell in '..\Tools\ZBaseArbolShell.pas' {BaseArbolShell},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  FBaseReportes in '..\Reportes\FBaseReportes.pas' {BaseReportes},
  FEditBaseReportes in '..\Reportes\FEditBaseReportes.pas' {EditBaseReportes},
  FEditReportes in '..\Reportes\FEditReportes.pas' {EditReportes},
  DBaseDiccionario in '..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DBaseReportes in '..\DataModules\DBaseReportes.pas' {dmBaseReportes: TDataModule},
  DBaseGlobal in '..\DataModules\dBaseGlobal.pas',
  DCajaAhorro in '..\CajaAhorro\DCajaAhorro.pas' {dmCajaAhorro: TDataModule},
  DCliente in '..\CajaAhorro\DCliente.pas' {dmCliente: TDataModule},
  FEditCtasBancarias in '..\CajaAhorro\FEditCtasBancarias.pas' {EditCtasBancarias},
  FRegistroDepositoRetiro in '..\CajaAhorro\FRegistroDepositoRetiro.pas' {RegistroDepositoRetiro},
  FRegistroInscripcion in '..\CajaAhorro\FRegistroInscripcion.pas' {RegistroInscripcion},
  FSaldosEmpleado in '..\CajaAhorro\FSaldosEmpleado.pas' {SaldosEmpleado},
  ZBaseEdicionRenglon in '..\Tools\ZBaseEdicionRenglon.pas' {BaseEdicionRenglon},
  DSistema in '..\DataModules\DSistema.pas' {dmSistema: TDataModule},
  DTablas in '..\DataModules\DTablas.pas' {dmTablas: TDataModule},
  DCatalogos in '..\CajaAhorro\DCatalogos.pas' {dmCatalogos: TDataModule};

{$R *.RES}
{$R WindowsXP.res}

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  SplashScreen := TSplashScreen.Create( Application );
  with SplashScreen do
  begin
       Show;
       Update;
  end;
  Application.Title := 'Tress Caja de Ahorro';
  Application.HelpFile := 'TressCajadeAhorro.chm';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login( False ) then
       begin
            CierraSplash;
            Show;
            Update;
            BeforeRun;
            Application.Run;
       end
       else
       begin
            CierraSplash;
       end;
       Free;
  end;
end.
