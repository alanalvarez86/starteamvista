program TressKiosko;

uses
  Forms,
  ZetaClientTools,
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FPortada in '..\Portal\Kiosko\FPortada.pas' {Portada},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  FFormaBase in '..\Portal\Kiosko\FFormaBase.pas' {FormaBase},
  FFormaBasePeriodo in '..\Portal\Kiosko\FFormaBasePeriodo.pas' {FormaBasePeriodo},
  FFormaEmpleado in '..\Portal\Kiosko\FFormaEmpleado.pas' {FormaEmpleado},
  DBaseDiccionario in '..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DBaseGlobal in '..\DataModules\dBaseGlobal.pas',
  DExportEngine in '..\DataModules\DExportEngine.pas' {dmExportEngine: TDataModule},
  DGlobal in '..\DataModules\DGlobal.pas',
  DReportesGenerador in '..\DataModules\DReportesGenerador.pas' {dmReportGenerator: TDataModule},
  dBaseTressDiccionario in '..\DataModules\dBaseTressDiccionario.pas' {dmBaseTressDiccionario: TDataModule};

{$R *.RES}
{$R WindowsXP.res}

var
   sEmpleado: String;
begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  Application.Title := 'Tress Kiosko';
  if FPortada.EntrarDirecto( sEmpleado ) then
  begin
       Application.CreateForm(TFormaEmpleado, FormaEmpleado);
  with FormaEmpleado do
       begin
            EsShow := True;
            ShowEmpleado( sEmpleado );
       end;
  end
  else
  begin
       Application.CreateForm(TPortada, Portada);
  end;
  Application.Run;
end.
