program VisitantesEmail;

uses
  Forms,
  SysUtils,
  ZetaClientTools,
  ZetaSplash,
  FReportesMailCFG in '..\Email\FReportesMailCFG.pas' {ReportesMailTest},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  DReportesGenerador in '..\Datamodules\DReportesGenerador.pas' {dmReportGenerator: TDataModule},
  DEntidades in '..\Evaluador\DEntidades.pas' {dmEntidades: TDataModule},
  DExportEngine in '..\DataModules\DExportEngine.pas' {dmExportEngine: TDataModule},
  ZetaAdicionalMgr in '..\Seleccion\ZetaAdicionalMgr.pas',
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  ZetaTipoEntidad in '..\VisitantesMgr\ZetaTipoEntidad.pas',
  ZGlobalTress in '..\VisitantesMgr\ZGlobalTress.pas',
  ZReportModulo in '..\VisitantesMgr\ZReportModulo.pas',
  DEntidadesTress in '..\VisitantesMgr\DEntidadesTress.pas' {dmEntidadesTress: TDataModule},
  DGlobal in '..\VisitantesMgr\DGlobal.pas',
  DBaseDiccionario in '..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DDiccionario in '..\Email\Visitantes\DDiccionario.pas' {dmDiccionario: TDataModule},
  ZBaseCreator in '..\Servidor\ZBaseCreator.pas' {ZetaBaseCreator: TDataModule},
  ZCreator in '..\VisitantesMgr\ZCreator.pas' {ZetaCreator: TDataModule},
  DBaseExisteCampo in '..\Evaluador\DBaseExisteCampo.pas' {dmBaseExisteCampo: TDataModule},
  DExisteCampo in '..\VisitantesMgr\DExisteCampo.pas' {dmExisteCampo: TDataModule};

{$R *.RES}
{$R WindowsXP.res}

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;
begin
     ZetaClientTools.InitDCOM;
     Application.Initialize;
     Application.Title := 'Visitantes E-Mail  ';
  Application.HelpFile := 'TressEmail.chm';
  Application.CreateForm(TReportesMailTest, ReportesMailTest);
  if ( ParamCount > 0 ) then
     begin
          try
             ReportesMailTest.ProcesarBatch
          finally
                 FreeAndNil( ReportesMailTest );
          end
     end
     else
     begin
          SplashScreen := TSplashScreen.Create( Application );
          with SplashScreen do
          begin
               Show;
               Update;
          end;
          with ReportesMailTest do
          begin
               if Login( False ) then
               begin
                    CierraSplash;
                    Show;
                    Update;
                    BeforeRun;
                    Application.Run;
               end
               else
               begin
                    CierraSplash;
                    Free;
               end;
          end;
     end;
end.
