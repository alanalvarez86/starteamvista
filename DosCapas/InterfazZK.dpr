program InterfazZK;

uses
  Forms,
  ComObj,
  ActiveX,
  DInterfase in '..\Linx.5\ZKSoftware\InterfazZK\DInterfase.pas' {dmInterfase: TDataModule},
  FTressShell in '..\Linx.5\ZKSoftware\InterfazZK\FTressShell.pas' {TressShell},
  ZetaSplash in '..\Tools\ZetaSplash.pas' {SplashScreen},
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  ZBaseImportaShell in '..\Tools\ZBaseImportaShell.pas' {BaseImportaShell},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FConfTerminales in '..\Linx.5\ZKSoftware\InterfazZK\FConfTerminales.pas' {ConfTerminales};

{$R *.RES}

procedure MuestraSplash;
begin
     SplashScreen := TSplashScreen.Create( Application );
     with SplashScreen do
     begin
          Show;
          Update;
     end;
end;

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;

begin
     ComObj.CoInitFlags := COINIT_APARTMENTTHREADED;
     Application.Initialize;
     if ( ParamCount = 0 ) then
        MuestraSplash;
     Application.Title := 'Interfaz ZK';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
     begin
          if ( ParamCount = 0 ) then
          begin
               if Login( False ) then
               begin
                    CierraSplash;
                    Show;
                    Update;
                    BeforeRun;
                    Application.Run;
               end
               else
               begin
                    CierraSplash;
                    Free;
               end;
          end
          else
          begin
               try
                  ProcesarBatch;
               finally
                  Free;
               end;
          end;
     end;
end.
