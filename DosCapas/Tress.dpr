program Tress;

uses
  Forms,
  ZetaClientTools,
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  ZBaseConsulta in '..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZetaSplash in '..\Tools\ZetaSplash.pas' {SplashScreen},
  DCafeteria in '..\DataModules\DCafeteria.pas' {dmCafeteria: TDataModule},
  dCatalogos in '..\DataModules\dCatalogos.pas' {dmCatalogos: TDataModule},
  dAsistencia in '..\DataModules\dAsistencia.pas' {dmAsistencia: TDataModule},
  DDiccionario in '..\DataModules\DDiccionario.pas' {dmDiccionario: TDataModule},
  DGlobal in '..\DataModules\DGlobal.pas',
  dIMSS in '..\DataModules\dIMSS.pas' {dmIMSS: TDataModule},
  DNomina in '..\DataModules\DNomina.pas' {dmNomina: TDataModule},
  dRecursos in '..\DataModules\dRecursos.pas' {dmRecursos: TDataModule},
  DReportes in '..\DataModules\DReportes.pas' {dmReportes: TDataModule},
  dSistema in '..\DataModules\dSistema.pas' {dmSistema: TDataModule},
  dTablas in '..\DataModules\dTablas.pas' {dmTablas: TDataModule},
  ZBaseConsultaBotones in '..\Tools\ZBaseConsultaBotones.pas' {BaseBotones},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  ZBaseEdicion in '..\Tools\ZBaseEdicion.pas' {BaseEdicion},
  ZBaseGlobal in '..\Tools\ZBaseGlobal.pas' {BaseGlobal},
  ZBaseEscogeGrid in '..\Tools\ZBaseEscogeGrid.pas' {BaseEscogeGrid},
  ZWizardBasico in '..\Tools\ZWizardBasico.pas' {WizardBasico},
  ZBaseGridEdicion in '..\Tools\ZBaseGridEdicion.pas' {BaseGridEdicion},
  ZBaseWizard in '..\Tools\ZBaseWizard.pas' {BaseWizard},
  ZBaseWizardFiltro in '..\Tools\ZBaseWizardFiltro.pas' {BaseWizardFiltro},
  FWizNomBase in '..\Wizards\FWizNomBase.pas' {WizNomBase},
  FWizIMSSBase in '..\Wizards\FWizIMSSBase.pas' {WizIMSSBase},
  ZBaseSelectGrid in '..\Tools\ZBaseSelectGrid.pas' {BaseGridSelect},
  FTressShell in '..\Tress\FTressShell.pas' {TressShell},
  ZetaWizardFeedBack in '..\Tools\ZetaWizardFeedBack.pas' {WizardFeedback},
  ZetaBuscaEntero in '..\Tools\ZetaBuscaEntero.pas' {BuscaEntero},
  DBaseCliente in '..\DataModules\DBaseCliente.pas' {BaseCliente: TDataModule},
  DCliente in '..\DataModules\DCliente.pas' {dmCliente: TDataModule},
  ZBaseArbolShell in '..\Tools\ZBaseArbolShell.pas' {BaseArbolShell},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DBaseSistema in '..\DataModules\DBaseSistema.pas' {dmBaseSistema: TDataModule},
  FBaseReportes in '..\Reportes\FBaseReportes.pas' {BaseReportes},
  FEditBaseReportes in '..\Reportes\FEditBaseReportes.pas' {EditBaseReportes},
  FEditReportes in '..\Reportes\FEditReportes.pas' {EditReportes},
  DBaseDiccionario in '..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DBaseReportes in '..\DataModules\DBaseReportes.pas' {dmBaseReportes: TDataModule},
  DBaseGlobal in '..\DataModules\dBaseGlobal.pas',
  DMailMerge in '..\DataModules\dmailmerge.pas' {dmMailMerge: TDataModule};

{$R *.RES}
{$R WindowsXP.res}

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  SplashScreen := TSplashScreen.Create( Application );
  with SplashScreen do
  begin
       Show;
       Update;
  end;
  Application.Title := 'Tress';
  Application.HelpFile := 'Tress.chm';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login( False ) then
       begin
            CierraSplash;
            Show;
            Update;
            BeforeRun;
            Application.Run;
       end
       else
       begin
            CierraSplash;
       end;
       Free;
  end;
end.
