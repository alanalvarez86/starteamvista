program InterfazSy;

uses
  Forms,
  ComObj,
  ActiveX,
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  ZetaSplash in '..\Tools\ZetaSplash.pas' {SplashScreen},
  ZBaseImportaShell in '..\Tools\ZBaseImportaShell.pas' {BaseImportaShell},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  ZBaseConsulta in '..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZBaseEdicion in '..\Tools\ZBaseEdicion.pas' {BaseEdicion},
  FTressShell in '..\Linx.5\InterfazSy\FTressShell.pas' {TressShell},
  FGeneral in '..\Linx.5\InterfazSy\FGeneral.pas' {General},
  FEditAlarmas in '..\Linx.5\InterfazSy\FEditAlarmas.pas' {EditAlarmas},
  FAlarmas in '..\Linx.5\InterfazSy\FAlarmas.pas' {Alarmas},
  DInterfase in '..\Linx.5\InterfazSy\DInterfase.pas' {dmInterfase: TDataModule};

{$R *.RES}

procedure MuestraSplash;
begin
     SplashScreen := TSplashScreen.Create( Application );
     with SplashScreen do
     begin
          Show;
          Update;
     end;
end;

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;

begin
     ComObj.CoInitFlags := COINIT_APARTMENTTHREADED;
     Application.Initialize;
     if ( ParamCount = 0 ) then
        MuestraSplash;
     Application.Title := 'InterfazSy';
  Application.CreateForm(TTressShell, TressShell);
  Application.CreateForm(TGeneral, General);
  Application.CreateForm(TEditAlarmas, EditAlarmas);
  Application.CreateForm(TAlarmas, Alarmas);
  Application.CreateForm(TdmInterfase, dmInterfase);
  with TressShell do
     begin
          if ( ParamCount = 0 ) then
          begin
               if Login( False ) then
               begin
                    CierraSplash;
                    Show;
                    Update;
                    BeforeRun;
                    Application.Run;
               end
               else
               begin
                    CierraSplash;
                    Free;
               end;
          end
          else
          begin
               try
                  ProcesarBatch;
               finally
                  Free;
               end;
          end;
     end;
end.
