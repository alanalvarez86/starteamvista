program L7Poll;

uses
  Forms,
  SysUtils,
  ZetaClientTools,
  FLinxBase in '..\Linx.5\LinxBase\FLinxBase.pas' {LinxBase},
  FAcercaDe in '..\Linx.5\LinxBase\FAcercaDe.pas' {AcercaDe},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FLinx7Poll in '..\Linx.5\L7Poll\FLinx7Poll.pas' {Linx7Poll},
  DPoll in '..\Linx.5\LinxBase\DPoll.pas' {dmPoll: TDataModule};

{$R *.RES}
{$R WindowsXP.res}

begin
     ZetaClientTools.InitDCOM;
     Application.Initialize;
     Application.Title := 'L7Poll';
  Application.HelpFile := '';
  Application.CreateForm(TLinx7Poll, Linx7Poll);
  case ParamCount of
          1:
          begin
               Linx7Poll.ProcesaParametro( UpperCase( Trim( ParamStr( 1 ) ) ) );
               FreeAndNil( Linx7Poll );
          end;
          2:
          begin
               Linx7Poll.ProcesaParametro( UpperCase( Trim( ParamStr( 1 ) ) ), ParamStr( 2 ) );
               FreeAndNil( Linx7Poll );
          end;
     end;
     Application.Run;
end.
