program ConciliadorFonacot;

uses
  Forms,
  ZetaSplash,
  ZetaClientTools,
  FTressShell in '..\Nominas\ConciliadorFonacot\FTressShell.pas' {TressShell},
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  DConcilia in '..\Nominas\ConciliadorFonacot\DConcilia.pas' {dmConcilia: TDataModule},
  DCliente in '..\Nominas\ConciliadorFonacot\DCliente.pas',
  DBaseCliente in '..\DataModules\DBaseCliente.pas' {BaseCliente: TDataModule};

{$R *.res}
{$R WindowsXP.res}

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  SplashScreen := TSplashScreen.Create( Application );
  with SplashScreen do
  begin
       Show;
       Update;
  end;
  Application.Title := 'Conciliador Fonacot';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login( True ) then
       begin
            CierraSplash;
            Show;
            Update;
            BeforeRun;
            Application.Run;
       end
       else
       begin
            CierraSplash;
       end;
       Free;
  end;
end.

