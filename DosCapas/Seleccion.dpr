program Seleccion;

uses
  Forms,
  ZetaClientTools,
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  FTressShell in '..\Seleccion\FTressShell.pas' {TressShell},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  ZBaseConsulta in '..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  DBaseDiccionario in '..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DBaseReportes in '..\DataModules\DBaseReportes.pas' {dmBaseReportes: TDataModule},
  ZetaSplash in '..\Tools\ZetaSplash.pas' {SplashScreen},
  ZBaseEdicion in '..\Tools\ZBaseEdicion.pas' {BaseEdicion},
  ZBaseWizard in '..\Tools\ZBaseWizard.pas' {BaseWizard},
  ZBaseGlobal in '..\Tools\ZBaseGlobal.pas' {BaseGlobal},
  ZBaseArbolShell in '..\Tools\ZBaseArbolShell.pas' {BaseArbolShell},
  DConsultas in '..\DataModules\DConsultas.pas' {dmConsultas: TDataModule},
  DBaseGlobal in '..\DataModules\dBaseGlobal.pas',
  ZBaseGridEdicion in '..\Tools\ZBaseGridEdicion.pas' {BaseGridEdicion},
  DBaseSistema in '..\DataModules\DBaseSistema.pas' {dmBaseSistema: TDataModule},
  FBuscaRequisicion in '..\Seleccion\FBuscaRequisicion.pas' {BuscaRequisicion},
  DSeleccion in '..\Seleccion\dseleccion.pas' {dmSeleccion: TDataModule},
  DCatalogos in '..\Seleccion\DCatalogos.pas' {dmCatalogos: TDataModule},
  ZBaseEdicionRenglon in '..\Tools\ZBaseEdicionRenglon.pas' {BaseEdicionRenglon},
  DMailMerge in '..\datamodules\dmailmerge.pas' {dmMailMerge: TDataModule},
  DExportEngine in '..\DataModules\DExportEngine.pas' {dmExportEngine: TDataModule};

{$R *.RES}
{$R WindowsXP.res}

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  SplashScreen := TSplashScreen.Create( Application );
  with SplashScreen do
  begin
       Show;
       Update;
  end;
  Application.Title := 'Selecci�n de Personal';
  Application.HelpFile := 'Seleccion.chm';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login( False ) then
       begin
            CierraSplash;
            RevisaParams;
            if ( not OnlySolicita ) and ( not OnlyRequiere ) then
               Show;
            Update;
            BeforeRun;
            if OnlySolicita then
               CicloRegistroSolicita
            else if OnlyRequiere then
               CicloRegistroRequiere
            else
               Application.Run;
       end
       else
       begin
            CierraSplash;
            Free;
       end;
  end;


end.
