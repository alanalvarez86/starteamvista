unit DebugUnit;

// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Prop�sito..........: Unidad para guardar los mensajes de depuraci�n del servicio
// Notas adicionales..: Fuente: http://delphi.jmrds.com/?q=node/37
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

interface

type
  eIndent = (iNone, iAdd, iDelete);

procedure DebugMsg(const Message: String; Indent: eIndent = iNone);

implementation

uses
  Windows, SysUtils;

var
  Spacing: string;

// Fuente: http://edn.embarcadero.com/article/40404
function WriteEventLog(AEntry: string; AServer: string = ''; ASource: string = 'ELog';
  AEventType: word = EVENTLOG_INFORMATION_TYPE; AEventId: word = 0; AEventCategory: word = 0): Boolean;
var
  EventLog: integer;
  P       : Pointer;
begin
  Result := False;
  P      := PWideChar(AEntry);
  if Length(AServer) = 0 then // Write to the local machine
    EventLog := RegisterEventSourceA(nil, PAnsiChar(ASource))
  else // Write to a remote machine
    EventLog := RegisterEventSourceA(PAnsiChar(AServer), PAnsiChar(ASource));
  if EventLog <> 0 then
    try
      ReportEvent(EventLog, // event log handle
        AEventType,         // event type
        AEventCategory,     // category zero
        AEventId,           // event identifier
        nil,                // no user security identifier
        1,                  // one substitution string
        0,                  // no data
        @P,                 // pointer to string array
        nil);               // pointer to data
      Result := True;
    finally
      DeregisterEventSource(EventLog);
    end;
end;

procedure Log(const Message: String; FileName: string = '');
const
  SEP = Chr(9);
var
  F        : TextFile;
  Mutex    : THandle;
  SearchRec: TSearchRec;
  Msg      : string;
begin
  // Hora + SEPARADOR + ID de proceso + SEPARADOR + Mensaje
  Msg := FormatDateTime('[dd/mmm/yyyy hh:nn:ss.zzz]', Now) + SEP +
         'PID=' + IntToStr(GetCurrentProcessId) + SEP +
         Message;
  if FileName = '' then
    Filename := ChangeFileExt(ParamStr(0), '_debug.log');
    //Filename := 'C:\' + ChangeFileExt(ExtractFileName(ParamStr(0)), '_debug.log');
  Mutex := CreateMutex(nil, False, PChar(StringReplace(Filename, '\', '/', [rfReplaceAll])));
  if Mutex <> 0 then begin
    WaitForSingleObject(Mutex, INFINITE);
    try
      if FindFirst(Filename, faAnyFile, SearchRec) = 0 then begin
        if SearchRec.Size > (1024 * 1024) then
          MoveFileEx(PChar(Filename), PChar(Filename + '.1'), MOVEFILE_REPLACE_EXISTING);
        FindClose(SearchRec);
      end;
      try
        AssignFile(F, Filename);
        {$I-}
        Append(F);
        if IOResult <> 0 then
          Rewrite(F);
        {$I+}
        if IOResult = 0 then begin
          Writeln(F, Msg);
          CloseFile(F);
        end;
      except
        on e: Exception do
          WriteEventLog('DebugMsg: ' + e.Message, '', ParamStr(0) + '->DebugUnit.pas', EVENTLOG_ERROR_TYPE);
      end;
    finally
      ReleaseMutex(Mutex);
      CloseHandle(Mutex);
    end;
  end;
end;

procedure DebugMsg(const Message: String; Indent: eIndent = iNone);
begin
  if Indent = iDelete then
    Delete(Spacing, 1, 2);
  Log(Spacing + Message);
  WriteEventLog(Message);
  if Indent = iAdd then
    Spacing := Spacing + '  ';
end;

end.
