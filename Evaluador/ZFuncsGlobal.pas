unit ZFuncsGlobal;

interface
uses Classes, Sysutils, ZetaQRExpr,
     ZEvaluador,
     DZetaServerProvider;

{$define QUINCENALES}
{.$undefine QUINCENALES}

type
  TGetGlobal = class( TZetaConstante )
  private

  public
    function GetDigitoEmpresa( const sCodigo: String ): String;
    function Calculate: TQREvResult; override;
  end;

  {TZetaActivo}
  TZetaActivo = class ( TZetaConstante )
  public
    function Calculate: TQREvResult; override;
  end;

implementation

uses
    ZGlobalTress,
    ZetaCommonClasses,
    ZetaCommonTools,
    ZetaCommonLists;

function TGetGlobal.GetDigitoEmpresa( const sCodigo: String ): String;
const
     K_QRY_DIGITO_EMPRESA = 'select CM_DIGITO from COMPANY where CM_CODIGO = %s';
var
   oProvider: TdmZetaServerProvider;
   FQryDigito: TZetaCursor;
begin
     Result := VACIO;
     oProvider := TdmZetaServerProvider.Create( nil );
     try
        with oProvider do
        begin
             EmpresaActiva := Comparte;
             FQryDigito := CreateQuery;
             try
                if AbreQueryScript( FQryDigito, Format( K_QRY_DIGITO_EMPRESA, [ EntreComillas( sCodigo ) ] ) ) then
                begin
                     with FQryDigito do
                     begin
                          if not IsEmpty then
                          begin
                               Result := FieldByName( 'CM_DIGITO' ).AsString;
                          end;
                     end;
                end;
             finally
                FreeAndNil( FQryDigito );
             end;
        end;
     finally
        FreeAndNil( oProvider );
     end;
end;

function TGetGlobal.Calculate: TQREvResult;
var
   iGlobal: Integer;
begin
     ParametrosFijos( 1 );
     iGlobal:= ParamInteger( 0 );
     {$ifndef ADUANAS }
     if ( iGlobal = K_GLOBAL_DIGITO_EMPRESA ) then
     begin
          with Result do
          begin
               Kind := resString;
               if ( not Evaluador.BuscandoRequeridos ) then
                  strResult := GetDigitoEmpresa( oZetaProvider.CodigoEmpresaActiva );   // No hace falta conservar el valor ya que hereda de TZetaConstante y entra una sola vez
          end;
     end
     else
     {$ENDIF}
     begin
          oZetaProvider.InitGlobales;
          with Result do
          begin
               case GetTipoGlobal( iGlobal ) of
                    tgBooleano:
                    begin
                         Kind := resBool;
                         BooResult := oZetaProvider.GetGlobalBooleano( iGlobal );
                    end;
                    tgFloat:
                    begin
                         Kind := resDouble;
                         dblResult := oZetaProvider.GetGlobalReal( iGlobal );
                    end;
                    tgNumero:
                    begin
                         Kind := resInt;
                         intResult := oZetaProvider.GetGlobalInteger( iGlobal );
                    end;
                    tgFecha:
                    begin
                         Kind := resDouble;
                         esFecha:= TRUE;
                         dblResult := oZetaProvider.GetGlobalDate( iGlobal );
                    end;
               else
                   begin             //tgTexto, tgMemo, tgAutomatico
                        Kind := resString;
                        strResult := oZetaProvider.GetGlobalString( iGlobal );
                   end;
               end;
          end;
     end;
end;

function TZetaActivo.Calculate: TQREvResult;
 var nRegresa: Integer;
begin
     ParametrosFijos(1);
     oZetaProvider.InitGlobales;
     {$ifdef TRESS}
     oZetaProvider.GetDatosPeriodo;
     oZetaProvider.GetDatosImss;
     {$endif}
     oZetaProvider.GetDatosActivos;
     nRegresa := ParamInteger( 0 );
     with Result do
     begin
          Kind:= resInt;
          case ( nRegresa ) of
               1 : intResult := ZetaEvaluador.GetEmpleado;  // empleado activo
               2 : intResult := oZetaProvider.YearDefault;  // a�o activo
               {$ifdef TRESS}
               3 : intResult := Ord(oZetaProvider.DatosPeriodo.Tipo);		// Tipo de N�mina
               4 : intResult := oZetaProvider.DatosPeriodo.Numero;	// N�mero de Per�odo
               5 : intResult := oZetaProvider.DatosPeriodo.Mes;    // Mes de Per�odo de N�mina
               6 :
               begin
                    Kind:= resDouble;
                    dblResult := oZetaProvider.DatosPeriodo.Inicio;
                    EsFecha := TRUE; // Fecha Inicio Per�odo
               end;
               7 :
               begin
                    Kind:= resDouble;
                    dblResult := oZetaProvider.DatosPeriodo.Fin;
                    EsFecha := TRUE; // Fecha Final Per�odo
               end;
               8 :
               begin
                    Kind:= resDouble;
                    dblResult := oZetaProvider.DatosPeriodo.Pago;
                    EsFecha := TRUE; // Fecha Pago Per�odo
               end;
               {$endif}
               9 :
               begin
                    Kind:= resDouble;
                    dblResult := oZetaProvider.FechaDefault;
                    EsFecha := TRUE;  // Fecha Activa
               end;
               10 :
               begin
                    Kind:= resDouble;
                    dblResult := oZetaProvider.FechaAsistencia;
                    EsFecha := TRUE; // Fecha Asistencia
               end;
               {$ifdef TRESS}
               11 :
               begin
                    Kind:= resString;
                    strResult := oZetaProvider.DatosImss.Patron
               end;
               12 : intResult := oZetaProvider.DatosImss.Year;
               13 : intResult := oZetaProvider.DatosImss.Mes;
               14 : intResult := Ord(oZetaProvider.DatosImss.Tipo);
               {$endif}
               15 : intResult := oZetaProvider.UsuarioActivo;
               16 :
               begin
                    Kind:= resString;
                    strResult := oZetaProvider.NombreUsuario;
               end;
               17 :
               begin
                    Kind:= resString;
                    strResult := oZetaProvider.CodigoEmpresa;
               end;
               18 :
               begin
                    Kind:= resString;
                    {$IFNDEF ADUANAS}
                    strResult := oZetaProvider.GetGlobalString( K_GLOBAL_RAZON_EMPRESA );// Nombre largo Empresa
                    {$ENDIF}
               end;
               {$ifdef TRESS}
               19 :
               begin
                    Kind:= resString;
                    with oZetaProvider.DatosPeriodo do
                    begin
                         if ( PosMes = 1 ) then
                            strResult:= 'I'
                         else if ( PerMes = PosMes ) then
                            strResult:= 'F'
                         else
                            strResult:= ZetaCommonTools.Space( 1 );
                    end;
               end;
               {$endif}
               {$ifdef QUINCENALES}
               20 :
               begin
                    Kind:= resDouble;
                    dblResult := oZetaProvider.DatosPeriodo.InicioAsis;  //Fecha de Inicio de Asistencia
                    EsFecha := TRUE; 
               end;
               21 :
               begin
                    Kind:= resDouble;
                    dblResult := oZetaProvider.DatosPeriodo.FinAsis;     //Fecha de Fin de Asistencia
                    EsFecha := TRUE;
               end;
               {$endif}
               else Result.IntResult := 0;
               {else
                   Result := ErrorCreate( ...); }
          end;
     end;
end;

end.
