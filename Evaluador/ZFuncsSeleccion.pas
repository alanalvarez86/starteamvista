unit ZFuncsSeleccion;

interface

uses Classes, Sysutils, DB, DBClient,
     ZetaQRExpr,
     ZEvaluador;

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );

implementation

uses ZFuncsGlobal,
     ZetaTipoEntidad;

{CV: DIC2002:
La clase base de estas funciones estan en la unidad ZEvaluador.
En ZEvaluador, estan las clases base, en esta unidad estan las
clases Hijas.
En ZFuncsTress, se tienen las mismas funciones }
{ ************* TZetaPRETTY_NAME ************* }

type
TZetaPRETTY_NAME = class ( TZetaBasePRETTY_NAME )
protected
  procedure AgregaDatosRequeridos;override;
end;

procedure TZetaPRETTY_NAME.AgregaDatosRequeridos;
begin
     AgregaRequerido( 'SOLICITA.SO_FOLIO', enSolicitud ); //Join
end;

{ ************* TZetaEDAD ************* }
type
  TZetaEDAD = class( TZetaBaseEDAD )
  protected
    function GetFecNacField: String;override;
    procedure AgregaDatosRequeridos;override;
  end;

function TZetaEDAD.GetFecNacField: String;
begin
     Result := 'SO_FEC_NAC';
end;

procedure TZetaEDAD.AgregaDatosRequeridos;
begin
     AgregaRequerido( Format('SOLICITA.%s',[GetFecNacField]), enSolicitud );
end;

{ ************* TZetaEDADLETRA ************* }

type
 TZetaEDADLETRA = class( TZetaBaseEDADLETRA )
 protected
    function GetFecNacField: String;override;
    procedure AgregaDatosRequeridos;override;
  end;

function TZetaEDADLETRA.GetFecNacField: String;
begin
     Result := 'SO_FEC_NAC';
end;

procedure TZetaEDADLETRA.AgregaDatosRequeridos;
begin
     AgregaRequerido( Format('SOLICITA.%s',[GetFecNacField]), enSolicitud );
end;

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );
begin
     with FunctionLibrary do
     begin
          RegisterFunction( ZFuncsGlobal.TGetGlobal, 'GLOBAL','Info de Globales de Empresa' );
          RegisterFunction( ZFuncsGlobal.TZetaActivo, 'ACTIVO','Valores Activos sistema' );
          RegisterFunction( TZetaEDADLETRA, 'EDAD_LETRA','Edad en Letra' );
          RegisterFunction( TZetaEDAD, 'EDAD','Edad' );
          RegisterFunction( TZetaPRETTY_NAME, 'PRETTY_NAM' );
     end;
end;

end.
