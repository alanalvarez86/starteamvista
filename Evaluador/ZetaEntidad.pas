unit ZetaEntidad;

interface

{$INCLUDE DEFINES.INC}


uses Classes, SysUtils, DB,
     ZetaTipoEntidad,
     ZetaCommonLists;

type
  TEntidad = class;


  { ******************** TRelaciones ******************* }
  TRelacionItem = class( TObject )
  private
    FTipoPadre: TipoEntidad;
    FCampoHijo: String;
    FPadre: TEntidad;
    FEntidad : TEntidad;
    FEsInnerJoin : Boolean;
  public
    property TipoPadre: TipoEntidad read FTipoPadre write FTipoPadre;
    property CampoHijo: String read FCampoHijo write FCampoHijo;
    property Padre : TEntidad read FPadre write FPadre;
    property Entidad : TEntidad read FEntidad write FEntidad;
    property EsInnerJoin : Boolean read FEsInnerJoin write FEsInnerJoin;

    function BuscaJoin( const Tabla : TipoEntidad; oListaJoin: TStringList ): Boolean;
//  function GetRelaciones : String;
  end;

  TRelaciones = class( TList )
  private
    function GetItems( Index: Integer ): TRelacionItem;
  public
    destructor  Destroy; override;
    property Items[ Index: Integer ]: TRelacionItem read GetItems;
  end;

  { ********************* TEntidad ************************** }

  TEntidad = class( TObject )
  private
    FTipoEntidad: TipoEntidad;
    FNombreTabla: String;
    FKeyFields: String;
    FRelaciones: TRelaciones;
    FDescripcion : String;
    FAlias: string;
    FAlAgregarRelacion : TNotifyEvent;
    FdmEntidades : TObject;
    function GetNombreTabla: String;
    function GetEntidades : TObject;
  public
    property KeyFields: String read FKeyFields write FKeyFields;
    property NombreTabla: String read GetNombreTabla write FNombreTabla;
    property Alias: string read FAlias write FAlias;
    property Relaciones: TRelaciones read FRelaciones write FRelaciones;
    property Tipo: TipoEntidad read FTipoEntidad write FTipoEntidad;
    property Descripcion : String read FDescripcion write FDescripcion;
    property AlAgregarRelacion: TNotifyEvent read FAlAgregarRelacion write FAlAgregarRelacion;
    property dmEntidades : TObject read GetEntidades write FdmEntidades;
    constructor Create;
    destructor Destroy; override;

    function BuscaJoin( const Tabla: TipoEntidad; oListaJoin: TStringList ): Boolean;
//    function GetRelaciones : String;
    procedure AddRelacion( const eTipoPadre : TipoEntidad; const sCampoHijo : String; const lEsInnerJoin : Boolean = FALSE );
    procedure AgregaRelacion( const oPadre : TEntidad; const sCampoHijo : String );
end;

implementation

uses
    {$ifdef RDD}
     DEntidades,
    {$else}
     DEntidadesTress,
    {$endif}
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaSQL;

{$ifndef RDD}
type TdmEntidades = TdmEntidadesTress;
{$endif}

{ ************ TRelacionItem *********** }
{
function TRelacionItem.GetRelaciones : String;
begin
     Result := Padre.GetRelaciones;
end;
}


function TRelacionItem.BuscaJoin( const Tabla : TipoEntidad; oListaJoin: TStringList ) : Boolean;
var
   sJoin: String;
   i: Integer;
   FCamposHijo, FCamposPadre : TStringList;
begin
     Result := ( Padre <> NIL ) ;
     if Result then
     begin
          Result := Padre.BuscaJoin( Tabla, oListaJoin );
          if Result then
          begin
               FCamposHijo := TStringList.Create;
               FCamposHijo.CommaText  := CampoHijo;

               FCamposPadre := TStringList.Create;
               FCamposPadre.CommaText := Padre.KeyFields;

               sJoin := '';
               if ( FCamposHijo.Count <> FCamposPadre.Count ) then
               begin
                    Raise Exception.Create( Format( 'Error en la lista de relaciones de la tabla %s (#%d)' + CR_LF +
                                              'Revisar la relaci�n hacia la tabla %s (#%d):' + CR_LF +
                                              'La lista de campos de la relaci�n DEBE de' + CR_LF +
                                              'coincidir con la Llave Primaria de %s',
                                              [Entidad.NombreTabla, Ord( Entidad.Tipo ),
                                               Padre.NombreTabla, Ord( Padre.Tipo ), Padre.NombreTabla ] ) ); 

               end;

               for i := 0 to ( FCamposHijo.Count - 1 ) do
               begin
                   sJoin := sJoin + LeftOuterJoin( Entidad.NombreTabla,
                                                   FCamposHijo[ i ],
                                                   Padre.NombreTabla,
                                                   FCamposPadre[ i ],
                                                   i = 0,
                                                   EsInnerJoin );
               end;

               // Agrega al inicio
               // Debe ser al inicio porque el orden de los joins debe ir de
               // nieto a hijo a padre
               // Se almacena en el Objects el Tipo de la entidad inmediata
               oListaJoin.InsertObject( 0, sJoin, TObject( Padre.Tipo ));

               FCamposHijo.Free;
               FCamposPadre.Free;
          end;
     end;
end;


{ **************** TRelaciones ************ }
destructor TRelaciones.Destroy;
var
    i : Integer;
begin
     for i := 0 to Count-1 do
         Items[ i ].Free;
     inherited Destroy;
end;

function TRelaciones.GetItems( Index: Integer ): TRelacionItem;
begin
     Result := TRelacionItem( inherited Items[ Index ] );
end;

{****************** TEntidad *****************************}
constructor TEntidad.Create;
begin
     inherited Create;
     FRelaciones := TRelaciones.Create;
end;

destructor TEntidad.Destroy;
begin
     FRelaciones.Free;
     inherited Destroy;
end;

function TEntidad.GetNombreTabla: String;
begin
     {$ifdef FALSE}
     if StrVacio(FAlias) then
        Result := FNombreTabla
     else
         Result := FNombreTabla +' '+ FAlias;
     {$endif}
     Result := FNombreTabla;
end;

function TEntidad.GetEntidades : TObject;
begin
     if ( FdmEntidades <> NIl ) then
     begin
          Result := FdmEntidades;
     end
     else
     begin
          Raise Exception.Create('No se asigno dmEntidades. Contactar al departamento de Desarrollo');
     end;
end;

function TEntidad.BuscaJoin( const Tabla : TipoEntidad; oListaJoin: TStringList ): Boolean;
const
     K_FORMAT = '%d;%d';
var
   i: Integer;
   oListaTempo, oListaMenor  : TStringList;
   nMenor       : Integer;
begin
     Result := ( Tabla = Tipo ) and
               ( TdmEntidades( dmEntidades ).ReferenciaCircular.IndexOf(Format( K_FORMAT, [Ord(Tipo), Ord(Tabla)] ) )= -1 );
     if ( not Result ) then
     begin
          oListaTempo  := TStringList.Create;
          oListaMenor  := TStringList.Create;
          nMenor       := MAXINT;
          if Assigned(FAlAgregarRelacion) then
          begin
               Relaciones.Clear; {Esto es para que se vuelvan a Generar SIEMPRE las relaciones}
               FAlAgregarRelacion(Self);
          end;

          with Relaciones do
          begin
               i := 0;
               // Si encuentra un camino de longitud 1
               // ya no tiene caso seguir buscando
               while ( i < Count ) and ( nMenor > 1 ) do
               begin
                    oListaTempo.Clear;
                    if ( TdmEntidades( dmEntidades ).ReferenciaCircular.IndexOf(Format( K_FORMAT, [Ord(Tipo), Ord(Items[ i ].TipoPadre)] ) )= -1 ) then
                    begin
                         TdmEntidades( dmEntidades ).ReferenciaCircular.Add( Format( K_FORMAT, [Ord(Tipo), Ord(Items[ i ].TipoPadre)] ) );
                         TdmEntidades( dmEntidades ).ReferenciaCircular.Add( Format( K_FORMAT, [Ord(Items[ i ].TipoPadre),Ord(Tipo)] ) );

                         if ( Items[ i ].BuscaJoin( Tabla, oListaTempo )) and
                         ( oListaTempo.Count < nMenor ) then
                         begin
                              Result := TRUE;
                              oListaMenor.Assign( oListaTempo );
                              nMenor := oListaMenor.Count;
                         end;
                    end;
                    i := i + 1;
               end;
          end;

          with oListaMenor do
               for i := 0 to Count-1 do
                   oListaJoin.AddObject( Strings[ i ], Objects[ i ] );

          oListaTempo.Free;
          oListaMenor.Free;
     end;
end;

procedure TEntidad.AgregaRelacion( const oPadre : TEntidad; const sCampoHijo : String );
begin
     AddRelacion( oPadre.Tipo,sCampoHijo );
     with FRelaciones.Items[ FRelaciones.Count-1 ] do
          Padre := oPadre;
end;

{CV: 3-Dic-2001
Se agreg� la propiedad lEsInnerJoin, para que el join del Padre contra el hijo se haga
con JOIN enves de con LEFT OUTER JOIN. Est propiedad se asigna al agregar la relacion
y se hace en dmEntidadesTress, las unicas RELACIONES que ponen en TRUE esta propiedad
son ACUMULA-COLABORA y ACUMULA-CONCENPTO.
La propiedad InnerJoin tambien tienen implementacion en
las unidades ZetaEntidad.Pas DEntidadesTress.pas y ZetaSql.pas}
procedure TEntidad.AddRelacion(const eTipoPadre: TipoEntidad; const sCampoHijo : String; const lEsInnerJoin : Boolean = FALSE );
 var oRelacion: TRelacionItem;
     {$ifdef FALSE}
     oLista : TStrings;
     {$endif}
begin
     {$ifdef FALSE}
     oLista := TStringList.Create;
     oLista.LoadFromFile('d:\temp\Relaciones.txt');
     oLista.Add( 'INSERT INTO R_RELACION( EN_CODIGO,RL_ENTIDAD,RL_CAMPOS,RL_ORDEN,RL_INNER ) '+
                 Format( 'VALUES(%d,%d,%s,%d,%s);', [Ord(Tipo),Ord(eTipoPadre),Comillas(sCampoHijo),FRelaciones.Count,Comillas(zBoolToStr(lEsInnerJoin))] ) );
     oLista.SaveToFile('d:\temp\Relaciones.txt');
     {$ENDIF}

     oRelacion := TRelacionItem.Create;
     with oRelacion do
     begin
          TipoPadre := eTipoPadre;
          CampoHijo := sCampoHijo;
          Entidad   := self;
          EsInnerJoin := lEsInnerJoin;
     end;
     FRelaciones.Add( oRelacion );
end;




end.
