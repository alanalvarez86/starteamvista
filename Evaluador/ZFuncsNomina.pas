unit ZFuncsNomina;

interface

uses Classes, Sysutils, DB,
     ZetaQRExpr,
     ZEvaluador;

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );

implementation

uses ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaTipoEntidad,
     DZetaServerProvider,
     DCalculoNomina;

{ ****************** TZetaCC **************************}
type
  TZetaCC = class( TZetaRegresaFloat )
  public
    { Public Declarations }
    function Calculate: TQREvResult; override;
  end;

function TZetaCC.Calculate: TQREvResult; // Sustituye la funcion CC() de TRESS //
begin
     Result.Kind      := resDouble;
     Result.dblResult := TCalcNomina(ZetaEvaluador.CalcNomina).ConceptoCalculado( ParamInteger( 0 ));
end;

{*** US 12365: Timbrado/Declaraciones : Mejorar calculo de exentos en n�mina ***}
{ *************  Prueba de Concepto  TZetaEC **************}
type
  TZetaEC = class( TZetaRegresaFloat )
  public
    { Public Declarations }
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

procedure TZetaEC.GetRequeridos( var TipoRegresa : TQREvResultType );
   function EsInteger( const iParam: Word ): Integer;
   begin
        Result := 0;
        with Argument( iParam ) do
        begin
             case Kind of
                  resInt: Result := intResult;
             else
                 ErrorParametroTipo( iParam, 'No es un n�mero entero' );
             end;
        end;
   end;
begin
     inherited;
     if not ZetaEvaluador.CalculandoNomina then
        ErrorFuncion( 'Solamente se puede utilizar en c�lculo de n�mina', 'Funci�n: ' + Expresion );
     TipoRegresa := resDouble;
     ParametrosFijos( 1 );
     EsInteger( 0 );
end;

function TZetaEC.Calculate: TQREvResult;
begin
     Result.Kind      := resDouble;
     Result.dblResult := TCalcNomina(ZetaEvaluador.CalcNomina).ConceptoCalculadoEx( ParamInteger( 0 ));
end;

{ *************  Calculo de Exento  TZetaCalExento **************}
type
  TZetaCalExento = class( TZetaRegresaFloat )
  public
    { Public Declarations }
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

procedure TZetaCalExento.GetRequeridos( var TipoRegresa : TQREvResultType );
   function EsInteger( const iParam: Word ): Integer;
   begin
        Result := 0;
        with Argument( iParam ) do
        begin
             case Kind of
                  resInt: Result := intResult;
             else
                 ErrorParametroTipo( iParam, 'No es un n�mero entero' );
             end;
        end;
   end;
begin
     inherited;
     if not ZetaEvaluador.CalculandoNomina then
        ErrorFuncion( 'Solamente se puede utilizar en c�lculo de n�mina', 'Funci�n: ' + Expresion );
     TipoRegresa := resDouble;
     ParametrosFijos( 2 );
     EsInteger( 0 );
     ParamFloat( 1 );
end;

function TZetaCalExento.Calculate: TQREvResult;
begin
     Result.Kind      := resDouble;
     Result.dblResult := TCalcNomina(ZetaEvaluador.CalcNomina).CalExento( ParamInteger( 0 ), ParamFloat( 1 ) );
end;



{ *************  Calculo de Exento  TZetaCalExentoPrevisionSocial **************}
type
  TZetaCalExentoPrevisionSocial = class( TZetaRegresaFloat )
  public
    { Public Declarations }
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
    procedure AgregaAcumulado( const iConcepto : Integer; const sAlias : String);
  end;

procedure TZetaCalExentoPrevisionSocial.AgregaAcumulado( const iConcepto : Integer; const sAlias : String);
var
    sScript : STring;
begin
     with oZetaProvider.DatosPeriodo do
        sScript := Format( 'DBO.SP_AS( %d, %d, %d, %d, NOMINA.CB_CODIGO ) %s',
	                        [ iConcepto, 1, Mes, Year, sAlias ] );
     AgregaRequerido( sScript, enNomina );
end;

procedure TZetaCalExentoPrevisionSocial.GetRequeridos( var TipoRegresa : TQREvResultType );
   {function EsInteger( const iParam: Word ): Integer;
   begin
        Result := 0;
        with Argument( iParam ) do
        begin
             case Kind of
                  resInt: Result := intResult;
             else
                 ErrorParametroTipo( iParam, 'No es un n�mero entero' );
             end;
        end;
   end;}
begin
     inherited;
     if not ZetaEvaluador.CalculandoNomina then
        ErrorFuncion( 'Solamente se puede utilizar en c�lculo de n�mina', 'Funci�n: ' + Expresion );

   {  with ZetaEvaluador.oZetaCreator do
     begin
          PreparaValUma;
     end;}

     AgregaAcumulado( 1018, 'APS');
     AgregaAcumulado( 1019, 'ASL');

     TipoRegresa := resDouble;
     ParametrosFijos( 0 );
    //// EsInteger( 0 );
    /// ParamFloat( 1 );
end;

function TZetaCalExentoPrevisionSocial.Calculate: TQREvResult;
  var
   dMontoExentoCalculado: TPesos;
   dMonto : TPesos;
   dTope, dTopeInicial : TPesos;
   dTopeExentoAnual, dTopeSalariosAnual: TPesos;
   dAcumuladoPSocial, dAcumuladoSalarios, dAcumuladoSalariosTotal : TPesos;
   dSalUMAAnual, dMontoExentoTotal, dMontoSalariosTotal : TPesos;
   oConcepto : TConceptoNomina;
   lRevisaSalarios, lAplicaTope : boolean;
   iPos: Integer;

   function ObtenerConceptoActual: Integer;
   begin

        Result := TCalcNomina( ZetaEvaluador.CalcNomina ).FConceptoActual;
   end;

begin
      Result.Kind      := resDouble;
     iPos := 0;
     lAplicaTope := TRUE;


     with TCalcNomina( ZetaEvaluador.CalcNomina ) do
     begin
       dSalUMAAnual :=  nEmpleadoUMA_Anual;
       dMontoExentoTotal := TotNomina(ttnExentoPS);
       dMontoSalariosTotal := TotNomina( ttnSalarioPS);
       dMonto := nMontoFormula;
     end;

     with DataSetEvaluador, oZetaProvider do
     begin
        dTopeExentoAnual     := DefaultFloat( 0, dSalUMAAnual * 1.0);
        lRevisaSalarios      := DefaultBoolean(1, TRUE);
        dTopeSalariosAnual   := DefaultFloat( 2, dSalUMAAnual * 7.0  );
        dAcumuladoPSocial    := DefaultFloat( 3, FieldByName( 'APS' ).AsFloat );
        dAcumuladoSalarios   := DefaultFloat( 4, FieldByName( 'ASL' ).AsFloat );
        dAcumuladoSalariosTotal := dAcumuladoSalarios +  dMontoSalariosTotal;
     end;

     if (lRevisaSalarios)  then
     begin
        lAplicaTope := ( dAcumuladoSalariosTotal > dTopeSalariosAnual );
     end;

     dTopeInicial :=  dTopeExentoAnual -  dAcumuladoPSocial;

     if ( lAplicaTope )  then
     begin
         if (dTopeInicial < 0.00 ) then
            dTopeInicial := 0.00;

         dTope := dTopeInicial - dMontoExentoTotal; //Tope Inicial - Exento Total del Periodo por PS
         if (dTope < 0.00 ) then
            dTope := 0.00;
     end
     else
     begin
        dTope := dMonto;
     end;


     Result.dblResult := rMin( dTope, dMonto );

     if ZetaEvaluador.Rastreando or ZetaEvaluador.RastreandoNomina  then
     begin
               with ZetaEvaluador.Rastreador do
               begin
                    oConcepto := nil;
                    oConcepto := TCalcNomina( ZetaEvaluador.CalcNomina ).GetConcepto( TCalcNomina( ZetaEvaluador.CalcNomina ).FConceptoActual );
                    RastreoHeader('');
                    if ( oConcepto <> nil ) then
                    begin
                    RastreoTextoLibre( 'F�rmula Exento ISPT : ' + Copy( oConcepto.ExentoISPT,  1, 60 ));
                    RastreoNoVacio(    '          ' + Copy( oConcepto.ExentoISPT, 61, 60 ));
                    RastreoNoVacio(    '          ' + Copy( oConcepto.ExentoISPT,121, 60 ));
                    RastreoNoVacio(    '          ' + Copy( oConcepto.ExentoISPT,181, 60 ));
                    RastreoEspacio;
                    end;
                    if ( lRevisaSalarios ) then
                    begin
                    RastreoPesos( '                    Tope Salarios Anual:', dTopeSalariosAnual );
                    RastreoPesos( '            Acumulado de Salarios Anual:', dAcumuladoSalarios );
                    RastreoPesos( '(+)       Monto de Salarios del Periodo:', dMontoSalariosTotal );
                    RastreoPesos( '(=)          Monto Total Salarios Anual:', dAcumuladoSalariosTotal );
                    end;

                    if (lAplicaTope) then
                    begin
                    RastreoPesos( '     Tope Exento Previsi�n Social Anual:', dTopeExentoAnual );
                    RastreoPesos( '(-)    Acumulado Previsi�n Social Anual:', dAcumuladoPSocial );
                    RastreoPesos( '(=)             Tope Exento del Periodo:', dTopeInicial );
                    RastreoPesos( '(-)        Exento Acumulado del Periodo:', dMontoExentoTotal );
                    RastreoPesos( '(=)          Tope Exento ISPT Calculado:', dTope );
                    end
                    else
                    begin
                    RastreoPesos( '     Tope Exento Previsi�n Social Anual:', dTopeExentoAnual );
                    RastreoPesos( '       Acumulado Previsi�n Social Anual:', dAcumuladoPSocial );
                    end;
                    RastreoEspacio;
                    if (lAplicaTope) then
                    begin
                    RastreoPesos( '             Tope Exento ISPT Calculado:', dTope );
                    RastreoPesos( '(MIN)      Monto Calculado del Concepto:', dMonto);
                    RastreoPesos( '(=)                    Exento Calculado:', Result.dblResult );
                    end
                    else
                    begin
                    RastreoPesos( '           Monto Calculado del Concepto:', dMonto);
                    RastreoPesos( '                       Exento Calculado:', Result.dblResult );
                    end;
                    RastreoEspacio;
                    if (lAplicaTope) then
                    RastreoPesos( '                   Tope Exento Restante:', dTope - Result.dblResult );

                    if ( lRevisaSalarios) and ( not lAplicaTope )   then
                    begin
               RastreoTextoLibre( 'NOTA: Se exenta al 100% debido a que no excede ');
               RastreoTextoLibre( 'el Tope de Salarios Anual.');
                    end;

               end;
     end;

end;


{ ****************** TZetaMonto **************************}
type
  TZetaMonto = class( TZetaRegresaFloat )
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaMonto.Calculate: TQREvResult;
begin
     Result.Kind      := resDouble;
     Result.dblResult := TCalcNomina(ZetaEvaluador.CalcNomina).nMontoFormula;
end;

{ ****************** TZetaExento **************************}
type
  TZetaExento = class( TZetaRegresaFloat )
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaExento.Calculate: TQREvResult;
begin
     Result.Kind      := resDouble;
     Result.dblResult := TCalcNomina(ZetaEvaluador.CalcNomina).nExentoFormula;
end;

{ ****************** TZetaTotales **************************}
type
  TZetaTotales = class( TZetaRegresaFloat )
  protected
    function Totales( Tipo : TipoTotalNomina ) : TQREvResult;
  end;

function TZetaTotales.Totales( Tipo : TipoTotalNomina ) : TQREvResult;
begin
     with Result do
     begin
          Kind := resDouble;
          resFecha := FALSE;
          dblResult := TCalcNomina(ZetaEvaluador.CalcNomina).TotNomina( Tipo );
     end;
end;

{ ****************** TZetaTotPer **************************}
type
  TZetaTotPer = class( TZetaTotales )
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaTotPer.Calculate : TQREvResult;
begin
     Result := Totales( ttnPercepciones );
end;

{ ****************** TZetaTotDed **************************}
type
  TZetaTotDed = class( TZetaTotales )
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaTotDed.Calculate : TQREvResult;
begin
     Result := Totales( ttnDeducciones );
end;

{ ****************** TZetaTotNeto **************************}
type
  TZetaTotNeto = class( TZetaTotales )
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaTotNeto.Calculate : TQREvResult;
begin
     Result := Totales( ttnNeto );
end;

{ ****************** TZetaTotExento **************************}
type
  TZetaTotExento = class( TZetaTotales )
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaTotExento.Calculate : TQREvResult;
begin
     Result := Totales( ttnExentas );
end;

{ ****************** TZetaTotExMen **************************}
type
  TZetaTotExMen = class( TZetaTotales )
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaTotExMen.Calculate : TQREvResult;
begin
     Result := Totales( ttnExentoMensual );
end;

{ ****************** TZetaIndiExe **************************}
type
  TZetaIndiExe = class( TZetaTotales )
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaIndiExe.Calculate : TQREvResult;
begin
     Result := Totales( ttnExentoCal );
end;

{ ****************** TZetaTotMens **************************}
type
  TZetaTotMens = class( TZetaTotales )
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaTotMens.Calculate : TQREvResult;
begin
     Result := Totales( ttnMensuales );
end;

{ ****************** TZetaIndiImp **************************}
type
  TZetaIndiImp = class( TZetaTotales )
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaIndiImp.Calculate : TQREvResult;
begin
     Result := Totales( ttnImpCal );
end;

{ ****************** TZetaIndiPer **************************}
type
  TZetaIndiPer = class( TZetaTotales )
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaIndiPer.Calculate : TQREvResult;
begin
     Result := Totales( ttnPerCal );
end;

{ ****************** TZetaTotPresta **************************}
type
  TZetaTotPresta = class( TZetaTotales )
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaTotPresta.Calculate : TQREvResult;
begin
     Result := Totales( ttnPrestaciones );
end;


{ ****************** TZetaTotPrevGravada **************************}
type
  TZetaTotPrevGravada = class( TZetaTotales )
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaTotPrevGravada.Calculate : TQREvResult;
begin
     Result := Totales( ttnGravadoPrev );
end;

{ ****************** TZetaPrestaRef **************************}
type
  TZetaPrestaRef = class( TZetaRegresa )
  public
        procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
        function Calculate: TQREvResult; override;
  end;

procedure TZetaPrestaRef.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resString;
end;

function TZetaPrestaRef.Calculate: TQREvResult;
begin
     Result.Kind      := resString;
     Result.strResult := TCalcNomina(ZetaEvaluador.CalcNomina).ReferenciaActiva;
end;


{ ****************** TZetaTotISNGravada **************************}
type
  TZetaTotISNGravada = class( TZetaTotales )
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaTotISNGravada.Calculate : TQREvResult;
begin
     Result := Totales( ttnGravadoISN );
end;


{ ************** Registro de Funciones ********** }

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );
begin
     with FunctionLibrary do
     begin
          RegisterFunction( TZetaCC, 'CC', ' Concepto Calculado' );
          RegisterFunction( TZetaMonto, 'MONTO', 'Monto F�rmula del Concepto' );
          RegisterFunction( TZetaExento, 'EXENTO', 'F�rmula Exento' );
          RegisterFunctionDP( TZetaTotPer, 'TOT_PER', 'Variable Total Percepciones' );
          RegisterFunctionDP( TZetaTotDed, 'TOT_DED', 'Variable Total Deducciones' );
          RegisterFunctionDP( TZetaTotNeto,'TOT_NETO', 'Monto neto de n�mina' );
          RegisterFunction( TZetaTotExento,'TOT_EXENTO', 'Variable Total Exento' );
          RegisterFunction( TZetaTotExMen,'TOT_EX_MEN', 'Variable Total Exento Mensual' );
          RegisterFunction( TZetaIndiExe,'INDI_EXE', 'Monto Exento Individual N�mina' );
          RegisterFunction( TZetaTotMens,'TOT_MENS', 'Variable Total Percep. Mensual' );
          RegisterFunction( TZetaIndiImp,'INDI_IMP', 'Monto Impuesto N�mina' );
          RegisterFunction( TZetaIndiPer,'INDI_PER', 'Monto Percepciones N�mina' );
          RegisterFunction( TZetaTotPresta,'TOT_PRESTA', 'Variable Total Prestaciones' );
          RegisterFunction( TZetaTotPrevGravada,'TOT_PREVGR', 'Total de Previsiones Gravadas' );
          RegisterFunction( TZetaPrestaRef, 'PRESTA_REF', 'Referencia Pr�stamo' );
          RegisterFunction( TZetaTotISNGravada, 'TOT_ISN', 'Monto gravado para Impuesto de N�minas' );
          {*** US 12365: Timbrado/Declaraciones : Mejorar calculo de exentos en n�mina ***}
          RegisterFunction( TZetaEC, 'EC', ' Exento Calculado' );
          RegisterFunction( TZetaCalExento, 'CAL_EXENTO', 'Evalua el monto exento');
          RegisterFunction( TZetaCalExentoPrevisionSocial, 'CAL_EX_PS', 'Evalua el monto exento de Previsi�n Social');
     end;
end;


end.
