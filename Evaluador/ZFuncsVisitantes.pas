unit ZFuncsVisitantes;

interface

uses Classes, Sysutils, DB, DBClient,
     ZetaQRExpr,
     ZEvaluador,
     DZetaServerProvider,
     ZetaCommonClasses;

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );

implementation



uses
    ZetaTipoEntidad,
    ZFuncsGlobal,
    ZVisitantesTools;

{************** TZetaBasePRETTY_NAME ************* }
type
TZetaNOMBRE = class ( TZetaRegresa )
  protected
    FPretty: string;
    procedure AgregaDatosRequeridos;dynamic;
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaNOMBRE.AgregaDatosRequeridos;
begin
end;

procedure TZetaNOMBRE.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resString;
     // Si lo pongo desde aqu�, me ahorro la b�squeda del campo calculado en el diccionario
     AgregaDatosRequeridos;
     AgregaScript( FPretty, TipoRegresa, False  );
end;

{ ************* TZetaNOMBRE_VIS ************* }

type
TZetaNOMBRE_VIS = class ( TZetaNOMBRE )
protected
  procedure AgregaDatosRequeridos;override;
end;

procedure TZetaNOMBRE_VIS.AgregaDatosRequeridos;
begin
     FPretty := PRETTY_VISIT;
     AgregaRequerido( 'VISITA.VI_NUMERO', enVisita ); //Join
end;

{ ************* TZetaNOMBRE_ANF ************* }

type
TZetaNOMBRE_ANF = class ( TZetaNOMBRE )
protected
  procedure AgregaDatosRequeridos;override;
end;

procedure TZetaNOMBRE_ANF.AgregaDatosRequeridos;
begin
     FPretty := PRETTY_ANFITRIO;
     AgregaRequerido( 'ANFITRIO.AN_NUMERO', enAnfitrio ); //Join
end;

{ ************* TZetaEDAD ************* }
type
  TZetaEDAD = class( TZetaBaseEDAD )
  protected
    function GetFecNacField: String;override;
    procedure AgregaDatosRequeridos;override;
  end;

function TZetaEDAD.GetFecNacField: String;
begin
     Result := 'VI_FEC_NAC';
end;

procedure TZetaEDAD.AgregaDatosRequeridos;
begin
     AgregaRequerido( Format('VISITA.%s',[GetFecNacField]), enVisita );
end;

{ ************* TZetaEDADLETRA ************* }

type
 TZetaEDADLETRA = class( TZetaBaseEDADLETRA )
 protected
    function GetFecNacField: String;override;
    procedure AgregaDatosRequeridos;override;
  end;

function TZetaEDADLETRA.GetFecNacField: String;
begin
     Result := 'VI_FEC_NAC';
end;

procedure TZetaEDADLETRA.AgregaDatosRequeridos;
begin
     AgregaRequerido( Format('VISITA.%s',[GetFecNacField]), enVisita );
end;

type
  TZetaUsuario = class( TZetaFunc )
  private
    FDataSet : TClientDataSet;
  public
    constructor Create(oEvaluador:TQrEvaluator); override;
    destructor Destroy; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

constructor TZetaUsuario.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create(oEvaluador);
     FDataSet := TClientDataSet.Create( oZetaProvider );
end;

destructor TZetaUsuario.Destroy;
begin
     FDataSet.Free;
     inherited Destroy;
end;

procedure TZetaUsuario.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resString;
     oZetaProvider.GetDatosActivos;
end;

function TZetaUsuario.Calculate: TQREvResult;
const
     sSQL = 'select US_CODIGO, US_NOMBRE from USUARIO where US_CODIGO = %s';
var
   iUsuario: Integer;
begin
     with oZetaProvider, Result do
     begin
          iUsuario:= DefaultInteger( 0, UsuarioActivo );
          Kind := resString;
          if ( iUsuario = UsuarioActivo ) then
             strResult := NombreUsuario
          else
          begin
               with FDataSet do
               begin
                    if ( not Active ) or ( FieldByName( 'US_CODIGO' ).AsInteger <> iUsuario ) then
                       Data := OpenSQL( Comparte, Format( sSQL, [ IntToStr( iUsuario ) ] ), TRUE );
                    strResult:= FieldByName( 'US_NOMBRE' ).AsString;
               end;
          end;
     end;
end;


procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );
begin
     with FunctionLibrary do
     begin
          RegisterFunction( ZFuncsGlobal.TGetGlobal, 'GLOBAL', 'Info de Globales de Empresa' );
          RegisterFunction( ZFuncsGlobal.TZetaActivo, 'ACTIVO','Valores activos de sistema' );
          RegisterFunction( TZetaUSUARIO, 'USUARIO', 'Regresa el Nombre del Usuario' );
          RegisterFunction( TZetaEDAD, 'EDAD','Edad en N�mero' );
          RegisterFunction( TZetaEDADLETRA, 'EDAD_LETRA','Edad en Letra' );
          RegisterFunction( TZetaNOMBRE_ANF,'N_ANFITRIO','Nombre anfitri�n' );
          RegisterFunction( TZetaNOMBRE_VIS,'N_VISITANT','Nombre visitante' );
     end;
end;

end.
