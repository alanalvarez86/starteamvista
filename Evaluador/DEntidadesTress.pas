unit DEntidadesTress;

interface

{$INCLUDE DEFINES.INC}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DEntidades, ZetaTipoEntidad, ZetaEntidad;

type
  TdmEntidadesTress = class(TdmEntidades)
  private
    { Private declarations }
  protected
    procedure CreaEntidades; override;
  public
    { Public declarations }
  end;

implementation

uses ZetaCommonLists;

{$R *.DFM}

{ TdmEntidadesTress }

procedure TdmEntidadesTress.CreaEntidades;
begin
      {CV: 3-Dic-2001
      Se agreg� la propiedad lEsInnerJoin, para que el join del Padre contra el hijo se haga
      con JOIN enves de con LEFT OUTER JOIN. Est propiedad se asigna al agregar la relacion
      y se hace en dmEntidadesTress, las unicas RELACIONES que ponen en TRUE esta propiedad
      son ACUMULA-COLABORA y ACUMULA-CONCENPTO.
      La propiedad InnerJoin tambien tienen implementacion en
      las unidades ZetaEntidad.Pas DEntidadesTress.pas y ZetaSql.pas}
      with AddEntidad( enAcumula,'ACUMULA','CB_CODIGO,CO_NUMERO,AC_YEAR','Acumulados') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO', TRUE );
           AddRelacion( enConcepto,'CO_NUMERO', TRUE );
      end;

      with AddEntidad( enEmpleado,'COLABORA','CB_CODIGO', 'Empleados') do
      begin
           AddRelacion( enContrato,'CB_CONTRAT');
           AddRelacion( enClasifi,'CB_CLASIFI');
           AddRelacion( enRPatron,'CB_PATRON');
           AddRelacion( enPuesto,'CB_PUESTO');
           AddRelacion( enTurno,'CB_TURNO');
           AddRelacion( enSSocial,'CB_TABLASS');
           AddRelacion( enMotBaja,'CB_MOT_BAJ');
           AddRelacion( enNivel1,'CB_NIVEL1');
           AddRelacion( enNivel2,'CB_NIVEL2');
           AddRelacion( enNivel3,'CB_NIVEL3');
           AddRelacion( enNivel4,'CB_NIVEL4');
           AddRelacion( enNivel5,'CB_NIVEL5');
           AddRelacion( enNivel6,'CB_NIVEL6');
           AddRelacion( enNivel7,'CB_NIVEL7');
           AddRelacion( enNivel8,'CB_NIVEL8');
           AddRelacion( enNivel9,'CB_NIVEL9');
           {$ifdef ACS}
           AddRelacion( enNivel10,'CB_NIVEL10');{OP: 4.Ago.08}
           AddRelacion( enNivel11,'CB_NIVEL11');{OP: 4.Ago.08}
           AddRelacion( enNivel12,'CB_NIVEL12');{OP: 4.Ago.08}
           {$endif}
           AddRelacion( enEdoCivil,'CB_EDO_CIV');
           AddRelacion( enEntidad,'CB_ESTADO');
           AddRelacion( enMunicipio,'CB_MUNICIP');

           AddRelacion( enEstudios,'CB_ESTUDIO');
           AddRelacion( enTransporte,'CB_MED_TRA');
           AddRelacion( enViveCon,'CB_VIVECON');
           AddRelacion( enViveEn,'CB_VIVEEN');
           AddRelacion( enExtra1,'CB_G_TAB_1');
           AddRelacion( enExtra2,'CB_G_TAB_2');
           AddRelacion( enExtra3,'CB_G_TAB_3');
           AddRelacion( enExtra4,'CB_G_TAB_4');
           AddRelacion( enExtra5,'CB_G_TAB_5');
           AddRelacion( enExtra6,'CB_G_TAB_6');
           AddRelacion( enExtra7,'CB_G_TAB_7');
           AddRelacion( enExtra8,'CB_G_TAB_8');
           AddRelacion( enExtra9,'CB_G_TAB_9');
           AddRelacion( enExtra10,'CB_G_TAB10');
           AddRelacion( enExtra11,'CB_G_TAB11');
           AddRelacion( enExtra12,'CB_G_TAB12');
           AddRelacion( enExtra13,'CB_G_TAB13');
           AddRelacion( enExtra14,'CB_G_TAB14');
           AddRelacion( enFoto,'CB_CODIGO');
           AddRelacion( enArea, 'CB_AREA' );
           {$ifdef SERVERCAFE}
           AddRelacion( enTarjetaXY,'CB_CODIGO,:Fecha');
           {$endif}
           AddRelacion( enEntidadNac, 'CB_ENT_NAC' );
           AddRelacion( enColonia, 'CB_COD_COL' );
          // AddRelacion( enBancos, 'CB_BANCO' ) ;
      end;
      
      AddEntidad( enEntidadNac,'ENT_NAC','TB_CODIGO');

      with AddEntidad( enKardex,'KARDEX','CB_CODIGO,CB_FECHA,CB_TIPO') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
           AddRelacion( enClasifi,'CB_CLASIFI');
           AddRelacion( enPuesto,'CB_PUESTO');
           AddRelacion( enContrato,'CB_CONTRAT');
           AddRelacion( enTurno,'CB_TURNO');
           AddRelacion( enRPatron,'CB_PATRON');
           AddRelacion( enSSocial,'CB_TABLASS');
           AddRelacion( enNivel1,'CB_NIVEL1');
           AddRelacion( enNivel2,'CB_NIVEL2');
           AddRelacion( enNivel3,'CB_NIVEL3');
           AddRelacion( enNivel4,'CB_NIVEL4');
           AddRelacion( enNivel5,'CB_NIVEL5');
           AddRelacion( enNivel6,'CB_NIVEL6');
           AddRelacion( enNivel7,'CB_NIVEL7');
           AddRelacion( enNivel8,'CB_NIVEL8');
           AddRelacion( enNivel9,'CB_NIVEL9');
           {$ifdef ACS}
           AddRelacion( enNivel10,'CB_NIVEL10');{OP: 4.Ago.08}
           AddRelacion( enNivel11,'CB_NIVEL11');{OP: 4.Ago.08}
           AddRelacion( enNivel12,'CB_NIVEL12');{OP: 4.Ago.08}
           {$endif}
           AddRelacion( enMotBaja,'CB_MOT_BAJ');
           AddRelacion( enTKardex,'CB_TIPO');
           AddRelacion( enPeriodo,'CB_NOMYEAR,CB_NOMTIPO,CB_NOMNUME');
      end;

      with AddEntidad( enVacacion,'VACACION','CB_CODIGO,VA_FEC_INI,VA_TIPO') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
           AddRelacion( enSSocial,'CB_TABLASS');
      end;

      with AddEntidad( enPermiso,'PERMISO','CB_CODIGO,PE_FEC_INI') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
           AddRelacion( enIncidencia,'PM_TIPO');
      end;

      with AddEntidad( enIncapacidad,'INCAPACI','CB_CODIGO,IN_FEC_INI') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
           AddRelacion( enIncidencia,'IN_TIPO');
      end;

      with AddEntidad( enNomina,'NOMINA','PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO', 'N�mina' ) do
      begin
           AddRelacion( enPeriodo,'PE_YEAR,PE_TIPO,PE_NUMERO');
           AddRelacion( enEmpleado,'CB_CODIGO');
           AddRelacion( enPuesto,'CB_PUESTO');
           AddRelacion( enTurno,'CB_TURNO');
           AddRelacion( enClasifi,'CB_CLASIFI');
           AddRelacion( enRPatron,'CB_PATRON');
           AddRelacion( enNivel1,'CB_NIVEL1');
           AddRelacion( enNivel2,'CB_NIVEL2');
           AddRelacion( enNivel3,'CB_NIVEL3');
           AddRelacion( enNivel4,'CB_NIVEL4');
           AddRelacion( enNivel5,'CB_NIVEL5');
           AddRelacion( enNivel6,'CB_NIVEL6');
           AddRelacion( enNivel7,'CB_NIVEL7');
           AddRelacion( enNivel8,'CB_NIVEL8');
           AddRelacion( enNivel9,'CB_NIVEL9');
           {$ifdef ACS}
           AddRelacion( enNivel10,'CB_NIVEL10');{OP: 4.Ago.08}
           AddRelacion( enNivel11,'CB_NIVEL11');{OP: 4.Ago.08}
           AddRelacion( enNivel12,'CB_NIVEL12');{OP: 4.Ago.08}
           {$endif}
      end;

      with AddEntidad( enPuesto,'PUESTO','PU_CODIGO', 'Puesto') do
      begin
           AddRelacion( enClasifi,'PU_CLASIFI');
           AddRelacion( enTurno,'PU_TURNO');
           AddRelacion( enRPatron,'PU_PATRON');
           AddRelacion( enNivel1,'PU_NIVEL1');
           AddRelacion( enNivel2,'PU_NIVEL2');
           AddRelacion( enNivel3,'PU_NIVEL3');
           AddRelacion( enNivel4,'PU_NIVEL4');
           AddRelacion( enNivel5,'PU_NIVEL5');
           AddRelacion( enNivel6,'PU_NIVEL6');
           AddRelacion( enNivel7,'PU_NIVEL7');
           AddRelacion( enNivel8,'PU_NIVEL8');
           AddRelacion( enNivel9,'PU_NIVEL9');
           {$ifdef ACS}
           AddRelacion( enNivel10,'PU_NIVEL10');{OP: 4.Ago.08}
           AddRelacion( enNivel11,'PU_NIVEL11');{OP: 4.Ago.08}
           AddRelacion( enNivel12,'PU_NIVEL12');{OP: 4.Ago.08}
           {$endif}
           AddRelacion( enSSocial,'PU_TABLASS');
           AddRelacion( enContrato,'PU_CONTRAT');
           AddRelacion( enArea, 'PU_AREA' );
           AddRelacion( enOcupaNac, 'PU_CLAVE' );
      end;

      with AddEntidad( enConcepto,'CONCEPTO','CO_NUMERO') do
      begin
           AddRelacion( enQuerys,'CO_QUERY');
      end;

      with AddEntidad( enAhorro,'AHORRO','CB_CODIGO,AH_TIPO') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
           AddRelacion( enTAhorro,'AH_TIPO');
      end;

      with AddEntidad( enTArt_80, 'T_ART_80', 'NU_CODIGO,TI_INICIO' ) do
      begin
           AddRelacion( enNumerica, 'NU_CODIGO' );
      end;

      with AddEntidad( enArt_80, 'ART_80', 'NU_CODIGO,TI_INICIO,A80_LI' ) do
      begin
           AddRelacion( enTArt_80, 'NU_CODIGO,TI_INICIO' );
      end;

      with AddEntidad( enAusencia,'AUSENCIA','CB_CODIGO,AU_FECHA') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
           AddRelacion( enNomina,'PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO');
           AddRelacion( enIncidencia,'AU_TIPO');
           AddRelacion( enClasifi,'CB_CLASIFI');
           AddRelacion( enTurno,'CB_TURNO');
           AddRelacion( enPuesto,'CB_PUESTO');
           AddRelacion( enHorario,'HO_CODIGO');
           AddRelacion( enNivel1,'CB_NIVEL1');
           AddRelacion( enNivel2,'CB_NIVEL2');
           AddRelacion( enNivel3,'CB_NIVEL3');
           AddRelacion( enNivel4,'CB_NIVEL4');
           AddRelacion( enNivel5,'CB_NIVEL5');
           AddRelacion( enNivel6,'CB_NIVEL6');
           AddRelacion( enNivel7,'CB_NIVEL7');
           AddRelacion( enNivel8,'CB_NIVEL8');
           AddRelacion( enNivel9,'CB_NIVEL9');
           {$ifdef ACS}
           AddRelacion( enNivel10,'CB_NIVEL10');{OP: 4.Ago.08}
           AddRelacion( enNivel11,'CB_NIVEL11');{OP: 4.Ago.08}
           AddRelacion( enNivel12,'CB_NIVEL12');{OP: 4.Ago.08}
           {$endif}
      end;

      with AddEntidad( enCalCurso,'CALCURSO','CU_CODIGO') do
      begin
           AddRelacion( enCurso,'CU_CODIGO');
      end;

      with AddEntidad( enACarAbo,'ACAR_ABO','CB_CODIGO,AH_TIPO,CR_FECHA') do
      begin
           AddRelacion( enAhorro,'CB_CODIGO,AH_TIPO');
      end;

      with AddEntidad( enChecadas,'CHECADAS','CB_CODIGO,AU_FECHA,CH_H_REAL') do
      begin
           AddRelacion( enAusencia,'CB_CODIGO,AU_FECHA');
           AddRelacion( enMotAuto,'CH_RELOJ');
           AddRelacion( enMotCheca,'CH_MOTIVO');
      end;

      with AddEntidad( enCurso,'CURSO','CU_CODIGO') do
      begin
           AddRelacion( enMaestros,'MA_CODIGO');
           AddRelacion( enTCurso,'CU_CLASIFI');
           AddRelacion( enCCurso, 'CU_CLASE' );
           AddRelacion( enAreaTematica ,'AT_CODIGO');
      end;


      with AddEntidad( enEntrena,'ENTRENA','PU_CODIGO,CU_CODIGO') do
      begin
           AddRelacion( enCurso,'CU_CODIGO');
           AddRelacion( enPuesto,'PU_CODIGO');
      end;

      with AddEntidad( enEntNivel,'ENTNIVEL','PU_CODIGO,CU_CODIGO,ET_CODIGO')  do
      begin
           AddRelacion( enEntrena,'PU_CODIGO,CU_CODIGO');
      end;

      with AddEntidad( enEventoAlta,'EV_ALTA','EA_CODIGO') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
           AddRelacion( enPuesto,'EA_PUESTO');
           AddRelacion( enTurno,'EA_TURNO');
           AddRelacion( enClasifi,'EA_CLASIFI');
           AddRelacion( enContrato,'EA_CONTRAT');
           AddRelacion( enRPatron,'EA_PATRON');
           AddRelacion( enSSocial,'EA_TABLASS');
           AddRelacion( enNivel1,'EA_NIVEL1');
           AddRelacion( enNivel2,'EA_NIVEL2');
           AddRelacion( enNivel3,'EA_NIVEL3');
           AddRelacion( enNivel4,'EA_NIVEL4');
           AddRelacion( enNivel5,'EA_NIVEL5');
           AddRelacion( enNivel6,'EA_NIVEL6');
           AddRelacion( enNivel7,'EA_NIVEL7');
           AddRelacion( enNivel8,'EA_NIVEL8');
           AddRelacion( enNivel9,'EA_NIVEL9');
           {$ifdef ACS}
           AddRelacion( enNivel10,'EA_NIVEL10');{OP: 4.Ago.08}
           AddRelacion( enNivel11,'EA_NIVEL11');{OP: 4.Ago.08}
           AddRelacion( enNivel12,'EA_NIVEL12');{OP: 4.Ago.08}
           {$endif}
      end;
      
      with AddEntidad( enEvento,'EVENTO','EV_CODIGO') do
      begin
           AddRelacion( enPuesto,'EV_PUESTO');
           AddRelacion( enTurno,'EV_TURNO');
           AddRelacion( enClasifi,'EV_CLASIFI');
           AddRelacion( enContrato,'EV_CONTRAT');
           AddRelacion( enTKardex,'EV_KARDEX');
           AddRelacion( enMotBaja,'EV_MOT_BAJ');
           AddRelacion( enRPatron,'EV_PATRON');
           AddRelacion( enPeriodo,'EV_PE_NUM');
           AddRelacion( enQuerys,'EV_QUERY');
           AddRelacion( enSSocial,'EV_TABLASS');
           AddRelacion( enNivel1,'EV_NIVEL1');
           AddRelacion( enNivel2,'EV_NIVEL2');
           AddRelacion( enNivel3,'EV_NIVEL3');
           AddRelacion( enNivel4,'EV_NIVEL4');
           AddRelacion( enNivel5,'EV_NIVEL5');
           AddRelacion( enNivel6,'EV_NIVEL6');
           AddRelacion( enNivel7,'EV_NIVEL7');
           AddRelacion( enNivel8,'EV_NIVEL8');
           AddRelacion( enNivel9,'EV_NIVEL9');
           {$ifdef ACS}
           AddRelacion( enNivel10,'EV_NIVEL10');{OP: 4.Ago.08}
           AddRelacion( enNivel11,'EV_NIVEL11');{OP: 4.Ago.08}
           AddRelacion( enNivel12,'EV_NIVEL12');{OP: 4.Ago.08}
           {$endif}
      end;

      with AddEntidad( enCuboPresup,'TOTPRESUP','') do
      begin
           AddRelacion( enConcepto,'CO_NUMERO');
           AddRelacion( enPuesto,'CB_PUESTO');
           AddRelacion( enTurno,'CB_TURNO');
           AddRelacion( enClasifi,'CB_CLASIFI');
           AddRelacion( enNivel1,'CB_NIVEL1');
           AddRelacion( enNivel2,'CB_NIVEL2');
           AddRelacion( enNivel3,'CB_NIVEL3');
           AddRelacion( enNivel4,'CB_NIVEL4');
           AddRelacion( enNivel5,'CB_NIVEL5');
           AddRelacion( enNivel6,'CB_NIVEL6');
           AddRelacion( enNivel7,'CB_NIVEL7');
           AddRelacion( enNivel8,'CB_NIVEL8');
           AddRelacion( enNivel9,'CB_NIVEL9');
           {$ifdef ACS}
           AddRelacion( enNivel10,'CB_NIVEL10');{OP: 4.Ago.08}
           AddRelacion( enNivel11,'CB_NIVEL11');{OP: 4.Ago.08}
           AddRelacion( enNivel12,'CB_NIVEL12');{OP: 4.Ago.08}
           {$endif}
      end;

      AddEntidad( enSupuestosRH,'SUPUEST_RH','SR_FECHA,SR_TIPO,EV_CODIGO');

      with AddEntidad( enFaltas,'FALTAS','FA_MOTIVO,FA_FEC_INI,PE_YEAR,CB_CODIGO,PE_TIPO,PE_NUMERO') do
      begin
           AddRelacion( enNomina,'PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO');
      end;

      with AddEntidad( enFestivo,'FESTIVO','TU_CODIGO,FE_MES,FE_DIA') do
      begin
           AddRelacion( enTurno,'TU_CODIGO');
      end;

      with AddEntidad( enKarCurso,'KARCURSO','CB_CODIGO;PU_CODIGO,CU_CODIGO') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
           AddRelacion( enCurso,'CU_CODIGO');
           AddRelacion( enPuesto,'CB_PUESTO');
           AddRelacion( enTurno,'CB_TURNO');
           AddRelacion( enClasifi,'CB_CLASIFI');
           AddRelacion( enNivel1,'CB_NIVEL1');
           AddRelacion( enNivel2,'CB_NIVEL2');
           AddRelacion( enNivel3,'CB_NIVEL3');
           AddRelacion( enNivel4,'CB_NIVEL4');
           AddRelacion( enNivel5,'CB_NIVEL5');
           AddRelacion( enNivel6,'CB_NIVEL6');
           AddRelacion( enNivel7,'CB_NIVEL7');
           AddRelacion( enNivel8,'CB_NIVEL8');
           AddRelacion( enNivel9,'CB_NIVEL9');
           {$ifdef ACS}
           AddRelacion( enNivel10,'CB_NIVEL10');{OP: 4.Ago.08}
           AddRelacion( enNivel11,'CB_NIVEL11');{OP: 4.Ago.08}
           AddRelacion( enNivel12,'CB_NIVEL12');{OP: 4.Ago.08}
           {$endif}
           AddRelacion( enMaestros,'MA_CODIGO' );
           AddRelacion( enSesion,'SE_FOLIO' );
           AddRelacion( enEstablecimiento,'KC_EST' );
      end;

      with AddEntidad( enLiq_Emp,'LIQ_EMP','LS_PATRON,LS_YEAR,LS_MONTH,LS_TIPO,CB_CODIGO') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
           AddRelacion( enLiq_IMSS,'LS_PATRON,LS_YEAR,LS_TIPO,LS_MONTH');
           AddRelacion( enRPatron,'LS_PATRON');
      end;

      with AddEntidad( enLiq_IMSS,'LIQ_IMSS','LS_PATRON,LS_YEAR,LS_TIPO,LS_MONTH') do
      begin
           AddRelacion( enRPatron,'LS_PATRON');
      end;

      with AddEntidad( enLiq_Mov,'LIQ_MOV','LS_PATRON,LS_YEAR,LS_MONTH,LS_TIPO,CB_CODIGO') do
      begin
           AddRelacion( enLiq_Emp,'LS_PATRON,LS_YEAR,LS_MONTH,LS_TIPO,CB_CODIGO');
           AddRelacion( enRPatron,'LS_PATRON');
           AddRelacion( enTKardex,'LM_KAR_TIP');
      end;

      with AddEntidad( enPoll,'POLL','') do
      begin
           AddRelacion( enEmpleado,'PO_NUMERO');
      end;

      with AddEntidad( enTAhorro,'TAHORRO','TB_CODIGO') do
      begin
           AddRelacion( enConcepto,'TB_CONCEPT');
           AddRelacion( enTPresta, 'TB_PRESTA' );
      end;

      with AddEntidad( enTPresta,'TPRESTA','TB_CODIGO') do
      begin
           AddRelacion( enConcepto,'TB_CONCEPT');
      end;

      with AddEntidad( enReporte,'REPORTE','RE_CODIGO') do
      begin
           AddRelacion( enQuerys,'QU_CODIGO');
      end;

      with AddEntidad( enMisReportes, 'MISREPOR', 'RE_CODIGO,US_CODIGO' ) do
      begin
           AddRelacion( enReporte,'RE_CODIGO');
      end;

      with AddEntidad( enPrestacion,'PRESTACI','TB_CODIGO,PT_YEAR') do
      begin
           AddRelacion( enSSocial,'TB_CODIGO');
      end;

      with AddEntidad( enBitacora,'BITACORA','') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
           AddRelacion( enCompanys,'CM_CODIGO');
           AddRelacion( enProceso,'BI_NUMERO');
      end;

      with AddEntidad( enUsuarios,'USUARIO','US_CODIGO') do
      begin
           AddRelacion( enGrupos,'GR_CODIGO');
      end;

      with AddEntidad( enPrestamo,'PRESTAMO','CB_CODIGO,PR_TIPO,PR_REFEREN') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
           AddRelacion( enTPresta,'PR_TIPO');
      end;

      with AddEntidad( enMovimien,'MOVIMIEN','PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO,CO_NUMERO,MO_REFEREN') do
      begin
           AddRelacion( enNomina,'PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO');
           AddRelacion( enConcepto,'CO_NUMERO');
      end;



      with AddEntidad( enAntesCur,'ANTESCUR','CB_CODIGO,AR_FOLIO') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
      end;

      with AddEntidad( enAntesPto,'ANTESPTO','CB_CODIGO,AP_FOLIO') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
      end;

      with AddEntidad( enCampoRep,'CAMPOREP','RE_CODIGO,CR_TIPO,CR_POSICIO') do
      begin
           AddRelacion( enReporte,'RE_CODIGO');
      end;

      with AddEntidad( enKarFija,'KAR_FIJA','CB_CODIGO,KF_FOLIO,CB_FECHA,CB_TIPO') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
           AddRelacion( enOtrasPer,'KF_CODIGO');
           AddRelacion( enKardex,'CB_CODIGO,CB_FECHA,CB_TIPO');
           AddRelacion( enTKardex,'CB_TIPO');
      end;

      with AddEntidad( enImagen,'IMAGEN','') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
      end;

      with AddEntidad( enPCarAbo,'PCAR_ABO','') do
      begin
           AddRelacion( enPrestamo,'CB_CODIGO,PR_TIPO,PR_REFEREN');
      end;

      with AddEntidad( enPariente,'PARIENTE','CB_CODIGO,PA_RELACIO,PA_FOLIO') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
      end;

      with AddEntidad( enPRiesgo,'PRIESGO','TB_CODIGO,RT_FECHA') do
      begin
           AddRelacion( enRPatron,'TB_CODIGO');
      end;

      with AddEntidad( enAcceso,'ACCESO','RE_CODIGO') do
      begin
           AddRelacion( enCompanys,'CM_CODIGO');
           AddRelacion( enGrupos,'GR_CODIGO');
      end;

      with AddEntidad( enMovimienLista,'TMPLISTA','') do
      begin
           AddRelacion( enNomina,'PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO');
           AddRelacion( enConcepto, 'CO_NUMERO' );
           AddRelacion( enTemporalNomina, 'TA_USER,TA_NIVEL1,TA_NIVEL2,TA_NIVEL3,TA_NIVEL4,TA_NIVEL5,CB_CODIGO' );
      end;

      with AddEntidad( enListadoNomina,'TMPBALA2','') do
      begin
           AddRelacion( enNomina,'PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO');
      end;

      with AddEntidad( enMovimienBalanzaMensual,'TMPBALAMES','') do
      begin
           AddRelacion( enNomMes,'PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO');
      end;

      with AddEntidad( enNomMes,'V_NOMMES','PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
           AddRelacion( enPuesto,'CB_PUESTO');
           AddRelacion( enTurno,'CB_TURNO');
           AddRelacion( enClasifi,'CB_CLASIFI');
           AddRelacion( enRPatron,'CB_PATRON');
           AddRelacion( enNivel1,'CB_NIVEL1');
           AddRelacion( enNivel2,'CB_NIVEL2');
           AddRelacion( enNivel3,'CB_NIVEL3');
           AddRelacion( enNivel4,'CB_NIVEL4');
           AddRelacion( enNivel5,'CB_NIVEL5');
           AddRelacion( enNivel6,'CB_NIVEL6');
           AddRelacion( enNivel7,'CB_NIVEL7');
           AddRelacion( enNivel8,'CB_NIVEL8');
           AddRelacion( enNivel9,'CB_NIVEL9');
           {$ifdef ACS}
           AddRelacion( enNivel10,'CB_NIVEL10');{OP: 4.Ago.08}
           AddRelacion( enNivel11,'CB_NIVEL11');{OP: 4.Ago.08}
           AddRelacion( enNivel12,'CB_NIVEL12');{OP: 4.Ago.08}
           {$endif}
      end;

      with AddEntidad( enMovimienBalanza,'TMPBALAN','') do
      begin
           AddRelacion( enNomina,'PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO');
      end;

      with AddEntidad( enEstadoAhorro,'TMPESTAD','TE_USER,TE_CODIGO') do
      begin
           AddRelacion( enAhorro,'CB_CODIGO,TE_TIPO');
      end;

      with AddEntidad( enEstadoPrestamo,'TMPESTAB','TE_USER,TE_CODIGO') do
      begin
           AddRelacion( enPrestamo,'CB_CODIGO,TE_TIPO,TE_REFEREN');
      end;

      with AddEntidad( enCurProg,'CUR_PROG','') do
      begin
           AddRelacion( enCurso,'CU_CODIGO');
           AddRelacion( enEmpleado,'CB_CODIGO');
           AddRelacion( enMaestros,'MA_CODIGO');
      end;

      with AddEntidad( enEmpProg,'EMP_PROG','') do
      begin
           AddRelacion( enCurso,'CU_CODIGO');
           AddRelacion( enEmpleado,'CB_CODIGO');
      end;

      with AddEntidad( enPoliza,'POLIZA','PE_YEAR,PE_TIPO,PE_NUMERO,PT_CODIGO,TP_FOLIO') do
      begin
           AddRelacion( enPolHead,'PE_YEAR,PE_TIPO,PE_NUMERO,PT_CODIGO');
      end;

      with AddEntidad( enDemografica,'TMPDEMO','TD_USER,TD_GRUPO') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
      end;

      with AddEntidad( enRepAhorro,'REP_AHO','') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
      end;

      with AddEntidad( enAguinaldo,'AGUINAL','AG_YEAR,CB_CODIGO') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
      end;

      with AddEntidad( enRotacion,'TMPROTA','TR_GRUPO') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
      end;

      with AddEntidad( enCalendario,'TMPCALEN','US_CODIGO,CB_CODIGO,TL_YEAR,TL_MES') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
      end;

      with AddEntidad( enCalendarioHoras,'TMPCALHR','US_CODIGO,CB_CODIGO,TL_YEAR,TL_MES,TL_CAMPO') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
      end;

      with AddEntidad( enCompara,'COMPARA','CB_CODIGO, CP_YEAR') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
      end;

      with AddEntidad( enDeclara,'DECLARA','CB_CODIGO, DC_YEAR') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
      end;

      with AddEntidad( enRepartoPTU,'REP_PTU','') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
      end;

      with AddEntidad( enRegla,'CAFREGLA','CL_CODIGO') do
      begin
           AddRelacion( enQuerys,'CL_QUERY');
      end;

      with AddEntidad( enInvitacion,'CAF_INV','IV_CODIGO,CF_FECHA,CF_HORA,CF_TIPO') do
      begin
           AddRelacion( enInvitador,'IV_CODIGO');
           AddRelacion( enRegla,'CL_CODIGO');
      end;

      with AddEntidad( enComida,'CAF_COME','CB_CODIGO,CF_FECHA,CF_HORA,CF_TIPO') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
      end;

      with AddEntidad( enInvitador,'INVITA','IV_CODIGO') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
      end;

      with AddEntidad( enAsigna,'ASIGNA','') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
      end;

      with AddEntidad( enFolio,'FOLIO','FL_CODIGO') do
      begin
           AddRelacion( enReporte,'FL_REPORTE');
      end;

      with AddEntidad( enOrdFolio,'ORDFOLIO','FL_CODIGO,OF_POSICIO') do
      begin
           AddRelacion( enFolio,'FL_CODIGO');
      end;

      with AddEntidad( enConcilia,'TMPFOLIO','TO_USER,TO_FOLIO,CB_CODIGO') do
      begin
           AddRelacion( enNomina,'PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO');
      end;

      AddEntidad( enModula1, 'MODULA1','TB_CODIGO' );
      AddEntidad( enModula2, 'MODULA2','TB_CODIGO' );
      AddEntidad( enModula3, 'MODULA3','TB_CODIGO' );
      AddEntidad( enTMuerto, 'TMUERTO','TB_CODIGO' );
      AddEntidad( enTParte, 'TPARTE','TT_CODIGO' );
      AddEntidad( enTOpera, 'TOPERA','TO_CODIGO' );

      with AddEntidad( enPartes, 'PARTES','AR_CODIGO' ) do
      begin
           AddRelacion( enTParte,'TT_CODIGO')
      end;

      with AddEntidad( enOpera, 'OPERA','OP_NUMBER' ) do
      begin
           AddRelacion( enTOpera,'TO_CODIGO');
      end;

      with AddEntidad( enOrdenesFijas, 'WOFIJA','CB_CODIGO,WO_NUMBER' ) do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
           AddRelacion( enOpera,'OP_NUMBER');
           AddRelacion( enPartes,'AR_CODIGO');
           AddRelacion( enWorder,'WO_NUMBER');
      end;

      with AddEntidad( enWorks, 'WORKS','CB_CODIGO,WK_FECHA_A,WK_HORA_A' ) do
      begin
           AddRelacion( enPartes,'AR_CODIGO');
           AddRelacion( enOpera,'OP_NUMBER');
           AddRelacion( enWorder,'WO_NUMBER');
           AddRelacion( enCedula,'WK_CEDULA');
           AddRelacion( enModula1, 'WK_MOD_1' );
           AddRelacion( enModula2, 'WK_MOD_2' );
           AddRelacion( enModula3, 'WK_MOD_3' );
           AddRelacion( enTMuerto, 'WK_TMUERTO' );
           AddRelacion( enArea,'CB_AREA');
           AddRelacion( enPuesto,'CB_PUESTO');
           AddRelacion( enAusencia,'CB_CODIGO,AU_FECHA');
      end;

      with AddEntidad( enLecturas, 'LECTURAS','CB_CODIGO,LX_FECHA,LX_HORA,LX_WORDER,LX_OPERA' ) do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
           AddRelacion( enOpera,'LX_OPERA');
           AddRelacion( enWorder,'LX_WORDER');
           AddRelacion( enModula1, 'LX_MODULA1' );
           AddRelacion( enModula2, 'LX_MODULA2' );
           AddRelacion( enModula3, 'LX_MODULA3' );
           AddRelacion( enTMuerto, 'LX_TMUERTO' );
      end;

      with AddEntidad( enDefSteps, 'DEFSTEPS','DF_SEQUENC,AR_CODIGO' ) do
      begin
           AddRelacion( enOpera,'OP_NUMBER');
           AddRelacion( enPartes,'AR_CODIGO');
      end;

      with AddEntidad( enSteps, 'STEPS','WO_NUMBER,ST_SEQUENC' ) do
      begin
           AddRelacion( enWorder,'WO_NUMBER');
           AddRelacion( enOpera,'OP_NUMBER');
      end;

      with AddEntidad( enArea, 'AREA','TB_CODIGO' ) do
      begin
           AddRelacion( enNivel1,'CB_NIVEL1');
           AddRelacion( enNivel2,'CB_NIVEL2');
           AddRelacion( enNivel3,'CB_NIVEL3');
           AddRelacion( enNivel4,'CB_NIVEL4');
           AddRelacion( enNivel5,'CB_NIVEL5');
           AddRelacion( enNivel6,'CB_NIVEL6');
           AddRelacion( enNivel7,'CB_NIVEL7');
           AddRelacion( enNivel8,'CB_NIVEL8');
           AddRelacion( enNivel9,'CB_NIVEL9');
           {$ifdef ACS}
           AddRelacion( enNivel10,'CB_NIVEL10');{OP: 4.Ago.08}
           AddRelacion( enNivel11,'CB_NIVEL11');{OP: 4.Ago.08}
           AddRelacion( enNivel12,'CB_NIVEL12');{OP: 4.Ago.08}
           {$endif}
      end;

      with AddEntidad( enWorder, 'WORDER','WO_NUMBER' ) do
      begin
           AddRelacion( enPartes,'AR_CODIGO');
      end;

      with AddEntidad( enCedula, 'CEDULA','CE_FOLIO' ) do
      begin
           AddRelacion( enOpera,'OP_NUMBER');
           AddRelacion( enPartes,'AR_CODIGO');
           AddRelacion( enArea,'CE_AREA');
           AddRelacion( enModula1, 'CE_MOD_1' );
           AddRelacion( enModula2, 'CE_MOD_2' );
           AddRelacion( enModula3, 'CE_MOD_3' );
           AddRelacion( enTMuerto, 'CE_TMUERTO' );
           AddRelacion( enWorder,'WO_NUMBER');
      end;

      with AddEntidad( enCedulaEmpleado, 'CED_EMP','CE_FOLIO,CB_CODIGO' ) do
      begin
           AddRelacion( enCedula,'CE_FOLIO');
           AddRelacion( enEmpleado,'CB_CODIGO');
      end;

      with AddEntidad( enCedulaWOrder, 'CED_WORD','CE_FOLIO,WO_NUMBER' ) do
      begin
           AddRelacion( enCedula,'CE_FOLIO');
           AddRelacion( enWorder,'WO_NUMBER');
      end;

      AddEntidad( enBreak, 'BREAKS', 'BR_CODIGO');
      with AddEntidad( enBrkHora, 'BRK_HORA' , 'BR_CODIGO,BR_INICIO') do
      begin
           AddRelacion( enBreak, 'BR_CODIGO');
      end;

      with AddEntidad( enSupArea, 'SUP_AREA', 'CB_NIVEL,CB_AREA' ) do
      begin
           AddRelacion( enArea,'CB_AREA');
//           AddRelacion( enNivelX,'CB_NIVEL');  PENDIENTE: Investigar como relacionar la entidad enNivel que corresponda
      end;

      with AddEntidad( enKardexArea, 'KAR_AREA', 'CB_CODIGO,KA_FECHA,KA_HORA' ) do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
           AddRelacion( enArea,'CB_AREA');
      end;

      with AddEntidad( enPtoFijas,'PTOFIJAS', 'PF_FOLIO,PU_CODIGO' ) do
      begin
           AddRelacion( enPuesto, 'PU_CODIGO' );
           AddRelacion( enOtrasPer, 'PF_CODIGO' );
      end;

      with AddEntidad( enPtoTools, 'PTOTOOLS', 'PU_CODIGO, TO_CODIGO' ) do
      begin
           AddRelacion( enPuesto, 'PU_CODIGO' );
           AddRelacion( enTools, 'TO_CODIGO');
      end;

      with AddEntidad( enMaestros,'MAESTRO','MA_CODIGO') do
      begin
           AddRelacion( enProvCap, 'PC_CODIGO');
      end;

      AddEntidad( enEdoCivil,'EDOCIVIL','TB_CODIGO');
      AddEntidad( enEntidad,'ENTIDAD','TB_CODIGO');
      with AddEntidad( enMunicipio,'MUNICIPIO','TB_CODIGO')do
      begin
           AddRelacion( enEntidad, 'TB_ENTIDAD');
      end;

      AddEntidad( enEstudios,'ESTUDIOS','TB_CODIGO');

      AddEntidad( enExtra1,'EXTRA1','TB_CODIGO');
      AddEntidad( enExtra2,'EXTRA2','TB_CODIGO');
      AddEntidad( enExtra3,'EXTRA3','TB_CODIGO');
      AddEntidad( enExtra4,'EXTRA4','TB_CODIGO');
      AddEntidad( enExtra5,'EXTRA5','TB_CODIGO');
      AddEntidad( enExtra6,'EXTRA6','TB_CODIGO');
      AddEntidad( enExtra7,'EXTRA7','TB_CODIGO');
      AddEntidad( enExtra8,'EXTRA8','TB_CODIGO');
      AddEntidad( enExtra9,'EXTRA9','TB_CODIGO');
      AddEntidad( enExtra10,'EXTRA10','TB_CODIGO');
      AddEntidad( enExtra11,'EXTRA11','TB_CODIGO');
      AddEntidad( enExtra12,'EXTRA12','TB_CODIGO');
      AddEntidad( enExtra13,'EXTRA13','TB_CODIGO');
      AddEntidad( enExtra14,'EXTRA14','TB_CODIGO');

      AddEntidad( enViveEn,'VIVE_EN','TB_CODIGO');
      AddEntidad( enHorario,'HORARIO','HO_CODIGO');
      AddEntidad( enIncidencia,'INCIDEN','TB_CODIGO');
      AddEntidad( enLeyIMSS,'LEY_IMSS','SS_INICIAL');
      AddEntidad( enMotBaja,'MOT_BAJA','TB_CODIGO');
      AddEntidad( enMoneda,'MONEDA','TB_CODIGO');
      AddEntidad( enTKardex,'TKARDEX','TB_CODIGO');
      AddEntidad( enNivel1,'NIVEL1','TB_CODIGO');
      AddEntidad( enNivel2,'NIVEL2','TB_CODIGO');
      AddEntidad( enNivel3,'NIVEL3','TB_CODIGO');
      AddEntidad( enNivel4,'NIVEL4','TB_CODIGO');
      AddEntidad( enNivel5,'NIVEL5','TB_CODIGO');
      AddEntidad( enNivel6,'NIVEL6','TB_CODIGO');
      AddEntidad( enNivel7,'NIVEL7','TB_CODIGO');
      AddEntidad( enNivel8,'NIVEL8','TB_CODIGO');
      AddEntidad( enNivel9,'NIVEL9','TB_CODIGO');
      {$ifdef ACS}
      AddEntidad( enNivel10,'NIVEL10','TB_CODIGO');{OP: 4.Ago.08}
      AddEntidad( enNivel11,'NIVEL11','TB_CODIGO');{OP: 4.Ago.08}
      AddEntidad( enNivel12,'NIVEL12','TB_CODIGO');{OP: 4.Ago.08}
      {$endif}
      AddEntidad( enOtrasPer,'OTRASPER','TB_CODIGO');
      AddEntidad( enPeriodo,'PERIODO','PE_YEAR,PE_TIPO,PE_NUMERO', 'Per�odo de N�mina' );
      AddEntidad( enQuerys,'QUERYS','QU_CODIGO');
      with AddEntidad( enRPatron,'RPATRON','TB_CODIGO') do
      begin
           AddRelacion( enRSocial, 'RS_CODIGO');
      end;
      AddEntidad( enSal_Min,'SAL_MIN','');
      AddEntidad( enTCurso,'TCURSO','TB_CODIGO');
      AddEntidad( enCCurso,'CCURSO','TB_CODIGO');
      AddEntidad( enTransporte,'TRANSPOR','TB_CODIGO');
      AddEntidad( enViveCon,'VIVE_CON','TB_CODIGO');
      AddEntidad( enContrato,'CONTRATO','TB_CODIGO');
      AddEntidad( enNumerica,'NUMERICA','NU_CODIGO');
      AddEntidad( enRiesgo,'RIESGO','');
      AddEntidad( enCompanys,'COMPANY','CM_CODIGO');
      AddEntidad( enGrupos,'GRUPO','GR_CODIGO');
      //AddEntidad( enGeneral,'','');      //CETYS
      AddEntidad( enGlobal,'GLOBAL','');
      AddEntidad( enDiccion,'DICCION','DI_CLASIFI,DI_NOMBRE');
      //AddEntidad( enConfigura,'Configura','');   //CETYS
      AddEntidad( enAusenciaRL,'','');
      AddEntidad( enChecadasRL,'','');
      AddEntidad( enProceso,'PROCESO','PC_NUMERO');
      AddEntidad( enNomParam,'NOMPARAM','NP_FOLIO');
      AddEntidad( enMovimienG,'','PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO,CO_NUMERO,MO_REFEREN');
      AddEntidad( enTemporalNomina,'TMPNOM','TA_USER,TA_NIVEL1,TA_NIVEL2,TA_NIVEL3,TA_NIVEL4,TA_NIVEL5,CB_CODIGO');
      AddEntidad( enHorarioTemp,'','');
      AddEntidad( enChecadasG,'','');
      AddEntidad( enArbol,'','US_CODIGO,AR_ORDEN');
      AddEntidad( enImpresora,'PRINTER','PI_NOMBRE');
      AddEntidad( enSuper,'SUPER','');
      AddEntidad( enMovGral,'MOV_GRAL','PE_YEAR,PE_TIPO,PE_NUMERO,CO_NUMERO');
      {CV; Se elimino esta entidad, por que mete ruido con la Entidad de
      FALTAS. La entidad FALTASG ya no se usa en 2.0, se tenia por compatibilidad
      con 1.3}
      //AddEntidad( enFaltasG,'FALTAS','FA_MOTIVO,FA_FEC_INI,PE_YEAR,CB_CODIGO,PE_TIPO,PE_NUMERO');
      AddEntidad( enTFijas,'TFIJAS','TF_CODIGO,TF_TABLA');
      AddEntidad( enTCambio,'TCAMBIO','TC_FEC_INI');
      AddEntidad( enMotAuto,'MOT_AUTO','TB_CODIGO');
      AddEntidad( enConteo,'CONTEO','CT_FECHA');
      AddEntidad( enCorreo,'CORREO','');
      AddEntidad( enTPeriodo,'TPERIODO','TP_TIPO');
      AddEntidad( enAutorizaciones,'','');
      AddEntidad( enNominaPR,'','');
      AddEntidad( enSSocial,'SSOCIAL','TB_CODIGO');
      AddEntidad( enClasifi,'CLASIFI','TB_CODIGO');
      AddEntidad( enTurno,'TURNO','TU_CODIGO', 'Turno' );
      AddEntidad( enColonia,'COLONIA','TB_CODIGO' );
      AddEntidad( enMotCheca,'MOT_CHECA','TB_CODIGO');

      {$ifdef SERVERCAFE}
      with AddEntidad( enTarjetaXY,'TARJETAXY','CB_CODIGO,AU_FECHA', 'TarjetaX') do
      begin
           AddRelacion( enHorario,'HO_CODIGO');
           AddRelacion( enIncidencia,'AU_TIPO');
      end;
      {$endif}

      with AddEntidad( enKarTool, 'KAR_TOOL', 'CB_CODIGO' ) do
      begin
           AddRelacion( enEmpleado, 'CB_CODIGO' );
           AddRelacion( enTools, 'TO_CODIGO' );
           AddRelacion( enMotTool,'KT_MOT_FIN' );
           AddRelacion( enTalla, 'KT_TALLA' );
      end;

      with AddEntidad( enMotTool,'MOT_TOOL','TB_CODIGO') do
      begin
           AddRelacion( enTPresta,'TB_PRESTA');
      end;

      AddEntidad( enTalla, 'TALLA', 'TB_CODIGO');
      AddEntidad( enTools, 'TOOL', 'TO_CODIGO');

      with AddEntidad( enSuscrip,'SUSCRIP','RE_CODIGO,US_CODIGO') do
      begin
           AddRelacion( enReporte,'RE_CODIGO');
      end;
      AddEntidad( enFoto,'FOTO','CB_CODIGO');


      AddEntidad( enTAccidente, 'TACCIDEN', 'TB_CODIGO' );
      AddEntidad( enCausaAccidente, 'CAUSACCI', 'TB_CODIGO' );
      AddEntidad( enMotivoAcc, 'MOTACCI', 'TB_CODIGO' );
      AddEntidad( enTEstudio, 'TESTUDIO', 'TB_CODIGO' );
      AddEntidad( enTConsulta, 'TCONSLTA', 'TB_CODIGO' );
      AddEntidad( enDiagnostico, 'DIAGNOST', 'DA_CODIGO' );

      with AddEntidad( enExpediente, 'EXPEDIEN', 'EX_CODIGO' ) do
      begin
           AddRelacion( enEmpleado, 'CB_CODIGO' );
      end;

      with AddEntidad( enConsulta, 'CONSULTA', 'EX_CODIGO,CN_FECHA' ) do
      begin
           AddRelacion( enExpediente, 'EX_CODIGO' );
           AddRelacion( enTEstudio, 'CN_EST_TIP' );
           AddRelacion( enTConsulta, 'CN_TIPO' );
           AddRelacion( enDiagnostico, 'DA_CODIGO' );
      end;

      with AddEntidad( enEmbarazo, 'EMBARAZO', 'EX_CODIGO,EM_FEC_UM' ) do
      begin
           AddRelacion( enExpediente, 'EX_CODIGO' );
      end;

      with AddEntidad( enAccidente, 'ACCIDENT', 'EX_CODIGO,AX_FECHA' ) do
      begin
           AddRelacion( enExpediente, 'EX_CODIGO' );
           AddRelacion( enTAccidente, 'AX_TIP_ACC' );
           AddRelacion( enCausaAccidente, 'AX_CAUSA' );
           AddRelacion( enMotivoAcc, 'AX_MOTIVO' );
      end;

      AddEntidad( enMedicina, 'MEDICINA', 'ME_CODIGO' );
      with AddEntidad( enMedEntregada, 'MED_ENTR', 'ME_CODIGO,MT_FECHA,EX_CODIGO' ) do
      begin
           AddRelacion( enExpediente, 'EX_CODIGO' );
           AddRelacion( enMedicina, 'ME_CODIGO' );
      end;


      AddEntidad( enGrupoEx, 'GRUPO_EX', 'GX_CODIGO' );
      with AddEntidad( enCampoEx, 'CAMPO_EX', 'CX_NOMBRE' ) do
      begin
           AddRelacion( enGrupoEx, 'GX_CODIGO' );
      end;

      AddEntidad( enTDefecto, 'TDEFECTO', 'TB_CODIGO' );
      with AddEntidad( enDefecto, 'DEFECTO', 'CI_FOLIO,DE_FOLIO' ) do
      begin
           AddRelacion( enTDefecto, 'DE_CODIGO' );
           AddRelacion( enCedulaInspeccion, 'CI_FOLIO' );
      end;

      with AddEntidad( enCedulaInspeccion, 'CED_INSP', 'CI_FOLIO' ) do
      begin
           AddRelacion( enPartes,'AR_CODIGO');
           AddRelacion( enArea,'CI_AREA');
           AddRelacion( enWorder,'WO_NUMBER');
           //AddRelacion( enCedula,'CE_FOLIO');
      end;

      with AddEntidad( enCedulaScrap, 'CEDSCRAP', 'CS_FOLIO' ) do
      begin
           AddRelacion( enPartes,'AR_CODIGO');
           AddRelacion( enArea,'CS_AREA');
           AddRelacion( enWorder,'WO_NUMBER');
           AddRelacion( enOpera,'OP_NUMBER');
      end;

      with AddEntidad( enScrap, 'SCRAP', 'CS_FOLIO,SC_FOLIO' ) do
      begin
           AddRelacion( enCedulaScrap,'CS_FOLIO');
           AddRelacion( enComponentes,'CN_CODIGO');
           AddRelacion( enMotivoScrap,'SC_MOTIVO');
      end;

      AddEntidad( enComponentes, 'COMPONEN', 'CN_CODIGO' );
      AddEntidad( enMotivoScrap, 'MOTSCRAP', 'TB_CODIGO' );

      AddEntidad( enFamiliasPuesto, 'FAM_PTO', 'FP_CODIGO');
      AddEntidad( enNivelesPuesto, 'NIV_PTO', 'NP_CODIGO' );
      AddEntidad( enDimensiones,  'DIMENSIO', 'DM_CODIGO' );

      with AddEntidad( enPuestoDimension, 'PTO_DIME', 'PU_CODIGO,DM_CODIGO' ) do
      begin
           AddRelacion( enPuesto,'PU_CODIGO');
           AddRelacion( enDimensiones,'DM_CODIGO');
      end;

      AddEntidad( enCalifica,  'CALIFICA', 'CA_CODIGO' );
      AddEntidad( enTCompetencia, 'TCOMPETE', 'TC_CODIGO' );

      with AddEntidad( enCompetencias, 'COMPETEN', 'CM_CODIGO' ) do
      begin
           AddRelacion( enTCompetencia,'TC_CODIGO');
      end;

      with AddEntidad( enCompeteFamilia, 'COMP_FAM', 'FP_CODIGO,CM_CODIGO' ) do
      begin
           AddRelacion( enFamiliasPuesto,'FP_CODIGO');
           AddRelacion( enCompetencias,'CM_CODIGO');
      end;

      with AddEntidad( enCompeteCalifica, 'COMP_CAL', 'CM_CODIGO,CA_CODIGO' ) do
      begin
           AddRelacion( enCompetencias,'CM_CODIGO');
           AddRelacion( enCalifica,'CA_CODIGO');
      end;

      with AddEntidad( enCompetePuesto,  'COMP_PTO', 'PU_CODIGO,CM_CODIGO' ) do
      begin
            AddRelacion( enPuesto,'PU_CODIGO');
            AddRelacion( enCompetencias,'CM_CODIGO');
            AddRelacion( enCalifica,'CA_CODIGO');
      end;

      with AddEntidad( enEmpleadoCompete,  'EMP_COMP', 'CB_CODIGO,CM_CODIGO' ) do
      begin
            AddRelacion( enEmpleado,'CB_CODIGO');
            AddRelacion( enCompetencias,'CM_CODIGO');
            AddRelacion( enCalifica,'CA_CODIGO');
      end;

      with AddEntidad( enAcciones, 'ACCION', 'AN_CODIGO' ) do
      begin
            AddRelacion( enCurso,'CU_CODIGO');
      end;

      with AddEntidad( enEmpleadoPlan,  'EMP_PLAN', 'CB_CODIGO,AN_CODIGO' ) do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
           AddRelacion( enAcciones,'AN_CODIGO');
      end;

      with AddEntidad( enCompeteMapa, 'COMP_MAP', 'CM_CODIGO,AN_CODIGO' ) do
      begin
           AddRelacion( enCompetencias,'CM_CODIGO');
           AddRelacion( enAcciones,'AN_CODIGO');
      end;

      with AddEntidad( enNivelDimension,   'NIV_DIME', 'DM_CODIGO,NP_CODIGO' ) do
      begin
           AddRelacion( enNivelesPuesto,'NP_CODIGO');
           AddRelacion( enDimensiones,'DM_CODIGO');
      end;

      AddEntidad( enDeclaraAnualCierre, 'TMPDIMMTOT', 'TD_YEAR,US_CODIGO' );

      with AddEntidad( enDeclaraAnual, 'TMPDIMM', 'TD_YEAR,US_CODIGO,CB_CODIGO' ) do
      begin
           AddRelacion( enEmpleado, 'CB_CODIGO' );
           AddRelacion( enCompara,  'CB_CODIGO,TD_YEAR');
           AddRelacion( enDeclaraAnualCierre,  'TD_YEAR,US_CODIGO');
      end;


      with AddEntidad( enSesion, 'SESION', 'SE_FOLIO' ) do
      begin
           AddRelacion( enCurso,'CU_CODIGO');
           AddRelacion( enPuesto,'CB_PUESTO');
           AddRelacion( enTurno,'CB_TURNO');
           AddRelacion( enClasifi,'CB_CLASIFI');
           AddRelacion( enNivel1,'CB_NIVEL1');
           AddRelacion( enNivel2,'CB_NIVEL2');
           AddRelacion( enNivel3,'CB_NIVEL3');
           AddRelacion( enNivel4,'CB_NIVEL4');
           AddRelacion( enNivel5,'CB_NIVEL5');
           AddRelacion( enNivel6,'CB_NIVEL6');
           AddRelacion( enNivel7,'CB_NIVEL7');
           AddRelacion( enNivel8,'CB_NIVEL8');
           AddRelacion( enNivel9,'CB_NIVEL9');
           {$ifdef ACS}
           AddRelacion( enNivel10,'CB_NIVEL10');{OP: 4.Ago.08}
           AddRelacion( enNivel11,'CB_NIVEL11');{OP: 4.Ago.08}
           AddRelacion( enNivel12,'CB_NIVEL12');{OP: 4.Ago.08}
           {$endif}
           AddRelacion( enMaestros,'MA_CODIGO');
           AddRelacion( enEstablecimiento,'SE_EST');

      end;
      // Seleccion
      AddEntidad( enSolicitud, 'SOLICITA', 'SO_FOLIO' );

      with AddEntidad( enAccRegla, 'ACCREGLA', 'AE_CODIGO' ) do
      begin
           AddRelacion( enQuerys,'QU_CODIGO');
      end;

      with AddEntidad( enAccesLog, 'ACCESLOG', 'CB_CODIGO,AL_FECHA,AL_HORA' ) do
      begin
           AddRelacion( enEmpleado, 'CB_CODIGO' );
           AddRelacion( enAccRegla, 'AE_CODIGO' );
           //AddRelacion( enNinguno, 'AL_CASETA' );
      end;

      with AddEntidad( enClasifiTemp, 'CLASITMP', 'CB_CODIGO, AU_FECHA' ) do
      begin
           AddRelacion( enEmpleado, 'CB_CODIGO' );
           AddRelacion( enClasifi,'CB_CLASIFI');
           AddRelacion( enPuesto,'CB_PUESTO');
           AddRelacion( enTurno,'CB_TURNO');
           AddRelacion( enNivel1,'CB_NIVEL1');
           AddRelacion( enNivel2,'CB_NIVEL2');
           AddRelacion( enNivel3,'CB_NIVEL3');
           AddRelacion( enNivel4,'CB_NIVEL4');
           AddRelacion( enNivel5,'CB_NIVEL5');
           AddRelacion( enNivel6,'CB_NIVEL6');
           AddRelacion( enNivel7,'CB_NIVEL7');
           AddRelacion( enNivel8,'CB_NIVEL8');
           AddRelacion( enNivel9,'CB_NIVEL9');
           {$ifdef ACS}
           AddRelacion( enNivel10,'CB_NIVEL10');{OP: 4.Ago.08}
           AddRelacion( enNivel11,'CB_NIVEL11');{OP: 4.Ago.08}
           AddRelacion( enNivel12,'CB_NIVEL12');{OP: 4.Ago.08}
           {$endif}
      end;

      AddEntidad( enGrupoAdicional, 'GRUPO_AD', 'GX_CODIGO' );

      with AddEntidad( enCampoAdicional, 'CAMPO_AD', 'CX_NOMBRE' ) do
      begin
           AddRelacion( enGrupoAdicional, 'GX_CODIGO' );
      end;

      //MA:5/Nov/20004:Relaciones para Evaluaci�n de Desempe�o
      AddEntidad( enEscalas, 'ESCALA', 'SC_CODIGO' );
      
      with AddEntidad( enEscNivel, 'ESCNIVEL', 'SC_CODIGO,SN_ORDEN' ) do
      begin
           AddRelacion( enEscalas, 'SC_CODIGO' );
      end;

      with AddEntidad( enPreguntas, 'PREGUNTA', 'PG_FOLIO' ) do
      begin
           AddRelacion( enCompetencias, 'CM_CODIGO' );
      end;

      with AddEntidad( enEncNive, 'ENC_NIVE', 'ET_CODIGO,EE_CODIGO,EL_ORDEN' ) do
      begin
           AddRelacion( enEncEsca, 'ET_CODIGO,EE_CODIGO' );
      end;

      with AddEntidad( enEncEsca, 'ENC_ESCA', 'ET_CODIGO,EE_CODIGO' ) do
      begin
           AddRelacion( enEncuesta, 'ET_CODIGO' );
      end;

      AddEntidad( enEncuesta, 'ENCUESTA', 'ET_CODIGO' );

      with AddEntidad( enEncComp, 'ENC_COMP', 'ET_CODIGO,EC_ORDEN' ) do
      begin
           AddRelacion( enEncuesta, 'ET_CODIGO' );
           AddRelacion( enCompetencias, 'CM_CODIGO' );
      end;

      with AddEntidad( enEncPreg, 'ENC_PREG', 'ET_CODIGO,EC_ORDEN,EP_ORDEN' ) do
      begin
           AddRelacion( enEncComp, 'ET_CODIGO,EC_ORDEN' );
           AddRelacion( enEncEsca, 'EE_CODIGO' );
           AddRelacion( enPreguntas, 'PG_FOLIO' );
      end;

      with AddEntidad( enEncRela, 'ENC_RELA', 'ET_CODIGO,ER_TIPO' ) do
      begin
           AddRelacion( enEncuesta, 'ET_CODIGO' );
      end;

      with AddEntidad( enSujeto, 'SUJETO', 'ET_CODIGO,CB_CODIGO' ) do
      begin
           AddRelacion( enEncuesta, 'ET_CODIGO' );
           AddRelacion( enEmpleado,'CB_CODIGO' );
      end;

      with AddEntidad( enEvalua, 'EVALUA', 'EV_FOLIO' ) do
      begin
           AddRelacion( enEncuesta, 'ET_CODIGO' );
           AddRelacion( enSujeto, 'ET_CODIGO,CB_CODIGO' );
      end;

      with AddEntidad( enSujComp, 'SUJ_COMP', 'ET_CODIGO,CB_CODIGO,EC_ORDEN' ) do
      begin
           AddRelacion( enEncComp, 'ET_CODIGO,EC_ORDEN' );
           AddRelacion( enSujeto, 'ET_CODIGO,CB_CODIGO' );
      end;

      with AddEntidad( enSujPreg, 'SUJ_PREG', 'ET_CODIGO,CB_CODIGO,EC_ORDEN,EP_ORDEN' ) do
      begin
           AddRelacion( enSujComp, 'ET_CODIGO,CB_CODIGO,EC_ORDEN' );
           AddRelacion( enEncPreg, 'ET_CODIGO,EC_ORDEN,EP_ORDEN' );
      end;

      with AddEntidad( enEvaComp, 'EVA_COMP', 'EV_FOLIO,VC_ORDEN' ) do
      begin
           AddRelacion( enEvalua, 'EV_FOLIO' );
      end;

      with AddEntidad( enEvaPreg, 'EVA_PREG', 'EV_FOLIO,VC_ORDEN,VP_ORDEN' ) do
      begin
           AddRelacion( enEvaComp, 'EV_FOLIO,VC_ORDEN' );
      end;

      with AddEntidad( enVEvaComp, 'V_EVA_COMP', '' ) do
      begin
           AddRelacion( enEvalua, 'EV_FOLIO' );
           AddRelacion( enEncuesta, 'ET_CODIGO' );
           AddRelacion( enEmpleado, 'CB_CODIGO' );
           AddRelacion( enEncComp, 'CM_CODIGO' );
      end;

      with AddEntidad( enVEvaPreg, 'V_EVA_PREG', '' ) do
      begin
           AddRelacion( enEvalua, 'EV_FOLIO' );
           AddRelacion( enEncuesta, 'ET_CODIGO' );
           AddRelacion( enEmpleado, 'CB_CODIGO' );
           AddRelacion( enEncComp, 'CM_CODIGO' );
           AddRelacion( enPreguntas, 'PG_FOLIO' );
      end;

      with AddEntidad( enVCompetencias, 'V_COMPETEN', 'CM_CODIGO' ) do
      begin
           AddRelacion( enTCompetencia, 'TC_CODIGO');
      end;
      {$ifdef FALSE}
      with AddEntidad( enVEvalua, 'V_EVALUA', 'EV_FOLIO' ) do
      begin
           AddRelacion( enEncuesta, 'ET_CODIGO' );
           AddRelacion( enEmpleado, 'CB_CODIGO' );
           AddRelacion( enSujeto, 'ET_CODIGO,CB_CODIGO' );
           AddRelacion( enPuesto, 'CB_PUESTO' );
           AddRelacion( enTurno, 'CB_TURNO' );
           AddRelacion( enClasifi, 'CB_CLASIFI' );
           AddRelacion( enNivel1, 'CB_NIVEL1' );
           AddRelacion( enNivel2, 'CB_NIVEL2' );
           AddRelacion( enNivel3, 'CB_NIVEL3' );
           AddRelacion( enNivel4, 'CB_NIVEL4' );
           AddRelacion( enNivel5, 'CB_NIVEL5' );
           AddRelacion( enNivel6, 'CB_NIVEL6' );
           AddRelacion( enNivel7, 'CB_NIVEL7' );
           AddRelacion( enNivel8, 'CB_NIVEL8' );
           AddRelacion( enNivel9, 'CB_NIVEL9' );
           {$ifdef ACS}
           AddRelacion( enNivel10,'CB_NIVEL10');{OP: 4.Ago.08}
           AddRelacion( enNivel11,'CB_NIVEL11');{OP: 4.Ago.08}
           AddRelacion( enNivel12,'CB_NIVEL12');{OP: 4.Ago.08}
           {$endif}
           AddRelacion( enEncRela, 'ET_CODIGO,EV_RELACIO' );
           {$ifdef HACE_FALTA_REVISAR_V_EVALUA_PARA_INCLUIR_ENCUESTA.ET_ESC_CAL_EN_EL_SELECT}
           AddRelacion( enEncNive, 'ET_CODIGO,ENCUESTA.ET_ESC_CAL,EV_CAL_FIN' );
           {$endif}
      end;
      {$else}
      AddEntidad( enVTCompete, 'V_TCOMPETE', 'TC_CODIGO' );      
      {$endif}

      AddEntidad( enAula,'AULA','AL_CODIGO');

      with AddEntidad( enReserva,'RESERVA','RV_FOLIO') do
      begin
	          AddRelacion( enAula, 'AL_CODIGO');
	          AddRelacion( enMaestros, 'MA_CODIGO');
	          AddRelacion( enSesion, 'SE_FOLIO' );
      end;

      with AddEntidad( enCursoPre,'CURSOPRE','CU_CODIGO') do
      begin
           AddRelacion( enCurso,'CU_CODIGO');
      end;

      with AddEntidad( enInscrito, 'INSCRITO', 'SE_FOLIO,CB_CODIGO' ) do
      begin
           AddRelacion( enEmpleado, 'CB_CODIGO' );
           AddRelacion( enSesion, 'SE_FOLIO' );
      end;

      with AddEntidad( enCurAsis, 'CUR_ASIS', 'RV_FOLIO,SE_FOLIO,CB_CODIGO' ) do
      begin
           AddRelacion( enInscrito, 'SE_FOLIO,CB_CODIGO' );
	          AddRelacion( enReserva, 'RV_FOLIO' );
      end;

      AddEntidad( enTCtaMovs,'TCTAMOVS','TB_CODIGO');

      with AddEntidad( enCtaBanco, 'CTABANCO', 'CT_CODIGO' ) do
      begin
           AddRelacion( enTAhorro, 'AH_TIPO' );
      end;
      
      with AddEntidad( enCtaMovs, 'CTA_MOVS', 'CM_FOLIO' ) do
      begin
           AddRelacion( enCtaBanco, 'CT_CODIGO' );
           AddRelacion( enTCtaMovs, 'CM_TIPO' );
           AddRelacion( enEmpleado, 'CB_CODIGO' );
           AddRelacion( enPrestamo,'CB_CODIGO,PR_TIPO,PR_REFEREN')
      end;

      AddEntidad( enCertific, 'CERTIFIC', 'CI_CODIGO' );

      with AddEntidad( enKarCert, 'KAR_CERT', 'KI_FEC_CER' ) do
      begin
           AddRelacion( enEmpleado, 'CB_CODIGO' );
           AddRelacion( enCertific, 'CI_CODIGO' );
      end;
      
      AddEntidad( enPolTipo, 'POL_TIPO', 'PT_CODIGO' );

      with AddEntidad( enPolHead, 'POL_HEAD', 'PE_YEAR,PE_TIPO,PE_NUMERO,PT_CODIGO' ) do
      begin
           AddRelacion( enPolTipo, 'PT_CODIGO' );
           AddRelacion( enPeriodo, 'PE_YEAR,PE_TIPO,PE_NUMERO' );
           AddRelacion( enReporte, 'PH_REPORTE' );
      end;

      with AddEntidad( enPlanVacacion, 'VACAPLAN', 'CB_CODIGO, VP_FEC_INI' ) do
      begin
           AddRelacion( enEmpleado, 'CB_CODIGO' );
      end;

      with AddEntidad( enPerfilPuesto, 'PERFIL', 'PU_CODIGO' ) do
      begin
           AddRelacion( enPuesto, 'PU_CODIGO' );
      end;

      AddEntidad( enSeccPerfil, 'DESCTIPO', 'DT_CODIGO' );


      with AddEntidad( enDescPerfil, 'DESC_PTO', 'PU_CODIGO,DT_CODIGO,DP_ORDEN' ) do
      begin
           AddRelacion( enSeccPerfil, 'DT_CODIGO' );
           AddRelacion( enPerfilPuesto, 'PU_CODIGO' );
      end;

      with AddEntidad( enCamposPerfil, 'DESC_FLD', 'DT_CODIGO,DF_ORDEN' ) do
      begin
           AddRelacion( enSeccPerfil, 'DT_CODIGO' );
      end;

      with AddEntidad( enPlazas, 'PLAZA', 'PL_FOLIO' ) do
      begin
           AddRelacion( enEmpleado, 'CB_CODIGO');
           AddRelacion( enPuesto,'PU_CODIGO');
           AddRelacion( enClasifi ,'PL_CLASIFI');
           AddRelacion( enTurno ,'PL_TURNO');
           AddRelacion( enRPatron ,'PL_PATRON');
           AddRelacion( enContrato ,'PL_CONTRAT');
           AddRelacion( enArea ,'PL_AREA');
           AddRelacion( enNivel1 ,'PL_NIVEL1');
           AddRelacion( enNivel2 ,'PL_NIVEL2');
           AddRelacion( enNivel3 ,'PL_NIVEL3');
           AddRelacion( enNivel4 ,'PL_NIVEL4');
           AddRelacion( enNivel5 ,'PL_NIVEL5');
           AddRelacion( enNivel6 ,'PL_NIVEL6');
           AddRelacion( enNivel7 ,'PL_NIVEL7');
           AddRelacion( enNivel8 ,'PL_NIVEL8');
           AddRelacion( enNivel9 ,'PL_NIVEL9');
           {$ifdef ACS}
           AddRelacion( enNivel10,'PL_NIVEL10');{OP: 4.Ago.08}
           AddRelacion( enNivel11,'PL_NIVEL11');{OP: 4.Ago.08}
           AddRelacion( enNivel12,'PL_NIVEL12');{OP: 4.Ago.08}
           {$endif}
      end;

      with AddEntidad( enPermutas, 'Z_PERMUTAS', 'ZP_FOLIO' ) do
      begin
           AddRelacion( enEmpleado, 'ZP_EMP_CUB' );
           AddRelacion( enEmpleado, 'ZP_EMPACUB' );
           AddRelacion( enTurno, 'ZP_TURNO' );
      end;

      AddEntidad( enKarPlaza, 'KARPLAZA', 'KP_FOLIO' );

      with AddEntidad( enRPromVar, 'TMPPROMVAR', 'CB_CODIGO,CB_FECHA,CB_TIPO,CO_NUMERO' ) do
      begin
           AddRelacion( enEmpleado, 'CB_CODIGO');
           AddRelacion( enConcepto, 'CO_NUMERO');
           AddRelacion( enKardex, 'CB_CODIGO,CB_FECHA,CB_TIPO');
      end;

      with AddEntidad( enProvCap, 'PROV_CAP', 'PC_CODIGO' ) do
      begin
           AddRelacion( enEntidad, 'PC_ESTADO');
      end;

      with AddEntidad( enFonTot,'FON_TOT','FT_YEAR,FT_MONTH,PR_TIPO') do
      begin
           AddRelacion( enTPresta,'PR_TIPO');
           //AddRelacion( enTPresta,'FT_TAJUST'); No se agrega esta relacion por que no puede relacionarse 2 veces a la misma tabla
      end;

      with AddEntidad( enFonEmp,'FON_EMP','FT_YEAR,FT_MONTH,PR_TIPO,CB_CODIGO') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
           AddRelacion( enFonTot,'FT_YEAR,FT_MONTH,PR_TIPO');
      end;

      with AddEntidad( enFonCre,'FON_CRE','FT_YEAR,FT_MONTH,PR_TIPO,CB_CODIGO,PR_REFEREN') do
      begin
           AddRelacion( enFonEmp,'FT_YEAR,FT_MONTH,PR_TIPO,CB_CODIGO');
           AddRelacion( enPrestamo,'CB_CODIGO,PR_TIPO,PR_REFEREN');
      end;

      with AddEntidad( enRSocial,'RSOCIAL','RS_CODIGO') do
      begin
           AddRelacion( enEntidad, 'RS_ENTIDAD');
      end;

      with AddEntidad( enCerProg,'CER_PROG','') do
      begin
           AddRelacion( enCertific, 'CI_CODIGO');
           AddRelacion( enEmpleado, 'CB_CODIGO');
      end;

      with AddEntidad( enMatrizCertif,'PTO_CERT','CI_CODIGO,PU_CODIGO') do
      begin
           AddRelacion( enCertific, 'CI_CODIGO');
           AddRelacion( enPuesto, 'PU_CODIGO');
      end;

      with AddEntidad( enVLiqEmp, 'V_LIQ_EMP', 'LS_MONTH, LS_YEAR, CB_CODIGO') do
      begin
           AddRelacion( enEmpleado, 'CB_CODIGO');
      end;

      with AddEntidad( enKarInf, 'KARINF', 'CB_CODIGO' ) do
      begin
           AddRelacion( enEmpleado, 'CB_CODIGO' );
      end;

      AddEntidad( enReglaPresta,'REGLAPRESTA','RP_CODIGO');

      with AddEntidad( enPrestaXRegla,'PRESTAXREG','RP_CODIGO,TB_CODIGO') do
      begin
           AddRelacion(enTPresta,'TB_CODIGO' );
           AddRelacion(enReglaPresta,'RP_CODIGO' );
      end;

      with AddEntidad( enHistRevision,'CUR_REV','CU_CODIGO,CH_REVISIO,CH_FECHA') do
      begin
           AddRelacion( enCurso,'CU_CODIGO');
      end;

      AddEntidad( enOcupaNac,'OCUPA_NAC','TB_CODIGO');
      AddEntidad( enAreaTematica, 'AR_TEM_CUR', 'TB_CODIGO');
      AddEntidad( enEstablecimiento, 'ESTABLEC', 'ES_CODIGO');

      AddEntidad( enTMaquina,'TMAQUINA','TB_CODIGO');
      AddEntidad( enLayout,'LAY_PRO_CE','LY_CODIGO');
      AddEntidad( enLayMaq,'LAY_MAQ','');
      AddEntidad( enLaySilla,'SILLA_LAY','');
      AddEntidad( enSillaMaq,'SILLA_MAQ','');
      with AddEntidad( enKarEmpSilla,'KAR_EMPSIL','') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
      end;

      with AddEntidad( enMaquina,'MAQUINA','MQ_CODIGO') do
      begin
           AddRelacion( enTMaquina,'MQ_TMAQUIN');
      end;

      with AddEntidad( enMaqCert,'MAQ_CERT','MQ_CODIGO,CI_CODIGO')do
      begin
           AddRelacion( enMaquina,'MQ_CODIGO');
      end;

      with AddEntidad( enDocumento,'DOCUMENTO','') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
      end;

      with AddEntidad( enHuella,'HUELLA','CB_CODIGO,NO_HUELLA','Huellas') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO', TRUE );
      end;

      AddEntidad( enCatSegGasMed,'POLIZA_MED','PM_CODIGO');

      with AddEntidad( enCatVigenciasSGM,'V_POL_VIG','PM_CODIGO,PV_REFEREN') do
      begin
           AddRelacion( enCatSegGasMed,'PM_CODIGO', TRUE );
      end;

      with AddEntidad( enHisSegGasMed,'V_EMP_POL','CB_CODIGO,PM_CODIGO,PV_REFEREN,EP_TIPO,PA_FOLIO,PA_RELACIO') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO', TRUE );
           AddRelacion( enCatSegGasMed,'PM_CODIGO', TRUE );
           AddRelacion( enPariente,'CB_CODIGO,PA_RELACIO,PA_FOLIO', TRUE );
      end;

      with AddEntidad( enSaldoVacaciones,'V_SALD_VAC','CB_CODIGO,VS_ANIV','SaldosVacacion') do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO',TRUE);
      end;

      with AddEntidad( enPrevioISR, 'ISR_AJUSTE', 'CB_CODIGO,IS_YEAR,IS_MES' ) do
      begin
           AddRelacion( enEmpleado,'CB_CODIGO');
      end;

      AddEntidad( enTPension, 'TPENSION', 'TP_CODIGO' );

      with AddEntidad( enPensiones, 'PENSION', 'CB_CODIGO,PS_ORDEN' ) do
      begin
           AddRelacion( enEmpleado, 'CB_CODIGO' );
      end;

      with AddEntidad( enPorcPensiones, 'PEN_PORCEN', 'TP_CODIGO,CB_CODIGO,PS_ORDEN' ) do
      begin
           AddRelacion( enTPension, 'TP_CODIGO' );
           AddRelacion( enPensiones, 'CB_CODIGO,PS_ORDEN' );
      end;

      AddEntidad(enCatTablasCotSGM,'AMORTIZ_TB','AT_CODIGO');

      with AddEntidad(enCatCotizacionSGM,'AMORTIZ_CS','AT_CODIGO,AC_EDADINI,AC_EDADFIN')do
      begin
           AddRelacion(enCatTablasCotSGM,'AT_CODIGO');
      end;
end;

end.
