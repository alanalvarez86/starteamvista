unit ZFuncsTress;

interface

uses Classes, Sysutils, DB, DBClient, Controls,
     ZetaQRExpr,
     ZEvaluador,
     DZetaServerProvider,
     ZetaCommonClasses,
     DateUtils;

{$define QUINCENALES}
{$define CAMBIO_TNOM}
{.$undefine QUINCENALES}
{.$define FLEXIBLES}
{.$undefine FLEXIBLES}

type
  //Esta aqui Arriba por que el reporteador hace uso de estas funciones.
  TZetaCBase = class( TZetaRegresaQuery )
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function GetScript : String; override;
  protected
    bLista, bCalcula: Boolean;
    sNumero, sTipo, sYear, sEmpleado : String;
    procedure SetEmpleado( const iParam: Byte );
    procedure SetDatos; virtual;
    function  EsNomina : Boolean;
    function  ParamPeriodo( const iParam : Integer; const sCampo : String ) : String;
    function  FijoPeriodo( const sCampo : String ) : String;
  end;

  TZetaCONCEPTO = class( TZetaCBase )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult;override;
  end;



procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );
procedure RegistraFuncionesPrestamo( FunctionLibrary: TQRFunctionLibrary );


implementation

uses ZSuperEvaluador,
     ZGlobalTress,
     ZetaSQLBroker,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaServerTools,
     ZFuncsGlobal,
     DExisteCampo,
     {$ifdef RDD}
     DEntidades,
     {$else}
     DEntidadesTress,
     {$endif}
     DTarjeta,
     DPrestaciones,
     DQueries,
     DCalculoNomina,
     ZFuncsTools;


{ ******************* Funciones Traspasadas de ZetaCommonTools ********************** }

function GetEquivaleAcumulado( sConcepto : String ) : Integer;

function PosAcum( const sAcumulados : String; nBase : Integer ) : Integer;
  var
     nPos : Integer;
  begin
       nPos := Pos( sConcepto, sAcumulados );
       if ( nPos > 0 ) then
          Result := nBase + ( nPos - 1 ) div 3
       else
           Result := 0;
  end;

begin
       Result := 0;
       if ( Length( sConcepto ) <> 2 ) then
          Exit;
       sConcepto := AnsiUpperCase( sConcepto );

       Result := PosAcum( 'T0,T1,TN,TX,TU,TM,TY,T2,T3,TZ', 1000 );
       if ( Result = 0 ) then
          Result := PosAcum( 'FI,FJ,DG,DP,DU,DO,DI,D7,DE,DR,DT,DS,NT,FV,DV,DA,DH,DM,DY', 1100 );
       if ( Result = 0 ) then
          Result := PosAcum( 'HT,HR,HE,H2,H3,HA,HS,HD,HX,XY,HF,HC,HU', 1200 );
end;

function DifMinutos( sHorario, sHora: String; nVacios: Integer; lNegativos: Boolean ) : Integer;
begin
     if StrVacio( sHorario ) or StrVacio( sHora ) then
        Result := nVacios
     else
     begin
          Result := AMinutos( sHora ) - AMinutos( sHorario );
          if ( Result < 0 ) and ( not lNegativos ) then
             Result := 0;
     end;
end;

{ ******************* Funciones Privadas ********************** }

function CalcDiasInca( dFecIni, dFecFin, dInicial, dFinal : TDate; iBrinca : Integer ) : Integer;
begin
     dInicial := rMax( dFecIni + iBrinca, dInicial );
     dFinal   := rMin( dFecFin - 1, dFinal );
     Result   := iMax( Trunc( dFinal-dInicial ) + 1, 0 );
end;

function CalculaPrettyName( sPaterno, sMaterno, sNombres : String ) : String; // Viene de ZetaCalculados de la 1.3
begin
     Result := Trim( sPaterno) + ' ' + Trim( sMaterno ) + ', ' +  Trim( sNombres) ;
end;

{ NOTAS:
  1) GetRequeridos es un Procedure
  2) Lo que antes se regresaba en Result, ahora se regresa en una serie
     de llamadas a 'AgregaRequerido' o 'AgregaRequeridoAlias'
  3) Las llamada a 'AgregaScript( x, resError, y )' se sustituyen por
     la llamada a 'EquivaleScript( x )';
}


function SetMesesRelativo( var nDeltaMes, iMesIni, iOffSet: Integer; const lIncluirMes13, lBrincaYear :Boolean; const iMesDefault: Integer  ): Boolean;
var
   iOffSetMes13: Integer;
begin
     iOffSet := 0;
     iOffSetMes13 := 0;

     if ( iMesIni  < 1 )then
     begin
          if lBrincaYear then
             iOffSet := -1;//a�o anterior

          if ( lIncluirMes13 )then
               iOffSetMes13 := 1;

          iMesIni := 12 + iOffSetMes13 + iMesIni
     end
     else
     begin
          if ( iMesIni > 12 )then
          begin
               if  NOT ( lIncluirMes13 and ( ( iMesIni ) = 13 ) )then
               begin
                    if( lIncluirMes13 and ( ( iMesIni ) > 13 ) )then
                    begin
                         iOffSetMes13 := 1
                    end;

                    if lBrincaYear then
                    begin
                         iOffSet := 1;
                         iMesIni := Abs( 12 + iOffSetMes13 - iMesDefault - nDeltaMes);
                    end;
               end;
          end;
     end;

     Result:= not ( ( iMesIni < 1 ) or ( iMesIni > 13 ) );
end;


{ ******************* TZetaRegresaEval **************************}

type
  TZetaRegresaEval = class( TZetaRegresa )
  protected
    oEvaluador : TZetaEvaluador;
    function Evalua( const sExpresion : String ) : TQREvResult;
  public
    constructor Create(oEvaluator:TQrEvaluator); override;
    destructor Destroy; override;
  end;

constructor TZetaRegresaEval.Create(oEvaluator:TQrEvaluator);
begin
     inherited Create(oEvaluator);
     oEvaluador := TZetaEvaluador.Create(ZetaEvaluador.oZetaCreator);
end;

destructor TZetaRegresaEval.Destroy;
begin
     oEvaluador.Free;
     inherited Destroy;
end;

function TZetaRegresaEval.Evalua( const sExpresion : String ) : TQREvResult;
begin
     if ( oEvaluador.DataSets.Count = 0 ) then
        oEvaluador.AgregaDataset( DataSetEvaluador );
     Result := oEvaluador.Calculate( sExpresion );
end;

{ ******************* TGetGlobal **************************}
{CV: 26-nov-2001}
{La funcion Global se transfiri� hacia la unidad ZFuncsGlobal.
Debido a que la poliza contable requeria esta funci�n y no se quiere
que la poliza haga un uses de todo ZFuncsTress.
}

{ ******************* TZetaLISTA **************************}

type
  TZetaLISTA = class( TZetaFunc )
  private
   oLista : TStringList;
  public
   constructor Create(oEvaluador:TQrEvaluator); override;
   destructor Destroy; override;
   function Calculate: TQREvResult; override;
   procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

constructor TZetaLISTA.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create(oEvaluador);
     oLista := TStringList.Create
end;

destructor TZetaLISTA.Destroy;
begin
     oLista.Free;
     inherited Destroy;
end;

procedure TZetaLISTA.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resBool;
     ParametrosFijos( 1 );
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

function TZetaLISTA.Calculate: TQREvResult;
var
   i : Integer;
   nEmpleado : Integer;
begin
     oLista.CommaText := ParamString( 0 );
     with Result do
     begin
          Kind := resBool;
          booResult := FALSE;
          nEmpleado := ZetaEvaluador.GetEmpleado;
          with oLista do
          begin
               CommaText := ParamString( 0 );
               for i := 0 to Count-1 do
               begin
                    if ( StrToInt( Strings[i] ) = nEmpleado ) then
                    begin
                         booResult := TRUE;
                         Exit;
                    end;
               end;
          end;
     end;
end;

{ ******************* TZetaAHORRO **************************}

type
  TZetaAHORRO = class( TZetaRegresaquery )
  private
   FTipo : string;
   FCampo : String;
   procedure CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQREvResultType; var lEsFecha : Boolean );
  public
   function GetScript : String; override;
   function Calculate: TQREvResult; override;
   procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaAHORRO.GetScript : String;
begin
     //Se sustituye el filtro de status, ya que puede ser que no se quiera incluir
     Result := 'select %s from AHORRO where AHORRO.CB_CODIGO = %s and AHORRO.AH_TIPO = %s and ' +
               '( (2 = %s) or (AHORRO.AH_STATUS=%s) )';
end;

procedure TZetaAHORRO.CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQREvResultType; var lEsFecha : Boolean );
begin
     Tipo := resDouble;
     lEsFecha := FALSE;
     case iRegresa of
          1: sCampo := 'SUM( AHORRO.AH_SALDO )';
          2: begin
                  sCampo := 'SUM( AHORRO.AH_NUMERO )';
                  Tipo := resInt;
             end;
          3: sCampo := 'SUM( AHORRO.AH_TOTAL )';
          4: sCampo := 'SUM( AHORRO.AH_SALDO_I )';
          5: sCampo := 'SUM( AHORRO.AH_ABONOS )';
          6: sCampo := 'SUM( AHORRO.AH_CARGOS )';
          7: begin
                  sCampo := 'AHORRO.AH_FECHA';
                  lEsFecha := TRUE;
             end;
          8: begin
                  sCampo := 'AH_FORMULA';
                  Tipo := resString;
             end;
          9: begin
                   sCampo := 'US_CODIGO';
                   Tipo := resInt;
             end
          else
          begin
                  sCampo := 'AH_SUB_CTA';
                  Tipo := resString;
          end

     end;
end;

procedure TZetaAHORRO.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sStatus : String;
   lEsFecha : Boolean;
begin
     ValidaConfidencial;
     ParametrosFijos( 1 );
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );

     if ParametrosConstantes then
     begin
          TipoRegresa := resDouble;
          if ParamEsCampo( 0 ) or ( Argument( 0 ).Kind = resString ) then
             FTipo := ParamCampoString( 0 )
          else
             FTipo := EntreComillas( IntToStr( ParamInteger( 0 )));
          if ParamVacio( 2 ) then
             sStatus := IntToStr( Ord( saActivo ))
          else
              sStatus  := ParamCampoInteger( 2 );


          CaseCampo( DefaultInteger( 1, 1 ), FCampo, TipoRegresa, lEsFecha );
          EsFecha:= lEsFecha;

          AgregaScript( Format( GetScript, [ FCampo, 'COLABORA.CB_CODIGO', FTipo,
                                             sStatus, sStatus ] ), TipoRegresa, lEsFecha );
     end;
end;

function TZetaAHORRO.Calculate: TQREvResult;
var
   TipoRegresa : TQREvResultType;
   lEsFecha : Boolean;
   sStatus: string;
begin
     if Argument(0).Kind = resInt then
        FTipo := IntToStr( ParamInteger( 0 ) )
     else
        FTipo := ParamString(0);

     CaseCampo( DefaultInteger( 1, 1 ), FCampo, TipoRegresa, lEsFecha );
     sStatus := IntToStr( DefaultInteger( 2, Ord( saActivo ) ) );
     EsFecha:= lEsFecha;
     Result := EvaluaQuery( Format( GetScript, [ CampoRes( FCampo ), IntToStr( ZetaEvaluador.GetEmpleado ), EntreComillas( FTipo ),
                                                 sStatus,sStatus  ] ) );
end;

{ ******************* TZetaPRESTAMO **************************}
type
  TZetaPRESTAMO = class( TZetaRegresaQuery )
  private
    bCalcula: Boolean;
    procedure CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQREvResultType; var lEsFecha : Boolean );
  public
    function GetScript : String; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaPRESTAMO.GetScript : String;
var
   nRegresa: Byte;

    function AgregaCondicion: String;
    var
       sReferen: String;
    begin
         if not ParamVacio(3) then
         begin
              if bCalcula then
                 Result := ' and PR.PR_REFEREN=' + EntreComillas( Trim( ParamString( 3 ) ) )
              else
              begin
                   if ParamEsCampo(3) then
                      sReferen := ParamNombreCampo(3)
                   else
                      sReferen := EntreComillas( Trim( ParamString(3) ) );
                   Result := ' and PR.PR_REFEREN=' + sReferen;
              end;
                 //Result := ' and PR.PR_REFEREN=' + ParamCampoString( 3 );
         end
         else if ( nRegresa in [ 6, 13, 14, 15 ] ) then
            Result := ' and PR.PR_REFEREN= (select min(PR2.PR_REFEREN) from PRESTAMO PR2 ' +
                           'where PR2.CB_CODIGO = PR.CB_CODIGO and PR2.PR_TIPO = PR.PR_TIPO ) '
         else
            Result := VACIO;
    end;

begin
     nRegresa:= DefaultInteger( 1, 1 );
     {$ifdef ANTES}
     Result := 'select %s from PRESTAMO PR where ( PR.CB_CODIGO = %s ) and ( PR.PR_TIPO = %s ) and ( PR.PR_STATUS = %s ) ' +
     {$else}
     Result := 'select %s from PRESTAMO PR where ( PR.CB_CODIGO = %s ) and ( PR.PR_TIPO = %s ) and '+
               'case %s when 3 then 3 else PR.PR_STATUS end = %s ' +
     {$endif}
                AgregaCondicion;

end;

procedure TZetaPRESTAMO.CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQREvResultType; var lEsFecha : Boolean );
begin
     Tipo := resDouble;
     lEsFecha := FALSE;
     case iRegresa of
          1: sCampo := 'SUM( PR.PR_MONTO )'; { PR_SUMA DE MONTO PRESTADO PARA EL TIPO DE PRESTAMO }
          2: sCampo := 'SUM( PR.PR_NUMERO )';  { NUMERO DE APORTACIONES PARA EL TIPO DE PRESTAMO }
          3: sCampo := 'SUM( PR.PR_TOTAL + PR.PR_SALDO_I )'; { SUMA DE MONTO ABONADO PARA EL TIPO DE PRESTAMO }
          4: begin
                  sCampo := 'COUNT(*)'; { NUMERO DE PRESTAMOS PARA EL TIPO DE PRESTAMO }
                  Tipo := resInt;
             end;
          5: sCampo := 'SUM( PR.PR_SALDO )'; { SUMA DE SALDOS PARA EL TIPO DE PRESTAMO }
          6: begin
                  sCampo := 'PR.PR_FORMULA'; { FORMULA DE ABONO DEL ULTIMO PRESTAMO }
                  Tipo := resString;
             end;
          7: sCampo := 'SUM( PR.PR_SALDO_I )'; { TOTAL ABONADO DEL A�O ANTERIOR }
          8: sCampo := 'SUM( PR.PR_ABONOS )'; { SUMA DE OTROS ABONOS }
          9: sCampo := 'SUM( PR.PR_CARGOS )'; { SUMA DE OTROS CARGOS }
         10: sCampo := 'SUM( PR.PR_MONTO + PR.PR_CARGOS )'; { SUMA DE PRESTAMO + CARGOS }
         11: sCampo := 'SUM( PR.PR_TOTAL + PR.PR_ABONOS )'; { SUMA DE ABONOS NOMINA Y OTROS ABONOS }
         12: sCampo := 'SUM( PR.PR_TOTAL )'; { ABONOS POR NOMINA ESTE A�O }
         13: begin
                  sCampo := 'PR.PR_FECHA'; { FECHA DEL PRESTAMO }
                  lEsFecha := TRUE;
             end;
         14: begin
                  sCampo := 'PR.US_CODIGO';
                  Tipo := resInt;
             end;
         15: begin
                  sCampo := 'PR.PR_SUB_CTA'; {Subcuenta contable}
                  Tipo := resString;
             end;
         16: sCampo := 'SUM( PR.PR_MONTO_S )'; {Monto solicitado}
         17: sCampo := 'SUM( PR.PR_INTERES )'; {Intereses}
         18: sCampo := 'SUM( PR.PR_TASA )'; {Tasa de interes}
         19: sCampo := 'SUM( PR.PR_MESES )'; {Meses en lo que se va a pagar}
         20: sCampo := 'SUM( PR.PR_PAGOS )' {Numero de pagos}
         else
         begin
              sCampo := 'SUM( PR.PR_PAG_PER )'; {}
//              Tipo := resInt;
         end;
     end;
end;

procedure TZetaPRESTAMO.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sStatus, sTipoPrestamo, sCampo : String;
   lEsFecha : Boolean;
begin
     ValidaConfidencial;
     bCalcula:= FALSE;
     ParametrosFijos( 1 );
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     if ParametrosConstantes then
     begin
          TipoRegresa := resDouble;
          if ParamEsCampo( 0 ) or ( Argument( 0 ).Kind = resString ) then
             sTipoPrestamo := ParamCampoString( 0 )
          else
             sTipoPrestamo := EntreComillas( IntToStr( ParamInteger( 0 )));
          if ParamVacio( 2 ) then
             sStatus := IntToStr( Ord( spActivo ) )
          else
              sStatus  := ParamCampoInteger( 2 );

          CaseCampo( DefaultInteger( 1, 1 ), sCampo, TipoRegresa, lEsFecha );
          EsFecha:= lEsFecha;
          AgregaScript( Format( GetScript, [ sCampo, 'COLABORA.CB_CODIGO',
                                             sTipoPrestamo, sStatus, sStatus ] ),
                                             TipoRegresa, lEsFecha );
     end;
end;

function TZetaPRESTAMO.Calculate: TQREvResult;
var
   sTipo, sCampo, sStatus : String;
   TipoRegresa : TQREvResultType;
   lEsFecha : Boolean;
begin
     bCalcula:= TRUE;
     if Argument(0).Kind = resInt then
        sTipo := IntToStr( ParamInteger( 0 ) )
     else
         sTipo := ParamString(0);

     sStatus := IntToStr( DefaultInteger( 2, Ord( spActivo ) ) );

     CaseCampo( DefaultInteger( 1, 1 ), sCampo, TipoRegresa, lEsFecha );
     EsFecha:= lEsFecha;
     Result := EvaluaQuery( Format( GetScript, [ CampoRes( sCampo ), IntToStr( ZetaEvaluador.GetEmpleado ), EntreComillas( sTipo ),
                                                 sStatus, sStatus ] ) );
end;

{ ******************* TZetaPRESTAMOACTIVO **************************}

type
  TZetaPRESTAMOACTIVO = class( TZetaPRESTAMO )
  private
    bCalcula: Boolean;
  public
    function GetScript : String; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaPRESTAMOACTIVO.GetScript : String;
var
   nRegresa: Byte;

    function AgregaCondicion: String;
    var
       sReferen, sTipoPrestamo: String;
    begin
         Result := VACIO;

         if NOT ParamVacio( 0 ) then
         begin
              if ParamEsCampo( 0 ) or ( Argument( 0 ).Kind = resString ) then
                 sTipoPrestamo := ParamCampoString( 0 )
              else
                  sTipoPrestamo := EntreComillas( IntToStr( ParamInteger( 0 )));
              Result := 'PR.PR_TIPO = ' + sTipoPrestamo ;
         end;

         if not ParamVacio(3) then
         begin
              if bCalcula then
                 Result := ConcatFiltros( Result,  ' PR.PR_REFEREN=' + EntreComillas( Trim( ParamString( 3 ) ) ) )
              else
              begin
                   if ParamEsCampo(3) then
                      sReferen := ParamNombreCampo(3)
                   else
                      sReferen := EntreComillas( Trim( ParamString(3) ) );
                   Result := ConcatFiltros( Result, ' PR.PR_REFEREN=' + sReferen );
              end;
         end
         else if ( nRegresa in [ 6, 13, 14, 15 ] )then
            Result := ConcatFiltros( Result, ' PR.PR_REFEREN= (select min(PR2.PR_REFEREN) from PRESTAMO PR2 ' +
                                             'where PR2.CB_CODIGO = PR.CB_CODIGO and PR2.PR_TIPO = PR.PR_TIPO ) ' );
    end;
{$ifdef DOS_CAPAS}
 const K_FILTRO_PRES = '(( PR.PR_TIPO ||''.''|| PR.PR_REFEREN ) <> ''%s.%s'' ) ';
{$else}
 const K_FILTRO_PRES = '(( PR.PR_TIPO +''.''+ PR.PR_REFEREN ) <> ''%s.%s'' ) ';
{$endif}
begin
     nRegresa:= DefaultInteger( 1, 1 );
     Result := ConcatFiltros( 'select %s from PRESTAMO PR where ( PR.CB_CODIGO = %s ) and ( PR.PR_STATUS = %s ) ' ,
               //No se toma en cuenta el prestamo activo
               ConcatFiltros( Format( K_FILTRO_PRES, [oZetaProvider.DatosPrestamo.Tipo,oZetaProvider.DatosPrestamo.Referencia] ) ,
               AgregaCondicion ) );
end;

  
  
//PRESTAMOA( [sTipo[,n Regresa] [, Status ] [, Referencia ] )

procedure TZetaPRESTAMOACTIVO.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sStatus, sCampo : String;
   lEsFecha : Boolean;
begin
     ValidaConfidencial;
     bCalcula:= FALSE;
     //ParametrosFijos( 1 );
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     if ParametrosConstantes then
     begin
          TipoRegresa := resDouble;

          if ParamVacio( 2 ) then
             sStatus := IntToStr( Ord( spActivo ) )
          else
             sStatus  := ParamCampoInteger( 2 );

          CaseCampo( DefaultInteger( 1, 1 ), sCampo, TipoRegresa, lEsFecha );
          EsFecha:= lEsFecha;
          AgregaScript( Format( GetScript, [ sCampo, 'COLABORA.CB_CODIGO', sStatus ] ),
                                             TipoRegresa, lEsFecha );
     end;
end;

function TZetaPRESTAMOACTIVO.Calculate: TQREvResult;
var
   sCampo : String;
   TipoRegresa : TQREvResultType;
   lEsFecha : Boolean;
begin
     bCalcula:= TRUE;

     CaseCampo( DefaultInteger( 1, 1 ), sCampo, TipoRegresa, lEsFecha );
     EsFecha:= lEsFecha;
     Result := EvaluaQuery( Format( GetScript, [ CampoRes( sCampo ), IntToStr( ZetaEvaluador.GetEmpleado ), 
                                                 IntToStr( DefaultInteger( 2, Ord( spActivo ) ) ) ] ) );
end;

{ ******************* TZetaSALDOPRESTAMO **************************}
type
  TZetaSALDOPRESTAMO = class( TZetaRegresaQuery )
  private
  public
    function GetScript : String; override;
    function GetScriptSR : String;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

//Sintaxis de la funcion:
//SALDO_PRE( Concepto, [Referencia,] [Fecha])

function TZetaSALDOPRESTAMO.GetScript : String;
begin
     //dbo.SP_SALDOPRE ( 	@Empleado NumeroEmpleado,		@Concepto Concepto,		@Referencia Referencia,		@FechaPeriodo Fecha )
     //select dbo.SP_SALDOPRE ( 	4, 57, 	'7835', @fecha )
     Result := SelRes('SP_SALDOPRE( %s, %s, %s, %s )')
end;

function TZetaSALDOPRESTAMO.GetScriptSR : String;
begin
     //No lleva la referencia, suma los saldos de los prestamos del mismo tipo que tenga el empleado
     //dbo.SP_SALDOPRE_PR ( @Empleado NumeroEmpleado,	@Concepto Concepto,		@FechaPeriodo Fecha )
     Result := SelRes('SP_SALDOPRE_SR( %s, %s, %s )')
end;


procedure TZetaSALDOPRESTAMO.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sFecha, sEmpleado  : String;
begin
     ValidaConfidencial;
     TipoRegresa := resDouble; //Esta funcion siempre regresa el saldo del prestamo
     ParametrosFijos( 1 );

     with ZetaEvaluador do
     begin
          sEmpleado := oZetaCreator.dmEntidadesTress.NombreEntidad( Entidad ) + '.CB_CODIGO';
          AgregaRequerido( sEmpleado, Entidad );
     end;

     if ParametrosConstantes then
     begin
          //Los unicos que ponen la fecha, son las balanzas
          if (oZetaProvider.UltimaFechaBalanza <> NullDateTime ) then
               sFecha := ParamCampoDefFecha( 2, EntreComillas( DateToStrSQL( oZetaProvider.UltimaFechaBalanza ) ) )
          else
              sFecha := ParamCampoDefFecha( 2, EntreComillas( DateToStrSQL( Date ) ) );

          if ParamVacio( 1 ) then
             AgregaScript( Format( GetScriptSR, [ sEmpleado,
                                                  ParamCampoInteger( 0 ),
                                                  sFecha ] ),
                                                  TipoRegresa, FALSE )
          else
             AgregaScript( Format( GetScript, [ sEmpleado,
                                                ParamCampoInteger( 0 ),
                                                ParamCampoString( 1 ),
                                                sFecha ] ),
                                                TipoRegresa, FALSE );


     end;
end;

function TZetaSALDOPRESTAMO.Calculate: TQREvResult;
var
   sFecha : String;
begin
     //Los unicos que ponen la fecha, son las balanzas
     if (oZetaProvider.UltimaFechaBalanza <> NullDateTime ) then
          sFecha := EntreComillas( DateToStrSQL( DefaultDate( 2, oZetaProvider.UltimaFechaBalanza ) ) )
     else
         sFecha := EntreComillas( DateToStrSQL( DefaultDate( 2, Date ) ) );

          if ParamVacio( 1 ) then
             Result := EvaluaQuery( Format( GetScriptSR, [ IntToStr( ZetaEvaluador.GetEmpleado ),
                                                           IntToStr(ParamInteger( 0 )),
                                                           sFecha ] ) )
          else
              Result := EvaluaQuery( Format( GetScript, [ IntToStr( ZetaEvaluador.GetEmpleado ),
                                                          IntToStr(ParamInteger( 0 )),
                                                          EntreComillas(ParamString( 1 )),
                                                          sFecha ] ) );
end;



{ ******************* TZetaSALDOAHORRO **************************}
type
  TZetaSALDOAHORRO = class( TZetaRegresaQuery )
  private
  public
    function GetScript : String; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

//Sintaxis de la funcion:
//SALDO_AHO( Concepto, [Fecha])

function TZetaSALDOAHORRO.GetScript : String;
begin
     //dbo.SP_SALDOAHO ( 	@Empleado NumeroEmpleado,		@Concepto Concepto,	@FechaPeriodo Fecha )
     //select dbo.SP_SALDOAHO ( 	4, 57, @fecha )
     Result := SelRes('SP_SALDOAHO( %s, %s, %s )')
end;

procedure TZetaSALDOAHORRO.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sFecha, sEmpleado  : String;
begin
     ValidaConfidencial;
     TipoRegresa := resDouble; //Esta funcion siempre regresa el saldo del prestamo
     ParametrosFijos( 1 );

     with ZetaEvaluador do
     begin
          sEmpleado := oZetaCreator.dmEntidadesTress.NombreEntidad( Entidad ) + '.CB_CODIGO';
          AgregaRequerido( sEmpleado, Entidad );
     end;

     if ParametrosConstantes then
     begin
          if (oZetaProvider.UltimaFechaBalanza <> NullDateTime ) then
             sFecha := ParamCampoDefFecha( 1, EntreComillas( DateToStrSQL( oZetaProvider.UltimaFechaBalanza ) ) )
          else
              sFecha := ParamCampoDefFecha( 1, EntreComillas( DateToStrSQL( Date ) ) );

          AgregaScript( Format( GetScript, [ sEmpleado,
                                             ParamCampoInteger( 0 ),
                                             sFecha ] ),
                                             TipoRegresa, FALSE );
     end;
end;

function TZetaSALDOAHORRO.Calculate: TQREvResult;
var
   sFecha : String;
begin
     if (oZetaProvider.UltimaFechaBalanza <> NullDateTime ) then
          sFecha := EntreComillas( DateToStrSQL( DefaultDate( 1, oZetaProvider.UltimaFechaBalanza ) ) )
     else
         sFecha := EntreComillas( DateToStrSQL( DefaultDate( 1, Date ) ) );

     Result := EvaluaQuery( Format( GetScript, [ IntToStr( ZetaEvaluador.GetEmpleado ),
                                                 ParamInteger( 0 ),
                                                 sFecha ] ) );
end;

{ ******************* TZetaCALBASE **************************}

type
  TZetaCALBASE = class( TZetaSFuncion )
  protected
    procedure PreparaAgente; override;
    procedure InitDatosCalendario;
    procedure GetFechasCalendario(const iParamFiltro : integer );
  end;

procedure TZetaCALBASE.GetFechasCalendario(const iParamFiltro : integer );
begin
     with oZetaProvider.DatosCalendario do
     begin
          if NOT ParamVacio( iParamFiltro ) then
             CalFiltro:= ParamString( iParamFiltro )
          else CalFiltro:= '';

          if NOT ParamVacio( 1 ) then
          begin
               CalInicial := ParamCampoFecha(1);
               dCalInicial := ParamDate( 1 );
          end;

          if NOT ParamVacio( 2 ) then
          begin
               CalFinal := ParamCampoFecha(2);
               dCalFinal := ParamDate( 2 );
          end;
     end;
end;

procedure TZetaCALBASE.InitDatosCalendario;
begin
     {en una llamada posterior, en el m�todo GetFechasCalendario, se revisa si los
     par�metros vienen o no asignados, si vienen vacios, se quedan con los defaults.}
     with oZetaProvider do
     begin
          if ZetaEvaluador.CalculandoNomina then
          begin
              with DatosCalendario do
              begin
                   {$ifdef QUINCENALES}
                   dCalInicial := DatosPeriodo.InicioAsis;
                   CalInicial := EntreComillas(DateToStrSQL( dCalInicial ));
                   dCalFinal := DatosPeriodo.FinAsis;
                   CalFinal := EntreComillas(DateToStrSQL( dCalFinal ));
                   {$else}
                   dCalInicial := DatosPeriodo.Inicio;
                   CalInicial := EntreComillas(DateToStrSQL( dCalInicial ));
                   dCalFinal := DatosPeriodo.Fin;
                   CalFinal := EntreComillas(DateToStrSQL( dCalFinal ));
                   {$endif}
                   CalFiltro:= ''
               end;
          end
          else {CV: 3-Abril-2003. Asi estaba el codigo hasta la version 2.3.96}
          begin
               with DatosCalendario do
               begin
                    if dCalInicial = NullDateTime then
                    begin
                         {$ifdef QUINCENALES}
                         dCalInicial := DatosPeriodo.InicioAsis;
                         CalInicial := EntreComillas(DateToStrSQL( dCalInicial ));
                         {$else}
                         dCalInicial := DatosPeriodo.Inicio;
                         CalInicial := EntreComillas(DateToStrSQL( dCalInicial ));
                         {$endif}
                    end;
                    if dCalFinal = NullDateTime then
                    begin
                         {$ifdef QUINCENALES}
                         dCalFinal := DatosPeriodo.FinAsis;
                         CalFinal := EntreComillas(DateToStrSQL( dCalFinal ));
                         {$else}
                         dCalFinal := DatosPeriodo.Fin;
                         CalFinal := EntreComillas(DateToStrSQL( dCalFinal ));
                         {$endif}
                    end;
               end;
          end;
     end;
end;

procedure TZetaCALBASE.PreparaAgente;
begin
     with FAgente do
     begin
          AgregaFiltro( 'AUSENCIA.CB_CODIGO = :Empleado', TRUE, enAusencia );
          AgregaFiltro( 'AUSENCIA.AU_FECHA >= :FechaIni', TRUE, enAusencia );
          AgregaFiltro( 'AUSENCIA.AU_FECHA <= :FechaFin', TRUE, enAusencia );
     end;
end;

{ ******************* TZetaCALENDARIOSUMA **************************}

type
  TZetaCALENDARIOSUMA= class( TZetaCALBASE )
  protected
    function GetResultado( const sExpresion, sFiltro : string;
                           const dInicio, dFin : TDate ) : TQREvResult;
  public
    function GetScript : string; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaCALENDARIOSUMA.GetScript : string;
begin
     Result := 'select %s from AUSENCIA where ( AUSENCIA.CB_CODIGO = %s ) and ( AUSENCIA.AU_FECHA >= %s ) and ( AUSENCIA.AU_FECHA <= %s )';
end;

procedure TZetaCALENDARIOSUMA.GetRequeridos( var TipoRegresa : TQREvResultType );
const
     CONTAR = '1';
var
   sCampo, {sInicial, sFinal,} sExpresion: string;
   lEsFecha :  Boolean;
   TipoCampo : TQREvResultType;
begin
     ParametrosFijos( 1 );
     oZetaProvider.GetDatosPeriodo;
     InitDatosCalendario;
     GetFechasCalendario(3);
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     sCampo:= Trim( ParamString(0) );
     lEsFecha:= False;

     if ( sCampo = CONTAR ) then TipoRegresa := resInt
     else TipoRegresa := resDouble;

     {$ifdef CAMBIO_TNOM}
     if ( EsEntidadNomina ) then
     begin
          AgregaRequerido( 'NOMINA.NO_ASI_INI', enNomina );
          AgregaRequerido( 'NOMINA.NO_ASI_FIN', enNomina );

          with oZetaProvider.DatosCalendario do
          begin
               if ParamVacio(1) then
                  CalInicial:= 'NOMINA.NO_ASI_INI';
               if ParamVacio(2) then
                  CalFinal:= 'NOMINA.NO_ASI_FIN';
          end;
     end;
     {$endif}

     if ( ParametrosConstantes ) and StrVacio(oZetaProvider.DatosCalendario.CalFiltro) then
     begin
         if ( sCampo = CONTAR ) then
         begin
              TipoCampo:= resInt;
              sExpresion:= 'COUNT(*)';
         end
         else
         begin
              TipoCampo := InvestigaTipo( enAusencia, sCampo, lEsFecha );
              sExpresion:= 'SUM(' + sCampo + ')';
         end;

         if ( TipoCampo <> resError ) then
         begin
              if ( not( TipoCampo in [resDouble,resInt] ) ) or lEsFecha then
                 ErrorParametro( 'El Par�metro #1 TIENE que ser de tipo NUMERICO', sCampo );
              with oZetaProvider.DatosCalendario do
                   AgregaScript( Format( GetScript, [ sExpresion, 'COLABORA.CB_CODIGO',
                                                      CalInicial, CalFinal ] ), TipoRegresa, FALSE );

         end;
     end;

end;

function TZetaCALENDARIOSUMA.GetResultado( const sExpresion, sFiltro : string;
                                           const dInicio, dFin : TDate ) : TQREvResult;
begin
     PreparaSuperEv( sExpresion, sFiltro,
                     enAusencia, [tgFloat,tgNumero]);

     CierraDataSet;
     SetParamValue( 'Empleado', tgNumero, ZetaEvaluador.GetEmpleado );

     SetParamValue( 'FechaIni', tgFecha, dInicio );
     SetParamValue( 'FechaFin', tgFecha, dFin );

     AbreDataSet;

     Result.Kind      := resDouble;
     Result.dblResult := SumaRegistros;
end;

function TZetaCALENDARIOSUMA.Calculate: TQREvResult;
begin
     InitDatosCalendario;
     GetFechasCalendario( 3 );

     with oZetaProvider.DatosCalendario do
     begin
          if ( EsEntidadNomina ) then
          begin
               dCalInicial:= DefaultDate(1, DataSetEvaluador.FieldByName('NO_ASI_INI').AsDateTime  ) ;
               dCalFinal:= DefaultDate(2, DataSetEvaluador.FieldByName('NO_ASI_FIN').AsDateTime );
          end;

          Result := GetResultado( ParamString( 0 ), CalFiltro, dCalInicial, dCalFinal );
     end;
end;

{ ******************* TZetaCALENDARIO **************************}

type
  TZetaCALENDARIO = class( TZetaCALBASE )
  protected
    FRegresa: Integer;
    FEntidad : TipoEntidad;
    FPreparoAgente : Boolean;
    function ObtieneCondicion: String;virtual;
    function UsarDiccion: Boolean; virtual;
    function CaseCampo( const sDefault: String; var sCampo: String; const lRequeridos: Boolean ): TQREvResultType;
    function GetSumaDias: TPesos;
    procedure PreparaAgente; override;
    function PreparoAgente : boolean;
  public
    function GetScript : string; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaCALENDARIO.PreparoAgente : boolean;
begin
     Result := FPreparoAgente;
end;

procedure TZetaCALENDARIO.PreparaAgente;
var
   sCondicion: String;
begin
     FPreparoAgente := TRUE;

    sCondicion:= ObtieneCondicion;
    case FRegresa of
         3: with FAgente do
            begin
                 AgregaColumna( 'INCAPACI.IN_FEC_INI', TRUE, enIncapacidad, tgFecha, 0, 'IN_FEC_INI' );
                 AgregaColumna( 'INCAPACI.IN_FEC_FIN', TRUE, enIncapacidad, tgFecha, 0, 'IN_FEC_FIN' );
                 AgregaFiltro( 'INCAPACI.CB_CODIGO = :Empleado', TRUE, enIncapacidad );
                 AgregaFiltro( 'INCAPACI.IN_FEC_FIN > :FechaIni', TRUE, enIncapacidad );
                 AgregaFiltro( 'INCAPACI.IN_FEC_INI <= :FechaFin', TRUE, enIncapacidad );
                 if ( sCondicion <> VACIO ) then
                    AgregaFiltro( sCondicion, UsarDiccion, enIncapacidad );
            end;
         9: with FAgente do
            begin
                 AgregaColumna( 'VACACION.VA_FEC_INI', TRUE, enVacacion, tgFecha, 0, 'VA_FEC_INI' );
                 AgregaColumna( 'VACACION.VA_FEC_FIN', TRUE, enVacacion, tgFecha, 0, 'VA_FEC_FIN' );
                 AgregaFiltro( 'VACACION.CB_CODIGO = :Empleado', TRUE, enVacacion );
                 AgregaFiltro( 'VACACION.VA_FEC_FIN > :FechaIni', TRUE, enVacacion );
                 AgregaFiltro( 'VACACION.VA_FEC_INI <= :FechaFin', TRUE, enVacacion );
                 if ( sCondicion <> VACIO ) then
                    AgregaFiltro( sCondicion, UsarDiccion, enVacacion );
            end;
    else
        inherited;
        if ( sCondicion <> VACIO ) then
           FAgente.AgregaFiltro( sCondicion, UsarDiccion, enAusencia );
    end;
end;

function TZetaCALENDARIO.ObtieneCondicion: String;
const
     K_I_FALTA_INJUSTIFICADA = 'FI';
     K_I_RETARDO = 'RE';
begin
     Case FRegresa of
           1: Result := 'AUSENCIA.AU_TIPO = ''' + K_I_FALTA_INJUSTIFICADA + '''';           // Cuantas Faltas Injustificadas
           2: Result := 'AUSENCIA.AU_TIPODIA = ' + IntToStr( Ord( daJustificada ) );      // Cuantas Faltas Justificadas
           8: Result := 'AUSENCIA.AU_TIPO = ''' + K_I_RETARDO + '''';                       // Dias de Retardo
          10: Result := 'AUSENCIA.AU_TIPODIA = ' + IntToStr( Ord( daConGoce ) );          // Permisos de Dias Con Goce
          11: Result := 'AUSENCIA.AU_TIPODIA = ' + IntToStr( Ord( daSinGoce ) );          // Permisos de Dias Sin Goce
          12: Result := 'AUSENCIA.AU_TIPODIA = ' + IntToStr( Ord( daSuspension ) );       // Suspensiones
          13: Result := 'AUSENCIA.AU_TIPODIA = ' + IntToStr( Ord( daOtroPermiso ) );      // Permisos Otros
          14: Result := 'AUSENCIA.AU_TIPODIA = ' + IntToStr( Ord( daVacaciones ) );       // Vacaciones
          15: Result := 'AUSENCIA.AU_TIPODIA = ' + IntToStr( Ord( daIncapacidad ) );      // #Incapacidades
     else
         Result:= VACIO;
     end;
end;


function TZetaCALENDARIO.UsarDiccion: Boolean;
begin
     Result := FALSE;
end;


function TZetaCALENDARIO.GetScript : string;

    Function AgregaCondicion: String;
    var
       sCondicion: String;
    begin
         Result:= VACIO;
         sCondicion:= ObtieneCondicion;
         if sCondicion <> VACIO then
            Result:= ' and (' + sCondicion + ')';
    end;

begin
     case FRegresa of
          3: Result := 'select %s from INCAPACI INI where ( INI.CB_CODIGO = %s ) and ( INI.IN_FEC_FIN > %s ) and ( INI.IN_FEC_INI <= %s )';
          9: Result := 'select %s from VACACION VA where ( VA.CB_CODIGO = %s ) and ( VA.VA_FEC_FIN > %s ) and ( VA.VA_FEC_INI <= %s )';
     else
          Result := 'select %s from AUSENCIA where ( AUSENCIA.CB_CODIGO = %s ) and ( AUSENCIA.AU_FECHA >= %s ) and ( AUSENCIA.AU_FECHA <= %s )' +
                    AgregaCondicion;
     end;
end;

function TZetaCALENDARIO.CaseCampo( const sDefault: String; var sCampo: String;
                                    const lRequeridos: Boolean ): TQREvResultType;
begin
     Result := resInt;
     sCampo := sDefault;
     case FRegresa of
          4: begin
                  sCampo := 'AU_HORAS';
                  Result := resDouble;
             end;
          5: begin
                  sCampo := 'AU_EXTRAS';
                  Result := resDouble;
             end;
          6: begin
                  sCampo := 'AU_DOBLES';
                  Result := resDouble;
             end;
          7: begin
                  sCampo := 'AU_TARDES';
                  Result := resDouble;
             end;
     end;
     if (sCampo <> sDefault) AND lRequeridos then
        sCampo := 'SUM('+sCampo+')';

end;

function TZetaCALENDARIO.GetSumaDias: TPesos;

   function GetSubCampo: string;
   begin
        if ( FEntidad = enIncapacidad ) then
           Result := 'IN_'
        else
           Result := 'VA_';
   end;

begin
     Result := 0;
     with SuperEv.DataSet do
     begin
          while NOT EOF do
          begin
               with oZetaProvider.DatosCalendario do
                    Result := Result + GetDiasRango( FieldByName( GetSubCampo + 'FEC_INI' ).AsDateTime,
                                                     FieldByName( GetSubCampo + 'FEC_FIN' ).AsDateTime,
                                                     dCalInicial, dCalFinal );
               Next;
          end;
     end;
end;

procedure TZetaCALENDARIO.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sCampo,sInicial,sFinal : string;
begin
    // TipoRegresa := resInt;
     ParametrosFijos( 1 );
     oZetaProvider.GetDatosPeriodo;
     InitDatosCalendario;
     GetFechasCalendario(3);
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     {$ifdef CAMBIO_TNOM}

     with oZetaProvider.DatosCalendario do
     begin
          sInicial:= CalInicial;
          sFinal:= CalFinal;
     end;

     if ( EsEntidadNomina ) then
     begin
          AgregaRequerido( 'NOMINA.NO_ASI_INI', enNomina );
          AgregaRequerido( 'NOMINA.NO_ASI_FIN', enNomina );
          if ParamVacio(1) then
             sInicial:= 'NOMINA.NO_ASI_INI';
          if ParamVacio(2) then
             sFinal:= 'NOMINA.NO_ASI_FIN';
     end;
     {$endif}
     FRegresa:= DefaultInteger( 0, 1 );
     TipoRegresa:= CaseCampo( 'COUNT(*)', sCampo, TRUE );
     if NOT ( FRegresa in [ 3, 9 ] ) and ( ParametrosConstantes ) and StrVacio(oZetaProvider.DatosCalendario.CalFiltro) then
     begin
          TipoRegresa:= CaseCampo( 'COUNT(*)', sCampo, TRUE );
          {$ifdef CAMBIO_TNOM}
          AgregaScript( Format( GetScript, [ sCampo, 'COLABORA.CB_CODIGO',
                             sInicial, sFinal ] ), TipoRegresa, FALSE );
          {$else}
          with oZetaProvider.DatosCalendario do
               AgregaScript( Format( GetScript, [ sCampo, 'COLABORA.CB_CODIGO',
                             CalInicial, CalFinal ] ), TipoRegresa, FALSE );
          {$endif}
     end;
end;

function TZetaCALENDARIO.Calculate: TQREvResult;
var
   sCampo : String;
begin
     InitDatosCalendario;
     GetFechasCalendario(3);
     FRegresa:= DefaultInteger( 0, 1 );
     CaseCampo( '1', sCampo, FALSE );
     case FRegresa of
          3 : FEntidad:= enIncapacidad;
          9 : FEntidad:= enVacacion;
     else
         FEntidad:= enAusencia;
     end;


     with oZetaProvider.DatosCalendario do
     begin
          {$ifdef CAMBIO_TNOM}
          if ( EsEntidadNomina ) then
          begin
               dCalInicial:= DefaultDate( 1, DataSetEvaluador.FieldByName('NO_ASI_INI').AsDateTime );
               dCalFinal:= DefaultDate( 2, DataSetEvaluador.FieldByName('NO_ASI_FIN').AsDateTime );
          end;
          {$endif}
          PreparaSuperEv( sCampo, CalFiltro, FEntidad, [ tgFloat, tgNumero ] );
          CierraDataSet;
          SetParamValue( 'Empleado', tgNumero, ZetaEvaluador.GetEmpleado );
          SetParamValue( 'FechaIni', tgFecha, dCalInicial );
          SetParamValue( 'FechaFin', tgFecha, dCalFinal );
          AbreDataSet;
     end;

     with Result do
     begin
          Kind:= resDouble;
          if ( FEntidad = enAusencia ) then
             dblResult := SumaRegistros
          else
             dblResult := GetSumaDias;
     end;
end;

{ ******************* TZetaCSEMANA **************************}

type
  TZetaCSEMANA = class( TZetaCALBASE )
  protected
    FRegresa: Integer;
    sExpresion, sFiltro: String;
    function GetResultado( const sResultado: string; const eTipo : TQREvResultType ) : TQREvResult;
    procedure PreparaAgente; override;
  public
    function GetScript : string; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaCSEMANA.PreparaAgente;
begin
     inherited;
     if ( sExpresion <> VACIO ) then
        FAgente.AgregaFiltro( sExpresion, FALSE, enAusencia );  // el Filtro se Agrega en el PreparaSuperEv
end;

function TZetaCSEMANA.GetScript : string;

    Function AgregaCondicion( const sCondicion: String ): String;
    begin
         if sCondicion <> VACIO then
            Result:= ' and (' + sCondicion + ')'
         else
            Result:= VACIO;
    end;

begin
     Result := 'select %s from AUSENCIA where ( AUSENCIA.CB_CODIGO = %s ) and ( AUSENCIA.AU_FECHA >= %s ) and ( AUSENCIA.AU_FECHA <= %s )' +
                AgregaCondicion( sExpresion ) + AgregaCondicion( sFiltro );
end;

procedure TZetaCSEMANA.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sInicial, sFinal: string;
   lEsFecha : Boolean;
begin
     TipoRegresa := resInt;
     ParametrosFijos( 1 );
     oZetaProvider.GetDatosPeriodo;

     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );

     {$ifdef CAMBIO_TNOM}
     if ( EsEntidadNomina ) then
     begin
          AgregaRequerido( 'NOMINA.NO_ASI_INI', enNomina );
          AgregaRequerido( 'NOMINA.NO_ASI_FIN', enNomina );
     end;
     {$endif}

     if ParametrosConstantes then
     begin
          sExpresion := ParamString( 0 );
          if InvestigaTipo( enAusencia, sExpresion, lEsFecha ) = resError then Exit;

          sFiltro := DefaultString( 1, VACIO );
          if sFiltro <> VACIO then
             if InvestigaTipo( enAusencia, sFiltro, lEsFecha ) = resError then Exit;
          {$ifdef CAMBIO_TNOM}
          if ( EsEntidadNomina ) then
          begin
               sInicial:= ParamCampoDefFecha( 2, 'NOMINA.NO_ASI_INI' );
               sFinal:= ParamCampoDefFecha( 3, 'NOMINA.NO_ASI_FIN' );
          end
          else
          begin
          {$endif}
          {$ifdef QUINCENALES}
               sInicial:= ParamCampoDefFecha( 2, EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.InicioAsis ) ) );
               sFinal:= ParamCampoDefFecha( 3, EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.FinAsis ) ) );
          {$else}
               sInicial:= ParamCampoDefFecha( 2, EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.Inicio ) ) );
               sFinal:= ParamCampoDefFecha( 3, EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.Fin ) ) );
          {$endif}
          {$ifdef CAMBIO_TNOM}
          end;
          {$endif}
          AgregaScript( Format( GetScript, [ 'COUNT( * )', 'COLABORA.CB_CODIGO',
                                             sInicial, sFinal ] ), TipoRegresa, FALSE );
     end;
end;

function TZetaCSEMANA.GetResultado( const sResultado: string; const eTipo : TQREvResultType ) : TQREvResult;
begin
     PreparaSuperEv( sResultado, sFiltro, enAusencia, [tgNumero,tgFloat]);  // La Expresion se agrega en el PreparaAgente

     CierraDataSet;
     SetParamValue( 'Empleado', tgNumero, ZetaEvaluador.GetEmpleado );

     {$ifdef CAMBIO_TNOM}
     if ( EsEntidadNomina ) then
     begin
          SetParamValue( 'FechaIni', tgFecha, DefaultDate( 2, DataSetEvaluador.FieldByName('NO_ASI_INI').AsDateTime ) );
          SetParamValue( 'FechaFin', tgFecha, DefaultDate( 3, DataSetEvaluador.FieldByName('NO_ASI_FIN').AsDateTime ) );
     end
     else
     begin
     {$endif}
          {$ifdef QUINCENALES}
          SetParamValue( 'FechaIni', tgFecha, DefaultDate( 2, oZetaProvider.DatosPeriodo.InicioAsis ) );
          SetParamValue( 'FechaFin', tgFecha, DefaultDate( 3, oZetaProvider.DatosPeriodo.FinAsis ) );
          {$else}
          SetParamValue( 'FechaIni', tgFecha, DefaultDate( 2, oZetaProvider.DatosPeriodo.Inicio ) );
          SetParamValue( 'FechaFin', tgFecha, DefaultDate( 3, oZetaProvider.DatosPeriodo.Fin ) );
          {$endif}
     {$ifdef CAMBIO_TNOM}
     end;
     {$endif}
     AbreDataSet;

     with Result do
     begin
          Kind:= eTipo;
          if ( Kind = resInt ) then intResult := Trunc( SumaRegistros )
          else dblResult := SumaRegistros;
     end;
end;

function TZetaCSEMANA.Calculate: TQREvResult;
begin
     sExpresion:= ParamString(0);
     sFiltro := DefaultString( 1, VACIO );
     Result := GetResultado( '1', resInt );
end;

{ ************* TZetaCSEMANASUMA ************* }

type
  TZetaCSEMANASUMA = class( TZetaCSEMANA )
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaCSEMANASUMA.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sCampo, sInicial, sFinal: string;
   lEsFecha : Boolean;
begin
     TipoRegresa := resDouble;
     ParametrosFijos( 1 );

     oZetaProvider.GetDatosPeriodo;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     {$ifdef CAMBIO_TNOM}
     if ( EsEntidadNomina ) then
     begin
          AgregaRequerido( 'NOMINA.NO_ASI_INI', enNomina );
          AgregaRequerido( 'NOMINA.NO_ASI_FIN', enNomina );
     end;
     {$endif}

     if ParametrosConstantes then
     begin
          sCampo := ParamString( 0 );
          if InvestigaTipo( enAusencia, sCampo, lEsFecha ) = resError then Exit;
          sExpresion:= VACIO;

          sFiltro := DefaultString( 1, VACIO );
          if sFiltro <> VACIO then
             if InvestigaTipo( enAusencia, sFiltro, lEsFecha ) = resError then Exit;

          if ( EsEntidadNomina ) then
          begin
               sInicial:= ParamCampoDefFecha( 2, 'NOMINA.NO_ASI_INI' );
               sFinal:= ParamCampoDefFecha( 3, 'NOMINA.NO_ASI_FIN' );
          end
          else
          begin
               {$ifdef QUINCENALES}
               sInicial:= ParamCampoDefFecha( 2, EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.InicioAsis ) ) );
               sFinal:= ParamCampoDefFecha( 3, EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.FinAsis ) ) );
               {$else}
               sInicial:= ParamCampoDefFecha( 2, EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.Inicio ) ) );
               sFinal:= ParamCampoDefFecha( 3, EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.Fin ) ) );
               {$endif}

          {$ifdef CAMBIO_TNOM}
          end;
          {$endif}

          AgregaScript( Format( GetScript, [ 'SUM(' + sCampo + ')', 'COLABORA.CB_CODIGO',
                                                  sInicial, sFinal ] ), TipoRegresa, FALSE );
     end;
end;

function TZetaCSEMANASUMA.Calculate: TQREvResult;
begin
     sExpresion:= VACIO;
     sFiltro:= DefaultString( 1, VACIO );
     Result:= GetResultado( ParamString( 0 ), resDouble );
end;


{****************** TZetaSQ *****************************}

type
  TZetaSQ = class( TZetaCSEMANASUMA )
  public
    FTipoDia,FStatus:Integer;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function ObtieneTipoDia: String;
    function ObtieneStatus: String;
  end;


function TZetaSQ.Calculate: TQREvResult;
begin
     sExpresion:= VACIO;
     FTipoDia:= DefaultInteger( 1, -1 );
     FStatus:= DefaultInteger( 0, 0 );
     sFiltro:= ConcatFiltros( ObtieneTipoDia ,ObtieneStatus );
     Result:= GetResultado( '1', resDouble );
end;

procedure TZetaSQ.GetRequeridos(var TipoRegresa: TQREvResultType);
var
   sInicial, sFinal: string;
begin
     TipoRegresa := resDouble;
     oZetaProvider.GetDatosPeriodo;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     if ( EsEntidadNomina ) then
     begin
          AgregaRequerido( 'NOMINA.NO_ASI_INI', enNomina );
          AgregaRequerido( 'NOMINA.NO_ASI_FIN', enNomina );
     end;
     if ParametrosConstantes then
     begin
          if ParamVacio( 0 ) then
             FStatus := 0
          else
              FStatus := ParamInteger(0);

          if ParamVacio( 1 ) then
             FTipoDia := -1
          else
              FTipoDia := ParamInteger(1);

          if ( EsEntidadNomina ) then
          begin
               sInicial:= ParamCampoDefFecha( 2, 'NOMINA.NO_ASI_INI' );
               sFinal:= ParamCampoDefFecha( 3, 'NOMINA.NO_ASI_FIN' );
          end
          else
          begin
               sInicial:= ParamCampoDefFecha( 2, EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.InicioAsis ) ) );
               sFinal:= ParamCampoDefFecha( 3, EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.FinAsis ) ) );
          end;
          sFiltro:= ConcatFiltros( ObtieneTipoDia ,ObtieneStatus );
          AgregaScript( Format( GetScript, [ 'Count(*)', 'COLABORA.CB_CODIGO',sInicial, sFinal ] ), TipoRegresa, FALSE );
     end;
end;


function TZetaSQ.ObtieneStatus: String;
begin
     Case FStatus of
        0: Result := 'AUSENCIA.AU_STATUS = ' + IntToStr( Ord( auHabil  ) );         // Dias Habiles
        1: Result := 'AUSENCIA.AU_STATUS = ' + IntToStr( Ord( auSabado  ) );        // Sabados
        2: Result := 'AUSENCIA.AU_STATUS = ' + IntToStr( Ord( auDescanso   ) );     // Descansos
     else
         Result:= VACIO;
     end;
end;

function TZetaSQ.ObtieneTipoDia: String;
const
     K_I_FALTA_INJUSTIFICADA = 'FI';
     K_I_RETARDO = 'RE';
begin
     Case FTipoDia of
           0: Result := 'AUSENCIA.AU_TIPODIA = ' + IntToStr( Ord( daNormal ) );         // Dias sin incidencia
           1: Result := 'AUSENCIA.AU_TIPODIA = ' + IntToStr( Ord( daIncapacidad ) );    // Cuantas dias de incapacidad
           2: Result := 'AUSENCIA.AU_TIPODIA = ' + IntToStr( Ord( daVacaciones  ) );    // Cuantas dias de vacaciones
           3: Result := 'AUSENCIA.AU_TIPODIA = ' + IntToStr( Ord( daConGoce  ) );       // Dias de permiso con goce
           4: Result := 'AUSENCIA.AU_TIPODIA = ' + IntToStr( Ord( daSinGoce  ) );       // Dias de permiso sin goce
           5: Result := 'AUSENCIA.AU_TIPODIA = ' + IntToStr( Ord( daJustificada  ) );   // Dias falta justificada
           6: Result := 'AUSENCIA.AU_TIPODIA = ' + IntToStr( Ord( daSuspension  ) );    // Duas de Suspension
           7: Result := 'AUSENCIA.AU_TIPODIA = ' + IntToStr( Ord( daOtroPermiso ) );    // Permisos Otros
           8: Result := 'AUSENCIA.AU_TIPODIA = ' + IntToStr( Ord( daFestivo  ) );       // Festivos
           9: Result := 'AUSENCIA.AU_TIPODIA = ' + IntToStr( Ord( daNoTrabajado ) );    // Dias No trabajados
          10: Result := 'AUSENCIA.AU_TIPO = ''' + K_I_FALTA_INJUSTIFICADA + '''';       // Dias Faltas Injustificadas
          11: Result := 'AUSENCIA.AU_TIPO = ''' + K_I_RETARDO + '''';                   // Dias Retardos

     else
         Result:= VACIO;
     end;
end;



{ ************* TZetaCFECHA ************* }

type
  TZetaCFECHA = class( TZetaSFuncion )
  protected
    function GetFecha: String; virtual;
    function GetFiltro: String; virtual;
    function GetResultado( const sExpresion : string;
                           const dFecha : TDate ) : TQREvResult;
    procedure PreparaAgente; override;
  public
    function GetScript : string; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaCFECHA.PreparaAgente;
begin
     with FAgente do
     begin
          AgregaFiltro( 'AUSENCIA.CB_CODIGO = :Empleado', TRUE, enAusencia );
          AgregaFiltro( 'AUSENCIA.AU_FECHA = :Fecha', TRUE, enAusencia );
     end;
end;

function TZetaCFECHA.GetScript : String;
begin
     Result := 'select %s from AUSENCIA where ( AUSENCIA.CB_CODIGO = %s ) and ( AUSENCIA.AU_FECHA = %s ) %s';
end;

function TZetaCFECHA.GetFecha: String;
begin
     Result:= ParamCampoFecha( 0 );
end;

function TZetaCFecha.GetFiltro: String;
begin
     Result:= VACIO;
end;

procedure TZetaCFECHA.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sCampo : string;
   lEsFecha : Boolean;
begin
     { No se le asigna valor a tiporegresa porque puede regresar cualquier tipo de dato
     TipoRegresa:= resDouble; }

     oZetaProvider.GetDatosPeriodo;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );

     ParametrosFijos( 1 );
     if ParametrosConstantes then
     begin
          sCampo := DefaultString( 1, 'AU_HORAS' );
          TipoRegresa:= InvestigaTipo( enAusencia, sCampo, lEsFecha );
          EsFecha:= lEsFecha;
          if TipoRegresa = resError then
             Exit;
          AgregaScript( Format( GetScript, [ sCampo, 'COLABORA.CB_CODIGO',
                                             GetFecha,
                                             GetFiltro ] ), TipoRegresa, lEsFecha );
     end;
end;

function TZetaCFECHA.GetResultado( const sExpresion : string;
                                    const dFecha : TDate ) : TQREvResult;
begin
     PreparaSuperEv( sExpresion, VACIO,
                     enAusencia, [tgBooleano,tgFloat,tgNumero,tgFecha,tgTexto]);

     CierraDataSet;
     SetParamValue( 'Empleado', tgNumero, ZetaEvaluador.GetEmpleado );
     SetParamValue( 'Fecha', tgFecha, dFecha );
     AbreDataSet;

     Result := SuperEv.Value;
     EsFecha := Result.resFecha;
end;

function TZetaCFECHA.Calculate: TQREvResult;
begin
     Result := GetResultado( DefaultString( 1, 'AU_HORAS' ), ParamDate( 0 ) );
end;

{ ************* TZetaCDIA ************* }

type
  TZetaCDIA = class( TZetaCFECHA )
  protected
    function GetFecha: String; override;
	{$ifdef ACS}
    function GetFiltro: String; override;
    procedure PreparaAgente; override;
	{$endif}
    function GetResultado( const sExpresion : string;
                           const dFecha : TDate ) : TQREvResult;
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaCDIA.GetFecha: String;
begin
     {$ifdef QUINCENALES}
     Result:= EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.InicioAsis + ( ParamInteger( 0 ) - 1 ) ) );
     {$else}
     Result:= EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.Inicio + ( ParamInteger( 0 ) - 1 ) ) );
     {$endif}
end;

{$ifdef ACS}
function TZetaCDIA.GetFiltro: String;
begin
     {$IFDEF CAMBIO_TNOM}
     if ( EsEntidadNomina ) then
        Result:= Format( ' AND AUSENCIA.CB_NOMINA = %d', [ Ord( oZetaProvider.DatosPeriodo.Tipo ) ] )
     {$ENDIF}
end;


procedure TZetaCDIA.PreparaAgente;
begin
     inherited PreparaAgente;
     {$IFDEF CAMBIO_TNOM}
     if ( EsEntidadNomina ) then
     begin
          with FAgente do
          begin
               AgregaFiltro( 'AUSENCIA.CB_NOMINA = :TNomina', TRUE, enAusencia );
          end;
     end;
     {$ENDIF}
end;
{$endif}
function TZetaCDIA.Calculate: TQREvResult;
begin
     {$ifdef QUINCENALES}
     Result := GetResultado( DefaultString( 1, 'AU_HORAS' ),
                             oZetaProvider.DatosPeriodo.InicioAsis + ParamInteger( 0 ) - 1 );
     {$else}
     Result := GetResultado( DefaultString( 1, 'AU_HORAS' ),
                             oZetaProvider.DatosPeriodo.Inicio + ParamInteger( 0 ) - 1 );

     {$endif}
end;

function TZetaCDIA.GetResultado(const sExpresion: string; const dFecha: TDate): TQREvResult;
begin
     PreparaSuperEv( sExpresion, VACIO,
                     enAusencia, [tgBooleano,tgFloat,tgNumero,tgFecha,tgTexto]);

     CierraDataSet;
     SetParamValue( 'Empleado', tgNumero, ZetaEvaluador.GetEmpleado );
     SetParamValue( 'Fecha', tgFecha, dFecha );
     {$ifdef ACS}
     {$IFDEF CAMBIO_TNOM}
     if ( EsEntidadNomina ) then
     begin
          SetParamValue( 'TNomina', tgNumero, oZetaProvider.DatosPeriodo.Tipo );
     end;
     {$ENDIF}
     {$endif}
     AbreDataSet;

     Result := SuperEv.Value;
     EsFecha := Result.resFecha;
end;


{ ************* TZetaSUMABASE ************* }

type
  TZetaSUMABASE = class( TZetaSFuncion )
  private
    FColSalario: Integer;
    FFormula, FFormulaSalario, FFiltro: String;
    FFactor : TTasa;
    FParamFijos: Integer;
    function GetSuperEv: TPesos;
    function GetSalarioEv: TPesos;
    function GetResultado( const dInicial, dFinal: TDate ): TQREvResult;
  protected
    property ParamFijos: Integer read FParamFijos write FParamFijos;
    function CalculaDataSet: TQREvResult; virtual; abstract;
    procedure PreparaAgente; override;
  public
    constructor Create(oEvaluador:TQrEvaluator); override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

constructor TZetaSUMABASE.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create(oEvaluador);
     FParamFijos:= 3;
end;

procedure TZetaSUMABASE.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa:= resDouble;

     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     AgregaRequerido( 'NOMINA.NO_ASI_INI', enNomina );
     AgregaRequerido( 'NOMINA.NO_ASI_FIN', enNomina );

     ParametrosFijos( FParamFijos );

end;

function TZetaSUMABASE.GetResultado( const dInicial, dFinal: TDate ): TQREvResult;
begin
     PreparaSuperEv( FFormula, FFiltro, enAusencia, [ tgFloat, tgNumero ] );

     CierraDataSet;
     SetParamValue( 'Empleado', tgNumero, ZetaEvaluador.GetEmpleado );
     SetParamValue( 'FechaIni', tgFecha, dInicial );
     SetParamValue( 'FechaFin', tgFecha, dFinal );
     AbreDataSet;

     with ZetaEvaluador do
     begin
          if Rastreando then
             Rastreador.RastreoTextoLibre( 'Asistencia : ' + FechaCorta( DatasetEvaluador.FieldByName( 'NO_ASI_INI' ).AsDateTime ) +
                                           ' al ' + FechaCorta( DatasetEvaluador.FieldByName( 'NO_ASI_FIN' ).AsDateTime ) );
     end;

     Result := CalculaDataSet;
     EsFecha := Result.resFecha;
end;

procedure TZetaSUMABASE.PreparaAgente;
begin
     with FAgente do
     begin
          FColSalario := AgregaColumna( FFormulaSalario, FALSE, enAusencia, tgFloat, 0, '' );
          AgregaColumna( 'AUSENCIA.AU_FECHA', TRUE, enAusencia, tgFecha, 0, 'AU_FECHA' );
          AgregaColumna( 'AUSENCIA.CB_SALARIO', TRUE, enAusencia, tgFloat, 0, 'CB_SALARIO' );
          AgregaColumna( 'AUSENCIA.AU_POSICIO', TRUE, enAusencia, tgNumero, 0, 'AU_POSICIO' );
          AgregaFiltro( 'AUSENCIA.CB_CODIGO = :Empleado', TRUE, enAusencia );
          AgregaFiltro( 'AUSENCIA.AU_FECHA >= :FechaIni', TRUE, enAusencia );
          AgregaFiltro( 'AUSENCIA.AU_FECHA <= :FechaFin', TRUE, enAusencia );
     end;
end;

function TZetaSUMABASE.GetSuperEv: TPesos;
begin
     Result := 0;
     with SuperEv.Value do
     case Kind of
          resDouble : Result := dblResult;
          resInt : Result := intResult
          else ErrorFuncion( 'Error en la Evaluaci�n', 'Funci�n: ' + Expresion );
     end;
end;

function TZetaSUMABASE.GetSalarioEv: TPesos;
begin
     with AsignaResultado( SuperEv.DataSet.Fields[ FColSalario ] ) do
     begin
          Result := dblResult;
     end;
end;

{ ************* TZetaSUMAMONTOS ************* }

type
  TZetaSUMAMONTOS = class( TZetaSUMABASE )
  private
  protected
    function CalculaDataSet: TQREvResult; override;
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaSUMAMONTOS.Calculate: TQREvResult;
begin
     FFormula := ParamString(0);
     FFormulaSalario := ParamString(1);
     FFactor := ParamFloat(2);
     FFiltro := DefaultString( 3, VACIO );

     Result := GetResultado( DatasetEvaluador.FieldByName( 'NO_ASI_INI' ).AsDateTime,
                             DatasetEvaluador.FieldByName( 'NO_ASI_FIN' ).AsDateTime );
end;

function TZetaSUMAMONTOS.CalculaDataSet: TQREvResult;
var
   rTotal, rMonto: TPesos;
   rFormula, rSalario, rTotalInc: TPesos;

   procedure AgregaTituloRastreo;
   begin
        with ZetaEvaluador.Rastreador do
        begin
             RastreoHeader( 'EVALUACION DE DIAS DE PRENOMINA' );
             RastreoEspacio;
             RastreoTextoLibre( 'POS   FECHA       DIA     SALARIO   HRS/INC   FACTOR        MONTO' );
        end;
   end;

   procedure AgregaDetalleRastreo;
   var
      dFecha: TDate;
   begin
        dFecha := SuperEv.DataSet.FieldByName( 'AU_FECHA' ).AsDateTime;
        with ZetaEvaluador.Rastreador do
        begin
             //  1   07/Oct/05   Vie       52.08      3.00     2          312.48
             //  4   10/Oct/05   Lun       52.08      4.00     2          416.64
             RastreoTextoLibre( PadL( SuperEv.DataSet.FieldByName( 'AU_POSICIO' ).AsString, 3 ) + Space(3) +
                                FechaCorta( dFecha ) + Space(3) +
                                Copy( DiaSemana( dFecha ), 1, 3 ) + Space(3) +
                                PadL( Format( '%6.2n', [ rSalario ] ), 9 ) + Space(3) +
                                PadL( Format( '%4.2n', [ rFormula ] ), 7 ) + Space(3) +
                                PadC( Format( '%g', [ FFactor ] ), 6 ) + Space(3) +
                                PadL( Format( '%7.2n', [ rMonto ] ), 10 )
                                );
        end;
   end;

   procedure AgregaTotalRastreo;
   begin
        with ZetaEvaluador.Rastreador do
        begin
             RastreoEspacio;
             //            TOTALES DEL PERIODO :      1.00                240.00
             RastreoTextoLibre( Space(12) + 'TOTALES DEL PERIODO :' + Space(3) +
                                PadL( Format( '%4.2n', [ rTotalInc ] ), 7 ) + Space(12) +
                                PadL( Format( '%7.2n', [ rTotal ] ), 10 )
                                );
             //RastreoPesos( 'TOTAL DEL PERIODO : ', rTotal );
        end;
   end;

begin
     rTotal := 0;
     rTotalInc := 0;

     if ZetaEvaluador.Rastreando then
        AgregaTituloRastreo;

     with SuperEv.DataSet do
     begin
          while ( not EOF ) do
          begin
               rFormula := GetSuperEv;
               rSalario := GetSalarioEv;
               rMonto := rFormula * rSalario * FFactor;
               rTotal := rTotal + rMonto;
               if ZetaEvaluador.Rastreando then
               begin
                    rTotalInc := rTotalInc + rFormula;
                    AgregaDetalleRastreo;
               end;
               Next;
          end;
     end;

     if ZetaEvaluador.Rastreando then
        AgregaTotalRastreo;

     with Result do
     begin
          Kind:= resDouble;
          dblResult := rTotal;
     end;
end;

{ ************* TZetaSUMAHORAS ************* }

type
  TZetaSUMAHORAS = class( TZetaSUMAMONTOS )
  private
    procedure SetParametrosHoras( const iTipoIncidencia: Integer );
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaSUMAHORAS.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParamFijos:= 1;
     inherited;
end;

function TZetaSUMAHORAS.Calculate: TQREvResult;
begin
     FFormulaSalario := DefaultString( 1, 'CB_SALARIO / 8' );
     SetParametrosHoras( ParamInteger(0) );
     Result := GetResultado( DatasetEvaluador.FieldByName( 'NO_ASI_INI' ).AsDateTime,
                             DatasetEvaluador.FieldByName( 'NO_ASI_FIN' ).AsDateTime );
end;

procedure TZetaSUMAHORAS.SetParametrosHoras( const iTipoIncidencia: Integer );
begin
     { Valores mas comunes }
     FFiltro := VACIO;
     FFactor  := 2;
     case iTipoIncidencia of
          1 : begin    // Dobles
                   FFormula := 'AU_DOBLES';
              end;
          2 : begin    // Triples
                   FFormula := 'AU_TRIPLES';
                   FFactor  := 3;
              end;
          3 : begin    // Descanso Trabajado
                   FFormula := 'AU_DES_TRA';
                   FFiltro  := 'AU_TIPODIA=0';
              end;
          4 : begin    // Festivo Trabajado
                   FFormula := 'AU_DES_TRA';
                   FFiltro  := 'AU_TIPODIA=8';
              end;
          5 : begin    // Domingo
                   FFormula := 'AU_DES_TRA';
                   FFiltro  := 'DOW(AU_FECHA)=1';
              end;
          6 : begin    // Horas de Retardo
                   FFormula := 'AU_TARDES';
                   FFactor  := 1;
                   FFiltro  := 'AU_STATUS=0';
              end;
          7 : begin    // Horas no trabajadas
                   FFormula := 'AU_HORASNT';
                   FFactor  := 1;
                   FFiltro  := 'AU_STATUS=0';
              end;
     else
         ErrorParametro(  'Solo se permiten valores 1 a 7', '' );
     end;
end;

{ ************* TZetaSUMADIAS ************* }

type
  TZetaSUMADIAS = class( TZetaSUMAMONTOS )
  private
    procedure SetFiltroDias( const iTipoIncidencia: Integer );
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaSUMADIAS.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParamFijos:= 1;
     inherited;
end;

function TZetaSUMADIAS.Calculate: TQREvResult;
begin
     FFormula := '1';                         // Solo puede haber una incidencia por tarjeta
     FFormulaSalario := 'CB_SALARIO';         // Siempre usar� el salario de la tarjeta
     FFactor := DefaultTasa( 1, 1.0 );
     SetFiltroDias( ParamInteger(0) );
     Result := GetResultado( DatasetEvaluador.FieldByName( 'NO_ASI_INI' ).AsDateTime,
                             DatasetEvaluador.FieldByName( 'NO_ASI_FIN' ).AsDateTime );
end;

procedure TZetaSUMADIAS.SetFiltroDias( const iTipoIncidencia: Integer );
begin
{
1 = Falta Injustificada
2 = Incapacidad
3 = Vacaciones
4 = Permiso c/Goce
5 = Permiso s/Goce
6 = Falta Justificada
7 = Suspensi�n
8 = Otro Permiso
9 = Festivo
10 = No Trabajado
}
     case iTipoIncidencia of
          1 : FFiltro := Format( 'AU_TIPO=%s', [ EntreComillas( 'FI' ) ] );   // Faltas Injustificadas
          2..10 : FFiltro := Format( 'AU_TIPODIA=%d', [ iTipoIncidencia - 1 ] ); // Orden de enumerado eTipoDiaAusencia
     else
         ErrorParametro(  'Solo se permiten valores 1 a 10', '' );
     end;
end;

{ ************* TZetaSUMAEXENTO ************* }

type
  TZetaSUMAEXENTO = class( TZetaSUMABASE )
  private
    FTope, FSalMin : TPesos;
    FInicioAsis, FFinAsis, FFechaCorte: TDate;
  protected
    function CalculaDataSet: TQREvResult; override;
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaSUMAEXENTO.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParamFijos:= 1;

     inherited;

     if ( not ZetaEvaluador.CalculandoNomina ) then
     begin
          AgregaRequerido( 'NOMINA.CB_ZONA_GE', enNomina );
          ZetaEvaluador.oZetaCreator.PreparaSalMin;
     end;

     oZetaProvider.InitGlobales;

end;

function TZetaSUMAEXENTO.Calculate: TQREvResult;
begin
     FFormula := ParamString(0);
     FFormulaSalario := DefaultString( 1, 'CB_SALARIO / 8' );
     FFactor := DefaultFloat( 2, 2 );
     FFiltro := DefaultString( 4, VACIO );
     FInicioAsis := DatasetEvaluador.FieldByName( 'NO_ASI_INI' ).AsDateTime;
     FFinAsis := DatasetEvaluador.FieldByName( 'NO_ASI_FIN' ).AsDateTime;
     FFechaCorte := ZetaCommonTools.LastCorte( FInicioAsis, oZetaProvider.GetGlobalInteger( K_GLOBAL_PRIMER_DIA ) );

     if ZetaEvaluador.CalculandoNomina then
        FSalMin := TCalcNomina( ZetaEvaluador.CalcNomina ).nEmpleadoSalMin
     else
         FSalMin := ZetaEvaluador.oZetaCreator.dmSalMin.GetSalMin( FInicioAsis, DatasetEvaluador.FieldByName( 'CB_ZONA_GE' ).AsString );

     FTope := DefaultFloat( 3, FSalMin * 5 );

     Result := GetResultado( FFechaCorte, FFinAsis );
end;

function TZetaSUMAEXENTO.CalculaDataSet: TQREvResult;
var
   rMonto: TPesos;
   rFormula, rSalario: TPesos;
   rTotalExento: TPesos;
   rTotalSem, rExentoSem: TPesos;
   rTotalSemAnt, rExentoSemAnt: TPesos;
   dFecha, dFechaSemana: TDate;
   iSemana: Integer;
   lTituloSemAnterior, lHuboSemAnterior: Boolean;

   procedure AgregaTituloSemana;
   begin
        with ZetaEvaluador.Rastreador do
        begin
             RastreoHeader( 'EVALUACION PRENOMINA SEMANA #' + IntToStr( iSemana ) );
             RastreoEspacio;
             RastreoTextoLibre( 'POS  FECHA      DIA   SALARIO  SAL/HORA  HORAS  FACTOR      MONTO' );
        end;
   end;

   procedure AgregaTituloRastreo;
   begin
        with ZetaEvaluador.Rastreador do
        begin
             RastreoTextoLibre( 'Tope Exento: ' + PadL( Format( '%8.2n', [ FTope ] ), 11 ) );
             RastreoTextoLibre( 'Sal. M�nimo: ' + PadL( Format( '%8.2n', [ FSalMin ] ), 11 ) );
        end;
        AgregaTituloSemana;
   end;

   procedure AgregaTotalExento( const rMontoTotal: TPesos );
   begin
        with ZetaEvaluador.Rastreador do
        begin
             //             Exento ( MIN( 416.64 / 2, 234.00 ) :          208.32
             RastreoPesos( Format( ' Exento - MIN( %s / 2, %s ) : ', [ Trim( Format( '%7.2n', [ rMontoTotal ] ) ),
                                                                       Trim( Format( '%7.2n', [ FTope ] ) ) ] ),
                           rMin( rMontoTotal / 2, FTope ) );
        end;
   end;

   procedure AgregaDetalleRastreo;
   var
      sPosicion: String;
   begin
        if ( dFecha < FInicioAsis ) then   // Inicio de semana
           sPosicion := Space(3)
        else
            sPosicion := PadL( SuperEv.DataSet.FieldByName( 'AU_POSICIO' ).AsString, 3 );
        with ZetaEvaluador.Rastreador do
        begin
             //  1  07/Oct/05  Vie    500.00     52.08   3.00    2        312.48
             //  2  08/Oct/05  Sab    500.00     52.08   0.00    2          0.00
             RastreoTextoLibre( sPosicion + Space(2) +
                                FechaCorta( dFecha ) + Space(2) +
                                Copy( DiaSemana( dFecha ), 1, 3 ) + Space(2) +
                                PadL( Format( '%5.2n', [ SuperEv.DataSet.FieldByName( 'CB_SALARIO' ).AsFloat ] ), 8 ) + Space(2) +
                                PadL( Format( '%5.2n', [ rSalario ] ), 8 ) + Space(2) +
                                PadL( Format( '%2.2n', [ rFormula ] ), 5 ) + Space(2) +
                                PadC( Format( '%g', [ FFactor ] ), 6 ) + Space(2) +
                                PadL( Format( '%6.2n', [ rMonto ] ), 9 )
                                );
        end;
   end;

   procedure SumaExentoSemana;
   begin
        if ( rTotalSem > 0 ) then              // Hay que exentar algo de la semana - Sin considerar dias de la semana anterior
        begin
             rExentoSem := rMin( rTotalSem / 2, FTope );
             rExentoSemAnt := rMin( rTotalSemAnt / 2, FTope );
             rExentoSem := rMax( rExentoSem - rExentoSemAnt, 0 );
             rTotalExento := rTotalExento + rExentoSem;
        end
        else
            rExentoSem := 0;

        if ZetaEvaluador.Rastreando then
        begin
             with ZetaEvaluador.Rastreador do
             begin
                  RastreoEspacio;
                  AgregaTotalExento( rTotalSem );
                  if ( iSemana = 1 ) and lHuboSemAnterior then
                  begin
                       RastreoPesos( '(-) Exento acumulado - Inicio de Semana : ', rExentoSemAnt );
                       RastreoPesos( '(=)                              Exento : ', rExentoSem );
                  end;
             end;
        end;
   end;

   procedure AgregaTotalRastreo;
   begin
        with ZetaEvaluador.Rastreador do
        begin
             RastreoEspacio;
             RastreoPesos( 'TOTAL EXENTO DEL PERIODO : ', rTotalExento );
        end;
   end;

begin
     rTotalExento := 0;
     lTituloSemAnterior := FALSE;
     lHuboSemAnterior := FALSE;

     if ZetaEvaluador.Rastreando then
     begin
          iSemana := 1;
          AgregaTituloRastreo;
     end;

     with SuperEv.DataSet do
     begin
          dFechaSemana := FFechaCorte + 6;    // Revisar la primer semana
          rTotalSem := 0;
          rTotalSemAnt := 0;

          while ( not EOF ) do
          begin
               dFecha := FieldByName( 'AU_FECHA' ).AsDateTime;
               rFormula := GetSuperEv;
               rSalario := GetSalarioEv;
               rMonto := rFormula * rSalario * FFactor;

               while ( dFecha > dFechaSemana ) do     // Dimensiona la semana
               begin
                    SumaExentoSemana;                      // Suma el exento de la semana que pas�
                    FFechaCorte := dFechaSemana + 1;
                    dFechaSemana := FFechaCorte + 6;
                    rTotalSem := 0;
                    rTotalSemAnt := 0;

                    if ZetaEvaluador.Rastreando then
                    begin
                         iSemana := iSemana + 1;   // Cambia de Semana
                         AgregaTituloSemana;
                    end;
               end;

               if ( dFecha < FInicioAsis ) then   // Inicio de semana
               begin
                    rTotalSemAnt := rTotalSemAnt + rMonto;

                    if ZetaEvaluador.Rastreando and ( not lTituloSemAnterior ) then
                    begin
                         ZetaEvaluador.Rastreador.RastreoTextoLibre( 'Inicio de Semana' );
                         lHuboSemAnterior := TRUE;
                         lTituloSemAnterior := TRUE;
                    end;
               end
               else if ZetaEvaluador.Rastreando and lTituloSemAnterior then
               begin
                    AgregaTotalExento( rTotalSemAnt );
                    ZetaEvaluador.Rastreador.RastreoEspacio;
                    lTituloSemAnterior := FALSE;
               end;

               if ZetaEvaluador.Rastreando then
                  AgregaDetalleRastreo;

               rTotalSem := rTotalSem + rMonto;

               Next;
          end;
          SumaExentoSemana;       // Agrega el remanente de la ultima semana
     end;

     if ZetaEvaluador.Rastreando then
        AgregaTotalRastreo;

     with Result do
     begin
          Kind:= resDouble;
          dblResult := rTotalExento;
     end;
end;

{ ************* TZetaIMSSEXENTO ************* }

type
  TZetaIMSSEXENTO = class( TZetaSUMABASE )
  private
    FInicioAsis, FFinAsis, FFechaCorte: TDate;
    FRegla3x3: eRegla3x3;
    FDiasAplicaExento: eDiasAplicaExento;
  protected
    function CalculaDataSet: TQREvResult; override;
    procedure PreparaAgente; override;
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaIMSSEXENTO.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParamFijos:= 0;

     inherited;

     oZetaProvider.InitGlobales;
end;

function TZetaIMSSEXENTO.Calculate: TQREvResult;
begin
     FFormula := 'AU_EXTRAS';
     FFormulaSalario := DefaultString( 0, 'CB_SALARIO / 8' );
     FFactor := 2;
     FFiltro := VACIO;
     FInicioAsis := DatasetEvaluador.FieldByName( 'NO_ASI_INI' ).AsDateTime;
     FFinAsis := DatasetEvaluador.FieldByName( 'NO_ASI_FIN' ).AsDateTime;
     with oZetaProvider do
     begin
          FRegla3x3 := eRegla3x3( GetGlobalInteger( K_GLOBAL_HRS_EXENTAS_IMSS ) );
          FDiasAplicaExento := eDiasAplicaExento( GetGlobalInteger( K_GLOBAL_DIAS_APLICA_EXENTO) );
          FFechaCorte := ZetaCommonTools.LastCorte( FInicioAsis, GetGlobalInteger( K_GLOBAL_PRIMER_DIA ) );
     end;
     Result := GetResultado( FFechaCorte, FFinAsis );
end;

procedure TZetaIMSSEXENTO.PreparaAgente;
begin
     inherited PreparaAgente;
     with FAgente do
     begin
          AgregaColumna( 'AUSENCIA.AU_DOBLES', TRUE, enAusencia, tgFloat, 0, 'AU_DOBLES' );
          AgregaColumna( 'AUSENCIA.AU_STATUS', TRUE, enAusencia, tgNumero, 0, 'AU_STATUS' );
          AgregaColumna( 'AUSENCIA.AU_TIPODIA', TRUE, enAusencia, tgNumero, 0, 'AU_TIPODIA' );
     end;
end;

function TZetaIMSSEXENTO.CalculaDataSet: TQREvResult;
const
     K_EXENTAS_DIARIO = 3;
     K_EXENTAS_SEMANAL = 3;
var
   rMonto: TPesos;
   rSalario: TPesos;
   rExentoSem, rTotalExento: TPesos;
   rExtras, rDobles, rTriples, rExentas: TDiasHoras;
   rDobles3x3, rTriples3x3: TDiasHoras;
   iDiasExtras: Integer;
   dFecha, dFechaSemana: TDate;
   iSemana: Integer;
   lTituloSemAnterior: Boolean;
   StatusAusencia: eStatusAusencia;
   TipoDia: eTipoDiaAusencia;

   procedure AgregaTituloSemana;
   begin
        with ZetaEvaluador.Rastreador do
        begin
             RastreoHeader( 'EVALUACION PRENOMINA SEMANA #' + IntToStr( iSemana ) );
             RastreoEspacio;
             RastreoTextoLibre( 'POS FECHA     DIA EXTRAS DOBLES TRIPLES EXENTAS SAL/HORA    MONTO' );
        end;
   end;

   procedure AgregaTituloRastreo;
   begin
        with ZetaEvaluador.Rastreador do
        begin
             RastreoTextoLibre( 'Regla 3x3  : ' + ZetaCommonLists.ObtieneElemento( lfRegla3x3, Ord( FRegla3x3 ) ) );
        end;
        AgregaTituloSemana;
   end;

   procedure AgregaExentoSem;
   begin
        with ZetaEvaluador.Rastreador do
        begin
             //                                         Exento :          208.32
             RastreoEspacio;
             RastreoPesos( ' Exento : ', rExentoSem );
        end;
   end;

   procedure AgregaDetalleRastreo;
   var
      sPosicion, sMonto: String;
      sDobles, sTriples: String;
   begin
        if ( dFecha < FInicioAsis ) then   // Inicio de semana
        begin
             sPosicion := Space(3);
             sMonto := PadLCar( '', 8, '-' );
        end
        else
        begin
             sPosicion := PadL( SuperEv.DataSet.FieldByName( 'AU_POSICIO' ).AsString, 3 );
             sMonto := PadL( Format( '%6.2n', [ rMonto ] ), 8 );
        end;

        if ( FRegla3x3 = r3x3_Cuantas ) then    // No se evaluan dobles y triples
        begin
             sDobles := Space(6);
             sTriples := Space(7);
        end
        else
        begin
             sDobles := PadL( Format( '%3.2n', [ rDobles ] ), 6 );
             sTriples := PadL( Format( '%4.2n', [ rTriples ] ), 7 );
        end;

        with ZetaEvaluador.Rastreador do
        begin
             {
               1 07/Oct/05 Vie   4.00                   3.00    52.08   312.50
               2 08/Oct/05 S�b   1.00                   0.00    52.08     0.00
                                          �
               1 07/Oct/05 Vie   4.00   2.00    2.00    3.50    52.08   364.56
               2 08/Oct/05 S�b   1.00   0.00    1.00    0.00    52.08     0.00
             }
             RastreoTextoLibre( sPosicion + Space(1) +
                                FechaCorta( dFecha ) + Space(1) +
                                Copy( DiaSemana( dFecha ), 1, 3 ) + Space(1) +
                                PadL( Format( '%3.2n', [ rExtras ] ), 6 ) + Space(1) +
                                sDobles + Space(1) +
                                sTriples + Space(1) +
                                PadL( Format( '%4.2n', [ rExentas ] ), 7 ) + Space(1) +
                                PadL( Format( '%5.2n', [ rSalario ] ), 8 ) + Space(1) +
                                sMonto );
        end;
   end;

   procedure AgregaTotalRastreo;
   begin
        with ZetaEvaluador.Rastreador do
        begin
             RastreoEspacio;
             RastreoPesos( 'TOTAL EXENTO DEL PERIODO : ', rTotalExento );
        end;
   end;

begin
     rTotalExento := 0;
     lTituloSemAnterior := FALSE;

     if ZetaEvaluador.Rastreando then
     begin
          iSemana := 1;
          AgregaTituloRastreo;
     end;

     with SuperEv.DataSet do
     begin
          dFechaSemana := FFechaCorte + 6;    // Revisar la primer semana
          iDiasExtras := 0;
          rExentoSem := 0;
          while ( not EOF ) do
          begin
               dFecha := FieldByName( 'AU_FECHA' ).AsDateTime;
               rSalario := GetSalarioEv;
               rExtras := GetSuperEv;
               rDobles := FieldByName( 'AU_DOBLES' ).AsFloat;
               StatusAusencia := eStatusAusencia( FieldByName( 'AU_STATUS' ).AsInteger );
               TipoDia := eTipoDiaAusencia( FieldByName( 'AU_TIPODIA' ).AsInteger );
               rTriples := ZetaCommonTools.rMax( rExtras - rDobles, 0 );           // Para mostrarlo en rastreo

               while ( dFecha > dFechaSemana ) do     // Dimensiona la siguiente semana
               begin
                    if ZetaEvaluador.Rastreando then
                       AgregaExentoSem;
                    FFechaCorte := dFechaSemana + 1;
                    dFechaSemana := FFechaCorte + 6;
                    iDiasExtras := 0;
                    rExentoSem := 0;

                    if ZetaEvaluador.Rastreando then
                    begin
                         iSemana := iSemana + 1;   // Cambia de Semana
                         AgregaTituloSemana;
                    end;
               end;

               if ( dFecha < FInicioAsis ) then   // Inicio de semana
               begin
                    if ZetaEvaluador.Rastreando and ( not lTituloSemAnterior ) then
                    begin
                         ZetaEvaluador.Rastreador.RastreoTextoLibre( 'Inicio de Semana' );
                         lTituloSemAnterior := TRUE;
                    end;
               end
               else if ZetaEvaluador.Rastreando and lTituloSemAnterior then
               begin
                    ZetaEvaluador.Rastreador.RastreoEspacio;
                    lTituloSemAnterior := FALSE;
               end;

               rExentas := 0;

               if ( rExtras > 0 ) and ( AplicaRegla3X3(FDiasAplicaExento ,StatusAusencia,TipoDia )  ) then
               begin
                    iDiasExtras := ( iDiasExtras + 1 );
                    if ( dFecha >= FInicioAsis ) and             // Si es inicio de semana no se exenta en este periodo
                       ( iDiasExtras <= K_EXENTAS_SEMANAL ) then
                    begin
                         case FRegla3x3 of
                              r3x3_Proporcional:
                              begin
                                   rDobles3x3 := ZetaCommonTools.rMin( rDobles, K_EXENTAS_DIARIO );
                                   rTriples3x3 := ZetaCommonTools.rMin( rTriples, ( K_EXENTAS_DIARIO - rDobles3x3 ) );
                                   rExentas := rDobles3x3 + ( rTriples3x3 * 1.5 ); // Cada hora doble vale 1, Cada hora triple vale 1.5  ( 3 / 2 = 1.5 )
                              end;
                              r3x3_Cuantas:
                              begin
                                   rExentas := ZetaCommonTools.rMin( rExtras, K_EXENTAS_DIARIO );
                              end;
                              else // SoloDobles
                              begin
                                   if ( rDobles > 0 ) then // Solo considera horas dobles como exentas
                                      rExentas := ZetaCommonTools.rMin( rDobles, K_EXENTAS_DIARIO )
                                   else
                                       rExentas := 0;
                              end;
                         end;
                    end;
               end;

               rMonto := rExentas * rSalario * FFactor;

               if ZetaEvaluador.Rastreando then
                  AgregaDetalleRastreo;

               rExentoSem := rExentoSem + rMonto;
               rTotalExento := rTotalExento + rMonto;

               Next;
          end;
          if ZetaEvaluador.Rastreando then
             AgregaExentoSem;                  // Muestra el remanente de la ultima semana
     end;

     if ZetaEvaluador.Rastreando then
        AgregaTotalRastreo;

     with Result do
     begin
          Kind:= resDouble;
          dblResult := rTotalExento;
     end;
end;

{ ************* TZetaEMPLEADOAUSENCIAS ************* }

type
  TZetaEMPLEADOAUSENCIAS = class( TZetaRegresaQuery )
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  protected
    iRegresa : integer;
    FFecha : TDate;
    procedure CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQREvResultType; var lEsFecha : Boolean );virtual;
  end;

procedure TZetaEMPLEADOAUSENCIAS.CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQREvResultType; var lEsFecha : Boolean );
begin
end;

procedure TZetaEMPLEADOAUSENCIAS.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sFecha, sCampo : string;
   lEsFecha : Boolean;
begin
//     TipoRegresa := resDouble;
//     EsFecha := True;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );

     if ParametrosConstantes then
     begin
          if ( not ParamVacio( 0 ) and ( Argument( 0 ).Kind = resInt ) ) then  // Ultima Incapacidad
          begin
               iRegresa:= DefaultInteger( 0, 1 );
               FFecha := DefaultDate( 1, NullDateTime );
               sFecha := EntreComillas( DateToStrSQL( FFecha ) );
               CaseCampo( iRegresa, sCampo, TipoRegresa, lEsFecha );
               EsFecha:= lEsFecha;
               if FFecha = NullDateTime then
                  AgregaScript( Format( GetScript, [ sCampo, 'COLABORA.CB_CODIGO', 'COLABORA.CB_CODIGO' ] ), TipoRegresa, lEsFecha )
               else
                  AgregaScript( Format( GetScript, [ sCampo, 'COLABORA.CB_CODIGO', sFecha, sFecha ] ), TipoRegresa, lEsFecha );
          end
          else
          begin
               if ParamVacio(1) then
               begin
                    TipoRegresa := resString;
                    iRegresa := 0;
               end
               else
               begin
                    iRegresa := ParamInteger(1);
                    CaseCampo( iRegresa, sCampo, TipoRegresa, lEsFecha );
                    EsFecha:= lEsFecha;
               end;

               if ParamVacio(0) then
               begin
                    sFecha := EntreComillas( DateToStrSQL( oZetaProvider.FechaDefault ) );
                    FFecha := NullDateTime;
               end
               else
               begin
                    sFecha := ParamCampoFecha( 0 );
                    FFecha := 1;   { Para que use otro Script }
               end;
               if iRegresa = 0 then
                  AgregaScript( Format( GetScript, [ sFecha, sFecha, 'COLABORA.CB_CODIGO' ] ), TipoRegresa, FALSE  )
               else
               begin
                    if FFecha = NullDateTime then
                       AgregaScript( Format( GetScript, [ sCampo,'COLABORA.CB_CODIGO', 'COLABORA.CB_CODIGO' ] ), TipoRegresa, lEsFecha )
                    else
                       AgregaScript( Format( GetScript, [ sCampo, 'COLABORA.CB_CODIGO', sFecha, sFecha ] ), TipoRegresa, lEsFecha );
               end
          end;
     end;
end;

function TZetaEMPLEADOAUSENCIAS.Calculate: TQREvResult;
var
   sFecha, sCampo, sEmpleado : string;
   TipoRegresa : TQREvResultType;
   lEsFecha : Boolean;
begin
     sEmpleado := IntToStr( ZetaEvaluador.GetEmpleado );
     if ( not ParamVacio( 0 ) and ( Argument( 0 ).Kind = resInt ) ) then  // Ultima Incapacidad
     begin
          iRegresa:= ParamInteger( 0 );
          FFecha := DefaultDate( 1, NullDateTime );
          sFecha := EntreComillas( DateToStrSQL( FFecha ) );
          CaseCampo( iRegresa, sCampo, TipoRegresa, lEsFecha );
          EsFecha := lEsFecha;
          if FFecha = NullDateTime then
             Result := EvaluaQuery( Format( GetScript, [ sCampo, sEmpleado, sEmpleado ] ) )
          else
             Result := EvaluaQuery( Format( GetScript, [ CampoRes( sCampo ), sEmpleado, sFecha, sFecha ] ) );
     end
     else
     begin
          if ParamVacio(1) then
             iRegresa := 0
          else
          begin
               iRegresa := ParamInteger(1);
               CaseCampo( iRegresa, sCampo, TipoRegresa, lEsFecha );
               EsFecha := lEsFecha;
          end;

          sFecha := EntreComillas( DateToStrSQL( DefaultDate( 0, oZetaProvider.FechaDefault ) ) );
          FFecha := 1;   { Para que use otro Script }

          if iRegresa = 0 then
             Result := EvaluaQuery( Format( GetScript, [ sFecha, sFecha, sEmpleado ] ) )
          else
             Result := EvaluaQuery( Format( GetScript, [ sCampo, sEmpleado, sFecha, sFecha ] ) )
     end;
end;

{ ************* TZetaEMPLEADOINCAPACIDAD ************* }

type
  TZetaEMPLEADOINCAPACIDAD = class( TZetaEMPLEADOAUSENCIAS )
  public
    function  GetScript : String; override;
  protected
    procedure CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQREvResultType; var lEsFecha : Boolean );override;
  end;

procedure TZetaEMPLEADOINCAPACIDAD.CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQrEvResultType; var lEsFecha : Boolean );
begin
     Tipo := resDouble;
     lEsFecha := FALSE;
     case iRegresa of
          1: begin
                  sCampo := 'IN_FEC_INI';   {FECHA INICIO DE INCAPACIDAD}
                  lEsFecha := TRUE;
             end;
          2: begin
                  sCampo := 'IN_FEC_FIN';   {FECHA DE FIN DE INCAPACIDAD}
                  lEsFecha := TRUE;
             end;
          3: sCampo := 'IN_DIAS';           {DIAS QUE ABARCA INCAPACIDAD}
          4: begin
                  sCampo := 'IN_TIPO';      {TIPO DE ULTIMA INCAPACIDAD}
                  Tipo := resString;
             end;
          5: begin
                  sCampo := 'IN_NUMERO';    {NUMERO DE ULTIMA INCAPACIDAD}
                  Tipo := resString;
             end;
          6: begin
                  sCampo := 'IN_DIAS';      {DIAS DE DURACION ULTIMA INCAPACIDAD}
                  Tipo :=  resInt;
             end;
          7: begin
                  sCampo := 'IN_MOTIVO';    {VALOR LOGICO QUE INDICA SI LA ULTIMA INCAPACIDAD ES INICIAL O SUBSECUENTE/RECAIDA}
                  Tipo := resInt;
             end
          else
          begin
               sCampo := 'US_CODIGO';
               Tipo := resInt;
          end;
     end;
end;

function  TZetaEMPLEADOINCAPACIDAD.GetScript : String;
begin
     if ( iRegresa = 0 ) then
        Result := SelRes('SP_EMP_INCA( %s, %s, %s )')
     else if ( FFecha <> NullDateTime ) then
        Result := 'select %s from INCAPACI INC where ( INC.CB_CODIGO = %s ) and ( INC.IN_FEC_INI <= %s ) and ( INC.IN_FEC_FIN > %s )'
     else
        Result := 'select %s from INCAPACI INC where ( INC.CB_CODIGO = %s ) and ( INC.IN_FEC_INI = ( select MAX( INC1.IN_FEC_INI) from INCAPACI INC1 where ( INC1.CB_CODIGO = %s ) ) )';
end;

{ ************* TZetaEMPLEADOPERMISO ************* }

type
  TZetaEMPLEADOPERMISO = class( TZetaEMPLEADOAUSENCIAS )
  public
    function  GetScript : String; override;
  protected
    procedure CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQREvResultType; var lEsFecha : Boolean );override;
  end;

procedure TZetaEMPLEADOPERMISO.CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQrEvResultType; var lEsFecha : Boolean );
begin
     Tipo := resDouble;
     lEsFecha := FALSE;
     case iRegresa of
          1: begin
                  sCampo := 'PM_FEC_INI';
                  lEsFecha := TRUE;
             end;
          2: begin
                  sCampo := 'PM_FEC_FIN';
                  lEsFecha := TRUE;
             end;
          3: sCampo := 'PM_DIAS';
          4: begin
                  sCampo := 'PM_TIPO';
                  Tipo := resString;
             end;
          5: begin
                  sCampo := 'PM_NUMERO';
                  Tipo := resString;
             end;
          6: begin
                  sCampo := 'PM_DIAS';
                  Tipo :=  resInt;
             end;
          //7: {no implementado}
          8: begin
                  sCampo := 'PM_CLASIFI'; {Clasificaci�n [ "S" , "N", "J", "U", "O" ]}
                  Tipo := resString;
             end;
          else
          begin
               sCampo := 'US_CODIGO';
               Tipo := resInt;
          end;
     end;
end;

function  TZetaEMPLEADOPERMISO.GetScript : String;
begin
     if ( iRegresa = 0 ) then
        Result := SelRes('SP_EMP_PERM( %s, %s, %s )')
     else if ( FFecha <> NullDateTime ) then
        Result := 'select %s from PERMISO PE where ( PE.CB_CODIGO = %s ) and ( PE.PM_FEC_INI <= %s ) and ( PE.PM_FEC_FIN > %s )'
     else
        Result := 'select %s from PERMISO PE where ( PE.CB_CODIGO = %s ) and ( PE.PM_FEC_INI = ( select MAX( PE1.PM_FEC_INI) from PERMISO PE1 where ( PE1.CB_CODIGO = %s ) ) )';
end;

{ ************* TZetaEMPLEADOVACACION ************* }

type
  TZetaEMPLEADOVACACION = class( TZetaEMPLEADOAUSENCIAS )
  public
    function  GetScript : String; override;
  protected
    procedure CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQREvResultType; var lEsFecha : Boolean );override;
  end;

procedure TZetaEMPLEADOVACACION.CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQrEvResultType; var lEsFecha : Boolean );
begin
     Tipo := resDouble;
     lEsFecha := FALSE;
     case iRegresa of
          1: begin
                  sCampo := 'VA_FEC_INI';
                  lEsFecha := TRUE;
             end;
          2: begin
                  sCampo := 'VA_FEC_FIN';
                  lEsFecha := TRUE;
             end;
          3: sCampo := 'VA_GOZO';
          4: begin
                  sCampo := 'VA_TIPO';
                  Tipo := resString;
             end;
          //5: {NO IMPLEMENTADO}
          6: sCampo := 'VA_GOZO';
          7: sCampo := 'VA_PAGO';
          8: begin
                  sCampo := 'VA_NOMNUME';
                  Tipo := resInt;
             end;
          9: sCampo := 'VA_P_PRIMA';   
          else
          begin
               sCampo := 'US_CODIGO';
               Tipo := resInt;
          end;
     end;
end;

function  TZetaEMPLEADOVACACION.GetScript : String;
begin
     if ( iRegresa = 0 ) then
        Result := SelRes('SP_EMP_VACA( %s, %s, %s )')
     else if ( FFecha <> NullDateTime ) then
        Result := 'select %s from VACACION VA where ( VA.CB_CODIGO = %s ) and ( VA.VA_FEC_INI <= %s ) and ( VA.VA_FEC_FIN > %s ) AND (VA.VA_TIPO =1)'
     else
        Result := 'select %s from VACACION VA where ( VA.CB_CODIGO = %s ) and ( VA.VA_FEC_INI = ( select MAX( VA1.VA_FEC_INI) from VACACION VA1 where ( VA1.CB_CODIGO = %s ) AND (VA1.VA_TIPO =1))) AND (VA.VA_TIPO =1)'
end;

{ ************* TZetaFECHAKARDEX ************* }

type
  TZetaFECHAKARDEX = class( TZetaSFuncion )
  private
    sCampo: String;
  protected
    procedure PreparaAgente; override;
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function  GetScript : String; override;
  end;

procedure TZetaFECHAKARDEX.PreparaAgente;
begin
     with FAgente do
     begin
          AgregaFiltro( 'KARDEX.CB_CODIGO = :Empleado', TRUE, enKardex );
          AgregaFiltro( '( KARDEX.CB_FECHA = ( select MAX( K2.CB_FECHA ) from KARDEX K2 where ( K2.CB_CODIGO = :Empleado2 ) and ( K2.CB_FECHA <= :Fecha2 ) ) )', TRUE, enKardex );
          AgregaOrden( 'KARDEX.CB_NIVEL', FALSE, enKardex );
     end;
end;

function TZetaFECHAKARDEX.GetScript : string;
begin
     {$ifdef INTERBASE}
     Result := 'select %s from SP_FECHA_KARDEX( %s , %s ) KARDEX';
     {$endif}
     {$ifdef MSSQL}
     Result := 'DBO.SP_%s( %s, %s )';
     {$endif}
end;

procedure TZetaFECHAKARDEX.GetRequeridos( var TipoRegresa : TQREvResultType );
var
//   sCampo : String;
   lEsFecha : Boolean;
begin
     ValidaConfidencial;
     ParametrosFijos( 2 );
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     if ParametrosConstantes then
     begin
          sCampo := ParamString( 0 );
          TipoRegresa := InvestigaTipo( enKardex, sCampo, lEsFecha );
          EsFecha:= lEsFecha;
{$ifdef MSSQL}
          sCampo := StrTransform( sCampo, PUNTO, '_' );
{$endif}
          if TipoRegresa <> resError then
             AgregaScript( Format( GetScript, [ sCampo, ParamCampoFecha( 1 ), 'COLABORA.CB_CODIGO'
                                                 ] ), TipoRegresa, lEsFecha );
     end
end;

function TZetaFECHAKARDEX.Calculate: TQREvResult;
var
   sExpresion : String;
   Empleado : Integer;
   Fecha : TDate;
begin
     Empleado:= ZetaEvaluador.GetEmpleado;
     Fecha:= ParamDate(1);
     sExpresion:= ParamString( 0 );

     PreparaSuperEv( sExpresion, VACIO,
                     enKardex, [tgFloat,tgNumero,tgTexto,tgFecha,tgBooleano]);
     CierraDataSet;
     SetParamValue( 'Empleado', tgNumero, Empleado );
     SetParamValue( 'Empleado2', tgNumero, Empleado );
     SetParamValue( 'Fecha2', tgFecha, Fecha );
     AbreDataSet;
     with Result do
     begin
          Kind := SuperEv.Value.Kind;
          esFecha := ( SuperEv.Value.ResFecha );
          case Kind of
               resDouble : dblResult:= SuperEv.Value.dblResult;
               resInt : intResult:= SuperEv.Value.intResult;
               resBool : booResult:= SuperEv.Value.booResult;
               resString : strResult:= SuperEv.Value.strResult;
          else
              ErrorFuncion( 'Error en la Evaluaci�n', 'Funci�n: ' + sExpresion );
          end;
     end;
end;

{ ************* TZetaSALARIOFECHA ************* }

type
  TZetaSALARIOFECHA = class( TZetaRegresaQuery )
  public
    function Calculate: TQREvResult; override;
    function GetScript : string; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  private
    iNumCampo : Integer;
    function CaseCampo( iRegresa : Integer ): String;
  end;

function TZetaSALARIOFECHA.GetScript : string;
begin
     {$ifdef INTERBASE}
     //  SP_FECHA_KARDEX (  FECHA DATE,  EMPLEADO INTEGER)
     Result := 'select %s from SP_FECHA_KARDEX( %s , %s )';
     {$endif}
     {$ifdef MSSQL}
     case iNumCampo of
        1 : Result := 'select %s = DBO.SP_KARDEX_CB_SALARIO( %s, %s )';
        2 : Result := 'select %s = DBO.SP_KARDEX_CB_SAL_INT( %s, %s )';
        else Result := 'select %s = DBO.SP_KARDEX_CB_PER_VAR( %s, %s )';
     end;
     {$endif}
end;

function TZetaSALARIOFECHA.CaseCampo( iRegresa : Integer ): String;
begin
     case iRegresa of
          1: Result := 'CB_SALARIO';
          2: Result := 'CB_SAL_INT';
          3: Result := 'CB_PER_VAR';
     else
         ErrorParametro(  'Solo se Permiten Valores 1 a 3', '' );
     end;
     iNumCampo := iRegresa;
end;

procedure TZetaSALARIOFECHA.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sCampo : String;
begin
     //SAL_FECHA( iRegresa, dFecha )
     //ParametrosFijos( 2 );
     ValidaConfidencial;
     oZetaProvider.GetDatosActivos;
     TipoRegresa:= resDouble;
     if ParametrosConstantes then
     begin
          sCampo := CaseCampo( DefaultInteger( 0, 1 ) );
          AgregaScript( Format( GetScript, [ sCampo, ParamCampoDefFecha( 1, EntreComillas( DateToStrSQL( oZetaProvider.FechaDefault ) ) ),
                                             'COLABORA.CB_CODIGO' ] ), TipoRegresa, FALSE );
     end
     else
         AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

function TZetaSALARIOFECHA.Calculate: TQREvResult;
var
   sCampo : String;
begin
     sCampo := CaseCampo( DefaultInteger( 0, 1 ) );
     Result := EvaluaQuery( Format( GetScript, [ sCampo, EntreComillas( DateToStrSQL( DefaultDate( 1, oZetaProvider.FechaDefault ) ) ),
                                                 IntToStr( ZetaEvaluador.GetEmpleado ) ] ) );
end;

{ ************* TZetaANTIGUEDAD ************* }

type
  TZetaANTIGUEDAD = class( TZetaFunc )
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaANTIGUEDAD.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
     if ( DefaultInteger( 2, 1 ) =  1 ) then
        AgregaRequerido( 'COLABORA.CB_FEC_ANT', enEmpleado )
     else
        AgregaRequerido( 'COLABORA.CB_FEC_ING', enEmpleado );
end;

function TZetaANTIGUEDAD.Calculate: TQREvResult;
var
   dIngreso, dReferencia: TDate;
begin
     dReferencia := DefaultDate( 0, Date );
     with DataSetEvaluador do
     begin
          if ( DefaultInteger( 2, 1 ) =  1 ) then
             dIngreso := FieldByName( 'CB_FEC_ANT' ).AsDateTime
          else
             dIngreso := FieldByName( 'CB_FEC_ING' ).AsDateTime;
     end;
     with Result do
     begin
          Kind := resDouble;
          if DefaultBoolean( 1, True ) then
             dblResult := Years( dIngreso, dReferencia )
          else
             dblResult := dReferencia - dIngreso + 1;
     end;
end;

{ ************** TZetaDIASACTIVOS **************** }

type
  TZetaDIASACTIVOS = class( TZetaRegresaQuery )
  public
   function GetScript : String; override;
   function Calculate: TQREvResult; override;
   procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaDIASACTIVOS.GetScript : string;
begin
     Result := SelRes('SP_RANGO_STATUS( %s, %s, %s, %s )');
end;

procedure TZetaDIASACTIVOS.GetRequeridos( var TipoRegresa : TQREvResultType );
const
     STATUS_ACTIVO = '1';
var
   sInicial, sFinal: String;
begin
     oZetaProvider.GetDatosActivos;
     TipoRegresa := resInt;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     if ParametrosConstantes then
     begin
          sInicial:= ParamCampoDefFecha( 0, EntreComillas( DateToStrSQL( oZetaProvider.FechaDefault ) ) );
          sFinal:= ParamCampoDefFecha( 1, EntreComillas( DateToStrSQL( oZetaProvider.FechaDefault ) ) );
          AgregaScript( Format( GetScript, [ 'COLABORA.CB_CODIGO',
                                             sInicial, sFinal,
                                             STATUS_ACTIVO ] ), TipoRegresa, FALSE );
     end;
end;

function TZetaDIASACTIVOS.Calculate : TQREvResult;
const
     STATUS_ACTIVO = '1';
begin
     Result:= EvaluaQuery( Format( GetScript, [ IntToStr( ZetaEvaluador.GetEmpleado ),
                           EntreComillas( DateToStrSQL( DefaultDate( 0, oZetaProvider.FechaDefault ) ) ),
                           EntreComillas( DateToStrSQL( DefaultDate( 1, oZetaProvider.FechaDefault ) ) ),
                           STATUS_ACTIVO ] ) );
end;

{ ************* TZetaDIASBIM ************* }

type
  TZetaDIASBIM = class( TZetaConstante )
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaDIASBIM.Calculate: TQREvResult;
var
   iBimestre, iYear: Word;
begin
     oZetaProvider.GetDatosImss;
     with oZetaProvider.DatosImss do
     begin
          iBimestre := 2 * iMin( 6, DefaultInteger( 0, Trunc( ( Mes + 1 ) / 2 ) ) );
          iYear := DefaultInteger( 1, Year );
     end;
     with Result do
     begin
          Kind := resInt;
          intResult := DiasEnMes( iYear, ( iBimestre - 1 ) ) + DiasEnMes( iYear, iBimestre );
     end;
end;

{ ************* TZetaDIASMES ************* }

type
  TZetaDIASMES = class( TZetaConstante )
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaDIASMES.Calculate: TQREvResult;
begin
     oZetaProvider.GetDatosImss;
     with Result, oZetaProvider.DatosImss do
     begin
          Kind := resInt;
          intResult := DiasEnMes( DefaultInteger( 1, Year ), DefaultInteger( 0, Mes ) );
     end;
end;

{ ************* TZetaCUMPLE ************* }

type
  TZetaCumple = class( TZetaRegresa )
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaCUMPLE.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     inherited;
     TipoRegresa:= resDouble;
     EsFecha := TRUE;
     AgregaRequerido( 'COLABORA.CB_FEC_NAC', enEmpleado );
     lQuitarOptimizeCursorB8 := FALSE;         // Si no requiere un query en el calculate, se deja optimizaci�n de Build #8
end;

function TZetaCUMPLE.Calculate: TQREvResult;
 var dFecha : TDate;
begin
     with Result do
     begin
          Kind := resDouble;
          esFecha := TRUE;
          dFecha := DefaultDate( 0, DataSetEvaluador.FieldByName( 'CB_FEC_NAC' ).AsDateTime);
          if dFecha <= NullDateTime then
             dblResult := NullDateTime
          else dblResult := DateTheYear( dFecha,
                                         DefaultInteger( 1, TheYear(Date) ) );
     end;
end;

{ ************* TZetaGENERA_RFC ************* }

type
  TZetaGENERA_RFC = class( TZetaRegresa )
  protected
    FOffSet: integer;
    procedure ValidaParametros;
    procedure ValidaParametro( const sCampo: string; const iPosicion: integer );
  public
    constructor Create(oEvaluador:TQrEvaluator); override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

constructor TZetaGENERA_RFC.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create(oEvaluador);
     FOffSet := 0;
     lQuitarOptimizeCursorB8 := FALSE;         // Si no requiere un query en el calculate, se deja optimizaci�n de Build #8
end;

procedure TZetaGENERA_RFC.ValidaParametro( const sCampo: string; const iPosicion: integer );
begin
     if ParamVacio( iPosicion ) then
        AgregaRequerido( 'COLABORA.' + sCampo, enEmpleado )
     else
         ParamString( iPosicion );
end;

procedure TZetaGENERA_RFC.ValidaParametros;
begin
     ValidaParametro( 'CB_APE_PAT', 0 + FOffset );
     ValidaParametro( 'CB_APE_MAT', 1 + FOffset );
     ValidaParametro( 'CB_NOMBRES', 2 + FOffset );
     if ParamVacio( 3 + FOffSet ) then
        AgregaRequerido( 'COLABORA.CB_FEC_NAC', enEmpleado )
     else
         ParamDate( 3 + FOffSet ); //Se hace la llamada a ParamDate() para validar el Tipo de Dato
end;

procedure TZetaGENERA_RFC.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     inherited;
     TipoRegresa:= resString;
     ValidaParametros;
end;

function TZetaGENERA_RFC.Calculate: TQREvResult;
var
   sPaterno, sMaterno, sNombres: string;
   dFechaNac: TDate;
begin
     with DatasetEvaluador do
     begin
          if ParamVacio( 0 ) then
             sPaterno := FieldByName( 'CB_APE_PAT' ).AsString
          else
              sPaterno  := ParamString( 0 );
          if ParamVacio( 1 ) then
             sMaterno  := FieldByName( 'CB_APE_MAT' ).AsString
          else
              sMaterno  := ParamString( 1 );
          if ParamVacio( 2 ) then
             sNombres  := FieldByName( 'CB_NOMBRES' ).AsString
          else
              sNombres  := ParamString( 2 );
          if ParamVacio( 3 ) then
             dFechaNac := FieldByName( 'CB_FEC_NAC' ).AsDateTime
          else
              dFechaNac := ParamDate( 3 );
     end;
     with Result do
     begin
          Kind := resString;
          strResult := ZetaCommonTools.CalcRFC( sPaterno, sMaterno, sNombres, dFechaNac );
     end;
end;

{ ************* TZetaVALIDA_RFC ************* }

type
  TZetaVALIDA_RFC = class( TZetaGENERA_RFC )
  public
    constructor Create(oEvaluador:TQrEvaluator); override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

constructor TZetaVALIDA_RFC.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create(oEvaluador);
     FOffSet := 1;
     lQuitarOptimizeCursorB8 := FALSE;         // Si no requiere un query en el calculate, se deja optimizaci�n de Build #8
end;

procedure TZetaVALIDA_RFC.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     inherited;
     ValidaParametro( 'CB_RFC', 0 );
end;

function TZetaVALIDA_RFC.Calculate: TQREvResult;
var
   sPaterno, sMaterno, sNombres, sRFC: string;
   dFechaNac: TDate;
begin
     with DatasetEvaluador do
     begin
          if ParamVacio( 0 ) then
             sRFC := FieldByName( 'CB_RFC' ).AsString
          else
              sRFC  := ParamString( 0 );
          if ParamVacio( 1 ) then
             sPaterno := FieldByName( 'CB_APE_PAT' ).AsString
          else
              sPaterno  := ParamString( 1 );
          if ParamVacio( 2 ) then
             sMaterno  := FieldByName( 'CB_APE_MAT' ).AsString
          else
              sMaterno  := ParamString( 2 );
          if ParamVacio( 3 ) then
             sNombres  := FieldByName( 'CB_NOMBRES' ).AsString
          else
              sNombres  := ParamString( 3 );
          if ParamVacio( 4 ) then
             dFechaNac := FieldByName( 'CB_FEC_NAC' ).AsDateTime
          else
              dFechaNac := ParamDate( 4 );
     end;
     with Result do
     begin
          Kind := resString;
          strResult := ZetaCommonTools.RFCVerifica( sRFC, sPaterno, sMaterno, sNombres, dFechaNac );
     end;
end;

{ ************* TZetaGENERA_CURP ************* }

type
  TZetaGENERA_CURP = class( TZetaRegresaQuery )
  private
    function GetClaveEstado( const sEstado: String ): String;
    procedure ValidaParametros;
    procedure ValidaParametro( const sCampo: string; const iPosicion: integer );
  public
    function GetScript : String; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaGENERA_CURP.GetScript: String;
begin
     Result := 'select TB_CURP from ENTIDAD where ( TB_CODIGO = %s )';
end;

function TZetaGENERA_CURP.GetClaveEstado( const sEstado: String ): String;
begin
     Result := VACIO;
     AbreQuery( Format( GetScript, [ ZetaCommonTools.EntreComillas( sEstado ) ] ) );
     with FQuery do
     begin
          if ( not EOF ) then
          begin
               Result := FieldByName( 'TB_CURP' ).AsString;
          end;
     end;
end;

procedure TZetaGENERA_CURP.ValidaParametro( const sCampo: string; const iPosicion: integer );
begin
     if ParamVacio( iPosicion ) then
        AgregaRequerido( 'COLABORA.' + sCampo, enEmpleado )
     else
         ParamString( iPosicion );
end;

procedure TZetaGENERA_CURP.ValidaParametros;
begin
     ValidaParametro( 'CB_APE_PAT', 0 );
     ValidaParametro( 'CB_APE_MAT', 1 );
     ValidaParametro( 'CB_NOMBRES', 2 );
     if ParamVacio( 3 ) then
        AgregaRequerido( 'COLABORA.CB_FEC_NAC', enEmpleado )
     else
         ParamDate( 3 ); //Se hace la llamada a ParamDate() para validar el Tipo de Dato
     ValidaParametro( 'CB_SEXO', 4 );
     ValidaParametro( 'CB_ENT_NAC', 5 );
end;

procedure TZetaGENERA_CURP.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     inherited;
     TipoRegresa:= resString;
     ValidaParametros;
end;

function TZetaGENERA_CURP.Calculate: TQREvResult;
var
   sPaterno, sMaterno, sNombres, sSexo, sEntidad: string;
   dFechaNac: TDate;
begin
     with DatasetEvaluador do
     begin
          if ParamVacio( 0 ) then
             sPaterno := FieldByName( 'CB_APE_PAT' ).AsString
          else
              sPaterno  := ParamString( 0 );
          if ParamVacio( 1 ) then
             sMaterno  := FieldByName( 'CB_APE_MAT' ).AsString
          else
              sMaterno  := ParamString( 1 );
          if ParamVacio( 2 ) then
             sNombres  := FieldByName( 'CB_NOMBRES' ).AsString
          else
              sNombres  := ParamString( 2 );
          if ParamVacio( 3 ) then
             dFechaNac := FieldByName( 'CB_FEC_NAC' ).AsDateTime
          else
              dFechaNac := ParamDate( 3 );
          if ParamVacio( 4 ) then
             sSexo  := FieldByName( 'CB_SEXO' ).AsString
          else
              sSexo  := ParamString( 4 );
          if ParamVacio( 5 ) then
             sEntidad  := FieldByName( 'CB_ENT_NAC' ).AsString
          else
              sEntidad  := ParamString( 5 );
     end;
     with Result do
     begin
          Kind := resString;
          strResult := ZetaCommonTools.CalcCURP( sPaterno, sMaterno, sNombres, dFechaNac,
                                                 sSexo, GetClaveEstado( sEntidad ) );
     end;
end;

{CV: DIC2002:
Se movieron estas funciones a la unidad ZEvaluador,
porque tambien se requieren en el Modulo de Seleccion de Personal.
En ZEvaluador, estan las clases base, en esta unidad estan las
clases Hijas.
La parte comentada es como estaba anteriormente la funcion.
En ZFuncsSeleccion, se tienen las mismas funciones }
{ ************* TZetaEDAD ************* }

{type
  TZetaEDAD = class( TZetaRegresaFloat )
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaEDAD.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     inherited;
     oZetaProvider.GetDatosActivos;
     AgregaRequerido( 'COLABORA.CB_FEC_NAC', enEmpleado );
end;

function TZetaEDAD.Calculate: TQREvResult;
 var dFecha : TDate;
begin
     with DataSetEvaluador, Result do
     begin
          Kind := resDouble;
          dFecha := FieldByName( 'CB_FEC_NAC' ).AsDateTime;
          if dFecha <= NullDateTime then
             dblResult := NullDateTime
          else dblResult := Years( dFecha,
                                   DefaultDate( 0, oZetaProvider.FechaDefault ) );
     end;
end;
}
type
  TZetaEDAD = class( TZetaBaseEDAD )
  protected
    function GetFecNacField: String;override;
    procedure AgregaDatosRequeridos;override;
  end;

function TZetaEDAD.GetFecNacField: String;
begin
     Result := 'CB_FEC_NAC';
end;

procedure TZetaEDAD.AgregaDatosRequeridos;
begin
     AgregaRequerido( Format( 'COLABORA.%s',[GetFecNacField]), enEmpleado );
end;

{CV: DIC2002:
Se movieron estas funciones a la unidad ZEvaluador,
porque tambien se requieren en el Modulo de Seleccion de Personal.
En ZEvaluador, estan las clases base, en esta unidad estan las
clases Hijas.
En ZFuncsSeleccion, se tienen las mismas funciones }
{ ************* TZetaEDADLETRA ************* }

type
 TZetaEDADLETRA = class( TZetaBaseEDADLETRA )
  public
    function GetFecNacField: String;override;
    procedure AgregaDatosRequeridos;override;
  end;

function TZetaEDADLETRA.GetFecNacField: String;
begin
     Result := 'CB_FEC_NAC';
end;

procedure TZetaEDADLETRA.AgregaDatosRequeridos;
begin
     AgregaRequerido( Format( 'COLABORA.%s', [GetFecNacField] ), enEmpleado );
end;

{ ************* TZetaIMSS ************* }

type
  TZetaIMSS = class( TZetaRegresaQuery )
  public
    function GetScript : String; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  protected
    bYear, bBooleano: Boolean;
    sCampo: String;
    iNumCampo : Integer;
    procedure SetCampo( const iCampo: Integer; var Tipo : TQREvResultType );
  end;

procedure TZetaIMSS.SetCampo( const iCampo: Integer; var Tipo : TQREvResultType );
begin
     bBooleano := FALSE;
     case iCampo of
          1: sCampo := 'DIAS_VA';
	  2: sCampo := 'PRIMAVA';
	  3: sCampo := 'DIAS_AG';
	  4: sCampo := 'FACTOR';
	  5: sCampo := 'PRIMADO';
	  6: begin
                  sCampo := 'PAGO_7';
                  Tipo := resString;
                  bBooleano := TRUE;
             end;
	  7: begin
                  sCampo := 'PRIMA_7';
                  Tipo := resString;
                  bBooleano := TRUE;
             end;
     else
         sCampo := 'DIAS_AD';
     end;
     iNumCampo := iCampo;
end;

function TZetaIMSS.GetScript : string;

{$ifdef MSSQL}
   function SetBooleano( const sFuncion: String ): String;
   begin
        Result := sFuncion;
        if bBooleano then
           Result := 'dbo.SP_BOOL( ' + Result + ' )';
   end;
{$endif}

begin
{$ifdef INTERBASE}
        {$ifdef FLEXIBLES}
            if bYear then
               Result := 'SELECT %s FROM SP_IMSS_YEAR_V( %s, %s, %s )'
            else
               Result := 'SELECT %s FROM SP_IMSS_V( %s, %s , %s, %s )' ;
        {$else}
            if bYear then
               Result := 'SELECT %s FROM SP_IMSS_YEAR( %s, %s )'
            else
               Result := 'SELECT %s FROM SP_IMSS( %s, %s , %s)' ;
        {$endif}

{$endif}
{$ifdef MSSQL}
        {$ifdef FLEXIBLES}
            if bYear then
               Result := 'SELECT %s = ' + SetBooleano( 'DBO.SP_IMSS_YEAR_V( %s, %s, %d, %s )' )
            else
               Result := 'SELECT %s = ' + SetBooleano( 'DBO.SP_IMSS_V( %s, %s , %s, %d, %s )' );
        {$else}
            if bYear then
               Result := 'SELECT %s = ' + SetBooleano( 'DBO.SP_IMSS_YEAR( %s, %s, %d )' )
            else
               Result := 'SELECT %s = ' + SetBooleano( 'DBO.SP_IMSS( %s, %s , %s, %d )' );
        {$endif}
{$endif}
end;

procedure TZetaIMSS.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sReferencia, sInicial, sTabla: String;
begin
     // IMSS( sRegresa [ , FechaRef, [dFechaIni, [sTablaSS ] ] ] )
     TipoRegresa := resDouble;
     oZetaProvider.GetDatosActivos;
     AgregaRequerido( 'COLABORA.CB_FEC_ANT', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_TABLASS', enEmpleado );

     if ParametrosConstantes then
     begin
          SetCampo( DefaultInteger( 0, 1 ), TipoRegresa );
          bYear:= ( ( not ParamVacio( 1 ) ) and ( Argument( 1 ).Kind = resInt ) ) or
                  ( ( not ParamVacio( 1 ) ) and ( Argument( 1 ).Kind = resDouble ) and ( not Argument( 1 ).ResFecha ) );

          if ParamVacio( 3 ) then
             sTabla := 'COLABORA.CB_TABLASS'
          else
          begin
               if ParamEsCampo( 3 ) then
                  sTabla := ParamNombreCampo( 3 )
               else
                   sTabla := EntreComillas( Trim( ParamString(3) ) );
          end;

          if bYear then
             AgregaScript( Format( GetScript, [ sCampo, sTabla, ParamCampoInteger( 1 ){$ifdef MSSQL}, iNumCampo {$endif} {$ifdef FLEXIBLES},'COLABORA.CB_CODIGO'{$endif} ] ),
                           TipoRegresa, EsFecha )
          else
          begin
               if ( ( not ParamVacio( 1 ) ) and ( ParamEsCampo( 1 ) ) ) then
                  sReferencia := ParamNombreCampo( 1 )
               else
                  sReferencia := EntreComillas( DateToStrSQL( DefaultDate( 1, oZetaProvider.FechaDefault ) ) );
               if ( not ParamVacio( 2 ) ) and ParamEsCampo( 2 ) then
                  sInicial := ParamNombreCampo( 2 )
               else
                  sInicial := 'COLABORA.CB_FEC_ANT';
               AgregaScript( Format( GetScript, [ sCampo, sTabla, sInicial, sReferencia {$ifdef MSSQL}, iNumCampo {$endif} {$ifdef FLEXIBLES},'COLABORA.CB_CODIGO'{$endif} ] ),
                             TipoRegresa, EsFecha );
          end;
     end;
end;

function TZetaIMSS.Calculate : TQREvResult;
var
   sReferencia, sInicial, sTabla: String;
   TipoRegresa: TQREvResultType;
begin
     SetCampo( DefaultInteger( 0, 1 ), TipoRegresa );
     bYear:= ( ( not ParamVacio( 1 ) ) and ( Argument( 1 ).Kind = resInt ) ) or
             ( ( not ParamVacio( 1 ) ) and ( Argument( 1 ).Kind = resDouble ) and ( not Argument( 1 ).ResFecha ) );
     with DataSetEvaluador, Result do
     begin
          sTabla:= DefaultString( 3, EntreComillas( FieldByName( 'CB_TABLASS' ).AsString ) );
          if bYear then
             Result:= EvaluaQuery( Format( GetScript, [ sCampo, sTabla, IntToStr( ParamInteger( 1 ) ){$ifdef MSSQL} , iNumCampo {$endif} {$ifdef FLEXIBLES},IntToStr( ZetaEvaluador.GetEmpleado ){$endif} ] ) )
          else
          begin
               sReferencia:= EntreComillas( DateToStrSQL( DefaultDate( 1, oZetaProvider.FechaDefault ) ) );
               sInicial:= EntreComillas( DateToStrSQL( DefaultDate( 2, FieldByName( 'CB_FEC_ANT' ).AsDateTime ) ) );
               Result:= EvaluaQuery( Format( GetScript, [ sCampo, sTabla, sInicial, sReferencia{$ifdef MSSQL} , iNumCampo {$endif} {$ifdef FLEXIBLES},IntToStr( ZetaEvaluador.GetEmpleado ){$endif} ] ) );
          end;
     end;
end;

{ ************* TZetaNOMBRE ************* }

type
  TZetaNOMBRE = class( TZetaFunc )
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaNOMBRE.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resString;
     AgregaRequerido( 'COLABORA.CB_NOMBRES', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_APE_PAT', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_APE_MAT', enEmpleado );
end;

function TZetaNOMBRE.Calculate: TQREvResult;
begin
     with DataSetEvaluador, Result do
     begin
          Kind := resString;
          case DefaultInteger( 0, 1 ) of
               1: strResult := FieldByName( 'CB_NOMBRES' ).AsString + ' ' +
                               FieldByName( 'CB_APE_PAT' ).AsString + ' ' +
                               FieldByName( 'CB_APE_MAT' ).AsString;
          else
              strResult := CalculaPrettyName( FieldByName( 'CB_APE_PAT' ).AsString,
                                              FieldByName( 'CB_APE_MAT' ).AsString,
                                              FieldByName( 'CB_NOMBRES' ).AsString );
          end;
          strResult := Copy( strResult, 1, DefaultInteger( 1, 50 ) );
     end;
end;

{ ************* TZetaTarjetaDecimal ************* }

type
  TZetaTarjetaDecimal = class( TZetaFunc )
  private
    { Private declarations }
    FLongitud: Integer;
    FPrefijo: String;
  public
    { Public declarations }
    constructor Create(oEvaluador:TQrEvaluator); override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

constructor TZetaTarjetaDecimal.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create( oEvaluador );
     with oZetaProvider do
     begin
          InitGlobales;
          FLongitud := GetGlobalInteger( K_GLOBAL_PROXIMITY_TOTAL_BYTES );
          FPrefijo := GetGlobalString( K_GLOBAL_PROXIMITY_FACILITY_CODE );
     end;
end;

procedure TZetaTarjetaDecimal.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resString;
     AgregaRequerido( 'COLABORA.CB_ID_NUM', enEmpleado );
end;

function TZetaTarjetaDecimal.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resString;
          strResult := ZetaServerTools.ProximidadHexToDec( DefaultString( 0, DataSetEvaluador.FieldByName( 'CB_ID_NUM' ).AsString ),
                                                           DefaultString( 1, FPrefijo ),
                                                           DefaultInteger( 2, FLongitud ) );
     end;
end;

{ ************* TZetaV_FUTURO ************* }

type
  TZetaV_FUTURO = class( TZetaFunc )
  private
  public
   constructor Create(oEvaluador:TQrEvaluator); override;
   destructor Destroy; override;
   procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
   function Calculate: TQREvResult; override;
  end;

constructor TZetaV_FUTURO.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create(oEvaluador);
     ZetaEvaluador.oZetaCreator.PreparaPrestaciones;
end;

destructor TZetaV_FUTURO.Destroy;
begin
     inherited Destroy;
end;

procedure TZetaV_FUTURO.GetRequeridos( var TipoRegresa: TQREvResultType );
begin
     TipoRegresa := resDouble;
     oZetaProvider.GetDatosActivos;
     AgregaRequerido( 'COLABORA.CB_DER_FEC', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_DER_PAG', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_V_PAGO', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_DER_GOZ', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_V_GOZO', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_FEC_ANT', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_TABLASS', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_DER_PV', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_V_PRIMA', enEmpleado );
end;

function TZetaV_FUTURO.Calculate: TQREvResult;
var
   iRegresa: Integer;
   dReferencia, dAntig: TDate;
   sTabla: String;
   rProp, rDiasTabla, rDiasBase, rPrima  : Currency;
begin
     iRegresa := DefaultInteger( 0, 1 );
     dReferencia := DefaultDate( 1, oZetaProvider.FechaDefault );
     with DatasetEvaluador do
     begin
          dAntig := DefaultDate( 2, FieldByName( 'CB_FEC_ANT' ).AsDateTime );
          sTabla := DefaultString( 3, FieldByName( 'CB_TABLASS' ).AsString );
          if ( dReferencia >= dAntig ) then
              rProp := ZetaEvaluador.oZetaCreator.dmPrestaciones.VacaProporcional( dAntig, FieldByName( 'CB_DER_FEC' ).AsDateTime, dReferencia,
                                                                                  sTabla, rDiasTabla, rDiasBase, rPrima {$ifdef FLEXIBLES}, ZetaEvaluador.GetEmpleado{$endif} )

          else
          begin
               rProp := 0;
               rPrima := 0;
          end;
          with Result do
          begin
               Kind := resDouble;
	       case iRegresa of
                    1: dblResult := FieldByName( 'CB_DER_PAG' ).AsFloat + rProp - FieldByName( 'CB_V_PAGO' ).AsFloat;	{ Saldo de Pago }
	            2: dblResult := FieldByName( 'CB_DER_GOZ' ).AsFloat + rProp - FieldByName( 'CB_V_GOZO' ).AsFloat;	{ Saldo de Gozo }
	            3: dblResult := FieldByName( 'CB_DER_PAG' ).AsFloat + rProp;			                { Derecho de Pago }
	            4: dblResult := FieldByName( 'CB_DER_GOZ' ).AsFloat + rProp;                                        { Derecho de Gozo }
                    6: dblResult := FieldByName( 'CB_DER_PV' ).AsFloat + rPrima - FieldByName( 'CB_V_PRIMA' ).AsFloat;  { Saldo de d�as de prima  }
                    7: dblResult := FieldByName( 'CB_DER_PV' ).AsFloat + rPrima;                                        { Derecho de d�as de prima }
                    8: dblResult := rPrima;                                                                             { Saldo de prima despu�s de cierre }
               else
                    dblResult := rProp;	{ Despu�s del Cierre }
               end;
          end;
     end;
end;

{ ************* TZetaV_DERECHO ************* }

type
  TZetaV_DERECHO = class( TZetaRegresaQuery )
  private
  public
    function GetScript: string; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
  end;

function TZetaV_DERECHO.GetScript: String;
begin
     {$IFDEF INTERBASE}
        {$ifdef FLEXIBLES}
        Result := 'Select DIAS_VA from SP_IMSS_V( %s, %s, %s, %s )';
        {$else}
        Result := 'Select DIAS_VA from SP_IMSS( %s, %s, %s )';
        {$endif}
     {$ENDIF}
     {$IFDEF MSSQL}
        {$ifdef FLEXIBLES}
        Result := 'Select DIAS_VA = DBO.SP_IMSS_V( %s, %s, %s, 1, %s )';
        {$else}
        Result := 'Select DIAS_VA = DBO.SP_IMSS( %s, %s, %s, 1 )';
        {$endif}
     {$ENDIF}
end;


procedure TZetaV_DERECHO.GetRequeridos( var TipoRegresa: TQREvResultType );
begin
     TipoRegresa := resDouble;
     AgregaRequerido( 'COLABORA.CB_TABLASS', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_FEC_ANT', enEmpleado );
end;

function TZetaV_DERECHO.Calculate: TQREvResult;
var
   iYear, iDiasAniv, iDiasYear, iDiasViejo, iDiasNuevo: Integer;
   dAntig, dFinal, dAniv: TDate;
   sAntig, sInicial, sFinal, sTabla: String;

function CalculaDias( const sTabla, sInicial, sFinal: String ): TQREvResult;
begin
    //Result:= EvaluaQuery( Format( GetScript, [ sTabla, sInicial, sFinal ] ));
    Result:= EvaluaQuery( Format( GetScript, [ EntreComillas( sTabla ), sInicial, sFinal {$ifdef FLEXIBLES},IntToStr(ZetaEvaluador.GetEmpleado){$endif} ] ));
end;

begin
     { Par�metros }
     iYear := DefaultInteger( 0, TheYear( Date ) );
     with DatasetEvaluador do
     begin
          sAntig := DateToStrSql( DefaultDate( 1, FieldByName( 'CB_FEC_ANT' ).AsDateTime ));
          sTabla := DefaultString( 2, FieldByName( 'CB_TABLASS' ).AsString );
          dAntig := DefaultDate( 1, FieldByName( 'CB_FEC_ANT' ).AsDateTime )
     end;
     { C�lculos de variables locales }
     sInicial := DateToStrSql( FirstDayOfYear( iYear ));         { 01-Ene-iYear }
     sFinal := DateToStrSql( LastDayOfYear( iYear ));            { 31-Dic-iYear }
     dFinal :=  LastDayOfYear( iYear );
     dAniv := DateTheYear(dAntig, iYear );
     iDiasAniv := Trunc( dAniv ) - Trunc( StrToDate(sInicial ));
     iDiasYear := ( Trunc( dFinal ) - Trunc( StrToDate( sInicial )) ) + 1;
     { 2 Querys }
     Result := CalculaDias( sTabla, EntreComillas( sAntig) , EntreComillas(sInicial ));
     if ( Result.Kind = resInt ) then
         iDiasViejo := Result.intResult
     else
         iDiasViejo := Trunc( Result.dblResult );
     Result := CalculaDias( sTabla, EntreComillas( sAntig ), EntreComillas( sFinal ));
     if ( Result.Kind = resInt ) then
        iDiasNuevo := Result.intResult
     else
        iDiasNuevo := Trunc( Result.dblResult );
     with Result do
     begin
          Kind := resDouble;
          dblResult := ( iDiasAniv / iDiasYear )* iDiasViejo + ( ( iDiasYear - iDiasAniv ) / iDiasYear ) * iDiasNuevo;
          resFecha := FALSE;
     end;
end;

{ ************* TZetaDATOSVACA ************* }

type
  TZetaDATOSVACA = class( TZetaV_FUTURO )
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaDATOSVACA.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     inherited;
     AgregaRequerido( 'COLABORA.CB_FEC_BAJ', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_ACTIVO', enEmpleado );
end;

function TZetaDATOSVACA.Calculate: TQREvResult;
var
   iRegresa, nYearsAntig : Integer;
   dFecha, dAntig, dUltimoCierre : TDate;
   sTabla : String;
   rProp, rDiasTabla, rDiasBase, rPrima : Currency;
begin
     iRegresa:= DefaultInteger( 0, 1 );
     with DataSetEvaluador do
     begin
          dAntig := FieldByName( 'CB_FEC_ANT' ).AsDateTime;
          sTabla := FieldByName( 'CB_TABLASS' ).AsString;
          dUltimoCierre := DefaultDate( 2, FieldByName( 'CB_DER_FEC' ).AsDateTime );
          {if ( zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString ) ) then
             dFecha:= DefaultDate( 1, oZetaProvider.FechaDefault )
          else
             dFecha:= FieldByName( 'CB_FEC_BAJ' ).AsDateTime;}
           dFecha := DefaultDate( 1, oZetaProvider.FechaDefault );
          if ( not ( zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString ) ) ) and    // Si es Baja y
             ( dFecha > FieldByName( 'CB_FEC_BAJ' ).AsDateTime ) then            // fecha solicitada es mayor a la baja
               dFecha := FieldByName( 'CB_FEC_BAJ' ).AsDateTime;                     // Se regresar� el saldo a la baja
     end;

     with Result do
     begin
          Kind := resDouble;

          if ( ( iRegresa >= 1 ) and ( iRegresa <= 3 ) ) or ( ( iRegresa >= 8 ) and ( iRegresa <=9 ) ) then
          begin
               with DataSetEvaluador do
               begin
                    if ( dFecha >= dAntig ) then
                       rProp := ZetaEvaluador.oZetaCreator.dmPrestaciones.VacaProporcional( dAntig,
                                dUltimoCierre, dFecha, sTabla, rDiasTabla, rDiasBase, rPrima{$ifdef FLEXIBLES}, ZetaEvaluador.GetEmpleado{$endif} )
                    else
                    begin
                         rProp := 0;
                         rPrima := 0;
                    end;

                    case iRegresa of
                         1: dblResult :=  rProp;
                         2: dblResult :=  FieldByName( 'CB_DER_PAG' ).AsFloat + rProp - FieldByName( 'CB_V_PAGO' ).AsFloat;	{ Saldo de Pago }
                         3: dblResult :=  FieldByName( 'CB_DER_GOZ' ).AsFloat + rProp - FieldByName( 'CB_V_GOZO' ).AsFloat;	{ Saldo de Gozo }
                         8: dblResult :=  rPrima;                                                                               { Saldo de derecho de prima vacacional }
                         9: dblResult :=  FieldByName( 'CB_DER_PV' ).AsFloat + rPrima - FieldByName( 'CB_V_PRIMA' ).AsFloat;	{ Saldo de prima vacacional }
                    end;
               end;
          end
          else
          begin
               nYearsAntig := Trunc( rMax( Years( dAntig, dFecha ), 0 ) ) + 1;
               with ZetaEvaluador.oZetaCreator.dmPrestaciones.GetRenglonPresta( sTabla, nYearsAntig{$ifdef FLEXIBLES}, ZetaEvaluador.GetEmpleado{$endif} ) do
               begin
                    case iRegresa of
                         4: dblResult := FieldByName( 'PT_FACTOR' ).AsFloat;
                         5: dblResult := FieldByName( 'PT_DIAS_AG' ).AsFloat;
                         6: dblResult := FieldByName( 'PT_DIAS_VA' ).AsFloat;
                         7: dblResult := FieldByName( 'PT_PRIMAVA' ).AsFloat;
                    end;
               end;
          end;
     end;
end;

{ ************* TZetaV_SALDO ************* }

type
  TZetaV_SALDO = class( TZetaRegresaQuery )
  private
    FRegresa : Integer;
    function RegresaDerechoVaca: Boolean;
    function GetNombreCampo: String;
    procedure ValidaTipoFecha( const iParam: Integer );
  public
    constructor Create(oEvaluador:TQrEvaluator); override;
    function GetScript : String; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

constructor TZetaV_SALDO.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create(oEvaluador);
     ZetaEvaluador.oZetaCreator.PreparaPrestaciones;    // No hace falta destruirlo, de eso se encarga el oZetaCreator
end;

function TZetaV_SALDO.RegresaDerechoVaca: Boolean;
begin
     Result := ( FRegresa in [ 1, 2, 5 ] );
end;

function TZetaV_SALDO.GetNombreCampo: String;
begin
     case FRegresa of
          1 : Result := 'VA_D_PAGO';
          2 : Result := 'VA_D_GOZO';
          3 : Result := 'SUM( VA_PAGO )';
          4 : Result := 'SUM( VA_GOZO )';
          5 : Result := 'VA_D_PRIMA';
          6 : Result := 'SUM( VA_P_PRIMA)';
     end;
end;

function TZetaV_SALDO.GetScript: String;
begin
     if RegresaDerechoVaca then                       // Este query solo se manda ejecutar en el Calculate ( AbreQuery ) por eso es v�lido poner Alias a las columnas
        Result := 'select MAX( VA_FEC_INI ) as VA_FEC_INI, SUM( VA_D_GOZO ) as VA_D_GOZO, ' +
                  'SUM( VA_D_PAGO ) as VA_D_PAGO, ' +
                  'SUM( VA_D_PRIMA ) as VA_D_PRIMA ' +
                  'from VACACION where ( CB_CODIGO = %d ) and ( VA_TIPO = %d ) and '+
                  '( VA_FEC_INI >= %s ) and ( VA_FEC_INI <= %s )'
     else
         Result := 'select %s from VACACION where ( CB_CODIGO = %s ) and ( VA_TIPO = %d ) and ' +
                   '( VA_FEC_INI >= %s ) and ( VA_FEC_INI <= %s )';
end;

procedure TZetaV_SALDO.ValidaTipoFecha( const iParam: Integer );
begin
     if ( not ParamVacio( iParam ) ) then
        ParamDate( iParam );
end;

procedure TZetaV_SALDO.GetRequeridos( var TipoRegresa: TQREvResultType );
begin
     TipoRegresa := resDouble;
     oZetaProvider.GetDatosActivos;
     FRegresa := DefaultInteger( 0, 1 );
     ValidaTipoFecha(1);
     ValidaTipoFecha(2);
     AgregaRequerido( 'COLABORA.CB_FEC_ANT', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_TABLASS', enEmpleado );

     if ParametrosConstantes and ( not RegresaDerechoVaca ) then  // Puede Resolverse agregando el Script
     begin
          AgregaScript( Format( GetScript, [ GetNombreCampo, 'COLABORA.CB_CODIGO', Ord( tvVacaciones ),
                                             ParamCampoDefFecha( 2, 'COLABORA.CB_FEC_ANT' ),
                                             ParamCampoDefFecha( 1, DateToStrSQLC( oZetaProvider.FechaDefault ) ) ] ),
                                             TipoRegresa, FALSE );
     end;
end;

function TZetaV_SALDO.Calculate: TQREvResult;
var
   dReferencia, dAntig, dUltimoCierre : TDate;
   sTabla : String;
   iEmpleado : Integer;
   rProp, rDiasTabla, rDiasBase, rPrima : TDiasHoras;    // Las requiere la funci�n CalculaProporcional
begin
     with Result do
     begin
          Kind := resDouble;
          dblResult := 0.0;
     end;
     FRegresa := DefaultInteger( 0, 1 );
     dReferencia := DefaultDate( 1, oZetaProvider.FechaDefault );
     iEmpleado := ZetaEvaluador.GetEmpleado;
     with DatasetEvaluador do
     begin
          dAntig := DefaultDate( 2, FieldByName( 'CB_FEC_ANT' ).AsDateTime );
          sTabla := DefaultString( 3, FieldByName( 'CB_TABLASS' ).AsString );
     end;
     if ( dReferencia >= dAntig ) then       // Si la referencia es anterior a la antiguedad se regresa 0.0
     begin
          if RegresaDerechoVaca then
          begin
               AbreQuery( Format( GetScript, [ iEmpleado, Ord( tvCierre ),
                                               DateToStrSQLC( dAntig ),
                                               DateToStrSQLC( dReferencia ) ] ) );
               with FQuery do
               begin
                    if ( not EOF ) then
                    begin
                         dUltimoCierre := FieldByName( 'VA_FEC_INI' ).AsDateTime;
                         Result.dblResult := FieldByName( GetNombreCampo ).AsFloat;
                    end
                    else
                        dUltimoCierre := NullDateTime;
               end;
               with Result do
               begin
                    rProp := ZetaEvaluador.oZetaCreator.dmPrestaciones.VacaProporcional( dAntig, dUltimoCierre, dReferencia, sTabla, rDiasTabla, rDiasBase,rPrima{$ifdef FLEXIBLES}, iEmpleado{$endif} );
                    case FRegresa of
                         1,2:      dblResult := dblResult + rProp;
                         5:        dblResult := dblResult + rPrima;
                    end;
               end;
          end
          else
          begin
               Result := EvaluaQuery( Format( GetScript, [ CampoRes( GetNombreCampo ),
                                                           IntToStr( iEmpleado ),
                                                           Ord( tvVacaciones ),
                                                           DateToStrSQLC( dAntig ),
                                                           DateToStrSQLC( dReferencia ) ] ) );
          end;
     end;
end;

{ ************* TZetaPuesto ************* }

type
  TZetaPUESTO = class( TZetaRegresa )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaPUESTO.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resString;
//     EquivaleScript( 'CB_PUESTO', enNinguno );
     AgregaScript( 'COLABORA.CB_PUESTO', TipoRegresa, False );
     AgregaRequerido( 'COLABORA.CB_PUESTO', enEmpleado );
end;

{ ************* TZetaTPUESTO ************* }
type
  TZetaTPUESTO = class( TZetaRegresa )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaTPUESTO.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   FCampoRequiere : string;
begin
     TipoRegresa := resString;
     case DefaultInteger( 0, 1 ) of
          1 : FCampoRequiere := 'PU_DESCRIP';
          2 : begin
                   FCampoRequiere := 'PU_NUMERO';
                   TipoRegresa := resDouble;
              end;
          3 : FCampoRequiere := 'PU_TEXTO';
          4 : FCampoRequiere := 'PU_INGLES';
          5 : FCampoRequiere := 'PU_CLASIFI';
          6 : FCampoRequiere := 'PU_DETALLE';
     else
         ErrorFuncion( 'Error en Par�metro', 'Solo se Permiten Valores 1 a 6' );
     end;
     EquivaleScript( 'PUESTO.' + FCampoRequiere, enPuesto );
end;


{ ************* TZetaRegresaNIVEL ************* }
type
  TZetaRegresaNIVEL = class( TZetaRegresa )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaRegresaNIVEL.GetRequeridos( var TipoRegresa : TQREvResultType );
var
    sScript : String;
    eTipoEntidad : TipoEntidad;
begin
     ParametrosFijos( 1 );
     TipoRegresa := resString;
     {$ifdef ACS}
     if ( ParamInteger( 0 ) < 1 ) or ( ParamInteger( 0 ) > 12 ) then     //MV:(23/Oct/2008) Cambio Niveles ACS
        ErrorFuncion( 'Error en Par�metro', 'Solo se Permiten Valores 1 a 12' );
     {$else}
     if ( ParamInteger( 0 ) < 1 ) or ( ParamInteger( 0 ) > 9 ) then     
        ErrorFuncion( 'Error en Par�metro', 'Solo se Permiten Valores 1 a 9' );
     {$endif}
     case ZetaEvaluador.Entidad of
        enKardex, enKarFija : eTipoEntidad := enKardex;
        enNomina, enFaltas, enMovimien : eTipoEntidad := enNomina;
        enKarCurso : eTipoEntidad := enKarCurso;
        enAusencia, enChecadas, enWorks : eTipoEntidad := enAusencia;
        else eTipoEntidad := enEmpleado;
     end;

     sScript := ZetaEvaluador.oZetaCreator.dmEntidadesTress.NombreEntidad( eTipoEntidad ) + '.CB_NIVEL' + IntToStr( ParamInteger( 0 ) );
     EquivaleScript( sScript, eTipoEntidad );
end;

type
  TZetaEDOCIVIL = class( TZetaRegresa )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaEDOCIVIL.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resString;
     EquivaleScript( 'EDOCIVIL.TB_ELEMENT', enEdoCivil );
end;

{ ************** TZetaCODCLASIFI **************** }

type
  TZetaCODCLASIFI = class( TZetaRegresaQuery )
  public
    function Calculate: TQREvResult; override;
    function GetScript: String; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaCODCLASIFI.GetScript : String;
begin
     Result:= 'select PUESTO.PU_CLASIFI from PUESTO ' +
              'where ( PUESTO.PU_CODIGO = %s )';
end;

procedure TZetaCODCLASIFI.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos(1);
     TipoRegresa:= resString;
     if not ParametrosConstantes then Exit;
     AgregaScript( Format( GetScript, [ ParamCampoString(0) ] ), TipoRegresa, False );
end;

function TZetaCODCLASIFI.Calculate: TQrEvResult;
begin
     Result:= EvaluaQuery( Format( GetScript, [ EntreComillas(ParamString(0)) ] ));
end;

{ ************** TZetaCURSO **************** }

type
  TZetaCURSO = class( TZetaRegresaQuery )
  private
    FCampo: String;
  public
    function Calculate: TQREvResult; override;
    function GetScript: String; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    procedure CaseCampo( const iRegresa : Integer; var sCampo: String; var Tipo : TQREvResultType; var lEsFecha : Boolean );
  end;

procedure TZetaCURSO.CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQREvResultType; var lEsFecha : Boolean );
begin
     Tipo := resString;
     case iRegresa of
          1: sCampo := 'CU_NOMBRE';
          2: sCampo := 'CU_INGLES';
     {$ifdef INTERBASE}
          3: sCampo := 'CU_TEXTO1';
          4: sCampo := 'CU_TEXTO2';
     {$endif}
     {$ifdef MSSQL}
          3: sCampo := Format( 'substring( CU_TEXTO1, 1, %d )', [ K_MAX_VARCHAR ] );
          4: sCampo := Format( 'substring( CU_TEXTO2, 1, %d )', [ K_MAX_VARCHAR ] );
     {$endif}
          5:
          begin
               sCampo := 'CU_HORAS';
               Tipo:= resDouble;
          end;
          6: sCampo := 'CU_CLASIFI';
          7: sCampo := 'MA_CODIGO';
          8: sCampo := 'CU_ACTIVO';
          9:
          begin
               sCampo := 'CU_COSTO1';
               Tipo:= resDouble;
          end;
          10:
          begin
               sCampo := 'CU_COSTO2';
               Tipo:= resDouble;
          end;
          11:
          begin
               sCampo := 'CU_COSTO3';
               Tipo:= resDouble;
          end;
          12:
          begin
               sCampo := 'CU_NUMERO';
               Tipo:= resDouble;
          end;
          13: sCampo := 'CU_TEXTO';
          14: sCampo := 'CU_CLASE';
          15: sCampo := 'CU_REVISIO';
          16:
          begin
               Tipo := resDouble;
               lEsFecha := TRUE;
               sCampo := 'CU_FEC_REV';
          end;
          17:
          begin
               Tipo := resBool;
               sCampo := 'CU_STPS';
          end;
          18: sCampo := 'CU_FOLIO';
          19: sCampo := 'CU_DOCUM';
          20: sCampo := 'AT_CODIGO';
          21: sCampo := 'CU_OBJETIV';
          22: sCampo := 'CU_MODALID';

     end;
end;

function TZetaCURSO.GetScript: String;
begin
     Result:= 'select %s from CURSO ' +
              'where ( CURSO.CU_CODIGO = %s )';
end;

procedure TZetaCURSO.GetRequeridos( var TipoRegresa : TQREvResultType );
const
     sCurso = 'KARCURSO.CU_CODIGO';
var
   lEsFecha : Boolean;
begin
     ParametrosFijos(1);
     TipoRegresa := resString;
     if ParametrosConstantes then
     begin
          CaseCampo( ParamInteger(0), FCampo, TipoRegresa, lEsFecha );
          EsFecha:= lEsFecha;
          AgregaScript( Format( GetScript, [ FCampo , UpperCase( ParamCampoDefString(1, sCurso ) ) ] ), TipoRegresa, lEsFecha );
     end
     else
          AgregaRequerido( sCurso, enKarCurso );
end;

function TZetaCURSO.Calculate: TQREvResult;
var
   TipoRegresa: TQREvResultType;
   sCodigo : String;
   lEsFecha : Boolean;
begin
     CaseCampo( ParamInteger(0), FCampo, TipoRegresa, lEsFecha );
     EsFecha := lEsFecha;
     if ParamVacio(1) then
        sCodigo := DataSetEvaluador.FieldByName( 'CU_CODIGO' ).AsString
     else
        sCodigo := UpperCase( ParamString(1) );
     Result:= EvaluaQuery( Format( GetScript, [ FCampo, EntreComillas( sCodigo ) ] ) );
end;

{ ************** TZetaCUR_ASIS ***************** }
type
  TZetaCURASIS = class( TZetaRegresaQuery )
  private
    FCampo: String;
    FEsReferencia : Boolean;
  public
    function Calculate: TQREvResult; override;
    function GetScript: String; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    procedure CaseCampo( const iRegresa : Integer; var sCampo: String; var Tipo : TQREvResultType );
 end;

procedure TZetaCURASIS.CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQREvResultType);
begin
     Tipo := resDouble;
     case iRegresa of
          1:
   	      begin
               sCampo := 'CS_ASISTIO';
               Tipo:= resString;
          end;
          2: sCampo := 'CS_EVA_1';
          3: sCampo := 'CS_EVA_2';
          4: sCampo := 'CS_EVA_3';
          5: sCampo := 'CS_EVA_FIN';
          6:
          begin
               sCampo := 'CS_COMENTA';
               Tipo:= resString;
          end;
     end;
end;

function TZetaCURASIS.GetScript: String;
begin
     if FEsReferencia then
        Result:= 'select %0:s from CUR_ASIS ' +
                 'left outer join RESERVA on ( RESERVA.RV_FOLIO = CUR_ASIS.RV_FOLIO ) '+
                 'where ( RESERVA.RV_ORDEN = %1:s ) and ( CUR_ASIS.SE_FOLIO = %2:s ) and ( CUR_ASIS.CB_CODIGO = %3:s )'
     else
        Result:= 'select %0:s from CUR_ASIS ' +
{$ifdef INTERBASE}
                 'where ( CUR_ASIS.RV_FOLIO = ( select RESULTADO from SP_RESERVA_FOLIO( %2:s, %1:s ) ) ) and (CUR_ASIS.SE_FOLIO = %2:s ) and ( CUR_ASIS.CB_CODIGO = %3:s )';
{$endif}
{$ifdef MSSQL}
                 'where ( CUR_ASIS.RV_FOLIO = dbo.SP_RESERVA_FOLIO( %2:s, %1:s ) ) and (CUR_ASIS.SE_FOLIO = %2:s ) and ( CUR_ASIS.CB_CODIGO = %3:s )';
{$endif}
end;

procedure TZetaCURASIS.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sEmpleado, sSesion, sFiltroReserva : String;
begin
     ParametrosFijos(2);
     if ParametrosConstantes then
     begin
          FEsReferencia := ParamEsCampo( 1 ) or ( Argument( 1 ).Kind = resString );
          if FEsReferencia then
             sFiltroReserva := ParamCampoString( 1 )
          else
             sFiltroReserva := IntToStr( ParamInteger( 1 ) );

          if ParamVacio(2) then
          begin
               sSesion := 'SESION.SE_FOLIO';
               AgregaRequerido( sSesion, enSesion );
          end
          else
              sSesion  := ParamCampoInteger(2);

          if ParamVacio(3) then
          begin
               sEmpleado := 'COLABORA.CB_CODIGO';
               AgregaRequerido( sEmpleado, enEmpleado );
          end
          else
              sEmpleado := ParamCampoInteger(3);

          CaseCampo( ParamInteger(0), FCampo, TipoRegresa );

          AgregaScript( Format( GetScript, [ FCampo, sFiltroReserva, sSesion, sEmpleado ] ), TipoRegresa, False );
     end;
end;

function TZetaCURASIS.Calculate: TQREvResult;
var
   TipoRegresa: TQREvResultType;
   iEmpleado, iSesion: Integer;
   sFiltroReserva: String;
begin
     if Argument(1).Kind = resInt then
        sFiltroReserva := IntToStr( ParamInteger( 1 ) )
     else
        sFiltroReserva := EntreComillas( ParamString(1) );

     if ParamVacio(2) then
        iSesion := DataSetEvaluador.FieldByName( 'SE_FOLIO' ).AsInteger
     else
         iSesion := ParamInteger(2);

     if ParamVacio(3) then
        iEmpleado := ZetaEvaluador.GetEmpleado
     else
         iEmpleado:= ParamInteger(3);

     CaseCampo( ParamInteger(0), FCampo, TipoRegresa);

     Result:= EvaluaQuery( Format( GetScript, [ FCampo, sFiltroReserva, IntToStr( iSesion ), IntToStr( iEmpleado ) ] ) );
end;

{ ************** TZetaRESERVA ***************** }
type
  TZetaRESERVA = class( TZetaRegresaQuery )
  private
    FCampo: String;
    FEsReferencia : Boolean;
  public
    function Calculate: TQREvResult; override;
    function GetScript: String; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    procedure CaseCampo( const iRegresa : Integer; var sCampo: String; var Tipo : TQREvResultType; var lEsFecha : Boolean );
 end;

procedure TZetaRESERVA.CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQREvResultType; var lEsFecha: Boolean );
begin
     Tipo := resString;
     lEsFecha := FALSE;
     case iRegresa of
          1: sCampo := 'RV_ORDEN';
          2: sCampo := 'AL_CODIGO';
          3: sCampo := 'MA_CODIGO';
          4:
          begin
               Tipo := resDouble;
               lEsFecha := TRUE;
               sCampo := 'RV_FEC_INI';
          end;
          5:
          begin
               Tipo := resDouble;
               lEsFecha := TRUE;
               sCampo := 'RV_FEC_FIN';
          end;
          6: sCampo := 'RV_HOR_INI';
          7: sCampo := 'RV_HOR_FIN';
          8: sCampo := 'RV_RESUMEN';
          9: sCampo := 'RV_DETALLE';
          10:
          begin
               Tipo:= resInt;
               sCampo := 'US_CODIGO';
          end;
          11:
          begin
               Tipo := resDouble;
               lEsFecha := TRUE;
               sCampo := 'RV_FEC_RES';
          end;
          12: sCampo := 'RV_HOR_RES';
          13:
          begin
               Tipo:= resInt;
               sCampo := 'RV_TIPO';
          end;
          14:
          begin
               Tipo:= resInt;
               sCampo := 'RV_FOLIO';
          end;
     end;
end;

function TZetaRESERVA.GetScript: String;
begin
     if FEsReferencia then
        Result:= 'select %0:s from RESERVA ' +
                 'where ( RESERVA.RV_ORDEN = %1:s ) and ( RESERVA.SE_FOLIO = %2:s )'
     else
        Result:= 'select %0:s from RESERVA ' +
{$ifdef INTERBASE}
                 'where ( RESERVA.RV_FOLIO = ( select RESULTADO from SP_RESERVA_FOLIO( %2:s, %1:s ) ) )';
{$endif}
{$ifdef MSSQL}
                 'where ( RESERVA.RV_FOLIO = dbo.SP_RESERVA_FOLIO( %2:s, %1:s ) )';
{$endif}
end;

procedure TZetaRESERVA.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sSesion, sFiltroReserva : String;
   lEsFecha : Boolean;
begin
     ParametrosFijos(2);
     if ParametrosConstantes then
     begin
          FEsReferencia := ParamEsCampo( 1 ) or ( Argument( 1 ).Kind = resString );
          if FEsReferencia then
             sFiltroReserva := ParamCampoString( 1 )
          else
             sFiltroReserva := IntToStr( ParamInteger( 1 ) );

          if ParamVacio(2) then
          begin
               sSesion := 'SESION.SE_FOLIO';
               AgregaRequerido( sSesion, enSesion );
          end
          else
              sSesion  := ParamCampoInteger(2);

          CaseCampo( ParamInteger(0), FCampo, TipoRegresa, lEsFecha );
          EsFecha:= lEsFecha;

          AgregaScript( Format( GetScript, [ FCampo, sFiltroReserva, sSesion ] ), TipoRegresa, lEsFecha );
     end;
end;

function TZetaRESERVA.Calculate: TQREvResult;
var
   TipoRegresa: TQREvResultType;
   iSesion: Integer;
   sFiltroReserva: String;
   lEsFecha : Boolean;
begin
     if Argument(1).Kind = resInt then
        sFiltroReserva := IntToStr( ParamInteger( 1 ) )
     else
        sFiltroReserva := EntreComillas( ParamString(1) );

     if ParamVacio(2) then
        iSesion := DataSetEvaluador.FieldByName( 'SE_FOLIO' ).AsInteger
     else
         iSesion := ParamInteger(2);

     CaseCampo( ParamInteger(0), FCampo, TipoRegresa, lEsFecha );
     EsFecha:= lEsFecha;

     Result:= EvaluaQuery( Format( GetScript, [ FCampo, sFiltroReserva, IntToStr( iSesion ) ] ) );
end;

{ ************** TZetaDOMICILIO **************** }

type
  TZetaDOMICILIO = class( TZetaFunc )
  public
    { Public Declarations }
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaDOMICILIO.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa:= resString;
     if not ParametrosConstantes or ParamEsCampo(0) then
     begin
          AgregaRequerido( 'COLABORA.CB_CALLE', enEmpleado );
          AgregaRequerido( 'COLABORA.CB_COLONIA', enEmpleado );
          AgregaRequerido( 'COLABORA.CB_CIUDAD', enEmpleado );
          AgregaRequerido( 'ENTIDAD.TB_ELEMENT', enEntidad );
          AgregaRequerido( 'COLABORA.CB_CODPOST', enEmpleado );
          AgregaRequerido( 'COLABORA.CB_TEL', enEmpleado );
          AgregaRequerido( 'COLABORA.CB_ZONA', enEmpleado );
          AgregaRequerido( 'COLABORA.CB_NUM_EXT', enEmpleado );
          AgregaRequerido( 'COLABORA.CB_NUM_INT', enEmpleado );
          ZetaEvaluador.AgregaRequeridoAlias( 'MUNICIPIO.TB_ELEMENT', 'MUNICIPTXT', tgTexto, enMunicipio );
          Exit;
     end;
     case DefaultInteger(0,1) of
          1: AgregaRequerido( 'COLABORA.CB_CALLE', enEmpleado );
          2: AgregaRequerido( 'COLABORA.CB_COLONIA', enEmpleado );
          3: AgregaRequerido( 'COLABORA.CB_CIUDAD', enEmpleado );
          4: AgregaRequerido( 'ENTIDAD.TB_ELEMENT', enEntidad );
          5: AgregaRequerido( 'COLABORA.CB_CODPOST', enEmpleado );
          6: AgregaRequerido( 'COLABORA.CB_TEL', enEmpleado );
          7: AgregaRequerido( 'COLABORA.CB_ZONA', enEmpleado );
          8: AgregaRequerido( 'COLABORA.CB_NUM_EXT', enEmpleado );
          9: AgregaRequerido( 'COLABORA.CB_NUM_INT', enEmpleado );
          10: AgregaRequerido( 'COLABORA.CB_MUNICIP', enEmpleado );
          12: begin
                   AgregaRequerido( 'COLABORA.CB_CALLE', enEmpleado );
                   AgregaRequerido( 'COLABORA.CB_COLONIA', enEmpleado );
                   AgregaRequerido( 'COLABORA.CB_NUM_EXT', enEmpleado );
                   AgregaRequerido( 'COLABORA.CB_NUM_INT', enEmpleado );
              end;
          34: begin
                   AgregaRequerido( 'COLABORA.CB_CIUDAD', enEmpleado );
                   AgregaRequerido( 'ENTIDAD.TB_ELEMENT', enEntidad );
                   ZetaEvaluador.AgregaRequeridoAlias( 'MUNICIPIO.TB_ELEMENT', 'MUNICIPTXT', tgTexto, enMunicipio );
              end;
     end;
end;

function TZetaDOMICILIO.Calculate: TQREvResult;
begin
     with DataSetEvaluador, Result do
     begin
          Kind := resString;
          case DefaultInteger( 0, 1 ) of
               1: strResult := FieldByName( 'CB_CALLE' ).AsString;
               2: strResult := FieldByName( 'CB_COLONIA' ).AsString;
               3: strResult := FieldByName( 'CB_CIUDAD' ).AsString;
               4: strResult := FieldByName( 'TB_ELEMENT' ).AsString;
               5: strResult := FieldByName( 'CB_CODPOST' ).AsString;
               6: strResult := FieldByName( 'CB_TEL' ).AsString;
               7: strResult := FieldByName( 'CB_ZONA' ).AsString;
               8: strResult := FieldByName( 'CB_NUM_EXT' ).AsString;
               9: strResult := FieldByName( 'CB_NUM_INT' ).AsString;
               10: strResult := FieldByName( 'CB_MUNICIP' ).AsString;
               12: strResult := Trim( FieldByName( 'CB_CALLE' ).AsString ) + ', ' +
                                Trim( FieldByName( 'CB_COLONIA' ).AsString )+ ', ' +
                                Trim( FieldByName( 'CB_NUM_EXT' ).AsString )+ ', ' +
                                Trim( FieldByName( 'CB_NUM_INT' ).AsString );
               34: strResult := Trim( FieldByName( 'CB_CIUDAD' ).AsString ) + ', ' +
                                Trim(FieldByName( 'TB_ELEMENT' ).AsString )+ ', ' +
                                Trim(FieldByName( 'MUNICIPTXT' ).AsString ) ;
          end;
     end;
end;

{ ************** TZetaFACTORSDI **************** }

type
  TZetaFACTORSDI = class( TZetaRegresaQuery )
  public
    function Calculate: TQREvResult; override;
    function GetScript: String; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaFACTORSDI.GetScript: String;
begin
     {$IFDEF INTERBASE}
         {$ifdef FLEXIBLES}
         Result:= 'select FACTOR from SP_IMSS_V( %s, %s, %s, %s )';
         {$else}
         Result:= 'select FACTOR from SP_IMSS( %s, %s, %s )';
         {$endif}
     {$ENDIF}
     {$IFDEF MSSQL}
         {$ifdef FLEXIBLES}
         Result:= 'select FACTOR = DBO.SP_IMSS_V( %s, %s, %s, 4, %s )';
         {$else}
         Result:= 'select FACTOR = DBO.SP_IMSS( %s, %s, %s, 4 )';
         {$endif}
     {$ENDIF}
end;



procedure TZetaFACTORSDI.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     oZetaProvider.GetDatosActivos;
     //ParametrosFijos(3);
     TipoRegresa:= resDouble;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );

     if ParamVacio(0) then
     begin
          AgregaRequerido( 'COLABORA.CB_FEC_ING', enEmpleado );
          AgregaRequerido( 'COLABORA.CB_TABLASS', enEmpleado );
     end
     else
          AgregaRequerido( 'COLABORA.CB_TABLASS', enEmpleado );

     if ParametrosConstantes then
        AgregaScript( Format( GetScript, [ ParamCampoDefString(2,'COLABORA.CB_TABLASS'), ParamCampoDefFecha(0,'COLABORA.CB_FEC_ING'),
                                           ParamCampoDefFecha( 1, EntreComillas( DateToStrSQL( oZetaProvider.FechaDefault ) ) ) {$ifdef FLEXIBLES},'COLABORA.CB_CODIGO'{$endif} ] ),
                                           TipoRegresa, False );
end;

function TZetaFACTORSDI.Calculate: TQrEvResult;
var
   sTablass: String;
   dIngreso: TDate;
begin
     dIngreso := NullDateTime;
     with DataSetEvaluador do
     begin
          if ( FieldCount = 0 ) then
             sTablass := VACIO
          else
          begin
               sTablass := FieldByName( 'CB_TABLASS' ).AsString;
               if ( FindField( 'CB_FEC_ING' ) <> nil ) then
                  dIngreso := FieldByName( 'CB_FEC_ING' ).AsDateTime;
          end;
     end;
     Result:= EvaluaQuery( Format( GetScript, [ EntreComillas( DefaultString( 2, sTablass ) ),
                                                EntreComillas( DateToStrSQL( DefaultDate( 0, dIngreso ) ) ),
                                                EntreComillas( DateToStrSQL( DefaultDate( 1, oZetaProvider.FechaDefault ) ) ) {$ifdef FLEXIBLES},IntToStr(ZetaEvaluador.GetEmpleado){$endif} ] ) );
end;

{ ************** TZetaFECHA_REFE **************** }

type
  TZetaFECHA_REFE = class( TZetaFunc )
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaFECHA_REFE.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa:= resDouble;
     EsFecha := TRUE;
     oZetaProvider.GetDatosActivos;
     AgregaRequerido( 'COLABORA.CB_ACTIVO', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_FEC_BAJ', enEmpleado );
end;

function TZetaFECHA_REFE.Calculate: TQREvResult;
begin
     with DataSetEvaluador, Result do
     begin
          Kind := resDouble;
          EsFecha := TRUE;
	  if zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString ) then
             dblResult := oZetaProvider.FechaDefault
          else
             dblResult := FieldByName( 'CB_FEC_BAJ' ).AsDateTime;
     end;
end;

{ ************** TZetaFECHA_VENCI **************** }

type
  TZetaFECHA_VENCI = class( TZetaRegresaQuery )
  public
    { Public Declarations }
    function Calculate: TQREvResult; override;
    function GetScript :String; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaFECHA_VENCI.GetScript :String;
begin
     Result:= SelRes('SP_FECHA_VENCI( %s, %s )');
end;

procedure TZetaFECHA_VENCI.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa:= resDouble;
     EsFecha := TRUE;
     AgregaRequerido( 'COLABORA.CB_FEC_CON', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_CONTRAT', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     if ParametrosConstantes then
        AgregaScript( Format( GetScript, [ ParamCampoDefFecha( 0, 'COLABORA.CB_FEC_CON' ),
                                           ParamCampoDefString( 1, 'COLABORA.CB_CONTRAT') ] ),
                      TipoRegresa, EsFecha );
end;

function TZetaFECHA_VENCI.Calculate: TQREvResult;
begin
     Result.Kind:= resDouble;
     Result.resFecha:= TRUE;
     with DataSetEvaluador do
          Result:= EvaluaQuery( Format( GetScript, [ EntreComillas( DateToStrSQL( DefaultDate( 0, FieldByName( 'CB_FEC_CON' ).AsDateTime ) ) ),
                                                     EntreComillas( DefaultString( 1, FieldByName( 'CB_CONTRAT' ).AsString ) ) ] ) );
end;

{ ************** TZetaF_BAJA **************** }
type
  TZetaF_BAJA = class( TZetaFunc )
  public
    { Public Declarations }
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaF_BAJA.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa:= resDouble;
     EsFecha:= True;
     AgregaRequerido( 'COLABORA.CB_FEC_BAJ', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_FEC_BSS', enEmpleado );
end;

function TZetaF_BAJA.Calculate: TQREvResult;
begin
     with DataSetEvaluador, Result do
     begin
          Kind := resDouble;
          EsFecha := TRUE;
          case DefaultInteger( 0, 1 ) of
               1: dblResult := FieldByName( 'CB_FEC_BAJ' ).AsDateTime;
               2: dblResult := FieldByName( 'CB_FEC_BSS' ).AsDateTime;
          end;
     end;
end;

{ ************** TZetaF_CONTRATO **************** }

type
  TZetaF_CONTRATO = class( TZetaRegresaQuery )
  private
    FRegresa : Integer;
  public
    function GetScript :String; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaF_CONTRATO.GetScript :String;
begin
     {$IFDEF INTERBASE}
     Result:= 'select %s from SP_FECHA_VENCI( %s, %s )';
     {$ENDIF}
     {$IFDEF MSSQL}
     if ( FRegresa = 3 ) then
        Result:= 'select %s =DBO.SP_DIAS_VENCI( %s%s )'
     else
        Result:= 'select %s =DBO.SP_FECHA_VENCI( %s, %s )';
     {$ENDIF}
end;

procedure TZetaF_CONTRATO.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sCampo, sFecha: String;
begin
     TipoRegresa:= resDouble;
     EsFecha := TRUE;
     if not ParametrosConstantes then
     begin
          AgregaRequerido( 'COLABORA.CB_FEC_CON', enEmpleado );
          AgregaRequerido( 'COLABORA.CB_CONTRAT', enEmpleado );
     end
     else
     begin
          FRegresa := DefaultInteger( 0, 1 );
          case FRegresa of
               1 : begin
                        AgregaRequerido( 'COLABORA.CB_FEC_CON', enEmpleado );
                        Exit;
                   end;
               2 : sCampo:= 'RESULTADO';
               3 : begin
                        sCampo:= 'DIAS';
                        TipoRegresa:= resInt;
                        EsFecha:= False;
                   end;
          else
               ErrorFuncion( 'Error en Par�metro', 'Solo se Permiten Valores 1 a 3' );
          end;
          sFecha := ParamCampoDefFecha( 1, 'COLABORA.CB_FEC_CON' );
{$ifdef MSSQL}
          if FRegresa = 3 then
             sFecha := VACIO;
{$endif}
          AgregaScript( Format( GetScript, [ sCampo, sFecha,
                                             ParamCampoDefString( 2, 'COLABORA.CB_CONTRAT') ] ),
                                             TipoRegresa, EsFecha );
          AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     end;
end;

function TZetaF_CONTRATO.Calculate: TQREvResult;
var
   dFecha : TDate;
   sFecha : String;
begin
     with DataSetEvaluador, Result do
     begin
          FRegresa := DefaultInteger( 0, 1 );
          dFecha := DefaultDate( 1, FieldByName( 'CB_FEC_CON' ).AsDateTime );
          sFecha := EntreComillas( DateToStrSQL( dFecha ) );
          case FRegresa of
               1 : begin
                        Kind := resDouble;
                        EsFecha := TRUE;
                        dblResult := dFecha;
                   end;
               2 : Result:= EvaluaQuery( Format( GetScript, [ 'RESULTADO', sFecha,
                                                              EntreComillas( DefaultString( 2, FieldByName( 'CB_CONTRAT' ).AsString ) ) ] ) );
               3 : begin
{$ifdef MSSQL}
                        sFecha := VACIO;
{$endif}
                        Result:= EvaluaQuery( Format( GetScript, [ 'DIAS', sFecha,
                                                                   EntreComillas( DefaultString( 2, FieldByName( 'CB_CONTRAT' ).AsString ) ) ] ) );
                   end;
          else
               ErrorFuncion( 'Error en Par�metro', 'Solo se Permiten Valores 1 a 3' );
          end;
     end;
end;

{ ************** TZetaMODULO18 **************** }

type
  TZetaMODULO18 = class( TZetaFunc )
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaMODULO18.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa:= resBool;
     AgregaRequerido( 'RPATRON.TB_MODULO', enRPatron );
end;

function TZetaMODULO18.Calculate: TQREvResult;
begin
     with DataSetEvaluador, Result do
     begin
          Kind := resBool;
          booResult := ( FieldByName( 'TB_MODULO' ).AsInteger = Ord( pmEventual )  );
     end;
end;

{ ************** TZetaNOMSTATUS **************** }

type
  TZetaNOMSTATUS = class( TZetaFunc )
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaNOMSTATUS.Calculate: TQREvResult;
begin
     oZetaProvider.GetDatosPeriodo;
     with Result do
     begin
          Kind := resString;
          strResult:= IntToStr( Ord( oZetaProvider.DatosPeriodo.Status ) );
     end;
end;

{ ************** TZetaSEXO **************** }

type
  TZetaSEXO = class( TZetaFunc )
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaSEXO.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resString;
     AgregaRequerido( 'COLABORA.CB_SEXO', enEmpleado );
end;

function TZetaSEXO.Calculate: TQREvResult;
begin
     with DataSetEvaluador, Result do
     begin
          Kind:= resString;
          if ( FieldByName( 'CB_SEXO' ).AsString = 'F' ) then
    	     strResult := ObtieneElemento( lfSexoDesc, Ord( esFemenino ) )
          else
             strResult := ObtieneElemento( lfSexoDesc, Ord( esMasculino ) );
     end;
end;

{ ************** TZetaSTATUSACTIVO **************** }

type
  TZetaSTATUSACTIVO = class( TZetaRegresaQuery )
  public
   function Calculate: TQREvResult; override;
   function GetScript : String; override;
   procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  protected
   function GetNameSP : String; virtual;
  end;

function TZetaSTATUSACTIVO.GetNameSP : string;
begin
     Result := 'SP_STATUS_ACT';
end;

function TZetaSTATUSACTIVO.GetScript : string;
begin
     Result := SelRes(GetNameSP + '( %s, %s )');
end;

procedure TZetaSTATUSACTIVO.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     oZetaProvider.GetDatosActivos;
     TipoRegresa := resInt;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     if ParametrosConstantes then
        AgregaScript( Format( GetScript, [ ParamCampoDefFecha( 0, EntreComillas( DateToStrSQL( oZetaProvider.FechaDefault ) ) ),
                                           'COLABORA.CB_CODIGO' ] ), TipoRegresa, FALSE );
end;

function TZetaSTATUSACTIVO.Calculate : TQREvResult;
begin
     Result:= EvaluaQuery( Format( GetScript, [ EntreComillas( DateToStrSQL( DefaultDate( 0, oZetaProvider.FechaDefault ) ) ),
                                                IntToStr( ZetaEvaluador.GetEmpleado ) ] ));
end;

{ ************** TZetaSTATUSEMP **************** }

type
  TZetaSTATUSEMP = class( TZetaSTATUSACTIVO )
  protected
   function GetNameSP : String; override;
  end;

function TZetaSTATUSEMP.GetNameSP : string;
begin
     Result := 'SP_STATUS_EMP';
end;

{ ************** TZetaCUR_MAX **************** }

type
  TZetaCUR_MAX = class( TZetaRegresaQuery )
  public
   FCurso:string;     
   function Calculate: TQREvResult; override;
   function GetScript : String; override;
   procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaCUR_MAX.GetScript : string;
begin
     Result := SelRes('SP_CUR_MAX' + '( %s, %s, %s )');
end;

procedure TZetaCUR_MAX.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sEmpleado: String;
begin
     ParametrosFijos( 1 );
     oZetaProvider.GetDatosActivos;

     TipoRegresa := resDouble;
     EsFecha := True;

     if ParamVacio(2) then
     begin
          sEmpleado := 'COLABORA.CB_CODIGO';
          AgregaRequerido( sEmpleado, enEmpleado );
     end
     else
         sEmpleado := ParamCampoInteger(2);

     FCurso := ParamCampoString(0);

     if ParametrosConstantes then
                AgregaScript( Format( GetScript, [sEmpleado,
                              FCurso,
                              ParamCampoDefFecha( 1, EntreComillas( DateToStrSQL( oZetaProvider.FechaDefault ) ) )] ),TipoRegresa, EsFecha );
end;

function TZetaCUR_MAX.Calculate : TQREvResult;
var
   iEmpleado: Integer;
begin
     Result.Kind:= resDouble;
     Result.resFecha:= TRUE;

     if ParamVacio(2) then
        iEmpleado := ZetaEvaluador.GetEmpleado
     else
         iEmpleado:= ParamInteger(2);

     FCurso := ParamString(0);

     Result:= EvaluaQuery( Format( GetScript, [ IntToStr( iEmpleado ),EntreComillas(FCurso),DateToStrSQLC( DefaultDate( 1, oZetaProvider.FechaDefault ) ) ] ) );
end;

{ ************** TZetaRANGOSTATUS **************** }

type
  TZetaRANGOSTATUS = class( TZetaRegresaQuery )
  public
   function GetScript : String; override;
   function Calculate: TQREvResult; override;
   procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaRANGOSTATUS.GetScript : string;
begin
     Result := SelRes('SP_RANGO_STATUS( %s, %s, %s, %s )');
end;

procedure TZetaRANGOSTATUS.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     oZetaProvider.GetDatosActivos;
     ParametrosFijos( 1 );
     TipoRegresa := resBool;
     if ( ParamInteger( 0 ) < 0 ) or ( ParamInteger( 0 ) > 9 ) then
        ErrorFuncion( 'Error en Par�metro', 'Solo se Permiten Valores 0 a 9' )
     else
        AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

function TZetaRANGOSTATUS.Calculate : TQREvResult;
var
   ResultadoSP: TQREvResult;
begin
     ResultadoSP:= EvaluaQuery( Format( GetScript, [ IntToStr( ZetaEvaluador.GetEmpleado ),
                                                EntreComillas( DateToStrSQL( DefaultDate( 1, oZetaProvider.FechaDefault ) ) ),
                                                EntreComillas( DateToStrSQL( DefaultDate( 2, oZetaProvider.FechaDefault ) ) ),
                                                IntToStr( ParamInteger( 0 ) ) ] ) );
     with Result do
     begin
          Kind:= resBool;
          booResult:= ( ResultadoSP.intResult > 0 );
     end;
end;

{ ************** TZetaTTURNO **************** }

type
  TZetaTTURNO = class( TZetaRegresa )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaTTURNO.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   FCampoRequiere : string;
begin
     TipoRegresa := resString;
     case DefaultInteger( 0, 1 ) of
          1 : FCampoRequiere := 'TU_DESCRIP';
          2 : begin
                   FCampoRequiere := 'TU_NUMERO';
                   TipoRegresa := resDouble;
              end;
          3 : FCampoRequiere := 'TU_TEXTO';
          4 : FCampoRequiere := 'TU_INGLES';
     else
         ErrorFuncion( 'Error en Par�metro', 'Solo se Permiten Valores 1 a 4' );
     end;
     EquivaleScript( 'TURNO.' + FCampoRequiere, enTurno );
end;

{ ************** TZetaABase **************** }

type
  TZetaABase = class( TZetaRegresaQuery )
  public
    function Calculate: TQREvResult; override;
    function GetScript : String; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  private
    function GetEquivalente : String;
{$ifdef MSSQL}
    function GetCamposAcumula: String;
{$endif}
  protected
    bLista: Boolean;
    sMesIni, sMesFin : String;
    iMesIni, iMesFin : Integer;
    procedure SetMeses; virtual;
    function GetYear  : String; virtual;
  end;

{$ifdef MSSQL}
function  TZetaABase.GetCamposAcumula : String;
const
     K_CAMPO_ACUMULA = 'ACUMULA.AC_MES_%s';
var
   i: Integer;
begin
     Result := VACIO;
     for i := iMesIni to iMesFin do
         Result := ConcatString( Result, Format( K_CAMPO_ACUMULA, [ StrZero( IntToStr( i ), 2 ) ] ), '+' );
end;
{$endif}

function  TZetaABase.GetScript : String;
begin
{$ifdef INTERBASE}
     if bLista then
        Result := 'select sum((select RESULTADO from SP_AS( CONCEPTO.CO_NUMERO, %s, %s, %s, %s ))) from CONCEPTO where CO_NUMERO in (%s)'
     else
        Result := 'select RESULTADO from SP_AS( %s, %s, %s, %s, %s )';
{$endif}
{$ifdef MSSQL}
     if bLista then
        Result := 'select COALESCE( sum( %s ), 0.0 ) as RESULTADO from ACUMULA where ( ACUMULA.CB_CODIGO = %s ) and ( ACUMULA.AC_YEAR = %s ) and  ( ACUMULA.CO_NUMERO in (%s) )'
     else
        Result := 'SELECT RESULTADO = DBO.SP_AS( %s,%s,%s,%s,%s )';
{$endif}
end;

function TZetaABase.GetYear : String;
begin
     Result := IntToStr( DefaultInteger( 3, oZetaProvider.YearDefault ) );
end;

procedure TZetaABase.SetMeses;
begin
     case ParamInteger( 1 ) of
          1: begin
                  iMesIni := DefaultInteger( 2, oZetaProvider.MesDefault );
                  iMesFin := iMesIni;
             end;
          2: begin
                  iMesIni := DefaultInteger( 2, oZetaProvider.MesDefault );
                  if ( ( iMesIni mod 2 ) = 1 ) then
                     iMesFin := iMesIni + 1
                  else
                  begin
                       iMesFin := iMesIni;
                       iMesIni := iMesIni - 1;
                  end;

                  if ( iMesFin > 13) then //No hay una constante con el mes 13
                     iMesFin := 13;
             end
     else
          begin
               iMesIni := 1;
               iMesFin := 13;
          end;
     end;
     sMesIni := IntToStr( iMesIni );
     sMesFin := IntToStr( iMesFin );
end;

function TZetaABase.GetEquivalente : String;
var
   nConcepto : Integer;
begin
     if ParamEsCampo( 0 ) then
          Result := ParamNombreCampo( 0 )
     else
     begin
          nConcepto := 0;
          try
             nConcepto := ParamInteger( 0 );
          except
          end;
          if nConcepto = 0 then
             nConcepto := GetEquivaleAcumulado( ParamString( 0 ) );

          if nConcepto = 0 then
             ErrorParametro( 'La Lista de Acumulados no es V�lida en el Par�metro #1', VACIO );

          Result := IntToStr( nConcepto );
     end;
end;

procedure TZetaABase.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sLista : String;
begin
     ValidaConfidencial;
     TipoRegresa := resDouble;
     with oZetaProvider do
     begin
          GetDatosActivos;
          GetDatosPeriodo;
     end;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );

     bLista := ( Argument( 0 ).Kind = resString ) and ( Pos( ',',ParamString( 0 )) > 0 );

     if not ParametrosConstantes then
     	  Exit;
     SetMeses;
     if bLista then
     begin
          if ParamEsCampo( 0 ) then
             sLista := ParamNombreCampo( 0 )
          else
          begin
               sLista := ParamString( 0 );
               if NOT ValidaListaNumeros( sLista ) then
                  ErrorParametro( 'La Lista de Acumulados no es V�lida en el Par�metro #1', VACIO );
          end;
{$ifdef INTERBASE}
          AgregaScript( Format( GetScript, [ sMesIni, sMesFin, GetYear, 'COLABORA.CB_CODIGO',
                                             sLista ] ), TipoRegresa , FALSE )
{$endif}
{$ifdef MSSQL}
          AgregaScript( Format( GetScript, [ GetCamposAcumula, 'COLABORA.CB_CODIGO', GetYear,
                                             sLista ] ), TipoRegresa , FALSE )
{$endif}
     end
     else
         AgregaScript( Format( GetScript, [ GetEquivalente, sMesIni, sMesFin, GetYear,
                                             'COLABORA.CB_CODIGO' ] ), TipoRegresa, FALSE);


end;

function TZetaABase.Calculate: TQREvResult;
begin
     SetMeses;
     bLista := ( Argument( 0 ).Kind = resString ) and ( Pos( ',',ParamString( 0 )) > 0 );
     if bLista then
     begin
          if NOT ValidaListaNumeros( ParamString( 0 ) ) then
             ErrorParametro( 'La Lista de Acumulados no es V�lida en el Par�metro #1', VACIO );
{$ifdef INTERBASE}
          Result := EvaluaQuery( Format( GetScript, [ sMesIni, sMesFin, GetYear,
                                                      IntToStr( ZetaEvaluador.GetEmpleado ),
                                                      ParamString( 0 ) ]) )
{$endif}
{$ifdef MSSQL}
          Result := EvaluaQuery( Format( GetScript, [ GetCamposAcumula,
                                                      IntToStr( ZetaEvaluador.GetEmpleado ),
                                                      GetYear, ParamString( 0 ) ] ) )
{$endif}
     end
     else
{$ifdef INTERBASE}
        Result := EvaluaQuery( Format( GetScript, [ GetEquivalente, sMesIni, sMesFin,
                                                    GetYear, IntToStr( ZetaEvaluador.GetEmpleado ) ]) );
{$endif}
{$ifdef MSSQL}
        {SP_AS ( @Concepto SMALLINT,
                 @MesIni SMALLINT, @MesFin SMALLINT,
                 @Year SMALLINT, @Empleado INTEGER )}
        Result := EvaluaQuery( Format( GetScript, [ GetEquivalente,
                                                    sMesIni, sMesFin,
                                                    GetYear,
                                                    IntToStr( ZetaEvaluador.GetEmpleado ) ] ) );
{$endif}
end;

{ ************** TZetaADosBase **************** }

type
  TZetaADosBase = class( TZetaABase )
  public
    function GetScript : String; override;
    function GetScriptRazonSocial : String;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
    function  GetRazonSocialParam: String; virtual;
  private
    function GetCamposAcumula: String;
  end;

function TZetaADosBase.GetRazonSocialParam : String;
begin
     Result :=  DefaultString( 4, VACIO );
end;

procedure TZetaADosBase.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sLista : String;
begin
     ValidaConfidencial;
     TipoRegresa := resDouble;
     with oZetaProvider do
     begin
          GetDatosActivos;
          GetDatosPeriodo;
     end;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );

     bLista := ( Argument( 0 ).Kind = resString ) and ( Pos( ',',ParamString( 0 )) > 0 );

     if not ParametrosConstantes then
     	  Exit;
     SetMeses;
     if bLista then
     begin
          if ParamEsCampo( 0 ) then
             sLista := ParamNombreCampo( 0 )
          else
          begin
               sLista := ParamString( 0 );
               if NOT ValidaListaNumeros( sLista ) then
                  ErrorParametro( 'La Lista de Acumulados no es V�lida en el Par�metro #1', VACIO );
          end;
          if GetRazonSocialParam <> VACIO then
             AgregaScript( Format( GetScriptRazonSocial, [ GetCamposAcumula, 'COLABORA.CB_CODIGO', GetYear,
                                                           sLista, EntreComillas( GetRazonSocialParam ) ] ), TipoRegresa , FALSE )
          else
              AgregaScript( Format( GetScript, [ GetCamposAcumula, 'COLABORA.CB_CODIGO', GetYear,
                                                 sLista ] ), TipoRegresa , FALSE );
     end
     else
         AgregaScript( Format( GetScript, [ GetEquivalente, sMesIni, sMesFin, GetYear,
                                            'COLABORA.CB_CODIGO', EntreComillas( GetRazonSocialParam ) ] ), TipoRegresa, FALSE);
end;

function TZetaADosBase.GetCamposAcumula: String;
const
     K_CAMPO_ACUMULA = 'ACUMULA_RS.AC_MES_%s';
var
   i: Integer;
begin
     Result := VACIO;
     for i := iMesIni to iMesFin do
         Result := ConcatString( Result, Format( K_CAMPO_ACUMULA, [ StrZero( IntToStr( i ), 2 ) ] ), '+' );
end;

function  TZetaADosBase.GetScript : String;
begin
     if bLista then
        Result := 'select COALESCE( sum( %s ), 0.0 ) as RESULTADO from ACUMULA_RS where ( ACUMULA_RS.CB_CODIGO = %s ) and ( ACUMULA_RS.AC_YEAR = %s ) and  ( ACUMULA_RS.CO_NUMERO in (%s) )'
     else
        Result := 'select RESULTADO = DBO.SP_AS2( %s, %s, %s, %s, %s , %s )';
end;

function  TZetaADosBase.GetScriptRazonSocial : String;
begin
     Result := 'select COALESCE( sum( %s ), 0.0 ) as RESULTADO from ACUMULA_RS where ( ACUMULA_RS.CB_CODIGO = %s ) and ( ACUMULA_RS.AC_YEAR = %s ) and  ( ACUMULA_RS.CO_NUMERO in (%s) ) and ( ACUMULA_RS.RS_CODIGO = %s )';
end;

function TZetaADosBase.Calculate: TQREvResult;
begin
     SetMeses;
     bLista := ( Argument( 0 ).Kind = resString ) and ( Pos( ',',ParamString( 0 )) > 0 );
     if bLista then
     begin
          if NOT ValidaListaNumeros( ParamString( 0 ) ) then
             ErrorParametro( 'La Lista de Acumulados no es V�lida en el Par�metro #1', VACIO );

          if GetRazonSocialParam <> VACIO then
             Result := EvaluaQuery( Format( GetScriptRazonSocial, [ GetCamposAcumula, IntToStr( ZetaEvaluador.GetEmpleado ), GetYear,
                                                                    ParamString( 0 ), EntreComillas( GetRazonSocialParam ) ] ) )
          else
              Result := EvaluaQuery( Format( GetScript, [ GetCamposAcumula, IntToStr( ZetaEvaluador.GetEmpleado ), GetYear,
                                                          ParamString( 0 ) ] ) );
     end
     else
        {SP_AS2 ( @Concepto SMALLINT,
                 @MesIni SMALLINT, @MesFin SMALLINT,
                 @Year SMALLINT, @Empleado INTEGER
                 @RazonSocial CHAR)}
        Result := EvaluaQuery( Format( GetScript, [ GetEquivalente,
                                                    sMesIni, sMesFin,
                                                    GetYear,
                                                    IntToStr( ZetaEvaluador.GetEmpleado ),
                                                    EntreComillas( GetRazonSocialParam ) ] ));
end;
{ ************** FIN ************** }

{ ************** TZetaACUMULADOS **************** }

type
  TZetaACUMULADOS = class( TZetaABase )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaACUMULADOS.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos( 2 );
     inherited GetRequeridos( TipoRegresa );
end;

{ ************** TZetaACUMULADOSDos Acumulados Razon Social **************** }

type
  TZetaACUMULADOSDos = class( TZetaADosBase )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaACUMULADOSDos.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos( 2 );
     inherited GetRequeridos( TipoRegresa );
end;

{ ************** TZetaACUMULADOS **************** }

type
  TZetaACUMULADOMENSUAL = class( TZetaABase )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  protected
    procedure SetMeses;override;
    function GetYear  : String; override;
  end;

function TZetaACUMULADOMENSUAL.GetYear : String;
begin
     Result := IntToStr( DefaultInteger( 2,oZetaProvider.YearDefault) );
end;

procedure TZetaACUMULADOMENSUAL.SetMeses;
begin
     iMesIni := DefaultInteger( 1, oZetaProvider.MesDefault );
     iMesFin := iMesIni;
     sMesIni := IntToStr( iMesIni );
     sMesFin := sMesIni;
end;

procedure TZetaACUMULADOMENSUAL.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos( 1 );
     inherited GetRequeridos( TipoRegresa );
end;

{ ************** TZetaACUMULADORELATIVO **************** }
//AR ( Concepto,  nDeltaMes  [, nDeltaYear] , [lBrincaA�o],[lIncluirMes13])
type
  TZetaACUMULADORELATIVO = class( TZetaABase )
  iOffSet :Integer;
  iOffSetMes13 :Integer;
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  protected
    procedure SetMeses;override;
    function GetYear  : String; override;
  end;

function TZetaACUMULADORELATIVO.GetYear : String;
begin
     Result := IntToStr( oZetaProvider.YearDefault + DefaultInteger( 2, 0 ) + iOffSet )
end;

procedure TZetaACUMULADORELATIVO.SetMeses;
var
   nDeltaMes :Integer;
   lIncluirMes13, lBrincaYear :Boolean;
begin
     nDeltaMes := ParamInteger( 1 );
     //iOffSet := 0;
     //iOffSetMes13 := 0;
     lBrincaYear := DefaultBoolean( 3,FALSE );
     lIncluirMes13 := DefaultBoolean( 4,FALSE );
     iMesIni := oZetaProvider.MesDefault + nDeltaMes;

     if not( SetMesesRelativo( nDeltaMes, iMesIni, iOffSet,
             lIncluirMes13, lBrincaYear, oZetaProvider.MesDefault  ) ) then
     begin
          ErrorFuncion( 'El mes solicitado no es v�lido', 'Funci�n: ' + Expresion );
     end;

     iMesFin := iMesIni;
     sMesIni := IntToStr( iMesIni );
     sMesFin := sMesIni;  {  AP: Se cambi� para compartir l�gica con la funci�n RAR()

     if ( iMesIni  < 1 )then
     begin
          if lBrincaYear then
             iOffSet := -1;//a�o anterior

          if ( lIncluirMes13 )then
               iOffSetMes13 := 1;

          iMesIni := 12 + iOffSetMes13 + iMesIni
     end
     else
     begin
          if ( iMesIni > 12 )then
          begin
               if  NOT ( lIncluirMes13 and ( ( iMesIni ) = 13 ) )then
               begin
                    if( lIncluirMes13 and ( ( iMesIni ) > 13 ) )then
                    begin
                         iOffSetMes13 := 1
                    end;

                    if lBrincaYear then
                    begin
                         iOffSet := 1;
                         iMesIni := Abs( 12 + iOffSetMes13 - oZetaProvider.MesDefault - nDeltaMes)
                    end;
               end;
          end;
     end;

     if ( iMesIni < 1 ) or ( iMesIni > 13 ) then
        ErrorFuncion( 'El mes solicitado no es v�lido', 'Funci�n: ' + Expresion );

     iMesFin := iMesIni;
     sMesIni := IntToStr( iMesIni );
     sMesFin := sMesIni;   }
end;

procedure TZetaACUMULADORELATIVO.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos( 2 );
     inherited GetRequeridos( TipoRegresa );
end;

{ ************** TZetaACUMULADORELATIVODos Razon Social **************** }
//AR2 ( Concepto,  nDeltaMes  [, nDeltaYear] , [lBrincaA�o],[lIncluirMes13],[sRazonSocial])
type
  TZetaACUMULADORELATIVODos = class( TZetaADosBase )
  iOffSet :Integer;
  iOffSetMes13 :Integer;
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function GetRazonSocialParam: String; override;
  protected
    procedure SetMeses;override;
    function GetYear  : String; override;
  end;

function TZetaACUMULADORELATIVODos.GetYear : String;
begin
     Result := IntToStr( oZetaProvider.YearDefault + DefaultInteger( 2, 0 ) + iOffSet )
end;

function TZetaACUMULADORELATIVODos.GetRazonSocialParam : String;
begin
     Result :=  DefaultString( 5, VACIO );
end;

procedure TZetaACUMULADORELATIVODos.SetMeses;
var
   nDeltaMes :Integer;
   lIncluirMes13, lBrincaYear :Boolean;
begin
     nDeltaMes := ParamInteger( 1 );
     lBrincaYear := DefaultBoolean( 3,FALSE );
     lIncluirMes13 := DefaultBoolean( 4,FALSE );
     iMesIni := oZetaProvider.MesDefault + nDeltaMes;
     if not( SetMesesRelativo( nDeltaMes, iMesIni, iOffSet,
             lIncluirMes13, lBrincaYear, oZetaProvider.MesDefault  ) ) then
     begin
          ErrorFuncion( 'El mes solicitado no es v�lido', 'Funci�n: ' + Expresion );
     end;

     iMesFin := iMesIni;
     sMesIni := IntToStr( iMesIni );
     sMesFin := sMesIni;
end;

procedure TZetaACUMULADORELATIVODos.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
      ParametrosFijos( 2 );
     inherited GetRequeridos( TipoRegresa );
end;
{*** FIN ***}

type
  TZetaACUMULADOSUMA = class( TZetaABase )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  protected
    procedure SetMeses;override;
  end;

procedure TZetaACUMULADOSUMA.SetMeses;
begin
     iMesIni := ParamInteger( 1 );
     iMesFin := ParamInteger( 2 );
     sMesIni := IntToStr( iMesIni );
     sMesFin := IntToStr( iMesFin );
end;

procedure TZetaACUMULADOSUMA.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos( 3 );
     inherited GetRequeridos( TipoRegresa );
end;

type
  TZetaACUMULADOSUMADos = class( TZetaADosBase )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  protected
    procedure SetMeses;override;
  end;

procedure TZetaACUMULADOSUMADos.SetMeses;
begin
     iMesIni := ParamInteger( 1 );
     iMesFin := ParamInteger( 2 );
     sMesIni := IntToStr( iMesIni );
     sMesFin := IntToStr( iMesFin );
end;

procedure TZetaACUMULADOSUMADos.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos( 3 );
     inherited GetRequeridos( TipoRegresa );
end;

{ ************** TZetaACUMULADOAJUSTA **************** }
{ Esta funcion regresa la suma de acumulados desde enero hasta el parametro Mes (Mes final)}
{ Sintaxis: AJ ( Concepto [,Mes, Year] ) }


type
  TZetaACUMULADOAJUSTA = class( TZetaABase )
  private
    procedure PreparaEmpleadosConAjuste;
    function AjustaEmpleado : Boolean;
  protected
    procedure SetMeses;override;
    function GetYear  : String; override;
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    constructor Create(oEvaluador:TQrEvaluator); override;
  end;

constructor TZetaACUMULADOAJUSTA.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create(oEvaluador);
     PreparaEmpleadosConAjuste;
end;

procedure TZetaACUMULADOAJUSTA.PreparaEmpleadosConAjuste;
begin
     if ( oZetaProvider.EmpleadosConAjuste = nil ) then
        oZetaProvider.EmpleadosConAjuste := TEmpleadosConAjuste.Create(ZetaEvaluador.oZetaCreator);
end;

function TZetaACUMULADOAJUSTA.AjustaEmpleado: Boolean;
begin
     Result := FALSE;
     if ( not ZetaEvaluador.CalculandoNomina ) or ( oZetaProvider.DatosPeriodo.EsUltimaNomina ) then
        Result := TEmpleadosConAjuste(oZetaProvider.EmpleadosConAjuste).AplicaAjuste(ZetaEvaluador.GetEmpleado);
end;

procedure TZetaACUMULADOAJUSTA.SetMeses;
begin
     iMesFin := DefaultInteger( 1, oZetaProvider.MesDefault );
     if ( iMesFin <1 ) or(iMesFin >13 ) then
        ErrorFuncion( 'El mes solicitado no es v�lido', 'Funci�n: ' + Expresion );

     //Si el empleado cumple con la expresion K_GLOBAL_EMPLEADO_CON_AJUSTE, el mes inicial es ENERO.
     if AjustaEmpleado then
        iMesIni := 1
     else
          iMesIni := iMesFin;

     sMesIni := IntToStr( iMesIni );
     sMesFin := IntToStr( iMesFin );
end;

function TZetaACUMULADOAJUSTA.GetYear  : String;
begin
     Result := IntToStr( DefaultInteger( 2, oZetaProvider.YearDefault ) );
end;

procedure TZetaACUMULADOAJUSTA.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos( 1 );
     with oZetaProvider do
     begin
          if ( not ZetaEvaluador.CalculandoNomina ) or DatosPeriodo.EsUltimaNomina then
          begin
               if StrVacio( FExpresion ) then
                  inherited GetRequeridos( TipoRegresa )
               else
               begin
                    ValidaConfidencial;
                    TipoRegresa := resDouble;
                    with oZetaProvider do
                    begin
                         GetDatosActivos;
                         GetDatosPeriodo;
                    end;
                    AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
               end;
          end
          else
              inherited GetRequeridos( TipoRegresa )
     end;
end;

{ ************** TZetaRABase **************** }

type
  TZetaRABase = class( TZetaRegresaQuery )
  public
    function Calculate: TQREvResult; override;
    function GetScript : String; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  private
    function GetEquivalente : String;
  protected
    sLista,sReferencia,
    sRazonSocial,sValidaReingreso,sTipoNomina,sConcepto, sValorEmpleado: String;
    bLista: Boolean;
    sMesIni, sMesFin : String;
    iMesIni, iMesFin : Integer;
    procedure SetMeses; virtual;
    function GetYear  : String; virtual;
    procedure SetValores; virtual;
    procedure SetRazonSocial; virtual;
    function GetFormatScript: String; virtual;
  end;


function  TZetaRABase.GetScript : String;
begin
     inherited;
      {$ifdef INTERBASE}
     if bLista then
        Result := 'select sum( ( select RESULTADO from SP_RAS( CONCEPTO.CO_NUMERO, %1:s, %2:s, %3:s, %4:s, %5:s, %6:s, %7:s, %8:s ))) from CONCEPTO where CO_NUMERO in (%0:s)'
     else
        Result := 'select RESULTADO from SP_RAS( %s, %s, %s, %s, %s, %s, %s, %s, %s )';
     {$endif}
     {$ifdef MSSQL}
     if bLista then
        Result:= 'select sum( dbo.SP_RAS( CONCEPTO.CO_NUMERO, %1:s, %2:s, %3:s, %4:s, %5:s , %6:s, %7:s, %8:s )  ) as RESULTADO from CONCEPTO ' +
                 '%9:s where CO_NUMERO in (%0:s)'
     else
        Result := 'SELECT RESULTADO = DBO.SP_RAS( %s,%s,%s,%s,%s,%s,%s,%s, %s )';
     {$endif}
end;

function  TZetaRABase.GetFormatScript : String;
const
     {$ifdef MSSQL}
     aNombreColabora: array[FALSE..TRUE] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ('COLABORA.CB_CODIGO','C.CB_CODIGO');
     K_QRY_COLABORA = 'left outer join COLABORA C on C.CB_CODIGO = COLABORA.CB_CODIGO';
     {$endif}
     {$ifdef INTERBASE}
     // en Interbase no es necesario un Prefijo en Colabora cuando viene una lista
      aNombreColabora: array[FALSE..TRUE] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ('COLABORA.CB_CODIGO','COLABORA.CB_CODIGO');
     {$endif}
begin
     if  ParametrosConstantes then
          Result:= Format( GetScript, [ sConcepto, sMesIni, sMesFin,
                                        GetYear, aNombreColabora[bLista],
                                            EntreComillas(sReferencia), EntreComillas(sRazonSocial), sTipoNomina,
                                            EntreComillas(sValidaReingreso)
                                            {$ifdef MSSQL}
                                            ,K_QRY_COLABORA
                                            {$endif} ] )
      else
          Result:= Format( GetScript, [ sConcepto, sMesIni, sMesFin, GetYear,
                                          IntToStr( ZetaEvaluador.GetEmpleado ),
                                            EntreComillas(sReferencia), EntreComillas(sRazonSocial), sTipoNomina,
                                            EntreComillas(sValidaReingreso)
                                            {$ifdef MSSQL}
                                            ,VACIO
                                            {$endif} ] )
end;


procedure TZetaRABase.SetRazonSocial;
begin
     sRazonSocial:= DefaultString( 2,VACIO);
end;

function TZetaRABase.GetYear : String;
begin
     if ParamEsCampo( 4 ) then
          Result := ParamNombreCampo( 4 )
     else
         Result := IntToStr( DefaultInteger( 4, oZetaProvider.YearDefault ) );
end;

procedure TZetaRABase.SetMeses;
begin
     case ParamInteger( 1 ) of
          1: begin
                  iMesIni := DefaultInteger( 3, oZetaProvider.MesDefault );
                  iMesFin := iMesIni;
             end;
          2: begin
                  iMesIni := DefaultInteger( 3, oZetaProvider.MesDefault );
                  if ( ( iMesIni mod 2 ) = 1 ) then
                     iMesFin := iMesIni + 1
                  else
                  begin
                       iMesFin := iMesIni;
                       iMesIni := iMesIni - 1;
                  end;
             end
     else
          begin
               iMesIni := 1;
               iMesFin := 13;
          end;
     end;
     sMesIni := IntToStr( iMesIni );
     sMesFin := IntToStr( iMesFin );
end;

procedure TZetaRABase.SetValores;
begin
     sReferencia:= DefaultString( 5,VACIO);
     sTipoNomina:= IntToStr( DefaultInteger(6, -1) );
     sValidaReingreso:= DefaultString(7,VACIO);
end;

function TZetaRABase.GetEquivalente : String;
var                                                   
   nConcepto : Integer;
begin
     if ParamEsCampo( 0 ) then
          Result := ParamNombreCampo( 0 )
     else
     begin
          nConcepto := 0;
          try
             nConcepto := ParamInteger( 0 );
          except
          end;

          if nConcepto = 0 then
             nConcepto := GetEquivaleAcumulado( ParamString( 0 ) );

          if nConcepto = 0 then
             ErrorParametro( 'La Lista de Acumulados no es V�lida en el Par�metro #1', VACIO );

          Result := IntToStr( nConcepto );
     end;
end;

procedure TZetaRABase.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ValidaConfidencial;
     TipoRegresa := resDouble;
     with oZetaProvider do
     begin
          GetDatosActivos;
          GetDatosPeriodo;
     end;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );

     bLista := ( Argument( 0 ).Kind = resString ) and ( Pos( ',',ParamString( 0 )) > 0 );

     if not ParametrosConstantes then
     	  Exit;
     SetMeses;
     SetRazonSocial;
     SetValores;
     if bLista then
     begin
          if ParamEsCampo( 0 ) then
             sLista := ParamNombreCampo( 0 )
          else
          begin
               sLista := ParamString( 0 );
               if NOT ValidaListaNumeros( sLista ) then
                  ErrorParametro( 'La Lista de Acumulados no es V�lida en el Par�metro #1', VACIO );
          end;
          sConcepto:= sLista;
     end
     else
         sConcepto:= GetEquivalente;

     AgregaScript( GetFormatScript, TipoRegresa, FALSE );

end;

function TZetaRABase.Calculate: TQREvResult;
begin
     SetMeses;
     SetRazonSocial;
     SetValores;
     bLista := ( Argument( 0 ).Kind = resString ) and ( Pos( ',',ParamString( 0 )) > 0 );
     if bLista then
     begin
          sLista:= ParamString( 0 );
          if NOT ValidaListaNumeros( ParamString( 0 ) ) then
             ErrorParametro( 'La Lista de Acumulados no es V�lida en el Par�metro #1', VACIO );
          sConcepto:= sLista;
     end
     else
         sConcepto:= GetEquivalente;

     Result := EvaluaQuery( GetFormatScript );
end;


{  ****** TZetaRACUMULADOS *********** }

type
  TZetaRACUMULADOS = class( TZetaRABase )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  protected
  end;

procedure TZetaRACUMULADOS.GetRequeridos(var TipoRegresa: TQREvResultType);
begin
     ParametrosFijos( 2 );
     inherited GetRequeridos( TipoRegresa );

end;



{  ****** TZetaRACUMULADOSUMA  ******* }

type
  TZetaRACUMULADOSUMA = class( TZetaRABase )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  protected
    procedure SetMeses;override;
    procedure SetRazonSocial; override;
  end;

procedure TZetaRACUMULADOSUMA.SetMeses;
begin
     iMesIni := ParamInteger( 1 );
     iMesFin := ParamInteger( 2 );
     sMesIni := IntToStr( iMesIni );
     sMesFin := IntToStr( iMesFin );
end;

procedure TZetaRACUMULADOSUMA.SetRazonSocial;
begin
     sRazonSocial:= DefaultString(3,VACIO);
end;

procedure TZetaRACUMULADOSUMA.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos( 3 );
     inherited GetRequeridos( TipoRegresa );
end;



{  ****** TZetaRACUMULADORELATIVO ***** }

type
  TZetaRACUMULADORELATIVO = class( TZetaRABase )
  iOffSet :Integer;
  iOffSetMes13 :Integer;
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  protected
    procedure SetMeses;override;
    procedure SetValores; override;
    function GetYear: String; override;
  end;

procedure TZetaRACUMULADORELATIVO.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos( 2 );
     inherited GetRequeridos( TipoRegresa );
end;

function TZetaRACUMULADORELATIVO.GetYear : String;
begin
     Result := IntToStr( oZetaProvider.YearDefault + DefaultInteger( 3, 0 ) + iOffSet )
end;


procedure TZetaRACUMULADORELATIVO.SetMeses;
var
   nDeltaMes: Integer;
   lIncluirMes13, lBrincaYear : Boolean;
begin
     nDeltaMes := ParamInteger( 1 );
     lBrincaYear := DefaultBoolean( 7,FALSE );
     lIncluirMes13 := DefaultBoolean( 8,FALSE );
     iMesIni := oZetaProvider.MesDefault + nDeltaMes;
     if not( SetMesesRelativo( nDeltaMes, iMesIni, iOffSet,
             lIncluirMes13, lBrincaYear, oZetaProvider.MesDefault  ) ) then
        ErrorFuncion( 'El mes solicitado no es v�lido', 'Funci�n: ' + Expresion );

     iMesFin := iMesIni;
     sMesIni := IntToStr( iMesIni );
     sMesFin := sMesIni;
end;

procedure TZetaRACUMULADORELATIVO.SetValores;
begin
     sReferencia:= DefaultString( 4,VACIO);
     sTipoNomina:= IntToStr( DefaultInteger(5, -1) );
     sValidaReingreso:= DefaultString(6,VACIO);
end;




{ ************** TZetaCBase    **************** }

function  TZetaCBase.GetScript : String;
begin
{$ifdef INTERBASE}
     if bLista then
        Result := 'select sum((select RESULTADO from SP_C( CONCEPTO.CO_NUMERO, %s, %s, %s, %s ))) from CONCEPTO where CO_NUMERO in (%s)'
     else
        Result := SelRes('SP_C( %s, %s, %s, %s, %s )');
{$endif}
{$ifdef MSSQL}
     if bLista then
        Result := 'select COALESCE( sum( MOVIMIEN.MO_PERCEPC + MOVIMIEN.MO_DEDUCCI ), 0.0 ) as RESULTADO ' +
                  'from MOVIMIEN where ( MOVIMIEN.CB_CODIGO = %3:s ) and ( MOVIMIEN.PE_YEAR = %2:s ) ' +
                  'and ( MOVIMIEN.PE_TIPO = %1:s ) and ( MOVIMIEN.PE_NUMERO = %0:s ) and ( MOVIMIEN.CO_NUMERO IN (%4:s) ) and ' +
                  '( MOVIMIEN.MO_ACTIVO = ''S'' )'
     else
        Result := 'SELECT RESULTADO = DBO.SP_C(%0:s,%1:s,%2:s,%3:s,%4:s)';
{$endif}
end;

function TZetaCBase.EsNomina: Boolean;
begin
    Result := ( Evaluador is TZetaEvaluador ) and  ( TZetaEvaluador( Evaluador ).Entidad in NomParamEntidades );
end;

procedure TZetaCBase.SetEmpleado( const iParam: Byte );
begin
     if bCalcula then
        sEmpleado := IntToStr( DefaultInteger( iParam, ZetaEvaluador.GetEmpleado ))
     else
     begin
          if ParamVacio(iParam) then
          begin
             if EsNomina then
                sEmpleado := 'NOMINA.CB_CODIGO'
             else
                sEmpleado := 'COLABORA.CB_CODIGO';
          end
          else
             sEmpleado := ParamCampoInteger(iParam);
     end;
end;

function TZetaCBase.ParamPeriodo(const iParam: Integer; const sCampo : String): String;
begin
    if bCalcula then
        Result := IntToStr( DefaultInteger( iParam, DataSetEvaluador.FieldByName( sCampo ).AsInteger ))
    else if ParamVacio( iParam ) then
        Result := 'NOMINA.' + sCampo
    else
        Result := ParamCampoInteger( iParam );
end;

function TZetaCBase.FijoPeriodo(const sCampo: String): String;
begin
    if bCalcula then
        Result := DataSetEvaluador.FieldByName( sCampo ).AsString
    else
        Result := 'NOMINA.' + sCampo;
end;


procedure TZetaCBase.SetDatos;
begin
     if ( EsNomina ) then
     begin
        sNumero := ParamPeriodo( 1, 'PE_NUMERO' );
        sTipo   := ParamPeriodo( 2, 'PE_TIPO' );
        sYear   := ParamPeriodo( 3, 'PE_YEAR' );
     end
     else
       with oZetaProvider.DatosPeriodo do
       begin
            sNumero := ParamCampoDefInteger( 1, Numero );
            sTipo   := ParamCampoDefInteger( 2, Ord( Tipo ) );
            sYear   := ParamCampoDefInteger( 3, Year );
       end;
     SetEmpleado( 4 );
end;

procedure TZetaCBase.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sLista : String;
begin
     ValidaConfidencial;
     bCalcula:= FALSE;
     TipoRegresa := resDouble;
     if ( EsNomina ) then
        AgregaRequerido( 'NOMINA.CB_CODIGO', enNomina )
     else
        AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     oZetaProvider.GetEncabPeriodo;
     bLista := ( Argument( 0 ).Kind <> resInt );
     if not ParametrosConstantes then
     begin
        // Los requiere el CALCULATE
        if ( EsNomina ) then
        begin
           AgregaRequerido( 'NOMINA.PE_YEAR', enNomina );
           AgregaRequerido( 'NOMINA.PE_TIPO', enNomina );
           AgregaRequerido( 'NOMINA.PE_NUMERO', enNomina );
        end;
     	Exit;
     end;

     SetDatos;
     if bLista then
     begin
          if ParamEsCampo( 0 ) then
              sLista := ParamNombreCampo( 0 )
          else
          begin
               sLista := ParamString( 0 );
               if NOT ValidaListaNumeros( sLista ) then
                  ErrorParametro( 'La Lista de Conceptos no es V�lida en el Par�metro #1', VACIO );

          end;
          AgregaScript( Format( GetScript, [ sNumero, sTipo, sYear, sEmpleado,
                                             sLista ] ), TipoRegresa, FALSE)
     end
     else
          AgregaScript( Format( GetScript, [ ParamCampoInteger( 0 ), sNumero, sTipo, sYear,
                                             sEmpleado ] ), TipoRegresa, FALSE);
end;

function TZetaCBase.Calculate: TQREvResult;
begin
     bCalcula:= TRUE;
     bLista := ( Argument( 0 ).Kind <> resInt );
     SetDatos;
     if bLista then
     begin
          if NOT ValidaListaNumeros( ParamString( 0 ) ) then
             ErrorParametro( 'La Lista de Conceptos no es V�lida en el Par�metro #1', VACIO );

          Result := EvaluaQuery( Format( GetScript, [ sNumero, sTipo, sYear, sEmpleado,
                                                      ParamString( 0 ) ]) )
     end
     else
        Result := EvaluaQuery( Format( GetScript, [ IntToStr( ParamInteger( 0 ) ), sNumero, sTipo, sYear,
                                                    sEmpleado ]) );
end;

{ ************* TZetaCONCEPTO ***********}

procedure TZetaCONCEPTO.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     {solamente aplica para c(),
     esta validacion nada mas es en el GetRequeridos
     en el calculate preguntar por calculandoNomina
     Caso de prueba:
          tener un catalogo de conceptos,
                en el que se tengan referencias circulares
                guardar los valores del primer y segundo calculo.
     }
     ParametrosFijos( 1 );
     TipoRegresa := resDouble;

     if ZetaEvaluador.CalculandoNomina AND
        ( ArgList.Count = 1 ) then
     begin
          if ( Argument( 0 ).Kind <> resInt ) then
             ErrorFuncion( 'Conceptos de N�mina,' + CR_LF +
                           'si el Primer Par�metro No es Num�rico' + CR_LF +
                           'Indicar al Menos el N�mero de Per�odo', 'Funci�n: ' + Nombre  );
     end
     else inherited GetRequeridos( TipoRegresa );
end;

function TZetaCONCEPTO.Calculate: TQREvResult;
begin
     if ZetaEvaluador.CalculandoNomina AND
         ( ArgList.Count = 1 ) AND
         ( Argument( 0 ).Kind = resInt ) then
     begin
          Result.Kind := resDouble;
          Result.dblResult := TCalcNomina(ZetaEvaluador.CalcNomina).ConceptoCalculado( ParamInteger( 0 ) );
     end
     else
         Result := Inherited Calculate;
end;
{ ************* TZetaCONCEPTORELATIVO ***********}
//CR( Concepto, DeltaNumero  [,nEmpleado], [lBrincaA�o ]  )
type
  TZetaCONCEPTORELATIVO = class( TZetaCBase )
  iOffSet :Integer;
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  protected
    procedure SetDatos; override;
  end;

procedure TZetaCONCEPTORELATIVO.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos( 2 );
     inherited GetRequeridos( TipoRegresa );
end;

procedure TZetaCONCEPTORELATIVO.SetDatos;
var
   iDeltaNumero:Integer;
   lBrincaAnio:Boolean;
   sNumeroSinCalcular:string;
   iUltPerOrd,iPeriodoACalcular:Integer;
const
     qryUltPerOrd = 'select max(PE_NUMERO) ULTIMO from PERIODO where ( PE_YEAR = %0:d and PE_TIPO = %1:d and PE_NUMERO < %2:d and PE_CAL = %3:s ) ';
begin
     lBrincaAnio := DefaultBoolean( 3,FALSE );
     iDeltaNumero := ParamInteger( 1 );
     iOffSet := 0;
     iPeriodoACalcular := oZetaProvider.DatosPeriodo.Numero + iDeltaNumero;
     oZetaProvider.InitGlobales;
     if( lBrincaAnio )then
     begin //Montos de concepto de a�os anteriores
          //Obtener el ultimo periodo ordinario del a�o actual
          AbreQuery( Format( qryUltPerOrd,[oZetaProvider.DatosPeriodo.Year,Ord( oZetaProvider.DatosPeriodo.Tipo),oZetaProvider.GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ),EntreComillas( K_GLOBAL_SI ) ] ) );
          iUltPerOrd := FQuery.FieldByName('ULTIMO').AsInteger;
          //A�os anteriores
          if ( ( iPeriodoACalcular ) < 1 )then
          begin
               //obtener el ultimo periodo ordinario
               AbreQuery( Format(qryUltPerOrd,[oZetaProvider.DatosPeriodo.Year - 1,Ord( oZetaProvider.DatosPeriodo.Tipo),oZetaProvider.GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ),EntreComillas( K_GLOBAL_SI ) ] ) );
               iUltPerOrd := FQuery.FieldByName('ULTIMO').AsInteger;

               iPeriodoACalcular := iUltPerOrd + oZetaProvider.DatosPeriodo.Numero + iDeltaNumero;
               iOffSet := -1;
          end
          else if ( ( iPeriodoACalcular ) > iUltPerOrd )then
          begin
               iPeriodoACalcular := Abs ( iUltPerOrd - oZetaProvider.DatosPeriodo.Numero - iDeltaNumero);
               iOffSet := 1;
          end;
          sNumeroSinCalcular := IntToStr( iPeriodoACalcular );
     end
     else
     begin //Montos de concepto de a�o activo
          iPeriodoACalcular := oZetaProvider.DatosPeriodo.Numero + iDeltaNumero;
          sNumeroSinCalcular := 'NOMINA.PE_NUMERO +' + ParamCampoInteger( 1 ); 
     end;
     if ( EsNomina ) then
     begin
          if bCalcula then
             sNumero := IntToStr( iPeriodoACalcular )
          else
              sNumero := sNumeroSinCalcular;

        sTipo := FijoPeriodo( 'PE_TIPO' );
        sYear := '(' + FijoPeriodo( 'PE_YEAR' ) + '+' + IntToStr( iOffSet ) + ')';
     end
     else
       with oZetaProvider.DatosPeriodo do
       begin
            sNumero := IntToStr( iPeriodoACalcular );
            sTipo   := IntToStr( Ord( Tipo ) );
            sYear   := IntToStr( Year + iOffSet );
       end;
     SetEmpleado(2);
end;

{ ************* TZetaCONCEPTORANGO ***********}

type
  TZetaCONCEPTORANGO = class( TZetaRegresaQuery )
  private
    function EsNomina: Boolean;
    function ParamPeriodo(const iParam: Integer;
      const sCampo: String): String;
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function GetScript : String; override;
  protected
    bLista, bCalcula,lContinua: Boolean;
    sDeltaIni, sDeltaFin, sEmpleado, sTipo, sYear : String;
    procedure SetEmpleado;
    procedure SetDatos; virtual;
  end;

function  TZetaCONCEPTORANGO.GetScript : String;
begin
{$ifdef INTERBASE}
     if bLista then
        Result := 'select sum(( '+SelRes('SP_CS( CONCEPTO.CO_NUMERO, %s, %s, %s, %s , %s ))) from CONCEPTO where CO_NUMERO in (%s)')
     else
        Result := SelRes('SP_CS( %s, %s, %s, %s, %s , %s)');
{$endif}
{$ifdef MSSQL}
     if bLista then
        Result := 'select COALESCE( sum( MOVIMIEN.MO_PERCEPC + MOVIMIEN.MO_DEDUCCI ), 0.0 ) as RESULTADO ' +
                  'from MOVIMIEN where ( MOVIMIEN.CB_CODIGO = %4:s ) and ( MOVIMIEN.PE_YEAR = %3:s ) ' +
                  'and ( MOVIMIEN.PE_TIPO = %2:s ) and ( MOVIMIEN.PE_NUMERO BETWEEN %0:s and %1:s ) and ( MOVIMIEN.CO_NUMERO IN (%5:s) ) and ' +
                  '( MOVIMIEN.MO_ACTIVO = ''S'' )'
     else
        Result := 'SELECT RESULTADO = DBO.SP_CS(%0:s,%1:s,%2:s,%3:s,%4:s,%5:s)';
{$endif}
end;

function TZetaCONCEPTORANGO.EsNomina: Boolean;
begin
    Result := ( Evaluador is TZetaEvaluador ) and  ( TZetaEvaluador( Evaluador ).Entidad in NomParamEntidades );
end;

procedure TZetaCONCEPTORANGO.SetEmpleado;
begin
     if bCalcula then
        sEmpleado := IntToStr( DefaultInteger( 5, ZetaEvaluador.GetEmpleado ))
     else
     begin
          if ParamVacio(5) then
          begin
             if EsNomina then
                sEmpleado := 'NOMINA.CB_CODIGO'
             else
                sEmpleado := 'COLABORA.CB_CODIGO';
          end
          else
             sEmpleado := ParamCampoInteger(5);
     end;
end;

function TZetaCONCEPTORANGO.ParamPeriodo(const iParam: Integer; const sCampo : String): String;
begin
    if bCalcula then
        Result := IntToStr( DefaultInteger( iParam, DataSetEvaluador.FieldByName( sCampo ).AsInteger ))
    else if ParamVacio( iParam ) then
        Result := 'NOMINA.' + sCampo
    else
        Result := ParamCampoInteger( iParam );
end;


procedure TZetaCONCEPTORANGO.SetDatos;
begin
     sDeltaIni := ParamCampoInteger(1);
     sDeltaFin := ParamCampoInteger(2);

     if ( EsNomina ) then
     begin
        sTipo   := ParamPeriodo( 3, 'PE_TIPO' );
        sYear   := ParamPeriodo( 4, 'PE_YEAR' );
     end
     else
       with oZetaProvider.DatosPeriodo do
       begin
            sTipo   := ParamCampoDefInteger( 3, Ord( Tipo ) );
            sYear   := ParamCampoDefInteger( 4, Year );
       end;

     SetEmpleado;
     //Continuar sin brincar de a�o
     lContinua := TRUE;
end;

procedure TZetaCONCEPTORANGO.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sLista : String;
begin
     bCalcula:= FALSE;
     ParametrosFijos( 3 );
     oZetaProvider.GetEncabPeriodo;
     if ( EsNomina ) then
        AgregaRequerido( 'NOMINA.CB_CODIGO', enNomina )
     else
        AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     TipoRegresa := resDouble;
     bLista := ( Argument( 0 ).Kind <> resInt );
     if not ParametrosConstantes then
     begin
        // Los requiere el CALCULATE
        if ( EsNomina ) then
        begin
           AgregaRequerido( 'NOMINA.PE_YEAR', enNomina );
           AgregaRequerido( 'NOMINA.PE_TIPO', enNomina );
           AgregaRequerido( 'NOMINA.PE_NUMERO', enNomina );
        end;
     	Exit;
     end;
     //Parametros son constantes
     if( oZetaProvider.DatosPeriodo.Numero + ParamInteger( 1 ) >  oZetaProvider.DatosPeriodo.Numero + ParamInteger( 2 ) )then
     begin
           ErrorFuncion('Error en par�metros','El periodo final no puede ser menor al periodo inicial')
     end
     else
     begin
          SetDatos;
          // para y que se vaya al calculate de CSR a�os y lograr una intersecci�n entre los a�os
          if ( lContinua )then
          begin

               if bLista then
               begin
                    if ParamEsCampo( 0 ) then
                        sLista := ParamNombreCampo( 0 )
                    else
                    begin
                         sLista := ParamString( 0 );
                         if NOT ValidaListaNumeros( sLista ) then
                            ErrorParametro( 'La Lista de Conceptos no es V�lida en el Par�metro #1', VACIO );
                    end;
                    AgregaScript( Format( GetScript, [ sDeltaIni,   // Inicial
                                                       sDeltaFin,   // Final
                                                       sTipo, // Tipo
                                                       sYear,      // Year
                                                       sEmpleado, sLista ] ), TipoRegresa, FALSE)
               end
               else
                    AgregaScript( Format( GetScript, [ ParamCampoInteger( 0 ),   // Conceptos
                                                       sDeltaIni,   // Inicial
                                                       sDeltaFin,   // Final
                                                       sTipo, // Tipo
                                                       sYear,
                                                       sEmpleado ] ), TipoRegresa, FALSE);
          end;
     end;
end;

function TZetaCONCEPTORANGO.Calculate: TQREvResult;
begin
     bCalcula:= TRUE;
     bLista := ( Argument( 0 ).Kind <> resInt );
     SetDatos;
     if bLista then
     begin
          if NOT ValidaListaNumeros( ParamString( 0 ) ) then
             ErrorParametro( 'La Lista de Conceptos no es V�lida en el Par�metro #1', VACIO );

          Result := EvaluaQuery( Format( GetScript, [ sDeltaIni,   // Inicial
                                                      sDeltaFin,   // Final
                                                      sTipo, // Tipo
                                                      sYear, //Year
                                                      sEmpleado, //Empleado
                                                      ParamString( 0 ) ]) );

     end
     else
        Result := EvaluaQuery( Format( GetScript, [ IntToStr( ParamInteger( 0 ) ),
                                                    sDeltaIni, // Inicial
                                                    sDeltaFin, // Final
                                                    sTipo, // Tipo
                                                    sYear,
                                                    sEmpleado ]) );
end;

{ ************* TZetaCONCEPTORANGORELATIVO ***********}
//CSR(Concepto,DeltaIni,DeltaFin[,Tipo][,Year ][,nEmpleado], [lBrincaA�o ] )
type
  TZetaCONCEPTORANGORELATIVO = class( TZetaCONCEPTORANGO )
  private
    iOffSet: integer;
  protected
    function GetUltimoPeriodo(const iYear,iTipo:Integer ):Integer;
    procedure SetDatos; override;
    function Calculate: TQREvResult; override;
  end;


function TZetaCONCEPTORANGORELATIVO.GetUltimoPeriodo(const iYear,iTipo:Integer ):Integer;
const
     qryUltPerOrd = 'select max(PE_NUMERO) ULTIMO from PERIODO where PE_YEAR = %0:d and PE_TIPO = %1:d and PE_NUMERO < %2:d AND PE_CAL = %3:s ';
begin
      oZetaProvider.InitGlobales;
      AbreQuery( Format(qryUltPerOrd,[ iYear, iTipo, oZetaProvider.GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ),EntreComillas( K_GLOBAL_SI )  ] ) );
      Result := FQuery.FieldByName('ULTIMO').AsInteger;
end;

procedure TZetaCONCEPTORANGORELATIVO.SetDatos;
var
    iPeriodoActivo,iDeltaIni,iDeltaFin,iUltPerOrd ,iYear: Integer;
    iPerIniAProcesar ,iPerFinAProcesar {,iOffSet}:Integer;
    lBrincarAnio: Boolean;
    sPerIniAProcSinCalc ,sPerFinAProcSinCalc :string;
begin
     iYear := DefaultInteger(4,oZetaProvider.DatosPeriodo.Year);
     lBrincarAnio := DefaultBoolean( 6,FALSE );
     iDeltaIni := ParamInteger( 1 );
     iDeltaFin := ParamInteger( 2 );
     iOffSet := 0;
     iPeriodoActivo := oZetaProvider.DatosPeriodo.Numero;
     iPerIniAProcesar := iPeriodoActivo;
     iPerFinAProcesar := iPeriodoActivo;

     sDeltaIni := ParamCampoInteger( 1 );
     sDeltaFin := ParamCampoInteger( 2 );
     sYear := IntToStr(iYear);
     sTipo := IntToStr( Ord( oZetaProvider.DatosPeriodo.Tipo ) );
     lContinua := True;

     if( ( iPeriodoActivo + iDeltaIni ) >  ( iPeriodoActivo + iDeltaFin ) )then
     begin
           ErrorFuncion('Error en par�metros','El periodo final no puede ser menor al periodo inicial')
     end
     else
     begin
          if ( lBrincarAnio )then
          begin
               //obtener
               iUltPerOrd := GetUltimoPeriodo( iYear,Ord( oZetaProvider.DatosPeriodo.Tipo ) );
               //los dos delta son positivos
               if( ( iPeriodoActivo + iDeltaIni > 0 ) and ( iPeriodoActivo + iDeltaFin > 0 ) )then
               begin
                    // si estan en el a�o activo
                    if( (iPeriodoActivo + iDeltaIni <= iUltPerOrd ) and (iPeriodoActivo + iDeltaFin <= iUltPerOrd ) )then
                    begin
                         //se conserva igual
                         lContinua := true;
                         iPerIniAProcesar := iPeriodoActivo + iDeltaIni;
                         iPerFinAProcesar := iPeriodoActivo + iDeltaFin;
                    end
                    else
                    begin

                         //los dos delta caen en el a�o siguiente
                         if( ( iPeriodoActivo + iDeltaIni > iUltPerOrd ) and (iPeriodoActivo + iDeltaFin > iUltPerOrd ) )then
                         begin
                              iOffSet := 1;
                              //Asignar los periodos inicial y final
                              iPerIniAProcesar := Abs( iUltPerOrd - iPeriodoActivo - iDeltaIni );
                              iPerFinAProcesar := Abs( iUltPerOrd - iPeriodoActivo - iDeltaFin );
                         end
                         else
                         begin //Existe una intersecci�n entre los a�os
                              lContinua := FALSE; //para que no se resuelva en el get requeridos
                              iOffset := 1;
                              if( ( ( iPeriodoActivo + iDeltaIni ) <= iUltPerOrd ) and ( ( iPeriodoActivo + iDeltaFin ) > iUltPerOrd ) )then
                              begin
                                   iPerIniAProcesar := iPeriodoActivo + iDeltaIni;
                                   iPerFinAProcesar := Abs( iUltPerOrd - iPeriodoActivo - iDeltaFin );
                              end
                              else
                              if( ( iPeriodoActivo + iDeltaIni > iUltPerOrd ) and ( iPeriodoActivo + iDeltaFin <= iUltPerOrd ) )then
                              begin
                                   ErrorFuncion('Error en par�metros','El periodo final no puede ser menor al periodo inicial')
                              end;
                         end;
                    end;
               end
               else
               begin
                    //si los dos deltas son negativos caen en el a�o anterior

                    if ( ( iPeriodoActivo + iDeltaIni <= 0 ) and ( iPeriodoActivo + iDeltaFin <= 0 ) )then
                    begin
                          //obtener el ultimo periodo para el a�o anterior
                          iUltPerOrd := GetUltimoPeriodo(iYear -1,Ord( oZetaProvider.DatosPeriodo.Tipo ) );
                          iOffSet := -1;
                          //Asignar los periodos inicial y final
                          iPerIniAProcesar := iUltPerOrd + ( iPeriodoActivo + iDeltaIni );
                          iPerFinAProcesar := iUltPerOrd + ( iPeriodoActivo + iDeltaFin );
                    end
                    else // si existe intersecci�n entre a�os
                    begin
                         if( ( iPeriodoActivo + iDeltaIni <= 0 ) and ( iPeriodoActivo + iDeltaFin > 0 ) )then
                         begin
                              lContinua := FALSE; //para que no se resuelva en el get requeridos
                              iOffset := -1;
                              iPerIniAProcesar := iUltPerOrd + ( iPeriodoActivo + iDeltaIni );
                              iPerFinAProcesar := iPeriodoActivo + iDeltaFin;
                         end
                         else
                         if( ( iPeriodoActivo + iDeltaIni > 0 ) and ( iPeriodoActivo + iDeltaFin < 0 ) )then
                         begin
                              ErrorFuncion('Error en par�metros','El periodo final no puede ser menor al periodo inicial')
                         end;
                    end;
               end;
               sPerIniAProcSinCalc := IntToStr( iPerIniAProcesar );
               sPerFinAProcSinCalc := IntToStr( iPerFinAProcesar );
          end
          else
          begin //funcionalidad sin brincar Anios
               iPerIniAProcesar := iPeriodoActivo + iDeltaIni;
               iPerFinAProcesar := iPeriodoActivo + iDeltaFin;

               sPerIniAProcSinCalc := 'NOMINA.PE_NUMERO +' + ParamCampoInteger( 1 );
               sPerFinAProcSinCalc := 'NOMINA.PE_NUMERO +' + ParamCampoInteger( 2 );
          end;
     end;

     if ( EsNomina ) then
     begin
        if bCalcula then
        begin
            sDeltaIni := IntToStr( iPerIniAProcesar );
            sDeltaFin := IntToStr( iPerFinAProcesar );
        end
        else
        begin
            sDeltaIni :=  sPerIniAProcSinCalc;
            sDeltaFin :=  sPerFinAProcSinCalc;
        end
     end
     else
     begin
            sDeltaIni := IntToStr( iPerIniAProcesar );
            sDeltaFin := IntToStr( iPerFinAProcesar );
     end;
     SetEmpleado;
     sYear := sYear + '+' + IntToStr( iOffSet );
end;

function TZetaCONCEPTORANGORELATIVO.Calculate: TQREvResult;
var
   iUltPerOrd,iYear: Integer;
   sDeltaSigPer :string;
   sAnioAnterior :string;
begin
     bCalcula:= TRUE;
     SetDatos;
     if lContinua or (  not DefaultBoolean( 6,FALSE ) )then
     begin
          Result := inherited Calculate;
     end
     else
     begin
          Result.Kind := ResDouble;
          bLista := ( Argument( 0 ).Kind <> resInt );
          iYear := DefaultInteger(4,oZetaProvider.DatosPeriodo.Year);
          //Obtener el ultimo periodo del an�o anterior
          iUltPerOrd := GetUltimoPeriodo( iYear - 1,Ord( oZetaProvider.DatosPeriodo.Tipo ) );

          //determinar el delta Inicial , y final
          sDeltaSigPer := sDeltaFin;
          sDeltaFin := IntToStr( iUltPerOrd );

          sAnioAnterior := sYear + '-1';
          if bLista then
          begin
               if NOT ValidaListaNumeros( ParamString( 0 ) ) then
                  ErrorParametro( 'La Lista de Conceptos no es V�lida en el Par�metro #1', VACIO );

               //Obtener el primer monto del rango de periodos

               //Intersecci�n con el a�o anterior
               AbreQuery(Format( GetScript, [ sDeltaIni, // Inicial
                                                           sDeltaFin,   // Final
                                                           sTipo, // Tipo
                                                           IntToStr( iMin( iYear, iYear + iOffset ) ), //Year
                                                           sEmpleado, //Empleado
                                                           ParamString( 0 ) ]));
               Result.DblResult := FQuery.Fields[0].AsFloat;

               //ANIO SIGUIENTE
               //determinar el delta Inicial , y final
               sDeltaFin := sDeltaSigPer ;
               sDeltaIni := '1';
               AbreQuery(Format( GetScript, [ sDeltaIni,   // Inicial
                                                          sDeltaFin,   // Final
                                                          sTipo, // Tipo
                                                          IntToStr( iMax( iYear, iYear + iOffset ) ), //Year
                                                          sEmpleado, //Empleado
                                                          ParamString( 0 ) ]));

               Result.DblResult := Result.DblResult + FQuery.Fields[0].AsFloat;
         end
         else
         begin
              //Intersecci�n con el a�o anterior
              AbreQuery( Format( GetScript, [ IntToStr( ParamInteger( 0 ) ),
                                                       sDeltaIni, // Inicial
                                                       sDeltaFin, // Final
                                                       sTipo, // Tipo
                                                       IntToStr( iMin( iYear, iYear + iOffset ) ),
                                                       sEmpleado ]) );
              Result.DblResult := FQuery.Fields[0].AsFloat;
              //ANIO SIGUIENTE
              //determinar el delta Inicial , y final
              sDeltaFin := sDeltaSigPer ;
              sDeltaIni := '1';
              AbreQuery( Format( GetScript, [ IntToStr( ParamInteger( 0 ) ),
                                                       sDeltaIni, // Inicial
                                                       sDeltaFin, // Final
                                                       sTipo, // Tipo
                                                       IntToStr( iMax( iYear, iYear + iOffset ) ),
                                                       sEmpleado ]) );
              Result.DblResult := Result.DblResult + FQuery.Fields[0].AsFloat;
         end;

     end;
end;

{ ************* TZetaCONCEPTOREFERENCIA ***********}

type
  TZetaCONCEPTOREFERENCIA = class( TZetaRegresaQuery )
  private
    function EsNomina: Boolean;
    procedure SetEmpleado( const iParam : integer );
    procedure SetDatos;
    function ParamPeriodo(const iParam: Integer;
      const sCampo: String): String;
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function GetScript : String; override;
  protected
    bLista, bCalcula : Boolean;
    sEmpleado, sYear, sTipo, sNumero : String;
  end;

function  TZetaCONCEPTOREFERENCIA.GetScript : String;
begin
{$ifdef INTERBASE}
     if bLista then
        Result := 'select sum(( '+SelRes('SP_CM( CONCEPTO.CO_NUMERO, %s, %s, %s, %s , %s ))) from CONCEPTO where CO_NUMERO in (%s)')
     else
        Result := SelRes('SP_CM( %s, %s, %s, %s, %s , %s)');
{$endif}
{$ifdef MSSQL}
     if bLista then
        Result := 'select COALESCE( sum( MOVIMIEN.MO_PERCEPC + MOVIMIEN.MO_DEDUCCI ), 0.0 ) as RESULTADO ' +
                  'from MOVIMIEN where ( MOVIMIEN.CB_CODIGO = %4:s ) and ( MOVIMIEN.PE_YEAR = %3:s ) ' +
                  'and ( MOVIMIEN.PE_TIPO = %2:s ) and ( MOVIMIEN.PE_NUMERO = %1:s ) and ( MOVIMIEN.CO_NUMERO IN (%5:s) ) and ' +
                  '( MOVIMIEN.MO_REFEREN = %0:s ) and ( MOVIMIEN.MO_ACTIVO = ''S'' )'
     else
        Result := 'SELECT RESULTADO = DBO.SP_CM(%0:s,%1:s,%2:s,%3:s,%4:s,%5:s)';
{$endif}
end;

function TZetaCONCEPTOREFERENCIA.EsNomina: Boolean;
begin
    Result := ( Evaluador is TZetaEvaluador ) and  ( TZetaEvaluador( Evaluador ).Entidad in NomParamEntidades );
end;

procedure TZetaCONCEPTOREFERENCIA.SetEmpleado( const iParam : integer );
begin
     if bCalcula then
        sEmpleado := IntToStr( DefaultInteger( iParam, ZetaEvaluador.GetEmpleado ))
     else
     begin
          if ParamVacio(iParam) then
          begin
             if EsNomina then
                sEmpleado := 'NOMINA.CB_CODIGO'
             else
                sEmpleado := 'COLABORA.CB_CODIGO';
          end
          else
             sEmpleado := ParamCampoInteger(iParam);
     end;
end;

function TZetaCONCEPTOREFERENCIA.ParamPeriodo(const iParam: Integer; const sCampo : String): String;
begin
    if bCalcula then
        Result := IntToStr( DefaultInteger( iParam, DataSetEvaluador.FieldByName( sCampo ).AsInteger ))
    else if ParamVacio( iParam ) then
        Result := 'NOMINA.' + sCampo
    else
        Result := ParamCampoInteger( iParam );
end;

procedure TZetaCONCEPTOREFERENCIA.SetDatos;
begin
     if ( EsNomina ) then
     begin
        sNumero := ParamPeriodo( 2, 'PE_NUMERO' );
        sTipo   := ParamPeriodo( 3, 'PE_TIPO' );
        sYear   := ParamPeriodo( 4, 'PE_YEAR' );
     end
     else
       with oZetaProvider.DatosPeriodo do
       begin
            sNumero := ParamCampoDefInteger( 2, Numero );
            sTipo   := ParamCampoDefInteger( 3, Ord( Tipo ) );
            sYear   := ParamCampoDefInteger( 4, Year );
       end;
     SetEmpleado(5);
end;


procedure TZetaCONCEPTOREFERENCIA.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sLista : String;
begin
     TipoRegresa := resDouble;
     bCalcula := FALSE;

     if ( EsNomina ) then
        AgregaRequerido( 'NOMINA.CB_CODIGO', enNomina )
     else
        AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     oZetaProvider.GetEncabPeriodo;
     ParametrosFijos( 1 );

     bLista := ( Argument( 0 ).Kind <> resInt );
     if not ParametrosConstantes then
     begin
        // Los requiere el CALCULATE
        if ( EsNomina ) then
        begin
           AgregaRequerido( 'NOMINA.PE_YEAR', enNomina );
           AgregaRequerido( 'NOMINA.PE_TIPO', enNomina );
           AgregaRequerido( 'NOMINA.PE_NUMERO', enNomina );
        end;
     	Exit;
     end;
     SetDatos;
     if bLista then
     begin
          if ParamEsCampo( 0 ) then
              sLista := ParamNombreCampo( 0 )
          else
          begin
               sLista := ParamString( 0 );
               if NOT ValidaListaNumeros( sLista ) then
                  ErrorParametro( 'La Lista de Conceptos no es V�lida en el Par�metro #1', VACIO );
          end;
          AgregaScript( Format( GetScript, [ ParamCampoDefString( 1 , EntreComillas( VACIO )),       // Referencia
                                             sNumero,     // Numero
                                             sTipo,  // Tipo
                                             sYear,      // Year
                                             sEmpleado,
                                             sLista ] ), TipoRegresa, FALSE )

     end
     else
          AgregaScript( Format( GetScript, [ ParamCampoInteger( 0 ),   // Conceptos
                                             ParamCampoDefString( 1 , EntreComillas( VACIO )) ,       // Referencia
                                             sNumero,     // Numero
                                             sTipo,  // Tipo
                                             sYear,      // Year
                                             sEmpleado ] ), TipoRegresa, FALSE );
end;

function TZetaCONCEPTOREFERENCIA.Calculate: TQREvResult;
begin
     bLista := ( Argument( 0 ).Kind <> resInt );
     bCalcula := TRUE;
     SetDatos;
     if bLista then
     begin
          if NOT ValidaListaNumeros( ParamString( 0 ) ) then
             ErrorParametro( 'La Lista de Conceptos no es V�lida en el Par�metro #1', VACIO );

          Result := EvaluaQuery( Format( GetScript, [ EntreComillas( DefaultString( 1 , '') ),       // Referencia
                                                    sNumero,     // Numero
                                                    sTipo,  // Tipo
                                                    sYear,      // Year
                                                    sEmpleado,
                                                    ParamString( 0 ) ]) )
     end
     else
        Result := EvaluaQuery( Format( GetScript, [ IntToStr( ParamInteger( 0 ) ),
                                                    EntreComillas( DefaultString( 1 , '') ),       // Referencia
                                                    sNumero,     // Numero
                                                    sTipo,  // Tipo
                                                    sYear,      // Year
                                                    sEmpleado ]) );
end;


{ ************* TZetaCONCEPTOREFERENCIASUMA ***********}

{CMS (Concepto [, sReferencia] [,NumeroInicial] [,NumeroFinal] [, Tipo] [, A�o] [,nEmpleado] )}
type
  TZetaCONCEPTOREFERENCIASUMA = class( TZetaCONCEPTOREFERENCIA )
  private
    procedure SetDatos;
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function GetScript : String; override;
  protected
    sNumeroInicial, sNumeroFinal : string;
end;

function  TZetaCONCEPTOREFERENCIASUMA.GetScript : String;
begin
{$ifdef INTERBASE}
     if bLista then
        Result := 'select sum(( '+SelRes('SP_CMS( CONCEPTO.CO_NUMERO, %1:s, %2:s, %3:s, %4:s, %5:s , %6:s ))) from CONCEPTO where CO_NUMERO in (%0:s)')
     else
        Result := SelRes('SP_CMS( %s, %s, %s, %s, %s, %s , %s)');
        {$endif}
{$ifdef MSSQL}
     { dbo.SP_CMS( 0-@CONCEPTO,
     		           1-@REFERENCIA,
		               2-@NUMEROINICIAL,
		               3-@NUMEROFINAL,
		               4-@TIPO,
		               5-@ANIO,
		               6-@EMPLEADO    )}
     if bLista then
        Result := 'select COALESCE( sum( MOVIMIEN.MO_PERCEPC + MOVIMIEN.MO_DEDUCCI ), 0.0 ) as RESULTADO ' +
                  'from MOVIMIEN where ( MOVIMIEN.CB_CODIGO = %6:s ) and ( MOVIMIEN.PE_YEAR = %5:s ) ' +
                  'and ( MOVIMIEN.PE_TIPO = %4:s ) and ( MOVIMIEN.PE_NUMERO between %2:s and %3:s ) ' +
                  'and ( MOVIMIEN.CO_NUMERO IN (%0:s) ) and ' +
                  '( MOVIMIEN.MO_REFEREN = %1:s ) and ( MOVIMIEN.MO_ACTIVO = ''S'' )'
     else
        Result := 'SELECT RESULTADO = DBO.SP_CMS(%0:s,%1:s,%2:s,%3:s,%4:s,%5:s,%6:s)';
{$endif}
end;


procedure TZetaCONCEPTOREFERENCIASUMA.SetDatos;
begin
     {CMS(0:Concepto [, 1:sReferencia] [,2:NumeroInicial] [,3:NumeroFinal] [, 4:Tipo] [, 5:A�o] [,6:nEmpleado] )}
     if ( EsNomina ) then
     begin
          sNumeroInicial := ParamPeriodo( 2, 'PE_NUMERO' );
          sNumeroFinal   := ParamPeriodo( 3, 'PE_NUMERO' );
          sTipo   := ParamPeriodo( 4, 'PE_TIPO' );
          sYear   := ParamPeriodo( 5, 'PE_YEAR' );
     end
     else
       with oZetaProvider.DatosPeriodo do
       begin
            sNumeroInicial := ParamCampoDefInteger( 2, Numero );
            sNumeroFinal   := ParamCampoDefInteger( 3, Numero );
            sTipo   := ParamCampoDefInteger( 4, Ord( Tipo ) );
            sYear   := ParamCampoDefInteger( 5, Year );
       end;
     SetEmpleado(6);
end;


procedure TZetaCONCEPTOREFERENCIASUMA.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sLista : String;
begin
     TipoRegresa := resDouble;
     bCalcula := FALSE;

     if ( EsNomina ) then
        AgregaRequerido( 'NOMINA.CB_CODIGO', enNomina )
     else
        AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     oZetaProvider.GetEncabPeriodo;
     ParametrosFijos( 1 );

     bLista := ( Argument( 0 ).Kind <> resInt );
     if not ParametrosConstantes then
     begin
        // Los requiere el CALCULATE
        if ( EsNomina ) then
        begin
           AgregaRequerido( 'NOMINA.PE_YEAR', enNomina );
           AgregaRequerido( 'NOMINA.PE_TIPO', enNomina );
           AgregaRequerido( 'NOMINA.PE_NUMERO', enNomina );
        end;
     	Exit;
     end;
     SetDatos;
     if bLista then
     begin
          if ParamEsCampo( 0 ) then
              sLista := ParamNombreCampo( 0 )
          else
          begin
               sLista := ParamString( 0 );
               if NOT ValidaListaNumeros( sLista ) then
                  ErrorParametro( 'La Lista de Conceptos no es V�lida en el Par�metro #1', VACIO );
          end;
          AgregaScript( Format( GetScript, [ sLista,                                       //Conceptos
                                             ParamCampoDefString( 1 , EntreComillas( VACIO )),// Referencia
                                             sNumeroInicial,                               // NumeroInicial
                                             sNumeroFinal,                                 // NumeroFinal
                                             sTipo,                                        // Tipo
                                             sYear,                                        // Year
                                             sEmpleado] ), TipoRegresa, FALSE )            // Empleado




     end
     else
          AgregaScript( Format( GetScript, [ ParamCampoInteger( 0 ),                                  // Conceptos
                                             ParamCampoDefString( 1 , EntreComillas( VACIO )) ,       // Referencia
                                             sNumeroInicial,                                          // NumeroInicial
                                             sNumeroFinal,                                            // NumeroFinal
                                             sTipo,                                                   // Tipo
                                             sYear,                                                   // Year
                                             sEmpleado ] ), TipoRegresa, FALSE );
end;

function TZetaCONCEPTOREFERENCIASUMA.Calculate: TQREvResult;
begin
     bLista := ( Argument( 0 ).Kind <> resInt );
     bCalcula := TRUE;
     SetDatos;
     if bLista then
     begin
          if NOT ValidaListaNumeros( ParamString( 0 ) ) then
             ErrorParametro( 'La Lista de Conceptos no es V�lida en el Par�metro #1', VACIO );

          Result := EvaluaQuery( Format( GetScript, [ ParamString( 0 ),                             //Conceptos
                                                      EntreComillas( DefaultString( 1 , '') ),      // Referencia
                                                      sNumeroInicial,                               // NumeroInicial
                                                      sNumeroFinal,                                 // NumeroFinal
                                                      sTipo,                                        // Tipo
                                                      sYear,                                        // Year
                                                      sEmpleado ]) )                                // Empleado

     end
     else
        Result := EvaluaQuery( Format( GetScript, [ IntToStr( ParamInteger( 0 ) ),
                                                    EntreComillas( DefaultString( 1 , '') ),      // Referencia
                                                    sNumeroInicial,                               // NumeroInicial
                                                    sNumeroFinal,                                 // NumeroFinal
                                                    sTipo,                                        // Tipo
                                                    sYear,                                        // Year
                                                    sEmpleado ]) );
end;

{ *************** TZetaTBase  ************** }

type
  TZetaTBase = class( TZetaRegresaQuery )
  public
    { Public Declarations }
    function  Calculate: TQREvResult; override;
    function  GetScript : String; override;
    procedure CaseCampo( const nTabla : Integer; iRegresa : Integer );
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  protected
    sTabla, sCampoCodigo, sCampo : String;
    iTabla, iRegresa : Integer;
    procedure SetDatos; virtual;
  end;

function TZetaTBase.GetScript : string;
begin
     Result := 'select %s from %s where %s = %s';
end;

procedure TZetaTBase.SetDatos; { Este aplica para T(), las demas haran un override }
begin
     if ( Argument( 0 ).Kind = resInt ) then
        iTabla := ParamInteger( 0 )
     else
        iTabla := StrToInt( ParamString( 0 ));
     iRegresa := DefaultInteger( 2, 1 );
end;

procedure TZetaTBase.CaseCampo( const nTabla : Integer; iRegresa : Integer );
begin
     sCampoCodigo := 'TB_CODIGO';     // Mayor�a de las Tablas
     case iRegresa of
          1 : sCampo := 'TB_ELEMENT';
          2 : sCampo := 'TB_NUMERO';
          3 : sCampo := 'TB_TEXTO';
          4 : sCampo := 'TB_INGLES';
     else
          ErrorFuncion( 'Error en Par�metro', 'Solo se Permiten Valores 1 a 4' );
     end;
     case nTabla of
          1 : sTabla := 'EDOCIVIL';
          2 : sTabla := 'VIVE_EN';
          3 : sTabla := 'VIVE_CON';
          4 : sTabla := 'TRANSPOR';
          5 : sTabla := 'ESTUDIOS';
          6 : begin {AL SOP-4479}
                   sTabla := 'NUMERICA';
                   if iRegresa = 1 then
                      sCampo := 'NU_DESCRIP'
                   else
                        sCampo := 'NU' + Copy( sCampo, 3, 10 );
                   sCampoCodigo := 'NU_CODIGO';
                   //Delete( sCampo, 1, 2 );
                   //Insert( 'NU', sCampo, 1 );
              end;
          7 : begin {AL SOP-4479}
                     sTabla := 'HORARIO';
                     if iRegresa = 1 then
                        sCampo := 'HO_DESCRIP'
                     else
                          sCampo := 'HO' + Copy( sCampo, 3, 10 );
                     sCampoCodigo := 'HO_CODIGO'
                end;
           8 : begin
                    sTabla := 'TURNO';
                     if iRegresa = 1 then
                        sCampo := 'TU_DESCRIP'
                     else
                        sCampo := 'TU' + Copy( sCampo, 3, 10 );
                        //begin
                        //     Delete( sCampo, 1, 2 );
                        //     Insert( 'TU', sCampo, 1 );
                        //end;
                     sCampoCodigo := 'TU_CODIGO';
               end;
          9 : sTabla := 'CONTRATO';
          10 : sTabla := 'MOT_BAJA';
          12 : sTabla := 'INCIDEN';
          13 : sTabla := 'CLASIFI';
          14 : begin
                     sTabla := 'PUESTO';
                     if iRegresa = 1 then
                        sCampo := 'PU_DESCRIP'
                     else
                     begin
                          sCampo := 'PU' + Copy( sCampo, 3, 10 );
                          //Delete( sCampo, 1, 2 );
                          //Insert( 'PU', sCampo, 1 );
                     end;
                     sCampoCodigo := 'PU_CODIGO'
                end;
          15 : sTabla := 'OTRASPER';
          16 : sTabla := 'TKARDEX';
          17 : sTabla := 'SSOCIAL';
          18 : sTabla := 'MONEDA';
          21..29 : sTabla := 'NIVEL' + IntToStr( nTabla - 20 );
          31..38 : sTabla := 'EXTRA' + IntToStr( nTabla - 30 ); {AL SOP-4479}
          39 : sTabla := 'TCURSO';
          40 : sTabla := 'TAHORRO';
          44 : sTabla := 'ENTIDAD';
          46 : sTabla := 'RPATRON';
          47..52 : sTabla := 'EXTRA' + IntToStr( nTabla - 38 ); {AL SOP-4479}
          {$ifdef ACS}
          110..112 : sTabla := 'NIVEL' + IntToStr( nTabla - 100 );   //MV:(23/Oct/2008) Cambios de Niveles ACS
          {$endif}
     end;
end;

procedure TZetaTBase.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     if not ParametrosConstantes then
        Exit;
     SetDatos;
     CaseCampo( iTabla ,iRegresa );
     AgregaScript( Format( GetScript,[ sCampo, sTabla, sCampoCodigo,
                                       UpperCase( ParamCampoString( 1 ) ) ] ), // ValorCodigo
                                       TipoRegresa , FALSE);
                                       UpperCase( ParamCampoString(1) )
end;

function TZetaTBase.Calculate: TQREvResult;
begin
     SetDatos;
     CaseCampo( iTabla, iRegresa );
     Result := EvaluaQuery( Format( GetScript, [ sCampo, sTabla, sCampoCodigo,
                                                 EntreComillas( UpperCase( ParamString( 1 ) ) ) ] ) );
end;

{ *************** TZetaTABLAGENERICO ************** }

type
  TZetaTABLAGENERICO = class( TZetaTBase )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  protected
    procedure SetDatos; override;
  end;

procedure TZetaTABLAGENERICO.SetDatos;
begin
     iTabla := ParamInteger( 2 );
     if ( UpperCase( ParamString( 0 ) ) = 'F' ) then
         iRegresa := 1
     else
         iRegresa := 4;
end;

procedure TZetaTABLAGENERICO.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     // TABLA_GENERICO( cRegresa, cCodigo, nTabla )
     TipoRegresa := resString;
     ParametrosFijos( 3 );
     inherited GetRequeridos( TipoRegresa );
end;

{*********************** TZetaTG *********************}
type
  TZetaTG = class( TZetaTBase )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  protected
    procedure SetDatos; override;
  end;

procedure TZetaTG.SetDatos;
begin
     iTabla :=  ParamInteger( 0 );
     if DefaultBoolean( 2, True ) then
        iRegresa := 1
     else
        iRegresa := 4;
end;

procedure TZetaTG.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     // TG( tabla, codigo [, lDescrip ] )
     TipoRegresa := resString;
     ParametrosFijos( 2 );
     inherited GetRequeridos( TipoRegresa );
end;

{*********************** TZetaT *********************}

type
  TZetaT = class( TZetaTBase )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaT.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     // T( nTabla, sCodigo [, nRegresa  ] )
     if DefaultInteger( 2, 1 ) = 2 then
         TipoRegresa := resDouble
     else
         TipoRegresa := resString;
     ParametrosFijos( 2 );
     inherited GetRequeridos( TipoRegresa );
end;

{*********************** TZetaTURNOFECHA *********************}

type
  TZetaTURNOFECHA = class( TZetaRegresaQuery )
  public
    function GetScript : String; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function  TZetaTURNOFECHA.GetScript : String;
begin
{$ifdef INTERBASE}
     Result := 'select CB_TURNO from SP_FECHA_KARDEX( %s, %s )';
{$else}
     Result := 'select CB_TURNO = dbo.SP_KARDEX_CB_TURNO( %s, %s )';
{$endif}
     // SP_FECHA_KARDEX( Fecha, empleado )
end;

procedure TZetaTURNOFECHA.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resString;
     ParametrosFijos( 1 );
     if ParametrosConstantes then
        AgregaScript( Format( GetScript, [ ParamCampoFecha( 0 ),
                                           'COLABORA.CB_CODIGO' ] ), TipoRegresa , FALSE)
     else
        AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

function TZetaTURNOFECHA.Calculate: TQREvResult;
begin
     Result := EvaluaQuery( Format( GetScript, [ EntreComillas( DateToStrSQL( ParamDate( 0 ) ) ),
                                                 IntToStr( ZetaEvaluador.GetEmpleado ) ] ) );
end;

{ ******************* TZetaCALCONSEC **************************}

type
  TZetaCALCONSEC = class( TZetaCALBASE )
  protected
    FRegresa: Integer;
    sExpresion, {sFiltro,} sFiltroTipoDia: String;
    function GetResultado( const sResultado, sFiltro: string; const dInicio, dFinal : TDate ): TQREvResult;
    procedure PreparaAgente; override;
    function GetFiltroTipoDia(eTipoDia:eTipoDiaAusencia):string;virtual;
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaCALCONSEC.PreparaAgente;
begin
     inherited;
     with FAgente do
     begin
          if ( sExpresion <> VACIO ) then
             AgregaFiltro( sExpresion, FALSE, enAusencia );  // el Filtro se Agrega en el PreparaSuperEv
          if ( sFiltroTipoDia <> VACIO ) then
             AgregaFiltro( sFiltroTipoDia, TRUE, enAusencia );
          AgregaOrden( 'AUSENCIA.AU_FECHA', TRUE, enAusencia );
     end;
end;

procedure TZetaCALCONSEC.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resInt;
     ParametrosFijos( 1 );
     oZetaProvider.GetDatosPeriodo;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     {$ifdef CAMBIO_TNOM}
     if ( EsEntidadNomina ) then
     begin
          AgregaRequerido( 'NOMINA.NO_ASI_INI', enEmpleado );
          AgregaRequerido( 'NOMINA.NO_ASI_FIN', enEmpleado );
     end;
     {$endif}

end;

function TZetaCALCONSEC.GetResultado( const sResultado, sFiltro: string; const dInicio, dFinal : TDate ): TQREvResult;
var
   iCuantos, iMaximo : integer;
   dAnterior, dAusencia: TDate;
begin
     PreparaSuperEv( sResultado, sFiltro, enAusencia, [tgFecha]);  // La Expresion se agrega en el PreparaAgente
     CierraDataSet;
     SetParamValue( 'Empleado', tgNumero, ZetaEvaluador.GetEmpleado );
     SetParamValue( 'FechaIni', tgFecha, dInicio );
     SetParamValue( 'FechaFin', tgFecha, dFinal );
     AbreDataSet;

     with SuperEv.DataSet do
     begin
          dAnterior := Fields[0].AsDateTime-1;
          iCuantos := 0;
          iMaximo := 0;
          while not EOF do
          begin
               dAusencia:= Fields[0].AsDateTime;
               if ( dAnterior + 1 <> dAusencia ) then
                  iCuantos:= 0;
               iCuantos:= iCuantos + 1;
               iMaximo:= iMax( iCuantos, iMaximo );
               dAnterior:= dAusencia;
               Next;
          end;
     end;

     with Result do
     begin
          Kind := resInt;
          intResult := iMaximo;
     end;
end;

function TZetaCALCONSEC.Calculate: TQREvResult;
var
   iTipoDia : Integer;
begin
     InitDatosCalendario;
     GetFechasCalendario(4);
     sFiltroTipoDia:= VACIO;
     iTipoDia:= DefaultInteger( 3, -1 );
     if iTipoDia >= 0 then
        sFiltroTipoDia:= GetFiltroTipoDia( eTipoDiaAusencia( iTipoDia ) );
     sExpresion:= ParamString(0);

     with oZetaProvider.DatosCalendario do
     begin
          {$ifdef CAMBIO_TNOM}
          if ( EsEntidadNomina ) then
          begin
               dCalInicial:= DefaultDate( 1, DataSetEvaluador.FieldByName('NO_ASI_INI').AsDateTime );
               dCalFinal:= DefaultDate( 2,DataSetEvaluador.FieldByName('NO_ASI_FIN').AsDateTime );
          end;
          {$endif}
          Result := GetResultado( 'AU_FECHA', CalFiltro, dCalInicial, dCalFinal );
     end;
end;

function TZetaCALCONSEC.GetFiltroTipoDia(eTipoDia:eTipoDiaAusencia):string;
begin
     Result := 'AUSENCIA.AU_TIPODIA=' + IntToStr( Ord( eTipoDia ) );
end;

{ ******************* TZetaFALTASCONSEC **************************}

type
  TZetaFALTASCONSEC = class( TZetaCALCONSEC )
  protected
    procedure PreparaAgente; override;
    procedure AgregaFiltroFI;virtual;
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;
procedure TZetaFALTASCONSEC.AgregaFiltroFI;
begin
     FAgente.AgregaFiltro( 'AUSENCIA.AU_TIPO = ''FI''', TRUE, enAusencia );  // el Filtro se Agrega en el PreparaSuperEv
end;

procedure TZetaFALTASCONSEC.PreparaAgente;
begin
     inherited;
     AgregaFiltroFI;
end;

procedure TZetaFALTASCONSEC.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resInt;
{$ifdef ANTES}
     ParametrosFijos( 2 );
{$endif}
     oZetaProvider.GetDatosPeriodo;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
{$ifdef CAMBIO_TNOM}
     if ( EsEntidadNomina ) then
     begin
          AgregaRequerido( 'NOMINA.NO_ASI_INI', enNomina );
          AgregaRequerido( 'NOMINA.NO_ASI_FIN', enNomina );
     end;
{$endif}
end;

function TZetaFALTASCONSEC.Calculate: TQREvResult;
var
   iTipoDia : Integer;
begin
     sExpresion:= VACIO;
     //sFiltro := DefaultString( 3, VACIO );
     sFiltroTipoDia:= VACIO;
     iTipoDia:= DefaultInteger( 2, -1 );
     if iTipoDia >= 0 then
        sFiltroTipoDia:= 'AUSENCIA.AU_TIPODIA=' + IntToStr( iTipoDia );
     {$ifdef CAMBIO_TNOM}
     if ( EsEntidadNomina ) then
     begin
          Result := GetResultado( 'AU_FECHA', DefaultString( 3, VACIO ), DefaultDate( 0, DataSetEvaluador.FieldByName('NO_ASI_INI').AsDateTime ),
                                                                         DefaultDate( 1, DataSetEvaluador.FieldByName('NO_ASI_FIN').AsDateTime ) );
     end
     else
     begin
     {$endif}
         {$ifdef QUINCENALES}
         Result := GetResultado( 'AU_FECHA', DefaultString( 3, VACIO ), DefaultDate( 0, oZetaProvider.DatosPeriodo.InicioAsis ), DefaultDate( 1, oZetaProvider.DatosPeriodo.FinAsis ) );
         {$else}
         Result := GetResultado( 'AU_FECHA', DefaultString( 3, VACIO ), DefaultDate( 0, oZetaProvider.DatosPeriodo.Inicio ), DefaultDate( 1, oZetaProvider.DatosPeriodo.Fin ) );
         {$endif}
     {$ifdef CAMBIO_TNOM}
     end;
     {$endif}
end;

{*********************** TZetaDIASINCA *********************}

type
  TZetaDIASINCA = class( TZetaRegresa )
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaDIASINCA.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos(2);
     TipoRegresa := resInt;
     AgregaRequerido( 'INCAPACI.IN_FEC_INI', enIncapacidad );
     AgregaRequerido( 'INCAPACI.IN_FEC_FIN', enIncapacidad );
     lQuitarOptimizeCursorB8 := FALSE;         // Si no requiere un query en el calculate, se deja optimizaci�n de Build #8
end;

function TZetaDIASINCA.Calculate: TQREvResult;
begin
     with DataSetEvaluador, Result do
     begin
          Kind:= resInt;
          intResult:= CalcDiasInca( FieldByName( 'IN_FEC_INI' ).AsDateTime,
                                    FieldByName( 'IN_FEC_FIN' ).AsDateTime,
                                    ParamDate( 0 ), ParamDate( 1 ),
                                    DefaultInteger( 2, 0 ) );
     end;
end;

{*********************** TZetaCLINCA *********************}

type
  TZetaCLINCA = class( TZetaRegresaQuery )
  public
    function GetScript : String; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaCLINCA.GetScript : String;
begin
     Result :=  'select TB_INCAPA from INCIDEN where TB_CODIGO = %s';
end;

procedure TZetaCLINCA.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos(1);
     TipoRegresa := resInt;
     if ParametrosConstantes then
        AgregaScript( Format( GetScript, [  UpperCase( ParamCampoString( 0 ) ) ] ), TipoRegresa, FALSE );
end;

function TZetaCLINCA.Calculate: TQREvResult;
begin
     Result := EvaluaQuery( Format( GetScript, [  EntreComillas( UpperCase( ParamString( 0 ) ) ) ] ) );
end;

{*********************** TZetaSUMAINCA *********************}

type
  TZetaSUMAINCA = class( TZetaSFuncion )
  private
    FRegresa: Integer;
    function CaseCampo: String;
    function GetTipoEntidad: TipoEntidad;
    procedure PreparaAgente; override;
  public
    function GetScript: String; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaSUMAINCA.PreparaAgente;
begin
    case FRegresa of
         1: with FAgente do
            begin
                 AgregaFiltro( 'INCAPACI.CB_CODIGO = :Empleado', TRUE, enIncapacidad );
                 AgregaFiltro( 'INCAPACI.IN_FEC_FIN > :FechaIni', TRUE, enIncapacidad );
                 AgregaFiltro( 'INCAPACI.IN_FEC_INI <= :FechaFin', TRUE, enIncapacidad );
            end;
         2: with FAgente do
            begin
                 AgregaFiltro( 'VACACION.CB_CODIGO = :Empleado', TRUE, enVacacion );
                 AgregaFiltro( 'VACACION.VA_FEC_FIN > :FechaIni', TRUE, enVacacion );
                 AgregaFiltro( 'VACACION.VA_FEC_INI <= :FechaFin', TRUE, enVacacion );
                 AgregaFiltro( 'VACACION.VA_TIPO = 1', TRUE, enVacacion );
            end;
    else
        with FAgente do
        begin
             AgregaFiltro( 'PERMISO.CB_CODIGO = :Empleado', TRUE, enPermiso );
             AgregaFiltro( 'PERMISO.PM_FEC_FIN > :FechaIni', TRUE, enPermiso );
             AgregaFiltro( 'PERMISO.PM_FEC_INI <= :FechaFin', TRUE, enPermiso );
        end;
    end;
end;

function TZetaSUMAINCA.GetScript : string;
begin
     case FRegresa of
          1 : Result:= 'select %s from INCAPACI where ( INCAPACI.CB_CODIGO = %s ) and ( INCAPACI.IN_FEC_INI <= %s ) and ( INCAPACI.IN_FEC_FIN > %s )';
	  2 : Result:= 'select %s from VACACION where ( VACACION.CB_CODIGO = %s ) and ( VACACION.VA_FEC_INI <= %s ) and ( VACACION.VA_FEC_FIN > %s ) and ( VACACION.VA_TIPO = 1 )';
     else
          Result := 'select %s from PERMISO where ( PERMISO.CB_CODIGO = %s ) and ( PERMISO.PM_FEC_INI <= %s ) and ( PERMISO.PM_FEC_FIN > %s )';
     end;
end;

function TZetaSUMAINCA.CaseCampo: String;
begin
     case FRegresa of
          1 : Result:= 'IN_DIAS';
	  2 : Result:= 'VA_GOZO';
     else
          Result := 'PM_DIAS';
     end;
end;

function TZetaSUMAINCA.GetTipoEntidad: TipoEntidad;
begin
     case FRegresa of
          1 : Result:= enIncapacidad;
	  2 : Result:= enVacacion;
     else
          Result := enPermiso;
     end;
end;

procedure TZetaSUMAINCA.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sCampo: String;
   lEsFecha: Boolean;
   TipoCampo: TQREvResultType;
begin
     // SUMA_INCA(nClase [, dInicial] [, dFinal] [, sRegresa] [, sFiltro] )
     ParametrosFijos(1);
     TipoRegresa := resDouble;
     oZetaProvider.GetDatosActivos;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     if ParametrosConstantes and ParamVacio( 4 ) then
     begin
          FRegresa:= ParamInteger( 0 );
          sCampo:= DefaultString( 3, CaseCampo );

          TipoCampo := InvestigaTipo( GetTipoEntidad, sCampo, lEsFecha );

          if TipoCampo <> resError then
          begin
               TipoRegresa:= TipoCampo;
               EsFecha:= lEsFecha;
               AgregaScript( Format( GetScript, [ 'SUM('+sCampo+')', 'COLABORA.CB_CODIGO',
                                                  ParamCampoDefFecha( 2, EntreComillas( DateToStrSQL( oZetaProvider.FechaDefault ) ) ),
                                                  ParamCampoDefFecha( 1, EntreComillas( DateToStrSQL( oZetaProvider.FechaDefault ) ) ) ] ),
                                                  TipoRegresa, lEsFecha );
          end;
     end;
end;

function TZetaSUMAINCA.Calculate: TQREvResult;
var
   sExpresion: String;
begin
     FRegresa:= ParamInteger( 0 );
     if FRegresa = 1 then
        sExpresion := DefaultString(3, Format( 'DIAS_INCAP( %s,%s )',
                                       [ EntreComillas(FechaAsStr(DefaultDate( 1, oZetaProvider.FechaDefault))),
                                         EntreComillas(FechaAsStr(DefaultDate( 2, oZetaProvider.FechaDefault))) ]))
     else
         sExpresion:= DefaultString( 3, CaseCampo );
     PreparaSuperEv( sExpresion, DefaultString( 4, VACIO ),
                     GetTipoEntidad, [ tgFloat, tgNumero ] );
     CierraDataSet;
     SetParamValue( 'Empleado', tgNumero, ZetaEvaluador.GetEmpleado );
     SetParamValue( 'FechaIni', tgFecha, DefaultDate( 1, oZetaProvider.FechaDefault ) );
     SetParamValue( 'FechaFin', tgFecha, DefaultDate( 2, oZetaProvider.FechaDefault ) );
     AbreDataSet;

     Result.Kind      := resDouble;
     Result.dblResult := SumaRegistros;
end;

{*********************** TZetaDIASBRINCA *********************}

type
  TZetaDIASBRINCA = class( TZetaRegresaEval )
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaDIASBRINCA.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos(2);
     TipoRegresa := resInt;
     AgregaRequerido( 'INCAPACI.IN_FEC_INI', enIncapacidad );
     AgregaRequerido( 'INCAPACI.IN_FEC_FIN', enIncapacidad );
     AgregaRequerido( 'INCAPACI.IN_MOTIVO', enIncapacidad );
end;

function TZetaDIASBRINCA.Calculate: TQREvResult;
var
   iBrinca : Integer;
   ResultFiltro : TQREvResult;
begin
     // DIAS_BRINCA( dInicial, dFinal [, nBrinca, sFiltro ] )
     iBrinca := DefaultInteger( 2, 3 );	// Aqu� el default es 3
     ResultFiltro:= Evalua( DefaultString( 3, 'IN_MOTIVO=0' ) ); // Filtro de Inicio de Incapacidad
     if ( ResultFiltro.Kind = resBool ) then
     begin
          if NOT ( ResultFiltro.booResult ) then
             iBrinca := 0;
          with DataSetEvaluador, Result do
          begin
               Kind := resInt;
               intResult:= CalcDiasInca( FieldByName( 'IN_FEC_INI' ).AsDateTime,
                                         FieldByName( 'IN_FEC_FIN' ).AsDateTime,
                                         ParamDate( 0 ), ParamDate( 1 ), iBrinca );
          end;
     end
     else
         ErrorParametro( 'Error en Par�metro', 'El Filtro no se pudo Evaluar como Condici�n' );
end;

{*********************** TZetaMOVKARDEX *********************}

type
  TZetaMOVKARDEX = class( TZetaRegresa )
  public
    function GetScript : String; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaMOVKARDEX.GetScript : String;
begin
     Result :=  'select %s from KARDEX where CB_CODIGO = LIQ_MOV.CB_CODIGO and CB_FECHA = LIQ_MOV.LM_KAR_FEC and CB_TIPO = LIQ_MOV.LM_KAR_TIP';
end;

procedure TZetaMOVKARDEX.GetRequeridos( var TipoRegresa : TQREvResultType );
var sCampo : String;
    lEsFecha : Boolean;
begin
     ValidaConfidencial;
     ParametrosFijos(1);
     sCampo := ParamString(0);
     TipoRegresa := InvestigaTipo( enKardex, sCampo, lEsFecha );
     EsFecha := lEsFecha;
     if TipoRegresa <> resError then
        AgregaScript( Format( GetScript, [ sCampo ] ), TipoRegresa, lEsFecha )
     else
        ErrorFuncion( 'Error en Par�metro', 'No existe Campo: ' + sCampo );
end;

{*********************** TZetaNOM *********************}

type
  TZetaNOM = class( TZetaRegresa )
  public
    function GetScript : String; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  protected
    function SetPeriodo : String ;virtual;
  end;

function TZetaNOM.GetScript : String;
begin
     Result := 'select %s from NOMINA where PE_YEAR = %s and PE_TIPO = %s and PE_NUMERO = %s and CB_CODIGO = %s';
end;

function TZetaNOM.SetPeriodo : String;
begin
     Result := IntToStr( ParamInteger( 0 ) );
end;

procedure TZetaNOM.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sCampo , sEmpleado: String;
   lEsFecha : Boolean;
begin
     // NOM( nPeriodo, sRegresa [, nTipo, nEmpleado, nYear ] ) : Variant
     ValidaConfidencial;
     ParametrosFijos(2);
     oZetaProvider.GetEncabPeriodo;

     sCampo := ParamString(1);
     if ParamVacio( 3 ) then
     begin
          sEmpleado := 'COLABORA.CB_CODIGO';
          AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     end
     else
          sEmpleado := IntToStr( ParamInteger( 3 ) );

     TipoRegresa := InvestigaTipo( enNomina, sCampo, lEsFecha );
     if ( TipoRegresa = resError ) then
        ErrorFuncion( 'Campo no existe en NOMINA', sCampo );

     EsFecha := lEsFecha;
     AgregaScript( Format( GetScript, [  sCampo ,
                   IntToStr( DefaultInteger( 4, oZetaProvider.DatosPeriodo.Year ) ),
                   IntToStr( DefaultInteger( 2, Ord( oZetaProvider.DatosPeriodo.Tipo ))),
                   SetPeriodo ,
                   sEmpleado ] ), TipoRegresa, lEsFecha );
end;

{*********************** TZetaNOMR *********************}

type
  TZetaNOMR = class( TZetaNOM )
  protected
    function SetPeriodo : String; override;
  end;

function TZetaNOMR.SetPeriodo : String;
begin
     Result := IntToStr( oZetaProvider.DatosPeriodo.Numero + ParamInteger( 0 ) );
end;

{*********************** TZetaNOMS *********************}

type
  TZetaNOMS = class( TZetaRegresa )
  public
    function  GetScript : String; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  protected
    sPeriodoIni, sPeriodoFin : String;
    procedure SetPeriodos; virtual;
  end;

function TZetaNOMS.GetScript : String;
begin
     Result := 'select SUM(%s) from NOMINA where PE_YEAR = %s and PE_TIPO = %s and PE_NUMERO >= %s and PE_NUMERO <= %s and CB_CODIGO = %s';
end;

procedure TZetaNOMS.SetPeriodos;
begin
     sPeriodoIni := IntToStr( ParamInteger( 0 ) );
     sPeriodoFin := IntToStr( ParamInteger( 1 ) );
end;

procedure TZetaNOMS.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sCampo , sEmpleado: String;
   lEsFecha : Boolean;
begin
     // NOMS( nInicial,  nFinal,  sRegresa [, nTipo, nEmpleado, nYear ]  )  : TPesos;
     ValidaConfidencial;
     ParametrosFijos( 3 );
     oZetaProvider.GetEncabPeriodo;
     SetPeriodos;
     sCampo := ParamString(2);

     if ParamVacio( 4 ) then
     begin
          sEmpleado := 'COLABORA.CB_CODIGO';
          AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     end
     else
          sEmpleado := IntToStr( ParamInteger( 4 ) );

     TipoRegresa := InvestigaTipo( enNomina, sCampo, lEsFecha );
     if ( TipoRegresa = resError ) then
        ErrorFuncion( 'Campo no existe en NOMINA', sCampo );

     EsFecha := lEsFecha;
     AgregaScript( Format( GetScript, [  sCampo ,
                                         IntToStr( DefaultInteger( 5, oZetaProvider.DatosPeriodo.Year ) ),
                                         IntToStr( DefaultInteger( 3, Ord( oZetaProvider.DatosPeriodo.Tipo ))),
                                         sPeriodoIni,
                                         sPeriodoFin,
                                         sEmpleado ] ), TipoRegresa, lEsFecha );
end;

{*********************** TZetaNOMSR *********************}

type
  TZetaNOMSR = class( TZetaNOMS )
  protected
    procedure SetPeriodos; override;
  end;

procedure TZetaNOMSR.SetPeriodos;
var
   iFactor: Byte;
begin
     with oZetaProvider.DatosPeriodo do
     begin
          iFactor:= 0;
          if ( ParamInteger(0) > ParamInteger(1) ) then
             iFactor:= 1;
          sPeriodoIni := IntToStr( Numero + ParamInteger( 0 + iFactor ) );
          sPeriodoFin := IntToStr( Numero + ParamInteger( 1 - iFactor ) );
     end;
end;

{*********************** TZetaCUALKARDEX *********************}

type
  TZetaCUALKARDEX = class( TZetaSFuncion )
  protected
    function Diferente( Last, New : TQREvResult ) : Boolean;
    procedure PreparaAgente; override;
  public
    function Calculate : TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaCUALKARDEX.PreparaAgente;
var
   lAscendente: Boolean;
begin
     lAscendente:= ( DefaultInteger( 0, 1 ) >= 0 );
     with FAgente do
     begin
          AgregaFiltro( 'KARDEX.CB_CODIGO = :Empleado', TRUE, enKardex );
          AgregaFiltro( 'KARDEX.CB_FECHA >= :FechaIni', TRUE, enKardex );
          AgregaFiltro( 'KARDEX.CB_FECHA <= :FechaFin', TRUE, enKardex );
          AgregaColumna( 'KARDEX.CB_FECHA', TRUE, enKardex, tgFecha, 10, 'CB_FECHA' );
          AgregaOrden( 'KARDEX.CB_FECHA', lAscendente, enKardex );
     end;
end;

procedure TZetaCUALKARDEX.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     //Cual_kardex( nCual  [,sExpresion] [, nRegresa] [, dFecha_inicio] [, dFecha_fin], sFiltro )
     //TipoRegresa:= resInt;
     ValidaConfidencial;
     ParametrosFijos(1);
     oZetaProvider.GetDatosActivos;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

function TZetaCUALKARDEX.Diferente( Last, New : TQREvResult ) : Boolean;
begin
     Result := TRUE;   // EZM: 7/Abr/99
     if Last.Kind = New.Kind then
     begin
          if ( Last.Kind = resDouble ) then
                Result := ( Last.dblResult <> New.dblResult )
          else if ( Last.Kind = resString ) then
                Result := ( Last.strResult <> New.strResult )
          else if ( Last.Kind = resInt ) then
                Result := ( Last.intResult <> New.IntResult )
          else if ( Last.Kind = resBool ) then
                Result := ( Last.booResult <> New.BooResult );
    end;
end;

function TZetaCUALKARDEX.Calculate: TQREvResult;
var
   iPos, iCual, iRegresa : Integer;
   dAnterior, dRegistro, dReferencia, dInicial, dFinal : TDate;
   LastEv : TQREvResult;
   lNegativo : Boolean;
begin
     PreparaSuperEv( DefaultString( 1, 'CB_PUESTO' ), DefaultString( 5, VACIO ),
                     enKardex, [ tgBooleano,tgFloat,tgNumero,tgFecha,tgTexto,tgMemo ] );

     iCual := DefaultInteger( 0, 1 );
     lNegativo := ( iCual < 0 );
     if ( lNegativo ) then
        iCual := Abs( iCual );

     if ( iCual = 0 ) then            // 'Cuantos'
        iRegresa := 1                 // Evita que de varias vueltas
     else
         iRegresa := DefaultInteger( 2, 1 );
     dInicial := DefaultDate( 3, 0 );
     dFinal   := DefaultDate( 4, oZetaProvider.FechaDefault );

     CierraDataSet;
     SetParamValue( 'Empleado', tgNumero, ZetaEvaluador.GetEmpleado );
     SetParamValue( 'FechaIni', tgFecha, dInicial );
     SetParamValue( 'FechaFin', tgFecha, dFinal );
     AbreDataSet;

     with SuperEv do
     begin
          if ( lNegativo ) then
          begin
               dReferencia := dFinal+1;
               iPos := 0;
               Result := Value;
               dRegistro := DataSet.FieldbyName( 'CB_FECHA' ).AsDateTime;
               dAnterior := dRegistro;
               while not DataSet.EOF do
               begin
                    LastEv := Value;
                    dRegistro := DataSet.FieldbyName( 'CB_FECHA' ).AsDateTime;
                    if ( iPos = 0 ) then
                       Inc( iPos )
                    else if ( Diferente( LastEv, Result )) then
                    begin
                         if iPos = iCual then
                         begin
                              Break;
                         end;
                         iPos := iPos + 1;
                         dReferencia := dAnterior+1;
                         Result := LastEv;
                    end;
                    dAnterior := dRegistro;
                    DataSet.Next;
               end;
               // ( iRegresa = 1 ) Ya se asign� valor al Hacer Result := Value / LastEv
               if ( iRegresa = 2 )  then   // fecha de movimiento
               begin
                    Result.Kind      := resDouble;
                    Result.resFecha  := TRUE;
                    Result.dblResult := dAnterior;
               end
               else if ( iRegresa = 3 ) then      // Tiempo transcurrido
               begin
                    Result.Kind      := resDouble;
                    Result.resFecha  := FALSE;
                    Result.dblResult := ABS( dReferencia - dAnterior  );
               end;
               EsFecha := Result.resFecha;
          end
          else
          begin
               iPos := 0;
               Result := Value;
               dRegistro := DataSet.FieldbyName( 'CB_FECHA' ).AsDateTime;
               dAnterior := dRegistro;
               if ( iCual = 0 ) or ( iCual > 1 ) or ( iRegresa = 3 ) then
               begin
                    while not DataSet.EOF do
                    begin
                         LastEv := Value;
                         if ( iPos = 0 ) then
                            Inc( iPos )
                         else if ( Diferente( LastEv, Result )) then
                         begin
                              Inc( iPos );
                              Result := LastEv;
                              dAnterior := dRegistro;
                              dRegistro := DataSet.FieldbyName( 'CB_FECHA' ).AsDateTime;
                              // Cuando iRegresa = 3, tiene que dar otra vuelta en el while
                              if (( iRegresa = 3 ) and ( iPos > iCual )) or
                                 (( iRegresa <> 3 ) and ( iPos = iCual )) then
                                 Break;
                         end;
                         DataSet.Next;
                    end;
               end;
               with Result do
               begin
                    if ( iCual = 0  ) then        // cuantos que cumplen....
                    begin
                         Kind      := resInt;
                         intResult := iPos;
                    end
                    else if ( iRegresa = 2 )  then   // fecha de movimiento
                    begin
                         Kind      := resDouble;
                         resFecha  := TRUE;
                         dblResult := dRegistro;
                    end
                    else if ( iRegresa = 3 ) then      // Tiempo transcurrido
                    begin
                         if ( iPos <= iCual ) then     // Si es el �ltimo registro
                         begin
                            dAnterior := dRegistro;
                            dRegistro := dFinal;
                         end;
                         Kind      := resDouble;
                         resFecha  := FALSE;
                         dblResult := ABS( dRegistro + 1 - dAnterior );
                    end;
                    EsFecha := resFecha;
               end;
          end;
     end;
end;

{*********************** TZetaPOSKARDEX *********************}

type
  TZetaPOSKARDEX = class( TZetaSFuncion )
  protected
    procedure PreparaAgente; override;
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaPOSKARDEX.PreparaAgente;
var
   sFiltroTipos : String;
   lAscendente: Boolean;
begin
     sFiltroTipos := DefaultString( 1, VACIO );
     lAscendente:= ( ParamInteger( 0 ) >= 0 );
     with FAgente do
     begin
          AgregaFiltro( 'KARDEX.CB_CODIGO = :Empleado', TRUE, enKardex );
          if StrLleno( sFiltroTipos ) then
             AgregaFiltro( CampoInLista( 'KARDEX.CB_TIPO', sFiltroTipos ), TRUE, enKardex );
          AgregaOrden( 'KARDEX.CB_FECHA', lAscendente, enKardex );
          AgregaOrden( 'KARDEX.CB_NIVEL', lAscendente, enKardex );
     end;
end;

procedure TZetaPOSKARDEX.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     //TipoRegresa:= resDouble;           // Para que no vaya a Calculate a Investigarlo
     ValidaConfidencial;
     ParametrosFijos(1);
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

function TZetaPOSKARDEX.Calculate: TQREvResult;
var
   iPos , iCuantos: Integer;
begin
     PreparaSuperEv( DefaultString( 2, '1' ), DefaultString( 3, VACIO ),
                     enKardex, [ tgBooleano,tgFloat,tgNumero,tgFecha,tgTexto,tgMemo ] );

     iPos := Abs( ParamInteger( 0 )); // Contempla posiciones negativas

     CierraDataSet;
     SetParamValue( 'Empleado', tgNumero, ZetaEvaluador.GetEmpleado );
     AbreDataSet;

     with SuperEv do
     begin
          iCuantos := 0;
          while not DataSet.EOF do
          begin
               Inc( iCuantos );
               if ( iCuantos = iPos ) then    // Ya lo encontr�
                  Break;
               DataSet.Next;
          end;
          Result := Value;   // Eval�a expresi�n en el registro buscado
          EsFecha := Result.resFecha;
     end;
end;

{*********************** TZetaRELATED *********************}

type
  TZetaRELATED = class( TZetaRegresa )
  public
   procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaRELATED.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sCampo : String;
   lEsFecha: Boolean;
begin
     //RELATED( sKey, sTabla, sCampo ) :
     ParametrosFijos( 3 );
     if ( UpperCase( ParamString( 1 ) ) <> 'COLABORA' ) then
        ErrorFuncion( 'Error en Par�metro', 'Tabla especificada NO es COLABORA' )
     else
     begin
          sCampo := ParamString( 2 );
          TipoRegresa := InvestigaTipo( enEmpleado, sCampo, lEsFecha );
          EsFecha:= lEsFecha;
          if TipoRegresa <> resError then
             EquivaleScript( sCampo, enEmpleado ) // Investiga tipo ya lo regresa como 'COLABORA.' + sCampo
          else
             ErrorFuncion( 'Error en Par�metro', 'Campo No Existe en COLABORA:' + sCampo );
     end;
end;

{*********************** TZetaREL *********************}

type
  TZetaREL = class( TZetaRegresa )
  public
   procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaREL.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sCampo : String;
   lEsFecha : Boolean;
begin
     //REL( sCampo ) :
     ParametrosFijos( 1 );
     sCampo := ParamString( 0 );
     TipoRegresa := InvestigaTipo( enEmpleado, sCampo, lEsFecha );
     if TipoRegresa <> resError then
        EquivaleScript( sCampo, enEmpleado ) // Investiga tipo ya lo regresa como 'COLABORA.' + sCampo
     else
        ErrorFuncion( 'Error en Par�metro', 'Campo No Existe en COLABORA:' + sCampo );
end;

//**********  TZetaF_INGRESO  ****
type
TZetaF_INGRESO = class ( TZetaRegresa )
public
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
end;

procedure TZetaF_INGRESO.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa:= resDouble;
     EsFecha := TRUE;
     EquivaleScript( 'COLABORA.CB_FEC_ING', enEmpleado );
end;

{TZetaHD}
type
TZetaHD = class ( TZetaRegresa )
public
 function GetScript: String; override;
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
end;

function TZetaHD.GetScript: String;
begin
     Result:= 'Select au_horas + au_extras from ausencia where cb_codigo = colabora.cb_codigo ' +
              'and au_fecha= %s';
end;

procedure TZetaHD.GetRequeridos( var TipoRegresa : TQREvResultType ) ;
begin
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado);
     oZetaProvider.GetDatosPeriodo;
     TipoRegresa:= resDouble;
     ParametrosFijos(1);
     {$ifdef QUINCENALES}
     AgregaScript( Format( GetScript,[ EntreComillas(DateToStrSql( oZetaProvider.DatosPeriodo.InicioAsis + ParamInteger(0) - 1)) ]), TipoRegresa, False );
     {$else}
     AgregaScript( Format( GetScript,[ EntreComillas(DateToStrSql( oZetaProvider.DatosPeriodo.Inicio + ParamInteger(0) - 1)) ]), TipoRegresa, False );
     {$endif}
end;

{TZetaHORAS_NT}
type
TZetaHORAS_NT = class ( TZetaRegresa )
public
 function GetScript: String; override;
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
end;

function TZetaHORAS_NT.GetScript: String;
begin
     Result:= SelRes('SP_HORAS_NT( AUSENCIA.AU_HORAS, AUSENCIA.AU_PER_CG,' +
                                                    'AUSENCIA.AU_PER_SG, AUSENCIA.HO_CODIGO )');
end;

procedure TZetaHORAS_NT.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa:= resInt;
     AgregaRequerido( 'AUSENCIA.CB_CODIGO', enAusencia );
     AgregaScript( GetScript, tipoRegresa, False);
end;

{*********************** TZetaMIN_TARDE *********************}

type
  TZetaMIN_TARDE = class( TZetaRegresaquery )
  protected
    DFecha: TDate;
    iBase: Integer;
  public
    constructor Create(oEvaluador:TQrEvaluator); override;
    function GetScript : String; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

constructor TZetaMIN_TARDE.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create(oEvaluador);
     dFecha := 0;
     iBase := 0;
end;

function TZetaMIN_TARDE.GetScript: String;
begin
     Result:= 'select %s, %s from CHECADAS left outer join AUSENCIA on AUSENCIA.CB_CODIGO = CHECADAS.CB_CODIGO and AUSENCIA.AU_FECHA = CHECADAS.AU_FECHA ' +
                                          'left outer join HORARIO on HORARIO.HO_CODIGO = AUSENCIA.HO_CODIGO ' +
              'where CHECADAS.CB_CODIGO = %s and CHECADAS.AU_FECHA = %s and CHECADAS.CH_TIPO = %s and CHECADAS.CH_POSICIO = %s';
end;

procedure TZetaMIN_TARDE.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resInt;
     AgregaRequerido( 'AUSENCIA.CB_CODIGO', enAusencia );
     AgregaRequerido( 'AUSENCIA.AU_FECHA', enAusencia );
end;

function TZetaMIN_TARDE.Calculate: TQREvResult;
var
   sCual, sHora, sHorario: String;
   iPos, iTipo, iVacios: Integer;
   lNegativos: Boolean;
begin
     if ( iBase = 0 ) then
        dFecha := DataSetEvaluador.FieldByName( 'AU_FECHA' ).AsDateTime;

     // Obtiene Par�metros y defaults
     iPos := DefaultInteger( 0 + iBase, 1 );	        // Posici�n, default PRIMERA
     if ParamVacio( 1 + iBase ) then		        // Si viene vac�o, default ENTRADA
        iTipo := 1
     else if Argument( 1 + iBase ).Kind = resInt then	// Con n�mero, tipo Windows
        iTipo := ParamInteger( 1 + iBase )
     else if ParamString( 1 + iBase ) = 'E' then	// Con letra, tipo DOS
        iTipo := 1			                // E = ENTRADA
     else
        iTipo := 2;			                // S = SALIDA

     if ( iTipo = 1 ) then
        sHorario := 'HO_INTIME'
     else
        sHorario := 'HO_OUTTIME';

     if  ( DefaultInteger( 2 + iBase, 1 ) = 1 ) then		// Default, 1 = CH_H_REAL
        sCual := 'CH_H_REAL'
     else
        sCual := 'CH_H_AJUS';

     iVacios := DefaultInteger( 3 + iBase, 0 );
     lNegativos := DefaultBoolean( 4 + iBase, FALSE );

// La siguiente l�nea marca error hasta que se suba el ZetaEvaluador bueno
     AbreQuery( Format( GetScript, [ sCual, sHorario, IntToStr( ZetaEvaluador.GetEmpleado ), EntreComillas( DateToStrSQL( dFecha ) ),
                                     IntToStr(iTipo), IntToStr(iPos) ] ) );

     with FQuery do
     begin
          sHora := aHora24( FieldByName( sCual ).AsString );
	         sHorario := FieldByName( sHorario ).AsString;
     end;
     with Result do
     begin
          Kind := resInt;
          intResult := DifMinutos( sHorario, sHora, iVacios, lNegativos );
     end;
end;

{*********************** TZetaDIA_TARDE *********************}

type
  TZetaDIA_TARDE = class ( TZetaMIN_TARDE )
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaDIA_TARDE.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resInt;
     with oZetaProvider do
     begin
          GetDatosPeriodo;
          GetDatosActivos;
     end;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     {$IFDEF CAMBIO_TNOM}
     if ( EsEntidadNomina ) then
     begin
          AgregaRequerido( 'NOMINA.NO_ASI_INI', enNomina );
     end;
     {$ENDIF}
end;

function TZetaDIA_TARDE.Calculate : TQREvResult;
begin
    iBase := 1;
    if ParamVacio( 0 ) then
       dFecha:= oZetaProvider.FechaDefault
    else if ( Argument( 0 ).Kind = resInt ) then
       {$ifdef CAMBIO_TNOM}
       if ( EsEntidadNomina ) then
       begin
            dFecha:= DatasetEvaluador.FieldByName( 'NO_ASI_INI' ).AsDateTime + ParamInteger( 0 ) - 1
       end
       else
       {$endif}
           {$ifdef QUINCENALES}
           dFecha := oZetaProvider.DatosPeriodo.InicioAsis + ParamInteger( 0 ) - 1
           {$else}
           dFecha := oZetaProvider.DatosPeriodo.Inicio + ParamInteger( 0 ) - 1
           {$endif}
    else
       dFecha := ParamDate( 0 );
    Result := inherited Calculate;
end;

{TZetaPROT}
type
TZetaPROT = class ( TZetaRegresa )
public
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
 function Calculate : TQREvResult; override;
end;

 procedure TZetaPROT.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos(1);
     TipoRegresa:= resString;
     lQuitarOptimizeCursorB8 := FALSE;         // Si no requiere un query en el calculate, se deja optimizaci�n de Build #8
end;

function TZetaPROT.Calculate : TQREvResult;
var
   nLinea, nAncho, nAnchoStars : Integer;
   sStars : String;
begin
     with Result do
     begin
          nAncho := DefaultInteger( 1, 40 );
          nLinea := DefaultInteger( 2, 1 );
          sStars := DefaultString( 4, '***' );
          nAnchoStars := 2 * ( Length( sStars ) + 1 );

          Kind:= resString;
          StrResult:= MontoConLetra( ParamFloat(0),
                                     DefaultString( 3, '' ),
                                     DefaultBoolean( 5, True ));

          if ( nLinea = 1 ) then
               StrResult := PadC( sStars + ' ' + Trim( MemoLine( StrResult, nAncho-nAnchoStars, 1 )) + ' ' + sStars, nAncho )
          else
          begin
                StrResult := Trim( MemoLine( StrResult, nAncho-nAnchoStars, nLinea ));
                if Length( StrResult ) > 0 then
                   StrResult := PadC( sStars + ' ' + StrResult + ' ' + sStars, nAncho );
          end;
     end;
end;

{TZetaREG_PATRONAL}
type
TZetaREG_PATRONAL= class ( TZetaRegresaQuery )
public
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
 procedure CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQREvResultType );
 function GetScript: String; override;
 function Calculate : TQREvResult; override;
end;

function TZetaREG_PATRONAL.GetScript: String;
begin
     Result:= 'Select %s from RPatron where tb_codigo= %s';
end;

procedure TZetaREG_PATRONAL.GetRequeridos( var TipoRegresa : TQREvResultType );
 var sCampo: String;
begin
     AgregaRequerido( 'COLABORA.CB_PATRON', enEmpleado );
     if not ParametrosConstantes then Exit;
     CaseCampo( DefaultInteger(1,5), sCampo, TipoRegresa );
     if ParamVacio(0) then
        AgregaScript( Format(GetScript,[ sCampo, DefaultString(0, 'COLABORA.CB_PATRON' ) ]), TipoRegresa, False )
     else
         AgregaScript( Format(GetScript,[ sCampo, ParamCampoString(0)]), TipoRegresa, False )
end;

procedure TZetaREG_PATRONAL.CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQREvResultType );
begin
     Tipo := resString;
     case iRegresa of
          1: sCampo := 'TB_ELEMENT';
          2: sCampo := 'TB_NUMERO';
          3: sCampo := 'TB_TEXTO';
          4: sCampo := 'TB_INGLES';
          5: sCampo := 'TB_NUMREG';
     else
     begin
          sCampo := 'TB_MODULO';
          Tipo := resInt;
     end;
     end;
end;

function TZetaREG_PATRONAL.Calculate : TQREvResult;
 var TipoRegresa : TQREvResultType;
     sCampo: String;
begin
     CaseCampo( DefaultInteger(1, 1), sCampo, TipoRegresa  );
     Result:= EvaluaQuery( Format( GetScript, [ sCampo, DefaultString(0,DatasetEvaluador.FieldByName('CB_PATRON').AsString) ]));
end;


{TZetaSET_HEADER}
type
TZetaSET_HEADER = class ( TZetaConstante )
public
 function Calculate : TQREvResult; override;
end;
function TZetaSET_HEADER.Calculate : TQREvResult;
var i:Integer;
begin
     oZetaProvider.GetDatosPeriodo;
     Result.Kind:= resInt;
     i := DefaultInteger( 0, 1 );
     with oZetaProvider.DatosPeriodo, Result do
          case i of
               1:
               begin
                    Kind:= resString;
                    strResult:= IntToStr( Ord(Tipo) ) + Padl(IntToStr( Numero ),3);
               end;
               2: intResult:= Ord(Tipo);
               3:
               begin
                    Kind:= resString;
                    strResult:= Copy( IntToStr( Numero ),0,3);
               end;
               4: intResult:= Ord( Status );
               5:
               begin
                    Kind:= resDouble;
                    EsFecha := TRUE;
                    dblResult:= Inicio;
               end;
               6:
               begin
                    Kind:= resDouble;
                    EsFecha := TRUE;
                    dblResult:= Fin;
               end;
               7:
               begin
                    Kind:= resDouble;
                    EsFecha := TRUE;
                    dblResult:= Pago;
               end;
               8:
               begin
                    Kind:= resString;
                    if ( PosMes = 1 ) then
                       strResult := 'I'
                    else if ( PosMes = PerMes ) then
                       strResult := 'F'
                    else
                        strResult := Space( 1 );
               end;
               9: Result := ErrorCreate( ErrorEvaluador( eeFuncionParam,
                                                                  'Par�metro Descontinuado N�mero: 9',
                                                                  'Error en la Expresi�n: SET_HEADER/PERIODO( 9 )' ));
               10:
               begin
                    Kind:= resString;
                    if ( Numero = 1 ) then strResult:= 'I'
                    else if ( Numero = PerAnio ) then strResult:= 'F'
                    else strResult:= SPACE(1);
               end;
               11: IntResult:= Usuario;
               12: intResult:= Dias;
               13: Result := ErrorCreate( ErrorEvaluador( eeFuncionParam,
                                                                  'Par�metro Descontinuado N�mero: 13',
                                                                  'Error en la Expresi�n: SET_HEADER/PERIODO( 13 )' ));
               14: intResult:= DiasAcumula;
               15: intResult:= Mes;
               16: Result := ErrorCreate( ErrorEvaluador( eeFuncionParam,
                                                              'Par�metro Descontinuado N�mero: 16',
                                                                  'Error en la Expresi�n: SET_HEADER/PERIODO( 16 )' ));
               17:
               begin
                    if DescuentaAhorros  and
                       DescuentaPrestamos  then
                       intResult:= 1
                    else if DescuentaAhorros and
                            not DescuentaPrestamos then
                            intResult:= 2
                    else if not DescuentaAhorros and
                            DescuentaPrestamos then
                            IntResult:= 3
                    else intResult:= 4
               end;
               18:
               begin
                    Kind:= resString;
                    if zStrToBool( SoloEx ) then
                       strResult:= '2'
                    else strResult:= '1';
               end;
               19:
               begin
                    Kind:= resString;
                    strResult:= Descripcion;
               end;
               20: IntResult:= NumeroEmpleados;
               else
                   Result := ErrorCreate( ErrorEvaluador( eeFuncionParam,
                                                                  'Par�metro Descontinuado N�mero:' + IntToStr( i ),
                                                                  'Error en la Expresi�n: SET_HEADER/PERIODO( ' + IntToStr( i ) + ' )' ));
     end;
end;

{TZetaTAB_SALARIO}
type
TZetaTAB_SALARIO = class ( TZetaRegresaQuery )
public
 function GetScript: String; override;
 function GetScript2: String;
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
 function Calculate : TQREvResult; override;
end;

function TZetaTAB_SALARIO.GetScript: String;
begin
     Result:= 'Select TB_SALARIO from CLASIFI where TB_CODIGO = %s';
end;

function TZetaTAB_SALARIO.GetScript2: String;
begin
     Result := '(select CB_CLASIFI from AUSENCIA where ' +
	       'AUSENCIA.CB_CODIGO = %s and AUSENCIA.AU_FECHA = %s )';
     // Nota: Es el mismo script que usa TAB_TAB().
end;

procedure TZetaTAB_SALARIO.GetRequeridos( var TipoRegresa : TQREvResultType );
 var sCodigo,Script: String;
     dFecha: TDate;
begin
     ValidaConfidencial;
     TipoRegresa := resDouble;
     ParametrosFijos( 1 );
     if not ParametrosConstantes then Exit;
     if ( Argument(0).Kind = resString ) and not EsFechaValida( ParamString(0), DFecha ) then
        sCodigo := ParamCampoString( 0 )
     else
     begin
          sCodigo := Format( GetScript2, [ 'COLABORA.CB_CODIGO', ParamCampoFecha(0) ]);
          AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado); // Join
     end;
     Script:= Format( GetScript, [ sCodigo ] );
     AgregaScript( Script , TipoRegresa, False );
end;

function TZetaTAB_SALARIO.Calculate : TQREvResult;
 var sCodigo,Script : String;
     dFecha: TDate;
begin
     if ( Argument(0).Kind = resString ) and not EsFechaValida( ParamString(0), DFecha ) then
	sCodigo := ParamCampoString( 0 ) // El c�digo fu� proporcionado por el usuario
     else  // Select anidado para obtener el CB_CLASIFI a la fecha dada
         sCodigo := Format( GetScript2, [ IntToStr( ZetaEvaluador.GetEmpleado ),
				       ParamCampoFecha(0)  ] );
     Script:= Format( GetScript, [ sCodigo ] );
     Result := EvaluaQuery( Script );
end;

{TZetaTAB_TAB}
type
TZetaTAB_TAB = class ( TZetaRegresaQuery )
public
 function GetScript: String; override;
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
 function Calculate : TQREvResult; override;
end;

function TZetaTAB_TAB.GetScript: String;
begin
     Result:= 'select CB_CLASIFI from AUSENCIA where ' +
              'AUSENCIA.CB_CODIGO = %s and AUSENCIA.AU_FECHA = %s';
end;

procedure TZetaTAB_TAB.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   Script: String;
begin
     ValidaConfidencial;
     TipoRegresa := resString;
     ParametrosFijos( 1 );
     if ParametrosConstantes then
     begin
          Script:= Format( GetScript, [ 'COLABORA.CB_CODIGO', ParamCampoFecha(0) ] );
          AgregaScript( Script, TipoRegresa, False );
     end
     else
          AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado ); // Join
end;

function TZetaTAB_TAB.Calculate : TQREvResult;
var
   Script: String;
begin
     Script := Format( GetScript, [ IntToStr( ZetaEvaluador.GetEmpleado ), DateToStrSQLC( ParamDate( 0 )) ] );
     Result := EvaluaQuery( Script );
end;

{TZetaTAB_GENERAL}
type
TZetaTAB_GENERAL = class ( TZetaRegresa )
public
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
 function GetScriptFijo : String;
 procedure Construye( const sExpresion: string; sFiltro : String );
end;

function TZetaTAB_GENERAL.GetScriptFijo : String;
begin
     Result := 'CLASIFI.TB_SALARIO / 8.0 * %s ';
end;

procedure TZetaTAB_GENERAL.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ValidaConfidencial;
     oZetaProvider.GetDatosPeriodo;
     TipoRegresa := resDouble;
     {$ifdef CAMBIO_TNOM}
     if ( EsEntidadNomina ) then
     begin
          AgregaRequerido('NOMINA.NO_ASI_INI', enNomina );
          AgregaRequerido('NOMINA.NO_ASI_FIN', enNomina );
     end;
     {$endif}
     ParametrosFijos( 1 );
     if not ParametrosConstantes then Exit;
     Construye( ParamString( 0 ), DefaultString( 1, '' ));
     lQuitarOptimizeCursorB8 := FALSE;         // Si no requiere un query en el calculate, se deja optimizaci�n de Build #8
end;

procedure TZetaTAB_GENERAL.Construye( const sExpresion: string; sFiltro : String );
var Script, sInicial,sFinal: String;
begin
{
     Script := 'select SUM( %s ) AS RESULTADO from AUSENCIA ' +
        'left outer join CLASIFI on AUSENCIA.CB_CLASIFI = CLASIFI.TB_CODIGO ' +
	    'where AUSENCIA.CB_CODIGO = COLABORA.CB_CODIGO and ' +
            'AUSENCIA.AU_FECHA BETWEEN %s and %s %s';
}
     Script := 'select SUM( %s ) from AUSENCIA, CLASIFI ' +
	    'where ( AUSENCIA.CB_CODIGO = COLABORA.CB_CODIGO ) and ' +
            '( CLASIFI.TB_CODIGO = AUSENCIA.CB_CLASIFI ) and ' +
            '( AUSENCIA.AU_FECHA BETWEEN %s and %s ) %s';
     {$ifdef CAMBIO_TNOM}
     if ( EsEntidadNomina ) then
     begin
          sInicial:= 'NOMINA.NO_ASI_INI';
          sFinal:= 'NOMINA.NO_ASI_FIN';
     end
     else
     begin
     {$endif}
     {$ifdef QUINCENALES}
          sInicial := EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.InicioAsis ) );
          sFinal := EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.FinAsis ) );
          {$else}
          sInicial := EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.Inicio ) );
          sFinal := EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.Fin ) );
     {$endif}
     {$ifdef CAMBIO_TNOM}
     end;
     {$endif}
     if ( not StrVacio( sFiltro )) then
          sFiltro := ' and (' + sFiltro + ')';

     Script := Format( Script, [ sExpresion, sInicial, sFinal, sFiltro ] );
     AgregaScript( Script, resDouble, False );
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );      // Join
end;


{TZetaTAB_ORDINARIO}
type
TZetaTAB_ORDINARIO = class ( TZetaTAB_GENERAL )
public
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
end;

procedure TZetaTAB_ORDINARIO.GetRequeridos( var TipoRegresa : TQREvResultType );
 var sExpresion: String;
begin
     ValidaConfidencial;
     TipoRegresa := resDouble;
     sExpresion := Format( GetScriptFijo, [ 'AU_HORAS' ] );
     {$ifdef CAMBIO_TNOM}
     if ( EsEntidadNomina ) then
     begin
          AgregaRequerido('NOMINA.NO_ASI_INI', enNomina );
          AgregaRequerido('NOMINA.NO_ASI_FIN', enNomina );
     end;
     {$endif}
     Construye( sExpresion, DefaultString( 0, '' ));
end;


{TZetaTAB_DOBLES}
type
TZetaTAB_DOBLES = class ( TZetaTAB_GENERAL )
public
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
end;

procedure TZetaTAB_DOBLES.GetRequeridos( var TipoRegresa : TQREvResultType );
 var sExpresion: String;
begin
     ValidaConfidencial;
     TipoRegresa := resDouble;
     {$ifdef CAMBIO_TNOM}
     if ( EsEntidadNomina ) then
     begin
          AgregaRequerido('NOMINA.NO_ASI_INI', enNomina );
          AgregaRequerido('NOMINA.NO_ASI_FIN', enNomina );
     end;
     {$endif}
     sExpresion := Format( GetScriptFijo, [ 'AU_DOBLES' ] );
     Construye( sExpresion, DefaultString( 0, '' ));
end;


{TZetaTAB_TRIPLES}
type
TZetaTAB_TRIPLES = class ( TZetaTAB_GENERAL )
public
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
end;

procedure TZetaTAB_TRIPLES.GetRequeridos( var TipoRegresa : TQREvResultType );
 var sExpresion: String;
begin
     ValidaConfidencial;
     TipoRegresa := resDouble;
     {$ifdef CAMBIO_TNOM}
     if ( EsEntidadNomina ) then
     begin
          AgregaRequerido('NOMINA.NO_ASI_INI', enNomina );
          AgregaRequerido('NOMINA.NO_ASI_FIN', enNomina );
     end;
     {$endif}
     sExpresion := Format( GetScriptFijo, [ 'AU_TRIPLES' ] );
     Construye( sExpresion, DefaultString( 0, '' ));
end;


{CV: DIC2002:
Se movieron estas funciones a la unidad ZEvaluador,
porque tambien se requieren en el Modulo de Seleccion de Personal.
En ZEvaluador, estan las clases base, en esta unidad estan las
clases Hijas.
La parte comentada es como estaba anteriormente la funcion.
En ZFuncsSeleccion, se tienen las mismas funciones }

{TZetaPRETTY_NAME}
{
type
TZetaPRETTY_NAME = class ( TZetaRegresa )
public
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
end;

procedure TZetaPRETTY_NAME.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resString;
     // Si lo pongo desde aqu�, me ahorro la b�squeda del campo calculado en el diccionario
     AgregaScript( K_PRETTYNAME, TipoRegresa, False  );
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado ); //Join
end;
}
type

TZetaPRETTY_NAME = class ( TZetaBasePRETTY_NAME )
protected
  procedure AgregaDatosRequeridos;override;
end;

procedure TZetaPRETTY_NAME.AgregaDatosRequeridos;
begin
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado ); //Join
end;

{TZetaOLD_PUESTO}
type
TZetaOLD_PUESTO  = class ( TZetaRegresa )
  private
{$ifdef MSSQL}
    function GetScriptPlaza: String;
{$endif}
  public
    function GetScript: String; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
end;

function TZetaOLD_PUESTO.GetScript: String;
begin
{$ifdef INTERBASE}
     Result:=  '(Select %s from %s( COLABORA.CB_CODIGO ))';
{$else}
     Result:=  'dbo.%s( COLABORA.CB_CODIGO, %s )';
{$endif}
end;

{$ifdef MSSQL}
function TZetaOLD_PUESTO.GetScriptPlaza: String;
begin
     Result := 'dbo.SP_OLD_PLAZA( COLABORA.CB_CODIGO )';
end;
{$endif}

procedure TZetaOLD_PUESTO.GetRequeridos( var TipoRegresa : TQREvResultType );
 var sStoredProc,sScript,sCampo: String;
     nRegresa, nNivel: Integer;
begin
     ParametrosFijos( 1 );
     nRegresa := ParamInteger( 0 );
     nNivel := DefaultInteger( 1, 1 );
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado) ; // Join

     TipoRegresa := resString;

{$ifdef INTERBASE}
     case nRegresa of
          1, 2 : sStoredProc := 'sp_OLD_PUESTO';
          3, 10: sStoredProc := 'sp_OLD_CLASIFI';
          4, 5 : sStoredProc := Format( 'sp_OLD_NIVEL%s',  [ IntToStr( nNivel ) ] );
          6, 9 : sStoredProc := 'sp_OLD_TURNO';
          7,11 : sStoredProc := 'sp_OLD_TABLASS';
          8,12 : sStoredProc := 'sp_OLD_CONTRATO'
          else
              sStoredProc := 'sp_OLD_PLAZA'; //13 y 14;
     end;

     if ( nRegresa in [ 1, 4, 9, 10, 11, 12, 14 ] ) then
     begin
          sCampo := 'FECHA';
          TipoRegresa := resDouble;
          EsFecha:= True;
     end
     else
     begin
	         sCampo := 'RESULTADO';
          if nRegresa = 13 then
             TipoRegresa := resInt //La plaza es numerica
          else
              TipoRegresa := resString;
     end;

     sScript := Format( GetScript, [ sCampo, sStoredProc ] );
{$else}
     case nRegresa of
        7, 8 : sStoredProc := 'SP_OLD_CODIGO1';
        2, 3, 5, 6 : sStoredProc := 'SP_OLD_CODIGO6';
        13 : sStoredProc := 'SP_OLD_CODIGO_PLAZA'
        else
        begin
            sStoredProc := 'SP_OLD_FECHA';
            TipoRegresa := resDouble;
            EsFecha     := TRUE;
        end;
     end;

     if nRegresa = 13 then
        TipoRegresa := resInt; //La plaza es numerica

     case nRegresa of
        1, 2  : sCampo := '10';   // Puesto
        3, 10 : sCampo := '11';   // Clasificaci�n
        4, 5  : sCampo := IntToStr( nNivel ); // Nivelx
        6, 9  : sCampo := '12';  // Turno
        7, 11 : sCampo := '13';  // TablaSS
        8, 12 : sCampo := '14';  // Contrato
        else
            sCampo := '15';
     end;

     if ( nRegresa = 13 ) then
        sScript := GetScriptPlaza
     else
         sScript := Format( GetScript, [ sStoredProc, sCampo ] );
{$endif}

     AgregaScript( sScript, TipoRegresa, EsFecha );
     lQuitarOptimizeCursorB8 := FALSE;         // Si no requiere un query en el calculate, se deja optimizaci�n de Build #8
end;

{TZetaOP_GRAVADA}
type
TZetaOP_GRAVADA = class ( TZetaRegresa )
public
 function GetScript: String; override;
 function GetScript2: String;
 function GetScript3: String;
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
end;

function TZetaOP_GRAVADA.GetScript: String;
begin
     Result:= 'select SUM( KF_GRAVADO ) from KAR_FIJA where ' +
  			'CB_CODIGO = COLABORA.CB_CODIGO and '+
			'CB_FECHA  = COLABORA.CB_FEC_SAL and ' +
		  	'CB_TIPO = COLABORA.CB_TIP_REV';
end;

function TZetaOP_GRAVADA.GetScript2: String;
begin
     Result:= 'select %s from KAR_FIJA where ' +
                      'CB_CODIGO = COLABORA.CB_CODIGO and ' +
                      'CB_FECHA  = COLABORA.CB_FEC_SAL and ' +
                      'CB_TIPO = COLABORA.CB_TIP_REV and ' +
                      'KF_FOLIO = %s';
end;

function TZetaOP_GRAVADA.GetScript3: String;
begin
     Result:= 'select %s from KAR_FIJA where ' +
                      'CB_CODIGO = COLABORA.CB_CODIGO and ' +
                      'CB_FECHA  = COLABORA.CB_FEC_SAL and ' +
                      'CB_TIPO = COLABORA.CB_TIP_REV and ' +
                      'KF_CODIGO = %s';
end;

procedure TZetaOP_GRAVADA.GetRequeridos( var TipoRegresa : TQREvResultType );
 var nRegresa: Integer;
     sScript,sCampo, sOp: String;
begin
     ValidaConfidencial;
     if NOT ParametrosConstantes then EXIT;

     if (NOT ParamVacio(0)) AND (Argument(0).Kind = resString) then
     begin
          sOp := ParamString(0);
          sScript := GetScript3;
     end
     else
     begin
          sOp := IntToStr(DefaultInteger( 0, 1 ));
          sScript := GetScript2;
     end;

     nRegresa := DefaultInteger( 1, 1 );
     TipoRegresa := resDouble;

     if ( nRegresa = 7 ) then
        sScript := GetScript
     else
     begin
          case ( nRegresa ) of
               1, 2 : sCampo := 'KF_GRAVADO';
               3, 4 : sCampo := 'KF_MONTO-KF_GRAVADO';
               5 : sCampo := 'KF_MONTO';
               6 :
               begin
                    sCampo := 'KF_IMSS';
                    TipoRegresa := resInt;
               end;
               else
               begin
                    sCampo := 'KF_CODIGO';
                    TipoRegresa := resString;
               end;
          end;
          sScript := Format( sScript, [ sCampo, sOp ] );
     end;
     AgregaScript( sScript, TipoRegresa, False );
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado); // Join
     lQuitarOptimizeCursorB8 := FALSE;         // Si no requiere un query en el calculate, se deja optimizaci�n de Build #8
end;


{ ***************** TZetaDELIMITA ******************** }

type
  TZetaDELIMITA = class ( TZetaRegresa )
  public
   procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
   function Calculate: TQREvResult; override;
end;

procedure TZetaDELIMITA.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     lQuitarOptimizeCursorB8 := FALSE;         // Si no requiere un query en el calculate, se deja optimizaci�n de Build #8
end;

function TZetaDELIMITA.Calculate: TQREvResult;
var
   Valor: TQREvResult;
begin
     ParametrosFijos( 1 );
     Result.Kind := resString;

     Valor := Argument( 0 );

     case ( Valor.Kind ) of
          resString : Result.strResult := '"' + Valor.strResult + '"';	// Comillas Dobles;
          resDouble : if Valor.resFecha then
                         Result.strResult := FechaAsStr( Valor.dblResult )
                      else
                          Result.strResult := FloatToStr( Valor.dblResult );
          resInt : Result.strResult := IntToStr( Valor.intResult );
          else
              Result.strResult := '';
     end;
     Result.strResult := Result.strResult + DefaultString( 1, ',' );
end;


{TZetaTIPO_TRABAJA}
type
TZetaTIPO_TRABAJA = class ( TZetaRegresa )
public
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); Override;
end;

procedure TZetaTIPO_TRABAJA.GetRequeridos( var TipoRegresa : TQREvResultType ) ;
begin
     TipoRegresa := resInt;
     EquivaleScript( 'RPATRON.TB_MODULO', enRPatron );
end;


{TZetaPARIENTE}
type
TZetaPARIENTE = class ( TZetaRegresa )
public
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); Override;
 function GetScript: String; override;
end;

function TZetaPARIENTE.GetScript: String;
begin
{     Result := 'select %s from PARIENTE where CB_CODIGO = %s and PA_RELACIO = %s and ' +
               ' PA_FOLIO = %s %s';
 }
     Result := 'select %s from PARIENTE where CB_CODIGO = %s and PA_RELACIO = %s %s ';
end;

procedure TZetaPARIENTE.GetRequeridos( var TipoRegresa : TQREvResultType );
 var nRegresa,nRelacion,nFolio :Integer;
     sFiltro,sCampo : String;
begin
     ParametrosFijos( 2 );
     nRegresa := ParamInteger( 0 );
     nRelacion := ParamInteger( 1 );
     nFolio := DefaultInteger( 2, 1 );
     sFiltro := DefaultString( 3, '' );
     EsFecha:= False;
     if not StrVacio( sFiltro ) then
            sFiltro := ' and (' + sFiltro + ')';

     TipoRegresa := resString; // La mayor�a;
     case nRegresa of
          0 :
          begin
               sCampo := 'COUNT(*)';
               TipoRegresa:= resInt;
          end;
          1 : sCampo := 'PA_NOMBRE';
          2 : sCampo := 'PA_SEXO';
          3 :
          begin
               sCampo := 'PA_FEC_NAC';
               TipoRegresa:= resDouble;
               EsFecha:= True;
          end;
          4 :
          begin
               sCampo := 'PA_TRABAJA';
               TipoRegresa:= resInt;
          end;
          5 :
          begin
               sCampo := 'PA_NUMERO';
               TipoRegresa:= resDouble;
          end;
          6 : sCampo := 'PA_TEXTO';
     end;

     if ( nRegresa = 0 ) AND ParamVacio(2) then
          AgregaScript( Format( Parentesis( GetScript ), [ sCampo,
                                                           'COLABORA.CB_CODIGO',
                                                           IntToStr(nRelacion),
                                                           sFiltro ] ),
                        TipoRegresa , EsFecha )
     else
         AgregaScript( Format( Parentesis( GetScript ), [ sCampo,
                                                          'COLABORA.CB_CODIGO',
                                                          IntToStr(nRelacion),
                                                          ' and PA_FOLIO = ' + IntToStr(nFolio) + sFiltro ] ),
                      TipoRegresa , EsFecha );
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado);
end;

{TZetaCHUELLAS}
type
TZetaCHUELLAS = class ( TZetaRegresa )
public
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); Override;
 function GetScript: String; override;
end;

function TZetaCHUELLAS.GetScript: String;
begin
     Result := 'SELECT COALESCE (COUNT(HU_ID), 0) AS CANTIDAD FROM VHUELLAGTI WHERE CB_CODIGO = %s AND IV_CODIGO = 0 AND HU_ID != 0 AND CM_CODIGO = ''%s'' ' ;
end;

procedure TZetaCHUELLAS.GetRequeridos( var TipoRegresa : TQREvResultType );
 var nRegresa,nRelacion,nFolio :Integer;
     sFiltro,sCampo : String;
begin
     TipoRegresa:= resInt;
     oZetaProvider.GetDatosActivos;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     ParametrosFijos( 0 );
     if ParamVacio(0) then
        AgregaScript( Format( GetScript , ['COLABORA.CB_CODIGO', oZetaProvider.CodigoEmpresa ] ),TipoRegresa , False )
end;


type
TZetaTF = class ( TZetaRegresaQuery )
public
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); Override;
 function Calculate: TQREvResult; override;
 function GetScript : String; override;
end;

function TZetaTF.GetScript : String;
begin
     Result := SelRes('TF(%d,%s)');
end;

procedure TZetaTF.GetRequeridos( var TipoRegresa : TQREvResultType );
 var iLista: integer;
     lEsFecha: Boolean;
begin
     TipoRegresa := resString;
     lEsFecha := FALSE;
     ParametrosFijos( 2 );
     if ParametrosConstantes and NOT ( ParamEsCampo( 0 ) ) then
     begin
          iLista:= ParamInteger(0);
          if NOT ( ListasFijas( iLista ) in [lfMeses,lfMes13] ) then
             AgregaScript( Format( GetScript, [iLista,ParamCampoInteger(1)] ), TipoRegresa, lEsFecha );
     end;
end;

function TZetaTF.Calculate : TQREvResult;
 var iLista: integer;
begin
     with Result do
     begin
          Kind := resString;
          EsFecha := FALSE;
          iLista:= ParamInteger(0);
          Result := EvaluaQuery( Format( GetScript, [iLista,IntToStr( ParamInteger(1)-GetOffSet(ListasFijas(iLista)) )] ) );

     end;
end;

{TZetaClasifi}
type
TZetaClasifi = class ( TZetaRegresa )
public
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); Override;
end;

procedure TZetaClasifi.GetRequeridos( var TipoRegresa : TQREvResultType ) ;
begin
     ValidaConfidencial;
     TipoRegresa:= resString;
     EquivaleScript('COLABORA.CB_CLASIFI', enEmpleado )
end;



{TZetaTBAJA_IMSS}
type
TZetaTBAJA_IMSS = class ( TZetaRegresaQuery )
public
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
 function GetScript: String; override;
 function GetScript2: String;
 function Calculate : TQREvResult; override;
end;

function TZetaTBAJA_IMSS.GetScript: String;
begin
     Result:= 'Select tf_descrip from TFijas where tf_tabla= 5 and tf_codigo = %s';
end;

function TZetaTBAJA_IMSS.GetScript2: String;
begin
     Result:= '(Select tb_imss from Mot_Baja where tb_codigo = %s)';
end;

procedure TZetaTBAJA_IMSS.GetRequeridos( var TipoRegresa : TQREvResultType ) ;
begin
     TipoRegresa:= resString;
   AgregaRequerido( 'COLABORA.CB_MOT_BAJ', enEmpleado );
     if not ParametrosConstantes then
        if ParamVacio( 0 ) then
          exit;
     if ParamVacio(0) then // Caso B
        AgregaScript( Format( GetScript, [ Format( GetScript2, [ 'COLABORA.CB_MOT_BAJ' ] )] ),TipoRegresa,False)
     else if not ParamVacio(0) and ( Argument( 0 ).Kind <> resInt ) then // Caso B
                 AgregaScript( Format( GetScript, [ Format( GetScript2, [ ParamCampoString(0) ] )] ),TipoRegresa,False)
     else // Caso A
         AgregaScript( Format( GetScript, [ IntToStr(ParamInteger(0)) ] ),TipoRegresa,False);

end;

function TZetaTBAJA_IMSS.Calculate : TQREvResult;
begin
     if ParamVacio(0) or ( Argument( 0 ).Kind <> resInt ) then // Caso B
        Result:= EvaluaQuery( Format( GetScript, [ Format( GetScript2, [ DefaultString( 0, DatasetEvaluador.FieldByName('CB_MOT_BAJ' ).AsString )])]))
     else // Caso A
         Result:= EvaluaQuery( Format( GetScript, [ IntToStr(ParamInteger(0)) ] ));
end;

{ ************* TZetaTINCAPACI ************* }

type
   TZetaTINCAPACI = class ( TZetaRegresaQuery )
public
   procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
   function GetScript: String; override;
   function Calculate : TQREvResult; override;
end;

function TZetaTINCAPACI.GetScript: String;
begin
     Result:= 'select TF_DESCRIP from TFIJAS where TF_TABLA = 10 and TF_CODIGO = %s';
end;

procedure TZetaTINCAPACI.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos(1);
     TipoRegresa:= resString;
     AgregaScript( Format(GetScript, [ ParamCampoInteger(0) ]), TipoRegresa, False);
end;

function TZetaTINCAPACI.Calculate : TQREvResult;
begin
     Result:= EvaluaQuery( Format( GetScript, [ IntToStr( ParamInteger(0) ) ] ) );
end;

{TZetaTIPO_JORNADA}
type
TZetaTIPO_JORNADA = class ( TZetaRegresaQuery )
public
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
 function GetScript: String; override;
 function Calculate : TQREvResult; override;
end;

function TZetaTIPO_JORNADA.GetScript: String;
begin
     Result:= 'select ' + CampoRes( 'TU_TIP_JOR-1' ) + ' from TURNO where TU_CODIGO= %s';
end;

procedure TZetaTIPO_JORNADA.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     TipoRegresa:= resDouble;
     if not ParametrosConstantes then exit;
     if ParamVacio(0) then
        AgregaScript(Format(GetScript, [ 'COLABORA.CB_TURNO' ]),TipoRegresa, False)
     else
         AgregaScript(Format(GetScript, [ ParamCampoString(0) ]),TipoRegresa, False);
end;

function TZetaTIPO_JORNADA.Calculate : TQREvResult;
begin
     Result:= EvaluaQuery(Format(GetScript, [ DefaultString(0, DatasetEvaluador.FieldByName('CB_TURNO').AsString ) ]));
end;

{TZetaTIPO_SAL}
type
TZetaTIPO_SAL = class ( TZetaRegresa )
public
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ) ; override;
 function Calculate : TQREvResult; override;
end;

procedure TZetaTIPO_SAL.GetRequeridos( var TipoRegresa : TQREvResultType ) ;
begin
     TipoRegresa:= resString;
     AgregaRequerido( 'KARDEX.CB_SALARIO', enKardex );
     AgregaRequerido( 'KARDEX.CB_PER_VAR', enKardex );
     lQuitarOptimizeCursorB8 := FALSE;         // Si no requiere un query en el calculate, se deja optimizaci�n de Build #8
end;

function TZetaTIPO_SAL.Calculate : TQREvResult;
begin
     with Result, DatasetEvaluador do
     begin
          Kind:= resString;
          if ( FieldByName('CB_PER_VAR').AsFloat = 0 ) then
             strResult:= '0'
          else if (FieldByName('CB_SALARIO').AsFloat = 0 ) then
               strResult:= '1'
          else strResult:= '2';
     end;
end;

{TZetaTNIVEL}
type
TZetaTNIVEL = class ( TZetaRegresa )
public
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
 function GetScript: String; override;
 procedure CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQREvResultType );
end;

function TZetaTNIVEL.GetScript: String;
begin
     Result:= 'Select %s from NIVEL%s where TB_CODIGO = COLABORA.CB_NIVEL%s'
end;

procedure TZetaTNIVEL.GetRequeridos( var TipoRegresa : TQREvResultType );
 var sCampo: String;
begin
     ParametrosFijos(1);
     CaseCampo( DefaultInteger(1,1), sCampo, TipoRegresa );
     AgregaScript(Format(GetScript,[ sCampo, IntToStr(ParamInteger(0)), IntToStr(ParamInteger(0)) ]),TipoRegresa,False);
end;

procedure TZetaTNIVEL.CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQREvResultType );
begin
     Tipo:= resString;
     case iRegresa of
          2:
          begin
               Tipo:= resInt;
               sCampo:= 'TB_NUMERO';
          end;
          3: sCampo:= 'TB_TEXTO';
          4: sCampo:= 'TB_INGLES';
          else
              sCampo:= 'TB_ELEMENT';
     end;
end;

{TZetaVACA_OTRAS}
type
TZetaVACA_OTRAS = class ( TZetaRegresa )
public
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
 function GetScript: String; override;
end;

function TZetaVACA_OTRAS.GetScript: String;
begin
     //result:= 'Select VA_OTROS from Vacacion where cb_codigo = COLABORA.CB_CODIGO and ' +
     //         'va_tipo = 1 and va_nomyear = %s and va_nomtipo = %s and va_nomnume = %s';

     //POR QUE LA FUNCION ESTA LIMITADA A TIPO DE NOMINA SEMANAL.
     result := ' Select VA.VA_OTROS ' +
	       ' from Vacacion VA ' +
	       ' where VA.cb_codigo = COLABORA.CB_CODIGO ' +
	       ' and VA.va_tipo = 1 ' +
	       ' and VA.va_nomyear = %s ' +
               ' and VA.va_nomtipo = %s ' +
	       ' and VA.va_nomnume = %s ' +
	       ' and VA.va_fec_ini = ( Select MAX( VA1.VA_FEC_INI) ' +
					'from Vacacion VA1 ' +
					'where VA1.cb_codigo = COLABORA.CB_CODIGO ' +
		  			'and VA1.va_tipo = VA.va_tipo ' +
		  			'and VA1.va_nomyear = VA.va_nomyear ' +
                  			'and VA1.va_nomtipo = VA.va_nomtipo ' +
		  			'and VA1.va_nomnume = VA.va_nomnume) ';
end;

procedure TZetaVACA_OTRAS.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ValidaConfidencial;
     TipoRegresa:= resDouble;
     oZetaProvider.GetEncabPeriodo;
     with oZetaProvider do
          AgregaScript( Format( GetScript,[ IntToStr( DatosPeriodo.Year ), IntToStr(Ord( DatosPeriodo.Tipo )),
                                            IntToStr( DatosPeriodo.Numero ) ] ), TipoRegresa, False );
end;

{TZetaPRESTA_INFO}
type
TZetaPRESTA_INFO = class ( TZetaRegresa )
public
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
end;

procedure TZetaPRESTA_INFO.GetRequeridos( var TipoRegresa : TQREvResultType );
 var sCampo: String;
begin
     ParametrosFijos(1);
     TipoRegresa:= resDouble;
     case ParamInteger(0) of
          1:
          begin
               TipoRegresa:= resString;
               sCampo:= 'CB_INFCRED';
          end;
          2:
          begin
               TipoRegresa:= resInt;
               sCampo:= 'CB_INFTIPO';
          end;
          3:
          begin
               TipoRegresa:= resDouble;
               sCampo:= 'CB_INFTASA';
          end;
          4: sCampo:= 'CB_INFMANT';
          5:
          begin
               TipoRegresa:= resDouble;
               EsFecha := TRUE;
               sCampo:= 'CB_INF_INI';
          end;
          6: sCampo:= 'CB_INF_OLD';
          7:
          begin
               TipoRegresa:= resString;
               sCampo:= 'CB_CLINICA';
          end;
     end;
     EquivaleScript( 'COLABORA.' + sCampo, enEmpleado );
end;

{TZetaANTES_CUR}
type
TZetaANTES_CUR = class ( TZetaRegresa )
public
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
 function GetScript: string; override;
end;

function TZetaANTES_CUR.GetScript: string;
begin
     Result:= 'select %s from ANTESCUR where CB_CODIGO = %s AND AR_FOLIO = %s';
end;

procedure TZetaANTES_CUR.GetRequeridos( var TipoRegresa : TQREvResultType );
 var nRegresa, nFolio: Integer;
     sRegresa: String;
begin
     if not ParametrosConstantes then
        Exit;
     ParametrosFijos( 1 );
     nRegresa := DefaultInteger( 1, 1 );
     nFolio   := ParamInteger(0);

     TipoRegresa := resString;
     case nRegresa of
          1 :  sRegresa := 'AR_CURSO';
          2 :  sRegresa :=  'AR_LUGAR';
          else
          begin
               sRegresa := 'AR_FECHA';
               TipoRegresa := resDouble;
               EsFecha := TRUE;
          end;
     end;
     AgregaScript( Format( GetScript, [ sRegresa, 'COLABORA.CB_CODIGO', IntToStr(nFolio) ]), TipoRegresa, EsFecha);
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

{TZetaANTES_PTO}
type
TZetaANTES_PTO = class ( TZetaRegresa )
public
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
 function GetScript: string; override;
end;

function TZetaANTES_PTO.GetScript: string;
begin
     Result:= 'Select %s from ANTESPTO where CB_CODIGO = %s AND AP_FOLIO = %s';
end;

procedure TZetaANTES_PTO.GetRequeridos( var TipoRegresa : TQREvResultType );
 var nRegresa, nFolio: Integer;
     sRegresa: String;
begin
     if not ParametrosConstantes then
        Exit;
     ParametrosFijos( 1 );
     nFolio   := ParamInteger(0);
     nRegresa := DefaultInteger( 1, 1 );

     TipoRegresa := resString;
     case nRegresa of
          1 :  sRegresa := 'AP_PUESTO';
          2 :  sRegresa :=  'AP_EMPRESA';
          else
          begin
               if ( nRegresa = 3 ) then
                  sRegresa := 'AP_FEC_INI'
               else
                   sRegresa := 'AP_FEC_FIN';
               TipoRegresa := resDouble;
               EsFecha := TRUE;
          end;
     end;
     AgregaScript( Format( GetScript, [ sRegresa, 'COLABORA.CB_CODIGO', IntToStr(nFolio) ]), TipoRegresa, EsFecha );
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

{TZetaTIPO_CAMBIO}
type
TZetaTIPO_CAMBIO = class( TZetaRegresaQuery )
private
  procedure CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQREvResultType; var lFecha : Boolean );
  function GetScript: String; override;
public
  function Calculate: TQREvResult; override;
  procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
end;

function TZetaTIPO_CAMBIO.GetScript: String;
begin
     Result:= 'select %s from TCAMBIO ' +
              'where ( TC_FEC_INI = '+
              '(SELECT MAX(TC_FEC_INI) FROM TCAMBIO '+
              'WHERE TC_FEC_INI <= %s) )';
end;

procedure TZetaTIPO_CAMBIO.CaseCampo( const iRegresa : Integer;
                                      var sCampo : String;
                                      var Tipo : TQREvResultType;
                                      var lFecha : Boolean );
begin
     lFecha := FALSE;
     case iRegresa of
          1:
          begin
               sCampo := 'TC_MONTO';
               Tipo := resDouble;
          end;
          2:
          begin
               sCampo := 'TC_NUMERO';
               Tipo := resDouble;
          end;
          3:
          begin
               sCampo := 'TC_TEXTO';
               Tipo := resString;
          end
          else
          begin
               sCampo := 'TC_FEC_INI';
               Tipo := resDouble;
               lFecha := TRUE;
          end;
     end;
end;

procedure TZetaTIPO_CAMBIO.GetRequeridos( var TipoRegresa: TQREvResultType );
 var sCampo, sFecha : string;
     lesFecha : Boolean;
begin
     if not ParametrosConstantes then EXIT;
     CaseCampo( DefaultInteger(1,1), sCampo, TipoRegresa, lesFecha );
     EsFecha := lesFecha;

     if ParamVacio( 0 ) then sFecha := EntreComillas(DateToStrSQL(Date))
     else sFecha := ParamCampoFecha(0);
     AgregaScript( Format( GetScript, [ sCampo, sFecha ] ), TipoRegresa, EsFecha );
end;

function TZetaTIPO_CAMBIO.Calculate: TQREvResult;
 var TipoRegresa: TQREvResultType;
     sCampo : string;
     lEsFecha : Boolean;
begin
     CaseCampo( DefaultInteger(1,1), sCampo, TipoRegresa, lEsFecha );
     EsFecha := lEsFecha;
     Result:= EvaluaQuery( Format( GetScript, [ sCampo,EntreComillas(DateToStrSQL(DefaultDate(0,Date))) ] ));
end;

{TZetaFECPAGO}
type
TZetaFECPAGO = class( TZetaConstante )
public
  function Calculate: TQREvResult; override;
end;

function TZetaFECPAGO.Calculate: TQREvResult;
begin
     oZetaProvider.GetDatosPeriodo;
     with Result do
     begin
          Kind := resDouble;
          EsFecha := TRUE;
          dblResult := oZetaProvider.DatosPeriodo.Pago ;
     end;
end;

{TZetaFECHAINI}
type
TZetaFECHAINI = class( TZetaConstante )
public
  function Calculate: TQREvResult; override;
end;

function TZetaFECHAINI.Calculate: TQREvResult;
begin
     oZetaProvider.GetDatosPeriodo;
     with Result do
     begin
          Kind := resDouble;
          EsFecha := TRUE;
          dblResult := oZetaProvider.DatosPeriodo.Inicio ;
     end;
end;

{TZetaFECHAFIN}
type
TZetaFECHAFIN = class( TZetaConstante )
public
  function Calculate: TQREvResult; override;
end;

function TZetaFECHAFIN.Calculate: TQREvResult;
begin
     oZetaProvider.GetDatosPeriodo;
     with Result do
     begin
          Kind := resDouble;
          EsFecha := TRUE;
          dblResult := oZetaProvider.DatosPeriodo.Fin;
     end;
end;

{TZetaFECHAASIINI}
{$ifdef QUINCENALES}
type
TZetaFECHAASIINI = class( TZetaConstante )
public
  function Calculate: TQREvResult; override;
end;

function TZetaFECHAASIINI.Calculate: TQREvResult;
begin
     oZetaProvider.GetDatosPeriodo;
     with Result do
     begin
          Kind := resDouble;
          EsFecha := TRUE;
          dblResult := oZetaProvider.DatosPeriodo.InicioAsis;
     end;
end;

{TZetaFECHAASIFIN}
type
TZetaFECHAASIFIN = class( TZetaConstante )
public
  function Calculate: TQREvResult; override;
end;

function TZetaFECHAASIFIN.Calculate: TQREvResult;
begin
     oZetaProvider.GetDatosPeriodo;
     with Result do
     begin
          Kind := resDouble;
          EsFecha := TRUE;
          dblResult := oZetaProvider.DatosPeriodo.FinAsis;
     end;
end;
{$endif}
{TZetaTURNO}
type
TZetaTURNO = class( TZetaRegresa ) {GA}
private
public
 procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
end;

procedure TZetaTURNO.GetRequeridos( var TipoRegresa: TQREvResultType );
begin
     TipoRegresa := resString;
     AgregaScript( 'COLABORA.CB_TURNO', TipoRegresa, False );
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

{TZetaREINGRESO}
type
TZetaREINGRESO = class( TZetaRegresa ) {GA}
private
public
 function Calculate: TQREvResult; override;
 procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
end;

procedure TZetaREINGRESO.GetRequeridos( var TipoRegresa: TQREvResultType );
begin
     TipoRegresa := resBool;
     AgregaRequerido( 'COLABORA.CB_ACTIVO', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_FEC_BAJ', enEmpleado );
     lQuitarOptimizeCursorB8 := FALSE;         // Si no requiere un query en el calculate, se deja optimizaci�n de Build #8
end;

function TZetaREINGRESO.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resBool;
          with DatasetEvaluador do
          begin
	       { Reingresos son empleados ACTIVOS actualmente pero que alguna vez fueron dados de baja }
	       booResult := zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString ) and ( FieldByName( 'CB_FEC_BAJ' ).AsDateTime > NullDateTime );
          end;
     end;
end;

{TZetaV_FIJAS}
type
TZetaV_FIJAS = class( TZetaRegresaQuery )
private
    sFinal, sAntig, sTabla: String;
public
 procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
 function Calculate: TQREvResult; override;
 function GetScript: String; override;
end;

procedure TZetaV_FIJAS.GetRequeridos( var TipoRegresa: TQREvResultType );
begin
     TipoRegresa := resDouble;
     oZetaProvider.GetDatosActivos;
     AgregaRequerido( 'COLABORA.CB_TABLASS', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_FEC_ANT', enEmpleado );
     if ParametrosConstantes then
     begin
          sFinal := ParamCampoDefFecha( 0, EntreComillas( DateToStrSQL( oZetaProvider.FechaDefault )));
          sAntig := ParamCampoDefFecha( 1, 'COLABORA.CB_FEC_ANT' );
          sTabla := ParamCampoDefString( 2, 'COLABORA.CB_TABLASS' );
	  AgregaScript( Format( GetScript, [ sTabla, sAntig, sFinal ] ), TipoRegresa, False );
          AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     end;
end;

function TZetaV_FIJAS.GetScript: String;
begin
     Result := SelRes('SP_V_FIJAS( %s, %s, %s )');
end;

function TZetaV_FIJAS.Calculate: TQREvResult;
begin
    sFinal := EntreComillas( DateToStrSql( DefaultDate( 0, oZetaProvider.FechaDefault )));
    sAntig := EntreComillas( DateToStrSql(DefaultDate( 1, DatasetEvaluador.FieldByName( 'CB_FEC_ANT' ).AsDateTime)));
    sTabla := EntreComillas( DefaultString ( 2, DatasetEvaluador.FieldByName( 'CB_TABLASS' ).AsString));
    Result:= EvaluaQuery( Format( GetScript, [ sTabla, sAntig, sFinal ] ));
end;

{TZetaREC_PAGADO}
type
TZetaREC_PAGADO = class( TZetaRegresa ) {GA}
private
public
 function Calculate: TQREvResult; override;
 procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
end;

procedure TZetaREC_PAGADO.GetRequeridos( var TipoRegresa: TQREvResultType );
begin
     TipoRegresa := resBool;
     AgregaRequerido( 'NOMINA.NO_FEC_PAG', enEmpleado );
     lQuitarOptimizeCursorB8 := FALSE;         // Si no requiere un query en el calculate, se deja optimizaci�n de Build #8
end;

function TZetaREC_PAGADO.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resBool;
          with DatasetEvaluador do
          begin
               { Recibo pagado se detecta cuando la fecha de pago del recibo no est� vac�a }
	       booResult := ( FieldByName( 'NO_FEC_PAG' ).AsDateTime > NullDateTime );
          end;
     end;
end;

{TZetaN_CURSOS}
type
TZetaN_CURSOS = class( TZetaRegresaQuery )
private
  oLista: TStringList;
public
  constructor Create(oEvaluador:TQrEvaluator); override;
  destructor Destroy; override;
  procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
  function Calculate: TQREvResult; override;
end;

constructor TZetaN_CURSOS.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create(oEvaluador);
     oLista := TStringList.Create;
end;

destructor TZetaN_CURSOS.Destroy;
begin
     oLista.Free;
     inherited Destroy;
end;

procedure TZetaN_CURSOS.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resInt;
     oZetaProvider.GetDatosPeriodo;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     {$ifdef CAMBIO_TNOM}
     if ( EsEntidadNomina ) then
     begin
          AgregaRequerido( 'NOMINA.NO_ASI_INI', enNomina );
          AgregaRequerido( 'NOMINA.NO_ASI_FIN', enNomina );
     end;
     {$endif}
end;

function TZetaN_CURSOS.Calculate: TQREvResult;
var
   dInicial, dFinal: TDate;
   sScript, sCursos, sFiltro: String;
   lTomados: Boolean;
begin
     { Par�metros }
     with oZetaProvider.DatosPeriodo do
     begin
          {$ifdef CAMBIO_TNOM}
          if ( EsEntidadNomina ) then
          begin
               dInicial := DefaultDate( 0, DataSetEvaluador.FieldByName('NO_ASI_INI').AsDateTime );
               dFinal := DefaultDate( 1, DataSetEvaluador.FieldByName('NO_ASI_FIN').AsDateTime );
          end
          else
          begin
          {$endif}
               {$ifdef QUINCENALES}
               dInicial := DefaultDate( 0, InicioAsis );
               dFinal := DefaultDate( 1, FinAsis );
               {$else}
               dInicial := DefaultDate( 0, Inicio );
               dFinal := DefaultDate( 1, Fin );
               {$endif}
          {$ifdef CAMBIO_TNOM}
          end;
          {$endif}
     end;
     sCursos := DefaultString( 2, VACIO );
     sFiltro := DefaultString( 3, VACIO );
     lTomados := DefaultBoolean( 4, True );
     { Nota: Filtro s�lo permite expresiones de servidor }
     if StrLleno( sFiltro ) then
        sFiltro := Parentesis( sFiltro );
     if StrLleno( sCursos ) then
     begin
	  sCursos := Parentesis( 'CU_CODIGO IN ' + Parentesis( ConvierteALista( sCursos, False, oLista ) ) );
	  sFiltro := ConcatFiltros( sFiltro, sCursos );
     end;
     if lTomados then
        sScript := 'select COUNT(*) AS CUANTOS from KARCURSO where ( CB_CODIGO = %d ) and ( KC_FEC_TOM between %s and %s )'
     else
    	 sScript := 'select COUNT(*) AS CUANTOS from CUR_PROG where ( CB_CODIGO = %d ) and ( ( KC_FEC_PRO >= %s ) and ( KC_FEC_PRO <= %s ) ) and ( KC_FEC_TOM is NULL )';
     sScript := Format( ConcatFiltros( sScript, sFiltro ), [ ZetaEvaluador.GetEmpleado, EntreComillas( DateToStrSQL( dInicial ) ), EntreComillas( DateToStrSQL( dFinal ) ) ] );
     Result := EvaluaQuery( sScript );
end;

{TZetaAUTORIZADA}
type
TZetaAUTORIZADA = class( TZetaRegresaQuery )
private
protected
  function GetRegreso : string; virtual;
  function GetTipoAutorizacion : integer; virtual;
  function GetFiltroFechas : string; virtual;
public
  procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
  function Calculate: TQREvResult; override;
end;

procedure TZetaAUTORIZADA.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resInt; //se agreg� para poder usar ZetaEvaluador.GetEmpleado en el Calculate
     oZetaProvider.GetDatosActivos;
     AgregaRequerido( 'AUSENCIA.AU_FECHA', enAusencia );
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;


function TZetaAUTORIZADA.GetRegreso : string;
begin
     Result := DefaultString( 2, 'CH_HOR_ORD+CH_HOR_EXT' );
end;

function TZetaAUTORIZADA.GetTipoAutorizacion : integer;
begin
     Result := DefaultInteger( 0, 1 );

     case Result of
          1: Result := Ord( chExtras );
          2: Result := Ord( chDescanso );
          3: Result := Ord( chConGoce );
          4: Result := Ord( chSinGoce );
          5: Result := Ord( chConGoceEntrada );
          6: Result := Ord( chSinGoceEntrada );
          7: Result := Ord( chPrepagFueraJornada );
     else
         Result := Ord( chPrepagDentroJornada ); //8
     end;

end;


function TZetaAUTORIZADA.GetFiltroFechas : string;
const
   K_FILTRO_FECHAS = ' CHECADAS.AU_FECHA = %s ';
var
 dChecada: TDate;
begin
   if Evaluador.BuscandoRequeridos then
        dChecada := oZetaProvider.FechaDefault
     else
         dChecada := DefaultDate( 4, DatasetEvaluador.FieldByName( 'AU_FECHA' ).AsDateTime );

   Result := Format ( K_FILTRO_FECHAS, [EntreComillas( DateToStrSQL( dChecada ))  ] );
end;


function TZetaAUTORIZADA.Calculate: TQREvResult;
const
     K_SCRIPT = 'select %s AS RESULTADO from CHECADAS where ( CHECADAS.CB_CODIGO = %d ) and ( %s ) and ( CH_H_REAL = ''----'' ) and %s';
var
   iTipo: Integer;
   sScript, sMotivo, sRegresa, sFiltro: String;
begin
     { Par�metros }
     iTipo := GetTipoAutorizacion;
     sMotivo := DefaultString( 1, '' );
     sRegresa := GetRegreso;
     sFiltro := DefaultString( 3, '' );

     { Nota: Filtro s�lo permite expresiones de servidor }
     if StrLleno( sFiltro ) then
     begin
          sFiltro := Parentesis( sFiltro );
     end;

     { MAV: 04/Abr/2003 Se checa que el Motivo no venga Vacio }
     if strLleno( sMotivo ) then
        sMotivo := Parentesis( Format( 'CH_RELOJ = %s', [ EntreComillas( sMotivo ) ] ) );

     sFiltro := ConcatFiltros( sFiltro, Parentesis( 'CH_TIPO = ' + IntToStr( iTipo ) ) );
     sFiltro := ConcatFiltros( sFiltro, sMotivo );


     sScript := Format( K_SCRIPT, [ sRegresa, ZetaEvaluador.GetEmpleado, GetFiltroFechas, sFiltro ] );

     Result := EvaluaQuery( sScript );
end;


{TZetaAUTORIZADA}
type
TZetaAUTORIZADAN = class( TZetaAUTORIZADA )
private
protected
  function GetRegreso : string; override;
  function GetFiltroFechas : string; override;
public
  procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
end;

procedure TZetaAUTORIZADAN.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resInt; //se agreg� para poder usar ZetaEvaluador.GetEmpleado en el Calculate
     oZetaProvider.GetDatosActivos;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );

     if ( EsEntidadNomina ) then
     begin
          AgregaRequerido( 'NOMINA.NO_ASI_INI', enNomina );
          AgregaRequerido( 'NOMINA.NO_ASI_FIN', enNomina );
     end
     else
     begin
          AgregaRequerido( 'AUSENCIA.AU_FECHA', enAusencia );
          oZetaProvider.GetDatosPeriodo;
     end;
end;

function TZetaAUTORIZADAN.GetRegreso : string;
begin
     Result := DefaultString( 2, 'CH_HOR_ORD+CH_HOR_EXT' );
     Result := Format('SUM( %s )' , [Result] );
end;

function TZetaAUTORIZADAN.GetFiltroFechas : string;
const
   K_FILTRO_FECHAS = ' CHECADAS.AU_FECHA between %s and %s  ';
var
 dChecadaIni, dChecadaFin: TDate;
begin

     with oZetaProvider.DatosPeriodo do
     begin
          if ( EsEntidadNomina ) then
          begin
               dChecadaIni := DefaultDate( 4, DataSetEvaluador.FieldByName('NO_ASI_INI').AsDateTime );
               dChecadaFin := DefaultDate( 5, DataSetEvaluador.FieldByName('NO_ASI_FIN').AsDateTime );
          end
          else
          begin
               dChecadaIni := DefaultDate( 4, InicioAsis );
               dChecadaFin := DefaultDate( 5, FinAsis );
          end;
     end;

     Result := Format ( K_FILTRO_FECHAS, [EntreComillas( DateToStrSQL( dChecadaIni ) ),  EntreComillas( DateToStrSQL( dChecadaFin ) )] );
end;

{ ************* TZetaBaseHorario ************* }
type
  TZetaBaseHorario = class( TZetaRegresaQuery )
  private
    function GetHorarioStatus(const sTurno: String; const dFecha: TDate): TStatusHorario;
  public
   constructor Create(oEvaluador:TQrEvaluator); override;
   destructor Destroy; override;
  end;

constructor TZetaBaseHorario.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create(oEvaluador);
     with ZetaEvaluador.oZetaCreator do
     begin
          PreparaQueries;
          PreparaRitmos;
     end;
end;

destructor TZetaBaseHorario.Destroy;
begin
     inherited Destroy;
end;

function TZetaBaseHorario.GetHorarioStatus( const sTurno: String; const dFecha: TDate ): TStatusHorario;
begin
     with ZetaEvaluador.oZetaCreator.Ritmos do
     begin
          { GA: 05/Dic/2001: Hay mucho lugar para optimizaci�n, dado que
          si la fecha no cambia, no es necesario volver a llamar RitmosBegin }
          RitmosBegin( dFecha, dFecha );
          Result:= GetStatusHorario( sTurno, dFecha );
          RitmosEnd;
     end;
end;

{ ************* TZetaFECHA_HORARIO ************* }

type
  TZetaFECHA_HORARIO = class( TZetaBaseHorario )
  private
  public
    procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

procedure TZetaFECHA_HORARIO.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     inherited;
     with oZetaProvider do
     begin
          InitGlobales;
          GetDatosActivos;
     end;
     if ( DefaultInteger( 1, 1 ) = 1 ) then
        TipoRegresa := resInt
     else
        TipoRegresa := resString;
     AgregaRequerido( 'COLABORA.CB_TURNO', enEmpleado );
end;

function TZetaFECHA_HORARIO.Calculate: TQREvResult;
var
   iRegresa: Integer;
   dReferencia: TDate;
   sTurno: String;
begin
     dReferencia := DefaultDate( 0, oZetaProvider.FechaDefault );
     iRegresa := DefaultInteger( 1, 1 );
     sTurno := DefaultString( 2, DatasetEvaluador.FieldByName( 'CB_TURNO' ).AsString );
     with GetHorarioStatus( sTurno, dReferencia ), Result do
     begin
          if ( iRegresa = 1 ) then
	  begin
               Kind := resInt;
               intResult := Ord( Status );
          end
          else
          begin
               Kind := resString;
               strResult := Horario;
          end;
     end;
end;

{ ************* TZetaFECHA_HABIL ************* }

type
  TZetaFECHA_HABIL = class( TZetaBaseHorario )
  private
  public
    function GetScript : String; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
  end;

function TZetaFECHA_HABIL.GetScript : string;
begin
     Result := SelRes('SP_STATUS_EMP( %s, %s )');
end;

procedure TZetaFECHA_HABIL.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     inherited;
     oZetaProvider.GetDatosActivos;
     TipoRegresa := resBool;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_TURNO', enEmpleado );
end;

function TZetaFECHA_HABIL.Calculate: TQREvResult;
var
   dReferencia: TDate;
   sTurno: String;
   eAusencia: eStatusAusencia;
   ResultadoSP: TQREvResult;
begin
     dReferencia := DefaultDate( 0, oZetaProvider.FechaDefault );
     sTurno := Trim(DefaultString( 2, DatasetEvaluador.FieldByName( 'CB_TURNO' ).AsString));
     eAusencia := eStatusAusencia( DefaultInteger( 1, Ord( auHabil ) ) );

     ResultadoSP:= EvaluaQuery( Format( GetScript, [ EntreComillas( DateToStrSQL( dReferencia ) ),
                                                     IntToStr( ZetaEvaluador.GetEmpleado ) ] ) );
     with Result do
     begin
          Kind:= resBool;
          if ( eStatusEmpleado( ResultadoSP.intResult ) <> steEmpleado ) then
             booResult := False
          else
          begin
	       { Regresa TRUE si el Status del horario para ese d�a es igual al par�metro 'nStatus' }
               with GetHorarioStatus( sTurno, dReferencia ) do
		    booResult := ( Status = eAusencia );
          end;
     end;
end;

{TZetaCHECO}
type
TZetaCHECO = class( TZetaRegresaQuery )
private
  function GetScript : string;  override;
public
  function Calculate: TQREvResult; override;
  procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
end;

procedure TZetaCHECO.GetRequeridos( var TipoRegresa: TQREvResultType );
 var sFecha : string;
begin
     TipoRegresa := resBool;
     if ParamVacio( 0 ) then
     begin
          sFecha := 'AUSENCIA.AU_FECHA';
          AgregaRequerido( sFecha, enAusencia );
     end
     else
         sFecha := ParamCampoFecha(0);

     if not ParametrosConstantes then
     begin
          AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
          Exit;
     end;

     AgregaRequerido( Parentesis(Format( GetScript, ['COLABORA.CB_CODIGO',sFecha] )) + ' COUNT_CHECO', enEmpleado );
end;

function TZetaCHECO.GetScript : string;
begin
     Result := 'select ' + CampoRes( 'COUNT(*)' ) + ' from CHECADAS where ( CHECADAS.CB_CODIGO = %s ) and ( CHECADAS.AU_FECHA = %s ) and ( CHECADAS.CH_H_REAL <> ''----'' )';
end;

function TZetaCHECO.Calculate: TQREvResult;
var
   dReferencia: TDate;
   sScript: String;
   COUNT_CHECO : TZetaField;
begin
     COUNT_CHECO := DatasetEvaluador.FindField( 'COUNT_CHECO' );
     if (COUNT_CHECO = NIL) OR not ParametrosConstantes then
     begin
          { Par�metros }
          if ParamVacio( 0 ) then
             dReferencia := DatasetEvaluador.FieldByName( 'AU_FECHA' ).AsDateTime
          else
              dReferencia := ParamDate( 0 );
          sScript := Format( GetScript, [ IntToStr(ZetaEvaluador.GetEmpleado), EntreComillas( DateToStrSQL( dReferencia ) ) ] );
          Result := EvaluaQuery( sScript );
          with Result do
          begin
               Kind := resBool;
               booResult := ( intResult > 0 );
          end;
     end
     else
     begin
          with Result do
          begin
               Kind := resBool;
               booResult := ( COUNT_CHECO.AsInteger > 0 );
          end;
     end;
end;


{TZetaPROM_SAL_MIN}
type
TZetaPROM_SAL_MIN = class( TZetaRegresaQuery )
private
  function GetScript : String; override;
public
  function Calculate: TQREvResult; override;
  procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
end;

function TZetaPROM_SAL_MIN.GetScript: String;
begin
     Result := SelRes('SP_PROM_SAL_MIN( %s, %s, %s )');
end;

procedure TZetaPROM_SAL_MIN.GetRequeridos( var TipoRegresa: TQREvResultType );
begin
     TipoRegresa := resDouble;
     oZetaProvider.GetDatosPeriodo;
     if ParamVacio( 2 ) then
        AgregaRequerido( 'COLABORA.CB_ZONA_GE', enEmpleado );
end;

function TZetaPROM_SAL_MIN.Calculate: TQREvResult;
var
   sFechaIni, sFechaFin, sZonaGeo: String;
begin
     if ParamVacio( 2 ) then
        sZonaGeo := EntreComillas(DatasetEvaluador.FieldByName( 'CB_ZONA_GE' ).AsString)
     else
         sZonaGeo := EntreComillas(ParamString( 2 ));

     sFechaIni := EntreComillas(DateToStrSql( DefaultDate(0, oZetaProvider.DatosPeriodo.Inicio )));
     sFechaFin := EntreComillas(DateToStrSql( DefaultDate(1, oZetaProvider.DatosPeriodo.Fin )));
    Result:= EvaluaQuery( Format( GetScript, [ sFechaIni, sFechaFin, sZonaGeo ] ));
end;

{TZetaOP}
type
TZetaOP = class ( TZetaRegresaQuery )
 public
 function GetScript: String; override;
 function GetScript2: String;
 function GetScript3: String;
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
 function Calculate : TQREvResult; override;
end;

function TZetaOP.GetScript: String;
begin
     Result:= '( Select KF_CODIGO FROM KAR_FIJA where CB_CODIGO = COLABORA.CB_CODIGO and ' +
                 'CB_FECHA = COLABORA.CB_FEC_SAL and ' +
 		 'CB_TIPO = COLABORA.CB_TIP_REV and ' +
                 'KF_FOLIO= %s )';
end;

function TZetaOP.GetScript2: String;
begin
     Result:= SelRes('SP_OP( %s, %s )');
end;

function TZetaOP.GetScript3: String;
begin
     Result:= SelRes('SP_CODIGO_OP( %s, %s, %s, %s )');
end;

procedure TZetaOP.GetRequeridos( var TipoRegresa : TQREvResultType );
 var sSalario : string;
begin
     ValidaConfidencial;
     ParametrosFijos(1);
     TipoRegresa:= resDouble;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     if not ParametrosConstantes then
     begin
          AgregaRequerido( 'COLABORA.CB_SALARIO', enEmpleado );
          AgregaRequerido( 'COLABORA.CB_FEC_SAL', enEmpleado );
          AgregaRequerido( 'COLABORA.CB_TIP_REV', enEmpleado );
          exit;
     end;
     if ParamVacio(1) then
        sSalario := 'COLABORA.CB_SALARIO'
     else sSalario := ParamCampoDouble(1);

     AgregaScript( Format( GetScript2, [ Format( Parentesis(GetScript3), [ 'COLABORA.CB_CODIGO',
                                                               'COLABORA.CB_FEC_SAL',
                                                               'COLABORA.CB_TIP_REV',
                                                               ParamCampoInteger(0) ]),
                                                  sSalario ]),tipoRegresa,False)
end;

function TZetaOP.Calculate: TQREvResult;
var sCodigo: String;
    sEmpleado, sNumero, sTipo, sFecha, sSalario : string;
begin
     with DatasetEvaluador do
     begin
         sEmpleado := IntToStr( FieldByName( 'CB_CODIGO' ).AsInteger );
         sFecha := EntreComillas( DateToStrSQL( FieldByName( 'CB_FEC_SAL' ).AsDateTime ) );
         sTipo := EntreComillas( FieldByName( 'CB_TIP_REV').AsString );
         sNumero := IntToStr( ParamInteger(0) ) ;
     end;
    Result:= EvaluaQuery( Format( GetScript3, [ sEmpleado, sFecha, sTipo, sNumero ] ));

    sCodigo := EntreComillas( Result.strResult );
    sSalario := FloatToStr(DefaultFloat( 1, DatasetEvaluador.FieldByName('CB_SALARIO').AsFloat));
    Result:= EvaluaQuery( Format( GetScript2, [ sCodigo, sSalario ] ));
end;

{TZetaCHECADA}
type
TZetaCHECADA = class ( TZetaSFuncion )
private
 fRegresa : string;
 fFiltro : string;
public
 function GetScript: String; override;
 procedure PreparaAgente; override;
 function Calculate: TQREvResult; override;
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
 function ConcatFiltro( const sFiltro1, sFiltro2 : String ) : String;
 procedure PreparaScriptChecada( var TipoRegresa : TQREvResultType );
end;

function TZetaCHECADA.GetScript: String;
begin
      Result:=  'select %s from CHECADAS ' +
		'where CHECADAS.CB_CODIGO = %s and ' +
		'CHECADAS.AU_FECHA = %s and %s';
end;

procedure TZetaChecada.PreparaAgente;
begin
     with FAgente do
     begin
          AgregaFiltro( 'CHECADAS.CB_CODIGO = :Empleado', TRUE, enChecadas );
          AgregaFiltro('CHECADAS.AU_FECHA = :dFecha', TRUE, enChecadas );
          AgregaFiltro('CHECADAS.CH_TIPO = :iTipo', TRUE, enChecadas );
          AgregaFiltro( 'CHECADAS.CH_POSICIO = :iPos', TRUE, enChecadas );
     end;
end;

procedure TZetaCHECADA.PreparaScriptChecada( var TipoRegresa : TQREvResultType );
var
   sDescrip, sPreFiltro, sTipo: String;
   nTipo, nRegresa, nPos, nDescrip: integer;
   lEsFecha : Boolean;
begin
     sPreFiltro := 'CH_POSICIO = %s and CH_TIPO = %s';
     nPos := DefaultInteger( 0, 1 );
     if ParamVacio( 1 ) then nTipo := 1	// Entrada
     else if Argument( 1 ).Kind = resInt then
          nTipo := ParamInteger( 1 )
     else
     begin
          sTipo := ParamString( 1 );
          if StrVacio( sTipo ) then nTipo := 1
          else nTipo := Pos( sTipo[1], 'ESIFPNA' );
     end;

     if nTipo > 4 then  // Ajusta ch_posicio si se pide informaci�n de una Autorizacion
        nPos := 0;

     if ParamVacio( 2 ) then nRegresa := 1
     else if Argument( 2 ).Kind = resInt then
          nRegresa := ParamInteger( 2 )
     else
     begin
          nRegresa := 0;
          fRegresa := ParamString( 2 );
          TipoRegresa := InvestigaTipo( enChecadas,
                                        fRegresa,
                                        lEsFecha );
          EsFecha := lEsFecha;
          if TipoRegresa = resError then
             raise EErrorEvaluador.Create( ErrorEvaluador( eeFuncionParam,
                                           'El Par�metro #3 no Soporta el Uso de Expresiones',
                                           'Error en la Expresi�n: CHECADA( )' ) )

     end;
     if ( nRegresa > 0 ) then
        case ( nRegresa ) of
             1 : fRegresa := 'CH_H_REAL';	// No son 100% compatibles
             2 : fRegresa := 'CH_H_AJUS';	// pues no hacen el HORA24
             3 :
             begin
                  fRegresa := 'CH_DESCRIP';	// No compatible. Era caracter
                  TipoRegresa := resInt;
             end;
             4 :
             begin
                  fRegresa := 'CH_DESCRIP';	// No compatible. Era Join con TFIJAS
                  TipoRegresa := resInt;
             end;
             5 :
             begin
                  fRegresa := 'CH_HOR_ORD';
                  TipoRegresa := resDouble;
             end;
             6 : fRegresa := 'CH_RELOJ';
             7 : if ( nTipo > 4 ) then fRegresa := 'CH_RELOJ' else fRegresa := 'CH_MOTIVO';
        end;
     fFiLtro  := VACIO;
     if ParamVacio( 4 ) then nDescrip := 0
     else if Argument( 4 ).Kind = resInt then nDescrip := ParamInteger( 4 )
     else
     begin
          sDescrip := ParamString( 4 );
          if StrVacio( sDescrip ) then nDescrip := 0
          else nDescrip := Pos( sDescrip[1], 'ORPEC12DN' );
     end;

     if ( nDescrip > 0 ) then fFiltro := ConcatFiltro( fFiltro, Format( 'CH_DESCRIP = %s',[ IntToStr(nDescrip) ]));
     sPreFiltro := Format( sPreFiltro, [ IntToStr(nPos), IntToStr(nTipo) ] );
     fFiltro    := ConcatFiltro( sPreFiltro, fFiltro );

end;

function TZetaCHECADA.Calculate: TQREvResult;
var
   sRegresa, sDescrip, sTipo : string;
   nTipo, nRegresa, nPos, nDescrip: integer;
   dFecha : TDate;
begin
     dFecha := NullDateTime;
     nPos := DefaultInteger( 0, 1 );
     if ParamVacio( 1 ) then nTipo := 1	// Entrada
     else if Argument( 1 ).Kind = resInt then
          nTipo := ParamInteger( 1 )
     else
     begin
          sTipo := ParamString( 1 );
          if StrVacio( sTipo ) then nTipo := 1
          else nTipo := Pos( sTipo[1], 'ESIFPNA' );
     end;

     if nTipo > 4 then  // Ajusta ch_posicio si se pide informaci�n de una Autorizacion
        nPos := 0;

     if ParamVacio( 2 ) then
        nRegresa := 1
     else if Argument( 2 ).Kind = resInt then
          nRegresa := ParamInteger( 2 )
     else
     begin
          nRegresa := 0;
          sRegresa := ParamString( 2 );
     end;
     case ( nRegresa ) of
          1 : sRegresa := 'CH_H_REAL';	// No son 100% compatibles
          2 : sRegresa := 'CH_H_AJUS';	// pues no hacen el HORA24
          3 : sRegresa := 'CH_DESCRIP';	// No compatible. Era caracter
          4 : sRegresa := 'CH_DESCRIP';	// No compatible. Era Join con TFIJAS
          5 : sRegresa := 'CH_HOR_ORD';
          6 : sRegresa := 'CH_RELOJ';
          7 : if ( nTipo > 4 ) then fRegresa := 'CH_RELOJ' else fRegresa := 'CH_MOTIVO';
     end;
     if ParamVacio( 4 ) then nDescrip := 0
     else
         if Argument( 4 ).Kind = resInt then
            nDescrip := ParamInteger( 4 )
         else
             begin
                  sDescrip := ParamString( 4 );
                  if StrVacio( sDescrip ) then
                     nDescrip := 0
                  else nDescrip := Pos( sDescrip[1], 'ORPEC12DN' );
             end;
     if ( nDescrip > 0 ) then
        fFiltro := ' AND CH_DESCRIP ='+IntToStr(nDescrip);

     PreparaSuperEv( sRegresa, DefaultString( 3, VACIO ),
                     enChecadas, [tgBooleano,tgFloat,tgNumero,tgFecha,tgTexto] );
     CierraDataSet;
     SetParamValue( 'Empleado', tgNumero, ZetaEvaluador.GetEmpleado );

     if ParamVacio(5) then
     begin
        {CV:Va a ser cero cuando se esta Calculando Requeridos.}  
        if ZetaEvaluador.DataSets.Count > 0 then
           dFecha := DataSetEvaluador.FieldByName( 'AU_FECHA' ).AsDateTime;
     end
     else
        dFecha := ParamDate(5);

     SetParamValue( 'dFecha', tgFecha, dFecha );
     SetParamValue( 'iTipo', tgNumero, nTipo );
     SetParamValue( 'iPos', tgNumero, nPos );
     AbreDataSet;
     Result := SuperEv.Value;
end;


procedure TZetaCHECADA.GetRequeridos( var TipoRegresa : TQREvResultType );
 var sFecha : string;
begin
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     if ParamVacio( 5 ) then
        AgregaRequerido( 'AUSENCIA.AU_FECHA', enAusencia );
     if NOT ParametrosConstantes OR NOT ParamVacio( 3 ) then
        Exit;
     TipoRegresa := resString;
     PreparaScriptChecada( TipoRegresa );
     if ParamVacio( 5 ) then
        sFecha := 'AUSENCIA.AU_FECHA'
     else sFecha := ParamCampoFecha( 5 );
     AgregaScript( Format( GetScript, [ fRegresa,
                                        'COLABORA.CB_CODIGO',
                                        sFecha,
                                        fFiltro ] ),
                   TipoRegresa,
                   EsFecha );

end;

function TZetaCHECADA.ConcatFiltro( const sFiltro1, sFiltro2 : String ) : String;
begin
     if StrVacio( sFiltro2 ) then
        Result := sFiltro1
     else if StrVacio( sFiltro1 ) then
             Result := sFiltro2
     else
         Result := '(' + Trim( sFiltro1 ) + ') and (' + Trim( sFiltro2 ) + ')';
end;


{TZetaSUMA_DETALLE}
type
TZetaSUMA_DETALLE = class( TZetaSFuncion )
private
  FTabla : String;
  FEntidad : TipoEntidad;
  procedure PreparaAgente; override;
public
  function Calculate: TQREvResult; override;
  procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
  function SumaDetalle : TQREvResult;
end;

procedure TZetaSUMA_DETALLE.PreparaAgente;
begin
     with FAgente do
     begin
          AgregaFiltro( FTabla + '.CB_CODIGO = :Empleado', TRUE, FEntidad )
     end;
end;

procedure TZetaSUMA_DETALLE.GetRequeridos( var TipoRegresa: TQREvResultType );
begin
     ValidaConfidencial;
     TipoRegresa := resDouble;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

function TZetaSUMA_DETALLE.SumaDetalle : TQREvResult;
begin
     with SuperEv, DatasetEvaluador do
     begin
          CierraDataset;
          SetParamValue( 'Empleado', tgNumero, ZetaEvaluador.GetEmpleado );
          AbreDataSet;
          Result.Kind := resDouble;
          Result.dblResult := SumaRegistros;
     end;
end;

{ *** SUMA_DETALLE( Tabla, Expresion, Filtro ) *** }
function TZetaSUMA_DETALLE.Calculate: TQREvResult;
begin
     if ( NOT Preparado ) then
     begin
          FTabla   := AnsiUpperCase( ParamString( 0 ));
          FEntidad := ZetaEvaluador.oZetaCreator.dmEntidadesTress.GetTipoEntidad( FTabla );
          if FEntidad = enNinguno then
             ErrorFuncion( 'La Tabla Especificada no Existe o no Tiene Relaci�n con la Tabla <COLABORA>', 'Funci�n: ' + Expresion );
          PreparaSuperEv( DefaultString( 1, '1' ), DefaultString( 2, VACIO ),
                          FEntidad, [ tgFloat, tgNumero ] );
     end;
     Result := SumaDetalle;
end;

{TZetaSUMA_KARDEX}
type
TZetaSUMA_KARDEX = class( TZetaSUMA_DETALLE )
protected
  procedure PreparaAgente; override;
public
  function Calculate: TQREvResult; override;
end;

procedure TZetaSUMA_KARDEX.PreparaAgente;
var
   sFiltroTipos : String;
begin
     inherited;
     sFiltroTipos := DefaultString( 0, VACIO );
     if StrLleno( sFiltroTipos ) then
       with FAgente do
          AgregaFiltro( CampoInLista( 'KARDEX.CB_TIPO', sFiltroTipos ), TRUE, FEntidad );
end;

{ *** SUMA_KARDEX( Tipo, Expresion, Filtro ) *** }
function TZetaSUMA_KARDEX.Calculate: TQREvResult;
begin
     if ( NOT Preparado ) then
     begin
          FTabla := 'KARDEX';
          FEntidad := enKardex;
          PreparaSuperEv( DefaultString( 1, '1' ), DefaultString( 2, VACIO ),
                          FEntidad, [ tgFloat, tgNumero ] );
     end;
     Result := SumaDetalle;
end;


{TZetaBAJA_IMSS}
type
TZetaBAJA_IMSS = class( TZetaRegresaQuery )
private
public
  function GetScript: String; override;
  function Calculate: TQREvResult; override;
  procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
end;

function TZetaBAJA_IMSS.GetScript: String;
begin
     Result := 'select TB_IMSS from MOT_BAJA where ( MOT_BAJA.TB_CODIGO = %s )';
end;

procedure TZetaBAJA_IMSS.GetRequeridos( var TipoRegresa: TQREvResultType );
begin
     TipoRegresa := resInt;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     if ParametrosConstantes then
     begin
          if ParamVacio( 0 ) then
             EquivaleScript('MOT_BAJA.TB_IMSS', enMotBaja)
          else
              AgregaScript( Format( GetScript, [ ParamCampoString( 0 ) ] ), TipoRegresa, False );
     end;
end;

function TZetaBAJA_IMSS.Calculate: TQREvResult;
begin
     Result := EvaluaQuery( Format( GetScript, [ EntreComillas( ParamCampoString( 0 ) ) ] ) );
end;

{TZetaBAJA_IMSST}
type
TZetaBAJA_IMSST = class( TZetaRegresaQuery )
private
public
  function GetScript: String; override;
  function Calculate: TQREvResult; override;
  procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
end;

function TZetaBAJA_IMSST.GetScript: String;
begin
//     Result := 'select TB_IMSS from MOT_BAJA where ( MOT_BAJA.TB_CODIGO = %s )';
     Result := 'select case TB_IMSS when 10 then ''' + 'A' + ''' else cast( TB_IMSS as char(1)) end from MOT_BAJA where ( MOT_BAJA.TB_CODIGO = %s )';
end;

procedure TZetaBAJA_IMSST.GetRequeridos( var TipoRegresa: TQREvResultType );
begin
     TipoRegresa := resString;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_MOT_BAJ', enEmpleado );
     if ParametrosConstantes then
     begin
          if ParamVacio( 0 ) then
             AgregaScript( Format( GetScript, [ 'COLABORA.CB_MOT_BAJ' ] ), TipoRegresa, False )
          else
              AgregaScript( Format( GetScript, [ ParamCampoString( 0 ) ] ), TipoRegresa, False );
     end;
end;

function TZetaBAJA_IMSST.Calculate: TQREvResult;
begin
     Result := EvaluaQuery( Format( GetScript, [ EntreComillas( ParamString( 0 ) ) ] ) );
end;

{TZetaSST}
type
TZetaSST = class( TZetaRegresaQuery )
private
public
  function GetScript: String; override;
  function Calculate: TQREvResult; override;
  procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
end;

function TZetaSST.GetScript : string;
begin
     Result := SelRes('SP_STATUS_EMP( %s, %s )');
end;

procedure TZetaSST.GetRequeridos( var TipoRegresa: TQREvResultType );
begin
     TipoRegresa := resString;
     oZetaProvider.GetDatosActivos;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

function TZetaSST.Calculate: TQREvResult;
var
   eEmpleado: eStatusEmpleado;
   sFecha, sEmpleado : string;
begin
     if not ParamVacio( 0 ) and ( Argument( 0 ).Kind = resInt ) then
        eEmpleado := eStatusEmpleado( ParamInteger( 0 ) )
     else
          begin
               sFecha := EntreComillas(DateToStrSql(DefaultDate( 0, oZetaProvider.FechaDefault )));
               sEmpleado := IntToStr( ZetaEvaluador.GetEmpleado );
               Result:= EvaluaQuery( Format( GetScript, [ sFecha, sEmpleado ] ));
               eEmpleado := eStatusEmpleado(Result.IntResult);
          end;
     with Result do
     begin
	  Kind := resString;
	  strResult := ObtieneElemento( lfStatusEmpleado, Ord( eEmpleado ) );
     end;
end;

{ TZetaInvSemana }

type
TZetaInvSemana = class( TZetaRegresaQuery )
protected
  { Protected Declarations }
  FRegresaScript: Integer;
  function AgregaFiltro( const sSQL, sFiltro: String ): String;
public
  { Public Declarations }
  procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
  function GetScript: String; override;
  function Calculate: TQREvResult;override;
end;

function TZetaInvSemana.GetScript: String;
begin
     case FRegresaScript of
          1: Result := 'select SUM( CAF_INV.CF_COMIDAS - CAF_INV.CF_EXTRAS ) %s from CAF_INV ' +
                       'left outer join INVITA on INVITA.IV_CODIGO = CAF_INV.IV_CODIGO ' +
                       'where ( INVITA.CB_CODIGO = %s ) and ' +
                       '( CAF_INV.CF_FECHA >= %s ) and ' +
                       '( CAF_INV.CF_FECHA <= %s ) and ' +
                       '( CAF_INV.CF_TIPO = %s )';
          2: Result := 'select SUM( CAF_INV.CF_COMIDAS ) %s from CAF_INV ' +
                       'left outer join INVITA on INVITA.IV_CODIGO = CAF_INV.IV_CODIGO ' +
                       'where ( INVITA.CB_CODIGO = %s ) and ' +
                       '( CAF_INV.CF_FECHA >= %s ) and ' +
                       '( CAF_INV.CF_FECHA <= %s ) and ' +
                       '( CAF_INV.CF_TIPO = %s )';
          3: Result := 'select SUM( CAF_INV.CF_EXTRAS ) %s from CAF_INV ' +
                       'left outer join INVITA on INVITA.IV_CODIGO = CAF_INV.IV_CODIGO ' +
                       'where ( INVITA.CB_CODIGO = %s ) and ' +
                       '( CAF_INV.CF_FECHA >= %s ) and ' +
                       '( CAF_INV.CF_FECHA <= %s ) and ' +
                       '( CAF_INV.CF_TIPO = %s )';
          4: Result := 'select COUNT(*) %s from CAF_INV ' +
                       'left outer join INVITA on INVITA.IV_CODIGO = CAF_INV.IV_CODIGO ' +
                       'where ( INVITA.CB_CODIGO = %s ) and ' +
                       '( CAF_INV.CF_FECHA >= %s ) and ' +
                       '( CAF_INV.CF_FECHA <= %s ) and ' +
                       '( CAF_INV.CF_TIPO = %s )';
     end;
end;

function TZetaInvSemana.AgregaFiltro( const sSQL, sFiltro: String ): String;
begin
     Result := sSQL;
     if StrLleno( sFiltro ) then
     begin
          Result := Result + ' and ' + Parentesis( sFiltro );
     end;
end;

procedure TZetaInvSemana.GetRequeridos( var TipoRegresa: TQREvResultType );
var
   sInicial, sFinal, sTipoComida: String;
begin
     oZetaProvider.GetDatosPeriodo;
     TipoRegresa := resInt;
     if ParametrosConstantes and not ParamEsCampo( 0 ) then
     begin
          FRegresaScript := DefaultInteger( 0, 1 );
          if ParamVacio( 1 ) then
             sTipoComida := EntreComillas( '1' )
          else
              sTipoComida := ParamCampoString( 1 );
          if ParamVacio( 2 ) then
{$ifdef QUINCENALES}
             sInicial := EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.InicioAsis ) )
{$else}
             sInicial := EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.Inicio ) )
{$endif}
          else
              sInicial := ParamCampoFecha( 2 );
          if ParamVacio( 3 ) then
{$ifdef QUINCENALES}
             sFinal := EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.FinAsis ) )
{$else}
             sFinal := EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.Fin ) )
{$endif}
          else
              sFinal := ParamCampoFecha( 3 );
          AgregaScript( AgregaFiltro( Format( GetScript, [ VACIO, 'COLABORA.CB_CODIGO', sInicial, sFinal, sTipoComida ] ), DefaultString( 4, '' ) ), resString, False );
     end;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

function TZetaInvSemana.Calculate: TQREvResult;
const
     ALIAS_COLUMNA = ' as RESULTADO';
begin
     FRegresaScript := DefaultInteger( 0, 1 );
     Result := EvaluaQuery( AgregaFiltro( Format( GetScript,[ ALIAS_COLUMNA,
                                                    IntToStr( ZetaEvaluador.GetEmpleado ),
{$ifdef QUINCENALES}
                                                    EntreComillas( DateToStrSQL( DefaultDate( 2, oZetaProvider.DatosPeriodo.InicioAsis ) ) ),
                                                    EntreComillas( DateToStrSQL( DefaultDate( 3, oZetaProvider.DatosPeriodo.FinAsis ) ) ),
{$else}
                                                    EntreComillas( DateToStrSQL( DefaultDate( 2, oZetaProvider.DatosPeriodo.Inicio) ) ),
                                                    EntreComillas( DateToStrSQL( DefaultDate( 3, oZetaProvider.DatosPeriodo.Fin ) ) ),
{$endif}
                                                    DefaultString( 1, EntreComillas('1') ) ] ),
                                                    DefaultString( 4, '' ) ) );
end;

{TZetaDiario}
type
TZetaDiario = class( TZetaRegresaQuery )
protected
  { Protected Declarations }
  FRegresaScript: Integer;
  function AgregaFiltro( const sSQL, sFiltro: String ): String;
public
  { Public Declarations }
  procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
  function GetScript: String; override;
end;

function TZetaDiario.GetScript: String;
begin
     case FRegresaScript of
          1: Result := 'select SUM( CAF_COME.CF_COMIDAS - CAF_COME.CF_EXTRAS )%s from CAF_COME ' +
                       'where ( CAF_COME.CB_CODIGO = %s ) and ' +
                       '( CAF_COME.CF_FECHA >= %s ) and ' +
                       '( CAF_COME.CF_FECHA <= %s ) and ' +
                       '( CAF_COME.CF_TIPO = %s )';
          2: Result := 'select SUM( CAF_COME.CF_COMIDAS )%s from CAF_COME ' +
                       'where ( CAF_COME.CB_CODIGO = %s ) and ' +
                       '( CAF_COME.CF_FECHA >= %s ) and ' +
                       '( CAF_COME.CF_FECHA <= %s ) and ' +
                       '( CAF_COME.CF_TIPO = %s )';
          3: Result := 'select SUM( CAF_COME.CF_EXTRAS )%s from CAF_COME ' +
                       'where ( CAF_COME.CB_CODIGO = %s ) and ' +
                       '( CAF_COME.CF_FECHA >= %s ) and ' +
                       '( CAF_COME.CF_FECHA <= %s ) and ' +
                       '( CAF_COME.CF_TIPO = %s )';
          4: Result := 'select COUNT(*)%s from CAF_COME ' +
                       'where ( CAF_COME.CB_CODIGO = %s ) and ' +
                       '( CAF_COME.CF_FECHA >= %s ) and ' +
                       '( CAF_COME.CF_FECHA <= %s ) and ' +
                       '( CAF_COME.CF_TIPO = %s )';
     end;
end;

function TZetaDiario.AgregaFiltro( const sSQL, sFiltro: String ): String;
begin
     Result := sSQL;
     if StrLleno( sFiltro ) then
     begin
          Result := Result + 'AND' + Parentesis( sFiltro );
     end;
end;

procedure TZetaDiario.GetRequeridos( var TipoRegresa: TQREvResultType );
var
   sFecha, sTipoComida: String;
begin
     oZetaProvider.GetDatosActivos;
     TipoRegresa := resInt;
     if ParametrosConstantes and not ParamEsCampo( 0 ) then
     begin
          FRegresaScript := DefaultInteger( 0, 1 );
          if ( FRegresaScript >= 1 ) and ( FRegresaScript <= 4 ) then
          begin
               if ParamVacio(1) then
                  sTipoComida := EntreComillas( '1' )
               else
               begin
                    if Argument(1).Kind = resInt then
                       sTipoComida := ParamCampoInteger(1)
                    else
                       sTipoComida := ParamCampoString(1);
               end;
               if ParamVacio( 2 ) then
                  sFecha := EntreComillas( DateToStrSQL( oZetaProvider.FechaDefault ) )
               else
                  sFecha := ParamCampoFecha( 2 );
               AgregaScript( AgregaFiltro( Format( GetScript, [ VACIO, 'COLABORA.CB_CODIGO', sFecha, sFecha, sTipoComida ] ), DefaultString( 3, '' ) ), resString, False );
          end
          else
               AgregaScript( '0', resInt, False );
     end;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

{TZetaSemanal}
type
  TZetaSemanal = class( TZetaDiario )
  public
    procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
    function Calculate: TQREvResult;override;
end;

procedure TZetaSemanal.GetRequeridos( var TipoRegresa: TQREvResultType );
var
   sInicial, sFinal, sTipoComida: String;
begin
     oZetaProvider.GetDatosPeriodo;
     TipoRegresa := resInt;
     if ParametrosConstantes and not ParamEsCampo( 0 ) then
     begin
          FRegresaScript := DefaultInteger( 0, 1 );
          if ParamVacio( 1 ) then
             sTipoComida := EntreComillas( '1' )
          else
              sTipoComida := ParamCampoString( 1 );
          if ParamVacio( 2 ) then
{$ifdef QUINCENALES}
             sInicial := EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.InicioAsis ) )
{$else}
             sInicial := EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.Inicio ) )
{$endif}
          else
              sInicial := ParamCampoFecha( 2 );
          if ParamVacio( 3 ) then
{$ifdef QUINCENALES}
             sFinal := EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.FinAsis ) )
{$else}
             sFinal := EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.Fin ) )
{$endif}
          else
              sFinal := ParamCampoFecha( 3 );
          AgregaScript( AgregaFiltro( Format( GetScript, [ VACIO, 'COLABORA.CB_CODIGO', sInicial, sFinal, sTipoComida ] ), DefaultString( 4, '' ) ), resString, False );
     end;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

function TZetaSemanal.Calculate: TQREvResult;
const
     ALIAS_COLUMNA = ' as RESULTADO';
begin
     FRegresaScript := DefaultInteger( 0, 1 );
     Result := EvaluaQuery( AgregaFiltro( Format( GetScript,[ ALIAS_COLUMNA,
                                                    IntToStr( ZetaEvaluador.GetEmpleado ),
{$ifdef QUINCENALES}
                                                    EntreComillas( DateToStrSQL( DefaultDate( 2, oZetaProvider.DatosPeriodo.InicioAsis ) ) ),
                                                    EntreComillas( DateToStrSQL( DefaultDate( 3, oZetaProvider.DatosPeriodo.FinAsis ) ) ),
{$else}
                                                    EntreComillas( DateToStrSQL( DefaultDate( 2, oZetaProvider.DatosPeriodo.Inicio) ) ),
                                                    EntreComillas( DateToStrSQL( DefaultDate( 3, oZetaProvider.DatosPeriodo.Fin ) ) ),
{$endif}
                                                    DefaultString( 1, EntreComillas('1') ) ] ),
                                                    DefaultString( 4, '' ) ) );
end;

{TCafeAlDia}
type
TZetaAlDia = class( TZetaDiario )
public
  procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
end;

procedure TZetaAlDia.GetRequeridos( var TipoRegresa: TQREvResultType );
var
   sFecha, sTipoComida: String;
begin
     oZetaProvider.GetDatosPeriodo;
     TipoRegresa := resInt;
     if ParametrosConstantes and not ParamEsCampo( 0 ) and not ParamEsCampo( 1 ) then
     begin
{$ifdef QUINCENALES}
          sFecha := EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.InicioAsis + DefaultInteger( 0, 1 ) - 1 ) );
{$else}
          sFecha := EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.Inicio + DefaultInteger( 0, 1 ) - 1 ) );
{$endif}
          FRegresaScript := DefaultInteger( 1, 1 );
          if ParamVacio( 2 ) then
             sTipoComida := EntreComillas( '1' )
          else
              sTipoComida := ParamCampoString( 2 );
          AgregaScript( AgregaFiltro( Format( GetScript, [ VACIO, 'COLABORA.CB_CODIGO', sFecha, sFecha, sTipoComida ] ), DefaultString( 3, '' ) ), resString, False );
     end;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;


{TZetaEVALUA}
type
TZetaEVALUA = class( TZetaSFuncion )
public
  procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  procedure PreparaAgente; override;
  function Calculate: TQREvResult; override;
end;

procedure TZetaEVALUA.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ValidaConfidencial;
     ParametrosFijos(1);
     oZetaProvider.GetEncabPeriodo;
     TipoRegresa := resDouble;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

procedure TZetaEVALUA.PreparaAgente;
begin
     with FAgente do
     begin
          AgregaFiltro( 'NOMINA.PE_YEAR = :Year', TRUE, enNomina );
          AgregaFiltro( 'NOMINA.PE_TIPO = :Tipo', TRUE, enNomina );
          AgregaFiltro( 'NOMINA.PE_NUMERO = :Numero', TRUE, enNomina );
          AgregaFiltro( 'NOMINA.CB_CODIGO = :Empleado', TRUE, enNomina );
     end;
end;

function TZetaEVALUA.Calculate: TQREvResult;
 var sFormula : string;
begin
     sFormula := Trim(ParamString(0));

     if Length(sFormula) >0 then
     begin
          PreparaSuperEv( sFormula, VACIO,
                          enNomina,
                          [tgBooleano,tgFloat,tgNumero,tgFecha,tgTexto]);
          CierraDataset;
          with oZetaProvider.DatosPeriodo do
          begin
               SetParamValue( 'Year', tgNumero, Year );
               SetParamValue( 'Tipo', tgNumero, Ord( Tipo ) );
               SetParamValue( 'Numero', tgNumero, Numero );
               SetParamValue( 'Empleado', tgNumero, ZetaEvaluador.GetEmpleado );
          end;
          AbreDataSet;
          Result := SuperEv.Value;
          SuperEv.Free;
          SuperEv := NIL;
     end
     else
     begin
          Result.Kind := resDouble;
          Result.dblResult := 0;
     end;
end;

type
  TZetaLOOKUP = class( TZetaRegresa )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  protected
  end;

procedure TZetaLOOKUP.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sScript, sCampo, sTabla : String;
   iPos : integer;
   oEntidad, oEntidadLookUp : TipoEntidad;
   lEsSelect : Boolean;
begin
     TipoRegresa := resString;
     lEsSelect := FALSE;
     if not ParametrosConstantes or ParamEsCampo( 0 ) then
        ErrorParametroTipo( 0, 'Par�metro tiene que ser el Nombre del Campo' );

     sCampo := UpperCase( ParamString( 0 ));
     iPos := Pos('.',sCampo);
     if iPos = 0 then
     begin
          oEntidad := ZetaEvaluador.Entidad;
          sTabla := ZetaEvaluador.oZetaCreator.dmEntidadesTress.NombreEntidad(oEntidad);
     end
     else
     begin
          sTabla := Copy(sCampo,0, iPos-1);
          sCampo := Copy(sCampo,iPos+1, MAXINT);
          oEntidad := ZetaEvaluador.oZetaCreator.dmEntidadesTress.GetTipoEntidad( sTabla );
          if oEntidad = enNinguno then
             ErrorFuncion( 'La tabla:' + sTabla + ' no existe. ', 'LOOKUP' );
     end;

     sScript := ZetaEvaluador.dmExisteCampo.GetDatosLookUp( sCampo, sTabla, oEntidad, oEntidadLookUp, lEsSelect );
     if sScript = '' then
        ErrorFuncion( 'El campo no existe en la Tabla ' + sTabla, sCampo );

     AgregaRequerido( sTabla + '.' + sCampo, oEntidad );
     if lEsSelect then
        AgregaScript( sScript, TipoRegresa, FALSE )
     else
     begin
          //AgregaRequerido( sScript, oEntidadLookUp );
          EquivaleScript( sScript, oEntidadLookUp );
     end;
end;

type
  TZetaUsuario = class( TZetaFunc )
  private
    FDataSet : TClientDataSet;
  public
    constructor Create(oEvaluador:TQrEvaluator); override;
    destructor Destroy; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

constructor TZetaUsuario.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create(oEvaluador);
     FDataSet := TClientDataSet.Create( oZetaProvider );
end;

destructor TZetaUsuario.Destroy;
begin
     FDataSet.Free;
     inherited Destroy;
end;

procedure TZetaUsuario.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resString;
     oZetaProvider.GetDatosActivos;
end;

function TZetaUsuario.Calculate: TQREvResult;
const
     sSQL = 'select US_CODIGO, US_NOMBRE from USUARIO where US_CODIGO = %s';
var
   iUsuario: Integer;
begin
     with oZetaProvider, Result do
     begin
          iUsuario:= DefaultInteger( 0, UsuarioActivo );
          Kind := resString;
          if ( iUsuario = UsuarioActivo ) then
             strResult := NombreUsuario
          else
          begin
               with FDataSet do
               begin
                    if ( not Active ) or ( FieldByName( 'US_CODIGO' ).AsInteger <> iUsuario ) then
                       Data := OpenSQL( Comparte, Format( sSQL, [ IntToStr( iUsuario ) ] ), TRUE );
                    strResult:= FieldByName( 'US_NOMBRE' ).AsString;
               end;
          end;
     end;
end;

{**************************** TZetaDiasDomingo *****************************}
type
  TZetaDiasDomingo = class( TZetaRegresa )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

procedure TZetaDiasDomingo.GetRequeridos(var TipoRegresa: TQREvResultType);
begin
     TipoRegresa := resDouble;
     AgregaRequerido( 'COLABORA.CB_FEC_ING', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_FEC_BAJ', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_ACTIVO',  enEmpleado );
     {$ifdef CAMBIO_TNOM}
     if ( EsEntidadNomina ) then
     begin
          AgregaRequerido( 'NOMINA.NO_ASI_INI', enNomina );
          AgregaRequerido( 'NOMINA.NO_ASI_FIN', enNomina );
     end;
     {$endif}
     lQuitarOptimizeCursorB8 := FALSE;         // Si no requiere un query en el calculate, se deja optimizaci�n de Build #8
     oZetaProvider.GetDatosPeriodo;
end;

procedure AjustaFechas( oDataSet : TZetaCursor; var dInicial, dFinal : TDate );
begin
    with oDataSet do
    begin
        dInicial := rMax( dInicial, FieldByName( 'CB_FEC_ING' ).AsDateTime );
        if NOT zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString ) then
            dFinal := rMin( dFinal, FieldByName( 'CB_FEC_BAJ' ).AsDateTime );
    end;
end;

function TZetaDiasDomingo.Calculate: TQREvResult;
var
   dInicial, dFinal : TDate;
   nDias : Integer;
begin
     if ParamVacio( 0 ) then
     begin
          {$ifdef CAMBIO_TNOM}
          if ( EsEntidadNomina ) then
          begin
               dInicial := DataSetEvaluador.FieldByName('NO_ASI_INI').AsDateTime;
          end
          else
          {$endif}
             {$ifdef QUINCENALES}
             dInicial := oZetaProvider.DatosPeriodo.InicioAsis
             {$else}
             dInicial := oZetaProvider.DatosPeriodo.Inicio
             {$endif}
     end
     else
        dInicial := ParamDate( 0 );
     if ParamVacio( 1 ) then
     begin
          {$ifdef CAMBIO_TNOM}
           if ( EsEntidadNomina ) then
           begin
                dFinal := DataSetEvaluador.FieldByName('NO_ASI_FIN').AsDateTime;
           end
           else
          {$endif}
              {$ifdef QUINCENALES}
              dFinal := oZetaProvider.DatosPeriodo.FinAsis
              {$else}
              dFinal := oZetaProvider.DatosPeriodo.Fin
              {$endif}
     end
     else
        dFinal := ParamDate( 1 );

     AjustaFechas( DataSetEvaluador, dInicial, dFinal );
     nDias := Trunc( dFinal - dInicial ) + 1;
     with Result do
     begin
          Kind := resDouble;
          dblResult := Trunc( nDias / 7 );
 	      if DayOfWeek( dFinal ) <= ( nDias MOD 7 ) then
	        dblResult := dblResult + 1;
     end;
end;

{**************************** TZetaDIAS_HABILES *****************************}
type
  TZetaDIAS_HABILES = class ( TZetaRegresa )
  private
   FPrepared: Boolean;
  public
   destructor Destroy; override;
   function Calculate: TQREvResult; override;
   procedure GetRequeridos( var TipoRegresa : TQREvResultType ); Override;
  end;

destructor TZetaDIAS_HABILES.Destroy;
begin
     if FPrepared then
     begin
          with ZetaEvaluador.oZetaCreator do
          begin
               Ritmos.GetDatosTurnoEnd;
          end;
     end;
     inherited Destroy;
end;

procedure TZetaDIAS_HABILES.GetRequeridos( var TipoRegresa: TQREvResultType);
begin
     TipoRegresa := resDouble;
     AgregaParam( 'DIAS_NT' );
     AgregaParam( 'FALTAS_JU' );
     AgregaParam( 'FALTAS_SU' );
     AgregaParam( 'FALTAS_IN' );
     AgregaParam( 'DIAS_INCA' );
     AgregaRequerido( 'NOMINA.CB_TURNO', enNomina );
     AgregaRequerido( 'COLABORA.CB_FEC_ING', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_FEC_BAJ', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_ACTIVO',  enEmpleado );
     {$ifdef CAMBIO_TNOM}
     AgregaRequerido( 'NOMINA.NO_ASI_INI', enNomina );
     AgregaRequerido( 'NOMINA.NO_ASI_FIN', enNomina );
     {$endif}
     with oZetaProvider do
     begin
          GetDatosPeriodo;
          InitGlobales;
     end;
end;

function TZetaDIAS_HABILES.Calculate: TQREvResult;
var
   dInicial,dFinal: TDate;
   sTurno: String;
   lDescontar: Boolean;
   TipoDia: eStatusAusencia;
   nFaltas,nDiasInca: Integer;
   oDatosTurno: TDatosTurno;
   nPrimerDia: Integer;
begin
     if not FPrepared then
     begin
          with ZetaEvaluador.oZetaCreator do
          begin
               PreparaQueries;
               PreparaRitmos;
               Ritmos.GetDatosTurnoBegin;
          end;
          FPrepared := True;
     end;
     with DatasetEvaluador, Result do
     begin
          {$ifdef CAMBIO_TNOM}
          dInicial   := DefaultDate( 0, DataSetEvaluador.FieldByName('NO_ASI_INI').AsDateTime );
          dFinal     := DefaultDate( 1, DataSetEvaluador.FieldByName('NO_ASI_FIN').AsDateTime );
          {$else}
          {$ifdef QUINCENALES}
          dInicial   := DefaultDate( 0, oZetaProvider.DatosPeriodo.InicioAsis );
          dFinal     := DefaultDate( 1, oZetaProvider.DatosPeriodo.FinAsis );
          {$else}
          dInicial   := DefaultDate( 0, oZetaProvider.DatosPeriodo.Inicio );
          dFinal     := DefaultDate( 1, oZetaProvider.DatosPeriodo.Fin );
          {$endif}
          {$endif}
          sTurno     := DefaultString( 2, FieldByName( 'CB_TURNO' ).AsString );
          lDescontar := DefaultBoolean( 3, TRUE );
          TipoDia    := eStatusAusencia( DefaultInteger( 4, Ord( auHabil )));

          nPrimerDia := oZetaProvider.GetGlobalInteger( K_GLOBAL_PRIMER_DIA );


          Kind       := resInt;
          if ( not lDescontar ) then
             AjustaFechas( DataSetEvaluador, dInicial, dFinal );

          if ( dFinal >= dInicial ) then
          begin
               // EM: Duda, funciona con Turnos R�tmicos? Debe funcionar?

               with ZetaEvaluador.oZetaCreator.Ritmos do
               begin
                    oDatosTurno := GetDatosTurno( sTurno );
                    IntResult   := GetDiasHabiles( oDatosTurno, dInicial, dFinal, nPrimerDia );
               end;
               //IntResult  := 0;//CV Mientras no se quite el comentario anterior, tiene que ponerse en cero.

               if ( lDescontar and ( TipoDia = auHabil )) then
               begin
                    // Son de NOMPARAM
                    nFaltas := FieldByName( 'DIAS_NT' ).AsInteger +
                               FieldByName( 'FALTAS_JU' ).AsInteger +
                               FieldByName( 'FALTAS_SU' ).AsInteger +
                               FieldByName( 'FALTAS_IN' ).AsInteger;

                    nDiasInca := FieldByName( 'DIAS_INCA' ).AsInteger;
                    if ( nDiasInca > 0 ) then
                    begin
                         // Pendiente: Cosultar F_RANGO para ver s�lo HABILES

                    end;
                    IntResult := trunc(MiRound(rMax( IntResult - nFaltas - nDiasInca, 0 ),0));
               end;
          end
          else
              IntResult := 0;
     end;
end;

type
  TZetaRTotPer = class( TZetaRegresa )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaRTotPer.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
     EquivaleScript( 'NOMINA.NO_PERCEPC', enNomina );
end;

type
  TZetaRTotDed = class( TZetaRegresa )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaRTotDed.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
     EquivaleScript( 'NOMINA.NO_DEDUCCI', enNomina );
end;

type
  TZetaRTotNeto = class( TZetaRegresa )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaRTotNeto.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
     EquivaleScript( 'NOMINA.NO_NETO', enNomina );
end;

{ ************* TZetaSALARIO_CLA ************* }

type
  TZetaSALARIO_CLA = class( TZetaRegresaQuery )
  public
    function Calculate: TQREvResult; override;
    function GetScript : string; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  private
    function CaseCampo( iRegresa : Integer ): String;
  end;

function TZetaSALARIO_CLA.GetScript : string;
begin
{$ifdef INTERBASE}
     Result := 'select %s from SP_SALARIO_CLA( %s , %s )';
{$endif}
{$ifdef MSSQL}
     Result := 'select %s = dbo.SP_SALARIO_CLA( %s, %s, %s )';
{$endif}
end;

function TZetaSALARIO_CLA.CaseCampo( iRegresa : Integer ): String;
begin
     case iRegresa of
          1: Result := 'SALARIO';
          2: Result := 'PRESTA';
     else
         ErrorParametro(  'Solo se Permiten Valores 1 a 2', '' );
     end;
end;

procedure TZetaSALARIO_CLA.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sCampo, sRango : String;
   iRegresa: Integer;
begin
     TipoRegresa:= resDouble;
     if ParametrosConstantes then
     begin
          iRegresa := DefaultInteger( 0, 1 );
          sCampo := CaseCampo( iRegresa );
          if ParamVacio(2) then
             sRango := 'COLABORA.CB_RANGO_S'
          else
             sRango := ParamCampoDouble(2);
          AgregaScript( Format( GetScript, [ sCampo,
                                ParamCampoDefString( 1, 'COLABORA.CB_CLASIFI' ),
                                sRango
                 {$ifdef MSSQL} ,IntToStr( iRegresa ) {$endif}
                                ] ), TipoRegresa, FALSE );

     end
     else
     begin
          AgregaRequerido( 'COLABORA.CB_CLASIFI', enEmpleado );
          AgregaRequerido( 'COLABORA.CB_RANGO_S', enEmpleado );
     end;
end;

function TZetaSALARIO_CLA.Calculate: TQREvResult;
var
   sCampo : String;
   Rango  : TPesos;
   iRegresa: Integer;
begin
     iRegresa := DefaultInteger( 0, 1 );
     sCampo := CaseCampo( iRegresa );
     Rango  := DefaultFloat( 2, DataSetEvaluador.FieldByName('CB_RANGO_S').AsFloat );
     Result := EvaluaQuery( Format( GetScript, [ sCampo,
                            EntreComillas( DefaultString( 1, DataSetEvaluador.FieldByName( 'CB_CLASIFI' ).AsString ) ),
                            FloatToStr( Rango )
             {$ifdef MSSQL} ,IntToStr( iRegresa ) {$endif}
                            ] ) );
end;

{ ************* TZetaHorario ************* }

type
  TZetaHORARIO = class( TZetaRegresa )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaHORARIO.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resString;
     EquivaleScript( 'AUSENCIA.HO_CODIGO', enAusencia );
end;

{ ************* TZetaTipoDia ************* }

type
  TZetaTIPODIA = class( TZetaRegresa )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaTIPODIA.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resInt;
     EquivaleScript( 'AUSENCIA.AU_STATUS', enAusencia );
end;
{ ************* TZetaCalcExtras ************* }

type
  TZetaHayAuto = class( TZetaRegresa )
  private
    function Calculate: TQREvResult; override;
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaHayAuto.Calculate: TQREvResult;
begin
     Result.Kind := resBool;
     Result.booResult := DataSetEvaluador.FieldByName('AU_FECHA').AsDateTime > NullDateTime;
end;

procedure TZetaHayAuto.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resBool;
     AgregaRequerido( 'AUSENCIA.AU_FECHA',  enAusencia );
end;

{ ************* TZetaGETHORARIO ************* }

type
  TZetaGetHorario = class( TZetaRegresaQuery )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
    procedure CaseCampo( const iRegresa : Integer; var sCampo : String; var TipoRegresa : TQREvResultType; var lEsFecha : Boolean );
    function GetScript : String;override;

  end;

function TZetaGetHorario.GetScript : String;
begin
     Result := 'Select %s from HORARIO where HORARIO.HO_CODIGO = %s';
end;

procedure TZetaGetHorario.CaseCampo( const iRegresa : Integer; var sCampo : String; var TipoRegresa : TQREvResultType; var lEsFecha : Boolean );
begin
     TipoRegresa := resInt;
     lEsFecha := FALSE; //Actualmente HORARIO no tiene ningun campo FECHA
     case iRegresa of
          1:
          begin
               sCampo := 'HORARIO.HO_INTIME';     { Hora de Entrada en minutos (Ej. 7:00 ---> 7*60 = 420 ) }
               TipoRegresa := resString;
          end;
          2:
          begin
               sCampo := 'HORARIO.HO_OUTTIME';    { Hora de Salida en minutos (Ej. 17:00 ---> 17*60 = 1020 ) }
               TipoRegresa := resString;
          end;
          3: sCampo := 'HORARIO.HO_COMER';      { Tiempo para comer en minutos (Ej. 30 ) }
          4:
          begin
               sCampo := 'HORARIO.HO_JORNADA';    { Jornada diaria en horas (Ej. 9.60 ) }
               TipoRegresa := resDouble;
          end;
          5: sCampo := 'HORARIO.HO_IN_TEMP';    { Minutos de gracia antes de Entrada }
          6: sCampo := 'HORARIO.HO_IN_TARD';    { Minutos de gracia despu�s de Entrada }
          7: sCampo := 'HORARIO.HO_OU_TEMP';    { Minutos de gracia despu�s de Entrada }
          8: sCampo := 'HORARIO.HO_OU_TARD';    { Minutos de gracia despu�s de Salida }
          9:
          begin
               sCampo := 'HORARIO.HO_DOBLES';      { N�mero de horas dobles  (Ej. 3 ) }
               TipoRegresa := resDouble;
          end
          else
              ErrorFuncion( 'Error en Par�metro', 'Solo se Permiten Valores 1 a 9' );
     end;

end;

procedure TZetaGetHorario.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   FCampoRequiere, sHorario : string;
   lEsFecha : Boolean;
begin
     if ParametrosConstantes then
     begin
          CaseCampo( DefaultInteger( 0, 1 ), FCampoREquiere, TipoRegresa, lEsFecha );
          sHorario :=  DefaultString( 1, VACIO );

          if StrVacio( sHorario ) then
             EquivaleScript( FCampoRequiere, enHorario )
          else
              AgregaScript( Format( GetScript, [ FCampoRequiere, EntreComillas( sHorario ) ] ), TipoRegresa, lEsFecha );
     end;
end;

function TZetaGetHorario.Calculate: TQREvResult;
var
   FCampoRequiere : string;
   TipoRegresa : TQREvResultType;
   lEsFecha : Boolean;
begin
     CaseCampo( DefaultInteger( 0, 1 ), FCampoRequiere, TipoRegresa, lEsFecha );
     EsFecha:= lEsFecha;
     if Evaluador.BuscandoRequeridos then
     begin
          Result.Kind := TipoRegresa;
     end
     else
     begin
          Result := EvaluaQuery( Format( GetScript, [ FCampoRequiere, EntreComillas( DefaultString( 1, VACIO ) ) ] ) )
     end;

end;


{*********************** TZetaGafeteOK *********************}

type
  TZetaGafeteOK = class( TZetaConstante )
  private
    function Calculate: TQREvResult; override;
  end;

function TZetaGafeteOK.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resBool;
          booResult := True;
     end;
end;

{*********************** TZetaDIASRANGO *********************}

type TZetaDIASRANGO = class( TZetaFunc )
  private
    FEntidad : TipoEntidad;
    function CaseCampo: string;
    function CaseTabla: string;
  public
    { Public Declarations }
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;


function TZetaDIASRANGO.Calculate: TQREvResult;
 var sCampo : string;
begin
     FEntidad := TZetaEvaluador( Evaluador ).Entidad;
     sCampo := CaseCampo;
     with Result do
     begin
          Kind := resInt;
          with DataSetEvaluador, oZetaProvider.DatosPeriodo do
          begin
               {$ifdef CAMBIO_TNOM}
               if ( EsEntidadNomina ) then
               begin
                    IntResult := GetDiasRango( FieldByName( sCampo + 'FEC_INI' ).AsDateTime,
                                          FieldByName( sCampo + 'FEC_FIN' ).AsDateTime,
                                          DefaultDate( 0, DataSetEvaluador.FieldByName('NO_ASI_INI').AsDateTime ),
                                                          DefaultDate( 1, DataSetEvaluador.FieldByName('NO_ASI_FIN').AsDateTime ) );
               end
               else
               begin
               {$endif}
                   {$ifdef QUINCENALES}
                   IntResult := GetDiasRango( FieldByName( sCampo + 'FEC_INI' ).AsDateTime,
                                              FieldByName( sCampo + 'FEC_FIN' ).AsDateTime,
                                              DefaultDate( 0, InicioAsis ), DefaultDate( 1, FinAsis ) );
                   {$else}
                   IntResult := GetDiasRango( FieldByName( sCampo + 'FEC_INI' ).AsDateTime,
                                              FieldByName( sCampo + 'FEC_FIN' ).AsDateTime,
                                              DefaultDate( 0, Inicio ), DefaultDate( 1, Fin ) );
                   {$endif}
               {$ifdef CAMBIO_TNOM}
               end;
               {$endif}
          end;
{               IntResult := iMax(Trunc( ZetaCommonTools.rMin( FieldByName(sCampo+'FEC_FIN').AsDateTime-1,DefaultDate(1,Fin) ) -
                                        ZetaCommonTools.rMax( FieldByName(sCampo+'FEC_INI').AsDateTime,DefaultDate(0,Inicio) )) +1,0); }
     end;
end;

function TZetaDIASRANGO.CaseCampo: string;
begin
     case FEntidad of
          enIncapacidad : Result := 'IN_';
          enPermiso  : Result := 'PM_';
          enVacacion : Result := 'VA_'
     end;
end;

function TZetaDIASRANGO.CaseTabla: string;
begin
     case FEntidad of
          enIncapacidad : Result := 'INCAPACI.';
          enPermiso  : Result := 'PERMISO.';
          enVacacion : Result := 'VACACION.'
          else
              raise EErrorEvaluador.Create( ErrorEvaluador( eeFuncionParam,
                                            'Funci�n Solamente V�lida en Reportes de Incapacidades/Permisos/Vacaciones.',
                                            'Error en la Expresi�n: DIAS_RANGO( )' ) );
     end;
     Result := Result + CaseCampo;
end;

procedure TZetaDIASRANGO.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa:= resInt;
     oZetaProvider.GetDatosPeriodo;
     FEntidad := TZetaEvaluador( Evaluador ).Entidad;
     AgregaRequerido(CaseTabla + 'FEC_INI', FEntidad );
     AgregaRequerido(CaseTabla + 'FEC_FIN', FEntidad );
     {$ifdef CAMBIO_TNOM}
     if ( EsEntidadNomina ) then
     begin
          AgregaRequerido('NO_ASI_INI', enNomina );
          AgregaRequerido('NO_ASI_FIN', enNomina );
     end;
     {$endif}
end;

type
  TZetaPRESUPUESTO = class( TZetaRegresaQuery )
  private
   function PegaFiltro( sFiltro, sCodigos: string; const nNivel : integer ) : String;
   function CaseCampo(const iRegresa : integer ): string;
   function GetScript: string;override;
  public
   procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
   function Calculate: TQREvResult; override;
  end;

{ TZetaPRESUPUESTO }

function TZetaPRESUPUESTO.GetScript: string;
begin
     Result := 'select %s from CONTEO %s';
end;

function TZetaPRESUPUESTO.CaseCampo(const iRegresa : integer ): string;
begin
     case iRegresa of
          1 : Result := 'SUM( CT_CUANTOS )';
          2 : Result := 'SUM( CT_REAL )';
          3 : Result := 'SUM( CT_NUMERO1 )';
          4 : Result := 'SUM( CT_NUMERO2 )';
          5 : Result := 'SUM( CT_NUMERO3 )';
          6 : Result := 'MAX( CT_TEXTO1 )'
          else Result := 'MAX( CT_TEXTO2 )';
     end;
end;

function TZetaPRESUPUESTO.PegaFiltro( sFiltro, sCodigos: string; const nNivel : integer ) : String;
 function ListaPorComas(const sCodigo : string ) : string;
  var oLista : TStringList;
      i: integer;
 begin
      oLista := TStringList.Create;

      try
         oLista.CommaText := StrTransAll(sCodigo,UnaComilla,'');

         for i:= 0 to oLista.Count -1 do
             Result := Result + ',' + Comillas(oLista[i]);
         Result := Parentesis(Copy(Result,2,MaxInt));
      finally
             oLista.Free;
      end;
 end;
 var
    sFiltroNivel : String;
begin
     sCodigos := UpperCase( sCodigos );
     if StrVacio( sCodigos ) then
        sFiltroNivel := ''
     else if Pos( ',', sCodigos ) = 0 then
     begin
          sFiltroNivel := 'CT_NIVEL_' + IntToStr( nNivel ) + ' = ' + sCodigos
     end
     else
         sFiltroNivel := CampoInLista( 'CT_NIVEL_' + IntToStr( nNivel ), StrTransAll(sCodigos,UnaComilla,'') );

     Result := ConcatFiltros( sFiltro, sFiltroNivel );
end;

function TZetaPRESUPUESTO.Calculate: TQREvResult;
 var sCampo, sFiltro :string;
     i, iCampo: integer;
begin
     iCampo := DefaultInteger( 0, 1 );
     sCampo := CaseCampo( iCampo );

     sFiltro := '';
     for i := 1 to 5 do
         sFiltro := PegaFiltro( sFiltro, ParamCampoDefString( i,'' ), i );

     if ( sFiltro > '' ) then sFiltro := ' WHERE ' + sFiltro;

     Result := EvaluaQuery( Format( GetScript, [ sCampo,sFiltro] ) );
end;

procedure TZetaPRESUPUESTO.GetRequeridos(var TipoRegresa: TQREvResultType);
 var sCampo, sFiltro :string;
     i, iCampo: integer;
begin
     if ParametrosConstantes then
     begin
          iCampo := DefaultInteger( 0, 1 );
          if (iCampo in [1,2] ) then TipoRegresa:= resInt
          else if (iCampo in [3,4,5]) then TipoRegresa:= resDouble
          else TipoRegresa := resString;

          sCampo := CaseCampo( iCampo );

          sFiltro := '';
          for i := 1 to 5 do
              sFiltro := PegaFiltro( sFiltro, ParamCampoDefString( i,'' ), i );


          if ( sFiltro > '' ) then sFiltro := ' WHERE ' + sFiltro;

          AgregaScript( Format( GetScript, [ sCampo,
                                             sFiltro] ),
                        TipoRegresa, FALSE );
     end;
end;
{*********************** TZetaJEFE *********************}
type
  TZetaJEFE = class( TZetaRegresa )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaJEFE.GetRequeridos( var TipoRegresa : TQREvResultType );
 const
      {$ifdef INTERBASE}
      Q_SCRIPT = 'select RESULTADO from SP_JEFE( COLABORA.CB_CODIGO )';
      {$endif}
      {$ifdef MSSQL}
      Q_SCRIPT = 'select RESULTADO = DBO.SP_JEFE( COLABORA.CB_CODIGO )';
      {$endif}
begin
     TipoRegresa := resInt;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     AgregaScript( Q_SCRIPT, TipoRegresa, False );
end;

{*********************** TZetaDIASRANGO *********************}
type
  TZetaPOSICIONES = class( TZetaRegresaQuery )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
    function GetScript: string;override;
  end;

function TZetaPOSICIONES.GetScript: string;
begin
     {$ifdef INTERBASE}
     Result := 'select RESULTADO from SP_POSICIONES( %s, %s )'
     {$endif}
     {$ifdef MSSQL}
     Result := 'select RESULTADO = DBO.SP_POSICIONES( %s, %s )'
     {$endif}
end;

procedure TZetaPOSICIONES.GetRequeridos( var TipoRegresa : TQREvResultType );
 {var
    sFecha: string;}
begin
     TipoRegresa := resInt;
     ParametrosFijos( 1 );
     if ParametrosConstantes then
     begin

          {if ParamVacio(1) then
             sFecha := DateToStrSQLC( Date )
          else
          begin
               if ParamEsCampo( 1 ) then
                  sFecha := ParamCampoString( 1 )
               else
                   sFecha := DateToStrSQLC( ParamDate( 1 ) );
          end;}

          AgregaScript( Format( GetScript ,[ParamCampoString(0), ParamCampoDefFecha( 1, DateToStrSQLC( Date ) ) ]), TipoRegresa, False );
     end;
end;

function TZetaPOSICIONES.Calculate: TQREvResult;
 {var
    sPuesto : string;}
begin
     {if ParamEsCampo(0) then
        sPuesto := ParamCampoString(0)
     else
         sPuesto := Comillas( ParamString(0) );}

     Result := EvaluaQuery( Format( GetScript, [ EntreComillas( ParamString(0) ), DateToStrSQLC(DefaultDate(1, Date)) ] ) );
end;

{ ************* TZetaDatosJefe ************* }
type
  TZetaDATOSJEFE = class( TZetaRegresaQuery )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

procedure TZetaDATOSJEFE.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos( 1 );
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

function TZetaDATOSJEFE.Calculate: TQREvResult;
 var sCampoRequiere: string;
     iEmpleado, iRegresa : integer;
begin
     iEmpleado := 0;
     if NOT Evaluador.BuscandoRequeridos then
     begin
          iEmpleado := DefaultInteger( 1 , DataSetEvaluador.FieldByName('CB_CODIGO').AsInteger );
     end;

     if ( Argument( 0 ).Kind = resString ) then
        sCampoRequiere := ParamString( 0 )
     else
     begin
          iRegresa := ParamInteger( 0 );
          case iRegresa of
               1 : sCampoRequiere := K_PRETTYNAME;
               2 : sCampoRequiere := 'CB_TURNO';
               3 : sCampoRequiere := 'CB_PUESTO';
               4 : sCampoRequiere := 'CB_CLASIFI';
               5..13: sCampoRequiere := Format( 'CB_NIVEL%d', [ iRegresa - 4] );
               
               {OP(24/Oct/2008): Cambios de cambios de ACS. }
               {$ifdef ACS}
               110..112: sCampoRequiere := Format( 'CB_NIVEL%d', [ iRegresa - 100 ] );
               else
                   ErrorFuncion( 'Error en Par�metro', 'Solo se Permiten Valores 1 a 13 y de 110 a 112' );
               {$else}
               else
                   ErrorFuncion( 'Error en Par�metro', 'Solo se Permiten Valores 1 a 13' );
               {$endif}
          end;
     end;
     Result := EvaluaQuery( Format( 'SELECT %s FROM COLABORA WHERE COLABORA.CB_CODIGO = %d', [ sCampoRequiere, iEmpleado ] ) );
end;

{ ************* TZetaPuestoJefe ************* }
type
  TZetaPUESTOJEFE = class( TZetaRegresaQuery )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

procedure TZetaPUESTOJEFE.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos( 1 );
     AgregaRequerido( 'PUESTO.PU_REPORTA', enPuesto );
end;

function TZetaPUESTOJEFE.Calculate: TQREvResult;
 var sCampoRequiere, sPuesto: string;
begin
     sPuesto := VACIO;

     if NOT Evaluador.BuscandoRequeridos then
     begin
          sPuesto := DefaultString( 1 , DataSetEvaluador.FieldByName('PU_REPORTA').AsString );
     end;

     if ( Argument( 0 ).Kind = resString ) then
        sCampoRequiere := ParamString( 0 )
     else
     begin
          case ParamInteger( 0 ) of
               1 : sCampoRequiere := 'PU_DESCRIP';
               2 : sCampoRequiere := 'PU_NUMERO';
               3 : sCampoRequiere := 'PU_TEXTO';
               4 : sCampoRequiere := 'PU_INGLES';
               5 : sCampoRequiere := 'PU_CLASIFI';
               6 : sCampoRequiere := 'PU_DETALLE';
               else
                   ErrorFuncion( 'Error en Par�metro', 'Solo se Permiten Valores 1 a 6' );
          end;
     end;
     Result := EvaluaQuery( Format( 'SELECT %s FROM PUESTO WHERE PUESTO.PU_CODIGO = %s', [ sCampoRequiere, EntreComillas( sPuesto ) ] ) );
end;

{ ************* TZetaSalPeriodo ************* }
type
  TZetaSalPeriodo = class( TZetaRegresaQuery )
  public
    function Calculate: TQREvResult; override;
    function GetScript : string; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaSalPeriodo.GetScript : string;
begin
     Result := SelRes('SP_SUMA_SALARIO( %s, %s, %s)');
end;

procedure TZetaSalPeriodo.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sTopeDias :string;
   sInicial : string;
   dInicial,dFinal : TDate;
begin
     ValidaConfidencial;
     oZetaProvider.GetDatosPeriodo;
     TipoRegresa:= resDouble;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     with oZetaProvider.DatosPeriodo do
     begin
          dFinal   := Fin;
          dInicial := Inicio;
     end;
     if ParametrosConstantes then
     begin
          sInicial:= ParamCampoDefFecha( 0, DateToStrSQLC( dInicial ) );
          sTopeDias:= ParamCampoDefInteger( 1, Trunc( dFinal - dInicial ) + 1  );
          AgregaScript( Format( GetScript, ['COLABORA.CB_CODIGO',sInicial, sTopeDias ] ), TipoRegresa, FALSE );
     end;
end;

function TZetaSalPeriodo.Calculate: TQREvResult;
var
   iTopeDias : Integer;
   sInicial : string;
   dInicial,dFinal : TDate;
begin
     with oZetaProvider.DatosPeriodo do
     begin
          dFinal   := Fin;
          dInicial := Inicio;
     end;
     sInicial  := DateToStrSQLC( DefaultDate( 0, dInicial ) );
     iTopeDias := DefaultInteger( 1, Trunc( dFinal - dInicial ) + 1 );
     Result    := EvaluaQuery( Format( GetScript, [ IntToStr( ZetaEvaluador.GetEmpleado ),sInicial ,IntToStr( iTopeDias ) ] ) );
end;
{ ************* TZetaJefeP ************* }
type  TZetaJefeP = class( TZetaRegresaQuery )
public
   function Calculate: TQREvResult; override;
   function GetScript : string; override;
   procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
end;

function TZetaJefeP.GetScript : string;
begin
     Result := 'select C.CB_CODIGO from COLABORA C WHERE ( C.CB_PLAZA = ( select PL_REPORTA from PLAZA where ( PL_FOLIO = %s ) ) ) ';
end;

procedure TZetaJefeP.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resInt;
     ParametrosFijos(1);
end;

function TZetaJefeP.Calculate: TQREvResult;
 Var
    iPlaza:Integer;
begin
     iPlaza := ParamInteger(0);
     Result := EvaluaQuery( Format( GetScript, [ IntToStr( iPlaza ) ] ) );
end;

{ ************* TZetaJefeE ************* }
type  TZetaJefeE = class( TZetaRegresaQuery )
public
      function Calculate: TQREvResult; override;
      function GetScript : string; override;
      procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
end;

function TZetaJefeE.GetScript : string;
begin
     Result := 'select C.CB_CODIGO from COLABORA C where ( C.CB_PLAZA = ( select pl_reporta from plaza where plaza.cb_codigo = %s ) )';
end;
procedure TZetaJefeE.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resInt;
     ParametrosFijos(1);
end;

function TZetaJefeE.Calculate: TQREvResult;
var
   iEmpleado:Integer;
begin
     iEmpleado := ParamInteger(0);
     Result := EvaluaQuery( Format( GetScript, [ IntToStr( iEmpleado ) ] ) );
end;
{************************TZetaHCAL*****************************}
type
  TZetaHCAL = class( TZetaCALENDARIO )
  protected
    function ObtieneCondicion: String;override;
    function UsarDiccion: Boolean; override;
  end;

function TZetaHCAL.ObtieneCondicion: String;
const
     Q_SP_STATUS_EMP = 'SP_STATUS_EMP(AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO)';
     Q_STATUS_EMP_FUN = 'STATUS_EMP( AUSENCIA.AU_FECHA )';
     K_I_FALTA_INJUSTIFICADA = 'FI';
     K_I_RETARDO = 'RE';

     function GetStatusEmp:string;
     begin

        if PreparoAgente then
            Result := Q_STATUS_EMP_FUN
        else
             Result := '( '+SelRes(Q_SP_STATUS_EMP)+ ' )';
     end;
begin
     Case FRegresa of
           1: Result := GetStatusEmp +' = ' + IntToStr( Ord( steEmpleado ) ) +' AND '+'AUSENCIA.AU_TIPO = ''' + K_I_FALTA_INJUSTIFICADA + '''';
           2: Result := GetStatusEmp +' = ' + IntToStr( Ord( steJustificada ) );
           8: Result := 'AUSENCIA.AU_TIPO = ''' + K_I_RETARDO + '''';
          10: Result := GetStatusEmp +' = ' + IntToStr( Ord( steConGoce ) );          // Permisos de Dias Con Goce
          11: Result := GetStatusEmp +' = ' + IntToStr( Ord( steSinGoce ) );          // Permisos de Dias Sin Goce
          12: Result := GetStatusEmp +' = ' + IntToStr( Ord( steSuspension ) );       // Suspensiones
          13: Result := GetStatusEmp +' = ' + IntToStr( Ord( steOtroPermiso ) );      // Permisos Otros
          14: Result := GetStatusEmp +' = ' + IntToStr( Ord( steVacaciones ) );       // Vacaciones
          15: Result := GetStatusEmp +' = ' + IntToStr( Ord( steIncapacidad ) );      // #Incapacidades
     else
         Result:= VACIO;
     end;

end;

function TZetaHCAL.UsarDiccion: Boolean;
begin
     Result := FALSE;
end;


{************************TZetaHCAL_CONSE*****************************}
type
  TZetaHCAL_CONSE = class( TZetaCALCONSEC )
  private
    function TipoDia(eTipoDia:eTipoDiaAusencia):Integer;
  protected
    function GetFiltroTipoDia(eTipoDia:eTipoDiaAusencia):string;override;
  end;

function TZetaHCAL_CONSE.TipoDia(eTipoDia: eTipoDiaAusencia):Integer;
begin
     case eTipoDia of
       daNormal:      Result:= Ord(steEmpleado);
       daIncapacidad: Result:= Ord(steIncapacidad);
       daVacaciones:  Result:= Ord(steVacaciones);
       daConGoce:     Result:= Ord(steConGoce);
       daSinGoce:     Result:= Ord(steSinGoce);
       daJustificada: Result:= Ord(steJustificada);
       daSuspension:  Result:= Ord(steSuspension);
       daOtroPermiso: Result:= Ord(steOtroPermiso);
       else           Result:= -1;
     end;
end;

function TZetaHCAL_CONSE.GetFiltroTipoDia(eTipoDia: eTipoDiaAusencia): string;
const
     Q_SP_STATUS_EMP = 'SP_STATUS_EMP(AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO)';

     function GetStatusEmp:string;
     begin
        Result := '( '+SelRes(Q_SP_STATUS_EMP)+ ' )';
     end;

begin
     Result := GetStatusEmp +' = '+ IntToStr( TipoDia( eTipoDia ) );
end;

{*********************** TZETAHFALTAS_CO *********************}

type
  TZetaHFALTAS_CO = class( TZetaFALTASCONSEC )
  protected
    procedure AgregaFiltroFI;override;
  end;

procedure TZetaHFALTAS_CO.AgregaFiltroFI;
const
     Q_SP_STATUS_EMP = 'SP_STATUS_EMP(AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO)';

     K_ST_ACTIVO = '1';

     function GetStatusEmp:string;
     begin
        Result := '( '+SelRes(Q_SP_STATUS_EMP)+ ' )';
     end;
begin
     FAgente.AgregaFiltro( GetStatusEmp +' = '+ K_ST_ACTIVO +' and '+ 'AUSENCIA.AU_TIPO = ''FI''', TRUE, enAusencia );
end;

{*********************** Funciones para obtener la cuenta contable *********************}

 { ************* TZetaSubCuenta ************* }
type
 TZetaSubCuenta = class( TZetaRegresa )
  protected
    FParametrosFijos: integer;
    FCampo: string;
    function GetCampo: string;virtual;
    procedure Validaciones;virtual;
    procedure AgregaRequeridos;virtual;
  public
    constructor Create(oEvaluator:TQrEvaluator); override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;


  end;

constructor TZetaSubCuenta.Create(oEvaluator: TQrEvaluator);
begin
     inherited;
     FParametrosFijos := 1;
     FCampo:= VACIO;
end;

procedure TZetaSubCuenta.Validaciones;
begin
end;

procedure TZetaSubCuenta.AgregaRequeridos;
begin
end;


function TZetaSubCuenta.GetCampo: string;
begin
     Result := FCampo;
end;

procedure TZetaSubCuenta.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos( FParametrosFijos );
     TipoRegresa := resString;

     Validaciones;
     AgregaRequeridos;

     EquivaleScript( GetCampo, ZetaEvaluador.Entidad );
end;


 { ************* TZetaSubCuentaNivel ************* }
type
  TZetaSubCuentaNivel = class( TZetaSubCuenta )
  protected
    function GetCampo: string;override;
    procedure Validaciones;override;
    procedure AgregaRequeridos;override;
  end;

function TZetaSubCuentaNivel.GetCampo: string;
begin
     Result := 'NIVEL' + IntToStr( ParamInteger( 0 )) + '.TB_SUB_CTA';
end;

procedure TZetaSubCuentaNivel.AgregaRequeridos;
begin
     {$ifdef ACS}
     if( ParamInteger( 0 ) > 9 ) and ( ParamInteger( 0 ) < 13 ) then
          AgregaRequerido( 'NIVEL' + IntToStr( ParamInteger( 0 )) + '.TB_SUB_CTA', TipoEntidad( Ord( enNivel10 ) + ParamInteger( 0 ) - 10 ) )
     else
     {$endif}
         AgregaRequerido( 'NIVEL' + IntToStr( ParamInteger( 0 )) + '.TB_SUB_CTA', TipoEntidad( Ord( enNivel1 ) + ParamInteger( 0 ) - 1 ) );
end;

procedure TZetaSubCuentaNivel.Validaciones;
begin
     {$ifdef ACS}
     if ( ParamInteger( 0 ) < 1 ) or ( ParamInteger( 0 ) > 12 ) then             //MV(23/Oct/2008): Cambios Niveles de ACS  
        ErrorFuncion( 'Error en Par�metro', 'Solo se Permiten Valores 1 a 12' );
     {$else}
     if ( ParamInteger( 0 ) < 1 ) or ( ParamInteger( 0 ) > 9 ) then
        ErrorFuncion( 'Error en Par�metro', 'Solo se Permiten Valores 1 a 9' );
     {$endif}
end;

 { ************* TZetaSubCuentaEmpleado ************* }
type
  TZetaSubCuentaEmpleado = class( TZetaSubCuenta )
  public
    constructor Create(oEvaluator:TQrEvaluator); override;
    procedure AgregaRequeridos;override;

  end;

procedure TZetaSubCuentaEmpleado.AgregaRequeridos;
begin
     AgregaRequerido(FCampo, enEmpleado);
end;

constructor TZetaSubCuentaEmpleado.Create(oEvaluator:TQrEvaluator);
begin
     inherited;
     FParametrosFijos := 0;
     FCampo := 'COLABORA.CB_SUB_CTA';
end;

 { ************* TZetaSubCuentaConcepto ************* }
type
  TZetaSubCuentaConcepto = class( TZetaSubCuenta )
  protected
    procedure AgregaRequeridos;override;
  public
    constructor Create(oEvaluator:TQrEvaluator); override;
  end;

procedure TZetaSubCuentaConcepto.AgregaRequeridos;
begin
     AgregaRequerido( 'CONCEPTO.CO_NUMERO', enConcepto );
end;

constructor TZetaSubCuentaConcepto.Create(oEvaluator: TQrEvaluator);
begin
     inherited;
     FParametrosFijos := 0;
     FCampo := 'CONCEPTO.CO_SUB_CTA';
end;


{ ************* TZetaSubCuentaTabla ************* }
type
  TZetaSubCuentaTabla = class( TZetaSubCuenta )
  protected
    function GetCampo: string;override;
    procedure AgregaRequeridos;override;
  public
    constructor Create(oEvaluator:TQrEvaluator); override;
  end;

constructor TZetaSubCuentaTabla.Create(oEvaluator: TQrEvaluator);
begin
     inherited;
     FParametrosFijos := 1;
end;

procedure TZetaSubCuentaTabla.AgregaRequeridos;
 var
    eEntidad: TipoEntidad;
begin

     case ParamInteger(0) of
          1: eEntidad := enPuesto;
          2: eEntidad := enTurno;
          3: eEntidad := enClasifi;
          else
              eEntidad := enPlazas;
     end;

     AgregaRequerido( GetCampo, eEntidad );

end;

function TZetaSubCuentaTabla.GetCampo: string;
begin
     case ParamInteger(0) of
          1: Result := 'PUESTO.PU_SUB_CTA';
          2: Result := 'TURNO.TU_SUB_CTA';
          3: Result := 'CLASIFI.TB_SUB_CTA'
          else
              Result := 'PLAZA.PL_SUB_CTA';
     end;
end;

{ ************** TZetaCERT_MAX **************** }

type
  TZetaCERT_MAX = class( TZetaRegresaQuery )
  public
   FCertificacion:string;
   function Calculate: TQREvResult; override;
   function GetScript : String; override;
   procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaCERT_MAX.GetScript : string;
begin
     Result := 'Select MAX (KI_FEC_CER) from KAR_CERT '+
               'where CB_CODIGO = %s and CI_CODIGO = %s and KI_FEC_CER <= %s ';
end;

procedure TZetaCERT_MAX.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sEmpleado: String;
begin
     ParametrosFijos( 1 );
     oZetaProvider.GetDatosActivos;

     TipoRegresa := resDouble;
     EsFecha := True;

     if ParamVacio(2) then
     begin
          sEmpleado := 'COLABORA.CB_CODIGO';
          AgregaRequerido( sEmpleado, enEmpleado );
     end
     else
         sEmpleado := ParamCampoInteger(2);

     FCertificacion := ParamCampoString(0);

     if ParametrosConstantes then
                AgregaScript( Format( GetScript, [sEmpleado,
                              FCertificacion,
                              ParamCampoDefFecha( 1,DateToStrSQLC( oZetaProvider.FechaDefault ) )] ),TipoRegresa, EsFecha );
end;

function TZetaCERT_MAX.Calculate : TQREvResult;
var
   iEmpleado: Integer;
begin
     Result.Kind:= resDouble;
     Result.resFecha:= TRUE;

     if ParamVacio(2) then
        iEmpleado := ZetaEvaluador.GetEmpleado
     else
         iEmpleado:= ParamInteger(2);

     FCertificacion := ParamString(0);

     Result:= EvaluaQuery( Format( GetScript, [ IntToStr( iEmpleado ),EntreComillas(FCertificacion),DateToStrSQLC( DefaultDate( 1, oZetaProvider.FechaDefault ) ) ] ) );
end;

{*********************** TZetaBimestreNomina ************************}

type
  TZetaBimestreNomina = class( TZetaRegresaQuery )
  private
    FAsiFechaInicio: TDate;
    FAsiFechaFin: TDate;
    FNomFechaInicio: TDate;
    FNomFechaFin: TDate;
    FRegresa :Integer;
    FScript:Integer;
    FYear: Integer;
    FMes: Integer;
    FTipo: eTipoPeriodo;
    iMesIni:Integer;
    iMesFin:Integer;
    FTipoFecha:Integer;
    FNumPeriodos:Integer;
    procedure GetFechaBimestre(  const iYear, iMes: Integer; const TipoPer: eTipoPeriodo );
    function GetFechaInicio:TDate;virtual;
    function GetFechaFin:TDate;virtual;
    function GetFechaCalInicio:TDate;virtual;
    function GetFechaCalFin:TDate;virtual;
    function GetDiasCal:Integer;virtual;
    function GetDiasNom:Integer;virtual;
    function GetNumPeriodos: Integer;virtual;
    procedure GetFechas;virtual;
  public
    function GetScript : String; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaBimestreNomina.GetScript: String;
begin
     case FScript of
       1: Result := 'select PE_ASI_INI,PE_FEC_INI from PERIODO where PE_YEAR = %d and PE_TIPO = %d and PE_MES = %d and PE_POS_MES = 1 and PE_USO = 0';
       2: Result := 'select PE_ASI_FIN,PE_FEC_FIN from PERIODO where PE_YEAR = %d and PE_TIPO = %d and PE_MES = %d and PE_POS_MES = PE_PER_MES and PE_USO = 0';
       7: Result := 'select Count(*) as Cuantos from PERIODO where PE_YEAR = %d and PE_TIPO = %d and PE_MES in (%d,%d) and PE_USO = 0';
     end;
end;

procedure TZetaBimestreNomina.GetFechaBimestre( const iYear, iMes: Integer; const TipoPer: eTipoPeriodo );
begin
     AbreQuery( Format( GetScript, [ iYear, Ord( TipoPer ), iMes ] ) );
     with FQuery do
     begin
          if ( not EOF ) then
          begin
               if FScript = 1 then
               begin
                    FAsiFechaInicio := FieldByName( 'PE_ASI_INI' ).AsDateTime;
                    FNomFechaInicio := FieldByName( 'PE_FEC_INI' ).AsDateTime;
               end
               else if FScript = 2 then
               begin
                    FAsiFechaFin := FieldByName( 'PE_ASI_FIN' ).AsDateTime;
                    FNomFechaFin := FieldByName( 'PE_FEC_FIN' ).AsDateTime;
               end;
          end;
     end;
end;

procedure TZetaBimestreNomina.GetRequeridos( var TipoRegresa: TQREvResultType);
begin
     TipoRegresa := resDouble;
     EsFecha := TRUE;
     ParametrosFijos( 1 );
     oZetaProvider.GetDatosPeriodo;
     if not ParamEsCampo(0) then
     begin
          FRegresa:= DefaultInteger( 0, 1 );
          case FRegresa of
          5: begin
                   TipoRegresa := resInt;
                   EsFecha := False;
              end;
          6: begin
                  TipoRegresa := resInt;
                  EsFecha := False;
             end;
          7: begin
                  TipoRegresa := resInt;
                  EsFecha := False;
             end;
          end;
     end;
end;
function TZetaBimestreNomina.GetFechaInicio: TDate;
begin
     if FTipoFecha = 0 then
        Result := FAsiFechaInicio
     else
         Result := FNomFechaInicio;

end;
function TZetaBimestreNomina.GetFechaFin: TDate;
begin
     if FTipoFecha = 0 then
        Result := FAsiFechaFin
     else
         Result :=FNomFechaFin;
end;
function TZetaBimestreNomina.GetFechaCalFin: TDate;
begin
     Result := EncodeDate(FYear,iMesFin,MaxDay(FYear,iMesFin));
end;
function TZetaBimestreNomina.GetFechaCalInicio: TDate;
begin
     Result := EncodeDate(FYear,iMesIni,1);
end;
function TZetaBimestreNomina.GetDiasCal: Integer;
var
   dFechaIni,dFechaFin:TDate;
begin
     dFechaIni := GetFechaCalInicio;
     dFechaFin := GetFechaCalFin;
     Result := DaysBetween( dFechaIni, dFechaFin);
end;
function TZetaBimestreNomina.GetDiasNom: Integer;
begin
     Result := DaysBetween( GetFechaInicio, GetFechaFin );
end;
function TZetaBimestreNomina.GetNumPeriodos: Integer;
begin
     Result := 0;
     FScript := 7;
     AbreQuery( Format( GetScript, [ FYear, Ord( FTipo ), iMesIni,iMesFin  ] ) );
     with FQuery do
     begin
          if ( not EOF ) then
             Result := FieldByName( 'Cuantos' ).AsInteger;
     end;
end;

procedure TZetaBimestreNomina.GetFechas;
begin
     FScript := 1;
     GetFechaBimestre( FYear, iMesIni, FTipo );

     FScript := 2;
     GetFechaBimestre( FYear, iMesFin, FTipo );
end;

function TZetaBimestreNomina.Calculate : TQREvResult;
const
     K_FECHA_INICIO = 1;
var
   iBimestre: Integer;
begin
     FRegresa:= DefaultInteger( 0, 1 );
     FTipoFecha := DefaultInteger( 1, 0 );
     with oZetaProvider.DatosPeriodo do
     begin
          if ( Year <> FYear ) or ( Tipo <> FTipo ) or ( Mes <> FMes ) then
          begin
               // Investigar a�o y meses a revisar
                FYear := Year;
               FTipo := Tipo;
               FMes := Mes;

               iBimestre := NumeroBimestre( FMes );
               iMesFin := ( iBimestre * 2 );
               iMesIni := ( iMesFin - 1 );
               // Obtener fechas de inicio y fin
               // Si vuelve a invocarse con un periodo del mismo mes no ser� necesario volver a investigarlo

               GetFechas;
               FNumPeriodos := GetNumPeriodos;
          end;
     end;
     with Result do
     begin
          Kind := resDouble;
          EsFecha := TRUE;
          case FRegresa of
           1: dblResult := GetFechaInicio;
           2: dblResult := GetFechaFin;
           3: dblResult := GetFechaCalInicio;
           4: dblResult := GetFechaCalFin;
           5: begin
                   Kind := resInt;
                   EsFecha := False;
                   intResult := GetDiasCal;
              end;
           6: begin
                   Kind := resInt;
                   EsFecha := False;
                   intResult := GetDiasNom;
              end;
           7: begin
                   Kind := resInt;
                   EsFecha := False;
                   intResult := FNumPeriodos;
              end;
          else
             dblResult := GetFechaInicio
          end;
     end;
end;
type
  TZetaMesNomina = class( TZetaBimestreNomina )
  private
    function GetFechaCalInicio:TDate;override;
    function GetFechaCalFin:TDate;override;
    function GetDiasCal:Integer;override;
    function GetDiasNom:Integer;override;
    function GetNumPeriodos: Integer;override;
    procedure GetFechas;override;
  public
    function GetScript : String; override;
  end;
function TZetaMesNomina.GetScript(): String;
begin
     case FScript of
          1: Result := 'select PE_ASI_INI, PE_FEC_INI from PERIODO where PE_YEAR = %d and PE_TIPO = %d and PE_MES = %d and PE_POS_MES = 1 and PE_USO = 0';
          2: Result := 'select PE_ASI_FIN, PE_FEC_FIN from PERIODO where PE_YEAR = %d and PE_TIPO = %d and PE_MES = %d and PE_POS_MES = PE_PER_MES and PE_USO = 0';
          7: Result := 'select Count(*) as Cuantos from PERIODO where PE_YEAR = %d and PE_TIPO = %d and PE_MES = %d and PE_USO = 0';
     end;
end;

procedure TZetaMesNomina.GetFechas;
begin
     FScript := 1;
     GetFechaBimestre(FYear,FMes,FTipo);

     FScript := 2;
     GetFechaBimestre(FYear,FMes,FTipo);
end;

function TZetaMesNomina.GetFechaCalInicio: TDate;
begin
     Result := EncodeDate(FYear,FMes,1);
end;
function TZetaMesNomina.GetFechaCalFin: TDate;
begin
     Result := EncodeDate(FYear,FMes,MaxDay(FYear,FMes));
end;
function TZetaMesNomina.GetDiasCal: Integer;
var
   dFechaIni,dFechaFin:TDate;
begin
     dFechaIni := GetFechaCalInicio;
     dFechaFin := GetFechaCalFin;
     Result := DaysBetween( dFechaIni, dFechaFin);
end;

function TZetaMesNomina.GetDiasNom: Integer;
begin
     Result := DaysBetween( GetFechaInicio, GetFechaFin );
end;
function TZetaMesNomina.GetNumPeriodos: Integer;
begin
     Result := 0;
     FScript := 7;
     AbreQuery( Format( GetScript, [ FYear, Ord( FTipo ), FMes ] ) );
     with FQuery do
     begin
          if ( not EOF ) then
             Result := FieldByName( 'Cuantos' ).AsInteger;
     end;
end;

{*********************** TZetaINFO_EMPLEADO ************************}
type
  TZetaInfoEmpleado = class( TZetaRegresaQuery )
  private
    FCampo : string;
    FEmpleado: string;
  public
    function GetScript : String; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaInfoEmpleado.GetScript:string;
begin
     Result := 'select %s from COLABORA where ( CB_CODIGO = %s )';
end;

procedure TZetaInfoEmpleado.GetRequeridos( var TipoRegresa : TQREvResultType );
var
 lEsFecha :  Boolean;
begin
     ParametrosFijos(1);
     FEmpleado := ParamCampoInteger(0);
     if ParametrosConstantes then
     begin
          FCampo := DefaultString(1,K_PRETTYNAME );
          if ( FCampo = K_PRETTYNAME )then
          begin
               TipoRegresa := resString;
               EsFecha:= FALSE;
          end
          else
          begin
               TipoRegresa := InvestigaTipo( enEmpleado,FCampo,lEsFecha );
               EsFecha := lEsFecha;

          end;
          if ( TipoRegresa <> resError ) then
          begin
               AgregaScript( Format( GetScript, [FCampo,FEmpleado] ), TipoRegresa, EsFecha )
          end
          else
              ErrorFuncion( 'Error en Par�metro', 'Campo no existe en Empleados y Bajas ( COLABORA ): ' + FCampo );
     end;
end;

function TZetaInfoEmpleado.Calculate : TQREvResult;
begin
     FCampo := DefaultString( 1,K_PRETTYNAME );
     Result := EvaluaQuery( Format( GetScript, [ FCampo,IntToStr( ParamInteger( 0 ) ) ] ) );
end;

{*********************** TZetaDC_CONSTA ************************}
//DC_CONSTA ([,Curso] [,FechaInicial] [,FechaFinal] [,sFiltro] [,lTotalEmpresa], [Empleado] )
type
  TZetaDcConstancias = class( TZetaSFuncion )
  private
    FCurso : string;
    FFechaInicial: TDate;
    FFechaFinal: TDate;
    FFiltro: string;
    FTotalEmpresa :Boolean;
    FEmpleado :Integer;
    FLista : TStringList;
    function GetScript : String; override;
    function SumaEmpleados: Integer;
    procedure PreparaAgente;override;
    procedure GetScriptCursos( const sParamCursos:string );
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    constructor Create(oEvaluador:TQrEvaluator); override;
    destructor Destroy; override;
  end;

constructor TZetaDcConstancias.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create(oEvaluador);
     FCurso:= VACIO;
     FLista := TStringList.Create
end;

destructor TZetaDcConstancias.Destroy;
begin
     FLista.Free;
     inherited Destroy;
end;

function TZetaDcConstancias.GetScript:string;
begin
     if FTotalEmpresa then
     begin//Conteo de la empresa
          Result := 'select count( distinct( KARCURSO.CB_CODIGO ) ) from KARCURSO'+
                    ' %s';
     end
     else
     begin //Conteo del empleado
          Result := 'select count(*) from KARCURSO'+
                    ' %s ';
     end;
end;


procedure TZetaDcConstancias.GetScriptCursos( const sParamCursos:string );
var
   index :Integer;
begin
     FLista.CommaText := sParamCursos;
     with FLista do
      begin
           for index := 0  to Count - 1 do
           begin
                FCurso := ConcatString( FCurso, Parentesis(' KARCURSO.CU_CODIGO = '+ EntreComillas( FLista[index] ) ),' OR ')
           end;
      end;
end;

procedure TZetaDcConstancias.PreparaAgente;
begin
     with FAgente do
     begin
          if FTotalEmpresa then
          begin
               AgregaColumna( 'COLABORA.CB_CODIGO', TRUE, enEmpleado, tgNumero, 0, 'CB_CODIGO' );
               AgregaOrden( 'COLABORA.CB_CODIGO', TRUE, enEmpleado );
          end
          else
              AgregaFiltro( 'COLABORA.CB_CODIGO = :Empleado', TRUE, enEmpleado);
          AgregaFiltro( FCurso, FALSE, enKarCurso );
          if  ( FFechaInicial <> NullDateTime ) then
          begin
               AgregaFiltro( 'KARCURSO.KC_FEC_TOM between :FechaInicial and :FechaFinal', TRUE, enKarCurso )
          end
          else
          begin
               AgregaFiltro( 'KARCURSO.KC_FEC_TOM <= :FechaFinal', TRUE, enKarCurso )
          end;
     end;
end;

procedure TZetaDcConstancias.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sScript :string;
begin
     sScript := VACIO;
     GetScriptCursos( DefaultString(0,VACIO));

     FFechaInicial := DefaultDate(1,NullDateTime);
     FFechaFinal := DefaultDate(2,oZetaProvider.FechaDefault);
     FFiltro := DefaultString(3,VACIO);
     FTotalEmpresa := DefaultBoolean(4,FALSE);
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     FEmpleado := DefaultInteger(5,0);

     TipoRegresa := resInt;
     if ParametrosConstantes and StrVacio( FFiltro ) then
     begin
          if FCurso <> VACIO then
          begin
               sScript := Format( '( %s )',[ FCurso ] );
          end;

          if FFechaInicial <> NullDateTime then
          begin
               sScript := ConcatFiltros(Format( '( KARCURSO.KC_FEC_TOM between %s and %s )',[ DateToStrSQLC( FFechaInicial ),DateToStrSQLC( FFechaFinal ) ] ),sScript);
          end
          else
          begin
               sScript := ConcatFiltros(Format( '( KARCURSO.KC_FEC_TOM <= %s )',[ DateToStrSQLC( FFechaFinal ) ] ),sScript);
          end;

          if FTotalEmpresa then
          begin
               AgregaScript(Format( GetScript,[ ConcatString( ' where ',sScript,'')]),TipoRegresa,FALSE);
          end
          else
          begin
               if FEmpleado > 0 then
               begin
                    sScript := ConcatFiltros(Format('( KARCURSO.CB_CODIGO = %d )',[FEmpleado]),sScript);
               end
               else
               begin
                    sScript := ConcatFiltros(Format('( KARCURSO.CB_CODIGO = %s )',['COLABORA.CB_CODIGO']),sScript);
               end;

               AgregaScript(Format( GetScript,[ ConcatString( ' where ',sScript,'')]),TipoRegresa,FALSE);
          end;
     end;
end;

function TZetaDcConstancias.Calculate : TQREvResult;
begin
     FFechaInicial := DefaultDate(1,NullDateTime);
     FFechaFinal := DefaultDate(2,oZetaProvider.FechaDefault);
     FFiltro := DefaultString(3,VACIO);
     GetScriptCursos( DefaultString(0,VACIO));
     FTotalEmpresa := DefaultBoolean(4,FALSE);

     PreparaSuperEv( '1',FFiltro ,
                     enKarCurso, [tgNumero,tgFloat]);
     CierraDataSet;
     if ( not FTotalEmpresa ) then
        SetParamValue( 'Empleado', tgNumero, ZetaEvaluador.GetEmpleado );

     if  ( FFechaInicial <> NullDateTime ) then
     begin
          SetParamValue( 'FechaInicial', tgFecha , FFechaInicial  );
     end;
     SetParamValue( 'FechaFinal', tgFecha  , FFechaFinal );
     //SetParamValue( 'Curso', tgTexto ,  FCurso );

     AbreDataSet;

     with Result do
     begin
          Kind := resInt;
          esFecha := FALSE;
          if FTotalEmpresa then
             intResult := SumaEmpleados
          else
              intResult := Trunc( SumaRegistros );
     end;

end;

function TZetaDcConstancias.SumaEmpleados: Integer;
var
   iEmpleado, iEmpleadoAnt: Integer;
begin
     Result := 0;
     with SuperEv.DataSet do
     begin
          iEmpleadoAnt := 0;
          while ( not EOF ) do
          begin
               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
               if ( iEmpleadoAnt <> iEmpleado ) then
               begin
                    Inc( Result );
                    iEmpleadoAnt := iEmpleado;
               end;
               Next;
          end;
     end;
end;


{ ************* TZetaFECHAADD ************* }

type
  TZetaFechaAdd = class( TZetaRegresa )
  private
    FTipo : Integer;
    FFecha: TDate;
    FCuantos: Integer;
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
end;

procedure TZetaFechaAdd.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     inherited;
     TipoRegresa:= resDouble;
     EsFecha := TRUE;
     FTipo := DefaultInteger(0,0);
     if ( not ( FTipo in [0..2] ) ) then
        FTipo := 0;
     FCuantos := DefaultInteger(1,0);
     FFecha := DefaultDate(2,oZetaProvider.FechaDefault);
     lQuitarOptimizeCursorB8 := FALSE;         // Si no requiere un query en el calculate, se deja optimizaci�n de Build #8
end;

function TZetaFechaAdd.Calculate: TQREvResult;

function AgregaDias:TDate;
begin
     Result := IncDay(FFecha,FCuantos);
end;

function AgregaMes:TDate;
begin
     Result := IncMonth(FFecha,FCuantos);
end;

function AgregaAnios:TDate;
begin
     Result := IncYear(FFecha,FCuantos);
end;

begin
     FTipo := DefaultInteger(0,0);
     FCuantos := DefaultInteger(1,0);
     FFecha := DefaultDate(2,oZetaProvider.FechaDefault);
     with Result do
     begin
          Kind := resDouble;
          esFecha := TRUE;
          case FTipo of
            0: dblResult := AgregaDias;
            1: dblResult := AgregaMes;
            2: dblResult := AgregaAnios;
          else
              dblResult := AgregaDias;
          end;
     end;
end;


{ ************* TZetaFecVenciV ************* }

type
  TZetaFecVenciV = class( TZetaRegresa )
  private
    FFechaRef :TDate;
    FFechaAnt :TDate;
    FCuantos: Integer;
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
end;

procedure TZetaFecVenciV.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     inherited;
     TipoRegresa:= resDouble;
     EsFecha := TRUE;
     FCuantos := DefaultInteger(0,0);
     FFechaRef := DefaultDate(1,oZetaProvider.FechaDefault);
     FFechaAnt := DefaultDate(2,oZetaProvider.FechaDefault);
     lQuitarOptimizeCursorB8 := FALSE;         // Si no requiere un query en el calculate, se deja optimizaci�n de Build #8
end;

function TZetaFecVenciV.Calculate: TQREvResult;
var
   FechaVenc :TDate;
   iAnio:Integer;
   bVacVencidas : Boolean;
begin
     FCuantos := DefaultInteger(0,0);
     FFechaRef := DefaultDate(1,oZetaProvider.FechaDefault);
     FFechaAnt := DefaultDate(2,oZetaProvider.FechaDefault);
     bVacVencidas := False;

     iAnio := TheYear( FFechaRef );
     FechaVenc := DateTheYear( FFechaAnt,iAnio );

     while ( ( FechaVenc >= FFechaAnt ) and ( not bVacVencidas ) )do
     begin
         bVacVencidas := IncMonth( FechaVenc,FCuantos ) <= FFechaRef;
         if not bVacVencidas then
         begin
              iAnio := iAnio - 1;
              FechaVenc := DateTheYear( FFechaAnt,iAnio );
         end;
     end;
     with Result do
     begin
          Kind := resDouble;
          esFecha := TRUE;
          dblResult := FechaVenc;
     end;
end;


{ ************* TZetaSumPrimDomEx ************* }

type
  TZetaSumPrimDomEx = class( TZetaSUMABASE )
  private
    FTope, FSalMin ,FOrdinarias,FFestTrabajado,FDesTrabajado,FExtras: TPesos;
    FInicioAsis, FFinAsis: TDate;
    FPrimaDominicalTotal:Boolean;
    iEmpleado:Integer;
    FCalculaPrimaDom :TPrimaDominical;
  protected
    function CalculaDataSet: TQREvResult; override;
    procedure PreparaAgente; override;
  public
    function Calculate: TQREvResult; override;
    constructor Create(oEvaluador:TQrEvaluator); override;
    destructor Destroy;override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

constructor TZetaSumPrimDomEx.Create(oEvaluador: TQrEvaluator);
begin
     inherited Create(oEvaluador);
     FParamFijos:= 0;
     FCalculaPrimaDom := TPrimaDominical.Create(oZetaProvider);
end;

destructor TZetaSumPrimDomEx.Destroy;
begin
     FreeAndNil(FCalculaPrimaDom);
     inherited Destroy;
end;

procedure TZetaSumPrimDomEx.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     inherited;
     if ( not ZetaEvaluador.CalculandoNomina ) then
     begin
          AgregaRequerido( 'NOMINA.CB_ZONA_GE', enNomina );
          ZetaEvaluador.oZetaCreator.PreparaSalMin;
     end;
     oZetaProvider.InitGlobales;

end;

procedure TZetaSumPrimDomEx.PreparaAgente;
begin
     inherited PreparaAgente;
     with FAgente do
     begin
          AgregaColumna( 'AUSENCIA.AU_HORAS', TRUE, enAusencia, tgFloat, 0, 'AU_HORAS' );
          AgregaColumna( 'AUSENCIA.AU_DES_TRA', TRUE, enAusencia, tgFloat, 0, 'AU_DES_TRA' );
          AgregaColumna( 'AUSENCIA.AU_EXTRAS', TRUE, enAusencia, tgFloat, 0, 'AU_EXTRAS' );
          AgregaColumna( 'AUSENCIA.AU_TIPODIA', TRUE, enAusencia, tgFloat, 0, 'AU_TIPODIA' );
          AgregaColumna( 'AUSENCIA.AU_STATUS', TRUE, enAusencia, tgFloat, 0, 'AU_STATUS' );
          AgregaColumna( 'HORARIO.HO_TIPO', TRUE, enHorario, tgNumero, 0, 'HO_TIPO' );
     end;
end;

function TZetaSumPrimDomEx.Calculate: TQREvResult;
begin
     FFormula := 'AU_HORAS';
     FFormulaSalario := DefaultString( 0, 'CB_SALARIO / 8' );
     FFiltro := DefaultString( 1, VACIO );
     FInicioAsis := DatasetEvaluador.FieldByName( 'NO_ASI_INI' ).AsDateTime;
     FFinAsis := DatasetEvaluador.FieldByName( 'NO_ASI_FIN' ).AsDateTime;
     iEmpleado := DatasetEvaluador.FieldByName( 'CB_CODIGO' ).AsInteger;


     if ZetaEvaluador.CalculandoNomina then
        FSalMin := TCalcNomina( ZetaEvaluador.CalcNomina ).nEmpleadoSalMin
     else
         FSalMin := ZetaEvaluador.oZetaCreator.dmSalMin.GetSalMin( FInicioAsis, DatasetEvaluador.FieldByName( 'CB_ZONA_GE' ).AsString );

     FTope := DefaultFloat( 2, FSalMin );

     FPrimaDominicalTotal := oZetaProvider.GetGlobalBooleano(K_GLOBAL_PRIMA_DOMINICAL_TIPO_DIA);

     Result := GetResultado( FInicioAsis, FFinAsis );
end;

function TZetaSumPrimDomEx.CalculaDataSet: TQREvResult;
var
   rMonto: TPesos;
   rFormula, rSalario: TPesos;
   rTotalExento: TPesos;
   dFecha :TDate;


   procedure AgregaTitulo;
   begin
        with ZetaEvaluador.Rastreador do
        begin
             RastreoEspacio;
             RastreoTextoLibre( 'POS  FECHA      DIA   SALARIO  SAL/HORA  HORAS  MONTO' );
        end;
   end;

   procedure AgregaTituloRastreo;
   begin
        with ZetaEvaluador.Rastreador do
        begin
             RastreoHeader( 'EVALUACION PERIODO :' + DateToStr(FInicioAsis) +' al '+DateToStr(FFinAsis));
             RastreoTextoLibre( 'Tope Exento: ' + PadL( Format( '%8.2n', [ FTope ] ), 11 ) );
             RastreoTextoLibre( 'Sal. M�nimo: ' + PadL( Format( '%8.2n', [ FSalMin ] ), 11 ) );
        end;
        AgregaTitulo;
   end;

   procedure AgregaDetalleRastreo;
   var
      sPosicion: String;
   begin
        if ( dFecha < FInicioAsis ) then   
           sPosicion := Space(3)
        else
            sPosicion := PadL( SuperEv.DataSet.FieldByName( 'AU_POSICIO' ).AsString, 3 );
        with ZetaEvaluador.Rastreador do
        begin
             RastreoTextoLibre( sPosicion + Space(2) +
                                FechaCorta( dFecha ) + Space(2) +
                                Copy( DiaSemana( dFecha ), 1, 3 ) + Space(2) +
                                PadL( Format( '%5.2n', [ SuperEv.DataSet.FieldByName( 'CB_SALARIO' ).AsFloat ] ), 8 ) + Space(2) +
                                PadL( Format( '%5.2n', [ rSalario ] ), 8 ) + Space(2) +
                                PadL( Format( '%2.2n', [ rFormula ] ), 5 ) + Space(2) +
                                PadL( Format( '%6.2n', [ rMonto ] ), 9 )
                                );
        end;
   end;

   procedure AgregaTotalRastreo;
   begin
        with ZetaEvaluador.Rastreador do
        begin
             RastreoEspacio;
             RastreoPesos( 'TOTAL EXENTO DEL PERIODO : ', rTotalExento );
        end;
   end;

begin
     rTotalExento := 0;
     if ZetaEvaluador.Rastreando then
     begin
          AgregaTituloRastreo;
     end;

     with SuperEv.DataSet do
     begin
          while ( not EOF ) do
          begin
               if DayOfWeek( FieldByName( 'AU_FECHA' ).AsDateTime ) = 1 then
               begin
                    rFormula := 0;
                    dFecha := FieldByName( 'AU_FECHA' ).AsDateTime;
                    rSalario := GetSalarioEv;

                    FOrdinarias := FieldByName('AU_HORAS').AsFloat;
                    FExtras := FieldByName('AU_EXTRAS').AsFloat;

                    if eTipoDiaAusencia(FieldByName('AU_TIPODIA').AsInteger) = daFestivo then
                    begin
                         FFestTrabajado := FieldByName('AU_DES_TRA').AsFloat;
                         FDesTrabajado := 0;
                    end
                    else
                        FDesTrabajado := FieldByName('AU_DES_TRA').AsFloat;
                        FFestTrabajado := 0;

                    if eStatusAusencia(FieldByName('AU_STATUS').AsInteger) = auHabil then
                       rFormula := FCalculaPrimaDom.CalcularPrimaDominical(iEmpleado,dFecha,eTipoHorario( FieldByName( 'HO_TIPO' ).AsInteger ),FOrdinarias,FOrdinarias+FFestTrabajado+FExtras).Ordinarias
                    else
                    begin
                         if FPrimaDominicalTotal then
                              rFormula := FCalculaPrimaDom.CalcularPrimaDominical(iEmpleado,dFecha,eTipoHorario( FieldByName( 'HO_TIPO' ).AsInteger ),FDesTrabajado,FOrdinarias+FExtras+FDesTrabajado).Ordinarias;
                    end;

                    rMonto := rMin( rFormula * rSalario,FTope);

                    rTotalExento := rTotalExento + rMonto;

                    if ZetaEvaluador.Rastreando then
                       AgregaDetalleRastreo;
               end;
               Next;
          end;
     end;
     if ZetaEvaluador.Rastreando then
        AgregaTotalRastreo;

     with Result do
     begin
          Kind:= resDouble;
          dblResult := rTotalExento;
     end;
end;


{ ************* TZetaTopaPrimDomEx ************* }

type
  TZetaTopaPrimDomEx = class( TZetaSUMABASE )
  private
    FTope, FSalMin ,FOrdinarias,FFestTrabajado,FDesTrabajado,FExtras: TPesos;
    FSalario: TPesos;
    FInicioAsis, FFinAsis: TDate;
    FPrimaDominicalTotal:Boolean;
    iEmpleado:Integer;
    FCalculaPrimaDom :TPrimaDominical;
  protected
    function CalculaDataSet: TQREvResult; override;
    procedure PreparaAgente; override;
  public
    function Calculate: TQREvResult; override;
    constructor Create(oEvaluador:TQrEvaluator); override;
    destructor Destroy;override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

constructor TZetaTopaPrimDomEx.Create(oEvaluador: TQrEvaluator);
begin
     inherited Create(oEvaluador);
     FParamFijos:= 0;
     FCalculaPrimaDom := TPrimaDominical.Create(oZetaProvider);
end;

destructor TZetaTopaPrimDomEx.Destroy;
begin
     FreeAndNil(FCalculaPrimaDom);
     inherited Destroy;
end;

procedure TZetaTopaPrimDomEx.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     inherited;
     if ( not ZetaEvaluador.CalculandoNomina ) then
     begin
          AgregaRequerido( 'NOMINA.CB_ZONA_GE', enNomina );
          ZetaEvaluador.oZetaCreator.PreparaSalMin;
     end;
     oZetaProvider.InitGlobales;

end;

procedure TZetaTopaPrimDomEx.PreparaAgente;
begin
     inherited PreparaAgente;
     with FAgente do
     begin
          AgregaColumna( 'AUSENCIA.AU_HORAS', TRUE, enAusencia, tgFloat, 0, 'AU_HORAS' );
          AgregaColumna( 'AUSENCIA.AU_DES_TRA', TRUE, enAusencia, tgFloat, 0, 'AU_DES_TRA' );
          AgregaColumna( 'AUSENCIA.AU_EXTRAS', TRUE, enAusencia, tgFloat, 0, 'AU_EXTRAS' );
          AgregaColumna( 'AUSENCIA.AU_TIPODIA', TRUE, enAusencia, tgFloat, 0, 'AU_TIPODIA' );
          AgregaColumna( 'AUSENCIA.AU_STATUS', TRUE, enAusencia, tgFloat, 0, 'AU_STATUS' );
          AgregaColumna( 'HORARIO.HO_TIPO', TRUE, enHorario, tgNumero, 0, 'HO_TIPO' );
     end;
end;

function TZetaTopaPrimDomEx.Calculate: TQREvResult;
begin
     FFormula := 'AU_HORAS';
     // El valor default para este campo debe ser (CB_SALARIO/8)*0.25
     FFormulaSalario := DefaultString( 0, '(CB_SALARIO/8)*0.25' );

     FFiltro := DefaultString( 1, VACIO );
     FInicioAsis := DatasetEvaluador.FieldByName( 'NO_ASI_INI' ).AsDateTime;
     FFinAsis := DatasetEvaluador.FieldByName( 'NO_ASI_FIN' ).AsDateTime;
     iEmpleado := DatasetEvaluador.FieldByName( 'CB_CODIGO' ).AsInteger;

     if ZetaEvaluador.CalculandoNomina then
        FSalMin := TCalcNomina( ZetaEvaluador.CalcNomina ).nEmpleadoSalMin
     else
         FSalMin := ZetaEvaluador.oZetaCreator.dmSalMin.GetSalMin( FInicioAsis, DatasetEvaluador.FieldByName( 'CB_ZONA_GE' ).AsString );

     // Tope default de horas: 8.
     FTope := DefaultFloat (2, 8);

     FPrimaDominicalTotal := oZetaProvider.GetGlobalBooleano(K_GLOBAL_PRIMA_DOMINICAL_TIPO_DIA);

     Result := GetResultado( FInicioAsis, FFinAsis );
end;

function TZetaTopaPrimDomEx.CalculaDataSet: TQREvResult;
var
   rMonto: TPesos;
   rFormula, rSalario: TPesos;
   rTotalPD: TPesos;
   dFecha :TDate;
   // Banderas
   bDomingo, bTituloRastreo: Boolean;

   procedure AgregaTitulo;
   begin
        with ZetaEvaluador.Rastreador do
        begin
             RastreoEspacio;
             // RastreoTextoLibre( 'POS  FECHA      DIA   SALARIO  PRIMADOM/HORA  HORAS    MONTO' );
             RastreoTextoLibre( 'POS  FECHA      DIA   SALARIO  PRIMA/HORA  HORAS    HORAS/PAGO   MONTO' );
        end;
   end;

   procedure AgregaTituloRastreo;
   begin
        with ZetaEvaluador.Rastreador do
        begin
             RastreoHeader( 'EVALUACION PERIODO :' + DateToStr(FInicioAsis) +' al '+DateToStr(FFinAsis));
             // RastreoTextoLibre( 'Tope de prima dominical: ' + PadL( Format( '%8.2n', [ FTope * rSalario ] ), 11 ) );
             // RastreoTextoLibre( '         Salario m�nimo: ' + PadL( Format( '%8.2n', [ FSalMin ] ), 11 ) );
             // Agregar salario del empleado.
             // RastreoTextoLibre( '   Salario del empleado: ' + PadL( Format( '%8.2n', [ FSalario ] ), 11 ) );
             RastreoTextoLibre( 'Tope de horas: ' + PadL( Format( '%8.2n', [ FTope ] ), 11 ) );
        end;
        AgregaTitulo;
   end;

   procedure AgregaDetalleRastreo;
   var
      sPosicion: String;
      rHorasMenor: TPesos;
   begin
        if ( dFecha < FInicioAsis ) then
           sPosicion := Space(3)
        else
            sPosicion := PadL( SuperEv.DataSet.FieldByName( 'AU_POSICIO' ).AsString, 3 );
        with ZetaEvaluador.Rastreador do
        begin
             rHorasMenor := fTope;
             if rFormula < fTope then
                rHorasMenor := rFormula;


             RastreoTextoLibre( sPosicion + Space(2) +
                                FechaCorta( dFecha ) + Space(2) +
                                Copy( DiaSemana( dFecha ), 1, 3 ) + Space(2) +
                                PadL( Format( '%5.2n', [ SuperEv.DataSet.FieldByName( 'CB_SALARIO' ).AsFloat ] ), 8 ) + Space(2) +
                                PadL( Format( '%5.2n', [ rSalario ] ), 8 ) + Space(4) +
                                PadL( Format( '%2.2n', [ rFormula ] ), 5 ) + Space(2) + Space(2) + Space(2) +
                                PadL( Format( '%2.2n', [ rHorasMenor ] ), 5 ) + Space(2) +
                                PadL( Format( '%6.2n', [ rMonto ] ), 9 )
                                );
        end;
   end;

   procedure AgregaTotalRastreo;
   begin
        with ZetaEvaluador.Rastreador do
        begin
             RastreoEspacio;
             RastreoPesos( 'TOTAL PRIMA DOMINICAL DEL PERIODO : ', rTotalPD );
        end;
   end;

begin
     rTotalPD := 0;

     rFormula := 0;
     bTituloRastreo := FALSE;

     with SuperEv.DataSet do
     begin
          while ( not EOF ) do
          begin
               // Evaluaci�n del d�a domingo.
               bDomingo := FALSE;
               if DayOfWeek( FieldByName( 'AU_FECHA' ).AsDateTime ) = 1 then
                  bDomingo := TRUE;

               if DayOfWeek( FieldByName( 'AU_FECHA' ).AsDateTime ) in [1,7] then
               begin
                    dFecha := FieldByName( 'AU_FECHA' ).AsDateTime;
                    rSalario := GetSalarioEv;

                    // El tope es una expresi�n en horas que multiplique el valor del primer par�metro.
                    // El default es 8.
                    // FTope := DefaultFloat (2, 8);

                    if (ZetaEvaluador.Rastreando) and (not bTituloRastreo) then
                    begin
                         AgregaTituloRastreo;
                         bTituloRastreo := TRUE;
                    end;

                    FOrdinarias := FieldByName('AU_HORAS').AsFloat;
                    FExtras := FieldByName('AU_EXTRAS').AsFloat;

                    if eTipoDiaAusencia(FieldByName('AU_TIPODIA').AsInteger) = daFestivo then
                    begin
                         FFestTrabajado := FieldByName('AU_DES_TRA').AsFloat;
                         FDesTrabajado := 0;
                    end
                    else
                    begin
                         FDesTrabajado := FieldByName('AU_DES_TRA').AsFloat;
                         FFestTrabajado := 0;
                    end;

                    if eStatusAusencia(FieldByName('AU_STATUS').AsInteger) = auHabil then
                       rFormula := rFormula + FCalculaPrimaDom.CalcularPrimaDominical(iEmpleado,dFecha,eTipoHorario( FieldByName( 'HO_TIPO' ).AsInteger ),FOrdinarias,FOrdinarias+FFestTrabajado+FExtras, TRUE).Ordinarias
                    else
                    begin
                         if FPrimaDominicalTotal then
                              rFormula := rFormula + FCalculaPrimaDom.CalcularPrimaDominical(iEmpleado,dFecha,eTipoHorario( FieldByName( 'HO_TIPO' ).AsInteger ),FDesTrabajado,FOrdinarias+FExtras+FDesTrabajado, TRUE).Ordinarias;
                    end;

                    if bDomingo then
                    begin
                         // FTope.
                         // Cambio en FTope.
                         // Para obtener el m�ximo se multiplia por rSalario.
                         rMonto := rMin( (rFormula) * rSalario, FTope*rSalario);

                         rTotalPD := rTotalPD + rMonto;
                    end;

                    // if ZetaEvaluador.Rastreando then
                    if bDomingo then
                    begin
                         if (ZetaEvaluador.Rastreando) then
                         begin
                              AgregaDetalleRastreo;
                         end;

                         rMonto := 0;
                         rFormula := 0;
                    end;
               end;
               Next;
          end;
     end;
     if ZetaEvaluador.Rastreando then
        AgregaTotalRastreo;

     with Result do
     begin
          Kind:= resDouble;
          dblResult := rTotalPD;
     end;
end;

{ ****************** TZetaPension **************************}
{PENSION ([Tipo],[Orden])}
type
  TZetaPension = class( TZetaRegresaFloat )
  private
     FTPension:TZetaCursor;
     FPension:TZetaCursor;
     FPorcen:TZetaCursor;
     FTodasPercepciones:Boolean;
     FTodasPrestaciones:Boolean;
     FMontosNegativos:Boolean;
     FListaConPension: TStringList;
     FListaConDescuento: TStringList;
     FTipoPension:string;
     FOrdenPension:Integer;
     FTasaFija:TPesos;
     FechaPension:TDate;
     FPorcentaje:TTasa;
     FActiva:Boolean;
     function PensionValida: Boolean;
     function ExistePorcentaje: Boolean;
     function GetPorcentaje:TTasa;
     procedure CargarPension;
     function CalcularConceptos(ListaConceptos:TStringList;EsDescuento:Boolean):TPesos;
     function CalcularPension:TPesos;
     function SumarConcepto(Concepto:Integer):Boolean;
  public
    { Public Declarations }
    constructor Create(oEvaluador:TQrEvaluator); override;
    destructor Destroy; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
    procedure PreparaQuerys;
  end;

constructor TZetaPension.Create(oEvaluador: TQrEvaluator);
begin
     inherited;
     PreparaQuerys;
     FListaConPension := TStringList.Create;
     FListaConDescuento := TStringList.Create;
end;

destructor TZetaPension.Destroy;
begin
     inherited;
     FreeAndNil(FPorcen);
     FreeAndNil(FPension);
     FreeAndNil(FListaConPension);
     FreeAndNil(FListaConDescuento);
end;

procedure TZetaPension.GetRequeridos(var TipoRegresa: TQREvResultType);
begin
     inherited;
     TipoRegresa := resDouble;
     if NOT ZetaEvaluador.CalculandoNomina then
        ErrorFuncion( 'Solamente se Puede Utilizar en C�lculo de N�mina', 'Funci�n: ' + Expresion )
     else
     begin
          AgregaRequerido( 'COLABORA.CB_TIE_PEN', enEmpleado );
          AgregaRequerido( 'NOMINA.NO_LIQUIDA', enNomina );
     end;
end;

function TZetaPension.Calculate: TQREvResult;
begin
     Result.Kind      := resDouble;
     Result.dblResult := 0.0;
     if zStrToBool( DatasetEvaluador.FieldByName( 'CB_TIE_PEN' ).AsString ) then
     begin
          FTipoPension := DefaultString( 0, VACIO );
          FOrdenPension := DefaultInteger( 1, 1 );
          if PensionValida then
          begin
               if ( FTipoPension = VACIO ) then
                  Result.dblResult := FTasaFija
               else
               begin
                    if ExistePorcentaje then
                    begin
                         //Descargar las pensiones del empleado activo
                         CargarPension;
                         Result.dblResult :=  CalcularPension;
                    end;
               end;
               if Result.dblResult < 0 then
                  Result.dblResult := 0;
               //ErrorFuncion (Format('Monto calculado negativo para tipo de pensi�n: %s',[FTipoPension]),'PENSION()');
          end;
     end;
end;


procedure TZetaPension.PreparaQuerys;
const
     K_QUERY_PORC_PEN = 'select PP_PORCENT from PEN_PORCEN where TP_CODIGO = :Tipo and CB_CODIGO = :Empleado and PS_ORDEN = :Orden';
     K_QUERY_PENSION  = 'select PS_FIJA,PS_FECHA,PS_ACTIVA from PENSION where CB_CODIGO = :Empleado and PS_ORDEN = :Orden';
begin
     if ( FPorcen = NIL ) then
        FPorcen := oZetaProvider.CreateQuery( K_QUERY_PORC_PEN );

     if ( FPension = NIL ) then
        FPension := oZetaProvider.CreateQuery( K_QUERY_PENSION );
end;

function TZetaPension.PensionValida:Boolean;
begin
     with oZetaProvider do
     begin
          with FPension do
          begin
               Active := FALSE;
               ParamAsInteger( FPension, 'Orden', FOrdenPension );
               ParamAsInteger( FPension, 'Empleado', ZetaEvaluador.GetEmpleado );
               Active := TRUE;
               FTasaFija := FieldByName('PS_FIJA').AsFloat;
               FechaPension := FieldByName('PS_FECHA').AsDateTime;
               FActiva := zStrToBool( FieldByName( 'PS_ACTIVA' ).AsString );
//               Result := ( FechaPension <= DatosPeriodo.Fin ) and ( FechaPension <> NullDateTime);
               Result := ( FechaPension <= DatosPeriodo.Fin ) and ( FechaPension <> NullDateTime) and (FActiva);
          end;
     end;
end;

function TZetaPension.ExistePorcentaje:Boolean;
begin
     FPorcentaje := GetPorcentaje ;
     Result := FPorcentaje > 0;
end;

function TZetaPension.GetPorcentaje: TTasa;
begin
     Result := 0.0;
     with oZetaProvider do
     begin
          if StrLleno(FTipoPension)then
          begin
               with FPorcen do
               begin
                    Active := FALSE;
                    ParamAsString( FPorcen, 'Tipo', FTipoPension );
                    ParamAsInteger( FPorcen, 'Empleado', ZetaEvaluador.GetEmpleado );
                    ParamAsInteger( FPorcen, 'Orden', FOrdenPension );
                    Active := True;
                    Result := ( FieldByName('PP_PORCENT').AsFloat/100.0 );
               end;
          end;
     end;
end;

procedure TZetaPension.CargarPension;
const
     K_QUERY_TPENSION = ' select TP_NOMBRE ,TP_PERCEP ,TP_PRESTA ,TP_CO_PENS ,TP_CO_DESC ,TP_APL_NOR ,TP_APL_LIQ ,TP_APL_IND, TP_DESC_N from TPENSION '+
		        ' where TP_CODIGO = %s %s';

     function GetTiposLiquidacion:string;
     begin
          Result := VACIO;
          case eLiqNomina( DatasetEvaluador.FieldByName( 'NO_LIQUIDA' ).AsInteger) of
               lnNormal:        Result := Format( ' and TP_APL_NOR = ''%s'' ',[ K_GLOBAL_SI ] );
               lnLiquidacion:   Result := Format( ' and TP_APL_LIQ = ''%s'' ',[ K_GLOBAL_SI ] );
               lnIndemnizacion: Result := Format( ' and TP_APL_IND = ''%s'' ',[ K_GLOBAL_SI ] );
          else
              Result := VACIO;
          end;
     end;
begin
     with oZetaProvider do
     begin
          if StrLleno(FTipoPension)then
          begin
                if ( FTPension = NIL ) then
                   FTPension := oZetaProvider.CreateQuery( Format( K_QUERY_TPENSION,[EntreComillas(FTipoPension), GetTiposLiquidacion ] ) );
               Try
                  with FTPension do
                  begin
                       Active := TRUE;
                       FTodasPercepciones   := zStrToBool( FieldByName('TP_PERCEP').AsString);
                       FTodasPrestaciones   := zStrToBool( FieldByName('TP_PRESTA').AsString);
                       FListaConPension.CommaText   := FieldByName('TP_CO_PENS').AsString;
                       FListaConDescuento.CommaText := FieldByName('TP_CO_DESC').AsString;
                       FMontosNegativos := zStrToBool( FieldByName('TP_DESC_N').AsString);
                  end;
               finally
                      FreeAndNil(FTPension);
               end;
          end;
     end;
end;

function TZetaPension.CalcularPension : TPesos;
begin
     // ( Suma de Conceptos de Pago menos Suma de Conceptos de Descuento ) por porcentaje
     Result := ( ( CalcularConceptos( FListaConPension , False ) ) - CalcularConceptos( FListaConDescuento, True ) ) * FPorcentaje;
end;

function TZetaPension.CalcularConceptos(ListaConceptos: TStringList;EsDescuento: Boolean): TPesos;
var
   i,Concepto:Integer;
   MontoConcep:TPesos;
   Suma:TPesos;
   
   function EsDeduccion(Concepto:Integer):Boolean;
   begin
        Result := TCalcNomina(ZetaEvaluador.CalcNomina).ConceptoTipo(Concepto) = coDeduccion ;
   end;
begin
     Suma := 0.0;
     for i := 0 to ListaConceptos.Count - 1 do
     begin
          Concepto := StrToInt( ListaConceptos.Strings[i] );
          if ( SumarConcepto( Concepto ) ) or ( EsDescuento ) then
          begin
               MontoConcep := TCalcNomina(ZetaEvaluador.CalcNomina).ConceptoCalculado( Concepto );
               if( ( MontoConcep < 0 )and ( EsDeduccion(Concepto) ) )then //Si es una Deduccion Negativa , lo sumamos como percepcion
               begin
                    if EsDescuento then
                    begin
                         if Not FMontosNegativos  then //Si no se le descuentan las deducciones negativas, entonces no se suma al monto
                             MontoConcep := 0
                    end
                    else
                        MontoConcep := MontoConcep * (-1);
               end;
               Suma := Suma + MontoConcep;
          end;
     end;
     //Sumar los totales de Percepciones y Prestaciones , si estan marcadas para los conceptos de pago de pension
     if not EsDescuento then
     begin
          if FTodasPercepciones then
             Suma := Suma + TCalcNomina(ZetaEvaluador.CalcNomina).TotNomina(  ttnPercepciones  );
          if FTodasPrestaciones then
             Suma := Suma + TCalcNomina(ZetaEvaluador.CalcNomina).TotNomina(  ttnPrestaciones  );
     end;
     Result := Suma;
end;

function TZetaPension.SumarConcepto(Concepto: Integer): Boolean;

function EsPercepcion( Concepto :Integer):Boolean;
begin
     Result := TCalcNomina(ZetaEvaluador.CalcNomina).ConceptoTipo(Concepto) = coPercepcion ;
end;

function EsPrestacion( Concepto :Integer):Boolean;
begin
     Result := TCalcNomina(ZetaEvaluador.CalcNomina).ConceptoTipo(Concepto) = coPrestacion;
end;
begin
     Result := True;
     if FTodasPercepciones then               // si esta marcada TODAS percepciones  y NO es percepcion se agrega a la suma
        Result := not EsPercepcion (Concepto)
     else if FTodasPrestaciones then         // si esta marcada TODAS Prestaciones  y NO es Prestacion se agrega a la suma
             Result := not EsPrestacion(Concepto);
end;

{TZetaINFOPENSION}

type
TZetaINFOPENSION = class ( TZetaRegresaQuery )
  private
     FQuery:TZetaCursor;
     FOrdenPension:string;
     FCampo : String;
     procedure CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQREvResultType; var lEsFecha : Boolean; const lRequeridos :Boolean );
     function GetPorcentajes: string;
     procedure PreparaQuerys;
public
  constructor Create(oEvaluator:TQrEvaluator); override;
  destructor Destroy; override;
  procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  function Calculate: TQREvResult; override;
  function GetScript: string;override;
end;

constructor TZetaINFOPENSION.Create(oEvaluator: TQrEvaluator);
begin
     inherited;
     PreparaQuerys
end;

destructor TZetaINFOPENSION.Destroy;
begin
     inherited;
     FreeAndNil(FQuery);
end;

procedure TZetaINFOPENSION.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   lEsFecha : Boolean;
begin
     ParametrosFijos( 1 );
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );

     TipoRegresa := resDouble;
     if ParametrosConstantes then
     begin
          if ParamVacio( 1 ) then
             FOrdenPension := '1'
          else
              FOrdenPension := ParamCampoInteger(1);

          CaseCampo( ParamInteger(0), FCampo, TipoRegresa, lEsFecha, TRUE );
          EsFecha:= lEsFecha;
          if ParamInteger(0) < 10 then
             AgregaScript( Format( GetScript, [ FCampo, 'COLABORA.CB_CODIGO', FOrdenPension ] ), TipoRegresa, lEsFecha );
     end;
end;

function TZetaINFOPENSION.Calculate: TQREvResult;
var
   TipoRegresa : TQREvResultType;
   lEsFecha : Boolean;
begin
     FOrdenPension := DefaultString ( 1, '1' );
     CaseCampo( DefaultInteger( 0, 1 ), FCampo, TipoRegresa, lEsFecha,FALSE );
     EsFecha:= lEsFecha;
     Result := EvaluaQuery( Format( GetScript, [ FCampo, IntToStr(ZetaEvaluador.GetEmpleado) ,FOrdenPension ] ) );
end;

procedure TZetaINFOPENSION.CaseCampo(const iRegresa: Integer;
  var sCampo: String; var Tipo: TQREvResultType; var lEsFecha: Boolean;const lRequeridos :Boolean);
begin
     Tipo := resDouble;
     lEsFecha := FALSE;
     case iRegresa of
         1:
           begin
               Tipo:= resDouble;
               lEsFecha := TRUE;
               sCampo:= 'PS_FECHA';
          end;
          2:
          begin
               Tipo:= resString;
               sCampo:= 'PS_EXPEDIE';
          end;
          3:
          begin
               Tipo:= resString;
               sCampo:= 'PS_BENEFIC';
          end;
          4:
          begin
               Tipo:= resString;
               sCampo:= 'PS_CTA_1';
          end;
          5:
          begin
               Tipo:= resString;
               sCampo:= 'PS_BANCO_1';
          end;
          6:
          begin
               Tipo:= resString;
               sCampo:= 'PS_CTA_2';
          end;
          7:
          begin
               Tipo:= resString;
               sCampo:= 'PS_BANCO_2';
          end;
          8:
          begin
               Tipo:= resString;
               sCampo:= 'PS_OBSERVA';
          end;
          9:
          begin
               sCampo:= 'PS_FIJA';
          end;

          10:
          begin
               Tipo:= resString;
               if lRequeridos then
                  sCampo := VACIO
               else
                   sCampo:= EntreComillas( GetPorcentajes ) +' AS Porce';
          end;
          11:
          begin
              Tipo:= resString;
              sCampo := 'PS_ACTIVA';
          end;
          else
          begin
               sCampo := 'PS_FECHA';
               Tipo := resDouble;
               lEsFecha := TRUE;
          end

     end;
end;

procedure TZetaINFOPENSION.PreparaQuerys;
const
     K_QUERY_PORCEN = ' select PP_PORCENT,TP_CODIGO from PEN_PORCEN '+
		        ' where CB_CODIGO = :Empleado and PS_ORDEN = :Orden ';

begin
     if ( FQuery = NIL ) then
        FQuery := oZetaProvider.CreateQuery( K_QUERY_PORCEN );

end;

function TZetaINFOPENSION.GetScript:string;
begin
     Result := 'select %s from PENSION where ( CB_CODIGO = %s  and PS_ORDEN = %s )';
end;

Function TZetaINFOPENSION.GetPorcentajes:string;
begin
     Result := VACIO;
     with oZetaProvider do
     begin
          with FQuery do
          begin
               Active := FALSE;
               ParamAsInteger( FQuery, 'Empleado', ZetaEvaluador.GetEmpleado  );
               ParamAsInteger( FQuery, 'Orden', StrToInt(FOrdenPension) );
               Active := True;
               while not Eof do
               begin
                    Result := ConcatString(Result,FieldByName('TP_CODIGO').AsString + '='+ FormatFloat( '##.00%', FieldByName('PP_PORCENT').AsFloat),',');
                    Next;
               end;
          end;
     end;
end;

{ ************* TZetaTFecha ************* }

type
  TZetaTFecha = class( TZetaRegresaQuery  )
  private
   function GetFecha: String; virtual;
  protected
  public
    function GetScript : string; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaTFecha.GetScript : String;
begin
     Result := 'select CH_HORAS ' +
               'from CHORAS ' +
               'where ( CHORAS.AU_FECHA = %s ) and ( CHORAS.CB_CODIGO = %s ) and ( CHORAS.CC_CODIGO = %s ) and ' +
               '      ( CHORAS.CH_TIPO + 1 = %s ) and ( CHORAS.CH_CLASIFI = %s )';
end;

function TZetaTFecha.GetFecha: String;
begin
     Result := ParamCampoFecha(0);
end;

procedure TZetaTFecha.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sFecha, sEmpleado, sCodigo, sClasifi, sTipo : string;
   iTipo: Integer;
begin
     ParametrosFijos( 1 );
     TipoRegresa := resDouble;
     EsFecha := FALSE;

     if ParamVacio(1) then
     begin
          sTipo := 'PROP_COSTO.CR_ID';
          AgregaRequerido( sTipo, enPropCost );
     end
     else
         sTipo := ParamCampoInteger(1);

     if ParamVacio(2) then
     begin
          sEmpleado := 'PROP_COSTO.CB_CODIGO';
          AgregaRequerido( sEmpleado, enPropCost );
     end
     else
         sEmpleado := ParamCampoInteger(2);

     if ParamVacio(3) then
     begin
          sCodigo := 'PROP_COSTO.CC_CODIGO';
          AgregaRequerido( sCodigo, enPropCost );
     end
     else
         sCodigo := ParamCampoString(3);

     if ParamVacio(4) then
     begin
          sClasifi := 'PROP_COSTO.PC_CLASIFI';
          AgregaRequerido( sClasifi, enPropCost );
     end
     else
         sClasifi := ParamCampoString(4);

     if ParametrosConstantes then
     begin
          AgregaScript( Format( GetScript, [ GetFecha, sEmpleado, sCodigo, sTipo, sClasifi ] ), TipoRegresa, EsFecha );
     end;
end;

function TZetaTFecha.Calculate: TQREvResult;
begin
     Result := EvaluaQuery( Format( GetScript, [ GetFecha, IntToStr( ZetaEvaluador.GetEmpleado ), ParamString( 3 ), ParamString(1), ParamString(4) ] ) );
end;

{ ************* TZetaTDia ************* }

type
  TZetaTDia = class( TZetaTFecha )
  protected
    function GetFecha: String; override;
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaTDia.GetFecha: String;
begin
     Result:= EntreComillas( DateToStrSQL( oZetaProvider.DatosPeriodo.InicioAsis + ( ParamInteger( 0 ) - 1 ) ) );
end;

procedure TZetaTDia.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos( 1 );
     oZetaProvider.GetDatosPeriodo;
     inherited;
end;

{ ************* TZetaTMonto ************* }

type
  TZetaTMonto = class( TZetaRegresaQuery  )
  private
  protected
  public
    function GetScript : string; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaTMonto.GetScript : String;
begin
     Result := 'select sum( CT_MONTO ) ' +
               'from COSTOS ' +
               'where ( PE_YEAR = %s ) and ( PE_TIPO = %s ) and ( PE_NUMERO = %s ) and ( CB_CODIGO = %s ) and ' +
               '( CO_NUMERO = %s ) and ( CC_CODIGO = %s ) and ( CR_ID = %s ) and ( CT_CLASIFI = %s )';
end;

procedure TZetaTMonto.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sEmpleado, sCodigo, sClasifi, sTipo : string;
   sYear, sTipoNom, sPeriodo: string;
begin
     ParametrosFijos( 1 );
     oZetaProvider.GetDatosPeriodo;
     TipoRegresa := resDouble;
     EsFecha := FALSE;

     if ParamVacio(1) then
     begin
          sEmpleado := 'PROP_COSTO.CB_CODIGO';
          AgregaRequerido( sEmpleado, enPropCost );
     end
     else
         sEmpleado := ParamCampoInteger(1);

     if ParamVacio(2) then
     begin
          sTipo := 'PROP_COSTO.CR_ID';
          AgregaRequerido( sTipo, enPropCost );
     end
     else
         sTipo := ParamCampoInteger(2);

     if ParamVacio(3) then
     begin
          sCodigo := 'PROP_COSTO.CC_CODIGO';
          AgregaRequerido( sCodigo, enPropCost );
     end
     else
         sCodigo := ParamCampoString(3);

     if ParamVacio(4) then
     begin
          sClasifi := 'PROP_COSTO.PC_CLASIFI';
          AgregaRequerido( sClasifi, enPropCost );
     end
     else
         sClasifi := ParamCampoString(4);

     if ParamVacio(5) then
     begin
          sYear := 'PROP_COSTO.PE_YEAR';
          AgregaRequerido( sYear, enPropCost );
     end
     else
         sYear := ParamCampoInteger(5);

     if ParamVacio(6) then
     begin
          sTipoNom := 'PROP_COSTO.PE_TIPO';
          AgregaRequerido( sTipoNom, enPropCost );
     end
     else
         sTipoNom := ParamCampoInteger(6);

     if ParamVacio(7) then
     begin
          sPeriodo := 'PROP_COSTO.PE_NUMERO';
          AgregaRequerido( sPeriodo, enPropCost );
     end
     else
         sPeriodo := ParamCampoInteger(7);

     if ParametrosConstantes then
     begin
          AgregaScript( Format( GetScript, [ sYear, sTipoNom, sPeriodo, sEmpleado, ParamCampoInteger(0), sCodigo, sTipo, sClasifi ] ), TipoRegresa, EsFecha );
     end;
end;

function TZetaTMonto.Calculate: TQREvResult;
begin
     Result := EvaluaQuery( Format( GetScript, [ ParamInteger(5), ParamInteger(6), ParamInteger(7), IntToStr( ZetaEvaluador.GetEmpleado ), ParamInteger(0), ParamString(3), ParamString(2), ParamString(4) ] ) );
end;

{*********************** REGISTER *********************}

procedure RegistraFuncionesPrestamo( FunctionLibrary: TQRFunctionLibrary );
begin
     with FunctionLibrary do
     begin
          RegisterFunction( TZetaPRESTAMOACTIVO, 'PRESTAMOA', 'Info Pr�stamos, sin el pr�stamo activo' ); //ERG

     end;
end;
procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );
begin
     with FunctionLibrary do
     begin
		  { DATOS ACTIVOS }
          {CV: 26-nov-2001}
          {La funcion Global se transfiri� hacia la unidad ZFuncsGlobal.
          Debido a que la poliza contable requeria esta funci�n y no se quiere
          que la poliza haga un uses de todo ZFuncsTress.}
          RegisterFunction( ZFuncsGlobal.TGetGlobal, 'Global','Info de Globales de Empresa' ); //ERG
          RegisterFunction( ZFuncsGlobal.TZetaACTIVO,'ACTIVO', 'Valores Activos Sistema' );

          RegisterFunction( TZetaLISTA, 'LISTA','Evalua Lista de Empleados' ); //ERG

          RegisterFunction( TZetaAHORRO, 'AHORRO', 'Informaci�n de Ahorros' ); //ERG
          RegisterFunction( TZetaPRESTAMO, 'PRESTAMO', 'Info Pr�stamos' ); //ERG

          RegisterFunction( TZetaSALDOAHORRO, 'SALDO_AHO', 'Saldo del ahorro a determinada fecha' ); //CV, agregada en Sept 2010
          RegisterFunction( TZetaSALDOPRESTAMO, 'SALDO_PRE', 'Saldo del pr�stamo a determinada fecha' ); //CV, agregada en Sept 2010

          RegisterFunction( TZetaCALENDARIOSUMA, 'CAL_SUMA', 'Suma Totales de Calendario' ); //ERG
          RegisterFunction( TZetaCALENDARIO, 'CAL', 'Totales de Calendario' ); //ERG                   ANTIG
          RegisterFunction( TZetaCSEMANA, 'SEMANA', 'Informaci�n de Tarjeta Diaria' ); //ERG
          RegisterFunction( TZetaCSEMANA, 'QUINCENA', 'Info de la Tarjeta Diaria' ); //ERG
          RegisterFunction( TZetaCSEMANASUMA, 'SUMA_SEMAN', 'Info de Tarjeta Diaria' ); //ERG
          RegisterFunction( TZetaCSEMANASUMA, 'SUMA_QUINC', 'Info de Tarjeta Diaria' ); //ERG

{$ifdef QUINCENALES}
          RegisterFunction( TZetaSUMAMONTOS, 'SUMA_MONTOS','Monto Total' );
          RegisterFunction( TZetaSUMAMONTOS, 'SM','Monto Total' );

          RegisterFunction( TZetaSUMAHORAS, 'SUMA_HORAS','Suma de Horas' );
          RegisterFunction( TZetaSUMAHORAS, 'SH', 'Suma de Horas' );

          RegisterFunction( TZetaSUMADIAS, 'SUMA_DIAS','Suma de D�as' );
          RegisterFunction( TZetaSUMADIAS, 'SD', 'Suma de D�as' );                                      

          RegisterFunction( TZetaSUMAEXENTO, 'SUMA_EXENTO','Suma de Monto ISPT' );
          RegisterFunction( TZetaIMSSEXENTO, 'IMSS_M_EXENTO','Monto Exento IMSS' );

{$endif}
          RegisterFunction( TZetaCDIA, 'C_DIA', 'Informaci�n de Checadas' ); //ERG
          RegisterFunction( TZetaCFECHA, 'C_FECHA', 'Valor de Expresi�n a una Fecha' ); //ERG
          RegisterFunction( TZetaCDIA, 'DIA','Expresi�n en Tarjeta Diaria' ); //ERG
          RegisterFunction( TZetaEMPLEADOINCAPACIDAD, 'EMP_INCAPA','Info Incapacidades' ); //ERG
          RegisterFunction( TZetaEMPLEADOPERMISO, 'EMP_PERMIS','Info Permisos' ); //ERG
          RegisterFunction( TZetaEMPLEADOVACACION, 'EMP_VACACI','Info Vacaciones' ); //ERG
          RegisterFunction( TZetaFECHAKARDEX, 'FECHA_KARD','Montos. de Kardex a una Fecha' ); //ERG
          RegisterFunction( TZetaSALARIOFECHA, 'SAL_FECHA', 'Salario a una Fecha' ); //ERG
          RegisterFunction( TZetaANTIGUEDAD, 'ANTIG','Antiguedad en D�as o A�os' ); //ERG     
          RegisterFunction( TZetaDIASACTIVOS, 'DIAS_ACTIV', 'Dias Activos en un Rango de Fechas' ); //ERG
          RegisterFunction( TZetaDIASBIM, 'DIAS_BIM', 'D�as Naturales en el Bimestre' ); //ERG
          RegisterFunction( TZetaDIASMES, 'DIAS_MES','D�as Calendario' ); //ERG
          RegisterFunction( TZetaCUMPLE, 'CUMPLE','Cumplea�os' ); //CV
          RegisterFunction( TZetaEDAD, 'EDAD','Edad' ); //ERG
          RegisterFunction( TZetaEDADLETRA, 'EDAD_LETRA','Edad en Letra' ); //ERG
          RegisterFunction( TZetaIMSS, 'IMSS','Info de la Tabla IMSS' ); //ERG
          RegisterFunction( TZetaNOMBRE, 'NOMBRE', 'Nombre del Empleado' ); //ERG
          RegisterFunction( TZetaDATOSVACA, 'DATOS_VACA','Info de Saldos de Vacaciones o Aguinaldo' ); //ERG
          RegisterFunction( TZetaGENERA_RFC, 'GENERA_RFC','Genera RFC' ); //MVR
          RegisterFunction( TZetaVALIDA_RFC, 'VALIDA_RFC','Valida RFC' ); //MVR
          RegisterFunction( TZetaGENERA_CURP, 'GENERACURP','Genera CURP' ); //ERG

          RegisterFunction( TZetaCODCLASIFI, 'COD_CLASIF', 'Clasificaci�n de un Puesto' ); //ERG
          RegisterFunction( TZetaCURSO, 'CUR','Informaci�n de Cursos' ); //ERG
          RegisterFunction( TZetaDOMICILIO, 'DOMICILIO', 'Domicilio' ); //ERG
          RegisterFunction( TZetaFACTORSDI, 'FACTOR_SDI','Factor Sal. Diario Integrado' ); //ERG
          RegisterFunction( TZetaFECHA_REFE, 'FECHA_REFE', 'Fecha de Referencia' ); //ERG
          RegisterFunction( TZetaFECHA_VENCI, 'FECHA_VENC', 'Fecha Vencimiento Contrato' ); //ERG
          RegisterFunction( TZetaF_BAJA, 'F_BAJA','Fecha Baja Interna o IMSS' ); //ERG
          RegisterFunction( TZetaF_CONTRATO, 'F_CONTRATO','Info Tipo Contrato' ); //ERG
          RegisterFunction( TZetaMODULO18, 'MODULO18', 'Si Cod. del Turno es Modulo18' ); //ERG
          RegisterFunction( TZetaNOMSTATUS, 'NOMSTATUS', 'Status de la N�mina Activa' ); //ERG
          RegisterFunction( TZetaSEXO, 'SEXO', 'Sexo' ); //ERG

          RegisterFunction( TZetaPUESTO, 'PUESTO', 'C�digo del Puesto' ); //EZM
          RegisterFunction( TZetaTPUESTO, 'TPUESTO', 'Desc. de un Puesto' ); //ERG
          RegisterFunction( TZetaRegresaNIVEL, 'NIVEL', 'C�digo de Nivel de Organigrama' ); //ERG
          RegisterFunction( TZetaEDOCIVIL,  'EDO_CIVIL','C�digo Edo Civil' ); //ERG

          RegisterFunction( TZetaSTATUSACTIVO, 'STATUS_ACT', 'Status del Empleado' ); //ERG
          RegisterFunction( TZetaSTATUSEMP, 'STATUS_EMP', 'Status del Empleado' ); //ERG
          RegisterFunction( TZetaRANGOSTATUS, 'RANGO_STAT', 'Si el Empleado tiene X Status' ); //ERG
          RegisterFunction( TZetaTTURNO, 'TTURNO', 'Desc. del Turno Actual' ); //ERG

          RegisterFunction( TZetaACUMULADOS, 'A', 'Monto Acumulado' ); //ERG
          {*** US 13904: Es necesario cambiar el proceso interno de Calculo De Acumulados en procesos de Afectar, Desafectar Nomina,y Rec�lculo de Acumulados ***}
          RegisterFunction( TZetaACUMULADOSDos, 'A2', 'Monto Acumulado por Raz�n Social' );
          RegisterFunction( TZetaACUMULADOMENSUAL, 'AM', 'Acumulado Mensual' ); //ERG
          RegisterFunction( TZetaACUMULADORELATIVO, 'AR', 'Acumulado de un Mes Relativo' ); //ERG
          {*** US 13904: Es necesario cambiar el proceso interno de Calculo De Acumulados en procesos de Afectar, Desafectar Nomina,y Rec�lculo de Acumulados ***}
          RegisterFunction( TZetaACUMULADORELATIVODos, 'AR2', 'Acumulado de un Mes Relativo por Raz�n Social' );
          RegisterFunction( TZetaACUMULADOSUMA, 'AS', 'Suma de Acumulados' ); //ERG
          {*** US 13904: Es necesario cambiar el proceso interno de Calculo De Acumulados en procesos de Afectar, Desafectar Nomina,y Rec�lculo de Acumulados ***}
          RegisterFunction( TZetaACUMULADOSUMADos, 'AS2', 'Suma de Acumulados por Raz�n Social' ); //
          {*** FIN ***}
          RegisterFunction( TZetaACUMULADOS, 'SAC', 'Suma de Acumulados' ); //ERG
          RegisterFunction( TZetaACUMULADOAJUSTA, 'AJ', 'Suma de Acumulados Anualiza' ); //CV

          RegisterFunction( TZetaCONCEPTO, 'C', 'Monto de un Concepto' ); //EZM
          RegisterFunction( TZetaCONCEPTORELATIVO, 'CR', 'Monto de Concepto p/ Mes Relativo' ); //ERG
          RegisterFunction( TZetaCONCEPTORANGO, 'CS', 'Suma de Conceptos' ); //ERG
          RegisterFunction( TZetaCONCEPTORANGORELATIVO, 'CSR','Suma de Conceptos Relativo' ); //ERG
          RegisterFunction( TZetaCONCEPTOREFERENCIA, 'CM', 'Suma de Conceptos' ); //ERG
          RegisterFunction( TZetaCONCEPTOREFERENCIASUMA, 'CMS', 'Suma de Conceptos con Referencia' ); //ERG

          RegisterFunction( TZetaTABLAGENERICO, 'TABLA_GENE', 'Info Tablas Opciones' ); //ERG
          RegisterFunction( TZetaTG, 'TG', 'Desc. de una Tabla' ); //ERG
          RegisterFunction( TZetaT, 'T', 'Info Tablas Opciones' ); //ERG

          RegisterFunction( TZetaTURNOFECHA, 'TURNO_FECH', 'C�digo del Turno a una Fecha' ); //ERG
          RegisterFunction( TZetaCALCONSEC, 'CAL_CONSEC', '# de Incidencias Consecutivas de la Condici�n' ); //ERG
          RegisterFunction( TZetaFALTASCONSEC, 'FALTAS_CON','Info Faltas Consecutivas' ); //ERG

          RegisterFunction( TZetaDIASINCA, 'DIAS_INCAP','Info Incapacidades DATOS_VACA Informaci�n Vacaciones' ); //ERG
          RegisterFunction( TZetaCLINCA, 'CL_INCA', 'C�digo Tipo Incapacidad' ); //ERG
          RegisterFunction( TZetaSUMAINCA, 'SUMA_INCA', 'Info Incapacidades' ); //ERG
          RegisterFunction( TZetaDIASBRINCA, 'DIAS_BRINC', 'D�as Incapacidad'); //ERG
          RegisterFunction( TZetaMOVKARDEX, 'MOV_KARDEX', 'Info Movimientos de Kardex' ); //ERG
          RegisterFunction( TZetaNOM, 'NOM', 'Variables de N�mina' ); //ERG
          RegisterFunction( TZetaNOMR, 'NOMR', 'Variables de N�mina -Relativo' ); //ERG
          RegisterFunction( TZetaNOMS, 'NOMS', 'Variables de N�mina -Rango' ); //ERG
          RegisterFunction( TZetaNOMSR, 'NOMSR', 'Variables de N�mina -Rango, Relativo' ); //ERG

          RegisterFunction( TZetaPOSKARDEX, 'POS_KARDEX', 'Info Movimiento de Kardex' ); //ERG
          RegisterFunction( TZetaCUALKARDEX, 'CUAL_KARDE','En�simo Mov. de Kardex' ); //ERG
          RegisterFunction( TZetaRELATED, 'RELATED', 'Valor de una Expresi�n' ); //ERG
          RegisterFunction( TZetaREL, 'REL', 'Valor que tiene una Expresi�n' ); //ERG

          // Carlos
          RegisterFunction( TZetaF_INGRESO, 'F_INGRESO', 'Fecha de Ingreso' );
          RegisterFunction( TZetaHD, 'HD', 'Total de Horas Trabajadas');
          RegisterFunction( TZetaHORAS_NT, 'HORAS_NT', 'N�mero de Horas No Trabajadas' );
          RegisterFunction( TZetaMIN_TARDE, 'MIN_TARDE', 'Minutos Tarde' ); //ERG
          RegisterFunction( TZetaDIA_TARDE,'DIA_TARDE','Minutos Tarde en una Fecha' ); //ERG
          RegisterFunction( TZetaPROT, 'PROT', 'Monto en Letra');
          RegisterFunction( TZetaREG_PATRONAL, 'REG_PATRON', 'Registro Patronal');
          RegisterFunction( TZetaSET_HEADER, 'SET_HEADER', 'Info de la N�mina Activa' );
          RegisterFunction( TZetaSET_HEADER, 'PERIODO', 'Info Periodo' );
          RegisterFunction( TZetaTAB_SALARIO, 'TAB_SALARI', 'Salario de Tabulador' );
          RegisterFunction( TZetaTAB_TAB, 'TAB_TAB', 'Tabulador a una Fecha' );
          RegisterFunction( TZetaTAB_GENERAL, 'TAB_GENERA', 'Info Tarjeta Diaria' );
          RegisterFunction( TZetaTAB_ORDINARIO, 'TAB_ORDINA', 'Salario Ordinario' );
          RegisterFunction( TZetaTAB_DOBLES, 'TAB_DOBLES', 'Salario Ordinario' );
          RegisterFunction( TZetaTAB_TRIPLES, 'TAB_TRIPLES', 'Tiempo Extra Triple' );
          RegisterFunction( TZetaOLD_PUESTO, 'OLD_PUESTO','Info Cambios Puesto/Area/Turno' );
          RegisterFunction( TZetaOP_GRAVADA, 'OP_GRAVADA', 'Monto Gravado Otras Percep.' );
          RegisterFunction( TZetaPRETTY_NAME,'PRETTY_NAM', 'Nombre Completo' );

          RegisterFunction( TZetaDELIMITA,'DELIMITA','Delimita Archivos ASCII' );
          RegisterFunction( TZetaTIPO_TRABAJA,'TIPO_TRABA', 'Tipo de Trabajador Imss' );
          RegisterFunction( TZetaPARIENTE,'PARIENTE', 'Info Pariente' );
          RegisterFunction( TZetaCHUELLAS,'CHUELLAS', 'Cantidad de Huellas' );

          {$ifndef DOS_CAPAS}
           {$ifdef TRESS}
           RegisterFunction( TZetaTF,'TF', 'Info de Tabla Fija' );
           {$endif}
          {$endif}
          RegisterFunction( TZetaClasifi,'CLASIFI', 'Clasificaci�n actual');
          RegisterFunction( TZetaTBAJA_IMSS, 'TBAJA_IMSS', 'Desc. Motivo Baja IMSS' );
          RegisterFunction( TZetaTINCAPACI, 'TINCAPACI', 'Desc. Clasif.  Incapacidad' );
          RegisterFunction( TZetaTIPO_JORNADA, 'TIPO_JORNA', 'Tipo de Jornada de un Puesto' );
          RegisterFunction( TZetaTIPO_SAL, 'TIPO_SAL', 'Tipo de Salario' );
          RegisterFunction( TZetaTNIVEL, 'TNIVEL', 'Desc. de un Nivel' );
          RegisterFunction( TZetaVACA_OTRAS, 'VACA_OTRAS', 'Info Vacaciones' );
          RegisterFunction( TZetaPRESTA_INFO, 'PRESTA_INF', 'Info Pr�stamos Infonavit' );
          RegisterFunction( TZetaANTES_CUR,'ANTES_CUR', 'Cursos Anteriores');
          RegisterFunction( TZetaANTES_PTO,'ANTES_PTO', 'Puestos Anteriores');
          RegisterFunction( TZetaTIPO_CAMBIO,'TIPO_CAMBI', 'Tipo de cambio a una fecha');
          RegisterFunction( TZetaFECPAGO, 'FEC_PAGO', 'Fecha de Pago N�mina' );
          RegisterFunction( TZetaFECHAINI, 'FECHA_INI','Fecha Inicial Periodo' );
          RegisterFunction( TZetaFECHAFIN, 'FECHA_FIN', 'Fecha Final Periodo' );
          {$ifdef QUINCENALES}
          RegisterFunction( TZetaFECHAASIINI, 'FECASI_INI','Fecha Inicio Asistencia del Periodo' );
          RegisterFunction( TZetaFECHAASIFIN, 'FECASI_FIN','Fecha Fin Asistencia del Periodo' );
          {$endif}
          RegisterFunction( TZetaV_FIJAS, 'V_FIJAS', 'Dias Vacaciones Derecho Fecha' );
          RegisterFunction( TZetaTURNO, 'TURNO', 'C�digo del Turno Actual');
          RegisterFunction( TZetaREINGRESO, 'REINGRESO', 'Reingreso Empleado ?' );
          RegisterFunction( TZetaOP, 'OP', 'Monto de Otras Percepciones');
          RegisterFunction( TZetaREC_PAGADO, 'REC_PAGADO', 'Recibo Pagado?');
          RegisterFunction( TZetaN_CURSOS, 'N_CURSOS', 'N�mero de Cursos');
          RegisterFunction( TZetaAUTORIZADA, 'AUTORIZADA', '# Horas Extras Autorizadas' );
          RegisterFunction( TZetaAUTORIZADAN, 'AUTORIZA_N', '# Horas Extras Autorizadas sobre el periodo de n�mina' );

          RegisterFunction( TZetaFECHA_HORARIO, 'FECHA_HORARIO', 'C�digo del horario a una fecha' ); //ERG
          RegisterFunction( TZetaFECHA_HABIL, 'FECHA_HABIL', 'D�a H�bil a una fecha' ); //ERG
          RegisterFunction( TZetaCHECO, 'CHECO', 'Tiene Checada a una Fecha?' );
          RegisterFunction( TZetaPROM_SAL_MIN, 'PROM_SAL_MIN', 'Promedio Sal Min Rango Fecha' );
          RegisterFunction( TZetaV_DERECHO, 'V_DERECHO', 'Dias Vacaciones Derecho A�o' );
          RegisterFunction( TZetaV_FUTURO, 'V_FUTURO', 'Dias Vacaciones Historial' ); //ERG
          RegisterFunction( TZetaCHECADA,'CHECADA', 'Informaci�n Checadas Diarias' );
          RegisterFunction( TZetaSUMA_DETALLE, 'SUMA_DETALLE', 'Suma de Ocurrencias Colabora');
          RegisterFunction( TZetaBAJA_IMSS, 'BAJA_IMSS', '# Clasificaci�n de Mot. Baja' );
          RegisterFunction( TZetaBAJA_IMSST, 'BAJA_IMSST', 'Clasificaci�n de Mot. Baja' );
          RegisterFunction( TZetaSST, 'SST', 'Status laboral a una fecha' );
          RegisterFunction( TZetaV_SALDO, 'V_SALDO','Saldos de vacaciones' ); // ER

          RegisterFunction( TZetaDiario, 'DIARIO', 'Consumos a una fecha' );
          RegisterFunction( TZetaSemanal, 'SEMANAL', 'Info Comidas Semanal' );
          RegisterFunction( TZetaAlDia, 'CAF_DIA', 'Movimientos de Cafeter�a' );
          RegisterFunction( TZetaInvSemana, 'INV_SEMANA','Invitaciones de Comidas' );

          RegisterFunction( TZetaEVALUA, 'EVALUA','Evaluaci�n de F�rmula' );
          RegisterFunction( TZetaUSUARIO, 'USUARIO', 'Regresa el Nombre del Usuario' );
          RegisterFunction( TZetaSUMA_KARDEX, 'SUMA_KARDEX', 'Suma de Expesi�n kardex' );
          RegisterFunction( TZetaLOOKUP, 'LOOKUP','Descripci�n del campo' );//CV
          RegisterFunction( TZetaSALARIO_CLA,'SALARIO_CLA','Salario de la clasificaci�n' ); //ERG

          RegisterFunction( TZetaDiasDomingo, 'DIAS_DOMIN', 'N�mero de Domingos en Rango' ); // EM
          RegisterFunction( TZetaDIAS_HABILES,'DIAS_HABIL','D�as H�biles' ); // EM

          RegisterFunction( TZetaRTotPer, 'TOT_PER', 'Variable Total Percepciones' );
          RegisterFunction( TZetaRTotDed, 'TOT_DED', 'Variable Total Deducciones' );
          RegisterFunction( TZetaRTotNeto,'TOT_NETO', 'Variable Total Neto' );

          RegisterFunction( TZetaGetHorario, 'GET_HORARIO','Info Horario Empleado' );
          RegisterFunction( TZetaHorario, 'HORARIO','Horario' );
          RegisterFunction( TZetaTipoDia, 'TIPODIA','Tipo d�a' );
          RegisterFunction( TZetaHayAuto, 'HAY_AUTO','Existe Autorizaci�n' );
          RegisterFunction( TZetaDIASRANGO, 'DIAS_RANGO','D�as en un rango de fechas' );
          RegisterFunction( TZetaPRESUPUESTO, 'PRESUPUEST', 'Presupuesto de Personal' );
          RegisterFunction( TZetaJEFE, 'JEFE','Jefe' );
          RegisterFunction( TZetaPOSICIONES, 'POSICIONES','Configuraci�n Organigrama' );
          RegisterFunction( TZetaPUESTOJEFE, 'PUESTOJEFE','Datos Puesto Jefe' );
          RegisterFunction( TZetaDATOSJEFE, 'DATOSJEFE','Datos Jefe' );

          RegisterFunction( TZetaTarjetaDecimal, 'TARJETADEC','Tarjeta proximidad Hexadecimal a Decimal' );
          RegisterFunction( TZetaCURASIS, 'CUR_ASIS','Informaci�n de Lista de Asistencia' );
          RegisterFunction( TZetaRESERVA, 'RESERVA','Informaci�n de Reservaci�n' );

          RegisterFunction( TZetaCUR_MAX, 'F_CURSO','Fecha de Curso' );//EZ
          RegisterFunction( TZetaSalPeriodo, 'SAL_PERIODO','Suma de Salario' );//EZ
          RegisterFunction( TZetaJefeP, 'JEFEP', 'Jefe de la plaza' );//EZ
          RegisterFunction( TZetaJefeE, 'JEFEE', 'Jefe del Empleado' );//EZ

          RegisterFunction( TZetaHCAL, 'HCAL','C�lculo entre Fechas' );//EZ
          RegisterFunction( TZetaHCAL_CONSE, 'HCAL_CONSE','M�ximo de Veces Consecutivas'  );//EZ
          RegisterFunction( TZetaHFALTAS_CO, 'HFALTAS_CO','# Faltas Injustificadas Consecutivas' );//EZ

          //Formulas para obtener la cuenta contable
          RegisterFunction( TZetaSubCuentaNivel, 'CTAN', 'Subcuenta nivel' );//CV
          RegisterFunction( TZetaSubCuentaEmpleado, 'CTAE', 'Subcuenta empleado' );//CV
          RegisterFunction( TZetaSubCuentaConcepto, 'CTAC', 'Subcuenta concepto' );//CV
          RegisterFunction( TZetaSubCuentaTabla, 'CTAT', 'Subcuenta tabla' );//CV
          RegisterFunction( TZetaCERT_MAX, 'F_CERTIFIC','Fecha de Certificaci�n' );//EZ
          RegisterFunction( TZetaBimestreNomina, 'NOM_BIM', 'Bimestre de n�mina' );//ER

          RegisterFunction( TZetaInfoEmpleado, 'INFO_EMPLEADO', 'Informaci�n del empleado' );//EZ
          RegisterFunction( TZetaDcConstancias, 'DC_CONSTA', '# Veces que tom� curso' );

          RegisterFunction( TZetaRACUMULADOS, 'RA', 'Monto Acumulado por Raz�n social' ); //AP
          RegisterFunction( TZetaRACUMULADORELATIVO, 'RAR', 'Acumulado de un Mes Relativo por Raz�n Social' ); //AP
          RegisterFunction( TZetaRACUMULADOSUMA, 'RAS', 'Suma de Acumulados por Raz�n Social' ); //ERG
          RegisterFunction( TZetaFECHAADD, 'FECHAADD','Modificar Fecha' ); //EZ
          RegisterFunction( TZetaFecVenciV, 'FECHAVENCV','Fecha Vencimiento Vacaciones' ); //EZ
          RegisterFunction( TZetaSumPrimDomEx,'SUM_PRIM_D', 'Exento de Prima Dominical');
          RegisterFunction( TZetaTopaPrimDomEx,'SUMA_PD', 'Prima Dominical');
          RegisterFunction( TZetaMesNomina, 'NOM_MES', 'Mes de la n�mina' );//EZ
          RegisterFunction( TZetaPension, 'PENSION', 'Monto a descontar de pensi�n alimenticia' ); //ez
          RegisterFunction( TZetaINFOPENSION, 'INFOPENSION', 'Informaci�n de la Pensi�n solicitada' ); //ez
          RegisterFunction( TZetaSQ, 'SQ', 'Suma Quincena Simplificada' ); //ez
          RegisterFunction( TZetaSQ, 'SS', 'Suma Semana Simplificada' ); //av
          RegisterFunction( TZetaTFecha, 'T_FECHA', 'Horas de Transferencias a una Fecha' ); //ER
          RegisterFunction( TZetaTDia, 'T_DIA', 'Horas de Transferencias de Costeo' ); //ER
          RegisterFunction( TZetaTMonto, 'T_MONTO', 'Montos de Transferencias de Costeo' ); //ER
     end;
end;


end.
