unit FPruebaReporte;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, Grids, DBGrids, ZAgenteSQL, ZSuperSQL, DSuperReporte,
  ZetaStateComboBox, ZetaCommonLists, ExtCtrls, ComCtrls, DBClient,
  DBCtrls, ZCreator, ZetaTipoEntidad, ZetaKeyCombo, DZetaServerProvider;

type
  TFormReporte = class(TForm)
    DBGrid1: TDBGrid;
    dsReporte: TDataSource;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    memoColumnas: TMemo;
    CreaReporte: TButton;
    editOrden: TEdit;
    Label1: TLabel;
    cdsReporte: TClientDataSet;
    Label2: TLabel;
    editFiltro: TEdit;
    Memo1: TMemo;
    Splitter1: TSplitter;
    SoloTotales: TCheckBox;
    Grupos: TEdit;
    DBMemo1: TDBMemo;
    Reabre: TButton;
    ComboEntidad: TComboBox;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure CreaReporteClick(Sender: TObject);
    procedure ReabreClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    oAgente    : TSQLAgente;
    oSuperSQL  : TSuperSQL;
    oSuperReporte : TdmSuperReporte;
    procedure EvaluaColumna( DataSet : TZetaCursor; oColumna : TSQLColumna; oCampo : TField );
    procedure MuestraErrores;
  public
    { Public declarations }
  end;

var
  FormReporte: TFormReporte;

implementation

uses DEntidadesTress, ZFuncsReporte, FPruebaEvaluador;

{$R *.DFM}

procedure TFormReporte.FormCreate(Sender: TObject);
begin
     oAgente := TSQLAgente.Create;
     oSuperSQL := TSuperSQL.Create( oZetaCreator );
     FormaEvaluador.LlenaComboEntidad( ComboEntidad.Items );
     ComboEntidad.ItemIndex := Ord( enEmpleado );
     oSuperReporte := TdmSuperReporte.Create( oZetaCreator );
     oSuperReporte.OnEvaluaColumna := EvaluaColumna;
     oZetaCreator.SuperReporte := oSuperReporte;
//   ZFuncsReporte.SetSuperReporte( oSuperReporte );
end;

procedure TFormReporte.FormDestroy(Sender: TObject);
begin
    oAgente.Free;
//  oSuperSQL.Free;
end;

procedure TFormReporte.MuestraErrores;
var i : Integer;
begin
     with oAgente do
       for i := 0 to NumErrores-1 do
           ShowMessage( ListaErrores[ i ] );
end;

procedure TFormReporte.CreaReporteClick(Sender: TObject);
var
   i : Integer;
   eTotalizacion : eTipoOperacionCampo;
begin
     with oAgente do
     begin
          Clear;
          Entidad := TipoEntidad( ComboEntidad.ItemIndex );
          with memoColumnas.Lines do
               for i := 0 to Count-1 do
               begin
                   if ( i < StrToIntDef( Grupos.Text, 0 )) then
                      AgregaColumna( Strings[ i ], FALSE, Entidad, tgAutomatico, 30, '', ocCuantos, TRUE )
                   else if Pos( 'IMAGEN', Strings[ i ] ) > 0 then
                       AgregaColumna( '@0', FALSE, Entidad, tgMemo, 0, 'FOTO' )
                   else
                   begin
                       if Pos( 'RESULT', Strings[ i ] ) > 0 then
                          eTotalizacion := ocSobreTotales
                       else
                            eTotalizacion := ocAutomatico;
                       AgregaColumna( Strings[ i ], FALSE, Entidad, tgAutomatico, 30, '', eTotalizacion );
                   end;
               end;
          // AgregaColumna( 'COUNT', FALSE, Entidad, tgAutomatico, 0, 'Conteo', ocAutomatico, FALSE, 0 );
          //AgregaColumna( 'SUM(CB_SALARIO)', FALSE, Entidad, tgAutomatico, 0, 'SumaSalario', ocAutomatico, FALSE, 0 );
          if Length( editOrden.Text ) > 0 then
//             AgregaOrden( editOrden.Text, TRUE, Entidad, FALSE )
               AgregaOrden( editOrden.Text, TRUE, Entidad, TRUE )
          else
          begin
             // Probando que valide repetidos
             AgregaOrden( 'COLABORA.CB_PUESTO', FALSE, Entidad, TRUE );
             AgregaOrden( 'COLABORA.CB_CLASIFI', FALSE, Entidad, TRUE );
             AgregaOrden( 'COLABORA.CB_PUESTO', FALSE, Entidad, TRUE );
          end;

          if Length( editFiltro.Text ) > 0 then
             AgregaFiltro( editFiltro.Text, FALSE, Entidad );
          if oSuperSQL.Construye( oAgente ) then
          begin
               Memo1.Lines := oAgente.SQL;
               if oSuperReporte.Construye( oAgente, SoloTotales.Checked ) then
               begin
                  cdsReporte.Data := oSuperReporte.GetReporte;
                  dsReporte.DataSet := cdsReporte;
                  if ( not SoloTotales.Checked ) then
                    oAgente.OrdenaDataset( cdsReporte );
                  {
                  for i := 0 to NumColumnas-1 do
                    with GetColumna( i ) do
                        if EsConstante then
                            ShowMessage( 'Constante # ' + IntToStr( i ) + ' = ' + ValorConstante );
                  }
               end
               else
               begin
                   cdsReporte.Close;
                   MuestraErrores;
               end;
{
               if not oSuperReporte.Prepara( oAgente, FALSE ) then
                cdsReporte.Close;
}

          end
          else
          begin
             MuestraErrores;
             dsReporte.DataSet := NIL;
          end;
     end;
end;

procedure TFormReporte.EvaluaColumna(DataSet: TZetaCursor;
  oColumna: TSQLColumna; oCampo: TField);
begin
    oCampo.AsString := 'Esta es una prueba ' + DataSet.Fields[ 0 ].AsString;
end;

procedure TFormReporte.ReabreClick(Sender: TObject);
begin
    with oZetaProvider, oSuperReporte do
    begin
        QueryReporte.Close;
        ParamAsInteger( QueryReporte, 'Empleado', StrToIntDef( editFiltro.Text, 0 ));
        if AbreDataSet then
            cdsReporte.Data := GetReporte;
    end;
end;

procedure TFormReporte.Button1Click(Sender: TObject);
begin
    with memoColumnas.Lines do
    begin
        Clear;
        Add( 'CO_NUMERO' );
        Add( 'CB_CODIGO' );
        Add( 'AC_YEAR' );
    end;
    editOrden.Text := 'CO_NUMERO';
    editFiltro.Text := 'AC_YEAR = 2000';
    ComboEntidad.ItemIndex := 1;
    SoloTotales.Checked := TRUE;
end;

end.
