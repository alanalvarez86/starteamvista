unit ZFuncsTools;

interface

uses DBClient, SysUtils,
     DZetaServerProvider,
     ZCreator,
     ZEvaluador,
     ZetaSQLBroker;

type
    TEmpleadosConAjuste = class(TObject)
    private
      FEvaluador: TZetaEvaluador;
      FSQLBroker: TSQLBroker;
      FExpresion: string;
      FCreator: TZetaCreator;
      function GetZetaProvider: TdmZetaServerProvider;
    protected
      { Protected declarations }
      property oZetaProvider: TdmZetaServerProvider read GetZetaProvider;
      property oZetaCreator: TZetaCreator read FCreator;
    public
      constructor Create( Creator: TZetaCreator );
      destructor Destroy; override;
      function AplicaAjuste(const iEmpleado: integer): Boolean;
      procedure PreparaEmpleadosConAjuste;
    end;

implementation

uses
    ZetaCommonLists,
    ZetaCommonTools,
    ZetaCommonClasses,
    ZetaTipoEntidad,
    ZGlobalTress,
    DSuperReporte;

constructor TEmpleadosConAjuste.Create( Creator: TZetaCreator );
begin
     FCreator := Creator;
     PreparaEmpleadosConAjuste;
end;

destructor TEmpleadosConAjuste.Destroy;
begin
     if Assigned( FSQLBroker ) then
        FreeAndNil( FSQLBroker );
     if Assigned( FEvaluador ) then
        FreeAndNil( FEvaluador );
     inherited Destroy;
end;

function TEmpleadosConAjuste.GetZetaProvider: TdmZetaServerProvider;
begin
     Result := FCreator.oZetaProvider;
end;

procedure TEmpleadosConAjuste.PreparaEmpleadosConAjuste;
begin
     FExpresion :=  oZetaProvider.GetGlobalString( K_GLOBAL_EMPLEADO_CON_AJUSTE );
     FEvaluador := TZetaEvaluador.Create( oZetaCreator );
     FEvaluador.Entidad := enEmpleado;
     FSQLBroker := TSQLBroker.Create( oZetaCreator );
     FSQLBroker.Init( enEmpleado );
     with FSQLBroker do
     begin
          with Agente do
          begin
               AgregaColumna( 'COLABORA.CB_CODIGO', TRUE, enEmpleado, tgNumero,0, 'CB_CODIGO' );
               AgregaColumna( FExpresion, FALSE, enEmpleado, tgBooleano, 0, 'AJUSTA_ISR' );
               AgregaFiltro( 'COLABORA.CB_CODIGO = :Empleado', TRUE, enEmpleado );
          end;
          if ( not SuperSQL.Construye( Agente ) ) or ( not SuperReporte.Prepara( Agente ) ) then
          begin
               raise Exception.Create( Agente.ListaErrores.Text );
          end;
     end;
end;

function TEmpleadosConAjuste.AplicaAjuste(const iEmpleado: integer): Boolean;
begin
     Result := StrVacio(FExpresion);
     if ( not Result ) then   // Si hay expresión se evalua
     begin
          with FSQLBroker.SuperReporte do
          begin
               if not ( DatasetReporte.Active and ( DatasetReporte.FieldByName('CB_CODIGO').AsInteger = iEmpleado ) ) then
               begin
                    QueryReporte.Active := FALSE;
                    oZetaProvider.ParamAsInteger( QueryReporte, 'Empleado', iEmpleado );
                    AbreDataSet;
               end;
               Result := ( DatasetReporte.FieldByName('AJUSTA_ISR').AsString = K_BOOLEANO_SI );
          end;
     end;
end;

end.
