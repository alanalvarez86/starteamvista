unit ZFuncsReporte;

interface

uses Classes, Sysutils, DB, DBClient,
     Variants,
     ZetaQRExpr,
     ZEvaluador;

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );

implementation

uses DEntidadesTress,
     DZetaServerProvider,
     DSuperReporte,
     DSalMin,
     ZetaSQLBroker,
     ZReportConst,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses;

{ ************ TZetaPARAM *************}

type
  TZetaPARAM = class( TZetaConstante )
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaPARAM.Calculate : TQrEvResult;
var
   iParam: Integer;
   uParam: Variant;
   eTipo : eTipoGlobal;
begin
     iParam := ParamInteger( 0 );
     if ZetaEvaluador.oZetaCreator.SuperReporte = NIL then
        ErrorFuncion( 'Los Par�metros no se Pueden Evaluar', 'Param( ' + IntToStr( iParam ) + ')' );

     if iParam in [ 1 .. ZetaCommonClasses.K_MAX_PARAM ] then
     begin
          with TdmSuperReporte(ZetaEvaluador.oZetaCreator.SuperReporte) do
          begin
               uParam := ZReportConst.GetParametro( Parametros, iParam, eTipo );
               if uParam <> NULL then
                  case eTipo of
                       tgBooleano :
                       begin
                            Result.Kind := resBool;
                            Result.booResult := uParam;
                       end;
                       tgFloat :
                       begin
                            Result.Kind := resDouble;
                            Result.dblResult := uParam;
                       end;
                       tgNumero :
                       begin
                            Result.Kind := resInt;
                            Result.intResult := uParam;
                       end;
                       tgFecha :
                       begin
                            EsFecha:= True;
                            Result.Kind := resDouble;
                            Result.dblResult := uParam;
                       end;
                       tgTexto :
                       begin
                            Result.Kind := resString;
                            Result.strResult := uParam;
                       end
                       else
                           ErrorParametro( 'Valor Indefinido', 'Param( ' + IntToStr( iParam ) + ' )' );
                  end
               else
                   ErrorParametro( 'Par�metro No est� Definido', 'Param( ' + IntToStr( iParam ) + ' )' );

          end;
     end
     else
         ErrorParametro( 'Fuera de Rango', IntToStr( iParam ));
end;

{ ************ TZetaSumaResult *************}
type
  TZetaSumaResult = class( TZetaFunc )
  protected
  public
    function Calculate : TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaSumaResult.Calculate : TQREvResult;
type eTipoCuales = (etTodos, etPares, etNones);
var
   iLower, iUpper, iResult : Integer;
   eCuales:  eTipoCuales;
   lSuma: Boolean;
   {CV: 10/12/2008 :
   {iCuales puede tener el valor de :
           0 : (default) sumar todos los valres
           1 : sumar solo valores Pares
           2 : sumar solo valores nones}

   nSuma : TPesos;
begin
     iLower := ParamInteger( 0 );
     iUpper := DefaultInteger( 1, iLower );
     lSuma := FALSE;

     //Si el segundo parametro es igual al primero, o no lo especificaron ,
     //la funcion regresa el valor del primer parametro
     if iLower = iUpper then
        eCuales := etTodos
     else
          eCuales := eTipoCuales( DefaultInteger( 2, 0 ));

     nSuma := 0;
     for iResult := iLower to iUpper do
     begin
          case eCuales of
               etTodos: lSuma := TRUE;
               etPares: lSuma := ( iResult mod 2 ) = 0 ;
               etNones : lSuma := ( iResult mod 2 ) = 1 ;
          end;

          if lSuma then
             with TdmSuperReporte(ZetaEvaluador.oZetaCreator.SuperReporte) do
                  nSuma := nSuma + GetResult( iResult );
     end;

     Result.Kind      := resDouble;
     Result.dblResult := nSuma;
end;

procedure TZetaSumaResult.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
     ParametrosFijos( 1 );
end;

{ ************ TZetaReporte *************}
type TZetaREPORTE = class( TZetaConstante )
  public
  procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  function Calculate: TQREvResult; override;
end;

function TZetaREPORTE.Calculate: TQREvResult;
 function GetParametros: string;
  var
      oParams, oParam: Variant;
      lUnParametro: Boolean;
      i, iParametro: integer;
      eTipo : eTipoGlobal;
      sValor : string;
 begin
      Result := VACIO;
      oParams := TdmSuperReporte(ZetaEvaluador.oZetaCreator.SuperReporte).Parametros;

      iParametro := DefaultInteger( 1, 0 );
      lUnParametro := ( iParametro > 0 );

      if NOT( VarIsNull( oParams ) or VarIsEmpty( oParams ) ) then
      begin
           for i:=1 to VarArrayHighBound(oParams,1) do
           begin
                oParam := GetParametro( oParams, i, eTipo  );

                if NOT( VarIsNull( oParam ) or VarIsEmpty( oParam ) ) then
                begin
                     case eTipo of
                          tgBooleano : sValor := BoolToSiNo( oParam );
                          tgFloat : sValor := FormatFloat('#,0.00', oParam );
                          tgNumero : sValor := IntToStr( oParam );
                          tgFecha : sValor := FechaCorta( oParam );
                          tgTexto : sValor := oParam
                          else
                              ErrorParametro( 'Valor Indefinido', 'Param( ' + IntToStr( i ) + ' )' );
                     end;
                     oParam := oParams[i];
                     
                     if NOT lUnParametro or (lUnParametro AND (iParametro = i ) ) then
                        Result:= ConcatString( Result, oParam[1] + '=' + sValor, CR_LF );
                end;
           end;
      end;
 end;


 var iParam : integer;
begin
     iParam := ParamInteger( 0 );
     with Result, oZetaProvider.ParamList do
     begin
	  Kind := resString;
	  case iParam of
	       1 :
               begin
                    if (ParamByname('Nombre').AsString  = ParamByname('Titulo').AsString) then
                       strResult := VACIO
                    else
                        strResult := ParamByname('Nombre').AsString;
               end;
               2: strResult := ParamByname('Titulo').AsString;

               {2 :
               begin
                    if (ParamByname('Nombre').AsString  = ParamByname('Titulo').AsString) then
                       strResult := VACIO
                    else
                        strResult := ParamByname('Titulo').AsString;
               end;}
               3 : strResult := ParamByname('Codigo').AsString;
	       4 : strResult := ParamByname('Filtro').AsString;
	       5 : strResult := ParamByname('Condicion').AsString;
	       6 :
               begin
                    strResult := ParamByname('FiltroDescrip').AsString;
                    if StrLleno(ParamByname('Condicion').AsString) then
                    begin
                         if StrLleno(strResult) then
                            strResult := strResult +CR_LF;
                         strResult := strResult + ParamByname('Condicion').AsString;
                    end;
               end;
               7 : strResult := ParamByname('SoloTotalesDes').AsString;
               8 : strResult := ZetaEvaluador.oZetaCreator.dmEntidadesTress.DescripcionEntidad(TipoEntidad(ParamByname('TablaPrincipal').AsInteger));
               9 : strResult := 'Registros';
               10: strResult := GetParametros;
	       else
                   ErrorFuncion( 'Par�metro', 'Par�metro Descontinuado. N�mero:' + IntToStr( iParam ) );
          end;
     end;
end;

procedure TZetaREPORTE.GetRequeridos(var TipoRegresa: TQREvResultType);
begin
     inherited;
     ParametrosFijos( 1 );
     TipoRegresa := resString;
end;

{******************* TZetaFILTRO *******************}
type TZetaFILTRO = class( TZetaConstante )
public
  procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  function Calculate: TQREvResult; override;
end;

function TZetaFILTRO.Calculate: TQREvResult;
begin
     Result.Kind := resString;
     Result.strResult := oZetaProvider.ParamList.ParamByname('FiltroDescrip').AsString;
end;

procedure TZetaFILTRO.GetRequeridos( var TipoRegresa: TQREvResultType);
begin
     ParametrosFijos( 1 );
     TipoRegresa := resString;
end;

const K_MAX_MONEDAS = 25;
type
  TZetaDota = class( TZetaFunc )
  private
    FMonedas: Integer;
    aMonedas: array [ 0..K_MAX_MONEDAS ] of TPesos;
    function GetDotacion( nMonto: TPesos;
                          nPosicion,
                          nPrimero,
                          nUltimo: Integer): Integer;
    procedure LlenaMonedas;
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

procedure TZetaDota.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resInt;
     ParametrosFijos( 2 );
end;

function TZetaDota.Calculate: TQREvResult;
begin
     Result.Kind := resInt;
     Result.intResult := GetDotacion( ParamFloat( 1 ),
                                      ParamInteger( 0 ),
                                      DefaultInteger( 2, 0 ),
                                      DefaultInteger( 3, 0 ) );
end;

function TZetaDota.GetDotacion( nMonto : TPesos; nPosicion, nPrimero, nUltimo : Integer ) : Integer;
var
   i, nDotacion : Integer;
   nMoneda : TPesos;
begin
     // Si no est� listo el arreglo de monedas, llenarlo
     if ( FMonedas = 0 ) then
        LlenaMonedas;

     if ( nPrimero <= 0 ) then
        nPrimero := 1;

     nMonto := ZetaCommonTools.Redondea( nMonto );

     if ( nUltimo <= 0 ) OR ( nUltimo > FMonedas ) then
        nUltimo := FMonedas;

     Result := 0;  // Por si pasan un posici�n fuera del rango

     for i := nPrimero to nUltimo do
     begin
          if aMonedas[ i ] <= 0 then
             raise Exception.Create('Monto de la Moneda no Puede Ser Cero. Revise Tabla de Monedas');

          nMoneda      := aMonedas[ i ];
          nDotacion    := Trunc( nMonto / nMoneda );

          // Si es la posici�n relativa que estoy solicitando, ya termin�
          //if ( i - nPrimero + 1 ) = nPosicion then
          if i = nPosicion then
          begin
               Result := nDotacion;
               Exit;
          end;

          nMonto := Redondea( nMonto - ( nDotacion * nMoneda ));
     end;
end;

procedure TZetaDota.LlenaMonedas;
 const Q_DOTACION = 'SELECT TB_VALOR FROM MONEDA  '+
                    'ORDER BY TB_CODIGO';
 var Dotacion : TZetaCursor;
begin
     with oZetaProvider do
          Dotacion := CreateQuery( Q_DOTACION );

     with Dotacion do
     begin
          Active := TRUE;

          while ( FMonedas < K_MAX_MONEDAS ) AND ( NOT Eof ) do
          begin
               FMonedas := FMonedas + 1;
               aMonedas[ FMonedas ] := Redondea( Fields[0].AsFloat );
               Next;
          end;
          Active := FALSE;
     end;
end;


{**************** TZetaLinea *******************}
type
 TZetaLinea = class( TZetaRegresaInt )
  private
    FLinea : integer;
  public
    constructor Create(oEvaluador: TQrEvaluator);override;
    function Calculate: TQREvResult; override;
  end;

constructor TZetaLinea.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create(oEvaluador);
     FLinea := 1;
     lQuitarOptimizeCursorB8 := FALSE;         // Si no requiere un query en el calculate, se deja optimizaci�n de Build #8
end;

function TZetaLinea.Calculate : TQREvResult;
begin
     Result.Kind := resInt;
     Result.intResult := FLinea;
     Inc(FLinea);
end;


{ ************ TZetaSubTotal *************}
type
  TZetaSubTotal = class( TZetaFunc )
  protected
  public
    function Calculate : TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaSubTotal.Calculate : TQREvResult;
var
   iResult, iCorte : Integer;
begin
     iResult := ParamInteger( 0 );
     iCorte  := DefaultInteger( 1, 1 );
     with TdmSuperReporte(ZetaEvaluador.oZetaCreator.SuperReporte) do
     begin
        Result.Kind      := resDouble;
        Result.dblResult := GetSubTotal( iResult, iCorte );
     end;
end;

procedure TZetaSubTotal.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
     ParametrosFijos( 1 );
     ZetaEvaluador.AgregaSubTotal( ParamInteger( 0 ));
end;

{ ************ TZetaTotal *************}
type
  TZetaTotal = class( TZetaSubTotal )
  protected
  public
    function Calculate : TQREvResult; override;
  end;

function TZetaTotal.Calculate : TQREvResult;
var
   iResult : Integer;
begin
     iResult := ParamInteger( 0 );
     with TdmSuperReporte(ZetaEvaluador.oZetaCreator.SuperReporte) do
     begin
        Result.Kind      := resDouble;
        Result.dblResult := GetSubTotal( iResult, 0 );
     end;
end;

{ ************ TZetaCuantosSubTotal *************}
type
  TZetaCuantosSubTotal = class( TZetaFunc )
  protected
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate : TQREvResult; override;
  end;

procedure TZetaCuantosSubTotal.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resInt;
     ZetaEvaluador.AgregaSubTotal( 0 );
end;

function TZetaCuantosSubTotal.Calculate : TQREvResult;
var
   iCorte : Integer;
begin
     iCorte  := DefaultInteger( 0, 1 );
     with TdmSuperReporte(ZetaEvaluador.oZetaCreator.SuperReporte) do
     begin
        Result.Kind      := resInt;
        Result.intResult := GetCuantosSubTotal( iCorte );
     end;
end;

{ ************ TZetaCuantosTotal *************}
type
  TZetaCuantosTotal = class( TZetaCuantosSubTotal )
  protected
  public
    function Calculate : TQREvResult; override;
  end;

function TZetaCuantosTotal.Calculate : TQREvResult;
begin
     with TdmSuperReporte(ZetaEvaluador.oZetaCreator.SuperReporte) do
     begin
        Result.Kind      := resInt;
        Result.intResult := GetCuantosSubTotal( 0 );
     end;
end;

{$ifdef TRESS}
{ ************ TZetaImagen *************}
type
  TZetaImagen = class( TZetaFunc )
  protected
  public
        function Calculate: TQREvResult; override;
        procedure GetRequeridos(var TipoRegresa: TQREvResultType);override;
  end;

procedure TZetaImagen.GetRequeridos(var TipoRegresa: TQREvResultType);
begin
     TipoRegresa := resString;
     ParametrosFijos( 1 );
     AgregaRequerido('COLABORA.CB_CODIGO', enEmpleado);
end;


function TZetaImagen.Calculate: TQREvResult;

begin
     with Result do
     begin
          Kind := resString;
//          strResult := DatasetEvaluador.FieldByName( 'CB_CODIGO' ).AsString;
          strResult := IntToStr( DatasetEvaluador.FieldByName( 'CB_CODIGO' ).AsInteger );

          with oZetaProvider.ParamList do
          begin
               if FindParam( 'GRABA_IMAGEN_DISCO' ) = NIL then
               begin
                    AddString( 'GRABA_IMAGEN_DISCO',VerificaDir( ParamString(0) ) );
                    AddString( 'TIPO_IMAGEN_DISCO',DefaultString(1,'FOTO') );
                    if ( TdmSuperReporte(ZetaEvaluador.oZetaCreator.SuperReporte) <> NIL ) then
                    begin
                         with TdmSuperReporte(ZetaEvaluador.oZetaCreator.SuperReporte) do
                              if ( Agente <> NIL ) and
                                 ( Agente.GetColumna( Agente.ColumnaCalculada ) <> NIL ) then
                                   AddInteger( 'COL_IMAGEN_DISCO', Agente.GetColumna( Agente.ColumnaCalculada ).PosCampo );
                    end;
               end;
          end;
     end;
end;

{$ENDIF}

{$IFDEF TRESS}
// (JB) Anexar al expediente del trabajador documentos CR1880 T1107
{ ************ TZetaDocumento ******************}
type TZetaDocumento = class( TZetaFunc )
  private
  protected
  public
    procedure GetRequeridos(var TipoRegresa: TQREvResultType);override;
    function Calculate: TQREvResult; override;
end;
procedure TZetaDocumento.GetRequeridos(var TipoRegresa: TQREvResultType);
begin
     TipoRegresa := resString;
     ParametrosFijos( 1 );
     AgregaRequerido('COLABORA.CB_CODIGO', enEmpleado);
end;


function TZetaDocumento.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resString;
          strResult := IntToStr( DatasetEvaluador.FieldByName( 'CB_CODIGO' ).AsInteger );


          with oZetaProvider.ParamList do
          begin
               if FindParam( 'GRABA_DOCUMENTO_DISCO' ) = NIL then
               begin
                    AddString( 'GRABA_DOCUMENTO_DISCO',VerificaDir( ParamString(0) ) );
                    AddString( 'TIPO_DOCUMENTO_DISCO',DefaultString(1,'CONTRATO') );
                    if ( TdmSuperReporte(ZetaEvaluador.oZetaCreator.SuperReporte) <> NIL ) then
                    begin
                         with TdmSuperReporte(ZetaEvaluador.oZetaCreator.SuperReporte) do
                              if ( Agente <> NIL ) and
                                 ( Agente.GetColumna( Agente.ColumnaCalculada ) <> NIL ) then
                                   AddInteger( 'COL_DOCUMENTO_DISCO', Agente.GetColumna( Agente.ColumnaCalculada ).PosCampo );
                    end;
               end;
          end;
     end;
end;
{$ENDIF}

{$IFDEF TRESS}
{ ************ TZetaTotalEmpleados *************}
type
  TZetaTotalEmpleados = class( TZetaFunc )
  private
         oDataset: TZetaCursor;
  public
        constructor Create(oEvaluator:TQrEvaluator); override;
        destructor Destroy;override;
        function Calculate: TQREvResult; override;
        procedure GetRequeridos(var TipoRegresa: TQREvResultType);override;
  end;

constructor TZetaTotalEmpleados.Create(oEvaluator:TQrEvaluator);
begin
     inherited Create(oEvaluator);
     //oDataset := TZetaCursor.Create(oZetaProvider);
end;

destructor TZetaTotalEmpleados.Destroy;
begin
     FreeAndNil(oDataset);
end;

procedure TZetaTotalEmpleados.GetRequeridos(var TipoRegresa: TQREvResultType);
 const
      Q_BORRA_REP_EMP= 'delete from REP_EMPS where ( US_CODIGO = %d )';

begin
     oZetaProvider.OptimizeCursorB8 := FALSE;

     TipoRegresa := resInt;
     AgregaRequerido('COLABORA.CB_CODIGO', enEmpleado);

     with oZetaProvider do
     begin
          EjecutaAndFree(Format(Q_BORRA_REP_EMP,[UsuarioActivo]));
     end;
end;


function TZetaTotalEmpleados.Calculate: TQREvResult;
 const
      {$IFDEF INTERBASE}
      Q_SELECT = 'select RESULTADO from SP_TOT_EMPLEADOS(:Usuario,:Empleado)';
      {$ELSE}
      Q_SELECT = '{CALL SP_TOT_EMPLEADOS(:Usuario, :Empleado, :Resultado)}';
      {$ENDIF}
begin
     Result.Kind := resInt;
     with oZetaPRovider do
     begin
          if ( oDataSet = NIL ) then
             oDataset := CreateQuery( Q_SELECT );

          with oDataset do
          begin
               {$ifdef INTERBASE}
               Active := FALSE;
               {$endif}
               ParamAsInteger( oDataset, 'Usuario', UsuarioActivo );
               ParamAsInteger( oDataset, 'Empleado', DataSetEvaluador.FieldByName('CB_CODIGO').AsInteger );
               {$ifdef INTERBASE}
               Active := TRUE;
               Result.intResult := Fields[0].AsInteger;
               {$ELSE}
               ParamSalida( oDataset, 'Resultado' );
               Ejecuta( oDataset );
               Result.intResult := GetParametro( oDataset, 'Resultado' ).AsInteger;
               {$endif}
          end;
     end;
end;

{$ENDIF}

{ *************************}

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );
begin
     with FunctionLibrary do
     begin
          RegisterFunction( TZetaPARAM, 'PARAM', 'Par�metro de Reporte' );
          RegisterFunction( TZetaSumaResult, 'RESULT', 'Resultado Columnas Anteriores' );
          RegisterFunction( TZetaSumaResult, 'SUMA_RESUL', 'Suma de Resultados de Reporte' );
          RegisterFunction( TZetaSumaResult, 'RR', 'Suma de Resultados de Reporte' );
          RegisterFunction( TZetaREPORTE, 'REPORTE', 'Informaci�n del Reporte' );
          RegisterFunction( TZetaFILTRO, 'FILTRO', 'Filtro del Reporte' );
          RegisterFunction( TZetaDota, 'DOTA', 'Dotaci�n de Moneda' );
          RegisterFunction( TZetaLinea, 'LINEA','Lineas Secuencial Ascii' );
          RegisterFunction( TZetaSubTotal, 'SUBTOTAL', 'Subtotal' );
          RegisterFunction( TZetaTotal, 'TOTAL', 'Total' );
          RegisterFunction( TZetaCuantosSubTotal, 'NSUBTOTAL','# Registros Detalle de un Reporte' );
          RegisterFunction( TZetaCuantosTotal, 'NTOTAL','# Registros Detalle de un Reporte' );
          {$IFDEF TRESS}
          RegisterFunction( TZetaImagen, 'IMAGEN','Imagen' );
          RegisterFunction( TZetaDocumento, 'DOCUMENTO','Documento' );
          RegisterFunction( TZetaTotalEmpleados, 'TOT_EMP','Total de Empleados' );
          {$ENDIF}
     end;
end;

end.
