unit ZFuncsFonacot;

interface

uses Classes, Sysutils, DB,
     ZetaQRExpr,
     ZEvaluador,
     dImssClass,
     ZetaCommonClasses,
     ZGlobalTress;


procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );

implementation

uses ZSuperEvaluador,
     ZetaCommonLists,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaServerTools,
     ZetaSQLBroker,
     DCalculoNomina,
     Controls,
     DSalMin,
     DValUma,
     DPrimasIMSS,
     DZetaServerProvider,
     DSuperReporte, ZCreator,  DBClient;


{ ******************* TZetaPRESTAMO **************************}

type
  TZetaCALC_FON = class( TZetaRegresaQuery )
  private
    FTipoAjuste: Integer;                     // Par�metro 1. Tipo de ajuste.
    FAustarIncluyendoIncapacitados: Boolean;  // Par�metro 2. Ajustar incluyendo incapacitados.
    rSaldoPrestamoAjuste: TPesos;             // Par�metro 3. Saldo de Prestamo para Ajuste
    FIncapacidadDias: Integer;                // Par�metro 4. D�as de incapacidad.
    FIncapacidadesMes: Integer;               // Par�metro 5. D�as de incapacidad en los per�odos anteriores del mes.
    FAjusteMesAnterior: Boolean;              // Par�metro 6. Ajustar mes anterior.
  public
    function GetScript : String; override;
    function Calculate: TQREvResult; override;
  end;

function TZetaCALC_FON.GetScript : String;
begin
     Result := 'SELECT RETENCION_PERIODO, MONTO_CEDULA, MONTO_RETENIDO, MONTO_ESPERADO, AJUSTE, MONTO_CONCEPTO, DEUDA_MA, NUM_PERIODO_MES, POS_PERIODO_MES, MES_ANTERIOR, YEAR_ANTERIOR ' +
               ' FROM dbo.FN_CALCULA_FON (%d, %s, %s, %d, %s, %m, %d, %d, %s, %d, %d, %d, %d)';
end;

function TZetaCALC_FON.Calculate: TQREvResult;
var
   iEmpleado: Integer;
   sAjusteMesAnterior, sAustarIncluyendoIncapacitados, sConsultaSql: String;
   nMontoCedula, nDeudaMA, nMontoRetenido, nMontoEsperado : TPesos;
   FDataSet: TZetaCursor;
begin
     Result.Kind      := resDouble;

     // Par�metro 1.
     // Tipo de ajuste.
     FTipoAjuste := DefaultInteger (0, 0);
     // Si el FTipoAjuste que se recibe es menor a 0 o mayor a 3, se usa el default que es 0.
     if (FTipoAjuste < 0) or (FTipoAjuste > 3) then
        FTipoAjuste := 0;

     // Par�metro 2 (nuevo).
     // Ajustar incluyendo a empleados con incapacidad.
     FAustarIncluyendoIncapacitados := DefaultBoolean (1, TRUE);
     sAustarIncluyendoIncapacitados := K_GLOBAL_SI;
     if not FAustarIncluyendoIncapacitados then
        sAustarIncluyendoIncapacitados := K_GLOBAL_NO;

     // Par�metro 3
     // Saldo de Prestamo para Ajuste
     rSaldoPrestamoAjuste := DefaultFloat( 2, 0.0 );
     //Si el valor de ajuste en negativo es tomado en cuenta por que es un saldo a favor y se tomara su abs para mandarlo a la funci�n de SQL
     //en caso que llegue en positivo, es debido a que es un cargo al prestamo y se debe mandar un default de 0.0
     if rSaldoPrestamoAjuste < 0 then
     begin
          rSaldoPrestamoAjuste := Abs(rSaldoPrestamoAjuste);
     end
     else
     begin
          rSaldoPrestamoAjuste := 0.0;
     end;

     // Par�metro 4
     // D�as de incapacidad.
     FIncapacidadDias := DefaultInteger (3, -1);

     // Par�metro 5
     // D�as de incapacidad en el mes (per�odos anteriores).
     FIncapacidadesMes := DefaultInteger (4, -1);

     // Par�metro 6.
     FAjusteMesAnterior := DefaultBoolean (5, FALSE);


     sAjusteMesAnterior := K_GLOBAL_NO;
     if FAjusteMesAnterior then
        sAjusteMesAnterior := K_GLOBAL_SI;

     iEmpleado := ZetaEvaluador.GetEmpleado;

     if iEmpleado > 0 then
     begin
          sConsultaSql := Format (GetScript,
                   [iEmpleado, EntreComillas (TCalcNomina(ZetaEvaluador.CalcNomina).ReferenciaActiva),
                    EntreComillas (oZetaProvider.GetGlobalString (K_GLOBAL_FONACOT_PRESTAMO)),
                    FTipoAjuste, EntreComillas (sAustarIncluyendoIncapacitados), rSaldoPrestamoAjuste,
                    FIncapacidadDias, FIncapacidadesMes, EntreComillas (sAjusteMesAnterior),
                    oZetaProvider.DatosPeriodo.Year, oZetaProvider.DatosPeriodo.MesFonacot,
                    oZetaProvider.DatosPeriodo.Numero, oZetaProvider.DatosPeriodo.Tipo]);

          FDataSet := oZetaProvider.CreateQuery ( sConsultaSql );

          try
             FDataSet.Active := True;

             if not FDataSet.Eof then
             begin
                  Result.dblResult := FDataSet.FieldByName( 'MONTO_CONCEPTO' ).AsFloat;
             end
             else
                 Result.dblResult := 0.0;
             FDataSet.Active := False;
          finally
                 FreeAndNil( FDataset );
          end;
     end;
end;


{********************** FUNCIONES P�BLICAS ***************************}

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );
begin
     with FunctionLibrary do
     begin
          // FUNCIONES PARA CALCULOS DE FONACOT
          RegisterFunction( TZetaCALC_FON, 'CALC_FON','C�lculo de pr�stamo Fonacot' );
          {*** FIN ***}
     end;
end;

end.
