unit ZFuncsAguinaldo;

interface

uses Classes, Sysutils, Controls,
     ZetaQRExpr,
     ZEvaluador,
     ZetaCommonClasses;

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );

implementation

uses DZetaServerProvider,
     ZetaCommonTools,
     ZetaTipoEntidad,
     DSuperReporte;

{TZetaAnualAGUI_INI}
type
    TZetaAnualAGUI_INI = class( TZetaFunc )
public
  function Calculate: TQREvResult; override;
  procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
end;

procedure TZetaAnualAGUI_INI.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
     EsFecha := True;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_FEC_ING', enEmpleado );
end;

function TZetaAnualAGUI_INI.Calculate: TQREvResult;
var
   dFechaIng, dFechaInicio: TDate;
   iAnio: integer;
begin
     dFechaIng := DefaultDate( 0, DatasetEvaluador.FieldByName( 'CB_FEC_ING' ).AsDateTime );
     iAnio := DefaultInteger( 1 , 0 );

     if ( iAnio = 0 ) then
        dFechaInicio := DatasetEvaluador.FieldByName( 'FechaInicial' ).AsDateTime
     else
         dFechaInicio := FirstDayOfYear( iAnio );

     with Result do
     begin
          Kind := resDouble;
          EsFecha := TRUE;
          dblResult := rMax( dFechaIng, dFechaInicio );
     end;
end;

{TZetaAnualAGUI_FIN}
type
    TZetaAnualAGUI_FIN = class( TZetaFunc )
public
  function Calculate: TQREvResult; override;
  procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
end;

procedure TZetaAnualAGUI_FIN.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
     EsFecha := True;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_FEC_BAJ', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_ACTIVO', enEmpleado );
end;

function TZetaAnualAGUI_FIN.Calculate: TQREvResult;
var
   dFechaBaja, dFechaFinal: TDate;
   iAnio: integer;
   lActivo : boolean;

begin
     dFechaBaja := DefaultDate( 0, DatasetEvaluador.FieldByName( 'CB_FEC_BAJ' ).AsDateTime );
     iAnio := DefaultInteger( 1 , 0 );
     {CR 1889 :AV Se anexa la validacion sobre si el empleado esta activo o no }
     lActivo :=  zStrToBool( DatasetEvaluador.FieldByName( 'CB_ACTIVO' ).AsString ) or ( dFechaBaja = NullDateTime );


     if ( iAnio = 0 ) then
        dFechaFinal := DatasetEvaluador.FieldByName( 'FechaFinal' ).AsDateTime
     else
         dFechaFinal := LastDayOfYear( iAnio );

     with Result do
     begin
          Kind := resDouble;
          EsFecha := TRUE;
          if lActivo then
             dblResult := dFechaFinal
          else dblResult := rMin( dFechaBaja, dFechaFinal );
     end;
end;

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );
begin
     with FunctionLibrary do
     begin
          RegisterFunction( TZetaAnualAGUI_INI, 'AGUI_INI', 'Fecha de ingreso e inicio de a�o'  );
          RegisterFunction( TZetaAnualAGUI_FIN, 'AGUI_FIN', 'Fecha final o de baja' );
     end;
end;

end.
