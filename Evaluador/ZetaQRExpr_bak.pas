 { :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: QuickReport 2.0 for Delphi 1.0/2.0/3.0                  ::
  ::                                                         ::
  :: QRPRNTR - QRPrinter and other low level classes         ::
  ::                                                         ::
  :: Copyright (c) 1997 QuSoft AS                            ::
  :: All Rights Reserved                                     ::
  ::                                                         ::
  :: web: http://www.qusoft.no    mail: support@qusoft.no    ::
  ::                              fax: +47 22 41 74 91       ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

unit ZetaQRExpr_bak;

{$R-}
{$T-} { We don't need (nor want) this type checking! }
{$B-} { QuickReport source assumes boolean expression short-circuit  }

interface

{$INCLUDE JEDI.INC}

uses
  SysUtils, Classes, DB, QR2Const, ZetaCommonClasses, ZetaCommonTools, DZetaServerProvider, ZetaCommonLists;


type
  EErrorEvaluador = Class(Exception);
type
  TQRErrorString = String[100];
  TQRErrorEvaluador = (eeDesconocido,
                 eeSintaxis,
                 eeConstante,
                 eeTipos,
                 eeLogicos,
                 eeFuncionPendiente,
                 eeFuncionNoExiste,
                 eeVariableNoExiste,
                 eeParentesis,
                 eeFuncion,
                 eeFuncionParam );
type
  RecErrorEvaluador = record
    ErrTipo    : TQRErrorEvaluador;
    ErrDescrip : String;
    ErrMsg     : String;
    ErrExp     : String;
    ErrCompleto: String;
  end;

const
   cQRErrorDescrip : array[eeDesconocido..eeFuncionParam] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} =
       ('Desconocido',
        'Sintaxis',
        'Constante',
        'Tipos',
        'Faltan Par�ntesis al Comparar',
        'Funci�n Pendiente',
        'Funci�n No Existe',
        'Campo No Existe',
        'Par�ntesis',
        'Calculando Funci�n',
        'Par�metros de Funci�n' );

{ **************** Fin EZM ****************** }

type
  TQREvElementFunction = class;
  TQREvElement = class;

  { TQRLibraryEntry }
  TQRLibraryItemClass = class of TObject;

  TQRLibraryEntry = class
  private
    FDescription : string;
    FData : string;
    FItem : TQRLibraryItemClass;
    FName : string;
    FVendor : string;
  public
    property Data : string read FData write FData;
    property Description : string read FDescription write FDescription;
    property Name : string read FName write FName;
    property Vendor : string read FVendor write FVendor;
    property Item : TQRLibraryItemClass read FItem write FItem;
  end;

  { TQRLibrary }
  TQRLibrary = class
  private
    Entries : TStrings;
  protected
    function GetEntry(Index : integer) : TQRLibraryEntry; virtual;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Add(aItem : TQRLibraryItemClass; AName, ADescription, AVendor, AData : string);
    property EntryList : TStrings read Entries write Entries;
    property Entry[Index : integer] : TQRLibraryEntry read GetEntry;
  end;

  {CV TQREvaluator - Lo requiere desde TQrFunction}
  TQREvaluator = class;

  { TQRFunctionLibrary }
  TQRFunctionLibrary = class(TQRLibrary)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
    //CV
    function GetFunction(Name : string; oEvaluador : TQrEvaluator) : TQREvElement;
    procedure RegisterFunction( FunctionClass: TQRLibraryItemClass; const Name: String; const Description: string = '' );
    procedure RegisterFunctionDP( FunctionClass: TQRLibraryItemClass; const Name: String; const Description: string = '' );
  end;


  { TQREvaluator related declarations }
  { EZM: opDentro, opNegar }
  TQREvOperator = (opLess, opLessOrEqual, opGreater, opGreaterOrEqual, opEqual,
                   opUnequal, opPlus, opMinus, opOr, opMul, opDiv, opAnd, opDentro, opNegar, opModulo );

  TQREvResultClass = class
  public
    EvResult : TQREvResult;
    { EZM }
    Element : TQREvElement;
  end;

  TQRFiFo = class
  private
    FAggreg : boolean;
    FiFo : TList;
    FNextItem : integer;
    function GetFiFoCount : Integer;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Put(Value : TObject);
    procedure Start;
    function Get : TObject;
    function GetAndFree : TObject;
    procedure Delete( nPos : Integer );
    property Aggreg : boolean read FAggreg write FAggreg;
    property NextItem : Integer read FNextItem write FNextItem;
    property Count : Integer read GetFiFoCount;
  end;

  TQREvElement = class
  private
    FIsAggreg : boolean;
  public
    constructor Create(oEvaluador:TQrEvaluator=NIL); virtual;
    function Value(FiFo : TQRFiFo) : TQREvResult; virtual;
    procedure Reset; virtual;
    property IsAggreg : boolean read FIsAggreg write FIsAggreg;
    { EZM }
    function GetExpresion(FiFo : TQRFifo ) : String; virtual;
  end;

  { TQREvElementOperator }
  TQREvElementOperator = class(TQREvElement)
  private
    FOpCode :  TQREvOperator;
    procedure ConverTQREvResults(var Res1 : TQREvResult; var Res2 : TQREvResult );
    { EZM }
    function ErrorTipos( Texto : TQRErrorString ) : TQREvResult;
  public
    constructor CreateOperator(OpCode : TQREvOperator);
    function Value(FiFo : TQRFiFo) : TQREvResult; override;
    { EZM }
    function GetExpresion(FiFo : TQRFifo ) : String; override;
  end;

  TQREvElementFunction = class(TQREvElement)
  private
  protected
    ArgList : TList;
    { EZM }
    Nombre : String[10];
    FEvaluador : TQREvaluator;
    FExpresion : String;
    FEsFecha : Boolean;
    FRegresaConstante : Boolean;
    {$IFDEF MSSQL}
    FCalculando : Boolean;
    {$ENDIF}

    function ArgumentOK(Value : TQREvElement) : boolean;
    function Argument(Index : integer) : TQREvResult;
    procedure FreeArguments;
    procedure GetArguments(FiFo : TQRFiFo);
    function  GetArgumentsConstantes(FiFo : TQRFiFo) : Boolean;
    procedure Aggregate; virtual;
    function Calculate : TQREvResult; virtual;
    procedure ParametrosFijos( const Numero : Integer );
    {$IFDEF MSSQL}
    property  Calculando : Boolean read FCalculando write FCalculando;
    {$ENDIF}
  public
    //cv
    constructor Create(oEvaluador : TQREvaluator); override;
    destructor Destroy; override;
    function Value(FiFo : TQRFiFo) : TQREvResult; override;
    { EZM }
    function ValueConstante(FiFo : TQRFiFo) : TQREvResult;
    // function GetRequeridos: String; virtual;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); virtual;
    property Evaluador : TQREvaluator read FEvaluador write FEvaluador;
    function ArgumentElement(const Index : integer) : TQREvElement;
    function ParamEsCampo( const iParam : Word ) : Boolean;
    function ParamNombreCampo( const iParam : Word ) : String;
    property Expresion : String read FExpresion write FExpresion;
    function ParametrosConstantes : Boolean;
    property EsFecha : Boolean read FEsFecha write FEsFecha;
    property RegresaConstante : Boolean read FRegresaConstante write FRegresaConstante;
  end;

  TQREvElementFunctionClass = class of TQREvElementFunction;

  TQREvElementArgumentEnd = class(TQREvElement);

  TQREvElementDataField = class(TQREvElement)
  private
    FDataSet : TZetaCursor;
    FFieldNo : integer;
    FField : TZetaField;
  public
    constructor CreateField(aField : TZetaField); virtual;
    function Value(FiFo : TQRFiFo) : TQREvResult; override;
  end;

  {$warnings off}
  TQREvElementError = class(TQREvElement)
  private
    FErrorMessage : string;
  public
    constructor Create(ErrorMessage : string);
    function Value(FiFo : TQRFiFo) : TQREvResult; override;
  end;
  {$warnings on}

  { EZM
  TQREvElementError = class(TQREvElement)
  public
    function Value(FiFo : TQRFiFo) : TQREvResult; override;
  end;
  }

  { TQREvaluator class }

  TQREvaluator = class (TObject)
  private
    FLibrary: TQRFunctionLibrary;
    FDataSets : TList;
    FiFo : TQRFiFo;
    FPrepared : boolean;
    function  EvalFunctionExpr(const strFunc : string) : TQREvResult;
    function  EvalSimpleExpr(const strSimplExpr : string) : TQREvResult;
    { EZM }
    function  EvalOrExpr(const strSimplExpr : string) : TQREvResult;
    { EZM }
    function  EvalAndExpr(const strSimplExpr : string) : TQREvResult;
    function  EvalTerm(const strTermExpr : string) : TQREvResult;
    function  EvalFactor(strFactorExpr : string) : TQREvResult;
    function  EvalString(const strString : string) : TQREvResult;
    function  EvalConstant(const strConstant : string) : TQREvResult;
    function  GetAggregate : boolean;
    { EZM }
    procedure ResuelveComillas( c : Char; var cLastComilla : Char; var booString : Boolean );
    // function  NegateResult(const Res : TQREvResult) : TQREvResult;
    function  Evaluate(const strExpr : string) : TQREvResult;
    procedure FindDelimiter(strArg : string; var Pos : integer);
    procedure FindOp1(const strExpr : string; var Op : TQREvOperator; var Pos, Len : integer);
    procedure FindOp2(const strExpr : string; var Op : TQREvOperator; var Pos, Len : integer);
    procedure FindOp3(const strExpr : string; var Op : TQREvOperator; var Pos, Len : integer);
    { EZM }
    procedure FindOpLogico(const sOperador, strExpr : string; var Op : TQREvOperator; var Pos, Len : integer);
    procedure SetAggregate(Value : boolean);
    procedure TrimString(var strString : string);
  protected
    { EZM}
    FBuscandoRequeridos : Boolean;
    LastFunction : TQREvElement;
    property FunctionLibrary: TQRFunctionLibrary read FLibrary write FLibrary;
    function EvalFunction(strFunc : string; const strArg : string ) : TQREvResult; virtual;
    function EvalVariable(strVariable : string) : TQREvResult; virtual;
    function GetIsAggreg : boolean;
  public
    constructor Create;
    destructor Destroy; override;
    function Calculate(const StrExpr : string) : TQREvResult;
    function Value : TQREvResult;
    procedure Prepare(const StrExpr : string);
    procedure Reset;
    procedure UnPrepare;
    property IsAggreg : boolean read GetIsAggreg;
    property Aggregate : boolean read GetAggregate write SetAggregate;
    property DataSets : TList read FDataSets write FDataSets;
    property Prepared : boolean read FPrepared write FPrepared;
    { EZM }
    property  BuscandoRequeridos : boolean read FBuscandoRequeridos write FBuscandoRequeridos;
    procedure CreaCampo( Tipo : TQREvResultType; const Exp : String; const lEsFecha : Boolean );
    procedure IncluyeCampos( strCampos : string ); virtual;
    function  SQLValido( const Exp : String; var sSQL : String ) : Boolean;
    function  EsConstante : Boolean;
  end;

  { EZM }
  function ErrorEvaluador( const Numero : TQRErrorEvaluador; const Msg : TQRErrorString; const Exp : String ) : String;
  function ErrorCreate(Value : string) : TQREvResult;
  function HuboErrorEvaluador( const Res : TQREvResult; var Numero : Integer; var Descrip, Msg, Exp : String ) : Boolean;
  function DesarmaErrorEvaluador( sError : String ) : RecErrorEvaluador;


const
   { EZM }
   cQROpDescrip : array[opLess..opModulo] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} =
        ( '<','<=','>','>=', '=', '<>', '+', '-', 'OR', '*', '/', 'AND', '$', 'NOT', '%' );

implementation

const
     ArgSeparator = ',';

function Dentro( sSearch, sTarget : String ) : Boolean;

function NextToken( var sTarget: String ): String;
var
   i: Integer;
begin
     i := Pos( ',', sTarget );
     if ( i <> 0 ) then
     begin
          Result := System.Copy( sTarget, 1, ( i - 1 ) );
          System.Delete( sTarget, 1, i );
     end
     else
     begin
          Result := sTarget;
          sTarget := '';
     end;
end;

begin
     Result := FALSE;
     while not Result and ( Length( Trim( sTarget )) > 0 )  do
           Result := ( sSearch = NextToken( sTarget ));
end;

{ TQRLibrary }

constructor TQRLibrary.Create;
begin
  inherited Create;
  Entries := TStringList.Create;
end;

destructor TQRLibrary.Destroy;
var
  I : integer;
begin
  for I := 0 to Entries.Count - 1 do
    Entries.Objects[I].Free;
  Entries.Free;
  inherited Destroy;
end;

procedure TQRLibrary.Add(aItem : TQRLibraryItemClass; AName, ADescription, AVendor, AData : string);
var
  aLibraryEntry : TQRLibraryEntry;
begin
  aLibraryEntry := TQRLibraryEntry.Create;
  with aLibraryEntry do
  begin
    Name := AName;
    Description := ADescription;
    Vendor := AVendor;
    Data := AData;
    Item := aItem;
  end;
  Entries.AddObject(aName,aLibraryEntry);
end;

function TQRLibrary.GetEntry(Index : integer) : TQRLibraryEntry;
begin
  if Index <= Entries.Count then
    result := TQRLibraryEntry(Entries.Objects[Index])
  else
    result := nil;
end;

{ TQRFunctionLibrary }

function TQRFunctionLibrary.GetFunction(Name : string;oEvaluador : TQrEvaluator) : TQREvElement;
var
  I : integer;
  AObject : TQREvElementFunctionClass;
  aLibraryEntry : TQRLibraryEntry;
begin
  {EZM}
  if Length( Name ) > 10 then
     Name := Copy(Name, 1, 10 );

  I := Entries.IndexOf(Name);
  if I>=0 then
  begin
    aLibraryEntry := TQRLibraryEntry(Entry[I]);
    aObject := TQREvElementFunctionClass(aLibraryEntry.Item);
    result := aObject.Create(oEvaluador);
    {EZM}
    TQREvElementFunction( Result ).Nombre := Name;
  end else
    result := TQREvElementError.Create( ErrorEvaluador( eeFuncionNoExiste, '', Name ));

end;

procedure TQRFunctionLibrary.RegisterFunction( FunctionClass: TQRLibraryItemClass; const Name: String; const Description: string = '' );
begin
     Add( FunctionClass, Copy( Name, 1, 10 ), Description, '', '' );
end;

procedure TQRFunctionLibrary.RegisterFunctionDP( FunctionClass: TQRLibraryItemClass; const Name : String; const Description: string = '' );
var
   iPosicion: Integer;
begin
     with EntryList do
     begin
          iPosicion := IndexOf( Name );
          if ( iPosicion >= 0 ) then
             Delete( iPosicion );
     end;
     RegisterFunction( FunctionClass, Name );
end;

{ TQREvaluator }

constructor TQRFiFo.Create;
begin
  FiFo := TList.Create;
  FAggreg := false;
  FNextItem := 0;
end;

destructor TQRFiFo.Destroy;
var
  I : integer;
begin
  for I := 0 to FiFo.Count-1 do
    TObject(FiFo[I]).Free;
  FiFo.Free;
  inherited Destroy;
end;

procedure TQRFiFo.Start;
begin
  FNextItem := 0;
end;

procedure TQRFiFo.Put(Value : TObject);
begin
  FiFo.Add(Value);
end;

function TQRFiFo.GetAndFree : TObject;
begin
  if FiFo.Count>0 then
  begin
    result := FiFo[0];
    FiFo.Delete(0);
  end else
    result := nil;
end;

function TQRFiFo.Get : TObject;
begin
  if FNextItem<FiFo.Count then
  begin
    result := FiFo[FNextItem];
    inc(FNextItem);
  end else
    result := nil;
end;

function TQRFiFo.GetFiFoCount : Integer;
begin
     Result := FiFo.Count;
end;

procedure TQRFifo.Delete( nPos : Integer );
begin
     FiFo.Delete( nPos );
end;

{ ******************* EZM ******************** }
function ErrorCreate(Value : string) : TQREvResult;
begin
  Result.Kind := resError;
  Result.strResult := Value;
end;

function ErrorEvaluador( const Numero : TQRErrorEvaluador; const Msg : TQRErrorString; const Exp : String ) : String;
begin
     Result := '[' + IntToStr( Ord( Numero )) + ']' + Msg + '|' + Exp;
end;

function DesarmaErrorEvaluador( sError : String ) : RecErrorEvaluador;
var
    nPos : Integer;
begin
    with Result do
    begin
        if ( Copy( sError, 1, 1) = '[' ) then
        begin
             // Obtiene el N�mero
             nPos := Pos( ']', sError );
             ErrTipo := TQRErrorEvaluador( StrToInt( Copy( sError, 2, nPos-2 )));
             sError := Copy( sError, nPos+1, MAXINT );
             // Obtiene Msg
             nPos := Pos( '|', sError );
             ErrMsg := Copy( sError, 1, nPos-1 );
             // Obtiene Exp
             ErrExp := Copy( sError, nPos+1, MAXINT );
        end
        else
        begin
             ErrTipo := eeDesconocido;
             ErrMsg := sError;
             ErrExp := '';
        end;
        ErrDescrip := cQRErrorDescrip[ErrTipo];
        ErrCompleto := 'Error N�mero: ' + IntToStr( Ord( ErrTipo )) + CR_LF +
                            ErrDescrip + CR_LF + ErrMsg + CR_LF + ErrExp;
    end;
end;

function HuboErrorEvaluador( const Res : TQREvResult; var Numero : Integer; var Descrip, Msg, Exp : String ) : Boolean;
begin
     Result := ( Res.Kind = resError );
     if ( Result ) then
       with DesarmaErrorEvaluador( Res.strResult ) do
       begin
         Numero  := Ord( ErrTipo );
         Descrip := ErrDescrip;
         Msg     := ErrMsg;
         Exp     := ErrExp;
       end;
end;
{ ******************* FIN DE EZM **************}


{ TQREvElement }

constructor TQREvElement.Create(oEvaluador:TQrEvaluator);
begin
  inherited Create;
  FIsAggreg := false;
end;

function TQREvElement.Value(FiFo : TQRFiFo) : TQREvResult;
begin
end;

procedure TQREvElement.Reset;
begin
end;


constructor TQREvElementOperator.CreateOperator(OpCode : TQREvOperator);
begin
  inherited Create;
  FOpCode := OpCode;
end;


{ EZM }
function TQREvElementOperator.ErrorTipos( Texto : TQRErrorString ) : TQREvResult;
begin
     Result := ErrorCreate( ErrorEvaluador( eeTipos, Texto, 'Operador ' + cQROpDescrip[ FOpCode ] ));
end;

procedure TQREvElementOperator.ConverTQREvResults(var Res1 : TQREvResult; var Res2 : TQREvResult);
begin
 if (Res1.Kind <> resError) and (Res2.Kind <> resError) then
  if Res1.Kind <> Res2.Kind then
  begin
    if ((Res1.Kind = resInt) and (Res2.Kind = resDouble) and (not Res2.resFecha)) then
    begin
      Res1.Kind := resDouble;
      Res1.dblResult := Res1.intResult;
    end else
      if ((Res1.Kind = resDouble) and (Res2.Kind = resInt)) then
      begin
        Res2.Kind := resDouble;
        Res2.dblResult := Res2.intResult;
      end else
        if ( FOpCode = opDentro ) and ( Res1.Kind = resInt ) and ( Res2.Kind = resString ) then
        begin
          Res1.Kind := resString;
          Res1.strResult := IntToStr( Res1.intResult );
        end else
          { EZM }
          if (( FOpCode = opNegar ) and ( Res1.Kind = resInt ) and ( Res2.Kind = resBool )) then
          begin
            Res1.Kind := resBool;
          end else
          begin
            { EZM }
            Res1 := ErrorTipos( 'Tipos Diferentes' );
          end;
  end;
end;

function TQREvElementOperator.Value(FiFo : TQRFiFo) : TQREvResult;
var
  Res1,
  Res2 : TQREvResult;

  procedure ComparaBooleanos;
  const
    K_BOOLEANOS = [ 's', 'n' ];
  begin
       // Para comparar campos 'Booleanos', ignorando min�sculas
       // Ej. CB_CHECA = 's' es igual que CB_CHECA = 'S'
       if ( Length( Res1.strResult ) = 1 ) and ( Length( Res2.strResult ) = 1 ) and
          (( Res1.strResult[ 1 ] in K_BOOLEANOS ) or ( Res2.strResult[ 1 ] in K_BOOLEANOS )) then
       begin
            Res1.strResult := UpperCase( Res1.strResult );
            Res2.strResult := UpperCase( Res2.strResult );
       end;
  end;

begin { Value }
  Res1 := TQREvElement(FiFo.Get).Value(FiFo);
  Res2 := TQREvElement(FiFo.Get).Value(FiFo);
  ConverTQREvResults(Res1, Res2);
  result.Kind := Res1.Kind;
  { EZM }
  Result.resFecha := FALSE;
  if Res2.Kind = resError then
    Result.Kind := res2.Kind;
  if result.Kind <> resError then
  begin
    case FOpCode of
      opPlus: case result.Kind of
                resInt:    result.intResult := Res1.intResult + Res2.intResult;
                resDouble: begin
                             if ( Res1.resFecha ) then
                                   Result.resFecha := TRUE;
                             result.dblResult := Res1.dblResult + Res2.dblResult;
                           end;
                resString: result.strResult := Res1.strResult + Res2.strResult;
                { EZM }
                // resBool:   result.Kind := resError;
                resBool : Result := ErrorTipos( 'No pueden ser L�gicos' );
              end;
      opMinus:  case result.Kind of
                  resInt:    result.intResult := Res1.intResult - Res2.intResult;
                  resDouble: begin
                                  // La resta de 2 fechas es un Entero
                                  if ( Res1.resFecha ) then
                                  begin
                                    if ( Res2.resFecha ) then
                                    begin
                                       Result.Kind := resInt;
                                       Result.intResult := Trunc( Res1.dblResult - Res2.dblResult );
                                    end
                                    else
                                    begin
                                       // Fecha - Dias es una Fecha
                                       Result.resFecha := TRUE;
                                       Result.dblResult := Res1.dblResult - Res2.dblResult;
                                    end;
                                  end
                                  else
                                      result.dblResult := Res1.dblResult - Res2.dblResult;
                             end;
                  { EZM }
                  // resString: result.Kind := resError;
                  // resBool:   result.Kind := resError;
                  resString : Result := ErrorTipos( 'No pueden ser Textos');
                  resBool : Result := ErrorTipos( 'No pueden ser L�gicos');
                end;
      { EZM : opNegar Permite la NEGACION con NOT }
      opNegar : if result.Kind = resBool then
                    Result.booResult := not Res2.booResult
                else
                    Result := ErrorTipos( 'No es L�gico' );
      { EZM : opDentro }
      opDentro:  if result.Kind = resString then
                 begin
                      result.Kind := resBool;
                      result.booResult := Dentro( Res1.strResult, Res2.strResult );
                 end
                 else
                     Result := ErrorTipos( 'No son caracter' );
      opMul: case result.Kind of
               resInt:    result.intResult := Res1.intResult * Res2.intResult;
               resDouble: result.dblResult := Res1.dblResult * Res2.dblResult;
               { EZM }
               resString, resBool: Result := ErrorTipos( 'No son N�meros' );
             end;
      opDiv: case result.Kind of
               resInt:    if Res2.intResult <> 0 then
                          begin
                            result.dblResult := Res1.intResult / Res2.intResult;
                            result.Kind := resDouble;
                          end else { EZM : Divisi�n etre 0 = 0 }
                          begin
                            result.dblResult := 0;
                            result.Kind := resDouble;
                          end;
               resDouble: if Res2.dblResult <> 0 then
                            result.dblResult := Res1.dblResult / Res2.dblResult
                          else
                            { EZM : Divisi�n etre 0 = 0 }
                            result.dblResult := 0;
                            // result.Kind := resError;
               { EZM }
               resString, resBool: Result := ErrorTipos( 'No son N�meros' );
             end;
      opModulo : case result.Kind of
                   resInt : if Res2.intResult <> 0 then
                                 Result.intResult := Res1.intResult mod Res2.intResult
                            else
                                 Result.intResult := 0; { Divisi�n entre 0 }
                   resDouble: Result := ErrorTipos( 'S�lo se permiten n�meros enteros' );
                   else Result := ErrorTipos( 'No son n�meros' );
                 end;
      opGreater: begin
                   result.Kind := resBool;
                   case Res1.Kind of
                     resInt:    result.booResult := Res1.intResult > Res2.intResult;
                     resDouble: result.booResult := Res1.dblResult > Res2.dblResult;
                     resString: result.booResult := Res1.strResult > Res2.strResult;
                     { EZM }
                     resBool: Result := ErrorTipos( 'No pueden ser L�gicos' );
                   end;
                 end;
      opGreaterOrEqual: begin
                result.Kind := resBool;
                case Res1.Kind of
                  resInt:    result.booResult := Res1.intResult >= Res2.intResult;
                  resDouble: result.booResult := Res1.dblResult >= Res2.dblResult;
                  resString: result.booResult := Res1.strResult >= Res2.strResult;
                  { EZM }
                  resBool: Result := ErrorTipos( 'No pueden ser L�gicos' );
                end;
             end;
      opLess: begin
                result.Kind := resBool;
                case Res1.Kind of
                  resInt:    result.booResult := Res1.intResult < Res2.intResult;
                  resDouble: result.booResult := Res1.dblResult < Res2.dblResult;
                  resString: result.booResult := Res1.strResult < Res2.strResult;
                  { EZM }
                  resBool: Result := ErrorTipos( 'No pueden ser L�gicos' );
                end;
              end;
      opLessOrEqual: begin
                 result.Kind := resBool;
                 case Res1.Kind of
                   resInt:    result.booResult := Res1.intResult <= Res2.intResult;
                   resDouble: result.booResult := Res1.dblResult <= Res2.dblResult;
                   resString: result.booResult := Res1.strResult <= Res2.strResult;
                   { EZM }
                  resBool: Result := ErrorTipos( 'No pueden ser L�gicos' );
                 end;
               end;
      opEqual: begin
                 result.Kind := resBool;
                 case Res1.Kind of
                   resInt:    result.booResult := Res1.intResult = Res2.intResult;
                   resDouble: result.booResult := Res1.dblResult = Res2.dblResult;
                   resString: begin
                                   ComparaBooleanos;
                                   result.booResult := Res1.strResult = Res2.strResult;
                              end;
                   resBool:   result.booResult := Res1.booResult = Res2.booResult;
                 end;
               end;
      opUnequal: begin
                   result.Kind := resBool;
                   case Res1.Kind of
                     resInt:    result.booResult := Res1.intResult <> Res2.intResult;
                     resDouble: result.booResult := Res1.dblResult <> Res2.dblResult;
                     resString: begin
                                     ComparaBooleanos;
                                     result.booResult := Res1.strResult <> Res2.strResult;
                                end;
                     resBool:   result.booResult := Res1.booResult <> Res2.booResult;
                   end;
                 end;
      opOr: begin
              result.Kind := resBool;
              case Res1.Kind of
                { EZM : No tiene caso }
                resInt, resDouble, resString : Result := ErrorTipos( 'No son L�gicos' );
                resBool:   result.booResult := Res1.booResult or Res2.booResult;
              end;
            end;
      opAnd: begin
               result.Kind := resBool;
               case Res1.Kind of
                 { EZM : No tiene caso }
                 resInt, resDouble, resString : Result := ErrorTipos( 'No son L�gicos' );
                 resBool:   result.booResult := Res1.booResult and Res2.booResult;
               end;
             end;
    end;
  { EZM: Aqu� es la propagaci�n de los errores }
  end else
      if Res1.Kind = resError then
        Result := Res1
      else
        Result := Res2;

end;

{ TQREvElementConstant }

type
  TQREvElementConstant = class(TQREvElement)
  private
    FValue : TQREvResult;
    { EZM }
    FExpresion : String;
  public
    constructor CreateConstant(Value : TQREvResult);
    function Value(FiFo : TQRFiFo) : TQREvResult; override;
    { EZM }
    property Expresion : String read FExpresion write FExpresion;
    function GetExpresion( FiFo : TQRFiFo ) : String; override;
  end;

constructor TQREvElementConstant.CreateConstant(Value : TQREvresult);
begin
  inherited Create;
  FValue := Value;
  { EZM }
  FExpresion := '';
end;

function TQREvElementConstant.Value(FiFo : TQRFiFo): TQREvResult;
begin
  Result := FValue;
end;

{ EZM }
function TQREvElementConstant.GetExpresion( FiFo : TQRFiFo ) : String;
begin
     if FExpresion > '' then
        Result := FExpresion
     else
         case FValue.Kind of
              resString : Result := '''' + FValue.StrResult + '''';
              resInt : Result := IntToStr(FValue.IntResult);
              resDouble : if ( FValue.resFecha ) then
                             Result := '''' + FormatDateTime('mm/dd/yyyy',FValue.DblResult) + ''''
                          else
                             Result := FloatToStr(FValue.DblResult);
              resBool : if FValue.booResult then
                   Result := 'TRUE'
                 else
                   Result := 'FALSE';
         end;
end;

{ TQREvElementString }

type
  TQREvElementString = class(TQREvElement)
  private
    FValue : string;
  public
    constructor CreateString(Value : string);
    function Value(FiFo : TQRFiFo) : TQREvResult; override;
    { EZM }
    function GetExpresion(FiFo : TQRFifo ) : String; override;
  end;

constructor TQREvElementString.CreateString(Value : string);
begin
  inherited Create;
  FValue := Value;
end;

function TQREvElementString.Value(FiFo : TQRFiFo) : TQREvResult;
begin
  {EZM}
  result.resFecha := FALSE;
  result.Kind := resString;
  result.StrResult := FValue;
end;

function TQREvElementString.GetExpresion(FiFo : TQRFifo ) : String;
begin
     Result := '''' + FValue + '''';
end;


{ TQREvElementFunction }

constructor TQREvElementFunction.Create(oEvaluador : TQREvaluator);
begin
  inherited Create;
  Evaluador := oEvaluador;
  ArgList := TList.Create;
  { EZM }
  FEsFecha := FALSE;
  FRegresaConstante := FALSE;
  {$IFDEF MSSQL}
  Calculando := TRUE;
  {$ENDIF}
end;

destructor TQREvElementFunction.Destroy;
begin
  ArgList.Free;
  inherited Destroy;
end;

procedure TQREvElementFunction.GetArguments(FiFo : TQRFiFo);
var
  aArgument : TQREvElement;
  AResult : TQREvResultClass;
begin
  repeat
    aArgument := TQREvElement(FiFo.Get);
    if not (aArgument is TQREvElementArgumentEnd) then
    begin
      aResult := TQREvResultClass.Create;
      aResult.EvResult := aArgument.Value(FiFo);
      { EZM }
      aResult.Element := aArgument;
      ArgList.Add(aResult);
    end;
  until aArgument is TQREvElementArgumentEnd;
end;

procedure TQREvElementFunction.FreeArguments;
var
  I : integer;
begin
  for I := 0 to ArgList.Count - 1 do
    TQREvElement(ArgList.Items[I]).Free;
  ArgList.Clear;
end;

function TQREvElementFunction.Argument(Index : integer): TQREvResult;
begin
  if Index <= ArgList.Count then
    Result := TQREvResultClass(ArgList[Index]).EvResult;
end;

function TQREvElementFunction.ArgumentElement(const Index : integer): TQREvElement;
begin
    Result := TQREvResultClass(ArgList[Index]).Element;
end;

function TQREvElementFunction.Value(FiFo : TQRFiFo) : TQREvResult;
{ EZM }
var
   i : Integer;
   TipoRegresa : TQREvResultType;
begin
  GetArguments(FiFo);
  { EZM }
  for i := 0 to ArgList.Count-1 do
  if ( Argument(i).Kind = resError ) and ( Argument(i).strResult <> 'NIL' ) then
  begin
       Result := Argument(i);
       FreeArguments;
       Exit;
  end;
  try
     if FiFo.Aggreg then
        Aggregate;

     if Evaluador.BuscandoRequeridos then
     begin
          TipoRegresa := resError;
          {$IFDEF MSSQL}
          Calculando := FALSE;
          {$ENDIF}
          GetRequeridos( TipoRegresa );
          {$IFDEF MSSQL}
          Calculando := TRUE;
          {$ENDIF}
          // Puede ser que GetRequeridos determine el Tipo de Regresa
          // En ese caso, ya no es necesario llamar a Calculate
          if ( TipoRegresa <> resError ) then
          begin
             Result.Kind := TipoRegresa;
             // Evita 'basura' en numericos y string
             case ( TipoRegresa ) of
                  resInt        : Result.intResult := 1;
                  resDouble     : Result.dblResult := 1;
                  resString     : Result.strResult := '';
             end;
          end
          else
              Result := Calculate;
     end
     else
         Result := Calculate;
     { EZM }
     Result.resFecha := EsFecha;
  except
     on E : EErrorEvaluador do
     begin
        Result.Kind := resError;
        Result.strResult := E.Message;
     end;
     on E : Exception do
     begin
        Result.Kind := resError;
        Result.strResult := ErrorEvaluador( eeFuncion, E.Message, 'Funci�n:' + Nombre );
     end;
  end;
  FreeArguments;
end;

function TQREvElementFunction.GetArgumentsConstantes(FiFo : TQRFiFo) : Boolean;
var
  aArgument : TQREvElement;
  AResult : TQREvResultClass;
begin
  Result := TRUE;
  repeat
    aArgument := TQREvElement(FiFo.Get);
    if not (aArgument is TQREvElementArgumentEnd) then
    begin
      // Si el Par�metro es un CAMPO, aunque sea 'TQREvElementConstant'
      // no se debe considerar para que las funciones 'RegresenConsante'
      // Nos dabmos cuenta que es un campo por 'Expresion' no vac�a
      if not ((( aArgument is TQREvElementConstant ) and ( Length( TQREvElementConstant( aArgument ).Expresion ) = 0 )) or
              ( aArgument is TQREvElementString )) then
      begin
           Result := FALSE;
           Exit;
      end;

      aResult := TQREvResultClass.Create;
      aResult.EvResult := aArgument.Value(FiFo);
      { EZM }
      aResult.Element := aArgument;
      ArgList.Add(aResult);
    end;
  until aArgument is TQREvElementArgumentEnd;
end;


function TQREvElementFunction.ValueConstante(FiFo : TQRFiFo) : TQREvResult;
begin
  if GetArgumentsConstantes(FiFo) then
  begin
      try
         Result := Calculate;
         Result.resFecha := EsFecha;
      except
         Result.Kind := resError;
      end;
  end
  else
      Result.Kind := resError;
  FreeArguments;
end;


{EZM}
procedure TQREvElementFunction.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
end;


function TQREvElementFunction.ArgumentOK(Value : TQREvElement) : boolean;
begin
  Result := not (Value is TQREvElementArgumentEnd) and not (Value is TQREvElementError);
end;

procedure TQREvElementFunction.Aggregate;
begin
end;

function TQREvElementFunction.Calculate : TQREvResult;
begin
  Result := ErrorCreate( ErrorEvaluador( eeFuncionPendiente, '', Nombre ));
end;

procedure TQREvElementFunction.ParametrosFijos( const Numero : Integer );
begin
     if ( ArgList.Count < Numero ) then
        raise EErrorEvaluador.Create( ErrorEvaluador( eeFuncionParam, 'Faltan Par�metros Fijos:' + IntToStr( Numero - ArgList.Count ),
            'Funci�n: ' + Nombre + '()' ));
end;


{ TQREvSumFunction }

type
  TQREvSumFunction = class(TQREvElementFunction)
  private
    SumResult : TQREvResult;
    ResAssigned : boolean;
  public
    constructor Create(oEvaluador:TQrEvaluator); override;
    procedure Aggregate; override;
    function Calculate : TQREvResult; override;
    procedure Reset; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

constructor TQREvSumFunction.Create(oEvaluador:TQrEvaluator);
begin
  inherited Create(oEvaluador);
  ResAssigned := false;
  IsAggreg := true;
end;

procedure TQREvSumFunction.Reset;
begin
  ResAssigned := false;
end;

procedure TQREvSumFunction.Aggregate;
var
  aValue : TQREvResult;
begin
  if ArgList.Count = 1 then
  begin
    aValue := Argument(0);
    if ResAssigned then
    begin
      case aValue.Kind of
        resInt    : SumResult.dblResult := SumResult.dblResult + aValue.IntResult;
        resDouble : SumResult.dblResult := SumResult.dblResult + aValue.dblResult;
        else
            SumResult.Kind := resError
      end;
    end else
    begin
      SumResult.Kind := resDouble;
      case aValue.Kind of
        resInt    : SumResult.dblResult := aValue.IntResult;
        resDouble : SumResult.dblResult := aValue.dblResult;
      else
        SumResult.Kind := resError;
      end;
    end;
    ResAssigned := true;
    if ( SumResult.Kind = resError ) then
       SumResult.strResult := ErrorEvaluador( eeTipos, 'S�lo se permiten N�meros', 'Funci�n SUM()' );
  end else
    SumResult := ErrorCreate( ErrorEvaluador( eeFuncionParam, 'Falta par�metro Num�rico', 'Funci�n SUM()' ));
end;

function TQREvSumFunction.Calculate : TQREvResult;
begin
  if ResAssigned then
    Result := SumResult
  else
    Result := ErrorCreate( ErrorEvaluador( eeFuncion, 'No ha sido inicializado', 'Funci�n SUM()' ));
end;

procedure TQREvSumFunction.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos( 1 );
     TipoRegresa := resDouble;
end;


{ TQREvAverageFunction }

type
  TQREvAverageFunction = class(TQREvSumFunction)
  private
    Count : longint;
    aResult : TQREvResult;
  public
    function Calculate : TQREvResult; override;
    procedure Aggregate; override;
    procedure Reset; override;
  end;

procedure TQREvAverageFunction.Reset;
begin
  inherited Reset;
  aResult := ErrorCreate( ErrorEvaluador( eeFuncion, 'No hay registros', 'Funci�n AVERAGE()' ));
  Count := 0;
  IsAggreg := true;
end;

procedure TQREvAverageFunction.Aggregate;
var
  aValue : TQREvResult;
begin
  inherited Aggregate;
  inc(Count);
  aValue := inherited Calculate;
  aResult.Kind := resDouble;
  case aValue.Kind of
    ResInt : aResult.DblResult := aValue.IntResult / Count;
    ResDouble : aResult.DblResult := aValue.DblResult / Count;
  else
    aResult := aValue;            // Recibe el error
  end;
end;

function TQREvAverageFunction.Calculate : TQREvResult;
begin
  if ResAssigned then
    Result := aResult
  else
    Result := ErrorCreate( ErrorEvaluador( eeFuncion, 'No ha sido inicializado', 'Funci�n AVERAGE()' ));
end;

{ TQREvMaxFunction }

type
  TQREvMaxFunction = class(TQREvElementFunction)
  private
    MaxResult : TQREvResult;
    ResAssigned : boolean;
  public
    constructor Create(oEvaluador:TQrEvaluator); override;
    function Calculate : TQREvResult; override;
    procedure Aggregate; override;
    procedure Reset; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

constructor TQREvMaxFunction.Create(oEvaluador:TQrEvaluator);
begin
  inherited Create(oEvaluador);
  ResAssigned := false;
  IsAggreg := true;
end;

procedure TQREvMaxFunction.Reset;
begin
  ResAssigned := false;
end;

procedure TQREvMaxFunction.Aggregate;
var
  aValue : TQREvResult;
begin
  if ArgList.Count = 1 then
  begin
    aValue := Argument(0);
    if ResAssigned then
    begin
      case aValue.Kind of
        resInt : if aValue.IntResult > MaxResult.dblResult then
                   MaxResult.dblResult := aValue.IntResult;
        resDouble : if aValue.DblResult > MaxResult.DblResult then
                      MaxResult.DblResult := aValue.DblResult;
        resString : if aValue.StrResult > MaxResult.StrResult then
                      MaxResult.StrResult := aValue.StrResult;
        resBool : if aValue.booResult > MaxResult.BooResult then
                    MaxResult.BooResult := aValue.BooResult;
      else
        MaxResult := aValue;            // Pasa el Error
      end
    end else
    begin
      // Obliga a DOUBLE
      if ( aValue.Kind = resInt ) then
      begin
           MaxResult.Kind := resDouble;
           MaxResult.dblResult := aValue.intResult;
      end
      else
          MaxResult := aValue;
      ResAssigned := true;
    end
  end else
    MaxResult.Kind := resError;
end;

function TQREvMaxFunction.Calculate : TQREvResult;
begin
  if ResAssigned then
    Result := MaxResult
  else
    Result := ErrorCreate( ErrorEvaluador( eeFuncion, 'No ha sido inicializado', 'Funci�n MAXIMO()' ));

end;

procedure TQREvMaxFunction.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos( 1 );
     TipoRegresa := Argument(0).Kind;
end;

{ TQREvMinFunction }

type
  TQREvMinFunction = class(TQREvElementFunction)
  private
    MinResult : TQREvResult;
    ResAssigned : boolean;
  public
    constructor Create(oEvaluador:TQrEvaluator); override;
    function Calculate : TQREvResult; override;
    procedure Aggregate; override;
    procedure Reset; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

constructor TQREvMinFunction.Create(oEvaluador:TQrEvaluator);
begin
  inherited Create(oEvaluador);
  ResAssigned := false;
  IsAggreg := true;
end;

procedure TQREvMinFunction.Reset;
begin
  ResAssigned := false;
end;

procedure TQREvMinFunction.Aggregate;
var
  aValue : TQREvResult;
begin
  if ArgList.Count = 1 then
  begin
    aValue := Argument(0);
    if ResAssigned then
    begin
      case aValue.Kind of
        resInt : if aValue.IntResult < MinResult.dblResult then
                   MinResult.dblResult := aValue.IntResult;
        resDouble : if aValue.DblResult < MinResult.DblResult then
                      MinResult.DblResult := aValue.DblResult;
        resString : if aValue.StrResult < MinResult.StrResult then
                      MinResult.StrResult := aValue.StrResult;
        resBool : if aValue.booResult > MinResult.BooResult then
                    MinResult.BooResult := aValue.BooResult;
      else
        MinResult := aValue; // Pasa El error
      end
    end else
    begin
      ResAssigned := true;
      // Obliga a DOUBLE
      if ( aValue.Kind = resInt ) then
      begin
           MinResult.Kind := resDouble;
           MinResult.dblResult := aValue.intResult;
      end
      else
          MinResult := aValue;
    end
  end else
    MinResult.Kind := resError;
end;

function TQREvMinFunction.Calculate : TQREvResult;
begin
  if ResAssigned then
    Result := MinResult
  else
    Result := ErrorCreate( ErrorEvaluador( eeFuncion, 'No ha sido inicializado', 'Funci�n MINIMO()' ));
end;

procedure TQREvMinFunction.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos( 1 );
     TipoRegresa := Argument(0).Kind;
end;

{ TQREvCountFunction }

type
  TQREvCountFunction = class(TQREvElementFunction)
  private
    FCount : integer;
  public
    constructor Create(oEvaluador:TQrEvaluator); override;
    function Value(FiFo : TQRFiFo) : TQREvResult; override;
    procedure Reset; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

constructor TQREvCountFunction.Create(oEvaluador:TQrEvaluator);
begin
  inherited Create(oEvaluador);
  FCount := 0;
  IsAggreg := true;
end;

function TQREvCountFunction.Value(FiFo : TQRFiFo) : TQREvResult;
begin
  GetArguments(FiFo);
  { EZM }
  Result.resFecha := FALSE;
    if FiFo.Aggreg then
      inc(FCount);
    Result.Kind := resInt;
    Result.intResult := FCount;
  FreeArguments;
end;

procedure TQREvCountFunction.Reset;
begin
  FCount := 0;
end;

procedure TQREvCountFunction.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resInt;
end;

{ TQREvElementDataField }

constructor TQREvElementDataField.CreateField(aField : TZetaField);
begin
  inherited Create;
  // FDataSet := aField.DataSet;
  FDataSet := CursorDelCampo( aField );
  FFieldNo := aField.Index;
  FField := aField;
end;

function TQREvElementDataField.Value(FiFo : TQRFiFo) : TQREvResult;
begin
  // IBO if FDataSet.DefaultFields then
    FField := FDataSet.Fields[FFieldNo];
  { EZM }
  Result.resFecha := FALSE;
  case TipoCampo( FField ) of
    tgTexto :
        begin
            result.Kind := resString;
            { EZM: Puse el Trim para CHAR <> VARCHAR en ADO }
            result.strResult := Trim( FField.AsString );
        end;
    tgNumero :
        begin
          result.Kind := resInt;
          result.intResult := FField.AsInteger;
        end;
    tgFloat :
        begin
          result.Kind := resDouble;
          result.dblResult := FField.AsFloat;
        end;
    tgBooleano :
        begin
          result.Kind := resBool;
          result.BooResult := FField.AsBoolean;
        end;
    tgFecha :
          begin
            result.resFecha := TRUE;
            result.Kind := resDouble;
            result.dblResult := Trunc( FField.AsDateTime );
            if result.dblResult < 0 then    // IBO Regresa fechas < 0
                result.dblResult := 0;
          end;
    tgMemo :
          begin
            result.resFecha := FALSE;
            result.Kind := resString;
            result.strResult := TMemoField(FField).AsString;
          end
    else
            begin
              { EZM }
              result := ErrorCreate( ErrorEvaluador( eeTipos, 'Tipo de Campo No Existe', FField.FieldName ));
            end;
  end;
end;

constructor TQREvElementError.Create(ErrorMessage : string);
begin
  FErrorMessage := ErrorMessage;
end;

function TQREvElementError.Value(FiFo : TQRFiFo) : TQREvResult;
begin
  { EZM }
  Result.resFecha := FALSE;
  Result.Kind := resError;
  Result.strResult := FErrorMessage;
end;


function flip(aString : string; a, b : char) : string;
var
  ParLevel : integer;
  isString : boolean;
  I : integer;
  aChar : string;
begin
  ParLevel := 0;
  isString := False;
  I := 1;
  while I <= Length(aString) do
  begin
    aChar := copy(aString, I , 1);
    if aChar = ''''then
      isString := not IsString
    else
      if not isString then
      begin
        if aChar = '(' then
          inc(ParLevel)
        else
          if aChar = ')' then
            dec(parLevel)
          else
            if ParLevel = 0 then
              if aChar = a then
                aString[I] := b
              else
                if aChar = b then
                  aString[I] := a;
     end;
     inc(i);
   end;
   result := aString;
end;

{ TQREvaluator }
constructor TQREvaluator.Create;
begin
  Prepared := false;
  { EZM }
  FBuscandoRequeridos := FALSE;
end;

destructor TQREvaluator.Destroy;
begin
  if Prepared then Unprepare;
  inherited Destroy;
end;

{************** EZM *********************}
function TQREvElementFunction.ParametrosConstantes : Boolean;
var
   i : Integer;
   Elemento : TQREvElement;
begin
     Result := TRUE;
     for i := 0 to ArgList.Count-1 do
     begin
          Elemento := ArgumentElement( i );
          if not (( Elemento is TQREvElementConstant ) or
                  ( Elemento is TQREvElementString )) then
          begin
             Result := FALSE;
             Break;
          end;
     end;
end;

function  TQREvElementFunction.ParamEsCampo( const iParam : Word ) : Boolean;
var
   Elemento : TQREvElement;
begin
     Result := FALSE;
     if ArgList.Count > iParam then
     begin
          Elemento := ArgumentElement( iParam );
          if ( Elemento is TQREvElementConstant ) then
             with Elemento as TQREvElementConstant do
                  Result := Length( FExpresion ) > 0;
     end;
end;

function  TQREvElementFunction.ParamNombreCampo( const iParam : Word ) : String;
begin
     with ArgumentElement( iParam ) as TQREvElementConstant do
          Result := FExpresion;
end;


function TQREvElement.GetExpresion( FiFo : TQRFiFo ) : String;
begin
     Result := '@@Error';
end;

{ EZM }
function TQREvElementOperator.GetExpresion(FiFo : TQRFifo ) : String;
var
   Elemento1 : TQREvElement;
   Operando1, Operando2 : String;
   Operador : String;
   oLista : TStringList;
   i : Integer;
   lEntero : Boolean;
begin
  Elemento1 := TQREvElement(FiFo.Get);
  Operando1 := Elemento1.GetExpresion(FiFo);
  lEntero   := ( Elemento1 is TQREvElementConstant ) and ( Elemento1.Value(Fifo).Kind = resInt );
  Operando2 := TQREvElement(FiFo.Get).GetExpresion(FiFo);
  if ( FOpCode = opDentro ) then
  begin
     Operador := 'IN';
     oLista := TStringList.Create;
     with oLista do
     begin
          CommaText := Copy( Operando2, 2, Length( Operando2 )-2 );
          Operando2 := '';
          for i := 0 to Count-1 do
          begin
              if i > 0 then
                 Operando2 := Operando2 + ', ';
              if ( lEntero ) then
                 Operando2 := Operando2 + Trim( Strings[i] )
              else
                  Operando2 := Operando2 + '''' + Trim( Strings[i] ) + '''';
          end;
          Operando2 := '(' + Operando2 + ')';
     end;
     oLista.Free;
  end
  else if ( FOpCode = opNegar ) then
  begin
       Operando1 := '';
       Operador := cQROpDescrip[ FOpCode ];
  end
  else
      Operador := cQROpDescrip[ FOpCode ];
  Result := '(' + Operando1 + ' ' + Operador + ' ' + Operando2 + ')';
end;


function TQREvaluator.SQLValido( const Exp : String; var sSQL : String ) : Boolean;
var
  F : TObject;
  Res : TQREvResult;
begin
  FBuscandoRequeridos := TRUE;
  Prepare(Exp);

  FiFo.Start;
  F := FiFo.Get;
  Result := FALSE;
  if F <> nil then
  begin
       Res := TQREvElement(F).Value(FiFo);
       if Res.Kind <> ResError then
       begin
            Fifo.Start;
            F := FiFo.Get;
            sSQL := TQREvElement(F).GetExpresion(FiFo);
            Result := ( Pos( '@@Error', sSQL ) = 0 );
       end;
  end;
  UnPrepare;
  FBuscandoRequeridos := FALSE;
end;

procedure TQREvaluator.IncluyeCampos( strCampos : string );
begin
end;

procedure TQREvaluator.CreaCampo( Tipo : TQREvResultType; const Exp : String; const lEsFecha : Boolean );
var
   Valor : TQREvResult;
   oConstante : TQREvElementConstant;
begin
     Valor.resFecha := lEsFecha;
     Valor.Kind := Tipo;
     case Tipo of
          resInt        : Valor.intResult := 0;
          resDouble     : Valor.dblResult := 0;
          resString     : Valor.strResult := '';
          resBool       : Valor.booResult := FALSE;
     end;

     oConstante := TQREvElementConstant.CreateConstant( Valor );
     oConstante.Expresion := Exp;
     FiFo.Put(oConstante)
end;


{ ******************** FIN EZM ************************ }

procedure TQREvaluator.TrimString(var strString : string);
var
  intStart,
  intEnd : integer;
begin
  intStart := 1;
  intEnd := Length(strString);
  while (copy(strString, intStart,1) = ' ') and (intStart < intEnd) do
    inc(intStart);
  while (copy(strString, intEnd,1) = ' ') and (intEnd > intStart) do
    dec(intEnd);
  strString := Copy(strString, intStart, intEnd - intStart + 1);
end;

procedure TQREvaluator.FindDelimiter(strArg : string; var Pos : integer);
var
  n : integer;
  FoundDelim : boolean;
  booString : boolean;
  intParenteses : integer;
  cLastComilla : Char;
begin
  if strArg='' then
    Pos := 0
  else
  begin
    FoundDelim := false;
    BooString := false;
    intParenteses := 0;
    N := 1;
    { EZM }
    cLastComilla := Chr(0);
    // while (N<length(strArg)) and not FoundDelim do
    while (N<=length(strArg)) and not FoundDelim do
    begin
      case StrArg[N] of
        '(' : if not booString then inc(intParenteses);
        ')' : if not booString then dec(intParenteses);
        { EZM }
        '''','"','[',']' : ResuelveComillas( StrArg[N], cLastComilla, booString );
        // '''' : booString := not booString;
      end;
      if (intParenteses=0) and not booString then
        if strArg[N]=ArgSeparator then
        begin
          FoundDelim := true;
          break;
        end;
      inc(N);
    end;
    if FoundDelim then
      Pos := N
    else
      Pos := 0;
  end;
end;

{EZM }
procedure TQREvaluator.ResuelveComillas( c : Char; var cLastComilla : Char; var booString : Boolean );
begin
  case c of
      '''', '"' : if ( not booString ) then
                  begin
                       booString := TRUE;
                       cLastComilla := c;
                  end
                  else
                    if ( c = cLastComilla ) then
                       booString := FALSE;
      '[' : if ( not booString ) then
            begin
                 booString := TRUE;
                 cLastComilla := c;
            end;
      ']' : if ( booString AND ( cLastComilla = '[' )) then
              booString := FALSE;
  end;
end;


procedure TQREvaluator.FindOp1(const strExpr : string; var Op : TQREvOperator; var Pos, Len : integer);
var
  n : integer;
  booFound : boolean;
  intParenteses : integer;
  aString : string; //[255];
  booString : boolean;
  { EZM }
  cLastComilla : Char;
begin
  n := 1;
  intParenteses := 0;
  booFound := false;
  Len := 1;
  aString := strExpr;
  booString := false;
  cLastComilla := Chr(0);
  while (n < Length(strExpr)) and (not booFound) do
  begin
    booFound := true;
    case aString[N] of
      '(' : if not booString then inc(intParenteses);
      ')' : if not booString then dec(intParenteses);
      { EZM }
      '''','"','[',']' : ResuelveComillas( aString[N], cLastComilla, booString );
    end;
    if (intParenteses = 0) and (n > 1) and not booString then
      case aString[N] of
        '<' : begin
                if aString[N + 1] = '>' then
                begin
                  Op := opUnequal;
                  Len := 2;
                end else
                  if aString[N + 1] = '=' then
                  begin
                    Op := opLessOrEqual;
                    Len := 2;
                  end else
                    Op := opLess;
                end;
        '>' : if aString[N + 1] = '=' then
              begin
                Op := opGreaterOrEqual;
                Len := 2;
              end else
                Op := opGreater;
        '=' : Op := opEqual;
        { EZM }
        '$' : Op := opDentro;
      else
        booFound := false;
      end
    else
      booFound := false;
    inc(N);
  end;
  if booFound then
    Pos := n - 1
  else
    Pos := -1;
end;

procedure TQREvaluator.FindOp2(const strExpr : string; var Op : TQREvOperator; var Pos, Len : integer);
var
  n : integer;
  booFound : boolean;
  intParenteses : integer;
  booString : boolean;
  aString : string;//[255];
  { EZM }
  aTempo : string[255];
  cLastComilla : Char;
begin
  n := 1;
  intParenteses := 0;
  booFound := false;
  booString := false;
  aString := strExpr;
  Len := 1;
  while (n < Length(strExpr)) and (not booFound) do
  begin
    booFound := true;
    case aString[N] of
      '(' : if not boostring then inc(intParenteses);
      ')' : if not boostring then dec(intParenteses);
      { EZM }
      '''','"','[',']' : ResuelveComillas( aString[N], cLastComilla, booString );
    end;
    if (intParenteses = 0) and (not booString) and (N > 1) then
      case aString[N] of
        '+' : Op := opPlus;
        { EZM }
        '-' : begin
                // Permite operaciones con '-1'
                aTempo := Trim( Copy( strExpr, 1, N-1 ));
                aTempo := Copy( aTempo, Length( aTempo ), 1 );
                if ( aTempo = '' ) or ( aTempo = '*' ) or ( aTempo = '/' ) then
                begin
                   booFound := FALSE;
                   Break;
                end
                else
                    Op := opMinus;
               end;
        { EZM }
        '!' : Op := opNegar;
{ EZM
        ' ' : if (AnsiLowercase(copy(strExpr, N + 1, 3)) = 'or ') then
              begin
                Op := opOr;
                Len := 2;
                inc(N);
              end else
                booFound := false;
}
      else
        booFound := false;
    end else
      booFound := false;
    inc(N);
  end;
  if booFound then
    Pos := N - 1
  else
    Pos := -1;
end;

procedure TQREvaluator.FindOp3(const strExpr : string; var Op : TQREvOperator; var Pos, Len : integer);
var
  n : integer;
  booFound : boolean;
  intParenteses : integer;
  booString : boolean;
  aString : string;//[255];
  { EZM }
  cLastComilla : Char;
begin
  n := 1;
  intParenteses := 0;
  booFound := false;
  booString := false;
  Len := 1;
  aString := strExpr;
  cLastComilla := Chr(0);
  while (N < Length(strExpr)) and (not booFound) do
  begin
    booFound := true;
    case aString[N] of
      '(' : if not booString then inc(intParenteses);
      ')' : if not booString then dec(intParenteses);
      { EZM }
      '''','"','[',']' : ResuelveComillas( aString[N], cLastComilla, booString );
      // '''': booString := not booString;
    end;
    if (intParenteses = 0) and (not booString) and (N > 1) then
    begin
      case aString[N] of
        '*' : Op := opMul;
        '/' : Op := opDiv;
        '%' : Op := opModulo;
{ EZM
        ' ' : if (AnsiLowercase(copy(strExpr, n + 1, 4)) = 'and ') then
              begin
                Op := opAnd;
                Len := 3;
                inc(N);
              end else
                booFound := false;
}
      else
        booFound := false;
    end;
  end else
    booFound := false;
    inc(N);
  end;
  if booFound then
    Pos := N - 1
  else
    Pos := -1;
end;

procedure TQREvaluator.FindOpLogico(const sOperador, strExpr : string; var Op : TQREvOperator; var Pos, Len : integer);
var
  n : integer;
  booFound : boolean;
  intParenteses : integer;
  booString : boolean;
  aString : string;//[255];
  { EZM }
  cLastComilla : Char;
  nLenOp : Integer;
begin
  n := 1;
  intParenteses := 0;
  booFound := false;
  booString := false;
  Len := 1;
  aString := strExpr;
  cLastComilla := Chr(0);
  nLenOp := Length( sOperador );
  while (N < Length(strExpr)) and (not booFound) do
  begin
    booFound := true;
    case aString[N] of
      '(' : if not booString then inc(intParenteses);
      ')' : if not booString then dec(intParenteses);
      '''','"','[',']' : ResuelveComillas( aString[N], cLastComilla, booString );
    end;
    if (intParenteses = 0) and (not booString) and (N > 1) then
    begin
      case aString[N] of
        ' ' : if (AnsiLowercase(copy(strExpr, n + 1, nLenOp )) = sOperador ) then
              begin
                if ( sOperador = 'or ' ) then
                   Op := opOr
                else
                    Op := opAnd;
                Len := nLenOp-1;
                inc(N);
              end else
                booFound := false;
      else
        booFound := false;
    end;
  end else
    booFound := false;
    inc(N);
  end;
  if booFound then
    Pos := N - 1
  else
    Pos := -1;
end;



{ ********** EZM *********
function TQREvaluator.NegateResult(const Res : TQREvResult) : TQREvResult;
begin
  result.Kind := Res.Kind;
  case Res.Kind of
    resInt: result.intResult := - Res.intResult;
    resDouble: result.dblResult := -Res.dblResult;
    resBool: result.booResult := not Res.booResult;
  else
    result.Kind := resError;
  end;
end;
}

function TQREvaluator.EvalVariable(strVariable : string) : TQREvResult;
var
  SeparatorPos : integer;
  FieldName : string;
  aDataSet : TZetaCursor;
  aField : TZetaField;
  I : integer;
begin
  if assigned(FDataSets) then
  begin
    SeparatorPos := AnsiPos('.', strVariable);
    FieldName    := AnsiUpperCase(copy(strVariable, SeparatorPos + 1, length(StrVariable) - SeparatorPos));
    aField       := nil;
    for I := 0 to FDataSets.Count - 1 do
    begin
      aDataSet := TZetaCursor(FDataSets[I]);
      aField   := aDataSet.FindField(FieldName);
      if aField <> nil then break;
    end;

    if aField <> nil then
      FiFo.Put(TQREvElementDataField.CreateField(aField))
    else
      FiFo.Put(TQREvElementError.Create(ErrorEvaluador( eeVariableNoExiste, '', strVariable )));
  end
  else
    FiFo.Put(TQREvElementError.Create(ErrorEvaluador( eeVariableNoExiste, 'No hay Dataset', strVariable )));
end;

function TQREvaluator.EvalString(const strString : string) : TQREvResult;
begin
  result.Kind := resString;
  result.strResult := strString;
  FiFo.Put(TQREvElementString.CreateString(Result.StrResult));
end;

function TQREvaluator.EvalFunction(strFunc : string; const strArg : string) : TQREvResult;
var
  DelimPos : integer;
  aString : string;
  Res : TQREvResult;
  aFunc : TQREvElement;
  lFuncionOK : Boolean;
  nPosFunc : Integer;
begin
  StrFunc := AnsiUpperCase(StrFunc);
  { EZM }
  LastFunction := NIL;
{ Con 'RegresaConstante', ya no se necesita
  if ( StrFunc = 'DATE' ) then
  begin
     Res.Kind := resDouble;
     Res.resFecha := TRUE;
     Res.dblResult := DATE;
     FiFo.Put(TQREvElementConstant.CreateConstant(Res));
     Exit;
  end;
}

  aFunc := FunctionLibrary.GetFunction(strFunc, Self);
  if AFunc is TQREvElementError then
  begin
    if StrArg = '' then
    begin
      AFunc.Free;
      EvalVariable(StrFunc)
    end else
    begin
      FiFo.Put(AFunc);
    end
  end else
  begin
    { EZM }
    //TQREvElementFunction( AFunc ).Evaluador := Self;
    FiFo.Put(AFunc);
    nPosFunc := FiFo.Count-1;
    lFuncionOK := not (aFunc is TQREvElementError);
    if lFuncionOK then
    begin
      aString := strArg;
      repeat
        {EZM: Para encontrar ",," al principio }
        aString := Trim( aString );
        FindDelimiter(aString, DelimPos);
        if DelimPos > 0 then
        begin
          { EZM: Evaluar funciones con ",," o ",NIL," }
          if ( DelimPos = 1 ) or (Trim(AnsiLowercase(copy(aString, 1, DelimPos-1 ))) = 'nil') then
          begin
             { EZM }
             Res.resFecha := FALSE;
             Res.Kind := resError;
             Res.strResult := 'NIL';
             FiFo.Put(TQREvElementConstant.CreateConstant(Res));
             // Res := Evaluate( ' ' )
          end
          else
              Res := Evaluate(copy(aString, 1, DelimPos - 1));
        end
        else
          if length(aString) > 0 then Res := Evaluate(aString);
        Delete(aString, 1, DelimPos);
      until DelimPos = 0;
    end;
    FiFo.Put(TQREvElementArgumentEnd.Create);
    LastFunction := aFunc;
    if ( lFuncionOK ) and TQREvElementFunction( aFunc ).RegresaConstante then
      with TQREvElementFunction( aFunc ) do
      begin
           FiFo.NextItem := nPosFunc + 1;
           Res := ValueConstante( FiFo );
           // La funci�n SI se pudo resolver como constante
           // Quita funci�n y sus par�metros de FiFo
           // En su lugar, agrega una Constante
           if ( Res.Kind <> resError ) then
           begin
                FiFo.NextItem := nPosFunc;
                while ( nPosFunc < FiFo.Count ) do
                     FiFo.Delete( Fifo.Count-1 );
                FiFo.Put(TQREvElementConstant.CreateConstant(Res));
                LastFunction := NIL;
           end;
      end;
  end;
end;

function TQREvaluator.EvalConstant(const strConstant : string) : TQREvResult;
var
  N : integer;
  aString : string;//[255];
  { EZM }
  Op : TQREvOperator;
  Posicion, Longitud : integer;
  nFactor : Integer;
begin
  N := 1;
  { EZM }
  if ( strConstant[1] = '-' ) then
  begin
     aString := Copy( strConstant, 2, MAXINT );
     nFactor := -1;
  end
  else
  begin
      aString := strConstant;
      nFactor := 1;
  end;

  while (N <= Length(aString)) and  (aString[N] in ['0'..'9']) do
    inc(N);
  result.Kind := resInt;
  while ((N <= Length(aString)) and (aString[N] in ['0'..'9', '.', 'e', 'E', '+', '-'])) do
  begin
    inc(N);
    result.Kind := resDouble;
  end;
  if N - 1 <> Length(aString) then
  begin
    { EZM : Busca operadores L�gicos }
    if ( AnsiUpperCase( strConstant ) = '.T.' ) then
    begin
       Result.Kind := resBool;
       Result.booResult := TRUE;
    end
    else
    if ( AnsiUpperCase( strConstant ) = '.F.' ) then
    begin
       Result.Kind := resBool;
       Result.booResult := FALSE;
    end
    else
    begin
       FindOp1( strConstant, Op, Posicion, Longitud );
       if Posicion > 0 then
           Result := ErrorCreate( ErrorEvaluador( eeLogicos, '', strConstant ))
       else
           Result := ErrorCreate( ErrorEvaluador( eeSintaxis, 'Letras en N�mero', strConstant ));
    end;
  end
  else
  begin
    { EZM: Puse el try..except }
    try
      if result.Kind = resInt then
      try
        result.intResult := StrToInt(aString) * nFactor;
      except
        { EZM: Si es un entero muy grande, lo convierte a Double }
        result.Kind := resDouble;
      end;
      if result.Kind = resDouble then
      begin
          if {$IFDEF DELPHIXE3_UP}FormatSettings.{$ENDIF}DecimalSeparator <> '.' then
          begin
            while pos('.', aString) > 0 do
              aString[pos('.', aString)] := {$IFDEF DELPHIXE3_UP}FormatSettings.{$ENDIF}DecimalSeparator;
          end;
          result.dblResult := StrToFloat(aString) * nFactor;
      end;
     except
         Result := ErrorCreate( ErrorEvaluador( eeConstante, 'Constante Num�rica', aString ));
     end;
  end;
  { EZM }
  Result.resFecha := FALSE;
  if result.Kind=resError then
    FiFo.Put(TQREvElementError.Create( Result.strResult ))
  else
    FiFo.Put(TQREvElementConstant.CreateConstant(Result));
end;

function TQREvaluator.EvalFunctionExpr(const strFunc : string) : TQREvResult;
var
  argRes : TQREvResult;
  po : integer;
  { EZM }
  strArg : String;
begin
  po := AnsiPos('(', StrFunc);
  if po > 0 then
  begin
    if StrFunc[ length(StrFunc) ] = ')' then
    begin
      // Verifica "FUNCION()", sin espacio entre los parentesis
      if ( po = length(strFunc)-1 ) then
         strArg := ' '  // Obliga un espacio
      else
          strArg := copy(StrFunc, po + 1, length(strFunc) - po - 1);
      // Trim permite dejar espacios antes del primer paren
      result := EvalFunction(Trim(copy(StrFunc, 1, po - 1)), strArg );
      { EZM}
      if ( LastFunction <> NIL ) and BuscandoRequeridos then
      begin
         TQREvElementFunction( LastFunction ).Expresion := strFunc;
      end;
    end else
    begin
      { EZM }
      // result := EvalFunction( strFunc, '');
      Result := ErrorCreate( ErrorEvaluador( eeParentesis, 'Falt� Cerrar', strFunc ));
      FiFo.Put(TQREvElementError.Create( Result.strResult ));
    end;
  end else
  begin
    argRes.Kind := resError;
    result := EvalFunction(StrFunc, '');
    if ( LastFunction <> NIL ) and ( BuscandoRequeridos ) then
    begin
         TQREvElementFunction( LastFunction ).Expresion := strFunc;
    end;
  end;
end;

function TQREvaluator.EvalFactor(strFactorExpr : string) : TQREvResult;
var
  aString : string[255];
begin
  TrimString(strFactorExpr);
  aString := strFactorExpr;
//if (AnsiLowerCase(Copy(strFactorExpr, 1, 3)) = 'not') then
    { EZM }
//  result := EvalSimpleExpr('0!' + Copy(strFactorExpr, 4, Length(strFactorExpr)))
//  result := NegateResult(EvalFactor(Copy(strFactorExpr, 4, Length(strFactorExpr))))
// else
    case aString[1] of
      '(' : if strFactorExpr[Length(strFactorExpr)] = ')' then
              result := Evaluate(Copy(strFactorExpr, 2, Length(strFactorExpr) - 2))
            else
            begin
              { EZM }
              Result := ErrorCreate( ErrorEvaluador( eeParentesis, '', strFactorExpr ));
              FiFo.Put(TQREvElementError.Create( Result.strResult ));
            end;
      // '-' : result := EvalSimpleExpr('0-' + Copy(strFactorExpr, 2, Length(strFactorExpr)));
      '-' : if ( Length( strFactorExpr ) >= 2 ) and ( strFactorExpr[2] in [ '0'..'9' ] ) then
               result := EvalConstant( strFactorExpr )
            else
                Result := EvalSimpleExpr('0-' + Copy(strFactorExpr, 2, Length(strFactorExpr)));
      { EZM
      '-' : result := NegateResult(EvalFactor(Copy(strFactorExpr, 2, Length(strFactorExpr))));
      }
      '+' : result := EvalFactor(Copy(strFactorExpr, 2, Length(strFactorExpr)));
      { EZM }
      '0'..'9', '.' : result := EvalConstant(strFactorExpr);
      '''' : if aString[Length(strFactorExpr)] = '''' then
               result := EvalString(Copy(strFactorExpr, 2, Length(strFactorExpr) - 2))
             else
             begin
               { EZM }
               Result := ErrorCreate( ErrorEvaluador( eeSintaxis, 'Comillas sin Terminar', strFactorExpr ));
               FiFo.Put(TQREvElementError.Create( Result.strResult ));
             end;
      { EZM: Cambio de Corchetes para que sean equivalentes a comillas }
      '[' : if aString[Length(strFactorExpr)] = ']' then
               result := EvalString(Copy(strFactorExpr, 2, Length(strFactorExpr) - 2))
             else
             begin
               { EZM }
               Result := ErrorCreate( ErrorEvaluador( eeSintaxis, 'Comillas sin Terminar', strFactorExpr ));
               FiFo.Put(TQREvElementError.Create( Result.strResult ));
             end;
      '"' : if aString[Length(strFactorExpr)] = '"' then
               result := EvalString(Copy(strFactorExpr, 2, Length(strFactorExpr) - 2))
             else
             begin
               { EZM }
               Result := ErrorCreate( ErrorEvaluador( eeSintaxis, 'Comillas sin Terminar', strFactorExpr ));
               FiFo.Put(TQREvElementError.Create( Result.strResult ));
             end;
      'A'..'Z', 'a'..'z' : result := EvalFunctionExpr(strFactorExpr);
    else
    begin
      Result := ErrorCreate( ErrorEvaluador( eeSintaxis, 'Caracteres Raros', aString ));
      FiFo.Put(TQREvElementError.Create( Result.strResult ));
    end;
  end;
end;

function TQREvaluator.EvalSimpleExpr(const strSimplExpr : string) : TQREvResult;
var
  op : TQREvOperator;
  intStart,
  intLen : integer;
  Res1,
  Res2 : TQREvResult;
begin
  FindOp2(strSimplExpr, op, intStart, intLen);
  if intStart > 0 then
  begin
    FiFo.Put(TQREvElementOperator.CreateOperator(Op));
    Res1 := EvalTerm(Copy(strSimplExpr, 1, intStart - 1));
    if Op = opMinus then
      Res2 := EvalSimpleExpr(Flip(Copy(strSimplExpr, intStart + intLen, Length(strSimplExpr)),'+','-'))
    else
      Res2 :=EvalSimpleExpr(Copy(strSimplExpr, intStart + intLen, Length(strSimplExpr)))
  end else
    result := EvalTerm(strSimplExpr);
end;

function TQREvaluator.EvalOrExpr(const strSimplExpr : string) : TQREvResult;
var
  op : TQREvOperator;
  intStart,
  intLen : integer;
  Res1,
  Res2 : TQREvResult;
begin
  FindOpLogico( 'and ', strSimplExpr, op, intStart, intLen);
  if intStart > 0 then
  begin
    FiFo.Put(TQREvElementOperator.CreateOperator(Op));
    Res1 := EvalAndExpr(Copy(strSimplExpr, 1, intStart - 1));
    Res2 := Evaluate(Copy(strSimplExpr, intStart + intLen, Length(strSimplExpr)))
  end else
    result := EvalAndExpr(strSimplExpr);
end;

function TQREvaluator.EvalAndExpr(const strSimplExpr : string) : TQREvResult;
var
  op : TQREvOperator;
  intStart,
  intLen : integer;
  Res1,
  Res2 : TQREvResult;
begin
  if (AnsiLowerCase(Copy(Trim(strSimplExpr), 1, 3)) = 'not') then
    { EZM }
    result := EvalAndExpr('0!(' + Copy(Trim(strSimplExpr), 4, MAXINT ) + ')')
  else
  begin
    FindOp1(strSimplExpr, op, intStart, intLen);
    if intStart > 0 then
    begin
      FiFo.Put(TQREvElementOperator.CreateOperator(Op));
      Res1 := EvalSimpleExpr(Copy(strSimplExpr, 1, intStart - 1));
      Res2 := EvalSimpleExpr(Copy(strSimplExpr, intStart + intLen, Length(strSimplExpr)))
    end else
      result := EvalSimpleExpr(strSimplExpr);
  end;
end;


function TQREvaluator.EvalTerm(const strTermExpr : string) : TQREvResult;
var
  Op : TQREvOperator;
  intStart,
  intLen : integer;
  Res1,
  Res2 : TQREvResult;
begin
  FindOp3(strTermExpr, op, intStart, intLen);
  if intStart > 0 then
  begin
    FiFo.Put(TQREvElementOperator.CreateOperator(Op));
    Res1 := EvalFactor(Copy(strTermExpr, 1, intStart - 1));
    if Op = opDiv then
      Res2 := EvalTerm(Flip(Copy(strTermExpr, intStart + intLen, Length(strTermExpr)), '*', '/'))
    else
      Res2 := EvalTerm(Copy(strTermExpr, intStart + intLen, Length(strTermExpr)));
  end else
    result := EvalFactor(strTermExpr);
end;

function TQREvaluator.Evaluate(const strExpr : string) : TQREvResult;
var
  op : TQREvOperator;
  intStart,
  intLen : integer;
  Res1,
  Res2 : TQREvResult;
begin
//FindOp1(strExpr, op, intStart, intLen);
  FindOpLogico( 'or ', strExpr, op, intStart, intLen);
  if intStart > 0 then
  begin
    FiFo.Put(TQREvElementOperator.CreateOperator(Op));
    { EZM }
    Res1 := EvalOrExpr(Copy(strExpr, 1, intStart - 1));
    Res2 := Evaluate(Copy(strExpr, intStart + intLen, Length(strExpr)));
  end else
    result := EvalOrExpr(strExpr);
end;

procedure TQREvaluator.Prepare(const strExpr : string);
var
  Value : TQREvResult;
begin
  if Prepared then Unprepare;
  FiFo := TQRFiFo.Create;
  if strExpr = '' then
    Value := Evaluate(''' ''')
  else
    Value := Evaluate(strExpr);
  Prepared := true;
end;

procedure TQREvaluator.UnPrepare;
begin
  FiFo.Free;
  Prepared := false;
end;

procedure TQREvaluator.Reset;
var
  I : integer;
begin
  for I := 0 to FiFo.FiFo.Count - 1 do
    TQREvElement(FiFo.FiFo[I]).Reset;
end;

function TQREvaluator.Value : TQREvResult;
var
  F : TObject;
begin
  FiFo.Start;
  F := FiFo.Get;
  if F = nil then
    { EZM }
    Result := ErrorCreate( ErrorEvaluador( eeDesconocido, 'Vac�o', '' ))
  else
    Result := TQREvElement(F).Value(FiFo);
end;

function TQREvaluator.GetIsAggreg : boolean;
var
  I : integer;
begin
  Result := false;
  for I := 0 to FiFo.FiFo.Count - 1 do
    Result := Result or TQREvElement(FiFo.FiFo[I]).IsAggreg;
end;

function TQREvaluator.GetAggregate : boolean;
begin
  Result := FiFo.Aggreg;
end;

procedure TQREvaluator.SetAggregate(Value : boolean);
begin
  FiFo.Aggreg := Value;
end;

function TQREvaluator.Calculate(const strExpr : string) : TQREvResult;
begin
  Prepare(strExpr);
  result := Value;
  UnPrepare;
end;

function TQREvaluator.EsConstante: Boolean;
var
    Elemento : TQREvElement;
begin
    if Fifo.GetFifoCount = 1 then
    begin
        Elemento := TQREvElement( Fifo.Fifo[ 0 ] );
        Result := ( Elemento is TQREvElementString ) or
                  (( Elemento is TQREvElementConstant ) and
                   ( TQrEvElementConstant( Elemento ).Expresion = '' ));
    end
    else
        Result := FALSE;
end;


{ EZM }
{
procedure InitQRLibrary;
begin
  // Borra la Lista, y agrega las b�sicas //
  QRFunctionLibrary.Entries.Clear;
  RegisterQRFunction(TQREvSumFunction, 'SUM', 'SUM(<X>)|' + LoadStr(SqrSumDesc), LoadStr(SqrQSD), '3N');
  RegisterQRFunction(TQREvCountFunction, 'COUNT', 'COUNT|'+ LoadStr(SqrCountDesc), LoadStr(SqrQSD), '3');
  RegisterQRFunction(TQREvMaxFunction, 'MAXIMO', 'MAXIMO(<X>)|' + LoadStr(SqrMaxDesc), LoadStr(SqrQSD), '3V');
  RegisterQRFunction(TQREvMinFunction, 'MINIMO', 'MINIMO(<X>)|' + LoadStr(SqrMinDesc), LoadStr(SqrQSD), '3V');
  RegisterQRFunction(TQREvAverageFunction, 'AVERAGE', 'AVERAGE(<X>)|' + LoadStr(SqrAverageDesc), LoadStr(SqrQSD), '3N');
end;
}

end.
