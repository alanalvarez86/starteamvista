unit ZFuncsIMSS;

interface

uses Classes, Sysutils, DB,
     ZetaQRExpr,
     ZEvaluador,
     dImssClass,
     ZetaCommonClasses,
     ZGlobalTress
{$ifdef PROFILE},ZetaProfiler{$endif};

{$define QUINCENALES}
{$define CAMBIO_TNOM}
{$define IMSS_VACA}   //Corregir d�as IMSS cuando hay vacaciones
{$define IMSS_UMA}
{.$undefine QUINCENALES}
{.$define DESCUENTO_SUA}

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );

implementation

uses ZSuperEvaluador,
     ZetaCommonLists,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaServerTools,
     ZetaSQLBroker,
     DCalculoNomina,
     Controls,
     DSalMin,
     DValUma,
     DPrimasIMSS,
     DZetaServerProvider,
     DSuperReporte, ZCreator,  DBClient;

const
     K_DEFAULT_TOPE = 3;
     K_DEFAULT_TOPE_DOBLES = 9;
     K_REGLA3X3_PROPORCIONAL = 1;
     K_REGLA3X3_CUANTAS = 2;
     K_REGLA3X3_DOBLES = 3;
     K_EXENTAS_SEMANAL = 3;
     K_EXENTAS_DIARIO  = 3;
     QRY_EXENTAS = 'select FA_HORAS, FA_FEC_INI, FA_MOTIVO from FALTAS where FALTAS.PE_YEAR = :Year and ' +
                   'FALTAS.PE_TIPO = :Tipo and FALTAS.PE_NUMERO = :Numero and FALTAS.CB_CODIGO = :Empleado and ' +
                   'FALTAS.FA_DIA_HOR = ''H'' and FALTAS.FA_MOTIVO >= 1 and FALTAS.FA_MOTIVO <= 3 and ' +
                   'FALTAS.FA_HORAS > 0 order by FALTAS.FA_FEC_INI';
     QRY_SAL_POND = 'select CB_FEC_INT, CB_SAL_INT, CB_OLD_INT from KARDEX where '+
                    '( CB_CODIGO = :Empleado ) and ( CB_TIPO = ''%s'' ) and '+
                    '( CB_FECHA >= :Inicio ) and ( CB_FECHA <= :Final ) '+
                    'order by CB_FECHA desc';

type
    eTipoAjustaINFO = ( taiSiempre, taiFinMes, taiFinBimestre);

{ ********************* FUNCIONES LOCALES ***********************}

function GetIMSSAjust( FDataSet : TZetaCursor{$ifdef IMSS_VACA}; const lDiasCotizaSinFV: Boolean{$endif} ) : TPesos;
var
   nDiasAjust : TPesos;
begin
     with FDataSet do
     begin
          nDiasAjust:= FieldByName( 'DIAS_AJUST' ).AsFloat;
          if ( eLiqNomina( FieldByName( 'NO_LIQUIDA' ).AsInteger) = lnNormal ) then
          begin
{$ifdef IMSS_VACA}
               if lDiasCotizaSinFV then
                  { Se suman las vacaciones pagadas y se restan las vacaciones gozadas a valor de 1 d�a}
                  Result := nDiasAjust + ( FieldByName( 'DIAS_VCAL' ).AsInteger - FieldByName( 'DIAS_VTOT' ).AsInteger )
               else
{$endif}
               Result := nDiasAjust + MiRound( FieldByName( 'DIAS_VACA' ).AsFloat * 7 / 6, 0 )
          end
          else
              Result := nDiasAjust;
     end;
end;

function CalculaCuota( const nVeces : TPesos; const nPrima : TTasa; const nBase, nDias, nSMGDF : TPesos; cDescrip : String ; var nTopado : TPesos) : TPesos;
begin
     if ( nVeces > 0 ) then
         nTopado := rMin( nBase, nVeces*nSMGDF )
     else
         nTopado := nBase;
     Result := Redondea( nTopado * nDias * ( nPrima / 100 ) );
end;


function CalcCuota( oEvaluador : TZetaEvaluador; const nVeces : TPesos; const nPrima : TTasa; const nBase, nDias, nSMGDF : TPesos; const cDescrip : String ) : TPesos;
var
   nTopado : TPesos;
begin
     Result := CalculaCuota( nVeces ,nPrima , nBase, nDias, nSMGDF ,cDescrip ,nTopado );
     if oEvaluador.Rastreando then
     with oEvaluador.Rastreador do
       begin
            RastreoHeader( cDescrip );
            if ( nTopado > 0 ) then
            begin
                 RastreoPesos( '    Salario Base               : ', nTopado );
                 RastreoPesos( '(x) D�as                       : ', nDias );
                 RastreoTasa(  '(x) Prima                      : ', nPrima );
                 RastreoPesos( '(=) Cuota de este Seguro       : ', Result );
            end
            else
                RastreoPesos( 'No hay Excedente, No Retiene   : ', 0 );
       end;
end;

{$ifdef ANTES}
function CalculaHorasExentas( oProvider: TdmZetaServerProvider; oQuery: TZetaCursor;
         const iEmpleado, max_diario, max_semanal : Integer; const fec_Inicial,
         fec_final : TDate ): Double;
{$else}
function CalculaHorasExentas( oProvider: TdmZetaServerProvider; oQuery: TZetaCursor;
         const iEmpleado, max_diario, max_semanal, max_dobles_semanal : Integer; const fec_Inicial,
         fec_final : TDate ): Double;
{$endif}
var
   dias_extras, nDias, nPos, nDOWCorte : integer;
   h_dobles_3x3, h_triples_3x3, h_extras, h_dobles, h_triples,h_exentas: TDiasHoras;
{$ifndef ANTES}
   h_suma_dobles: TDiasHoras;
{$endif}
   dExtras, dCorte : TDate;
   FRegla3x3 : eRegla3x3;
   aExtras,aDobles: array of Double;
begin
     with oProvider do
     begin
          nDOWCorte := GetGlobalInteger( K_GLOBAL_PRIMER_DIA );
          FRegla3x3 := eRegla3x3( GetGlobalInteger( K_GLOBAL_HRS_EXENTAS_IMSS ) );
     end;
     dCorte := fec_Inicial;
     nDias := iMin( Trunc( fec_final - fec_inicial ) + 1, 31 );
     dias_extras := 0;
     h_suma_dobles := 0;
     h_exentas := 0;

     if ( nDias > 7 ) then
     begin
          // Nos vamos a la fecha de corte de la semana anterior //
          // Contamos cuantos d�as de horas extras hubo en esa parte //
          while DayofWeek( dCorte ) <> nDOWCorte do
                dCorte := dCorte - 1;
          // PENDIENTE: Contar dias_extras en semana anterior //
          dCorte := dCorte + 7;
     end
     else
          dCorte := fec_final + 1;

     SetLength( aExtras, nDias );
     SetLength( aDobles, nDias );
     for nPos := Low( aExtras ) to High( aExtras ) do
     begin
          aExtras[nPos] := 0;
          aDobles[nPos] := 0;
     end;

     with oQuery do
     begin
          Active := FALSE;
          with oProvider do
               ParamAsInteger( oQuery, 'Empleado', iEmpleado );
          Active := TRUE;
          while not EOF do
          begin
               if ( FieldbyName( 'FA_FEC_INI' ).AsDateTime = NullDateTime ) then
                  nPos := 0
               else
                  nPos := iMax( iMin( Trunc( FieldbyName( 'FA_FEC_INI' ).AsDateTime - fec_inicial ), nDias ), 0 );

               aExtras[nPos] := aExtras[nPos] + FieldByName( 'FA_HORAS' ).AsFloat;
               if eMotivoFaltaHoras(FieldByName('FA_MOTIVO').AsInteger) in [mfhExtras,mfhDobles] then
                  aDobles[nPos] := aDobles[nPos] + FieldByName( 'FA_HORAS' ).AsFloat;

               //esto era lo que estaba antes. aExtras[nPos] := aExtras[nPos] + FieldByName( 'FA_HORAS' ).AsFloat;
               Next;
          end;
          Active := FALSE;
     end;
     { Esto estaba antes:
     for nPos := Low( aExtras ) to High( aExtras ) do
     begin
          dExtras := fec_inicial + nPos;
          if dExtras >= dCorte then
          begin
               // Semana nueva, tiene otros 3 d�as frescos //
               dCorte := dCorte + 7;
               tot_dias := 0;
          end;
          if ( aExtras[nPos] > 0 ) then
          begin
               tot_dias := tot_dias + 1;
               if ( tot_dias <= max_semanal ) then
                  Result := Result + rMin( aExtras[nPos], max_diario );
          end;
     end;}

     {cv: 26-nov-2001
     Esta misma logica esta repetida en
     DNominaClass.PAS  TNomina.CalculaTotales
     ZFuncsImss.CalculaRegla3x3
     ZFuncsImss.CalculaHorasExentas
     si cambia algo en este algoritmo hay que
     verificarlo en los otros 2 lugares}
     for nPos := Low( aExtras ) to High( aExtras ) do
     begin
          dExtras := fec_inicial + nPos;
          if dExtras >= dCorte then
          begin
               // Semana nueva, tiene otros 3 d�as frescos //
               dCorte := dCorte + 7;
               dias_extras := 0;
{$ifndef ANTES}
               h_suma_dobles:= 0;
{$endif}
          end;
          h_extras := aExtras[nPos];
{$ifdef ANTES}
          h_dobles := aDobles[nPos];
{$else}
          h_dobles := ZetaCommonTools.rMin( aDobles[nPos], max_dobles_semanal - h_suma_dobles );
          h_suma_dobles := h_suma_dobles + h_dobles;
{$endif}

          if h_extras > 0 then
             case FRegla3x3 of
               r3x3_Proporcional:
               begin
                    { Cada horas doble vale 1, Cada hora triples vale 1.5  ( 3 / 2 = 1.5 ) }
                    { Esto facilita multiplicaci�n: NO_EXENTAS * SAL_HORA * 2 }
                    dias_extras := ( dias_extras + 1 );
                    if ( dias_extras <= max_semanal ) then
                    begin
                         h_triples := h_extras - h_dobles;
                         h_dobles_3x3 := ZetaCommonTools.rMin( h_dobles, max_diario );
                         h_triples_3x3 := ZetaCommonTools.rMin( h_triples, ( max_diario - h_dobles_3x3 ) );
                         h_exentas := h_exentas + h_dobles_3x3 + h_triples_3x3 * 1.5;
                    end;
               end;
               r3x3_Cuantas:
               begin
                    { Considera dobles y triples. Tanto dobles como triples valen 1 }
                    dias_extras := ( dias_extras + 1 );
                    if ( dias_extras <= max_semanal ) then
                       h_exentas := h_exentas + ZetaCommonTools.rMin( h_extras, max_diario );
               end;
               else { SoloDobles }
               begin
                    if ( h_dobles > 0 ) then { Solo considera horas dobles como exentas }
                    begin
                         dias_extras := ( dias_extras + 1 );
                         if ( dias_extras <= max_semanal ) then
                            h_exentas := h_exentas + ZetaCommonTools.rMin( h_dobles, max_diario );
                    end;
               end;
             end;
     end;

     Result := h_exentas;
     aExtras := nil;      // Libera Memoria de dynamic array
end;

{*********************** TZetaIMSSObrero ************************}
type
  TZetaIMSSObrero = class( TZetaRegresa )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

procedure TZetaIMSSObrero.GetRequeridos( var TipoRegresa: TQREvResultType);
begin
     TipoRegresa := resDouble;
     with ZetaEvaluador.oZetaCreator do
     begin
          PreparaPrimasIMSS;
          PreparaSalMin;
          {$ifdef IMSS_UMA}
          PreparaValUma;
          {$endif}
          oZetaProvider.GetDatosPeriodo;
          oZetaProvider.InitGlobales;
     end;

     AgregaParam( 'DIAS_AJUST' );
     AgregaParam( 'DIAS_VACA' );
     AgregaParam( 'DIAS_IMSS' );
     AgregaParam( 'DIAS_EYM' );
     AgregaParam( 'SAL_INT' );
     AgregaParam( 'SALARIO' );
{$ifdef IMSS_VACA}
     AgregaParam( 'DIAS_VTOT' );
     AgregaParam( 'DIAS_VCAL' );
{$endif}

     AgregaRequerido( 'NOMINA.NO_LIQUIDA', enNomina );
end;

function IMSSObrero( oEvaluador : TZetaEvaluador; const nDiasEym, nDiasIvcm, nSalBase, nSalMin, nSalario, nImssAjusta : TPesos; const dLey : TDate; const nSMGDF : TPesos ; const iRama :Integer ) : TPesos;
const
     K_TODOS   = 0;
     K_ENF_MAT = 1;
     K_INV_VID = 2;
     K_CES_VEJ = 3;
var
   nEym, nIV, nCV : TPesos;
   oPrimas : TPrimasIMSS;
begin
     if ( nSalario = nSalMin ) then
     begin
          if oEvaluador.Rastreando then
            with oEvaluador.Rastreador do
            begin
               RastreoHeader( 'RASTREO DE IMSS OBRERO' );
               RastreoPesos( 'Salario Diario = M�nimo : ', nSalario );
               RastreoPesos( 'No paga IMSS Obrero     : ', 0 );
            end;
          Result := 0
     end
     else
     begin
          nEym := 0;
          nIV  := 0;
          nCV  := 0;
          if  oEvaluador.Rastreando then
            with oEvaluador.Rastreador do
            begin
                 RastreoHeader( 'RASTREO DE IMSS TRABAJADOR' );
                 {$ifdef IMSS_UMA}
                 RastreoPesos(  'Unidad de Medida y Act. (UMA)..:', nSMGDF );   // Se remplaza en la llamada el valor de nSMGDF con el valor de UMA
                 {$else}
                 RastreoPesos(  'Salario M�nimo General del D.F.: ', nSMGDF );
                 {$endif}
            end;
          oPrimas := oEvaluador.oZetaCreator.dmPrimasIMSS.GetPrimasIMSS( dLey );
          if ( iRama = K_TODOS ) or ( iRama = K_ENF_MAT )then
          begin
               nEym := CalcCuota( oEvaluador, oPrimas.SS_L_A25, oPrimas.SS_O_A25, nSalBase, nDiasEyM, nSMGDF, 'Enfermedad y Maternidad (Art.25)' ) +
                       CalcCuota( oEvaluador, 0, oPrimas.SS_O_A1062, rMAX( nSalBase-oPrimas.SS_MAXIMO*nSMGDF, 0 ), nDiasEyM, nSMGDF, 'Enfermedad y Maternidad (Art.106-II)' ) +
                       CalcCuota( oEvaluador, oPrimas.SS_L_A107, oPrimas.SS_O_A107, nSalBase, nDiasEyM, nSMGDF, 'Enfermedad y Maternidad (Art.107)' );
          end;
          if ( iRama = K_TODOS ) or ( iRama = K_INV_VID )then
          begin
               nIV  := CalcCuota( oEvaluador, oPrimas.SS_L_IV, oPrimas.SS_O_IV, nSalBase, nDiasIVCM, nSMGDF, 'Invalidez y Vida (Art.147)' );
          end;
          if ( iRama = K_TODOS ) or ( iRama = K_CES_VEJ ) then
          begin
               nCV  := CalcCuota( oEvaluador, oPrimas.SS_L_CV, oPrimas.SS_O_CV, nSalBase, nDiasIVCM, nSMGDF, 'Cesant�a y Vejez (Art.168-II)' );
          end;

          Result := nEym + nIV + nCV;
          if ( iRama = K_TODOS )then
          begin
               if oEvaluador.Rastreando then
               begin
                    with oEvaluador.Rastreador do
                    begin
                         RastreoHeader( 'Suma de Totales IMSS TRABAJADOR' );
                         RastreoPesos( '    Enfermedad y Maternidad    : ', nEym );
                         RastreoPesos( '(+) Invalidez y Vida           : ', nIV );
                         RastreoPesos( '(+) Cesant�a y Vejez           : ', nCV );
                         RastreoPesos( '(=) TOTAL IMSS TRABAJADOR      : ', Result );
                    end;
               end;
          end;
     end;
end;

function TZetaIMSSObrero.Calculate: TQREvResult;
var
   rDiasEym , rImssAjusta, rSalMin : TPesos;
   dLey : TDate;
   iRama : Integer;
{$ifdef IMSS_VACA}
   lDiasCotizaSinFV: Boolean;
{$endif}
begin
     with DataSetEvaluador, Result do
     begin
{$ifdef IMSS_VACA}
          lDiasCotizaSinFV := oZetaProvider.GetGlobalBooleano( K_GLOBAL_DIAS_COTIZADOS_FV );
{$endif}
          rImssAjusta := DefaultFloat( 5, GetIMSSAjust( DataSetEvaluador{$ifdef IMSS_VACA}, lDiasCotizaSinFV{$endif} ) );
          dLey        := DefaultDate( 6, oZetaProvider.DatosPeriodo.Fin );
{$ifdef IMSS_VACA}
          if lDiasCotizaSinFV then
             rDiasEym := DefaultFloat( 0, FieldByName( 'DIAS_EYM' ).AsFloat + ( rImssAjusta - FieldByName('DIAS_AJUST').AsFloat ) )
          else
{$endif}
          rDiasEym    := DefaultFloat( 0, FieldByName( 'DIAS_EYM' ).AsFloat + rMax( rImssAjusta - FieldByName('DIAS_AJUST').AsFloat, 0 ) );  // Los ajustes no se consideran para EyM
          iRama := DefaultInteger( 8,0 );
          if ParamVacio( 3 ) then
          begin
               if ( ZetaEvaluador.CalculandoNomina ) then
                  rSalMin := TCalcNomina(ZetaEvaluador.CalcNomina).nEmpleadoSalMin
               else
                   // EM: Puse la zona como constante porque es muy
                   // complicado obtener la zona real y este par�metro
                   // ya no se usa.
                   rSalMin := ZetaEvaluador.oZetaCreator.dmSalMin.GetSalMin( dLey, 'A' );
          end
          else
              rSalMin := ParamFloat( 3 );

          if( iRama < 0 ) or ( iRama > 3 )then
          begin
               ErrorFuncion( 'Error en Par�metro', 'Solo se Permiten Valores 0 a 3' );
          end;

          Kind := resDouble;
          dblResult := IMSSObrero( ZetaEvaluador,
                                   rDiasEym,
                                   DefaultFloat( 1, FieldByName( 'DIAS_IMSS' ).AsFloat + rImssAjusta ) , // rDiasIVCM
                                   DefaultFloat( 2, FieldByName( 'SAL_INT' ).AsFloat ), // rSalBase
                                   rSalMin,
                                   DefaultFloat( 4, FieldByName( 'SALARIO' ).AsFloat ), // rSalario
                                   rImssAjusta,
                                   dLey,
                                   {$ifdef IMSS_UMA}
                                   DefaultFloat( 7, ZetaEvaluador.oZetaCreator.dmValUma.GetVUDefaultDiario( dLey )), // rSMGDF - Valor UMA 2017 //
                                   {$else}
                                   DefaultFloat( 7, ZetaEvaluador.oZetaCreator.dmSalMin.GetSMGDF( dLey )), // rSMGDF  Pendiente PROMEDIAR SALARIOS MINIMOS //
                                   {$endif}
                                   iRama );  //iRama 0: Sumar todas las ramas
     end;
end;

{*********************** TZetaIMSSPatron ************************}
type
  TZetaIMSSPatron = class( TZetaRegresaQuery )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

procedure TZetaIMSSPatron.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
     with ZetaEvaluador.oZetaCreator do
     begin
          PreparaPrimasIMSS;
          PreparaSalMin;
          {$ifdef IMSS_UMA}
          PreparaValUma;
          {$endif}
          oZetaProvider.GetDatosPeriodo;
          oZetaProvider.InitGlobales;
     end;

     AgregaParam( 'DIAS_EYM' );
     AgregaParam( 'DIAS_AJUST' );
     AgregaParam( 'DIAS_IMSS' );
     AgregaParam( 'SAL_INT' );
     AgregaParam( 'DIAS_VACA' );
{$ifdef IMSS_VACA}
     AgregaParam( 'DIAS_VTOT' );
     AgregaParam( 'DIAS_VCAL' );
{$endif}

     AgregaRequerido( 'NOMINA.NO_LIQUIDA', enNomina );
     AgregaRequerido( 'NOMINA.CB_PATRON', enNomina );
end;

function IMSSPatron( oEvaluador : TZetaEvaluador;const nDiasEym, nDiasIvcm, nSalBase, nImssAjusta : TPesos;
         const dLey : TDate; const nSMGDF : TPesos; const nPrimaRT : TTasa; const iRama:Integer
         {$ifdef DESCUENTO_SUA};const sPatron: String{$endif} ) : TPesos;
const
     K_TODOS   = 0;
     K_ENF_MAT = 1;
     K_INV_VID = 2;
     K_RIE_TRA = 3;
     K_GUA_PRE = 4;
var
   nEym, nIV, nRT, nGuard : TPesos;
   oPrimas : TPrimasIMSS;
begin
     nEym   := 0;
     nIV    := 0;
     nRT    := 0;
     nGuard := 0;
     if  oEvaluador.Rastreando then
       with oEvaluador.Rastreador do
       begin
            RastreoHeader( 'RASTREO DE IMSS PATRONAL' );
            {$ifdef IMSS_UMA}
            RastreoPesos(  'Unidad de Medida y Act. (UMA)..:', nSMGDF );   // Se remplaza en la llamada el valor de nSMGDF con el valor de UMA
            {$else}
            RastreoPesos(  'Salario M�nimo General del D.F.: ', nSMGDF );
            {$endif}
       end;
     oPrimas := oEvaluador.oZetaCreator.dmPrimasIMSS.GetPrimasIMSS( dLey {$ifdef DESCUENTO_SUA},sPatron {$endif} );
     if ( iRama = K_TODOS ) or ( iRama = K_ENF_MAT )then
     begin
          nEym := CalcCuota( oEvaluador, oPrimas.SS_L_A25, oPrimas.SS_P_A25, nSalBase, nDiasEyM, nSMGDF, 'Enfermedad y Maternidad (Art.25)' ) +
                  CalcCuota( oEvaluador, 1, oPrimas.SS_P_A1061, nSMGDF, nDiasEyM, nSMGDF, 'Enfermedad y Maternidad (Art.106-I)' ) +
                  CalcCuota( oEvaluador, 0, oPrimas.SS_P_A1062, rMAX( nSalBase-oPrimas.SS_MAXIMO*nSMGDF, 0 ), nDiasEyM, nSMGDF, 'Enfermedad y Maternidad (Art.106-II)' ) +
                  CalcCuota( oEvaluador, oPrimas.SS_L_A107, oPrimas.SS_P_A107, nSalBase, nDiasEyM, nSMGDF, 'Enfermedad y Maternidad (Art.107)' );
     end;
     if ( iRama = K_TODOS ) or ( iRama = K_INV_VID )then
     begin
          nIV    := CalcCuota( oEvaluador, oPrimas.SS_L_IV, oPrimas.SS_P_IV, nSalBase, nDiasIVCM, nSMGDF, 'Invalidez y Vida (Art.147)' );
     end;
     if ( iRama = K_TODOS ) or ( iRama = K_RIE_TRA )then
     begin
          nRT    := CalcCuota( oEvaluador, oPrimas.SS_L_RT, nPrimaRT, nSalBase, nDiasIVCM, nSMGDF, 'Riesgos de Trabajo (Art.71)' );
     end;
     if ( iRama = K_TODOS ) or ( iRama = K_GUA_PRE )then
     begin
          nGuard := CalcCuota( oEvaluador, oPrimas.SS_L_GUARD, oPrimas.SS_P_GUARD, nSalBase, nDiasIVCM, nSMGDF, 'Guarder�as y Prestaciones Sociales (Art.211)' );
     end;

     Result := nEym + nIV + nRT + nGuard;
     if ( iRama = K_TODOS )then
     begin
          if oEvaluador.Rastreando then
          begin
               with oEvaluador.Rastreador do
               begin
                    RastreoHeader( 'Suma de Totales IMSS PATRONAL' );
                    RastreoPesos( '    Enfermedad y Maternidad    : ', nEym );
                    RastreoPesos( '(+) Invalidez y Vida           : ', nIV );
                    RastreoPesos( '(+) Riesgos de Trabajo         : ', nRT );
                    RastreoPesos( '(+) Guarder�as y Prestaciones  : ', nGuard );
                    RastreoPesos( '(=) TOTAL IMSS PATRONAL        : ', Result );
               end;
          end;
     end;
end;

function TZetaIMSSPatron.Calculate: TQREvResult;
var
   rDiasEym , rImssAjusta : TPesos;
   dLey : TDate;
   rPrimaRT : TTasa;
   iRama :Integer;
{$ifdef IMSS_VACA}
   lDiasCotizaSinFV: Boolean;
{$endif}
begin
     with DataSetEvaluador, Result do
     begin
{$ifdef IMSS_VACA}
          lDiasCotizaSinFV := oZetaProvider.GetGlobalBooleano( K_GLOBAL_DIAS_COTIZADOS_FV );
{$endif}
          rImssAjusta :=  DefaultFloat( 3, GetIMSSAjust( DataSetEvaluador{$ifdef IMSS_VACA}, lDiasCotizaSinFV{$endif} ) );
          dLey := DefaultDate(  4, oZetaProvider.DatosPeriodo.Fin );
{$ifdef IMSS_VACA}
          if lDiasCotizaSinFV then
             rDiasEym := DefaultFloat( 0, FieldByName( 'DIAS_EYM' ).AsFloat + ( rImssAjusta - FieldByName('DIAS_AJUST').AsFloat ) )
          else
{$endif}
          rDiasEym := DefaultFloat( 0, FieldByName( 'DIAS_EYM' ).AsFloat + rMax( rImssAjusta - FieldByName('DIAS_AJUST').AsFloat, 0 ) );

          iRama := DefaultInteger( 7,0 );
          if ParamVacio( 6 ) then
             rPrimaRT := ZetaEvaluador.oZetaCreator.dmPrimasIMSS.GetPrimaRiesgo( oZetaProvider.DatosPeriodo.Fin, FieldByName( 'CB_PATRON' ).AsString )
          else
              rPrimaRT := ParamFloat( 6 );

          if( iRama < 0 )  or ( iRama > 4 )then
          begin
               ErrorFuncion( 'Error en Par�metro', 'Solo se Permiten Valores 0 a 4' );
          end;
          Kind := resDouble;
          dblResult := IMSSPatron( ZetaEvaluador,
                                   rDiasEym,
                                   DefaultFloat( 1, FieldByName( 'DIAS_IMSS' ).AsFloat + rImssAjusta ) , // rDiasIvcm
                                   DefaultFloat( 2, FieldByName( 'SAL_INT' ).AsFloat ), // rSalBase
                                   rImssAjusta,
                                   dLey,
                                   {$ifdef IMSS_UMA}
                                   DefaultFloat( 5, ZetaEvaluador.oZetaCreator.dmValUma.GetVUDefaultDiario( dLey ) ), // rSMGDF - Valor UMA 2017
                                   {$else}
                                   DefaultFloat( 5, ZetaEvaluador.oZetaCreator.dmSalMin.GetSMGDF( dLey ) ), // rSMGDF
                                   {$endif}
                                   DefaultTasa(  6, rPrimaRT ),
                                   iRama     // Default 0: Sumar todas las ramas
{$ifdef DESCUENTO_SUA}
                                   ,FieldByName( 'CB_PATRON' ).AsString
{$endif}
                                   );
     end;
end;

{*********************** TZetaIMSSRetiro ************************}
type
  TZetaIMSSRetiro = class( TZetaRegresa )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

procedure TZetaIMSSRetiro.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
     with ZetaEvaluador.oZetaCreator do
     begin
          PreparaPrimasIMSS;
          PreparaSalMin;
          {$ifdef IMSS_UMA}
          PreparaValUma;
          {$endif}
          oZetaProvider.GetDatosPeriodo;
          oZetaProvider.InitGlobales;
     end;

     AgregaParam( 'DIAS_INCA' );
     AgregaParam( 'DIAS_IMSS' );
     AgregaParam( 'DIAS_VACA' );
     AgregaParam( 'SAL_INT' );
     AgregaParam( 'DIAS_AJUST' );
{$ifdef IMSS_VACA}
     AgregaParam( 'DIAS_VTOT' );
     AgregaParam( 'DIAS_VCAL' );
{$endif}

     AgregaRequerido( 'NOMINA.NO_LIQUIDA', enNomina );
{$ifdef DESCUENTO_SUA}
     AgregaRequerido( 'NOMINA.CB_PATRON', enNomina );
{$endif}
end;

function IMSSRetiro( oEvaluador : TZetaEvaluador; const nDiasSar, nDiasCV, nSalBase, nImssAjusta, nDiasInca : TPesos;
                     const dLey : TDate; const nSMGDF : TPesos;const iRama:Integer {$ifdef DESCUENTO_SUA}; const sPatron: String {$endif} ) : TPesos;
const
     K_TODOS   = 0;
     K_RETIRO  = 1;
     K_CES_VEJ = 2;
var
   nCV, nSAR : TPesos;
   oPrimas : TPrimasIMSS;
begin
     nCV  := 0;
     nSAR := 0;
     if oEvaluador.Rastreando then
       with oEvaluador.Rastreador do
       begin
            RastreoHeader( 'RASTREO DE IMSS RETIRO' );
            {$ifdef IMSS_UMA}
            RastreoPesos(  'Unidad de Medida y Act. (UMA)..:', nSMGDF );   // Se remplaza en la llamada el valor de nSMGDF con el valor de UMA
            {$else}
            RastreoPesos( 'Salario M�nimo General del D.F.: ', nSMGDF );
            {$endif}
       end;
     oPrimas    := oEvaluador.oZetaCreator.dmPrimasIMSS.GetPrimasIMSS( dLey {$ifdef DESCUENTO_SUA}, sPatron {$endif} );
     if ( iRama = K_TODOS ) or ( iRama = K_RETIRO )then
     begin
          nSar	:= CalcCuota( oEvaluador, oPrimas.SS_L_SAR, oPrimas.SS_P_SAR, nSalBase, nDiasSAR, nSMGDF, 'Retiro (Art.168-I)' );
     end;
     if ( iRama = K_TODOS ) or ( iRama = K_CES_VEJ )then
     begin
          nCV	:= CalcCuota( oEvaluador, oPrimas.SS_L_CV, oPrimas.SS_P_CV, nSalBase, nDiasCV, nSMGDF, 'Cesant�a y Vejez (Art.168-II)' );
     end;
     Result := nSar + nCV;

    if ( iRama = K_TODOS )then
    begin
         if oEvaluador.Rastreando then
         begin
              with oEvaluador.Rastreador do
              begin
                   RastreoHeader( 'Suma de Totales IMSS RETIRO' );
                   RastreoPesos( '    Seguro de Retiro           : ', nSar );
                   RastreoPesos( '(+) Cesant�a y Vejez           : ', nCV );
                   RastreoPesos( '(=) TOTAL IMSS RETIRO          : ', Result );
              end;
         end;
    end;
end;

function TZetaIMSSRetiro.Calculate: TQREvResult;
var
   rDiasInca, rImssAjusta : TPesos;
   dLey : TDate;
   iRama : Integer;
begin
     with DataSetEvaluador, Result do
     begin
          rImssAjusta :=  DefaultFloat( 3, GetIMSSAjust( DataSetEvaluador{$ifdef IMSS_VACA}, oZetaProvider.GetGlobalBooleano( K_GLOBAL_DIAS_COTIZADOS_FV ){$endif} ) );
          rDiasInca := DefaultFloat( 4, FieldByName( 'DIAS_INCA' ).AsFloat );
          iRama := DefaultInteger( 7,0 );
          if( iRama < 0 ) or ( iRama > 2 )then
          begin
               ErrorFuncion( 'Error en Par�metro', 'Solo se Permiten Valores 0 a 2' );
          end;

          Kind := resDouble;
          dLey := DefaultDate( 5, oZetaProvider.DatosPeriodo.Fin );
          dblResult := IMSSRetiro( ZetaEvaluador,
                                   DefaultFloat( 0, FieldByName( 'DIAS_IMSS' ).AsFloat + rImssAjusta + rDiasInca) , // rDiasSar
                                   DefaultFloat( 1, FieldByName( 'DIAS_IMSS' ).AsFloat + rImssAjusta ) , // rDiasCV
                                   DefaultFloat( 2, FieldByName( 'SAL_INT' ).AsFloat ) , // rSalBase
                                   rImssAjusta,
                                   rDiasInca,
                                   dLey,
                                   {$ifdef IMSS_UMA}
                                   DefaultFloat( 6, ZetaEvaluador.oZetaCreator.dmValUma.GetVUDefaultDiario( dLey ) ), // Val UMA 2017
                                   {$else}
                                   DefaultFloat( 6, ZetaEvaluador.oZetaCreator.dmSalMin.GetSMGDF( dLey ) ), // rSMGDF
                                   {$endif}
                                   iRama  // iRama 0 : Sumar todas las ramas
{$ifdef DESCUENTO_SUA}
                                   ,FieldByName( 'CB_PATRON' ).AsString
{$endif}
                                   );
     end;
end;

{*********************** TZetaINFOCredito ************************}
type
  TZetaINFOCredito = class( TZetaRegresa )
  private
    FFechaBimestre : TDate;
    FQuery : TZetaCursor;
    oTIMSS : TIMSS;
    function GetSalBimestre :TPesos;
    procedure PreparaQuery;
  protected
    FSeguroVivienda: TPesos;
    FPago: TPesos;
    FOffSet: integer;
    property FechaBimestre: TDate read FFechaBimestre;

    // DES US #13101 Infonavit - Descuento tipo # veces UMA...(Funciones)
    // Agregar par�metro nUMA
    function INFOCredito( const nTipo : eTipoInfonavit;
                          const nMonto, nDiasIvcm, nSalBase, nImssAjusta : TPesos;
                          const dLey : TDate; const nSMGDF, nDiasPer, nFactor, nUMA, nFactorDescuento : TPesos ) : TPesos;

  public
    constructor Create(oEvaluador:TQrEvaluator);override;
    destructor  Destroy; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

constructor TZetaINFOCredito.Create(oEvaluador:TQrEvaluator);
begin
     FOffSet := 0;

     inherited Create(oEvaluador);
     with oZetaProvider.DatosPeriodo do
          if TheMonth( Inicio ) = Mes then
             FFechaBimestre := Inicio
          else FFechaBimestre := Fin;

     FFechaBimestre := FirstDayOfMonth(FFechaBimestre);

     if ( TheMonth(FFechaBimestre) MOD 2 ) = 0 then
        FFechaBimestre := FirstDayOfMonth(FFechaBimestre-1);

     oTIMSS := TIMSS.Create( ZetaEvaluador.oZetaCreator ); //acl

     oTIMSS.EvaluarIMSSPagosBegin( TRUE, FALSE, FSeguroVivienda, TheYear(FFechaBimestre), TheMonth(FFechaBimestre) );    //acl
     PreparaQuery;
end;

destructor TZetaINFOCredito.Destroy;
begin
     FreeAndNil(FQuery);
     oTIMSS.EvaluarIMSSPagosEnd;
     FreeAndNil( oTIMSS );
     inherited destroy;
end;

procedure TZetaINFOCredito.PreparaQuery;
const
     K_QUERY_SAL_BIM = 'select CB_SAL_INT from SP_FECHA_KARDEX( :Fecha, :Empleado )';
     {$ifdef MSSQL}
     K_LEE_TASA_INFONAVIT = 'select DBO.SP_DISM_TASA_INFONAVIT(:CB_CODIGO,:FECHA) AS CB_INFTASA';
     {$else}
     K_LEE_TASA_INFONAVIT = 'select TASANUEVA CB_INFTASA from SP_DISM_TASA_INFONAVIT(:CB_CODIGO,:FECHA)';
     {$endif}
begin
     if ( FQuery = NIL ) then
        FQuery := oZetaProvider.CreateQuery( K_QUERY_SAL_BIM );
end;

function TZetaINFOCredito.GetSalBimestre :TPesos;
begin
     with FQuery do
     begin
          Active := FALSE;
          with oZetaProvider do
          begin
               ParamAsDate( FQuery, 'Fecha', FFechaBimestre );
               ParamAsInteger( FQuery, 'Empleado', ZetaEvaluador.GetEmpleado );
          end;
          Active := TRUE;
          Result := Fields[0].AsFloat;
          Active := False;
     end;
end;

procedure TZetaINFOCredito.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;

     with ZetaEvaluador.oZetaCreator do
     begin
          PreparaSalMin;
          oZetaProvider.GetDatosPeriodo;
          oZetaProvider.InitGlobales;
     end;

     AgregaParam( 'DIAS_IMSS' );
     AgregaParam( 'SAL_INT' );
     AgregaParam( 'DIAS_VACA' );
     AgregaParam( 'DIAS_AJUST' );
{$ifdef IMSS_VACA}
     AgregaParam( 'DIAS_VTOT' );
     AgregaParam( 'DIAS_VCAL' );
{$endif}

     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_INFTIPO', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_INFTASA', enEmpleado );
     //AgregaRequerido( 'COLABORA.CB_INF_OLD', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_INF_INI', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_INFCRED', enEmpleado );

     AgregaRequerido( 'COLABORA.CB_SEGSOC', enEmpleado );//acl
     AgregaRequerido( 'COLABORA.CB_PATRON', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_FEC_BSS', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_SAL_INT', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_FEC_INT', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_TURNO', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_INF_ANT', enEmpleado );

     AgregaRequerido( 'COLABORA.CB_INFDISM', enEmpleado );
     AgregaRequerido( 'NOMINA.NO_LIQUIDA', enNomina );
     AgregaRequerido( 'PERIODO.PE_DIA_MES', enPeriodo );

end;

// DES US #13101 Infonavit - Descuento tipo # veces UMA...(Funciones)
// Agregar nUMA a los par�metros.
function TZetaINFOCredito.INFOCredito( const nTipo : eTipoInfonavit; const nMonto, nDiasIvcm, nSalBase, nImssAjusta : TPesos;
                                       const dLey : TDate; const nSMGDF, nDiasPer, nFactor, nUMA, nFactorDescuento : TPesos ) : TPesos;
begin
     if ZetaEvaluador.Rastreando then
        ZetaEvaluador.Rastreador.RastreoHeader( 'RASTREO DE AMORTIZACION INFONAVIT' );
     FPago := nMonto;
     case nTipo of
          tiPorcentaje: Result := CalcCuota( ZetaEvaluador, 0, nMonto, nSalBase, nDiasIVCM, 0, 'Amortizaci�n INFONAVIT' );
          tiCuotaFija:
          begin
               Result := nMonto * nFactor;
               if ZetaEvaluador.Rastreando then
                 with ZetaEvaluador.Rastreador do
                 begin
                      RastreoPesos( '    D�as del Per�odo      :', nDiasPer );
                      RastreoEspacio;
                      RastreoPesos( '    Monto Fijo Mensual    :', nMonto );
                      RastreoFactor('(x) Factor del Per�odo    :', nFactor );
                      RastreoPesos( '(=) Monto Amortizaci�n    :', Result );
                 end;
          end;
          tiVeces:
          begin
               // Result := nMonto * nSMGDF * nFactor;

               // DES US #13101 Infonavit - Descuento tipo # veces UMA...(Funciones)
               // Modificar la l�gica de credito en veces SMGDF para utilizar
               // el monto menor entre SMGDF y UMA. �No modificar el rastreo?

               if nFactorDescuento = 0 then
               begin
                    if (nUMA <> 0) and (nUMA < nSMGDF) then
                       FPago := nMonto * nUMA
                    else
                        FPago := nMonto * nSMGDF;
                    end
               else
               begin
                    FPago := nMonto * nFactorDescuento;
               end;

               Result := FPago * nFactor;
               if ZetaEvaluador.Rastreando then
                 with ZetaEvaluador.Rastreador do
                 begin
                      RastreoPesos(  '    D�as del Per�odo      :', nDiasPer );
                      RastreoEspacio;
                      RastreoPesos(  '    # Veces el SMGDF      :', nMonto );


                      if nFactorDescuento = 0 then
                      begin
                           // DES US #13101 Infonavit - Descuento tipo # veces UMA...(Funciones)
                           if (nUMA <> 0) and (nUMA < nSMGDF) then
                              RastreoPesos(  '(x) Valor UMA             :', nUMA  )
                           else
                               RastreoPesos(  '(x) Salario M�nimo GDF    :', nSMGDF  );
                           end
                      else
                      begin
                           RastreoPesos(  '(x) Factor de descuento   :', nFactorDescuento  );
                      end;

                      RastreoFactor( '(x) Factor del Per�odo    :', nFactor );
                      RastreoPesos(  '(=) Monto Amortizaci�n    :', Result );
                 end;
          end;
          // DES US #13101 Infonavit - Descuento tipo # veces UMA...(Funciones)
          tiVecesUMA:
          begin
               if nFactorDescuento = 0 then
                  FPago := nMonto * nUMA
               else
                   FPago := nMonto * nFactorDescuento;

               Result := FPago * nFactor;
               if ZetaEvaluador.Rastreando then
                 with ZetaEvaluador.Rastreador do
                 begin
                      RastreoPesos(  '    D�as del Per�odo      :', nDiasPer );
                      RastreoEspacio;
                      RastreoPesos(  '    # Veces UMA           :', nMonto );
                      if nFactorDescuento = 0 then
                         RastreoPesos(  '(x) Valor UMA             :', nUMA  )
                      else
                          RastreoPesos( '(x) Factor de descuento   :', nFactorDescuento  );
                      RastreoFactor( '(x) Factor del Per�odo    :', nFactor );
                      RastreoPesos(  '(=) Monto Amortizaci�n    :', Result );
                 end;
          end;
     else
         Result := 0;
     end;
end;

function TZetaINFOCredito.Calculate: TQREvResult;
var
   eTipo : eTipoInfonavit;
   rDiasImss, rDiasPer , rDiasMes, rSalBase, rImssAjusta, rTasaInfonavit: TPesos;
   dLey, dEmpFinal : TDate;
begin
{
     Se puede optimizar: Dependiendo del 'iTipo',
     s�lo se requieren algunos par�metros.
     No tiene caso hacer el trabajo de todos los DEFAULTS
}
     with DataSetEvaluador, Result do
     begin
          { Se evaluan solamente aquellos empleados que tengan cr�dito de infonavit}
          if ( FieldByName('CB_INFTIPO').AsInteger <> Ord( tiNoTiene ) ) and
               StrLleno( FieldByName('CB_INFCRED').AsString ) and ( FieldByName('CB_INFTASA').AsFloat <> 0 )  then
          begin
               oTIMSS.SetGlobalEmpleadoCursor( DataSetEvaluador, oZetaProvider.DatosPeriodo.Fin );
               if ParamVacio( FOffSet + 0 ) then
                    eTipo := eTipoInfonavit( oTIMSS.Empleado.PrestamoTipo )
               else
                   eTipo := eTipoInfonavit( ParamInteger( FOffSet + 0 ) );

               if ParamVacio( FOffSet + 3 ) then
               begin
                    if ( ( eTipo = tiPorcentaje ) and oTIMSS.Empleado.AplicaDisminucion ) then
                       rSalBase := GetSalBimestre
                    else rSalBase := FieldByName('SAL_INT').AsFloat;
               end
               else rSalBase := ParamFloat( FOffSet + 3 );

               rImssAjusta := DefaultFloat( FOffSet + 4, GetIMSSAjust( DataSetEvaluador{$ifdef IMSS_VACA}, oZetaProvider.GetGlobalBooleano( K_GLOBAL_DIAS_COTIZADOS_FV ){$endif} ) );
               dLey := DefaultDate(  FOffSet + 5, oZetaProvider.DatosPeriodo.Fin );
               rDiasPer := DefaultFloat(  FOffSet + 7, oZetaProvider.DatosPeriodo.Dias );
               { pendiente : rDiasIVCM  No contempla inicio de Pr�stamo a media
                             semana como la 2.51. No tenemos en  COLABORA la fecha de inicio del PRESTAMO !  }

               with oTIMSS do
               begin
                    if ( Empleado.FechaBSS > oZetaProvider.DatosPeriodo.Inicio ) and
                       ( Empleado.FechaBSS < oZetaProvider.DatosPeriodo.Fin ) then
                    begin
                         dEmpFinal:= Empleado.FechaBSS;
                    end
                    else
                        dEmpFinal:= oZetaProvider.DatosPeriodo.Fin;

                    if ParamVacio( FOffSet + 2 ) then //Se restan los d�as de suspensi�n y baja
                    begin
                         rDiasImss := FieldByName( 'DIAS_IMSS' ).AsFloat + rImssAjusta ; // rDiasIVCM
                         if (Empleado.PrestamoInicio > oZetaProvider.DatosPeriodo.Inicio) then
                            rDiasImss := rDiasImss - rMax(Empleado.PrestamoInicio - oZetaProvider.DatosPeriodo.Inicio,0);

                         if ( Empleado.FechaSuspension >= oZetaProvider.DatosPeriodo.Inicio ) and
                            ( Empleado.FechaSuspension < oZetaProvider.DatosPeriodo.Fin ) then
                         begin
                              rDiasImss:= rMax( rDiasImss - rMax( dEmpFinal - Empleado.FechaSuspension, 0), 0 );
                         end;
                    end
                    else rDiasImss := ParamFloat( FOffSet + 2 );
               end;

               rDiasMes := FieldByName('PE_DIA_MES').AsInteger;
               if rDiasMes = 0 then
                  rDiasMes := K_FACTOR_MENSUAL;

              {  Se Comentario porque no se usa.
              if ( ( eTipo = tiPorcentaje ) and zStrToBool( FieldByName('CB_INFDISM').AsString ) ) then
                  rTasaInfonavit:= GetTasaInfonavit
               else                                 }
               rTasaInfonavit:= oTIMSS.Empleado.Prestamo;//acl

               // Para cubrir la suspensi�n a medio periodo y tipo credito: cuota fija y veces
               if (oTIMSS.Empleado.FechaSuspension <> NullDateTime ) and ( oTIMSS.Empleado.FechaSuspension < oZetaProvider.DatosPeriodo.Inicio ) then
                   eTipo:= tiNotiene;

               Kind := resDouble;
               dblResult := INFOCredito( eTipo,
                                         DefaultFloat( FOffSet + 1, rTasaInfonavit ), // rMonto
                                         rDiasImss, // rDiasIVCM
                                         rSalBase , // rSalBase
                                         rImssAjusta,
                                         dLey,
                                         DefaultFloat( FOffSet + 6, ZetaEvaluador.oZetaCreator.dmSalMin.GetSMGDF( dLey ) ), // rSMGDF
                                         rDiasPer,
                                         {CV: Asi estaba hasta la version 2.3.96
                                         DefaultTasa( 8, rDiasPer / K_FACTOR_MENSUAL )
                                         }
                                         DefaultTasa( FOffSet + 8, rDiasPer / rDiasMes )
                                         // DES US #13101 Infonavit - Descuento tipo # veces UMA...(Funciones).
                                         , DefaultFloat( FOffSet + 6, ZetaEvaluador.oZetaCreator.dmValUma.GetVUDefaultDiario ( dLey ) ) // rValUMA
                                         , DefaultFloat( FOffSet + 6, ZetaEvaluador.oZetaCreator.dmValUma.GetFactorDescuento ( dLey ) )
                                         );// Factor
          end
          else
          begin
               Kind := resDouble;
               dblResult := 0;
          end;
     end;
end;

{*********************** TZetaINFOPatron ************************}
type
  TZetaINFOPatron = class( TZetaRegresa )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

procedure TZetaINFOPatron.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;

     with ZetaEvaluador.oZetaCreator do
     begin
          PreparaPrimasIMSS;
          PreparaSalMin;
          {$ifdef IMSS_UMA}
          PreparaValUma;
          {$endif}
          oZetaProvider.GetDatosPeriodo;
          oZetaProvider.InitGlobales;
     end;

     AgregaParam( 'DIAS_INCA' );
     AgregaParam( 'DIAS_IMSS' );
     AgregaParam( 'DIAS_VACA' );
     AgregaParam( 'SAL_INT' );
     AgregaParam( 'DIAS_AJUST' );
{$ifdef IMSS_VACA}
     AgregaParam( 'DIAS_VTOT' );
     AgregaParam( 'DIAS_VCAL' );
{$endif}
     AgregaRequerido( 'NOMINA.NO_LIQUIDA', enNomina );
{$ifdef DESCUENTO_SUA}
     AgregaRequerido( 'NOMINA.CB_PATRON', enNomina );
{$endif}
end;

function INFOPatron( oEvaluador : TZetaEvaluador; const nDiasIvcm, nSalBase, nImssAjusta, nDiasInca : TPesos;
                     const dLey : TDate; const nSMGDF : TPesos {$ifdef DESCUENTO_SUA}; const sPatron: String {$endif} ) : TPesos;
var
   oPrimas : TPrimasIMSS;
begin
     if oEvaluador.Rastreando then
       with oEvaluador.Rastreador do
       begin
            RastreoHeader( 'RASTREO DE INFONAVIT PATRONAL' );
            {$ifdef IMSS_UMA}
            RastreoPesos(  'Unidad de Medida y Act. (UMA)..:', nSMGDF );   // Se remplaza en la llamada el valor de nSMGDF con el valor de UMA
            {$else}
            RastreoPesos( 'Salario M�nimo General del D.F.: ', nSMGDF );
            {$endif}
       end;

     oPrimas := oEvaluador.oZetaCreator.dmPrimasIMSS.GetPrimasIMSS( dLey {$ifdef DESCUENTO_SUA}, sPatron {$endif} );
     Result  := CalcCuota( oEvaluador, oPrimas.SS_L_INFO, oPrimas.SS_P_INFO, nSalBase, nDiasIVCM, nSMGDF, 'INFONAVIT Patronal' );
end;

function TZetaINFOPatron.Calculate: TQREvResult;
var
   rDiasInca, rImssAjusta: TPesos;
   dLey: TDate;
begin
     with Result, DataSetEvaluador do
     begin
          rImssAjusta := DefaultFloat( 2, GetIMSSAjust( DataSetEvaluador{$ifdef IMSS_VACA}, oZetaProvider.GetGlobalBooleano( K_GLOBAL_DIAS_COTIZADOS_FV ){$endif} ) );
          rDiasInca := DefaultFloat( 3, FieldByName( 'DIAS_INCA' ).AsFloat );
          dLey := DefaultDate(  4, oZetaProvider.DatosPeriodo.Fin );

          Kind := resDouble;
          dblResult := INFOPatron( ZetaEvaluador,
                                   DefaultFloat( 0, FieldByName( 'DIAS_IMSS' ).AsFloat + rImssAjusta + rDiasInca ) , // rDiasIVCM
                                   DefaultFloat( 1, FieldByName( 'SAL_INT' ).AsFloat ) , // rSalBase
                                   rImssAjusta,
                                   rDiasInca,
                                   dLey,
                                   {$ifdef IMSS_UMA}
                                   DefaultFloat( 5, ZetaEvaluador.oZetaCreator.dmValUma.GetVUDefaultDiario( dLey ) )
                                   {$else}
                                   DefaultFloat( 5, ZetaEvaluador.oZetaCreator.dmSalMin.GetSMGDF( dLey ) )
                                   {$endif}
{$ifdef DESCUENTO_SUA}
                                   ,FieldByName( 'CB_PATRON' ).AsString
{$endif}
                                    ); // rSMGDF
     end;
end;

{*********************** TZetaIMSSAjust ************************}
type
  TZetaIMSSAjust = class( TZetaRegresa )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

procedure TZetaIMSSAjust.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
     oZetaProvider.InitGlobales;
     AgregaParam( 'DIAS_VACA' );
     AgregaParam( 'DIAS_AJUST' );
{$ifdef IMSS_VACA}
     AgregaParam( 'DIAS_VTOT' );
     AgregaParam( 'DIAS_VCAL' );
{$endif}
     AgregaRequerido( 'NOMINA.NO_LIQUIDA', enNomina );
end;

function TZetaIMSSAjust.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resDouble;
          dblResult := GetIMSSAjust( DataSetEvaluador{$ifdef IMSS_VACA}, oZetaProvider.GetGlobalBooleano( K_GLOBAL_DIAS_COTIZADOS_FV ){$endif} );
     end;
end;

{*********************** TZetaIMSS_EXTRAS ************************}
type
  TZetaIMSS_EXTRAS = class ( TZetaRegresa )
  private
   FQuery : TZetaCursor;
   procedure PreparaQuery( const Year, Tipo, Numero : Integer );
  public
   procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
   function Calculate: TQREvResult; override;
  end;

procedure TZetaIMSS_EXTRAS.PreparaQuery( const Year, Tipo, Numero : Integer );
begin
     if ( FQuery = nil ) then
        FQuery := oZetaProvider.CreateQuery( QRY_EXENTAS );
     with oZetaProvider do
     begin
          ParamAsInteger( FQuery, 'Year', Year );
          ParamAsInteger( FQuery, 'Tipo', Tipo );
          ParamAsInteger( FQuery, 'Numero', Numero );
     end;
end;

procedure TZetaIMSS_EXTRAS.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
     with oZetaProvider do
          InitGlobales;
     AgregaParam( 'SAL_HORA' );
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     AgregaRequerido( 'NOMINA.NO_USER_RJ', enNomina );
     AgregaRequerido( 'NOMINA.NO_EXENTAS', enNomina );
     AgregaRequerido( 'NOMINA.NO_DOBLES', enNomina );
     AgregaRequerido( 'NOMINA.NO_TRIPLES', enNomina );
     AgregaRequerido( 'NOMINA.PE_YEAR', enNomina );
     AgregaRequerido( 'NOMINA.PE_TIPO', enNomina );
     AgregaRequerido( 'NOMINA.PE_NUMERO', enNomina );
{$ifdef CAMBIO_TNOM}
     AgregaRequerido( 'NOMINA.NO_ASI_INI', enNomina );
     AgregaRequerido( 'NOMINA.NO_ASI_FIN', enNomina );
{$endif}
{$ifdef QUINCENALES}
     AgregaRequerido( 'PERIODO.PE_ASI_INI', enPeriodo );
     AgregaRequerido( 'PERIODO.PE_ASI_FIN', enPeriodo );
{$else}
     AgregaRequerido( 'PERIODO.PE_FEC_INI', enPeriodo );
     AgregaRequerido( 'PERIODO.PE_FEC_FIN', enPeriodo );
{$endif}
end;

function TZetaIMSS_EXTRAS.Calculate: TQREvResult;
 var lReloj: Boolean;
     nDoble, nTriple, nExentas, nSalDoble: Double;
begin
     with DataSetEvaluador do
     begin
          lReloj := DefaultBoolean( 0, FieldByName( 'NO_USER_RJ' ).AsInteger > 0 );
          if ( ZetaEvaluador.CalculandoNomina ) then
          begin
               with TCalcNomina(ZetaEvaluador.CalcNomina) do
               begin
                    nDoble  := DefaultFloat( 1, ConceptoCalculado( 3 ) );
                    nTriple := DefaultFloat( 2, ConceptoCalculado( 4 ) );
               end;
          end
          else
          begin
               if ParamVacio( 1 ) then
                  nDoble  := FieldByName( 'NO_DOBLES' ).AsFloat * FieldByName( 'SAL_HORA' ).AsFloat * 2
               else
                   nDoble := ParamFloat( 1 );
               if ParamVacio( 2 ) then
                  nTriple := FieldByName( 'NO_TRIPLES' ).AsFloat * FieldByName( 'SAL_HORA' ).AsFloat * 3
               else
                   nTriple:= ParamFloat( 2 );
          end;
          with Result do
          begin
               Kind:= resDouble;
               if ( nDoble = 0 ) and ( nTriple = 0 ) then
                  dblResult := 0
               else
               begin
                    if not ParamVacio( 3 ) then
                       nExentas := ParamFloat( 3 )
                    else if lReloj then
                       nExentas := FieldByName( 'NO_EXENTAS' ).AsFloat
                    else
                    begin
                         PreparaQuery( FieldByName( 'PE_YEAR' ).AsInteger, FieldByName( 'PE_TIPO' ).AsInteger,
                                       FieldByName( 'PE_NUMERO' ).AsInteger );
                         nExentas := CalculaHorasExentas( oZetaProvider, FQuery,
                                                 ZetaEvaluador.GetEmpleado,
                                                 K_DEFAULT_TOPE, K_DEFAULT_TOPE,
                                                 K_DEFAULT_TOPE_DOBLES,
                                                 {$ifdef QUINCENALES}
                                                 FieldByName( 'PE_ASI_INI' ).AsDateTime,
                                                 FieldByName( 'PE_ASI_FIN' ).AsDateTime );
                                                 {$else}
                                                 FieldByName( 'PE_FEC_INI' ).AsDateTime,
                                                 FieldByName( 'PE_FEC_FIN' ).AsDateTime );
                                                 {$endif}
                    end;
                    nSalDoble := DefaultFloat( 4, FieldByName( 'SAL_HORA' ).AsFloat * 2 );
                    dblResult := Redondea( rMax( nDoble + nTriple - nExentas * nSalDoble, 0 ));
                    with ZetaEvaluador, Rastreador do
                        if Rastreando then
                        begin
                            RastreoHeader( 'RASTREO DE EXTRAS Gravadas para IMSS' );
                            RastreoPesos( '                     Horas Exentas:', nExentas );
                            RastreoPesos( '        (x) Salario por Hora Doble:', nSalDoble );
                            RastreoPesos( '        (=) Monto Exento para IMSS:', nExentas*nSalDoble );
                            RastreoEspacio;
                            RastreoPesos( '     Monto Pagado por Tiempo Doble:', nDoble );
                            RastreoPesos( '(+) Monto Pagado por Tiempo Triple:', nTriple );
                            RastreoPesos( '(-)         Monto Exento para IMSS:', nExentas*nSalDoble );
                            RastreoPesos( '(=)        Monto Gravado para IMSS:', dblResult );
                        end;
               end;
          end;
     end;
end;

{ ****************** TZetaSalMin **************************}
type
  TZetaSalMin = class( TZetaRegresa )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

procedure TZetaSalMin.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
     if ( ArgList.Count > 0 ) or ( not ZetaEvaluador.CalculandoNomina ) then
     begin
          ZetaEvaluador.oZetaCreator.PreparaSalMin;

          if ParamVacio( 1 ) then
             AgregaRequerido( 'COLABORA.CB_ZONA_GE', enEmpleado );
     end;
end;

function TZetaSalMin.Calculate: TQREvResult; // Sustituye la variable SAL_MIN de TRESS //
var
   sZona : String;
begin
     Result.Kind := resDouble;
     if ( ArgList.Count = 0 ) and ZetaEvaluador.CalculandoNomina then
        Result.dblResult := TCalcNomina(ZetaEvaluador.CalcNomina).nEmpleadoSalMin
     else
     begin
         // PENDIENTE
         if ParamVacio( 1 ) then
             sZona := DataSetEvaluador.FieldByName( 'CB_ZONA_GE' ).AsString
         else
             sZona := ParamString( 1 );
         Result.dblResult := ZetaEvaluador.oZetaCreator.dmSalMin.GetSalMin( DefaultDate( 0, Date ), sZona );
     end;
end;


{ ****************** TZetaValUma **************************}
type
  TZetaValUma = class( TZetaRegresa )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

procedure TZetaValUma.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
     ZetaEvaluador.oZetaCreator.PreparaValUma;
end;

function TZetaValUma.Calculate: TQREvResult;
var
   dFecha : TDateTime;
begin
     Result.Kind := resDouble;
     if ZetaEvaluador.CalculandoNomina then
     begin
          dFecha := DefaultDate( 0, oZetaProvider.DatosPeriodo.Fin );
     end
     else
     begin
          dFecha := DefaultDate( 0, Date );
     end;
     Result.dblResult := ZetaEvaluador.oZetaCreator.dmValUma.GetValUma( dFecha, DefaultInteger( 1, 0 ) );
end;

{*** US 12365: Timbrado/Declaraciones : Mejorar calculo de exentos en n�mina ***}
{ *************  Prueba de Concepto  TZetaEC **************}
type
  TZetaEC = class( TZetaRegresaFloat )
  public
    { Public Declarations }
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

procedure TZetaEC.GetRequeridos( var TipoRegresa : TQREvResultType );
   function EsInteger( const iParam: Word ): Integer;
   begin
        Result := 0;
        with Argument( iParam ) do
        begin
             case Kind of
                  resInt: Result := intResult;
             else
                 ErrorParametroTipo( iParam, 'No es un n�mero entero' );
             end;
        end;
   end;
begin
     inherited;
     if not ZetaEvaluador.CalculandoNomina then
        ErrorFuncion( 'Solamente se puede utilizar en c�lculo de n�mina', 'Funci�n: ' + Expresion );
     TipoRegresa := resDouble;
     ParametrosFijos( 1 );
     EsInteger( 0 );
end;

function TZetaEC.Calculate: TQREvResult;
begin
     Result.Kind      := resDouble;
     Result.dblResult := TCalcNomina(ZetaEvaluador.CalcNomina).ConceptoCalculadoEx( ParamInteger( 0 ));
end;

{ *************  Calculo de Exento  TZetaCalExento **************}
type
  TZetaCalExento = class( TZetaRegresaFloat )
  public
    { Public Declarations }
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

procedure TZetaCalExento.GetRequeridos( var TipoRegresa : TQREvResultType );
   function EsInteger( const iParam: Word ): Integer;
   begin
        Result := 0;
        with Argument( iParam ) do
        begin
             case Kind of
                  resInt: Result := intResult;
             else
                 ErrorParametroTipo( iParam, 'No es un n�mero entero' );
             end;
        end;
   end;
begin
     inherited;
     if not ZetaEvaluador.CalculandoNomina then
        ErrorFuncion( 'Solamente se puede utilizar en c�lculo de n�mina', 'Funci�n: ' + Expresion );
     TipoRegresa := resDouble;
     ParametrosFijos( 2 );
     EsInteger( 0 );
     ParamFloat( 1 );
end;

function TZetaCalExento.Calculate: TQREvResult;
begin
     Result.Kind      := resDouble;
     Result.dblResult := TCalcNomina(ZetaEvaluador.CalcNomina).CalExento( ParamInteger( 0 ), ParamFloat( 1 ) );
end;

{ ****************** TZetaIMSSVARIABLE **************************}

type
  TZetaIMSSVARIABLE = class( TZetaSFuncion )
    FDetalleCalculo : TStringList;
  public
    procedure PreparaAgente; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
    destructor Destroy;override;
    constructor Create(oEvaluador : TQREvaluator); override;
  protected
     function GetCampo(var iMes, iYear: Integer): String; virtual;
end;

constructor TZetaIMSSVARIABLE.Create(oEvaluador : TQREvaluator);
begin
     inherited Create( oEvaluador );
     FDetalleCalculo := TStringList.Create;
end;

destructor TZetaIMSSVARIABLE.Destroy;
begin
     FreeAndNil( FDetalleCalculo );
     inherited Destroy;
end;

procedure TZetaIMSSVARIABLE.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
     with oZetaProvider do
     begin
          GetDatosActivos;
          GetDatosPeriodo;
          InitGlobales;
     end;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

procedure TZetaIMSSVARIABLE.PreparaAgente;

   function GetConceptosSistema: String;
   begin
{$ifdef IMSS_VACA}
        if oZetaProvider.GetGlobalBooleano( K_GLOBAL_DIAS_COTIZADOS_FV ) then
           Result := '1117'
        else
{$endif}
            Result := '1113,1117';
   end;

begin
     with FAgente do
     begin
          AgregaColumna( 'ACUMULA.CO_NUMERO', TRUE, enAcumula, tgNumero, 0, 'CO_NUMERO' );
          AgregaColumna( 'CO_DESCRIP', TRUE, enConcepto, tgTexto, 30, 'CO_DESCRIP' );
          AgregaFiltro( 'ACUMULA.CB_CODIGO = :Empleado', TRUE, enAcumula );
          AgregaFiltro( 'ACUMULA.AC_YEAR = :Year', TRUE, enAcumula );
          AgregaFiltro( 'ACUMULA.CO_NUMERO in ( select CO_NUMERO from CONCEPTO where CO_G_IMSS = ''S'' ) OR ' +
                        Format( 'ACUMULA.CO_NUMERO in ( %s )', [ GetConceptosSistema ] ), TRUE, enAcumula );
     end;
end;

function TZetaIMSSVARIABLE.GetCampo(var iMes, iYear: Integer):String;
var
   iBimestre: Integer;
begin
     if iYear < K_YEAR_REFORMA then
     begin
          if iMes < 10 then
             Result:= 'AC_MES_' + '0' + IntToStr( iMes )
          else if iMes < 14 then
             Result:= 'AC_MES_' + IntToStr( iMes )
          else
             ErrorFuncion( 'Error en Par�metro de Mes', 'Valores v�lidos 1 a 13' );
     end
     else
     begin
          if iMes < 13 then
          begin
               iBimestre := NumeroBimestre(iMes)*2;
               Result:= Format( 'AC_MES_%2.2d + AC_MES_%2.2d',
                                [ iBimestre - 1 , iBimestre ]);
          end
          else if iMes = 13 then
               Result:= 'AC_MES_13'
          else
              ErrorFuncion( 'Error en Par�metro de Mes', 'Valores v�lidos 1 a 13' );
     end;
end;

function TZetaIMSSVARIABLE.Calculate: TQREvResult;
var
   sCampo: String;
   iYear, iMes: Integer;
   rSuma, rPromedio, rDias : Double;
   sDetalle: String;
   lPrimero,lCalculaDias : Boolean;
begin

     iYear:= DefaultInteger( 2, oZetaProvider.YearDefault );
     iMes:= Abs( DefaultInteger( 1, oZetaProvider.MesDefault ) );

     {CV-17-Enero-2002: debido a los cambios de la Reforma Fiscal, si el a�o default
     es del 2001 o anterior, el c�lculo se realizar� de manera mensual,
     a partir del a�o 2002, el calculo se realiza de manera bimestral en base a que
     bimestre corresponde el mes que se pide}
     {
     if iYear < K_YEAR_REFORMA then
     begin
          if iMes < 10 then
             sCampo:= 'AC_MES_' + '0' + IntToStr( iMes )
          else if iMes < 14 then
             sCampo:= 'AC_MES_' + IntToStr( iMes )
          else
             ErrorFuncion( 'Error en Par�metro de Mes', 'Valores v�lidos 1 a 13' );
     end
     else
     begin
          if iMes < 13 then
          begin
               iBimestre := NumeroBimestre(iMes)*2;
               sCampo:= Format( 'AC_MES_%2.2d + AC_MES_%2.2d',
                                [ iBimestre - 1 , iBimestre ]);
          end
          else if iMes = 13 then
               sCampo:= 'AC_MES_13'
          else
              ErrorFuncion( 'Error en Par�metro de Mes', 'Valores v�lidos 1 a 13' );
     end;   }
     sCampo:= GetCampo(iMes, iYear); //Se cambi� para que compartir l�gica con la nueva funci�n de TZetaRIMSSVARIABLE

     lCalculaDias := ParamVacio(0);
     if lCalculaDias then
        rDias := 0
     else
         rDias:= ParamFloat( 0 );

     //iDias:= DefaultInteger( 0, DiasEnMes( iYear, iMes ) );

     PreparaSuperEv( sCampo, VACIO, enAcumula, [tgFloat] );
     CierraDataset;

     SetParamValue( 'Empleado', tgNumero, ZetaEvaluador.GetEmpleado );
     SetParamValue( 'Year', tgNumero, iYear );

     AbreDataSet;

     // RASTREO
     with SuperEv.DataSet do
     begin
          rSuma:= 0;
          lPrimero:= TRUE;
          with ZetaEvaluador do
          begin
               if oZetaCreator.Promediando then
               begin
                    with Rastreador do
                    begin
                         RastreoBegin;
                         RastreoHeader( 'Rastreo de Percepciones Variables ( Funci�n IMSSVARIABLE() )' );
                         RastreoEspacio;
                    end;
               end;
          end;
          FDetalleCalculo.Clear;
          while not EOF do
          begin
               if (FieldByName( 'CO_NUMERO' ).AsInteger <= K_MAX_CONCEPTO) then
               begin

                    rSuma:= rSuma + SuperEv.Value.dblResult;
                    with ZetaEvaluador do
                    begin
                         if oZetaCreator.Promediando then
                         begin
                              if not lPrimero then
                                 sDetalle:= '(+) ' + '[' + IntToStr( FieldByName( 'CO_NUMERO' ).AsInteger ) + '] ' + FieldByName( 'CO_DESCRIP' ).AsString
                              else
                                 sDetalle:= '    ' + '[' + IntToStr( FieldByName( 'CO_NUMERO' ).AsInteger ) + '] ' + FieldByName( 'CO_DESCRIP' ).AsString;
                              lPrimero:= FALSE;
                              Rastreador.RastreoPesos( PadR( sDetalle, 38 ) + ' : ', SuperEv.Value.dblResult );
                              FDetalleCalculo.Add( IntToStr( FieldByName( 'CO_NUMERO' ).AsInteger ) + '='+ FloatToStr( Redondea( SuperEv.Value.dblResult ) ) );
                         end;
                    end;
               end
               else
               begin
                    if lCalculaDias then
                       rDias:= rDias + SuperEv.Value.dblResult;
               end;
               Next;
          end;

          //if iDias = 0 then //se quito el if porque cambio el tipo de iDias de Entero a Flotante
          if PesosIguales( rDias, 0 ) then
             rPromedio:= 0
          else
          begin
               if rSuma = 0 then
                  rPromedio:= 0
               else
                  rPromedio := Redondea( rSuma / rDias );
          end;
          with ZetaEvaluador do
          begin
               if oZetaCreator.Promediando then
               begin
                    with Rastreador do
                    begin
                         RastreoEspacio;
                         RastreoPesos( '(=) TOTAL DE PERCEPCIONES VARIABLES    : ', rSuma );
                         RastreoEspacio;
                         RastreoPesos( '(/) DIAS a Promediar                   : ', rDias );
                         RastreoEspacio;
                         RastreoPesos( '(=) PROMEDIO DE PERCEPCIONES VARIABLES : ', rPromedio );
                    end;
                    with TdmSuperReporte( oZetaCreator.SuperReporte ).DataSetReporte do
                    begin
                         FieldByName( 'SUMA_PERC' ).AsFloat := rSuma;
                         FieldByName( 'DIAS_PERC' ).AsFloat := rDias;
                         FieldByName( 'DATOS_PERC' ).AsString := FDetalleCalculo.Text;
                    end;
               end;
          end;
     end;
     Result.Kind := resDouble;
     Result.dblResult := rPromedio;
end;

{********************** TZetaRIMSSVARIABLE  ******************** }

type
    TZetaRIMSSVARIABLE  = class ( TZetaIMSSVARIABLE )
    public
          //procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    protected
          function GetCampo(var iMes, iYear: Integer): String; override;
    end;

function TZetaRIMSSVARIABLE.GetCampo(var iMes, iYear: Integer): String;
var
   sRazonSocial: String;
   iBimestre: Integer;
begin
     if ( iMes < 14 ) then
     begin
          iBimestre:= NumeroBimestre(iMes)*2;
          sRazonSocial:= DefaultString( 3, VACIO );
          Result:= Format( 'RAS( ACUMULA.CO_NUMERO, %d, %d, %s, ACUMULA.AC_YEAR,,,%s)',[ ( iBimestre - 1 ),
                                                                                           iBimestre,
                                                                                            EntreComillas( sRazonSocial ),
                                                                                            EntreComillas( DefaultString( 4, VACIO ) )]);
     end
     else
         ErrorFuncion( 'Error en Par�metro de Mes', 'Valores v�lidos 1 a 13' );

end;


{*********************** TZetaIMSS_EXENTAS *********************}

type
  TZetaIMSS_EXENTAS = class ( TZetaRegresa )
  private
    FQuery : TZetaCursor;
    procedure PreparaQuery;
  public
   procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
   function Calculate: TQREvResult; override;
  end;

procedure TZetaIMSS_EXENTAS.PreparaQuery;
begin
     if ( FQuery = nil ) then
     begin
          FQuery := oZetaProvider.CreateQuery( QRY_EXENTAS );
          with oZetaProvider.DatosPeriodo do
          begin
               oZetaProvider.ParamAsInteger( FQuery, 'Year', Year );
               oZetaProvider.ParamAsInteger( FQuery, 'Tipo', Ord( Tipo ) );
               oZetaProvider.ParamAsInteger( FQuery, 'Numero', Numero );
          end;
     end;
end;

procedure TZetaIMSS_EXENTAS.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
     with oZetaProvider do
     begin
          GetDatosPeriodo;
          InitGlobales;
     end;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

function TZetaIMSS_EXENTAS.Calculate: TQREvResult;
begin
     PreparaQuery;
     with Result do
     begin
          Kind:= resDouble;
          with oZetaProvider.DatosPeriodo do
               dblResult := CalculaHorasExentas( oZetaProvider, FQuery,
                                                 ZetaEvaluador.GetEmpleado,
                                                 DefaultInteger( 0, K_DEFAULT_TOPE ),
                                                 DefaultInteger( 1, K_DEFAULT_TOPE ),
                                                 DefaultInteger( 2, K_DEFAULT_TOPE_DOBLES ),
{$ifdef QUINCENALES}
                                                 InicioAsis, FinAsis );
{$else}
                                                 Inicio, Fin );
{$endif}
     end;
end;

{*********************** TZetaCALC_EXENTAS *********************}

type
  TZetaCALC_EXENTAS = class ( TZetaRegresa )
  private
    FQuery : TZetaCursor;
    FDiasAplicaExento: eDiasAplicaExento;
    procedure PreparaQuery;
    function  CalculaRegla3x3( const nCriterio : Integer ) : Currency;
  public
   procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
   function Calculate: TQREvResult; override;
  end;

procedure TZetaCALC_EXENTAS.PreparaQuery;
const
     QRY_EXTRAS =   'select AU_EXTRAS, AU_DOBLES, AU_FECHA, AU_STATUS,AU_TIPODIA ' +
                    'from AUSENCIA ' +
                    'where CB_CODIGO = :Empleado and AU_FECHA BETWEEN :FechaIni and :FechaFin and AU_EXTRAS > 0 ' +
                    'order by AU_FECHA';
begin
     if ( FQuery = nil ) then
     begin
          FQuery := oZetaProvider.CreateQuery( QRY_EXTRAS );
     end;
end;

procedure TZetaCALC_EXENTAS.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
     with oZetaProvider do
     begin
          GetDatosPeriodo;
          InitGlobales;
     end;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     AgregaRequerido( 'NOMINA.NO_EXTRAS', enNomina );
{$ifdef CAMBIO_TNOM}
     AgregaRequerido( 'NOMINA.NO_ASI_INI', enNomina );
     AgregaRequerido( 'NOMINA.NO_ASI_FIN', enNomina );
{$endif}
{$ifdef QUINCENALES}
     AgregaRequerido( 'PERIODO.PE_ASI_INI', enPeriodo );
     AgregaRequerido( 'PERIODO.PE_ASI_FIN', enPeriodo );
{$else}
     AgregaRequerido( 'PERIODO.PE_FEC_INI', enPeriodo );
     AgregaRequerido( 'PERIODO.PE_FEC_FIN', enPeriodo );
{$endif}
end;

function TZetaCALC_EXENTAS.Calculate: TQREvResult;
begin
     PreparaQuery;
     with Result do
     begin
          Kind:= resDouble;
          resFecha  := FALSE;
          dblResult := CalculaRegla3x3( DefaultInteger( 0, K_REGLA3X3_PROPORCIONAL ));
     end;
end;

function TZetaCALC_EXENTAS.CalculaRegla3x3( const nCriterio: Integer): Currency;
var
    dias_extras : Integer;
    h_dobles, h_extras, h_triples, h_dobles_3x3, h_triples_3x3, h_suma : TDiasHoras;
    StatusAusencia: eStatusAusencia;
    TipoDia: eTipoDiaAusencia;


    procedure LimiteSemanal;
    begin
        with ZetaEvaluador, Rastreador do
            if Rastreando and ( dias_extras > K_EXENTAS_SEMANAL ) then
                RastreoTexto( Format( 'D�a #%d, Excede L�mite Semanal:', [ dias_extras] ), '' );
    end;


begin  // CalculaRegla3x3
    Result := 0;
    h_extras := DataSetEvaluador.FieldByName( 'NO_EXTRAS' ).AsFloat;

    with ZetaEvaluador, Rastreador do
        if Rastreando then
        begin
            RastreoHeader( 'RASTREO DE CALCULO DE EXENTAS IMSS (Regla 3 x 3)' );
            RastreoPesos( 'Horas Extras de Pre-N�mina:', h_extras );
            RastreoTexto( 'Criterio de Regla 3 x 3:', '  ' + IntToStr( nCriterio ));
        end;

    if ( h_extras > 0 ) then
    begin
        dias_extras := 0;
        with FQuery, ZetaEvaluador, Rastreador do
        begin
            Active := FALSE;
            with oZetaProvider do
            begin
                 {$ifdef CAMBIO_TNOM}
                 ParamAsDate( FQuery, 'FechaIni', DataSetEvaluador.FieldByName( 'NO_ASI_INI' ).AsDateTime );
                 ParamAsDate( FQuery, 'FechaFin', DataSetEvaluador.FieldByName( 'NO_ASI_FIN' ).AsDateTime );
                 {$else}
                 {$ifdef QUINCENALES}
                 ParamAsDate( FQuery, 'FechaIni', DataSetEvaluador.FieldByName( 'PE_ASI_INI' ).AsDateTime );
                 ParamAsDate( FQuery, 'FechaFin', DataSetEvaluador.FieldByName( 'PE_ASI_FIN' ).AsDateTime );
                 {$else}
                 ParamAsDate( FQuery, 'FechaIni', DataSetEvaluador.FieldByName( 'PE_FEC_INI' ).AsDateTime );
                 ParamAsDate( FQuery, 'FechaFin', DataSetEvaluador.FieldByName( 'PE_FEC_FIN' ).AsDateTime );
                 {$endif}
                 {$endif}
                 ParamAsInteger( FQuery, 'Empleado', ZetaEvaluador.GetEmpleado );

                 FDiasAplicaExento := eDiasAplicaExento( GetGlobalInteger( K_GLOBAL_DIAS_APLICA_EXENTO) );
            end;
            Active := TRUE;
            while not Eof do
            begin
                h_dobles  := FieldByName( 'AU_DOBLES' ).AsFloat;
                h_extras  := FieldByName( 'AU_EXTRAS' ).AsFloat;
                StatusAusencia := eStatusAusencia( FieldByName( 'AU_STATUS' ).AsInteger );
                TipoDia := eTipoDiaAusencia( FieldByName( 'AU_TIPODIA' ).AsInteger );
                h_triples := h_extras - h_dobles;

                if Rastreando then
                begin
                    RastreoEspacio;
                    RastreoPesos( Format( 'Horas Extras del d�a %s:', [ FormatDateTime( 'dd/mmm/yy', FieldByName( 'AU_FECHA' ).AsDateTime ) ] ), h_extras );
                    RastreoPesos( 'Horas Dobles:', h_dobles );
                    RastreoPesos( 'Horas Triples:', h_triples );
                end;

                {cv: 26-nov-2001
                Esta misma logica esta repetida en
                DNominaClass.PAS  TNomina.CalculaTotales
                ZFuncsImss.CalculaRegla3x3
                ZFuncsImss.CalculaHorasExentas
                si cambia algo en este algoritmo hay que
                verificarlo en los otros 2 lugares}
                if ( AplicaRegla3X3( FDiasAplicaExento,StatusAusencia,TipoDia ) ) then
                begin
                     case nCriterio of
                          K_REGLA3X3_PROPORCIONAL :
                          begin
                               // Cada horas doble vale 1, Cada hora triples vale 1.5  ( 3 / 2 = 1.5 )
                               // Esto facilita multiplicaci�n: NO_EXENTAS * SAL_HORA * 2
                               dias_extras := ( dias_extras + 1 );
                               if ( dias_extras <= K_EXENTAS_SEMANAL ) then
                               begin
                                    h_dobles_3x3  := rMin( h_dobles, K_EXENTAS_DIARIO );
                                                           h_triples_3x3 := rMin( h_triples, K_EXENTAS_DIARIO-h_dobles_3x3 );
                                    h_suma        := h_dobles_3x3 + h_triples_3x3 * 1.5;
                                    Result        := Result + h_suma;
                                    if Rastreando then
                                    begin
                                         RastreoPesos( Format( 'D�a #%d, Horas Exentas Dobles:', [ dias_extras ] ), h_dobles_3x3 );
                                         if ( h_triples_3x3 > 0 ) then
                                         begin
                                              RastreoPesos( Format( 'Horas Exentas Triples:', [ dias_extras ] ), h_triples_3x3 );
                                              RastreoPesos( 'Proporci�n = Dobles + ( Triples * 1.5 ):', h_suma );
                                         end;
                                    end;
                               end
                               else
                                   LimiteSemanal;
                          end;
                          K_REGLA3X3_CUANTAS :
                          begin
                               // Considera dobles y triples. Tanto dobles como triples valen 1
                               dias_extras := ( dias_extras + 1 );
                               if ( dias_extras <= K_EXENTAS_SEMANAL ) then
                               begin
                                    h_suma := rMin( h_extras, K_EXENTAS_DIARIO );
                                    Result := Result + h_suma;
                                    if Rastreando then
                                       RastreoPesos( Format( 'D�a #%d, Horas Exentas:', [ dias_extras ] ), h_suma );
                               end
                               else
                                   LimiteSemanal;
                          end;
                          else // K_REGLA3X3_DOBLES
                          begin
                               // Solo considera horas dobles como exentas
                               if ( h_dobles > 0 ) then
                               begin
                                    dias_extras := ( dias_extras + 1 );
                                    if ( dias_extras <= K_EXENTAS_SEMANAL ) then
                                    begin
                                         h_suma := rMin( h_dobles, K_EXENTAS_DIARIO );
                                         Result := Result + h_suma;
                                         if Rastreando then
                                            RastreoPesos( Format( 'D�a #%d, Horas Exentas Dobles:', [ dias_extras ] ), h_suma );
                                    end
                                    else
                                        LimiteSemanal;
                               end;
                          end;
                     end;
                end
                else
                begin
                     if Rastreando then
                        RastreoPesos( 'Horas Exentas:' , 0 );
                end;
                Next;
            end;
            Active := FALSE;
            Result := Redondea( Result );
            if Rastreando then
            begin
                RastreoEspacio;
                RastreoPesos( 'Total de Horas Exentas:', Result );
            end;
        end;
    end;
end;

{*********************** TZetaCALC_EXENTAS *********************}

type
  TZetaSAL_POND = class ( TZetaRegresaQuery )
  private
    {Private declarations}
    FQueryPreparado: Boolean;
  public
    {Public delarations }
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaSAL_POND.GetRequeridos(var TipoRegresa: TQREvResultType);
begin
     inherited;
     TipoRegresa := resDouble;
     AgregaRequerido( 'NOMINA.CB_CODIGO', enEmpleado );
     AgregaRequerido( 'NOMINA.CB_SAL_INT', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_FEC_INT', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_OLD_INT', enEmpleado );
{$IFDEF CAMBIO_TNOM}
     AgregaRequerido( 'NOMINA.NO_ASI_INI', enNomina );
     AgregaRequerido( 'NOMINA.NO_ASI_FIN', enNomina );
{$ELSE}
     AgregaRequerido( 'PERIODO.PE_FEC_INI', enPeriodo );
     AgregaRequerido( 'PERIODO.PE_FEC_FIN', enPeriodo );
{$ENDIF}
end;

function TZetaSAL_POND.Calculate: TQREvResult;
var
   dInicio, dFinal, dIntegrado: TDate;
   rActual, rAnterior: TPesos;
   iEmpleado: TNumEmp;

function Pondera: TPesos;
var
   iAntes, iActual: Integer;
begin
     iActual := Trunc( dIntegrado );
     iAntes := iActual - Trunc( dInicio );
     iActual := Trunc( dFinal ) - iActual + 1;
     Result := ( ( iAntes * rAnterior ) + ( iActual * rActual ) ) / ( iAntes + iActual );
end;

begin
     if not FQueryPreparado then
     begin
          if not Assigned( FQuery ) then
             FQuery := oZetaProvider.CreateQuery;

          oZetaProvider.PreparaQuery( FQuery, Format( QRY_SAL_POND, [ K_T_CAMBIO ] ) );
          FQueryPreparado := True;
     end;
     with DataSetEvaluador do
     begin
          rActual := DefaultFloat( 0, FieldByName( 'CB_SAL_INT' ).AsFloat );
          {$IFDEF CAMBIO_TNOM}
          dInicio := DefaultDate( 1, FieldByName( 'NO_ASI_INI' ).AsDateTime );
          dFinal := DefaultDate( 2, FieldByName( 'NO_ASI_FIN' ).AsDateTime );
          {$ELSE}
          dInicio := DefaultDate( 1, FieldByName( 'PE_FEC_INI' ).AsDateTime );
          dFinal := DefaultDate( 2, FieldByName( 'PE_FEC_FIN' ).AsDateTime );
          {$ENDIF}
          dIntegrado := FieldByName( 'CB_FEC_INT' ).AsDateTime;
          rAnterior := FieldByName( 'CB_OLD_INT' ).AsFloat;
          iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
     end;
     with Result do
     begin
          Kind := resDouble;
          if ( dIntegrado <= dInicio ) then { Fecha de Cambio Precede al Per�odo }
             dblResult := rActual
          else
              if ( dIntegrado > dInicio ) and ( dIntegrado <= dFinal ) then { Fecha de Cambio Cae Dentro Del Per�odo }
                 dblResult := Pondera
              else { Fecha de Cambio Posterior al Per�odo: averiguar si hay cambios durante el Per�odo }
              begin
                   with FQuery do
                   begin
                        Active := False;
                        with oZetaProvider do
                        begin
                             ParamAsInteger( FQuery, 'Empleado', iEmpleado );
                             ParamAsDate( FQuery, 'Inicio', dInicio );
                             ParamAsDate( FQuery, 'Final', dFinal );
                        end;
                        Active := True;
                        if Eof then
                           dblResult := rActual
                        else
                        begin
                             rActual := FieldByName( 'CB_SAL_INT' ).AsFloat;
                             dIntegrado := FieldByName( 'CB_FEC_INT' ).AsDateTime;
                             rAnterior := FieldByName( 'CB_OLD_INT' ).AsFloat;
                             dblResult := Pondera;
                        end;
                        Active := False;
                   end;
              end;
     end;
end;

{*********************** TZetaAJUSTA_INFO ************************}
type
  TZetaAJUSTA_INFO = class( TZetaINFOCredito )
  private
         function GetAScript: string;
  public
        constructor Create(oEvaluador:TQrEvaluator);override;
        procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
        function Calculate: TQREvResult; override;
  end;

constructor TZetaAJUSTA_INFO.Create(oEvaluador:TQrEvaluator);
begin
     FOffSet := 2;
     inherited Create( oEvaluador );
end;

{procedure TZetaAJUSTA_INFO.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     inherited GetRequeridos( TipoRegresa );
     if( ParamVacio( 9 ) )then
         ErrorFuncion( 'Error en Par�metro 10', 'Es necesario especificar un valor de 1 a 3 para el tipo de ajuste' );
     if( ParamVacio( 10 ) )then
         ErrorFuncion( 'Error en Par�metro 11', 'Es necesario especificar un monto para el acumulado de pagos al INFONAVIT' );
end;}

procedure TZetaAJUSTA_INFO.GetRequeridos( var TipoRegresa: TQrEvResultType );
begin
     inherited GetRequeridos( TipoRegresa );
end;

function TZetaAJUSTA_INFO.GetAScript: string;
begin
{$ifdef INTERBASE}
        Result := 'select RESULTADO from SP_AS( %s, %d, %d, %d, %d )';
{$endif}
{$ifdef MSSQL}
        Result := 'SELECT RESULTADO = DBO.SP_AS( %s, %d, %d, %d, %d )';
{$endif}
end;

function TZetaAJUSTA_INFO.Calculate: TQREvResult;
{
type
    eTipoAjustaINFO = ( taiSiempre, taiFinMes, taiFinBimestre);
}
var
   eTipoAjuste: eTipoAjustaINFO;
   iMeses: Integer;
   rPago, rTope, rAcumulado, rAjuste: TPesos;
   eTipo: eTipoInfonavit;
   lAjustar: Boolean;
   dInicio, dPrimerDia: TDate;
   oDataSet : TZetaCursor;
begin
     {Inicializar variables}
     lAjustar := False;
     eTipoAjuste := taiSiempre;
     dPrimerDia := Date;
     FOffSet := 2;
     rAcumulado := 0;
     rAjuste := 0;
     Result := inherited Calculate;

     with DataSetEvaluador do
     begin
          if ParamVacio( 2 ) then
            //eTipo := eTipoInfonavit( FieldByName( 'CB_INFTIPO' ).AsInteger )
            eTipo := eTipoInfonavit( oTIMSS.Empleado.PrestamoTipo )
          else
            eTipo := eTipoInfonavit( ParamInteger( 2 ) );
          dInicio := FieldByName('CB_INF_INI').AsDateTime;
      end;

     // DES US #13101 Infonavit - Descuento tipo # veces UMA...(Funciones)
     if ZetaEvaluador.CalculandoNomina and ( eTipo in [tiCuotaFija, tiVeces, tiVecesUMA ] ) then
     begin
          if ParamVacio( 1 ) then
          begin
               rAcumulado := 0;

               with ZetaEvaluador.oZetaProvider do
               begin
                    oDataSet := CreateQuery( Format( GetAScript, [ '54',
                                                                   MesDefault ,
                                                                   MesDefault ,
                                                                   YearDefault ,
                                                                   ZetaEvaluador.GetEmpleado ] ) ) ;
               end;
               try
                  try
                     oDataSet.Active := TRUE;
                     rAcumulado := oDataset.FieldByName('RESULTADO').AsFloat;
                  except
                        ErrorFuncion( 'Error al calcular la funci�n A(54,1)', 'AJUSTA_INFO()' );
                  end;
               finally
                      oDataSet.Active := FALSE;
                      FreeAndNil( oDataset );
               end;
          end
          else
          begin
               rAcumulado := ParamFloat( 1 );
          end;

          eTipoAjuste := eTipoAjustaINFO( DefaultInteger( 0, 1 ) - 1  );
          with ZetaEvaluador.oZetaProvider.DatosPeriodo do
          begin
                dPrimerDia := EncodeDate( Year, Mes, 1 );
                lAjustar := ( dInicio <= LastDayOfMonth( dPrimerDia ) ) AND ( PosMes > 1 );
                if lAjustar and ( eTipoAjuste <> taiSiempre ) then
                begin
                     lAjustar := ( PosMes = PerMes );
                     if( lAjustar and ( eTipoAjuste = taiFinBimestre ) )then
                        lAjustar := Mes in [ 2,4,6,8,10,12];
                end;
          end;
     end;
     if lAjustar then
     begin
          if ZetaEvaluador.Rastreando then
             ZetaEvaluador.Rastreador.RastreoHeader( 'RASTREO DE AJUSTE DE  AMORTIZACION INFONAVIT' );
               rPago := Result.dblResult;
          case eTipoAjuste of
               taiSiempre:
               begin
                    rTope := rPago * ZetaEvaluador.oZetaProvider.DatosPeriodo.PosMes;
                    rAjuste := rMax(rTope - ( rPago + rAcumulado ), 0 );
                    if ZetaEvaluador.Rastreando then
                    begin
                         with ZetaEvaluador.Rastreador do
                         begin
                              RastreoPesos( '     Amortizaci�n Proporcional :     ', rTope );
                              RastreoPesos( ' (-)        Monto Amortizaci�n :     ', rPago );
                              RastreoPesos( ' (-) Amortizaciones Acumuladas :     ', rAcumulado );
                              RastreoPesos( ' (=)     Ajuste a Amortizaci�n :     ', rAjuste );
                         end;
                    end;
               end;
               taiFinMes:
               begin
                    rAjuste := rMax( FPago - ( rPago + rAcumulado ), 0 );
                    if ZetaEvaluador.Rastreando then
                    begin
                         with ZetaEvaluador.Rastreador do
                         begin
                              RastreoPesos( '          Amortizaci�n Mensual :     ', FPago );
                              RastreoPesos( ' (-)        Monto Amortizaci�n :     ', rPago );
                              RastreoPesos( ' (-) Amortizaciones Acumuladas :     ', rAcumulado );
                              RastreoPesos( ' (=)     Ajuste a Amortizaci�n :     ', rAjuste );
                         end;
                    end;
               end;
               taiFinBimestre:
               begin
                    if ( dInicio >= dPrimerDia)then
                        iMeses := 1
                    else
                         iMeses := 2;
                    rAjuste := rMax( ( iMeses * FPago ) - ( rPago + rAcumulado ), 0 );
                    if ZetaEvaluador.Rastreando then
                    begin
                         with ZetaEvaluador.Rastreador do
                         begin
                              RastreoPesos( '        Amortizaci�n Bimestral :     ', iMeses*FPago);
                              RastreoPesos( ' (-)        Monto Amortizaci�n :     ', rPago );
                              RastreoPesos( ' (-) Amortizaciones Acumuladas :     ', rAcumulado );
                              RastreoPesos( ' (=)     Ajuste a Amortizaci�n :     ', rAjuste );
                         end;
                    end;
               end;
          end;
          Result.dblResult := rPago + rAjuste;
     end;
end;


{*********************** TZetaEvaluaIMSSBase ************************}
type
  TZetaEvaluaIMSSBase = class( TZetaFunc )
  private
    FEvaluarBegin: Boolean;
    FIMSSAdic: TIMSS;
    FYearIMSS, FMesIMSS: Integer;
  protected
    FIMSS: TIMSS;
    FRama: Integer;
    FSeguroVivienda: TPesos;
    FRastreoINFO: Boolean;
    FOffSet: Integer;
    FPatronInicial : string;  //C�digo de Patron Utilizado por AJUS_INF_N  solamente ( AV: 3/Mayo/2011)

    function EvaluaResultado( const Liquidacion: TLiquidacionEmpleado ): TPesos; virtual;
    function GetMontoEvaluado( const dFecInicial, dFecFinal: TDate ): TPesos; virtual;
  public
    constructor Create(oEvaluador:TQrEvaluator);override;
    destructor Destroy; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

constructor TZetaEvaluaIMSSBase.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create(oEvaluador);
     FIMSS := TIMSS.Create( ZetaEvaluador.oZetaCreator );
     FSeguroVivienda := 0;
     FRama := 0;
     FRastreoINFO := FALSE;
     FOffSet := 0;
     FPatronInicial := VACIO;
end;

destructor TZetaEvaluaIMSSBase.Destroy;
begin
     if FEvaluarBegin then
        FIMSS.EvaluarIMSSPagosEnd;

     FreeAndNil( FIMSS );
     FreeAndNil( FIMSSAdic );
     inherited Destroy;
end;

procedure TZetaEvaluaIMSSBase.GetRequeridos( var TipoRegresa: TQREvResultType);
begin
     TipoRegresa := resDouble;
     EsFecha := FALSE;
     with ZetaEvaluador.oZetaCreator do
     begin
          PreparaQueries;
          PreparaRitmos;
     end;
     oZetaProvider.GetDatosPeriodo;

     // Fechas a revisar
     AgregaRequerido( 'NOMINA.NO_ASI_INI', enNomina );
     AgregaRequerido( 'NOMINA.NO_ASI_FIN', enNomina );
     // Datos del empleado para calcular liquidaci�n
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_INFCRED', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_SEGSOC', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_SAL_INT', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_FEC_INT', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_INFTIPO', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_INFTASA', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_INF_INI', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_INF_OLD', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_TURNO', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_FEC_BSS', enEmpleado );
     AgregaRequerido( 'NOMINA.CB_PATRON', enNomina );
{$ifdef CAMBIO_TNOM}
     AgregaRequerido( 'COLABORA.CB_FEC_NOM', enEmpleado );
{$endif}
end;

function TZetaEvaluaIMSSBase.GetMontoEvaluado( const dFecInicial, dFecFinal: TDate ): TPesos;
var
   dInicio, dFin: TDate;
   iYear, iMes, iDia: Word;

   function GetResultado( oIMSS: TIMSS ): TPesos;
   begin
        with oIMSS do
        begin
             EvaluarIMSSPagosSetFechas( dInicio, dFin, FRastreoINFO );
             //Datos al final de n�mina por suspensiones y reinicios
             if StrVacio( FPatronInicial) then
                 SetGlobalEmpleadoCursor( DataSetEvaluador, dFin )
             else
                 SetGlobalEmpleadoCursor( DataSetEvaluador, dFin, FPatronInicial );
             CalcularIMSSPagos;
             Result := EvaluaResultado( LiquidacionEmpleado );
        end;
   end;

begin
     Result := 0;
     dInicio := dFecInicial;
     while ( dInicio <= dFecFinal ) do
     begin
          dFin := rMin( ZetaCommonTools.LastDayOfMonth( dInicio ), dFecFinal );
          DecodeDate( dInicio, iYear, iMes, iDia );
          if ( iYear = FYearIMSS ) and ( iMes = FMesIMSS ) then
             Result := Result + GetResultado( FIMSS )           // Usar FIMSS ya preparado - La mayor�a de las veces se resuelve con FIMSS
          else
          begin
               if ( FIMSSAdic = nil ) then
                  FIMSSAdic := TIMSS.Create( ZetaEvaluador.oZetaCreator );
               if ZetaEvaluador.Rastreando then
                  ZetaEvaluador.Rastreador.RastreoHeader( VACIO );

               if ( FIMSS.UsarFactorNomInfo ) then
                  FIMSSAdic.UsarFactorNominaInfonavit( FIMSS.FactorNomInfo );

               with FIMSSAdic do
               begin
                    EvaluarIMSSPagosBegin( TRUE, FALSE, FSeguroVivienda, iYear, iMes );
                    Result := Result + GetResultado( FIMSSAdic );
                    EvaluarIMSSPagosEnd;
               end;
          end;
          dInicio := dFin + 1;
     end;
end;

function TZetaEvaluaIMSSBase.Calculate: TQREvResult;
var
   dFecInicial, dFecFinal : TDate;
   iYearInicial, iMesInicial, iDia: Word;
begin
     with DataSetEvaluador do
     begin
          dFecInicial := DefaultDate( FOffSet + 0,  FieldByName( 'NO_ASI_INI' ).AsDateTime );
          dFecFinal   := DefaultDate( FOffSet + 1, FieldByName( 'NO_ASI_FIN' ).AsDateTime );
     end;
     DecodeDate( dFecInicial, iYearInicial, iMesInicial, iDia );
     if ( not FEvaluarBegin ) then
     begin
          FYearIMSS := iYearInicial;
          FMesIMSS := iMesInicial;
          FIMSS.EvaluarIMSSPagosBegin( TRUE, FALSE, FSeguroVivienda, FYearIMSS, FMesIMSS );
          FEvaluarBegin := TRUE;
     end;
     with Result do
     begin
          Kind := resDouble;
          dblResult := GetMontoEvaluado( dFecInicial, dFecFinal );
     end;
end;

function TZetaEvaluaIMSSBase.EvaluaResultado( const Liquidacion: TLiquidacionEmpleado ): TPesos;
begin
     Result := 0;
end;

{*********************** TZetaEvaluaIMSSObrero ************************}

type
  TZetaEvaluaIMSSObrero = class( TZetaEvaluaIMSSBase )
  protected
    function EvaluaResultado( const Liquidacion: TLiquidacionEmpleado ): TPesos; override;
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaEvaluaIMSSObrero.Calculate: TQREvResult;
begin
     if ZetaEvaluador.Rastreando then
        ZetaEvaluador.Rastreador.RastreoHeader( 'RASTREO DE IMSS OBRERO' );
     FRama := DefaultInteger( FOffSet + 2, 0 );
     Result := inherited Calculate;
end;

function TZetaEvaluaIMSSObrero.EvaluaResultado( const Liquidacion: TLiquidacionEmpleado ): TPesos;
const
     K_TODOS   = 0;
     K_ENF_MAT = 1;
     K_INV_VID = 2;
     K_CES_VEJ = 3;
var
   nEym, nIV, nCV : TPesos;
begin
     nEym := 0;
     nIV  := 0;
     nCV  := 0;
     with Liquidacion do
     begin
          if ( FRama = K_TODOS ) or ( FRama = K_ENF_MAT )then
          begin
               nEym := ( LE_EYM_EXC - LE_EYMEXCP ) +  // Enfermedad y Maternidad (Art.106-II) - Excedente / Obrero
                       ( LE_EYM_DIN - LE_EYMDINP ) +  // Enfermedad y Maternidad (Art.107) - Dinero / Obrero
                       ( LE_EYM_ESP - LE_EYMESPP );   // Enfermedad y Maternidad (Art.25) - Especie / Obrero
          end;

          if ( FRama = K_TODOS ) or ( FRama = K_INV_VID )then
          begin
               nIV  := ( LE_INV_VID - LE_INVVIDP );   // Invalidez y Vida (Art.147)
          end;

          if ( FRama = K_TODOS ) or ( FRama = K_CES_VEJ ) then
          begin
               nCV  := ( LE_CES_VEJ - LE_CESVEJP );   // Cesant�a y Vejez (Art.168-II)
          end;

          Result := nEym + nIV + nCV;

          if ( FRama = K_TODOS )then
          begin
               if ZetaEvaluador.Rastreando then
               begin
                    with ZetaEvaluador.Rastreador do
                    begin
                         RastreoHeader( 'Suma de Totales IMSS TRABAJADOR' );
                         RastreoPesos( '    Enfermedad y Maternidad    : ', nEym );
                         RastreoPesos( '(+) Invalidez y Vida           : ', nIV );
                         RastreoPesos( '(+) Cesant�a y Vejez           : ', nCV );
                         RastreoPesos( '(=) TOTAL IMSS TRABAJADOR      : ', Result );
                    end;
               end;
          end;
     end;
end;

{*********************** TZetaEvaluaIMSSPatron ************************}

type
  TZetaEvaluaIMSSPatron = class( TZetaEvaluaIMSSBase )
  protected
    function EvaluaResultado( const Liquidacion: TLiquidacionEmpleado ): TPesos; override;
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaEvaluaIMSSPatron.Calculate: TQREvResult;
begin
     if ZetaEvaluador.Rastreando then
        ZetaEvaluador.Rastreador.RastreoHeader( 'RASTREO DE IMSS PATRONAL' );
     FRama := DefaultInteger( FOffSet + 2, 0 );
     Result := inherited Calculate;
end;

function TZetaEvaluaIMSSPatron.EvaluaResultado( const Liquidacion: TLiquidacionEmpleado ): TPesos;
const
     K_TODOS   = 0;
     K_ENF_MAT = 1;
     K_INV_VID = 2;
     K_RIE_TRA = 3;
     K_GUA_PRE = 4;
var
   nEym, nIV, nRT, nGuard : TPesos;
begin
     nEym   := 0;
     nIV    := 0;
     nRT    := 0;
     nGuard := 0;
     with Liquidacion do
     begin
          if ( FRama = K_TODOS ) or ( FRama = K_ENF_MAT )then
          begin
               nEym   := LE_EYMESPP +                   // Enfermedad y Maternidad (Art.25) - Especie / Patron
                         LE_EYM_FIJ +                   // Enfermedad y Maternidad (Art.106-I) - Fijo
                         LE_EYMEXCP +                   // Enfermedad y Maternidad (Art.106-II) - Excedente / Patron
                         LE_EYMDINP;                    // Enfermedad y Maternidad (Art.107) - Dinero / Patron
          end;

          if ( FRama = K_TODOS ) or ( FRama = K_INV_VID )then
          begin
               nIV    := LE_INVVIDP;                    // Invalidez y Vida (Art.147) - Patron
          end;

          if ( FRama = K_TODOS ) or ( FRama = K_RIE_TRA )then
          begin
               nRT    := LE_RIESGOS;                    // Riesgos de Trabajo (Art.71)
          end;

          if ( FRama = K_TODOS ) or ( FRama = K_GUA_PRE )then
          begin
               nGuard := LE_GUARDER;                    // Guarder�as y Prestaciones Sociales (Art.211)
          end;

          Result := nEym + nIV + nRT + nGuard;

          if ( FRama = K_TODOS )then
          begin
               if ZetaEvaluador.Rastreando then
               begin
                    with ZetaEvaluador.Rastreador do
                    begin
                         RastreoHeader( 'Suma de Totales IMSS PATRONAL' );
                         RastreoPesos( '    Enfermedad y Maternidad    : ', nEym );
                         RastreoPesos( '(+) Invalidez y Vida           : ', nIV );
                         RastreoPesos( '(+) Riesgos de Trabajo         : ', nRT );
                         RastreoPesos( '(+) Guarder�as y Prestaciones  : ', nGuard );
                         RastreoPesos( '(=) TOTAL IMSS PATRONAL        : ', Result );
                    end;
               end;
          end;

     end;
end;

{*********************** TZetaEvaluaIMSSRetiro ************************}

type
  TZetaEvaluaIMSSRetiro = class( TZetaEvaluaIMSSBase )
  protected
    function EvaluaResultado( const Liquidacion: TLiquidacionEmpleado ): TPesos; override;
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaEvaluaIMSSRetiro.Calculate: TQREvResult;
begin
     if ZetaEvaluador.Rastreando then
        ZetaEvaluador.Rastreador.RastreoHeader( 'RASTREO DE IMSS RETIRO' );
     FRama := DefaultInteger( FOffSet + 2, 0 );
     Result := inherited Calculate;
end;

function TZetaEvaluaIMSSRetiro.EvaluaResultado( const Liquidacion: TLiquidacionEmpleado ): TPesos;
const
     K_TODOS   = 0;
     K_RETIRO  = 1;
     K_CES_VEJ = 2;
var
   nCV, nSAR : TPesos;
begin
     nCV  := 0;
     nSAR := 0;

     with Liquidacion do
     begin
          if ( FRama = K_TODOS ) or ( FRama = K_RETIRO )then
          begin
               nSar := LE_RETIRO;              // Retiro (Art.168-I)
          end;
          if ( FRama = K_TODOS ) or ( FRama = K_CES_VEJ )then
          begin
               nCV  := LE_CESVEJP;             // Cesant�a y Vejez (Art.168-II)
          end;
     end;

     Result := nSar + nCV;

     if ( FRama = K_TODOS )then
     begin
          if ZetaEvaluador.Rastreando then
          begin
               with ZetaEvaluador.Rastreador do
               begin
                    RastreoHeader( 'Suma de Totales IMSS RETIRO' );
                    RastreoPesos( '    Seguro de Retiro           : ', nSar );
                    RastreoPesos( '(+) Cesant�a y Vejez           : ', nCV );
                    RastreoPesos( '(=) TOTAL IMSS RETIRO          : ', Result );
               end;
          end;
     end;
end;

{*********************** TZetaEvaluaInfoCredito ************************}

type
  TZetaEvaluaInfoCredito = class( TZetaEvaluaIMSSBase )
  protected
    function EvaluaResultado( const Liquidacion: TLiquidacionEmpleado ): TPesos; override;
  public
    constructor Create( oEvaluador:TQrEvaluator ); override;
    function Calculate: TQREvResult; override;
  end;

constructor TZetaEvaluaInfoCredito.Create( oEvaluador:TQrEvaluator );
begin
     inherited Create( oEvaluador );
     FOffSet := 1;
end;

function TZetaEvaluaInfoCredito.Calculate: TQREvResult;
var
   rDiasVaca, rMonto: TPesos;
   iPosRenglon: Integer;
   sTextoRastreo: String;
begin
     FSeguroVivienda := DefaultFloat( FOffSet + 2, 0 );

     if ZetaEvaluador.Rastreando then
     begin
          with ZetaEvaluador.Rastreador do
          begin
               RastreoHeader( 'RASTREO DEL CALCULO DEL DESCUENTO INFONAVIT' );
               RastreoTextoLibre('', iPosRenglon);
          end;
     end;

     Result := inherited Calculate;

     if ( ZetaEvaluador.Rastreando )  then
     begin
          with ZetaEvaluador.Rastreador do
          begin
               with FImss.Empleado do
               begin
                    sTextoRastreo:= 'Tipo de Cr�dito    : ' + ZetaCommonLists.ObtieneElemento( lfTipoInfonavit, Ord( PrestamoTipo ) ) + CR_LF;

                    if ( PrestamoTipo <> tiNoTiene ) then
                    begin
                         sTextoRastreo:=  sTextoRastreo + 'Fecha de Inicio    : ' + FormatDateTime( 'dd/mmm/yyyy', DataSetEvaluador.FieldByName( 'CB_INF_INI' ).AsDateTime );
                         if ( FechaSuspension <> NullDateTime ) then
                         begin
                              if FechaSuspension < DataSetEvaluador.FieldByName( 'CB_INF_INI' ).AsDateTime then
                                 sTextoRastreo:= sTextoRastreo + CR_LF + 'Fecha de Suspensi�n: '
                              else
                                  sTextoRastreo:= sTextoRastreo + CR_LF + 'Fecha de Suspensi�n: ' + FormatDateTime( 'dd/mmm/yyyy', FechaSuspension );
                         end;
                         sTextoRastreo:= sTextoRastreo + CR_LF + 'Valor de Descuento : ' + Format( '%11.4n', [ FImss.Empleado.Prestamo ] ) + CR_LF;
                         if ( PrestamoTipo = tiPorcentaje ) then
                            sTextoRastreo:= sTextoRastreo + 'Reducci�n de Tasa  : ' + BoolAsSiNo( AplicaDisminucion ) + CR_LF;

                         RastreoInsertaTexto( sTextoRastreo, iPosRenglon );
                    end;
               end;
          end;
     end;

     if ( not ParamVacio( FOffSet - 1 ) ) then
     begin
          rDiasVaca := ParamFloat( FOffSet - 1 );
          if ( rDiasVaca > 0 ) then
          begin
               rMonto := FIMSS.CalculaMontoVacaciones( rDiasVaca );
               Result.dblResult := Result.dblResult + rMonto;

               if ZetaEvaluador.Rastreando then
               begin
                    with ZetaEvaluador.Rastreador do
                    begin
                         RastreoHeader( 'RASTREO DE AMORTIZACION DE VACACIONES' );
                         RastreoEspacio;
                         RastreoPesos( '            D�as de Vacaciones :     ', rDiasVaca );
                         RastreoPesos( '(+)        Monto de Vacaciones :     ', rMonto );
                         RastreoEspacio;
                    end;
               end;
          end;
     end;
end;

function TZetaEvaluaInfoCredito.EvaluaResultado( const Liquidacion: TLiquidacionEmpleado ): TPesos;
begin
     Result := Liquidacion.LE_INF_AMO;      // INFONAVIT - Amortizacion del Empleado
end;


{ *********************** TZetaValorDiaInfonavit ************************}

type
  TZetaValorDiaInfonavit = class( TZetaEvaluaIMSSBase )
  protected
    function GetMontoEvaluado( const dFecInicial, dFecFinal: TDate ): TPesos; override;
    function EvaluaResultado( const Liquidacion: TLiquidacionEmpleado ): TPesos; override;
  public
    constructor Create( oEvaluador:TQrEvaluator ); override;
    function Calculate: TQREvResult; override;
  end;

function TZetaValorDiaInfonavit.GetMontoEvaluado( const dFecInicial, dFecFinal: TDate ): TPesos;
begin
     with FIMSS do
     begin
          EvaluarIMSSPagosBegin( TRUE, FALSE, FSeguroVivienda, FYearIMSS, FMesIMSS );
          //FEvaluarBegin := TRUE; Que siempre investigue por cambio de mes
          EvaluarIMSSPagosSetFechas( dFecInicial, dFecFinal, FRastreoINFO );
          SetGlobalEmpleadoCursor( DataSetEvaluador );
          Result := FIMSS.CalculaMontoVacaciones( 1 );     { Se trae el valor de un d�a}
          EvaluarIMSSPagosEnd;
     end;
end;

constructor TZetaValorDiaInfonavit.Create( oEvaluador:TQrEvaluator );
begin
     inherited Create( oEvaluador );
     FOffSet := 1;
end;

function TZetaValorDiaInfonavit.Calculate: TQREvResult;
var
   dFecInicial, dFecFinal : TDate;
begin
     if ( ParamVacio( 0 ) ) then
     begin
          Result := inherited Calculate;
     end
     else
     begin
          with oZetaProvider.DatosPeriodo do
          begin
               dFecInicial:= EncodeDate( Year, ParamInteger(0), 1 );
               dFecFinal:= LastDayOfBimestre(dFecInicial);

               if ( not FEvaluarBegin ) then
               begin
                    FYearIMSS := Year;
                    FMesIMSS := ParamInteger(0);
               end;
          end;

          with Result do
          begin
               Kind := resDouble;
               dblResult := GetMontoEvaluado( dFecInicial, dFecFinal );
          end;

     end;
end;

function TZetaValorDiaInfonavit.EvaluaResultado( const Liquidacion: TLiquidacionEmpleado ): TPesos;
begin
     Result := Liquidacion.LE_INF_AMO;      // INFONAVIT - Amortizacion del Empleado
end;

{ *********************** TZetaValorNominaInfonavit ************************}
const
     K_ACTIVO_SI = 1 ;
     K_ACTIVO_NO_ING = 0;
     K_ACTIVO_NO_BAJ = 9;

     QUERY_RANGOS = 1;
     QUERY_NOMINA_EMP = 2;
     QUERY_STATUS_ACT = 3;
     QUERY_PATRON = 4;

type
  TZetaValorNominaInfonavit = class( TZetaEvaluaIMSSBase )
  private
    FFechaBimestre : TDate;

    FFecInicialBim, FFecFinalBim: TDate;
    FFecInicialRango, FFecFinalRango : TDate;
    FFecNom, FFecBaj, FFecIng : TDate;
    FTipoNomina : integer;

    FPatronIniBim : string;
    FPatronFinBim : string;

    FEmpActivo : boolean;
    FQueryRangos {$ifdef ANTES}, FQueryMesIMSS, FQueryPeriodosBim{$endif} : TZetaCursor ;
    FTipoNominaEmp, FStatusActEmp , FCurPatron: TZetaCursor;

    FDataSetPeriodos : TClientDataSet;

    DiasCotizados         : TPesos;
    DiasCotizadosBimestre : TPesos;
    DiasCotizadosNomina   : TPesos;

    FIMSSAux: TIMSS;
    FEmpleadoIMSS : TEmpleadoIMSS;

    FRedondear : boolean;

    {$ifdef PROFILE}
    vnomProfiler : TZetaProfiler;
    {$endif}

    procedure PreparaQuery( iTipo : integer);
    procedure SetFechasBimestre;
    procedure SetRangoFechas;
    procedure SetPatronesBimestreNominal( dFechaIni, dFechaFin : TDateTime);
    function  CargaPeriodosBimestre : boolean;
    function  GetCuotaBimPorCubrir : TPesos;
    function  GetMesIMSS( dFecha : TDateTime; iTipoNom : integer ) : integer;
    function  GetTipoNomina( dFecha : TDateTime ) : integer;
    function  GetStatusActivo( dFecha : TDateTime ) : integer;
    function  GetRegistroPatronal( dFecha : TDateTime ) : string;
    function  GetDiasCotizadosNomina( const dFecInicial, dFecFinal: TDate ): TPesos;
    function  EsEmpleadoActivoSinReingresos : boolean;
  protected
    function GetMontoEvaluado( const dFecInicial, dFecFinal: TDate ): TPesos; override;
    function GetMontoEvaluadoVarios( const dFecInicial, dFecFinal: TDate; oIMSS : TIMSS): TPesos;
    function GetMontoEvaluadoUtil( const dFecInicial, dFecFinal: TDate; oIMSS : TIMSS; const sPatronInicial : string = VACIO): TPesos;

    function EvaluaResultado( const Liquidacion: TLiquidacionEmpleado ): TPesos; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  public
    constructor Create( oEvaluador:TQrEvaluator ); override;
    destructor Destroy; override;
    function Calculate: TQREvResult; override;
    property Redondear: boolean read FRedondear write FRedondear;
    property PatronIniBim : string read FPatronIniBim;
    property PatronFinBim : string read FPatronFinBim;
    property EmpleadoIMSSEvaluado : TEmpleadoIMSS read FEmpleadoIMSS;
  end;

constructor TZetaValorNominaInfonavit.Create( oEvaluador:TQrEvaluator );
var
   iMes : integer;
begin
     inherited Create( oEvaluador );

     FRedondear := TRUE;

     FIMSSAux :=  TIMSS.Create( ZetaEvaluador.oZetaCreator );
     FOffSet := 1;


     //Mejora CR2367 
     with oZetaProvider.DatosPeriodo do
     begin
          iMes := Mes;

          if ( iMes MOD 2 ) = 0  then
             iMes := iMes -1;

          //codigo defensivo
          //Se valida en Calculate que el Mes del periodo sea <13
          //Solo se hace este codigo defensivo para calcular la fecha
          if ( iMes >= 1) and (iMes <=12 )  then
              FFechaBimestre := EncodeDate( Year, iMes, 1 )
          else
              FFechaBimestre := NullDateTime;
     end;

end;

destructor TZetaValorNominaInfonavit.Destroy;
begin
     FreeAndNil( FQueryRangos );
{$ifdef ANTES}
     FreeAndNil( FQueryMesIMSS );
     FreeAndNil( FQueryPeriodosBim );
{$endif}
     FreeAndNil( FTipoNominaEmp );
     FreeAndNil( FStatusActEmp );
     FreeAndNil( FIMSSAux );
     FreeAndNil( FCurPatron );

     inherited Destroy;
end;

function TZetaValorNominaInfonavit.GetDiasCotizadosNomina( const dFecInicial, dFecFinal: TDate ): TPesos;
var
   dIniIzq, dFinIzq : TDateTime;
   dIniDer, dFinDer : TDateTime;
   fDiasCotizadosNominaIZQ : TPesos;
   fDiasCotizadosNominaDER : TPesos;

   procedure AjusteEsquinaIzquierda;
   begin
     fDiasCotizadosNominaIZQ := 0;
   //Calcula Esquina Izquierda
     if ( dFecInicial > FFecInicialBim ) then
     begin
         dIniIzq := FFecInicialBim;
         dFinIzq := dFecInicial - 1;
         if (ZetaEvaluador.Rastreando)  then
         begin
              with  ZetaEvaluador.Rastreador do
              begin
                   RastreoTextoLibre('RESTA DE DIAS COTIZADOS');
              end;
         end;
         GetMontoEvaluadoVarios( dIniIzq, dFinIzq, FIMSSAux );
         fDiasCotizadosNominaIZQ :=  Self.DiasCotizados * -1;
     end
     else
     if ( dFecInicial < FFecInicialBim ) then
     begin
         dIniIzq := dFecInicial;
         dFinIzq := FFecInicialBim - 1;
         if (ZetaEvaluador.Rastreando)  then
         begin
              with  ZetaEvaluador.Rastreador do
              begin
                   RastreoTextoLibre('SUMA DE DIAS COTIZADOS');
              end;
         end;
         GetMontoEvaluadoVarios( dIniIzq, dFinIzq, FIMSSAux );
         fDiasCotizadosNominaIZQ :=  Self.DiasCotizados;
     end;

   end;
   procedure AjusteEsquinaDerecha;
   begin
     fDiasCotizadosNominaDER := 0;
    //Calcula Esquina Derecha
     //TODO :  La esquina derecha solo es un copy paste de la izq
     if ( dFecFinal > FFecFinalBim ) then
     begin
         dIniDer := FFecFinalBim+1;
         dFinDer := dFecFinal;
         if (ZetaEvaluador.Rastreando)  then
         begin
              with  ZetaEvaluador.Rastreador do
              begin
                   RastreoTextoLibre('SUMA DE DIAS COTIZADOS');
              end;
         end;
         GetMontoEvaluadoVarios( dIniDer, dFinDer, FIMSSAux );
         fDiasCotizadosNominaDER :=  Self.DiasCotizados;
     end
     else
     if ( dFecFinal < FFecFinalBim ) then
     begin
         dIniDer := dFecFinal + 1 ;
         dFinDer := FFecFinalBim;
         if (ZetaEvaluador.Rastreando)  then
         begin
              with  ZetaEvaluador.Rastreador do
              begin
                   RastreoTextoLibre('RESTA DE DIAS COTIZADOS');
              end;
         end;
         GetMontoEvaluadoVarios( dIniDer, dFinDer, FIMSSAux );
         fDiasCotizadosNominaDER :=  Self.DiasCotizados * -1;
     end;

   end;

begin
{$ifdef ANTES}
     Self.DiasCotizados:= 0;
     Self.DiasCotizadosNomina := 0;
     GetMontoEvaluadoVarios( dFecInicial, dFecFinal, FIMSSAux );
     Self.DiasCotizadosNomina := Self.DiasCotizados;
     Result := Self.DiasCotizadosNomina;
{$else}
     AjusteEsquinaIzquierda;
     AjusteEsquinaDerecha;
     Result := Self.DiasCotizadosBimestre +  fDiasCotizadosNominaIZQ +   fDiasCotizadosNominaDER;
     Self.DiasCotizadosNomina := Result;
{$endif}
end;

procedure TZetaValorNominaInfonavit.GetRequeridos( var TipoRegresa: TQREvResultType);
begin
     inherited GetRequeridos( TipoRegresa );
     AgregaRequerido( 'NOMINA.PE_TIPO', enNomina );
     AgregaRequerido( 'COLABORA.CB_ACTIVO', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_FEC_ING', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_FEC_BAJ', enEmpleado );
end;

procedure TZetaValorNominaInfonavit.PreparaQuery( iTipo : integer);
const
     K_GET_NOMINA_RANGOS_BIMESTRE = 'select MIN(PE_ASI_INI) MIN_ASI_INI,  MAX(PE_ASI_FIN) MAX_ASI_FIN from PERIODO WHERE PE_YEAR = :YEAR and PE_MES in (:MES1, :MES2) and PE_USO = 0 and PE_CAL = ''S'' ';

{$ifdef ANTES}
     K_GET_MES_PERIODO_POR_FECHA = 'select PE_MES from PERIODO WHERE PE_YEAR = :YEAR and PE_USO = 0 and PE_CAL = ''S'' and :FECHA between PE_ASI_INI and  PE_ASI_FIN and PE_TIPO = :TIPONOM ';
{$endif}

{$ifdef MSSQL}
     K_TIPO_NOMINA_EMP = 'select( DBO.SP_GET_NOMTIPO( :FECHA, :EMPLEADO ) ) TIPNOM ';
     K_STATUS_ACT_EMP = 'select( DBO.SP_STATUS_ACT( :FECHA, :EMPLEADO ) ) RESULTADO ';
     K_PATRON_INI = 'select( DBO.SP_KARDEX_CB_PATRON( :FECHA, :EMPLEADO) ) CB_PATRON';
{$endif}
{$ifdef INTERBASE}
     K_TIPO_NOMINA_EMP = 'select TIPNOM from SP_GET_NOMTIPO( :FECHA, :EMPLEADO ) ';
     K_STATUS_ACT_EMP = 'select RESULTADO from SP_STATUS_ACT( :FECHA, :EMPLEADO ) ';
     K_PATRON_INI = 'select CB_PATRON from SP_FECHA_KARDEX( :FECHA, :EMPLEADO ) ';
{$endif}

begin
     if ( FQueryRangos = nil ) and ( iTipo = QUERY_RANGOS )  then
        FQueryRangos := oZetaProvider.CreateQuery( K_GET_NOMINA_RANGOS_BIMESTRE );

     if ( FTipoNominaEmp = nil )  and ( iTipo = QUERY_NOMINA_EMP )  then
        FTipoNominaEmp := oZetaProvider.CreateQuery( K_TIPO_NOMINA_EMP );

     if ( FStatusActEmp = nil )  and ( iTipo = QUERY_STATUS_ACT )  then
        FStatusActEmp  := oZetaProvider.CreateQuery( K_STATUS_ACT_EMP );

     if ( FCurPatron = nil ) and ( iTipo = QUERY_PATRON ) then
        FCurPatron :=  oZetaProvider.CreateQuery( K_PATRON_INI );
end;

procedure TZetaValorNominaInfonavit.SetFechasBimestre;
begin
     with oZetaProvider.DatosPeriodo do
     begin
          FFecInicialBim:= FirstDayOfBimestre( FFechaBimestre );
          FFecFinalBim := LastDayOfBimestre( FFechaBimestre );
     end;
end;

procedure TZetaValorNominaInfonavit.SetRangoFechas;
var
   iYearInicial, iMesInicial, iDia: Word;
begin
     //ANTES>>>
     DecodeDate( FFecInicialBim, iYearInicial, iMesInicial, iDia );
     if ( not FEvaluarBegin ) then
     begin
          FYearIMSS := iYearInicial;
          FMesIMSS := iMesInicial;
          FEvaluarBegin := TRUE;
     end;

     if ( oZetaProvider.PeriodosNomInfo <> nil ) then
     begin
         FFecInicialRango :=  oZetaProvider.DatosNomInfo.dInicialRangoNomInfo;
         FFecFinalRango :=  oZetaProvider.DatosNomInfo.dFinalRangoNomInfo;
     end
     else
     begin
        PreparaQuery( QUERY_RANGOS );

        with FQueryRangos do
        begin
             Active := FALSE;
             with oZetaProvider do
             begin
                ParamAsInteger( FQueryRangos, 'YEAR', FYearIMSS );
                ParamAsInteger( FQueryRangos, 'MES1', FMesIMSS );
                ParamAsInteger( FQueryRangos, 'MES2', FMesIMSS +1 );
             end;
             Active := TRUE;
             if not isEmpty then
             begin
                  FFecInicialRango := FieldByName( 'MIN_ASI_INI' ).AsDateTime;
                  FFecFinalRango   := FieldByName( 'MAX_ASI_FIN' ).AsDateTime;
             end;
             Active := False;
        end;

        with oZetaProvider.DatosNomInfo do
        begin
             dInicialRangoNomInfo := FFecInicialRango;
             dFinalRangoNomInfo := FFecFinalRango;
        end;
     end;
end;

function TZetaValorNominaInfonavit.CargaPeriodosBimestre : boolean;
const
     K_GET_PERIODOS_BIMESTRE = 'select PE_MES, PE_TIPO, PE_NUMERO, PE_ASI_INI, PE_ASI_FIN from PERIODO WHERE PE_YEAR = %d and PE_MES in (%d, %d) and PE_USO = 0 and PE_CAL = ''S''  ORDER BY PE_TIPO,PE_ASI_INI';
begin
     if ( oZetaProvider.PeriodosNomInfo <> nil ) then
     begin
         FDataSetPeriodos :=  oZetaProvider.PeriodosNomInfo;
         Result := not FDataSetPeriodos.isEmpty;
     end
     else
     begin
         FDataSetPeriodos := TClientDataSet.Create( oZetaProvider );
         with  oZetaProvider do
         begin
            with FDataSetPeriodos do
            begin
                 if ( not Active ) then
                    Data := OpenSQL( EmpresaActiva, Format( K_GET_PERIODOS_BIMESTRE, [ FYearIMSS, FMesIMSS, FMesIMSS+1  ] ), TRUE );
                 Result := not isEmpty;
            end;
         end;
         oZetaProvider.PeriodosNomInfo :=  FDataSetPeriodos;
     end
end;

function  TZetaValorNominaInfonavit.GetMesIMSS( dFecha : TDateTime; iTipoNom : integer ) : integer;
begin
     Result := 0;
     with FDataSetPeriodos do
     begin
         Filtered := False;
         Filter := Format( '( PE_TIPO=%d )', [iTipoNom] );
         Filtered := True;
         if  ( not IsEmpty ) and ( RecordCount > 0 ) then
         begin
              First;
              while not Eof do
              begin
                   if ( iTipoNom = FieldByName('PE_TIPO').AsInteger  ) and
                      ( dFecha >= FieldByName('PE_ASI_INI').AsDatetime ) and
                      ( dFecha <= FieldByName('PE_ASI_FIN').AsDatetime ) then
                      begin
                               Result := FieldByName('PE_MES').AsInteger;
                               break;
                      end;
                 Next;
              end;
         end;
     end;
end;

function  TZetaValorNominaInfonavit.GetTipoNomina( dFecha : TDateTime ) : integer;
begin
     Result := -1;

     if ( FFecNom <= FFecInicialRango ) then
     begin
        Result := FTipoNomina;
     end
     else
     begin
        PreparaQuery( QUERY_NOMINA_EMP );
        with FTipoNominaEmp do
        begin
             Active := FALSE;
             with oZetaProvider do
             begin
                ParamAsDate( FTipoNominaEmp, 'FECHA', dFecha );
                ParamAsInteger( FTipoNominaEmp, 'EMPLEADO', ZetaEvaluador.GetEmpleado );
             end;
             Active := TRUE;
             if not isEmpty then
             begin
                  Result := FieldByName( 'TIPNOM' ).AsInteger;
             end;
             Active := False;
        end;
     end;
end;

function  TZetaValorNominaInfonavit.EsEmpleadoActivoSinReingresos : boolean;
begin
     Result :=( FEmpActivo ) and ( FFecIng > FFecBaj ) and ( FFecIng <= FFecInicialRango ) and ( FFecBaj < FFecInicialRango );
end;

function  TZetaValorNominaInfonavit.GetRegistroPatronal( dFecha : TDateTime ) : string;
begin
   PreparaQuery( QUERY_PATRON );
   Result := VACIO;
   with FCurPatron do
   begin
        Active := FALSE;
        with oZetaProvider do
        begin
           ParamAsDate( FCurPatron, 'FECHA', dFecha );
           ParamAsInteger( FCurPatron, 'EMPLEADO', ZetaEvaluador.GetEmpleado );
        end;
        Active := TRUE;
        if not isEmpty then
        begin
             Result := FieldByName( 'CB_PATRON' ).AsString;
        end;
        Active := False;
   end;
end;

procedure  TZetaValorNominaInfonavit.SetPatronesBimestreNominal( dFechaIni, dFechaFin : TDateTime);

   function MinDate( dFecha1, dFecha2 : TDateTime ) : TDateTime ;
   begin
        if ( dFecha1 < dFecha2 ) then
           Result := dFecha1
        else
            Result := dFecha2;
   end;

   function MaxDate( dFecha1, dFecha2 : TDateTime ) : TDateTime ;
   begin
        if ( dFecha1 > dFecha2 ) then
           Result := dFecha1
        else
            Result := dFecha2;
   end;

begin
     if EsEmpleadoActivoSinReingresos then
     begin
       with DataSetEvaluador do
       begin
          FPatronIniBim := FieldByName('CB_PATRON').AsString;
          FPatronFinBim := FPatronIniBim;
       end
     end
     else
     begin
          FPatronIniBim := GetRegistroPatronal( MinDate( dFechaIni, FFecInicialBim ) );
          FPatronFinBim := GetRegistroPatronal( MaxDate( dFechaFin, FFecFinalBim )  );
     end;
end;

function  TZetaValorNominaInfonavit.GetStatusActivo( dFecha : TDateTime ) : integer;
begin
     Result := K_ACTIVO_NO_ING;

     if EsEmpleadoActivoSinReingresos  then
     begin
        Result := K_ACTIVO_SI;
     end
     else
     begin
        PreparaQuery( QUERY_STATUS_ACT );
        with FStatusActEmp do
        begin
             Active := FALSE;
             with oZetaProvider do
             begin
                ParamAsDate( FStatusActEmp, 'FECHA', dFecha );
                ParamAsInteger( FStatusActEmp, 'EMPLEADO', ZetaEvaluador.GetEmpleado );
             end;
             Active := TRUE;
             if not isEmpty then
             begin
                  Result := FieldByName( 'RESULTADO' ).AsInteger;
             end;
             Active := False;
        end;
     end;
end;

function TZetaValorNominaInfonavit.GetMontoEvaluado( const dFecInicial, dFecFinal: TDate): TPesos;
begin
     Self.DiasCotizados := 0;
     Result := GetMontoEvaluadoVarios( dFecInicial, dFecFinal, FIMSS )
end;

function TZetaValorNominaInfonavit.GetMontoEvaluadoVarios( const dFecInicial, dFecFinal: TDate; oIMSS : TIMSS): TPesos;
begin
     Self.DiasCotizados := 0;
     //Validaci�n defensiva el verificar los FPatrionIniBim y FinBim Vacios
     if ( FPatronIniBim = FPatronFinBim ) or ( StrVacio(FPatronIniBim) or StrVacio(FPatronFinBim)  )  then
     begin
           Result := GetMontoEvaluadoUtil( dFecInicial, dFecFinal, oIMSS, VACIO )
     end
     else
     begin
          //Calculo del primer Me
          Result := GetMontoEvaluadoUtil( dFecInicial, dFecFinal, oIMSS, FPatronIniBim );
          Result := Result + GetMontoEvaluadoUtil( dFecInicial, dFecFinal, oIMSS, FPatronFinBim );
     end;
end;

//Antes GetMontoEvaluadoVarios
function TZetaValorNominaInfonavit.GetMontoEvaluadoUtil( const dFecInicial, dFecFinal: TDate; oIMSS : TIMSS; const sPatronInicial : string): TPesos;
var
   dInicio, dFin, dFinAsistencia: TDate;
   iYear, iMes, iDia: Word;
begin
     if strVacio (sPatronInicial) then
          Self.DiasCotizados := 0;
     Result := 0;
     {$ifdef PROFILE}vnomProfiler.Check(Format('TZetaValorNominaInfonavit.Calculate >> GetMontoEvaluadoVarios(%s,%s)',[FechaAsStr(dFecInicial),FechaAsStr(dFecFinal)]));{$endif}

     with DataSetEvaluador do
     begin
          dFinAsistencia   := DefaultDate( FOffSet + 1, FieldByName( 'NO_ASI_FIN' ).AsDateTime );
     end;

     dInicio := dFecInicial;
     while ( dInicio <= dFecFinal ) and ( dInicio > 0 ) and ( dFecFinal > 0)  do
     begin
          dFin := rMin( ZetaCommonTools.LastDayOfMonth( dInicio ), dFecFinal );
          DecodeDate( dInicio, iYear, iMes, iDia );
          with oIMSS do
          begin
               UsarLimiteRevisionFaltasInfonavit( dFinAsistencia );
               EvaluarIMSSPagosBegin( TRUE, FALSE, FSeguroVivienda, iYear, iMes );
               EvaluarIMSSPagosSetFechas( dInicio, dFin, False );
               //Datos al final de periodo de ajuste por suspensiones y reinicios
               SetGlobalEmpleadoCursor( DataSetEvaluador, dFin, sPatronInicial );
               CalcularIMSSPagos;
               Result := Result + EvaluaResultado( LiquidacionEmpleado );
               EvaluarIMSSPagosEnd;
               RemoverLimiteRevisionFaltasInfonavit;
          end;
          dInicio := dFin + 1;
     end;
     {$ifdef PROFILE}vnomProfiler.Check(Format('TZetaValorNominaInfonavit.Calculate >> GetMontoEvaluadoVarios(%s,%s)',[FechaAsStr(dFecInicial),FechaAsStr(dFecFinal)]));{$endif}
end;

function TZetaValorNominaInfonavit.GetCuotaBimPorCubrir : TPesos;
var
   iYearInicial, iMesInicial, iDia: Word;
   lCalcular : boolean;
begin
     lCalcular := True;
     Result := 0.00;

     //ANTES>>>
     DecodeDate( FFecInicialBim, iYearInicial, iMesInicial, iDia );
     if ( not FEvaluarBegin ) then
     begin
          FYearIMSS := iYearInicial;
          FMesIMSS := iMesInicial;
          FEvaluarBegin := TRUE;
     end;
     //<ANMTES

     //
     with oZetaProvider.DatosNomInfo do
     begin
         if ( Empleado = ZetaEvaluador.GetEmpleado ) and ( SeguroViviendaNomInfo = FSeguroVivienda ) and
            ( iYearIMSS = FYearIMSS ) and (iMesIMSS = FMesIMSS)   then
         begin
              Result := CuotaBimestralNomInfo;
              DiasCotizados := DiasCotizadosBimestreNomInfo;
              DiasCotizadosBimestre := DiasCotizados;
              lCalcular := False;
         end;
     end;


     if ( lCalcular ) then
     begin
     //
        //>>ANTES
        Result :=GetMontoEvaluado( FFecInicialBim, FFecFinalBim );
        DiasCotizadosBimestre := DiasCotizados;
       //<<ANTES

        //Empleado
        with  oZetaProvider.DatosNomInfo do
        begin
             Empleado := ZetaEvaluador.GetEmpleado;
             CuotaBimestralNomInfo := Result;
             DiasCotizadosBimestreNomInfo := DiasCotizadosBimestre;
             SeguroViviendaNomInfo := FSeguroVivienda;
             iYearIMSS := FYearIMSS;
             iMesIMSS := FMesIMSS;
        end;
     end;
end;

function TZetaValorNominaInfonavit.Calculate: TQREvResult;
var
   dFecInicialNom, dFecFinalNom: TDate;
   dFechaCal : TDate;
   DiasNomina: TPesos;
   CuotaBimxCubrir  : TPesos;
   sTextoRastreo : string;
   sTextoRastreoDias : string;
   iTipoNomina, iStatusAct, iMes : integer;


   procedure  EvaluarEmpleado;
   var
      dInicio, dFin: TDate;
      iYear, iMes, iDia: Word;
   begin
        {$ifdef PROFILE}vnomProfiler.Check('TZetaValorNominaInfonavit.Calculate >> EvaluarEmpleado');{$endif}
        dInicio := FFecInicialBim;
        while ( dInicio <= FFecFinalBim ) do
        begin
             dFin := rMin( ZetaCommonTools.LastDayOfMonth( dInicio ), FFecFinalBim );
             DecodeDate( dInicio, iYear, iMes, iDia );
             with FIMSS do
             begin
                  EvaluarIMSSPagosBegin( TRUE, FALSE, FSeguroVivienda, iYear, iMes );
                  EvaluarIMSSPagosSetFechas( dInicio, dFin, False );
                  //Datos al final de periodo de ajuste por suspensiones y reinicios
                  SetGlobalEmpleadoCursor( DataSetEvaluador, dFin );
                  EvaluarIMSSPagosEnd;
             end;
             dInicio := dFin + 1;
        end;
        {$ifdef PROFILE}vnomProfiler.Check('TZetaValorNominaInfonavit.Calculate >> EvaluarEmpleado');{$endif}
   end;

begin

   FPatronInicial := VACIO;
   
   with Result do
   begin
        Kind := resDouble;
        dblResult := 0;
   end;

   if (oZetaProvider.DatosPeriodo.Mes <= 12) and (DataSetEvaluador.FieldByName('CB_INF_INI').AsDateTime <> NullDateTime )  then
   begin
   {$ifdef PROFILE}   vnomProfiler :=  TZetaProfiler.Create;
      vnomProfiler.Start;
      vnomProfiler.Check('TZetaValorNominaInfonavit.Calculate >> Prepara Query');
      Randomize;
   {$endif}

      with DataSetEvaluador do
      begin
         FFecNom := FieldByName('CB_FEC_NOM').AsDateTime;
         FFecBaj := FieldByName('CB_FEC_BAJ').AsDateTime;
         FFecIng := FieldByName('CB_FEC_ING').AsDateTime;
         FEmpActivo := zStrToBool( FieldByName('CB_ACTIVO').AsString );
         FTipoNomina := FieldByName('PE_TIPO').AsInteger;
         FPatronIniBim := FieldByName('CB_PATRON').AsString;
         FPatronFinBim := FPatronIniBim;
      end;

      SetFechasBimestre;
      EvaluarEmpleado;
      FEmpleadoIMSS := FIMSS.Empleado;
   {$ifdef PROFILE}vnomProfiler.Check('TZetaValorNominaInfonavit.Calculate >> SetFechasBimestre');{$endif}
      dFecInicialNom := 0;
      dFecFinalNom := 0;
      CuotaBimxCubrir := 0.00;

      with Result do
      begin
           Kind := resDouble;
           dblResult := 0;
      end;

      if ( FIMSS.Empleado.PrestamoTipo <> tiNoTiene )  then
      begin
             if ( ZetaEvaluador.Rastreando            )  then
             begin
                    with ZetaEvaluador.Rastreador do
                    begin
                          RastreoHeader('CALCULO DE CUOTA BIMESTRAL POR CUBRIR');
                          RastreoEspacio;
                    end;
             end;



             SetRangoFechas;
          {$ifdef PROFILE}   vnomProfiler.Check('TZetaValorNominaInfonavit.Calculate >> SetRangoFechas'); {$endif}
             CargaPeriodosBimestre;
          {$ifdef PROFILE}   vnomProfiler.Check('TZetaValorNominaInfonavit.Calculate >> CargaPeriodosBimestre'); {$endif}

             //TODO: Mover esto a un funcion
             DiasCotizadosNomina := 0;
             DiasNomina := 0;

             dFecInicialNom   := NullDateTime;
             dFecFinalNom     := NullDateTime;

             sTextoRastreoDias := VACIO;
             sTextoRastreoDias:=                  '               DIAS CONTABILIZADOS PARA EL FACTOR ' + CR_LF;
             sTextoRastreoDias:= sTextoRastreo +  '                    Fecha         Tipo de N�mina'  + CR_LF;

             dFechaCal := FFecInicialRango;

          {$ifdef PROFILE}  vnomProfiler.Check('TZetaValorNominaInfonavit.Calculate >> Ajustando Rango de Fechas'); {$endif}

             while  dFechaCal <= FFecFinalRango do
             begin
          {$ifdef PROFILE}        vnomProfiler.Check('TZetaValorNominaInfonavit.Calculate >> Ajustando Rango de Fechas >> GetStatusActivo');{$endif}
                  iStatusAct  :=  GetStatusActivo( dFechaCal );
          {$ifdef PROFILE}        vnomProfiler.Check('TZetaValorNominaInfonavit.Calculate >> Ajustando Rango de Fechas >> GetStatusActivo');{$endif}

                  if ( iStatusAct = K_ACTIVO_SI ) then
                  begin
          {$ifdef PROFILE}vnomProfiler.Check('TZetaValorNominaInfonavit.Calculate >> Ajustando Rango de Fechas >> GetTipoNomina');{$endif}
                       iTipoNomina :=  GetTipoNomina( dFechaCal );
          {$ifdef PROFILE}             vnomProfiler.Check('TZetaValorNominaInfonavit.Calculate >> Ajustando Rango de Fechas >> GetTipoNomina');{$endif}
                       if ( iTipoNomina >= 0 ) then
                       begin
          {$ifdef PROFILE}  vnomProfiler.Check('TZetaValorNominaInfonavit.Calculate >> Ajustando Rango de Fechas >> GetMesIMSS');{$endif}
                            iMes :=  GetMesIMSS( dFechaCal, iTipoNomina);
          {$ifdef PROFILE}  vnomProfiler.Check('TZetaValorNominaInfonavit.Calculate >> Ajustando Rango de Fechas >> GetMesIMSS');{$endif}

                            if( iMes in [ FMesIMSS,  FMesIMSS+1 ] ) then
                            begin
                                if dFecInicialNom = NullDateTime then
                                      dFecInicialNom := dFechaCal;
                                dFecFinalNom := dFechaCal;
                                DiasNomina:= DiasNomina + 1;
                            end;
                       end;
                  end;

                  dFechaCal := dFechaCal + 1;
             end;

          {$ifdef PROFILE}   vnomProfiler.Check('TZetaValorNominaInfonavit.Calculate >> Ajustando Rango de Fechas'); {$endif}
           SetPatronesBimestreNominal( dFecInicialNom, dFecFinalNom) ;
           CuotaBimxCubrir := GetCuotaBimPorCubrir;
          {$ifdef PROFILE}   vnomProfiler.Check('TZetaValorNominaInfonavit.Calculate >> GetCuotaBimPorCubrir');{$endif}

             if (dFecFinalNom >= dFecInicialNom ) then
             begin

                  if ( ZetaEvaluador.Rastreando            )  then
                  begin
                         with ZetaEvaluador.Rastreador do
                         begin
                              RastreoEspacio;
                              {$ifdef ANTES}
                              RastreoHeader('CALCULO DE DIAS COTIZADOS NOMINA');
                              {$else}
                              RastreoHeader('AJUSTE PARA DIAS COTIZADOS NOMINA');
                              {$endif}
                              RastreoEspacio;
                         end;
                  end;


                  DiasCotizadosNomina := GetDiasCotizadosNomina( dFecInicialNom,dFecFinalNom );
          {$ifdef PROFILE} vnomProfiler.Check('TZetaValorNominaInfonavit.Calculate >> GetDiasCotizadosNomina');  {$endif}

             end;

             Self.FFecInicialRango := dFecInicialNom;
             Self.FFecFinalRango   := dFecFinalNom;

             with Result do
             begin
                  Kind := resDouble;
                  dblResult := 0;
                  if (DiasCotizadosNomina > 0) then
                  begin
                       dblResult :=  CuotaBimxCubrir / DiasCotizadosNomina;
                       if Redondear then
                          dblResult := Redondea( dblResult );
                  end;
             end;
      end;

      if ( ZetaEvaluador.Rastreando            )  then
      begin
           with ZetaEvaluador.Rastreador do
           begin
                with FImss.Empleado do
                begin
                     RastreoHeader( 'CALCULO DEL FACTOR DIARIO DE NOMINA' );
                     RastreoEspacio;
                     RastreoTextoLibre( 'Tipo de Cr�dito    : ' + ZetaCommonLists.ObtieneElemento( lfTipoInfonavit, Ord( PrestamoTipo ) ) );

                     if ( PrestamoTipo <> tiNoTiene ) then
                     begin
                          RastreoTextoLibre ( 'Fecha de Inicio    : ' + FormatDateTime( 'dd/mmm/yyyy', DataSetEvaluador.FieldByName( 'CB_INF_INI' ).AsDateTime ) );
                          if ( FechaSuspension <> NullDateTime ) then
                            RastreoTextoLibre ( 'Fecha de Suspensi�n: ' + FormatDateTime( 'dd/mmm/yyyy', FechaSuspension ) );
                          RastreoTextoLibre( 'Valor de Descuento : ' + Format( '%11.4n', [ FImss.Empleado.Prestamo ] ) );

                          if ( PrestamoTipo = tiPorcentaje ) then
                            RastreoTextoLibre( 'Reducci�n de Tasa  : ' + BoolAsSiNo( AplicaDisminucion )  );

                          RastreoTextoLibre( 'Fecha Inicial de N�minas  : '+ FormatDateTime( 'dd/mmm/yyyy', dFecInicialNom ) );
                          RastreoTextoLibre( 'Fecha Final de N�minas    : '+ FormatDateTime( 'dd/mmm/yyyy', dFecFinalNom )   );
                          RastreoEspacio;
                          RastreoTextoLibre('        Cuota Bimestral por Cubrir : ' + Format( ' %11.2n ', [ CuotaBimxCubrir ] ) );
                          RastreoTextoLibre('       (/)D�as Cotizados de N�mina : ' + Format( ' %11.2n ', [ DiasCotizadosNomina ] ) ) ;
                          RastreoTextoLibre('Factor INFONAVIT por D�a de N�mina : ' + Format( ' %11.2n ', [ Result.dblResult ] ));



                          RastreoTextoLibre(sTextoRastreo);


                     end;
                end;
           end;
      end;

   {$ifdef PROFILE}     vnomProfiler.Report( Format( 'C:\TEMP\PROFILE_V_NOM_EMP_%d_%d_%d.LOG', [ZetaEvaluador.GetEmpleado, Random(1000),Random(100000)]) ); {$endif}
   end;

end;

function TZetaValorNominaInfonavit.EvaluaResultado( const Liquidacion: TLiquidacionEmpleado ): TPesos;
begin
     Result := Liquidacion.LE_INF_AMO;      // INFONAVIT - Amortizacion del Empleado
     //DiasCotizadosNomina := DiasCotizadosNomina + Liquidacion.LE_INF_DIAS;
     DiasCotizados := DiasCotizados + Liquidacion.LE_INF_DIAS;
end;

{*********************** TZetaAjustaInfoCredito ************************}

type
  TZetaAjustaInfoCredito = class( TZetaEvaluaInfoCredito )
  private
    FQuery: TZetaCursor;
    FQueryPer{$ifdef CAMBIO_TNOM}, FQueryPerOtro, FTipoNominaEmp{$endif}: TZetaCursor;
    FIMSSAjuste: TIMSS;
    TipoAjuste: eTipoAjustaINFO;
    function GetAcumulado: TPesos;
    function GetAjusteInicio: TDate;
    function GetMontoAjustar( const dFecInicial, dFecFinal: TDate ): TPesos;
    {$ifdef CAMBIO_TNOM}
    function GetInicioNomina: TDate;
    function GetTNomInicio( const iEmpleado: Integer ): eTipoPeriodo;
    {$endif}
    procedure PreparaQuerys;
  public
    constructor Create( oEvaluador:TQrEvaluator ); override;
    destructor Destroy; override;
    function Calculate: TQREvResult; override;
  end;

constructor TZetaAjustaInfoCredito.Create( oEvaluador:TQrEvaluator );
begin
     inherited Create( oEvaluador );
     FIMSSAjuste := TIMSS.Create( ZetaEvaluador.oZetaCreator );
     FOffSet := 3;
end;

destructor TZetaAjustaInfoCredito.Destroy;
begin
     FreeAndNil( FTipoNominaEmp );
     FreeAndNil( FQueryPerOtro );
     FreeAndNil( FIMSSAjuste );
     FreeAndNil( FQueryPer );
     FreeAndNil( FQuery );
     inherited Destroy;
end;

procedure TZetaAjustaInfoCredito.PreparaQuerys;
const
     K_DEFAULT_CONCEPTO_INFO = '54';
     K_PRIMER_PERIODO_MES = 1;
     K_QRY_PERIODO_INI = 'select PE_ASI_INI from PERIODO where ( PE_YEAR = %d ) and ( PE_TIPO = %d ) and ( PE_POS_MES = %d ) and ( PE_MES = :Mes )';
{$ifdef INTERBASE}
     K_QRY_ACUMULADO = 'select RESULTADO from SP_AS( %s, :MesIni, :MesFin, :Anio, :Empleado )';
{$endif}
{$ifdef MSSQL}
     K_QRY_ACUMULADO = 'select RESULTADO = DBO.SP_AS( %s, :MesIni, :MesFin, :Anio, :Empleado )';
{$endif}

{$ifdef CAMBIO_TNOM}
     K_QRY_PERIODO_OTRO = 'select PE_ASI_INI, PE_STATUS from PERIODO where ( PE_TIPO = :TipoNominaOtro ) and ' +
                          '( :FechaCNomina1 >= PE_ASI_INI ) AND ( :FechaCNomina2 <= PE_ASI_FIN )';
{$ifdef MSSQL}
     K_TIPO_NOMINA_EMP = 'select( DBO.SP_GET_NOMTIPO( %0:s, :Empleado ) ) CB_NOMINA ';
{$endif}
{$ifdef INTERBASE}
     K_TIPO_NOMINA_EMP = 'select TIPNOM from SP_GET_NOMTIPO( %0:s, :Empleado ) CB_NOMINA ';
{$endif}
{$endif}

begin
     with ZetaEvaluador.oZetaProvider do
     begin
          if ( FQuery = nil ) then
             FQuery := CreateQuery( Format( K_QRY_ACUMULADO, [ K_DEFAULT_CONCEPTO_INFO ] ) );
          if ( FQueryPer = nil ) then
          begin
               with DatosPeriodo do
                    FQueryPer := CreateQuery( Format( K_QRY_PERIODO_INI, [ Year, Ord( Tipo ), K_PRIMER_PERIODO_MES ] ) );
          end;
          {$ifdef CAMBIO_TNOM}
          FQueryPerOtro := CreateQuery( K_QRY_PERIODO_OTRO );
          FTipoNominaEmp:= CreateQuery( Format( K_TIPO_NOMINA_EMP, [DateToStrSQLC( DatosPeriodo.InicioAsis )] ) );
          {$endif}
     end;
end;

 {$ifdef CAMBIO_TNOM}
function TZetaAjustaInfoCredito.GetTNomInicio( const iEmpleado: Integer ): eTipoPeriodo;
begin
     with oZetaProvider do
     begin
          with FTipoNominaEmp do
          begin
               Result:= Ord(tpDiario);
               Active:= False;
               ParamAsInteger( FTipoNominaEmp, 'Empleado', iEmpleado );
               Active:= True;
               if not IsEmpty then
               begin
                    Result:= eTipoPeriodo( FTipoNominaEmp.FieldByName('CB_NOMINA').AsInteger );
               end;
          end;
     end;
end;

function TZetaAjustaInfoCredito.GetInicioNomina: TDate;
var
   dInicioPerOtro: TDate;
   eStatusNomina: eStatusPeriodo;
   eOtroPeriodo: eTipoPeriodo;
begin
     { Cuando la n�mina del cambio de tipo de n�mina se encuentre afectada, ya no se tienen que considerar esos dias, de lo contrario
       se debe ajustar de acuerdo al periodo de la otra n�mina}
     with DataSetEvaluador do
     begin
          with ZetaEvaluador.oZetaProvider do
          begin
               Result:= FieldByName('NO_ASI_INI').AsDateTime;
               if ( EsCambioTNomPeriodo( FieldByName('CB_FEC_NOM').AsDateTime, DatosPeriodo ) or
                    ( FieldByName('CB_FEC_NOM').AsDateTime > DatosPeriodo.Fin ) ) then
               begin
                    if ( Result > DatosPeriodo.InicioAsis ) then
                    begin
                         eOtroPeriodo:= GetTNomInicio( FieldByName('CB_CODIGO').AsInteger );
                         if ( eOtroPeriodo <> DatosPeriodo.Tipo ) then
                         begin
                              with FQueryPerOtro do
                              begin
                                   ParamAsInteger( FQueryPerOtro, 'TipoNominaOtro', Ord( eOtroPeriodo ) );
                                   ParamAsDateTime( FQueryPerOtro, 'FechaCNomina1', Result );
                                   ParamAsDateTime( FQueryPerOtro, 'FechaCNomina2', Result );
                                   Active:= True;
                                   eStatusNomina:= eStatusPeriodo( FieldByName('PE_STATUS').AsInteger );
                                   dInicioPerOtro:= FieldByName( 'PE_ASI_INI').AsDateTime;
                                   Active:= False;
                              end;

                              if ( eStatusNomina <> spAfectadaTotal ) then
                              begin
                                   Result:= dInicioPerOtro;
                              end;
                         end;
                    end;
               end;
          end;
     end;
end;
{$endif}

function TZetaAjustaInfoCredito.GetAcumulado: TPesos;
const
     K_MESS_ERROR = 'Error al calcular la funci�n A(54,%s)';
var
   iMesInicio: Integer;
begin
     if ParamVacio( 1 ) then
     begin
          Result := 0;
          try
             with ZetaEvaluador.oZetaProvider do
             begin
                  if ( TipoAjuste <> taiFinMes ) and ( ( MesDefault mod 2 ) = 0 ) then
                     iMesInicio := MesDefault - 1  // Si es mes par se revisa desde el mes anterior
                  else
                      iMesInicio := MesDefault;

                  FQuery.Active := FALSE;
                  ParamAsInteger( FQuery, 'MesIni', iMesInicio );
                  ParamAsInteger( FQuery, 'MesFin', MesDefault );
                  ParamAsInteger( FQuery, 'Anio', YearDefault );
                  ParamAsInteger( FQuery, 'Empleado', ZetaEvaluador.GetEmpleado );

                  with FQuery do
                  begin
                       Active := TRUE;
                       Result := FieldByName( 'RESULTADO' ).AsFloat;
                  end;
             end;
          except
                ErrorFuncion( Format( K_MESS_ERROR, [ IntToStr( BoolToInt( TipoAjuste <> taiFinMes ) + 1 ) ] ), 'AJUS_INF_X()' );
          end;
     end
     else
     begin
          Result := ParamFloat( 1 );
     end;
end;

function TZetaAjustaInfoCredito.GetAjusteInicio: TDate;
const
     K_MESS_ERROR = 'Error al revisar periodo de amortizaci�n';
var
   iMes: Integer;
begin
     Result := NullDateTime;
     try
        with ZetaEvaluador.oZetaProvider do
        begin
             if ( TipoAjuste <> taiFinMes ) and ( ( MesDefault mod 2 ) = 0 ) then
                iMes := MesDefault - 1  // Si es mes par se revisa desde el mes anterior
             else
                 iMes := MesDefault;

             FQueryPer.Active := FALSE;
             ParamAsInteger( FQueryPer, 'Mes', iMes );

             with FQueryPer do
             begin
                  Active := TRUE;
                  if ( not EOF ) then
                     Result := FieldByName( 'PE_ASI_INI' ).AsDateTime
                  else
                      ErrorFuncion( K_MESS_ERROR, 'AJUS_INF_X()' );
             end;
        end;
     except
           ErrorFuncion( K_MESS_ERROR, 'AJUS_INF_X()' );
     end;
end;

function TZetaAjustaInfoCredito.GetMontoAjustar( const dFecInicial, dFecFinal: TDate ): TPesos;
var
   dInicio, dFin: TDate;
   iYear, iMes, iDia: Word;
begin
     Result := 0;
     dInicio := dFecInicial;
     while ( dInicio <= dFecFinal ) do
     begin
          dFin := rMin( ZetaCommonTools.LastDayOfMonth( dInicio ), dFecFinal );
          DecodeDate( dInicio, iYear, iMes, iDia );
          with FIMSSAjuste do
          begin
               EvaluarIMSSPagosBegin( TRUE, FALSE, FSeguroVivienda, iYear, iMes );
               EvaluarIMSSPagosSetFechas( dInicio, dFin, FRastreoINFO );
			   //Datos al final de periodo de ajuste por suspensiones y reinicios
               SetGlobalEmpleadoCursor( DataSetEvaluador, dFin );
               CalcularIMSSPagos;
               Result := Result + EvaluaResultado( LiquidacionEmpleado );
               EvaluarIMSSPagosEnd;
          end;
          dInicio := dFin + 1;
     end;
end;

function TZetaAjustaInfoCredito.Calculate: TQREvResult;
var
   lAjustar: Boolean;
   TipoInfo: eTipoInfonavit;
   rPago, rAcumulado, rMonto, rAjuste: TPesos;
begin
     {Inicializar variables}
     Result := inherited Calculate;
     lAjustar := FALSE;
     TipoInfo := eTipoInfonavit( DataSetEvaluador.FieldByName( 'CB_INFTIPO' ).AsInteger );

     with ZetaEvaluador do
     begin
          if CalculandoNomina  and  ( TipoInfo <> tiNoTiene ) { Se ajusta a todos los tipos } then
          begin
               TipoAjuste := eTipoAjustaINFO( DefaultInteger( 0, 1 ) - 1  );
               with oZetaProvider.DatosPeriodo do
               begin
                    {$ifdef ANTES}
                    { AP(20/10/2008): Para ajustar vacaciones es necesario ajustar en el primer periodo}
                    if ( ( PosMes > 1 ) or ( ( Mes mod 2 ) = 0 ) ) then  // No ajustar en primer periodo del bimestre
                    {$endif}
                    lAjustar := ( TipoAjuste = taiSiempre );
                    if ( not lAjustar ) then
                    begin
                         lAjustar := ( PosMes = PerMes );
                         if ( lAjustar and ( TipoAjuste = taiFinBimestre ) ) then
                            lAjustar := Mes in [ 2, 4, 6, 8, 10, 12 ];
                    end;
               end;
          end;
          if lAjustar then
          begin
               if Rastreando then
               begin
                    Rastreador.RastreoTextoLibre( Format('%s C�lculo del periodo:  $%9.2n',[ StringOfChar( ' ', 34 ), Result.dblResult ] ) );
                    Rastreador.RastreoHeader( 'AJUSTE DESCUENTO INFONAVIT' );
                    Rastreador.RastreoEspacio;
               end;

               PreparaQuerys;
               rPago := Result.dblResult;
               rAcumulado := GetAcumulado;
               {$ifdef CAMBIO_TNOM}
               rMonto := Redondea( GetMontoAjustar( GetAjusteInicio, GetInicioNomina - 1 ) );
               {$else}
               rMonto := Redondea( GetMontoAjustar( GetAjusteInicio, FieldByName('NO_ASI_INI').AsDateTime - 1 ) );
               {$endif}
               rAjuste := Redondea( rMonto - rAcumulado );
               Result.dblResult := rMax( Redondea( rPago + rAjuste ), 0 );
               if ZetaEvaluador.Rastreando then
               begin
                    with ZetaEvaluador.Rastreador do
                    begin
                         RastreoPesos( '                      Amortizaci�n calculada :  ', rMonto );
                         RastreoHeader('');
                         RastreoPesos( '                      Amortizaci�n calculada :  ', rMonto );
                         RastreoPesos( '(-) Descuento acumulado (Param. #2 funci�n ) :  ', rAcumulado );
                         RastreoPesos( '(=)                   Ajuste de amortizaci�n :  ', rAjuste );
                         RastreoEspacio;
                         RastreoHeader('');
                         RastreoPesos( '                      Ajuste de amortizaci�n :  ', rAjuste );
                         RastreoPesos( '(+)                      C�lculo del periodo :  ', rPago   );
                         RastreoPesos( '(=)             Monto Calculado del Concepto :  ', Result.dblResult );
                    end;
               end;
          end;
     end;
end;

{*********************** TZetaAjustaInfoCreditoEnNomina ************************}
type
  TZetaAjustaInfoCreditoEnNomina = class( TZetaEvaluaIMSSBase)
  private
    FQuery: TZetaCursor;
    FFactorNomina : TPesos;
    FFactorNominaRedondeado  : TPesos;
    FQueryPerOtro, FTipoNominaEmp : TZetaCursor;
    FIMSSAjuste: TIMSS;
    TipoAjuste: eTipoAjustaINFO;
    FDiasCotizadosNomina : TPesos;
    ZValorDiaInfo : TZetaValorNominaInfonavit;

    function GetAcumulado: TPesos;
    function GetAcumuladoVacaciones: TPesos;
    function GetNominaVacaciones: TPesos;
    function GetAjusteInicio: TDate;
    function GetDiasAjustar( const dFecInicial, dFecFinal: TDate ): TPesos;
    function GetDiasAjustarUtil( const dFecInicial, dFecFinal: TDate; const sPatronInicial : string ): TPesos;
    {$ifdef CAMBIO_TNOM}
    function GetInicioNomina: TDate;
    function GetTNomInicio( const iEmpleado: Integer ): eTipoPeriodo;
    {$endif}
    function GetMontoPorNomina : TPesos;
    procedure PreparaQuerys;
    procedure SetFactorNomina;

  protected
    function EvaluaResultado( const Liquidacion: TLiquidacionEmpleado ): TPesos; override;
  public
    constructor Create( oEvaluador:TQrEvaluator ); override;
    destructor Destroy; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;

  end;

constructor TZetaAjustaInfoCreditoEnNomina.Create( oEvaluador:TQrEvaluator );
begin                                  inherited Create( oEvaluador );
     FIMSSAjuste := TIMSS.Create( ZetaEvaluador.oZetaCreator );
     FOffSet := 3;
end;

destructor TZetaAjustaInfoCreditoEnNomina.Destroy;
begin
     FreeAndNil( FTipoNominaEmp );
     FreeAndNil( FQueryPerOtro );
     FreeAndNil( FIMSSAjuste );
     FreeAndNil( FQuery );
     inherited Destroy;
end;

procedure TZetaAjustaInfoCreditoEnNomina.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     inherited GetRequeridos(TipoRegresa);
     AgregaRequerido( 'NOMINA.NO_DIAS_VJ', enNomina );
     AgregaRequerido( 'NOMINA.NO_DIAS_SS', enNomina );
     AgregaRequerido( 'NOMINA.PE_TIPO', enNomina );
     AgregaRequerido( 'COLABORA.CB_ACTIVO', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_FEC_ING', enEmpleado );
     AgregaRequerido( 'COLABORA.CB_FEC_BAJ', enEmpleado );
end;

procedure TZetaAjustaInfoCreditoEnNomina.SetFactorNomina;
begin
    zValorDiaInfo := TZetaValorNominaInfonavit.Create( Self.ZetaEvaluador );
    zValorDiaInfo.Redondear := False;
    FFactorNomina := zValorDiaInfo.Calculate.dblResult;

    FFactorNominaRedondeado := Redondea( FFactorNomina );

    FIMSSAjuste.UsarFactorNominaInfonavit( FFactorNomina );
    FIMSS.UsarFactorNominaInfonavit( FFactorNomina );

end;

function TZetaAjustaInfoCreditoEnNomina.EvaluaResultado( const Liquidacion: TLiquidacionEmpleado ): TPesos;
begin
     Result := Liquidacion.LE_INF_AMO;      // INFONAVIT - Amortizacion del Empleado
     FDiasCotizadosNomina :=  FDiasCotizadosNomina +  Liquidacion.LE_INF_DIAS;
end;

procedure TZetaAjustaInfoCreditoEnNomina.PreparaQuerys;
const
     K_PRIMER_PERIODO_MES = 1;
     K_QRY_PERIODO_INI = 'select PE_ASI_INI from PERIODO where ( PE_YEAR = %d ) and ( PE_TIPO = %d ) and ( PE_POS_MES = %d ) and ( PE_MES = :Mes )';
{$ifdef INTERBASE}
     K_QRY_ACUMULADO = 'select RESULTADO from SP_AS( :Concepto, :MesIni, :MesFin, :Anio, :Empleado )';
{$endif}
{$ifdef MSSQL}
     K_QRY_ACUMULADO = 'select RESULTADO = DBO.SP_AS( :Concepto, :MesIni, :MesFin, :Anio, :Empleado )';
{$endif}

{$ifdef CAMBIO_TNOM}
     K_QRY_PERIODO_OTRO = 'select PE_ASI_INI, PE_STATUS from PERIODO where ( PE_TIPO = :TipoNominaOtro ) and ' +
                          '( :FechaCNomina1 >= PE_ASI_INI ) AND ( :FechaCNomina2 <= PE_ASI_FIN )';
{$ifdef MSSQL}
     K_TIPO_NOMINA_EMP = 'select( DBO.SP_GET_NOMTIPO( %0:s, :Empleado ) ) CB_NOMINA ';
{$endif}
{$ifdef INTERBASE}
     K_TIPO_NOMINA_EMP = 'select TIPNOM from SP_GET_NOMTIPO( %0:s, :Empleado ) CB_NOMINA ';
{$endif}
{$endif}

begin
     with ZetaEvaluador.oZetaProvider do
     begin
          if ( FQuery = nil ) then
             FQuery := CreateQuery(K_QRY_ACUMULADO);
          {$ifdef CAMBIO_TNOM}
          FQueryPerOtro := CreateQuery( K_QRY_PERIODO_OTRO );
          FTipoNominaEmp:= CreateQuery( Format( K_TIPO_NOMINA_EMP, [DateToStrSQLC( DatosPeriodo.InicioAsis )] ) );
          {$endif}
     end;
end;

 {$ifdef CAMBIO_TNOM}
function TZetaAjustaInfoCreditoEnNomina.GetTNomInicio( const iEmpleado: Integer ): eTipoPeriodo;
begin
     with oZetaProvider do
     begin
          with FTipoNominaEmp do
          begin
               Result:= Ord(tpDiario);
               Active:= False;
               ParamAsInteger( FTipoNominaEmp, 'Empleado', iEmpleado );
               Active:= True;
               if not IsEmpty then
               begin
                    Result:= eTipoPeriodo( FTipoNominaEmp.FieldByName('CB_NOMINA').AsInteger );
               end;
          end;
     end;
end;

function TZetaAjustaInfoCreditoEnNomina.GetInicioNomina: TDate;
var
   dInicioPerOtro: TDate;
   eStatusNomina: eStatusPeriodo;
   eOtroPeriodo: eTipoPeriodo;
begin
     { Cuando la n�mina del cambio de tipo de n�mina se encuentre afectada, ya no se tienen que considerar esos dias, de lo contrario
       se debe ajustar de acuerdo al periodo de la otra n�mina}
     with DataSetEvaluador do
     begin
          with ZetaEvaluador.oZetaProvider do
          begin
               Result:= FieldByName('NO_ASI_INI').AsDateTime;
               if ( EsCambioTNomPeriodo( FieldByName('CB_FEC_NOM').AsDateTime, DatosPeriodo ) or
                    ( FieldByName('CB_FEC_NOM').AsDateTime > DatosPeriodo.Fin ) ) then
               begin
                    if ( Result > DatosPeriodo.InicioAsis ) then
                    begin
                         eOtroPeriodo:= GetTNomInicio( FieldByName('CB_CODIGO').AsInteger );
                         if ( eOtroPeriodo <> DatosPeriodo.Tipo ) then
                         begin
                              with FQueryPerOtro do
                              begin
                                   ParamAsInteger( FQueryPerOtro, 'TipoNominaOtro', Ord( eOtroPeriodo ) );
                                   ParamAsDateTime( FQueryPerOtro, 'FechaCNomina1', Result );
                                   ParamAsDateTime( FQueryPerOtro, 'FechaCNomina2', Result );
                                   Active:= True;
                                   eStatusNomina:= eStatusPeriodo( FieldByName('PE_STATUS').AsInteger );
                                   dInicioPerOtro:= FieldByName( 'PE_ASI_INI').AsDateTime;
                                   Active:= False;
                              end;

                              if ( eStatusNomina <> spAfectadaTotal ) then
                              begin
                                   Result:= dInicioPerOtro;
                              end;
                         end;
                    end;
               end;
          end;
     end;
end;
{$endif}

function TZetaAjustaInfoCreditoEnNomina.GetAcumulado: TPesos;
const
     K_MESS_ERROR = 'Error al calcular la funci�n A(54,%s)';
     K_DEFAULT_CONCEPTO_INFO = '54';
var
   iMesInicio: Integer;
begin
     if ParamVacio( 0 ) then
     begin
          Result := 0;
          try
             with ZetaEvaluador.oZetaProvider do
             begin
                  if ( TipoAjuste <> taiFinMes ) and ( ( MesDefault mod 2 ) = 0 ) then
                     iMesInicio := MesDefault - 1  // Si es mes par se revisa desde el mes anterior
                  else
                      iMesInicio := MesDefault;

                  FQuery.Active := FALSE;
                  ParamAsString(  FQuery, 'Concepto', K_DEFAULT_CONCEPTO_INFO );
                  ParamAsInteger( FQuery, 'MesIni', iMesInicio );
                  ParamAsInteger( FQuery, 'MesFin', MesDefault );
                  ParamAsInteger( FQuery, 'Anio', YearDefault );
                  ParamAsInteger( FQuery, 'Empleado', ZetaEvaluador.GetEmpleado );

                  with FQuery do
                  begin
                       Active := TRUE;
                       Result := FieldByName( 'RESULTADO' ).AsFloat;
                  end;
             end;
          except
                ErrorFuncion( Format( K_MESS_ERROR, [ IntToStr( BoolToInt( TipoAjuste <> taiFinMes ) + 1 ) ] ), 'AJUS_INF_N()' );
          end;
     end
     else
     begin
          Result := ParamFloat( 0 );
     end;
end;

function TZetaAjustaInfoCreditoEnNomina.GetAcumuladoVacaciones: TPesos;
const
     K_MESS_ERROR = 'Error al calcular la funci�n A(1123,2)';
     K_CONCEPTO_INFO_ACUMULA_FALTAS_VACA = '1123';
var
   iMesInicio: Integer;
begin
     if ParamVacio( 1 ) then
     begin
          Result := 0;
          try
             with ZetaEvaluador.oZetaProvider do
             begin
                  if ( TipoAjuste <> taiFinMes ) and ( ( MesDefault mod 2 ) = 0 ) then
                     iMesInicio := MesDefault - 1  // Si es mes par se revisa desde el mes anterior
                  else
                      iMesInicio := MesDefault;

                  FQuery.Active := FALSE;
                  ParamAsString(  FQuery, 'Concepto', K_CONCEPTO_INFO_ACUMULA_FALTAS_VACA );
                  ParamAsInteger( FQuery, 'MesIni', iMesInicio );
                  ParamAsInteger( FQuery, 'MesFin', MesDefault );
                  ParamAsInteger( FQuery, 'Anio', YearDefault );
                  ParamAsInteger( FQuery, 'Empleado', ZetaEvaluador.GetEmpleado );

                  with FQuery do
                  begin
                       Active := TRUE;
                       //TO-DO Agregar Rastreo
                       Result := FieldByName( 'RESULTADO' ).AsFloat  * FFactorNomina;
                  end;
             end;
          except
                ErrorFuncion( Format( K_MESS_ERROR, [ IntToStr( BoolToInt( TipoAjuste <> taiFinMes ) + 1 ) ] ), 'AJUS_INF_N()' );
          end;
     end
     else
     begin
          Result := ParamFloat( 1 );
     end;
end;

function TZetaAjustaInfoCreditoEnNomina.GetNominaVacaciones: TPesos;
begin
     if ParamVacio( 2 ) then
     begin
          with ZetaEvaluador do
          begin
               Result := DataSetEvaluador.FieldByName('NO_DIAS_VJ').AsFloat * 7/6;
               Result := Result *  FFactorNomina;
          end;
     end
     else
     begin
          Result := ParamFloat( 2 );
     end;
end;

function TZetaAjustaInfoCreditoEnNomina.GetAjusteInicio: TDate;
begin
     Result := ZValorDiaInfo.FFecInicialRango;
end;

function TZetaAjustaInfoCreditoEnNomina.GetDiasAjustarUtil( const dFecInicial, dFecFinal: TDate; const sPatronInicial : string ): TPesos;
var
   dInicio, dFin, dFinAsistencia: TDate;
   iYear, iMes, iDia: Word;
   diasCotizados : TPesos;
begin
     with DataSetEvaluador do
     begin
          dFinAsistencia   := DefaultDate( FOffSet + 1, FieldByName( 'NO_ASI_FIN' ).AsDateTime );
     end;

     diasCotizados := 0;
     dInicio := dFecInicial;
     while ( dInicio <= dFecFinal ) do
     begin
          dFin := rMin( ZetaCommonTools.LastDayOfMonth( dInicio ), dFecFinal );
          DecodeDate( dInicio, iYear, iMes, iDia );
          with FIMSSAjuste do
          begin
               UsarLimiteRevisionFaltasInfonavit( dFinAsistencia );
               EvaluarIMSSPagosBegin( TRUE, FALSE, FSeguroVivienda, iYear, iMes );
               EvaluarIMSSPagosSetFechas( dInicio, dFin, FRastreoINFO );
               //Datos al final de periodo de ajuste por suspensiones y reinicios
               if StrLLeno ( sPatronInicial ) then
                  SetGlobalEmpleadoCursor( DataSetEvaluador, dFin, sPatronInicial  )
               else
                  SetGlobalEmpleadoCursor( DataSetEvaluador, dFin  );
               CalcularIMSSPagos;
               diasCotizados := diasCotizados + LiquidacionEmpleado.LE_INF_DIAS;
               EvaluarIMSSPagosEnd;
               RemoverLimiteRevisionFaltasInfonavit;
          end;
          dInicio := dFin + 1;
     end;
     Result := diasCotizados;
end;

function TZetaAjustaInfoCreditoEnNomina.GetDiasAjustar( const dFecInicial, dFecFinal: TDate ): TPesos;
begin
     Result := 0;

     if ( ZValorDiaInfo.PatronIniBim = ZValorDiaInfo.PatronFinBim ) then
     begin
          Result :=  GetDiasAjustarUtil( dFecInicial, dFecFinal, VACIO );
     end
     else
     begin
          if StrLleno(ZValorDiaInfo.PatronIniBim) then
                     Result := GetDiasAjustarUtil( dFecInicial, dFecFinal, ZValorDiaInfo.PatronIniBim );

          if StrLleno(ZValorDiaInfo.PatronFinBim) then
                     Result := Result + GetDiasAjustarUtil( dFecInicial, dFecFinal, ZValorDiaInfo.PatronFinBim );
     end;
end;

function TZetaAjustaInfoCreditoEnNomina.GetMontoPorNomina : TPesos;
begin
     Result := 0;

     if ZetaEvaluador.Rastreando then
     begin
          with ZetaEvaluador.Rastreador do
          begin
               RastreoHeader( 'RASTREO DEL CALCULO DEL DESCUENTO INFONAVIT' );
               RastreoTextoLibre('');
          end;
     end;

     if ( ZValorDiaInfo.PatronIniBim = ZValorDiaInfo.PatronFinBim ) then
     begin
          FPatronInicial := VACIO;
          Result :=  (inherited Calculate).dblResult;
     end
     else
     begin
          if StrLleno(ZValorDiaInfo.PatronIniBim) then
          begin
               FPatronInicial :=  ZValorDiaInfo.PatronIniBim;
               Result := ( inherited Calculate ).dblResult;
          end;

          if StrLleno(ZValorDiaInfo.PatronFinBim) then
          begin
               FPatronInicial :=  ZValorDiaInfo.PatronFinBim;
               Result := Result +  ( inherited Calculate ).dblResult;
          end;
     end;
end;

function TZetaAjustaInfoCreditoEnNomina.Calculate: TQREvResult;
var
   lAjustar, lFechasRangoValidas: Boolean;
   TipoInfo: eTipoInfonavit;
   rPago, rAcumulado, rAcumuladoVac, rPeriodoVac, rMonto, rAjuste: TPesos;
{$ifdef PROFILE}   ajusteProfiler : TZetaProfiler;
{$endif}
begin

   FPatronInicial := VACIO;
   FFactorNomina := 0;
   FFactorNominaRedondeado := 0;
   FDiasCotizadosNomina := 0;

   with Result do
   begin
        Kind := resDouble;
        dblResult := 0.00;
   end;

   if (oZetaProvider.DatosPeriodo.Mes <= 12)  and (DataSetEvaluador.FieldByName('CB_INF_INI').AsDateTime <> NullDateTime )  then
   begin
    {$ifdef PROFILE}
         ajusteProfiler := TZetaProfiler.Create;
         ajusteProfiler.Start;
         ajusteProfiler.Check('TZetaAjustaInfoCreditoEnNomina >> SetFactorNomina');
    {$endif}
         FSeguroVivienda := DefaultFloat( FOffSet + 3, 0 );
         SetFactorNomina;
    {$ifdef PROFILE}ajusteProfiler.Check('TZetaAjustaInfoCreditoEnNomina >> SetFactorNomina');
    {$endif}

         with ZValorDiaInfo do
              lFechasRangoValidas := ( FFecInicialRango <=  FFecFinalRango )and  (FFecInicialRango <> NullDateTime);
         FDiasCotizadosNomina := 0;

    {$ifdef ANTES}
         resPago := inherited Calculate;
    {$else}
         GetMontoPorNomina;
    {$endif}

    {$ifdef PROFILE}     ajusteProfiler.Check('TZetaAjustaInfoCreditoEnNomina >> inherited Calculate');
    {$endif}
         lAjustar := FALSE;

         //TipoInfo := FImss.Empleado.PrestamoTipo;
         TipoInfo := zValorDiaInfo.EmpleadoIMSSEvaluado.PrestamoTipo;
         TipoAjuste := taiSiempre;



         with Result do
         begin
              Kind := resDouble;
              dblResult := 0.00;
         end;

         with ZetaEvaluador do
         begin
              if CalculandoNomina  and  ( TipoInfo <> tiNoTiene ) and ( lFechasRangoValidas ) { Se ajusta a todos los tipos } then
              begin
                   with oZetaProvider.DatosPeriodo do
                   begin
                        lAjustar := ( TipoAjuste = taiSiempre );
                   end;
              end;

              if lAjustar then
              begin
    {$ifdef PROFILE}ajusteProfiler.Check('TZetaAjustaInfoCreditoEnNomina >> PreparaQuerys');
    {$endif}
                   PreparaQuerys;

                   rPago := FDiasCotizadosNomina * FFactorNomina;


                   if ZetaEvaluador.Rastreando then
                   begin
                        with ZetaEvaluador.Rastreador do
                        begin
                             RastreoTextoLibre( Format('%s C�lculo del periodo:  $%9.2n',[ StringOfChar( ' ', 34 ), rPago ] ) );
                             RastreoHeader('');
                             RastreoHeader( 'AJUSTE DESCUENTO INFONAVIT' );
                        end;
                   end;
    {$ifdef PROFILE}
                   ajusteProfiler.Check('TZetaAjustaInfoCreditoEnNomina >> PreparaQuerys');
                   ajusteProfiler.Check('TZetaAjustaInfoCreditoEnNomina >> GetAcumulado');
    {$endif}
                   rAcumulado := GetAcumulado;
    {$ifdef PROFILE}
                   ajusteProfiler.Check('TZetaAjustaInfoCreditoEnNomina >> GetAcumulado');
                   ajusteProfiler.Check('TZetaAjustaInfoCreditoEnNomina >> GetAcumuladoVacaciones');
    {$endif}
                   rAcumuladoVac := GetAcumuladoVacaciones;

    {$ifdef PROFILE} ajusteProfiler.Check('TZetaAjustaInfoCreditoEnNomina >> GetAcumuladoVacaciones');
    {$endif}

                   rAcumuladoVac := Redondea( rAcumuladoVac );
                   rPeriodoVac   := Redondea( GetNominaVacaciones );

                   rMonto := Redondea( GetDiasAjustar( GetAjusteInicio, GetInicioNomina - 1 ) * FFactorNomina  ) ;


                   rAjuste := Redondea( rMonto - rAcumulado - rAcumuladoVac - rPeriodoVac);

                   Result.dblResult := Redondea( rPago + rAjuste );

                   if ZetaEvaluador.Rastreando then
                   begin
                        with ZetaEvaluador.Rastreador do
                        begin
                             RastreoPesos( '                      Amortizaci�n calculada :  ', rMonto );
                             RastreoPesos( '(-)                      Descuento acumulado :  ', rAcumulado );
                             RastreoPesos( '(-)           Faltas x Vacaciones acumuladas :  ', rAcumuladoVac );
                             RastreoPesos( '(-)              Faltas x Vacaciones periodo :  ', rPeriodoVac );
                             RastreoPesos( '(=)                   Ajuste de amortizaci�n :  ', rAjuste );
                             RastreoEspacio;
                             RastreoHeader('');
                             RastreoPesos( '                      Ajuste de amortizaci�n :  ', rAjuste );
                             RastreoPesos( '(+)                      C�lculo del periodo :  ', rPago   );
                             RastreoPesos( '(=)             Monto Calculado del Concepto :  ', Result.dblResult );
                        end;
                   end;
              end;
         end;

    {$ifdef PROFILE}
         ajusteProfiler.Report('C:\TEMP\PROFILE_AJUSINFN.LOG');
         FreeAndNil( ajusteProfiler );
    {$endif}
   end;
end;

{*********************** TZetaEvaluaInfoPatron ************************}
type
  TZetaEvaluaInfoPatron = class( TZetaEvaluaIMSSBase )
  protected
    function EvaluaResultado( const Liquidacion: TLiquidacionEmpleado ): TPesos; override;
  public
    function Calculate: TQREvResult; override;
  end;

function TZetaEvaluaInfoPatron.Calculate: TQREvResult;
begin
     if ZetaEvaluador.Rastreando then
        ZetaEvaluador.Rastreador.RastreoHeader( 'RASTREO DE INFONAVIT PATRONAL' );
     Result := inherited Calculate;
end;

function TZetaEvaluaInfoPatron.EvaluaResultado( const Liquidacion: TLiquidacionEmpleado ): TPesos;
begin
     Result := Liquidacion.LE_INF_PAT;      // INFONAVIT Patronal
end;

{********************** FUNCIONES PUBLICAS ***************************}

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );
begin
     with FunctionLibrary do
     begin
          //FUNCIONES PARA CALCULOS DE IMSS
          RegisterFunction( TZetaIMSSObrero, 'IMSS_OBRER', 'Aportaci�n Obrero Imss' );
          RegisterFunction( TZetaIMSSPatron, 'IMSS_PATRO', 'Aportaci�n Patronal Imss' );
          RegisterFunction( TZetaIMSSRetiro, 'IMSS_RETIR', 'Aportaci�n Retiro Imss' );
          RegisterFunction( TZetaINFOCredito,'INFO_CREDI', 'Monto Amortizaci�n Infonavit' );
          RegisterFunction( TZetaINFOPatron, 'INFO_PATRO', 'Aportaci�n Patr�n Infonavit' );
          RegisterFunction( TZetaIMSSAjust,  'IMSS_AJUST','Dias Ajuste funciones IMSS' );
          RegisterFunction( TZetaIMSS_EXTRAS,'IMSS_EXTRA','Importe Gravado IMSS' );
          RegisterFunction( TZetaSalMin,'SAL_MIN','Salario M�nimo de Zona' );    // no es de uso general
          RegisterFunction( TZetaIMSSVARIABLE, 'IMSS_VARIABLE','Imss Variable' );
          RegisterFunction( TZetaIMSS_EXENTAS, 'IMSS_EXENTAS','Horas Exentas p/ Imss' );
          RegisterFunction( TZetaCALC_EXENTAS, 'CALC_EXENTAS', 'Horas Exentas p/ Imss' );
          RegisterFunction( TZetaSAL_POND, 'SAL_POND', 'Salario Ponderado' );
          RegisterFunction( TZetaAJUSTA_INFO, 'AJUSTA_INFO','Amortizaci�n de Cr�dito Infonavit' );
          RegisterFunction( TZetaEvaluaIMSSObrero, 'IMSS_OBR_X','Aportaci�n Obrero Imss' );
          RegisterFunction( TZetaEvaluaIMSSPatron, 'IMSS_PAT_X','Aportaci�n Patr�n Imss' );
          RegisterFunction( TZetaEvaluaIMSSRetiro, 'IMSS_RET_X','Aportaci�n Obrero-Patronales' );
          RegisterFunction( TZetaEvaluaInfoCredito, 'INFO_CRE_X','Amortizaci�n Cr�dito Infonavit' );
          RegisterFunction( TZetaAjustaInfoCredito, 'AJUS_INF_X','Amortizaci�n de Per�odo de N�mina' );
          RegisterFunction( TZetaEvaluaInfoPatron, 'INFO_PAT_X','Aportaci�n Patronal Infonavit' );
          RegisterFunction( TZetaValorDiaInfonavit, 'V_DIA_INFO','Valor Infonavit por d�a' );
          RegisterFunction( TZetaValorNominaInfonavit, 'V_NOM_INFO','Valor Infonavit por d�a de N�mina' );
          RegisterFunction( TZetaAjustaInfoCreditoEnNomina, 'AJUS_INF_N','Amortizaci�n de Per�odo de N�mina' );
          RegisterFunction( TZetaRIMSSVARIABLE,'IMSS_RVAR', 'Imss variable por raz�n social');
          RegisterFunction( TZetaValUma , 'VAL_UMA', 'Unidad de Medida y Actualizaci�n' );
          {*** US 12365: Timbrado/Declaraciones : Mejorar calculo de exentos en n�mina ***}
          RegisterFunction( TZetaEC, 'EC', ' Exento Calculado' );
          RegisterFunction( TZetaCalExento, 'CAL_EXENTO', 'Evalua el monto excento');
          {*** FIN ***}
     end;
end;

end.
