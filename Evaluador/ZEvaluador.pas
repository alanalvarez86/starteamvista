unit ZEvaluador;

interface

uses Messages, Classes, SysUtils, Dialogs, DB, Controls, Mask,Forms,
     ZetaQRExpr,
     ZetaTipoEntidad,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZCreator,
     DExisteCampo,
     DZetaServerProvider;

type
  TQueryID = Integer;
  TTasa = Double;
  TZetaEvaluador = class(TQREvaluator)
  private
    { Private Declarations }
    FCreator : TZetaCreator;
    FEntidad : TipoEntidad;
    FListaRequeridos: TStringList;
    FListaVariables : TStringList;
    FTransformada : String;
    FCalcNomina: TObject;
    FVarAux : TPesos;
    FListaSubTotal : String;
    procedure AgregaRequerido( const sExpresion: String; const oEntidad : TipoEntidad );
    procedure IncluyeScript( const sScript, sExpresion : String; const oEntidad : TipoEntidad );
    procedure CreaUnCampo( const sCampo: String; const TipoCampo: eTipoGlobal );
    procedure AgregaParam( const sParam : string );
    procedure AgregaVariable( const sScript, sExpresion : String; const oEntidad : TipoEntidad );
    function GetExisteCampo: TdmExisteCampo;
  protected
    { Protected Declarations }
    function  EvalVariable( strVariable: String ): TQREvResult; override;
  public
    { Public Declarations }
    constructor Create( oCreator : TZetaCreator );overload;
    constructor Create( oCreator : TZetaCreator; oCalcNomina : TObject );overload;
    destructor  Destroy; override;
    property oZetaCreator: TZetaCreator read FCreator;
    property dmExisteCampo: TdmExisteCampo read GetExisteCampo;
    procedure AgregaDataset( Dataset: TZetaCursor );
    procedure Prepare(const StrExpr : String);
    function oZetaProvider: TdmZetaServerProvider;

    function Rastreador: TRastreador;
    function  Calculate(const StrExpr : string) : TQREvResult;
    function  GetRequiere( const Expresion: String; var eTipo : eTipoGlobal; var Transformada : String; var lConstante : Boolean ) : Boolean;
    function  ValorPesos: TPesos;
    function  ValorEntero: integer;
    function  ValorTexto: String;
    function  ValorBooleano: Boolean;
    function  GetEmpleado : Integer;
    function  CalculaString( const Expresion : String; var eTipo : eTipoGlobal;
                             var Resultado : String ) : Boolean;
    function CalculandoNomina : Boolean;
    function Rastreando : Boolean;
    function RastreandoNomina: Boolean;
    property  CalcNomina : TObject read FCalcNomina write FCalcNomina;
    property  ListaRequeridos : TStringList read FListaRequeridos;
    property  Entidad : TipoEntidad read FEntidad write FEntidad;
    function  NoHayRequeridos : Boolean;
    property  VarAux : TPesos read FVarAux write FVarAux;
    function  FiltroSQLValido( var sFiltro, sError : String ) : Boolean;
    property  ListaSubTotal : String read FListaSubTotal;
    procedure AgregaSubTotal( const nPos : Integer );
    function  LibreriaFunciones: TQRFunctionLibrary;
    procedure AgregaRequeridoAlias( sExpresion, sAlias : String; const TipoCampo : eTipoGlobal; const oEntidad : TipoEntidad );

  end;

  TZetaFunc = class( TQREvElementFunction )
  private
    { Private Declarations }
    function GetEvaluador: TZetaEvaluador;
    function GetProvider: TdmZetaServerProvider;
  protected
    { Protected declarations }
    function InvestigaTipo(const Entidad: TipoEntidad; var sCampo: String; var lEsFecha: Boolean): TQrEvResultType;
  public
    { Public Declarations }
    property ZetaEvaluador: TZetaEvaluador read GetEvaluador;
    property oZetaProvider: TdmZetaServerProvider read GetProvider;
    function DataSetEvaluador : TZetaCursor;
    function ParamVacio( const iParam: Word ): Boolean;
    // 6 Tipos
    function ParamFloat( const iParam: Word ): TPesos;
    function ParamInteger( const iParam: Word ): Integer;
    function ParamString( const iParam: Word ): String;
    function ParamBoolean( const iParam: Word ): Boolean;
    function ParamDate( const iParam: Word ): TDate;
    function ParamTasa( const iParam: Word ): TTasa;
    // 6 Defaults
    function DefaultFloat( const iParam: Word; rDefault: TPesos ): TPesos;
    function DefaultInteger( const iParam: Word; iDefault: Integer ): Integer;
    function DefaultString( const iParam: Word; sDefault: String ): String;
    function DefaultBoolean( const iParam: Word; lDefault: Boolean ): Boolean;
    function DefaultDate( const iParam: Word; dDefault: TDate ): TDate;
    function DefaultTasa( const iParam: Word; rDefault: TTasa ): TTasa;
    // Manejo de Errores y Validaci�n
    function ErrorFuncion( const Msg : TQRErrorString; const Exp : String ) : TQREvResult;
    function ErrorParametro( const Msg : TQRErrorString; const Exp : String ) : String;
    procedure ErrorParametroTipo( const Numero : Integer; const Msg : TQRErrorString );
    procedure ParametrosFijos( const Numero : Integer );
    // Campos Requeridos
    procedure AgregaRequerido( const sExpresion: String; const oTipoEntidad : TipoEntidad );
    procedure ValidaConfidencial;

    function  SelRes( const sFunction : string ): string;
    function  CampoRes( const sCampo : string ): string;

    // Entidad de la que proviene
    function EsEntidadNomina: Boolean;
  end;

  TZetaConstante = class( TZetaFunc )
  public
    constructor Create(oEvaluador:TQrEvaluator); override;
  end;


  TZetaRegresa = class( TZetaFunc )
  protected
    lQuitarOptimizeCursorB8: Boolean;
  public
    { Public Declarations }
    constructor Create(oEvaluador:TQrEvaluator); override;
    destructor Destroy; override;
    function  GetScript: String; virtual; abstract;
    procedure AgregaScript( sScript: String; const Tipo: TQrEvResultType; const lEsFecha: Boolean );
    procedure EquivaleScript( const sScript : String; const oEntidad : TipoEntidad );
    procedure AgregaParam( const sParam : string );
    // Tipos de Par�metros
    function  ParamCampoDouble( const iParam : Word ) : String;
    function  ParamCampoInteger( const iParam : Word ) : String;
    function  ParamCampoString( const iParam : Word ) : String;
    function  ParamCampoFecha( const iParam : Word ) : String;
    // Defaults de Par�metros
    function  ParamCampoDefInteger( const iParam, iValor : integer ) : string;
    function  ParamCampoDefString( const iParam: integer; const sValor : string ) : string;
    function  ParamCampoDefFecha( const iParam: integer; const sValor : string ) : string;
  end;

  TZetaRegresaFloat = class( TZetaRegresa )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

  TZetaRegresaInt = class( TZetaRegresa )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

  TZetaRegresaQuery = class( TZetaRegresa )
  protected
    FQuery : TZetaCursor;
    procedure  AbreQuery( const sSQL : String );
    function  EvaluaQuery( const sSQL : String ) : TQREvResult;
  public
    constructor Create(oEvaluador:TQrEvaluator); override;
    destructor  Destroy; override;
  end;

{CV: DIC2002:
Se movieron estas funciones a esta unidad, porque tambien se
requieren en el Modulo de Seleccion de Personal.
En ZFuncsTress y ZFuncsSeleccion, se tienen las mismas funciones
que heredan de estas }

{************** TZetaBasePRETTY_NAME ************* }
  TZetaBasePRETTY_NAME = class ( TZetaRegresa )
  protected
    procedure AgregaDatosRequeridos;dynamic;
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

{ ************* TZetaBaseEDAD ************* }

type
  TZetaBaseEDAD = class( TZetaRegresaFloat )
  protected
    function GetFecNacField: String;dynamic;
    procedure AgregaDatosRequeridos;dynamic;
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

{ ************* TZetaBaseEDADLETRA ************* }

type
 TZetaBaseEDADLETRA = class( TZetaRegresa )
  protected
    function GetFecNacField: String;dynamic;
    procedure AgregaDatosRequeridos;dynamic;
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

  // Registro de ZetaFuncs
  {
  procedure RegisterFunction( FunctionClass: TQRLibraryItemClass; const Name : String );
  procedure RegisterFunctionDP( FunctionClass: TQRLibraryItemClass; const Name : String );
  }
  // Utiler�as
  function  ResultAsString( AValue : TQREvResult ) : String;
  function  Iguales( ValorX, ValorY: TQREvResult ): Boolean;
  function  TieneAlias( const sColumna : String ) : Boolean;
  function  EsFormulaSQL( const sColumna : String )  : Boolean;
  function  EsFormulaSQLTexto( const sTexto: string ) : Boolean;
  function  LimpiaFormula( const sColumna : String ) : String;
  function  Tokeniza( const iColumna : Integer ) : String;
  function  EsEquivalente( const sColumna : String ) : Boolean;
  function  StrTransVar( sOrigen, sSearch, sReplace: String ) : String;
  function  ResultadoQuery( Query : TZetaCursor; const nField: Integer): TQREvResult;
  function  ResToString( EvResult : TQREvResult ) : String;

implementation

uses ZetaSQL;

const
  aTipoGlobal : array[ eTipoGlobal ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} =('L�gico','Numero con Decimales','Numero Entero','Fecha','Texto','Memo', 'Autom�tico', 'Blob');
  K_TOKEN = 'TOKEN';
  K_ARROBA_SQL = '@';
  K_ARROBA_SQL_TEXTO = '@TEXTO:';
  K_MARCA_ALIAS = '#';
  K_MARCA_EQUIVALE = '&';

{************** Funciones Privadas ********* }
function TipoGlobalToSQL( Tipo : eTipoGlobal ) : eTipoSQL;
begin
     case Tipo of
          tgBooleano : Result := tsqlTexto;
          tgFloat    : Result := tsqlFloat;
          tgNumero   : Result := tsqlEntero;
          tgFecha    : Result := tsqlFecha;
          else
                       Result := tsqlTexto;
     end;
end;

function TipoGlobalToEvResult( const Tipo : eTipoGlobal; var EsFecha : Boolean ) : TQREvResultType;
begin
     EsFecha := FALSE;
     case Tipo of
          tgBooleano : Result := resBool;
          tgFloat    : Result := resDouble;
          tgNumero   : Result := resInt;
          tgFecha    : begin
                            Result  := resDouble;
                            EsFecha := TRUE;
                       end;
          tgTexto    : Result := resString;
          else
              Result := resError;
     end;
end;


procedure AgregaConjunto( Lista : TStringList; s : String );
begin
     with Lista do
          if IndexOf( s ) < 0 then
             Add( s );
end;

function AgregaConjuntoObjeto( Lista : TStringList; s : String; o : TObject ) : Integer;
begin
     with Lista do
     begin
        Result := IndexOf( s );
        if Result < 0 then
             Result := AddObject( s, o );
     end;
end;

function ResToString( EvResult : TQREvResult ) : String;
begin
   with EvResult do
     case Kind of
       resBool   : Result:= ZetaCommonTools.zBoolToStr( booResult );
       resDouble : if ( resFecha ) then
                      Result := FormatDateTime( 'dd/mm/yyyy', dblResult )
                   else
                       Result := FloatToStr( dblResult );
       resInt    : Result := IntToStr( intResult );
       else        Result := strResult;
     end;
end;

function TipoGlobal( EvResult : TQREvResult ) : eTipoGlobal;
begin
     case EvResult.Kind of
          resBool : Result := tgBooleano;
          resDouble:
          begin
               if EvResult.resFecha then Result := tgFecha
               else Result := tgFloat;
          end;
          resInt: Result := tgNumero;
          else { resString}
               Result := tgTexto;
     end;
end;

{ ************* TZetaEvaluador ************* }
procedure TZetaEvaluador.IncluyeScript( const sScript, sExpresion : String; const oEntidad : TipoEntidad );
var
    iPos : Integer;
begin
     iPos := AgregaConjuntoObjeto( FListaRequeridos, K_MARCA_EQUIVALE + sScript, TObject( oEntidad ));
     FTransformada := StrTransVar( FTransformada, sExpresion, Tokeniza( iPos ));

end;

procedure TZetaEvaluador.AgregaVariable(const sScript, sExpresion: String; const oEntidad: TipoEntidad);
var
    iPos : Integer;
begin
     // EZM: Problema de COD_CLASIFI( CB_PUESTO )
     // Por EvalVariable, La transformada queda como COD_CLASIFI( TOKEN0 )
     // Y el AgregaScript no la sustituye correctamente
     iPos := AgregaConjuntoObjeto( FListaRequeridos, K_MARCA_EQUIVALE + sScript, TObject( oEntidad ));
     AgregaConjuntoObjeto( FListaVariables, sExpresion, TObject( iPos ));
end;

{********************* TZetaEvaluador *************** }

constructor TZetaEvaluador.Create( oCreator : TZetaCreator );
begin
     FCreator := oCreator;
     FunctionLibrary := FCreator.FunctionLibrary;
     FListaRequeridos := TStringList.Create;
     FListaVariables  := TStringList.Create;
     DataSets := TList.Create;
     FCreator.PreparaExisteCampo;
     FVarAux := 0;
end;

constructor TZetaEvaluador.Create(oCreator : TZetaCreator; oCalcNomina: TObject);
begin
     Create(oCreator);
     FCalcNomina := oCalcNomina;
end;


destructor TZetaEvaluador.Destroy;
begin
     //FreeAndNil( FExisteCampo );
     DataSets.Free;
     FListaRequeridos.Free;
     FListaVariables.Free;
     inherited Destroy;
end;

procedure TZetaEvaluador.AgregaDataset( Dataset: TZetaCursor );
begin
     Datasets.Add( Dataset );
end;

function TZetaEvaluador.Calculate(const StrExpr : string) : TQREvResult;
begin
     Result := inherited Calculate( Trim( BorraCReturn( StrExpr ) ) );
end;

procedure TZetaEvaluador.Prepare(const StrExpr : string);
begin
     inherited Prepare( Trim( BorraCReturn( StrExpr ) ) );
end;

function  TZetaEvaluador.GetRequiere( const Expresion: String; var eTipo : eTipoGlobal;
                           var Transformada : String; var lConstante : Boolean ) : Boolean;
var
   Resultado : TQREvResult;
   i, ErrorNumero : integer;
   ErrorDescrip, ErrorMsg, ErrorExp : string;
begin
     FBuscandoRequeridos := TRUE;
     FListaRequeridos.Clear;
     FListaVariables.Clear;
     FTransformada := Expresion;
     FListaSubTotal := '';
     // Evalua la expresi�n para validarla y obtener sus campos requeridos
     Prepare( Expresion );
     Resultado  := Value;
     lConstante := EsConstante;
     UnPrepare;
     // Si hubo error, regresa su descripci�n
     if HuboErrorEvaluador( Resultado, ErrorNumero, ErrorDescrip, ErrorMsg, ErrorExp ) then
     begin
          Result := FALSE;
          eTipo := tgAutomatico;
          FListaRequeridos.Clear;
          FListaVariables.Clear;
          Transformada :=  'Error N�mero: ' + IntToStr( ErrorNumero ) + CR_LF +
                            ErrorDescrip + CR_LF + ErrorMsg + CR_LF + ErrorExp;
     end
     else
     begin
          Result := TRUE;
          eTipo := TipoGlobal( Resultado );
          // Se hace hasta el final para evitar transformar expresiones
          // de funciones que SI se pueden resolver en GetRequiere
          // Ej. COD_CLASIFI( CB_PUESTO )
          with FListaVariables do
            for i := 0 to Count-1 do
                FTransformada := StrTransVar( FTransformada, Strings[ i ], Tokeniza( Integer( Objects[ i ] )));

          Transformada := FTransformada;
     end;
     FBuscandoRequeridos := FALSE;
end;

function  TZetaEvaluador.CalculaString( const Expresion: String; var eTipo : eTipoGlobal;
                           var Resultado : String ) : Boolean;
var
   Res : TQREvResult;
   ErrorNumero : integer;
   ErrorDescrip, ErrorMsg, ErrorExp : string;
begin
     // Evalua la expresi�n para validarla y obtener sus campos requeridos
     Res := Calculate( Expresion );
     // Si hubo error, regresa su descripci�n
     if HuboErrorEvaluador( Res, ErrorNumero, ErrorDescrip, ErrorMsg, ErrorExp ) then
     begin
          Result := FALSE;
          eTipo := tgAutomatico;
          FListaRequeridos.Clear;
          FListaVariables.Clear;
          Resultado  :=  'Error N�mero: ' + IntToStr( ErrorNumero ) + '|' +
                            ErrorDescrip + '|' + ErrorMsg + '|' + ErrorExp;
     end
     else
     begin
          Result := TRUE;
          eTipo := TipoGlobal( Res );
          Resultado := ResToString( Res );
     end;
end;


function TZetaEvaluador.ValorTexto: String;
begin
     Result := ResToString( Value );
end;

function TZetaEvaluador.ValorBooleano: Boolean;
begin
     with Value do
     begin
          case Kind of
               resInt: Result := ( intResult <> 0 );
               resDouble: Result := ( dblResult <> 0 );
               resBool: Result := booResult;
          else
              Result := False;
          end;
     end;
end;

function TZetaEvaluador.ValorEntero: integer;
begin
     with Value do
     begin
          case Kind of
               resInt: Result := intResult;
               resDouble: Result := Round( dblResult );
               resString: Result := StrAsInteger( strResult );
               resBool:
               begin
                    if booResult then
                       Result := 1
                    else
                        Result := 0;
               end
               else Result := 0;
          end;
     end;

end;

function TZetaEvaluador.ValorPesos: TPesos;
begin
     with Value do
     begin
          case Kind of
               resDouble: Result := Redondea( dblResult );
               resInt: Result := intResult;
          else
	      Result := 0;
          end;
     end;
end;


procedure TZetaEvaluador.AgregaRequerido( const sExpresion : string; const oEntidad : TipoEntidad );
begin
     AgregaConjuntoObjeto( FListaRequeridos, sExpresion, TObject( oEntidad ));
end;

procedure TZetaEvaluador.AgregaRequeridoAlias( sExpresion, sAlias : string; const TipoCampo : eTipoGlobal; const oEntidad : TipoEntidad );
begin
     if EsFormulaSQL( sExpresion ) then
        sExpresion := GetCastFormula( LimpiaFormula( sExpresion ), TipoGlobalToSQL( TipoCampo ));
     AgregaRequerido(K_MARCA_ALIAS + sExpresion + ' ' + sAlias, oEntidad );
end;


procedure TZetaEvaluador.CreaUnCampo( const sCampo: String; const TipoCampo: eTipoGlobal );
begin
     case TipoCampo of
          tgBooleano  : CreaCampo( resString, sCampo, False );
          tgFloat     : CreaCampo( resDouble, sCampo, False );
          tgNumero    : CreaCampo( resInt,    sCampo, False );
          tgFecha     : CreaCampo( resDouble, sCampo, TRUE );
          tgTexto     : CreaCampo( resString, sCampo, False );
          tgMemo      : CreaCampo( resString, sCampo, False );
          tgAutomatico: CreaCampo( resError,  sCampo, False );
     end;
end;

function TZetaEvaluador.EvalVariable( strVariable: String ): TQREvResult;
var
   nTipo : Integer;
   TipoCampo: eTipoGlobal;
   sFormula : String;
   lParametro {//CV, lConfidencial} : Boolean;
   oEntidad : TipoEntidad;
begin
     if ( FBuscandoRequeridos ) then
     begin
          // PENDIENTE el valor activo de CONFIDENCIALES
          //CV lConfidencial := TRUE;
          oEntidad := FEntidad;
          //CV
          if dmExisteCampo.ExisteCampo( oEntidad, strVariable, sFormula, nTipo, lParametro ) then
          begin
               TipoCampo := eTipoGlobal( nTipo );
               if ( lParametro ) then
               begin
                    AgregaRequeridoAlias( sFormula, strVariable, TipoCampo, oEntidad );
                    CreaUnCampo( strVariable, TipoCampo );
                    // FTransformada := StrTransform( FTransformada, strVariable, sFormula );
               end
               else
               begin
                    // EZM: Agregar con todo y Punto y sustituye la original
                    // El punto hace expl�cito al SQL la diferencia entre NOMINA.CB_SALARIO y COLABORA.CB_SALARIO
                    // El token permite distinguirlos pues el FieldByName( 'CB_SALARIO' ) regresar�a el primero
                    // IncluyeScript( sFormula, strVariable, oEntidad );
                    AgregaVariable( sFormula, strVariable, oEntidad );
                    CreaUnCampo( sFormula, TipoCampo );
               end;
          end
          else
              Result := inherited EvalVariable( strVariable );
     end
     else
         Result := inherited EvalVariable( strVariable );
end;

procedure TZetaEvaluador.AgregaParam( const sParam : String );
{$ifdef TRESS}
var sFormula : string;
    nTipo : Integer;
{$endif}
begin
     {$ifdef TRESS}
     {CV: no se puede poner un Dummy dmExisteCampo.ExisteParam,
      porque de todas formas marca error de compilacion,
      porque en el AgregaRequeridoAlias, se esta usando
      la entidad enNomina, y esa entidad no existe en otros
      modulos que no sean de Tress}
     if dmExisteCampo.ExisteParam( sParam, sFormula, nTipo ) then
        AgregaRequeridoAlias( sFormula, sParam, eTipoGlobal( nTipo ), enNomina );
     {$endif}
end;
function TZetaEvaluador.GetEmpleado : Integer;
begin
     try
        with TZetaCursor( DataSets[ 0 ] ) do
             Result := FieldByName( 'CB_CODIGO' ).AsInteger
     except
         Result := 0;
     end;
end;

function TZetaEvaluador.oZetaProvider: TdmZetaServerProvider;
begin
     Result := FCreator.oZetaProvider;
end;

function TZetaEvaluador.CalculandoNomina: Boolean;
begin
     Result := FCalcNomina <> NIL
end;

function TZetaEvaluador.Rastreador: TRastreador;
begin
     Result := FCreator.Rastreador;
end;

function TZetaEvaluador.Rastreando: Boolean;
begin
     Result := FCreator.Rastreando;
end;

function TZetaEvaluador.RastreandoNomina: Boolean;
begin
     Result := FCreator.RastreandoNomina;
end;


{******************** TZetaFunc ********************* }
{ Clase Base de la cual heredan todas
  las funciones del evaluador }

function TZetaFunc.ParamVacio( const iParam: Word ) : Boolean;
begin
     Result := ( Arglist.Count <= iParam ) or (( Argument( iParam ).Kind = resError ) and ( Argument( iParam ).strResult = 'NIL' ));
end;

function TZetaFunc.ParamFloat( const iParam: Word ): TPesos;
begin
     Result := 0;
     with Argument( iParam ) do
     begin
          case Kind of
               // Importante redondear a 2 decimales
               resDouble: Result := Redondea( dblResult );
               resInt:    Result := intResult;
          else
              ErrorParametroTipo( iParam, 'No es Num�rico' );
          end;
     end;
end;

function TZetaFunc.ParamTasa( const iParam: Word ): TTasa;
begin
     Result := 0;
     with Argument( iParam ) do
     begin
          case Kind of
               resDouble: Result := dblResult;
               resInt:    Result := intResult;
          else
              ErrorParametroTipo( iParam, 'No es Num�rico' );
          end;
     end;
end;

function TZetaFunc.ParamInteger( const iParam: Word ): Integer;
begin
     Result := 0;
     with Argument( iParam ) do
     begin
          case Kind of
               resDouble: Result := Trunc( dblResult );
               resInt:    Result := intResult;
               resString: Result := StrToIntDef( strResult, 0 );
          else
              ErrorParametroTipo( iParam, 'No es Num�rico' );
          end;
     end;
end;

function TZetaFunc.ParamString( const iParam: Word ): String;
begin
     Result := '';
     with Argument( iParam ) do
     begin
          if ( Kind = resString ) then
             Result := strResult
          else
              ErrorParametroTipo( iParam, 'No es Texto' );
     end;
end;

function TZetaFunc.ParamBoolean( const iParam: Word ): Boolean;
begin
     Result := False;
     with Argument( iParam ) do
     begin
          if ( Kind = resBool ) then
             Result := booResult
          else
              ErrorParametroTipo( iParam, 'No es L�gico' );
     end;
end;

function TZetaFunc.ParamDate( const iParam: Word ): TDate;
begin
     Result := 0;
     with Argument( iParam ) do
     begin
          case Kind of
               resDouble:
               begin
                    if ( resFecha ) then
                        Result := dblResult
                    else
                        ErrorParametroTipo( iParam, 'No es Fecha' );
               end;
               resString:
               begin
                    try
                       Result := StrAsFecha( strResult );
                    except
                          Result := NullDateTime;
                    end;
               end;
          else
              ErrorParametroTipo( iParam, 'No es Fecha' );
       end;
     end;
end;

function TZetaFunc.DefaultFloat( const iParam: Word; rDefault: TPesos ): TPesos;
begin
     if ParamVacio( iParam ) then
        Result := rDefault
     else
         Result := ParamFloat( iParam );
end;

function TZetaFunc.DefaultInteger( const iParam: Word; iDefault: Integer ): Integer;
begin
     if ParamVacio( iParam ) then
        Result := iDefault
     else
         Result := ParamInteger( iParam );
end;

function TZetaFunc.DefaultString( const iParam: Word; sDefault: String ): String;
begin
     if ParamVacio( iParam ) then
        Result := sDefault
     else
         Result := ParamString( iParam );
end;

function TZetaFunc.DefaultBoolean( const iParam: Word; lDefault: Boolean ): Boolean;
begin
     if ParamVacio( iParam ) then
        Result := lDefault
     else
         Result := ParamBoolean( iParam );
end;

function TZetaFunc.DefaultDate( const iParam: Word; dDefault: TDate ): TDate;
begin
     if ParamVacio( iParam ) then
        Result := dDefault
     else
         Result := ParamDate( iParam );
end;

function TZetaFunc.DefaultTasa( const iParam: Word; rDefault: TTasa ): TTasa;
begin
     if ParamVacio( iParam ) then
        Result := rDefault
     else
         Result := ParamTasa( iParam );
end;

procedure TZetaFunc.ParametrosFijos( const Numero : Integer );
begin
     if ( ArgList.Count < Numero ) then
        raise EErrorEvaluador.Create( ErrorParametro(
           'Faltan Par�metros Fijos:' + IntToStr( Numero - ArgList.Count ), '' ));
end;

procedure TZetaFunc.ErrorParametroTipo( const Numero : Integer; const Msg : TQRErrorString );
begin
     raise EErrorEvaluador.Create( ErrorParametro( Msg,
           ', Par�metro[' + IntToStr( Numero+1 ) + ']; ' + ResultAsString( Argument( Numero )) ));
end;

function TZetaFunc.ErrorParametro( const Msg : TQRErrorString; const Exp : String ) : String;
begin
     raise EErrorEvaluador.Create( ErrorEvaluador( eeFuncionParam, Msg,
            'Funci�n: ' + Nombre + '(); ' + Exp));
end;

function TZetaFunc.ErrorFuncion( const Msg : TQRErrorString; const Exp : String ) : TQREvResult;
begin
     raise EErrorEvaluador.Create( ErrorEvaluador( eeFuncion, Msg, 'Funci�n: ' + Nombre + '(); ' + Exp ));
end;

procedure TZetaFunc.AgregaRequerido(const sExpresion: String; const oTipoEntidad : TipoEntidad );
begin
     with ZetaEvaluador do
     begin
          AgregaRequerido( sExpresion, oTipoEntidad );
     end;
end;

procedure TZetaFunc.ValidaConfidencial;
begin
     if NOT oZetaProvider.VerConfidencial then
        ErrorFuncion( 'Funci�n Confidencial.'+CR_LF+'Usuario NO Tiene Derechos para Consultar Datos Confidenciales.','' );
end;

function TZetaFunc.GetEvaluador: TZetaEvaluador;
begin
     Result := TZetaEvaluador( FEvaluador );
end;

function TZetaFunc.GetProvider: TdmZetaServerProvider;
begin
     Result := GetEvaluador.oZetaProvider;
end;

function TZetaFunc.DataSetEvaluador: TZetaCursor;
begin
     Result := ZetaEvaluador.DataSets[ 0 ];
end;

function TZetaFunc.InvestigaTipo( const Entidad: TipoEntidad; var sCampo: String; var lEsFecha : Boolean ) : TQrEvResultType;
var
   nTipo : Integer;
   oEntidad: TipoEntidad;
   lParametro: Boolean;
begin
     oEntidad := Entidad;
     if ZetaEvaluador.dmExisteCampo.ExisteCampo( oEntidad, sCampo, sCampo, nTipo, lParametro ) then
        Result:= TipoGlobalToEvResult( eTipoGlobal( nTipo ), lEsFecha )
     else
         Result:= resError;
end;

function TZetaFunc.SelRes( const sFunction : string ): string;
const
{$IFDEF INTERBASE}
  K_RESULTADO = 'SELECT RESULTADO from ';
{$ENDIF}
{$IFDEF MSSQL}
  K_RESULTADO = 'DBO.';
{$ENDIF}
begin
     Result := Format('%s%s',[ K_RESULTADO, sFunction]);
{$IFDEF MSSQL}
     if Calculando then
        Result := 'SELECT RESULTADO=' + Result;
{$ENDIF}
end;

function TZetaFunc.CampoRes( const sCampo : string ): string;
const
     sAlias = ' as RESULTADO';
begin
     Result := sCampo;
{$ifdef MSSQL}
     Result := Result + sAlias;
{$endif}
end;

function TZetaFunc.EsEntidadNomina: Boolean;
begin
{$IFDEF TRESS}
     Result:= ( Evaluador is TZetaEvaluador ) and  ( TZetaEvaluador( Evaluador ).Entidad in EntidadesNomina );
{$else}
     Result := False;
{$endif}
end;

{****************** TZetaConstante ***************}

constructor TZetaConstante.Create;
begin
     inherited;
     RegresaConstante := TRUE;
end;


{******************** TZetaRegresa  ********************* }

constructor TZetaRegresa.Create;
begin
     lQuitarOptimizeCursorB8 := TRUE;    // Por default todas estas funciones quitan la optimizaci�n de Build #8
     inherited;
end;

destructor TZetaRegresa.Destroy;
begin
     {$ifndef SERVERCAFE}
     {$ifndef SELECCION}
     if oZetaProvider.OptimizeCursorB8 and lQuitarOptimizeCursorB8 then
        oZetaProvider.OptimizeCursorB8 := FALSE;
     {$endif}
     {$endif}
    inherited Destroy;
end;

procedure TZetaRegresa.AgregaScript( sScript : String; const Tipo : TQrEvResultType; const lEsFecha : Boolean );
var
   TipoSQL : eTipoSQL;
begin
     if ( Tipo <> resError ) then
     begin
        case Tipo of
             resString : TipoSQL := tsqlTexto;
             resBool : TipoSQL := tsqlBooleano;
             resDouble :
             begin
                  if lEsFecha then
                     TipoSQL := tsqlFecha
                  else TipoSQL := tsqlFloat;
             end;
             resInt    : TipoSQL := tsqlEntero;
             else
                 TipoSQL := tsqlBlob;
        end;
        sScript := GetCastFormula( sScript, TipoSQL );
     end
     else
         ErrorFuncion( 'No est� permitido pasar "resError"', sScript );

     ZetaEvaluador.IncluyeScript( sScript, Expresion, enNinguno );
     lQuitarOptimizeCursorB8 := FALSE;         // Si se resuelve por script no se remueve la optimizaci�n por build #8
end;

procedure TZetaRegresa.EquivaleScript( const sScript : String; const oEntidad : TipoEntidad );
begin
     ZetaEvaluador.IncluyeScript( sScript, Expresion, oEntidad );
     lQuitarOptimizeCursorB8 := FALSE;         // Si se resuelve por script no se remueve la optimizaci�n por build #8
end;

procedure TZetaRegresa.AgregaParam( const sParam : string );
begin
     ZetaEvaluador.AgregaParam( sParam );
end;

function TZetaRegresa.ParamCampoString( const iParam: Word ): String;
begin
     if ParamEsCampo( iParam ) then
        Result := ParamNombreCampo( iParam )
     else
         Result := EntreComillas( ParamString( iParam ) );
end;

function TZetaRegresa.ParamCampoInteger( const iParam: Word ): String;
begin
     if ParamEsCampo( iParam ) then
        Result := ParamNombreCampo( iParam )
     else
         Result := IntToStr( ParamInteger( iParam ));
end;

function TZetaRegresa.ParamCampoFecha( const iParam : Word ) : String;
begin
     if ParamEsCampo( iParam ) then
        Result := ParamNombreCampo( iParam )
     else
         Result := EntreComillas( DateToStrSQL( ParamDate( iParam ) ) );
end;

function TZetaRegresa.ParamCampoDouble( const iParam : Word ) : String;
begin
     if ParamEsCampo( iParam ) then
        Result := ParamNombreCampo( iParam )
     else
         Result := FloatToStr( ParamFloat( iParam ) );
end;

function TZetaRegresa.ParamCampoDefInteger( const iParam, iValor : integer ) : string;
begin
     if ParamVacio( iParam ) then
        Result := IntToStr( iValor )
     else Result := ParamCampoInteger( iParam )
end;

function TZetaRegresa.ParamCampoDefString( const iParam: integer; const sValor : string ) : string;
begin
     if ParamVacio( iParam ) then
        Result := sValor
     else Result := ParamCampoString( iParam )
end;

function TZetaRegresa.ParamCampoDefFecha( const iParam: integer; const sValor : string ) : string;
begin
     if ParamVacio( iParam ) then
        Result := sValor
     else Result := ParamCampoFecha( iParam )
end;

{********************* TZetaRegresaFloat ******************** }
procedure TZetaRegresaFloat.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
end;

{********************** TZetaRegresaInt ********************** }
procedure TZetaRegresaInt.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resInt;
end;

{******************** TZetaRegresaQuery  ********************* }
{*************** TZetaRegresaSP ****************************** }
{ Ahora tienen que llamar a m�todos del Servidor que le resuelvan }
constructor TZetaRegresaQuery.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create(oEvaluador);
     {$ifdef SERVERCAFE}
     FQuery := oZetaProvider.CreateQuery;       // Solo aplica para Cafeter�a
     {$endif}
end;

destructor TZetaRegresaQuery.Destroy;
begin
     FreeAndNil( FQuery );
     inherited Destroy;
end;

procedure TZetaRegresaQuery.AbreQuery( const sSQL : String );
begin
     if not Assigned( FQuery ) then
        FQuery := oZetaProvider.CreateQuery;

     if not oZetaProvider.AbreQueryScript( FQuery, sSQL ) then
         ErrorFuncion( 'Error en Query', sSQL );
end;

function  TZetaRegresaQuery.EvaluaQuery( const sSQL : String ) : TQREvResult;
begin
     AbreQuery( sSQL );
     Result := ResultadoQuery( FQuery, 0 )
end;


{************************** REGISTRO DE FUNCIONES ********************}
// Tiene que llamarse una vez. Limpia la librer�a de Funciones
{
procedure RegisterFunction( FunctionClass: TQRLibraryItemClass; const Name : String );
begin
     RegisterQRFunction( FunctionClass, Copy( Name, 1, 10 ), '', '', '' );
end;

procedure RegisterFunctionDP( FunctionClass: TQRLibraryItemClass; const Name : String );
 var iPosicion : integer;
begin
     iPosicion := QRFunctionLibrary.EntryList.IndexOf( Name );
     if iPosicion >= 0 then
        QRFunctionLibrary.EntryList.Delete(iPosicion);
     RegisterFunction( FunctionClass, Name );
end;
}

{************************** UTILERIAS ********************************}
function ResultAsString( AValue : TQREvResult ) : String;
begin
  Result := '';
  case AValue.Kind of
    resString : Result := '''' + AValue.StrResult + '''';
    resInt    : Result := IntToStr(AValue.IntResult);
    resDouble : Result := FloatToStr(AValue.DblResult);
    resBool   : if AValue.booResult then
                   Result := 'TRUE'
                else
                   Result := 'FALSE';
  end;
end;

function Iguales( ValorX, ValorY: TQREvResult ): Boolean;
begin
     if ( ValorX.Kind = ValorY.Kind ) then
          case ValorX.Kind of
               resInt    :  Result := ( ValorX.intResult = ValorY.intResult );
               resDouble :  Result := ( ValorX.dblResult = ValorY.dblResult );
               resString :  Result := ( ValorX.strResult = ValorY.strResult );
               resBool   :  Result := ( ValorX.booResult = ValorY.booResult );
          else
              Result := False;
          end
     else
         Result := False;
end;

function  TieneAlias( const sColumna : String ) : Boolean;
begin
     Result := ( Copy( sColumna, 1, 1 ) = K_MARCA_ALIAS );
end;

function EsFormulaSQL( const sColumna : String ): Boolean;
begin
     // Si Expr es F�rmula, empieza con @ //
     Result := Copy( sColumna, 1, 1 ) = K_ARROBA_SQL;
end;

function EsFormulaSQLTexto( const sTexto: string ) : Boolean;
begin
      Result := Copy( sTexto, 1, Length(K_ARROBA_SQL_TEXTO) ) = K_ARROBA_SQL_TEXTO;
end;

function EsEquivalente( const sColumna : String ) : Boolean;
begin
     Result := Copy( sColumna, 1, 1 ) = K_MARCA_EQUIVALE;
end;

function LimpiaFormula( const sColumna : String ) : String;
begin
     if EsFormulaSQLTexto( sColumna ) then
        Result := Copy( sColumna, Length(K_ARROBA_SQL_TEXTO)+1, MAXINT )
     else if TieneAlias( sColumna ) or EsFormulaSQL( sColumna ) or EsEquivalente( sColumna ) then
        Result := Copy( sColumna, 2, MAXINT )
     else
         Result := sColumna;
end;

function Tokeniza( const iColumna : Integer ) : String;
begin
     Result := K_TOKEN + IntToStr( iColumna );
end;

function StrTransVar( sOrigen, sSearch, sReplace: String ) : String;
var
   iPos, iLast, iSearch : Integer;
   sCopia : String;
   lCambia : Boolean;
begin
     Result  := '';
     sSearch := AnsiUpperCase( sSearch );
     iSearch := Length( sSearch );
     sCopia  := AnsiUpperCase( sOrigen );
     iPos    := AnsiPos( sSearch, sCopia );
     while ( iPos > 0 ) do
     begin
          iLast := iPos + iSearch;
          // Revisa que ho haya otras letras anteriores al inicio
          // Ej. TPUESTO() cuando 'sSearch' es 'PUESTO'
          if ( iPos > 1 ) then
             lCambia := not ( sCopia[iPos-1] in ['A'..'Z','0'..'9','_'] )
          else
              lCambia := TRUE;
          // Revisa que no haya letras al final
          if ( lCambia ) then
            if ( iLast <= Length( sCopia )) then
                 lCambia := not ( sCopia[iLast] in ['A'..'Z','0'..'9','_'] )
            else
                 lCambia := TRUE;

          if ( lCambia ) then
               Result := Result + Copy( sOrigen, 1, iPos-1 ) + sReplace
          else
          begin
              Result := Result + Copy( sOrigen, 1, iPos );
              iLast := iPos + 1;
          end;
          sCopia := Copy( sCopia, iLast, MAXINT );
          sOrigen := Copy( sOrigen, iLast, MAXINT );
          iPos := AnsiPos( sSearch, sCopia );
     end;
     Result := Result + sOrigen;
end;


function ResultadoQuery( Query : TZetaCursor; const nField: Integer): TQREvResult;
begin
     try
        with Query do
             Result := AsignaResultado( Fields[ nField ] );
     except
        Result.Kind := resError;
        Result.strResult := 'Error en Query';
     end;
end;


function TZetaEvaluador.GetExisteCampo: TdmExisteCampo;
begin
     Result := FCreator.dmExisteCampo;
end;

function TZetaEvaluador.NoHayRequeridos: Boolean;
begin
    // Sirve para Filtros.
    // Cuando los Filtros son SQL Valido y los unicos requeridos son Variables
    // entonces se puede enviar al servidor
    Result := FListaRequeridos.Count = FListaVariables.Count;
end;

function TZetaEvaluador.FiltroSQLValido(var sFiltro, sError: String): Boolean;
var
    eTipo : eTipoGlobal;
    lConstante : Boolean;
    sTransformada : String;
    i : Integer;

    procedure CambiaNivel( const iNivel : Integer );
    begin
        sFiltro := StrTransVar( sFiltro, Format( 'NIVEL(%d)', [ iNivel ] ),
                                         Format( 'CB_NIVEL%d', [ iNivel ] ));
    end;

begin  // FiltroSQLValido
    sFiltro := BorraCReturn( Trim( sFiltro ) );        // Le quita espacios
    Result := EsFormulaSQL( sFiltro );
    if Result then
        sFiltro := PreparaFormulaSQL( LimpiaFormula( sFiltro ))
    else
    begin
        // Cambia NIVEL(x) por CB_NIVELx para agilizar los filtros
        for i := 1 to {$ifdef ACS}12{$else}9{$endif} do    //MV:(23/Oct/2008) Cambios ACS
            CambiaNivel( i );
        Result := GetRequiere( sFiltro, eTipo, sTransformada, lConstante );
        if Result then
        begin
            Result := ( eTipo = tgBooleano );
            if Result then
            begin
                Result := NoHayRequeridos and SQLValido( sFiltro, sTransformada );
                if Result then
                    sFiltro := sTransformada
                else
                    sError := 'Filtro contiene FUNCIONES'
            end
            else
                sError := 'Filtro debe ser tipo LOGICO';
        end
        else
            sError := sTransformada;
    end;
end;

procedure TZetaEvaluador.AgregaSubTotal(const nPos: Integer);
begin
    if FListaSubTotal = '' then
        FListaSubTotal := IntToStr( nPos )
    else
        FListaSubTotal := FListaSubTotal + ',' + IntToStr( nPos );
end;


{Implementacion de la Funcion TZetaBasePRETTY_NAME}

procedure TZetaBasePRETTY_NAME.AgregaDatosRequeridos;
begin
end;

procedure TZetaBasePRETTY_NAME.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resString;
     // Si lo pongo desde aqu�, me ahorro la b�squeda del campo calculado en el diccionario
     AgregaScript( K_PRETTYNAME, TipoRegresa, False  );
     AgregaDatosRequeridos;
     lQuitarOptimizeCursorB8 := FALSE;         // Si no requiere un query en el calculate, se deja optimizaci�n de Build #8
end;

{Implementacion de la Funcion TZetaBaseEDAD}

procedure TZetaBaseEDAD.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     inherited;
     oZetaProvider.GetDatosActivos;
     AgregaDatosRequeridos;
     lQuitarOptimizeCursorB8 := FALSE;         // Si no requiere un query en el calculate, se deja optimizaci�n de Build #8
end;

function TZetaBaseEDAD.Calculate: TQREvResult;
 var dFecha : TDate;
begin
     with DataSetEvaluador, Result do
     begin
          Kind := resDouble;
          dFecha := FieldByName( GetFecNacField ).AsDateTime;
          if dFecha <= NullDateTime then
             dblResult := NullDateTime
          else dblResult := Years( dFecha,
                                   DefaultDate( 0, oZetaProvider.FechaDefault ) );
     end;
end;

procedure TZetaBaseEDAD.AgregaDatosRequeridos;
begin
end;

function TZetaBaseEDAD.GetFecNacField: String;
begin
     Result := '';
end;

{Implementacion de la Funcion TZetaBaseEDADLETRA}

procedure TZetaBaseEDADLETRA.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resString;
     AgregaDatosRequeridos;
end;

function TZetaBaseEDADLETRA.Calculate: TQREvResult;
 var sDetalle : String;
     eTipo : ETiempoDias;
begin
     sDetalle := DefaultString( 0, 'A' );

     if NOT ( sDetalle[ 1 ] in [ 'A', 'a', 'D', 'd'] ) then sDetalle := 'A';

     with Result do
     begin
          Kind := resString;
          if sDetalle[ 1 ] in [ 'D', 'd' ] then
             eTipo := etDias
          else eTipo := etMeses;
          strResult := Tiempo( DataSetEvaluador.FieldByName( GetFecNacField ).AsDateTime,
                               DefaultDate( 1, Date ),
                               eTipo );
     end;
end;

procedure TZetaBaseEDADLETRA.AgregaDatosRequeridos;
begin
end;

function TZetaBaseEDADLETRA.GetFecNacField: String;
begin
     Result := '';
end;


function TZetaEvaluador.LibreriaFunciones: TQRFunctionLibrary;
begin
     Result := FunctionLibrary;
end;


end.
