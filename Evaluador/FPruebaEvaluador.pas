unit FPruebaEvaluador;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZEvaluador, StdCtrls, ZetaQRExpr, Db, Grids, DBGrids, ZCreator,
  ZetaStateComboBox, ZAgenteSQL, ZSuperSQL, DBClient, DBTables,
  ZetaCommonClasses, ZetaTipoEntidad, DZetaServerProvider;


type
  TFormaEvaluador = class(TForm)
    Expresion: TEdit;
    Evalua: TButton;
    Resultado: TLabel;
    Tipo: TLabel;
    EditTransformada: TEdit;
    Requeridos: TMemo;
    GetRequiere: TButton;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    Database1: TDatabase;
    qryTransformada: TQuery;
    EvalRegistro: TButton;
    Reporte: TButton;
    Globales: TButton;
    FiltroSQL: TButton;
    ComboEntidad: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure EvaluaClick(Sender: TObject);
    procedure GetRequiereClick(Sender: TObject);
    procedure EvalRegistroClick(Sender: TObject);
    procedure ComboEntidadChange(Sender: TObject);
    procedure ReporteClick(Sender: TObject);
    procedure GlobalesClick(Sender: TObject);
    procedure FiltroSQLClick(Sender: TObject);
  private
    { Private declarations }
    oEvaluador : TZetaEvaluador;
    oAgente    : TSQLAgente;
    oSuperSQL  : TSuperSQL;
    FParametros : TZetaParams;
    procedure EvaluaExpresion( const sExpresion : String );
    procedure AsignaParametros;
  public
    { Public declarations }
    procedure LlenaComboEntidad( oLista : TStrings );
  end;

var
  FormaEvaluador: TFormaEvaluador;
  oZetaProvider : TdmZetaServerProvider;
  oZetaCreator  : TZetaCreator;

implementation

uses ZetaCommonLists,
     ZFuncsGenerales,
     ZFuncsTress,
     ZFuncsReporte,
     ZetaCommonTools,
     ZetaEntidad,
     DEntidades, DEntidadesTress, FPruebaReporte;

{$R *.DFM}


procedure TFormaEvaluador.AsignaParametros;
begin
    FParametros := TZetaParams.Create;
    with FParametros do
    begin
        Clear;
        AddInteger( 'Year', 2000 );
        AddInteger( 'Tipo', 1 );
        AddInteger( 'Numero', 5 );
        AddBoolean( 'MuestraTiempos', FALSE );
        AddInteger( 'TipoCalculo', Ord( tcTodos ));
        AddString(  'RangoLista', '' );
        AddString(  'Condicion', '' );
        AddString(  'Filtro', '' );
        AddString(  'RegistroPatronal', '1' );
        AddInteger( 'IMSSYear', 2000 );
        AddInteger( 'IMSSMes', 3 );
        AddInteger( 'IMSSTipo', 0 );
        AddDate(    'FechaAsistencia', Date );
        AddDate(    'FechaDefault', Date );
        AddInteger( 'YearDefault', 2000 );
        AddInteger( 'EmpleadoActivo', 200 );
        AddString(  'NombreUsuario', 'Edgar Mora' );
        AddString(  'CodigoEmpresa', 'MONICA' );
        oZetaProvider.AsignaParamList( VarValues );
    end;
end;


procedure TFormaEvaluador.FormCreate(Sender: TObject);
var
   Periodo : TDatosPeriodo;
begin
     oZetaProvider := TdmZetaServerProvider.Create( self );
{$ifdef INTERBASE}
     // oZetaProvider.EmpresaActiva := VarArrayOf( [ 'c:\Desarrollo\3Windows\Datos\Datos2000.gdx', 'SYSDBA', '40C0', 1 ] );
     oZetaProvider.EmpresaActiva := VarArrayOf( [ 'c:\Desarrollo\3Windows\Datos\Tress Datos.gdx', 'SYSDBA', '40C0', 1 ] );
{$else}
     //oZetaProvider.EmpresaActiva := VarArrayOf( [ 'TressDatosVacia', 'SYSDBA', '40C0', 1 ] );
     oZetaProvider.EmpresaActiva := VarArrayOf( [ 'QA_Rec_Hum_II', 'SYSDBA', '40C0', 1 ] );
{$endif}
     AsignaParametros;

     oZetaCreator := TZetaCreator.Create( oZetaProvider );
     oZetaCreator.RegistraFunciones(  [efGenerales,
                 efTress,
                 efNomina,
                 efReporte,
                 efPoliza,
                 efAnuales,
                 efImss,
                 efISPT ] );

     oSuperSQL := TSuperSQL.Create( oZetaCreator );

     ShortDateFormat := 'dd/mm/yy';
     oEvaluador := TZetaEvaluador.Create( oZetaCreator ) ;
     oEvaluador.Entidad := enNomina;
     // No funciona con IBObjects oEvaluador.AgregaDataSet( qryTransformada );

     ComboEntidad.ItemIndex := Ord( oEvaluador.Entidad );
     // PENDIENTE
     { Qui�n va a llenar este Record? El Cliente? El Servidor? }
     with Periodo do
     begin
       Year := 1999;
       Tipo := tpSemanal;
       Numero:= 30;
       Inicio:= StrToDate( '25/7/99' );
       Fin   := StrToDate( '31/7/99' );
       Pago  := StrToDate( '7/8/99' );
       Dias  := 7;
       Status:= spCalculadaTotal;
       Uso   := upOrdinaria;
       Mes   := 7;
       MesActivo:= 7;
       SoloExcepciones := FALSE;
       IncluyeBajas := FALSE;
     end;
     // ZFuncsTress.SetPeriodoActivo( Periodo );
//     ZFuncsReporte.RegistraFunciones;
//     ZFuncsReporte.SetValoresParams( VarArrayOf( [ 1, 1.1, 'Uno', TRUE, Now, NULL, NULL, NULL, NULL ] ));
     oAgente := TSQLAgente.Create;
end;

procedure TFormaEvaluador.FormDestroy(Sender: TObject);
begin
     oEvaluador.Free;
     oAgente.Free;
     oSuperSQL.Free;
     FParametros.Free;
end;

procedure TFormaEvaluador.EvaluaClick(Sender: TObject);
begin
     // EvaluaExpresion( Expresion.Text );
     with oAgente do
     begin
          Clear;
          Entidad := enNinguno;
          AgregaColumna( Expresion.Text, FALSE,  Entidad, tgAutomatico, 30, '' );
          if oSuperSQL.EvaluaParams( oAgente ) then
          begin
             with GetColumna( 0 ) do
             begin
                  EditTransformada.Text := '*** EVALUACION ***';
                  Resultado.Caption := Formula;
                  Requeridos.Lines.Clear;
                  Tipo.Caption := ObtieneElemento( lfTipoGlobal, Ord( TipoFormula ));
             end;
          end
          else
          begin
             with GetColumna( 0 ) do
             begin
               Resultado.Caption := '';
               Tipo.Caption := 'Error';
               EditTransformada.Text := '*** Errores = ' + IntToStr( NumErrores );
               Requeridos.Lines.Text := StrTransAll( GetError( Status ), '|', CR_LF );
             end;
          end;
     end;
end;

procedure TFormaEvaluador.EvaluaExpresion( const sExpresion : String );
var
   Res : TQREvResult;
begin
     Res := oEvaluador.Calculate( sExpresion );
     case Res.Kind of
       resError  : begin
                        Tipo.Caption := 'ERROR';
                        Resultado.Caption := Res.strResult;
                   end;
       resString : begin
                        Tipo.Caption := 'String';
                        Resultado.Caption := Res.strResult;
                   end;
       resDouble : if ( Res.resFecha ) then
                   begin
                        Tipo.Caption := 'Fecha';
                        Resultado.Caption := DateToStr( Res.dblResult );
                   end
                   else
                   begin
                        Tipo.Caption := 'Float';
                        Resultado.Caption := FloatToStr( Res.dblResult );
                   end;
       resInt    : begin
                        Tipo.Caption := 'Entero';
                        Resultado.Caption := IntToStr( Res.intResult );
                   end;
       resBool   : begin
                        Tipo.Caption := 'Booleano';
                        if ( Res.booResult ) then
                           Resultado.Caption := 'SI'
                        else
                            Resultado.Caption := 'NO';
                   end;
     end;
end;

procedure TFormaEvaluador.GetRequiereClick(Sender: TObject);
{
var
   eTipo : eTipoGlobal;
   Transformada : String;
   i : Integer;
   sRequerido : String;
}
begin
     with oAgente do
     begin
          Clear;
          Entidad := TipoEntidad( ComboEntidad.ItemIndex );
          AgregaColumna( Expresion.Text, FALSE,  Entidad, tgAutomatico, 30, '' );
          if oSuperSQL.Construye( oAgente ) then
          begin
             EditTransformada.Text := GetColumna( 0 ).Formula;
             Requeridos.Lines.Assign( SQL );
             if ( not DataBase1.Connected ) then
                DataBase1.Connected := TRUE;
             qryTransformada.SQL.Assign( Requeridos.Lines );
             qryTransformada.Open;
          end
          else
          begin
             EditTransformada.Text := '*** Error ***';
             Requeridos.Lines.Assign( ListaErrores );
             qryTransformada.Close;
          end;
     end;
{
    if oEvaluador.GetRequiere( Expresion.Text, eTipo, Transformada ) then
    begin
         EditTransformada.Text := Transformada;
         with Requeridos.Lines do
         begin
              Clear;
              // En lugar de hacer esto, usar el SuperSQL
              Add( 'SELECT' );
              if ( oEvaluador.ListaRequeridos.Count = 0 ) then
                 Add( '*' )
              else
                for i := 0 to oEvaluador.ListaRequeridos.Count-1 do
                begin
                    sRequerido := oEvaluador.ListaRequeridos[ i ];
                    if sRequerido[ 1 ] = '@' then
                       sRequerido := Copy( sRequerido, 2, MAXINT ) + ' TOKEN' + IntToStr( i );
                    if ( i > 0 ) then
                       sRequerido := ', ' + sRequerido;
                    Add( sRequerido );
                end;
              Add( 'FROM ' + dmEntidadesTress.NombreEntidad( oEvaluador.Entidad ));
         end;
         if ( not DataBase1.Connected ) then
            DataBase1.Connected := TRUE;
         qryTransformada.SQL.Assign( Requeridos.Lines );
         qryTransformada.Open;
    end
    else
    begin
         EditTransformada.Text := '*** Error ***';
         Requeridos.Lines.Text := Transformada;
         qryTransformada.Close;
    end;
}
end;

procedure TFormaEvaluador.EvalRegistroClick(Sender: TObject);
begin
     EvaluaExpresion( EditTransformada.Text );
     qryTransformada.Next;
end;

procedure TFormaEvaluador.ComboEntidadChange(Sender: TObject);
begin
     oEvaluador.Entidad := TipoEntidad( ComboEntidad.ItemIndex );
end;


procedure TFormaEvaluador.ReporteClick(Sender: TObject);
begin
     FormReporte.ShowModal;
end;

procedure TFormaEvaluador.GlobalesClick(Sender: TObject);
var
   iCodigo : Integer;
begin
     with oZetaProvider do
     begin
          InitGlobales;
          iCodigo := StrToIntDef( Expresion.Text, 1 );
          Resultado.Caption := GetGlobalString( iCodigo );
          Expresion.Text := IntToStr( iCodigo + 1 );
     end;
end;

procedure TFormaEvaluador.FiltroSQLClick(Sender: TObject);
var
    sFiltro, sError : String;
begin
    sFiltro := Expresion.Text;
    if oEvaluador.FiltroSQLValido( sFiltro, sError ) then
        ShowMessage( 'OK = ' + sFiltro )
    else
        ShowMessage( 'ERROR = ' + sError );
end;

procedure TFormaEvaluador.LlenaComboEntidad(oLista: TStrings);
var
   i : TipoEntidad;
begin
     with oLista do
     begin
          try
             BeginUpdate;
             Clear;
             for i := Low( TipoEntidad ) to High( TipoEntidad ) do
                 Add( aTipoEntidad[ i ] );
          finally
                 EndUpdate;
          end;
     end;

end;

end.
