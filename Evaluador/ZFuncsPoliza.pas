unit ZFuncsPoliza;

interface

uses Classes, Sysutils, DB,
     ZetaQRExpr,
     ZEvaluador,
     ZetaCommonClasses;

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );
procedure RegistraFuncionesGeneral( FunctionLibrary: TQRFunctionLibrary );

implementation

uses ZetaServerTools,
     ZetaCommonTools,
     ZFuncsGlobal,
     DZetaServerProvider,
     DSuperReporte;

function CampoInListaTipo( const sCampo, sLista : String; const lEntero : Boolean ) : String;
var
   oLista : TStringList;
begin
     oLista := TStringList.Create;
     Result := '(' +sCampo + ' IN ' + '('+ ConvierteALista( sLista, lEntero, oLista ) +') )';
     oLista.Free;
end;

{**************** TZetaSuma **********************}

type
  TZetaSuma = class( TZetaRegresaFloat )
  protected
    { Protected Declarations }
    function Suma( const lMontos: Boolean ): TQREvResult;
  public
    { Public Declarations }
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaSuma.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     {Agregar validaciones }
     TipoRegresa := resDouble;
     with ZetaEvaluador.oZetaCreator do
     begin
          PolizaPrimero := False;
     end;
end;

function TZetaSuma.Suma( const lMontos: Boolean ): TQREvResult;
var
   sExpresion, sConceptos: String;
   rNumero, rMonto : Double;

   function AgregaLista( const sCampo, sLista: String; lCaracter: Boolean ): String;
   begin
        if (sLista > '') then
           Result := CampoInListaTipo( sCampo, sLista, NOT lCaracter )
        else
            Result := '';
   end;

   function AgregaGrupoLista( const iGrupo : integer ): string;
    var sParam : string;
   begin

        sParam := DefaultString( iGrupo, '' );
        if sParam > '' then
        begin
             with ZetaEvaluador.oZetaProvider.ParamList.ParamByName('MaxGrupos') do
                  if iGrupo > AsInteger then
                     raise EErrorEvaluador.Create( ErrorEvaluador( eeFuncionParam,
                                                   'FUNCION :' +CR_LF+ Expresion,
                                                   'Error en el Par�metro #' + IntToStr(iGrupo) +CR_LF+
                                                   'Se Est� Trantando de Usar un Grupo que no Est� Definido en la P�liza Contable.'+CR_LF+
                                                   'Revisar Definici�n de la P�liza') ) ;

             Result := AgregaLista( 'Grupo'+IntToStr(iGrupo), sParam, True ) ;
        end;
   end;


 var oSuperReporte:TdmSuperReporte;
begin
     oSuperReporte := TdmSuperReporte(ZetaEvaluador.oZetaCreator.SuperReporte);
     if ParamVacio( 0 ) then
        sConceptos := ''
     else
         with Argument( 0 ) do
         begin
              if ( Kind = resInt ) then
                 sConceptos := IntToStr( intResult )
              else
                  sConceptos := ParamString( 0 );
         end;
     sExpresion := '';
     sExpresion := ZetaCommonTools.ConcatFiltros( sExpresion, AgregaLista( 'CO_NUMERO', sConceptos, False ) );
     sExpresion := ZetaCommonTools.ConcatFiltros( sExpresion, AgregaGrupoLista( 1 ) );
     sExpresion := ZetaCommonTools.ConcatFiltros( sExpresion, AgregaGrupoLista( 2 ) );
     sExpresion := ZetaCommonTools.ConcatFiltros( sExpresion, AgregaGrupoLista( 3 ) );
     sExpresion := ZetaCommonTools.ConcatFiltros( sExpresion, AgregaGrupoLista( 4 ) );
     sExpresion := ZetaCommonTools.ConcatFiltros( sExpresion, AgregaGrupoLista( 5 ) );

     with oSuperReporte.DataSetReporte do
     begin
       Filter := sExpresion;
       if sExpresion = '' then
          Filtered := FALSE
       else
           try
              Filtered := TRUE;
           except
                 raise
           end;
     end;

     rMonto := 0;
     rNumero := 0;

     oSuperReporte.DataSetReporte.First;
     while NOT oSuperReporte.DataSetReporte.EOF do
     begin
          rMonto := rMonto + oSuperReporte.DataSetReporte.FieldByName('TOT_MONTO').AsFloat;
          rNumero := rNumero+1;
          oSuperReporte.DataSetReporte.Next;
     end;

     Result.Kind := resDouble;
     if lMontos then
        Result.dblResult := rMonto
     else
         Result.dblResult := rNumero;
end;

function TZetaSuma.Calculate : TQREvResult;
begin
     Result := Suma( True );
end;

{**************** TZetaCuenta **********************}
type TZetaCuenta = class( TZetaSuma )
public
  function Calculate: TQREvResult; override;
end;

function TZetaCuenta.Calculate: TQREvResult;
begin
     Result := Suma( False );
end;

{*************** TZetaCuadre **************************}

type TZetaCuadre = class( TZetaRegresaFloat )
public
  function Calculate: TQREvResult; override;
end;

function TZetaCuadre.Calculate: TQrEvResult;
begin
     with ZetaEvaluador.oZetaCreator do
     begin
          Result.Kind      := resDouble;
          Result.dblResult := PolizaCuadre;
     end;
end;

{*************** TZetaCPoliza **************************}
type TZetaCPoliza = class( TZetaRegresaQuery )
private
  bLista : Boolean;
  function GetScript: String;override;
protected
  function GetResultado( const nEmpParam : integer ): TQrEvResult;
public
  function Calculate: TQREvResult; override;
end;

function TZetaCPoliza.GetScript: String;
begin
     {$ifdef INTERBASE}
     if  bLista then
        Result := 'select sum((select RESULTADO from SP_C( CONCEPTO.CO_NUMERO, %1:d, %2:d, %3:d, %4:d ))) from CONCEPTO where CO_NUMERO in (%0:s)'
     else
        Result := SelRes('SP_C( %0:s, %1:d, %2:d, %3:d, %4:d )');
     {$ENDIF}
     {$ifdef MSSQL}
     if bLista then
        Result := 'select COALESCE( sum( MOVIMIEN.MO_PERCEPC + MOVIMIEN.MO_DEDUCCI ), 0.0 ) as RESULTADO ' +
                  'from MOVIMIEN where ( MOVIMIEN.CB_CODIGO = %4:d ) and ( MOVIMIEN.PE_YEAR = %3:d ) ' +
                  'and ( MOVIMIEN.PE_TIPO = %2:d ) and ( MOVIMIEN.PE_NUMERO = %1:d ) and ( MOVIMIEN.CO_NUMERO IN (%0:s) ) and ' +
                  '( MOVIMIEN.MO_ACTIVO = ''S'' )'
     else
        Result := 'select COALESCE( sum( MOVIMIEN.MO_PERCEPC + MOVIMIEN.MO_DEDUCCI ), 0.0 ) as RESULTADO ' +
                  'from MOVIMIEN where ( MOVIMIEN.CB_CODIGO = %4:d ) and ( MOVIMIEN.PE_YEAR = %3:d ) ' +
                  'and ( MOVIMIEN.PE_TIPO = %2:d ) and ( MOVIMIEN.PE_NUMERO = %1:d ) and ( MOVIMIEN.CO_NUMERO = %0:s ) and ' +
                  '( MOVIMIEN.MO_ACTIVO = ''S'' )';
     {$endif}

end;

function TZetaCPoliza.Calculate: TQrEvResult;
begin
     ParametrosFijos(2);
     Result := GetResultado(1);
end;

function TZetaCPoliza.GetResultado( const nEmpParam : integer ): TQrEvResult;
 var sConcepto : string;
begin
     bLista := ( Argument( 0 ).Kind <> resInt ) ;

     if bLista then sConcepto := ParamString(0)
     else sConcepto := IntToStr(ParamInteger(0));

     with oZetaProvider.DatosPeriodo do
          Result := EvaluaQuery(Format(GetScript,[sConcepto,Numero,Ord(Tipo),Year,ParamInteger(nEmpParam)]));
end;

{*************** TZetaCPoliza **************************}
type TZetaCCPoliza = class( TZetaCPoliza )
public
  function Calculate: TQREvResult; override;
end;

function TZetaCCPoliza.Calculate: TQrEvResult;
begin
     ParametrosFijos(1);
     Result := GetResultado(4);
end;


procedure RegistraFuncionesGeneral( FunctionLibrary: TQRFunctionLibrary );
begin
     with FunctionLibrary do
     begin
          RegisterFunction( TZetaCuadre, 'CUADRE' );
     end;
end;

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );
begin
     RegistraFuncionesGeneral(FunctionLibrary);
     with FunctionLibrary do
     begin
          RegisterFunction( TZetaSuma, 'SUMA', 'Suma Montos por Nivel Poliza' );
          RegisterFunction( TZetaCuenta, 'NSUMA', 'N�mero de empleados por Nivel' );
          RegisterFunction( TZetaCuadre, 'CUADRE' );
          RegisterFunction( TZetaCPoliza, 'CPOLIZA', 'Monto calculado de conceptos' );
          RegisterFunction( TZetaCCPoliza, 'C','Monto de un concepto' );
          RegisterFunction( ZFuncsGlobal.TGetGlobal, 'GLOBAL', 'Info de Globales de Empresa' );

          {$ifdef PENDIENTE}
          RegisterFunction( TZetaCONCEPTO, 'CC', 'CC(#)' );
          {$endif}
     end;
end;


end.
