unit ZSuperEvaluador;

interface
uses Classes, ZetaQRExpr, ZetaCommonClasses, ZetaCommonLists,
     DB,
     DSuperReporte,
     ZAgenteSQL,
     ZEvaluador,
     ZCreator,
     ZetaTipoEntidad,
     DZetaServerProvider;

type
  TSuperEv = class( TObject )
    private
       FColExpresion : Integer;
       FSuperReporte : TdmSuperReporte;
       FAgente       : TSQLAgente;
       FCreator      : TZetaCreator;
       procedure CierraDataSet;
       function  AbreDataSet : Boolean;
       procedure SetParamValue( const sNombre : String; const Tipo : eTipoGlobal; const Valor : Variant );
    public
      constructor Create( oCreator : TZetaCreator );
      destructor Destroy; override;
      function Prepara( oAgente : TSQLAgente;
                        const sFiltro, sExpresion : String;
                        const oTipos : TTiposValidos ) : Boolean;
      function Value : TQREvResult;
      function DataSet : TDataSet;
      procedure Inicializa;
    end;

  TZetaSFuncion = class( TZetaRegresa )
  protected
    Preparado : Boolean;
    SuperEv   : TSuperEv;
    FAgente   : TSQLAgente;
    procedure CierraDataSet;
    procedure AbreDataSet;
    procedure SetParamValue( const sNombre : String; const Tipo : eTipoGlobal; const Valor : Variant );
    procedure PreparaAgente; virtual;
    function  SumaRegistros : TPesos;
    procedure PreparaSuperEv( const sExpresion, sFiltro : string;
                              const oTipoEntidad : TipoEntidad;
                              const oTipos : TTiposValidos );
  public
    destructor Destroy;override;
  end;


{*******************************************************}

implementation

uses ZetaCommonTools, ZSuperSQL;

{ ******************** TSuperEv ***************** }
constructor TSuperEv.Create(oCreator: TZetaCreator);
begin
     FCreator := oCreator;
end;

destructor TSuperEv.Destroy;
begin
     FSuperReporte.Free;
     inherited Destroy;
end;

function TSuperEv.Value : TQREvResult;
begin
     Result := AsignaResultado( DataSet.Fields[ FColExpresion ] );
end;

function TSuperEv.Prepara( oAgente : TSQLAgente; const sFiltro, sExpresion : String;
                           const oTipos : TTiposValidos ) : Boolean;
var
   oSuperSQL : TSuperSQL;
begin
     FSuperReporte := TdmSuperReporte.Create( FCreator );
     FAgente := oAgente;

     oSuperSQL := NIL;   // Evita el Warning
     try
        oSuperSQL := TSuperSQL.Create(FCreator);
        // Agrega al Agente el Filtro y la Expresi�n del Usuario
        with oAgente do
        begin
             if ( sExpresion = '1' ) then
               {$ifndef ANTES} //Se obtenia error al evaluar reporte sin 'Ver derechos de confidenciales'
               FColExpresion := AgregaColumna( sExpresion, TRUE, Entidad, tgNumero, 0, '' )
               {$else}
               FColExpresion := AgregaColumna( sExpresion, FALSE, Entidad, tgNumero, 0, '' )
               {$endif}
             else
               FColExpresion := AgregaColumna( sExpresion, FALSE, Entidad, tgAutomatico, 30, '' );
             if StrLleno( sFiltro ) then
                AgregaFiltro( sFiltro, FALSE, Entidad );
        end;
        Result := oSuperSQL.Construye( oAgente );
        if ( Result ) then
        begin
           with oAgente.GetColumna( FColExpresion ) do
                Result := (TipoFormula in oTipos );
        end;
        if ( Result ) then
           Result := FSuperReporte.Prepara( oAgente );
     finally
        oSuperSQL.Free;
     end;
end;

function TSuperEv.DataSet: TDataSet;
begin
     Result := FSuperReporte.DataSetReporte;
end;

function TSuperEv.AbreDataSet : Boolean;
begin
     Result := FSuperReporte.AbreDataSet;
     if NOT Result then
        Result := FSuperReporte.QueryReporte.Eof;
end;

procedure TSuperEv.Inicializa;
begin
     FSuperReporte.DataSetReporte.EmptyDataSet;
end;

procedure TSuperEv.CierraDataSet;
begin
     FSuperReporte.CierraDataSet;
end;

procedure TSuperEv.SetParamValue(const sNombre: String;
  const Tipo : eTipoGlobal; const Valor: Variant);
begin
     FSuperReporte.SetParamValue( sNombre, Tipo, Valor );
end;




{***************** TZetaSFuncion *************************}
destructor TZetaSFuncion.Destroy;
begin
     SuperEv.Free;
     FAgente.Free;
     Inherited Destroy;
end;

procedure TZetaSFuncion.PreparaAgente;
begin
end;

function TZetaSFuncion.SumaRegistros : TPesos;
begin
     Result := 0;
     with SuperEv.DataSet do
     begin
          while NOT EOF do
          begin
              with SuperEv.Value do
              case Kind of
                   resDouble : Result := Result + dblResult;
                   resInt : Result := Result +intResult
                   else ErrorFuncion( 'Error en la Evaluaci�n', 'Funci�n: ' + Expresion );
              end;
              Next;
          end;
     end;
end;

procedure TZetaSFuncion.PreparaSuperEv( const sExpresion, sFiltro : string;
                                        const oTipoEntidad : TipoEntidad;
                                        const oTipos : TTiposValidos );
begin
     if SuperEv = NIL then
     begin
          FAgente := TSQLAgente.Create;
          FAgente.Entidad := oTipoEntidad;
          {$ifdef FALSE}
          //Este codigo se requiere porque el agente se queda con su FEntidades sin asignar y causa problemas
          //cuando el Agente del SuperEvaluador agrega columnas sin el nombre de la tabla, ejemplo:
          //AgregaColumna( 'CB_SALARIO', True,..... )
          //Este es codigo riesgoso y faltaria saber si nos pega en otro lado
          FAgente.Entidades := ZetaEvaluador.oZetaCreator.dmEntidadesTress;
          {$endif}

          PreparaAgente; // Hook para que las funciones agreguen filtros y columnas propias

          SuperEv := TSuperEv.Create(ZetaEvaluador.oZetaCreator);
          if not SuperEV.Prepara( FAgente, sFiltro, sExpresion, oTipos ) then
             // Temporal
             //ErrorFuncion( 'Error en Expresiones', sFiltro + ' / ' + sExpresion );
             ErrorFuncion( 'Error en Expresiones', 'Expresion: ' + sExpresion +CR_LF +
                                                   'Filtro:' + sFiltro + CR_LF  +
                                                   FAgente.listaerrores.text  );
          Preparado := TRUE;
     end
     else
         SuperEv.Inicializa;      // Hace un EmptyDataSet para llamadas consecutivas;
end;

procedure TZetaSFuncion.AbreDataSet;
begin
     if not SuperEv.AbreDataSet then
        ErrorFuncion( 'Error en Evaluadores', '' );
end;

procedure TZetaSFuncion.CierraDataSet;
begin
     SuperEv.CierraDataSet;
end;

procedure TZetaSFuncion.SetParamValue(const sNombre: String; const Tipo : eTipoGlobal;
  const Valor: Variant);
begin
     SuperEv.SetParamValue( sNombre, Tipo, Valor );
end;



end.
