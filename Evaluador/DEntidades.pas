unit DEntidades;

interface
{$INCLUDE DEFINES.INC}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Variants,
  {$ifdef RDD}
  DZetaServerProvider,
  ZGlobalTress,
  {$else}
  {$endif}

  ZetaCommonLists,
  ZetaTipoEntidad,
  ZetaEntidad;

type
  TdmEntidades = class(TDataModule)
    procedure dmEntidadesDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FListaEntidades : TStringList;
    FReferenciaCircular : TStringList;
    {$ifdef RDD}
    FArregloEntidades : array [SmallInt] of TEntidad;
    {$else}
    FArregloEntidades : array[ Low(TipoEntidad)..High(TipoEntidad) ] of TEntidad;
    {$endif}
    //FEntidad: integer;
    {$ifdef RDD}
    oZetaProvider: TdmZetaServerProvider;
    FQEntidades: TZetaCursor;
    FQRelaciones: TZetaCursor;
    {$else}
    {$endif}
    procedure ResetRelaciones;
    procedure DestruyeEntidades;
    procedure ArregloEntidades;
    procedure ConectaEntidades;
    procedure EntidadesEspeciales;
    procedure PreparaEntidades;
    function EntidadesPreparadas: Boolean;

{$ifdef RDD}
    procedure PreparaQuerys;
    procedure DesPreparaQuerys;
    function GetSQLScript(const iScript: Integer): String;
{$else}
{$endif}

  protected
{$ifdef RDD}
    procedure CreaEntidades; virtual;
{$else}
    procedure CreaEntidades; virtual; abstract;
{$endif}

    function AddEntidad(const TipoEnt: TipoEntidad; const Nombre,
                              Llave: string; Desc : String = '';  Aliass : String  = '' ): TEntidad;
  public
    { Public declarations }
    property ReferenciaCircular : TStringList read FReferenciaCircular  write FReferenciaCircular ;
    function  NombreEntidad( const TipoEnt : TipoEntidad ) : String;
    function  DescripcionEntidad( const TipoEnt : TipoEntidad ) : String;
    function  GetTipoEntidad( const sTabla : String ) : TipoEntidad;
    function GetEntidad(const oTipo: TipoEntidad): TEntidad;
    function  BuscaJoins( const TipoHijo, TipoPadre : TipoEntidad; oJoins : TStringList ) : Boolean;
    // Se necesita en la Interfase???
    procedure RelacionesEntidad( const TipoEnt : TipoEntidad; oRelaciones : TStringList; const lRecursiva : Boolean );
    constructor Create( oProvider: TComponent );override;
  end;

implementation

uses
    ZetaCommonClasses,
    ZetaCommonTools;




{$R *.DFM}

{ TdmEntidades }

{$ifdef RDD}
const
     Q_ENTIDADES = 0;
     Q_RELACIONES = 1;
     Q_ENTIDADES_DERECHOS = 2;

function TdmEntidades.GetSQLScript( const iScript: Integer ): String;
begin
     case iScript of
          Q_ENTIDADES: Result := 'select * from R_ENTIDAD ORDER BY EN_CODIGO';
          Q_ENTIDADES_DERECHOS:
           with oZetaProvider do
                Result := 'select * from R_ENTIDAD '+
                          'where EN_CODIGO IN '+
                          '( SELECT R_ENT_ACC.EN_CODIGO FROM R_ENT_ACC WHERE  '+
                          Format( 'CM_CODIGO = %s and GR_CODIGO = %d AND RE_DERECHO>0 )  ',
                                  [EntreComillas( CodigoEmpresaActiva ), CodigoGrupo] );
          Q_RELACIONES: Result := 'select * from R_RELACION where EN_CODIGO = :Entidad order by RL_ORDEN'
     end;
end;

procedure TdmEntidades.DesPreparaQuerys;
begin
     FreeAndNil( FQEntidades );
     FreeAndNil( FQRelaciones );

end;

procedure TdmEntidades.PreparaQuerys;
begin
     with oZetaProvider do
     begin
          {$ifdef RDDAPP}
          if ( CodigoGrupo = D_GRUPO_SIN_RESTRICCION ) then
             FQEntidades := CreateQuery( GetSQLScript( Q_ENTIDADES ) )
          else
              FQEntidades := CreateQuery( GetSQLScript( Q_ENTIDADES_DERECHOS ) );
          {$else}
          FQEntidades := CreateQuery( GetSQLScript( Q_ENTIDADES ) );
          {$endif}

          FQEntidades.Active := TRUE;
          FQRelaciones := CreateQuery( GetSQLScript( Q_RELACIONES ) );
     end;
end;
{$else}
{$endif}


function TdmEntidades.NombreEntidad(const TipoEnt: TipoEntidad): String;
var
   oEntidad : TEntidad;
begin
     PreparaEntidades;
     oEntidad := FArregloEntidades[ TipoEnt ];
     if ( oEntidad <> NIL ) then
        Result := oEntidad.NombreTabla;
end;

procedure TdmEntidades.RelacionesEntidad( const TipoEnt: TipoEntidad;
                                          oRelaciones: TStringList; const lRecursiva : Boolean );
var
   oEntidad, oPadre : TEntidad;
   i : Integer;
begin
     PreparaEntidades;
     oEntidad := FArregloEntidades[ TipoEnt ];
     if ( oEntidad <> NIL ) then
     begin
          if Assigned(oEntidad.AlAgregarRelacion) then
          begin
               oEntidad.Relaciones.Clear;
               oEntidad.AlAgregarRelacion(oEntidad);
          end;
          with oEntidad.Relaciones do
               for i := 0 to Count-1 do
               begin                                 
                    oPadre := Items[i].Padre;
                    if ( oPadre <> NIL ) then
                    begin
                         if oRelaciones.IndexOf( IntToStr(Ord(oPadre.Tipo)) ) = -1 then
                         begin
                              oRelaciones.Add( IntToStr(Ord(oPadre.Tipo)) );
                              // Llamada recursiva
                              if ( lRecursiva ) then
                                 RelacionesEntidad( oPadre.Tipo, oRelaciones, lRecursiva );
                         end;
                    end
                    else
                        raise Exception.Create(Format( 'Se encontr� una relaci�n no v�lida entre : %s(Tabla #%d) y su relaci�n #%d',
                                                       [oEntidad.NombreTabla,Ord(TipoEnt),i]) );
               end;
     end;

end;


function TdmEntidades.AddEntidad( const TipoEnt : TipoEntidad; const Nombre, Llave  : String; Desc : String; Aliass : string ) : TEntidad;
begin
     Result := TEntidad.Create;
     with Result do
     begin
          Tipo := TipoEnt;
          NombreTabla := Nombre;
          Alias := Aliass;
          KeyFields   := Llave;
          if Length( Desc ) > 0 then
             Descripcion := Desc
          else
              Descripcion := Nombre;
          dmEntidades := SELF;
     end;
     FListaEntidades.AddObject( Nombre, Result );
end;


procedure TdmEntidades.DestruyeEntidades;
var
   i : Integer;
begin
     //CV:15/oct/2008: Puede ser que nunca se haya llegado a preparar las entidades
     //y que este objeto sea NIL, por lo que marca access violation.
     
     if ( FListaEntidades <> NIL ) then
     begin
          with FListaEntidades do
          begin
               for i := 0 to Count-1 do
                   TEntidad( Objects[ i ] ).Free;
               Free;
          end;
     end;
end;

procedure TdmEntidades.ArregloEntidades;
var
   i : Integer;
   oEntidad : TEntidad;
begin
     PreparaEntidades;
     // Hace m�s r�pido el Acceso a una TEntidad cuando se tiene su TipoEntidad
     with FListaEntidades do
          for i := 0 to Count-1 do
          begin
               oEntidad := TEntidad( Objects[ i ] );
               FArregloEntidades[ oEntidad.Tipo ] := oEntidad;
          end;
end;

procedure TdmEntidades.ConectaEntidades;
var
   i, j : Integer;
   oEntidad : TEntidad;
begin
     PreparaEntidades;
     // Permite a las Relaciones hacer referencia a una Instancia de TEntidad
     // dado que en inicialmente s�lo tienen un TipoEntidad
     with FListaEntidades do
          for i := 0 to Count-1 do
          begin
               oEntidad := TEntidad( Objects[ i ] );
               with oEntidad.Relaciones do
                    for j := 0 to Count-1 do
                        with Items[ j ] do
                             Padre := FArregloEntidades[ TipoPadre ];
          end;
end;

function TdmEntidades.BuscaJoins(const TipoHijo, TipoPadre : TipoEntidad;
                                        oJoins: TStringList ) : Boolean;
var
   oEntidad   : TEntidad;
begin
     PreparaEntidades;
     oEntidad   := FArregloEntidades[ TipoHijo ];
     if ( TipoPadre <> enNinguno ) and ( oEntidad <> NIL ) then
     begin
          ResetRelaciones;
          Result := oEntidad.BuscaJoin( TipoPadre, oJoins )
     end
     else
         Result := FALSE;
end;

function TdmEntidades.GetEntidad(const oTipo:TipoEntidad):TEntidad;
begin
     PreparaEntidades;
     Result := FArregloEntidades[ oTipo ];
end;

function TdmEntidades.GetTipoEntidad(const sTabla: String): TipoEntidad;
var
   iPos : Integer;
begin
     PreparaEntidades;
     // Busca en la Lista previamente Sorteada
     with FListaEntidades do
          if Find( sTabla, iPos ) then
             with Objects[ iPos ] as TEntidad do
                  Result := Tipo
          else
              Result := enNinguno;
end;

procedure TdmEntidades.DataModuleCreate(Sender: TObject);
begin
     FReferenciaCircular := TStringList.Create;
end;

procedure TdmEntidades.dmEntidadesDestroy(Sender: TObject);
begin
     FreeAndNil( FReferenciaCircular );
     DestruyeEntidades;
     inherited;
end;

procedure TdmEntidades.ResetRelaciones;
begin
     if FReferenciaCircular <> NIL then
        FReferenciaCircular.Clear;
end;

function TdmEntidades.DescripcionEntidad(
  const TipoEnt: TipoEntidad): String;
var
   oEntidad : TEntidad;
begin
     PreparaEntidades;
     oEntidad := FArregloEntidades[ TipoEnt ];
     if ( oEntidad <> NIL ) then
        Result := oEntidad.Descripcion;
end;

constructor TdmEntidades.Create( oProvider: TComponent );
begin
     inherited Create(oProvider);
     {$ifdef RDD}
     oZetaProvider := TdmZetaServerProvider( oProvider );
     {$else}
     {$endif}
     PreparaEntidades;

end;

function TdmEntidades.EntidadesPreparadas: Boolean;
begin
     Result := ( FListaEntidades <> NIL );
end;

procedure TdmEntidades.PreparaEntidades;
begin
     if NOT EntidadesPreparadas then
     begin
          {$ifdef RDD}
          with oZetaProvider do
          begin
               if NOT VarIsNull( oZetaProvider.EmpresaActiva ) and
                  NOT VarIsEmpty(oZetaProvider.EmpresaActiva) and
                  NOT ( EmpresaActiva[0] = Comparte[0] ) then
               begin
                    FListaEntidades := TStringList.Create;
                    PreparaQuerys;
               end;
          end;
          {$else}
          FListaEntidades := TStringList.Create;
          {$endif}
          try
             {$ifdef RDD}
             if EntidadesPreparadas then
             begin
             {$endif}
                  CreaEntidades;
                  ArregloEntidades;
                  ConectaEntidades;
                  EntidadesEspeciales;
                  FListaEntidades.Sorted := TRUE;
             {$ifdef RDD}
             end;
             {$endif}
          finally
                 {$ifdef RDD}
                 DesPreparaQuerys;
                 {$else}
                 {$endif}
          end;
     end;
end;

{$ifdef RDD}
procedure TdmEntidades.CreaEntidades;
begin
     with FQEntidades do
     begin
          while NOT EOF do
          begin
               with AddEntidad( TipoEntidad(FieldByName('EN_CODIGO').AsInteger),
                                FieldByName('EN_TABLA').AsString,
                                FieldByName('EN_PRIMARY').AsString,
                                VACIO,
                                FieldByName('EN_ALIAS').AsString ) do
               begin
                    FQRelaciones.Active := FALSE;
                    oZetaProvider.ParamAsInteger(FQRelaciones, 'Entidad', FieldByName('EN_CODIGO').AsInteger );
                    FQRelaciones.Active := TRUE;
                    while NOT FQRelaciones.EOF do
                    begin
                         AddRelacion( TipoEntidad(FQRelaciones.FieldByName('RL_ENTIDAD').AsInteger) ,
                                      FQRelaciones.FieldByName('RL_CAMPOS').AsString ,
                                      ZStrToBool( FQRelaciones.FieldByName('RL_INNER').AsString ) );
                         FQRelaciones.Next;
                    end;
                    {$IFDEF SERVERCAFE}
                     {$IFDEF RDD}
                     if ( TipoEntidad(FieldByName('EN_CODIGO').AsInteger) = enEmpleado ) then
                     begin
                          AddRelacion( enTarjetaXY,'CB_CODIGO,:Fecha');
                     end;
                     {$ENDIF}
                    {$ENDIF}
               end;


               Next;
          end;
     end;
end;
{$ENDIF}

procedure TdmEntidades.EntidadesEspeciales;
{$ifdef RDD}
 var
   oConteo, oEntidad : TEntidad;
   i: integer;
   eTipoConteo : eCamposConteo;
   eEntidad: TipoEntidad;
 {$endif}

begin
     {$ifdef RDD}
     oZetaProvider.InitGlobales;
     oConteo := FArregloEntidades[ enConteo ];
     if oConteo <> NIL then
     begin
          for i:=0 to K_MAX_CONTEO_NIVELES do
           begin
                eTipoConteo := eCamposConteo( oZetaProvider.GetGlobalInteger(K_GLOBAL_CONTEO_NIVEL1+i) );
                if ( eTipoConteo <> coNinguno ) then
                begin
                     eEntidad := ZetaTipoEntidad.GetConteoEntidad( eTipoConteo );
                     oEntidad := FArregloEntidades[eEntidad];
                     if ( oEntidad = NIL ) then
                     begin
                          Exit
                     end
                     else if ( oEntidad.Tipo= enNinguno ) then
                          Exit
                     else
                     begin
                          if oEntidad.Tipo <> enNivel0 then
                             oConteo.AgregaRelacion( oEntidad,'CT_NIVEL_'+ IntToStr(i));
                     end;
                end
                else
                    Exit;
           end;
     end;

     {$else}
     {$endif}
end;



end.
