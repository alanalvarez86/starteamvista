unit ZFuncsCAFE;

{$define QUINCENALES}
{.$undefine QUINCENALES}

interface
Uses Classes, Sysutils, Controls, DB,
     ZetaCommonClasses,
     ZEvaluador,
     ZetaQRExpr,
     DZetaServerProvider;

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );

implementation

uses ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaCommonLists;

{ ************* TZetaCafeEmpleado ************* }
type
    TZetaCafeEmpleado = class( TZetaFunc )
      public
            function Calculate: TQREvResult; override;
      end;

function TZetaCafeEmpleado.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resInt;
          IntResult := oZetaProvider.DatosComida.Empleado;
     end;
end;

{ ************* TZetaCafeFecha ************* }
type TZetaCafeFecha = class( TZetaFunc )
public
  function Calculate: TQREvResult; override;
end;

function TZetaCafeFecha.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resDouble;
          EsFecha := TRUE;
          dblResult := oZetaProvider.DatosComida.Fecha;
     end;
end;

{ ************* TZetaCafeHora ************* }
type TZetaCafeHora = class( TZetaFunc )
public
  function Calculate: TQREvResult; override;
end;

function TZetaCafeHora.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resString;
          StrResult := oZetaProvider.DatosComida.Hora;
     end;
end;

{ ************* TZetaCafeTipo ************* }

type TZetaCafeTipo = class( TZetaFunc )
public
  function Calculate: TQREvResult; override;
end;

function TZetaCafeTipo.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resString;
          StrResult := oZetaProvider.DatosComida.TipoComida;
     end;
end;

{ ************* TZetaCafeEmpresaNombre ************* }

type TZetaCafeEmpresaNombre = class( TZetaFunc )
public
  function Calculate: TQREvResult; override;
end;

function TZetaCafeEmpresaNombre.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resString;
          StrResult := oZetaProvider.DatosComida.EmpresaNombre;
     end;
end;

{ ************* TZetaCafeTurno ************* }

type TZetaCafeTurno = class( TZetaFunc )
public
  function Calculate: TQREvResult; override;
end;

function TZetaCafeTurno.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resString;
          StrResult := oZetaProvider.DatosComida.Turno;
     end;
end;

{ ************* TZetaCafeHorario ************* }

type TZetaCafeHorario = class( TZetaFunc )
public
  function Calculate: TQREvResult; override;
end;

function TZetaCafeHorario.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resString;
          StrResult := oZetaProvider.DatosComida.Horario;
     end;
end;

{ ************* TZetaCafeStatusDia ************* }

type TZetaCafeStatusDia = class( TZetaFunc )
public
  function Calculate: TQREvResult; override;
end;

function TZetaCafeStatusDia.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resInt;
          IntResult := Ord( oZetaProvider.DatosComida.StatusDia );
     end;
end;

{ ************* TZetaCafeStatusEmpleado ************* }

type TZetaCafeStatusEmpleado = class( TZetaFunc )
public
  function Calculate: TQREvResult; override;
end;

function TZetaCafeStatusEmpleado.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resInt;
          IntResult := Ord( oZetaProvider.DatosComida.StatusEmpleado );
     end;
end;

{ ************* TZetaCafeTipoChecada ************* }

type TZetaCafeTipoChecada = class( TZetaFunc )
public
  function Calculate: TQREvResult; override;
end;

function TZetaCafeTipoChecada.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resInt;
          IntResult := Ord( oZetaProvider.DatosComida.TipoChecada );
     end;
end;

{ ************* TZetaCafeComidas ************* }
type TZetaCafeComidas = class( TZetaFunc )
public
  function Calculate: TQREvResult; override;
end;

function TZetaCafeComidas.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resInt;
          IntResult := oZetaProvider.DatosComida.NumComidas;
     end;
end;


{ ************* TZetaCafeEstacion ************* }
type TZetaCafeEstacion = class( TZetaFunc )
public
  function Calculate: TQREvResult; override;
end;

function TZetaCafeEstacion.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resString;
          StrResult := oZetaProvider.DatosComida.Estacion;
     end;
end;

{ ************* TZetaCafeCalcExtras ************* }
type
  TCafeCalcExtras = class( TZetaFunc )
  private
    function Calculate: TQREvResult; override;
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TCafeCalcExtras.GetRequeridos( var TipoRegresa: TQREvResultType );
begin
     TipoRegresa := resInt;
     AgregaRequerido( 'HORARIO.HO_OUTTIME', enHorario)
end;

function TCafeCalcExtras.Calculate: TQREvResult;
var
   sSalida: String;
begin
     sSalida := DataSetEvaluador.FieldByName( 'HO_OUTTIME' ).AsString;
     with Result do
     begin
          Kind := resInt;
          if StrLleno( sSalida ) then
             intResult := iMax( 0, ( aMinutos( DefaultString( 0, oZetaProvider.DatosComida.Hora ) ) - aMinutos( sSalida ) ) )
          else
              intResult := 0;
     end;
end;

{TCafeDiario}
type
TCafeDiario = class( TZetaRegresaQuery )
  private
    function GetTipoComida(const iParam: integer): string;
    procedure PreparaQuery(const iParamFiltro: integer);
  protected
    { Protected Declarations }
    FRegresaScript: Integer;
    FQueryPreparado : Boolean;
    function AgregaFiltro( const sSQL, sFiltro: String ): String;
    function GetResultado( const dFechaI, dFechaF : TDate;
                           const sTipo : String ): TQREvResult;
public
  { Public Declarations }
  constructor Create(oEvaluador:TQrEvaluator);override;
  procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
  function GetScript: String; override;
  function Calculate: TQREvResult;override;

end;

constructor TCafeDiario.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create(oEvaluador);
     FQueryPreparado := FALSE;
end;

function TCafeDiario.GetScript: String;
 const K_CAFE_WHERE =  'where ( CAF_COME.CB_CODIGO = :Empleado ) and ' +
                       '( CAF_COME.CF_FECHA >= :FechaInicial ) and ' +
                       '( CAF_COME.CF_FECHA <= :FechaFinal ) and ' +
                       '( CAF_COME.CF_TIPO = :TipoComida ) ';

begin
     case FRegresaScript of
          1: Result := 'select SUM( CAF_COME.CF_COMIDAS - CAF_COME.CF_EXTRAS ) SUMA from CAF_COME ';
          2: Result := 'select SUM( CAF_COME.CF_COMIDAS ) SUMA from CAF_COME ';
          3: Result := 'select SUM( CAF_COME.CF_EXTRAS ) SUMA from CAF_COME ';
          4: Result := 'select COUNT(*) CUANTOS from CAF_COME ';
     end;
     Result := Result + K_CAFE_WHERE;
end;

function TCafeDiario.AgregaFiltro( const sSQL, sFiltro: String ): String;
begin
     Result := sSQL;
     if StrLleno( sFiltro ) then
     begin
          Result := Result + 'AND' + Parentesis( sFiltro );
     end;
end;

function TCafeDiario.GetTipoComida( const iParam : integer ) : string;
begin
     if ParamVacio(iParam) then
         Result := oZetaProvider.DatosComida.TipoComida
     else
     begin
          if Argument(iParam).Kind = resInt then
             Result := IntToStr(ParamInteger(iParam))
          else Result := ParamString(iParam);
     end;
end;

procedure TCafeDiario.PreparaQuery( const iParamFiltro : integer );
begin
     if NOT FQueryPreparado then
     begin
          FRegresaScript := DefaultInteger( 0, 1 );

          oZetaProvider.PreparaQuery( FQuery, AgregaFiltro( GetScript,
                                                            DefaultString( iParamFiltro, '' )));
          FQueryPreparado := TRUE;
     end;
end;

function TCafeDiario.Calculate: TQREvResult;
 var dComida : TDate;
begin
     Result.Kind := resInt;
     PreparaQuery(3);
     dComida := DefaultDate( 2,oZetaProvider.DatosComida.Fecha );
     Result := GetResultado( dComida, dComida, GetTipoComida(1) );
end;


function TCafeDiario.GetResultado( const dFechaI, dFechaF : TDate;
                                   const sTipo : String ): TQREvResult;
begin
     with oZetaProvider do
     begin
          FQuery.Active := FALSE;
          ParamAsInteger( FQuery,'Empleado', DatosComida.Empleado );
          ParamAsDate( FQuery,'FechaInicial', dFechaI );
          ParamAsDate( FQuery,'FechaFinal', dFechaF );
          ParamAsString( FQuery, 'TipoComida', sTipo );
          FQuery.Active := TRUE;
     end;
     Result := ResultadoQuery(FQuery,0);
     FQuery.Active := FALSE;
end;

procedure TCafeDiario.GetRequeridos( var TipoRegresa: TQREvResultType );
begin
     TipoRegresa := resInt;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

{TCafeSemanal}
type
  TCafeSemanal = class( TCafeDiario )
  public
    procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
    function Calculate: TQREvResult;override;
end;

procedure TCafeSemanal.GetRequeridos( var TipoRegresa: TQREvResultType );
begin
     TipoRegresa := resInt;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

function TCafeSemanal.Calculate: TQREvResult;
begin
     Result.Kind := resInt;
     PreparaQuery(4);
     with oZetaProvider.DatosPeriodo do
          Result := GetResultado(
                                  {$ifdef QUINCENALES}
                                  DefaultDate( 2, InicioAsis ),
                                  DefaultDate( 3, FinAsis ),
                                  {$else}
                                  DefaultDate( 2, Inicio ),
                                  DefaultDate( 3, Fin ),
                                  {$endif}
                                  GetTipoComida(1) );

end;

{TCafeAlDia}
type
TCafeAlDia = class( TCafeDiario )
public
  procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
  function Calculate: TQREvResult;override;
end;

procedure TCafeAlDia.GetRequeridos( var TipoRegresa: TQREvResultType );
begin
     TipoRegresa := resInt;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

function TCafeAlDia.Calculate: TQREvResult;
 var dComida : TDate;
begin
     Result.Kind := resInt;
     PreparaQuery(3);
     dComida := oZetaProvider.DatosComida.Fecha + DefaultInteger( 0, 1 ) - 1;
     Result := GetResultado( dComida,
                             dComida,
                             GetTipoComida(2) );
end;

type
    TZetaCHECO = class( TZetaRegresaQuery )
    private
      FQueryPreparado : Boolean;
    public
          constructor Create(oEvaluador:TQrEvaluator);override;
          function Calculate: TQREvResult; override;
          procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
    end;

constructor TZetaCHECO.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create(oEvaluador);
     FQueryPreparado := FALSE;
end;
procedure TZetaCHECO.GetRequeridos( var TipoRegresa: TQREvResultType );
begin
     TipoRegresa := resBool;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

function TZetaCHECO.Calculate: TQREvResult;
 const K_SQL = 'select COUNT(*) cuantos from CHECADAS where '+
               '( CB_CODIGO = :Empleado ) and ( AU_FECHA = :Fecha ) and ( CH_H_REAL <> ''----'' )';
begin
     if NOT FQueryPreparado then
     begin
          oZetaProvider.PreparaQuery( FQuery, K_SQL );
          FQueryPreparado := TRUE;
     end;
     with oZetaProvider do
     begin
          FQuery.Active := FALSE;
          ParamAsInteger( FQuery, 'Empleado', DatosComida.Empleado );
          ParamAsDate( FQuery, 'Fecha', DatosComida.Fecha );
          FQuery.Active := TRUE;
          Result := ResultadoQuery( FQuery, 0 );
          FQuery.Active := FALSE;
     end;
     with Result do
     begin
          Kind := resBool;
	  booResult := ( intResult > 0 );
     end;
end;

type
  TZetaHayAuto = class( TZetaRegresa )
  private
    function Calculate: TQREvResult; override;
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaHayAuto.Calculate: TQREvResult;
begin
     Result.Kind := resBool;
     Result.booResult := DataSetEvaluador.FieldByName('AU_FECHA').AsDateTime > NullDateTime;
end;

procedure TZetaHayAuto.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resBool;
     AgregaRequerido( 'TARJETAXY.AU_FECHA',  enTarjetaXY );
end;

{*********************** TZetaHORA_DIF *********************}

type
  TZetaHORA_DIF = class( TZetaRegresaquery )
  protected
  public
    function GetScript : String; override;
    function Calculate: TQREvResult; override;
    function GetDifMinutos( iRevisa : Integer; sHora: String; sHorario: String; lUsaGracia: Boolean ) : Integer;   
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaHORA_DIF.GetScript: String;
begin
     Result:= 'select %s %s  from HORARIO where ( HO_CODIGO = %s )';
end;

procedure TZetaHORA_DIF.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resInt;
end;


function TZetaHORA_DIF.Calculate: TQREvResult;
const
     K_REVISA_ENTRADA = 0;
var
   iRevisa: Integer;
   sHora, sHorario : String;
   lUsaGracia: Boolean;
begin
     iRevisa := DefaultInteger( 0, K_REVISA_ENTRADA );
     sHora := DefaultString( 1, oZetaProvider.DatosComida.Hora );
     sHorario := DefaultString( 2, oZetaProvider.DatosComida.Horario );
     lUsaGracia := DefaultBoolean( 3, FALSE );

     with Result do
     begin
          Kind := resInt;
          intResult := 0;  // Por Default regresa 0
     end;
     
     Result.intResult:= GetDifMinutos( iRevisa, sHora, sHorario, lUsaGracia );
end;

function TZetaHORA_DIF.GetDifMinutos( iRevisa : Integer; sHora: String; sHorario: String; lUsaGracia: Boolean ) : Integer;
const
     K_HR_ENTRADA = 0;
     K_HR_SALIDA = 1;
     aTime : array [ 0 .. 6 ] of ZetaPChar = ( 'HO_INTIME', 'HO_OUTTIME', 'HO_OUT_EAT', 'HO_IN_EAT', 'HO_OUT_BRK', 'HO_IN_BRK', 'HO_LASTOUT');
var
   sTime, sGraciaTemp, sGraciaTarde, sFiltro, sFieldMadrugada: String;    // Nombres de Campos
   iMinutosHora, iMinutosMin, iMinutosMax, iMinutosMadrugada: Integer;
   function CheckMinutosMadrugada( iMinutosEvaluar: Integer ): Integer;
   begin
        if ( iMinutosEvaluar <= iMinutosMadrugada ) then
           Result := ( iMinutosEvaluar + K_24HORAS )
        else
            Result := iMinutosEvaluar;
   end;

begin
     Result := 0; 
     case iRevisa of
                    K_HR_ENTRADA :
                    begin
                         sGraciaTemp := 'HO_IN_TEMP';
                         sGraciaTarde := 'HO_IN_TARD';
                    end;
                    K_HR_SALIDA :
                    begin
                         sGraciaTemp := 'HO_OU_TEMP';
                         sGraciaTarde := 'HO_OU_TARD';
                    end;
     end;
     
     lUsaGracia:= lUsaGracia and (iRevisa <= K_HR_SALIDA);
     sTime := aTime[ iRevisa ];     

     if ( iRevisa <=5 ) then
     begin
          sFieldMadrugada := 'HO_LASTOUT ';
     end;

     if ( iRevisa <= K_HR_SALIDA ) then
       sFiltro := ConcatString( ',' + sGraciaTemp, sGraciaTarde,',');

     AbreQuery( Format( GetScript, [ ConcatString(sTime, sFieldMadrugada,','), sFiltro, EntreComillas( sHorario ) ] ) );

     with FQuery do
     begin
          if ( not EOF ) then
          begin
               iMinutosMin := aMinutos( FieldByName( aTime[ iRevisa ] ).AsString );
               if lUsaGracia then    // Va a considerar un rango de minutos validos
               begin
                    iMinutosMax := ( iMinutosMin + FieldByName( sGraciaTarde ).AsInteger );
                    iMinutosMin := ( iMinutosMin - FieldByName( sGraciaTemp ).AsInteger );
               end
               else
               begin
                    iMinutosMax := iMinutosMin;    // Sin rango, misma hora para validar temprano y/o tarde
               end;
               iMinutosHora := aMinutos( aHora24( sHora ) );                       // Se proporciona en formato de hasta 47:59
               { Revisar vs. Ultima de Madrugada }
               iMinutosMadrugada := aMinutos( FieldByName( 'HO_LASTOUT' ).AsString );
               iMinutosMax  := CheckMinutosMadrugada( iMinutosMax );
               iMinutosMin  := CheckMinutosMadrugada( iMinutosMin );
               iMinutosHora := CheckMinutosMadrugada( iMinutosHora );
               { Evalua Minutos Ajustados }
               if ( iMinutosHora < iMinutosMin ) then
                  Result := ( iMinutosHora - iMinutosMin )   // Ser� negativo si es antes de la hora inicial
               else if ( iMinutosHora > iMinutosMax ) then
                  Result := ( iMinutosHora - iMinutosMax );  // Ser� positivo si es despu�s de la hora inicial
          end;
     end;
end;

{*********************** TZetaEN_COMIDA *********************}

type
  TZetaEN_COMIDA = class( TZetaHORA_DIF )
  protected
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaEN_COMIDA.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resBool;
end;

function TZetaEN_COMIDA.Calculate: TQREvResult;
const
     K_REVISA_ENTRADA = 0;
var
   iRevisa, iDifComSalida, iDifComEntrada : Integer;
   sHora, sHorario : String;
begin
     iRevisa := DefaultInteger( 0, K_REVISA_ENTRADA );
     sHora := DefaultString( 1, oZetaProvider.DatosComida.Hora );
     sHorario := DefaultString( 2, oZetaProvider.DatosComida.Horario );

     with Result do
     begin
          Kind := resBool;
          booResult := FALSE;     
     end;

     if ( iRevisa = 0 ) then
     begin
          iDifComSalida:= GetDifMinutos( 2, sHora, sHorario, FALSE );
          iDifComEntrada:= GetDifMinutos( 3, sHora, sHorario, FALSE );
     end
     else
     begin
          iDifComSalida:= GetDifMinutos( 4, sHora, sHorario, FALSE );
          iDifComEntrada:= GetDifMinutos( 5, sHora, sHorario, FALSE );
     end;

     Result.booResult:= ( iDifComSalida >= 0 ) and ( iDifComEntrada <= 0 );
end;

{*********************** TZetaC_ADENTRO *********************}

type
  TZetaC_ADENTRO = class( TZetaRegresaquery )
  protected
  public
    function GetScript : String; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaC_ADENTRO.GetScript: String;
begin
     {$ifdef INTERBASE}
     Result:= 'select RESULTADO from EMPLEADO_ADENTRO( %d, %s )';
     {$endif}
     {$ifdef MSSQL}
     Result:= 'select RESULTADO = dbo.EMPLEADO_ADENTRO( %d, %s )';
     {$endif}
end;

procedure TZetaC_ADENTRO.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resBool;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

function TZetaC_ADENTRO.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resBool;
          booResult := FALSE;     
     end;
     
     with oZetaProvider do
     AbreQuery( Format( GetScript, [ DatosComida.Empleado ,
                                     DateToStrSQLC( DefaultDate( 1, DatosComida.Fecha ) ) ]  ) );

     Result.booResult := System.Odd( ResultadoQuery( FQuery, 0 ).intResult );
end;

{*********************** TZetaC_ULTACCESO *********************}

type
  TZetaC_ULTACCESO = class( TZetaRegresaquery )
  protected
  public
    function GetScript : String; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaC_ULTACCESO.GetScript: String;
begin
     {$ifdef INTERBASE}
     Result:= 'select AL_HORA as RESULTADO from EMPLEADO_ULTACCESO( %d, %s )';
     {$endif}
     {$ifdef MSSQL}
     Result:= 'select RESULTADO = dbo.EMPLEADO_ULTACCESO( %d, %s )';
     {$endif}
end;

procedure TZetaC_ULTACCESO.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resString; 
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

function TZetaC_ULTACCESO.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resString;
          strResult := VACIO ;
     end;

     with oZetaProvider do
     AbreQuery( Format( GetScript, [ DatosComida.Empleado ,
                                     DateToStrSQLC( DefaultDate( 1, DatosComida.Fecha ) ) ]  ) );

     Result :=  ResultadoQuery ( FQuery,0 );
end;

{*********************** TZetaC_ULTCASETA *********************}

type
  TZetaC_ULTCASETA = class( TZetaRegresaquery )
  protected
  public
    function GetScript : String; override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

function TZetaC_ULTCASETA.GetScript: String;
begin
     {$ifdef INTERBASE}
     Result:= 'select AL_CASETA as RESULTADO from EMPLEADO_ULTCASETA( %d, %s )';
     {$endif}
     {$ifdef MSSQL}
     Result:= 'select RESULTADO = dbo.EMPLEADO_ULTCASETA( %d, %s )';
     {$endif}
end;

procedure TZetaC_ULTCASETA.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resString;
     AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

function TZetaC_ULTCASETA.Calculate: TQREvResult;
begin
     with Result do
     begin
          Kind := resString;
          strResult := VACIO ;
     end;
     
     with oZetaProvider do
     AbreQuery( Format( GetScript, [ DatosComida.Empleado ,
                                     DateToStrSQLC( DefaultDate( 1, DatosComida.Fecha ) ) ]  ) );

     Result :=  ResultadoQuery ( FQuery,0 );
end;

{ ************* TZetaAUTO_HORAS ************* }

type
  TZetaAUTO_HORAS = class( TZetaRegresaQuery )
  public
    function Calculate: TQREvResult; override;
    function GetScript : string; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  private
    function GetTipoAuto( iTipo: Integer ): Integer;
  end;

function TZetaAUTO_HORAS.GetScript : string;
begin
{$ifdef INTERBASE}
     Result := SelRes( 'AUTORIZACIONES_SELECT( %d, %s, %d )' );
{$endif}
{$ifdef MSSQL}
     Result := SelRes( 'AUTORIZACIONES_SELECT_HORAS( %d, %s, %d )' );
{$endif}
end;

function TZetaAUTO_HORAS.GetTipoAuto( iTipo: Integer ): Integer;
begin
     case iTipo of
          1: Result := Ord( chExtras );
          2: Result := Ord( chDescanso );
          3: Result := Ord( chConGoce );
          4: Result := Ord( chSinGoce );
          5: Result := Ord( chConGoceEntrada );
          6: Result := Ord( chSinGoceEntrada );
     else
         Result := Ord( chExtras ); // Cualquier valor fuera del rango se considera Horas Extras, Est� repetido en el Case para optimizar ya que lo mas com�n es checar horas extras
     end;
end;

procedure TZetaAUTO_HORAS.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa:= resDouble;
end;

function TZetaAUTO_HORAS.Calculate: TQREvResult;
begin
     Result := EvaluaQuery( Format( GetScript, [ oZetaProvider.DatosComida.Empleado,
                                                 DateToStrSQLC( DefaultDate( 1, oZetaProvider.DatosComida.Fecha ) ),
                                                 GetTipoAuto( DefaultInteger( 0, 1 ) ) ] ) );
end;

{$ifdef PANASONIC}
type
    TZetaLIMITE = class( TZetaRegresaQuery )
    private
      FQueryPreparado : Boolean;
    public
          constructor Create(oEvaluador:TQrEvaluator);override;
          function Calculate: TQREvResult; override;
          procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
    end;

constructor TZetaLIMITE.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create(oEvaluador);
     FQueryPreparado := FALSE;
end;
procedure TZetaLIMITE.GetRequeridos( var TipoRegresa: TQREvResultType );
begin
     TipoRegresa := resInt;
     //AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

function TZetaLIMITE.Calculate: TQREvResult;
const
     K_SQL = 'select RESULTADO = DBO.PE_LIMITE_COMIDAS( :CB_CODIGO, :INICIO, :FIN )';
var
   iEmpleado: integer;
   dInicio, dFin: TDate;
begin
     iEmpleado := DefaultInteger( 0, oZetaProvider.DatosComida.Empleado );
     dInicio := DefaultDate( 1, oZetaProvider.DatosPeriodo.Inicio );
     dFin := DefaultDate( 2, oZetaProvider.DatosPeriodo.Fin );

     if NOT FQueryPreparado then
     begin
          oZetaProvider.PreparaQuery( FQuery, K_SQL );
          FQueryPreparado := TRUE;
     end;
     with oZetaProvider do
     begin
          FQuery.Active := FALSE;
          ParamAsInteger( FQuery, 'CB_CODIGO', iEmpleado );
          ParamAsDate( FQuery, 'INICIO', dInicio );
          ParamAsDate( FQuery, 'FIN', dFin );
          FQuery.Active := TRUE;
          Result := ResultadoQuery( FQuery, 0 );
          FQuery.Active := FALSE;
     end;
     {
     with Result do
     begin
          Kind := resInt;
     end;
     }
end;

type
    TZetaTEXTRA = class( TZetaRegresaQuery )
    private
      FQueryPreparado : Boolean;
    public
          constructor Create(oEvaluador:TQrEvaluator);override;
          function Calculate: TQREvResult; override;
          procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
    end;

constructor TZetaTEXTRA.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create(oEvaluador);
     FQueryPreparado := FALSE;
end;
procedure TZetaTEXTRA.GetRequeridos( var TipoRegresa: TQREvResultType );
begin
     TipoRegresa := resInt;
     //AgregaRequerido( 'COLABORA.CB_CODIGO', enEmpleado );
end;

function TZetaTEXTRA.Calculate: TQREvResult;
const
     K_SQL = 'select RESULTADO = DBO.PE_COMIDAS_EXTRAS( :CB_CODIGO, :FECHA )';
var
   iEmpleado: integer;
   dFecha: TDate;
begin
     iEmpleado := DefaultInteger( 0, oZetaProvider.DatosComida.Empleado );
     dFecha := DefaultDate( 1, oZetaProvider.DatosComida.Fecha );
     if NOT FQueryPreparado then
     begin
          oZetaProvider.PreparaQuery( FQuery, K_SQL );
          FQueryPreparado := TRUE;
     end;
     with oZetaProvider do
     begin
          FQuery.Active := FALSE;
          ParamAsInteger( FQuery, 'CB_CODIGO', iEmpleado );
          ParamAsDate( FQuery, 'FECHA', dFecha );
          FQuery.Active := TRUE;
          Result := ResultadoQuery( FQuery, 0 );
          FQuery.Active := FALSE;
     end;
     {
     with Result do
     begin
          Kind := resInt;
     end;
     }
end;
{$endif}

type
    TZetaCafePEExtRango = class( TZetaRegresaQuery )
public
      function GetScript: String; override;
      function Calculate: TQREvResult;override;
      procedure GetRequeridos( var TipoRegresa: TQREvResultType ); override;
end;

{ TZetaCafePEExtRango }

procedure TZetaCafePEExtRango.GetRequeridos(var TipoRegresa: TQREvResultType);
begin
     inherited;
     TipoRegresa := resInt;
end;

function TZetaCafePEExtRango.GetScript: String;
begin
     {Se trae la hora de los consumos por fecha tipo y estaci�n}
     Result:= 'select CF_HORA from CAF_COME where ( CAF_COME.CB_CODIGO = %d ) and ' +
              '( CAF_COME.CF_FECHA = %s ) and ' +
              '( CAF_COME.CF_TIPO = %s ) and ' +
              '( CAF_COME.CF_RELOJ = %s ) order by CF_HORA';

end;

function TZetaCafePEExtRango.Calculate: TQREvResult;
var
   iDifMinutos: Integer;
begin
     with Result do
     begin
          Kind := resInt;
          IntResult:= 0;  //0: Valor default. Es decir, no le corresponden extras
     end;
     with oZetaProvider.DatosComida do
     begin
          AbreQuery( Format( GetScript, [ Empleado, DateToStrSQLC(Fecha),
                                          EntreComillas( TipoComida ), EntreComillas(Estacion) ] ) );

          with FQuery do
          begin
               if not IsEmpty then  //Solamente revisa el primer consumo del d�a. Orden por hora.
               begin
                    iDifMinutos:= Abs( aMinutos(FieldByName('CF_HORA').AsString) - aMinutos(Hora) );
                    if ( iDifMinutos <= DefaultInteger(0,30) ) then  //Si esta dentro del rango le corresponden extras
                       Result.IntResult:= 99;
               end;
          end;
     end;
end;

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );
begin
     with FunctionLibrary do
     begin
          RegisterFunctionDP( TCafeDiario, 'DIARIO' );
          RegisterFunctionDP( TCafeSemanal, 'SEMANAL', 'Info Comidas Semanal' );
          RegisterFunctionDP( TCafeAlDia, 'CAF_DIA' );
          RegisterFunctionDP( TZetaCHECO, 'CHECO' );
          RegisterFunctionDP( TZetaHayAuto, 'HAY_AUTO' );
          RegisterFunction( TCafeCalcExtras, 'CALC_EXTRAS' );
          RegisterFunction( TZetaCafeEmpleado, 'EMPLEADO' );
          RegisterFunction( TZetaCafeFecha, 'FECHA' );
          RegisterFunction( TZetaCafeHora, 'HORA' );
          RegisterFunction( TZetaCafeTipo, 'TIPO' );
          RegisterFunction( TZetaCafeComidas, 'COMIDAS' );
          { GA: Funciones agregadas para Control de Accesos }
          RegisterFunction( TZetaCafeEmpresaNombre, 'C_COMPANY' );     { Regresa el Nombre de la Empresa }
          RegisterFunction( TZetaCafeTurno, 'C_TURNO' );               { Regresa El Turno Determinado Para La Checada }
          RegisterFunction( TZetaCafeHorario, 'C_HORARIO' );           { Regresa El Horario Determinado Para La Checada }
          RegisterFunction( TZetaCafeStatusDia, 'C_TIPODIA' );         { Regresa El Tipo de Dia: 0=Habil, 1=Sabado, 2=Descanso }
          RegisterFunction( TZetaCafeStatusEmpleado, 'C_STAT_EMP' );   { Regresa El Status Del Empleado: ver eStatusEmpleado en ZetaCommonList.pas }
          RegisterFunction( TZetaCafeTipoChecada, 'C_CHECADA' );       { Regresa El Tipo De Checada: 0=Desconocida, 1=Entrada, 2=Salida }
          RegisterFunction( TZetaHORA_DIF, 'HORA_DIF' );               { Regresa Los minutos de diferencia entre la hora indicada y la entrada o salida del horario }
          RegisterFunction( TZetaAUTO_HORAS, 'AUTO_HORAS' );           { Regresa La Cantidad de Horas de Autorizaci�n del Tipo Indicado }
          {$ifdef PANASONIC}
          RegisterFunction( TZetaLIMITE, 'CLIMITE' );
          RegisterFunction( TZetaTEXTRA, 'CEXTRAS' );
          {$endif}
          RegisterFunction( TZetaEN_COMIDA, 'EN_COMIDA' );
          RegisterFunction( TZetaC_ADENTRO, 'C_ADENTRO' );
          RegisterFunction( TZetaC_ULTACCESO, 'C_ULTACCESO' );
          RegisterFunction( TZetaC_ULTCASETA, 'C_ULTCASETA' );
          RegisterFunction( TZetaCafeEstacion, 'ESTACION' );           { Regresa la estaci�n (cadena de 4 caracteres) en la que se esta consumiendo o registrando acceso}
          RegisterFunction( TZetaCafePEExtRango, 'CAFE_EXTRA' );        { Funci�n especial para el cliente FoxConn, regresa 0 o 99: 0=Si no le
                                                                         corresponden extras 99=Si al empleado le corresponden extras en un rango de tiempo}
     end;

end;


end.
