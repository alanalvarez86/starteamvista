unit ZFuncsMedico;

interface

uses Classes, Sysutils, DB, DBClient, Controls, 
     ZetaQRExpr,
     ZEvaluador,
     DZetaServerProvider,
     ZetaCommonClasses;

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );

implementation

uses ZetaTipoEntidad, ZetaCommonTools, ZetaMedicoTools;

const
     K_SEMANAS_GESTA = 6;
     K_STATUS_INCA = 7;

{ NOTAS:
  1) GetRequeridos es un Procedure
  2) Lo que antes se regresaba en Result, ahora se regresa en una serie
     de llamadas a 'AgregaRequerido' o 'AgregaRequeridoAlias'
  3) Las llamada a 'AgregaScript( x, resError, y )' se sustituyen por
     la llamada a 'EquivaleScript( x )';
}

{TZetaPACIENTE}
type
TZetaPACIENTE = class ( TZetaRegresa )
public
 procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
end;

procedure TZetaPACIENTE.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resString;
     // Si lo pongo desde aqu�, me ahorro la b�squeda del campo calculado en el diccionario
     AgregaScript( PRETTY_EXP, TipoRegresa, False  );
     AgregaRequerido( 'EXPEDIEN.EX_CODIGO', enExpediente ); //Join
end;


{ ************* TZetaOBTIENESTATUS ************* }

type
    eTipoScript = ( eReciente, eAunaFecha, eStatus );
  TZetaOBTIENESTATUS = class( TZetaRegresaQuery )
  public
    function GetExpediente : Integer;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  protected
    iRegresa : integer;
    eTipoStatus: eTipoScript;
    function Calculate: TQREvResult; override;
    procedure CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQREvResultType; var lEsFecha, lAgregaScript: Boolean );virtual;
  end;

function TZetaOBTIENESTATUS.Calculate: TQREvResult;
var
   sFecha, sCampo : string;
   TipoRegresa : TQREvResultType;
   lEsFecha, lAgregaScript : Boolean;
begin
     lAgregaScript := False;
     if ( not ParamVacio( 0 ) and ( Argument( 0 ).Kind = resInt ) ) then
     begin
          eTipoStatus := eReciente;
          iRegresa:= ParamInteger( 0 );
          CaseCampo( iRegresa, sCampo, TipoRegresa, lEsFecha, lAgregaScript );
          EsFecha := lEsFecha;
          Result := EvaluaQuery( Format( GetScript, [ sCampo, IntToStr( GetExpediente )  ] ) );
     end
     else
     begin
          if ParamVacio(0) then
             sFecha := DatetoStrSQLC( oZetaprovider.FechaDefault )
          else
              sFecha := DatetoStrSQLC( ParamDate(0) );

          if ParamVacio(1) then
          begin
               TipoRegresa := resString;
               eTipoStatus := eStatus;
               sCampo := IntToStr( GetExpediente );
               lEsFecha := False;
               Result := EvaluaQuery( Format( GetScript, [ sCampo, sFecha ] ) )
          end
          else
          begin
               eTipoStatus := eAunaFecha;
               iRegresa := ParamInteger(1);
               CaseCampo( iRegresa, sCampo, TipoRegresa, lEsFecha, lAgregaScript );
               EsFecha := lEsFecha;
               Result := EvaluaQuery( Format( GetScript, [ sCampo, InttoStr( GetExpediente ), sFecha ] ) )
          end;
     end;
end;

procedure TZetaOBTIENESTATUS.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   sFecha, sCampo : string;
   lEsFecha, lAgregaScript : Boolean;
begin
     lAgregaScript := True;
     eTipoStatus := eAunaFecha;
     AgregaRequerido( 'EXPEDIEN.EX_CODIGO', enExpediente );
     if ParametrosConstantes then
     begin
          if ( not ParamVacio( 0 ) and ( Argument( 0 ).Kind = resInt ) ) then
          begin
               eTipoStatus := eReciente;
               iRegresa := ParamInteger( 0 );
               CaseCampo( iRegresa, sCampo, TipoRegresa, lEsFecha, lAgregaScript );
               EsFecha := lEsFecha;
               if lAgregaScript then
                  AgregaScript( Format( GetScript, [ sCampo, 'EXPEDIEN.EX_CODIGO' ] ), TipoRegresa, lEsFecha );
          end
          else
          begin
               if ParamVacio(0) then
                  sFecha := DateToStrSQLC( oZetaProvider.FechaDefault )
               else
                   sFecha := ParamCampoFecha( 0 );
               if ParamVacio(1) then
               begin
                    TipoRegresa := resString;
                    eTipoStatus := eStatus;
                    sCampo := 'EXPEDIEN.EX_CODIGO';
                    lEsFecha := False;
                    if lAgregaScript then
                       AgregaScript( Format( GetScript, [ sCampo, sFecha, sFecha ] ), TipoRegresa, lEsFecha );
               end
               else
               begin
                    iRegresa := ParamInteger( 1 );
                    CaseCampo( iRegresa, sCampo, TipoRegresa, lEsFecha, lAgregaScript );
                    EsFecha:= lEsFecha;
                    if lAgregaScript then
                         AgregaScript( Format( GetScript, [ sCampo, 'EXPEDIEN.EX_CODIGO', sFecha ] ), TipoRegresa, lEsFecha );
               end;
          end;
     end;
end;

function TZetaOBTIENESTATUS.GetExpediente : Integer;
begin
     try
        with TZetaCursor( ZetaEvaluador.DataSets[ 0 ] ) do
             Result := FieldByName( 'EX_CODIGO' ).AsInteger
     except
         Result := 0;
     end;
end;

procedure TZetaOBTIENESTATUS.CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQREvResultType; var lEsFecha, lAgregaScript: Boolean );
begin
end;

{ ************* TZetaACCIDENTE ************* }

type
  TZetaACCIDENTE = class( TZetaOBTIENESTATUS )
  public
    function GetScript : String; override;
  protected
    procedure CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQREvResultType; var lEsFecha, lAgregaScript : Boolean );override;
  end;

function TZetaACCIDENTE.GetScript : String;
begin
     case eTipoStatus of
          eReciente : Result := ' select %s from ACCIDENT A1 ' +
                                ' where A1.EX_CODIGO = %s ' +
                                ' and A1.AX_FECHA = ( select MAX( A2.AX_FECHA ) '+
                                                      ' from ACCIDENT A2 '+
                                                      ' where A2.EX_CODIGO = A1.EX_CODIGO )';
          eAunaFecha : Result := ' select %s from ACCIDENT A1 ' +
                                 ' where A1.EX_CODIGO = %s and A1.AX_FECHA = %s ';
          eStatus: Result := SelRes( 'SP_STATUS_ACC( %s, %s, %s )' );
     end;
end;

procedure TZetaACCIDENTE.CaseCampo( const iRegresa : Integer; var sCampo : String; var Tipo : TQREvResultType; var lEsFecha, lAgregaScript : Boolean );
begin
     Tipo := resInt;
     lEsFecha := FALSE;
     case iRegresa of
          1: begin
                  sCampo := 'AX_FECHA';
                  Tipo := resDouble;
                  lEsFecha := TRUE;
             end;
          2: begin
                  sCampo := 'AX_FEC_SUS';
                  Tipo := resDouble;
                  lEsFecha := TRUE;
             end;
          3: begin
                  sCampo := 'AX_TIPO';
             end;
          4: begin
                  sCampo := 'AX_TIP_LES';
             end;
          5: begin
                  sCampo := 'AX_NUMERO';
                  Tipo := resString;
             end;
          6: begin
                  sCampo := 'AX_NUM_INC';
                  Tipo := resString;
             end;
          7: begin
                  sCampo := 'AX_HORA';
                  Tipo := resString;
             end
          else
          begin
               sCampo := 'US_CODIGO';
          end;
     end;
end;

{TZetaEMBARAZO}
type
    TZetaEMBARAZO = class ( TZetaOBTIENESTATUS )
    public
      function GetScript : String; override;
    protected
      sSQL: String;
      FDataSet: TZetaCursor;
      function Calculate: TQREvResult; override;
      function Incapacitado( const dInicio, dFinal, dAqFecha: TDateTime ): Boolean;
      function ChecaParametro( const iParam: Integer ): Boolean;
      function CalculatePrincipal( const iParametro: Integer ): TQREvResult;
      function CalculateDataSet( const iParametro: Integer; const dParametro: TDateTime; const sCampoInicio, sCampoFin, sSQL: String; var lIncapacitado: Boolean ): Integer;
      function CalculateReciente(const sCampoInicio, sCampoFin: String; const iParametro: Integer; var lIncapacitado: Boolean ): Integer;
      function CalculateAunaFecha(const sCampoInicio, sCampoFin: String; const iParametro: Integer; var lIncapacitado: Boolean ): Integer;
      procedure CaseCampo( const iRegresa : Integer; var sCampo: String; var Tipo : TQREvResultType; var lEsFecha, lAgregaScript : Boolean ); override;
end;

function TZetaEMBARAZO.GetScript : String;
begin
     case eTipoStatus of
          eReciente : Result := ' select %s from EMBARAZO EM1 ' +
                                ' where EM1.EX_CODIGO = %s ' +
                                ' and EM1.EM_FEC_UM = ( select MAX( EM2.EM_FEC_UM ) '+
                                                      ' from EMBARAZO EM2 '+
                                                      ' where EM2.EX_CODIGO = EM1.EX_CODIGO ) ';

          eAunaFecha : begin
                            if ( ParamInteger( 1 ) = K_STATUS_INCA ) then
                                Result := ' select %s '+
                                          ' from EMBARAZO E1 '+
                                          ' where E1.EX_CODIGO = %s and '+
                                          ' ( %s between EM_INC_INI and EM_INC_FIN )'
                            else
                                Result := ' select %s '+
                                          ' from EMBARAZO E1 '+
                                          ' where E1.EX_CODIGO = %s and '+
                                          ' ( %s between EM_FEC_UM and EM_FEC_FIN )';
                       end;
          eStatus: Result := SelRes( 'SP_STATUS_EMB( %s, %s, %s )' );
     end
end;

function TZetaEMBARAZO.ChecaParametro( const iParam: Integer ): Boolean;
begin
     Result := ( not ParamVacio( iParam ) and ( Argument( iParam ).Kind = resInt ) and
               ( ParamInteger( iParam ) in [ K_SEMANAS_GESTA, K_STATUS_INCA ] ) );
end;

function TZetaEMBARAZO.Incapacitado( const dInicio, dFinal, dAqFecha: TDateTime ): Boolean;
begin
     with oZetaProvider do
     begin
          Result := ( dInicio <= dAqFecha ) and ( dFinal >= dAqFecha );
     end;
end;

function TZetaEMBARAZO.CalculateReciente(const sCampoInicio, sCampoFin: String; const iParametro: Integer; var lIncapacitado: Boolean ): Integer;
begin
     with oZetaProvider do
     begin
          eTipoStatus := eReciente;
          sSQL := Format( GetScript, [ sCampoInicio + ',' + sCampoFin, IntToStr( GetExpediente )  ] );
          Result := CalculateDataSet( iParametro, FechaDefault, sCampoInicio, sCampoFin, sSQL, lIncapacitado );
     end;
end;

function TZetaEMBARAZO.CalculateAunaFecha(const sCampoInicio, sCampoFin: String; const iParametro: Integer; var lIncapacitado: Boolean ): Integer;
var
   dFechaEsp: TDate;
begin
     if ParamVacio( 0 ) then
          dFechaEsp := oZetaProvider.FechaDefault
      else
          dFechaEsp := ParamDate( 0 );
     eTipoStatus := eAunaFecha;
     sSQL := Format( GetScript, [ sCampoInicio+ ',' +sCampoFin, IntToStr( GetExpediente ), DateToStrSQLC( dFechaEsp ) ] );  //ParamDate(0)
     Result := CalculateDataSet( iParametro, dFechaEsp, sCampoInicio, sCampoFin, sSQL, lIncapacitado );
end;

function TZetaEMBARAZO.Calculate: TQREvResult;
begin
     if ChecaParametro( 0 ) then
     begin
          eTipoStatus := eReciente;
          Result := CalculatePrincipal( ParamInteger( 0 ) );
     end
     else
         if ChecaParametro( 1 ) then
         begin
              eTipoStatus := eAunaFecha;
              Result := CalculatePrincipal( ParamInteger( 1 ) );
         end
         else
             Result := inherited Calculate;
end;

function TZetaEMBARAZO.CalculatePrincipal( const iParametro: Integer ): TQREvResult;
var
   lIncapacitado: Boolean;
begin
     lIncapacitado := False;
     if ( iParametro = K_SEMANAS_GESTA ) then
     begin
          Result.Kind := resInt;
          if ( eTipoStatus = eReciente ) then
              Result.intResult := CalculateReciente( 'EM_FEC_UM', 'EM_FEC_FIN', iParametro, lIncapacitado )
          else
              Result.intResult := CalculateAunaFecha( 'EM_FEC_UM', 'EM_FEC_FIN', iParametro, lIncapacitado );
     end;
     if ( iParametro= K_STATUS_INCA ) then
     begin
          Result.Kind := resBool;
          if ( eTipoStatus = eReciente ) then
              CalculateReciente( 'EM_INC_INI', 'EM_INC_FIN', iParametro, lIncapacitado )
          else
              CalculateAunaFecha( 'EM_INC_INI', 'EM_INC_FIN', iParametro, lIncapacitado );
          Result.booResult := lIncapacitado;
     end;
end;

function TZetaEMBARAZO.CalculateDataSet( const iParametro: Integer; const dParametro: TDateTime; const sCampoInicio, sCampoFin, sSQL: String; var lIncapacitado: Boolean ): Integer;
begin
     Result := 0;
     FDataSet := oZetaProvider.CreateQuery( sSQL );
     try
        with FDataSet do
        begin
             Open;
             if not isEmpty then
             begin
                  if ( iParametro = K_SEMANAS_GESTA ) then
                  begin
                       if ( FieldByName( sCampoInicio ).AsDateTime <= dParametro ) and
                          ( FieldByName( sCampoFin ).AsDateTime >= dParametro ) then
                            Result := GestacionSemanasInt( dParametro, FieldByName( sCampoInicio ).AsDateTime );
                  end;
                  if ( iParametro = K_STATUS_INCA ) then
                  begin
                       lIncapacitado := Incapacitado( FieldByName( sCampoInicio ).AsDateTime, FieldByName( sCampoFin ).AsDateTime, dParametro );
                  end;
             end;
             Close;
        end;
     finally
            FreeAndNil( FDataSet );
     end;
end;

procedure TZetaEMBARAZO.CaseCampo( const iRegresa: Integer; var sCampo: String; var Tipo : TQREvResultType; var lEsFecha, lAgregaScript : Boolean );
begin
     Tipo := resInt;
     lEsFecha := FALSE;
     case iRegresa of
          1: begin
                  sCampo := 'EM_FEC_UM';
                  Tipo := resDouble;
                  lEsFecha := TRUE;
             end;
          2: begin
                  sCampo := 'EM_FEC_PP';
                  Tipo := resDouble;
                  lEsFecha := TRUE;
             end;
          3: begin
                  sCampo := 'EM_INC_INI';
                  Tipo := resDouble;
                  lEsFecha := TRUE;
             end;
          4: begin
                  sCampo := 'EM_INC_FIN';
                  Tipo := resDouble;
                  lEsFecha := TRUE;
             end;
          5: begin
                  sCampo := 'EM_FEC_FIN';
                  Tipo := resDouble;
                  lEsFecha := TRUE;
             end;
          6: begin
                  lAgregaScript := False;
             end;
          7: begin
                  lAgregaScript := False;
                  Tipo := resBool;
             end;
          else
          begin
               sCampo := 'US_CODIGO';
          end;
     end;
end;



{*********************** REGISTER *********************}

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );
begin
     with FunctionLibrary do
     begin
          RegisterFunction( TZetaPACIENTE,'PACIENTE', 'Nombre del Paciente');
          RegisterFunction( TZetaACCIDENTE, 'ACCIDENTE','Servicio M�dico: Datos de un accidente');
          RegisterFunction( TZetaEmbarazo, 'EMBARAZO','Servicio M�dico: Datos de un embarazo');
     end;
end;

end.


