unit DBaseExisteCampo;

interface
{$INCLUDE DEFINES.INC}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient,
  ZetaEntidad,
  ZetaServerDataSet,
  ZetaTipoEntidad,
  ZetaCommonLists,
  ZetaCommonTools,
  DZetaServerProvider;


type
  TdmBaseExisteCampo = class(TDataModule)
  private
    { Private declarations }
    FExisteCampo: TZetaCursor;
    FLookUp: TZetaCursor;
    FLookUpEntidad: TZetaCursor;
    function EsCampoDiccion( const sCampo: string;
                             var sFormula: string;
                             var iTipo : integer;
                             var oTipoEntidad: TipoEntidad ): Boolean;
    function GetNombreTabla(const oEntidad: TipoEntidad): String;
    function oZetaCreator: TDataModule;
  protected
    function oZetaProvider: TdmZetaServerProvider;
    function EntidadEspecial(const oEntidad: TipoEntidad;var sLookup: string): Boolean;virtual;
  public
    {Regresa : La tabla, tipo de campo y si se excluye el campo para el Calculo de Nomina}
    function ExisteCampo( var oTipoEntidad: TipoEntidad;
                          const sCampo: string;
                          var sFormula: string;
                          var iTipo: integer;
                          var lNomParam : Boolean): Boolean; virtual;
    function GetDatosLookUp( const sCampo, sTabla: string;
                             oEntidad:TipoEntidad;
                             var oEntidadLookUp: TipoEntidad;
                             var lEsSelect : Boolean ): String;

end;
const K_ANCHO_NOMBRECAMPO = 10;

implementation

uses ZetaSQL,
     ZCreator;


{$ifdef RDD}
const Q_CampoDiccion = 'select AT_CAMPO DI_NOMBRE, AT_TIPO DI_TFIELD, AT_CONFI DI_CONFI from R_ATRIBUTO '+
                       'where EN_CODIGO = :Clasifi and AT_CAMPO = :Nombre';

{$else}
const Q_CampoDiccion = 'select DI_NOMBRE,DI_TFIELD, DI_CLASIFI, DI_CONFI '+
                       'from DICCION where '+
                       '( DI_CLASIFI =:Clasifi ) and ( DI_NOMBRE =:Nombre ) ';
{$endif}

{$R *.DFM}

{ TdmExisteCampo }

function TdmBaseExisteCampo.oZetaProvider: TdmZetaServerProvider;
begin
     Result := TZetaCreator(oZetaCreator).oZetaProvider;
end;

function TdmBaseExisteCampo.oZetaCreator: TDataModule;
begin
     Result := TZetaCreator(Owner);
end;


function TdmBaseExisteCampo.EsCampoDiccion( const sCampo: string;
                                        var sFormula: string;
                                        var iTipo: integer;
                                        var oTipoEntidad: TipoEntidad): Boolean;
 var sNombreTabla, sNombreCampo : string;
     iPunto : integer;
     iEntidad : TipoEntidad;

     //PENDIENTE:FALTA LO DE EXCLUYE CAMPOS.... PARA PRESTAMOS Y AHORROS
begin
     Result := FALSE;
     iPunto := Pos( '.', sCampo );
     if iPunto > 0 then
     begin
          sNombreTabla := Copy( sCampo, 1, iPunto - 1 );
          sNombreCampo := Copy( sCampo, iPunto + 1, MaxInt );
          iEntidad := TZetaCreator(oZetaCreator).dmEntidadesTress.GetTipoEntidad(sNombreTabla);
     end
     else
     begin
          sNombreTabla := GetNombreTabla( oTipoEntidad );
          sNombreCampo := sCampo;
          iEntidad := oTipoEntidad;
     end;

     if ( iEntidad <> enNinguno ) and
        ( Length( sNombreCampo ) <= K_ANCHO_NOMBRECAMPO ) then
     begin
          FExisteCampo.Active := FALSE;
          with oZetaProvider do
          begin
               ParamAsInteger( FExisteCampo, 'CLASIFI', Ord(iEntidad) );
               ParamAsVarChar( FExisteCampo, 'NOMBRE', sNombreCampo, K_ANCHO_NOMBRECAMPO );
          end;
          FExisteCampo.Active := TRUE;
          with FExisteCampo do
          begin
                //CV Result := NOT EOF;
                if ( NOT EOF ) AND ( oZetaProvider.VerConfidencial OR ( FieldByName( 'DI_CONFI' ).AsString = 'N' )) then
                begin
                     Result := TRUE;
                     sFormula := sNombreTabla+'.'+sNombreCampo;
                     iTipo := FieldByName('DI_TFIELD').AsInteger;
                     // Tiene que regresar el 'TipoEntidad'
                     oTipoEntidad := iEntidad;
                end;//if
          end;
          FExisteCampo.Active := FALSE;
     end;
end;

function TdmBaseExisteCampo.ExisteCampo( var oTipoEntidad: TipoEntidad;
                                     const sCampo: string;
                                     var sFormula: string;
                                     var iTipo: integer;
                                     var lNomParam : Boolean): Boolean;
begin
     if FExisteCampo = NIL then
        FExisteCampo := oZetaProvider.CreateQuery(Q_CampoDiccion);

     lNomParam := FALSE;
     Result := EsCampoDiccion( UpperCase(sCampo),
                               sFormula, iTipo,
                               oTipoEntidad);
     {
     //NOMPARAM ES PROPIO DEL TRESS
     if (Not Result) AND (Length(sCampo)<=K_ANCHO_NOMBRECAMPO) AND
        (oTipoEntidad in NomParamEntidades) then
     begin
          CreateNomParam;
          Result := dmNomParam.EsNomParam( sCampo, sFormula, iTipo );
          lNomParam := Result;
     end;
     }
end;

function TdmBaseExisteCampo.GetNombreTabla( const oEntidad: TipoEntidad): String;
begin
     Result :=  TZetaCreator(oZetaCreator).dmEntidadesTress.NombreEntidad( oEntidad );
end;


function TdmBaseExisteCampo.EntidadEspecial(const oEntidad: TipoEntidad; var sLookup:string ) : Boolean;
begin
     Result := FALSE;
end;

function TdmBaseExisteCampo.GetDatosLookUp( const sCampo, sTabla: string;
                                        oEntidad: TipoEntidad;
                                        var oEntidadLookUp: TipoEntidad;
                                        var lEsSelect : Boolean ): String;
{$ifdef RDD}

 const Q_LOOKUP = 'select AT_TIPO, AT_TRANGO , AT_ENTIDAD, LV_CODIGO '+
                  'from R_ATRIBUTO where  '+
                  '( EN_CODIGO = :Clasifi ) and  '+
                  '( AT_CAMPO = :Nombre )';
       Q_LOOKUPENTIDAD = 'select R_ENTIDAD.EN_ATDESC '+
                         'from R_ENTIDAD where '+
                         '( EN_CODIGO = :CLASIFI )';
{$else}
 const Q_LOOKUP = 'select DI_CALC, DI_REQUIER, DI_MASCARA, DI_ANCHO, DI_TFIELD, DI_TRANGO, DI_NUMERO, DI_CLASIFI, DI_TITULO  '+
                  'from DICCION where  '+
                  '( DI_CLASIFI = :Clasifi ) and  '+
                  '( DI_NOMBRE = :Nombre )';
       Q_LOOKUPENTIDAD = 'select D1.DI_REQUIER '+
                         'from DICCION D1 where '+
                         '( DI_CLASIFI = -1 ) and '+
                         '( DI_CALC = :CLASIFI )';
{$endif}
 var sRequiere : string;
     iPos, iEntidad  : integer;
begin
     Result := '';
     if FLookUp = NIL then
        FLookUp := oZetaProvider.CreateQuery(Q_LOOKUP);

     with FLookUp do
     begin
          Active := FALSE;
          oZetaProvider.ParamAsInteger(FLookUp,'Clasifi',Ord(oEntidad));
          oZetaProvider.ParamAsVarChar(FLookUp,'Nombre',sCampo,K_ANCHO_NOMBRECAMPO);
          Active := TRUE;
          if ( NOT EOF ) then
          begin
               {$ifdef RDD}
               case eTipoRango( FieldByName('AT_TRANGO').AsInteger ) of
               {$else}
               case eTipoRango( FieldByName('DI_TRANGO').AsInteger ) of
               {$endif}

                    rNinguno,rFechas,rBool : Result := sTabla+'.'+sCampo;
                    rRangoEntidad :
                    begin
                         {$ifdef RDD}
                         iEntidad := FieldByName('AT_ENTIDAD').AsInteger;
                         {$else}
                         iEntidad := FieldByName('DI_NUMERO').AsInteger;
                         {$endif}

                         if iEntidad > 0 then
                          begin
                               oEntidad := TipoEntidad(iEntidad);
                               if EntidadEspecial( oEntidad, Result ) then Exit;

                               oEntidadLookUp := oEntidad;
                               if FLookUpEntidad = NIL then
                                  FLookUpEntidad := oZetaProvider.CreateQuery( Q_LOOKUPENTIDAD );
                               with FLookUpEntidad do
                               begin
                                    Active := FALSE;
                                    oZetaProvider.ParamAsInteger( FLookUpEntidad, 'CLASIFI', iEntidad );
                                    Active := TRUE;
                                    if not FLookUpEntidad.Eof then
                                    begin
                                         sRequiere := Fields[0].AsString;
                                         iPos := Pos(';',sRequiere);
                                         if iPos > 0 then
                                            sRequiere := Copy( sRequiere, iPos+1, MaxInt );

                                         iPos := Pos(';',sRequiere);
                                         if iPos > 0 then
                                            sRequiere := Copy( sRequiere, 0, iPos-1 );

                                         Result := sRequiere;
                                    end;
                                    Active := FALSE;
                               end;
                          end;
                    end;
                    rRangoListas:
                    begin
                         {$ifdef RDD}
                         if eTipoGlobal(FieldByName('AT_TIPO').AsInteger) = tgNumero then
                         {$else}
                         if eTipoGlobal(FieldByName('DI_TFIELD').AsInteger) = tgNumero then
                         {$endif}

                         begin
                              {$ifdef RDD}
                              iEntidad := FieldByName('LV_CODIGO').AsInteger;
                              {$else}
                              iEntidad := FieldByName('DI_NUMERO').AsInteger;
                              {$endif}
                              Result := Format( 'SELECT TF_DESCRIP FROM '+
                                                'TFIJAS WHERE TF_TABLA=%d '+
                                                'AND TF_CODIGO = %s ',
                                                [iEntidad,sTabla+'.'+sCampo] );
                              lEsSelect := TRUE;
                         end
                         else Result := sTabla+'.'+sCampo;
                    end;
               end;
          end;
          Active := FALSE;
     end;
end;

end.



