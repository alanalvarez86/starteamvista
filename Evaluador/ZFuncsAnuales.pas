unit ZFuncsAnuales;

interface

uses Classes, Sysutils,
     ZetaQRExpr,
     ZEvaluador,
     ZetaCommonClasses;

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );

implementation

uses DZetaServerProvider,
     DSuperReporte;

{$ifdef ANTESVERSION2010}
const K_FUNC_BRUTO =1;
      K_FUNC_EXENTO =2;
{$else}
const K_FUNC_BRUTO =2;
      K_FUNC_EXENTO =3;
{$endif}

type TZetaISPTAnualMONTO = class( TZetaRegresaFloat )
  public
    function Calculate: TQREvResult; override;
end;

function TZetaISPTAnualMonto.Calculate: TQREvResult;
begin
     with Result Do
     begin
          Kind := resDouble;
          {Las POSICIONES de K_FUNC_BRUTO y K_FUNC_EXENTO
          estan dadas en el proceso ISPTAnual en el
          metodo ISPTAnualBuildDataset}
          with TdmSuperReporte(ZetaEvaluador.oZetaCreator.SuperReporte) do
               dblResult := GetResult( K_FUNC_BRUTO )-
                            GetResult( K_FUNC_EXENTO );
     end;
end;

type TZetaISPTAnualEXENTO = class( TZetaRegresaFloat )
  public
    function Calculate: TQREvResult; override;
end;

function TZetaISPTAnualEXENTO.Calculate: TQREvResult;
begin
     with Result Do
     begin
          Kind := resDouble;
          {Las POSICIONES de K_FUNC_BRUTO y K_FUNC_EXENTO
          estan dadas en el proceso ISPTAnual en el
          metodo ISPTAnualBuildDataset}
          with TdmSuperReporte(ZetaEvaluador.oZetaCreator.SuperReporte) do
               dblResult := GetResult( K_FUNC_EXENTO );
     end;
end;

type TZetaISPTAnualSUBSIDIO = class( TZetaRegresaFloat )
  public
    function Calculate: TQREvResult; override;
end;

function TZetaISPTAnualSUBSIDIO.Calculate: TQREvResult;
begin
     with Result Do
     begin
          Kind := resDouble;
          dblResult := oZetaProvider.ParamList.ParamByName('Subsidio').AsFloat;
     end;
end;

type TZetaISPTAnualSALMIN = class( TZetaRegresaFloat )
  public
    function Calculate: TQREvResult; override;
end;

function TZetaISPTAnualSALMIN.Calculate: TQREvResult;
begin
     with Result Do
     begin
          Kind := resDouble;
          dblResult := 0;
     end;
end;

type TZetaAnualVALUMA = class( TZetaRegresaFloat )
  public
    function Calculate: TQREvResult; override;
end;

function TZetaAnualVALUMA.Calculate: TQREvResult;
begin
     with Result Do
     begin
          Kind := resDouble;
          dblResult := 0;
     end;
end;

{ ************* TZetaISPTAnualCOMPARA ************* }

type
  TZetaISPTAnualCOMPARA = class( TZetaRegresa )
  public
        procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaISPTAnualCOMPARA.GetRequeridos( var TipoRegresa : TQREvResultType );
 var
    sCampo, sEmpleado, sYear: string;
begin
     TipoRegresa := resDouble;
     case DefaultInteger( 0, 1 ) of
          1 :
          begin
               sCampo := 'C.CP_CALCULO';
               TipoRegresa := resString
          end;
          2 : sCampo := 'C.CP_A_PAGAR';
          3 : sCampo := 'C.CP_FAVOR';
          4 : sCampo := 'C.CP_CONTRA';
          5 : sCampo := 'C.CP_PAGADO';
          6 : sCampo := 'C.CP_RETENID';
          7 : sCampo := 'C.CP_GRAVADO';
          8 : sCampo := 'C.CP_TOT_PER';
          9 : sCampo := 'C.CP_ART_141';
          10 : sCampo := 'C.CB_SALARIO';
     end;

     if ParamVacio(1) then
        sEmpleado := 'COLABORA.CB_CODIGO'
     else
         sEmpleado :=  ParamCampoInteger(2);


     if ParamVacio(2) then
        sYear := oZetaProvider.ParamList.ParamByName('Year').AsString
     else
         sYear :=  ParamCampoInteger(3);

     AgregaScript( Format( 'SELECT %s FROM COMPARA C WHERE C.CP_YEAR = %s AND C.CB_CODIGO = %s ', [ sCampo, sYear, sEmpleado] ) , TipoRegresa, False );
end;

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );
begin
     with FunctionLibrary do
     begin
          RegisterFunction( TZetaISPTAnualMONTO, 'MONTO', 'Monto' );
          RegisterFunction( TZetaISPTAnualEXENTO, 'EXENTO', 'C�lculo de exento' );
          RegisterFunctionDP( TZetaISPTAnualSUBSIDIO, 'SUBSIDIO', 'C�lculo de subsidio' );
          RegisterFunction( TZetaISPTAnualSALMIN, 'SAL_MIN', 'Salario M�nimo por Zona' );
          RegisterFunction( TZetaISPTAnualCOMPARA, 'RCOMPARA', 'Info del Comparativo Anual' );
          RegisterFunction( TZetaAnualVALUMA , 'VAL_UMA', 'Unidad de Medida y Actualizaci�n' );
     end;
end;

end.
