unit ZFuncsCliente;

interface

uses Classes, Sysutils, DB, Forms,
     {$ifndef VER130}Variants,{$endif}
     QRExpr, Controls,ZetaCommonLists,ZReportConst;

var
   TipoValor1,TipoValor2: TipoEstado;
   Valor1,Valor2,FActiveFormCaption: string;
   ImprimeGrid,EsSoloTotales  : Boolean;
   nGruposListado : integer;
   Grupos : Array[1..K_MAXGRUPOS,1..3] of string;
   ParametrosReporte : OleVariant;
   
procedure RegistraFunciones;
procedure RegistraFuncionesCliente;
procedure RegistraFuncionesTotales;
procedure RegistraFuncListadoTotales;

implementation

uses DGlobal,
{$ifndef TRESSCFG}
     DReportes,
{$endif}
     {FTressShell,}
     ZGlobalTress,
     ZetaCommonTools,
     ZetaCommonClasses;

type  TResultados = 1..100;
var aResultados : array[ TResultados ] of TPesos;


type
  TGetGlobal = class( TQREvElementFunction )
  public
    function Calculate: TQREvResult; override;
  end;

{***(@am): Se cambia el envio del resultado de strResult a StringVal porque en XE5 el tipo de dato
        de strResult es un string[255], es decir que esta limitado a 255. Por lo tanto cuando la
        cadena se pase de ese valor no la mostrara.***}
function TGetGlobal.Calculate: TQREvResult;
 {$ifndef RDDAPP}
var
   iGlobal: Integer;
 {$ENDIF}
begin
     {$ifdef RDDAPP}
     with Result do
     begin
          Kind := resString;
          {$IFDEF TRESS_DELPHIXE5_UP}StringVal{$ELSE} strResult {$ENDIF}:=VACIO ;
     end;
     {$ELSE}
     if ( ArgList.Count < 1 ) then
        raise Exception.Create('Faltan Parametros Fijos');

     with Argument( 0 ) do
     begin
          case Kind of
               resDouble: iGlobal := Trunc( dblResult );
               resInt:    iGlobal := intResult;
               resString: iGlobal := StrToIntDef( {$IFDEF TRESS_DELPHIXE5_UP}StringVal{$ELSE} strResult {$ENDIF}, 0 );
          else
              raise Exception.Create( 'Parametro #1 No es Num�rico' );
          end;
     end;
     with Result do
     begin
          case ZGlobalTress.GetTipoGlobal( iGlobal ) of
               tgBooleano:
               begin
                    Kind := resBool;
                    BooResult := Global.GetGlobalBooleano( iGlobal );
               end;
               tgFloat:
               begin
                    Kind := resDouble;
                    dblResult := Global.GetGlobalReal( iGlobal );
               end;
               tgNumero:
               begin
                    Kind := resInt;
                    intResult := Global.GetGlobalInteger( iGlobal );
               end;
               tgFecha:
               begin
                    Kind := resDouble;
                    //resFecha:= TRUE; PENDIENTE ?????
                    dblResult := Global.GetGlobalDate( iGlobal );
               end;
          else
              begin             //tgTexto, tgMemo, tgAutomatico
                   Kind := resString;
                  {$IFDEF TRESS_DELPHIXE5_UP} StringVal {$ELSE} strResult {$ENDIF}:= Global.GetGlobalString( iGlobal );
              end;
          end;
     end;
     {$ENDIF}
end;

type TZetaREPORTE = class( TQREvElementFunction )
  public
  function Calculate: TQREvResult; override;
end;

{***(@am): Se cambia el envio del resultado de strResult a StringVal porque en XE5 el tipo de dato
        de strResult es un string[255], es decir que esta limitado a 255. Por lo tanto cuando la
        cadena se pase de ese valor no la mostrara.***}

function TZetaREPORTE.Calculate: TQREvResult;
 var iParam : integer;

 function GetDescValor( const eTipo : TipoEstado ): string;
 begin
      case eTipo of
           {$ifdef ADUANAS}
           stContribuyente: Result := 'Contribuyente : ';
           stFecha: Result := 'Fecha : ';
           {$else}
           stEmpleado : Result := 'Empleado : ';
           stFecha,stSistema : Result := 'Fecha : ';
           {$endif}
      else
          Result := '';
      end;
 end;
{$IFDEF TRESS_DELPHIXE5_UP}
begin
     iParam := Argument( 0 ).IntResult;

     with Result do
     begin
	  Kind := resString;
	  case iParam of
	       1,8 :  StringVal := FActiveFormCaption;
               6 :
               begin
                    if ImprimeGrid then
                    begin
                         //if ( TipoValor1 <> stNinguno ) then
                         StringVal := GetDescValor(TipoValor1) + Valor1;
                         if StrLleno( StringVal ) then
                            StringVal := StringVal + CR_LF;
                         //if ( TipoValor2 <> stNinguno ) then
                         StringVal := StringVal + GetDescValor(TipoValor2) + Valor2;
                    end
                    else  StringVal := '';
               end;
               2 :
               begin
                    if ImprimeGrid then
                        StringVal := 'Impresi�n de Tabla'
                    else  StringVal := 'Impresi�n de Gr�fica'
               end;
               3 :
               begin
                    if ImprimeGrid then
                       StringVal := 'IGRID'
                    else StringVal := 'IGRAFICA'
               end;
               else
                   StringVal := ''
          end;
     end;
end;
{$ELSE}

begin
     iParam := Argument( 0 ).IntResult;

     with Result do
     begin
	  Kind := resString;
	  case iParam of
	       1,8 : strResult := FActiveFormCaption;
               6 :
               begin
                    if ImprimeGrid then
                    begin
                         //if ( TipoValor1 <> stNinguno ) then
                         strResult := GetDescValor(TipoValor1) + Valor1;
                         if StrLleno( strResult ) then
                            strResult :=  strResult + CR_LF;
                         //if ( TipoValor2 <> stNinguno ) then
                         strResult := strResult + GetDescValor(TipoValor2) + Valor2;
                    end
                    else  strResult := '';
               end;
               2 :
               begin
                    if ImprimeGrid then
                       strResult := 'Impresi�n de Tabla'
                    else  strResult := 'Impresi�n de Gr�fica'
               end;
               3 :
               begin
                    if ImprimeGrid then
                       strResult := 'IGRID'
                    else  strResult := 'IGRAFICA'
               end;
               else
                     strResult := ''
          end;
     end;
end;
{$ENDIF}

{***(@am): Se cambia el envio del resultado de strResult a StringVal porque en XE5 el tipo de dato
        de strResult es un string[255], es decir que esta limitado a 255. Por lo tanto cuando la
        cadena se pase de ese valor no la mostrara.***}
type TZetaFILTRO = class( TQREvElementFunction )
public
  function Calculate: TQREvResult; override;
end;

function TZetaFILTRO.Calculate: TQREvResult;
begin
     Result.Kind := resString;
     Result.{$IFDEF TRESS_DELPHIXE5_UP} StringVal {$ELSE} strResult {$ENDIF}:= '';
end;

{ *********************************************
  Las Funciones SUMA_RESULT y SAVE_RES son para
  Listados que utilicen el criterio de
  Totalizacion = SobreTotales
  *********************************************}
{ ************ TZetaSumaResult *************}
type TZetaSumaResult = class( TQREvElementFunction )
public
  function Calculate: TQREvResult; override;
end;

function TZetaSumaResult.Calculate : TQREvResult;
var
   iLower, iUpper, iResult : Integer;
   nSuma : TPesos;
begin
     iLower := Argument( 0 ).IntResult;
     if ( ArgList.Count < 2 ) then
        iUpper := iLower
     else iUpper := Argument( 1 ).IntResult;

     nSuma := 0;
     for iResult := iLower to iUpper do
     begin
     //'TOTAL_'+IntToStr(iResult)
     //     with dmReportes,SQLAgenteRep.GetColumna(iResult-1) do
     //     nSuma := nSuma + cdsResultados.FieldByName('TOTAL_'+IntToStr(PosAgente)).AsFloat;
            nSuma := nSuma + aResultados[iResult];
     end;

     Result.Kind      := resDouble;
     Result.dblResult := nSuma;
end;

type
TZetaSaveResult = class( TQREvElementFunction )
  private
    FSaveResult : TQREvResult;
  public
    constructor Create; override;
    procedure Aggregate; override;
    function Calculate : TQREvResult; override;
  end;

constructor TZetaSaveResult.Create;
begin
     inherited Create;
     IsAggreg := true;
     FSaveResult.Kind := resError;
end;

procedure TZetaSaveResult.Aggregate;
 var iPosicion : integer;
begin
     FSaveResult := Argument(1);
     iPosicion := Argument( 0 ).IntResult;
     //EsFecha  := FSaveResult.ResFecha;
     with FSaveResult do
       case Kind of
           resInt    : aResultados[ iPosicion ] := intResult;
           resDouble : aResultados[ iPosicion ] := dblResult;
           else aResultados[ iPosicion ] := 0;
       end
end;


function TZetaSaveResult.Calculate : TQREvResult;
begin
     Result := FSaveResult;
end;

type TZetaTITULO = class( TQREvElementFunction )
  public
    function Calculate: TQREvResult; override;
end;

{***(@am): Se cambia el envio del resultado de strResult a StringVal porque en XE5 el tipo de dato
        de strResult es un string[255], es decir que esta limitado a 255. Por lo tanto cuando la
        cadena se pase de ese valor no la mostrara.***}
function TZetaTITULO.Calculate: TQREvResult;
 var sCodigo, sField, sEmpleado : string;
     iCodigo : integer;
     FDataSet : TDataSet;
 function GetNombreEmpleado : string;
 begin
      if sEmpleado > '' then
         with FDataset do
              Result := '    Empleado ' + sEmpleado +
                        ' : ' +
                        Trim(FieldByName('CB_APE_PAT').AsString) + ' ' +
                        Trim(FieldByName('CB_APE_MAT').AsString) + ', ' +
                        Trim(FieldByName('CB_NOMBRES').AsString) ;
 end;

 function GetCountDes : string;
  var iTotal : integer;
 begin
      iTotal := FDataSet.FieldByName('TA_TOTAL').AsInteger;
         Result := ' -- ' + IntToStr( iTotal ) + ' Empleado';
      if iTotal > 1 then
         Result := Result + 's'
 end;

 function GetFieldString(const iPosicion: integer) : string;
 begin
      Result :='';
      if StrLleno(Grupos[iPosicion][3]) then
         Result := FDataSet.FieldByName(Grupos[iPosicion][3]).AsString
 end;

 function GetDesc(const iPosicion : integer) : string;
  var sDescrip : string;
 begin
      sDescrip := GetFieldString(iPosicion);
      Result := '       ' + Grupos[iPosicion][2] + ' ' + sField +
                ': '+ sDescrip;

 end;

 function GetDescripcionTotal(const iPosicion : integer) : string;
  var sDescrip : string;
 begin
      Result := '';
      if (iPosicion in [1..K_MAXGRUPOS]) AND
         ((NOT EsSoloTotales) OR
         ((EsSoloTotales) AND (iPosicion < nGruposListado))) then
      begin
           with FDataSet do
           begin
                sDescrip := FieldByName('TA_NIVEL'+IntToStr(iPosicion)).AsString;
                if sDescrip <> Q_GRUPOS then
                begin
                     Result :=
                     'Totales de ' +
                     Grupos[iPosicion][2] + ' ' + sDescrip +
                     ': '+ GetFieldString(iPosicion) +
                     GetCountDes
                end;
           end;
      end;
 end;

 function GetDescripcion(const iPosicion : integer) : string;
 begin
      {Result := '';
      //if sField <> Q_ZZZ then
      if (sField <> Q_ZZZ) AND (sEmpleado <> Q_MAX_EMPLEADO) then
      begin
           Result := GetDesc(iPosicion);
      end;
      if EsSoloTotales then
         if (sField <> Q_ZZZ) then
            Result := Trim(GetDesc(iPosicion)) + GetCountDes
         else Result := GetDescripcionTotal(iPosicion);}

      Result := '';
      if sField <> Q_EMPRESA then
      begin
           Result := GetDesc(iPosicion); 
           {Result := '       ' + Grupos[iPosicion][2] + ' ' + sField +
                     ': '+ sDescrip;}
           if EsSoloTotales then
              Result := Trim(Result) + GetCountDes;
      end;
 end;
{$IFDEF TRESS_DELPHIXE5_UP}
begin
     Result.Kind := resString;
     Result.StringVal:= '';
{$ifndef TRESSCFG}
     FDataSet := dmReportes.cdsResultados;
{$endif}

     iCodigo := Argument( 0 ).IntResult;
     sCodigo := 'TA_NIVEL'+ IntToStr(iCodigo);

     sField := FDataSet.FieldByName(sCodigo).AsString;
     with FDataSet.FieldByName('CB_CODIGO') do
          if AsInteger > 0 then
             sEmpleado := FDataSet.FieldByName('CB_CODIGO').AsString
          else sEmpleado := Q_MAX_EMPLEADO;

     if nGruposListado > 0 then //Mas de un Grupo
     begin
          if iCodigo = nGruposListado+1 then
          begin
               if sEmpleado < Q_MAX_EMPLEADO then
                  Result.StringVal:= GetNombreEmpleado
               else if (sField <> Q_EMPRESA) then
                    Result.StringVal:= GetDescripcionTotal(iCodigo-1)
          end
          else
          begin
               if ( iCodigo = 1 ) then
               begin
                    if ( sField = Q_EMPRESA ) then
                       Result.StringVal:= 'Totales de Empresa' + GetCountDes
                    else if iCodigo = nGruposListado then
                         Result.StringVal:= GetDescripcion(iCodigo)
                    else  Result.StringVal:= GetDesc(iCodigo);
               end
               else if sField = Q_GRUPOS then Result.StringVal := GetDescripcionTotal(iCodigo-1)
               else Result.StringVal := GetDescripcion(iCodigo);
          end;
     end
     else //Nada mas esta Agrupado por Empleado
     begin
          if sEmpleado < Q_MAX_EMPLEADO then
             Result.StringVal := GetNombreEmpleado
          else
          begin
               if (sField = Q_EMPRESA) then
                  Result.StringVal := 'Totales de Empresa' + GetCountDes;
          end
     end;
end;
{$ELSE}
begin
     Result.Kind := resString;
     Result.strResult:= '';
{$ifndef TRESSCFG}
     FDataSet := dmReportes.cdsResultados;
{$endif}

     iCodigo := Argument( 0 ).IntResult;
     sCodigo := 'TA_NIVEL'+ IntToStr(iCodigo);

     sField := FDataSet.FieldByName(sCodigo).AsString;
     with FDataSet.FieldByName('CB_CODIGO') do
          if AsInteger > 0 then
             sEmpleado := FDataSet.FieldByName('CB_CODIGO').AsString
          else sEmpleado := Q_MAX_EMPLEADO;

     if nGruposListado > 0 then //Mas de un Grupo
     begin
          if iCodigo = nGruposListado+1 then
          begin
               if sEmpleado < Q_MAX_EMPLEADO then
                  Result.strResult:= GetNombreEmpleado
               else if (sField <> Q_EMPRESA) then
                    Result.strResult:= GetDescripcionTotal(iCodigo-1)
          end
          else
          begin
               if ( iCodigo = 1 ) then
               begin
                    if ( sField = Q_EMPRESA ) then
                       Result.strResult:= 'Totales de Empresa' + GetCountDes
                    else if iCodigo = nGruposListado then
                         Result.strResult:= GetDescripcion(iCodigo)
                    else  Result.strResult:= GetDesc(iCodigo);
               end
               else if sField = Q_GRUPOS then Result.strResult := GetDescripcionTotal(iCodigo-1)
               else Result.strResult := GetDescripcion(iCodigo);
          end;
     end
     else //Nada mas esta Agrupado por Empleado
     begin
          if sEmpleado < Q_MAX_EMPLEADO then
             Result.strResult := GetNombreEmpleado
          else
          begin
               if (sField = Q_EMPRESA) then
                  Result.strResult := 'Totales de Empresa' + GetCountDes;
          end
     end;
end;
{$ENDIF}

function ParamInteger( const oParam: TQREvResult ): Integer;
begin
     with oParam do
     begin
          case Kind of
               resDouble: Result := Trunc( dblResult );
               resInt:    Result := intResult;
               resString: Result := StrToIntDef( {$IFDEF TRESS_DELPHIXE5_UP}StringVal{$ELSE} strResult {$ENDIF}, 0 );
          else
              raise Exception.Create( 'Par�metro No es Num�rico' );
          end;
     end;
end;

function ParamTasa( const oParam: TQREvResult ): TTasa;
begin
     with oParam do
     begin
          case Kind of
               resDouble: Result := dblResult;
               resInt:    Result := intResult;
          else
              raise Exception.Create( 'Par�metro No es Num�rico' );
          end;
     end;
end;

function ParamBoolean( const oParam: TQREvResult ): Boolean;
begin
     with oParam do
     begin
          if ( Kind = resBool ) then
             Result := booResult
          else
              raise Exception.Create('Parametro No es L�gico' );
     end;
end;

procedure ParametrosFijos( ArgList: TList; const iParams : integer );
begin
     if ( ArgList.Count < iParams ) then
        raise Exception.Create('Faltan Parametros Fijos');
end;

type
    TZetaABS = class( TQREvElementFunction )
    private
    public
      function Calculate : TQREvResult; override;
    end;

function TZetaABS.Calculate: TQREvResult;
begin
     ParametrosFijos( ArgList, 1 );
     with Result do
     begin
          Kind := resDouble;
          dblResult := Abs( ParamTasa( Argument( 0 ) ) );
     end;
end;

type
    TZetaINT = class( TQREvElementFunction )
    private
    public
      function Calculate : TQREvResult; override;
    end;

function TZetaINT.Calculate : TQrEvResult;
begin
     ParametrosFijos( ArgList, 1 );
     with Result do
     begin
          Kind := resInt;
          intResult := Trunc( ParamTasa( Argument( 0 ) ) );
     end
end;

type
    TZetaMAX = class( TQREvElementFunction )
    private
    public
      function Calculate : TQREvResult; override;
    end;

{CV: 1-dic-2001:
Esta funcion es diferente a la que est� en ZFuncsGenerales.
Esta no compara entre fechas, en el evaluador del cliente no
tenemos manera de saber que un dato es tipo fecha}
function TZetaMAX.Calculate : TQrEvResult;
begin
     ParametrosFijos( ArgList, 2 );
     with Result do
     begin
          Kind := resDouble;
          dblResult := rMax( ParamTasa( Argument( 0 ) ), ParamTasa( Argument( 1 ) ) );
     end
end;

type
    TZetaMIN = class( TQREvElementFunction )
    private
    public
      function Calculate : TQREvResult; override;
    end;
{CV: 1-dic-2001:
Esta funcion es diferente a la que est� en ZFuncsGenerales.
Esta no compara entre fechas, en el evaluador del cliente no
tenemos manera de saber que un dato es tipo fecha}
function TZetaMIN.Calculate : TQrEvResult;
begin
     ParametrosFijos( ArgList, 2 );
     with Result do
     begin
          Kind := resDouble;
          dblResult := rMin( ParamTasa( Argument( 0 ) ), ParamTasa( Argument( 1 ) ) );
     end
end;

type
    TZetaROUND = class( TQREvElementFunction )
    private
    public
      function Calculate : TQREvResult; override;
    end;

function TZetaROUND.Calculate : TQrEvResult;
begin
     ParametrosFijos( ArgList, 2 );
     with Result do
     begin
          Kind := resDouble;
          dblResult := MiRound( ParamTasa( Argument( 0 ) ), ParamInteger( Argument( 1 ) ) );
     end
end;

type
    TZetaIIf = class( TQREvElementFunction )
    private
    public
      function Calculate : TQREvResult; override;
    end;

function TZetaIIf.Calculate : TQrEvResult;// Funci�n IIF( Bool, exp1,exp2 ) //
begin
     ParametrosFijos( ArgList, 3 );
     if ParamBoolean( Argument( 0 ) ) then
          Result := Argument( 1 )
     else
          Result := Argument( 2 );
end;

type
    TZetaParam = class( TQREvElementFunction )
    private
    public
      function Calculate : TQREvResult; override;
    end;

{***(@am): Se cambia el envio del resultado de strResult a StringVal porque en XE5 el tipo de dato
        de strResult es un string[255], es decir que esta limitado a 255. Por lo tanto cuando la
        cadena se pase de ese valor no la mostrara.***}
function TZetaPARAM.Calculate : TQrEvResult;
var
   iParam: Integer;
   uParam: Variant;
   eTipo : eTipoGlobal;
begin
     iParam := ParamInteger( Argument( 0 ) );

     if iParam in [ 1 .. ZetaCommonClasses.K_MAX_PARAM ] then
     begin
          uParam := ZReportConst.GetParametro( ParametrosReporte, iParam, eTipo );
          if uParam <> NULL then
             case eTipo of
                  tgBooleano :
                  begin
                       Result.Kind := resBool;
                       Result.booResult := uParam;
                  end;
                  tgFloat :
                  begin
                       Result.Kind := resDouble;
                       Result.dblResult := uParam;
                  end;
                  tgNumero :
                  begin
                       Result.Kind := resInt;
                       Result.intResult := uParam;
                  end;
                  tgFecha :
                  begin
                       Result.Kind := resDouble;
                       Result.dblResult := uParam;
                  end;
                  tgTexto :
                  begin
                       Result.Kind := resString;
                       Result.{$IFDEF TRESS_DELPHIXE5_UP}StringVal{$ELSE} strResult {$ENDIF} := uParam;
                  end
                  else
                      raise Exception.Create( 'Valor Indefinido' + CR_LF + 'Param( ' + IntToStr( iParam ) + ' )' );
             end
          else
              raise Exception.Create( 'Par�metro No est� Definido' + CR_LF+ 'Param( ' + IntToStr( iParam ) + ' )' );
     end
     else
         raise Exception.Create( 'Par�metro Fuera de Rango ' + CR_LF +  'Param( ' + IntToStr( iParam ) + ' )');
end;


procedure RegistraFuncListadoTotales;
begin
     QRExpr.RegisterQRFunction( TZetaTITULO, 'TITULO', 'TITULO()', 'Grupo Tress Internacional', 'CV' );
end;

procedure RegistraFunciones;
begin
     //FUNCIONES PARA LA IMPRESION DE GRIDS.
     QRExpr.RegisterQRFunction( TGetGlobal,'Global', 'Global()', 'Grupo Tress Internacional', 'CV');
     QRExpr.RegisterQRFunction( TZetaREPORTE, 'REPORTE', 'REPORTE()', 'Grupo Tress Internacional', 'CV' );
     QRExpr.RegisterQRFunction( TZetaFILTRO, 'FILTRO', 'FILTRO()', 'Grupo Tress Internacional', 'CV' );
end;

procedure RegistraFuncionesCliente;
begin
     QRExpr.RegisterQRFunction( TZetaABS, 'ABS', 'ABS()', 'Grupo Tress Internacional', 'CV' );
     QRExpr.RegisterQRFunction( TZetaINT, 'INT', 'INT()', 'Grupo Tress Internacional', 'CV' );
     QRExpr.RegisterQRFunction( TZetaROUND, 'ROUND', 'ROUND()', 'Grupo Tress Internacional', 'CV' );
     QRExpr.RegisterQRFunction( TZetaParam, 'PARAM', 'PARAM()', 'Grupo Tress Internacional', 'CV' );
     QRExpr.RegisterQRFunction( TZetaMAX, 'zMAXIMO', 'zMAXIMO()', 'Grupo Tress Internacional', 'CV' );
     QRExpr.RegisterQRFunction( TZetaMIN, 'zMINIMO', 'zMINIMO()', 'Grupo Tress Internacional', 'CV' );
     QRExpr.RegisterQRFunction( TZetaIIf, 'IF', 'IF()', 'Grupo Tress Internacional', 'CV' );
end;


procedure RegistraFuncionesTotales;
 var i: integer;
begin
     ImprimeGrid := FALSE;
     for i := Low( aResultados ) to High( aResultados ) do
         aResultados[ i ] := 0;

     QRExpr.RegisterQRFunction( TZetaSUMARESULT, 'SUMA_RESUL', 'SUMA_RESULT()', 'Grupo Tress Internacional', 'CV' );
     QRExpr.RegisterQRFunction( TZetaSUMARESULT, 'RESULT', 'RESULT()', 'Grupo Tress Internacional', 'CV' );
     QRExpr.RegisterQRFunction( TZetaSAVERESULT, 'SAVE_RES', 'SAV_RES()', 'Grupo Tress Internacional', 'CV' );
end;

end.
