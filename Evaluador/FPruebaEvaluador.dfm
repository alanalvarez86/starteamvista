object FormaEvaluador: TFormaEvaluador
  Left = 198
  Top = 131
  Width = 645
  Height = 393
  Caption = 'Evaluador de Expresiones'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Resultado: TLabel
    Left = 24
    Top = 68
    Width = 48
    Height = 13
    Caption = 'Resultado'
  end
  object Tipo: TLabel
    Left = 24
    Top = 52
    Width = 21
    Height = 13
    Caption = 'Tipo'
  end
  object Expresion: TEdit
    Left = 24
    Top = 24
    Width = 381
    Height = 21
    TabOrder = 0
    Text = '2 + 2'
  end
  object Evalua: TButton
    Left = 416
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Evalua'
    Default = True
    TabOrder = 1
    OnClick = EvaluaClick
  end
  object EditTransformada: TEdit
    Left = 28
    Top = 94
    Width = 381
    Height = 21
    TabOrder = 2
  end
  object Requeridos: TMemo
    Left = 28
    Top = 118
    Width = 600
    Height = 109
    Lines.Strings = (
      'Requeridos')
    TabOrder = 3
  end
  object GetRequiere: TButton
    Left = 420
    Top = 91
    Width = 75
    Height = 25
    Caption = 'GetRequiere'
    TabOrder = 4
    OnClick = GetRequiereClick
  end
  object DBGrid1: TDBGrid
    Left = 28
    Top = 232
    Width = 600
    Height = 120
    DataSource = DataSource1
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object EvalRegistro: TButton
    Left = 496
    Top = 91
    Width = 75
    Height = 25
    Caption = 'EvalRegistro'
    TabOrder = 6
    OnClick = EvalRegistroClick
  end
  object Reporte: TButton
    Left = 496
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Reporte'
    TabOrder = 7
    OnClick = ReporteClick
  end
  object Globales: TButton
    Left = 576
    Top = 24
    Width = 49
    Height = 25
    Caption = 'Globales'
    TabOrder = 8
    OnClick = GlobalesClick
  end
  object FiltroSQL: TButton
    Left = 332
    Top = 60
    Width = 75
    Height = 25
    Caption = 'FiltroSQL'
    TabOrder = 9
    OnClick = FiltroSQLClick
  end
  object ComboEntidad: TComboBox
    Left = 420
    Top = 60
    Width = 213
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 10
    OnChange = ComboEntidadChange
  end
  object DataSource1: TDataSource
    DataSet = qryTransformada
    Left = 472
    Top = 240
  end
  object Database1: TDatabase
    AliasName = 'Tress Datos'
    DatabaseName = 'dbxTress'
    LoginPrompt = False
    Params.Strings = (
      'USER NAME=SYSDBA'
      'PASSWORD=m')
    SessionName = 'Default'
    Left = 528
    Top = 240
  end
  object qryTransformada: TQuery
    DatabaseName = 'dbxTress'
    Left = 500
    Top = 252
  end
end
