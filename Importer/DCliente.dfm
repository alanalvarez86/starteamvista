inherited dmCliente: TdmCliente
  OldCreateOrder = True
  object cdsTPeriodos: TZetaLookupDataSet
    Tag = 33
    Aggregates = <>
    IndexFieldNames = 'TP_TIPO'
    Params = <>
    AlAdquirirDatos = cdsTPeriodosAlAdquirirDatos
    LookupName = 'Tipos de periodo'
    LookupDescriptionField = 'TP_DESCRIP'
    LookupKeyField = 'TP_TIPO'
    Left = 33
    Top = 138
  end
end
