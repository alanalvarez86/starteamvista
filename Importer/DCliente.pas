unit DCliente;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     DBasicoCliente,
     Login_TLB,
     ZetaClientDataSet,
     ZetaCommonLists,
     ZetaCommonClasses;

type
  TdmCliente = class(TBasicoCliente)
    cdsTPeriodos: TZetaLookupDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsTPeriodosAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }
    procedure LeeTiposNominaDinamicos(const lTodosPeriodos: Boolean = FALSE );
    procedure LeeTiposNominaConfidencialDinamicos;
  public
    { Public declarations }
    function BuscaCompany( const sCodigo: String ): Boolean;
    function Empresa: OleVariant;
    function GetValorActivoStr( const eTipo: TipoEstado ): String; override;
    function GetAuto: OleVariant;
    procedure CargaActivosTodos( Parametros: TZetaParams ); override;
    procedure LeeTiposNomina(Lista: TStrings; const lTodosPeriodos: Boolean = FALSE );//acl
    procedure InitArrayTPeriodo;
  end;

var
  dmCliente: TdmCliente;

implementation

{$R *.DFM}

uses ZetaRegistryCliente,
     ZetaCommonTools;

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     inherited;
     InitClientRegistry;
     SetUsuario( 1 );
     FArregloPeriodo := TStringList.Create;//acl
     FArregloPeriodoConfidencial := TStringList.Create;
     FArregloTipoNomina := TStringList.Create;
     FListaTiposPeriodoConfidencialidad := TStringList.Create;
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil(FArregloPeriodo); //acl
     FreeAndNil(FArregloTipoNomina);  //acl
     FreeAndNil(FArregloPeriodoConfidencial);
     FreeAndNil(FListaTiposPeriodoConfidencialidad);
     ClearClientRegistry;
     inherited;
end;

//(@am): Se crea este metodo para guardar en un alista de objetos (estructura TPeriodo) 'N' cantidad de tipos de periodo.
 procedure TdmCliente.LeeTiposNominaDinamicos(const lTodosPeriodos: Boolean = FALSE );
 var
   sValorLista: String;
   iPos: Integer;
 begin
      try
         FArregloPeriodo.BeginUpdate;
         FArregloTipoNomina.BeginUpdate;

         FArregloPeriodo.Clear;
         FArregloTipoNomina.Clear;

         with cdsTPeriodos do
         begin
              Refrescar;
              First;
              while ( not EOF ) do
              begin
                   if strLleno( FieldByName( 'TP_DESCRIP' ).AsString ) or ( lTodosPeriodos ) then
                   begin
                        //Llena lista de strings
                        sValorLista:= FieldByName( 'TP_DESCRIP' ).AsString;

                        iPos:= Pos ( '/', sValorLista );

                        if ( ( iPos > 0 ) and ( sValorLista[iPos+1] <> '' ) and ( sValorLista[iPos-1] <> '' ) ) then
                           FArregloPeriodo.Add(FieldByName( 'TP_TIPO' ).AsString + '=' + Copy( sValorLista, 1, iPos - 1 ))
                        else
                            FArregloPeriodo.Add(FieldByName( 'TP_TIPO' ).AsString + '=' + sValorLista);

                            FArregloTipoNomina.Add(FieldByName( 'TP_TIPO' ).AsString + '=' + Copy( sValorLista, Pos ( '/', sValorLista ) + 1, Length ( sValorLista )));
                   end;
                   Next;
              end;
         end;
      finally
             FArregloPeriodo.EndUpdate;
             FArregloTipoNomina.EndUpdate;
      end;
 end;

//acl
procedure TdmCliente.InitArrayTPeriodo;
begin
     LeeTiposNominaDinamicos(TRUE);
     // Mejora Confidencialidad
     LeeTiposNominaConfidencialDinamicos;
end;

//(@am): Se crea este metodo para guardar una lista de objetos (estructura TPeriodo) 'N' cantidad de tipos de periodo.
procedure TdmCliente.LeeTiposNominaConfidencialDinamicos;
var
   sConfidencialidad : String;
   bIntersectaConfidencialidad : boolean;
   sValorLista: String;
   iPos: Integer;
begin
     try
        FArregloPeriodoConfidencial.BeginUpdate;
        FListaTiposPeriodoConfidencialidad.Clear;
        FArregloPeriodoConfidencial.Clear;

        with cdsTPeriodos do
        begin
             Refrescar;
             First;
             while ( not EOF ) do
             begin
                  try
                     sConfidencialidad := FieldByName(LookupConfidenField).AsString;
                  except
                        on Error : Exception do
                        sConfidencialidad := VACIO;
                  end;

                  bIntersectaConfidencialidad := ListaIntersectaConfidencialidad(dmCliente.Confidencialidad, sConfidencialidad);

                  //Lista de objetos
                  FListaTiposPeriodoConfidencialidad.Add(FieldByName( 'TP_TIPO' ).AsString + '=' + FieldByName( 'TP_CLAS' ).AsString );

                  //Lista de strings
                  sValorLista:= FieldByName( 'TP_DESCRIP' ).AsString;
                  iPos:= Pos ( '/', sValorLista );
                  if bIntersectaConfidencialidad then
                  begin
                       if ( ( iPos > 0 ) and ( sValorLista[iPos+1] <> '' ) and ( sValorLista[iPos-1] <> '' ) ) then
                          FArregloPeriodoConfidencial.Add(FieldByName( 'TP_TIPO' ).AsString + '=' + Copy( sValorLista, 1, iPos - 1 ))
                       else
                           FArregloPeriodoConfidencial.Add(FieldByName( 'TP_TIPO' ).AsString + '=' + sValorLista);
                  end;
                  Next;
             end;
        end;
     finally
            FArregloPeriodoConfidencial.EndUpdate;
     end;
end;

//acl
procedure TdmCliente.LeeTiposNomina( Lista: TStrings; const lTodosPeriodos: Boolean );
begin
     with Lista do
     begin
          BeginUpdate;
          Clear;
          with cdsTPeriodos do
          begin
               Refrescar;
               First;
               while ( not EOF ) do
               begin
                    if strLleno( FieldByName( 'TP_DESCRIP' ).AsString ) or ( lTodosPeriodos ) then
                       Add( FieldByName( 'TP_TIPO' ).AsString + '=' + FieldByName( 'TP_DESCRIP' ).AsString);
                    Next;
               end;
          end;
          EndUpdate;
     end;
end;

function TdmCliente.BuscaCompany( const sCodigo: String ): Boolean;
begin
     with cdsCompany do
     begin
          Data := Servidor.GetCompanys( 0, Ord( tc3Datos ) );
          Result := Locate( 'CM_CODIGO', sCodigo, [] );
     end;
end;

function TdmCliente.Empresa: OleVariant;
begin
     Result := BuildEmpresa( cdsCompany );
end;

function TdmCliente.GetAuto: OleVariant;
begin
     Result := Servidor.GetAuto;
end;

procedure TdmCliente.CargaActivosTodos(Parametros: TZetaParams);
begin
end;

function TdmCliente.GetValorActivoStr(const eTipo: TipoEstado): String;
begin
end;

procedure TdmCliente.cdsTPeriodosAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     cdsTPeriodos.Data := Servidor.GetTipoPeriodo( Empresa );
end;

end.
