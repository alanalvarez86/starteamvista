inherited FormaSuscribeUsuario: TFormaSuscribeUsuario
  Caption = 'Suscripción a Reportes via e-mail'
  ClientHeight = 348
  ClientWidth = 377
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 312
    Width = 377
    inherited OK: TBitBtn
      Left = 209
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 294
      Cancel = False
      Kind = bkCustom
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 377
    Height = 65
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 7
      Width = 39
      Height = 13
      Caption = 'Usuario:'
    end
    object DBText1: TDBText
      Left = 64
      Top = 7
      Width = 25
      Height = 17
      DataField = 'US_CODIGO'
      DataSource = dsUsuario
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText2: TDBText
      Left = 90
      Top = 7
      Width = 244
      Height = 17
      DataField = 'US_NOMBRE'
      DataSource = dsUsuario
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnAgregar: TBitBtn
      Left = 15
      Top = 28
      Width = 110
      Height = 29
      Caption = 'Incluir Reporte'
      TabOrder = 0
      OnClick = btnAgregarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
        333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
        0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
        07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
        07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
        0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
        33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
        B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
        3BB33773333773333773B333333B3333333B7333333733333337}
      NumGlyphs = 2
    end
    object btnBorrar: TBitBtn
      Left = 132
      Top = 28
      Width = 110
      Height = 29
      Caption = 'Excluir Reporte'
      TabOrder = 1
      OnClick = btnBorrarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
        305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
        005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
        B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
        B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
        B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
        B0557777FF577777F7F500000E055550805577777F7555575755500000555555
        05555777775555557F5555000555555505555577755555557555}
      NumGlyphs = 2
    end
    object btnModificar: TBitBtn
      Left = 249
      Top = 28
      Width = 110
      Height = 29
      Caption = 'Modificar'
      TabOrder = 2
      OnClick = btnModificarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
    end
  end
  object ZetaDBGrid1: TZetaDBGrid
    Left = 0
    Top = 65
    Width = 377
    Height = 247
    Align = alClient
    DataSource = dsSuscrip
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'RE_NOMBRE'
        Title.Caption = 'Reporte a recibir'
        Width = 221
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SU_FRECUEN'
        Title.Caption = 'Frecuencia'
        Width = 111
        Visible = True
      end>
  end
  object dsUsuario: TDataSource
    DataSet = dmSistema.cdsUsuarios
    Left = 328
    Top = 4
  end
  object dsSuscrip: TDataSource
    Left = 330
    Top = 36
  end
end
