{$HINTS OFF}
unit FSistSolicitudEnviosLog_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ExtCtrls, StdCtrls, Buttons,
     Grids, DBGrids, DBCtrls, Db, ComCtrls, Variants,
     ZetaCommonLists,
     ZetaDBTextBox,
     ZetaDBGrid, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters,
  Menus, cxButtons, cxNavigator, cxDBNavigator, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  ZetaCXGrid, cxContainer, cxTextEdit, cxMemo, cxDBEdit;

type
  TSistSolicitudEnviosLog_DevEx = class(TForm)
    Datasource: TDataSource;
    dsLog: TDataSource;
    PanelInferior_DevEx: TPanel;
    DevEx_cxDBNavigatorEdicion: TcxDBNavigator;
    Salir_DevEx: TcxButton;
    PanelSuperior: TPanel;
    CAL_INICIO: TZetaDBTextBox;
    RE_CODIGO: TZetaDBTextBox;
    CA_NOMBRE: TZetaDBTextBox;
    CAL_FIN: TZetaDBTextBox;
    CAL_ESTATUS: TZetaDBTextBox;
    CAL_FECHA: TZetaDBTextBox;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    ZetaCXGrid: TZetaCXGrid;
    ZetaCXGridDBTableView: TcxGridDBTableView;
    BI_FECHA: TcxGridDBColumn;
    BI_HORA: TcxGridDBColumn;
    BI_TIPO: TcxGridDBColumn;
    BI_TEXTO: TcxGridDBColumn;
    ZetaCXGridLevel: TcxGridLevel;
    CAL_MENSAJ: TDBMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure Salir_DevExClick(Sender: TObject);
    procedure ZetaCXGridDBTableViewDblClick(Sender: TObject);
    procedure ZetaCXGridDBTableViewDataControllerFilterGetValueList( Sender: TcxFilterCriteria; AItemIndex: Integer; AValueList: TcxDataFilterValueList);
    procedure ZetaCXGridDBTableViewKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    FProceso: Procesos;
    procedure Connect;
    procedure Disconnect;
    procedure ReconnectGrid;
    procedure RefreshGrid;
    procedure ApplyMinWidth;
  public
    { Public declarations }
  end;

var
  SistSolicitudEnviosLog_DevEx: TSistSolicitudEnviosLog_DevEx;

    procedure ShowLog;

implementation

uses ZAccesosMgr,
     ZAccesosTress,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     ZGridModeTools,
     ZetaMessages,
     ZetaDialogo,
     ZetaClientDataSet,
     ZImprimeGrid, //(@am): Se cambia FBaseReportes por ZImprimeGrid pues ahora toda la logica de exportacion esta ahi.
     DSistema;

{$R *.DFM}

procedure ShowLog;
begin
     with TSistSolicitudEnviosLog_DevEx.Create( Application ) do
     begin
          try
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

{ ************ TShowLog ************ }

procedure TSistSolicitudEnviosLog_DevEx.FormCreate(Sender: TObject);
begin
     HelpContext := H00005_Bitacora_procesos;    //PENDIENTE
     ZetaCXGridDBTableView.DataController.DataModeController.GridMode:= True;
     ZetaCXGridDBTableView.DataController.Filter.AutoDataSetFilter := True;
end;

procedure TSistSolicitudEnviosLog_DevEx.FormShow(Sender: TObject);
begin
     Connect;
     ZetaCXGridDBTableView.OptionsCustomize.ColumnGrouping := False;
     ZetaCXGridDBTableView.OptionsView.GroupByBox := False;
     ZetaCXGridDBTableView.FilterBox.Visible := fvNever;
     ZetaCXGridDBTableView.FilterBox.CustomizeDialog := False;
     ApplyMinWidth;
     ZetaCXGridDBTableView.ApplyBestFit();  
end;

procedure TSistSolicitudEnviosLog_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     ZGridModeTools.LimpiaCacheGrid(ZetaCXGridDBTableView);
     ZetaCXGridDBTableView.DataController.Filter.Root.Clear;
     Disconnect;
end;

procedure TSistSolicitudEnviosLog_DevEx.Connect;
var i: integer;
    FParametros: TZetaParams;
begin
     FParametros := TZetaParams.Create;
     try
        with dmSistema do
        begin
             cdsBitacoraReportes.Conectar;
             Datasource.Dataset := cdsSistSolicitudEnvios;

             with FParametros do
             begin
                  AddInteger( 'Folio', cdsSistSolicitudEnvios.FieldByName('CAL_FOLIO').AsInteger );
             end;

             dmSistema.RefrescarBitacoraReportes( FParametros );
             dsLog.Dataset := cdsBitacoraReportes;
        end;
     finally
            FParametros.Free;
     end;
end;

procedure TSistSolicitudEnviosLog_DevEx.Disconnect;
begin
     Datasource.Dataset := nil;
end;

procedure TSistSolicitudEnviosLog_DevEx.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if Key = Chr( VK_ESCAPE ) then
     begin
          Key := #0;
          Close;
     end;
end;

procedure TSistSolicitudEnviosLog_DevEx.US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
//     dmConsultas.UsuarioGetText( Sender, Text, DisplayText );//PEndiente
end;

procedure TSistSolicitudEnviosLog_DevEx.Salir_DevExClick(Sender: TObject);
begin
     Close;
end;

procedure TSistSolicitudEnviosLog_DevEx.ZetaCXGridDBTableViewDblClick(Sender: TObject);
begin
     TZetaClientDataSet(dsLog.Dataset).Modificar;
end;

procedure TSistSolicitudEnviosLog_DevEx.ZetaCXGridDBTableViewDataControllerFilterGetValueList( Sender: TcxFilterCriteria; AItemIndex: Integer; AValueList: TcxDataFilterValueList);
var
  AIndex: Integer;
begin
     inherited;
     ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if ZetaCXGridDBTableView.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( ZetaCXGridDBTableView, AItemIndex, AValueList );
end;

procedure TSistSolicitudEnviosLog_DevEx.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =35;
begin
     with  ZetaCXGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;
end;

procedure TSistSolicitudEnviosLog_DevEx.ZetaCXGridDBTableViewKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     if Key = VK_RETURN then
     begin
           TZetaClientDataSet(dsLog.Dataset).Modificar;
     end;
end;

procedure TSistSolicitudEnviosLog_DevEx.ReconnectGrid;
begin
     inherited;
     ZGridModeTools.LimpiaCacheGrid( ZetaCXGridDBTableView );
end;

procedure TSistSolicitudEnviosLog_DevEx.RefreshGrid;
begin
     inherited;
     ZGridModeTools.LimpiaCacheGrid( ZetaCXGridDBTableView );
end;

end.
