unit FSistTablerosRoles_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db,
  ExtCtrls, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, System.Actions, cxButtons, ZetaKeyCombo,
  Vcl.Mask, ZetaFecha, cxContainer, cxTextEdit, ZetaKeyLookup_DevEx, cxMaskEdit,
  cxDropDownEdit, Vcl.DBCtrls, cxDBEdit;

type
  TSistTablerosRoles_DevEx = class(TBaseGridLectura_DevEx)
    PanelParam: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    Roles: TZetaKeyLookup_DevEx;
    Label2: TLabel;
    Label4: TLabel;
    fBuscar: TcxTextEdit;
    CM_CODIGO: TcxGridDBColumn;
    CM_NOMBRE: TcxGridDBColumn;
    TableroNombre: TcxGridDBColumn;
    RO_NOMBRE: TcxGridDBColumn;
    RO_CODIGO: TcxGridDBColumn;
    Tablero: TZetaKeyCombo;
    Empresa: TZetaKeyCombo;
    procedure FormShow(Sender: TObject);
    procedure EmpresaChange(Sender: TObject);
    procedure TableroChange(Sender: TObject);
    procedure RolesValidKey(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure fBuscarPropertiesChange(Sender: TObject);
    procedure PanelParamEnter(Sender: TObject);
  private
    { Private declarations }
    procedure ConfigAgrupamiento;
    procedure LlenarInformacion;
    procedure Filtrar;
  protected
    { Protected declarations }
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Connect; override;
    procedure Refresh; override;
    function PuedeModificar(var sMensaje: String): Boolean;override;
  public
    { Public declarations }
  end;

var
  SistTablerosRoles_DevEx: TSistTablerosRoles_DevEx;

implementation

uses dCliente, dConsultas, FEditSistTablerosRoles_DevEx, dSistema, ZetaCommonClasses, ZFiltroSQLTools;

{$R *.DFM}

procedure TSistTablerosRoles_DevEx.Connect;
begin
     inherited;
     with dmConsultas do
     begin
          cdsTableroRolesInformacion.Conectar;
          DataSource.DataSet:= cdsTableroRolesInformacion;
     end;

     with dmSistema do
     begin
          Roles.LookupDataset := cdsRoles;
          cdsRoles.Conectar;
     end;
end;

procedure TSistTablerosRoles_DevEx.EmpresaChange(Sender: TObject);
begin
     inherited;
     Filtrar;
end;

procedure TSistTablerosRoles_DevEx.fBuscarPropertiesChange(Sender: TObject);
begin
     inherited;
     Filtrar;
end;

procedure TSistTablerosRoles_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H394004_PantallaInicioConfiguracion_TRESS;
     with dmConsultas do
     begin
          TableroIndicadores := TSTringList.Create;
          TableroEmpresa := TSTringList.Create;
     end;
end;

procedure TSistTablerosRoles_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     dmConsultas.TableroEmpresa.Free;
     dmConsultas.TableroIndicadores.Free;
end;

procedure TSistTablerosRoles_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
     inherited;
     ConfigAgrupamiento;
     LlenarInformacion;
     Filtrar;
end;

procedure TSistTablerosRoles_DevEx.Agregar;
begin
     if EditSistTablerosRoles_DevEx = NIL then
        EditSistTablerosRoles_DevEx := TEditSistTablerosRoles_DevEx.Create( Self );

     try
     begin
          EditSistTablerosRoles_DevEx.iEmpresa := Empresa.ItemIndex;
          EditSistTablerosRoles_DevEx.iTablero := Tablero.ItemIndex;
          EditSistTablerosRoles_DevEx.sRol := Roles.Llave;
          EditSistTablerosRoles_DevEx.ShowModal;

          if EditSistTablerosRoles_DevEx.ModalResult = mrOk then
             Filtrar;
     end
     finally
            FreeAndNil(EditSistTablerosRoles_DevEx);
     end;
end;

procedure TSistTablerosRoles_DevEx.PanelParamEnter(Sender: TObject);
begin
     inherited;
     Empresa.SetFocus;
end;

function TSistTablerosRoles_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje:= 'No se pueden modificar tableros por roles de usuario';
end;

procedure TSistTablerosRoles_DevEx.Borrar;
begin
     inherited;
     dmConsultas.cdsTableroRolesInformacion.Borrar;
     Refresh;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TSistTablerosRoles_DevEx.Refresh;
begin
     inherited;
     Filtrar;
end;

procedure TSistTablerosRoles_DevEx.RolesValidKey(Sender: TObject);
begin
     inherited;
     Filtrar;
end;

procedure TSistTablerosRoles_DevEx.TableroChange(Sender: TObject);
begin
     inherited;
     Filtrar;
end;

procedure TSistTablerosRoles_DevEx.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE;
     CM_CODIGO.Options.Grouping:= FALSE;
     CM_NOMBRE.Options.Grouping:= FALSE;
     TableroNombre.Options.Grouping:= FALSE;
     RO_CODIGO.Options.Grouping:= FALSE;
     RO_NOMBRE.Options.Grouping:= FALSE;
end;

procedure TSistTablerosRoles_DevEx.LlenarInformacion;
var iEmpresa : Integer;
begin
     with dmConsultas do
     begin
          TableroGlobal := True;
          cdsTREmpresa.Refrescar;
          cdsTRTablero.Refrescar;

          LlenarLista(TableroEmpresa, cdsTREmpresa);
          Empresa.Lista := TableroEmpresa;
          LlenarLista(TableroIndicadores, cdsTRTablero);
          Tablero.Lista := TableroIndicadores;
     end;

     dmConsultas.TableroEmpresa.Find(dmCliente.Compania, iEmpresa);
     Tablero.ItemIndex := 0;
     Empresa.ItemIndex := iEmpresa;
end;

procedure TSistTablerosRoles_DevEx.Filtrar;
var sEmpresa : String;
begin
     with dmConsultas do
     begin
          if TableroEmpresa.Names[Empresa.ItemIndex] = '0' then
             sEmpresa := VACIO
          else
              sEmpresa := TableroEmpresa.Names[Empresa.ItemIndex];

          GetRolTablerosConsulta(1, StrToInt(TableroIndicadores.Names[Tablero.ItemIndex]),
          sEmpresa, FiltrarTextoSQL(fBuscar.Text), Roles.Llave );
     end;

     ZetaDBGridDBTableView.ApplyBestFit();
end;


end.
