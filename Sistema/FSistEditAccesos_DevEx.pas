unit FSistEditAccesos_DevEx;

interface

{$INCLUDE DEFINES.INC}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     StdCtrls, ComCtrls, ExtCtrls, Dialogs, ImgList, Buttons,
     ZetaCommonClasses,
     DBaseSistema,
     ZetaDBTextBox, ZBaseEditAccesos_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxContainer, cxEdit,
  cxTreeView, cxButtons, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC,
  dxBarBuiltInMenu;

type
  TSistEditAccesos_DevEx = class(TZetaEditAccesos_DevEx)
    tsReporteador: TcxTabSheet;
    Reporteador: TcxPageControl;
    tsClasificaciones: TcxTabSheet;
    tsEntidades: TcxTabSheet;
    ArbolClasifi: TcxTreeView;
    ArbolEntidades: TcxTreeView;
    procedure FormCreate(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FNodoAdicionales, FNodoAdicionalesLast: TTreeNode;
    FConectado: Boolean;
{$ifdef RDD}
    procedure ApagaPrendePadres;
{$endif}
  protected
    { Protected declarations }
    procedure ArbolDefinir; override;
{$ifdef RDD}
    procedure ArbolDefinirRDD;
{$endif}
    procedure AgregaDerechosCamposAdicionales;
    procedure Cargar;override;
    function GetArbol: TcxTreeView;override;
    function EsDerechoEspecial(const iIndex: Integer): Boolean; override;
    function GetTipoDerecho(Nodo: TTreeNode): eDerecho; override;
    function NodoEspecial( const iIndex: Integer ): eTipoNodo; override;
    function TextoEspecial(const sText: String): Boolean; override;
    function GetImageIndex: integer;override;
    function GetImageIndexLimit: integer;override;
    procedure RefrescaArbolAdicionales( Metodo: TBuscaDerecho );
    procedure CopiaDerechos;override;
    procedure CambiosAdicionales; override;
                                               
  public
    { Public declarations }
    end;

var
  SistEditAccesos_DevEx: TSistEditAccesos_DevEx;

implementation

uses ZAccesosTress,
     ZGlobalTress,
     ZToolsPe,
     FEscogeGrupoEmpresa_DevEx,
     FAutoClasses,
     DSistema,
     DGlobal,
     DCliente,
     DDiccionario;


{$define QUINCENALES}

const
     D_TEXT_RESTRINGIR_MOVS_SIST = 'Restringir Consulta Movs. Sistema';
     D_TEXT_BORRA_SISTEMA = 'Borrar/Modif. Movs. Sistema';
     D_TEXT_BANCA = 'Banca Electr�nica';
     D_TEXT_CHECADAS_CASETA = 'Checadas Caseta';     
     D_TEXT_CANCELAR_PROCESOS = 'Cancelar Procesos';
     D_TEXT_NIVEL0 = 'Confidencialidad';
     D_TEXT_CONTEO = 'Configurar';
     D_TEXT_IMPORTAR_EMPLEADOS = 'Importar Empleados';
     D_TEXT_REGRESO_PRIMERA_HORA = 'Cambiar Area Con Regreso a Primera Hora';
     D_TEXT_REGRESO_FECHA_HORA = 'Cambiar Area Con Regreso a Fecha y Hora Especifica';
     D_TEXT_MED_DATOS_CONTRAT = 'Datos de Contrataci�n';
     D_TEXT_EDITAR_OTROS_INSP = 'Editar C�dulas de Otros Inspectores';
     D_TEXT_INFONAVIT = 'Infonavit';
     D_TEXT_APROBAR_AUTORIZACION = 'Aprobar';
     D_TEXT_MODIFICAR_TARJETAS_ANTERIORES = 'Modificar Tarjetas Anteriores';
     D_TEXT_MODIFICAR_FECHA_LIMITE = 'Modificar Fecha L�mite';
     { C�dulas de Labor/Supervisores }
     D_TEXT_LISTA_EMPLEADOS = 'Editar Lista de Empleados';
     D_TEXT_LABOR_CEDULAS_PRODUCCION = 'Capturar C�dulas de Producci�n';
     D_TEXT_LABOR_CEDULAS_TMUERTO = 'Capturar C�dulas de Tiempo Muerto';
     D_TEXT_LABOR_CEDULAS_ESPECIALES = 'Capturar C�dulas de Horas Especiales';
     D_TEXT_LABOR_CEDULAS_GENERALES = 'Capturar Datos Generales de la C�dula';
     D_TEXT_LABOR_CEDULAS_MODULADORES = 'Capturar Moduladores de la C�dula';
     { Vacaciones en Supervisores }
     D_TEXT_SUPER_VACACIONES_PAGADAS = 'Registrar Pago De Vacaciones';
     D_TEXT_SUPER_VACACIONES_PASADAS = 'Registrar Vacaciones Pasadas';
     D_TEXT_SUPER_EXCEDER_GOZADAS    = 'Exceder Saldos De Vacaciones Gozadas';
     D_TEXT_SUPER_EXCEDER_PAGADAS    = 'Exceder Saldos De Vacaciones Pagadas';
     D_TEXT_SUPER_EXCEDER_PRIMA_PAGO = 'Exceder Saldos De Prima Vacacional';
     { Niveles Clasificaciones Temporales }
     D_TEXT_NIVEL_PUESTO = 'Puesto';
     D_TEXT_NIVEL_CLASIFICACION = 'Clasificaci�n';
     D_TEXT_NIVEL_TURNO = 'Turno';
     D_TEXT_NIVEL_1 = 'Nivel 1';
     D_TEXT_NIVEL_2 = 'Nivel 2';
     D_TEXT_NIVEL_3 = 'Nivel 3';
     D_TEXT_NIVEL_4 = 'Nivel 4';
     D_TEXT_NIVEL_5 = 'Nivel 5';
     D_TEXT_NIVEL_6 = 'Nivel 6';
     D_TEXT_NIVEL_7 = 'Nivel 7';
     D_TEXT_NIVEL_8 = 'Nivel 8';
     D_TEXT_NIVEL_9 = 'Nivel 9';
     {$ifdef ACS}
     D_TEXT_NIVEL_10 = 'Nivel 10';
     D_TEXT_NIVEL_11 = 'Nivel 11';
     D_TEXT_NIVEL_12 = 'Nivel 12';
     {$endif}
     D_RESERVADO_SISTEMA = 'Reservado por el sistema';
     D_TEXT_AUTORIZAR = 'Autorizar';
     D_TEXT_ESPEC_PER_PAGO = 'Especificar periodo de pago';
     D_TEXT_PAGAR_AFECTADAS = 'Pagar en N�minas Afectadas';
     D_TEXT_TARJ_CTA_DESP = 'Tarjeta de Despensa';
     D_TEXT_TARJ_CTA_GAS = 'Tarjeta de Gasolina';
	 D_TEXT_ASIGNAR = 'Asignar M�quina';
     {Derecho que se usa en ReporteadorCFG\Diccionario de Datos\Edicion}
     D_TEXT_TABLAS_CON_CONFIDENCIALIDAD= 'Modificar Confidencialidad sobre Empleados';
     //************* Derechos de Acceso Administrador Especial *********************//
     D_ASIGNA_DERECHOS_OTROS_GRUPOS = 'Asignar Derechos a Otros Grupos';
     D_ASIGNA_DERECHOS_GRUPO_PROPIO = 'Asignar Derechos a SU Grupo';
     D_MODIFICAR_USUARIO_PROPIO = 'Modificar SU Usuario';
     D_TEXT_REINICIA_NIP ='Borrar NIP (Mis Datos)';
     {$ifndef DOS_CAPAS}
     D_TEXT_NUM_BIOMETRICOS = 'Identificador Biom�trico'; // SYNERGY
     D_TEXT_IMPORTA_TERMINAL = 'Importar Terminales'; // SYNERGY
     D_TEXT_REINICIA_TERMINAL = 'Reiniciar Terminales'; // SYNERGY
     D_TEXT_ENROLAR_HUELLAS = 'Enrolar Huellas (Empleado)'; // BIOMETRICO
     D_TEXT_BORRAR_HUELLAS = 'Borrar Huellas (Empleado)'; // BIOMETRICO
     D_TEXT_NUM_BIOMETRICOS_INV = 'N�mero Biom�trico (Invitador)'; // SYNERGY
     D_TEXT_ENROLAR_HUELLAS_INV = 'Enrolar Huellas (Invitador)'; // BIOMETRICO
     D_TEXT_BORRAR_HUELLAS_INV = 'Borrar Huellas (Invitador)'; // BIOMETRICO
     D_TEXT_BORRAR_FACIAL = 'Borrar Facial';
     {$endif}
     D_EXPORTAR_TABLAS_XML = 'Exportar Tablas XML';
     D_IMPORTAR_TABLAS_XML = 'Importar Tablas XML';
     D_IMPORTAR_TABLAS_CSV = 'Importar Tablas CSV';
     D_CAT_IMPORTAR_CONCEPTOS = 'Importar conceptos';
     D_TEXT_INSCRIBIR_ROLES = 'Inscribir Roles';
     D_TEXT_INSCRIBIR_USUARIOS = 'Inscribir Usuarios';
     D_TEXT_INSCRIBIR_ROLES_ENVIOS = 'Inscribir Roles Envios';
     D_TEXT_INSCRIBIR_USUARIOS_ENVIOS = 'Inscribir Usuarios Envios';
{$R *.DFM}

{ TSistEditAccesos }

procedure TSistEditAccesos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     //self.ArbolConstruir;
     {$ifndef RDD}
     tsTress.TabVisible := False;
     tsReporteador.TabVisible := False;
     {$endif}
     PageControl.ActivePage := tsTress;

end;

function TSistEditAccesos_DevEx.EsDerechoEspecial(const iIndex: Integer): Boolean;
begin
     Result := FALSE;
     if ( ArbolSeleccionado = adTress ) then
     begin
          case iIndex of
               D_SUPER_AJUSTAR_ASISTENCIA_ESPECIAL: Result := True;
               D_SUPER_TIPO_KARDEX: Result := True;
               D_SUPER_TIPO_PERMISO: Result := True;
               D_EMP_CLASE_PERMISO: Result := True;
               D_ASIS_PROC_CALC_PRENOMINA: Result := True;
               D_NOM_PROC_CALCULAR: Result := True;
               D_NOM_PROC_IMP_MOVIMIEN: Result := True;
               D_EMP_REG_ALTA: Result := True;
               D_CONS_CONTEO_PERSONAL: Result := True;
               D_EMP_NOM_SIM_FINIQUITOS:Result := True;
               //****************** Plan Carrera Web *******************//
               D_CARRERA_CONSULTAS_WEB: Result := True;
               D_CARRERA_PUESTOS_WEB: Result := True;
               D_CARRERA_CATALOGOS_WEB: Result := True;
               D_CARRERA_DETALLE_CATALOGOS: Result := True;
               D_CARRERA_ANALITICOS_WEB: Result := True;
               D_CARRERA_MIS_COLABORADORES: Result := True;
               //***************** Evaluacion *************************//
               D_CRITERIOS_EVAL: Result := True;
               D_ESCALAS: Result := True;
               D_ENCUESTAS: Result := True;
               D_CONS_PLANVACACION : Result := True;
               D_CAT_VALUACIONES : Result := True;
               D_SUPER_GRUPOS: Result:= True;
               D_SUPER_AUTORIZACION_COLECTIVA : Result:= True;
               {$ifdef RDD}
               D_CONS_REPORTES  : Result := True;
               {$endif}
               D_PAGOS_FONACOT_TOT : Result:= True;
               D_EMP_NOM_PRESTAMOS : Result := True;
               D_EMP_SIM_FINIQUITOS_EMPLEADO : Result := True;
               D_EMP_SIM_FINIQUITOS_TOTALES  : Result := True;
               D_ASIS_PROC_RECALC_TARJETA    : Result := True;
			   {$ifdef COMMSCOPE}
			   D_SUPER_AUTORIZAR_EVALUACION  : Result := True;
               {$endif}
               D_SUPER_AUTORIZAR_PRENOMINA : Result := True;
               D_ASIS_PROC_AUTORIZ_PRENOM : Result := True;
               D_EMP_SEGUROS_GASTOS_MEDICOS : Result := True;
               D_EMP_EXP_VACA : Result := True;
               // D_EMP_DATOS_OTROS_ASISTENCIA : Result := True;
               D_SIST_LST_DISPOSITIVOS: Result := True;
               D_SIST_CALENDARIO_REPORTES : Result := True;
          else
              Result := False;
          end;
     end;
end;

function TSistEditAccesos_DevEx.GetTipoDerecho(Nodo: TTreeNode): eDerecho;
begin
     Result := edConsulta;
     case ArbolSeleccionado of
          adTress:
          begin
               with Nodo do
               begin
                    if ( Text = D_TEXT_NIVEL_PUESTO )then
                       Result := edConsulta
                    else
                        if ( Text = D_TEXT_NIVEL_CLASIFICACION )or
                           ( Text = D_TEXT_ASIGNAR )  then
                           Result := edAlta
                        else
                            if ( Text = D_TEXT_CANCELAR_PROCESOS ) or
                               ( Text = D_TEXT_NIVEL_TURNO )  then
                               Result := edBaja
                            else
                                if ( Text = D_TEXT_NIVEL_1 ) then
                                   Result := edModificacion
                                else
                                    if ( Text = D_TEXT_NIVEL_2 ) then
                                       Result := edImpresion
                                    else
                                        if ( Text = D_TEXT_BORRA_SISTEMA ) or
                                           ( Text = D_TEXT_LISTA_EMPLEADOS ) or
                                           ( Text = D_TEXT_IMPORTAR_EMPLEADOS ) or
                                           ( Text = D_TEXT_MED_DATOS_CONTRAT ) or
                                           ( Text = D_TEXT_EDITAR_OTROS_INSP) or
                                           ( Text = D_TEXT_INFONAVIT ) or
                                           ( Text = D_TEXT_APROBAR_AUTORIZACION ) or
                                           ( Text = D_TEXT_MODIFICAR_FECHA_LIMITE ) or
                                           ( Text = D_TEXT_SUPER_VACACIONES_PAGADAS ) or
                                           ( Text = D_TEXT_NIVEL_3 ) or
                                           ( Text = D_ASIGNA_DERECHOS_OTROS_GRUPOS) or
                                           ( Text = D_MODIFICAR_USUARIO_PROPIO)or
                                           ( Text = D_TEXT_AUTORIZAR ) or
                                           ( Text = D_RESERVADO_SISTEMA ) or
                                           ( Text = D_EXPORTAR_TABLAS_XML ) or
                                           ( Text = D_CAT_IMPORTAR_CONCEPTOS ) or
                                           ( Text = D_TEXT_TABLAS_CON_CONFIDENCIALIDAD )
                                           {$ifndef DOS_CAPAS}
                                           or ( Text = D_TEXT_IMPORTA_TERMINAL ) // SYNERGY
                                           {$endif}
                                           then
                                            Result := edBorraSistKardex
                                        else
                                            if ( Text = D_TEXT_RESTRINGIR_MOVS_SIST ) or
                                               ( Text = D_TEXT_BANCA ) or
                                               ( Text = D_TEXT_REGRESO_PRIMERA_HORA ) or
                                               ( Text = D_TEXT_LABOR_CEDULAS_PRODUCCION ) or
                                               ( Text = D_TEXT_MODIFICAR_TARJETAS_ANTERIORES ) or
                                               ( Text = D_TEXT_SUPER_VACACIONES_PASADAS ) or
                                               ( Text = D_TEXT_NIVEL_4 ) or
                                               ( Text = D_TEXT_ESPEC_PER_PAGO ) or
                                               ( Text = D_IMPORTAR_TABLAS_XML ) or
                                               ( Text = D_ASIGNA_DERECHOS_GRUPO_PROPIO) or
                                               ( Text = D_TEXT_INSCRIBIR_ROLES_ENVIOS )
                                               {$ifndef DOS_CAPAS}
                                               or ( Text = D_TEXT_REINICIA_TERMINAL ) // SYNERGY
                                               {$endif}
                                               then

                                               Result := edBanca
                                            else
                                                if ( Text = D_TEXT_NIVEL0 ) or
                                                   ( Text = D_TEXT_REGRESO_FECHA_HORA ) or
                                                   ( Text = D_TEXT_LABOR_CEDULAS_TMUERTO ) or
                                                   ( Text = D_TEXT_SUPER_EXCEDER_GOZADAS ) or
                                                   ( Text = D_TEXT_PAGAR_AFECTADAS ) or
                                                   ( Text = D_IMPORTAR_TABLAS_CSV ) or
                                                   ( Text = D_TEXT_NIVEL_5 ) or
                                                   ( Text = D_TEXT_INSCRIBIR_USUARIOS_ENVIOS  ) then
                                                   Result := edNivel0
                                                else
                                                    if ( Text = D_TEXT_CONTEO ) or
                                                       ( Text = D_TEXT_LABOR_CEDULAS_ESPECIALES ) or
                                                       ( Text = D_TEXT_SUPER_EXCEDER_PAGADAS ) or
                                                       ( Text = D_TEXT_NIVEL_6 ) or
                                                       ( Text =  D_TEXT_TARJ_CTA_GAS )then

                                                       Result := edConfigurar
                                                    else
                                                        if ( Text = D_TEXT_LABOR_CEDULAS_GENERALES ) or
                                                           ( Text = D_TEXT_SUPER_EXCEDER_PRIMA_PAGO ) or
                                                           ( Text = D_TEXT_NIVEL_7 ) or
                                                           ( Text = D_TEXT_TARJ_CTA_DESP ) then

                                                           Result := edAdicional9
                                                        else
                                                            if ( Text = D_TEXT_LABOR_CEDULAS_MODULADORES ) or
                                                               ( Text = D_TEXT_NIVEL_8 ) or
                                                               ( Text = D_TEXT_REINICIA_NIP ) then
                                                               Result := edAdicional10
                                                            else
                                                                if ( Text = D_TEXT_NIVEL_9 )
                                                                   {$ifndef DOS_CAPAS}
                                                                   or ( Text = D_TEXT_NUM_BIOMETRICOS ) // SYNERGY
                                                                   or ( Text = D_TEXT_NUM_BIOMETRICOS_INV ) // BIOMETRICO
                                                                   {$endif}
                                                                   then
                                                                   Result := edAdicional11
                                                                else
                                                                    {$ifndef DOS_CAPAS}
                                                                    if ( Text = D_TEXT_ENROLAR_HUELLAS ) // BIOMETRICO
                                                                       or ( Text = D_TEXT_ENROLAR_HUELLAS_INV ) // BIOMETRICO
                                                                       {$ifdef ACS}
                                                                       or ( Text = D_TEXT_NIVEL_10 )
                                                                       {$endif} then
                                                                       Result := edAdicional12
                                                                    else
                                                                        if ( Text = D_TEXT_BORRAR_HUELLAS ) // BIOMETRICO
                                                                           or ( Text = D_TEXT_BORRAR_HUELLAS_INV ) // BIOMETRICO
                                                                           {$ifdef ACS}
                                                                           or ( Text = D_TEXT_NIVEL_11 )
                                                                           {$endif} then
                                                                           Result := edAdicional13
                                                                        else
                                                                            if ( Text = D_TEXT_BORRAR_FACIAL )
                                                                            {$ifdef ACS}
                                                                               or ( Text = D_TEXT_NIVEL_12 )
                                                                            {$endif} then
                                                                               Result := edAdicional14
                                                                            else
                                                                    {$endif}

                                                                    Result := inherited GetTipoDerecho( Nodo );
               end;
          end;
          adClasifi: Result := inherited GetTipoDerecho( Nodo );
          adEntidades: Result := edConsulta;
          adAdicionales: Result := inherited GetTipoDerecho( Nodo );
     end
end;

function TSistEditAccesos_DevEx.NodoEspecial(const iIndex: Integer): eTipoNodo;
begin
     {AV CR #1839 No aplican Nodos Especiales para Arbol de RDD   }
     {$ifdef RDD}
     if ( ArbolSeleccionado in [adClasifi,adEntidades])  then
     begin
          Result := inherited NodoEspecial( iIndex );
     end
     else
     {$endif}
     case iIndex of
          D_CONTEO_CONFIGURACION: Result := tpEspecial; { Presupuesto de Personal }
          D_SUPER_AJUSTAR_ASISTENCIA_ESPECIAL: Result := tpEspecial;
          D_SUPER_TIPO_KARDEX: Result := tpEspecial;
          D_SUPER_VER_BAJAS: Result := tpEspecial; { Supervisores \ Ver Bajas }
          D_SUPER_VER_COLABORA: Result := tpEspecial; { Supervisores \ Ver Colabora }
          D_SUPER_TIPO_PERMISO: Result := tpEspecial; { Supervisores \ Tipo de Permiso }
          D_SUPER_ACTIVA_CACHE: Result := tpEspecial; { Supervisores \ Archivos Temporales }
          D_EMP_CLASE_PERMISO: Result := tpEspecial; { Servicio Medico \ Clases de Permisos =ConGoce y Sin Goce= }
          D_ASIS_PROC_CALC_PRENOMINA: Result := tpEspecial; { Asistencia\Procesos\Calcular Pre-n�mina }
          D_NOM_PROC_CALCULAR: Result := tpEspecial; { N�mina\Procesos\Calcular N�mina }
          D_NOM_PROC_IMP_MOVIMIEN: Result := tpEspecial; { N�mina\Procesos\Importar Movimientos }
          D_SIST_PRENDE_USUARIOS: Result := tpEspecial; { Sistema\Usuarios\Activa Todos }
          D_SIST_APAGA_USUARIOS: Result := tpEspecial; { Sistema\Usuarios\Suspende Todos }
          D_SIST_ASIGNAR_SUPER: Result := tpEspecial; { Sistema\Usuarios\Asignar Supervisores }
          D_SIST_ASIGNAR_AREAS: Result := tpEspecial; { Sistema\Usuarios\Asignar Areas }
          D_EMP_REG_ALTA: Result := tpEspecial; { Empleados\Registro\Alta }
          D_CONS_CONTEO_PERSONAL: Result := tpEspecial; { Consultas\Presupuesto de Personal }
          D_EMP_NOM_SIM_FINIQUITOS:Result := tpEspecial; { Aplicar finiquitos\Liquidaci�n }
          //************* Plan Carrera Web ***********************//
          D_CARRERA_CONSULTAS_WEB: Result := tpEspecial;{ Mi Plan Carrera\Competencias Vs. Puesto ... ( TODA LA RAMA ) }
          D_CARRERA_PUESTOS_WEB: Result := tpEspecial; { Puestos\Por Familia...( TODA LA RAMA ) }
          D_CARRERA_CATALOGOS_WEB: Result := tpEspecial; { Cat�logos\Competencias ... ( TODA LA RAMA ) }
          D_CARRERA_DETALLE_CATALOGOS: Result := tpEspecial; { Detalle de Cat�logos\Puesto ... ( TODA LA RAMA ) }
          D_CARRERA_ANALITICOS_WEB: Result := tpEspecial; { Anal�ticos\Comparativo de Puestos .. ( TODA LA RAMA ) }
          D_CARRERA_MIS_COLABORADORES: Result := tpEspecial; { Mis Colaboradores\Calificar Competencias ... ( TODA LA RAMA ) }
          //************* Evaluacion ****************************//
          D_CRITERIOS_EVAL: Result := tpEspecial;
          D_ESCALAS: Result := tpEspecial;
          D_ENCUESTAS: Result := tpEspecial;
          D_CONS_PLANVACACION: Result:= tpEspecial;
          D_CAT_VALUACIONES: Result:= tpEspecial;
          {En Corporativa si es un derecho especial, en profesional no}
          {$ifdef RDD}
          D_CONS_REPORTES  : Result := tpEspecial;
          {$endif}
          D_REPORTES_CONFIDENCIALES: Result := tpEspecial; { Reportes \ Datos Confidenciales }
          D_SUSCRIPCION_REPORTE: Result:= tpEspecial; {Consultas\Reportes}
          D_SUSCRIPCION_USUARIOS: Result:= tpEspecial; {Consultas\Reportes}
          D_SUPER_GRUPOS: Result:= tpEspecial; {Supervisores\Grupos... ( TODA LA RAMA )}
          D_SUPER_AUTORIZACION_COLECTIVA: Result:= tpEspecial;
          D_PAGOS_FONACOT_TOT: Result:= tpEspecial;
          D_EMP_NOM_PRESTAMOS: Result := tpEspecial;
          D_EMP_SIM_FINIQUITOS_EMPLEADO: Result:= tpEspecial;
          D_EMP_SIM_FINIQUITOS_TOTALES: Result := tpEspecial;
          D_NOM_EXCEPCIONES_LIM_MONTO: Result:= tpEspecial;
          D_SUPER_STATUS_EMPLEADO : Result := tpEspecial;
          D_ASIS_PROC_AUTORIZ_PRENOM : Result := tpEspecial;
          D_SUPER_AUTORIZAR_PRENOMINA : Result := tpEspecial;
          {$ifdef COMMSCOPE}
          D_SUPER_AUTORIZAR_EVALUACION  : Result := True;
          {$endif}
          D_EMP_SEGUROS_GASTOS_MEDICOS : Result := tpEspecial;
          D_EMP_EXP_VACA : Result := tpEspecial;
          // D_EMP_DATOS_OTROS_ASISTENCIA : Result := tpEspecial;
          D_SIST_LST_DISPOSITIVOS: Result := tpEspecial;
          D_SIST_CALENDARIO_REPORTES : Result := tpEspecial;
     else
         Result := inherited NodoEspecial( iIndex );
     end;

end;

function TSistEditAccesos_DevEx.TextoEspecial(const sText: String): Boolean;
begin
{$ifdef RDD}
     Result := FALSE;
     case ArbolSeleccionado of
          adTress:
          begin
{$endif}
               Result := ( sText = 'Registro' ) or
                         ( sText = 'Procesos' ) or
                         ( sText = 'Anuales' ) or
                         {$ifdef RDD}
                         ( sText = 'Reportes') or
                         {$endif}
                         inherited TextoEspecial( sText );
{$ifdef RDD}
          end;
          adClasifi: Result := inherited TextoEspecial( sText );
          adEntidades: Result := (sText = '< Sin M�dulo >') or
                                 dmDiccionario.cdsModulos.Locate( 'MO_NOMBRE',sText,[] ) or
                                 inherited TextoEspecial( sText );
     end;
{$endif}
end;

procedure TSistEditAccesos_DevEx.ArbolDefinir;
begin
     PageControl.ActivePage := tsTress;
     { *** Empleados *** }
     
AddElemento( 0, D_EMPLEADOS, 'Empleados' );
   AddElemento( 1, D_EMP_DATOS, 'Datos' );
      AddElemento( 2, D_EMP_DATOS_IDENTIFICA, 'Identificaci�n' );
         AddDerechoConsulta;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_EMP_DATOS_CONTRATA, 'Contrataci�n' );
         AddDerechoConsulta;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_EMP_DATOS_AREA, 'Area' );
         AddDerechoConsulta;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_EMP_DATOS_PERCEP, 'Percepciones' );
         AddDerechoConsulta;
         AddDerechoEdicion;
         AddDerechoImpresion;
      {$ifndef OSRAM_INTERFAZ}
      AddElemento( 2, D_EMP_DATOS_GAFETE_BIOMETRICO, 'Gafete y biom�trico' );
         AddDerechoConsulta;
         AddDerechoEdicion;
         AddDerechoImpresion;
         {$ifndef DOS_CAPAS}
         AddDerecho( D_TEXT_NUM_BIOMETRICOS ); // SYNERGY 11
         AddDerecho( D_TEXT_ENROLAR_HUELLAS ); // BIOMETRICO 12
         AddDerecho( D_TEXT_BORRAR_HUELLAS ); // BIOMETRICO 13
         AddDerecho( D_TEXT_BORRAR_FACIAL ); // BIOMETRICO 14
         {$endif}
      {$endif}
      AddElemento( 2, D_EMP_DATOS_OTROS, 'Otros' );
      {$ifdef ANTES}
         AddDerechoConsulta;
         AddDerechoEdicion;
         AddDerechoImpresion;
         {Esta rama ahora esta en Expediente - Cr�dito Infonavit: AddDerecho( 'Infonavit' );}
         AddDerecho( D_RESERVADO_SISTEMA );
         AddDerecho( D_TEXT_BANCA );
         AddDerecho( D_TEXT_NIVEL0 );
         AddDerecho( D_TEXT_TARJ_CTA_GAS );
         AddDerecho( D_TEXT_TARJ_CTA_DESP );
         {$ifndef DOS_CAPAS}
         AddDerecho( D_TEXT_REINICIA_NIP );
         {AddDerecho( D_TEXT_NUM_BIOMETRICOS ); // SYNERGY 11
         AddDerecho( D_TEXT_ENROLAR_HUELLAS ); // BIOMETRICO 12
         AddDerecho( D_TEXT_BORRAR_HUELLAS ); // BIOMETRICO 13}
         {$endif}
      {$endif}
      { V2013
      Protecci�n Civil
      2013-03-18.
      AL.
      Nueva estructura de Derechos para Pantalla Otros
      }

      //   AddDerecho( D_RESERVADO_SISTEMA );
        {$ifndef DOS_CAPAS}
         AddElemento( 3, D_EMP_DATOS_OTROS_MISDATOS, 'Mis Datos' );
            AddDerecho( D_TEXT_REINICIA_NIP );
         {$endif}
         AddElemento( 3, D_EMP_DATOS_OTROS_INFONAVIT, 'Infonavit' );
            AddDerechoConsulta;
            AddDerechoImpresion;
         AddElemento( 3, D_EMP_DATOS_OTROS_FONACOT, 'Fonacot' );
            AddDerechoConsulta;
            AddDerechoEdicion;
            AddDerechoImpresion;
         AddElemento( 3, D_EMP_DATOS_OTROS_PRIMER_EMPLEO, 'Programa de Primer Empleo' );
            AddDerechoConsulta;
            AddDerechoEdicion;
            AddDerechoImpresion;
         AddElemento( 3, D_EMP_DATOS_OTROS_ASISTENCIA, 'Asistencia' );
            AddDerechoConsulta;
            AddDerechoEdicion;
            AddDerechoImpresion;
            {$ifndef DOS_CAPAS}
            {AddDerecho( D_TEXT_NUM_BIOMETRICOS ); // SYNERGY
            AddDerecho( D_TEXT_ENROLAR_HUELLAS ); // BIOMETRICO
            AddDerecho( D_TEXT_BORRAR_HUELLAS ); // BIOMETRICO    }
            {$endif}
         AddElemento( 3, D_EMP_DATOS_OTROS_EVALUACION, 'Evaluaci�n' );
            AddDerechoConsulta;
            AddDerechoEdicion;
            AddDerechoImpresion;
         AddElemento( 3, D_EMP_DATOS_OTROS_CUENTAS, 'Cuentas' );
            AddDerechoConsulta;
            AddDerechoEdicion;
            AddDerechoImpresion;
            AddDerecho( D_TEXT_BANCA );
            AddDerecho( D_TEXT_TARJ_CTA_GAS );
            AddDerecho( D_TEXT_TARJ_CTA_DESP );
         AddElemento( 3, D_EMP_DATOS_OTROS_NIVEL0, 'Confidencialidad' );
            AddDerechoConsulta;
            AddDerechoEdicion;
            AddDerechoImpresion;
         AddElemento( 3, D_EMP_DATOS_OTROS_MEDICOS, 'Datos M�dicos' );
            AddDerechoConsulta;
            AddDerechoEdicion;
            AddDerechoImpresion;
         AddElemento( 3, D_EMP_DATOS_OTROS_BRIGADAS, 'Brigadas de Protecci�n Civil' );
            AddDerechoConsulta;
            AddDerechoEdicion;
            AddDerechoImpresion;
         AddElemento( 3, D_EMP_DATOS_OTROS_TIMBRADO, 'Timbrado de N�mina' );
            AddDerechoConsulta;
            AddDerechoEdicion;
            AddDerechoImpresion;

      {$ifdef ANTES}
      AddElemento( 2, D_EMP_DATOS_ADICION, 'Adicionales' );
         AddDerechoConsulta;
         AddDerechoEdicion;
         AddDerechoImpresion;
      {$else}
      AgregaDerechosCamposAdicionales;
      {$endif}
      AddElemento( 2, D_EMP_DATOS_USUARIO, 'Usuario' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
   AddElemento( 1, D_EMP_CURRICULUM, 'Curr�culum' );
      AddElemento( 2, D_EMP_CURR_PERSONALES, 'Personales' );
         AddDerechoConsulta;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_EMP_CURR_FOTO, 'Foto e Im�genes' ); // (JB) Anexar al expediente del trabajador documentos CR1880 T1107 (Cambia de Nombre)
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_EMP_CURR_DOCUMENTO, 'Documentos' ); // (JB) Anexar al expediente del trabajador documentos CR1880 T1107
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_EMP_CURR_PARIENTES, 'Parientes' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_EMP_CURR_EXPERIENCIA, 'Experiencia' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_EMP_CURR_CURSOS, 'Cursos Anteriores' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_EMP_CURR_PUESTOS, 'Puestos Anteriores' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
   AddElemento( 1, D_EMP_EXPEDIENTE, 'Expediente' );
      AddElemento( 2, D_EMP_EXP_KARDEX, 'Kardex' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
         AddDerecho( 'Borrar/Modif. Movs. Sistema' );
         AddDerecho( 'Restringir Consulta Movs. Sistema' );
      AddElemento( 2, D_EMP_EXP_VACA, 'Vacaciones' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
         AddDerecho('Pagar en N�minas Afectadas' );
      AddElemento( 2, D_EMP_EXP_INCA, 'Incapacidades' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_EMP_EXP_PERM, 'Permisos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      If ( dmCliente.ModuloAutorizadoDerechos( okCursos ) ) then
      Begin
           AddElemento( 2, D_EMP_EXP_CURSOS_TOMADOS, 'Cursos Tomados' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_EMP_EXP_CURSOS_PROGRAMADOS, 'Cursos Programados' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_EMP_EXP_CERTIFICACIONES, 'Certificaciones Tomadas' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_EMP_EXP_CERTIFIC_PROG, 'Certificaciones Programadas' );
              AddDerechoConsulta;
              AddDerechoImpresion;
      End;
      AddElemento( 2, D_EMP_EXP_PAGOS_IMSS, 'Pagos IMSS' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_EMP_EXP_INFONAVIT, 'Cr�dito INFONAVIT' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
       AddElemento( 2, D_EMP_SEGUROS_GASTOS_MEDICOS, 'Seguros de Gastos M�dicos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
         AddDerecho( 'Puede Modificar P�lizas Vencidas' );
      If (dmCliente.ModuloAutorizadoDerechos( okHerramientas ) ) then
      Begin
           AddElemento( 2, D_EMP_EXP_TOOLS, 'Herramientas' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
      End;
      If (dmCliente.ModuloAutorizadoDerechos( okCursos ) ) then
      Begin
           {$ifndef DOS_CAPAS}
           AddElemento( 2, D_EMP_PLANCAPACITA , 'Plan de Capacitaci�n' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_EMP_HIS_EVA_COMPETEN, 'Competencias Evaluadas' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           {$endif}
      end;
   AddElemento( 1, D_EMP_NOMINA, 'N�mina' );
      AddElemento( 2, D_EMP_NOM_AHORROS, 'Ahorros' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_EMP_NOM_PRESTAMOS, 'Pr�stamos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
         AddDerecho( 'Guardar aunque las reglas no se cumplan' );
      AddElemento( 2, D_EMP_NOM_PENSIONES, 'Pensiones Alimenticias' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_EMP_NOM_ACUMULA, 'Acumulados' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_EMP_NOM_HISTORIAL, 'Historial ' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_EMP_NOM_SIM_FINIQUITOS, 'Simulaci�n Finiquitos' );
         AddElemento( 3, D_EMP_SIM_FINIQUITOS_TOTALES , 'Totales' );
            AddDerechoConsulta;
            AddDerechoImpresion;
            AddDerecho( 'Liquidaci�n' );
            AddDerecho( 'Aprobar Finiquitos' );
            AddDerecho( 'Aplicar Finiquitos' );
            AddDerecho( 'Borrar Finiquitos' );
         AddElemento( 3, D_EMP_SIM_FINIQUITOS_EMPLEADO, 'Por Empleado' );
            AddDerechoConsulta;
            AddDerechoAlta;
            AddDerechoBaja;
            AddDerechoEdicion;
            AddDerechoImpresion;
            AddDerecho( 'Liquidaci�n' );
            AddDerecho( 'Aplicar Finiquito' );
            AddDerecho( 'Aprobar Finiquito' );
            AddDerecho( 'Borrar Finiquito' );



   If ( dmCliente.ModuloAutorizadoDerechos( okCafeteria ) ) then
   Begin
        AddElemento( 1, D_EMP_CAFETERIA, 'Cafeter�a' );
           AddElemento( 2, D_EMP_CAFE_COMIDAS_DIA, 'Comidas Diarias' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_EMP_CAFE_COMIDAS_PERIODO, 'Comidas Per�odo' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_EMP_CAFE_INVITACIONES, 'Invitaciones' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
   End;
   If ( dmCliente.ModuloAutorizadoDerechos( okAccesos ) ) then
   Begin
        AddElemento( 1, D_EMP_CASETA, 'Caseta' );
           AddElemento( 2, D_EMP_CASETA_DIARIAS, 'Checadas Diarias' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
   End;   
   AddElemento( 1, D_EMP_REGISTRO, 'Registro' );
      AddElemento( 2, D_EMP_REG_ALTA, 'Alta' );
         AddDerechoAlta;
         AddDerecho( 'Permitir Excedentes en Plazas' );
         AddDerecho( 'Cambiar Valores Predeterminados por el Puesto' );
         AddDerecho( 'Modificar # de empleado autom�tico' );
      AddElemento( 2, D_EMP_REG_BAJA, 'Baja' );
      AddElemento( 2, D_EMP_REG_REINGRESO, 'Reingreso' );
      AddElemento( 2, D_EMP_REG_CAMBIO_SALARIO, 'Cambio de Salario' );
      if dmCliente.UsaPlazas then
      begin
           AddElemento( 2, D_EMP_REG_CAMBIO_PLAZA, 'Cambio de Plaza' );
           AddElemento( 2, D_EMP_REG_CAMBIO_PUESTO, 'Cambio de Clasificaci�n' );
      end
      else
      begin
           AddElemento( 2, D_EMP_REG_CAMBIO_TURNO, 'Cambio de Turno' );
           AddElemento( 2, D_EMP_REG_CAMBIO_PUESTO, 'Cambio de Puesto' );
           AddElemento( 2, D_EMP_REG_CAMBIO_AREA, 'Cambio de Area' );
      end;
      AddElemento( 2, D_EMP_REG_CAMBIO_CONTRATO, 'Cambio de Contrato' );
      AddElemento( 2, D_EMP_REG_CAMBIO_PRESTA, 'Cambio de Prestaciones' );
      AddElemento( 2, D_EMP_REG_CAMBIO_TIPO_NOMINA, 'Cambio de Tipo de N�mina' );
      if ( not dmCliente.UsaPlazas ) then
         AddElemento( 2, D_EMP_REG_CAMBIO_MULTIPLE, 'Cambios M�ltiples' );
      AddElemento( 2, D_EMP_REG_VACACIONES, 'Vacaciones' );
      AddElemento( 2, D_EMP_REG_CIERRE_VACA, 'Cerrar Vacaciones' );
      AddElemento( 2, D_EMP_REG_INCAPACIDAD, 'Incapacidad' );
      AddElemento( 2, D_EMP_REG_PERMISO, 'Permiso' );
      AddElemento( 2, D_EMP_REG_KARDEX, 'Kardex general' );
      If ( dmCliente.ModuloAutorizadoDerechos( okCursos ) ) then
         AddElemento( 2, D_EMP_REG_CURSO_TOMADO, 'Curso Tomado' );
      If ( dmCliente.ModuloAutorizadoDerechos( okCafeteria ) ) then
         AddElemento( 2, D_EMP_REG_COMIDAS, 'Comidas' );
      AddElemento( 2, D_EMP_REG_PRESTAMO, 'Pr�stamos' );
      If ( dmCliente.ModuloAutorizadoDerechos( okHerramientas ) ) then
      Begin
           AddElemento( 2, D_EMP_REG_ENTREGA_TOOLS, 'Entregar Herramienta' );
           AddElemento( 2, D_EMP_REG_ENTREGA_GLOBAL, 'Entrega Global' );
           AddElemento( 2, D_EMP_REG_REGRESA_TOOLS, 'Regresar Herramienta' );
      End;
      If ( dmCliente.ModuloAutorizadoDerechos( okCursos ) ) then
      begin
           AddElemento( 2, D_EMP_PROC_CERTIFIC_EMP, 'Certificaciones por Empleado' );
           AddElemento( 2, D_EMP_PROC_CERTIFIC_GLOBAL, 'Empleados Certificados' );
      end;
      If ( dmCliente.ModuloAutorizadoDerechos( okAccesos ) ) then
      begin
           AddElemento( 2, D_EMP_REG_CASETA_DIARIAS, 'Caseta' );
      end;         
   AddElemento( 1, D_EMP_PROCESOS, 'Procesos' );
      AddElemento( 2, D_EMP_PROC_RECALC_INT, 'Recalcular Integrados' );
      AddElemento( 2, D_EMP_PROC_PROM_VARIABLES, 'Promediar Percepciones Variables' );
      AddElemento( 2, D_EMP_PROC_SALARIO_GLOBAL, 'Cambio de Salario Global' );
      AddElemento( 2, D_EMP_PROC_TABULADOR, 'Aplicar Tabulador' );
      AddElemento( 2, D_EMP_PROC_VACA_GLOBAL, 'Vacaciones Globales' );
      AddElemento( 2, D_EMP_PROC_CANCELAR_VACA, 'Cancelar Vacaciones' );
      AddElemento( 2, D_EMP_PROC_CIERRE_VACACIONES, 'Cierre Global de Vacaciones' );
      AddElemento( 2, D_EMP_PROC_CANCELA_CIERRE_VACA, 'Cancelar Cierres de Vacaciones' );
      AddElemento( 2, D_EMP_PROC_RECALCULA_SALDOS_VACA, 'Recalcula Saldos de Vacaciones' );
      AddElemento( 2, D_EMP_PROC_APLICA_EVENTOS, 'Aplicar Eventos' );
      AddElemento( 2, D_EMP_PROC_IMPORTA_KARDEX, 'Importar Kardex' );
      AddElemento( 2, D_EMP_PROC_CANCELA_KARDEX, 'Cancelar Kardex' );
      AddElemento( 2, D_EMP_PROC_IMPORTAR_ALTAS, 'Importar Altas' );
      AddElemento( 2, D_EMP_PROC_PERMISO_GLOBAL, 'Permisos Globales' );
      AddElemento( 2, D_EMP_PROC_CANCELA_PERMISO_GLOBAL, 'Cancelar Permisos Globales' );
      If ( dmCliente.ModuloAutorizadoDerechos( okCursos ) ) then
      Begin
           AddElemento( 2, D_EMP_PROC_CURSO_GLOBAL, 'Curso Tomado Global' );
           AddElemento( 2, D_EMP_PROC_BORRAR_CURSO_GLOBAL, 'Cancelar Curso Tomado Global' );
           AddElemento( 2, D_EMP_PROC_CURSO_PROG_GLOBAL, 'Curso Programado Global' );
           AddElemento( 2, D_EMP_PROC_CANC_CUR_PROG_GLOB, 'Cancelar Curso Programado Global' );
           AddElemento( 2, D_EMP_PROC_REVISAR_CAPACITACIONES, 'Revisar datos para STPS' );
           AddElemento( 2, D_EMP_PROC_FOLIAR_CAPACITACIONES, 'Foliar Capacitaciones de STPS' ); 
           AddElemento( 2, D_EMP_PROC_REINICIAR_CAPACITACIONES, 'Reiniciar Folio Capacitaciones de STPS' );
      End;
      AddElemento( 2, D_EMP_PROC_IMP_ASISTENCIA_SESIONES, 'Importar Cursos Tomados' );
      AddElemento( 2, D_EMP_PROC_TRANSFERENCIA, 'Transferencia de Empleado' );
      AddElemento( 2, D_EMP_PROC_RENUMERA, 'Renumera Empleados' );

      If ( dmCliente.ModuloAutorizadoDerechos( okHerramientas ) ) then
      Begin
           AddElemento( 2, D_EMP_PROC_ENTREGAR_HERRAMIENTA, 'Entregar Herramienta' );
           AddElemento( 2, D_EMP_PROC_REGRESAR_HERRAMIENTA, 'Regresar Herramienta' );
      End;
      If ( dmCliente.ModuloAutorizadoDerechos( okCafeteria ) ) then
      Begin
           AddElemento( 2, D_EMP_PROC_COMIDAS_GRUPALES, 'Registro Grupal de Comidas' );
           AddElemento( 2, D_EMP_PROC_CORREGIR_FECHAS_CAFE, 'Corregir Fechas Globales de Cafeter�a' );
      End;
      AddElemento( 2, D_EMP_PROC_IMP_AHORROS_PRESTAMOS, 'Importar Ahorros/Pr�stamos' );
      AddElemento( 2, D_EMP_PROC_RENUEVA_SGM, 'Renovar Seguros de Gastos M�dicos' );
      AddElemento( 2, D_EMP_PROC_IMP_SGM, 'Importar Seguros de Gastos M�dicos' );
      AddElemento( 2, D_EMP_PROC_BORRA_SGM, 'Borrar Seguros de Gastos M�dicos' );
      AddElemento( 2, D_EMP_PROC_IMPORTA_ORG, 'Importar Organigrama' );
      AddElemento( 2, D_EMP_IMPORTAR_IMAGENES, 'Importar Im�genes' );
      AddElemento( 2, D_EMP_PROG_CAMBIO_TURNO, 'Cambio Masivo de Turno' );


{ *** Asistencia *** }
If ( dmCliente.ModuloAutorizadoDerechos( okAsistencia ) ) then
Begin
     AddElemento( 0, D_ASISTENCIA, 'Asistencia' );
        AddElemento( 1, D_ASIS_DATOS, 'Datos' );
           AddElemento( 2, D_ASIS_DATOS_TARJETA_DIARIA, 'Tarjeta Diaria' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_ASIS_DATOS_PRENOMINA, 'Pre-N�mina' );
              AddDerechoConsulta;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_ASIS_DATOS_CALENDARIO, 'Calendario' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_ASIS_DATOS_AUTORIZACIONES, 'Autorizaci�n' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
              AddDerecho( 'Aprobar' );
           AddElemento( 2, D_ASIS_DATOS_CLASIFI, 'Clasificaciones' );
              AddElemento( 3, D_ASIS_DATOS_CLASIFI_TEMP, 'Temporales' );
                 AddDerechoConsulta;
                 AddDerechoAlta;
                 AddDerechoBaja;
                 AddDerechoEdicion;
                 AddDerechoImpresion;
              AddElemento( 3, D_ASIS_DATOS_CLASIFI_TEMP_NIVELES, 'Tipos' );
                 AddDerecho( 'Puesto' );
                 AddDerecho( 'Clasificaci�n' );
                 AddDerecho( 'Turno' );
                 AddDerecho( 'Nivel 1' );
                 AddDerecho( 'Nivel 2' );
                 AddDerecho( 'Nivel 3' );
                 AddDerecho( 'Nivel 4' );
                 AddDerecho( 'Nivel 5' );
                 AddDerecho( 'Nivel 6' );
                 AddDerecho( 'Nivel 7' );
                 AddDerecho( 'Nivel 8' );
                 AddDerecho( 'Nivel 9' );
                 {$ifdef ACS}
                 AddDerecho( 'Nivel 10' );
                 AddDerecho( 'Nivel 11' );
                 AddDerecho( 'Nivel 12' );
                 {$endif}
        AddElemento( 1, D_ASIS_REGISTRO, 'Registro' );
           AddElemento( 2, D_ASIS_REG_AUTO_EXTRAS, 'Autorizar Extras y Permisos' );
           AddElemento( 2, D_ASIS_REG_HORARIO_TEMPO, 'Horarios Temporales' );
           AddElemento( 2, D_ASIS_REG_AUTORIZACIONES_APROBAR, 'Autorizaciones por Aprobar' );
        AddElemento( 1, D_ASIS_PROCESOS, 'Procesos' );
           AddElemento( 2, D_ASIS_PROC_POLL_RELOJES, 'Poll de Relojes' );
           AddElemento( 2, D_ASIS_PROC_PROC_TARJETAS, 'Procesar Tarjetas' );
           AddElemento( 2, D_ASIS_PROC_CALC_PRENOMINA, 'Calcular Pre-N�mina' );
              AddDerecho( 'Nueva' );
              AddDerecho( 'Todos' );
           AddElemento( 2, D_ASIS_PROC_EXTRAS_GLOBAL, 'Extras y Permisos Globales' );
           AddElemento( 2, D_ASIS_PROC_REG_AUTOMATICO, 'Registros Autom�ticos' );
           AddElemento( 2, D_ASIS_PROC_FECHAS_GLOBAL, 'Corregir Fechas Globales' );
           AddElemento( 2, D_ASIS_PROC_RECALC_TARJETA, 'Recalcular Tarjetas' );
              AddDerecho( 'Revisar Clasificaci�n Tarjeta' ); // (JB) Se desglosan los derechos de FWizAsistRecalcular
              AddDerecho( 'Efectuar Cancelaciones de Autorizaciones' );
              AddDerecho( 'Reprocesar Tarjetas' );
              AddDerecho( 'Efectuar Cancelaciones de Horarios Temporales' );
           AddElemento( 2, D_ASIS_PROC_INTERCAMBIO_FESTIVOS, 'Intercambio de Festivos' );
           AddElemento( 2, D_ASIS_PROC_CANCELAR_EXCEP_FEST, 'Cancelar Excepciones de Festivos' );
           AddElemento( 2, D_ASIS_PROC_REGISTRAR_EXCEP_FEST, 'Registrar Excepciones de Festivos' );
           AddElemento( 2, D_ASIS_AJUST_INCAPA_CAL, 'Ajustar Incapacidades en Calendario' );
           AddElemento( 2, D_ASIS_PROC_AUTORIZ_PRENOM, 'Autorizaci�n de Pre-N�mina' );
              AddDerecho( 'Autorizar Pre-N�mina' );
              AddDerecho( 'Desautorizar Pre-N�mina' );
           AddElemento( 2, D_ASIS_PROC_IMPORTA_CLASIFICACIONES, 'Importar Clasificaciones Temporales' );
           AddElemento( 2, D_SIST_TERM_EXTRAER_CHECADAS, 'Extraer checadas Terminales GTI' );
           AddElemento( 2, D_SIST_TERM_RECUPERAR_CHECADAS , 'Transformar checadas Terminales GTI' ); //@DC
        AddElemento( 1, D_ASIS_BLOQUEO_CAMBIOS, 'Bloqueo de Cambios' );
           AddDerecho( 'Modificar Fecha L�mite' );
           AddDerecho( 'Modificar Tarjetas Anteriores' );
End;
{ *** N�minas *** }

AddElemento( 0, D_NOMINA   , 'N�minas' );
   AddElemento( 1, D_NOM_DATOS, 'Datos' );
      AddElemento( 2, D_NOM_DATOS_TOTALES, 'Totales' );
         AddDerechoConsulta;
         AddDerechoImpresion;
      AddElemento( 2, D_NOM_DATOS_DIAS_HORAS, 'D�as/Horas' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_NOM_DATOS_MONTOS, 'Montos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_NOM_DATOS_PRENOMINA, 'Pre-N�mina' );
         AddDerechoConsulta;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_NOM_DATOS_CLASIFI, 'Clasificaci�n' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_NOM_DATOS_POLIZAS, 'P�lizas' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoImpresion;
   AddElemento( 1, D_NOM_EXCEPCIONES, 'Excepciones' );
      AddElemento( 2, D_NOM_EXCEP_DIAS_HORAS, 'D�as/Horas' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_NOM_EXCEP_MONTOS, 'Montos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_NOM_EXCEP_GLOBALES, 'Globales' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      if ( dmCliente.ModuloAutorizadoCosteo ) then
      begin
           AddElemento( 2, D_COSTEO_TRANSFERENCIAS, 'Transferencias' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
      end;


    AddElemento( 1, D_PAGOS_FONACOT, 'Pagos Fonacot' );
      AddElemento( 2, D_PAGOS_FONACOT_TOT, 'Totales' );
         AddDerechoConsulta;
         AddDerechoBaja;
         AddDerechoImpresion;
         AddDerecho( 'Cerrar c�lculo Fonacot' );
         AddDerecho( 'Abrir c�lculo Fonacot' );
      AddElemento( 2, D_PAGOS_FONACOT_TOT_EMPL, 'Totales por Empleado' );
         AddDerechoConsulta;
         AddDerechoImpresion;
   AddElemento( 1, D_NOM_REGISTRO, 'Registro' );
      AddElemento( 2, D_NOM_REG_NOMINA_NUEVA, 'N�mina Nueva' );
      AddElemento( 2, D_NOM_REG_BORRA_ACTIVA, 'Borrar N�mina Activa' );
      AddElemento( 2, D_NOM_REG_EXCEP_DIA_HORA, 'Excepci�n de D�a/Hora' );
      AddElemento( 2, D_NOM_REG_EXCEP_MONTO, 'Excepci�n de Monto' );
      AddElemento( 2, D_NOM_REG_EXCEP_MONTO_GLOBAL, 'Excepciones de Monto Globales' );
      AddElemento( 2, D_NOM_REG_LIQUIDACION, 'Liquidaci�n' );
      AddElemento( 2, D_NOM_REG_PAGO_RECIBOS, 'Pago de Recibos' );
   AddElemento( 1, D_NOM_PROCESOS, 'Procesos' );
      AddElemento( 2, D_NOM_PROC_CALCULAR, 'Calcular N�mina' );
         AddDerecho( 'Nueva' );
         AddDerecho( 'Todos' );
         AddDerecho( 'Rango' );
         AddDerecho( 'Pendiente' );
      AddElemento( 2, D_NOM_PROC_AFECTAR, 'Afectar N�mina' );
      AddElemento( 2, D_NOM_PROC_FOLIAR_RECIBOS, 'Foliar Recibos' );
      AddElemento( 2, D_NOM_PROC_IMP_LISTADO, 'Imprimir Listado' );
      AddElemento( 2, D_NOM_PROC_IMP_RECIBOS, 'Imprimir Recibos' );
      AddElemento( 2, D_NOM_PROC_CALC_POLIZA, 'Generar P�liza Contable' );
      AddElemento( 2, D_NOM_PROC_IMP_POLIZA, 'Imprimir P�liza Contable' );
      AddElemento( 2, D_NOM_PROC_EXP_POLIZA, 'Exportar P�liza Contable' );
{$ifdef ANTES}
      AddElemento( 2, D_NOM_PROC_CRED_APLICADO, 'Cr�dito Aplicado Mensual' );
{$else}
      AddElemento( 2, D_NOM_PROC_CRED_APLICADO, 'SUBE Aplicado Mensual' );
{$endif}
      AddElemento( 2, D_NOM_PROC_DESAFECTAR, 'Desafectar N�mina' );
      AddElemento( 2, D_NOM_PROC_LIMPIA_ACUM, 'Limpiar Acumulados' );
      AddElemento( 2, D_NOM_PROC_RECALC_ACUMULADOS, 'Recalcular Acumulados' );
      AddElemento( 2, D_NOM_PROC_RECALC_AHORROS, 'Recalcular Ahorros' );
      {*** US 15765: Es necesario separar los procesos (wizards) de recalculo de ahorros y pr�stamos para administrar de mejor manera los accesos al proceso y prevenir rec�lculos innecesarios***}
      AddElemento( 2, D_NOM_PROC_RECALC_PRESTAMOS, 'Recalcular Pr�stamos' );
      AddElemento( 2, D_NOM_PROC_IMP_MOVIMIEN, 'Importar Movimientos' );
         AddDerecho( 'Excepciones' );
         AddDerecho( 'Acumulados' );
      AddElemento( 2, D_NOM_PROC_EXP_MOVIMIEN, 'Exportar Movimientos' );
      AddElemento( 2, D_NOM_PROC_IMP_PAGO_RECIBO, 'Importar Pago de Recibos' );
      AddElemento( 2, D_NOM_PROC_RE_FOLIAR, 'Re-Foliar Recibos' );
      AddElemento( 2, D_NOM_PROC_RETROACTIVOS, 'Calcular Retroactivos' );
      AddElemento( 2, D_NOM_PROC_COPIAR_NOMINA, 'Copiar N�mina' );
      AddElemento( 2, D_NOM_PROC_CALC_DIFERENCIAS, 'Calcular Diferencias' );
      AddElemento( 2, D_NOM_PROC_PAGAR_FUERA, 'Pagar por Fuera' );
      AddElemento( 2, D_NOM_PROC_CANCELA_PASADA, 'Cancelar N�minas Pasadas' );
      AddElemento( 2, D_NOM_PROC_LIQUIDA_GLOBAL, 'Liquidaci�n Global' );
      AddElemento( 2, D_NOM_PROC_NETO_BRUTO, 'Calcular Salario Neto/Bruto' );
      AddElemento( 2, D_NOM_PROC_RASTREO, 'Rastrear C�lculo' );
      AddElemento( 2, D_NOM_PROC_AJUSTE_RET_FONACOT, 'Ajustar Retenci�n de Fonacot');
      AddElemento( 2, D_NOM_PROC_AJUSTE_RET_FONACOT_CANC, 'Cancelar Ajuste Retenci�n de Fonacot');
      AddElemento( 2, D_NOM_PROC_CALCULO_PAGO_FONACOT, 'Calcular Pago Fonacot');
      {$ifndef OSRAM_INTERFAZ}
      AddElemento( 2, D_NOM_FONACOT_IMPORTAR_CEDULA, 'Importar c�dula de cr�ditos Fonacot');
      AddElemento( 2, D_NOM_FONACOT_ELIMINAR_CEDULA, 'Eliminar registros de c�dula Fonacot');
      AddElemento( 2, D_NOM_FONACOT_GENERAR_CEDULA, 'Generar c�dula de pago Fonacot');
      {$endif}
      if ( dmCliente.ModuloAutorizadoCosteo ) then
      begin
           AddElemento( 2, D_NOM_PROC_TRANSFERENCIAS_COSTEO, 'Transferencias');
           AddElemento( 2, D_NOM_PROC_CANCEL_TRANSF_COSTEO, 'Eliminar Transferencias' );
           AddElemento( 2, D_NOM_PROC_CALCULO_COSTEO, 'C�lculo de Costeo');
      end;
      AddElemento( 2, D_NOM_PROC_PREVIO_ISR, 'C�lculo Previo de ISR');
      AddElemento( 2, D_NOM_PROC_PREVIO_ISR_IMPRIMIR, 'Imprimir C�lculo Previo de ISR');

   AddElemento( 1, D_NOM_ANUALES, 'Anuales' );
      AddElemento( 2, D_NOM_ANUAL_PERIODOS, 'Definir Per�odos' );
      AddElemento( 2, D_NOM_ANUAL_CALC_AGUINALDO, 'Calcular Aguinaldos' );
      AddElemento( 2, D_NOM_ANUAL_IMP_AGUINALDO, 'Imprimir Aguinaldos' );
      AddElemento( 2, D_NOM_ANUAL_PAGA_AGUINALDO, 'Pagar Aguinaldos' );
      AddElemento( 2, D_NOM_ANUAL_CALC_AHORROS, 'Calcular Reparto de Ahorros' );
      AddElemento( 2, D_NOM_ANUAL_IMP_AHORRO, 'Imprimir Reparto de Ahorros' );
      AddElemento( 2, D_NOM_ANUAL_CALC_AHORRO, 'Pagar Ahorro' );
      AddElemento( 2, D_NOM_ANUAL_CIERRE_AHORRO, 'Cierre de Ahorros' );
      AddElemento( 2, D_NOM_ANUAL_CIERRE_PRESTA, 'Cierre de Pr�stamos' );
      AddElemento( 2, D_NOM_ANUAL_CALC_PTU, 'Calcular PTU' );
      AddElemento( 2, D_NOM_ANUAL_IMP_PTU, 'Imprimir PTU' );
      AddElemento( 2, D_NOM_ANUAL_PAGA_PTU, 'Pagar PTU' );
{$ifdef ANTES}
      AddElemento( 2, D_NOM_ANUAL_CREDITO, 'Declaraci�n de Cr�dito al Salario' );
      AddElemento( 2, D_NOM_ANUAL_IMP_CREDITO, 'Imprimir Declaraci�n de Cr�dito Al Salario' );
{$else}
      AddElemento( 2, D_NOM_ANUAL_CREDITO, 'Declaraci�n de Subsidio al Empleo' );
      AddElemento( 2, D_NOM_ANUAL_IMP_CREDITO, 'Imprimir Declaraci�n de Subsidio al Empleo' );
{$endif}
      AddElemento( 2, D_NOM_ANUAL_COMPARA_ISPT, 'Comparar ISPT Anual' );
      AddElemento( 2, D_NOM_ANUAL_IMP_COMPARA, 'Imprimir Comparativo Anual' );
      AddElemento( 2, D_NOM_ANUAL_DIF_COMPARA, 'Diferencias de ISPT a N�mina' );
      AddElemento( 2, D_NOM_ANUAL_DECLARACION_ANUAL, 'Declaraci�n Anual' );
      AddElemento( 2, D_NOM_ANUAL_DECLARACION_ANUAL_CIERRE, 'Cierre Declaraci�n Anual' );
      AddElemento( 2, D_NOM_ANUAL_IMP_DECLARACION_ANUAL, 'Imprimir Declaraci�n Anual' );
   AddElemento( 1, D_NOM_EXCEPCIONES_LIM_MONTO, 'Exceder L�mite de Monto en Excepciones' );

{ *** Pagos IMSS *** }

AddElemento( 0, D_IMSS     , 'Pagos IMSS' );
   AddElemento( 1, D_IMSS_DATOS, 'Datos' );
      AddElemento( 2, D_IMSS_DATOS_TOTALES, 'Totales' );
         AddDerechoConsulta;
         AddDerechoBaja;
         AddDerechoImpresion;
      AddElemento( 2, D_IMSS_DATOS_EMP_MEN, 'Por Empleado Mensual' );
         AddDerechoConsulta;
         AddDerechoBaja;
         AddDerechoImpresion;
      AddElemento( 2, D_IMSS_DATOS_EMP_BIM, 'Por Empleado Bimestral' );
         AddDerechoConsulta;
         AddDerechoBaja;
         AddDerechoImpresion;
      AddElemento( 2, D_IMSS_CONCILIA_INFONAVIT, 'Conciliaci�n Infonavit' );
         AddDerechoConsulta;
         AddDerechoBaja;
         AddDerechoImpresion;
   AddElemento( 1, D_IMSS_HISTORIA, 'Historia' );
      AddElemento( 2, D_IMSS_HIST_EMP_MEN, 'Por Empleado Mensual' );
         AddDerechoConsulta;
         AddDerechoBaja;
         AddDerechoImpresion;
      AddElemento( 2, D_IMSS_HIST_EMP_BIM, 'Por Empleado Bimestral' );
         AddDerechoConsulta;
         AddDerechoBaja;
         AddDerechoImpresion;
   AddElemento( 1, D_IMSS_REGISTRO, 'Registro' );
      AddElemento( 2, D_IMSS_REG_NUEVO, 'Pago de IMSS Nuevo' );
      AddElemento( 2, D_IMSS_REG_BORRAR_ACTIVO, 'Borrar Pago de IMSS Activo' );
   AddElemento( 1, D_IMSS_PROCESOS, 'Procesos' );
      AddElemento( 2, D_IMSS_PROC_CALCULAR, 'Calcular Pago' );
      AddElemento( 2, D_IMSS_PROC_RECARGOS, 'Calcular Recargos' );
      { AP(23/May/2008): Se elimin� el proceso
      AddElemento( 2, D_IMSS_PROC_CALC_TASA_INFO, 'Calcular Tasa INFONAVIT' );}
      AddElemento( 2, D_IMSS_PROC_REVISAR_SUA, 'Revisar Datos para SUA' );
      AddElemento( 2, D_IMSS_PROC_EXP_SUA, 'Exportar a SUA' );
      AddElemento( 2, D_IMSS_PROC_CALC_RIESGO, 'Calcular Prima de Riesgo' );
      AddElemento( 2, D_IMSS_PROC_AJUS_INFONAVIT, 'Ajuste de Retenci�n Infonavit' );
      AddElemento( 2, D_IMSS_PROC_VALID_MOV_IDSE, 'Validaci�n movimientos IDSE');

{ *** Capacitaci�n *** }
If ( dmCliente.ModuloAutorizadoDerechos( okCursos ) ) then
Begin
     AddElemento( 0, D_CAPACITACION, 'Capacitaci�n' );
        AddElemento( 1, D_CAT_CAPA_SESIONES, 'Grupos' );
           AddDerechoConsulta;
           AddDerechoAlta;
           AddDerechoBaja;
           AddDerechoEdicion;
           AddDerechoImpresion;
     AddElemento( 1, D_CAPA_RESERVAS, 'Reservaciones de aula' );
        AddDerechoConsulta;
        AddDerechoAlta;
        AddDerechoBaja;
        AddDerechoEdicion;
        AddDerechoImpresion;
     AddElemento( 1, D_CAP_MATRIZ_CURSOS , 'Plan de Cursos Programados' );
        AddDerechoConsulta;
        AddDerechoImpresion;
     {$ifndef DOS_CAPAS}
     AddElemento( 1, D_CAP_MATRIZ_HBLDS , 'Matriz de Habilidades' );
        AddDerechoConsulta;
        AddDerechoImpresion;
     {$endif}
End;

{ *** Consultas *** }

AddElemento( 0, D_CONSULTAS, 'Consultas' );
   {$ifdef DOS_CAPAS}
   AddElemento( 1, D_CONS_REPORTES, 'Reportes' );
      AddElemento( 2, D_REPORTES_EMPLEADOS, 'Empleados' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_REPORTES_CURSOS, 'Cursos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_REPORTES_ASISTENCIA, 'Asistencia' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_REPORTES_NOMINA, 'Nomina' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_REPORTES_IMSS, 'Pago Imss' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_REPORTES_CONSULTAS, 'Consultas' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_REPORTES_CATALOGOS, 'Cat�logos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_REPORTES_TABLAS, 'Tablas' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_REPORTES_CAFETERIA, 'Cafeter�a' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_REPORTES_KIOSCO, 'Kiosco' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_REPORTES_ACCESOS, 'Caseta' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_REPORTES_MIGRACION, 'Migraci�n' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_REPORTES_ENVIO_EMAIL, 'Env�o Email Empleados' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
   {$else}
   AddElemento( 1, D_CONS_REPORTES, 'Reportes - Datos Confidenciales y Suscripciones' );
   {$endif}
      AddElemento( 2, D_REPORTES_CONFIDENCIALES, 'Ver Datos Confidenciales' );
      AddElemento( 2, D_SUSCRIPCION_USUARIOS, 'Suscribir a otros Usuarios/Roles' );

   //************** INICIO NUEVOS DERECHOS DE ACCESO RDD *****************
   AddElemento( 1, D_CONFIGURADOR_REPORTEADOR , 'Configurador de Reporteador' );

      AddElemento( 2, D_CR_REPORTEADOR, 'Reporteador' );
         AddElemento( 3, D_CRR_REPORTES, 'Reportes' );
            AddDerechoConsulta;
         AddElemento( 3, D_CRR_SQL, 'SQL' );
            AddDerechoConsulta;
            AddDerechoImpresion;
         AddElemento( 3, D_CRR_BITACORA, 'Bit�cora' );
            AddDerechoConsulta;
            AddDerechoImpresion;
         AddElemento( 3, D_CRR_BITACORA_PROCESOS, 'Bit�cora de Procesos' );
            AddDerechoConsulta;
            AddDerecho( D_TEXT_CANCELAR_PROCESOS );
            AddDerechoImpresion;

      AddElemento( 2, D_CR_CONFIGURACION, 'Configuraci�n' );
         AddElemento( 3, D_CRC_CATALOGOS_CLASIFICACIONES, 'Cat�logos de Clasificaciones' );
            AddDerechoConsulta;
            AddDerechoAlta;
            AddDerechoBaja;
            AddDerechoEdicion;
            AddDerechoImpresion;
         AddElemento( 3, D_CRC_DICCIONARIO_DATOS, 'Diccionario de Datos' );
            AddDerechoConsulta;
            AddDerechoAlta;
            AddDerechoBaja;
            AddDerechoEdicion;
            AddDerechoImpresion;
            AddDerecho( D_TEXT_TABLAS_CON_CONFIDENCIALIDAD );

         AddElemento( 3, D_CRC_CATALOGO_VALORES, 'Cat�logo de Lista de Valores' );
            AddDerechoConsulta;
            AddDerechoAlta;
            AddDerechoBaja;
            AddDerechoEdicion;
            AddDerechoImpresion;
         AddElemento( 3, D_CRC_CATALOGOS_CONDICIONES, 'Cat�logos de Condiciones' );
            AddDerechoConsulta;
            AddDerechoAlta;
            AddDerechoBaja;
            AddDerechoEdicion;
            AddDerechoImpresion;
         AddElemento( 3, D_CRC_CATALOGOS_MODULOS, 'Cat�logos de M�dulos' );
            AddDerechoConsulta;
            AddDerechoAlta;
            AddDerechoBaja;
            AddDerechoEdicion;
            AddDerechoImpresion;
         AddElemento( 3, D_CRC_PROCESO, 'Procesos' );
            AddElemento( 4, D_CRC_PROC_IMPORTAR, 'Importar Diccionario' );
            AddElemento( 4, D_CRC_PROC_EXPORTAR, 'Exportar Diccionario' );

      AddElemento( 2, D_CR_SISTEMA, 'Sistema' );
         AddElemento( 3, D_CRS_GRUPO_USUARIOS, 'Grupo de Usuarios' );
            AddDerechoConsulta;
            AddDerechoEdicion;
            AddDerechoImpresion;

//************** FIN NUEVOS DERECHOS DE ACCESO RDD *****************


   AddElemento( 1, D_CONS_SQL , 'SQL' );
      AddDerechoConsulta;
      AddDerechoImpresion;
   AddElemento( 1, D_CONS_BITACORA, 'Bit�cora / Procesos' );
      AddDerechoConsulta;
      AddDerecho( 'Cancelar Procesos' );
      AddDerechoImpresion;
   AddElemento( 1, D_CONS_CONTEO_PERSONAL, 'Presupuesto de Personal' );
      AddDerechoConsulta;
      AddDerechoAlta;
      AddDerechoBaja;
      AddDerechoEdicion;
      AddDerechoImpresion;
      AddElemento( 2, D_CONTEO_CONFIGURACION, 'Configurar' );
   AddElemento( 1, D_CONS_ORGANIGRAMA, 'Organigrama' );
      AddDerechoConsulta;
      AddDerechoAlta;
      AddDerechoBaja;
      AddDerechoEdicion;
      AddDerechoImpresion;
   AddElemento( 1, D_CONS_PLANVACACION, 'Solicitud de vacaciones');
      AddDerechoConsulta;
      AddDerechoAlta;
      AddDerechoBaja;
      AddDerechoEdicion;
      AddDerechoImpresion;
      AddDerecho( D_TEXT_AUTORIZAR );
      AddDerecho( D_TEXT_ESPEC_PER_PAGO );
      AddDerecho( D_TEXT_PAGAR_AFECTADAS );
  AddElemento( 1, D_TABLEROS, 'Tableros');
      AddElemento( 2, D_INICIO, 'Tablero de Inicio' );
          AddDerechoConsulta;
          AddDerechoImpresion;
      AddElemento( 2, D_TABLEROS_ROLES, 'Tableros de Inicio por Roles de Usuario');
          AddDerechoConsulta;
          AddDerechoAlta;
          AddDerechoBaja;
          AddDerechoImpresion;
  AddElemento( 1, D_CONS_ENVIOS_PROGRAMADOS, 'Env�os Programados' );
     AddDerechoConsulta;
     AddDerechoAlta;
     AddDerechoBaja;
     AddDerechoEdicion;
     AddDerechoImpresion;
     AddDerecho(D_TEXT_INSCRIBIR_ROLES_ENVIOS);
     AddDerecho(D_TEXT_INSCRIBIR_USUARIOS_ENVIOS);

{ *** Cat�logos *** }

AddElemento( 0, D_CATALOGOS, 'Cat�logos' );
   AddElemento( 1, D_CAT_CONTRATACION, 'Contrataci�n' );
      AddElemento( 2, D_CAT_CONT_PUESTOS, 'Puestos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_CAT_CONT_CLASIFI, 'Clasificaciones' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_CAT_CONT_PER_FIJAS, 'Percepciones Fijas' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_CAT_CONT_TURNOS, 'Turnos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_CAT_CONT_HORARIOS, 'Horarios' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_CAT_CONT_PRESTA, 'Prestaciones' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_CAT_CONT_CONTRATO, 'Tipo de Contrato' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
   AddElemento( 1, D_CAT_DESCRIPCION_PUESTOS, 'Descripci�n de Puestos' );
      AddElemento( 2, D_CAT_DESC_PERF_PUESTOS, 'Perfiles de Puestos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_CAT_DESC_SECC_PERF, 'Secciones de Perfiles' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_CAT_DESC_CAMPOS_PERF, 'Campos de Perfil' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;

   {$IFNDEF DOS_CAPAS}
   AddElemento( 1, D_CAT_VALUACION_PUESTOS, 'Valuaci�n de Puestos' );
      AddElemento( 2, D_CAT_VALUACIONES, 'Valuaciones de Puestos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
         AddDerecho( 'Copiar Valuaciones' );
      AddElemento( 2, D_CAT_VAL_PLANTILLAS, 'Plantillas de Valuaci�n' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_CAT_VAL_FACTORES, 'Factores de Valuaci�n' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_CAT_VAL_SUBFACTORES, 'Subfactores de Valuaci�n' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
   {$ENDIF}

   AddElemento( 1, D_CAT_NOMINA, 'N�mina' );
      AddElemento( 2, D_CAT_NOMINA_CONCEPTOS, 'Conceptos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
         AddDerecho( D_CAT_IMPORTAR_CONCEPTOS );
      AddElemento( 2, D_CAT_NOMINA_PARAMETROS, 'Par�metros' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_CAT_NOMINA_PERIODOS, 'Per�odos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_CAT_NOMINA_FOLIOS, 'Folios de Recibos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_CAT_NOMINA_POLIZAS, 'Tipos de P�liza' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_CAT_NOMINA_TPENSION, 'Tipos de Pensi�n' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
{$ifdef QUINCENALES}
      AddElemento( 2, D_CAT_TPERIODOS, 'Tipos de Periodo' );
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoConsulta;
         AddDerechoEdicion;
         AddDerechoImpresion;
{$endif}
      if ( dmCliente.ModuloAutorizadoCosteo ) then
      begin
           AddElemento( 2, D_COSTEO_GRUPOS, 'Grupos de Costeo' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;

           AddElemento( 2, D_COSTEO_CRITERIOS, 'Criterios de Costeo' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;

           AddElemento( 2, D_COSTEO_CRITERIOS_CONCEPTO, 'Conceptos de Costeo' ); //JB Cambio de Nombre en fase de redise�o
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
      end;

   AddElemento( 1, D_CAT_GENERALES, 'Generales' );
      AddElemento( 2, D_CAT_GRALES_SOCIALES, 'Razones Sociales' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_CAT_GRALES_PATRONALES, 'Registros Patronales' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_CAT_GRALES_FEST_GRAL, 'Festivos Generales' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_CAT_GRALES_FEST_TURNO, 'Festivos por Turno' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_CAT_GRALES_CONDICIONES, 'Condiciones' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_CAT_GRALES_EVENTOS, 'Eventos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      If ( dmCliente.ModuloAutorizadoDerechos( okAccesos ) ) then
      Begin
           AddElemento( 2, D_CAT_ACCESOS_REGLAS, 'Reglas de Caseta' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
      End;
      AddElemento( 2, D_CAT_GRAL_SEG_GASTOS_MEDICOS, 'Seguros de Gastos M�dicos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_CAT_GRAL_TABLA_AMORTIZACION, 'Tablas de Cotizaci�n SGM' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;

   If ( dmCliente.ModuloAutorizadoDerechos( okCursos ) ) then
   Begin
        AddElemento( 1, D_CAT_CAPACITACION, 'Capacitaci�n' );
           AddElemento( 2, D_CAT_CAPA_TIPO_CURSO, 'Tipo de Curso' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_CLASIFI_CURSO, 'Clasifica Cursos' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_CAPA_CURSOS, 'Cursos' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_CAPA_MAESTROS, 'Maestros' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_CAPA_PROVEEDORES, 'Proveedores de Capacitaci�n' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_CAPA_MATRIZ_CURSO, 'Matriz por Curso' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_CAPA_MATRIZ_PUESTO, 'Matriz por  Puesto' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_CAPA_MATRIZ_CERT_PUESTO, 'Matriz Certificaciones por Puesto' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_CAPA_MATRIZ_PUESTO_CERT, 'Matriz Puestos por Certificaci�n' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_CAPA_CALENDARIO, 'Calendario' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_CAPA_AULAS, 'Aulas' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_PREREQUISITOS_CURSO, 'Prerrequisitos por Curso' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_CERTIFICACIONES, 'Certificaciones' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_ESTABLECIMIENTOS , 'Establecimientos' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
        {$ifndef DOS_CAPAS}
        AddElemento( 1, D_CAT_NODO_COMPETENCIAS, 'Competencias' );
           AddElemento( 2, D_CAT_COMPETENCIAS , 'Competencias' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_CPERFILES , 'Grupos de Competencias' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_MAT_PERFILES_COMP , 'Matriz de Funciones' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           {$endif}
   End;
   AddElemento( 1, D_CAT_CONFIGURACION, 'Configuraci�n' );
      AddElemento( 2, D_CAT_CONFI_GLOBALES, 'Globales de Empresa' );
         AddElemento( 3, D_CAT_CONFI_IDENTIFICA, 'Identificaci�n' );
            AddDerechoConsulta;
            AddDerechoEdicion;
         AddElemento( 3, D_CAT_CONFI_AREAS, 'Areas' );
             AddDerechoConsulta;
             AddDerechoEdicion;
         AddElemento( 3, D_CAT_CONFI_ADICIONALES, 'Campos Adicionales' );
             AddDerechoConsulta;
             AddDerechoEdicion;
         AddElemento( 3, D_CAT_CONFI_RECURSOS_H, 'Recursos Humanos' );
             AddDerechoConsulta;
             AddDerechoEdicion;
         AddElemento( 3, D_CAT_CONFI_NOMINA, 'N�mina' );
             AddDerechoConsulta;
             AddDerechoEdicion;
         If ( dmCliente.ModuloAutorizadoDerechos( okAsistencia ) ) then
         Begin
              AddElemento( 3, D_CAT_CONFI_ASISTENCIA, 'Asistencia' );
                 AddDerechoConsulta;
                 AddDerechoEdicion;
         end;
         AddElemento( 3, D_CAT_CONFI_IMSS_INFO, 'IMSS/INFONAVIT' );
             AddDerechoConsulta;
             AddDerechoEdicion;
         If ( dmCliente.ModuloAutorizadoDerechos( okCursos ) ) then
         Begin
              AddElemento( 3, D_CAT_CONFI_CAPACITACION, 'Capacitaci�n' );
                  AddDerechoConsulta;
                  AddDerechoEdicion;
         end;
         AddElemento( 3, D_CAT_CONFI_VAR_GLOBALES, 'Variables globales' );
             AddDerechoConsulta;
             AddDerechoEdicion;
         AddElemento( 3, D_CAT_CONFI_EMPLEADOS, 'Empleados' );
             AddDerechoConsulta;
             AddDerechoEdicion;
         If ( dmCliente.ModuloAutorizadoDerechos( okCafeteria ) ) then
         Begin
              AddElemento( 3, D_CAT_CONFI_CAFETERIA, 'Cafeter�a' );
                  AddDerechoConsulta;
                  AddDerechoEdicion;
         end;
         If ( dmCliente.ModuloAutorizadoDerechos( okSupervisores ) ) then
         Begin
              AddElemento( 3, D_CAT_CONFI_SUPERVISORES, 'Supervisores' );
                  AddDerechoConsulta;
                  AddDerechoEdicion;
         end;
         If ( dmCliente.ModuloAutorizadoDerechos( okReportesMail ) ) then
         Begin
              AddElemento( 3, D_CAT_CONFI_REP_EMAIL, 'Reportes V�a-Email' );
                  AddDerechoConsulta;
                  AddDerechoEdicion;
         end;
         AddElemento( 3, D_CAT_CONFI_SEGURIDAD, 'Seguridad' );
             AddDerechoConsulta;
             AddDerechoEdicion;
         AddElemento( 3, D_CAT_CONFI_NOTIFICACIONES, 'Notificaciones y Advertencias' );
             AddDerechoConsulta;
             AddDerechoEdicion;
         AddElemento( 3, D_CAT_CONFI_BITACORA, 'Bit�cora' );
             AddDerechoConsulta;
             AddDerechoEdicion;
         If ( dmCliente.ModuloAutorizadoDerechos( okAsistencia ) ) then
         Begin
              AddElemento( 3, D_CAT_CONFI_BLOQ_ASIST, 'Bloqueo de Asistencia' );
                 AddDerechoConsulta;
                 AddDerechoEdicion;
         end;
         AddElemento( 3, D_CAT_CONFI_ENROLL, 'Enrolamiento de Usuarios' );
             AddDerechoConsulta;
             AddDerechoEdicion;
         AddElemento( 3, D_GLOBAL_DISPOSITIVOS, 'Lista de Dispositivos' );
             AddDerechoConsulta;
             AddDerechoEdicion;
         AddElemento( 3, D_GLOBAL_DISPOSITIVOS, 'Solicitudes de env�o de correos' );
             AddDerechoConsulta;
             AddDerechoEdicion;
{$ifndef DOS_CAPAS}
         AddElemento( 3, D_CAT_CONFI_MISDATOS, 'Mis Datos' );
             AddDerechoConsulta;
             AddDerechoEdicion;
{$endif}
         AddElemento( 3, D_CAT_CONFI_NOMINA, 'Timbrado de N�mina' );
             AddDerechoConsulta;
             AddDerechoEdicion;
      AddElemento( 2, D_CAT_CONFI_DICCIONARIO, 'Diccionario de Datos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
   If ( dmCliente.ModuloAutorizadoDerechos( okCafeteria ) ) then
   Begin
        AddElemento( 1, D_CAT_CAFETERIA, 'Cafeter�a' );
           AddElemento( 2, D_CAT_CAFE_REGLAS, 'Reglas' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_CAFE_INVITADORES, 'Invitadores' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
{$ifndef DOS_CAPAS}
              AddDerecho( D_TEXT_NUM_BIOMETRICOS_INV );
              AddDerecho( D_TEXT_ENROLAR_HUELLAS_INV );
              AddDerecho( D_TEXT_BORRAR_HUELLAS_INV );
{$endif}
   end;
   If ( dmCliente.ModuloAutorizadoDerechos( okHerramientas ) ) then
   Begin
        AddElemento( 1, D_CAT_HERRAMIENTAS, 'Herramientas' );
           AddElemento( 2, D_CAT_HERR_TOOLS, 'Herramientas' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_TAB_HERR_TALLAS, 'Tallas' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_TAB_HERR_MOTTOOL, 'Devoluci�n' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
   end;

{ *** Tablas *** }

AddElemento( 0, D_TABLAS   , 'Tablas' );
   AddElemento( 1, D_TAB_PERSONALES, 'Personales' );
      AddElemento( 2, D_TAB_PER_EDOCIVIL, 'Estado Civil' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_PER_VIVE_EN, 'En D�nde Vive' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_PER_VIVE_CON, 'Con Qui�n Vive' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_PER_ESTUDIOS, 'Grado de Estudios' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_PER_TRANSPORTE, 'Transporte' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_PER_ESTADOS, 'Estados del Pa�s' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_PER_MUNICIPIOS, 'Municipios' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_COLONIA, 'Colonias' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
   AddElemento( 1, D_TAB_AREAS, 'Areas' );
      AddElemento( 2, D_TAB_AREAS_NIVEL1, 'Nivel 1' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_AREAS_NIVEL2, 'Nivel 2' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_AREAS_NIVEL3, 'Nivel 3' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_AREAS_NIVEL4, 'Nivel 4' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_AREAS_NIVEL5, 'Nivel 5' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_AREAS_NIVEL6, 'Nivel 6' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_AREAS_NIVEL7, 'Nivel 7' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_AREAS_NIVEL8, 'Nivel 8' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_AREAS_NIVEL9, 'Nivel 9' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      {$ifdef ACS}
      AddElemento( 2, D_TAB_AREAS_NIVEL10, 'Nivel 10' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_AREAS_NIVEL11, 'Nivel 11' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_AREAS_NIVEL12, 'Nivel 12' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      {$endif}
   AddElemento( 1, D_TAB_ADICIONALES, 'Adicionales' );
      AddElemento( 2, D_TAB_ADICION_TABLA1, 'Adicional 1' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_ADICION_TABLA2, 'Adicional 2' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_ADICION_TABLA3, 'Adicional 3' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_ADICION_TABLA4, 'Adicional 4' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_ADICION_TABLA5, 'Adicional 5' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_ADICION_TABLA6, 'Adicional 6' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_ADICION_TABLA7, 'Adicional 7' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_ADICION_TABLA8, 'Adicional 8' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_ADICION_TABLA9, 'Adicional 9' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_ADICION_TABLA10, 'Adicional 10' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_ADICION_TABLA11, 'Adicional 11' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_ADICION_TABLA12, 'Adicional 12' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_ADICION_TABLA13, 'Adicional 13' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_ADICION_TABLA14, 'Adicional 14' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
   AddElemento( 1, D_TAB_HISTORIAL, 'Historial' );
      AddElemento( 2, D_TAB_HIST_TIPO_KARDEX, 'Tipo de Kardex' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_HIST_MOT_BAJA, 'Motivo de Baja' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_HIST_INCIDEN, 'Incidencias' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_HIST_AUT_EXTRAS, 'Motivos de Autorizaci�n' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_HIST_CHECA_MANUAL, 'Motivos Checadas Manuales' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;

      if ( dmCliente.ModuloAutorizadoCosteo ) then
      begin
           AddElemento( 2, D_COSTEO_MOTIVOS_TRANSFER, 'Motivos de Transferencias' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
      end;
   AddElemento( 1, D_TAB_NOMINA, 'N�mina' );
      AddElemento( 2, D_TAB_NOM_TIPO_AHORRO, 'Tipo de Ahorro' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_NOM_TIPO_PRESTA, 'Tipo de Pr�stamo' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_NOM_MONEDAS, 'Monedas' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_NOM_REGLAS_PRESTA, 'Reglas de Pr�stamos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
   AddElemento( 1, D_TAB_OFICIALES, 'Oficiales' );
      AddElemento( 2, D_TAB_OFI_SAL_MINIMOS, 'Salarios M�nimos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_OFI_CUOTAS_IMSS, 'Cuotas de IMSS' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_OFI_ISPT_NUMERICAS, 'ISPT y Num�ricas' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_OFI_GRADOS_RIESGO, 'Grados de Riesgo' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_TAB_OFI_TIPOS_CAMBIO, 'Tipos de Cambio' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
     AddElemento( 2, D_TAB_OFI_STPS, 'STPS' );
         AddElemento( 3, D_TAB_OFI_STPS_CAT_NAC_OCUPA, 'Cat�logo Nacional de Ocupaciones' );
            AddDerechoConsulta;
            AddDerechoAlta;
            AddDerechoBaja;
            AddDerechoEdicion;
            AddDerechoImpresion;
         AddElemento( 3, D_TAB_OFI_STPS_ARE_TEM_CUR, '�reas Tem�ticas de Cursos' );
            AddDerechoConsulta;
            AddDerechoAlta;
            AddDerechoBaja;
            AddDerechoEdicion;
            AddDerechoImpresion;
     AddElemento( 2, D_TAB_OFI_SAT, 'SAT' );
         AddElemento( 3, D_TAB_OFI_SAT_TIPOS_CONCEPTO, 'Tipos de Conceptos' );
            AddDerechoConsulta;
            AddDerechoAlta;
            AddDerechoBaja;
            AddDerechoEdicion;
            AddDerechoImpresion;
         AddElemento( 3, D_TAB_OFI_SAT_TIPO_CONTRATO, 'Tipos de Contratos' );
            AddDerechoConsulta;
            AddDerechoAlta;
            AddDerechoBaja;
            AddDerechoEdicion;
            AddDerechoImpresion;
         AddElemento( 3, D_TAB_OFI_SAT_TIPO_JORNADA, 'Tipos de Jornadas' );
            AddDerechoConsulta;
            AddDerechoAlta;
            AddDerechoBaja;
            AddDerechoEdicion;
            AddDerechoImpresion;
         AddElemento( 3, D_TAB_OFI_SAT_RIESGO_PUESTO, 'Riesgos de Puestos' );
            AddDerechoConsulta;
            AddDerechoAlta;
            AddDerechoBaja;
            AddDerechoEdicion;
            AddDerechoImpresion;

   {$IFNDEF DOS_CAPAS}
     AddElemento( 2, D_CAT_TAB_NAC_COMP, 'Cat�logo Nacional de Competencias' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
     AddElemento( 2, D_TAB_BANCOS, 'Cat�logo de Bancos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
     AddElemento( 2, D_TAB_UMA, 'Unidad de Medida y Actualizaci�n' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;


   AddElemento( 1, D_TAB_CAPACITACION, 'Competencias' );
     AddElemento( 2, D_TAB_TCOMPETENCIAS , 'Tipos de Competencias' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
     AddElemento( 2, D_TAB_TPERFILES , 'Tipos de Grupos de Competencias' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
   {$ENDIF}
   
{ *** Sistema *** }

AddElemento( 0, D_SISTEMA  , 'Sistema' );
   AddElemento( 1, D_SIST_DATOS, 'Datos' );
       AddElemento( 2, D_SIST_BASE_DATOS, 'Bases de Datos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
         AddDerecho( D_EXPORTAR_TABLAS_XML );
         AddDerecho( D_IMPORTAR_TABLAS_XML );
         AddDerecho( D_IMPORTAR_TABLAS_CSV );
      AddElemento( 2, D_SIST_DATOS_EMPRESAS, 'Empresas' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_SIST_DATOS_GRUPOS, 'Grupos de Usuarios' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
         AddDerecho( D_ASIGNA_DERECHOS_OTROS_GRUPOS );
         AddDerecho( D_ASIGNA_DERECHOS_GRUPO_PROPIO );
      AddElemento( 2, D_CAT_ROLES, 'Roles' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_SIST_DATOS_USUARIOS, 'Usuarios' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
         AddDerecho( D_MODIFICAR_USUARIO_PROPIO );
      AddElemento( 2, D_SIST_DATOS_POLL_PENDIENTES, 'Poll Pendientes' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_SIST_DATOS_IMPRESORAS, 'Impresoras' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_SIST_NIVEL0, 'Confidencialidad' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_SIST_BITACORA, 'Bit�cora de Sistema' );
         AddDerechoConsulta;
         AddDerechoImpresion;
      AddElemento( 2, D_SIST_LST_DISPOSITIVOS, 'Lista de Dispositivos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
         AddDerecho('Configuraci�n cafeter�a');
      {$ifndef DOS_CAPAS}
   AddElemento( 1, D_SIST_TERMINALESGTI, 'Terminales GTI' );
      AddElemento( 2, D_SIST_TERMINALES, 'Terminales' ); // SYNERGY
         AddDerechoConsulta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
         AddDerecho( D_TEXT_IMPORTA_TERMINAL );
         AddDerecho( D_TEXT_REINICIA_TERMINAL );
      AddElemento( 2, D_SIST_LISTA_GRUPOS, 'Grupos' ); // SYNERGY
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      {
      //Se pide se comente, pero no elimine por completo:
      AddElemento( 2, D_SIST_MENSAJES, 'Mensajes' ); // SYNERGY
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      }
      AddElemento( 2, D_SIST_TERM_GRUPO, 'Terminales por Grupo' ); // SYNERGY
         AddDerechoConsulta;
         AddDerechoAlta;
      AddElemento( 2, D_SIST_MENSAJES_TERM, 'Mensajes por Terminal' ); // SYNERGY
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_SIST_BITACORA_BIO, 'Bit�cora Terminales GTI' ); // SYNERGY
         AddDerechoConsulta;
         AddDerechoImpresion;
      {$endif}
   AddElemento( 1, D_SIST_REPORTES_EMAIL , 'Reportes email' );
      AddElemento( 2, D_SIST_CALENDARIO_REPORTES, 'Administrador de env�os programados' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
         AddDerecho(D_TEXT_INSCRIBIR_ROLES);
         AddDerecho(D_TEXT_INSCRIBIR_USUARIOS);
      AddElemento( 2, D_SIST_SOLICITUD_ENVIO, 'Solicitudes de env�os programados' );
         AddDerechoConsulta;
         AddDerechoImpresion;
      AddElemento( 2, D_SIST_BITACORA_REPORTES, 'Bit�cora de Reportes Email' );
         AddDerechoConsulta;
         AddDerechoImpresion;
      AddElemento( 2,  D_SIST_SOLICITUD_CORREOS, 'Bit�cora de correos' );
         AddDerechoConsulta;
         AddDerechoImpresion;

   AddElemento( 1, D_SIST_REGISTRO, 'Registro' );
      AddElemento( 2, D_SIST_REG_EMPRESA_NUEVA, 'Empresa Nueva' );

   AddElemento( 1, D_SIST_PROCESOS, 'Procesos' );
      AddElemento( 2, D_SIST_PROC_BORRAR_BAJAS, 'Borrar Bajas' );
      AddElemento( 2, D_SIST_PROC_BORRA_NOMINAS, 'Borrar N�minas' );
      If (dmCliente.ModuloAutorizadoDerechos( okAsistencia ) ) then
      Begin
           AddElemento( 2, D_SIST_PROC_BORRAR_TARJETAS, 'Borrar Tarjetas' );
           AddElemento( 2, D_SIST_BORRAR_POLL, 'Borrar POLL' );
      end;
      AddElemento( 2, D_SIST_BORRAR_BITACORA, 'Borrar Bit�cora' );
      If ( dmCliente.ModuloAutorizadoDerechos( okHerramientas ) ) then
        AddElemento( 2, D_SIST_BORRAR_HERRAMIENTA, 'Borrar Herramienta' );
      AddElemento( 2, D_SIST_PROC_BORRA_TIMBRADO_NOMINAS, 'Borrar Timbrado' );        
      AddElemento( 2, D_SIST_PROC_NUMERO_TARJETA, 'Actualizar N�mero de Tarjeta' );
      AddElemento( 2, D_SIST_PROC_ENROLAMIENTO_MASIVO, 'Enrolamiento Masivo' );
      AddElemento( 2, D_SIST_PROC_IMP_ENROLAMIENTO_MAS, 'Importar Enrolamiento Masivo' );
      AddElemento( 2, D_SIST_IMPORTAR_CAFETERIA, 'Copiar Configuraci�n' );
      {$ifndef DOS_CAPAS}
      AddElemento( 2, D_SIST_PROC_ASIGNA_NUM_BIO, 'Asignaci�n de N�meros Biom�tricos' );
      AddElemento( 2, D_SIST_PROC_ASIGNA_GRUPO_TERM, 'Asignaci�n de Grupo de Terminales' );
      AddElemento( 2, D_SIST_PROC_DEPURA_BIT_BIO, 'Borrar Bit�cora Terminales GTI' );
      {$endif}
      AddElemento( 2, D_SIST_PROC_BD_TRESS, 'Agregar BD de tipo Tress' );
      AddElemento( 2, D_SIST_PROC_BD_PRUEBAS, 'Agregar BD de tipo Pruebas' );
      AddElemento( 2, D_SIST_PROC_BD_SELECCION, 'Agregar BD de tipo Selecci�n' );
      AddElemento( 2, D_SIST_PROC_BD_VISITANTES, 'Agregar BD de tipo Visitantes' );
      AddElemento( 2, D_SIST_PROC_BD_PRESUPUESTOS, 'Agregar BD de tipo Presupuestos' );
      AddElemento( 2, D_SIST_ACTUALIZAR_BDS , 'Actualizaci�n de Base de Datos' );
   AddElemento( 1, D_SIST_PRENDE_USUARIOS, 'Activar Usuarios' );
   AddElemento( 1, D_SIST_APAGA_USUARIOS, 'Bloquear Usuarios' );
   If ( dmCliente.ModuloAutorizadoDerechos( okSupervisores ) or (Autorizacion.Plataforma = TPlataforma( 0 )  )   ) then
      AddElemento( 1, D_SIST_ASIGNAR_SUPER, 'Asignar Supervisores' );
   If ( dmCliente.ModuloAutorizadoDerechos( okLabor ) ) then
      AddElemento( 1, D_SIST_ASIGNAR_AREAS, 'Asignar Areas' );
{$ifdef TECMILENIO}
AddElemento( 0, D_MOD_EXCEPCIONES, 'M�dulo Env�o de Excepciones' );
   AddElemento( 1, D_ADMON_EXCEPCIONES, 'Administraci�n' );
      AddElemento( 2, D_EXCEPCIONES, 'Excepciones' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_MOTIVOS_PAGO, 'Motivos de pago' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
   AddElemento( 1, D_PROCESOS_EXCEPCIONES, 'Procesos' );
      AddElemento( 2, D_ENVIO_EXCEPCIONES, 'Env�o de Excepciones' );
{$endif}

{ *** Supervisores *** }
If ( dmCliente.ModuloAutorizadoDerechos( okSupervisores ) ) then
Begin
     AddElemento( 0, 163, 'Supervisores' );
        AddElemento( 1, D_SUPER_AJUSTAR_ASISTENCIA, 'Ajustar Asistencia' );
           AddDerechoConsulta;
           AddDerechoAlta;
           AddDerechoBaja;
           AddDerechoEdicion;
           AddDerechoImpresion;
        AddElemento( 1, D_SUPER_AJUSTAR_ASISTENCIA_ESPECIAL, 'Autorizaciones de Asistencia' );
           AddDerecho( 'Autorizar Horas Extras' );
           AddDerecho( 'Autorizar Descanso Trabajado' );
           AddDerecho( 'Autorizar Permiso con Goce' );
           AddDerecho( 'Autorizar Permiso sin Goce' );
           AddDerecho( 'Cambiar Tipo de D�a' );
           AddDerecho( 'Cambiar Horario' );
           AddDerecho( 'Autorizar Descuento de Comida' );
           AddDerecho( 'Aprobar Autorizaciones' );
           AddDerecho( 'Autorizar horas prepagadas');
        AddElemento( 1, D_SUPER_PERMISOS, 'Permisos en D�as' );
           AddDerechoConsulta;
           AddDerechoAlta;
           AddDerechoBaja;
           AddDerechoEdicion;
           AddDerechoImpresion;
        AddElemento( 1, D_SUPER_TIPO_PERMISO, 'Clase de Permisos' );
           AddDerecho( 'Con Goce' );
           AddDerecho( 'Falta Justificada' );
           AddDerecho( 'Suspensi�n' );
           AddDerecho( 'Otros' );
        AddElemento( 1, D_SUPER_KARDEX, 'Kardex de Empleados' );
           AddDerechoConsulta;
           AddDerechoAlta;
           AddDerechoBaja;
           AddDerechoEdicion;
           AddDerechoImpresion;
        AddElemento( 1, D_SUPER_VACACIONES, 'Vacaciones' );
           AddDerechoConsulta;
           AddDerechoAlta;
           AddDerechoBaja;
           AddDerechoEdicion;
           AddDerechoImpresion;
           AddDerecho( 'Registrar Pago De Vacaciones' );
           AddDerecho( 'Registrar Vacaciones Pasadas' );
           AddDerecho( 'Exceder Saldos De Vacaciones Gozadas' );
           AddDerecho( 'Exceder Saldos De Vacaciones Pagadas' );
           AddDerecho( 'Exceder Saldos De Prima Vacacional' );
        AddElemento( 1, D_SUPER_CLASIFI, 'Clasificaciones' );
           AddElemento( 2, D_SUPER_CLASIFI_TEMP, 'Temporales' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_SUPER_CLASIFI_TEMP_NIVELES, 'Tipos' );
              AddDerecho( 'Puesto' );
              AddDerecho( 'Clasificaci�n' );
              AddDerecho( 'Turno' );
              AddDerecho( 'Nivel 1' );
              AddDerecho( 'Nivel 2' );
              AddDerecho( 'Nivel 3' );
              AddDerecho( 'Nivel 4' );
              AddDerecho( 'Nivel 5' );
              AddDerecho( 'Nivel 6' );
              AddDerecho( 'Nivel 7' );
              AddDerecho( 'Nivel 8' );
              AddDerecho( 'Nivel 9' );
              {$ifdef ACS}
              AddDerecho( 'Nivel 10' );
              AddDerecho( 'Nivel 11' );
              AddDerecho( 'Nivel 12' );
              {$endif}
        AddElemento( 1, D_SUPER_CONSULTAR_PRENOMINA, 'Consultar Pre-N�mina' );
           AddDerechoConsulta;
           AddDerechoImpresion;
        AddElemento( 1, D_SUPER_BITACORA, 'Bit�cora' );
           AddDerechoConsulta;
           AddDerechoImpresion;
        AddElemento( 1, D_SUPER_ASIGNAR_EMPLEADOS, 'Asignar Empleados' );
           AddDerechoConsulta;
           AddDerechoEdicion;
           AddDerechoImpresion;
        AddElemento( 1, D_SUPER_DATOS_EMPLEADOS, 'Datos del Empleado' );
           AddDerechoConsulta;
           AddDerechoImpresion;
        AddElemento(1, D_SUPER_CURSOS_TOMADOS, 'Cursos Tomados');
             AddDerechoConsulta;
             AddDerechoAlta;
             AddDerechoBaja;
             AddDerechoEdicion;
             AddDerechoImpresion;
        AddElemento(1, D_SUPER_CURSOS_PROGRAMADOS, 'Cursos Programados');
             AddDerechoConsulta;
             AddDerechoImpresion;
        AddElemento(1, D_SUPER_GRUPOS, 'Grupos');
             AddDerechoConsulta;
             AddDerechoAlta;
             AddDerechoBaja;
             AddDerechoEdicion;
             AddDerechoImpresion;
             AddElemento(2,D_SUPER_SESIONES_OTROS,'Grupos de otros supervisores');
                AddDerechoConsulta;
                AddDerechoBaja;
                AddDerechoEdicion;
                AddDerechoImpresion;
        AddElemento( 1, D_SUPER_TRANSFERENCIAS, 'Transferencias');
           AddDerechoConsulta;
           AddDerechoAlta;
           AddDerechoBaja;
           AddDerechoEdicion;
           AddDerechoImpresion;
        AddElemento( 1, D_LAY_PRODUCCION, 'Plantillas de Producci�n');
           AddDerechoConsulta;
           AddDerecho( D_TEXT_ASIGNAR );
        If ( dmCliente.ModuloAutorizadoDerechos( okLabor ) ) then
        Begin
             AddElemento( 1, D_SUPER_LABOR, 'Labor' );
                AddDerechoConsulta;
                AddDerechoAlta;
                AddDerechoBaja;
                AddDerechoEdicion;
                AddDerechoImpresion;
             AddElemento( 1, D_SUPER_CEDULAS, 'C�dulas' );
                AddDerechoConsulta;
                AddDerechoAlta;
                AddDerechoBaja;
                AddDerechoEdicion;
                AddDerechoImpresion;
                AddDerecho( 'Editar Lista de Empleados' );
                AddDerecho( 'Capturar C�dulas de Producci�n' );
                AddDerecho( 'Capturar C�dulas de Tiempo Muerto' );
                AddDerecho( 'Capturar C�dulas de Horas Especiales' );
                AddDerecho( 'Capturar Datos Generales de la C�dula' );
                AddDerecho( 'Capturar Moduladores de la C�dula' );
             AddElemento( 1, D_SUPER_ASIGNAR_AREAS, 'Asignar Area de Empleados' );
                AddDerechoConsulta;
                AddDerechoAlta;
                AddDerechoBaja;
                AddDerechoEdicion;
                AddDerechoImpresion;
                AddDerecho( 'Importar Empleados' );
                AddDerecho( 'Cambiar Area Con Regreso a Primera Hora' );
                AddDerecho( 'Cambiar Area Con Regreso a Fecha y Hora Especifica' );
        end;
        AddElemento( 1, D_SUPER_TIPO_KARDEX, 'Tipo de Kardex' );
           AddDerecho( 'Kardex General' );
           AddDerecho( 'Cambio de Puesto' );
           AddDerecho( 'Cambio de Turno' );
           AddDerecho( 'Cambio de Area' );
        AddElemento( 1, 261, 'Procesos' );
           AddElemento( 2, D_SUPER_AUTORIZACION_COLECTIVA, 'Autorizaci�n Colectiva' );
           AddDerecho('Ejecutar');
           AddDerecho('Mostrar autorizaciones que no puede modificar');
           If ( dmCliente.ModuloAutorizadoDerechos( okLabor ) ) then
           Begin
                AddElemento( 2, D_SUPER_REGISTRO_BREAKS, 'Registro de Breaks' );
                AddElemento( 2, D_SUPER_CANCELA_BREAKS, 'Cancelaci�n de Breaks' );
           end;
           AddElemento( 2, D_SUPER_CAMBIO_TURNO, 'Cambio Masivo de Turnos' );
           AddElemento( 2, D_SUPER_AUTORIZAR_PRENOMINA, 'Autorizaci�n de Pre-N�mina' );
              AddDerecho('Autorizar Pre-N�mina');
              AddDerecho('Desautorizar Pre-N�mina');
              AddDerecho('Consultar');
           AddElemento( 2, D_SUPER_APROBAR_TRANSFERENCIAS, 'Aprobar/Cancelar Transferencias' );
		   {$ifdef COMMSCOPE}
           AddElemento( 2, D_SUPER_AUTORIZAR_EVALUACION, 'Evaluaci�n Diaria' );
              AddDerecho('Consultar');
              AddDerecho('Evaluar');
              AddDerecho('Modificar');
           {$endif}
        AddElemento( 1, D_REPORTES_SUPERVISORES, 'Reportes' );
           AddDerechoConsulta;
           AddDerechoAlta;
           AddDerechoBaja;
           AddDerechoEdicion;
           AddDerechoImpresion;
        AddElemento( 1, D_SUPER_ACTIVA_CACHE, 'Archivos Temporales' );
        AddElemento( 1, D_SUPER_VER_BAJAS, 'Ver Empleados Dados de BAJA' );
        AddElemento( 1, D_SUPER_VER_COLABORA, 'Ver Todos Los Empleados' );
        AddElemento( 1, D_SUPER_PLANVACACION, 'Solicitud de vacaciones');
           AddDerechoConsulta;
           AddDerechoAlta;
           AddDerechoBaja;
           AddDerechoEdicion;
           AddDerechoImpresion;
           AddDerecho( D_TEXT_AUTORIZAR );
           AddDerecho( D_TEXT_ESPEC_PER_PAGO );
        AddElemento( 1, D_SUPER_STATUS_EMPLEADO, 'Ver Status de Empleado' );
      {$IFNDEF DOS_CAPAS }
       if ( dmCliente.ModuloAutorizadoDerechos( okCursos) ) then
       begin
            AddElemento( 1, D_SUP_PLAN_CAPACITA , 'Plan de Capacitaci�n');
               AddDerechoConsulta;
               AddDerechoAlta;
               AddDerechoBaja;
               AddDerechoEdicion;
               AddDerechoImpresion;
            AddElemento( 1, D_SUP_EVAL_COMPETEN , 'Competencias Evaluadas');
               AddDerechoConsulta;
               AddDerechoAlta;
               AddDerechoBaja;
               AddDerechoEdicion;
               AddDerechoImpresion;
            AddElemento( 1, D_SUP_MAT_CURSOS , 'Plan de Cursos Programados' );
               AddDerechoConsulta;
               AddDerechoImpresion;
            AddElemento( 1, D_SUP_MAT_HBLDS , 'Matriz de Habilidades');
               AddDerechoConsulta;
               AddDerechoImpresion;
       end;
      {$endif}
end;
{ *** Labor *** }
If ( dmCliente.ModuloAutorizadoDerechos( okLabor ) ) then
Begin
     AddElemento( 0, D_LABOR    , 'Labor' );
        AddElemento( 1, D_LAB_EMPLEADOS, 'Empleados' );
           AddElemento( 2, D_LAB_EMP_DATOS, 'Datos' );
              AddDerechoConsulta;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_EMP_OPERACIONES, 'Operaciones' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_EMP_ORDENES_FIJAS, 'Ordenes Fijas' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_EMP_KARDEX_AREAS, 'Kardex de Areas' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
        AddElemento( 1, D_LAB_LABOR, 'Labor' );
           AddElemento( 2, D_LAB_WORK_ORDER, 'Orden de Trabajo' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_KARDEX_ORDER, 'Kardex de Orden de Trabajo' );
              AddDerechoConsulta;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_HISTORIA_ORDENES, 'Historial de Orden de Trabajo' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_CEDULAS, 'C�dulas' );
              AddDerechoConsulta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
              AddDerecho( 'Editar Lista de Empleados' );
              AddDerecho( 'Capturar C�dulas de Producci�n' );
              AddDerecho( 'Capturar C�dulas de Tiempo Muerto' );
              AddDerecho( 'Capturar C�dulas de Horas Especiales' );
              AddDerecho( 'Capturar Datos Generales de la C�dula' );
              AddDerecho( 'Capturar Moduladores de la C�dula' );
           AddElemento( 2, D_LAB_REGISTRO, 'Registro' );
              AddElemento( 3, D_LAB_REG_MULTILOTE, 'Multi-Lote' );
           AddElemento( 2, D_LAB_PROCESO, 'Procesos' );
              AddElemento( 3, D_LAB_PROC_CALC_TIEMPOS, 'C�lculo de Tiempos' );
              AddElemento( 3, D_LAB_PROC_AFECTARLABOR, 'Afectar Labor' );
              AddElemento( 3, D_LAB_PROC_IMP_ORDENES, 'Importar Ordenes de Trabajo' );
              AddElemento( 3, D_LAB_PROC_IMP_PARTES, 'Importar Partes' );
              AddElemento( 3, D_LAB_PROC_IMP_COMPONENTES, 'Importar Componentes' );
              AddElemento( 3, D_LAB_PROC_IMP_CEDULAS, 'Importar C�dulas' );
              AddElemento( 3, D_LAB_PROC_IMP_CAMBIO_AREAS, 'Importar Cambio de Area' );
              AddElemento( 3, D_LAB_PROC_IMP_PROD_IND, 'Importar Producci�n Individual' );
              AddElemento( 3, D_LAB_PROC_DEPURACION, 'Depuraci�n' );
           AddElemento( 2, D_LAB_PLANTILLAS , 'Plantillas Producci�n' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
        AddElemento( 1, D_LAB_CALIDAD, 'Calidad' );
           AddElemento( 2, D_LAB_CEDULAS_INSP, 'C�dulas de Inspecci�n' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
              AddDerecho( 'Editar C�dulas de Otros Inspectores' );
           AddElemento( 2, D_LAB_CEDULAS_SCRAP, 'C�dulas de Scrap' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
              AddDerecho( 'Editar C�dulas de Otros Inspectores' );
           AddElemento( 2, D_LAB_CAT_TDEFECTO, 'Clase de defecto' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_CAT_COMPONENTES, 'Componentes' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_CAT_MOT_SCRAP, 'Motivos de Scrap' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
        AddElemento( 1, D_LAB_CONSULTAS, 'Consultas' );
           AddElemento( 2, D_REPORTES_LABOR, 'Reportes' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_CONS_SQL, 'SQL' );
              AddDerechoConsulta;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_CONS_PROCESOS, 'Bit�cora de Procesos' );
              AddDerechoConsulta;
              AddDerecho( 'Cancelar Procesos' );
              AddDerechoImpresion;
        AddElemento( 1, D_LAB_CATALOGOS, 'Cat�logos' );
           AddElemento( 2, D_LAB_CAT_PARTES, 'Partes' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_CAT_OPERACIONES, 'Operaciones' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_CAT_TIPO_PARTE, 'Clase de Partes' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_CAT_TIPO_OPERACION, 'Clase de Operaciones' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_CAT_AREA, 'Areas' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_CAT_MAQUINAS, 'M�quinas' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_CAT_TMAQUINAS, 'Clase de M�quinas' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_CAT_TIEMPO_MUERTO, 'Tiempo Muerto' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_CAT_BREAKS, 'Breaks' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_CAT_MODULA_1, 'Modulador #1' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_CAT_MODULA_2, 'Modulador #2' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_CAT_MODULA_3, 'Modulador #3' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_CAT_GLOBAL, 'Globales' );
              AddDerechoConsulta;
              AddDerechoEdicion;
              AddDerechoImpresion;
end
else
if( dmCliente.ModuloAutorizadoDerechos( okSupervisores ) ) then
begin
      AddElemento( 0, D_LABOR    , 'Labor' );
        AddElemento( 1, D_LAB_LABOR, 'Labor' );
          AddElemento( 2, D_LAB_PLANTILLAS , 'Plantillas de Producci�n' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
        AddElemento( 1, D_LAB_CATALOGOS, 'Cat�logos' );
           AddElemento( 2, D_LAB_CAT_AREA, 'Areas' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_CAT_MAQUINAS, 'M�quinas' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_LAB_CAT_TMAQUINAS, 'Clase de M�quinas' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
		   AddElemento( 2, D_LAB_CAT_GLOBAL, 'Globales' );
              AddDerechoConsulta;
              AddDerechoEdicion;
              AddDerechoImpresion;
end;
{ *** Servicio M�dico *** }
If ( dmCliente.ModuloAutorizadoDerechos( okEnfermeria ) ) then
Begin
     AddElemento( 0, D_MODULO_MEDICO, 'Servicio M�dico' );
        AddElemento( 1, D_EMP_SERVICIO_MEDICO, 'Pacientes' );
           AddElemento( 2, D_EMP_SERV_EXPEDIENTE, 'Expediente' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
              AddDerecho( 'Datos de Contrataci�n' );
           AddElemento( 2, D_EMP_KARDEX_CONSULTAS, 'Kardex de Consultas' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_EMP_KARDEX_EMBARAZOS, 'Kardex de Embarazos' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_MEDICINA_ENTR, 'Medicina Entregada' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_EMP_KARDEX_ACCIDENTES, 'Kardex de Accidentes/Enfermedades de Trabajo' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_EMP_INCAPACIDADES, 'Kardex de Incapacidades - Permisos' );
              AddElemento( 3, D_PACIENTE_INCAPACIDADES, 'Incapacidades' );
     //            AddDerechoConsulta;
                 AddDerechoAlta;
                 AddDerechoBaja;
                 AddDerechoEdicion;
                 AddDerechoImpresion;
              AddElemento( 3, D_PACIENTE_PERMISOS, 'Permisos' );
     //            AddDerechoConsulta;
                 AddDerechoAlta;
                 AddDerechoBaja;
                 AddDerechoEdicion;
                 AddDerechoImpresion;
           AddElemento( 2, D_EMP_CLASE_PERMISO, 'Clases de Permisos' );
              AddDerecho( 'Con Goce' );
              AddDerecho( 'Sin Goce' );
           AddElemento( 2, D_SERV_REGISTRO, 'Registro' );
              AddElemento( 3, D_REG_CONS_MED_GLOBAL, 'Consulta M�dica Global' );
        AddElemento( 1, D_SERV_CONSULTAS, 'Consultas Del Sistema' );
           AddElemento( 2, D_REPORTES_MEDICOS, 'Reportes' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CONSULTA_SQL, 'SQL' );
              AddDerechoConsulta;
              AddDerechoImpresion;
           AddElemento( 2, D_CONSULTA_PROCESOS, 'Bit�cora de Procesos' );
              AddDerechoConsulta;
              AddDerecho( 'Cancelar Procesos' );
              AddDerechoImpresion;
        AddElemento( 1, D_SERV_CATALOGOS, 'Cat�logos' );
           AddElemento( 2, D_CAT_MEDICAMENTOS, 'Medicamentos' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_DIAGNOSTICO, 'Diagn�sticos' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_TIPO_CONSULTA, 'Tipos de Consulta' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_TIPO_EST_LAB, 'Tipos de Estudio de Laboratorio' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_TIPO_ACC, 'Tipos de Accidente' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_CAU_ACC, 'Causas de Accidente' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_MOT_ACC, 'Motivos de Accidente' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
        AddElemento( 1, D_SERV_SISTEMA, 'Sistema' );
           AddElemento( 2, D_SERV_GLOBALES, 'Globales de Servicios M�dicos' );
              AddDerechoConsulta;
              AddDerechoEdicion;
              AddDerechoImpresion;
end;

If ( dmCliente.ModuloAutorizadoDerechos( okPlanCarrera ) ) then
Begin
     { *** Plan Carrera Admin. *** }

     AddElemento( 0, D_CARRERA, 'Plan Carrera Admin.' );
        AddElemento( 1, D_CARRERA_CATALOGOS, 'Cat�logos' );
           AddElemento( 2, D_CARRERA_CAT_TCOMPETEN, 'Tipo de Competencias' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CARRERA_CAT_CALIFICA, 'Calificaciones' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CARRERA_CAT_COMPETEN, 'Competencias / Habilidades' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CARRERA_CAT_ACCION, 'Acciones' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CARRERA_CAT_PUESTO, 'Puestos' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CARRERA_CAT_FAMILIA, 'Familias' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CARRERA_CAT_NIVEL, 'Niveles' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CARRERA_CAT_DIMENSIO, 'Dimensiones' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
        AddElemento( 1, D_CARRERA_CONSULTAS, 'Consultas' );
           AddElemento( 2, D_REPORTES_CARRERA, 'Reportes' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
        AddElemento( 1, D_CARRERA_SISTEMA, 'Sistema' );
           AddElemento( 2, D_CARRERA_GLOBALES, 'Globales de Plan Carrera' );
              AddDerechoConsulta;
              AddDerechoEdicion;
              AddDerechoImpresion;

     { *** Plan Carrera *** }

     AddElemento( 0, D_CARRERA_WEB, 'Plan Carrera' );
        AddElemento( 1, D_CARRERA_CONSULTAS_WEB, 'Mi Plan Carrera' );
           AddDerecho( 'Competencias Vs. Mi Puesto' );
           AddDerecho( 'Mis Competencias' );
           AddDerecho( 'Mi Plan de Acci�n' );
           AddDerecho( 'Mis Cursos' );
           AddDerecho( 'Mis Datos de Empleado' );
        AddElemento( 1, D_CARRERA_PUESTOS_WEB, 'Puestos' );
           AddDerecho( 'Por Familia' );
           AddDerecho( 'Por Nivel' );
           AddDerecho( 'Por Dimensi�n' );
           AddDerecho( 'Matriz de Entrenamiento' );
        AddElemento( 1, D_CARRERA_CATALOGOS_WEB, 'Cat�logos' );
           AddDerecho( 'Competencias' );
           AddDerecho( 'Acciones' );
           AddDerecho( 'Cursos' );
           AddDerecho( 'Matriz de Entrenamiento por Curso' );
        AddElemento( 1, D_CARRERA_DETALLE_CATALOGOS, 'Detalle de Cat�logos' );
           AddDerecho( 'Puesto' );
           AddDerecho( 'Competencia' );
           AddDerecho( 'Acci�n' );
           AddDerecho( 'Curso' );
           AddDerecho( 'Maestro' );
        AddElemento( 1, D_CARRERA_ANALITICOS_WEB, 'Anal�ticos' );
           AddDerecho( 'Comparativo de Puestos' );
           AddDerecho( 'B�squeda de Candidatos' );
           AddDerecho( 'Desempe�o por Competencia Totales ' );
           AddDerecho( 'Desempe�o por Competencia Detalle' );
           AddDerecho( 'Cumplimiento de Compromisos Totales' );
           AddDerecho( 'Cumplimiento de Compromisos Detalle' );
           AddDerecho( 'Cursos Programados Totales' );
           AddDerecho( 'Cursos Programados Detalle' );
        AddElemento( 1, D_CARRERA_MIS_COLABORADORES, 'Mis Colaboradores' );
           AddDerecho( 'Calificar Competencias' );
           AddDerecho( 'Asignar Plan de Acci�n' );
           AddDerecho( 'Agregar Competencias' );
end;

{ *** Evaluaci�n Admin. *** }
If dmCliente.ModuloAutorizadoDerechos( okEvaluacion ) then
Begin
     AddElemento( 0, D_EVALUACION, 'Administrador Evaluaci�n/Encuestas' );
        AddElemento( 1, D_EVAL_DISENO_ENCUESTA, 'Dise�o de Encuesta' );
           AddElemento( 2, D_CRITERIOS_EVAL, 'Criterios a evaluar' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
              AddDerecho( 'Agregar competencias' );
           AddElemento( 2, D_PREG_CRITERIO, 'Preguntas por criterio' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_ESCALAS, 'Escalas de Encuesta' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
              AddDerecho( 'Copiar escalas' );
           AddElemento( 2, D_PERFIL_EVAL, 'Perfil de evaluadores' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_EMPLEADOS_EVAL, 'Empleados a evaluar' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
        AddElemento( 1, D_DISENO_REG, 'Registro' );
           AddElemento( 2, D_AGREGAR_EMP_EVAL, 'Agregar empleados a evaluar' );
        AddElemento( 1, D_DISENO_PROC, 'Procesos' );
           AddElemento( 2, D_GENERAR_LISTA, 'Generar lista de empleados a evaluar' );
           AddElemento( 2, D_PREPARA_EVAL, 'Preparar Evaluaciones' );
           AddElemento( 2, D_CALCULA_RESUL, 'Calcular resultados' );
           AddElemento( 2, D_RECALCULA_PROM, 'Recalcular promedios' );
           AddElemento( 2, D_CORREOS_INVITACION, 'Envia correos de invitaci�n' );
           AddElemento( 2, D_CORREOS_NOTIFICACION, 'Envia correos de notificaci�n' );
           AddElemento( 2, D_ASIGNA_RELACIONES, 'Asignaci�n de relaciones' );
           AddElemento( 2, D_CREA_ENCUESTA_SIMPLE, 'Crear encuesta simple' );
        AddElemento( 1, D_EVAL_RESULTADOS_ENC, 'Resultados de Encuesta' );
           AddElemento( 2, D_TOTALES_ENCUESTA, 'Totales de Encuesta' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_EMPLEADOS_EVALUADOS, 'Empleados evaluados' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_EVALUADORES, 'Evaluadores' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_EVALUACIONES, 'Evaluaciones' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_ANALISIS_CRITERIO, 'An�lisis por criterio' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_ANALISIS_PREGUNTA, 'An�lisis por pregunta' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_FRECUENCIA_RESP, 'Frecuencia de respuestas' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
        AddElemento( 1, D_EVA_RESULTADOS_INDIV, 'Resultados individuales' );
           AddElemento( 2, D_RESULTADO_CRITERIO, 'Resultados por criterio' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_RESULTADO_PREGUNTA, 'Resultados por pregunta' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
        AddElemento( 1, D_EVALUACION_CONSULTAS, 'Consultas' );
           AddElemento( 2, D_REPORTES_EVALUACION, 'Reportes' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_EVAL_CONSULTAS, 'SQL' );
              AddDerechoConsulta;
              AddDerechoImpresion;
           AddElemento( 2, D_EVAL_PROCESOS, 'Bit�cora de Procesos' );
              AddDerechoConsulta;
              AddDerecho( 'Cancelar Procesos' );
              AddDerechoImpresion;
           AddElemento( 2, D_EVAL_CORREOS, 'Correos' );
                AddDerechoConsulta;
                AddDerechoEdicion;
                AddDerechoImpresion;
        AddElemento( 1, D_EVALUACION_CATALOGOS, 'Cat�logos' );
           AddElemento( 2, D_CAT_EVAL_TCOMPETEN, 'Tipos de competencia' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_EVAL_COMPETEN, 'Competencias' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
           AddElemento( 2, D_CAT_EVAL_PREGUNTAS, 'Preguntas' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
            AddElemento( 2, D_CAT_EVAL_ESCALAS, 'Escalas' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
            AddElemento( 2, D_ENCUESTAS, 'Encuestas' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
              AddDerecho( 'Copiar Encuestas' );
          AddElemento( 1, D_EVALUACION_SISTEMA, 'Sistema' );
            AddElemento( 2, D_EVAL_REPORTA, 'Usuarios' );
                AddDerechoConsulta;
                AddDerechoEdicion;
                AddDerechoImpresion;
            AddElemento( 2, D_GLOBALES_EVALUACION , 'Globales de Empresa' );
              AddDerechoConsulta;
              AddDerechoEdicion;
              AddDerechoImpresion;
            AddElemento( 2, D_EVAL_DICCIONARIO, 'Diccionario de Datos' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
end;
        //Caja de ahorro

        AddElemento( 0, D_CAJA_AHORRO, 'Caja de Ahorro' );
          AddElemento( 1, D_AHORRO_ADMON, 'Administraci�n' );
            AddElemento( 2, D_AHORRO_SALDOS_X_EMP, 'Saldos de Empleado' );
              AddDerechoConsulta;
              AddDerechoEdicion;
              AddDerechoImpresion;
            AddElemento( 2, D_AHORRO_TOT_CAJA_FONDO, 'Totales de Caja de Ahorro' );
              AddDerechoConsulta;
              AddDerechoImpresion;
            AddElemento( 2, D_AHORRO_EDO_CUENTA_BANC, 'Estado de Cuenta Bancaria' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
          AddElemento( 1, D_AHORRO_ADMON_REGISTRO , 'Registro' );
            AddElemento( 2, D_AHORRO_REG_INSCRIP_EMP, 'Inscribir Empleado' );
            AddElemento( 2, D_AHORRO_REG_LIQ_RET, 'Liquidaci�n/Retiro' );
          AddElemento( 1, D_AHORRO_ADMON_PROCESO, 'Procesos' );
            AddElemento( 2, D_AHORRO_DEPOSITAR_RET, 'Depositar Retenciones' );
          AddElemento( 1, D_AHORRO_CONSULTAS, 'Consultas' );
            AddElemento( 2, D_REPORTES_CAJAAHORRO, 'Reportes' );
              AddDerechoConsulta;
              {$ifdef DOS_CAPAS}
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
              {$endif}
            AddElemento( 2, D_AHORRO_SQL, 'SQL' );
              AddDerechoConsulta;
              AddDerechoImpresion;
            AddElemento( 2, D_AHORRO_BITACORA, 'Bit�cora / Procesos' );
              AddDerechoConsulta;
              AddDerecho( 'Cancelar Procesos');
              AddDerechoImpresion;
          AddElemento( 1, D_AHORRO_CATALOGOS, 'Cat�logos' );
            AddElemento( 2, D_AHORRO_CAT_CUENTAS_BANC, 'Cuentas Bancarias' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
            AddElemento( 2, D_AHORRO_CAT_TIPO_DEPOSITO, 'Tipo de Dep�sito/Retiro' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;

        //Presupuestos

      {$IFNDEF DOS_CAPAS}
      AddElemento( 0, D_PRESUPUESTO, 'Presupuestos de n�mina' );
          AddElemento( 1, D_PRESUP_CONFI, 'Configuraci�n' );
            AddElemento( 2, D_PRESUP_CONTRATACIONES, 'Contrataciones' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
              // (JB) Escenarios de presupuestos T1060 CR1872
              AddElemento( 2, D_PRESUP_ESCENARIOS, 'Escenarios' ); 
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
            AddElemento( 2, D_PRESUP_SUPUESTO_PER, 'Supuestos de personal' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
          AddElemento( 1, D_PRESUP_PROC, 'Procesos' );
           AddElemento( 2, D_PRESUP_PROC_SIMULA, 'Simulaci�n de presupuestos');
           AddElemento( 2, D_PRESUP_PROC_IMPRIME, 'Imprimir simulaci�n');
           AddElemento( 2, D_PRESUP_IMP_KARDEX, 'Importar Kardex ');
           AddElemento( 2, D_PRESUP_PROC_GEN_POLIZA, 'Generar P�liza Contable');
           AddElemento( 2, D_PRESUP_PROC_IMP_POLIZA, 'Imprimir P�liza Contable');
           AddElemento( 2, D_PRESUP_PROC_EXP_POLIZA, 'Exportar P�liza Contable');

      {$ENDIF}
      AddElemento( 0, D_TIMBRADO, 'Timbrado de N�mina' );
          AddElemento( 1, D_TIMBRADO_NOMINA, 'Consultas' );
            AddElemento( 2, D_TIM_DATOS_CUENTAS, 'Cuentas de timbrado' );
              AddDerechoConsulta;
              AddDerechoAlta;
              AddDerechoBaja;
              AddDerechoEdicion;
              AddDerechoImpresion;
            AddElemento( 2, D_TIM_DATOS_SALDO, 'Estado de cuenta de timbrado' );
              AddDerechoConsulta;
              AddDerechoImpresion;
            AddElemento( 2, D_TIM_DATOS_CONTRIBUYENTES, 'Contribuyentes' );
              AddDerechoConsulta;
              AddDerechoImpresion;
            AddElemento( 2, D_TIM_DATOS_PERIODOS, 'N�minas a timbrar' );
              AddDerechoConsulta;
              AddDerechoImpresion;
          AddElemento( 1, D_TIM_PROCESOS, 'Procesos' );
           AddElemento( 2, D_TIM_REG_REGISTRAR_CONTRIBUYENTE, 'Actualizar contribuyentes');
           AddElemento( 2, D_TIM_REG_CANCELAR_CONTRIBUYENTE, 'Cancelar contribuyente');
           AddElemento( 2, D_TIM_PROC_TIMBRAR_NOMINA, 'Timbrar n�mina');
           AddElemento( 2, D_TIM_PROC_CANCELAR_TIMBRADO, 'Cancelar timbrado');
           AddElemento( 2, D_TIM_PROC_TOMAR_RECIBOS, 'Tomar recibos');
           AddElemento( 2, D_TIM_PROC_TOMAR_RECIBOS_EMPLEADO, 'Tomar recibos por empleado');
           AddElemento( 2, D_TIM_PROC_ENVIAR_RECIBOS, 'Enviar recibos');
           AddElemento( 2, D_TIM_PROC_ADQUIRIR_FOLIOS, 'Adquirir folios fiscales');
{$ifdef RDD}
      ArbolDefinirRDD;
{$endif}
end;

{******************************************************************************}
function TSistEditAccesos_DevEx.GetArbol: TcxTreeView;
begin
     case ArbolSeleccionado of
          adTress: Result := ArbolBase;
          adClasifi: Result := ArbolClasifi;
          adEntidades: Result := ArbolEntidades;
          else
              Result := NIL;
     end;
end;

{$ifdef RDD}
procedure TSistEditAccesos_DevEx.ArbolDefinirRDD;
var
   iLastModulo : Integer;
begin
     ArbolSeleccionado := adClasifi;
     ArbolClasifi.Items.Clear;

     with dmDiccionario.cdsClasificaciones do
     begin
          dmDiccionario.ConectaClasificacionesGrupoAdmin( dmSistema.GetEmpresaAccesos );
          First;
          while (not EOF) do
          begin
               AddElemento(0, FieldByName('RC_CODIGO').AsInteger, FieldByName('RC_NOMBRE').AsString);
                  AddDerechoConsulta;
                  AddDerechoAlta;
                  AddDerechoBaja;
                  AddDerechoEdicion;
                  AddDerechoImpresion;
               Next;
          end;
     end;

     iLastModulo := -1;
     ArbolSeleccionado := adEntidades;
     ArbolEntidades.Items.Clear;
     with dmDiccionario.cdsEntidadesPorModulo do
     begin
          dmDiccionario.ConectaEntidadesPorModulo( dmSistema.GetEmpresaAccesos );
          First;
          while (not EOF) do
          begin
               if ( (iLastModulo <> FieldByName('MO_CODIGO').AsInteger)) then
               begin
                    iLastModulo := FieldByName('MO_CODIGO').AsInteger;
                    AddElemento(0, GetImageIndex, FieldByName('MO_NOMBRE').AsString);
               end;

               AddElemento(1, FieldByName('EN_CODIGO').AsInteger, FieldByName('EN_TITULO').AsString + '  ( ' + FieldByName('EN_TABLA').AsString + ' )' );
                  {AddDerechoConsulta;
                  AddDerechoAlta;
                  AddDerechoBaja;
                  AddDerechoEdicion;
                  AddDerechoImpresion;}
               Next;
          end;
     end;
     PageControl.ActivePage := tsTress;
     Reporteador.ActivePage := tsClasificaciones;

end;
{$endif}

{$ifdef RDD}
procedure TSistEditAccesos_DevEx.ApagaPrendePadres;
var
   oPadre: TTreeNode;

   procedure CambiaImagenParent;
   var
      oHijo: TTreeNode;
   begin
         oPadre.StateIndex:= K_UN_CHECK;
         oHijo:= oPadre.GetNext;
         repeat
               if oHijo.StateIndex = K_CHECK then
                  oPadre.StateIndex:= K_CHECK;
               oHijo:= oHijo.GetNext;
         until( oPadre.StateIndex = K_CHECK ) or ( oHijo = NIL ) or ( oHijo.HasChildren );
   end;

begin
      with ArbolEntidades do
      begin
           Items.BeginUpdate;
           oPadre := ArbolEntidades.Items[ 0 ];
           repeat
                 if ( ( oPadre.HasChildren ) ) then
                      CambiaImagenParent;
                 oPadre:= oPadre.GetNext;
           until ( oPadre = nil );
           Items.EndUpdate;
      end;

end;
{$endif}

procedure TSistEditAccesos_DevEx.Cargar;
{$ifdef RDD}
var
   oCursor: TCursor;
   sGroupName, sCompanyName: String;
{$endif}
begin
     ArbolSeleccionado := adTress;
     inherited Cargar;

{$ifdef RDD}
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
{$endif}
     try

        RefrescaArbolAdicionales( dmSistema.BuscaDerechoAdicionales );

        //Se cargan los derechos de acceso de Reporteador
        {$ifdef RDD}
        ArbolSeleccionado := adClasifi;
        dmSistema.ConectaAccesos( sGroupName, sCompanyName );
        RefrescaArbol( dmSistema.BuscaDerechoClasificaciones );

        ArbolSeleccionado := adEntidades;
        try
           dmDiccionario.ConectaEntidadesGrupo( dmSistema.GetEmpresaAccesos );
           RefrescaArbol( dmSistema.BuscaDerechoEntidades );
           {$ifdef RDD}
           ApagaPrendePadres;
           {$endif}
        finally
               PageControl.ActivePage := tsTress;
               Reporteador.ActivePage := tsClasificaciones;
        end;
        Grupo.Caption := sGroupName;
        Company.Caption := sCompanyName;
        {$endif}

     finally
            {$ifdef RDD}
            Screen.Cursor := oCursor;
            {$endif}
     end;
end;

procedure TSistEditAccesos_DevEx.CopiaDerechos;
var
   oCursor: TCursor;
   iGrupo: Integer;
   sEmpresa: String;
begin
     case ArbolSeleccionado of
          adTress:
          begin
               //Se copian primero los derechos de Tress
               inherited CopiaDerechos;


          end;
          adClasifi:
          begin
               if FEscogeGrupoEmpresa_DevEx.SeleccionaGrupoEmpresa( iGrupo, sEmpresa ) then
               begin
                    oCursor := Screen.Cursor;
                    Screen.Cursor := crHourglass;
                    try
                       dmSistema.CopyAccesos( iGrupo, sEmpresa );
                       RefrescaArbol( dmSistema.BuscaDerechoCopiadoClasificaciones );
                       SetEditState;
                    finally
                           Screen.Cursor := oCursor;
                    end;
               end;
          end;
          adEntidades:
          begin
               if FEscogeGrupoEmpresa_DevEx.SeleccionaGrupoEmpresa( iGrupo, sEmpresa ) then
               begin
                    oCursor := Screen.Cursor;
                    Screen.Cursor := crHourglass;
                    try
                       dmSistema.CopyAccesos( iGrupo, sEmpresa );
                       RefrescaArbol( dmSistema.BuscaDerechoCopiadoEntidades );
                       SetEditState;
                       {$ifdef RDD}
                       ApagaPrendePadres;
                       {$endif}
                    finally
                           Screen.Cursor := oCursor;
                    end;
               end;
          end;
     end;
end;

procedure TSistEditAccesos_DevEx.RefrescaArbolAdicionales( Metodo: TBuscaDerecho );
var
   Nodo: TTreeNode;
begin
     if ( FNodoAdicionales <> NIL ) then
     begin
          if NOT FConectado then
          begin
               with dmSistema do
               begin
                    ConectaAccesosAdicionales;
                    ConectaGrupoAdAdmin( GetEmpresaAccesos );
               end;
          end;

          Nodo := FNodoAdicionales.GetFirstChild;
          repeat
                if ( Nodo.ImageIndex > GetImageIndex ) then
                begin
                     SetDerechos( Nodo, Metodo( Nodo.ImageIndex ) );
                end;
                Nodo := Nodo.GetNext;
          until ( Nodo = nil ) or ( Nodo = FNodoAdicionalesLast);
     end;
end;

function TSistEditAccesos_DevEx.GetImageIndex: integer;
begin
     case ArbolSeleccionado of
          adTress,adAdicionales: Result := K_IMAGENINDEX;
          else
              Result := K_IMAGENINDEXRDD;
     end;
end;

function TSistEditAccesos_DevEx.GetImageIndexLimit: integer;
begin
     {$ifdef TRESS}
     case ArbolSeleccionado of
          adTress: Result := K_IMAGENINDEX_LIMIT;
          else
              Result := MaxInt;
     end;
     {$else}
     Result := MaxInt;
     {$endif}

end;


procedure TSistEditAccesos_DevEx.OKClick(Sender: TObject);
begin
     //Graba los derechos de Tress
     ArbolSeleccionado := adTress;
     inherited;


     if FNodoAdicionales <> NIL then
     begin
          ArbolSeleccionado := adAdicionales;
          Descargar(FNodoAdicionales.GetFirstChild, FNodoAdicionalesLast );
     end;
     {$ifdef RDD}
     ArbolSeleccionado := adClasifi;
     Descargar;

     ArbolSeleccionado := adEntidades;
     Descargar;
     {$endif}

end;

procedure TSistEditAccesos_DevEx.PageControlChange(Sender: TObject);
begin
     inherited;
     if PageControl.ActivePage = tsTress then
        ArbolSeleccionado := adTress
     else if PageControl.ActivePage = tsReporteador then
     begin
          if Reporteador.ActivePage = tsClasificaciones then
             ArbolSeleccionado := adClasifi
          else
              ArbolSeleccionado := adEntidades;
     end;
end;

procedure TSistEditAccesos_DevEx.FormShow(Sender: TObject);
begin
     //Se cambio al ArbolConstruir para aca. Se TIENE que hacer antes del Inherited;
     ArbolSeleccionado := adTress;
     self.ArbolConstruir;
     inherited;
     ArbolSeleccionado := adTress;
     FConectado:= FALSE;
end;

procedure TSistEditAccesos_DevEx.AgregaDerechosCamposAdicionales;

   function GetLlave: integer;
   begin
        with dmSistema.cdsAdicionalesAccesos do
             if Locate( 'GX_CODIGO', dmSistema.cdsGruposAdic.FieldByName('GX_CODIGO').AsString, [] ) then
                Result:= K_IMAGENINDEX_LIMIT + FieldByName('LLave').AsInteger
             else
                 Result := K_IMAGENINDEX_LIMIT;
   end;
  var
     sIndexField: string;
begin
     with dmSistema do
     begin
          ConectaGrupoAdAdmin ( GetEmpresaAccesos );

          with cdsGruposAdic do
          begin
               First;
               if not EOF then
               begin
                    cdsEmpresasAccesos.Conectar;
                    ConectaAccesosAdicionales;
                    AddElemento( 2, D_EMP_DATOS_ADICION, 'Adicionales' );
                    FNodoAdicionales := ArbolBase.Items.Item[ ArbolBase.Items.Count-1];

                    sIndexField := IndexFieldNames;
                    IndexFieldNames := 'GX_POSICIO';

                    try
                       while (not EOF) do
                       begin
                            AddElemento(3, GetLLave, FieldByName('GX_TITULO').AsString);
                              AddDerechoConsulta;
                              AddDerechoEdicion;
                              AddDerechoImpresion;
                            Next;
                       end;
                       FNodoAdicionalesLast := ArbolBase.Items.Item[ ArbolBase.Items.Count-1];

                    finally
                           IndexFieldNames:= sIndexField;
                    end;
               end
               else
                  FNodoAdicionales := nil;
          end;
          FConectado:= TRUE;
     end;
end;


procedure TSistEditAccesos_DevEx.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     inherited;
     dmSistema.cdsGruposAdic.Close;
end;

procedure TSistEditAccesos_DevEx.CambiosAdicionales;
var
   oCursor: TCursor;
begin
     inherited;
     case ArbolSeleccionado of
          adTress:
          begin
               if FNodoAdicionales <> NIL then
               begin
                    ArbolSeleccionado := adAdicionales;
                    oCursor := Screen.Cursor;
                    Screen.Cursor := crHourglass;
                    try
                       dmSistema.CopyAccesos( FGrupoCopiado, FEmpresaCopiado );
                       dmSistema.CopiaDerechosAdicionales;
                       FConectado := TRUE;
                       RefrescaArbolAdicionales( dmSistema.BuscaDerechoAdicionales );
                       //SetEditState;
                    finally
                           Screen.Cursor := oCursor;
                    end;
               end;
          end;
     end;
end;



procedure TSistEditAccesos_DevEx.OK_DevExClick(Sender: TObject);
begin
 //Graba los derechos de Tress
     ArbolSeleccionado := adTress;
     inherited;


     if FNodoAdicionales <> NIL then
     begin
          ArbolSeleccionado := adAdicionales;
          Descargar(FNodoAdicionales.GetFirstChild, FNodoAdicionalesLast );
     end;
     {$ifdef RDD}
     ArbolSeleccionado := adClasifi;
     Descargar;

     ArbolSeleccionado := adEntidades;
     Descargar;
     {$endif}
end;

end.

