unit FSistBaseEditEmpresas_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Mask, Forms, Dialogs, ExtCtrls,
     Buttons, DBCtrls, StdCtrls, Db,
     ZetaKeyCombo, ZetaEdit, ZetaSmartLists, ZBaseEdicion_DevEx,
     cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
     Menus, dxSkinsCore, TressMorado2013, dxSkinsDefaultPainters,
     cxControls, dxSkinsdxBarPainter,
     dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
     cxButtons, cxContainer, cxEdit, cxGroupBox;

type
  TSistBaseEditEmpresas_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    Label2: TLabel;
    CM_NOMBRE: TDBEdit;
    CM_CODIGO: TZetaDBEdit;
    GroupBox1: TcxGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label5: TLabel;
    CM_USRNAME: TDBEdit;
    CM_PASSWRD: TDBEdit;
    CM_CONTROL: TDBEdit;
    CM_DATOS: TDBEdit;
    Label7: TLabel;
    CM_ALIAS: TDBEdit;
    Panel2: TPanel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  SistBaseEditEmpresas_DevEx: TSistBaseEditEmpresas_DevEx;

implementation

{$R *.DFM}

uses DSistema,
     ZetaCommonClasses,
     ZAccesosTress;

procedure TSistBaseEditEmpresas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_SIST_DATOS_EMPRESAS;
     HelpContext := H80811_Empresas;
     FirstControl := CM_CODIGO;
end;

procedure TSistBaseEditEmpresas_DevEx.Connect;
begin
     with dmSistema do
     begin
          cdsEmpresasLookUp.Conectar;
          Datasource.Dataset := cdsEmpresas;
     end;
end;

end.
