unit FSistDispPorGrupo_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseGridLectura_DevEx, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  StdCtrls, Buttons, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, Menus, cxButtons, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, ActnList, ImgList, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, ZetaKeyLookup_DevEx;

type
  TSistDispPorGrupo_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    luGrupo: TZetaKeyLookup_DevEx;
    TE_CODIGO: TcxGridDBColumn;
    GP_CODIGO: TcxGridDBColumn;
    BAgregaDisp_DevEx: TcxButton;
    procedure luGrupoValidKey(Sender: TObject);
    //procedure BAgregaDispClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BAgregaDisp_DevExClick(Sender: TObject);
  private
    { Private declarations }
    lDerechoAlta: Boolean;
    procedure ConfigAgrupamiento;
  public
    { Public declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
  end;

var
  SistDispPorGrupo_DevEx: TSistDispPorGrupo_DevEx;

implementation

uses dSistema,
     ZetaCommonClasses,
     ZetaDialogo,
     ZetaCommonTools,
     ZBaseDlgModal_DevEx,
     ZAccesosMgr,
     ZAccesosTress,
     FListadoDispositivos_DevEx;

{$R *.dfm}

{ TSistDispPorGrupo }

procedure TSistDispPorGrupo_DevEx.Agregar;
begin
     ZInformation( Self.Caption, 'Por favor utilizar el bot�n de Agregar Terminales', 0 );
end;

procedure TSistDispPorGrupo_DevEx.Borrar;
begin
     ZInformation( Self.Caption, 'Por favor utilizar el bot�n de Agregar Terminales', 0 );
end;

procedure TSistDispPorGrupo_DevEx.Connect;
begin
     inherited;
     with dmSistema do
     begin
          GrupoDeTerm := VACIO;
          cdsTermPorGrupo.Conectar;
          cdsListaGrupos.Conectar;
          DataSource.DataSet := cdsTermPorGrupo;
          luGrupo.LookUpDataSet := cdsListaGrupos;
     end;
end;

procedure TSistDispPorGrupo_DevEx.Modificar;
begin
     ZInformation( Self.Caption, 'Por favor utilizar el bot�n de Agregar Terminales', 0 );
end;

procedure TSistDispPorGrupo_DevEx.Refresh;
begin
     inherited;
     with dmSistema do
     begin
          GrupoDeTerm := luGrupo.Llave;
          cdsTermPorGrupo.Refrescar;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TSistDispPorGrupo_DevEx.luGrupoValidKey(Sender: TObject);
begin
     inherited;
     with dmSistema do
     begin
          GrupoDeTerm := luGrupo.Llave;
          cdsTermPorGrupo.Refrescar;
          BAgregaDisp_DevEx.Enabled := StrLleno( luGrupo.Llave ) and lDerechoAlta;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

{procedure TSistDispPorGrupo_DevEx.BAgregaDispClick(Sender: TObject);
begin
     inherited;
     if lDerechoAlta then
     begin
          with dmSistema do
          begin
               GrupoDeTerm := luGrupo.Llave;
               cdsTermPorGrupo.Refrescar;
          end;
          ZBaseDlgModal_DevEx.ShowDlgModal( ListadoDispositivos_DevEx, TListadoDispositivos_DevEx );
     end
     else
         ZWarning( Self.Caption, 'No tiene Derecho para agregar terminales',0,mbOk);
end;}

procedure TSistDispPorGrupo_DevEx.ConfigAgrupamiento;
begin
     //Cambio de configuracion
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Esconde la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     TE_CODIGO.Options.Grouping:= FALSE;
end;

procedure TSistDispPorGrupo_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     {***Banda Sumatoria***}
     CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
     inherited;
     SetFocusedControl(luGrupo);
     ConfigAgrupamiento;
end;

procedure TSistDispPorGrupo_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_TRESS_SISTEMA_TERMINALESGTI_TERMINALESXGRUPO;
     lDerechoAlta := CheckDerecho( D_SIST_TERM_GRUPO, K_DERECHO_ALTA );
end;

procedure TSistDispPorGrupo_DevEx.BAgregaDisp_DevExClick(Sender: TObject);
begin
inherited;
     if lDerechoAlta then
     begin
          with dmSistema do
          begin
               GrupoDeTerm := luGrupo.Llave;
               cdsTermPorGrupo.Refrescar;
          end;
          ZBaseDlgModal_DevEx.ShowDlgModal( ListadoDispositivos_DevEx, TListadoDispositivos_DevEx );
     end
     else
         ZWarning( Self.Caption, 'No tiene Derecho para agregar terminales',0,mbOk);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

end.
