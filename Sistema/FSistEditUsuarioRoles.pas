unit FSistEditUsuarioRoles;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, CheckLst, Buttons, ExtCtrls,
     ZBaseDlgModal, ZetaSmartLists;

type
  TFSistEditUsuarioRoles = class(TZetaDlgModal)
    Roles: TCheckListBox;
    Prender: TBitBtn;
    Apagar: TBitBtn;
    PanelBusqueda: TPanel;
    EmpleadoBuscaBtn: TZetaSpeedButton;
    EditBusca: TEdit;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
    procedure PrenderClick(Sender: TObject);
    procedure ApagarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure RolesClickCheck(Sender: TObject);
    procedure EmpleadoBuscaBtnClick(Sender: TObject);
  private
    { Private declarations }
    FLista: TStrings;
    FUltimoTexto: String;
    FPosUltimo: Integer;
    function EncontroSuper: Boolean;
    procedure PrendeApaga( const lState: Boolean );
    procedure Cargar;
    procedure Descargar;
    procedure HabilitaControlesDerecho;
  public
    { Public declarations }
  end;

var
  SistEditUsuarioRoles: TFSistEditUsuarioRoles;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     DSistema,
     ZAccesosTress,
     ZAccesosMgr;



{$R *.DFM}

procedure TFSistEditUsuarioRoles.FormCreate(Sender: TObject);
begin
     inherited;
     FLista := TStringList.Create;
end;

procedure TFSistEditUsuarioRoles.FormShow(Sender: TObject);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     dmSistema.cdsRoles.Conectar;
     dmSistema.cdsUserRoles.Refrescar;
     {$endif}
     Caption := 'Roles asignados al usuario: '+ dmSistema.cdsUsuarios.FieldByName('US_CORTO').AsString;
     Cargar;
     OK.Enabled := False;
     FUltimoTexto:= VACIO;
     FPosUltimo:= -1;
     HabilitaControlesDerecho;
     If Roles.Enabled then
        ActiveControl := Roles;

end;

procedure TFSistEditUsuarioRoles.FormDestroy(Sender: TObject);
begin
     inherited;
     FLista.Free;
end;

procedure TFSistEditUsuarioRoles.HabilitaControlesDerecho;
begin
     Prender.Enabled :=  ZAccesosMgr.CheckDerecho( D_CAT_ROLES, K_DERECHO_CAMBIO  );
     Apagar.Enabled := Prender.Enabled;
     Roles.Enabled := Prender.Enabled;
end;


procedure TFSistEditUsuarioRoles.Cargar;
{$ifndef DOS_CAPAS}
var
   sCodigo, sNombre, sRegistro : string;
{$endif}
begin
     inherited;
     {$ifndef DOS_CAPAS}
     //Cargar los roles en el CheckListBox
     dmSistema.CargaListaRoles(FLista);
     with dmSistema.cdsRoles do
     begin
          First;
          Roles.Clear;
          while not Eof do
          begin
               sCodigo := FieldByName('RO_CODIGO').AsString;
               sNombre := FieldByName('RO_NOMBRE').AsString;
               sRegistro := sCodigo + ' : ' + sNombre;
               Roles.Items.Add( sRegistro );

               if ( dmSistema.cdsUserRoles.Locate('RO_CODIGO',sCodigo,[])) then
               begin
                    Roles.Checked[ Roles.Items.Count - 1 ] := True;
               end;
               Next;
          end;
     end;
     {$endif}
end;

procedure TFSistEditUsuarioRoles.Descargar;
var
   i: Integer;
begin
     with Roles do
     begin
          with Items do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    if Checked[ i ] then
                       FLista.Objects[ i ] := TObject( 1 )
                    else
                        FLista.Objects[ i ] := TObject( 0 );
               end;
          end;
     end;
     dmSistema.DescargaListaUserRoles ( FLista );
end;

procedure TFSistEditUsuarioRoles.PrendeApaga( const lState: Boolean );
var
   i: Integer;
begin
     with Roles do
     begin
          with Items do
          begin
               try
                  BeginUpdate;
                  for i := 0 to ( Count - 1 ) do
                      Checked[ i ] := lState;
               finally
                      EndUpdate;
               end;
          end;
     end;
     RolesClickCheck( Roles );
end;

procedure TFSistEditUsuarioRoles.PrenderClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( True );
end;

procedure TFSistEditUsuarioRoles.ApagarClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( False );
end;

procedure TFSistEditUsuarioRoles.OKClick(Sender: TObject);
begin
     inherited;
     Descargar;
end;

procedure TFSistEditUsuarioRoles.RolesClickCheck(Sender: TObject);
begin
     inherited;
     with OK do
     begin
          if not Enabled then
             Enabled := True;
     end;
end;

procedure TFSistEditUsuarioRoles.EmpleadoBuscaBtnClick(Sender: TObject);
var
   lEncontro: Boolean;
begin
     if strLleno( EditBusca.Text ) and ( FPosUltimo <> Roles.Count )then
     begin
          lEncontro:= EncontroSuper;
     end
     else
     begin
          lEncontro:= FALSE;
     end;
     if ( not lEncontro ) then
     begin
          Roles.Selected[ FPosUltimo ]:= False;

          if( FPosUltimo > 0 ) then
              ZetaDialogo.ZInformation( 'B�squeda de Roles', '! No hay otro Rol con esos Datos !' ,0 )
          else
              ZetaDialogo.ZInformation( 'B�squeda de Roles', '! No hay un Rol con esos Datos !' ,0 );
          with EditBusca do
          begin
               SetFocus;
               SelectAll;
          end;

     FPosUltimo:=0;
     end;
end;

function TFSistEditUsuarioRoles.EncontroSuper: Boolean;
var
   i : Integer;
   sTexto: String;
begin
     sTexto:= UpperCase( EditBusca.Text );
     with Roles do
     begin
          if ( sTexto <> FUltimoTexto ) then
          begin
               FUltimoTexto := sTexto;
               FPosUltimo:= 0;
          end;
          Result:= False;
          for i:= FPosUltimo  to ( Count - 1 ) do
          begin
               if ( Pos( sTexto, UpperCase( Items[i] ) ) > 0 ) then
               begin
                    FUltimoTexto:= sTexto;
                    FPosUltimo:= i;
                    Selected[ FPosUltimo ]:= True;
                    FPosUltimo:= FPosUltimo + 1;
                    Result:= True;
                    Exit;
               end;
          end;
     end;
end;



end.
