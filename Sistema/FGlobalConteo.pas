unit FGlobalConteo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ZetaKeyCombo, Buttons, ExtCtrls,
  ZBaseGlobal, ZetaCommonClasses;

type
  TGlobalConteo = class(TBaseGlobal)
    GBNiveles: TGroupBox;
    lbCriterio1: TLabel;
    Nivel1: TComboBox;
    lbCriterio2: TLabel;
    Nivel2: TComboBox;
    lbCriterio3: TLabel;
    Nivel3: TComboBox;
    lbCriterio4: TLabel;
    Nivel4: TComboBox;
    lbCriterio5: TLabel;
    Nivel5: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    //procedure OKClick(Sender: TObject);
    procedure NivelesChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure OKClick(Sender: TObject);
    //procedure CancelarClick(Sender: TObject);
  private
    { Private declarations }
    FNivel1, FNivel2, FNivel3, FNivel4, FNivel5 : Integer;
    FCombos : array[ 1..K_MAX_CONTEO_NIVELES ] of TComboBox;
    //FSICerrar : Boolean;
    function CambiosValidos: Boolean;
    function HuboCambios: Boolean;
    procedure LlenaCombos;
    procedure SetControls(const iTag: integer);
    procedure SetDefaultsConteo;
  public
    { Public declarations }
    procedure Descargar; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  end;

var
  GlobalConteo: TGlobalConteo;

implementation

uses DGlobal,
     ZGlobalTress,
     ZAccesosTress,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaCommonLists;

{$R *.DFM}

procedure TGlobalConteo.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_CONTEO_CONFIGURACION;
     HelpContext := H50553_Configuracion_del_presupuesto;

     FCombos[ 1 ] := Nivel1;
     FCombos[ 2 ] := Nivel2;
     FCombos[ 3 ] := Nivel3;
     FCombos[ 4 ] := Nivel4;
     FCombos[ 5 ] := Nivel5;

     Nivel1.Tag := K_GLOBAL_CONTEO_NIVEL1;
     Nivel2.Tag := K_GLOBAL_CONTEO_NIVEL2;
     Nivel3.Tag := K_GLOBAL_CONTEO_NIVEL3;
     Nivel4.Tag := K_GLOBAL_CONTEO_NIVEL4;
     Nivel5.Tag := K_GLOBAL_CONTEO_NIVEL5;
     LlenaCombos;

     ActualizaDiccion := True;
end;

procedure TGlobalConteo.FormShow(Sender: TObject);
const
     K_LEFT_OK = 207;
begin
     inherited;
{
     Ok.Left := 207;
     Cancelar.Visible := TRUE;
}
     FNivel1 := Global.GetGlobalInteger( K_GLOBAL_CONTEO_NIVEL1 );
     FNivel2 := Global.GetGlobalInteger( K_GLOBAL_CONTEO_NIVEL2 );
     FNivel3 := Global.GetGlobalInteger( K_GLOBAL_CONTEO_NIVEL3 );
     FNivel4 := Global.GetGlobalInteger( K_GLOBAL_CONTEO_NIVEL4 );
     FNivel5 := Global.GetGlobalInteger( K_GLOBAL_CONTEO_NIVEL5 );

     Cancelar.Visible := ( FNivel1 <> Ord( coNinguno ) );
     if Cancelar.Visible then
        Ok.Left := K_LEFT_OK
     else
        SetDefaultsConteo;

{
     if FNivel1 = 0 then
     begin
          FNivel1 := Ord(coNivel1);
          Nivel1.ItemIndex := FNivel1;
          if FNivel2 = 0 then
          begin
               FNivel2 := Ord(coPuesto);
               Nivel2.ItemIndex := FNivel2;
          end;
          Cancelar.Visible := FALSE;
          Ok.Left := Cancelar.Left;
     end;

     FSICerrar := TRUE;
}
     SetControls(1);
end;

procedure TGlobalConteo.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     inherited;
     CanClose := Cancelar.Visible or ( LastAction = K_EDICION_MODIFICACION );   // Si no se puede cancelar debe haber grabado los cambios
{
     CanClose := FSiCerrar AND Cancelar.Visible;
     FSiCerrar := TRUE;
}
end;

procedure TGlobalConteo.SetDefaultsConteo;
begin
     FNivel1 := Ord(coNivel1);
     Nivel1.ItemIndex := FNivel1;
     if FNivel2 = 0 then
     begin
          FNivel2 := Ord(coPuesto);
          Nivel2.ItemIndex := FNivel2;
     end;
     Ok.Left := Cancelar.Left;
end;

procedure TGlobalConteo.LlenaCombos;
var
   i: Integer;
begin
     with Nivel1.Items do
     begin
          BeginUpdate;
          Clear;
          try
             Add( '<Ninguno>' );
             Add( 'Puesto' );
             Add( 'Turno' );
             Add( 'Clasificaci�n' );
             Add( 'Confidencialidad' );
             for i := 1 to Global.NumNiveles do
                 Add( Global.NombreNivel( i ) )
{
             for i := 1 to 9 do
             begin
                  with Global do
                  begin
                       if StrLleno( NombreNivel( i ) ) then
                          Add( NombreNivel( i ) )
                       else
                          Break;
                  end;
             end;
}
          finally
             EndUpdate;
          end;
     end;
     Nivel2.Items.Assign(Nivel1.Items);
     Nivel3.Items.Assign(Nivel1.Items);
     Nivel4.Items.Assign(Nivel1.Items);
     Nivel5.Items.Assign(Nivel1.Items);
end;

procedure TGlobalConteo.DesCargar;
var
   lOk : Boolean;
begin
     lOk := CambiosValidos;
     if lOk then
     begin
          if Cancelar.Visible then
          begin
               lOk := HuboCambios;
               if lOk then
                  lOk := ZWarningConfirm( Self.Caption, '� Cualquier Informaci�n Presupuestada que se Tenga Ser� Borrada !' + CR_LF + CR_LF + '� Desea Guardar los Cambios ? ', 0, mbCancel )
               else
                  LastAction := K_EDICION_CANCELAR;
          end;
          if lOk then
             inherited Descargar;        // Procede a Grabar y poner el LastAction en Modificaci�n
     end;
end;

procedure TGlobalConteo.OKClick(Sender: TObject);
begin
     inherited;
     if ( LastAction = K_EDICION_CANCELAR ) then
        self.ModalResult := mrOk;
end;

{
procedure TGlobalConteo.OKClick(Sender: TObject);
begin
     FSICerrar := CambiosValidos;
     if FSiCerrar then
     begin
          if HuboCambios and Cancelar.Visible then
          begin
               if not ZWarningConfirm( Self.Caption, '� Cualquier Informaci�n Presupuestada que se Tenga Ser� Borrada !' +
                                   CR_LF + CR_LF + '� Desea Guardar los Cambios ? ', 0, mbCancel ) then
                  CancelarClick( Sender )
               else
                   inherited;
          end
          else if NOT Cancelar.Visible then
          begin
               Cancelar.Visible := TRUE;
               inherited;
          end
          else LastAction := K_EDICION_CANCELAR;
     end;
end;
}

function TGlobalConteo.CambiosValidos: Boolean;
const
     K_MESS_ERROR_EMPTY = 'Se Tiene que Especificar por lo Menos un Criterio';
     K_MESS_ERROR_REPETIDOS = 'No se Pueden Repetir los Criterios';
var
   i, j: integer;

   procedure ReportaError( const sMensaje: String; oControl: TWinControl );
   begin
        ZetaDialogo.ZError( Caption, sMensaje, 0 );
        oControl.SetFocus;
   end;

begin
     Result := FCombos[1].ItemIndex <> 0;
     if NOT Result then
        ReportaError( K_MESS_ERROR_EMPTY, FCombos[1] )
     else
     begin
          for i:= 1 to K_MAX_CONTEO_NIVELES do
          begin
               if ( FCombos[i].ItemIndex <> Ord( coNinguno ) ) then
               begin
                    for j := i + 1 to K_MAX_CONTEO_NIVELES do
                    begin
                         if ( FCombos[j].ItemIndex <> Ord( coNinguno ) ) then
                         begin
                              Result := ( FCombos[i].ItemIndex <> FCombos[j].ItemIndex );
                              if not Result then
                              begin
                                   ReportaError( K_MESS_ERROR_REPETIDOS, FCombos[j] );
                                   Exit;
                              end;
                         end
                         else
                             Break;
                    end;
               end
               else
                   Break;
          end;
     end;
{
     if NOT Result then
     begin
          ZError(Caption, 'Se Tiene que Especificar por lo Menos un Criterio', 0);
          FCombos[1].SetFocus;
          Exit;
     end;

     for i:= 1 to 5 do
     begin
          if FCombos[i].ItemIndex = 0 then Break;

          for j:=i+1 to 5 do
          begin
               if FCombos[j].ItemIndex = 0 then Break;

               Result := Result AND
                         (FCombos[i].ItemIndex <> FCombos[j].ItemIndex);

               if NOT Result then Break;
          end;
          if NOT Result then Break;
     end;

     if Not RESULT then
     begin
          ZError(Caption, 'No se Pueden Repetir los Criterios', 0);
          FCombos[1].SetFocus;
     end;
}
end;

function TGlobalConteo.HuboCambios: Boolean;
begin
     Result := ( FNivel1 <> 0 ) and
               ( ( FNivel1 <> Nivel1.ItemIndex ) or ( FNivel2 <> Nivel2.ItemIndex ) or
                 ( FNivel3 <> Nivel3.ItemIndex ) or ( FNivel4 <> Nivel4.ItemIndex ) or
                 ( FNivel5 <> Nivel5.ItemIndex ) );
end;

procedure TGlobalConteo.SetControls(const iTag : integer);
 var i: integer;
begin
     for i := iTag to 4 do
     begin
          //FCombos[i+1].Enabled := (FCombos[i].ItemIndex > 0) AND (i < 5);
          FCombos[i+1].Enabled := ( FCombos[i].ItemIndex <> Ord( coNinguno ) );
          if NOT FCombos[i+1].Enabled then
             FCombos[i+1].ItemIndex := Ord( coNinguno );
     end;
end;
procedure TGlobalConteo.NivelesChange(Sender: TObject);
begin
     inherited;
     SetControls(TComboBox(Sender).Tag-K_GLOBAL_CONTEO_NIVEL1+1);
end;

{
procedure TGlobalConteo.CancelarClick(Sender: TObject);
begin
     FSiCerrar := TRUE;
     inherited;
end;
}

function TGlobalConteo.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := CheckDerechos( K_DERECHO_CONSULTA );
     if not Result then
        sMensaje := 'No Tiene Permiso Para Modificar Registros';
end;

end.
