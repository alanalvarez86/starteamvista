unit FEditUsuarioBaseDatos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, Mask, DBCtrls, Buttons, StdCtrls, ExtCtrls, ZetaEdit, DB;

type
  TEditUsuarioBaseDatos = class(TZetaDlgModal)
    lblUsuario: TLabel;
    edUsuario: TEdit;
    Label5: TLabel;
    edPassword: TEdit;
    rbDefault: TRadioButton;
    rbOtro: TRadioButton;
    Label1: TLabel;
    edConfirmacion: TEdit;
    procedure edUsuarioChange(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure rbOtroClick(Sender: TObject);
    procedure rbDefaultClick(Sender: TObject);

  private
    FUsuario: String;
    FPassword: String;
    { Private declarations }
    procedure rbOtroChecked;
  public
    { Public declarations }
  end;

function EditarUsuario( const sUsuario: String; const iHelpCtx: LongInt): Boolean;

var
  EditUsuarioBaseDatos: TEditUsuarioBaseDatos;

implementation

uses ZetaCommonClasses, dSistema, ZetaDialogo, ZetaServerTools;

{$R *.DFM}

function EditarUsuario( const sUsuario: String; const iHelpCtx: LongInt): Boolean;
begin
     with TEditUsuarioBaseDatos.Create( Application ) do
     begin
          try
             Result:= True;
             HelpContext:= iHelpCtx;
             ShowModal;

             //
             FUsuario := edUsuario.Text;
             FPassword := edPassword.Text;
             // ----- -----
          finally
            Free;
          end;
     end;
end;

procedure TEditUsuarioBaseDatos.edUsuarioChange(Sender: TObject);
begin
     inherited;
     FUsuario := edUsuario.Text;
     FPassword := edPassword.Text;
end;

procedure TEditUsuarioBaseDatos.OKClick(Sender: TObject);
begin
     inherited;

     if rbOtro.Checked then
     begin
          if Trim (edUsuario.Text) <> '' then
          begin
              if  edPassword.Text = edConfirmacion.Text then
              begin
                  FUsuario := edUsuario.Text;
                  FPassword := edPassword.Text;   

                  if not (dmSistema.cdsSistBaseDatos.state in [dsInsert, dsEdit ]) then
                     dmSistema.cdsSistBaseDatos.Edit;

                  dmSistema.cdsSistBaseDatos.FieldByName ('DB_USRDFLT').AsString := K_GLOBAL_NO;
                  dmSistema.cdsSistBaseDatos.FieldByName ('DB_USRNAME').AsString := edUsuario.Text;
                  dmSistema.cdsSistBaseDatos.FieldByName ('DB_PASSWRD').AsString := edPassword.Text;

                  Close;
              end
              else
              begin
                   ZetaDialogo.zError( '� Error en Clave !', 'Por favor, confirme su clave correctamente', 0 );
                   edConfirmacion.SetFocus;
              end;
          end
          else
          begin
               ZetaDialogo.zError( '� Error en Usuario !', 'Campo Usuario no puede quedar vac�o', 0 );
               edUsuario.SetFocus;
          end;
     end
     else
     begin
          if not (dmSistema.cdsSistBaseDatos.state in [dsInsert, dsEdit ]) then
             dmSistema.cdsSistBaseDatos.Edit;
             
          dmSistema.cdsSistBaseDatos.FieldByName ('DB_USRDFLT').AsString := K_GLOBAL_SI;
          dmSistema.cdsSistBaseDatos.FieldByName ('DB_USRNAME').AsString :=  dmSistema.GetSQLUserName;
          dmSistema.cdsSistBaseDatos.FieldByName ('DB_PASSWRD').AsString := '';
          Close;
     end;
end;

procedure TEditUsuarioBaseDatos.FormShow(Sender: TObject);
begin
     inherited;
     edUsuario.Text := FUsuario;
     edPassword.Text := FPassword;
     OK.Enabled := TRUE;
     rbDefault.SetFocus;
end;

procedure TEditUsuarioBaseDatos.rbOtroClick(Sender: TObject);
begin
    rbOtroChecked;
    if not OK.Enabled then
       OK.Enabled := TRUE;
end;

procedure TEditUsuarioBaseDatos.rbDefaultClick(Sender: TObject);
begin
     rbOtroChecked;
end;

procedure TEditUsuarioBaseDatos.rbOtroChecked;
begin
     edUsuario.Enabled := rbOtro.Checked;
     edPassword.Enabled := rbOtro.Checked;
     edConfirmacion.Enabled := rbOtro.Checked;
end;

end.
