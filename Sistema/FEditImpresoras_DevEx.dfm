inherited EditImpresoras_DevEx: TEditImpresoras_DevEx
  Left = 622
  Top = 266
  Caption = 'Impresoras'
  ClientHeight = 363
  ClientWidth = 469
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 327
    Width = 469
    TabOrder = 3
    inherited OK_DevEx: TcxButton
      Left = 302
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 381
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 469
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 143
      inherited textoValorActivo2: TLabel
        Width = 137
      end
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 50
    Width = 469
    Height = 41
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 13
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nombre:'
    end
    object PI_NOMBRE: TZetaDBEdit
      Left = 61
      Top = 9
      Width = 348
      Height = 21
      TabOrder = 0
      ConfirmEdit = True
      DataField = 'PI_NOMBRE'
      DataSource = DataSource
    end
  end
  object PageControl_DevEx: TcxPageControl [4]
    Left = 0
    Top = 91
    Width = 469
    Height = 236
    Align = alClient
    TabOrder = 2
    Properties.ActivePage = GeneralesTab_DevEx
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 234
    ClientRectLeft = 2
    ClientRectRight = 467
    ClientRectTop = 28
    object GeneralesTab_DevEx: TcxTabSheet
      Caption = 'Generales'
      object Label10: TLabel
        Left = 73
        Top = 187
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fin Italica:'
      end
      object Label9: TLabel
        Left = 62
        Top = 165
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'Inicio Italica:'
      end
      object Label8: TLabel
        Left = 67
        Top = 143
        Width = 54
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fin Negrita:'
      end
      object Label7: TLabel
        Left = 56
        Top = 121
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = 'Inicio Negrita:'
      end
      object Label6: TLabel
        Left = 50
        Top = 99
        Width = 71
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fin Subrayado:'
      end
      object Label5: TLabel
        Left = 39
        Top = 77
        Width = 82
        Height = 13
        Alignment = taRightJustify
        Caption = 'Inicio Subrayado:'
      end
      object Label4: TLabel
        Left = 64
        Top = 55
        Width = 57
        Height = 13
        Alignment = taRightJustify
        Caption = 'Comprimido:'
      end
      object Label3: TLabel
        Left = 38
        Top = 33
        Width = 83
        Height = 13
        Alignment = taRightJustify
        Caption = 'Semi-Comprimido:'
      end
      object Label2: TLabel
        Left = 51
        Top = 11
        Width = 70
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ancho Normal:'
      end
      object PI_ITAL_OF: TDBEdit
        Left = 126
        Top = 183
        Width = 300
        Height = 21
        DataField = 'PI_ITAL_OF'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 8
      end
      object PI_ITAL_ON: TDBEdit
        Left = 126
        Top = 161
        Width = 300
        Height = 21
        DataField = 'PI_ITAL_ON'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 7
      end
      object PI_BOLD_OF: TDBEdit
        Left = 126
        Top = 139
        Width = 300
        Height = 21
        DataField = 'PI_BOLD_OF'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 6
      end
      object PI_BOLD_ON: TDBEdit
        Left = 126
        Top = 117
        Width = 300
        Height = 21
        DataField = 'PI_BOLD_ON'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 5
      end
      object PI_UNDE_OF: TDBEdit
        Left = 126
        Top = 95
        Width = 300
        Height = 21
        DataField = 'PI_UNDE_OF'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 4
      end
      object PI_UNDE_ON: TDBEdit
        Left = 126
        Top = 73
        Width = 300
        Height = 21
        DataField = 'PI_UNDE_ON'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 3
      end
      object PI_CHAR_17: TDBEdit
        Left = 126
        Top = 51
        Width = 300
        Height = 21
        DataField = 'PI_CHAR_17'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 2
      end
      object PI_CHAR_12: TDBEdit
        Left = 126
        Top = 29
        Width = 300
        Height = 21
        DataField = 'PI_CHAR_12'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 1
      end
      object PI_CHAR_10: TDBEdit
        Left = 126
        Top = 7
        Width = 300
        Height = 21
        DataField = 'PI_CHAR_10'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 0
      end
    end
    object EspecialesTab_DevEx: TcxTabSheet
      Caption = 'Especiales'
      object Label17: TLabel
        Left = 118
        Top = 143
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Extra 2:'
        Visible = False
      end
      object Label16: TLabel
        Left = 118
        Top = 121
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Extra 1:'
        Visible = False
      end
      object Label15: TLabel
        Left = 50
        Top = 55
        Width = 104
        Height = 13
        Alignment = taRightJustify
        Caption = 'Impresi'#243'n Landscape:'
      end
      object Label14: TLabel
        Left = 3
        Top = 99
        Width = 151
        Height = 13
        Alignment = taRightJustify
        Caption = 'Imprimir 8 Renglones / Pulgada:'
      end
      object Label13: TLabel
        Left = 3
        Top = 77
        Width = 151
        Height = 13
        Alignment = taRightJustify
        Caption = 'Imprimir 6 Renglones / Pulgada:'
      end
      object Label12: TLabel
        Left = 59
        Top = 33
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Reset de Impresora:'
      end
      object Label11: TLabel
        Left = 127
        Top = 11
        Width = 27
        Height = 13
        Alignment = taRightJustify
        Caption = 'Eject:'
      end
      object PI_EXTRA_2: TDBEdit
        Left = 159
        Top = 139
        Width = 300
        Height = 21
        DataField = 'PI_EXTRA_2'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 6
        Visible = False
      end
      object PI_EXTRA_1: TDBEdit
        Left = 159
        Top = 117
        Width = 300
        Height = 21
        DataField = 'PI_EXTRA_1'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 5
        Visible = False
      end
      object PI_LANDSCA: TDBEdit
        Left = 159
        Top = 51
        Width = 300
        Height = 21
        DataField = 'PI_LANDSCA'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 2
      end
      object PI_8_LINES: TDBEdit
        Left = 159
        Top = 95
        Width = 300
        Height = 21
        DataField = 'PI_8_LINES'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 4
      end
      object PI_6_LINES: TDBEdit
        Left = 159
        Top = 73
        Width = 300
        Height = 21
        DataField = 'PI_6_LINES'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 3
      end
      object PI_RESET: TDBEdit
        Left = 159
        Top = 29
        Width = 300
        Height = 21
        DataField = 'PI_RESET'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 1
      end
      object PI_EJECT: TDBEdit
        Left = 159
        Top = 7
        Width = 300
        Height = 21
        DataField = 'PI_EJECT'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 0
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
