inherited ArbolConfigura: TArbolConfigura
  Left = 531
  Top = 151
  Caption = 'Configuraci'#243'n de Arbol'
  ClientHeight = 473
  ClientWidth = 470
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 437
    Width = 470
    inherited OK: TBitBtn
      Left = 302
      Enabled = False
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 387
      Caption = '&Salir'
      ModalResult = 0
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      Kind = bkCustom
    end
  end
  object SistemaGB: TGroupBox
    Left = 0
    Top = 28
    Width = 235
    Height = 409
    Align = alLeft
    Caption = 'Arbol Sistema'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object ArbolSistema: TTreeView
      Left = 2
      Top = 18
      Width = 231
      Height = 320
      Align = alClient
      DragMode = dmAutomatic
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Indent = 19
      ParentFont = False
      PopupMenu = PopSistema
      ReadOnly = True
      TabOrder = 0
      OnDragDrop = ArbolSistemaDragDrop
      OnDragOver = ArbolSistemaDragOver
    end
    object Panel2: TPanel
      Left = 2
      Top = 338
      Width = 231
      Height = 69
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object Gauge: TGauge
        Left = 3
        Top = 7
        Width = 224
        Height = 21
        Color = clBtnFace
        ForeColor = clNavy
        ParentColor = False
        Progress = 0
        Visible = False
      end
      object AplicaVistaClasica: TCheckBox
        Left = 8
        Top = 44
        Width = 121
        Height = 17
        Caption = 'Aplicar Estilo Cl'#225'sico'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = IncluirSistCBClick
      end
    end
  end
  object UsuarioGB: TGroupBox
    Left = 235
    Top = 28
    Width = 235
    Height = 409
    Align = alClient
    Caption = 'Arbol Usuario'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    object ArbolUsuario: TTreeView
      Left = 2
      Top = 18
      Width = 231
      Height = 320
      Align = alClient
      DragMode = dmAutomatic
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Indent = 19
      ParentFont = False
      PopupMenu = PopUsuaio
      TabOrder = 0
      OnDragDrop = ArbolUsuarioDragDrop
      OnDragOver = ArbolUsuarioDragOver
      OnEdited = ArbolUsuarioEdited
      OnMouseDown = ArbolUsuarioMouseDown
    end
    object Panel3: TPanel
      Left = 2
      Top = 338
      Width = 231
      Height = 69
      Align = alBottom
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      object Label1: TLabel
        Left = 5
        Top = 8
        Width = 37
        Height = 13
        Caption = 'Default:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object IncluirSistCB: TCheckBox
        Left = 48
        Top = 44
        Width = 121
        Height = 17
        Caption = 'Incluir Arbol Sistema'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = IncluirSistCBClick
      end
      object ArbolDefault: TTreeView
        Left = 48
        Top = 4
        Width = 178
        Height = 37
        DragMode = dmAutomatic
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Indent = 19
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        OnDragDrop = ArbolDefaultDragDrop
        OnDragOver = ArbolDefaultDragOver
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 470
    Height = 28
    Align = alTop
    Caption = 'Panel1'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    object ToolBar1: TToolBar
      Left = 1
      Top = 1
      Width = 235
      Height = 26
      Align = alClient
      Caption = 'ToolBar1'
      Flat = True
      Images = TressShell.ArbolImages
      TabOrder = 0
      object ToolButton2: TToolButton
        Left = 0
        Top = 0
        Action = _AgregaRama
      end
      object ToolButton8: TToolButton
        Left = 23
        Top = 0
        Width = 8
        Caption = 'ToolButton8'
        ImageIndex = 6
        Style = tbsSeparator
      end
      object ToolButton1: TToolButton
        Left = 31
        Top = 0
        Action = _BuscaRamaUsuario
      end
    end
    object ToolBar2: TToolBar
      Left = 236
      Top = 1
      Width = 233
      Height = 26
      Align = alRight
      Caption = 'ToolBar2'
      Flat = True
      Images = TressShell.ArbolImages
      TabOrder = 1
      object ToolButton3: TToolButton
        Left = 0
        Top = 0
        Action = _BorraRama
      end
      object ToolButton4: TToolButton
        Left = 23
        Top = 0
        Action = _BorraTodo
      end
      object ToolButton6: TToolButton
        Left = 46
        Top = 0
        Action = _Renombrar
      end
      object ToolButton9: TToolButton
        Left = 69
        Top = 0
        Width = 8
        Caption = 'ToolButton9'
        ImageIndex = 6
        Style = tbsSeparator
      end
      object ToolButton5: TToolButton
        Left = 77
        Top = 0
        Action = _NodoDefault
      end
      object ToolButton10: TToolButton
        Left = 100
        Top = 0
        Width = 8
        Caption = 'ToolButton10'
        ImageIndex = 6
        Style = tbsSeparator
      end
      object ToolButton7: TToolButton
        Left = 108
        Top = 0
        Action = _BuscaRamaUsuario
      end
      object ToolButton11: TToolButton
        Left = 131
        Top = 0
        Width = 8
        Caption = 'ToolButton11'
        ImageIndex = 6
        Style = tbsSeparator
      end
      object ToolButton12: TToolButton
        Left = 139
        Top = 0
        Action = _Exportar
      end
      object ToolButton13: TToolButton
        Left = 162
        Top = 0
        Action = _Importar
      end
    end
  end
  object PopUsuaio: TPopupMenu
    Images = TressShell.ArbolImages
    Left = 424
    Top = 152
    object LimpiarUsuario: TMenuItem
      Action = _BorraTodo
    end
    object BorraRama1: TMenuItem
      Action = _BorraRama
    end
    object Renombrar1: TMenuItem
      Action = _Renombrar
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object NodoDefaultItem: TMenuItem
      Action = _NodoDefault
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object BuscarRama1: TMenuItem
      Action = _BuscaRamaUsuario
    end
  end
  object PopSistema: TPopupMenu
    Images = TressShell.ArbolImages
    Left = 112
    Top = 159
    object BuscarRama2: TMenuItem
      Action = _BuscaRamaSistema
    end
  end
  object ActionListArbol: TActionList
    Images = TressShell.ArbolImages
    Left = 360
    Top = 153
    object _BorraTodo: TAction
      Caption = 'Borrar &Todo'
      Hint = 'Borra Todo el Arbol de Usuario'
      ImageIndex = 13
      OnExecute = _BorraTodoExecute
    end
    object _BorraRama: TAction
      Caption = '&Borrar Rama'
      Hint = 'Borra la Rama Seleccionada'
      ImageIndex = 7
      ShortCut = 46
      OnExecute = _BorraRamaExecute
    end
    object _Renombrar: TAction
      Caption = 'Renombrar'
      Hint = 'Cambia de Nombre la Rama Seleccionada'
      ImageIndex = 8
      ShortCut = 16466
      OnExecute = _RenombrarExecute
    end
    object _NodoDefault: TAction
      Caption = 'Nodo &Default'
      Hint = 'Marca la Forma que aparece abierta'
      ImageIndex = 20
      ShortCut = 16452
      OnExecute = _NodoDefaultExecute
    end
    object _BuscaRamaUsuario: TAction
      Caption = 'Busca Rama'
      Hint = 'Busca Forma en Arbol Usuario'
      ImageIndex = 5
      ShortCut = 16450
      OnExecute = _BuscaRamaUsuarioExecute
    end
    object _BuscaRamaSistema: TAction
      Caption = 'Busca Rama'
      Hint = 'Busca Forma en Arbol Sistema'
      ImageIndex = 5
      ShortCut = 16450
      OnExecute = _BuscaRamaSistemaExecute
    end
    object _AgregaRama: TAction
      Caption = '&Agregar a Usuario'
      Hint = 'Agrega Rama al Arbol de Usuario'
      ImageIndex = 6
      OnExecute = _AgregaRamaExecute
    end
    object _Exportar: TAction
      Caption = 'E&xportar'
      Hint = 'Exportar Arbol de Usuario'
      ImageIndex = 21
      OnExecute = _ExportarExecute
    end
    object _Importar: TAction
      Caption = '&Importar'
      Hint = 'Importar Arbol de Usuario'
      ImageIndex = 22
      OnExecute = _ImportarExecute
    end
  end
  object ExportaDlg: TSaveDialog
    DefaultExt = '*.arb'
    Filter = 'Arboles de Usuario (*.arb)|*.arb|Todos (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Exportar Arbol de Usuario'
    Left = 431
    Top = 60
  end
  object ImportaDlg: TOpenDialog
    DefaultExt = '*.arb'
    Filter = 'Arboles de Usuario (*.arb)|*.arb|Todos (*.*)|*.*'
    Title = 'Importar Arbol de Usuario'
    Left = 431
    Top = 92
  end
end
