inherited GlobalCapacitacion_DevEx: TGlobalCapacitacion_DevEx
  Left = 403
  Top = 203
  ActiveControl = Registro
  Caption = 'Capacitaci'#243'n'
  ClientHeight = 353
  ClientWidth = 397
  Color = clWhite
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Tag = 80
    Left = 13
    Top = 126
    Width = 80
    Height = 13
    Alignment = taRightJustify
    Caption = 'Giro de empresa:'
    Color = clWhite
    ParentColor = False
  end
  inherited PanelBotones: TPanel
    Top = 317
    Width = 397
    Color = clWhite
    TabOrder = 1
    inherited OK_DevEx: TcxButton
      Left = 230
      Top = 5
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 310
      Top = 5
    end
  end
  object Secretaria: TGroupBox [2]
    Left = 6
    Top = 7
    Width = 386
    Height = 101
    Caption = ' Secretar'#237'a de Trabajo y Previsi'#243'n Social '
    Color = clWhite
    ParentColor = False
    TabOrder = 0
    TabStop = True
    object Label2: TLabel
      Left = 98
      Top = 20
      Width = 67
      Height = 13
      Alignment = taRightJustify
      Caption = '# de Registro:'
    end
    object Label3: TLabel
      Left = 34
      Top = 44
      Width = 131
      Height = 13
      Alignment = taRightJustify
      Caption = 'Representante de empresa:'
    end
    object Label4: TLabel
      Left = 23
      Top = 67
      Width = 142
      Height = 13
      Alignment = taRightJustify
      Caption = 'Representante de empleados:'
    end
    object Registro: TEdit
      Tag = 81
      Left = 168
      Top = 16
      Width = 209
      Height = 21
      TabOrder = 0
    end
    object RepresentanteEmpresa: TEdit
      Tag = 82
      Left = 168
      Top = 40
      Width = 209
      Height = 21
      TabOrder = 1
    end
    object RepresentanteEmpleados: TEdit
      Tag = 83
      Left = 168
      Top = 64
      Width = 209
      Height = 21
      TabOrder = 2
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 17
    Top = 224
    Width = 361
    Height = 81
    Caption = ' Criterios adicionales de programaci'#243'n '
    Color = clWhite
    ParentColor = False
    TabOrder = 2
    object Label5: TLabel
      Left = 57
      Top = 24
      Width = 35
      Height = 13
      Caption = 'Cursos:'
    end
    object Label6: TLabel
      Left = 20
      Top = 48
      Width = 72
      Height = 13
      Caption = 'Certificaciones:'
    end
    object CriterioAdicional: TComboBox
      Left = 96
      Top = 20
      Width = 145
      Height = 21
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 0
    end
    object cboCriterioCertific: TComboBox
      Left = 96
      Top = 46
      Width = 145
      Height = 21
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 1
    end
  end
  object GiroEmpresa: TcxMemo [4]
    Tag = 80
    Left = 96
    Top = 122
    Properties.ScrollBars = ssVertical
    TabOrder = 3
    Height = 89
    Width = 297
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
