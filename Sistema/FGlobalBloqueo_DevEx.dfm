inherited GlobalBloqueo_DevEx: TGlobalBloqueo_DevEx
  Left = 429
  Top = 290
  Caption = 'Bloqueo de Cambios de Asistencia'
  ClientHeight = 202
  ClientWidth = 404
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 166
    Width = 404
    Color = clWhite
    inherited OK_DevEx: TcxButton
      Left = 238
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 318
    end
  end
  object GroupBox3: TGroupBox [1]
    Left = 0
    Top = 0
    Width = 404
    Height = 166
    Align = alClient
    TabOrder = 1
    object gbBloqueStatus: TGroupBox
      Left = 2
      Top = 112
      Width = 398
      Height = 44
      Color = clWhite
      ParentColor = False
      TabOrder = 0
      object StatusLimite2lbl: TLabel
        Left = 33
        Top = 20
        Width = 169
        Height = 13
        Alignment = taRightJustify
        Caption = '&Status l'#237'mite para ajustar asistencia:'
        FocusControl = StatusLimite2
      end
      object StatusLimite2: TZetaKeyCombo
        Left = 205
        Top = 16
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 0
        ListaFija = lfStatusPeriodo
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object rbBloqueoStatus: TRadioButton
        Left = 8
        Top = 0
        Width = 181
        Height = 17
        Caption = 'Bloqueo por status de la n'#243'mina'
        TabOrder = 1
        OnClick = rbBloqueoStatusClick
      end
    end
    object gbBloqueoFecha: TGroupBox
      Left = 2
      Top = 8
      Width = 398
      Height = 97
      Color = clWhite
      ParentColor = False
      TabOrder = 1
      object StatusLimiteLBL: TLabel
        Left = 33
        Top = 22
        Width = 169
        Height = 13
        Alignment = taRightJustify
        Caption = '&Status l'#237'mite para ajustar asistencia:'
        FocusControl = StatusLimite
      end
      object TipoNominaLBL: TLabel
        Left = 20
        Top = 47
        Width = 182
        Height = 13
        Alignment = taRightJustify
        Caption = '&Tipo de n'#243'mina para modificar tarjetas:'
      end
      object LimiteCambioTarjetalbl: TLabel
        Left = 38
        Top = 72
        Width = 164
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha l'#237'mite de cambios a tarjetas:'
      end
      object FechaLimite: TZetaTextBox
        Left = 205
        Top = 70
        Width = 80
        Height = 21
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object bbEditarFecha: TcxButton
        Left = 286
        Top = 70
        Width = 21
        Height = 21
        Hint = 'Editar Fecha L'#237'mite de Cambios a Tarjetas'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = bbEditarFechaClick
        OptionsImage.Glyph.Data = {
          DE020000424DDE0200000000000036000000280000000D000000110000000100
          180000000000A8020000C30E0000C30E000000000000000000003E3CBF3E3CC0
          3C3ABF3D3BBF3C3ABF3E3CC03D3BBF3C3ABF3D3BC03D3BBF3D3BC03D3BC03E3C
          C0003E3CC13E3CC03E3CC03E3CC03F3DC03F3DC03F3DC04240C24442C23D3BC0
          3D3BC04947C43C3ABF003E3CC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF3D3BBF00403EC0FFFFFFFFFFFFFFFFFF6557
          674B3B4D4B3B4D6B5E6CFFFFFFFFFFFFFFFFFFFFFFFF3E3CC0003D3BC0FFFFFF
          FFFFFF7D737F4C3C4EFCFBFCFDFDFD5B4C5D6C606EFFFFFFFFFFFFFFFFFF4240
          C1004645C2FFFFFFFFFFFF877C88ECEAECFFFFFFFFFFFFECEAED4D3D4FFFFFFF
          FFFFFFFFFFFF3F3DC000403EC1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F74B
          3B4D7F7481FFFFFFFFFFFFFFFFFF3D3BBF003D3BC0FFFFFFFFFFFFFFFFFFFFFF
          FF8B818C4B3B4D7D717FD1CED2FFFFFFFFFFFFFFFFFF3E3CC0003F3DC1FFFFFF
          FFFFFFFFFFFFFFFFFFFEFEFEFFFFFF4B3C4DC5C0C6FFFFFFFFFFFFFFFFFF3D3B
          BFFF3C3ABFFFFFFFFFFFFFFFFFFF4B3B4DFFFFFFFFFFFF4B3B4DFEFEFEFFFFFF
          FFFFFFFFFFFF3D3BBF003F3DC0FFFFFFFFFFFFFFFFFF8A808C4B3B4D4C3C4E85
          7A86FFFFFFFFFFFFFFFFFFFFFFFF3C3ABF003F3DC1FFFFFFFFFFFFFFFFFFFFFF
          FFFEFEFEFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFF3F3DC0003E3CC03E3CC0
          4240C13E3CC03F3DC04C4BC5403EC14947C33F3DC0413FC13E3CC03C3ABF3C3A
          BFFF3E3CC03C3ABF3C3ABF3C3ABF3C3ABF3C3ABF3C3ABF3C3ABF3C3ABF3C3ABF
          3C3ABF3C3ABF3C3ABF003C3ABF3C3ABFE8E8F7F3F3FBF9F9FD3E3CC03C3ABF3C
          3ABFFAFAFDF3F3FBF3F3FB3C3ABF3C3ABF007878D23C3ABFF3F3FB3C3ABFE2E2
          F5413FC03C3ABF3C3ABFF3F3FB3C3ABFEFEFFA3C3ABF7371D000FFFFFFF4F4FB
          FEFEFF7978D2EFEFFAF6F6FCF3F3FBF3F3FBFBFBFE6E6CCFFDFDFEF4F4FBFFFF
          FFFF}
      end
      object StatusLimite: TZetaKeyCombo
        Left = 205
        Top = 18
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 0
        ListaFija = lfStatusPeriodo
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object TipoNomina: TZetaKeyCombo
        Left = 205
        Top = 44
        Width = 190
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 1
        ListaFija = lfTipoPeriodo
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = True
      end
      object rbBloqueoFecha: TRadioButton
        Left = 8
        Top = 0
        Width = 145
        Height = 17
        Caption = 'Bloqueo por fecha l'#237'mite'
        TabOrder = 2
        OnClick = rbBloqueoFechaClick
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
