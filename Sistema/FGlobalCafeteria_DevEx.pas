unit FGlobalCafeteria_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     ZBaseGlobal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, ImgList, cxButtons;

type
  TGlobalCafeteria_DevEx = class(TBaseGlobal_DevEx)
    Label1: TLabel;
    Letrero: TEdit;
    AgregarconError: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GlobalCafeteria_DevEx: TGlobalCafeteria_DevEx;

implementation

{$R *.DFM}

uses ZGlobalTress,
     ZAccesosTress,
     ZetaCommonClasses;

procedure TGlobalCafeteria_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos       := ZAccesosTress.D_CAT_CONFI_CAFETERIA;
     Letrero.Tag         := K_GLOBAL_LETRERO;
     AgregarconError.Tag := K_GLOBAL_AGREGAR_CON_ERROR;
     HelpContext := H65111_globales_de_cafeteria;
end;

procedure TGlobalCafeteria_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  Letrero.SetFocus;
end;

end.
