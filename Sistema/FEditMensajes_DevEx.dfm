inherited EditMensajes_DevEx: TEditMensajes_DevEx
  Caption = 'Mensajes'
  ClientHeight = 172
  ClientWidth = 436
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 29
    Top = 42
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
  end
  object Label2: TLabel [1]
    Left = 22
    Top = 66
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Mensaje:'
  end
  object Label3: TLabel [2]
    Left = 33
    Top = 90
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Orden:'
  end
  object Label4: TLabel [3]
    Left = 29
    Top = 114
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Espera:'
  end
  object Label6: TLabel [4]
    Left = 122
    Top = 115
    Width = 42
    Height = 13
    Alignment = taRightJustify
    Caption = '(minutos)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  inherited PanelBotones: TPanel
    Top = 136
    Width = 436
    TabOrder = 5
    inherited OK_DevEx: TcxButton
      Left = 270
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 350
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 436
    TabOrder = 7
    inherited ValorActivo2: TPanel
      Width = 110
      inherited textoValorActivo2: TLabel
        Width = 104
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Left = 0
    Top = 0
  end
  object DM_ORDEN: TZetaDBNumero [8]
    Left = 69
    Top = 87
    Width = 50
    Height = 21
    Mascara = mnEmpleado
    TabOrder = 3
    DataField = 'DM_ORDEN'
    DataSource = DataSource
  end
  object DM_SLEEP: TZetaDBNumero [9]
    Left = 69
    Top = 111
    Width = 50
    Height = 21
    Mascara = mnEmpleado
    TabOrder = 4
    DataField = 'DM_SLEEP'
    DataSource = DataSource
  end
  object DM_ACTIVO: TDBCheckBox [10]
    Left = 364
    Top = 41
    Width = 52
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Activo:'
    DataField = 'DM_ACTIVO'
    DataSource = DataSource
    TabOrder = 1
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object ZetaDBEdit1: TZetaDBEdit [11]
    Left = 69
    Top = 63
    Width = 348
    Height = 21
    TabOrder = 2
    Text = 'DM_MENSAJE'
    DataField = 'DM_MENSAJE'
    DataSource = DataSource
  end
  object DM_CODIGO: TZetaDBNumero [12]
    Left = 69
    Top = 38
    Width = 50
    Height = 21
    Mascara = mnEmpleado
    TabOrder = 0
    DataField = 'DM_CODIGO'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 4
    Top = 57
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
