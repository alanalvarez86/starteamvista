unit FImpresoras_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db,  ExtCtrls, Grids, DBGrids,
     ZBaseGridLectura_DevEx,
     ZetaDBGrid, StdCtrls, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TImpresoras_DevEx = class(TBaseGridLectura_DevEx)
    PI_NOMBRE: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  Impresoras_DevEx: TImpresoras_DevEx;

implementation

uses DSistema,
     ZetaCommonClasses;

{$R *.DFM}

procedure TImpresoras_DevEx.Connect;
begin
     with dmSistema do
     begin
          cdsImpresoras.Conectar;
          DataSource.DataSet := cdsImpresoras;
     end;
end;

procedure TImpresoras_DevEx.Refresh;
begin
     dmSistema.cdsImpresoras.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TImpresoras_DevEx.Agregar;
begin
     dmSistema.cdsImpresoras.Agregar;
end;

procedure TImpresoras_DevEx.Borrar;
begin
     dmSistema.cdsImpresoras.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TImpresoras_DevEx.Modificar;
begin
     dmSistema.cdsImpresoras.Modificar;
end;

procedure TImpresoras_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_CONS_IMPRESORAS;
     {$endif}
     {$ifdef WORKFLOWCFG}
     HelpContext := H_WORKFLOWCFG_SISTEMA_IMPRESORA;
     {$endif}
end;

procedure TImpresoras_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  {***Banda Sumatoria***}
  CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  inherited;
end;

end.
