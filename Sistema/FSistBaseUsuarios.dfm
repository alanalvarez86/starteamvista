inherited SistBaseUsuarios: TSistBaseUsuarios
  Left = 580
  Top = 187
  Caption = 'Usuarios'
  ClientHeight = 170
  ClientWidth = 644
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 644
    inherited Slider: TSplitter
      Left = 357
    end
    inherited ValorActivo1: TPanel
      Width = 341
    end
    inherited ValorActivo2: TPanel
      Left = 360
      Width = 284
    end
  end
  object ZetaDBGrid: TZetaDBGrid [1]
    Left = 0
    Top = 49
    Width = 644
    Height = 121
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDrawColumnCell = ZetaDBGridDrawColumnCell
    Columns = <
      item
        Expanded = False
        FieldName = 'us_codigo'
        Title.Caption = 'N'#250'mero'
        Width = 47
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'us_corto'
        Title.Caption = 'Usuario'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'us_nombre'
        Title.Caption = 'Nombre'
        Width = 241
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'GR_DESCRIP'
        Title.Caption = 'Grupo'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_ACTIVO'
        Title.Caption = 'Activo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_DENTRO'
        Title.Caption = 'Dentro'
        Width = 38
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_LUGAR'
        Title.Caption = 'Tel'#233'fono'
        Width = 65
        Visible = True
      end>
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 19
    Width = 644
    Height = 30
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 17
      Top = 8
      Width = 33
      Height = 13
      Caption = 'Busca:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object BBusca: TSpeedButton
      Left = 177
      Top = 1
      Width = 27
      Height = 27
      Hint = 'Buscar Reporte'
      AllowAllUp = True
      GroupIndex = 1
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D808DDDDDDDD
        DDDD83308DDDDDDDDDDD873308DDDDDDDDDDD373308DDDDDDDDDDD3733088888
        8DDDDDD37330000088DDDDDD37338777808DDDDDD038F7F77808DDDDD08FCCCC
        C788DDDCD0F7C7F7C770DDDCC07FCCCCCF70DDDCD0F7CFFCF770DDDDC88FCCCC
        7F88DDDDDD08F7F7F80DDDDDDDD08F7F80DDDDDDDDDD80008DDD}
      ParentShowHint = False
      ShowHint = True
      OnClick = BBuscaClick
    end
    object Label2: TLabel
      Left = 210
      Top = 8
      Width = 33
      Height = 13
      Caption = 'Status:'
    end
    object lblGrupo: TLabel
      Left = 334
      Top = 8
      Width = 32
      Height = 13
      Caption = 'Grupo:'
    end
    object EBuscaNombre: TEdit
      Left = 52
      Top = 4
      Width = 121
      Height = 21
      TabOrder = 0
      OnChange = EBuscaNombreChange
    end
    object FiltroActivos: TZetaKeyCombo
      Left = 246
      Top = 5
      Width = 85
      Height = 21
      AutoComplete = False
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
      OnChange = FiltroActivosChange
      ListaFija = lfFiltroStatusEmpleado
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
    object FiltroGrupo: TZetaKeyCombo
      Left = 372
      Top = 4
      Width = 125
      Height = 21
      AutoComplete = False
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
      OnChange = FieldGroupChange
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
  end
  inherited DataSource: TDataSource
    Left = 24
    Top = 96
  end
end
