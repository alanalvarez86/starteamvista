unit FEditSistListaGrupos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion_DevEx, DB, ExtCtrls, DBCtrls, ZetaSmartLists, Buttons,
  StdCtrls, ZetaEdit, Mask, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TEditSistListaGrupos_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label2: TLabel;
    Label1: TLabel;
    GP_CODIGO: TZetaDBEdit;
    GP_DESCRIP: TDBEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
  end;

var
  EditSistListaGrupos_DevEx: TEditSistListaGrupos_DevEx;

implementation

uses dSistema,
     ZetaCommonClasses,
     ZAccesosTress;

{$R *.dfm}

{ TEditSistListaGrupos }

procedure TEditSistListaGrupos_DevEx.Connect;
begin
     inherited;
     {$ifndef DOS_CAPAS}
     with dmSistema do
     begin
          cdsListaGrupos.Conectar;
          DataSource.DataSet := cdsListaGrupos;
     end;
     {$endif}
end;

procedure TEditSistListaGrupos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_TRESS_SISTEMA_TERMINALESGTI_GRUPOS_EDIC;
     IndexDerechos := ZAccesosTress.D_SIST_LISTA_GRUPOS ;
     FirstControl := GP_CODIGO;
end;

end.
