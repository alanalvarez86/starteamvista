inherited SistBaseEditUsuarios: TSistBaseEditUsuarios
  Left = 300
  Top = 227
  Caption = 'Usuarios'
  ClientHeight = 380
  ClientWidth = 426
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 347
    Width = 426
    Height = 33
    TabOrder = 4
    DesignSize = (
      426
      33)
    inherited OK: TBitBtn
      Left = 267
      Top = 3
      Height = 27
      Anchors = [akTop, akRight]
    end
    inherited Cancelar: TBitBtn
      Left = 345
      Top = 3
      Height = 27
      Anchors = [akTop, akRight]
    end
    object Suspender: TBitBtn
      Left = 4
      Top = 3
      Width = 111
      Height = 27
      Caption = '&Suspende Todos'
      TabOrder = 2
      TabStop = False
      OnClick = SuspenderClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
        333333333337FF3333333333330003333333333333777F333333333333080333
        3333333F33777FF33F3333B33B000B33B3333373F777773F7333333BBB0B0BBB
        33333337737F7F77F333333BBB0F0BBB33333337337373F73F3333BBB0F7F0BB
        B333337F3737F73F7F3333BB0FB7BF0BB3333F737F37F37F73FFBBBB0BF7FB0B
        BBB3773F7F37337F377333BB0FBFBF0BB333337F73F333737F3333BBB0FBF0BB
        B3333373F73FF7337333333BBB000BBB33333337FF777337F333333BBBBBBBBB
        3333333773FF3F773F3333B33BBBBB33B33333733773773373333333333B3333
        333333333337F33333333333333B333333333333333733333333}
      NumGlyphs = 2
    end
    object Activar: TBitBtn
      Left = 118
      Top = 3
      Width = 109
      Height = 27
      Caption = '&Activa Todos'
      TabOrder = 3
      TabStop = False
      OnClick = ActivarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
        333333333337FF3333333333330003333333333333777F333333333333080333
        3333333333777F3333333333330003333333333333777F3333333333330F0333
        33333333337F7F3333333333330F033333333333337373F33333333330F7F033
        333333333737F73F333333330FF7FF03333333337F37F37F333333330FF7FF03
        333333337F37337F333333330FFFFF033333333373F333733333333330FFF033
        33333333373FF733333333333300033333333333337773333333333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
    end
  end
  inherited PanelSuperior: TPanel
    Width = 426
    TabOrder = 0
  end
  inherited PanelIdentifica: TPanel
    Width = 426
    TabOrder = 1
    inherited ValorActivo2: TPanel
      Width = 100
    end
  end
  object PageControl: TPageControl [3]
    Left = 0
    Top = 98
    Width = 426
    Height = 249
    ActivePage = Acceso
    Align = alClient
    TabOrder = 3
    object Acceso: TTabSheet
      Caption = 'Acceso'
      object Label5: TLabel
        Left = 59
        Top = 5
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nom&bre:'
        FocusControl = US_NOMBRE
      end
      object Label6: TLabel
        Left = 69
        Top = 26
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cla&ve:'
        FocusControl = US_PASSWRD
      end
      object Label3: TLabel
        Left = 67
        Top = 68
        Width = 32
        Height = 13
        Alignment = taRightJustify
        Caption = '&Grupo:'
        FocusControl = GR_CODIGO
      end
      object Label8: TLabel
        Left = 55
        Top = 89
        Width = 44
        Height = 13
        Alignment = taRightJustify
        Caption = '&Prioridad:'
        FocusControl = US_NIVEL
      end
      object Label11: TLabel
        Left = 54
        Top = 110
        Width = 45
        Height = 13
        Caption = '&Tel'#233'fono:'
        FocusControl = US_LUGAR
      end
      object US_NOMBRE: TDBEdit
        Left = 101
        Top = 1
        Width = 313
        Height = 21
        DataField = 'US_NOMBRE'
        DataSource = DataSource
        TabOrder = 0
      end
      object US_PASSWRD: TDBEdit
        Left = 101
        Top = 22
        Width = 145
        Height = 21
        DataField = 'US_PASSWRD'
        DataSource = DataSource
        MaxLength = 14
        PasswordChar = '*'
        TabOrder = 1
      end
      object US_CAMBIA: TDBCheckBox
        Left = 25
        Top = 46
        Width = 89
        Height = 15
        Alignment = taLeftJustify
        Caption = 'Ca&mbiar Clave:'
        DataField = 'US_CAMBIA'
        DataSource = DataSource
        TabOrder = 2
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object GR_CODIGO: TZetaDBKeyLookup
        Left = 101
        Top = 64
        Width = 315
        Height = 21
        Opcional = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 40
        DataField = 'GR_CODIGO'
        DataSource = DataSource
      end
      object US_NIVEL: TZetaDBKeyCombo
        Left = 101
        Top = 85
        Width = 145
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 4
        ListaFija = lfNivelUsuario
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        DataField = 'US_NIVEL'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 125
        Width = 418
        Height = 96
        Align = alBottom
        Caption = ' Acceso Al Sistema '
        TabOrder = 6
        object Label9: TLabel
          Left = 29
          Top = 19
          Width = 72
          Height = 13
          Alignment = taRightJustify
          Caption = 'Ultima Entrada:'
        end
        object Label10: TLabel
          Left = 37
          Top = 38
          Width = 64
          Height = 13
          Alignment = taRightJustify
          Caption = 'Ultima Salida:'
        end
        object US_FEC_IN: TZetaDBTextBox
          Left = 102
          Top = 17
          Width = 81
          Height = 17
          AutoSize = False
          Caption = 'US_FEC_IN'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'US_FEC_IN'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object US_FEC_OUT: TZetaDBTextBox
          Left = 102
          Top = 36
          Width = 81
          Height = 17
          AutoSize = False
          Caption = 'US_FEC_OUT'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'US_FEC_OUT'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label12: TLabel
          Left = 11
          Top = 57
          Width = 90
          Height = 13
          Alignment = taRightJustify
          Caption = 'Ultima Suspensi'#243'n:'
        end
        object US_FEC_SUS: TZetaDBTextBox
          Left = 102
          Top = 55
          Width = 81
          Height = 17
          AutoSize = False
          Caption = 'US_FEC_SUS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'US_FEC_SUS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object US_MAQUINA: TZetaDBTextBox
          Left = 102
          Top = 74
          Width = 251
          Height = 17
          AutoSize = False
          Caption = 'US_MAQUINA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'US_MAQUINA'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label13: TLabel
          Left = 23
          Top = 76
          Width = 76
          Height = 13
          Alignment = taRightJustify
          Caption = 'Ultima M'#225'quina:'
        end
        object US_BLOQUEA: TDBCheckBox
          Left = 296
          Top = 16
          Width = 56
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Pro&hibir:'
          DataField = 'US_BLOQUEA'
          DataSource = DataSource
          TabOrder = 0
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object US_DENTRO: TDBCheckBox
          Left = 242
          Top = 34
          Width = 110
          Height = 14
          Alignment = taLeftJustify
          Caption = '&Dentro del Sistema:'
          DataField = 'US_DENTRO'
          DataSource = DataSource
          TabOrder = 1
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
      end
      object US_LUGAR: TDBEdit
        Left = 101
        Top = 106
        Width = 145
        Height = 21
        DataField = 'US_LUGAR'
        DataSource = DataSource
        TabOrder = 5
      end
    end
    object Operacion: TTabSheet
      Caption = 'Operaci'#243'n'
      ImageIndex = 1
      object gbEMail: TGroupBox
        Left = 0
        Top = 0
        Width = 418
        Height = 93
        Align = alTop
        Caption = ' Correo Electr'#243'nico '
        TabOrder = 0
        object Label4: TLabel
          Left = 22
          Top = 24
          Width = 48
          Height = 13
          Caption = '&Direcci'#243'n:'
          FocusControl = US_EMAIL
        end
        object Label7: TLabel
          Left = 14
          Top = 48
          Width = 56
          Height = 26
          Alignment = taRightJustify
          Caption = '&Formato de Reportes:'
          FocusControl = US_FORMATO
          WordWrap = True
        end
        object btnSuscripciones: TBitBtn
          Left = 268
          Top = 50
          Width = 124
          Height = 25
          Caption = '&Reportes a Recibir'
          TabOrder = 2
          OnClick = btnSuscripcionesClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000000000
            0000377777777777777707FFFFFFFFFFFF70773FF33333333F770F77FFFFFFFF
            77F07F773FFFFFFF77F70FFF7700000000007F337777777777770FFFFF0FFFFF
            FFF07F333F7F3FFFF3370FFF700F0000FFF07F3F777F777733370F707F0FFFFF
            FFF07F77337F3FFFFFF7007EEE0F000000F077FFFF7F777777370777770FFFFF
            FFF07777777F3FFFFFF7307EEE0F000000F03773FF7F7777773733707F0FFFFF
            FFF03337737F3FFF33373333700F000FFFF03333377F77733FF73333330FFFFF
            00003333337F3FF377773333330F00FF0F033333337F77337F733333330FFFFF
            00333333337FFFFF773333333300000003333333337777777333}
          NumGlyphs = 2
        end
        object US_EMAIL: TDBEdit
          Left = 72
          Top = 20
          Width = 321
          Height = 21
          DataField = 'US_EMAIL'
          DataSource = DataSource
          TabOrder = 0
        end
        object US_FORMATO: TZetaDBKeyCombo
          Left = 72
          Top = 52
          Width = 165
          Height = 21
          AutoComplete = False
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 1
          Items.Strings = (
            'Mensaje en HTML'
            'Archivo adjunto en HTML')
          ListaFija = lfEmailFormato
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          DataField = 'US_FORMATO'
          DataSource = DataSource
          LlaveNumerica = True
        end
      end
    end
  end
  object Pn_Codigos: TPanel [4]
    Left = 0
    Top = 51
    Width = 426
    Height = 47
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 64
      Top = 6
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = '&N'#250'mero:'
    end
    object Label2: TLabel
      Left = 65
      Top = 27
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = '&Usuario:'
      FocusControl = US_CORTO
    end
    object US_CORTO: TDBEdit
      Left = 106
      Top = 23
      Width = 145
      Height = 21
      DataField = 'US_CORTO'
      DataSource = DataSource
      TabOrder = 1
    end
    object US_CODIGO: TZetaDBNumero
      Left = 106
      Top = 2
      Width = 49
      Height = 21
      Mascara = mnDias
      TabOrder = 0
      Text = '0'
      DataField = 'US_CODIGO'
      DataSource = DataSource
    end
  end
  inherited DataSource: TDataSource
    Left = 398
    Top = 6
  end
end
