unit FSistArbolGrupos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, CheckLst, Buttons, ExtCtrls,
     ZBaseDlgModal, ComCtrls, Db, DBClient, ImgList;

type
  TSistArbolGrupos = class(TZetaDlgModal)
    ArbolGrupos: TTreeView;
    ImageList: TImageList;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ArbolGruposDragDrop( Sender, Source: TObject; X, Y: Integer );
    procedure ArbolGruposDragOver( Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean );
    procedure CancelarClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
    function CambiaGrupoPadre( const sCodigoOrigen, sCodigoDestino: string ): boolean;
    function GetTextoNodo( const oNodo: TTreeNode ): String;
    function HayCambios: boolean;
    function InitArbolGrupos: TTreeNode;
    procedure CreaArbolGrupos;
    procedure HabilitaControles;
    procedure CancelarCambios;
    procedure EscribirCambios;
  public
    { Public declarations }
  end;

var
   SistArbolGrupos: TSistArbolGrupos;

implementation

uses ZetaClientDataSet,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     ZCerrarEdicion,
     ZTreeTools,
     ZGlobalTress,
     DGlobal,
     DCliente,
     DSistema;

{$R *.DFM}

procedure TSistArbolGrupos.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H80819_Mapa_de_Grupos_de_usuarios;
end;

procedure TSistArbolGrupos.FormShow(Sender: TObject);
begin
     inherited;
     HabilitaControles;
     CreaArbolGrupos;
     ArbolGrupos.DragMode := dmAutomatic    //Propiedad para que si de permita el Drag and Drop en el Arbol

     {if ( dmCliente.GetGrupoActivo = ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION ) then
        ArbolGrupos.DragMode := dmAutomatic    //Propiedad para que si de permita el Drag and Drop en el Arbol
     else
         ArbolGrupos.DragMode := dmManual; //}
end;

procedure TSistArbolGrupos.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     inherited;
     if HayCambios then
     begin
          case ZCerrarEdicion.CierraEdicion of
               mrOk:
               begin
                    EscribirCambios;
                    CanClose := not HayCambios;
               end;
               mrIgnore:
               begin
                    CancelarCambios;
                    CanClose := not HayCambios;
               end;
          else
              CanClose := False;
          end;
     end;
end;

function TSistArbolGrupos.InitArbolGrupos: TTreeNode;
var
   sPadre: string;
begin
     with dmSistema.cdsGrupos do
     begin
          sPadre := IntToStr( dmCliente.GetGrupoActivo );
          if Locate( 'GR_CODIGO', sPadre, [] ) then
             Result := ZTreeTools.InitArbol( ArbolGrupos, ImageList, sPadre + ZTreeTools.K_SEPARADOR + FieldByName( 'GR_DESCRIP' ).AsString )
          else
              raise Exception.Create( 'ˇ No Existe Grupo Activo !' );
     end;
end;

procedure TSistArbolGrupos.CreaArbolGrupos;
var
   oPadre: TTreeNode;
   {
   sOldIndex: string;
   Pos: TBookMark;
   }
begin
     {
     with dmSistema.cdsGrupos do
     begin
          DisableControls;
          sOldIndex := IndexFieldNames;
          try
             Pos := GetBookMark;
             try
             }
                oPadre := InitArbolGrupos;
                ZTreeTools.CreaArbol( ArbolGrupos, ImageList, dmSistema.cdsGrupos, oPadre, 'GR_PADRE', 'GR_CODIGO', 'GR_DESCRIP' );
             {
             finally
                    if ( Pos <> nil ) then
                    begin
                         GotoBookMark( Pos );
                         FreeBookMark( Pos );
                    end;
             end;
          finally
                 IndexFieldNames:= sOldIndex;
                 EnableControls;
          end;
     end;
     }
end;

procedure TSistArbolGrupos.HabilitaControles;
begin
     OK.Enabled := HayCambios;
     if OK.Enabled then
     begin
          with Cancelar do
          begin
               Kind := bkCancel;
               ModalResult := mrNone;
               Cancel := False;
               Caption := '&Cancelar';
               Hint := 'Cancelar Cambios';
          end;
     end
     else
     begin
          with Cancelar do
          begin
               Kind := bkClose;
               ModalResult := mrNone;
               Cancel := True;
               Caption := '&Salir';
               Hint := 'Cerrar Pantalla y Salir';
               if Self.Active then
                  SetFocus;
          end;
     end;
end;

function TSistArbolGrupos.CambiaGrupoPadre(const sCodigoOrigen, sCodigoDestino: string): boolean;
begin
     with dmSistema.cdsGrupos do
     begin
          Result := Locate( 'GR_CODIGO', sCodigoOrigen, [] );
          if ( Result ) then
          begin
               try
                  Edit;
                  FieldByName( 'GR_PADRE' ).AsString := sCodigoDestino;
                  Post;
               except
                     Result := False;
                     Cancel;
               end;
          end;
     end;
end;

function TSistArbolGrupos.GetTextoNodo( const oNodo: TTreeNode ): String;
var
   sTemporal: string;
begin
     sTemporal := oNodo.Text;
     Result := copy( sTemporal, 1, pos( K_SEPARADOR, sTemporal ) - 1 );
end;

function TSistArbolGrupos.HayCambios: boolean;
begin
     Result := ( dmSistema.cdsGrupos.ChangeCount > 0 );
end;

procedure TSistArbolGrupos.CancelarCambios;
begin
     dmSistema.cdsGrupos.CancelUpdates;
     HabilitaControles;
     CreaArbolGrupos;
end;

procedure TSistArbolGrupos.EscribirCambios;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             dmSistema.cdsGrupos.Enviar;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

procedure TSistArbolGrupos.ArbolGruposDragDrop(Sender, Source: TObject; X, Y: Integer);
var
   oNodoOrigen, oNodoDestino: TTreeNode;
   sCodigoOrigen, sCodigoDestino: string;
begin
     inherited;
     oNodoOrigen:= ArbolGrupos.Selected;
     oNodoDestino:= ArbolGrupos.GetNodeAt( X, Y );
     sCodigoOrigen := GetTextoNodo( oNodoOrigen );
     sCodigoDestino := GetTextoNodo( oNodoDestino );
     if ZTreeTools.EsNodo( oNodoDestino ) then
     begin
          if CambiaGrupoPadre( sCodigoOrigen, sCodigoDestino ) then
          begin
               ArbolGrupos.Items.BeginUpdate;
               try
                  oNodoOrigen.MoveTo( oNodoDestino, naAddChild );
                  oNodoDestino.Expand( True );
                  HabilitaControles;
               finally
                      ArbolGrupos.Items.EndUpdate;
               end;
          end;
     end;
end;

procedure TSistArbolGrupos.ArbolGruposDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
var
   oNodoDestino, oNodoSeleccionado: TTreeNode;
begin
     inherited;
     Accept := ( Sender is TTreeView );
     if ( Accept ) then
     begin
          with TTreeView( Sender ) do
          begin
               oNodoDestino := GetNodeAt( X, Y );
               oNodoSeleccionado := Selected;
               Accept:= ( ZTreeTools.EsNodo( oNodoDestino ) ) and ( oNodoDestino <> oNodoSeleccionado ) and
                        ( not oNodoDestino.HasAsParent( oNodoSeleccionado ) );  //Esta Condición es para evitar la Relación Circular
          end;
     end;
end;

procedure TSistArbolGrupos.OKClick(Sender: TObject);
begin
     inherited;
     EscribirCambios;
end;

procedure TSistArbolGrupos.CancelarClick(Sender: TObject);
begin
     inherited;
     CancelarCambios;
end;

end.
