inherited GlobalSeguridad: TGlobalSeguridad
  Left = 49
  Top = 359
  Caption = 'Seguridad'
  ClientHeight = 268
  ClientWidth = 450
  PixelsPerInch = 96
  TextHeight = 13
  object eExpiracionLBL: TLabel [0]
    Left = 99
    Top = 12
    Width = 89
    Height = 13
    Alignment = taRightJustify
    Caption = '&Claves Expiran En:'
    FocusControl = eExpiracion
  end
  object eExpiracionPFX: TLabel [1]
    Left = 237
    Top = 12
    Width = 23
    Height = 13
    Caption = 'D'#237'as'
  end
  object eLimitePassLBL: TLabel [2]
    Left = 59
    Top = 36
    Width = 129
    Height = 13
    Alignment = taRightJustify
    Caption = 'Longitud &M'#237'nima De Clave:'
    FocusControl = eLimitePass
  end
  object eLimitePassPFX: TLabel [3]
    Left = 237
    Top = 36
    Width = 51
    Height = 13
    Caption = 'Caracteres'
  end
  object eIntentosLBL: TLabel [4]
    Left = 74
    Top = 132
    Width = 114
    Height = 13
    Alignment = taRightJustify
    Caption = 'Bloquear &Usuario A Los:'
    FocusControl = eIntentos
  end
  object eIntentosPFX: TLabel [5]
    Left = 237
    Top = 132
    Width = 76
    Height = 13
    Caption = 'Intentos Fallidos'
  end
  object eDiasInactivosLBL: TLabel [6]
    Left = 74
    Top = 156
    Width = 114
    Height = 13
    Alignment = taRightJustify
    Caption = '&Bloquear Usuario A Los:'
    FocusControl = eDiasInactivos
  end
  object eDiasInactivosPFX: TLabel [7]
    Left = 237
    Top = 156
    Width = 95
    Height = 13
    Caption = 'D'#237'as De Inactividad'
  end
  object eTiempoInactivoLBL: TLabel [8]
    Left = 29
    Top = 180
    Width = 159
    Height = 13
    Alignment = taRightJustify
    Caption = 'L'#237'mite de &Inactividad del Sistema:'
    FocusControl = eTiempoInactivo
  end
  object eTiempoInactivoPFX: TLabel [9]
    Left = 237
    Top = 180
    Width = 37
    Height = 13
    Caption = 'Minutos'
  end
  object eMinLetrasPassLBL: TLabel [10]
    Left = 11
    Top = 60
    Width = 177
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cantidad M'#237'nima de &Letras De Clave:'
    FocusControl = eMinLetrasPass
  end
  object eMinLetrasPassPFX: TLabel [11]
    Left = 237
    Top = 60
    Width = 29
    Height = 13
    Caption = 'Letras'
  end
  object eMinDigitosPassLBL: TLabel [12]
    Left = 6
    Top = 84
    Width = 182
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cantidad M'#237'nima de &D'#237'gitos De Clave:'
    FocusControl = eMinDigitosPass
  end
  object eMinDigitosPassPFX: TLabel [13]
    Left = 237
    Top = 84
    Width = 34
    Height = 13
    Caption = 'D'#237'gitos'
  end
  object eAlmacenarPassLBL: TLabel [14]
    Left = 31
    Top = 108
    Width = 157
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cantidad de Claves a &Almacenar:'
    FocusControl = eAlmacenarPass
  end
  object eAlmacenarPassPFX: TLabel [15]
    Left = 237
    Top = 108
    Width = 32
    Height = 13
    Caption = 'Claves'
  end
  object lblUsuarioTareasAutomaticas: TLabel [16]
    Left = 52
    Top = 204
    Width = 136
    Height = 13
    Alignment = taRightJustify
    Caption = 'Usuario Tareas Autom'#225'ticas:'
    FocusControl = eTiempoInactivo
  end
  inherited PanelBotones: TPanel
    Top = 232
    Width = 450
    TabOrder = 8
    inherited OK: TBitBtn
      Left = 287
    end
    inherited Cancelar: TBitBtn
      Left = 367
    end
  end
  object eExpiracion: TZetaNumero
    Left = 191
    Top = 8
    Width = 43
    Height = 21
    Mascara = mnDias
    TabOrder = 0
    Text = '0'
  end
  object eLimitePass: TZetaNumero
    Left = 191
    Top = 32
    Width = 43
    Height = 21
    Mascara = mnDias
    TabOrder = 1
    Text = '0'
  end
  object eIntentos: TZetaNumero
    Left = 191
    Top = 128
    Width = 43
    Height = 21
    Mascara = mnDias
    TabOrder = 5
    Text = '0'
  end
  object eDiasInactivos: TZetaNumero
    Left = 191
    Top = 152
    Width = 43
    Height = 21
    Mascara = mnDias
    TabOrder = 6
    Text = '0'
  end
  object eTiempoInactivo: TZetaNumero
    Left = 191
    Top = 176
    Width = 43
    Height = 21
    Mascara = mnDias
    TabOrder = 7
    Text = '0'
  end
  object eMinLetrasPass: TZetaNumero
    Left = 191
    Top = 56
    Width = 43
    Height = 21
    Mascara = mnDias
    TabOrder = 2
    Text = '0'
  end
  object eMinDigitosPass: TZetaNumero
    Left = 191
    Top = 80
    Width = 43
    Height = 21
    Mascara = mnDias
    TabOrder = 3
    Text = '0'
  end
  object eAlmacenarPass: TZetaNumero
    Left = 191
    Top = 104
    Width = 43
    Height = 21
    Mascara = mnDias
    TabOrder = 4
    Text = '0'
  end
  object eUsuarioTareasAutomaticas: TZetaKeyLookup
    Left = 191
    Top = 200
    Width = 250
    Height = 21
    TabOrder = 9
    TabStop = True
    WidthLlave = 43
  end
end
