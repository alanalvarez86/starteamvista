inherited SistListaGrupos_DevEx: TSistListaGrupos_DevEx
  Left = 478
  Top = 258
  Caption = 'Grupos'
  ClientHeight = 226
  ClientWidth = 519
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 519
    inherited ValorActivo2: TPanel
      Width = 260
      inherited textoValorActivo2: TLabel
        Width = 254
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 519
    Height = 207
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object GP_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'GP_CODIGO'
      end
      object GP_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'GP_DESCRIP'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 152
    Top = 8
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
