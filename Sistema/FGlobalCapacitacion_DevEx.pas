unit FGlobalCapacitacion_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     ZBaseGlobal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxMemo, ImgList, cxButtons;

type
  TGlobalCapacitacion_DevEx = class(TBaseGlobal_DevEx)
    Label1: TLabel;
    Secretaria: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Registro: TEdit;
    RepresentanteEmpresa: TEdit;
    RepresentanteEmpleados: TEdit;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    CriterioAdicional: TComboBox;
    Label6: TLabel;
    cboCriterioCertific: TComboBox;
    GiroEmpresa: TcxMemo;
    procedure FormCreate(Sender: TObject);
  private
     procedure LlenaCriterioAdicional;
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses DGlobal,
     ZAccesosTress,
     ZGlobalTress,
     ZetaCommonTools,
     ZetaCommonClasses;

{$R *.DFM}

procedure TGlobalCapacitacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos              := ZAccesosTress.D_CAT_CONFI_CAPACITACION;
     GiroEmpresa.Tag            := K_GLOBAL_GIRO_EMPRESA;
     Registro.Tag               := K_GLOBAL_REGISTRO_STPS;
     RepresentanteEmpresa.Tag   := K_GLOBAL_REPRESENTA_STPS_EMPRESA;
     RepresentanteEmpleados.Tag := K_GLOBAL_REPRESENTA_STPS_EMPLEADO;
     CriterioAdicional.Tag      := K_NIVEL_PROGRAMACION_CURSOS;
     cboCriterioCertific.Tag    := K_NIVEL_PROGRAMACION_CERTIFIC;
     LlenaCriterioAdicional;
     HelpContext:= H65108_Capacitacion;
end;

procedure TGlobalCapacitacion_DevEx.LlenaCriterioAdicional;
var
   i: Integer;
begin

     with CriterioAdicional.Items do
     begin
          BeginUpdate;
          Clear;
          try
             Add( '<Indefinido>' );
             Add( 'Clasificación' );
             Add( 'Turno' );
             for i := 1 to 9 do
             begin
                  with Global do
                  begin
                       if StrLleno( NombreNivel( i ) ) then
                          Add( NombreNivel( i ) )
                       else
                           Break;
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;
     cboCriterioCertific.Items := CriterioAdicional.Items;
end;

end.
