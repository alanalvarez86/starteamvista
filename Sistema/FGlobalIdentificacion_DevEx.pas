unit FGlobalIdentificacion_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Mask,
     {$ifndef VER130}MaskUtils,{$endif} 
     ZetaKeyCombo,
     ZBaseGlobal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, ZetaKeyLookup_DevEx, ImgList,
  cxButtons;

type
  TGlobalIdentificacion_DevEx = class(TBaseGlobal_DevEx)
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label15: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label21: TLabel;
    Label1: TLabel;
    Entidad: TZetaKeyLookup_DevEx;
    RazonSocial: TEdit;
    Calle: TEdit;
    Colonia: TEdit;
    Ciudad: TEdit;
    CodigoPostal: TEdit;
    Telefono: TEdit;
    RepresentanteLegal: TEdit;
    RFC: TMaskEdit;
    Infonavit: TMaskEdit;
    Label2: TLabel;
    Label3: TLabel;
    Numexterior: TEdit;
    Numinterior: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GlobalIdentificacion_DevEx: TGlobalIdentificacion_DevEx;

implementation

uses DTablas,
     ZAccesosTress,
     ZGlobalTress,
     ZetaCommonClasses;

{$R *.DFM}

procedure TGlobalIdentificacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos          := ZAccesosTress.D_CAT_CONFI_IDENTIFICA;
     RazonSocial.Tag        := K_GLOBAL_RAZON_EMPRESA;
     Calle.Tag              := K_GLOBAL_CALLE_EMPRESA;
     Colonia.Tag            := K_GLOBAL_COLONIA_EMPRESA;
     Ciudad.Tag             := K_GLOBAL_CIUDAD_EMPRESA;
     with Entidad do
     begin
          LookupDataset     := dmTablas.cdsEstado;
          Tag               := K_GLOBAL_ENTIDAD_EMPRESA;
     end;
     CodigoPostal.Tag       := K_GLOBAL_CP_EMPRESA;
     Telefono.Tag           := K_GLOBAL_TEL_EMPRESA;
     RepresentanteLegal.Tag := K_GLOBAL_REPRESENTANTE;
     RFC.Tag                := K_GLOBAL_RFC_EMPRESA;
     Infonavit.Tag          := K_GLOBAL_INFONAVIT_EMPRESA;
     HelpContext            := H65101_Identificacion;
     NumExterior.Tag        := K_GLOBAL_NUM_EXT_EMPRESA;
     NumInterior.Tag        := K_GLOBAL_NUM_INT_EMPRESA;
end;

procedure TGlobalIdentificacion_DevEx.FormShow(Sender: TObject);
begin
     dmTablas.cdsEstado.Conectar;
     inherited;
end;

end.
