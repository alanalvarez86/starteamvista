inherited CatRoles_DevEx: TCatRoles_DevEx
  Left = 327
  Top = 273
  Caption = 'Roles'
  ClientHeight = 257
  ClientWidth = 849
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 849
    inherited Slider: TSplitter
      Left = 357
    end
    inherited ValorActivo1: TPanel
      Width = 341
      inherited textoValorActivo1: TLabel
        Width = 335
      end
    end
    inherited ValorActivo2: TPanel
      Left = 360
      Width = 489
      inherited textoValorActivo2: TLabel
        Width = 483
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 849
    Height = 238
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = DataSource
      object RO_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'RO_CODIGO'
        MinWidth = 80
        Width = 80
      end
      object RO_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'RO_NOMBRE'
        MinWidth = 100
        Width = 214
      end
      object RO_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'RO_DESCRIP'
        MinWidth = 100
        Width = 341
      end
      object RO_NIVEL: TcxGridDBColumn
        Caption = 'Nivel'
        DataBinding.FieldName = 'RO_NIVEL'
        MinWidth = 80
        Width = 94
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 280
    Top = 8
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end