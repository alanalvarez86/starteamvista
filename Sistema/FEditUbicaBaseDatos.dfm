inherited EditUbicaBaseDatos: TEditUbicaBaseDatos
  Left = 467
  Caption = 'Ubicaci'#243'n de Base de Datos'
  ClientHeight = 122
  ClientWidth = 353
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblServidor: TLabel [0]
    Left = 41
    Top = 14
    Width = 42
    Height = 13
    Alignment = taRightJustify
    Caption = 'Servidor:'
  end
  object Label5: TLabel [1]
    Left = 10
    Top = 52
    Width = 73
    Height = 13
    Caption = 'Base de Datos:'
  end
  inherited PanelBotones: TPanel
    Top = 86
    Width = 353
    inherited OK: TBitBtn
      Left = 185
      Enabled = False
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 270
    end
  end
  object edServidor: TEdit
    Left = 86
    Top = 10
    Width = 227
    Height = 21
    TabOrder = 1
    OnExit = edServidorExit
  end
  object cbBasesDatos: TComboBox
    Left = 86
    Top = 48
    Width = 225
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
    OnChange = cbBasesDatosChange
  end
end
