unit FSistGlobales_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, ComCtrls, ToolWin, StdCtrls, Buttons, ImgList,
     ZBaseGlobal_DevEx,
     ZBaseConsulta,
     ZetaDBTextBox,ZetaCommonLists, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinsdxBarPainter,
  dxSkinsdxDockControlPainter, cxPC, dxDockControl, dxDockPanel, cxClasses,
  dxBar, cxGraphics;

type
  TSistGlobales_DevEx = class(TBaseConsulta)
    PanelTitulos: TPanel;
    ImageList: TImageList;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    RfcLbl: TZetaDBTextBox;
    InfonavitLbl: TZetaDBTextBox;
    RazonLbl: TZetaDBTextBox;
    dxBarManager1: TdxBarManager;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarDockControl1: TdxBarDockControl;
    dxBarManager1Bar1: TdxBar;
    Identificacion_DevEx: TdxBarLargeButton;
    MisDatos_DevEx: TdxBarLargeButton;
    lista_DevEx: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    Bloqueo_DevEx: TdxBarLargeButton;
    Bitacora_DevEx: TdxBarLargeButton;
    Seguridad_DevEx: TdxBarLargeButton;
    Reportes_DevEx: TdxBarLargeButton;
    Cafeteria_DevEx: TdxBarLargeButton;
    Asistencia_DevEx: TdxBarLargeButton;
    Nomina_DevEx: TdxBarLargeButton;
    Recursos_DevEx: TdxBarLargeButton;
    Adicionales_DevEx: TdxBarLargeButton;
    Areas_DevEx: TdxBarLargeButton;
    Supervisroes_DevEx: TdxBarLargeButton;
    dxBarLargeButton24: TdxBarLargeButton;
    Capacitacion_DevEx: TdxBarLargeButton;
    dxBarLargeButton26: TdxBarLargeButton;
    Imss_DevEx: TdxBarLargeButton;
    ToolBar: TToolBar;
    IdentificacionBtn: TToolButton;
    NivelesBtn: TToolButton;
    AdicionalesBtn: TToolButton;
    RecursosBtn: TToolButton;
    NominaBtn: TToolButton;
    AsistenciaBtn: TToolButton;
    ImssBtn: TToolButton;
    Capacitacionbtn: TToolButton;
    VariablesBtn: TToolButton;
    EmpleadosBtn: TToolButton;
    CafeteriaBtn: TToolButton;
    SupervisoresBtn: TToolButton;
    ReportesBtn: TToolButton;
    SeguridadBtn: TToolButton;
    BitacoraBtn: TToolButton;
    BloqueoAsistenciaBtn: TToolButton;
    AccesoTressBtn: TToolButton;
    ListaDispositivosBtn: TToolButton;
    MisDatos: TToolButton;
    cxImageList1: TcxImageList;
    TimbradoBtn: TdxBarLargeButton;
    Notificaciones_DevEx: TdxBarLargeButton;
    procedure FormCreate(Sender: TObject);
    procedure IdentificacionBtnClick(Sender: TObject);
    procedure NivelesBtnClick(Sender: TObject);
    procedure AdicionalesBtnClick(Sender: TObject);
    procedure RecursosBtnClick(Sender: TObject);
    procedure NominaBtnClick(Sender: TObject);
    procedure AsistenciaBtnClick(Sender: TObject);
    procedure ImssClick(Sender: TObject);
    procedure Globales_DevAExClick(Sender: TObject);
    procedure Empleados_DevExClick(Sender: TObject);
    procedure Cafeteria_DevExClick(Sender: TObject);
    procedure Supervisores_DevExClick(Sender: TObject);
    procedure Reportes_DevExClick(Sender: TObject);
    procedure Seguridad_DevExClick(Sender: TObject);
    procedure ToolBarDblClick(Sender: TObject);
    procedure Bitacora_DevExClick(Sender: TObject);
    procedure Lista_DevExClick(Sender: TObject);
    procedure MisDatosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BloqueoClick(Sender: TObject);
    procedure AccesoTressBtnClick(Sender: TObject);
    procedure dxBarDockControl1DblClick(Sender: TObject);
    procedure FormDblClick(Sender: TObject);
    procedure Capacitacion_DevExClick(Sender: TObject);
    procedure TimbradoBtnClick(Sender: TObject);
    procedure Notificaciones_DevExClick(Sender: TObject);
  private
    { Private declarations }
//**    function AbrirGlobal( GlobalClass: TBaseGlobalClass ): Integer;overload;
    function AbrirGlobal( GlobalClass: TbaseGlobalClass_DevEx):Integer; //**Overload;
    procedure AplicaGlobalDiccion;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
  end;

var
  SistGlobales_DevEx: TSistGlobales_DevEx;

implementation

uses DGlobal,
     DCliente,
     DSistema,
     DDiccionario,
     ZGlobalTress,
     ZetaCommonClasses,
     FAutoClasses,
     FGlobalAsistencia_DevEx,
     //FGlobalAdicionales,
     FGlobalCapacitacion_DevEx,
     FGlobalImss_DevEx,
     FGlobalIdentificacion_DevEx,
     FGlobalNiveles_DevEx,
     FGlobalNomina_DevEx,
     FGlobalRecursos_DevEx,
     FGlobalVariables_DevEx,
     FGlobalEmpleados_DevEx,
     FGlobalCafeteria_DevEx,
     FGlobalSupervisores_DevEx,
     FGlobalSeguridad_DevEx,
     FGlobalReportesViaEmail_DevEx,
     FGlobalBitacora_DevEx,
     FGlobalBloqueo_DevEx,
     FGlobalEnrolamientoUsuarios_DevEx,
     FCamposCFG_DevEx,
     FEmpAlta_DevEx,
     FEditEmpAdicionales_DevEx,
     FTressShell,
     ZetaDialogo,
     ZAccesosTress,
     ZAccesosMgr,
     FCamposMgr,
     FGlobalDispositivos_DevEx,
     FGlobalMisDatos_DevEx,
     FGlobalTimbrado,
     FGlobalNotificacionAdvertecia_DevEx;
                      
{$R *.DFM}

{ ************* TSistGlobales *************** }

procedure TSistGlobales_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     EsConsulta := False;
     HelpContext := H60651_Globales_empresa;
{$ifdef DOS_CAPAS}
     MisDatos.Visible := False;
{$endif}
end;

procedure TSistGlobales_DevEx.Connect;
begin
     with Global do
     begin
          RFCLbl.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_RFC_EMPRESA );
          InfonavitLbl.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_INFONAVIT_EMPRESA );
          RazonLbl.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_RAZON_EMPRESA );
     end;
end;

procedure TSistGlobales_DevEx.Refresh;
begin
end;

procedure TSistGlobales_DevEx.AplicaGlobalDiccion;
begin
     TressShell.CreaArbol( False );
end;

//**
{
function TSistGlobales_DevEx.AbrirGlobal( GlobalClass: TBaseGlobalClass ): Integer;
var
   Forma: TBaseGlobal;
begin
     Forma := GlobalClass.Create( Application );
     with Forma do
     begin
          try
             if ZAccesosMgr.Revisa( IndexDerechos ) then
             begin
                  ShowModal;
                  Result := LastAction;
             end
             else
             begin
                  ZetaDialogo.ZInformation( self.Caption, 'No tiene derechos para consultar estos globales', 0 );
                  Result := K_EDICION_CANCELAR;
             end;
          finally
                 Free;
          end;
     end;
end;
}
 function TSistGlobales_DevEx.AbrirGlobal( GlobalClass: TBaseGlobalClass_DevEx ): Integer;
var
   Forma: TBaseGlobal_DevEx;
begin
     Forma := GlobalClass.Create( Application );
     with Forma do
     begin
          try
             if ZAccesosMgr.Revisa( IndexDerechos ) then
             begin
                  ShowModal;
                  Result := LastAction;
             end
             else
             begin
                  ZetaDialogo.ZInformation( self.Caption, 'No tiene derechos para consultar estos globales', 0 );
                  Result := K_EDICION_CANCELAR;
             end;
          finally
                 Free;
          end;
     end;
end;
procedure TSistGlobales_DevEx.IdentificacionBtnClick(Sender: TObject);
begin
     inherited;
     if ( AbrirGlobal( TGlobalIdentificacion_DevEx ) = K_EDICION_MODIFICACION ) then
        Connect;
end;

procedure TSistGlobales_DevEx.NivelesBtnClick(Sender: TObject);
begin
     inherited;
     if ( AbrirGlobal( TGlobalNiveles_DevEx ) = K_EDICION_MODIFICACION ) then
        AplicaGlobalDiccion;
end;

procedure TSistGlobales_DevEx.AdicionalesBtnClick(Sender: TObject);
var
   oCamposAdic: TListaCampos;
   oClasificaciones: TListaClasificaciones;
   lDiferenteAdmin: Boolean;
begin
     inherited;
{
     if ( AbrirGlobal( TGlobalAdicionales ) = K_EDICION_MODIFICACION ) then
}
     lDiferenteAdmin:= ( dmCliente.GetGrupoActivo <> D_GRUPO_SIN_RESTRICCION );

     if ZAccesosMgr.Revisa( D_CAT_CONFI_ADICIONALES )then
     begin
          with dmSistema do
          begin
               { Aqui se valida el derecho de Modificar adicionales no el de consulta sobre clasificaciones }
               if ( lDiferenteAdmin ) then
                  ConectaGrupoAdAdmin ( dmCliente.Empresa );
               oCamposAdic := TListaCampos.Create;
               oClasificaciones := TListaClasificaciones.Create;
               CargaCamposAdicionales( oCamposAdic, oClasificaciones, FALSE );
               try
                  if FCamposCFG_DevEx.ConfiguracionCampos( oCamposAdic, oClasificaciones ) then
                  begin
                       // Libera las formas de edici�n que puedan tener configurados los controles de adicionales
                       FreeAndNil( EmpAlta_DevEx );
                       FreeAndNil( EditEmpAdicionales_DevEx );
                       dmDiccionario.CambioGlobales;      // Para que refresque diccionario y LookupNames
                       AplicaGlobalDiccion;
                  end;
               finally
                      { Regresa el dataset para que valide derechos }
                      FreeAndNil( oClasificaciones );
                      FreeAndNil( oCamposAdic );
                      if ( lDiferenteAdmin ) then
                         cdsGruposAdic.Refrescar;
               end;
          end;
     end
     else
     begin
          ZetaDialogo.ZInformation( self.Caption, 'No tiene derechos para consultar estos globales', 0 );
     end;
end;
procedure TSistGlobales_DevEx.RecursosBtnClick(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okRecursos ) then
     begin
          AbrirGlobal( TGlobalRecursos_DevEx );
     end;
end;

procedure TSistGlobales_DevEx.NominaBtnClick(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okNomina ) then
     begin
          AbrirGlobal( TGlobalNomina_DevEx );
     end;
end;

procedure TSistGlobales_DevEx.Notificaciones_DevExClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalNotificacionAdvertencia_DevEx );
end;

procedure TSistGlobales_DevEx.AsistenciaBtnClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalAsistencia_DevEx );
end;

procedure TSistGlobales_DevEx.ImssClick(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okIMSS ) then
     begin
          AbrirGlobal( TGlobalImss_DevEx );
     end;
end;

procedure TSistGlobales_DevEx.Globales_DevAExClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalVariables_DevEx );
end;

procedure TSistGlobales_DevEx.Empleados_DevExClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalEmpleados_DevEx );
end;

procedure TSistGlobales_DevEx.Cafeteria_DevExClick(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okCafeteria ) then
     begin
          AbrirGlobal( TGlobalCafeteria_DevEx );
     end;
end;

procedure TSistGlobales_DevEx.Supervisores_DevExClick(Sender: TObject);
begin
     inherited;
     if ( Autorizacion.Plataforma = TPlataforma( 0 ) )then
     begin
          AbrirGlobal( TGlobalSupervisores_DevEx );
     end
     else
     begin
          if ( dmCliente.ModuloAutorizado( okSupervisores ) )then
          begin
               AbrirGlobal( TGlobalSupervisores_DevEx );
          end;
     end;
end;

procedure TSistGlobales_DevEx.Reportes_DevExClick(Sender: TObject);
begin
     inherited;
     //pendiente
     if dmCliente.ModuloAutorizado( okReportesMail ) or dmCliente.ModuloAutorizado( okKiosko ) then
     begin
          AbrirGlobal( TGlobalReportesViaEmail_DevEx );
     end;
end;

procedure TSistGlobales_DevEx.Seguridad_DevExClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalSeguridad_DevEx );
end;

procedure TSistGlobales_DevEx.Bitacora_DevExClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalBitacora_DevEx );
end;

procedure TSistGlobales_DevEx.TimbradoBtnClick(Sender: TObject);
begin
  inherited;
     AbrirGlobal( TGlobalTimbrado );
end;

procedure TSistGlobales_DevEx.ToolBarDblClick(Sender: TObject);
begin
     inherited;
     IdentificacionBtnClick( Sender );
end;

procedure TSistGlobales_DevEx.Lista_DevExClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalDispositivos_DevEx );
end;

procedure TSistGlobales_DevEx.MisDatosClick(Sender: TObject);
begin
  	inherited;
     AbrirGlobal( TGlobalMisDatos_DevEx );
end;

procedure TSistGlobales_DevEx.FormShow(Sender: TObject);
begin
  inherited;
    dxBarDockControl1.left := 15 ;
    dxBarDockControl1.top  := PanelTitulos.Height + 15;
end;

procedure TSistGlobales_DevEx.BloqueoClick(Sender: TObject);
begin
   inherited;
     if ( AbrirGlobal( TGlobalBloqueo_devEx ) = K_EDICION_MODIFICACION ) then
        TressShell.SetBloqueo;;
end;

procedure TSistGlobales_DevEx.AccesoTressBtnClick(Sender: TObject);
begin
  inherited;
     AbrirGlobal( TGlobalEnrolamientoUsuarios_DevEx );
end;

procedure TSistGlobales_DevEx.dxBarDockControl1DblClick(Sender: TObject);
begin
  inherited;
   IdentificacionBtnClick( Sender );
end;

procedure TSistGlobales_DevEx.FormDblClick(Sender: TObject);
begin
  inherited;
 IdentificacionBtnClick( Sender );
end;

procedure TSistGlobales_DevEx.Capacitacion_DevExClick(Sender: TObject);
begin
  inherited;
   inherited;
     if dmCliente.ModuloAutorizado( okCursos ) then
     begin
          AbrirGlobal( TGlobalCapacitacion_DevEx );
     end;
end;

end.

