unit FSistGrupos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, Db, ExtCtrls,
     ZBaseConsulta,
     ZetaDBGrid, StdCtrls, Buttons;

type
  TSistGrupos = class(TBaseConsulta)
    ZetaDBGrid: TZetaDBGrid;
    Panel1: TPanel;
    Grupos: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure GruposClick(Sender: TObject);
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    //function ChecaGrupoSinRestriccion( const sAccion: string; var sMensaje: string ): boolean;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }

  end;

var
  SistGrupos: TSistGrupos;

implementation

{$R *.DFM}


uses DCliente,
     DSistema,
     ZetaCommonClasses,
     ZBaseDlgModal,
     ZetaDialogo,
     FSistArbolGrupos;

const
     K_MENSAJE_GRUPOS = 'ˇ Solo Usuarios del Grupo Administradores ' + CR_LF +
                        'Pueden %s Grupos de Usuarios !';
     K_MENSAJE_GRUPOS_RDD = 'No se puede %s Grupo de Usuarios desde esta Aplicación';
     K_MODIFICAR = 'Modificar';
     
{ ********** TSistGrupos ********* }

procedure TSistGrupos.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef RDDAPP}
     Grupos.Visible := FALSE;
     {$else}
     {$endif}
     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_CONS_GPO_USR;
     {$else}
     HelpContext := H80812_Grupos_usuarios;
     {$endif}
     {$ifdef WORKFLOWCFG}
     HelpContext := H_WORKFLOWCFG_SISTEMA_GPOUSUARIOS;
     {$endif}

end;

procedure TSistGrupos.Connect;
begin
     with dmSistema do
     begin
          cdsGrupos.Conectar;
          DataSource.DataSet := cdsGrupos;
          Grupos.Enabled := Not ( cdsGrupos.IsEmpty );
     end;
end;

procedure TSistGrupos.Refresh;
begin
     dmSistema.cdsGrupos.Refrescar;
end;

procedure TSistGrupos.Agregar;
begin
     dmSistema.cdsGrupos.Agregar;
end;

procedure TSistGrupos.Borrar;
begin
     dmSistema.cdsGrupos.Borrar
end;

procedure TSistGrupos.Modificar;
begin
     dmSistema.cdsGrupos.Modificar;
end;

procedure TSistGrupos.GruposClick(Sender: TObject);
var
   sOldIndex: string;
   Pos: TBookMark;
   sMensaje : string;
begin
     if PuedeModificar( sMensaje ) then
     begin
          with dmSistema.cdsGrupos do
          begin
               DisableControls;
               sOldIndex := IndexFieldNames;
               try
                  Pos := GetBookMark;
                  try
                     ZBaseDlgModal.ShowDlgModal( SistArbolGrupos, TSistArbolGrupos )
                  finally
                         if ( Pos <> nil ) then
                         begin
                              GotoBookMark( Pos );
                              FreeBookMark( Pos );
                         end;
                  end;
               finally
                      IndexFieldNames:= sOldIndex;
                      EnableControls;
               end;
          end;//with dmSistema.cdsGrupos do
     end
     else
     begin
          zInformation( 'Mapa de Grupos', 'No tiene permiso para consultar o modificar el mapa de grupos.', 0 );
     end;

end;

procedure TSistGrupos.ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     inherited;
     with ZetaDBGrid do
     begin
          if ( gdSelected in State )  then
          begin
               with Canvas do
               begin
                    Font.Color := clWhite;
                    Brush.Color := clNavy;
               end;
          end
          else
          begin
               with Canvas do
               begin
                    Brush.Color := clWhite;
                    if ( DataSource.Dataset.FieldByName( 'GR_CODIGO' ).AsInteger = dmCliente.GetGrupoActivo ) then
                       Font.Color := clRed
                    else
                        Font.Color := ZetaDBGrid.Font.Color;
               end;
          end;
          DefaultDrawDataCell( Rect, Column.Field, State );
     end;
end;

{function TSistGrupos.ChecaGrupoSinRestriccion( const sAccion: string; var sMensaje: string ): boolean;
begin
     {$ifdef RDDAPP}
{     Result := sAccion = K_MODIFICAR;
     sMensaje := Format( K_MENSAJE_GRUPOS_RDD, [ sAccion ] );
     {$else}
{     Result := ( dmCliente.GetGrupoActivo = D_GRUPO_SIN_RESTRICCION );
     if not Result then
        sMensaje := format( K_MENSAJE_GRUPOS, [ sAccion ] );
     {$endif}
{end; //}

procedure TSistGrupos.FormShow(Sender: TObject);
begin
     inherited;
     Grupos.Enabled := CheckDerechos( K_DERECHO_CAMBIO );
end;

end.

