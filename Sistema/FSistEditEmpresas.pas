unit FSistEditEmpresas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FSistBaseEditEmpresas, Db, StdCtrls, ZetaKeyLookup, DBCtrls, ZetaEdit,
  Mask, ExtCtrls, Buttons, ZetaSmartLists, ZetaKeyCombo, ZetaCommonLists;

type
  TSistEditEmpresas = class(TSistBaseEditEmpresas)
    Label3: TLabel;
    CM_USACAFE: TDBCheckBox;
    DigitoGafetesLBL: TLabel;
    CM_DIGITO: TDBEdit;
    lbReportesKiosco: TLabel;
    CM_KCLASIFI: TZetaKeyCombo;
    gbConfidencialidad: TGroupBox;
    btSeleccionarConfiden: TSpeedButton;
    listaConfidencialidad: TListBox;
    rbConfidenNinguna: TRadioButton;
    rbConfidenAlgunas: TRadioButton;
    CM_USACASE: TDBCheckBox;
    DB_CODIGO: TZetaDBKeyLookup;
    procedure FormCreate(Sender: TObject);
    procedure CM_DIGITOKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure CM_KCLASIFIChange(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormDestroy(Sender: TObject);
    procedure btSeleccionarConfidenClick(Sender: TObject);
    procedure rbConfidenNingunaClick(Sender: TObject);
    procedure rbConfidenAlgunasClick(Sender: TObject);
  private
    { Private declarations }
    lValoresConfidencialidad : TStringList;

    procedure ActualizaComboClasifi( eClasifi: eClasifiReporte );

    procedure FillListaConfidencialidad;

    procedure SetListConfidencialidad( sValores : string );
    procedure GetConfidencialidad;

  public
    { Public declarations }
    procedure Connect;override;
    procedure EscribirCambios;override; 
    procedure Agregar;override;
  end;

var
  SistEditEmpresas: TSistEditEmpresas;

implementation

uses DSistema,
     DDiccionario,
     ZetaCommonTools,
     ZReportConst,
     FSeleccionarConfidencialidad,
     ZAccesosTress;

const
     {Funciona de forma similar a crFavoritos}
     crSinRestriccion = MaxInt;
     K_SIN_RESTRICCION = -1;


{$R *.DFM}

procedure TSistEditEmpresas.FormCreate(Sender: TObject);
begin
     inherited;
     with dmSistema do
     begin
          ///CM_ACUMULA.LookupDataset := cdsEmpresasLookup;
          DB_CODIGO.LookupDataset := cdsSistBaseDatosLookUp;
     end;
     {$IFDEF DOS_CAPAS}
     //Si esta en profesional no estar� disponible la opci�n de restrigir reportes en Kiosco
     lbReportesKiosco.Visible:= False;
     CM_KCLASIFI.Visible:= False;
     {$ENDIF}

     lValoresConfidencialidad := TStringList.Create;    
end;

procedure TSistEditEmpresas.Connect;
begin
     with dmSistema do
     begin
          cdsNivel0.Conectar;
          cdsEmpresasLookup.Conectar;
          cdsSistBaseDatosLookup.Conectar;
     end;
     inherited;
end;

procedure TSistEditEmpresas.CM_DIGITOKeyPress(Sender: TObject; var Key: Char);
begin
     inherited;
     if ZetaCommonTools.EsMinuscula( Key ) then     // Solo acepta may�sculas
        Key := ZetaCommonTools.aMayuscula( Key );
     if ( Key >= #32 ) and
        ( ( not ( ZetaCommonTools.EsDigito( Key ) ) ) and ( not ( ZetaCommonTools.EsMayuscula( Key ) ) ) ) then
        Key := #0;       // Si no es caracter de control y no es D�gito o Letra May�scula se ignora
end;

procedure TSistEditEmpresas.FormShow(Sender: TObject);
const   
     K_MEN_SIN_RESTRICCION = 'Sin Restricci�n';
begin
     inherited;
     if (dmSistema.cdsEmpresas.RecordCount > 0) then
     begin
        if ( CM_KCLASIFI.Visible ) then
        begin
             dmSistema.GetListaClasifiEmpresa( CM_KCLASIFI.Items );
             try
                ActualizaComboClasifi( eClasifiReporte( dmSistema.cdsEmpresas.FieldByName( 'CM_KCLASIFI' ).AsInteger ) );
             except
             end;
        end;
     end
     else
     begin
          CM_KCLASIFI.Items.AddObject( K_MEN_SIN_RESTRICCION , TObject( crSinRestriccion ));
          CM_KCLASIFI.ItemIndex := 1;
          CM_KCLASIFI.Enabled := FALSE;
     end;
     FillListaConfidencialidad;
     
     if dmSistema.cdsEmpresas.State = dsInsert then
     begin
          rbConfidenNinguna.Checked := TRUE;
          listaConfidencialidad.Items.Clear;
     end;
end;

procedure TSistEditEmpresas.CM_KCLASIFIChange(Sender: TObject);
begin
     inherited;
     with dmSistema.cdsEmpresas do
     begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;
     end;
end;

procedure TSistEditEmpresas.EscribirCambios;
var
   iClasifi: Integer;
begin
      with CM_KCLASIFI do
      begin
           { Cuando es sin restricci�n, se graba como -1 }
           if ( Visible ) then
           begin
                if Items.Count > 1 then
                   iClasifi:= Integer(Items.Objects[ItemIndex])
                else
                    iClasifi := crSinRestriccion;
                if iClasifi = crSinRestriccion then
                  dmSistema.cdsEmpresas.FieldByName('CM_KCLASIFI').AsInteger:= K_SIN_RESTRICCION
                else
                    dmSistema.cdsEmpresas.FieldByName('CM_KCLASIFI').AsInteger := iClasifi;
           end;
      end;
      inherited EscribirCambios;
end;


procedure TSistEditEmpresas.ActualizaComboClasifi( eClasifi: eClasifiReporte );
begin
     {Para la base de datos el valor cuando es sin restricci�n es negativo, pero para Tress es el MaxInt}
     if ( Ord( eClasifi ) = K_SIN_RESTRICCION ) then
        eClasifi:= eClasifiReporte ( crSinRestriccion );
     CM_KCLASIFI.ItemIndex := dmDiccionario.GetPosClasifi( CM_KCLASIFI.Items, eClasifi);
end;


procedure TSistEditEmpresas.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( CM_KCLASIFI.Visible ) then
     begin
          with DataSource.DataSet do
          begin
               if ( State in [dsBrowse] ) then
               ActualizaComboClasifi( eClasifiReporte( FieldByName( 'CM_KCLASIFI' ).AsInteger ) );
          end;
     end;

     if ( Field = nil ) then
     begin
          with DataSource.DataSet do
          begin
               if ( State in [dsBrowse] ) then
                  GetConfidencialidad;
          end;

     end;
end;

procedure TSistEditEmpresas.SetListConfidencialidad( sValores : string );
var
   Lista : TStringList;
   i, j : integer;
begin
    Lista := TStringList.Create;

    sValores := Trim( sValores );
    if StrLleno( sValores ) then
        Lista.CommaText  := sValores;

    listaConfidencialidad.Items.Clear;

    rbConfidenNinguna.OnClick := nil;
    rbConfidenAlgunas.OnClick := nil;
    rbConfidenNinguna.Checked := False;
    rbConfidenAlgunas.Checked := False;
    
    if lValoresConfidencialidad.Count = 0 then
       FillListaConfidencialidad;

    for i:= 0 to lValoresConfidencialidad.Count - 1 do
    begin
        if ( Lista.Count > 0 ) then
        begin
                j := Lista.IndexOf( Trim( lValoresConfidencialidad.Names[i] )  );

                if ( j >= 0 ) then
                   listaConfidencialidad.Items.Add( lValoresConfidencialidad[i] )

                //CM_NIVEL0.Checked[i] :=  ( j >= 0 );
        end
    end;

    rbConfidenNinguna.Checked :=   listaConfidencialidad.Items.Count = 0;
    rbConfidenAlgunas.Checked := not rbConfidenNinguna.Checked;

    if ( gbConfidencialidad.Enabled ) then
    begin
        listaConfidencialidad.Enabled := rbConfidenAlgunas.Checked;
        btSeleccionarConfiden.Enabled := rbConfidenAlgunas.Checked;
    end;

    rbConfidenNinguna.OnClick := rbConfidenNingunaClick;
    rbConfidenAlgunas.OnClick := rbConfidenAlgunasClick;

    FreeAndNil( Lista );

end;

procedure TSistEditEmpresas.GetConfidencialidad;
begin
   if (  DataSource.DataSet  <> nil ) then
   with DataSource.DataSet do
   begin
        SetListConfidencialidad(  FieldByName('CM_NIVEL0').AsString );
   end;
end;


procedure TSistEditEmpresas.FormDestroy(Sender: TObject);
begin
  inherited;
   FreeAndNil( lValoresConfidencialidad ) ;
end;

procedure TSistEditEmpresas.btSeleccionarConfidenClick(Sender: TObject);
var
   selConfi : TSeleccionarConfidencialidad;
   sCM_NIVEL0Nuevo : string;
begin
  inherited;

  selConfi := TSeleccionarConfidencialidad.Create( Self );
  sCM_NIVEL0Nuevo := selConfi.GetConfidencialidad( DataSource.DataSet.FieldByName('CM_NIVEL0').AsString, lValoresConfidencialidad );

  if (  sCM_NIVEL0Nuevo <>  DataSource.DataSet.FieldByName('CM_NIVEL0').AsString ) then
  begin
     with DataSource.DataSet do
     begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

           FieldByName('CM_NIVEL0').AsString  := sCM_NIVEL0Nuevo;
           GetConfidencialidad;
     end;
  end;

  FreeAndNil ( selConfi ) ;

end;

procedure TSistEditEmpresas.FillListaConfidencialidad;
begin
     with dmSistema.cdsNivel0 do
     begin
          First;
          lValoresConfidencialidad.Clear;
          lValoresConfidencialidad.NameValueSeparator := '=' ;
          While( not Eof )do
          begin
               lValoresConfidencialidad.Add(FieldByName('TB_CODIGO').AsString +'='+ FieldByName('TB_ELEMENT').AsString);
               Next;
          end;
     end;
end;

procedure TSistEditEmpresas.rbConfidenNingunaClick(Sender: TObject);
begin
  inherited;

   with DataSource.DataSet do
   begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

        FieldByName('CM_NIVEL0').AsString  :=  '';
   end;
   GetConfidencialidad;
end;

procedure TSistEditEmpresas.rbConfidenAlgunasClick(Sender: TObject);
begin
  inherited;
     if ( rbConfidenAlgunas.Checked ) then
     begin
        btSeleccionarConfidenClick( Sender );
        GetConfidencialidad;
     end;

end;

procedure TSistEditEmpresas.Agregar;
begin
     inherited;
     rbConfidenNinguna.Checked := TRUE;
     listaConfidencialidad.Items.Clear;
end;

end.
