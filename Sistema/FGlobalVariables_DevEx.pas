unit FGlobalVariables_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Mask,
     ZetaNumero, ZBaseGlobal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, ImgList, cxButtons;

type
  TGlobalVariables_DevEx = class(TBaseGlobal_DevEx)
    Numeros: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Textos: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Texto1: TEdit;
    Texto2: TEdit;
    Texto3: TEdit;
    Numero1: TZetaNumero;
    Numero2: TZetaNumero;
    Numero3: TZetaNumero;
    Numero4: TZetaNumero;
    Numero5: TZetaNumero;
    Numero6: TZetaNumero;
    Numero7: TZetaNumero;
    Numero8: TZetaNumero;
    Numero9: TZetaNumero;
    Numero10: TZetaNumero;
    lblNumero1: TEdit;
    lblNumero2: TEdit;
    lblNumero4: TEdit;
    lblNumero3: TEdit;
    lblNumero5: TEdit;
    lblNumero10: TEdit;
    lblNumero9: TEdit;
    lblNumero8: TEdit;
    lblNumero6: TEdit;
    lblNumero7: TEdit;
    lblTexto1: TEdit;
    lblTexto2: TEdit;
    lblTexto3: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    procedure AlmacenarDescripcionesGlobales;
    procedure AsignarEtiquetasGlobales;
  public
    { Public declarations }
  end;

var
  GlobalVariables_DevEx: TGlobalVariables_DevEx;

implementation

uses ZGlobalTress,
     ZAccesosTress,
     DGlobal,
     Variants,
     ZetaCommonClasses;

{$R *.DFM}



procedure TGlobalVariables_DevEx.AsignarEtiquetasGlobales;
begin
     Global.ConectarDescripcionesGV;
     lblNumero1.Text   := Global.ResultArregloDescripcionesGV[0,1];
     lblNumero2.Text   := Global.ResultArregloDescripcionesGV[1,1];
     lblNumero3.Text   := Global.ResultArregloDescripcionesGV[2,1];
     lblNumero4.Text   := Global.ResultArregloDescripcionesGV[3,1];
     lblNumero5.Text   := Global.ResultArregloDescripcionesGV[4,1];
     lblNumero6.Text   := Global.ResultArregloDescripcionesGV[5,1];
     lblNumero7.Text   := Global.ResultArregloDescripcionesGV[6,1];
     lblNumero8.Text   := Global.ResultArregloDescripcionesGV[7,1];
     lblNumero9.Text   := Global.ResultArregloDescripcionesGV[8,1];
     lblNumero10.Text  := Global.ResultArregloDescripcionesGV[9,1];
     lblTexto1.Text    := Global.ResultArregloDescripcionesGV[10,1];
     lblTexto2.Text    := Global.ResultArregloDescripcionesGV[11,1];
     lblTexto3.Text    := Global.ResultArregloDescripcionesGV[12,1];
end;

procedure TGlobalVariables_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_CAT_CONFI_VAR_GLOBALES;
     Numero1.Tag   := K_GLOBAL_NUM_GLOBAL1;
     Numero2.Tag   := K_GLOBAL_NUM_GLOBAL2;
     Numero3.Tag   := K_GLOBAL_NUM_GLOBAL3;
     Numero4.Tag   := K_GLOBAL_NUM_GLOBAL4;
     Numero5.Tag   := K_GLOBAL_NUM_GLOBAL5;
     Numero6.Tag   := K_GLOBAL_NUM_GLOBAL6;
     Numero7.Tag   := K_GLOBAL_NUM_GLOBAL7;
     Numero8.Tag   := K_GLOBAL_NUMERO_GLOBAL8;
     Numero9.Tag   := K_GLOBAL_NUM_GLOBAL9;
     Numero10.Tag  := K_GLOBAL_NUM_GLOBAL10;
     Texto1.Tag    := K_GLOBAL_TEXT_GLOBAL1;
     Texto2.Tag    := K_GLOBAL_TEXT_GLOBAL2;
     Texto3.Tag    := K_GLOBAL_TEXT_GLOBAL3;
     HelpContext   := H65109_Variables_globales;

end;

procedure TGlobalVariables_DevEx.AlmacenarDescripcionesGlobales;
var
    aDescripciones : Variant;
begin
  inherited;
            aDescripciones := VarArrayCreate([0, K_TOT_DESCRIPCIONES_GV, 0, 2],varVariant);
            aDescripciones[0,0]:= Global.ResultArregloDescripcionesGV[0,0];
            aDescripciones[0,1]:= lblNumero1.Text;

            aDescripciones[1,0] := Global.ResultArregloDescripcionesGV[1,0];
            aDescripciones[1,1] := lblNumero2.Text;

            aDescripciones[2,0] := Global.ResultArregloDescripcionesGV[2,0];
            aDescripciones[2,1] := lblNumero3.Text;

            aDescripciones[3,0] := Global.ResultArregloDescripcionesGV[3,0];
            aDescripciones[3,1] := lblNumero4.Text;

            aDescripciones[4,0] := Global.ResultArregloDescripcionesGV[4,0];
            aDescripciones[4,1] := lblNumero5.Text;

            aDescripciones[5,0] := Global.ResultArregloDescripcionesGV[5,0];
            aDescripciones[5,1] := lblNumero6.Text;

            aDescripciones[6,0] := Global.ResultArregloDescripcionesGV[6,0];
            aDescripciones[6,1] := lblNumero7.Text;

            aDescripciones[7,0] := Global.ResultArregloDescripcionesGV[7,0];
            aDescripciones[7,1] := lblNumero8.Text;

            aDescripciones[8,0] := Global.ResultArregloDescripcionesGV[8,0];
            aDescripciones[8,1] := lblNumero9.Text;

            aDescripciones[9,0] := Global.ResultArregloDescripcionesGV[9,0];
            aDescripciones[9,1] :=  lblNumero10.Text;

            aDescripciones[10,0] := Global.ResultArregloDescripcionesGV[10,0];
            aDescripciones[10,1] := lblTexto1.Text;

            aDescripciones[11,0] := Global.ResultArregloDescripcionesGV[11,0];
            aDescripciones[11,1] := lblTexto2.Text;

            aDescripciones[12,0] := Global.ResultArregloDescripcionesGV[12,0];
            aDescripciones[12,1] := lblTexto3.Text;

            Global.ProcesaDescripcionesGV(aDescripciones)
end;


procedure TGlobalVariables_DevEx.FormShow(Sender: TObject);
begin
  inherited;
            try
               AsignarEtiquetasGlobales;
            Except
            end;
end;


procedure TGlobalVariables_DevEx.OK_DevExClick(Sender: TObject);
begin
  inherited;
            try
               AlmacenarDescripcionesGlobales;
            Except
            end;
end;

end.
