inherited EditTerminales_DevEx: TEditTerminales_DevEx
  Left = 621
  Top = 192
  Caption = 'Terminales'
  ClientHeight = 438
  ClientWidth = 576
  OnDestroy = FormDestroy
  ExplicitWidth = 582
  ExplicitHeight = 467
  PixelsPerInch = 96
  TextHeight = 13
  object Label32: TLabel [0]
    Left = 166
    Top = 191
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Versi'#243'n:'
  end
  object ZetaDBTextBox1: TZetaDBTextBox [1]
    Left = 207
    Top = 189
    Width = 233
    Height = 17
    AutoSize = False
    Caption = 'ZetaDBTextBox1'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'TE_VERSION'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  inherited PanelBotones: TPanel
    Top = 402
    Width = 576
    TabOrder = 2
    ExplicitTop = 402
    ExplicitWidth = 576
    inherited OK_DevEx: TcxButton
      Left = 408
      ExplicitLeft = 408
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 487
      ExplicitLeft = 487
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 576
    TabOrder = 0
    ExplicitTop = 26
    ExplicitWidth = 576
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 317
      end
    end
    inherited ValorActivo2: TPanel
      Width = 250
      ExplicitWidth = 250
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 244
        ExplicitLeft = 164
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Width = 72
    Buttons.OnButtonClick = DevEx_cxDBNavigatorEdicionButtonsButtonClick
    ExplicitWidth = 72
  end
  object PageControl_DevEx: TcxPageControl [5]
    Left = 0
    Top = 45
    Width = 576
    Height = 357
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = TabSheet2_DevEx
    Properties.CustomButtons.Buttons = <>
    Properties.MultiLine = True
    ClientRectBottom = 356
    ClientRectLeft = 1
    ClientRectRight = 575
    ClientRectTop = 39
    object tbDatosGenerales_DevEx: TcxTabSheet
      Caption = 'Datos Generales'
      object TE_MAC: TZetaTextBox
        Left = 199
        Top = 161
        Width = 233
        Height = 17
        AutoSize = False
        Caption = 'TE_MAC'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object TE_BEAT: TZetaDBTextBox
        Left = 199
        Top = 179
        Width = 233
        Height = 17
        AutoSize = False
        Caption = 'TE_BEAT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'TE_BEAT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label8: TLabel
        Left = 168
        Top = 99
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Zona:'
      end
      object Label7: TLabel
        Left = 136
        Top = 181
        Width = 60
        Height = 13
        Alignment = taRightJustify
        Caption = #218'ltimo latido:'
      end
      object Label6: TLabel
        Left = 144
        Top = 77
        Width = 52
        Height = 13
        Alignment = taRightJustify
        Caption = 'Aplicaci'#243'n:'
      end
      object Label5: TLabel
        Left = 122
        Top = 163
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'Direcci'#243'n MAC:'
      end
      object Label4: TLabel
        Left = 166
        Top = 143
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Texto:'
      end
      object Label3: TLabel
        Left = 156
        Top = 121
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero:'
      end
      object Label2: TLabel
        Left = 137
        Top = 55
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n:'
      end
      object Label1: TLabel
        Left = 160
        Top = 17
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo:'
      end
      object TE_CODIGO: TZetaDBTextBox
        Left = 199
        Top = 15
        Width = 49
        Height = 17
        AutoSize = False
        Caption = 'TE_CODIGO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'TE_CODIGO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label31: TLabel
        Left = 158
        Top = 199
        Width = 38
        Height = 13
        Alignment = taRightJustify
        Caption = 'Versi'#243'n:'
      end
      object TE_VERSION: TZetaDBTextBox
        Left = 199
        Top = 197
        Width = 233
        Height = 17
        AutoSize = False
        Caption = 'TE_VERSION'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'TE_VERSION'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label35: TLabel
        Left = 135
        Top = 217
        Width = 61
        Height = 13
        Alignment = taRightJustify
        Caption = 'Direcci'#243'n IP:'
      end
      object TE_IP: TZetaDBTextBox
        Left = 199
        Top = 215
        Width = 233
        Height = 17
        AutoSize = False
        Caption = 'TE_IP'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'TE_IP'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label36: TLabel
        Left = 93
        Top = 235
        Width = 103
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cantidad de plantillas:'
      end
      object TE_HUELLAS: TZetaDBTextBox
        Left = 199
        Top = 233
        Width = 233
        Height = 17
        AutoSize = False
        Caption = 'TE_HUELLAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'TE_HUELLAS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label38: TLabel
        Left = 172
        Top = 35
        Width = 24
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo:'
      end
      object ZetaDBTextBox2: TZetaDBTextBox
        Left = 199
        Top = 33
        Width = 177
        Height = 17
        AutoSize = False
        Caption = 'ZetaDBTextBox2'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'TE_ID'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object TE_TEXTO: TDBEdit
        Left = 199
        Top = 139
        Width = 233
        Height = 21
        DataField = 'TE_TEXTO'
        DataSource = DataSource
        TabOrder = 5
      end
      object TE_NOMBRE: TDBEdit
        Left = 199
        Top = 51
        Width = 233
        Height = 21
        DataField = 'TE_NOMBRE'
        DataSource = DataSource
        TabOrder = 1
      end
      object TE_APP: TZetaKeyCombo
        Left = 199
        Top = 73
        Width = 233
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 2
        OnChange = TE_APPChange
        Items.Strings = (
          'Asistencia'
          'Cafeter'#237'a'
          'Acceso')
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object TE_ZONA: TZetaKeyCombo
        Left = 199
        Top = 95
        Width = 233
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 3
        OnChange = TE_ZONAChange
        Items.Strings = (
          '(GMT-05:00) Este (US & Canada)'
          '(GMT-06:00) Central (US & Canada)'
          '(GMT-06:00) Guadalajara, D. F., Monterrey'
          '(GMT-07:00) Arizona'
          '(GMT-07:00) Chihuahua, La Paz, Mazatl'#225'n'
          '(GMT-07:00) Monta'#241'a (US & Canada)'
          '(GMT-08:00) Pac'#237'fico (US & Canada)'
          '(GMT-08:00) Baja California')
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object TE_ACTIVO: TDBCheckBox
        Left = 382
        Top = 33
        Width = 50
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Activo'
        DataField = 'TE_ACTIVO'
        DataSource = DataSource
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object TE_NUMERO: TZetaDBNumero
        Left = 199
        Top = 117
        Width = 113
        Height = 21
        Mascara = mnHoras
        TabOrder = 4
        Text = '0.00'
        DataField = 'TE_NUMERO'
        DataSource = DataSource
      end
      object btnReiniciarTerminal_DevEx: TcxButton
        Left = 199
        Top = 252
        Width = 202
        Height = 26
        Hint = 'Sincronizar plantillas de la terminal'
        Caption = 'Sincronizar plantillas de la terminal'
        Enabled = False
        OptionsImage.Glyph.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          2000000000000009000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDCB38DFFEBD3
          BDFFF3E4D7FFFBF6F1FFFDFAF7FFF6ECE3FFF0DFCFFFE2BF9FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFDDB58FFFD8AA7FFFE5C7ABFFFDFAF7FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3E4D7FFDEB6
          91FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFF5EADFFFF7EEE5FFFFFFFFFFFFFFFFFFFAF4
          EFFFEAD2BBFFE4C3A5FFE2C1A1FFE9CEB5FFF5EADFFFFFFFFFFFFFFFFFFFF9F3
          EDFFDCB28BFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFF9F3EDFFFFFFFFFFFFFFFFFFF5E8DDFFDAAE
          85FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE3C2A3FFFDFBF9FFFFFF
          FFFFF6ECE3FFD9AB81FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFFDFAF7FFFFFFFFFFFFFFFFFFFEFEFDFFE2C1
          A1FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE2BF9FFFFFFF
          FFFFFFFFFFFFE4C3A5FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFFFFFFFFFFDFAF7FFEEDAC7FFDCB38DFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFF5EA
          DFFFFFFFFFFFF2E2D3FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFDCB28BFFEAD1B9FFDAAF87FFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE4C3
          A5FFF5EADFFFF4E7DBFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFE1BD9BFFE2BF9FFFDCB38DFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFF6EBE1FFFFFFFFFFF2E2D3FFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AD
          83FFE7CAAFFFDEB793FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFE7CBB1FFFFFFFFFFFEFCFBFFDBB189FFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDFB995FFEEDBC9FFFDFA
          F7FFFFFFFFFFDDB58FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFDAAF87FFFEFCFBFFFFFFFFFFF4E7DBFFD9AB81FFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDFB995FFFBF6F1FFFFFFFFFFFFFF
          FFFFFFFFFFFFDAAF87FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFE7CBB1FFFFFFFFFFFFFFFFFFF3E4D7FFDDB5
          8FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDDB58FFFF5EADFFFFFFFFFFFFFFF
          FFFFFEFEFDFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFEDD8C5FFFFFFFFFFFFFFFFFFFEFC
          FBFFF3E4D7FFEDD7C3FFEDD8C5FFF3E6D9FFFEFCFBFFFFFFFFFFFFFFFFFFF5EA
          DFFFF9F2EBFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE6C9ADFFFBF7F3FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCF8F5FFE5C6A9FFD8AA
          7FFFDCB28BFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDAAE85FFE8CD
          B3FFEFDCCBFFF7EEE5FFF6EBE1FFEEDAC7FFE6C9ADFFDBB189FFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        OnClick = btnReiniciarTerminal_DevExClick
      end
    end
    object tbConfiguracion_DevEx: TcxTabSheet
      Caption = 'Informaci'#243'n del reloj'
      object Label29: TLabel
        Left = 151
        Top = 98
        Width = 73
        Height = 13
        Caption = 'EnableKeyPad:'
      end
      object Label28: TLabel
        Left = 166
        Top = 82
        Width = 58
        Height = 13
        Caption = 'ShowPhoto:'
      end
      object Label11: TLabel
        Left = 143
        Top = 61
        Width = 81
        Height = 13
        Alignment = taRightJustify
        Caption = 'DateTimeFormat:'
      end
      object Label16: TLabel
        Left = 186
        Top = 19
        Width = 38
        Height = 13
        Alignment = taRightJustify
        Caption = 'Volume:'
      end
      object Label15: TLabel
        Left = 134
        Top = 40
        Width = 90
        Height = 13
        Alignment = taRightJustify
        Caption = 'MenuAccessCode:'
      end
      object Label10: TLabel
        Left = 149
        Top = 114
        Width = 75
        Height = 13
        Caption = 'DetalleBitacora:'
      end
      object EnableKeyPad: TCheckBox
        Left = 227
        Top = 96
        Width = 25
        Height = 17
        Hint = 'Activar teclado de la terminal'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = XMLChange
      end
      object ShowPhoto: TCheckBox
        Left = 227
        Top = 80
        Width = 25
        Height = 17
        Hint = 'Presentar foto en la terminal'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = XMLChange
      end
      object DateTimeFormat: TEdit
        Left = 227
        Top = 59
        Width = 238
        Height = 21
        Hint = 'Formato de fecha (ej: DD/MM hh:mm:ss)'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnChange = XMLChange
      end
      object Volume: TZetaNumero
        Left = 227
        Top = 15
        Width = 65
        Height = 21
        Hint = 'Volumen de la terminal {00-100}'
        Mascara = mnDias
        MaxLength = 3
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Text = '0'
        OnChange = XMLChange
      end
      object MenuAccessCode: TZetaNumero
        Left = 227
        Top = 37
        Width = 65
        Height = 21
        Hint = 'Contrase'#241'a para ingresar al men'#250' de la terminal {4...6 d'#237'gitos}'
        Mascara = mnDias
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Text = '0'
        OnChange = XMLChange
      end
      object GroupBox10: TGroupBox
        Left = 270
        Top = 352
        Width = 258
        Height = 105
        Caption = 'Validaciones:'
        TabOrder = 5
      end
      object GroupBox5: TGroupBox
        Left = 16
        Top = 448
        Width = 257
        Height = 41
        Caption = ' Mensaje de autentificaci'#243'n: '
        TabOrder = 6
      end
      object GroupBox11: TGroupBox
        Left = 270
        Top = 456
        Width = 258
        Height = 57
        Caption = ' Versi'#243'n del software: '
        TabOrder = 7
      end
      object DetalleBitacora: TCheckBox
        Left = 227
        Top = 112
        Width = 25
        Height = 17
        Hint = 'Activar teclado de la terminal'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
        OnClick = XMLChange
      end
    end
    object TabSheet1_DevEx: TcxTabSheet
      Caption = 'Configuraci'#243'n de red'
      ImageIndex = 2
      object Label53: TLabel
        Left = 141
        Top = 102
        Width = 26
        Height = 13
        Alignment = taRightJustify
        Caption = 'DNS:'
      end
      object Label52: TLabel
        Left = 85
        Top = 80
        Width = 82
        Height = 13
        Alignment = taRightJustify
        Caption = 'DefaultGateWay:'
      end
      object Label51: TLabel
        Left = 121
        Top = 58
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = 'NetMask:'
      end
      object Label50: TLabel
        Left = 155
        Top = 36
        Width = 12
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ip:'
      end
      object Label49: TLabel
        Left = 92
        Top = 228
        Width = 75
        Height = 13
        Alignment = taRightJustify
        Caption = 'ProxyPassword:'
      end
      object Label48: TLabel
        Left = 88
        Top = 206
        Width = 79
        Height = 13
        Alignment = taRightJustify
        Caption = 'ProxyUserName:'
      end
      object Label47: TLabel
        Left = 51
        Top = 184
        Width = 116
        Height = 13
        Alignment = taRightJustify
        Caption = 'UseProxyAuthentication:'
      end
      object Label46: TLabel
        Left = 119
        Top = 164
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = 'ProxyPort:'
      end
      object Label45: TLabel
        Left = 138
        Top = 142
        Width = 29
        Height = 13
        Alignment = taRightJustify
        Caption = 'Proxy:'
      end
      object Label44: TLabel
        Left = 119
        Top = 121
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = 'UseProxy:'
      end
      object Label14: TLabel
        Left = 105
        Top = 14
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'WebService:'
      end
      object ProxyPassword: TEdit
        Left = 171
        Top = 224
        Width = 200
        Height = 21
        Hint = 'Contrase'#241'a del proxy'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 10
        OnChange = XMLChange
      end
      object ProxyUserName: TEdit
        Left = 171
        Top = 202
        Width = 200
        Height = 21
        Hint = 'Usuario del proxy'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 9
        OnChange = XMLChange
      end
      object ProxyPort: TEdit
        Left = 171
        Top = 160
        Width = 200
        Height = 21
        Hint = 'Puerto del proxy'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
        OnChange = XMLChange
      end
      object Proxy: TEdit
        Left = 171
        Top = 138
        Width = 200
        Height = 21
        Hint = 'Direcci'#243'n del proxy'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        OnChange = XMLChange
      end
      object DNS: TEdit
        Left = 171
        Top = 98
        Width = 200
        Height = 21
        Hint = 'DNS a utilizar'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnChange = XMLChange
      end
      object DefaultGateWay: TEdit
        Left = 171
        Top = 76
        Width = 200
        Height = 21
        Hint = 'Puerta de salida'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnChange = XMLChange
      end
      object NetMask: TEdit
        Left = 171
        Top = 54
        Width = 200
        Height = 21
        Hint = 'M'#225'scara de red'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnChange = XMLChange
      end
      object Ip: TEdit
        Left = 171
        Top = 32
        Width = 200
        Height = 21
        Hint = 'Ip de la terminal *Utilizar DHCP para obtener la ip din'#225'mica'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnChange = XMLChange
      end
      object WebService: TEdit
        Left = 171
        Top = 10
        Width = 350
        Height = 21
        Hint = 'URL del WS de checada'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnChange = XMLChange
      end
      object UseProxyAuthentication: TCheckBox
        Left = 171
        Top = 182
        Width = 17
        Height = 17
        Hint = 'Utilizar autentificaci'#243'n del proxy'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
        OnClick = XMLChange
      end
      object UseProxy: TCheckBox
        Left = 171
        Top = 119
        Width = 17
        Height = 17
        Hint = 'Validaci'#243'n de uso de proxy'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = XMLChange
      end
    end
    object TabSheet3_DevEx: TcxTabSheet
      Caption = 'Dispositivos, validaciones y mensajes'
      ImageIndex = 3
      object Label30: TLabel
        Left = 116
        Top = 77
        Width = 53
        Height = 13
        Alignment = taRightJustify
        Caption = 'FingerPrint:'
      end
      object Label17: TLabel
        Left = 76
        Top = 151
        Width = 93
        Height = 13
        Alignment = taRightJustify
        Caption = 'OneMinuteValidate:'
      end
      object Label18: TLabel
        Left = 147
        Top = 55
        Width = 22
        Height = 13
        Alignment = taRightJustify
        Caption = 'HID:'
      end
      object Label13: TLabel
        Left = 76
        Top = 135
        Width = 93
        Height = 13
        Alignment = taRightJustify
        Caption = 'EnableOfflineRelay:'
      end
      object Label12: TLabel
        Left = 111
        Top = 119
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'FingerVerify:'
      end
      object Label27: TLabel
        Left = 90
        Top = 259
        Width = 79
        Height = 13
        Alignment = taRightJustify
        Caption = 'OfflineValidation:'
      end
      object Label26: TLabel
        Left = 69
        Top = 237
        Width = 100
        Height = 13
        Alignment = taRightJustify
        Caption = 'AutenticateMessage:'
      end
      object Label25: TLabel
        Left = 78
        Top = 215
        Width = 91
        Height = 13
        Alignment = taRightJustify
        Caption = 'DuplicateMessage:'
      end
      object Label24: TLabel
        Left = 40
        Top = 193
        Width = 129
        Height = 13
        Alignment = taRightJustify
        Caption = 'NotValidByServerMessage:'
      end
      object Label9: TLabel
        Left = 54
        Top = 171
        Width = 115
        Height = 13
        Alignment = taRightJustify
        Caption = 'FpuNotIdentifyMessage:'
      end
      object Label23: TLabel
        Left = 110
        Top = 11
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'ProxReader:'
      end
      object Label22: TLabel
        Left = 90
        Top = 33
        Width = 79
        Height = 13
        Alignment = taRightJustify
        Caption = 'BarCodeReader:'
      end
      object Label37: TLabel
        Left = 126
        Top = 99
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'MIFARE:'
      end
      object FingerPrint: TComboBox
        Left = 172
        Top = 73
        Width = 65
        Height = 21
        Hint = 'Lector de huella d'#237'gital'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        Text = 'FingerPrint'
        OnChange = XMLChange
        Items.Strings = (
          'ON'
          'OFF')
      end
      object OneMinuteValidate: TCheckBox
        Left = 172
        Top = 149
        Width = 17
        Height = 17
        Hint = 
          'Validar de no permitir registro del mismo empleado en el mismo m' +
          'inuto'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
        OnClick = XMLChange
      end
      object OfflineValidation: TEdit
        Left = 172
        Top = 255
        Width = 350
        Height = 21
        Hint = 'Mensaje de checada fuera de l'#237'nea'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 12
        OnChange = XMLChange
      end
      object AutenticateMessage: TEdit
        Left = 172
        Top = 233
        Width = 350
        Height = 21
        Hint = 'Mensaje de autentificaci'#243'n v'#225'lida'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 11
        OnChange = XMLChange
      end
      object DuplicateMessage: TEdit
        Left = 172
        Top = 211
        Width = 350
        Height = 21
        Hint = 'Mensaje de checada duplicada'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 10
        OnChange = XMLChange
      end
      object NotValidByServerMessage: TEdit
        Left = 172
        Top = 189
        Width = 350
        Height = 21
        Hint = 'Mensaje de error de validaci'#243'n del servidor'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 9
        OnChange = XMLChange
      end
      object FpuNotIdetifyMessage: TEdit
        Left = 172
        Top = 167
        Width = 350
        Height = 21
        Hint = 'Mensaje de huella inv'#225'lida'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
        OnChange = XMLChange
      end
      object HID: TComboBox
        Left = 172
        Top = 51
        Width = 65
        Height = 21
        Hint = 'HID'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        Text = 'ComboBox1'
        OnChange = XMLChange
        Items.Strings = (
          'ON'
          'OFF')
      end
      object EnableOfflineRelay: TCheckBox
        Left = 172
        Top = 133
        Width = 17
        Height = 17
        Hint = 
          'Indica si se desea la apertura del relay en modo acceso y offlin' +
          'e'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        OnClick = XMLChange
      end
      object FingerVerify: TCheckBox
        Left = 172
        Top = 117
        Width = 17
        Height = 17
        Hint = 'Segunda validaci'#243'n con huella'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = XMLChange
      end
      object BarCodeReader: TComboBox
        Left = 172
        Top = 29
        Width = 65
        Height = 21
        Hint = 'C'#243'digo de barras'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Text = 'ComboBox1'
        OnChange = XMLChange
        Items.Strings = (
          'ON'
          'OFF')
      end
      object ProxReader: TComboBox
        Left = 172
        Top = 7
        Width = 65
        Height = 21
        Hint = 'Proximidad'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Text = 'ProxReader'
        OnChange = XMLChange
        Items.Strings = (
          'ON'
          'OFF')
      end
      object MiFare: TComboBox
        Left = 172
        Top = 95
        Width = 65
        Height = 21
        Hint = 'Lector de huella d'#237'gital'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        Text = 'MiFare'
        OnChange = XMLChange
        Items.Strings = (
          'ON'
          'OFF')
      end
    end
    object TabSheet6_DevEx: TcxTabSheet
      Caption = 'Tiempos'
      ImageIndex = 4
      object Label21: TLabel
        Left = 196
        Top = 77
        Width = 68
        Height = 13
        Alignment = taRightJustify
        Caption = 'ErrorMsgTime:'
      end
      object Label34: TLabel
        Left = 211
        Top = 99
        Width = 53
        Height = 13
        Alignment = taRightJustify
        Caption = 'RelayTime:'
      end
      object Label33: TLabel
        Left = 218
        Top = 55
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = 'MsgTime:'
      end
      object Label20: TLabel
        Left = 187
        Top = 33
        Width = 77
        Height = 13
        Alignment = taRightJustify
        Caption = 'HeartBeatTimer:'
      end
      object Label19: TLabel
        Left = 221
        Top = 11
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'TimeOut:'
      end
      object ErrorMsgTime: TZetaNumero
        Left = 268
        Top = 73
        Width = 65
        Height = 21
        Hint = 'Tiempo de despliegue de mensajes de error {01-06}'
        Mascara = mnDias
        MaxLength = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        Text = '0'
        OnChange = XMLChange
      end
      object RelayTime: TZetaNumero
        Left = 268
        Top = 95
        Width = 65
        Height = 21
        Hint = 'Duraci'#243'n de apertura del relevador {01-06}'
        Mascara = mnDias
        MaxLength = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        Text = '0'
        OnChange = XMLChange
      end
      object MsgTime: TZetaNumero
        Left = 268
        Top = 51
        Width = 65
        Height = 21
        Hint = 'Tiempo de despliegue de mensajes {01-06}'
        Mascara = mnDias
        MaxLength = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        Text = '0'
        OnChange = XMLChange
      end
      object HeartBeatTimer: TZetaNumero
        Left = 268
        Top = 29
        Width = 65
        Height = 21
        Hint = 'Frecuencia de comunicaci'#243'n al WS {01-86400}'
        Mascara = mnDias
        MaxLength = 5
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Text = '0'
        OnChange = XMLChange
      end
      object TimeOut: TZetaNumero
        Left = 268
        Top = 7
        Width = 65
        Height = 21
        Hint = 'Time out de llamadas al WS {01-3600} '
        Mascara = mnDias
        MaxLength = 4
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Text = '0'
        OnChange = XMLChange
      end
    end
    object TabSheet2_DevEx: TcxTabSheet
      Caption = 'Propiedades (XML)'
      ImageIndex = 5
      object cxDBMemo_XML: TcxDBMemo
        Left = 0
        Top = 0
        Align = alClient
        DataBinding.DataField = 'TE_XML'
        DataBinding.DataSource = DataSource
        ParentFont = False
        Properties.ReadOnly = True
        Properties.ScrollBars = ssBoth
        Properties.WordWrap = False
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Courier New'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 0
        Height = 317
        Width = 574
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 108
    Top = 368
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 24117328
    ImageInfo = <
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF98A1E2FFF6F7FDFFB7BEEBFF4959CDFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFB1B8
          E9FFF6F7FDFFACB3E8FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFFE2E5F7FFFFFFFFFFFFFFFFFFC3C8EEFF4959CDFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFF
          FFFFFFFFFFFFF6F7FDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF9BA4E3FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959
          CDFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFA6AEE6FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4959CDFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4959CDFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFC3C8EEFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6
          E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8
          EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959CDFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959CDFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFAFB6E9FFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959
          CDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4959CDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4959CDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFFA9B0E7FFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFB7BEEBFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFFE2E5F7FFFFFFFFFFFFFFFFFFAFB6E9FF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFF6F7FDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF8792DEFFDFE2F6FFA1A9E5FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF9BA4
          E3FFDFE2F6FF98A1E2FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF64D8
          A1FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF8CE2B8FFE9F9
          F1FF53D396FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF64D8A1FFF4FCF8FFFFFF
          FFFFADEACCFF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF51D395FFD6F4E6FFFFFFFFFFFFFF
          FFFFFCFEFDFF6FDAA7FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FFA5E8C8FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFD9F5E7FF51D395FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF77DDACFFFCFEFDFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF9AE5C1FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF59D59AFFE9F9F1FFFFFFFFFFFFFFFFFFC3EFDAFFBDEE
          D6FFFFFFFFFFFFFFFFFFF4FCF8FF61D79FFF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FFC3EFDAFFFFFFFFFFF7FDFAFF8CE2B8FF4ED293FF53D3
          96FFE1F7ECFFFFFFFFFFFFFFFFFFC8F1DDFF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF8AE1B7FFE6F9F0FF69D9A4FF4ED293FF4ED293FF4ED2
          93FF5ED69DFFE4F8EEFFFFFFFFFFFFFFFFFF8FE2BAFF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF51D395FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF64D8A1FFEFFBF5FFFFFFFFFFF7FDFAFF67D8A2FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF69D9A4FFEFFBF5FFFFFFFFFFDEF7EBFF51D395FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF77DDACFFF7FDFAFFFFFFFFFFB8EDD3FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF6FDAA7FFEFFBF5FFFFFFFFFFA0E6
          C4FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF64D8A1FFE1F7ECFFFFFF
          FFFF8FE2BAFF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF56D498FFBDEE
          D6FFFCFEFDFF8AE1B7FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF81DFB1FF8FE2BAFF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF5160CFFF6B78D6FF8D97DFFFACB3E8FFC8CD
          F0FFE8EAF9FFBAC0ECFF6E7BD7FF7482D9FF6370D4FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF848FDDFF8D97DFFFACB3E8FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFF969FE2FF6370D4FF969FE2FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
          FEFFD4D8F3FFBAC0ECFF4656CCFF4656CCFF969FE2FFF9FAFDFFFFFFFFFFFFFF
          FFFFA4ACE6FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE5E7F8FFBAC0ECFF4656CCFF4656CCFF6E7BD7FFD1D5F2FFE5E7F8FFBAC0
          ECFF818CDCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF6875D6FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFFACB3E8FFBAC0ECFFB1B8E9FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF5463D0FF7482D9FF969FE2FFB1B8
          E9FFD1D5F2FFACB3E8FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end>
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 48
    Top = 368
    DockControlHeights = (
      0
      0
      26
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 24117264
  end
end
