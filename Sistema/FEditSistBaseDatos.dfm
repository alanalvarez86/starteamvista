inherited EditSistBaseDatos: TEditSistBaseDatos
  Left = 362
  Top = 149
  Caption = 'Bases de Datos'
  ClientHeight = 227
  ClientWidth = 467
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel [0]
    Left = 25
    Top = 73
    Width = 92
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n de BD:'
  end
  object Label3: TLabel [1]
    Left = 78
    Top = 117
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Usuario:'
  end
  object Label4: TLabel [2]
    Left = 66
    Top = 139
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ubicaci'#243'n:'
  end
  object Label5: TLabel [3]
    Left = 93
    Top = 95
    Width = 24
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo:'
  end
  object Label1: TLabel [4]
    Left = 81
    Top = 51
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = '&C'#243'digo:'
    FocusControl = DB_CODIGO
  end
  object lblProgramEspecial: TLabel [5]
    Left = 6
    Top = 161
    Width = 111
    Height = 13
    Alignment = taRightJustify
    Caption = 'Programaci'#243'n Especial:'
  end
  inherited PanelBotones: TPanel
    Top = 191
    Width = 467
    TabOrder = 7
    inherited OK: TBitBtn
      Left = 299
    end
    inherited Cancelar: TBitBtn
      Left = 384
    end
  end
  inherited PanelSuperior: TPanel
    Width = 467
    TabOrder = 8
    inherited ImprimirBtn: TSpeedButton
      Visible = False
    end
    inherited ImprimirFormaBtn: TSpeedButton
      Visible = False
    end
    inherited CortarBtn: TSpeedButton
      Visible = False
    end
    inherited CopiarBtn: TSpeedButton
      Visible = False
    end
    inherited PegarBtn: TSpeedButton
      Visible = False
    end
    inherited UndoBtn: TSpeedButton
      Visible = False
    end
    inherited ExportarBtn: TZetaSpeedButton
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 467
    TabOrder = 9
    inherited Splitter: TSplitter
      Left = 241
    end
    inherited ValorActivo1: TPanel
      Width = 241
    end
    inherited ValorActivo2: TPanel
      Left = 244
      Width = 223
    end
  end
  object btCambiarUbicacion: TButton [9]
    Left = 402
    Top = 136
    Width = 59
    Height = 22
    Caption = 'Cambiar'
    TabOrder = 6
    OnClick = btCambiarUbicacionClick
  end
  object btCambiarUsuario: TButton [10]
    Left = 402
    Top = 112
    Width = 59
    Height = 21
    Caption = 'Cambiar'
    TabOrder = 4
    OnClick = btCambiarUsuarioClick
  end
  object DB_DESCRIP: TDBEdit [11]
    Left = 120
    Top = 69
    Width = 277
    Height = 21
    DataField = 'DB_DESCRIP'
    DataSource = DataSource
    TabOrder = 1
  end
  object DB_DATOS: TDBEdit [12]
    Left = 120
    Top = 135
    Width = 277
    Height = 21
    DataField = 'DB_DATOS'
    DataSource = DataSource
    Enabled = False
    ReadOnly = True
    TabOrder = 5
  end
  object DB_USRNAME: TDBEdit [13]
    Left = 120
    Top = 113
    Width = 157
    Height = 21
    DataField = 'DB_USRNAME'
    DataSource = DataSource
    Enabled = False
    ReadOnly = True
    TabOrder = 3
  end
  object DB_TIPO: TZetaDBKeyCombo [14]
    Left = 120
    Top = 91
    Width = 156
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
    ListaFija = lfTipoCompanyName
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'DB_TIPO'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object DB_CODIGO: TZetaDBEdit [15]
    Left = 120
    Top = 47
    Width = 143
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 0
    ConfirmEdit = True
    DataField = 'DB_CODIGO'
    DataSource = DataSource
  end
  object chbEspecial: TDBCheckBox [16]
    Left = 119
    Top = 161
    Width = 26
    Height = 17
    BiDiMode = bdLeftToRight
    DataField = 'DB_ESPC'
    DataSource = DataSource
    ParentBiDiMode = False
    TabOrder = 10
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  inherited DataSource: TDataSource
    Left = 24
    Top = 89
  end
end
