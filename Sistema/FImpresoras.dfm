inherited Impresoras: TImpresoras
  Left = 153
  Top = 95
  Caption = 'Impresoras'
  PixelsPerInch = 96
  TextHeight = 13
  object ZetaDBGrid: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 429
    Height = 254
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'PI_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 308
        Visible = True
      end>
  end
end
