unit FSistEditUsuarioRoles_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, CheckLst, Buttons, ExtCtrls,
     ZetaSmartLists, ZBaseDlgModal_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, cxContainer, cxEdit, cxCheckListBox, ImgList, cxButtons;

type
  TFSistEditUsuarioRoles_DevEx = class(TZetaDlgModal_DevEx)
    Roles: TcxCheckListBox;
    Prender: TcxButton;
    Apagar: TcxButton;
    PanelBusqueda: TPanel;
    EmpleadoBuscaBtn: TcxButton;
    EditBusca: TEdit;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
    procedure PrenderClick(Sender: TObject);
    procedure ApagarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure RolesClickCheck2(Sender: TObject);
    procedure EmpleadoBuscaBtnClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure RolesClickCheck(Sender: TObject; AIndex: Integer; APrevState,
      ANewState: TcxCheckBoxState);
  private
    { Private declarations }
    FLista: TStrings;
    FUltimoTexto: String;
    FPosUltimo: Integer;
    function EncontroSuper: Boolean;
    procedure PrendeApaga( const lState: Boolean );
    procedure Cargar;
    procedure Descargar;
    procedure HabilitaControlesDerecho;
  public
    { Public declarations }
  end;

var
  SistEditUsuarioRoles_DevEx: TFSistEditUsuarioRoles_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     DSistema,
     ZAccesosTress,
     ZAccesosMgr
     {$IFNDEF LABOR}
     {$IFNDEF CAJAAHORRO}
     ,FSistCalendarioReportes_DevEx
     {$ENDIF}
     {$ENDIF}
     ;

{$R *.DFM}

procedure TFSistEditUsuarioRoles_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FLista := TStringList.Create;
end;

procedure TFSistEditUsuarioRoles_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     dmSistema.cdsRoles.Conectar;
     dmSistema.cdsUserRoles.Refrescar;
     {$endif}
     Caption := 'Roles asignados al usuario: '+ dmSistema.cdsUsuarios.FieldByName('US_CORTO').AsString;
     Cargar;
     OK_DevEx.Enabled := False;
     FUltimoTexto:= VACIO;
     FPosUltimo:= -1;
     HabilitaControlesDerecho;
     EditBusca.Text := VACIO;
     If Roles.Enabled then
        ActiveControl := Roles;

end;

procedure TFSistEditUsuarioRoles_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     FLista.Free;
end;

procedure TFSistEditUsuarioRoles_DevEx.HabilitaControlesDerecho;
begin
     Prender.Enabled :=  ZAccesosMgr.CheckDerecho( D_CAT_ROLES, K_DERECHO_CAMBIO  );
     Apagar.Enabled := Prender.Enabled;
     Roles.Enabled := Prender.Enabled;
end;


procedure TFSistEditUsuarioRoles_DevEx.Cargar;
{$ifndef DOS_CAPAS}
var
   sCodigo, sNombre, sRegistro : string;
{$endif}
begin
     inherited;
     {$ifndef DOS_CAPAS}
     //Cargar los roles en el CheckListBox
     dmSistema.CargaListaRoles(FLista);
     with dmSistema.cdsRoles do
     begin
          First;
          Roles.Clear;
          while not Eof do
          begin
               sCodigo := FieldByName('RO_CODIGO').AsString;
               sNombre := FieldByName('RO_NOMBRE').AsString;
               sRegistro := sCodigo + ' : ' + sNombre;
               with Roles.Items.Add do
               begin
               text := sRegistro ;

               if ( dmSistema.cdsUserRoles.Locate('RO_CODIGO',sCodigo,[])) then
                    Checked := True
               else
                    checked := false;
               end;
               Next;
          end;
     end;
     {$endif}
end;

procedure TFSistEditUsuarioRoles_DevEx.Descargar;
var
   i: Integer;
begin
     with Roles do
     begin
     for i := 0 to ( Count - 1 ) do
        begin
          with Items[i] do
          begin

                    if Checked then
                       FLista.Objects[ i ] := TObject( 1 )
                    else
                        FLista.Objects[ i ] := TObject( 0 );
               end;
          end;
     end;
     dmSistema.DescargaListaUserRoles ( FLista );
end;

procedure TFSistEditUsuarioRoles_DevEx.PrendeApaga( const lState: Boolean );
var
   i: Integer;
begin
     with Roles do
     begin
      try
        Items.BeginUpdate;
        for i := 0 to ( Count - 1 ) do
          with Items[i] do
          begin
            Checked := lState;
          end;
        finally
          Items.EndUpdate;
        end;
     end;
     RolesClickCheck2( Roles );
end;

procedure TFSistEditUsuarioRoles_DevEx.PrenderClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( True );
end;

procedure TFSistEditUsuarioRoles_DevEx.ApagarClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( False );
end;

procedure TFSistEditUsuarioRoles_DevEx.OKClick(Sender: TObject);
begin
     inherited;
     Descargar;
end;

procedure TFSistEditUsuarioRoles_DevEx.RolesClickCheck2(Sender: TObject);
begin
     inherited;
     with OK_DevEx do
     begin
          if not Enabled then
             Enabled := True;
     end;
end;

procedure TFSistEditUsuarioRoles_DevEx.EmpleadoBuscaBtnClick(Sender: TObject);
var
   lEncontro: Boolean;
begin
     if strLleno( EditBusca.Text ) and ( FPosUltimo <> Roles.Count )then
     begin
          lEncontro:= EncontroSuper;
     end
     else
     begin
          lEncontro:= FALSE;
     end;
     if ( not lEncontro ) then
     begin
          Roles.Selected[ FPosUltimo ]:= False;

          if( FPosUltimo > 0 ) then
              ZetaDialogo.ZInformation( 'B�squeda de Roles', '! No hay otro Rol con esos Datos !' ,0 )
          else
              ZetaDialogo.ZInformation( 'B�squeda de Roles', '! No hay un Rol con esos Datos !' ,0 );
          with EditBusca do
          begin
               SetFocus;
               SelectAll;
          end;

     FPosUltimo:=0;
     end;
end;

function TFSistEditUsuarioRoles_DevEx.EncontroSuper: Boolean;
var
   i : Integer;
   sTexto: String;
begin
     sTexto:= UpperCase( EditBusca.Text );
     with Roles do
     begin
          if ( sTexto <> FUltimoTexto ) then
          begin
               FUltimoTexto := sTexto;
               FPosUltimo:= 0;
          end;
          Result:= False;
          for i:= FPosUltimo  to ( Count - 1 ) do
          begin
               if ( Pos( sTexto, UpperCase( Items[i].text ) ) > 0 ) then
               begin
                    FUltimoTexto:= sTexto;
                    FPosUltimo:= i;
                    Selected[ FPosUltimo ]:= True;
                    FPosUltimo:= FPosUltimo + 1;
                    Result:= True;
                    Exit;
               end;
          end;
     end;
end;



procedure TFSistEditUsuarioRoles_DevEx.OK_DevExClick(Sender: TObject);
begin
    inherited;
     Descargar;
end;

procedure TFSistEditUsuarioRoles_DevEx.RolesClickCheck(Sender: TObject;
  AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
begin
inherited;
     with OK_DevEx do
     begin
          if not Enabled then
             Enabled := True;
     end;
end;

end.
