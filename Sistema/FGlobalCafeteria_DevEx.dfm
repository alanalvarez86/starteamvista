inherited GlobalCafeteria_DevEx: TGlobalCafeteria_DevEx
  Left = 289
  Top = 165
  Caption = 'Cafeter'#237'a'
  ClientHeight = 116
  ClientWidth = 407
  Color = clWhite
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 67
    Top = 19
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Letrero con '#233'xito:'
    Color = clWhite
    ParentColor = False
  end
  inherited PanelBotones: TPanel
    Top = 80
    Width = 407
    Color = clWhite
    inherited OK_DevEx: TcxButton
      Left = 240
      Top = 5
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 320
      Top = 5
    end
  end
  object Letrero: TEdit [2]
    Left = 154
    Top = 15
    Width = 244
    Height = 21
    TabOrder = 1
    Text = 'Letrero'
  end
  object AgregarconError: TCheckBox [3]
    Left = 10
    Top = 43
    Width = 157
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Agregar con error al importar:'
    Color = clWhite
    ParentColor = False
    TabOrder = 2
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
