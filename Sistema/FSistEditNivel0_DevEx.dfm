inherited EditNivel0_DevEx: TEditNivel0_DevEx
  Top = 194
  Caption = 'Confidencialidad'
  ClientHeight = 142
  ClientWidth = 428
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 106
    Width = 428
    TabOrder = 2
    inherited OK_DevEx: TcxButton
      Left = 262
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 341
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 428
    inherited ValorActivo2: TPanel
      Width = 102
      inherited textoValorActivo2: TLabel
        Width = 96
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 5
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 50
    Width = 428
    Height = 56
    Align = alClient
    TabOrder = 0
    object DBCodigoLBL: TLabel
      Left = 70
      Top = 10
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
    end
    object DBDescripcionLBL: TLabel
      Left = 47
      Top = 32
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci'#243'n:'
    end
    object TB_CODIGO: TZetaDBEdit
      Left = 109
      Top = 8
      Width = 75
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      ConfirmEdit = True
      DataField = 'TB_CODIGO'
      DataSource = DataSource
    end
    object TB_ELEMENT: TDBEdit
      Left = 109
      Top = 33
      Width = 280
      Height = 21
      DataField = 'TB_ELEMENT'
      DataSource = DataSource
      TabOrder = 1
    end
  end
  inherited DataSource: TDataSource
    Left = 12
    Top = 57
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 8
    Top = 112
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 5767176
  end
end
