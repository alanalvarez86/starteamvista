unit FEditBitacoraSistema_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FBaseEditBitacora_DevEx, DB, ExtCtrls, ZetaDBTextBox, StdCtrls,
  DBCtrls, Buttons, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxContainer, cxEdit,
  ImgList, cxTextEdit, cxMemo, cxDBEdit, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TEditBitacoraSistema_DevEx = class(TBaseEditBitacora_DevEx)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EditBitacoraSistema_DevEx: TEditBitacoraSistema_DevEx;

implementation

uses ZetaCommonClasses;

{$R *.dfm}

procedure TEditBitacoraSistema_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_BITACORA_SISTEMA; 

end;

end.
