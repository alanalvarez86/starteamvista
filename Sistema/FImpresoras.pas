unit FImpresoras;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db,  ExtCtrls, Grids, DBGrids,
     ZBaseConsulta,
     ZetaDBGrid;

type
  TImpresoras = class(TBaseConsulta)
    ZetaDBGrid: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  Impresoras: TImpresoras;

implementation

uses DSistema,
     ZetaCommonClasses;

{$R *.DFM}

procedure TImpresoras.Connect;
begin
     with dmSistema do
     begin
          cdsImpresoras.Conectar;
          DataSource.DataSet := cdsImpresoras;
     end;
end;

procedure TImpresoras.Refresh;
begin
     dmSistema.cdsImpresoras.Refrescar;
end;

procedure TImpresoras.Agregar;
begin
     dmSistema.cdsImpresoras.Agregar;
end;

procedure TImpresoras.Borrar;
begin
     dmSistema.cdsImpresoras.Borrar;
end;

procedure TImpresoras.Modificar;
begin
     dmSistema.cdsImpresoras.Modificar;
end;

procedure TImpresoras.FormCreate(Sender: TObject);
begin
  inherited;
     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_CONS_IMPRESORAS;
     {$endif}
     {$ifdef WORKFLOWCFG}
     HelpContext := H_WORKFLOWCFG_SISTEMA_IMPRESORA;
     {$endif}
end;

end.
