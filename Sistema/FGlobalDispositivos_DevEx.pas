{$HINTS OFF}
unit FGlobalDispositivos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, ZetaSmartLists, StdCtrls, ComCtrls,
  ExtCtrls, ZBaseGlobal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxControls, cxPC, ImgList, cxButtons,
  ZetaSmartLists_DevEx, cxContainer, cxEdit, cxListBox;

type
  TGlobalDispositivos_DevEx = class(TBaseGlobal_DevEx)
    PageControl: TcxPageControl;
    tsRelojes: TcxTabSheet;
    Label20: TLabel;
    LBDisponiblesReloj: TZetaSmartListBox_DevEx;
    ZetaSmartListsButton1: TZetaSmartListsButton_DevEx;
    ZetaSmartListsButton2: TZetaSmartListsButton_DevEx;
    LBAsignadosReloj: TZetaSmartListBox_DevEx;
    Label21: TLabel;
    ZetaSmartLists: TZetaSmartLists_DevEx;
    tsCafeteria: TcxTabSheet;
    tsCaseta: TcxTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    LBDisponiblesCafe: TZetaSmartListBox_DevEx;
    ZetaSmartListsButton3: TZetaSmartListsButton_DevEx;
    ZetaSmartListsButton4: TZetaSmartListsButton_DevEx;
    LBAsignadosCafe: TZetaSmartListBox_DevEx;
    Label3: TLabel;
    Label4: TLabel;
    LBDisponiblesCase: TZetaSmartListBox_DevEx;
    ZetaSmartListsButton5: TZetaSmartListsButton_DevEx;
    ZetaSmartListsButton6: TZetaSmartListsButton_DevEx;
    LBAsignadosCase: TZetaSmartListBox_DevEx;
    ZetaSmartLists1: TZetaSmartLists_DevEx;
    ZetaSmartLists2: TZetaSmartLists_DevEx;
    lblRelojes: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    lblCafeteria: TLabel;
    lblCaseta: TLabel;
    tsKiosco: TcxTabSheet;
    tsMisDatos: TcxTabSheet;
    lblKiosco: TLabel;
    Label6: TLabel;
    LBDisponiblesKiosco: TZetaSmartListBox_DevEx;
    ZetaSmartListsButton_DevEx1: TZetaSmartListsButton_DevEx;
    ZetaSmartListsButton_DevEx2: TZetaSmartListsButton_DevEx;
    LBAsignadosKiosco: TZetaSmartListBox_DevEx;
    Label7: TLabel;
    lblMisDatos: TLabel;
    Label9: TLabel;
    LBDisponiblesMisDatos: TZetaSmartListBox_DevEx;
    ZetaSmartListsButton_DevEx3: TZetaSmartListsButton_DevEx;
    ZetaSmartListsButton_DevEx4: TZetaSmartListsButton_DevEx;
    LBAsignadosMisDatos: TZetaSmartListBox_DevEx;
    Label12: TLabel;
    ZetaSmartLists3: TZetaSmartLists_DevEx;
    ZetaSmartLists4: TZetaSmartLists_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Cargar;override;
    procedure DesCargar;override;        
  end;

var
  GlobalDispositivos_DevEx: TGlobalDispositivos_DevEx;

implementation
uses
    ZAccesosTress,
    DSistema,
    ZetaCommonLists,
    ZetaCommonClasses;
{$R *.dfm}

procedure TGlobalDispositivos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_GLOBAL_DISPOSITIVOS;
     HelpContext := H_DISPOSIT_GLOBAL;
     
     lblRelojes.Caption := 'Seleccione solamente los Relojes Checadores que' +CR_LF + '�sta Empresa utilizar� al Procesar Tarjetas.'
     +CR_LF+ ''  +CR_LF+
     'Si no se selecciona ninguno,' +CR_LF+ ' la Empresa procesar� todos los relojes.';

     lblCafeteria.Caption := 'Seleccione solamente las cafeter�as que �sta ' +CR_LF + ' Empresa utilizar� al Procesar Tarjetas.'
     +CR_LF+ ''  +CR_LF+
     'Si no se selecciona ninguna, '+CR_LF+ ' la Empresa procesar� todas las cafeter�as.';
     
     lblCaseta.Caption := 'Seleccione solamente las casetas que �sta ' +CR_LF + ' Empresa utilizar� al Procesar Tarjetas.'
     +CR_LF+ ''  +CR_LF+
     'Si no se selecciona ninguna,'  +CR_LF+ ' la Empresa procesar� todas las casetas.';

     lblKiosco.Caption := 'Seleccione solamente los kioscos que' +CR_LF + '�sta empresa utilizar�.'
     +CR_LF+ ''  +CR_LF+
     'Si no se selecciona ninguno,' +CR_LF+ ' la empresa utilizar� todos los kioscos.';

     lblMisDatos.Caption := 'Seleccione solamente Mis Datos que' +CR_LF + '�sta empresa utilizar�.'
     +CR_LF+ ''  +CR_LF+
     'Si no se selecciona ninguno,' +CR_LF+ ' la empresa utilizar� todos los Mis Datos.';
end;

procedure TGlobalDispositivos_DevEx.Cargar;
const
     K_FILTRO =  'DI_TIPO = %d';
var
   i : eLstDispositivos;
   oLista : TStrings;
begin
     inherited;
     dmSistema.GetListaDispositivos;
     for i:= Low( eLstDispositivos ) to High( eLstDispositivos ) do
     begin
          with dmSistema.cdsLstDispDisponibles do
          begin
               Filter := Format( K_FILTRO, [ ord(i) ] );
               Filtered := TRUE;
               while not EOF do
               begin
                    case i of
                         dpRelojChecados :oLista := LBDisponiblesReloj.Items;
                         dpCafeteria: oLista := LBDisponiblesCafe.Items;
                         dpCaseta: oLista := LBDisponiblesCase.Items;
                         dpKiosco: oLista := LBDisponiblesKiosco.Items;
                         dpMisDatos: oLista := LBDisponiblesMisDatos.Items;
                         else oLista:= NIL;
                    end;
                    if oLista <> Nil then
                       oLista.AddObject( Fieldbyname('DI_NOMBRE').AsString + '=' + FieldByName('DI_DESCRIP').AsString , TObject(i) );
                    next;
               end;
          end;

          dmSistema.cdsSistLstDispositivos.Conectar;
          with dmSistema.cdsLstDispAsignados do
          begin
               Filter := Format( K_FILTRO, [ ord(i) ] );
               Filtered := TRUE;
               while not EOF do
               begin
                    case i of
                         dpRelojChecados : oLista := LBAsignadosReloj.Items;
                         dpCafeteria: oLista := LBAsignadosCafe.Items;
                         dpCaseta: oLista := LBAsignadosCase.Items;
                         dpKiosco: oLista := LBAsignadosKiosco.Items;
                         dpMisDatos: oLista := LBAsignadosMisDatos.Items;
                         else oLista:= NIL;
                    end;
                    if oLista<> Nil then
                    begin
                         if NOT dmSistema.cdsSistLstDispositivos.Locate( 'DI_NOMBRE;DI_TIPO',VarArrayOf([ Fieldbyname('DI_NOMBRE').AsString, i ] ), [] ) then
                           raise Exception.Create( 'No se encontr� Dispositivo' + dmSistema.cdsSistLstDispositivos.Fieldbyname('DI_NOMBRE').AsString );
                         oLista.AddObject( Fieldbyname('DI_NOMBRE').AsString + '=' + dmSistema.cdsSistLstDispositivos.FieldByName('DI_DESCRIP').AsString, TObject(i) );
                    end;
                    next;
               end;
          end;
     end;
end;

procedure TGlobalDispositivos_DevEx.DesCargar;
begin
     inherited;
     with dmSistema do
     begin
         PreparaListaDispxCom;
         LlenacdsLstDispAsignados( LBAsignadosReloj.Items );
         LlenacdsLstDispAsignados( LBAsignadosCafe.Items );
         LlenacdsLstDispAsignados( LBAsignadosCase.Items );
         LlenacdsLstDispAsignados( LBAsignadosKiosco.Items );
         LlenacdsLstDispAsignados( LBAsignadosMisDatos.Items );
         GrabaDispxCom;
     end;
end;

procedure TGlobalDispositivos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage:= tsRelojes;
end;

end.
