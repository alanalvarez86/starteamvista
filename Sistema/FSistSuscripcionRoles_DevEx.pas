unit FSistSuscripcionRoles_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, CheckLst, Buttons, ExtCtrls,
  ZetaCommonTools, ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, ImgList, cxButtons, cxControls, cxContainer, cxEdit,
  cxCheckListBox, cxTextEdit;

type
  TSistSuscripcionRoles_DevEx = class(TZetaDlgModal_DevEx)
    Lista: TcxCheckListBox;
    Apagar: TcxButton;
    Prender: TcxButton;
    Label1: TLabel;
    BuscaBtn: TcxButton;
    EditBusca: TEdit;
    PanelBusqueda: TPanel;
    PanelIdentifica: TPanel;
    Splitter: TSplitter;
    ValorActivo1: TPanel;
    textoValorActivo1: TLabel;
    ValorActivo2: TPanel;
    textoValorActivo2: TLabel;
    procedure PrenderClick(Sender: TObject);
    procedure ApagarClick(Sender: TObject);
    procedure PrendeApaga( const lState: Boolean );
    procedure Cargar;
    procedure Descargar;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure CheckCLick;
    procedure ListaClickCheck(Sender: TObject; AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
    procedure BuscaBtnClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FLista: TStrings;
    FUltimoTexto: String;
    FPosUltimo: Integer;
    function EncontroSuper: Boolean;
  public
    { Public declarations }
  end;

var
  SistSuscripcionRoles_DevEx: TSistSuscripcionRoles_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaDialogo,
     DSistema,
     dCatalogos,
     FSistCalendarioReportes_DevEx,
     DCliente;

{$R *.dfm}

{ TSeleccionarConfidencialidad }

procedure TSistSuscripcionRoles_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FLista := TStringList.Create;
end;

procedure TSistSuscripcionRoles_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     FLista.Free;
end;

procedure TSistSuscripcionRoles_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     dmSistema.cdsSistTareaRoles.Conectar;
     dmSistema.cdsRoles.Conectar;
     Cargar;
     OK_DevEx.Enabled := False;
     ActiveControl := Lista;
     FUltimoTexto:= VACIO;
     FPosUltimo:= -1;
     if  dmSistema.EnvioProgramadoXEmpresa = true then
     begin
         textoValorActivo1.Caption := dmSistema.cdsEnviosProgramadosEmpresa.FieldByName('CA_NOMBRE').AsString;
         textoValorActivo2.Caption := dmSistema.cdsEnviosProgramadosEmpresa.FieldByName('CM_NOMBRE').AsString;
     end
     else
     begin
         textoValorActivo1.Caption := dmSistema.cdsSistTareaCalendario.FieldByName('CA_NOMBRE').AsString;
         textoValorActivo2.Caption := dmSistema.cdsSistTareaCalendario.FieldByName('CM_NOMBRE').AsString;
     end;
end;

procedure TSistSuscripcionRoles_DevEx.ListaClickCheck(Sender: TObject; AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
begin
     inherited;
     with OK_DevEx do
     begin
          if not Enabled then
             Enabled := True;
     end;
end;

procedure TSistSuscripcionRoles_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     Descargar;
end;

procedure TSistSuscripcionRoles_DevEx.Cargar;
var
   i: Integer;
begin
     dmSistema.CargaListaRoles( FLista );
     dmSistema.cdsSistTareaRoles.Refrescar;
     with Lista do
     begin
          try
             Clear;
             Items.BeginUpdate;
             with FLista do
             begin
                  for i := 0 to ( Count - 1 ) do
                  begin
                       with Items.Add do
                       begin
                            text := ( FLista[i] );
                            Checked := False;

                            dmSistema.cdsSistTareaRoles.First;
                            while not dmSistema.cdsSistTareaRoles.Eof do
                            begin
                                 if Trim(Names[i]) = dmSistema.cdsSistTareaRoles.FieldByName('RO_CODIGO').AsString then
                                      Checked := zStrToBool( dmSistema.cdsSistTareaRoles.FieldByName('RS_ACTIVO').AsString );

                                 dmSistema.cdsSistTareaRoles.Next;
                            end;
                       end
                  end;
             end;
          finally
                 Items.EndUpdate;
          end;
     end;
end;

procedure TSistSuscripcionRoles_DevEx.Descargar;
var
   i: Integer;
begin
     with Lista do
     begin
          for i := 0 to ( Items.Count - 1 ) do
          begin
               with items[i] do
               begin
                    if Checked then
                       FLista.Objects[ i ] := TObject( 1 )
                    else
                        FLista.Objects[ i ] := TObject( 0 );
               end;
          end;
     end;
     dmSistema.DescargaListaRolesCalendario(FLista );
end;

function TSistSuscripcionRoles_DevEx.EncontroSuper: Boolean;
var
   i : Integer;
   sTexto: String;
begin
     sTexto:= UpperCase( EditBusca.Text );
     with Lista do
     begin
          if ( sTexto <> FUltimoTexto ) then
          begin
               FUltimoTexto := sTexto;
               FPosUltimo:= 0;
          end;
          Result:= False;
          for i:= FPosUltimo  to ( Count - 1 ) do
          begin
               if ( Pos( sTexto, UpperCase( Items[i].text ) ) > 0 ) then
               begin
                    FUltimoTexto:= sTexto;
                    FPosUltimo:= i;
                    Selected[ FPosUltimo ]:= True;
                    FPosUltimo:= FPosUltimo + 1;
                    Result:= True;
                    Exit;
               end;
          end;
     end;
end;

procedure TSistSuscripcionRoles_DevEx.BuscaBtnClick(Sender: TObject);
var
   lEncontro: Boolean;
begin
     if strLleno( EditBusca.Text ) and ( FPosUltimo <> Lista.Count )then
     begin
          lEncontro:= EncontroSuper;
     end
     else
     begin
          lEncontro:= FALSE;
     end;
     if ( not lEncontro ) then
     begin
          Lista.Selected[ FPosUltimo ]:= False;

          if( FPosUltimo > 0 ) then
              ZetaDialogo.ZInformation( 'B�squeda de Roles', '� No hay otro rol con esos datos !' ,0 )
          else
              ZetaDialogo.ZInformation( 'B�squeda de Roles', '� No hay un rol con esos datos !' ,0 );
          with EditBusca do
          begin
               SetFocus;
               SelectAll;
          end;

     FPosUltimo:=0;
     end;
end;

procedure TSistSuscripcionRoles_DevEx.PrendeApaga( const lState: Boolean );
var
   i: Integer;
begin
     with Lista do
     begin
     items.BeginUpdate;
        try
            for i := 0 to (items.Count - 1 ) do
            begin
                with Items[i] do
                begin
                    Checked := lState;
                end;
            end;
        finally
            items.EndUpdate;
        end;
     end;
     CheckCLick;
end;

procedure TSistSuscripcionRoles_DevEx.CheckCLick;
begin
     with OK_DevEx do
     begin
          if not Enabled then
             Enabled := True;
     end;
end;

procedure TSistSuscripcionRoles_DevEx.PrenderClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( True );
end;

procedure TSistSuscripcionRoles_DevEx.ApagarClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( False );
end;

end.
