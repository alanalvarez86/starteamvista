unit FGlobalBloqueo_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ZetaKeyCombo, ZetaDBTextBox, Buttons, ExtCtrls,
  ZBaseGlobal_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, ImgList, cxButtons;

type
  TGlobalBloqueo_DevEx = class(TBaseGlobal_DevEx)
    GroupBox3: TGroupBox;
    gbBloqueStatus: TGroupBox;
    StatusLimite2lbl: TLabel;
    StatusLimite2: TZetaKeyCombo;
    gbBloqueoFecha: TGroupBox;
    StatusLimiteLBL: TLabel;
    TipoNominaLBL: TLabel;
    LimiteCambioTarjetalbl: TLabel;
    FechaLimite: TZetaTextBox;
    bbEditarFecha: TcxButton;
    StatusLimite: TZetaKeyCombo;
    TipoNomina: TZetaKeyCombo;
    rbBloqueoFecha: TRadioButton;
    rbBloqueoStatus: TRadioButton;
    procedure bbEditarFechaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure rbBloqueoFechaClick(Sender: TObject);
    procedure rbBloqueoStatusClick(Sender: TObject);
  private
    { Private declarations }
    procedure GetFechaLimite;
    procedure EnabledBloqueoFecha(const lEnabled: Boolean);
    procedure EnabledBloqueoStatus(const lEnabled: Boolean);
  public
    { Public declarations }
  end;

var
  GlobalBloqueo_DevEx: TGlobalBloqueo_DevEx;

implementation

{$R *.DFM}

uses
     FTressShell,
     DCliente,
     DGlobal,
     FAutoClasses,
     ZAccesosTress,
     ZGlobalTress,
     ZBaseDlgModal_DevEx,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaCommonLists,
     FEditFechaLimite_DevEx;

procedure TGlobalBloqueo_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos            := ZAccesosTress.D_CAT_CONFI_BLOQ_ASIST;
     StatusLimite.Tag         := K_LIMITE_MODIFICAR_ASISTENCIA;
     TipoNomina.Tag           := K_GLOBAL_TIPO_NOMINA;

     //Se utiliza el mismo Global, ya que no pueden usarse los dos mecanismos al mismo tiempo.
     StatusLimite2.Tag         := K_LIMITE_MODIFICAR_ASISTENCIA;
     rbBloqueoStatus.Tag       := K_GLOBAL_BLOQUEO_X_STATUS;
     HelpContext               := H65115_Globales_de_bloqueo_de_Asistencia;


end;

procedure TGlobalBloqueo_DevEx.FormShow(Sender: TObject);
var
   lAsistencia: Boolean;
   lEnabled : Boolean;
begin
     TipoNomina.ListaFija:=lfTipoPeriodo; //acl
     inherited;
     
     lAsistencia := dmCliente.ModuloAutorizado( okAsistencia );

     rbBloqueoFecha.Enabled := lAsistencia;
     StatusLimite.Enabled := lAsistencia;
     StatusLimiteLBL.Enabled := lAsistencia;
     TipoNomina.Enabled := lAsistencia;
     TipoNominaLBL.Enabled := lAsistencia;

     rbBloqueoStatus.Enabled := lAsistencia;
     StatusLimite2LBL.Enabled := lAsistencia;
     StatusLimite2.Enabled := lAsistencia;

     GetFechaLimite;
     if lAsistencia then
     begin
          lEnabled := rbBloqueoStatus.Checked ;
          EnabledBloqueoFecha( not lEnabled );
     end
     else
     begin
          EnabledBloqueoFecha( lAsistencia );
          EnabledBloqueoStatus( lAsistencia );
     end;
end;

procedure TGlobalBloqueo_DevEx.bbEditarFechaClick(Sender: TObject);
var
   Forma: TZetaDlgModal_DevEx;
begin
     inherited;
     Forma := TEditFechaLimite_DevEx.Create( Application );
     try
        with Forma do
        begin
             ShowModal;
             if ( ModalResult = mrOk ) then
             begin
                  GetFechaLimite;
                  TressShell.GetFechaLimiteShell;
             end;
        end;
     finally
            FreeAndNil( Forma );
     end;
end;

procedure TGlobalBloqueo_DevEx.GetFechaLimite;
begin
     Global.Conectar;
     FechaLimite.Caption := FechaCorta( Global.GetGlobalDate( K_GLOBAL_FECHA_LIMITE ) );
end;

procedure TGlobalBloqueo_DevEx.EnabledBloqueoFecha( const lEnabled: Boolean );
begin
     rbBloqueoFecha.Checked := lEnabled;
     StatusLimiteLBL.Enabled := lEnabled;
     StatusLimite.Enabled := lEnabled;
     TipoNominaLBL.Enabled := lEnabled;
     TipoNomina.Enabled := lEnabled;
     LimiteCambioTarjetalbl.Enabled := lEnabled;
     bbEditarFecha.Enabled := lEnabled;

     if lEnabled then
     begin
          StatusLimite.Tag := K_LIMITE_MODIFICAR_ASISTENCIA;
          StatusLimite2.Tag := 0;
     end;

end;

procedure TGlobalBloqueo_DevEx.EnabledBloqueoStatus( const lEnabled: Boolean );
begin
     rbBloqueoStatus.Checked := lEnabled;
     StatusLimite2LBL.Enabled := lEnabled;
     StatusLimite2.Enabled := lEnabled;

     if lEnabled then
     begin
          StatusLimite.Tag := 0;
          StatusLimite2.Tag := K_LIMITE_MODIFICAR_ASISTENCIA;
     end;
end;

procedure TGlobalBloqueo_DevEx.rbBloqueoFechaClick(Sender: TObject);
 var
    lEnabled : Boolean;
begin
     inherited;
     lEnabled := rbBloqueoFecha.Checked;
     EnabledBloqueoFecha( lEnabled );
     //rbBloqueoStatus.Checked := NOT lEnabled;
     EnabledBloqueoStatus( not lEnabled );
end;

procedure TGlobalBloqueo_DevEx.rbBloqueoStatusClick(Sender: TObject);
 var
    lEnabled : Boolean;
begin
     inherited;
     lEnabled := rbBloqueoStatus.Checked;
     EnabledBloqueoStatus( lEnabled );
     EnabledBloqueoFecha( not lEnabled );

end;

end.






