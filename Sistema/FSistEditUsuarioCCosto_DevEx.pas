unit FSistEditUsuarioCCosto_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, CheckLst, Buttons, ExtCtrls,
     ZetaSmartLists, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxButtons, ZBaseDlgModal_DevEx,
  cxControls, cxContainer, cxEdit, cxCheckListBox, ImgList;

type
  TSistEditUsuarioCCosto_DevEx = class(TZetaDlgModal_DevEx)
    CCosto: TcxCheckListBox;
    Prender: TcxButton;
    Apagar: TcxButton;
    PanelBusqueda: TPanel;
    EmpleadoBuscaBtn: TcxButton;
    EditBusca: TEdit;
    Label1: TLabel;
    MostrarActivos: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure PrenderClick(Sender: TObject);
    procedure ApagarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CCostoClickCheck2(Sender: TObject);
    procedure EmpleadoBuscaBtnClick(Sender: TObject);
    procedure MostrarActivosClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure CCostoClickCheck(Sender: TObject; AIndex: Integer;
      APrevState, ANewState: TcxCheckBoxState);
  private
    { Private declarations }
    FLista: TStrings;
    FUltimoTexto: String;
    FPosUltimo: Integer;
    function EncontroCCosto: Boolean;
    procedure PrendeApaga( const lState: Boolean );
    procedure Cargar;
    procedure Descargar;
  public
    { Public declarations }
  end;

var
  SistEditUsuarioCCosto_DevEx: TSistEditUsuarioCCosto_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     DGlobal,
     DSistema;

{$R *.DFM}

procedure TSistEditUsuarioCCosto_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FLista := TStringList.Create;
end;

procedure TSistEditUsuarioCCosto_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Cargar;
     OK_DevEx.Enabled := False;
     ActiveControl := CCosto;
     with MostrarActivos do
     begin
          Checked:= True;
          Enabled:= True;
     end;
     FUltimoTexto:= VACIO;
     FPosUltimo:= -1;
     EditBusca.Text := VACIO;
end;

procedure TSistEditUsuarioCCosto_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     FLista.Free;
end;

procedure TSistEditUsuarioCCosto_DevEx.Cargar;
var
   i: Integer;
begin
     Caption := dmSistema.CargaListaCCosto( FLista, MostrarActivos.Checked );
     with CCosto do
     begin
          try
             Clear;
             Items.BeginUpdate;
             with FLista do
             begin
                  for i := 0 to ( Count - 1 ) do
                  begin
                       with Items.Add do
                       begin
                       Text := FLista[i] ;
                       Checked := ( Integer( Objects[ i ] ) > 0 );
                    end;
                  end;
             end;
          finally
                 Items.EndUpdate;
          end;
     end;
end;

procedure TSistEditUsuarioCCosto_DevEx.Descargar;
var
   i: Integer;
begin
     with CCosto do
     begin
      for i := 0 to ( Count - 1 ) do
      begin
          with Items[i] do
          begin
                    if Checked then
                       FLista.Objects[ i ] := TObject( 1 )
                    else
                        FLista.Objects[ i ] := TObject( 0 );
               end;
          end;
     end;
     dmSistema.DescargaListaCCosto( FLista );
end;

procedure TSistEditUsuarioCCosto_DevEx.PrendeApaga( const lState: Boolean );
var
   i: Integer;
begin
     with CCosto do
     begin
        try
        items.BeginUpdate;
        for i := 0 to ( Count - 1 ) do
          with Items[i] do
          begin
            Checked := lState;
          end;
         finally
           items.EndUpdate;
        end;
     end;
     CCostoClickCheck2( CCosto );
end;

procedure TSistEditUsuarioCCosto_DevEx.PrenderClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( True );
end;

procedure TSistEditUsuarioCCosto_DevEx.ApagarClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( False );
end;

procedure TSistEditUsuarioCCosto_DevEx.OKClick(Sender: TObject);
begin
     inherited;
     Descargar;
end;

procedure TSistEditUsuarioCCosto_DevEx.CCostoClickCheck2(Sender: TObject);
begin
     inherited;
     with OK_DevEx do
     begin
          if not Enabled then
             Enabled := True;
     end;
     MostrarActivos.Enabled:=False;
end;

procedure TSistEditUsuarioCCosto_DevEx.EmpleadoBuscaBtnClick(Sender: TObject);
var
   lEncontro: Boolean;
begin
     if strLleno( EditBusca.Text ) and ( FPosUltimo <> CCosto.Count )then
     begin
          lEncontro:= EncontroCCosto;
     end
     else
     begin
          lEncontro:= FALSE;
     end;
     if ( not lEncontro ) then
     begin
          CCosto.Selected[ FPosUltimo ]:= False;

          if( FPosUltimo > 0 ) then
              ZetaDialogo.ZInformation( Format('B�squeda de %s' , [ Global.NombreCosteo] ), Format( '! No hay otro(a) %s con esos Datos !', [Global.NombreCosteo] ) ,0 )
          else
              ZetaDialogo.ZInformation( Format('B�squeda de %s' , [ Global.NombreCosteo ] ), Format( '! No hay un(a) %s con esos Datos !' , [Global.NombreCosteo] ) ,0 );
          with EditBusca do
          begin
               SetFocus;
               SelectAll;
          end;

     FPosUltimo:=0;
     end;
end;

function TSistEditUsuarioCCosto_DevEx.EncontroCCosto: Boolean;
var
   i : Integer;
   sTexto: String;
begin
     sTexto:= UpperCase( EditBusca.Text );
     with CCosto do
     begin
          if ( sTexto <> FUltimoTexto ) then
          begin
               FUltimoTexto := sTexto;
               FPosUltimo:= 0;
          end;
          Result:= False;
          for i:= FPosUltimo  to ( Count - 1 ) do
          begin
               if ( Pos( sTexto, UpperCase( Items[i].text ) ) > 0 ) then
               begin
                    FUltimoTexto:= sTexto;
                    FPosUltimo:= i;
                    Selected[ FPosUltimo ]:= True;
                    FPosUltimo:= FPosUltimo + 1;
                    Result:= True;
                    Exit;
               end;
          end;
     end;
end;

procedure TSistEditUsuarioCCosto_DevEx.MostrarActivosClick(Sender: TObject);
begin
     Cargar;
end;

procedure TSistEditUsuarioCCosto_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     Descargar;
end;

procedure TSistEditUsuarioCCosto_DevEx.CCostoClickCheck(Sender: TObject;
  AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
begin
       inherited;
     with OK_DevEx do
     begin
          if not Enabled then
             Enabled := True;
     end;
     MostrarActivos.Enabled:=False;

end;

end.
