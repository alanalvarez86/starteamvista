unit FSistEditUsuarioCCosto;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, CheckLst, Buttons, ExtCtrls,
     ZBaseDlgModal, ZetaSmartLists;

type
  TSistEditUsuarioCCosto = class(TZetaDlgModal)
    CCosto: TCheckListBox;
    Prender: TBitBtn;
    Apagar: TBitBtn;
    PanelBusqueda: TPanel;
    EmpleadoBuscaBtn: TZetaSpeedButton;
    EditBusca: TEdit;
    Label1: TLabel;
    MostrarActivos: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure PrenderClick(Sender: TObject);
    procedure ApagarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CCostoClickCheck(Sender: TObject);
    procedure EmpleadoBuscaBtnClick(Sender: TObject);
    procedure MostrarActivosClick(Sender: TObject);
  private
    { Private declarations }
    FLista: TStrings;
    FUltimoTexto: String;
    FPosUltimo: Integer;
    function EncontroCCosto: Boolean;
    procedure PrendeApaga( const lState: Boolean );
    procedure Cargar;
    procedure Descargar;
  public
    { Public declarations }
  end;

var
  SistEditUsuarioCCosto: TSistEditUsuarioCCosto;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     DGlobal,
     DSistema;

{$R *.DFM}

procedure TSistEditUsuarioCCosto.FormCreate(Sender: TObject);
begin
     inherited;
     FLista := TStringList.Create;
end;

procedure TSistEditUsuarioCCosto.FormShow(Sender: TObject);
begin
     inherited;
     Cargar;
     OK.Enabled := False;
     ActiveControl := CCosto;
     with MostrarActivos do
     begin
          Checked:= True;
          Enabled:= True;
     end;
     FUltimoTexto:= VACIO;
     FPosUltimo:= -1;
end;

procedure TSistEditUsuarioCCosto.FormDestroy(Sender: TObject);
begin
     inherited;
     FLista.Free;
end;

procedure TSistEditUsuarioCCosto.Cargar;
var
   i: Integer;
begin
     Caption := dmSistema.CargaListaCCosto( FLista, MostrarActivos.Checked );
     with CCosto do
     begin
          try
             Clear;
             Items.BeginUpdate;
             with FLista do
             begin
                  for i := 0 to ( Count - 1 ) do
                  begin
                       Items.Add( FLista[i] );
                       Checked[ i ] := ( Integer( Objects[ i ] ) > 0 );
                  end;
             end;
          finally
                 Items.EndUpdate;
          end;
     end;
end;

procedure TSistEditUsuarioCCosto.Descargar;
var
   i: Integer;
begin
     with CCosto do
     begin
          with Items do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    if Checked[ i ] then
                       FLista.Objects[ i ] := TObject( 1 )
                    else
                        FLista.Objects[ i ] := TObject( 0 );
               end;
          end;
     end;
     dmSistema.DescargaListaCCosto( FLista );
end;

procedure TSistEditUsuarioCCosto.PrendeApaga( const lState: Boolean );
var
   i: Integer;
begin
     with CCosto do
     begin
          with Items do
          begin
               try
                  BeginUpdate;
                  for i := 0 to ( Count - 1 ) do
                      Checked[ i ] := lState;
               finally
                      EndUpdate;
               end;
          end;
     end;
     CCostoClickCheck( CCosto );
end;

procedure TSistEditUsuarioCCosto.PrenderClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( True );
end;

procedure TSistEditUsuarioCCosto.ApagarClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( False );
end;

procedure TSistEditUsuarioCCosto.OKClick(Sender: TObject);
begin
     inherited;
     Descargar;
end;

procedure TSistEditUsuarioCCosto.CCostoClickCheck(Sender: TObject);
begin
     inherited;
     with OK do
     begin
          if not Enabled then
             Enabled := True;
     end;
     MostrarActivos.Enabled:=False;
end;

procedure TSistEditUsuarioCCosto.EmpleadoBuscaBtnClick(Sender: TObject);
var
   lEncontro: Boolean;
begin
     if strLleno( EditBusca.Text ) and ( FPosUltimo <> CCosto.Count )then
     begin
          lEncontro:= EncontroCCosto;
     end
     else
     begin
          lEncontro:= FALSE;
     end;
     if ( not lEncontro ) then
     begin
          CCosto.Selected[ FPosUltimo ]:= False;

          if( FPosUltimo > 0 ) then
              ZetaDialogo.ZInformation( Format('B�squeda de %s' , [ Global.NombreCosteo] ), Format( '! No hay otro(a) %s con esos Datos !', [Global.NombreCosteo] ) ,0 )
          else
              ZetaDialogo.ZInformation( Format('B�squeda de %s' , [ Global.NombreCosteo ] ), Format( '! No hay un(a) %s con esos Datos !' , [Global.NombreCosteo] ) ,0 );
          with EditBusca do
          begin
               SetFocus;
               SelectAll;
          end;

     FPosUltimo:=0;
     end;
end;

function TSistEditUsuarioCCosto.EncontroCCosto: Boolean;
var
   i : Integer;
   sTexto: String;
begin
     sTexto:= UpperCase( EditBusca.Text );
     with CCosto do
     begin
          if ( sTexto <> FUltimoTexto ) then
          begin
               FUltimoTexto := sTexto;
               FPosUltimo:= 0;
          end;
          Result:= False;
          for i:= FPosUltimo  to ( Count - 1 ) do
          begin
               if ( Pos( sTexto, UpperCase( Items[i] ) ) > 0 ) then
               begin
                    FUltimoTexto:= sTexto;
                    FPosUltimo:= i;
                    Selected[ FPosUltimo ]:= True;
                    FPosUltimo:= FPosUltimo + 1;
                    Result:= True;
                    Exit;
               end;
          end;
     end;
end;

procedure TSistEditUsuarioCCosto.MostrarActivosClick(Sender: TObject);
begin
     Cargar;
end;

end.
