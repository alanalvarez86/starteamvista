unit FConfiguraTress;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, ComCtrls, Registry,
  Grids, DBGrids, Db, FileCtrl, ZBaseDlgModal_DevEx, ZetaDBGrid, ZetaKeyCombo, ZetaMessages, Mask, ZetaNumero, ZetaEdit, OleCtrls,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  dxBarBuiltInMenu, cxControls, cxContainer, cxEdit, cxGroupBox,
  dxCheckGroupBox, cxTrackBar, cxSpinEdit, cxSpinButton, cxCheckBox, cxMaskEdit,
  cxDropDownEdit, cxTextEdit, cxPC, Vcl.ImgList, cxButtons, SFE, ZetaHora,
  dxSkinsCore, TressMorado2013, dxSkinscxPCPainter, ZetaCommonLists,
  ZBaseEdicion_DevEx, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  cxNavigator, cxDBNavigator, Vcl.DBCtrls, cxDBEdit, dxDBCheckGroupBox,
  ZBaseEdicionRenglon_DevEx, cxDBTrackBar;

type

  TFormaConfigura = class(TBaseEdicionRenglon_DevEx)
    PanelGeneral: TPanel;
    Label1: TLabel;
    Label4: TLabel;
    Label15: TLabel;
    lblTipoComida: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Simultaneas: TZetaDBKeyCombo;
    DefaultGB: TGroupBox;
    Label18: TLabel;
    Label19: TLabel;
    edDefaultEmpresa: TDBEdit;
    edDefaultCredencial: TDBEdit;
    chkReiniciaMediaNoche: TDBCheckBox;
    Letrero: TGroupBox;
    Label3: TLabel;
    editLetrero: TDBEdit;
    Vista: TGroupBox;
    cbMostrarFoto: TDBCheckBox;
    cbMostrarComidas: TDBCheckBox;
    cbMostrarSigComida: TDBCheckBox;
    pCalendario: TPanel;
    Label8: TLabel;
    pEventos: TPanel;
    Label29: TLabel;
    cbSemana: TZetaKeyCombo;
    ZCombo: TZetaDBKeyCombo;
    editTipoComida3: TDBEdit;
    editTipoComida4: TDBEdit;
    editTipoComida1: TDBEdit;
    editTipoComida2: TDBEdit;
    editTipoComida5: TDBEdit;
    editTipoComida8: TDBEdit;
    editTipoComida9: TDBEdit;
    editTipoComida6: TDBEdit;
    editTipoComida7: TDBEdit;
    Label22: TLabel;
    Label23: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label24: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    eGafeteAdmon: TDBEdit;
    eClaveAdmon: TDBEdit;
    cbConcesionImprimir: TDBCheckBox;
    cbConcesionCancelar: TDBCheckBox;
    OpenDialogSonidos: TOpenDialog;
    OpenDialog: TOpenDialog;
    tbVelocidad_DevEx: TcxTrackBar;
    ArchivoSeekCalendario_DevEx: TcxButton;
    EditCalendario_DevEx: TDBEdit;
    cxImageList19_PanelBotones: TcxImageList;
    Label32: TLabel;
    editTiempoEspera: TZetaDBNumero;
    Label36: TLabel;
    cbUsaTeclado: TdxCheckGroupBox;
    cbTipoComida: TDBCheckBox;
    cbCantidad: TDBCheckBox;
    Comida: TcxTabSheet;
    Seguridad: TcxTabSheet;
    CF_NOMBRE: TDBEdit;
    editTipo: TcxTextEdit;
    UDTipoComida_DevEx: TcxSpinButton;

    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure GridRenglonesDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure GridRenglonesColExit(Sender: TObject);
    procedure ConfiguracionChange(Sender: TObject);
    procedure editTipoChange(Sender: TObject);
    procedure cbSemanaChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbTipoGafeteChange(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure cbUsaTecladoClick(Sender: TObject);
    procedure editTipoPropertiesChange(Sender: TObject);
    procedure tbVelocidad_DevExPropertiesChange(Sender: TObject);
    procedure cbUsaTecladoPropertiesChange(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    function GridEnfocado: Boolean;
    procedure SeleccionaSiguienteRenglon;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    procedure TextosTipoComida;
    procedure ActualizaLabelTipoComida;
    procedure CalendarioDeSemana;
    procedure CargaDatos;
  protected
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override; { TWinControl }
    procedure Connect; override;
  public
    { Public declarations }
    procedure SetControlesServidor;
  end;

const
  COL_HORA       = 0;
  COL_ACCION     = 1;
  TAB_CALENDARIO = 1;
  TAB_SERVIDOR   = 2;
  TAB_SEGURIDAD  = 3;
  TAB_IMPRESORA  = 4;
  TAB_SONIDOS    = 5;
  TAB_SERIAL     = 6;
  TAB_CONTADOR   = 7;

var
  FormaConfigura: TFormaConfigura;

implementation

uses
  ZetaCommonTools, ZetaDialogo,
  ZetaCommonClasses, DBClient,
  dSistema, ZAccesosTress, ZetaHelpCafe;

{$R *.DFM}

procedure TFormaConfigura.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_SIST_LST_DISPOSITIVOS;
     Self.HelpContext := H394002_Configuracion_Cafeteria_TRESS;
     cbSemana.ItemIndex       := Ord(dcTodaSemana);
     PageControl.ActivePageIndex := TAB_CALENDARIO;
end;

procedure TFormaConfigura.Connect;
var
   oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        with dmSistema do
        begin
             cdsConfiguracionCafeteria.Refrescar;
             if cdsConfiguracionCafeteria.IsEmpty then
                  cdsConfiguracionCafeteria.Append;

             DataSource.DataSet := cdsConfiguracionCafeteria;
             dsRenglon.DataSet := cdsCalendarioCafeteria;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TFormaConfigura.FormShow(Sender: TObject);
begin
     inherited;
     ActualizaLabelTipoComida;
     CalendarioDeSemana;
     TextosTipoComida;
     CargaDatos;
end;

procedure TFormaConfigura.CargaDatos;
begin
     with dmSistema.cdsConfiguracionCafeteria do
     begin
          tbVelocidad_DevEx.Position := FieldByName('CF_VELOCI').AsInteger;
          editTipo.Text := IntToStr (FieldByName('CF_TIP_COM').AsInteger);
          cbUsaTeclado.CheckBox.Checked :=  zStrToBool (FieldByName('CF_TECLADO').AsString);
     end;
end;

{ Control de Calendario }
procedure TFormaConfigura.BBAgregarClick(Sender: TObject);
begin
     inherited;
     dmSistema.cdsCalendarioCafeteria.Append;
     GridRenglones.SetFocus;
end;

procedure TFormaConfigura.BBBorrarClick(Sender: TObject);
begin
     with dmSistema.cdsCalendarioCafeteria do
     if not IsEmpty then
        Delete;
end;

procedure TFormaConfigura.BBModificarClick(Sender: TObject);
begin
     inherited;
     with dmSistema.cdsCalendarioCafeteria do
     if not IsEmpty then
        Edit;
     GridRenglones.SetFocus;
end;

{ Control de Grid de Calendario }
function TFormaConfigura.GridEnfocado: Boolean;
begin
     Result := (ActiveControl = GridRenglones);
end;

procedure TFormaConfigura.GridRenglonesDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     inherited;
     if (gdFocused in State) then begin
        if (Column.FieldName = 'ACCION') then begin
           with ZCombo do begin
                Left    := Rect.Left + GridRenglones.Left;
                Top     := Rect.Top + GridRenglones.Top;
                Width   := Column.Width + 2;
                Visible := True;
           end;
        end;
     end;
end;

procedure TFormaConfigura.GridRenglonesColExit(Sender: TObject);
begin
     inherited;
     with GridRenglones do begin
          if (SelectedField <> nil) and (SelectedField.FieldName = 'ACCION') and (ZCombo.Visible) then
             ZCombo.Visible := FALSE;
     end;
end;

procedure TFormaConfigura.WMExaminar(var Message: TMessage);
begin
     if (TExaminador(message.LParam) = exEnter) then
        if GridEnfocado then
        begin
             if (GridRenglones.SelectedIndex = COL_HORA) then
                GridRenglones.SelectedIndex := COL_ACCION
             else
                  SeleccionaSiguienteRenglon;
        end;
end;

procedure TFormaConfigura.KeyDown(var Key: Word; Shift: TShiftState);
begin
     if (GridEnfocado) and (Key = VK_RETURN) then
        Key := 0;
     inherited KeyDown(Key, Shift);
end;

procedure TFormaConfigura.KeyPress(var Key: Char);
begin
     if (GridEnfocado) then begin
        if (Key = Chr(VK_RETURN)) then begin
           Key := #0;
           if (GridRenglones.SelectedIndex = COL_HORA) then
              GridRenglones.SelectedIndex := COL_ACCION
           else
               SeleccionaSiguienteRenglon;
           end
           else if (Key = Chr(VK_ESCAPE)) then
           begin
                GridRenglones.SelectedIndex := COL_HORA;
           end
           else if ((Key <> Chr(9)) and (Key <> #0) and (GridRenglones.SelectedIndex = COL_ACCION)) then
           begin
                ZCombo.SetFocus;
                SendMessage(ZCombo.Handle, WM_Char, Word(Key), 0);
                Key := #0;
           end
        end
        else if (ActiveControl = ZCombo) and ((Key = Chr(VK_RETURN)) or (Key = Chr(9))) then
        begin
             Key := #0;
             with GridRenglones do
             begin
                  GridRenglones.SelectedIndex := COL_HORA;
                  SeleccionaSiguienteRenglon;
             end;
        end
     else
         inherited KeyPress(Key);
end;

procedure TFormaConfigura.OK_DevExClick(Sender: TObject);
begin
  inherited;
  dmSistema.ActualizaStatus;
end;

procedure TFormaConfigura.SeleccionaSiguienteRenglon;
begin
     with dmSistema.cdsCalendarioCafeteria do
     begin
          Next;
          if EOF then
             Append;
     end;
     GridRenglones.SelectedIndex := COL_HORA;
end;

procedure TFormaConfigura.ConfiguracionChange(Sender: TObject);
begin
     inherited;
     case PageControl.ActivePageIndex of
          TAB_CALENDARIO: Self.HelpContext := H00002_Configuracion_Calendario;
          //TAB_SERVIDOR: Self.HelpContext := H00003_Configuracion_Servidor;
          TAB_SEGURIDAD: Self.HelpContext := H00004_Configuracion_Seguridad;
          //TAB_IMPRESORA: Self.HelpContext := H00005_Configuracion_Impresora;
          //TAB_SONIDOS: Self.HelpContext := H00006_Configuracion_Sonidos;
          //TAB_SERIAL: Self.HelpContext := H00012_Configuracion_Interfase_Serial;
     else
         Self.HelpContext := H00001_Configuracion_General;
  end;
end;

procedure TFormaConfigura.SetControlesServidor;
begin
end;

procedure TFormaConfigura.tbVelocidad_DevExPropertiesChange(Sender: TObject);
begin
     dmSistema.cdsConfiguracionCafeteria.Edit;
end;

procedure TFormaConfigura.editTipoChange(Sender: TObject);
begin
     inherited;
     dmSistema.cdsConfiguracionCafeteria.Edit;
     ActualizaLabelTipoComida;
end;

procedure TFormaConfigura.editTipoPropertiesChange(Sender: TObject);
begin
     dmSistema.cdsConfiguracionCafeteria.Edit;
end;

procedure TFormaConfigura.TextosTipoComida;
var
   iContador: Integer;
begin
     with ZCombo do
     begin
          if (Items.Count >= 8) then
             for iContador := 0 to 8 do
             begin
                  Items[iContador] := dmSistema.cdsConfiguracionCafeteria.FieldByName('CF_TCOM_' + IntToStr(iContador + 1)).AsString;
             end;
          ItemIndex := 0;
     end;
end;

procedure TFormaConfigura.ActualizaLabelTipoComida;
begin
     lblTipoComida.Caption := VACIO;
     if StrLleno(editTipo.Text) then
        if StrToInt(editTipo.Text) > 0 then
           lblTipoComida.Caption := dmSistema.cdsConfiguracionCafeteria.FieldByName('CF_TCOM_' + editTipo.Text).AsString
end;

procedure TFormaConfigura.CalendarioDeSemana;
begin
     dmSistema.SemanaCalendario := eDiasCafeteria(cbSemana.Valor);
end;

procedure TFormaConfigura.cbSemanaChange(Sender: TObject);
begin
     inherited;
     dmSistema.SemanaCalendario := eDiasCafeteria(cbSemana.Valor);
end;

procedure TFormaConfigura.cbTipoGafeteChange(Sender: TObject);
begin
     inherited;
     //cbLectorProximidad.Enabled := cbTipoGafete.ItemIndex = Ord(egProximidad);
end;

procedure TFormaConfigura.cbUsaTecladoClick(Sender: TObject);
begin
     inherited;
     dmSistema.cdsConfiguracionCafeteria.Edit;
end;

procedure TFormaConfigura.cbUsaTecladoPropertiesChange(Sender: TObject);
begin
     dmSistema.cdsConfiguracionCafeteria.Edit;
end;

procedure TFormaConfigura.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     dmSistema.QuitaFiltro;
end;

end.
