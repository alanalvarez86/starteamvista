unit FEditBitacoraRegBio_DevEx;

interface

uses
    Windows, Messages, SysUtils, Variants, Classes, Graphics,
    Controls, Forms, Dialogs,ZBaseDlgModal_DevEx,cxMemo, cxGraphics,
    cxLookAndFeels, cxLookAndFeelPainters,  dxSkinsCore,
    TressMorado2013, cxControls, cxContainer, cxEdit, cxTextEdit, cxButtons,
    StdCtrls, Menus, ExtCtrls, ImgList, cxRichEdit, ZetaDBTextBox;

type
  TEditBitacoraRegBio_DevEx = class(TZetaDlgModal_DevEx)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    MemBitacora: TcxMemo;
    txtEmpresa: TZetaTextBox;
    txtTerminal: TZetaTextBox;
    txtEmpleado: TZetaTextBox;
    txtFecha: TZetaTextBox;
    procedure OK_DevExClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EditBitacoraRegBio_DevEx: TEditBitacoraRegBio_DevEx;

implementation

uses
    dSistema,
    DBaseSistema;

{$R *.dfm}

procedure TEditBitacoraRegBio_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     with dmSistema do
     begin
          cdsEmpresasLookUp.Conectar;
          cdsTerminales.Conectar;
          cdsBitacoraBio.Conectar;
     end;
end;

procedure TEditBitacoraRegBio_DevEx.FormShow(Sender: TObject);
var
 sCadena  : String;
begin
     inherited;
     sCadena :='';
     MemBitacora.Text:='';
     with dmSistema do
     begin
          txtFecha.Caption    := dmSistema.cdsBitacoraBio.FieldByName( 'WS_FECHA'  ).AsString;
          txtTerminal.Caption := dmSistema.cdsBitacoraBio.FieldByName( 'CH_RELOJ'  ).AsString;
          txtEmpleado.Caption := dmSistema.cdsBitacoraBio.FieldByName( 'CB_CODIGO' ).AsString;
          txtEmpresa.Caption  := dmSistema.cdsBitacoraBio.FieldByName( 'CM_CODIGO' ).AsString;
          sCadena:=  cdsBitacoraBio.FieldByName( 'WS_MENSAJE' ).AsString;
          MemBitacora.Text:= sCadena;
     end;
end;

procedure TEditBitacoraRegBio_DevEx.OK_DevExClick(Sender: TObject);
begin
     Close;
end;

end.
