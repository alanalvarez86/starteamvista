inherited SistBaseUsuarios_DevEx: TSistBaseUsuarios_DevEx
  Left = 580
  Top = 187
  Caption = 'Usuarios'
  ClientHeight = 294
  ClientWidth = 644
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 644
    inherited Slider: TSplitter
      Left = 357
    end
    inherited ValorActivo1: TPanel
      Width = 341
      inherited textoValorActivo1: TLabel
        Width = 335
      end
    end
    inherited ValorActivo2: TPanel
      Left = 360
      Width = 284
      inherited textoValorActivo2: TLabel
        Width = 278
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 49
    Width = 644
    Height = 245
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = DataSource
      object us_codigo: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'us_codigo'
        MinWidth = 80
        Width = 80
      end
      object us_corto: TcxGridDBColumn
        Caption = 'Usuario'
        DataBinding.FieldName = 'us_corto'
        MinWidth = 50
        Width = 72
      end
      object us_nombre: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'us_nombre'
        MinWidth = 50
        Width = 50
      end
      object GR_DESCRIP: TcxGridDBColumn
        Caption = 'Grupo'
        DataBinding.FieldName = 'GR_DESCRIP'
        MinWidth = 80
        Width = 80
      end
      object US_ACTIVO: TcxGridDBColumn
        Caption = 'Activo'
        DataBinding.FieldName = 'US_ACTIVO'
        MinWidth = 80
      end
      object US_DENTRO: TcxGridDBColumn
        Caption = 'Dentro'
        DataBinding.FieldName = 'US_DENTRO'
        MinWidth = 80
        Width = 80
      end
      object US_LUGAR: TcxGridDBColumn
        Caption = 'Tel'#233'fono'
        DataBinding.FieldName = 'US_LUGAR'
        MinWidth = 80
        Width = 80
      end
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 19
    Width = 644
    Height = 30
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 17
      Top = 8
      Width = 33
      Height = 13
      Caption = 'Busca:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 210
      Top = 8
      Width = 33
      Height = 13
      Caption = 'Status:'
    end
    object lblGrupo: TLabel
      Left = 334
      Top = 8
      Width = 32
      Height = 13
      Caption = 'Grupo:'
    end
    object BBusca: TcxButton
      Left = 177
      Top = 4
      Width = 21
      Height = 21
      Hint = 'Buscar Reporte'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = BBuscaClick
      OptionsImage.Glyph.Data = {
        DA050000424DDA05000000000000360000002800000013000000130000000100
        200000000000A4050000000000000000000000000000000000008B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCCC8CCFFCAC6
        CBFF8D838EFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFFB5AEB5FFFFFFFFFFFDFDFDFF9F97A0FF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF9F97A0FFFAF9FAFFFFFFFFFFBAB4BBFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF988F99FFC0BAC0FFD2CED2FFBEB8BFFFEFEDEFFFFFFFFFFFD3D0
        D4FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFAF9FAFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFE9E7E9FF8F8590FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8F8590FFF4F3F4FFF0EF
        F1FFAFA9B0FF908791FFAFA9B0FFFBFBFBFFE9E7E9FF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFFA8A1A9FFFFFFFFFFC5C0C6FF8B818CFF8B818CFF8B818CFFCAC6CBFFFFFF
        FFFF9D959EFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFAAA3ABFFFFFFFFFFB7B0B7FF8B818CFF8B81
        8CFF8B818CFFC5C0C6FFFFFFFFFFA39BA3FF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF908791FFFBFB
        FBFFE9E7E9FF8B818CFF8B818CFF968D97FFEDEBEDFFF8F7F8FF908791FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFFBEB8BFFFFFFFFFFFF8F7F8FFDCD9DDFFFAF9FAFFFDFD
        FDFFB5AEB5FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFAEA7AEFFE7E5
        E8FFF0EFF1FFE0DDE0FFAAA3ABFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
      SpeedButtonOptions.GroupIndex = 1
      SpeedButtonOptions.AllowAllUp = True
    end
    object EBuscaNombre: TEdit
      Left = 52
      Top = 4
      Width = 121
      Height = 21
      TabOrder = 0
      OnChange = EBuscaNombreChange
    end
    object FiltroActivos: TZetaKeyCombo
      Left = 246
      Top = 5
      Width = 85
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 1
      OnChange = FiltroActivosChange
      ListaFija = lfFiltroStatusEmpleado
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
    object FiltroGrupo: TZetaKeyCombo
      Left = 372
      Top = 4
      Width = 125
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 2
      OnChange = FieldGroupChange
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
  end
  inherited DataSource: TDataSource
    Left = 24
    Top = 96
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
    DesignInfo = 8913166
  end
  inherited ActionList: TActionList
    Left = 210
    Top = 136
  end
  inherited PopupMenu1: TPopupMenu
    Left = 120
    Top = 116
  end
end