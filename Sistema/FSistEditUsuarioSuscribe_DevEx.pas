unit FSistEditUsuarioSuscribe_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DBCtrls, Db, Grids, DBGrids,
  ZetaDBGrid, ZetaMessages, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridCustomView, cxClasses, cxGridLevel, cxGrid, ZetaCXGrid,
  ZBaseDlgModal_DevEx, Menus, ImgList, cxButtons;

type
  TFormaSuscribeUsuario_DevEx = class(TZetaDlgModal_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    dsUsuario: TDataSource;
    DBText1: TDBText;
    DBText2: TDBText;
    dsSuscrip: TDataSource;
    ZetaCXGrid1: TZetaCXGrid;
    ZetaCXGrid1Level1: TcxGridLevel;
    ZetaCXGrid1DBTableView1: TcxGridDBTableView;
    ZetaCXGrid1DBTableView1RE_NOMBRE: TcxGridDBColumn;
    ZetaCXGrid1DBTableView1CA_FREC: TcxGridDBColumn;
    ZetaCXGrid1DBTableView1REPORTE: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure ZetaCXGrid1DBTableView1DataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
  private
    { Private declarations }
    function ModificarSuscripcion : Boolean;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
  public
    { Public declarations }
  end;

var
  FormaSuscribeUsuario_DevEx: TFormaSuscribeUsuario_DevEx;

implementation

uses dSistema, FLookupReporte_DevEx, FSistEditSuscribe_DevEx, ZetaCommonLists, ZGridModeTools;

{$R *.DFM}

procedure TFormaSuscribeUsuario_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     {$IFNDEF CAJAAHORRO}
     dsSuscrip.Dataset := dmSistema.cdsSuscripUsuarioCalendario;
     dmSistema.cdsSuscripUsuarioCalendario.Refrescar;
     {$ENDIF}
end;

function TFormaSuscribeUsuario_DevEx.ModificarSuscripcion: Boolean;
begin
end;

procedure TFormaSuscribeUsuario_DevEx.WMExaminar(var Message: TMessage);
begin
end;

procedure TFormaSuscribeUsuario_DevEx.ZetaCXGrid1DBTableView1DataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
begin
  inherited;
 		ZGridModeTools.BorrarItemGenericoAll( AValueList );
     		if ZetaCXGrid1DBTableView1.DataController.IsGridMode then
        		ZGridModeTools.FiltroSetValueLista( ZetaCXGrid1DBTableView1, AItemIndex, AValueList );
end;

end.
