inherited SistEmpresas_DevEx: TSistEmpresas_DevEx
  Top = 119
  Caption = 'Empresas'
  ClientWidth = 821
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 821
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 495
      inherited textoValorActivo2: TLabel
        Width = 489
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 821
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsData.DeletingConfirmation = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object CM_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CM_CODIGO'
        Width = 90
      end
      object CM_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'CM_NOMBRE'
        Width = 250
      end
      object CM_CTRL_RL: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'CM_CTRL_RL'
        Width = 110
      end
      object CM_DIGITO: TcxGridDBColumn
        Caption = 'D'#237'gito'
        DataBinding.FieldName = 'CM_DIGITO'
        Width = 45
      end
      object CM_USACAFE: TcxGridDBColumn
        Caption = 'Cafeter'#237'a'
        DataBinding.FieldName = 'CM_USACAFE'
        Width = 70
      end
      object CM_USACASE: TcxGridDBColumn
        Caption = 'Caseta'
        DataBinding.FieldName = 'CM_USACASE'
        Width = 70
      end
      object CM_DATOS: TcxGridDBColumn
        Caption = 'Base de Datos'
        DataBinding.FieldName = 'CM_DATOS'
        Width = 79
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
