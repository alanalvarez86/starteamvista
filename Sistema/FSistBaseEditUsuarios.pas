unit FSistBaseEditUsuarios;

interface

{$DEFINE SISNOM_CLAVES}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, ComCtrls,
     Forms, Dialogs, Db, ExtCtrls, Buttons, DBCtrls, StdCtrls, Mask,
     ZBaseEdicion,
     ZetaDBTextBox,
     ZetaKeyLookup,
     ZetaKeyCombo,
     ZetaEdit,
     ZetaNumero, ZetaSmartLists;

type
  TSistBaseEditUsuarios = class(TBaseEdicion)
    PageControl: TPageControl;
    Acceso: TTabSheet;
    Operacion: TTabSheet;
    Label5: TLabel;
    US_NOMBRE: TDBEdit;
    Label6: TLabel;
    US_PASSWRD: TDBEdit;
    US_CAMBIA: TDBCheckBox;
    Label3: TLabel;
    GR_CODIGO: TZetaDBKeyLookup;
    Label8: TLabel;
    US_NIVEL: TZetaDBKeyCombo;
    GroupBox1: TGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    US_FEC_IN: TZetaDBTextBox;
    US_FEC_OUT: TZetaDBTextBox;
    US_BLOQUEA: TDBCheckBox;
    US_DENTRO: TDBCheckBox;
    gbEMail: TGroupBox;
    btnSuscripciones: TBitBtn;
    Label4: TLabel;
    US_EMAIL: TDBEdit;
    Label7: TLabel;
    US_FORMATO: TZetaDBKeyCombo;
    Label11: TLabel;
    US_LUGAR: TDBEdit;
    Suspender: TBitBtn;
    Activar: TBitBtn;
    Label12: TLabel;
    US_FEC_SUS: TZetaDBTextBox;
    US_MAQUINA: TZetaDBTextBox;
    Label13: TLabel;
    Pn_Codigos: TPanel;
    Label1: TLabel;
    US_CORTO: TDBEdit;
    Label2: TLabel;
    US_CODIGO: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
    procedure SuspenderClick(Sender: TObject);
    procedure ActivarClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure btnSuscripcionesClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    function GetNavegacion: Boolean;
    procedure SetNavegacion(const Value: Boolean);
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Borrar;override;    
  public
    { Public declarations }
    property Navegacion :Boolean read GetNavegacion write SetNavegacion;
  end;

var
  SistBaseEditUsuarios: TSistBaseEditUsuarios;

implementation

{$R *.DFM}

uses DCliente,
     DSistema,

     FSistEditUsuarioSuscribe,
     ZBaseDlgModal,
     ZAccesosTress,
     ZetaDialogo,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZAccesosMgr
     {$ifdef TRESS},DGlobal,ZGlobalTress{$endif};

procedure TSistBaseEditUsuarios.FormCreate(Sender: TObject);
const
{$ifdef COMPARTE_MGR}
   K_ADV_SUPERVISORES_EMPRESA_EDIT_US_CODIGO = 'Este usuario puede pertenecer a un Supervisor Enrolado. Si cambia el c�digo de este usuario puede quedar alg�n Supervisor sin enrolar.�Est� seguro que desea cambiar el c�digo de este usuario?';
{$else}
   K_ADV_SUPERVISORES_EMPRESA_EDIT_US_CODIGO = 'Este usuario puede pertenecer a un Supervisor Enrolado en esta o en alguna otra de sus empresas. Si cambia el c�digo de este usuario puede quedar alg�n Supervisor sin enrolar.�Est� seguro que desea cambiar el c�digo de este usuario?';
{$endif}
begin
     inherited;
     FirstControl := US_CODIGO;
     HelpContext := H80813_Usuarios;
     IndexDerechos := D_SIST_DATOS_USUARIOS;
     PageControl.ActivePage := Acceso;
     GR_CODIGO.LookUpDataset := dmSistema.cdsGrupos;
     Suspender.Visible:= FALSE;
     Activar.Visible:= FALSE;
{$ifndef DOS_CAPAS}
     US_CODIGO.ConfirmEdit := False;
     {$ifdef COMPARTE_MGR}
     US_CODIGO.ConfirmEdit := True;
     {$endif}
     {$ifdef TRESS}
     if (dmCliente.EmpresaAbierta)  then
        US_CODIGO.ConfirmEdit := Global.GetGlobalBooleano( K_GLOBAL_ENROLAMIENTO_SUPERVISORES );
     {$endif}
     US_CODIGO.ConfirmMsg :=  K_ADV_SUPERVISORES_EMPRESA_EDIT_US_CODIGO;
{$endif}


end;

procedure TSistBaseEditUsuarios.FormShow(Sender: TObject);
begin
     inherited;
     {$ifdef TRESS}
      btnSuscripciones.Enabled:= CheckDerecho( D_SUSCRIPCION_REPORTE, K_DERECHO_CONSULTA ) or
                                CheckDerecho( D_SUSCRIPCION_USUARIOS, K_DERECHO_CONSULTA );
     {$else}
      btnSuscripciones.Enabled:= TRUE
     {$endif}
end;

procedure TSistBaseEditUsuarios.Connect;
begin
     with dmSistema do
     begin
          Datasource.Dataset := cdsUsuarios;
     end;
end;

procedure TSistBaseEditUsuarios.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with DataSource.Dataset do
     begin
          if ( FindField( 'US_DENTRO' ) <> nil ) then
             US_DENTRO.Enabled := ZetaCommonTools.zStrToBool( FieldByName( 'US_DENTRO' ).AsString );
     end;
{$ifdef SISNOM_CLAVES}
     if ( Field = nil ) then
     begin
          with dmSistema do
          begin
               ClaveEncriptada := cdsUsuarios.FieldByName( 'US_PASSWRD' ).AsString;
          end;
     end;
{$endif}
end;


procedure TSistBaseEditUsuarios.SuspenderClick(Sender: TObject);
var
   sMensaje: String;
begin
     if PuedeModificar( sMensaje ) then
        dmSistema.SuspendeUsuarios
     else
         ZetaDialogo.zInformation( Caption, sMensaje, 0 );
end;

procedure TSistBaseEditUsuarios.ActivarClick(Sender: TObject);
var
   sMensaje: String;
begin
     if PuedeModificar( sMensaje ) then
        dmSistema.ActivaUsuarios
     else
         ZetaDialogo.zInformation( Caption, sMensaje, 0 );
end;

procedure TSistBaseEditUsuarios.btnSuscripcionesClick(Sender: TObject);
var
   lDerechoSuscrip, lDerechoSuscripUsuarios, lEsMismoUsuario, lMuestraForma: Boolean;

begin
     inherited;
     lMuestraForma:= False;

     {$ifdef TRESS}
     lDerechoSuscrip:= CheckDerecho( D_SUSCRIPCION_REPORTE, K_DERECHO_CONSULTA );
     lDerechoSuscripUsuarios:= CheckDerecho( D_SUSCRIPCION_USUARIOS, K_DERECHO_CONSULTA );
     {$else}
     lDerechoSuscrip:= TRUE;
     lDerechoSuscripUsuarios:= TRUE;
     {$endif}

     lEsMismoUsuario:= dmSistema.cdsUsuarios.FieldByName('US_CODIGO').AsInteger = dmCliente.Usuario;

     if lDerechoSuscrip and lDerechoSuscripUsuarios then
        lMuestraForma:= True
     else
     begin
          if lDerechoSuscrip then
          begin
               if lEsMismoUsuario then
                  lMuestraForma:= True
               else
                   ZError( 'Reportes', 'No se tiene derecho de suscribir a otros usuarios' , 0 )
          end;

          if lDerechoSuscripUsuarios then
          begin
             if lEsMismoUsuario then
                ZetaDialogo.ZError( 'Reportes', 'No se tiene derecho de suscribirse a un reporte' , 0 )
             else
                lMuestraForma:= True;
          end;
     end;
      if lMuestraForma then
             ZBaseDlgModal.ShowDlgModal( FormaSuscribeUsuario, TFormaSuscribeUsuario );
end;

function TSistBaseEditUsuarios.GetNavegacion: Boolean;
begin
     Result := DBNavigator.Visible;
end;

procedure TSistBaseEditUsuarios.SetNavegacion(const Value: Boolean);
begin
     DBNavigator.Visible := Value;
     AgregarBtn.Visible := Value;

end;



procedure TSistBaseEditUsuarios.Borrar;
begin
   inherited;
end;

end.
