unit FSistEditGrupos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Mask, Db, ExtCtrls,
     Buttons, DBCtrls, ComCtrls, Menus, Grids, DBGrids,
     ZBaseEdicion,
     ZetaMessages,
     ZetaEdit,
     ZetaDBGrid,
     ZetaCommonClasses,
     ZetaNumero, ZetaKeyLookup, ZetaSmartLists;

type
  TSistEditGrupos = class(TBaseEdicion)
    Datos: TGroupBox;
    GR_CODIGOlbl: TLabel;
    GR_DESCRIPlbl: TLabel;
    GR_DESCRIP: TDBEdit;
    GR_CODIGO: TZetaDBNumero;
    Empresas: TGroupBox;
    GridEmpresas: TZetaDBGrid;
    dsEmpresas: TDataSource;
    Accesos: TBitBtn;
    SUBORDINADOlbl: TLabel;
    GR_PADRE: TZetaDBKeyLookup;
    procedure FormCreate(Sender: TObject);
    procedure AccesosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBNavigatorClick(Sender: TObject; Button: TNavigateBtn);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    procedure ActualizaEmpresas;
    procedure ChecaGrupoActual;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure HabilitaControles; override;
  public
    { Public declarations }
  end;

var
  SistEditGrupos: TSistEditGrupos;

const
     K_ASIGNA_DERECHOS_OTROS_GRUPOS = ZetaCommonClasses.K_DERECHO_SIST_KARDEX;
     K_ASIGNA_DERECHOS_GRUPO_PROPIO = ZetaCommonClasses.K_DERECHO_BANCA;

implementation

uses DSistema,
     DCliente,
     DBaseSistema,
     ZAccesosTress,
     ZAccesosMgr,
     ZetaDialogo;

{$R *.DFM}

procedure TSistEditGrupos.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_SIST_DATOS_GRUPOS;

     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_EDIT_GPO_USR;
     {$else}
     HelpContext := H80812_Grupos_usuarios;
     {$endif}        
     FirstControl := GR_CODIGO;
     with GR_PADRE do
     begin
          LookupDataset := dmSistema.cdsGruposLookup;
     end;

end;

procedure TSistEditGrupos.FormShow(Sender: TObject);
begin
     inherited;
end;

procedure TSistEditGrupos.Connect;
begin
     with dmSistema do
     begin
          cdsGrupos.Conectar;
          cdsGruposLookUp.Conectar;
          Datasource.Dataset := cdsGrupos;
          ActualizaEmpresas;
     end;
end;

procedure TSistEditGrupos.ActualizaEmpresas;
begin
     with dmSistema do
     begin
          with cdsEmpresasAccesos do
          begin
               Refrescar;
               Locate( 'CM_CODIGO', dmCliente.Compania, [ loCaseInsensitive ] );
          end;
          dsEmpresas.Dataset := cdsEmpresasAccesos;
     end;
end;

procedure TSistEditGrupos.ChecaGrupoActual;
var
   sCodigo, sDescripcion: String;
begin
     with GR_PADRE do
     begin
          Enabled := ( dmSistema.cdsGrupos.FieldByName( 'GR_CODIGO' ).Asinteger <> dmCliente.GetGrupoActivo );
          if not Enabled then
          begin
               dmSistema.ObtieneDescripcionPadre( dmSistema.cdsGrupos.FieldByName( 'GR_PADRE' ).Asinteger, sCodigo, sDescripcion );
               SetLlaveDescripcion( sCodigo, sDescripcion );
          end;
          SUBORDINADOlbl.Enabled := Enabled;
     end;
end;

procedure TSistEditGrupos.HabilitaControles;
begin
     inherited HabilitaControles;
     Accesos.Enabled := not Editing;
end;

procedure TSistEditGrupos.WMExaminar(var Message: TMessage);
begin
     Accesos.Click;
end;

procedure TSistEditGrupos.AccesosClick(Sender: TObject);
begin
     if not Inserting then
     begin
          if ( DataSource.Dataset.FieldByName( 'GR_CODIGO' ).AsInteger <> D_GRUPO_SIN_RESTRICCION ) then
          begin
               if ( ZAccesosMgr.CheckDerecho( D_SIST_DATOS_GRUPOS, K_ASIGNA_DERECHOS_OTROS_GRUPOS )) and
                  ( dmCliente.GetGrupoActivo <> DataSource.Dataset.FieldByName( 'GR_CODIGO' ).AsInteger ) then
               begin
                    DBaseSistema.CambiarAccesos;
               end
               else
               begin
                    if ( ZAccesosMgr.CheckDerecho( D_SIST_DATOS_GRUPOS, K_ASIGNA_DERECHOS_GRUPO_PROPIO )) and
                       ( dmCliente.GetGrupoActivo = DataSource.Dataset.FieldByName( 'GR_CODIGO' ).AsInteger ) then
                    begin
                         DBaseSistema.CambiarAccesos;
                    end
                    else
                    begin
                         ZetaDialogo.zInformation( Self.Caption, '� No tiene Permiso para Modificar Accesos !', 0 );
                    end;
               end;
          end
          else
          begin
               ZetaDialogo.zInformation( Self.Caption, '� El Grupo 1 Tiene Todos Los Derechos: No Se Permite Cambiarle Accesos !', 0 );
          end;
      end;
end;

procedure TSistEditGrupos.DBNavigatorClick(Sender: TObject; Button: TNavigateBtn);
begin
     inherited;
     ActualizaEmpresas;
//     ChecaGrupoActual;
end;

procedure TSistEditGrupos.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = nil ) then
        ChecaGrupoActual;
end;

end.
