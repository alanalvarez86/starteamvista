unit FSistEditPoll_devEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Buttons, DBCtrls, StdCtrls, Mask,
     ZetaHora,
     ZetaFecha,
     ZetaNumero, ZetaSmartLists, ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TSistEditPoll_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    PO_EMPRESA: TDBEdit;
    Label2: TLabel;
    PO_NUMERO: TZetaDBNumero;
    Label3: TLabel;
    PO_LETRA: TDBEdit;
    Label4: TLabel;
    PO_FECHA: TZetaDBFecha;
    Label5: TLabel;
    PO_HORA: TZetaDBHora;
    Label6: TLabel;
    PO_LINX: TDBEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  SistEditPoll_DevEx: TSistEditPoll_DevEx;

implementation

uses DSistema,
     ZAccesosTress,
     ZetaCommonClasses;

{$R *.DFM}

procedure TSistEditPoll_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_SIST_DATOS_POLL_PENDIENTES;
     HelpContext := H80814_Poll_pendientes;
     FirstControl := PO_EMPRESA;
end;

procedure TSistEditPoll_DevEx.Connect;
begin
     with dmSistema do
     begin
          Datasource.Dataset := cdsPoll;
     end;
end;

end.
