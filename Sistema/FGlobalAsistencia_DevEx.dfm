inherited GlobalAsistencia_DevEx: TGlobalAsistencia_DevEx
  Left = 318
  Top = 247
  Caption = 'Asistencia'
  ClientHeight = 484
  ClientWidth = 572
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 448
    Width = 572
    TabOrder = 1
    inherited OK_DevEx: TcxButton
      Left = 406
      Top = 5
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 486
      Top = 5
    end
  end
  object PageControl: TcxPageControl [1]
    Left = 0
    Top = 0
    Width = 572
    Height = 448
    Align = alClient
    TabOrder = 0
    Properties.ActivePage = Generales
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 446
    ClientRectLeft = 2
    ClientRectRight = 570
    ClientRectTop = 27
    object Generales: TcxTabSheet
      Caption = 'Generales'
      object Configuracion: TGroupBox
        Left = 0
        Top = 0
        Width = 568
        Height = 183
        Align = alTop
        Caption = ' Configuraci'#243'n '
        Color = clWhite
        ParentColor = False
        TabOrder = 0
        object PrimerDiaLBL: TLabel
          Left = 94
          Top = 20
          Width = 117
          Height = 13
          Alignment = taRightJustify
          Caption = '&Primer d'#237'a de la semana:'
          FocusControl = PrimerDia
        end
        object GraciaRepetidasLBL: TLabel
          Left = 81
          Top = 46
          Width = 130
          Height = 13
          Alignment = taRightJustify
          Caption = '&Gracia checadas repetidas:'
          FocusControl = GraciaRepetidas
        end
        object PrimerDia: TZetaKeyCombo
          Left = 214
          Top = 16
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 1
          ListaFija = lfDiasSemana
          ListaVariable = lvPuesto
          Offset = 1
          Opcional = False
          EsconderVacios = False
        end
        object SumarJornadas: TCheckBox
          Left = 72
          Top = 86
          Width = 155
          Height = 15
          Alignment = taLeftJustify
          Caption = 'Sumar &Jornadas de Horarios:'
          TabOrder = 0
        end
        object GraciaRepetidas: TZetaNumero
          Left = 214
          Top = 42
          Width = 49
          Height = 21
          Mascara = mnMinutos
          TabOrder = 2
          Text = '0'
        end
        object NoChecan: TCheckBox
          Left = 24
          Top = 84
          Width = 204
          Height = 17
          Alignment = taLeftJustify
          Caption = 'E&xtras para empleados que no checan:'
          TabOrder = 4
        end
        object NoChecanUsarJornadas: TCheckBox
          Left = 10
          Top = 101
          Width = 218
          Height = 15
          Alignment = taLeftJustify
          Caption = 'Jornadas para empleados que no c&hecan:'
          TabOrder = 5
        end
        object AutDesctoComidas: TCheckBox
          Left = 55
          Top = 118
          Width = 173
          Height = 15
          Alignment = taLeftJustify
          Caption = '&Autorizar descuento de comidas:'
          TabOrder = 6
        end
        object BloqueoPreNominas: TCheckBox
          Left = 45
          Top = 135
          Width = 183
          Height = 15
          Alignment = taLeftJustify
          Caption = '&Bloquear pren'#243'minas autorizadas:'
          TabOrder = 7
        end
        object CredencialesInvalidas: TCheckBox
          Left = 57
          Top = 68
          Width = 171
          Height = 15
          Alignment = taLeftJustify
          Caption = 'Procesar credenciales in&v'#225'lidas:'
          TabOrder = 3
        end
        object CBSalarioTemporal: TCheckBox
          Left = 13
          Top = 152
          Width = 215
          Height = 15
          Alignment = taLeftJustify
          Caption = 'Usar salario x tabulador de clasific. temp:'
          TabOrder = 8
        end
      end
      object Redondeos: TGroupBox
        Left = 0
        Top = 333
        Width = 568
        Height = 78
        Align = alTop
        Caption = ' Tablas para redondeo '
        Color = clWhite
        ParentColor = False
        TabOrder = 3
        object RedondeoExtrasLBL: TLabel
          Left = 149
          Top = 44
          Width = 62
          Height = 13
          Alignment = taRightJustify
          Caption = 'Horas &extras:'
          FocusControl = RedondeoExtras
        end
        object RedondeoOrdinariasLBL: TLabel
          Left = 132
          Top = 20
          Width = 79
          Height = 13
          Alignment = taRightJustify
          Caption = '&Horas ordinarias:'
          FocusControl = RedondeoOrdinarias
        end
        object RedondeoOrdinarias: TZetaKeyLookup_DevEx
          Left = 214
          Top = 16
          Width = 322
          Height = 21
          LookupDataset = dmTablas.cdsNumericas
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
        end
        object RedondeoExtras: TZetaKeyLookup_DevEx
          Left = 214
          Top = 40
          Width = 322
          Height = 21
          LookupDataset = dmTablas.cdsNumericas
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 60
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 183
        Width = 568
        Height = 62
        Align = alTop
        Caption = 'IMSS'
        Color = clWhite
        ParentColor = False
        TabOrder = 1
        object Regla3x3lbl: TLabel
          Left = 30
          Top = 22
          Width = 181
          Height = 13
          Alignment = taRightJustify
          Caption = 'Horas exentas para IMSS (Regla &3x3):'
          FocusControl = Regla3x3
        end
        object Label2: TLabel
          Left = 404
          Top = 22
          Width = 26
          Height = 13
          Alignment = taRightJustify
          Caption = '&D'#237'as:'
        end
        object Regla3x3: TZetaKeyCombo
          Left = 214
          Top = 18
          Width = 171
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 0
          ListaFija = lfRegla3x3
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object DiasExento: TZetaKeyCombo
          Left = 436
          Top = 18
          Width = 116
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 1
          ListaFija = lfDiasAplicaExento
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 245
        Width = 568
        Height = 88
        Align = alTop
        Caption = 'Vacaciones'
        Color = clWhite
        ParentColor = False
        TabOrder = 2
        object Label3: TLabel
          Left = 233
          Top = 20
          Width = 158
          Height = 13
          Alignment = taRightJustify
          Caption = 'Aplicar pago con an&ticipaci'#243'n de:'
          FocusControl = NumPerAnticipado
        end
        object Label4: TLabel
          Left = 433
          Top = 20
          Width = 98
          Height = 13
          Alignment = taRightJustify
          Caption = 'periodo(s) de n'#243'mina'
          FocusControl = NumPerAnticipado
        end
        object AplicarPlanVaca: TCheckBox
          Left = 77
          Top = 38
          Width = 151
          Height = 15
          Alignment = taLeftJustify
          Caption = 'Aplicar p&lan de vacaciones:'
          TabOrder = 0
        end
        object PagoPrimaVacacional: TCheckBox
          Left = 13
          Top = 56
          Width = 215
          Height = 15
          Alignment = taLeftJustify
          Caption = 'Pago de prima &vacacional en aniversario:'
          TabOrder = 1
        end
        object CBDiasSinFaltasVac: TCheckBox
          Left = 19
          Top = 20
          Width = 209
          Height = 15
          Alignment = taLeftJustify
          Caption = 'Calcular d'#237'as cotizados sin faltas x vac.:'
          TabOrder = 2
        end
        object NumPerAnticipado: TZetaNumero
          Left = 395
          Top = 18
          Width = 32
          Height = 21
          Mascara = mnDias
          TabOrder = 3
          Text = '0'
        end
      end
    end
    object ExtrasYFestivos: TcxTabSheet
      Caption = 'Extras y festivos'
      object TratamientoHorasExtras: TGroupBox
        Left = 0
        Top = 0
        Width = 568
        Height = 159
        Align = alTop
        Caption = ' Tratamiento de horas extras '
        Color = clWhite
        ParentColor = False
        TabOrder = 0
        object HorasSabadosLBL: TLabel
          Left = 122
          Top = 20
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = '&S'#225'bados:'
          FocusControl = HorasSabados
        end
        object HorasDescansosLBL: TLabel
          Left = 111
          Top = 44
          Width = 56
          Height = 13
          Alignment = taRightJustify
          Caption = '&Descansos:'
          FocusControl = HorasDescansos
        end
        object HorasFestivosLBL: TLabel
          Left = 125
          Top = 68
          Width = 42
          Height = 13
          Alignment = taRightJustify
          Caption = '&Festivos:'
          FocusControl = HorasFestivos
        end
        object TratamientoTELBL: TLabel
          Left = 33
          Top = 110
          Width = 134
          Height = 13
          Alignment = taRightJustify
          Caption = 'T&ratamiento de tiempo extra:'
          FocusControl = TratamientoTE
        end
        object HorasSabados: TZetaKeyCombo
          Left = 169
          Top = 16
          Width = 190
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 0
          ListaFija = lfHorasExtras
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object HorasDescansos: TZetaKeyCombo
          Left = 169
          Top = 40
          Width = 190
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 1
          ListaFija = lfHorasExtras
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object HorasFestivos: TZetaKeyCombo
          Left = 169
          Top = 64
          Width = 190
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 2
          ListaFija = lfHorasExtras
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object TopeDoblesSemanal: TCheckBox
          Left = 19
          Top = 87
          Width = 163
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Tope de dobles corte semanal:'
          TabOrder = 3
        end
        object TratamientoTE: TZetaKeyCombo
          Left = 169
          Top = 106
          Width = 190
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 4
          ListaFija = lfTratamientoExtras
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object IncidenciaFestivoDescansos: TCheckBox
          Left = 10
          Top = 129
          Width = 172
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Incidencia festivo en descansos:'
          TabOrder = 5
        end
      end
      object CapturaObligatoriadeMotivo: TGroupBox
        Left = 0
        Top = 287
        Width = 568
        Height = 71
        Align = alTop
        Caption = ' Captura obligatoria del motivo de autorizaci'#243'n de: '
        Color = clWhite
        ParentColor = False
        TabOrder = 3
        object HrsExtras: TCheckBox
          Left = 62
          Top = 17
          Width = 82
          Height = 17
          Alignment = taLeftJustify
          Caption = '&Horas extras:'
          TabOrder = 0
        end
        object DescansoTrabajado: TCheckBox
          Left = 26
          Top = 36
          Width = 118
          Height = 17
          Alignment = taLeftJustify
          Caption = 'D&escanso trabajado:'
          TabOrder = 1
        end
        object PermisoCGoce: TCheckBox
          Left = 230
          Top = 17
          Width = 109
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Permiso con &goce:'
          TabOrder = 2
        end
        object PermisoCGentrada: TCheckBox
          Left = 176
          Top = 36
          Width = 163
          Height = 17
          Alignment = taLeftJustify
          Caption = '&Permiso con goce de entrada:'
          TabOrder = 3
        end
        object PermisoSGoce: TCheckBox
          Left = 423
          Top = 17
          Width = 105
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Permiso sin g&oce:'
          TabOrder = 4
        end
        object PermisoSGEntrada: TCheckBox
          Left = 369
          Top = 36
          Width = 159
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Permiso sin goce de e&ntrada:'
          TabOrder = 5
        end
      end
      object AutorizacionExtrasYDescansos: TGroupBox
        Left = 0
        Top = 217
        Width = 568
        Height = 70
        Align = alTop
        Caption = 'Se puede autorizar como extras o descanso trabajado: '
        Color = clWhite
        ParentColor = False
        TabOrder = 2
        object ExtraDescansoSab: TCheckBox
          Left = 120
          Top = 32
          Width = 62
          Height = 17
          Alignment = taLeftJustify
          Caption = 'S'#225'bados:'
          TabOrder = 1
        end
        object ExtraDescansoDes: TCheckBox
          Left = 109
          Top = 48
          Width = 73
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Descansos:'
          TabOrder = 2
        end
        object ExtraDescansoFes: TCheckBox
          Left = 123
          Top = 16
          Width = 59
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Festivos:'
          TabOrder = 0
        end
      end
      object MotivodeCapturaManualChecadas: TGroupBox
        Left = 0
        Top = 358
        Width = 345
        Height = 61
        Align = alLeft
        Caption = 'Motivo de captura manual de checadas: '
        Color = clWhite
        ParentColor = False
        TabOrder = 4
        object PermitirCaptura: TCheckBox
          Left = 47
          Top = 22
          Width = 97
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Per&mitir captura:'
          TabOrder = 0
          OnClick = PermitirCapturaClick
        end
        object CapturaObligatoria: TCheckBox
          Left = 32
          Top = 41
          Width = 112
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Captura o&bligatoria:'
          TabOrder = 1
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 159
        Width = 568
        Height = 58
        Align = alTop
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 1
        object AutorizacionPago: TGroupBox
          Left = 0
          Top = 0
          Width = 281
          Height = 58
          Align = alLeft
          Caption = ' Requiere autorizaci'#243'n para pago '
          Color = clWhite
          ParentColor = False
          TabOrder = 0
          object HorasExtras: TCheckBox
            Left = 103
            Top = 17
            Width = 79
            Height = 17
            Alignment = taLeftJustify
            Caption = 'Horas e&xtras:'
            TabOrder = 0
            OnClick = HorasExtrasClick
          end
          object DiasFestivos: TCheckBox
            Left = 71
            Top = 35
            Width = 111
            Height = 17
            Alignment = taLeftJustify
            Caption = 'Festivos &trabajados:'
            TabOrder = 1
            OnClick = DiasFestivosClick
          end
        end
        object GroupBox3: TGroupBox
          Left = 281
          Top = 0
          Width = 299
          Height = 58
          Align = alLeft
          Caption = 'Requiere autorizaci'#243'n de aprobaciones para pago'
          TabOrder = 1
          object AprobarAutorizacion: TCheckBox
            Left = 68
            Top = 25
            Width = 132
            Height = 15
            Alignment = taLeftJustify
            Caption = 'Aprobar a&utorizaciones:'
            TabOrder = 0
          end
        end
      end
    end
    object Tarjetas: TcxTabSheet
      Caption = 'Tarjetas'
      ImageIndex = 2
      object ProximidadGB: TGroupBox
        Left = 0
        Top = 0
        Width = 568
        Height = 88
        Align = alTop
        Caption = ' Tarjetas de proximidad '
        Color = clWhite
        ParentColor = False
        TabOrder = 0
        object CodeSizeLBL: TLabel
          Left = 61
          Top = 27
          Width = 96
          Height = 13
          Alignment = taRightJustify
          Caption = 'Longitud del c'#243'digo:'
        end
        object FacilityCodeLBL: TLabel
          Left = 50
          Top = 51
          Width = 107
          Height = 13
          Alignment = taRightJustify
          Caption = 'Prefijo ( Facility Code ):'
        end
        object Label1: TLabel
          Left = 286
          Top = 51
          Width = 73
          Height = 13
          Caption = '( Hexadecimal )'
        end
        object CodeSize: TZetaNumero
          Left = 160
          Top = 24
          Width = 41
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
        end
        object FacilityCode: TEdit
          Left = 160
          Top = 48
          Width = 121
          Height = 21
          TabOrder = 1
        end
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 6029718
  end
end
