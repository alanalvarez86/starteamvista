inherited SistGrupos: TSistGrupos
  Left = 371
  Top = 269
  Caption = 'Grupos de Usuarios'
  ClientHeight = 212
  ClientWidth = 397
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 397
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 71
    end
  end
  object ZetaDBGrid: TZetaDBGrid [1]
    Left = 0
    Top = 49
    Width = 397
    Height = 163
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDrawColumnCell = ZetaDBGridDrawColumnCell
    Columns = <
      item
        Expanded = False
        FieldName = 'gr_codigo'
        Title.Caption = 'N�mero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'gr_descrip'
        Title.Caption = 'Nombre'
        Width = 308
        Visible = True
      end>
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 19
    Width = 397
    Height = 30
    Align = alTop
    TabOrder = 2
    object Grupos: TBitBtn
      Left = 282
      Top = 2
      Width = 111
      Height = 26
      Hint = 'Ver Mapa General de Grupos de Usuarios'
      Anchors = [akRight, akBottom]
      Caption = '&Mapa de Grupos'
      TabOrder = 0
      TabStop = False
      OnClick = GruposClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000333300
        0000377777F3337777770FFFF099990FFFF07FFFF7FFFF7FFFF7000000999900
        00007777777777777777307703399330770337FF7F37F337FF7F300003399330
        000337777337F337777333333339933333333FFFFFF7F33FFFFF000000399300
        0000777777F7F37777770FFFF099990FFFF07FFFF7F7FF7FFFF7000000999900
        00007777777777777777307703399330770337FF7F37F337FF7F300003399330
        0003377773F7FFF77773333330000003333333333777777F3333333330FFFF03
        3333333337FFFF7F333333333000000333333333377777733333333333077033
        33333333337FF7F3333333333300003333333333337777333333}
      NumGlyphs = 2
    end
  end
  inherited DataSource: TDataSource
    Left = 8
    Top = 152
  end
end
