unit FSistNivel0_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db,  ExtCtrls, Grids, DBGrids,
     ZBaseGridLectura_DevEx,
     ZetaDBGrid, StdCtrls, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TNivel0_DevEx = class(TBaseGridLectura_DevEx)
    TB_CODIGO: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  Nivel0_DevEx: TNivel0_DevEx;

implementation

uses DSistema,
     ZetaCommonClasses;

{$R *.DFM}

procedure TNivel0_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H80818_Confidencialidad;
end;

procedure TNivel0_DevEx.Connect;
begin
     with dmSistema do
     begin
          cdsNivel0.Conectar;
          DataSource.DataSet := cdsNivel0;
     end;
end;

procedure TNivel0_DevEx.Refresh;
begin
     dmSistema.cdsNivel0.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TNivel0_DevEx.Agregar;
begin
     dmSistema.cdsNivel0.Agregar;
end;

procedure TNivel0_DevEx.Borrar;
begin
     dmSistema.cdsNivel0.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TNivel0_DevEx.Modificar;
begin
     dmSistema.cdsNivel0.Modificar;
end;

procedure TNivel0_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  {***Banda Sumatoria***}
  CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  inherited;

end;

end.
