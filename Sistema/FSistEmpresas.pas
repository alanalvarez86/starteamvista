unit FSistEmpresas;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids,
     ZBaseConsulta,
     ZetaDBGrid;

type
  TSistEmpresas = class(TBaseConsulta)
    ZetaDBGrid: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  SistEmpresas: TSistEmpresas;

implementation

{$R *.DFM}

uses DSistema,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZAccesosTress;

{ TSistEmpresas }

procedure TSistEmpresas.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_CONS_EMPRESAS;
     {$else}
     HelpContext := H80811_Empresas;
     {$endif}

     IndexDerechos := D_SIST_DATOS_EMPRESAS;          
end;

procedure TSistEmpresas.Connect;
begin
     with dmSistema do
     begin
          cdsEmpresas.Conectar;
          DataSource.DataSet:= cdsEmpresas;
     end;
end;

procedure TSistEmpresas.Refresh;
begin
     dmSistema.cdsEmpresas.Refrescar;
end;

procedure TSistEmpresas.Agregar;
begin
// {$ifdef ANTES}
     dmSistema.cdsEmpresas.Agregar;
// {$endif}
end;

procedure TSistEmpresas.Borrar;
begin
// {$ifdef ANTES}
     dmSistema.cdsEmpresas.Borrar;
// {$endif}
end;

procedure TSistEmpresas.Modificar;
begin
     dmSistema.cdsEmpresas.Modificar;
end;

procedure TSistEmpresas.FormShow(Sender: TObject);
begin
     inherited;

     {$ifdef SELECCION}
     ZetaDBGrid.Columns[3].Visible := FALSE;
     ZetaDBGrid.Columns[4].Visible := FALSE;
     ZetaDBGrid.Columns[5].Visible := FALSE;
     {$endif}

     {$ifdef VISITANTES}
     ZetaDBGrid.Columns[3].Visible := FALSE;
     ZetaDBGrid.Columns[4].Visible := FALSE;
     ZetaDBGrid.Columns[5].Visible := FALSE;
     {$endif}
end;

end.

