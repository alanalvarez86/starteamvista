unit FSistPOLL_DevEx;

{  Prop�sito  : Pantalla de Consulta de POLL Pendientes
   Creaci�n   : 11/Nov/1997
   Responsable: Gustavo
   Calculados : No
   Lista Calculados[] }

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids,
     ZBaseConsulta,
     ZetaDBGrid, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TSistPOLL_DevEx = class(TBaseGridLectura_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ConfigAgrupamiento;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  SistPOLL_DevEx: TSistPOLL_DevEx;

implementation

{$R *.DFM}

uses DSistema,
     ZetaCommonLists,
     ZetaCommonClasses;

{ ********** TSistPOLL *********** }

procedure TSistPOLL_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H80814_Poll_pendientes;
end;

procedure TSistPOLL_DevEx.Connect;
begin
     with dmSistema do
     begin
          cdsPoll.Conectar;
          DataSource.DataSet:= cdsPoll;
     end;
end;

procedure TSistPOLL_DevEx.Refresh;
begin
     dmSistema.cdsPoll.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TSistPOLL_DevEx.Agregar;
begin
     dmSistema.cdsPoll.Agregar;
end;

procedure TSistPOLL_DevEx.Borrar;
begin
     dmSistema.cdsPoll.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TSistPOLL_DevEx.Modificar;
begin
     dmSistema.cdsPoll.Modificar;
end;

procedure TSistPOLL_DevEx.ConfigAgrupamiento;
Const
K_PONUMERO =1;
K_POHORA =4;
begin
     //Cambio de configuracion
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Esconde la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     ZetaDBGridDBTableView.Columns[K_PONUMERO].Options.Grouping := FALSE;
     ZetaDBGridDBTableView.Columns[K_POHORA].Options.Grouping := FALSE;
end;

procedure TSistPOLL_DevEx.FormShow(Sender: TObject);
begin
   ApplyMinWidth;
     {***Banda Sumatoria***}
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  inherited;
     ConfigAgrupamiento;
end;

end.

