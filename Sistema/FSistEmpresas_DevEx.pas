unit FSistEmpresas_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids,
     StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
     TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
     cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
     cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
     Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
     cxGrid, ZetaCXGrid;

type
  TSistEmpresas_DevEx = class(TBaseGridLectura_DevEx)
    //ZetaDBGridDBTableView: TZetaDBGrid;
    CM_CODIGO: TcxGridDBColumn;
    CM_NOMBRE: TcxGridDBColumn;
    CM_CTRL_RL: TcxGridDBColumn;
    CM_DIGITO: TcxGridDBColumn;
    CM_USACAFE: TcxGridDBColumn;
    CM_USACASE: TcxGridDBColumn;
    CM_DATOS: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  SistEmpresas_DevEx: TSistEmpresas_DevEx;

implementation

{$R *.DFM}

uses DSistema,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZAccesosTress;

{ TSistEmpresas }

procedure TSistEmpresas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_CONS_EMPRESAS;
     {$else}
     HelpContext := H80811_Empresas;
     {$endif}

     IndexDerechos := D_SIST_DATOS_EMPRESAS;          
end;

procedure TSistEmpresas_DevEx.Connect;
begin
     with dmSistema do
     begin
          cdsEmpresas.Conectar;
          DataSource.DataSet:= cdsEmpresas;
     end;
end;

procedure TSistEmpresas_DevEx.Refresh;
begin
     dmSistema.cdsEmpresas.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TSistEmpresas_DevEx.Agregar;
begin
// {$ifdef ANTES}
     dmSistema.cdsEmpresas.Agregar;
// {$endif}
end;

procedure TSistEmpresas_DevEx.Borrar;
begin
// {$ifdef ANTES}
     dmSistema.cdsEmpresas.Borrar;
// {$endif}
   ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TSistEmpresas_DevEx.Modificar;
begin
     dmSistema.cdsEmpresas.Modificar;
end;

procedure TSistEmpresas_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;

     {$ifdef SELECCION}
    // ZetaDBGrid.Columns[3].Visible := FALSE;
     //ZetaDBGrid.Columns[4].Visible := FALSE;
    // ZetaDBGrid.Columns[5].Visible := FALSE;
     {$endif}

     {$ifdef VISITANTES}
    // ZetaDBGrid.Columns[3].Visible := FALSE;
     //ZetaDBGrid.Columns[4].Visible := FALSE;
     //ZetaDBGrid.Columns[5].Visible := FALSE;
     {$endif}

     {$ifdef SELECCION}
     ZetaDBGridDBTableView.Columns[3].Visible := FALSE;
     ZetaDBGridDBTableView.Columns[4].Visible := FALSE;
     ZetaDBGridDBTableView.Columns[5].Visible := FALSE;
     {$endif}

     {$ifdef VISITANTES}
     ZetaDBGridDBTableView.Columns[3].Visible := FALSE;
     ZetaDBGridDBTableView.Columns[4].Visible := FALSE;
     ZetaDBGridDBTableView.Columns[5].Visible := FALSE;
     {$endif}

end;

end.

