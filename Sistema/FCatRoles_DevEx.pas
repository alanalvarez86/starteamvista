unit FCatRoles_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids, Buttons, StdCtrls,
     ZBaseConsulta,
     ZetaDBGrid, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TCatRoles_DevEx = class(TBaseGridLectura_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  CatRoles_DevEx: TCatRoles_DevEx;

implementation

{$R *.DFM}

uses ZAccesosTress,
     ZetaCommonTools,
     ZetaCommonClasses, dSistema;

{ *********** TSistUsuarios *********** }

procedure TCatRoles_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_CAT_ROLES;
     IndexDerechos := D_CAT_ROLES;
     {$ifdef WORKFLOWCFG}
     HelpContext := H_WORKFLOWCFG_CATALOGOS_ROLES;
     {$endif}
end;

procedure TCatRoles_DevEx.Connect;
begin
     with dmSistema do
     begin
          cdsRoles.Conectar;
          DataSource.DataSet:= cdsRoles;
     end;
end;

procedure TCatRoles_DevEx.Refresh;
begin
     dmSistema.cdsRoles.Refrescar;
end;

procedure TCatRoles_DevEx.Agregar;
begin
     dmSistema.cdsRoles.Agregar;
end;

procedure TCatRoles_DevEx.Borrar;
begin
     dmSistema.cdsRoles.Borrar;
end;

procedure TCatRoles_DevEx.Modificar;
begin
     dmSistema.cdsRoles.Modificar;
end;

procedure TCatRoles_DevEx.FormShow(Sender: TObject);
begin
  CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0],0, '' , SkCount );
  inherited;

end;

end.


