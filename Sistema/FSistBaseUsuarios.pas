unit FSistBaseUsuarios;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids,
     ZBaseConsulta,
     ZetaDBGrid, Buttons, StdCtrls, ZetaKeyCombo;

type
  TSistBaseUsuarios = class(TBaseConsulta)
    ZetaDBGrid: TZetaDBGrid;
    Panel1: TPanel;
    Label1: TLabel;
    EBuscaNombre: TEdit;
    BBusca: TSpeedButton;
    Label2: TLabel;
    FiltroActivos: TZetaKeyCombo;
    lblGrupo: TLabel;
    FiltroGrupo: TZetaKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure EBuscaNombreChange(Sender: TObject);
    procedure BBuscaClick(Sender: TObject);
    procedure FiltroActivosChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FieldGroupChange(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    FFiltroNombres : string;
    FFiltroActivos : String;
    FFiltroGrupos : String;
    procedure FiltraUsuario;
    procedure FiltraActivo;
    procedure SetFiltros;
    procedure LimpiaGrid;
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  SistBaseUsuarios: TSistBaseUsuarios;

const
     K_TODOS_ITEMS = '<Todos>';

implementation

{$R *.DFM}

uses DSistema,
     ZetaCommonTools,
     ZetaCommonClasses, DCliente, ZetaDialogo;

{ *********** TSistUsuarios *********** }

procedure TSistBaseUsuarios.FormCreate(Sender: TObject);
     procedure LlenaCombo;
     begin
          dmSistema.LeeTodosGrupos( FiltroGrupo );
          with FiltroGrupo do
          begin
               Items.Add( K_TODOS_ITEMS );
               ItemIndex := 0;
          end;

     end;
begin
     inherited;
     HelpContext := H80813_Usuarios;
     FiltroActivos.ItemIndex := 0;
     FFiltroNombres := VACIO;
     FFiltroActivos := 'US_ACTIVO = ''S'' ';
     LlenaCombo;
end;

procedure TSistBaseUsuarios.Connect;
begin
     with dmSistema do
     begin
          cdsGrupos.Conectar;
          cdsUsuarios.Conectar;
          DataSource.DataSet:= cdsUsuarios;
     end;
end;

procedure TSistBaseUsuarios.Refresh;
begin
     dmSistema.cdsUsuarios.Refrescar;
end;

procedure TSistBaseUsuarios.ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     inherited;
     with ZetaDBGrid do
     begin
          if ( gdSelected in State )  then
          begin
               with Canvas do
               begin
                    Font.Color := clWhite;
                    Brush.Color := clNavy;
               end;
          end
          else
          begin
               with Canvas do
               begin
                    Brush.Color := clWhite;
                    if zStrToBool( DataSource.Dataset.FieldByName( 'US_DENTRO' ).AsString ) then
                       Font.Color := clRed
                    else
                        Font.Color := ZetaDBGrid.Font.Color;
               end;
          end;
          DefaultDrawDataCell( Rect, Column.Field, State );
     end;
end;

procedure TSistBaseUsuarios.Agregar;
begin
     dmSistema.cdsUsuarios.Agregar;
end;

procedure TSistBaseUsuarios.Borrar;
begin
   dmSistema.cdsUsuarios.Borrar;
end;

procedure TSistBaseUsuarios.Modificar;
begin
     dmSistema.cdsUsuarios.Modificar;
end;

procedure TSistBaseUsuarios.BBuscaClick(Sender: TObject);
begin
     inherited;
     FiltraUsuario;
end;

procedure TSistBaseUsuarios.LimpiaGrid;
begin
     ZetadbGrid.SelectedRows.Clear;
end;

procedure TSistBaseUsuarios.EBuscaNombreChange(Sender: TObject);
begin
     inherited;
     BBusca.Down := Length(EBuscaNombre.Text)>0;
     FiltraUsuario;
end;

procedure TSistBaseUsuarios.FiltroActivosChange(Sender: TObject);
begin
     inherited;
     FiltraActivo;
end;

procedure TSistBaseUsuarios.FiltraUsuario;
begin
     if BBusca.Down then
     begin
          FFiltroNombres := 'UPPER(US_NOMBRE) LIKE '+ chr(39) + '%'+ UpperCase(EBuscaNombre.Text) + '%' + chr(39);
     end
     else
     begin
          FFiltroNombres := VACIO;
     end;
     SetFiltros;
     LimpiaGrid;
end;

procedure TSistBaseUsuarios.FiltraActivo;
begin
     case FiltroActivos.ItemIndex of
          0: FFiltroActivos := 'US_ACTIVO = ''S'' ';
          1: FFiltroActivos := 'US_ACTIVO = ''N'' ';
     else
         FFiltroActivos := VACIO;
     end;
     SetFiltros;
     LimpiaGrid;
end;

procedure TSistBaseUsuarios.SetFiltros;
begin
     with dmSistema.cdsUsuarios do
     begin
          Filter := ConcatFiltros( FFiltroNombres, FFiltroActivos );
          Filter := ConcatFiltros( Filter, FFiltroGrupos );
          Filtered := StrLleno( Filter );
     end;
end;

procedure TSistBaseUsuarios.FormShow(Sender: TObject);
begin
     inherited;
     SetFiltros;
end;

procedure TSistBaseUsuarios.FieldGroupChange(Sender: TObject);
begin
     inherited;
     with FiltroGrupo do
     begin
          if not ( Text = K_TODOS_ITEMS ) then
              FFiltroGrupos := 'GR_CODIGO = ''' + IntToStr( Integer( Items.Objects[ItemIndex] ) ) + ''''
          else
              FFiltroGrupos := VACIO;
     end;
     SetFiltros;
     LimpiaGrid;
end;

procedure TSistBaseUsuarios.FormResize(Sender: TObject);
begin
     inherited;
     FiltroGrupo.Width := iMin( 250, iMax( 125, self.width - 663 ) );
end;

end.
