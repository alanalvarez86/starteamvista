inherited GlobalRecursos_DevEx: TGlobalRecursos_DevEx
  Left = 2562
  Top = 271
  Caption = 'Recursos Humanos'
  ClientHeight = 484
  ClientWidth = 571
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 448
    Width = 571
    TabOrder = 1
    inherited OK_DevEx: TcxButton
      Left = 404
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 484
    end
  end
  object cxPageControl1: TcxPageControl [1]
    Left = 0
    Top = 0
    Width = 571
    Height = 448
    Align = alClient
    TabOrder = 0
    Properties.ActivePage = cxTabSheet1
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 444
    ClientRectLeft = 4
    ClientRectRight = 567
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Generales'
      ImageIndex = 0
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 563
        Height = 68
        Align = alTop
        Caption = 'IMSS'
        Color = clWhite
        ParentColor = False
        TabOrder = 0
        object Label1: TLabel
          Left = 128
          Top = 18
          Width = 109
          Height = 13
          Alignment = taRightJustify
          Caption = 'Numero de &gu'#237'a IMSS:'
          FocusControl = GuiaIMSS
        end
        object Label2: TLabel
          Left = 146
          Top = 42
          Width = 91
          Height = 13
          Alignment = taRightJustify
          Caption = '&Zonas geogr'#225'ficas:'
          FocusControl = ZonasGeograficas
        end
        object GuiaIMSS: TMaskEdit
          Tag = 11
          Left = 241
          Top = 12
          Width = 66
          Height = 21
          MaxLength = 5
          TabOrder = 0
        end
        object ZonasGeograficas: TEdit
          Tag = 44
          Left = 241
          Top = 40
          Width = 66
          Height = 21
          MaxLength = 3
          TabOrder = 1
        end
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 68
        Width = 563
        Height = 115
        Align = alTop
        Caption = 'Configuraci'#243'n'
        Color = clWhite
        ParentColor = False
        TabOrder = 1
        object lbPlantillas: TLabel
          Left = 148
          Top = 24
          Width = 89
          Height = 13
          Alignment = taRightJustify
          Caption = '&Directorio reportes:'
          FocusControl = Plantillas
        end
        object Label5: TLabel
          Left = 72
          Top = 47
          Width = 165
          Height = 13
          Alignment = taRightJustify
          Caption = 'Empresa de &selecci'#243'n de personal:'
          FocusControl = ZKEmpresaSeleccion
        end
        object Buscar: TcxButton
          Left = 530
          Top = 20
          Width = 21
          Height = 21
          Hint = 'Buscar Directorio'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BuscarClick
          OptionsImage.Glyph.Data = {
            DA050000424DDA05000000000000360000002800000013000000130000000100
            200000000000A40500000000000000000000000000000000000000B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFDFF5
            FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
            FCFFDFF5FCFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF10B7EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF40C5EFFF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF40C5EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6CD3
            F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF6CD3F2FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFF93DEF6FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF97E0F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFEBF9FDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF54CB
            F0FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CF
            F1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF80D9F4FF80D9
            F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC3EDFAFFDFF5FCFFDFF5FCFFDFF5
            FCFF70D4F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
        end
        object Plantillas: TEdit
          Tag = 61
          Left = 241
          Top = 20
          Width = 288
          Height = 21
          TabOrder = 0
        end
        object ZKEmpresaSeleccion: TZetaKeyLookup_DevEx
          Left = 241
          Top = 44
          Width = 312
          Height = 21
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 2
          TabStop = True
          WidthLlave = 75
        end
        object chkConservarTamFotos: TCheckBox
          Left = 42
          Top = 72
          Width = 212
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Conservar el tama'#241'o original de las fotos:'
          TabOrder = 3
        end
        object CkUsarValidacion: TCheckBox
          Left = 4
          Top = 90
          Width = 250
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Usar validaci'#243'n de estatus de tablas y cat'#225'logos:'
          TabOrder = 4
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 255
        Width = 563
        Height = 69
        Align = alTop
        Caption = ' Status empleado '
        Color = clWhite
        ParentColor = False
        TabOrder = 3
        object Label3: TLabel
          Left = 82
          Top = 40
          Width = 155
          Height = 13
          Alignment = taRightJustify
          Caption = '&F'#243'rmula para empleados activos:'
          FocusControl = FormulaEmpleadoActivo
        end
        object ckStatusAlDia: TCheckBox
          Left = 70
          Top = 16
          Width = 184
          Height = 17
          Alignment = taLeftJustify
          Caption = '&Mostrar status del empleado al d'#237'a:'
          TabOrder = 0
        end
        object FormulaEmpleadoActivo: TEdit
          Tag = 44
          Left = 241
          Top = 36
          Width = 310
          Height = 21
          MaxLength = 255
          TabOrder = 1
        end
      end
      object ckNoValidarBajasExt: TCheckBox
        Left = 81
        Top = 337
        Width = 173
        Height = 17
        Alignment = taLeftJustify
        Caption = 'No validar &bajas extempor'#225'neas:'
        Color = clWhite
        ParentColor = False
        TabOrder = 4
      end
      object CkValidarPrestamos: TCheckBox
        Left = 46
        Top = 357
        Width = 208
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Va&lidar pol'#237'ticas para otorgar pr'#233'stamos:'
        Color = clWhite
        ParentColor = False
        TabOrder = 5
      end
      object AddEmpSalTopado: TCheckBox
        Left = 22
        Top = 377
        Width = 232
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Actualizar factor integraci'#243'n salarios topados:'
        Color = clWhite
        ParentColor = False
        TabOrder = 6
      end
      object GroupBox4: TGroupBox
        Left = 0
        Top = 183
        Width = 563
        Height = 72
        Align = alTop
        Caption = 'Alta de empleados'
        Color = clWhite
        ParentColor = False
        TabOrder = 2
        object Label7: TLabel
          Left = 52
          Top = 41
          Width = 185
          Height = 13
          Alignment = taRightJustify
          Caption = 'Validar &repetidos en alta de empleados:'
        end
        object ckOrganigrama: TCheckBox
          Left = 32
          Top = 14
          Width = 222
          Height = 17
          Alignment = taLeftJustify
          Caption = '&Usar validaciones de organigrama (Plazas):'
          TabOrder = 0
        end
        object cbValidaRepetidos: TZetaKeyCombo
          Left = 241
          Top = 38
          Width = 161
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 1
          ListaFija = lfNinguna
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
      end
      object IncluirSentenciaSQL: TCheckBox
        Left = 60
        Top = 397
        Width = 194
        Height = 17
        Alignment = taLeftJustify
        Color = clWhite
        ParentColor = False
        TabOrder = 7
        Visible = False
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Incidencias'
      ImageIndex = 1
      object GroupBox6: TGroupBox
        Left = 0
        Top = 121
        Width = 567
        Height = 66
        Align = alTop
        Caption = 'Permisos'
        Color = clWhite
        ParentColor = False
        TabOrder = 1
        object PermisosDiasHabiles: TCheckBox
          Left = 73
          Top = 26
          Width = 180
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Capturar permiso con d'#237'as h'#225'biles:'
          TabOrder = 0
        end
      end
      object GroupBox7: TGroupBox
        Left = 0
        Top = 187
        Width = 567
        Height = 78
        Align = alTop
        Caption = 'Incapacidades'
        Color = clWhite
        ParentColor = False
        TabOrder = 2
        object LblIncapacidadeGeneral: TLabel
          Left = 73
          Top = 32
          Width = 166
          Height = 13
          Alignment = taRightJustify
          Caption = 'Incidencia de incapacidad general:'
        end
        object IncapacidadGeneral: TZetaKeyLookup_DevEx
          Left = 241
          Top = 28
          Width = 302
          Height = 21
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 75
        end
      end
      object GroupBox5: TGroupBox
        Left = 0
        Top = 0
        Width = 567
        Height = 121
        Align = alTop
        Caption = 'Vacaciones'
        Color = clWhite
        ParentColor = False
        TabOrder = 0
        object Label4: TLabel
          Left = 100
          Top = 18
          Width = 139
          Height = 13
          Alignment = taRightJustify
          Caption = 'Valor de d'#237'as en &vacaciones:'
          FocusControl = cbValorDias
        end
        object Label6: TLabel
          Left = 62
          Top = 90
          Width = 177
          Height = 13
          Alignment = taRightJustify
          Caption = '&Plan de vacaciones ( D'#237'as de pago ):'
          FocusControl = cbPlanVacPago
        end
        object Label8: TLabel
          Left = 105
          Top = 64
          Width = 134
          Height = 13
          Alignment = taRightJustify
          Caption = 'Vencimiento de vacaciones:'
        end
        object Label9: TLabel
          Left = 267
          Top = 94
          Width = 31
          Height = 13
          Alignment = taRightJustify
          Caption = 'Meses'
        end
        object cbValorDias: TZetaKeyCombo
          Left = 241
          Top = 14
          Width = 160
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 0
          ParentCtl3D = False
          TabOrder = 0
          ListaFija = lfValorDiasVacaciones
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object cbPlanVacPago: TZetaKeyCombo
          Left = 241
          Top = 86
          Width = 160
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 0
          ParentCtl3D = False
          TabOrder = 3
          ListaFija = lfPagoPlanVacacion
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object chkMostrarSaldoVacaciones: TCheckBox
          Left = 28
          Top = 39
          Width = 226
          Height = 18
          Alignment = taLeftJustify
          Caption = 'Mostrar saldo de vacaciones en expediente:'
          TabOrder = 1
        end
        object MesesVencimiento: TZetaNumero
          Tag = 43
          Left = 241
          Top = 60
          Width = 36
          Height = 21
          Mascara = mnMinutos
          TabOrder = 2
          Text = '0'
        end
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 3146020
  end
end
