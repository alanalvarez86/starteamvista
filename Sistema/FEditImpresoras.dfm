inherited EditImpresoras: TEditImpresoras
  Left = 221
  Top = 109
  Caption = 'Impresoras'
  ClientHeight = 363
  ClientWidth = 469
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 327
    Width = 469
    TabOrder = 2
    inherited OK: TBitBtn
      Left = 299
    end
    inherited Cancelar: TBitBtn
      Left = 382
    end
  end
  inherited PanelSuperior: TPanel
    Width = 469
    TabOrder = 3
  end
  inherited PanelIdentifica: TPanel
    Width = 469
    TabOrder = 4
    inherited ValorActivo2: TPanel
      Width = 143
    end
  end
  object PageControl: TPageControl [3]
    Left = 0
    Top = 92
    Width = 469
    Height = 235
    ActivePage = GeneralesTab
    Align = alClient
    TabOrder = 1
    object GeneralesTab: TTabSheet
      Caption = 'Generales'
      object Label2: TLabel
        Left = 35
        Top = 11
        Width = 70
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ancho Normal:'
      end
      object Label3: TLabel
        Left = 22
        Top = 33
        Width = 83
        Height = 13
        Alignment = taRightJustify
        Caption = 'Semi-Comprimido:'
      end
      object Label4: TLabel
        Left = 48
        Top = 55
        Width = 57
        Height = 13
        Alignment = taRightJustify
        Caption = 'Comprimido:'
      end
      object Label5: TLabel
        Left = 23
        Top = 77
        Width = 82
        Height = 13
        Alignment = taRightJustify
        Caption = 'Inicio Subrayado:'
      end
      object Label6: TLabel
        Left = 34
        Top = 99
        Width = 71
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fin Subrayado:'
      end
      object Label7: TLabel
        Left = 40
        Top = 121
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = 'Inicio Negrita:'
      end
      object Label8: TLabel
        Left = 51
        Top = 143
        Width = 54
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fin Negrita:'
      end
      object Label9: TLabel
        Left = 46
        Top = 165
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'Inicio Italica:'
      end
      object Label10: TLabel
        Left = 57
        Top = 187
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fin Italica:'
      end
      object PI_CHAR_10: TDBEdit
        Left = 110
        Top = 7
        Width = 300
        Height = 21
        DataField = 'PI_CHAR_10'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 0
      end
      object PI_CHAR_12: TDBEdit
        Left = 110
        Top = 29
        Width = 300
        Height = 21
        DataField = 'PI_CHAR_12'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 1
      end
      object PI_CHAR_17: TDBEdit
        Left = 110
        Top = 51
        Width = 300
        Height = 21
        DataField = 'PI_CHAR_17'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 2
      end
      object PI_UNDE_ON: TDBEdit
        Left = 110
        Top = 73
        Width = 300
        Height = 21
        DataField = 'PI_UNDE_ON'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 3
      end
      object PI_UNDE_OF: TDBEdit
        Left = 110
        Top = 95
        Width = 300
        Height = 21
        DataField = 'PI_UNDE_OF'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 4
      end
      object PI_BOLD_ON: TDBEdit
        Left = 110
        Top = 117
        Width = 300
        Height = 21
        DataField = 'PI_BOLD_ON'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 5
      end
      object PI_BOLD_OF: TDBEdit
        Left = 110
        Top = 139
        Width = 300
        Height = 21
        DataField = 'PI_BOLD_OF'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 6
      end
      object PI_ITAL_ON: TDBEdit
        Left = 110
        Top = 161
        Width = 300
        Height = 21
        DataField = 'PI_ITAL_ON'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 7
      end
      object PI_ITAL_OF: TDBEdit
        Left = 110
        Top = 183
        Width = 300
        Height = 21
        DataField = 'PI_ITAL_OF'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 8
      end
    end
    object EspecialesTab: TTabSheet
      Caption = 'Especiales'
      object Label11: TLabel
        Left = 127
        Top = 11
        Width = 27
        Height = 13
        Alignment = taRightJustify
        Caption = 'Eject:'
      end
      object Label12: TLabel
        Left = 59
        Top = 33
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Reset de Impresora:'
      end
      object Label13: TLabel
        Left = 3
        Top = 77
        Width = 151
        Height = 13
        Alignment = taRightJustify
        Caption = 'Imprimir 6 Renglones / Pulgada:'
      end
      object Label14: TLabel
        Left = 3
        Top = 99
        Width = 151
        Height = 13
        Alignment = taRightJustify
        Caption = 'Imprimir 8 Renglones / Pulgada:'
      end
      object Label15: TLabel
        Left = 50
        Top = 55
        Width = 104
        Height = 13
        Alignment = taRightJustify
        Caption = 'Impresión Landscape:'
      end
      object Label16: TLabel
        Left = 118
        Top = 121
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Extra 1:'
        Visible = False
      end
      object Label17: TLabel
        Left = 118
        Top = 143
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Extra 2:'
        Visible = False
      end
      object PI_EJECT: TDBEdit
        Left = 159
        Top = 7
        Width = 300
        Height = 21
        DataField = 'PI_EJECT'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 0
      end
      object PI_RESET: TDBEdit
        Left = 159
        Top = 29
        Width = 300
        Height = 21
        DataField = 'PI_RESET'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 1
      end
      object PI_6_LINES: TDBEdit
        Left = 159
        Top = 73
        Width = 300
        Height = 21
        DataField = 'PI_6_LINES'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 3
      end
      object PI_8_LINES: TDBEdit
        Left = 159
        Top = 95
        Width = 300
        Height = 21
        DataField = 'PI_8_LINES'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 4
      end
      object PI_LANDSCA: TDBEdit
        Left = 159
        Top = 51
        Width = 300
        Height = 21
        DataField = 'PI_LANDSCA'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 2
      end
      object PI_EXTRA_1: TDBEdit
        Left = 159
        Top = 117
        Width = 300
        Height = 21
        DataField = 'PI_EXTRA_1'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 5
        Visible = False
      end
      object PI_EXTRA_2: TDBEdit
        Left = 159
        Top = 139
        Width = 300
        Height = 21
        DataField = 'PI_EXTRA_2'
        DataSource = DataSource
        MaxLength = 50
        TabOrder = 6
        Visible = False
      end
    end
  end
  object Panel1: TPanel [4]
    Left = 0
    Top = 51
    Width = 469
    Height = 41
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 13
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nombre:'
    end
    object PI_NOMBRE: TZetaDBEdit
      Left = 61
      Top = 9
      Width = 348
      Height = 21
      TabOrder = 0
      ConfirmEdit = True
      DataField = 'PI_NOMBRE'
      DataSource = DataSource
    end
  end
end
