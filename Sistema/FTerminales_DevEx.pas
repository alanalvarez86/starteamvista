unit FTerminales_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseGridLectura_DevEx, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  StdCtrls, ImgList, ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  ZetaCXGrid, cxButtons, System.Actions, ZetaKeyCombo, cxTextEdit,
  cxImageComboBox;
  type
  TcxViewInfoAcess = class(TcxGridTableDataCellViewInfo);
  TcxPainterAccess = class(TcxGridTableDataCellPainter);

type
  TTerminales_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    TE_CODIGO: TcxGridDBColumn;
    TE_NOMBRE: TcxGridDBColumn;
    TE_APP: TcxGridDBColumn;
    btnImportarTerminales_DevEx: TcxButton;
    btnReiniciaTerminales_DevEx: TcxButton;
    TE_TER_HOR: TcxGridDBColumn;
    TE_VERSION: TcxGridDBColumn;
    TE_IP: TcxGridDBColumn;
    lblFiltro: TLabel;
    cmbFiltro: TZetaKeyCombo;
    TE_HUELLAS: TcxGridDBColumn;
    TE_ID: TcxGridDBColumn;
    DiagnosticoTerminales: TcxButton;
    IMAGEN_STATUS: TcxGridDBColumn;
    IconosList: TImageList;
    STATUS: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnImportarTerminales_DevExClick(Sender: TObject);
    procedure btnReiniciaTerminales_DevExClick(Sender: TObject);
    procedure cmbFiltroChange(Sender: TObject);
    procedure DiagnosticoTerminalesClick(Sender: TObject);
    procedure IMAGEN_STATUSCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    
  private
    { Private declarations }


  public
    { Public declarations }
    procedure DoLookup; override;


  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Refresh; override;
  end;


const
  K_INDEX_ACTIVAS = 1;
  K_INDEX_INACTIVAS = 2;

var
  Terminales_DevEx: TTerminales_DevEx;

implementation

uses dSistema,
     ZetaCommonClasses,
     ZAccesosTress,
     ZAccesosMgr,
     dProcesos,
     ZetaCommonLists,
     DateUtils,
     ZetaDialogo,
     ZetaCommonTools,
     ZetaBuscaEntero_DevEx;

{$R *.dfm}

{ TTerminales }

procedure TTerminales_DevEx.Agregar;
begin
     dmSistema.cdsTerminales.Agregar;
end;

procedure TTerminales_DevEx.Borrar;
begin
     inherited;
     dmSistema.cdsTerminales.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TTerminales_DevEx.Connect;
begin
     inherited;
     with dmSistema do
     begin
          FiltroTerminal := VACIO;
          cdsTerminales.Conectar;
          DataSource.DataSet := cdsTerminales;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();

end;



procedure TTerminales_DevEx.DiagnosticoTerminalesClick(Sender: TObject);
begin
  inherited;

  if ZConfirm( 'Diagn�stico de terminales', 'Esta acci�n aplicar� el mensaje para obtener diagn�stico de las terminales existentes.' + CR_LF + '�Desea continuar?', 0, mbYes ) then
     begin
          dmSistema.ReiniciaTerminales( -2 );
          ZInformation( 'Diagn�stico de terminales', 'Fue enviada la solicitud de diagn�stico de todas las terminales.', 0);
     end;
end;

procedure TTerminales_DevEx.Modificar;
begin
     inherited;
     dmSistema.cdsTerminales.Modificar;
end;

procedure TTerminales_DevEx.Refresh;
begin
     inherited;
     with dmSistema do
     begin
          FiltroTerminal := VACIO;

          if cmbFiltro.ItemIndex = K_INDEX_ACTIVAS then
            //FiltroTerminal :=  Format ('TE_ACTIVO = %s', [EntreComillas ('S')])
            FiltroTerminal := 'S'
          else if cmbFiltro.ItemIndex = K_INDEX_INACTIVAS then
            //FiltroTerminal :=  Format ('TE_ACTIVO = %s', [EntreComillas ('N')]);
           FiltroTerminal := 'N';
          cdsTerminales.Refrescar;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TTerminales_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_TRESS_SISTEMA_TERMINALESGTI_TERMINALES;
     btnImportarTerminales_DevEx.Enabled := CheckDerecho( D_SIST_TERMINALES, K_DERECHO_SIST_KARDEX );
     btnReiniciaTerminales_DevEx.Enabled := CheckDerecho( D_SIST_TERMINALES, K_DERECHO_BANCA );

     // Agregar elementos al combo para filtrado.
     cmbFiltro.Items.Add('Todas');
     cmbFiltro.Items.Add('S�');
     cmbFiltro.Items.Add('No');
     cmbFiltro.ItemIndex := 0;

     CanLookup := True;
end;

procedure TTerminales_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);

  inherited;

end;

procedure TTerminales_DevEx.IMAGEN_STATUSCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
  var
  APainter: TcxPainterAccess;
begin
  inherited;
  APainter := TcxPainterAccess(TcxViewInfoAcess(AViewInfo).GetPainterClass.Create(ACanvas, AViewInfo));
  with APainter do
    begin
      try
      with TcxCustomTextEditViewInfo(AViewInfo.EditViewInfo).TextRect do
            Left := 20 + IconosList.Width;
      DrawContent;
      DrawBorders;

    with AViewInfo.ClientBounds do
    begin

    case  StrToInt(vartostr(AViewInfo.GridRecord.Values [STATUS.Index])) of
        0 : IconosList.Draw(ACanvas.Canvas, Left+25 , Top + 1, 0 );
        1 : IconosList.Draw(ACanvas.Canvas, Left + 25 , Top + 1, 1 );

      end;
      end;
    finally
      Free;

    end;
end;
  ADone := True;
end;

procedure TTerminales_DevEx.btnImportarTerminales_DevExClick(
  Sender: TObject);
begin
  inherited;
  ShowWizard( prSISTImportaTerminales );
end;

procedure TTerminales_DevEx.btnReiniciaTerminales_DevExClick(
  Sender: TObject);
begin
  inherited;
  if ZConfirm( 'Sincronizar todas las plantillas', 'Esta acci�n sincronizar� todas las plantillas en las terminales existentes.' + CR_LF + '�Desea continuar?', 0, mbYes ) then
     begin
          dmSistema.ReiniciaTerminales( -1 );
          ZInformation( 'Sincronizar todas las plantillas', 'Fue enviada la solicitud de actualizaci�n de plantillas en todas las terminales.', 0);
     end;
end;

procedure TTerminales_DevEx.cmbFiltroChange(Sender: TObject);
begin
    inherited;
    Refresh;
end;



procedure TTerminales_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscaEntero_DevEx.BuscarCodigo( 'N�mero', 'Terminales GTI', 'TE_CODIGO', dmSistema.cdsTerminales );
end;

end.
