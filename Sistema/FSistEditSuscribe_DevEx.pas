unit FSistEditSuscribe_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ZetaKeyCombo, Db, ZetaEdit,
  ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, ImgList, cxButtons;

type
  TFormaEditSuscrip_DevEx = class(TZetaDlgModal_DevEx)
    Label1: TLabel;
    RE_NOMBRE: TZetaDBEdit;
    dsSuscrip: TDataSource;
    Label2: TLabel;
    SU_FRECUEN: TZetaDBKeyCombo;
    Label3: TLabel;
    SU_ENVIAR: TZetaDBKeyCombo;
    Label4: TLabel;
    SU_VACIO: TZetaDBKeyCombo;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormaEditSuscrip_DevEx: TFormaEditSuscrip_DevEx;

implementation

uses DReportes, dSistema;

{$R *.DFM}

procedure TFormaEditSuscrip_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  ActiveControl := SU_FRECUEN;
end;

procedure TFormaEditSuscrip_DevEx.OKClick(Sender: TObject);
begin
  inherited;
  with dsSuscrip.DataSet do
    if State <> dsBrowse then
        Post;
end;

procedure TFormaEditSuscrip_DevEx.CancelarClick(Sender: TObject);
begin
  inherited;
  with dsSuscrip.DataSet do
    if State <> dsBrowse then
        Cancel;
end;

procedure TFormaEditSuscrip_DevEx.OK_DevExClick(Sender: TObject);
begin
  inherited;
  inherited;
  with dsSuscrip.DataSet do
    if State <> dsBrowse then
        Post;
end;

procedure TFormaEditSuscrip_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
  inherited;
  with dsSuscrip.DataSet do
    if State <> dsBrowse then
        Cancel;
end;

end.
