unit FSistBaseEditEmpresas;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Mask, Forms, Dialogs, ExtCtrls,
     Buttons, DBCtrls, StdCtrls, Db,
     ZBaseEdicion,
     ZetaKeyCombo,
     ZetaEdit,
     ZetaKeyLookup, ZetaSmartLists;

type
  TSistBaseEditEmpresas = class(TBaseEdicion)
    Label1: TLabel;
    Label2: TLabel;
    CM_NOMBRE: TDBEdit;
    CM_CODIGO: TZetaDBEdit;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label5: TLabel;
    CM_USRNAME: TDBEdit;
    CM_PASSWRD: TDBEdit;
    CM_CONTROL: TDBEdit;
    CM_DATOS: TDBEdit;
    Label7: TLabel;
    CM_ALIAS: TDBEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  SistBaseEditEmpresas: TSistBaseEditEmpresas;

implementation

{$R *.DFM}

uses DSistema,
     ZetaCommonClasses,
     ZAccesosTress;

procedure TSistBaseEditEmpresas.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_SIST_DATOS_EMPRESAS;
     HelpContext := H80811_Empresas;
     FirstControl := CM_CODIGO;
end;

procedure TSistBaseEditEmpresas.Connect;
begin
     with dmSistema do
     begin
          cdsEmpresasLookUp.Conectar;
          Datasource.Dataset := cdsEmpresas;
     end;
end;

end.
