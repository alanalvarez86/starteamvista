inherited Terminales_DevEx: TTerminales_DevEx
  Caption = 'Terminales'
  ClientHeight = 231
  ClientWidth = 1025
  ExplicitWidth = 1025
  ExplicitHeight = 231
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 1025
    ExplicitWidth = 1025
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 766
      ExplicitWidth = 766
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 760
        ExplicitLeft = 680
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 1025
    Height = 41
    Align = alTop
    TabOrder = 2
    DesignSize = (
      1025
      41)
    object lblFiltro: TLabel
      Left = 918
      Top = 14
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Activo:'
    end
    object btnImportarTerminales_DevEx: TcxButton
      Left = 16
      Top = 11
      Width = 129
      Height = 26
      Hint = 'Importar archivo encriptado de terminales'
      Caption = 'Importar Terminales'
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        200000000000000900000000000000000000000000000000000050D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFF7CDD
        AEFF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFFFFFFFFF7CDDAEFF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF66D8A1FFFFFFFFFF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFFFFFFFFF7CDDAEFF50D293FF50D293FF50D293FF50D293FF50D2
        93FF58D498FF50D293FF50D293FF53D395FF66D8A1FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF53D395FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFFFFFFFFF7CDDAEFF50D293FF50D293FF50D293FF50D293FF7FDE
        B0FFB5ECD1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFA5E8C8FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFFFFFFFFF7CDDAEFF50D293FF50D293FF50D293FF8FE2BAFFFCFE
        FDFFBDEED6FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF60D69DFFB8EDD3FFC8F1DDFF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFFFFFFFFF7CDDAEFF50D293FF50D293FFA0E6C4FFFFFFFFFFFFFF
        FFFFFFFFFFFFE9F9F1FFC8F1DDFFAFEBCEFFA8E9C9FFA8E9C9FFBDEED6FFD9F5
        E7FFFCFEFDFFFCFEFDFF6EDAA6FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFFFFFFFFF7CDDAEFF53D395FFB2EBD0FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF9AE5C1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFFFFFFFFF7CDDAEFF50D293FFB2EBD0FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4FC
        F8FF8FE2BAFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFFFFFFFFF7CDDAEFF50D293FF50D293FFB2EBD0FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDBF6E9FFA5E8C8FF5BD5
        9AFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFFFFFFFFF7CDDAEFF50D293FF50D293FF50D293FFB2EBD0FFFFFF
        FFFFDEF7EBFF9DE6C2FF92E3BCFF7FDEB0FF5ED69BFF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFFFFFFFFF7CDDAEFF50D293FF50D293FF50D293FF50D293FFB2EB
        D0FFBDEED6FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFFFFFFFFF7CDDAEFF50D293FF50D293FF50D293FF50D293FF50D2
        93FF79DDACFF50D293FF50D293FF55D396FF7CDDAEFF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFFFFFFFFF7CDDAEFF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF66D8A1FFFFFFFFFF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF66D8A1FF66D8A1FF66D8A1FF66D8A1FF66D8A1FF66D8A1FF66D8
        A1FF66D8A1FF66D8A1FF66D8A1FF66D8A1FF66D8A1FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
      OptionsImage.Margin = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnImportarTerminales_DevExClick
    end
    object btnReiniciaTerminales_DevEx: TcxButton
      Left = 153
      Top = 11
      Width = 176
      Height = 26
      Hint = 
        'Actualizar todas las terminales con las plantillas correspondien' +
        'tes'
      Caption = 'Sincronizar todas las plantillas'
      Enabled = False
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        2000000000000009000000000000000000000000000000000000D8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDCB38DFFEBD3
        BDFFF3E4D7FFFBF6F1FFFDFAF7FFF6ECE3FFF0DFCFFFE2BF9FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFDDB58FFFD8AA7FFFE5C7ABFFFDFAF7FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3E4D7FFDEB6
        91FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFF5EADFFFF7EEE5FFFFFFFFFFFFFFFFFFFAF4
        EFFFEAD2BBFFE4C3A5FFE2C1A1FFE9CEB5FFF5EADFFFFFFFFFFFFFFFFFFFF9F3
        EDFFDCB28BFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFF9F3EDFFFFFFFFFFFFFFFFFFF5E8DDFFDAAE
        85FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE3C2A3FFFDFBF9FFFFFF
        FFFFF6ECE3FFD9AB81FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFFDFAF7FFFFFFFFFFFFFFFFFFFEFEFDFFE2C1
        A1FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE2BF9FFFFFFF
        FFFFFFFFFFFFE4C3A5FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFFFFFFFFFFDFAF7FFEEDAC7FFDCB38DFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFF5EA
        DFFFFFFFFFFFF2E2D3FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFDCB28BFFEAD1B9FFDAAF87FFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE4C3
        A5FFF5EADFFFF4E7DBFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFE1BD9BFFE2BF9FFFDCB38DFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFF6EBE1FFFFFFFFFFF2E2D3FFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AD
        83FFE7CAAFFFDEB793FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFE7CBB1FFFFFFFFFFFEFCFBFFDBB189FFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDFB995FFEEDBC9FFFDFA
        F7FFFFFFFFFFDDB58FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFDAAF87FFFEFCFBFFFFFFFFFFF4E7DBFFD9AB81FFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDFB995FFFBF6F1FFFFFFFFFFFFFF
        FFFFFFFFFFFFDAAF87FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFE7CBB1FFFFFFFFFFFFFFFFFFF3E4D7FFDDB5
        8FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDDB58FFFF5EADFFFFFFFFFFFFFFF
        FFFFFEFEFDFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFEDD8C5FFFFFFFFFFFFFFFFFFFEFC
        FBFFF3E4D7FFEDD7C3FFEDD8C5FFF3E6D9FFFEFCFBFFFFFFFFFFFFFFFFFFF5EA
        DFFFF9F2EBFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE6C9ADFFFBF7F3FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCF8F5FFE5C6A9FFD8AA
        7FFFDCB28BFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDAAE85FFE8CD
        B3FFEFDCCBFFF7EEE5FFF6EBE1FFEEDAC7FFE6C9ADFFDBB189FFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
      OptionsImage.Margin = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnReiniciaTerminales_DevExClick
    end
    object cmbFiltro: TZetaKeyCombo
      Left = 957
      Top = 11
      Width = 60
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Anchors = [akTop, akRight]
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 2
      OnChange = cmbFiltroChange
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
    object DiagnosticoTerminales: TcxButton
      Left = 339
      Top = 11
      Width = 196
      Height = 26
      Hint = 'Obtener informaci'#243'n para diagn'#243'stico de todas las terminales'
      Caption = 'Obtener informaci'#243'n de terminales'
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        20000000000000090000000000000000000000000000000000005B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF8EB4FFFFE3ECFFFFEAF1FFFFEAF1FFFFEAF1
        FFFFEAF1FFFFEAF1FFFFEAF1FFFFEAF1FFFF6D9EFFFFE5EEFFFF96B9FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFFF0F5FFFFFFFFFFFFFFFFFFFFFFFFFFFF94B8
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFFFFFFFFFFFFFFFFFB2CC
        FFFF5E94FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFFFFFFFFFFFFFFFFFFFFFFFFFFE0EAFFFF82AC
        FFFFFCFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFD1E0FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFFFFFFFFFFFFFFFFFFFFFFFFFFDBE7FFFF96B9
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFEDF3FFFF6095FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF77A5
        FFFFA6C4FFFFADC9FFFFADC9FFFFADC9FFFFADC9FFFFFFFFFFFFFFFFFFFFD8E5
        FFFF6A9CFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFD
        FFFFD8E5FFFFD6E4FFFFD6E4FFFFD6E4FFFF70A0FFFFF7FAFFFFC6D9FFFF6397
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6C4FFFF77A5FFFF82ACFFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEE9FFFFFCFDFFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFFF0F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0F5FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF8EB4FFFFEAF1FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8F0FFFF8EB4FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
        FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF}
      OptionsImage.Margin = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = DiagnosticoTerminalesClick
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 60
    Width = 1025
    Height = 171
    ExplicitTop = 60
    ExplicitWidth = 1025
    ExplicitHeight = 171
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object TE_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'TE_CODIGO'
      end
      object TE_ID: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'TE_ID'
        GroupSummaryAlignment = taCenter
      end
      object TE_NOMBRE: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'TE_NOMBRE'
      end
      object TE_APP: TcxGridDBColumn
        Caption = 'Aplicaci'#243'n'
        DataBinding.FieldName = 'TE_APP'
      end
      object TE_TER_HOR: TcxGridDBColumn
        Caption = 'Fecha en la terminal'
        DataBinding.FieldName = 'TE_TER_HOR'
      end
      object TE_VERSION: TcxGridDBColumn
        Caption = 'Versi'#243'n'
        DataBinding.FieldName = 'TE_VERSION'
      end
      object TE_IP: TcxGridDBColumn
        Caption = 'Direcci'#243'n IP'
        DataBinding.FieldName = 'TE_IP'
      end
      object TE_HUELLAS: TcxGridDBColumn
        Caption = 'Cantidad de plantillas'
        DataBinding.FieldName = 'TE_HUELLAS'
        Width = 100
      end
      object IMAGEN_STATUS: TcxGridDBColumn
        Caption = 'Estatus'
        OnCustomDrawCell = IMAGEN_STATUSCustomDrawCell
        FooterAlignmentHorz = taCenter
        HeaderGlyphAlignmentHorz = taCenter
        Width = 45
      end
      object STATUS: TcxGridDBColumn
        DataBinding.FieldName = 'STATUS'
        Visible = False
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 24
    Top = 128
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
    DesignInfo = 8388832
  end
  inherited ActionList: TActionList
    Left = 162
    Top = 128
  end
  inherited PopupMenu1: TPopupMenu
    Left = 96
    Top = 128
  end
  object IconosList: TImageList
    Left = 352
    Top = 120
    Bitmap = {
      494C010102000800580010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000DFE2F60099A2E300727FD7005160CD005160CD00727FD70099A2E300D7DA
      F400000000000000000000000000000000000000000000000000000000000000
      0000DEF6EB009DE6C30078DDAC0057D3970057D3970078DDAC009DE6C300D6F4
      E500000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000AFB6
      E8004E5CCE004858CC004858CC004858CC004858CC004858CC004858CC004B5B
      CD00A1A9E500FCFCFE0000000000000000000000000000000000FCFEFD00A7E8
      C90053D3950050D2930050D2930050D2930050D2930050D2930050D2930050D2
      93009AE4C100F7FDFA0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848EDD004858
      CC004858CC004858CC004858CC004858CC004858CC004858CC004858CC004858
      CC004858CC00727FD700FCFCFE000000000000000000FCFEFD007CDDAD0050D2
      930050D2930050D2930050D2930050D2930050D2930050D2930050D2930050D2
      930050D293006BD8A400F7FDFA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B4BBEA004858CC004858
      CC004858CC004858CC004858CC004858CC004858CC004858CC004858CC004858
      CC004858CC004858CC00A1A9E5000000000000000000AFEACE0050D2930050D2
      930050D2930057D39700D0F3E20069D7A10050D2930050D2930050D2930050D2
      930050D2930050D293009AE4C100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E5E7F8005160CD004858CC004858
      CC004858CC0099A2E300848EDD004858CC004858CC00848EDD00ACB3E7004858
      CC004858CC004858CC004B5BCD00D7DAF400E4F8EE0055D2950050D2930050D2
      930050D29300BDEED60000000000E4F8EE005BD59A0050D2930050D2930050D2
      930050D2930050D2930050D29300D6F4E5000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000ACB3E7004858CC004858CC004858
      CC00939CE10000000000FCFCFE00848EDD00848EDD00FCFCFE0000000000A9B0
      E7004858CC004858CC004858CC0099A2E300AFEACE0050D2930050D2930050D2
      930098E5BE00000000000000000000000000D6F4E50053D3950050D2930050D2
      930050D2930050D2930050D293009DE6C3000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000848EDD004858CC004858CC004858
      CC006571D300EEEFFA0000000000FCFCFE00FCFCFE0000000000F6F7FD00727F
      D7004858CC004858CC004858CC00727FD7008AE1B70050D2930050D2930074DA
      A800FAFEFC0000000000CBF1DF00FCFEFD0000000000C0EED80050D2930050D2
      930050D2930050D2930050D2930078DDAC000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006270D2004858CC004858CC004858
      CC004858CC006571D300F1F2FB000000000000000000F6F7FD00727FD7004858
      CC004858CC004858CC004858CC005160CD0069D7A10050D2930050D29300A7E8
      C90000000000CEF2E00053D3950076DCAB00EFFBF50000000000AAE9CA0050D2
      930050D2930050D2930050D2930057D397000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006875D5004858CC004858CC004858
      CC004858CC00848EDD00FCFCFE000000000000000000FCFCFE00848EDD004858
      CC004858CC004858CC004858CC005362CE006DDAA60050D2930050D2930050D2
      9300A3E8C6005ED69B0050D2930050D2930060D59C00D9F5E7000000000092E3
      BC0050D2930050D2930050D293005BD59A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008994DE004858CC004858CC004858
      CC00848EDD00FCFCFE0000000000F6F7FD00EEEFFA0000000000FCFCFE00848E
      DD004858CC004858CC004858CC007985DA008EE3BA0050D2930050D2930050D2
      930050D2930050D2930050D2930050D2930050D2930053D39500BDEED600FCFE
      FD007FDDB00050D2930050D293007FDDB0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000AFB6E8004858CC004858CC004858
      CC00848EDD00FCFCFE00F6F7FD00727FD7006571D300EEEFFA000000000099A2
      E3004858CC004858CC004858CC009BA4E400B2EBCF0050D2930050D2930050D2
      930050D2930050D2930050D2930050D2930050D2930050D2930050D293009AE4
      C100D9F5E70050D2930050D29300A0E6C4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EEEFFA005564CF004858CC004858
      CC004858CC00818CDB00727FD7004858CC004858CC006571D300939CE1004858
      CC004858CC004858CC004E5CCE00E2E5F700ECFAF3005BD59A0050D2930050D2
      930050D2930050D2930050D2930050D2930050D2930050D2930050D2930050D2
      930053D3950050D2930053D39500E1F7EC000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6CBEF004858CC004858
      CC004858CC004858CC004858CC004858CC004858CC004858CC004858CC004858
      CC004858CC004858CC00B1B8E9000000000000000000BDEED60050D2930050D2
      930050D2930050D2930050D2930050D2930050D2930050D2930050D2930050D2
      930050D2930050D29300A7E8C900000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009BA4E4004858
      CC004858CC004858CC004858CC004858CC004858CC004858CC004858CC004858
      CC004858CC008692DE00000000000000000000000000000000008EE3BA0050D2
      930050D2930050D2930050D2930050D2930050D2930050D2930050D2930050D2
      930050D293007CDDAD00FCFEFD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6CB
      EF005564CF004858CC004858CC004858CC004858CC004858CC004858CC005160
      CD00B7BDEB00000000000000000000000000000000000000000000000000BDEE
      D6005BD59A0050D2930050D2930050D2930050D2930050D2930050D2930055D2
      9500AFEACE00FCFEFD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EEEFFA00AFB6E8008994DE006875D5006875D5008994DE00AFB6E800E8EA
      F900000000000000000000000000000000000000000000000000000000000000
      0000ECFAF300B2EBCF008EE3BA006DDAA6006DDAA6008EE3BA00B2EBCF00E6F9
      F000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00F00FF00F00000000E003C00300000000
      C001800100000000800180010000000000000200000000000420070000000000
      0240048000000000018008400000000001800020000000000240000000000000
      002000000000000000000000000000008001800100000000C003C00100000000
      E007E00300000000F00FF00F0000000000000000000000000000000000000000
      000000000000}
  end
end
