inherited SistBitacoraSistema_DevEx: TSistBitacoraSistema_DevEx
  Caption = 'Bit'#225'cora de Sistema'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelFiltros: TPanel
    inherited lbUsuario: TLabel
      Left = 8
    end
    inherited Label2: TLabel
      Left = 8
    end
    inherited TipoChk: TCheckBox
      Left = 38
    end
    inherited Usuario: TZetaKeyLookup_DevEx
      Width = 329
    end
    inherited GroupBox1: TGroupBox
      Left = 8
      Width = 433
    end
    inherited ClaseBit: TComboBox
      Width = 225
    end
    inherited Refrescar_DevEx: TcxButton
      Left = 391
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      inherited BI_NUMERO: TcxGridDBColumn
        Visible = False
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
