unit FCamposCFG_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     FCamposMgr,
     ZetaSmartLists, ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, ImgList, cxButtons, cxControls,
  ZetaSmartLists_DevEx, cxContainer, cxEdit, cxListBox;

type
  TCamposCFG_DevEx = class(TZetaDlgModal_DevEx)
    ZetaSmartLists: TZetaSmartLists_DevEx;
    DisponiblesGB: TGroupBox;
    lbDisponibles: TZetaSmartListBox_DevEx;
    PanelEscoger: TPanel;
    btnSeleccionar: TZetaSmartListsButton_DevEx;
    btnDeseleccionar: TZetaSmartListsButton_DevEx;
    PanelSubirBajar: TPanel;
    btnSubir: TZetaSmartListsButton_DevEx;
    btnBajar: TZetaSmartListsButton_DevEx;
    PanelEscogidos: TPanel;
    EscogidosGB: TGroupBox;
    lbEscogidos: TZetaSmartListBox_DevEx;
    GruposGB: TGroupBox;
    btnEditar: TcxButton;
    lbGrupos: TListBox;
    btnSubirGrupo: TcxButton;
    btnBajarGrupo: TcxButton;
    btnAgregarGrupo: TcxButton;
    btnBorrarGrupo: TcxButton;
    btnModificarGrupo: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ZetaSmartListsAlEscoger(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure ZetaSmartListsAlRechazar(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure btnEditarClick(Sender: TObject);
    procedure btnSubirGrupoClick(Sender: TObject);
    procedure btnBajarGrupoClick(Sender: TObject);
    procedure btnAgregarGrupoClick(Sender: TObject);
    procedure lbGruposClick(Sender: TObject);
    procedure btnModificarGrupoClick(Sender: TObject);
    procedure btnBorrarGrupoClick(Sender: TObject);
    procedure DisponiblesDblClick(Sender: TObject);
    procedure ZetaSmartListsAlSeleccionar(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FCampos: TListaCampos;
    FClasificaciones: TListaClasificaciones;
    function EditarCampo(oCampo: TCampo ): Boolean;
    function EditarClasificacion(const lAlta: Boolean; oGrupo: TClasificacion): Boolean;
    function GetClasificacion: TClasificacion;
    function GetExcede: Boolean;
    procedure CargarGrupo;
    procedure CargarGrupos;
    procedure OrdenaClasificaciones;
    procedure SetClasificaciones;
    procedure SetControls( oLista : TZetaSmartListBox_DevEx; const lEscogido : Boolean );
    procedure SetControlsEscogidos;
    procedure CargarDisponibles;
    procedure ZetaSmartListsAlCambiarTexto( Objeto: TObject; var Texto: String );
    procedure ChecarDerecho(const iDerecho:Integer);
protected
    procedure EscribeCambios;
  public
    { Public declarations }
    property Campos: TListaCampos read FCampos;
    property Excede: Boolean read GetExcede;
    property Clasificaciones: TListaClasificaciones read FClasificaciones;
  end;

const
     K_TOPE_VERTICAL_TRESS = 14;
var
  CamposCFG_DevEx: TCamposCFG_DevEx;

function ConfiguracionCampos( oCampos: TListaCampos; oClasificaciones: TListaClasificaciones ): Boolean;

implementation

{$R *.DFM}

uses dSistema,
     ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZAccesosTress,
     ZAccesosMgr,
     FCampoEdit_DevEx,
     FClasificacionEdit_DevEx;

function ConfiguracionCampos( oCampos: TListaCampos; oClasificaciones: TListaClasificaciones ): Boolean;
begin
     CamposCFG_DevEx := TCamposCFG_DevEx.Create( Application );
     try
        with CamposCFG_DevEx do
        begin
             Campos.Assign( oCampos );
             Clasificaciones.Assign( oClasificaciones );
             ShowModal;
             Result := ( ModalResult = mrOk );
             if Result then
             begin
                  oCampos.Assign( Campos );
                  oClasificaciones.Assign( Clasificaciones );
             end;
        end;
     finally
            FreeAndNil( CamposCFG_DevEx );
     end;
end;

procedure TCamposCFG_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H65103_Campos_adicionales;   // Se usar� el mismo HelpContext de antes, pero cambia mucho la forma
     FCampos := TListaCampos.Create;
     FClasificaciones := TListaClasificaciones.Create;
     ZetaSmartLists.AlCambiarTexto := ZetaSmartListsAlCambiarTexto;
     lbEscogidos.OnDblClick := btnEditarClick;
end;

procedure TCamposCFG_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     dmSistema.cdsGruposAdic.Conectar;
     CargarDisponibles;
     CargarGrupos;
     lbGrupos.ItemIndex := 0;
     SetClasificaciones;
     CargarGrupo;
     ChecarDerecho(ZAccesosTress.D_CAT_CONFI_ADICIONALES);
end;

procedure TCamposCFG_DevEx.FormDestroy(Sender: TObject);
begin
     {EditClasificacion y CampoEdit, se crean si se van a utilizar}
     FreeAndNil( EditClasificacion_DevEx );
     FreeAndNil( CampoEdit_DevEx );
     FreeAndNil( FClasificaciones );
     FreeAndNil( FCampos );
     inherited;
end;

procedure TCamposCFG_DevEx.ChecarDerecho(const iDerecho:Integer);
var
   lCanEdit:Boolean;
begin
     lCanEdit := ZAccesosMgr.CheckDerecho( iDerecho, K_DERECHO_CAMBIO );
     with OK_DevEx do
     begin
          Enabled := lCanEdit;
          Visible := lCanEdit;
     end;
     with Cancelar_DevEx do
     begin
          if lCanEdit then
          begin
               //Kind := bkCancel;
               OptionsImage.ImageIndex := 0;
               Cancel := False;
               Caption := '&Cancelar';
               Hint := 'Cancelar Cambios';
          end
          else
          begin
               //Kind := bkClose;
               OptionsImage.ImageIndex := 2;
               Cancel := True;
               Caption := '&Salir';
               Hint := 'Cerrar Pantalla y Salir';
               if Self.Active then  // EZM: S�lo si la forma es visible
                 SetFocus;
          end;
     end;
end;

function TCamposCFG_DevEx.GetClasificacion: TClasificacion;
begin
     with lbGrupos do
     begin
          if ( ItemIndex < 0 ) then
             Result := nil
          else
              Result := TClasificacion( Items.Objects[ ItemIndex ] );
     end;
end;

procedure TCamposCFG_DevEx.CargarDisponibles;
begin
     with Campos do
     begin
          CargarDisponibles( lbDisponibles.Items );
     end;
end;

procedure TCamposCFG_DevEx.CargarGrupos;
begin
     with Clasificaciones do
     begin
          Cargar( lbGrupos.Items );
     end;
     OrdenaClasificaciones;
end;

procedure TCamposCFG_DevEx.CargarGrupo;
begin
     if ( lbGrupos.ItemIndex >= 0 ) then
     begin
          with Campos do
          begin
               CargarEscogidos( lbEscogidos.Items, GetClasificacion.Codigo );
          end;

          ZetaSmartLists.SelectEscogido( 0 );
          ZetaSmartLists.Refrescar;
     end
     else
         lbEscogidos.Clear;
end;

procedure TCamposCFG_DevEx.SetClasificaciones;
var
   lEnabled: Boolean;
begin
     lEnabled := ( lbGrupos.ItemIndex >= 0 );
     btnSubirGrupo.Enabled := lEnabled and ( lbGrupos.Items.Count > 1 ) and ( lbGrupos.ItemIndex > 0 );
     btnBajarGrupo.Enabled := lEnabled and ( lbGrupos.Items.Count > 1 ) and ( lbGrupos.ItemIndex < ( lbGrupos.Items.Count - 1 ) );
     btnBorrarGrupo.Enabled := lEnabled;
     btnModificarGrupo.Enabled := lEnabled;
end;

function TCamposCFG_DevEx.EditarClasificacion( const lAlta: Boolean; oGrupo: TClasificacion ): Boolean;
begin
     if not Assigned( EditClasificacion_DevEx ) then
        EditClasificacion_DevEx := TEditClasificacion_DevEx.Create( Self );

     with EditClasificacion_DevEx do
     begin
          EsAlta := lAlta;
          Clasificaciones := FClasificaciones;
          Clasificacion := oGrupo;
          ShowModal;
          Result := ( ModalResult = mrOk );
     end;
end;

function TCamposCFG_DevEx.EditarCampo( oCampo: TCampo ): Boolean;
begin
     inherited;
     if not Assigned( CampoEdit_DevEx ) then
        CampoEdit_DevEx := TCampoEdit_DevEx.Create( Self );
     with CampoEdit_DevEx do
     begin
{
          //MA:Linea Para limpiar al campo si se vuelve a escoger
          With oCampo do
          begin
               if not Capturar then
               begin
                    Letrero := VACIO;
               end;
          end;
}
          Campo := oCampo;
          ExcedeAlto := Excede;
          ShowModal;
          Result := ( ModalResult = mrOk );
     end;
end;

procedure TCamposCFG_DevEx.SetControls( oLista : TZetaSmartListBox_DevEx; const lEscogido : Boolean );
begin
     with oLista.Items do
     begin
          if Count > 0 then
          begin
               with TCampo( Objects[ Count - 1 ] ) do
               begin
                    if lEscogido then
                       Strings[ Count - 1 ] := TextoEscogido
                    else
                        Strings[ Count - 1 ] := TextoDisponible;
               end;
          end
     end;

     with ZetaSmartLists do
     begin
          SelectEscogido( oLista.Items.Count - 1 );
     end;

     SetControlsEscogidos;
end;

procedure TCamposCFG_DevEx.OrdenaClasificaciones;
var
   i: Integer;
begin
     with lbGrupos.Items do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with TClasificacion( Objects[ i ] ) do
               begin
                    Posicion := i + 1;
               end;
          end;
     end;
end;

procedure TCamposCFG_DevEx.ZetaSmartListsAlEscoger(Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     //MA:Linea Para que no se puedan escoger campos si no existe una clasificacion
     if ( FClasificaciones.Count > 0 ) then
     begin
          if Assigned( Objeto ) then
          begin
               if EditarCampo( TCampo( Objeto ) ) then
               begin
                    TCampo( Objeto ).Clasificacion := GetClasificacion.Codigo;
               end
               else ZetaSmartLists.Ok2Move := False;
          end;
          SetControls( lbEscogidos, TRUE );
     end
     else
     begin
          lbEscogidos.Clear;
          ZetaSmartLists.Ok2Move := False;
          ZError(Caption,'Se tiene que dar de Alta por lo Menos una Clasificaci�n', 0);
     end;
end;

procedure TCamposCFG_DevEx.ZetaSmartListsAlRechazar(Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     if Assigned( Objeto ) then
     begin
          with TCampo( Objeto ) do
               Inicializa;
     end;
     SetControls( lbDisponibles, FALSE );
end;

procedure TCamposCFG_DevEx.ZetaSmartListsAlCambiarTexto( Objeto: TObject; var Texto: String );
begin
     with TCampo( Objeto ) do
     begin
          if Capturar then
             Texto := TextoEscogido
          else
              Texto := TextoDisponible;
     end;
end;



procedure TCamposCFG_DevEx.EscribeCambios;
begin
     with dmSistema do
     begin
          DescargarGruposAdic( self.Clasificaciones );
          DescargarCamposAdic( self.Campos );

          cdsGruposAdic.Enviar;

          if ( cdsGruposAdic.ChangeCount = 0 ) and ( cdsCamposAdic.ChangeCount = 0 ) then
             self.ModalResult := mrOk;  // Cerrar la forma
     end;
end;

procedure TCamposCFG_DevEx.btnEditarClick(Sender: TObject);
var
   iPtr: Integer;
   oCampo: TCampo;
begin
     inherited;
     with lbEscogidos do
     begin
          iPtr := ItemIndex;
          //MA:Linea Para Cuando no se tiene seleccionado ningun campo
          if iPtr <> -1 then
          begin
               oCampo := TCampo( Items.Objects[ iPtr ] );
               if EditarCampo( oCampo ) then
               begin
                    with lbEscogidos.Items do
                    begin
                         Strings[ iPtr ] := oCampo.TextoEscogido;
                    end;
               end;
          end
          else
              ZInformation('Operaci�n No V�lida','No Se Tiene Seleccionado Ning�n Campo',0);
     end;
end;

procedure TCamposCFG_DevEx.lbGruposClick(Sender: TObject);
begin
     inherited;
     SetClasificaciones;
     CargarGrupo;
end;

procedure TCamposCFG_DevEx.btnSubirGrupoClick(Sender: TObject);
var
   iPtr: Integer;
begin
     inherited;
     with lbGrupos do
     begin
          iPtr := ItemIndex;
          if ( iPtr > 0 ) then
             Items.Exchange( iPtr, iPtr - 1 );
     end;
     OrdenaClasificaciones;
     SetClasificaciones
end;

procedure TCamposCFG_DevEx.btnBajarGrupoClick(Sender: TObject);
var
   iPtr: Integer;
begin
     inherited;
     with lbGrupos do
     begin
          iPtr := ItemIndex;
          with Items do
          begin
               if ( iPtr >= 0 ) and ( iPtr < Count - 1 ) then
                  Exchange( iPtr, iPtr + 1 );
          end;
     end;
     OrdenaClasificaciones;
     SetClasificaciones;
end;

procedure TCamposCFG_DevEx.btnAgregarGrupoClick(Sender: TObject);
var
   oGrupo: TClasificacion;
begin
     inherited;
     oGrupo := TClasificacion.Create;
     try
        if EditarClasificacion( True, oGrupo ) then
        begin
             with lbGrupos do
             begin
                  Items.AddObject( oGrupo.Nombre, FClasificaciones.Add( oGrupo.Codigo, oGrupo.Nombre, oGrupo.Posicion ,oGrupo.Derecho) );
                  ItemIndex := Items.Count - 1;
             end;
             //btnSeleccionar.Enabled:=True;
             OrdenaClasificaciones;
             SetClasificaciones;
             CargarGrupo;
        end;
     finally
            FreeAndNil( oGrupo );
     end;
end;

procedure TCamposCFG_DevEx.btnBorrarGrupoClick(Sender: TObject);
var
   iPtr: Integer;
   sCodigo: String;
begin
     inherited;
     iPtr := lbGrupos.ItemIndex;
     if ( iPtr >= 0 ) and ZetaDialogo.zConfirm( '� Atenci�n !', '� Desea Borrar Esta Clasificaci�n ?', 0, mbNo ) then
     begin
          sCodigo := GetClasificacion.Codigo;
          FClasificaciones.Borrar( sCodigo );
          FCampos.BorrarClasificacion( sCodigo );
          CargarDisponibles;
          CargarGrupos;
          with lbGrupos do
          begin
               if ( Items.Count > 0 ) then
                  ItemIndex := iMax( iPtr - 1, 0 )
               else
                   ItemIndex := -1;
          end;
          SetClasificaciones;
          CargarGrupo;
          SetControls( lbEscogidos, TRUE );
     end;
end;

procedure TCamposCFG_DevEx.btnModificarGrupoClick(Sender: TObject);
var
   iPtr: Integer;
begin
     inherited;
     iPtr := lbGrupos.ItemIndex;
     if ( iPtr >= 0 ) then
     begin
          if EditarClasificacion( False, GetClasificacion ) then
             lbGrupos.Items.Strings[ iPtr ] := GetClasificacion.Nombre;
     end;
end;

procedure TCamposCFG_DevEx.DisponiblesDblClick(Sender: TObject);
begin
     inherited;
     if ( Clasificaciones.Count > 0 ) then
     begin
          ZetaSmartLists.Escoger;

     end
     else
         ZError(Caption,'Se tiene que dar de Alta por lo Menos una Clasificaci�n', 0);
end;

function TCamposCFG_DevEx.GetExcede: Boolean;
begin
     Result := ( ( lbEscogidos.Items.Count + 1 ) > K_TOPE_VERTICAL_TRESS );
end;

procedure TCamposCFG_DevEx.ZetaSmartListsAlSeleccionar(Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     SetControlsEscogidos;
end;

procedure TCamposCFG_DevEx.SetControlsEscogidos;
begin
     btnEditar.Enabled := ( lbEscogidos.Items.Count > 0 );
end;

procedure TCamposCFG_DevEx.OK_DevExClick(Sender: TObject);
var
   oCursor : TCursor;
   i: Integer;
begin
     oCursor := Screen.Cursor;
     try
        Screen.Cursor := crHourGlass;

        inherited;

        with lbEscogidos.Items do
        begin
             for i := 0 to ( Count - 1 ) do
             begin
                  with TCampo( Objects[ i ] ) do
                  begin
                       Posicion := i + 1;
                  end;
             end;
        end;

        OrdenaClasificaciones;

        EscribeCambios;

     finally
            Screen.Cursor := oCursor;
     end;
end;

end.
