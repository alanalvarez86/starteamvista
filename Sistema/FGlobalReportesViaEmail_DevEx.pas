unit FGlobalReportesViaEmail_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Mask,
     ZetaNumero, ZetaKeyCombo, ZBaseGlobal_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters, ImgList,
  cxButtons;

type
  TGlobalReportesViaEmail_DevEx = class(TBaseGlobal_DevEx)
    UserId: TEdit;
    Plantilla: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Buscar: TcxButton;
    Label6: TLabel;
    UserEmail: TEdit;
    Label7: TLabel;
    DisplayName: TEdit;
    Label8: TLabel;
    ErrorEmail: TEdit;
    Host: TEdit;
    OpenDialog: TOpenDialog;
    Label4: TLabel;
    EmailPswd: TEdit;
    Label5: TLabel;
    PuertoSMTP: TZetaNumero;
    PrimerDiaLBL: TLabel;
    Autenticacion: TZetaKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure BuscarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GlobalReportesViaEmail_DevEx: TGlobalReportesViaEmail_DevEx;

implementation

uses DGlobal,
     ZReportTools,
     ZAccesosTress,
     ZGlobalTress,
     ZetaCommonTools,
     ZetaCommonClasses;

{$R *.DFM}

procedure TGlobalReportesViaEmail_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos  := ZAccesosTress.D_CAT_CONFI_REP_EMAIL;
     Host.Tag       := K_GLOBAL_EMAIL_HOST;
     PuertoSMTP.Tag := K_GLOBAL_EMAIL_PORT;
     UserId.Tag     := K_GLOBAL_EMAIL_USERID;
     EmailPswd.Tag  := K_GLOBAL_EMAIL_PSWD;
     UserEmail.Tag  := K_GLOBAL_EMAIL_FROMADRESS;
     DisplayName.Tag:= K_GLOBAL_EMAIL_FROMNAME;
     ErrorEMail.Tag  := K_GLOBAL_EMAIL_MSGERROR;
     Plantilla.Tag  := K_GLOBAL_EMAIL_PLANTILLA;
     Autenticacion.Tag := K_GLOBAL_EMAIL_AUTH;

     HelpContext := H65112_Globales_de_reportes_email;
end;

procedure TGlobalReportesViaEmail_DevEx.BuscarClick(Sender: TObject);
begin
     inherited;
     with OpenDialog do
     begin
          InitialDir := VerificaDir( Global.GetGlobalString( K_GLOBAL_DIR_PLANT ));
          FileName := StrDef(Plantilla.Text, K_TEMPLATE_HTM);
          if Execute then
          begin
               if UpperCase(ExtractFileDir(FileName)) = UpperCase(InitialDir) then
                  Plantilla.Text := ExtractFileName(FileName)
               else
                  Plantilla.Text := FileName;
          end;
     end;

end;

procedure TGlobalReportesViaEmail_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  with Autenticacion do
  begin
       if ItemIndex < 0 then
          ItemIndex := 0;
  end;
end;

end.
