inherited EditUsuarioBaseDatos: TEditUsuarioBaseDatos
  Left = 467
  Caption = 'Usuario de MSSQL'
  ClientHeight = 174
  ClientWidth = 288
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblUsuario: TLabel [0]
    Left = 60
    Top = 54
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Usuario:'
  end
  object Label5: TLabel [1]
    Left = 69
    Top = 76
    Width = 30
    Height = 13
    Caption = 'Clave:'
  end
  object Label1: TLabel [2]
    Left = 35
    Top = 100
    Width = 64
    Height = 13
    Caption = 'Confirmaci'#243'n:'
  end
  inherited PanelBotones: TPanel
    Top = 138
    Width = 288
    TabOrder = 5
    inherited OK: TBitBtn
      Left = 120
      Enabled = False
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 205
    end
  end
  object edUsuario: TEdit
    Left = 102
    Top = 50
    Width = 123
    Height = 21
    Enabled = False
    TabOrder = 2
    OnChange = edUsuarioChange
  end
  object edPassword: TEdit
    Left = 102
    Top = 72
    Width = 123
    Height = 21
    Enabled = False
    MaxLength = 30
    PasswordChar = '*'
    TabOrder = 3
  end
  object rbDefault: TRadioButton
    Left = 16
    Top = 8
    Width = 265
    Height = 17
    Caption = 'Usuario y clave default (Configuraci'#243'n del Sistema)'
    Checked = True
    TabOrder = 0
    TabStop = True
    OnClick = rbDefaultClick
  end
  object rbOtro: TRadioButton
    Left = 16
    Top = 32
    Width = 201
    Height = 17
    Caption = 'Utilizar otro usuario y clave de acceso:'
    TabOrder = 1
    OnClick = rbOtroClick
  end
  object edConfirmacion: TEdit
    Left = 102
    Top = 96
    Width = 123
    Height = 21
    Enabled = False
    MaxLength = 30
    PasswordChar = '*'
    TabOrder = 4
  end
end
