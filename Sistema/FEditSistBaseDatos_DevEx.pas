unit FEditSistBaseDatos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion_DevEx, DB, ExtCtrls, DBCtrls, ZetaSmartLists, 
  StdCtrls, Mask, ZetaNumero, ZetaEdit, ZetaKeyCombo,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, TressMorado2013, dxSkinsDefaultPainters, cxControls,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, ImgList,
  cxNavigator, cxDBNavigator, cxButtons;

type
  TEditSistBaseDatos_DevEx = class(TBaseEdicion_DevEx)
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    btCambiarUbicacion: TcxButton;
    btCambiarUsuario: TcxButton;
    Label5: TLabel;
    DB_DESCRIP: TDBEdit;
    DB_DATOS: TDBEdit;
    DB_USRNAME: TDBEdit;
    DB_TIPO: TZetaDBKeyCombo;
    DB_CODIGO: TZetaDBEdit;
    Label1: TLabel;
    lblProgramEspecial: TLabel;
    chbEspecial: TDBCheckBox;
    Panel1: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure btCambiarUbicacionClick(Sender: TObject);
    procedure btCambiarUsuarioClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    { Protected declarations }
    procedure Connect; override;  
    procedure DoLookup; override;
  end;

var
  EditSistBaseDatos_DevEx: TEditSistBaseDatos_DevEx;

implementation

uses dSistema,
     ZAccesosTress,
     ZetaCommonLists,
     ZetaCommonClasses,
     FEditUsuarioBaseDatos_DevEx,
     dCatalogos,
     FEditUbicaBaseDatos_DevEx, ZetaClientDataSet;

{$R *.dfm}

{ TEditSistBaseDatos }

procedure TEditSistBaseDatos_DevEx.Connect;
begin
     inherited;
     with dmSistema do
     begin
          DataSource.DataSet := cdsSistBaseDatos;
     end;
end;

procedure TEditSistBaseDatos_DevEx.FormCreate(Sender: TObject);
var
   eCiclo: eTipoCompany;
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_SIST_BASE_DATOS;
     FirstControl := DB_CODIGO;

     with DB_TIPO.Lista do
     begin
          for eCiclo := Low( eTipoCompany ) to High( eTipoCompany ) do
          begin
               Add( Format( '%d=%s', [ Ord( eCiclo ) , ZetaCommonLists.ObtieneElemento( lfTipoCompanyName, Ord( eCiclo )  ) ] ) );
          end;
     end;

     HelpContext := H_SIST_BASE_DATOS;

end;

procedure TEditSistBaseDatos_DevEx.btCambiarUbicacionClick(Sender: TObject);
var
   sServidor, sBaseDatos: String;
begin
     with dmSistema.cdsSistBaseDatos do
     begin
          sServidor :=  Copy (FieldByName ('DB_DATOS').AsString, 0, (AnsiPos('.', FieldByName ('DB_DATOS').AsString)-1));
          
          sBaseDatos := Copy (FieldByName ('DB_DATOS').AsString,
                     (AnsiPos('.', FieldByName ('DB_DATOS').AsString)+1),
                     Length (FieldByName ('DB_DATOS').AsString));

          if FEditUbicaBaseDatos_DevEx.EditarUbicacion (sServidor, sBaseDatos, H_SIST_BASE_DATOS) then
          begin
                 HabilitaControles;
          end;
     end;
end;

procedure TEditSistBaseDatos_DevEx.btCambiarUsuarioClick(Sender: TObject);
begin
     with dmSistema.cdsSistBaseDatos do
     begin
          if FEditUsuarioBaseDatos_DevEx.EditarUsuario( FieldByName('DB_USRNAME').AsString, H_SIST_BASE_DATOS) then
          begin
                  HabilitaControles;
          end;
     end;
end;

procedure TEditSistBaseDatos_DevEx.FormShow(Sender: TObject);
begin
      inherited;                          
      if (DB_TIPO.Text = ObtieneElemento( lfTipoCompanyName, Ord( tc3Prueba ) )) then
         DB_TIPO.Enabled := FALSE
      else
         DB_TIPO.Enabled := TRUE;
         
      // Campo de Programaciones especiales
      chbEspecial.Checked := FALSE;
      if (dmSistema.cdsSistBaseDatos.FieldByName('DB_ESPC').AsString = K_GLOBAL_SI) then
         chbEspecial.Checked := TRUE;
end;

procedure TEditSistBaseDatos_DevEx.DoLookup;
begin
     inherited;
end;

procedure TEditSistBaseDatos_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;

     with dmSistema.cdsSistBaseDatos do
     begin
          if ( State in [ dsInsert, dsEdit, dsBrowse ] ) then
          begin     
               chbEspecial.Checked := FALSE;
               if (FieldByName('DB_ESPC').AsString = K_GLOBAL_SI) then
                  chbEspecial.Checked := TRUE;
          end;

          if ( State in [ dsBrowse ] ) then
          begin
               if (DB_TIPO.Text = ObtieneElemento( lfTipoCompanyName, Ord( tc3Prueba ) )) then
                  DB_TIPO.Enabled := FALSE
               else
                  DB_TIPO.Enabled := TRUE;
          end
          else if ( Field = nil ) and (State = dsInsert) then
               DB_TIPO.Enabled := TRUE;
     end;
end;

procedure TEditSistBaseDatos_DevEx.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
      if dmSistema.ActualizarBD then
      begin
         dmSistema.cdsSistBaseDatos.Refrescar;
         dmSistema.ActualizarBD := FALSE;
      end;
end;

end.
