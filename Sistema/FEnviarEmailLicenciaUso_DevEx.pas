unit FEnviarEmailLicenciaUso_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls,
     ZBaseDlgModal_DevEx,DEmailService, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, ImgList, cxButtons;

type
  TEnviarEmailLicenciaUso_DevEx = class(TZetaDlgModal_DevEx)
    Panel1: TPanel;
    RemitenteDireccionLBL: TLabel;
    eRemitenteDireccion: TEdit;
    DireccionEnvio: TLabel;
    eDestinatario: TEdit;
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FRemitente: String;
    FRemitenteDireccion: String;
    FDestinatario: String;
    FEmailPrueba: TdmEmailService;
    procedure PosicionaPrimerControl;
    function EnviarEmailPrueba: Boolean;
    procedure SetRemitenteDireccion( lValue: String );
    procedure HabilitarBotones( lHabilitar: Boolean );
  public
    { Public declarations }
    property Remitente: String read FRemitente write FRemitente;
    property RemitenteDireccion: String read FRemitenteDireccion write SetRemitenteDireccion;
    property Destinatario: String read FDestinatario write FDestinatario;
    property EmailPrueba: TdmEmailService read FEmailPrueba write FEmailPrueba;
    procedure ThreadTerminate( Sender: TObject );
  end;

  { *** Threads de Email Service *** }
  TEmailThread = class(TThread)
  private
    { Private declarations }
    FMensajeError : String;
    FFromAddress : String;
    FFromName : String;
    FTimeout : Integer;
    FEmailPruebaTH: TdmEmailService;
    FDestinatario: String;
  public
    constructor Create(CreateSuspended:Boolean; FEmailPrueba: TdmEmailService; sRemitenteDireccion, sRemitente, sDestinatario: String );
    destructor Destroy; override;
    procedure Execute(); override;
    property MensajeError: String read FMensajeError write FMensajeError;
    property FromAddress: String read FFromAddress write FFromAddress;
    property FromName: String read FFromName write FFromName;
    property Destinatario: String read FDestinatario write FDestinatario;
    property Timeout:Integer read FTimeout write FTimeout;
  end;

var
  EnviarEmailLicenciaUso_DevEx: TEnviarEmailLicenciaUso_DevEx;

implementation

uses ZetaDialogo,ZetaCommonTools, ZetaCommonClasses;

{$R *.DFM}

{ TFEnviarEmailLicenciaUso }

procedure TEnviarEmailLicenciaUso_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PosicionaPrimerControl;
end;

procedure TEnviarEmailLicenciaUso_DevEx.OKClick(Sender: TObject);
var
   lOk :Boolean;
   sRemitenteDireccion, sDestinatario: String;
begin
     inherited;
     lOk := False;
     sRemitenteDireccion := eRemitenteDireccion.Text;
     sDestinatario := eDestinatario.Text;
     if StrVacio( sRemitenteDireccion ) then
     begin
          ZError(Caption,'Direcci�n De Correo Electr�nico Del Remitente Vac�a',0);
          ActiveControl := eRemitenteDireccion;
     end
     else if not ValidarEmail( sRemitenteDireccion ) then
     begin
          ZError(Caption,'Por Favor Capture Una Direcci�n De Correo Electr�nico V�lida',0);
          ActiveControl := eRemitenteDireccion;
     end
     else if StrVacio( sDestinatario ) then
     begin
          ZError(Caption,'Direcci�n De Correo Electr�nico Del Destinatario Vac�a',0);
          ActiveControl := eDestinatario;
     end
     else if not ValidarEmail( sDestinatario ) then
     begin
          ZError(Caption,'Por Favor Capture Una Direcci�n De Correo Electr�nico V�lida',0);
          ActiveControl := eDestinatario;
     end
     else
     begin
          lOk := TRUE;
     end;
     if lOk then
     begin
          RemitenteDireccion := eRemitenteDireccion.Text;
          Remitente := VACIO;
          Destinatario := eDestinatario.Text;
          if EnviarEmailPrueba then
             ModalResult := mrOk;
     end;
end;

procedure TEnviarEmailLicenciaUso_DevEx.PosicionaPrimerControl;
begin
     ActiveControl := eRemitenteDireccion;
end;

procedure TEnviarEmailLicenciaUso_DevEx.SetRemitenteDireccion( lValue: String );
begin
     FRemitenteDireccion := lValue;
     if ValidarEmail( lValue ) then
     begin
          eRemitenteDireccion.Text := lValue;
     end
     else
         eRemitenteDireccion.Text := VACIO;
end;

function TEnviarEmailLicenciaUso_DevEx.EnviarEmailPrueba: Boolean;
var
   sMensajeError: String;
   th: TEmailThread;
begin
     Result := FALSE;
     HabilitarBotones( Result );
     Screen.Cursor := crHourGlass;
     Application.ProcessMessages;
     try
        th := TEmailThread.Create(True, FEmailPrueba, RemitenteDireccion, Remitente, Destinatario);
        th.OnTerminate := ThreadTerminate;
        th.Start;
        while WaitForSingleObject(th.Handle, 50) = WAIT_TIMEOUT do
         Application.ProcessMessages;
        sMensajeError := th.MensajeError;
        if th <> nil then
           FreeAndNil( th );
        if StrVacio( sMensajeError ) then
        begin
             ZetaDialogo.zInformation( Caption, 'El correo fue enviado de forma exitosa.', 0 );
             Result := TRUE;
             HabilitarBotones( Result );
             Screen.Cursor := crDefault;
             Application.ProcessMessages;
        end
        else
        begin
             ZetaDialogo.zError( Caption, Format( 'El correo no pudo ser enviado.' + CR_LF + 'Detalle: %s', [ sMensajeError] ), 0 );
             HabilitarBotones( TRUE );
             Screen.Cursor := crDefault;
             Application.ProcessMessages;
        end;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zError( Caption, Format( '%s', [ Error.Message ] ), 0 );
                HabilitarBotones( TRUE );
                Screen.Cursor := crDefault;
                Application.ProcessMessages;
           end;
     end;
end;

procedure TEnviarEmailLicenciaUso_DevEx.HabilitarBotones( lHabilitar: Boolean );
begin
     OK_DevEx.Enabled := lHabilitar;
     Cancelar_DevEx.Enabled := lHabilitar;
end;

constructor TEmailThread.Create(CreateSuspended:Boolean; FEmailPrueba: TdmEmailService; sRemitenteDireccion, sRemitente, sDestinatario: String );
begin
     inherited Create(CreateSuspended);
     Self.FreeOnTerminate := False;
     FMensajeError := VACIO;
     FFromAddress := sRemitenteDireccion;
     FFromName := sRemitente;
     FDestinatario := sDestinatario;
     FTimeout := 0;
     FEmailPruebaTH := FEmailPrueba;
end;

destructor TEmailThread.Destroy;
begin
     inherited;
end;

procedure TEmailThread.Execute();
begin
     inherited;
     try
        while not Self.Terminated do
        begin
             with FEmailPruebaTH do
             begin
                  FromAddress := FFromAddress;//Importante
                  FromName := FFromName;//Importante
                  Timeout := 30;
                  {*** Us 13258: Prueba de Env�o de Correo E-Mail se queda con configuraci�n anterior
                       Es necesario limpiar el TString para que se tome el nuevo destinatario a enviar el correo ***}
                  ToAddress.Clear;
                  ToAddress.Add( FDestinatario );
                  if SendEmail( FMensajeError ) then
                  begin
                       FMensajeError := VACIO;
                  end;
             end;
             Self.Terminate;
        end;
    except
          on Error: Exception do
          begin
               FMensajeError := Error.Message;
               Self.Terminate;
          end;
    end;
end;

procedure TEnviarEmailLicenciaUso_DevEx.ThreadTerminate( Sender: TObject );
begin
     HabilitarBotones( TRUE );
end;

end.
