{$HINTS OFF}
unit FGlobalNotificacionAdvertecia_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, ComCtrls,
     {$ifndef VER130}Variants,{$endif}     
     ZetaNumero, ZetaCommonClasses,
  ZBaseGlobal_DevEx, ZetaCommonLists, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, cxControls, cxContainer, cxEdit, cxCheckBox, ZetaKeyLookup_DevEx,
  Vcl.ImgList, cxButtons, dxBarBuiltInMenu, cxPC, cxGroupBox, dxCheckGroupBox,
  ZetaKeyCombo, dxSkinsCore, TressMorado2013, dxSkinscxPCPainter;

type
 {TEmail}
 TEmail = class( TObject )
    public
    { Public declarations }
    MailServer: String;
    Puerto: Integer;
    User: String;
    PassWord: String;
    AuthMethod: eAuthTressEmail;
    Remitente: String;
    RemitenteDireccion: String;
    Destinatario: String;
    constructor Create;
    destructor Destroy; override;
  end;
  {TGlobalSeguridad_DevEx}
  TGlobalNotificacionAdvertencia_DevEx = class(TBaseGlobal_DevEx)
    PageControl: TcxPageControl;
    TabTimbrado: TcxTabSheet;
    TabEmail: TcxTabSheet;
    GroupBox1: TGroupBox;
    lblServer: TLabel;
    eHost: TEdit;
    lblPuerto: TLabel;
    ePuertoSMTP: TZetaNumero;
    lblAutentificacion: TLabel;
    eAutenticacion: TZetaKeyCombo;
    lblIdUsuario: TLabel;
    eUserId: TEdit;
    lblClave: TLabel;
    eEmailPswd: TEdit;
    ProbarEnvioServerEmail: TcxButton;
    TomarDatosTressEmail: TcxButton;
    TabLicencia: TcxTabSheet;
    eGrupoUsuariosEmail: TZetaKeyLookup_DevEx;
    eMostrarAdvertenciasLicencia: TcxCheckBox;
    eRecibirAdvertenciaPorEmail: TcxCheckBox;
    lblGrupo: TLabel;
    eConsiderarNominaFechaPago: TZetaNumero;
    eConsiderarNominaFechaPagoLBL: TLabel;
    eRecibirAdverNominaSinTimbrar: TCheckBox;
    eSoloNominaOrdinaria: TCheckBox;
    GroupBox2: TGroupBox;
    lblDirectorioReportes: TLabel;
    lblDirectorioImagenes: TLabel;
    lblURLImagenes: TLabel;
    lblExpiracionImagenes: TLabel;
    lblExpiracionImagenesDias: TLabel;
    eDirectorioReportes: TEdit;
    btnDirectorioReportes: TcxButton;
    btnDirectorioImagenes: TcxButton;
    eURLImagenes: TEdit;
    eExpiracionImagenes: TZetaNumero;
    eDirectorioImagenes: TEdit;
    eRecuperarclaveusandoserviciocorreo: TcxCheckBox;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure TomarDatosTressEmailClick(Sender: TObject);
    procedure ProbarEnvioServerEmailClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure eRecibirAdverNominaSinTimbrarClick(Sender: TObject);
    procedure eRecibirAdvertenciaPorEmailPropertiesChange(Sender: TObject);
    procedure btnDirectorioReportesClick(Sender: TObject);
    procedure btnDirectorioImagenesClick(Sender: TObject);
    procedure eHostChange(Sender: TObject);
    procedure ePuertoSMTPChange(Sender: TObject);
  private
    { Private declarations }
    FEmail: TEmail;
    procedure IniciarPruebaEmail;
    procedure ConfigurarEmailPrueba;
    procedure SetConfiguracionEmail;
    procedure ObtenerValoresDefaultEmailGlobalSistema;
    procedure HabilitaControls(const lHabilita:Boolean);
    procedure HabilitaGrupoUsuario(const lHabilita:Boolean);
    procedure HabilitarCheckRecuperarPwd;
  public
    { Public declarations }
    procedure Cargar; override;
    procedure Descargar; override;
  end;

var
  GlobalNotificacionAdvertencia_DevEx: TGlobalNotificacionAdvertencia_DevEx;

implementation

uses ZetaDialogo,
     ZAccesosTress,
     DCliente, DGlobal, ZGlobalTress, dSistema, DEmailService,
     ZetaServerTools, FEnviarEmailLicenciaUso_DevEx, ZetaCommonTools;

const
     K_LIMITE_PASS = 14; {Longitud Maxima del Password}
     K_DEF_MAILSERVER = '255.255.255.255';
     K_DEF_USER = 'Usuario';
     K_DEF_PASSWORD = 'PassWord';
     K_DEF_AUTH = 0; {Metodo de Autentificacion Ninguno Default}
     K_DEF_PORT = 25; {Puerto Defualt de Servidor Email}

{$R *.DFM}

procedure TGlobalNotificacionAdvertencia_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     with dmSistema do
     begin
          cdsUsuarios.Conectar; // (JB) Se agrega Configuracion de Tress Automatiza
          cdsGruposTodos.Conectar;
          eGrupoUsuariosEmail.LookupDataset := cdsGruposTodos;
     end;
     FEmail := TEmail.Create;

     IndexDerechos := ZAccesosTress.D_CAT_CONFI_NOTIFICACIONES;
     HelpContext := H65117_Globales_NotificacionAdvertencia;
     {***US 13038: Timbrado - Enviar notificaci�n a un grupo de usuarios cuando una nomina afectada tiene pendiente el timbrado***}
     eRecibirAdverNominaSinTimbrar.Tag := K_GLOBAL_NOTIFICACION_TIMBRADO_ACTIVO;
     eSoloNominaOrdinaria.Tag :=  K_GLOBAL_NOTIFICACION_TIMBRADO_SOLO_ORDINARIAS;
     eConsiderarNominaFechaPago.Tag := K_GLOBAL_NOTIFICACION_TIMBRADO_DIAS;
     eRecuperarclaveusandoserviciocorreo.Tag := CLAVE_RECUPERAR_PWD_SERVICIO_CORREOS;
     with eConsiderarNominaFechaPago do
     begin
          Mascara := mnDiasNoFraccion;
     end
end;

procedure TGlobalNotificacionAdvertencia_DevEx.FormDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil( EnviarEmailLicenciaUso_DevEx );
  FreeAndNil( FEmail );
  FreeAndNil( dmEmailService );
end;

procedure TGlobalNotificacionAdvertencia_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage :=  TabLicencia;
     HabilitaControls( eRecibirAdverNominaSinTimbrar.Checked );
     HabilitarCheckRecuperarPwd;
end;

procedure TGlobalNotificacionAdvertencia_DevEx.ProbarEnvioServerEmailClick(Sender: TObject);
begin
     if StrVacio( eHost.Text )  then
     begin
          ZetaDialogo.ZError( Caption, 'Servidor de correos no puede quedar vac�o.', 0 );
          eHost.SetFocus;
     end
     else
     begin
          SetConfiguracionEmail;
          ConfigurarEmailPrueba;
          IniciarPruebaEmail;
     end;
end;

procedure TGlobalNotificacionAdvertencia_DevEx.TomarDatosTressEmailClick(Sender: TObject);
begin
     with Global do
     begin
          Conectar;
          eHost.Text := Global.GetGlobalString( K_GLOBAL_EMAIL_HOST );
          ePuertoSMTP.Valor := Global.GetGlobalInteger( K_GLOBAL_EMAIL_PORT );
          eAutenticacion.ItemIndex := Ord( eAuthTressEmail( Global.GetGlobalInteger(K_GLOBAL_EMAIL_AUTH) - 1 ) );
          eUserId.Text := Global.GetGlobalString( K_GLOBAL_EMAIL_USERID );
          eEmailPswd.Text := Global.GetGlobalString( K_GLOBAL_EMAIL_PSWD );
     end;
end;

procedure TGlobalNotificacionAdvertencia_DevEx.btnDirectorioImagenesClick(
  Sender: TObject);
begin
     eDirectorioImagenes.Text := BuscarDirectorio ( eDirectorioImagenes.Text );
end;

procedure TGlobalNotificacionAdvertencia_DevEx.btnDirectorioReportesClick(
  Sender: TObject);
begin
     eDirectorioReportes.Text := BuscarDirectorio ( eDirectorioReportes.Text );
end;

procedure TGlobalNotificacionAdvertencia_DevEx.Cargar;
begin
     inherited;
     with dmCliente do
     begin
          GetNotificacionAdvertencia;
          with DatosNotificacionAdvertencia do
          begin
               // US #9666 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
               eMostrarAdvertenciasLicencia.Checked := not AdvertenciaLicenciaEmpleados;
               // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico.
               eHost.Text := ServidorCorreos;
               ePuertoSMTP.Valor := PuertoSMTP;
               eAutenticacion.ItemIndex := Ord( eAuthTressEmail( AutentificacionCorreo - 1 ) );
               eUserId.Text := UserID;
               eEmailPswd.Text := ZetaServerTools.Decrypt( EmailPSWD );
               eRecibirAdvertenciaPorEmail.Checked := not RecibirAdvertenciaPorEmail;
               eGrupoUsuariosEmail.Valor := GrupoUsuariosEmail;
               HabilitaGrupoUsuario(eRecibirAdvertenciaPorEmail.Checked);
          end;

          // US #14275: Servicio de env�o de correos - Modificaci�n a pantallas de globales.
          GetServicioReportesCorreos;
          with DatosServicioReportesCorreos do
          begin
               eDirectorioReportes.Text := DirectorioReportes;
               eDirectorioImagenes.Text := DirectorioImagenes;
               eURLImagenes.Text := URLImagenes;
               eExpiracionImagenes.Valor := ExpiracionImagenes;
               eRecuperarclaveusandoserviciocorreo.Checked := Recuperarpwsserviciocorreos;
          end;
     end;
end;

procedure TGlobalNotificacionAdvertencia_DevEx.DesCargar;
begin
     inherited Descargar;
     if eRecibirAdvertenciaPorEmail.Checked and ( StrVacio( eHost.Text ) ) then
     begin
          PageControl.ActivePage :=  TabEmail;
          ZetaDialogo.ZError( Caption, 'Es necesario configurar la cuenta de E-mail para enviar las advertencias y notificaciones del sistema.', 0 );
          eHost.SetFocus;
          LastAction := K_EDICION_CANCELAR;
     end
     else if eRecibirAdverNominaSinTimbrar.Checked and ( StrVacio( eHost.Text ) ) then
     begin
          PageControl.ActivePage :=  TabEmail;
          ZetaDialogo.ZError( Caption, 'Es necesario configurar la cuenta de E-mail para enviar las advertencias y notificaciones del sistema.', 0 );
          eHost.SetFocus;
          LastAction := K_EDICION_CANCELAR;
     end
     else
     begin
          try

             dmCliente.SetNotificacionAdvertencia( VarArrayOf( [ // US #9666 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
                                                   not eMostrarAdvertenciasLicencia.Checked, // SEG_MOSTRAR_ADV_LICENCIA
                                                   // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo Electr�nico
                                                   eHost.Text,                                //SEG_SERVIDOR_CORREOS
                                                   ePuertoSMTP.Valor,                         //SEG_PUERTO_SMTP
                                                   eAutenticacion.Valor,                      //SEG_AUTENTIFICACION_CORREO
                                                   eUserId.Text,                              //SEG_USER_ID
                                                   ZetaServerTools.Encrypt( eEmailPswd.Text ),//SEG_EMAIL_PSWD
                                                   not eRecibirAdvertenciaPorEmail.Checked,   //SEG_RECIBIR_ADV_LICENCIA_CORREO
                                                   eGrupoUsuariosEmail.Valor                  //SEG_GRUPO_USUARIO_EMAIL
                                                    ] ) );
             //LastAction := K_EDICION_MODIFICACION;

             // US #14275: Servicio de env�o de correos - Modificaci�n a pantallas de globales.
             dmCliente.SetServicioReportesCorreos(VarArrayOf ( [
                                                             eDirectorioReportes.Text,
                                                             eDirectorioImagenes.Text,
                                                             eURLImagenes.Text,
                                                             eExpiracionImagenes.ValorEntero,
                                                             eRecuperarclaveusandoserviciocorreo.Checked
                                                             ]));
          except
                on Error: Exception do
                begin
                     // ZetaDialogo.zExcepcion( Caption, 'Error al Escribir Valores De Notificaciones y Advertencias', Error, 0 );
                     ZetaDialogo.zExcepcion( Caption, 'Error al escribir valores de notificaciones, advertencias y reportes', Error, 0 );
                end;
          end;
     end;
end;

procedure TGlobalNotificacionAdvertencia_DevEx.eHostChange(Sender: TObject);
begin
  inherited;
            HabilitarCheckRecuperarPwd;
end;

procedure TGlobalNotificacionAdvertencia_DevEx.ePuertoSMTPChange(
  Sender: TObject);
begin
  inherited;
            HabilitarCheckRecuperarPwd;
end;

procedure TGlobalNotificacionAdvertencia_DevEx.eRecibirAdverNominaSinTimbrarClick(
  Sender: TObject);
begin
     inherited;
     HabilitaControls( eRecibirAdverNominaSinTimbrar.Checked );
end;

procedure TGlobalNotificacionAdvertencia_DevEx.eRecibirAdvertenciaPorEmailPropertiesChange(Sender: TObject);
begin
     inherited;
     HabilitaGrupoUsuario(eRecibirAdvertenciaPorEmail.Checked);
end;

procedure TGlobalNotificacionAdvertencia_DevEx.HabilitaGrupoUsuario(const lHabilita:Boolean);
begin
     lblGrupo.Enabled := lHabilita;
     eGrupoUsuariosEmail.Enabled := lHabilita;
end;

procedure TGlobalNotificacionAdvertencia_DevEx.HabilitarCheckRecuperarPwd;
begin
      try
            if (eHost.Text <> vacio) and (ePuertoSMTP.Text <> vacio) and (ePuertoSMTP.Text <> '0') then
            begin
                 eRecuperarclaveusandoserviciocorreo.Enabled := true;

            end
            else
            begin
                 eRecuperarclaveusandoserviciocorreo.Checked := false;
                 eRecuperarclaveusandoserviciocorreo.Enabled := false;
            end;
      finally

      end;
end;

procedure TGlobalNotificacionAdvertencia_DevEx.HabilitaControls(const lHabilita:Boolean);
begin
     eSoloNominaOrdinaria.Enabled := lHabilita;
     eConsiderarNominaFechaPago.Enabled := lHabilita;
     eConsiderarNominaFechaPagoLBL.Enabled := lHabilita;
end;

procedure TGlobalNotificacionAdvertencia_DevEx.IniciarPruebaEmail;
begin
     try
        if not Assigned( EnviarEmailLicenciaUso_DevEx ) then
           EnviarEmailLicenciaUso_DevEx := TEnviarEmailLicenciaUso_DevEx.Create( Self );
        with EnviarEmailLicenciaUso_DevEx do
        begin
             EmailPrueba := dmEmailService;
             RemitenteDireccion := FEmail.User;
             ShowModal;
        end;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zError( Caption, Format( '%s', [ Error.Message ] ), 0 );
           end;
     end;
end;

procedure TGlobalNotificacionAdvertencia_DevEx.ConfigurarEmailPrueba;
const
     K_MENSAJE_SUBJECT = 'Correo de prueba de Sistema TRESS';
var
   Puerto: Integer;
   sMensajeError : String;
   sMensajePrueba: UTF8String;
begin
     try
        sMensajePrueba := 'Estimado usuario(a): esto es una prueba de Sistema TRESS, tu configuraci&oacute;n es correcta!.';
        if not Assigned( dmEmailService ) then
           dmEmailService := TdmEmailService.Create( self );
        with dmEmailService do
        begin
             NewEMail;
             MailServer := FEmail.MailServer;
             Port := FEmail.Puerto;
             if Port = 0 then //Definir Puerto Default SMTP 25
                Port := K_DEF_PORT;
             User := FEmail.User;
             Password := FEmail.PassWord;
             AuthMethod :=  FEmail.AuthMethod;
             SubType := emtHTML;
             Subject := K_MENSAJE_SUBJECT;
             TimeOut := 30;
             MessageText.Clear;
             MessageText.Add ( string(sMensajePrueba) );
        end;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zError( Caption, Format( '%s', [ Error.Message ] ), 0 );
           end;
     end;
end;

procedure TGlobalNotificacionAdvertencia_DevEx.SetConfiguracionEmail;
begin
     FEmail.MailServer := eHost.Text;
     FEmail.Puerto := Trunc( ePuertoSMTP.Valor);
     FEmail.User := eUserId.Text;
     FEmail.PassWord := eEmailPswd.Text;
     FEmail.AuthMethod := eAuthTressEmail( eAutenticacion.ItemIndex );
end;

{ TEmail }
constructor TEmail.Create;
begin
     inherited Create;
     MailServer := K_DEF_MAILSERVER;
     Puerto := K_DEF_PORT;
     User := K_DEF_USER;
     PassWord := K_DEF_PASSWORD;
     AuthMethod := eAuthTressEmail( K_DEF_AUTH );
     Remitente := VACIO;
     RemitenteDireccion := VACIO;
     Destinatario := VACIO;
end;

procedure TGlobalNotificacionAdvertencia_DevEx.ObtenerValoresDefaultEmailGlobalSistema;
begin
     with dmCliente do
     begin
          GetNotificacionAdvertencia;
          with DatosNotificacionAdvertencia do
          begin
               eHost.Text := ServidorCorreos;
               ePuertoSMTP.Valor := PuertoSMTP;
               eAutenticacion.ItemIndex := Ord( eAuthTressEmail( AutentificacionCorreo - 1 ) );
               eUserId.Text := UserID;
               eEmailPswd.Text := ZetaServerTools.Decrypt( EmailPSWD );
               eGrupoUsuariosEmail.Valor := GrupoUsuariosEmail;
          end;
     end;
end;

destructor TEmail.Destroy;
begin
     inherited Destroy;
end;

end.
