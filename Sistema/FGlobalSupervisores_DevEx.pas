unit FGlobalSupervisores_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     ZBaseGlobal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, ImgList, cxButtons, Vcl.Mask,
  ZetaFecha;

type
  TGlobalSupervisores_DevEx = class(TBaseGlobal_DevEx)
    Label1: TLabel;
    NivelesCB: TComboBox;
    Label2: TLabel;
    Inactividad: TEdit;
    Label3: TLabel;
    Penalizacion: TEdit;
    Enrolamiento: TCheckBox;
    lbCosteo: TLabel;
    CosteoNivelesCB: TComboBox;
    FechaEvaluacionDiaria: TZetaFecha;
    FechaEvaluacionDiariaLBL: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    //procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    FNivelAnterior: Integer;
    FUsarEnrolamientoAnterior : Boolean;
  public
    procedure Descargar; override;
    procedure LlenaNListaNiveles;
  end;

var
  GlobalSupervisores_DevEx: TGlobalSupervisores_DevEx;

implementation

{$R *.DFM}

uses DGlobal,
     ZAccesosTress,
     ZGlobalTress,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo, DBaseGlobal, DCliente;

procedure TGlobalSupervisores_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos     := ZAccesosTress.D_CAT_CONFI_SUPERVISORES;
     NivelesCB.Tag     := K_GLOBAL_NIVEL_SUPERVISOR;
     Inactividad.Tag   := K_SUPER_SEGUNDOS;
     Penalizacion.Tag  := K_GLOBAL_SUPER_PENALIZACION;
     Enrolamiento.Tag  := K_GLOBAL_ENROLAMIENTO_SUPERVISORES;
     CosteoNivelesCB.Tag := K_GLOBAL_NIVEL_COSTEO;
     CosteoNivelesCB.Visible := dmCliente.ModuloAutorizadoCosteo;
     lbCosteo.Visible := CosteoNivelesCB.Visible;
     {$ifdef COMMSCOPE}
     FechaEvaluacionDiaria.Visible := False;
     FechaEvaluacionDiariaLBL.Visible := False;
     {$endif}

     LlenaNListaNiveles;
     HelpContext := H60112_Globales_de_Supervisores;
     {$ifdef DOS_CAPAS}
     Enrolamiento.Visible := False;
     Enrolamiento.Enabled := False;     
     {$endif}

end;

procedure TGlobalSupervisores_DevEx.FormShow(Sender: TObject);
begin
     {$ifdef COMMSCOPE}
     Self.Height := 250;
     FechaEvaluacionDiaria.Visible := True;
     FechaEvaluacionDiariaLBL.Visible := True;
     FechaEvaluacionDiaria.Tag := K_GLOBAL_FECHA_EVALUACION_DIARIA;
     {$else}
     FechaEvaluacionDiaria.Visible := False;
     FechaEvaluacionDiariaLBL.Visible := False;
     {$endif}

     inherited;
     FNivelAnterior := Global.GetGlobalInteger( K_GLOBAL_NIVEL_SUPERVISOR );
     FUsarEnrolamientoAnterior := Global.GetGlobalBooleano( K_GLOBAL_ENROLAMIENTO_SUPERVISORES );
end;

procedure TGlobalSupervisores_DevEx.DesCargar;
var
   iNivelNuevo: Integer;
{$ifndef DOS_CAPAS}
   bUsarEnrolamientoNuevo : Boolean;
   iCosteoAnterior: Integer;
{$endif}
begin
{$ifndef DOS_CAPAS}
     iCosteoAnterior := Global.GetGlobalInteger( K_GLOBAL_NIVEL_COSTEO );
{$endif}
     inherited Descargar;               // Procede a Grabar y poner el LastAction en Modificación
     if ( LastAction = K_EDICION_MODIFICACION ) then
     begin
          iNivelNuevo := Global.GetGlobalInteger( K_GLOBAL_NIVEL_SUPERVISOR );
{$ifndef DOS_CAPAS}
          bUsarEnrolamientoNuevo :=  Global.GetGlobalBooleano( K_GLOBAL_ENROLAMIENTO_SUPERVISORES );
{$endif}
          if ( iNivelNuevo > 0 ) and ( FNivelAnterior <> iNivelNuevo ) then
             ZInformation( Self.Caption, 'Se Requiere Salir de Sistema Tress Para que el Programa' + CR_LF +
                                         'Supervisores Refleje el Cambio en el Nivel de Organigrama', 0 )
{$ifdef DOS_CAPAS};
{$else}
          else if (iCosteoAnterior <> Global.GetGlobalInteger( K_GLOBAL_NIVEL_COSTEO )) then
          begin
               ZInformation( Self.Caption, 'Se requiere salir de Sistema Tress para reflejar ' + CR_LF +
                                           'el cambio en el Nivel de Costeo', 0 )
          end
          else
          begin
               if ( bUsarEnrolamientoNuevo <> FUsarEnrolamientoAnterior ) then
                  ZInformation( Self.Caption, 'Se Requiere Salir de Sistema Tress para que se ' + CR_LF +
                                             'reflejen los cambios al uso de Enrolamiento de Supervisores', 0 )
          end;
{$endif}
     end;
end;

procedure TGlobalSupervisores_DevEx.LlenaNListaNiveles;
 procedure PreparaCombo( oCombo : TComboBox );
 begin
      with oCombo.Items do
      begin
           BeginUpdate;
           Clear;
           Add( '<Indefinido>' );
      end;
 end;

 procedure DesPreparaCombo( oCombo : TComboBox );
 begin
      oCombo.Items.EndUpdate;
 end;

var
   i: Integer;
begin
     try
        PreparaCombo( NivelesCB );
        PreparaCombo( CosteoNivelesCB  );

        for i := 1 to Global.NumNiveles do
        begin
             NivelesCB.Items.Add( Global.NombreNivel( i ) );
             CosteoNivelesCB.Items.Add( Global.NombreNivel( i ) );
        end;
     finally
            DesPreparaCombo( NivelesCB );
            DesPreparaCombo( CosteoNivelesCB  );
     end;
end;

{
procedure TGlobalSupervisores.OKClick(Sender: TObject);
var
   iNivelNuevo: Integer;
begin
     inherited;
     try
        with Global do
        begin
             iNivelNuevo := GetGlobalInteger( K_GLOBAL_NIVEL_SUPERVISOR );
             if ( iNivelNuevo > 0 ) and ( FNivelAnterior <> iNivelNuevo ) then
             begin
                  //CambiaSPSupervisores;
                  ZInformation( Self.Caption, 'Se Requiere Salir de Sistema Tress para que el programa' +
                                              CR_LF +
                                              'Supervisores Refleje el cambio en el nivel de Organigrama', 0 );
             end;
        end;
     except
           on Error: Exception do
           begin
                zExcepcion( Caption, 'Error Al Modificar Stored Procedure SP_FECHA_NIVEL', Error, 0 );
           end;
     end;
end;
}

end.
