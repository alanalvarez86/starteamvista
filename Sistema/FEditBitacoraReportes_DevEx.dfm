inherited EditBitacoraReportes_DevEx: TEditBitacoraReportes_DevEx
  Left = 489
  Top = 185
  BorderIcons = [biSystemMenu, biMaximize]
  BorderStyle = bsSizeable
  Caption = 'Registro de bit'#225'cora de reportes email'
  ClientHeight = 406
  ClientWidth = 438
  OldCreateOrder = True
  OnClose = FormClose
  ExplicitWidth = 454
  ExplicitHeight = 445
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 370
    Width = 438
    BevelOuter = bvNone
    ExplicitTop = 370
    ExplicitWidth = 438
    DesignSize = (
      438
      36)
    inherited OK_DevEx: TcxButton
      Left = 351
      Top = 6
      Cancel = True
      ExplicitLeft = 351
      ExplicitTop = 6
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 431
      Top = 6
      Cancel = False
      Visible = False
      ExplicitLeft = 431
      ExplicitTop = 6
    end
    object DevEx_cxDBNavigatorEdicion: TcxDBNavigator
      Left = 8
      Top = 7
      Width = 94
      Height = 25
      Buttons.CustomButtons = <>
      Buttons.First.Hint = 'Primero'
      Buttons.First.Visible = True
      Buttons.PriorPage.Enabled = False
      Buttons.PriorPage.Visible = False
      Buttons.Prior.Hint = 'Anterior'
      Buttons.Prior.Visible = True
      Buttons.Next.Hint = 'Siguiente'
      Buttons.Next.Visible = True
      Buttons.NextPage.Enabled = False
      Buttons.NextPage.Visible = False
      Buttons.Last.Hint = #218'ltimo'
      Buttons.Insert.Enabled = False
      Buttons.Insert.Visible = False
      Buttons.Append.Enabled = False
      Buttons.Delete.Enabled = False
      Buttons.Delete.Visible = False
      Buttons.Edit.Enabled = False
      Buttons.Edit.Visible = False
      Buttons.Post.Enabled = False
      Buttons.Post.Visible = False
      Buttons.Cancel.Enabled = False
      Buttons.Cancel.Visible = False
      Buttons.Refresh.Enabled = False
      Buttons.Refresh.Visible = False
      Buttons.SaveBookmark.Enabled = False
      Buttons.SaveBookmark.Visible = False
      Buttons.GotoBookmark.Enabled = False
      Buttons.GotoBookmark.Visible = False
      Buttons.Filter.Enabled = False
      Buttons.Filter.Visible = False
      DataSource = DataSource
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 0
    Width = 438
    Height = 219
    Align = alTop
    BevelOuter = bvNone
    Constraints.MinWidth = 438
    TabOrder = 1
    object FechaLBL: TLabel
      Left = 31
      Top = 83
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha:'
    end
    object BI_FECHA: TZetaDBTextBox
      Left = 67
      Top = 82
      Width = 109
      Height = 17
      AutoSize = False
      Caption = 'BI_FECHA'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'BI_FECHA'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object HoraLBL: TLabel
      Left = 38
      Top = 101
      Width = 26
      Height = 13
      Alignment = taRightJustify
      Caption = 'Hora:'
    end
    object BI_HORA: TZetaDBTextBox
      Left = 67
      Top = 100
      Width = 109
      Height = 17
      AutoSize = False
      Caption = 'BI_HORA'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'BI_HORA'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object TipoLBL: TLabel
      Left = 40
      Top = 119
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo:'
    end
    object BI_TIPO: TZetaDBTextBox
      Left = 67
      Top = 118
      Width = 109
      Height = 17
      AutoSize = False
      Caption = 'BI_TIPO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'BI_TIPO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object MensajeLBL: TLabel
      Left = 5
      Top = 136
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci'#243'n:'
    end
    object Imagen: TImage
      Left = 380
      Top = 33
      Width = 34
      Height = 34
      Picture.Data = {
        07544269746D6170360C0000424D360C00000000000036000000280000002000
        0000200000000100180000000000000C0000C30E0000C30E0000000000000000
        0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFDBF4FC9FE2F69FE2F69FE2F69FE2F69FE2F69FE2F69FE2F6
        9FE2F69FE2F69FE2F69FE2F69FE2F69FE2F69FE2F69FE2F69FE2F69FE2F69FE2
        F69FE2F69FE2F69FE2F69FE2F69FE2F69FE2F69FE2F69FE2F6CFF0FBFFFFFFFF
        FFFFFFFFFFFFFFFF20BCEC00B2E900B2E900B2E900B2E900B2E900B2E900B2E9
        00B2E900B2E900B2E900B2E900B2E900B2E900B2E900B2E900B2E900B2E900B2
        E900B2E900B2E900B2E900B2E900B2E900B2E900B2E900B2E920BCECFFFFFFFF
        FFFFFFFFFFFFFFFF78D7F400B2E900B2E900B2E900B2E900B2E900B2E900B2E9
        00B2E900B2E900B2E900B2E900B2E900B2E900B2E900B2E900B2E900B2E900B2
        E900B2E900B2E900B2E900B2E900B2E900B2E900B2E900B2E958CDF1FFFFFFFF
        FFFFFFFFFFFFFFFFEFFAFE18B9EB00B2E900B2E900B2E900B2E900B2E900B2E9
        00B2E900B2E900B2E900B2E960CFF27FD8F47FD8F460CFF200B2E900B2E900B2
        E900B2E900B2E900B2E900B2E900B2E900B2E900B2E908B4EADFF5FCFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF9BE1F600B2E900B2E900B2E900B2E900B2E900B2E9
        00B2E900B2E900B2E900B2E9BFECF9FFFFFFFFFFFFBFECF900B2E900B2E900B2
        E900B2E900B2E900B2E900B2E900B2E900B2E900B2E97CD8F4FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFBFEFF30C1ED00B2E900B2E900B2E900B2E900B2E9
        00B2E900B2E900B2E900B2E9BFECF9FFFFFFFFFFFFBFECF900B2E900B2E900B2
        E900B2E900B2E900B2E900B2E900B2E900B2E918B9EBF3FBFEFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF900B2E900B2E900B2E900B2E900B2E9
        00B2E900B2E900B2E900B2E948C8EF60CFF260CFF248C8EF00B2E900B2E900B2
        E900B2E900B2E900B2E900B2E900B2E900B2E99FE2F6FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF54CCF100B2E900B2E900B2E900B2E9
        00B2E900B2E900B2E900B2E900B2E900B2E900B2E900B2E900B2E900B2E900B2
        E900B2E900B2E900B2E900B2E900B2E934C2EEFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDBF4FC08B4EA00B2E900B2E900B2E9
        00B2E900B2E900B2E900B2E948C8EF60CFF260CFF248C8EF00B2E900B2E900B2
        E900B2E900B2E900B2E900B2E900B2E9C3EDFAFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF78D7F400B2E900B2E900B2E9
        00B2E900B2E900B2E900B2E9BFECF9FFFFFFFFFFFFBFECF900B2E900B2E900B2
        E900B2E900B2E900B2E900B2E958CDF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFAFE18B9EB00B2E900B2E9
        00B2E900B2E900B2E900B2E9BFECF9FFFFFFFFFFFFBFECF900B2E900B2E900B2
        E900B2E900B2E900B2E908B4EADFF5FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9BE1F600B2E900B2E9
        00B2E900B2E900B2E900B2E9BFECF9FFFFFFFFFFFFBFECF900B2E900B2E900B2
        E900B2E900B2E900B2E97CD8F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFEFF30C1ED00B2E9
        00B2E900B2E900B2E900B2E9BFECF9FFFFFFFFFFFFBFECF900B2E900B2E900B2
        E900B2E900B2E918B9EBF3FBFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF900B2E9
        00B2E900B2E900B2E900B2E9BFECF9FFFFFFFFFFFFBFECF900B2E900B2E900B2
        E900B2E900B2E99FE2F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4CC9F0
        00B2E900B2E900B2E900B2E9BFECF9FFFFFFFFFFFFBFECF900B2E900B2E900B2
        E900B2E930C1EDFBFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDBF4FC
        04B3E900B2E900B2E900B2E9BFECF9FFFFFFFFFFFFBFECF900B2E900B2E900B2
        E900B2E9BFECF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        74D5F300B2E900B2E900B2E9BFECF9FFFFFFFFFFFFBFECF900B2E900B2E900B2
        E954CCF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        EFFAFE14B8EB00B2E900B2E948C8EF60CFF260CFF248C8EF00B2E900B2E908B4
        EADBF4FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF97DFF600B2E900B2E900B2E900B2E900B2E900B2E900B2E900B2E978D7
        F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFBFEFF2CBFED00B2E900B2E900B2E900B2E900B2E900B2E918B9EBEFFA
        FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFBBEAF900B2E900B2E900B2E900B2E900B2E900B2E99BE1F6FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF4CC9F000B2E900B2E900B2E900B2E930C1EDFBFEFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFDBF4FC04B3E900B2E900B2E900B2E9BFECF9FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF74D5F300B2E900B2E954CCF1FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFEFFAFE14B8EB0CB6EADBF4FCFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFAFEE7F8FDFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF}
      Visible = False
    end
    object CalendarioLBL: TLabel
      Left = 11
      Top = 11
      Width = 53
      Height = 13
      Alignment = taRightJustify
      Caption = 'Calendario:'
    end
    object CA_NOMBRE: TZetaDBTextBox
      Left = 67
      Top = 10
      Width = 347
      Height = 17
      AutoSize = False
      Caption = 'CA_NOMBRE'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CA_NOMBRE'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object EmpresaLBL: TLabel
      Left = 20
      Top = 29
      Width = 44
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empresa:'
    end
    object CM_CODIGO: TZetaDBTextBox
      Left = 67
      Top = 28
      Width = 109
      Height = 17
      AutoSize = False
      Caption = 'CM_CODIGO'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object ReporteLBL: TLabel
      Left = 23
      Top = 47
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Caption = 'Reporte:'
    end
    object CA_REPORT: TZetaDBTextBox
      Left = 67
      Top = 46
      Width = 109
      Height = 17
      AutoSize = False
      Caption = 'CA_REPORT'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CA_REPORT'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object FrecuenciaLBL: TLabel
      Left = 8
      Top = 65
      Width = 56
      Height = 13
      Alignment = taRightJustify
      Caption = 'Frecuencia:'
    end
    object CA_FREC: TZetaDBTextBox
      Left = 67
      Top = 64
      Width = 109
      Height = 17
      AutoSize = False
      Caption = 'CA_FREC'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CA_FREC'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object BI_TEXTO: TcxDBMemo
      Left = 67
      Top = 137
      DataBinding.DataField = 'BI_TEXTO'
      DataBinding.DataSource = DataSource
      ParentFont = False
      Properties.ReadOnly = True
      Properties.ScrollBars = ssVertical
      Style.Font.Charset = ANSI_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -12
      Style.Font.Name = 'Courier New'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 0
      Height = 63
      Width = 347
    end
  end
  object BI_DATA: TcxDBMemo [2]
    Left = 0
    Top = 219
    Align = alClient
    DataBinding.DataField = 'BI_DATA'
    DataBinding.DataSource = DataSource
    ParentFont = False
    Properties.ReadOnly = True
    Properties.ScrollBars = ssVertical
    Style.Font.Charset = ANSI_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -12
    Style.Font.Name = 'Courier New'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 2
    ExplicitTop = 192
    ExplicitHeight = 178
    Height = 151
    Width = 438
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 23068904
  end
  object DataSource: TDataSource
    Left = 128
    Top = 353
  end
end
