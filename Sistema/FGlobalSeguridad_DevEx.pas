unit FGlobalSeguridad_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, ComCtrls,
     {$ifndef VER130}Variants,{$endif}     
     ZetaNumero, ZetaCommonClasses,
  ZBaseGlobal_DevEx, ZetaCommonLists, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, cxControls, cxContainer, cxEdit, cxCheckBox, ZetaKeyLookup_DevEx,
  Vcl.ImgList, cxButtons, dxBarBuiltInMenu, cxPC, cxGroupBox, dxCheckGroupBox,
  ZetaKeyCombo, dxSkinsCore, TressMorado2013, dxSkinscxPCPainter;

type
  {TGlobalSeguridad_DevEx}
  TGlobalSeguridad_DevEx = class(TBaseGlobal_DevEx)
    Panel1: TPanel;
    eAlmacenarPass: TZetaNumero;
    eAlmacenarPassLBL: TLabel;
    eAlmacenarPassPFX: TLabel;
    eDiasInactivos: TZetaNumero;
    eDiasInactivosLBL: TLabel;
    eDiasInactivosPFX: TLabel;
    eExpiracion: TZetaNumero;
    eExpiracionLBL: TLabel;
    eExpiracionPFX: TLabel;
    eIntentos: TZetaNumero;
    eIntentosLBL: TLabel;
    eIntentosPFX: TLabel;
    eLimitePass: TZetaNumero;
    eLimitePassLBL: TLabel;
    eLimitePassPFX: TLabel;
    eMinDigitosPass: TZetaNumero;
    eMinDigitosPassLBL: TLabel;
    eMinDigitosPassPFX: TLabel;
    eMinLetrasPass: TZetaNumero;
    eMinLetrasPassLBL: TLabel;
    eMinLetrasPassPFX: TLabel;
    eTiempoInactivo: TZetaNumero;
    eTiempoInactivoLBL: TLabel;
    eTiempoInactivoPFX: TLabel;
    eUsuarioTareasAutomaticas: TZetaKeyLookup_DevEx;
    lblUsuarioTareasAutomaticas: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure eUsuarioTareasAutomaticasExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Cargar; override;
    procedure Descargar; override;
  end;

var
  GlobalSeguridad_DevEx: TGlobalSeguridad_DevEx;

implementation

uses ZetaDialogo,
     ZAccesosTress,
     DCliente, DGlobal, ZGlobalTress, dSistema,
     ZetaServerTools, FEnviarEmailLicenciaUso_DevEx, ZetaCommonTools;

const
     K_LIMITE_PASS = 14; {Longitud Maxima del Password}

{$R *.DFM}

procedure TGlobalSeguridad_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     with dmSistema do
     begin
          cdsUsuarios.Conectar; // (JB) Se agrega Configuracion de Tress Automatiza
          cdsGruposTodos.Conectar;
          eUsuarioTareasAutomaticas.LookupDataset  := cdsUsuarios;
     end;


     {$ifdef TRESS}
     IndexDerechos := ZAccesosTress.D_CAT_CONFI_SEGURIDAD;
     {$else}
     IndexDerechos := ZAccesosTress.D_CAT_CONFI_GLOBALES;
     {$endif}
     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_GE_SEGURIDAD;
     {$else}
     HelpContext := H65113_Globales_De_Seguridad;
     {$endif}
end;

procedure TGlobalSeguridad_DevEx.Cargar;
begin
     with dmCliente do
     begin
          GetSeguridad;
          with DatosSeguridad do
          begin
               eExpiracion.Valor := PasswordExpiracion;
               eLimitePass.Valor := PasswordLongitud;
               eMinLetrasPass.Valor := PasswordLetras;
               eMinDigitosPass.Valor := PasswordDigitos;
               eAlmacenarPass.Valor := PasswordLog;
               eIntentos.Valor := PasswordIntentos;
               eDiasInactivos.Valor := Inactividad;
               eTiempoInactivo.Valor := TimeOut;
               eUsuarioTareasAutomaticas.Valor := UsuarioTareasAutomaticas; // (JB) Se agrega Configuracion de Tress Automatiza
          end;
     end;
end;

procedure TGlobalSeguridad_DevEx.DesCargar;
var
   iLimite: Integer;
begin
     iLimite := eLimitePass.ValorEntero;
     if ( iLimite > K_LIMITE_PASS ) then
     begin
          ZetaDialogo.zError( Caption, Format( 'La Longitud M�xima De La Clave Es De %d Caracteres', [ K_LIMITE_PASS ] ), 0 );
          with eLimitePass do
          begin
               Valor := K_LIMITE_PASS;
               SetFocus;
          end;
     end
     else if ( iLimite < ( eMinLetrasPass.Valor + eMinDigitosPass.Valor ) ) then
     begin
          ZetaDialogo.zError( Caption, Format( 'La Cantidad de Letras y D�gitos debe Ser Igual o Menor que Longitud M�nima de la Clave: ', [ iLimite ] ), 0 );
          eMinLetrasPass.SetFocus;
     end
     else if eUsuarioTareasAutomaticas.Valor < 1 then
     begin
          ZetaDialogo.ZError( Caption, 'Usuario Tareas Autom�ticas no puede quedar vac�o.', 0 );
          eUsuarioTareasAutomaticas.SetFocus;
     end
     else
     begin
          try

             dmCliente.SetSeguridad( VarArrayOf( [ eExpiracion.Valor,                    //SEG_VENCIMIENTO
                                                   eLimitePass.Valor,                    //SEG_LIMITE_PASSWORD
                                                   eIntentos.Valor,                      //SEG_INTENTOS
                                                   eDiasInactivos.Valor,                 //SEG_DIAS_INACTIVOS
                                                   eTiempoInactivo.Valor,                //SEG_TIEMPO_INACTIVO
                                                   eMinLetrasPass.Valor,                 //SEG_MIN_LETRAS_PASSWORD
                                                   eMinDigitosPass.Valor,                //SEG_MIN_DIGITOS_PASSWORD
                                                   eAlmacenarPass.Valor,                 //SEG_MAX_LOG_PASSWORD
                                                   eUsuarioTareasAutomaticas.Valor      //SEG_USUARIO_TRESSAUTOMATIZA // (JB) Se agrega Configuracion de Tress Automatiza
                                                    ] ) );
             LastAction := K_EDICION_MODIFICACION;
          except
                on Error: Exception do
                begin
                     ZetaDialogo.zExcepcion( Caption, 'Error al Escribir Valores De Seguridad', Error, 0 );
                end;
          end;
     end;
end;

procedure TGlobalSeguridad_DevEx.eUsuarioTareasAutomaticasExit(Sender: TObject);
begin
     if eUsuarioTareasAutomaticas.Valor < 1 then
     begin
          ZetaDialogo.ZError( Caption, 'Usuario Tareas Autom�ticas no puede quedar vac�o.', 0 );
          eUsuarioTareasAutomaticas.SetFocus;
     end
end;

end.
