inherited MensajesPorTerminal_DevEx: TMensajesPorTerminal_DevEx
  Caption = 'Mensajes por Terminal'
  ClientHeight = 235
  ExplicitHeight = 235
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    ExplicitWidth = 0
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 359
        ExplicitLeft = 279
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 624
    Height = 38
    Align = alTop
    TabOrder = 1
    ExplicitWidth = 0
    object Label1: TLabel
      Left = 18
      Top = 12
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Terminal:'
    end
    object luTerminal: TZetaKeyLookup_DevEx
      Left = 65
      Top = 8
      Width = 300
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidKey = luTerminalValidKey
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 57
    Height = 178
    TabOrder = 2
    ExplicitTop = 57
    ExplicitWidth = 0
    ExplicitHeight = 171
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object DM_CODIGO: TcxGridDBColumn
        Caption = 'ID Mensaje'
        DataBinding.FieldName = 'DM_CODIGO'
      end
      object DM_MENSAJE: TcxGridDBColumn
        Caption = 'Mensaje'
        DataBinding.FieldName = 'DM_MENSAJE'
      end
      object TM_SLEEP: TcxGridDBColumn
        Caption = 'Espera'
        DataBinding.FieldName = 'TM_SLEEP'
      end
      object TM_NEXTXHR: TcxGridDBColumn
        Caption = 'Por hora espec'#237'fica'
        DataBinding.FieldName = 'TM_NEXTXHR'
      end
      object TM_NEXTHOR: TcxGridDBColumn
        Caption = 'Hora espec'#237'fica'
        DataBinding.FieldName = 'TM_NEXTHOR'
      end
      object TM_NEXT: TcxGridDBColumn
        Caption = 'Siguiente mensaje'
        DataBinding.FieldName = 'TM_NEXT'
      end
      object TM_ULTIMA: TcxGridDBColumn
        Caption = #218'ltima Ejecuci'#243'n'
        DataBinding.FieldName = 'TM_ULTIMA'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 536
    Top = 24
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
    DesignInfo = 1573344
  end
  inherited ActionList: TActionList
    Left = 442
    Top = 24
  end
  inherited PopupMenu1: TPopupMenu
    Left = 408
    Top = 24
  end
end
