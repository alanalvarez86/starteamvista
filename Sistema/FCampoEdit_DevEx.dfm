inherited CampoEdit_DevEx: TCampoEdit_DevEx
  Left = 357
  Top = 371
  ActiveControl = edLetrero
  Caption = 'Propiedades del Campo'
  ClientHeight = 150
  ClientWidth = 403
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LetreroLBL: TLabel [0]
    Left = 60
    Top = 14
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = '&Descripci'#243'n:'
    FocusControl = edLetrero
  end
  object FormulaLBL: TLabel [1]
    Left = 4
    Top = 40
    Width = 115
    Height = 13
    Alignment = taRightJustify
    Caption = '&Valor Default al Agregar:'
    FocusControl = FormulaTexto
  end
  object lblExcede: TLabel [2]
    Left = 75
    Top = 91
    Width = 249
    Height = 13
    Caption = 'El N'#250'mero De Controles Exceden El Espacio Vertical'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  inherited PanelBotones: TPanel
    Top = 114
    Width = 403
    TabOrder = 7
    inherited OK_DevEx: TcxButton
      Left = 236
      Top = 5
      OnClick = OK_DevExClick
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 316
      Top = 5
    end
  end
  object edLetrero: TEdit [4]
    Left = 122
    Top = 10
    Width = 273
    Height = 21
    HelpContext = 101
    TabOrder = 0
  end
  object FormulaTexto: TEdit [5]
    Left = 122
    Top = 36
    Width = 273
    Height = 21
    HelpContext = 101
    TabOrder = 1
  end
  object FormulaNumero: TZetaNumero [6]
    Left = 122
    Top = 36
    Width = 130
    Height = 21
    HelpContext = 101
    Mascara = mnNumeroGlobal
    TabOrder = 2
    Text = '0.00'
  end
  object FormulaBool: TCheckBox [7]
    Left = 122
    Top = 38
    Width = 15
    Height = 17
    HelpContext = 101
    Caption = '                        '
    TabOrder = 3
  end
  object FormulaFecha: TZetaFecha [8]
    Left = 122
    Top = 36
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 4
    Text = '22/sep/04'
    Valor = 38252.000000000000000000
  end
  object FormulaTabla: TZetaKeyLookup_DevEx [9]
    Left = 122
    Top = 36
    Width = 273
    Height = 21
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 5
    TabStop = True
    WidthLlave = 60
  end
  object chObligatorio: TCheckBox [10]
    Left = 64
    Top = 61
    Width = 71
    Height = 17
    Alignment = taLeftJustify
    Caption = 'O&bligatorio: '
    TabOrder = 6
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
