inherited GlobalConteo_DevEx: TGlobalConteo_DevEx
  Left = 275
  Top = 258
  Caption = 'Configuraci'#243'n de Presupuesto'
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object GBNiveles: TGroupBox [1]
    Left = 7
    Top = 3
    Width = 361
    Height = 153
    Caption = ' Criterios a Utilizar '
    TabOrder = 1
    object lbCriterio1: TLabel
      Left = 18
      Top = 25
      Width = 57
      Height = 13
      Alignment = taRightJustify
      Caption = 'Criterio #&1 : '
      FocusControl = Nivel1
    end
    object lbCriterio2: TLabel
      Left = 18
      Top = 49
      Width = 57
      Height = 13
      Alignment = taRightJustify
      Caption = 'Criterio #&2 : '
      FocusControl = Nivel2
    end
    object lbCriterio3: TLabel
      Left = 18
      Top = 73
      Width = 57
      Height = 13
      Alignment = taRightJustify
      Caption = 'Criterio #&3 : '
      FocusControl = Nivel3
    end
    object lbCriterio4: TLabel
      Left = 18
      Top = 97
      Width = 57
      Height = 13
      Alignment = taRightJustify
      Caption = 'Criterio #&4 : '
      FocusControl = Nivel4
    end
    object lbCriterio5: TLabel
      Left = 18
      Top = 121
      Width = 57
      Height = 13
      Alignment = taRightJustify
      Caption = 'Criterio #&5 : '
      FocusControl = Nivel5
    end
    object Nivel1: TComboBox
      Left = 78
      Top = 21
      Width = 267
      Height = 21
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 0
      OnChange = NivelesChange
    end
    object Nivel2: TComboBox
      Left = 78
      Top = 45
      Width = 267
      Height = 21
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      Enabled = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 1
      OnChange = NivelesChange
    end
    object Nivel3: TComboBox
      Left = 78
      Top = 69
      Width = 267
      Height = 21
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      Enabled = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 2
      OnChange = NivelesChange
    end
    object Nivel4: TComboBox
      Left = 78
      Top = 93
      Width = 267
      Height = 21
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      Enabled = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 3
      OnChange = NivelesChange
    end
    object Nivel5: TComboBox
      Left = 78
      Top = 117
      Width = 267
      Height = 21
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      Enabled = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 4
      OnChange = NivelesChange
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
