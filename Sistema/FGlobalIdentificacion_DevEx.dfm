inherited GlobalIdentificacion_DevEx: TGlobalIdentificacion_DevEx
  ActiveControl = RazonSocial
  Caption = 'Identificaci'#243'n'
  ClientHeight = 308
  ClientWidth = 419
  PixelsPerInch = 96
  TextHeight = 13
  object Label12: TLabel [0]
    Left = 46
    Top = 16
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = 'Raz'#243'n Social:'
  end
  object Label13: TLabel [1]
    Left = 86
    Top = 39
    Width = 26
    Height = 13
    Alignment = taRightJustify
    Caption = 'Calle:'
  end
  object Label14: TLabel [2]
    Left = 74
    Top = 86
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Colonia:'
  end
  object Label16: TLabel [3]
    Left = 76
    Top = 109
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ciudad:'
  end
  object Label17: TLabel [4]
    Left = 44
    Top = 154
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo Postal:'
  end
  object Label15: TLabel [5]
    Left = 76
    Top = 132
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Estado:'
  end
  object Label18: TLabel [6]
    Left = 79
    Top = 200
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'R.F.C.:'
  end
  object Label19: TLabel [7]
    Left = 42
    Top = 223
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = '# INFONAVIT:'
  end
  object Label21: TLabel [8]
    Left = 14
    Top = 246
    Width = 98
    Height = 13
    Alignment = taRightJustify
    Caption = 'Representante legal:'
  end
  object Label1: TLabel [9]
    Left = 67
    Top = 177
    Width = 45
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tel'#233'fono:'
  end
  object Label2: TLabel [10]
    Left = 64
    Top = 62
    Width = 48
    Height = 13
    Caption = '# Exterior:'
  end
  object Label3: TLabel [11]
    Left = 194
    Top = 62
    Width = 45
    Height = 13
    Caption = '# Interior:'
  end
  inherited PanelBotones: TPanel
    Top = 272
    Width = 419
    inherited OK_DevEx: TcxButton
      Left = 254
      Top = 5
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 334
      Top = 5
    end
  end
  object Entidad: TZetaKeyLookup_DevEx [13]
    Tag = 5
    Left = 117
    Top = 128
    Width = 300
    Height = 21
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 7
    TabStop = True
    WidthLlave = 60
  end
  object RazonSocial: TEdit [14]
    Tag = 1
    Left = 117
    Top = 12
    Width = 274
    Height = 21
    TabOrder = 1
  end
  object Calle: TEdit [15]
    Tag = 2
    Left = 117
    Top = 35
    Width = 274
    Height = 21
    TabOrder = 2
  end
  object Colonia: TEdit [16]
    Tag = 3
    Left = 117
    Top = 82
    Width = 274
    Height = 21
    TabOrder = 5
  end
  object Ciudad: TEdit [17]
    Tag = 4
    Left = 117
    Top = 105
    Width = 274
    Height = 21
    TabOrder = 6
  end
  object CodigoPostal: TEdit [18]
    Tag = 6
    Left = 117
    Top = 150
    Width = 188
    Height = 21
    TabOrder = 8
  end
  object Telefono: TEdit [19]
    Tag = 7
    Left = 117
    Top = 173
    Width = 188
    Height = 21
    TabOrder = 9
  end
  object RepresentanteLegal: TEdit [20]
    Tag = 12
    Left = 117
    Top = 242
    Width = 271
    Height = 21
    TabOrder = 12
  end
  object RFC: TMaskEdit [21]
    Tag = 8
    Left = 117
    Top = 196
    Width = 188
    Height = 21
    TabOrder = 10
    Text = 'RFC'
  end
  object Infonavit: TMaskEdit [22]
    Tag = 9
    Left = 117
    Top = 219
    Width = 271
    Height = 21
    TabOrder = 11
    Text = 'Infonavit'
  end
  object Numexterior: TEdit [23]
    Left = 117
    Top = 58
    Width = 70
    Height = 21
    TabOrder = 3
  end
  object Numinterior: TEdit [24]
    Left = 242
    Top = 58
    Width = 70
    Height = 21
    TabOrder = 4
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
