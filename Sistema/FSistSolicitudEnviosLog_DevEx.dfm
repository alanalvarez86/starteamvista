object SistSolicitudEnviosLog_DevEx: TSistSolicitudEnviosLog_DevEx
  Left = 304
  Top = 161
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Solicitudes de env'#237'os programados'
  ClientHeight = 464
  ClientWidth = 404
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelInferior_DevEx: TPanel
    Left = 0
    Top = 430
    Width = 404
    Height = 34
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      404
      34)
    object DevEx_cxDBNavigatorEdicion: TcxDBNavigator
      Left = 8
      Top = 4
      Width = 102
      Height = 25
      Buttons.CustomButtons = <>
      Buttons.First.Hint = 'Primero'
      Buttons.First.Visible = True
      Buttons.PriorPage.Enabled = False
      Buttons.PriorPage.Visible = False
      Buttons.Prior.Hint = 'Anterior'
      Buttons.Prior.Visible = True
      Buttons.Next.Hint = 'Siguiente'
      Buttons.Next.Visible = True
      Buttons.NextPage.Enabled = False
      Buttons.NextPage.Visible = False
      Buttons.Last.Hint = #218'ltimo'
      Buttons.Insert.Enabled = False
      Buttons.Insert.Visible = False
      Buttons.Append.Enabled = False
      Buttons.Delete.Enabled = False
      Buttons.Delete.Visible = False
      Buttons.Edit.Enabled = False
      Buttons.Edit.Visible = False
      Buttons.Post.Enabled = False
      Buttons.Post.Visible = False
      Buttons.Cancel.Enabled = False
      Buttons.Cancel.Visible = False
      Buttons.Refresh.Enabled = False
      Buttons.Refresh.Visible = False
      Buttons.SaveBookmark.Enabled = False
      Buttons.SaveBookmark.Visible = False
      Buttons.GotoBookmark.Enabled = False
      Buttons.GotoBookmark.Visible = False
      Buttons.Filter.Enabled = False
      Buttons.Filter.Visible = False
      DataSource = dsLog
      Anchors = [akLeft, akBottom]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
    object Salir_DevEx: TcxButton
      Left = 324
      Top = 4
      Width = 75
      Height = 26
      Hint = 'Cerrar Pantalla y Salir'
      Anchors = [akRight, akBottom]
      Caption = '  &Salir'
      LookAndFeel.SkinName = 'TressMorado2013'
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        20000000000000090000000000000000000000000000000000004858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF5362CFFF6D7AD6FF8D97DFFFACB3E8FFC8CD
        F0FFE8EAF9FFBAC0ECFF707DD7FF7682D9FF6572D4FF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFBAC0ECFF848FDDFF8D97DFFFACB3E8FF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFF969FE2FF6572D4FF969FE2FF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
        FEFFD4D8F3FFBAC0ECFF4858CCFF4858CCFF969FE2FFF9FAFDFFFFFFFFFFFFFF
        FFFFA4ACE6FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFE5E7F8FFBAC0ECFF4858CCFF4858CCFF707DD7FFD1D5F2FFE5E7F8FFBAC0
        ECFF818CDCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF6A77D6FF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFBAC0ECFFACB3E8FFBAC0ECFFB1B8E9FF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF5665D0FF7682D9FF969FE2FFB1B8
        E9FFD1D5F2FFACB3E8FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
      OptionsImage.Margin = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = Salir_DevExClick
    end
  end
  object PanelSuperior: TPanel
    Left = 0
    Top = 0
    Width = 404
    Height = 201
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object CAL_INICIO: TZetaDBTextBox
      Left = 108
      Top = 63
      Width = 229
      Height = 17
      AutoSize = False
      Caption = 'CAL_INICIO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CAL_INICIO'
      DataSource = Datasource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object RE_CODIGO: TZetaDBTextBox
      Left = 108
      Top = 27
      Width = 109
      Height = 17
      AutoSize = False
      Caption = 'RE_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'RE_CODIGO'
      DataSource = Datasource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CA_NOMBRE: TZetaDBTextBox
      Left = 108
      Top = 9
      Width = 161
      Height = 17
      AutoSize = False
      Caption = 'CA_NOMBRE'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CA_NOMBRE'
      DataSource = Datasource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CAL_FIN: TZetaDBTextBox
      Left = 108
      Top = 81
      Width = 229
      Height = 17
      AutoSize = False
      Caption = 'CAL_FIN'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CAL_FIN'
      DataSource = Datasource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CAL_ESTATUS: TZetaDBTextBox
      Left = 108
      Top = 99
      Width = 109
      Height = 17
      AutoSize = False
      Caption = 'CAL_ESTATUS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CAL_ESTATUS'
      DataSource = Datasource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CAL_FECHA: TZetaDBTextBox
      Left = 108
      Top = 45
      Width = 229
      Height = 17
      AutoSize = False
      Caption = 'CAL_FECHA'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CAL_FECHA'
      DataSource = Datasource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label1: TLabel
      Left = 11
      Top = 9
      Width = 91
      Height = 13
      Alignment = taRightJustify
      Caption = 'Env'#237'o programado:'
    end
    object Label2: TLabel
      Left = 61
      Top = 27
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Caption = 'Reporte:'
    end
    object Label4: TLabel
      Left = 59
      Top = 45
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Solicitud:'
    end
    object Label5: TLabel
      Left = 74
      Top = 64
      Width = 28
      Height = 13
      Alignment = taRightJustify
      Caption = 'Inicio:'
    end
    object Label6: TLabel
      Left = 85
      Top = 82
      Width = 17
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fin:'
    end
    object Label7: TLabel
      Left = 64
      Top = 100
      Width = 38
      Height = 13
      Alignment = taRightJustify
      Caption = 'Estatus:'
    end
    object Label8: TLabel
      Left = 59
      Top = 118
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Mensaje:'
    end
    object Label10: TLabel
      Left = 8
      Top = 182
      Width = 42
      Height = 13
      Alignment = taRightJustify
      Caption = 'Bit'#225'cora:'
    end
    object CAL_MENSAJ: TDBMemo
      Left = 108
      Top = 117
      Width = 229
      Height = 66
      TabStop = False
      DataField = 'CAL_MENSAJ'
      DataSource = Datasource
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 0
    end
  end
  object ZetaCXGrid: TZetaCXGrid
    Left = 0
    Top = 201
    Width = 404
    Height = 229
    Align = alClient
    TabOrder = 2
    object ZetaCXGridDBTableView: TcxGridDBTableView
      OnDblClick = ZetaCXGridDBTableViewDblClick
      OnKeyDown = ZetaCXGridDBTableViewKeyDown
      Navigator.Buttons.CustomButtons = <>
      FilterBox.CustomizeDialog = False
      DataController.DataSource = dsLog
      DataController.Filter.OnGetValueList = ZetaCXGridDBTableViewDataControllerFilterGetValueList
      DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnFilteredItemsList = True
      OptionsCustomize.ColumnGrouping = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object BI_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'BI_FECHA'
      end
      object BI_HORA: TcxGridDBColumn
        Caption = 'Registro'
        DataBinding.FieldName = 'BI_HORA'
      end
      object BI_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'BI_TIPO'
      end
      object BI_TEXTO: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'BI_TEXTO'
        Width = 96
      end
    end
    object ZetaCXGridLevel: TcxGridLevel
      GridView = ZetaCXGridDBTableView
    end
  end
  object Datasource: TDataSource
    Left = 456
    Top = 72
  end
  object dsLog: TDataSource
    Left = 412
    Top = 71
  end
end
