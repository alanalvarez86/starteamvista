unit FEditImpresoras;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, Buttons, DBCtrls, StdCtrls, ComCtrls, Mask,
     ZBaseEdicion,
     ZetaEdit, ZetaSmartLists;

type
  TEditImpresoras = class(TBaseEdicion)
    PageControl: TPageControl;
    Panel1: TPanel;
    Label1: TLabel;
    PI_NOMBRE: TZetaDBEdit;
    GeneralesTab: TTabSheet;
    EspecialesTab: TTabSheet;
    Label2: TLabel;
    PI_CHAR_10: TDBEdit;
    Label3: TLabel;
    PI_CHAR_12: TDBEdit;
    Label4: TLabel;
    PI_CHAR_17: TDBEdit;
    PI_UNDE_ON: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    PI_UNDE_OF: TDBEdit;
    Label7: TLabel;
    PI_BOLD_ON: TDBEdit;
    Label8: TLabel;
    PI_BOLD_OF: TDBEdit;
    PI_ITAL_ON: TDBEdit;
    Label9: TLabel;
    Label10: TLabel;
    PI_ITAL_OF: TDBEdit;
    Label11: TLabel;
    PI_EJECT: TDBEdit;
    PI_RESET: TDBEdit;
    Label12: TLabel;
    Label13: TLabel;
    PI_6_LINES: TDBEdit;
    PI_8_LINES: TDBEdit;
    Label14: TLabel;
    Label15: TLabel;
    PI_LANDSCA: TDBEdit;
    PI_EXTRA_1: TDBEdit;
    Label16: TLabel;
    Label17: TLabel;
    PI_EXTRA_2: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditImpresoras: TEditImpresoras;

implementation

uses DSistema,
     ZAccesosTress,
     ZetaCommonClasses;

{$R *.DFM}

{ ************ TEditImpresoras ************ }

procedure TEditImpresoras.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_SIST_DATOS_IMPRESORAS;
     FirstControl := PI_NOMBRE;
     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_EDIT_IMPRESORAS;
     {$endif}     
end;

procedure TEditImpresoras.FormShow(Sender: TObject);
begin
     PageControl.ActivePage := GeneralesTab;
     inherited;
end;

procedure TEditImpresoras.Connect;
begin
     with dmSistema do
     begin
          Datasource.Dataset := cdsImpresoras;
     end;
end;

end.
