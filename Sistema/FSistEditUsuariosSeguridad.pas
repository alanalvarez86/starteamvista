unit FSistEditUsuariosSeguridad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ZetaDBTextBox, StdCtrls, ZetaKeyCombo,
  DBCtrls, Mask, ZetaNumero, ComCtrls, ExtCtrls, Buttons,
  ZetaSmartLists, FSistBaseEditUsuariosSeguridad, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, ZetaKeyLookup_DevEx, cxPC, cxNavigator, cxDBNavigator, cxButtons;

type
  TSistEditUsuariosSeguridad = class(TSistBaseEditUsuariosSeguridad)
    Supervisa: TBitBtn;
    Preferencias: TBitBtn;
    Areas: TBitBtn;
    GroupBox2: TGroupBox;
    Label14: TLabel;
    CM_CODIGO: TZetaDBKeyLookup_DevEx;
    CB_CODIGO: TZetaDBKeyLookup_DevEx;
    lblEmpleado: TLabel;
    BtnTemp: TButton;
    Label15: TLabel;
    US_DOMAIN: TDBEdit;
    US_ACTIVO: TDBCheckBox;
    US_PORTAL: TDBCheckBox;
    spbtnUsuarioRepetido: TcxButton;
    GroupBox3: TGroupBox;
    US_JEFE: TZetaDBKeyLookup_DevEx;
    Label16: TLabel;
    btnRoles: TcxButton;
    btnDominioUsuario: TcxButton;
    CCosto: TBitBtn;
    procedure SupervisaClick(Sender: TObject);
    procedure PreferenciasClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AreasClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure CM_CODIGOValidKey(Sender: TObject);
    procedure BtnTempEnter(Sender: TObject);
    procedure CM_CODIGOExit(Sender: TObject);
    procedure btnRolesClick(Sender: TObject);
    procedure BuscarRepetidoBtnClick(Sender: TObject);
    procedure btnDominioUsuarioClick(Sender: TObject);
    procedure US_CORTOChange(Sender: TObject);
    procedure US_DOMAINChange(Sender: TObject);
    procedure DataSourceStateChange(Sender: TObject);
    procedure CCostoClick(Sender: TObject);
  private
    { Private declarations }
    sEmpresaOriginal: String;
    FCerrarForma: Boolean;
    procedure SetControls( const lEnabled: Boolean);
    procedure SetControlEmpleado;
  public
    { Public declarations }
    procedure Connect;override;
    procedure HabilitaControles; override;
  protected
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure EscribirCambios;override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    procedure Borrar;override;
  end;

var
  SistEditUsuariosSeguridad: TSistEditUsuariosSeguridad;

implementation
uses FSistEditUsuarioSupervisor_DevEx,
     FArbolConfigura_DevEx,
     FSistEditUsuarioArea_DevEx,
     ZetaCommonClasses,
     ZBaseDlgModal_DevEx,
     ZGlobalTress,
     ZAccesosMgr,
     ZAccesosTress,
     DGlobal,
     ZetaDialogo,
     ZetaCommonTools,
     DCliente,
     dSistema,
     DTablas,
     DBaseSistema,
     FSistEditUsuarioCCosto_DevEx,
     FSistEditUsuarioRoles_devEx,
     ZBaseEdicion,
     ZToolsPe;


{$R *.DFM}

procedure TSistEditUsuariosSeguridad.FormCreate(Sender: TObject);
begin
     inherited;
     with dmSistema do
     begin
          CM_CODIGO.LookupDataset := cdsEmpresasPortal;
          CB_CODIGO.LookupDataset := cdsEmpleadosPortal;
          US_JEFE.LookupDataset := cdsUsuariosSeguridadLookup;
     end;
     FCerrarForma:= FALSE;

     {$ifdef DOS_CAPAS}
     US_PORTAL.Visible := FALSE;
    {$endif}
end;

procedure TSistEditUsuariosSeguridad.Connect;
begin
     with dmSistema do
     begin
          cdsEmpresasPortal.Conectar;
          cdsEmpleadosPortal.Conectar;
     end;
     inherited; { Quedo asi porque se llama primero al metodo cdsEmpleadosPortalLookupSearch por lo que se dispara el
                  metodo BuildEmpresaPortal y trata de conectar la empresa con los datos de cdsEmpresasPortal pero como
                  no se ah Conectado truena }

     if NOT Navegacion then
        IndexDerechos := D_EMP_DATOS_USUARIO;
end;


procedure TSistEditUsuariosSeguridad.FormShow(Sender: TObject);
var
   sAreas, sCCosto, sMensaje: string;
   bUsaEnrolamientoSup : Boolean;
begin
     inherited;
     sAreas := Global.GetGlobalString( K_GLOBAL_LABOR_AREAS );
     bUsaEnrolamientoSup := Global.GetGlobalBooleano( K_GLOBAL_ENROLAMIENTO_SUPERVISORES);

     with Areas do
     begin
          Caption := Format( '&%s A Su Cargo', [ sAreas ] );
          Hint    := Format( 'Configurar %s A Su Cargo', [ sAreas ] );
          Enabled := ZAccesosMgr.CheckDerecho( D_SIST_ASIGNAR_AREAS, K_DERECHO_CONSULTA );
     end;
     Supervisa.Enabled := ZAccesosMgr.CheckDerecho( D_SIST_ASIGNAR_SUPER, K_DERECHO_CONSULTA );
     btnRoles.Enabled := ZAccesosMgr.CheckDerecho( D_CAT_ROLES, K_DERECHO_CONSULTA ) OR ZAccesosMgr.CheckDerecho( D_CAT_ROLES, K_DERECHO_CAMBIO )
                         OR ZAccesosMgr.CheckDerecho( D_CAT_ROLES, K_DERECHO_ALTA );
     US_JEFE.Enabled := not bUsaEnrolamientoSup;

     with CCosto do
     begin
          sCCosto := Global.NombreCosteo;
          Caption := Format( '&%s A Su Cargo', [ sCCosto ] );
          Hint    := Format( 'Configurar %s A Su Cargo', [ sCCosto ] );
          Visible := dmCliente.ModuloAutorizadoCosteo and dmTablas.HayDataSetTransferencia( sMensaje );
     end;
end;

procedure TSistEditUsuariosSeguridad.HabilitaControles;
begin
     inherited HabilitaControles;
     Areas.Enabled := dmCliente.EmpresaAbierta and not Editing;
     Supervisa.Enabled := dmCliente.EmpresaAbierta and not Editing;
     Preferencias.Enabled := not Editing;
     {$ifdef DOS_CAPAS}
     btnRoles.Enabled := FALSE;
     {$else}
     btnRoles.Enabled := not Editing and(ZAccesosMgr.CheckDerecho( D_CAT_ROLES, K_DERECHO_CONSULTA ) OR ZAccesosMgr.CheckDerecho( D_CAT_ROLES, K_DERECHO_CAMBIO ) );
     {$endif}
     spbtnUsuarioRepetido.Enabled := Editing and StrLleno( US_CORTO.Text );
     btnDominioUsuario.Enabled := Editing and StrLleno( US_DOMAIN.Text );
     //US_BLOQUEA.Enabled := ZAccesosMgr.CheckDerecho( D_SIST_APAGA_USUARIOS, K_DERECHO_CONSULTA );
end;

procedure TSistEditUsuariosSeguridad.SupervisaClick(Sender: TObject);
var
   sMensaje: String;
begin
     if PuedeModificar( sMensaje ) then
     begin
          //ZBaseDlgModal.ShowDlgModal( FSistEditUserSuper, TFSistEditUserSuper )

          if dmSistema.PuedeModificarUsuarioPropio( DataSource.Dataset ) then
          begin
               ZBaseDlgModal_DevEx.ShowDlgModal( FSistEditUserSuper_DevEx, TFSistEditUserSuper_DevEx );
          end
          else
          begin
               ZetaDialogo.zInformation( Caption, 'No Tiene Permiso Para Modificar Registros', 0 );
          end;
     end
     else
     begin
          ZetaDialogo.zInformation( Caption, sMensaje, 0 );
     end;
end;

procedure TSistEditUsuariosSeguridad.PreferenciasClick(Sender: TObject);
var
   sMensaje: String;
begin
     if PuedeModificar( sMensaje ) then
        ZBaseDlgModal_DevEx.ShowDlgModal( ArbolConfigura_DevEx, TArbolConfigura_DevEx )
     else
         ZetaDialogo.zInformation( Caption, sMensaje, 0 );
end;


procedure TSistEditUsuariosSeguridad.AreasClick(Sender: TObject);
var
   sMensaje: String;
begin
     if PuedeModificar( sMensaje ) then
     begin
          //ZBaseDlgModal.ShowDlgModal( FSistEditUserArea, TFSistEditUserArea )

          if dmSistema.PuedeModificarUsuarioPropio( DataSource.Dataset ) then
          begin
               ZBaseDlgModal_DevEx.ShowDlgModal( FSistEditUserArea_DevEx, TFSistEditUserArea_DevEx );
          end
          else
          begin
               ZetaDialogo.zInformation( Caption, 'No Tiene Permiso Para Modificar Registros', 0 );
          end;
     end
     else
     begin
          ZetaDialogo.zInformation( Caption, sMensaje, 0 );
     end;
end;

procedure TSistEditUsuariosSeguridad.SetControls( const lEnabled: Boolean );
begin
     CB_CODIGO.Enabled := lEnabled;
     lblEmpleado.Enabled := lEnabled;
end;

procedure TSistEditUsuariosSeguridad.SetControlEmpleado;
begin
     if ( dmSistema.cdsUsuariosSeguridad.FieldByName('CB_CODIGO').AsInteger <= 0 ) then
        CB_CODIGO.SetLlaveDescripcion( VACIO, VACIO );
end;

procedure TSistEditUsuariosSeguridad.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = Nil ) then
     begin
          sEmpresaOriginal := dmSistema.cdsUsuariosSeguridad.FieldByName('CM_CODIGO').AsString;
          DataSource.AutoEdit := dmSistema.PuedeModificarUsuarioPropio( DataSource.Dataset );
           {OP:30.Abr.08}
          if ( dmSistema.cdsUsuariosSeguridad.FieldByName( 'US_BLOQUEA' ).AsString = K_GLOBAL_SI ) and
             ( ZAccesosMgr.Revisa( D_SIST_PRENDE_USUARIOS ) ) then
             US_BLOQUEA.Enabled := True
          else if ( dmSistema.cdsUsuariosSeguridad.FieldByName( 'US_BLOQUEA' ).AsString = K_GLOBAL_NO ) and
             ( ZAccesosMgr.Revisa( D_SIST_APAGA_USUARIOS ) ) then
             US_BLOQUEA.Enabled := True
          else
              US_BLOQUEA.Enabled := False;

     end;
     if (Field = Nil) or ( Field.FieldName = 'CM_CODIGO' ) then
     begin
          SetControls( strLleno( dmSistema.cdsUsuariosSeguridad.FieldByName('CM_CODIGO').AsString ) );
     end;
     if (Field = Nil) or ( Field.FieldName = 'CB_CODIGO' ) then
     begin
          SetControlEmpleado;
     end;
end;

procedure TSistEditUsuariosSeguridad.CM_CODIGOValidKey(Sender: TObject);
begin
     inherited;
     if not ( dmSistema.cdsUsuariosSeguridad.State in [dsBrowse]) then
     begin
          if ( BtnTemp.Focused ) then
          begin
               if ( CB_CODIGO.Enabled ) then
                  CB_CODIGO.SetFocus
          end;
          if ( ActiveControl = Nil ) then
             US_BLOQUEA.SetFocus;
     end;
end;

procedure TSistEditUsuariosSeguridad.BtnTempEnter(Sender: TObject);
begin
     inherited;
     if ( CB_CODIGO.Enabled ) or strVacio( CM_CODIGO.Llave ) then
         US_BLOQUEA.SetFocus;
end;

procedure TSistEditUsuariosSeguridad.CM_CODIGOExit(Sender: TObject);
begin
     inherited;
     if ( sEmpresaOriginal <> CM_CODIGO.Llave ) then
     begin
          CB_CODIGO.Llave := VACIO;
          sEmpresaOriginal := CM_CODIGO.LLave;
     end;
end;

procedure TSistEditUsuariosSeguridad.btnRolesClick(Sender: TObject);
var sMensaje :string;
begin
     inherited;
      if PuedeModificar( sMensaje ) then
        ZBaseDlgModal_DevEx.ShowDlgModal( SistEditUsuarioRoles_DevEx, TFSistEditUsuarioRoles_DevEx )
     else
         ZetaDialogo.zInformation( Caption, sMensaje, 0 );
end;

procedure TSistEditUsuariosSeguridad.BuscarRepetidoBtnClick(Sender: TObject);
begin
     inherited;
     with dmSistema do
     begin
          if ( UpperCase(US_CORTO.Text) <> UpperCase( cdsUsuariosSeguridad.FieldByName('US_CORTO').AsString ) )then
          begin
               if (cdsUsuariosSeguridadLookup.Locate('US_CORTO',UpperCase(US_CORTO.Text) ,[]))then
               begin
                    ZetaDialogo.zInformation( Caption, 'El Usuario: '+US_CORTO.Text+' est� repetido' , 0 )
               end
               else
               begin
                    ZetaDialogo.zInformation( Caption, 'No existe Usuario repetido' , 0 )
               end;
          end
          else
              ZetaDialogo.zInformation( Caption, 'No existe Usuario repetido' , 0 );
     end;
end;

procedure TSistEditUsuariosSeguridad.EscribirCambios;
{$ifndef DOS_CAPAS}
var
   Lista : TStrings;
   lNuevoRegistro:Boolean;
{$endif}
begin
     dmSistema.EditandoUsuarios := True; 
     {$ifndef DOS_CAPAS}
     lNuevoRegistro := Inserting;
     {$endif}

     inherited;
     
     {$ifndef DOS_CAPAS}
     if( lNuevoRegistro )then
     begin
          with dmSistema do
          begin
               if (cdsUsuariosSeguridad.ChangeCount = 0) then
               begin
                    Lista := TStringList.Create;
                    CargaListaRolesDefault(Lista);
                    Lista.Free;
               end;
          end;
     end;
     {$endif}
end;

function TSistEditUsuariosSeguridad.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := Navegacion ;
     if NOT Result then
        sMensaje := 'El Empleado ya tiene Acceso a Sistema Tress';
end;

procedure TSistEditUsuariosSeguridad.btnDominioUsuarioClick(Sender: TObject);
begin
     inherited;
     with dmSistema do
     begin
          if ( UpperCase(US_DOMAIN.Text) <> UpperCase( cdsUsuariosSeguridad.FieldByName('US_DOMAIN').AsString ) )then
          begin
               if (cdsUsuariosSeguridadLookup.Locate('US_DOMAIN',UpperCase(US_DOMAIN.Text),[]))then
               begin
                    ZetaDialogo.zInformation( Caption, 'El Usuario del Dominio: '+US_DOMAIN.Text+' est� repetido' , 0 )
               end
               else
               begin
                    ZetaDialogo.zInformation( Caption, 'No existe Usuario repetido' , 0 )
               end;
          end
          else
              ZetaDialogo.zInformation( Caption, 'No existe Usuario del Dominio repetido' , 0 );
     end;
end;

procedure TSistEditUsuariosSeguridad.Borrar;
const
   K_ADV_SUPERVISORES_EMPRESA = 'El Empleado puede estar como un Supervisor Enrolado en esta o en alguna otra de sus empresas. Si le quita el Acceso a Sistema TRESS puede quedar alg�n Supervisor sin enrolar.';
begin
     if Navegacion then
        inherited Borrar
     else
     begin
          if ZConfirm( Caption,K_ADV_SUPERVISORES_EMPRESA +'� Desea quitarle el Acceso a Sistema Tress al Empleado ?', 0, mbNo ) then
          begin
               dmSistema.BajaUsuario(dmCliente.cdsEmpleado.FieldByName('CB_CODIGO').AsInteger, TRUE);
               Close;
          end;
     end;
end;

procedure TSistEditUsuariosSeguridad.US_CORTOChange(Sender: TObject);
begin
     inherited;
     spbtnUsuarioRepetido.Enabled := Editing and StrLleno( US_CORTO.Text );

end;
procedure TSistEditUsuariosSeguridad.US_DOMAINChange(Sender: TObject);
begin
     inherited;
      btnDominioUsuario .Enabled := Editing and StrLleno( US_DOMAIN.Text );
end;

function TSistEditUsuariosSeguridad.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := dmSistema.PuedeModificarUsuarioPropio( DataSource.Dataset );
          if (not Result) then
          begin
               sMensaje := 'No Tiene Permiso Para Modificar Registros';
          end;
     end;
end;

procedure TSistEditUsuariosSeguridad.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     Supervisa.Enabled := (not Editing) and (ZAccesosMgr.CheckDerecho( D_SIST_ASIGNAR_SUPER, K_DERECHO_CONSULTA ));
     Areas.Enabled := (not Editing) and (ZAccesosMgr.CheckDerecho( D_SIST_ASIGNAR_AREAS, K_DERECHO_CONSULTA ));
     CCosto.Enabled := (not Editing) and dmCliente.ModuloAutorizadoCosteo;
end;

procedure TSistEditUsuariosSeguridad.CCostoClick(Sender: TObject);
var
   sMensaje: String;
begin
     if PuedeModificar( sMensaje ) then
     begin
          if dmSistema.PuedeModificarUsuarioPropio( DataSource.Dataset ) then
          begin
               if ( dmTablas.HayDataSetTransferencia( sMensaje ) ) then
                    ZBaseDlgModal_DevEx.ShowDlgModal( SistEditUsuarioCCosto_DevEx, TSistEditUsuarioCCosto_DevEx )
               else
                    ZInformation( 'Usuarios', sMensaje, 0 )
          end
          else
          begin
               ZetaDialogo.zInformation( Caption, 'No Tiene Permiso Para Modificar Registros', 0 );
          end;
     end
     else
     begin
          ZetaDialogo.zInformation( Caption, sMensaje, 0 );
     end;
end;

end.
