inherited ExportarTablas_DevEx: TExportarTablas_DevEx
  Left = 671
  Top = 217
  Caption = 'Exportar Tablas'
  ClientHeight = 379
  ClientWidth = 535
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 343
    Width = 535
    TabOrder = 2
    inherited OK_DevEx: TcxButton
      Left = 371
      Top = 5
      ModalResult = 0
      OnClick = OK_DevExClick
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 450
      Top = 5
      Cancel = True
      OnClick = Cancelar_DevExClick
    end
  end
  object DirectorioGB: TcxGroupBox [1]
    Left = 0
    Top = 89
    Align = alTop
    Caption = ' Directorio donde guardar las tablas exportadas: '
    TabOrder = 1
    Height = 56
    Width = 535
    object PathLBL: TLabel
      Left = 37
      Top = 21
      Width = 57
      Height = 13
      Caption = 'Guardar En:'
      Transparent = True
    end
    object PathSeek: TcxButton
      Left = 476
      Top = 17
      Width = 21
      Height = 21
      Hint = 'Buscar Directorio'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = PathSeekClick
      OptionsImage.Glyph.Data = {
        DA050000424DDA05000000000000360000002800000013000000130000000100
        200000000000A40500000000000000000000000000000000000000B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFDFF5
        FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
        FCFFDFF5FCFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF10B7EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF40C5EFFF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF40C5EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6CD3
        F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF6CD3F2FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF93DEF6FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF97E0F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFEBF9FDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF54CB
        F0FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CF
        F1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF80D9F4FF80D9
        F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC3EDFAFFDFF5FCFFDFF5FCFFDFF5
        FCFF70D4F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
      OptionsImage.Margin = 1
    end
    object Directorio: TEdit
      Left = 97
      Top = 17
      Width = 376
      Height = 21
      TabOrder = 0
    end
  end
  object gbBaseDatos: TcxGroupBox [2]
    Left = 0
    Top = 0
    Align = alTop
    Caption = 'Base de Datos: '
    TabOrder = 0
    Height = 89
    Width = 535
    object lblDescripcion: TLabel
      Left = 30
      Top = 38
      Width = 59
      Height = 13
      Caption = 'Descripci'#243'n:'
      Transparent = True
    end
    object lblCodigo: TLabel
      Left = 53
      Top = 16
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      Transparent = True
    end
    object lblTipo: TLabel
      Left = 65
      Top = 60
      Width = 24
      Height = 13
      Caption = 'Tipo:'
      Transparent = True
    end
    object DB_CODIGO: TZetaDBTextBox
      Left = 96
      Top = 16
      Width = 105
      Height = 17
      AutoSize = False
      Caption = 'DB_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'DB_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object DB_DESCRIP: TZetaDBTextBox
      Left = 96
      Top = 36
      Width = 377
      Height = 17
      AutoSize = False
      Caption = 'DB_DESCRIP'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'DB_DESCRIP'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object DB_TIPO: TZetaDBTextBox
      Left = 96
      Top = 56
      Width = 105
      Height = 17
      AutoSize = False
      Caption = 'DB_TIPO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'DB_TIPO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  object PageControl: TcxPageControl [3]
    Left = 0
    Top = 145
    Width = 535
    Height = 198
    Align = alClient
    TabOrder = 3
    Properties.ActivePage = TabTablas
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 194
    ClientRectLeft = 4
    ClientRectRight = 531
    ClientRectTop = 24
    object TabTablas: TcxTabSheet
      Caption = 'Tablas a exportar'
      object ExportTables: TcxCheckListBox
        Left = 0
        Top = 0
        Width = 527
        Height = 170
        Align = alClient
        Columns = 5
        EditValueFormat = cvfStatesString
        Items = <>
        TabOrder = 0
        OnClickCheck = ExportTablesClickCheck
      end
    end
    object TabBitacora: TcxTabSheet
      Caption = 'Bit'#225'cora'
      ImageIndex = 1
      object MemBitacora: TcxMemo
        Left = 0
        Top = 0
        Hint = 'Bit'#225'cora'
        Align = alClient
        ParentFont = False
        Properties.ReadOnly = True
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Courier New'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 0
        Height = 170
        Width = 527
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 2097640
  end
  object DataSource: TDataSource
    Left = 456
    Top = 8
  end
  object cdsDataset: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 496
    Top = 8
  end
end
