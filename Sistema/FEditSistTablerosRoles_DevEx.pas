unit FEditSistTablerosRoles_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls, ZetaNumero, ZetaKeyCombo, Mask,
  ZetaEdit, DB, ExtCtrls, Buttons, ZetaCommonLists,ZetaDBTextBox,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  cxContainer, cxEdit, cxTextEdit, cxMemo, cxDBEdit, ZetaKeyLookup_DevEx,
  ZBaseDlgModal_DevEx, cxGroupBox;

type
  TEditSistTablerosRoles_DevEx = class(TZetaDlgModal_DevEx)
    Label5: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    RO_CODIGO: TZetaKeyLookup_DevEx;
    cxGroupBox1: TcxGroupBox;
    Empresa: TZetaKeyCombo;
    Tablero: TZetaKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
  private
    { Private declarations }
    procedure LlenarInformacion;
    procedure SetDefaults;
  protected
    { Protected declarations }
  public
    { Public declarations }
    iEmpresa : Integer;
    sRol : String;
    iTablero : Integer;
  end;

var
  EditSistTablerosRoles_DevEx: TEditSistTablerosRoles_DevEx;

implementation
uses
    dConsultas,
    dSistema,
    dCliente,
    ZetaCommonClasses,
    ZAccesosTress,
    ZetaDialogo;

{$R *.dfm}

procedure TEditSistTablerosRoles_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
     Close;
end;

procedure TEditSistTablerosRoles_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H394004_PantallaInicioConfiguracion_TRESS;
     with dmSistema do
     begin
          RO_CODIGO.LookupDataset := cdsRoles;
          cdsRoles.Conectar;
     end;

     dmConsultas.TableroGlobal := False;
     LlenarInformacion;
end;

procedure TEditSistTablerosRoles_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     RO_CODIGO.Llave := VACIO;
     SetDefaults;
end;

procedure TEditSistTablerosRoles_DevEx.SetDefaults;
begin
     if iTablero = 0 then
        iTablero := 1;

     if iEmpresa = 0 then
        dmConsultas.TableroEmpresa.Find(dmCliente.Compania, iEmpresa);

     Tablero.ItemIndex := (iTablero - 1);
     Empresa.ItemIndex := (iEmpresa - 1);
     RO_CODIGO.Llave := sRol;
end;

procedure TEditSistTablerosRoles_DevEx.LlenarInformacion;
begin
     with dmConsultas do
     begin
          TableroGlobal := True;
          cdsTREmpresa.Refrescar;
          cdsTRTablero.Refrescar;

          LlenarLista(TableroEmpresa, cdsTREmpresa);
          Empresa.Lista := TableroEmpresa;
          LlenarLista(TableroIndicadores, cdsTRTablero);
          Tablero.Lista := TableroIndicadores;
     end;

     Empresa.Items.Delete(0);
     Tablero.Items.Delete(0);
end;

procedure TEditSistTablerosRoles_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     ModalResult := mrNone;
     if Empresa.ItemIndex > -1 then
     begin
          if Tablero.ItemIndex > -1 then
          begin
               if RO_CODIGO.Llave = VACIO then
               begin
                    ZetaDialogo.ZWarning(Self.Caption,'Se debe seleccionar el rol', 0, mbOK);
                    ActiveControl := RO_CODIGO;
               end
               else
               begin
                    with dmConsultas do
                    begin
                         if GrabaTableroRoles( StrToInt(TableroIndicadores.Names[Tablero.ItemIndex+1]),
                            TableroEmpresa.Names[Empresa.ItemIndex+1], RO_CODIGO.Llave, K_GLOBAL_SI ) = 1 then
                         begin
                              ModalResult := mrOk;
                         end;
                    end;
               end;
          end
          else
          begin
               ZetaDialogo.ZWarning(Self.Caption,'Se debe seleccionar el tablero', 0, mbOK);
               ActiveControl := Tablero;
          end;
     end
     else
     begin
          ZetaDialogo.ZWarning(Self.Caption,'Se debe seleccionar la empresa', 0, mbOK);
          ActiveControl := Empresa;
     end;
end;

end.
