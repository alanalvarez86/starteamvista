unit FBitacoraRegBio_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseGridLectura_DevEx, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls,
  StdCtrls, Buttons, Mask, ZetaFecha, ZetaCommonClasses,
  ZetaKeyCombo, ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxButtons, ZetaKeyLookup_DevEx, System.Actions;//@DC

type
  TBitacoraRegBio_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    luEmpresa: TZetaKeyLookup_DevEx;
    Label2: TLabel;
    zTerminales: TZetaKeyLookup_DevEx;
    edtMensaje: TEdit;
    Label5: TLabel;
    Label3: TLabel;
    dInicio: TZetaFecha;
    Label4: TLabel;
    dFin: TZetaFecha;
    CM_CODIGO: TcxGridDBColumn;
    CB_CODIGO: TcxGridDBColumn;
    WS_FECHA: TcxGridDBColumn;
    WS_CHECADA: TcxGridDBColumn;
    CH_RELOJ: TcxGridDBColumn;
    WS_MENSAJE: TcxGridDBColumn;
    BtnFiltrar_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtnFiltrar_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FParametros: TZetaParams;
    procedure ConfigAgrupamiento;
  protected
    { Protected declarations }
    procedure Connect; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
    procedure Refresh; override;
  public
    { Public declarations }
  end;

var
  BitacoraRegBio_DevEx: TBitacoraRegBio_DevEx;

implementation

uses
    dSistema,ZetaDialogo, DBaseSistema,ZBaseDlgModal_DevEx,FEditBitacoraRegBio_DevEx;  //@DC

{$R *.dfm}

{ TBitacoraRegBio }

procedure TBitacoraRegBio_DevEx.Connect;
begin
     inherited;
     with dmSistema do
     begin
          cdsEmpresasLookUp.Conectar;
          cdsTerminales.Conectar;
          luEmpresa.LookupDataset := cdsEmpresasLookUp;
          zTerminales.LookUpDataSet := cdsTerminales;
          {$ifndef DOS_CAPAS}
          with FParametros do
          begin
               AddDate( 'FechaInicio', dInicio.Valor + 1 );
               AddDate( 'FechaFin', dFin.Valor + 2 );
               AddString( 'Empresa', luEmpresa.Llave );
               AddString( 'Terminal', zTerminales.Llave );
               AddString( 'Mensaje', edtMensaje.Text );
          end;
          dmSistema.CargaBitacora( FParametros );
          {$endif}
          DataSource.DataSet := cdsBitacoraBio;
     end;
end;

procedure TBitacoraRegBio_DevEx.Refresh;
begin
     inherited;
     {$ifndef DOS_CAPAS}
     with FParametros do
     begin
          AddDate( 'FechaInicio', dInicio.Valor );
          AddDate( 'FechaFin', dFin.Valor );
          AddString( 'Empresa', luEmpresa.Llave );
          AddString( 'Terminal', zTerminales.Llave );
          AddString( 'Mensaje', edtMensaje.Text );
     end;
     dmSistema.CargaBitacora( FParametros );
     {$endif}
end;

procedure TBitacoraRegBio_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_TRESS_SISTEMA_TERMINALESGTI_BITACORATERMINALESGTI;
     FParametros := TZetaParams.Create;
end;

procedure TBitacoraRegBio_DevEx.FormDestroy(Sender: TObject);
begin
     FParametros.Free;
     dmSistema.cdsBitacoraBio.EmptyDataSet;
     inherited;
end;

{procedure TBitacoraRegBio_DevEx.BtnFiltrarClick(Sender: TObject);
begin
     inherited;
     DoRefresh;
end;}

procedure TBitacoraRegBio_DevEx.ConfigAgrupamiento;
begin
     //Cambio de configuracion
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Esconde la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     CB_CODIGO.Options.Grouping:= FALSE;
     WS_FECHA.Options.Grouping := FALSE;
     WS_CHECADA.Options.Grouping:= FALSE;
     WS_CHECADA.Options.Grouping:= FALSE;
     WS_MENSAJE.Options.Grouping:= FALSE;
end;

procedure TBitacoraRegBio_DevEx.FormShow(Sender: TObject);
begin
      ApplyMinWidth;
     {***Banda Sumatoria***}
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     //CreaColumaAgrupadaSumatoria( CB_TIPO.Name, skCount );
     inherited;
     dInicio.Valor := Now;
     dFin.Valor := Now;
     //DevEx (by am):Configuracion Especial para agrupamiento
     ConfigAgrupamiento;
end;

function TBitacoraRegBio_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'Esta acci�n no es permitida en la bit�cora';
end;

function TBitacoraRegBio_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'Esta acci�n no es permitida en la bit�cora';
end;

function TBitacoraRegBio_DevEx.PuedeModificar( var sMensaje: String ): Boolean; //@DC
begin
     dmsistema.cdsBitacoraBio.Modificar;
     Result := true;
end;

procedure TBitacoraRegBio_DevEx.BtnFiltrar_DevExClick(Sender: TObject);
begin
     inherited;
     DoRefresh;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

end.
