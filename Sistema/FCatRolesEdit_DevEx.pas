unit FCatRolesEdit_devEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask, Grids, DBGrids,
     ZetaDBTextBox,
     ZetaEdit,
     ZetaNumero,
     ZetaDBGrid,
     ZetaSmartLists, ComCtrls, ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons, cxContainer, cxEdit,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, cxTextEdit, cxMemo,
  cxDBEdit;

type
  TCatRolesEdit_DevEx = class(TBaseEdicion_DevEx)
    PanelControles: TPanel;
    RO_CODIGO: TZetaDBEdit;
    RO_NOMBRE: TDBEdit;
    RO_NOMBRElbl: TLabel;
    RO_CODIGOlbl: TLabel;
    RO_DESCRIP: TcxDBMemo;
    RO_DESCRIPlbl: TLabel;
    RO_NIVELlbl: TLabel;
    RO_NIVEL: TZetaDBNumero;
    dsUsuarios: TDataSource;
    PageControl: TcxPageControl;
    Usuarios: TcxTabSheet;
    PanelAgregar: TPanel;
    BorrarUsuario: TcxButton;
    AgregarUsuario: TcxButton;
    gridUsuarios: TZetaDBGrid;
    Modelos: TcxTabSheet;
    PanelModelos: TPanel;
    BorrarModelo: TcxButton;
    AgregarModelo: TcxButton;
    gridModelos: TZetaDBGrid;
    dsModelos: TDataSource;
    AgregarModelosTodos: TcxButton;
    BorrarModelosTodos: TcxButton;
    BorrarUsuariosTodos: TcxButton;
    AgregarUsuariosTodos: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure AgregarUsuarioClick(Sender: TObject);
    procedure BorrarUsuarioClick(Sender: TObject);
    procedure dsUsuariosDataChange(Sender: TObject; Field: TField);
    procedure dsModelosDataChange(Sender: TObject; Field: TField);
    procedure AgregarModeloClick(Sender: TObject);
    procedure BorrarModeloClick(Sender: TObject);
    procedure AgregarModelosTodosClick(Sender: TObject);
    procedure BorrarModelosTodosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AgregarUsuariosTodosClick(Sender: TObject);
    procedure BorrarUsuariosTodosClick(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
    procedure HabilitaControles; override;
  public
    { Public declarations }
  end;

var
  CatRolesEdit_DevEx: TCatRolesEdit_DevEx;

implementation

uses DSistema,
     ZetaCommonClasses,
     ZetaDialogo,
     ZetaBuscador_DevEx,
     ZAccesosTress;

{$R *.DFM}

procedure TCatRolesEdit_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_CAT_ROLES;
     IndexDerechos := D_CAT_ROLES;
     FirstControl := RO_CODIGO;
     {$ifdef WORKFLOWCFG}
     HelpContext := H_WORKFLOWCFG_CATALOGOS_ROLES_EDIC;     
     {$endif}
end;

procedure TCatRolesEdit_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := Usuarios;
end;

procedure TCatRolesEdit_DevEx.Connect;
begin
     with dmSistema do
     begin
          Datasource.Dataset := cdsRoles;
          dsUsuarios.Dataset := cdsRolUsuarios;
          //dsModelos.Dataset := cdsRolModelos;
     end;
end;

procedure TCatRolesEdit_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Rol', 'Rol', 'RO_CODIGO', dmSistema.cdsRoles );
end;


procedure TCatRolesEdit_DevEx.AgregarUsuarioClick(Sender: TObject);
var
   iUsuario: Integer;
   sNombre: String;
begin
     inherited;
     with dmSistema do
     begin
          if BuscarUsuario( '', iUsuario, sNombre ) then
          begin
               dmSistema.RolUsuarioAgregar( iUsuario, sNombre );
          end;
     end;
end;

procedure TCatRolesEdit_DevEx.BorrarUsuarioClick(Sender: TObject);
begin
     inherited;
     dmSistema.RolUsuarioBorrar;
end;

procedure TCatRolesEdit_DevEx.AgregarUsuariosTodosClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     if ZetaDialogo.zConfirm( 'Agregar Todos los Usuarios', '� Desea Agregar Todos Los Usuarios A Este Rol ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             dmSistema.RolUsuarioAgregarTodos;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TCatRolesEdit_DevEx.BorrarUsuariosTodosClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     if ZetaDialogo.zConfirm( 'Borrar Todos los Usuarios', '� Desea Remover Todos Los Usuarios De Este Rol ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             dmSistema.RolUsuarioBorrarTodos;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TCatRolesEdit_DevEx.dsUsuariosDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     BorrarUsuario.Enabled := not dsUsuarios.Dataset.IsEmpty;
     BorrarUsuariosTodos.Enabled := BorrarUsuario.Enabled;
end;

procedure TCatRolesEdit_DevEx.AgregarModeloClick(Sender: TObject);
{$ifdef FALSE}
var
   sModelo, sNombre: String;
{$endif}
begin
     inherited;
     {$ifdef FALSE}
     with dmSistema do
     begin
          if ModeloBuscar( '', sModelo, sNombre ) then
          begin
               dmSistema.RolModeloAgregar( sModelo, sNombre );
          end;
     end;
     {$endif}
end;

procedure TCatRolesEdit_DevEx.BorrarModeloClick(Sender: TObject);
begin
     inherited;
     {$ifdef FALSE}
     dmSistema.RolModeloBorrar;
     {$endif}
end;

procedure TCatRolesEdit_DevEx.dsModelosDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     {$IFDEF FALSE}
     BorrarModelo.Enabled := not dsModelos.Dataset.IsEmpty;
     BorrarModelosTodos.Enabled := BorrarModelo.Enabled;
     {$endif}
end;

procedure TCatRolesEdit_DevEx.AgregarModelosTodosClick(Sender: TObject);
{$ifdef FALSE}
var
   oCursor: TCursor;
{$endif}
begin
     inherited;
     {$ifdef FALSE}
     if ZetaDialogo.zConfirm( 'Agregar Todos los Modelos', '� Desea Agregar Todos Los Modelos A Este Rol ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             dmWorkFlow.RolModeloAgregarTodos;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
     {$endif}
end;

procedure TCatRolesEdit_DevEx.BorrarModelosTodosClick(Sender: TObject);
{$ifdef FALSE}
var
   oCursor: TCursor;
{$endif}
begin
     inherited;
     {$ifdef FALSE}
     if ZetaDialogo.zConfirm( 'Borrar Todos los Modelos', '� Desea Remover Todos Los Modelos De Este Rol ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             dmWorkFlow.RolModeloBorrarTodos;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
     {$endif}
end;


procedure TCatRolesEdit_DevEx.HabilitaControles;
begin
     inherited;
     AgregarUsuario.Enabled := not Editing ; 
end;

end.
