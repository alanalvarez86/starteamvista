unit FSistSolicitudEnvios_DevEx;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ZBaseConsulta, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  dxSkinsCore, TressMorado2013, cxControls, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons, ZetaKeyCombo,
  ZetaKeyLookup_DevEx, Vcl.Mask, ZetaFecha, ZetaCommonLists, ZetaCommonClasses, ZetaDialogo;

type
  TSistSolicitudEnvios_DevEx = class(TBaseConsulta)
    ZetaDBGrid: TZetaCXGrid;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    CA_FOLIO: TcxGridDBColumn;
    RE_CODIGO: TcxGridDBColumn;
    CAL_FECHA: TcxGridDBColumn;
    CAL_INICIO: TcxGridDBColumn;
    CAL_FIN: TcxGridDBColumn;
    CAL_ESTATUS: TcxGridDBColumn;
    CAL_MENSAJ: TcxGridDBColumn;
    ZetaDBGridLevel: TcxGridLevel;
    PanelProcesos: TPanel;
    ProcessStartLBL: TLabel;
    ProcessEndLBL: TLabel;
    ProcessLBL: TLabel;
    FechaInicial: TZetaFecha;
    FechaFinal: TZetaFecha;
    ActualizarRegistros: TcxButton;
    Estatus: TZetaKeyCombo;
    CancelarEnvio: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ZetaDBGridDBTableViewDblClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewDataControllerFilterGetValueList(Sender: TcxFilterCriteria; AItemIndex: Integer; AValueList: TcxDataFilterValueList);
    procedure ActualizarRegistrosClick(Sender: TObject);
    procedure CancelarEnvioClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewFocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
  private
    { Private declarations }
    procedure ConfigAgrupamiento;
  protected
    { Protected declarations}
    FParametros: TZetaParams;
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;

    procedure RefrescaDatos;
    procedure ApplyMinWidth; Dynamic;
  end;

var
  SistSolicitudEnvios_DevEx: TSistSolicitudEnvios_DevEx;

implementation

uses
    DCliente,
    DSistema,
    ZAccesosMgr,
    ZAccesosTress,
    ZGridModeTools,
    ZetaCommonTools;

{$R *.dfm}

procedure TSistSolicitudEnvios_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          FechaInicial.Valor := FechaDefault;
          FechaFinal.Valor := FechaDefault;
     end;

     with Estatus do
     begin
          ZetaCommonLists.LlenaLista( lfSolicitudEnvios, Items );
          Items.Insert( 0, 'Todos' );
          ItemIndex := 0;
     end;

     FParametros := TZetaParams.Create;
     HelpContext := H5005407_Solicitudes_Envios_Programados;
end;

procedure TSistSolicitudEnvios_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Refresh;  // Para que salga en el OnShow
     //Para que nunca muestre el filterbox inferior
     ZetaDBGridDBTableView.FilterBox.Visible := fvNever;
     //Para que no aparezca el Custom Dialog
     ZetaDBGridDBTableView.FilterBox.CustomizeDialog := False;
     // No permitir agrupacion y aparecer la caja de agrupacion
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := FALSE;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := FALSE;
end;

procedure TSistSolicitudEnvios_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     FParametros.Free;
end;

procedure TSistSolicitudEnvios_DevEx.Connect;
begin
     inherited;
     with dmSistema do
     begin
          cdsBitacoraReportes.Conectar;
          cdsSistTareaCalendarioLookup.Conectar;
          cdsSistSolicitudEnvios.Conectar;
          DataSource.DataSet := cdsSistSolicitudEnvios;
     end;
     Refresh;
end;

procedure TSistSolicitudEnvios_DevEx.Refresh;
var
   iTipo: Integer;
begin
     iTipo := Estatus.ItemIndex - 1;

     with FParametros do
     begin
          AddInteger( 'Tipo', iTipo );
          AddDate( 'FechaInicial', FechaInicial.Valor );
          AddDate( 'FechaFinal', FechaFinal.Valor );
     end;
     RefrescaDatos;

     {***DevEx: Metodo agregado para asignar el MinWidth ideal a las columnas.
     Se invoca despues de definir las columnas del grid asi como su titulo. Posteriormente se aplica el BestFit.***}
     ApplyMinWidth;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TSistSolicitudEnvios_DevEx.ZetaDBGridDBTableViewDataControllerFilterGetValueList( Sender: TcxFilterCriteria; AItemIndex: Integer; AValueList: TcxDataFilterValueList);
begin
     inherited;
     ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if ZetaDBGridDBTableView.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( ZetaDBGridDBTableView, AItemIndex, AValueList );

end;

procedure TSistSolicitudEnvios_DevEx.ActualizarRegistrosClick(Sender: TObject);
begin
     inherited;
     DoRefresh;
     //ZetaDBGrid1.SetFocus;
     ZetaDBGrid.SetFocus;
     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     //Se vuleve a llamar porque se refrescan os datos.
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TSistSolicitudEnvios_DevEx.CancelarEnvioClick(Sender: TObject);
var sNombre, sError : String;
    iEstatus, iFolio : Integer;
begin
     with dmSistema do
     begin
          sNombre := cdsSistSolicitudEnvios.FieldByName ('CA_NOMBRE').AsString;
          iEstatus := cdsSistSolicitudEnvios.FieldByName ('CAL_ESTATUS').AsInteger;
          iFolio := cdsSistSolicitudEnvios.FieldByName ('CAL_FOLIO').AsInteger;
          if (iEstatus = ord(tcSEPendiente)) or (iEstatus = ord(tcSEProcesando)) then
          begin
               if ZConfirm(Self.Caption,'�Desea cancelar el env�o programado: ' + sNombre + '?',0 , mbYes ) then
               begin
                    sError := CancelarSolicitudEnvio;
                    if StrLleno(sError) then
                       ZError(Self.Caption, sError, 0)
                    else
                    begin
                         ZInformation(Self.Caption, 'Se ha solicitado la cancelaci�n del env�o: ' + sNombre + '.', 0);
                         RefrescaDatos;
                         cdsSistSolicitudEnvios.Locate( 'CAL_FOLIO', iFolio,[] );
                    end;
               end;
          end
          else
              ZError(Self.Caption, 'Solo se pueden cancelar env�os que se encuentren en estado Pendiente o Procesando.', 0);
     end;
end;

function TSistSolicitudEnvios_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No se puede agregar un registro';
end;

function TSistSolicitudEnvios_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No se permite borrar registro';
end;

function TSistSolicitudEnvios_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= True;
end;

procedure TSistSolicitudEnvios_DevEx.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =40;
begin
     with  ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;
end;

procedure TSistSolicitudEnvios_DevEx.Agregar;
begin
     inherited;
end;

procedure TSistSolicitudEnvios_DevEx.Modificar;
begin
     inherited;
     dmSistema.cdsSistSolicitudEnvios.Modificar;
end;

procedure TSistSolicitudEnvios_DevEx.Borrar;
begin
     inherited;
end;

procedure TSistSolicitudEnvios_DevEx.ConfigAgrupamiento;
begin
     //Cambio de configuracion
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := false;//Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := false; //Esconde la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     // EE_FOLIO.Options.Grouping:= FALSE;
    { EE_TO.Options.Grouping:= FALSE;
     EE_SUBJECT.Options.Grouping:= FALSE;
     EE_FROM_NA.Options.Grouping:= FALSE;
     EE_FROM_AD.Options.Grouping:= FALSE;
     EE_STATUS.Options.Grouping:= FALSE;
     EE_ENVIADO.Options.Grouping:= FALSE;
     EE_FEC_IN.Options.Grouping:= FALSE;
     EE_LOG.Options.Grouping:= FALSE; }
end;


procedure TSistSolicitudEnvios_DevEx.RefrescaDatos;
begin
     dmSistema.RefrescarSolicitudEnvios( FParametros );
end;

procedure TSistSolicitudEnvios_DevEx.ZetaDBGridDBTableViewDblClick(Sender: TObject);
begin
     inherited;
     DoEdit;
end;


procedure TSistSolicitudEnvios_DevEx.ZetaDBGridDBTableViewFocusedRecordChanged(Sender: TcxCustomGridTableView; APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
const K_CERO = 0;
begin
     inherited;
     CancelarEnvio.Enabled := (ZetaDBGridDBTableView.DataController.DataRowCount <> K_CERO)
                               and (
                               (dmSistema.cdsSistSolicitudEnvios.FieldByName ('CAL_ESTATUS').AsInteger = ord(tcSEPendiente)) or
                               (dmSistema.cdsSistSolicitudEnvios.FieldByName ('CAL_ESTATUS').AsInteger = ord(tcSEProcesando)));
end;

end.
