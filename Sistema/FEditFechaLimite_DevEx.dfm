inherited EditFechaLimite_DevEx: TEditFechaLimite_DevEx
  Left = 340
  Top = 318
  Caption = 'Fecha L'#237'mite De Cambios A Tarjetas'
  ClientHeight = 141
  ClientWidth = 354
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object FechaLimiteLBL: TLabel [0]
    Left = 7
    Top = 17
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Caption = '&Fecha L'#237'mite:'
  end
  object Label1: TLabel [1]
    Left = 11
    Top = 40
    Width = 61
    Height = 13
    Alignment = taRightJustify
    Caption = 'Comentarios:'
  end
  object GL_DESCRIP: TZetaTextBox [2]
    Left = 75
    Top = 38
    Width = 270
    Height = 60
    AutoSize = False
    ShowAccelChar = False
    WordWrap = True
    Brush.Color = clBtnFace
    Border = True
  end
  inherited PanelBotones: TPanel
    Top = 105
    Width = 354
    TabOrder = 1
    inherited OK_DevEx: TcxButton
      Left = 188
      OnClick = OK_DevExClick
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 268
    end
  end
  object FechaLimite: TZetaFecha [4]
    Left = 75
    Top = 12
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 0
    Text = '14-Jan-04'
    Valor = 38000.000000000000000000
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
