unit FSistEditUsuarioSuscribe;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, Buttons, ExtCtrls, DBCtrls, Db, Grids, DBGrids,
  ZetaDBGrid, ZetaMessages;

type
  TFormaSuscribeUsuario = class(TZetaDlgModal)
    Panel1: TPanel;
    Label1: TLabel;
    dsUsuario: TDataSource;
    DBText1: TDBText;
    DBText2: TDBText;
    btnAgregar: TBitBtn;
    btnBorrar: TBitBtn;
    dsSuscrip: TDataSource;
    ZetaDBGrid1: TZetaDBGrid;
    btnModificar: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure btnAgregarClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnBorrarClick(Sender: TObject);
    procedure btnModificarClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    function ModificarSuscripcion : Boolean;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
  public
    { Public declarations }
  end;

var
  FormaSuscribeUsuario: TFormaSuscribeUsuario;

implementation

uses dSistema, FLookupReporte, FSistEditSuscribe, ZetaCommonLists;

{$R *.DFM}

procedure TFormaSuscribeUsuario.FormShow(Sender: TObject);
begin
     inherited;
     dsSuscrip.Dataset := dmSistema.cdsSuscrip;
     dmSistema.cdsSuscrip.Refrescar;
end;

procedure TFormaSuscribeUsuario.btnAgregarClick(Sender: TObject);
begin
     inherited;
     with FormaLookupReporte do
     begin
          if ShowModal = mrOk then
          begin
               with dmSistema.cdsSuscrip do
               begin
                    Append;
                    FieldByName( 'RE_CODIGO' ).AsInteger := NumReporte;
                    FieldByName( 'RE_NOMBRE' ).AsString  := NombreReporte;
                    FieldByName( 'US_CODIGO' ).AsInteger := dmSistema.UsuarioPosicionado;
                    ModificarSuscripcion;
               end;
          end;
     end;
end;

procedure TFormaSuscribeUsuario.FormDestroy(Sender: TObject);
begin
  FreeAndNil( FormaLookupReporte );
  FreeAndNil( FormaEditSuscrip );
  inherited;
end;

procedure TFormaSuscribeUsuario.FormCreate(Sender: TObject);
begin
  inherited;
  FormaLookupReporte := TFormaLookupReporte.Create( self );
  FormaEditSuscrip   := TFormaEditSuscrip.Create( self );
end;

procedure TFormaSuscribeUsuario.btnBorrarClick(Sender: TObject);
begin
  inherited;
  with dmSistema.cdsSuscrip do
    if not IsEmpty then
        Delete;
end;

function TFormaSuscribeUsuario.ModificarSuscripcion: Boolean;
begin
     with FormaEditSuscrip do
     begin
          ShowModal;
          Result := ModalResult = mrOk;
     end;
end;

procedure TFormaSuscribeUsuario.btnModificarClick(Sender: TObject);
begin
  inherited;
  ModificarSuscripcion;
end;

procedure TFormaSuscribeUsuario.WMExaminar(var Message: TMessage);
begin
    ModificarSuscripcion;
end;

procedure TFormaSuscribeUsuario.OKClick(Sender: TObject);
begin
  inherited;
  dmSistema.cdsSuscrip.Enviar;
end;

end.
