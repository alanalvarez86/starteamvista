unit FGlobalTimbrado;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     ZBaseGlobal, ZBaseGlobal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore,  TressMorado2013, dxSkinsDefaultPainters,
  ImgList, cxButtons;

type
  TGlobalTimbrado = class(TBaseGlobal_DevEx)
    EnviarDetalleTimbrado: TCheckBox;
    EnviarIncapacidadesTimbrado: TCheckBox;
    EnviarTiempoExtraTimbrado: TCheckBox;
    UsarDiasPeriodoComoPeriodicidad: TCheckBox;
    ConsiderarTransferidos: TCheckBox;
    DetalleSubcontrato: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GlobalTimbrado: TGlobalTimbrado;

implementation

{$R *.DFM}

uses ZGlobalTress,
     ZAccesosTress,
     ZetaCommonClasses;

procedure TGlobalTimbrado.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_CAT_CONFI_NOMINA;
     EnviarDetalleTimbrado.Tag :=  K_GLOBAL_TIMBRADO_ENVIAR_DETALLE_5;
     EnviarIncapacidadesTimbrado.Tag :=  K_GLOBAL_TIMBRADO_ENVIAR_INCAPACIDADES_4;
     EnviarTiempoExtraTimbrado.Tag :=  K_GLOBAL_TIMBRADO_ENVIAR_TIEMPO_EXTRA_3;
     UsarDiasPeriodoComoPeriodicidad.tag := K_GLOBAL_TIMBRADO_USAR_PERIODICIDAD_2;
     ConsiderarTransferidos.tag := K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1;
     DetalleSubcontrato.Tag := K_GLOBAL_TIMBRADO_DETALLE_SUBCONTRATO_6;
     HelpContext := H65116_Globales_de_Configuracion_Timbrado;
end;

procedure TGlobalTimbrado.FormShow(Sender: TObject);
begin
  inherited;
  EnviarDetalleTimbrado.SetFocus;
end;

end.
