inherited GlobalNiveles_DevEx: TGlobalNiveles_DevEx
  Left = 893
  Top = 238
  ActiveControl = NombreNivel1
  Caption = #193'reas'
  ClientHeight = 329
  ClientWidth = 365
  PixelsPerInch = 96
  TextHeight = 13
  object Label12: TLabel [0]
    Left = 25
    Top = 11
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel 1:'
  end
  object Label1: TLabel [1]
    Left = 25
    Top = 35
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel 2:'
  end
  object Label2: TLabel [2]
    Left = 25
    Top = 58
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel 3:'
  end
  object Label3: TLabel [3]
    Left = 25
    Top = 82
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel 4:'
  end
  object Label4: TLabel [4]
    Left = 25
    Top = 105
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel 5:'
  end
  object Label5: TLabel [5]
    Left = 25
    Top = 129
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel 6:'
  end
  object Label6: TLabel [6]
    Left = 25
    Top = 152
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel 7:'
  end
  object Label7: TLabel [7]
    Left = 25
    Top = 176
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel 8:'
  end
  object Label8: TLabel [8]
    Left = 25
    Top = 199
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel 9:'
  end
  object NIVEL12lbl: TLabel [9]
    Left = 19
    Top = 268
    Width = 42
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel 12:'
    Visible = False
  end
  object NIVEL11lbl: TLabel [10]
    Left = 19
    Top = 245
    Width = 42
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel 11:'
    Visible = False
  end
  object NIVEL10lbl: TLabel [11]
    Left = 19
    Top = 222
    Width = 42
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel 10:'
    Visible = False
  end
  inherited PanelBotones: TPanel
    Top = 293
    Width = 365
    inherited OK_DevEx: TcxButton
      Left = 200
      Top = 5
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 280
      Top = 5
    end
  end
  object NombreNivel1: TEdit [13]
    Tag = 13
    Left = 64
    Top = 7
    Width = 282
    Height = 21
    MaxLength = 25
    TabOrder = 1
  end
  object NombreNivel2: TEdit [14]
    Tag = 14
    Left = 64
    Top = 31
    Width = 282
    Height = 21
    MaxLength = 25
    TabOrder = 2
  end
  object NombreNivel3: TEdit [15]
    Tag = 15
    Left = 64
    Top = 54
    Width = 282
    Height = 21
    MaxLength = 25
    TabOrder = 3
  end
  object NombreNivel4: TEdit [16]
    Tag = 16
    Left = 64
    Top = 78
    Width = 282
    Height = 21
    MaxLength = 25
    TabOrder = 4
  end
  object NombreNivel5: TEdit [17]
    Tag = 17
    Left = 64
    Top = 101
    Width = 282
    Height = 21
    MaxLength = 25
    TabOrder = 5
  end
  object NombreNivel6: TEdit [18]
    Tag = 18
    Left = 64
    Top = 125
    Width = 282
    Height = 21
    MaxLength = 25
    TabOrder = 6
  end
  object NombreNivel7: TEdit [19]
    Tag = 19
    Left = 64
    Top = 148
    Width = 282
    Height = 21
    MaxLength = 25
    TabOrder = 7
  end
  object NombreNivel8: TEdit [20]
    Tag = 20
    Left = 64
    Top = 172
    Width = 282
    Height = 21
    MaxLength = 25
    TabOrder = 8
  end
  object NombreNivel9: TEdit [21]
    Tag = 21
    Left = 64
    Top = 195
    Width = 282
    Height = 21
    MaxLength = 25
    TabOrder = 9
  end
  object NombreNivel12: TEdit [22]
    Tag = 236
    Left = 64
    Top = 264
    Width = 282
    Height = 21
    MaxLength = 25
    TabOrder = 12
    Visible = False
  end
  object NombreNivel11: TEdit [23]
    Tag = 235
    Left = 64
    Top = 241
    Width = 282
    Height = 21
    MaxLength = 25
    TabOrder = 11
    Visible = False
  end
  object NombreNivel10: TEdit [24]
    Tag = 234
    Left = 64
    Top = 218
    Width = 282
    Height = 21
    MaxLength = 25
    TabOrder = 10
    Visible = False
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
