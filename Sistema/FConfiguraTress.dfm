�
 TFORMACONFIGURA 0|�  TPF0�TFormaConfiguraFormaConfiguraLeft�TopVCaption   Configuración de CafeteríaClientHeight�ClientWidth|ExplicitWidth�ExplicitHeight�PixelsPerInch`
TextHeight �TPanelPanelBotonesTopkWidth|ExplicitTopkExplicitWidth| �	TcxButtonOK_DevExLeft�Top
ParentFontExplicitLeft�ExplicitTop  �	TcxButtonCancelar_DevExLeft$TopExplicitLeft$ExplicitTop   �TPanelPanelIdentificaTop Width|VisibleExplicitTop ExplicitWidth| �TPanelValorActivo1 �TLabeltextoValorActivo1Width=   �TPanelValorActivo2Width6ExplicitWidth6 �TLabeltextoValorActivo2LeftWidth0ExplicitLeft�     �TcxDBNavigatorDevEx_cxDBNavigatorEdicionLeftTopExplicitLeftExplicitTop  �TPanelPanel1TopWidth|HeightVisibleExplicitTopExplicitWidth|ExplicitHeight  �TcxPageControlPageControlTop%Width|HeightF
ParentFontProperties.ActivePageTablaExplicitTop%ExplicitWidth|ExplicitHeightFClientRectBottomDClientRectRightz �TcxTabSheetDatosExplicitLeftExplicitTopExplicitWidthxExplicitHeight) TdxCheckGroupBoxcbUsaTecladoLeft�TopCaptionUsa Teclado: Ctl3DParentCtl3DProperties.OnChangecbUsaTecladoPropertiesChangeTabOrderOnClickcbUsaTecladoClickHeightQWidth�  TDBCheckBoxcbTipoComidaLeftTopWidth� HeightCaptionPreguntar Tipo de ComidaCtl3D	DataField
CF_PRE_TIP
DataSource
DataSourceParentCtl3DTabOrder ValueCheckedSValueUncheckedN  TDBCheckBox
cbCantidadLeftTop*Width� HeightCaptionPreguntar Cantidad	DataField
CF_PRE_QTY
DataSource
DataSourceTabOrderValueCheckedSValueUncheckedN   	TGroupBoxLetreroLeft Top� WidthxHeightUAlignalBottomCaption
 Letrero: Ctl3DFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ParentCtl3D
ParentFontTabOrder TLabelLabel3LeftTop8Width2HeightCaption
Velocidad:  TDBEditeditLetreroLeftTopWidth�HeightCtl3D		DataField	CF_BANNER
DataSource
DataSourceParentCtl3DTabOrder   TcxTrackBartbVelocidad_DevExLeftHTop/Properties.OnChange!tbVelocidad_DevExPropertiesChangeTabOrderHeight#Width�    TPanelPanelGeneralLeftTopWidth�Height� 
BevelOuterbvNoneCtl3D	ParentCtl3DTabOrder 
DesignSize��   TLabelLabel1Left&TopWidthMHeightCaptionTipo de Comida:  TLabelLabel4LeftGTop@Width,HeightCaption
   Estación:  TLabelLabel15LeftTop[WidthoHeight	AlignmenttaRightJustifyCaption   Checadas Simultáneas:  TLabellblTipoComidaLeft� TopWidthBHeightCaptionlblTipoComida  TLabelLabel30Left*Top,WidthIHeight	AlignmenttaRightJustifyCaptiona media noche:  TLabelLabel31LeftTopWidth]Height	AlignmenttaRightJustifyCaptionReiniciar Consumos  TLabelLabel32LeftTopsWidth[Height	AlignmenttaRightJustifyCaptionLimpiar pantalla en:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel36Left� TopsWidth.Height	AlignmenttaRightJustifyCaptionsegundosFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TZetaDBKeyComboSimultaneasLeftvTopVWidth� Height	BevelKindbkFlatStylecsDropDownListCtl3DParentCtl3DTabOrder	ListaFijalfChecadasSimultaneasListaVariablelvPuestoOffset OpcionalEsconderVacios	DataField
CF_CHECK_S
DataSource
DataSourceLlaveNumerica	  	TGroupBox	DefaultGBLeft Top� Width�HeightAAlignalCustomCaption Defaults: Ctl3DFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ParentCtl3D
ParentFontTabOrder TLabelLabel18LeftGTopWidth,Height	AlignmenttaRightJustifyCaptionEmpresa:  TLabelLabel19Left>Top*Width5Height	AlignmenttaRightJustifyCaptionCredencial:  TDBEditedDefaultEmpresaLeftvTopWidthHeightCtl3D		DataField
CF_DEF_COM
DataSource
DataSource	MaxLengthParentCtl3DTabOrder   TDBEditedDefaultCredencialLeftvTop&WidthHeightCtl3D		DataField
CF_DEF_CRE
DataSource
DataSource	MaxLengthParentCtl3DTabOrder   TDBCheckBoxchkReiniciaMediaNocheLeftuTop%WidthHeight	DataField
CF_REINICI
DataSource
DataSourceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderValueCheckedSValueUncheckedN  TZetaDBNumeroeditTiempoEsperaLeftvToppWidthHeightAnchorsakTopakRight Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style MascaramnDias	MaxLength
ParentFontTabOrderText0	DataFieldCF_T_ESP
DataSource
DataSource  TDBEdit	CF_NOMBRELeftvTop;Width� HeightColorclInfoBkCtl3D		DataField	CF_NOMBRE
DataSource
DataSource	MaxLengthParentCtl3DReadOnly	TabOrder  TcxTextEditeditTipoLeftuTopProperties.MaxLengthProperties.OnChangeeditTipoChangeTabOrder Text1Width  TcxSpinButtonUDTipoComida_DevExLeft� Top	AssociateeditTipoAutoSizeProperties.MaxValue       �@Properties.MinValue       ��?Properties.OnChangeeditTipoChangeTabOrderValueHeightWidth   	TGroupBoxVistaLeft�Top\Width� Height]Caption    Vista de Información: Ctl3DFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ParentCtl3D
ParentFontTabOrder TDBCheckBoxcbMostrarFotoLeftTopWidthaHeightCaptionMostrar FotoCtl3D		DataFieldCF_FOTO
DataSource
DataSourceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ParentCtl3D
ParentFontTabOrder ValueCheckedSValueUncheckedN  TDBCheckBoxcbMostrarComidasLeftTop-WidthaHeightCaptionMostrar ComidasCtl3D		DataFieldCF_COM
DataSource
DataSourceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ParentCtl3D
ParentFontTabOrderValueCheckedSValueUncheckedN  TDBCheckBoxcbMostrarSigComidaLeftTopFWidth� HeightCaptionMostrar siguiente comidaCtl3D		DataField
CF_SIG_COM
DataSource
DataSourceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ParentCtl3D
ParentFontTabOrderValueCheckedSValueUncheckedN    �TcxTabSheetTablaCaption
CalendarioExplicitLeftExplicitTopExplicitWidthxExplicitHeight) �TZetaDBGridGridRenglonesTopFWidthxHeight� Ctl3DOptions	dgEditingdgTitlesdgIndicator
dgColLines
dgRowLinesdgTabsdgCancelOnExit ParentCtl3DTabOrder	OnColExitGridRenglonesColExitColumnsExpanded	FieldNameHORATitle.CaptionHoraWidth(Visible	 Expanded	FieldNameACCIONTitle.Caption   AcciónWidth� Visible	 Expanded	FieldNameCONTADORTitle.AlignmenttaCenterTitle.CaptionIniciar contadorWidthPVisible	 Expanded	FieldNameSYNCTitle.AlignmenttaCenterTitle.CaptionSinc. HuellasWidthPVisible	 Expanded	FieldNameSEMANATitle.CaptionSemanaVisible    �TPanelPanel2Top)WidthxTabOrderExplicitTop)ExplicitWidthx  TPanelpCalendarioLeft Top WidthxHeight)AlignalBottom
BevelOuterbvNoneTabOrder TLabelLabel8LeftTopWidthkHeightCaptionArchivo de Calendario:Visible  	TcxButtonArchivoSeekCalendario_DevExTagLeftTTopWidthHeightHint)Indicar Archivo para Almacenar CalendarioOptionsImage.ImageIndex OptionsImage.ImagescxImageList19_PanelBotonesParentShowHintShowHint	TabOrderVisible  TDBEditEditCalendario_DevExLeftvTopWidth�HeightTabOrder Visible   TPanelpEventosLeft Top WidthxHeight)AlignalTop
BevelOuterbvNoneTabOrder  TLabelLabel29LeftTopWidth9HeightCaptionEventos de:  TZetaKeyCombocbSemanaLeftPTopWidth� HeightAutoComplete	BevelKindbkFlatStylecsDropDownListCtl3DParentCtl3DTabOrder OnChangecbSemanaChange	ListaFijalfDiasCafeteriaListaVariablelvPuestoOffset OpcionalEsconderVacios   TZetaDBKeyComboZComboLeft� Top� Width� HeightAutoComplete	BevelKindbkFlatStylecsDropDownListCtl3DParentCtl3DTabOrderTabStopVisible	ListaFijalfCafeCalendarioListaVariablelvPuestoOffset OpcionalEsconderVacios	DataFieldACCION
DataSource	dsRenglonLlaveNumerica	   TcxTabSheetComidaCaptionTipo Comida
ImageIndex
DesignSizex)  TLabelLabel20Left0TopWidth]Height	AlignmenttaRightJustifyCaptionTipo de Comida #1:  TLabelLabel21Left0Top,Width]Height	AlignmenttaRightJustifyCaptionTipo de Comida #2:  TLabelLabel22Left0TopDWidth]Height	AlignmenttaRightJustifyCaptionTipo de Comida #3:  TLabelLabel23Left0Top\Width]Height	AlignmenttaRightJustifyCaptionTipo de Comida #4:  TLabelLabel24Left0ToptWidth]Height	AlignmenttaRightJustifyCaptionTipo de Comida #5:  TLabelLabel25Left0Top� Width]Height	AlignmenttaRightJustifyCaptionTipo de Comida #6:  TLabelLabel26Left0Top� Width]Height	AlignmenttaRightJustifyCaptionTipo de Comida #7:  TLabelLabel27Left0Top� Width]Height	AlignmenttaRightJustifyCaptionTipo de Comida #8:  TLabelLabel28Left0Top� Width]Height	AlignmenttaRightJustifyCaptionTipo de Comida #9:  TDBEditeditTipoComida1Left� TopWidth�HeightAnchorsakLeftakTopakRight 	DataField	CF_TCOM_1
DataSource
DataSourceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength
ParentFontTabOrder   TDBEditeditTipoComida2Left� Top(Width�HeightAnchorsakLeftakTopakRight 	DataField	CF_TCOM_2
DataSource
DataSource	MaxLengthTabOrder  TDBEditeditTipoComida3Left� Top@Width�HeightAnchorsakLeftakTopakRight 	DataField	CF_TCOM_3
DataSource
DataSource	MaxLengthTabOrder  TDBEditeditTipoComida4Left� TopXWidth�HeightAnchorsakLeftakTopakRight 	DataField	CF_TCOM_4
DataSource
DataSource	MaxLengthTabOrder  TDBEditeditTipoComida5Left� ToppWidth�HeightAnchorsakLeftakTopakRight 	DataField	CF_TCOM_5
DataSource
DataSource	MaxLengthTabOrder  TDBEditeditTipoComida6Left� Top� Width�HeightAnchorsakLeftakTopakRight 	DataField	CF_TCOM_6
DataSource
DataSource	MaxLengthTabOrder  TDBEditeditTipoComida7Left� Top� Width�HeightAnchorsakLeftakTopakRight 	DataField	CF_TCOM_7
DataSource
DataSource	MaxLengthTabOrder  TDBEditeditTipoComida8Left� Top� Width�HeightAnchorsakLeftakTopakRight 	DataField	CF_TCOM_8
DataSource
DataSource	MaxLengthTabOrder  TDBEditeditTipoComida9Left� Top� Width�HeightAnchorsakLeftakTopakRight 	DataField	CF_TCOM_9
DataSource
DataSource	MaxLengthTabOrder   TcxTabSheet	SeguridadCaption	Seguridad
ImageIndex
DesignSizex)  	TGroupBox	GroupBox2LeftTopWidthdHeight� AnchorsakLeftakTopakRight Caption Datos del Administrador: Ctl3DParentCtl3DTabOrder 
DesignSized�   TLabelLabel11LeftLTopWidth#Height	AlignmenttaRightJustifyCaptionGafete:  TLabelLabel12LeftTop0WidthiHeight	AlignmenttaRightJustifyCaptionClave para Configurar:  TDBEditeGafeteAdmonLeftuTopWidth�HeightAnchorsakLeftakTopakRight Ctl3D		DataField	CF_GAFETE
DataSource
DataSourceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ParentCtl3D
ParentFontTabOrder   TDBEditeClaveAdmonLeftuTop,Width�HeightAnchorsakLeftakTopakRight Ctl3D		DataFieldCF_CLAVE
DataSource
DataSourceParentCtl3DPasswordChar*TabOrder  TDBCheckBoxcbConcesionImprimirLeftuTopHWidthaHeightCaptionPuede ImprimirCtl3D		DataField
CF_A_PRINT
DataSource
DataSourceParentCtl3DTabOrderValueCheckedSValueUncheckedN  TDBCheckBoxcbConcesionCancelarLeftuTopXWidth� HeightCaptionPuede Cancelar ComidasCtl3D		DataField	CF_A_CANC
DataSource
DataSourceParentCtl3DTabOrderValueCheckedSValueUncheckedN     �TDataSource
DataSourceLeft�Top  �TcxImageListcxImageList24_PanelBotonesFormatVersion
DesignInfo� �	ImageInfo
Image.Data
:	  6	  BM6	      6   (                 	                  GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��������������JZ��GW��GW��GW��GW��GW��GW��GW��GW��JZ��������������GW��GW��GW��GW��GW��GW��GW��GW������������������JZ��GW��GW��GW��GW��GW��GW��JZ������������������GW��GW��GW��GW��GW��GW��GW��GW����������������������JZ��GW��GW��GW��GW��JZ����������������������GW��GW��GW��GW��GW��GW��GW��GW��GW����������������������JZ��GW��GW��JZ����������������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW����������������������JZ��JZ����������������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW����������������������������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��������������������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��JZ��������������������������JZ��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��JZ����������������������������������JZ��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��JZ������������������������������������������JZ��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��JZ����������������������GW��GW����������������������JZ��GW��GW��GW��GW��GW��GW��GW��GW��GW��JZ����������������������GW��GW��GW��GW����������������������JZ��GW��GW��GW��GW��GW��GW��GW��GW����������������������GW��GW��GW��GW��GW��GW����������������������GW��GW��GW��GW��GW��GW��GW��GW������������������GW��GW��GW��GW��GW��GW��GW��GW������������������GW��GW��GW��GW��GW��GW��GW��GW��������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��	Mask.Data
�   �   BM�       >   (               `                      ���                                                                                                  
Image.Data
:	  6	  BM6	      6   (                 	                  Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�eء�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ��������TӖ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�eء�������������Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Rӕ�����������������pڧ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�������������������������Rӕ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�xݬ�����������������������������Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Z՚���������������������������������bן�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ����������������Oғ�TӖ�����������������Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ��������j٤�Oғ�Oғ�Oғ�_֝����������������Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Rӕ�Oғ�Oғ�Oғ�Oғ�Oғ�eء�������������hآ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�j٤�������������Rӕ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�xݬ�������������Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�pڧ�������������Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�eء������������Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�WԘ������������Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ��߱����Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�Oғ�	Mask.Data
�   �   BM�       >   (               `                      ���                                                                                                  
Image.Data
:	  6	  BM6	      6   (                 	                  GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��Ra��ly����������������������o|��u���dq��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW������dq������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW����������������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW��o|������������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW������GW��iv��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������GW��GW������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW������������������������������������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��Ud��u�������������������GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��GW��	Mask.Data
�   �   BM�       >   (               `                      ���                                                                                                  
Image.Data
:	  6	  BM6	      6   (                 	                  Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Sӕ�������������������������������������������������[՚�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ��߳�������������������������������������������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ��������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ���������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ��������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ���������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ��������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ���������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ��������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ���������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ��������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ���������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ��������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ���������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ��������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ���������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ��������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ���������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ��������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ���������������������vܫ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ��������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ��������������������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ��������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ����������������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ��������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ������������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�ް�������������������������������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ���������������������������������Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ�Pғ� 
Image.Data
:	  6	  BM6	      6   (                 	                  HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��������������������������������������������������HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��������������������������������������������������HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��������������HX������������������HX��������������HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��������������HX������������������HX��������������HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��������������HX������������������HX��������������HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��������������HX������������������HX��������������HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��������������HX������������������HX��������������HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��������������HX������������������HX��������������HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��������������HX������������������HX��������������HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��������������������������������������������������HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��������������������������������������������������HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX����������������������������������������������������������HX��HX��HX��HX��HX��HX��HX��HX��HX��HX����������������������������������������������������������HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��������������������������HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��������������������������HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX��HX�� 
Image.Data
:	  6	  BM6	      6   (                 	                   ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���\�������<��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���`���������������\������ ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���x���������������p������ ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ������������������ ������8��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���8������ ��� ��������������� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���\�������������������\��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���������������������������������� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ������������������������������� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� �������������������������������$��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���������������������������������� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���`�����������������������4��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���������������X��� ��� ��������� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���<���|������ ������p�������t��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���P���������������h��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ����������������������<��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���`�������\��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� 
Image.Data
:	  6	  BM6	      6   (                 	                   ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���d������������������������������������������������������� ��� ��� ��� ��� ��� ��� ��� ��� ��� ����������������������������������������������������������� ��� ��� ��� ��� ��� ��� ��� ��� ��� ����������������������������������������������������������� ��� ��� ��� ��� ��� ��� ��� ��� ��� �����������������������������������������������������������L��� ��� ��� ��� ��� ��� ��� ��� ���P�����������������������������������������������������������t��� ��� ��� ��� ��� ��� ��� ��� ���x��������������������������������������������������������������� ��� ��� ��� ��� ��� ��� ��� ������������������������������������������������������������������� ��� ��� ��� ��� ��� ��� ��� ������������������������������������������������������������������� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ����������������������������������������������������������� ��� ��� ��� ��� ��� ��� ��� ��� ��� ����������������������������������������������������������� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��������������������������� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���d�������������������x��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� 
Image.Data
:	  6	  BM6	      6   (                 	                  [���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[�����������[�����������]���\���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[�����������y�����������h�����������[���[���[���[���[���[���[���[���[���[���[���[���[�������`�������������������e�������������������r���[���[���[���[���[���[���[���[���[���[���[����������~�������r�������c�����������l�����������r���[���[���[���[���[���[���[���[���[���[���[�������h�������j�������c�������������������t�����������p���[���[���[���[���[���[���[���[���[���[�������c�������r����������������������h�����������u���[���[���[���[���[���[���[���[���[���[���`���������������������������e�������h�������w���z�������z���[���[���[���[���[���[���[���[���[���]���������������������������h���a�������������������e���[���[���[���[���[���[���[���[���[���[���[�������g�������z�������r���������������r�������[�������m���[���[���[���[���[���[���[���[���[���[�������e�������i�����������^���h�����������j�����������[���[���[���[���[���[���[���[���[���e���������������y�������g�������������������c�����������\���[���[���[���[���[���[���[���[���[���[�������i�������y���������������b���_���������������_���[���[���[���[���[���[���[���[���[���[���[���u�������j�����������`�������������������e�����������e���[���[���[���[���[���[���[���[���[���[���[�����������[������������������|���������������`���[���[���[���[���[���[���[���[���[���[���[���[���[�����������u���\�������������������c���[���[���[���[���[���[���[���[���[���[���[���[���[���[���w�������l�������������������������������[���[���[���[���[���[���[���[���[���[���[���[���[���[���]���������������`���p������h���]���]���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���h�������������������u���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���^���]���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���    �TdxBarManagerDevEx_BarManagerEdicionLeft� Top�DockControlHeights      �TdxBarBarSuperiorVisible   �TcxImageListcxImageList16EdicionFormatVersion
DesignInfo8 �	ImageInfo
Image.Data
:  6  BM6      6   (                                                      |I+��`$�p+�l@�7 � @                            	X�p+��4��4��4��4��4��4��4�I+�   $            `8�sD��4��4��["�xL D
l`8��4��4��V!�           �V!��4��4�Y5�                       H�w-��4�l@�       �p+��4��4�؄1� D                         @�4��4�L    �4��p+�(�                                   `8��4�?%�   t                                           L`8�S2�                                                                 8 @                                                       f<��4�?%�                                           `   (d�4��~/�                                 ,+	��p+��4�       �~/��4�S2�                         ,�`$��4��4��4�       d�4��4�I+�                        `8��4��4�؄1�            $��4��4��~/�I+�!�$�O.��~/��4��4�`8��Q�                
\�d'��4��4��4��4��4��4��i)�T                              h/
�sD�f<�(�
\                   	Mask.Data
�   ~   BM~       >   (               @                      ���                                                                  
Image.Data
:  6  BM6      6   (                                          H1�Nғ�Nғ�Nғ�Nғ�Nғ�Nғ�Nғ�Nғ�Nғ�Nғ�%dE�           LNғ�d
`
`
`
`
`
`
`
`dC�~�
l        
`Nғ�                                        4�b�
�        
`Nғ�                                        4�b�
�        
`Nғ�                                        4�b�
�        
`Nғ�                                        4�b�
�        
`Nғ�                                        4�b�
�        
`Nғ�                                        4�b�
�        
`Nғ�                                        4�b�
�        
`Nғ�                                        4�b�
�        
`Nғ�                        pC�~�Nғ�Nғ�C�~� 8        
`Nғ�                        G���Nғ�Nғ�Kȍ�T            
`Nғ�                        Nғ�Nғ�Kȍ�T                
`Nғ�                        Nғ�Kȍ�T                    DNғ�
l
`
`
`
`dNғ�	\                            '�G���Nғ�Nғ�Nғ�Nғ�Nғ�p                        	Mask.Data
�   ~   BM~       >   (               @                      ���                                                                  
Image.Data
:  6  BM6      6   (                                           FV��FV��FV��FV��FV��FV��FV��FV��FV��FV��FV��FV��                FV��FV��/:��	�@�FV��FV��$U�	�%.m�FV��FV��                FV��FV��$U�    
`FV��FV��	�    1�FV��FV��                FV��FV��$U�    
`FV��FV��	�    1�FV��FV��                FV��FV��$U�    
`FV��FV��	�    1�FV��FV��                FV��FV��$U�    
`FV��FV��	�    1�FV��FV��                FV��FV��$U�    
`FV��FV��	�    1�FV��FV��                FV��FV��$U�    
`FV��FV��	�    1�FV��FV��                FV��FV��$U�    
`FV��FV��	�    1�FV��FV��                FV��FV��FV��FV��FV��FV��FV��FV��FV��FV��FV��FV��                
`
`
`
`
`
`
`
`
`
`
`
`                                                                            FV��FV��FV��FV��FV��FV��FV��FV��FV��FV��FV��FV��FV��FV��        FV��FV��FV��FV��FV��FV��FV��FV��FV��FV��FV��FV��FV��FV��                        FV��FV��FV��FV��FV��FV��                                        FV��FV��FV��FV��FV��FV��                    	Mask.Data
�   ~   BM~       >   (               @                      ���                                                                  
Image.Data
:  6  BM6      6   (                                            P <O� <                                                     h ��� d��                                                     h 
d                                                                               <                                                      +� ��� k��                                            l }�� ��� ��� ��� h                                     !� ��� ��� ��� ��� ��� ���                                       ��� ��� ��� ��� ��� ��� (�                                     | ��� ��� ��� ��� ��� ���   $                                    }�� ��� ��� ��� ��� ��� <O�                                     	X ��� ��� ��� ��� Oh� 0                                         To� ��� v�� L                                               4 l           x ��� !�                                             L v�� ��� ��� t                                         8J� ��� ��� d�� D                                             T k�� 
d                   	Mask.Data
�   ~   BM~       >   (               @                      ���                                                                  
Image.Data
:  6  BM6      6   (                                                                                                                                                                   x	W0�	W0�	W0�	W0�	W0�       uA��M�uA�                      �a��u��u��u��u�    >#��u��u�l                        
`�u��u�|E� @ T�u��M�B&�        '�2�               >#��u��u�
t�W�  4
`
`
`
`	\3��u�i;�              �[��u��u�	\3�|�u��u��u��u��u��u��u��R�             T�u��u��M� L�n��u��u��u��u��u��u��               �d��u��u�d                2��i�
`                2��u��u��u��a�                D 8                 L�u��u��W��u��u�G(�                                   �R��u��n�  0>#��u��u�
`                            2��u��u��       �R��u��a�                          (	W0�	W0�>#�            H	W0�	W0�x                                                                                                                                                    	Mask.Data
�   ~   BM~       >   (               @                      ���                                                                  
Image.Data
:  6  BM6      6   (                                                                                   
�
�                                                   `I9K�I9K� !�                                              0?1@�I9K�I9K��                                           1'1�I9K�I9K�%&�                                          �I9K�I9K�8,;�                          8�F5H�I9K�7*8�""�I9K�I9K�F5H�D                    TF5H�I9K�I9K�I9K�I9K�I9K�I9K�I9K�|                       $F5H�I9K�+"-�\ @p7*8�I9K�F5H�                       �I9K�) *�                  7*8�I9K�	�                    1'1�I9K�X                    |I9K�%&�                    I9K�I9K�   <                    `I9K�1'1�                    .$/�I9K�d                    
�I9K� !�                    
�I9K�7*8�                  ,C4E�I9K�x                       7*8�I9K�7*8�x`	
�?1@�I9K�4(6�                              8;.=�I9K�I9K�I9K�I9K�I9K�) *�                                      		� �1'1��l                           	Mask.Data
�   ~   BM~       >   (               @                      ���                                                                  
Image.Data
:  6  BM6      6   (                                                                                                                                                                                                                  () �                                                       zت}�7+ �                                                     1ɞt�ت}�F        9?::ER                   hت}�ZF4�        ��d�ت}�ت}�ت}�ت}�~                      
ت}���q�        }bI�ت}�ت}�ت}�}                            ت}�͡w�        O>.�ت}�ت}���n�                               )ت}��wX�        8+ �ت}�ت}�ت}�$�                        N=-�ت}��        \H6��rS� 3ت}�ت}��              1&�ت}���l�           i  -    
\ت}�ت}���b�=/$�C4'�ƛt�ت}�ɞt�:                            >\H6�ت}�ت}�ت}�ت}�F7)�   *                                       	a>0$�6*�	Y                                                                                                                                                   	Mask.Data
�   ~   BM~       >   (               @                      ���                                                                  
Image.Data
:  6  BM6      6   (                                                       P��"磇 ߣ� ߣ� ߣ� ߣ� ߣ� ߭�"�   $                        7,
�
`                        ;0�
`            P;0�;0���
`                        ;0�
`            Ӯ)�;0�;0���
`                        ;0�
`         @�� �        ;0�
`                        ;0�
`         @�� �        ;0�
`                        ;0�
`         @�� �        ;0�t                       ;0�
`         @�� �         <Ӯ)���/���/�t            ;0�
`         @�� �             <Ӯ)���/�;0�            ;0�
`         @�� �                P��-�;0�            ;0�
`         @��"� @ @ ,        T��"�gT�gT�gT���"�   (           mY���/���/���/��               I<�I<�                      �x���/���/���                ;0�;0�                           �x���/��� �                ;0�;0�                               �� ߭�"�                ZJ�+#�                                   (��"���/���/���/���/�|f�   (            	Mask.Data
�   ~   BM~       >   (               @                      ���                                                                  
Image.Data
:  6  BM6      6   (                                                      	
�		�                                                   �+"-� !�?1@�   4                                            ��    ��                                            `;.=�   DI9K�                                              8,;�xPI9K�                                               �8,;�C4E��   ( @ @ @ @                           @I9K���I9K�I9K�I9K�I9K�I9K�8,;�d   (�1'1�;.=�I9K�I9K�I9K�F5H�%&��
�tX   <        '(��`` @I9K��F5H�                               '(��       hI9K�pI9K�x                             @7*8�?1@�8,;�C4E�p   $F5H�1'1�                                  PL           �I9K�L                                                       $F5H��                                                        
�I9K�                                                           ��                                                               $                    	Mask.Data
�   ~   BM~       >   (               @                      ���                                                                  
Image.Data
:  6  BM6      6   (                                                                                                            
d H_� Qk� Zw� 0D�CZ='�'�!��|`H�&�                 I`� ��� F[� Xs� $3�&&!�%�   
_ 2w-#�                 I`� Pi�            �$�J;."�	k	l�                 Kc� ^|�             �#�   E   i+!�                 Kc� ^|�            �'�       }��l�5+ �                 Kc� ^|�            ) �2'�    Yݪ{�S6�                    Kc� ^|�            	Xy`F�soP4�:(�                         Kc� ^|�                =h 0z {                     Kc� _~�                             _�� $�                     Ib� Kc�     t #� � &� s     I`� #�                     F]� Zw�   ( ��� ��� ��� ��� ���   ) Xs� �                     <P� ��� ��� ��� ��� ��� ��� ��� ��� ��� r                     8 
] ^ Kc� s�� 7 }�� I`� ^ 
\   "                                     s h�� o                                                                                            	Mask.Data
�   ~   BM~       >   (               @                      ���                                                                  
Image.Data
:  6  BM6      6   (                                                                                                                      %+#�+#�+#�+#�+$	�+#� >                                l��1���1���1���1���1���1�,$	�                                e��2���2���1���1���1���2�+$	�                                e��1���1���2���3���1���1�+#�                   X`   <c��1���0���2���0���1���2�+#�   `\   �I9K�I9K��c��1���1���1���2���1���1�+#�AC4E�7*8�+"-�I9K�I9K�I9K��j��2���2���2���1���2���1�+$	�A	
�   I9K�I9K�I9K�I9K��   / 	� 	�#
�&�&�& �^H.$/��I9K�I9K�I9K�I9K�?1@�q``````d'(�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�'(�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�7*8�   0� ��P                        		� � �� @                                                                	Mask.Data
�   ~   BM~       >   (               @                      ���                                                                  
Image.Data
:  6  BM6      6   (                                                                                                            �+��+��+��+�ݶ)��)�ݶ)�ϩ&�                                �+���-�jW�bP�   ��1���1�ƣ%�                                ��,���-�    h   ޸,���1�۴)�                                ��,���-�    c      SD�۴)�                P'(�1'1�	���,���-�    1(	�M   5+
�۴)�   1'1�+"-�d1'1�I9K�I9K����,���-�    nY���2�bP���2�۴)�   ! !�	
�?1@�I9K�I9K�I9K���)���/�h޸,���2���2���1��)�   
�   I9K�I9K�I9K�I9K� !�     ,  -  -  ,  -B   OI9K�I9K�I9K�I9K�I9K�I9K�I9K�+"-� � � � � � �""�F5H�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�J9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�F5H�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K�I9K��I9K�I9K�I9K�.$/� � � � � � �;.=�I9K�I9K�I9K��       0 @ @                               @ @   8                                                                    	Mask.Data
�   ~   BM~       >   (               @                      ���                                                                     �TDataSource	dsRenglonLeft�Top  TOpenDialogOpenDialogSonidos
DefaultExtdatFilter1Archivos de Sonidos (*.wav)|*.wav|Todos (*.*)|*.*OptionsofHideReadOnly TitleSeleccione el ArchivoLeft� Top�  TOpenDialog
OpenDialog
DefaultExtdatFilterOArchivos de Datos (*.dat)|*.dat|Archivos de Texto (*.txt)|*.txt|Todos (*.*)|*.*OptionsofHideReadOnly TitleSeleccione el ArchivoLeft}Top�  TcxImageListcxImageList19_PanelBotonesHeightWidthFormatVersion
DesignInfo_ �	ImageInfo
Image.Data
:	  6	  BM6	      6   (                 	                                                                                                                                                                                                                                                                                                                           ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���                     ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���                     ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���                     ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���                     ��� ��� ��� �������������������������������������������������� ��� ��� ���                     ��� ��� ��������������������������������������������������?��� ��� ��� ���                     ��� ��� ���?�����������������������������������������������j��� ��� ��� ���                     ��� ��� ���j��������������������������������������������������� ��� ��� ���                     ��� ��� ������������������������������������������������������� ��� ��� ���                     ��� ��� ������������������������������������������������������� ��� ��� ���                     ��� ��� ���S���_���_���_���_���_���_���_���_���_���_���_���_��� ��� ��� ���                     ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���                     ��� ��� ��� ����������������������������������������������� ��� ��� ��� ���                     ��� ��� ��� �����������������������~���~���~���~���~���~��� ��� ��� ��� ���                     ��� ��� ��� �������������������n��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���                     ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���                     ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���                     ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���                     ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���                                                                                                                                                                                                             
Image.Data
:	  6	  BM6	      6   (                 	                  [���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���������������������������������������������������������������������������[���[���[���[���[���[���������������������������������������������������������������������������[���[���[���[���[���[���������������������������������������������������������������������������[���[���[���[���[���[�����������[���[���[���[���[���[���[���[���[���[���[���[���[���[�����������[���[���[���[���[���[�����������[���[�������������������������������������������[���[�����������[���[���[���[���[���[�����������[���[�������������������������������������������[���[�����������[���[���[���[���[���[���[���[���[���[�������������������������������������������[���[���[���[���[���[���[���[���[���[���[���[���[���[�������������������������������������������[���[���[���[���[���[���[���[���[���[���[���[���[���[�����������[���[���[���[���[���[�����������[���[���[���[���[���[���[���[���[���[���[���[���[���[�������������������������������������������[���[���[���[���[���[���[���[���[���[���[���[���[���[�������������������������������������������[���[���[���[���[���[���[���[���[���[���[���[���[���[�����������p���p���p���p���p���p�����������[���[���[���[���[���[���[���[���[���[���[���[���[���[�������������������������������������������[���[���[���[���[���[���[���[���[���[���[���[���[���[������������������������������������������[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[���[��� 
Image.Data
:	  6	  BM6	      6   (                 	                  ������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������     