inherited SistBaseEditEmpresas: TSistBaseEditEmpresas
  Left = 214
  Top = 292
  Caption = 'Empresas'
  ClientHeight = 264
  ClientWidth = 491
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 62
    Top = 51
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = '&C'#243'digo:'
    Enabled = False
    FocusControl = CM_CODIGO
  end
  object Label2: TLabel [1]
    Left = 58
    Top = 73
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = '&Nombre:'
    FocusControl = CM_NOMBRE
  end
  inherited PanelBotones: TPanel
    Top = 228
    Width = 491
    TabOrder = 5
    DesignSize = (
      491
      36)
    inherited OK: TBitBtn
      Left = 325
    end
    inherited Cancelar: TBitBtn
      Left = 408
    end
  end
  inherited PanelSuperior: TPanel
    Width = 491
    TabOrder = 3
  end
  inherited PanelIdentifica: TPanel
    Width = 491
    TabOrder = 4
    inherited ValorActivo2: TPanel
      Width = 165
    end
  end
  object CM_NOMBRE: TDBEdit [5]
    Left = 101
    Top = 69
    Width = 388
    Height = 21
    DataField = 'CM_NOMBRE'
    DataSource = DataSource
    TabOrder = 1
  end
  object CM_CODIGO: TZetaDBEdit [6]
    Left = 101
    Top = 47
    Width = 143
    Height = 21
    CharCase = ecUpperCase
    Enabled = False
    TabOrder = 0
    ConfirmEdit = True
    DataField = 'CM_CODIGO'
    DataSource = DataSource
  end
  object GroupBox1: TGroupBox [7]
    Left = 0
    Top = 91
    Width = 491
    Height = 137
    Align = alBottom
    Caption = ' Base de Datos '
    Enabled = False
    TabOrder = 2
    object Label8: TLabel
      Left = 25
      Top = 42
      Width = 73
      Height = 13
      Alignment = taRightJustify
      Caption = '&Base de Datos:'
      Enabled = False
      FocusControl = CM_DATOS
    end
    object Label9: TLabel
      Left = 4
      Top = 65
      Width = 94
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nombre de Usuar&io:'
      Enabled = False
      FocusControl = CM_USRNAME
    end
    object Label10: TLabel
      Left = 14
      Top = 88
      Width = 84
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cla&ve de Acceso:'
      Enabled = False
      FocusControl = CM_PASSWRD
    end
    object Label5: TLabel
      Left = 62
      Top = 111
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'Con&trol:'
      Enabled = False
      FocusControl = CM_CONTROL
    end
    object Label7: TLabel
      Left = 73
      Top = 19
      Width = 25
      Height = 13
      Alignment = taRightJustify
      Caption = 'A&lias:'
      Enabled = False
      FocusControl = CM_ALIAS
    end
    object CM_USRNAME: TDBEdit
      Left = 100
      Top = 61
      Width = 145
      Height = 21
      DataField = 'CM_USRNAME'
      DataSource = DataSource
      Enabled = False
      TabOrder = 2
    end
    object CM_PASSWRD: TDBEdit
      Left = 100
      Top = 84
      Width = 145
      Height = 21
      DataField = 'CM_PASSWRD'
      DataSource = DataSource
      Enabled = False
      MaxLength = 14
      PasswordChar = '*'
      TabOrder = 3
    end
    object CM_CONTROL: TDBEdit
      Left = 100
      Top = 107
      Width = 145
      Height = 21
      DataField = 'CM_CONTROL'
      DataSource = DataSource
      Enabled = False
      TabOrder = 4
    end
    object CM_DATOS: TDBEdit
      Left = 100
      Top = 38
      Width = 382
      Height = 21
      DataField = 'CM_DATOS'
      DataSource = DataSource
      Enabled = False
      TabOrder = 1
    end
    object CM_ALIAS: TDBEdit
      Left = 100
      Top = 15
      Width = 145
      Height = 21
      DataField = 'CM_ALIAS'
      DataSource = DataSource
      Enabled = False
      TabOrder = 0
    end
  end
  inherited DataSource: TDataSource
    Left = 372
    Top = 9
  end
end
