unit ZetaUserLogin_DevEx;

interface

{$ifndef DOS_CAPAS}
{$define HTTP_CONNECTION}
{$endif}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Mask, Buttons, ExtCtrls,
     {$ifndef VER130}MaskUtils,{$endif}
     cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore,
  dxSkinsDefaultPainters,
  dxSkinscxPCPainter, dxLayoutContainer, dxLayoutControl,
  dxLayoutControlAdapters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid,
  ZBaseShell, ZetaCommonLists, Menus, cxButtons, dxSkinsForm,
  dxGDIPlusClasses, TressMorado2013, cxContainer, cxLabel, ZetaDialogo, DBasicoCliente, ZetaCommonClasses, ZetaDialogoLink,
  ZetaClientDataSet, ZetaWinAPITools, WinSvc, DBClient;

type
  TZUserLogin_DevEx = class(TForm)
    ImageCandado: TImage;
    User: TMaskEdit;
    lblUser: TLabel;
    lblPassWord: TLabel;
    PassWord: TMaskEdit;
    OK: TcxButton;
    Cancelar: TcxButton;
    ImageSistemaTRESS: TImage;
    BtnServidor: TcxButton;
    cxBtnHelp: TcxButton;
    lblVersion: TcxLabel;
    lblVersionInfo: TcxLabel;
    lblRespuestaLogin: TLabel;
    Image4: TImage;
    lblRecuperarClave: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure BtnServidorClick(Sender: TObject);
    procedure cxBtnHelpClick(Sender: TObject);
    procedure lblRecuperarClaveClick(Sender: TObject);
  private
    { Private declarations }
    //DevEx: Variable global para el tipo de LogIn
    FTipoLogIn: eTipoLogin;
    ServidorName: String;
    EReplyLogin : eLogReply;
    iNumeroIntentosLogin : integer;
    //
    function GetUserName: String;
    function GetUserPassword: String;
    function GetRespuestaLogin: eLogReply;
    procedure SetRespuestaLogin(const Value: eLogReply);
    function GetNumeroIntentosLogin: integer;
    procedure SetNumeroIntentosLogin(const Value: integer);
    procedure SetUserName( const sUser: String );
    procedure SetUserPassword( const sPassword: String );
    procedure SetRespuestaLoginLabel(const eReply: eLogReply);
    function EstadoServicioCorreo: boolean;
    procedure OcultarBotonClave;
    {$ifdef HTTP_CONNECTION}
    procedure SetServidorData;
    {$endif}
  protected
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
    { Public declarations }
    property UserName: String read GetUserName write SetUserName;
    property UserPassword: String read GetUserPassword write SetUserPassword;
    property RespuestaLogin: eLogReply read GetRespuestaLogin write SetRespuestaLogin;
    property NumeroIntentosLogin: integer read GetNumeroIntentosLogin write SetNumeroIntentosLogin;
    procedure Clear;
    //DevEx: Propieda para leer FTipoLogIn
    property GetTipoLogIn: eTipoLogIn write FTipoLogIn;
    //
  end;

var
   ZUserLogin_DevEx: TZUserLogin_DevEx;

procedure InitUserLogin;

implementation

uses ZetaRegistryCliente, FBuscaServidor_DevEx, ZetaSmartLists, ZetaClientTools,
  DCliente;

{$R *.DFM}

procedure InitUserLogin;
begin
     //if not Assigned( ZUserLogin_DevEx ) then  //OLD: ESTA LINEA YA NO ES NECESARIA PUES YA NO HAY REUTILIZACION DE LA FORMA
        ZUserLogin_DevEx := TZUserLogin_DevEx.Create( Application );
     ZUserLogin_DevEx.Clear;
end;

{ ********* TZUserLogin ********* }

procedure TZUserLogin_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     lblVersionInfo.Caption := '';
{$ifdef DOS_CAPAS}
     BtnServidor.Visible := False;
{$else}
     {$ifdef HTTP_CONNECTION}
     SetServidorData;
     {$else}
     ServidorName := ClientRegistry.ComputerName;
     {$endif}
     BtnServidor.Enabled := ClientRegistry.CanWrite;
{$endif}
end;

procedure TZUserLogin_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Caption := 'Entrando a ' + Application.Title;

     {$ifndef DOS_CAPAS}
      {$ifdef HTTP_CONNECTION}
      SetServidorData;
      {$else}
      //Servidor.Text := ClientRegistry.ComputerName;
      ServidorName := ClientRegistry.ComputerName;
      {$endif}
      BtnServidor.Enabled := ClientRegistry.CanWrite;
     {$ENDIF}

     //DevEx(by am): Se agrega el a�o de la Version.
     //lblVersionInfo.Caption := GetApplicationVersionInfo( 'ProductVersion' );
     lblVersionInfo.Caption := GetApplicationProductVersionSimple;
     ActiveControl := User;

     if NumeroIntentosLogin > 1 then
     begin
          SetRespuestaLoginLabel(RespuestaLogin);
     end;

     EstadoServicioCorreo;

end;

function TZUserLogin_DevEx.EstadoServicioCorreo: boolean;
 var cdsServicios : TZetaClientDataSet;
begin
      try
           Result := False;
           cdsServicios := TZetaClientDataSet.Create(Self);
           with cdsServicios do
           begin
                Active:= False;
                Filtered := False;
                Filter := '';
                IndexFieldNames := 'colServicios';
                AddStringField('colServicios', 50);
                AddStringField('colStatus', 50);
                AddStringField('colServiceName', 100);
                AddStringField('colServiceRealName', 300);
                AddStringField('colServiceType', 50);
                CreateTempDataset;
                Open;
           end;
           Result := ZetaWinAPITools.ServiceGetListCorreo('', SERVICE_WIN32, SERVICE_STATE_ALL,  TClientDataSet( cdsServicios ));
           if (Result) then
           begin
                Image4.Visible := True;
                lblRecuperarClave.Visible := True;
           end
           else
           begin
                OcultarBotonClave;
           end;
      Except
           on Error: Exception do
           begin
                OcultarBotonClave;
                Result := false;
           end;
      end;
end;

procedure TZUserLogin_DevEx.OcultarBotonClave;
begin
    Image4.Visible := false;
    lblRecuperarClave.Visible := false;
end;


procedure TZUserLogin_DevEx.SetRespuestaLoginLabel(const eReply: eLogReply);
begin
          case eReply of
               lrLoggedIn: lblRespuestaLogin.Visible := false; //'� Usuario Ya Entr� Al Sistema !' + ' - Ya Hay Otro Usuario Con Esta Clave En El Sistema' ;
               lrLockedOut: lblRespuestaLogin.Visible := false;//'� Usuario Bloqueado !' + ' - El Administrador Ha Bloqueado Su Acceso Al Sistema' ;
               lrChangePassword: lblRespuestaLogin.Visible := false;//'� Debe Cambiar Clave de Usuario !' + ' - El Administrador Ha forzado que Cambie su Password' ;
               lrExpiredPassword: lblRespuestaLogin.Visible := false;//'� Clave de Usuario Vencida !' + ' - Su Clave de Acceso Anterior Ha Expirado' ;
               lrInactiveBlock: lblRespuestaLogin.Visible := false;//'� Usuario Bloqueado !' + ' - Usuario Ha Sido Bloqueado Por No Entrar Al Sistema Durante Mucho Tiempo' ;
               lrNotFound: lblRespuestaLogin.Visible := true;//'� No se Encontr� el Usuario !' + ' - Verifique que sea Correcto el Nombre del Usuario' ;
               lrAccessDenied: lblRespuestaLogin.Visible := true;//'� Password Incorrecto de Usuario !' + ' - Verifique el Password del Usuario' ;
               lrSystemBlock: lblRespuestaLogin.Visible := false;//'� Sistema Bloqueado !' +  ' - El Administrador Ha Bloqueado el Acceso Al Sistema' ;
          end;
end;

procedure TZUserLogin_DevEx.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     inherited;
     if ( ModalResult = mrOK ) then
     begin
          CanClose := False; // Default //
          if ( Length( User.Text ) = 0 ) then
             User.SetFocus
          else
              if ( Length( PassWord.Text ) = 0 ) then
                 PassWord.SetFocus
              else
                  CanClose := True;
     end
     else
         CanClose := True;
end;

procedure TZUserLogin_DevEx.Clear;
begin
     User.Clear;
     Password.Clear;
end;

{$ifdef HTTP_CONNECTION}
procedure TZUserLogin_DevEx.SetServidorData;
begin
     with ClientRegistry do
     begin
          case TipoConexion of
               conxHTTP:
               begin
                    ServidorName := URL;
               end;
               else
               begin
                     ServidorName := ComputerName;
               end;
          end;
     end;
end;
{$endif}



function TZUserLogin_DevEx.GetUserName: String;
begin
     Result := User.Text;
end;

function TZUserLogin_DevEx.GetUserPassword: String;
begin
     Result := PassWord.Text;
end;

function TZUserLogin_DevEx.GetRespuestaLogin: eLogReply;
begin
     Result := EReplyLogin;
end;

function TZUserLogin_DevEx.GetNumeroIntentosLogin: integer;
begin
     Result := iNumeroIntentosLogin;
end;

procedure TZUserLogin_DevEx.SetNumeroIntentosLogin(const Value: integer);
begin
        iNumeroIntentosLogin := Value;
end;

procedure TZUserLogin_DevEx.SetRespuestaLogin(const Value: eLogReply);
begin
        EReplyLogin := Value;
end;

procedure TZUserLogin_DevEx.SetUserName( const sUser: String );
begin
     User.Text := sUser;
end;

procedure TZUserLogin_DevEx.SetUserPassword( const sPassword: String );
begin
     PassWord.Text := sPassword;
end;

procedure TZUserLogin_DevEx.BtnServidorClick(Sender: TObject);
begin
     {$ifdef HTTP_CONNECTION}
     with ClientRegistry do
     begin
          if BuscaServidor then
          begin
               Self.SetServidorData;
          end;
     end;
     {$else}
     //with TFindServidor.Create( Self ) do //Borrar
     with TFindServidor_DevEx.Create( Self ) do
     begin
          try
             //Servidor := Self.Servidor.Text;
             Servidor := ServidorName;
             if ( ShowModal = mrOk ) then
             begin
                  ClientRegistry.ComputerName := Servidor;
                  //Self.Servidor.Text := Servidor;
                  ServidorName := Servidor;
             end;
          finally
                 Free;
          end;
     end;
     {$endif}
end;

procedure TZUserLogin_DevEx.KeyPress( var Key: Char );
begin
     inherited KeyPress( Key );
     case Key of
          Chr( VK_RETURN ):
          begin
               if ( not ( ActiveControl is TButton ) ) and
                  ( not ( ActiveControl is TZetaSmartListBox ) ) and
                  ( not ( ActiveControl is TCustomMemo ) ) then
               begin
                    Key := #0;
                    Perform( WM_NEXTDLGCTL, ( Hi( GetKeyState( VK_SHIFT ) ) and 1 ), 0 );
               end;
          end;
     end;
end;

procedure TZUserLogin_DevEx.lblRecuperarClaveClick(Sender: TObject);
var
  sUsuario: string;
begin
      try
         sUsuario := User.Text;
         if sUsuario = VACIO then
         begin
              ZetaDialogo.ZError('Cambio de Contrase�a','Es necesario ingresar un usuario.',0);
         end
         else
         begin
              ZMensajeConfirm( 'Cambiar Clave de acceso ' , sUsuario, 0, mbCancel);
         end;

      Except
             on Error: Exception do
             begin
                  Raise Exception.Create( 'No fue posible procesar el cambio de clave, '+ Error.Message );
             end;
      end;
end;

procedure TZUserLogin_DevEx.cxBtnHelpClick(Sender: TObject);
const
     K_HELP_CAMBIOS = 'Version.chm';
var
   sHelpFile: String;
begin
     with Application do
     begin
          sHelpFile := HelpFile;
          try
             HelpFile := K_HELP_CAMBIOS;
             HelpCommand( HELP_FINDER, 0 );
          finally
             HelpFile := sHelpFile;
          end;
     end;
end;

end.
