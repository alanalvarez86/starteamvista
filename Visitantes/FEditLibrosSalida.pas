unit FEditLibrosSalida;

interface

uses     
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaDBTextBox,
  ComCtrls, Mask, ZetaKeyCombo, ZetaKeyLookup, ZetaHora, ZetaFecha,
  ZetaSmartLists, ZBaseEdicion, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, TressMorado2013, cxControls,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList,
  cxNavigator, cxDBNavigator, cxButtons;

type
  TEditLibrosSalida = class(TBaseEdicion_DevEx)
    GroupBox4: TGroupBox;
    lblSalFec: TLabel;
    lblHorSal: TLabel;
    GroupBox1: TGroupBox;
    LI_OBSERVA: TDBMemo;
    LI_SAL_FEC: TZetaTextBox;
    LI_SAL_HOR: TZetaTextBox;
    Label1: TLabel;
    LI_NOMBRE: TZetaDBTextBox;
    LI_DEVGAF: TDBCheckBox;
    procedure FormShow(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FEstadoAlerta:boolean;
  protected
     procedure Connect; override;
     function PuedeAgregar(var sMensaje: String): Boolean; override;
     function PuedeBorrar(var sMensaje: String): Boolean; override;
  public
    { Public declarations }
    property FPEstadoAlerta: Boolean read FEstadoAlerta  write FEstadoAlerta;
  end;

var
  EditLibrosSalida: TEditLibrosSalida;

implementation
uses DVisitantes,
     DCliente,
     ZetaCommonTools,
     FTressShell,
     FEditLibros,
     FAlerta;

{$R *.DFM}

{ TEditLibros }
procedure TEditLibrosSalida.Connect;
begin
     DataSource.DataSet:= dmVisitantes.cdsLibrosSalidas;
     with dmVisitantes.cdsLibros do
     begin
          LI_SAL_FEC.Caption := FechaAsStr( Date );
          LI_SAL_HOR.Caption := HoraAsStr( dmCliente.HoraDefault, 'hh:nn' );
     end;
end;

function TEditLibrosSalida.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     if not Result then
        sMensaje := 'No se pueden agregar registros de libro desde VisitantesMgr';
end;

function TEditLibrosSalida.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     if not Result then
        sMensaje := 'No se pueden borrar registros de libro desde VisitantesMgr';
end;



procedure TEditLibrosSalida.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     inherited;
     TressShell.TimerAlertas.Enabled := FEstadoAlerta;
end;

procedure TEditLibrosSalida.FormShow(Sender: TObject);
begin
     inherited;
     LI_OBSERVA.SetFocus;
     FEstadoAlerta := TressShell.TimerAlertas.Enabled;
end;

procedure TEditLibrosSalida.OK_DevExClick(Sender: TObject);
begin
     inherited;
     TressShell.RefrescaShell;
     close;
     if EditLibros <> nil then
        EditLibros.Close
     else if Alerta <> nil then
          Alerta.close;
end;

procedure TEditLibrosSalida.CancelarClick(Sender: TObject);
begin
     inherited;
     Close;
end;

end.
