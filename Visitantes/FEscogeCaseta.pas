unit FEscogeCaseta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, Grids, DBGrids, Db, StdCtrls, Buttons, ExtCtrls,
  ZBaseDlgModal, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  dxSkinsCore, TressMorado2013, Vcl.ImgList, cxButtons, cxStyles, cxClasses,
  cxControls, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridCustomView, cxGridLevel, cxGrid;

type
  TEscogeCaseta = class(TZetaDlgModal_DevEx)
    dsCaseta: TDataSource;
    cxStyleRepository2: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    CA_NOMBRE: TcxGridDBColumn;
    CA_ABIERTA: TcxGridDBColumn;
    cxStyle3: TcxStyle;
    procedure GridEmpresasDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GridEmpresasDrawDataCell(Sender: TObject; const Rect: TRect;
      Field: TField; State: TGridDrawState);
    procedure GridEmpresasDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure CA_ABIERTACustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure CA_NOMBRECustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
  private
    //procedure GridEmpresasDrawColumnCell(Sender: TObject;const Rect: TRect; DataCol: Integer; Column: TColumn;State: TGridDrawState);
    { Private declarations }
    procedure EliminaBordesColumna(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
  public
    { Public declarations }
  end;

var
  EscogeCaseta: TEscogeCaseta;

function EscogeUnaCaseta( const lSetCasetaDefault: Boolean ): Boolean;

implementation
uses
    DVisitantes,
    ZetaCommonClasses;

{$R *.DFM}
function EscogeUnaCaseta( const lSetCasetaDefault: Boolean ): Boolean;
var
    sOldCaseta : String;
begin
     sOldCaseta := dmVisitantes.Caseta;
     EscogeCaseta  := TEscogeCaseta.Create( Application );
     try
        with EscogeCaseta do
        begin
             dsCaseta.DataSet := dmVisitantes.cdsCaseta;
             ShowModal;
             Result := ( ModalResult = mrOk );

             if Result AND lSetCasetaDefault then
                dmVisitantes.SetCasetaDefault;

        end;
     finally
            EscogeCaseta.Free;
     end;
     if NOT ( Result ) then
        dmVisitantes.FindCaseta( sOldCaseta );
end;


procedure TEscogeCaseta.GridEmpresasDblClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrOK;
end;

procedure TEscogeCaseta.GridEmpresasDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;
  color:=clBlack;
  if (Column.Field.FieldName = 'CA_ABIERTA') then begin
    if (Column.Field.Text = 'S') then
      color:= RGB(62, 175, 75)
    else color:= RGB(194, 0, 0);
    column.Font.Color :=  color;
  end;
end;

procedure TEscogeCaseta.GridEmpresasDrawDataCell(Sender: TObject;
  const Rect: TRect; Field: TField; State: TGridDrawState);
begin
  inherited;
end;

{procedure TEscogeCaseta.GridEmpresasDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
 var
    sTexto: string;
begin
  inherited;
  if DataCol IN [ 0,1 ] then
    with GridEmpresas.Canvas, Rect do
    begin
        if gdSelected in State then
            Font.Style := [ fsBold ];
        with GridEmpresas.Canvas do
             if DataCol = 0 then
               TextRect( Rect, Left+25, Top+4, dmVisitantes.cdsCaseta.FieldByName( 'CA_NOMBRE' ).AsString )
             else
             begin
                  if dmVisitantes.cdsCaseta.FieldByName( 'CA_ABIERTA' ).AsString = K_GLOBAL_SI then
                     sTexto :=  'Abierta'
                  else sTexto :=  'Cerrada';
                  TextRect( Rect, Left+25, Top+4, sTexto );
             end;
    end
end;}


procedure TEscogeCaseta.CA_ABIERTACustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  color : TColor;
begin
  inherited;
  if (AViewInfo.Text = 'Abierta') then
    color:= RGB(62, 175, 75)
  else color:= RGB(194, 0, 0);
  ACanvas.Font.Color :=  color;

  EliminaBordesColumna(Sender,ACanvas, AViewInfo, ADone);
end;

procedure TEscogeCaseta.CA_NOMBRECustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
begin
   EliminaBordesColumna(Sender,ACanvas, AViewInfo, ADone);
end;

procedure TEscogeCaseta.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrOK;
end;

procedure TEscogeCaseta.EliminaBordesColumna(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  ARect: TRect;
begin
  with AViewInfo do begin
  AViewInfo.Borders := [];
  ARect := Bounds;
  ACanvas.FillRect(ARect);
  ACanvas.DrawTexT(Text, TextAreaBounds, cxAlignVCenter);

  ADone := True;
end;
  ACanvas.DrawComplexFrame(ARect, AViewInfo.BorderColor[bRight], AViewInfo.BorderColor[bLeft], [], 1);

end;

procedure TEscogeCaseta.FormCreate(Sender: TObject);
begin
  inherited;
  HelpContext := H_VIS_CASETA_ESCOJE;

end;

end.
