unit FTableroTotales;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore,
  TressMorado2013, cxButtons, dxGDIPlusClasses, Vcl.ExtCtrls, Data.DB,
  VclTee.TeeGDIPlus, VCLTee.TeEngine, VCLTee.TeeProcs, VCLTee.Chart,
  VCLTee.Series;

type
  TFTableroTots = class(TForm)
    Label1: TLabel;
    Label3: TLabel;
    lbFecha: TLabel;
    PeriodoAnterior_DevEx: TcxButton;
    PeriodoSiguiente_DevEx: TcxButton;
    Image1: TImage;
    Image3: TImage;
    Image4: TImage;
    Image6: TImage;
    Label4: TLabel;
    lbNumVisDentro: TLabel;
    lbNumVisTot: TLabel;
    lbNumCarDentro: TLabel;
    dsSQL: TDataSource;
    Chart1: TChart;
    Series2: THorizBarSeries;
    lbA1: TLabel;
    lbA2: TLabel;
    lbA3: TLabel;
    lbA4: TLabel;
    lbA5: TLabel;
    Label5: TLabel;
    lbt1: TLabel;
    lbt2: TLabel;
    lbt3: TLabel;
    lbt4: TLabel;
    lbt5: TLabel;
    Label2: TLabel;
    Image2: TImage;
    Image5: TImage;
    lbNumVisFuera: TLabel;
    lbNumCarFuera: TLabel;
    Label6: TLabel;
    Image7: TImage;
    Chart2: TChart;
    HorizBarSeries2: THorizBarSeries;
    Image9: TImage;
    procedure FormShow(Sender: TObject);
    procedure PeriodoAnterior_DevExClick(Sender: TObject);
    procedure PeriodoSiguiente_DevExClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    dFechaActualCorte:TDate;
    procedure IniciaCaptura;
    procedure IniciaLbls;
    procedure ActualizaTablero;
    procedure ActualizaGraficaAsuntos;
    procedure ActualizaGraficaTiempos;
    function GetHrsMinutos(iCantMinutos:integer):string;
    function ObtenerAnchoDeseado(sCadena: TLabel; iAnchoDeseado:integer):string;
  public
    { Public declarations }
    property FechaActualCorte: TDate read dFechaActualCorte write dFechaActualCorte;
  end;

var
  FTableroTots: TFTableroTots;

implementation
uses
  DVisitantes,
  ZetaCommonTools;
{$R *.dfm}

procedure TFTableroTots.ActualizaGraficaAsuntos;
var
   oColorG1:TColor;
   oColorG2:TColor;
   iA1, iA2, iA3, iA4, iA5:integer;
   sT1, sT2, sT3, sT4, sT5:string;
   iCont:integer;

Const
     iMAXLEN = 150;
begin
     iCont:=0;
     iA1:=0;
     iA2:=0;
     iA3:=0;
     iA4:=0;
     iA5:=0;
     oColorG1:=rgb(185, 185, 185);
     oColorG2:=rgb(218, 218, 218);
     with dmVisitantes do
     begin
          with dmVisitantes.cdsTotales do
          begin
               FiltroSQL := DateToStrSQLC(FechaActualCorte);
               OPCSQL := 7;
               AlAdquirirDatos(Self);
               while not eof do
               begin
                    if iCont = 0 then
                    begin
                         iA1 := FieldByName('tops').AsInteger;
                         if FieldByName('TB_ELEMENT').AsString <> '' then lba1.Caption := FieldByName('TB_ELEMENT').AsString else lba1.Caption := '';
                         if lbA1.Width >= iMAXLEN then
                         begin
                              lbA1.Caption := ObtenerAnchoDeseado(lba1, iMAXLEN);
                              lbA1.Caption := ('('+IntToStr(iA1)+')')+'... '+ lbA1.Caption;
                         end
                         else if length(lbA1.Caption) > 0 then
                              lbA1.Caption := ('('+IntToStr(iA1)+')')+' '+lbA1.Caption
                         else lbA1.Caption := ('('+IntToStr(iA1)+')')+' '+'SIN ASUNTO '
                    end
                    else if iCont = 1 then
                    begin
                         iA2 := FieldByName('tops').AsInteger;
                         if FieldByName('TB_ELEMENT').AsString <> '' then lba2.Caption := FieldByName('TB_ELEMENT').AsString else lba2.Caption := '';
                         if lbA2.Width >= iMAXLEN then
                         begin
                              lbA2.Caption := ObtenerAnchoDeseado(lba2, iMAXLEN);
                              lbA2.Caption := ('('+IntToStr(iA2)+')')+'... '+ lbA2.Caption;
                         end
                         else if length(lbA2.Caption) > 0 then
                              lbA2.Caption := ('('+IntToStr(iA2)+')')+' '+lbA2.Caption
                         else lbA2.Caption := ('('+IntToStr(iA2)+')')+' '+'SIN ASUNTO '
                    end
                    else if iCont = 2 then
                    begin
                         iA3 := FieldByName('tops').AsInteger;
                         if FieldByName('TB_ELEMENT').AsString <> '' then lba3.Caption := FieldByName('TB_ELEMENT').AsString else lba3.Caption := '';
                         if lbA3.Width >= iMAXLEN then
                         begin
                              lbA3.Caption := ObtenerAnchoDeseado(lba3, iMAXLEN);
                              lbA3.Caption := ('('+IntToStr(iA3)+')')+'... '+ lbA3.Caption;
                         end
                         else if length(lbA3.Caption) > 0 then
                              lbA3.Caption := ('('+IntToStr(iA3)+')')+' '+lbA3.Caption
                         else lbA3.Caption := ('('+IntToStr(iA3)+')')+' '+'SIN ASUNTO '
                    end
                    else if iCont = 3 then
                    begin
                         iA4 := FieldByName('tops').AsInteger;
                         if FieldByName('TB_ELEMENT').AsString <> '' then lba4.Caption := FieldByName('TB_ELEMENT').AsString else lba4.Caption := '';
                         if lbA4.Width >= iMAXLEN then
                         begin
                              lbA4.Caption := ObtenerAnchoDeseado(lba4, iMAXLEN);
                              lbA4.Caption := ('('+IntToStr(iA4)+')')+'... '+ lbA4.Caption;
                         end
                         else if length(lbA4.Caption) > 0 then
                              lbA4.Caption := ('('+IntToStr(iA4)+')')+' '+lbA4.Caption
                         else lbA4.Caption := ('('+IntToStr(iA4)+')')+' '+'SIN ASUNTO '
                    end
                    else if iCont = 4 then
                    begin
                         iA5 := FieldByName('tops').AsInteger;
                         if FieldByName('TB_ELEMENT').AsString <> '' then lba5.Caption := FieldByName('TB_ELEMENT').AsString else lba5.Caption := '';
                         if lbA5.Width >= iMAXLEN then
                         begin
                              lbA5.Caption := ObtenerAnchoDeseado(lba5, iMAXLEN);
                              lbA5.Caption := ('('+IntToStr(iA5)+')')+'... '+ lbA5.Caption;
                         end
                         else if length(lbA5.Caption) > 0 then
                              lbA5.Caption := ('('+IntToStr(iA5)+')')+' '+lbA5.Caption
                         else lbA5.Caption := ('('+IntToStr(iA5)+')')+' '+'SIN ASUNTO '
                    end;
                    next;
                    inc(iCont);
          end;
     end;
end;


    Series2.Clear;
    With Series2 do
    begin
         Add (iA5, '', oColorG1);
         Add (iA4, '', oColorG2);
         Add (iA3, '', oColorG1);
         Add (iA2, '', oColorG2);
         Add (iA1, '', oColorG1);
    end;
end;

procedure TFTableroTots.ActualizaGraficaTiempos;
var
   oColorG1:TColor;
   oColorG2:TColor;
   iA1, iA2, iA3, iA4, iA5:integer;
   sT1, sT2, sT3, sT4, sT5:string;
   iCont:integer;
begin
     iCont:=0;
     iA1:=0;
     iA2:=0;
     iA3:=0;
     iA4:=0;
     iA5:=0;
     oColorG1:=rgb(185, 185, 185);
     oColorG2:=rgb(218, 218, 218);
     with dmVisitantes do
     begin
          with dmVisitantes.cdsTotales do
     begin
          FiltroSQL := DateToStrSQLC(FechaActualCorte);
          OPCSQL := 8;
          AlAdquirirDatos(Self);
          while not eof do
          begin
            if iCont = 0 then
            begin
                 iA1 := FieldByName('tiempo').AsInteger;
                 if FieldByName('Nombre').AsString <> '' then lbt1.Caption := FieldByName('Nombre').AsString  else lbt1.Caption := '';
                 if lbt1.Width >= 140 then
                 begin
                      lbt1.Caption := ObtenerAnchoDeseado(lbt1, 140);
                      lbt1.Caption := GetHrsMinutos(iA1) + '... ' + lbt1.Caption ;
                 end
                 else lbt1.Caption := GetHrsMinutos(iA1) + ' ' + lbt1.Caption ;
            end
            else if iCont = 1 then
            begin
                 iA2 := FieldByName('tiempo').AsInteger;
                 if FieldByName('Nombre').AsString <> '' then lbt2.Caption := FieldByName('Nombre').AsString else lbt2.Caption := '';
                 if lbt2.Width >= 140 then
                 begin
                      lbt2.Caption := ObtenerAnchoDeseado(lbt2, 140);
                      lbt2.Caption := GetHrsMinutos(iA2) + '... ' + lbt2.Caption ;
                 end
                 else lbt2.Caption := GetHrsMinutos(iA2) + ' ' + lbt2.Caption ;
            end
            else if iCont = 2 then
            begin
                 iA3 := FieldByName('tiempo').AsInteger;
                 if FieldByName('Nombre').AsString <> '' then lbt3.Caption := FieldByName('Nombre').AsString else lbt3.Caption := '';
                 if lbt3.Width >= 140 then
                 begin
                      lbt3.Caption := ObtenerAnchoDeseado(lbt3, 140);
                      lbt3.Caption := GetHrsMinutos(iA3) + '... ' + lbt3.Caption ;
                 end
                 else lbt3.Caption := GetHrsMinutos(iA3) + ' ' + lbt3.Caption ;
            end
            else if iCont = 3 then
            begin
                 iA4 := FieldByName('tiempo').AsInteger;
                 if FieldByName('Nombre').AsString <> '' then lbt4.Caption := FieldByName('Nombre').AsString else lbt4.Caption := '';
                 if lbt4.Width >= 140 then
                 begin
                      lbt4.Caption := ObtenerAnchoDeseado(lbt4, 140);
                      lbt4.Caption := GetHrsMinutos(iA4) + '... ' + lbt4.Caption ;
                 end
                 else lbt4.Caption := GetHrsMinutos(iA4) + ' ' + lbt4.Caption ;
            end
            else if iCont = 4 then
            begin
                 iA5 := FieldByName('tiempo').AsInteger;
                 if FieldByName('Nombre').AsString <> '' then lbt5.Caption := FieldByName('Nombre').AsString  else lbt5.Caption := '';
                 if lbt5.Width >= 140 then
                 begin
                      lbt5.Caption := ObtenerAnchoDeseado(lbt5, 140);
                      lbt5.Caption := GetHrsMinutos(iA5) + '... ' + lbt5.Caption ;
                 end
                 else lbt5.Caption := GetHrsMinutos(iA5) + ' ' + lbt5.Caption ;
            end;
            next;
            inc(iCont);
            end;
     end;

     HorizBarSeries2.Clear;
     With HorizBarSeries2 do
     begin
          Add (iA5, '', oColorG1);
          Add (iA4, '', oColorG2);
          Add (iA3, '', oColorG1);
          Add (iA2, '', oColorG2);
          Add (iA1, '', oColorG1);
     end;
     end;
end;

procedure TFTableroTots.ActualizaTablero;
begin
     with dmVisitantes do
     begin
          with dmVisitantes.cdsTotales do
          begin
               FiltroSQL := DateToStrSQLC(FechaActualCorte);
               OPCSQL := 4;
               AlAdquirirDatos(Self);
               lbNumVisFuera.Caption := FieldByName('Tot').AsString;

               OPCSQL := 3;
               AlAdquirirDatos(Self);
               lbNumVisDentro.Caption := inttostr(strtoint(FieldByName('Tot').AsString) - strtoint(lbNumVisFuera.Caption));

               OPCSQL := 6;
               AlAdquirirDatos(Self);
               lbNumCarFuera.Caption := FieldByName('Tot').AsString;

               OPCSQL := 5;
               AlAdquirirDatos(Self);
               lbNumCarDentro.Caption := inttostr(strtoint(FieldByName('Tot').AsString) - strtoint(lbNumCarFuera.Caption));

               lbNumVisTot.Caption := inttostr(strtoint(lbNumVisDentro.Caption) + strtoint(lbNumVisFuera.Caption));
               //lbNumCarTot.Caption := inttostr(strtoint(lbNumCarDentro.Caption) + strtoint(lbNumCarFuera.Caption));

               lbFecha.Caption := DateToStr(FechaActualCorte);
               PeriodoSiguiente_DevEx.Left := lbFecha.Left + lbFecha.Width + 7;
          end;
     end;
     ActualizaGraficaAsuntos;
     ActualizaGraficaTiempos;
end;

procedure TFTableroTots.FormCreate(Sender: TObject);
begin
     Self.KeyPreview := True;
end;

procedure TFTableroTots.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if (Key = #27) then
        Close;
end;

procedure TFTableroTots.FormShow(Sender: TObject);
begin
     IniciaLbls;
     IniciaCaptura;
end;

function TFTableroTots.GetHrsMinutos(iCantMinutos: integer): string;
var
   sHrsMins:string;
   iHrs, iMins:integer;
begin
     iHrs := iCantMinutos div 60;
     iMins := iCantMinutos - (iHrs * 60);
     if length(inttostr(iCantMinutos)) = 1 then
        Result := '('+inttostr(iHrs) +':0'+ inttostr(iMins)+ ')'
     else
         Result := '('+inttostr(iHrs) +':'+ inttostr(iMins)+ ')';
end;

procedure TFTableroTots.IniciaCaptura;
begin
     FechaActualCorte:=date;
     ActualizaTablero;
end;

procedure TFTableroTots.IniciaLbls;
begin
     lbt1.Caption := '';
     lbt2.Caption := '';
     lbt3.Caption := '';
     lbt4.Caption := '';
     lbt5.Caption := '';
     lbA1.Caption := '';
     lbA2.Caption := '';
     lbA3.Caption := '';
     lbA4.Caption := '';
     lbA5.Caption := '';
end;

function TFTableroTots.ObtenerAnchoDeseado(sCadena: TLabel;
  iAnchoDeseado: integer): string;
var
   iAnchoActual:integer;
   iCont:integer;
   sCad:string;
begin
     iCont:=0;
     iAnchoActual:=0;
     scad:= sCadena.Caption;
     sCadena.Caption:='';
     while sCadena.Width < iAnchoDeseado do
     begin
          sCadena.Caption := sCad.Substring(0, iCont);
          inc(iCont);
     end;
     Result:=sCadena.Caption;
end;

procedure TFTableroTots.PeriodoAnterior_DevExClick(Sender: TObject);
begin
     IniciaLbls;
     FechaActualCorte := FechaActualCorte - 1;
     ActualizaTablero;
end;

procedure TFTableroTots.PeriodoSiguiente_DevExClick(Sender: TObject);
begin
     IniciaLbls;
     FechaActualCorte := FechaActualCorte + 1;
     ActualizaTablero;
end;

end.
