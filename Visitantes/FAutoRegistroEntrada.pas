unit FAutoRegistroEntrada;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZCXWizardBasico, cxRichEdit, dxCustomWizardControl, dxWizardControl,
  Buttons, StdCtrls, DBCtrls, ZetaKeyLookup, Mask, ComCtrls,
  ZetaWizard, ExtCtrls, Db,
  //:todo TDMULTIP,
  Videocap,ZetaSmartLists,
  {$ifndef VER130}
  Variants,
  {$endif}

  ZetaClientDataset, ZetaKeyCombo, ZetaFecha,ZVisitantesTools, Grids, DBGrids;
  //:todo FToolsVideo;

type
  TAutoRegistroEntrada = class(TCXWizardBasico)
    DataSource: TDataSource;
    tsBuscaVisita: TdxWizardControlPage;
    tsDatosPersonales1: TdxWizardControlPage;
    tsCompaniaPregunta: TdxWizardControlPage;
    tsBuscaCompania: TdxWizardControlPage;
    tsDatosPersonales2: TdxWizardControlPage;
    tsCompaniaNombre: TdxWizardControlPage;
    tsBuscaVisitandoA: TdxWizardControlPage;
    tsVisitandoANombre: TdxWizardControlPage;
    tsAsuntoATratar: TdxWizardControlPage;
    tsIdentificacion: TdxWizardControlPage;
    tsVehiculo: TdxWizardControlPage;
    tsObservaciones: TdxWizardControlPage;
    tsGracias: TdxWizardControlPage;
    PanelTitulo1: TPanel;
    eBuscaVisita: TEdit;
    bBuscaVisita: TBitBtn;
    dsBuscaVisita: TDataSource;
    GridBuscaVisita: TDBGrid;
    Label4: TLabel;
    lbBuscaVisita: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    lblApePat: TLabel;
    VI_APE_PAT: TDBEdit;
    VI_APE_MAT: TDBEdit;
    lblApeMat: TLabel;
    lblNombre: TLabel;
    VI_NOMBRES: TDBEdit;
    lblTipo: TLabel;
    VI_TIPO: TDBGrid;
    Label3: TLabel;
    Label5: TLabel;
    eBuscaCompania: TEdit;
    bBuscaCompania: TBitBtn;
    lbBuscaCompania: TLabel;
    GridBuscaCompania: TDBGrid;
    lblNacion: TLabel;
    VI_NACION: TDBEdit;
    VI_MIGRA: TDBEdit;
    lblMigra: TLabel;
    lblFecNac: TLabel;
    VI_FEC_NAC: TZetaDBFecha;
    lblSexo: TLabel;
    Panel5: TPanel;
    Panel6: TPanel;
    Label7: TLabel;
    eCompaniaNombre: TEdit;
    Label8: TLabel;
    dsBuscaCompania: TDataSource;
    Panel7: TPanel;
    Label9: TLabel;
    eBuscaVisitandoA: TEdit;
    bBuscaVisitandoA: TBitBtn;
    lbBuscaVisitandoA: TLabel;
    GridBuscaVisitandoA: TDBGrid;
    LI_ASUNTO: TDBGrid;
    Panel8: TPanel;
    Panel9: TPanel;
    lblTipoId: TLabel;
    LI_TIPO_ID: TDBGrid;
    lblLicId: TLabel;
    LI_ID: TDBEdit;
    LI_GAFETE: TDBEdit;
    Label1: TLabel;
    Label12: TLabel;
    Panel10: TPanel;
    Panel11: TPanel;
    lblCarro: TLabel;
    LI_CAR_PLA: TDBEdit;
    LI_CAR_DES: TDBEdit;
    lblCajon: TLabel;
    lblDescrip: TLabel;
    lblPlacas: TLabel;
    LI_CAR_TIP: TDBGrid;
    LI_CAR_EST: TDBEdit;
    Panel12: TPanel;
    LI_OBSERVA: TDBMemo;
    lblTexto1: TLabel;
    LI_TEXTO1: TDBEdit;
    LI_TEXTO2: TDBEdit;
    lblTexto2: TLabel;
    LI_TEXTO3: TDBEdit;
    lblTexto3: TLabel;
    Label13: TLabel;
    PanelTitulo2: TPanel;
    Label14: TLabel;
    cbNoExisteVisita: TCheckBox;
    RGRepresentaCompania: TRadioGroup;
    cbNoExisteCompania: TCheckBox;
    cbNoExisteVisitandoA: TCheckBox;
    dsTipoVisitante: TDataSource;
    dsBuscaVisitandoA: TDataSource;
    dsAsunto: TDataSource;
    dsTipoID: TDataSource;
    dsVehiculo: TDataSource;
    cbNoExisteAsunto: TCheckBox;
    cbNoExisteID: TCheckBox;
    cbNoExisteVehiculo: TCheckBox;
    dsVisita: TDataSource;
    VI_SEXO: TZetaKeyCombo;
    oRichEdit: TcxRichEdit;
    Label2: TLabel;
    eNombreAnfitrion: TEdit;
    eApePatAnfitrion: TEdit;
    eApeMatAnfitrion: TEdit;
    Label6: TLabel;
    Label10: TLabel;
    lblDriver: TLabel;
    btnMuestra: TBitBtn;
    btnCapturar: TBitBtn;
    btnVideo: TBitBtn;
    cbDrivers: TComboBox;
    Panel1: TPanel;
  //:todo   Documento: TPDBMultiImage;
  //:todo   VideoCap1: TVideoCap;
    procedure tsFotoShow(Sender: TObject);    
    procedure FormCreate(Sender: TObject);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure bBuscaVisitaClick(Sender: TObject);
    procedure bBuscaCompaniaClick(Sender: TObject);
    procedure bBuscaVisitandoAClick(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
    procedure SiguienteClick(Sender: TObject);
    procedure GridBuscaVisitaDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure BtnCapturarClick(Sender: TObject);
    procedure btnVideoClick(Sender: TObject);
    procedure btnMuestraClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure eBuscaVisitaKeyPress(Sender: TObject; var Key: Char); //acl
  private
    { Private declarations }
    FBuscaVisitaActivada: Boolean;
    FBuscaVisitandoAActivada: Boolean;
    FBuscaCompaniaActivada: Boolean;
    FVisitaAgregada: Boolean;
    FCompaniaAgregada: Boolean;
    FAnfitrionAgregada: Boolean;
    fEditor : TEditorCliente;
    lVaciosVisA : Boolean; //acl2    
    procedure SetDriver;
    procedure TomaFotografia;

    function CancelarWizard: Boolean;
    function EjecutarWizard: Boolean;
    procedure TerminarWizard;

    procedure Connect;
    procedure Disconnect;
    procedure DoConnect;
    procedure DoDisconnect;

    procedure ChecaGlobales;

    procedure InitCompania;
    procedure InitVisita;
    procedure InitVisitandoA;

    procedure ToggleCompania(const lVisible: Boolean);
    procedure ToggleVisita(const lVisible: Boolean);
    procedure ToggleVisitandoA(const lVisible: Boolean);
    procedure ToggleControles(const lVisible: Boolean; oControl1,oControl2, oControl3, oControl4, oControl5: TControl);

    procedure BuscaCompania;overload;
    procedure BuscaCompania( const sBusca: string );overload;
    procedure BuscaVisitandoA;overload;
    procedure BuscaVisitandoA( const sBusca: string );overload;
    procedure BuscaVisitantes;overload;
    procedure BuscaVisitantes( const sBusca: string );overload;

    procedure InitGrids;
    procedure AgregarCompania;
    procedure AgregarVisitante;
    procedure AgregarAnfitrion;
    procedure AntesDeEnviar;
    procedure SetPaginaPrincipal;
    function ExisteCompania: Boolean;
    function ExisteVisitandoA: Boolean;
    function ExisteVisitante: Boolean;
    function VisitanteConEmpresa: Boolean;
    function VisitanteAdentro(var sMensaje: String): Boolean;
    procedure GetVisitandoA;//acl

    procedure DialogoError(const sMensaje: string; oControl: TWinControl);

    procedure GetNombres( sPrettyName: string; var sNombre, sApePat,sApeMat: string );
    property BuscaVisitaActivada:Boolean read FBuscaVisitaActivada default FALSE;
    property BuscaVisitandoAActivada:Boolean read FBuscaVisitandoAActivada default FALSE;
    property BuscaCompaniaActivada:Boolean read FBuscaCompaniaActivada default FALSE;
    procedure AgregaInfo;

    procedure SetControls(lHabilita: Boolean);

  protected
      procedure KeyPress(var Key: Char); override;
  public
    { Public declarations }
  end;

var
  AutoRegistroEntrada: TAutoRegistroEntrada;

implementation
uses
    FBuscaAnfitriones,
    FBuscaVisitantes,
    FTressShell,
    ZetaDialogo,
    ZetaCommonLists,
    ZetaCommonTools,
    ZetaCommonClasses,
    ZGlobalTress,
    DGlobal,
    DVisitantes,
    FMostrarFotografia,
    ZAccesosTress;
 //:todo    FToolsFoto;


{$R *.DFM}

const
     K_CONTINUAR = '� Para poder continuar, es necesario que escriba ';
     K_MSG_FAVOR_NOMBRES = K_CONTINUAR + 'su nombre y apellido !';
     K_MSG_FAVOR_NOMBRE = K_CONTINUAR + 'su nombre !';
     K_MSG_FAVOR_APELLIDO  = K_CONTINUAR + 'su apellido';
     K_MSG_FAVOR_COMPANIA = K_CONTINUAR + 'el nombre de la compa��a';
     K_MSG_FAVOR_VISITANDO = K_CONTINUAR + 'el nombre de la persona a la que visita';

procedure TAutoRegistroEntrada.FormCreate(Sender: TObject);
 var
    sTitulo: string;
begin
     inherited;
     sTitulo := ' Bienvenido a ' + Global.GetGlobalString( K_GLOBAL_RAZON_EMPRESA );
     PanelTitulo1.Caption := sTitulo;
     PanelTitulo2.Caption := sTitulo;

    { for i:= 0 to PageControl.PageCount - 1 do
         PageControl.Pages[i].TabVisible := FALSE;        //:todo
                //:todo
     PageControl.ActivePage := tsBuscaVisita;}

     InitGrids;

          //IndexDerechos := ZAccesosTress.D_REG_LIBRO;
     FEditor := TEditorCliente.Create;

     cbNoExisteVehiculo.Visible := NOT Global.GetGlobalBooleano( K_GLOBAL_REQ_VEHICULO );
     cbNoExisteID.Visible := NOT Global.GetGlobalBooleano( K_GLOBAL_REQ_IDENTIFICA );
     cbNoExisteAsunto.Visible := NOT Global.GetGlobalBooleano( K_GLOBAL_REQ_ASUNTO );
     HelpContext := H_VIS_REG_ENTRADA;

  //:todo   FToolsVideo.SetDriver := SetDriver; //acl
  //:todo   FToolsVideo.SetControls := SetControls;  //acl
end;

procedure TAutoRegistroEntrada.FormShow(Sender: TObject);
begin
     inherited;
     FVisitaAgregada := FALSE;
     FCompaniaAgregada := FALSE;
     FAnfitrionAgregada := FALSE;

     DoConnect;

     InitVisita;
     InitCompania;
     InitVisitandoA;

     Wizard.Primero;

     eBuscaVisita.SetFocus;

     SetPaginaPrincipal;
end;

procedure TAutoRegistroEntrada.Connect;
begin
     with dmVisitantes do
     begin
          cdsTipoAsunto.Refrescar;
          cdsTipoCarro.Refrescar;
          cdsTipoID.Refrescar;
          cdsTipoVisita.Refrescar;
          cdsEmpVisitante.Conectar;//Se refresca en la busqueda de compa�ia
          //cdsCompaniaLookup.Conectar;
          //cdsCaseta.Conectar;
          //cdsDepto.Conectar;
          DataSource.DataSet:= cdsLibros;
          dsTipoVisitante.DataSet := cdsTipoVisita;
          dsBuscaVisitandoA.DataSet := cdsAnfitrionLookup;
          dsBuscaVisita.DataSet := cdsVisitanteLookup;
          dsBuscaCompania.DataSet := cdsCompaniaLookup;
          dsAsunto.Dataset := cdsTipoAsunto;
          dsTipoID.Dataset := cdsTipoID;
          dsVehiculo.DataSet := cdsTipoCarro;
          dsVisita.Dataset := cdsVisitante;
     end;
     //LI_NOMBRE.SetFocus;
     ChecaGlobales;
     VI_SEXO.ItemIndex :=0;

end;

procedure TAutoRegistroEntrada.Disconnect;
begin
     Datasource.Dataset := nil;
end;

procedure TAutoRegistroEntrada.DoConnect;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             try
                Connect;
             except
                   on Error: Exception do
                      zExcepcion( '� Error Al Conectar Forma !', '� Ventana No Pudo Ser Conectada A Sus Datos !', Error, 0 );
             end;
          finally
                 Cursor := oCursor;
          end;
     end;

end;

procedure TAutoRegistroEntrada.DoDisconnect;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Disconnect;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

procedure TAutoRegistroEntrada.DialogoError( const sMensaje: string; oControl:TWinControl );
begin
     oControl.SetFocus;
     ZInformation( Caption, sMensaje, 0 );
end;

procedure TAutoRegistroEntrada.KeyPress( var Key: Char );
begin
     case Key of
          Chr( VK_RETURN ):
          begin
               if ( ActiveControl = eBuscaVisita ) then
               begin
                    Key := #0;
                    bBuscaVisita.Click;
               end;
               if ( ActiveControl = eBuscaCompania ) then
               begin
                    Key := #0;
                    bBuscaCompania.Click;
               end;
               if ( ActiveControl = eBuscaVisitandoA ) then
               begin
                    Key := #0;
                    bBuscaVisitandoA.Click;
               end;
          end;
     end;
     inherited KeyPress( Key );
end;



procedure TAutoRegistroEntrada.InitGrids;
 var
    oOptions: TDBGridOptions;
begin
     oOptions := [dgColumnResize,dgRowLines,dgTabs,dgRowSelect,dgAlwaysShowSelection,dgConfirmDelete,dgCancelOnExit];
     GridBuscaVisita.Options := oOptions;
     GridBuscaVisitandoA.Options := oOptions;
     GridBuscaCompania.Options := oOptions;
     VI_TIPO.Options := oOptions;
     LI_ASUNTO.Options := oOptions;
     LI_TIPO_ID.Options := oOptions;
     LI_CAR_TIP.Options := oOptions;
end;

procedure TAutoRegistroEntrada.InitVisita;
begin
     eBuscaVisita.Text:= VACIO;
     cbNoExisteVisita.Checked := FALSE;
     ToggleVisita(FALSE);
end;

procedure TAutoRegistroEntrada.InitCompania;
begin
     eBuscaCompania.Text:= VACIO;
     cbNoExisteCompania.Checked := FALSE;
     ToggleCompania(FALSE);
end;

procedure TAutoRegistroEntrada.InitVisitandoA;
begin
     eBuscaVisitandoA.Text:= VACIO;
     cbNoExisteVisitandoA.Checked := FALSE;
     ToggleVisitandoA(FALSE);
end;


procedure TAutoRegistroEntrada.ToggleControles( const lVisible : Boolean;
          oControl1, oControl2, oControl3, oControl4, oControl5: TControl );
begin
     //oControl1.Visible := lVisible;
     oControl2.Visible := lVisible;
     oControl3.Visible := lVisible;
     oControl4.Visible := lVisible;
     oControl5.Visible := lVisible; //ACL
end;

procedure TAutoRegistroEntrada.ToggleVisita( const lVisible: Boolean );
begin
     ToggleControles( lVisible, eBuscaVisita, lbBuscaVisita, GridBuscaVisita, CBNoExisteVisita, btnMuestra);
end;

procedure TAutoRegistroEntrada.ToggleCompania( const lVisible: Boolean );
begin
     ToggleControles( lVisible, eBuscaCompania, lbBuscaCompania, GridBuscaCompania, CBNoExisteCompania, btnMuestra );
end;

procedure TAutoRegistroEntrada.ToggleVisitandoA( const lVisible: Boolean );
begin
     ToggleControles( lVisible, eBuscaVisitandoA, lbBuscaVisitandoA, GridBuscaVisitandoA, CBNoExisteVisitandoA, btnMuestra );
end;

procedure TAutoRegistroEntrada.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        Application.ProcessMessages;
        try
           lOk := EjecutarWizard;
        except
              on Error: Exception do
              begin
                   zExcepcion( Caption, 'Error Al Ejecutar Wizard', Error, 0 );
                   lOk := False;
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     if lOk then
        TerminarWizard;
end;

function TAutoRegistroEntrada.ExisteVisitante: Boolean;
begin
     Result := NOT cbNoExisteVisita.Checked;
end;

function TAutoRegistroEntrada.ExisteVisitandoA: Boolean;
begin
     Result := NOT cbNoExisteVisitandoA.Checked;
end;

function TAutoRegistroEntrada.ExisteCompania: Boolean;
begin
     Result := NOT cbNoExisteCompania.Checked;
end;

function TAutoRegistroEntrada.VisitanteConEmpresa: Boolean;
begin
     Result := RGRepresentaCompania.ItemIndex = 0;
end;

//acl2
procedure TAutoRegistroEntrada.GetVisitandoA;
var
   lEnabled: Boolean;
   sNombre, sApePat, sApeMat: string;
begin
     lEnabled := not ExisteVisitandoA;
     tsVisitandoANombre.Enabled := lEnabled;
     if lEnabled then
     begin
          GetNombres( PropperCase( eBuscaVisitandoA.Text ), sNombre, sApePat, sApeMat );
          eNombreAnfitrion.Text := sNombre;
          eApePatAnfitrion.Text := sApePat;
          eApeMatAnfitrion.Text := sApeMat;
     end;
end;

procedure TAutoRegistroEntrada.AgregarVisitante;
begin
     if NOT ExisteVisitante then
     begin
          with dmVisitantes do
          begin
               with cdsVisitante do
               begin
                    if ( strLleno( dmVisitantes.cdsVisitante.FieldByName('VI_FOTO').AsString ) ) then
                    begin
                        { with Documento do  //acl
                         begin
                              CutToClipboard;
                              UpdateAsJPG:=True;                   //:todo
                              PastefromClipboard;
                         end;}
                         try
                           { if( Documento.Picture.Bitmap.Width > 300 ) then
                            begin                                                            //:todo
                                FToolsFoto.ResizeImageBestFit( Documento, 180, 300 );
                            end;}
                         except
                               on Error: Exception do
                               begin
                                    ZError( self.Caption,'No se pudo ajustar el tama�o de la fotograf�a. Se guardar� con su tama�o original'+CR_LF+Error.Message, 0);
                               end;
                         end;
                    end;

                    if dmVisitantes.AutoReg_AsuntoATratar then
                       FieldByName('VI_ASUNTO').AsString :=  cdsTipoAsunto.FieldByName('TB_CODIGO').AsString;
                    FieldByName('VI_TIPO').AsString :=  cdsTipoVisita.FieldByName('TB_CODIGO').AsString;
                    FieldByName('VI_SEXO').AsString :=  ObtieneElemento( lfSexo, VI_SEXO.ItemIndex );
                    if ExisteVisitandoA then
                    begin
                       if ( lVaciosVisA ) then   //acl2
                       begin
                            FieldByName('AN_NUMERO').AsInteger := 0;
                       end
                       else
                       FieldByName('AN_NUMERO').AsInteger := cdsAnfitrionLookup.FieldByName('AN_NUMERO').AsInteger
                    end
                    else if FAnfitrionAgregada then
                       FieldByName('AN_NUMERO').AsInteger := cdsAnfitrion.FieldByName('AN_NUMERO').AsInteger;
                    if VisitanteConEmpresa then
                       FieldByName('EV_NUMERO').AsInteger := cdsCompaniaLookup.FieldByName('EV_NUMERO').AsInteger;

                    Enviar;
                    DisableControls; //acl {Se cambio a despues de enviar para que guarde la foto. ACL}

                    try
                       //Refrescar;
                       Last;
                       FVisitaAgregada := TRUE;
                    finally
                           EnableControls;
                    end;
               end;
          end;
     end;
end;

procedure TAutoRegistroEntrada.AgregarCompania;
begin
     if NOT ExisteCompania then
     begin
          with dmVisitantes.cdsEmpVisitante do
          begin
               Agregar;
               FieldByName('EV_NOMBRE').AsString := eCompaniaNombre.Text;
               Enviar;
               Refrescar;
               dmVisitantes.cdsCompaniaLookup.Last;
               FCompaniaAgregada := TRUE;
          end;
     end;
end;

procedure TAutoRegistroEntrada.AgregarAnfitrion;
begin
     if NOT ExisteVisitandoA then
     begin
          if StrLleno( eApePatAnfitrion.Text ) OR
             StrLleno( eNombreAnfitrion.Text ) then
          begin

               with dmVisitantes.cdsAnfitrion do
               begin
                    Agregar;
                    FieldByName('AN_APE_PAT').AsString := eApePatAnfitrion.Text;
                    FieldByName('AN_APE_MAT').AsString := eApeMatAnfitrion.Text;
                    FieldByName('AN_NOMBRES').AsString := eNombreAnfitrion.Text;
                    DisableControls;
                    try
                       Enviar;
                       Refrescar;
                       IndexFieldNames := 'AN_NUMERO';
                       Last;
                    finally
                           EnableControls;
                    end;
                    //dmVisitantes.cdsAnfitrionLookup.Last;
                    FAnfitrionAgregada := TRUE;
               end;
          end;
     end;
end;

procedure TAutoRegistroEntrada.AntesDeEnviar;

 function GetDato( const sCodigo: string; const lNoExiste: Boolean ): string;
 begin
      if lNoExiste then
         Result := VACIO
      else
          Result := sCodigo;
 end;
 var
    DataSetVisita: TDataSet;
    DataSetAnfitrion: TDataset;
begin
     with dmVisitantes do
     begin
          with cdsLibros do
          begin
               if dmVisitantes.AutoReg_Identificacion then
                  FieldByName('LI_TIPO_ID').AsString := GetDato( cdsTipoID.FieldByName('TB_CODIGO').AsString, cbNoExisteID.Checked );

               if dmVisitantes.AutoReg_Vehiculo then
                  FieldByName('LI_CAR_TIP').AsString := GetDato( cdsTipoCarro.FieldByName('TB_CODIGO').AsString, cbNoExisteVehiculo.Checked );

               if ExisteVisitante OR dmVisitantes.AutoReg_AsuntoATratar then
                  FieldByName('LI_ASUNTO').AsString := GetDato( cdsTipoAsunto.FieldByName('TB_CODIGO').AsString, cbNoExisteAsunto.Checked );

               if ExisteVisitante then
                  DataSetVisita := cdsVisitanteLookup
               else
                   DataSetVisita := cdsVisitante;

               FieldByName('VI_NUMERO').AsInteger := DataSetVisita.FieldByName('VI_NUMERO').AsInteger;
               FieldByName('LI_NOMBRE').AsString := DataSetVisita.FieldByName( 'VI_NOMBRES' ).AsString +' '+DataSetVisita.FieldByName( 'VI_APE_PAT' ).AsString +' '+DataSetVisita.FieldByName( 'VI_APE_MAT' ).AsString;

               if VisitanteConEmpresa then
               begin
                    FieldByName('EV_NUMERO').AsInteger := cdsCompaniaLookup.FieldByName('EV_NUMERO').AsInteger;
                    FieldByName('LI_EMPRESA').AsString := cdsCompaniaLookup.FieldByName('EV_NOMBRE').AsString;
               end;

               if ( ExisteVisitandoA and not lVaciosVisA ) then //acl2
                  DataSetAnfitrion := cdsAnfitrionLookup
               else if FAnfitrionAgregada then
                    DataSetAnfitrion := cdsAnfitrion
               else
                   DataSetAnfitrion := NIL;

               if (DataSetAnfitrion <> NIL ) then
               begin
                    FieldByName('AN_NUMERO').AsInteger := DataSetAnfitrion.FieldByName('AN_NUMERO').AsInteger;
                    FieldByName('LI_ANFITR').AsString :=  DataSetAnfitrion.FieldByName(P_ANFITRION_NAME).AsString;

                    FieldByName('LI_CDEPTO').AsString := DataSetAnfitrion.FieldByName('AN_DEPTO').AsString;
                    FieldByName('LI_DEPTO').AsString := DataSetAnfitrion.FieldByName('TB_ELEMENT').AsString;
               end;
          end;
     end;
end;

function TAutoRegistroEntrada.EjecutarWizard: Boolean;
var
   oCursor: TCursor;
   iRecords, iFolio : integer;
   sIndex: string;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          iFolio := -1;
          try
             Result := TRUE;
             try

                with dmVisitantes.cdsLibros do
                begin
                     iRecords := RecordCount;

                     if NOT FCompaniaAgregada then
                        AgregarCompania;

                     if NOT FAnfitrionAgregada then
                        AgregarAnfitrion;

                     if NOT FVisitaAgregada then
                        AgregarVisitante;


                     AntesDeEnviar;
                     try
                        DisableControls;
                        sIndex := IndexFieldNames;
                        IndexFieldNames := 'LI_FOLIO';

                        Enviar;

                        Refrescar;

                        if ( iRecords <> RecordCount ) then Last;

                        iFolio := FieldByName('LI_FOLIO').AsInteger;

                        TressShell.ImprimeForma(TRUE);

                     finally
                            IndexFieldNames := sIndex;
                            if (iFolio >= 0 ) then
                               Locate('LI_FOLIO',VarArrayOf([iFolio]), []);
                            EnableControls;
                     end;
                end;

             except
                      Result := FALSE;
                      Raise;
             end;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

function TAutoRegistroEntrada.CancelarWizard: Boolean;
begin
     with dmVisitantes do
     begin
          cdsLibros.Cancel;
          cdsVisitante.Cancel;
     end;

     Close;
     Result := True;
end;

procedure TAutoRegistroEntrada.TerminarWizard;
begin
     if not Wizard.Reejecutar then
     begin
          Close;
          Windows.SetForegroundWindow( Handle ); { Evita que la aplicaci�n pierda el Focus }
     end;
end;

procedure TAutoRegistroEntrada.WizardAlCancelar(Sender: TObject; var lOk: Boolean);
begin
     inherited;
     lOk := CancelarWizard;
end;

procedure TAutoRegistroEntrada.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     inherited;
     DoDisconnect;
     Action := caHide;

end;

procedure TAutoRegistroEntrada.ChecaGlobales;
begin
     {LI_ANFITR.ReadOnly := not Global.GetGlobalBooleano( K_GLOBAL_ABIERTA_ANFI );
     LI_NOMBRE.ReadOnly := not Global.GetGlobalBooleano( K_GLOBAL_ABIERTA_VISI );
     LI_EMPRESA.ReadOnly := not Global.GetGlobalBooleano( K_GLOBAL_ABIERTA_COMPANY );
     LI_DEPTO.ReadOnly := not Global.GetGlobalBooleano( K_GLOBAL_ABIERTA_DEPTO );}
end;

procedure TAutoRegistroEntrada.AgregaInfo;
 var sDato: string;
begin
     With FEditor do
     begin
          Inicializa(oRichEdit);
          with dmVisitantes do
          begin

               if ExisteVisitante then
                  sDato := cdsVisitanteLookup.FieldByName(P_VISITA_NAME).AsString
               else
               begin
                    with cdsVisitante do
                         sDato := Trim( FieldByName( 'VI_NOMBRES' ).AsString ) + ', ' +
                                  Trim( FieldByName( 'VI_APE_PAT' ).AsString ) + ' ' +
                                  Trim( FieldByName( 'VI_APE_MAT' ).AsString );
               end;


               Titulo( 'Visitante: ' +  sDato );

               if ExisteCompania then
               begin
                    sDato := cdsCompaniaLookup.FieldByName('EV_NOMBRE').AsString;
               end
               else
                   sDato := eCompaniaNombre.Text;

               Titulo( 'Compa��a: ' + sDato );
               RenglonVacio;

               if FAnfitrionAgregada then
               begin
                    Titulo1( 'Visita A: ' + cdsAnfitrionLookup.FieldByName(P_ANFITRION_NAME).AsString );
                    RenglonVacio;
               end;
          end;
     end;
end;

procedure TAutoRegistroEntrada.GetNombres( sPrettyName: string; var sNombre, sApePat, sApeMat: string );

 function GetDato( const sBusca: string; var sPretty: string ): string;
  var iPos: integer;
 begin
      iPos := Pos( sBusca, sPrettyName );
      if ( iPos > 0 ) then
      begin
           Result := Copy( sPretty, 1, iPos - 1 );
           sPretty := Trim( Copy( sPretty, iPos + 1, Length( sPretty ) ));
      end;
 end;

begin
     sNombre := GetDato( ',', sPrettyName );

     if StrVacio( sNombre ) then
        sNombre := GetDato( ' ', sPrettyName );

     sApePat := GetDato( ',', sPrettyName );
     if StrVacio( sApePat ) then
        sApePat := GetDato( ' ', sPrettyName );

     if StrLleno( sApePat ) then
        sApeMat := sPrettyName
     else
         sApePat := sPrettyName;
end;


procedure TAutoRegistroEntrada.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
 var
    lEnabled : Boolean;
    sBusca,sNombre, sApePat, sApeMat, sError: string;
begin
     inherited;
     if CanMove then
     begin
          if Wizard.Adelante then
          begin
               tsAsuntoATratar.Enabled := dmVisitantes.AutoReg_AsuntoATratar;
               tsIdentificacion.Enabled := dmVisitantes.AutoReg_Identificacion;
               tsVehiculo.Enabled := dmVisitantes.AutoReg_Vehiculo;
               tsObservaciones.Enabled := dmVisitantes.AutoReg_Observaciones;

               if Wizard.EsPaginaActual( tsBuscaVisita ) then
               begin
                    CanMove := BuscaVisitaActivada;

                    if CanMove then
                    begin
                         lEnabled := NOT ExisteVisitante;
                         tsDatosPersonales1.Enabled := lEnabled;
                         tsDatosPersonales2.Enabled := lEnabled;
                         tsCompaniaPregunta.Enabled := TRUE;


                         eBuscaCompania.Text := VACIO;
                         ToggleCompania(FALSE);
                         eBuscaVisitandoA.Text := VACIO;
                         FBuscaVisitandoAActivada:= False; //acl
                         ToggleVisitandoA(FALSE);

                         if ExisteVisitante then
                         begin
                              CanMove:= not VisitanteAdentro(sError);
                              if CanMove then
                              begin
                                   with dmVisitantes do
                                   begin
                                        with cdsVisitanteLookup do
                                             lEnabled := (FieldByName('EV_NUMERO').AsInteger = 0) OR
                                                         FieldByName('EV_NUMERO').IsNull;

                                        tsCompaniaPregunta.Enabled := lEnabled;

                                        if NOT lEnabled then
                                        begin
                                             sBusca := cdsVisitanteLookup.FieldByName('EV_NOMBRE').AsString;;
                                             eBuscaCompania.Text := sBusca;
                                             BuscaCompania( sBusca );
                                             tsCompaniaPregunta.Enabled := cdsCompaniaLookup.IsEmpty;
                                        end;


                                          with cdsVisitanteLookup do
                                               lEnabled := (FieldByName('AN_NUMERO').AsInteger = 0) OR
                                                           FieldByName('AN_NUMERO').IsNull;

                                        if NOT lEnabled then
                                        begin
                                             sBusca := cdsVisitanteLookup.FieldByName('AN_NOMBRE').AsString;
                                             eBuscaVisitandoA.Text := sBusca;
                                             BuscaVisitandoA( sBusca );
                                        end;

                                        if NOT cdsTipoAsunto.Locate( 'TB_CODIGO', cdsVisitanteLookup.FieldByName('VI_ASUNTO').AsString, [] ) then
                                           cdsTipoAsunto.First;
                                   end;
                              end
                              else
                              begin
                                   CanMove:= False;
                                   DialogoError( sError, eBuscaVisita );
                              end;
                         end
                         else
                         begin
                              with dmVisitantes.cdsVisitante do
                              begin
                                   Agregar;

                                   GetNombres( PropperCase( eBuscaVisita.Text ), sNombre, sApePat, sApeMat );
                                   FieldByName( 'VI_NOMBRES' ).AsString := sNombre;
                                   FieldByName( 'VI_APE_PAT' ).AsString := sApePat;
                                   FieldByName( 'VI_APE_MAT' ).AsString := sApeMat;
                              end;
                         end;
                    end
                    else
                    begin
                         DialogoError( K_MSG_FAVOR_NOMBRES, eBuscaVisita );
                    end;
               end
               else if Wizard.EsPaginaActual( tsDatosPersonales1 ) then
               begin
                    CanMove := NOT( StrVacio(VI_NOMBRES.Text) or StrVacio(VI_APE_PAT.Text) );

                    if StrVacio(VI_NOMBRES.Text) then
                       DialogoError( K_MSG_FAVOR_NOMBRE, VI_NOMBRES )

                    else if StrVacio(VI_APE_PAT.Text) then
                       DialogoError( K_MSG_FAVOR_APELLIDO, VI_APE_PAT );

               end
               //else if Wizard.EsPaginaActual( tsDatosPersonales2 ) then
               else if Wizard.EsPaginaActual( tsCompaniaPregunta ) then
               begin
                    lEnabled := VisitanteConEmpresa;
                    tsBuscaCompania.Enabled := lEnabled;
                    tsCompaniaNombre.Enabled := lEnabled;
               end
               else if Wizard.EsPaginaActual( tsBuscaCompania ) then
               begin
                    CanMove := BuscaCompaniaActivada;
                    if CanMove then
                    begin
                         lEnabled := NOT ExisteCompania;
                         tsCompaniaNombre.Enabled := lEnabled;
                         if lEnabled then
                         begin
                              eCompaniaNombre.Text := PropperCase( eBuscaCompania.Text );
                         end;
                    end
                    else
                    begin
                         DialogoError( K_MSG_FAVOR_COMPANIA, eBuscaCompania );
                    end;
               end
               else if Wizard.EsPaginaActual( tsBuscaVisitandoA ) then
               begin
                    if ( Global.GetGlobalBooleano( K_GLOBAL_REQ_ANFITRION ) ) then
                    begin
                         CanMove := BuscaVisitandoAActivada; //acl2
                         if CanMove then //acl2
                         begin
                              GetVisitandoA;
                         end
                         else
                         begin
                              DialogoError( K_MSG_FAVOR_VISITANDO, eBuscaVisitandoA );
                         end;
                    end
                    else
                    begin //acl2
                         GetVisitandoA;
                         lVaciosVisA := NOT FBuscaVisitandoAActivada;
                    end;
               end
               else if Wizard.EsPaginaActual( tsVisitandoANombre ) then
               begin
                    if ( Global.GetGlobalBooleano( K_GLOBAL_REQ_ANFITRION ) ) then //acl2
                    begin
                          CanMove := NOT( StrVacio(eNombreAnfitrion.Text) or StrVacio(eApePatAnfitrion.Text) );

                          if StrVacio(eNombreAnfitrion.Text) then
                             DialogoError( K_MSG_FAVOR_NOMBRE, eNombreAnfitrion )

                          else if StrVacio(eApePatAnfitrion.Text) then
                             DialogoError( K_MSG_FAVOR_APELLIDO, eApePatAnfitrion );
                    end;
               end
               //else if Wizard.EsPaginaActual( tsAsuntoATratar ) then
               //else if Wizard.EsPaginaActual( tsIdentificacion ) then
               //else if Wizard.EsPaginaActual( tsVehiculo ) then
               else if Wizard.EsPaginaActual( tsObservaciones ) then
               begin
                    //AgregaInfo;
               end
               //else if Wizard.EsPaginaActual( tsGracias ) then
          end
          {else
          begin
               if Wizard.EsPaginaActual( tsBuscaVisita ) then
               else if Wizard.EsPaginaActual( tsBuscaVisita ) then
               else if Wizard.EsPaginaActual( tsDatosPersonales1 ) then
               else if Wizard.EsPaginaActual( tsDatosPersonales2 ) then
               else if Wizard.EsPaginaActual( tsCompaniaPregunta ) then
               else if Wizard.EsPaginaActual( tsBuscaCompania ) then
               else if Wizard.EsPaginaActual( tsCompaniaNombre ) then
               else if Wizard.EsPaginaActual( tsBuscaVisitandoA ) then
               else if Wizard.EsPaginaActual( tsVisitandoANombre ) then
               else if Wizard.EsPaginaActual( tsAsuntoATratar ) then
               else if Wizard.EsPaginaActual( tsIdentificacion ) then
               else if Wizard.EsPaginaActual( tsVehiculo ) then
               else if Wizard.EsPaginaActual( tsObservaciones ) then
               else if Wizard.EsPaginaActual( tsGracias ) then
          end;}
     end;
end;

procedure TAutoRegistroEntrada.WizardAfterMove(Sender: TObject);
begin
     inherited;
     if Wizard.EsPaginaActual( tsBuscaVisita ) then
     begin
          eBuscaVisita.SetFocus;
     end
     else if Wizard.EsPaginaActual( tsDatosPersonales1 ) then
     begin
          VI_NOMBRES.SetFocus;
     end
     else if Wizard.EsPaginaActual( tsDatosPersonales2 ) then
     begin
          VI_FEC_NAC.SetFocus;
     end
     else if Wizard.EsPaginaActual( tsCompaniaPregunta ) then
     begin
          RGRepresentaCompania.SetFocus;
     end
     else if Wizard.EsPaginaActual( tsBuscaCompania ) then
     begin
          eBuscaCompania.SetFocus;
     end
     else if Wizard.EsPaginaActual( tsCompaniaNombre ) then
     begin
          eCompaniaNombre.SetFocus;
     end
     else if Wizard.EsPaginaActual( tsBuscaVisitandoA ) then
     begin
          eBuscaVisitandoA.SetFocus;
     end
     else if Wizard.EsPaginaActual( tsVisitandoANombre ) then
     begin
          eNombreAnfitrion.SetFocus;
     end
     else if Wizard.EsPaginaActual( tsAsuntoATratar ) then
     begin
          LI_ASUNTO.SetFocus;
     end
     else if Wizard.EsPaginaActual( tsIdentificacion ) then
     begin
          LI_TIPO_ID.SetFocus;
     end
     else if Wizard.EsPaginaActual( tsVehiculo ) then
     begin
          LI_CAR_TIP.SetFocus;
     end
     else if Wizard.EsPaginaActual( tsObservaciones ) then
     begin
          LI_OBSERVA.SetFocus;
     end
     else if Wizard.EsPaginaActual( tsGracias ) then
     begin
          AgregaInfo;
     end;

end;

procedure TAutoRegistroEntrada.bBuscaVisitaClick(Sender: TObject);
begin
     inherited;
     BuscaVisitantes;
end;

procedure TAutoRegistroEntrada.bBuscaCompaniaClick(Sender: TObject);
begin
     inherited;
     BuscaCompania;
end;

procedure TAutoRegistroEntrada.bBuscaVisitandoAClick(Sender: TObject);
begin
     inherited;
     BuscaVisitandoA;
end;

procedure TAutoRegistroEntrada.BuscaCompania( const sBusca: string );
begin
     ToggleCompania( TRUE );
     with dmVisitantes do
     begin
          cdsEmpVisitante.Refrescar;
          with cdsCompaniaLookup do
          begin
               Filtered := TRUE;
               Filter := 'Upper(EV_NOMBRE) like ' + EntreComillas( '%' + UpperCase(sBusca) + '%');
          end;
     end;
     FBuscaCompaniaActivada:= TRUE;
end;

procedure TAutoRegistroEntrada.BuscaCompania;
begin
     if StrLleno( eBuscaCompania.Text ) then
     begin
          BuscaCompania(eBuscaCompania.Text );

          with dmVisitantes.cdsCompaniaLookup do
          begin
               cbNoExisteCompania.Checked := IsEmpty;

               if IsEmpty then
               begin
                    eBuscaCompania.SelectAll;
                    eBuscaCompania.SetFocus;
               end
               else
               begin
                    GridBuscaCompania.SetFocus;
               end;
          end;

     end
     else
     begin
          DialogoError( K_MSG_FAVOR_COMPANIA, eBuscaCompania );
     end;
end;

procedure TAutoRegistroEntrada.BuscaVisitandoA( const sBusca: string );
var
   FParamList: TZetaParams;
begin
     ToggleVisitandoA( TRUE );

     FParamList := TZetaParams.Create;
     try
        FParamList.AddString( 'NOMBRE', UPPERCASE( eBuscaVisitandoA.Text ) );
        FParamList.AddString( 'DEPTO', VACIO );
        FParamList.AddInteger('STATUS', 0 );
        with dmVisitantes do
             HacerBusquedaAnfitrion( FParamList );

        FBuscaVisitandoAActivada:= TRUE;

     finally
            FreeAndNil( FParamList );
     end;

end;

procedure TAutoRegistroEntrada.BuscaVisitandoA;
begin
     if StrLleno( eBuscaVisitandoA.Text ) then
     begin
          BuscaVisitandoA( eBuscaVisitandoA.Text );
          with dmVisitantes do
          begin
               cbNoExisteVisitandoA.Checked := cdsAnfitrionLookup.IsEmpty;
               if cdsAnfitrionLookup.IsEmpty then
               begin
                    eBuscaVisitandoA.SelectAll;
                    eBuscaVisitandoA.SetFocus;
               end
               else
               begin
                    GridBuscaVisitandoA.SetFocus;
               end;
          end;
     end
     else
     begin
          DialogoError( K_MSG_FAVOR_VISITANDO, eBuscaVisitandoA );
     end;
end;

procedure TAutoRegistroEntrada.BuscaVisitantes( const sBusca: string );
var
   FParamList: TZetaParams;
begin
     ToggleVisita( TRUE );
     FParamList := TZetaParams.Create;

     try        
        FParamList.AddString( 'NOMBRE', UPPERCASE( eBuscaVisita.Text ) );
        FParamList.AddString( 'TIPO', VACIO );
        FParamList.AddString( 'ANFITRION', VACIO );
        FParamList.AddString( 'EMPRESA', VACIO );
        FParamList.AddInteger('STATUS', 0 );

        with dmVisitantes do
        begin
             HacerBusquedaVisitante( FParamList );
        end;
        FBuscaVisitaActivada:= TRUE;

     finally
            FreeAndNil( FParamList );
     end;
end;

procedure TAutoRegistroEntrada.BuscaVisitantes;
begin
     if StrLleno( eBuscaVisita.Text ) then
     begin
          BuscaVisitantes( eBuscaVisita.Text );
          with dmVisitantes do
          begin
               cbNoExisteVisita.Checked := cdsVisitanteLookup.IsEmpty;
               if cdsVisitanteLookup.IsEmpty then
               begin
                    eBuscaVisita.SelectAll;
                    eBuscaVisita.SetFocus;
                    btnMuestra.Visible:=false;//acl1                    
               end
               else
               begin
                    GridBuscaVisita.SetFocus;
               end;
          end;
     end
     else
     begin
          DialogoError( K_MSG_FAVOR_NOMBRES, eBuscaVisita );
     end;

end;



procedure TAutoRegistroEntrada.SiguienteClick(Sender: TObject);
begin
     inherited;
     Wizard.Siguiente;
end;

procedure TAutoRegistroEntrada.SetPaginaPrincipal;

     procedure SetControlPagina( const eActiveTabSheet: eTabSheetRegEnt );
     begin
          case eActiveTabSheet of
               eVisitante:
               begin
                    eBuscaVisita.Text:= dmVisitantes.cdsLibros.FieldByName('LI_NOMBRE').AsString;
                    cbNoExisteVisita.Checked := FALSE;
                    BuscaVisitantes(eBuscaVisita.Text);
               end;
               eCompany:
               begin
                    eBuscaCompania.Text:= dmVisitantes.cdsLibros.FieldByName('LI_EMPRESA').AsString;
                    cbNoExisteCompania.Checked:= FALSE;
                    BuscaCompania(eBuscaCompania.Text) ;
               end;
               eVisitaA:
               begin
                     eBuscaVisitandoA.Text:= dmVisitantes.cdsLibros.FieldByName('LI_ANFITR').AsString;
                     cbNoExisteVisitandoA.Checked:= FALSE;
                     BuscaVisitandoA(eBuscaVisitandoA.Text);
               end;
               eAsunto:
               begin
                    dsAsunto.DataSet.Locate('TB_CODIGO',dmVisitantes.cdsLibros.FieldByName('LI_ASUNTO').AsString,[] )
               end;
          end;
          Wizard.Siguiente;
     end;
begin
     with dmVisitantes.cdsLibros do
     begin
          if StrLleno( FieldByName('LI_NOMBRE').AsString ) then
          begin
               SetControlPagina( eVisitante );
               if ( FieldByName('EV_NUMERO').AsInteger <> 0  )then
               begin
                    SetControlPagina( eCompany );
                    if ( FieldByName('AN_NUMERO').AsInteger <> 0 ) then
                    begin
                         SetControlPagina( eVisitaA );
                         if StrLleno( FieldByName('LI_ASUNTO').AsString )  then
                            SetControlPagina( eAsunto );
                    end;
               end;
          end;
     end;
end;

//acl
procedure TAutoRegistroEntrada.GridBuscaVisitaDrawColumnCell( Sender: TObject; const Rect: TRect; DataCol:
Integer; Column: TColumn; State: TGridDrawState);

procedure CambiaColor(const sField: string);
var
   StatusCompany : eCatCompanys;
   StatusVis : eVisitanteStatus;
begin
     with TDBGrid( Sender ) do
     begin
          if ( Column.FieldName = P_VISITA_NAME ) then
          begin
               StatusVis := eVisitanteStatus( TDBGrid( Sender ).DataSource.DataSet.FieldByName( 'VI_STATUS' ).AsInteger );
               if ( StatusVis = vfSuspendido ) then
                   Canvas.Font.Color := clRed;
          end;
          if ( Column.FieldName = 'EV_NOMBRE' ) then
          begin
               StatusCompany := eCatCompanys( TDBGrid( Sender ).DataSource.DataSet.FieldByName( 'EV_STATUS' ).AsInteger );             
               if( StatusCompany = ccSuspendida) then
                    Canvas.Font.Color := clRed;
          end;
          
     DefaultDrawDataCell( Rect, Column.Field, State );
     end;
end;
begin
     CambiaColor( P_VISITA_NAME );
     CambiaColor( 'EV_NOMBRE' );
   inherited;

end;

//acl
procedure TAutoRegistroEntrada.btnMuestraClick(Sender: TObject);
begin
  inherited;
     dmVisitantes.MuestraFotografia;
end;

//acl
procedure TAutoRegistroEntrada.btnVideoClick(Sender: TObject);
begin
  inherited;
     SetControls( True );
  //:todo    btnCapturar.Enabled := FToolsVideo.InicializaVideo( VideoCap1, cbDrivers, lblDriver );
end;

//acl
procedure TAutoRegistroEntrada.SetControls( lHabilita: Boolean );
begin
 //:todo    if ( FToolsVideo.DriverManager( cbDrivers, lblDriver ) = true ) then
     begin
     //:todo      Documento.Visible:= lHabilita;
          btnVideo.Enabled := lHabilita;
      //:todo     VideoCap1.Visible:= not lHabilita;
          btnCapturar.Enabled:= not lHabilita;
     end
  //:todo   else
//:todo     begin
     //:todo     lHabilita:=false;
   //:todo        Documento.Visible:= not lHabilita;
     //:todo     btnVideo.Enabled := lHabilita;
   //:todo        VideoCap1.Visible:= lHabilita;
    //:todo      btnCapturar.Enabled:= lHabilita;
   //:todo  end;
end;

//acl
procedure TAutoRegistroEntrada.tsFotoShow(Sender: TObject);
begin
     inherited;
  //:todo   SetControls(  FToolsVideo.DriverManager( cbDrivers, lblDriver ) );
end;

//acl
procedure TAutoRegistroEntrada.SetDriver;
begin
 //:todo     VideoCap1.DriverIndex := cbDrivers.ItemIndex;
end;

//acl
procedure TAutoRegistroEntrada.BtnCapturarClick(Sender: TObject);
begin
  inherited;
       TomaFotografia;
end;

//acl
procedure TAutoRegistroEntrada.TomaFotografia;
begin
     SetControls( True );
{     VideoCap1.SaveToClipboard;
     with Documento do
     begin
          UpdateAsJPG:=True;               //:todo
          PastefromClipboard;
     end;}
end;

procedure TAutoRegistroEntrada.FormDestroy(Sender: TObject);
begin
     inherited;
//:todo     FToolsVideo.SetDriver := nil;
 //:todo    FToolsVideo.SetControls := nil;
end;

//acl
procedure TAutoRegistroEntrada.eBuscaVisitaKeyPress(Sender: TObject;
  var Key: Char);
begin
     if ( Key = ZetaCommonClasses.UnaCOMILLA ) then
        Key := Chr( 0 );
  inherited KeyPress( Key );
end;

function TAutoRegistroEntrada.VisitanteAdentro( var sMensaje: String): Boolean;
begin
     Result:= dmVisitantes.VisitanteAdentro(dmVisitantes.cdsVisitanteLookup.FieldByName('VI_NUMERO').AsInteger, sMensaje);
end;

end.
