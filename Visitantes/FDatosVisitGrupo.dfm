object FDatosVisitanteGrupo: TFDatosVisitanteGrupo
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Visitante registrado'
  ClientHeight = 190
  ClientWidth = 436
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 17
  object Label1: TLabel
    Left = 96
    Top = 16
    Width = 240
    Height = 25
    Caption = 'Se registr'#243' exitosamente a:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 157
    Top = 57
    Width = 24
    Height = 17
    Caption = 'No.:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 129
    Top = 77
    Width = 52
    Height = 17
    Caption = 'Visitante:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 127
    Top = 97
    Width = 54
    Height = 17
    Caption = 'Empresa:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 113
    Top = 117
    Width = 68
    Height = 17
    Caption = 'Visitando a:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Image1: TImage
    Left = 22
    Top = 55
    Width = 78
    Height = 79
    Picture.Data = {
      0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000046
      000000460806000000712EE284000000206348524D00007A25000080830000F9
      FF000080E9000075300000EA6000003A980000176F925FC54600000009704859
      7300000EC400000EC401952B0E1B0000068E49444154785EDD9C3D8F1C451086
      B10DC6878DF91001200758422022448E0422C6B77348A410F0132E2182C02992
      7D7BE7C80112FFC11148FC01A4BB5D9C939E101212020EF3E1E37D87AEA1A6A7
      7AB666A66776CF253DDA9DFEA8AE7AAF67A6A777F71E3B3D3D1D9DFD65F10C38
      0DFC137808FE0EAFBA5CDE4BFBB84EDAA7B0EA5926D04FDCA6D1C74C243718E8
      A21A54C488934FD1083A81B46B7BF58E6927921B0CA48589FFFA144ADE4B1B0F
      71FBB69960B5D5C70DCC447283819E0C03B625DF56D7953FD57B2D0EA128FC63
      5842569889E406035D568332A0F82FC6E393C0AFE0AF5026C9E87EC96422A4AF
      4031E43416AC7E256622B9C1403C95ACA0E263A205D1E531524F113943F82AEF
      891E4FB78F89FD9654C177B5F96171FDF36FDF0947690BC29C030C8233E267F0
      131001F8CA242428BED7893E007FA832D6CB2BFB6A3104B6D7F544C6915796E5
      17069D3F21E13069411811E745F052E03DF01A90FAA9B800AE8137C168C29CEE
      2F66EF8622D394309B0867D748C280F962F66A286E98F847BBA700678D0EAC42
      DA0963991A933387F19BB3A67720E85C09034EE6CBEDE74355CD74B2046D1B65
      16631A6300BC538E782A05306B8E6F1FED5C0AD595E96409DA36CA2CC634C600
      9E96D82DAA40F6BF9F3DDB8965713376365F148BBDC5CE85307E693AD9729CCD
      11E665C019D37E2A5995BD5814F76E1DCECE85186AC97A19DB1027E1F58E4B01
      338F2A18ABB23FB33B5FFEF07119844ED88365DE765E438C0257D98CB9316BAA
      81E28A0CEC7201A89319833E86D804DEAE290A177DB5F8AB01E28A1C1C2C8B42
      2731165D0DB1093253B80AAEC55E398F2B32B1AB1318932E86B86261A69C31FF
      5D6774F0EB2436C448F8CCC478271246DD9974709B8018E224BC2B4D738D89D7
      323AA84D400CB192DF00859153AAA2EA602EE2DA301778F6EAD7637B473B2FCC
      971F5C0C872ED3097781867865C6D472106A8DBB183AD71E09C009050BD59D8D
      0FA2986DDF1CDCFFF07C285A693AD92ED010AF08C3D3283D63BA1A3AD79F950E
      8BEBA1AA975198D2CFA2F80AE2542BE736D3C976056311D9C01A471824F37628
      EE6D224CE96F39FBCCBB3BD8178C43E4FA92BEF87635742E85C122EEA35034C8
      B4305EBF3AD1AE600CF20BA03072DBCE26CC4DCF5FD663B130258EDDC1BEC03F
      F9318C954F98BDC5EC2DEFB5C063A63080E5A149C374A25D816FC28DF9F65329
      07432C250C38E1AD3C34AB99158317F825F28C94BEF8E680C9F585D794383801
      F5C70787B3ADA04765560C5EE097C818E9532907B1F39C70458DC78CC78326A5
      593178814F2282A49FAE73103BCFCEA2F81AAFAF045DCC18BCC0CF16F83DF83E
      BB33E67F660FFAEE0E6AE08BDFD9E12796EDB7EB1CC4CE4764F0EE207C9C0772
      0A3D32C2B4EE0EA2DE2CD7B00D90DBF4B877A5D8F9C80CDA1D447F2233E55111
      66F8EE20FC105E63E8F3111026D3EE207C91862082D9A92FD6003989D732560C
      5EE08F9CFD6B8CB5FAB562F0029F44BE0632EEB3129F86FB537C1A07A730BF4D
      61C5E0053EF58C39930F91C9276C2B062FF04B648C714FA5219612A66D77D08A
      C10B7C93692EBE43CC1266D52E9E158317F83F9BC278F67DAD18BC600C22EB98
      0666A7BE0CB14898BB9EDD412B062F1823AF306DC64F2053BB6DAB4C84C135C5
      FDD992159F178C45F2EDE0B519175F48F0787FB97D3914B98DC27001D7E5D348
      2B3E2F489EE47B566A330A43A75D13A4ED2DB6AFDD3E2AAE86439759F179419C
      A4B17E11CC4E6DB499085382671A7D4A58BED60962CC2B0C3AF1F3A414BB400D
      30BB231751CBD73A417CA4B1D72B989DDAB09CAC6092EFE275057111D9DA6CC4
      6D766AC372B28AB6DDB67581B808670C85197EF18D1D3899ECBB785E1013C97A
      8D311DA5D9ACEFE209888DBF2558FDC5212FF365712B051CDEAD0D803BD317DF
      BD5FDE992C5FEB04F1F10E9A6F6BB3CDF4ED9A6B9983FB379E0855A6AF758218
      2F01D9A89A6A81373BC6E3C195505C9AE56B9D204E7EAE24424C228CF9ED04CB
      D7BA51428C2F0C9F79C261CD2C5FEB04C95F51420C17A62F186CCB2A5F178847
      FEDF84AC65D626CC0DC05FEC9BF55383389EA300A0210A313BE58686C12A3C16
      DAF2CEC104DE00AF034EFFAB06FC191F45B7287FA01AC784B28D1146BEB72F83
      F3BD1CCB7B5D9FA2AD8DE58F70854BB8FF42743BDDBFC24C2437411806C4E044
      A064500E74DF217E84860F3391DC04617224A0A1BFDC02559889E42608C301E3
      6436163391DC0461E2BF2E4F2B4B288F70561B4F3F376622B909C270401D7CEA
      BD05EB65E37A12CC44721384E10CE1628A3FE94DCD9614DE765E56F82B1EFE0B
      9E5CD47AC7483F480000000049454E44AE426082}
  end
  object edNumero: TZetaTextBox
    Left = 187
    Top = 58
    Width = 235
    Height = 17
    AutoSize = False
    Caption = 'edNumero'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    ShowAccelChar = False
    Layout = tlCenter
    Brush.Color = clBtnFace
    Border = True
  end
  object edVisitante: TZetaTextBox
    Left = 187
    Top = 78
    Width = 235
    Height = 17
    AutoSize = False
    Caption = 'edNumero'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    ShowAccelChar = False
    Layout = tlCenter
    Brush.Color = clBtnFace
    Border = True
  end
  object edEmpresa: TZetaTextBox
    Left = 187
    Top = 98
    Width = 235
    Height = 17
    AutoSize = False
    Caption = 'edNumero'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    ShowAccelChar = False
    Layout = tlCenter
    Brush.Color = clBtnFace
    Border = True
  end
  object edAnfitrion: TZetaTextBox
    Left = 187
    Top = 118
    Width = 235
    Height = 17
    AutoSize = False
    Caption = 'edNumero'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    ShowAccelChar = False
    Layout = tlCenter
    Brush.Color = clBtnFace
    Border = True
  end
  object PanelBotones: TPanel
    Left = 0
    Top = 154
    Width = 436
    Height = 36
    Align = alBottom
    TabOrder = 0
    ExplicitTop = 153
    DesignSize = (
      436
      36)
    object OK_DevEx: TcxButton
      Left = 11
      Top = 5
      Width = 118
      Height = 26
      Hint = 'Aceptar y Escribir Datos'
      Anchors = [akTop, akRight]
      BiDiMode = bdRightToLeft
      Caption = '  &Otro visitante'
      LookAndFeel.SkinName = 'TressMorado2013'
      ModalResult = 1
      OptionsImage.ImageIndex = 3
      OptionsImage.Images = cxImageList24_PanelBotones
      OptionsImage.Margin = 1
      ParentBiDiMode = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      OnClick = OK_DevExClick
    end
    object Cancelar_DevEx: TcxButton
      Left = 341
      Top = 5
      Width = 81
      Height = 26
      Hint = 'Cancelar Cambios'
      Anchors = [akTop, akRight]
      BiDiMode = bdRightToLeft
      Cancel = True
      Caption = '&Salir'
      LookAndFeel.SkinName = 'TressMorado2013'
      ModalResult = 2
      OptionsImage.ImageIndex = 2
      OptionsImage.Images = cxImageList24_PanelBotones
      OptionsImage.Margin = 1
      ParentBiDiMode = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
  end
  object cxImageList24_PanelBotones: TcxImageList
    Height = 24
    Width = 24
    FormatVersion = 1
    DesignInfo = 3735864
    ImageInfo = <
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF98A1E2FFF6F7FDFFB7BEEBFF4A5ACDFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFB1B8
          E9FFF6F7FDFFACB3E8FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFFE2E5F7FFFFFFFFFFFFFFFFFFC3C8EEFF4A5ACDFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFF
          FFFFFFFFFFFFF6F7FDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF9BA4E3FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5A
          CDFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFA6AEE6FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4A5ACDFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4A5ACDFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFC3C8EEFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6
          E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8
          EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5ACDFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5ACDFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFAFB6E9FFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5A
          CDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4A5ACDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4A5ACDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFFA9B0E7FFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFB7BEEBFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFFE2E5F7FFFFFFFFFFFFFFFFFFAFB6E9FF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFF6F7FDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF8792DEFFDFE2F6FFA1A9E5FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF9BA4
          E3FFDFE2F6FF98A1E2FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF65D8
          A1FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF8CE2B8FFE9F9
          F1FF54D396FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF65D8A1FFF4FCF8FFFFFF
          FFFFADEACCFF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF52D395FFD6F4E6FFFFFFFFFFFFFF
          FFFFFCFEFDFF70DAA7FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FFA5E8C8FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFD9F5E7FF52D395FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF78DDACFFFCFEFDFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF9AE5C1FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF5AD59AFFE9F9F1FFFFFFFFFFFFFFFFFFC3EFDAFFBDEE
          D6FFFFFFFFFFFFFFFFFFF4FCF8FF62D79FFF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FFC3EFDAFFFFFFFFFFF7FDFAFF8CE2B8FF4FD293FF54D3
          96FFE1F7ECFFFFFFFFFFFFFFFFFFC8F1DDFF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF8AE1B7FFE6F9F0FF6AD9A4FF4FD293FF4FD293FF4FD2
          93FF5FD69DFFE4F8EEFFFFFFFFFFFFFFFFFF8FE2BAFF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF52D395FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF65D8A1FFEFFBF5FFFFFFFFFFF7FDFAFF68D8A2FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF6AD9A4FFEFFBF5FFFFFFFFFFDEF7EBFF52D395FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF78DDACFFF7FDFAFFFFFFFFFFB8EDD3FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF70DAA7FFEFFBF5FFFFFFFFFFA0E6
          C4FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF65D8A1FFE1F7ECFFFFFF
          FFFF8FE2BAFF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF57D498FFBDEE
          D6FFFCFEFDFF8AE1B7FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF81DFB1FF8FE2BAFF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF5261CFFF6C79D6FF8D97DFFFACB3E8FFC8CD
          F0FFE8EAF9FFBAC0ECFF6F7CD7FF7582D9FF6471D4FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF848FDDFF8D97DFFFACB3E8FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFF969FE2FF6471D4FF969FE2FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
          FEFFD4D8F3FFBAC0ECFF4757CCFF4757CCFF969FE2FFF9FAFDFFFFFFFFFFFFFF
          FFFFA4ACE6FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE5E7F8FFBAC0ECFF4757CCFF4757CCFF6F7CD7FFD1D5F2FFE5E7F8FFBAC0
          ECFF818CDCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF6976D6FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFFACB3E8FFBAC0ECFFB1B8E9FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF5564D0FF7582D9FF969FE2FFB1B8
          E9FFD1D5F2FFACB3E8FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          200000000000000900000000000000000000000000000000000048BD85E64CC7
          8CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC7
          8CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC7
          8CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF248BD85E650D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF4CC78CF250D2
          93FF50D293FF50D293FF50D293FF51D293FF48D08EFF45CF8CFF45CF8CFF45CF
          8CFF45CF8CFF45CF8CFF45CF8CFF45CF8CFF45CF8CFF45CF8CFF45CF8CFF45CF
          8CFF4ED191FF51D294FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF50D293FF4FD293FF56D497FF59D498FF59D498FF59D4
          98FF59D498FF59D498FF59D498FF59D498FF59D498FF59D498FF59D498FF59D4
          98FF52D395FF4FD293FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF49D190FF45CF8DFFD5F4E5FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6FD
          FAFF71DAA7FF40CE89FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF4CD190FF4AD08FFFADEACCFFD6F4E6FFD0F3E2FFD0F3
          E2FFD0F3E2FFD0F3E2FFD0F3E2FFD0F3E2FFD0F3E2FFCAF1DEFFE2F7EDFFFFFF
          FFFF7ADDADFF3DCD87FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF51D293FF51D294FF47D08EFF44CF8BFF40CE89FF40CE
          89FF40CE89FF40CE89FF40CE89FF40CE89FF40CE89FF29C87BFF81DEB1FFFFFF
          FFFF7CDDAEFF3ECD88FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF50D293FF50D293FF4CD191FF4BD190FF4CD190FF4CD1
          91FF4AD08FFF48D08EFF4DD191FF4CD190FF4BD190FF35CB82FF8AE1B7FFFFFF
          FFFF7ADDADFF3ECD88FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF50D293FF51D294FF51D294FF50D293FF50D293FF50D2
          93FF53D395FF43CF8BFF46D08DFF52D394FF52D294FF3CCC87FF8BE1B7FFFFFF
          FFFF7BDDAEFF3FCD89FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF50D293FF51D394FF48D08EFF45CF8CFF45CF8CFF39CC
          85FF5AD599FFB2EBD0FF43CF8BFF3FCE89FF54D396FF3CCD87FF8CE1B7FFFFFF
          FFFF7BDDAEFF3FCE89FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF4FD292FF51D293FF59D599FF5AD599FF56D497FF53D2
          95FF77DCABFFFFFFFFFFCCF1DFFF41CE8AFF44CF8CFF3CCD87FF8DE2B9FFFFFF
          FFFF7BDDADFF3ECE88FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF45CF8CFF59D499FFF9FEFBFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD1F3E3FF45CF8CFF33CA82FF8FE2B9FFFFFF
          FFFF79DCACFF3ECE88FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF44CF8CFF5AD499FFFAFEFCFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD7F5E6FF45CF8CFF35CB82FF8CE2B7FFFFFF
          FFFF79DCACFF3FCE89FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF4FD293FF51D294FF5BD59AFF5AD599FF56D497FF53D2
          95FF76DCAAFFFFFFFFFFD2F3E3FF43CF8AFF44CF8BFF3CCD87FF8BE1B7FFFFFF
          FFFF79DDADFF3FCE89FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF51D293FF50D293FF46CF8CFF45CF8CFF45CF8CFF39CC
          85FF5BD599FFB9EDD3FF45CF8CFF3ECE88FF53D395FF3ACC86FF8CE1B7FFFFFF
          FFFF7ADDADFF3FCE89FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF50D293FF51D293FF50D293FF50D293FF50D293FF50D2
          93FF54D396FF44CF8BFF45CF8CFF53D395FF50D293FF3ACC85FF8DE1B8FFFFFF
          FFFF7ADDADFF3FCE89FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF50D293FF50D293FF4CD190FF4AD190FF4CD191FF4CD1
          91FF4AD08FFF47D08EFF4DD191FF4CD190FF4CD190FF36CB83FF8AE0B7FFFFFF
          FFFF7BDDAEFF3FCE89FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF51D293FF51D293FF4BD08FFF45CF8CFF40CE89FF40CE
          89FF40CE89FF40CE89FF40CE89FF40CE89FF40CE89FF2AC87CFF80DEB0FFFFFF
          FFFF7ADDADFF3ECE88FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF4CD090FF49D08EFFAEEBCDFFD7F5E6FFCFF3E2FFD0F3
          E2FFD0F3E2FFD0F3E2FFD0F3E2FFD0F3E2FFD0F3E2FFCBF2DFFFE0F7ECFFFFFF
          FFFF78DCABFF3ECE88FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF4AD090FF45D08CFFD5F5E5FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6FC
          F8FF72DAA8FF40CE89FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF50D293FF4FD293FF57D497FF59D498FF59D498FF59D4
          98FF59D498FF59D498FF59D498FF59D498FF59D498FF59D498FF59D498FF59D4
          98FF52D295FF4FD293FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF50D293FF51D293FF48D08EFF45CF8CFF45CF8CFF45CF
          8CFF45CF8CFF45CF8CFF45CF8CFF45CF8CFF45CF8CFF45CF8CFF45CF8CFF45CF
          8CFF4ED191FF51D294FF50D293FF50D293FF50D293FF4CC78CF250D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF48BD85E64CC7
          8CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC7
          8CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC7
          8CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF248BD85E6}
      end>
  end
end
