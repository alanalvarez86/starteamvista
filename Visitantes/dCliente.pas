unit DCliente;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBasicoCliente, Db, DBClient,
    {$ifdef DOS_CAPAS}
     DServerSistema,
     DServerVisReportes,

     //DServerCatalogos,}
     DServerReportes,
     DServerVisitantes,
     DServerGlobalVisitantes,
     {DServerConsultas,
     ,}
  {$endif}
  ZetaCommonClasses,
  ZetaClientDataSet;

type
  TdmCliente = class(TBasicoCliente)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  protected
     {$ifdef DOS_CAPAS}
     FServerGlobal: TdmServerGlobalVisitantes;
     FServerReportes : TdmServerReportes;

     FServerVisReportes: TdmServerVisReportes;
     FServerVisitantes: TdmServerVisitantes;
     FServerSistema: TdmServerSistema;
     //FServerCatalogos: TdmServerCatalogos;
     {FServerConsultas: TdmServerConsultas;}
    {$endif}
  private
    { Private declarations }
    FParamsCliente : TZetaParams;
    function GetHoraDefault: TTime;
  public
    { Public declarations }
    {$ifdef DOS_CAPAS}
    property ServerGlobal: TdmServerGlobalVisitantes read FServerGlobal;
    property ServerReportes: TdmServerReportes read FServerReportes;

    property ServerVisReportes: TdmServerVisReportes read FServerVisReportes;
    property ServerVisitantes: TdmServerVisitantes read FServerVisitantes;
    property ServerSistema: TdmServerSistema read FServerSistema;
    //property ServerCatalogos: TdmServerCatalogos read FServerCatalogos;
    {property ServerConsultas: TdmServerConsultas read FServerConsultas;}
{$endif}
    property HoraDefault: TTime read GetHoraDefault;

  end;

var
  dmCliente: TdmCliente;

implementation
uses ZetaCommonLists;

{$R *.DFM}



procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     inherited;
     TipoCompany := tcVisitas;
     ModoVisitantes := TRUE;
     FParamsCliente := TZetaParams.Create;
{$ifdef DOS_CAPAS}
     FServerGlobal := TdmServerGlobalVisitantes.Create( Self );
     FServerReportes := TdmServerReportes.Create( self );
     FServerVisReportes := TdmServerVisReportes.Create( self );
     FServerVisitantes := TdmServerVisitantes.Create( self );
     FServerSistema := TdmServerSistema.Create( Self );
     ////FServerCatalogos := TdmServerCatalogos.Create( Self );
     //FServerConsultas := TdmServerConsultas.Create( self );
{$endif}
end;


procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
{$ifdef DOS_CAPAS}
     //FreeAndNil( FServerConsultas );
     //FreeAndNil( FServerCatalogos );
     FreeAndNil( FServerSistema );
     FreeAndNil( FServerVisitantes );
     FreeAndNil( FServerVisReportes );
     FreeAndNil( FServerReportes );
     FreeAndNil( FServerGlobal );
{$endif}
     FParamsCliente.Free;
     inherited;
end;

function TdmCliente.GetHoraDefault: tTime;
begin
     Result := Time;
end;

end.
