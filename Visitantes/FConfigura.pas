unit FConfigura;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, StdCtrls, Buttons, ExtCtrls, ZetaKeyCombo, Mask,
  ZetaNumero, ZBaseDlgModal, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, dxSkinsCore, TressMorado2013, Vcl.ImgList, cxButtons, cxControls,
  cxContainer, cxEdit, cxCheckBox;

type
  TConfigura = class(TZetaDlgModal_DevEx)
    Label1: TLabel;
    cbImpresionGafete: TZetaKeyCombo;
    Label2: TLabel;
    cbImpresora: TZetaKeyCombo;
    GBMostrarCitas: TGroupBox;
    cbFiltrarTiempo: TcxCheckBox;
    GBFiltroTiempo: TGroupBox;
    lbMinAntes: TLabel;
    lbMinDespues: TLabel;
    cbMostrarCitas: TcxCheckBox;
    edMinAntes: TZetaNumero;
    edMinDespues: TZetaNumero;
    chRequiereGridAvanzado: TcxCheckBox;
    chRegistroGrupo: TcxCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure cbMostrarCitasClick(Sender: TObject);
    procedure cbFiltrarTiempoClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    procedure LLenaListas(const sImpresora: string);
    procedure SetControlsConfCitas;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Configura: TConfigura;

implementation
uses Printers,
     DVisitantes,
     ZetaCommonLists,
     ZetaDialogo,
     ZetaCommonClasses,
     DGlobal, FTressShell;

{$R *.DFM}


procedure TConfigura.FormCreate(Sender: TObject);
begin
     inherited;
     with dmVisitantes do
     begin
          cbImpresionGafete.ItemIndex := Ord( ImpresionGafete );
          cbMostrarCitas.Checked := MostrarGridCitas;
          cbFiltrarTiempo.Checked := FiltrarTiempo;
          chRequiereGridAvanzado.Checked := MostrarGridAvanzado;
          chRegistroGrupo.Checked := MostrarRegistroGrupo;
          edMinAntes.Valor:= MinutosAntes;
          edMinDespues.Valor:= MinutosDespues;
          LLenaListas( ImpresoraDefault );
          HelpContext := H_VIS_CASETA_CONFIGURA;
     end;
     SetControlsConfCitas;
end;

procedure TConfigura.OKClick(Sender: TObject);
begin
     inherited;
     with dmVisitantes do
     begin
          ImpresionGafete := eImpresionGafete( cbImpresionGafete.ItemIndex );
          MostrarGridCitas := cbMostrarCitas.Checked;
          ImpresoraDefault := CBImpresora.Items[CBImpresora.ItemIndex];
          FiltrarTiempo :=  cbFiltrarTiempo.Checked;
          if ( cbFiltrarTiempo.Checked )  then
          begin
               if ( ( edMinAntes.ValorEntero >= 0 ) or ( edMinDespues.ValorEntero >= 0 ) ) then
               begin
                    MinutosAntes := edMinAntes.ValorEntero;
                    MinutosDespues := edMinDespues.ValorEntero;
               end
               else
                   ZetaDialogo.ZError( '� Atenci�n !', 'El valor de los minutos debe ser mayor a 0', 0 );
          end;
     end;
end;

procedure TConfigura.OK_DevExClick(Sender: TObject);
begin
   inherited;
     with dmVisitantes do
     begin
          ImpresionGafete := eImpresionGafete( cbImpresionGafete.ItemIndex );
          MostrarGridCitas := cbMostrarCitas.Checked;
          MostrarGridAvanzado := chRequiereGridAvanzado.Checked;
          MostrarRegistroGrupo := chRegistroGrupo.Checked;
          ImpresoraDefault := CBImpresora.Items[CBImpresora.ItemIndex];
          FiltrarTiempo :=  cbFiltrarTiempo.Checked;
          if ( cbFiltrarTiempo.Checked )  then
          begin
               if ( ( edMinAntes.ValorEntero >= 0 ) or ( edMinDespues.ValorEntero >= 0 ) ) then
               begin
                    MinutosAntes := edMinAntes.ValorEntero;
                    MinutosDespues := edMinDespues.ValorEntero;
               end
               else
                   ZetaDialogo.ZError( '� Atenci�n !', 'El valor de los minutos debe ser mayor a 0', 0 );
          end;
          TressShell.ActualizaConfiguracionVisitantes;
          TressShell.RefrescaShell;
     end;

end;

procedure TConfigura.LLenaListas( const sImpresora : string );
 var
    fPrinter : TPrinter;
    i: integer;
begin
     cbImpresora.Items.Clear;

     fPrinter := TPrinter.Create;

     with fPrinter do
     begin
          if Printers.Count > 0 then
          begin
               with CBImpresora do
               begin
                    Items.AddStrings( Printers );
                    i := Items.IndexOf( sImpresora );
                    if (i < 0 ) then
                       ItemIndex := PrinterIndex
                    else
                        ItemIndex := i;
               end;
          end;
     end;
end;


procedure TConfigura.cbMostrarCitasClick(Sender: TObject);
begin
     inherited;
     SetControlsConfCitas;
end;

procedure TConfigura.SetControlsConfCitas;
begin
     cbFiltrarTiempo.Enabled:= cbMostrarCitas.Checked;
     edMinAntes.Enabled:=  cbFiltrarTiempo.Enabled and cbFiltrarTiempo.Checked;
     lbMinAntes.Enabled:= edMinAntes.Enabled;

     edMinDespues.Enabled:= edMinAntes.Enabled;
     lbMinDespues.Enabled:= edMinAntes.Enabled;
end;

procedure TConfigura.cbFiltrarTiempoClick(Sender: TObject);
begin
     inherited;
     SetControlsConfCitas;
end;

end.
