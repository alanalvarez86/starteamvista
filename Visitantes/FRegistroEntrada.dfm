inherited RegistroEntrada: TRegistroEntrada
  Left = 391
  Top = 253
  Width = 395
  Height = 382
  Caption = 'Registro de Entrada'
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    Top = 319
    Width = 387
    AlEjecutar = WizardAlEjecutar
    AlCancelar = WizardAlCancelar
    BeforeMove = WizardBeforeMove
    inherited Salir: TZetaWizardButton
      Left = 299
      Kind = bkCancel
    end
    inherited Ejecutar: TZetaWizardButton
      Left = 216
      Caption = '&Registrar'
      ModalResult = 1
    end
  end
  inherited PageControl: TPageControl
    Width = 387
    Height = 319
    ActivePage = tsVisitantes
    object tsVisitantes: TTabSheet
      Caption = 'tsVisitantes'
      TabVisible = False
      object lblVisitante: TLabel
        Left = 29
        Top = 61
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Visitante:'
      end
      object btnVisitantes: TSpeedButton
        Left = 351
        Top = 57
        Width = 21
        Height = 21
        Hint = 'Buscar Visitante'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33033333333333333F7F3333333333333000333333333333F777333333333333
          000333333333333F777333333333333000333333333333F77733333333333300
          033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
          33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
          3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
          33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
          333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
          333333773FF77333333333370007333333333333777333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = btnVisitantesClick
      end
      object btnEmpresa: TSpeedButton
        Left = 351
        Top = 82
        Width = 21
        Height = 21
        Hint = 'Buscar Visitante'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33033333333333333F7F3333333333333000333333333333F777333333333333
          000333333333333F777333333333333000333333333333F77733333333333300
          033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
          33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
          3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
          33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
          333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
          333333773FF77333333333370007333333333333777333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = btnEmpresaClick
      end
      object lblLicId: TLabel
        Left = 42
        Top = 210
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = '#  ID.:'
      end
      object lblTipoId: TLabel
        Left = 6
        Top = 185
        Width = 66
        Height = 13
        Alignment = taRightJustify
        Caption = 'Identificaci�n:'
      end
      object lblAsunto: TLabel
        Left = 36
        Top = 111
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Asunto:'
      end
      object lblEmpresa: TLabel
        Left = 20
        Top = 86
        Width = 52
        Height = 13
        Alignment = taRightJustify
        Caption = 'Compa��a:'
      end
      object lblAnfitrion: TLabel
        Left = 16
        Top = 135
        Width = 56
        Height = 13
        Alignment = taRightJustify
        Caption = 'Visitando A:'
      end
      object btnAnfitrion: TSpeedButton
        Left = 351
        Top = 131
        Width = 21
        Height = 21
        Hint = 'Buscar Anfitri�n'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33033333333333333F7F3333333333333000333333333333F777333333333333
          000333333333333F777333333333333000333333333333F77733333333333300
          033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
          33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
          3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
          33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
          333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
          333333773FF77333333333370007333333333333777333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = btnAnfitrionClick
      end
      object btnDepto: TSpeedButton
        Left = 351
        Top = 156
        Width = 21
        Height = 21
        Hint = 'Buscar Visitante'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33033333333333333F7F3333333333333000333333333333F777333333333333
          000333333333333F777333333333333000333333333333F77733333333333300
          033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
          33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
          3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
          33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
          333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
          333333773FF77333333333370007333333333333777333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = btnDeptoClick
      end
      object lblDepto: TLabel
        Left = 2
        Top = 160
        Width = 70
        Height = 13
        Alignment = taRightJustify
        Caption = 'Departamento:'
      end
      object Label1: TLabel
        Left = 27
        Top = 234
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = '# Gafete:'
      end
      object LI_NOMBRE: TDBEdit
        Left = 74
        Top = 57
        Width = 275
        Height = 21
        DataField = 'LI_NOMBRE'
        DataSource = DataSource
        TabOrder = 0
      end
      object LI_EMPRESA: TDBEdit
        Left = 74
        Top = 82
        Width = 275
        Height = 21
        DataField = 'LI_EMPRESA'
        DataSource = DataSource
        TabOrder = 1
      end
      object LI_ASUNTO: TZetaDBKeyLookup
        Left = 74
        Top = 107
        Width = 300
        Height = 21
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        DataField = 'LI_ASUNTO'
        DataSource = DataSource
      end
      object LI_ID: TDBEdit
        Left = 74
        Top = 206
        Width = 275
        Height = 21
        DataField = 'LI_ID'
        DataSource = DataSource
        TabOrder = 6
      end
      object LI_TIPO_ID: TZetaDBKeyLookup
        Left = 74
        Top = 181
        Width = 300
        Height = 21
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        DataField = 'LI_TIPO_ID'
        DataSource = DataSource
      end
      object LI_ANFITR: TDBEdit
        Left = 74
        Top = 131
        Width = 275
        Height = 21
        DataField = 'LI_ANFITR'
        DataSource = DataSource
        TabOrder = 3
      end
      object LI_DEPTO: TDBEdit
        Left = 74
        Top = 155
        Width = 274
        Height = 21
        DataField = 'LI_DEPTO'
        DataSource = DataSource
        TabOrder = 4
      end
      object LI_GAFETE: TDBEdit
        Left = 74
        Top = 230
        Width = 275
        Height = 21
        DataField = 'LI_GAFETE'
        DataSource = DataSource
        TabOrder = 7
      end
    end
    object tsVehiculo: TTabSheet
      Caption = 'tsVehiculo'
      ImageIndex = 1
      TabVisible = False
      object gbCarro: TGroupBox
        Left = 0
        Top = 0
        Width = 379
        Height = 105
        Align = alTop
        Caption = ' Veh�culo '
        TabOrder = 0
        object lblCajon: TLabel
          Left = 41
          Top = 80
          Width = 30
          Height = 13
          Alignment = taRightJustify
          Caption = 'Caj�n:'
        end
        object lblDescrip: TLabel
          Left = 12
          Top = 59
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = 'Descripci�n:'
        end
        object lblPlacas: TLabel
          Left = 36
          Top = 37
          Width = 35
          Height = 13
          Alignment = taRightJustify
          Caption = 'Placas:'
        end
        object lblCarro: TLabel
          Left = 25
          Top = 16
          Width = 46
          Height = 13
          Alignment = taRightJustify
          Caption = 'Veh�culo:'
        end
        object LI_CAR_EST: TDBEdit
          Left = 74
          Top = 76
          Width = 150
          Height = 21
          DataField = 'LI_CAR_EST'
          DataSource = DataSource
          TabOrder = 3
        end
        object LI_CAR_DES: TDBEdit
          Left = 74
          Top = 55
          Width = 275
          Height = 21
          DataField = 'LI_CAR_DES'
          DataSource = DataSource
          TabOrder = 2
        end
        object LI_CAR_PLA: TDBEdit
          Left = 74
          Top = 33
          Width = 150
          Height = 21
          DataField = 'LI_CAR_PLA'
          DataSource = DataSource
          TabOrder = 1
        end
        object LI_CAR_TIP: TZetaDBKeyLookup
          Left = 74
          Top = 12
          Width = 300
          Height = 21
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
          DataField = 'LI_CAR_TIP'
          DataSource = DataSource
        end
      end
      object gbOtros: TGroupBox
        Left = 0
        Top = 105
        Width = 379
        Height = 89
        Align = alTop
        Caption = ' Detalles '
        TabOrder = 1
        object lblTexto1: TLabel
          Left = 27
          Top = 19
          Width = 46
          Height = 13
          Alignment = taRightJustify
          Caption = 'Texto #1:'
        end
        object lblTexto3: TLabel
          Left = 27
          Top = 65
          Width = 46
          Height = 13
          Alignment = taRightJustify
          Caption = 'Texto #3:'
        end
        object lblTexto2: TLabel
          Left = 27
          Top = 42
          Width = 46
          Height = 13
          Alignment = taRightJustify
          Caption = 'Texto #2:'
        end
        object LI_TEXTO1: TDBEdit
          Left = 77
          Top = 15
          Width = 276
          Height = 21
          DataField = 'LI_TEXTO1'
          DataSource = DataSource
          TabOrder = 0
        end
        object LI_TEXTO2: TDBEdit
          Left = 77
          Top = 38
          Width = 276
          Height = 21
          DataField = 'LI_TEXTO2'
          DataSource = DataSource
          TabOrder = 1
        end
        object LI_TEXTO3: TDBEdit
          Left = 77
          Top = 61
          Width = 276
          Height = 21
          DataField = 'LI_TEXTO3'
          DataSource = DataSource
          TabOrder = 2
        end
      end
      object gbObserva: TGroupBox
        Left = 0
        Top = 194
        Width = 379
        Height = 115
        Align = alClient
        Caption = ' Observaciones '
        TabOrder = 2
        object LI_OBSERVA: TDBMemo
          Left = 2
          Top = 15
          Width = 375
          Height = 98
          Align = alClient
          DataField = 'LI_OBSERVA'
          DataSource = DataSource
          ScrollBars = ssVertical
          TabOrder = 0
        end
      end
    end
  end
  object DataSource: TDataSource
    Left = 340
    Top = 8
  end
end
