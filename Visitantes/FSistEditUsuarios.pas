unit FSistEditUsuarios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FSistBaseEditUsuarios, Db, ZetaDBTextBox, StdCtrls, ZetaKeyCombo,
  ZetaKeyLookup, DBCtrls, Mask, ZetaNumero, ComCtrls, ExtCtrls, Buttons;

type
  TSistEditUsuarios = class(TSistBaseEditUsuarios)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SistEditUsuarios: TSistEditUsuarios;

implementation
uses
    ZetaCommonClasses;
{$R *.DFM}

procedure TSistEditUsuarios.FormCreate(Sender: TObject);
begin
     inherited;
     Operacion.TabVisible := FALSE;
end;

end.
