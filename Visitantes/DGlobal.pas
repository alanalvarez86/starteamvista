unit DGlobal;

interface

uses Forms, Controls, DB, Classes, StdCtrls, SysUtils,
{$ifdef DOS_CAPAS}
     DServerGlobalVisitantes,
{$else}
     GlobalVisitantes_TLB,
{$endif}
     dBaseGlobal;

type
  TdmGlobal = class( TdmBaseGlobal )
  private
  FListaUsados: TStrings;
{$ifdef DOS_CAPAS}
    function GetServer: TdmServerGlobalVisitantes;
    property Server: TdmServerGlobalVisitantes read GetServer;
{$else}
    FServidor: IdmServerGlobalVisitantesDisp;
    function GetServer: IdmServerGlobalVisitantesDisp;
    procedure SetRequiereGridAvanzado(const Value: boolean);
    property Server: IdmServerGlobalVisitantesDisp read GetServer;

{$endif}
  protected
    function GetGlobales: Variant; override;
    procedure GrabaGlobales( const aGlobalServer: Variant; const lActualizaDiccion: Boolean; var ErrorCount: Integer ); override;
  public
    { Public declarations }
    property ListaUsados: TStrings read FListaUsados;
    destructor Destroy; override;
    function NombreRefLab( const Index: Integer ): String;
    function NumCamposRefLab: Integer;
    procedure InitListaUsados;
    property RequiereGridAvanzado: boolean write SetRequiereGridAvanzado;
  end;

var
   Global: TdmGlobal;

implementation
uses dCliente, ZGlobalTress;


{ TdmGlobal }

{$ifdef DOS_CAPAS}
function TdmGlobal.GetServer: TdmServerGlobalVisitantes;
begin
     Result := dmCliente.ServerGlobal;
end;
{$else}
function TdmGlobal.GetServer: IdmServerGlobalVisitantesDisp;
begin
     Result := IdmServerGlobalVisitantesDisp( dmCliente.CreaServidor( CLASS_dmServerGlobalVisitantes, FServidor ) );
end;
{$endif}

{ER: Si se requiere declarar GetGlobales y GrabaGlobales, porque no se
puede declarar el SERVER en la clase base}
function TdmGlobal.GetGlobales: Variant;
begin
     Result := Server.GetGlobales( dmCliente.Empresa );
end;


procedure TdmGlobal.GrabaGlobales(const aGlobalServer: Variant; const lActualizaDiccion: Boolean;
          var ErrorCount: Integer);
begin
     Server.GrabaGlobales( dmCliente.Empresa, aGlobalServer, lActualizaDiccion, ErrorCount );
end;

destructor TdmGlobal.Destroy;
begin
     FreeAndNil( FListaUsados );
     inherited Destroy;
end;

procedure TdmGlobal.InitListaUsados;
begin
     if ( FListaUsados = nil ) then
        FListaUsados := TStringList.Create;
     //FListaUsados.CommaText := GetGlobalString( ZGlobalTress.K_GLOBAL_POSICION_ADIC );
end;

function TdmGlobal.NombreRefLab( const Index: Integer ): String;
begin
     //Result := Trim( GetGlobalString( K_GLOBAL_REF_LABORAL_BASE + Index ) );
     Result := '';
end;

function TdmGlobal.NumCamposRefLab: Integer;
begin
     Result := 0;
     // Cuenta los Campos NO vacios. En cuanto encuentra uno vac�o, deja de contar
     {while ( Result < K_GLOBAL_REF_LABORAL_MAX ) and ( Length( NombreRefLab( Result+1 )) > 0 ) do
           Inc( Result );}
end;

procedure TdmGlobal.SetRequiereGridAvanzado(const Value: boolean);
begin
     RequiereGridAvanzado:=Value;
end;

end.


