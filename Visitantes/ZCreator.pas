unit ZCreator;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseCreator{, DQueries};

type
  TRastreador = integer;

  TZetaCreator = class(TZetaBaseCreator)
  private
    { Private declarations }
    FRastreando : Boolean;
    FRastreandoNomina: Boolean;
  public
    { Public declarations }
    Rastreador: TRastreador;
    RastreadorNomina: TRastreador;
    {FQueries: TCommonQueries;
    Ritmos: TRitmos;}
    procedure RegistraFunciones( Funciones: TipoFunciones );override;
    property Rastreando: Boolean read FRastreando;
    property RastreandoNomina: Boolean read FRastreandoNomina;
  end;

var
  ZetaCreator: TZetaCreator;

implementation
uses ZetaCommonLists,
     ZFuncsGenerales,
     ZFuncsVisitantes,
     ZFuncsReporte;
     
{$R *.DFM}

procedure TZetaCreator.RegistraFunciones( Funciones: TipoFunciones );
begin
     inherited;
     if (efComunes in Funciones) then
     begin
        ZFuncsGenerales.RegistraFunciones( FunctionLibrary );
        ZFuncsVisitantes.RegistraFunciones( FunctionLibrary );
     end;
     if efReporte in Funciones then
        ZFuncsReporte.RegistraFunciones( FunctionLibrary );
end;

{procedure TZetaCreator.PreparaQueries;
begin
     if not Assigned( FQueries ) then
        FQueries := TCommonQueries.Create( oZetaProvider );
end;


procedure TZetaCreator.DesPreparaQueries;
begin
     FreeAndNil( FQueries );
end;}

end.
