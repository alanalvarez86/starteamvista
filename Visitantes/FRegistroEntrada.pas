unit FRegistroEntrada;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZWizardBasico, Buttons, StdCtrls, DBCtrls, ZetaKeyLookup, Mask, ComCtrls,
  ZetaWizard, ExtCtrls, Db,
  ZetaClientDataset;

type
  TRegistroEntrada = class(TWizardBasico)
    tsVisitantes: TTabSheet;
    tsVehiculo: TTabSheet;
    gbCarro: TGroupBox;
    lblCajon: TLabel;
    lblDescrip: TLabel;
    lblPlacas: TLabel;
    lblCarro: TLabel;
    LI_CAR_EST: TDBEdit;
    LI_CAR_DES: TDBEdit;
    LI_CAR_PLA: TDBEdit;
    LI_CAR_TIP: TZetaDBKeyLookup;
    gbOtros: TGroupBox;
    lblTexto1: TLabel;
    lblTexto3: TLabel;
    lblTexto2: TLabel;
    LI_TEXTO1: TDBEdit;
    LI_TEXTO2: TDBEdit;
    LI_TEXTO3: TDBEdit;
    gbObserva: TGroupBox;
    LI_OBSERVA: TDBMemo;
    lblVisitante: TLabel;
    LI_NOMBRE: TDBEdit;
    btnVisitantes: TSpeedButton;
    btnEmpresa: TSpeedButton;
    LI_EMPRESA: TDBEdit;
    LI_ASUNTO: TZetaDBKeyLookup;
    LI_ID: TDBEdit;
    LI_TIPO_ID: TZetaDBKeyLookup;
    lblLicId: TLabel;
    lblTipoId: TLabel;
    lblAsunto: TLabel;
    lblEmpresa: TLabel;
    lblAnfitrion: TLabel;
    LI_ANFITR: TDBEdit;
    btnAnfitrion: TSpeedButton;
    btnDepto: TSpeedButton;
    LI_DEPTO: TDBEdit;
    lblDepto: TLabel;
    DataSource: TDataSource;
    Label1: TLabel;
    LI_GAFETE: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnVisitantesClick(Sender: TObject);
    procedure btnEmpresaClick(Sender: TObject);
    procedure btnAnfitrionClick(Sender: TObject);
    procedure btnDeptoClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
  private
    function CancelarWizard: Boolean;
    procedure Connect;
    procedure Disconnect;
    procedure DoConnect;
    procedure DoDisconnect;
    function EjecutarWizard: Boolean;
    procedure TerminarWizard;
    procedure ChecaGlobales;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RegistroEntrada: TRegistroEntrada;

implementation
uses
    FBuscaAnfitriones,
    FBuscaVisitantes,
    FTressShell,
    ZetaDialogo,
    ZetaCommonTools,
    ZetaCommonClasses,
    ZGlobalTress,
    DGlobal,
    DVisitantes;

{$R *.DFM}

procedure TRegistroEntrada.FormCreate(Sender: TObject);
begin
     inherited;
     //IndexDerechos := ZAccesosTress.D_REG_LIBRO;
     with dmVisitantes do
     begin
          LI_ASUNTO.LookupDataset := cdsTipoAsunto;
          LI_TIPO_ID.LookupDataset := cdsTipoID;
          LI_CAR_TIP.LookupDataset := cdsTipoCarro;
     end;
end;

procedure TRegistroEntrada.FormShow(Sender: TObject);
begin
     inherited;
     DoConnect;
end;

procedure TRegistroEntrada.Connect;
begin
     with dmVisitantes do
     begin
          cdsTipoID.Conectar;
          cdsTipoAsunto.Conectar;
          cdsTipoCarro.Conectar;
          cdsCaseta.Conectar;
          cdsEmpVisitante.Conectar;
          cdsDepto.Conectar;
          DataSource.DataSet:= cdsLibros;
     end;
     PageControl.ActivePage := tsVisitantes;
     LI_NOMBRE.SetFocus;
     ChecaGlobales;
end;

procedure TRegistroEntrada.Disconnect;
begin
     Datasource.Dataset := nil;
end;

procedure TRegistroEntrada.DoConnect;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             try
                Connect;
             except
                   on Error: Exception do
                      zExcepcion( 'ˇ Error Al Conectar Forma !', 'ˇ Ventana No Pudo Ser Conectada A Sus Datos !', Error, 0 );
             end;
          finally
                 Cursor := oCursor;
          end;
     end;

end;

procedure TRegistroEntrada.DoDisconnect;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Disconnect;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

procedure TRegistroEntrada.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        Application.ProcessMessages;
        try
           lOk := EjecutarWizard;
        except
              on Error: Exception do
              begin
                   zExcepcion( Caption, 'Error Al Ejecutar Wizard', Error, 0 );
                   lOk := False;
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     if lOk then
        TerminarWizard;
end;

function TRegistroEntrada.EjecutarWizard: Boolean;
var
   oCursor: TCursor;
   iRecords : integer;
begin
     with TZetaClientDataSet(DataSource.DataSet) do
     begin
          with Screen do
          begin
               oCursor := Cursor;
               Cursor := crHourglass;
               try
                  Result := TRUE;
                  try
                     iRecords := RecordCount;

                     Enviar;

                     Refrescar;

                     if ( iRecords <> RecordCount ) then Last;

                     TressShell.ImprimeForma(TRUE);
                  except
                        Result := FALSE;
                        Raise;
                  end;
               finally
                      Cursor := oCursor;
               end;
          end;
     end;
end;

function TRegistroEntrada.CancelarWizard: Boolean;
begin
     Close;
     Result := True;
end;

procedure TRegistroEntrada.TerminarWizard;
begin
     if not Wizard.Reejecutar then
     begin
          Close;
          Windows.SetForegroundWindow( Handle ); { Evita que la aplicación pierda el Focus }
     end;
end;

procedure TRegistroEntrada.WizardAlCancelar(Sender: TObject; var lOk: Boolean);
begin
     inherited;
     lOk := CancelarWizard;
end;

procedure TRegistroEntrada.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     inherited;
     DoDisconnect;
     Action := caHide;

end;

procedure TRegistroEntrada.btnVisitantesClick(Sender: TObject);
var
   iNumero, iEmpresa, iAnfitrion: Integer;
   sDescription, sAsunto, sAnfitrion, sDepto: String;
begin
     if FBuscaVisitantes.BuscaVisitanteDialogo( iNumero, sDescription ) then
     begin
          with dmVisitantes.cdsLibros do
          begin
               //it;
               FieldByName('LI_NOMBRE').AsString := sDescription;
               FieldByName('VI_NUMERO').AsInteger := iNumero;
          end;
     end;
     with dmVisitantes do
     begin
          if( iNumero <> 0 )then
          begin
               cdsVisitante.Conectar;
               if cdsVisitante.Locate( 'VI_NUMERO', iNumero, [] ) then
               begin
                    iEmpresa := cdsVisitante.FieldByName('EV_NUMERO').AsInteger;
                    sAsunto := cdsVisitante.FieldByName('VI_ASUNTO').AsString;
                    iAnfitrion := cdsVisitante.FieldByName('AN_NUMERO').AsInteger;
                    if cdsEmpVisitante.Locate( 'EV_NUMERO', iEmpresa, [] ) then
                    begin
                         cdsLibros.FieldByName('EV_NUMERO').AsInteger := iEmpresa;
                         cdsLibros.FieldByName('LI_EMPRESA').AsString := cdsEmpVisitante.FieldByName('EV_NOMBRE').AsString;
                    end;
                    if cdsTipoAsunto.Locate( 'TB_CODIGO', sAsunto, [] ) then
                         cdsLibros.FieldByName('LI_ASUNTO').AsString := cdsTipoAsunto.FieldByName('TB_CODIGO').AsString;
                    cdsAnfitrion.Conectar;
                    if( cdsAnfitrion.Locate( 'AN_NUMERO', iAnfitrion, [] ) )then
                    begin
                         cdsLibros.FieldByName('AN_NUMERO').AsInteger := iAnfitrion;
                         sAnfitrion := Trim( cdsAnfitrion.FieldByName( 'AN_APE_PAT' ).AsString ) + ' ' +
                                         Trim( cdsAnfitrion.FieldByName( 'AN_APE_MAT' ).AsString ) + ', ' +
                                         Trim( cdsAnfitrion.FieldByName( 'AN_NOMBRES' ).AsString ) ;
                         cdsLibros.FieldByName('LI_ANFITR').AsString := sAnfitrion;
                         sDepto := cdsAnfitrion.FieldByName('AN_DEPTO').AsString;
                         if strLleno( sDepto )then
                         begin
                              if( cdsDepto.Locate( 'TB_CODIGO', sDepto, [] ) )then
                              begin
                                   cdsLibros.FieldByName('LI_CDEPTO').AsString := sDepto;
                                   cdsLibros.FieldByName('LI_DEPTO').AsString := cdsDepto.FieldByName('TB_ELEMENT').AsString;
                              end;
                         end;
                    end
                    else
                    begin
                         with cdsLibros do
                         begin
                              FieldByName('AN_NUMERO').AsInteger := 0;
                              FieldByName('LI_ANFITR').AsString := VACIO;
                              FieldByName('LI_CDEPTO').AsString := VACIO;
                              FieldByName('LI_DEPTO').AsString := VACIO;
                         end;
                    end;
               end;
          end;
     end;
end;

procedure TRegistroEntrada.btnEmpresaClick(Sender: TObject);
var
   sKey, sDescription: String;
begin
     inherited;
     if dmVisitantes.cdsEmpVisitante.Search( VACIO, sKey, sDescription ) then
     begin
          with dmVisitantes.cdsLibros do
          begin
               Edit;
               FieldByName('LI_EMPRESA').AsString := sDescription;
               FieldByName('EV_NUMERO').AsInteger := StrtoInt( sKey );
          end;
     end;
end;

procedure TRegistroEntrada.btnAnfitrionClick(Sender: TObject);
var
   iNumero: Integer;
   sDescription, sDepto: String;
begin
     inherited;
     if FBuscaAnfitriones.BuscaAnfitrionDialogo( iNumero, sDescription ) then
     begin
          with dmVisitantes do
          begin
               with cdsLibros do
               begin
                    Edit;
                    FieldByName('LI_ANFITR').AsString := sDescription;
                    FieldByName('AN_NUMERO').AsInteger := iNumero;


                    sDepto := cdsAnfitrionLookup.FieldByName('AN_DEPTO').AsString;
                    if strLleno( sDepto )then
                    begin
                         if( cdsDepto.Locate( 'TB_CODIGO', sDepto, [] ) )then
                         begin
                              cdsLibros.FieldByName('LI_CDEPTO').AsString := sDepto;
                              cdsLibros.FieldByName('LI_DEPTO').AsString := cdsDepto.FieldByName('TB_ELEMENT').AsString;
                         end;
                    end;
               end;
          end;
     end;
end;

procedure TRegistroEntrada.btnDeptoClick(Sender: TObject);
var
   sKey, sDescription: String;
begin
     inherited;
     if dmVisitantes.cdsDepto.Search( VACIO, sKey, sDescription ) then
     begin
          with dmVisitantes.cdsLibros do
          begin
               Edit;
               FieldByName('LI_DEPTO').AsString := sDescription;
               FieldByName('LI_CDEPTO').AsString := sKey;
          end;
     end;
end;

procedure TRegistroEntrada.ChecaGlobales;
begin
     LI_ANFITR.ReadOnly := not Global.GetGlobalBooleano( K_GLOBAL_ABIERTA_ANFI );
     LI_NOMBRE.ReadOnly := not Global.GetGlobalBooleano( K_GLOBAL_ABIERTA_VISI );
     LI_EMPRESA.ReadOnly := not Global.GetGlobalBooleano( K_GLOBAL_ABIERTA_COMPANY );
     LI_DEPTO.ReadOnly := not Global.GetGlobalBooleano( K_GLOBAL_ABIERTA_DEPTO );
end;

procedure TRegistroEntrada.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if CanMove AND Wizard.Adelante then
     begin
          dmVisitantes.ValidaCamposLibros( DataSource.DataSet, Wizard.EsPaginaActual( tsVehiculo ) );
     end;
end;

end.



