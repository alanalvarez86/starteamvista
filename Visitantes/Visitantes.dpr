program Visitantes;

uses
  Forms,
  ZetaClientTools,
  MidasLib,
  FTressShell in 'FTressShell.pas' {TressShell},
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FBuscaAnfitriones in '..\VisitantesMGR\FBuscaAnfitriones.pas',
  FBaseBusquedas in '..\VisitantesMGR\FBaseBusquedas.pas' {BaseBusquedas},
  FBuscaVisitantes in '..\VisitantesMGR\FBuscaVisitantes.pas' {BuscaVisitantes},
  FEditCortes in '..\VisitantesMGR\FEditCortes.pas',
  ZAccesosTress in '..\VisitantesMGR\ZAccesosTress.pas',
  ZBaseEdicion in '..\tools\ZBaseEdicion.pas' {BaseEdicion},
  ZGlobalTress in '..\VisitantesMGR\ZGlobalTress.pas',
  ZetaTipoEntidad in '..\VisitantesMGR\ZetaTipoEntidad.pas',
  DEntidadesTress in '..\VisitantesMGR\DEntidadesTress.pas' {dmEntidadesTress: TDataModule},
  DReportes in '..\VisitantesMGR\DReportes.pas' {dmReportes: TDataModule},
  FEditLibros in 'FEditLibros.pas' {EditLibros},
  EditorDResumen in '..\Medicos\EditorDResumen.pas',
  FMostrarFotografia in 'FMostrarFotografia.pas' {MostrarFotografia},
  FRegistroEntradaVisi in 'FRegistroEntradaVisi.pas' {FRegistroEntradaVisit},
  FBusquedaVisit in 'FBusquedaVisit.pas' {FBusquedaVisitante},
  FTableroTotales in 'FTableroTotales.pas' {FTableroTots};

// FToolsVideo in '..\Tools\FToolsVideo.pas';
{$R *.RES}
//{$R WindowsXP.res}

{$R ..\..\Traducciones\Spanish.RES}
begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;

  Application.Title := 'Visitantes';
  Application.HelpFile := 'Visitantes.chm';
  Application.CreateForm(TTressShell, TressShell);
  Application.CreateForm(TFTableroTots, FTableroTots);
  with TressShell do
  begin
       if Login( False ) then
       begin

            Show;
            Update;
            BeforeRun;
            Application.Run;
       end
       else
       begin

            Free;
       end;
  end;
end.

