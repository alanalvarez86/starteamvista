unit FAlerta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaDBTextBox, StdCtrls, Buttons, ExtCtrls, ZBaseDlgModal, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore,
  TressMorado2013, cxButtons, Vcl.ImgList, dxGDIPlusClasses;

type
  TAlerta = class(TForm)
    Label1: TLabel;
    eVisitante: TZetaTextBox;
    Label2: TLabel;
    eEmpresa: TZetaTextBox;
    Label3: TLabel;
    eAnfitrion: TZetaTextBox;
    Label4: TLabel;
    eDepartamento: TZetaTextBox;
    Label5: TLabel;
    eTelefono: TZetaTextBox;
    Label6: TLabel;
    eEntrada: TZetaTextBox;
    cxImageList24_PanelBotones: TcxImageList;
    Image1: TImage;
    PanelBotones: TPanel;
    Cancelar_DevEx: TcxButton;
    OK_DevEx: TcxButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure bRegistraSalidaClick(Sender: TObject);
    procedure bIgnorarClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
  private
    FEmpresa: String;
    FTelefono: String;
    FDepartamento: String;
    FAnfitrion: String;
    FEntrada: String;
    FVisitante: String;
    FFolio: integer;
    procedure SetAnfitrion(const Value: String);
    procedure SetDepartamento(const Value: String);
    procedure SetEmpresa(const Value: String);
    procedure SetEntrada(const Value: String);
    procedure SetTelefono(const Value: String);
    procedure SetVisitante(const Value: String);
    { Private declarations }
  public
    { Public declarations }
    property Folio: integer read FFolio write FFolio;
    property Visitante: String read FVisitante write SetVisitante;
    property Empresa: String read FEmpresa write SetEmpresa;
    property Anfitrion: String read FAnfitrion write SetAnfitrion;
    property Departamento: String read FDepartamento write SetDepartamento;
    property Telefono: String read FTelefono write SetTelefono;
    property Entrada: String read FEntrada write SetEntrada;
  end;

var
  Alerta: TAlerta;

implementation
uses
    DVisitantes,
    ZetaCommonClasses,
    ZetaCommonTools;
{$R *.DFM}

{ TAlerta }



{ TAlerta }
procedure TAlerta.FormCreate(Sender: TObject);
 function GetStyle(const i: integer) : TBrushStyle;
 begin
      case i of
           0:Result := bsHorizontal;
           1:Result := bsVertical;
           2:Result := bsFDiagonal;
           3:Result := bsBDiagonal;
           4:Result := bsCross;
           else
               Result := bsDiagCross;
      end;

 end;
 var
    oColorEdit : TColor;

begin
     Randomize;

     Brush.Style := GetStyle( Random( 6 ) );
     Brush.Color := $00D5D5;

     oColorEdit := $C8FFFF;
     eVisitante.Brush.Color := oColorEdit;
     eEmpresa.Brush.Color := oColorEdit;
     eAnfitrion.Brush.Color := oColorEdit;
     eDepartamento.Brush.Color := oColorEdit;
     eTelefono.Brush.Color := oColorEdit;

   //  bRegistraSalida.


end;

procedure TAlerta.OK_DevExClick(Sender: TObject);
begin
     Close;
end;

procedure TAlerta.SetAnfitrion(const Value: String);
begin
     FAnfitrion := Value;
     eAnfitrion.Caption := FAnfitrion;
end;

procedure TAlerta.SetDepartamento(const Value: String);
begin
     FDepartamento := Value;
     eDepartamento.Caption := FDepartamento;
end;

procedure TAlerta.SetEmpresa(const Value: String);
begin
     FEmpresa := Value;
     eEmpresa.Caption := FEmpresa;
end;

procedure TAlerta.SetEntrada(const Value: String);
begin
     if Length(Value) = 3 then
        FEntrada := '0' + Value.Substring(0,1)+':'+ Value.Substring(1, 2)
     else FEntrada :=  Value.Substring(0,2)+':'+ Value.Substring(2, 2);
     eEntrada.Caption := HoraAsStr( StrToDateTime('21/12/86 '+FEntrada+':00.000'), 'hh:nn' );
end;

procedure TAlerta.SetTelefono(const Value: String);
begin
     FTelefono := Value;
     eTelefono.Caption := FTelefono;
end;

procedure TAlerta.SetVisitante(const Value: String);
begin
     FVisitante := Value;
     eVisitante.Caption := FVisitante;
end;

procedure TAlerta.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     dmVisitantes.CierraDialogoAlerta( Self );
end;

procedure TAlerta.bRegistraSalidaClick(Sender: TObject);
begin
     dmVisitantes.DoRegistraSalida( Folio );
     Close;
end;

procedure TAlerta.Cancelar_DevExClick(Sender: TObject);
begin
     dmVisitantes.DoRegistraSalida( Folio );
     Close;
end;

procedure TAlerta.bIgnorarClick(Sender: TObject);
begin
     Close;
end;

end.





