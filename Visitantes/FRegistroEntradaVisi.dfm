object FRegistroEntradaVisit: TFRegistroEntradaVisit
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Registro entrada visitante'
  ClientHeight = 623
  ClientWidth = 656
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 656
    Height = 623
    Align = alClient
    Color = clWindow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PanelBotones: TPanel
      Left = 1
      Top = 586
      Width = 654
      Height = 36
      Align = alBottom
      TabOrder = 5
      TabStop = True
      DesignSize = (
        654
        36)
      object Cancelar_DevEx: TcxButton
        Left = 555
        Top = 6
        Width = 84
        Height = 26
        Hint = 'Cancelar visita'
        Anchors = [akTop, akRight]
        BiDiMode = bdRightToLeft
        Cancel = True
        Caption = '&Cancelar'
        LookAndFeel.SkinName = 'TressMorado2013'
        ModalResult = 2
        OptionsImage.ImageIndex = 0
        OptionsImage.Images = cxImageList24_PanelBotones
        OptionsImage.Margin = 1
        ParentBiDiMode = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        OnClick = Cancelar_DevExClick
      end
      object bOk: TcxButton
        Left = 476
        Top = 6
        Width = 75
        Height = 26
        Hint = 'Registrar visita'
        Anchors = [akTop, akRight]
        Caption = '&OK'
        OptionsImage.ImageIndex = 1
        OptionsImage.Images = cxImageList24_PanelBotones
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        OnClick = bOkClick
      end
      object btnOtro: TcxButton
        Left = 12
        Top = 4
        Width = 175
        Height = 26
        Hint = 'Registrar otro visitante'
        Anchors = [akTop, akRight]
        Caption = '&Registrar otro en grupo'
        OptionsImage.ImageIndex = 3
        OptionsImage.Images = cxImageList24_PanelBotones
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        TabStop = False
        Visible = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        OnClick = btnOtroClick
      end
    end
    object pnlObserva: TPanel
      Left = 1
      Top = 471
      Width = 654
      Height = 115
      Align = alBottom
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      object icoObserva: TImage
        Left = 12
        Top = 3
        Width = 39
        Height = 33
        Picture.Data = {
          0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
          000000200806000000737A7AF4000000206348524D00007A25000080830000F9
          FF000080E9000075300000EA6000003A980000176F925FC54600000009704859
          7300000EC400000EC401952B0E1B000001AE494441545847EDD2BB4A03411406
          E001AFA555AC7C0D7B5B4B2F8510043B5F4276B649B2339B6825826F108C48C0
          2B79006D045B7BB1118C0A1616EA7F923D61989C91C440B4D8E26372CECECCF9
          D98D4A63D3825BB81E339AD94200FBF5972840DB6F8E513B0FE007F8CC6AF6E2
          D5CF5E2DF1CFB8F52BD08C6000DA5C800998865998CC6A5ADDDF53407B6825DC
          A333524DE7E6C19D2706A04D3C84563ACC17F2A57C2107A095F01EBFE6FD0BF0
          06C100FE271885FF29883B5C0C306EFF2FC03B6CC12AAC64EB20D660DDA96FC0
          1D14D217E009A837AA03700785F405F880261CC171B6361C5CBB7DDAC73DF600
          EEA090FC4F9807C803E4013A01A40763A3AAB1ADC3259C0A4EE04A3A087778D6
          840B38873390EE08A199750A105403AB8D323A3944ED0728FAFB7F436CB25E80
          2899C1C07B37808DED0688E78621365D95A8A2CA90E864C90BB04901F05B3C37
          28B1E9EA05881255D576DF0FC0A4B383109B5DA9AAE98ADA8D4A50667318FE98
          0528BA017EFB26C466971840A5DA2C6701B6DD004CBE2B4C6C76879BBEE164AF
          13226998D8EE4801867D1362F3E700259546A66074BA68358606C8F7FAACFA06
          40DE0D701F30BC5E0000000049454E44AE426082}
      end
      object lbIcoObserva: TLabel
        Left = 54
        Top = 5
        Width = 129
        Height = 25
        Caption = 'Observaciones'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Image5: TImage
        Left = 191
        Top = 18
        Width = 448
        Height = 6
        Picture.Data = {
          0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000258
          0000000308060000004FB071C10000000467414D410000B18F0BFC6105000000
          097048597300000EC400000EC401952B0E1B0000002A494441545847EDD62101
          0000080330FA67C24031287139B10EABED39000072040B00204CB00000A2E61E
          63B5CD3B79688B200000000049454E44AE426082}
      end
      object lbObserva1: TLabel
        Left = 13
        Top = 34
        Width = 74
        Height = 17
        Alignment = taRightJustify
        Caption = 'Comentarios'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object lbObserva2: TLabel
        Left = 19
        Top = 48
        Width = 68
        Height = 17
        Alignment = taRightJustify
        Caption = 'adicionales:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object lbTexto1: TLabel
        Left = 372
        Top = 35
        Width = 54
        Height = 17
        Alignment = taRightJustify
        Caption = 'Texto #1:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object lbTexto2: TLabel
        Left = 372
        Top = 62
        Width = 54
        Height = 17
        Alignment = taRightJustify
        Caption = 'Texto #2:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object lbTexto3: TLabel
        Left = 372
        Top = 89
        Width = 54
        Height = 17
        Alignment = taRightJustify
        Caption = 'Texto #3:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object LI_OBSERVA: TcxMemo
        Left = 90
        Top = 35
        ParentFont = False
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -13
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 0
        Height = 66
        Width = 242
      end
      object LI_TEXTO1: TEdit
        Left = 430
        Top = 32
        Width = 209
        Height = 25
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        MaxLength = 40
        ParentFont = False
        TabOrder = 1
      end
      object LI_TEXTO2: TEdit
        Left = 430
        Top = 59
        Width = 209
        Height = 25
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        MaxLength = 40
        ParentFont = False
        TabOrder = 2
      end
      object LI_TEXTO3: TEdit
        Left = 430
        Top = 86
        Width = 209
        Height = 25
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        MaxLength = 40
        ParentFont = False
        TabOrder = 3
        OnKeyDown = LI_TEXTO3KeyDown
      end
    end
    object cxGroupBox1: TPanel
      Left = 1
      Top = 339
      Width = 654
      Height = 132
      Margins.Left = 1
      Margins.Top = 1
      Margins.Right = 1
      Margins.Bottom = 1
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      object pnlIdentifica: TPanel
        Left = 0
        Top = 0
        Width = 322
        Height = 132
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object icoIdenti: TImage
          Left = 12
          Top = 0
          Width = 35
          Height = 29
          Picture.Data = {
            0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
            000000200806000000737A7AF40000000467414D410000B18F0BFC6105000000
            097048597300000EC300000EC301C76FA86400000145494441545847ED542156
            C340108DA8E0003D42250740227A008E5059D903F0DEB028BAB3511CA0B20281
            E000888A0A24B2A21289405454C09FED92A6C90402DB4704FBDFFB229B9D993F
            7F26C91212129CE175971401EF5D32096814C0C6BE324DC74C7C6AC90ED9F0BD
            762F96AA00147F93C2614F0BE48667DAFD18EA0E5CDBDB50F3008E5C5FBD1F41
            DD01581F6AD680F72FD5FB313CA6031B8C6EE18C7DFC9EFE1FE0E38EB6035FB9
            A641965CE27407C09F7E05BB845AB72A9F3FE39A0460CE765E39DB42C8048556
            95F328D60560FE44D4034FF0BC810B97E87E2914EBE41DC4DDD5E27EC9030128
            F22005FC900074EB97CA8FC3F055380E22F8691F07FB697A81719DB761B981B2
            806D4EF920D4F090EE7D6214CFE9E62C1C7BC87E9462C5A9B1233B6A43E42BF6
            A910205DC896B76539490C9B96F0CF9804742F2021E19F23CB3E003FBDCB13A5
            C773F10000000049454E44AE426082}
        end
        object lbIcoIdenti: TLabel
          Left = 48
          Top = 3
          Width = 121
          Height = 25
          Caption = 'Identificaci'#243'n'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Segoe UI'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Image2: TImage
          Left = 176
          Top = 15
          Width = 127
          Height = 6
          Picture.Data = {
            0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000258
            0000000308060000004FB071C10000000467414D410000B18F0BFC6105000000
            097048597300000EC400000EC401952B0E1B0000002A494441545847EDD62101
            0000080330FA67C24031287139B10EABED39000072040B00204CB00000A2E61E
            63B5CD3B79688B200000000049454E44AE426082}
        end
        object lbIdentificacion: TLabel
          Left = 13
          Top = 30
          Width = 79
          Height = 17
          Alignment = taRightJustify
          Caption = 'Identificaci'#243'n:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object lbId: TLabel
          Left = 58
          Top = 57
          Width = 34
          Height = 17
          Alignment = taRightJustify
          Caption = '#  ID.:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object liGafete: TLabel
          Left = 39
          Top = 85
          Width = 53
          Height = 17
          Alignment = taRightJustify
          Caption = '# Gafete:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object cbTipoId: TcxComboBox
          Left = 97
          Top = 29
          ParentFont = False
          Properties.DropDownListStyle = lsFixedList
          Properties.ReadOnly = False
          Style.Font.Charset = ANSI_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -13
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 0
          Width = 207
        end
        object LI_ID: TEdit
          Left = 98
          Top = 55
          Width = 205
          Height = 25
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          MaxLength = 40
          ParentFont = False
          TabOrder = 1
        end
        object LI_GAFETE: TEdit
          Left = 98
          Top = 82
          Width = 205
          Height = 25
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          MaxLength = 20
          ParentFont = False
          TabOrder = 2
        end
      end
      object pnlAuto: TPanel
        Left = 322
        Top = 0
        Width = 332
        Height = 132
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alClient
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object icoAuto: TImage
          Left = 12
          Top = 1
          Width = 35
          Height = 30
          Picture.Data = {
            0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
            000000200806000000737A7AF40000000467414D410000B18F0BFC6105000000
            097048597300000EC300000EC301C76FA864000001DB494441545847ED55A152
            C340108D402010884A040281442011880A24B21281442210CC2C0D82E62E9D08
            FA07084425A21281A8402290C80A04028140C0EEDDA6B9BB1C692F196698216F
            E6CD646EEFEDBDEC6D36518B16195C6D36E508466B9C2E1C695F7E352688234E
            170E6FC250FE150312E4BEECCB0B1FE9AAD4812EBC094389060630D8C0E7CF52
            ACE02300ACF0B1053C1BC38906E8EDBD3193B1CCF8D802DE8DA1F418C0B24FCD
            2F258DC5B5DE9B1CF2D11AA6A836BD1510F77C8402955FF6C503F2CDEA075B54
            934B1820509F90018C17FD608B6A72490304BA0215CFFBC116D564800102C66E
            B50EFBC116D5A4D7807C716741CEDC005DC76F1A588AAD814A037447F4EDE29D
            3DF9E273FA0D7CA06E9CC6E9A51E42FE1C3F199861C21E37AD829EF5DCBD2E1D
            0364DAFDF9A84104F204E3D6FFA26400C5EF02C44E06D97AEE1C3BF70ED7BA94
            04F74C5C8D6900F5CFA4F5E9C98884E4DCD4962B8003428F4D39756374C81086
            5BEE3A1950F35E3D27C7557A1D13AFBC368BE8BB349942BA8DDC338505C598DE
            02DFE6D4D450C578BD8BDACE223D1AE95125725D09F37159E684B754A2A91E13
            A41D2C13FD34EC0420CE784B259AEA15B03C07C65D116FE8FE38BC104DF50A28
            584D20D9A537E2A52034D5B7F80F88A26F360996C72DEABF480000000049454E
            44AE426082}
        end
        object lbIcoAuto: TLabel
          Left = 49
          Top = 4
          Width = 76
          Height = 25
          Caption = 'Veh'#237'culo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Segoe UI'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Image4: TImage
          Left = 129
          Top = 17
          Width = 189
          Height = 11
          Picture.Data = {
            0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000258
            0000000308060000004FB071C10000000467414D410000B18F0BFC6105000000
            097048597300000EC400000EC401952B0E1B0000002A494441545847EDD62101
            0000080330FA67C24031287139B10EABED39000072040B00204CB00000A2E61E
            63B5CD3B79688B200000000049454E44AE426082}
        end
        object liTipoVehiculo: TLabel
          Left = 76
          Top = 31
          Width = 29
          Height = 17
          Alignment = taRightJustify
          Caption = 'Tipo:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object lbCarPla: TLabel
          Left = 66
          Top = 56
          Width = 39
          Height = 17
          Alignment = taRightJustify
          Caption = 'Placas:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object lbCarDesc: TLabel
          Left = 34
          Top = 83
          Width = 71
          Height = 17
          Alignment = taRightJustify
          Caption = 'Descripci'#243'n:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object lbCarEst: TLabel
          Left = 69
          Top = 110
          Width = 36
          Height = 17
          Alignment = taRightJustify
          Caption = 'Caj'#243'n:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object cbTipoAuto: TcxComboBox
          Left = 109
          Top = 30
          ParentFont = False
          Properties.DropDownListStyle = lsFixedList
          Properties.ReadOnly = False
          Style.Font.Charset = ANSI_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -13
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 0
          Width = 209
        end
        object LI_CAR_PLA: TEdit
          Left = 110
          Top = 56
          Width = 207
          Height = 25
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          MaxLength = 8
          ParentFont = False
          TabOrder = 1
        end
        object LI_CAR_DES: TEdit
          Left = 110
          Top = 83
          Width = 207
          Height = 25
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          MaxLength = 40
          ParentFont = False
          TabOrder = 2
        end
        object LI_CAR_EST: TEdit
          Left = 110
          Top = 110
          Width = 207
          Height = 25
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          MaxLength = 8
          ParentFont = False
          TabOrder = 3
        end
      end
    end
    object pnlVisitaA: TPanel
      Left = 1
      Top = 228
      Width = 654
      Height = 86
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      Align = alTop
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object Image1: TImage
        Left = 12
        Top = 3
        Width = 34
        Height = 32
        Picture.Data = {
          0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
          000000200806000000737A7AF40000000467414D410000B18F0BFC6105000000
          097048597300000EC300000EC301C76FA86400000146494441545847ED96AD6E
          024110C757202B10086465058F80E8A3201195F5D33D43D859720292CACACA0A
          0412D107A8402210081E0089A033972121C7B0E5EEC842D29BE467667666FEFB
          717B6B6ABBC452489B0EDCEBC8E2C725A0C5371ADF96F4EAE61337F616F74520
          115349AF6E547025857FB4191F83D67DCBD895A457B783005E5A719D350FAE57
          4A00003452183C6A50B1755634C1548B1FC3674504ACB538C3BDA4AD3113983C
          78EB3E2961278931E05EB3ECA0FAC4BFE7823199F1FE6E72CEA8B00035108B5A
          402DE0F602B4FB3C26721FDED034552138C7837FCAFBC9D72D554FDB9710DC04
          019F4F62F417E4D889FF0F4A0908AD809613A27002CDBEC38D3423112D2D2744
          6101C4263FFB03F41A5A2AE383941170556A01F720C02DB4400CE8D06EF95279
          D18231182588F2FDBA1EBDF9BF6835E631E05E08C37ED6FC9F9B31BFCE917142
          106FDD950000000049454E44AE426082}
      end
      object Label3: TLabel
        Left = 46
        Top = 8
        Width = 163
        Height = 25
        Caption = 'Visitando / asunto'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Image3: TImage
        Left = 215
        Top = 19
        Width = 425
        Height = 6
        Picture.Data = {
          0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000258
          0000000308060000004FB071C10000000467414D410000B18F0BFC6105000000
          097048597300000EC400000EC401952B0E1B0000002A494441545847EDD62101
          0000080330FA67C24031287139B10EABED39000072040B00204CB00000A2E61E
          63B5CD3B79688B200000000049454E44AE426082}
      end
      object Label7: TLabel
        Left = 63
        Top = 37
        Width = 45
        Height = 17
        Alignment = taRightJustify
        Caption = 'Visita a:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object Label8: TLabel
        Left = 21
        Top = 64
        Width = 87
        Height = 17
        Alignment = taRightJustify
        Caption = 'Departamento:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object VISITAA: TZetaKeyLookup_DevEx
        Left = 710
        Top = 2
        Width = 338
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = False
        WidthLlave = 50
        OnExit = VISITAAExit
      end
      object bAgregaVisitante: TcxButton
        Left = 1005
        Top = -14
        Width = 21
        Height = 21
        Hint = 'Registrar anfitri'#243'n'
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A40500000000000000000000000000000000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFFFFF
          FFFFFFFFFFFFFFFFFFFF66D8A1FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFF66D8A1FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFF
          FFFF66D8A1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FFFFFFFFFFFFFFFFFFFFFFFFFF66D8A1FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8A1FF66D8
          A1FF66D8A1FF66D8A1FF66D8A1FFFFFFFFFFFFFFFFFFFFFFFFFF79DDACFF66D8
          A1FF66D8A1FF66D8A1FF66D8A1FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFFFFF
          FFFFFFFFFFFFFFFFFFFF66D8A1FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFF66D8A1FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFF
          FFFF66D8A1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FFFFFFFFFFFFFFFFFFFFFFFFFF66D8A1FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFF66D8A1FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        TabStop = False
        OnClick = bAgregaVisitanteClick
      end
      object cmbDEPTO: TcxComboBox
        Left = 111
        Top = 62
        TabStop = False
        Enabled = False
        ParentFont = False
        Properties.DropDownListStyle = lsFixedList
        Properties.ReadOnly = False
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -13
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 2
        Width = 299
      end
      object LI_ANFITR: TEdit
        Left = 112
        Top = 36
        Width = 297
        Height = 25
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnEnter = LI_ANFITREnter
        OnExit = LI_ANFITRExit
      end
      object bBuscaAnfitrion: TcxButton
        Left = 411
        Top = 36
        Width = 72
        Height = 24
        Hint = 'Buscar Visitante'
        Caption = 'B&uscar'
        OptionsImage.Glyph.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000008B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCECACEFFCECA
          CEFF8D838EFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFFFFF
          FFFFE4E1E4FF8F8590FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFA199A2FFFAF9FAFFFFFFFFFFFFFF
          FFFFD2CED2FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF948B95FFF0EFF1FFFFFFFFFFFFFFFFFFE7E5
          E8FF8F8590FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8D838EFFE0DDE0FFFFFFFFFFFFFFFFFFF6F5F6FF988F
          99FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA49DA5FFD9D5D9FFFDFD
          FDFFFFFFFFFFF4F3F4FFE6E3E6FFFFFFFFFFFFFFFFFFFDFDFDFFAAA3ABFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFB1ABB2FFFDFDFDFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3BEC4FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF9B939CFFFDFDFDFFFFFFFFFFEDEBEDFFB5AE
          B5FFA8A1A9FFBEB8BFFFF4F3F4FFFFFFFFFFFDFDFDFF968D97FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFD3D0D4FFFFFFFFFFEBE9EBFF908791FF8B81
          8CFF8B818CFF8B818CFF988F99FFF4F3F4FFFFFFFFFFCAC6CBFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFF0EFF1FFFFFFFFFFB3ACB4FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFC3BEC4FFFFFFFFFFE7E5E8FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFFFFFFA69FA7FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFF0EFF1FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFEFEDEFFFFFFFFFFFB8B2B9FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFCCC8CCFFFFFFFFFFE4E1E4FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFCCC8CCFFFFFFFFFFF4F3F4FF968D97FF8B81
          8CFF8B818CFF8B818CFF9F97A0FFFBFBFBFFFFFFFFFFC2BCC2FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF988F99FFF4F3F4FFFFFFFFFFF4F3F4FFC2BC
          C2FFB7B0B7FFC7C2C7FFFAF9FAFFFFFFFFFFF2F1F2FF8F8590FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFA49DA5FFF8F7F8FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFEBE9EBFF988F99FF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF928993FFC5C0C6FFE2DF
          E2FFF0EFF1FFE0DDE0FFBCB6BDFF908791FF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
        OptionsImage.Margin = 0
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        TabStop = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        OnClick = bBuscaAnfitrionClick
      end
    end
    object pnlTop: TPanel
      Left = 1
      Top = 1
      Width = 654
      Height = 227
      Align = alTop
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object Label2: TLabel
        Left = 13
        Top = 178
        Width = 92
        Height = 13
        Alignment = taRightJustify
        Caption = 'Seleccione c'#225'mara:'
      end
      object Label24: TLabel
        Left = 288
        Top = 18
        Width = 52
        Height = 17
        Alignment = taRightJustify
        Caption = 'Nombre:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object lblApePat: TLabel
        Left = 239
        Top = 45
        Width = 101
        Height = 17
        Alignment = taRightJustify
        Caption = 'Apellido paterno:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object lblApeMat: TLabel
        Left = 236
        Top = 72
        Width = 104
        Height = 17
        Alignment = taRightJustify
        Caption = 'Apellido materno:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object lblSexo: TLabel
        Left = 309
        Top = 98
        Width = 31
        Height = 17
        Alignment = taRightJustify
        Caption = 'Sexo:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 241
        Top = 126
        Width = 99
        Height = 17
        Alignment = taRightJustify
        Caption = 'Tipo de visitante:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object lblNacion: TLabel
        Left = 260
        Top = 151
        Width = 80
        Height = 17
        Alignment = taRightJustify
        Caption = 'Nacionalidad:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object Label5: TLabel
        Left = 255
        Top = 178
        Width = 85
        Height = 17
        Alignment = taRightJustify
        Caption = 'Inf. migratoria:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object Label6: TLabel
        Left = 278
        Top = 204
        Width = 62
        Height = 17
        Alignment = taRightJustify
        Caption = 'Compa'#241#237'a:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object lblFecNac: TLabel
        Left = 431
        Top = 98
        Width = 60
        Height = 17
        Alignment = taRightJustify
        Caption = 'Fecha nac:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object Foto: TImageEn
        Left = 12
        Top = 9
        Width = 200
        Height = 163
        Cursor = crArrow
        Background = clBtnFace
        ParentCtl3D = False
        AutoShrink = True
        OnDShowNewFrame = FotoDShowNewFrame
        EnableInteractionHints = True
        TabOrder = 0
      end
      object cmbDriverFoto: TComboBox
        Left = 12
        Top = 191
        Width = 160
        Height = 21
        Style = csDropDownList
        TabOrder = 1
        TabStop = False
        OnChange = cmbDriverFotoChange
        Items.Strings = (
          '')
      end
      object btnTomaFoto: TcxButton
        Left = 178
        Top = 178
        Width = 34
        Height = 34
        Hint = 'Tomar foto'
        Enabled = False
        OptionsImage.ImageIndex = 0
        OptionsImage.Images = cxImageList32
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        TabStop = False
        OnClick = btnTomaFotoClick
      end
      object VI_NOMBRES: TEdit
        Left = 344
        Top = 15
        Width = 269
        Height = 25
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        MaxLength = 30
        ParentFont = False
        TabOrder = 3
      end
      object VI_APE_PAT: TEdit
        Left = 344
        Top = 42
        Width = 269
        Height = 25
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        MaxLength = 30
        ParentFont = False
        TabOrder = 4
      end
      object VI_APE_MAT: TEdit
        Left = 344
        Top = 69
        Width = 269
        Height = 25
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        MaxLength = 30
        ParentFont = False
        TabOrder = 5
      end
      object cbSexo: TcxComboBox
        Left = 343
        Top = 95
        ParentFont = False
        Properties.DropDownListStyle = lsFixedList
        Properties.Items.Strings = (
          'Masculino'
          'Femenino')
        Properties.ReadOnly = False
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -13
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 6
        Text = 'Masculino'
        Width = 85
      end
      object cbTipoVisitante: TcxComboBox
        Left = 343
        Top = 122
        ParentFont = False
        Properties.DropDownListStyle = lsFixedList
        Properties.ReadOnly = False
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -13
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 8
        Width = 226
      end
      object VI_NACIONALIDAD: TEdit
        Left = 344
        Top = 148
        Width = 224
        Height = 25
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        MaxLength = 30
        ParentFont = False
        TabOrder = 9
      end
      object VI_MIGRA: TEdit
        Left = 344
        Top = 175
        Width = 224
        Height = 25
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        MaxLength = 40
        ParentFont = False
        TabOrder = 10
      end
      object dblCompania: TZetaKeyLookup_DevEx
        Left = 710
        Top = 174
        Width = 267
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 11
        TabStop = False
        Visible = False
        WidthLlave = 50
      end
      object bAgregaEmpre: TcxButton
        Left = 983
        Top = 175
        Width = 21
        Height = 21
        Hint = 'Registrar compa'#241#237'a'
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A40500000000000000000000000000000000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFFFFF
          FFFFFFFFFFFFFFFFFFFF66D8A1FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFF66D8A1FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFF
          FFFF66D8A1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FFFFFFFFFFFFFFFFFFFFFFFFFF66D8A1FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8A1FF66D8
          A1FF66D8A1FF66D8A1FF66D8A1FFFFFFFFFFFFFFFFFFFFFFFFFF79DDACFF66D8
          A1FF66D8A1FF66D8A1FF66D8A1FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFFFFF
          FFFFFFFFFFFFFFFFFFFF66D8A1FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFF66D8A1FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFF
          FFFF66D8A1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FFFFFFFFFFFFFFFFFFFFFFFFFF66D8A1FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFF66D8A1FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 12
        TabStop = False
        Visible = False
        OnClick = bAgregaEmpreClick
      end
      object LI_EMPRESA: TEdit
        Left = 344
        Top = 202
        Width = 224
        Height = 25
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        TabOrder = 13
        OnExit = LI_EMPRESAExit
      end
      object bBuscaEmpre: TcxButton
        Left = 570
        Top = 202
        Width = 70
        Height = 24
        Hint = 'Buscar Visitante'
        Caption = '&Buscar'
        OptionsImage.Glyph.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000008B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCECACEFFCECA
          CEFF8D838EFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFFFFF
          FFFFE4E1E4FF8F8590FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFA199A2FFFAF9FAFFFFFFFFFFFFFF
          FFFFD2CED2FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF948B95FFF0EFF1FFFFFFFFFFFFFFFFFFE7E5
          E8FF8F8590FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8D838EFFE0DDE0FFFFFFFFFFFFFFFFFFF6F5F6FF988F
          99FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA49DA5FFD9D5D9FFFDFD
          FDFFFFFFFFFFF4F3F4FFE6E3E6FFFFFFFFFFFFFFFFFFFDFDFDFFAAA3ABFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFB1ABB2FFFDFDFDFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3BEC4FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF9B939CFFFDFDFDFFFFFFFFFFEDEBEDFFB5AE
          B5FFA8A1A9FFBEB8BFFFF4F3F4FFFFFFFFFFFDFDFDFF968D97FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFD3D0D4FFFFFFFFFFEBE9EBFF908791FF8B81
          8CFF8B818CFF8B818CFF988F99FFF4F3F4FFFFFFFFFFCAC6CBFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFF0EFF1FFFFFFFFFFB3ACB4FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFC3BEC4FFFFFFFFFFE7E5E8FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFFFFFFA69FA7FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFF0EFF1FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFEFEDEFFFFFFFFFFFB8B2B9FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFCCC8CCFFFFFFFFFFE4E1E4FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFCCC8CCFFFFFFFFFFF4F3F4FF968D97FF8B81
          8CFF8B818CFF8B818CFF9F97A0FFFBFBFBFFFFFFFFFFC2BCC2FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF988F99FFF4F3F4FFFFFFFFFFF4F3F4FFC2BC
          C2FFB7B0B7FFC7C2C7FFFAF9FAFFFFFFFFFFF2F1F2FF8F8590FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFA49DA5FFF8F7F8FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFEBE9EBFF988F99FF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF928993FFC5C0C6FFE2DF
          E2FFF0EFF1FFE0DDE0FFBCB6BDFF908791FF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
        OptionsImage.Margin = 0
        ParentShowHint = False
        ShowHint = True
        TabOrder = 14
        TabStop = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        OnClick = bBuscaEmpreClick
      end
      object VI_FEC_NAC: TZetaDBFecha
        Left = 495
        Top = 96
        Width = 118
        Height = 24
        Cursor = crArrow
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        Text = '25-Feb-15'
        Valor = 42060.000000000000000000
        OnKeyDown = VI_FEC_NACKeyDown
        DataField = 'VI_FEC_NAC'
        DataSource = dsVisita
      end
    end
    object pnlAsunto: TPanel
      Left = 1
      Top = 314
      Width = 654
      Height = 25
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object Label9: TLabel
        Left = 18
        Top = 2
        Width = 90
        Height = 17
        Alignment = taRightJustify
        Caption = 'Asunto a tratar:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object cmbTipoAsunto: TcxComboBox
        Left = 111
        Top = 1
        ParentFont = False
        Properties.DropDownListStyle = lsFixedList
        Properties.ReadOnly = False
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -13
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 0
        Width = 299
      end
    end
  end
  object cxImageList24_PanelBotones: TcxImageList
    Height = 24
    Width = 24
    FormatVersion = 1
    DesignInfo = 2621496
    ImageInfo = <
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF98A1E2FFF6F7FDFFB7BEEBFF4B5BCDFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFB1B8
          E9FFF6F7FDFFACB3E8FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFFE2E5F7FFFFFFFFFFFFFFFFFFC3C8EEFF4B5BCDFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFF
          FFFFFFFFFFFFF6F7FDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF9BA4E3FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4B5B
          CDFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFA6AEE6FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4B5BCDFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4B5BCDFF4B5BCDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFC3C8EEFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6
          E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8
          EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4B5BCDFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4B5BCDFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFAFB6E9FFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4B5B
          CDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4B5BCDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4B5BCDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFFA9B0E7FFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFB7BEEBFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFFE2E5F7FFFFFFFFFFFFFFFFFFAFB6E9FF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFF6F7FDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF8792DEFFDFE2F6FFA1A9E5FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF9BA4
          E3FFDFE2F6FF98A1E2FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          200000000000000900000000000000000000000000000000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8
          A1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF8CE2B8FFE9F9
          F1FF55D396FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8A1FFF4FCF8FFFFFF
          FFFFADEACCFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF53D395FFD6F4E6FFFFFFFFFFFFFF
          FFFFFCFEFDFF71DAA7FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FFA5E8C8FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFD9F5E7FF53D395FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF79DDACFFFCFEFDFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF9AE5C1FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF5BD59AFFE9F9F1FFFFFFFFFFFFFFFFFFC3EFDAFFBDEE
          D6FFFFFFFFFFFFFFFFFFF4FCF8FF63D79FFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FFC3EFDAFFFFFFFFFFF7FDFAFF8CE2B8FF50D293FF55D3
          96FFE1F7ECFFFFFFFFFFFFFFFFFFC8F1DDFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF8AE1B7FFE6F9F0FF6BD9A4FF50D293FF50D293FF50D2
          93FF60D69DFFE4F8EEFFFFFFFFFFFFFFFFFF8FE2BAFF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF53D395FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF66D8A1FFEFFBF5FFFFFFFFFFF7FDFAFF69D8A2FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF6BD9A4FFEFFBF5FFFFFFFFFFDEF7EBFF53D395FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF79DDACFFF7FDFAFFFFFFFFFFB8EDD3FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF71DAA7FFEFFBF5FFFFFFFFFFA0E6
          C4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8A1FFE1F7ECFFFFFF
          FFFF8FE2BAFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF58D498FFBDEE
          D6FFFCFEFDFF8AE1B7FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF81DFB1FF8FE2BAFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF5362CFFF6D7AD6FF8D97DFFFACB3E8FFC8CD
          F0FFE8EAF9FFBAC0ECFF707DD7FF7682D9FF6572D4FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF848FDDFF8D97DFFFACB3E8FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFF969FE2FF6572D4FF969FE2FF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
          FEFFD4D8F3FFBAC0ECFF4858CCFF4858CCFF969FE2FFF9FAFDFFFFFFFFFFFFFF
          FFFFA4ACE6FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE5E7F8FFBAC0ECFF4858CCFF4858CCFF707DD7FFD1D5F2FFE5E7F8FFBAC0
          ECFF818CDCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF6A77D6FF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFFACB3E8FFBAC0ECFFB1B8E9FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF5665D0FF7682D9FF969FE2FFB1B8
          E9FFD1D5F2FFACB3E8FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          200000000000000900000000000000000000000000000000000048BD85E64CC7
          8CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC7
          8CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC7
          8CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF248BD85E650D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF4CC78CF250D2
          93FF50D293FF50D293FF50D293FF51D293FF48D08EFF45CF8CFF45CF8CFF45CF
          8CFF45CF8CFF45CF8CFF45CF8CFF45CF8CFF45CF8CFF45CF8CFF45CF8CFF45CF
          8CFF4ED191FF51D294FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF50D293FF4FD293FF56D497FF59D498FF59D498FF59D4
          98FF59D498FF59D498FF59D498FF59D498FF59D498FF59D498FF59D498FF59D4
          98FF52D395FF4FD293FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF49D190FF45CF8DFFD5F4E5FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6FD
          FAFF71DAA7FF40CE89FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF4CD190FF4AD08FFFADEACCFFD6F4E6FFD0F3E2FFD0F3
          E2FFD0F3E2FFD0F3E2FFD0F3E2FFD0F3E2FFD0F3E2FFCAF1DEFFE2F7EDFFFFFF
          FFFF7ADDADFF3DCD87FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF51D293FF51D294FF47D08EFF44CF8BFF40CE89FF40CE
          89FF40CE89FF40CE89FF40CE89FF40CE89FF40CE89FF29C87BFF81DEB1FFFFFF
          FFFF7CDDAEFF3ECD88FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF50D293FF50D293FF4CD191FF4BD190FF4CD190FF4CD1
          91FF4AD08FFF48D08EFF4DD191FF4CD190FF4BD190FF35CB82FF8AE1B7FFFFFF
          FFFF7ADDADFF3ECD88FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF50D293FF51D294FF51D294FF50D293FF50D293FF50D2
          93FF53D395FF43CF8BFF46D08DFF52D394FF52D294FF3CCC87FF8BE1B7FFFFFF
          FFFF7BDDAEFF3FCD89FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF50D293FF51D394FF48D08EFF45CF8CFF45CF8CFF39CC
          85FF5AD599FFB2EBD0FF43CF8BFF3FCE89FF54D396FF3CCD87FF8CE1B7FFFFFF
          FFFF7BDDAEFF3FCE89FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF4FD292FF51D293FF59D599FF5AD599FF56D497FF53D2
          95FF77DCABFFFFFFFFFFCCF1DFFF41CE8AFF44CF8CFF3CCD87FF8DE2B9FFFFFF
          FFFF7BDDADFF3ECE88FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF45CF8CFF59D499FFF9FEFBFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD1F3E3FF45CF8CFF33CA82FF8FE2B9FFFFFF
          FFFF79DCACFF3ECE88FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF44CF8CFF5AD499FFFAFEFCFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD7F5E6FF45CF8CFF35CB82FF8CE2B7FFFFFF
          FFFF79DCACFF3FCE89FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF4FD293FF51D294FF5BD59AFF5AD599FF56D497FF53D2
          95FF76DCAAFFFFFFFFFFD2F3E3FF43CF8AFF44CF8BFF3CCD87FF8BE1B7FFFFFF
          FFFF79DDADFF3FCE89FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF51D293FF50D293FF46CF8CFF45CF8CFF45CF8CFF39CC
          85FF5BD599FFB9EDD3FF45CF8CFF3ECE88FF53D395FF3ACC86FF8CE1B7FFFFFF
          FFFF7ADDADFF3FCE89FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF50D293FF51D293FF50D293FF50D293FF50D293FF50D2
          93FF54D396FF44CF8BFF45CF8CFF53D395FF50D293FF3ACC85FF8DE1B8FFFFFF
          FFFF7ADDADFF3FCE89FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF50D293FF50D293FF4CD190FF4AD190FF4CD191FF4CD1
          91FF4AD08FFF47D08EFF4DD191FF4CD190FF4CD190FF36CB83FF8AE0B7FFFFFF
          FFFF7BDDAEFF3FCE89FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF51D293FF51D293FF4BD08FFF45CF8CFF40CE89FF40CE
          89FF40CE89FF40CE89FF40CE89FF40CE89FF40CE89FF2AC87CFF80DEB0FFFFFF
          FFFF7ADDADFF3ECE88FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF4CD090FF49D08EFFAEEBCDFFD7F5E6FFCFF3E2FFD0F3
          E2FFD0F3E2FFD0F3E2FFD0F3E2FFD0F3E2FFD0F3E2FFCBF2DFFFE0F7ECFFFFFF
          FFFF78DCABFF3ECE88FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF4AD090FF45D08CFFD5F5E5FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6FC
          F8FF72DAA8FF40CE89FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF50D293FF4FD293FF57D497FF59D498FF59D498FF59D4
          98FF59D498FF59D498FF59D498FF59D498FF59D498FF59D498FF59D498FF59D4
          98FF52D295FF4FD293FF50D293FF50D293FF50D293FF4CC78CF24CC78CF250D2
          93FF50D293FF50D293FF50D293FF51D293FF48D08EFF45CF8CFF45CF8CFF45CF
          8CFF45CF8CFF45CF8CFF45CF8CFF45CF8CFF45CF8CFF45CF8CFF45CF8CFF45CF
          8CFF4ED191FF51D294FF50D293FF50D293FF50D293FF4CC78CF250D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF48BD85E64CC7
          8CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC7
          8CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF24CC7
          8CF24CC78CF24CC78CF24CC78CF24CC78CF24CC78CF248BD85E6}
      end>
  end
  object dsBuscaVisita: TDataSource
    Left = 156
    Top = 103
  end
  object dsBuscaVisitandoA: TDataSource
    Left = 92
    Top = 120
  end
  object dsTipoVisitante: TDataSource
    Left = 84
    Top = 40
  end
  object dsVehiculo: TDataSource
    Left = 68
    Top = 88
  end
  object dsAsunto: TDataSource
    Left = 148
    Top = 64
  end
  object DataSource: TDataSource
    Left = 180
    Top = 80
  end
  object dsBuscaCompania: TDataSource
    Left = 36
    Top = 86
  end
  object dsTipoID: TDataSource
    Left = 44
    Top = 96
  end
  object dsVisita: TDataSource
    Left = 156
    Top = 120
  end
  object TDataSource
    Left = 148
    Top = 16
  end
  object dsSQL: TDataSource
    Left = 76
    Top = 64
  end
  object cxImageList32: TcxImageList
    Height = 32
    Width = 32
    FormatVersion = 1
    DesignInfo = 4194440
    ImageInfo = <
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          200000000000001000000000000000000000000000000000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF7CDDAEFFD6F4E6FFF7FDFAFFFAFEFCFFDBF6
          E9FF92E3BCFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FFA5E8C8FFFFFFFFFFFFFFFFFFFBFEFDFFFBFEFDFFFFFF
          FFFFFFFFFFFFC0EFD8FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF76DCABFFFDFEFDFFFFFFFFFFFFFFFFFF87E0B5FF87E0B5FFFFFF
          FFFFFFFFFFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFC5F0DBFFFFFFFFFFFFFFFFFFFFFFFFFF84DFB3FF84DFB3FFFFFF
          FFFFFFFFFFFFFFFFFFFFDCF6E9FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFE7F9F0FFFEFFFEFF68D8A2FF6FDAA6FF5ED59BFF5FD59CFF69D9
          A2FF66D8A0FFFBFEFDFFFAFEFCFF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF4FD092FD50D2
          93FF50D293FFE2F7ECFFFDFEFEFF8FE3B9FF8FE3B9FF60D59CFF64D79FFF9AE5
          C0FF96E4BEFFFCFEFDFFF7FDFAFF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFC0EFD8FFFFFFFFFFFFFFFFFFFFFFFFFF7DDDAFFF75DCAAFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6F4E6FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF51D293FF50D293FF50D293FF50D2
          93FF50D293FF51D293FF51D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF6BD9A4FFFAFEFCFFFFFFFFFFFFFFFFFF8FE1BAFF8DE1B8FFFFFF
          FFFFFFFFFFFFFFFFFFFF7CDDAEFF50D293FF50D293FF50D293FF50D293FF50D2
          93FF4ECC8FF84FD092FD4FD091FC50D192FE50D192FE50D293FF50D192FE4FD0
          92FD50D192FE4FD091FC4FD092FD50D293FF50D293FF50D293FF50D293FF50D1
          92FE50D293FF50D293FF8CE2B8FFF7FDFAFFFFFFFFFFFBFEFDFFFBFEFDFFFFFF
          FFFFFDFEFEFFA5E8C8FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF92E3BCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFF
          FFFFFAFEFCFFFAFEFCFFFAFEFCFFFAFEFCFFFBFEFCFFFAFEFCFFFBFEFCFFFFFF
          FFFFFEFFFEFFA0E7C5FF50D293FF6BD9A4FFC0EFD8FFE1F7ECFFE4F8EEFFC5F0
          DBFF76DCABFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF92E3BCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFEFFFEFFFEFEFEFFB6ECD2FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF92E3BCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF83DFB2FF50D293FF50D192FE50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF92E3BCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA8E9C9FF50D293FF4FD092FD50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FFE4F8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFF4FCF8FF55D396FF50D293FFFFFFFFFFA8E9C9FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF63D79FFFDBF6E9FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEF7
          EBFF6EDAA6FF50D293FF50D293FFFFFFFFFFA7E9C9FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF87E0
          B5FFDEF7EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEF7EBFF87E0B5FF50D2
          93FF50D293FF50D293FF50D293FFFFFFFFFFA6E9C8FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF50D2
          93FF53D395FFCEF2E1FFFFFFFFFFFFFFFFFFDBF6E9FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFFFFFFFFFA6E9C8FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFD3F4E4FFFFFFFFFFFFFFFFFFE6F9F0FF53D395FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFFFFFFFFFA6E8C8FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF50D2
          93FF92E3BCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA8E9C9FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFFFFFFFFFA6E9C8FF50D293FF50D192FE50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF50D2
          93FFECFAF3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFEFCFF53D395FF50D2
          93FF50D293FF50D293FF50D293FFFFFFFFFFA7E9C8FF50D293FF50D293FF50D2
          93FF50D192FE50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF5ED6
          9BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF71DAA7FF50D2
          93FF50D293FF50D293FF50D293FFFFFFFFFFA5E8C7FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF76DC
          ABFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8AE1B7FF50D2
          93FF50D293FF50D293FF50D293FFFFFFFFFFA5E8C7FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF5BD5
          9AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF71DAA7FF50D2
          93FF50D293FF50D293FF50D293FFFFFFFFFFA4E8C7FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF50D2
          93FFB2EBD0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC8F1DDFF50D293FF50D2
          93FF50D293FF50D293FF50D293FFFFFFFFFFA5E8C7FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF97E4BFFFD6F4E6FFDBF6E9FFA2E7C6FF53D395FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFFFFFFFFFA6E8C8FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFFFFFFFFFA6E8C8FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF92E3BCFFFFFFFFFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3
          BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3
          BCFF92E3BCFF92E3BCFF92E3BCFFFFFFFFFFA7E9C8FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF92E3BCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA8E9C9FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000D8AA7FFFD9AB
          80FFD9AB80FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD9AB80FFDAAC81FFDAAC81FFD9AB80FFD8AA7FFFD8AA7FFFD9AB
          80FFDAAC81FFDBAD82FFDDAF84FFDCAE83FFDCAE83FFDEB085FFDEB085FFDEB0
          85FFDCAE83FFD9AB80FFDAAC81FFD9AB80FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD9AB80FFDCAE83FFDAAC81FFD8AA7FFFD8AA7FFFDBAD
          82FFE0B287FFE5B78CFFE7B98EFFFACCA1FFFFEEDFFFFFFCF9FFFFFDFDFFFFF1
          E6FFFFD6B1FFE6B88DFFE6B88DFFDDAF84FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFDAAC81FFDAAC81FFD8AA7FFFD9AB80FFDBAD
          82FFE1B388FFE5B78CFFFFDFC3FFFFFFFFFFFFFFFFFFFFFDFDFFFFFDFDFFFFFF
          FFFFFFFFFFFFFFE7D2FFE6B88DFFDAAC81FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB80FFD9AB80FFD8AA7FFFD9AB80FFDCAE
          83FFE3B58AFFF7C99EFFFFFEFEFFFFFFFFFFFFFFFFFFFFD4AEFFFFD4AEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFDD2ACFFDCAE83FFD9AB80FFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB80FFD9AB80FFD8AA7FFFD9AB80FFDEB0
          85FFE3B58AFFFDE6D1FFFFFFFFFFFFFFFFFFFFFFFFFFFFD1A6FFFED0A5FFFFFF
          FFFFFFFFFFFFFFFFFFFFFDEDE1FFE3B58AFFDAAC81FFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDAAC81FFDFB1
          86FFE4B68BFFFEF5EEFFFFFFFFFFF1C398FFF5C79CFFECBE93FFEDBF94FFF2C4
          99FFF1C398FFFFFEFEFFFCF7F5FFE3B58AFFDBAD82FFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB80FFDCAE83FFE2B4
          89FFE6B88DFFFFF4EBFFFFFEFEFFFFD4AEFFFFD5AFFFEEC095FFF0C297FFFFDA
          B9FFFFDAB9FFFFFDFDFFFCF6F1FFE2B489FFDBAD82FFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB80FFD9AB80FFDAAC81FFDEB085FFE3B5
          8AFFE6B88DFFFFE8D3FFFFFFFFFFFFFFFFFFFFFFFFFFFBCDA2FFF8CA9FFFFFFF
          FFFFFFFFFFFFFFFFFFFFFBE9D8FFE1B388FFD9AB80FFD8AA7FFFD8AA7FFFD8AA
          7FFFD9AB80FFD9AB80FFD9AB80FFD9AB80FFD9AB80FFD9AB80FFD9AB80FFDAAC
          81FFDAAC81FFDBAD82FFDCAE83FFDCAE83FFDDAF84FFDFB186FFE3B58AFFE6B8
          8DFFE7B98EFFF3C59AFFFFFDFDFFFFFFFFFFFFFFFFFFFFD4AEFFFFD4AEFFFFFF
          FFFFFFFFFFFFFFFEFDFFF5C79CFFDFB186FFD8AA7FFFD8AA7FFFD8AA7FFFD9AB
          80FFDBAE83FCDDAF84FEDDAF84FEDEB085FFDFB186FFDFB186FFE0B287FFE0B2
          87FEE1B388FEE0B388FDE2B489FEE3B58AFFE3B58AFFE4B68BFFE6B88DFFE6B8
          8DFEE7B98EFFE7B98EFFFFD4AEFFFFFCFBFFFFFFFFFFFEFDFDFFFEFDFDFFFFFF
          FFFFFFFDFDFFFCD9B9FFE3B58AFFDBAD82FFD8AA7FFFD8AA7FFFD8AA7FFFD9AB
          80FFE5B78CFFF6D2ADFFF9F2EBFFF9F2ECFFFAF3EDFFFAF3EDFFFAF4EEFFFAF6
          F1FFF9F5EFFFFAF6F0FFFBF7F2FFFBF8F3FFFCF9F5FFFDFAF9FFFFFCFCFFFFFF
          FEFFFFFFFFFFFFDBBBFFE7B98EFFF3C59AFFFFE7D2FFF8F4EAFFF6F6EDFFEAEA
          D6FFF5C79CFFE3B58AFFDFB186FFD9AB80FFD8AA7FFFD8AA7FFFD8AA7FFFD9AB
          80FFE7B98EFFFAD8B3FFFFFEFDFFFFFEFEFFFFFEFEFFFFFEFEFFFFFEFEFFFFFE
          FEFFFFFFFEFFFFFEFEFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFEFEFFFFE5CFFFE8BA8FFFE7B98EFFE7B98EFFE6B88DFFE5B7
          8CFFE2B489FFDEB085FFDBAD82FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFE7B98EFFFED9B5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD1A6FFE7B98EFFE4B68BFEE1B3
          88FFDDAF84FFDAAC81FFD9AB80FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFE7B98EFFFFD9B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFC2FFE6B88DFFE1B388FEDDAF
          84FFD9AB80FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB
          80FFE7B98EFFFFD9B6FFFFFFFFFFE7B98EFFE7B98EFFFFF6F0FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFCF9FFE9BB90FFE7B98EFFFFFFFFFFFEDDC1FFE5B78CFFDFB186FFDAAC
          81FFD8AA7FFFD8AA7FFFD9AB80FFDAAC81FFD8AA7FFFD8AA7FFFD8AA7FFFD9AB
          80FFE7B98EFFFFDAB9FFFFFFFFFFE7B98EFFE7B98EFFF0C297FFFFF2E7FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5
          ECFFF5C79CFFE7B98EFFE7B98EFFFFFFFEFFFCDBBEFFE5B78CFFDCAE83FFD9AB
          80FFD8AA7FFFD9AB80FFDAAC81FFDBAD82FFD8AA7FFFD8AA7FFFD8AA7FFFD9AB
          80FFE7B98EFFFFD8B4FFFFFFFFFFE7B98EFFE8BA8FFFE7B98EFFE7B98EFFFED4
          ABFFFFF5ECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3E9FFFFD2A9FFE8BA
          8FFFE8BA8FFFE7B98EFFE7B98EFFFFFEFDFFFBD9BCFFE3B58AFFDAAC81FFD8AA
          7FFFD8AA7FFFD9AB80FFDBAD82FFDBAD82FFD8AA7FFFD8AA7FFFD8AA7FFFD9AB
          80FFE7B98EFFFFD6B1FFFFFFFFFFE7B98EFFE7B98EFFE7B98EFFE8BA8FFFE7B9
          8EFFE8BA8FFFFFEDDDFFFFFFFFFFFFFFFFFFFFF1E6FFE7B98EFFE8BA8FFFE9BB
          90FFE7B98EFFE8BA8FFFE7B98EFFFFFEFDFFF9D7B9FFE0B287FFDAAC81FFD8AA
          7FFFD8AA7FFFD8AA7FFFD9AB80FFDBAD82FFD8AA7FFFD8AA7FFFD8AA7FFFD9AB
          80FFE5B78CFFFFD8B4FFFFFFFFFFE8BA8FFFE7B98EFFE7B98EFFE7B98EFFE7B9
          8EFFE7B98EFFFFF0E2FFFFFFFFFFFFFFFFFFFFF6F0FFE9BB90FFE9BB90FFE7B9
          8EFFE8BA8FFFE7B98EFFE7B98EFFFEFDFCFFF8D3B2FFDFB186FFD9AB80FFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB80FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFE2B489FFFFD6B1FFFFFFFFFFE7B98EFFE7B98EFFE8BA8FFFE7B98EFFE7B9
          8EFFFFD5AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFC2FFE7B98EFFE7B9
          8EFFE7B98EFFE7B98EFFE7B98EFFFEFCFBFFF5D2B2FFDDAF84FFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB
          80FFE1B388FFFFD8B4FFFFFFFFFFE6B88DFFE7B98EFFE7B98EFFE7B98EFFE6B8
          8DFFFFF8F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFFE8BA8FFFE8BA
          8FFFE8BA8FFFE7B98EFFE8BA8FFFFEFCFBFFF4D0AFFFDBAD82FFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB
          80FFE0B287FFFFD8B4FFFFFFFFFFE7B98EFFE7B98EFFE7B98EFFE7B98EFFEEC0
          95FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6C89DFFE9BB
          90FFE8BA8FFFE7B98EFFE7B98EFFFEFCFAFFF3CDAAFFDBAD82FFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB
          80FFE2B489FFFFD8B4FFFFFFFFFFE8BA8FFFE8BA8FFFE7B98EFFE7B98EFFF8CA
          9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD5AFFFE8BA
          8FFFE7B98EFFE7B98EFFE7B98EFFFEFCFBFFF3CDAAFFDBAD82FFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFE7B98EFFFFDAB9FFFFFFFFFFE8BA8FFFE8BA8FFFE8BA8FFFE7B98EFFECBE
          93FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6C89DFFE7B9
          8EFFE7B98EFFE7B98EFFE7B98EFFFEFEFDFFF5CFADFFDBAD82FFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFE7B98EFFFFD8B4FFFFFFFFFFE8BA8FFFE8BA8FFFE7B98EFFE8BA8FFFE7B9
          8EFFFFE1C6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBDAFFE7B98EFFE7B9
          8EFFE7B98EFFE7B98EFFE8BA8FFFFEFDFCFFF6D1AFFFDCAE83FFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFE6B88DFFFFD8B4FFFFFFFFFFE7B98EFFE8BA8FFFE7B98EFFE7B98EFFE7B9
          8EFFE7B98EFFFCDCB8FFFFF0E2FFFFF1E6FFFDDEBFFFEABC91FFE7B98EFFE7B9
          8EFFE7B98EFFE7B98EFFE7B98EFFFEFDFCFFF6D1AFFFDCAE83FFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFE7B98EFFFFDAB9FFFFFFFFFFE8BA8FFFE7B98EFFE8BA8FFFE7B98EFFE7B9
          8EFFE7B98EFFE7B98EFFE7B98EFFE7B98EFFE8BA8FFFE7B98EFFE9BB90FFE7B9
          8EFFE7B98EFFE7B98EFFE7B98EFFFEFEFDFFF6D3B4FFDCAE83FFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFE7B98EFFFFD8B4FFFFFFFFFFFFD6B1FFFFD6B1FFFFD8B4FFFFD8B4FFFFD5
          AFFFFFD6B1FFFFD5AFFFFFD8B4FFFFD8B4FFFFD8B4FFFFD6B1FFFFD8B4FFFFD6
          B1FFFFD5AFFFFFD5AFFFFFD5AFFFFEFEFDFFF7D4B5FFDDAF84FFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFE7B98EFFFED9B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFFFAD8BAFFE0B287FFD9AB80FFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFE7B98EFFE7B98EFFE8BA8FFFE7B98EFFE7B98EFFE7B98EFFE8BA8FFFE8BA
          8FFFE7B98EFFE7B98EFFE7B98EFFE7B98EFFE7B98EFFE7B98EFFE7B98EFFE7B9
          8EFFE6B88DFFE6B88DFFE6B88DFFE6B88DFFE5B78CFFE1B388FFDCAE83FFD9AB
          80FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB
          80FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
      end>
  end
end
