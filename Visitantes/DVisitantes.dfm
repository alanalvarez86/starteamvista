object dmVisitantes: TdmVisitantes
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 479
  Width = 741
  object cdsCaseta: TZetaLookupDataSet
    Tag = 2
    Aggregates = <>
    IndexFieldNames = 'CA_CODIGO'
    Params = <>
    OnReconcileError = cdsCatalogosReconcileError
    AlAdquirirDatos = cdsCasetaAlAdquirirDatos
    AlCrearCampos = cdsCasetaAlCrearCampos
    UsaCache = True
    LookupName = 'Caseta'
    LookupDescriptionField = 'CA_NOMBRE'
    LookupKeyField = 'CA_CODIGO'
    Left = 36
    Top = 20
  end
  object cdsCortes: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsCatalogosReconcileError
    AlEnviarDatos = cdsCortesAlEnviarDatos
    AlCrearCampos = cdsCortesAlCrearCampos
    Left = 120
    Top = 20
  end
  object cdsCitas: TZetaClientDataSet
    Tag = 7
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsCatalogosReconcileError
    AlAdquirirDatos = cdsCitasAlAdquirirDatos
    AlEnviarDatos = cdsTablaAlEnviarDatos
    AlCrearCampos = cdsCitasAlCrearCampos
    Left = 328
    Top = 16
  end
  object cdsLibros: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsLibrosBeforePost
    OnCalcFields = cdsLibrosCalcFields
    OnNewRecord = cdsLibrosNewRecord
    OnReconcileError = cdsCatalogosReconcileError
    AlAdquirirDatos = cdsLibrosAlAdquirirDatos
    AlEnviarDatos = cdsTablaAlEnviarDatos
    AlCrearCampos = cdsLibrosAlCrearCampos
    AlModificar = cdsLibrosAlModificar
    Left = 328
    Top = 80
  end
  object cdsTipoAsunto: TZetaLookupDataSet
    Tag = 2
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    OnReconcileError = cdsCatalogosReconcileError
    AlAdquirirDatos = cdsCatalogoAlAdquirirDatos
    UsaCache = True
    LookupName = 'Tipo De Asunto'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = cdsGetRights
    Left = 124
    Top = 196
  end
  object cdsAnfitrion: TZetaLookupDataSet
    Tag = 2
    Aggregates = <>
    IndexFieldNames = 'AN_NUMERO'
    Params = <>
    OnReconcileError = cdsCatalogosReconcileError
    AlAdquirirDatos = cdsCatalogoAlAdquirirDatos
    AlEnviarDatos = cdsCatalogoAlEnviarDatos
    AlCrearCampos = cdsAnfitrionAlCrearCampos
    UsaCache = True
    LookupName = 'Anfitri'#243'n'
    LookupDescriptionField = 'Pretty_Anfi'
    LookupKeyField = 'AN_NUMERO'
    OnLookupDescription = cdsAnfitrionLookupDescription
    Left = 36
    Top = 132
  end
  object cdsDepto: TZetaLookupDataSet
    Tag = 4
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    OnReconcileError = cdsCatalogosReconcileError
    AlAdquirirDatos = cdsCatalogoAlAdquirirDatos
    UsaCache = True
    LookupName = 'Departamentos'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = cdsGetRights
    Left = 124
    Top = 252
  end
  object cdsAnfitrionLookup: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'AN_NUMERO'
    Params = <>
    OnReconcileError = cdsCatalogosReconcileError
    AlCrearCampos = cdsAnfitrionLookupAlCrearCampos
    UsaCache = True
    LookupName = 'Visitando A'
    LookupDescriptionField = 'Pretty_Anfi'
    LookupKeyField = 'AN_NUMERO'
    Left = 40
    Top = 192
  end
  object cdsVisitante: TZetaLookupDataSet
    Tag = 2
    Aggregates = <>
    IndexFieldNames = 'VI_NUMERO'
    Params = <>
    OnNewRecord = cdsVisitanteNewRecord
    OnReconcileError = cdsCatalogosReconcileError
    AlAdquirirDatos = cdsCatalogoAlAdquirirDatos
    AlEnviarDatos = cdsVisitanteAlEnviarDatos
    AlCrearCampos = cdsVisitanteAlCrearCampos
    UsaCache = True
    LookupName = 'Visitante'
    LookupDescriptionField = 'VI_TIPO'
    LookupKeyField = 'VI_NUMERO'
    OnLookupDescription = cdsVisitanteLookupDescription
    OnLookupSearch = cdsVisitanteLookupSearch
    Left = 124
    Top = 308
  end
  object cdsVisitanteLookup: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsCatalogosReconcileError
    AlCrearCampos = cdsVisitanteLookupAlCrearCampos
    UsaCache = True
    LookupName = 'Visitantes'
    LookupDescriptionField = 'Pretty_Visit'
    LookupKeyField = 'VI_NUMERO'
    Left = 36
    Top = 256
  end
  object cdsEmpVisitante: TZetaLookupDataSet
    Tag = 8
    Aggregates = <>
    IndexFieldNames = 'EV_NUMERO'
    Params = <>
    OnReconcileError = cdsCatalogosReconcileError
    AlAdquirirDatos = cdsEmpVisitanteAlAdquirirDatos
    AlEnviarDatos = cdsCatalogoAlEnviarDatos
    AlCrearCampos = cdsEmpVisitanteAlCrearCampos
    UsaCache = True
    LookupName = 'Empresas'
    LookupDescriptionField = 'EV_NOMBRE'
    LookupKeyField = 'EV_NUMERO'
    OnGetRights = cdsGetRights
    Left = 208
    Top = 16
  end
  object cdsTipoID: TZetaLookupDataSet
    Tag = 1
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    OnReconcileError = cdsCatalogosReconcileError
    AlAdquirirDatos = cdsCatalogoAlAdquirirDatos
    UsaCache = True
    LookupName = 'Tipo De Identificaci'#243'n'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = cdsGetRights
    Left = 120
    Top = 132
  end
  object cdsTipoCarro: TZetaLookupDataSet
    Tag = 1
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    OnReconcileError = cdsCatalogosReconcileError
    AlAdquirirDatos = cdsCatalogoAlAdquirirDatos
    UsaCache = True
    LookupName = 'Tipo De Veh'#237'culo'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = cdsGetRights
    Left = 120
    Top = 76
  end
  object cdsTipoVisita: TZetaLookupDataSet
    Tag = 3
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    OnReconcileError = cdsCatalogosReconcileError
    AlAdquirirDatos = cdsCatalogoAlAdquirirDatos
    UsaCache = True
    LookupName = 'Tipo De Visitante'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 36
    Top = 76
  end
  object cdsLibrosLookup: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsCatalogosReconcileError
    AlAdquirirDatos = cdsLibrosLookupAlAdquirirDatos
    Left = 328
    Top = 136
  end
  object cdsLibrosSalidas: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnNewRecord = cdsLibrosNewRecord
    OnReconcileError = cdsCatalogosReconcileError
    AlAdquirirDatos = cdsLibrosSalidasAlAdquirirDatos
    AlEnviarDatos = cdsTablaAlEnviarDatos
    AlCrearCampos = cdsLibrosAlCrearCampos
    Left = 328
    Top = 208
  end
  object cdsCorteDetail: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsCatalogosReconcileError
    AlEnviarDatos = cdsCortesAlEnviarDatos
    AlCrearCampos = cdsCortesAlCrearCampos
    Left = 208
    Top = 76
  end
  object cdsLibroBusqueda: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsCatalogosReconcileError
    AlCrearCampos = cdsLibrosAlCrearCampos
    Left = 320
    Top = 328
  end
  object cdsAlertas: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsAlertasAlAdquirirDatos
    Left = 328
    Top = 272
  end
  object cdsCompaniaLookup: TZetaLookupDataSet
    Tag = 8
    Aggregates = <>
    Filtered = True
    IndexFieldNames = 'EV_NOMBRE'
    Params = <>
    OnReconcileError = cdsCatalogosReconcileError
    AlCrearCampos = cdsEmpVisitanteAlCrearCampos
    UsaCache = True
    LookupName = 'compa'#241#237'as'
    LookupDescriptionField = 'EV_NOMBRE'
    LookupKeyField = 'EV_NUMERO'
    OnGetRights = cdsGetRights
    Left = 40
    Top = 312
  end
  object cdsMuestraFoto: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    Left = 208
    Top = 136
  end
  object cdsSQL: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnCalcFields = cdsSQLCalcFields
    OnReconcileError = cdsCatalogosReconcileError
    AlAdquirirDatos = cdsSQLAlAdquirirDatos
    AlCrearCampos = cdsSQLAlCrearCampos
    Left = 406
    Top = 16
  end
  object cdsTotales: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsTotalesReconcileError
    AlAdquirirDatos = cdsTotalesAlAdquirirDatos
    AlCrearCampos = cdsTotalesAlCrearCampos
    Left = 407
    Top = 72
  end
  object cdsCitasSQL: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsTotalesReconcileError
    AlAdquirirDatos = cdsCitasSQLAlAdquirirDatos
    AlCrearCampos = cdsCitasSQLAlCrearCampos
    Left = 406
    Top = 128
  end
end
