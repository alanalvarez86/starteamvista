unit FBusquedaLibro;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, StdCtrls, Buttons, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  Db, ZetaKeyCombo, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDBData, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGridLevel, cxGridCustomTableView, cxGridCardView,
  cxClasses, cxGridCustomView, cxGridCustomLayoutView, cxGrid, Vcl.ImgList,
  ZBaseDlgModal, Vcl.Menus, cxButtons, cxContainer, cxCheckBox,StrUtils;

type
  TBusquedaLibro = class(TZetaDlgModal_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    eBusca: TEdit;
    btnBuscar: TcxButton;
    DataSource: TDataSource;
    FBusquedaLibro: TZetaDBGrid;
    cxGrid2: TcxGrid;
    cxGridCardView1: TcxGridCardView;
    cxGridCardViewRow1: TcxGridCardViewRow;
    cxGridLevel1: TcxGridLevel;
    cxGrid2DBBandedTableView1: TcxGridDBBandedTableView;
    LI_NOMBRE: TcxGridDBBandedColumn;
    LI_EMPRESA: TcxGridDBBandedColumn;
    LI_ANFITR: TcxGridDBBandedColumn;
    LI_DEPTO: TcxGridDBBandedColumn;
    LI_ENT_HOR: TcxGridDBBandedColumn;
    LI_ENT_FEC: TcxGridDBBandedColumn;
    LI_SAL_HOR: TcxGridDBBandedColumn;
    LI_SAL_FEC: TcxGridDBBandedColumn;
    SmallImageList: TcxImageList;
    cxStyleRepository: TcxStyleRepository;
    cxStyle1: TcxStyle;
    chkHistorialVisitas: TcxCheckBox;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle2: TcxStyle;
    cbTipoBusqueda: TZetaKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure FBusquedaLibroDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure NoBorderEnTopCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure NoBorderEnBottomCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure eBuscaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxGrid2DBBandedTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure chkHistorialVisitasPropertiesChange(Sender: TObject);
  private
    procedure PreparaFiltros;
    {$ifdef TEST_COMPLETE} function GetInformacionGridBusquedaGral: String; {$endif}
    { Private declarations }

  published
    { Published declarations }
    {$ifdef TEST_COMPLETE} property InformacionGridBusquedaGral: String read GetInformacionGridBusquedaGral;{$endif}
    function TextoNecesitaMascara(bandedColumn: string):boolean;
  public
    { Public declarations }
  end;

var
  BusquedaLibro: TBusquedaLibro;

implementation
uses
    DVisitantes,
    ZetaDialogo,
    ZetaCommonClasses,
    ZetaCommonTools,
    ZetaCommonLists,
    cxTextEdit;

{$R *.DFM}

type
  TcxViewInfoAcess = class(TcxGridTableDataCellViewInfo);
  TcxPainterAccess = class(TcxGridTableDataCellPainter);

procedure TBusquedaLibro.FormCreate(Sender: TObject);
begin
     inherited;
     cbTipoBusqueda.ItemIndex := 0;
     DataSource.Dataset := dmVisitantes.cdsLibroBusqueda;
     HelpContext := H_VIS_BUS_VISITANTE;
     //cxGrid2DBBandedTableView1.ApplyBestFit();
     cxGrid2DBBandedTableView1.DataController.DataModeController.GridMode:= True;
end;

procedure TBusquedaLibro.btnBuscarClick(Sender: TObject);
begin
     inherited;
     if (eBusca.Text <> VACIO) or (sender = btnBuscar)then
        PreparaFiltros;
end;

{procedure TBusquedaLibro.PreparaFiltros;
 var
    sCampo: string;
begin
     case eBusquedaLibro( cbTipoBusqueda.ItemIndex ) of
          blNombre: sCampo := 'LI_NOMBRE' ;
          blEmpresa: sCampo := 'LI_EMPRESA' ;
          blVisitaA: sCampo := 'LI_ANFITR' ;
          blDepartamento: sCampo := 'LI_DEPTO' ;
          blPlacas: sCampo := 'LI_CAR_PLA' ;
          blGafete: sCampo := 'LI_GAFETE'
     end;

     with dmVisitantes.cdsLibros do
     begin
          //frescar;
          Filtered := TRUE;
          Filter := Format( 'Upper(%s) LIKE Upper(%s)' , [sCampo, EntreComillas( '%'+ eBusca.Text +'%' ) ] );
     end;
end;}


procedure TBusquedaLibro.PreparaFiltros;
var
    sFiltro, sCampo: string;

begin
     case eBusquedaLibro( cbTipoBusqueda.ItemIndex ) of
          blNombre: sCampo := 'LI_NOMBRE' ;
          blEmpresa: sCampo := 'LI_EMPRESA' ;
          blVisitaA: sCampo := 'LI_ANFITR' ;
          blDepartamento: sCampo := 'LI_DEPTO' ;
          blPlacas: sCampo := 'LI_CAR_PLA' ;
          blGafete: sCampo := 'LI_GAFETE'
      end;
      sFiltro := Format( 'Upper(%s) LIKE Upper(%s)' , [sCampo, EntreComillas( '%'+ eBusca.Text +'%' ) ] );

      if not chkHistorialVisitas.Checked then
         sFiltro := sFiltro + ' and LI_ENT_FEC = ' + quotedstr(DateToStrSQL(Date));

      dmVisitantes.BuscaLibro( NullDateTime, NullDateTime, sFiltro );

      if ( dmVisitantes.cdsLibroBusqueda.IsEmpty ) then
      begin
           ZetaDialogo.ZInformation('Información','No Existe Información Con Los Datos Indicados', 0);
      end;
end;


function TBusquedaLibro.TextoNecesitaMascara(bandedColumn: string): boolean;
begin
  if (bandedColumn = 'Entrada') then
      Result := true
    else if (bandedColumn = 'Salida') then
      Result := true
    else
      Result := false;
end;

procedure TBusquedaLibro.chkHistorialVisitasPropertiesChange(Sender: TObject);
begin
  inherited;
  PreparaFiltros;
end;

procedure TBusquedaLibro.cxGrid2DBBandedTableView1CellDblClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  //if eBusca.Text <> VACIO then
     //OK_DevEx.inh
end;

procedure TBusquedaLibro.DataSourceDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  //cxGrid2DBBandedTableView1.ApplyBestFit();
end;

procedure TBusquedaLibro.eBuscaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if eBusca.Text <> VACIO then
  if key = 13 then
    PreparaFiltros;
end;

procedure TBusquedaLibro.FBusquedaLibroDblClick(Sender: TObject);
begin
     inherited;
     Close;
     ModalResult := mrOk;
end;

procedure TBusquedaLibro.FormShow(Sender: TObject);
begin
   {***(am):Trabaja CON GridMode***}
  if cxGrid2DBBandedTableView1.DataController.DataModeController.GridMode then
    cxGrid2DBBandedTableView1.OptionsCustomize.ColumnFiltering := False;

  //Desactiva la posibilidad de agrupar
  cxGrid2DBBandedTableView1.OptionsCustomize.ColumnGrouping := False;
  //Esconde la caja de agrupamiento
  cxGrid2DBBandedTableView1.OptionsView.GroupByBox := False;
  //Para que nunca muestre el filterbox inferior
  cxGrid2DBBandedTableView1.FilterBox.Visible := fvNever;
  //Para que no aparezca el Custom Dialog
  cxGrid2DBBandedTableView1.FilterBox.CustomizeDialog := False;
  //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
  //cxGrid2DBBandedTableView1.ApplyBestFit();
     eBusca.Text := VACIO;
     with dmVisitantes.cdsLibroBusqueda do
     begin
          if Active then
             EmptyDataSet;
     end;
  eBusca.SetFocus;
  chkHistorialVisitas.Checked := false;
end;


procedure TBusquedaLibro.NoBorderEnBottomCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  ARect: TRect;
begin
  with AViewInfo do
  begin
      AViewInfo.Borders := [];
      ARect := Bounds;
      ACanvas.FillRect(ARect);
      if (AViewInfo.Item <> LI_ENT_HOR) and (AViewInfo.Item <> LI_SAL_HOR) then
        ACanvas.DrawTexT(Text, TextAreaBounds, cxAlignVCenter);


      ADone := True;
  end;
{    if (AViewInfo.Item = LI_CAR_PLA)then
        PaintIcons(Sender,ACanvas,AViewInfo, ADone);
 }
  ACanvas.DrawComplexFrame(ARect, AViewInfo.BorderColor[bRight], AViewInfo.BorderColor[bLeft], [bRight, bBottom], 1);

end;

procedure TBusquedaLibro.NoBorderEnTopCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  ARect: TRect;
 s : string;
 sTexto: String;
 begin
  sTexto:= AnsiUpperCase ( AViewInfo.Text );
  if AnsiContainsStr ( UpperCase(sTexto), UpperCase(eBusca.Text) ) then
  with ACanvas do
  begin
     //Font.Style:= [fsUnderline];
     //Brush.Color := RGB(245,208,169);
  end;

  s := upperCase(AViewInfo.GridRecord.DisplayTexts[cxGrid2DBBandedTableView1.Columns[1].Index ]);
  with AViewInfo do
  begin
      AViewInfo.Borders := [];
      ARect := Bounds;
      ACanvas.FillRect(ARect);
      ACanvas.DrawTexT(Text, TextAreaBounds, cxAlignVCenter);
      ADone := True;
  end;
  if (AViewInfo.Item <> LI_ENT_HOR) and (AViewInfo.Item <> LI_SAL_HOR) then
    ACanvas.DrawComplexFrame(ARect, AViewInfo.BorderColor[bRight], AViewInfo.BorderColor[bLeft], [bRight, bBottom], 1);

  if (AViewInfo.Item = LI_ENT_HOR) or (AViewInfo.Item = LI_SAL_HOR) then
    ACanvas.DrawComplexFrame(ARect, AViewInfo.BorderColor[bRight], AViewInfo.BorderColor[bLeft], [bBottom], 1);
end;

{$ifdef TEST_COMPLETE}
function TBusquedaLibro.GetInformacionGridBusquedaGral: String;
var
  sSalHor:string;
begin
     Result := '';
      with DataSource.DataSet do
      begin
           first;
           while not eof do
           begin
                sSalHor:='';
                if strLleno(fieldByName('LI_SAL_HOR').AsString) then
                  sSalHor := fieldByName('LI_SAL_HOR').AsString;
                result := result + Trim( fieldByName('LI_NOMBRE').AsString ) + K_PIPE +
                                    Trim( fieldByName('LI_EMPRESA').AsString ) + K_PIPE +
                                    Trim( fieldByName('LI_ANFITR').AsString )+ K_PIPE +
                                    Trim( fieldByName('LI_DEPTO').AsString )+ K_PIPE +
                                    Trim( fieldByName('LI_ENT_HOR').AsString )+ ' ' +
                                    Trim( fieldByName('LI_ENT_FEC').AsString ) + K_PIPE;
                                    if strLleno(sSalHor) then
                                    begin
                                         result := result + Trim( fieldByName('LI_SAL_HOR').AsString )+ ' ' +
                                         Trim( fieldByName('LI_SAL_FEC').AsString ) + K_PIPE + CR_LF;
                                    end
                                    else
                                        result := result + K_PIPE + CR_LF;
                Next;
          end;
          Result := result + CR_LF;
      end;
end;
{$endif}

end.
