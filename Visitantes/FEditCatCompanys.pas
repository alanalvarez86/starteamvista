unit FEditCatCompanys;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask, ZetaNumero,
  ZetaKeyCombo, ZetaSmartLists, ZBaseEdicion, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, TressMorado2013, cxControls,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList,
  cxNavigator, cxDBNavigator, cxButtons;

type
  TEditCatCompanys = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    lblNombre: TLabel;
    EV_NOMBRE: TDBEdit;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
   EditCatCompanys: TEditCatCompanys;

implementation

uses dVisitantes,
     ZAccesosTress,
     ZetaCommonClasses,
     FRegistroEntradaVisi,
     ZVisitBusqueda_DevEx;

{$R *.DFM}

procedure TEditCatCompanys.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_CAT_COMPANYS;
     HelpContext:= H_VISMGR_EDIT_COMPANIAS;
     //FirstControl := VI_NUMERO;
     //TipoValorActivo1 := stExpediente;
end;

procedure TEditCatCompanys.FormShow(Sender: TObject);
begin
     inherited;
     EditCatCompanys.dxBarButton_AgregarBtnClick(Self);
     EV_NOMBRE.SetFocus;
     self.Height := 150;
     if FRegistroEntradaVisit.LI_EMPRESA.Text <> VACIO then
        EV_NOMBRE.Text := FRegistroEntradaVisit.LI_EMPRESA.Text;
end;

procedure TEditCatCompanys.OK_DevExClick(Sender: TObject);
var
   sNombreEmpresaNueva:string;
begin
     sNombreEmpresaNueva:=EV_NOMBRE.Text;
     if EV_NOMBRE.Text = VACIO then
        DatabaseError('El nombre no puede quedar vacio.')
     else
     begin
          with dmVisitantes.cdsEmpVisitante do
          begin
               Enviar;
               Refrescar;
               Locate('EV_NOMBRE', sNombreEmpresaNueva, []);
               with FRegistroEntradaVisit do
               begin
                    dblCompania.Llave := FieldByName('EV_NUMERO').AsString;
                    LI_EMPRESA.Text := dblCompania.Descripcion;
                    FRegistroEntradaVisit.VISITAA.SetFocus;
                    FRegistroEntradaVisit.LI_ANFITR.SetFocus;
               end;
          end;
     end;
     close;
end;

procedure TEditCatCompanys.Cancelar_DevExClick(Sender: TObject);
begin
     inherited;
     close;
end;

procedure TEditCatCompanys.Connect;
begin
     with dmVisitantes do
     begin
          DataSource.DataSet:= cdsEmpVisitante;
     end;
end;

end.
