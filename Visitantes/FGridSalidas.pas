unit FGridSalidas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, Grids, DBGrids, Db, StdCtrls, Buttons, ExtCtrls,
  ZetaDBGrid, ZBaseDlgModal, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, dxSkinsCore, TressMorado2013, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  Vcl.ImgList, cxButtons;

type
  TGridSalidas = class(TZetaDlgModal_DevEx)
    DataSource: TDataSource;
    GridLibro: TZetaDBGrid;
    Panel1: TPanel;
    lbBusca: TLabel;
    EBuscaVisitante: TEdit;
    BBusca: TcxButton;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    LI_FOLIO: TcxGridDBColumn;
    LI_ENT_CAS: TcxGridDBColumn;
    LI_ENT_HOR: TcxGridDBColumn;
    LI_NOMBRE: TcxGridDBColumn;
    LI_EMPRESA: TcxGridDBColumn;
    LI_ANFITR: TcxGridDBColumn;
    LI_DEPTO: TcxGridDBColumn;
    US_ENT_LIB: TcxGridDBColumn;
    LI_SAL_HOR: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure GridLibroDblClick(Sender: TObject);
    procedure GridLibroDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure EBuscaVisitanteChange(Sender: TObject);
    procedure BBuscaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxGrid1DBTableView1CellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGrid1DBTableView1DataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure cxGrid1DBTableView1CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FPFolio:integer;
    function GetFolio: integer;
    procedure FiltraVisitante;
    procedure LimpiaGrid;
    function TiempoExcede(sLiEntHor:string; sLiSalHor:string):boolean;
    {$ifdef TEST_COMPLETE} function GetInformacionGridSalidas: String;{$endif}
  published
    { Published declarations }
    {$ifdef TEST_COMPLETE} property InformacionGridSalidas: String read GetInformacionGridSalidas;{$endif}

  public
    { Public declarations }
    property Folio: integer read GetFolio;

    property iPFolio:integer read FPFolio write FPFolio;

  end;

var
  GridSalidas: TGridSalidas;

implementation
uses
    DVisitantes,
    ZetaCommonClasses,
    FTressShell,
    ZetaCommonTools;

{$R *.DFM}

procedure TGridSalidas.FormShow(Sender: TObject);
begin
  inherited;
  DataSource.DataSet := dmVisitantes.cdsLibrosSalidas;
  HelpContext := H_VIS_BUSQ_LIBRO;

  //Desactiva la posibilidad de agrupar
  cxGrid1DBTableView1.OptionsCustomize.ColumnGrouping := False;
  //Esconde la caja de agrupamiento
  cxGrid1DBTableView1.OptionsView.GroupByBox := False;
  //Para que nunca muestre el filterbox inferior
  cxGrid1DBTableView1.FilterBox.Visible := fvNever;
  //Para que no aparezca el Custom Dialog
  cxGrid1DBTableView1.FilterBox.CustomizeDialog := False;
  //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
  cxGrid1DBTableView1.ApplyBestFit();
  EBuscaVisitante.SetFocus;
end;

function TGridSalidas.GetFolio: integer;
begin
     with dmVisitantes.cdsLibrosSalidas do begin
        Result := FieldByName('LI_FOLIO').AsInteger;
     end;
end;
{$ifdef TEST_COMPLETE}
function TGridSalidas.GetInformacionGridSalidas: String;
const
  K_CAMPOS_GRID = '|LI_FOLIO| |LI_ENT_CAS| |LI_ENT_HOR| |LI_NOMBRE| |LI_EMPRESA| |LI_ANFITR| |LI_DEPTO| |US_ENT_LIB|';
begin
          with TressShell do
          begin
               result := '';
               result := GetDatosGridTestComplete(DataSource.DataSet, K_CAMPOS_GRID);
          end;
end;
{$endif}

procedure TGridSalidas.GridLibroDblClick(Sender: TObject);
begin
     inherited;
     Close;
     ModalResult := mrOk;
end;

procedure TGridSalidas.GridLibroDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
 var
    oColor: TColor;
begin
     inherited;
     if dmVisitantes.AlertasEnabled then
     begin
          with GridLibro do
          begin
               oColor := Canvas.Font.Color;
               if  dmVisitantes.TiempoExcedido( DataSource.Dataset ) then
                  Canvas.Font.Color := clRed
               else
                   Canvas.Font.Color := oColor;

               DefaultDrawColumnCell( Rect, DataCol, Column, State );
          end;
     end;
end;

procedure TGridSalidas.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;
  FPFolio := StrToInt(cxGrid1DBTableView1.Controller.FocusedRow.Values[cxGrid1DBTableView1.Columns[0].Index]);
  Close;
  ModalResult := mrOk;

end;

procedure TGridSalidas.cxGrid1DBTableView1CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
    oColor: TColor;
begin
     inherited;
     if dmVisitantes.AlertasEnabled then
     begin
          with cxGrid1DBTableView1.DataController do
          begin
               if  TiempoExcede(AViewInfo.GridRecord.Values[2], AViewInfo.GridRecord.Values[8]  ) then
                  ACanvas.Font.Color := RGB(194, 0, 0)
               else
                   ACanvas.Font.Color := clBlack;

               //DefaultDrawColumnCell( Rect, DataCol, Column, State );
          end;
     end;

end;

procedure TGridSalidas.cxGrid1DBTableView1DataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
  AIndex: Integer;
begin
  inherited;
  AIndex := AValueList.FindItemByKind(fviCustom);
    if AIndex <> -1 then
      AValueList.Delete(AIndex);
end;

procedure TGridSalidas.EBuscaVisitanteChange(Sender: TObject);
begin
     inherited;
     BBusca.Down := Length(EBuscaVisitante.Text)>0;
     FiltraVisitante;
end;

procedure TGridSalidas.FiltraVisitante;
begin
     with dmVisitantes.cdsLibrosSalidas do
     begin
          Filtered := BBusca.Down;
          Filter := 'UPPER(LI_NOMBRE) LIKE '+ chr(39)+'%'+ UpperCase(EBuscaVisitante.Text) + '%' + chr(39);
     end;
     LimpiaGrid;
end;

procedure TGridSalidas.LimpiaGrid;
begin
     GridLibro.SelectedRows.Clear;
     if dmVisitantes.cdsLibrosSalidas.Active then GridLibro.SelectedRows.Refresh;
end;

procedure TGridSalidas.OK_DevExClick(Sender: TObject);
begin
  inherited;
  FPFolio := StrToInt(cxGrid1DBTableView1.Controller.FocusedRow.Values[cxGrid1DBTableView1.Columns[0].Index]);
end;

function TGridSalidas.TiempoExcede(sLiEntHor, sLiSalHor: string): boolean;
begin
  if (sLiEntHor = vacio) and (sLiSalHor = vacio) then
    exit;
  with dmVisitantes do begin
  Result := NOT AlertasEnabled;
     if NOT Result then
     begin
       Result := StrVacio( sLiSalHor ) AND
                 (( aMinutos(HoraAsStr(Time)) - aMinutos(sLiEntHor )) > TiempoMinimo );
     end;
  end;
end;

procedure TGridSalidas.BBuscaClick(Sender: TObject);
begin
     inherited;
     FiltraVisitante;
end;

procedure TGridSalidas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     EBuscaVisitante.Text := VACIO;
end;

end.
