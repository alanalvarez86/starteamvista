inherited EscogeCaseta: TEscogeCaseta
  Left = 482
  Top = 273
  Caption = 'Seleccione una caseta'
  ClientHeight = 198
  ClientWidth = 385
  OldCreateOrder = True
  OnCreate = FormCreate
  ExplicitWidth = 391
  ExplicitHeight = 232
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 162
    Width = 385
    ExplicitLeft = -32
    ExplicitTop = 154
    ExplicitWidth = 385
    inherited OK_DevEx: TcxButton
      Left = 218
      ExplicitLeft = 468
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 298
      ExplicitLeft = 548
    end
  end
  object cxGrid1: TcxGrid [1]
    Left = 0
    Top = 0
    Width = 385
    Height = 162
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ExplicitWidth = 635
    ExplicitHeight = 295
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnDblClick = cxGrid1DBTableView1DblClick
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dsCaseta
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.UnselectFocusedRecordOnExit = False
      OptionsView.GroupByBox = False
      OptionsView.Header = False
      object CA_NOMBRE: TcxGridDBColumn
        DataBinding.FieldName = 'CA_NOMBRE'
        OnCustomDrawCell = CA_NOMBRECustomDrawCell
        Styles.Content = cxStyle3
        Width = 252
      end
      object CA_ABIERTA: TcxGridDBColumn
        DataBinding.FieldName = 'CA_ABIERTA'
        OnCustomDrawCell = CA_ABIERTACustomDrawCell
        Styles.Content = cxStyle3
        Width = 129
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 1573016
  end
  object dsCaseta: TDataSource
    Left = 16
    Top = 24
  end
  object cxStyleRepository2: TcxStyleRepository
    Left = 267
    Top = 16
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svFont, svTextColor]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clGreen
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svFont, svTextColor]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clRed
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
    end
  end
end
