unit FCierraCorte;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, StdCtrls, DBCtrls, ZetaDBTextBox, Buttons, ExtCtrls, Db,
  ZBaseDlgModal, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  dxSkinsCore, TressMorado2013, Vcl.ImgList, cxButtons;

type
  TCierraCorte = class(TZetaDlgModal_DevEx)
    lblCaseta: TLabel;
    CA_NOMBRE: TZetaDBTextBox;
    GroupBox2: TGroupBox;
    lblFechaIni: TLabel;
    lblHoraIni: TLabel;
    CO_INI_FEC: TZetaDBTextBox;
    CO_INI_HOR: TZetaDBTextBox;
    gbTerminaCorte: TGroupBox;
    lblFechaFin: TLabel;
    lblHoraFin: TLabel;
    ztbFecha_Fin: TZetaDBTextBox;
    ztbHora_Fin: TZetaDBTextBox;
    GroupBox1: TGroupBox;
    lblIniCorte: TLabel;
    lblFinCorte: TLabel;
    US_NOMBRE: TZetaDBTextBox;
    ztbTermina_corte: TZetaDBTextBox;
    gbObserva: TGroupBox;
    CO_OBSERVA: TDBMemo;
    DataSource: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CierraCorte: TCierraCorte;

implementation
uses
    DVisitantes,
    ZetaCommonClasses;

{$R *.DFM}

procedure TCierraCorte.FormShow(Sender: TObject);
begin
     inherited;
     with dmVisitantes do
     begin
          DataSource.Dataset := cdsCortes;
          CO_OBSERVA.SetFocus;
     end;

end;

procedure TCierraCorte.OKClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     with dmVisitantes.cdsCortes do
     begin
          with Screen do
          begin
               oCursor := Cursor;
               Cursor := crHourglass;
               try
                  Enviar;
               finally
                      Cursor := oCursor;
               end;
          end;
     end;
end;

procedure TCierraCorte.OK_DevExClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     with dmVisitantes.cdsCortes do
     begin
          with Screen do
          begin
               oCursor := Cursor;
               Cursor := crHourglass;
               try
                  dmVisitantes.cdsCatalogoAlEnviarDatos(dmVisitantes.cdsCaseta);
                  Enviar;
               finally
                      Cursor := oCursor;
               end;
          end;
     end;

end;

procedure TCierraCorte.CancelarClick(Sender: TObject);
begin
     inherited;
     dmVisitantes.cdsCortes.Cancel;
end;

procedure TCierraCorte.Cancelar_DevExClick(Sender: TObject);
begin
     inherited;
     dmVisitantes.cdsCortes.Cancel;
     dmVisitantes.cdsCaseta.Cancel;
end;

procedure TCierraCorte.FormCreate(Sender: TObject);
begin
  inherited;
     HelpContext := H_VIS_CIERRE_CORTE;
end;

end.
