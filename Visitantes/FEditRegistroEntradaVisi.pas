unit FRegistroEntradaVisi;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, TressMorado2013,
  Vcl.Menus, cxButtons, Data.DB, Vcl.ImgList, Vcl.StdCtrls, cxTextEdit, cxMemo,
  cxDBEdit, Vcl.DBCtrls, imageenview, ieview, imageen, ZetaFecha, ZetaKeyCombo,
  Vcl.Mask, dxGDIPlusClasses, Vcl.ExtCtrls, ZetaKeyLookup_DevEx, ZVisitantesTools,
  cxMaskEdit, cxDropDownEdit;



type
  TFRegistroEntradaVisit = class(TForm)
    Panel2: TPanel;
    lblApePat: TLabel;
    lblApeMat: TLabel;
    lblSexo: TLabel;
    lblFecNac: TLabel;
    Label1: TLabel;
    lblNacion: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    icoAuto: TImage;
    lbIcoAuto: TLabel;
    icoObserva: TImage;
    lbIcoObserva: TLabel;
    icoIdenti: TImage;
    lbIcoIdenti: TLabel;
    lbId: TLabel;
    liGafete: TLabel;
    liTipoVehiculo: TLabel;
    lbCarPla: TLabel;
    lbCarDesc: TLabel;
    lbCarEst: TLabel;
    lbIdentificacion: TLabel;
    lbTexto1: TLabel;
    lbTexto2: TLabel;
    lbTexto3: TLabel;
    lbObserva1: TLabel;
    lbObserva2: TLabel;
    Label24: TLabel;
    VI_FEC_NAC: TZetaDBFecha;
    VI_MIGRA: TEdit;
    Foto: TImageEn;
    PanelBotones: TPanel;
    cxImageList24_PanelBotones: TcxImageList;
    dsBuscaVisita: TDataSource;
    dsBuscaVisitandoA: TDataSource;
    dsTipoVisitante: TDataSource;
    dsVehiculo: TDataSource;
    dsAsunto: TDataSource;
    DataSource: TDataSource;
    dsBuscaCompania: TDataSource;
    dsTipoID: TDataSource;
    dsVisita: TDataSource;
    Cancelar_DevEx: TcxButton;
    LI_CAR_PLA: TDBEdit;
    LI_CAR_DES: TDBEdit;
    LI_CAR_EST: TDBEdit;
    LI_TEXTO1: TDBEdit;
    LI_TEXTO2: TDBEdit;
    LI_TEXTO3: TDBEdit;
    VISITAA: TZetaKeyLookup_DevEx;
    VI_NOMBRES: TEdit;
    VI_APE_PAT: TEdit;
    VI_APE_MAT: TEdit;
    Label3: TLabel;
    cbTipoVisitante: TcxComboBox;
    cbSexo: TcxComboBox;
    dblCompania: TZetaKeyLookup_DevEx;
    dsSQL: TDataSource;
    Image1: TImage;
    cmbTipoAsunto: TcxComboBox;
    VI_NACIONALIDAD: TEdit;
    bAgregaVisitante: TcxButton;
    bAgregaEmpre: TcxButton;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    cmbDriverFoto: TComboBox;
    btnTomaFoto: TcxButton;
    LI_ID: TDBEdit;
    LI_GAFETE: TDBEdit;
    cmbDEPTO: TcxComboBox;
    LI_OBSERVA: TcxMemo;
    cbTipoId: TcxComboBox;
    cbTipoAuto: TcxComboBox;
    bOk: TcxButton;
    btnOtro: TcxButton;
    Label2: TLabel;
    cxImageList32: TcxImageList;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnTomaFotoClick(Sender: TObject);
    procedure cmbDriverFotoChange(Sender: TObject);
    procedure FotoDShowNewFrame(Sender: TObject);
    procedure bAgregaEmpreClick(Sender: TObject);
    procedure bAgregaVisitanteClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure VISITAAExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure bOkClick(Sender: TObject);
    procedure btnOtroClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    lVaciosVisA : Boolean;
    iVisitNumero:integer;
    arTiposVisitantes: array of string;
    arTiposAsuntos: array of string;
    arDeptos: array of string;
    arTipoId: array of string;
    arTipoAuto: array of string;
    FVisitaAgregada: Boolean;
    FCompaniaAgregada: Boolean;
    FAnfitrionAgregada: Boolean;
    FReejecutar: Boolean;
    FTomoFoto: Boolean;
    function ExisteVisitandoA: Boolean;
    function ExisteVisitante: Boolean;
    function ExisteCompania: Boolean;
    function VisitanteConEmpresa: Boolean;
    procedure AgregarCompania;
    procedure AgregarVisitante;
    procedure AgregarAnfitrion;
    procedure connect;
    procedure AntesDeEnviar;
    function EnviarEntrada:boolean;
    procedure Ejecutar;
    procedure TerminarEjecucion;
    procedure VerificaCamposNecesarios;
    procedure AjustaTamanioForma;
    procedure SetIVisiNumero(const Value: integer);
    procedure LlenaDatosVisitante;
    procedure LlenaComboTipoVisitante;
    procedure LlenaComboAsunto;
    procedure LlenaComboTipoId;
    procedure LlenaComboDepto;
    procedure LlenaComboTipoAuto;
    function GetTipoVisitante(sTipoVisi:string):integer;
    procedure SetControls(lHabilita: Boolean);
    procedure ManejaDriverFoto;
    function DriverManager:boolean;
    procedure MuestraImagenFoto;
    procedure ShowVideoFormats;
    procedure ConectaCamar;
    procedure Disconnect;
    procedure ConectaCamaraTomaFoto;
    procedure LimpiaControlesEntrada;
    function GetEmpInsertada: boolean;
    procedure SetEmpInsertada(const Value: boolean);
    procedure LlenaCombos;
    procedure LlenaComboDs(cmbName:TcxComboBox; ds:TDataSet; ar:array of string);
    procedure IniciaControlesVisitante;
    procedure IniciaControlesNuevoVisitante;
    procedure IniciaCaptura;
    function ValidaCamposEnt:boolean;
    function GetSTRDepto(sDepto:string):integer;


    procedure _AlCancelar(Sender: TObject; var lOk: Boolean);
    procedure _AlEjecutar(Sender: TObject; var lOk: Boolean);
    function CancelarWizard: Boolean;
    function EjecutarWizard: Boolean;
    procedure TerminarWizard;
  public
    { Public declarations }
    property iVisiNumero: integer read iVisitNumero write SetIVisiNumero;
    property Reejecutar: Boolean read FReejecutar write FReejecutar;
    property TomoFoto: Boolean read FTomoFoto  write FTomoFoto;

  end;
var
  FRegistroEntradaVisit: TFRegistroEntradaVisit;

implementation
uses
    DVisitantes,
    ZetaCommonClasses,
    ZetaDialogo,
    DGlobal,
    ZGlobalTress,
    FToolsImageEn,
    ZAccesosMGR,
    ZAccesosTress,
    hyiedefs,
    FEditCatCompanys,
    FEditCatAnfitrion,
    ZetaCommonLists,
    ZetaCommonTools,
    FBusquedaVisit;
{$R *.dfm}

procedure TFRegistroEntradaVisit.AgregarAnfitrion;
begin
    if NOT ExisteVisitandoA then
     begin
          if StrLleno( VI_NOMBRES.Text ) OR
             StrLleno( VISITAA.Descripcion ) then
          begin

               with dmVisitantes.cdsAnfitrion do
               begin
                    Agregar;
                    FieldByName('AN_APE_PAT').AsString := VI_APE_PAT.Text;
                    FieldByName('AN_APE_MAT').AsString := VI_APE_MAT.Text;
                    FieldByName('AN_NOMBRES').AsString := VI_NOMBRES.Text;
                    DisableControls;
                    try
                       Enviar;
                       Refrescar;
                       IndexFieldNames := 'AN_NUMERO';
                       Last;
                    finally
                           EnableControls;
                    end;
                    //dmVisitantes.cdsAnfitrionLookup.Last;
                    FAnfitrionAgregada := TRUE;
               end;
          end;
     end;
end;

procedure TFRegistroEntradaVisit.AgregarCompania;
begin
   if NOT ExisteCompania then
     begin
          with dmVisitantes.cdsEmpVisitante do
          begin
               Agregar;
               FieldByName('EV_NOMBRE').AsString := dblCompania.Llave;
               Enviar;
               Refrescar;
               dmVisitantes.cdsCompaniaLookup.Last;
               FCompaniaAgregada := TRUE;
          end;
     end;
end;

procedure TFRegistroEntradaVisit.AgregarVisitante;
begin
    if NOT ExisteVisitante then
     begin
          with dmVisitantes do
          begin
               with cdsVisitante do
               begin
                    FieldByName('VI_NOMBRES').AsString := VI_NOMBRES.Text;                                //Se llenan datos si es nuevo visitante
                    FieldByName('VI_APE_PAT').AsString := VI_APE_PAT.Text;
                    FieldByName('VI_APE_MAT').AsString := VI_APE_MAT.Text;
                    FieldByName('VI_TIPO').AsString := arTiposVisitantes[cbTipoVisitante.ItemIndex];
                    FieldByName('VI_ASUNTO').AsString := arTiposAsuntos[cmbTipoAsunto.ItemIndex];
                    if cbSexo.ItemIndex = 0 then
                      FieldByName('VI_SEXO').AsString := 'M'
                    else FieldByName('VI_SEXO').AsString := 'F';
                    FieldByName('AN_NUMERO').AsInteger := strToInt(VISITAA.Llave);
                    FieldByName('VI_NACION').AsString := VI_NACIONALIDAD.Text;
                    FieldByName('VI_MIGRA').AsString := VI_MIGRA.Text;
                    FieldByName('EV_NUMERO').AsInteger := dblCompania.Valor;
                    if iVisitNumero <=0 then
                    if TomoFoto then
                      FToolsImageEn.AsignaImagenABlob( FOTO, dmVisitantes.cdsVisitante, 'VI_FOTO' )
                    else Foto.clear;

                    Enviar;
                    DisableControls; //acl {Se cambio a despues de enviar para que guarde la foto. ACL}

                    try
                       Last;
                       iVisitNumero := FieldByName('VI_NUMERO').AsInteger;
                       FVisitaAgregada := TRUE;
                       Refrescar;
                    finally
                           EnableControls;
                    end;
               end;
          end;
     end;
end;

procedure TFRegistroEntradaVisit.AjustaTamanioForma;
const
  iTAMCHI:integer=341;
  iTAMMED:integer=536;
  iTAMGDE:integer=745;
begin
  with dmVisitantes do begin
  with FRegistroEntradaVisit do begin
    if (AutoReg_Identificacion) and (AutoReg_Vehiculo)then begin
      if (AutoReg_Observaciones) then
          Height:= iTAMCHI
      else Height:= iTAMMED;
    end
    else Height:= iTAMGDE;
  end;
  end;
end;

procedure TFRegistroEntradaVisit.AntesDeEnviar;

 function GetDato( const sCodigo: string; const lNoExiste: Boolean ): string;
 begin
      if lNoExiste then
         Result := VACIO
      else
          Result := sCodigo;
 end;
 var
    DataSetVisita: TDataSet;
    DataSetAnfitrion: TDataset;
begin
  with dmVisitantes do begin
   if iVisitNumero <=0 then
      AgregarVisitante;

    with cdsLibros do begin
      if dmVisitantes.AutoReg_Identificacion then
        FieldByName('LI_TIPO_ID').AsString := arTipoId[cbTipoId.ItemIndex];

      if cbTipoAuto.ItemIndex >= 0 then
        FieldByName('LI_CAR_TIP').AsString := arTipoAuto[cbTipoAuto.ItemIndex];

      if cmbTipoAsunto.ItemIndex >= 0 then
        FieldByName('LI_ASUNTO').AsString := arTiposAsuntos[cmbTipoAsunto.ItemIndex];
      DataSetVisita := cdsVisitante;
      FieldByName('LI_NOMBRE').AsString := VI_NOMBRES.Text +' '+VI_APE_PAT.Text+' '+VI_APE_MAT.Text;
      FieldByName('EV_NUMERO').AsInteger := dblCompania.Valor;
      FieldByName('LI_EMPRESA').AsString := dblCompania.Descripcion;
      FieldByName('VI_NUMERO').AsInteger  := iVisitNumero;

       DataSetAnfitrion := cdsAnfitrion;
       if (DataSetAnfitrion <> NIL ) then
       begin
          FieldByName('LI_ANFITR').AsString :=  VISITAA.Descripcion;
          FieldByName('LI_CDEPTO').AsString :=  DataSetAnfitrion.FieldByName('AN_DEPTO').AsString;
          FieldByName('LI_DEPTO').AsString := DataSetAnfitrion.FieldByName('TB_ELEMENT').AsString;
          FieldByName('AN_NUMERO').AsInteger := strToInt(VISITAA.Llave);
       end;
    end;
  end;
end;



procedure TFRegistroEntradaVisit.connect;
begin
  with dmVisitantes do
     begin
          cdsTipoAsunto.Refrescar;
          cdsTipoCarro.Refrescar;
          cdsTipoID.Refrescar;
          cdsTipoVisita.Refrescar;
          cdsEmpVisitante.Conectar;//Se refresca en la busqueda de compa�ia
          cdsSQL.Conectar;
          //cdsCompaniaLookup.Conectar;
          //cdsCaseta.Conectar;
          cdsDepto.Conectar;
          DataSource.DataSet:= cdsLibros;
          dsTipoVisitante.DataSet := cdsTipoVisita;
          dsBuscaVisitandoA.DataSet := cdsAnfitrionLookup;
          dsBuscaVisita.DataSet := cdsVisitanteLookup;
          dsBuscaCompania.DataSet := cdsCompaniaLookup;
          dsAsunto.Dataset := cdsTipoAsunto;
          dsTipoID.Dataset := cdsTipoID;
          dsVehiculo.DataSet := cdsTipoCarro;
          dsVisita.Dataset := cdsVisitante;
          dsSQL.DataSet := cdsSQL;



          LlenaCombos;
     end;
    //:todo ChecaGlobales;
    //:todo VI_SEXO.ItemIndex :=0;
end;




procedure TFRegistroEntradaVisit.Disconnect;
begin
    // stop and disconnect
    FOTO.IO.DShowParams.Disconnect;
end;



function TFRegistroEntradaVisit.DriverManager: boolean;
begin
  Result := True;

end;

procedure TFRegistroEntradaVisit.bAgregaEmpreClick(Sender: TObject);
begin
  if EditCatCompanys = nil then
    EditCatCompanys := TEditCatCompanys.Create(self);
  EditCatCompanys.ShowModal;
  dmVisitantes.cdsCompaniaLookup.Refrescar;
end;

procedure TFRegistroEntradaVisit.bAgregaVisitanteClick(Sender: TObject);
begin
   if EditCatAnfitrion = nil then
    EditCatAnfitrion := TEditCatAnfitrion.Create(self);
  EditCatAnfitrion.ShowModal;
  dmVisitantes.cdsAnfitrion.Refrescar;
end;

procedure TFRegistroEntradaVisit.bOkClick(Sender: TObject);
var
  lOk:boolean;
begin
  lOk:=true;
  if ValidaCamposEnt then begin
    _AlEjecutar(Self, lOk);
    close;
    FBusquedaVisitante.Close;
  end;
end;

procedure TFRegistroEntradaVisit.btnOtroClick(Sender: TObject);
var
  lOk:boolean;
begin
  lOk:=true;
  if ValidaCamposEnt then begin
    _AlEjecutar(Self, lOk);
    FBusquedaVisitante.Close;
    LimpiaControlesEntrada;
    dmVisitantes.cdsLibros.Append;
  end;
end;

procedure TFRegistroEntradaVisit.btnTomaFotoClick(Sender: TObject);
begin
  ConectaCamaraTomaFoto;
  FTomoFoto:=true;
  if btnTomaFoto.OptionsImage.ImageIndex <> 0 then
    cmbDriverFotoChange(Self);


  if btnTomaFoto.OptionsImage.ImageIndex =0 then begin
    btnTomaFoto.OptionsImage.ImageIndex := 1;
    btnTomaFoto.Hint := 'Prender c�mara';
    cmbDriverFoto.Enabled := false;
  end
  else begin
     btnTomaFoto.OptionsImage.ImageIndex := 0;
     btnTomaFoto.Hint := 'Tomar foto';
     cmbDriverFoto.Enabled := true;
  end;
end;

function TFRegistroEntradaVisit.CancelarWizard: Boolean;
begin

end;

procedure TFRegistroEntradaVisit.Cancelar_DevExClick(Sender: TObject);
begin
  CloseQuery;
  close;
end;

procedure TFRegistroEntradaVisit.cmbDriverFotoChange(Sender: TObject);
begin
  ConectaCamaraTomaFoto;
  Foto.Clear;
  if cmbDriverFoto.ItemIndex > 0 then begin
   ShowVideoFormats;
   MuestraImagenFoto;
   btnTomaFoto.Enabled:=FOTO.IO.DShowParams.Connected;
  end
  else begin
    ConectaCamaraTomaFoto;
    btnTomaFoto.Enabled:=false;
    Foto.Enabled:=false;
  end;
end;

procedure TFRegistroEntradaVisit.ConectaCamar;
begin
  if (not FOTO.IO.DShowParams.Connected) then
  begin
    // set video source as index of IO.DShowParams.VideoInputs
    FOTO.IO.DShowParams.SetVideoInput(cmbDriverFoto.ItemIndex-1,
        StrToIntDef('0',0), // set this parameter if you have more than one camera with same name
        StrToIntDef('640', 0), // capture width
        StrToIntDef('480', 0), // capture height
        AnsiString('YUY2' )    // format
       );
    // enable frame grabbing
    FOTO.IO.DShowParams.EnableSampleGrabber := true;
    // connect to the video input
    FOTO.IO.DShowParams.Connect;

    //imageenview1.io.dshowparams.SaveGraph('c:\1.grf');
end;
end;

procedure TFRegistroEntradaVisit.ConectaCamaraTomaFoto;
var
   sMsgError: String;
begin
  SetControls( True );
  with Foto.IO.DShowParams do
  begin
    GetSample( Foto.IEBitmap );
    Stop;
    Disconnect;
    Update;
  end;
  if ( not FToolsImageEn.ResizeImagen( FOTO, sMsgError ) ) then
     ZError( self.Caption, sMsgError, 0);
  FToolsImageEn.AsignaImagenABlob( FOTO, dmVisitantes.cdsVisitante, 'VI_FOTO' );
end;

procedure TFRegistroEntradaVisit.Ejecutar;
var
   oCursor: TCursor;
   lOk:boolean;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        Application.ProcessMessages;
        try
           EnviarEntrada;
        except
              on Error: Exception do
              begin
                   zExcepcion( Caption, 'Error al registrar entrada', Error, 0 );
                   lOk := False;
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     if lOk then
        TerminarEjecucion;
end;

function TFRegistroEntradaVisit.EjecutarWizard: Boolean;
var
   oCursor: TCursor;
   iRecords, iFolio : integer;
   sIndex: string;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          iFolio := -1;
          try
             Result := TRUE;
             try

                with dmVisitantes.cdsLibros do
                begin
                     iRecords := RecordCount;
                     {
                     if NOT FCompaniaAgregada then
                        AgregarCompania;

                     if NOT FAnfitrionAgregada then
                        AgregarAnfitrion;

                     if NOT FVisitaAgregada then
                        AgregarVisitante;

                      }
                     AntesDeEnviar;
                     try
                        DisableControls;
                        sIndex := IndexFieldNames;
                        IndexFieldNames := 'LI_FOLIO';

                        Enviar;

                        Refrescar;

                        if ( iRecords <> RecordCount ) then Last;

                        iFolio := FieldByName('LI_FOLIO').AsInteger;

         //               TressShell.ImprimeForma(TRUE);

                     finally
                            IndexFieldNames := sIndex;
                            if (iFolio >= 0 ) then
                               Locate('LI_FOLIO',VarArrayOf([iFolio]), []);
                            EnableControls;
                     end;
                end;

             except
                      Result := FALSE;
                      Raise;
             end;
          finally
                 Cursor := oCursor;
          end;
     end;

end;

function TFRegistroEntradaVisit.EnviarEntrada:boolean;
var
   oCursor: TCursor;
   iRecords, iFolio : integer;
   sIndex: string;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          iFolio := -1;
          try
             Result := TRUE;
             try

                with dmVisitantes.cdsLibros do
                begin
                     iRecords := RecordCount;

                     AntesDeEnviar;
                     try
                        DisableControls;
                        sIndex := IndexFieldNames;
                        IndexFieldNames := 'LI_FOLIO';

                        Enviar;

                        Refrescar;

                        if ( iRecords <> RecordCount ) then Last;

                        iFolio := FieldByName('LI_FOLIO').AsInteger;

                   //     TressShell.ImprimeForma(TRUE);

                     finally
                            IndexFieldNames := sIndex;
                            if (iFolio >= 0 ) then
                               Locate('LI_FOLIO',VarArrayOf([iFolio]), []);
                            EnableControls;
                     end;
                end;

             except
                      Result := FALSE;
                      Raise;
             end;
          finally
                 Cursor := oCursor;
          end;
     end;

end;

function TFRegistroEntradaVisit.ExisteCompania: Boolean;
begin
  Result:=false;
end;

function TFRegistroEntradaVisit.ExisteVisitandoA: Boolean;
begin
    Result := false;
end;

function TFRegistroEntradaVisit.ExisteVisitante: Boolean;
begin
    Result := false;
end;

procedure TFRegistroEntradaVisit.FormActivate(Sender: TObject);
var
  arCamaras:array of string;
  iCont:integer;
  iContFuera:integer;
begin
   // Fill video source combobox
    cmbDriverFoto.Items.Assign(FOTO.IO.DShowParams.VideoInputs);
    SetLength(arCamaras, cmbDriverFoto.Items.Count);
    for iContFuera := 0 to 1 do begin
      iCont:=0;
        if (iContFuera = 1) and (iCont = 0) then begin
          cmbDriverFoto.Clear;
          cmbDriverFoto.Items.Add('');
        end;
        while iCont < length(arCamaras) do begin
           if iContFuera = 0 then
              arCamaras[iCont] := cmbDriverFoto.Items[iCont]
           else cmbDriverFoto.Items.Add(arCamaras[iCont]);
           Inc(iCont);
        end;
    end;
    // Select first item
    if (cmbDriverFoto.Items.Count > 0) and (iVisitNumero <= 0 ) then begin
      cmbDriverFoto.ItemIndex:=1;
      cmbDriverFotoChange(Self);
    end;
end;

procedure TFRegistroEntradaVisit.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
   with dmVisitantes.cdsLibros do begin
   if State in [dsInsert, dsEdit] then
      Cancel;
   end;
   ConectaCamaraTomaFoto;

end;

procedure TFRegistroEntradaVisit.FormCreate(Sender: TObject);
begin
   connect;
   VISITAA.LookupDataset := dmVisitantes.cdsAnfitrion;
   dblCompania.LookupDataset := dmVisitantes.cdsCompaniaLookup;
   iVisitNumero:=-1;
end;

procedure TFRegistroEntradaVisit.FormShow(Sender: TObject);
begin
   LimpiaControlesEntrada;
   VerificaCamposNecesarios;
   IniciaCaptura;
   if (iVisitNumero >=  0)then begin
      LlenaDatosVisitante;
      IniciaControlesVisitante;
   end
   else IniciaControlesNuevoVisitante;
   btnTomaFoto.Enabled:=FOTO.IO.DShowParams.Connected;
end;

procedure TFRegistroEntradaVisit.FotoDShowNewFrame(Sender: TObject);
begin
   // copy current sample to ImageEnView bitmap
  FOTO.IO.DShowParams.GetSample(FOTO.Layers[0].Bitmap);
  // refresh ImageEnView1
  FOTO.Update;
end;

function TFRegistroEntradaVisit.GetEmpInsertada: boolean;
begin

end;



function TFRegistroEntradaVisit.GetSTRDepto(sDepto: string): integer;
var
  iCont:integer;
begin
  iCont:=0;
  while iCont < length(arDeptos) do begin
    if sDepto = arDeptos[iCont] then
      Result:=iCont;
      inc(iCont);
  end;

end;

function TFRegistroEntradaVisit.GetTipoVisitante(sTipoVisi: string): integer;
var
  iContTVisi:integer;
begin
  iContTVisi:= 0;
  while iContTVisi <= length(arTiposVisitantes)-1 do begin
    if arTiposVisitantes[iContTVisi] = sTipoVisi then
        Result:= iContTVisi;
    inc(iContTVisi);
  end;
end;

procedure TFRegistroEntradaVisit.IniciaCaptura;
begin
  if not VI_NOMBRES.Enabled then
      VISITAA.SetFocus
    else VI_NOMBRES.SetFocus;
    cbSexo.ItemIndex :=0;
    cbTipoVisitante.ItemIndex :=0;
    if (iVisitNumero <=0) and (Global.GetGlobalBooleano(K_GLOBAL_REQ_DEPARTAMENTO)) then
      cmbDEPTO.ItemIndex :=0;
    cmbTipoAsunto.ItemIndex :=0;
    with dmVisitantes do begin
    if AutoReg_Identificacion then
      cbTipoId.ItemIndex:=0
    else cbTipoId.Text := VACIO;
    if AutoReg_Vehiculo then
      cbTipoAuto.ItemIndex:=0
    else cbTipoAuto.Text := VACIO;
    if AutoReg_AsuntoATratar then
      cmbTipoAsunto.ItemIndex:=0
    else cmbTipoAsunto.Text := VACIO;


    end;

    Foto.Enabled := dmVisitantes.MostrarGridAvanzado;
end;

procedure TFRegistroEntradaVisit.IniciaControlesNuevoVisitante;
begin
    FTomoFoto:=false;
    VI_FEC_NAC.Texto := DateToStr(date);
    Foto.Clear;
    ConectaCamaraTomaFoto;
    with FBusquedaVisitante do begin
    if edNombre.Text <> VACIO then
      VI_NOMBRES.Text := edNombre.Text;
    if edApePat.Text <> VACIO then
      Self.VI_APE_PAT.Text := edApePat.Text;
    end;

  cmbDriverFoto.Enabled:=true;
  btnTomaFoto.Enabled:=true;
end;

procedure TFRegistroEntradaVisit.IniciaControlesVisitante;
begin
  cmbDriverFoto.Enabled:=false;
  btnTomaFoto.Enabled:=false;
end;

procedure TFRegistroEntradaVisit.LimpiaControlesEntrada;
begin
  VI_NOMBRES.Clear;
  VI_APE_PAT.Clear;
  VI_APE_MAT.Clear;
  cbSexo.ItemIndex:=0;
  VI_FEC_NAC.Clear;
  cbTipoVisitante.Clear;
  VI_NACIONALIDAD.Clear;
  VI_MIGRA.Clear;
  LI_OBSERVA.Clear;
  LI_TEXTO1.Clear;
  LI_TEXTO2.Clear;
  LI_TEXTO3.Clear;
  LI_GAFETE.Clear;
  LI_ID.Clear;
  LI_CAR_PLA.Clear;
  LI_CAR_DES.Clear;
  LI_CAR_EST.Clear;
  Foto.Clear;
  btnTomaFoto.Enabled:= false;
  VISITAA.Llave:=VACIO;
  cmbDEPTO.Clear;
  cmbTipoAsunto.Clear;
  dblCompania.Llave:=VACIO;
end;

procedure TFRegistroEntradaVisit.LlenaComboAsunto;
var
  iContAsunto:integer;
begin
  iContAsunto:=0;
  cmbTipoAsunto.Clear;
  with dmVisitantes.cdsTipoAsunto do begin
  SetLength(arTiposAsuntos, RecordCount - 1);
  if RecordCount <=0 then
      DatabaseError('No existen asuntos');
  while iContAsunto < RecordCount-1 do begin
      cmbTipoAsunto.Properties.Items.Add(FieldByName('TB_ELEMENT').AsString);
      arTiposAsuntos[iContAsunto]:= FieldByName('TB_CODIGO').AsString;
      inc(iContAsunto);
      next;
  end;
  end;

end;

procedure TFRegistroEntradaVisit.LlenaComboDepto;
var
  iContDepto:integer;
begin
  iContDepto:=0;
  cmbDepto.Clear;
  with dmVisitantes.cdsDepto do begin
  SetLength(arDeptos, RecordCount - 1);
  while iContDepto < RecordCount-1 do begin
      cmbDepto.Properties.Items.Add(FieldByName('TB_ELEMENT').AsString);
      arDeptos[iContDepto]:= FieldByName('TB_CODIGO').AsString;
      inc(iContDepto);
      next;
  end;
  end;

end;

procedure TFRegistroEntradaVisit.LlenaComboDs(cmbName: TcxComboBox;
  ds: TDataSet; ar:array of string);
var
  iCont:integer;
begin
  iCont:=0;
  cmbName.Clear;
  with ds do begin
  //SetLength( ar, RecordCount - 1);
  while iCont < RecordCount-1 do begin
      cmbName.Properties.Items.Add(FieldByName('TB_ELEMENT').AsString);
      ar[iCont]:= FieldByName('TB_CODIGO').AsString;
      inc(iCont);
      next;
  end;
  end;

end;

procedure TFRegistroEntradaVisit.LlenaCombos;
begin
  with dmVisitantes do begin
    {SetLength(arTiposVisitantes, cdsTipoVisita.RecordCount);
    LlenaComboDs(cbTipoVisitante, cdsTipoVisita, arTiposVisitantes);//llena combo tipos visitantes

    SetLength(arTiposAsuntos, cdsTipoAsunto.RecordCount);
    LlenaComboDs(cmbTipoAsunto, cdsTipoAsunto, arTiposAsuntos);//llena combo asunto

    SetLength(arDeptos, cdsDepto.RecordCount);
    LlenaComboDs(cmbDEPTO, cdsDepto, arDeptos);//llena combo deptos

    SetLength(arTipoId, cdsTipoID.RecordCount);
    LlenaComboDs(cbTipoId, cdsTipoID, arTipoId);//llena combo id

    SetLength(arTipoAuto, cdsTipoCarro.RecordCount);
    LlenaComboDs(cbTipoAuto, cdsTipoCarro, arTipoAuto);//llena combo autos
     }
    LlenaComboTipoVisitante;
    LlenaComboAsunto;
    LlenaComboDepto;
    LlenaComboTipoId;
    LlenaComboTipoAuto
  end;
end;

procedure TFRegistroEntradaVisit.LlenaComboTipoAuto;
var
  iContId:integer;
begin
  iContId:=0;
  cbTipoAuto.Clear;
  with dmVisitantes.cdsTipoCarro do begin
  SetLength(arTipoAuto, RecordCount - 1);
  if RecordCount <=0 then
      DatabaseError('No existen tipos de auto');
  while iContId < RecordCount-1 do begin
      cbTipoAuto.Properties.Items.Add(FieldByName('TB_ELEMENT').AsString);
      arTipoAuto[iContId]:= FieldByName('TB_CODIGO').AsString;
      inc(iContId);
      next;
  end;
  end;
end;

procedure TFRegistroEntradaVisit.LlenaComboTipoId;
var
  iContId:integer;
begin
  iContId:=0;
  cmbTipoAsunto.Clear;
  with dmVisitantes.cdsTipoID do begin
  SetLength(arTipoId, RecordCount - 1);
  if RecordCount <=0 then
      DatabaseError('No existen asuntos');
  while iContId < RecordCount-1 do begin
      cbTipoId.Properties.Items.Add(FieldByName('TB_ELEMENT').AsString);
      arTipoId[iContId]:= FieldByName('TB_CODIGO').AsString;
      inc(iContId);
      next;
  end;
  end;
end;

procedure TFRegistroEntradaVisit.LlenaComboTipoVisitante;
var
  iContTipoVisitante:integer;
begin
  iContTipoVisitante:=0;
  cbTipoVisitante.Clear;
  with dmVisitantes.cdsTipoVisita do begin
  SetLength(arTiposVisitantes, RecordCount - 1);
  if RecordCount <=0 then
      DatabaseError('No existen tipos de visitante');
  while iContTipoVisitante < RecordCount-1 do begin
      cbTipoVisitante.Properties.Items.Add(FieldByName('TB_ELEMENT').AsString);
      arTiposVisitantes[iContTipoVisitante]:= FieldByName('TB_CODIGO').AsString;
      inc(iContTipoVisitante);
      next;
  end;
  end;
end;

procedure TFRegistroEntradaVisit.LlenaDatosVisitante;
var   Filtro : String;
begin
  with dmVisitantes do begin
    with cdsSQL do begin
      if Locate('VI_NUMERO',  iVisitNumero, []) then begin
        VI_NOMBRES.Text := FieldByName('VI_NOMBRES').AsString;
        VI_APE_PAT.Text := FieldByName('VI_APE_PAT').AsString;
        VI_APE_MAT.Text := FieldByName('VI_APE_MAT').AsString;
        VI_FEC_NAC.Text := FieldByName('VI_FEC_NAC').AsString;
        VI_MIGRA.Text := FieldByName('VI_MIGRA').AsString;
        VI_NACIONALIDAD.Text := FieldByName('VI_NACION').AsString;
        VI_MIGRA.Text := FieldByName('VI_MIGRA').AsString;
        dblCompania.Valor := FieldByName('EV_NUMERO').AsInteger;
        cbTipoVisitante.ItemIndex := getTipoVisitante(FieldByName('VI_TIPO').AsString);
        FToolsImageEn.AsignaBlobAImagen( FOTO, dmVisitantes.cdsSQL, 'VI_FOTO' );
        if FieldByName('VI_SEXO').AsString = 'M' then
          cbSexo.ItemIndex := 0
        else cbSexo.ItemIndex := 1;
      end;
    end;
  end;
end;

procedure TFRegistroEntradaVisit.ManejaDriverFoto;
begin
  if DriverManager then
  begin
      SetControls( False );
  end
  else
  begin
      SetControls( True );
  end;
end;

procedure TFRegistroEntradaVisit.MuestraImagenFoto;
var
  w, h: integer;
  f: AnsiString;
begin
  ConectaCamar;
  // show info
  FOTO.IO.DShowParams.GetCurrentVideoFormat(w, h, f);
  // start capture
  FOTO.IO.DShowParams.Run;
end;

procedure TFRegistroEntradaVisit.SetControls(lHabilita: Boolean);
begin

end;

procedure TFRegistroEntradaVisit.SetEmpInsertada(const Value: boolean);
begin

end;

procedure TFRegistroEntradaVisit.SetIVisiNumero(const Value: integer);
begin
    iVisitNumero:=Value;
end;

procedure TFRegistroEntradaVisit.ShowVideoFormats;
var
  i: integer;
  s: string;
begin

end;

procedure TFRegistroEntradaVisit.TerminarEjecucion;
begin
  Close;
  winapi.Windows.SetForegroundWindow( Handle ); { Evita que la aplicaci�n pierda el Focus }
end;


procedure TFRegistroEntradaVisit.TerminarWizard;
begin
    //Close;
    Winapi.Windows.SetForegroundWindow( Handle ); { Evita que la aplicaci�n pierda el Focus }

end;

function TFRegistroEntradaVisit.ValidaCamposEnt: boolean;
var
  bOk:boolean;
begin
  bOk:=true;
  if (VI_NOMBRES.Text = VACIO) and (VI_APE_PAT.Text=VACIO) then begin
    bOk:=false;
    VI_NOMBRES.SetFocus;
    ZError('Registro de entrada', 'Campo visitante requerido para grabar el registro de visitas', 0);
  end
  else if (dblCompania.Descripcion = VACIO) and (Global.GetGlobalBooleano( K_GLOBAL_REQ_COMPANYS)) then begin
    bOk:=false;
    ZError('Registro de entrada', 'Campo Compa��a requerido para grabar el registro de visitas', 0);
    dblCompania.SetFocus;
  end
  else if (cmbTipoAsunto.Text = VACIO) and (dmVisitantes.AutoReg_AsuntoATratar) then begin
    bOk:=false;
    ZError('Registro de entrada', 'ElCampo asunto requerido para grabar el registro de visitas', 0);
    cmbTipoAsunto.SetFocus;
  end
  else if (VISITAA.Descripcion = VACIO) and (Global.GetGlobalBooleano( K_GLOBAL_REQ_ANFITRION)) then begin
    bOk:=false;
    ZError('Registro de entrada', 'Campo anfitrion requerido para grabar el registro de visitas', 0);
    VISITAA.SetFocus;
  end
  else if (cmbDEPTO.Text = VACIO) and (Global.GetGlobalBooleano( K_GLOBAL_REQ_DEPARTAMENTO)) then begin
    bOk:=false;
    ZError('Registro de entrada', 'Campo departamento requerido para grabar el registro de visitas', 0);
    cmbDEPTO.SetFocus;
  end
  else if (cbTipoId.Text = VACIO) and (dmVisitantes.AutoReg_Identificacion) then begin
    bOk:=false;
    ZError('Registro de entrada', 'Campo identificaci�n requerido para grabar el registro de visitas', 0);
    cbTipoId.SetFocus;
  end
  else if (LI_GAFETE.Text = VACIO) and (Global.GetGlobalBooleano( K_GLOBAL_REQ_GAFETE)) then begin
    bOk:=false;
    ZError('Registro de entrada', 'Campo gafete requerido para grabar el registro de visitas', 0);
    LI_GAFETE.SetFocus;
  end
  else if (cbTipoAuto.Text = VACIO) and (dmVisitantes.AutoReg_Vehiculo) then begin
    bOk:=false;
    ZError('Registro de entrada', 'Campo vehiculo requerido para grabar el registro de visitas', 0);
    cbTipoAuto.SetFocus;
  end;
    Result:=bOk;
end;


procedure TFRegistroEntradaVisit.VerificaCamposNecesarios;
var
  bMuestraIdentifica:boolean;
  bMuestraAutomovil:boolean;
  bMuestraObserva:boolean;
  bDatosEmpleado:boolean;
begin

  with dmVisitantes do begin
    if iVisitNumero >=0 then
      bDatosEmpleado := false;

    bMuestraIdentifica := AutoReg_Identificacion;
    bMuestraAutomovil := AutoReg_Vehiculo;
    bMuestraObserva := AutoReg_Observaciones;

  end;


  VI_NOMBRES.Enabled := bDatosEmpleado;
  VI_APE_PAT.Enabled := bDatosEmpleado;
  VI_APE_MAT.Enabled := bDatosEmpleado;
  cbSexo.Enabled := bDatosEmpleado;
  VI_FEC_NAC.Enabled := bDatosEmpleado;
  cbTipoVisitante.Enabled := bDatosEmpleado;
  VI_NACIONALIDAD.Enabled := bDatosEmpleado;
  VI_MIGRA.Enabled := bDatosEmpleado;
  {cmbDriverFoto.Enabled := bDatosEmpleado;
  btnTomaFoto.Enabled := bDatosEmpleado;
   }
  cbTipoId.enabled := bMuestraIdentifica;
  LI_ID.enabled := bMuestraIdentifica;
  LI_GAFETE.enabled := bMuestraIdentifica;
  lbId.enabled := bMuestraIdentifica;
  lbIdentificacion.enabled := bMuestraIdentifica;
  liGafete.enabled := bMuestraIdentifica;

  cbTipoAuto.enabled := bMuestraAutomovil;
  LI_CAR_PLA.enabled := bMuestraAutomovil;
  LI_CAR_DES.enabled := bMuestraAutomovil;
  LI_CAR_EST.enabled := bMuestraAutomovil;
  lbCarPla.enabled := bMuestraAutomovil;
  lbCarDesc.enabled := bMuestraAutomovil;
  lbCarEst.enabled := bMuestraAutomovil;
  liTipoVehiculo.enabled := bMuestraAutomovil;

  LI_OBSERVA.enabled := bMuestraObserva;
  LI_TEXTO1.enabled := bMuestraObserva;
  LI_TEXTO2.enabled := bMuestraObserva;
  LI_TEXTO3.enabled := bMuestraObserva;
  lbObserva1.enabled := bMuestraObserva;
  lbObserva2.enabled := bMuestraObserva;
  lbTexto1.enabled := bMuestraObserva;
  lbTexto2.enabled := bMuestraObserva;
  lbTexto3.enabled := bMuestraObserva;



end;

procedure TFRegistroEntradaVisit.VISITAAExit(Sender: TObject);
var
  iCont:integer;
begin
  cmbDEPTO.Clear;
  iCont:=0;
  while iCont <= length(arDeptos)-1 do begin
      if arDeptos[iCont] = dmVisitantes.cdsAnfitrion.FieldByName('AN_DEPTO').AsString then
          cmbDEPTO.ItemIndex := iCont;
    inc(iCont);

  end;

end;

function TFRegistroEntradaVisit.VisitanteConEmpresa: Boolean;
begin

end;

procedure TFRegistroEntradaVisit._AlCancelar(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        Application.ProcessMessages;
        try
           lOk := EjecutarWizard;
        except
              on Error: Exception do
              begin
                   zExcepcion( Caption, 'Error al registrar entrada', Error, 0 );
                   lOk := False;
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     if lOk then
        TerminarWizard;
end;

procedure TFRegistroEntradaVisit._AlEjecutar(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        Application.ProcessMessages;
        try
           lOk := EjecutarWizard;
        except
              on Error: Exception do
              begin
                   zExcepcion( Caption, 'Error al registrar entrada', Error, 0 );
                   lOk := False;
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     if lOk then
        TerminarWizard;
end;

end.
