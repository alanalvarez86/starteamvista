unit FMenuConfigura;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ImgList, ComCtrls, ToolWin, cxGraphics, dxSkinsCore, TressMorado2013,
  dxSkinsdxBarPainter, dxBar, cxClasses;

type
  TMenuConfigura = class(TForm)
    ImageList1: TImageList;
    SmallImageList: TcxImageList;
    dxBarDockControl1: TdxBarDockControl;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    CasetaDefault: TdxBarLargeButton;
    cxImageList1: TcxImageList;
    Salir: TdxBarLargeButton;
    AutoRegistro: TdxBarLargeButton;
    Configurar: TdxBarLargeButton;
    procedure ButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FOperacion: Integer;
  protected
    { Protected declarations }
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
    { Public declarations }
    property Operacion : Integer read FOperacion;
  end;

var
  MenuConfigura: TMenuConfigura;

implementation

uses ZetaDialogo,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaCommonClasses;

{$R *.DFM}

procedure TMenuConfigura.FormCreate(Sender: TObject);
begin
     HelpContext := H_VIS_MENU_CASETA;
end;

procedure TMenuConfigura.FormShow(Sender: TObject);
begin
     FOperacion := 0;
     CasetaDefault.Enabled := ZAccesosMgr.CheckDerecho( D_SIST_ADMIN_CASETA, K_DERECHO_CONSULTA );
     Configurar.Enabled := ZAccesosMgr.CheckDerecho( D_SIST_ADMIN_CASETA, K_DERECHO_ALTA );
end;

procedure TMenuConfigura.ButtonClick(Sender: TObject);
begin
     FOperacion := TWinControl( Sender ).Tag;
     self.Close;
end;

procedure TMenuConfigura.KeyPress( var Key: Char );
begin
     inherited KeyPress( Key );
     if ( Key = Chr( VK_ESCAPE ) ) then
        ButtonClick( Salir );
end;

end.
