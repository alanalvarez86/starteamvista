inherited GridSalidas: TGridSalidas
  Left = 219
  Top = 347
  BorderIcons = [biSystemMenu, biMaximize]
  BorderStyle = bsSizeable
  Caption = 'Seleccione el visitante'
  ClientHeight = 280
  ClientWidth = 913
  OldCreateOrder = True
  OnClose = FormClose
  OnShow = FormShow
  ExplicitWidth = 929
  ExplicitHeight = 324
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 244
    Width = 913
    ExplicitTop = 244
    ExplicitWidth = 913
    inherited OK_DevEx: TcxButton
      Left = 743
      Top = 5
      Hint = 'Aceptar y escribir datos'
      OnClick = OK_DevExClick
      ExplicitLeft = 743
      ExplicitTop = 5
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 823
      Top = 5
      Hint = 'Cancelar cambios'
      ExplicitLeft = 823
      ExplicitTop = 5
    end
  end
  object GridLibro: TZetaDBGrid [1]
    Left = 0
    Top = 30
    Width = 913
    Height = 80
    Align = alTop
    DataSource = DataSource
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -13
    TitleFont.Name = 'Verdana'
    TitleFont.Style = []
    Visible = False
    OnDrawColumnCell = GridLibroDrawColumnCell
    OnDblClick = GridLibroDblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'LI_FOLIO'
        Title.Alignment = taCenter
        Title.Caption = '#'
        Width = 49
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LI_ENT_CAS'
        Title.Caption = 'Caseta'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LI_ENT_HOR'
        Title.Caption = 'Entrada'
        Width = 57
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LI_NOMBRE'
        Title.Caption = 'Visitante'
        Width = 130
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LI_EMPRESA'
        Title.Caption = 'Compa'#241#237'a'
        Width = 130
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LI_ANFITR'
        Title.Caption = 'Visitando a'
        Width = 196
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LI_DEPTO'
        Title.Caption = 'Departamento'
        Width = 130
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_ENT_LIB'
        Title.Caption = 'Registr'#243' Entrada'
        Width = 127
        Visible = True
      end>
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 0
    Width = 913
    Height = 30
    Align = alTop
    TabOrder = 2
    object lbBusca: TLabel
      Left = 10
      Top = 8
      Width = 33
      Height = 13
      Caption = 'Busca:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object BBusca: TcxButton
      Left = 306
      Top = 5
      Width = 21
      Height = 21
      Hint = 'Buscar reporte'
      OptionsImage.Glyph.Data = {
        DA050000424DDA05000000000000360000002800000013000000130000000100
        200000000000A4050000000000000000000000000000000000008B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8A80
        8BFF887E89FF837984FF887E89FF8A808BFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8A808BFF877C88FF988F99FFCECACEFF938A94FF887E89FF8A80
        8BFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF897E8AFF8B818BFFE1DDE0FFFFFF
        FFFFFDFCFCFF887D89FF8A808BFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8A808BFF8277
        83FFC7C4C9FFFFFFFFFFFFFFFFFFC4BFC5FF857B86FF8A808BFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8A808BFF897E8AFF837984FF7E73
        80FF7E737FFF7B707CFFB0A9B0FFFFFFFFFFFFFFFFFFD6D3D8FF8A818CFF897F
        8AFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8A808BFF887D
        89FF8F8690FFC4BFC5FFFCFBFCFFFDFCFDFFE7E5E7FFF9F8F9FFFFFFFFFFE8E7
        E9FF928993FF877D88FF8A808BFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8A808BFF887E89FF958C95FFEAE9EAFFFFFFFFFFF2F1F2FFEDECEEFFFEFE
        FEFFFFFFFFFFFDFDFDFF9A929BFF857A86FF8A808BFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8A808BFF857A86FFCBC7CBFFFFFFFFFFD1CE
        D2FF988F99FF918892FFAFA8B0FFF3F2F2FFFDFEFEFF988F99FF877D88FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8A808BFF8379
        84FFFDFDFEFFE9E7EAFF938994FF857B87FF887E89FF827783FFB9B3BAFFFFFF
        FFFFC5C1C6FF847985FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8A808BFF837784FFFFFFFFFFD8D5D9FF8B818CFF897F8AFF8B81
        8CFF867B87FFA299A2FFFFFFFFFFD2CED3FF827883FF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8A808BFF837984FFF7F7F7FFECEB
        EDFF988F9AFF847A85FF877C88FF827783FFC4C0C4FFFFFFFFFFC0BAC1FF847A
        85FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8A80
        8BFF867C87FFC0BBC1FFFFFFFFFFDBD8DBFFA29BA3FF9B929CFFB9B3BAFFFBFB
        FBFFFEFEFEFF938994FF887E89FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8A808BFF897F8AFF8F8690FFDDD9DCFFFFFFFFFFFCFC
        FCFFF8F7F7FFFFFFFFFFFFFFFFFFA7A1A8FF857A86FF8A808BFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8A808BFF897E
        8AFF847A85FFABA4ACFFDBD6DBFFE7E6E8FFC7C3C8FF928993FF857A86FF8A80
        8BFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8A808BFF8A808BFF867B87FF817783FF807581FF8379
        84FF887E89FF8A808BFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
      ParentShowHint = False
      ShowHint = True
      SpeedButtonOptions.GroupIndex = 1
      SpeedButtonOptions.AllowAllUp = True
      TabOrder = 1
      OnClick = BBuscaClick
    end
    object EBuscaVisitante: TEdit
      Left = 45
      Top = 4
      Width = 255
      Height = 21
      TabOrder = 0
      OnChange = EBuscaVisitanteChange
    end
  end
  object cxGrid1: TcxGrid [3]
    Left = 0
    Top = 110
    Width = 913
    Height = 134
    Align = alClient
    TabOrder = 3
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      OnCustomDrawCell = cxGrid1DBTableView1CustomDrawCell
      DataController.DataSource = DataSource
      DataController.Filter.OnGetValueList = cxGrid1DBTableView1DataControllerFilterGetValueList
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object LI_FOLIO: TcxGridDBColumn
        Caption = '#'
        DataBinding.FieldName = 'LI_FOLIO'
      end
      object LI_ENT_CAS: TcxGridDBColumn
        Caption = 'Caseta'
        DataBinding.FieldName = 'LI_ENT_CAS'
      end
      object LI_ENT_HOR: TcxGridDBColumn
        Caption = 'Entrada'
        DataBinding.FieldName = 'LI_ENT_HOR'
      end
      object LI_NOMBRE: TcxGridDBColumn
        Caption = 'Visitante'
        DataBinding.FieldName = 'LI_NOMBRE'
      end
      object LI_EMPRESA: TcxGridDBColumn
        Caption = 'Compa'#241#237'a'
        DataBinding.FieldName = 'LI_EMPRESA'
      end
      object LI_ANFITR: TcxGridDBColumn
        Caption = 'Visitando a'
        DataBinding.FieldName = 'LI_ANFITR'
      end
      object LI_DEPTO: TcxGridDBColumn
        Caption = 'Departamento'
        DataBinding.FieldName = 'LI_DEPTO'
      end
      object US_ENT_LIB: TcxGridDBColumn
        Caption = 'Registr'#243' entrada'
        DataBinding.FieldName = 'US_ENT_LIB'
        Width = 92
      end
      object LI_SAL_HOR: TcxGridDBColumn
        DataBinding.FieldName = 'LI_SAL_HOR'
        Visible = False
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  object DataSource: TDataSource
    Left = 264
    Top = 64
  end
end
