unit FSistEditUsuarios_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FSistBaseEditUsuarios_DevEx,
  Db, ZetaDBTextBox, StdCtrls, ZetaKeyCombo,
  ZetaKeyLookup, DBCtrls, Mask, ZetaNumero, ComCtrls, ExtCtrls, Buttons;

type
  TSistEditUsuarios_DevEx = class(TSistBaseEditUsuarios_DevEx)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SistEditUsuarios_DevEx: TSistEditUsuarios_DevEx;

implementation
uses
    ZetaCommonClasses;
{$R *.DFM}

procedure TSistEditUsuarios_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     Operacion.TabVisible := FALSE;
end;

end.
