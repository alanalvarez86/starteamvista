unit DVisitantes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient, ZetaClientDataSet,
{$ifdef DOS_CAPAS}
  DServerVisitantes,
{$else}
  Visitantes_TLB,
{$endif}
  FAlerta,

  ZetaCommonLists,
  ZetaCommonClasses,
  ZetaCommonTools,
  Variants;


const
     K_ANCHO_FOLIO_MGR = 10;
     K_ANCHO_FORMA = 329;
     K_ALTO_FORMA = 174;
     K_DIAGONAL = 15;
     K_MSG_VISITANTE_ADENTRO = 'El Visitante ya tiene un registro de entrada. ' + CR_LF +
                               'Para poder continuar es necesario registrar primero la salida.';
     K_MSG_LLEGO_CITA = 'No es posible registrar llegada. El visitante ya lleg� a su cita';

type
  TAlertasMGR = class
  private
    FAlertasMGR : TStrings;
    FDialogos : TList;
    FDataSet: TDataSet;
    FTop: integer;
    FLeft : integer;
    function GetFolio(const iFolio: integer): string;
    function ExisteAlerta( const iFolio: integer ): Boolean;
    procedure AgregaFolio( const iFolio: integer );
    procedure BorraFolio( const iFolio: integer );
    procedure DestruyeDialogos;
    procedure AjustaPosicion;
    procedure InitPosicion;
    function ObtenerNombreAnfi(iAnfi:integer):string;
  public
    property DataSet: TDataSet read FDataSet write FDataSet;
    constructor Create;
    destructor Destroy;override;
    procedure ShowDialog;
    procedure CloseDialog(oAlerta: TAlerta);

  end;


  TdmVisitantes = class(TDataModule)
    cdsCaseta: TZetaLookupDataSet;
    cdsCortes: TZetaLookupDataSet;
    cdsCitas: TZetaClientDataSet;
    cdsLibros: TZetaClientDataSet;
    cdsTipoAsunto: TZetaLookupDataSet;
    cdsAnfitrion: TZetaLookupDataSet;
    cdsDepto: TZetaLookupDataSet;
    cdsAnfitrionLookup: TZetaLookupDataSet;
    cdsVisitante: TZetaLookupDataSet;
    cdsVisitanteLookup: TZetaLookupDataSet;
    cdsEmpVisitante: TZetaLookupDataSet;
    cdsTipoID: TZetaLookupDataSet;
    cdsTipoCarro: TZetaLookupDataSet;
    cdsTipoVisita: TZetaLookupDataSet;
    cdsLibrosLookup: TZetaClientDataSet;
    cdsLibrosSalidas: TZetaClientDataSet;
    cdsCorteDetail: TZetaLookupDataSet;
    cdsLibroBusqueda: TZetaClientDataSet;
    cdsAlertas: TZetaClientDataSet;
    cdsCompaniaLookup: TZetaLookupDataSet;
    cdsMuestraFoto: TZetaLookupDataSet;
    cdsSQL: TZetaClientDataSet;
    cdsTotales: TZetaClientDataSet;
    cdsCitasSQL: TZetaClientDataSet; //acl
    procedure DataModuleCreate(Sender: TObject);
    procedure cdsCatalogoAlAdquirirDatos(Sender: TObject);
    procedure cdsCasetaAlCrearCampos(Sender: TObject);
    procedure cdsCitasAlAdquirirDatos(Sender: TObject);
    procedure cdsCitasAlCrearCampos(Sender: TObject);
    procedure cdsAnfitrionAlCrearCampos(Sender: TObject);
    procedure cdsAnfitrionLookupAlCrearCampos(Sender: TObject);
    procedure cdsAnfitrionLookupDescription(Sender: TZetaLookupDataSet; var sDescription: String);
    procedure cdsAnfitrionLookupSearch(Sender: TZetaLookupDataSet;var lOk: Boolean; const sFilter: String; var sKey,sDescription: String);
    procedure cdsVisitanteLookupAlCrearCampos(Sender: TObject);
    procedure cdsVisitanteAlCrearCampos(Sender: TObject);
    procedure cdsVisitanteLookupDescription(Sender: TZetaLookupDataSet;var sDescription: String);
    procedure cdsVisitanteLookupSearch(Sender: TZetaLookupDataSet;var lOk: Boolean; const sFilter: String; var sKey,sDescription: String);
    procedure cdsEmpVisitanteAlCrearCampos(Sender: TObject);
    procedure cdsLibrosAlAdquirirDatos(Sender: TObject);
    procedure cdsCatalogoAlEnviarDatos(Sender: TObject);
    procedure cdsCortesAlCrearCampos(Sender: TObject);
    procedure cdsLibrosAlCrearCampos(Sender: TObject);
    procedure cdsLibrosLookupAlAdquirirDatos(Sender: TObject);
    procedure cdsTablaAlEnviarDatos(Sender: TObject);
    procedure cdsLibrosNewRecord(DataSet: TDataSet);
    procedure cdsLibrosSalidasAlAdquirirDatos(Sender: TObject);
    procedure cdsLibrosAlModificar(Sender: TObject);
    procedure cdsCasetaAlAdquirirDatos(Sender: TObject);
    procedure cdsCortesAlEnviarDatos(Sender: TObject);
    {$ifdef VER130}
    procedure cdsCatalogosReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsCatalogosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}

    procedure cdsLibrosBeforePost(DataSet: TDataSet);
    procedure cdsGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    procedure cdsAlertasAlAdquirirDatos(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsVisitanteNewRecord(DataSet: TDataSet);
    procedure cdsEmpVisitanteAlAdquirirDatos(Sender: TObject);
    procedure cdsVisitanteAlEnviarDatos(Sender: TObject);
    procedure cdsLibrosCalcFields(DataSet: TDataSet);
    procedure cdsTotalesAlAdquirirDatos(Sender: TObject);
    procedure cdsDeptoLookupSearch(Sender: TZetaLookupDataSet; var lOk: Boolean;
      const sFilter: string; var sKey, sDescription: string);
    procedure cdsSQLAlCrearCampos(Sender: TObject);
    procedure cdsSQLCalcFields(DataSet: TDataSet);
    procedure cdsSQLAlAdquirirDatos(Sender: TObject);
    procedure cdsTotalesAlCrearCampos(Sender: TObject);
    procedure cdsTotalesReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
    procedure cdsCitasSQLAlAdquirirDatos(Sender: TObject);
    procedure cdsCitasSQLAlCrearCampos(Sender: TObject);
  private
    { Private declarations }
    FCorte: TNumEmp;
    FCorteEOF: Boolean;
    FCorteBOF: Boolean;
    FAlertasEnabled: Boolean;
    FTiempoMinimo: integer;
    FAlertasMGR : TAlertasMGR;
    FPuedeModificarLibro: Boolean;
    FHayCorteAbierto: Boolean;
    FUltimoCorteAbierto: Integer;
    sFiltroSQL:string;
    iOPCSQL:integer;
{$ifdef DOS_CAPAS}
    function GetServidor: TdmServerVisitantes;
    property Servidor: TdmServerVisitantes read GetServidor;
{$else}
    FServidor: IdmServerVisitantesDisp;
    function GetServidor: IdmServerVisitantesDisp;
    function GetMostrarGridAvanzado: Boolean;
    procedure SetMostrarGridAvanzado(const Value: Boolean);
    function GetMostrarRegistroGrupo: Boolean;
    procedure SetMostrarRegistroGrupo(Value: Boolean);
    procedure SetFiltroSQL(const Value: string);
    property Servidor: IdmServerVisitantesDisp read GetServidor;
{$endif}
    function GetCorteActivo: Boolean;
    function GetTieneSalida: Boolean;
    function FetchCorte(const iCorte: TNumEmp): Boolean;
    function FetchNextCorte(const iCorte: TNumEmp): Boolean;
    function FetchPreviousCorte(const iCorte: TNumEmp): Boolean;
    function GetCaseta: string;
    function GetFiltroSQL:string;
    //    function GetFolioSalida(var iFolio: integer): Boolean;
    function GetImpresionGafete: eImpresionGafete;
    function GetMostrarGridCitas: Boolean;
    function GetImpresoraDefault: string;
    function GetFiltrarTiempo: Boolean;
    function GetMinutosAntes: Integer;
    function GetMinutosDespues: Integer;
    function GetAutoReg_AsuntoATratar: boolean;
    function GetAutoReg_Identificacion: boolean;
    function GetAutoReg_Observaciones: boolean;
    function GetAutoReg_Vehiculo: boolean;
    procedure SetImpresoraDefault(const Value: string);
    procedure SetAutoReg_AsuntoATratar(const Value: boolean);
    procedure SetAutoReg_Identificacion(const Value: boolean);
    procedure SetAutoReg_Observaciones(const Value: boolean);
    procedure SetAutoReg_Vehiculo(const Value: boolean);
    procedure SetImpresionGafete(const Value: eImpresionGafete);
    procedure SetMostrarGridCitas(const Value: Boolean);
    procedure SetFiltrarTiempo(const Value: Boolean);
    procedure SetMinutosAntes(const Value: Integer);
    procedure SetMinutosDespues(const Value: Integer);
    procedure ObtieneNombreAnfi(Sender: TField; var Text: String;DisplayText: Boolean);
    procedure ObtieneNombreVisit(Sender: TField; var Text: String;DisplayText: Boolean);
    procedure ObtieneStatusVisit(Sender: TField; var Text: String;DisplayText: Boolean);
    procedure ObtieneAbiertaCaseta(Sender: TField; var Text: String;DisplayText: Boolean);
    procedure QuitaFechasNulas(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure ObtieneStatusVis(Sender: TField; var Text: String;DisplayText: Boolean);
    procedure ObtieneCompany (Sender: TField; var Text: String; DisplayText: Boolean);
    procedure ObtieneBuscaCompany (Sender: TField; var Text: String; DisplayText: Boolean);
    procedure MuestraDialogoAlerta;
  public
    { Public declarations }
    function getTotAutos:OleVariant;
    function InitCaseta(const lLeeRegistry: Boolean): Integer;
    function FindCaseta(const Value: String): Boolean;

    function GetCorteInicial: Boolean;
    function GetCortePrimero: Boolean;
    function GetCorteAnterior: Boolean;
    function GetCorteSiguiente: Boolean;
    function GetCorteUltimo: Boolean;

    function CorteDownEnabled: Boolean;
    function CorteUpEnabled: Boolean;

    function TiempoExcedido(DataSet: TDataSet): Boolean;

    procedure InitActivosCorte;

    procedure CargaCitas;
    procedure CargaLibro;

    property Caseta: string read GetCaseta;
    property Corte: TNumEmp read FCorte;
    property CorteActivo : Boolean read GetCorteActivo;
    property TieneSalida: Boolean read GetTieneSalida;
    property ImpresionGafete: eImpresionGafete read GetImpresionGafete write SetImpresionGafete;
    property MostrarGridCitas: Boolean read GetMostrarGridCitas write SetMostrarGridCitas;
    property MostrarGridAvanzado: Boolean read GetMostrarGridAvanzado write SetMostrarGridAvanzado;
    property MostrarRegistroGrupo: Boolean read GetMostrarRegistroGrupo write SetMostrarRegistroGrupo;

    property ImpresoraDefault: string read GetImpresoraDefault write SetImpresoraDefault;
    property FiltrarTiempo: Boolean read GetFiltrarTiempo write SetFiltrarTiempo;
    property MinutosAntes: Integer read GetMinutosAntes write SetMinutosAntes;
    property MinutosDespues: Integer read GetMinutosDespues write SetMinutosDespues;
    property AutoReg_AsuntoATratar: boolean read GetAutoReg_AsuntoATratar write SetAutoReg_AsuntoATratar;
    property AutoReg_Vehiculo: boolean read GetAutoReg_Vehiculo write SetAutoReg_Vehiculo;
    property AutoReg_Identificacion: boolean read GetAutoReg_Identificacion write SetAutoReg_Identificacion;
    property AutoReg_Observaciones: boolean read GetAutoReg_Observaciones write SetAutoReg_Observaciones;
    property AlertasEnabled: Boolean read FAlertasEnabled;
    property TiempoMinimo: integer read FTiempoMinimo;
    property PuedeModificarLibro: Boolean read FPuedeModificarLibro default TRUE;
    property HayCorteAbierto: Boolean read FHayCorteAbierto;
    property UltimoCorteAbierto: Integer read FUltimoCorteAbierto;
    property FiltroSQL: string read sFiltroSQL write SetFiltroSQL;
    property OPCSQL: integer read iOPCSQL write iOPCSQL;


    procedure HacerBusquedaAnfitrion( oParamList: TZetaParams );
    procedure HacerBusquedaVisitante( oParamList: TZetaParams );
    procedure BuscaVisitanteSQL(iOpc:integer; sFiltro:string);

    procedure SetCasetaDefault;

    procedure AbrirLibro;
    procedure CerrarLibro;

    function RegistraEntrada: Boolean;
    procedure RegistraSalida;
    procedure CancelaSalida;
    function DoRegistraSalida(const iFolio: integer; const lRefrescar: Boolean = TRUE; const lDevGafete: Boolean = FALSE ): Boolean;

    procedure ValidaCamposLibros( DataSet: TDataSet; const lValidaVehiculo: Boolean = TRUE );

    procedure BuscaLibro(const dInicio, dFin: TDate; const sFiltro: string);
    function RevisaAlertas: Boolean;
    procedure CierraDialogoAlerta(oAlerta: TAlerta);
    procedure RegistraLlegada;
    procedure MuestraFotografia; //acl
    function RegistraSalidaGafete( const sGafete: String ): Boolean;
    function BuscaUltimoGafete( const sGafete: String ): Integer;
    function VisitanteAdentro(const iVisitante: Integer; var sMensaje: String): Boolean;
    procedure BuscarVisitante(sNombres: string);
  end;

var
  dmVisitantes: TdmVisitantes;

implementation

uses DCliente,
     DSistema,
     DGlobal,
     FBuscaAnfitriones,
     FBuscaVisitantes,
     FCierraCorte,
     FEditLibros,
     FEditLibrosSalida,
     FFolioSalida,
     FAutoRegistroEntrada,
     FGridSalidas,
     ZGlobalTress,
     ZReconcile,
     ZetaDialogo,
     ZBaseEdicion_DevEx,
 //    ZWizardBasico,
     ZetaRegistryCliente,
     ZVisitantesTools,
     FMostrarFotografia,
     FTressShell,
     FRegistroEntradaVisi, ZBaseEdicion;


{$R *.DFM}


{ ******************* TAlertasMGR ********************}

constructor TAlertasMGR.Create;
begin
     FAlertasMGR := TStringList.Create;
     FDialogos := TList.Create;
     InitPosicion;

end;

destructor TAlertasMGR.Destroy;
begin
     DestruyeDialogos;
     FreeAndNil( FDialogos );
     FreeAndNil( FAlertasMGR );
end;

function TAlertasMGR.GetFolio(const iFolio: integer): string;
begin
     Result := PadLCar( IntToStr( iFolio ), K_ANCHO_FOLIO_MGR,'0' )
end;

procedure TAlertasMGR.AgregaFolio(const iFolio: integer);
begin
     FAlertasMGR.Add( GetFolio( iFolio ) );
end;

procedure TAlertasMGR.BorraFolio(const iFolio: integer);
 var
    Indice : integer;
begin
     Indice := FAlertasMGR.IndexOf( GetFolio( iFolio ) );
     if Indice >= 0 then
        FAlertasMGR.Delete( Indice );
end;

function TAlertasMGR.ExisteAlerta(const iFolio: integer): Boolean;
begin
     Result := FAlertasMGR.IndexOf( GetFolio( iFolio ) ) >= 0;
end;

procedure TAlertasMGR.InitPosicion;
begin
     FTop := Trunc( Screen.Monitors[0].Height/2) ;
     FLeft := Trunc( Screen.Monitors[0].Width/2);
end;

function TAlertasMGR.ObtenerNombreAnfi(iAnfi: integer): string;
begin
     with dmVisitantes.cdsAnfitrion do
     begin
          if Locate('AN_NUMERO', iAnfi, []) then
          begin
               Result := FieldByName('AN_NOMBRES').AsString +' '+ FieldByName('AN_APE_PAT').AsString+
                         ' '+fieldByName('AN_APE_MAT').AsString;
          end;
     end;
end;

procedure TAlertasMGR.AjustaPosicion;
 const K_OFFSET = 30;
begin
     if ( ( FTop + K_ALTO_FORMA + K_OFFSET) > Screen.Monitors[0].Height )  OR
          ( ( FlEFT + K_ANCHO_FORMA+ K_OFFSET ) > Screen.Monitors[0].Width ) then
        InitPosicion
     else
     begin
          FTop := FTop + K_DIAGONAL;
          FLeft := FLeft + K_DIAGONAL;
     end;
end;

procedure TAlertasMGR.ShowDialog;
 var
    oAlerta : TAlerta;
    iFolio: integer;
begin
     DestruyeDialogos;

     iFolio := DataSet.FieldByName('LI_FOLIO').AsInteger;

     if NOT ExisteAlerta( iFolio ) then
     begin
          oAlerta := TAlerta.Create( NIL );
          with oAlerta do
          begin
               //Parent := Application.Mainform;
               AjustaPosicion;

               Top := FTop;
               Left := FLeft;
               
               with DataSet do
               begin
                    Folio := iFolio;
                    Visitante := FieldByName('NOMBRE_VISITANTE').AsString;
                    Empresa:=  FieldByName('EV_NOMBRE').AsString;
                    Anfitrion:= FieldByName('NOMBRE_ANFITRIO').AsString;
                    Departamento := FieldByName('NOMBRE_DEPTO').AsString;
                    Telefono:= FieldByName('AN_TEL').AsString;
                    Entrada:= FieldByName('LI_ENT_HOR').AsString;

               end;

               AgregaFolio( iFolio );
               Show;
          end;
     end;
end;

procedure TAlertasMGR.DestruyeDialogos;
 var
    oAlerta: TAlerta;
begin
     with FDialogos do
     begin
          while ( Count > 0 ) do
          begin
               oAlerta := FDialogos.List[ Count - 1 ];
               FDialogos.Delete( Count - 1 );
               FreeAndNil( oAlerta );
          end;
     end;
end;

procedure TAlertasMGR.CloseDialog( oAlerta:TAlerta );
begin
     BorraFolio( oAlerta.Folio );
     FDialogos.Add( oAlerta );
end;


{ ******************* TdmVisitantes ********************}

procedure TdmVisitantes.DataModuleCreate(Sender: TObject);
begin
     cdsCaseta.Tag := K_CASETA;
     cdsTipoAsunto.Tag :=  K_TIPO_ASUNTO;
     cdsAnfitrion.Tag := K_ANFITRION;
     cdsDepto.Tag := K_DEPTO;
     cdsVisitante.Tag := K_VISITANTE;
     cdsEmpVisitante.Tag := K_EMP_VISI;
     cdsTipoID.Tag :=  K_TIPO_ID;
     cdsTipoCarro.Tag :=  K_TIPO_CARRO;
     cdsTipoVisita.Tag :=  K_TIPO_VISITA;
     cdsCortes.Tag := K_CORTE;
     cdsLibros.Tag :=  K_LIBRO;
     cdsLibrosSalidas.Tag :=  K_LIBRO;
     FiltroSQL:=' ';
     {cdsCondiciones.Tag := K_CONDICIONES;
     cdsCaseta.Tag := K_CASETA;
     cdsCitas.Tag := K_EDIT_CITA;
     }

end;

procedure TdmVisitantes.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FAlertasMGR );
end;


{$ifdef DOS_CAPAS}
function TdmVisitantes.GetServidor: TdmServerVisitantes;
begin
     Result := DCliente.dmCliente.ServerVisitantes;
end;
{$else}
function TdmVisitantes.GetServidor: IdmServerVisitantesDisp;
begin
     Result := IdmServerVisitantesDisp( dmCliente.CreaServidor( CLASS_dmServerVisitantes, FServidor ) );
end;
{$endif}

procedure TdmVisitantes.cdsCatalogoAlAdquirirDatos(Sender: TObject);
begin
   with TZetaClientDataSet( Sender ) do
   begin
        Data := Servidor.GetCatalogo( dmCliente.Empresa, Tag );
   end;
end;

function TdmVisitantes.GetCaseta: string;
begin
     Result := cdsCaseta.FieldByName('CA_CODIGO').AsString;
end;

function TdmVisitantes.InitCaseta( const lLeeRegistry: Boolean ): Integer;
begin
     with cdsCaseta do
     begin
          //Siempre hay refrescar;
          Data := Servidor.GetCatalogo( dmCliente.Empresa, Tag );
          Filtered := TRUE;
          Filter := Format( 'CA_STATUS = %d', [ Ord( cfActiva ) ] );

          ResetDataChange;

          if lLeeRegistry and StrLLeno( ClientRegistry.CasetaDefault ) then
          begin
               if Locate('CA_CODIGO',ClientRegistry.CasetaDefault,[]) then
                  Result := 1
               else
                   Result := RecordCount;
          end
          else
              Result := RecordCount;
     end;
end;

procedure TdmVisitantes.SetCasetaDefault;
begin
     ClientRegistry.CasetaDefault := cdsCaseta.FieldByName('CA_CODIGO').AsString;
end;


function TdmVisitantes.FindCaseta( const Value: String ): Boolean;
begin
     with cdsCaseta do
     begin
          Result := Locate( 'CA_CODIGO', Value, [] );
     end;
end;

procedure TdmVisitantes.cdsCasetaAlCrearCampos(Sender: TObject);
begin
     with cdsCaseta do
     begin
          ListaFija( 'CA_STATUS', lfCasetaStatus );
          with FieldByName('CA_ABIERTA') do
          begin
               OnGetText := ObtieneAbiertaCaseta;
               Alignment := taLeftJustify;
          end;
     end;
end;



procedure TdmVisitantes.ObtieneAbiertaCaseta(Sender: TField; var Text: String;DisplayText: Boolean);
begin
     if DisplayText then
     begin
          with cdsCaseta do
          begin
               if IsEmpty then
                  Text := VACIO
               else
               begin
                    if( Sender.AsString = 'S' )then
                        Text := 'Abierta'
                    else
                        Text := 'Cerrada';
               end;
          end;
     end
     else
         Text:= Sender.AsString;
end;

function TdmVisitantes.GetCorteAnterior: Boolean;
begin
     Result := FetchPreviousCorte( FCorte );
     if Result then
     begin
          with cdsCortes do
          begin
               FCorte := FieldByName( 'CO_FOLIO' ).AsInteger;
          end;
          FCorteEOF := False;
     end
     else
         FCorteBOF := True;
end;

function TdmVisitantes.GetCorteInicial: Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.CorteSiguiente( dmCliente.Empresa, Caseta, dmCliente.Usuario, 0, Datos );
     with cdsCortes do
     begin
          Data := Datos;
          FCorte := FieldByName( 'CO_FOLIO' ).AsInteger;
     end;
     FCorteBOF := True;
end;

function TdmVisitantes.GetCortePrimero: Boolean;
var
   iNewCorte: TNumEmp;
begin
     Result := FetchNextCorte( 0 );
     if Result then
     begin
          with cdsCortes do
          begin
               iNewCorte := FieldByName( 'CO_FOLIO' ).AsInteger;
               FCorte := iNewCorte;
               FCorteBOF := ( iNewCorte = FCorte );
               FCorteEOF := False;
          end;
     end;
end;

function TdmVisitantes.GetCorteSiguiente: Boolean;
begin
     Result := FetchNextcORTE( FCorte );
     if Result then
     begin
          with cdsCortes do
          begin
               FCorte := FieldByName( 'CO_FOLIO' ).AsInteger;
          end;
          FCorteBOF := False;
     end
     else
         FCorteEOF := True;
end;

function TdmVisitantes.GetCorteUltimo: Boolean;
var
   iNewCorte: TNumEmp;
begin
     Result := FetchPreviousCorte( MAXINT );
     if Result then
     begin
          with cdsCortes do
          begin
               iNewCorte := FieldByName( 'CO_FOLIO' ).AsInteger;
               FCorteBOF := False;
               FCorte := iNewCorte;
               FCorteEOF := ( iNewCorte = FCorte );
          end;
     end;
end;

function TdmVisitantes.CorteDownEnabled: Boolean;
begin
     with cdsCortes do
     begin
          Result := Active and not IsEmpty and not FCorteBOF;
     end;
end;

function TdmVisitantes.CorteUpEnabled: Boolean;
begin
     with cdsCortes do
     begin
          Result := Active and not IsEmpty and not FCorteEOF;
     end;
end;


function TdmVisitantes.FetchCorte(const iCorte: TNumEmp): Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.CorteActivo( dmCliente.Empresa, iCorte, Datos );
     if Result then
     begin
          with cdsCortes do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmVisitantes.FetchNextCorte(const iCorte: TNumEmp): Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.CorteSiguiente( dmCliente.Empresa, Caseta, dmCliente.Usuario, iCorte, Datos );
     if Result then
     begin
          with cdsCortes do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmVisitantes.FetchPreviousCorte(const iCorte: TNumEmp): Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.CorteAnterior( dmCliente.Empresa, Caseta, dmCliente.Usuario, iCorte, Datos );
     if Result then
     begin
          with cdsCortes do
          begin
               Data := Datos;
          end;
     end;
end;

procedure TdmVisitantes.InitActivosCorte;
 var
    Datos : Olevariant;
begin
     FCorteBOF := False;
     FCorteEOF := False;
     FCorte := 0;

     with cdsCortes do
     begin
          Servidor.CorteActivo( dmCliente.Empresa, 0, Datos );
          Data := Datos;
     end;

     GetCorteUltimo;

     FHayCorteAbierto := CorteActivo;
     FUltimoCorteAbierto := cdsCortes.FieldByName('CO_FOLIO').AsInteger;

     with Global do
     begin
          FAlertasEnabled := GetGlobalBooleano( K_GLOBAL_ALERTAS_ACTIVADA );
          FTiempoMinimo := GetGlobalInteger( K_GLOBAL_ALERTAS_TIEMPO_MINIMO );

          if FAlertasEnabled then
          begin
               FAlertasMGR := TAlertasMGR.Create;
               FAlertasMGR.DataSet := cdsAlertas;
          end;
     end;
end;

procedure TdmVisitantes.CargaCitas;
begin
     cdsCitas.Refrescar;
end;

procedure TdmVisitantes.CargaLibro;
begin
     with cdsLibros do
     begin
          DisableControls;
          try
             Refrescar;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TdmVisitantes.cdsCitasAlAdquirirDatos(Sender: TObject);
 var
    rHora: Integer;
    sHoraIni, sHoraFin: string;
begin
     if ( MostrarGridCitas ) then
     begin
          rHora := aMinutos( HoraAsStr( dmCliente.HoraDefault ) );
          if ( FiltrarTiempo ) then
          begin
               { Se cambio para que el valor de los minutos fuera configurable
               sHoraIni := aHoraString( rHora - 20 );
               sHoraFin := aHoraString( rHora + 20 ); }
               sHoraIni := aHoraString( rHora - MinutosAntes );
               sHoraFin := aHoraString( rHora + MinutosDespues );
          end;

          cdsCitas.Data := Servidor.GetCitas( dmCliente.Empresa,
                                              dmCliente.FechaDefault, dmCliente.FechaDefault, VACIO,
                                              sHoraIni, sHoraFin );
     end;
end;

procedure TdmVisitantes.cdsCitasAlCrearCampos(Sender: TObject);
begin
     cdsTipoAsunto.Conectar;
     cdsAnfitrion.Conectar;
     cdsVisitante.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with cdsCitas do
     begin
          MaskTime( 'CI_HORA' );
          ListaFija( 'CI_STATUS', lfCitaStatus );
          CreateSimpleLookup( cdsTipoAsunto, 'TB_ELEMENT', 'CI_ASUNTO' );
          CreateSimpleLookup(dmSistema.cdsUsuarios,'US_NOMBRE','US_CODIGO');
          with FieldByName('AN_NUMERO') do
          begin
               OnGetText := ObtieneNombreAnfi;
               Alignment := taLeftJustify;
          end;

          with FieldByName('VI_NUMERO') do
          begin
               OnGetText := ObtieneNombreVisit;
               Alignment := taLeftJustify;
          end;
          with FieldByName('CI_STATUS') do
          begin
               OnGetText := ObtieneStatusVisit;
               Alignment := taLeftJustify;
          end;
     end;
end;

procedure TdmVisitantes.ObtieneNombreAnfi(Sender: TField; var Text: String;DisplayText: Boolean);
begin
   if DisplayText then
   begin
        with cdsCitas do
        begin
             if IsEmpty then
                Text := VACIO
             else
             begin
                  Text := cdsAnfitrion.GetDescripcion( Sender.AsString );
             end;
        end;
   end
   else
         Text:= Sender.AsString;
end;

procedure TdmVisitantes.ObtieneNombreVisit(Sender: TField; var Text: String;DisplayText: Boolean);
begin
if DisplayText then
begin
  with Sender.DataSet do
  begin
       if IsEmpty then
          Text := VACIO
       else
       begin
            Text := FieldByName('VI_NOMBRES').AsString+' '+FieldByName('VI_APE_PAT').AsString+' '+FieldByName('VI_APE_MAT').AsString;
       end;
  end;
end
else
 Text:= Sender.AsString;
end;

procedure TdmVisitantes.ObtieneStatusVisit(Sender: TField; var Text: String;DisplayText: Boolean);
var
   strLlego:string;
begin
     if DisplayText then
     begin
          with Sender.DataSet do
          begin
               if FieldByName('CI_STATUS').AsInteger = 0 then
                  strLlego := 'Si'
               else if FieldByName('CI_STATUS').AsInteger = 1 then
                   strLlego := 'No'
               else if FieldByName('CI_STATUS').AsInteger = 2  then
                    strLlego := 'Cancelada';
               if IsEmpty then
                  Text := VACIO
               else
               begin
                    Text := 'Lleg�: ' + strLlego;
               end;
          end;
     end
     else
         Text:= Sender.AsString;
end;


procedure TdmVisitantes.cdsAnfitrionAlCrearCampos(Sender: TObject);
begin
     cdsDepto.Conectar;
     with cdsAnfitrion do
     begin
          ListaFija( 'AN_STATUS', lfAnfitrionStatus );
          CreateSimpleLookup( cdsDepto, 'TB_ELEMENT', 'AN_DEPTO' );
     end;
end;

procedure TdmVisitantes.cdsAnfitrionLookupAlCrearCampos(Sender: TObject);
begin
     cdsDepto.Conectar;
     with cdsAnfitrionLookup do
     begin
          ListaFija( 'AN_STATUS', lfAnfitrionStatus );
          CreateSimpleLookup( cdsDepto, 'TB_ELEMENT', 'AN_DEPTO' );
     end;
end;

procedure TdmVisitantes.cdsAnfitrionLookupDescription(Sender: TZetaLookupDataSet; var sDescription: String);
begin
     with cdsAnfitrion do
     begin
          sDescription := FieldByName( 'AN_NOMBRES' ).AsString  +' '+
                               FieldByName( 'AN_APE_PAT' ).AsString +' '+
                               FieldByName( 'AN_APE_MAT' ).AsString;

     end;
end;


procedure TdmVisitantes.cdsAnfitrionLookupSearch(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String;var sKey, sDescription: String);
var
   iNumero: Integer;
begin
     iNumero := StrToIntDef( sKey, 0 );
     lOk := FBuscaAnfitriones.BuscaAnfitrionDialogo( iNumero, sDescription );

     if lOk then
        sKey := IntToStr( iNumero );
end;
 //acl
procedure TdmVisitantes.cdsVisitanteLookupAlCrearCampos(Sender: TObject);
begin
     cdsTipoVisita.Conectar;
     cdsAnfitrion.Conectar;
     cdsEmpVisitante.Conectar;

     with cdsVisitanteLookup do
     begin
          ListaFija( 'VI_STATUS', lfVisitanteStatus );
          CreateSimpleLookup( cdsTipoVisita, 'TB_ELEMENT', 'VI_TIPO' );
          CreateSimpleLookup( cdsEmpVisitante, 'EV_NOMBRE', 'EV_NUMERO' );
          CreateSimpleLookup( cdsAnfitrion, 'AN_NOMBRE', 'AN_NUMERO' );
          CreateLookup( cdsEmpVisitante, 'EV_STATUS', 'EV_NUMERO','EV_NUMERO','EV_STATUS');
          FieldByName( 'PRETTY_VISIT' ).OnGetText := ObtieneStatusVis;
          FieldByName( 'EV_NOMBRE' ).OnGetText := ObtieneCompany;
     end;

end;
 //acl
procedure TdmVisitantes.ObtieneStatusVis(Sender: TField; var Text: String;DisplayText: Boolean);
var
   StatusVis : eVisitanteStatus;
   sStatusDesc : String;
begin
     if DisplayText then
     begin
          StatusVis := eVisitanteStatus( cdsVisitanteLookup.FieldByName( 'VI_STATUS' ).AsInteger );
          if ( StatusVis = vfSuspendido ) then
          begin
               sStatusDesc := ZetaCommonLists.ObtieneElemento( lfVisitanteStatus, Ord(StatusVis) );
               Text := cdsVisitanteLookup.FieldByName( 'PRETTY_VISIT' ).asString + '(' + sStatusDesc + ')'
          end
          else
             Text := Sender.AsString;
     end
     else
         Text := Sender.AsString;
end;

//acl
procedure TdmVisitantes.ObtieneCompany(Sender: TField; var Text: String;DisplayText: Boolean);
var
   StatusCompany : eCatCompanys;
   sStatusCompany : String;
begin
     if DisplayText then
     begin
          StatusCompany := eCatCompanys( cdsVisitanteLookup.FieldByName( 'EV_STATUS' ).AsInteger );
          if ( StatusCompany = ccSuspendida ) then
          begin
               sStatusCompany := ZetaCommonLists.ObtieneElemento( lfCatCompanys, Ord(StatusCompany) );
               Text := cdsVisitanteLookup.FieldByName( 'EV_NOMBRE' ).asString + '(' + sStatusCompany + ')'
          end
          else
             Text := Sender.AsString;
     end
     else
         Text := Sender.AsString;     
end;

procedure TdmVisitantes.ObtieneBuscaCompany(Sender: TField; var Text: String;DisplayText: Boolean);
var
   StatusCompany : eCatCompanys;
   sStatusCompany : String;
begin
     if DisplayText then
     begin
          StatusCompany := eCatCompanys( cdsCompaniaLookup.FieldByName( 'EV_STATUS' ).AsInteger );
          if ( StatusCompany = ccSuspendida ) then
          begin
               sStatusCompany := ZetaCommonLists.ObtieneElemento( lfCatCompanys, Ord(StatusCompany) );
               Text := cdsCompaniaLookup.FieldByName( 'EV_NOMBRE' ).asString + '(' + sStatusCompany + ')'
          end
          else
             Text := Sender.AsString;
     end
     else
         Text := Sender.AsString;     
end;

procedure TdmVisitantes.cdsVisitanteAlCrearCampos(Sender: TObject);
begin
     cdsTipoVisita.Conectar;
     cdsEmpVisitante.Conectar;
     with cdsVisitante do
     begin
          ListaFija( 'VI_STATUS', lfVisitanteStatus );
          ListaFija( 'VI_SEXO', lfSexoDesc );
          CreateSimpleLookup( cdsTipoVisita, 'TB_ELEMENT', 'VI_TIPO' );
          CreateSimpleLookup( cdsEmpVisitante, 'EV_NOMBRE', 'EV_NUMERO' );
     end;
end;

procedure TdmVisitantes.cdsVisitanteLookupDescription(
  Sender: TZetaLookupDataSet; var sDescription: String);
begin
     with cdsVisitante do
     begin
          sDescription := Trim( FieldByName( 'VI_APE_PAT' ).AsString ) + ' ' +
                          Trim( FieldByName( 'VI_APE_MAT' ).AsString ) + ', ' +
                          Trim( FieldByName( 'VI_NOMBRES' ).AsString ) ;
     end;

end;

procedure TdmVisitantes.cdsVisitanteLookupSearch( Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String;
  var sKey, sDescription: String);
var
   iNumero: Integer;
begin
     iNumero := StrToIntDef( sKey, 0 );
     lOk := FBuscaVisitantes.BuscaVisitanteDialogo( iNumero, sDescription );

     if lOk then
        sKey := IntToStr( iNumero );

end;
//acl
procedure TdmVisitantes.cdsEmpVisitanteAlCrearCampos(Sender: TObject);
begin
     with TZetaLookupDataset(Sender) do
     begin
          ListaFija( 'EV_STATUS', lfCatCompanys );
          FieldByName( 'EV_NOMBRE' ).OnGetText := ObtieneBuscaCompany;
     end;
end;

procedure TdmVisitantes.HacerBusquedaAnfitrion( oParamList: TZetaParams );
begin
     cdsAnfitrionLookup.Data := Servidor.BuscaAnfitrion( dmCliente.Empresa, oParamList.VarValues );
end;

procedure TdmVisitantes.HacerBusquedaVisitante( oParamList: TZetaParams );
begin
     cdsVisitanteLookup.Data := Servidor.BuscaVisitante( dmCliente.Empresa, oParamList.VarValues );
end;

procedure TdmVisitantes.cdsLibrosAlAdquirirDatos(Sender: TObject);
begin
     with cdsLibros do
     begin
          if FCorte <> 0 then begin
             if MostrarGridAvanzado  then
                Data := Servidor.GetLibroActivoFoto(dmCliente.Empresa, FCorte)
             else Data := Servidor.GetLibroActivo(dmCliente.Empresa, FCorte);
          end
          else if Active then
               EmptyDataset;
     end;
end;

procedure TdmVisitantes.cdsLibrosAlCrearCampos(Sender: TObject);
begin
     cdsCaseta.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     cdsEmpVisitante.Conectar;
     cdsDepto.Conectar;
     cdsAnfitrion.Conectar;
     cdsVisitante.Conectar;
     cdsTipoId.Conectar;
     cdsTipoAsunto.Conectar;
     with TZetaClientDataSet( Sender ) do
     begin
          MaskTime( 'LI_ENT_HOR' );
          MaskTime( 'LI_SAL_HOR' );
          ListaFija( 'LI_STATUS', lfLibroStatus );
          CreateSimpleLookup( cdsCaseta, 'CA_NOM_ENT', 'LI_ENT_CAS' );
          CreateSimpleLookup( cdsCaseta, 'CA_NOM_SAL', 'LI_SAL_CAS' );
          CreateSimpleLookup( cdsEmpVisitante, 'EV_NOMBRE', 'EV_NUMERO' );
          CreateSimpleLookup( cdsDepto, 'TB_ELEMENT', 'LI_CDEPTO' );
          CreateSimpleLookup( cdsTipoId, 'ID_NOMBRE', 'LI_TIPO_ID' );
          CreateSimpleLookup( cdsTipoAsunto, 'TIPO_ASUNTO', 'LI_ASUNTO' );
          CreateCalculated( 'ENT_SAL', ftString, 40 );
          CreateCalculated( 'Visitante', ftString, 100 );
          CreateCalculated( 'VisitandoA', ftString, 100 );
          CreateCalculated( 'RegisEntSal', ftString, 100 );
          CreateCalculated('Identificacion',FTString,100);
          CreateCalculated('NombreCasEnt',FTString,100);
          CreateCalculated('LI_WARN_SAL',FTString,100);
          with FieldByName('LI_SAL_FEC') do
          begin
               OnGetText := QuitaFechasNulas;
               Alignment := taRightJustify;
          end;
          with FieldByName('LI_ENT_FEC') do
          begin
               OnGetText := QuitaFechasNulas;
               Alignment := taRightJustify;
          end;
          with FieldByName('AN_NUMERO') do
          begin
               OnGetText := ObtieneNombreAnfi;
               Alignment := taLeftJustify;
          end;
          with FieldByName('VI_NUMERO') do
          begin
               OnGetText := ObtieneNombreVisit;
               Alignment := taLeftJustify;
          end;

          CreateSimpleLookup( dmSistema.cdsUsuarios, 'US_ENT_LIB', 'LI_ENT_VIG' );
          CreateSimpleLookup( dmSistema.cdsUsuarios, 'US_SAL_LIB', 'LI_SAL_VIG' );
     end;

end;

procedure TdmVisitantes.QuitaFechasNulas(Sender: TField; var Text: String;DisplayText: Boolean );
begin
     if DisplayText then
     begin
          if( Sender.AsDateTime = NullDateTime )then
              Text := VACIO
          else
              Text:= Sender.AsString;
     end
     else
         Text:= Sender.AsString;
end;


procedure TdmVisitantes.AbrirLibro;
begin
     with cdsCortes do
     begin
          Append;
          FieldByName('CA_CODIGO').AsString := Caseta;
          FieldByName('CO_INI_FEC').AsDateTime := Date;
          FieldByName('CO_INI_HOR').AsString := HoraAsStr( dmCliente.HoraDefault );
          FieldByName('CO_INI_VIG').AsInteger := dmCliente.Usuario;
          Enviar;
          FCorte := FieldByName('CO_FOLIO').AsInteger;
          FHayCorteAbierto := TRUE;
          FUltimoCorteAbierto := FCorte;
     end;

end;

procedure TdmVisitantes.CerrarLibro;
var
sCaseta :String; //DChavez: Variable para guardar el i de la caseta activa
begin
     try
        with cdsCortes do
        begin
             GetCorteUltimo;//DACP
             Edit;
             FieldByName('CO_FIN_FEC').AsDateTime := Date;
             FieldByName('CO_FIN_HOR').AsString   := HoraAsStr( Now );
             FieldByName('CO_FIN_VIG').AsInteger := dmCliente.Usuario;  //DChavez: Agregamos el usuario que cierra el libro  
             FUltimoCorteAbierto := cdsCortes.FieldByName('CO_FOLIO').AsInteger;
             sCaseta:=FieldByName('CA_CODIGO').AsString;    //Dchavez: obtenemos la caseta activa
        end;
        with cdsCaseta do
        begin
             conectar;
             if cdsCaseta.Locate( 'CA_CODIGO', sCaseta, [] ) then  //buscamos la caseta
             begin
                  edit;
                  FieldByName('CA_CORTE').AsInteger :=   FUltimoCorteAbierto;  //y asignamos el ultimo corte para en el Forma FcierraCorte en el evento ok_DevEx hacer el envio.
             end;
        end;

        if CierraCorte = NIL then
           CierraCorte := TCierraCorte.Create( self );
        with CierraCorte do
        begin
             ShowModal;
        end;
     finally
            FetchCorte( cdsCortes.FieldByName('CO_FOLIO').AsInteger );
            FHayCorteAbierto := FALSE;
            FUltimoCorteAbierto := 0;
     end;
end;


procedure TdmVisitantes.cdsCatalogoAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     with TZetaClientDataSet( Sender ) do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
             Reconcile( Servidor.GrabaCatalogo( dmCliente.Empresa, Delta, Tag, ErrorCount ) );
     end;

end;

procedure TdmVisitantes.cdsCortesAlCrearCampos(Sender: TObject);
begin
     cdsCaseta.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with cdsCortes do
     begin
          MaskTime( 'CO_INI_HOR' );
          MaskTime( 'CO_FIN_HOR' );
          CreateSimpleLookup( cdsCaseta, 'CA_NOMBRE', 'CA_CODIGO' );
          CreateSimpleLookup( dmSistema.cdsUsuarios, 'US_INI_CORTE', 'CO_INI_VIG' );
          CreateSimpleLookup( dmSistema.cdsUsuarios, 'US_FIN_CORTE', 'CO_FIN_VIG' );
     end;
end;

function TdmVisitantes.GetCorteActivo: Boolean;
begin
     with cdsCortes do
          Result := Active AND (NOT IsEmpty) AND ( FieldByName('CO_FIN_FEC').AsDateTime = NullDateTime );
end;


procedure TdmVisitantes.cdsLibrosLookupAlAdquirirDatos(Sender: TObject);
 var
    dFinal: TDateTime;
begin
     with cdsCortes do
     begin
          dFinal := FieldByName('CO_FIN_FEC').AsDateTime;
          if dFinal = NullDateTime then
             dFinal := FieldByName('CO_INI_FEC').AsDateTime;

          cdsLibrosLookup.Data := Servidor.GetLibros( dmCliente.Empresa, FieldByName('CO_INI_FEC').AsDateTime, dFinal, VACIO );
     end;
end;

procedure TdmVisitantes.cdsTablaAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     with TZetaClientDataSet( Sender ) do
     begin
          PostData;
          if ( ChangeCount > 0 ) then		
             Reconcile( Servidor.GrabaLibro( dmCliente.Empresa, Delta, Tag, Now, ErrorCount ) ); {ACL280409}
     end;
end;

function TdmVisitantes.RegistraEntrada: Boolean;
begin
     with cdsLibros do
     begin
          if not ( State in [dsInsert] ) then
             Append;
 //:todo         Result := ZWizardBasico.ShowWizard( TAutoRegistroEntrada );
          if FRegistroEntradaVisit = NIL then
              FRegistroEntradaVisit := TFRegistroEntradaVisit.Create( self );
          FRegistroEntradaVisit.ShowModal;
          if NOT Result and ( State in [dsEdit,dsInsert] ) then
             Cancel;
     end;
end;

{function TdmVisitantes.GetFolioSalida( var iFolio: integer ): Boolean;
begin
     if FolioSalida = NIL then
        FolioSalida := TFolioSalida.Create( self );

     with FolioSalida do
     begin
          Folio := cdsLibros.FieldByName('LI_FOLIO').AsInteger;
          ShowModal;
          Result := ModalResult = mrOk;
          iFolio := Folio;
     end;
end;}

function TdmVisitantes.DoRegistraSalida(const iFolio: integer; const lRefrescar: Boolean; const lDevGafete: Boolean ): Boolean;
begin

     if EditLibrosSalida = nil then
        EditLibrosSalida := TEditLibrosSalida.Create(Self);
     if EditLibrosSalida.Visible = true then
        ZError('Error en visitantes', '! Se encontr� un error !'+#13+'No se puede registrar la salida, finalice antes el proceso en curso', 0)
     else
     begin
          Result := FALSE;

          with cdsLibrosSalidas do
          begin
               if lRefrescar then
                  Refrescar;

               if Locate( 'LI_FOLIO', iFolio, []) then
               begin
                    Edit;
                    FieldByName('LI_SAL_CAS').AsString := Caseta;
                    FieldByName('LI_SAL_COR').AsInteger := Corte;
                    FieldByName('LI_SAL_VIG').AsInteger := dmCliente.Usuario;
                    FieldByName('LI_STATUS').AsInteger := Ord(lsFuera);
                    FieldByName('LI_DEVGAF').AsString := zBoolToStr( lDevGafete);

                    ZBaseEdicion_DevEx.ShowFormaEdicion( EditLibrosSalida, TEditLibrosSalida );
                    Result := ( EditLibrosSalida.ModalResult = mrOK );

                    if Result then
                    begin
                         cdsLibros.DisableControls;
                         try
                            cdsLibros.Refrescar;
                            cdsLibros.Locate('LI_FOLIO',iFolio, []);
                         finally
                                cdsLibros.EnableControls;
                         end;
                    end;

               end
               else
                   ZetaDialogo.ZInformation( 'Registro de salida', 'No se encontr� el folio especificado', 0 )
          end;
     end;
end;

procedure TdmVisitantes.RegistraSalida;
 var
    iFolio: integer;
begin
     with cdsLibrosSalidas do
     begin
          Refrescar;

          if GridSalidas = NIL then
             GridSalidas := TGridSalidas.Create( self );

          GridSalidas.ShowModal;
          if GridSalidas.ModalResult = mrOk then
          begin
               iFolio := GridSalidas.iPFolio;

               if ( iFolio <> 0 ) then
                  DoRegistraSalida( iFolio, FALSE );
          end;

     end;
end;

function TdmVisitantes.RegistraSalidaGafete( const sGafete: String ): Boolean;
 var
    iFolio: integer;
begin
     Result := FALSE;
     with cdsLibrosSalidas do
     begin
          Refrescar;

          iFolio := BuscaUltimoGafete( sGafete );
          if iFolio <> 0 then
              DoRegistraSalida( iFolio, FALSE, TRUE)
          else
              ZetaDialogo.ZInformation( 'Registro de salida', 'No se encontr� visitante con el gafete # ' + sGafete, 0 );

     end;
end;

function TdmVisitantes.BuscaUltimoGafete( const sGafete: String ): Integer;
begin
     Result := 0;
     with cdsLibrosSalidas do
     begin
          Last;
          while ( not BOF ) do
          begin
               if ( FieldByName('LI_GAFETE').AsString = sGafete ) then
               begin
                    Result := FieldByName('LI_FOLIO').AsInteger;
                    break;
               end;
               Prior;
          end;
     end;
end;

procedure TdmVisitantes.BuscaVisitanteSQL(iOpc:integer; sFiltro:string);
begin
     cdsSQL.Data := Servidor.GetSQL( dmCliente.Empresa, iOpc, sFiltro);
end;

procedure TdmVisitantes.CancelaSalida;
begin
     with cdsLibros do
     begin
          Edit;
          FieldByName('LI_SAL_CAS').AsString := VACIO;
          FieldByName('LI_SAL_COR').AsInteger := 0;
          FieldByName('LI_SAL_VIG').AsInteger := 0;
          FieldByName('LI_SAL_FEC').AsDateTime := NullDateTime;
          FieldByName('LI_SAL_HOR').AsString   := VACIO;
          FieldByName('LI_STATUS').AsInteger := Ord(lsDentro);
          Enviar;
     end;
end;

procedure TdmVisitantes.cdsLibrosNewRecord(DataSet: TDataSet);
begin
     with cdsLibros do
     begin
          FieldByName('LI_ENT_VIG').AsInteger := dmCliente.Usuario;
          FieldByName('LI_ENT_CAS').AsString := Caseta;
          FieldByName('LI_ENT_COR').AsInteger := Corte;
          FieldByName('LI_ENT_FEC').AsDateTime := Date;
          FieldByName('LI_ENT_HOR').AsString := HoraAsStr(dmCliente.HoraDefault);
          FieldByName('LI_STATUS').AsInteger := Ord(lsDentro);
          FieldByName('LI_STATUS').AsInteger := Ord(lsDentro);
     end;
end;

procedure TdmVisitantes.cdsLibrosSalidasAlAdquirirDatos(Sender: TObject);
begin
     cdsLibrosSalidas.Data := Servidor.GetLibroSalidas( dmCliente.Empresa );
end;

procedure TdmVisitantes.cdsSQLAlAdquirirDatos(Sender: TObject);
begin
     with cdsSQL do
     begin
     Data := Servidor.GetSQL(dmCliente.Empresa,OPCSQL, GetFiltroSQL);
     if not Active then
        EmptyDataset;
     end;
end;

procedure TdmVisitantes.cdsSQLAlCrearCampos(Sender: TObject);
begin
     with cdsSQL do
     begin
          CreateCalculated( 'VI_PRETYNAME', ftString, 120 );
     end;
end;

procedure TdmVisitantes.cdsSQLCalcFields(DataSet: TDataSet);
var
   sSuspendido:string;
begin
     sSuspendido := '';
     with cdsSQL do
     begin
          if FieldByName('VI_STATUS').AsString = '1' then
             sSuspendido := ' (Suspendido)';
          FieldByName('VI_PRETYNAME').AsString := FieldByName('VI_NOMBRES').AsString +' '+FieldByName('VI_APE_PAT').AsString+' '+
          FieldByName('VI_APE_MAT').AsString + sSuspendido
     end;
end;

procedure TdmVisitantes.cdsLibrosAlModificar(Sender: TObject);
begin
     Sleep(200);
     FPuedeModificarLibro := FALSE;
     try
        ZBaseEdicion_DevEx.ShowFormaEdicion( EditLibros, TEditLibros );
     finally
            FPuedeModificarLibro := TRUE;
     end;
end;

function TdmVisitantes.GetImpresionGafete: eImpresionGafete;
begin
     Result := eImpresionGafete( ClientRegistry.ImpresionGafete );
end;

function TdmVisitantes.GetMostrarGridAvanzado: Boolean;
begin
    Result := ClientRegistry.MostrarGridAvanzado;
end;

function TdmVisitantes.GetMostrarGridCitas: Boolean;
begin
     Result := ClientRegistry.MostrarGridCitas;
end;

function TdmVisitantes.GetMostrarRegistroGrupo: Boolean;
begin
     Result := ClientRegistry.MostrarRegistroGrupo;
end;

function TdmVisitantes.GetImpresoraDefault: string;
begin
     Result := ClientRegistry.ImpresoraDefault;
end;

function TdmVisitantes.GetAutoReg_AsuntoATratar: boolean;
begin
     Result := ClientRegistry.AutoReg_AsuntoATratar;
end;

function TdmVisitantes.GetAutoReg_Identificacion: boolean;
begin
     Result := ClientRegistry.AutoReg_Identificacion;
end;

function TdmVisitantes.GetAutoReg_Observaciones: boolean;
begin
     Result := ClientRegistry.AutoReg_Observaciones;
end;

function TdmVisitantes.GetAutoReg_Vehiculo: boolean;
begin
     Result := ClientRegistry.AutoReg_Vehiculo;
end;

function TdmVisitantes.GetFiltrarTiempo: Boolean;
begin
      Result := ClientRegistry.FiltrarTiempo;
end;

function TdmVisitantes.GetFiltroSQL: string;
begin
    Result := FiltroSQL;
end;

function TdmVisitantes.GetMinutosAntes: Integer;
begin
     Result := ClientRegistry.MinutosAntes;
end;

function TdmVisitantes.GetMinutosDespues: Integer;
begin
     Result := ClientRegistry.MinutosDespues;
end;

procedure TdmVisitantes.SetAutoReg_AsuntoATratar(const Value: boolean);
begin
     ClientRegistry.AutoReg_AsuntoATratar := Value;
end;

procedure TdmVisitantes.SetAutoReg_Identificacion(const Value: boolean);
begin
     ClientRegistry.AutoReg_Identificacion := Value;
end;

procedure TdmVisitantes.SetAutoReg_Observaciones(const Value: boolean);
begin
     ClientRegistry.AutoReg_Observaciones := Value;
end;

procedure TdmVisitantes.SetAutoReg_Vehiculo(const Value: boolean);
begin
     ClientRegistry.AutoReg_Vehiculo := Value;
end;

procedure TdmVisitantes.SetImpresoraDefault(const Value: string);
begin
     ClientRegistry.ImpresoraDefault := Value;
end;

procedure TdmVisitantes.SetImpresionGafete(const Value: eImpresionGafete);
begin
     ClientRegistry.ImpresionGafete := Ord( Value );
end;

procedure TdmVisitantes.SetMostrarGridAvanzado(const Value: Boolean);
begin
    ClientRegistry.MostrarGridAvanzado := Value;
end;

procedure TdmVisitantes.SetMostrarGridCitas(const Value: Boolean);
begin
     ClientRegistry.MostrarGridCitas := Value;
end;

procedure TdmVisitantes.SetMostrarRegistroGrupo(Value: Boolean);
begin
     ClientRegistry.MostrarRegistroGrupo := Value;
end;

procedure TdmVisitantes.SetFiltrarTiempo(const Value: Boolean);
begin
     ClientRegistry.FiltrarTiempo := Value;
end;

procedure TdmVisitantes.SetFiltroSQL(const Value: string);
begin
   sFiltroSQL:= Value;
end;

procedure TdmVisitantes.SetMinutosAntes(const Value: Integer);
begin
     ClientRegistry.MinutosAntes := Value;
end;

procedure TdmVisitantes.SetMinutosDespues(const Value: Integer);
begin
     ClientRegistry.MinutosDespues := Value;
end;

procedure TdmVisitantes.cdsCasetaAlAdquirirDatos(Sender: TObject);
begin
     cdsCatalogoAlAdquirirDatos( Sender );
     cdsCaseta.Filtered := TRUE;
     cdsCaseta.Filter := Format( 'CA_STATUS = %d', [ Ord( cfActiva ) ] );
end;

procedure TdmVisitantes.cdsCortesAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   iCorte: integer;
begin
     cdsCaseta.Conectar;
     with cdsCortes do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( Servidor.GrabaCorte( dmCliente.Empresa, Delta,iCorte, ErrorCount ) );
               Edit;
               FieldByName('CO_FOLIO').AsInteger:= iCorte;
               Post;
               MergeChangeLog;
          end;
     end;
end;

procedure TdmVisitantes.cdsDeptoLookupSearch(Sender: TZetaLookupDataSet;
  var lOk: Boolean; const sFilter: string; var sKey, sDescription: string);
begin

end;

{$ifdef VER130}
procedure TdmVisitantes.cdsCatalogosReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError( Dataset, UpdateKind, E );
end;
{$else}
procedure TdmVisitantes.cdsCatalogosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError( Dataset, UpdateKind, E );
end;
{$endif}

procedure TdmVisitantes.ValidaCamposLibros( DataSet: TDataSet; const lValidaVehiculo: Boolean );

const
     aGlobales : array[1..8] of integer = (  K_GLOBAL_REQ_VISITANTE,
                                             K_GLOBAL_REQ_COMPANYS,
                                             K_GLOBAL_REQ_ASUNTO,
                                             K_GLOBAL_REQ_ANFITRION,
                                             K_GLOBAL_REQ_DEPARTAMENTO,
                                             K_GLOBAL_REQ_IDENTIFICA,
                                             K_GLOBAL_REQ_GAFETE,
                                             K_GLOBAL_REQ_VEHICULO  );
     K_MIN_GLOBAL_LIBRO = 25;
     K_MAX_GLOBAL_LIBRO = 32;
var
   i, iGlobal: Integer;
   sCampo, sField: String;
          procedure GetDescripcionError( const iGlobal: Integer; const sCampo, sField: String );
          var
             sError: String;
          begin
               sError := VACIO;
               if( Global.GetGlobalBooleano( iGlobal ) and StrVacio( sCampo ) )then
               begin
                   sError := Format( '%s Para grabar el registro de visitas', [ GetDescripcionGlobal( iGlobal ) ] );
                   cdsLibros.FieldByName( sField ).FocusControl;
                   DatabaseError( sError );
               end;
          end;
begin
     with cdsLibros do
     begin
          for i := Low(aGlobales) to High(aGlobales) do
          begin
               iGlobal := aGlobales[i];
               case iGlobal of
                    K_GLOBAL_REQ_VISITANTE: sField := 'LI_NOMBRE';
                    K_GLOBAL_REQ_ANFITRION: sField := 'LI_ANFITR';
                    K_GLOBAL_REQ_COMPANYS: sField := 'LI_EMPRESA';
                    K_GLOBAL_REQ_DEPARTAMENTO: sField := 'LI_DEPTO';
                    //K_GLOBAL_REQ_ASUNTO: sField := 'LI_ASUNTO';
                    //K_GLOBAL_REQ_VEHICULO: sField := 'LI_CAR_TIP';
                   // K_GLOBAL_REQ_IDENTIFICA: sField := 'LI_TIPO_ID';
                   // K_GLOBAL_REQ_GAFETE: sField := 'LI_GAFETE';
               end;
               if ( iGlobal <> K_GLOBAL_REQ_DEPARTAMENTO )then//K_GLOBAL_REQ_DEPARTAMENTO) then
               begin
                    if ( (NOT lValidaVehiculo AND (sField <> 'LI_CAR_TIP')) OR lValidaVehiculo ) then
                    begin
                         sCampo := FieldByName( sField ).AsString;
                         GetDescripcionError( iGlobal, sCampo, sField )
                    end;
               end;
          end;
     end;
end;

procedure TdmVisitantes.cdsLibrosBeforePost(DataSet: TDataSet);
const
     K_ESPACIO = ' ';
begin
     //ValidaCamposLibros( DataSet );
     with cdsLibros do
     begin
          if ( State in [dsEdit] ) then
          begin
               if StrVacio( FieldByName('LI_OBSERVA').AsString ) then
                  FieldByName('LI_OBSERVA').AsString := K_ESPACIO;
          end;
     end;
end;

procedure TdmVisitantes.cdsLibrosCalcFields(DataSet: TDataSet);

  function TiempoExcede(sLiEntHor, sLiSalHor: string): boolean;
  begin
    if (sLiEntHor = null) or (sLiSalHor = null) then
      exit;
    with dmVisitantes do begin
    Result := NOT AlertasEnabled;
       if NOT Result then
       begin
         Result := StrVacio( sLiSalHor ) AND
                   (( aMinutos(HoraAsStr(Time)) - aMinutos(sLiEntHor )) > TiempoMinimo );
       end;
    end;
  end;


begin
  with DataSet do
  begin
   {   if Length(FieldByName('LI_NOMBRE').AsString) > TressShell.iMaxLenVisit then
        TressShell.iMaxLenVisit := (FieldByName('LI_NOMBRE').AsString).Length
      else if Length(FieldByName('LI_EMPRESA').AsString) > TressShell.iMaxLenVisit then
        TressShell.iMaxLenVisit := (FieldByName('LI_EMPRESA').AsString).Length;


     FieldByName('ENT_SAL').AsString := MaskHora(FieldByName('LI_ENT_HOR').AsString) + #10 + MaskHora(FieldByName('LI_ENT_HOR').AsString);
     FieldByName('Visitante').AsString := FieldByName('LI_NOMBRE').AsString + #10 + FieldByName('LI_DEPTO').AsString + #10 + FieldByName('LI_EMPRESA').AsString;}
     FieldByName('VisitandoA').AsString := FieldByName('LI_ANFITR').AsString + #10 + FieldByName('LI_DEPTO').AsString;
     FieldByName('RegisEntSal').AsString := 'E: '+ FieldByName('US_ENT_LIB').AsString + #10 + 'S: '+FieldByName('US_SAL_LIB').AsString;
     FieldByName('Identificacion').AsString := FieldByName('ID_NOMBRE').AsString;

     if FieldByName('LI_SAL_HOR').AsString = VACIO then begin
       if (dmVisitantes.AlertasEnabled) then
            if (FieldByName('LI_ENT_HOR').AsString <> null) and (FieldByName('LI_SAL_HOR').AsString <> null) then
            if  tiempoexcede( FieldByName('LI_ENT_HOR').AsString, FieldByName('LI_SAL_HOR').AsString ) then
              FieldByName('US_SAL_LIB').AsString := 'Tiempo excedido';
        end;
     end;
end;

procedure TdmVisitantes.BuscaLibro( const dInicio, dFin: TDate;
                                       const sFiltro: string );
begin
     cdsLibroBusqueda.Data := Servidor.GetLibros( dmCliente.Empresa, dInicio, dFin, sFiltro );
end;

procedure TdmVisitantes.cdsGetRights( Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     //No se puede dar de alta ningun catalogo en este cliente
     if (iRight = K_DERECHO_ALTA) OR (iRight = K_DERECHO_CONSULTA) then
        lHasRights := FALSE;
end;


procedure TdmVisitantes.MuestraDialogoAlerta;
begin
     FAlertasMgr.ShowDialog;
end;

procedure TdmVisitantes.CierraDialogoAlerta( oAlerta: TAlerta );
begin
     FAlertasMgr.CloseDialog( oAlerta );
end;

function TdmVisitantes.RevisaAlertas: Boolean;
begin
     with cdsAlertas do
     begin
          Refrescar;
          Result := NOT EOF;

          while NOT EOF do
          begin
               MuestraDialogoAlerta;
               Next;
          end;

     end;
end;

procedure TdmVisitantes.cdsAlertasAlAdquirirDatos(Sender: TObject);
begin
     cdsAlertas.Data := Servidor.GetAlertas( dmCliente.Empresa, Caseta );
end;


function TdmVisitantes.TiempoExcedido( DataSet: TDataSet ): Boolean;
begin
     Result := NOT FAlertasEnabled;
     if NOT Result then
     begin
          with DataSet do
          begin
               Result := StrVacio( FieldByName('LI_SAL_HOR').AsString ) AND
                         (( aMinutos(HoraAsStr(Time)) - aMinutos(FieldByName('LI_ENT_HOR').AsString) ) > fTiempoMinimo );
          end;
     end;
end;

procedure TdmVisitantes.cdsVisitanteNewRecord(DataSet: TDataSet);
begin
     with cdsVisitante do
     begin
          FieldByName('VI_FEC_NAC').AsDateTime := Date;
          FieldByName('VI_SEXO').AsString := ObtieneElemento( lfSexo, Ord( esMasculino ) );
          FieldByName('VI_FOTO').AsString := ObtieneElemento( lfSexo, Ord( esMasculino ) );
     end;
end;

procedure TdmVisitantes.cdsCitasSQLAlAdquirirDatos(Sender: TObject);
begin
     with cdsCitasSQL do
     begin
     Data := Servidor.GetSQL(dmCliente.Empresa,OPCSQL, GetFiltroSQL);
     if not Active then
        EmptyDataset;
     end;
end;

procedure TdmVisitantes.cdsCitasSQLAlCrearCampos(Sender: TObject);
begin
     with cdsCitasSQL do
     begin
          CreateCalculated( 'VI_PRETYNAME', ftString, 120 );
     end;
end;

procedure TdmVisitantes.cdsEmpVisitanteAlAdquirirDatos(Sender: TObject);
begin
     cdsCatalogoAlAdquirirDatos(Sender);
     with cdsCompaniaLookup do
     begin
          Filtered := FALSE;
          IndexFieldNames := 'EV_NUMERO';
          Data := cdsEmpVisitante.Data;
     end;
end;

function TdmVisitantes.GetTieneSalida: Boolean;
begin
     Result := cdsLibros.FieldByName('LI_SAL_COR').AsInteger <> 0 ;
end;


function TdmVisitantes.getTotAutos: OleVariant;
begin
    with Servidor do
     begin
          //Result := Servidor.GetTotales( dmCliente.Empresa);
     end;
end;

procedure TdmVisitantes.BuscarVisitante(sNombres:string);
var
   FParamList: TZetaParams;
begin
     FParamList := TZetaParams.Create;
     try        
        FParamList.AddString( 'NOMBRE', UPPERCASE( sNombres ) );
        FParamList.AddString( 'TIPO', VACIO );
        FParamList.AddString( 'ANFITRION', VACIO );
        FParamList.AddString( 'EMPRESA', VACIO );
        FParamList.AddInteger('STATUS', 0 );

        HacerBusquedaVisitante( FParamList );
     finally
            FreeAndNil( FParamList );
     end;
end;

procedure TdmVisitantes.RegistraLlegada;
var
   sMensaje: String;

begin
     if ( cdsCitas.FieldByName('CI_STATUS').AsInteger = 1 ) and
         not (VisitanteAdentro(cdsCitas.FieldByName('VI_NUMERO').AsInteger, sMensaje) ) then
     begin
          with cdsLibros do
          begin
               Append;
               { Datos del Visitante y Compa�ia}
               FieldByName('CI_FOLIO').AsInteger := cdsCitas.FieldByName('CI_FOLIO').AsInteger;
               FieldByName('VI_NUMERO').AsInteger:= cdsCitas.FieldByName('VI_NUMERO').AsInteger;
               BuscarVisitante(cdsCitas.FieldByName('VI_NOMBRES').AsString);
               if ( cdsVisitanteLookup.Locate('VI_NUMERO', cdsCitas.FieldByName('VI_NUMERO').AsString, []) ) then
               begin
                    FieldByName('LI_NOMBRE').AsString := cdsVisitanteLookup.GetDescripcion(cdsCitas.FieldByName('VI_NUMERO').AsString);
                    FieldByName('EV_NUMERO').AsInteger := cdsVisitanteLookup.FieldByName('EV_NUMERO').AsInteger;
                    FieldByName('LI_EMPRESA').AsString := cdsVisitanteLookup.FieldByName('EV_NOMBRE').AsString;
               end;
               { Datos del anfitri�n}
                FieldByName('AN_NUMERO').AsString := cdsCitas.FieldByName('AN_NUMERO').AsString;
               if ( cdsAnfitrion.Locate('AN_NUMERO', cdsCitas.FieldByName('AN_NUMERO').AsString, [] ) ) then
               begin
                    FieldByName('LI_ANFITR').AsString :=  cdsAnfitrion.GetDescripcion(cdsCitas.FieldByName('AN_NUMERO').AsString);
                    FieldByName('LI_CDEPTO').AsString := cdsAnfitrion.FieldByName('AN_DEPTO').AsString;
                    FieldByName('LI_DEPTO').AsString := cdsAnfitrion.FieldByName('TB_ELEMENT').AsString;
               end;

               { Datos del asunto a tratar}
               FieldByName('LI_ASUNTO').AsString := cdsCitas.FieldByName('CI_ASUNTO').AsString;

          end;
          RegistraEntrada;
          if ( ( cdsCitas.FieldByName('CI_FOLIO').AsInteger = cdsLibros.FieldByName('CI_FOLIO').AsInteger ) or
                                           cdsCitas.Locate('CI_FOLIO',cdsLibros.FieldByName('CI_FOLIO').AsInteger,[]) )  then
          begin
               with cdsCitas do
               begin
                    if not( State in [dsInsert, dsEdit] ) then
                       Edit;
                    FieldByName('CI_STATUS').AsInteger:= Ord(csSi);
                    Enviar;
               end;
          end;
     end
     else
     begin
          ZInformation( 'Registrar llegada', StrDef(sMensaje,K_MSG_LLEGO_CITA), 0 );
     end;
end;

//acl.
procedure TdmVisitantes.MuestraFotografia;
begin
     cdsMuestraFoto.Data := Servidor.GetFoto( dmCliente.Empresa, cdsVisitanteLookup.FieldByName('VI_NUMERO').AsInteger );
     if ( cdsMuestraFoto.FieldByName('VI_FOTO').IsNull) then
     begin
          ZetaDialogo.ZInformation('Fotograf�a','No existe fotograf�a para el visitante seleccionado.',0);
     end
     else
     begin
          FMostrarFotografia.MostrarFoto(cdsVisitanteLookup.FieldByName('VI_NUMERO').AsInteger,cdsMuestraFoto);
     end;
end;

function TdmVisitantes.VisitanteAdentro(const iVisitante: Integer; var sMensaje: String): Boolean;
begin
     Result:= FALSE;
     with cdsLibrosSalidas do
     begin
          Refrescar;
          if not IsEmpty then //El visitante esta adentro si coincide el numero con el visitante, la fecha es de entrada es ahora y no tiene salida
          begin
               Result:= Locate('VI_NUMERO;LI_ENT_FEC',VarArrayOf([iVisitante,FechaAsStr(dmCliente.FechaDefault)]),[]);
          end;
     end;
     if Result then
     begin
          sMensaje:= K_MSG_VISITANTE_ADENTRO;
     end;
end;

procedure TdmVisitantes.cdsTotalesAlAdquirirDatos(Sender: TObject);
begin
     with cdsTotales do
     begin
     Data := Servidor.GetSQL(dmCliente.Empresa,OPCSQL, GetFiltroSQL);
     if not Active then
        EmptyDataset;
     end;
end;

procedure TdmVisitantes.cdsTotalesAlCrearCampos(Sender: TObject);
begin
     with cdsTotales do
     begin
          CreateCalculated( 'VI_PRETYNAME', ftString, 120 );
     end;
end;

procedure TdmVisitantes.cdsTotalesReconcileError(DataSet: TCustomClientDataSet;
  E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
      Action := ZReconcile.HandleReconcileError( Dataset, UpdateKind, E );
end;

procedure TdmVisitantes.cdsVisitanteAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   iVisitante: integer;
begin
     with cdsVisitante do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( Servidor.GrabaVisita( dmCliente.Empresa, Delta,ErrorCount,iVisitante ) )then
               begin
                    Edit;
                    FieldByName('VI_NUMERO').AsInteger:= iVisitante;
                    Post;
                    MergeChangeLog;
               end;
          end;
     end;
end;

end.

