unit FDatosVisitGrupo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, TressMorado2013, Vcl.StdCtrls,
  cxButtons, Vcl.ExtCtrls, Vcl.ImgList, dxGDIPlusClasses, ZetaDBTextBox;

type
  TFDatosVisitanteGrupo = class(TForm)
    cxImageList24_PanelBotones: TcxImageList;
    PanelBotones: TPanel;
    OK_DevEx: TcxButton;
    Cancelar_DevEx: TcxButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Image1: TImage;
    edNumero: TZetaTextBox;
    edVisitante: TZetaTextBox;
    edEmpresa: TZetaTextBox;
    edAnfitrion: TZetaTextBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    sNumero:string;
    sVisitante:string;
    sEmpresa:string;
    sAnfitrion:string;
  public
    { Public declarations }
    property sPNumero: string read sNumero write sNumero;
    property sPVisitante: string read sVisitante write sVisitante;
    property sPEmpresa: string read sEmpresa write sEmpresa;
    property sPAnfitrion: string read sAnfitrion write sAnfitrion;
  end;

var
  FDatosVisitanteGrupo: TFDatosVisitanteGrupo;

implementation

{$R *.dfm}

uses FRegistroEntradaVisi, FTressShell;

procedure TFDatosVisitanteGrupo.FormCreate(Sender: TObject);
begin
     sPNumero := '';
     sPVisitante := '';
     sPEmpresa := '';
     sPAnfitrion := '';
     sPNumero := '1';
end;

procedure TFDatosVisitanteGrupo.FormShow(Sender: TObject);
begin
     edNumero.Caption := sPNumero;
     edVisitante.Caption := sPVisitante;
     edEmpresa.Caption := sPEmpresa;
     edAnfitrion.Caption := sPAnfitrion;
     OK_DevEx.SetFocus;
end;

procedure TFDatosVisitanteGrupo.OK_DevExClick(Sender: TObject);
begin
     TressShell.FPPrimerRegGrupo := false;
end;

end.
