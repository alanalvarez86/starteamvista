unit ZVisitBusqueda_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Db, Grids, DBGrids,
     ZetaClientDataset,
     ZetaDBGrid,
     ZetaMessages, DBCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, cxButtons,
  cxControls, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid,  dxSkinsDefaultPainters, TressMorado2013;

type
  TBusqueda_DevEx = class(TForm)
    PanelBotones: TPanel;
    PanelSuperior: TPanel;
    PistaLBL: TLabel;
    Pista: TEdit;
    Datasource: TDataSource;
    MostrarActivos: TCheckBox;
    Cancelar_DevEx: TcxButton;
    OK_DevEx: TcxButton;
    Filtrar_DevEx: TcxButton;
    DBGrid_DevExDBTableView: TcxGridDBTableView;
    DBGrid_DevExLevel: TcxGridLevel;
    DBGrid_DevEx: TZetaCXGrid;
    Codigo_DevEx: TcxGridDBColumn;
    DBGrid_DevExDBTableViewColumn2: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    Refrescar_DevEx: TcxButton;
    btnNuevo: TcxButton;
    STATUS: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure PistaChange(Sender: TObject);
    procedure PistaKeyPress(Sender: TObject; var Key: Char);
    procedure Modificar_DevExClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure Filtrar_DevExClick(Sender: TObject);
    procedure RefrescarClick(Sender: TObject);
    procedure DBGrid_DevExDBTableViewDblClick(Sender: TObject);
    procedure DBGrid_DevExDBTableViewKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure btnNuevoClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure DBGrid_DevExDBTableViewCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
  private
    { Private declarations }
    FDataset: TZetaLookupDataset;
    FFiltro: String;
    sFFiltroEdit:string;
    FCodigo: String;
    FMuestraBotonAgregar:boolean;
    FShowModificar: Boolean;
    FFiltroActivos: Boolean;
    FFiltroConfidencialidad: Boolean;

    function Llave: String;
    procedure Connect;
    procedure Disconnect;
    procedure RemoveFilter;
    procedure SetControls;
    procedure SetDataset( Value: TZetaLookupDataset );
    procedure SetFilter;
    procedure SetFiltro( const Value: String );
    procedure SetFiltroEdit( const Value: String );
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    procedure ValidaOk;
    procedure SetMuestraBotonAgregar(const Value: Boolean);

  protected
    { Protected declarations }
{$ifdef FALSE}
    procedure KeyPress( var Key: Char ); override; { TWinControl }
{$endif}
    procedure OnFiltrarConfidencialidad( DataSet: TDataSet;
          var Accept: Boolean);
  public
    { Public declarations }
    property Codigo: String read FCodigo write FCodigo;
    property Dataset: TZetaLookupDataset read FDataset write SetDataset;
    property Filtro: String read FFiltro write SetFiltro;
    property sFiltroEdit: string read sFFiltroEdit write SetFiltroEdit;
    property bMuestraBotonAgregar: Boolean read FMuestraBotonAgregar  write SetMuestraBotonAgregar;
  end;

var
  Busqueda_DevEx: TBusqueda_DevEx;

function ShowSearchForm( LookupDataset: TZetaLookupDataset; const sFilter: String; var sKey, sDescription: String; const lShowModificar: Boolean = TRUE; const SoloActivos: Boolean = TRUE;  const lUsarConfidencialidad : Boolean = TRUE): Boolean;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     FEditCatCompanys,
     FEditCatAnfitrion;

{$R *.DFM}

function ShowSearchForm( LookupDataset: TZetaLookupDataset; const sFilter: String; var sKey, sDescription: String; const lShowModificar: Boolean; const SoloActivos: Boolean; const lUsarConfidencialidad : Boolean): Boolean;
var
   oBusqueda: TBusqueda_DevEx;
begin
//   if ( Busqueda = nil ) then
//   Se agreg� un objeto TBusqueda para manejar multiples instancias
//   de la forma de busqueda

     oBusqueda := TBusqueda_DevEx.Create( Application );
     try
        with oBusqueda do
        begin
             Dataset := LookupDataset;
             Filtro := sFilter;
             Codigo := sKey;
             FShowModificar:= lShowModificar;
             FFiltroActivos:= SoloActivos;
             FFiltroConfidencialidad := lUsarConfidencialidad;
             ShowModal;
             Result := ( ModalResult = mrOk );
             if Result then
             begin
                  sKey := Codigo;
                  sDescription := LookupDataset.GetDescription;
             end;
        end;
     finally
            oBusqueda.Free;
     end;
end;

{ *********** TBusqueda ************ }

procedure TBusqueda_DevEx.FormCreate(Sender: TObject);
begin
     HelpContext:= H00015_busqueda_catalogos;
     bMuestraBotonAgregar := false;
end;

procedure TBusqueda_DevEx.FormShow(Sender: TObject);
var
   sDescription: String;
   function getNameLookUp(lookUpName:string):string;
   begin
        if lookUpName = 'compa��as' then
           result := '&Nueva compa��a'
        else if lookUpName = 'Anfitri�n' then
             result := '&Nuevo anfitri�n'
        else if lookUpName = 'Departamento' then
             result := '&Nuevo departamento'
   end;

begin
     inherited;
     btnNuevo.visible := Busqueda_DevEx.FMuestraBotonAgregar;
     //DevEx:
     with Dataset do
     begin
          if strLleno( Codigo ) then
             LookupKey( Codigo, Filtro, sDescription );
          Caption := 'B�squeda de ' + LowerCase(LookupName);
          btnNuevo.Caption := getNameLookUp(LookUpName);
          Self.Refrescar_DevEx.Hint := 'Refrescar ' + LookupName;
          with  DBGrid_DevExDBTableView do
          begin
               Columns[ 0 ].DataBinding.FieldName := LookupKeyField;
               Columns[ 1 ].DataBinding.FieldName := LookupDescriptionField;
          end;

          if getNameLookUp(LookUpName) = '&Nueva compa��a' then
             STATUS.DataBinding.FieldName := 'EV_STATUS'
          else STATUS.DataBinding.FieldName := '';
     end;
    // Pista.Clear;
     if ( FShowModificar ) then //FShowModificar indica si viene de un lookup
        ActiveControl := DBGrid_DevEx
     else
         ActiveControl:= Pista;

     Connect;

     DBGrid_DevExDBTableView.ApplyBestFit();

     if Codigo <> VACIO then
     begin
          Pista.Text := Codigo;
          Filtrar_DevExClick(Self);
     end;

     if not OK_DevEx.Enabled then
        btnNuevo.SetFocus
     else
         OK_DevEx.SetFocus;

end;

procedure TBusqueda_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Disconnect;
     Action := caHide;
end;

procedure TBusqueda_DevEx.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     {with Modificar_DevEx do
     begin
          if Visible and ( Key = VK_INSERT ) and ( ssShift in Shift ) then
          begin
               Key := 0;
               Click;
          end;
     end;}
end;

{$ifdef FALSE}
procedure TBusqueda_DevEx.KeyPress( var Key: Char );
begin
     inherited KeyPress( Key );
     case Key of
          Chr( VK_RETURN ):
          begin
               if ( not ( ActiveControl is TButton ) ) then
               begin
                    Key := #0;
                    Perform( WM_NEXTDLGCTL, ( Hi( GetKeyState( VK_SHIFT ) ) and 1 ), 0 );
               end;
          end;
     end;
end;
{$endif}

procedure TBusqueda_DevEx.SetControls;
begin
    { with Modificar_DevEx do
     begin
          with Dataset do
          begin
               if PuedeConsultar and FShowModificar then
               begin
                    if IsEmpty then
                    begin
                         if GetRights( K_DERECHO_ALTA ) then
                         begin
                              Caption := '&Agregar';
                              Hint := 'Agregar ' + LookupName;
                              Visible := True;
                         end
                         else
                             Visible := False;
                    end
                    else
                    begin
                         Caption := '&Modificar';
                         Hint := 'Editar ' + LookupName;
                         Visible := True;
                    end;
               end
               else
               begin
                   Visible := False;
               end;

               with MostrarActivos do
               begin
                    Visible := StrLleno( LookupActivoField ) and ( Pos( LookupActivoField, Filtro ) <= 0 ) ;
                    Checked := FFiltroActivos;
               end;
          end;

     end;
     Self.Refrescar_DevEx.Visible:= FShowModificar;  }
end;

procedure TBusqueda_DevEx.SetFiltro( const Value: String );
begin
     if strVacio( Value ) then
        FFiltro := VACIO
     else
         FFiltro := ZetaCommonTools.Parentesis( Value );
end;

procedure TBusqueda_DevEx.SetFiltroEdit(const Value: String);
begin
     if strVacio( Value ) then
        sFFiltroEdit := VACIO
     else
         sFFiltroEdit := Value;
end;

procedure TBusqueda_DevEx.SetMuestraBotonAgregar(const Value: Boolean);
begin
     FMuestraBotonAgregar := Value;
end;

procedure TBusqueda_DevEx.ValidaOk;
begin
     if DBGrid_DevExDBTableView.DataController.RecordCount > 0 then
        OK_DevEx.Enabled := true
     else
     begin
          OK_DevEx.Enabled := false;
     end;
end;

procedure TBusqueda_DevEx.SetDataset( Value: TZetaLookupDataset );
begin
     if ( FDataset <> Value ) then
     begin
          FDataset := Value;
     end;
end;

procedure TBusqueda_DevEx.OnFiltrarConfidencialidad( DataSet: TDataSet; var Accept: Boolean);
  var sNivel0 : String;
begin
     Accept := TRUE;
     if (DataSet <> nil ) then
     begin
         if StrLleno(ZetaClientDataSet.GlobalListaConfidencialidad) and StrLleno( TZetaLookupDataSet( Dataset ).LookupConfidenField) then
         begin
              sNivel0 := DataSet.FieldByName(TZetaLookupDataSet( Dataset ).LookupConfidenField).AsString;
              Accept := ZetaCommonTools.ListaIntersectaConfidencialidad( sNivel0 , ZetaClientDataSet.GlobalListaConfidencialidad ) ;
         end;
     end;
end;

procedure TBusqueda_DevEx.SetFilter;
var
   Pos : TBookMark;

   function GetFiltroActivos: String;
   begin
        with MostrarActivos do
        begin
             if Visible and Checked then
             begin
                  Result:= Parentesis( DataSet.LookupActivoField + '=' + EntreComillas( K_GLOBAL_SI ) );
             end
             else
             begin
                  Result:= VACIO;
             end;
        end;
   end;


   function GetFilterLlave : String;
   var
      iDefTipoDato : String;
      TipoCampo: TFieldType;
      sFiltro: String;

   begin
        with DataSet do
        begin
             TipoCampo := FieldByName( LookupKeyField ).DataType;
             //if ( TipoCampo in [ ftInteger, ftSmallInt, ftWord ] ) then //OLD
             {***DevEx(@am): En realacion al Bug 5241 del proyecto de nueva imagen. Se agrega el tipo ftAutoInc pues de lo contrario
                             el flujo entraria a la negacion del If. En la cual se intentara hacer un UPPER a un dato numerico, lo cual producira
                             un mensaje de error notificando que los tipos de datos son incompatibles.

                             En la VS. 2013 no se daba esta situacion, sin embargo para la version 2014 se realizo un cambio de provider,
                             por lo que es posible que los AutoInc antes fueran detectados como ftInteger.***}
             if ( TipoCampo in [ ftInteger, ftSmallInt, ftWord, ftAutoInc ] ) then
             begin
                  iDefTipoDato:=IntToStr( StrToIntDef( Llave, 0 ));
                  sFiltro:= LookupKeyField + ' = ' + iDefTipoDato ;
             end
             else
             begin
                  sFiltro:= ' UPPER( ' + LookupKeyField + ' ) like ' + EntreComillas(  Llave + '%' );
             end;

             Result:= Parentesis( ConcatString( sFiltro, ' UPPER( ' + LookupDescriptionField + ' ) like ' + EntreComillas( '%' + Llave + '%' ), 'OR' ) );
        end;
   end;

begin
     with Dataset do
     begin
          DisableControls;
          try
             if strLleno( Filtro ) or strLleno( Llave ) or MostrarActivos.Checked  or FFiltroConfidencialidad    then
             begin
                  Pos:= GetBookMark;
                  Filtered := False;
                  Filter := ZetaCommonTools.ConcatFiltros ( GetFilterLlave, GetFiltroActivos );
                  Filter := ZetaCommonTools.ConcatFiltros ( Filtro, Filter );
                  DataSet.OnFilterRecord := OnFiltrarConfidencialidad;
                  Filtered := True;
                  if ( Pos <> nil ) then
                  begin
                       if BookMarkValid( Pos ) then
                          GotoBookMark( Pos );
                       FreeBookMark( Pos );
                  end;
             end
             else if Filtered then
             begin
                  Pos:= GetBookMark;
                  Filtered := False;
                  Filter := VACIO;
                  if ( Pos <> nil ) then
                  begin
                       if BookMarkValid( Pos ) then
                          GotoBookMark( Pos );
                       FreeBookMark( Pos );
                  end;
             end;
          finally
                 EnableControls;

          end;
     end;
end;

procedure TBusqueda_DevEx.RemoveFilter;
begin
     with Dataset do
     begin
          if Filtered then
          begin
               Filtered := False;
               Filter := VACIO;
          end;
     end;
end;

procedure TBusqueda_DevEx.btnNuevoClick(Sender: TObject);
begin
     Pista.Clear;
     SetFilter;
     with Dataset do
     begin
          if LookupName = 'compa��as' then
          begin
               self.Visible:= false;
               if EditCatCompanys <> nil then
                  EditCatCompanys := nil;
               EditCatCompanys := TEditCatCompanys.Create(self);
               EditCatCompanys.ShowModal;
          end
          else if LookupName = 'Anfitri�n' then
          begin
               self.Visible:= false;
               if EditCatAnfitrion <> nil then
                  EditCatAnfitrion := nil;
               EditCatAnfitrion := TEditCatAnfitrion.Create(self);
               EditCatAnfitrion.ShowModal;
          end;
     end;
     self.Close;

end;

procedure TBusqueda_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
     Disconnect;
end;

procedure TBusqueda_DevEx.Connect;
begin
     SetFilter;
     SetControls;
     Datasource.Dataset := Dataset;
end;

procedure TBusqueda_DevEx.Disconnect;
var
   bAnterior: TBookMark;
begin
     Datasource.Dataset := nil;
     with DataSet do
          if Filtered then
          begin
               bAnterior:= GetBookMark;
               RemoveFilter;
               GotoBookMark( bAnterior );
               FreeBookMark( bAnterior );
          end;
end;

procedure TBusqueda_DevEx.WMExaminar(var Message: TMessage);
begin
     OK_DevEx.Click;
end;

function TBusqueda_DevEx.Llave: String;
begin
     Result := Pista.Text;
end;

{ ********** Eventos de Controles ******** }

procedure TBusqueda_DevEx.PistaChange(Sender: TObject);
begin
     if strVacio( Pista.Text ) then
     begin
          SetFilter;
          Filtrar_DevEx.Enabled := False;
     end
     else
          Filtrar_DevEx.Enabled := True;
     ValidaOk;
end;

procedure TBusqueda_DevEx.PistaKeyPress(Sender: TObject; var Key: Char);
begin
     case Key of
          Chr( VK_RETURN ):
          begin
               Key := Chr( 0 );
               if Filtrar_DevEx.Enabled then
                  Filtrar_DevEx.Click
               else
                   OK_DevEx.Click;
          end;
     end;
end;

procedure TBusqueda_DevEx.Filtrar_DevExClick(Sender: TObject);
begin
     SetFilter;
     validaOk;
end;

procedure TBusqueda_DevEx.Modificar_DevExClick(Sender: TObject);
begin
     Disconnect;
     with Dataset do
     begin
          if IsEmpty then
          begin
               if GetRights( K_DERECHO_ALTA ) then
                  Agregar
               else
                   Beep;
          end
          else
              Modificar;
     end;
     Connect;
end;

procedure TBusqueda_DevEx.RefrescarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        FFiltroActivos:= MostrarActivos.Checked;
        Disconnect;
        Dataset.Refrescar;
        Connect;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBusqueda_DevEx.OK_DevExClick(Sender: TObject);
begin
     with Dataset do
     begin
          FCodigo := FieldByName( LookupKeyField ).AsString;
     end;
end;


procedure TBusqueda_DevEx.DBGrid_DevExDBTableViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
     with DBGrid_DevExDBTableView.DataController do
     begin
          if AViewInfo.Item = DBGrid_DevExDBTableViewColumn2 then
          if (AViewInfo.GridRecord.Values[2] = '1') then
             ACanvas.Font.Color := RGB(194, 0, 0);
     end;
end;

procedure TBusqueda_DevEx.DBGrid_DevExDBTableViewDblClick(Sender: TObject);
begin
   OK_DevEx.Click;
end;

procedure TBusqueda_DevEx.DBGrid_DevExDBTableViewKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
      if Key = VK_RETURN then
      begin
          if OK_DevEx.Visible and OK_DevEx.Enabled then
             OK_DevEx.Click;
      end;
end;

end.
