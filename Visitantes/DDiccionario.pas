unit DDiccionario;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     DBaseDiccionario,
     ZetaTipoEntidad,
     ZetaClientDataSet,
     ZetaCommonLists, cxCheckListBox;

type
  TdmDiccionario = class(TdmBaseDiccionario)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CamposPorEntidad( const oEntidad: TipoEntidad; const lTodos: Boolean; oLista: TStrings );overload;
    procedure CamposPorEntidad( const oEntidad: TipoEntidad; const lTodos: Boolean; oLista : tcxCheckListbox );overload;
    procedure SetLookupNames;override;
    function GetDerechosClasifi(eClasifi : eClasifiReporte): Integer;override;
    function ChecaDerechoConfidencial: Boolean;override;

  end;

var
  dmDiccionario: TdmDiccionario;

implementation
uses ZAccesosTress;
{$R *.DFM}

procedure TdmDiccionario.CamposPorEntidad( const oEntidad: TipoEntidad; const lTodos: Boolean; oLista: TStrings );
var
   oCampo: TObjetoString;
begin
     oLista.Clear;
     GetListaDatosTablas( oEntidad, lTodos );
     with cdsBuscaPorTabla do
     begin
          while NOT EOF do
          begin
               oCampo := TObjetoString.Create;
               oCampo.Campo := FieldByName( 'DI_NOMBRE' ).AsString;
               oLista.AddObject( FieldByName( 'DI_TITULO' ).AsString, oCampo );
               Next;
          end;
     end;
end;

//DevEx
procedure TdmDiccionario.CamposPorEntidad( const oEntidad: TipoEntidad; const lTodos: Boolean; oLista: tcxCheckListbox );
var
   oCampo: TObjetoString;
begin
     oLista.Clear;
     GetListaDatosTablas( oEntidad, lTodos );
     with cdsBuscaPorTabla do
     begin
          while NOT EOF do
          begin
               oCampo := TObjetoString.Create;
               oCampo.Campo := FieldByName( 'DI_NOMBRE' ).AsString;
               //oLista.AddObject( FieldByName( 'DI_TITULO' ).AsString, oCampo );
               with oLista.items.Add do
               begin
                 text := FieldByName( 'DI_TITULO' ).AsString;
                 Tag := integer(oCampo);
               end;
               Next;
          end;
     end;
end;

procedure TdmDiccionario.SetLookupNames;
begin
     inherited;
     //dmSeleccion.SetLookupNames;
end;

function TdmDiccionario.GetDerechosClasifi(eClasifi : eClasifiReporte): Integer;
begin
     Result := D_CONS_REPORTES;
end;

function TdmDiccionario.ChecaDerechoConfidencial: Boolean;
begin
     Result := TRUE;
end;


end.
