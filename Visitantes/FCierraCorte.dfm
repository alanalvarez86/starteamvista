inherited CierraCorte: TCierraCorte
  Left = 445
  Top = 374
  Caption = 'Cierre de corte'
  ClientHeight = 304
  ClientWidth = 355
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  ExplicitWidth = 361
  ExplicitHeight = 338
  PixelsPerInch = 96
  TextHeight = 13
  object lblCaseta: TLabel [0]
    Left = 21
    Top = 11
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Caseta:'
  end
  object CA_NOMBRE: TZetaDBTextBox [1]
    Left = 62
    Top = 9
    Width = 283
    Height = 17
    AutoSize = False
    Caption = 'CA_NOMBRE'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'CA_NOMBRE'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  inherited PanelBotones: TPanel
    Top = 268
    Width = 355
    ExplicitTop = 268
    ExplicitWidth = 355
    inherited OK_DevEx: TcxButton
      Left = 183
      OnClick = OK_DevExClick
      ExplicitLeft = 183
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 264
      OnClick = Cancelar_DevExClick
      ExplicitLeft = 264
    end
  end
  object GroupBox2: TGroupBox [3]
    Left = 8
    Top = 31
    Width = 169
    Height = 67
    Caption = ' Inicio del corte '
    TabOrder = 1
    object lblFechaIni: TLabel
      Left = 25
      Top = 20
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha:'
    end
    object lblHoraIni: TLabel
      Left = 32
      Top = 44
      Width = 26
      Height = 13
      Alignment = taRightJustify
      Caption = 'Hora:'
    end
    object CO_INI_FEC: TZetaDBTextBox
      Left = 63
      Top = 18
      Width = 80
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CO_INI_FEC'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CO_INI_FEC'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CO_INI_HOR: TZetaDBTextBox
      Left = 63
      Top = 42
      Width = 80
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CO_INI_HOR'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CO_INI_HOR'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  object gbTerminaCorte: TGroupBox [4]
    Left = 192
    Top = 31
    Width = 153
    Height = 67
    Caption = 'Termina corte '
    TabOrder = 2
    object lblFechaFin: TLabel
      Left = 18
      Top = 18
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha:'
    end
    object lblHoraFin: TLabel
      Left = 25
      Top = 42
      Width = 26
      Height = 13
      Alignment = taRightJustify
      Caption = 'Hora:'
    end
    object ztbFecha_Fin: TZetaDBTextBox
      Left = 55
      Top = 18
      Width = 80
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'ztbFecha_Fin'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CO_FIN_FEC'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object ztbHora_Fin: TZetaDBTextBox
      Left = 55
      Top = 42
      Width = 80
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'ztbHora_Fin'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CO_FIN_HOR'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  object GroupBox1: TGroupBox [5]
    Left = 8
    Top = 100
    Width = 337
    Height = 65
    Caption = ' Usuarios '
    TabOrder = 3
    object lblIniCorte: TLabel
      Left = 19
      Top = 17
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Inicia corte:'
    end
    object lblFinCorte: TLabel
      Left = 6
      Top = 40
      Width = 68
      Height = 13
      Alignment = taRightJustify
      Caption = 'Termina corte:'
    end
    object US_NOMBRE: TZetaDBTextBox
      Left = 77
      Top = 15
      Width = 250
      Height = 17
      AutoSize = False
      Caption = 'US_NOMBRE'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'US_INI_CORTE'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object ztbTermina_corte: TZetaDBTextBox
      Left = 77
      Top = 38
      Width = 250
      Height = 17
      AutoSize = False
      Caption = 'ztbTermina_corte'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'US_FIN_CORTE'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  object gbObserva: TGroupBox [6]
    Left = 8
    Top = 167
    Width = 337
    Height = 95
    Caption = ' Observaciones: '
    TabOrder = 4
    object CO_OBSERVA: TDBMemo
      Left = 2
      Top = 15
      Width = 333
      Height = 78
      Align = alClient
      DataField = 'CO_OBSERVA'
      DataSource = DataSource
      ScrollBars = ssVertical
      TabOrder = 0
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  object DataSource: TDataSource
    Left = 200
    Top = 72
  end
end
