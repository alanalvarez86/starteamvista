unit FFolioSalida;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, Mask, ZetaNumero, Buttons, ExtCtrls;

type
  TFolioSalida = class(TZetaDlgModal)
    lbFolio: TLabel;
    LI_FOLIO: TZetaNumero;
    btnBuscar: TBitBtn;
    procedure btnBuscarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    function GetFolio: integer;
    procedure SetFolio(const Value: integer);
    { Private declarations }
  public
    { Public declarations }
    property Folio: integer read GetFolio write SetFolio;
  end;

var
  FolioSalida: TFolioSalida;

implementation
uses
    FGridSalidas;

{$R *.DFM}

{ TFolioSalida }

function TFolioSalida.GetFolio: integer;
begin
     Result :=  LI_FOLIO.ValorEntero;
end;

procedure TFolioSalida.SetFolio(const Value: integer);
begin
     LI_FOLIO.Valor := Value;
end;

procedure TFolioSalida.btnBuscarClick(Sender: TObject);
begin
     inherited;
     if GridSalidas = NIL then
        GridSalidas := TGridSalidas.Create( self );

     GridSalidas.ShowModal;
     if GridSalidas.ModalResult = mrOk then
     begin
          LI_FOLIO.Valor := GridSalidas.Folio;
          Close;
          ModalResult := mrOK;
          //LI_FOLIO.SetFocus;
     end;
end;

procedure TFolioSalida.FormShow(Sender: TObject);
begin
     inherited;
     LI_FOLIO.SetFocus;
end;



end.
