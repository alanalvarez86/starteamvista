unit FBusquedaVisit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, TressMorado2013, cxControls,
  cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, Data.DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, Vcl.StdCtrls, cxButtons, Vcl.Mask, Vcl.DBCtrls, Vcl.ExtCtrls,
  Vcl.ImgList, cxButtonEdit, System.Actions, Vcl.ActnList;

type
  TFBusquedaVisitante = class(TForm)
    Panel1: TPanel;
    Cancel: TcxButton;
    Ok: TcxButton;
    dsVisita: TDataSource;
    btnNuevo: TcxButton;
    dsSQL: TDataSource;
    cxImageList24_PanelBotones: TcxImageList;
    ActionList1: TActionList;
    _AMuestraFoto: TAction;
    Panel2: TPanel;
    Label24: TLabel;
    lblApePat: TLabel;
    btnBuscar: TcxButton;
    edNombre: TEdit;
    edApePat: TEdit;
    lbVisiEncontrados: TLabel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    VI_NUMERO: TcxGridDBColumn;
    VI_FOTO: TcxGridDBColumn;
    btnVerFoto2: TcxGridDBColumn;
    Empresa: TcxGridDBColumn;
    VI_PRETYNAME: TcxGridDBColumn;
    VI_STATUS: TcxGridDBColumn;
    procedure OkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CancelClick(Sender: TObject);
    procedure _AMuestraFotoExecute(Sender: TObject);
    procedure btnNuevoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxGrid1DBTableView1DataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure edNombreKeyPress(Sender: TObject; var Key: Char);
    procedure edApePatKeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxGrid1DBTableView1ColumnHeaderClick(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure edNombreKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edApePatKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    { Private declarations }
    FCerrarBusqueda:boolean;
    FCancelaBusqueda:boolean;
    procedure connect;
    procedure MuestraGrid(bMuestra:boolean);
    function ExisteVisitanteEnLibro(iVisit:integer):boolean;
    procedure NoEscribir(Sender: TObject; var Key: Char);
    procedure DialogoError( const sMensaje: string; oControl:TWinControl );
	{$ifdef TEST_COMPLETE}
    function GetInformacionGridBusquedaVisit: String;
    function GetDatosGridTestComplete(const ds: TDataSet; const sCampos: string): string;
	{$endif}
  public
    { Public declarations }
    property bCerrarBusqueda: Boolean read FCerrarBusqueda write FCerrarBusqueda;
    property CancelaBusqueda: Boolean read FCancelaBusqueda write FCancelaBusqueda;
  published
    { Published declarations }
    {$ifdef TEST_COMPLETE} property InformacionGridBusquedaVisit: String read GetInformacionGridBusquedaVisit;{$endif}

  protected
    AColumn: TcxGridDBColumn;
  end;

  procedure creaBusquedaVisitante;

var
  FBusquedaVisitante: TFBusquedaVisitante;

implementation

uses
    DVisitantes,
    DCliente,
    ZetaCommonClasses,
    FRegistroEntradaVisi,
    ZetaDialogo,
    FMostrarFotografia,
    ZetaCommonTools, 
	FTressShell;

{$R *.dfm}

const
     K_CONTINUAR = '� Para poder continuar, es necesario que escriba ';
     K_MSG_FAVOR_NOMBRES = K_CONTINUAR + 'su nombre o apellido !';
     K_MSG_FAVOR_NOMBRE = K_CONTINUAR + 'su nombre !';
     K_MSG_FAVOR_APELLIDO  = K_CONTINUAR + 'su apellido';
     K_MSG_FAVOR_COMPANIA = K_CONTINUAR + 'el nombre de la compa��a';
     K_MSG_FAVOR_VISITANDO = K_CONTINUAR + 'el nombre de la persona a la que visita';

procedure CreaBusquedaVisitante;
begin
     if FBusquedaVisitante = nil then
        FBusquedaVisitante := TFBusquedaVisitante.Create( Application );
     if (FBusquedaVisitante <> nil) then
        FBusquedaVisitante.ShowModal;
end;

procedure TFBusquedaVisitante.btnBuscarClick(Sender: TObject);
var
   bBusquedaExitosa:boolean;
begin
     if StrLleno( edNombre.Text ) or StrLleno(edApePat.Text) then
     begin
          Screen.Cursor := crHourglass;
          try
          with dmVisitantes do
          begin
               BuscaVisitanteSQL(1, UPPERCASE( edNombre.Text + ' ' + edApePat.Text) );
               bBusquedaExitosa := (cdsSQL.RecordCount > 0);
               Ok.Enabled:=bBusquedaExitosa;
               MuestraGrid(bBusquedaExitosa);
               lbVisiEncontrados.Visible:=bBusquedaExitosa;
               btnNuevo.Enabled := true;
               btnNuevo.SetFocus;
          end;
          finally
                 Screen.Cursor := crDefault;
          end;
     end
     else
     begin
          DialogoError( K_MSG_FAVOR_NOMBRES, edNombre );
     end;
end;

procedure TFBusquedaVisitante.btnNuevoClick(Sender: TObject);
begin
     if FRegistroEntradaVisit = nil then
        FRegistroEntradaVisit := TFRegistroEntradaVisit.Create(self);
     FRegistroEntradaVisit.iVisiNumero := -1;
     dmVisitantes.cdsLibros.Append;

     FRegistroEntradaVisit.sPNombreVisiNuevo := edNombre.Text;
     FRegistroEntradaVisit.sPApePatVisiNuevo := edApePat.Text;
     //FRegistroEntradaVisit.ShowModal; //Se paso al shell
     CancelaBusqueda := False; //Agregado para saber si se debe abrir la forma de captura del visitante o no.
     self.Close; //Para que siempre limpie el formState
end;

procedure TFBusquedaVisitante.CancelClick(Sender: TObject);
begin
     {***Cuando se cancela la operacion se reinician los valores.***}
     if FRegistroEntradaVisit <> nil then
        FRegistroEntradaVisit.iVisiNumero:=-1;
     TressShell.bEsRegistroGrupo := false;
     {***}
     close;
end;

procedure TFBusquedaVisitante.connect;
begin
     with dmVisitantes do
     begin
          dsVisita.Dataset := cdsVisitanteLookup;
          cdsVisitante.Conectar;
          dsSQL.DataSet := cdsSQL;
          //cdsSQL.Conectar;
     end;
end;

procedure TFBusquedaVisitante.cxGrid1DBTableView1ColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
     if cxGrid1DBTableView1.DataController.DataModeController.GridMode then
     begin
          inherited;
          self.AColumn := TcxGridDBColumn(AColumn);
     end;
end;

procedure TFBusquedaVisitante.cxGrid1DBTableView1CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
     with cxGrid1DBTableView1.DataController do
     begin
          if AViewInfo.Item = VI_PRETYNAME then
          if (AViewInfo.GridRecord.Values[5] = '1') then
             ACanvas.Font.Color := RGB(194, 0, 0);
     end;
end;

procedure TFBusquedaVisitante.cxGrid1DBTableView1DataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
   AIndex: Integer;
begin
     inherited;
     AIndex := AValueList.FindItemByKind(fviCustom);
     if AIndex <> -1 then
        AValueList.Delete(AIndex);
end;

procedure TFBusquedaVisitante.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
     OkClick(self);
end;

procedure TFBusquedaVisitante.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     if key = 13 then
        cxGrid1DBTableView1DblClick(Self);
end;

procedure TFBusquedaVisitante.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     NoEscribir(Sender, Key);
end;


procedure TFBusquedaVisitante.DialogoError(const sMensaje: string;
  oControl: TWinControl);
begin
     oControl.SetFocus;
     ZInformation( Caption, sMensaje, 0 );
end;

procedure TFBusquedaVisitante.edApePatKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if key = 13 then
     begin
          btnBuscarClick(Self);
          //self.SelectNext(TEdit(Sender), true, true) ;
     end;
end;

procedure TFBusquedaVisitante.edApePatKeyPress(Sender: TObject; var Key: Char);
begin
     if ( Key = ZetaCommonClasses.UnaCOMILLA ) then
        Key := Chr( 0 );
     inherited KeyPress( Key );
end;

procedure TFBusquedaVisitante.edNombreKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if key = 13 then
        edApePat.SetFocus;
end;

procedure TFBusquedaVisitante.edNombreKeyPress(Sender: TObject; var Key: Char);
begin
     if ( Key = ZetaCommonClasses.UnaCOMILLA ) then
        Key := Chr( 0 );
     inherited KeyPress( Key );
end;

function TFBusquedaVisitante.ExisteVisitanteEnLibro(iVisit: integer): boolean;
begin
      Result:= dmVisitantes.cdsLibros.Locate('VI_NUMERO;LI_SAL_HOR', VarArrayOf([iVisit,'']),[]);
end;

procedure TFBusquedaVisitante.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     //FRegistroEntradaVisit.iVisiNumero:=-1;  //Se mueve a los eventos que lo requieren porque el boton de OTRO VISITANTE, NO LO USA
     //TressShell.bEsRegistroGrupo := false;
end;

procedure TFBusquedaVisitante.FormCreate(Sender: TObject);
begin
     FRegistroEntradaVisit := NIL;
     connect;
     Self.KeyPreview := True;
end;

procedure TFBusquedaVisitante.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if key = #27 then
        Close;
end;

procedure TFBusquedaVisitante.FormShow(Sender: TObject);
begin
     self.Width := 540;
     dmVisitantes.OPCSQL:=1;
     MuestraGrid(false);
     //cxGrid1DBTableView1.ApplyBestFit();
     lbVisiEncontrados.Visible := false;
     edNombre.SetFocus;
     edNombre.Clear;
     edApePat.Clear;
     dsSQL.DataSet.Close;
     ok.Enabled:=false;
     btnNuevo.Enabled := false;

     //Desactiva la posibilidad de agrupar
     cxGrid1DBTableView1.OptionsCustomize.ColumnGrouping := False;
     //Esconde la caja de agrupamiento
     cxGrid1DBTableView1.OptionsView.GroupByBox := False;
     //Para que nunca muestre el filterbox inferior
     cxGrid1DBTableView1.FilterBox.Visible := fvNever;
     //Para que no aparezca el Custom Dialog
     cxGrid1DBTableView1.FilterBox.CustomizeDialog := False;
     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     //cxGrid1DBTableView1.ApplyBestFit();

     if FRegistroEntradaVisit = nil then
       FRegistroEntradaVisit := TFRegistroEntradaVisit.Create(Self);
     FRegistroEntradaVisit.iVisiNumero:=-1;

     //Para que nunca muestre el filterbox inferior
     cxGrid1DBTableView1.FilterBox.Visible := fvNever;
     FCancelaBusqueda := True; //Agregado para saber si se debe abrir la forma de captura del visitante o no.
end;



procedure TFBusquedaVisitante.MuestraGrid(bMuestra: boolean);
begin
     if bMuestra then
     begin
          FBusquedaVisitante.Height := 327;
          cxGrid1.Visible:=true;
          self.Width := 600;
     end
     else
     begin
          FBusquedaVisitante.Height := 162;
          cxGrid1.Visible:=false;
          self.Width := 600;
     end;
end;

procedure TFBusquedaVisitante.NoEscribir(Sender: TObject; var Key: Char);
begin
     Key := #0;
end;

procedure TFBusquedaVisitante.OkClick(Sender: TObject);
const
     K_MENSAJE = 'El visitante ya tiene un registro de entrada,'+#13+'para poder continuar es necesario registrar primero la salida.';
var
   iVisit:integer;
begin
     iVisit:=StrToInt(cxGrid1DBTableView1.Controller.FocusedRow.Values[cxGrid1DBTableView1.Columns[0].Index]);
     if FRegistroEntradaVisit = nil then
        FRegistroEntradaVisit := TFRegistroEntradaVisit.Create(self);
     if not ExisteVisitanteEnLibro(iVisit) then
     begin
          self.visible := false;
          with dmVisitantes.cdsLibros do
          begin
               if not ( State in [dsInsert] ) then
                  Append;
               FRegistroEntradaVisit.iVisiNumero := iVisit;
               //FRegistroEntradaVisit.ShowModal;   //Se paso al shell
               //if (not TressShell.bEsRegistroGrupo) and (not TressShell.FPPrimerRegGrupo) then   //Con Ok o doble clic siempre hara un close de la forma
               FCancelaBusqueda := False; //Agregado para saber si se debe abrir la forma de captura del visitante o no.
               self.close;
          end;
     end
     else
         ZetaDialogo.ZInformation( 'Registro de entrada', K_MENSAJE, 0 );
end;

procedure TFBusquedaVisitante._AMuestraFotoExecute(Sender: TObject);
begin
     if MostrarFotografia = nil then
        MostrarFotografia := TMostrarFotografia.Create(self);
     MostrarFotografia.ShowModal;
end;

{$ifdef TEST_COMPLETE}
function TFBusquedaVisitante.GetDatosGridTestComplete(const ds: TDataSet;
  const sCampos: string): string;
var
  sInfoGrid:string;
  sListaCampos:TStringList;
  i:Integer;
begin
     sListaCampos := TStringList.Create;
     sListaCampos.Delimiter := ' ';
     sListaCampos.QuoteChar := '|';
     sListaCampos.DelimitedText := sCampos;
     Result := '';
     sInfoGrid := '';
      with ds do
      begin
           first;
           while not eof do
           begin
                for i := 0 to sListaCampos.Count-1 do
                   sInfoGrid := sInfoGrid + Trim( fieldByName(sListaCampos[i]).AsString ) + K_PIPE;

                sInfoGrid := sInfoGrid + CR_LF;
                Next;
          end;
          Result := sInfoGrid + CR_LF;
      end;
end;

function TFBusquedaVisitante.GetInformacionGridBusquedaVisit: String;
{$ifdef TEST_COMPLETE}
const
     K_CAMPOS_GRID = '|VI_PRETYNAME| |Empresa|';
{$endif}
begin
     {$ifdef TEST_COMPLETE}

         result := '';
         result := GetDatosGridTestComplete(dsSql.DataSet, K_CAMPOS_GRID);

     {$endif}
end;
{$endif}
end.
