inherited Configura: TConfigura
  Left = 418
  Top = 383
  Caption = 'Configurar Estaci'#243'n'
  ClientHeight = 283
  ClientWidth = 399
  OldCreateOrder = True
  OnCreate = FormCreate
  ExplicitWidth = 405
  ExplicitHeight = 317
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 8
    Top = 40
    Width = 111
    Height = 13
    Caption = 'Impresi'#243'n de gafetes:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel [1]
    Left = 25
    Top = 16
    Width = 94
    Height = 13
    Caption = 'Impresora default:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  inherited PanelBotones: TPanel
    Top = 247
    Width = 399
    ExplicitTop = 221
    ExplicitWidth = 399
    inherited OK_DevEx: TcxButton
      Left = 226
      OnClick = OK_DevExClick
      ExplicitLeft = 226
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 306
      ExplicitLeft = 306
    end
  end
  object cbImpresionGafete: TZetaKeyCombo [3]
    Left = 120
    Top = 36
    Width = 145
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 1
    ListaFija = lfImpresionGafete
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
  end
  object cbImpresora: TZetaKeyCombo [4]
    Left = 120
    Top = 12
    Width = 270
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 2
    ListaFija = lfImpresionGafete
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
  end
  object GBMostrarCitas: TGroupBox [5]
    Left = 120
    Top = 93
    Width = 269
    Height = 121
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    object GBFiltroTiempo: TGroupBox
      Left = 16
      Top = 22
      Width = 238
      Height = 83
      TabOrder = 1
      object lbMinAntes: TLabel
        Left = 128
        Top = 24
        Width = 74
        Height = 13
        Caption = 'Minutos antes'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object lbMinDespues: TLabel
        Left = 128
        Top = 48
        Width = 89
        Height = 13
        Caption = 'Minutos despu'#233's'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object edMinAntes: TZetaNumero
        Left = 84
        Top = 20
        Width = 37
        Height = 21
        Mascara = mnDias
        TabOrder = 0
        Text = '0'
      end
      object edMinDespues: TZetaNumero
        Left = 84
        Top = 44
        Width = 37
        Height = 21
        Mascara = mnDias
        TabOrder = 1
        Text = '0'
      end
    end
    object cbFiltrarTiempo: TcxCheckBox
      Left = 24
      Top = 13
      Caption = 'Filtrar por tiempo'
      ParentBackground = False
      ParentColor = False
      ParentFont = False
      Style.Color = clWhite
      Style.Font.Charset = ANSI_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Segoe UI'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 0
      OnClick = cbFiltrarTiempoClick
      Width = 114
    end
  end
  object cbMostrarCitas: TcxCheckBox [6]
    Left = 126
    Top = 84
    Caption = 'Mostrar citas'
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    Style.Color = clWhite
    Style.Font.Charset = ANSI_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 3
    OnClick = cbMostrarCitasClick
    Width = 91
  end
  object chRequiereGridAvanzado: TcxCheckBox [7]
    Left = 118
    Top = 61
    Caption = 'Mostrar libro con detalle'
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    Style.Color = clWhite
    Style.Font.Charset = ANSI_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 5
    Width = 160
  end
  object chRegistroGrupo: TcxCheckBox [8]
    Left = 118
    Top = 220
    Caption = 'Habilitar registro de visitantes en grupo'
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    Style.Color = clWhite
    Style.Font.Charset = ANSI_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 6
    Width = 235
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 7012496
  end
end
