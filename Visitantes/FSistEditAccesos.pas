unit FSistEditAccesos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEditAccesos, ImgList, Buttons, ZetaDBTextBox, StdCtrls, ComCtrls,
  ExtCtrls;

type
  TSistEditAccesos = class(TZetaEditAccesos)
  private
    { Private declarations }
  protected
    function GetTipoDerecho(Nodo: TTreeNode): eDerecho; override;
    function TextoEspecial(const sText: String): Boolean; override;
  public
    { Public declarations }
  end;

var
  SistEditAccesos: TSistEditAccesos;

implementation
uses ZAccesosTress;
const  D_TEXT_DATOS_CONFI = 'Ver Datos Confidenciales';
       D_TEXT_CANCELAR_PROCESOS = 'Cancelar Procesos';


{$R *.DFM}

function TSistEditAccesos.GetTipoDerecho(Nodo: TTreeNode): eDerecho;
begin
     with Nodo do
     begin
          if ( Text = D_TEXT_DATOS_CONFI ) then
              Result := edBorraSistKardex
          else if ( Text = D_TEXT_CANCELAR_PROCESOS ) then
              Result := edBaja
          else
              Result := inherited GetTipoDerecho( Nodo );
     end;
end;


function TSistEditAccesos.TextoEspecial(const sText: String): Boolean;
begin
     Result := ( sText = 'Registro' ) or
               ( sText = 'Procesos' ) or
               inherited TextoEspecial( sText );
end;

end.
