unit FGridCaseta_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Grids, DBGrids, ZetaDBGrid, DBCtrls,
  ZetaSmartLists, Buttons, ExtCtrls, StdCtrls, ZetaKeyCombo, Mask,
  ZetaFecha, ZBaseGridEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TGridCaseta_DevEx = class(TBaseGridEdicion_DevEx)
    Panel1: TPanel;
    BtnEntrar: TcxButton;
    BtnSalir: TcxButton;
    ZCombo: TZetaDBKeyCombo;
    ZFecha: TZetaDBFecha;
    procedure FormCreate(Sender: TObject);
    procedure CasetaPuntualClickGrid(Sender: TObject);    
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure ZetaDBGridColExit(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    procedure BuscaEmpleado;
    procedure HabilitaEntradaSalida;
  public
    { Public declarations }
  protected
    { Protected declarations}
    procedure Connect; override;
    procedure Buscar; override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  end;

var
  GridCaseta_DevEx: TGridCaseta_DevEx;

implementation
uses
    DCafeteria,
    DCliente,
    DAsistencia,
    ZetaDialogo,
    dCatalogos,
    dSistema,
    ZetaBuscaEmpleado_DevEx,
    ZetaCommonClasses;

{$R *.dfm}

procedure TGridCaseta_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_EMP_REG_CASETA_DIARIAS;
     with ZCombo.Lista do
     begin
          Add( K_GLOBAL_SI + '=' + 'Entrada' );
          Add( K_GLOBAL_NO + '=' + 'Salida' );
     end;
end;

procedure TGridCaseta_DevEx.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmCafeteria do
     begin
          cdsCaseta.Refrescar;
          DataSource.DataSet:= cdsCaseta;
     end;
     dmCatalogos.cdsHorarios.Conectar;
     dmSistema.cdsUsuarios.Conectar;     
end;

procedure TGridCaseta_DevEx.Buscar;
begin
     with ZetaDBGrid do
     begin
          if ( SelectedField.FieldName = 'CB_CODIGO' ) then
               BuscaEmpleado;
     end;
end;

procedure TGridCaseta_DevEx.BuscaEmpleado;
var
   sKey, sDescription: String;
begin
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
     begin
          with dmCafeteria.cdsCaseta do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName('CB_CODIGO').AsString:= sKey;
          end;
     end;
end;

procedure TGridCaseta_DevEx.KeyPress(var Key: Char);
begin
     inherited;
     if GridEnfocado then
     begin
          if ( ( Key <> chr(9) ) and ( key <> #0 ) ) then
          begin
               if ( ZetaDBGrid.SelectedField.FieldName = 'AL_FECHA' ) then
               begin
                    ZFecha.SetFocus;
                    SendMessage(ZFecha.Handle, WM_Char, word(Key), 0);
                    Key:= #0;
               end;
          end
     end
     else if ( ActiveControl = ZFecha ) and ( ( Key = chr( VK_RETURN ) ) or ( Key = chr( 9 ) ) ) then
     begin                                      
          Key := #0;
          with ZetaDBGrid do
          begin
               SetFocus;
               SelectedField := dmCafeteria.cdsCaseta.FieldByName( 'AL_HORA' );
          end;
     end;
end;

procedure TGridCaseta_DevEx.ZetaDBGridDrawColumnCell(Sender: TObject;
           const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
inherited;
    if (gdFocused in State) then
        if ( Column.FieldName = 'AL_ENTRADA' ) then
        begin
           with ZCombo do
           begin
                Left := Rect.Left + ZetaDBGrid.Left;
                Top := Rect.Top + ZetaDBGrid.Top;
                Width:= Column.Width + 2;
                Visible := True;
           end;
        end
        else if ( Column.FieldName = 'AL_FECHA' ) then
        begin
           with zFecha do
           begin
                Left := Rect.Left + ZetaDBGrid.Left;
                Top := Rect.Top + ZetaDBGrid.Top;
                Width:= Column.Width + 2;
                Visible := True;
           end;
    end;
end;
     
procedure TGridCaseta_DevEx.ZetaDBGridColExit(Sender: TObject);
begin
     inherited;
     with ZetaDBGrid do
     begin
          if ( SelectedField <> nil ) and ( SelectedField.FieldName = 'AL_ENTRADA' ) and
             ( ZCombo.Visible ) then
             ZCombo.Visible:= False
          else if ( SelectedField <> nil ) and ( SelectedField.FieldName = 'AL_FECHA' ) and
             ( ZFecha.Visible ) then
             ZFecha.Visible:= False;
     end;
end;

procedure TGridCaseta_DevEx.CasetaPuntualClickGrid(Sender: TObject);
begin
     with dmCafeteria.CdsCaseta do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;     
          FieldByName( 'AL_HORA' ).AsString := dmCafeteria.GetHoraEntSalEmp( dmCafeteria.cdsCaseta, FieldByName( 'CB_CODIGO' ).AsInteger , dmCliente.FechaDefault, TBitBtn(Sender).Tag );
     end;
end;

procedure TGridCaseta_DevEx.HabilitaEntradaSalida;
begin
     inherited;
     BtnEntrar.Enabled:= not ( dmCafeteria.cdsCaseta.IsEmpty );
     BtnSalir.Enabled:= BtnEntrar.Enabled;
end;

procedure TGridCaseta_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     HabilitaEntradaSalida;
end;

end.

