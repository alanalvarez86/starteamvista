unit FCasDiarias_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, Grids, DBGrids,ZetaDBGrid,ZBaseGridLectura_DevEx,
  StdCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore,dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, Mask,
  ZetaFecha,ftressShell;
type
  TCasDiarias_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    AsistenciaFechaZF: TZetaFecha;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AsistenciaFechaZFValidDate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  protected
     { Protected declarations }
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Connect; override;
    procedure Refresh; override; 
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CasDiarias_DevEx: TCasDiarias_DevEx;

implementation

uses dRecursos,
     DCafeteria,
     ZetaCommonLists,
     ZetaCommonClasses, DCliente;

{$R *.dfm}

procedure TCasDiarias_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_EMP_CASETA;
     TipoValorActivo1 := stEmpleado;
end;

procedure TCasDiarias_DevEx.Connect;
begin
     with dmCafeteria do
     begin
          cdsCasDiarias.Conectar;
          DataSource.DataSet:= cdsCasDiarias;
     end;
end;

procedure TCasDiarias_DevEx.Refresh;
begin
     dmCafeteria.cdsCasDiarias.Refrescar;
end;

procedure TCasDiarias_DevEx.Agregar;
begin
     dmCafeteria.cdsCasDiarias.Agregar;
end;

procedure TCasDiarias_DevEx.Borrar;
begin
     dmCafeteria.cdsCasDiarias.Borrar;
end;

procedure TCasDiarias_DevEx.Modificar;
begin
     dmCafeteria.cdsCasDiarias.Modificar;
end;

procedure TCasDiarias_DevEx.FormShow(Sender: TObject);
begin
  CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
  inherited;
//CreaColumaSumatoria('AL_COMENT', SkCount);
 AsistenciaFechaZF.Valor :=  dmCliente.FechaAsistencia;
end;

procedure TCasDiarias_DevEx.AsistenciaFechaZFValidDate(Sender: TObject);
begin
  inherited;
 TressShell.AsistenciaFechavalid(AsistenciaFechaZF.Valor);
end;

procedure TCasDiarias_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
 AsistenciaFechaZF.Valor :=  dmCliente.FechaAsistencia;
end;

end.
