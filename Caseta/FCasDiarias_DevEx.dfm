inherited CasDiarias_DevEx: TCasDiarias_DevEx
  Left = 686
  Top = 226
  Caption = 'Checadas Diarias'
  PixelsPerInch = 96
  TextHeight = 13
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 60
    Height = 164
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = DataSource
      object AL_HORA: TcxGridDBColumn
        Caption = 'Hora'
        DataBinding.FieldName = 'AL_HORA'
        MinWidth = 40
        Width = 40
      end
      object AL_ENTRADA: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'AL_ENTRADA'
        MinWidth = 50
        Width = 105
      end
      object AL_CASETA: TcxGridDBColumn
        Caption = 'Caseta'
        DataBinding.FieldName = 'AL_CASETA'
        MinWidth = 60
        Width = 85
      end
      object AE_CODIGO: TcxGridDBColumn
        Caption = 'Regla'
        DataBinding.FieldName = 'AE_CODIGO'
        MinWidth = 60
        Width = 85
      end
      object AL_COMENT: TcxGridDBColumn
        Caption = 'Observaciones'
        DataBinding.FieldName = 'AL_COMENT'
        MinWidth = 100
        Width = 200
      end
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 19
    Width = 624
    Height = 41
    Align = alTop
    TabOrder = 2
    DesignSize = (
      624
      41)
    object Panel3: TPanel
      Left = 313
      Top = 4
      Width = 307
      Height = 33
      Anchors = [akTop, akRight]
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        307
        33)
      object Label1: TLabel
        Left = 102
        Top = 12
        Width = 69
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'Fecha Activa :'
      end
      object AsistenciaFechaZF: TZetaFecha
        Left = 182
        Top = 6
        Width = 115
        Height = 22
        Cursor = crArrow
        Hint = 'Fecha Default para Tarjetas de Asistencia'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Text = '10/dic/97'
        UseEnterKey = True
        Valor = 35774.000000000000000000
        OnValidDate = AsistenciaFechaZFValidDate
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 376
    Top = 88
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end