inherited GridCaseta: TGridCaseta
  Left = 493
  Top = 362
  Caption = 'Registro de checadas de caseta'
  ClientHeight = 284
  ClientWidth = 509
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 248
    Width = 509
    inherited OK: TBitBtn
      Left = 341
    end
    inherited Cancelar: TBitBtn
      Left = 426
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 509
    Height = 36
    inherited Splitter: TSplitter
      Height = 36
    end
    inherited ValorActivo1: TPanel
      Height = 36
    end
    inherited ValorActivo2: TPanel
      Width = 183
      Height = 36
    end
  end
  inherited PanelSuperior: TPanel
    Width = 509
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Top = 68
    Width = 509
    Height = 180
    OnColExit = ZetaDBGridColExit
    OnDrawColumnCell = ZetaDBGridDrawColumnCell
    Columns = <
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Caption = 'N'#250'mero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRETTYNAME'
        Title.Caption = 'Nombre Completo'
        Width = 105
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AL_FECHA'
        Title.Caption = 'Fecha'
        Width = 85
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AL_HORA'
        Title.Caption = 'Hora'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AL_ENTRADA'
        Title.Caption = 'Tipo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AL_COMENT'
        Title.Caption = 'Observaciones'
        Width = 105
        Visible = True
      end>
  end
  object BtnEntrar: TButton [4]
    Left = 362
    Top = 36
    Width = 33
    Height = 25
    Hint = 'Agregar entrada puntual'
    Caption = 'E'
    TabOrder = 4
    OnClick = CasetaPuntualGridClick
  end
  object BtnSalir: TButton [5]
    Tag = 1
    Left = 399
    Top = 36
    Width = 32
    Height = 25
    Hint = 'Agregar salida puntual'
    Caption = 'S'
    TabOrder = 5
    OnClick = CasetaPuntualGridClick
  end
  object ZEntradaSalida: TZetaKeyCombo [6]
    Left = 216
    Top = 120
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 6
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
  end
  inherited DataSource: TDataSource
    Left = 284
    Top = 33
  end
end
