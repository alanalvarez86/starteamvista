inherited EditPresup_DevEx: TEditPresup_DevEx
  Left = 308
  Top = 189
  Caption = 'Editar Presupuesto'
  ClientHeight = 320
  ClientWidth = 439
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 284
    Width = 439
    inherited OK_DevEx: TcxButton
      Left = 275
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 354
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 439
    inherited ValorActivo2: TPanel
      Width = 113
      inherited textoValorActivo2: TLabel
        Width = 107
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 11
  end
  object Panel5: TPanel [3]
    Left = 0
    Top = 134
    Width = 439
    Height = 21
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object CT_NIVEL5lbl: TLabel
      Left = 88
      Top = 4
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nivel5:'
    end
    object CT_NIVEL_5: TZetaDBTextBox
      Left = 127
      Top = 2
      Width = 60
      Height = 17
      AutoSize = False
      Caption = 'CT_NIVEL_5'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataField = 'CT_NIVEL_5'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object TB_ELEMENT: TZetaDBTextBox
      Left = 194
      Top = 2
      Width = 230
      Height = 17
      AutoSize = False
      Caption = 'TB_ELEMENT'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  object Panel4: TPanel [4]
    Left = 0
    Top = 113
    Width = 439
    Height = 21
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object CT_NIVEL4lbl: TLabel
      Left = 88
      Top = 4
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nivel4:'
    end
    object CT_NIVEL_4: TZetaDBTextBox
      Left = 127
      Top = 2
      Width = 60
      Height = 17
      AutoSize = False
      Caption = 'CT_NIVEL_4'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataField = 'CT_NIVEL_4'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object TB_ELEMENT4: TZetaDBTextBox
      Left = 194
      Top = 2
      Width = 230
      Height = 17
      AutoSize = False
      Caption = 'TB_ELEMENT4'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  object Panel3: TPanel [5]
    Left = 0
    Top = 92
    Width = 439
    Height = 21
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 5
    object CT_NIVEL3lbl: TLabel
      Left = 88
      Top = 4
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nivel3:'
    end
    object CT_NIVEL_3: TZetaDBTextBox
      Left = 127
      Top = 2
      Width = 60
      Height = 17
      AutoSize = False
      Caption = 'CT_NIVEL_3'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataField = 'CT_NIVEL_3'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object TB_ELEMENT3: TZetaDBTextBox
      Left = 194
      Top = 2
      Width = 230
      Height = 17
      AutoSize = False
      Caption = 'TB_ELEMENT3'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  object Panel2: TPanel [6]
    Left = 0
    Top = 71
    Width = 439
    Height = 21
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 6
    object CT_NIVEL2lbl: TLabel
      Left = 88
      Top = 4
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nivel2:'
    end
    object CT_NIVEL_2: TZetaDBTextBox
      Left = 127
      Top = 2
      Width = 60
      Height = 17
      AutoSize = False
      Caption = 'CT_NIVEL_2'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataField = 'CT_NIVEL_2'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object TB_ELEMENT2: TZetaDBTextBox
      Left = 194
      Top = 2
      Width = 230
      Height = 17
      AutoSize = False
      Caption = 'TB_ELEMENT2'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  object Panel1: TPanel [7]
    Left = 0
    Top = 50
    Width = 439
    Height = 21
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 7
    object CT_NIVEL1lbl: TLabel
      Left = 88
      Top = 4
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nivel1:'
    end
    object CT_NIVEL_1: TZetaDBTextBox
      Left = 127
      Top = 2
      Width = 60
      Height = 17
      AutoSize = False
      Caption = 'CT_NIVEL_1'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataField = 'CT_NIVEL_1'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object TB_ELEMENT1: TZetaDBTextBox
      Left = 194
      Top = 2
      Width = 230
      Height = 17
      AutoSize = False
      Caption = 'TB_ELEMENT1'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  object PageControl_DevEx: TcxPageControl [8]
    Left = 0
    Top = 155
    Width = 439
    Height = 129
    Align = alClient
    TabOrder = 12
    Properties.ActivePage = Conteo_DevEx
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 127
    ClientRectLeft = 2
    ClientRectRight = 437
    ClientRectTop = 27
    object Conteo_DevEx: TcxTabSheet
      Caption = 'Conteo'
      object PanelCaptura: TPanel
        Left = 0
        Top = 0
        Width = 435
        Height = 100
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 115
          Top = 12
          Width = 62
          Height = 13
          Alignment = taRightJustify
          Caption = 'Presupuesto:'
        end
        object Label2: TLabel
          Left = 152
          Top = 36
          Width = 25
          Height = 13
          Caption = 'Real:'
        end
        object CT_DIFERENCIA: TZetaDBTextBox
          Left = 183
          Top = 56
          Width = 60
          Height = 21
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'CT_DIFERENCIA'
          ShowAccelChar = False
          Brush.Color = clSilver
          Border = False
          DataField = 'VACANTES'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label3: TLabel
          Left = 129
          Top = 60
          Width = 48
          Height = 13
          Caption = 'Vacantes:'
        end
        object ZetaDBTextBox1: TZetaDBTextBox
          Left = 183
          Top = 80
          Width = 60
          Height = 21
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ZetaDBTextBox1'
          ShowAccelChar = False
          Brush.Color = clSilver
          Border = False
          DataField = 'EXCEDENTE'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label4: TLabel
          Left = 123
          Top = 84
          Width = 54
          Height = 13
          Caption = 'Excedente:'
        end
        object Label5: TLabel
          Left = 313
          Top = 36
          Width = 8
          Height = 13
          Caption = '%'
        end
        object CT_CUANTOS: TZetaDBNumero
          Left = 183
          Top = 8
          Width = 61
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
          UseEnterKey = True
          OnExit = CT_CUANTOSExit
          DataField = 'CT_CUANTOS'
          DataSource = DataSource
        end
        object ZetaDBNumero1: TZetaDBNumero
          Left = 183
          Top = 32
          Width = 61
          Height = 21
          TabStop = False
          Color = clBtnFace
          Mascara = mnEmpleado
          ReadOnly = True
          TabOrder = 1
          DataField = 'CT_REAL'
          DataSource = DataSource
        end
        object ZetaDBNumero2: TZetaDBNumero
          Left = 247
          Top = 32
          Width = 61
          Height = 21
          TabStop = False
          Color = clBtnFace
          Mascara = mnPesos
          ReadOnly = True
          TabOrder = 2
          Text = '0.00'
          DataField = 'CT_TASA'
          DataSource = DataSource
        end
      end
    end
    object Adicionales_DevEx: TcxTabSheet
      Caption = 'Adicionales'
      object GBNumeroAdic: TGroupBox
        Left = 304
        Top = 6
        Width = 121
        Height = 97
        Caption = ' N'#250'meros '
        TabOrder = 1
        object CT_NUMERO1: TZetaDBNumero
          Left = 16
          Top = 18
          Width = 89
          Height = 21
          Mascara = mnPesos
          TabOrder = 0
          Text = '0.00'
          DataField = 'CT_NUMERO1'
          DataSource = DataSource
        end
        object CT_NUMERO2: TZetaDBNumero
          Left = 16
          Top = 44
          Width = 89
          Height = 21
          Mascara = mnPesos
          TabOrder = 1
          Text = '0.00'
          DataField = 'CT_NUMERO2'
          DataSource = DataSource
        end
        object CT_NUMERO3: TZetaDBNumero
          Left = 16
          Top = 70
          Width = 89
          Height = 21
          Mascara = mnPesos
          TabOrder = 2
          Text = '0.00'
          DataField = 'CT_NUMERO3'
          DataSource = DataSource
        end
      end
      object GBTextoAdic: TGroupBox
        Left = 24
        Top = 6
        Width = 265
        Height = 81
        Caption = ' Textos '
        TabOrder = 0
        object CT_TEXTO1: TZetaDBEdit
          Left = 8
          Top = 18
          Width = 241
          Height = 21
          TabOrder = 0
          Text = 'CT_TEXTO1'
          DataField = 'CT_TEXTO1'
          DataSource = DataSource
        end
        object CT_TEXTO2: TZetaDBEdit
          Left = 8
          Top = 48
          Width = 241
          Height = 21
          TabOrder = 1
          Text = 'CT_TEXTO2'
          DataField = 'CT_TEXTO2'
          DataSource = DataSource
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 4
    Top = 41
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_AgregarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_BorrarBtn: TdxBarButton
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
