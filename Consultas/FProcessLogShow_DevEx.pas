{$HINTS OFF}
unit FProcessLogShow_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ExtCtrls, StdCtrls, Buttons,
     Grids, DBGrids, DBCtrls, Db, ComCtrls, Variants,
     ZetaCommonLists,
     ZetaDBTextBox,
     ZetaDBGrid, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters,
  Menus, cxButtons, cxNavigator, cxDBNavigator, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  ZetaCXGrid, cxContainer, cxTextEdit, cxMemo, cxDBEdit;

type
  TLogShow_DevEx = class(TForm)
    Datasource: TDataSource;
    PanelSuperior: TPanel;
    InicioLBL: TLabel;
    FinLBL: TLabel;
    DuracionLBL: TLabel;
    Label2: TLabel;
    CanceladoPorLBL: TLabel;
    UsuarioLBL: TLabel;
    StatusLBL: TLabel;
    EmpleadosLBL: TLabel;
    PC_TIEMPO: TZetaDBTextBox;
    dsLog: TDataSource;
    PC_STATUS: TZetaDBTextBox;
    PC_NUMERO: TZetaDBTextBox;
    US_CODIGO: TZetaDBTextBox;
    US_CANCELA: TZetaDBTextBox;
    PC_MAXIMO: TZetaDBTextBox;
    UltimoEmpleadoLBL: TLabel;
    CB_CODIGO: TZetaDBTextBox;
    AvanceLBL: TLabel;
    PC_AVANCE: TZetaDBTextBox;
    PC_INICIO: TZetaDBTextBox;
    PC_FIN: TZetaDBTextBox;
    SaveDialog: TSaveDialog;
    lblParametros: TLabel;
    PanelInferior_DevEx: TPanel;
    DevEx_cxDBNavigatorEdicion: TcxDBNavigator;
    Imprimir_DevEx: TcxButton;
    ExportarBtn_DevEx: TcxButton;
    Salir_DevEx: TcxButton;
    ZetaCXGridDBTableView: TcxGridDBTableView;
    ZetaCXGridLevel: TcxGridLevel;
    ZetaCXGrid: TZetaCXGrid;
    BI_FECHA: TcxGridDBColumn;
    BI_HORA: TcxGridDBColumn;
    BI_TIPO: TcxGridDBColumn;
    CB_CODIGO_GRID: TcxGridDBColumn;
    BI_TEXTO: TcxGridDBColumn;
    BI_FEC_MOV: TcxGridDBColumn;
    PC_PARAM: TcxDBMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    //procedure DBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    //procedure DBGridDblClick(Sender: TObject);
    procedure Imprimir_DevExClick(Sender: TObject);
    procedure ExportarBtn_DevExClick(Sender: TObject);
    procedure Salir_DevExClick(Sender: TObject);
    procedure ZetaCXGridDBTableViewDblClick(Sender: TObject);
    procedure ZetaCXGridDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure ZetaCXGridDBTableViewCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure ZetaCXGridDBTableViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    FProceso: Procesos;
    function GetBitacoraFile( var sArchivo: String ): Boolean;
    procedure Connect;
    procedure Disconnect;
    procedure ExportarBitacora;
    procedure SetProceso(const Value: Procesos);
    procedure ReconnectGrid;
    procedure RefreshGrid;
{$IFNDEF TRESS_DELPHIXE5_UP}
    function ConvierteATDBGrid (DevEx_TableView: TcxGridDBTableView): TDBGrid;
{$ENDIF}
    procedure ApplyMinWidth;
  public
    { Public declarations }
    property Proceso: Procesos read FProceso write SetProceso;
  end;

var
  LogShow_DevEx: TLogShow_DevEx;

procedure GetBitacora( const eProceso: Procesos; const iFolio: Integer );
procedure ShowLog( const eProceso: Procesos );
implementation

uses ZAccesosMgr,
     ZAccesosTress,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     ZGridModeTools,
     ZetaMessages,
     ZetaDialogo,
     ZetaClientDataSet,
     {$IFDEF TRESS_DELPHIXE5_UP}
        ZImprimeGrid, //(@am): Se cambia FBaseReportes por ZImprimeGrid pues ahora toda la logica de exportacion esta ahi.
        {$ifdef CONCILIA}
        {$ifdef FONACOT}
          DConcilia;
        {$else}
          DCliente;
        {$endif}
        {$else}
          DConsultas;
        {$endif}
     {$ELSE}
        {$ifdef CONCILIA}
          ZImprimeGrid,
        {$ifdef FONACOT}
          DConcilia;
        {$else}
          DCliente;
        {$endif}
        {$else}
          FBaseReportes,
          DConsultas;
        {$endif}
     {$ENDIF}


{$R *.DFM}

procedure ShowLog( const eProceso: Procesos );
begin
     with TLogShow_DevEx.Create( Application ) do
     begin
          try
             Proceso := eProceso;
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure GetBitacora( const eProceso: Procesos; const iFolio: Integer );
begin
     {$ifdef CONCILIA}
     {$ifdef FONACOT}
     with dmConcilia do
     {$else}
     with dmCliente do
     {$endif}
     {$else}
     with dmConsultas do
          {$ifdef TRESS}
          if (eProceso in [ prSistImportarTablas, prSistImportarTablasCSV, prPatchCambioVersion ] ) then
          begin
               {***DevEx(@am): La siguiente validacion es necesaria pues la propiedad dmConsultas.BaseDatosImportacionXMLCSV
                               solo se inicializaba desde la ejecutacion de los procesos de: Importacion de Tablas, Importacion de Tablas CSV
                               y actualizacion de BD. De esta forma, al acceder la bitacora al final de un proceso o desde la opcion F9 despues de
                               ejecutar un proceso, esta aparecia correctamente. Sin embargo, al entrar a TRESS e ir directamente a la opcion F9
                               para ver una bitacora correspondiente a alguno de estos tres casos, SE desplegaba el error reportado en el bug 5493 en TP.***}
                if VarIsEmpty(BaseDatosImportacionXMLCSV) then
                    GetProcessLog(iFolio, FALSE)
                else
                    GetProcessLog(iFolio, TRUE);
          end
          else
          {$endif}
     {$endif}
          GetProcessLog(iFolio);
     ShowLog( eProceso );
end;

{ ************ TShowLog ************ }

procedure TLogShow_DevEx.FormCreate(Sender: TObject);
begin
     HelpContext := H00005_Bitacora_procesos;
     ZetaCXGridDBTableView.DataController.DataModeController.GridMode:= True;
     ZetaCXGridDBTableView.DataController.Filter.AutoDataSetFilter := True;
end;

procedure TLogShow_DevEx.FormShow(Sender: TObject);
begin
     Connect;
     //Desactiva la posibilidad de agrupar
     ZetaCXGridDBTableView.OptionsCustomize.ColumnGrouping := False;
     //Esconde la caja de agrupamiento
     ZetaCXGridDBTableView.OptionsView.GroupByBox := False;
     //Para que nunca muestre el filterbox inferior
     ZetaCXGridDBTableView.FilterBox.Visible := fvNever;
     //Para que no aparezca el Custom Dialog
     ZetaCXGridDBTableView.FilterBox.CustomizeDialog := False;
     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     ApplyMinWidth;
     ZetaCXGridDBTableView.ApplyBestFit();  
end;

procedure TLogShow_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     ZGridModeTools.LimpiaCacheGrid(ZetaCXGridDBTableView);
     ZetaCXGridDBTableView.DataController.Filter.Root.Clear;
     Disconnect;
end;

procedure TLogShow_DevEx.SetProceso( const Value: Procesos );
begin
     FProceso := Value;
     Caption := ZetaClientTools.GetProcessName( Proceso );
     EmpleadosLBL.Caption := ZetaClientTools.GetProcessElement( Proceso ) + ':';
end;

procedure TLogShow_DevEx.Connect;
begin
    {$ifdef CONCILIA}
     {$ifdef FONACOT}
     with dmConcilia do
     {$else}
     with dmCliente do
     {$endif}
     {$else}
     with dmConsultas do
     {$endif}
     begin
          Datasource.Dataset := cdsProcesos;
          dsLog.Dataset := cdsLogDetail;
     end;
     //DevEx:
     with Imprimir_DevEx do
     begin
          Enabled := ZAccesosMgr.CheckDerecho( D_CONS_BITACORA, K_DERECHO_IMPRESION );
          ExportarBtn_DevEx.Enabled := Enabled;
     end;
end;

procedure TLogShow_DevEx.Disconnect;
begin
     Datasource.Dataset := nil;
end;

{procedure TLogShow_DevEx.DBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
   oColor: TColor;
begin
     with DBGrid do
     begin
          oColor := Canvas.Font.Color;
          try
             if ( eTipoBitacora( Columns.Items[ 2 ].Field.AsInteger ) in [ tbError, tbErrorGrave ] ) then
                Canvas.Font.Color := clRed;
             DefaultDrawColumnCell( Rect, DataCol, Column, State );
          finally
                 Canvas.Font.Color := oColor;
          end;
     end;
end; }

procedure TLogShow_DevEx.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if Key = Chr( VK_ESCAPE ) then
     begin
          Key := #0;
          Close;
     end;
end;

function TLogShow_DevEx.GetBitacoraFile( var sArchivo: String ): Boolean;
begin
     with SaveDialog do
     begin
          FileName := ExtractFileName( sArchivo );
          InitialDir := ExtractFilePath( sArchivo );
          if Execute then
          begin
               Result := True;
               sArchivo := FileName;
          end
          else
              Result := False;
     end;
end;

procedure TLogShow_DevEx.ExportarBitacora;
var
   {$ifdef TRESS_DELPHIXE5_UP}
      Posicion: TBookMark;
   {$else}
      Posicion: TBookMarkStr;
   {$endif}
   sArchivo: String;
   oCursor: TCursor;
begin
    sArchivo := SetFileNameDefaultPath( Caption + '.txt' );
     if GetBitacoraFile( sArchivo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          with TStringList.Create do
          begin
               try
                  Add( 'Proceso:  ' + Self.Caption );
                  Add( 'Folio:    ' + PC_NUMERO.Caption );
                  Add( 'Inicio:   ' + PC_INICIO.Caption );
                  Add( 'Fin:      ' + PC_FIN.Caption );
                  Add( 'Duraci�n: ' + PC_TIEMPO.Caption );
                  Add( 'Status:   ' + PC_STATUS.Caption );
                  Add( 'Usuario:  ' + US_CODIGO.Caption );
                  Add( 'Cancelado Por: ' + US_CANCELA.Caption );
                  Add( ZetaClientTools.GetProcessElement( Proceso ) + ': ' + PC_MAXIMO.Caption );
                  Add( 'Ultimo Empleado: ' + CB_CODIGO.Caption );
                  Add( 'Parametros' );
                  Add( PC_PARAM.Text );                                    
                  Add( StringOfChar( '-', 40 ) );
                  with dsLog.Dataset do
                  begin
                       Posicion := BookMark;
                       DisableControls;
                       try
                          First;
                          while not Eof do
                          begin
                               Add( 'Tipo: ' + FieldByName( 'BI_TIPO' ).AsString );
                               Add( 'Empleado: ' + FieldByName( 'CB_CODIGO' ).AsString );
                               Add( 'Movimiento: ' + FieldByName( 'BI_FEC_MOV' ).AsString );
                               Add( 'Error: ' + FieldByName( 'BI_TEXTO' ).AsString );
                               Add( '' );
                               Add( FieldByName( 'BI_DATA' ).AsString );
                               Add( '===============================================' );
                               Next;
                          end;
                       finally
                              BookMark := Posicion;
                              EnableControls;
                       end;
                  end;
                  SaveToFile( sArchivo );
                  if ZetaDialogo.zConfirm( Caption, 'El Archivo ' + sArchivo + ' Fu� Creado' + CR_LF + '� Desea Verlo ?', 0, mbYes ) then
                  begin
                       ZetaClientTools.ExecuteFile( 'NOTEPAD.EXE', sArchivo, ExtractFileDir( sArchivo ), SW_SHOWDEFAULT );
                  end;
               finally
                      Free;
               end;
          end;
          Screen.Cursor := oCursor;
     end;
end;

procedure TLogShow_DevEx.US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     {$ifdef CONCILIA}
     {$else}
     dmConsultas.UsuarioGetText( Sender, Text, DisplayText );
     {$endif}
end;

{procedure TLogShow_DevEx.DBGridDblClick(Sender: TObject);
begin
     TZetaClientDataSet(dsLog.Dataset).Modificar;
end;}

{$IFNDEF TRESS_DELPHIXE5_UP}
function TLogShow_DevEx.ConvierteATDBGrid (DevEx_TableView: TcxGridDBTableView): TDBGrid;
var
   i: Integer;
   oGrid: TDBGrid;
begin
     //Creamos un TDBGrid Virtual
     oGrid := TDBGrid.Create(Self);
     for i := 0 to DevEx_TableView.ColumnCount -1 do
     begin

          with oGrid.Columns.Add do
          begin
               Visible       := DevEx_TableView.Columns[ i ].Visible;
               FieldName     := DevEx_TableView.Columns[ i ].DataBinding.FieldName;
               Title.Caption := DevEx_TableView.Columns[ i ].Caption;
          end;
     end;
     oGrid.DataSource :=  DevEx_TableView.DataController.DataSource;
     result := oGrid;
end;
{$ENDIF}

procedure TLogShow_DevEx.Imprimir_DevExClick(Sender: TObject);
{$IFNDEF TRESS_DELPHIXE5_UP}
var
   oGrid: TDBGrid;
{$ENDIF}
begin
     if ZetaDialogo.zConfirm( 'Imprimir...', '� Desea Imprimir El Grid De ' + Caption + '?', 0, mbYes ) then
     begin
          {$IFDEF TRESS_DELPHIXE5_UP}
          ZImprimeGrid.ImprimirGrid( ZetaCXGridDBTableView, ZetaCXGridDBTableView.DataController.DataSource.DataSet, Caption, 'IM', '','',stNinguno,stNinguno);
          {$ELSE}
          //(@am): Se deja el codigo tal y como estaba en vs 2014. Sin embargo se verifico el metodo ImprimirGrid en ambas unidades y es exactaqmente igual.
          oGrid := ConvierteATDBGrid ( ZetaCXGridDBTableView );
          try
             {$ifdef CONCILIA}
             ZImprimeGrid.ImprimirGrid( oGrid, oGrid.DataSource.DataSet, Caption, 'IM', '', '', stNinguno, stNinguno );
            {$else}
             FBaseReportes.ImprimirGrid( oGrid, oGrid.DataSource.DataSet, Caption, 'IM', '', '', stNinguno, stNinguno );
            {$endif}
          finally
                 oGrid.Free;
          end;
          {$ENDIF}
      end;
end;

procedure TLogShow_DevEx.ExportarBtn_DevExClick(Sender: TObject);
begin
     ExportarBitacora;
end;

procedure TLogShow_DevEx.Salir_DevExClick(Sender: TObject);
begin
     Close;
end;

procedure TLogShow_DevEx.ZetaCXGridDBTableViewDblClick(Sender: TObject);
begin
     TZetaClientDataSet(dsLog.Dataset).Modificar;
end;

procedure TLogShow_DevEx.ZetaCXGridDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
  AIndex: Integer;
begin
     inherited;
     ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if ZetaCXGridDBTableView.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( ZetaCXGridDBTableView, AItemIndex, AValueList );
end;

procedure TLogShow_DevEx.ZetaCXGridDBTableViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   //oColor: TColor;
   tipoBitacora:eTipoBitacora;
begin
  try
      //oColor:= ACanvas.Font.Color;
     for tipoBitacora:= tbError to tbErrorGrave do
     begin
          if ( ObtieneElemento( lfTipoBitacora, Ord( tipoBitacora))=  AViewInfo.GridRecord.DisplayTexts [2] ) then
          begin
               ACanvas.Font.Color := clRed;
          end;
     end;
  finally
         //ACanvas.Font.Color:= oColor;
  end;
end;

procedure TLogShow_DevEx.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =35;
begin
     with  ZetaCXGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;
end;

procedure TLogShow_DevEx.ZetaCXGridDBTableViewKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     if Key = VK_RETURN then
     begin
           TZetaClientDataSet(dsLog.Dataset).Modificar;
     end;
end;

procedure TLogShow_DevEx.ReconnectGrid;
begin
     inherited;
     ZGridModeTools.LimpiaCacheGrid( ZetaCXGridDBTableView );
end;

procedure TLogShow_DevEx.RefreshGrid;
begin
     inherited;
     ZGridModeTools.LimpiaCacheGrid( ZetaCXGridDBTableView );
end;

end.
