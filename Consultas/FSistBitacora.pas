unit FSistBitacora;

interface

{$INCLUDE DEFINES.INC}

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FBaseSistBitacora, DB, Grids, DBGrids, ZetaDBGrid, ComCtrls,
  StdCtrls, Mask, ZetaFecha, ZetaKeyLookup, Buttons, ZetaKeyCombo, ExtCtrls,
  ZetaCommonClasses; 

type
  TSistBitacora = class(TBaseSistBitacora)
    LblEmpleado: TLabel;
    Empleado: TZetaKeyLookup;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure SetFormaSuper(const lModoSuper: Boolean);
  protected
    procedure Connect; override;
    procedure Modificar; override;
    procedure RefrescaDatos; override;
    procedure LlenaClaseBit; override;
  public
    { Public declarations }

  end;

var
  SistBitacora: TSistBitacora;

implementation

{$R *.dfm}

uses DSistema,
     DCliente,
     DConsultas,
     {$IFDEF ADUANAS}
     ZHelpContext,
     {$endif}
     ZetaCommonLists,
     ZetaCommonTools;

procedure TSistBitacora.FormCreate(Sender: TObject);
begin
     inherited;
     {$IFDEF ADUANAS}
     HelpContext := H_BITACORA;
     {$ELSE}
     if dmCliente.ModoSuper then
        HelpContext := 60001
     else
         HelpContext := H60652_Bitacora_Clasica;
        //HelpContext := H60652_Bitacora; //old
     {$ENDIF}
     {$ifdef WORKFLOWCFG}
     HelpContext := H_WORKFLOWCFG_CONSULTAS_BITACORA;
     {$endif}

     {$ifdef SELECCION}
     Empleado.Visible := FALSE;
     LblEmpleado.Visible := FALSE;
     ZetaDBGrid.Columns[5].Visible := FALSE;
     {$endif}
     PosCol:= 6;

end;

procedure TSistBitacora.FormShow(Sender: TObject);
begin
     SetFormaSuper( dmCliente.ModoSuper );
     inherited;
end;

procedure TSistBitacora.Connect;
begin
     inherited;
     {$ifdef TRESS}
     {$ifndef RDD}
     dmCliente.cdsEmpleadoLookUp.Conectar;
     {$endif}
     {$endif}
     with dmConsultas do
     begin
          DataSource.DataSet := cdsBitacora;
     end;
end;

procedure TSistBitacora.RefrescaDatos;
begin
     with FParametros do
     begin
          AddInteger( 'Empleado', Empleado.Valor );
     end;
     dmConsultas.RefrescarBitacora( FParametros );
end;

procedure TSistBitacora.Modificar;
begin
     with dmConsultas do
     begin
          cdsBitacora.Modificar;
     end;
end;

procedure TSistBitacora.LlenaClaseBit;
var
   i: integer;
begin
     with ClaseBit do
     begin
          Sorted := FALSE;
          for i:= 0 to Ord(High(eClaseBitacora)) do
              Items.AddObject( ObtieneElemento(lfClaseBitacora,i), TObject( i ) );

          ItemIndex := 0;
          Sorted := TRUE;
     end;
end;

procedure TSistBitacora.SetFormaSuper( const lModoSuper: Boolean );
begin
     Usuario.Enabled := not lModoSuper;
     lbUsuario.Enabled := not lModoSuper;
     if lModoSuper then
     begin
          Usuario.Valor := dmCliente.Usuario;
          FNumProceso := -1;
          with ZetaDBGrid.Columns[ 3 ] do
          begin
               FieldName := 'BI_CLASE';
               Title.Caption := 'Clase';
               Width := 100;
          end;
     end;
end;
end.
