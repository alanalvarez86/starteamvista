inherited SistBitacora_DevEx: TSistBitacora_DevEx
  Left = 465
  Top = 249
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelFiltros: TPanel
    object LblEmpleado: TLabel [3]
      Left = 3
      Top = 102
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empleado:'
      FocusControl = Empleado
    end
    object Empleado: TZetaKeyLookup_DevEx [9]
      Left = 56
      Top = 103
      Width = 308
      Height = 21
      LookupDataset = dmCliente.cdsEmpleadoLookUp
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 5
      TabStop = True
      WidthLlave = 50
    end
    inherited Refrescar_DevEx: TcxButton
      TabOrder = 6
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CB_CODIGO: TcxGridDBColumn [5]
        Caption = 'Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
        MinWidth = 80
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
