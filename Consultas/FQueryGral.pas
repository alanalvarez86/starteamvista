unit FQueryGral;

interface

{  Folio      :
   Prop�sito  :
   Creaci�n   :
   Responsable:
   Calculados : No
   Lista Calculados[] }

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, Grids, ExtCtrls, Db, DBGrids,
     ZBaseConsulta,
     ZetaDBGrid;

type
  TQueryGral = class(TBaseConsulta)
    Panel1: TPanel;
    Memo: TMemo;
    AbrirQueryDlg: TOpenDialog;
    GuardarQueryDlg: TSaveDialog;
    Panel2: TPanel;
    EjecutaBtn: TBitBtn;
    NuevoSqlBtn: TBitBtn;
    AbrirSqlBtn: TBitBtn;
    GuardarSqlBtn: TBitBtn;
    DBGrid: TZetaDBGrid;
    AnteriorBtn: TSpeedButton;
    SiguienteBtn: TSpeedButton;
    Splitter1: TSplitter;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure GuardarSqlBtnClick(Sender: TObject);
    procedure AbrirSqlBtnClick(Sender: TObject);
    procedure NuevoSqlBtnClick(Sender: TObject);
    procedure AnteriorBtnClick(Sender: TObject);
    procedure SiguienteBtnClick(Sender: TObject);
    procedure EjecutaBtnClick(Sender: TObject);
  private
    { Private declarations }
    ListaSQLS: TStringList;
    ListaActiva: Integer;
    procedure AddSql;
    procedure ShowBotones;
    procedure AsignaAnterior;
    procedure AsignaSiguiente;
    procedure EjecutaQuery;
//    procedure ValidaNivel0(const sSQL, sNivel: String);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations}
  end;

var
  QueryGral: TQueryGral;

implementation

uses DConsultas,
     DCliente,
     {$ifdef ADUANAS}
     ZHelpContext,
     {$ENDIF}
     ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonClasses;

{$R *.DFM}

procedure TQueryGral.FormCreate(Sender: TObject);
begin
     inherited;
     ListaSQLS := TStringList.Create;
     {$ifdef ADUANAS}
     HelpContext := H_PESTANA_DE_CONSULTAS_EN_SQL;
     {$ELSE}
     HelpContext := H50054_SQL_Clasica;
     //HelpContext := H50054_SQL; //old
     {$ENDIF}
     {$ifdef WORKFLOWCFG}
     HelpContext := H_WORKFLOWCFG_CONSULTAS_SQL;
     {$endif}
end;

procedure TQueryGral.FormDestroy(Sender: TObject);
var
   i:Integer;
begin
     with ListaSQLS do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               TStringList( Objects[ i ] ).Free;
          end;
          Free;
     end;
     inherited;
end;

procedure TQueryGral.Connect;
begin
    DataSource.DataSet := dmConsultas.cdsQueryGeneral;
end;


procedure TQueryGral.Refresh;
var
   sSQL: String;
begin
     sSQL := UpperCase( Trim( Memo.Lines.Text ) );
     if ZetaCommonTools.StrLleno( sSQL ) then
     begin
          if ( Pos( 'SELECT', sSQL ) = 1 ) then
          begin
               //ValidaNivel0( sSQL, dmCliente.Confidencialidad );
               with dmConsultas do
               begin
                    SQLText := Memo.Lines.Text;
                    cdsQueryGeneral.Refrescar;
                    // DataSource.DataSet := cdsQueryGeneral;
                    Connect;
               end;
          end
          else
              ZetaDialogo.zError( Caption, 'Comando No Soportado', 0 );
     end;
end;

procedure TQueryGral.EjecutaBtnClick(Sender: TObject);
begin
     inherited;
     EjecutaQuery;
end;

procedure TQueryGral.EjecutaQuery;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           Refresh;
           AddSQL;
        except
              on Error: Exception do
              begin
                   ZetaDialogo.zExcepcion( Caption, 'Error Al Ejecutar Query', Error, 0 );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TQueryGral.GuardarSqlBtnClick(Sender: TObject);
begin
     inherited;
     with GuardarQueryDlg do
     begin
          Execute;
          if ZetaCommonTools.StrLleno( FileName ) then
          begin
               Memo.Lines.SaveToFile( FileName );
          end;
     end;
end;

procedure TQueryGral.AbrirSqlBtnClick(Sender: TObject);
begin
     inherited;
     with AbrirQueryDlg do
     begin
          Execute;
          if ZetaCommonTools.StrLleno( FileName ) then
          begin
               Memo.Clear;
               Memo.Lines.LoadFromFile( FileName );
          end;
     end;
end;

procedure TQueryGral.NuevoSqlBtnClick(Sender: TObject);
begin
     inherited;
     Memo.Clear;
     if DbGrid.DataSource.Dataset <> NIL then
        DbGrid.DataSource.DataSet.Close;
end;

procedure TQueryGral.AddSql;
var
   i: Integer;
   lExiste: Boolean;
   oLista: TStringList;
begin
     lExiste := False;
     for i := 0 to ListaSQLS.Count-1 do
     begin
          lExiste := ( Memo.Lines.Equals( TStrings( ListaSQLS.Objects[ i ] ) ) );
          if lExiste then
             Exit;
     end;
     if not lExiste then
     begin
          oLista := TStringList.Create;
          oLista.Assign( Memo.Lines );
          ListaSQLS.AddObject( '', oLista );
          ListaActiva := ListaSQLS.Count - 1;
          ShowBotones;
     end;
end;

procedure TQueryGral.ShowBotones;
begin
     if ( ListaSQLS.Count > 0 ) then
     begin
          AnteriorBtn.Enabled := not ( ListaActiva = 0 );
          SiguienteBtn.Enabled := not ( ListaActiva = ListaSQLS.Count - 1 );
     end;
end;

procedure TQueryGral.AnteriorBtnClick(Sender: TObject);
begin
     inherited;
     AsignaAnterior;
end;

procedure TQueryGral.SiguienteBtnClick(Sender: TObject);
begin
     inherited;
     AsignaSiguiente;
end;

procedure TQueryGral.AsignaAnterior;
var
   i: Integer;
begin
     i := ListaActiva - 1;
     if ( i >= 0 ) then
     begin
          with Memo.Lines do
          begin
               BeginUpdate;
               Assign( TStrings( ListaSQLS.Objects[ i ] ) );
               EndUpdate;
          end;
          ListaActiva := i;
          ShowBotones;
     end;
end;

procedure TQueryGral.AsignaSiguiente;
var
   i: Integer;
begin
     i := ListaActiva + 1;
     if ( i <= ListaSQLS.Count - 1 ) then
     begin
          with Memo.Lines do
          begin
               BeginUpdate;
               Assign( TStrings( ListaSQLS.Objects[ i ] ) );
               EndUpdate;
          end;
          ListaActiva := i;
          ShowBotones;
     end;
end;

{procedure TQueryGral.ValidaNivel0( const sSQL, sNivel : String );

    function TablasEmpleado : Boolean;
    const
        K_MAX_TABLAS = 45;
        aTablas : array[ 0..K_MAX_TABLAS ] of Pchar =
            ( 'COLABORA','REP_PTU','REP_AHO','AGUINAL','KARDEX','TMPHORAS','TMPEVENT',
              'VACACION','ANTESCUR','ANTESPTO','PARIENTE','KAR_FIJA','NOMINA',
              'MOVIMIEN','LIQ_EMP','LIQ_MOV','PERMISO','FALTAS','PRESTAMO','KARCURSO',
              'INCAPACI','AUSENCIA','CHECADAS','AHORRO','ACAR_ABO','ACUMULA','PCAR_ABO',
              'IMAGEN','BITACORA','TMPBALAN','TMPESTAD','TMPSALAR','TMPMOVS','COMPARA',
              'DECLARA','TMPROTAI','INVITA','CAF_COME','ASIGNA','TMPCALEN','TMPLISTA',
              'TMPFOLIO','TMPNOM','TMPCALHR','ALTABAJA','CUR_PROG');
    var
        i : Integer;
    begin
        Result := FALSE;
        i := 0;
        while ( not Result ) and ( i < K_MAX_TABLAS ) do
        begin
            Result := Pos( aTablas[ i ], sSQL ) > 0;
            Inc( i );
        end;
    end;

    procedure RevisaFiltro;
    var
        sFiltro : String;
    begin
        sFiltro := Format( 'COLABORA.CB_NIVEL0=''%s''', [ sNivel ] );
        //if Pos( sFiltro, sSQL ) = 0 then
           // DataBaseError( 'Falta Incluir Filtro: ' + sFiltro );
    end;

begin  // ValidaNivel0
    //sSQL := UpperCase( sSQL );
    if strLleno( sNivel ) and TablasEmpleado then
       RevisaFiltro;
end;
 }
end.
