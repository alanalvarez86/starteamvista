unit FConteo_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, TeEngine, Series, TeeProcs,
  Chart, DBChart, ComCtrls, DBClient, ZetaClientDataSet, ImgList,
  {$ifndef VER130}Variants,{$endif}
  ZetaDBTextBox, StdCtrls, ZetaDBGrid, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  ZetaCXGrid, Menus, ActnList, cxPCdxBarPopupMenu, cxPC, dxBarBuiltInMenu,
  VclTee.TeeGDIPlus, System.Actions;

type
  TConteo_DevEx = class(TBaseConsulta)
    ImageList: TImageList;
    PanelInferior: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    ZetaDBTextBox1: TZetaDBTextBox;
    Label4: TLabel;
    ZetaDBTextBox3: TZetaDBTextBox;
    Label3: TLabel;
    ZetaDBTextBox2: TZetaDBTextBox;
    dsTotales: TDataSource;
    Label6: TLabel;
    ZetaDBTextBox5: TZetaDBTextBox;
    cxImage16: TcxImageList;
    ActionList: TActionList;
    _A_Imprimir: TAction;
    _E_Agregar: TAction;
    _E_Borrar: TAction;
    _E_Modificar: TAction;
    PopupMenu1: TPopupMenu;
    mAgregar: TMenuItem;
    mBorrar: TMenuItem;
    mModificar: TMenuItem;
    mImprimir: TMenuItem;
    PageControl_DevEx: TcxPageControl;
    tsGrid_DevEx: TcxTabSheet;
    tsGrafica_DevEx: TcxTabSheet;
    Panel1: TPanel;
    ZetaDBGrid: TZetaCXGrid;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    CT_NIVEL_1: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    CT_CUANTOS: TcxGridDBColumn;
    CT_REAL: TcxGridDBColumn;
    CT_TASA: TcxGridDBColumn;
    CT_VACANTES: TcxGridDBColumn;
    CT_EXCEDENTE: TcxGridDBColumn;
    ZetaDBGridLevel: TcxGridLevel;
    Panel2: TPanel;
    DBChart3: TDBChart;
    Series3: TBarSeries;
    Series4: TBarSeries;
    CT_DIFERENCIA: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ZetaDBGridDBTableViewDblClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewDataControllerFilterGetValueList( Sender: TcxFilterCriteria; AItemIndex: Integer; AValueList: TcxDataFilterValueList);
    procedure ZetaDBGridDBTableViewCustomDrawCell( Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure DataSourceUpdateData(Sender: TObject);
    procedure _A_ImprimirExecute(Sender: TObject);
    procedure _E_AgregarExecute(Sender: TObject);
    procedure _E_BorrarExecute(Sender: TObject);
    procedure _E_ModificarExecute(Sender: TObject);
    procedure ZetaDBGridDBTableViewKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ZetaDBGridDBTableViewDataControllerSummaryAfterSummary(
      ASender: TcxDataSummary);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    procedure ShowGrafica;
    procedure SetTitulos;
    procedure ApplyMinWidth;    //DevEx(by am): Metodo agregado para manejar el ancho minimo de las columnas
  protected
    function ValoresGrid: Variant;override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    function PuedeImprimir( var sMensaje: String ): Boolean; override;
    procedure Imprimir;override;
    procedure Connect; override;
    procedure Agregar; override;
    procedure Refresh; override;
    procedure Modificar; override;
    procedure ReconnectGrid; override;
    procedure RefreshGrid; override;
  public
    { Public declarations }
  end;

var
  Conteo_DevEx: TConteo_DevEx;

implementation
uses
    ZetaCommonLists,
    ZetaCommonClasses,
    ZAccesosTress,
    ZGlobalTress,
    ZetaDialogo,
    ZGridModeTools,
    DGlobal,
    DConteo;

{$R *.DFM}

procedure TConteo_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     DBChart3.AutoRefresh := FALSE;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= True;
     ZetaDBGridDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
end;

procedure TConteo_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     HelpContext:= H50055_Presupuesto_de_personal;
     SetTitulos;
     PageControl_DevEx.ActivePage := tsGrid_DevEx;

     //Se aplica el MinWidth a las columnas y se crea la sumatoria
     ApplyMinWidth;
     ZGridModeTools.CreaColumnaSumatoria( ZetadbGriddbtableview.Columns[0], 0, 'Total:', SkCount );
     //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
     //Esconde la caja de agrupamiento
     ZetaDBGridDBTableView.OptionsView.GroupByBox := False;
     //Para que nunca muestre el filterbox inferior
     ZetaDBGridDBTableView.FilterBox.Visible := fvNever;
     //Para que no aparezca el Custom Dialog
     ZetaDBGridDBTableView.FilterBox.CustomizeDialog := False;
     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     ZetaDBGridDBTableView.ApplyBestFit();

     //Esta columna se agrego para leer el valor del cual depende el color del font para cada renglon del grid.
     CT_DIFERENCIA.Visible := FALSE;
end;

procedure TConteo_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     ZGridModeTools.LimpiaCacheGrid( ZetaDBGridDBTableView );
end;

procedure TConteo_DevEx.ReconnectGrid;
begin
     inherited;
     ZGridModeTools.LimpiaCacheGrid( ZetaDBGridDBTableView );
end;

procedure TConteo_DevEx.RefreshGrid;
begin
     inherited;
     ZGridModeTools.LimpiaCacheGrid( ZetaDBGridDBTableView );
end;

procedure TConteo_DevEx.SetTitulos;
begin
     Panel1.Caption := 'Personal por ' + dmConteo.GetDescripConteoNiveles(0);
     Panel2.Caption := Panel1.Caption;
end;

procedure TConteo_DevEx.ShowGrafica;
begin
     // Gr�fica Presupuesto vs Real
        Series3.DataSource := dmConteo.DataSetTotales(1);
        Series3.XLabelsSource := dmConteo.cdsConteo.Fields[0].FieldName;
        Series3.YValues.ValueSource := 'CT_CUANTOS';

        Series4.DataSource := dmConteo.DataSetTotales(1);
        Series4.XLabelsSource := dmConteo.cdsConteo.Fields[0].FieldName;
        Series4.YValues.ValueSource := 'CT_REAL';
        DBChart3.RefreshData;
end;

procedure TConteo_DevEx.Connect;

   procedure ConectarConteo;
   begin
        with dmConteo do
        begin
            PreparaConteo;
            cdsConteo.Conectar;
            DataSource.DataSet := DataSetTotales( 1 );
            dsTotales.DataSet := DataSetTotales( 0 );
            ShowGrafica;
        end;
   end;

   procedure ShowConfiguracion;
   begin
        with dmConteo do
             if GetGlobalConteo then
                ConectarConteo;
   end;

begin
     if (dmConteo.NumConteoNiveles= 0) then ShowConfiguracion;

     ConectarConteo;

     with dmConteo do
          if cdsConteo.IsEmpty then ShowConfiguracion;
end;

procedure TConteo_DevEx.Refresh;
begin
     dmConteo.cdsConteo.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TConteo_DevEx.Agregar;
begin
     dmConteo.cdsConteo.Modificar;
     DBChart3.RefreshData;
end;

procedure TConteo_DevEx.Modificar;
begin
     dmConteo.cdsConteo.Modificar;
     DBChart3.RefreshData;
     SetTitulos;
end;

function TConteo_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := TRUE;
end;

function TConteo_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     //Result := TRUE;  //PENDIENTE: NO HAY METODO BORRAR, SERIA MEJOR PONER FALSE Y DESPLEGAR UN MENSAJE.
     //DevEx(by am): Se implemento el mensaje sugerido en la linea anterior para la nueva imagen
     Result := FALSE;
     sMensaje := 'No se permite borrar en '+ Self.Caption + '.';
end;

function TConteo_DevEx.PuedeImprimir(var sMensaje: String): Boolean;
begin
     Result := TRUE;
end;

function TConteo_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := TRUE;
end;

procedure TConteo_DevEx.Imprimir;
 var iAncho: integer;
begin
     if PageControl_DevEx.ActivePage = tsGrid_DevEx then
     begin
          with ZetaDBGridDBTableView.DataController.DataSet.FieldByName('TB_ELEMENT') do
          begin
               iAncho := DisplayWidth;
               DisplayWidth := 30;

               Inherited Imprimir;

               DisplayWidth := iAncho;
          end
     end
     else if zConfirm( 'Imprimir...', '� Desea Imprimir La Pantalla ' + Caption + ' ?', 0, mbYes ) then
          Print;
end;

function TConteo_DevEx.ValoresGrid: Variant;
begin
     Result := VarArrayOf( [ 'Totales de Empresa',
                             '',
                             stTodos,
                             stNinguno ] );
end;

procedure TConteo_DevEx.ZetaDBGridDBTableViewDblClick(Sender: TObject);
begin
     inherited;
     DoEdit;
end;

procedure TConteo_DevEx.ZetaDBGridDBTableViewDataControllerFilterGetValueList(Sender: TcxFilterCriteria; AItemIndex: Integer;
          AValueList: TcxDataFilterValueList);
begin
     inherited;
     ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if ZetaDBGridDBTableView.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( ZetaDBGridDBTableView, AItemIndex, AValueList );
end;

procedure TConteo_DevEx.ZetaDBGridDBTableViewDataControllerSummaryAfterSummary(ASender: TcxDataSummary);
begin
     inherited;
     if ZetaDBGridDBTableView.DataController.IsGridMode and ZetaDbGridDBTableView.OptionsView.Footer then      // Hay columnas totalizadas
        ZGridModeTools.AsignaValorColumnaSummary( ASender );
end;

procedure TConteo_DevEx.ZetaDBGridDBTableViewCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
          AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   sValor:String;
begin
     inherited;
     sValor := AViewInfo.GridRecord.DisplayTexts [CT_DIFERENCIA.Index];
     with ACanvas do
     begin
          with Font do
          begin
               if StrToFloat(sValor) < 0 then
                  Color := clRed
               else if StrToFloat(sValor) > 0 then
                  Color := clGreen
               else
                  Color := clBlack;
          end;
     end;
end;

procedure TConteo_DevEx.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =40;
begin
     with  ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;
end;

procedure TConteo_DevEx.DataSourceUpdateData(Sender: TObject);
begin
     inherited;
     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TConteo_DevEx._A_ImprimirExecute(Sender: TObject);
begin
     inherited;
     DoPrint;
end;

procedure TConteo_DevEx._E_AgregarExecute(Sender: TObject);
begin
     inherited;
     DoInsert;
end;

procedure TConteo_DevEx._E_BorrarExecute(Sender: TObject);
begin
     inherited;
     DoDelete;
end;

procedure TConteo_DevEx._E_ModificarExecute(Sender: TObject);
begin
     inherited;
     DoEdit;
end;

procedure TConteo_DevEx.ZetaDBGridDBTableViewKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     inherited;
     if Key = VK_RETURN then
     begin
          DoEdit;
     end;
end;

end.
