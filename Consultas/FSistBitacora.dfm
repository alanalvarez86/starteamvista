inherited SistBitacora: TSistBitacora
  Left = 384
  Top = 260
  ClientWidth = 741
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 741
    inherited ValorActivo2: TPanel
      Width = 399
    end
  end
  inherited PanelFiltros: TPanel
    Width = 741
    object LblEmpleado: TLabel [3]
      Left = 3
      Top = 102
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = '&Empleado:'
      FocusControl = Empleado
    end
    object Empleado: TZetaKeyLookup
      Left = 56
      Top = 98
      Width = 308
      Height = 21
      LookupDataset = dmCliente.cdsEmpleadoLookUp
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 6
      TabStop = True
      WidthLlave = 50
    end
  end
  inherited BitacoraPG: TPageControl
    Width = 741
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 741
    Columns = <
      item
        Expanded = False
        FieldName = 'BI_FECHA'
        Title.Caption = 'Fecha'
        Width = 73
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BI_HORA'
        Title.Caption = 'Hora'
        Width = 47
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BI_TIPO'
        Title.Caption = 'Tipo'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_CODIGO'
        Title.Caption = 'Usuario'
        Width = 44
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BI_TEXTO'
        Title.Caption = 'Descripci'#243'n'
        Width = 272
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Caption = 'Empleado'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BI_FEC_MOV'
        Title.Caption = 'Fecha Mov.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BI_NUMERO'
        Title.Caption = 'Proceso'
        Visible = True
      end>
  end
end
