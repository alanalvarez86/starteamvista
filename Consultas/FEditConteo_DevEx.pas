unit FEditConteo_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, Buttons, ExtCtrls, ComCtrls, Series, TeEngine,
  TeeProcs, Chart, DBChart, ZetaDBTextBox, Grids, DBGrids, ZBaseEdicion_DevEx,
  {$ifndef VER130}Variants,{$endif}
  ZetaSmartLists, Db, DBCtrls, ImgList, DbClient, ZetaDBGrid, ZetaMessages,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, cxNavigator, cxDBNavigator, cxButtons, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxDBData, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid, ActnList,
  cxPCdxBarPopupMenu, cxPC, cxContainer, cxTreeView, dxBarBuiltInMenu,
  VclTee.TeeGDIPlus, System.Actions;

type
  TEditConteo_DevEx = class(TBaseEdicion_DevEx)
    PanelArbol: TPanel;
    Splitter1: TSplitter;
    ImageList: TImageList;
    dsDetalle: TDataSource;
    dsTotales: TDataSource;
    Panel1: TPanel;
    PanelInferior: TPanel;
    lbTotales: TLabel;
    Label2: TLabel;
    CT_CUANTOS: TZetaDBTextBox;
    Label4: TLabel;
    CT_REAL: TZetaDBTextBox;
    Label3: TLabel;
    CT_EXCEDENTE: TZetaDBTextBox;
    Label6: TLabel;
    CT_VACANTES: TZetaDBTextBox;
    lbNiveles: TLabel;
    dxBarButton_RefrescarBtn: TdxBarButton;
    dxBarButton_BtnConfigurar: TdxBarButton;
    dxBarButton_UpBtn: TdxBarButton;
    PopupMenu1: TPopupMenu;
    mAgregar: TMenuItem;
    mBorrar: TMenuItem;
    mModificar: TMenuItem;
    mImprimir: TMenuItem;
    ActionList: TActionList;
    _A_Imprimir: TAction;
    _E_Agregar: TAction;
    _E_Borrar: TAction;
    _E_Modificar: TAction;
    cxImage16: TcxImageList;
    PageDetalle_DevEx: TcxPageControl;
    Detalle_DevEx: TcxTabSheet;
    Compara_DevEx: TcxTabSheet;
    ZetaDBGrid: TZetaCXGrid;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    CT_NIVEL_1: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    CT_CUANTOS_GRID: TcxGridDBColumn;
    CT_REAL_GRID: TcxGridDBColumn;
    CT_TASA: TcxGridDBColumn;
    CT_VACANTES_GRID: TcxGridDBColumn;
    CT_EXCEDENTE_GRID: TcxGridDBColumn;
    ZetaDBGridLevel: TcxGridLevel;
    Real_DevEx: TcxTabSheet;
    DBChart1: TDBChart;
    Series3: TBarSeries;
    Series4: TBarSeries;
    LblNoGraficar: TLabel;
    DBChart3: TDBChart;
    Series2: TPieSeries;
    CT_DIFERENCIA: TcxGridDBColumn;
    Arbol_DevEx: TcxTreeView;
    ImageList_DevEx: TcxImageList;
    procedure FormCreate(Sender: TObject);
    procedure ArbolChange(Sender: TObject; Node: TTreeNode);
    procedure DataSourceStateChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    //procedure PageDetalleChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure OK_DevExClick(Sender: TObject);
    procedure dxBarButton_ModificarBtnClick(Sender: TObject);
    procedure dxBarButton_RefrescarBtnClick(Sender: TObject);
    procedure dxBarButton_BtnConfigurarClick(Sender: TObject);
    procedure dxBarButton_UpBtnClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure ZetaDBGridDBTableViewDblClick(Sender: TObject);
    procedure _A_ImprimirExecute(Sender: TObject);
    procedure _E_AgregarExecute(Sender: TObject);
    procedure _E_BorrarExecute(Sender: TObject);
    procedure _E_ModificarExecute(Sender: TObject);
    procedure ZetaDBGridDBTableViewCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure PageDetalle_DevExChange(Sender: TObject);
    procedure Arbol_DevExChange(Sender: TObject; Node: TTreeNode);
    procedure ZetaDBGridDBTableViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    //efrescarGraficas : Array[1..2] of Boolean;
    FChangeCount:Integer;
    FTitulo : string;
    procedure Refrescar;
    //procedure ConstruyeArbol( const lRefresca: Boolean = TRUE );
    procedure ConstruyeArbol_DevEx( const lRefresca: Boolean = TRUE );
    function GetFiltroGrupos(Nodo: TTreeNode): String;
    procedure SetBotonesEdicion;
    procedure PosicionaArbol( Llaves: array of string; const lParcialKey: Boolean = FALSE );
    //ocedure SetRefrescarGraficas(const lEnabled: Boolean);
    procedure RefrescaGrafica(iPos: Integer);
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    procedure InvocarModificacion;
    //DevEx (by am): Agregado para aplicar el Min width a las columnas de los nuevos grids.
    procedure ApplyMinWidth;
    //DevEx (by am): Metodo agregado para mostrar la sumatoria de renglones
    Procedure CreaColumaSumatoria(Columna:TcxGridDBColumn; TipoFormato: Integer ; TextoSumatoria: String ; FooterKind : tcxSummaryKind );
  protected
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    function BotonesActivos: Boolean; override;
    procedure EscribirCambios;override;
    procedure DoCancelChanges; override;
    procedure Imprimir;override;
    procedure Connect; override;
    procedure Disconnect;override;
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    procedure HabilitaControles; override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
  public
    { Public declarations }
    property ConteoCambios:Integer  read FChangeCount write FChangeCount;
  end;

const
    OFF_SET = 1;

var
  EditConteo_DevEx: TEditConteo_DevEx;

implementation

uses dConteo, FEditPresup_DevEx, FGlobalConteo_DevEx, ZetaCommonClasses, ZetaCommonTools, ZGlobalTress,
     ZetaDialogo, ZAccesosMgr, FWizConteo_DevEx, ZAccesosTress, FAutoClasses, ZetaCommonLists;

{$R *.DFM}

procedure TEditConteo_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CONS_CONTEO_PERSONAL;
     //SetRefrescarGraficas( FALSE );

     DBChart1.Title.Text.Text := '';
     DBChart3.Title.Text.Text := '';

     HelpContext := H50551_Detalle_del_presupuesto;
end;

procedure TEditConteo_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PageDetalle_DevEx.ActivePage := Detalle_DevEx;
     //GridDetalle.ReadOnly :=  not CheckDerecho( D_CONS_CONTEO_PERSONAL, K_DERECHO_CAMBIO );

     {***Configuracion nuevo Grid***}
     ApplyMinWidth;
     CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
     //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
     //Esconde la caja de agrupamiento
     ZetaDBGridDBTableView.OptionsView.GroupByBox := False;
     //Para remover las opcioens de filtrado
     ZetaDBGridDBTableView.OptionsCustomize.ColumnFiltering := False;
     //Para que nunca muestre el filterbox inferior
     ZetaDBGridDBTableView.FilterBox.Visible := fvNever;
     //Para que no aparezca el Custom Dialog
     ZetaDBGridDBTableView.FilterBox.CustomizeDialog := False;
     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     ZetaDBGridDBTableView.ApplyBestFit();

     //Esta columna se agrego para leer el valor del cual depende el color del font para cada renglon del grid.
     CT_DIFERENCIA.Visible := FALSE;
end;

procedure TEditConteo_DevEx.Connect;
begin
     with dmConteo do
     begin
          cdsConteo.Conectar;
          DataSource.DataSet:= cdsConteo;
          //ConstruyeArbol;
          ConstruyeArbol_DevEx;
     end;
end;

procedure TEditConteo_DevEx.Disconnect;
begin
     inherited;
     dmConteo.QuitaFiltros;
end;

procedure TEditConteo_DevEx.Refrescar;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             with dmConteo do
             begin
                  cdsConteo.Refrescar;
                  DataSource.DataSet:= cdsConteo;
             end;
             //ConstruyeArbol;
             ConstruyeArbol_DevEx;
          finally
             Cursor := oCursor;
          end;
     end;
     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TEditConteo_DevEx.Agregar;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     with dmConteo.cdsConteo do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
     end;

     with TWizConteo_DevEx.Create( Application ) do
     begin
          try
             NivelActual := Arbol_DevEx.Selected.Level + 1;
             if ( NivelActual > 0 ) then
                ShowModal;
             Application.ProcessMessages;
             if Wizard.Ejecutado then
             begin
                  dmConteo.RecalculaTotales;
                  ConstruyeArbol_DevEx( FALSE );
                  PosicionaArbol( [ Llave1, Llave2, Llave3, Llave4 ] );
                  DataSourceStateChange( self );
             end;
          finally
                 Free;
                 Screen.Cursor := oCursor;
          end;
     end;
     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TEditConteo_DevEx.Modificar;
begin
     with dmConteo do
     begin
          with cdsConteo do
               if State in [ dsEdit, dsInsert ] then
                  Post;
{
          RefreshTotales := FALSE;
}
     end;

     ZBaseEdicion_DevEx.ShowFormaEdicion( EditPresup_DevEx, TEditPresup_DevEx );

{     with dmConteo do
          if RefreshTotales then
          begin
               RecalculaTotales;
               ArbolChange( Arbol, Arbol.Selected );   // Filtrar el Detalle
               DataSourceStateChange( self );
          end;
}
     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TEditConteo_DevEx.Borrar;
var
   aPosicion : Array[0..3] of string;
   i: integer;
   sFiltro, sNivel : string;
   oCursor: TCursor;
begin
     if dsDetalle.DataSet.FieldByName('CT_REAL').AsInteger > 0 then
        ZInformation( Caption, Format('El Criterio de %s no se Puede Borrar porque Tiene Empleados Activos',[Detalle_DevEx.Caption]), 0 )
     else if ZConfirm( self.Caption, '� Desea Borrar Este Registro ?', 0, mbOk ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             with dmConteo do
             begin
                  cdsConteo.DisableControls;
                  for i:=0 to NumNiveles do
                  begin
                      sNivel := DataSetTotales(Arbol_DevEx.Selected.Level+1).Fields[i].AsString;
                      if (sNivel <> '') and (i<K_MAX_CONTEO_NIVELES) then
                         sFiltro := sFiltro + ' CT_NIVEL_'+ IntToStr(i+1) + '=' +
                                    EntreComillas(sNivel) + ' AND ' ;
                      if i < Arbol_DevEx.Selected.Level then
                         aPosicion[i] := sNivel;
                  end;

                  sFiltro := Copy( sFiltro, 1, Length(sFiltro)-4 );

                  with cdsConteo do
                  begin
                       Filter := sFiltro;
                       Filtered := TRUE;

                       while NOT EOF do
                             Delete;

                       Filtered := FALSE;
                  end;

                  if cdsConteo.ChangeCount > 0 then
                  begin
                       DataSourceStateChange( self );
                       RecalculaTotales;
                       ConstruyeArbol_DevEx( FALSE );
                       PosicionaArbol( aPosicion, TRUE );
                  end;
                  cdsConteo.EnableControls;
             end;
          finally
                 Screen.Cursor:= oCursor;
          end;
     end;
     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     ZetaDBGridDBTableView.ApplyBestFit();
end;

function TEditConteo_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := ( dsDetalle.DataSet.Name = dmConteo.cdsConteo.Name );
          if not Result then
             sMensaje:= Format( 'No se puede Modificar %s', [ Detalle_DevEx.Caption ] );
     end;
end;

{ Metodos del Arbol }
procedure TEditConteo_DevEx.ConstruyeArbol_DevEx( const lRefresca: Boolean = TRUE );
const
     K_IMG_EMPRESA=0;
     //Naranja
     K_IMG_NIVEL1= 1;
     K_IMG_NIVEL1_SELEC= 2;
     //Azul
     K_IMG_NIVEL2= 3;
     K_IMG_NIVEL2_SELEC=4;
var
   oPadre, oHijo : TTreeNode;
   sActual, sOldIndex : String;
   aGrupos : Array of String;
   nCorte, nNivel : Integer;
   cdsGrupos : TClientDataSet;

   procedure InicializaArbol;
   var
       i : Integer;
   begin
        if dmConteo.NumNiveles > 1 then
        begin
             aGrupos := VarArrayCreate( [ 0, dmConteo.NumNiveles-2 ], varOleStr );
             for i := 0 to dmConteo.NumNiveles-2 do
                 aGrupos[ i ] := '';
        end;
             Arbol_DevEx.Items.BeginUpdate;
             Arbol_DevEx.Items.Clear;
             oPadre := Arbol_DevEx.Items.AddChild( NIL, 'Empresa' );

             with oPadre do
             begin
                 ImageIndex := K_IMG_EMPRESA;
                 SelectedIndex := K_IMG_EMPRESA;
                 Data := Pointer( 0 );
             end;
   end;

   function GrupoCorte : Integer;
   var
      i : Integer;
   begin
       Result := -1;
       for i := 0 to dmConteo.NumNiveles-2 do
       begin
           if ( cdsGrupos.Fields[ i ].AsString <> aGrupos[ i ] ) then
           begin
               Result := i;
               Exit;
           end;
       end;
   end;
begin
      InicializaArbol;
     if dmConteo.NumNiveles > 1 then
     begin
          cdsGrupos := dmConteo.cdsConteo;
          with cdsGrupos do
          begin
               DisableControls;
               sOldIndex := IndexFieldNames;
               IndexFieldNames := 'CT_NIVEL_1;CT_NIVEL_2;CT_NIVEL_3;CT_NIVEL_4;CT_NIVEL_5';
               try
                  First;
                  while not Eof do
                  begin
                       nCorte := GrupoCorte;
                       if ( nCorte >= 0 ) then
                       begin
                            while oPadre.Level > nCorte do
                                oPadre := oPadre.Parent;

                            for nNivel := nCorte to dmConteo.NumNiveles-2 do
                            begin
                                 sActual := Fields[ nNivel*Off_SET ].AsString;
                                 aGrupos[ nNivel ] := sActual ;

                                 oHijo := Arbol_DevEx.Items.AddChild( oPadre, sActual + ' = '+
                                                                FieldByName( 'TB_ELEMENT'+IntToStr(nNivel*Off_SET+1) ).AsString );
                                 with oHijo do
                                 begin
                                      if Level = 1 then
                                      begin
                                           ImageIndex := K_IMG_NIVEL1;
                                           SelectedIndex := K_IMG_NIVEL1_SELEC;
                                      end
                                      else
                                      begin
                                           {Las imagenes de colores empiezan en el Index 21}
                                           ImageIndex := K_IMG_NIVEL2 + ((Level - 2) *2);
                                           SelectedIndex := K_IMG_NIVEL2_SELEC + ((Level - 2) *2);
                                      end;
                                      Data := Pointer( RecNo );
                                 end;
                                 oPadre := oHijo;
                            end;
                       end;
                       Next;
                  end;
               finally
                  IndexFieldNames := sOldIndex;
                  EnableControls;
               end;
          end;
     end;
     with Arbol_DevEx do
     begin
          if lRefresca then
          begin
               Items[ 0 ].Expand( FALSE );
               Selected := Items[ 0 ];
          end;
          Items.EndUpdate;
          // ArbolChange( self, Selected );  No se necesita. Selected lo dispara.
     end;
end;
{procedure TEditConteo_DevEx.ConstruyeArbol( const lRefresca: Boolean = TRUE );
var
   oPadre, oHijo : TTreeNode;
   sActual, sOldIndex : String;
   aGrupos : Array of String;
   nCorte, nNivel : Integer;
   cdsGrupos : TClientDataSet;

   procedure InicializaArbol;
   var
       i : Integer;
   begin
        if dmConteo.NumNiveles > 1 then
        begin
             aGrupos := VarArrayCreate( [ 0, dmConteo.NumNiveles-2 ], varOleStr );
             for i := 0 to dmConteo.NumNiveles-2 do
                 aGrupos[ i ] := '';
        end;
             Arbol.Items.BeginUpdate;
             Arbol.Items.Clear;
             oPadre := Arbol.Items.AddChild( NIL, 'Empresa' );

             with oPadre do
             begin
                 ImageIndex := 19;
                 SelectedIndex := 19;
                 Data := Pointer( 0 );
             end;
   end;

   function GrupoCorte : Integer;
   var
      i : Integer;
   begin
       Result := -1;
       for i := 0 to dmConteo.NumNiveles-2 do
       begin
           if ( cdsGrupos.Fields[ i ].AsString <> aGrupos[ i ] ) then
           begin
               Result := i;
               Exit;
           end;
       end;
   end;

begin   // ConstruyeArbol
     InicializaArbol;
     if dmConteo.NumNiveles > 1 then
     begin
          cdsGrupos := dmConteo.cdsConteo;
          with cdsGrupos do
          begin
               DisableControls;
               sOldIndex := IndexFieldNames;
               IndexFieldNames := 'CT_NIVEL_1;CT_NIVEL_2;CT_NIVEL_3;CT_NIVEL_4;CT_NIVEL_5';
               try
                  First;
                  while not Eof do
                  begin
                       nCorte := GrupoCorte;
                       if ( nCorte >= 0 ) then
                       begin
                            while oPadre.Level > nCorte do
                                oPadre := oPadre.Parent;

                            for nNivel := nCorte to dmConteo.NumNiveles-2 do
                            begin
                                 sActual := Fields[ nNivel*Off_SET ].AsString;
                                 aGrupos[ nNivel ] := sActual ;

                                 oHijo := Arbol.Items.AddChild( oPadre, sActual + ' = '+
                                                                FieldByName( 'TB_ELEMENT'+IntToStr(nNivel*Off_SET+1) ).AsString );
                                 with oHijo do
                                 begin
                                      if Level = 1 then
                                      begin
                                           ImageIndex := 14;
                                           SelectedIndex := 15;
                                      end
                                      else
                                      begin
                                           //Las imagenes de colores empiezan en el Index 21
                                           ImageIndex := 21 + ((Level - 2) *2);
                                           SelectedIndex := 22 + ((Level - 2) *2);
                                      end;
                                      Data := Pointer( RecNo );
                                 end;
                                 oPadre := oHijo;
                            end;
                       end;
                       Next;
                  end;
               finally
                  IndexFieldNames := sOldIndex;
                  EnableControls;
               end;
          end;
     end;
     with Arbol do
     begin
          if lRefresca then
          begin
               Items[ 0 ].Expand( FALSE );
               Selected := Items[ 0 ];
          end;
          Items.EndUpdate;
          // ArbolChange( self, Selected );  No se necesita. Selected lo dispara.
     end;
end;}

procedure TEditConteo_DevEx.ArbolChange(Sender: TObject; Node: TTreeNode);
var
    oTotales, oDataSet : TDataset;
    sCodigo  : String;

    procedure SetTitulos;
    var
       oNode: TTreeNode;
       sNivel, sField : string;
       i: integer;
    begin
         oNode := Node;
         lbNiveles.Caption := '';
         lbTotales.Caption := '';
         sNivel := '';
         FTitulo :='';

         if oNode.Level = 0 then
         begin
              FTitulo := 'Totales de Empresa';
              lbNiveles.Caption := FTitulo;
              lbTotales.Caption := '';
         end
         else if oNode.Level = 1 then
         begin
              FTitulo := 'Totales de ' + dmConteo.GetDescripConteoNiveles( oNode.Level-1 ) + ': '+
                         dmConteo.cdsConteo.FieldByName('CT_NIVEL_1').AsString;
              lbNiveles.Caption := FTitulo;
              lbTotales.Caption := '';
         end
         else
         begin
              with dmConteo, cdsConteo do
              begin
                   for i:= 1 to oNode.Level - 1 do
                   begin
                        sNivel := GetDescripConteoNiveles( i-1) + ': ';
                        sField := FieldByName('CT_NIVEL_'+ IntToStr(i)).AsString;
                        lbNiveles.Caption := lbNiveles.Caption + ' -- ' + sNivel + sField;
                        FTitulo := FTitulo + sNivel + sField  +'='+ FieldByName('TB_ELEMENT'+ IntToStr(i)).AsString + CR_LF;
                   end;
                   sNivel := dmConteo.GetDescripConteoNiveles( oNode.Level -1 ) + ': ';
                   lbNiveles.Caption := lbNiveles.Caption + ' -- ';
                   sField := FieldByName('CT_NIVEL_'+ IntToStr(oNode.Level) ).AsString;
                   lbTotales.Caption := 'Totales de ' + sNivel + sField;
                   FTitulo := FTitulo + sNivel + sField + '='+ FieldByName('TB_ELEMENT'+ IntToStr(oNode.Level)).AsString;
              end;
         end;
    end;

var
   //i : integer;  //old
   oCursor : TCursor;
   oParentNodo : TTreeNode;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if Node <> NIL then
        begin
            // Filtra los padres hacia arriba del nodo para recalculo de Totales
            oParentNodo := Node.Parent;
            while ( oParentNodo <> nil ) and ( oParentNodo.Level > 0 ) do
            begin
                 with dmConteo.DataSetTotales( oParentNodo.Level ) do
                 begin
                      Filter := GetFiltroGrupos( oParentNodo );
                      Filtered := TRUE;
                 end;
                 oParentNodo := oParentNodo.Parent;
            end;
            // Filtra Dataset Detalle
            oDataSet := dmConteo.DataSetTotales( Node.Level+1 );
            oDataSet.Filtered := FALSE;
            oDataSet.Filter := GetFiltroGrupos( Node );
            oDataSet.Filtered := TRUE;
            dsDetalle.DataSet := oDataSet;
            with dmConteo do
            begin
                 Detalle_DevEx.Caption := GetDescripConteoNiveles( Node.Level );
            end;
            // DataSet de Totales
            oTotales := dmConteo.DataSetTotales( Node.Level );
            oTotales.Filter := oDataSet.Filter;
            oTotales.Filtered := TRUE;
            dsTotales.DataSet := oTotales;

            // Columna de Grid
            sCodigo := oDataSet.Fields[ Node.Level ].FieldName;
            with ZetaDBGridDBTableView do
            begin
                 Columns[ 0 ].DataBinding.FieldName := sCodigo;
                 Columns[ 1 ].Caption:= Detalle_DevEx.Caption;
                 Columns[ 5 ].DataBinding.FieldName := 'CT_VACANTES';
                 Columns[ 6 ].DataBinding.FieldName := 'CT_EXCEDENTE';
            end;
            with dmConteo do
            begin
                 if dsDetalle.DataSet.Name = dmConteo.cdsConteo.Name then
                 begin
                      with ZetaDBGridDBTableView do
                      begin
                           Columns[ 5 ].DataBinding.FieldName := 'VACANTES';
                           Columns[ 6 ].DataBinding.FieldName := 'EXCEDENTE';

                      end;
                 end;
            end;

            SetBotonesEdicion;

            // Gr�fica Real
            Series2.DataSource := oDataSet;
            //Series2.XLabelsSource := sCodigo;
            Series2.XLabelsSource := 'TB_ELEMENT';
            Series2.YValues.ValueSource   := 'CT_REAL';
            with Series2.OtherSlice do
            begin
                 Style := poBelowPercent;
                 Text := 'Otros';
                 Value := 5;
            end;

            // Gr�fica Presupuesto vs Real
            Series3.DataSource := oDataSet;
            Series3.XLabelsSource := sCodigo;
            Series3.YValues.ValueSource := 'CT_CUANTOS';

            Series4.DataSource := oDataSet;
            Series4.XLabelsSource := sCodigo;
            Series4.YValues.ValueSource := 'CT_REAL';

            SetTitulos;

            //SetRefrescarGraficas( TRUE ); //OLD
            //PageDetalleChange( PageDetalle ); //OLD
            PageDetalle_DevExChange( PageDetalle_DevEx );
            dxBarButton_UpBtn.Enabled := Node.Level > 0 ;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TEditConteo_DevEx.GetFiltroGrupos(Nodo: TTreeNode): String;
var
   nRecord : Integer;
   i : Integer;
   cdsGrupos : TDataset;
   sIndexField : string;

   function FiltroUnCampo( const nPos : Integer ) : String;
   begin //PENDIENTE SI SE AGRUPA POR TIPO <> STRING
        // Supone que todos los Grupos son tipo String
        with cdsGrupos.Fields[ nPos*Off_SET] do
             if DataType in [ftDate,ftTime,ftDateTime] then
                Result := FieldName  + ' = ' + EntreComillas(FormatDateTime('dd/mm/yyyy',AsDateTime))
             else Result := FieldName  + ' = ' + EntreComillas(AsString);
   end;

begin
     nRecord := Integer( Nodo.Data );
     if ( nRecord = 0 ) then
         Result := ''
     else
     begin
          sIndexField := dmConteo.cdsConteo.IndexFieldNames;
          dmConteo.cdsConteo.IndexFieldNames := 'CT_NIVEL_1;CT_NIVEL_2;CT_NIVEL_3;CT_NIVEL_4;CT_NIVEL_5';

          cdsGrupos := dmConteo.cdsConteo;
          cdsGrupos.Filtered := FALSE;
          cdsGrupos.Recno := nRecord;
          Result := FiltroUnCampo( 0 );
          for i := 1 to Nodo.Level-1 do
              Result := ConcatFiltros(Result, FiltroUnCampo( i ));

          dmConteo.cdsConteo.IndexFieldNames := sIndexField;
     end;
end;

procedure TEditConteo_DevEx.PosicionaArbol( Llaves: array of string; const lParcialKey: Boolean = FALSE );
var
   oNodo : TTreeNode;
   i: integer;
   oCursor : TCursor;

   function SetParcialKey: Boolean;
   var
      j : Integer;
   begin
        Result := lParcialKey;
        if Result then
        begin
             for j := K_MAX_CONTEO_NIVELES-2 downto 0 do
                 if StrLleno( Llaves[j] ) then
                 begin
                      Llaves[j] := VACIO;
                      Break;
                 end;
             Result := StrLleno( Llaves[0] );
        end;
   end;

begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with Arbol_DevEx do
        begin
             Items.BeginUpdate;
             FullCollapse;

             Repeat
                   oNodo := Items.GetFirstNode;
                   for i:=0 to K_MAX_CONTEO_NIVELES-2 do
                   begin
                        if Llaves[i] = '' then Break;
                        if oNodo <> NIL then
                           oNodo := oNodo.GetNext;
                        while ( oNodo <> NIL ) and
                              (Copy(oNodo.Text,1,Pos(' = ',oNodo.Text)-1) <> Llaves[i]) do
                              oNodo := oNodo.GetNextSibling;
                   end;
             Until ( oNodo <> nil ) or ( not SetParcialKey );

             if oNodo <> NIL then
                Selected := oNodo
             else
                Selected := Items.GetFirstNode;

             Selected.Expand( FALSE );

             Items.EndUpdate;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEditConteo_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     //inherited;    ... No debe hacer nada
end;

procedure TEditConteo_DevEx.DataSourceStateChange(Sender: TObject);
begin
     //inherited;
     with Datasource do
     begin
          if ( Dataset = nil ) then
             Modo := dsInactive
          else if ( TClientDataSet( DataSet ).ChangeCount > 0 ) then
             Modo := dsEdit
          else
             Modo := Dataset.State;

     end;
     dxBarButton_RefrescarBtn.Enabled := Modo = dsBrowse;
end;

procedure TEditConteo_DevEx.DoCancelChanges;
begin
     inherited;
     dmConteo.RecalculaTotales;
     //ConstruyeArbol;
     ConstruyeArbol_DevEx;
     DataSourceStateChange( self );
end;

procedure TEditConteo_DevEx.HabilitaControles;
begin
     inherited;
     dxBarButton_AgregarBtn.Enabled := TRUE;
     dxBarButton_ImprimirBtn.Enabled := dxBarButton_AgregarBtn.Enabled;
     SetBotonesEdicion;
end;

procedure TEditConteo_DevEx.SetBotonesEdicion;
begin
     dxBarButton_BorrarBtn.Enabled := ( dsDetalle.Dataset <> nil ) and ( dsDetalle.Dataset.Active ) and ( dsDetalle.DataSet.RecordCount > 0 );
     dxBarButton_ModificarBtn.Enabled := dxBarButton_BorrarBtn.Enabled;
end;

{rocedure TEditConteo_DevEx.SetRefrescarGraficas( const lEnabled: Boolean );
var
   i : Integer;
begin
     for i := 1 to 3 do
         FRefrescarGraficas[i] := Enabled;
end;}

{procedure TEditConteo_DevEx.PageDetalleChange(Sender: TObject);
var
   iPag : Integer;
begin
     inherited;
     iPag := PageDetalle.ActivePageIndex;
     if ( iPag > 0 ) and FRefrescarGraficas[iPag] then
        RefrescaGrafica( iPag );
end; }

procedure TEditConteo_DevEx.RefrescaGrafica( iPos: Integer );
begin
     case iPos of
          1 : DBChart1.RefreshData;
          2 : begin
                   LblNoGraficar.Visible := ( dsTotales.Dataset.FieldByName( 'CT_REAL' ).AsFloat = 0 );
                   DBChart3.Visible := not LblNoGraficar.Visible;
                   if DBChart3.Visible then
                      DBChart3.RefreshData;
              end;
     end;
     //FRefrescarGraficas[iPos] := FALSE;
end;

procedure TEditConteo_DevEx.Imprimir;
var
   iAncho : integer;
begin
     if PageDetalle_DevEx.ActivePage = Detalle_DevEx then
     begin
          with ZetaDBGridDBTableView.DataController.DataSource.DataSet.FieldByName('TB_ELEMENT') do
          begin
               iAncho := DisplayWidth;
               DisplayWidth := 30;

               Inherited Imprimir;

               DisplayWidth := iAncho;
          end;
     end
     else if zConfirm( 'Imprimir...', '� Desea Imprimir La Pantalla ' + Caption + ' ?', 0, mbYes ) then
          Print;
end;

procedure TEditConteo_DevEx.InvocarModificacion;
var
   i : Integer;
   aPosicion : Array[0..3] of string;
begin
     if ( dsDetalle.DataSet.Name = dmConteo.cdsConteo.Name ) then
        DoEdit
     else
        with dmConteo, Arbol_DevEx.Selected do
        begin
             for i:=0 to iMin(Level+1,3) do
                 aPosicion[i] := DataSetTotales(Level+1).Fields[i].AsString;
             PosicionaArbol( aPosicion );
        end;
end;

procedure TEditConteo_DevEx.WMExaminar(var Message: TMessage);
begin
     InvocarModificacion;
end;

procedure TEditConteo_DevEx.KeyDown( var Key: Word; Shift: TShiftState );
begin
     inherited KeyDown( Key, Shift );

     if ( Key <> 0 ) then
     begin
          if ( Key = VK_F5 ) and ( not Editing ) then
          begin
               Key := 0;
               Refrescar;
          end;
     end;
end;

procedure TEditConteo_DevEx.EscribirCambios;
begin
     {cv: CUMPLEVERSION ES TEMPORAL SE DEBE ELIMINAR CUANDO SE GENERE LA 2.1}
     if Autorizacion.CumpleVersion( '2.1' ) and not Autorizacion.EsDemo then
     begin
          inherited EscribirCambios;
     end
     else
     begin
          ZetaDialogo.zInformation( Caption, 'Operaci�n no Permitida en Modo Demo', 0);
     end;
end;

procedure TEditConteo_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     if ( dmConteo.cdsConteo.ChangeCount > 0 ) then
        DoCancelChanges;
     inherited;
end;

function TEditConteo_DevEx.BotonesActivos: Boolean;
begin
     Result := TRUE;
end;

procedure TEditConteo_DevEx.OK_DevExClick(Sender: TObject);
var
   aPosicion : Array[0..3] of string;
   i: integer;
   sNivel : string;
   oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try

        with dmConteo do
          for i:=0 to NumNiveles do
          begin
               sNivel := DataSetTotales(Arbol_DevEx.Selected.Level+1).Fields[i].AsString;
               if i < Arbol_DevEx.Selected.Level then
                  aPosicion[i] := sNivel;
          end;

        inherited;

        if ( ClientDataset.ChangeCount = 0 ) then
        begin
             DataSourceStateChange( self );
             ConstruyeArbol_DevEx;
             PosicionaArbol( aPosicion, TRUE );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEditConteo_DevEx.dxBarButton_ModificarBtnClick(Sender: TObject);
begin
  InvocarModificacion;
end;

procedure TEditConteo_DevEx.dxBarButton_RefrescarBtnClick(Sender: TObject);
begin
  Refrescar;

end;

procedure TEditConteo_DevEx.dxBarButton_BtnConfigurarClick(
  Sender: TObject);
begin
     if dmConteo.GetGlobalConteo then
     begin
          FreeAndNil( EditPresup_DevEx );
          Refrescar;
     end;
end;

procedure TEditConteo_DevEx.dxBarButton_UpBtnClick(Sender: TObject);
begin
     inherited;
     with Arbol_DevEx do
          if ( Selected <> NIL ) and ( Selected.Parent <> NIL ) then
             Selected := Selected.Parent;
end;

procedure TEditConteo_DevEx.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =40;
begin
     with  ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;

end;

procedure TEditConteo_DevEx.ZetaDBGridDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
  AIndex: Integer;
begin
  inherited;
  AIndex := AValueList.FindItemByKind(fviCustom);
    if AIndex <> -1 then
      AValueList.Delete(AIndex);
end;

procedure TEditConteo_DevEx.ZetaDBGridDBTableViewDblClick(Sender: TObject);
begin
  inherited;
  DoEdit;
end;

procedure TEditConteo_DevEx._A_ImprimirExecute(Sender: TObject);
begin
  inherited;
  DoPrint;
end;

procedure TEditConteo_DevEx._E_AgregarExecute(Sender: TObject);
begin
  inherited;
   DoInsert;
end;

procedure TEditConteo_DevEx._E_BorrarExecute(Sender: TObject);
begin
  inherited;
  DoDelete;
end;

procedure TEditConteo_DevEx._E_ModificarExecute(Sender: TObject);
begin
  inherited;
  DoEdit;
end;

//DevEx (by am): Se sobrecarga el metodo para invocar este cuando se desee aplicar un formato a la columna
Procedure TEditConteo_DevEx.CreaColumaSumatoria(Columna:TcxGridDBColumn; TipoFormato: Integer ; TextoSumatoria: String ; FooterKind : tcxSummaryKind );
const
     K_FORMATO_DEFAULT ='0';
begin
if Columna.Visible then
begin
     //Si la banda no esta visible se muestra.
     if ZetaDbGridDBTableView.OptionsView.Footer = false then
        ZetaDbGridDBTableView.OptionsView.Footer := true;
      with ZetaDbGridDBTableView.DataController.Summary do
      begin
           BeginUpdate;
           try
              with FooterSummaryItems.Add as TcxGridDBTableSummaryItem do
              begin
                   Column := Columna;
                    Kind := FooterKind;
                    {***Tipo de formato que se desee aplicar**}
                    Format := 'Total: ' + K_FORMATO_DEFAULT;
              end;
           finally
                  EndUpdate;
           end;
      end;
end;
end;

procedure TEditConteo_DevEx.ZetaDBGridDBTableViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   sValor:String;
begin
  inherited;
  sValor := AViewInfo.GridRecord.DisplayTexts [CT_DIFERENCIA.Index];
  With ACanvas do
  begin
      with Font do
      begin
           if StrToFloat(sValor) < 0 then
              Color := clRed
           else if StrToFloat(sValor) > 0 then
                Color := clGreen
           else Color := clBlack;
      end;
  end;
end;

procedure TEditConteo_DevEx.PageDetalle_DevExChange(Sender: TObject);
var
   iPag : Integer;
begin
     inherited;
     iPag := PageDetalle_DevEx.ActivePageIndex;
          if ( iPag > 0 ) {and FRefrescarGraficas[iPag]} then
             RefrescaGrafica( iPag );
end;

procedure TEditConteo_DevEx.Arbol_DevExChange(Sender: TObject;
  Node: TTreeNode);
var
    oTotales, oDataSet : TDataset;
    sCodigo  : String;

    procedure SetTitulos;
    var
       oNode: TTreeNode;
       sNivel, sField : string;
       i: integer;
    begin
         oNode := Node;
         lbNiveles.Caption := '';
         lbTotales.Caption := '';
         sNivel := '';
         FTitulo :='';

         if oNode.Level = 0 then
         begin
              FTitulo := 'Totales de Empresa';
              lbNiveles.Caption := FTitulo;
              lbTotales.Caption := '';
         end
         else if oNode.Level = 1 then
         begin
              FTitulo := 'Totales de ' + dmConteo.GetDescripConteoNiveles( oNode.Level-1 ) + ': '+
                         dmConteo.cdsConteo.FieldByName('CT_NIVEL_1').AsString;
              lbNiveles.Caption := FTitulo;
              lbTotales.Caption := '';
         end
         else
         begin
              with dmConteo, cdsConteo do
              begin
                   for i:= 1 to oNode.Level - 1 do
                   begin
                        sNivel := GetDescripConteoNiveles( i-1) + ': ';
                        sField := FieldByName('CT_NIVEL_'+ IntToStr(i)).AsString;
                        lbNiveles.Caption := lbNiveles.Caption + ' -- ' + sNivel + sField;
                        FTitulo := FTitulo + sNivel + sField  +'='+ FieldByName('TB_ELEMENT'+ IntToStr(i)).AsString + CR_LF;
                   end;
                   sNivel := dmConteo.GetDescripConteoNiveles( oNode.Level -1 ) + ': ';
                   lbNiveles.Caption := lbNiveles.Caption + ' -- ';
                   sField := FieldByName('CT_NIVEL_'+ IntToStr(oNode.Level) ).AsString;
                   lbTotales.Caption := 'Totales de ' + sNivel + sField;
                   FTitulo := FTitulo + sNivel + sField + '='+ FieldByName('TB_ELEMENT'+ IntToStr(oNode.Level)).AsString;
              end;
         end;
    end;

var
   //i : integer;  //old
   oCursor : TCursor;
   oParentNodo : TTreeNode;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if Node <> NIL then
        begin
            // Filtra los padres hacia arriba del nodo para recalculo de Totales
            oParentNodo := Node.Parent;
            while ( oParentNodo <> nil ) and ( oParentNodo.Level > 0 ) do
            begin
                 with dmConteo.DataSetTotales( oParentNodo.Level ) do
                 begin
                      Filter := GetFiltroGrupos( oParentNodo );
                      Filtered := TRUE;
                 end;
                 oParentNodo := oParentNodo.Parent;
            end;
            // Filtra Dataset Detalle
            oDataSet := dmConteo.DataSetTotales( Node.Level+1 );
            oDataSet.Filtered := FALSE;
            oDataSet.Filter := GetFiltroGrupos( Node );
            oDataSet.Filtered := TRUE;
            dsDetalle.DataSet := oDataSet;
            with dmConteo do
            begin
                 Detalle_DevEx.Caption := GetDescripConteoNiveles( Node.Level );
            end;
            // DataSet de Totales
            oTotales := dmConteo.DataSetTotales( Node.Level );
            oTotales.Filter := oDataSet.Filter;
            oTotales.Filtered := TRUE;
            dsTotales.DataSet := oTotales;

            // Columna de Grid
            sCodigo := oDataSet.Fields[ Node.Level ].FieldName;
            with ZetaDBGridDBTableView do
            begin
                 Columns[ 0 ].DataBinding.FieldName := sCodigo;
                 Columns[ 1 ].Caption:= Detalle_DevEx.Caption;
                 Columns[ 5 ].DataBinding.FieldName := 'CT_VACANTES';
                 Columns[ 6 ].DataBinding.FieldName := 'CT_EXCEDENTE';
            end;
            with dmConteo do
            begin
                 if dsDetalle.DataSet.Name = dmConteo.cdsConteo.Name then
                 begin
                      with ZetaDBGridDBTableView do
                      begin
                           Columns[ 5 ].DataBinding.FieldName := 'VACANTES';
                           Columns[ 6 ].DataBinding.FieldName := 'EXCEDENTE';

                      end;
                 end;
            end;

            SetBotonesEdicion;

            // Gr�fica Real
            Series2.DataSource := oDataSet;
            //Series2.XLabelsSource := sCodigo;
            Series2.XLabelsSource := 'TB_ELEMENT';
            Series2.YValues.ValueSource   := 'CT_REAL';
            with Series2.OtherSlice do
            begin
                 Style := poBelowPercent;
                 Text := 'Otros';
                 Value := 5;
            end;

            // Gr�fica Presupuesto vs Real
            Series3.DataSource := oDataSet;
            Series3.XLabelsSource := sCodigo;
            Series3.YValues.ValueSource := 'CT_CUANTOS';

            Series4.DataSource := oDataSet;
            Series4.XLabelsSource := sCodigo;
            Series4.YValues.ValueSource := 'CT_REAL';

            SetTitulos;

            //SetRefrescarGraficas( TRUE ); //OLD
            //PageDetalleChange( PageDetalle ); //OLD
            PageDetalle_DevExChange( PageDetalle_DevEx );
            dxBarButton_UpBtn.Enabled := Node.Level > 0 ;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TEditConteo_DevEx.ZetaDBGridDBTableViewKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
       DoEdit;
  end;
end;

end.
