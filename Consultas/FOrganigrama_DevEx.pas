unit FOrganigrama_DevEx;

interface

uses
    Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
    ZBaseConsulta, Db, ExtCtrls, ComCtrls, ImgList, StdCtrls, Buttons,
    ZetaDBTextBox, ZetaStateComboBox, ZetaSmartLists, Mask, DBCtrls,
    ZetaKeyCombo, Grids, DBGrids,
    ZetaCommonClasses,
    ZetaCommonLists,
    ZetaDBGrid, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, Menus,
  ActnList, cxContainer, cxTreeView, cxButtons, imageenview, ieview,
  System.Actions;

type
  TOrganigrama_DevEx = class(TBaseConsulta)
    Splitter1: TSplitter;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel: TPanel;
    dsGrid: TDataSource;
    Label16: TLabel;
    Panel3: TPanel;
    gbSubordinados: TGroupBox;
    Panel6: TPanel;
    PanelInfoEmp: TPanel;
    Splitter2: TSplitter;
    Label1: TLabel;
    PL_FOLIO: TZetaDBTextBox;
    PL_CODIGO: TZetaDBTextBox;
    PL_NOMBRE: TZetaDBTextBox;
    PU_DESCRIP: TZetaDBTextBox;
    PU_CODIGO: TZetaDBTextBox;
    Label19: TLabel;
    Label6: TLabel;
    Label23: TLabel;
    PL_FEC_INI: TZetaDBTextBox;
    Label21: TLabel;
    PL_FEC_FIN: TZetaDBTextBox;
    ZREPORTA: TZetaTextBox;
    PL_REPORTA: TZetaDBTextBox;
    Label3: TLabel;
    Label22: TLabel;
    CB_CODIGO: TZetaDBTextBox;
    PL_TIPO: TZetaDBTextBox;
    Label4: TLabel;
    Label7: TLabel;
    PL_TIREP: TZetaDBTextBox;
    PanelInferior: TPanel;
    Label5: TLabel;
    NodoPlazas: TZetaTextBox;
    Label8: TLabel;
    NodoOcupadas: TZetaTextBox;
    Label10: TLabel;
    NodoVacantes: TZetaTextBox;
    ZTITULAR: TZetaTextBox;
    ZetaDBGrid: TZetaCXGrid;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    ZetaDBGridLevel: TcxGridLevel;
    CB_CODIGO_GRID: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    PL_FOLIO_GRID: TcxGridDBColumn;
    PL_NOMBRE_GRID: TcxGridDBColumn;
    cxImage16: TcxImageList;
    ActionList: TActionList;
    _A_Imprimir: TAction;
    _E_Agregar: TAction;
    _E_Borrar: TAction;
    _E_Modificar: TAction;
    PopupMenu1: TPopupMenu;
    mAgregar: TMenuItem;
    mBorrar: TMenuItem;
    mModificar: TMenuItem;
    mImprimir: TMenuItem;
    Arbol: TcxTreeView;
    ImageList: TcxImageList;
    Expander: TcxButton;
    Compactar: TcxButton;
    BuscarPlaza: TcxButton;
    Buscar: TcxButton;
    AplicaTabulador: TcxButton;
    PlazaActiva: TcxButton;
    bUbicarJefe: TcxButton;
    EmpleadoActivo: TcxButton;
    FotoSwitch: TcxButton;
    FOTO: TImageEnView;
    dsFoto: TDataSource;
    procedure ExpanderClick(Sender: TObject);
    procedure CompactarClick(Sender: TObject);
    procedure ArbolChange(Sender: TObject; Node: TTreeNode);
    procedure BuscarClick(Sender: TObject);
    procedure FotoSwitcholdClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EmpleadoActivoClick(Sender: TObject);
    procedure UbicarJefeClick(Sender: TObject);
//    procedure PageControlChange(Sender: TObject);
//    procedure GridEmpleadosDblClick(Sender: TObject);
    procedure ArbolDblClick(Sender: TObject);
    procedure PlazaActivaClick(Sender: TObject);
    procedure BuscarPlazaClick(Sender: TObject);
    procedure ArbolAdvancedCustomDrawItem(Sender: TCustomTreeView; Node: TTreeNode; State: TCustomDrawState; Stage: TCustomDrawStage; var PaintImages, DefaultDraw: Boolean);
    procedure ArbolDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ArbolDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure ArbolCompare(Sender: TObject; Node1, Node2: TTreeNode;
      Data: Integer; var Compare: Integer);
    procedure AplicaTabuladorClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure _A_ImprimirExecute(Sender: TObject);
    procedure _E_AgregarExecute(Sender: TObject);
    procedure _E_BorrarExecute(Sender: TObject);
    procedure _E_ModificarExecute(Sender: TObject);
    procedure ZetaDBGridDBTableViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ZetaDBGridDBTableViewDataControllerSortingChanged(
      Sender: TObject);
    procedure ZetaDBGridDBTableViewColumnHeaderClick(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
  private
    { Private declarations }
//    FEmpleadoFoto: integer;
    FInicial: boolean;
    AColumn: TcxGridDBColumn;
    function EsNodo( oNodoDestino: TTreeNode ): Boolean;
    procedure RefrescaTotalesNodo(oNodoActual: TTreeNode);
    procedure IniciarArbol;
    procedure SetControlesPlaza;
    procedure ApplyMinWidth;
//    function EmpleadoActive: integer;
//    function  ObtenNodoSeleccionado( oArbol: TTreeview ):Integer;
//    procedure RefrescaGrid( const iPlaza: Integer );
//    procedure GetFotoEmpleado;
//    procedure InitDatosOrganigrama;
//    procedure CrearArbol;
  protected
    { Protected declarations }
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Modificar; override;
    procedure Agregar; override;
    procedure Borrar; override;
  public
    { Public declarations }
  end;

var
   Organigrama_DevEx: TOrganigrama_DevEx;

implementation

{$R *.DFM}

uses
    dBasicoCliente,
    dBaseCliente,
    dCliente,
    dCatalogos,
    dRecursos,
    ZAccesosTress,
    ZArbolOrganigramaTools,
    ZetaSystemWorking,
    ZetaCommonTools,
    ZetaDialogo,
    FTressShell, 
    FWizPlazas_DevEx,
    FToolsImageEn,
    ZetaClientDataSet;

{ TOrganigrama }

procedure TOrganigrama_DevEx.FormCreate(Sender: TObject);
begin
     {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= True;
     {***}
     FInicial := TRUE;
     inherited;
     IndexDerechos := D_CONS_ORGANIGRAMA;
     HelpContext := H50056_Organigrama_de_la_empresa;
end;

procedure TOrganigrama_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsPuestos.Conectar;
     end;
     with dmRecursos do
     begin
          DataSource.DataSet := cdsPlazas;
          dsGrid.DataSet := cdsOrganigrama;
          dsFoto.DataSet := cdsFotoOrganigrama;
          cdsEmpPlaza.Conectar;
     end;
     if FInicial then
     begin
          self.IniciarArbol;
          FInicial := FALSE;
     end;
end;

procedure TOrganigrama_DevEx.Refresh;
begin
     IniciarArbol;
     dmRecursos.cdsEmpPlaza.Refrescar;
end;

procedure TOrganigrama_DevEx.Agregar;
var
   iPlaza: Integer;
begin
     dmCliente.SetLookupEmpleado( eLookEmpPlaza ); //Se usa por que ya tiene el Puesto
     try
        WizPlazas_DevEx := TWizPlazas_DevEx .Create( Application );
        try
           iPlaza := ZArbolOrganigramaTools.ObtenPlazaSeleccionada( Arbol );
           WizPlazas_DevEx .ReportaA := iPlaza;
           WizPlazas_DevEx .ShowModal;
           iPlaza := WizPlazas_DevEx .ReportaA;
        finally
               FreeAndNil( WizPlazas_DevEx  );
        end;
     finally
            dmCliente.ResetLookupEmpleado;
     end;
     // Si se agregaron plazas se refresca el nodo reporta a del arbol
     with dmRecursos.cdsPlazas do
     begin
          if ( RecordCount > 0 ) and ( FieldByName( 'PL_FOLIO' ).AsInteger = 0 ) then
          begin
               if dmRecursos.GetPlazasArbol( iPlaza ) then
               begin
                    if ( iPlaza = ZArbolOrganigramaTools.ObtenPlazaSeleccionada( Arbol ) ) then
                       self.ArbolChange( Arbol, ZArbolOrganigramaTools.GetNodoPlaza( Arbol, iPlaza ) )
                    else
                        Arbol.Selected := GetNodoPlaza( Arbol, iPlaza );      // Si cambi� el arbolchange se invoca al cambiar el nodo seleccionado
                    ZArbolOrganigramaTools.AgregaNuevosNodosSubordinados( Arbol, Arbol.Selected );
               end;
          end;
     end;
end;

function TOrganigrama_DevEx.PuedeModificar( var sMensaje: String ): Boolean;
var
   iPlaza: Integer;
begin
     Result := Inherited PuedeModificar( sMensaje );
     if Result then
     begin
          iPlaza := dmRecursos.cdsPlazas.FieldByName( 'PL_FOLIO' ).AsInteger;
          Result := ( iPlaza > 0 );
          if Result then
          begin
               Result := ( ZArbolOrganigramaTools.ObtenPlazaSeleccionada( Arbol ) = iPlaza );
               if ( not Result ) then
                  sMensaje := Format( 'No se pudo ubicar el registro de la plaza %d', [ iPlaza ] ) + CR_LF + 'Se recomienda refrescar la pantalla';
          end
          else sMensaje := 'No se tiene seleccionada una plaza a modificar';
     end;
end;

procedure TOrganigrama_DevEx.Modificar;
const
     K_FILTRO_LISTA =  '( PL_FOLIO IN ( select PL_FOLIO from PLAZA where ( PL_REPORTA IN ( %s ) ) ) )';
var
   oNodo, oNodoDestino: TTreeNode;
   iReportaA: Integer;
   {$ifdef DOS_CAPAS}
   sPadres,sLista: String;
   {$else}
   sHijos: String;
   {$endif}

   {$ifdef DOS_CAPAS}
   function ConstruyeListaPlazas: String;
   var
      oHermano: TTreeNode;
      oUltimoNodo:TTreeNode;
      lTopeLimite: Boolean;
   begin
        Result := VACIO;
        oUltimoNodo:= oNodo;
        oHermano:= oUltimoNodo.GetNextSibling;

        while ( ( oUltimoNodo <> Nil ) and ( oUltimoNodo <> oHermano ) ) do
        begin
             ZArbolOrganigramaTools.GetNodosPadres( sPadres, oUltimoNodo, oHermano, lTopeLimite );
             if ( lTopeLimite ) then
             begin
                  Result := ConcatString( Result, Format( K_FILTRO_LISTA, [ sPadres ] ), ' OR ' );
                  sPadres:= VACIO;
                  ZArbolOrganigramaTools.GetNodosPadres( sPadres, oUltimoNodo, oHermano, lTopeLimite );
             end;
        end;

        if ZetaCommonTools.strLleno( sPadres ) then
           Result := ConcatString( Result, Format( K_FILTRO_LISTA, [ sPadres ] ), ' OR ' );
   end;
   {$endif}

begin
     with dmRecursos do
     begin
          iReportaA := cdsPlazas.FieldByName( 'PL_REPORTA' ).AsInteger;
          oNodo := Arbol.Selected;
          {$ifdef DOS_CAPAS}
          sPadres := VACIO;
          {Arregla el defecto del limite de items en un IN de FIREBIRD, Se optimiz� para que concatenar un select en el IN
          que revisara PL_REPORTA }
          sLista:= ConstruyeListaPlazas;
          {Concatena todos los que son padres}
          if ( StrLleno ( sLista )  ) then
          begin
               FiltroReportaAPlaza := Format( 'NOT ( %s )',[ sLista ] );
          end;
          { Se concatena el mismo }
          FiltroReportaAPlaza := ConcatString( FiltroReportaAPlaza, Format( '( PL_FOLIO <> %s )', [ GetTextoNodo(oNodo)] ), ' AND ' );
          {$else}
          sHijos := VACIO;
          ZArbolOrganigramaTools.GetNodosHijos( sHijos, oNodo, oNodo.GetNextSibling );
          if strLleno( sHijos ) then
             FiltroReportaAPlaza := Format( 'NOT ( PL_FOLIO IN ( %s ) )', [ sHijos ] );
          {$endif}
          try
             cdsPlazas.Modificar;
             SetControlesPlaza;
             Arbol.Repaint;    
          finally
                 FiltroReportaAPlaza := VACIO;
          end;
          with cdsPlazas do
          begin
               if ( iReportaA <> FieldByName( 'PL_REPORTA' ).AsInteger ) then    // Cambi� el Reporta A
               begin
                    oNodoDestino := ZArbolOrganigramaTools.GetNodoPlaza( Arbol, FieldByName( 'PL_REPORTA' ).AsInteger );
                    ZArbolOrganigramaTools.MoverNodoArbol( Arbol, oNodo, oNodoDestino );
               end;
               with oNodo do
               begin
                    Text := FieldByName( 'PL_FOLIO' ).AsString + ' = ' + FieldByName( 'PL_NOMBRE' ).AsString;
                    Data := TObject( FieldByName( 'CB_CODIGO' ).AsInteger );
               end;
          end;
     end;
end;

function TOrganigrama_DevEx.PuedeBorrar( var sMensaje: String ): Boolean;
var
   iPlaza: Integer;
begin
     Result := Inherited PuedeBorrar( sMensaje );
     if Result then
     begin
          with dmRecursos.cdsPlazas do
          begin
               iPlaza := FieldByName( 'PL_FOLIO' ).AsInteger;
               Result := ( iPlaza > 0 );
               if Result then
               begin
                    Result := ( FieldByName( 'CB_CODIGO' ).AsInteger = 0 );
                    if Result then
                    begin
                         Result := ( ZArbolOrganigramaTools.ObtenPlazaSeleccionada( Arbol ) = iPlaza );
                         if Result then
                         begin
                              Result := ( not ZArbolOrganigramaTools.NodoTieneTitular( Arbol.Selected ) );
                              if ( not Result ) then
                                 sMensaje := 'No se puede borrar la plaza si tiene subordinados con titular asignado';
                         end
                         else sMensaje := Format( 'No se pudo ubicar el registro de la plaza %d', [ iPlaza ] ) + CR_LF + 'Se recomienda refrescar la pantalla';
                    end
                    else sMensaje := 'No se puede borrar la plaza si tiene titular';
               end
               else sMensaje := 'No se tiene seleccionada una plaza a borrar';
          end;
     end;
end;

procedure TOrganigrama_DevEx.Borrar;
var
   oNodo: TTreeNode;
   sPlazas: String;
   iCuantas: Integer;
begin
     oNodo := Arbol.Selected;
     ZArbolOrganigramaTools.GetNodosHijos( sPlazas, oNodo, oNodo.GetNextSibling );
     if strLleno( sPlazas ) then
     begin
          if ZConfirm(Caption, '� Est� seguro de borrar la Plaza ?' + CR_LF +
                               'Si la Plaza tiene Plazas subordinadas, �stas se borrar�n tambi�n.', 0, mbNo ) then
          begin
               if dmRecursos.BorrarPlazas( sPlazas, iCuantas ) then
               begin
                    ZArbolOrganigramaTools.BorraNodo( Arbol, oNodo );
                    //ZInformation( self.Caption, Format( 'Se borraron %d plazas', [ iCuantas ] ), 0 );
               end;
          end;
     end
     else ZError( self.Caption, 'Error no se pudo ubicar plaza a borrar', 0 )
end;

{ M�todos privados }

procedure TOrganigrama_DevEx.IniciarArbol;
var
   iPlazaActual: Integer;
begin
     ZetaSystemWorking.InitAnimation( 'Leyendo Organigrama...' );
     try
        iPlazaActual := ZArbolOrganigramaTools.ObtenPlazaSeleccionada( Arbol );
        dmRecursos.cdsArbolPlazas.Refrescar;
        ZArbolOrganigramaTools.CrearArbol( Arbol, iPlazaActual );
     finally
            TressShell.ReconectaMenu;
            ZetaSystemWorking.EndAnimation;
     end;
end;

procedure TOrganigrama_DevEx.SetControlesPlaza;
var
   iRecNo: Integer;
   sDescrip: String;
begin
     with dmRecursos do
     begin
          // Nombre del titular
          PlazaActiva.Enabled := ( cdsPlazas.FieldByName('CB_CODIGO').AsInteger > 0 );
          if PlazaActiva.Enabled then
             ZTITULAR.Caption := dmCliente.cdsEmpleadoLookup.GetDescripcion( cdsPlazas.FieldByName('CB_CODIGO').AsString )
          else
              ZTITULAR.Caption := VACIO;
          // Nombre de reporta a
          if cdsPlazasLookup.Lookupkey( cdsPlazas.FieldByName( 'PL_REPORTA' ).AsString, VACIO, sDescrip ) then
             ZREPORTA.Caption := dmCliente.cdsEmpleadoLookup.GetDescripcion( cdsPlazasLookUp.FieldByName( 'CB_CODIGO' ).AsString )
          else
              ZREPORTA.Caption := VACIO;

          PanelInfoEmp.Visible := ( not cdsPlazas.IsEmpty );
     end;
     iRecNo := dsGrid.Dataset.RecordCount;
     gbSubordinados.Caption := ' Subordinados directos: ' + IntToStr( iRecNo ) + ' ';
     EmpleadoActivo.Enabled := ( iRecNo > 0 );
end;

procedure TOrganigrama_DevEx.RefrescaTotalesNodo( oNodoActual: TTreeNode );
var
   iTotPlazas, iTotOcupadas, iTotVacantes: Integer;
begin
     ZArbolOrganigramaTools.ObtenTotalesNodo( oNodoActual, iTotPlazas, iTotOcupadas, iTotVacantes );
     NodoPlazas.Caption := IntToStr( iTotPlazas );
     NodoOcupadas.Caption := IntToStr( iTotOcupadas );
     NodoVacantes.Caption := IntToStr( iTotVacantes );
end;

{ Eventos }

procedure TOrganigrama_DevEx.ArbolChange(Sender: TObject; Node: TTreeNode);
var
   iPlazaArbol: Integer;
   oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if ( Node <> nil ) then
        begin
             //iPlazaArbol := ObtenPlazaSeleccionada( TTreeView( Sender ) );
             {***DevEx(@am): El cast de TcxTreeView no funciona bien, por lo tanto mandamos directamente el Objeto
                              en vez del sender lo cual no afecta pues este evento solo se utiliza en esta forca con este arbol***}
             iPlazaArbol := ObtenPlazaSeleccionada( Arbol );

             {
               AL 2014-09-30. Nuevo componente de imagen.
               Indicar si se visualiza o no la foto del empleado en lugar de enviar siempre FALSE.
             }
             // dmRecursos.RefrescaPlaza( iPlazaArbol, FALSE );
             dmRecursos.RefrescaPlaza( iPlazaArbol, FOTO.Visible );
             if FOTO.Visible then
                  AsignaBlobAImagen( FOTO, dsFoto.DataSet, 'FOTOGRAFIA' );

             SetControlesPlaza;
             RefrescaTotalesNodo( Node );
             if dmRecursos.cdsPlazas.IsEmpty and ( iPlazaArbol > 0 ) then
                ZetaDialogo.zInformation( Self.Caption, Format('La plaza %d no existe',[ iPlazaArbol ] ), 0 );
             //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
             ZetaDBGridDBTableView.ApplyBestFit();
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TOrganigrama_DevEx.ArbolDblClick(Sender: TObject);
var
   oNodo: TTreeNode;
   sMensaje:string;
begin
     if ( PuedeModificar( sMensaje ) ) then
     begin
          oNodo := Arbol.Selected;
          if Assigned( oNodo ) and ( oNodo.Level > 0 ) then   // Click sobre una plaza
             self.Modificar;     // Ya estar� posicionado el dataset
     end
     else zInformation( Caption, 'No tiene permiso para modificar', 0 );
end;

procedure TOrganigrama_DevEx.ArbolDragDrop(Sender, Source: TObject; X, Y: Integer);
var
   oNodoOrigen, oNodoDestino: TTreeNode;
begin
     inherited;
     oNodoOrigen := TcxTreeView( (Source as tcxDragControlObject).Control).Selected;
     {***DevEx(by am): Anteriormente se utilizaba (oNodoOrigen := TTreeView( Source ).Selected;) sin embargo el cast de source
      no funciona asi con el componente de DevExpress y deja la variable oNodoOrigen en nil, apesar de que si compila. ***}
     //with TcxTreeView( Sender ) do //FIX
     with ( Sender as TcxTreeView ) do
     begin
          oNodoDestino:= GetNodeAt( X, Y );
          if EsNodo( oNodoDestino ) then
          begin
               if dmRecursos.CambiaCodigoReporta( ZArbolOrganigramaTools.GetPlazaNodo( oNodoDestino ) ) then
                  ZArbolOrganigramaTools.MoverNodoArbol( Arbol, oNodoOrigen, oNodoDestino );
          end;
     end;
end;

procedure TOrganigrama_DevEx.ArbolCompare(Sender: TObject; Node1, Node2: TTreeNode; Data: Integer; var Compare: Integer);
var
   iPlaza1, iPlaza2: Integer;
begin
     inherited;
     iPlaza1 := ZArbolOrganigramaTools.GetPlazaNodo( Node1 );
     iPlaza2 := ZArbolOrganigramaTools.GetPlazaNodo( Node2 );
     if ( Data = 0 ) then
       Compare := ( iPlaza1 - iPlaza2 )
     else
       Compare := -1 * ( iPlaza1 - iPlaza2 );
end;

procedure TOrganigrama_DevEx.ArbolDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
var
   oNodoDestino, oNodoSeleccionado: TTreeNode;
   sMensaje: String;
begin
     inherited;
     Accept := ( PuedeModificar ( sMensaje ) ) and ( Sender is TcxTreeView ) and (Source is tcxDragControlObject);
     //DevEx(by am): Anteriormente se utilizaba este cast ( Source is TcxTreeView ) pero con el componente de DevExpress no funciona
     if ( Accept ) then
     begin
          with TcxTreeView( Sender ) do
          //with ( Sender as TcxTreeView ) do  //FIX
          begin
               oNodoDestino := GetNodeAt( X, Y );
               oNodoSeleccionado := Selected;
               Accept:= ( EsNodo( oNodoDestino ) ) and ( oNodoDestino <> oNodoSeleccionado ) and
                        ( not oNodoDestino.HasAsParent( oNodoSeleccionado ) );  //Esta Condici�n es para evitar la Relaci�n Circular
          end;
     end;
end;

function TOrganigrama_DevEx.EsNodo( oNodoDestino: TTreeNode ): Boolean;
begin
     Result := Assigned( oNodoDestino );
end;

procedure TOrganigrama_DevEx.ArbolAdvancedCustomDrawItem(Sender: TCustomTreeView; Node: TTreeNode;
          State: TCustomDrawState; Stage: TCustomDrawStage; var PaintImages, DefaultDraw: Boolean);
begin
     inherited;
     if ( Node.Level > 0 ) and ( Integer( Node.Data ) = 0 ) then  // Es una plaza y no tiene titular
        Sender.Canvas.Font.Color := clRed;
end;

procedure TOrganigrama_DevEx.ExpanderClick(Sender: TObject);
begin
     ZArbolOrganigramaTools.CambiaArbol( Arbol, FALSE );
end;

procedure TOrganigrama_DevEx.CompactarClick(Sender: TObject);
begin
     ZArbolOrganigramaTools.CambiaArbol( Arbol, TRUE );
     Arbol.Selected := Arbol.Items[0];
end;

procedure TOrganigrama_DevEx.FotoSwitcholdClick(Sender: TObject);
begin
     inherited;

     with dmRecursos do
     begin
         FOTO.Visible := FotoSwitch.Down;
         if FOTO.Visible then
         begin
            dmRecursos.GetOrganigramaFoto( dmRecursos.cdsPlazas.FieldByName( 'CB_CODIGO' ).AsInteger );
            AsignaBlobAImagen( FOTO, dsFoto.DataSet, 'FOTOGRAFIA' );
         end;
     end;
end;

procedure TOrganigrama_DevEx.BuscarPlazaClick(Sender: TObject);
var
   sKey, sDescription: String;
begin
     inherited;
     if dmRecursos.cdsPlazasLookup.Search_DevEx( VACIO, sKey, sDescription ) then
        ZArbolOrganigramaTools.UbicaPlaza( Arbol, StrToIntDef( sKey, 0 ) );
end;

procedure TOrganigrama_DevEx.EmpleadoActivoClick(Sender: TObject);
begin
     inherited;
     if dmCliente.SetEmpleadoNumero( dsGrid.Dataset.FieldByName('CB_CODIGO').AsInteger ) then
        TressShell.RefrescaEmpleado;
end;

procedure TOrganigrama_DevEx.PlazaActivaClick(Sender: TObject);
var
   iEmpleado: Integer;
begin
     inherited;
     iEmpleado := dmRecursos.cdsPlazas.FieldByName( 'CB_CODIGO' ).AsInteger;
     if ( iEmpleado <> 0 ) and dmCliente.SetEmpleadoNumero( iEmpleado ) then
        TressShell.RefrescaEmpleado;
end;

procedure TOrganigrama_DevEx.UbicarJefeClick(Sender: TObject);
begin
     inherited;
     if ( not UbicaPlaza( Arbol, dmRecursos.cdsPlazas.FieldByName('PL_REPORTA').AsInteger ) ) then
        ZetaDialogo.zInformation( Self.Caption, 'La plaza no tiene asignado un Jefe' , 0 );
end;

procedure TOrganigrama_DevEx.BuscarClick(Sender: TObject);
var
   sDescription, sKey: string;
begin
     inherited;
     Arbol.SetFocus;
     with dmCliente do
     begin
          SetLookupEmpleado( eLookEmpPlaza );
          try
             with cdsEmpleadoLookup do
             begin
                  if Search( VACIO, sKey, sDescription ) then
                  begin
                       LookupKey( sKey, VACIO, sDescription );
                       if ( ( ( dmCliente.UsaPlazas )     and ( not UbicaPlaza( Arbol, FieldByName( 'CB_PLAZA' ).AsInteger ) ) )
                         or ( ( not dmCliente.UsaPlazas ) and ( not UbicaEmpleado(Arbol, FieldByName( 'CB_CODIGO' ).AsInteger ) ) )
                          ) then
                          ZetaDialogo.zInformation(Self.Caption,Format( 'El Empleado %s : %s no se encuentra en el organigrama',[ sKey, sDescription ] ), 0 );
                  end;
             end;
          finally
                 ResetLookupEmpleado;
          end;
     end;
end;

procedure TOrganigrama_DevEx.AplicaTabuladorClick(Sender: TObject);
begin
     inherited;
     //Refresca el salario por tabulador cuando se tiene prendido el checkbox  "usa tabulador = SI"
     if ( ZetaDialogo.ZConfirm('� Atenci�n !', 'Se aplicar� tabulador de salarios a las plazas que corresponda'+CR_LF+'� Desea continuar ?',0,mbOk ) ) then
     begin
          dmRecursos.RefrescaTabuladorPlaza;
     end;
end;

{
procedure TOrganigrama.Agregar;
 var
    iRecordCount: integer;
begin
     with dmCliente do
     begin
          SetLookupEmpleado( eLookEmpPlaza ); //Se usa por que ya tiene el Puesto
          try
             iRecordCount := dmRecursos.cdsPlazas.RecordCount;
             WizPlazas := TWizPlazas.Create(Application);
             try
                WizPlazas.ReportaA := ObtenNodoSeleccionado(Arbol);
                WizPlazas.ShowModal;
                with dmRecursos.cdsPlazas do
                begin
                     if ( iRecordCount <> dmRecursos.cdsPlazas.RecordCount ) then
                     begin
                          DisableControls;
                          try
                             //DoRefresh; Ya se refresc� al grabar
                             self.CrearArbol;
                             Last;
                             UbicaPlaza(Arbol, FieldByName('PL_FOLIO').AsInteger );
                          finally
                                 EnableControls;
                          end;
                     end;
                end;
             finally
                    FreeAndNil(WizPlazas);
             end;
          finally
                 ResetLookupEmpleado;
          end;
     end;
end;

procedure TOrganigrama.Borrar;
var
 oNodo: TTreeNode;
begin
     inherited;
     with dmRecursos.cdsPlazas do
     begin
          DisableControls;
          Arbol.Items.BeginUpdate;
          try
             if( FieldByName('CB_CODIGO').AsInteger = 0 )then //si tiene asignado un titular
             begin
                  if ( dsGrid.DataSet.RecordCount = 0) then //si no tiene subordinados directos
                  begin
                        oNodo:= GetNodo( Arbol, FieldByName( 'PL_FOLIO' ).AsString );
                        if ( oNodo <> NIL ) then
                        begin
                             //Se busca que NINGUN hijo tenga Titular asignado, con uno que lo tenga, no se puede borrar la rama
                             if NOT NodoTieneEmpleado( dmRecursos.cdsPlazas, oNodo, oNodo.GetNextSibling ) then
                             begin
                                  if ZConfirm(Caption, '�Est� seguro de borrar la Plaza?' + CR_LF +
                                                       'Si la Plaza tiene Plazas subordinadas, �stas se borrar�n tambi�n.', 0, mbNo ) then
                                  begin
                                       BorraRama( dmRecursos.cdsPlazas, oNodo, oNodo.GetNextSibling );
                                       Enviar;
                                       Self.Refresh;
                                  end;
                             end
                             else zError(Self.Caption,'No se puede borrar la plaza si tiene subordinados con titular asignado',0);
                        end
                        else zError(Self.Caption,'Error al posicionarse sobre el Nodo seleccionado',0);
                  end
                  else zError(Self.Caption,'No se puede borrar la plaza si tiene subordinados',0);
             end
             else zError(Self.Caption,'No se puede borrar la plaza si tiene titular',0);
          finally
                 Arbol.Items.EndUpdate;
                 EnableControls;
          end;
     end;
end;

procedure TOrganigrama.Modificar;
var
   oNodo, oNodo2: TTreeNode;
   iReportaA: Integer;
   Resultado: string;
begin
     with dmRecursos.cdsPlazas do
     begin
          oNodo := GetNodo( Arbol, FieldByName('PL_FOLIO').AsString);
          iReportaA := FieldByName( 'PL_REPORTA' ).AsInteger;
          if (oNodo <> NIL) then
          begin
               GetNodosHijos( Resultado, oNodo, oNodo.GetNextSibling );
               dmRecursos.FiltroReportaAPlaza := Format( 'NOT ( PL_FOLIO IN ( %s ) )', [Resultado] );
          end;
          try
             Modificar;
          finally
                 dmRecursos.FiltroReportaAPlaza := VACIO;
          end;
          // Refrescar descripcion del nodo
          oNodo2 := GetNodo( Arbol, FieldByName('PL_FOLIO').AsString);
          if (oNodo2 <> NIL) then
          begin
               if ( oNodo = oNodo2 ) and ( iReportaA = FieldByName( 'PL_REPORTA' ).AsInteger ) then
                  oNodo.Text := FieldByName( 'PL_FOLIO' ).AsString + ' = ' + FieldByName( 'PL_NOMBRE' ).AsString
               else
                   self.CrearArbol;
          end
          else
              self.Refresh;
     end;
//     UbicaPlaza(Arbol, iPlazaSel);  // Lo posiciona el refresh
end;

procedure TOrganigrama.CrearArbol;
begin
     InitDatosOrganigrama;
     ZArbolOrganigramaTools.CreaArbol( Arbol, ImageList, ObtenNodoSeleccionado( Arbol ) );
end;

procedure TOrganigrama.ArbolChange(Sender: TObject; Node: TTreeNode);
var
   iPlazaArbol: Integer;
   oCursor : TCursor;
   lExistePlaza: Boolean;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if ( Node <> nil ) then
        begin
             iPlazaArbol:= ObtenNodoSeleccionado( Arbol );
             lExistePlaza := dmRecursos.cdsPlazas.Locate( 'PL_FOLIO', iPlazaArbol, [] );
             RefrescaGrid( iPlazaArbol );
             RefrescaTotalesNodo( Node );
             if lExistePlaza then
             begin
                  PanelInfoEmp.Visible := TRUE;
                  GetFotoEmpleado;
                  PlazaActiva.Enabled := ( dmRecursos.cdsPlazas.FieldByName('CB_CODIGO').AsInteger <> 0 );
             end
             else if ( dmRecursos.cdsPlazas.RecordCount = 0 ) then
             begin
                  ZetaDialogo.zInformation( Self.Caption, Format('No existen plazas creadas',[ iPlazaArbol ]),0 );
             end
             else if (iPlazaArbol <> 0) then
             begin
                  ZetaDialogo.zInformation( Self.Caption, Format('La plaza %d no existe',[ iPlazaArbol ]),0 );
             end
             else
             begin
                  PanelInfoEmp.Visible := FALSE;
                  PlazaActiva.Enabled := FALSE;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TOrganigrama.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( EditOrganigrama );
end;

procedure TOrganigrama.ArbolDblClick(Sender: TObject);
var
   oNodo: TTreeNode;
   iPlazaActual: Integer;
   iPosX, iPosY: Integer;
   sMensaje:string;
begin
     if ( PuedeModificar ( sMensaje ) ) then
     begin
          with Mouse.CursorPos do
          begin
               iPosX := x - Arbol.ClientOrigin.x;
               iPosY := y - Arbol.ClientOrigin.y;
          end;
          oNodo := Arbol.GetNodeAt( iPosX, iPosY );
          if Assigned( oNodo ) and ( oNodo.Level > 0 ) then   // Click sobre una plaza
             self.Modificar     // Ya estar� posicionado el dataset
          else
          begin
               iPlazaActual := ZArbolOrganigramaTools.ObtenPlazaSeleccionada( Arbol );
               if EditOrganigrama = nil then
                   EditOrganigrama := TEditOrganigrama.Create( Self );
               with EditOrganigrama do
               begin
                    PlazaConsulta := iPlazaActual;
                    ShowModal;
                    Self.Refresh;
               end;
          end;
     end
     else zInformation( Caption, 'No tiene permiso para modificar', 0 );
end;

procedure TOrganigrama.PageControlChange(Sender: TObject);
begin
     RefrescaGrid( ObtenNodoSeleccionado( Arbol ) );
end;

procedure TOrganigrama.RefrescaGrid( const iPlaza: Integer );
var
   iRecNo: integer;
   sDescrip: String;
begin
     with dmRecursos do
     begin
          LlenaGridOrganigrama( iPlaza );

          // Nombre del titular
          if ( cdsPlazas.FieldByName('CB_CODIGO').AsInteger > 0 ) then
             ZTITULAR.Caption := dmCliente.cdsEmpleadoLookup.GetDescripcion( cdsPlazas.FieldByName('CB_CODIGO').AsString );
          // Nombre de reporta a
          if cdsPlazasLookup.Lookupkey( cdsPlazas.FieldByName( 'PL_REPORTA' ).AsString, VACIO, sDescrip ) then
             ZREPORTA.Caption := dmCliente.cdsEmpleadoLookup.GetDescripcion( cdsPlazasLookUp.FieldByName( 'CB_CODIGO' ).AsString );

          iRecNo := dsGrid.Dataset.RecordCount;
          gbSubordinados.Caption := ' Subordinados directos: '+IntToStr( iRecNo )+' ';
          EmpleadoActivo.Enabled := ( iRecNo > 0 );
          cdsOrganigrama.ResetDataChange;
     end;
end;

procedure TOrganigrama.GetFotoEmpleado;
begin
     if ( FOTO.Visible ) then
     begin
          with dmRecursos do
          begin
               if ( EmpleadoActive = 0 ) and ( cdsFotoOrganigrama.Active ) then
               begin
                    cdsFotoOrganigrama.EmptyDataSet;
                    FEmpleadoFoto := 0;
               end
               else if ( FEmpleadoFoto <> EmpleadoActive ) then
                    begin
                         GetOrganigramaFoto( EmpleadoActive );
                         FEmpleadoFoto := EmpleadoActive;
                    end;
          end;
     end;
end;

function TOrganigrama.EmpleadoActive: integer;
begin
     Result := dmRecursos.cdsPlazas.FieldByName( 'CB_CODIGO' ).AsInteger;
end;

procedure TOrganigrama.InitDatosOrganigrama;
begin
     FEmpleadoFoto := 0;
end;

procedure TOrganigrama.GridEmpleadosDblClick(Sender: TObject);
begin
     inherited;
     //if ( not UbicaPlaza(Arbol, dsGrid.DataSet.FieldByName('CB_PLAZA').AsInteger ) ) then
     //    ZetaDialogo.zInformation( Self.Caption,'La plaza no tiene asignado un Jefe',0 );
end;

function TOrganigrama.ObtenNodoSeleccionado( oArbol:TTreeView ):Integer;
var
   sNodo: string;
begin
     sNodo := ZArbolOrganigramaTools.ObtenNodoSeleccionado( oArbol );
     if sNodo = VACIO then
     begin
          Result := 0;
     end
     else
         Result := StrToInt( sNodo );
end;
}



procedure TOrganigrama_DevEx.ZetaDBGridDBTableViewColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  inherited;
  {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
  self.AColumn := TcxGridDBColumn(AColumn);
end;

procedure TOrganigrama_DevEx.ZetaDBGridDBTableViewDataControllerSortingChanged(
  Sender: TObject);
begin
  inherited;
  {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
  ZetaDBGrid.OrdenarPor( AColumn , TZetaClientDataset(dsGrid.DataSet) );
end;

procedure TOrganigrama_DevEx.ZetaDBGridDBTableViewDblClick(
  Sender: TObject);
begin
  inherited;
  DoEdit;
end;

procedure TOrganigrama_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  //Desactiva la posibilidad de agrupar
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
  //Esconde la caja de agrupamiento
  ZetaDBGridDBTableView.OptionsView.GroupByBox := False;
  //Para que nunca muestre el filterbox inferior
  ZetaDBGridDBTableView.OptionsCustomize.ColumnFiltering := False;
  ZetaDBGridDBTableView.FilterBox.Visible := fvNever;
  //Para que no aparezca el Custom Dialog
  ZetaDBGridDBTableView.FilterBox.CustomizeDialog := False;
  //Aplica el Width ideal a las columnas del grid
  ApplyMinWidth;
  //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
  ZetaDBGridDBTableView.ApplyBestFit();

end;

procedure TOrganigrama_DevEx._A_ImprimirExecute(Sender: TObject);
begin
  inherited;
  DoPrint;
end;

procedure TOrganigrama_DevEx._E_AgregarExecute(Sender: TObject);
begin
  inherited;
  DoInsert;
end;

procedure TOrganigrama_DevEx._E_BorrarExecute(Sender: TObject);
begin
  inherited;
  DoDelete;
end;

procedure TOrganigrama_DevEx._E_ModificarExecute(Sender: TObject);
begin
  inherited;
  DoEdit;
end;

procedure TOrganigrama_DevEx.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =40;
begin
     with  ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;

end;

procedure TOrganigrama_DevEx.ZetaDBGridDBTableViewKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
       DoEdit;
  end;
end;

end.

