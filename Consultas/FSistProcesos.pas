unit FSistProcesos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, ExtCtrls, Grids,
     DBGrids, StdCtrls, Buttons, DBCtrls, Mask, ComCtrls,
     ZetaMessages,
     ZBaseConsulta,
     ZetaDBGrid,
     ZetaKeyCombo,
     ZetaKeyLookup,
     ZetaFecha;

type
  TSistProcesos = class(TBaseConsulta)
    PanelProcesos: TPanel;
    ProcessLBL: TLabel;
    ProcessStartLBL: TLabel;
    ProcessEndLBL: TLabel;
    ProcessUserLBL: TLabel;
    FechaInicial: TZetaFecha;
    FechaFinal: TZetaFecha;
    Usuario: TZetaKeyLookup;
    RefrescarProcesos: TBitBtn;
    Process: TZetaKeyCombo;
    ProcessGrid: TZetaDBGrid;
    StatusLBL: TLabel;
    Status: TComboBox;
    Cancelar: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure RefrescarProcesosClick(Sender: TObject);
    procedure ProcessGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure StatusChange(Sender: TObject);
    procedure ProcessChange(Sender: TObject);
    procedure UsuarioValidLookup(Sender: TObject);
  private
    { Private declarations }
    FPuedeCancelar: Boolean;
    procedure WMFocus(var Message: TMessage); message WM_FOCUS;
    procedure WMDefocus(var Message: TMessage); message WM_DEFOCUS;
  protected
    { Protected declarations }
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
    procedure Connect; override;
    procedure Disconnect; override;
    procedure Refresh; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  SistProcesos: TSistProcesos;

implementation

uses DConsultas,
     DSistema,
     {$ifdef ADUANAS}
     ZHelpContext,
     {$ENDIF}
     ZetaDialogo,
     ZetaClientTools,
     ZetaCommonLists,
     ZetaCommonClasses;

{$R *.DFM}

procedure TSistProcesos.FormCreate(Sender: TObject);
begin
     FechaInicial.Valor := Date();
     FechaFinal.Valor := Date();
     inherited;
     {$ifdef ADUANAS}
     HelpContext := H_PROCESOS_DEL_SISTEMA;
     {$ELSE}
     HelpContext := H60655_Procesos_Abiertos;
     {$ENDIF}
     {$ifdef WORKFLOWCFG}
     HelpContext := H_WORKFLOWCFG_CONSULTAS_PROCESOS;
     {$endif}
     Status.ItemIndex := 0;
     with Process do
     begin
          dmConsultas.LlenaListaProcesos( Lista );
          ItemIndex := 0;
     end;
     Usuario.LookupDataset := dmSistema.cdsUsuarios;
end;

procedure TSistProcesos.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmConsultas do
     begin
          DataSource.DataSet := cdsProcesos;
     end;
     DoRefresh;
     FPuedeCancelar := CheckDerechos( K_DERECHO_BAJA );
end;

procedure TSistProcesos.Disconnect;
begin
     inherited Disconnect;
end;

procedure TSistProcesos.Refresh;
begin
     with dmConsultas do
     begin
          RefrescarListaProcesos( Status.ItemIndex, Process.LlaveEntero, Usuario.Valor, FechaInicial.Valor, FechaFinal.Valor );
     end;
end;

function TSistProcesos.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Agregar Un Proceso';
end;

function TSistProcesos.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Permite Borrar Procesos';
end;

function TSistProcesos.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := True; { Siempre se permite ya que la interfase de usuario no permite cambios }
end;

procedure TSistProcesos.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     Cancelar.Enabled := FPuedeCancelar and dmConsultas.ProcesoEstaAbierto;
end;

procedure TSistProcesos.CancelarClick(Sender: TObject);
begin
     inherited;
     dmConsultas.CancelarProceso;
     DoRefresh;
end;

procedure TSistProcesos.Modificar;
begin
     inherited;
     dmConsultas.cdsProcesos.Modificar;
end;

procedure TSistProcesos.RefrescarProcesosClick(Sender: TObject);
begin
     inherited;
     DoRefresh;
     ProcessGrid.SetFocus;
end;

procedure TSistProcesos.ProcessGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     with ProcessGrid do
     begin
          if ( gdSelected in State )  then
          begin
               with Canvas do
               begin
                    Font.Color := clWhite;
                    Brush.Color := clNavy;
               end;
          end
          else
          begin
               with Canvas do
               begin
                    Brush.Color := clWhite;
                    with Font do
                    begin
                         case dmConsultas.GetStatusProceso of
                              epsEjecutando: Color := clGreen;
                              epsCancelado: Color := clFuchsia;
                              epsError: Color := clRed;
                         else
                             Color := ProcessGrid.Font.Color;
                         end;
                    end;
               end;
          end;
          DefaultDrawDataCell( Rect, Column.Field, State );
     end;
end;

procedure TSistProcesos.StatusChange(Sender: TObject);
begin
     inherited;
     DoRefresh;
end;

procedure TSistProcesos.ProcessChange(Sender: TObject);
begin
     inherited;
     DoRefresh;
end;

procedure TSistProcesos.UsuarioValidLookup(Sender: TObject);
begin
     inherited;
     DoRefresh;
end;

procedure TSistProcesos.WMDefocus(var Message: TMessage);
begin
     RefrescarProcesos.Default := False;
end;

procedure TSistProcesos.WMFocus(var Message: TMessage);
begin
     RefrescarProcesos.Default := True;
end;

end.
