unit FQueryGral_DevEx;

interface

{  Folio      :
   Prop�sito  :
   Creaci�n   :
   Responsable:
   Calculados : No
   Lista Calculados[] }

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, Grids, ExtCtrls, Db, DBGrids,
     ZBaseConsulta,
     ZetaDBGrid, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinsDefaultPainters, 
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ZetaCXGrid, Menus, ActnList, ImgList, TressMorado2013, cxButtons, cxContainer,
  cxTextEdit, cxMemo, System.Actions;

type
  TQueryGral_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Memo: TcxMemo;
    AbrirQueryDlg: TOpenDialog;
    GuardarQueryDlg: TSaveDialog;
    Panel2: TPanel;
    Splitter1: TSplitter;
    GuardarSqlBtn_DevEx: TcxButton;
    AbrirSqlBtn_DevEx: TcxButton;
    NuevoSqlBtn_DevEx: TcxButton;
    EjecutaBtn_DevEx: TcxButton;
    SiguienteBtn_DevEx: TcxButton;
    AnteriorBtn_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure GuardarSqlBtnClick(Sender: TObject);
    procedure AbrirSqlBtnClick(Sender: TObject);
    procedure NuevoSqlBtnClick(Sender: TObject);
    procedure AnteriorBtnClick(Sender: TObject);
    procedure SiguienteBtnClick(Sender: TObject);
    procedure EjecutaBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    ListaSQLS: TStringList;
    ListaActiva: Integer;
    procedure AddSql;
    procedure ShowBotones;
    procedure AsignaAnterior;
    procedure AsignaSiguiente;
    procedure EjecutaQuery;
    function SQL_Injection (sScript: String): Boolean;
//    procedure ValidaNivel0(const sSQL, sNivel: String);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
  public
    { Public declarations}
  end;

var
  QueryGral_DevEx: TQueryGral_DevEx;

  aSQL_INJECTION: array[ 0..11 ] of String =
       ('ADD','ALTER','CREATE','DELETE','DROP','EXEC','EXECUTE','FUNCTION','INSERT','RECONFIGURE','TABLE','UPDATE');

implementation

uses DConsultas,
     DCliente,
     {$ifdef ADUANAS}
     ZHelpContext,
     {$ENDIF}
     ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonClasses;

{$R *.DFM}

procedure TQueryGral_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ListaSQLS := TStringList.Create;
     {$ifdef ADUANAS}
     HelpContext := H_PESTANA_DE_CONSULTAS_EN_SQL;
     {$ELSE}
     HelpContext := H50054_SQL_DevEx;
     //HelpContext := H50054_SQL; //old
     {$ENDIF}
     {$ifdef WORKFLOWCFG}
     HelpContext := H_WORKFLOWCFG_CONSULTAS_SQL;
     {$endif}
end;

procedure TQueryGral_DevEx.FormDestroy(Sender: TObject);
var
   i:Integer;
begin
     with ListaSQLS do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               TStringList( Objects[ i ] ).Free;
          end;
          Free;
     end;
     inherited;
end;

procedure TQueryGral_DevEx.Connect;
begin
    DataSource.DataSet := dmConsultas.cdsQueryGeneral;

end;


procedure TQueryGral_DevEx.Refresh;
var
   sSQL: String;
   {*** US 11231: La opci�n de Consultas | SQL permite utilizar comandos Delete y Update ***}
   function ImpedirEjecucionSql( sSQL: TStrings ): Boolean;
   var
      i: Integer;
      lExiste: Boolean;
   begin
        Result := False;
        for i := 0 to sSQL.Count-1 do
        begin
             lExiste := ( Ansipos( 'UPDATE', sSQL.ToStringArray[i] ) > 0 ) or ( Ansipos( 'DELETE', sSQL.ToStringArray[i] ) > 0 ) or ( Ansipos( 'EXECUTE', sSQL.ToStringArray[i] ) > 0 );
             if lExiste then
             begin
                  Result := TRUE;
                  Exit;
             end;
        end;
   end;
begin
     if not ImpedirEjecucionSql( Memo.Lines ) then
     begin
          sSQL := UpperCase( Trim( Memo.Lines.Text ) );
          if ZetaCommonTools.StrLleno( sSQL ) then
          begin
               if ( Pos( 'SELECT', sSQL ) = 1 ) then
               begin
                    if (not SQL_Injection (sSQL)) then
                    begin
                         //ValidaNivel0( sSQL, dmCliente.Confidencialidad );
                         with dmConsultas do
                         begin
                              SQLText := Memo.Lines.Text;
                              cdsQueryGeneral.Refrescar;
                              // DataSource.DataSet := cdsQueryGeneral;
                              Connect;

                              ZetaDBGridDBTableView.ClearItems;
                              if ( ZetaDBGridDBTableView.ItemCount = 0 ) then
                                 ZetaDBGridDBTableView.DataController.CreateAllItems();
                              if ( ZetaDBGridDBTableView.ItemCount > 0 ) then
                                 CreaColumaSumatoria(ZetaDBGridDBTableView.Columns[0],0,'',SkCount);
                         end;
                    end
                    else
                        ZetaDialogo.zError( Caption, 'Script Inv�lido: Solo puede utilizar un comando SELECT.', 0 );
               end
               else
                   ZetaDialogo.zError( Caption, 'Comando no soportado.', 0 );
          end
     end
     else
     begin
          ZetaDialogo.zError( Caption, 'Comando no soportado.', 0 );
     end;
end;

procedure TQueryGral_DevEx.EjecutaBtnClick(Sender: TObject);
begin
     inherited;
     {***DevEx(@am): La sig. linea se incluyo dentro del metodo Refresh para que tmb.
                     se ejecute la consulta aon F5. De esta forma se dio solucion a los bugs 5415 y 5589
                     del proyecto V 2014. ***}
     //ZetaDBGridDBTableView.ClearItems;
     EjecutaQuery;
     {***DevEx(@am): La sig. linea se incluyo dentro del metodo Refresh para que tmb.
                     se ejecute la consulta aon F5. De esta forma se dio solucion a los bugs 5415 y 5589
                     del proyecto V 2014. ***}
     //ZetaDBGridDBTableView.DataController.CreateAllItems();
end;

procedure TQueryGral_DevEx.EjecutaQuery;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           Refresh;
           AddSQL;
        except
              on Error: Exception do
              begin
                   ZetaDialogo.zExcepcion( Caption, 'Error al ejecutar la consulta.', Error, 0 );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TQueryGral_DevEx.GuardarSqlBtnClick(Sender: TObject);
begin
     inherited;
     with GuardarQueryDlg do
     begin
          Execute;
          if ZetaCommonTools.StrLleno( FileName ) then
          begin
               Memo.Lines.SaveToFile( FileName );
          end;
     end;
end;

procedure TQueryGral_DevEx.AbrirSqlBtnClick(Sender: TObject);
begin
     inherited;
     with AbrirQueryDlg do
     begin
          Execute;
          if ZetaCommonTools.StrLleno( FileName ) then
          begin
               Memo.Clear;
               Memo.Lines.LoadFromFile( FileName );
          end;
     end;
end;

procedure TQueryGral_DevEx.NuevoSqlBtnClick(Sender: TObject);
begin
     inherited;
     Memo.Clear;
     //DevEx: Limpiar las columnas del grid
     ZetaDBGridDBTableView.ClearItems;
     {if DbGrid.DataSource.Dataset <> NIL then
        DbGrid.DataSource.DataSet.Close;}
     //DevEx
     if ZetaDBGridDBTableView.DataController.DataSource.DataSet <> NIL then
        ZetaDBGridDBTableView.DataController.DataSource.DataSet.Close;
end;

procedure TQueryGral_DevEx.AddSql;
var
   i: Integer;
   lExiste: Boolean;
   oLista: TStringList;
begin
     lExiste := False;
     for i := 0 to ListaSQLS.Count-1 do
     begin
          lExiste := ( Memo.Lines.Equals( TStrings( ListaSQLS.Objects[ i ] ) ) );
          if lExiste then
             Exit;
     end;
     if not lExiste then
     begin
          oLista := TStringList.Create;
          oLista.Assign( Memo.Lines );
          ListaSQLS.AddObject( '', oLista );
          ListaActiva := ListaSQLS.Count - 1;
          ShowBotones;
     end;
end;

procedure TQueryGral_DevEx.ShowBotones;
begin
     if ( ListaSQLS.Count > 0 ) then
     begin
          AnteriorBtn_DevEx.Enabled := not ( ListaActiva = 0 );
          SiguienteBtn_DevEx.Enabled := not ( ListaActiva = ListaSQLS.Count - 1 );
     end;
end;

procedure TQueryGral_DevEx.AnteriorBtnClick(Sender: TObject);
begin
     inherited;
     AsignaAnterior;
end;

procedure TQueryGral_DevEx.SiguienteBtnClick(Sender: TObject);
begin
     inherited;
     AsignaSiguiente;
end;

procedure TQueryGral_DevEx.AsignaAnterior;
var
   i: Integer;
begin
     i := ListaActiva - 1;
     if ( i >= 0 ) then
     begin
          with Memo.Lines do
          begin
               BeginUpdate;
               Assign( TStrings( ListaSQLS.Objects[ i ] ) );
               EndUpdate;
          end;
          ListaActiva := i;
          ShowBotones;
     end;
end;

procedure TQueryGral_DevEx.AsignaSiguiente;
var
   i: Integer;
begin
     i := ListaActiva + 1;
     if ( i <= ListaSQLS.Count - 1 ) then
     begin
          with Memo.Lines do
          begin
               BeginUpdate;
               Assign( TStrings( ListaSQLS.Objects[ i ] ) );
               EndUpdate;
          end;
          ListaActiva := i;
          ShowBotones;
     end;
end;


function TQueryGral_DevEx.SQL_Injection (sScript: String): Boolean;
var
  i, j: Integer;
  slScript: TStringList;

  function MantenerAlfanumericos(const str: string): string;
  var
     i, Count: Integer;
  begin
       SetLength(Result, Length(str));
       Count := 0;
       for i := 1 to Length(str) do
       begin
            if (CharInSet ( str[i], ['a'..'z', 'A'..'Z', '0'..'9'] ) ) then
            begin
                 inc(Count);
                 Result[Count] := str[i];
            end
            else
            begin
                 inc(Count);
                 Result[Count] := ' ';
            end;
       end;
       SetLength(Result, Count);
  end;

begin
     Result := FALSE;
     slScript := TStringList.Create;

     try
        slScript.Delimiter := ' ';
        // slScript.DelimitedText := sScript;
        slScript.DelimitedText := MantenerAlfanumericos (sScript);

        for i := ( Low( aSQL_INJECTION ) ) to High( aSQL_INJECTION ) do
        begin
             for j := 0 to (slScript.Count - 1) do
             begin
                  // slScript[j] :=  MantenerAlfanumericos (slScript[j] );
                  if UpperCase (aSQL_INJECTION[i]) = UpperCase (slScript[j] )then
                  begin
                       RESULT := TRUE;
                       break;
                  end;
             end;
        end;
     finally
            FreeAndNil (slScript);
     end;
end;

{procedure TQueryGral.ValidaNivel0( const sSQL, sNivel : String );

    function TablasEmpleado : Boolean;
    const
        K_MAX_TABLAS = 45;
        aTablas : array[ 0..K_MAX_TABLAS ] of Pchar =
            ( 'COLABORA','REP_PTU','REP_AHO','AGUINAL','KARDEX','TMPHORAS','TMPEVENT',
              'VACACION','ANTESCUR','ANTESPTO','PARIENTE','KAR_FIJA','NOMINA',
              'MOVIMIEN','LIQ_EMP','LIQ_MOV','PERMISO','FALTAS','PRESTAMO','KARCURSO',
              'INCAPACI','AUSENCIA','CHECADAS','AHORRO','ACAR_ABO','ACUMULA','PCAR_ABO',
              'IMAGEN','BITACORA','TMPBALAN','TMPESTAD','TMPSALAR','TMPMOVS','COMPARA',
              'DECLARA','TMPROTAI','INVITA','CAF_COME','ASIGNA','TMPCALEN','TMPLISTA',
              'TMPFOLIO','TMPNOM','TMPCALHR','ALTABAJA','CUR_PROG');
    var
        i : Integer;
    begin
        Result := FALSE;
        i := 0;
        while ( not Result ) and ( i < K_MAX_TABLAS ) do
        begin
            Result := Pos( aTablas[ i ], sSQL ) > 0;
            Inc( i );
        end;
    end;

    procedure RevisaFiltro;
    var
        sFiltro : String;
    begin
        sFiltro := Format( 'COLABORA.CB_NIVEL0=''%s''', [ sNivel ] );
        //if Pos( sFiltro, sSQL ) = 0 then
           // DataBaseError( 'Falta Incluir Filtro: ' + sFiltro );
    end;

begin  // ValidaNivel0
    //sSQL := UpperCase( sSQL );
    if strLleno( sNivel ) and TablasEmpleado then
       RevisaFiltro;
end;
 }
procedure TQueryGral_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  //Permite agrupar por columnas
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
   //Visualiza la caja de agrupamiento
  ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
  //Para que nunca muestre el filterbox inferior
  ZetaDBGridDBTableView.FilterBox.Visible := fvNever;
  //Para que no aparezca el Custom Dialog
  ZetaDBGridDBTableView.FilterBox.CustomizeDialog := False;
end;

function TQueryGral_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No se permite agregar en '+ self.Caption +'.';
end;

function TQueryGral_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No se permite borrar en '+ self.Caption +'.';
end;

function TQueryGral_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No se permite modificar en '+ self.Caption +'.';
end;
end.
