unit FEditPresup_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, ZetaDBTextBox, StdCtrls, Db, ExtCtrls, DBCtrls, Buttons,
  Mask, ZetaNumero, ZetaEdit, ComCtrls, ZetaSmartLists, 
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxSkinscxPCPainter, cxPCdxBarPopupMenu,
  cxPC, dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator,
  cxDBNavigator, cxButtons;

type
  TEditPresup_DevEx = class(TBaseEdicion_DevEx)
    Panel5: TPanel;
    CT_NIVEL5lbl: TLabel;
    CT_NIVEL_5: TZetaDBTextBox;
    TB_ELEMENT: TZetaDBTextBox;
    Panel4: TPanel;
    CT_NIVEL4lbl: TLabel;
    CT_NIVEL_4: TZetaDBTextBox;
    TB_ELEMENT4: TZetaDBTextBox;
    Panel3: TPanel;
    CT_NIVEL3lbl: TLabel;
    CT_NIVEL_3: TZetaDBTextBox;
    TB_ELEMENT3: TZetaDBTextBox;
    Panel2: TPanel;
    CT_NIVEL2lbl: TLabel;
    CT_NIVEL_2: TZetaDBTextBox;
    TB_ELEMENT2: TZetaDBTextBox;
    Panel1: TPanel;
    CT_NIVEL1lbl: TLabel;
    CT_NIVEL_1: TZetaDBTextBox;
    TB_ELEMENT1: TZetaDBTextBox;
    PageControl_DevEx: TcxPageControl;
    Conteo_DevEx: TcxTabSheet;
    Adicionales_DevEx: TcxTabSheet;
    PanelCaptura: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    CT_DIFERENCIA: TZetaDBTextBox;
    Label3: TLabel;
    ZetaDBTextBox1: TZetaDBTextBox;
    Label4: TLabel;
    Label5: TLabel;
    CT_CUANTOS: TZetaDBNumero;
    ZetaDBNumero1: TZetaDBNumero;
    ZetaDBNumero2: TZetaDBNumero;
    GBNumeroAdic: TGroupBox;
    CT_NUMERO1: TZetaDBNumero;
    CT_NUMERO2: TZetaDBNumero;
    CT_NUMERO3: TZetaDBNumero;
    GBTextoAdic: TGroupBox;
    CT_TEXTO1: TZetaDBEdit;
    CT_TEXTO2: TZetaDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure CT_CUANTOSExit(Sender: TObject);
  private
    { Private declarations }
    procedure SetCamposNivel;
  protected
    { Protected declarations }
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Connect; override;
    procedure EscribirCambios; override;
    procedure DoCancelChanges; override;
  public
    { Public declarations }
  end;

var
  EditPresup_DevEx: TEditPresup_DevEx;

implementation

uses dConteo, ZAccesosTress, ZGlobalTress, ZetaCommonClasses;

{$R *.DFM}

procedure TEditPresup_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     //IndexDerechos := D_ASIS_DATOS_PRENOMINA;
     IndexDerechos := D_CONS_CONTEO_PERSONAL;
     HelpContext := H50552_Edicion_del_presupuesto;
     FirstControl := CT_CUANTOS;
     SetCamposNivel;
     with PageControl_DevEx do
          ActivePage := Pages[0];
end;

procedure TEditPresup_DevEx.Connect;
begin
     with dmConteo do
     begin
          cdsConteo.Conectar;
          DataSource.DataSet := cdsConteo;
     end;
end;

procedure TEditPresup_DevEx.Agregar;
begin
     { No debe Hacer nada }
end;

procedure TEditPresup_DevEx.Borrar;
begin
     { No debe Hacer nada }
end;

procedure TEditPresup_DevEx.EscribirCambios;
begin
     ClientDataset.Post;
//     dmConteo.RefreshTotales := TRUE;
end;

procedure TEditPresup_DevEx.DoCancelChanges;
begin
     ClientDataset.Cancel;        // Si se deja la herencia cancela todos los post que se hayan hecho
end;

procedure TEditPresup_DevEx.SetCamposNivel;

   procedure SetActivaControles( oPanel: TPanel; oLabel: TLabel; oControl: TZetaDBTextBox;
             const iNivel: Integer );
   begin
        with dmConteo do
        begin
             oPanel.Visible := ( iNivel <= NumNiveles );
             if oPanel.Visible then
             begin
                  oLabel.Caption := GetDescripConteoNiveles( iNivel - 1 ) + ':';
                  oControl.DataField := oControl.Name;
             end
             else
                self.Height := self.Height - 21;
        end;
   end;

begin
     SetActivaControles( Panel1, CT_NIVEL1lbl, TB_ELEMENT1, 1 );
     SetActivaControles( Panel2, CT_NIVEL2lbl, TB_ELEMENT2, 2 );
     SetActivaControles( Panel3, CT_NIVEL3lbl, TB_ELEMENT3, 3 );
     SetActivaControles( Panel4, CT_NIVEL4lbl, TB_ELEMENT4, 4 );
     SetActivaControles( Panel5, CT_NIVEL5lbl, TB_ELEMENT, 5 );
end;

procedure TEditPresup_DevEx.CT_CUANTOSExit(Sender: TObject);
begin
     inherited;
     if ( not ( ActiveControl = Cancelar_DevEx ) ) and ( CT_CUANTOS.ValorEntero < 0 ) then
     begin
          ActiveControl := CT_CUANTOS;
          DataBaseError( 'Presupuesto debe Ser Igual o Mayor a Cero' );
     end;
end;

end.
