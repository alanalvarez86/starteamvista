unit FSistBitacora_DevEx;

interface

{$INCLUDE DEFINES.INC}

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Grids, DBGrids, ZetaDBGrid, ComCtrls,
  StdCtrls, Mask, ZetaFecha, ZetaKeyLookup_DevEx, Buttons, ZetaKeyCombo, ExtCtrls,
  ZetaCommonClasses, FBaseSistBitacora_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ZetaCXGrid, TressMorado2013, Menus, cxButtons,
  ActnList, ImgList;

type
  TSistBitacora_DevEx = class(TBaseSistBitacora_DevEx)
    LblEmpleado: TLabel;
    Empleado: TZetaKeyLookup_DevEx;
    CB_CODIGO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure SetFormaSuper(const lModoSuper: Boolean);
  protected
    procedure Connect; override;
    procedure Modificar; override;
    procedure RefrescaDatos; override;
    procedure LlenaClaseBit; override;
    procedure DefineColAgrupacion; override;
  public
    { Public declarations }
  end;

var
  SistBitacora_DevEx: TSistBitacora_DevEx;

implementation

{$R *.dfm}

uses DSistema,
     DCliente,
     DConsultas,
     {$IFDEF ADUANAS}
     ZHelpContext,
     {$endif}
     ZetaCommonLists,
     ZetaCommonTools;

procedure TSistBitacora_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$IFDEF ADUANAS}
     HelpContext := H_BITACORA;
     {$ELSE}
     if dmCliente.ModoSuper then
        HelpContext := 60001
     else
        HelpContext := H60652_Bitacora_DevEx;
        //HelpContext := H60652_Bitacora;
     {$ENDIF}
     {$ifdef WORKFLOWCFG}
     HelpContext := H_WORKFLOWCFG_CONSULTAS_BITACORA;
     {$endif}

     {$ifdef SELECCION}
     Empleado.Visible := FALSE;
     LblEmpleado.Visible := FALSE;
     //ZetaDBGrid1.Columns[5].Visible := FALSE;
     //DevEx
     ZetaDBGridDBTableView.Columns[ 5 ].Visible := FALSE;
     {$endif}
     PosCol:= 6;

end;

procedure TSistBitacora_DevEx.FormShow(Sender: TObject);
begin
     SetFormaSuper( dmCliente.ModoSuper );
     inherited;
end;

procedure TSistBitacora_DevEx.Connect;
begin
     inherited;
     {$ifdef TRESS}
     {$ifndef RDD}
     dmCliente.cdsEmpleadoLookUp.Conectar;
     {$endif}
     {$endif}
     with dmConsultas do
     begin
          DataSource.DataSet := cdsBitacora;
     end;
end;

procedure TSistBitacora_DevEx.RefrescaDatos;
begin
     with FParametros do
     begin
          AddInteger( 'Empleado', Empleado.Valor );
     end;
     dmConsultas.RefrescarBitacora( FParametros );
end;

procedure TSistBitacora_DevEx.Modificar;
begin
     with dmConsultas do
     begin
          cdsBitacora.Modificar;
     end;
end;
procedure TSistBitacora_DevEx.DefineColAgrupacion;
begin
     inherited;
     //Establece las columnas por las cuales no se podra agrupar.
     ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO').Options.Grouping := False;
end;
procedure TSistBitacora_DevEx.LlenaClaseBit;
var
   i: integer;
begin
     with ClaseBit do
     begin
          Sorted := FALSE;
          for i:= 0 to Ord(High(eClaseBitacora)) do
              Items.AddObject( ObtieneElemento(lfClaseBitacora,i), TObject( i ) );

          ItemIndex := 0;
          Sorted := TRUE;
     end;
end;

procedure TSistBitacora_DevEx.SetFormaSuper( const lModoSuper: Boolean );
begin
     Usuario.Enabled := not lModoSuper;
     lbUsuario.Enabled := not lModoSuper;
     if lModoSuper then
     begin
          Usuario.Valor := dmCliente.Usuario;
          FNumProceso := -1;
          {with ZetaDBGrid1.Columns[ 3 ] do
          begin
               FieldName := 'BI_CLASE';
               Title.Caption := 'Clase';
               Width := 100;
          end;}
          //DevEx
          with  ZetaDBGridDBTableView.Columns[ 3 ] do
          begin
               DataBinding.FieldName:= 'BI_CLASE';
               Caption := 'Clase';
               Width := 100;
          end;
     end;
end;

end.
