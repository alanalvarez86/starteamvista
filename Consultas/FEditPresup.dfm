inherited EditPresup: TEditPresup
  Left = 308
  Top = 189
  Caption = 'Editar Presupuesto'
  ClientHeight = 320
  ClientWidth = 458
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 284
    Width = 458
    inherited OK: TBitBtn
      Left = 290
    end
    inherited Cancelar: TBitBtn
      Left = 375
    end
  end
  inherited PanelSuperior: TPanel
    Width = 458
    inherited AgregarBtn: TSpeedButton
      Visible = False
    end
    inherited BorrarBtn: TSpeedButton
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 458
    inherited ValorActivo2: TPanel
      Width = 132
    end
  end
  object PageControl: TPageControl [3]
    Left = 0
    Top = 156
    Width = 458
    Height = 128
    ActivePage = Conteo
    Align = alClient
    TabOrder = 3
    object Conteo: TTabSheet
      Caption = 'Conteo'
      object PanelCaptura: TPanel
        Left = 0
        Top = 0
        Width = 450
        Height = 100
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 59
          Top = 12
          Width = 62
          Height = 13
          Alignment = taRightJustify
          Caption = 'Presupuesto:'
        end
        object Label2: TLabel
          Left = 96
          Top = 36
          Width = 25
          Height = 13
          Caption = 'Real:'
        end
        object CT_DIFERENCIA: TZetaDBTextBox
          Left = 127
          Top = 56
          Width = 60
          Height = 21
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'CT_DIFERENCIA'
          ShowAccelChar = False
          Brush.Color = clSilver
          Border = False
          DataField = 'VACANTES'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label3: TLabel
          Left = 73
          Top = 60
          Width = 48
          Height = 13
          Caption = 'Vacantes:'
        end
        object ZetaDBTextBox1: TZetaDBTextBox
          Left = 127
          Top = 80
          Width = 60
          Height = 21
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ZetaDBTextBox1'
          ShowAccelChar = False
          Brush.Color = clSilver
          Border = False
          DataField = 'EXCEDENTE'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label4: TLabel
          Left = 67
          Top = 84
          Width = 54
          Height = 13
          Caption = 'Excedente:'
        end
        object Label5: TLabel
          Left = 257
          Top = 36
          Width = 8
          Height = 13
          Caption = '%'
        end
        object CT_CUANTOS: TZetaDBNumero
          Left = 127
          Top = 8
          Width = 61
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
          UseEnterKey = True
          OnExit = CT_CUANTOSExit
          DataField = 'CT_CUANTOS'
          DataSource = DataSource
        end
        object ZetaDBNumero1: TZetaDBNumero
          Left = 127
          Top = 32
          Width = 61
          Height = 21
          TabStop = False
          Color = clBtnFace
          Mascara = mnEmpleado
          ReadOnly = True
          TabOrder = 1
          DataField = 'CT_REAL'
          DataSource = DataSource
        end
        object ZetaDBNumero2: TZetaDBNumero
          Left = 191
          Top = 32
          Width = 61
          Height = 21
          TabStop = False
          Color = clBtnFace
          Mascara = mnPesos
          ReadOnly = True
          TabOrder = 2
          Text = '0.00'
          DataField = 'CT_TASA'
          DataSource = DataSource
        end
      end
    end
    object Adicionales: TTabSheet
      Caption = 'Adicionales'
      ImageIndex = 1
      object GBTextoAdic: TGroupBox
        Left = 24
        Top = 6
        Width = 265
        Height = 81
        Caption = ' Textos '
        TabOrder = 0
        object CT_TEXTO1: TZetaDBEdit
          Left = 8
          Top = 18
          Width = 241
          Height = 21
          TabOrder = 0
          Text = 'CT_TEXTO1'
          DataField = 'CT_TEXTO1'
          DataSource = DataSource
        end
        object CT_TEXTO2: TZetaDBEdit
          Left = 8
          Top = 48
          Width = 241
          Height = 21
          TabOrder = 1
          Text = 'CT_TEXTO2'
          DataField = 'CT_TEXTO2'
          DataSource = DataSource
        end
      end
      object GBNumeroAdic: TGroupBox
        Left = 304
        Top = 6
        Width = 121
        Height = 97
        Caption = ' N�meros '
        TabOrder = 1
        object CT_NUMERO1: TZetaDBNumero
          Left = 16
          Top = 18
          Width = 89
          Height = 21
          Mascara = mnPesos
          TabOrder = 0
          Text = '0.00'
          DataField = 'CT_NUMERO1'
          DataSource = DataSource
        end
        object CT_NUMERO2: TZetaDBNumero
          Left = 16
          Top = 44
          Width = 89
          Height = 21
          Mascara = mnPesos
          TabOrder = 1
          Text = '0.00'
          DataField = 'CT_NUMERO2'
          DataSource = DataSource
        end
        object CT_NUMERO3: TZetaDBNumero
          Left = 16
          Top = 70
          Width = 89
          Height = 21
          Mascara = mnPesos
          TabOrder = 2
          Text = '0.00'
          DataField = 'CT_NUMERO3'
          DataSource = DataSource
        end
      end
    end
  end
  object Panel5: TPanel [4]
    Left = 0
    Top = 135
    Width = 458
    Height = 21
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object CT_NIVEL5lbl: TLabel
      Left = 88
      Top = 4
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nivel5:'
    end
    object CT_NIVEL_5: TZetaDBTextBox
      Left = 127
      Top = 2
      Width = 60
      Height = 17
      AutoSize = False
      Caption = 'CT_NIVEL_5'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataField = 'CT_NIVEL_5'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object TB_ELEMENT: TZetaDBTextBox
      Left = 194
      Top = 2
      Width = 230
      Height = 17
      AutoSize = False
      Caption = 'TB_ELEMENT'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  object Panel4: TPanel [5]
    Left = 0
    Top = 114
    Width = 458
    Height = 21
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 5
    object CT_NIVEL4lbl: TLabel
      Left = 88
      Top = 4
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nivel4:'
    end
    object CT_NIVEL_4: TZetaDBTextBox
      Left = 127
      Top = 2
      Width = 60
      Height = 17
      AutoSize = False
      Caption = 'CT_NIVEL_4'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataField = 'CT_NIVEL_4'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object TB_ELEMENT4: TZetaDBTextBox
      Left = 194
      Top = 2
      Width = 230
      Height = 17
      AutoSize = False
      Caption = 'TB_ELEMENT4'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  object Panel3: TPanel [6]
    Left = 0
    Top = 93
    Width = 458
    Height = 21
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 6
    object CT_NIVEL3lbl: TLabel
      Left = 88
      Top = 4
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nivel3:'
    end
    object CT_NIVEL_3: TZetaDBTextBox
      Left = 127
      Top = 2
      Width = 60
      Height = 17
      AutoSize = False
      Caption = 'CT_NIVEL_3'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataField = 'CT_NIVEL_3'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object TB_ELEMENT3: TZetaDBTextBox
      Left = 194
      Top = 2
      Width = 230
      Height = 17
      AutoSize = False
      Caption = 'TB_ELEMENT3'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  object Panel2: TPanel [7]
    Left = 0
    Top = 72
    Width = 458
    Height = 21
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 7
    object CT_NIVEL2lbl: TLabel
      Left = 88
      Top = 4
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nivel2:'
    end
    object CT_NIVEL_2: TZetaDBTextBox
      Left = 127
      Top = 2
      Width = 60
      Height = 17
      AutoSize = False
      Caption = 'CT_NIVEL_2'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataField = 'CT_NIVEL_2'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object TB_ELEMENT2: TZetaDBTextBox
      Left = 194
      Top = 2
      Width = 230
      Height = 17
      AutoSize = False
      Caption = 'TB_ELEMENT2'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  object Panel1: TPanel [8]
    Left = 0
    Top = 51
    Width = 458
    Height = 21
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 8
    object CT_NIVEL1lbl: TLabel
      Left = 88
      Top = 4
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nivel1:'
    end
    object CT_NIVEL_1: TZetaDBTextBox
      Left = 127
      Top = 2
      Width = 60
      Height = 17
      AutoSize = False
      Caption = 'CT_NIVEL_1'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataField = 'CT_NIVEL_1'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object TB_ELEMENT1: TZetaDBTextBox
      Left = 194
      Top = 2
      Width = 230
      Height = 17
      AutoSize = False
      Caption = 'TB_ELEMENT1'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  inherited DataSource: TDataSource
    Left = 4
    Top = 41
  end
end
