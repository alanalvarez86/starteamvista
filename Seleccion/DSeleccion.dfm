object dmSeleccion: TdmSeleccion
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 204
  Top = 151
  Height = 479
  Width = 741
  object cdsCondiciones: TZetaLookupDataSet
    Tag = 4
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsCondicionesAlAdquirirDatos
    AlEnviarDatos = cdsTablasAlEnviarDatos
    AlAgregar = cdsCondicionesAlAgregar
    AlBorrar = cdsCondicionesAlBorrar
    AlModificar = cdsTablaAlModificar
    UsaCache = True
    LookupName = 'Cat�logo de Condiciones'
    LookupDescriptionField = 'QU_DESCRIP'
    LookupKeyField = 'QU_CODIGO'
    OnGetRights = GetRights
    Left = 36
    Top = 52
  end
  object cdsAreaInteres: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    AfterDelete = cdsTablasAfterDelete
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsTablasAlAdquirirDatos
    AlEnviarDatos = cdsTablasAlEnviarDatos
    AlModificar = cdsTablaAlModificar
    UsaCache = True
    LookupName = 'Areas de Inter�s'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = GetRights
    Left = 36
    Top = 196
  end
  object cdsPuestos: TZetaLookupDataSet
    Tag = 3
    Aggregates = <>
    IndexFieldNames = 'PU_CODIGO'
    Params = <>
    BeforePost = cdsPuestosBeforePost
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsPuestosAlAdquirirDatos
    AlEnviarDatos = cdsTablasAlEnviarDatos
    AlCrearCampos = cdsPuestosAlCrearCampos
    AlAgregar = cdsPuestosAlAgregar
    AlBorrar = cdsPuestosAlBorrar
    AlModificar = cdsTablaAlModificar
    UsaCache = True
    LookupName = 'Cat�logo de Puestos'
    LookupDescriptionField = 'PU_DESCRIP'
    LookupKeyField = 'PU_CODIGO'
    OnGetRights = GetRights
    Left = 32
    Top = 8
  end
  object cdsTipoGasto: TZetaLookupDataSet
    Tag = 2
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    AfterDelete = cdsTablasAfterDelete
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsTablasAlAdquirirDatos
    AlEnviarDatos = cdsTablasAlEnviarDatos
    AlModificar = cdsTablaAlModificar
    UsaCache = True
    LookupName = 'Tipos de Gasto'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = GetRights
    Left = 36
    Top = 100
  end
  object cdsCliente: TZetaLookupDataSet
    Tag = 1
    Aggregates = <>
    IndexFieldNames = 'CL_CODIGO'
    Params = <>
    BeforePost = cdsClienteBeforePost
    AfterDelete = cdsTablasAfterDelete
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsTablasAlAdquirirDatos
    AlEnviarDatos = cdsTablasAlEnviarDatos
    AlModificar = cdsTablaAlModificar
    UsaCache = True
    LookupName = 'Clientes'
    LookupDescriptionField = 'CL_NOMBRE'
    LookupKeyField = 'CL_CODIGO'
    OnGetRights = GetRights
    Left = 36
    Top = 148
  end
  object cdsRequiere: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'RQ_PRIORID;RQ_FEC_INI'
    Params = <>
    AfterDelete = cdsRequiereAfterDelete
    OnReconcileError = cdsReconcileError
    AlCrearCampos = cdsRequiereAlCrearCampos
    AlAgregar = cdsRequiereAlAgregar
    AlModificar = cdsRequiereAlModificar
    Left = 123
    Top = 4
  end
  object cdsReqCandidatos: TZetaClientDataSet
    Tag = 8
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsReconcileError
    AlEnviarDatos = cdsReqCandidatosAlEnviarDatos
    AlCrearCampos = cdsReqCandidatosAlCrearCampos
    AlAgregar = cdsReqCandidatosAlAgregar
    AlBorrar = cdsReqCandidatosAlBorrar
    AlModificar = cdsReqCandidatosAlModificar
    Left = 123
    Top = 100
  end
  object cdsExamenes: TZetaClientDataSet
    Tag = 7
    Aggregates = <
      item
        Active = True
        AggregateName = 'MaxExamen'
        Expression = 'Max( EX_FOLIO ) + 0'
        Visible = False
      end>
    AggregatesActive = True
    Params = <>
    BeforePost = cdsExamenesBeforePost
    OnNewRecord = cdsExamenesNewRecord
    OnReconcileError = cdsReconcileError
    AlEnviarDatos = cdsTablasAlEnviarDatos
    AlCrearCampos = cdsExamenesAlCrearCampos
    AlBorrar = cdsExamenesAlBorrar
    AlModificar = cdsExamenesAlModificar
    Left = 219
    Top = 148
  end
  object cdsReqGastos: TZetaClientDataSet
    Tag = 9
    Aggregates = <>
    Params = <>
    OnNewRecord = cdsReqGastosNewRecord
    OnReconcileError = cdsReconcileError
    AlEnviarDatos = cdsReqGastosAlEnviarDatos
    AlCrearCampos = cdsReqGastosAlCrearCampos
    AlAgregar = cdsReqGastosAlAgregar
    AlBorrar = cdsReqGastosAlBorrar
    AlModificar = cdsReqGastosAlModificar
    Left = 123
    Top = 196
  end
  object cdsSolicita: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'SO_FOLIO'
    Params = <>
    AfterDelete = cdsSolicitaAfterDelete
    OnReconcileError = cdsSolicitaReconcileError
    AlCrearCampos = cdsSolicitaAlCrearCampos
    AlAgregar = cdsSolicitaAlAgregar
    AlModificar = cdsSolicitaAlModificar
    Left = 216
    Top = 8
  end
  object cdsSolCandidatos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsReconcileError
    AlCrearCampos = cdsSolCandidatosAlCrearCampos
    Left = 216
    Top = 100
  end
  object cdsEditRequiere: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsEditRequiereBeforePost
    OnNewRecord = cdsEditRequiereNewRecord
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsEditRequiereAlAdquirirDatos
    AlEnviarDatos = cdsEditRequiereAlEnviarDatos
    AlCrearCampos = cdsEditRequiereAlCrearCampos
    AlAgregar = cdsEditRequiereAlAgregar
    AlModificar = cdsEditRequiereAlModificar
    Left = 123
    Top = 52
  end
  object cdsEditSolicita: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsEditSolicitaBeforePost
    AfterCancel = cdsEditSolicitaAfterCancel
    OnNewRecord = cdsEditSolicitaNewRecord
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsEditSolicitaAlAdquirirDatos
    AlEnviarDatos = cdsEditSolicitaAlEnviarDatos
    AlCrearCampos = cdsEditSolicitaAlCrearCampos
    AlAgregar = cdsEditSolicitaAlAgregar
    AlModificar = cdsEditSolicitaAlModificar
    Left = 216
    Top = 52
  end
  object cdsEntrevista: TZetaClientDataSet
    Tag = 11
    Aggregates = <>
    Params = <>
    BeforePost = cdsEntrevistaBeforePost
    OnNewRecord = cdsEntrevistaNewRecord
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsEntrevistaAlAdquirirDatos
    AlEnviarDatos = cdsEntrevistaAlEnviarDatos
    AlCrearCampos = cdsEntrevistaAlCrearCampos
    AlAgregar = cdsEntrevistaAlAgregar
    AlBorrar = cdsEntrevistaAlBorrar
    AlModificar = cdsEntrevistaAlModificar
    Left = 123
    Top = 148
  end
  object cdsAdicional1: TZetaLookupDataSet
    Tag = 13
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    AfterDelete = cdsTablasAfterDelete
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsTablasAlAdquirirDatos
    AlEnviarDatos = cdsTablasAlEnviarDatos
    AlModificar = cdsTablaAlModificar
    UsaCache = True
    LookupName = 'Adicional #1'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = GetRights
    Left = 320
    Top = 5
  end
  object cdsAdicional2: TZetaLookupDataSet
    Tag = 14
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    AfterDelete = cdsTablasAfterDelete
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsTablasAlAdquirirDatos
    AlEnviarDatos = cdsTablasAlEnviarDatos
    AlModificar = cdsTablaAlModificar
    UsaCache = True
    LookupName = 'Adicional #2'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = GetRights
    Left = 320
    Top = 53
  end
  object cdsAdicional3: TZetaLookupDataSet
    Tag = 15
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    AfterDelete = cdsTablasAfterDelete
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsTablasAlAdquirirDatos
    AlEnviarDatos = cdsTablasAlEnviarDatos
    AlModificar = cdsTablaAlModificar
    UsaCache = True
    LookupName = 'Adicional #3'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = GetRights
    Left = 320
    Top = 100
  end
  object cdsAdicional4: TZetaLookupDataSet
    Tag = 16
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    AfterDelete = cdsTablasAfterDelete
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsTablasAlAdquirirDatos
    AlEnviarDatos = cdsTablasAlEnviarDatos
    AlModificar = cdsTablaAlModificar
    UsaCache = True
    LookupName = 'Adicional #4'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = GetRights
    Left = 320
    Top = 148
  end
  object cdsAdicional5: TZetaLookupDataSet
    Tag = 17
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    AfterDelete = cdsTablasAfterDelete
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsTablasAlAdquirirDatos
    AlEnviarDatos = cdsTablasAlEnviarDatos
    AlModificar = cdsTablaAlModificar
    UsaCache = True
    LookupName = 'Adicional #5'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = GetRights
    Left = 322
    Top = 196
  end
  object cdsRefLaboral: TZetaClientDataSet
    Tag = 20
    Aggregates = <>
    AggregatesActive = True
    Params = <>
    OnReconcileError = cdsReconcileError
    AlEnviarDatos = cdsTablasAlEnviarDatos
    AlModificar = cdsRefLaboralAlModificar
    Left = 219
    Top = 196
  end
  object Email: TNMSMTP
    Host = '10.10.10.1'
    Port = 25
    ReportLevel = 0
    UserID = 'cveliz'
    PostMessage.ToAddress.Strings = (
      'cveliz@tress.com.mx')
    PostMessage.Body.Strings = (
      
        '{\rtf1\ansi\ansicpg1252\deff0\deftab720{\fonttbl{\f0\fswiss MS S' +
        'ans Serif;}{\f1\froman\fcharset2 Symbol;}{\f2\fswiss\fprq2 Trebu' +
        'chet MS;}{\f3\froman Times New Roman;}}'
      '{\colortbl\red0\green0\blue0;\red0\green128\blue0;}'
      
        '\deflang1033\horzdoc{\*\fchars }{\*\lchars }\pard\plain\f2\fs20\' +
        'cf1\b N\'#39'famero  Nombre           Puesto  inca4    Ingreso    Ac' +
        'tivo  '
      
        '\par ======= ================ ======= ======== ========== ======' +
        '= '
      '\par '
      '\par 001     AYUDANTE COCINA B          '
      
        '\par     200 Guti\'#39'e9rrez L\'#39'f3pez  001        48.00 27/Sep/00  ' +
        'S       '
      
        '\par   44048 Lopez Perez, Yo  001         0.00 29/Feb/00  S     ' +
        '  '
      '\par '
      '\par 003     OPERADOR B                 '
      
        '\par     254 DURAN LOPEZ, OS  003        48.51 06/Jul/95  N     ' +
        '  '
      '\par '
      '\par 004     OPERADOR C                 '
      
        '\par     231 ESPINO ESPINOSA  004        24.70 03/Jul/95  N     ' +
        '  '
      
        '\par     242 SEGUNDO CORTES,  004        42.18 06/Jul/95  N     ' +
        '  '
      
        '\par     251 CAMACHO RIVERA,  004        42.18 06/Jul/95  N     ' +
        '  '
      
        '\par     252 CONTRERAS SOTO,  004        32.19 06/Jul/95  N     ' +
        '  '
      '\par '
      '\par 005     OPERADOR A                 '
      
        '\par     217 DUNCAN PEREZ, G  005        67.00 03/Jul/95  S     ' +
        '  '
      
        '\par     250 QUIROGA TAMAYO,  005        67.00 06/Jul/95  S     ' +
        '  '
      
        '\par     256 ARJONA BANUET,   005        67.00 06/Jul/95  S     ' +
        '  '
      
        '\par     267 PARTIDA PINEDO,  005        67.00 10/Jul/95  S     ' +
        '  '
      
        '\par     270 PEDRAZA CASTILL  005        67.00 10/Jul/95  S     ' +
        '  '
      '\par '
      '\par 006     TECNICO C                  '
      
        '\par     213 MENDEZ SERRANO,  006        82.00 03/Jul/95  S     ' +
        '  '
      
        '\par    4906 RAMOS ESCOBEDO,  006        79.00 10/Jul/95  S     ' +
        '  '
      '\par '
      '\par 007     TECNICO A                  '
      
        '\par     204 HERNANDEZ GUTIE  007       141.00 23/Nov/00  S     ' +
        '  '
      
        '\par     230 MIJARES NARVAEZ  007       141.00 03/Jul/95  S     ' +
        '  '
      
        '\par     262 JIMENEZ OCHOA,   007       141.00 06/Jul/95  S     ' +
        '  '
      
        '\par     266 RAMIREZ ROMO, O  007       141.00 10/Jul/95  S     ' +
        '  '
      '\par '
      '\par 008     TECNICO B                  '
      
        '\par     279 ACOSTA CLINTON,  008       105.00 10/Jul/95  S     ' +
        '  '
      
        '\par   44046 Montejo BARRIOS  008       105.00 24/Feb/00  N     ' +
        '  '
      '\par '
      '\par 010     TECNICO SR                 '
      
        '\par    3147 DE LA ROSA VILL  010       136.51 15/May/00  S     ' +
        '  '
      '\par '
      '\par 011     LIDER A                    '
      
        '\par     227 CESTO PEREZ, LE  011       141.00 03/Jul/95  S     ' +
        '  '
      
        '\par     253 DIAZ MASCARENO,  011       141.00 19/Oct/95  S     ' +
        '  '
      
        '\par     261 COSTA SORIA, AR  011       141.00 06/Jul/95  S     ' +
        '  '
      '\par '
      '\par 012     LIDER B                    '
      
        '\par     268 MENDOZA ORTIZ,   012       105.00 10/Jul/95  S     ' +
        '  '
      
        '\par     269 RIVERA STANLEY,  012       110.00 10/Jul/95  S     ' +
        '  '
      
        '\par     278 HERNANDEZ BERMU  012       110.00 10/Jul/95  S     ' +
        '  '
      
        '\par     243 QUIROZ CABRERA,  012       110.00 06/Jul/95  N     ' +
        '  '
      '\par '
      '\par 014     LIDER SR                   '
      
        '\par    3132 CARDENAS QUINTA  014       136.51 20/Jul/99  S     ' +
        '  '
      
        '\par    7598 ANGULO DEL BARR  014       181.00 10/Jul/95  S     ' +
        '  '
      
        '\par     205 LOPEZ GARDU\'#39'd1O,   014       181.00 27/Jun/95  N  ' +
        '     '
      '\par '
      '\par 017     OPERADOR MONTACARGAS A     '
      
        '\par     212 ORMAZABAL BOJOR  017        65.34 30/Jun/95  N     ' +
        '  '
      
        '\par     240 PRADO OCEGUERA,  017        79.00 03/Jul/95  N     ' +
        '  '
      '\par '
      '\par 018     OPERADOR MONTACARGAS B     '
      
        '\par     255 GUZMAN GUADIAN,  018        53.82 06/Jul/95  N     ' +
        '  '
      
        '\par     265 ORTEGA ZEPEDA,   018        67.00 10/Jul/95  N     ' +
        '  '
      '\par '
      '\par 047     ASISTENTE OF. BILINGUE A   '
      
        '\par     263 HERNANDEZ MARTI  047       141.00 06/Jul/95  S     ' +
        '  '
      '\par '
      '\par 048     ASISTENTE OF. BILINGUE B   '
      
        '\par     232 LAGUARDIA RAMIR  048        73.60 03/Jul/95  S     ' +
        '  '
      
        '\par    1555 BARON RAMIREZ,   048        73.60 29/Jun/95  N     ' +
        '  '
      '\par '
      '\par 061     INGENIERO A                '
      
        '\par     206 SANCHEZ JIMENEZ  061       245.00 27/Jun/95  N     ' +
        '  '
      
        '\par     211 TORRECILLAS ARE  061       307.80 30/Jun/95  N     ' +
        '  '
      '\par '
      '\par 062     INGENIERO B                '
      
        '\par     208 MEJIA BRAVO, RI  062       274.00 28/Jun/95  N     ' +
        '  '
      
        '\par   44047 Mora Cardenas,   062       100.00 28/Feb/99  N     ' +
        '  '
      '\par '
      '\par }'
      ' ')
    EncodeType = uuMime
    ClearParams = True
    SubType = mtSgml
    Charset = 'us-ascii'
    OnFailure = EmailFailure
    Left = 32
    Top = 248
  end
  object cdsLookUpSolicita: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AlCrearCampos = cdsSolicitaAlCrearCampos
    Left = 224
    Top = 248
  end
  object cdsLookupRequiere: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    Left = 120
    Top = 248
  end
  object cdsDocumentos: TZetaClientDataSet
    Tag = 21
    Aggregates = <>
    Params = <>
    OnCalcFields = cdsDocumentosCalcFields
    OnReconcileError = cdsReconcileError
    AlEnviarDatos = cdsTablasAlEnviarDatos
    AlCrearCampos = cdsDocumentosAlCrearCampos
    AlAgregar = cdsDocumentosAlAgregar
    AlBorrar = cdsDocumentosAlBorrar
    AlModificar = cdsDocumentosAlModificar
    Left = 323
    Top = 244
  end
  object cdsEditDocumentos: TZetaClientDataSet
    Tag = 21
    Aggregates = <>
    Params = <>
    BeforePost = cdsEditDocumentosBeforePost
    OnNewRecord = cdsEditDocumentosNewRecord
    OnReconcileError = cdsEditDocumentosReconcileError
    AlAdquirirDatos = cdsEditDocumentosAlAdquirirDatos
    AlEnviarDatos = cdsEditDocumentosAlEnviarDatos
    AlModificar = cdsEditDocumentosAlModificar
    Left = 320
    Top = 296
  end
  object cdsEstados: TZetaLookupDataSet
    Tag = 22
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    StoreDefs = True
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsEstadosAlAdquirirDatos
    AlModificar = cdsTablaAlModificar
    UsaCache = True
    LookupName = 'Estados del Pa�s'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = cdsEstadosGetRights
    Left = 32
    Top = 312
  end
  object cdsContratos: TZetaLookupDataSet
    Tag = 23
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsContratosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Tipos de Contrato'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = cdsEstadosGetRights
    Left = 112
    Top = 312
  end
  object cdsTransporte: TZetaLookupDataSet
    Tag = 24
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsTransporteAlAdquirirDatos
    LookupName = 'Transporte'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = cdsEstadosGetRights
    Left = 200
    Top = 312
  end
end
