unit FSistEditEmpresas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FSistBaseEditEmpresas, Db, StdCtrls, ZetaEdit, Mask, DBCtrls, ExtCtrls,
  Buttons, ZetaSmartLists, ZetaKeyLookup;

type
  TSistEditEmpresas = class(TSistBaseEditEmpresas)
    Label3: TLabel;
    DB_CODIGO: TZetaDBKeyLookup;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations } 
    procedure Connect;override;
  end;

var
  SistEditEmpresas: TSistEditEmpresas;

implementation

uses DSistema;

{$R *.DFM}

procedure TSistEditEmpresas.FormCreate(Sender: TObject);
begin
  inherited;

     with dmSistema do
     begin
          DB_CODIGO.LookupDataset := cdsSistBaseDatosLookUp;
     end;
end;

procedure TSistEditEmpresas.Connect;
begin
     with dmSistema do
     begin
          // cdsEmpresasLookup.Conectar;
          cdsSistBaseDatosLookup.Conectar;
     end;
     inherited;
end;

end.
