unit FGlobalRequisitos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     ZBaseGlobal, ZetaKeyCombo;

type
  TGlobalRequisitos = class(TBaseGlobal)
    Requisito1LBL: TLabel;
    Requisito2LBL: TLabel;
    Requisito3LBL: TLabel;
    Requisito4LBL: TLabel;
    Requisito5LBL: TLabel;
    Requisito6LBL: TLabel;
    Requisito7LBL: TLabel;
    Requisito8LBL: TLabel;
    Requisito9LBL: TLabel;
    Requisito1: TEdit;
    Requisito2: TEdit;
    Requisito3: TEdit;
    Requisito4: TEdit;
    Requisito5: TEdit;
    Requisito6: TEdit;
    Requisito7: TEdit;
    Requisito8: TEdit;
    Requisito9: TEdit;
    RequisitoTipo1: TZetaKeyCombo;
    RequisitoTipo2: TZetaKeyCombo;
    RequisitoTipo3: TZetaKeyCombo;
    RequisitoTipo4: TZetaKeyCombo;
    RequisitoTipo5: TZetaKeyCombo;
    RequisitoTipo6: TZetaKeyCombo;
    RequisitoTipo7: TZetaKeyCombo;
    RequisitoTipo8: TZetaKeyCombo;
    RequisitoTipo9: TZetaKeyCombo;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GlobalRequisitos: TGlobalRequisitos;

implementation

uses ZGlobalTress,
     ZAccesosTress,
     ZetaCommonClasses;

{$R *.DFM}

procedure TGlobalRequisitos.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos  := D_CAT_CONFI_GLOBALES;
     Requisito1.Tag := K_GLOBAL_REQUISITO1;
     Requisito2.Tag := K_GLOBAL_REQUISITO2;
     Requisito3.Tag := K_GLOBAL_REQUISITO3;
     Requisito4.Tag := K_GLOBAL_REQUISITO4;
     Requisito5.Tag := K_GLOBAL_REQUISITO5;
     Requisito6.Tag := K_GLOBAL_REQUISITO6;
     Requisito7.Tag := K_GLOBAL_REQUISITO7;
     Requisito8.Tag := K_GLOBAL_REQUISITO8;
     Requisito9.Tag := K_GLOBAL_REQUISITO9;
     RequisitoTipo1.Tag := K_GLOBAL_TPO_REQUISITO1;{OP:16.Abr.08}
     RequisitoTipo2.Tag := K_GLOBAL_TPO_REQUISITO2;{OP:16.Abr.08}
     RequisitoTipo3.Tag := K_GLOBAL_TPO_REQUISITO3;{OP:16.Abr.08}
     RequisitoTipo4.Tag := K_GLOBAL_TPO_REQUISITO4;{OP:16.Abr.08}
     RequisitoTipo5.Tag := K_GLOBAL_TPO_REQUISITO5;{OP:16.Abr.08}
     RequisitoTipo6.Tag := K_GLOBAL_TPO_REQUISITO6;{OP:16.Abr.08}
     RequisitoTipo7.Tag := K_GLOBAL_TPO_REQUISITO7;{OP:16.Abr.08}
     RequisitoTipo8.Tag := K_GLOBAL_TPO_REQUISITO8;{OP:16.Abr.08}
     RequisitoTipo9.Tag := K_GLOBAL_TPO_REQUISITO9;{OP:16.Abr.08}
     HelpContext    := H00078_Solicitud_Requisitos;
     ActualizaDiccion := True;
end;

end.
