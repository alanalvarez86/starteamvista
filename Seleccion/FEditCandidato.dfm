inherited EditCandidato: TEditCandidato
  Left = 186
  Top = 74
  Caption = 'Candidato'
  ClientHeight = 481
  ClientWidth = 581
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 445
    Width = 581
    inherited OK: TBitBtn
      Left = 423
    end
    inherited Cancelar: TBitBtn
      Left = 502
    end
  end
  inherited PanelSuperior: TPanel
    Width = 581
    inherited AgregarBtn: TSpeedButton
      Visible = False
    end
    inherited BorrarBtn: TSpeedButton
      Visible = False
    end
    inherited ModificarBtn: TSpeedButton
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 581
    inherited Splitter: TSplitter
      Left = 305
    end
    inherited ValorActivo1: TPanel
      Width = 305
    end
    inherited ValorActivo2: TPanel
      Left = 308
      Width = 273
    end
  end
  inherited Panel1: TPanel
    Width = 581
    Height = 30
    object Label1: TLabel
      Left = 21
      Top = 7
      Width = 58
      Height = 13
      Caption = 'Requisici�n:'
    end
    object eRequisicion: TZetaTextBox
      Left = 87
      Top = 5
      Width = 390
      Height = 19
      AutoSize = False
      Caption = '#1234: 1 Vacante de SECRETARIA EJECUTIVA'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
  end
  inherited PageControl: TPageControl
    Top = 181
    Width = 581
    Height = 264
    inherited Datos: TTabSheet
      TabVisible = False
    end
    inherited Tabla: TTabSheet
      Caption = 'Entrevistas'
      inherited GridRenglones: TZetaDBGrid
        Width = 573
        Height = 207
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
        OnDblClick = BBModificarClick
        Columns = <
          item
            Expanded = False
            FieldName = 'ER_FOLIO'
            PickList.Strings = ()
            Title.Caption = '#'
            Width = 17
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ER_FECHA'
            PickList.Strings = ()
            Title.Caption = 'Fecha'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ER_NOMBRE'
            PickList.Strings = ()
            Title.Caption = 'Entrevistador'
            Width = 128
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ER_RESUMEN'
            PickList.Strings = ()
            Title.Caption = 'Resumen'
            Width = 129
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ER_RESULT'
            PickList.Strings = ()
            Title.Caption = 'Recomendaci�n'
            Width = 85
            Visible = True
          end>
      end
      inherited Panel2: TPanel
        Width = 573
        inherited BBAgregar: TBitBtn
          Left = 13
          Width = 80
          Caption = 'Agregar'
        end
        inherited BBBorrar: TBitBtn
          Left = 97
          Width = 80
          Caption = 'Borrar'
        end
        inherited BBModificar: TBitBtn
          Left = 181
          Width = 80
          Caption = 'Modificar'
        end
      end
    end
  end
  object Panel3: TPanel [5]
    Left = 0
    Top = 81
    Width = 581
    Height = 100
    Align = alTop
    TabOrder = 5
    object Label2: TLabel
      Left = 30
      Top = 8
      Width = 51
      Height = 13
      Caption = 'Candidato:'
    end
    object eNombreCandidato: TZetaTextBox
      Left = 87
      Top = 5
      Width = 390
      Height = 19
      AutoSize = False
      Caption = '#101: Perez Martinez, Juan'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object Label3: TLabel
      Left = 55
      Top = 28
      Width = 26
      Height = 13
      Caption = 'Perfil:'
    end
    object ePerfil: TZetaTextBox
      Left = 87
      Top = 25
      Width = 390
      Height = 19
      AutoSize = False
      Caption = 'Masculino, 34 a�os, Secundaria, 60% Ingl�s'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object Label4: TLabel
      Left = 48
      Top = 52
      Width = 33
      Height = 13
      Caption = 'Status:'
    end
    object Label5: TLabel
      Left = 7
      Top = 76
      Width = 74
      Height = 13
      Caption = 'Observaciones:'
    end
    object BtnSolicitud: TBitBtn
      Left = 488
      Top = 17
      Width = 85
      Height = 65
      Caption = 'Ver Solicitud'
      TabOrder = 0
      OnClick = BtnSolicitudClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        5555555555FFFFFFFFFF5555500000000005555557777777777F55550BFBFBFB
        FB0555557F555555557F55500FBFBFBFBF0555577F555555557F550B0BFBFBFB
        FB05557F7F555555557F500F0FBFBFBFBF05577F7F555555557F0B0B0BFBFBFB
        FB057F7F7F555555557F0F0F0FBFBFBFBF057F7F7FFFFFFFFF750B0B00000000
        00557F7F7777777777550F0FB0FBFB0F05557F7FF75FFF7575550B0007000070
        55557F777577775755550FB0FBFB0F0555557FF75FFF75755555000700007055
        5555777577775755555550FBFB0555555555575FFF7555555555570000755555
        5555557777555555555555555555555555555555555555555555}
      Layout = blGlyphTop
      NumGlyphs = 2
    end
    object cbStatus: TZetaDBKeyCombo
      Left = 87
      Top = 48
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
      ListaFija = lfCanStatus
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      DataField = 'CD_STATUS'
      DataSource = DataSource
      LlaveNumerica = True
    end
    object DBEdit1: TDBEdit
      Left = 87
      Top = 72
      Width = 390
      Height = 21
      DataField = 'CD_COMENT'
      DataSource = DataSource
      TabOrder = 2
    end
  end
  inherited dsRenglon: TDataSource
    Left = 415
  end
end
