unit FGlobalReferencias;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     ZBaseGlobal, Mask, ZetaNumero, ComCtrls;

type
  TGlobalReferencias = class(TBaseGlobal)
    QtyReferencias: TZetaNumero;
    LblCuantos: TLabel;
    PageControl1: TPageControl;
    Datos: TTabSheet;
    Verificacion: TTabSheet;
    Label1: TLabel;
    NombreCampo1: TEdit;
    Label2: TLabel;
    NombreCampo2: TEdit;
    Label3: TLabel;
    NombreCampo3: TEdit;
    Label4: TLabel;
    NombreCampo4: TEdit;
    Label5: TLabel;
    NombreCampo5: TEdit;
    Label6: TLabel;
    NombreCampo6: TEdit;
    Label7: TLabel;
    NombreCampo7: TEdit;
    Label8: TLabel;
    NombreCampo8: TEdit;
    Label9: TLabel;
    NombreCampo9: TEdit;
    Label10: TLabel;
    NombreCampo10: TEdit;
    {OP:18.Abr.08}
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    NombreCampo11: TEdit;
    NombreCampo12: TEdit;
    NombreCampo13: TEdit;
    NombreCampo14: TEdit;
    NombreCampo15: TEdit;
    NombreCampo16: TEdit;
    NombreCampo17: TEdit;
    NombreCampo18: TEdit;
    NombreCampo19: TEdit;
    NombreCampo20: TEdit;
    Label31: TLabel;
    NombreCampoRV1: TEdit;
    Label32: TLabel;
    NombreCampoRV2: TEdit;
    Label33: TLabel;
    NombreCampoRV3: TEdit;
    Label34: TLabel;
    NombreCampoRV4: TEdit;
    Label35: TLabel;
    NombreCampoRV5: TEdit;
    Label36: TLabel;
    NombreCampoRV6: TEdit;
    Label37: TLabel;
    NombreCampoRV7: TEdit;
    Label38: TLabel;
    NombreCampoRV8: TEdit;
    Label39: TLabel;
    NombreCampoRV9: TEdit;
    Label40: TLabel;
    NombreCampoRV10: TEdit;
    Label41: TLabel;
    NombreCampoRV20: TEdit;
    Label42: TLabel;
    NombreCampoRV19: TEdit;
    NombreCampoRV18: TEdit;
    Label43: TLabel;
    Label44: TLabel;
    NombreCampoRV17: TEdit;
    NombreCampoRV16: TEdit;
    Label45: TLabel;
    Label46: TLabel;
    NombreCampoRV15: TEdit;
    NombreCampoRV14: TEdit;
    Label47: TLabel;
    Label48: TLabel;
    NombreCampoRV13: TEdit;
    NombreCampoRV12: TEdit;
    Label49: TLabel;
    Label50: TLabel;
    NombreCampoRV11: TEdit;

    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GlobalReferencias: TGlobalReferencias;

implementation

uses ZGlobalTress,
     ZAccesosTress,
     ZetaCommonClasses;

{$R *.DFM}

procedure TGlobalReferencias.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos      := D_CAT_CONFI_GLOBALES;
     QtyReferencias.Tag := K_GLOBAL_REF_LABORAL_QTY;
     NombreCampo1.Tag   := K_GLOBAL_REF_LABORAL1;
     NombreCampo2.Tag   := K_GLOBAL_REF_LABORAL2;
     NombreCampo3.Tag   := K_GLOBAL_REF_LABORAL3;
     NombreCampo4.Tag   := K_GLOBAL_REF_LABORAL4;
     NombreCampo5.Tag   := K_GLOBAL_REF_LABORAL5;
     NombreCampo6.Tag   := K_GLOBAL_REF_LABORAL6;
     NombreCampo7.Tag   := K_GLOBAL_REF_LABORAL7;
     NombreCampo8.Tag   := K_GLOBAL_REF_LABORAL8;
     NombreCampo9.Tag   := K_GLOBAL_REF_LABORAL9;
     NombreCampo10.Tag  := K_GLOBAL_REF_LABORAL10;
     NombreCampo11.Tag  := K_GLOBAL_REF_LABORAL11;
     NombreCampo12.Tag  := K_GLOBAL_REF_LABORAL12;
     NombreCampo13.Tag  := K_GLOBAL_REF_LABORAL13;
     NombreCampo14.Tag  := K_GLOBAL_REF_LABORAL14;
     NombreCampo15.Tag  := K_GLOBAL_REF_LABORAL15;
     NombreCampo16.Tag  := K_GLOBAL_REF_LABORAL16;
     NombreCampo17.Tag  := K_GLOBAL_REF_LABORAL17;
     NombreCampo18.Tag  := K_GLOBAL_REF_LABORAL18;
     NombreCampo19.Tag  := K_GLOBAL_REF_LABORAL19;
     NombreCampo20.Tag  := K_GLOBAL_REF_LABORAL20;
     NombreCampoRV1.Tag   := K_GLOBAL_REF_LABORALRV1;
     NombreCampoRV2.Tag   := K_GLOBAL_REF_LABORALRV2;
     NombreCampoRV3.Tag   := K_GLOBAL_REF_LABORALRV3;
     NombreCampoRV4.Tag   := K_GLOBAL_REF_LABORALRV4;
     NombreCampoRV5.Tag   := K_GLOBAL_REF_LABORALRV5;
     NombreCampoRV6.Tag   := K_GLOBAL_REF_LABORALRV6;
     NombreCampoRV7.Tag   := K_GLOBAL_REF_LABORALRV7;
     NombreCampoRV8.Tag   := K_GLOBAL_REF_LABORALRV8;
     NombreCampoRV9.Tag   := K_GLOBAL_REF_LABORALRV9;
     NombreCampoRV10.Tag  := K_GLOBAL_REF_LABORALRV10;
     NombreCampoRV11.Tag  := K_GLOBAL_REF_LABORALRV11;
     NombreCampoRV12.Tag  := K_GLOBAL_REF_LABORALRV12;
     NombreCampoRV13.Tag  := K_GLOBAL_REF_LABORALRV13;
     NombreCampoRV14.Tag  := K_GLOBAL_REF_LABORALRV14;
     NombreCampoRV15.Tag  := K_GLOBAL_REF_LABORALRV15;
     NombreCampoRV16.Tag  := K_GLOBAL_REF_LABORALRV16;
     NombreCampoRV17.Tag  := K_GLOBAL_REF_LABORALRV17;
     NombreCampoRV18.Tag  := K_GLOBAL_REF_LABORALRV18;
     NombreCampoRV19.Tag  := K_GLOBAL_REF_LABORALRV19;
     NombreCampoRV20.Tag  := K_GLOBAL_REF_LABORALRV20;
     ActualizaDiccion   := True;
     HelpContext        := H00050_Globales_de_empresa;
end;

procedure TGlobalReferencias.FormShow(Sender: TObject);
begin
  inherited;
  PageControl1.ActivePage := Datos;
end;

end.
