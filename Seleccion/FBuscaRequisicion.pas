unit FBuscaRequisicion;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, Mask, ZetaNumero, ZetaKeyLookup, StdCtrls, Buttons,
  ExtCtrls, Grids, DBGrids, ZetaDBGrid, Db, ZetaMessages,ZetaCommonClasses;

type
  TBuscaRequisicion = class(TZetaDlgModal)
    DataSource: TDataSource;
    PanelFiltro: TPanel;
    AreaLBL: TLabel;
    CriterioLbl: TLabel;
    Refrescar: TBitBtn;
    Cliente: TZetaKeyLookup;
    Puesto: TZetaKeyLookup;
    Label3: TLabel;
    cbStatus: TComboBox;
    Label4: TLabel;
    cbPrioridad: TComboBox;
    ZetaDBGrid: TZetaDBGrid;
    EdadMinimaLBL: TLabel;
    FolioInicial: TZetaNumero;
    EdadMaximaLBL: TLabel;
    FolioFinal: TZetaNumero;
    procedure RefrescarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FParametros : TZetaParams;
    procedure Connect;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
  public
    { Public declarations }
    procedure Refresh;
  end;

var
  BuscaRequisicion: TBuscaRequisicion;

function BuscaRequisicionDialogo(const sFilter: String; var sKey, sDescription: String) : Boolean;

implementation
uses DSeleccion,
     ZetaCommonLists,
     ZetaCommonTools;
{$R *.DFM}

function BuscaRequisicionDialogo(const sFilter: String; var sKey, sDescription: String) : Boolean;
begin
     if BuscaRequisicion = NIL then
        BuscaRequisicion := TBuscaRequisicion.Create(Application);

     BuscaRequisicion.ShowModal;

     Result := BuscaRequisicion.ModalResult = mrOk ;
     if Result then
     begin
          with dmSeleccion.cdsRequiere do
          begin
               sKey := FieldByName('RQ_FOLIO').AsString;
               sDescription := '';
          end;
     end;
end;

procedure TBuscaRequisicion.FormCreate(Sender: TObject);
 procedure FillCombo( Control: TComboBox; Tipo: ListasFijas; const sTodos: String );
 begin
      with Control do
      begin
           ZetaCommonLists.LlenaLista( Tipo, Items );
           Items.Insert( 0, Format( '< %s >', [ sTodos ] ) );
           ItemIndex := 0;
      end;
 end;
begin
     inherited;
     HelpContext := H00073_Solicitudes;
     FParametros := TZetaParams.Create;
     FillCombo( cbStatus, lfReqStatus, 'Todos' );
     FillCombo( cbPrioridad, lfPrioridad, 'Todas' );
     FolioInicial.Valor := 0;
     FolioInicial.Valor := 0;
     Cliente.LookupDataset := dmSeleccion.cdsCliente;
     Puesto.LookupDataset := dmSeleccion.cdsPuestos;

end;


procedure TBuscaRequisicion.RefrescarClick(Sender: TObject);
begin
     inherited;
     Refresh;
     ZetaDBGrid.SetFocus;
end;

procedure TBuscaRequisicion.FormDestroy(Sender: TObject);
begin
     inherited;
     FParametros.Free;
end;

procedure TBuscaRequisicion.Refresh;
begin
     with FParametros do
     begin
          AddInteger( 'Status', CBStatus.ItemIndex - 1 );
          AddInteger( 'Prioridad', CBPrioridad.ItemIndex - 1 );
          AddInteger( 'FolioInicial', FolioInicial.ValorEntero );
          AddInteger( 'FolioFinal', FolioFinal.ValorEntero );
          AddString( 'Cliente', Cliente.Llave );
          AddString( 'Puesto', Puesto.Llave );
     end;
     dmSeleccion.RefrescarRequiere( FParametros );
end;

procedure TBuscaRequisicion.FormShow(Sender: TObject);
begin
     inherited;
     Connect;
end;

procedure TBuscaRequisicion.Connect;
begin
     with dmSeleccion do
     begin
          cdsRequiere.Close;
          DataSource.DataSet := cdsRequiere;
     end;
end;

procedure TBuscaRequisicion.WMExaminar(var Message: TMessage);
begin
     Ok.Click;
end;


end.



