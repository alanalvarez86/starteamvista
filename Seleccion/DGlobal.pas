unit DGlobal;

interface

uses Forms, Controls, DB, Classes, StdCtrls, SysUtils,
{$ifdef DOS_CAPAS}
     DServerGlobalSeleccion,
{$else}
     GlobalSeleccion_TLB,
{$endif}
     dBaseGlobal;

type
  TdmGlobal = class( TdmBaseGlobal )
  private
    FListaUsados: TStrings;
{$ifdef DOS_CAPAS}
    function GetServer: TdmServerGlobalSeleccion;
    property Server: TdmServerGlobalSeleccion read GetServer;
{$else}
    FServidor: IdmServerGlobalSeleccionDisp;
    function GetServer: IdmServerGlobalSeleccionDisp;
    property Server: IdmServerGlobalSeleccionDisp read GetServer;
{$endif}
  protected
    function GetGlobales: Variant; override;
    procedure GrabaGlobales( const aGlobalServer: Variant; const lActualizaDiccion: Boolean;
              var ErrorCount: Integer ); override;
  public
    { Public declarations }
    property ListaUsados: TStrings read FListaUsados;
    destructor Destroy; override;
    function NombreRefLab( const Index: Integer ): String;
    function NumCamposRefLab: Integer;
    function NombreRefVLab( const Index: Integer ): String;{OP:22.Abr.08}
    function NumCamposRefVLab: Integer;{OP:22.Abr.08}
    procedure InitListaUsados;
  end;

var
   Global: TdmGlobal;

implementation

uses dCliente, ZGlobalTress;

{ TdmGlobal }

{$ifdef DOS_CAPAS}
function TdmGlobal.GetServer: TdmServerGlobalSeleccion;
begin
     Result := dmCliente.ServerGlobal;
end;
{$else}
function TdmGlobal.GetServer: IdmServerGlobalSeleccionDisp;
begin
     Result := IdmServerGlobalSeleccionDisp( dmCliente.CreaServidor( CLASS_dmServerGlobalSeleccion, FServidor ) );
end;
{$endif}

{ER: Si se requiere declarar GetGlobales y GrabaGlobales, porque no se
puede declarar el SERVER en la clase base}
function TdmGlobal.GetGlobales: Variant;
begin
     Result := Server.GetGlobales( dmCliente.Empresa );
end;

procedure TdmGlobal.GrabaGlobales(const aGlobalServer: Variant; const lActualizaDiccion: Boolean;
          var ErrorCount: Integer);
begin
     Server.GrabaGlobales( dmCliente.Empresa, aGlobalServer, lActualizaDiccion, ErrorCount );
end;

destructor TdmGlobal.Destroy;
begin
     FreeAndNil( FListaUsados );
     inherited Destroy;
end;

procedure TdmGlobal.InitListaUsados;
begin
     if ( FListaUsados = nil ) then
        FListaUsados := TStringList.Create;
     FListaUsados.CommaText := GetGlobalString( ZGlobalTress.K_GLOBAL_POSICION_ADIC );
end;

function TdmGlobal.NombreRefLab( const Index: Integer ): String;
var
   iGlobalBase : Integer;
begin
     {OP:28.Abr.08}
     iGlobalBase := K_GLOBAL_REF_LABORAL_BASE;
     if Index > K_MAXIMO_REF_1 then
        iGlobalBase := K_GLOBAL_REF_LABORAL_BASE2;
     Result := Trim( GetGlobalString( iGlobalBase + Index ) );
end;

function TdmGlobal.NumCamposRefLab: Integer;
begin
     Result := 0;
     // Cuenta los Campos NO vacios. En cuanto encuentra uno vac�o, deja de contar
     while ( Result < K_GLOBAL_REF_LABORAL_MAX ) and ( Length( NombreRefLab( Result+1 )) > 0 ) do
           Inc( Result );
end;

function TdmGlobal.NombreRefVLab( const Index: Integer ): String;
begin
     Result := Trim( GetGlobalString( K_GLOBAL_REFV_LABORAL_BASE + Index ) );
end;

function TdmGlobal.NumCamposRefVLab: Integer;
begin
     Result := 0;
     // Cuenta los Campos NO vacios. En cuanto encuentra uno vac�o, deja de contar
     while ( Result < K_GLOBAL_REFV_LABORAL_MAX ) and ( Length( NombreRefVLab( Result+1 )) > 0 ) do
           Inc( Result );
end;

end.
