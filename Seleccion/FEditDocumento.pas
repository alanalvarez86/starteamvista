unit FEditDocumento;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask, dbTables,
     ShellAPI,
     ZBaseEdicion,
     ZetaFecha,
     ZetaDBTextBox, 
     ZetaKeyCombo,
     ZetaNumero, DLL95V1, TDMULTIP, TDMULTIM, TMultiP, MPlayer, TMultiMP;

type
  TEditDocumento = class(TBaseEdicion)
    Panel1: TPanel;
    Label1: TLabel;
    Label4: TLabel;
    SO_FOLIO: TZetaDBTextBox;
    PRETTYNAME: TZetaTextBox;
    DO_TIPO: TDBComboBox;
    Label2: TLabel;
    EArchivo: TEdit;
    SpeedButton1: TSpeedButton;
    SpeedButton4: TSpeedButton;
    Label5: TLabel;
    DO_OBSERVA: TDBEdit;
    OpenDialog: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EArchivoChange(Sender: TObject);
    procedure DO_TIPOChange(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    FConectando : Boolean;
    procedure EscribirCambios;override;
    procedure Connect; override;
    procedure Disconnect;override;
  public
    { Public declarations }
  end;

var
  EditDocumento: TEditDocumento;

implementation

{$R *.DFM}

uses DSeleccion,
     ZAccesosTress,
     ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonClasses;

procedure TEditDocumento.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := DO_TIPO;
     IndexDerechos := D_SOLICITUD_DODUMENTO;
     //HelpContext := H00076_Solicitud_Examenes;
     DO_TIPO.CharCase := ecUpperCase;
end;

procedure TEditDocumento.Connect;
begin
     FConectando := TRUE;
     try
        with dmSeleccion do
        begin
             Datasource.Dataset := cdsEditDocumentos;
             PRETTYNAME.Caption := cdsEditSolicita.FieldByName( 'PRETTYNAME' ).AsString;
        end;
        EArchivo.Text := '';
        DO_TIPOChange(NIL);
     finally
        FConectando := FALSE;
     end;
end;

procedure TEditDocumento.Disconnect;
begin
     DataSource.Dataset := NIL;
end;

procedure TEditDocumento.SpeedButton1Click(Sender: TObject);
begin
     inherited;
     with OpenDialog do
     begin
          FileName := EArchivo.Text;
          if Execute then
          begin
               EArchivo.Text := FileName;
               Modo := dsEdit;
          end;
     end;
end;

procedure TEditDocumento.EscribirCambios;
begin
     if dmSeleccion.CargaDocumento(eArchivo.text) then
     begin
          inherited EscribirCambios;
          if ( dmSeleccion.cdsEditDocumentos.State = dsBrowse ) then
          begin
               dmSeleccion.SetCambiosDocumentos;
               Close;
          end;
     end
     else
         EArchivo.SetFocus;
end;

procedure TEditDocumento.EArchivoChange(Sender: TObject);
begin
     inherited;
     if Not FConectando then
     begin
          Modo := dsEdit;
          dmSeleccion.cdsEditDocumentos.FieldByName('DO_OBSERVA').AsString :=
          Copy(ExtractFileName(EArchivo.Text),1,Pos('.',EArchivo.Text)-1);
     end;
end;

procedure TEditDocumento.DO_TIPOChange(Sender: TObject);
var
   sTipo : string;
begin
     inherited;
     sTipo := DO_TIPO.Text;
     if ( sTipo = 'HUELLA') or ( sTipo  = 'FOTO') then
        OpenDialog.FilterIndex := 2
     else if (sTipo  = 'AUDIO') or ( sTipo = 'VOZ') then
        OpenDialog.FilterIndex := 3
     else
        OpenDialog.FilterIndex := 1
end;

end.

