unit FAltaSolicitud;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZWizardBasico, ComCtrls, StdCtrls, Buttons, ZetaWizard, ExtCtrls,
  ZetaFecha, Mask, ZetaNumero, ZetaKeyLookup, ZetaKeyCombo, DBCtrls, Db,
  TDMULTIP, ZetaDBTextBox, Grids, DBGrids, ZetaDBGrid, ToolWin, ImgList,
  ExtDlgs, CheckLst;

type
  TAltaSolicitud = class(TWizardBasico)
    DataSource: TDataSource;
    dsRefLaboral: TDataSource;
    Identificacion: TTabSheet;
    LblApePat: TLabel;
    LblApeMat: TLabel;
    LblNombres: TLabel;
    LblFecNac: TLabel;
    LblSexo: TLabel;
    ZEdad: TZetaTextBox;
    SO_APE_PAT: TDBEdit;
    SO_APE_MAT: TDBEdit;
    SO_NOMBRES: TDBEdit;
    SO_FEC_NAC: TZetaDBFecha;
    SO_SEXO: TZetaDBKeyCombo;
    Solicitud: TTabSheet;
    LblFecSolicitud: TLabel;
    SO_FEC_INI: TZetaDBFecha;
    SO_PUE_SOL: TDBEdit;
    LblPuestoSolic: TLabel;
    GBAreas: TGroupBox;
    Adicionales: TTabSheet;
    Requisitos: TTabSheet;
    SO_REQUI_1: TDBCheckBox;
    SO_REQUI_2: TDBCheckBox;
    SO_REQUI_3: TDBCheckBox;
    SO_REQUI_4: TDBCheckBox;
    SO_REQUI_5: TDBCheckBox;
    SO_REQUI_6: TDBCheckBox;
    SO_REQUI_7: TDBCheckBox;
    SO_REQUI_8: TDBCheckBox;
    SO_REQUI_9: TDBCheckBox;
    Referencias1: TTabSheet;
    Documentos: TTabSheet;
    Notas: TTabSheet;
    SO_OBSERVA: TDBMemo;
    dsDocumentos: TDataSource;
    ImageListPrende: TImageList;
    Foto: TTabSheet;
    BarraFotografia: TToolBar;
    tbLeerFoto: TToolButton;
    tbGrabarFoto: TToolButton;
    tbLeerTwain: TToolButton;
    tbSelectTwain: TToolButton;
    FOTOGRAFIA: TPDBMultiImage;
    Identificacion2: TTabSheet;
    SO_INGLES: TZetaDBNumero;
    LblIngles: TLabel;
    Label2: TLabel;
    SO_IDIOMA2: TDBEdit;
    LblIdioma: TLabel;
    LblDominio: TLabel;
    SO_DOMINA2: TZetaDBNumero;
    Label4: TLabel;
    EArchivo: TEdit;
    SpeedButton1: TSpeedButton;
    Label1: TLabel;
    DO_TIPO: TDBComboBox;
    DO_OBSERVA: TDBEdit;
    Label5: TLabel;
    Label3: TLabel;
    AdicionalScrollBox: TScrollBox;
    PanelReferencia: TPanel;
    PanelNumRef: TPanel;
    OpenDialog: TOpenDialog;
    OpenPictureDialog: TOpenPictureDialog;
    SavePictureDialog: TSavePictureDialog;
    ListaInteres: TCheckListBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    LblEscuela: TLabel;
    SO_ESTUDIO: TZetaDBKeyCombo;
    LblEdoCivil: TLabel;
    SO_EDO_CIV: TZetaDBKeyCombo;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Label6: TLabel;
    SO_PAIS: TDBEdit;
    Label7: TLabel;
    SO_CURP: TDBEdit;
    Label8: TLabel;
    SO_RFC: TDBEdit;
    Direccion: TTabSheet;
    Panel10: TPanel;
    Label9: TLabel;
    SO_CALLE: TDBEdit;
    Label10: TLabel;
    SO_COLONIA: TDBEdit;
    Label11: TLabel;
    SO_CODPOST: TDBEdit;
    Label12: TLabel;
    SO_CIUDAD: TDBEdit;
    lblEstado: TLabel;
    Label14: TLabel;
    SO_NACION: TDBEdit;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    SO_TEL: TDBEdit;
    Label18: TLabel;
    SO_EMAIL: TDBEdit;
    Label20: TLabel;
    SO_ESCUELA: TDBEdit;
    SO_ESTADO: TZetaDBKeyLookup;
    ZYear: TZetaNumero;
    Label21: TLabel;
    ZMonth: TZetaNumero;
    Label22: TLabel;
    lblTransporte: TLabel;
    SO_MED_TRA: TZetaDBKeyLookup;
    DocEntregadaGb: TGroupBox;
    OtrosRequisitosGb: TGroupBox;
    RL_CAMPO1Pnl: TPanel;
    RL_CAMPO1Lbl: TLabel;
    RL_CAMPO1: TDBEdit;
    RL_CAMPO2Pnl: TPanel;
    RL_CAMPO2Lbl: TLabel;
    RL_CAMPO2: TDBEdit;
    RL_CAMPO3Pnl: TPanel;
    RL_CAMPO3Lbl: TLabel;
    RL_CAMPO3: TDBEdit;
    RL_CAMPO4Pnl: TPanel;
    RL_CAMPO4Lbl: TLabel;
    RL_CAMPO4: TDBEdit;
    RL_CAMPO5Pnl: TPanel;
    RL_CAMPO5Lbl: TLabel;
    RL_CAMPO5: TDBEdit;
    RL_CAMPO6Pnl: TPanel;
    RL_CAMPO6Lbl: TLabel;
    RL_CAMPO6: TDBEdit;
    RL_CAMPO7Pnl: TPanel;
    RL_CAMPO7Lbl: TLabel;
    RL_CAMPO7: TDBEdit;
    RL_CAMPO8Pnl: TPanel;
    RL_CAMPO8Lbl: TLabel;
    RL_CAMPO8: TDBEdit;
    RL_CAMPO9Pnl: TPanel;
    RL_CAMPO9Lbl: TLabel;
    RL_CAMPO9: TDBEdit;
    RL_CAMPO10Pnl: TPanel;
    RL_CAMPO10Lbl: TLabel;
    RL_CAMPO10: TDBEdit;
    Referencias2: TTabSheet;
    PanelNumRef2: TPanel;
    Panel12: TPanel;
    RL_CAMPO11Pnl: TPanel;
    RL_CAMPO11Lbl: TLabel;
    RL_CAMPO11: TDBEdit;
    RL_CAMPO12Pnl: TPanel;
    RL_CAMPO12Lbl: TLabel;
    RL_CAMPO12: TDBEdit;
    RL_CAMPO13Pnl: TPanel;
    RL_CAMPO13Lbl: TLabel;
    RL_CAMPO13: TDBEdit;
    RL_CAMPO14Pnl: TPanel;
    RL_CAMPO14Lbl: TLabel;
    RL_CAMPO14: TDBEdit;
    RL_CAMPO15Pnl: TPanel;
    RL_CAMPO15Lbl: TLabel;
    RL_CAMPO15: TDBEdit;
    RL_CAMPO16Pnl: TPanel;
    RL_CAMPO16Lbl: TLabel;
    RL_CAMPO16: TDBEdit;
    RL_CAMPO17Pnl: TPanel;
    RL_CAMPO17Lbl: TLabel;
    RL_CAMPO17: TDBEdit;
    RL_CAMPO18Pnl: TPanel;
    RL_CAMPO18Lbl: TLabel;
    RL_CAMPO18: TDBEdit;
    RL_CAMPO19Pnl: TPanel;
    RL_CAMPO19Lbl: TLabel;
    RL_CAMPO19: TDBEdit;
    RL_CAMPO20Pnl: TPanel;
    RL_CAMPO20Lbl: TLabel;
    RL_CAMPO20: TDBEdit;
    Label13: TLabel;
    Label19: TLabel;
    SO_NUM_EXT: TDBEdit;
    SO_NUM_INT: TDBEdit;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure bDocumentoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure tbLeerFotoClick(Sender: TObject);
    procedure tbGrabarFotoClick(Sender: TObject);
    procedure tbLeerTwainClick(Sender: TObject);
    procedure tbSelectTwainClick(Sender: TObject);
    procedure EArchivoChange(Sender: TObject);
    procedure DO_TIPOChange(Sender: TObject);
    procedure ListaInteresClickCheck(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ZYearExit(Sender: TObject);
    procedure ZMonthExit(Sender: TObject);
  private
    { Private declarations }
    FFirstAdicional : TWinControl;
    FTieneTwain: Boolean;
    FRevisaTwain: Boolean;
    FListacodigos: TStringList;
    rMonthActual: Real;
    rYearActual: Real;

    procedure DoConnect;
    procedure DoDisconnect;
    procedure Connect;
    procedure Disconnect;
    function CancelarWizard: Boolean;
    function EjecutarWizard: Boolean;
    procedure TerminarWizard;

    procedure SetAdicionales;
    procedure SetReferencias;
    procedure SetRequisitos;
    procedure TraspasaListaInteres;
    procedure CambiaFechaResidencia;
  public
    { Public declarations }
  end;

const
     K_MAX_AREA_INTERES = 3;
var
  AltaSolicitud: TAltaSolicitud;

implementation

uses DSeleccion,
     DGlobal,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaAdicionalMgr,
     ZetaClientDataSet,
     ZetaDialogo,
     ZGlobalTress,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaClientTools,
     FToolsSeleccion,
     DLL95V1,
     FToolsFoto;

{$R *.DFM}

procedure TAltaSolicitud.FormShow(Sender: TObject);
begin
     inherited;
     DoConnect;
     with dmSeleccion do
     begin
          CargaListaInteres( TStringList( ListaInteres.Items ), FListaCodigos );
     end;
end;

procedure TAltaSolicitud.FormClose(Sender: TObject;var Action: TCloseAction);
begin
     inherited;
     DoDisconnect;
     Action := caHide;
end;

procedure TAltaSolicitud.Connect;
begin
     with dmSeleccion do
     begin
          cdsAreaInteres.Conectar;
          cdsPuestos.Conectar;
          DataSource.DataSet:= cdsEditSolicita;
          dsRefLaboral.DataSet := cdsRefLaboral;
          dsDocumentos.DataSet := cdsEditDocumentos;
          if SetControlesEmpresaTress( lblEstado, SO_ESTADO ) then
             cdsEstados.Conectar;
          if SetControlesEmpresaTress( lblTransporte, SO_MED_TRA ) then
             cdsTransporte.Conectar;
     end;

     Documentos.Enabled := ZAccesosMgr.CheckDerecho( D_SOLICITUD_DODUMENTO, K_DERECHO_ALTA );
     SetAdicionales;
     SetReferencias;
     SetRequisitos;

     EArchivo.Text := '';
     PageControl.ActivePage := Identificacion;
     SO_APE_PAT.SetFocus;
end;

procedure TAltaSolicitud.Disconnect;
begin
     Datasource.Dataset := nil;
end;

procedure TAltaSolicitud.DoConnect;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             try
                Connect;
             except
                   on Error: Exception do
                      zExcepcion( '� Error Al Conectar Forma !', '� Ventana No Pudo Ser Conectada A Sus Datos !', Error, 0 );
             end;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

procedure TAltaSolicitud.DoDisconnect;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Disconnect;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

procedure TAltaSolicitud.WizardAlEjecutar(Sender: TObject;var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        Application.ProcessMessages;
        TraspasaListaInteres;
     if ( strLleno( dmSeleccion.cdsEditSolicita.FieldByName('SO_FOTO').AsString ) ) then
     begin
        {OP: 05/06/08}
           with FOTOGRAFIA do
           begin
                CutToClipboard;
                UpdateAsJPG:=True;
                PastefromClipboard;
           end;
        try //acl1           
           if( FOTOGRAFIA.Picture.Bitmap.Width > 300 ) then
               FToolsFoto.ResizeImageBestFit( FOTOGRAFIA, 300, 300 );
        except
           on Error: Exception do
           begin
                ZError( self.Caption,'No se pudo ajustar el tama�o de la fotograf�a. Se guardar� con su tama�o original'+CR_LF+Error.Message, 0);
           end;
        end;
      end;
        try
           lOk := EjecutarWizard;
        except
              on Error: Exception do
              begin
                   zExcepcion( Caption, 'Error Al Ejecutar Wizard', Error, 0 );
                   lOk := False;
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     if lOk then
        TerminarWizard;
end;

function TAltaSolicitud.EjecutarWizard: Boolean;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Result := TRUE;
             with dmSeleccion do
             begin
                  cdsEditSolicita.Enviar;
                  if ( cdsEditSolicita.ChangeCount = 0 ) then
                  begin
                       if CargaDocumento(eArchivo.text) then
                       begin
                            with cdsEditDocumentos do
                            begin
                                 Edit;
                                 FieldByName('SO_FOLIO').AsInteger := cdsEditSolicita.FieldByName('SO_FOLIO').AsInteger;
                                 Enviar;
                                 cdsDocumentos.Append;
                                 try
                                    SetCambiosDocumentos;
                                 finally
                                    cdsDocumentos.MergeChangeLog;
                                 end;
                            end;
                       end
                       else if Strlleno( EArchivo.Text ) then
                       begin
                            Wizard.Saltar( 5 );
                            EArchivo.SetFocus;
                       end
                       else cdsEditDocumentos.Cancel;
                  end;
             end;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

function TAltaSolicitud.CancelarWizard: Boolean;
begin
     Close;
     Result := True;
end;

procedure TAltaSolicitud.TerminarWizard;
begin
     if not Wizard.Reejecutar then
     begin
          Close;
          Windows.SetForegroundWindow( Handle ); { Evita que la aplicaci�n pierda el Focus }
     end;
end;

procedure TAltaSolicitud.WizardAlCancelar(Sender: TObject;
  var lOk: Boolean);
begin
     inherited;
     lOk := CancelarWizard;
end;

procedure TAltaSolicitud.WizardAfterMove(Sender: TObject);
begin
     inherited;
     with Wizard do
          if EsPaginaActual( Identificacion ) then
             SO_APE_PAT.SetFocus
          else if EsPaginaActual( Direccion ) then
               SO_CALLE.SetFocus
          else if EsPaginaActual( Identificacion2 ) then
               SO_ESTUDIO.SetFocus
          else if EsPaginaActual( Solicitud ) then
               SO_FEC_INI.SetFocus
          else if EsPaginaActual( Adicionales ) then
          begin
               if  FFirstAdicional <> NIL then
                   FFirstAdicional.SetFocus
          end
          else if EsPaginaActual(Requisitos ) then
               SO_REQUI_1.SetFocus
          else if EsPaginaActual( Referencias1 ) then
               RL_CAMPO1.SetFocus
          else if EsPaginaActual( Referencias2 ) then {OP:24.Abr.08}
               RL_CAMPO11.SetFocus
          else if EsPaginaActual( Documentos ) then
               DO_TIPO.SetFocus
          else if EsPaginaActual( Notas ) then
               SO_OBSERVA.SetFocus;
end;

{OP:24.Abr.08}
procedure TAltaSolicitud.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     CanMove := TRUE;
     with Wizard do
     begin
          if Adelante then
          begin
               if EsPaginaActual(Identificacion) then
               begin
                    with dmSeleccion.cdsEditSolicita do
                    begin
                         if StrVacio(FieldByName('SO_APE_PAT').AsString) then
                         begin
                              CanMove := FALSE;
                              FieldByName('SO_APE_PAT').FocusControl;
                              DataBaseError('El Apellido Paterno no Puede Quedar Vac�o');
                         end;
                         if StrVacio(FieldByName('SO_NOMBRES').AsString) then
                         begin
                              CanMove := FALSE;
                              FieldByName('SO_NOMBRES').FocusControl;
                              DataBaseError('El Nombre no Puede Quedar Vac�o');
                         end;
                         if FieldByName('SO_FEC_NAC').AsDateTime = NullDateTime then
                         begin
                              CanMove := FALSE;
                              FieldByName('SO_FEC_NAC').FocusControl;
                              DataBaseError('La Fecha de Nacimiento no Puede Quedar Vac�a');
                         end;
                    end;
               end;
          end;
          if EsPaginaActual(Requisitos) OR
             EsPaginaActual(Referencias1) OR
             EsPaginaActual(Referencias2) then
          begin
               if dsRefLaboral.Dataset <> NIL then
                  with dsRefLaboral.Dataset do
                  begin
                       if EsPaginaActual(Referencias1) then
                       begin
                            if Adelante and Referencias2.Enabled then
                            begin
                                 CanMove := True;
                            end
                            else if Adelante then Next
                            else Prior;

                            if NOT Referencias2.Enabled then
                            begin
                                 CanMove := BOF or EOF;
                                 if NOT CanMove then
                                    RL_CAMPO1.SetFocus;
                            end
                            else
                            begin
                                 if not BOF then
                                    iNewPage := Referencias2.PageIndex;
                            end;
                                    
                       end
                       else if EsPaginaActual( Referencias2 ) then
                       begin
                            if Adelante then Next
                            else iNewPage := Referencias1.PageIndex;

                            CanMove := True;

                            if not EOF then
                            begin
                                 iNewPage := Referencias1.PageIndex;
                            end;
                       end;

                       PanelNumRef.Caption := Format( ' Empleos Anteriores #%d de %d', [RecNo,RecordCount] );
                       PanelNumRef2.Caption := PanelNumRef.Caption;
                  end;
          end;
     end;
end;

procedure TAltaSolicitud.SetAdicionales;
var
   FCampos: TListaCampos;

   procedure LimpiaAdicionales;
   var
      i : Integer;
   begin
        for i := AdicionalScrollBox.ControlCount - 1 downto 0 do
            AdicionalScrollBox.Controls[i].Free;
   end;

   procedure DrawAdicionales;
   var
      i, iTop, iMiddle, iHeight, iWidth: Integer;
   begin
        iTop := 2;
        iWidth := AdicionalScrollBox.Width;
        with FCampos do
        begin
             for i := 0 to ( Count - 1 ) do
             begin
                  iMiddle := Trunc( iWidth / 3 );
                  with Campo[ i ] do
                  begin
                       if Capturar then
                       begin
                            with TStaticText.Create( Self ) do
                            begin
                                 Name := Format( '%s_LBL', [ Campo ] );
                                 Parent := AdicionalScrollBox;
                                 Alignment := taRightJustify;
                                 Height := 13;
                                 Top := iTop + 3;
                                 Caption := Letrero + ':';
                                 Left := iMiddle - 2 - Width;

                            end;
                            case Tipo of
                                 cdtTexto:
                                 begin
                                      with TDBEdit.Create( Forma ) do
                                      begin
                                           Name := Format( '%s', [ Campo ] );
                                           DataField := Campo;
                                           Parent := AdicionalScrollBox;;
                                           iHeight := Height;
                                           Top := iTop;
                                           Left := iMiddle + 2;
                                           //Width := iWidth - Left - 2;
                                           Width := 300;
                                           Enabled := True;
                                           Datasource := Self.Datasource;
                                      end;
                                 end;
                                 cdtNumero:
                                 begin
                                      with TZetaDBNumero.Create( Self ) do
                                      begin
                                           Name := Format( '%s', [ Campo ] );
                                           DataField := Campo;
                                           Parent := AdicionalScrollBox;;
                                           iHeight := Height;
                                           Top := iTop;
                                           Left := iMiddle + 2;
                                           Mascara := mnNumeroGlobal;
                                           Enabled := True;
                                           Datasource := Self.Datasource;
                                      end;
                                 end;
                                 cdtFecha:
                                 begin
                                      with TZetaDBFecha.Create( Self ) do
                                      begin
                                           Name := Format( '%s', [ Campo ] );
                                           DataField := Campo;
                                           Parent := AdicionalScrollBox;;
                                           iHeight := Height;
                                           Top := iTop;
                                           Left := iMiddle + 2;
                                           Enabled := True;
                                           Opcional := TRUE;
                                           Datasource := Self.Datasource;
                                      end;
                                 end;
                                 cdtBooleano:
                                 begin
                                      with TDBCheckBox.Create( Self ) do
                                      begin
                                           Name := Format( '%s', [ Campo ] );
                                           DataField := Campo;
                                           Caption := '';
                                           Parent := AdicionalScrollBox;;
                                           iHeight := Height + 2;
                                           Top := iTop;
                                           Left := iMiddle + 2;
                                           Width := 25;
                                           AllowGrayed := False;
                                           Enabled := True;
                                           ValueChecked := K_GLOBAL_SI;
                                           ValueUnchecked := K_GLOBAL_NO;
                                           Datasource := Self.Datasource;
                                      end;
                                 end;
                                 cdtLookup:
                                 begin
                                      with TZetaDBKeyLookup.Create( Self ) do
                                      begin
                                           Name := Format( '%s', [ Campo ] );
                                           DataField := Campo;
                                           Parent := AdicionalScrollBox;;
                                           iHeight := Height;
                                           Top := iTop;
                                           Left := iMiddle + 2;
                                           Enabled := True;
                                           Datasource := Self.Datasource;
                                           LookupDataSet := dmSeleccion.GetcdsAdicional( Global );
                                           LookupDataSet.Conectar;
                                           Opcional := TRUE;
                                      end;
                                 end;
                            else
                                iHeight := 0;
                            end;
                            iTop := iTop + iHeight + 1;
                            if FFirstAdicional = NIL then
                               FFirstAdicional := TWinControl(AdicionalScrollBox.FindChildControl(Campo));

                       end;
                  end;
             end;
        end;
   end; { DrawAdicionales }

begin { SetAdicionales }
     LimpiaAdicionales;
     FCampos := TListaCampos.Create;
     try
        dmSeleccion.LlenaAdicionales( FCampos );
        with Adicionales do
        begin
             Enabled := ( FCampos.Count > 0 );
             if Enabled then
                DrawAdicionales;
        end;
     finally
            FCampos.Free;
     end;
end;

{OP:23.Abr.08 Modificaci�n}
procedure TAltaSolicitud.SetRequisitos;
var
   lHay: Boolean;
   DocumentoCoord: TCoordenadas;
   OtrosCoord: TCoordenadas;
   
   procedure SetRequisito( const iGlobal, iGlobalTipo: Word; Control: TDBCheckBox; Etiqueta: TLabel );
   const
        K_ALTURA_OTROS     = 17;
   var
      sEtiqueta: String;
      lOk: Boolean;
      TipoRequisito: eRequisitoTipo;
   begin
        TipoRequisito := eRequisitoTipo( Global.GetGlobalInteger( iGlobalTipo ) );
        sEtiqueta := Global.GetGlobalString( iGlobal );

        case TipoRequisito of
             rtDocumento:
             begin
                  lOk := FToolsSeleccion.AcomodaControles( DocumentoCoord, sEtiqueta, DocEntregadaGb, Etiqueta, Control );
                  OtrosRequisitosGb.Top  := OtrosRequisitosGb.Top + K_ALTURA_OTROS;
             end;
             rtOtro: lOk := FToolsSeleccion.AcomodaControles( OtrosCoord, sEtiqueta, OtrosRequisitosGb, Etiqueta, Control );
        else
            lOk := False;
        end;
        
        if lOk then
           lHay := True;
   end;

begin
     with DocumentoCoord do
     begin
          TopEtiq := 20;
          TopContr := 20;
          LeftEtiq := 30;
          LeftContr := 10;
          Height := 41;
     end;

     with OtrosCoord do
     begin
          TopEtiq := 20;
          TopContr := 20;
          LeftEtiq := 30;
          LeftContr := 10;
          Height    := 41;
     end;

     DocEntregadaGb.Top := 32;
     OtrosRequisitosGb.Top := 76;
     DocEntregadaGb.Height := DocumentoCoord.Height;
     OtrosRequisitosGb.Height := OtrosCoord.Height;

     lHay := False;
     {SetRequisito( K_GLOBAL_REQUISITO1, SO_REQUI_1, SO_REQUI_1lbl );
     SetRequisito( K_GLOBAL_REQUISITO2, SO_REQUI_2, SO_REQUI_2lbl );
     SetRequisito( K_GLOBAL_REQUISITO3, SO_REQUI_3, SO_REQUI_3lbl );
     SetRequisito( K_GLOBAL_REQUISITO4, SO_REQUI_4, SO_REQUI_4lbl );
     SetRequisito( K_GLOBAL_REQUISITO5, SO_REQUI_5, SO_REQUI_5lbl );
     SetRequisito( K_GLOBAL_REQUISITO6, SO_REQUI_6, SO_REQUI_6lbl );
     SetRequisito( K_GLOBAL_REQUISITO7, SO_REQUI_7, SO_REQUI_7lbl );
     SetRequisito( K_GLOBAL_REQUISITO8, SO_REQUI_8, SO_REQUI_8lbl );
     SetRequisito( K_GLOBAL_REQUISITO9, SO_REQUI_9, SO_REQUI_9lbl );}
     SetRequisito( K_GLOBAL_REQUISITO1, K_GLOBAL_TPO_REQUISITO1, SO_REQUI_1, NIL );
     SetRequisito( K_GLOBAL_REQUISITO2, K_GLOBAL_TPO_REQUISITO2, SO_REQUI_2, NIL );
     SetRequisito( K_GLOBAL_REQUISITO3, K_GLOBAL_TPO_REQUISITO3, SO_REQUI_3, NIL );
     SetRequisito( K_GLOBAL_REQUISITO4, K_GLOBAL_TPO_REQUISITO4, SO_REQUI_4, NIL );
     SetRequisito( K_GLOBAL_REQUISITO5, K_GLOBAL_TPO_REQUISITO5, SO_REQUI_5, NIL );
     SetRequisito( K_GLOBAL_REQUISITO6, K_GLOBAL_TPO_REQUISITO6, SO_REQUI_6, NIL );
     SetRequisito( K_GLOBAL_REQUISITO7, K_GLOBAL_TPO_REQUISITO7, SO_REQUI_7, NIL );
     SetRequisito( K_GLOBAL_REQUISITO8, K_GLOBAL_TPO_REQUISITO8, SO_REQUI_8, NIL );
     SetRequisito( K_GLOBAL_REQUISITO9, K_GLOBAL_TPO_REQUISITO9, SO_REQUI_9, NIL );
     { Prende / Apaga el TabSheet }
     Requisitos.Enabled := lHay;
end;

{OP:24.Abr.08}
procedure TAltaSolicitud.SetReferencias;
const
     K_REF_MAX_TAB = 10;
var
   FNumReferencias : Integer;
   procedure SetPanelLabel( oPanel: TPanel; oLabel: TLabel;
                            const iGlobal, iRef: Integer );
   begin
        oLabel.Caption := Global.GetGlobalString( iGlobal );
        oPanel.Visible := ( iRef <= FNumReferencias ) and strLleno( oLabel.Caption );
        oLabel.Caption := oLabel.Caption + ':';
   end;
begin
     FNumReferencias := Global.NumCamposRefLab;
     Referencias1.Enabled := ( Global.GetGlobalInteger( K_GLOBAL_REF_LABORAL_QTY ) > 0 );

     if Referencias1.Enabled then
     begin
          SetPanelLabel( RL_CAMPO1Pnl, RL_CAMPO1Lbl, K_GLOBAL_REF_LABORAL1, 1 );
          SetPanelLabel( RL_CAMPO2Pnl, RL_CAMPO2Lbl, K_GLOBAL_REF_LABORAL2, 2 );
          SetPanelLabel( RL_CAMPO3Pnl, RL_CAMPO3Lbl, K_GLOBAL_REF_LABORAL3, 3 );
          SetPanelLabel( RL_CAMPO4Pnl, RL_CAMPO4Lbl, K_GLOBAL_REF_LABORAL4, 4 );
          SetPanelLabel( RL_CAMPO5Pnl, RL_CAMPO5Lbl, K_GLOBAL_REF_LABORAL5, 5 );
          SetPanelLabel( RL_CAMPO6Pnl, RL_CAMPO6Lbl, K_GLOBAL_REF_LABORAL6, 6 );
          SetPanelLabel( RL_CAMPO7Pnl, RL_CAMPO7Lbl, K_GLOBAL_REF_LABORAL7, 7 );
          SetPanelLabel( RL_CAMPO8Pnl, RL_CAMPO8Lbl, K_GLOBAL_REF_LABORAL8, 8 );
          SetPanelLabel( RL_CAMPO9Pnl, RL_CAMPO9Lbl, K_GLOBAL_REF_LABORAL9, 9 );
          SetPanelLabel( RL_CAMPO10Pnl, RL_CAMPO10Lbl, K_GLOBAL_REF_LABORAL10, 10 );
          PanelReferencia.Height := Trunc((K_REF_MAX_TAB - FNumReferencias)/2) * 24
     end;

     if FNumReferencias > 10 then
     begin
          Referencias2.Enabled := Referencias1.Enabled;

          if Referencias2.Enabled then
          begin
               SetPanelLabel( RL_CAMPO11Pnl, RL_CAMPO11Lbl, K_GLOBAL_REF_LABORAL11, 11 );
               SetPanelLabel( RL_CAMPO12Pnl, RL_CAMPO12Lbl, K_GLOBAL_REF_LABORAL12, 12 );
               SetPanelLabel( RL_CAMPO13Pnl, RL_CAMPO13Lbl, K_GLOBAL_REF_LABORAL13, 13 );
               SetPanelLabel( RL_CAMPO14Pnl, RL_CAMPO14Lbl, K_GLOBAL_REF_LABORAL14, 14 );
               SetPanelLabel( RL_CAMPO15Pnl, RL_CAMPO15Lbl, K_GLOBAL_REF_LABORAL15, 15 );
               SetPanelLabel( RL_CAMPO16Pnl, RL_CAMPO16Lbl, K_GLOBAL_REF_LABORAL16, 16 );
               SetPanelLabel( RL_CAMPO17Pnl, RL_CAMPO17Lbl, K_GLOBAL_REF_LABORAL17, 17 );
               SetPanelLabel( RL_CAMPO18Pnl, RL_CAMPO18Lbl, K_GLOBAL_REF_LABORAL18, 18 );
               SetPanelLabel( RL_CAMPO19Pnl, RL_CAMPO19Lbl, K_GLOBAL_REF_LABORAL19, 19 );
               SetPanelLabel( RL_CAMPO20Pnl, RL_CAMPO20Lbl, K_GLOBAL_REF_LABORAL20, 20 );
               PanelReferencia.Height := Trunc((10 - FNumReferencias)/2) * 24
          end;
     end
     else
         Referencias2.Enabled := ( FNumReferencias > 10 );

end;

procedure TAltaSolicitud.DataSourceDataChange(Sender: TObject; Field: TField);
var
   dNacimiento : TDateTime;
   rValor: Double;
begin
     inherited;
     // Edad
     rValor := 0;
     with dmSeleccion.cdsEditSolicita do
     begin
          if ( Field = nil ) or ( Field.FieldName = 'SO_FEC_NAC' ) then
          begin
               dNacimiento := FieldByName( 'SO_FEC_NAC' ).AsDateTime;
               if ( dNacimiento > 0 ) then
                  rValor := Trunc( Now - dNacimiento );
               ZEdad.Caption := TiempoDias( rValor, etYear );
          end;
     end;
end;

procedure TAltaSolicitud.bDocumentoClick(Sender: TObject);
begin
     inherited;
     dmSeleccion.AbreDocumento;
end;

procedure TAltaSolicitud.FormCreate(Sender: TObject);
var
   eSex : eSexo;
   eEdo : eEdoCivil;
begin
     inherited;
     FRevisaTwain := True;
     with SO_SEXO.Lista do
     begin
          BeginUpdate;
          try
             Clear;
             for eSex := Low( eSexo ) to High( eSexo ) do
             begin
                  Add( Format( '%s=%s', [ ZetaCommonLists.ObtieneElemento( lfSexo, Ord( eSex ) ), ZetaCommonLists.ObtieneElemento( lfSexoDesc, Ord( eSex ) ) ] ) );
             end;
          finally
                 EndUpdate;
          end;
     end;
     with SO_EDO_CIV.Lista do
     begin
          BeginUpdate;
          try
             Clear;
             for eEdo := Low( eEdoCivil ) to High( eEdoCivil ) do
             begin
                  Add( Format( '%s=%s', [ ZetaCommonLists.ObtieneElemento( lfEdoCivil, Ord( eEdo ) ), ZetaCommonLists.ObtieneElemento( lfEdoCivilDesc, Ord( eEdo ) ) ] ) );
             end;
          finally
                 EndUpdate;
          end;
     end;
     FListaCodigos := TStringList.Create;
     SO_ESTADO.LookupDataset := dmSeleccion.cdsEstados;
     SO_MED_TRA.LookupDataSet := dmSeleccion.cdsTransporte;
     DO_TIPO.CharCase := ecUpperCase;
end;

procedure TAltaSolicitud.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FListaCodigos );
     inherited;
end;

procedure TAltaSolicitud.SpeedButton1Click(Sender: TObject);
begin
     inherited;
     with OpenDialog do
     begin
          FileName := EArchivo.Text;
          if Execute then
          begin
               EArchivo.Text := FileName;
          end;
     end;

end;

procedure TAltaSolicitud.tbLeerFotoClick(Sender: TObject);
begin
     inherited;
     if OpenPictureDialog.Execute then
     begin
          with dmSeleccion.cdsEditSolicita do
               if ( not ( State in [ dsEdit, dsInsert ] ) ) then
                  Edit;
          FOTOGRAFIA.LoadFromFile( OpenPictureDialog.FileName );
     end;
end;

procedure TAltaSolicitud.tbGrabarFotoClick(Sender: TObject);
begin
     inherited;
     SavePictureDialog.FilterIndex := 0;
     SavePictureDialog.FileName := '';
     if SavePictureDialog.Execute then
     begin
          with FOTOGRAFIA do
          begin
               case SavePictureDialog.FilterIndex of
                    1,2:
                    begin
                         if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'JPG';
                         SaveToFileAsJPG(SavePictureDialog.FileName);
                    end;
                    3:
                    begin
                         if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'BMP';
                         SaveToFileAsBMP(SavePictureDialog.FileName);
                    end;
                    4:
                    begin
                         if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'GIF';
                         SaveToFileAsGIF(SavePictureDialog.FileName);
                    end;
                    5:
                    begin
                         if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'TIF';
                         SaveToFileAsTIF(SavePictureDialog.FileName);
                    end;
                    6:
                    begin
                         if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'PCX';
                         SaveToFileAsPCX(SavePictureDialog.FileName);
                    end;
                    7:
                    begin
                         if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'PNG';
                         SaveToFileAsPNG(SavePictureDialog.FileName);
                    end;
               end;
          end;
     end;
end;

procedure TAltaSolicitud.tbLeerTwainClick(Sender: TObject);
begin
     inherited;
     if FRevisaTwain then
     begin
          FRevisaTwain:= False;
          FTieneTwain:= HasTwain( Handle );
     end;
     if FTieneTwain then
     begin
         try
            with dmSeleccion.cdsEditSolicita do
                 if ( not ( State in [ dsEdit, dsInsert ] ) ) then
                    Edit;
            with FOTOGRAFIA do
            begin
                 UseTwainWindow:= True;
                 ScanImage( Handle );
                 Refresh;
            end;
         except
            ZError('','No se pudo obtener la fotograf�a - Error en Dispositivo Twain',0);
         end;
     end;
end;

procedure TAltaSolicitud.tbSelectTwainClick(Sender: TObject);
begin
     inherited;
     try
        FOTOGRAFIA.SelectScanner(Handle);
     except
        ZError('','No se pudo Seleccionar - Error en Dispositivo Twain',0);
     end;

end;

procedure TAltaSolicitud.EArchivoChange(Sender: TObject);
begin
     inherited;
     with dmSeleccion.cdsEditDocumentos do
     begin
          Edit;
          FieldByName('DO_OBSERVA').AsString :=
          Copy(ExtractFileName(EArchivo.Text),1,Pos('.',EArchivo.Text)-1);
     end;
end;

procedure TAltaSolicitud.DO_TIPOChange(Sender: TObject);
 var sTipo : string;
begin
     inherited;
     sTipo := DO_TIPO.Text;
     if ( sTipo = 'HUELLA') or ( sTipo  = 'FOTO') then
          OpenDialog.FilterIndex := 2
     else if (sTipo  = 'AUDIO') or ( sTipo = 'VOZ') then
          OpenDialog.FilterIndex := 3
     else
         OpenDialog.FilterIndex := 1
end;

procedure TAltaSolicitud.ListaInteresClickCheck(Sender: TObject);
var
   i, iCuantos: Integer;
   lEnabled : Boolean;
begin
     inherited;
     iCuantos := 0;
     lEnabled := TRUE;
     with TCheckListBox( Sender ) do
     begin
          Items.BeginUpdate;
          try
             // Revisa cuantos estan seleccionados
             for i:=0 to Items.Count - 1 do
             begin
                  if Checked[i] then
                     Inc( iCuantos );
                  if ( iCuantos = K_MAX_AREA_INTERES ) then
                  begin
                       lEnabled := FALSE;
                       Break;
                  end;
             end;
             // Habilita o Inhabilita dependiendo si ya se tienen tres items checked.
             for i:= 0 to Items.Count - 1 do
             begin
                  if not Checked[i] then
                     ItemEnabled[i] := lEnabled;
             end;
          finally
             Items.EndUpdate;
          end;
     end;
end;

procedure TAltaSolicitud.TraspasaListaInteres;
var
   i, iCampo : Integer;
begin
     iCampo := 0;
     with ListaInteres do
          for i:=0 to Items.Count - 1 do
              if Checked[i] then
              begin
                   Inc( iCampo );
                   if ( iCampo <= K_MAX_AREA_INTERES ) then
                   begin
                        with dmSeleccion.cdsEditSolicita do
                        begin
                             if ( not ( State in [ dsEdit, dsInsert ] ) ) then
                                Edit;
                             FieldByName( Format( 'SO_AREA_%d', [ iCampo ] ) ).AsString := FListaCodigos[i];
                        end;
                   end
                   else
                      Break;
              end;
end;

procedure TAltaSolicitud.ZYearExit(Sender: TObject);
begin
     if ( rYearActual <> ZYear.Valor ) then
     begin
          CambiaFechaResidencia;
          rYearActual := ZYear.Valor;
     end;
end;

procedure TAltaSolicitud.ZMonthExit(Sender: TObject);
begin
     if ( rMonthActual <> ZMonth.Valor ) then
     begin
          if ZMonth.Valor >= 12 then
          begin
             ZError( VACIO, 'Meses Tiene Que Ser Menor a 12', 0);
             ActiveControl:= ZMonth;
          end
          else
          begin
               CambiaFechaResidencia;
               rMonthActual := ZMonth.Valor;
          end;
     end;
end;

procedure TAltaSolicitud.CambiaFechaResidencia;
begin
     with dmSeleccion.cdsEditSolicita do
     begin
          if ( not ( State in [ dsEdit, dsInsert ] ) ) then
             Edit;
          FieldByName( 'SO_FEC_RES' ).AsDateTime:= ZetaClientTools.FechaResidencia( ZYear.Valor, ZMonth.Valor );
     end;
end;

end.
