inherited GlobalServidorEmail: TGlobalServidorEmail
  Left = 823
  Top = 185
  Caption = 'Servidor de Correos'
  ClientHeight = 205
  ClientWidth = 455
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 19
    Top = 12
    Width = 96
    Height = 13
    Alignment = taRightJustify
    Caption = 'Servidor de Correos:'
  end
  object Label2: TLabel [1]
    Left = 59
    Top = 83
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'ID. Usuario:'
  end
  object Label6: TLabel [2]
    Left = 24
    Top = 128
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = 'Direcci'#243'n de Email:'
  end
  object Label7: TLabel [3]
    Left = 14
    Top = 151
    Width = 101
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descipci'#243'n del Email:'
  end
  object Label3: TLabel [4]
    Left = 81
    Top = 35
    Width = 34
    Height = 13
    Alignment = taRightJustify
    Caption = 'Puerto:'
  end
  object Label4: TLabel [5]
    Left = 85
    Top = 105
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Clave:'
  end
  object AutenticacionLBL: TLabel [6]
    Left = 47
    Top = 59
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Autenticaci'#243'n:'
    FocusControl = Autenticacion
  end
  inherited PanelBotones: TPanel
    Top = 169
    Width = 455
    TabOrder = 7
    inherited OK: TBitBtn
      Left = 282
    end
    inherited Cancelar: TBitBtn
      Left = 362
    end
  end
  object UserId: TEdit
    Tag = 44
    Left = 118
    Top = 79
    Width = 129
    Height = 21
    MaxLength = 255
    TabOrder = 3
  end
  object UserEmail: TEdit
    Tag = 44
    Left = 118
    Top = 124
    Width = 300
    Height = 21
    MaxLength = 255
    TabOrder = 5
  end
  object DisplayName: TEdit
    Tag = 44
    Left = 118
    Top = 147
    Width = 300
    Height = 21
    MaxLength = 255
    TabOrder = 6
  end
  object Host: TEdit
    Tag = 44
    Left = 118
    Top = 8
    Width = 129
    Height = 21
    MaxLength = 255
    TabOrder = 0
  end
  object Password: TEdit
    Tag = 44
    Left = 118
    Top = 101
    Width = 129
    Height = 21
    MaxLength = 255
    PasswordChar = '*'
    TabOrder = 4
  end
  object PuertoSMTP: TZetaNumero
    Left = 118
    Top = 31
    Width = 128
    Height = 21
    Mascara = mnDias
    TabOrder = 1
    Text = '0'
  end
  object Autenticacion: TZetaKeyCombo
    Left = 118
    Top = 55
    Width = 132
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
    ListaFija = lfAuthTressEmail
    ListaVariable = lvPuesto
    Offset = 1
    Opcional = False
    EsconderVacios = False
  end
end
