inherited CatPuestos: TCatPuestos
  Left = 338
  Top = 257
  Caption = 'Puestos'
  ClientHeight = 277
  ClientWidth = 601
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 601
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 275
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 601
    Height = 258
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'PU_CODIGO'
        Title.Caption = 'C�digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PU_DESCRIP'
        Title.Caption = 'Descripci�n'
        Width = 217
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AI_DESCRIP'
        Title.Caption = 'Area de Interes'
        Width = 142
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PU_INGLES'
        Title.Caption = 'Ingl�s'
        Width = 228
        Visible = True
      end>
  end
end
