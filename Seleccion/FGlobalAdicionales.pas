unit FGlobalAdicionales;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     ZetaAdicionalMgr,
     ZBaseDlgModal,
     ZetaSmartLists, ZetaCommonLists;

type
  TGlobalAdicionales = class(TZetaDlgModal)
    ZetaSmartLists: TZetaSmartLists;
    DisponiblesGB: TGroupBox;
    Disponibles: TZetaSmartListBox;
    PanelEscoger: TPanel;
    Seleccionar: TZetaSmartListsButton;
    Deseleccionar: TZetaSmartListsButton;
    PanelSubirBajar: TPanel;
    Subir: TZetaSmartListsButton;
    Bajar: TZetaSmartListsButton;
    PanelEscogidos: TPanel;
    EscogidosGB: TGroupBox;
    Escogidos: TZetaSmartListBox;
    CampoGB: TGroupBox;
    FieldName: TEdit;
    FieldNameLBL: TLabel;
    EquivalenciaLBL: TLabel;
    cbEquivalencia: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ZetaSmartListsAlEscoger(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure ZetaSmartListsAlRechazar(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure OKClick(Sender: TObject);
    procedure EscogidosClick(Sender: TObject);
    procedure FieldNameChange(Sender: TObject);
    procedure cbEquivalenciaChange(Sender: TObject);
  private
    { Private declarations }
    FCampos: TListaCampos;
    procedure SetControls;
    procedure AgregaGlobal; //( const iGlobal, iGlobalBase, iGlobalEqBase: integer;
              //const eTipo : eCampoTipo; const sTexto: string );
  public
    { Public declarations }
    property Campos: TListaCampos read FCampos;
  end;

var
  GlobalAdicionales: TGlobalAdicionales;

implementation

uses DGlobal,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZGlobalTress;

{$R *.DFM}

procedure TGlobalAdicionales.FormCreate(Sender: TObject);
begin
     inherited;
     FCampos := TListaCampos.Create;
     HelpContext := H00079_Solicitud_Adicionales;
end;

procedure TGlobalAdicionales.FormShow(Sender: TObject);
begin
     inherited;
     Global.InitListaUsados;
     AgregaGlobal;
     Campos.CargarListas( Disponibles.Items, Escogidos.Items );
     Disponibles.Sorted := True;
     ZetaSmartLists.SelectEscogido( 0 );
end;

procedure TGlobalAdicionales.AgregaGlobal;
const
    aTextoCampo: array[ eCampoTipo ] of PChar =( 'Texto', 'N�mero', 'Fecha', 'L�gico', 'Tabla', VACIO );
var
   i, iEquiv, iPos, iValor: integer;
   eTipo : eCampoTipo;
begin
     iEquiv := 0;
     iValor := 0;
     for i := 1 to K_NUM_GLOBALES do
     begin
          eTipo := ZGlobalTress.GetTipoGlobalAdicional( i, iEquiv, iValor );
          if ( eTipo <> cdtNinguno ) then
          begin
               with Global do
               begin
                    iPos := ListaUsados.IndexOf( IntToStr( i ) ) + 1;  //  0-based index
                    Campos.Add( eTipo, GetGlobalString( i ), Format( aTextoCampo[ eTipo ] + ' %s', [ PadL( IntToStr( iValor ), 2 ) ] ), '',
                                GetGlobalString( iEquiv ), iPos, i, iEquiv, ( iPos > 0 ) );
               end;
          end;
     end;
end;

procedure TGlobalAdicionales.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FCampos );
end;

procedure TGlobalAdicionales.SetControls;
begin
     FieldName.OnChange := NIL;
     cbEquivalencia.OnChange := NIL;

     with Escogidos do
     begin
          if ( ItemIndex >= 0 ) then
          begin
               with TCampo( Items.Objects[ ItemIndex ] ) do
               begin
                    with FieldName do
                    begin
                         if ZetaCommonTools.StrVacio( Letrero ) then
                            Letrero := Nombre;
                         Text := Letrero;
                         Enabled := True;
                    end;
                    with cbEquivalencia do
                    begin
                         ItemIndex := Items.IndexOf(Equivalencia);
                         Enabled := True;
                    end;
               end;
          end
          else
          begin
               with FieldName do
               begin
                    Text := '';
                    Enabled := False;
               end;
               cbEquivalencia.Enabled := False;
          end;
     end;

     FieldName.OnChange := FieldNameChange;
     cbEquivalencia.OnChange := cbEquivalenciaChange;

end;

procedure TGlobalAdicionales.ZetaSmartListsAlEscoger(Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     SetControls;
end;

procedure TGlobalAdicionales.ZetaSmartListsAlRechazar(Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     SetControls;
end;

procedure TGlobalAdicionales.EscogidosClick(Sender: TObject);
begin
     inherited;
     SetControls;
end;

procedure TGlobalAdicionales.FieldNameChange(Sender: TObject);
begin
     inherited;
     with Escogidos do
     begin
          if ( ItemIndex >= 0 ) then
          begin
               with Items do
               begin
                    with TCampo( Objects[ ItemIndex ] ) do
                    begin
                         Letrero := FieldName.Text;
                         Strings[ ItemIndex ] := TextoEscogido;
                    end;
               end;
          end;
     end;
end;

procedure TGlobalAdicionales.OKClick(Sender: TObject);
var
   i: Integer;
   FParametros: TZetaParams;
   sEscogidos: String;

procedure CargaGlobal( const iGlobal: Word; const sValor: String );
begin
     FParametros.AddString( Format( '%d', [ iGlobal ] ), sValor );
end;

begin
     inherited;
     with Disponibles.Items do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with TCampo( Objects[ i ] ) do
               begin
                    Orden := 0;
                    Letrero := '';
                    Capturar := False;
               end;
          end;
     end;
     sEscogidos := '';
     with Escogidos.Items do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with TCampo( Objects[ i ] ) do
               begin
                    Orden := i + 1;
                    Capturar := True;
                    if ZetaCommonTools.StrVacio( Letrero ) then
                       Letrero := Nombre;
                    sEscogidos := sEscogidos + Format( '%d,', [ Global ] );
               end;
          end;
     end;
     FParametros := TZetaParams.Create;
     try
        with Campos do
        begin
             for i := 0 to ( Count - 1 ) do
             begin
                  with Campo[ i ] do
                  begin
                       if Capturar then
                       begin
                            CargaGlobal( Global, Letrero );
                            CargaGlobal( GlobalEq, Equivalencia );
                       end
                       else
                       begin
                            CargaGlobal( Global, VACIO );
                            CargaGlobal( GlobalEq, VACIO );
                       end;
                  end;
             end;
        end;
        CargaGlobal( K_GLOBAL_POSICION_ADIC, ZetaCommonTools.CortaUltimo( sEscogidos ) );
        Global.Descargar( FParametros, TRUE );
     finally
            FParametros.Free;
     end;
end;

procedure TGlobalAdicionales.cbEquivalenciaChange(Sender: TObject);
begin
     inherited;
     with Escogidos do
     begin
          if ( ItemIndex >= 0 ) then
          begin
               with Items do
                    TCampo( Objects[ ItemIndex ] ).Equivalencia := cbEquivalencia.Text;
          end;
     end;
end;

end.
