unit FGlobalServidorEmail;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Mask,
     ZBaseGlobal,
     ZetaNumero, ZetaKeyCombo;

type
  TGlobalServidorEmail = class(TBaseGlobal)
    UserId: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label6: TLabel;
    UserEmail: TEdit;
    Label7: TLabel;
    DisplayName: TEdit;
    Host: TEdit;
    Label3: TLabel;
    Password: TEdit;
    Label4: TLabel;
    PuertoSMTP: TZetaNumero;
    AutenticacionLBL: TLabel;
    Autenticacion: TZetaKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GlobalServidorEmail: TGlobalServidorEmail;

implementation

uses DGlobal,
     ZAccesosTress,
     ZGlobalTress,
     ZetaCommonTools,
     ZetaCommonClasses;

{$R *.DFM}

procedure TGlobalServidorEmail.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos     := D_CAT_CONFI_GLOBALES;
     Host.Tag          := K_GLOBAL_EMAIL_HOST;
     PuertoSMTP.Tag    := K_GLOBAL_EMAIL_PORT;
     UserId.Tag        := K_GLOBAL_EMAIL_USERID;
     Password.Tag      := K_GLOBAL_EMAIL_PSWD;
     UserEmail.Tag     := K_GLOBAL_EMAIL_FROMADRESS;
     DisplayName.Tag   := K_GLOBAL_EMAIL_FROMNAME;
     Autenticacion.Tag := K_GLOBAL_EMAIL_AUTH;
     //EMailAnuncio.Tag  := K_GLOBAL_EMAIL_ANUNCIO;
     HelpContext       := H00084_Servidor_Correos;
end;

procedure TGlobalServidorEmail.FormShow(Sender: TObject);
begin
  inherited;
  with Autenticacion do
  begin
       if ItemIndex < 0 then
          ItemIndex := 0;
  end;
end;

end.
