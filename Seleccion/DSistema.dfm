inherited dmSistema: TdmSistema
  OldCreateOrder = True
  Height = 628
  inherited cdsEmpresas: TZetaLookupDataSet
    AlCrearCampos = cdsEmpresasAlCrearCampos
  end
  inherited cdsGrupos: TZetaLookupDataSet
    Aggregates = <
      item
        Active = True
        Expression = 'Max( GR_CODIGO ) + 0'
        Visible = False
      end>
  end
  inherited cdsGruposTodos: TZetaLookupDataSet
    Aggregates = <
      item
        Active = True
        Expression = 'Max( GR_CODIGO ) + 0'
        Visible = False
      end>
  end
  object cdsEmpresasTress: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsEmpresasTressAlAdquirirDatos
    LookupName = 'Empresas'
    LookupDescriptionField = 'CM_NOMBRE'
    LookupKeyField = 'CM_CODIGO'
    Left = 112
    Top = 136
  end
end
