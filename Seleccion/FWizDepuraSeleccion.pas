unit FWizDepuraSeleccion;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Mask, ComCtrls, Buttons, StdCtrls, ExtCtrls,
     ZBaseWizard,
     ZetaFecha,
     ZetaWizard;

type
  TWizDepuraSeleccion = class(TBaseWizard)
    RequiereGB: TGroupBox;
    FechaLBL: TLabel;
    FechaReq: TZetaFecha;
    Label3: TLabel;
    cbStatus: TComboBox;
    SolicitaGB: TGroupBox;
    Label1: TLabel;
    FechaSol: TZetaFecha;
    SolStatusLBL: TLabel;
    SolStatus: TComboBox;
    CBRequiere: TCheckBox;
    CBSolicita: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure CBRequiereClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
  private
    { Private declarations }
    procedure SetControles(oControl: TWinControl; const lEnabled: Boolean);
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

const
     K_CHECK_REQUIERE = 1;

var
  WizDepuraSeleccion: TWizDepuraSeleccion;

implementation

uses DSeleccion, ZetaCommonClasses, ZetaCommonLists, ZetaDialogo;

{$R *.DFM}

procedure TWizDepuraSeleccion.FormCreate(Sender: TObject);

  procedure FillCombo( Control: TComboBox; Tipo: ListasFijas; const sTodos: String;
            const iDefault: Integer );
  begin
       with Control do
       begin
            ZetaCommonLists.LlenaLista( Tipo, Items );
            Items.Insert( 0, Format( '< %s >', [ sTodos ] ) );
            ItemIndex := iDefault;
       end;
  end;

begin
     inherited;
//     HelpContext := H00005_Depuracion;
     FillCombo( cbStatus, lfReqStatus, 'Todos', 2 );
     FillCombo( SolStatus, lfSolStatus, 'Todos', 0 );
     ParametrosControl := CBRequiere;

     FechaReq.Valor := Date - 365;
     FechaSol.Valor := Date - 365;
     CBRequiere.Checked := TRUE;
     CBSolicita.Checked := TRUE;
end;

procedure TWizDepuraSeleccion.CargaParametros;
begin
     inherited;
     with ParameterList do
     begin
          AddDate( 'FechaRequiere', FechaReq.Valor );
          AddDate( 'FechaSolicita', FechaSol.Valor );
          AddBoolean( 'BorrarRequiere', CBRequiere.Checked );
          AddBoolean( 'BorrarSolicita', CBSolicita.Checked );
          AddInteger( 'StatusRequiere', cbStatus.ItemIndex - 1 );      // Por la opci�n agregada < Todos >
          AddInteger( 'StatusSolicita', SolStatus.ItemIndex - 1 );     // Por la opci�n agregada < Todos >
     end;
end;

function TWizDepuraSeleccion.EjecutarWizard: Boolean;
begin
     Result := dmSeleccion.Depuracion( ParameterList );
end;

procedure TWizDepuraSeleccion.CBRequiereClick(Sender: TObject);
begin
     inherited;
     with Sender as TCheckBox do
          if ( Tag = K_CHECK_REQUIERE ) then
             SetControles( RequiereGB, Checked )
          else
             SetControles( SolicitaGB, Checked );
end;

procedure TWizDepuraSeleccion.SetControles( oControl : TWinControl; const lEnabled: Boolean );
var
   i : Integer;
begin
     with oControl do
          for i := 0 to ControlCount - 1 do
              Controls[i].Enabled := lEnabled;
end;

procedure TWizDepuraSeleccion.WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
          var CanMove: Boolean);
begin
     inherited;
     if CanMove and Wizard.Adelante and Wizard.EsPaginaActual( Parametros ) then
     begin
          if ( not CBRequiere.Checked ) and ( not CBSolicita.Checked ) then
          begin
               ZetaDialogo.zError( Caption, 'Debe indicarse al menos un Tipo de Depuraci�n' + CR_LF + '( Requisiciones � Solicitudes )', 0 );
               ActiveControl := CBRequiere;
               CanMove := False;
          end
     end;
end;

end.
