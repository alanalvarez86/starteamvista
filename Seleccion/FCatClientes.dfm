inherited CatClientes: TCatClientes
  Left = 191
  Top = 211
  Caption = 'Clientes'
  ClientHeight = 277
  ClientWidth = 601
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 601
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 275
    end
  end
  object GridClientes: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 601
    Height = 258
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CL_CODIGO'
        Title.Caption = 'C�digo'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CL_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 225
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CL_TEL'
        Title.Caption = 'Tel�fono'
        Width = 115
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CL_DESCRIP'
        Title.Caption = 'Descripci�n'
        Width = 225
        Visible = True
      end>
  end
end
