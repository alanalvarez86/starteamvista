inherited AltaRequisicion: TAltaRequisicion
  Left = 619
  Top = 259
  Width = 473
  Height = 323
  Caption = 'Registro de Requisici'#243'n'
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    Top = 253
    Width = 465
    AlEjecutar = WizardAlEjecutar
    AlCancelar = WizardAlCancelar
    AfterMove = WizardAfterMove
    BeforeMove = WizardBeforeMove
    inherited Anterior: TZetaWizardButton
      Enabled = True
    end
    inherited Salir: TZetaWizardButton
      Left = 379
      Kind = bkCancel
    end
    inherited Ejecutar: TZetaWizardButton
      Left = 296
      Caption = '&Terminar'
    end
  end
  inherited PageControl: TPageControl
    Width = 465
    Height = 253
    ActivePage = Generales
    object Inicial: TTabSheet
      Caption = 'Inicial'
      TabVisible = False
      object Label4: TLabel
        Left = 51
        Top = 176
        Width = 44
        Height = 13
        Caption = 'Prioridad:'
      end
      object Label2: TLabel
        Left = 59
        Top = 74
        Width = 36
        Height = 13
        Caption = 'Puesto:'
      end
      object Label5: TLabel
        Left = 47
        Top = 99
        Width = 48
        Height = 13
        Caption = 'Vacantes:'
      end
      object Label20: TLabel
        Left = 67
        Top = 125
        Width = 28
        Height = 13
        Caption = 'Inici'#243':'
      end
      object Label21: TLabel
        Left = 51
        Top = 151
        Width = 44
        Height = 13
        Caption = 'Promesa:'
      end
      object RQ_PRIORID: TZetaDBKeyCombo
        Left = 100
        Top = 172
        Width = 115
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 4
        ListaFija = lfPrioridad
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        DataField = 'RQ_PRIORID'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object RQ_PUESTO: TZetaDBKeyLookup
        Left = 100
        Top = 70
        Width = 350
        Height = 21
        LookupDataset = dmSeleccion.cdsPuestos
        TabOrder = 0
        TabStop = True
        WidthLlave = 80
        OnValidLookup = RQ_PUESTOValidLookup
        DataField = 'RQ_PUESTO'
        DataSource = DataSource
      end
      object RQ_VACANTE: TZetaDBNumero
        Left = 100
        Top = 95
        Width = 80
        Height = 21
        Mascara = mnEmpleado
        TabOrder = 1
        DataField = 'RQ_VACANTE'
        DataSource = DataSource
      end
      object RQ_FEC_INI: TZetaDBFecha
        Left = 100
        Top = 120
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 2
        Text = '04/Aug/01'
        Valor = 37107.000000000000000000
        DataField = 'RQ_FEC_INI'
        DataSource = DataSource
      end
      object RQ_FEC_PRO: TZetaDBFecha
        Left = 100
        Top = 146
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 3
        Text = '04/Aug/01'
        Valor = 37107.000000000000000000
        DataField = 'RQ_FEC_PRO'
        DataSource = DataSource
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 457
        Height = 25
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = ' Requisici'#243'n'
        Color = clBtnShadow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 5
      end
    end
    object Generales: TTabSheet
      Caption = 'v'
      ImageIndex = 1
      TabVisible = False
      object Label18: TLabel
        Left = 60
        Top = 32
        Width = 35
        Height = 13
        Caption = 'Motivo:'
      end
      object Label23: TLabel
        Left = 64
        Top = 82
        Width = 31
        Height = 13
        Caption = 'Turno:'
      end
      object Label24: TLabel
        Left = 60
        Top = 106
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object PresupuestoLbl: TLabel
        Left = 33
        Top = 131
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Presupuesto:'
        WordWrap = True
      end
      object lblContratos: TLabel
        Left = 13
        Top = 222
        Width = 82
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de Contrato:'
      end
      object ReemplazaLbl: TLabel
        Left = 30
        Top = 57
        Width = 65
        Height = 13
        Caption = 'Reemplaza a:'
      end
      object RQ_MOTIVO: TZetaDBKeyCombo
        Left = 100
        Top = 28
        Width = 154
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
        OnChange = RQ_MOTIVOChange
        ListaFija = lfMotivoVacante
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        DataField = 'RQ_MOTIVO'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object RQ_TURNO: TDBEdit
        Left = 100
        Top = 78
        Width = 325
        Height = 21
        DataField = 'RQ_TURNO'
        DataSource = DataSource
        TabOrder = 2
      end
      object RQ_CLIENTE: TZetaDBKeyLookup
        Left = 100
        Top = 103
        Width = 350
        Height = 21
        LookupDataset = dmSeleccion.cdsCliente
        TabOrder = 3
        TabStop = True
        WidthLlave = 80
        OnValidLookup = RQ_CLIENTEValidLookup
        DataField = 'RQ_CLIENTE'
        DataSource = DataSource
      end
      object RQ_GTOPRES: TZetaDBNumero
        Left = 100
        Top = 128
        Width = 80
        Height = 21
        Mascara = mnPesos
        TabOrder = 4
        Text = '0.00'
        DataField = 'RQ_GTOPRES'
        DataSource = DataSource
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 457
        Height = 25
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = ' Datos Generales'
        Color = clBtnShadow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 7
      end
      object RQ_TIPO_CO: TZetaDBKeyLookup
        Left = 100
        Top = 219
        Width = 350
        Height = 21
        LookupDataset = dmSeleccion.cdsContratos
        TabOrder = 6
        TabStop = True
        WidthLlave = 80
        DataField = 'RQ_TIPO_CO'
        DataSource = DataSource
      end
      object RQ_REEMPLA: TDBEdit
        Left = 100
        Top = 53
        Width = 325
        Height = 21
        DataField = 'RQ_REEMPLA'
        DataSource = DataSource
        TabOrder = 1
      end
      object GroupBox1: TGroupBox
        Left = 16
        Top = 150
        Width = 417
        Height = 65
        Caption = 'Autoriz'#243
        TabOrder = 5
        object Label19: TLabel
          Left = 38
          Top = 16
          Width = 40
          Height = 13
          Caption = 'Nombre:'
        end
        object Label9: TLabel
          Left = 42
          Top = 40
          Width = 36
          Height = 13
          Caption = 'Puesto:'
        end
        object RQ_AUTORIZ: TDBEdit
          Left = 84
          Top = 12
          Width = 325
          Height = 21
          DataField = 'RQ_AUTORIZ'
          DataSource = DataSource
          TabOrder = 0
        end
        object RQ_PUES_AU: TDBEdit
          Left = 84
          Top = 36
          Width = 325
          Height = 21
          DataField = 'RQ_PUES_AU'
          DataSource = DataSource
          TabOrder = 1
        end
      end
    end
    object Perfil: TTabSheet
      Caption = 'Perfil'
      ImageIndex = 2
      TabVisible = False
      object Label29: TLabel
        Left = 68
        Top = 71
        Width = 27
        Height = 13
        Caption = 'Sexo:'
      end
      object Label7: TLabel
        Left = 9
        Top = 97
        Width = 86
        Height = 13
        Caption = 'Estudios M'#237'nimos:'
      end
      object LblIngles: TLabel
        Left = 23
        Top = 124
        Width = 72
        Height = 13
        Caption = 'Dominio Ingl'#233's:'
      end
      object Label12: TLabel
        Left = 142
        Top = 155
        Width = 8
        Height = 13
        Caption = '%'
      end
      object Label6: TLabel
        Left = 20
        Top = 149
        Width = 75
        Height = 13
        Caption = 'Area de Inter'#233's:'
      end
      object Label15: TLabel
        Left = 28
        Top = 173
        Width = 67
        Height = 13
        Caption = 'Otros criterios:'
      end
      object Label3: TLabel
        Left = 10
        Top = 198
        Width = 86
        Height = 13
        Caption = 'Otras Habilidades:'
      end
      object GBEdad: TGroupBox
        Left = 52
        Top = 27
        Width = 201
        Height = 38
        Caption = ' Edad '
        TabOrder = 0
        object Label8: TLabel
          Left = 16
          Top = 16
          Width = 25
          Height = 13
          Caption = 'Entre'
        end
        object Label11: TLabel
          Left = 166
          Top = 16
          Width = 23
          Height = 13
          Caption = 'a'#241'os'
        end
        object Label10: TLabel
          Left = 102
          Top = 16
          Width = 5
          Height = 13
          Caption = 'y'
        end
        object RQ_EDADMIN: TZetaDBNumero
          Left = 48
          Top = 12
          Width = 41
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
          DataField = 'RQ_EDADMIN'
          DataSource = DataSource
        end
        object RQ_EDADMAX: TZetaDBNumero
          Left = 117
          Top = 12
          Width = 41
          Height = 21
          Mascara = mnDias
          TabOrder = 1
          Text = '0'
          DataField = 'RQ_EDADMAX'
          DataSource = DataSource
        end
      end
      object RQ_SEXO: TZetaDBKeyCombo
        Left = 100
        Top = 69
        Width = 154
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        ListaFija = lfSexoDesc
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        DataField = 'RQ_SEXO'
        DataSource = DataSource
        LlaveNumerica = False
      end
      object RQ_EST_MIN: TZetaDBKeyCombo
        Left = 100
        Top = 94
        Width = 154
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        DropDownCount = 10
        ItemHeight = 13
        TabOrder = 2
        ListaFija = lfEstudios
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        DataField = 'RQ_ESTUDIO'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object RQ_INGLES: TZetaDBNumero
        Left = 100
        Top = 119
        Width = 41
        Height = 21
        Mascara = mnEmpleado
        TabOrder = 3
        DataField = 'RQ_INGLES'
        DataSource = DataSource
      end
      object RQ_AREA: TZetaDBKeyLookup
        Left = 100
        Top = 144
        Width = 350
        Height = 21
        TabOrder = 4
        TabStop = True
        WidthLlave = 80
        DataField = 'RQ_AREA'
        DataSource = DataSource
      end
      object QU_CODIGO: TZetaDBKeyLookup
        Left = 100
        Top = 169
        Width = 350
        Height = 21
        TabOrder = 5
        TabStop = True
        WidthLlave = 80
        DataField = 'QU_CODIGO'
        DataSource = DataSource
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 457
        Height = 25
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = ' Perfil'
        Color = clBtnShadow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 7
      end
      object RQ_HABILID: TDBMemo
        Left = 100
        Top = 194
        Width = 321
        Height = 49
        DataField = 'RQ_HABILID'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 6
      end
    end
    object Oferta: TTabSheet
      Caption = 'Oferta'
      ImageIndex = 3
      TabVisible = False
      object Panel4: TPanel
        Left = 0
        Top = 25
        Width = 457
        Height = 81
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label13: TLabel
          Left = 5
          Top = 10
          Width = 63
          Height = 13
          Alignment = taRightJustify
          Caption = 'Sueldo Base:'
        end
        object Label14: TLabel
          Left = 9
          Top = 34
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = 'Vacaciones:'
        end
        object Label30: TLabel
          Left = 18
          Top = 58
          Width = 50
          Height = 13
          Alignment = taRightJustify
          Caption = 'Aguinaldo:'
        end
        object Label31: TLabel
          Left = 227
          Top = 10
          Width = 39
          Height = 13
          Alignment = taRightJustify
          Caption = 'Bono(s):'
        end
        object Label17: TLabel
          Left = 237
          Top = 34
          Width = 29
          Height = 13
          Alignment = taRightJustify
          Caption = 'Vales:'
        end
        object RQ_SUELDO: TDBEdit
          Left = 70
          Top = 6
          Width = 140
          Height = 21
          DataField = 'RQ_SUELDO'
          DataSource = DataSource
          MaxLength = 30
          TabOrder = 0
        end
        object RQ_VACACI: TDBEdit
          Left = 70
          Top = 30
          Width = 140
          Height = 21
          DataField = 'RQ_VACACI'
          DataSource = DataSource
          MaxLength = 30
          TabOrder = 1
        end
        object RQ_BONO: TDBEdit
          Left = 270
          Top = 6
          Width = 140
          Height = 21
          DataField = 'RQ_BONO'
          DataSource = DataSource
          MaxLength = 30
          TabOrder = 3
        end
        object RQ_VALES: TDBEdit
          Left = 270
          Top = 30
          Width = 140
          Height = 21
          DataField = 'RQ_VALES'
          DataSource = DataSource
          MaxLength = 30
          TabOrder = 4
        end
        object RQ_AGUINAL: TDBEdit
          Left = 70
          Top = 54
          Width = 140
          Height = 21
          DataField = 'RQ_AGUINAL'
          DataSource = DataSource
          MaxLength = 30
          TabOrder = 2
        end
      end
      object RQ_OFERTA: TDBMemo
        Left = 0
        Top = 104
        Width = 459
        Height = 141
        DataField = 'RQ_OFERTA'
        DataSource = DataSource
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        MaxLength = 255
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 1
      end
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 457
        Height = 25
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = ' Oferta'
        Color = clBtnShadow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
    end
    object Observaciones: TTabSheet
      Caption = 'Observaciones'
      ImageIndex = 4
      TabVisible = False
      object RQ_OBSERVA: TDBMemo
        Left = 0
        Top = 25
        Width = 457
        Height = 218
        Align = alClient
        DataField = 'RQ_OBSERVA'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 0
      end
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 457
        Height = 25
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = ' Observaciones'
        Color = clBtnShadow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
    end
  end
  object DataSource: TDataSource
    Left = 401
    Top = 39
  end
end
