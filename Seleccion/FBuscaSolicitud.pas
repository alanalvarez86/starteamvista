unit FBuscaSolicitud;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, Mask, ZetaNumero, ZetaKeyLookup, StdCtrls, Buttons,
  ExtCtrls, Grids, DBGrids, ZetaDBGrid, Db, ZetaMessages,ZetaCommonClasses;

type
  TBuscaSolicitud = class(TZetaDlgModal)
    ZetaDBGrid: TZetaDBGrid;
    DataSource: TDataSource;
    PanelFiltro: TPanel;
    SolStatusLBL: TLabel;
    AreaLBL: TLabel;
    EstudiosLBL: TLabel;
    SexoLBL: TLabel;
    EdadMinimaLBL: TLabel;
    EdadMaximaLBL: TLabel;
    InglesLBL: TLabel;
    InglesPctLBL: TLabel;
    ApePatLBL: TLabel;
    NombreLBL: TLabel;
    Label1: TLabel;
    ApeMatLbl: TLabel;
    CriterioLbl: TLabel;
    SolStatus: TComboBox;
    MinEstudios: TComboBox;
    Sexo: TComboBox;
    ApePat: TEdit;
    Nombre: TEdit;
    Refrescar: TBitBtn;
    Area: TZetaKeyLookup;
    EdadMinima: TZetaNumero;
    EdadMaxima: TZetaNumero;
    Ingles: TZetaNumero;
    ApeMat: TEdit;
    Condicion: TZetaKeyLookup;
    BotonReset: TSpeedButton;
    BtnVerSolicitud: TBitBtn;
    OtrasHabLBL: TLabel;{OP:14.Abr.08}
    OtrasHab: TEdit;{OP:14.Abr.08}
    procedure RefrescarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure BotonResetClick(Sender: TObject);
    procedure DataSourceStateChange(Sender: TObject);
    procedure BtnVerSolicitudClick(Sender: TObject);
    procedure EdadExit(Sender: TObject);
  private
    { Private declarations }
    FParametros : TZetaParams;
    FAgregando : Boolean;
    function GetCondicion: String;
    procedure SetControles;
    procedure Connect;
    procedure InicializaCampos;
    procedure SetValoresDefault;
    procedure SetEnabledBtn;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
  public
    { Public declarations }
    procedure Refresh;
  end;

var
  BuscaSolicitud: TBuscaSolicitud;

function BuscaSolicitudDialogo(const sFilter: String; var sKey, sDescription: String) : Boolean;
procedure AgregaCandidatosDialogo;

implementation

uses DSeleccion, FTressShell, ZetaTipoEntidad,
     ZetaCommonLists, ZetaCommonTools, ZetaDialogo,
     ZAccesosMgr, ZAccesosTress;

{$R *.DFM}

function BuscaSolicitudDialogo(const sFilter: String; var sKey, sDescription: String) : Boolean;
begin
     if BuscaSolicitud = NIL then
        BuscaSolicitud := TBuscaSolicitud.Create(Application);

     with BuscaSolicitud do
     begin
          FAgregando := FALSE;
          SetControles;
          ShowModal;
          Result := ( ModalResult = mrOk );
     end;
     if Result then
        with dmSeleccion.cdsLookupSolicita do
        begin
             sKey := FieldByName('SO_FOLIO').AsString;
             sDescription := FieldByName('PRETTYNAME').AsString;
        end;
end;

procedure AgregaCandidatosDialogo;
begin
     if BuscaSolicitud = NIL then
        BuscaSolicitud := TBuscaSolicitud.Create(Application);

     with BuscaSolicitud do
     begin
          FAgregando := TRUE;
          SetControles;
          ShowModal;
     end;
end;

procedure TBuscaSolicitud.FormCreate(Sender: TObject);

 procedure FillCombo( Control: TComboBox; Tipo: ListasFijas; const sTodos: String );
 begin
      with Control do
      begin
           ZetaCommonLists.LlenaLista( Tipo, Items );
           Items.Insert( 0, Format( '< %s >', [ sTodos ] ) );
           ItemIndex := 0;
      end;
 end;

begin
     inherited;
     HelpContext := H00073_Solicitudes;
     FParametros := TZetaParams.Create;
     FillCombo( SolStatus, lfSolStatus, 'Todos' );
     SolStatus.Items.Insert( 1, Format( '%s', [ 'Disponible/En Tramite' ] ) );
     FillCombo( MinEstudios, lfEstudios, 'Cualquiera' );
     FillCombo( Sexo, lfSexoDesc, 'Todos' );
     EdadMinima.Valor := 0;
     EdadMaxima.Valor := 0;
     Ingles.Valor := 0;
     SolStatus.ItemIndex := 1;
     Area.LookupDataset := dmSeleccion.cdsAreaInteres;
     Condicion.LookupDataset := dmSeleccion.cdsCondiciones;
end;

procedure TBuscaSolicitud.FormDestroy(Sender: TObject);
begin
     inherited;
     FParametros.Free;
end;

procedure TBuscaSolicitud.FormShow(Sender: TObject);
begin
     inherited;
     Connect;
end;

procedure TBuscaSolicitud.Connect;
begin
     with dmSeleccion do
     begin
          cdsLookupSolicita.Close;
          DataSource.DataSet := cdsLookupSolicita;
     end;
end;

procedure TBuscaSolicitud.Refresh;
begin
     with FParametros do
     begin
          AddInteger( 'Status', SolStatus.ItemIndex - 2 );
          AddInteger( 'EdadMinima', EdadMinima.ValorEntero );
          AddInteger( 'EdadMaxima', EdadMaxima.ValorEntero );
          AddInteger( 'Sexo', Sexo.ItemIndex - 1 );
          AddInteger( 'MinEstudios', MinEstudios.ItemIndex - 1 );
          AddInteger( 'Ingles', Ingles.ValorEntero );
          AddString( 'ApePat', ApePat.Text );
          AddString( 'ApeMat', ApeMat.Text );
          AddString( 'Nombres', Nombre.Text );
          AddString( 'Area', Area.Llave );
          AddString( 'Condicion', GetCondicion );
          AddString( 'OtrasHab', OtrasHab.Text );{OP:14.Abr.08}          
     end;
     dmSeleccion.RefrescarSolicita( FParametros, TRUE );
end;

function TBuscaSolicitud.GetCondicion: String;
begin
     if StrLleno( Condicion.Llave ) then
        Result:= Trim( Condicion.LookUpDataSet.FieldByName( 'QU_FILTRO' ).AsString )
     else
        Result:= '';
end;

procedure TBuscaSolicitud.SetControles;
begin
     if FAgregando then
     begin
          with OK do
          begin
               Left := 400;
               Width := 125;
               ModalResult := mrNone;
               Caption := '&Agregar Solicitud';
               Hint := 'Agregar Candidatura a Requisici�n';
          end;
          with Cancelar do
          begin
               Kind := bkClose;
               ModalResult := mrAbort;
               Cancel := True;
               Caption := '&Salir';
               Hint := 'Cerrar Pantalla y Salir';
          end;
          SetValoresDefault;
     end
     else
     begin
          with OK do
          begin
               Left := 450;
               Width := 75;
               ModalResult := mrOK;
               Caption := '&OK';
               Hint := 'Aceptar';
          end;
          with Cancelar do
          begin
               Kind := bkCancel;
               ModalResult := mrCancel;
               Cancel := False;
               Caption := '&Cancelar';
               Hint := 'Cancelar Cambios';
          end;
     end;
     SetEnabledBtn;
end;

procedure TBuscaSolicitud.InicializaCampos;
begin
     SolStatus.ItemIndex := 1;
     Sexo.ItemIndex := 0;
     EdadMinima.Valor := 0;
     EdadMaxima.Valor := 0;
     MinEstudios.ItemIndex := 0;
     Ingles.Valor := 0;
     Area.Llave := VACIO;
     Condicion.Llave := VACIO;
     ApePat.Text := VACIO;
     ApeMat.Text := VACIO;
     Nombre.Text := VACIO;
end;

procedure TBuscaSolicitud.SetValoresDefault;
begin
     with dmSeleccion.cdsEditRequiere do
     begin
          if StrVacio( FieldByName('RQ_SEXO').AsString ) then
             Sexo.ItemIndex := 0
          else if ( FieldByName('RQ_SEXO').AsString = 'M' ) then
             Sexo.ItemIndex := 1
          else
             Sexo.ItemIndex := 2;
          EdadMinima.Valor := FieldByName( 'RQ_EDADMIN' ).AsInteger;
          EdadMaxima.Valor := FieldByName( 'RQ_EDADMAX' ).AsInteger;
          MinEstudios.ItemIndex := FieldByName( 'RQ_ESTUDIO' ).AsInteger + 1;    // Aqu� se agrega la opci�n Cualquiera
          Ingles.Valor := FieldByName( 'RQ_INGLES' ).AsInteger;
          Area.Llave := FieldByName( 'RQ_AREA' ).AsString;
          Condicion.Llave := FieldByName( 'QU_CODIGO' ).AsString;
     end;
end;

procedure TBuscaSolicitud.SetEnabledBtn;
begin
     OK.Enabled := ( DataSource.State <> dsInactive ) and ( DataSource.DataSet.RecordCount > 0 );
     BtnVerSolicitud.Enabled := OK.Enabled;
end;

procedure TBuscaSolicitud.RefrescarClick(Sender: TObject);
begin
     inherited;
     Refresh;
     ZetaDBGrid.SetFocus;
end;

procedure TBuscaSolicitud.WMExaminar(var Message: TMessage);
begin
     if OK.Enabled then
        OK.Click;
end;

procedure TBuscaSolicitud.OKClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     if FAgregando then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             with dmSeleccion do
             begin
                  AgregaUnCandidato;
                  with cdsReqCandidatos do
                  begin
                       Enviar;
                       if ( ChangeCount > 0 ) then
                          ZetaDialogo.ZError( self.Caption, 'No se Pudo Agregar Candidato', 0 )
                       else
                          cdsLookupSolicita.Delete;
                  end;
             end;
          finally
             Screen.Cursor := oCursor;
          end;
          SetEnabledBtn;
     end;
end;

procedure TBuscaSolicitud.BotonResetClick(Sender: TObject);
begin
     inherited;
     InicializaCampos;
end;

procedure TBuscaSolicitud.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     SetEnabledBtn;
end;

procedure TBuscaSolicitud.BtnVerSolicitudClick(Sender: TObject);
begin
     inherited;
     if ( ZAccesosMgr.Revisa( D_SOLICITUD_DATOS ) ) then
     begin
          with dmSeleccion do
               ModificaSolicitud( DataSource.DataSet.FieldByName( 'SO_FOLIO' ).AsInteger, FALSE );
     end
     else
        ZetaDialogo.zInformation( Caption, 'No Tiene Permiso Para Ver Solicitudes', 0 );
end;

procedure TBuscaSolicitud.EdadExit(Sender: TObject);
begin
     inherited;
     with Sender As TZetaNumero do
     begin
          if ( ValorEntero < 0 ) or ( ValorEntero > 99 ) then
          begin
               ZetaDialogo.zInformation( Caption, 'La edad est� fuera del rango permitido (0-99)', 0 );
               Valor := 0;
               SetFocus;
          end;
     end;
end;

end.

