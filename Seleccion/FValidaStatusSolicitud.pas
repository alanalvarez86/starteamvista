unit FValidaStatusSolicitud;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ZetaDBTextBox, ZetaCommonLists, Mask, ZetaNumero,
  Grids, DBGrids, ZetaDBGrid, Db;

type

  TValidaStatusSolicitud = class(TForm)
    Mensaje: TLabel;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    SolicitudLbl: TLabel;
    PrettyNameLbl: TLabel;
    Label6: TLabel;
    StatusLbl: TLabel;
    PanelBotones: TPanel;
    IgnorarBtn: TBitBtn;
    CancelarBtn: TBitBtn;
    Label5: TLabel;
    GridCandidaturas: TZetaDBGrid;
    DataSource: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  function ShowDlgStatusCandidaturas( const sFolio, PrettyName, sStatus: String;
           oDataSet: TDataSet ): eOperacionConflicto;

var
  ValidaStatusSolicitud: TValidaStatusSolicitud;

implementation

{$R *.DFM}

function ShowDlgStatusCandidaturas( const sFolio, PrettyName, sStatus: String;
           oDataSet: TDataSet ): eOperacionConflicto;
begin

     if ( ValidaStatusSolicitud = nil ) then
        ValidaStatusSolicitud := TValidaStatusSolicitud.Create( Application.MainForm ); // Se destruye al Salir del Programa //

     try
        with ValidaStatusSolicitud do
        begin
             SolicitudLbl.Caption:= sFolio;
             PrettyNameLbl.Caption:= PrettyName;
             StatusLbl.Caption:= sStatus;
             DataSource.DataSet := oDataSet;
             ShowModal;
             if ( ModalResult = mrYes ) then
                Result:= ocIgnorar
             else
                Result:= ocReportar;
        end;
     finally
        ValidaStatusSolicitud.DataSource.DataSet := nil;
     end;

end;

procedure TValidaStatusSolicitud.FormCreate(Sender: TObject);
begin
     HelpContext:= 0;
end;

procedure TValidaStatusSolicitud.FormShow(Sender: TObject);
begin
     CancelarBtn.SetFocus;
end;

end.
