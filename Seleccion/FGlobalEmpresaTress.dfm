inherited GlobalEmpresaTress: TGlobalEmpresaTress
  Left = 293
  Top = 294
  Caption = 'Empresa de Tress'
  ClientHeight = 127
  ClientWidth = 408
  PixelsPerInch = 96
  TextHeight = 13
  object lblEmpresa: TLabel [0]
    Left = 8
    Top = 44
    Width = 88
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa de &Tress:'
    FocusControl = ZEmpresaTress
  end
  inherited PanelBotones: TPanel
    Top = 91
    Width = 408
    inherited OK: TBitBtn
      Left = 240
    end
    inherited Cancelar: TBitBtn
      Left = 325
    end
  end
  object ZEmpresaTress: TZetaDBKeyLookup
    Left = 99
    Top = 40
    Width = 300
    Height = 21
    TabOrder = 1
    TabStop = True
    WidthLlave = 60
  end
end
