inherited AltaSolicitud: TAltaSolicitud
  Left = 310
  Top = 208
  Width = 512
  Height = 359
  Caption = 'Registro de Solicitud'
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label14: TLabel [0]
    Left = 153
    Top = 154
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = '&Estado:'
  end
  inherited Wizard: TZetaWizard
    Top = 289
    Width = 504
    AlEjecutar = WizardAlEjecutar
    AlCancelar = WizardAlCancelar
    AfterMove = WizardAfterMove
    BeforeMove = WizardBeforeMove
    DesignSize = (
      504
      36)
    inherited Anterior: TZetaWizardButton
      Enabled = True
    end
    inherited Salir: TZetaWizardButton
      Left = 419
      Kind = bkCancel
    end
    inherited Ejecutar: TZetaWizardButton
      Left = 336
      Caption = '&Terminar'
    end
  end
  inherited PageControl: TPageControl
    Width = 504
    Height = 289
    ActivePage = Foto
    object Identificacion: TTabSheet
      Caption = 'Identificacion'
      TabVisible = False
      object LblApePat: TLabel
        Left = 101
        Top = 47
        Width = 80
        Height = 13
        Caption = 'Apellido Paterno:'
      end
      object LblApeMat: TLabel
        Left = 99
        Top = 71
        Width = 82
        Height = 13
        Caption = 'Apellido Materno:'
      end
      object LblNombres: TLabel
        Left = 136
        Top = 95
        Width = 45
        Height = 13
        Caption = 'Nombres:'
      end
      object LblFecNac: TLabel
        Left = 92
        Top = 119
        Width = 89
        Height = 13
        Caption = 'Fecha Nacimiento:'
      end
      object LblSexo: TLabel
        Left = 154
        Top = 168
        Width = 27
        Height = 13
        Caption = 'Sexo:'
      end
      object ZEdad: TZetaTextBox
        Left = 313
        Top = 117
        Width = 92
        Height = 17
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object LblEdoCivil: TLabel
        Left = 123
        Top = 192
        Width = 58
        Height = 13
        Caption = 'Estado Civil:'
      end
      object Label6: TLabel
        Left = 95
        Top = 144
        Width = 86
        Height = 13
        Alignment = taRightJustify
        Caption = 'Lugar Nacimiento:'
      end
      object Label7: TLabel
        Left = 136
        Top = 240
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'C.U.R.P.:'
      end
      object Label8: TLabel
        Left = 151
        Top = 216
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'R.F.C:'
      end
      object SO_APE_PAT: TDBEdit
        Left = 185
        Top = 43
        Width = 220
        Height = 21
        DataField = 'SO_APE_PAT'
        DataSource = DataSource
        TabOrder = 1
      end
      object SO_APE_MAT: TDBEdit
        Left = 185
        Top = 67
        Width = 220
        Height = 21
        DataField = 'SO_APE_MAT'
        DataSource = DataSource
        TabOrder = 2
      end
      object SO_NOMBRES: TDBEdit
        Left = 185
        Top = 91
        Width = 220
        Height = 21
        DataField = 'SO_NOMBRES'
        DataSource = DataSource
        TabOrder = 3
      end
      object SO_FEC_NAC: TZetaDBFecha
        Left = 185
        Top = 115
        Width = 125
        Height = 22
        Cursor = crArrow
        Opcional = True
        TabOrder = 4
        Text = '18/Dec/97'
        Valor = 35782.000000000000000000
        DataField = 'SO_FEC_NAC'
        DataSource = DataSource
      end
      object SO_SEXO: TZetaDBKeyCombo
        Left = 185
        Top = 164
        Width = 125
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 6
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        DataField = 'SO_SEXO'
        DataSource = DataSource
        LlaveNumerica = False
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 496
        Height = 25
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = ' Datos Personales'
        Color = clBtnShadow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object SO_EDO_CIV: TZetaDBKeyCombo
        Left = 185
        Top = 188
        Width = 125
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 7
        ListaFija = lfEdoCivil
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        DataField = 'SO_EDO_CIV'
        DataSource = DataSource
        LlaveNumerica = False
      end
      object SO_PAIS: TDBEdit
        Left = 185
        Top = 140
        Width = 220
        Height = 21
        DataField = 'SO_PAIS'
        DataSource = DataSource
        TabOrder = 5
      end
      object SO_CURP: TDBEdit
        Left = 185
        Top = 236
        Width = 190
        Height = 21
        CharCase = ecUpperCase
        DataField = 'SO_CURP'
        DataSource = DataSource
        TabOrder = 9
      end
      object SO_RFC: TDBEdit
        Left = 185
        Top = 212
        Width = 121
        Height = 21
        CharCase = ecUpperCase
        DataField = 'SO_RFC'
        DataSource = DataSource
        TabOrder = 8
      end
    end
    object Direccion: TTabSheet
      Caption = 'Direccion'
      ImageIndex = 9
      TabVisible = False
      object Label9: TLabel
        Left = 96
        Top = 30
        Width = 26
        Height = 13
        Alignment = taRightJustify
        Caption = 'Calle:'
      end
      object Label10: TLabel
        Left = 84
        Top = 82
        Width = 38
        Height = 13
        Alignment = taRightJustify
        Caption = 'Colonia:'
      end
      object Label11: TLabel
        Left = 54
        Top = 126
        Width = 68
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo Postal:'
      end
      object Label12: TLabel
        Left = 86
        Top = 104
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ciudad:'
      end
      object lblEstado: TLabel
        Left = 86
        Top = 148
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Estado:'
      end
      object Label15: TLabel
        Left = 97
        Top = 170
        Width = 25
        Height = 13
        Alignment = taRightJustify
        Caption = 'Pa'#237's:'
      end
      object Label16: TLabel
        Left = 13
        Top = 192
        Width = 109
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tiempo de Residencia:'
      end
      object Label17: TLabel
        Left = 77
        Top = 215
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tel'#233'fono:'
      end
      object Label18: TLabel
        Left = 32
        Top = 237
        Width = 90
        Height = 13
        Alignment = taRightJustify
        Caption = 'Correo Electr'#243'nico:'
      end
      object Label21: TLabel
        Left = 175
        Top = 193
        Width = 24
        Height = 13
        Caption = 'A'#241'os'
      end
      object Label22: TLabel
        Left = 315
        Top = 186
        Width = 31
        Height = 13
        Caption = 'Meses'
      end
      object lblTransporte: TLabel
        Left = 68
        Top = 259
        Width = 54
        Height = 13
        Alignment = taRightJustify
        Caption = 'Transporte:'
      end
      object Label13: TLabel
        Left = 76
        Top = 58
        Width = 48
        Height = 13
        Caption = '# Exterior:'
      end
      object Label19: TLabel
        Left = 210
        Top = 58
        Width = 45
        Height = 13
        Caption = '# Interior:'
      end
      object Panel10: TPanel
        Left = 0
        Top = 0
        Width = 496
        Height = 25
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = 'Domicilio'
        Color = clBtnShadow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object SO_CALLE: TDBEdit
        Left = 124
        Top = 30
        Width = 320
        Height = 21
        DataField = 'SO_CALLE'
        DataSource = DataSource
        TabOrder = 1
      end
      object SO_COLONIA: TDBEdit
        Left = 126
        Top = 78
        Width = 220
        Height = 21
        DataField = 'SO_COLONIA'
        DataSource = DataSource
        TabOrder = 4
      end
      object SO_CODPOST: TDBEdit
        Left = 126
        Top = 122
        Width = 80
        Height = 21
        DataField = 'SO_CODPOST'
        DataSource = DataSource
        MaxLength = 8
        TabOrder = 6
      end
      object SO_CIUDAD: TDBEdit
        Left = 126
        Top = 100
        Width = 220
        Height = 21
        DataField = 'SO_CIUDAD'
        DataSource = DataSource
        TabOrder = 5
      end
      object SO_NACION: TDBEdit
        Left = 126
        Top = 166
        Width = 220
        Height = 21
        DataField = 'SO_NACION'
        DataSource = DataSource
        TabOrder = 8
      end
      object SO_TEL: TDBEdit
        Left = 126
        Top = 211
        Width = 220
        Height = 21
        DataField = 'SO_TEL'
        DataSource = DataSource
        TabOrder = 11
      end
      object SO_EMAIL: TDBEdit
        Left = 126
        Top = 233
        Width = 220
        Height = 21
        DataField = 'SO_EMAIL'
        DataSource = DataSource
        TabOrder = 12
      end
      object SO_ESTADO: TZetaDBKeyLookup
        Left = 126
        Top = 144
        Width = 320
        Height = 21
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
        DataField = 'SO_ESTADO'
        DataSource = DataSource
      end
      object ZYear: TZetaNumero
        Left = 126
        Top = 189
        Width = 45
        Height = 21
        Mascara = mnDias
        TabOrder = 9
        Text = '0'
        OnExit = ZYearExit
      end
      object ZMonth: TZetaNumero
        Left = 207
        Top = 189
        Width = 45
        Height = 21
        Mascara = mnDiasFraccion
        TabOrder = 10
        Text = '0.00'
        OnExit = ZMonthExit
      end
      object SO_MED_TRA: TZetaDBKeyLookup
        Left = 126
        Top = 255
        Width = 320
        Height = 21
        TabOrder = 13
        TabStop = True
        WidthLlave = 60
        DataField = 'SO_MED_TRA'
        DataSource = DataSource
      end
      object SO_NUM_EXT: TDBEdit
        Left = 126
        Top = 54
        Width = 80
        Height = 21
        DataField = 'SO_NUM_EXT'
        DataSource = DataSource
        TabOrder = 2
      end
      object SO_NUM_INT: TDBEdit
        Left = 258
        Top = 54
        Width = 80
        Height = 21
        DataField = 'SO_NUM_INT'
        DataSource = DataSource
        TabOrder = 3
      end
    end
    object Identificacion2: TTabSheet
      Caption = 'Identificacion2'
      ImageIndex = 8
      TabVisible = False
      object LblIngles: TLabel
        Left = 109
        Top = 126
        Width = 72
        Height = 13
        Alignment = taRightJustify
        Caption = 'Dominio Ingl'#233's:'
      end
      object Label2: TLabel
        Left = 226
        Top = 174
        Width = 8
        Height = 13
        Caption = '%'
      end
      object LblIdioma: TLabel
        Left = 124
        Top = 150
        Width = 57
        Height = 13
        Alignment = taRightJustify
        Caption = 'Otro Idioma:'
      end
      object LblDominio: TLabel
        Left = 140
        Top = 174
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = 'Dominio:'
      end
      object Label4: TLabel
        Left = 226
        Top = 126
        Width = 8
        Height = 13
        Caption = '%'
      end
      object LblEscuela: TLabel
        Left = 123
        Top = 78
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Escolaridad:'
      end
      object Label20: TLabel
        Left = 13
        Top = 102
        Width = 168
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nombre de la Instituci'#243'n Educativa:'
      end
      object SO_INGLES: TZetaDBNumero
        Left = 185
        Top = 122
        Width = 37
        Height = 21
        Mascara = mnEmpleado
        TabOrder = 3
        DataField = 'SO_INGLES'
        DataSource = DataSource
      end
      object SO_IDIOMA2: TDBEdit
        Left = 185
        Top = 146
        Width = 125
        Height = 21
        DataField = 'SO_IDIOMA2'
        DataSource = DataSource
        TabOrder = 4
      end
      object SO_DOMINA2: TZetaDBNumero
        Left = 185
        Top = 170
        Width = 37
        Height = 21
        Mascara = mnEmpleado
        TabOrder = 5
        DataField = 'SO_DOMINA2'
        DataSource = DataSource
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 496
        Height = 25
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = ' Escolaridad'
        Color = clBtnShadow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object SO_ESTUDIO: TZetaDBKeyCombo
        Left = 185
        Top = 74
        Width = 125
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        DropDownCount = 10
        ItemHeight = 13
        TabOrder = 1
        ListaFija = lfEstudios
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        DataField = 'SO_ESTUDIO'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object SO_ESCUELA: TDBEdit
        Left = 185
        Top = 98
        Width = 220
        Height = 21
        DataField = 'SO_ESCUELA'
        DataSource = DataSource
        TabOrder = 2
      end
    end
    object Foto: TTabSheet
      Caption = 'Foto'
      TabVisible = False
      object BarraFotografia: TToolBar
        Left = 196
        Top = 195
        Width = 105
        Height = 29
        Align = alNone
        Caption = 'BarraFotografia'
        Flat = True
        Images = ImageListPrende
        Indent = 6
        TabOrder = 0
        object tbLeerFoto: TToolButton
          Left = 6
          Top = 0
          Hint = 'Leer Fotograf'#237'a de Disco'
          Caption = 'tbLeerFoto'
          ImageIndex = 0
          ParentShowHint = False
          ShowHint = True
          OnClick = tbLeerFotoClick
        end
        object tbGrabarFoto: TToolButton
          Left = 29
          Top = 0
          Hint = 'Grabar Fotograf'#237'a a Disco'
          Caption = 'tbGrabarFoto'
          ImageIndex = 1
          ParentShowHint = False
          ShowHint = True
          OnClick = tbGrabarFotoClick
        end
        object tbLeerTwain: TToolButton
          Left = 52
          Top = 0
          Hint = 'Leer Dispositivo Twain'
          Caption = 'tbLeerTwain'
          ImageIndex = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = tbLeerTwainClick
        end
        object tbSelectTwain: TToolButton
          Left = 75
          Top = 0
          Hint = 'Seleccionar Dispositivo Twain'
          Caption = 'tbSelectTwain'
          ImageIndex = 3
          ParentShowHint = False
          ShowHint = True
          OnClick = tbSelectTwainClick
        end
      end
      object FOTOGRAFIA: TPDBMultiImage
        Left = 151
        Top = 41
        Width = 195
        Height = 147
        Hint = 'Foto del Empleado'
        ImageReadRes = lAutoMatic
        ImageWriteRes = sAutoMatic
        DataEngine = deBDE
        JPegSaveQuality = 75
        JPegSaveSmooth = 0
        PNGInterLaced = True
        ImageDither = True
        UpdateAsJPG = False
        UpdateAsBMP = False
        UpdateAsGIF = False
        UpdateAsPCX = False
        UpdateAsPNG = False
        UpdateAsTIF = True
        Ctl3D = True
        DataField = 'SO_FOTO'
        DataSource = DataSource
        ParentCtl3D = False
        RichTools = False
        ReadOnly = True
        StretchRatio = True
        TabOrder = 1
        TextLeft = 0
        TextTop = 0
        TextRotate = 0
        TextTransParent = False
        TifSaveCompress = sNONE
        UseTwainWindow = False
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 496
        Height = 25
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = ' Fotograf'#237'a'
        Color = clBtnShadow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
    end
    object Solicitud: TTabSheet
      Caption = 'Solicitud'
      ImageIndex = 1
      TabVisible = False
      object LblFecSolicitud: TLabel
        Left = 35
        Top = 47
        Width = 91
        Height = 13
        Caption = 'Fecha de Solicitud:'
      end
      object LblPuestoSolic: TLabel
        Left = 41
        Top = 73
        Width = 85
        Height = 13
        Caption = 'Puesto Solicitado:'
      end
      object SO_FEC_INI: TZetaDBFecha
        Left = 131
        Top = 43
        Width = 125
        Height = 22
        Cursor = crArrow
        Opcional = True
        TabOrder = 0
        Text = '18/Dec/97'
        Valor = 35782.000000000000000000
        DataField = 'SO_FEC_INI'
        DataSource = DataSource
      end
      object SO_PUE_SOL: TDBEdit
        Left = 131
        Top = 69
        Width = 334
        Height = 21
        DataField = 'SO_PUE_SOL'
        DataSource = DataSource
        TabOrder = 1
      end
      object GBAreas: TGroupBox
        Left = 30
        Top = 102
        Width = 438
        Height = 171
        Caption = ' Areas de Inter'#233's ( M'#225'ximo Tres ) '
        TabOrder = 2
        object ListaInteres: TCheckListBox
          Left = 2
          Top = 15
          Width = 434
          Height = 154
          OnClickCheck = ListaInteresClickCheck
          Align = alClient
          Columns = 2
          ItemHeight = 13
          Sorted = True
          TabOrder = 0
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 496
        Height = 25
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = ' Solicitud'
        Color = clBtnShadow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
      end
    end
    object Adicionales: TTabSheet
      Caption = 'Adicionales'
      ImageIndex = 2
      TabVisible = False
      object AdicionalScrollBox: TScrollBox
        Left = 0
        Top = 25
        Width = 496
        Height = 254
        HorzScrollBar.Visible = False
        Align = alClient
        BorderStyle = bsNone
        TabOrder = 0
      end
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 496
        Height = 25
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = ' Datos Adicionales'
        Color = clBtnShadow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
    end
    object Requisitos: TTabSheet
      Caption = 'Requisitos'
      ImageIndex = 3
      TabVisible = False
      object SO_REQUI_1: TDBCheckBox
        Left = 107
        Top = 43
        Width = 350
        Height = 17
        DataField = 'SO_REQUI_1'
        DataSource = DataSource
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object SO_REQUI_2: TDBCheckBox
        Left = 107
        Top = 66
        Width = 350
        Height = 17
        DataField = 'SO_REQUI_2'
        DataSource = DataSource
        TabOrder = 1
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object SO_REQUI_3: TDBCheckBox
        Left = 107
        Top = 89
        Width = 350
        Height = 17
        DataField = 'SO_REQUI_3'
        DataSource = DataSource
        TabOrder = 2
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object SO_REQUI_4: TDBCheckBox
        Left = 107
        Top = 113
        Width = 350
        Height = 17
        DataField = 'SO_REQUI_4'
        DataSource = DataSource
        TabOrder = 3
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object SO_REQUI_5: TDBCheckBox
        Left = 107
        Top = 137
        Width = 350
        Height = 17
        DataField = 'SO_REQUI_5'
        DataSource = DataSource
        TabOrder = 4
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object SO_REQUI_6: TDBCheckBox
        Left = 107
        Top = 160
        Width = 350
        Height = 17
        DataField = 'SO_REQUI_6'
        DataSource = DataSource
        TabOrder = 5
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object SO_REQUI_7: TDBCheckBox
        Left = 107
        Top = 183
        Width = 350
        Height = 17
        DataField = 'SO_REQUI_7'
        DataSource = DataSource
        TabOrder = 6
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object SO_REQUI_8: TDBCheckBox
        Left = 107
        Top = 207
        Width = 350
        Height = 17
        DataField = 'SO_REQUI_8'
        DataSource = DataSource
        TabOrder = 7
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object SO_REQUI_9: TDBCheckBox
        Left = 107
        Top = 230
        Width = 350
        Height = 17
        DataField = 'SO_REQUI_9'
        DataSource = DataSource
        TabOrder = 8
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 496
        Height = 25
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = ' Requisitos'
        Color = clBtnShadow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 9
      end
      object DocEntregadaGb: TGroupBox
        Left = 56
        Top = 32
        Width = 401
        Height = 41
        Caption = 'Documentaci'#243'n entregada'
        TabOrder = 10
      end
      object OtrosRequisitosGb: TGroupBox
        Left = 56
        Top = 76
        Width = 401
        Height = 41
        Caption = 'Otros requisitos'
        TabOrder = 11
      end
    end
    object Referencias1: TTabSheet
      Caption = 'Referencias1'
      ImageIndex = 4
      TabVisible = False
      object PanelReferencia: TPanel
        Left = 0
        Top = 273
        Width = 496
        Height = 0
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
      end
      object PanelNumRef: TPanel
        Left = 0
        Top = 0
        Width = 496
        Height = 25
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = ' Empleos Anteriores # 1 de %s'
        Color = clBtnShadow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
      object Panel7: TPanel
        Left = 0
        Top = 25
        Width = 496
        Height = 8
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
      end
      object RL_CAMPO1Pnl: TPanel
        Left = 0
        Top = 33
        Width = 496
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 3
        object RL_CAMPO1Lbl: TLabel
          Left = 117
          Top = 5
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Campo 1:'
          FocusControl = RL_CAMPO1
        end
        object RL_CAMPO1: TDBEdit
          Left = 166
          Top = 2
          Width = 300
          Height = 21
          DataField = 'RL_CAMPO1'
          DataSource = dsRefLaboral
          TabOrder = 0
        end
      end
      object RL_CAMPO2Pnl: TPanel
        Left = 0
        Top = 57
        Width = 496
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 4
        object RL_CAMPO2Lbl: TLabel
          Left = 117
          Top = 5
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Campo 2:'
          FocusControl = RL_CAMPO2
        end
        object RL_CAMPO2: TDBEdit
          Left = 166
          Top = 2
          Width = 300
          Height = 21
          DataField = 'RL_CAMPO2'
          DataSource = dsRefLaboral
          TabOrder = 0
        end
      end
      object RL_CAMPO3Pnl: TPanel
        Left = 0
        Top = 81
        Width = 496
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 5
        object RL_CAMPO3Lbl: TLabel
          Left = 117
          Top = 5
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Campo 3:'
          FocusControl = RL_CAMPO3
        end
        object RL_CAMPO3: TDBEdit
          Left = 166
          Top = 2
          Width = 300
          Height = 21
          DataField = 'RL_CAMPO3'
          DataSource = dsRefLaboral
          TabOrder = 0
        end
      end
      object RL_CAMPO4Pnl: TPanel
        Left = 0
        Top = 105
        Width = 496
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 6
        object RL_CAMPO4Lbl: TLabel
          Left = 117
          Top = 5
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Campo 4:'
          FocusControl = RL_CAMPO4
        end
        object RL_CAMPO4: TDBEdit
          Left = 166
          Top = 2
          Width = 300
          Height = 21
          DataField = 'RL_CAMPO4'
          DataSource = dsRefLaboral
          TabOrder = 0
        end
      end
      object RL_CAMPO5Pnl: TPanel
        Left = 0
        Top = 129
        Width = 496
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 7
        object RL_CAMPO5Lbl: TLabel
          Left = 117
          Top = 5
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Campo 5:'
          FocusControl = RL_CAMPO5
        end
        object RL_CAMPO5: TDBEdit
          Left = 166
          Top = 2
          Width = 300
          Height = 21
          DataField = 'RL_CAMPO5'
          DataSource = dsRefLaboral
          TabOrder = 0
        end
      end
      object RL_CAMPO6Pnl: TPanel
        Left = 0
        Top = 153
        Width = 496
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 8
        object RL_CAMPO6Lbl: TLabel
          Left = 117
          Top = 6
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Campo 6:'
          FocusControl = RL_CAMPO6
        end
        object RL_CAMPO6: TDBEdit
          Left = 166
          Top = 2
          Width = 300
          Height = 21
          DataField = 'RL_CAMPO6'
          DataSource = dsRefLaboral
          TabOrder = 0
        end
      end
      object RL_CAMPO7Pnl: TPanel
        Left = 0
        Top = 177
        Width = 496
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 9
        object RL_CAMPO7Lbl: TLabel
          Left = 117
          Top = 5
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Campo 7:'
          FocusControl = RL_CAMPO7
        end
        object RL_CAMPO7: TDBEdit
          Left = 166
          Top = 2
          Width = 300
          Height = 21
          DataField = 'RL_CAMPO7'
          DataSource = dsRefLaboral
          TabOrder = 0
        end
      end
      object RL_CAMPO8Pnl: TPanel
        Left = 0
        Top = 201
        Width = 496
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 10
        object RL_CAMPO8Lbl: TLabel
          Left = 117
          Top = 5
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Campo 8:'
          FocusControl = RL_CAMPO8
        end
        object RL_CAMPO8: TDBEdit
          Left = 166
          Top = 2
          Width = 300
          Height = 21
          DataField = 'RL_CAMPO8'
          DataSource = dsRefLaboral
          TabOrder = 0
        end
      end
      object RL_CAMPO9Pnl: TPanel
        Left = 0
        Top = 225
        Width = 496
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 11
        object RL_CAMPO9Lbl: TLabel
          Left = 117
          Top = 5
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Campo 9:'
          FocusControl = RL_CAMPO9
        end
        object RL_CAMPO9: TDBEdit
          Left = 166
          Top = 2
          Width = 300
          Height = 21
          DataField = 'RL_CAMPO9'
          DataSource = dsRefLaboral
          TabOrder = 0
        end
      end
      object RL_CAMPO10Pnl: TPanel
        Left = 0
        Top = 249
        Width = 496
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 12
        object RL_CAMPO10Lbl: TLabel
          Left = 111
          Top = 5
          Width = 51
          Height = 13
          Alignment = taRightJustify
          Caption = 'Campo 10:'
          FocusControl = RL_CAMPO10
        end
        object RL_CAMPO10: TDBEdit
          Left = 166
          Top = 2
          Width = 300
          Height = 21
          DataField = 'RL_CAMPO10'
          DataSource = dsRefLaboral
          TabOrder = 0
        end
      end
    end
    object Referencias2: TTabSheet
      Caption = 'Referencias2'
      ImageIndex = 5
      TabVisible = False
      object PanelNumRef2: TPanel
        Left = 0
        Top = 0
        Width = 496
        Height = 25
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = ' Empleos Anteriores # 1 de %s'
        Color = clBtnShadow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object Panel12: TPanel
        Left = 0
        Top = 25
        Width = 496
        Height = 8
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
      end
      object RL_CAMPO11Pnl: TPanel
        Left = 0
        Top = 33
        Width = 496
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object RL_CAMPO11Lbl: TLabel
          Left = 111
          Top = 5
          Width = 51
          Height = 13
          Alignment = taRightJustify
          Caption = 'Campo 11:'
          FocusControl = RL_CAMPO11
        end
        object RL_CAMPO11: TDBEdit
          Left = 166
          Top = 2
          Width = 300
          Height = 21
          DataField = 'RL_CAMPO11'
          DataSource = dsRefLaboral
          TabOrder = 0
        end
      end
      object RL_CAMPO12Pnl: TPanel
        Left = 0
        Top = 57
        Width = 496
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 3
        object RL_CAMPO12Lbl: TLabel
          Left = 111
          Top = 5
          Width = 51
          Height = 13
          Alignment = taRightJustify
          Caption = 'Campo 12:'
          FocusControl = RL_CAMPO12
        end
        object RL_CAMPO12: TDBEdit
          Left = 166
          Top = 2
          Width = 300
          Height = 21
          DataField = 'RL_CAMPO12'
          DataSource = dsRefLaboral
          TabOrder = 0
        end
      end
      object RL_CAMPO13Pnl: TPanel
        Left = 0
        Top = 81
        Width = 496
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 4
        object RL_CAMPO13Lbl: TLabel
          Left = 111
          Top = 5
          Width = 51
          Height = 13
          Alignment = taRightJustify
          Caption = 'Campo 13:'
          FocusControl = RL_CAMPO13
        end
        object RL_CAMPO13: TDBEdit
          Left = 166
          Top = 2
          Width = 300
          Height = 21
          DataField = 'RL_CAMPO13'
          DataSource = dsRefLaboral
          TabOrder = 0
        end
      end
      object RL_CAMPO14Pnl: TPanel
        Left = 0
        Top = 105
        Width = 496
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 5
        object RL_CAMPO14Lbl: TLabel
          Left = 111
          Top = 5
          Width = 51
          Height = 13
          Alignment = taRightJustify
          Caption = 'Campo 14:'
          FocusControl = RL_CAMPO14
        end
        object RL_CAMPO14: TDBEdit
          Left = 166
          Top = 2
          Width = 300
          Height = 21
          DataField = 'RL_CAMPO14'
          DataSource = dsRefLaboral
          TabOrder = 0
        end
      end
      object RL_CAMPO15Pnl: TPanel
        Left = 0
        Top = 129
        Width = 496
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 6
        object RL_CAMPO15Lbl: TLabel
          Left = 111
          Top = 5
          Width = 51
          Height = 13
          Alignment = taRightJustify
          Caption = 'Campo 15:'
          FocusControl = RL_CAMPO15
        end
        object RL_CAMPO15: TDBEdit
          Left = 166
          Top = 2
          Width = 300
          Height = 21
          DataField = 'RL_CAMPO15'
          DataSource = dsRefLaboral
          TabOrder = 0
        end
      end
      object RL_CAMPO16Pnl: TPanel
        Left = 0
        Top = 153
        Width = 496
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 7
        object RL_CAMPO16Lbl: TLabel
          Left = 111
          Top = 6
          Width = 51
          Height = 13
          Alignment = taRightJustify
          Caption = 'Campo 16:'
          FocusControl = RL_CAMPO16
        end
        object RL_CAMPO16: TDBEdit
          Left = 166
          Top = 2
          Width = 300
          Height = 21
          DataField = 'RL_CAMPO16'
          DataSource = dsRefLaboral
          TabOrder = 0
        end
      end
      object RL_CAMPO17Pnl: TPanel
        Left = 0
        Top = 177
        Width = 496
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 8
        object RL_CAMPO17Lbl: TLabel
          Left = 111
          Top = 5
          Width = 51
          Height = 13
          Alignment = taRightJustify
          Caption = 'Campo 17:'
          FocusControl = RL_CAMPO17
        end
        object RL_CAMPO17: TDBEdit
          Left = 166
          Top = 2
          Width = 300
          Height = 21
          DataField = 'RL_CAMPO17'
          DataSource = dsRefLaboral
          TabOrder = 0
        end
      end
      object RL_CAMPO18Pnl: TPanel
        Left = 0
        Top = 201
        Width = 496
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 9
        object RL_CAMPO18Lbl: TLabel
          Left = 111
          Top = 5
          Width = 51
          Height = 13
          Alignment = taRightJustify
          Caption = 'Campo 18:'
          FocusControl = RL_CAMPO18
        end
        object RL_CAMPO18: TDBEdit
          Left = 166
          Top = 2
          Width = 300
          Height = 21
          DataField = 'RL_CAMPO18'
          DataSource = dsRefLaboral
          TabOrder = 0
        end
      end
      object RL_CAMPO19Pnl: TPanel
        Left = 0
        Top = 225
        Width = 496
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 10
        object RL_CAMPO19Lbl: TLabel
          Left = 111
          Top = 5
          Width = 51
          Height = 13
          Alignment = taRightJustify
          Caption = 'Campo 19:'
          FocusControl = RL_CAMPO19
        end
        object RL_CAMPO19: TDBEdit
          Left = 166
          Top = 2
          Width = 300
          Height = 21
          DataField = 'RL_CAMPO19'
          DataSource = dsRefLaboral
          TabOrder = 0
        end
      end
      object RL_CAMPO20Pnl: TPanel
        Left = 0
        Top = 249
        Width = 496
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 11
        object RL_CAMPO20Lbl: TLabel
          Left = 111
          Top = 5
          Width = 51
          Height = 13
          Alignment = taRightJustify
          Caption = 'Campo 20:'
          FocusControl = RL_CAMPO20
        end
        object RL_CAMPO20: TDBEdit
          Left = 166
          Top = 2
          Width = 300
          Height = 21
          DataField = 'RL_CAMPO20'
          DataSource = dsRefLaboral
          TabOrder = 0
        end
      end
    end
    object Documentos: TTabSheet
      Caption = 'Documentos'
      ImageIndex = 6
      TabVisible = False
      object SpeedButton1: TSpeedButton
        Left = 444
        Top = 124
        Width = 23
        Height = 22
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          5555555555555555555555555555555555555555555555555555555555555555
          555555555555555555555555555555555555555FFFFFFFFFF555550000000000
          55555577777777775F55500B8B8B8B8B05555775F555555575F550F0B8B8B8B8
          B05557F75F555555575F50BF0B8B8B8B8B0557F575FFFFFFFF7F50FBF0000000
          000557F557777777777550BFBFBFBFB0555557F555555557F55550FBFBFBFBF0
          555557F555555FF7555550BFBFBF00055555575F555577755555550BFBF05555
          55555575FFF75555555555700007555555555557777555555555555555555555
          5555555555555555555555555555555555555555555555555555}
        NumGlyphs = 2
        OnClick = SpeedButton1Click
      end
      object Label1: TLabel
        Left = 80
        Top = 104
        Width = 24
        Height = 13
        Caption = '&Tipo:'
      end
      object Label5: TLabel
        Left = 45
        Top = 153
        Width = 59
        Height = 13
        Caption = 'Descripci'#243'n:'
      end
      object Label3: TLabel
        Left = 65
        Top = 129
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Archivo:'
      end
      object EArchivo: TEdit
        Left = 107
        Top = 125
        Width = 333
        Height = 21
        TabOrder = 1
        OnChange = EArchivoChange
      end
      object DO_TIPO: TDBComboBox
        Left = 107
        Top = 100
        Width = 108
        Height = 21
        DataField = 'DO_TIPO'
        DataSource = dsDocumentos
        ItemHeight = 13
        Items.Strings = (
          'CURRIC'
          'CURSO'
          'HUELLA'
          'FOTO'
          'ACTA'
          'AUDIO'
          'VOZ')
        TabOrder = 0
        OnChange = DO_TIPOChange
      end
      object DO_OBSERVA: TDBEdit
        Left = 107
        Top = 150
        Width = 360
        Height = 21
        DataField = 'DO_OBSERVA'
        DataSource = dsDocumentos
        MaxLength = 30
        TabOrder = 2
      end
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 496
        Height = 25
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = ' Documentos'
        Color = clBtnShadow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
      end
    end
    object Notas: TTabSheet
      Caption = 'Notas'
      ImageIndex = 7
      TabVisible = False
      object SO_OBSERVA: TDBMemo
        Left = 0
        Top = 25
        Width = 496
        Height = 254
        Align = alClient
        DataField = 'SO_OBSERVA'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 0
      end
      object Panel9: TPanel
        Left = 0
        Top = 0
        Width = 496
        Height = 25
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = ' Notas'
        Color = clBtnShadow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
    end
  end
  object DataSource: TDataSource
    OnDataChange = DataSourceDataChange
    Left = 193
    Top = 271
  end
  object dsRefLaboral: TDataSource
    Left = 216
    Top = 272
  end
  object dsDocumentos: TDataSource
    Left = 240
    Top = 272
  end
  object ImageListPrende: TImageList
    Left = 168
    Top = 274
    Bitmap = {
      494C010104000900040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000003000000001002000000000000030
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000000000000000000000
      000000000000000000000000000000FFFF000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007B7B7B000000
      00007B7B7B007B7B7B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF000000
      00007B7B7B007B7B7B007B7B7B0000FFFF0000FFFF007B7B7B007B7B7B007B7B
      7B007B7B7B0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000BDBDBD000000
      0000BDBDBD00BDBDBD0000000000000000000000000000000000000000000000
      000000000000000000007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B
      7B007B7B7B000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C600000000000000000000000000000000000000
      00000000000000000000C6C6C60000000000000000000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000BDBDBD00BDBD
      BD00BDBDBD00BDBDBD000000000000000000000000007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840000000000000000000000
      0000848484000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000007B7B7B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007B7B7B000000FF007B7B
      7B0000FF00007B7B7B007B7B7B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000007B7B7B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B00BDBDBD00BDBDBD00000000000000
      00000000000000000000BDBDBD000000000000000000BD000000BD000000BD00
      0000BD00000000000000C6C6C60000000000000000000000000000000000C6C6
      C600000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000FFFFFF00000000000000000000000000FFFF
      FF00000000007B7B7B00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000007B7B7B00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00BDBDBD007B7B7B00BDBDBD00BDBDBD00000000000000
      0000000000007B7B7B007B7B7B000000000000000000BD000000FFFFFF00FFFF
      FF00FFFFFF0000000000C6C6C60000000000000000000000000000000000C6C6
      C600000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000007B7B7B00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000007B7B7B00000000000000
      00000000000000000000BDBDBD007B7B7B00BDBDBD00BDBDBD00000000007B7B
      7B00000000007B7B7B00BDBDBD000000000000000000BD000000FFFFFF008484
      8400848484000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF000000
      0000FFFFFF0000000000000000000000000000000000FFFFFF0000000000FFFF
      FF000000000000FFFF0000FFFF000000000000000000FFFFFF00000000000000
      0000FFFFFF000000000000000000BDBDBD0000000000FF000000FF000000FF00
      00000000FF00FF000000FF00000000000000000000007B7B7B00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00BDBDBD007B7B7B00BDBDBD00BDBDBD0000000000BDBD
      BD0000000000BDBDBD00BDBDBD000000000000000000BD000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000C6C6C6000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000FFFF0000FFFF0000FFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      FF000000FF000000FF000000000000000000000000007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B00BDBDBD00BDBDBD00000000000000
      000000000000BDBDBD00BDBDBD000000000000000000BD000000FFFFFF008484
      840084848400FFFFFF008484840000000000000000008484840084848400FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000FFFFFF000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      00000000000000000000FFFFFF0000000000FFFFFF00000000000000FF000000
      FF000000FF000000FF000000FF000000000000000000BDBDBD00BDBDBD00BDBD
      BD00000000000000000000000000BDBDBD00BDBDBD00BDBDBD00000000000000
      00000000000000000000BDBDBD000000000000000000BD000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      000000000000BDBDBD0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BD000000FFFFFF008484
      840084848400FFFFFF008484840000000000000000008484840084848400FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000BDBDBD00FFFFFF0000000000FFFFFF000000000000FF
      FF000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      0000BDBDBD000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BD000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00BD0000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000FF
      FF0000FFFF0000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF000000000000000000000000000000
      FF000000FF000000FF000000000000000000000000000000000000000000BDBD
      BD00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BD000000BD000000BD00
      0000BD000000BD000000BD000000BD000000BD000000BD000000BD000000BD00
      0000BD000000000000000000000000000000000000000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF00000000000000000000000000FFFFFF0000000000BDBD
      BD00FFFFFF0000000000FFFFFF000000000000000000000000007B7B7B000000
      FF000000FF000000FF00000000000000000000000000BDBDBD00BDBDBD000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BD000000C6C6C600BD00
      0000BD000000C6C6C600BD000000BD000000C6C6C600BD000000BD000000C6C6
      C600BD0000000000000000000000000000000000000000FFFF0000FFFF000000
      000000000000000000000000000000FFFF0000FFFF0000000000000000000000
      00000000000000FFFF0000FFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000FF000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000BDBDBD000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BD000000BD000000BD00
      0000BD000000BD000000BD000000BD000000BD000000BD000000BD000000BD00
      0000BD00000000000000000000000000000000FFFF0000000000000000000000
      000000000000000000000000000000FFFF000000000000000000000000000000
      000000000000000000000000000000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000300000000100010000000000800100000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF7EFF00FC07F8F89001FF008003F8F8
      C003FF000007F870E003FF00003CF800E003000000188000E003000000008000
      E00300000000800000010000000080018000002300008003E007000100188003
      E00F0000803C8003E00F0023E3FF8007E027006387FF8007C07300C30FFF8007
      9E7901078FFF80077EFE03FFDFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object OpenDialog: TOpenDialog
    Filter = 
      'Documentos (*.DOC,*.TXT,*.RTF,*.XLS,*.PDF,*.HTM,*.HTML)|*.doc;*.' +
      'txt;*.RTF;*.XLS;*.PDF|Imagenes (*.BMP;*.CMS;*.GIF;*.JPG;*.PCX;*.' +
      'PNG;*.SCM;*.TIF;*.WAV;*.MID;*.RMI;*.AVI;*.MOV)|*.BMP;*.CMS;*.GIF' +
      ';*.JPG;*.PCX;*.PNG;*.SCM;*.TIF;*.WAV;*.MID;*.RMI;*.AVI;*.MOV|Aud' +
      'io/Video (*.WAV,*.MDI,*.MP3,*.WMA,*.CDA,*.MPG,*.AVI,*.MOV)|*.WAV' +
      ';*.MDI;*.MP3;*.WMA;*.CDA;*.MPG;*.AVI;*.MOV|Todos los Archivos (*' +
      '.*)|*.*'
    Left = 272
    Top = 272
  end
  object OpenPictureDialog: TOpenPictureDialog
    DefaultExt = 'JPG'
    Filter = 
      'JPEG Image File (*.jpg)|*.jpg|JPEG Image File (*.jpeg)|*.jpeg|Bi' +
      'tmaps (*.bmp)|*.bmp|GIF Image File (*.gif)|*.gif|TIF Image File ' +
      '(*.tif)|*.tif|PCX Image File (*.pcx)|*.pcx|PNG Image File (*.png' +
      ')|*.png'
    FilterIndex = 0
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist]
    Title = 'Leer Fotograf'#237'a de Disco'
    Left = 120
    Top = 274
  end
  object SavePictureDialog: TSavePictureDialog
    Filter = 
      'JPEG Image File (*.jpg)|*.jpg|JPEG Image File (*.jpeg)|*.jpeg|Bi' +
      'tmaps (*.bmp)|*.bmp|GIF Image File (*.gif)|*.gif|TIF Image File ' +
      '(*.tif)|*.tif|PCX Image File (*.pcx)|*.pcx|PNG Image File (*.png' +
      ')|*.png'
    FilterIndex = 0
    Title = 'Grabar Fotograf'#237'a a Disco'
    Left = 144
    Top = 276
  end
end
