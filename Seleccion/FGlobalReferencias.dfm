inherited GlobalReferencias: TGlobalReferencias
  Left = 441
  Top = 243
  ActiveControl = QtyReferencias
  Caption = 'Referencias Laborales'
  ClientHeight = 344
  ClientWidth = 584
  PixelsPerInch = 96
  TextHeight = 13
  object LblCuantos: TLabel [0]
    Left = 64
    Top = 10
    Width = 169
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cantidad de &Referencias Laborales:'
    FocusControl = QtyReferencias
  end
  object Label11: TLabel [1]
    Left = 304
    Top = 66
    Width = 45
    Height = 13
    Alignment = taRightJustify
    Caption = 'Campo &1:'
  end
  object Label12: TLabel [2]
    Left = 304
    Top = 89
    Width = 45
    Height = 13
    Alignment = taRightJustify
    Caption = 'Campo &2:'
  end
  object Label13: TLabel [3]
    Left = 304
    Top = 112
    Width = 45
    Height = 13
    Alignment = taRightJustify
    Caption = 'Campo &3:'
  end
  object Label14: TLabel [4]
    Left = 304
    Top = 135
    Width = 45
    Height = 13
    Alignment = taRightJustify
    Caption = 'Campo &4:'
  end
  object Label15: TLabel [5]
    Left = 304
    Top = 157
    Width = 45
    Height = 13
    Alignment = taRightJustify
    Caption = 'Campo &5:'
  end
  object Label16: TLabel [6]
    Left = 304
    Top = 180
    Width = 45
    Height = 13
    Alignment = taRightJustify
    Caption = 'Campo &6:'
  end
  object Label17: TLabel [7]
    Left = 304
    Top = 203
    Width = 45
    Height = 13
    Alignment = taRightJustify
    Caption = 'Campo &7:'
  end
  object Label18: TLabel [8]
    Left = 304
    Top = 226
    Width = 45
    Height = 13
    Alignment = taRightJustify
    Caption = 'Campo &8:'
  end
  object Label19: TLabel [9]
    Left = 304
    Top = 248
    Width = 45
    Height = 13
    Alignment = taRightJustify
    Caption = 'Campo &9:'
  end
  object Label20: TLabel [10]
    Left = 298
    Top = 272
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = 'Campo 1&0:'
  end
  inherited PanelBotones: TPanel
    Top = 308
    Width = 584
    inherited OK: TBitBtn
      Left = 415
    end
    inherited Cancelar: TBitBtn
      Left = 495
    end
  end
  object QtyReferencias: TZetaNumero
    Left = 242
    Top = 5
    Width = 49
    Height = 21
    Mascara = mnMinutos
    TabOrder = 1
    Text = '0'
  end
  object PageControl1: TPageControl
    Left = 8
    Top = 32
    Width = 561
    Height = 273
    ActivePage = Datos
    TabOrder = 2
    object Datos: TTabSheet
      Caption = 'Datos de la Referencia'
      object Label1: TLabel
        Left = 16
        Top = 10
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 1:'
        FocusControl = NombreCampo1
      end
      object Label2: TLabel
        Left = 16
        Top = 33
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 2:'
        FocusControl = NombreCampo2
      end
      object Label3: TLabel
        Left = 16
        Top = 56
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 3:'
        FocusControl = NombreCampo3
      end
      object Label4: TLabel
        Left = 16
        Top = 79
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 4:'
        FocusControl = NombreCampo4
      end
      object Label5: TLabel
        Left = 16
        Top = 101
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 5:'
        FocusControl = NombreCampo5
      end
      object Label6: TLabel
        Left = 16
        Top = 124
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 6:'
        FocusControl = NombreCampo6
      end
      object Label7: TLabel
        Left = 16
        Top = 147
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 7:'
        FocusControl = NombreCampo7
      end
      object Label8: TLabel
        Left = 16
        Top = 170
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 8:'
        FocusControl = NombreCampo8
      end
      object Label9: TLabel
        Left = 16
        Top = 192
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 9:'
        FocusControl = NombreCampo9
      end
      object Label10: TLabel
        Left = 10
        Top = 216
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 10:'
        FocusControl = NombreCampo10
      end
      object Label21: TLabel
        Left = 286
        Top = 10
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 11:'
        FocusControl = NombreCampo1
      end
      object Label22: TLabel
        Left = 286
        Top = 33
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 12:'
        FocusControl = NombreCampo2
      end
      object Label23: TLabel
        Left = 286
        Top = 56
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 13:'
        FocusControl = NombreCampo3
      end
      object Label24: TLabel
        Left = 286
        Top = 79
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 14:'
        FocusControl = NombreCampo4
      end
      object Label25: TLabel
        Left = 286
        Top = 101
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 15:'
        FocusControl = NombreCampo5
      end
      object Label26: TLabel
        Left = 286
        Top = 124
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 16:'
        FocusControl = NombreCampo6
      end
      object Label27: TLabel
        Left = 286
        Top = 147
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 17:'
        FocusControl = NombreCampo7
      end
      object Label28: TLabel
        Left = 286
        Top = 170
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 18:'
        FocusControl = NombreCampo8
      end
      object Label29: TLabel
        Left = 286
        Top = 192
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 19:'
        FocusControl = NombreCampo9
      end
      object Label30: TLabel
        Left = 286
        Top = 216
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 20:'
        FocusControl = NombreCampo10
      end
      object NombreCampo1: TEdit
        Tag = 13
        Left = 64
        Top = 6
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 0
      end
      object NombreCampo2: TEdit
        Tag = 14
        Left = 64
        Top = 29
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 1
      end
      object NombreCampo3: TEdit
        Tag = 15
        Left = 64
        Top = 52
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 2
      end
      object NombreCampo4: TEdit
        Tag = 16
        Left = 64
        Top = 75
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 3
      end
      object NombreCampo5: TEdit
        Tag = 17
        Left = 64
        Top = 98
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 4
      end
      object NombreCampo6: TEdit
        Tag = 18
        Left = 64
        Top = 121
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 5
      end
      object NombreCampo7: TEdit
        Tag = 19
        Left = 64
        Top = 144
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 6
      end
      object NombreCampo8: TEdit
        Tag = 20
        Left = 64
        Top = 167
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 7
      end
      object NombreCampo9: TEdit
        Tag = 21
        Left = 64
        Top = 190
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 8
      end
      object NombreCampo10: TEdit
        Tag = 21
        Left = 64
        Top = 213
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 9
      end
      object NombreCampo11: TEdit
        Tag = 13
        Left = 340
        Top = 6
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 10
      end
      object NombreCampo12: TEdit
        Tag = 14
        Left = 340
        Top = 29
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 11
      end
      object NombreCampo13: TEdit
        Tag = 15
        Left = 340
        Top = 52
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 12
      end
      object NombreCampo14: TEdit
        Tag = 16
        Left = 340
        Top = 75
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 13
      end
      object NombreCampo15: TEdit
        Tag = 17
        Left = 340
        Top = 98
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 14
      end
      object NombreCampo16: TEdit
        Tag = 18
        Left = 340
        Top = 121
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 15
      end
      object NombreCampo17: TEdit
        Tag = 19
        Left = 340
        Top = 144
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 16
      end
      object NombreCampo18: TEdit
        Tag = 20
        Left = 340
        Top = 167
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 17
      end
      object NombreCampo19: TEdit
        Tag = 21
        Left = 340
        Top = 190
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 18
      end
      object NombreCampo20: TEdit
        Tag = 21
        Left = 340
        Top = 213
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 19
      end
    end
    object Verificacion: TTabSheet
      Caption = 'Resultados de Verificaci'#243'n'
      ImageIndex = 1
      object Label31: TLabel
        Left = 16
        Top = 10
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 1:'
        FocusControl = NombreCampoRV1
      end
      object Label32: TLabel
        Left = 16
        Top = 33
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 2:'
        FocusControl = NombreCampoRV2
      end
      object Label33: TLabel
        Left = 16
        Top = 56
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 3:'
        FocusControl = NombreCampoRV3
      end
      object Label34: TLabel
        Left = 16
        Top = 79
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 4:'
        FocusControl = NombreCampoRV4
      end
      object Label35: TLabel
        Left = 16
        Top = 101
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 5:'
        FocusControl = NombreCampoRV5
      end
      object Label36: TLabel
        Left = 16
        Top = 124
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 6:'
        FocusControl = NombreCampoRV6
      end
      object Label37: TLabel
        Left = 16
        Top = 147
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 7:'
        FocusControl = NombreCampoRV7
      end
      object Label38: TLabel
        Left = 16
        Top = 170
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 8:'
        FocusControl = NombreCampoRV8
      end
      object Label39: TLabel
        Left = 16
        Top = 192
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 9:'
        FocusControl = NombreCampoRV9
      end
      object Label40: TLabel
        Left = 10
        Top = 216
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 10:'
        FocusControl = NombreCampoRV10
      end
      object Label41: TLabel
        Left = 286
        Top = 216
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 20:'
        FocusControl = NombreCampoRV10
      end
      object Label42: TLabel
        Left = 286
        Top = 192
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 19:'
        FocusControl = NombreCampoRV9
      end
      object Label43: TLabel
        Left = 286
        Top = 170
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 18:'
        FocusControl = NombreCampoRV8
      end
      object Label44: TLabel
        Left = 286
        Top = 147
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 17:'
        FocusControl = NombreCampoRV7
      end
      object Label45: TLabel
        Left = 286
        Top = 124
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 16:'
        FocusControl = NombreCampoRV6
      end
      object Label46: TLabel
        Left = 286
        Top = 101
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 15:'
        FocusControl = NombreCampoRV5
      end
      object Label47: TLabel
        Left = 286
        Top = 79
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 14:'
        FocusControl = NombreCampoRV4
      end
      object Label48: TLabel
        Left = 286
        Top = 56
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 13:'
        FocusControl = NombreCampoRV3
      end
      object Label49: TLabel
        Left = 286
        Top = 33
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 12:'
        FocusControl = NombreCampoRV2
      end
      object Label50: TLabel
        Left = 286
        Top = 10
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Campo 11:'
        FocusControl = NombreCampoRV1
      end
      object NombreCampoRV1: TEdit
        Tag = 13
        Left = 64
        Top = 6
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 0
      end
      object NombreCampoRV2: TEdit
        Tag = 14
        Left = 64
        Top = 29
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 1
      end
      object NombreCampoRV3: TEdit
        Tag = 15
        Left = 64
        Top = 52
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 2
      end
      object NombreCampoRV4: TEdit
        Tag = 16
        Left = 64
        Top = 75
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 3
      end
      object NombreCampoRV5: TEdit
        Tag = 17
        Left = 64
        Top = 98
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 4
      end
      object NombreCampoRV6: TEdit
        Tag = 18
        Left = 64
        Top = 121
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 5
      end
      object NombreCampoRV7: TEdit
        Tag = 19
        Left = 64
        Top = 144
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 6
      end
      object NombreCampoRV8: TEdit
        Tag = 20
        Left = 64
        Top = 167
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 7
      end
      object NombreCampoRV9: TEdit
        Tag = 21
        Left = 64
        Top = 190
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 8
      end
      object NombreCampoRV10: TEdit
        Tag = 21
        Left = 64
        Top = 213
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 9
      end
      object NombreCampoRV20: TEdit
        Tag = 21
        Left = 340
        Top = 213
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 19
      end
      object NombreCampoRV19: TEdit
        Tag = 21
        Left = 340
        Top = 190
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 18
      end
      object NombreCampoRV18: TEdit
        Tag = 20
        Left = 340
        Top = 167
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 17
      end
      object NombreCampoRV17: TEdit
        Tag = 19
        Left = 340
        Top = 144
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 16
      end
      object NombreCampoRV16: TEdit
        Tag = 18
        Left = 340
        Top = 121
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 15
      end
      object NombreCampoRV15: TEdit
        Tag = 17
        Left = 340
        Top = 98
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 14
      end
      object NombreCampoRV14: TEdit
        Tag = 16
        Left = 340
        Top = 75
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 13
      end
      object NombreCampoRV13: TEdit
        Tag = 15
        Left = 340
        Top = 52
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 12
      end
      object NombreCampoRV12: TEdit
        Tag = 14
        Left = 340
        Top = 29
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 11
      end
      object NombreCampoRV11: TEdit
        Tag = 13
        Left = 340
        Top = 6
        Width = 200
        Height = 21
        MaxLength = 25
        TabOrder = 10
      end
    end
  end
end
