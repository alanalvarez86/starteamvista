unit FAltaRequisicion;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZWizardBasico, ComCtrls, StdCtrls, Buttons, ZetaWizard, ExtCtrls,
  ZetaFecha, Mask, ZetaNumero, ZetaKeyLookup, ZetaKeyCombo, DBCtrls, Db;

type
  TAltaRequisicion = class(TWizardBasico)
    Inicial: TTabSheet;
    Generales: TTabSheet;
    Perfil: TTabSheet;
    Label4: TLabel;
    RQ_PRIORID: TZetaDBKeyCombo;
    Label2: TLabel;
    RQ_PUESTO: TZetaDBKeyLookup;
    Label5: TLabel;
    RQ_VACANTE: TZetaDBNumero;
    Label20: TLabel;
    RQ_FEC_INI: TZetaDBFecha;
    RQ_FEC_PRO: TZetaDBFecha;
    Label21: TLabel;
    Label18: TLabel;
    RQ_MOTIVO: TZetaDBKeyCombo;
    RQ_TURNO: TDBEdit;
    Label23: TLabel;
    Label24: TLabel;
    RQ_CLIENTE: TZetaDBKeyLookup;
    RQ_GTOPRES: TZetaDBNumero;
    PresupuestoLbl: TLabel;
    GBEdad: TGroupBox;
    Label8: TLabel;
    Label11: TLabel;
    Label10: TLabel;
    RQ_EDADMIN: TZetaDBNumero;
    RQ_EDADMAX: TZetaDBNumero;
    Label29: TLabel;
    RQ_SEXO: TZetaDBKeyCombo;
    RQ_EST_MIN: TZetaDBKeyCombo;
    Label7: TLabel;
    LblIngles: TLabel;
    RQ_INGLES: TZetaDBNumero;
    Label12: TLabel;
    RQ_AREA: TZetaDBKeyLookup;
    Label6: TLabel;
    Label15: TLabel;
    QU_CODIGO: TZetaDBKeyLookup;
    Oferta: TTabSheet;
    Panel4: TPanel;
    Label13: TLabel;
    Label14: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label17: TLabel;
    RQ_SUELDO: TDBEdit;
    RQ_VACACI: TDBEdit;
    RQ_BONO: TDBEdit;
    RQ_VALES: TDBEdit;
    RQ_AGUINAL: TDBEdit;
    RQ_OFERTA: TDBMemo;
    DataSource: TDataSource;
    RQ_OBSERVA: TDBMemo;
    Observaciones: TTabSheet;
    Panel3: TPanel;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    lblContratos: TLabel;
    RQ_TIPO_CO: TZetaDBKeyLookup;
    ReemplazaLbl: TLabel;{OP:11.Abr.08}
    RQ_REEMPLA: TDBEdit;
    Label3: TLabel;
    RQ_HABILID: TDBMemo;
    GroupBox1: TGroupBox;{OP:14.Abr.08}
    RQ_AUTORIZ: TDBEdit;{OP:14.Abr.08}
    Label19: TLabel;{OP:14.Abr.08}
    Label9: TLabel;{OP:14.Abr.08}
    RQ_PUES_AU: TDBEdit;{OP:14.Abr.08}
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
    procedure RQ_PUESTOValidLookup(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure RQ_MOTIVOChange(Sender: TObject);
    procedure RQ_CLIENTEValidLookup(Sender: TObject);
  private
    { Private declarations }
    procedure DoConnect;
    procedure DoDisconnect;
    procedure Connect;
    procedure Disconnect;
    function CancelarWizard: Boolean;
    function EjecutarWizard: Boolean;
    procedure TerminarWizard;
    procedure VerificaReemplazo;{OP:11.Abr.08}
    procedure ActivaAutorizo;{OP:14.Abr.08}
  public
    { Public declarations }
  end;

var
  AltaRequisicion: TAltaRequisicion;

implementation

uses DSeleccion,
     ZetaClientDataSet,
     ZetaDialogo,
     ZetaCommonLists,
     ZetaCommonTools,
     ZAccesosMgr,
     ZAccesosTress;

{$R *.DFM}

procedure TAltaRequisicion.FormCreate(Sender: TObject);
var
   eSex: eSexo;
begin
     inherited;
     with RQ_SEXO.Lista do
     begin
          BeginUpdate;
          try
             Clear;
             Add( Format( '%s=%s', [ 'I', '<Cualquiera>' ] ) );
             for eSex := Low( eSexo ) to High( eSexo ) do
             begin
                  Add( Format( '%s=%s', [ ZetaCommonLists.ObtieneElemento( lfSexo, Ord( eSex ) ), ZetaCommonLists.ObtieneElemento( lfSexoDesc, Ord( eSex ) ) ] ) );
             end;
          finally
                 EndUpdate;
          end;
     end;
     RQ_TIPO_CO.LookupDataset := dmSeleccion.cdsContratos;
end;
procedure TAltaRequisicion.FormShow(Sender: TObject);
begin
     inherited;
     DoConnect;
     RQ_GTOPRES.Visible := ZAccesosMgr.Revisa( D_REQUISICION_PRESUPUESTO );{OP:7.Mayo.08}
     PresupuestoLbl.Visible := RQ_GTOPRES.Visible;{OP:7.Mayo.08}
     Oferta.Enabled := ZAccesosMgr.Revisa( D_REQUISICION_OFERTA );{OP:11.Abr.08}
end;

procedure TAltaRequisicion.FormClose(Sender: TObject;var Action: TCloseAction);
begin
     inherited;
     DoDisconnect;
     Action := caHide;
end;

procedure TAltaRequisicion.Connect;
begin
     with dmSeleccion do
     begin
          RQ_AREA.LookUpDataSet := cdsAreaInteres;
          RQ_PUESTO.LookUpDataSet := cdsPuestos;
          RQ_CLIENTE.LookUpDataSet := cdsCliente;
          QU_CODIGO.LookUpDataSet := cdsCondiciones;
          cdsAreaInteres.Conectar;
          cdsTipoGasto.Conectar;
          cdsPuestos.Conectar;
          cdsCliente.Conectar;
          cdsCondiciones.Conectar;
          DataSource.DataSet:= cdsEditRequiere;
          if StrVacio(cdsEditRequiere.FieldByName('RQ_SEXO').AsString) then
             RQ_SEXO.ItemIndex := 0;
          if SetControlesEmpresaTress( lblContratos, RQ_TIPO_CO ) then
             cdsContratos.Conectar;
     end;
     PageControl.ActivePage := Inicial;
     RQ_PUESTO.SetFocus;
     RQ_PUESTOValidLookup( self );
     VerificaReemplazo;{OP:11.Abr.08}     
end;

procedure TAltaRequisicion.Disconnect;
begin
     Datasource.Dataset := nil;
end;

procedure TAltaRequisicion.DoConnect;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             try
                Connect;
             except
                   on Error: Exception do
                      zExcepcion( '� Error Al Conectar Forma !', '� Ventana No Pudo Ser Conectada A Sus Datos !', Error, 0 );
             end;
          finally
                 Cursor := oCursor;
          end;
     end;

end;

procedure TAltaRequisicion.DoDisconnect;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Disconnect;
          finally
                 Cursor := oCursor;
          end;
     end;end;

procedure TAltaRequisicion.WizardAlEjecutar(Sender: TObject;
  var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        Application.ProcessMessages;
        try
           lOk := EjecutarWizard;
        except
              on Error: Exception do
              begin
                   zExcepcion( Caption, 'Error Al Ejecutar Wizard', Error, 0 );
                   lOk := False;
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     if lOk then
        TerminarWizard;
end;

function TAltaRequisicion.EjecutarWizard: Boolean;
var
   oCursor: TCursor;
begin
     with TZetaClientDataSet(DataSource.DataSet) do
     begin
          with Screen do
          begin
               oCursor := Cursor;
               Cursor := crHourglass;
               try
                  Result := TRUE;
                  Enviar;
               finally
                      Cursor := oCursor;
               end;
          end;
     end;
end;

function TAltaRequisicion.CancelarWizard: Boolean;
begin
     Close;
     Result := True;
end;

procedure TAltaRequisicion.TerminarWizard;
begin
     if not Wizard.Reejecutar then
     begin
          Close;
          Windows.SetForegroundWindow( Handle ); { Evita que la aplicaci�n pierda el Focus }
     end;
end;

procedure TAltaRequisicion.WizardAlCancelar(Sender: TObject;
  var lOk: Boolean);
begin
     inherited;
     lOk := CancelarWizard;
end;

procedure TAltaRequisicion.RQ_PUESTOValidLookup(Sender: TObject);
begin
     with RQ_PUESTO do
     begin
          if strLleno( Llave ) then
          begin
               //PU_HABILID.Lines.Text := LookupDataSet.FieldByName( 'PU_HABILID' ).AsString;
               //PU_ACTIVID.Lines.Text := LookupDataSet.FieldByName( 'PU_ACTIVID' ).AsString;

               with DataSource.Dataset do
               begin
                    if State <> dsBrowse then
                    begin
                         FieldByName('RQ_EDADMIN').AsInteger := LookupDataSet.FieldByName( 'PU_EDADMIN' ).AsInteger;
                         FieldByName('RQ_EDADMAX').AsInteger := LookupDataSet.FieldByName( 'PU_EDADMAX').AsInteger;
                         if StrVacio(LookupDataSet.FieldByName( 'PU_SEXO' ).AsString) then
                            RQ_SEXO.ItemIndex := 0
                         else FieldByName('RQ_SEXO').AsString := LookupDataSet.FieldByName( 'PU_SEXO' ).AsString;
                         FieldByName('RQ_ESTUDIO').AsInteger := LookupDataSet.FieldByName( 'PU_ESTUDIO').AsInteger;
                         FieldByName('RQ_INGLES').AsInteger := LookupDataSet.FieldByName( 'PU_DOMINA1').AsInteger;
                         FieldByName('RQ_AREA').AsString := LookupDataSet.FieldByName( 'PU_AREA' ).AsString;
                         FieldByName('QU_CODIGO').AsString := LookupDataSet.FieldByName( 'QU_CODIGO' ).AsString;
                         FieldByName('RQ_SUELDO').AsString := LookupDataSet.FieldByName('PU_SUELDO').AsString;
                         FieldByName('RQ_VACACI').AsString := LookupDataSet.FieldByName('PU_VACACI').AsString;
                         FieldByName('RQ_AGUINAL').AsString := LookupDataSet.FieldByName('PU_AGUINAL').AsString;
                         FieldByName('RQ_BONO').AsString := LookupDataSet.FieldByName('PU_BONO').AsString;
                         FieldByName('RQ_VALES').AsString := LookupDataSet.FieldByName('PU_VALES').AsString;
                         FieldByName('RQ_OFERTA').AsString := LookupDataSet.FieldByName('PU_OFERTA').AsString;
                    end;
               end;
          end
          else
          begin
               //PU_HABILID.Lines.Text := VACIO;
               //PU_ACTIVID.Lines.Text := VACIO;
          end;
     end;
end;

procedure TAltaRequisicion.WizardAfterMove(Sender: TObject);
begin
     inherited;
     with Wizard do
          if EsPaginaActual( Inicial ) then
             RQ_PUESTO.SetFocus
          else if EsPaginaActual( Generales ) then
               RQ_MOTIVO.SetFocus
          else if EsPaginaActual( Perfil ) then
               RQ_EDADMIN.SetFocus
          else if EsPaginaActual( Oferta ) then
               RQ_SUELDO.SetFocus
          else if EsPaginaActual( Observaciones ) then
               RQ_OBSERVA.SetFocus;
end;

procedure TAltaRequisicion.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if (DataSource.Dataset <> NIL) AND Wizard.EsPaginaActual(Inicial) then
        with DataSource.Dataset do
        begin
             if StrVacio(FieldByName('RQ_PUESTO').AsString) then
             begin
                  CanMove := FALSE;
                  FieldByName('RQ_PUESTO').FocusControl;
                  DataBaseError('El Puesto no Puede Quedar Vac�o');
             end;
             if FieldByName('RQ_VACANTE').AsInteger = 0  then
             begin
                  CanMove := FALSE;
                  FieldByName('RQ_VACANTE').FocusControl;
                  DataBaseError('El N�mero de Vacantes no Puede Ser Cero');
             end;
        end;
end;

{OP:11.Abr.08}
{Verifica si el motivo es 'Reemplazo', de serlo, activa el campo para
ingresar el nombre del personal que se reemplaza.}
procedure TAltaRequisicion.VerificaReemplazo;
var
   lEnabled : Boolean;
begin
     lEnabled := eMotivoVacante( RQ_MOTIVO.Valor ) = mvReemplazo;

     RQ_REEMPLA.Enabled := lEnabled;
     ReemplazaLbl.Enabled := lEnabled;
end;

{OP:14.Abr.08}
{Al ser invocado, si el nombre y puesto de quien autoriza est� tiene info.
deshabilita ambos controles.}
procedure TAltaRequisicion.ActivaAutorizo;
var
   lAutorNomLleno, lAutorPueLleno: Boolean;
   oControl: TWinControl;
begin
     with dmSeleccion.cdsCliente do
     begin
          lAutorNomLleno := StrLleno( FieldByName( 'CL_NOM_AU' ).AsString );
          lAutorPueLleno := StrLleno( FieldByName( 'CL_PUES_AU' ).AsString );
     end;

//     oControl := Self.ActiveControl;

     if ( RQ_GTOPRES.Visible ) then
        oControl := RQ_GTOPRES
     else
     begin
          if ( NOT lAutorNomLleno ) then
             oControl := RQ_AUTORIZ
          else if ( NOT lAutorPueLleno ) then
              oControl := RQ_PUES_AU
          else
              oControl := RQ_TIPO_CO;
     end;

     RQ_AUTORIZ.Enabled := NOT ( lAutorNomLleno );
     RQ_PUES_AU.Enabled := NOT ( lAutorPueLleno );

     Self.ActiveControl := oControl;
end;

procedure TAltaRequisicion.RQ_MOTIVOChange(Sender: TObject);
begin
     inherited;
     VerificaReemplazo();{OP:11.Abr.08}
end;

{OP:14.Abr.08}
procedure TAltaRequisicion.RQ_CLIENTEValidLookup(Sender: TObject);
begin
     inherited;
     ActivaAutorizo;
end;

end.
