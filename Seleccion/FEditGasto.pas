unit FEditGasto;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Mask,
     Forms, Dialogs, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls,
     ZBaseEdicion,
     ZetaKeyLookup,
     ZetaFecha,
     ZetaDBTextBox,
     ZetaNumero;

type
  TEditGasto = class(TBaseEdicion)
    RQ_FOLIOlbl: TLabel;
    RQ_FOLIO: TZetaDBTextBox;
    RG_FOLIOlbl: TLabel;
    RG_FOLIO: TZetaDBTextBox;
    RG_FECHAlbl: TLabel;
    RG_FECHA: TZetaDBFecha;
    RG_MONTOlbl: TLabel;
    RG_DESCRIPlbl: TLabel;
    RG_DESCRIP: TDBEdit;
    RG_MONTO: TZetaDBNumero;
    RG_TIPOlbl: TLabel;
    RG_TIPO: TZetaDBKeyLookup;
    RG_COMENTAlbl: TLabel;
    RG_COMENTA: TDBMemo;
    procedure FormCreate(Sender: TObject);
  private
  protected
    { Private declarations }
  public
    { Public declarations }
    procedure Connect; override;
    procedure Agregar;override;
  end;

var
  EditGasto: TEditGasto;

implementation

{$R *.DFM}

uses DSeleccion,
     ZAccesosTress,
     ZetaCommonClasses;

{ TFormaGasto }

procedure TEditGasto.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := RG_FECHA;
     IndexDerechos := D_REQUISICION_GASTOS;
     HelpContext := H00055_Requisicion_Gastos;
     RG_TIPO.LookupDataset := dmSeleccion.cdsTipoGasto;
end;

procedure TEditGasto.Connect;
begin
     with dmSeleccion do
     begin
          cdsTipoGasto.Conectar;
          Datasource.Dataset := cdsReqGastos;
     end;
end;

procedure TEditGasto.Agregar;
begin
     dmSeleccion.GetLastFolioGasto;
     inherited;
end;

end.


