unit FEditRefLaboral;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, ZetaDBTextBox, StdCtrls, Db, ExtCtrls, DBCtrls, Buttons,
  Mask, ZetaSmartLists, ComCtrls;

type
  TEditRefLaboral = class(TBaseEdicion)
    PanelHeader: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    SO_FOLIO: TZetaDBTextBox;
    RL_FOLIO: TZetaDBTextBox;
    PRETTYNAME: TZetaTextBox;
    PanelCampos: TPanel;
    ReferenciasPg: TPageControl;{OP:22.Abr.08}
    Referencia: TTabSheet;{OP:22.Abr.08}
    Verificacion: TTabSheet;{OP:22.Abr.08}
    RL_CAMPO1Pnl: TPanel;
    RL_CAMPO1Lbl: TLabel;
    RL_CAMPO1: TDBEdit;
    RL_CAMPO2Pnl: TPanel;
    RL_CAMPO2Lbl: TLabel;
    RL_CAMPO2: TDBEdit;
    RL_CAMPO4Pnl: TPanel;
    RL_CAMPO4Lbl: TLabel;
    RL_CAMPO4: TDBEdit;
    RL_CAMPO3Pnl: TPanel;
    RL_CAMPO3Lbl: TLabel;
    RL_CAMPO3: TDBEdit;
    RL_CAMPO5Pnl: TPanel;
    RL_CAMPO5Lbl: TLabel;
    RL_CAMPO5: TDBEdit;
    RL_CAMPO6Pnl: TPanel;
    RL_CAMPO6Lbl: TLabel;
    RL_CAMPO6: TDBEdit;
    RL_CAMPO7Pnl: TPanel;
    RL_CAMPO7Lbl: TLabel;
    RL_CAMPO7: TDBEdit;
    RL_CAMPO8Pnl: TPanel;
    RL_CAMPO8Lbl: TLabel;
    RL_CAMPO8: TDBEdit;
    RL_CAMPO9Pnl: TPanel;
    RL_CAMPO9Lbl: TLabel;
    RL_CAMPO9: TDBEdit;
    RL_CAMPO10Pnl: TPanel;
    RL_CAMPO10Lbl: TLabel;
    RL_CAMPO10: TDBEdit;
    RL_CAMPO11Pnl: TPanel;{OP:22.Abr.08}
    RL_CAMPO11Lbl: TLabel;{OP:22.Abr.08}
    RL_CAMPO11: TDBEdit;{OP:22.Abr.08}
    RL_CAMPO12Pnl: TPanel;{OP:22.Abr.08}
    RL_CAMPO12Lbl: TLabel;{OP:22.Abr.08}
    RL_CAMPO12: TDBEdit;{OP:22.Abr.08}
    RL_CAMPO13Pnl: TPanel;{OP:22.Abr.08}
    RL_CAMPO13Lbl: TLabel;{OP:22.Abr.08}
    RL_CAMPO13: TDBEdit;{OP:22.Abr.08}
    RL_CAMPO14Pnl: TPanel;{OP:22.Abr.08}
    RL_CAMPO14Lbl: TLabel;{OP:22.Abr.08}
    RL_CAMPO14: TDBEdit;{OP:22.Abr.08}
    RL_CAMPO15Pnl: TPanel;{OP:22.Abr.08}
    RL_CAMPO15Lbl: TLabel;{OP:22.Abr.08}
    RL_CAMPO15: TDBEdit;{OP:22.Abr.08}
    RL_CAMPO16Pnl: TPanel;{OP:22.Abr.08}
    RL_CAMPO16Lbl: TLabel;{OP:22.Abr.08}
    RL_CAMPO16: TDBEdit;{OP:22.Abr.08}
    RL_CAMPO17Pnl: TPanel;{OP:22.Abr.08}
    RL_CAMPO17Lbl: TLabel;{OP:22.Abr.08}
    RL_CAMPO17: TDBEdit;{OP:22.Abr.08}
    RL_CAMPO18Pnl: TPanel;{OP:22.Abr.08}
    RL_CAMPO18Lbl: TLabel;{OP:22.Abr.08}
    RL_CAMPO18: TDBEdit;{OP:22.Abr.08}
    RL_CAMPO19Pnl: TPanel;{OP:22.Abr.08}
    RL_CAMPO19Lbl: TLabel;{OP:22.Abr.08}
    RL_CAMPO19: TDBEdit;{OP:22.Abr.08}
    RL_CAMPO20Pnl: TPanel;{OP:22.Abr.08}
    RL_CAMPO20Lbl: TLabel;{OP:22.Abr.08}
    RL_CAMPO20: TDBEdit;{OP:22.Abr.08}
    RLV_CAMPO1Pnl: TPanel;{OP:22.Abr.08}
    RLV_CAMPO1Lbl: TLabel;{OP:22.Abr.08}
    RLV_CAMPO1: TDBEdit; {OP:22.Abr.08}
    RLV_CAMPO2Pnl: TPanel;{OP:22.Abr.08}
    RLV_CAMPO2Lbl: TLabel;{OP:22.Abr.08}
    RLV_CAMPO2: TDBEdit;{OP:22.Abr.08}
    RLV_CAMPO3Pnl: TPanel;{OP:22.Abr.08}
    RLV_CAMPO3Lbl: TLabel; {OP:22.Abr.08}
    RLV_CAMPO3: TDBEdit; {OP:22.Abr.08}
    RLV_CAMPO4Pnl: TPanel;{OP:22.Abr.08}
    RLV_CAMPO4Lbl: TLabel;{OP:22.Abr.08}
    RLV_CAMPO4: TDBEdit;{OP:22.Abr.08}
    RLV_CAMPO5Pnl: TPanel;{OP:22.Abr.08}
    RLV_CAMPO5Lbl: TLabel;{OP:22.Abr.08}
    RLV_CAMPO5: TDBEdit;{OP:22.Abr.08}
    RLV_CAMPO6Pnl: TPanel;{OP:22.Abr.08}
    RLV_CAMPO6Lbl: TLabel;{OP:22.Abr.08}
    RLV_CAMPO6: TDBEdit;{OP:22.Abr.08}
    RLV_CAMPO7Pnl: TPanel;{OP:22.Abr.08}
    RLV_CAMPO7Lbl: TLabel;{OP:22.Abr.08}
    RLV_CAMPO7: TDBEdit;{OP:22.Abr.08}
    RLV_CAMPO8Pnl: TPanel; {OP:22.Abr.08}
    RLV_CAMPO8Lbl: TLabel;{OP:22.Abr.08}
    RLV_CAMPO8: TDBEdit;{OP:22.Abr.08}
    RLV_CAMPO9Pnl: TPanel;{OP:22.Abr.08}
    RLV_CAMPO9Lbl: TLabel;{OP:22.Abr.08}
    RLV_CAMPO9: TDBEdit;{OP:22.Abr.08}
    RLV_CAMPO10Pnl: TPanel;{OP:22.Abr.08}
    RLV_CAMPO10Lbl: TLabel;{OP:22.Abr.08}
    RLV_CAMPO10: TDBEdit;{OP:22.Abr.08}
    RLV_CAMPO11Pnl: TPanel;{OP:22.Abr.08}
    RLV_CAMPO11Lbl: TLabel;{OP:22.Abr.08}
    RLV_CAMPO11: TDBEdit;{OP:22.Abr.08}
    RLV_CAMPO12Pnl: TPanel;{OP:22.Abr.08}
    RLV_CAMPO12Lbl: TLabel;{OP:22.Abr.08}
    RLV_CAMPO12: TDBEdit;{OP:22.Abr.08}
    RLV_CAMPO13Pnl: TPanel;{OP:22.Abr.08}
    RLV_CAMPO13Lbl: TLabel;{OP:22.Abr.08}
    RLV_CAMPO13: TDBEdit;{OP:22.Abr.08}
    RLV_CAMPO14Pnl: TPanel;{OP:22.Abr.08}
    RLV_CAMPO14Lbl: TLabel;{OP:22.Abr.08}
    RLV_CAMPO14: TDBEdit;{OP:22.Abr.08}
    RLV_CAMPO15Pnl: TPanel;{OP:22.Abr.08}
    RLV_CAMPO15Lbl: TLabel;{OP:22.Abr.08}
    RLV_CAMPO15: TDBEdit;{OP:22.Abr.08}
    RLV_CAMPO16Pnl: TPanel;{OP:22.Abr.08}
    RLV_CAMPO16Lbl: TLabel;{OP:22.Abr.08}
    RLV_CAMPO16: TDBEdit;{OP:22.Abr.08}
    RLV_CAMPO17Pnl: TPanel;{OP:22.Abr.08}
    RLV_CAMPO17Lbl: TLabel;{OP:22.Abr.08}
    RLV_CAMPO17: TDBEdit;{OP:22.Abr.08}
    RLV_CAMPO18Pnl: TPanel;{OP:22.Abr.08}
    RLV_CAMPO18Lbl: TLabel;{OP:22.Abr.08}
    RLV_CAMPO18: TDBEdit;{OP:22.Abr.08}
    RLV_CAMPO19Pnl: TPanel;{OP:22.Abr.08}
    RLV_CAMPO19Lbl: TLabel;{OP:22.Abr.08}
    RLV_CAMPO19: TDBEdit;{OP:22.Abr.08}
    RLV_CAMPO20Pnl: TPanel;{OP:22.Abr.08}
    RLV_CAMPO20Lbl: TLabel;{OP:22.Abr.08}
    RLV_CAMPO20: TDBEdit;{OP:22.Abr.08}
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FNumReferencias: Integer;
    FNumReferenciasV: Integer;{OP:22.Abr.08}
    FNumReferenciasMayor: Integer;{OP:22.Abr.08}
    procedure SetCampos;
    procedure SetPanelLabel(oPanel: TPanel; oLabel: TLabel; const iGlobal, iRef, iNumReferencia: Integer);{OP:28.Abr.08}
  protected
    { Protected declarations }
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    procedure Connect; override;
    procedure EscribirCambios; override;
  public
    { Public declarations }
  end;

const
     K_ANCHO_DEFAULT = 260;{OP:22.Abr.08}
     K_ANCHO_DEFAULT_C_TAB = 278;{OP:22.Abr.08}
     K_ANCHO_CAMPO = 24;
     K_CAMPOS_DEFAULT = 4;
var
  EditRefLaboral: TEditRefLaboral;

implementation

uses dSeleccion, dGlobal,
     ZetaCommonTools, ZGlobalTress, ZAccesosTress;

{$R *.DFM}

{ TEditRefLaboral }

procedure TEditRefLaboral.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := nil;
     IndexDerechos := D_SOLICITUD_DATOS;
     HelpContext := 0;
end;

procedure TEditRefLaboral.FormShow(Sender: TObject);
function TabsVisibles: Boolean;
begin
     {OP:28.Abr.08}
     Result := ( FNumReferencias > 0 ) and ( FNumReferenciasV > 0 );
end;

begin
     FNumReferencias := Global.NumCamposRefLab;
     FNumReferenciasV := Global.NumCamposRefVLab;

     {OP:22.Abr.08}
     if( FNumReferencias > FNumReferenciasV ) then
         FNumReferenciasMayor := FNumReferencias
     else
         FNumReferenciasMayor := FNumReferenciasV;

     {OP:22.Abr.08}
     Referencia.TabVisible := TabsVisibles;
     Verificacion.TabVisible := TabsVisibles;

     if FNumReferencias > 0 then
        ReferenciasPg.ActivePage := Referencia
     else if FNumReferenciasV > 0 then
          ReferenciasPg.ActivePage := Verificacion;

     if ( TabsVisibles ) then
        self.Height := K_ANCHO_DEFAULT_C_TAB + iMax( 0, K_ANCHO_CAMPO *
                    ( FNumReferenciasMayor - K_CAMPOS_DEFAULT ) )
     else
        self.Height := K_ANCHO_DEFAULT + iMax( 0, K_ANCHO_CAMPO *
                    ( FNumReferenciasMayor - K_CAMPOS_DEFAULT ) );

     SetCampos;
                    
     inherited;
end;

procedure TEditRefLaboral.Connect;
begin
     with dmSeleccion do
     begin
          Datasource.Dataset := cdsRefLaboral;
          PRETTYNAME.Caption := cdsEditSolicita.FieldByName( 'PRETTYNAME' ).AsString;
     end;
end;

function TEditRefLaboral.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Agregar En Esta Pantalla';
end;

function TEditRefLaboral.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Borrar En Esta Pantalla';
end;

procedure TEditRefLaboral.EscribirCambios;
begin
     with dmSeleccion do
          if ( cdsEditSolicita.State = dsInsert ) then
             cdsRefLaboral.Post
          else
             inherited;
end;

procedure TEditRefLaboral.SetCampos;
begin
     SetPanelLabel( RL_CAMPO1Pnl, RL_CAMPO1Lbl, K_GLOBAL_REF_LABORAL1, 1, FNumReferencias );
     SetPanelLabel( RL_CAMPO2Pnl, RL_CAMPO2Lbl, K_GLOBAL_REF_LABORAL2, 2, FNumReferencias );
     SetPanelLabel( RL_CAMPO3Pnl, RL_CAMPO3Lbl, K_GLOBAL_REF_LABORAL3, 3, FNumReferencias );
     SetPanelLabel( RL_CAMPO4Pnl, RL_CAMPO4Lbl, K_GLOBAL_REF_LABORAL4, 4, FNumReferencias );
     SetPanelLabel( RL_CAMPO5Pnl, RL_CAMPO5Lbl, K_GLOBAL_REF_LABORAL5, 5, FNumReferencias );
     SetPanelLabel( RL_CAMPO6Pnl, RL_CAMPO6Lbl, K_GLOBAL_REF_LABORAL6, 6, FNumReferencias );
     SetPanelLabel( RL_CAMPO7Pnl, RL_CAMPO7Lbl, K_GLOBAL_REF_LABORAL7, 7, FNumReferencias );
     SetPanelLabel( RL_CAMPO8Pnl, RL_CAMPO8Lbl, K_GLOBAL_REF_LABORAL8, 8, FNumReferencias );
     SetPanelLabel( RL_CAMPO9Pnl, RL_CAMPO9Lbl, K_GLOBAL_REF_LABORAL9, 9, FNumReferencias );
     SetPanelLabel( RL_CAMPO10Pnl, RL_CAMPO10Lbl, K_GLOBAL_REF_LABORAL10, 10, FNumReferencias );{OP:22.Abr.08}
     SetPanelLabel( RL_CAMPO11Pnl, RL_CAMPO11Lbl, K_GLOBAL_REF_LABORAL11, 11, FNumReferencias );{OP:22.Abr.08}
     SetPanelLabel( RL_CAMPO12Pnl, RL_CAMPO12Lbl, K_GLOBAL_REF_LABORAL12, 12, FNumReferencias );{OP:22.Abr.08}
     SetPanelLabel( RL_CAMPO13Pnl, RL_CAMPO13Lbl, K_GLOBAL_REF_LABORAL13, 13, FNumReferencias );{OP:22.Abr.08}
     SetPanelLabel( RL_CAMPO14Pnl, RL_CAMPO14Lbl, K_GLOBAL_REF_LABORAL14, 14, FNumReferencias );{OP:22.Abr.08}
     SetPanelLabel( RL_CAMPO15Pnl, RL_CAMPO15Lbl, K_GLOBAL_REF_LABORAL15, 15, FNumReferencias );{OP:22.Abr.08}
     SetPanelLabel( RL_CAMPO16Pnl, RL_CAMPO16Lbl, K_GLOBAL_REF_LABORAL16, 16, FNumReferencias );{OP:22.Abr.08}
     SetPanelLabel( RL_CAMPO17Pnl, RL_CAMPO17Lbl, K_GLOBAL_REF_LABORAL17, 17, FNumReferencias );{OP:22.Abr.08}
     SetPanelLabel( RL_CAMPO18Pnl, RL_CAMPO18Lbl, K_GLOBAL_REF_LABORAL18, 18, FNumReferencias );{OP:22.Abr.08}
     SetPanelLabel( RL_CAMPO19Pnl, RL_CAMPO19Lbl, K_GLOBAL_REF_LABORAL19, 19, FNumReferencias );{OP:22.Abr.08}
     SetPanelLabel( RL_CAMPO20Pnl, RL_CAMPO20Lbl, K_GLOBAL_REF_LABORAL20, 20, FNumReferencias );{OP:22.Abr.08}

     SetPanelLabel( RLV_CAMPO1Pnl, RLV_CAMPO1Lbl, K_GLOBAL_REF_LABORALRV1, 1, FNumReferenciasV );{OP:22.Abr.08}
     SetPanelLabel( RLV_CAMPO2Pnl, RLV_CAMPO2Lbl, K_GLOBAL_REF_LABORALRV2, 2, FNumReferenciasV );{OP:22.Abr.08}
     SetPanelLabel( RLV_CAMPO3Pnl, RLV_CAMPO3Lbl, K_GLOBAL_REF_LABORALRV3, 3, FNumReferenciasV );{OP:22.Abr.08}
     SetPanelLabel( RLV_CAMPO4Pnl, RLV_CAMPO4Lbl, K_GLOBAL_REF_LABORALRV4, 4, FNumReferenciasV );{OP:22.Abr.08}
     SetPanelLabel( RLV_CAMPO5Pnl, RLV_CAMPO5Lbl, K_GLOBAL_REF_LABORALRV5, 5, FNumReferenciasV );{OP:22.Abr.08}
     SetPanelLabel( RLV_CAMPO6Pnl, RLV_CAMPO6Lbl, K_GLOBAL_REF_LABORALRV6, 6, FNumReferenciasV );{OP:22.Abr.08}
     SetPanelLabel( RLV_CAMPO7Pnl, RLV_CAMPO7Lbl, K_GLOBAL_REF_LABORALRV7, 7, FNumReferenciasV );{OP:22.Abr.08}
     SetPanelLabel( RLV_CAMPO8Pnl, RLV_CAMPO8Lbl, K_GLOBAL_REF_LABORALRV8, 8, FNumReferenciasV );{OP:22.Abr.08}
     SetPanelLabel( RLV_CAMPO9Pnl, RLV_CAMPO9Lbl, K_GLOBAL_REF_LABORALRV9, 9, FNumReferenciasV );{OP:22.Abr.08}
     SetPanelLabel( RLV_CAMPO10Pnl, RLV_CAMPO10Lbl, K_GLOBAL_REF_LABORALRV10, 10, FNumReferenciasV );{OP:22.Abr.08}
     SetPanelLabel( RLV_CAMPO11Pnl, RLV_CAMPO11Lbl, K_GLOBAL_REF_LABORALRV11, 11, FNumReferenciasV );{OP:22.Abr.08}
     SetPanelLabel( RLV_CAMPO12Pnl, RLV_CAMPO12Lbl, K_GLOBAL_REF_LABORALRV12, 12, FNumReferenciasV );{OP:22.Abr.08}
     SetPanelLabel( RLV_CAMPO13Pnl, RLV_CAMPO13Lbl, K_GLOBAL_REF_LABORALRV13, 13, FNumReferenciasV );{OP:22.Abr.08}
     SetPanelLabel( RLV_CAMPO14Pnl, RLV_CAMPO14Lbl, K_GLOBAL_REF_LABORALRV14, 14, FNumReferenciasV );{OP:22.Abr.08}
     SetPanelLabel( RLV_CAMPO15Pnl, RLV_CAMPO15Lbl, K_GLOBAL_REF_LABORALRV15, 15, FNumReferenciasV );{OP:22.Abr.08}
     SetPanelLabel( RLV_CAMPO16Pnl, RLV_CAMPO16Lbl, K_GLOBAL_REF_LABORALRV16, 16, FNumReferenciasV );{OP:22.Abr.08}
     SetPanelLabel( RLV_CAMPO17Pnl, RLV_CAMPO17Lbl, K_GLOBAL_REF_LABORALRV17, 17, FNumReferenciasV );{OP:22.Abr.08}
     SetPanelLabel( RLV_CAMPO18Pnl, RLV_CAMPO18Lbl, K_GLOBAL_REF_LABORALRV18, 18, FNumReferenciasV );{OP:22.Abr.08}
     SetPanelLabel( RLV_CAMPO19Pnl, RLV_CAMPO19Lbl, K_GLOBAL_REF_LABORALRV19, 19, FNumReferenciasV );{OP:22.Abr.08}
     SetPanelLabel( RLV_CAMPO20Pnl, RLV_CAMPO20Lbl, K_GLOBAL_REF_LABORALRV20, 20, FNumReferenciasV );{OP:22.Abr.08}
end;

procedure TEditRefLaboral.SetPanelLabel( oPanel: TPanel; oLabel: TLabel;
          const iGlobal, iRef, iNumReferencia: Integer );
begin
     oLabel.Caption := Global.GetGlobalString( iGlobal );
     oPanel.Visible := ( iRef <= iNumReferencia ) and strLleno( oLabel.Caption );
     oLabel.Caption := oLabel.Caption + ':';
     if ( oPanel.Visible ) and ( FirstControl = nil ) then
        FirstControl := oLabel.FocusControl;
end;

end.
