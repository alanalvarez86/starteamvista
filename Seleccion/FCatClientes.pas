unit FCatClientes;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids,
     ZBaseConsulta,
     ZetaDBGrid;

type
  TCatClientes = class(TBaseConsulta)
    GridClientes: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar;override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations}
    procedure DoLookup; override;
  end;

var
  CatClientes: TCatClientes;

implementation

uses DSeleccion,
     ZetaBuscador,
     ZetaCommonClasses;

{$R *.DFM}

procedure TCatClientes.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext:= H00080_Clientes;
end;

procedure TCatClientes.Connect;
begin
     with dmSeleccion do
     begin
          cdsCliente.Conectar;
          DataSource.DataSet:= cdsCliente;
     end;
end;

procedure TCatClientes.Refresh;
begin
     dmSeleccion.cdsCliente.Refrescar;
end;

procedure TCatClientes.Agregar;
begin
     inherited;
     dmSeleccion.cdsCliente.Agregar;
end;

procedure TCatClientes.Borrar;
begin
     inherited;
     dmSeleccion.cdsCliente.Borrar;
end;

procedure TCatClientes.Modificar;
begin
     inherited;
     dmSeleccion.cdsCliente.Modificar;
end;

procedure TCatClientes.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'C�digo', 'Clientes', 'CL_CODIGO', dmSeleccion.cdsCliente );
end;

end.
