unit FRegArchivo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsultaBotones, ImgList, Db, ToolWin, ComCtrls, ExtCtrls;

type
  TRegArchivo = class(TBaseBotones)
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RegArchivo: TRegArchivo;

implementation

uses FTressShell,
     ZetaCommonClasses;

{$R *.DFM}

procedure TRegArchivo.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H00082_Registro;
end;

end.
