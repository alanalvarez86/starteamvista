unit ZAccesosTress;

interface

const

// El C�digo de grupo que no tiene restricciones
     D_GRUPO_SIN_RESTRICCION            = 1;
     K_IMAGENINDEX                      = 0;

{Constantes para Derechos}

{******** Archivo **********}
     D_ARCHIVO                          = 1;
       D_REQUISICION                    = 2;
         D_REQUISICION_DATOS            = 3;
         D_REQUISICION_GASTOS           = 4;
         D_CANDIDATOS                   = 5;
         D_CANDIDATOS_ENTREVISTAS       = 6;
         D_REQUISICION_OFERTA           = 39;{OP:11.Abr.08}
         D_REQUISICION_ANUNCIO          = 40;{OP:11.Abr.08}
         D_REQUISICION_PRESUPUESTO      = 41;{OP:11.Abr.08}
       D_SOLICITUD                      = 7;
         D_SOLICITUD_DATOS              = 8;
         D_SOLICITUD_EXAMENES           = 9;
         D_SOLICITUD_DODUMENTO          = 10;
         D_SOLICITUD_VER_CANDIDATOS     = 11;
       D_REGISTRO                       = 12;
         D_REGISTRO_REQUISICION         = 13;
         D_REGISTRO_SOLICITUD           = 14;
       D_PROCESO                        = 15;
         D_PROCESO_DEPURAR              = 16;
{******** Consultas **********}
     D_CONSULTAS                        = 17;
       D_CONS_REPORTES                  = 18;
       D_CONS_SQL                       = 19;
       D_CONS_BITACORA                  = 20;
{******** Cat�logos **********}
     D_CATALOGOS                        = 21;
       D_CAT_PUESTOS                    = 22;
       D_CAT_AREAS_INTERES              = 23;
       D_CAT_CLIENTES                   = 24;
       D_CAT_TIPO_GASTOS                = 25;
       D_CAT_GRALES_CONDICIONES         = 26;
       D_CAT_ADICION_TABLA1             = 27;
       D_CAT_ADICION_TABLA2             = 28;
       D_CAT_ADICION_TABLA3             = 29;
       D_CAT_ADICION_TABLA4             = 30;
       D_CAT_ADICION_TABLA5             = 31;
{******** Sistema **********}
     D_SISTEMA                          = 32;
       D_SIST_BASE_DATOS                = 42;
       D_SIST_DATOS_EMPRESAS            = 33;
       D_SIST_DATOS_GRUPOS              = 34;
       D_SIST_DATOS_USUARIOS            = 35;
       D_SIST_DATOS_IMPRESORAS          = 36;
       D_CAT_CONFI_GLOBALES             = 37;
       D_CAT_CONFI_DICCIONARIO          = 38;


// Nota: Si agregas un derecho, aumenta K_MAX_DERECHOS
     K_MAX_DERECHOS                     = 42;

implementation

end.
