unit DBaseSeleccionDiccionario;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBaseDiccionario, Db, DBClient, ZetaClientDataSet, ZetaCommonLists;

type
  TdmBaseSeleccionDiccionario = class(TdmBaseDiccionario)
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    function ChecaDerechoConfidencial: Boolean;override;
    function ValidaModulo( const eClasifi: eClasifiReporte ): Boolean;override;

  public
    { Public declarations }
    procedure GetListaClasifiModulo(oLista: TStrings; const lMigracion: Boolean);override;
    function GetDerechosClasifi(eClasifi : eClasifiReporte): Integer;override;

  end;

var
  dmBaseSeleccionDiccionario: TdmBaseSeleccionDiccionario;

implementation
uses
     FAutoClasses,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaCommonClasses;

{$R *.DFM}

procedure TdmBaseSeleccionDiccionario.DataModuleCreate(Sender: TObject);
begin
     inherited;
     ClasifiDefault := crSeleccion;
end;


procedure TdmBaseSeleccionDiccionario.GetListaClasifiModulo(oLista: TStrings;  const lMigracion: Boolean);
begin
     inherited GetListaClasifiModulo( oLista, lMigracion );
     AgregaClasifi( oLista, crSeleccion );
end;

function TdmBaseSeleccionDiccionario.ChecaDerechoConfidencial: Boolean;
begin
     Result := ZAccesosMgr.CheckDerecho( D_CONS_REPORTES, K_DERECHO_SIST_KARDEX);
end;

function TdmBaseSeleccionDiccionario.ValidaModulo(const eClasifi: eClasifiReporte): Boolean;
begin
     with Autorizacion do
          case eClasifi of
               crSeleccion: Result := OKModulo( OkSeleccion )
               else
                   Result := inherited ValidaModulo( eClasifi );
     end;
end;

function TdmBaseSeleccionDiccionario.GetDerechosClasifi( eClasifi: eClasifiReporte): Integer;
begin
     Result := D_CONS_REPORTES;

end;


end.
