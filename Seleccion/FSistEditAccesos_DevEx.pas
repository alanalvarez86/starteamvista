unit FSistEditAccesos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEditAccesos, ImgList, Buttons, ZetaDBTextBox, StdCtrls, ComCtrls,
  ExtCtrls;

type
  TSistEditAccesos_DevEx = class(TZetaEditAccesos)
    procedure FormCreate(Sender: TObject);
  private
         function EsDerechoConsulta( const sTexto: String ): Boolean;{OP:21.Abr.08}
    { Private declarations }
  protected
    function GetTipoDerecho(Nodo: TTreeNode): eDerecho; override;
    function TextoEspecial(const sText: String): Boolean; override;
    procedure ArbolDefinir; override;
  public
    { Public declarations }
  end;

var
  SistEditAccesos_DevEx: TSistEditAccesos_DevEx;

implementation
uses ZAccesosTress;
const  D_TEXT_DATOS_CONFI = 'Ver Datos Confidenciales';
       D_TEXT_CANCELAR_PROCESOS = 'Cancelar Procesos';
       D_TEXT_CANDIDATURAS = 'Ver Candidaturas';
       D_TEXT_OFERTA       = 'Oferta';{OP:11.Abr.08}
       D_TEXT_ANUNCIO      = 'Anuncio';{OP:11.Abr.08}
       D_TEXT_PRESUPUESTO  = 'Presupuesto';{OP:11.Abr.08}
              
       //************* Derechos de Acceso Administrador Especial *********************//
       D_ASIGNA_DERECHOS_OTROS_GRUPOS = 'Asignar Derechos a Otros Grupos';
       D_ASIGNA_DERECHOS_GRUPO_PROPIO = 'Asignar Derechos a SU Grupo';
       D_MODIFICAR_USUARIO_PROPIO = 'Modificar SU Usuario';


{$R *.DFM}

procedure TSistEditAccesos_DevEx.ArbolDefinir;
begin
  inherited;

{ *** Archivo *** }

AddElemento( 0, D_ARCHIVO, 'Archivo' );
   AddElemento( 1, D_REQUISICION, 'Requisiciones' );
      AddElemento( 2, D_REQUISICION_DATOS, 'Datos' );
         AddDerechoConsulta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_REQUISICION_GASTOS, 'Gastos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_CANDIDATOS, 'Candidatos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_CANDIDATOS_ENTREVISTAS, 'Entrevistas' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_REQUISICION_OFERTA, 'Oferta' );{OP:24.Abr.08}
      AddElemento( 2, D_REQUISICION_ANUNCIO, 'Anuncio' );{OP:24.Abr.08}
      AddElemento( 2, D_REQUISICION_PRESUPUESTO, 'Presupuesto' );{OP:24.Abr.08}
   AddElemento( 1, D_SOLICITUD, 'Solicitudes' );
      AddElemento( 2, D_SOLICITUD_DATOS, 'Datos' );
         AddDerechoConsulta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_SOLICITUD_EXAMENES, 'Examenes' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_SOLICITUD_DODUMENTO, 'Documentos' );
         AddDerechoConsulta;
         AddDerechoAlta;
         AddDerechoBaja;
         AddDerechoEdicion;
         AddDerechoImpresion;
      AddElemento( 2, D_SOLICITUD_VER_CANDIDATOS, 'Ver Candidaturas' );
   AddElemento( 1, D_REGISTRO, 'Registro' );
      AddElemento( 2, D_REGISTRO_REQUISICION, 'Requisici�n' );
      AddElemento( 2, D_REGISTRO_SOLICITUD, 'Solicitud' );
   AddElemento( 1, D_PROCESO, 'Procesos' );
      AddElemento( 2, D_PROCESO_DEPURAR, 'Depurar Informaci�n' );

{ *** Consultas *** }

AddElemento( 0, D_CONSULTAS, 'Consultas' );
   AddElemento( 1, D_CONS_REPORTES, 'Reportes' );
      AddDerechoConsulta;
      AddDerechoAlta;
      AddDerechoBaja;
      AddDerechoEdicion;
      AddDerechoImpresion;
      AddDerecho( 'Ver Datos Confidenciales' );
   AddElemento( 1, D_CONS_SQL, 'SQL' );
      AddDerechoConsulta;
      AddDerechoImpresion;
   AddElemento( 1, D_CONS_BITACORA, 'Bit�cora / Procesos' );
      AddDerechoConsulta;
      AddDerecho( 'Cancelar Procesos' );
      AddDerechoImpresion;

{ *** Cat�logos *** }

AddElemento( 0, D_CATALOGOS, 'Cat�logos' );
   AddElemento( 1, D_CAT_PUESTOS, 'Puestos' );
      AddDerechoConsulta;
      AddDerechoAlta;
      AddDerechoBaja;
      AddDerechoEdicion;
      AddDerechoImpresion;
   AddElemento( 1, D_CAT_AREAS_INTERES, 'Areas de Inter�s' );
      AddDerechoConsulta;
      AddDerechoAlta;
      AddDerechoBaja;
      AddDerechoEdicion;
      AddDerechoImpresion;
   AddElemento( 1, D_CAT_CLIENTES, 'Clientes' );
      AddDerechoConsulta;
      AddDerechoAlta;
      AddDerechoBaja;
      AddDerechoEdicion;
      AddDerechoImpresion;
   AddElemento( 1, D_CAT_TIPO_GASTOS, 'Tipos de Gasto' );
      AddDerechoConsulta;
      AddDerechoAlta;
      AddDerechoBaja;
      AddDerechoEdicion;
      AddDerechoImpresion;
   AddElemento( 1, D_CAT_GRALES_CONDICIONES, 'Condiciones' );
      AddDerechoConsulta;
      AddDerechoAlta;
      AddDerechoBaja;
      AddDerechoEdicion;
      AddDerechoImpresion;
   AddElemento( 1, D_CAT_ADICION_TABLA1, 'Adicional 1' );
      AddDerechoConsulta;
      AddDerechoAlta;
      AddDerechoBaja;
      AddDerechoEdicion;
      AddDerechoImpresion;
   AddElemento( 1, D_CAT_ADICION_TABLA2, 'Adicional 2' );
      AddDerechoConsulta;
      AddDerechoAlta;
      AddDerechoBaja;
      AddDerechoEdicion;
      AddDerechoImpresion;
   AddElemento( 1, D_CAT_ADICION_TABLA3, 'Adicional 3' );
      AddDerechoConsulta;
      AddDerechoAlta;
      AddDerechoBaja;
      AddDerechoEdicion;
      AddDerechoImpresion;
   AddElemento( 1, D_CAT_ADICION_TABLA4, 'Adicional 4' );
      AddDerechoConsulta;
      AddDerechoAlta;
      AddDerechoBaja;
      AddDerechoEdicion;
      AddDerechoImpresion;
   AddElemento( 1, D_CAT_ADICION_TABLA5, 'Adicional 5' );
      AddDerechoConsulta;
      AddDerechoAlta;
      AddDerechoBaja;
      AddDerechoEdicion;
      AddDerechoImpresion;

{ *** Sistema *** }

AddElemento( 0, D_SISTEMA, 'Sistema' );
   AddElemento( 1, D_SIST_DATOS_EMPRESAS, 'Empresas' );
      AddDerechoConsulta;
      AddDerechoAlta;
      AddDerechoBaja;
      AddDerechoEdicion;
      AddDerechoImpresion;
   AddElemento( 1, D_SIST_DATOS_GRUPOS, 'Grupos de Usuarios' );
      AddDerechoConsulta;
      AddDerechoAlta;
      AddDerechoBaja;
      AddDerechoEdicion;
      AddDerechoImpresion;
      {$ifdef ESPECIAL}
      AddDerecho( D_ASIGNA_DERECHOS_OTROS_GRUPOS );
      AddDerecho( D_ASIGNA_DERECHOS_GRUPO_PROPIO );
      {$endif}
   AddElemento( 1, D_SIST_DATOS_USUARIOS, 'Usuarios' );
      AddDerechoConsulta;
      AddDerechoAlta;
      AddDerechoBaja;
      AddDerechoEdicion;
      AddDerechoImpresion;
      {$ifdef ESPECIAL}
      AddDerecho( D_MODIFICAR_USUARIO_PROPIO );
      {$endif}
   AddElemento( 1, D_SIST_DATOS_IMPRESORAS, 'Impresoras' );
      AddDerechoConsulta;
      AddDerechoAlta;
      AddDerechoBaja;
      AddDerechoEdicion;
      AddDerechoImpresion;
   AddElemento( 1, D_CAT_CONFI_GLOBALES, 'Globales de Empresa' );
      AddDerechoConsulta;
      AddDerechoEdicion;
      AddDerechoImpresion;
   AddElemento( 1, D_CAT_CONFI_DICCIONARIO, 'Diccionario de Datos' );
      AddDerechoConsulta;
      AddDerechoAlta;
      AddDerechoBaja;
      AddDerechoEdicion;
      AddDerechoImpresion;

end;

function TSistEditAccesos_DevEx.GetTipoDerecho(Nodo: TTreeNode): eDerecho;
begin
     with Nodo do
     begin
          if ( EsDerechoConsulta( Text ) ) then {OP:21.Abr.08} {Text = D_TEXT_CANDIDATURAS ) then}
               Result := edConsulta
          else
              if ( Text = D_TEXT_CANCELAR_PROCESOS ) then
              Result := edBaja
              else
                  if ( Text = D_TEXT_DATOS_CONFI ) or
                     ( Text = D_ASIGNA_DERECHOS_OTROS_GRUPOS) or
                     ( Text = D_MODIFICAR_USUARIO_PROPIO) then
                      Result := edBorraSistKardex
                  else
                      if ( Text = D_ASIGNA_DERECHOS_GRUPO_PROPIO) then
                         Result := edBanca
                      else
                          Result := inherited GetTipoDerecho( Nodo );
     end;
end;


function TSistEditAccesos_DevEx.TextoEspecial(const sText: String): Boolean;
begin
     Result := ( sText = 'Registro' ) or
               ( sText = 'Procesos' ) or
               ( sText = D_TEXT_CANDIDATURAS ) or
               ( sText = D_TEXT_OFERTA ) or
               ( sText = D_TEXT_ANUNCIO ) or
               ( sText = D_TEXT_PRESUPUESTO ) or               
               inherited TextoEspecial( sText );
end;

{OP:21.Abr.08}
function TSistEditAccesos_DevEx.EsDerechoConsulta(const sTexto: String): Boolean;
begin
     Result := ( sTexto = D_TEXT_CANDIDATURAS ) or
               ( sTexto = D_TEXT_OFERTA ) or
               ( sTexto = D_TEXT_ANUNCIO ) or
               ( sTexto = D_TEXT_PRESUPUESTO );
end;

procedure TSistEditAccesos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     self.ArbolConstruir;
     PageControl.ActivePage := tsTress;
end;

end.
