inherited BuscaRequisicion: TBuscaRequisicion
  Left = 248
  Top = 208
  Caption = 'B�squeda de Requisiciones'
  ClientHeight = 299
  ClientWidth = 640
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 263
    Width = 640
    inherited OK: TBitBtn
      Left = 472
    end
    inherited Cancelar: TBitBtn
      Left = 557
    end
  end
  object PanelFiltro: TPanel
    Left = 0
    Top = 0
    Width = 640
    Height = 81
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object AreaLBL: TLabel
      Left = 189
      Top = 11
      Width = 35
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cliente:'
      FocusControl = Cliente
    end
    object CriterioLbl: TLabel
      Left = 188
      Top = 33
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'Puesto:'
      FocusControl = Puesto
    end
    object Label3: TLabel
      Left = 23
      Top = 11
      Width = 33
      Height = 13
      Caption = 'Status:'
    end
    object Label4: TLabel
      Left = 12
      Top = 33
      Width = 44
      Height = 13
      Caption = 'Prioridad:'
    end
    object EdadMinimaLBL: TLabel
      Left = 4
      Top = 55
      Width = 52
      Height = 13
      Alignment = taRightJustify
      Caption = 'Folio entre:'
      FocusControl = FolioInicial
    end
    object EdadMaximaLBL: TLabel
      Left = 116
      Top = 55
      Width = 7
      Height = 13
      Caption = '&Y'
      FocusControl = FolioFinal
    end
    object Refrescar: TBitBtn
      Left = 536
      Top = 7
      Width = 73
      Height = 43
      Hint = 'Buscar Solicitudes De Acuerdo A Los Criterios Especificados'
      Caption = '&Buscar'
      TabOrder = 6
      OnClick = RefrescarClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D808DDDDDDDD
        DDDD83308DDDDDDDDDDD873308DDDDDDDDDDD373308DDDDDDDDDDD3733088888
        8DDDDDD37330000088DDDDDD37338777808DDDDDD038F7F77808DDDDD08FCCCC
        C788DDDCD0F7C7F7C770DDDCC07FCCCCCF70DDDCD0F7CFFCF770DDDDC88FCCCC
        7F88DDDDDD08F7F7F80DDDDDDDD08F7F80DDDDDDDDDD80008DDD}
      Layout = blGlyphTop
    end
    object Cliente: TZetaKeyLookup
      Left = 226
      Top = 7
      Width = 300
      Height = 21
      LookupDataset = dmSeleccion.cdsCliente
      TabOrder = 4
      TabStop = True
      WidthLlave = 60
    end
    object Puesto: TZetaKeyLookup
      Left = 226
      Top = 29
      Width = 300
      Height = 21
      LookupDataset = dmSeleccion.cdsPuestos
      TabOrder = 5
      TabStop = True
      WidthLlave = 60
    end
    object cbStatus: TComboBox
      Left = 59
      Top = 7
      Width = 120
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
    end
    object cbPrioridad: TComboBox
      Left = 59
      Top = 29
      Width = 120
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
    end
    object FolioInicial: TZetaNumero
      Left = 59
      Top = 51
      Width = 50
      Height = 21
      Mascara = mnDias
      TabOrder = 2
      Text = '0'
    end
    object FolioFinal: TZetaNumero
      Left = 129
      Top = 51
      Width = 50
      Height = 21
      Mascara = mnDias
      TabOrder = 3
      Text = '0'
    end
  end
  object ZetaDBGrid: TZetaDBGrid
    Left = 0
    Top = 81
    Width = 640
    Height = 182
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'RQ_PRIORID'
        Title.Caption = 'Prioridad'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RQ_FEC_INI'
        Title.Caption = 'Inicio'
        Width = 78
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RQ_FOLIO'
        Title.Caption = 'Folio'
        Width = 31
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CL_DESCRIP'
        Title.Caption = 'Cliente'
        Width = 139
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PU_DESCRIP'
        Title.Caption = 'Puesto'
        Width = 133
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RQ_VACANTE'
        Title.Caption = 'Vac'
        Width = 25
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RQ_CANDIDA'
        Title.Caption = 'Can'
        Width = 23
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RQ_SELECCI'
        Title.Caption = 'Sel'
        Width = 23
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RQ_CONTRAT'
        Title.Caption = 'Con'
        Width = 26
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RQ_STATUS'
        Title.Caption = 'Status'
        Visible = True
      end>
  end
  object DataSource: TDataSource
    Left = 560
    Top = 64
  end
end
