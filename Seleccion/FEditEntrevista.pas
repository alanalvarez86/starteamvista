unit FEditEntrevista;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask, ZetaFecha,
  ZetaDBTextBox, ZetaKeyCombo;

type
  TEditEntrevista = class(TBaseEdicion)
    Panel1: TPanel;
    Panel2: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label1: TLabel;
    eRequisicion: TZetaTextBox;
    eNombreCandidato: TZetaTextBox;
    ePerfil: TZetaTextBox;
    Label3: TLabel;
    Label2: TLabel;
    ER_FECHA: TZetaDBFecha;
    ER_NOMBRE: TDBEdit;
    ER_RESUMEN: TDBEdit;
    ER_DETALLE: TDBMemo;
    ER_RESULT: TZetaDBKeyCombo;
    Label9: TLabel;
    ZetaDBTextBox1: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure SetTitulos;
  protected
  public
    { Public declarations }
    procedure Connect;override;
    procedure Agregar;override;
  end;

var
  EditEntrevista: TEditEntrevista;

implementation

uses DSeleccion,
     ZetaCommonTools,
     ZAccesosTress,
     ZetaCommonClasses,
     ZetaCommonLists;

{$R *.DFM}

procedure TEditEntrevista.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := ER_FECHA;
     SetTitulos;
     IndexDerechos := D_CANDIDATOS_ENTREVISTAS;
     HelpContext := H00072_Entrevista;
end;

procedure TEditEntrevista.Connect;
begin
     with dmSeleccion do
     begin
          cdsEntrevista.Conectar;
          DataSource.DataSet := cdsEntrevista;
          SetTitulos;
     end;
end;

procedure TEditEntrevista.SetTitulos;
 function GetSexo( const sSexo: string ): string ;
 begin
      if sSexo = 'F' then
         Result := 'Femenino'
      else
         Result := 'Masculino'
 end;
begin
     with dmSeleccion.cdsEditRequiere do
     begin
          eRequisicion.Caption := Format( '#%d: %d Vacante%s de %s',
                                          [ FieldByName('RQ_FOLIO').AsInteger,
                                            FieldByName('RQ_VACANTE').AsInteger,
                                            '(s)',
                                            FieldByName('PU_DESCRIP').AsString] );
     end;
     with dmSeleccion.cdsReqCandidatos do
     begin
          eNombreCandidato.Caption := Format( '#%d: %s',
                                              [ FieldByName('SO_FOLIO').AsInteger,
                                                FieldByName('PRETTYNAME').AsString] );
          ePerfil.Caption := Format( '%s, %d a�os, %s, %s Ingl�s',
                                     [ GetSexo(FieldByName('SO_SEXO').AsString),
                                       zYearsBetween(FieldByName('SO_FEC_NAC').AsDateTime, Date),
                                       ObtieneElemento(lfEstudios,FieldByName('SO_ESTUDIO').AsInteger),
                                       FieldByName('SO_INGLES').AsString + '%'] );
     end;
end;

procedure TEditEntrevista.Agregar;
begin
     dmSeleccion.GetLastFolioEntrevista;
     inherited;
end;

end.
