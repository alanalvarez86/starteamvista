program Seleccion;

uses
  Forms,
  ZetaClientTools,
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  ZBaseArbolShell in '..\Tools\ZBaseArbolShell.pas' {BaseArbolShell},
  FTressShell in 'FTressShell.pas' {TressShell},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DBaseDiccionario in '..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DBaseReportes in '..\DataModules\DBaseReportes.pas' {dmBaseReportes: TDataModule},
  DConsultas in '..\DataModules\DConsultas.pas' {dmConsultas: TDataModule},
  ZBaseConsulta in '..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZBaseEdicion in '..\Tools\ZBaseEdicion.pas' {BaseEdicion},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  ZetaSplash in '..\Tools\ZetaSplash.pas' {SplashScreen},
  DBaseGlobal in '..\DataModules\DBaseGlobal.pas',
  DEntidades in '..\Evaluador\DEntidades.pas' {dmEntidades: TDataModule},
  ZBaseConsultaBotones in '..\Tools\ZBaseConsultaBotones.pas' {BaseBotones},
  ZBaseGlobal in '..\Tools\ZBaseGlobal.pas' {BaseGlobal},
  DBaseSistema in '..\DataModules\DBaseSistema.pas' {dmBaseSistema: TDataModule},
  DSistema in 'DSistema.pas' {dmSistema: TDataModule},
  DSeleccion in 'DSeleccion.pas' {dmSeleccion: TDataModule},
  DMailMerge in '..\DataModules\dmailmerge.pas' {dmMailMerge: TDataModule},
  DExportEngine in '..\DataModules\DExportEngine.pas' {dmExportEngine: TDataModule};

{$R *.RES}
{$R WindowsXP.res}

{procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;}

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  {SplashScreen := TSplashScreen.Create( Application );
  with SplashScreen do
  begin
       Show;
       Update;
  end;}
  Application.Title := 'Selecci�n de Personal';
  Application.HelpFile := 'Seleccion.chm';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login( False ) then
       begin
            //CierraSplash;
            RevisaParams;
            if ( not OnlySolicita ) and ( not OnlyRequiere ) then
               Show;
            Update;
            BeforeRun;
            if OnlySolicita then
               CicloRegistroSolicita
            else if OnlyRequiere then
               CicloRegistroRequiere
            else
               Application.Run;
       end
       else
       begin
            //CierraSplash;
            Free;
       end;
  end;
end.
