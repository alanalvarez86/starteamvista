<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template match="/">

<xsl:element name="link">
		<xsl:attribute name="rel">stylesheet</xsl:attribute>
		<xsl:attribute name="href">seleccion.css</xsl:attribute>
		<xsl:attribute name="type">text/css</xsl:attribute>
</xsl:element>

  <SCRIPT LANGUAGE="javascript" DEFER="true">
     <xsl:comment>
      <![CDATA[ 
		function MuestraSolicitud(){
		document.frmPuestos.action="Seleccion1.asp"
		document.frmPuestos.submit();
	}   	 
      ]]> 
    </xsl:comment>
    </SCRIPT>
	<center>
		<table cellSpacing="0" cellPadding="0" width="95%" bgColor="#004080" border="1">
			<tr>
				<td>
					<font face="book antiqua" size="48" color="#FF8000">
						<b>S</b>
					</font>	
					<font face="book antiqua" size="32" color="#FFFFFF">
						<b>elecci<xsl:text>&#243;</xsl:text>n de </b>
					</font>
					<font face="book antiqua" size="48" color="#FF8000">
						<b>P</b>
					</font>
					<font face="book antiqua" size="32" color="#FFFFFF">
						<b>ersonal</b>
					</font>	
				</td>					
			</tr>			
		</table>
		<xsl:element name="br"/>
		<table bgcolor="#FFCFB9" width="80%">
			<tr>
				<td width="57%">
					<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">
						<b>Vacantes:</b><xsl:value-of select="//REQUISICION/@RQ_VACANTE"/>
					</font>
				</td>	
				<td>
					<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">
						<b>Fecha de Requisicion:</b>
						<xsl:value-of select="//REQUISICION/@RQ_FEC_INI"/>	
					</font>
				</td>				
			</tr>
			<tr>
				<table width="80%" bgcolor="#FFCFB9">
					<tr>
						<td>
							<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">
								<b>Puesto:</b></font>							
							<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="1">	
								<xsl:value-of select="//REQUISICION/@PU_DESCRIP"/>	
							</font>	
							<xsl:element name="br"/>
							<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">
								<b>Anuncio:</b></font>							
							<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="1">	
								<xsl:value-of select="//REQUISICION/@RQ_ANUNCIO"/>	
							</font>
							<xsl:element name="br"/>
							<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">
								<b>Area:</b></font>							
							<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="1">	
								<xsl:value-of select="//REQUISICION/@RQ_AREA"/>
							</font>
							<xsl:element name="br"/>
							<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">
								<b>Actividades:</b></font>							
							<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="1">	
								<xsl:value-of select="//REQUISICION/@PU_ACTIVID"/>	
							</font>
						</td>
					</tr>
					<tr>
						<!--***************REQUISITOS*************-->
						<table  width="80%" bgcolor="#FFCFB9">
						<tr>
							<td>
								<xsl:element name="hr"/>
								<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="3">
									<i><b>Requisitos</b></i>
								</font>
								<xsl:element name="hr"/>
							</td>	
						</tr>	
						<tr>
							<td bgcolor="#FFCFB9">	
								<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">
									<b>Habilidades:</b></font>							
								<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="1">	
									<xsl:value-of select="//REQUISICION/@PU_HABILID"/>	
								</font>
							</td>
						</tr>
						</table>
					</tr>
					<tr>
						<td>
							<table width ="80%" bgcolor="#FFCFB9" >
							<tr>
								<td width="20%">
									<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">
										<b>Sexo:</b></font>							
									<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="1">	
										<xsl:value-of select="//REQUISICION/@RQ_SEXO"/>
									</font>																	
								</td>
								<td width="30%">	
									<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">
										<b>Edad Minima:</b></font>							
									<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="1">	
										<xsl:value-of select="//REQUISICION/@RQ_EDADMIN"/><xsl:text> a&#241;os</xsl:text>
									</font>
								</td>
								<td width="30%">	
									<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">
										<b>Edad Maxima:</b></font>							
									<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="1">	
										<xsl:value-of select="//REQUISICION/@RQ_EDADMAX"/><xsl:text> a&#241;os</xsl:text>
									</font>
								</td>
								<td>
									<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">
										<b>Ingles:</b></font>							
									<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="1">	
										<xsl:value-of select="//REQUISICION/@RQ_INGLES"/><xsl:text>%</xsl:text>
									</font>
								</td>
							</tr>		
							</table>
							<!--********************* FIN REQUISITOS *******************-->
						</td>							
					</tr>
					<tr>
						<table  width="80%" bgcolor="#FFCFB9">
							<tr>
								<td>
									<xsl:element name="hr"/>
									<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="3">
									<i><b>Propuesta</b></i></font>
									<xsl:element name="hr"/>
									<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">
											<b>Cliente:</b></font>							
									<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="1">	
										<xsl:value-of select="//REQUISICION/@CL_NOMBRE"/>
									</font>
									<xsl:element name="br"/>
									<xsl:element name="br"/>
									<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">
										<b>Oferta:</b></font>							
									<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="1">	
										<xsl:value-of select="//REQUISICION/@RQ_OFERTA"/>
									</font>									
								</td>																																																										 						
							</tr>
							<tr>
								<td>
									<table bgcolor="#FFCFB9">
										<tr>
											<td width="79%">												
												<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">
													<b>Sueldo:</b></font>							
												<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="1">	
													<xsl:value-of select="//REQUISICION/@RQ_SUELDO"/>
												</font>
											</td>
											<td>
												<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">
													<b>Bono:</b></font>							
												<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="1">	
													<xsl:value-of select="//REQUISICION/RQ_BONO"/>
												</font>
											</td>
										</tr>
										<tr>	
											<td>
												<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">
													<b>Vales:</b></font>							
												<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="1">	
													<xsl:value-of select="//REQUISICION/RQ_VALES"/>
												</font>
											</td>
											<td>
												<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">
													<b>Turno:</b></font>							
												<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="1">	
													<xsl:value-of select="//REQUISICION/RQ_TURNO"/>
												</font>
											</td>
										</tr>
										<tr>
											<td>
												<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">
													<b>Vacaciones:</b></font>							
												<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="1">	
													<xsl:value-of select="//REQUISICION/RQ_VACACI"/>
												</font>
											</td>
											<td>
												<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">
													<b>Aguinaldo:</b></font>							
												<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="1">	
													<xsl:value-of select="//REQUISICION/RQ_AGUINAL"/>
												</font>
											</td>
										</tr>
									</table>												
								</td>
							</tr>
							<tr>
								<td>									
									<p align="right">	
										<a href="Seleccion1.asp">[Enviar Solicitud]</a>
									</p>	
								</td>	
							</tr>
						</table>						
					</tr>
				 </table>
			</tr>
	</table>
	</center>
</xsl:template>
</xsl:stylesheet>