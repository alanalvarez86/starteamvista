<%@ Language=VBScript %>
<%
	option explicit
	Response.Expires=0
	Response.Buffer=false
	
	'Declaramos Variables para Iniciar el COmponente y recibir el XML generado
	dim ComponenteXML	
	dim strResultado
	dim xmlSalida
	dim docXSL
	dim dFechaActual
	'Fecha Actual
	dFechaActual = Now()
	
	'Declaramos Variables para Iniciar el COmponente y recibir el XML generado
	set ComponenteXML = Server.CreateObject("SeleccionWeb.dmServerSeleccionWeb")

	'strResultado= ComponenteXML.GetPuestosDisponibles(xmlEmpresa.xml)
	strResultado= ComponenteXML.GetCFGSolicitud(Application("Empresa"))
	
	'Response.Write(strResultado)

	'Para Transformar el XML con el XSL tenemos que pasarl el string recibido  a documento XML
	set xmlSalida =  server.CreateObject("Msxml2.DOMDocument.4.0")
	xmlSalida.loadXML(strResultado)
	
	'Load the XSL para mostrar en pantalla los Combos
	set docXSL = Server.CreateObject("Msxml2.DOMDocument.4.0")
	docXSL.async = false
	docXSL.load(Server.MapPath("Solicitud.xsl"))	
	
	Response.Write(xmlSalida.transformNode(docXSL))
	
%>
