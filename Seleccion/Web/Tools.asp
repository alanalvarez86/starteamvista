<%
	Function GetXML( Parametros )
		Response.Expires=0
		Response.Buffer=false

		dim xmlcadena	
		dim ComponenteXML	
		dim strResultado
		dim xmlSalida
		dim docXSL

		'Para Transformar el XML con el XSL tenemos que pasarl el string recibido  a documento XML
		set xmlSalida =  server.CreateObject("Msxml2.DOMDocument.4.0")
		xmlSalida.loadXML(Application("Empresa"))
	
		'Response.Write(xmlSalida.xml)
		
		'Load the XSL para mostrar en pantalla
		set docXSL = Server.CreateObject("Msxml2.DOMDocument.4.0")
		docXSL.async = false
		docXSL.load(Server.MapPath("empgen.xsl"))	
	
		xmlcadena=""
		xmlcadena = "<?xml version='1.0' encoding='ISO-8859-1'?><RAIZ>" + xmlSalida.transformNode(docXSL) 
		if Parametros = "" then
			xmlcadena = xmlcadena + "</RAIZ>"
		else	
			xmlcadena = xmlcadena + "<PARAMETROS>"+ Parametros +"</PARAMETROS></RAIZ>"
		end if	

		GetXML = xmlCadena
	End Function
	
	Function GetAtributo( sCampo, sValue )
	    GetAtributo = sCampo + + "=" + chr(34) + sValue +  chr(34) + " "
	End Function
%>
