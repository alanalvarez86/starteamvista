<%@ Language=VBScript %>
<%option explicit%>
<!-- #include file="Tools.asp" -->
<%
	Response.Expires=0
	Response.Buffer=false


	dim formElement
	'dim cadena2
	dim xmlcadena, xmlAntecedentes, xmlOtrosAtrib
	dim sCampo, sValue
	dim icontador,  valor1, valor2
	
	'Declaramos Variables para Iniciar el Componente y recibir el XML generado
	dim ComponenteXML	
	dim strResultado, strFolio
	dim xmlSalida
	
	'cadena2=""
	xmlOtrosAtrib= GetAtributo( "PK", "" ) + GetAtributo( "Action", "ADD" )
	xmlAntecedentes= ""
	xmlcadena = "<SOLICITUD "

	for icontador = 1 to Request.Form.Count
	    sCampo = Request.Form.Key(icontador)
	    sValue = Request.Form(icontador)
	    
		'cadena2 = cadena2 + Request.Form.Key(icontador) +"=" + Request.Form(icontador)
		
		if left(sCampo,2)="RL" then     'Antecedentes Laborales
			if xmlAntecedentes="" then
				xmlAntecedentes = xmlAntecedentes + "<ROW "
				valor1 = 1
				valor2 = 2
			end if
			if  cint(valor1) <> cint(Right(sCampo,1)) and cint(valor1) <> cint(valor2) then
				valor1 = cint(Right(sCampo,1))
				valor2 = valor2 + 1
				xmlAntecedentes = xmlAntecedentes + xmlOtrosAtrib + "></ROW><ROW "
			end if
			xmlAntecedentes = xmlAntecedentes + GetAtributo( left(sCampo,len(sCampo)-2), sValue )
		elseif sCampo ="SO_AREAS" then
			'no hacemos nada para que no lo ponga en la cadena
		elseif sCampo ="SO_AREAS_XML" then
			xmlcadena = xmlcadena + sValue
		elseif sCampo ="submit" then
			'no hacemos nada para que no lo ponga en la cadena
		else
			xmlcadena = xmlcadena + GetAtributo( sCampo, sValue )
		end if
	next 
	xmlcadena = xmlcadena + xmlOtrosAtrib + "></SOLICITUD>"      ' Cierra el Tag de Solicitud
	if xmlAntecedentes <> "" then
   		xmlcadena = xmlcadena + "<ANTECEDENTES><ROWS>" + xmlAntecedentes + xmlOtrosAtrib + "></ROW></ROWS></ANTECEDENTES>"
   	end if

   strFolio = ""
	set ComponenteXML = Server.CreateObject("SeleccionWeb.dmServerSeleccionWeb")
	strResultado = ComponenteXML.AgregaSolicitud(GetXML(xmlcadena))

	set xmlSalida =  server.CreateObject("Msxml2.DOMDocument.4.0")
	xmlSalida.loadXML(strResultado)	
	strFolio = xmlSalida.documentelement.childNodes(0).text

	'Response.Write(GetXML(xmlCadena))

%>

<HTML>
<HEAD>
<META NAME="GENERATOR" Content="Microsoft FrontPage 4.0">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<BODY>
<center>
<table cellSpacing="0" cellPadding="0" width="95%" bgColor="#004080" border="1">
	<tr>
		<td>
			<font face="book antiqua" size="48" color="#FF8000">
				<b>S</b>
			</font>	
			<font face="book antiqua" size="32" color="#FFFFFF">
				<b>elecci<xsl:text>&#243;</xsl:text>n de </b>
			</font>
				<font face="book antiqua" size="48" color="#FF8000">
				<b>P</b>
			</font>
			<font face="book antiqua" size="32" color="#FFFFFF">
				<b>ersonal</b>
			</font>	
		</td>					
	</tr>			
</table>
<br> 
<%if( ChecaRespuesta(strResultado, "errores.xsl")=1 )then%>
 <table bgcolor="#FFCFB9" width="95%">
	<tr>
		<td width="38%">
			<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">
				<b>Muchas Gracias por enviar tu solicitud</b> 
			</font>
		</td>			
		<td>
			<font face="Verdana, Arial, Helvetica, sans-serif" color="#006688" size="3">
				<b><% =Request.Form("SO_NOMBRES") + " " + Request.Form("SO_APE_PAT") %></b>
			</font>
		</td>
	</tr>
	<tr>
		<td>
			<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="2">
				<b> El Folio Asignado a tu Solicitud es:</b>							
			</font>
		</td>
		<td>
			<font face="Verdana, Arial, Helvetica, sans-serif" color="#006688" size="2">
				<b><% =strFolio %></b>
			</font>
		</td>
	</tr>
</table>
<br>
<hr>
<IMG height=45 src="images/GPOTRESS.gif" width=115>
<%end if%>
</center>
</BODY>
