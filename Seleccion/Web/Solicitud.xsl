<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="html" encoding="ISO-8859-1"/>
	<xsl:template match="/">
		<html>
			<head>
				<title>Solicitud de Empleo - Grupo Tress</title>
				<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
				<LINK rel="stylesheet" type="text/css" href="definicion.css" /> 
				<SCRIPT Language="JavaScript">
					<xsl:comment><![CDATA[ 

					function getSelected(opt) {
					   var selected = new Array();
					   var index = 0;
					   for (var intLoop = 0; intLoop < opt.length; intLoop++) {
					      if ((opt[intLoop].selected) ||
					          (opt[intLoop].checked)) {
					         index = selected.length;
					         selected[index] = new Object;
					         selected[index].value = opt[intLoop].value;
					         selected[index].index = intLoop;
					      }
					   }
					   return selected;
					}

					function outputSelected(opt) {
					   var sel = getSelected(opt);
					   var strSel = "";
                            var i = 0;
					   for (var item in sel)
                            {
                               i = i + 1;
					      strSel += "SO_AREA_" + i + "=" + '"' + sel[item].value + '" ';
                               }
					   frmSolicitudEmpleo.SO_AREAS_XML.value = strSel;
					}

					function validateSelection()
					{				
						var result = true;
						var intCount;
						var intSelectedCount;
										
						intSelectedCount=0;
						for (intCount=0; intCount < frmSolicitudEmpleo.SO_AREAS.options.length; intCount++) 
							{
							if (frmSolicitudEmpleo.SO_AREAS.options(intCount).selected==true)
							{
								intSelectedCount=intSelectedCount+1;						
							}
					    }

						if(intSelectedCount > 3)
						{
							alert('el maximo numero de opciones seleccionadas son tres');
							frmSolicitudEmpleo.SO_AREAS.focus();
							result = false;
						}
						return result;
					}

					function isEmailAddr(email)
					{
					  var result = false;
					  var theStr = new String(email);
					  var index = theStr.indexOf("@");
					  if (index > 0)
					  {
					    var pindex = theStr.indexOf(".",index);
					    if ((pindex > index+1) && (theStr.length > pindex+1))
					  result = true;
					  }
					  return result;
					}

					function validRequired(formField,fieldLabel)
					{
					  var result = true;
  
					  if (formField.value == "")
					  {
					    alert('El campo "' + fieldLabel +'" es requerido.');
					    formField.focus();
					    result = false;
					  }
  
					  return result;
					}

					function validEmail(formField,fieldLabel,required)
					{
					  var result = true;
  
					  if (required && !validRequired(formField,fieldLabel))
					    result = false;

					  if (result && ((formField.value.length < 3) || !isEmailAddr(formField.value)) )
					  {
					    alert("Please enter a complete email address in the form: yourname@yourdomain.com");
					    formField.focus();
					    result = false;
					  }
   
					  return result;

					}


					function validNum(formField,fieldLabel,required)
					{
					  var result = true;

					  if (required && !validRequired(formField,fieldLabel))
					    result = false;
  
					   if (result)
					   {
					     var num = parseInt(formField.value);
					     if (isNaN(num))
					     {
					       alert('El campo "' + fieldLabel +'" debe ser numerico.');
					      formField.focus();    
					      result = false;
					    }
					  } 
  
					  return result;
					}

					function validDate(formField,fieldLabel,required)
					{
					  var result = true;

					  if (required && !validRequired(formField,fieldLabel))
					    result = false;
  
					   if (result)
					   {
					     var elems = formField.value.split("/");
     
					     result = (elems.length == 3); // should be three components
     
					     if (result)
					     {
					       var day = parseInt(elems[0]);
					        var month = parseInt(elems[1]);
					       var year = parseInt(elems[2]);
					      result = !isNaN(month) && (month > 0) && (month < 13) &&
					            !isNaN(day) && (day > 0) && (day < 32) &&
					            !isNaN(year) && (elems[2].length == 4);
					     }

					      if (!result)
					     {
					       alert('Por favor teclee la fecha en el formato dd/mm/yyyy para el campo "' + fieldLabel +'".');
					      formField.focus();
					    }
					  }

					  return result;
					}

                                        function validDate2(formField,fieldLabel,required)
					{
					  var result = true;

                                          result = required;
					  'if (required && !validRequired(formField,fieldLabel))
					    'result = false;

					   if (result)
					   {
					     var elems = formField.value.split("/");

					     result = (elems.length == 3); // should be three components

					     if (result)
					     {
					       var day = parseInt(elems[0]);
					        var month = parseInt(elems[1]);
					       var year = parseInt(elems[2]);
					      result = !isNaN(month) && (month > 0) && (month < 13) &&
					            !isNaN(day) && (day > 0) && (day < 32) &&
					            !isNaN(year) && (elems[2].length == 4);
					     }

					      if (!result)
					     {
					       alert('Por favor teclee la fecha en el formato dd/mm/yyyy "' + fieldLabel +'".');
					      formField.focus();
					    }
					  }

					  return result;
					}

				    ]]>
					<xsl:call-template name="validaforma"></xsl:call-template>
					</xsl:comment>
					
				</SCRIPT>
				<SCRIPT Language="VBScript">
					<xsl:comment>
					<![CDATA[ 
					
					Sub Window_OnLoad()
						frmSolicitudEmpleo.SO_FEC_INI.value = day(now) & "/" & month(now) & "/" & year(now)
					end sub
									    
				    ]]>
					</xsl:comment>
				</SCRIPT>			
			</head>
			<body bgcolor="#ffffff" link="#ff0000" vlink="#ff0000" alink="#ff0000">
                        <form name="frmSolicitudEmpleo" method="POST" action="SolicitudValida.asp" onsubmit="return validateForm(this)">
                                <center>
                                <table cellSpacing="0" cellPadding="0" width="95%" border="1">
                                       <tr>
                                           <td>
					       <font face="book antiqua" size="48" color="#FF8000">
					      	     <b>S</b></font>
                                               <font face="book antiqua" size="32" color="#FFFFFF">
						     <b>elecci<xsl:text>&#243;</xsl:text>n de </b></font>
                                               <font face="book antiqua" size="48" color="#FF8000">
						     <b>P</b></font>
                                               <font face="book antiqua" size="32" color="#FFFFFF">
						     <b>ersonal</b></font>
                                           </td>
                                       </tr>
                                </table>

                                <table border="1" cellpadding="5" cellspacing="0" width="60%" >
                                <tr>
                                    <TD class="TD_SubTitulo" colspan="2" width="50%">Datos Personales.</TD>
                                </tr>
                                </table>
                                <table border="1" cellpadding="5" cellspacing="0" width="60%" >
                                <tr>
                                    <TD class="TD_Etiquetas" ALIGN="right">Apellido Paterno:</TD>
                                    <TD class="TD_Campos"><input type="TEXT" size="30" name="SO_APE_PAT" Maxlength="30" ></input></TD>
                                </tr>
                                <tr>
                                    <TD class="TD_Etiquetas" ALIGN="right">Apellido Materno:</TD>
                                    <TD class="TD_Campos"><input type="text" size="30" name="SO_APE_MAT" Maxlength="30"></input></TD>
                                </tr>
                                <tr>
                                    <TD class="TD_Etiquetas" ALIGN="right">Nombres:</TD>
                                    <TD class="TD_Campos"><input type="text" size="30" name="SO_NOMBRES" Maxlength="30"></input></TD>
                                </tr>
                                <tr>
                                    <TD class="TD_Etiquetas" ALIGN="right">Fecha Nacimiento:</TD>
	                            <TD class="TD_Campos"><input type="text" name="SO_FEC_NAC" size="16"></input>(dd/mm/yyyy)</TD>
                                </tr>
                                <tr>
                                    <TD class="TD_Etiquetas" ALIGN="right">Sexo:</TD>
                                    <TD class="TD_Campos">
                                         <xsl:element name="select">
                                                 <xsl:attribute name="name">SO_SEXO</xsl:attribute>
                                                 <xsl:attribute name="ID">SO_SEXO</xsl:attribute>
                                                 <xsl:attribute name="style">WIDTH: 210px</xsl:attribute>
                                                 <xsl:for-each select="RAIZ/SEXOS/SEXO">
                                                         <xsl:element name="option">
                                                                 <xsl:attribute name="value">
                                                                         <xsl:value-of select="."/>
                                                                 </xsl:attribute>
                                                                 <xsl:value-of select="@descripcion"/>
                                                         </xsl:element>
                                                 </xsl:for-each>
                                         </xsl:element>
                                    </TD>
                                </tr>
                                <tr>
                                    <TD class="TD_Etiquetas" ALIGN="right">Estado Civil:</TD>
                                    <TD class="TD_Campos">
                                                 <xsl:element name="select">
                                                 <xsl:attribute name="name">SO_EDO_CIV</xsl:attribute>
                                                 <xsl:attribute name="ID">SO_EDO_CIV</xsl:attribute>
                                                 <xsl:attribute name="style">WIDTH: 210px</xsl:attribute>
                                                 <xsl:for-each select="RAIZ/ESTADOSCIVILES/EDOCIV">
                                                         <xsl:element name="option">
                                                                 <xsl:attribute name="value">
                                                                         <xsl:value-of select="."/>
                                                                 </xsl:attribute>
                                                                 <xsl:value-of select="@descripcion"/>
                                                         </xsl:element>
                                                 </xsl:for-each>
                                         </xsl:element>
                                    </TD>
                                </tr>
                                <tr>
                                    <TD class="TD_SubTitulo" colspan="2" width="50%">Escolaridad.</TD>
                                </tr>
                                <tr>
                                    <TD class="TD_Etiquetas" ALIGN="right">Escolaridad:</TD>
                                    <TD class="TD_Campos">
                                        <font>
                                              <xsl:element name="select">
                                                      <xsl:attribute name="name">SO_ESTUDIO</xsl:attribute>
                                                      <xsl:attribute name="ID">SO_ESTUDIO</xsl:attribute>
                                                      <xsl:attribute name="style">WIDTH: 210px</xsl:attribute>
                                                      <xsl:for-each select="RAIZ/ESCOLARIDAD/GRADO">
                                                              <xsl:element name="option">
                                                                      <xsl:attribute name="value">
                                                                              <xsl:value-of select="."/>
                                                                      </xsl:attribute>
                                                                      <xsl:value-of select="@descripcion"/>
                                                              </xsl:element>
                                                      </xsl:for-each>
                                              </xsl:element>
                                        </font>
                                    </TD>
                                 </tr>
                                 <tr>
                                     <TD class="TD_Etiquetas" ALIGN="right">Dominio Ingl�s:</TD>
                                     <TD class="TD_Campos"><input type="text" size="15" name="SO_INGLES"></input>
                                          <STRONG>
                                                  <FONT face="Verdana" color="#333333" size="2">%</FONT>
                                          </STRONG>
                                     </TD>
                                 </tr>
                                 <tr>
                                     <TD class="TD_Etiquetas" ALIGN="right">Otro Idioma:</TD>
                                     <TD class="TD_Campos"><input  type="text" size="30" name="SO_IDIOMA2"></input></TD>
                                  </tr>
                                  <tr>
                                      <TD class="TD_Etiquetas" ALIGN="right">Dominio:</TD>
                                      <TD class="TD_Campos"><input type="text" size="15" name="SO_DOMINA2"></input>
                                        <STRONG>
                                              <FONT face="Verdana" color="#333333" size="2">%</FONT>
                                        </STRONG>
                                       </TD>
                                  </tr>
                                  <tr>
                                      <TD class="TD_Etiquetas" ALIGN="right">Tel�fono:</TD>
                                      <TD class="TD_Campos">
                                         <input type="text" size="30" name="SO_TEL"></input>
                                      </TD>
                                  </tr>
                                  <tr>
                                      <TD class="TD_Subtitulo" colspan="2">Solicitud.</TD>
                                   </tr>
                                   <tr>
                                       <TD class="TD_Etiquetas" ALIGN="right">Fecha Solicitud:</TD>
                                       <TD class="TD_Campos">
                                           <xsl:element name="input">
                                                   <xsl:attribute name="name">SO_FEC_INI</xsl:attribute>
                                                   <xsl:attribute name="type">text</xsl:attribute>
                                                   <xsl:attribute name="ID">SO_FEC_INI</xsl:attribute>
                                                   <xsl:attribute name="size">16</xsl:attribute>
                                                   <xsl:attribute name="value"></xsl:attribute>
                                           </xsl:element>(dd/mm/yyyy)
                                       </TD>
                                   </tr>
                                   <tr>
                                       <TD class="TD_Etiquetas" ALIGN="right">Puesto Solicitado:</TD>
                                       <TD class="TD_Campos">
                                            <input type="text" size="30" name="SO_PUE_SOL"></input>
                                       </TD>
                                   </tr>
                                   <tr>
                                      <TD class="TD_Etiquetas" ALIGN="right">Areas de Inter�s :<br></br> (Maximo Tres)</TD>
                                      <TD class="TD_Campos">
                                             <xsl:element name="select">
                                                     <xsl:attribute name="name">SO_AREAS</xsl:attribute>
                                                     <xsl:attribute name="ID">SO_AREAS</xsl:attribute>
                                                     <xsl:attribute name="style">WIDTH: 210px; HEIGHT: 190px</xsl:attribute>
                                                     <xsl:attribute name="Size">2</xsl:attribute>
                                                     <xsl:attribute name="multiple">True</xsl:attribute>
                                                     <xsl:for-each select="RAIZ/INTERESES/AREA">
                                                             <xsl:element name="option">
                                                                     <xsl:attribute name="value">
                                                                             <xsl:value-of select="."/>
                                                                     </xsl:attribute>
                                                                     <xsl:value-of select="@descripcion"/>
                                                             </xsl:element>
                                                     </xsl:for-each>
                                             </xsl:element>
                                             <input type="hidden" name="SO_AREAS_XML" ></input>
                                      </TD>
                                  </tr>
                                  <xsl:if test="RAIZ/ADICIONALES/ADICIONAL" >
                                     <tr>
                                          <TD class="TD_Subtitulo" colspan="2">Datos Adicionales.</TD>
                                     </tr>
                                  </xsl:if>
                                  <xsl:for-each select="RAIZ/ADICIONALES/ADICIONAL">
                                    <tr>
                                        <td align="right" class="TD_Etiquetas"><xsl:value-of select="@letrero"/>:</td>
                                        <td class="TD_Campos">
                                             <xsl:choose>
                                                     <xsl:when test="@Tipo='TABLA'">
                                                             <xsl:element name="select">
                                                                     <xsl:attribute name="name"><xsl:value-of select="@Campo"/></xsl:attribute>
                                                                     <xsl:attribute name="ID"><xsl:value-of select="@Campo"/></xsl:attribute>
                                                                     <xsl:attribute name="style">WIDTH: 210px</xsl:attribute>
                                                                     <xsl:for-each select="VALORES/VALOR">
                                                                             <xsl:element name="option">
                                                                                     <xsl:attribute name="value">
                                                                                             <xsl:value-of select="."/>
                                                                                     </xsl:attribute>
                                                                                     <xsl:value-of select="@descripcion"/>
                                                                             </xsl:element>
                                                                     </xsl:for-each>
                                                             </xsl:element>
                                                     </xsl:when>
                                                     <xsl:when test="@Tipo='FECHA'">
                                                             <xsl:element name="input">
                                                                     <xsl:attribute name="name">
                                                                             <xsl:value-of select="@Campo"/>
                                                                     </xsl:attribute>
                                                                     <xsl:attribute name="type">text</xsl:attribute>
                                                                     <xsl:attribute name="ID">
                                                                             <xsl:value-of select="@Campo"/>
                                                                     </xsl:attribute>
                                                                     <xsl:attribute name="size">16</xsl:attribute>
                                                             </xsl:element>(dd/mm/yyyy)
                                                     </xsl:when>
                                                     <xsl:when test="@Tipo='NUMERO'">
                                                             <xsl:element name="input">
                                                                     <xsl:attribute name="name">
                                                                             <xsl:value-of select="@Campo"/>
                                                                     </xsl:attribute>
                                                                     <xsl:attribute name="type">text</xsl:attribute>
                                                                     <xsl:attribute name="ID">
                                                                             <xsl:value-of select="@Campo"/>
                                                                     </xsl:attribute>
                                                                     <xsl:attribute name="size">30</xsl:attribute>
                                                             </xsl:element>
                                                     </xsl:when>
                                                     <xsl:when test="@Tipo='LOGICO'">
                                                             <xsl:element name="input">
                                                                     <xsl:attribute name="type">checkbox</xsl:attribute>
                                                                     <xsl:attribute name="name">
                                                                             <xsl:value-of select="@Campo"/>
                                                                     </xsl:attribute>
                                                                     <xsl:attribute name="ID">
                                                                             <xsl:value-of select="@Campo"/>
                                                                     </xsl:attribute>
                                                                     <xsl:attribute name="size">30</xsl:attribute>
                                                             </xsl:element>
                                                     </xsl:when>
                                                     <xsl:when test="@Tipo='TEXTO'">
                                                             <xsl:element name="input">
                                                                     <xsl:attribute name="name"><xsl:value-of select="@Campo"/></xsl:attribute>
                                                                     <xsl:attribute name="type">text</xsl:attribute>
                                                                     <xsl:attribute name="ID"><xsl:value-of select="@Campo"/></xsl:attribute>
                                                                     <xsl:attribute name="size">30</xsl:attribute>
                                                             </xsl:element>
                                                     </xsl:when>
                                                     <xsl:otherwise>OTRO<xsl:element name="input"><xsl:attribute name="name"><xsl:value-of select="@Campo"/></xsl:attribute><xsl:attribute name="ID"><xsl:value-of select="@Campo"/></xsl:attribute><xsl:attribute name="size">40</xsl:attribute></xsl:element></xsl:otherwise>
                                             </xsl:choose>
                                        </td>
                                    </tr>
                                    </xsl:for-each>
                                    <tr>
                                        <xsl:call-template name="generaAntecedentes">
                                                <xsl:with-param name="nodosAProcesar" select="/RAIZ/ANTECEDENTES"/>
                                                <xsl:with-param name="indice" select="1"/>
                                                <xsl:with-param name="total" select="/RAIZ/ANTECEDENTES/CANTIDAD/."/>
                                        </xsl:call-template>
                                   </tr>
                                   <tr>
                                       <TD Class="TD_Submit" COLSPAN="2">
                                            <center>
                                                    <input type="submit"  value="Enviar Solicitud" name="submit"></input>
                                            </center>
                                       </TD>
                                   </tr>
                                </table>
                                </center>
				</form>
				<center>
					<p align="center">
                                                <center>
                                                <xsl:element name="img">
                                                      <xsl:attribute name="src">images/GPOTRESS.gif</xsl:attribute>
                                                </xsl:element>
                                                </center>
                                                <FONT SIZE="2" COLOR="#333333" FACE="Verdana, Arial, Helvetica, sans-serif">Copyright � 1998-2002<a href="http://www.tress.com.mx"><xsl:text> </xsl:text>Grupo Tress <xsl:text> </xsl:text></a>Todos los derechos reservados</FONT>
					</p>
				</center>
			</body>
		</html>
	</xsl:template>
	<xsl:template name="generaAntecedentes">
		<xsl:param name="nodosAProcesar"/>
		<xsl:param name="indice"/>
		<xsl:param name="total"/>
		<xsl:if test="$nodosAProcesar/letrero" >
			<tr>
				<TD class="TD_Subtitulo" colspan="2">Empleos Anteriores <xsl:value-of select="$indice"/>/<xsl:value-of select="$total"/></TD>
			</tr>
		</xsl:if>
		<xsl:for-each select="$nodosAProcesar/letrero">
			<tr>
				<td align="right" class="TD_Etiquetas"><xsl:value-of select="."/>:</td>
				<td class="TD_Campos">
						<xsl:element name="input">
							<xsl:attribute name="name"><xsl:value-of select="@Campo"/>_<xsl:value-of select="$indice"/></xsl:attribute>
							<xsl:attribute name="type">text</xsl:attribute>
							<xsl:attribute name="ID"><xsl:value-of select="@Campo"/>_<xsl:value-of select="$indice"/></xsl:attribute>
							<xsl:attribute name="size">40</xsl:attribute>
						</xsl:element>
				</td>
			</tr>
		</xsl:for-each>
		<xsl:if test="$indice &lt; $total">
			<xsl:call-template name="generaAntecedentes">
				<xsl:with-param name="nodosAProcesar" select="$nodosAProcesar"/>
				<xsl:with-param name="indice" select="$indice + 1"/>
				<xsl:with-param name="total" select="$total"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

<xsl:template name="validaforma">

		function validateForm(theForm)
		{
		  // Customize these calls for your form

		  // Start ------->
		  if (!validRequired(theForm.SO_APE_PAT,"Apellido Paterno"))
		    return false;

		  if (!validRequired(theForm.SO_APE_MAT,"Apellido Materno"))
		    return false;

		  if (!validRequired(theForm.SO_NOMBRES,"Nombres"))
		    return false;

		  if (!validDate(theForm.SO_FEC_NAC,"Fecha Nacimiento",true))
		    return false;

		  if (!validDate(theForm.SO_FEC_INI,"Fecha Solicitud",false))
		    return false;

                  for(var i in theForm)
                  {
                   if((i=='SO_G_FEC_1')||(i=='SO_G_FEC_2')||(i=='SO_G_FEC_3')||(i=='SO_G_FEC_4')||(i=='SO_G_FEC_5'))
                   {
                    if (!validDate2(theForm[i],'Fecha Adicional',false))
		       return false;
                   }
                  }

		  if (!validateSelection())
		  	return false;

		  //if (!validEmail(theForm.email,"Email Address",true))
		  //  return false;

		  //if (!validDate(theForm.available,"Date Available",true))
		  //  return false;

		  //if (!validNum(theForm.yearsexperience,"Years Experience",true))
		  //  return false;
		  //

<xsl:for-each select="RAIZ/ADICIONALES/letrero">
	<xsl:choose>
		<xsl:when test="@Tipo='FECHA'">
		if (theForm.<xsl:value-of select="@Campo"/>.value != "")
		{
			if (!validDate(theForm.<xsl:value-of select="@Campo"/>,"<xsl:value-of select="."/>",false))
			return false;
		}
		</xsl:when>
		<xsl:when test="@Tipo='NUMERO'">
		if (theForm.<xsl:value-of select="@Campo"/>.value != "")
		{
			if (!validNum(theForm.<xsl:value-of select="@Campo"/>,"<xsl:value-of select="."/>",false))
			return false;
		}
		</xsl:when>
	</xsl:choose>
</xsl:for-each>

		outputSelected(frmSolicitudEmpleo.SO_AREAS.options)

		  return true;
		}

</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario2" userelativepaths="no" externalpreview="no" url="file://c:\Proyectos\WEB\SeleccionPersonal\SeleccionPersonal_Local\Solicitud.xml" htmlbaseurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->