unit DDiccionario;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     DBaseSeleccionDiccionario,
     ZetaTipoEntidad,
     ZetaClientDataSet,
     DBaseDiccionario;

type
  TdmDiccionario = class(TdmBaseSeleccionDiccionario)
    procedure cdsDiccionAlModificar(Sender: TObject);
  private
    { Private declarations }
  protected


  public
    { Public declarations }
    procedure SetLookupNames;override;
  end;

var
  dmDiccionario: TdmDiccionario;

implementation
uses DSeleccion,
     FEditDiccion,
     ZBaseEdicion;

{$R *.DFM}


procedure TdmDiccionario.SetLookupNames;
begin
     inherited;
     dmSeleccion.SetLookupNames;
end;

procedure TdmDiccionario.cdsDiccionAlModificar(Sender: TObject);
begin
     inherited;
     ZBaseEdicion.ShowFormaEdicion( EditDiccion, TEditDiccion );

end;


end.
