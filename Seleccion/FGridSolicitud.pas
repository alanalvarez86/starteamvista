unit FGridSolicitud;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, StdCtrls, Buttons, Mask, Grids, DBGrids,
     ZetaCommonClasses,
     ZBaseConsulta,
     ZetaNumero,
     ZetaKeyLookup,
     ZetaDBGrid;

type
  TGridSolicitud = class(TBaseConsulta)
    PanelFiltro: TPanel;
    SolStatusLBL: TLabel;
    AreaLBL: TLabel;
    EstudiosLBL: TLabel;
    SexoLBL: TLabel;
    EdadMinimaLBL: TLabel;
    EdadMaximaLBL: TLabel;
    InglesLBL: TLabel;
    InglesPctLBL: TLabel;
    ApePatLBL: TLabel;
    NombreLBL: TLabel;
    SolStatus: TComboBox;
    MinEstudios: TComboBox;
    Sexo: TComboBox;
    ApePat: TEdit;
    Nombre: TEdit;
    Refrescar: TBitBtn;
    Area: TZetaKeyLookup;
    EdadMinima: TZetaNumero;
    EdadMaxima: TZetaNumero;
    Ingles: TZetaNumero;
    ZetaDBGrid: TZetaDBGrid;
    Label1: TLabel;
    ApeMatLbl: TLabel;
    ApeMat: TEdit;
    CriterioLbl: TLabel;
    Condicion: TZetaKeyLookup;
    BotonReset: TSpeedButton;
    OtrasHabLBL: TLabel;{OP:14.Abr.08}
    OtrasHab: TEdit;{OP:14.Abr.08}
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure RefrescarClick(Sender: TObject);
    procedure BotonResetClick(Sender: TObject);
    procedure EdadExit(Sender: TObject);
  private
    { Private declarations }
    FParametros: TZetaParams;
    function GetCondicion: String;
    function ValidaRango(const iMinimo, iMaximo: Integer; const sInfo: String; oControl: TWinControl ): Boolean;
    procedure InicializaCampos;
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
  end;

var
  GridSolicitud: TGridSolicitud;

implementation

{$R *.DFM}

uses DSeleccion,
     ZetaCommonLists,
     ZetaCommonTools,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaDialogo;

{ ********** TGridSolicitud ************ }

procedure TGridSolicitud.FormCreate(Sender: TObject);

procedure FillCombo( Control: TComboBox; Tipo: ListasFijas; const sTodos: String );
begin
     with Control do
     begin
          ZetaCommonLists.LlenaLista( Tipo, Items );
          Items.Insert( 0, Format( '< %s >', [ sTodos ] ) );
          ItemIndex := 0;
     end;
end;

begin
     inherited;
     FParametros := TZetaParams.Create;
     FillCombo( SolStatus, lfSolStatus, 'Todos' );
     SolStatus.Items.Insert( 1, Format( '%s', [ 'Disponible/En Tramite' ] ) );
     FillCombo( MinEstudios, lfEstudios, 'Cualquiera' );
     FillCombo( Sexo, lfSexoDesc, 'Todos' );
     InicializaCampos;
{     EdadMinima.Valor := 0;
     EdadMaxima.Valor := 0;
     Ingles.Valor := 0; }
     HelpContext := H00081_Solicitudes;
     dmSeleccion.cdsSolicita.SetDataChange;
     Area.LookupDataset := dmSeleccion.cdsAreaInteres;
     Condicion.LookupDataset := dmSeleccion.cdsCondiciones;
end;

procedure TGridSolicitud.FormShow(Sender: TObject);
begin
     inherited;
//     Refresh;  // Para que salga en el OnShow
     ZetaDBGrid.SetFocus;
end;

procedure TGridSolicitud.FormDestroy(Sender: TObject);
begin
     inherited;
     FParametros.Free;
end;

procedure TGridSolicitud.Connect;
begin
     with dmSeleccion do
     begin
          cdsAreaInteres.Conectar;
          cdsCondiciones.Conectar;
          DataSource.DataSet := cdsSolicita;
          if ( cdsSolicita.HayQueRefrescar ) then
             Refresh;
     end;
end;

procedure TGridSolicitud.Refresh;
var
   iFolio: Integer;
begin
     with FParametros do
     begin
          AddInteger( 'Status', SolStatus.ItemIndex - 2 );
          AddInteger( 'EdadMinima', EdadMinima.ValorEntero );
          AddInteger( 'EdadMaxima', EdadMaxima.ValorEntero );
          AddInteger( 'Sexo', Sexo.ItemIndex - 1 );
          AddInteger( 'MinEstudios', MinEstudios.ItemIndex - 1 );
          AddInteger( 'Ingles', Ingles.ValorEntero );
          AddString( 'ApePat', ApePat.Text );
          AddString( 'ApeMat', ApeMat.Text );
          AddString( 'Nombres', Nombre.Text );
          AddString( 'Area', Area.Llave );
          AddString( 'Condicion', GetCondicion );
          AddString( 'OtrasHab', OtrasHab.Text );{OP:14.Abr.08}
     end;
     with dmSeleccion do
     begin
          iFolio := FolioSolicitud;                   // Si hubo registro tiene el n�mero de folio, si no se posiciona sobre el actual del grid
          RefrescarSolicita( FParametros );
          with cdsSolicita do
               if ( iFolio > 0 ) and ( not Locate( 'SO_FOLIO', iFolio, [ ] ) ) then
                  First;
     end;
end;

function TGridSolicitud.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := ZAccesosMgr.Revisa( D_REGISTRO_SOLICITUD );
     if not Result then
        sMensaje := 'No Tiene Permiso Para Agregar Registros';
end;

procedure TGridSolicitud.Agregar;
begin
     dmSeleccion.cdsSolicita.Agregar;
end;

procedure TGridSolicitud.Borrar;
begin
     dmSeleccion.cdsSolicita.Borrar;
end;

procedure TGridSolicitud.Modificar;
begin
     dmSeleccion.cdsSolicita.Modificar;
end;

procedure TGridSolicitud.RefrescarClick(Sender: TObject);
begin
     inherited;
     if ValidaRango( EdadMinima.ValorEntero, EdadMaxima.ValorEntero, 'Edad', EdadMinima ) then
     begin
          DoRefresh;
          ZetaDBGrid.SetFocus;
     end;
end;

procedure TGridSolicitud.BotonResetClick(Sender: TObject);
begin
     inherited;
     InicializaCampos;
end;

function TGridSolicitud.GetCondicion: String;
begin
     if StrLleno( Condicion.Llave ) then
        Result:= Trim( Condicion.LookUpDataSet.FieldByName( 'QU_FILTRO' ).AsString )
     else
        Result:= '';
end;

function TGridSolicitud.ValidaRango( const iMinimo, iMaximo: Integer;
         const sInfo: String; oControl: TWinControl): Boolean;
begin
     Result := ( iMinimo <= 0 ) or ( iMaximo <= 0 ) or ( iMinimo <= iMaximo );
     if not Result then
     begin
          ZetaDialogo.ZError( self.Caption, Format( 'El Rango de %s debe ser de Menor a Mayor', [ sInfo ] ), 0 );
          oControl.SetFocus;
     end;
end;

procedure TGridSolicitud.InicializaCampos;
begin
     SolStatus.ItemIndex := 1;
     Sexo.ItemIndex := 0;
     EdadMinima.Valor := 0;
     EdadMaxima.Valor := 0;
     MinEstudios.ItemIndex := 0;
     Ingles.Valor := 0;
     ApePat.Text := VACIO;
     ApeMat.Text := VACIO;
     Nombre.Text := VACIO;
     Area.Llave := VACIO;
     Condicion.Llave := VACIO;
end;

procedure TGridSolicitud.EdadExit(Sender: TObject);
begin
     inherited;
     with Sender As TZetaNumero do
     begin
          if ( ValorEntero < 0 ) or ( ValorEntero > 99 ) then
          begin
               ZetaDialogo.zInformation( Caption, 'La edad est� fuera del rango permitido (0-99)', 0 );
               Valor := 0;
               SetFocus;
          end;
     end;
end;

end.

