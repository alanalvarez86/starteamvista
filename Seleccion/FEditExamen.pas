unit FEditExamen;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask,
     ZBaseEdicion,
     ZetaFecha,
     ZetaDBTextBox,
     ZetaKeyCombo,
     ZetaNumero;

type
  TEditExamen = class(TBaseEdicion)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EX_FECHA: TZetaDBFecha;
    Label4: TLabel;
    EX_TIPO: TZetaDBKeyCombo;
    Label5: TLabel;
    EX_APLICO: TDBEdit;
    Label6: TLabel;
    EX_RESUMEN: TDBEdit;
    GroupBox1: TGroupBox;
    EX_EVALUA1: TZetaDBNumero;
    Label8: TLabel;
    Label9: TLabel;
    EX_EVALUA2: TZetaDBNumero;
    Label10: TLabel;
    EX_EVALUA3: TZetaDBNumero;
    EX_OBSERVA: TDBMemo;
    Label11: TLabel;
    EX_APROBO: TDBCheckBox;
    SO_FOLIO: TZetaDBTextBox;
    EX_FOLIO: TZetaDBTextBox;
    PRETTYNAME: TZetaTextBox;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditExamen: TEditExamen;

implementation

{$R *.DFM}

uses DSeleccion, ZAccesosTress,ZetaCommonClasses;

procedure TEditExamen.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := EX_FECHA;
     IndexDerechos := D_SOLICITUD_EXAMENES;
     HelpContext := H00076_Solicitud_Examenes;
end;

procedure TEditExamen.Connect;
begin
     Datasource.Dataset := dmSeleccion.cdsExamenes;
     PRETTYNAME.Caption := dmSeleccion.cdsEditSolicita.FieldByName( 'PRETTYNAME' ).AsString;
end;

end.
