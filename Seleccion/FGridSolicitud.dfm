inherited GridSolicitud: TGridSolicitud
  Left = 276
  Top = 229
  ActiveControl = SolStatus
  Caption = 'Solicitudes'
  ClientHeight = 283
  ClientWidth = 607
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 607
    inherited ValorActivo2: TPanel
      Width = 348
    end
  end
  object PanelFiltro: TPanel [1]
    Left = 0
    Top = 19
    Width = 607
    Height = 142
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object SolStatusLBL: TLabel
      Left = 52
      Top = 6
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = '&Status:'
      FocusControl = SolStatus
    end
    object AreaLBL: TLabel
      Left = 253
      Top = 5
      Width = 75
      Height = 13
      Alignment = taRightJustify
      Caption = 'Area de &Inter'#233's:'
      FocusControl = Area
    end
    object EstudiosLBL: TLabel
      Left = -1
      Top = 72
      Width = 86
      Height = 13
      Alignment = taRightJustify
      Caption = '&Estudios M'#237'nimos:'
      FocusControl = MinEstudios
    end
    object SexoLBL: TLabel
      Left = 58
      Top = 28
      Width = 27
      Height = 13
      Alignment = taRightJustify
      Caption = 'Se&xo:'
      FocusControl = Sexo
    end
    object EdadMinimaLBL: TLabel
      Left = 29
      Top = 50
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'E&dad entre:'
      FocusControl = EdadMinima
    end
    object EdadMaximaLBL: TLabel
      Left = 136
      Top = 50
      Width = 7
      Height = 13
      Caption = '&Y'
      FocusControl = EdadMaxima
    end
    object InglesLBL: TLabel
      Left = 53
      Top = 94
      Width = 31
      Height = 13
      Alignment = taRightJustify
      Caption = 'I&ngl'#233's:'
      FocusControl = Ingles
    end
    object InglesPctLBL: TLabel
      Left = 123
      Top = 94
      Width = 8
      Height = 13
      Caption = '%'
    end
    object ApePatLBL: TLabel
      Left = 248
      Top = 50
      Width = 80
      Height = 13
      Alignment = taRightJustify
      Caption = 'A&pellido Paterno:'
      FocusControl = ApePat
    end
    object NombreLBL: TLabel
      Left = 288
      Top = 94
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'N&ombre:'
      FocusControl = Nombre
    end
    object Label1: TLabel
      Left = 199
      Top = 50
      Width = 24
      Height = 13
      Caption = 'A'#241'os'
      FocusControl = EdadMaxima
    end
    object ApeMatLbl: TLabel
      Left = 246
      Top = 72
      Width = 82
      Height = 13
      Alignment = taRightJustify
      Caption = 'Apellido &Materno:'
      FocusControl = ApeMat
    end
    object CriterioLbl: TLabel
      Left = 260
      Top = 27
      Width = 68
      Height = 13
      Alignment = taRightJustify
      Caption = 'Otros &Criterios:'
      FocusControl = Condicion
    end
    object BotonReset: TSpeedButton
      Left = 3
      Top = 2
      Width = 23
      Height = 22
      Hint = 'Reinicializa Campos de B'#250'squeda'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00500005000555
        555557777F777555F55500000000555055557777777755F75555005500055055
        555577F5777F57555555005550055555555577FF577F5FF55555500550050055
        5555577FF77577FF555555005050110555555577F757777FF555555505099910
        555555FF75777777FF555005550999910555577F5F77777775F5500505509990
        3055577F75F77777575F55005055090B030555775755777575755555555550B0
        B03055555F555757575755550555550B0B335555755555757555555555555550
        BBB35555F55555575F555550555555550BBB55575555555575F5555555555555
        50BB555555555555575F555555555555550B5555555555555575}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = BotonResetClick
    end
    object OtrasHabLBL: TLabel
      Left = -1
      Top = 116
      Width = 86
      Height = 13
      Caption = 'Otras &Habilidades:'
      FocusControl = OtrasHab
    end
    object SolStatus: TComboBox
      Left = 87
      Top = 2
      Width = 140
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      TabStop = False
    end
    object MinEstudios: TComboBox
      Left = 87
      Top = 68
      Width = 140
      Height = 21
      Style = csDropDownList
      DropDownCount = 10
      ItemHeight = 13
      TabOrder = 4
    end
    object Sexo: TComboBox
      Left = 87
      Top = 24
      Width = 140
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
    end
    object ApePat: TEdit
      Left = 330
      Top = 46
      Width = 185
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 9
    end
    object Nombre: TEdit
      Left = 330
      Top = 90
      Width = 185
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 11
    end
    object Refrescar: TBitBtn
      Left = 528
      Top = 56
      Width = 73
      Height = 43
      Hint = 'Buscar Solicitudes De Acuerdo A Los Criterios Especificados'
      Caption = '&Buscar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 12
      OnClick = RefrescarClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D808DDDDDDDD
        DDDD83308DDDDDDDDDDD873308DDDDDDDDDDD373308DDDDDDDDDDD3733088888
        8DDDDDD37330000088DDDDDD37338777808DDDDDD038F7F77808DDDDD08FCCCC
        C788DDDCD0F7C7F7C770DDDCC07FCCCCCF70DDDCD0F7CFFCF770DDDDC88FCCCC
        7F88DDDDDD08F7F7F80DDDDDDDD08F7F80DDDDDDDDDD80008DDD}
      Layout = blGlyphTop
    end
    object Area: TZetaKeyLookup
      Left = 330
      Top = 2
      Width = 275
      Height = 21
      LookupDataset = dmSeleccion.cdsAreaInteres
      TabOrder = 7
      TabStop = True
      WidthLlave = 60
    end
    object EdadMinima: TZetaNumero
      Left = 87
      Top = 46
      Width = 40
      Height = 21
      Mascara = mnDias
      TabOrder = 2
      Text = '0'
      OnExit = EdadExit
    end
    object EdadMaxima: TZetaNumero
      Left = 151
      Top = 46
      Width = 40
      Height = 21
      Mascara = mnDias
      TabOrder = 3
      Text = '0'
      OnExit = EdadExit
    end
    object Ingles: TZetaNumero
      Left = 87
      Top = 90
      Width = 35
      Height = 21
      Mascara = mnDias
      TabOrder = 5
      Text = '0'
    end
    object ApeMat: TEdit
      Left = 330
      Top = 68
      Width = 185
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 10
    end
    object Condicion: TZetaKeyLookup
      Left = 330
      Top = 24
      Width = 275
      Height = 21
      LookupDataset = dmSeleccion.cdsCondiciones
      TabOrder = 8
      TabStop = True
      WidthLlave = 60
    end
    object OtrasHab: TEdit
      Left = 87
      Top = 112
      Width = 138
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 6
    end
  end
  object ZetaDBGrid: TZetaDBGrid [2]
    Left = 0
    Top = 161
    Width = 607
    Height = 122
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'SO_FOLIO'
        Title.Alignment = taCenter
        Title.Caption = '#'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRETTYNAME'
        Title.Caption = 'Nombre'
        Width = 155
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SO_STATUS'
        Title.Caption = 'Status'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SO_SEXO'
        Title.Caption = 'Sexo'
        Width = 30
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'SO_FEC_NAC'
        Title.Alignment = taCenter
        Title.Caption = 'Edad'
        Width = 33
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SO_ESTUDIO'
        Title.Caption = 'Escolaridad'
        Width = 70
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'SO_INGLES'
        Title.Alignment = taCenter
        Title.Caption = 'Ingl'#233's'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SO_AREA_1'
        Title.Caption = 'Area 1'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SO_AREA_2'
        Title.Caption = 'Area 2'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SO_AREA_3'
        Title.Caption = 'Area 3'
        Width = 55
        Visible = True
      end>
  end
  inherited DataSource: TDataSource
    Left = 368
    Top = 192
  end
end
