unit ZetaAdicionalMgr;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, DB, DBCtrls,
     ZetaCommonLists,
     ZetaNumero,
     ZetaEdit,
     ZetaFecha;

type
  TListaCampos = class;
  TCampo = class( TObject )
  private
    { Private declarations }
    FTipo: eCampoTipo;
    FLetrero: String;
    FNombre: String;
    FCampo: String;
    FEquivalencia: String;
    FGlobal: Word;
    FGlobalEq : Word;
    FOrden: Word;
    FCapturar: Boolean;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Campo: String read FCampo write FCampo;
    property Capturar: Boolean read FCapturar write FCapturar;
    property Equivalencia: String read FEquivalencia write FEquivalencia;
    property Global: Word read FGlobal write FGlobal;
    property GlobalEq: Word read FGlobalEq write FGlobalEq;
    property Letrero: String read FLetrero write FLetrero;
    property Nombre: String read FNombre write FNombre;
    property Orden: Word read FOrden write FOrden;
    property Tipo: eCampoTipo read FTipo write FTipo;
    function Comparar(Value: TCampo): Integer;
    function TextoDisponible: String;
    function TextoEscogido: String;
  end;
  TListaCampos = class( TObject )
  private
    { Private declarations }
    FCampos: TList;
    FDatasource: TDatasource;
    FForma: TForm;
    function GetCampo(Index: Integer): TCampo;
    procedure Delete(const Index: Integer);
    procedure Ordena;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Campo[ Index: Integer ]: TCampo read GetCampo;
    property Forma: TForm read FForma write FForma;
    property Datasource: TDatasource read FDatasource write FDatasource;
    function Count: Integer;
    procedure Add( const eTipo: eCampoTipo; const sLetrero, sNombre, sCampo, sEquivalencia: String; const iOrden, iGlobal, iGlobalEq: Word; const lCapturar: Boolean );
    procedure Assign(Source: TListaCampos);
    procedure Clear;
    procedure CargarListas( ListaDisponibles, ListaEscogidos: TStrings );
  end;

implementation

uses ZetaCommonClasses;

function ComparaCampos( Item1, Item2: Pointer ): Integer;
begin
     Result := TCampo( Item1 ).Comparar( TCampo( Item2 ) );
end;

{ ******** TCampo ********* }

constructor TCampo.Create;
begin
     FTipo := cdtTexto;
     FLetrero := '';
     FCampo := '';
end;

destructor TCampo.Destroy;
begin
     inherited Destroy;
end;

function TCampo.Comparar( Value: TCampo ): Integer;
const
     K_MENOR = -1;
     K_IGUAL = 0;
     K_MAYOR = 1;
begin
     if ( Self.Orden < Value.Orden ) then
        Result := K_MENOR
     else
         if ( Self.Orden > Value.Orden ) then
            Result := K_MAYOR
         else
             Result := AnsiCompareText( Self.Nombre, Value.Nombre );
end;

function TCampo.TextoEscogido: String;
begin
     Result := Format( '%s ( %s )', [ Letrero, Nombre ] );
end;

function TCampo.TextoDisponible: String;
begin
     Result := Format( '%s', [ Nombre ] );
end;

{ ******* TListaCampos ******** }

constructor TListaCampos.Create;
begin
     FCampos := TList.Create;
end;

destructor TListaCampos.Destroy;
begin
     Clear;
     FreeAndNil( FCampos );
     inherited Destroy;
end;

procedure TListaCampos.Assign( Source: TListaCampos );
var
   i: Integer;
begin
     Self.Clear;
     with Source do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with Campo[ i ] do
               begin
                    Self.Add( Tipo, Letrero, Nombre, Campo, Equivalencia, Orden, Global, GlobalEq, Capturar );
               end;
          end;
     end;
end;

procedure TListaCampos.Add( const eTipo: eCampoTipo;
                            const sLetrero, sNombre, sCampo, sEquivalencia: String;
                            const iOrden, iGlobal, iGlobalEq: Word;
                            const lCapturar: Boolean );
var
   Campo: TCampo;
begin
     Campo := TCampo.Create;
     try
        FCampos.Add( Campo );
        with Campo do
        begin
             Tipo := eTipo;
             Letrero := sLetrero;
             Nombre := sNombre;
             Campo := sCampo;
             Equivalencia := sEquivalencia;
             Orden := iOrden;
             Global := iGlobal;
             GlobalEq := iGlobalEq;
             Capturar := lCapturar;
        end;
     except
           on Error: Exception do
           begin
                Campo.Free;
                raise;
           end;
     end;
end;

procedure TListaCampos.Clear;
var
   i: Integer;
begin
     with FCampos do
     begin
          for i := ( Count - 1 ) downto 0 do
          begin
               Self.Delete( i );
          end;
          Clear;
     end;
end;

function TListaCampos.Count: Integer;
begin
     Result := FCampos.Count;
end;

function TListaCampos.GetCampo(Index: Integer): TCampo;
begin
     with FCampos do
     begin
          if ( Index >= 0 ) and ( Index < Count ) then
             Result := TCampo( Items[ Index ] )
          else
              Result := nil;
     end;
end;

procedure TListaCampos.Delete(const Index: Integer);
begin
     Campo[ Index ].Free;
     FCampos.Delete( Index );
end;

procedure TListaCampos.Ordena;
begin
     FCampos.Sort( ComparaCampos );
end;

procedure TListaCampos.CargarListas(ListaDisponibles, ListaEscogidos: TStrings);
var
   i: Integer;
begin
     Ordena;
     ListaDisponibles.BeginUpdate;
     ListaEscogidos.BeginUpdate;
     try
        ListaDisponibles.Clear;
        ListaEscogidos.Clear;
        with FCampos do
             for i := 0 to ( Count - 1 ) do
                 if ( Campo[ i ].Capturar ) then
                    ListaEscogidos.AddObject( Campo[ i ].TextoEscogido, Campo[ i ] )
                 else
                    ListaDisponibles.AddObject( Campo[ i ].TextoDisponible, Campo[ i ] );
     finally
        ListaDisponibles.EndUpdate;
        ListaEscogidos.EndUpdate;
     end;
end;

end.
