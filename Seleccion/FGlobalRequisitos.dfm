inherited GlobalRequisitos: TGlobalRequisitos
  Left = 336
  Top = 240
  ActiveControl = Requisito1
  Caption = 'Requisitos para Solicitudes'
  ClientHeight = 259
  ClientWidth = 543
  PixelsPerInch = 96
  TextHeight = 13
  object Requisito1LBL: TLabel [0]
    Left = 5
    Top = 11
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Requisito 1:'
  end
  object Requisito2LBL: TLabel [1]
    Left = 5
    Top = 35
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Requisito 2:'
  end
  object Requisito3LBL: TLabel [2]
    Left = 5
    Top = 58
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Requisito 3:'
  end
  object Requisito4LBL: TLabel [3]
    Left = 5
    Top = 82
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Requisito 4:'
  end
  object Requisito5LBL: TLabel [4]
    Left = 5
    Top = 105
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Requisito 5:'
  end
  object Requisito6LBL: TLabel [5]
    Left = 5
    Top = 129
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Requisito 6:'
  end
  object Requisito7LBL: TLabel [6]
    Left = 5
    Top = 152
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Requisito 7:'
  end
  object Requisito8LBL: TLabel [7]
    Left = 5
    Top = 176
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Requisito 8:'
  end
  object Requisito9LBL: TLabel [8]
    Left = 5
    Top = 199
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Requisito 9:'
  end
  inherited PanelBotones: TPanel
    Top = 223
    Width = 543
    inherited OK: TBitBtn
      Left = 374
    end
    inherited Cancelar: TBitBtn
      Left = 454
    end
  end
  object Requisito1: TEdit
    Tag = 13
    Left = 64
    Top = 7
    Width = 325
    Height = 21
    MaxLength = 30
    TabOrder = 1
  end
  object Requisito2: TEdit
    Tag = 14
    Left = 64
    Top = 31
    Width = 325
    Height = 21
    MaxLength = 30
    TabOrder = 3
  end
  object Requisito3: TEdit
    Tag = 15
    Left = 64
    Top = 54
    Width = 325
    Height = 21
    MaxLength = 30
    TabOrder = 5
  end
  object Requisito4: TEdit
    Tag = 16
    Left = 64
    Top = 78
    Width = 325
    Height = 21
    MaxLength = 30
    TabOrder = 7
  end
  object Requisito5: TEdit
    Tag = 17
    Left = 64
    Top = 101
    Width = 325
    Height = 21
    MaxLength = 30
    TabOrder = 9
  end
  object Requisito6: TEdit
    Tag = 18
    Left = 64
    Top = 125
    Width = 325
    Height = 21
    MaxLength = 30
    TabOrder = 11
  end
  object Requisito7: TEdit
    Tag = 19
    Left = 64
    Top = 148
    Width = 325
    Height = 21
    MaxLength = 30
    TabOrder = 13
  end
  object Requisito8: TEdit
    Tag = 20
    Left = 64
    Top = 172
    Width = 325
    Height = 21
    MaxLength = 30
    TabOrder = 15
  end
  object Requisito9: TEdit
    Tag = 21
    Left = 64
    Top = 195
    Width = 325
    Height = 21
    MaxLength = 30
    TabOrder = 17
  end
  object RequisitoTipo1: TZetaKeyCombo
    Left = 392
    Top = 7
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
    ListaFija = lfRequisitoTipo
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
  end
  object RequisitoTipo2: TZetaKeyCombo
    Left = 392
    Top = 31
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 4
    ListaFija = lfRequisitoTipo
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
  end
  object RequisitoTipo3: TZetaKeyCombo
    Left = 392
    Top = 54
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 6
    ListaFija = lfRequisitoTipo
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
  end
  object RequisitoTipo4: TZetaKeyCombo
    Left = 392
    Top = 78
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 8
    ListaFija = lfRequisitoTipo
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
  end
  object RequisitoTipo5: TZetaKeyCombo
    Left = 392
    Top = 101
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 10
    ListaFija = lfRequisitoTipo
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
  end
  object RequisitoTipo6: TZetaKeyCombo
    Left = 392
    Top = 125
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 12
    ListaFija = lfRequisitoTipo
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
  end
  object RequisitoTipo7: TZetaKeyCombo
    Left = 392
    Top = 148
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 14
    ListaFija = lfRequisitoTipo
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
  end
  object RequisitoTipo8: TZetaKeyCombo
    Left = 392
    Top = 172
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 16
    ListaFija = lfRequisitoTipo
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
  end
  object RequisitoTipo9: TZetaKeyCombo
    Left = 392
    Top = 195
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 18
    ListaFija = lfRequisitoTipo
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
  end
end
