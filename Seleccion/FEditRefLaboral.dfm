inherited EditRefLaboral: TEditRefLaboral
  Left = 517
  Top = 174
  Caption = 'Referencias Laborales'
  ClientHeight = 616
  ClientWidth = 474
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 580
    Width = 474
    inherited OK: TBitBtn
      Left = 306
    end
    inherited Cancelar: TBitBtn
      Left = 391
    end
  end
  inherited PanelSuperior: TPanel
    Width = 474
    inherited AgregarBtn: TSpeedButton
      Visible = False
    end
    inherited BorrarBtn: TSpeedButton
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 474
    inherited ValorActivo2: TPanel
      Width = 148
    end
  end
  object PanelHeader: TPanel [3]
    Left = 0
    Top = 51
    Width = 474
    Height = 39
    Align = alTop
    TabOrder = 3
    object Label1: TLabel
      Left = 63
      Top = 8
      Width = 43
      Height = 13
      Caption = 'Solicitud:'
    end
    object Label2: TLabel
      Left = 51
      Top = 21
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Referencia:'
    end
    object SO_FOLIO: TZetaDBTextBox
      Left = 113
      Top = 1
      Width = 50
      Height = 17
      AutoSize = False
      Caption = 'SO_FOLIO'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'SO_FOLIO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object RL_FOLIO: TZetaDBTextBox
      Left = 113
      Top = 19
      Width = 50
      Height = 17
      AutoSize = False
      Caption = 'RL_FOLIO'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'RL_FOLIO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object PRETTYNAME: TZetaTextBox
      Left = 166
      Top = 1
      Width = 300
      Height = 17
      AutoSize = False
      Caption = 'PRETTYNAME'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
  end
  object PanelCampos: TPanel [4]
    Left = 0
    Top = 90
    Width = 474
    Height = 490
    Align = alClient
    TabOrder = 4
    object ReferenciasPg: TPageControl
      Left = 1
      Top = 1
      Width = 472
      Height = 488
      ActivePage = Referencia
      Align = alClient
      TabOrder = 0
      object Referencia: TTabSheet
        Caption = 'Datos de Referencia'
        object RL_CAMPO1Pnl: TPanel
          Left = 0
          Top = 0
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object RL_CAMPO1Lbl: TLabel
            Left = 109
            Top = 5
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 1:'
            FocusControl = RL_CAMPO1
          end
          object RL_CAMPO1: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_CAMPO1'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RL_CAMPO2Pnl: TPanel
          Left = 0
          Top = 23
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object RL_CAMPO2Lbl: TLabel
            Left = 109
            Top = 5
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 2:'
            FocusControl = RL_CAMPO2
          end
          object RL_CAMPO2: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_CAMPO2'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RL_CAMPO4Pnl: TPanel
          Left = 0
          Top = 69
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 3
          object RL_CAMPO4Lbl: TLabel
            Left = 109
            Top = 5
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 4:'
            FocusControl = RL_CAMPO4
          end
          object RL_CAMPO4: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_CAMPO4'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RL_CAMPO3Pnl: TPanel
          Left = 0
          Top = 46
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          object RL_CAMPO3Lbl: TLabel
            Left = 109
            Top = 5
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 3:'
            FocusControl = RL_CAMPO3
          end
          object RL_CAMPO3: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_CAMPO3'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RL_CAMPO5Pnl: TPanel
          Left = 0
          Top = 92
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 4
          object RL_CAMPO5Lbl: TLabel
            Left = 109
            Top = 5
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 5:'
            FocusControl = RL_CAMPO5
          end
          object RL_CAMPO5: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_CAMPO5'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RL_CAMPO6Pnl: TPanel
          Left = 0
          Top = 115
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 5
          object RL_CAMPO6Lbl: TLabel
            Left = 109
            Top = 5
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 6:'
            FocusControl = RL_CAMPO6
          end
          object RL_CAMPO6: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_CAMPO6'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RL_CAMPO7Pnl: TPanel
          Left = 0
          Top = 138
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 6
          object RL_CAMPO7Lbl: TLabel
            Left = 109
            Top = 5
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 7:'
            FocusControl = RL_CAMPO7
          end
          object RL_CAMPO7: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_CAMPO7'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RL_CAMPO8Pnl: TPanel
          Left = 0
          Top = 161
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 7
          object RL_CAMPO8Lbl: TLabel
            Left = 109
            Top = 5
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 8:'
            FocusControl = RL_CAMPO8
          end
          object RL_CAMPO8: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_CAMPO8'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RL_CAMPO9Pnl: TPanel
          Left = 0
          Top = 184
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 8
          object RL_CAMPO9Lbl: TLabel
            Left = 109
            Top = 5
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 9:'
            FocusControl = RL_CAMPO9
          end
          object RL_CAMPO9: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_CAMPO9'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RL_CAMPO10Pnl: TPanel
          Left = 0
          Top = 207
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 9
          object RL_CAMPO10Lbl: TLabel
            Left = 103
            Top = 5
            Width = 51
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 10:'
            FocusControl = RL_CAMPO10
          end
          object RL_CAMPO10: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_CAMPO10'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RL_CAMPO11Pnl: TPanel
          Left = 0
          Top = 230
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 10
          object RL_CAMPO11Lbl: TLabel
            Left = 103
            Top = 5
            Width = 51
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 11:'
            FocusControl = RL_CAMPO11
          end
          object RL_CAMPO11: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_CAMPO11'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RL_CAMPO12Pnl: TPanel
          Left = 0
          Top = 253
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 11
          object RL_CAMPO12Lbl: TLabel
            Left = 103
            Top = 5
            Width = 51
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 12:'
            FocusControl = RL_CAMPO12
          end
          object RL_CAMPO12: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_CAMPO12'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RL_CAMPO13Pnl: TPanel
          Left = 0
          Top = 276
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 12
          object RL_CAMPO13Lbl: TLabel
            Left = 103
            Top = 5
            Width = 51
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 13:'
            FocusControl = RL_CAMPO13
          end
          object RL_CAMPO13: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_CAMPO13'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RL_CAMPO14Pnl: TPanel
          Left = 0
          Top = 299
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 13
          object RL_CAMPO14Lbl: TLabel
            Left = 103
            Top = 5
            Width = 51
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 14:'
            FocusControl = RL_CAMPO14
          end
          object RL_CAMPO14: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_CAMPO14'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RL_CAMPO15Pnl: TPanel
          Left = 0
          Top = 322
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 14
          object RL_CAMPO15Lbl: TLabel
            Left = 103
            Top = 5
            Width = 51
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 15:'
            FocusControl = RL_CAMPO15
          end
          object RL_CAMPO15: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_CAMPO15'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RL_CAMPO16Pnl: TPanel
          Left = 0
          Top = 345
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 15
          object RL_CAMPO16Lbl: TLabel
            Left = 103
            Top = 5
            Width = 51
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 16:'
            FocusControl = RL_CAMPO16
          end
          object RL_CAMPO16: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_CAMPO16'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RL_CAMPO17Pnl: TPanel
          Left = 0
          Top = 368
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 16
          object RL_CAMPO17Lbl: TLabel
            Left = 103
            Top = 5
            Width = 51
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 17:'
            FocusControl = RL_CAMPO17
          end
          object RL_CAMPO17: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_CAMPO17'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RL_CAMPO18Pnl: TPanel
          Left = 0
          Top = 391
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 17
          object RL_CAMPO18Lbl: TLabel
            Left = 103
            Top = 5
            Width = 51
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 18:'
            FocusControl = RL_CAMPO18
          end
          object RL_CAMPO18: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_CAMPO18'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RL_CAMPO19Pnl: TPanel
          Left = 0
          Top = 414
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 18
          object RL_CAMPO19Lbl: TLabel
            Left = 103
            Top = 5
            Width = 51
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 19:'
            FocusControl = RL_CAMPO19
          end
          object RL_CAMPO19: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_CAMPO19'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RL_CAMPO20Pnl: TPanel
          Left = 0
          Top = 437
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 19
          object RL_CAMPO20Lbl: TLabel
            Left = 103
            Top = 5
            Width = 51
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 20:'
            FocusControl = RL_CAMPO20
          end
          object RL_CAMPO20: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_CAMPO20'
            DataSource = DataSource
            TabOrder = 0
          end
        end
      end
      object Verificacion: TTabSheet
        Caption = 'Resultados de Verificaci'#243'n'
        ImageIndex = 1
        object RLV_CAMPO1Pnl: TPanel
          Left = 0
          Top = 0
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object RLV_CAMPO1Lbl: TLabel
            Left = 109
            Top = 5
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 1:'
            FocusControl = RLV_CAMPO1
          end
          object RLV_CAMPO1: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_EVAL1'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RLV_CAMPO2Pnl: TPanel
          Left = 0
          Top = 23
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object RLV_CAMPO2Lbl: TLabel
            Left = 109
            Top = 5
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 2:'
            FocusControl = RLV_CAMPO2
          end
          object RLV_CAMPO2: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_EVAL2'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RLV_CAMPO3Pnl: TPanel
          Left = 0
          Top = 46
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          object RLV_CAMPO3Lbl: TLabel
            Left = 109
            Top = 5
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 3:'
            FocusControl = RLV_CAMPO3
          end
          object RLV_CAMPO3: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_EVAL3'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RLV_CAMPO4Pnl: TPanel
          Left = 0
          Top = 69
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 3
          object RLV_CAMPO4Lbl: TLabel
            Left = 109
            Top = 5
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 4:'
            FocusControl = RLV_CAMPO4
          end
          object RLV_CAMPO4: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_EVAL4'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RLV_CAMPO5Pnl: TPanel
          Left = 0
          Top = 92
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 4
          object RLV_CAMPO5Lbl: TLabel
            Left = 109
            Top = 5
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 5:'
            FocusControl = RLV_CAMPO5
          end
          object RLV_CAMPO5: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_EVAL5'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RLV_CAMPO6Pnl: TPanel
          Left = 0
          Top = 115
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 5
          object RLV_CAMPO6Lbl: TLabel
            Left = 109
            Top = 5
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 6:'
            FocusControl = RLV_CAMPO6
          end
          object RLV_CAMPO6: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_EVAL6'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RLV_CAMPO7Pnl: TPanel
          Left = 0
          Top = 138
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 6
          object RLV_CAMPO7Lbl: TLabel
            Left = 109
            Top = 5
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 7:'
            FocusControl = RLV_CAMPO7
          end
          object RLV_CAMPO7: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_EVAL7'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RLV_CAMPO8Pnl: TPanel
          Left = 0
          Top = 161
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 7
          object RLV_CAMPO8Lbl: TLabel
            Left = 109
            Top = 5
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 8:'
            FocusControl = RLV_CAMPO8
          end
          object RLV_CAMPO8: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_EVAL8'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RLV_CAMPO9Pnl: TPanel
          Left = 0
          Top = 184
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 8
          object RLV_CAMPO9Lbl: TLabel
            Left = 109
            Top = 5
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 9:'
            FocusControl = RLV_CAMPO9
          end
          object RLV_CAMPO9: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_EVAL9'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RLV_CAMPO10Pnl: TPanel
          Left = 0
          Top = 207
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 9
          object RLV_CAMPO10Lbl: TLabel
            Left = 103
            Top = 5
            Width = 51
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 10:'
            FocusControl = RLV_CAMPO10
          end
          object RLV_CAMPO10: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_EVAL10'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RLV_CAMPO11Pnl: TPanel
          Left = 0
          Top = 230
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 10
          object RLV_CAMPO11Lbl: TLabel
            Left = 103
            Top = 5
            Width = 51
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 11:'
            FocusControl = RLV_CAMPO11
          end
          object RLV_CAMPO11: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_EVAL11'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RLV_CAMPO12Pnl: TPanel
          Left = 0
          Top = 253
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 11
          object RLV_CAMPO12Lbl: TLabel
            Left = 103
            Top = 5
            Width = 51
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 12:'
            FocusControl = RLV_CAMPO12
          end
          object RLV_CAMPO12: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_EVAL12'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RLV_CAMPO13Pnl: TPanel
          Left = 0
          Top = 276
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 12
          object RLV_CAMPO13Lbl: TLabel
            Left = 103
            Top = 5
            Width = 51
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 13:'
            FocusControl = RLV_CAMPO13
          end
          object RLV_CAMPO13: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_EVAL13'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RLV_CAMPO14Pnl: TPanel
          Left = 0
          Top = 299
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 13
          object RLV_CAMPO14Lbl: TLabel
            Left = 103
            Top = 5
            Width = 51
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 14:'
            FocusControl = RLV_CAMPO14
          end
          object RLV_CAMPO14: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_EVAL14'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RLV_CAMPO15Pnl: TPanel
          Left = 0
          Top = 322
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 14
          object RLV_CAMPO15Lbl: TLabel
            Left = 103
            Top = 5
            Width = 51
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 15:'
            FocusControl = RLV_CAMPO15
          end
          object RLV_CAMPO15: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_EVAL15'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RLV_CAMPO16Pnl: TPanel
          Left = 0
          Top = 345
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 15
          object RLV_CAMPO16Lbl: TLabel
            Left = 103
            Top = 5
            Width = 51
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 16:'
            FocusControl = RLV_CAMPO16
          end
          object RLV_CAMPO16: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_EVAL16'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RLV_CAMPO17Pnl: TPanel
          Left = 0
          Top = 368
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 16
          object RLV_CAMPO17Lbl: TLabel
            Left = 103
            Top = 5
            Width = 51
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 17:'
            FocusControl = RLV_CAMPO17
          end
          object RLV_CAMPO17: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_EVAL17'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RLV_CAMPO18Pnl: TPanel
          Left = 0
          Top = 391
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 17
          object RLV_CAMPO18Lbl: TLabel
            Left = 103
            Top = 5
            Width = 51
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 18:'
            FocusControl = RLV_CAMPO18
          end
          object RLV_CAMPO18: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_EVAL18'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RLV_CAMPO19Pnl: TPanel
          Left = 0
          Top = 414
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 18
          object RLV_CAMPO19Lbl: TLabel
            Left = 103
            Top = 5
            Width = 51
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 19:'
            FocusControl = RLV_CAMPO19
          end
          object RLV_CAMPO19: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_EVAL19'
            DataSource = DataSource
            TabOrder = 0
          end
        end
        object RLV_CAMPO20Pnl: TPanel
          Left = 0
          Top = 437
          Width = 464
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 19
          object RLV_CAMPO20Lbl: TLabel
            Left = 103
            Top = 5
            Width = 51
            Height = 13
            Alignment = taRightJustify
            Caption = 'Campo 20:'
            FocusControl = RLV_CAMPO20
          end
          object RLV_CAMPO20: TDBEdit
            Left = 158
            Top = 2
            Width = 300
            Height = 21
            DataField = 'RL_EVAL20'
            DataSource = DataSource
            TabOrder = 0
          end
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 364
    Top = 25
  end
end
