unit DCliente;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, DBClient,
     DBasicoCliente,
     {$ifdef DOS_CAPAS}
     DServerSelReportes,
     DServerReportes,
     DServerSistema,
//     DServerCatalogos,
     DServerSeleccion,
     DServerConsultas,
     DServerGlobalSeleccion,
     {$endif}
     {$ifndef VER130}
     Variants,
     {$endif}     
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaClientDataSet;

type
  TdmCliente = class(TBasicoCliente)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FEmpresaTress: Variant;
    function GetEmpresaTress: Variant;
    procedure SetEmpresaTress( const sCodigo: String );
  protected
{$ifdef DOS_CAPAS}
    FServerGlobal: TdmServerGlobalSeleccion;
    FServerReportes : TdmServerReportes;
    FServerSelReportes: TdmServerSelReportes;
    FServerSeleccion: TdmServerSeleccion;
    FServerSistema: TdmServerSistema;
//    FServerCatalogos: TdmServerCatalogos;
    FServerConsultas: TdmServerConsultas;
{$endif}
    procedure InitCacheModules; override;
  public
    { Public declarations }
{$ifdef DOS_CAPAS}
    property ServerGlobal: TdmServerGlobalSeleccion read FServerGlobal;
    property ServerReportes: TdmServerReportes read FServerReportes;
    property ServerSelReportes: TdmServerSelReportes read FServerSelReportes;
    property ServerSeleccion: TdmServerSeleccion read FServerSeleccion;
    property ServerSistema: TdmServerSistema read FServerSistema;
    //property ServerCatalogos: TdmServerCatalogos read FServerCatalogos;
    property ServerConsultas: TdmServerConsultas read FServerConsultas;
{$endif}
    property EmpresaTress: Variant read GetEmpresaTress;
    procedure CargaActivosTodos(Parametros: TZetaParams);override;
  end;

var
  dmCliente: TdmCliente;

implementation

uses DGlobal,
     DSeleccion,
     DDiccionario,
     ZGlobalTress,
     ZetaCommonTools;

{$R *.DFM}

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     inherited;
     TipoCompany := tcRecluta;
     ModoSeleccion := TRUE;
{$ifdef DOS_CAPAS}
     FServerGlobal := TdmServerGlobalSeleccion.Create( Self );
     FServerReportes := TdmServerReportes.Create( self );
     FServerSelReportes := TdmServerSelReportes.Create( self );
     FServerSeleccion := TdmServerSeleccion.Create( self );
     FServerSistema := TdmServerSistema.Create( Self );
//     FServerCatalogos := TdmServerCatalogos.Create( Self );
     FServerConsultas := TdmServerConsultas.Create( self );
{$endif}
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
     inherited;
{$ifdef DOS_CAPAS}
     FreeAndNil( FServerConsultas );
//     FreeAndNil( FServerCatalogos );
     FreeAndNil( FServerSistema );
     FreeAndNil( FServerSeleccion );
     FreeAndNil( FServerSelReportes );
     FreeAndNil( FServerReportes );
     FreeAndNil( FServerGlobal );
{$endif}
end;

procedure TdmCliente.InitCacheModules;
begin
     DataCache.AddObject( 'Selecci�n', dmSeleccion );
     DataCache.AddObject( 'Diccionario', dmDiccionario );
end;

procedure TdmCliente.CargaActivosTodos(Parametros: TZetaParams);
begin
     with Parametros do
     begin
          AddDate('FechaAsistencia', FechaDefault );
          AddDate('FechaDefault', FechaDefault );
          AddInteger('YearDefault', TheYear(FechaDefault) );
          AddInteger( 'EmpleadoActivo', 0 );
          AddString( 'NombreUsuario', GetDatosUsuarioActivo.Nombre );
          AddString( 'CodigoEmpresa', GetDatosEmpresaActiva.Codigo );
     end;
end;

procedure TdmCliente.SetEmpresaTress( const sCodigo: String );
var
   oDataSet: TZetaClientDataSet;
begin
     oDataSet := TZetaClientDataSet.Create( self );
     try
        with oDataSet do
        begin
             Data := Servidor.GetCompanys( 0, Ord( tc3Datos ) );   // No validar derechos de acceso
             if Locate( 'CM_CODIGO', sCodigo, [] ) then
                FEmpresaTress := BuildEmpresa( oDataSet )
             else
                DataBaseError( 'No Se Pudo Localizar Base De Datos De Tress' + CR_LF +
                               'Verifique La Configuraci�n' );
        end;
     finally
            FreeAndNil( oDataSet );
     end;
end;

function TdmCliente.GetEmpresaTress: Variant;
var
   sCodigo: String;
begin
     sCodigo := Global.GetGlobalString( K_GLOBAL_EMPRESA_TRESS );
     if strLleno( sCodigo ) then
     begin
          if VarIsEmpty( FEmpresaTress ) or ( FEmpresaTress[ P_CODIGO ] <> sCodigo ) then
             SetEmpresaTress( sCodigo );
          Result := FEmpresaTress;
     end
     else
         DataBaseError( 'No Se Tiene Configurado El C�digo de Empresa de Tress' );
end;

end.

