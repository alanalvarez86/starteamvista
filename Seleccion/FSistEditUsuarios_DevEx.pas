unit FSistEditUsuarios_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FSistBaseEditUsuarios, Db, ZetaDBTextBox, StdCtrls, ZetaKeyCombo,
  ZetaKeyLookup, DBCtrls, Mask, ZetaNumero, ComCtrls, ExtCtrls, Buttons,
  ZetaCommonClasses, ZetaSmartLists;

type
  TSistEditUsuarios_DevEx = class(TSistBaseEditUsuarios)
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
  protected
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  public
    { Public declarations }
  end;

var
  SistEditUsuarios_DevEx: TSistEditUsuarios_DevEx;

implementation

uses ZAccesosMgr, DCliente, ZAccesosTress, DSistema;

{$R *.DFM}

{ TSistEditUsuarios }

function TSistEditUsuarios_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := dmSistema.PuedeModificarUsuarioPropio( DataSource.Dataset );
          if (not Result) then
          begin
               sMensaje := 'No Tiene Permiso Para Modificar Registros';
          end;
     end;
end;

procedure TSistEditUsuarios_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     if ( Field = Nil ) then
     begin
          DataSource.AutoEdit := dmSistema.PuedeModificarUsuarioPropio( DataSource.Dataset );
     end;
end;

end.
