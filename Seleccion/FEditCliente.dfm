inherited EditCliente: TEditCliente
  Left = 245
  Top = 183
  Caption = 'Clientes'
  ClientHeight = 428
  ClientWidth = 482
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 392
    Width = 482
    inherited OK: TBitBtn
      Left = 314
    end
    inherited Cancelar: TBitBtn
      Left = 399
    end
  end
  inherited PanelSuperior: TPanel
    Width = 482
  end
  inherited PanelIdentifica: TPanel
    Width = 482
    inherited ValorActivo2: TPanel
      Width = 156
    end
  end
  object PanelCodigo: TPanel [3]
    Left = 0
    Top = 51
    Width = 482
    Height = 50
    Align = alTop
    TabOrder = 3
    object CL_CODIGOLbl: TLabel
      Left = 66
      Top = 7
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = CL_CODIGO
    end
    object CL_NOMBRELbl: TLabel
      Left = 62
      Top = 28
      Width = 40
      Height = 13
      Caption = 'Nombre:'
      FocusControl = CL_NOMBRE
    end
    object CL_CODIGO: TZetaDBEdit
      Left = 106
      Top = 4
      Width = 97
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      Text = 'CL_CODIGO'
      ConfirmEdit = True
      DataField = 'CL_CODIGO'
      DataSource = DataSource
    end
    object CL_NOMBRE: TDBEdit
      Left = 106
      Top = 25
      Width = 325
      Height = 21
      DataField = 'CL_NOMBRE'
      DataSource = DataSource
      TabOrder = 1
    end
  end
  object PageControl: TPageControl [4]
    Left = 0
    Top = 101
    Width = 482
    Height = 291
    ActivePage = Generales
    Align = alClient
    TabOrder = 4
    object Generales: TTabSheet
      Caption = 'Generales'
      object CL_DESCRIPLbl: TLabel
        Left = 44
        Top = 32
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n:'
        FocusControl = CL_DESCRIP
      end
      object CL_INDUSTRLbl: TLabel
        Left = 60
        Top = 128
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Industria:'
        FocusControl = CL_INDUSTR
      end
      object CL_EMPLEADLbl: TLabel
        Left = 48
        Top = 152
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = 'Empleados:'
        FocusControl = CL_EMPLEAD
      end
      object CL_DIRECCLbl: TLabel
        Left = 55
        Top = 56
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = 'Direcci'#243'n:'
        FocusControl = CL_DIRECC
      end
      object CL_TELLbl: TLabel
        Left = 58
        Top = 80
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tel'#233'fono:'
        FocusControl = CL_TEL
      end
      object CL_FAXLbl: TLabel
        Left = 83
        Top = 104
        Width = 20
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fax:'
        FocusControl = CL_FAX
      end
      object CL_DESCRIP: TDBEdit
        Left = 106
        Top = 29
        Width = 325
        Height = 21
        DataField = 'CL_DESCRIP'
        DataSource = DataSource
        TabOrder = 0
      end
      object CL_INDUSTR: TDBEdit
        Left = 106
        Top = 125
        Width = 200
        Height = 21
        DataField = 'CL_INDUSTR'
        DataSource = DataSource
        TabOrder = 4
      end
      object CL_EMPLEAD: TZetaDBNumero
        Left = 106
        Top = 149
        Width = 50
        Height = 21
        Mascara = mnEmpleado
        TabOrder = 5
        DataField = 'CL_EMPLEAD'
        DataSource = DataSource
      end
      object CL_DIRECC: TDBEdit
        Left = 106
        Top = 53
        Width = 325
        Height = 21
        DataField = 'CL_DIRECC'
        DataSource = DataSource
        TabOrder = 1
      end
      object CL_TEL: TDBEdit
        Left = 106
        Top = 77
        Width = 200
        Height = 21
        DataField = 'CL_TEL'
        DataSource = DataSource
        TabOrder = 2
      end
      object CL_FAX: TDBEdit
        Left = 106
        Top = 101
        Width = 200
        Height = 21
        DataField = 'CL_FAX'
        DataSource = DataSource
        TabOrder = 3
      end
      object GroupBox3: TGroupBox
        Left = 8
        Top = 184
        Width = 457
        Height = 73
        Caption = 'Quien Autoriza'
        TabOrder = 6
        object CL_NOM_AULbl: TLabel
          Left = 54
          Top = 16
          Width = 40
          Height = 13
          Caption = 'Nombre:'
        end
        object CL_PUES_AULbl: TLabel
          Left = 58
          Top = 40
          Width = 36
          Height = 13
          Caption = 'Puesto:'
        end
        object CL_NOM_AU: TDBEdit
          Left = 98
          Top = 16
          Width = 325
          Height = 21
          DataField = 'CL_NOM_AU'
          DataSource = DataSource
          TabOrder = 0
        end
        object CL_PUES_AU: TDBEdit
          Left = 98
          Top = 40
          Width = 325
          Height = 21
          DataField = 'CL_PUES_AU'
          DataSource = DataSource
          TabOrder = 1
        end
      end
    end
    object Contactos: TTabSheet
      Caption = 'Contactos'
      ImageIndex = 1
      object GroupBox1: TGroupBox
        Left = 36
        Top = 5
        Width = 401
        Height = 125
        Caption = ' Contacto #1 '
        TabOrder = 0
        object Label1: TLabel
          Left = 17
          Top = 24
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
          FocusControl = CL_NOM_C1
        end
        object Label4: TLabel
          Left = 26
          Top = 96
          Width = 31
          Height = 13
          Alignment = taRightJustify
          Caption = 'Notas:'
          FocusControl = CL_DESC_C1
        end
        object Label3: TLabel
          Left = 29
          Top = 72
          Width = 28
          Height = 13
          Alignment = taRightJustify
          Caption = 'Email:'
          FocusControl = CL_MAIL_C1
        end
        object Label2: TLabel
          Left = 21
          Top = 48
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Puesto:'
          FocusControl = CL_PUES_C1
        end
        object CL_NOM_C1: TDBEdit
          Left = 60
          Top = 21
          Width = 325
          Height = 21
          DataField = 'CL_NOM_C1'
          DataSource = DataSource
          TabOrder = 0
        end
        object CL_PUES_C1: TDBEdit
          Left = 60
          Top = 45
          Width = 200
          Height = 21
          DataField = 'CL_PUES_C1'
          DataSource = DataSource
          TabOrder = 1
        end
        object CL_MAIL_C1: TDBEdit
          Left = 60
          Top = 69
          Width = 325
          Height = 21
          DataField = 'CL_MAIL_C1'
          DataSource = DataSource
          TabOrder = 2
        end
        object CL_DESC_C1: TDBEdit
          Left = 60
          Top = 93
          Width = 325
          Height = 21
          DataField = 'CL_DESC_C1'
          DataSource = DataSource
          TabOrder = 3
        end
      end
      object GroupBox2: TGroupBox
        Left = 36
        Top = 132
        Width = 401
        Height = 125
        Caption = ' Contacto #2 '
        TabOrder = 1
        object Label5: TLabel
          Left = 17
          Top = 24
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
          FocusControl = CL_NOM_C2
        end
        object Label6: TLabel
          Left = 26
          Top = 96
          Width = 31
          Height = 13
          Alignment = taRightJustify
          Caption = 'Notas:'
          FocusControl = CL_DESC_C2
        end
        object Label7: TLabel
          Left = 29
          Top = 72
          Width = 28
          Height = 13
          Alignment = taRightJustify
          Caption = 'Email:'
          FocusControl = CL_MAIL_C2
        end
        object Label8: TLabel
          Left = 21
          Top = 48
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Puesto:'
          FocusControl = CL_PUES_C2
        end
        object CL_NOM_C2: TDBEdit
          Left = 60
          Top = 21
          Width = 325
          Height = 21
          DataField = 'CL_NOM_C2'
          DataSource = DataSource
          TabOrder = 0
        end
        object CL_PUES_C2: TDBEdit
          Left = 60
          Top = 45
          Width = 200
          Height = 21
          DataField = 'CL_PUES_C2'
          DataSource = DataSource
          TabOrder = 1
        end
        object CL_MAIL_C2: TDBEdit
          Left = 60
          Top = 69
          Width = 325
          Height = 21
          DataField = 'CL_MAIL_C2'
          DataSource = DataSource
          TabOrder = 2
        end
        object CL_DESC_C2: TDBEdit
          Left = 60
          Top = 93
          Width = 325
          Height = 21
          DataField = 'CL_DESC_C2'
          DataSource = DataSource
          TabOrder = 3
        end
      end
    end
    object Notas: TTabSheet
      Caption = 'Notas'
      ImageIndex = 2
      object CL_OBSERVA: TDBMemo
        Left = 0
        Top = 0
        Width = 474
        Height = 263
        Align = alClient
        DataField = 'CL_OBSERVA'
        DataSource = DataSource
        TabOrder = 0
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 420
  end
end
