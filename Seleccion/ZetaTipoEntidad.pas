unit ZetaTipoEntidad;

interface

{.$DEFINE MULTIPLES_ENTIDADES}
{$DEFINE CAMBIO_TNOM}

uses Classes;
type
    { Tipo Entidades}
    TipoEntidad = ( enNinguno,              {0}
                    enRequisicion,          {1}
                    enSolicitud,            {2}
                    enCandidato,            {3}
                    enEntrevista,           {4}
                    enExamen,               {5}
                    enCliente,              {6}
                    enCondiciones,          {7}
                    enPuesto,               {8}
                    enAreaInteres,          {9}
                    enGasto,                {10}
                    enTipoGasto,            {11}
                    enTablaOpcion1,         {12}
                    enTablaOpcion2,         {13}
                    enTablaOpcion3,         {14}
                    enTablaOpcion4,         {15}
                    enTablaOpcion5,         {16}
                    enFunciones,            {17}
                    enFormula,              {18}
                    enUsuarios,             {19}
                    enCompanys,             {20}
                    enReporte,              {21}
                    enCampoRep,             {22}
                    enBitacora,             {23}
                    enProceso,              {24}
                    enGlobal,               {25}
                    enDiccion,              {26}
                    enReferenciasLaborales, {27}
                    enDocumentos,           {28}
                    enMisReportes,          {29}
                    enSuscrip               {30}
                    );

    {$ifndef MULTIPLES_ENTIDADES}
    ListaEntidades = set of TipoEntidad;
    {$endif}
const

    EntidadesDescontinuadas{$ifndef MULTIPLES_ENTIDADES}: ListaEntidades {$endif}= [];
    NomParamEntidades{$ifndef MULTIPLES_ENTIDADES}: ListaEntidades {$endif}= [];
    EntidadDefaultCondicion = enSolicitud;
    EntidadConCondiciones{$ifndef MULTIPLES_ENTIDADES}: ListaEntidades {$endif}=[
                     enRequisicion,enSolicitud,enCandidato,
                     enEntrevista,enExamen,enCliente,enCondiciones,enPuesto,
                     enAreaInteres,enGasto,enTipoGasto,enTablaOpcion1,enTablaOpcion2,
                     enTablaOpcion3,enTablaOpcion4,enTablaOpcion5,enFunciones,enFormula,
                     enUsuarios, enCompanys ];
    ReporteEspecial{$ifndef MULTIPLES_ENTIDADES}: ListaEntidades {$endif}= [];
    ReportesSinOrdenDef = [];

    {$ifdef CAMBIO_TNOM}
    { Entidades en las cuales se necesita agregar las fechas de n�mina para evaluar}
    EntidadesNomina = [  ];
    {$endif}


var
   aTipoEntidad: array[ TipoEntidad ] of PChar = (
   'Ninguno',
   'Requisici�n',
   'Solicitud',
   'Candidato',
   'Entrevista',
   'Examen',
   'Cliente',
   'Condiciones',
   'Puesto',
   'Area de Inter�s',
   'Gasto',
   'Tipo de Gasto',
   'Tabla de Opciones 1',
   'Tabla de Opciones 2',
   'Tabla de Opciones 3',
   'Tabla de Opciones 4',
   'Tabla de Opciones 5',
   'Funciones',
   'F�rmula',
   'Cat�logo de Usuarios',
   'Datos B�sicos de Empresa',
   'Reportes',
   'Detalle de Reportes',
   'Bit�cora',
   'Proceso',
   'Globales del Sistema',
   'Diccionario de Datos',
   'Referencias Laborales',
   'Documentos',
   'Mis Reportes Favoritos',
   'Suscripci�n a Reportes'
 );

//function ObtieneEntidad( const Index: TipoEntidad ): String;
procedure LlenaTipoPoliza( Lista: TStrings );
function Dentro( const Entidad: TipoEntidad; ListaEntidades: Array of TipoEntidad ): Boolean;

implementation
uses
    ZetaTipoEntidadTools;

{function ObtieneEntidad( const Index: TipoEntidad ): String;
begin
     Result :=  aTipoEntidad[ Index  ];
end;}

procedure LlenaTipoPoliza( Lista: TStrings );
begin
end;

function Dentro( const Entidad: TipoEntidad; ListaEntidades: Array of TipoEntidad ): Boolean;
begin
     Result := ZetaTipoEntidadTools.Dentro(Entidad,ListaEntidades);
end;

end.
