inherited BuscaSolicitud: TBuscaSolicitud
  Left = 308
  Top = 255
  Caption = 'B'#250'squeda de Solicitudes'
  ClientHeight = 373
  ClientWidth = 612
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 337
    Width = 612
    inherited OK: TBitBtn
      Left = 395
      Width = 125
      Hint = 'Aceptar'
      Caption = '&Agregar Solicitud'
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 529
    end
    object BtnVerSolicitud: TBitBtn
      Left = 14
      Top = 6
      Width = 125
      Height = 24
      Caption = '&Ver Solicitud'
      TabOrder = 2
      OnClick = BtnVerSolicitudClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        5555555555FFFFFFFFFF5555500000000005555557777777777F55550BFBFBFB
        FB0555557F555555557F55500FBFBFBFBF0555577F555555557F550B0BFBFBFB
        FB05557F7F555555557F500F0FBFBFBFBF05577F7F555555557F0B0B0BFBFBFB
        FB057F7F7F555555557F0F0F0FBFBFBFBF057F7F7FFFFFFFFF750B0B00000000
        00557F7F7777777777550F0FB0FBFB0F05557F7FF75FFF7575550B0007000070
        55557F777577775755550FB0FBFB0F0555557FF75FFF75755555000700007055
        5555777577775755555550FBFB0555555555575FFF7555555555570000755555
        5555557777555555555555555555555555555555555555555555}
      NumGlyphs = 2
    end
  end
  object ZetaDBGrid: TZetaDBGrid
    Left = 0
    Top = 145
    Width = 612
    Height = 192
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'SO_FOLIO'
        Title.Alignment = taCenter
        Title.Caption = '#'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRETTYNAME'
        Title.Caption = 'Nombre'
        Width = 170
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SO_SEXO'
        Title.Caption = 'Sexo'
        Width = 30
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'SO_FEC_NAC'
        Title.Alignment = taCenter
        Title.Caption = 'Edad'
        Width = 33
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SO_ESTUDIO'
        Title.Caption = 'Escolaridad'
        Width = 75
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'SO_INGLES'
        Title.Alignment = taCenter
        Title.Caption = 'Ingl'#233's'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SO_AREA_1'
        Title.Caption = 'Area 1'
        Width = 63
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SO_AREA_2'
        Title.Caption = 'Area 2'
        Width = 63
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SO_AREA_3'
        Title.Caption = 'Area 3'
        Width = 63
        Visible = True
      end>
  end
  object PanelFiltro: TPanel
    Left = 0
    Top = 0
    Width = 612
    Height = 145
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object SolStatusLBL: TLabel
      Left = 56
      Top = 6
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = '&Status:'
      FocusControl = SolStatus
    end
    object AreaLBL: TLabel
      Left = 269
      Top = 6
      Width = 75
      Height = 13
      Alignment = taRightJustify
      Caption = 'Area de &Inter'#233's:'
      FocusControl = Area
    end
    object EstudiosLBL: TLabel
      Left = 3
      Top = 72
      Width = 86
      Height = 13
      Alignment = taRightJustify
      Caption = '&Estudios M'#237'nimos:'
      FocusControl = MinEstudios
    end
    object SexoLBL: TLabel
      Left = 62
      Top = 28
      Width = 27
      Height = 13
      Alignment = taRightJustify
      Caption = 'Se&xo:'
      FocusControl = Sexo
    end
    object EdadMinimaLBL: TLabel
      Left = 33
      Top = 50
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'E&dad entre:'
      FocusControl = EdadMinima
    end
    object EdadMaximaLBL: TLabel
      Left = 139
      Top = 50
      Width = 7
      Height = 13
      Caption = '&Y'
      FocusControl = EdadMaxima
    end
    object InglesLBL: TLabel
      Left = 57
      Top = 94
      Width = 31
      Height = 13
      Alignment = taRightJustify
      Caption = 'I&ngl'#233's:'
      FocusControl = Ingles
    end
    object InglesPctLBL: TLabel
      Left = 151
      Top = 94
      Width = 8
      Height = 13
      Caption = '%'
    end
    object ApePatLBL: TLabel
      Left = 264
      Top = 50
      Width = 80
      Height = 13
      Alignment = taRightJustify
      Caption = 'A&pellido Paterno:'
      FocusControl = ApePat
    end
    object NombreLBL: TLabel
      Left = 304
      Top = 93
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'N&ombre:'
      FocusControl = Nombre
    end
    object Label1: TLabel
      Left = 199
      Top = 50
      Width = 24
      Height = 13
      Caption = 'A'#241'os'
      FocusControl = EdadMaxima
    end
    object ApeMatLbl: TLabel
      Left = 262
      Top = 72
      Width = 82
      Height = 13
      Alignment = taRightJustify
      Caption = 'Apellido &Materno:'
      FocusControl = ApeMat
    end
    object CriterioLbl: TLabel
      Left = 276
      Top = 29
      Width = 68
      Height = 13
      Alignment = taRightJustify
      Caption = 'Otros &Criterios:'
      FocusControl = Condicion
    end
    object BotonReset: TSpeedButton
      Left = 3
      Top = 2
      Width = 23
      Height = 22
      Hint = 'Reinicializa Campos de B'#250'squeda'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00500005000555
        555557777F777555F55500000000555055557777777755F75555005500055055
        555577F5777F57555555005550055555555577FF577F5FF55555500550050055
        5555577FF77577FF555555005050110555555577F757777FF555555505099910
        555555FF75777777FF555005550999910555577F5F77777775F5500505509990
        3055577F75F77777575F55005055090B030555775755777575755555555550B0
        B03055555F555757575755550555550B0B335555755555757555555555555550
        BBB35555F55555575F555550555555550BBB55575555555575F5555555555555
        50BB555555555555575F555555555555550B5555555555555575}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = BotonResetClick
    end
    object OtrasHabLBL: TLabel
      Left = 1
      Top = 116
      Width = 86
      Height = 13
      Caption = 'Otras &Habilidades:'
      FocusControl = OtrasHab
    end
    object SolStatus: TComboBox
      Left = 91
      Top = 2
      Width = 135
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      TabStop = False
    end
    object MinEstudios: TComboBox
      Left = 91
      Top = 68
      Width = 135
      Height = 21
      Style = csDropDownList
      DropDownCount = 10
      ItemHeight = 13
      TabOrder = 4
    end
    object Sexo: TComboBox
      Left = 91
      Top = 24
      Width = 135
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
    end
    object ApePat: TEdit
      Left = 346
      Top = 46
      Width = 175
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 9
    end
    object Nombre: TEdit
      Left = 346
      Top = 90
      Width = 175
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 11
    end
    object Refrescar: TBitBtn
      Left = 531
      Top = 60
      Width = 73
      Height = 43
      Hint = 'Buscar Solicitudes De Acuerdo A Los Criterios Especificados'
      Caption = '&Buscar'
      TabOrder = 12
      OnClick = RefrescarClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D808DDDDDDDD
        DDDD83308DDDDDDDDDDD873308DDDDDDDDDDD373308DDDDDDDDDDD3733088888
        8DDDDDD37330000088DDDDDD37338777808DDDDDD038F7F77808DDDDD08FCCCC
        C788DDDCD0F7C7F7C770DDDCC07FCCCCCF70DDDCD0F7CFFCF770DDDDC88FCCCC
        7F88DDDDDD08F7F7F80DDDDDDDD08F7F80DDDDDDDDDD80008DDD}
      Layout = blGlyphTop
    end
    object Area: TZetaKeyLookup
      Left = 346
      Top = 2
      Width = 260
      Height = 21
      LookupDataset = dmSeleccion.cdsAreaInteres
      TabOrder = 7
      TabStop = True
      WidthLlave = 60
    end
    object EdadMinima: TZetaNumero
      Left = 91
      Top = 46
      Width = 40
      Height = 21
      Mascara = mnDias
      MaxLength = 2
      TabOrder = 2
      Text = '0'
      OnExit = EdadExit
    end
    object EdadMaxima: TZetaNumero
      Left = 155
      Top = 46
      Width = 40
      Height = 21
      Mascara = mnDias
      MaxLength = 2
      TabOrder = 3
      Text = '0'
      OnExit = EdadExit
    end
    object Ingles: TZetaNumero
      Left = 91
      Top = 90
      Width = 55
      Height = 21
      Mascara = mnDias
      TabOrder = 5
      Text = '0'
    end
    object ApeMat: TEdit
      Left = 346
      Top = 68
      Width = 175
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 10
    end
    object Condicion: TZetaKeyLookup
      Left = 346
      Top = 24
      Width = 260
      Height = 21
      LookupDataset = dmSeleccion.cdsCondiciones
      TabOrder = 8
      TabStop = True
      WidthLlave = 60
    end
    object OtrasHab: TEdit
      Left = 91
      Top = 112
      Width = 138
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 6
    end
  end
  object DataSource: TDataSource
    OnStateChange = DataSourceStateChange
    Left = 400
    Top = 184
  end
end
