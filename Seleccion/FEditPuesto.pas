unit FEditPuesto;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ComCtrls, Mask,
     ZBaseEdicion,
     ZetaKeyLookup,
     ZetaNumero,
     ZetaKeyCombo,
     ZetaEdit;

type
  TEditPuesto = class(TBaseEdicion)
    PanelCodigo: TPanel;
    PU_CODIGOlbl: TLabel;
    PU_DESCRIPlbl: TLabel;
    PU_CODIGO: TZetaDBEdit;
    PU_DESCRIP: TDBEdit;
    PageControl: TPageControl;
    Perfil: TTabSheet;
    Habilidades: TTabSheet;
    Actividades: TTabSheet;
    Oferta: TTabSheet;
    PU_HABILID: TDBMemo;
    PU_ACTIVID: TDBMemo;
    PU_OFERTA: TDBMemo;
    Adicionales: TTabSheet;
    PU_INGLESlbl: TLabel;
    PU_INGLES: TDBEdit;
    PU_TEXTOlbl: TLabel;
    PU_TEXTO: TDBEdit;
    PU_NUMEROlbl: TLabel;
    PU_NUMERO: TZetaDBNumero;
    PU_AREAlbl: TLabel;
    PU_AREA: TZetaDBKeyLookup;
    Label9: TLabel;
    PU_SEXOlbl: TLabel;
    PU_SEXO: TZetaDBKeyCombo;
    EdadGB: TGroupBox;
    PU_EDADMINlbl: TLabel;
    PU_EDADMIN: TZetaDBNumero;
    EdadAniosLBL: TLabel;
    PU_EDADMAXLBL: TLabel;
    PU_EDADMAX: TZetaDBNumero;
    PU_ESTUDIOlbl: TLabel;
    PU_ESTUDIO: TZetaDBKeyCombo;
    PU_DOMINA1lbl: TLabel;
    PU_DOMINA1: TZetaDBNumero;
    QU_CODIGOlbl: TLabel;
    QU_CODIGO: TZetaDBKeyLookup;
    Panel4: TPanel;
    Label13: TLabel;
    Label14: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label17: TLabel;
    PU_SUELDO: TDBEdit;
    PU_VACACI: TDBEdit;
    PU_BONO: TDBEdit;
    PU_VALES: TDBEdit;
    PU_AGUINAL: TDBEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditPuesto: TEditPuesto;

implementation

{$R *.DFM}

uses ZetaCommonLists,
     ZAccesosTress,
     DSeleccion,
     ZetaCommonClasses;

{ TFormaPuesto }

procedure TEditPuesto.FormCreate(Sender: TObject);
const
     K_LISTA = '%s=%s';
     K_CUALQUIERA = ' ';
     K_CUALQUIERA_STR = '< Cualquiera >';
var
   eSex: eSexo;
begin
     PageControl.ActivePageIndex := 0;
     with PU_SEXO.Lista do
     begin
          BeginUpdate;
          try
             Clear;
             Add( Format( K_LISTA, [ K_CUALQUIERA, K_CUALQUIERA_STR ] ) );
             for eSex := Low( eSexo ) to High( eSexo ) do
             begin
                  Add( Format( K_LISTA, [ ZetaCommonLists.ObtieneElemento( lfSexo, Ord( eSex ) ), ZetaCommonLists.ObtieneElemento( lfSexoDesc, Ord( eSex ) ) ] ) );
             end;
          finally
                 EndUpdate;
          end;
     end;
     PU_ESTUDIO.ListaFija := lfEstudios;
     FirstControl := PU_CODIGO;
     inherited;
     IndexDerechos := D_CAT_PUESTOS;
     HelpContext := H00071_Puestos;
     PU_AREA.LookupDataset := dmSeleccion.cdsAreaInteres;
     QU_CODIGO.LookupDataset := dmSeleccion.cdsCondiciones;
end;

procedure TEditPuesto.Connect;
begin
     with dmSeleccion do
     begin
          cdsAreaInteres.Conectar;
          cdsCondiciones.Conectar;
          Datasource.Dataset := cdsPuestos;
     end;
end;

end.


