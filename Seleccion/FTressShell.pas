unit FTressShell;

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseArbolShell, Menus, ImgList, ActnList, ExtCtrls, ComCtrls, StdCtrls,
  Buttons, ZetaSmartLists, ZetaTipoEntidad, ZetaCommonLists, ZetaClientDataset,
  ZetaMessages,ZetaDespConsulta;

type
  TTressShell = class(TBaseArbolShell)
    _R_Requisicion: TAction;
    _R_Solicitud: TAction;
    Requisicin1: TMenuItem;
    Solicitud1: TMenuItem;
    _P_Depurar: TAction;
    Depuracin1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure _A_CatalogoUsuariosExecute(Sender: TObject);
    procedure _R_RequisicionExecute(Sender: TObject);
    procedure _R_SolicitudExecute(Sender: TObject);
    procedure _P_DepurarExecute(Sender: TObject);
  private
    { Private declarations }
    FOnlySolicita, FOnlyRequiere : Boolean;
    procedure RevisaDerechos;
    procedure ProcesaRegistro(oAction: TAction; oDataSet: TZetaClientDataSet);
    procedure WMWizardEnd(var Message: TMessage); message WM_WIZARD_END;
  protected
    { Protected declarations }
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
    procedure HabilitaControles; override;
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado); override;
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado); override;
    {$endif}
  public
    { Public declarations }
    property OnlySolicita : Boolean read FOnlySolicita write FOnlySolicita;
    property OnlyRequiere : Boolean read FOnlyRequiere write FOnlyRequiere;
    function GetLookUpDataSet(const eEntidad: TipoEntidad): TZetaLookUpDataSet;override;
    function ProcesaDialogo(oLookUp: TZetaLookupDataset; const eEntidad: TipoEntidad; const sFilter: String; var sKey, sDescription: String; var lEncontrado: Boolean): Boolean;override;

    procedure RevisaParams;
    procedure CicloRegistroSolicita;
    procedure CicloRegistroRequiere;
    procedure AbreFormaSimulaciones(const TipoForma: eFormaConsulta);
  end;
const
     K_PANEL_REQUISICION = 1;
     K_PANEL_SOLICITUD = 2;

const
     K_ONLY_SOLICITA = 'SOL';
     K_ONLY_REQUIERE = 'REQ';
var
  TressShell: TTressShell;

implementation

uses DCliente, DGlobal, DReportes, DDiccionario, DSeleccion, DConsultas, DSistema,
     DCatalogos,
     ZAccesosMgr, ZAccesosTress,ZetaDialogo, ZetaCommonClasses, ZetaClientTools,
     FCalendario,
     FBuscaSolicitud,
     FBuscaRequisicion,
     {$ifdef DOS_CAPAS}
     DZetaServerProvider,
     ZetaSQLBroker,
     {$endif}
     ZetaSystemWorking,
     ZGlobalTress;

{$R *.DFM}

procedure TTressShell.FormCreate(Sender: TObject);
begin
{$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
{$endif}
     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( Self );
     dmSistema := TdmSistema.Create( Self );
     dmConsultas := TdmConsultas.Create( Self );
     dmReportes := TdmReportes.Create( Self );
     dmDiccionario := TdmDiccionario.Create( Self );
     dmSeleccion := TdmSeleccion.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     FOnlySolicita := FALSE;
     FOnlyRequiere := FALSE;
     inherited;
     HelpContext := H00003_Explorador;
     FHelpGlosario := 'Seleccion.hlp>Main';
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     inherited;
     dmCatalogos.Free;
     dmSeleccion.Free;
     dmDiccionario.Free;
     dmReportes.Free;
     dmConsultas.Free;
     dmSistema.Free;
     dmCliente.Free;
     Global.Free;
{$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll;
     DZetaServerProvider.FreeAll;
{$endif}
end;

procedure TTressShell.DoOpenAll;
begin
     try
        inherited DoOpenAll;
        Global.Conectar;
        {***US 11757: Proteccion HTTP SkinController = True
        Evita que el backgound de algunos componentes de la aplicacion
        sea de color blanco
        ***}
        DevEx_SkinController.NativeStyle := TRUE;
        CreaArbol( True );
        RevisaDerechos;
        FCalendario.SetDefaultDlgFecha( Date ); { Refresca el Default del Lookup de ZetaFecha }
     except
        on Error : Exception do
        begin
             ZetaDialogo.zError( 'Error al Conectar', 'Empresa: ' + dmCliente.Compania + CR_LF + Error.Message, 0 );
             DoCloseAll;
        end;
     end;
end;

procedure TTressShell.DoCloseAll;
begin
     CierraFormaTodas;
     if EmpresaAbierta then
     begin
          dmCliente.CierraEmpresa;
          CierraDatasets( dmDiccionario );
          CierraDatasets( dmReportes );
          CierraDatasets( dmConsultas );
          CierraDatasets( dmSeleccion );
          CierraDatasets( dmSistema );
          CierraDatasets( dmCatalogos );
     end;
     inherited DoCloseAll;
{$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll;
{$endif}
end;

procedure TTressShell.HabilitaControles;
begin
     inherited HabilitaControles;
     if not EmpresaAbierta then
     begin
          StatusBarMsg( '', K_PANEL_REQUISICION );
          StatusBarMsg( '', K_PANEL_SOLICITUD );
     end;
end;

{$ifdef MULTIPLES_ENTIDADES}
procedure TTressShell.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
{$else}
procedure TTressShell.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
{$endif}
begin
     dmSeleccion.NotifyDataChange( Entidades );
end;

procedure TTressShell.RevisaDerechos;
begin
     Registro.Enabled := ZAccesosMgr.Revisa( D_REGISTRO );
     _R_Requisicion.Enabled := ZAccesosMgr.Revisa( D_REGISTRO_REQUISICION );
     _R_Solicitud.Enabled := ZAccesosMgr.Revisa( D_REGISTRO_SOLICITUD );
     Procesos.Enabled := ZAccesosMgr.Revisa( D_PROCESO );
     _P_Depurar.Enabled := ZAccesosMgr.Revisa( D_PROCESO_DEPURAR );
     ArchivoConfigurarEmpresa.Enabled := ZAccesosMgr.Revisa( D_CAT_CONFI_GLOBALES );
end;

procedure TTressShell.RevisaParams;
begin
     if ( ParamCount > 0 ) then
     begin
          OnlySolicita := ( UpperCase( ParamStr(1) ) = K_ONLY_SOLICITA );
          OnlyRequiere := ( UpperCase( ParamStr(1) ) = K_ONLY_REQUIERE );
     end;
end;

procedure TTressShell.WMWizardEnd(var Message: TMessage);
begin
     with Message do
     begin
          dmSeleccion.HandleProcessEnd( WParam, LParam );
     end;
end;

procedure TTressShell.ProcesaRegistro( oAction: TAction; oDataSet: TZetaClientDataSet );

   procedure ConectaDataSets;
   var
      i, iGlobal: Integer;
   begin
        with dmSeleccion do
        begin
             cdsAreaInteres.Conectar;
             cdsPuestos.Conectar;
             if OnlySolicita then
             begin
                  { Conectar Lookups Adicionales }
                  Global.InitListaUsados;
                  for i := 0 to ( Global.ListaUsados.Count - 1 ) do
                  begin
                       iGlobal := StrToIntDef( Global.ListaUsados.Strings[ i ], 0 );
                       if ( iGlobal > K_GLOBAL_TAB_BASE ) and ( iGlobal <= ( K_GLOBAL_TAB_BASE + K_GLOBAL_TAB_MAX ) ) then
                          GetcdsAdicional( iGlobal ).Conectar;
                  end;
                  FolioSolicitud := 0;     // Para que el refrescar traiga registro vacio
             end
             else
             begin
                  cdsTipoGasto.Conectar;
                  cdsCliente.Conectar;
                  cdsCondiciones.Conectar;
                  FolioRequisicion := 0;   // Para que el refrescar traiga registro vacio
             end;
        end;
   end;

begin
     if EmpresaAbierta then                  // Si no se seleccion� empresa se sale sin hacer nada
     begin
          if ( oAction.Enabled ) then        // Tiene Derechos
          begin
               ZetaSystemWorking.InitAnimation( 'Inicializando Captura...' );
               try
                  ConectaDataSets;           // Se conectan aqu� los lookups para aprovechar la animaci�n
                  oDataSet.Refrescar;        // Refresca para obtener el registro vacio
               finally
                  ZetaSystemWorking.EndAnimation;
               end;
               oDataSet.Agregar;             // Aqu� se presenta la forma de edici�n
          end
          else
              ZetaDialogo.zInformation( Caption, 'No Tiene Permiso Para Agregar Registros', 0 );
          CierraTodo; { Cierra Empresa Activa }
     end;
     Logout;     { Logout al Usuario Activo }
     Application.ProcessMessages;
end;

procedure TTressShell.CicloRegistroSolicita;
begin
     ProcesaRegistro( _R_Solicitud, dmSeleccion.cdsEditSolicita );
end;

procedure TTressShell.CicloRegistroRequiere;
begin
     ProcesaRegistro( _R_Requisicion, dmSeleccion.cdsEditRequiere );
end;

procedure TTressShell._A_CatalogoUsuariosExecute(Sender: TObject);
begin
     inherited;
     AbreFormaConsulta( efcSistUsuarios );
end;

procedure TTressShell._R_RequisicionExecute(Sender: TObject);
begin
     inherited;
     dmSeleccion.cdsRequiere.Agregar;
end;

procedure TTressShell._R_SolicitudExecute(Sender: TObject);
begin
     inherited;
     dmSeleccion.cdsSolicita.Agregar;
end;

procedure TTressShell._P_DepurarExecute(Sender: TObject);
begin
     inherited;
     DSeleccion.ShowWizard( prDepuraSeleccion );
end;

function TTressShell.ProcesaDialogo(oLookUp: TZetaLookupDataset; const eEntidad: TipoEntidad;
         const sFilter: String; var sKey, sDescription: String; var lEncontrado: Boolean): Boolean;
begin
     Result := ( eEntidad in [enSolicitud,enRequisicion] );
     if Result then
     begin
          if eEntidad = enSolicitud then
             lEncontrado := FBuscaSolicitud.BuscaSolicitudDialogo( '', sKey, sDescription )
          else
              lEncontrado := FBuscaRequisicion.BuscaRequisicionDialogo('', sKey, sDescription );
     end;
end;

function TTressShell.GetLookUpDataSet( const eEntidad: TipoEntidad ): TZetaLookUpDataSet;
begin
     case eEntidad of
          enRequisicion : Result := dmSeleccion.cdsLookupRequiere;
          enSolicitud : Result := dmSeleccion.cdsLookUpSolicita;
          enCliente : Result := dmSeleccion.cdsCliente;
          enCondiciones  : Result := dmSeleccion.cdsCondiciones;
          enPuesto :  Result := dmSeleccion.cdsPuestos;
          enAreaInteres : Result := dmSeleccion.cdsAreaInteres;
          enTipoGasto  : Result := dmSeleccion.cdsTipoGasto;
          enTablaOpcion1 : Result := dmSeleccion.cdsAdicional1;
          enTablaOpcion2 : Result := dmSeleccion.cdsAdicional2;
          enTablaOpcion3 : Result := dmSeleccion.cdsAdicional3;
          enTablaOpcion4 : Result := dmSeleccion.cdsAdicional4;
          enTablaOpcion5 : Result := dmSeleccion.cdsAdicional5;
          enUsuarios : Result := dmSistema.cdsUsuarios;
          enCompanys : Result := dmSistema.cdsEmpresas;
          enReporte : Result := dmReportes.cdsLookupReportes
     else
         Result := nil;
     end;
end;
procedure TTressShell.AbreFormaSimulaciones(const TipoForma: eFormaConsulta);
begin
     //AbreFormaConsulta(TipoForma); 
end;

end.



