unit FGlobales;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ImgList, Db, ToolWin, ComCtrls, ExtCtrls, StdCtrls,
     ZBaseGlobal,
     ZBaseConsulta,
     ZetaDBTextBox, ZBaseConsultaBotones;

type
  TSistGlobales = class(TBaseConsulta)
    Identificacion: TToolButton;
    Adicionales: TToolButton;
    Requisitos: TToolButton;
    PanelTitulos: TPanel;
    Label1: TLabel;
    RazonLbl: TZetaDBTextBox;
    Seguridad: TToolButton;
    Referencias: TToolButton;
    ServidorEmail: TToolButton;
    ToolBar: TToolBar;
    ImageList: TImageList;
    EmpresaTress: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure IdentificacionClick(Sender: TObject);
    procedure RequisitosClick(Sender: TObject);
    procedure AdicionalesClick(Sender: TObject);
    procedure SeguridadClick(Sender: TObject);
    procedure ReferenciasClick(Sender: TObject);
    procedure ServidorEmailClick(Sender: TObject);
    procedure EmpresaTressClick(Sender: TObject);
  private
    { Private declarations }
    function AbrirGlobal( GlobalClass: TBaseGlobalClass ): Integer;
    procedure AplicaGlobalDiccion;
  public
    { Public declarations }
    procedure Connect; override;
    procedure Refresh; override;
  end;

var
  SistGlobales: TSistGlobales;

implementation

uses DGlobal,
     ZGlobalTress,
     ZetaCommonClasses,
     FTressShell,
     FGlobalSeguridad,
     FGlobalIdentificacion,
     FGlobalAdicionales,
     FGlobalRequisitos,
     FGlobalReferencias,
     FGlobalServidorEmail,
     FGlobalEmpresaTress;

{$R *.DFM}

procedure TSistGlobales.FormCreate(Sender: TObject);
begin
     inherited;
     EsConsulta := False;
     HelpContext := H00050_Globales_de_empresa;
end;

procedure TSistGlobales.Connect;
begin
     RazonLbl.Caption := Global.GetGlobalString( ZGlobalTress.K_GLOBAL_RAZON_EMPRESA );
end;

procedure TSistGlobales.Refresh;
begin
end;

function TSistGlobales.AbrirGlobal(GlobalClass: TBaseGlobalClass): Integer;
var
   Forma: TBaseGlobal;
begin
     Forma := GlobalClass.Create( Application );
     with Forma do
     begin
          try
             ShowModal;
             Result := LastAction;
          finally
                 Free;
          end;
     end;
end;

procedure TSistGlobales.AplicaGlobalDiccion;
begin
     TressShell.CreaArbol( False );
end;

procedure TSistGlobales.IdentificacionClick(Sender: TObject);
begin
     inherited;
     if ( AbrirGlobal( TGlobalIdentificacion ) = K_EDICION_MODIFICACION ) then
        Connect;
end;

procedure TSistGlobales.RequisitosClick(Sender: TObject);
begin
     inherited;
     if ( AbrirGlobal( TGlobalRequisitos ) = K_EDICION_MODIFICACION ) then
        AplicaGlobalDiccion;
end;

procedure TSistGlobales.SeguridadClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalSeguridad );
end;

procedure TSistGlobales.ReferenciasClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalReferencias );
end;

procedure TSistGlobales.AdicionalesClick(Sender: TObject);
begin
     inherited;
     with TGlobalAdicionales.Create( Self ) do
     begin
          try
             ShowModal;
             if ( ModalResult = mrOk ) then
                AplicaGlobalDiccion;
          finally
             Free;
          end;
     end;
end;

procedure TSistGlobales.ServidorEmailClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalServidorEmail );
end;

procedure TSistGlobales.EmpresaTressClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalEmpresaTress );
end;

end.
