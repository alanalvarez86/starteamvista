const
     K_NUM_GLOBALES = 189;
     K_NUM_DEFAULTS = 0;
     K_TOT_GLOBALES = K_NUM_GLOBALES + K_NUM_DEFAULTS;
     K_BASE_DEFAULTS = 2000;

     {Contantes de indices para los campos adcionales}
     K_INDICE_0                      =  0;
     K_INDICE_1                      =  1;
     K_INDICE_2                      =  2;
     K_INDICE_3                      =  3;
     K_INDICE_4                      =  4;
     K_INDICE_5                      =  5;
     K_INDICE_6                      =  6;
     K_INDICE_7                      =  7;
     K_INDICE_8                      =  8;
     K_INDICE_9                      =  9;
     K_INDICE_10                     = 10;
     K_INDICE_11                     = 11;
     K_INDICE_12                     = 12;
     K_INDICE_13                     = 13;
     K_INDICE_14                     = 14;
     K_INDICE_15                     = 15;
     K_INDICE_16                     = 16;
     K_INDICE_17                     = 17;
     K_INDICE_18                     = 18;
     K_INDICE_19                     = 19;
     K_INDICE_20                     = 20;

     {Constantes para Globales}
     K_GLOBAL_RAZON_EMPRESA            =  1; {Raz�n Social de Empresa}
     K_GLOBAL_CALLE_EMPRESA            =  2; {Calle de Empresa}
     K_GLOBAL_COLONIA_EMPRESA          =  3; {Colonia de Empresa}
     K_GLOBAL_CIUDAD_EMPRESA           =  4; {Ciudad de Empresa}
     K_GLOBAL_ENTIDAD_EMPRESA          =  5; {Entidad de Empresa}
     K_GLOBAL_CP_EMPRESA               =  6; {C�digo Postal Empresa}
     K_GLOBAL_TEL_EMPRESA              =  7; {Tel�fono de Empresa}
     K_GLOBAL_RFC_EMPRESA              =  8; {R.F.C. de Empresa}
     K_GLOBAL_INFONAVIT_EMPRESA        =  9; { # de INFONAVIT de Empresa}
     K_GLOBAL_REPRESENTANTE            = 10; {Representante Legal}

     {Globales Generales}
     K_GLOBAL_TEXT_GLOBAL1             = 11; {Texto Global #1 }
     K_GLOBAL_TEXT_GLOBAL2             = 12; {Texto Global #2}
     K_GLOBAL_TEXT_GLOBAL3             = 13; {Texto Global #3}
     K_GLOBAL_NUM_GLOBAL1              = 14; {N�mero Global #1}
     K_GLOBAL_NUM_GLOBAL2              = 15; {N�mero Global #2}
     K_GLOBAL_NUM_GLOBAL3              = 16; {N�mero Global #3}

     {Campos Adicionales - ORIGINALES}
     K_GLOBAL_TEXTO_BASE               = 16;
     K_GLOBAL_TEXTO_MAX                = 5;
     K_GLOBAL_TEXTO1                   = 17; {Nombre de Texto #1}
     K_GLOBAL_TEXTO2                   = 18; {Nombre de Texto #2}
     K_GLOBAL_TEXTO3                   = 19; {Nombre de Texto #3}
     K_GLOBAL_TEXTO4                   = 20; {Nombre de Texto #4}
     K_GLOBAL_TEXTO5                   = 21; {Nombre de Texto #5}

     K_GLOBAL_NUM_BASE                 = 21;
     K_GLOBAL_NUM_MAX                  = 5;
     K_GLOBAL_NUM1                     = 22; {Nombre de N�mero #1}
     K_GLOBAL_NUM2                     = 23; {Nombre de N�mero #2}
     K_GLOBAL_NUM3                     = 24; {Nombre de N�mero #3}
     K_GLOBAL_NUM4                     = 25; {Nombre de N�mero #4}
     K_GLOBAL_NUM5                     = 26; {Nombre de N�mero #5}
     K_GLOBAL_LOG_BASE                 = 26;
     K_GLOBAL_LOG_MAX                  = 5;
     K_GLOBAL_LOG1                     = 27; {Nombre de L�gico #1}
     K_GLOBAL_LOG2                     = 28; {Nombre de L�gico #2}
     K_GLOBAL_LOG3                     = 29; {Nombre de L�gico #3}
     K_GLOBAL_LOG4                     = 30; {Nombre de L�gico #4}
     K_GLOBAL_LOG5                     = 31; {Nombre de L�gico #5}
     K_GLOBAL_FECHA_BASE               = 31;
     K_GLOBAL_FECHA_MAX                = 5;
     K_GLOBAL_FECHA1                   = 32; {Nombre de Fecha #1}
     K_GLOBAL_FECHA2                   = 33; {Nombre de Fecha #2}
     K_GLOBAL_FECHA3                   = 34; {Nombre de Fecha #3}
     K_GLOBAL_FECHA4                   = 35; {Nombre de Fecha #4}
     K_GLOBAL_FECHA5                   = 36; {Nombre de Fecha #5}
     K_GLOBAL_TAB_BASE                 = 36;
     K_GLOBAL_TAB_MAX                  = 5;
     K_GLOBAL_TAB1                     = 37; {Nombre de Tabla #1}
     K_GLOBAL_TAB2                     = 38; {Nombre de Tabla #2}
     K_GLOBAL_TAB3                     = 39; {Nombre de Tabla #3}
     K_GLOBAL_TAB4                     = 40; {Nombre de Tabla #4}
     K_GLOBAL_TAB5                     = 41; {Nombre de Tabla #5}

     K_GLOBAL_POSICION_ADIC            = 42; {Nombre de Tabla #5}

     K_GLOBAL_PRIMER_ADICIONAL = K_GLOBAL_TEXTO1;
     K_GLOBAL_LAST_ADICIONAL = K_GLOBAL_TAB5;

     K_GLOBAL_REQUISITO1               = 43; { Requisito #1 }
     K_GLOBAL_REQUISITO2               = 44; { Requisito #2 }
     K_GLOBAL_REQUISITO3               = 45; { Requisito #3 }
     K_GLOBAL_REQUISITO4               = 46; { Requisito #4 }
     K_GLOBAL_REQUISITO5               = 47; { Requisito #5 }
     K_GLOBAL_REQUISITO6               = 48; { Requisito #6 }
     K_GLOBAL_REQUISITO7               = 49; { Requisito #7 }
     K_GLOBAL_REQUISITO8               = 50; { Requisito #8 }
     K_GLOBAL_REQUISITO9               = 51; { Requisito #9 }

     K_GLOBAL_DIR_PLANT                = 52; {Directorio Plantillas}

     {Campos Adicionales - Transferencia a TRESS - ORIGINALES}
     K_GLOBAL_CB_TEXTO_BASE            = 52;
     K_GLOBAL_CB_TEXTO_MAX             = 5;
     K_GLOBAL_CB_TEXTO1                = 53; { Campo en Tress de Texto #1}
     K_GLOBAL_CB_TEXTO2                = 54; { Campo en Tress de Texto #2}
     K_GLOBAL_CB_TEXTO3                = 55; { Campo en Tress de Texto #3}
     K_GLOBAL_CB_TEXTO4                = 56; {Campo en Tress de Texto #4}
     K_GLOBAL_CB_TEXTO5                = 57; {Campo en Tress de Texto #5}
     K_GLOBAL_CB_NUM_BASE              = 57;
     K_GLOBAL_CB_NUM_MAX               = 5;
     K_GLOBAL_CB_NUM1                  = 58; {Campo en Tress de N�mero #1}
     K_GLOBAL_CB_NUM2                  = 59; {Campo en Tress de N�mero #2}
     K_GLOBAL_CB_NUM3                  = 60; {Campo en Tress de N�mero #3}
     K_GLOBAL_CB_NUM4                  = 61; {Campo en Tress de N�mero #4}
     K_GLOBAL_CB_NUM5                  = 62; {Campo en Tress de N�mero #5}
     K_GLOBAL_CB_LOG_BASE              = 62;
     K_GLOBAL_CB_LOG_MAX               = 5;
     K_GLOBAL_CB_LOG1                  = 63; {Campo en Tress de L�gico #1}
     K_GLOBAL_CB_LOG2                  = 64; {Campo en Tress de L�gico #2}
     K_GLOBAL_CB_LOG3                  = 65; {Campo en Tress de L�gico #3}
     K_GLOBAL_CB_LOG4                  = 66; {Campo en Tress de L�gico #4}
     K_GLOBAL_CB_LOG5                  = 67; {Campo en Tress de L�gico #5}
     K_GLOBAL_CB_FECHA_BASE            = 67;
     K_GLOBAL_CB_FECHA_MAX             = 5;
     K_GLOBAL_CB_FECHA1                = 68; { Campo en Tress de Fecha #1}
     K_GLOBAL_CB_FECHA2                = 69; { Campo en Tress de Fecha #2}
     K_GLOBAL_CB_FECHA3                = 70; { Campo en Tress de Fecha #3}
     K_GLOBAL_CB_FECHA4                = 71; { Campo en Tress de Fecha #4}
     K_GLOBAL_CB_FECHA5                = 72; { Campo en Tress de Fecha #5}
     K_GLOBAL_CB_TAB_BASE              = 72;
     K_GLOBAL_CB_TAB_MAX               = 5;
     K_GLOBAL_CB_TAB1                  = 73; {Campo en Tress de Tabla #1}
     K_GLOBAL_CB_TAB2                  = 74; {Campo en Tress de Tabla #2}
     K_GLOBAL_CB_TAB3                  = 75; {Campo en Tress de Tabla #3}
     K_GLOBAL_CB_TAB4                  = 76; {Campo en Tress de Tabla #4}
     K_GLOBAL_CB_TAB5                  = 77; {Campo en Tress de Tabla #5}

     K_GLOBAL_VERSION_DATOS            = 78;

     K_TOT_DESCRIPCIONES_GV            = 13;

     K_GLOBAL_REF_LABORAL_QTY          = 79;
     K_GLOBAL_REF_LABORAL_BASE         = 79;
     K_GLOBAL_REF_LABORAL_MAX          = 20;{OP:21.Abr.08}
     K_GLOBAL_REF_LABORAL1             = 80;
     K_GLOBAL_REF_LABORAL2             = 81;
     K_GLOBAL_REF_LABORAL3             = 82;
     K_GLOBAL_REF_LABORAL4             = 83;
     K_GLOBAL_REF_LABORAL5             = 84;
     K_GLOBAL_REF_LABORAL6             = 85;
     K_GLOBAL_REF_LABORAL7             = 86;
     K_GLOBAL_REF_LABORAL8             = 87;
     K_GLOBAL_REF_LABORAL9             = 88;
     K_GLOBAL_REF_LABORAL10            = 89;

     K_GLOBAL_EMAIL_HOST               = 90;
     K_GLOBAL_EMAIL_PORT               = 187;
     K_GLOBAL_EMAIL_USERID             = 91;
     K_GLOBAL_EMAIL_PSWD               = 188;
     K_GLOBAL_EMAIL_FROMADRESS         = 92;
     K_GLOBAL_EMAIL_FROMNAME           = 93;
     K_GLOBAL_EMAIL_ANUNCIO            = 94;
     K_GLOBAL_EMAIL_MSGERROR           = 95;
     K_GLOBAL_EMAIL_AUTH               = 189;

     K_GLOBAL_GRABA_BITACORA           = 96;

     { 09/Feb/2004: Global de Empresa de Tress - Cambio KenWorth}
     K_GLOBAL_EMPRESA_TRESS            = 97;


     { 11/Feb/2004: Lista de Campos Adicionales - Cambio KenWorth }
     K_GLOBAL_TEXTO_BASE2              = 97; {Base de los nuevos campos de texto}
     K_GLOBAL_TEXTO_MAX2               = 15; {Numero de campos nuevos de texto}
     K_GLOBAL_TEXTO6                   = 98; {Nombre de Texto #6}
     K_GLOBAL_TEXTO7                   = 99; {Nombre de Texto #7}
     K_GLOBAL_TEXTO8                   = 100; {Nombre de Texto #8}
     K_GLOBAL_TEXTO9                   = 101; {Nombre de Texto #9}
     K_GLOBAL_TEXTO10                  = 102; {Nombre de Texto #10}
     K_GLOBAL_TEXTO11                  = 103; {Nombre de Texto #11}
     K_GLOBAL_TEXTO12                  = 104; {Nombre de Texto #12}
     K_GLOBAL_TEXTO13                  = 105; {Nombre de Texto #13}
     K_GLOBAL_TEXTO14                  = 106; {Nombre de Texto #14}
     K_GLOBAL_TEXTO15                  = 107; {Nombre de Texto #15}
     K_GLOBAL_TEXTO16                  = 108; {Nombre de Texto #16}
     K_GLOBAL_TEXTO17                  = 109; {Nombre de Texto #17}
     K_GLOBAL_TEXTO18                  = 110; {Nombre de Texto #18}
     K_GLOBAL_TEXTO19                  = 111; {Nombre de Texto #19}
     K_GLOBAL_TEXTO20                  = 112; {Nombre de Texto #20}

     K_GLOBAL_NUM_BASE2                = 112; {Base de los campos nuevos numericos}
     K_GLOBAL_NUM_MAX2                 = 5;   {Numero de campos nuevos numericos}
     K_GLOBAL_NUM6                     = 113; {Nombre de N�mero #6}
     K_GLOBAL_NUM7                     = 114; {Nombre de N�mero #7}
     K_GLOBAL_NUM8                     = 115; {Nombre de N�mero #8}
     K_GLOBAL_NUM9                     = 116; {Nombre de N�mero #9}
     K_GLOBAL_NUM10                    = 117; {Nombre de N�mero #10}

     K_GLOBAL_LOG_BASE2                = 117; {Base de campos nuevos booleanos}
     K_GLOBAL_LOG_MAX2                 = 5;   {Numero de campos nuevos booleanos}
     K_GLOBAL_LOG6                     = 118; {Nombre de L�gico #6}
     K_GLOBAL_LOG7                     = 119; {Nombre de L�gico #7}
     K_GLOBAL_LOG8                     = 120; {Nombre de L�gico #8}
     K_GLOBAL_LOG9                     = 121; {Nombre de L�gico #9}
     K_GLOBAL_LOG10                    = 122; {Nombre de L�gico #10}

     { 11/Feb/2004: Lista de Campos Adicionales - Transferencia a Tress - Cambio KenWorth }

     K_GLOBAL_CB_TEXTO_BASE2           = 122; {Base de campos nuevos texto de tress}
     K_GLOBAL_CB_TEXTO_MAX2            = 15;  {Numero de campos nuevos de texto de tress}
     K_GLOBAL_CB_TEXTO6                = 123; {Campo en Tress de Texto #6}
     K_GLOBAL_CB_TEXTO7                = 124; {Campo en Tress de Texto #7}
     K_GLOBAL_CB_TEXTO8                = 125; {Campo en Tress de Texto #8}
     K_GLOBAL_CB_TEXTO9                = 126; {Campo en Tress de Texto #9}
     K_GLOBAL_CB_TEXTO10               = 127; {Campo en Tress de Texto #10}
     K_GLOBAL_CB_TEXTO11               = 128; {Campo en Tress de Texto #11}
     K_GLOBAL_CB_TEXTO12               = 129; {Campo en Tress de Texto #12}
     K_GLOBAL_CB_TEXTO13               = 130; {Campo en Tress de Texto #13}
     K_GLOBAL_CB_TEXTO14               = 131; {Campo en Tress de Texto #14}
     K_GLOBAL_CB_TEXTO15               = 132; {Campo en Tress de Texto #15}
     K_GLOBAL_CB_TEXTO16               = 133; {Campo en Tress de Texto #16}
     K_GLOBAL_CB_TEXTO17               = 134; {Campo en Tress de Texto #17}
     K_GLOBAL_CB_TEXTO18               = 135; {Campo en Tress de Texto #18}
     K_GLOBAL_CB_TEXTO19               = 136; {Campo en Tress de Texto #19}
     K_GLOBAL_CB_TEXTO20               = 137; {Campo en Tress de Texto #20}

     K_GLOBAL_CB_NUM_BASE2             = 137; {Base de campos nuevos numericos de tress}
     K_GLOBAL_CB_NUM_MAX2              = 5;   {Numero de campos nuevos numericos de tress}
     K_GLOBAL_CB_NUM6                  = 138; {Campo en Tress de N�mero #6}
     K_GLOBAL_CB_NUM7                  = 139; {Campo en Tress de N�mero #7}
     K_GLOBAL_CB_NUM8                  = 140; {Campo en Tress de N�mero #8}
     K_GLOBAL_CB_NUM9                  = 141; {Campo en Tress de N�mero #9}
     K_GLOBAL_CB_NUM10                 = 142; {Campo en Tress de N�mero #10}

     K_GLOBAL_CB_LOG_BASE2             = 142; {Base de campos nuevos booleanos}
     K_GLOBAL_CB_LOG_MAX2              = 5;   {Numero de campos nuevos booleanos}
     K_GLOBAL_CB_LOG6                  = 143; {Campo en Tress de L�gico #6}
     K_GLOBAL_CB_LOG7                  = 144; {Campo en Tress de L�gico #7}
     K_GLOBAL_CB_LOG8                  = 145; {Campo en Tress de L�gico #8}
     K_GLOBAL_CB_LOG9                  = 146; {Campo en Tress de L�gico #9}
     K_GLOBAL_CB_LOG10                 = 147; {Campo en Tress de L�gico #10}

     K_GLOBAL_TPO_REQUISITO1           = 148; { Tipo Requisito #1 }{OP:16.Abr.08}
     K_GLOBAL_TPO_REQUISITO2           = 149; { Tipo Requisito #2 }{OP:16.Abr.08}
     K_GLOBAL_TPO_REQUISITO3           = 150; { Tipo Requisito #3 }{OP:16.Abr.08}
     K_GLOBAL_TPO_REQUISITO4           = 151; { Tipo Requisito #4 }{OP:16.Abr.08}
     K_GLOBAL_TPO_REQUISITO5           = 152; { Tipo Requisito #5 }{OP:16.Abr.08}
     K_GLOBAL_TPO_REQUISITO6           = 153; { Tipo Requisito #6 }{OP:16.Abr.08}
     K_GLOBAL_TPO_REQUISITO7           = 154; { Tipo Requisito #7 }{OP:16.Abr.08}
     K_GLOBAL_TPO_REQUISITO8           = 155; { Tipo Requisito #8 }{OP:16.Abr.08}
     K_GLOBAL_TPO_REQUISITO9           = 156; { Tipo Requisito #9 }{OP:16.Abr.08}

     K_GLOBAL_REF_LABORAL11            = 157; {Globales de Referencia Laborales}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORAL12            = 158; {Globales de Referencia Laborales}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORAL13            = 159; {Globales de Referencia Laborales}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORAL14            = 160; {Globales de Referencia Laborales}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORAL15            = 161; {Globales de Referencia Laborales}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORAL16            = 162; {Globales de Referencia Laborales}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORAL17            = 163; {Globales de Referencia Laborales}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORAL18            = 164; {Globales de Referencia Laborales}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORAL19            = 165; {Globales de Referencia Laborales}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORAL20            = 166; {Globales de Referencia Laborales}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORALRV1           = 167; {Globales de Referencia Laborales: Resultados de Verificaci�n}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORALRV2           = 168; {Globales de Referencia Laborales: Resultados de Verificaci�n}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORALRV3           = 169; {Globales de Referencia Laborales: Resultados de Verificaci�n}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORALRV4           = 170; {Globales de Referencia Laborales: Resultados de Verificaci�n}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORALRV5           = 171; {Globales de Referencia Laborales: Resultados de Verificaci�n}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORALRV6           = 172; {Globales de Referencia Laborales: Resultados de Verificaci�n}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORALRV7           = 173; {Globales de Referencia Laborales: Resultados de Verificaci�n}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORALRV8           = 174; {Globales de Referencia Laborales: Resultados de Verificaci�n}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORALRV9           = 175; {Globales de Referencia Laborales: Resultados de Verificaci�n}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORALRV10           = 176; {Globales de Referencia Laborales: Resultados de Verificaci�n}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORALRV11           = 177; {Globales de Referencia Laborales: Resultados de Verificaci�n}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORALRV12           = 178; {Globales de Referencia Laborales: Resultados de Verificaci�n}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORALRV13           = 179; {Globales de Referencia Laborales: Resultados de Verificaci�n}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORALRV14           = 180; {Globales de Referencia Laborales: Resultados de Verificaci�n}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORALRV15           = 181; {Globales de Referencia Laborales: Resultados de Verificaci�n}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORALRV16           = 182; {Globales de Referencia Laborales: Resultados de Verificaci�n}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORALRV17           = 183; {Globales de Referencia Laborales: Resultados de Verificaci�n}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORALRV18           = 184; {Globales de Referencia Laborales: Resultados de Verificaci�n}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORALRV19           = 185; {Globales de Referencia Laborales: Resultados de Verificaci�n}{OP:18.Abr.08}
     K_GLOBAL_REF_LABORALRV20           = 186; {Globales de Referencia Laborales: Resultados de Verificaci�n}{OP:18.Abr.08}
     K_GLOBAL_REFV_LABORAL_QTY          = 166;{OP:21.Abr.08}
     K_GLOBAL_REFV_LABORAL_BASE         = 166;{OP:21.Abr.08}
     K_GLOBAL_REFV_LABORAL_MAX          = 20;{OP:21.Abr.08}

     K_MAXIMO_REF_1                     = 10;{OP:28.Abr.08}
     K_GLOBAL_REF_LABORAL_BASE2         = 146;{OP:28.Abr.08}


     {Esta constante se puso nada mas para que compile el proyecto.
     Se utiliza en DQUERIES, pero Seleccion de Personal, nunca va a llegar
     a ejecutar esa linea de codigo.
     El valor de esta constante es DUMMY.}
     K_GLOBAL_PRIMER_DIA = 999;
     K_GLOBAL_DIGITO_EMPRESA = 999;     // Se utiliza en ZFuncsGlobal

     { ��� OJO !!!   CUANDO SE AGREGUE UNA CONSTANTE NUEVA

     1) SE TIENE QUE AGREGAR EN EL METODO DGLOBAL.GETTIPOGLOBAL CUANDO EL GLOBAL NO SEA TEXTO
     2) Se debe aumentar en 1 la constante K_NUM_GLOBALES ( est� al principio de esta unidad )
     3) Se debe considerar la constante en la funci�n GetTipoGlobal ( est� al final de esta unidad )
     }

