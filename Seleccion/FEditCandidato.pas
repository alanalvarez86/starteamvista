unit FEditCandidato;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicionRenglon, StdCtrls, ZetaDBTextBox, Db, Grids, DBGrids,
  ZetaDBGrid, ComCtrls, ExtCtrls, DBCtrls, Buttons, ZetaKeyCombo, Mask;

type
  TEditCandidato = class(TBaseEdicionRenglon)
    Panel3: TPanel;
    Label2: TLabel;
    eNombreCandidato: TZetaTextBox;
    Label3: TLabel;
    ePerfil: TZetaTextBox;
    Label4: TLabel;
    Label5: TLabel;
    BtnSolicitud: TBitBtn;
    Label1: TLabel;
    eRequisicion: TZetaTextBox;
    cbStatus: TZetaDBKeyCombo;
    DBEdit1: TDBEdit;
    procedure FormShow(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtnSolicitudClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    procedure SetTitulos;
  protected
  public
    { Public declarations }
    procedure Connect;override;
  end;

const
     K_ACCESO_ENTREVISTAS = 'No Tiene Permiso Para %s Entrevistas';
var
  EditCandidato: TEditCandidato;

implementation

uses DSeleccion,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZAccesosTress,
     ZAccesosMgr,
     ZetaDialogo;

{$R *.DFM}

procedure TEditCandidato.FormCreate(Sender: TObject);
begin
     inherited;
     GridRenglones.Options := [ dgTitles, dgIndicator, dgColumnResize, dgColLines,
                                dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit ];
     IndexDerechos := D_CANDIDATOS;
     HelpContext := H00053_Requisicion_Candidatos;
     PageControl.Visible := ZAccesosMgr.Revisa( D_CANDIDATOS_ENTREVISTAS );
end;

procedure TEditCandidato.FormShow(Sender: TObject);
begin
     inherited;
     with PageControl do
     begin
          ActivePage := Tabla;
     end;
end;

procedure TEditCandidato.Connect;
begin
     with dmSeleccion do
     begin
          Datasource.DataSet := cdsReqCandidatos;
          // Refrescar Entrevistas
          cdsEntrevista.Refrescar;
          dsRenglon.DataSet:= cdsEntrevista;
          SetTitulos;
     end;
end;

procedure TEditCandidato.SetTitulos;
 function GetSexo( const sSexo: string ): string ;
 begin
      if sSexo = 'F' then
         Result := 'Femenino'
      else
         Result := 'Masculino'
 end;
begin
     with dmSeleccion.cdsEditRequiere do
     begin
          eRequisicion.Caption := Format( '#%d: %d Vacante%s de %s',
                                          [ FieldByName('RQ_FOLIO').AsInteger,
                                            FieldByName('RQ_VACANTE').AsInteger,
                                            '(s)',
                                            FieldByName('PU_DESCRIP').AsString] );
     end;
     with dmSeleccion.cdsReqCandidatos do
     begin
          eNombreCandidato.Caption := Format( '#%d: %s',
                                              [ FieldByName('SO_FOLIO').AsInteger,
                                                FieldByName('PRETTYNAME').AsString] );
          ePerfil.Caption := Format( '%s, %d a�os, %s, %s Ingl�s',
                                     [ GetSexo(FieldByName('SO_SEXO').AsString),
                                       zYearsBetween(FieldByName('SO_FEC_NAC').AsDateTime, Date),
                                       ObtieneElemento(lfEstudios,FieldByName('SO_ESTUDIO').AsInteger),
                                       FieldByName('SO_INGLES').AsString + '%'] );
     end;
end;

procedure TEditCandidato.BBAgregarClick(Sender: TObject);
begin
     if ( ZAccesosMgr.CheckDerecho( D_CANDIDATOS_ENTREVISTAS, K_DERECHO_ALTA ) ) then
        dmSeleccion.cdsEntrevista.Agregar
     else
        ZetaDialogo.zInformation( Caption, Format( K_ACCESO_ENTREVISTAS, [ 'Agregar' ] ), 0 );
end;

procedure TEditCandidato.BBBorrarClick(Sender: TObject);
begin
     if NoData( dsRenglon ) then
        ZetaDialogo.zInformation( Caption, 'No Hay Entrevistas Para Borrar', 0 )
     else if ( ZAccesosMgr.CheckDerecho( D_CANDIDATOS_ENTREVISTAS, K_DERECHO_BAJA ) ) then
        dmSeleccion.cdsEntrevista.Borrar
     else
        ZetaDialogo.zInformation( Caption, Format( K_ACCESO_ENTREVISTAS, [ 'Borrar' ] ), 0 );
end;

procedure TEditCandidato.BBModificarClick(Sender: TObject);
begin
     if NoData( dsRenglon ) then
        ZetaDialogo.zInformation( Caption, 'No Hay Entrevistas Para Modificar', 0 )
     else if ( ZAccesosMgr.CheckDerecho( D_CANDIDATOS_ENTREVISTAS, K_DERECHO_CAMBIO ) ) then
        dmSeleccion.cdsEntrevista.Modificar
     else
        ZetaDialogo.zInformation( Caption, Format( K_ACCESO_ENTREVISTAS, [ 'Modificar' ] ), 0 );
end;

procedure TEditCandidato.BtnSolicitudClick(Sender: TObject);
begin
     if ( ZAccesosMgr.Revisa( D_SOLICITUD_DATOS ) ) then
     begin
          with dmSeleccion do
               ModificaSolicitud( cdsReqCandidatos.FieldByName( 'SO_FOLIO' ).AsInteger, FALSE );
     end
     else
        ZetaDialogo.zInformation( Caption, 'No Tiene Permiso Para Ver Solicitudes', 0 );
end;

procedure TEditCandidato.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = nil ) then
     begin
          dmSeleccion.cdsEntrevista.Refrescar;
          SetTitulos;
     end;
end;

end.
