program TressLabor;

uses
  Forms,
  ZetaClientTools,
  ZBaseShell in '..\..\..\Tools\ZBaseShell.pas' {BaseShell},
  DBaseCliente in '..\..\..\DataModules\DBaseCliente.pas' {BaseCliente: TDataModule},
  ZBaseConsulta in '..\..\..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZBaseDlgModal in '..\..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FBaseReportes in '..\..\..\Reportes\FBaseReportes.pas' {BaseReportes},
  ZReportTools in '..\..\..\Reportes\ZReportTools.pas',
  DGlobal in '..\..\..\DataModules\DGlobal.pas',
  DBaseDiccionario in '..\..\..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  ZetaSQL in '..\..\..\Servidor\ZetaSQL.pas',
  DDiccionario in '..\..\..\Labor\DDiccionario.pas' {dmDiccionario: TDataModule},
  DReportes in '..\..\..\DataModules\DReportes.pas' {dmReportes: TDataModule},
  FEditReportes in '..\..\..\Reportes\FEditReportes.pas',
  FEditBaseReportes in '..\..\..\Reportes\FEditBaseReportes.pas' {EditBaseReportes},
  ZetaSplash in '..\..\..\Tools\ZetaSplash.pas' {SplashScreen},
  DLabor in '..\..\..\Labor\DLabor.pas' {dmLabor: TDataModule},
  ZBaseEdicion in '..\..\..\Tools\ZBaseEdicion.pas' {BaseEdicion},
  DTablas in '..\..\..\Labor\DTablas.pas' {dmTablas: TDataModule},
  ZBaseEdicionRenglon in '..\..\..\Tools\ZBaseEdicionRenglon.pas' {BaseEdicionRenglon},
  ZBaseConsultaBotones in '..\..\..\Tools\ZBaseConsultaBotones.pas' {BaseBotones},
  ZWizardBasico in '..\..\..\Tools\ZWizardBasico.pas' {WizardBasico},
  ZetaDespConsulta in '..\..\..\Labor\ZetaDespConsulta.pas',
  ZArbolTress in '..\..\..\Labor\ZArbolTress.pas',
  ZetaBuscaWorder in '..\..\..\Labor\ZetaBuscaWorder.pas' {BuscaWorder},
  ZBaseWizard in '..\..\..\Tools\ZBaseWizard.pas' {BaseWizard},
  ZBaseWizardFiltro in '..\..\..\Tools\ZBaseWizardFiltro.pas' {BaseWizardFiltro},
  ZBaseThreads in '..\..\..\Tools\ZBaseThreads.pas',
  DConsultas in '..\..\..\DataModules\DConsultas.pas' {dmConsultas: TDataModule},
  ZBaseGlobal in '..\..\..\Tools\ZBaseGlobal.pas' {BaseGlobal},
  ZetaLaborTools in '..\..\..\Labor\ZetaLaborTools.pas',
  DCatalogos in '..\..\..\Labor\DCatalogos.pas' {dmCatalogos: TDataModule},
  DCliente in '..\..\..\Labor\DCliente.pas' {dmCliente: TDataModule},
  DSistema in '..\..\..\Labor\DSistema.pas' {dmSistema: TDataModule},
  ZBaseArbolShell in '..\..\..\Tools\ZBaseArbolShell.pas' {BaseArbolShell},
  DBaseReportes in '..\..\..\DataModules\DBaseReportes.pas' {dmBaseReportes: TDataModule},
  DBasicoCliente in '..\..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DBaseGlobal in '..\..\..\DataModules\dBaseGlobal.pas',
  FTressShell in '..\..\..\Labor\FTressShell.pas' {TressShell},
  DMailMerge in '..\..\..\DataModules\dmailmerge.pas' {dmMailMerge: TDataModule},
  DConfigPoliza in '..\..\..\DataModules\DConfigPoliza.pas' {dmConfigPoliza: TDataModule},
  DExportEngine in '..\..\..\DataModules\DExportEngine.pas' {dmExportEngine: TDataModule},
  dBaseTressDiccionario in '..\..\..\DataModules\dBaseTressDiccionario.pas' {dmBaseTressDiccionario: TDataModule},
  DBaseSistema in '..\..\..\DataModules\DBaseSistema.pas' {dmBaseSistema: TDataModule},
  FLayouts in '..\..\..\Labor\Consulta\FLayouts.pas' {Layouts},
  FEditLayout in '..\..\..\Labor\Edicion\FEditLayout.pas' {EditPlantillas},
  TCClient in 'C:\Program Files\Automated QA\TestComplete\Clients\Delphi&BCB\TCClient.pas';

{$R *.RES}
{$R WindowsXP.res}

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  SplashScreen := TSplashScreen.Create( Application );
  with SplashScreen do
  begin
       Show;
       Update;
  end;
  Application.Title := 'Tress Labor';
  Application.HelpFile := 'LABOR.chm';
  Application.CreateForm(TTressShell, TressShell);
  Application.CreateForm(TLayouts, Layouts);
  Application.CreateForm(TEditPlantillas, EditPlantillas);
  with TressShell do
  begin
       if Login( False ) then
       begin
            CierraSplash;
            Show;
            Update;
            BeforeRun;
            Application.Run;
       end
       else
       begin
            CierraSplash;
            Free;
       end;
  end;

end.
