program TressCajaAhorro;

uses
  Forms,
  ZetaClientTools,
  ZBaseShell in '..\..\..\Tools\ZBaseShell.pas' {BaseShell},
  ZBaseConsulta in '..\..\..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZetaSplash in '..\..\..\Tools\ZetaSplash.pas' {SplashScreen},
  DDiccionario in '..\..\..\CajaAhorro\DDiccionario.pas' {dmDiccionario: TDataModule},
  DGlobal in '..\..\..\DataModules\DGlobal.pas',
  DReportes in '..\..\..\DataModules\DReportes.pas' {dmReportes: TDataModule},
  ZBaseConsultaBotones in '..\..\..\Tools\ZBaseConsultaBotones.pas' {BaseBotones},
  ZBaseDlgModal_DevEx in '..\..\..\Tools\ZBaseDlgModal_DevEx.pas' {ZetaDlgModal_DevEx},
  ZBaseDlgModal in '..\..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  ZBaseEdicion in '..\..\..\Tools\ZBaseEdicion.pas' {BaseEdicion},
  ZBaseEdicion_DevEx in '..\..\..\Tools\ZBaseEdicion_DevEx.pas' {BaseEdicion_DevEx},
  ZBaseGlobal in '..\..\..\Tools\ZBaseGlobal.pas' {BaseGlobal},
  ZBaseEscogeGrid_DevEx in '..\..\..\Tools\ZBaseEscogeGrid_DevEx.pas' {BaseEscogeGrid_DevEx},
  ZBaseEscogeGrid in '..\..\..\Tools\ZBaseEscogeGrid.pas' {BaseEscogeGrid},
  ZWizardBasico in '..\..\..\Tools\ZWizardBasico.pas' {WizardBasico},
  ZBaseGridEdicion in '..\..\..\Tools\ZBaseGridEdicion.pas' {BaseGridEdicion},
  ZBaseWizard in '..\..\..\Tools\ZBaseWizard.pas' {BaseWizard},
  ZBaseWizardFiltro in '..\..\..\Tools\ZBaseWizardFiltro.pas' {BaseWizardFiltro},
  ZBaseSelectGrid in '..\..\..\Tools\ZBaseSelectGrid.pas' {BaseGridSelect},
  FTressShell in '..\..\..\CajaAhorro\FTressShell.pas' {TressShell},
  ZetaWizardFeedBack in '..\..\..\Tools\ZetaWizardFeedBack.pas' {WizardFeedback},
  ZetaBuscaEntero in '..\..\..\Tools\ZetaBuscaEntero.pas' {BuscaEntero},
  DBaseCliente in '..\..\..\DataModules\DBaseCliente.pas' {BaseCliente: TDataModule},
  DBasicoCliente in '..\..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  FBaseReportes in '..\..\..\Reportes\FBaseReportes.pas' {BaseReportes},
  FEditBaseReportes in '..\..\..\Reportes\FEditBaseReportes.pas' {EditBaseReportes},
  FEditReportes in '..\..\..\Reportes\FEditReportes.pas' {EditReportes},
  DBaseDiccionario in '..\..\..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DBaseReportes in '..\..\..\DataModules\DBaseReportes.pas' {dmBaseReportes: TDataModule},
  DBaseGlobal in '..\..\..\DataModules\dBaseGlobal.pas',
  DCajaAhorro in '..\..\..\CajaAhorro\DCajaAhorro.pas' {dmCajaAhorro: TDataModule},
  DCliente in '..\..\..\CajaAhorro\DCliente.pas' {dmCliente: TDataModule},
  FEditCtasBancarias in '..\..\..\CajaAhorro\FEditCtasBancarias.pas' {EditCtasBancarias},
  FRegistroDepositoRetiro_DevEx in '..\..\..\CajaAhorro\FRegistroDepositoRetiro_DevEx.pas' {RegistroDepositoRetiro_DevEx},
  FRegistroDepositoRetiro in '..\..\..\CajaAhorro\FRegistroDepositoRetiro.pas' {RegistroDepositoRetiro},
  FRegistroInscripcion_DevEx in '..\..\..\CajaAhorro\FRegistroInscripcion_DevEx.pas' {RegistroInscripcion_DevEx},
  FRegistroInscripcion in '..\..\..\CajaAhorro\FRegistroInscripcion.pas' {RegistroInscripcion},
  FSaldosEmpleado in '..\..\..\CajaAhorro\FSaldosEmpleado.pas' {SaldosEmpleado},
  ZBaseEdicionRenglon_DevEx in '..\..\..\Tools\ZBaseEdicionRenglon_DevEx.pas' {BaseEdicionRenglon_DevEx},
  ZBaseEdicionRenglon in '..\..\..\Tools\ZBaseEdicionRenglon.pas' {BaseEdicionRenglon},
  DSistema in '..\..\..\DataModules\DSistema.pas' {dmSistema: TDataModule},
  DTablas in '..\..\..\DataModules\dTablas.pas' {dmTablas: TDataModule},
  DCatalogos in '..\..\..\CajaAhorro\DCatalogos.pas' {dmCatalogos: TDataModule},
  ZBaseNavBarShell in '..\..\..\Tools\ZBaseNavBarShell.pas' {BaseNavBarShell},
  ZBaseArbolShell in '..\..\..\Tools\ZBaseArbolShell.pas' {ZBaseArbolShell},
  ZNavBarTools in '..\..\..\Tools\ZNavBarTools.pas',
  DBaseSistema in '..\..\..\DataModules\DBaseSistema.pas' {dmBaseSistema: TDataModule},
  TressMorado2013 in '..\..\..\Skin\TressMorado2013.pas',
  ZWizardBasico_DevEx in '..\..\..\Tools\ZWizardBasico_DevEx.pas' {WizardBasico_DevEx},
  TCClient in 'C:\Program Files\Automated QA\TestComplete\Clients\Delphi&BCB\TCClient.pas';

{$R *.RES}
{$R WindowsXP.res}

{***DevEx (by am): Spanish.RES es el recurso con todas las traducciones de textos para los componentes de
DevExpress. El recruso es controlado por los componentes cxLocalizer que se encuentran en el Shell y en la forma
de la Matriz de habilidades. Puede haber varios cxLocalizer en el proyecto, pero debe haber un solo recurso de traduccion.
Estos recursos son generados desde un proyecto el cual se encuentra en StartTeam (Traducciones\Spanish.ini),. Cuando se
requiera agregar un texto el Spanish.ini debe ser modificado, y el Spanish.RES debe ser construido y cambiado en los folders
de los proyectos.

La siguiente directiva es la que indica que se utilizara este recurso el cual debe estar presente en el folder del proyecto.
***}

{$R ..\..\..\Traducciones\Spanish.RES}

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  Application.Title := 'Tress Caja de Ahorro';
  Application.HelpFile := 'TressCajadeAhorro.chm';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       //Inicializamos la varibale de maximizacion del Shell
       if Login_DevEx ( False ) then
       begin
            Show;
            Update;
            WindowState := wsMaximized; //Maximizar despues del update para que tome en cuenta que se escondio el Menu
            BeforeRun;
            Application.Run;
       end;
       Free;
  end;
end.
