program Supervisores;

uses
  Forms,
  ZetaClientTools,
  ZBaseShell in '..\..\..\Tools\ZBaseShell.pas' {BaseShell},
  FTressShell in '..\..\..\Super\FTressShell.pas' {TressShell},
  DBaseCliente in '..\..\..\DataModules\DBaseCliente.pas' {BaseCliente: TDataModule},
  DCliente in '..\..\..\Super\DCliente.pas' {dmCliente: TDataModule},
  DSuper in '..\..\..\Super\DSuper.pas' {dmSuper: TDataModule},
  ZBaseDlgModal in '..\..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  DCatalogos in '..\..\..\Super\DCatalogos.pas' {dmCatalogos: TDataModule},
  DTablas in '..\..\..\Super\DTablas.pas' {dmTablas: TDataModule},
  DBaseDiccionario in '..\..\..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DDiccionario in '..\..\..\Super\DDiccionario.pas' {dmDiccionario: TDataModule},
  ZBaseConsulta in '..\..\..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZBaseEdicion in '..\..\..\Tools\ZBaseEdicion.pas' {BaseEdicion},
  FKardexBase in '..\..\..\Empleados\FKardexBase.pas' {KardexBase},
  ZBaseEdicionRenglon in '..\..\..\Tools\ZBaseEdicionRenglon.pas' {BaseEdicionRenglon},
  ZWizardBasico in '..\..\..\Tools\ZWizardBasico.pas' {WizardBasico},
  ZBaseWizard in '..\..\..\Tools\ZBaseWizard.pas' {BaseWizard},
  ZBaseWizardFiltro in '..\..\..\Tools\ZBaseWizardFiltro.pas' {BaseWizardFiltro},
  ZBaseGridEdicion in '..\..\..\Tools\ZBaseGridEdicion.pas' {BaseGridEdicion},
  ZBaseGridEdicion_DevEx in '..\..\..\Tools\ZBaseGridEdicion_DevEx.pas' {BaseGridEdicion_DevEx},
  DConsultas in '..\..\..\DataModules\DConsultas.pas' {dmConsultas: TDataModule},
  DGlobal in '..\..\..\DataModules\DGlobal.pas',
  DReportes in '..\..\..\DataModules\DReportes.pas' {dmReportes: TDataModule},
  DLabor in '..\..\..\Super\DLabor.pas' {dmLabor: TDataModule},
  FBaseCedulas in '..\..\..\Labor\Edicion\FBaseCedulas.pas' {BaseCedulas},
  DBasicoCliente in '..\..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DBaseGlobal in '..\..\..\DataModules\dBaseGlobal.pas',
  DBaseReportes in '..\..\..\DataModules\DBaseReportes.pas' {dmBaseReportes: TDataModule},
  DMailMerge in '..\..\..\DataModules\dmailmerge.pas' {dmMailMerge: TDataModule},
  DConfigPoliza in '..\..\..\DataModules\DConfigPoliza.pas' {dmConfigPoliza: TDataModule},
  DExportEngine in '..\..\..\DataModules\DExportEngine.pas' {dmExportEngine: TDataModule},
  FCalendarioRitmo in '..\..\..\Catalogos\FCalendarioRitmo.pas' {CalendarioRitmo},
  FCalendarioRitmo_DevEx in '..\..\..\Catalogos\FCalendarioRitmo_DevEx.pas' {CalendarioRitmo},
  FEmpleadoGridSelect in '..\..\..\Wizards\FEmpleadoGridSelect.pas',
  FKardexDiario in '..\..\..\Super\FKardexDiario.pas' {KardexDiario},
  dBaseTressDiccionario in '..\..\..\datamodules\dBaseTressDiccionario.pas',
  TCClient in 'C:\Program Files\Automated QA\TestComplete\Clients\Delphi&BCB\TCClient.pas',
  ZBaseSelectGrid_DevEx in '..\..\..\Tools\ZBaseSelectGrid_DevEx.pas',
  ZBasicoSelectGrid_DevEx in '..\..\..\Tools\ZBasicoSelectGrid_DevEx.pas',
  ZBaseDlgModal_DevEx in '..\..\..\Tools\ZBaseDlgModal_DevEx.pas' {ZetaDlgModal_DevEx},
  FEmpleadoGridSelect_DevEx in '..\..\..\Wizards\FEmpleadoGridSelect_DevEx.pas';

{$R *.RES}
{$R WindowsXP.res}

//{$R Languages.RES}

{***DevEx (by am): Spanish.RES es el recurso con todas las traducciones de textos para los componentes de
DevExpress. El recruso es controlado por los componentes cxLocalizer que se encuentran en el Shell y en la forma
de la Matriz de habilidades. Puede haber varios cxLocalizer en el proyecto, pero debe haber un solo recurso de traduccion.
Estos recursos son generados desde un proyecto el cual se encuentra en StartTeam (Traducciones\Spanish.ini),. Cuando se
requiera agregar un texto el Spanish.ini debe ser modificado, y el Spanish.RES debe ser construido y cambiado en los folders
de los proyectos.

La siguiente directiva es la que indica que se utilizara este recurso el cual debe estar presente en el folder del proyecto.
***}

{$R ..\..\..\Traducciones\Spanish.RES}

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  Application.HelpFile := 'SuperWin.chm';
  Application.Title := 'Tress - Supervisores';
  Application.CreateForm(TTressShell, TressShell);
  if TressShell.Inicializa( True ) then
     Application.Run
  else
     TressShell.Free;
end.
