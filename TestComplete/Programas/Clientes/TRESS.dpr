program Tress;

uses
  MidasLib,
  Forms,
  ZetaClientTools,
  ZBaseShell in '..\..\..\Tools\ZBaseShell.pas' {BaseShell},
  ZBaseConsulta in '..\..\..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZetaSplash in '..\..\..\Tools\ZetaSplash.pas' {SplashScreen},
  DCafeteria in '..\..\..\DataModules\DCafeteria.pas' {dmCafeteria: TDataModule},
  dCatalogos in '..\..\..\DataModules\dCatalogos.pas' {dmCatalogos: TDataModule},
  dAsistencia in '..\..\..\DataModules\dAsistencia.pas' {dmAsistencia: TDataModule},
  DDiccionario in '..\..\..\DataModules\DDiccionario.pas' {dmDiccionario: TDataModule},
  DGlobal in '..\..\..\DataModules\DGlobal.pas',
  dIMSS in '..\..\..\DataModules\dIMSS.pas' {dmIMSS: TDataModule},
  DNomina in '..\..\..\DataModules\DNomina.pas' {dmNomina: TDataModule},
  dRecursos in '..\..\..\DataModules\dRecursos.pas' {dmRecursos: TDataModule},
  DReportes in '..\..\..\DataModules\DReportes.pas' {dmReportes: TDataModule},
  dSistema in '..\..\..\DataModules\dSistema.pas' {dmSistema: TDataModule},
  dTablas in '..\..\..\DataModules\dTablas.pas' {dmTablas: TDataModule},
  ZBaseGlobal_DevEx in '..\..\..\Tools\ZBaseGlobal_DevEx.pas' {BaseGlobal_DevEx},
  FWizNomBasico_DevEx in '..\..\..\Wizards\FWizNomBasico_DevEx.pas' {WizNomBasico_DevEx},
  ZBaseSelectGrid_DevEx in '..\..\..\Tools\ZBaseSelectGrid_DevEx.pas' {BaseGridSelect_DevEx},
  ZetaWizardFeedBack_DevEx in '..\..\..\Tools\ZetaWizardFeedBack_DevEx.pas' {WizardFeedback_DevEx},
  DBaseCliente in '..\..\..\DataModules\DBaseCliente.pas' {BaseCliente: TDataModule},
  DCliente in '..\..\..\DataModules\DCliente.pas' {dmCliente: TDataModule},
  DBasicoCliente in '..\..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DBaseSistema in '..\..\..\DataModules\DBaseSistema.pas' {dmBaseSistema: TDataModule},
  dBaseGlobal in '..\..\..\DataModules\dBaseGlobal.pas',
  DBaseReportes in '..\..\..\DataModules\DBaseReportes.pas' {dmBaseReportes: TDataModule},
  DBaseDiccionario in '..\..\..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  FBaseReportes in '..\..\..\Reportes\FBaseReportes.pas' {BaseReportes},
  ZBaseNavBarShell in '..\..\..\Tools\ZBaseNavBarShell.pas' {BaseNavBarShell},
  ZBaseGridLectura_DevEx in '..\..\..\Tools\ZBaseGridLectura_DevEx.pas' {BaseGridLectura_DevEx},
  ZBaseDlgModal_DevEx in '..\..\..\Tools\ZBaseDlgModal_DevEx.pas' {ZetaDlgModal_DevEx},
  ZBaseEdicion_DevEx in '..\..\..\Tools\ZBaseEdicion_DevEx.pas' {BaseEdicion_DevEx},
  ZcxBaseWizard in '..\..\..\Tools\ZcxBaseWizard.pas' {cxBaseWizard},
  ZcxWizardBasico in '..\..\..\Tools\ZcxWizardBasico.pas' {CXWizardBasico},
  ZCXBaseWizardFiltro in '..\..\..\Tools\ZCXBaseWizardFiltro.pas' {BaseCXWizardFiltro},
  FWizSistBaseFiltro in '..\..\..\Wizards\FWizSistBaseFiltro.pas' {WizSistBaseFiltro},
  ZBasicoNavBarShell in '..\..\..\Tools\ZBasicoNavBarShell.pas' {BasicoNavBarShell},
  FTressShell in '..\..\..\Tress\FTressShell.pas';

{$R *.RES}
{$R WindowsXP.res}
{***DevEx (by am): Spanish.RES es el recurso con todas las traducciones de textos para los componentes de
DevExpress. El recruso es controlado por los componentes cxLocalizer que se encuentran en el Shell y en la forma
de la Matriz de habilidades. Puede haber varios cxLocalizer en el proyecto, pero debe haber un solo recurso de traduccion.
Estos recursos son generados desde un proyecto el cual se encuentra en StartTeam (Traducciones\Spanish.ini),. Cuando se
requiera agregar un texto el Spanish.ini debe ser modificado, y el Spanish.RES debe ser construido y cambiado en los folders
de los proyectos.

La siguiente directiva es la que indica que se utilizara este recurso el cual debe estar presente en el folder del proyecto.
***}

{$R ..\..\..\Traducciones\Spanish.RES}

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  Application.Title := 'TRESS';
  Application.HelpFile := 'Tress.chm';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login( False ) then
       begin
            Show;
            Update;
            WindowState := wsMaximized;
            BeforeRun;
            Application.Run;
       end
       else
       begin
            Free;
       end;
  end;
end.
