program Supervisores;

uses
  Forms,
  ZetaClientTools,
  ZBaseShell in '..\..\..\Tools\ZBaseShell.pas' {BaseShell},
  FTressShell in '..\..\..\Super\FTressShell.pas' {TressShell},
  DBaseCliente in '..\..\..\DataModules\DBaseCliente.pas' {BaseCliente: TDataModule},
  DCliente in '..\..\..\Super\DCliente.pas' {dmCliente: TDataModule},
  DSuper in '..\..\..\Super\DSuper.pas' {dmSuper: TDataModule},
  ZBaseDlgModal in '..\..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  DCatalogos in '..\..\..\Super\DCatalogos.pas' {dmCatalogos: TDataModule},
  DTablas in '..\..\..\Super\DTablas.pas' {dmTablas: TDataModule},
  DBaseDiccionario in '..\..\..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DDiccionario in '..\..\..\Super\DDiccionario.pas' {dmDiccionario: TDataModule},
  ZBaseConsulta in '..\..\..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZBaseEdicion in '..\..\..\Tools\ZBaseEdicion.pas' {BaseEdicion},
  FKardexBase in '..\..\..\Empleados\FKardexBase.pas' {KardexBase},
  ZBaseEdicionRenglon in '..\..\..\Tools\ZBaseEdicionRenglon.pas' {BaseEdicionRenglon},
  ZWizardBasico in '..\..\..\Tools\ZWizardBasico.pas' {WizardBasico},
  ZBaseWizard in '..\..\..\Tools\ZBaseWizard.pas' {BaseWizard},
  ZBaseWizardFiltro in '..\..\..\Tools\ZBaseWizardFiltro.pas' {BaseWizardFiltro},
  ZBaseGridEdicion in '..\..\..\Tools\ZBaseGridEdicion.pas' {BaseGridEdicion},
  DConsultas in '..\..\..\DataModules\DConsultas.pas' {dmConsultas: TDataModule},
  DGlobal in '..\..\..\DataModules\DGlobal.pas',
  DReportes in '..\..\..\DataModules\DReportes.pas' {dmReportes: TDataModule},
  DLabor in '..\..\..\Super\DLabor.pas' {dmLabor: TDataModule},
  FBaseCedulas in '..\..\..\Labor\Edicion\FBaseCedulas.pas' {BaseCedulas},
  DBasicoCliente in '..\..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DBaseGlobal in '..\..\..\DataModules\dBaseGlobal.pas',
  DBaseReportes in '..\..\..\DataModules\DBaseReportes.pas' {dmBaseReportes: TDataModule},
  DMailMerge in '..\..\..\DataModules\dmailmerge.pas' {dmMailMerge: TDataModule},
  DConfigPoliza in '..\..\..\DataModules\DConfigPoliza.pas' {dmConfigPoliza: TDataModule},
  DExportEngine in '..\..\..\DataModules\DExportEngine.pas' {dmExportEngine: TDataModule},
  FCalendarioRitmo in '..\..\..\Catalogos\FCalendarioRitmo.pas' {CalendarioRitmo},
  FEmpleadoGridSelect in '..\..\..\Wizards\FEmpleadoGridSelect.pas',
  dBaseTressDiccionario in '..\..\..\DataModules\dBaseTressDiccionario.pas' {dmBaseTressDiccionario: TDataModule},
  TCClient in 'C:\Program Files\Automated QA\TestComplete\Clients\Delphi&BCB\TCClient.pas';
{$R *.RES}
{$R WindowsXP.res}

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  Application.HelpFile := 'SuperWin.chm';
  Application.Title := 'Tress - Supervisores';
  Application.CreateForm(TTressShell, TressShell);
  if TressShell.Inicializa( True ) then
     Application.Run
  else
     TressShell.Free;
end.
