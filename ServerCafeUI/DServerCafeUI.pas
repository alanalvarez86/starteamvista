unit DServerCafeUI;

interface

uses
  DBaseCafeCliente, Soap.InvokeRegistry, Vcl.ExtCtrls, Data.DB, Datasnap.DBClient, System.Classes, Soap.Rio,
  Soap.SOAPHTTPClient;

type

  TServerCafeUI = class(TdmBaseCafeCliente)
  private
    { Private declarations }
    function GetReinicio: TDateTime;
  public
    { Public declarations }
    procedure Init;
    procedure Restart;
    function SetParametros(iPuerto: Integer; Hora: TDateTime): Boolean;
    property Reinicio: TDateTime read GetReinicio;
  end;

var
  ServerCafeUI: TServerCafeUI;

implementation

uses
  SysUtils, Dialogs, CafeteraConsts, CafeteraUtils, ZetaDialogo;

{$R *.dfm}

procedure TServerCafeUI.Init;
begin
  inherited;
  FirstRun  := True;
  Direccion := GetGlobal(K_IDENT_SERVIDOR, K_CAFETERA_SERVIDOR);
  Puerto    := GetGlobal(K_IDENT_PUERTO, 0{K_CAFETERA_PUERTO});
  Interval  := GetGlobal(K_IDENT_INTERVALO, K_CAFETERA_INTERVALO);
  Active    := True;
end;

procedure TServerCafeUI.Restart;
const
  K_TITULO = 'Reiniciar Reglas';
begin
  if not ZWarningConfirm(K_TITULO, '�Desea reiniciar las reglas?', 0, mbOk) then
    Exit;

  // Enviar peticion al servidor SOAP
  SOAPMsg := CreateSOAPMsg(K_RESTART);
  Params  := TParams.Create(Self);
  if SendSOAPMsg(SOAPMsg, Params) and Params.ParamByName('Status').AsBoolean then
    ZInformation(K_TITULO, 'Se han reiniciado las reglas en el servidor.', 0)
  else
    ZError(K_TITULO, 'No se han reiniciado las reglas en el servidor.', 0);
  FreeAndNil(SOAPMsg);
  FreeAndNil(Params);
end;

function TServerCafeUI.SetParametros(iPuerto: Integer; Hora: TDateTime): Boolean;
const
  K_TITULO = 'Configurar Servicio';
begin
  Result := False;
  if not ZWarningConfirm(K_TITULO, '�Desea cambiar la configuraci�n del Servicio?', 0, mbOk) then
    Exit;

  Params := TParams.Create(Self);
  with Params.AddParameter do begin
    Name     := K_IDENT_PUERTO;
    DataType := ftInteger;
    Value    := iPuerto;
  end;
  with Params.AddParameter do begin
    Name     := K_IDENT_HORA_REINICIO;
    DataType := ftDateTime;
    Value    := Hora;
  end;

  // Enviar peticion al servidor SOAP
  SOAPMsg := CreateSOAPMsg(K_SET_PARAMETROS, ParamsToXML(Params));
  Result := SendSOAPMsg(SOAPMsg);
  if Result then
    SetGlobal(K_IDENT_PUERTO, iPuerto);
  FreeAndNil(SOAPMsg);
  FreeAndNil(Params);
end;

function TServerCafeUI.GetReinicio: TDateTime;
begin
  Result := 0;
  Params  := TParams.Create(Self);

  // Enviar peticion al servidor SOAP
  SOAPMsg := CreateSOAPMsg(K_GET_PARAMETROS);
  if SendSOAPMsg(SOAPMsg, Params) then
    Result := Params.ParamByName(K_IDENT_HORA_REINICIO).AsDateTime;
  FreeAndNil(SOAPMsg);
  FreeAndNil(Params);
end;

end.
