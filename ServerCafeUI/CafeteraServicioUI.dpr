program CafeteraServicioUI;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  Vcl.Forms,
  FCafeteraServicioUI in 'FCafeteraServicioUI.pas' {FormaCafeteraServicioUI},
  CafeteraUtils in '..\ServerCafe\CafeteraUtils.pas',
  FBaseServerCafeUI in '..\ServerCafe\Cliente\FBaseServerCafeUI.pas' {BaseServerCafeUI},
  DServerCafeUI in 'DServerCafeUI.pas' {ServerCafeUI: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFormaCafeteraServicioUI, FormaCafeteraServicioUI);
  FormaCafeteraServicioUI.Show;
  FormaCafeteraServicioUI.Init;
  Application.Run;
end.
