inherited SistEditGrupos: TSistEditGrupos
  Left = 354
  Top = 246
  Caption = 'Grupos de Usuarios'
  ClientHeight = 188
  ClientWidth = 450
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 152
    Width = 450
    TabOrder = 2
    inherited OK: TBitBtn
      Left = 289
      Top = 5
    end
    inherited Cancelar: TBitBtn
      Left = 369
      Top = 5
    end
    object Accesos: TBitBtn
      Left = 7
      Top = 5
      Width = 122
      Height = 25
      Hint = 'Asignar Permisos de Acceso'
      Caption = 'Cambiar &Accesos'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = AccesosClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777777777777777000777777777777033007777777777030B30777777777030B
        0B077777770030B0B000770000330B0B08F070333300B0B0FFF0037B7B3B0B08
        F8F007BB3B73B0FFFFF00BB3B3BB00F8F8F088303B3B70FFFFF0070703B3B0F8
        F8F00B803B3B70CCCCC070BB8BB7000000007700800077777777}
    end
  end
  inherited PanelSuperior: TPanel
    Width = 450
    TabOrder = 0
    inherited DBNavigator: TDBNavigator
      OnClick = DBNavigatorClick
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 450
    TabOrder = 1
    inherited Splitter: TSplitter
      Left = 249
    end
    inherited ValorActivo1: TPanel
      Width = 249
    end
    inherited ValorActivo2: TPanel
      Left = 252
      Width = 198
    end
  end
  object Datos: TGroupBox [3]
    Left = 0
    Top = 51
    Width = 450
    Height = 101
    Align = alClient
    TabOrder = 3
    object GR_CODIGOlbl: TLabel
      Left = 43
      Top = 19
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C�digo:'
    end
    object GR_DESCRIPlbl: TLabel
      Left = 20
      Top = 42
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci�n:'
    end
    object SUBORDINADOlbl: TLabel
      Left = 7
      Top = 66
      Width = 72
      Height = 13
      Alignment = taRightJustify
      Caption = 'Subordinado a:'
    end
    object GR_DESCRIP: TDBEdit
      Left = 82
      Top = 38
      Width = 257
      Height = 21
      DataField = 'GR_DESCRIP'
      DataSource = DataSource
      TabOrder = 1
    end
    object GR_CODIGO: TZetaDBNumero
      Left = 82
      Top = 15
      Width = 73
      Height = 21
      Mascara = mnEmpleado
      TabOrder = 0
      ConfirmEdit = True
      DataField = 'GR_CODIGO'
      DataSource = DataSource
    end
    object GR_PADRE: TZetaDBKeyLookup
      Left = 82
      Top = 62
      Width = 300
      Height = 21
      TabOrder = 2
      TabStop = True
      WidthLlave = 60
      DataField = 'GR_PADRE'
      DataSource = DataSource
    end
  end
  inherited DataSource: TDataSource
    Left = 316
    Top = 9
  end
  object dsEmpresas: TDataSource
    Left = 320
    Top = 51
  end
end
