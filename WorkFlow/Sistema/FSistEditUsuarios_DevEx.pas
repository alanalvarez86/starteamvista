unit FSistEditUsuarios_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, StdCtrls, DBCtrls, Mask, ComCtrls, ExtCtrls, Buttons,
     FSistBaseEditUsuarios,
     ZetaDBTextBox,
     ZetaKeyCombo,
     ZetaKeyLookup,
     ZetaNumero, ZetaSmartLists;

type
  TSistEditUsuarios_DevEx = class(TSistBaseEditUsuarios)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SistEditUsuarios_DevEx: TSistEditUsuarios_DevEx;

implementation

uses DSistema,
     ZHelpContext,
     ZAccesosTress,
     ZetaCommonClasses;

{$R *.DFM}

procedure TSistEditUsuarios_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     Operacion.TabVisible := FALSE;
     HelpContext := H_WORKFLOWCFG_SISTEMA_USUARIOS_EDIC;
     IndexDerechos := D_SIST_DATOS_USUARIOS;
end;

end.
