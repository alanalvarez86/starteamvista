unit FSistEditGrupos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Mask, Db, ExtCtrls,
     Buttons, DBCtrls, ComCtrls, Menus, Grids, DBGrids,
     ZBaseEdicion,
     ZetaMessages,
     ZetaEdit,
     ZetaDBGrid,
     ZetaNumero,
     ZetaKeyLookup,
     ZetaSmartLists;

type
  TSistEditGrupos = class(TBaseEdicion)
    Datos: TGroupBox;
    GR_CODIGOlbl: TLabel;
    GR_DESCRIPlbl: TLabel;
    GR_DESCRIP: TDBEdit;
    GR_CODIGO: TZetaDBNumero;
    dsEmpresas: TDataSource;
    Accesos: TBitBtn;
    SUBORDINADOlbl: TLabel;
    GR_PADRE: TZetaDBKeyLookup;
    procedure FormCreate(Sender: TObject);
    procedure AccesosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBNavigatorClick(Sender: TObject; Button: TNavigateBtn);
  private
    { Private declarations }
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    procedure ChecaGrupoActual;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure HabilitaControles; override;
  public
    { Public declarations }
  end;

var
  SistEditGrupos: TSistEditGrupos;

implementation

uses DSistema,
     DCliente,
     DBaseSistema,
     ZHelpContext,
     ZAccesosTress,
     ZetaCommonClasses,
     ZetaDialogo;

{$R *.DFM}

procedure TSistEditGrupos.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_WORKFLOWCFG_SISTEMA_GPOUSUARIOS_EDIC;
     IndexDerechos := D_SIST_DATOS_GRUPOS;
     FirstControl := GR_CODIGO;
     with GR_PADRE do
     begin
          LookupDataset := dmSistema.cdsGruposLookup;
     end;
end;

procedure TSistEditGrupos.FormShow(Sender: TObject);
begin
     inherited;
     ChecaGrupoActual;
end;

procedure TSistEditGrupos.Connect;
begin
     with dmSistema do
     begin
          cdsGrupos.Conectar;
          cdsGruposLookUp.Conectar;
          Datasource.Dataset := cdsGrupos;
     end;
end;

procedure TSistEditGrupos.ChecaGrupoActual;
begin
     with GR_PADRE do
     begin
          Enabled := ( dmSistema.cdsGrupos.FieldByName( 'GR_CODIGO' ).Asinteger <> ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION );
          if not Enabled then
             SetLlaveDescripcion( VACIO, VACIO );
          SUBORDINADOlbl.Enabled := Enabled;
     end;
end;

procedure TSistEditGrupos.HabilitaControles;
begin
     inherited HabilitaControles;
     Accesos.Enabled := not Editing;
end;

procedure TSistEditGrupos.WMExaminar(var Message: TMessage);
begin
     Accesos.Click;
end;

procedure TSistEditGrupos.AccesosClick(Sender: TObject);
begin
     if not Inserting then
     begin
          if ( DataSource.Dataset.FieldByName( 'GR_CODIGO' ).AsInteger <> D_GRUPO_SIN_RESTRICCION ) then
             DSistema.dmSistema.CambiarAccesos
          else
              ZetaDialogo.zInformation( Self.Caption, '� El Grupo 1 Tiene Todos Los Derechos: No Se Permite Cambiarle Accesos !', 0 );
      end;
end;

procedure TSistEditGrupos.DBNavigatorClick(Sender: TObject; Button: TNavigateBtn);
begin
     inherited;
     ChecaGrupoActual;
end;

end.
