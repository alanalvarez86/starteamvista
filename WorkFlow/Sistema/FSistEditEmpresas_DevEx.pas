unit FSistEditEmpresas_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, StdCtrls, Mask, DBCtrls, ExtCtrls, Buttons,
     FSistBaseEditEmpresas,
     ZetaEdit,
     ZetaSmartLists;

type
  TSistEditEmpresas_DevEx = class(TSistBaseEditEmpresas)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SistEditEmpresas_DevEx: TSistEditEmpresas_DevEx;

implementation

uses ZHelpContext,
     ZAccesosTress;
     
{$R *.DFM}

procedure TSistEditEmpresas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_SIST_DATOS_EMPRESAS;
     IndexDerechos := D_SIST_DATOS_EMPRESAS;
end;

end.
