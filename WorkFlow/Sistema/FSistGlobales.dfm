inherited SistGlobales: TSistGlobales
  Left = 280
  Top = 123
  Caption = 'Globales de Empresa'
  ClientHeight = 362
  ClientWidth = 612
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 31
    Top = 26
    Width = 44
    Height = 13
    Alignment = taRightJustify
    Caption = '&Empresa:'
  end
  inherited PanelIdentifica: TPanel
    Width = 612
    inherited Slider: TSplitter
      Left = 307
    end
    inherited ValorActivo1: TPanel
      Width = 291
    end
    inherited ValorActivo2: TPanel
      Left = 310
      Width = 302
    end
  end
  object PanelTitulos: TPanel [2]
    Left = 0
    Top = 19
    Width = 612
    Height = 343
    Align = alClient
    TabOrder = 1
    OnDblClick = PanelTitulosDblClick
    object Empresa: TZetaDBTextBox
      Left = 77
      Top = 24
      Width = 360
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Direccion: TZetaDBTextBox
      Left = 77
      Top = 43
      Width = 360
      Height = 17
      AutoSize = False
      Caption = 'Direccion'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object EmpresaLBL: TLabel
      Left = 31
      Top = 26
      Width = 44
      Height = 13
      Alignment = taRightJustify
      Caption = '&Empresa:'
    end
    object DireccionLBL: TLabel
      Left = 27
      Top = 45
      Width = 48
      Height = 13
      Alignment = taRightJustify
      Caption = 'D&irecci'#243'n:'
    end
    object CiudadLBL: TLabel
      Left = 39
      Top = 64
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = '&Ciudad:'
    end
    object EstadoLBL: TLabel
      Left = 39
      Top = 83
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'E&stado:'
    end
    object Ciudad: TZetaDBTextBox
      Left = 77
      Top = 62
      Width = 360
      Height = 17
      AutoSize = False
      Caption = 'Ciudad'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Estado: TZetaDBTextBox
      Left = 77
      Top = 81
      Width = 360
      Height = 17
      AutoSize = False
      Caption = 'Estado'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object ProyectoLBL: TLabel
      Left = 31
      Top = 7
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Caption = '&Proyecto:'
    end
    object Proyecto: TZetaDBTextBox
      Left = 78
      Top = 5
      Width = 360
      Height = 17
      AutoSize = False
      Caption = 'Proyecto'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  inherited DataSource: TDataSource
    Left = 240
    Top = 128
  end
end
