unit FSistUsuarios;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, StdCtrls, Buttons, Grids, DBGrids, ExtCtrls,
     FSistBaseUsuarios,
     ZetaDBGrid, ZetaKeyCombo;

type
  TSistUsuarios = class(TSistBaseUsuarios)
    Suspender: TBitBtn;
    Activar: TBitBtn;
    procedure SuspenderClick(Sender: TObject);
    procedure ActivarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SistUsuarios: TSistUsuarios;

implementation

{$R *.DFM}

uses DSistema,
     DGlobal,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZBaseDlgModal,
     ZetaDialogo,
     ZGlobalTress,
     ZHelpContext,
     ZAccesosMgr,
     ZAccesosTress;

procedure TSistUsuarios.FormShow(Sender: TObject);
begin
     inherited;
     HelpContext := H_WORKFLOWCFG_SISTEMA_USUARIOS;
     IndexDerechos := D_SIST_DATOS_USUARIOS;
     Activar.Enabled := ZAccesosMgr.Revisa( D_SIST_PRENDE_USUARIOS );
     Suspender.Enabled := ZAccesosMgr.Revisa( D_SIST_APAGA_USUARIOS );
end;

procedure TSistUsuarios.SuspenderClick(Sender: TObject);
begin
     dmSistema.SuspendeUsuarios;
end;

procedure TSistUsuarios.ActivarClick(Sender: TObject);
begin
     dmSistema.ActivaUsuarios;
end;

end.
