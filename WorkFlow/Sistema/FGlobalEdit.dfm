inherited GlobalEdit: TGlobalEdit
  Left = 390
  Top = 354
  Caption = 'Editar Valores Globales'
  ClientHeight = 370
  ClientWidth = 557
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 334
    Width = 557
    inherited OK: TBitBtn
      Left = 389
    end
    inherited Cancelar: TBitBtn
      Left = 474
    end
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 557
    Height = 334
    ActivePage = Generales
    Align = alClient
    TabOrder = 1
    object Generales: TTabSheet
      Caption = 'Generales'
      object GeneralesGB: TGroupBox
        Left = 0
        Top = 0
        Width = 549
        Height = 113
        Align = alTop
        Caption = ' Datos Generales '
        TabOrder = 0
        object EmpresaLBL: TLabel
          Left = 74
          Top = 16
          Width = 44
          Height = 13
          Alignment = taRightJustify
          Caption = '&Empresa:'
          FocusControl = Empresa
        end
        object DireccionLBL: TLabel
          Left = 70
          Top = 39
          Width = 48
          Height = 13
          Alignment = taRightJustify
          Caption = 'D&irecci'#243'n:'
          FocusControl = Direccion
        end
        object CiudadLBL: TLabel
          Left = 82
          Top = 62
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = '&Ciudad:'
          FocusControl = Ciudad
        end
        object EstadoLBL: TLabel
          Left = 82
          Top = 85
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'E&stado:'
          FocusControl = Estado
        end
        object Empresa: TEdit
          Tag = 6
          Left = 120
          Top = 13
          Width = 391
          Height = 21
          TabOrder = 0
        end
        object Direccion: TEdit
          Tag = 7
          Left = 120
          Top = 36
          Width = 391
          Height = 21
          TabOrder = 1
        end
        object Ciudad: TEdit
          Tag = 7
          Left = 120
          Top = 59
          Width = 391
          Height = 21
          TabOrder = 2
        end
        object Estado: TEdit
          Tag = 7
          Left = 120
          Top = 82
          Width = 391
          Height = 21
          TabOrder = 3
        end
      end
      object ReporteadorGB: TGroupBox
        Left = 0
        Top = 113
        Width = 549
        Height = 59
        Align = alTop
        Caption = ' Reporteador '
        TabOrder = 1
        object Label3: TLabel
          Left = 22
          Top = 23
          Width = 94
          Height = 13
          Alignment = taRightJustify
          Caption = '&Directorio Reportes:'
          FocusControl = Plantillas
        end
        object Buscar: TSpeedButton
          Left = 516
          Top = 17
          Width = 25
          Height = 25
          Hint = 'Buscar Directorio'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BuscarClick
        end
        object Plantillas: TEdit
          Tag = 61
          Left = 120
          Top = 19
          Width = 391
          Height = 21
          TabOrder = 0
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 172
        Width = 549
        Height = 59
        Align = alTop
        Caption = ' Usuario para grabar transacciones en Sistema TRESS '
        TabOrder = 2
        object UsuarioTressLBL: TLabel
          Left = 33
          Top = 23
          Width = 83
          Height = 13
          Alignment = taRightJustify
          Caption = '&Usuario de Tress:'
          FocusControl = UsuarioTress
          ParentShowHint = False
          ShowHint = True
        end
        object UsuarioTress: TZetaKeyLookup
          Left = 120
          Top = 19
          Width = 385
          Height = 21
          Hint = 'Usuario De Tress Quien Representa Al Motor De WorkFlow'
          ShowHint = True
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
        end
      end
    end
    object Correos: TTabSheet
      Caption = ' Correos '
      ImageIndex = 1
      object PageControlCorreos: TPageControl
        Left = 0
        Top = 228
        Width = 549
        Height = 78
        Align = alClient
        MultiLine = True
        TabOrder = 3
        TabPosition = tpBottom
      end
      object ConfirmacionGB: TGroupBox
        Left = 0
        Top = 73
        Width = 549
        Height = 79
        Align = alTop
        Caption = ' Acuse De Recibo '
        TabOrder = 1
        object ConfirmacionNombreLBL: TLabel
          Left = 89
          Top = 23
          Width = 40
          Height = 13
          Hint = 
            'Nombre De Quien Recibe Los Acuses De Recibo De Los Correos De Wo' +
            'rkFlow'
          Alignment = taRightJustify
          Caption = '&Nombre:'
          FocusControl = ConfirmacionNombre
          ParentShowHint = False
          ShowHint = True
        end
        object ConfirmacionEMailLBL: TLabel
          Left = 30
          Top = 44
          Width = 99
          Height = 13
          Hint = 
            'Direcci'#243'n De Correo De Quien Recibe Los Acuses De Recibo De Los ' +
            'Correos De WorkFlow'
          Alignment = taRightJustify
          Caption = '&Direcci'#243'n De Correo:'
          FocusControl = ConfirmacionEMail
          ParentShowHint = False
          ShowHint = True
        end
        object ConfirmacionEMail: TEdit
          Left = 130
          Top = 40
          Width = 385
          Height = 21
          Hint = 
            'Direcci'#243'n De Correo De Quien Recibe Los Acuses De Recibo De Los ' +
            'Correos De WorkFlow'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Text = 'gandrade@tress.com.mx'
        end
        object ConfirmacionNombre: TEdit
          Left = 130
          Top = 19
          Width = 385
          Height = 21
          Hint = 
            'Nombre De Quien Recibe Los Acuses De Recibo De Los Correos De Wo' +
            'rkFlow'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Text = 'german'
        end
      end
      object RemitenteGB: TGroupBox
        Left = 0
        Top = 152
        Width = 549
        Height = 76
        Align = alTop
        Caption = ' Remitente De Los Correos Enviados Por WorkFlow '
        TabOrder = 2
        object RemitenteLBL: TLabel
          Left = 89
          Top = 25
          Width = 40
          Height = 13
          Hint = 'Nombre Del Remitente De Los Correos De WorkFlow'
          Alignment = taRightJustify
          Caption = '&Nombre:'
          FocusControl = Remitente
          ParentShowHint = False
          ShowHint = True
        end
        object RemitenteDireccionLBL: TLabel
          Left = 30
          Top = 46
          Width = 99
          Height = 13
          Hint = 'Direcci'#243'n De Correo Del Remitente De Los Correos De WorkFlow'
          Alignment = taRightJustify
          Caption = '&Direcci'#243'n De Correo:'
          FocusControl = RemitenteDireccion
          ParentShowHint = False
          ShowHint = True
        end
        object RemitenteDireccion: TEdit
          Left = 130
          Top = 42
          Width = 385
          Height = 21
          Hint = 'Direcci'#243'n De Correo Del Remitente De Los Correos De WorkFlow'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Text = 'gandrade@tress.com.mx'
        end
        object Remitente: TEdit
          Left = 130
          Top = 21
          Width = 385
          Height = 21
          Hint = 'Nombre Del Remitente De Los Correos De WorkFlow'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Text = 'german'
        end
      end
      object MotorGB: TGroupBox
        Left = 0
        Top = 0
        Width = 549
        Height = 73
        Align = alTop
        Caption = ' Administrador Del WorkFlow '
        TabOrder = 0
        object AdministradorLBL: TLabel
          Left = 89
          Top = 23
          Width = 40
          Height = 13
          Hint = 'Nombre Del Administrador Del WorkFlow'
          Alignment = taRightJustify
          Caption = 'N&ombre:'
          FocusControl = Administrador
          ParentShowHint = False
          ShowHint = True
        end
        object AdministradorEMailLBL: TLabel
          Left = 30
          Top = 44
          Width = 99
          Height = 13
          Hint = 'Direcci'#243'n De Correo Electr'#243'nico Del Administrador De WorkFlow'
          Alignment = taRightJustify
          Caption = 'D&irecci'#243'n De Correo:'
          FocusControl = AdministradorEMail
          ParentShowHint = False
          ShowHint = True
        end
        object AdministradorEMail: TEdit
          Left = 130
          Top = 40
          Width = 385
          Height = 21
          Hint = 'Direcci'#243'n De Correo Electr'#243'nico Del Administrador De WorkFlow'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Text = 'gandrade@tress.com.mx'
        end
        object Administrador: TEdit
          Left = 130
          Top = 19
          Width = 385
          Height = 21
          Hint = 'Nombre Del Administrador Del WorkFlow'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Text = 'german'
        end
      end
    end
    object eMailServer: TTabSheet
      Caption = 'Servidor Correos'
      ImageIndex = 3
      DesignSize = (
        549
        306)
      object ServidorLBL: TLabel
        Left = 30
        Top = 17
        Width = 98
        Height = 13
        Hint = 'Direcci'#243'n de IP '#243' Nombre Del Sevidor De Correos'
        Alignment = taRightJustify
        Caption = '&Servidor De Correos:'
        FocusControl = Servidor
        ParentShowHint = False
        ShowHint = True
      end
      object UsuarioLBL: TLabel
        Left = 89
        Top = 41
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = '&Usuario:'
        FocusControl = Usuario
      end
      object PasswordLBL: TLabel
        Left = 71
        Top = 64
        Width = 57
        Height = 13
        Hint = 'Clave De Acceso Para Entrar Al Servidor De Correos'
        Alignment = taRightJustify
        Caption = '&Contrase'#241'a:'
        FocusControl = Passsword
        ParentShowHint = False
        ShowHint = True
      end
      object PuertoLBL: TLabel
        Left = 94
        Top = 87
        Width = 34
        Height = 13
        Hint = 'Puerto TCP Para Que Usa El Servidor De Correos'
        Alignment = taRightJustify
        Caption = '&Puerto:'
        FocusControl = Puerto
        ParentShowHint = False
        ShowHint = True
      end
      object TimeOutLBL: TLabel
        Left = 85
        Top = 110
        Width = 43
        Height = 13
        Hint = 'Tiempo De Espera Para Enviar Los Correos'
        Alignment = taRightJustify
        Caption = '&TimeOut:'
        FocusControl = TimeOut
        ParentShowHint = False
        ShowHint = True
      end
      object TimeOut: TZetaNumero
        Left = 130
        Top = 106
        Width = 54
        Height = 21
        Hint = 'Tiempo De Espera Para Enviar Los Correos'
        Mascara = mnDias
        TabOrder = 4
        Text = '0'
      end
      object Puerto: TZetaNumero
        Left = 130
        Top = 83
        Width = 54
        Height = 21
        Hint = 'Puerto TCP Para Que Usa El Servidor De Correos'
        Mascara = mnDias
        TabOrder = 3
        Text = '0'
      end
      object Passsword: TEdit
        Left = 130
        Top = 60
        Width = 385
        Height = 21
        Hint = 'Clave De Acceso Para Entrar Al Servidor De Correos'
        ParentShowHint = False
        PasswordChar = '*'
        ShowHint = True
        TabOrder = 2
      end
      object Usuario: TEdit
        Left = 130
        Top = 37
        Width = 385
        Height = 21
        TabOrder = 1
      end
      object Servidor: TEdit
        Left = 130
        Top = 14
        Width = 385
        Height = 21
        Hint = 'Direcci'#243'n de IP '#243' Nombre Del Sevidor De Correos'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Text = 'MAILSERVER'
      end
      object ProbarEMail: TBitBtn
        Left = 199
        Top = 141
        Width = 90
        Height = 44
        Hint = 'Enviar Un eMail de Prueba'
        Anchors = [akTop, akRight]
        Caption = '&Probar'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = ProbarEMailClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333333333333333333FFFFFFFFFFFFFFF000000000000
          000077777777777777770FFFFFFFFFFFFFF07F3333FFF33333370FFFF777FFFF
          FFF07F333777333333370FFFFFFFFFFFFFF07F3333FFFFFF33370FFFF777777F
          FFF07F33377777733FF70FFFFFFFFFFF99907F3FFF33333377770F777FFFFFFF
          9CA07F77733333337F370FFFFFFFFFFF9A907FFFFFFFFFFF7FF7000000000000
          0000777777777777777733333333333333333333333333333333333333333333
          3333333333333333333333333333333333333333333333333333333333333333
          3333333333333333333333333333333333333333333333333333}
        Layout = blGlyphTop
        NumGlyphs = 2
      end
    end
    object Estilos: TTabSheet
      Caption = 'Estilo de Correos'
      ImageIndex = 4
      object XSLTareaGB: TGroupBox
        Left = 0
        Top = 34
        Width = 549
        Height = 50
        Align = alTop
        Caption = ' Tareas '
        TabOrder = 1
        object XSLTareaLBL: TLabel
          Left = 89
          Top = 20
          Width = 29
          Height = 13
          Hint = 'Archivo XSL Para Generar Correos De Notificaciones'
          Alignment = taRightJustify
          Caption = 'Aviso:'
          FocusControl = XSLTarea
          ParentShowHint = False
          ShowHint = True
        end
        object XSLTarea: TEdit
          Left = 120
          Top = 17
          Width = 385
          Height = 21
          Hint = 'Archivo XSL Para Generar Correos De Notificaciones'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
      end
      object PanelEstilos: TPanel
        Left = 0
        Top = 0
        Width = 549
        Height = 34
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object XSLPathLBL: TLabel
          Left = 30
          Top = 11
          Width = 86
          Height = 13
          Alignment = taRightJustify
          Caption = '&Directorio de XSL:'
          FocusControl = XSLPath
        end
        object XSLPath: TEdit
          Tag = 61
          Left = 120
          Top = 7
          Width = 385
          Height = 21
          TabOrder = 0
        end
      end
      object PasosProcesoGB: TGroupBox
        Left = 0
        Top = 84
        Width = 549
        Height = 222
        Align = alClient
        Caption = ' Notificaciones '
        TabOrder = 2
        object XSLPasoProcesoActivarLBL: TLabel
          Left = 90
          Top = 20
          Width = 28
          Height = 13
          Hint = 'Archivo XSL Para Avisar Al Activar Un Paso De Proceso'
          Alignment = taRightJustify
          Caption = 'Iniciar'
          FocusControl = XSLPasoProcesoActivar
          ParentShowHint = False
          ShowHint = True
        end
        object XSLPasoProcesoTerminarLBL: TLabel
          Left = 74
          Top = 42
          Width = 44
          Height = 13
          Hint = 'Archivo XSL Para Avisar Al Terminar Un Paso De Proceso'
          Alignment = taRightJustify
          Caption = 'Terminar:'
          FocusControl = XSLPasoProcesoTerminar
          ParentShowHint = False
          ShowHint = True
        end
        object XSLPasoProcesoCancelarLBL: TLabel
          Left = 73
          Top = 64
          Width = 45
          Height = 13
          Hint = 'Archivo XSL Para Avisar Al Cancelar Un Paso De Proceso'
          Alignment = taRightJustify
          Caption = 'Cancelar:'
          FocusControl = XSLPasoProcesoCancelar
          ParentShowHint = False
          ShowHint = True
        end
        object Label2: TLabel
          Left = 43
          Top = 86
          Width = 75
          Height = 13
          Hint = 'Archivo XSL Para Avisar Al Cancelar Un Paso De Proceso'
          Alignment = taRightJustify
          Caption = 'Modificar datos:'
          FocusControl = XSLModificarPasos
          ParentShowHint = False
          ShowHint = True
        end
        object Label4: TLabel
          Left = 28
          Top = 108
          Width = 90
          Height = 13
          Hint = 'Archivo XSL Para Avisar Al Cancelar Un Paso De Proceso'
          Alignment = taRightJustify
          Caption = 'Despu'#233's de iniciar:'
          FocusControl = XSLDespuesIniciar
          ParentShowHint = False
          ShowHint = True
        end
        object Label5: TLabel
          Left = 43
          Top = 130
          Width = 75
          Height = 13
          Hint = 'Archivo XSL Para Avisar Al Cancelar Un Paso De Proceso'
          Alignment = taRightJustify
          Caption = 'Antes del l'#237'mite:'
          FocusControl = XSLAntesLimite
          ParentShowHint = False
          ShowHint = True
        end
        object Label6: TLabel
          Left = 28
          Top = 152
          Width = 90
          Height = 13
          Hint = 'Archivo XSL Para Avisar Al Cancelar Un Paso De Proceso'
          Alignment = taRightJustify
          Caption = 'Despu'#233's del l'#237'mite:'
          FocusControl = XSLDespuesLimite
          ParentShowHint = False
          ShowHint = True
        end
        object XSLPasoProcesoActivar: TEdit
          Left = 120
          Top = 17
          Width = 385
          Height = 21
          Hint = 'Archivo XSL Para Avisar Al Activar Un Paso De Proceso'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
        object XSLPasoProcesoTerminar: TEdit
          Left = 120
          Top = 39
          Width = 385
          Height = 21
          Hint = 'Archivo XSL Para Avisar Al Terminar Un Paso De Proceso'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
        object XSLPasoProcesoCancelar: TEdit
          Left = 120
          Top = 61
          Width = 385
          Height = 21
          Hint = 'Archivo XSL Para Avisar Al Cancelar Un Paso De Proceso'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object XSLModificarPasos: TEdit
          Left = 120
          Top = 83
          Width = 385
          Height = 21
          Hint = 
            'Archivo XSL para avisar en caso de una modificacion de los datos' +
            ' del proceso'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
        end
        object XSLDespuesIniciar: TEdit
          Left = 120
          Top = 105
          Width = 385
          Height = 21
          Hint = 'Archivo XSL para avisar que el proceso ya comenzo.'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
        end
        object XSLAntesLimite: TEdit
          Left = 120
          Top = 127
          Width = 385
          Height = 21
          Hint = 
            'Archivo XSL para avisar antes de que se llegue el tiempo limite ' +
            'de la tarea'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
        end
        object XSLDespuesLimite: TEdit
          Left = 120
          Top = 149
          Width = 385
          Height = 21
          Hint = 'Archivo XSL para avisar que la tarea ya paso su limite de tiempo'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
        end
      end
    end
    object InterfaseWEB: TTabSheet
      Caption = ' Interfase WEB '
      ImageIndex = 2
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 549
        Height = 65
        Align = alTop
        Caption = 'Rutas Globales '
        TabOrder = 0
        object Label1: TLabel
          Left = 6
          Top = 28
          Width = 102
          Height = 13
          Alignment = taRightJustify
          Caption = 'URL directorio virtual:'
          FocusControl = RootDirURL
        end
        object RootDirURL: TEdit
          Left = 112
          Top = 24
          Width = 419
          Height = 21
          TabOrder = 0
        end
      end
    end
  end
end
