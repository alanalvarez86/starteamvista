exec sp_addtype PathArchivo, 'varchar(150)', 'NOT NULL'
exec sp_bindefault StringVacio, PathArchivo
GO

exec sp_addtype Condicion, 'char(8)', 'NOT NULL'
exec sp_bindefault CodigoVacio, Condicion
go

exec sp_addtype NombreCampo, 'varchar(10)', 'NOT NULL'
exec sp_bindefault StringVacio, NombreCampo
GO

exec sp_addtype TituloCampo, 'varchar(30)', 'NOT NULL'
exec sp_bindefault StringVacio, TituloCampo
GO

exec sp_addtype CamposRequeridos, 'varchar(255)', 'NOT NULL'
exec sp_bindefault StringVacio, CamposRequeridos
GO

exec sp_addtype Tamanio, 'numeric(15,3)', 'NOT NULL'
exec sp_bindefault Cero, Tamanio
GO

exec sp_addtype Mascara, 'varchar(20)', 'NOT NULL'
exec sp_bindefault StringVacio, Mascara
GO

exec sp_addtype WindowsName, 'varchar(32)', 'NOT NULL'
exec sp_bindefault StringVacio, WindowsName
GO

create default NewGUID AS NEWID()
GO

exec sp_addtype GUID, 'uniqueidentifier', 'NOT NULL'
exec sp_bindefault NewGUID, GUID
GO

exec sp_addtype NullGUID, 'uniqueidentifier', 'NULL'
GO

exec sp_addtype Binario, 'image', 'NULL'
GO

exec sp_addtype XMLData, 'text', 'NULL'
GO

exec sp_addtype Notas, 'text', 'NULL'
GO

alter table USUARIO add US_JEFE Usuario
GO

alter table USUARIO add US_PAG_DEF Usuario
GO

INSERT INTO COMPANY (CM_CODIGO, CM_NOMBRE, CM_ALIAS, CM_USRNAME, CM_PASSWRD, CM_CONTROL )
             VALUES ('XCOMPARTEY', 'Comparte (No Borrar)', 'Tress Comparte', 'sa', '', '3OTRO' )
GO

CREATE TABLE DICCION (
       DI_CLASIFI           Status,
       DI_NOMBRE            NombreCampo,
       DI_TITULO            TituloCampo,
       DI_CALC              Status,
       DI_ANCHO             Status,
       DI_MASCARA           Descripcion,
       DI_TFIELD            Status,
       DI_ORDEN             Booleano,
       DI_REQUIER           CamposRequeridos,
       DI_TRANGO            Status,
       DI_NUMERO            Status,
       DI_VALORAC           Descripcion,
       DI_RANGOAC           Status,
       DI_CLAVES            TituloCampo,
       DI_TCORTO            TituloCampo,
       DI_CONFI             Booleano
)
GO

CREATE INDEX XIE1DICCION ON DICCION
(
       DI_CLASIFI,
       DI_CALC
)
GO

ALTER TABLE DICCION
       ADD CONSTRAINT PK_DICCION PRIMARY KEY (DI_CLASIFI, DI_NOMBRE)
GO

CREATE TABLE BITACORA (
       US_CODIGO            Usuario,
       BI_FECHA             Fecha,
       BI_HORA              HoraMinutosSegundos,
       BI_PROC_ID           Status,
       BI_TIPO              Status,
       BI_NUMERO            FolioGrande,
       BI_TEXTO             Observaciones,
       CB_CODIGO            NumeroEmpleado,
       BI_DATA              Memo,
       BI_CLASE             Status,
       BI_FEC_MOV           Fecha
)
GO

CREATE INDEX XIE1BITACORA ON BITACORA
(
       BI_NUMERO
)
GO

CREATE INDEX XIE2BITACORA ON BITACORA
(
       BI_FECHA,
       BI_HORA
)
GO

CREATE TABLE PROCESO (
       PC_PROC_ID           Status,
       PC_NUMERO            FolioGrande,
       US_CODIGO            Usuario,
       PC_FEC_INI           Fecha,
       PC_HOR_INI           HoraMinutosSegundos,
       PC_FEC_FIN           Fecha,
       PC_HOR_FIN           HoraMinutosSegundos,
       PC_ERROR             Booleano,
       CB_CODIGO            NumeroEmpleado,
       PC_MAXIMO            FolioGrande,
       PC_PASO              FolioGrande,
       US_CANCELA           Usuario,
       PC_PARAMS            Formula
)
GO

CREATE INDEX XIE1PROCESO ON PROCESO
(
       PC_FEC_INI,
       PC_ERROR
)
GO


ALTER TABLE PROCESO
       ADD CONSTRAINT PK_PROCESO PRIMARY KEY (PC_NUMERO)
GO

CREATE TABLE QUERYS (
       QU_CODIGO            Condicion,
       QU_DESCRIP           Formula,     
       QU_FILTRO            Formula,
       QU_NIVEL             Status,
       US_CODIGO            Usuario
)
go


ALTER TABLE QUERYS
       ADD CONSTRAINT PK_QUERYS PRIMARY KEY (QU_CODIGO)
go

CREATE TABLE REPORTE (
       RE_CODIGO            FolioChico,
       RE_NOMBRE            Descripcion,
       RE_TIPO              Status,
       RE_TITULO            Titulo,
       RE_ENTIDAD           Status,
       RE_SOLOT             Booleano,
       RE_FECHA             Fecha,
       RE_GENERAL           Descripcion,
       RE_REPORTE           PathArchivo,
       RE_CFECHA            NombreCampo,
       RE_IFECHA            Status,
       RE_HOJA              Status,
       RE_ALTO              Tamanio,
       RE_ANCHO             Tamanio,
       RE_PRINTER           Formula,
       RE_COPIAS            Status,
       RE_PFILE             Status,
       RE_ARCHIVO           PathArchivo,
       RE_VERTICA           Booleano,
       US_CODIGO            Usuario,
       RE_FILTRO            Formula,
       RE_COLNUM            Status,
       RE_COLESPA           Tamanio,
       RE_RENESPA           Tamanio,
       RE_MAR_SUP           Tamanio,
       RE_MAR_IZQ           Tamanio,
       RE_MAR_DER           Tamanio,
       RE_MAR_INF           Tamanio,
       RE_NIVEL             Status,
       RE_FONTNAM           WindowsName,
       RE_FONTSIZ           Status,
       QU_CODIGO            Condicion,
       RE_CLASIFI           Status,
       RE_CANDADO           Status
)
GO

CREATE UNIQUE INDEX XAK1REPORTE ON REPORTE
(
       RE_NOMBRE
)
GO

CREATE INDEX XIE1REPORTE ON REPORTE
(
       RE_CLASIFI,
       RE_NOMBRE
)
GO

ALTER TABLE REPORTE
       ADD CONSTRAINT PK_REPORTE PRIMARY KEY (RE_CODIGO)
GO

CREATE TABLE CAMPOREP (
       RE_CODIGO            FolioChico,
       CR_TIPO              Status,
       CR_POSICIO           Status,
       CR_CLASIFI           Status,
       CR_SUBPOS            Status,
       CR_TABLA             Status,
       CR_TITULO            TituloCampo,
       CR_REQUIER           Formula,
       CR_CALC              Status,
       CR_MASCARA           Mascara,
       CR_ANCHO             Status,
       CR_OPER              Status,
       CR_TFIELD            Status,
       CR_SHOW              Status,
       CR_DESCRIP           NombreCampo,
       CR_COLOR             Status,
       CR_FORMULA           Formula,
       CR_BOLD              Booleano,
       CR_ITALIC            Booleano,
       CR_SUBRAYA           Booleano,
       CR_STRIKE            Booleano,
       CR_ALINEA            Status
)
GO

ALTER TABLE CAMPOREP
       ADD CONSTRAINT PK_CAMPOREP PRIMARY KEY (RE_CODIGO, CR_TIPO, CR_POSICIO,
              CR_SUBPOS)
GO

CREATE TABLE MISREPOR (
       US_CODIGO            Usuario,
       RE_CODIGO            FolioChico )
GO

ALTER TABLE MISREPOR ADD CONSTRAINT PK_MISREPOR PRIMARY KEY (US_CODIGO,RE_CODIGO)
GO

CREATE TABLE ROL(                             /* Roles */
	RO_CODIGO           Codigo,           /* C�digo */
	RO_NOMBRE           Descripcion,      /* Nombre */
	RO_DESCRIP          Accesos,          /* Descripci�n */
	RO_NIVEL            FolioChico        /* Nivel */
)
GO

ALTER TABLE ROL
	ADD CONSTRAINT PK_ROL PRIMARY KEY ( RO_CODIGO )
GO

CREATE TABLE USER_ROL(
	US_CODIGO           Usuario,          /* Roles De Usuarios */
	RO_CODIGO           Codigo,           /* Rol ( FK ROL ) */
	UR_PPAL             Booleano          /* Rol Principal */
)
GO

ALTER TABLE USER_ROL
       ADD CONSTRAINT PK_USER_ROL PRIMARY KEY (US_CODIGO, RO_CODIGO )
GO

CREATE TABLE WACCION(                         /* Acciones */
	WA_CODIGO           Codigo,           /* C�digo */
	WA_NOMBRE           Observaciones,    /* Nombre */
	WA_DESCRIP          Memo              /* Descripci�n */
)

ALTER TABLE WACCION
      ADD CONSTRAINT PK_WACCION PRIMARY KEY( WA_CODIGO )
GO

CREATE TABLE WARCHIVO(                       /* Archivos De Procesos */
	WP_FOLIO            FolioGrande,     /* Proceso ( FK WPROCESO ) */
	WH_ORDEN            FolioChico,      /* Orden */
	WH_NOMBRE           Accesos,         /* Nombre */
	WH_RUTA             Accesos          /* URL */
)
GO

ALTER TABLE WARCHIVO
      ADD CONSTRAINT PK_WARCHIVO PRIMARY KEY( WP_FOLIO, WH_ORDEN )
GO

CREATE TABLE WMENSAJE(                       /* Mensajes */
	WN_GUID            GUID,             /* Identificador */
	WP_FOLIO           FolioGrande,      /* Proceso ( FK WPROCESO, WPROSTEP ) */
	WE_ORDEN           FolioChico,       /* Paso ( FK WPROSTEP ) */
	WT_GUID            NullGUID,         /* Tarea ( FK WTAREA ) */
	WN_USR_INI         Usuario,          /* Usuario Original ( FK USUARIO ) */
	WN_USR_FIN         Usuario,          /* Usuario Responsable ( FK USUARIO ) */
	WN_FECHA           Fecha,            /* Fecha */
	WN_NOTAS           Memo,             /* Observaciones */
	WN_MENSAJE         Descripcion,      /* Mensaje */
	WN_LEIDO           Booleano          /* Ya Fu� Procesado */
)
GO

ALTER TABLE WMENSAJE
      ADD CONSTRAINT PK_WMENSAJE PRIMARY KEY( WN_GUID )
GO

CREATE TABLE WMODSTEP(                       /* Pasos de Modelo */
	WM_CODIGO          Codigo,           /* Modelo ( FK WMODELO ) */
	WE_ORDEN           FolioChico,       /* Paso */
	WA_CODIGO          Codigo,           /* Acci�n ( FK WACCION ) */
	WE_NOMBRE          Observaciones,    /* Nombre */
	WE_DESCRIP         Memo,             /* Descripci�n */
	WE_TIPDEST         Status,           /* Responsabilidad: Tipo ( ZetaCommonLists.eWFNotificacion ) */
	WE_USRDEST         Observaciones,    /* Responsabilidad: Lista */
	WE_TIPNOT1         Status,           /* Notificaci�n Inicial: Tipo ( ZetaCommonLists.eWFNotificacion ) */
	WE_USRNOT1         Observaciones,    /* Notificaci�n Inicial: Lista */
	WE_TIPNOT2         Status,           /* Notificaci�n Final: Tipo ( ZetaCommonLists.eWFNotificacion ) */
	WE_USRNOT2         Observaciones,    /* Notificaci�n Final: Lista */
	WE_TIPNOT3         Status,           /* Notificaci�n Cancelaci�n: Tipo ( ZetaCommonLists.eWFNotificacion ) */
	WE_USRNOT3         Observaciones,    /* Notificaci�n Cancelaci�n: Lista */
	WE_XSL_ACT         NombreArchivo,    /* XSL Notificaci�n de Activaci�n */
	WE_XSL_CAN         NombreArchivo,    /* XSL Notificaci�n de Cancelaci�n */
	WE_XSL_TER         NombreArchivo,    /* XSL Notificaci�n de Terminaci�n */
	WE_XSL_TAR         NombreArchivo     /* XSL Aviso */
)
GO

ALTER TABLE WMODSTEP
      ADD CONSTRAINT PK_WMODSTEP PRIMARY KEY( WM_CODIGO, WE_ORDEN )
GO

CREATE FUNCTION DBO.PASOS_MODELO( @WM_CODIGO Codigo ) RETURNS Integer
AS
BEGIN
     return ( select COUNT(*) from WMODSTEP where ( WM_CODIGO = @WM_CODIGO ) )
END
GO

CREATE TABLE WMODELO (                       /* Modelos */
	WM_CODIGO          Codigo,           /* C�digo */
	WM_NOMBRE          Observaciones,    /* Nombre */
	WM_DESCRIP         Memo,             /* Descripci�n */
	WM_TIPNOT1         Status,           /* Notificaci�n Inicial: Tipo ( ZetaCommonLists.eWFNotificacion ) */
	WM_USRNOT1         Observaciones,    /* Notificaci�n Inicial: Lista */
	WM_TIPNOT2         Status,           /* Notificaci�n Final: Tipo ( ZetaCommonLists.eWFNotificacion ) */
	WM_USRNOT2         Observaciones,    /* Notificaci�n Final: Lista */
	WM_TIPNOT3         Status,           /* Notificaci�n Cancelaci�n: Tipo ( ZetaCommonLists.eWFNotificacion ) */
	WM_USRNOT3         Observaciones,    /* Notificaci�n Cancelaci�n: Lista */
	WM_URL             Accesos,          /* URL Para Solicitar */
	WM_URL_DEL         Accesos,          /* URL Para Delegar */
	WM_URL_EXA         Accesos,          /* URL Para Consultar */
	WM_URL_OK          Accesos,          /* URL Para Aprobar */
	WM_STATUS          Status,           /* Status ( ZetaCommonLists.eWFStatusModelo ) */
	WM_PASOS           as ( DBO.PASOS_MODELO( WM_CODIGO ) ),  /* Pasos Totales */
	WM_XSL_ACT         NombreArchivo,    /* XSL Notificaci�n de Activaci�n */
	WM_XSL_CAN         NombreArchivo,    /* XSL Notificaci�n de Cancelaci�n */
	WM_XSL_TER         NombreArchivo,    /* XSL Notificaci�n de Terminaci�n */
        WM_FORMATO         NombreArchivo     /* Archivo Fuente */
)
GO

ALTER TABLE WMODELO
      ADD CONSTRAINT PK_WMODELO PRIMARY KEY( WM_CODIGO )
GO

CREATE TABLE WOUTBOX(                        /* Buz�n */
	WO_GUID            GUID,             /* Identificador */
	WT_GUID            NullGUID,         /* Tarea ( FK WTAREA ) */
	WP_FOLIO           FolioGrande,      /* Proceso ( FK WPROCESO ) */
	WO_TO              Memo,             /* Destinatarios */
	WO_CC              Memo,             /* Con Copia A */
	WO_SUBJECT         Accesos,          /* Tema */
	WO_BODY            Memo,             /* Texto */
	WO_FROM_NA         Accesos,          /* Remitente - Nombre */
	WO_FROM_AD         Accesos,          /* Remitente - Direcci�n */
	WO_SUBTYPE         Status,           /* Formato ( ZetaCommonList.eEmailType ) */
        WO_STATUS          Status,           /* Status ( ZetaCommonLists.eWFCorreoStatus ) */
	WO_ENVIADO         Booleano,         /* Ya Fu� Enviado */
	WO_FEC_IN          Fecha,            /* Fecha De Creaci�n */
	WO_FEC_OUT         Fecha,            /* Fecha De Env�o */
	WO_TSTATUS         Status,
	WO_TAVANCE         FolioChico,
	US_ENVIA           Usuario,          /* Remitente - Usuario ( FK USUARIO ) */
        US_RECIBE          Usuario,          /* Destinatario - Usuario ( FK USUARIO ) */
        WO_LOG             Formula           /* Bit�cora */
)
GO

ALTER TABLE WOUTBOX
      ADD CONSTRAINT PK_WOUTBOX PRIMARY KEY( WO_GUID )
GO

CREATE TABLE WPROCESO(                       /* Procesos */
	WP_FOLIO           FolioGrande,      /* Proceso ( FK WPROCESO ) */
	WM_CODIGO          Codigo,           /* Modelo ( FK WMODELO ) */
	WP_NOMBRE          Observaciones,    /* Nombre */
	WP_USR_INI         Usuario,          /* Usuario Solicitante ( FK USUARIO ) */
	WP_FEC_INI         Fecha,            /* Fecha De Inicio */
	WP_FEC_FIN         Fecha,            /* Ultima Actividad */
	WP_STATUS          Status,           /* Status ( ZetaCommonLists.eWFProcesoStatus ) */
	WP_PASOS           FolioChico,       /* Pasos Totales */
	WP_PASO            FolioChico,       /* Paso Actual */
	WP_XMLDATA         XMLData,
	WP_NOTAS           Notas,            /* Observaciones */
	WP_DATA            Binario
)
GO

ALTER TABLE WPROCESO
      ADD CONSTRAINT PK_WPROCESO PRIMARY KEY( WP_FOLIO )
GO

CREATE TABLE WPROSTEP(                       /* Paso de Proceso */
	WP_FOLIO           FolioGrande,      /* Proceso ( FK WPROCESO ) */
	WE_ORDEN           FolioChico,       /* Paso */
	WA_CODIGO          Codigo,           /* Acci�n ( FK WACCION ) */
	WM_CODIGO          Codigo,           /* Modelo ( FK WMODELO ) */
	WS_NOMBRE          Observaciones,    /* Nombre  */
	WS_STATUS          Status,           /* Status ( ZetaCommonLists.eWFPasoProcesoStatus ) */
	WS_USR_INI         Usuario,          /* Usuario Original ( FK USUARIO ) */
	WS_FEC_INI         Fecha,            /* Fecha de Inicio */
	WS_FEC_FIN         Fecha,            /* Fecha De Terminaci�n */
	WS_NEXT            FolioChico        /* Siguiente Paso */
)
GO

ALTER TABLE WPROSTEP
      ADD CONSTRAINT PK_WPROSTEP PRIMARY KEY( WP_FOLIO, WE_ORDEN )
GO

CREATE TABLE WTAREA(                         /* Tareas */
	WT_GUID            GUID,             /* Identificador */
	WP_FOLIO           FolioGrande,      /* Proceso ( FK WPROCESO, WPROSTEP ) */
	WE_ORDEN           FolioChico,       /* Paso ( FK WPROSTEP ) */
	WT_DESCRIP         Observaciones,    /* Descripci�n */
	WT_STATUS          Status,           /* Status ( ZetaCommonLists.eWFTareaStatus ) */
	WT_FEC_INI         Fecha,            /* Fecha De Inicio */
	WT_FEC_FIN         Fecha,            /* Fecha De Terminaci�n */
	WT_USR_ORI         Usuario,          /* Usuario Original */
	WT_USR_DES         Usuario,          /* Usuario Responsable */
	WT_NOTAS           Notas,            /* Observaciones */
	WT_AVANCE          FolioChico        /* Avance */
)
GO

ALTER TABLE WTAREA
      ADD CONSTRAINT PK_WTAREA PRIMARY KEY( WT_GUID )
GO

CREATE TABLE WMOD_ROL(                       /* Roles Asignados a Modelos ( y viceversa ) */
       WM_CODIGO           Codigo,           /* C�digo del Modelo */
       RO_CODIGO           Codigo            /* C�digo del Rol */
)
GO

ALTER TABLE WMOD_ROL
      ADD CONSTRAINT PK_WMOD_ROL PRIMARY KEY( WM_CODIGO, RO_CODIGO )
GO

ALTER TABLE CAMPOREP
       ADD CONSTRAINT FK_CampoRep_Reporte
       FOREIGN KEY (RE_CODIGO)
       REFERENCES REPORTE
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

ALTER TABLE MISREPOR
       ADD CONSTRAINT FK_MISREPOR_REPORTE
       FOREIGN KEY (RE_CODIGO)
       REFERENCES REPORTE
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

ALTER TABLE WARCHIVO
       ADD CONSTRAINT FK_WARCHIVO_WPROCESO
       FOREIGN KEY (WP_FOLIO)
       REFERENCES WPROCESO
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

ALTER TABLE WMODSTEP
       ADD CONSTRAINT FK_WMODSTEP_WMODELO
       FOREIGN KEY (WM_CODIGO)
       REFERENCES WMODELO
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

ALTER TABLE WMODSTEP
       ADD CONSTRAINT FK_WMODSTEP_WACCION
       FOREIGN KEY (WA_CODIGO)
       REFERENCES WACCION
       ON DELETE NO ACTION
       ON UPDATE CASCADE;
GO

ALTER TABLE WPROSTEP
       ADD CONSTRAINT FK_WPROSTEP_WPROCESO
       FOREIGN KEY (WP_FOLIO)
       REFERENCES WPROCESO
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

ALTER TABLE WTAREA
       ADD CONSTRAINT FK_WTAREA_WPROSTEP
       FOREIGN KEY (WP_FOLIO, WE_ORDEN)
       REFERENCES WPROSTEP
       ON DELETE NO ACTION
       ON UPDATE CASCADE;
GO

ALTER TABLE USER_ROL
       ADD CONSTRAINT FK_USER_ROL_ROL
       FOREIGN KEY (RO_CODIGO)
       REFERENCES ROL
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

ALTER TABLE USER_ROL
       ADD CONSTRAINT FK_USER_ROL_USUARIO
       FOREIGN KEY (US_CODIGO)
       REFERENCES USUARIO
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

ALTER TABLE WMOD_ROL
       ADD CONSTRAINT FK_WMOD_ROL_MODELO
       FOREIGN KEY (WM_CODIGO)
       REFERENCES WMODELO
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

ALTER TABLE WMOD_ROL
       ADD CONSTRAINT FK_WMOD_ROL_ROL
       FOREIGN KEY (RO_CODIGO)
       REFERENCES ROL
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

CREATE FUNCTION DBO.GetNombreUsuario( @Usuario Usuario ) RETURNS Descripcion
AS
BEGIN
  declare @Nombre Descripcion;
  select @Nombre = US_NOMBRE from USUARIO where ( US_CODIGO = @Usuario );
  return @Nombre;
END
GO

CREATE VIEW V_JEFES( US_CODIGO, US_NOMBRE )
as
select U.US_JEFE,
       ( select U2.US_NOMBRE from USUARIO U2 where ( U2.US_CODIGO = U.US_JEFE ) ) as US_NOMBRE
from USUARIO U where ( U.US_JEFE is not NULL ) and ( U.US_JEFE <> 0 )
GO

CREATE VIEW V_PADRE( GR_CODIGO, GR_DESCRIP )
as
select G.GR_PADRE,
       ( select G2.GR_DESCRIP from GRUPO G2 where ( G2.GR_CODIGO = G.GR_PADRE ) ) as GR_DESCRIP
from GRUPO G where ( G.GR_PADRE is not NULL ) and ( G.GR_PADRE <> 0 )
GO

/**/
CREATE VIEW V_PASOS( WP_FOLIO,               /**/
                     WM_CODIGO,              /**/
                     WP_NOMBRE,              /**/
                     WP_FEC_INI,             /**/
                     WP_FEC_FIN,             /**/
                     WP_STATUS,              /**/
                     WP_PASO,                /**/
                     WP_PASOS,               /**/
                     WP_USR_INI,             /**/
                     WP_NOM_INI,             /**/
                     WP_NOTAS,               /**/
                     WE_ORDEN,               /* Paso ( FK WPROSTEP ) */
                     WS_NOMBRE,              /**/
                     WS_STATUS,              /**/
                     WS_USR_INI,             /**/
                     WS_NOM_INI,             /**/
                     WS_FEC_INI,             /**/
                     WS_FEC_FIN,             /**/
                     WT_GUID,                /* Tarea ( FK WTAREA ) */
                     WT_DESCRIP,             /* Tarea: Descripci�n */
                     WT_STATUS,              /**/
                     WT_FEC_INI,             /**/
                     WT_FEC_FIN,             /**/
                     WT_USR_ORI,             /**/
                     WT_NOM_ORI,             /**/
                     WT_USR_DES,             /**/
                     WT_NOM_DES,             /**/
                     WT_NOTAS,               /**/
                     WT_AVANCE,              /**/
                     WM_DESCRIP,             /* Modelo: Descripci�n */
                     WE_DESCRIP )            /* Paso De Modelo: Descripci�n */
as
select WPROCESO.WP_FOLIO,
       WPROCESO.WM_CODIGO,
       WPROCESO.WP_NOMBRE,
       WPROCESO.WP_FEC_INI,
       WPROCESO.WP_FEC_FIN,
       WPROCESO.WP_STATUS,
       WPROCESO.WP_PASO,
       WPROCESO.WP_PASOS,
       WPROCESO.WP_USR_INI,
       DBO.GetNombreUsuario( WPROCESO.WP_USR_INI ),
       WPROCESO.WP_NOTAS,

       WPROSTEP.WE_ORDEN,
       WPROSTEP.WS_NOMBRE,
       WPROSTEP.WS_STATUS,
       WPROSTEP.WS_USR_INI,
       DBO.GetNombreUsuario( WPROSTEP.WS_USR_INI ),
       WPROSTEP.WS_FEC_INI,
       WPROSTEP.WS_FEC_FIN,

       WTAREA.WT_GUID,
       WTAREA.WT_DESCRIP,
       WTAREA.WT_STATUS,
       WTAREA.WT_FEC_INI,
       WTAREA.WT_FEC_FIN,
       WTAREA.WT_USR_ORI,
       DBO.GetNombreUsuario( WTAREA.WT_USR_ORI ),
       WTAREA.WT_USR_DES,
       DBO.GetNombreUsuario( WTAREA.WT_USR_DES ),
       WTAREA.WT_NOTAS,
       WTAREA.WT_AVANCE,

       WMODELO.WM_DESCRIP,
       WMODSTEP.WE_DESCRIP
from WPROSTEP
join WPROCESO on ( WPROSTEP.WP_FOLIO = WPROCESO.WP_FOLIO )
join WMODELO on ( WPROCESO.WM_CODIGO = WMODELO.WM_CODIGO )
join WMODSTEP on ( WPROSTEP.WM_CODIGO = WMODSTEP.WM_CODIGO ) and ( WPROSTEP.WE_ORDEN = WMODSTEP.WE_ORDEN )
left outer join WTAREA on ( WPROSTEP.WP_FOLIO = WTAREA.WP_FOLIO ) and ( WPROSTEP.WE_ORDEN = WTAREA.WE_ORDEN )
GO

/**/
CREATE VIEW V_PROCESO( WP_FOLIO,             /**/
                       WM_CODIGO,            /**/
                       WP_NOMBRE,            /**/
                       WP_FEC_INI,           /**/
                       WP_FEC_FIN,           /**/
                       WP_STATUS,            /**/
                       WP_PASO,              /**/
                       WP_PASOS,             /**/
                       WP_USR_INI,           /**/
                       WP_NOM_INI,           /**/
                       WP_NOTAS,             /**/
                       WE_ORDEN,             /* Paso ( FK WPROSTEP ) */
                       WS_NOMBRE,            /**/
                       WS_STATUS,            /**/
                       WS_USR_INI,           /**/
                       WS_NOM_INI,           /**/
                       WS_FEC_INI,           /**/
                       WS_FEC_FIN,           /**/
                       WT_GUID,              /* Tarea ( FK WTAREA ) */
                       WT_DESCRIP,           /* Tarea: Descripci�n */
                       WT_STATUS,            /**/
                       WT_FEC_INI,           /**/
                       WT_FEC_FIN,           /**/
                       WT_USR_ORI,           /**/
                       WT_NOM_ORI,           /**/
                       WT_USR_DES,           /**/
                       WT_NOM_DES,           /**/
                       WT_NOTAS,             /**/
                       WT_AVANCE,            /**/
                       WM_DESCRIP,           /* Modelo: Descripci�n */
                       WE_DESCRIP,           /* Paso de Modelo: Descripci�n */
                       WM_URL_EXA,           /**/
                       WM_URL_DEL )          /**/
AS
select WPROCESO.WP_FOLIO,
       WPROCESO.WM_CODIGO,
       WPROCESO.WP_NOMBRE,
       WPROCESO.WP_FEC_INI,
       WPROCESO.WP_FEC_FIN,
       WPROCESO.WP_STATUS,
       WPROCESO.WP_PASO,
       WPROCESO.WP_PASOS,
       WPROCESO.WP_USR_INI,
       DBO.GetNombreUsuario( WPROCESO.WP_USR_INI ),
       WPROCESO.WP_NOTAS,

       WPROSTEP.WE_ORDEN,
       WPROSTEP.WS_NOMBRE,
       WPROSTEP.WS_STATUS,
       WPROSTEP.WS_USR_INI,
       DBO.GetNombreUsuario( WPROSTEP.WS_USR_INI ),
       WPROSTEP.WS_FEC_INI,
       WPROSTEP.WS_FEC_FIN,

       WTAREA.WT_GUID,
       WTAREA.WT_DESCRIP,
       WTAREA.WT_STATUS,
       WTAREA.WT_FEC_INI,
       WTAREA.WT_FEC_FIN,
       WTAREA.WT_USR_ORI,
       DBO.GetNombreUsuario( WTAREA.WT_USR_ORI ),
       WTAREA.WT_USR_DES,
       DBO.GetNombreUsuario( WTAREA.WT_USR_DES ),
       WTAREA.WT_NOTAS,
       WTAREA.WT_AVANCE,

       WMODELO.WM_DESCRIP,
       WMODSTEP.WE_DESCRIP,
       WMODELO.WM_URL_EXA,
       WMODELO.WM_URL_DEL
from WPROCESO
join WMODELO on ( WPROCESO.WM_CODIGO = WMODELO.WM_CODIGO )
left outer join WPROSTEP on ( WPROCESO.WP_FOLIO = WPROSTEP.WP_FOLIO ) and ( WPROCESO.WP_PASO = WPROSTEP.WE_ORDEN )
left outer join WTAREA on ( WPROSTEP.WP_FOLIO = WTAREA.WP_FOLIO ) and ( WPROSTEP.WE_ORDEN = WTAREA.WE_ORDEN )
left outer join WMODSTEP on ( WPROSTEP.WM_CODIGO = WMODSTEP.WM_CODIGO ) and ( WPROSTEP.WE_ORDEN = WMODSTEP.WE_ORDEN )
GO

/**/
CREATE VIEW V_TAREA( WP_FOLIO,               /**/
                     WM_CODIGO,              /**/
                     WP_NOMBRE,              /**/
                     WP_FEC_INI,             /**/
                     WP_FEC_FIN,             /**/
                     WP_STATUS,              /**/
                     WP_PASO,                /**/
                     WP_PASOS,               /**/
                     WP_USR_INI,             /**/
                     WP_NOM_INI,             /**/
                     WP_NOTAS,               /**/
                     WE_ORDEN,               /* Paso ( FK WPROSTEP ) */
                     WS_NOMBRE,              /**/
                     WS_STATUS,              /**/
                     WS_USR_INI,             /**/
                     WS_NOM_INI,             /**/
                     WS_FEC_INI,             /**/
                     WS_FEC_FIN,             /**/
                     WT_GUID,                /* Tarea ( FK WTAREA ) */
                     WT_DESCRIP,             /* Tarea: Descripci�n */
                     WT_STATUS,              /**/
                     WT_FEC_INI,             /**/
                     WT_FEC_FIN,             /**/
                     WT_USR_ORI,             /**/
                     WT_NOM_ORI,             /**/
                     WT_USR_DES,             /**/
                     WT_NOM_DES,             /**/
                     WT_NOTAS,               /**/
                     WT_AVANCE,              /**/
                     WM_DESCRIP,             /**/
                     WE_DESCRIP,             /* Paso de Modelo: Descripci�n */
                     WM_URL,                 /**/
                     WM_URL_EXA,             /**/
                     WM_URL_OK,              /**/
                     WM_URL_DEL )            /**/
AS
select WPROCESO.WP_FOLIO,
       WPROCESO.WM_CODIGO,
       WPROCESO.WP_NOMBRE,
       WPROCESO.WP_FEC_INI,
       WPROCESO.WP_FEC_FIN,
       WPROCESO.WP_STATUS,
       WPROCESO.WP_PASO,
       WPROCESO.WP_PASOS,
       WPROCESO.WP_USR_INI,
       DBO.GetNombreUsuario( WPROCESO.WP_USR_INI ),
       WPROCESO.WP_NOTAS,

       WPROSTEP.WE_ORDEN,
       WPROSTEP.WS_NOMBRE,
       WPROSTEP.WS_STATUS,
       WPROSTEP.WS_USR_INI,
       DBO.GetNombreUsuario( WPROSTEP.WS_USR_INI ),
       WPROSTEP.WS_FEC_INI,
       WPROSTEP.WS_FEC_FIN,

       WTAREA.WT_GUID,
       WTAREA.WT_DESCRIP,
       WTAREA.WT_STATUS,
       WTAREA.WT_FEC_INI,
       WTAREA.WT_FEC_FIN,
       WTAREA.WT_USR_ORI,
       DBO.GetNombreUsuario( WTAREA.WT_USR_ORI ),
       WTAREA.WT_USR_DES,
       DBO.GetNombreUsuario( WTAREA.WT_USR_DES ),
       WTAREA.WT_NOTAS,
       WTAREA.WT_AVANCE,

       WMODELO.WM_DESCRIP,
       WMODSTEP.WE_DESCRIP,
       WMODELO.WM_URL,
       WMODELO.WM_URL_EXA,
       WMODELO.WM_URL_OK,
       WMODELO.WM_URL_DEL

from WTAREA
join WPROSTEP on ( WPROSTEP.WP_FOLIO = WTAREA.WP_FOLIO ) and ( WPROSTEP.WE_ORDEN = WTAREA.WE_ORDEN )
join WPROCESO on ( WTAREA.WP_FOLIO = WPROCESO.WP_FOLIO )
join WMODELO on ( WPROCESO.WM_CODIGO = WMODELO.WM_CODIGO )
join WMODSTEP on ( WPROSTEP.WM_CODIGO = WMODSTEP.WM_CODIGO ) and ( WPROSTEP.WE_ORDEN = WMODSTEP.WE_ORDEN )
GO

create view V_MODELOS( WM_CODIGO, RO_CODIGO, US_CODIGO ) as
select WMOD_ROL.WM_CODIGO, WMOD_ROL.RO_CODIGO, USER_ROL.US_CODIGO
from WMOD_ROL
join USER_ROL on ( USER_ROL.RO_CODIGO = WMOD_ROL.RO_CODIGO )
GO

CREATE TRIGGER TU_QUERYS ON QUERYS AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( QU_CODIGO )
  BEGIN
	declare @NewCodigo Condicion, @OldCodigo Condicion;

	select @NewCodigo = QU_CODIGO from Inserted;
        select @OldCodigo = QU_CODIGO from Deleted;

	IF ( @OldCodigo <> '' ) AND ( @NewCodigo <> '' )
	BEGIN
	     UPDATE REPORTE SET QU_CODIGO = @NewCodigo WHERE QU_CODIGO = @OldCodigo;
	END
  END
END
GO

DROP TRIGGER TD_USUARIO
GO

CREATE TRIGGER TD_USUARIO ON USUARIO AFTER DELETE AS
BEGIN
     declare @OldUS_CODIGO Usuario;
     select @OldUS_CODIGO = US_CODIGO from Deleted;
     delete from PAGINA where ( PAGINA.US_CODIGO = @OldUS_CODIGO );
     update USUARIO set US_JEFE = 0 where ( US_JEFE = @OldUS_CODIGO );
     update USUARIO set US_PAG_DEF = 0 where ( US_PAG_DEF = @OldUS_CODIGO );
END
GO

DROP TRIGGER TU_USUARIO
GO

CREATE TRIGGER TU_USUARIO ON USUARIO AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE( US_CODIGO )
     BEGIN
	  declare @NewUS_CODIGO Usuario, @OldUS_CODIGO Usuario;

	  select @NewUS_CODIGO = US_CODIGO from Inserted;
          select @OldUS_CODIGO = US_CODIGO from Deleted;
          update PAGINA set PAGINA.US_CODIGO = @NewUS_CODIGO
          where ( PAGINA.US_CODIGO = @OldUS_CODIGO );
          update USUARIO set US_JEFE = @NewUS_CODIGO
          where ( US_JEFE = @OldUS_CODIGO );
          update USUARIO set US_PAG_DEF = @NewUS_CODIGO
          where ( US_PAG_DEF = @OldUS_CODIGO );
     end
END
GO

CREATE PROCEDURE DBO.INIT_PROCESS_LOG(
  @Proceso Integer,
  @Usuario Smallint,
  @Fecha   DateTime,
  @Hora    Varchar(8),
  @Maximo  Integer,
  @Params   varchar(255),
  @Folio   Integer OUTPUT )
AS
BEGIN
  SET NOCOUNT ON;
  select @Folio = MAX( PC_NUMERO ) from PROCESO

  if ( @Folio is NULL )
    select @Folio = 1
  else
    select @Folio = @Folio + 1

  insert into PROCESO ( PC_PROC_ID,
                        PC_NUMERO,
                        US_CODIGO,
                        PC_FEC_INI,
                        PC_HOR_INI,
                        PC_MAXIMO,
                        PC_ERROR,
			PC_PARAMS ) values
                         ( @Proceso,
                           @Folio,
                           @Usuario,
                           @Fecha,
                           @Hora,
                           @Maximo,
                           'A',
			   @Params )
END
GO

CREATE PROCEDURE DBO.UPDATE_PROCESS_LOG(
  @Proceso  Integer,
  @Empleado Integer,
  @Paso     Integer,
  @Fecha    DateTime,
  @Hora     VARCHAR( 8 ),
  @Status   SmallInt OUTPUT )
AS
BEGIN
  SET NOCOUNT ON;
  declare @Bandera Char( 1 )

  update PROCESO set
         CB_CODIGO = @Empleado,
         PC_PASO = @Paso,
         PC_FEC_FIN = @Fecha,
         PC_HOR_FIN = @Hora
  where ( PC_NUMERO = @Proceso )

  select @Bandera = PC_ERROR
  from   PROCESO
  where ( PC_NUMERO = @Proceso )

  if ( @Bandera = 'A' )
    select @Status = 0
  else
    select @Status = 1
END
GO

CREATE PROCEDURE DBO.SP_REFRESH_DICCION
AS
BEGIN
  SET NOCOUNT ON;
END
GO

CREATE PROCEDURE DBO.RECOMPILA_TODO
as
begin
     set NOCOUNT on;
     declare @TABLA sysname
     declare TEMPORAL cursor for
     select TABLE_NAME from INFORMATION_SCHEMA.TABLES
     order by TABLE_NAME
     open TEMPORAL
     fetch next from TEMPORAL into @TABLA
     while ( @@FETCH_STATUS = 0 )
     begin
          exec SP_RECOMPILE @TABLA
          fetch next from TEMPORAL into @TABLA
     end
     close TEMPORAL
     deallocate TEMPORAL
end
GO

CREATE FUNCTION DBO.GetDuracion( @FechaIni Fecha, @FechaFin Fecha ) RETURNS Descripcion
AS
BEGIN
     declare @Duracion Descripcion
     declare @Diferencia Integer

     set @Diferencia = DATEDIFF( dd, @FechaIni, @FechaFin )
     if ( @Diferencia > 1 )
     begin
          if ( @Diferencia >= 60 )
             set @Duracion = STR( @Diferencia / 30 ) + ' Meses'
          else
              set @Duracion = STR( @Diferencia ) + ' D�as'
     end
     else
     begin
          set @Diferencia = DATEDIFF( hh, @FechaIni, @FechaFin )
          if ( @Diferencia > 1 )
             set @Duracion = STR( @Diferencia ) + ' Horas'
          else
          begin
               set @Diferencia = DATEDIFF( mi, @FechaIni, @FechaFin )
               if ( @Diferencia > 1 )
                  set @Duracion = STR( @Diferencia ) +  ' Minutos'
               else
                   if ( @Diferencia = 1 )
                      set @Duracion = '1 Minuto'
                   else
                       set @Duracion = ''
          end
     end
     return LTRIM( @Duracion )
END
GO

CREATE FUNCTION DBO.GetStatusProceso( @Status Status ) RETURNS Descripcion AS
BEGIN
     declare @DescStatus Descripcion
     set @DescStatus = case @Status
                            when 0 then 'Inactivo'
                            when 1 then 'Activo'
                            when 2 then 'Termin�'
                            when 3 then 'Cancelado'
                            when 4 then 'Suspendido'
                            else ''
                       end;
     return @DescStatus;
END
GO

CREATE FUNCTION DBO.GetStatusTarea( @Status Status ) RETURNS Descripcion AS
BEGIN
     declare @DescStatus Descripcion
     set @DescStatus = CASE @Status
                             when 0 then 'Inactiva'
                             when 1 then 'Activa'
                             when 2 then 'Esperando'
                             when 3 then 'Termin�'
                             when 4 then 'Cancelada'
                             else ''
                       END
     return @DescStatus
END
GO

CREATE FUNCTION DBO.GetUsuarios( @Usuario Usuario, @TipoLista Status, @Detalle   Observaciones)
RETURNS @retUsuarios TABLE ( US_CODIGO SmallInt PRIMARY KEY, US_NOMBRE Varchar(50) NOT NULL )
AS
BEGIN
   DECLARE @Jefe Usuario
   /* TipoLista: 1=Rol, 2=Usuario Espec�fico, 3=Jefe, 4=Jefe del Jefe, 5=Subordinados Directos, 6=Nivel, 7=Mismo */
   if ( @TipoLista = 1 )
     INSERT @retUsuarios
     SELECT USUARIO.US_CODIGO, US_NOMBRE
     FROM   USUARIO, USER_ROL
     WHERE  USUARIO.US_CODIGO = USER_ROL.US_CODIGO AND
            USER_ROL.RO_CODIGO = @Detalle

   else if ( @TipoLista = 2 )
     INSERT @retUsuarios
     SELECT US_CODIGO, US_NOMBRE
     FROM   USUARIO
     WHERE  US_CODIGO = @Detalle

   else if ( @TipoLista = 3 or @TipoLista = 4 )
   BEGIN
     SELECT @Jefe = US_JEFE
     FROM   USUARIO
     WHERE  US_CODIGO = @Usuario

     if ( @TipoLista = 4 )
       SELECT @Jefe = US_JEFE
       FROM   USUARIO
       WHERE  US_CODIGO = @Jefe

     /* Si no tiene jefe, regresa el mismo
     if ( @Jefe = 0 )
       set @Jefe = @Usuario */

     INSERT @retUsuarios
     SELECT US_CODIGO, US_NOMBRE
     FROM   USUARIO
     WHERE  US_CODIGO = @Jefe
   END
   else if ( @TipoLista = 5 )
     INSERT @retUsuarios
     SELECT US_CODIGO, US_NOMBRE
     FROM   USUARIO
     WHERE  US_JEFE = @Usuario

   else if ( @TipoLista = 6 )
     INSERT @retUsuarios
     SELECT USUARIO.US_CODIGO, US_NOMBRE
     FROM   USUARIO, USER_ROL, ROL
     WHERE  USUARIO.US_CODIGO = USER_ROL.US_CODIGO AND
            USER_ROL.RO_CODIGO = ROL.RO_CODIGO AND
            ROL.RO_NIVEL = @Detalle
   else if ( @TipoLista = 7 )
     INSERT @retUsuarios
     SELECT US_CODIGO, US_NOMBRE
     FROM   USUARIO
     WHERE  US_CODIGO = @Usuario

   RETURN
END
GO

CREATE FUNCTION DBO.GetUsuariosIniciales( @Modelo Codigo, @Usuario Usuario )
RETURNS @retUsuarios TABLE ( US_CODIGO Smallint PRIMARY KEY, US_NOMBRE  Varchar(50) NOT NULL)
AS
BEGIN
   DECLARE @TipoLista Status
   DECLARE @Detalle   Observaciones

   SELECT  @TipoLista = WE_TIPDEST, @Detalle = WE_USRDEST
   FROM    WMODSTEP
   WHERE   WM_CODIGO = @Modelo and WE_ORDEN = 1

   INSERT @retUsuarios
   SELECT US_CODIGO, US_NOMBRE
   FROM   dbo.GetUsuarios( @Usuario, @TipoLista, @Detalle )

   RETURN
END
GO

CREATE FUNCTION DBO.GetUsuariosTarea( @Tarea GUID, @Usuario Usuario, @Offset SmallInt )
RETURNS @retUsuariosTarea TABLE ( US_CODIGO Smallint PRIMARY KEY, US_NOMBRE Varchar(50) NOT NULL)
AS
BEGIN
   DECLARE @UsuarioOriginal Usuario
   DECLARE @TipoLista       Status
   DECLARE @Detalle         Observaciones

   SELECT @TipoLista = WE_TIPDEST,
          @Detalle   = WE_USRDEST,
          @UsuarioOriginal = WP_USR_INI
   FROM   WTAREA, WPROSTEP, WPROCESO, WMODSTEP
   WHERE  ( WTAREA.WT_GUID = @Tarea ) AND
          ( WTAREA.WP_FOLIO = WPROSTEP.WP_FOLIO ) AND ( WTAREA.WE_ORDEN = WPROSTEP.WE_ORDEN ) AND
          ( WTAREA.WP_FOLIO = WPROCESO.WP_FOLIO ) AND
          ( WMODSTEP.WM_CODIGO = WPROSTEP.WM_CODIGO ) AND ( WMODSTEP.WE_ORDEN = WPROSTEP.WE_ORDEN + @Offset )

   insert @retUsuariosTarea select US_CODIGO, US_NOMBRE from DBO.GetUsuarios( @UsuarioOriginal, @TipoLista, @Detalle )

   RETURN
END
GO

CREATE FUNCTION DBO.GetUsuariosSiguientes( @Tarea GUID, @Usuario Usuario )
RETURNS @retUsuariosSig TABLE ( US_CODIGO Smallint PRIMARY KEY, US_NOMBRE Varchar(50) NOT NULL)
AS
BEGIN
     insert @retUsuariosSig select US_CODIGO, US_NOMBRE from DBO.GetUsuariosTarea( @Tarea, @Usuario, 1 )
     RETURN
END
GO

CREATE FUNCTION DBO.GetUsuariosDelega( @Tarea GUID, @Usuario Usuario )
RETURNS @retUsuariosDelega TABLE ( US_CODIGO Smallint PRIMARY KEY, US_NOMBRE Varchar(50) NOT NULL)
AS
BEGIN
     insert @retUsuariosDelega select US_CODIGO, US_NOMBRE from DBO.GetUsuariosTarea( @Tarea, @Usuario, 0 )
     RETURN
END
GO

CREATE PROCEDURE DBO.SP_ACTIVA_WORKFLOW(
  @Folio   FolioGrande,
  @Usuario Usuario,
  @Destino Usuario,
  @Notas   Notas )
AS
BEGIN
     set NOCOUNT ON;
     insert into WMENSAJE( WN_GUID, WP_FOLIO, WE_ORDEN, WT_GUID, WN_USR_INI, WN_USR_FIN, WN_FECHA, WN_NOTAS, WN_MENSAJE, WN_LEIDO )
     select NEWID(), WP_FOLIO, WE_ORDEN, NULL, @Usuario, @Destino, GetDate(), @Notas, 'Activar', 'N'
     from WPROSTEP where ( WP_FOLIO = @Folio ) and ( WE_ORDEN = 1 );
END
GO

CREATE PROCEDURE DBO.SP_ACTUALIZA_TAREA(
  @Guid   GUID,
  @Status Status,
  @Avance FolioChico,
  @Fecha  Fecha )
AS
BEGIN
     set NOCOUNT ON;
     update WTAREA set WT_STATUS  = @Status,
                       WT_AVANCE  = @Avance,
                       WT_FEC_FIN = @Fecha
     where ( WT_GUID = @Guid );
END
GO

CREATE PROCEDURE DBO.SP_BORRA_WMODSTEP( @WM_CODIGO Codigo, @Actual FolioChico )
AS
BEGIN
     set NOCOUNT ON;
     delete from WMODSTEP where ( WM_CODIGO = @WM_CODIGO ) and ( WE_ORDEN = @Actual );
     if ( @@ROWCOUNT > 0 )
     begin
          update WMODSTEP set WE_ORDEN = WE_ORDEN - 1
          where ( WM_CODIGO = @WM_CODIGO ) and ( WE_ORDEN > @Actual )
     end
END
GO

CREATE PROCEDURE DBO.SP_CAMBIA_WMODSTEP( @WM_CODIGO Codigo, @Actual FolioChico, @Nuevo FolioChico )
AS
BEGIN
     set NOCOUNT ON;
     declare @Maximo FolioChico

     IF ( @Nuevo < 1 ) or ( @Actual < 1 ) or ( @Actual = @Nuevo )
             RETURN

     select @Maximo = MAX( WE_ORDEN )
     from   WMODSTEP
     where  WM_CODIGO = @WM_CODIGO

     if ( @Nuevo > @Maximo ) or ( @Actual > @Maximo )
             RETURN

     UPDATE WMODSTEP
     SET    WE_ORDEN = 0
     WHERE  WM_CODIGO = @WM_CODIGO and WE_ORDEN = @Actual

     UPDATE WMODSTEP
     SET    WE_ORDEN = @Actual
     WHERE  WM_CODIGO = @WM_CODIGO and WE_ORDEN = @Nuevo

     UPDATE WMODSTEP
     SET    WE_ORDEN = @Nuevo
     WHERE  WM_CODIGO = @WM_CODIGO and WE_ORDEN = 0
END
GO

CREATE PROCEDURE DBO.SP_CREA_WORKFLOW(
  @Modelo    Codigo,
  @Usuario   Usuario,
  @Destino   Usuario,
  @Data      XmlData,
  @Notas     Notas,
  @Folio     FolioGrande OUTPUT )
AS
BEGIN
  SET NOCOUNT ON;

  DECLARE @AF_ID   Char(6)
  DECLARE @Fecha   DateTime
  SET     @Fecha = GetDate()

  DECLARE @Pasos  FolioChico
  DECLARE @Nombre Observaciones

  -- Obtiene Nombre y # de Pasos
  SELECT  @Pasos = WM_PASOS, @Nombre = WM_NOMBRE
  FROM    WMODELO
  WHERE   WM_CODIGO = @Modelo

  -- Obtiene Folio de siguiente proceso
  SELECT @Folio = MAX( WP_FOLIO )+1
  FROM   WPROCESO
  IF (@Folio IS NULL) select @FOLIO = 1

  -- Crea registro Proceso, padre de Workflow
  INSERT INTO WPROCESO ( WP_FOLIO, WM_CODIGO, WP_NOMBRE, WP_USR_INI, WP_FEC_INI, WP_FEC_FIN, WP_STATUS, WP_PASOS, WP_PASO, WP_XMLDATA, WP_NOTAS )
  VALUES ( @Folio, @Modelo, @Nombre, @Usuario, @Fecha, @Fecha, 0, @Pasos, 0, @Data, @Notas )

  -- Pendiente Validar que se agreg� un registro

  -- Inserta un registro de instancia por cada paso
  INSERT INTO WPROSTEP ( WP_FOLIO, WE_ORDEN, WA_CODIGO, WM_CODIGO, WS_NOMBRE, WS_STATUS )
  SELECT @Folio, WE_ORDEN, WA_CODIGO, WM_CODIGO, WE_NOMBRE, 0
  FROM   WMODSTEP
  WHERE  WM_CODIGO = @Modelo

  -- Asigna el valor de Next para todos los pasos excepto el �ltimo que queda en CERO
  UPDATE WPROSTEP
  SET    WS_NEXT = WE_ORDEN + 1
  WHERE  WP_FOLIO = @Folio and WE_ORDEN < @Pasos

  -- ENVIA EL MENSAJE DE ACTIVACION
  EXEC SP_ACTIVA_WORKFLOW @Folio, @Usuario, @Destino, @Notas
  RETURN
END
GO

CREATE PROCEDURE DBO.SP_ENVIA_OUTBOX(
  @ToAddress   Memo,
  @CCAddress   Memo,
  @Subject     Accesos,
  @Body        Memo,
  @FromName    Accesos,
  @FromAddress Accesos,
  @SubType     Status,
  @GUIDTarea   Uniqueidentifier,
  @IDProceso   FolioGrande,
  @Status      Status,
  @Avance      FolioChico,
  @Envia       Usuario,
  @Recibe      Usuario )
AS
BEGIN
  set NOCOUNT ON;
  insert into WOUTBOX( WO_GUID, WT_GUID, WP_FOLIO, WO_TO, WO_CC, WO_SUBJECT, WO_BODY, WO_FROM_NA, WO_FROM_AD, WO_SUBTYPE, WO_ENVIADO, WO_FEC_IN, WO_TSTATUS, WO_TAVANCE, US_ENVIA, US_RECIBE )
  values ( NEWID(), @GUIDTarea, @IDProceso, @ToAddress, @CCAddress, @Subject, @Body, @FromName, @FromAddress, @SubType, 'N', GetDate(), @Status, @Avance, @Envia, @Recibe )
END
GO

CREATE PROCEDURE DBO.SP_ENVIA_RESPUESTA(
  @Tarea   GUID,
  @Destino Usuario,
  @Accion  Descripcion,
  @Notas   Notas )
AS
BEGIN
  SET NOCOUNT ON;

  insert into WMENSAJE( WN_GUID, WP_FOLIO, WE_ORDEN, WT_GUID, WN_USR_INI, WN_USR_FIN, WN_FECHA, WN_NOTAS, WN_MENSAJE, WN_LEIDO )
  select NEWID(), WP_FOLIO, WE_ORDEN, WT_GUID, WT_USR_ORI, @Destino, GetDate(), @Notas, @Accion, 'N' from WTAREA where ( WT_GUID = @Tarea );

END
GO

CREATE PROCEDURE DBO.SP_DELEGA_TAREA(
  @Tarea   GUID,
  @Destino Usuario,
  @Notas   Notas )
AS
BEGIN
  SET NOCOUNT ON;

  insert into WMENSAJE( WN_GUID, WP_FOLIO, WE_ORDEN, WT_GUID, WN_USR_INI, WN_USR_FIN, WN_FECHA, WN_NOTAS, WN_MENSAJE, WN_LEIDO )
  select NEWID(), WP_FOLIO, WE_ORDEN, WT_GUID, WT_USR_ORI, @Destino, GetDate(), @Notas, 'Delegar', 'N' from WTAREA where ( WT_GUID = @Tarea );

END
GO

insert into WACCION( WA_CODIGO, WA_NOMBRE, WA_DESCRIP ) values ( 'FIRMA', 'Pide Autorizaci�n', 'Env�a eMail solicitando autorizaci�n del documento' )
GO
insert into WACCION( WA_CODIGO, WA_NOMBRE, WA_DESCRIP ) values ( 'GRABA3', 'Registra en TRESS', 'Registra los datos del documento en Sistema TRESS' )
GO



