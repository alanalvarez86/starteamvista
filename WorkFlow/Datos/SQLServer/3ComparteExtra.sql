exec DBO.SP_CREA_WORKFLOW 'PERM', 1, 2, '<DATOS><TIPO>PERMISO</TIPO><EMPRESA>CARRERA</EMPRESA><EMPLEADO>18</EMPLEADO><INICIO>20040916</INICIO><DIAS>10</DIAS><CLASE>2</CLASE><TIPO>FJ</TIPO><OBSERVACIONES>Se Rompio Una Pierna</OBSERVACIONES><REFERENCIA>1005</REFERENCIA></DATOS>', ''
go

CREATE TABLE PROYECTO (
  PR_FOLIO FOLIOGRANDE,
  PR_STATUS STATUS,
  PR_AVANCE FOLIOCHICO,
  PR_CLIENTE FORMULA,
  PR_RESUMEN FORMULA,
  PR_DETALLE MEMO,
  PR_MODULOS FORMULA,
  PR_LIDER DESCRIPCION,
  PR_EQUIPO FORMULA,
  PR_VENTAS DESCRIPCION,
  PR_ANALIZO DESCRIPCION,
  PR_COTIZO DESCRIPCION,
  PR_FEC_INI FECHA,
  PR_FEC_PRO FECHA,
  PR_FEC_FIN FECHA,
  PR_SER_HRP PESOS,
  PR_SER_HRR PESOS,
  PR_PRO_HRP PESOS,
  PR_PRO_HRR PESOS,
  PR_OBSERVA MEMO,
  PR_CL_RESP DESCRIPCION,
  PR_CL_CONT FORMULA,
  PR_CL_CIUD FORMULA
)
GO

ALTER TABLE PROYECTO ADD CONSTRAINT PK_PROYECTO PRIMARY KEY (PR_FOLIO)
GO

CREATE TABLE CTW10005 (
  CUENTA VARCHAR(20) NOT NULL,
  EJE SMALLINT NOT NULL,
  TIPO SMALLINT NOT NULL,
  SALDOINI NUMERIC(15, 2),
  IMP1 NUMERIC(15, 2),
  IMP2 NUMERIC(15, 2),
  IMP3 NUMERIC(15, 2),
  IMP4 NUMERIC(15, 2),
  IMP5 NUMERIC(15, 2),
  IMP6 NUMERIC(15, 2),
  IMP7 NUMERIC(15, 2),
  IMP8 NUMERIC(15, 2),
  IMP9 NUMERIC(15, 2),
  IMP10 NUMERIC(15, 2),
  IMP11 NUMERIC(15, 2),
  IMP12 NUMERIC(15, 2),
  IMP13 NUMERIC(15, 2),
  IMP14 NUMERIC(15, 2),
  CAPTADO CHAR(1)
)
GO

ALTER TABLE CTW10005
  ADD CONSTRAINT PK_CTW10005
  PRIMARY KEY (CUENTA, TIPO)
GO





