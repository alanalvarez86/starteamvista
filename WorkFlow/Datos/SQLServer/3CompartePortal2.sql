exec sp_addtype PathArchivo, 'varchar(150)', 'NOT NULL'
exec sp_bindefault StringVacio, PathArchivo
GO

exec sp_addtype Condicion, 'char(8)', 'NOT NULL'
exec sp_bindefault CodigoVacio, Condicion
go

exec sp_addtype NombreCampo, 'varchar(10)', 'NOT NULL'
exec sp_bindefault StringVacio, NombreCampo
GO

exec sp_addtype TituloCampo, 'varchar(30)', 'NOT NULL'
exec sp_bindefault StringVacio, TituloCampo
GO

exec sp_addtype CamposRequeridos, 'varchar(255)', 'NOT NULL'
exec sp_bindefault StringVacio, CamposRequeridos
GO

exec sp_addtype Tamanio, 'numeric(15,3)', 'NOT NULL'
exec sp_bindefault Cero, Tamanio
GO

exec sp_addtype Mascara, 'varchar(20)', 'NOT NULL'
exec sp_bindefault StringVacio, Mascara
GO

exec sp_addtype WindowsName, 'varchar(32)', 'NOT NULL'
exec sp_bindefault StringVacio, WindowsName
GO

CREATE TABLE DICCION (
       DI_CLASIFI           Status,
       DI_NOMBRE            NombreCampo,
       DI_TITULO            TituloCampo,
       DI_CALC              Status,
       DI_ANCHO             Status,
       DI_MASCARA           Descripcion,
       DI_TFIELD            Status,
       DI_ORDEN             Booleano,
       DI_REQUIER           CamposRequeridos,
       DI_TRANGO            Status,
       DI_NUMERO            Status,
       DI_VALORAC           Descripcion,
       DI_RANGOAC           Status,
       DI_CLAVES            TituloCampo,
       DI_TCORTO            TituloCampo,
       DI_CONFI             Booleano
)
GO

CREATE INDEX XIE1DICCION ON DICCION
(
       DI_CLASIFI,
       DI_CALC
)
GO

ALTER TABLE DICCION
       ADD CONSTRAINT PK_DICCION PRIMARY KEY (DI_CLASIFI, DI_NOMBRE)
GO

CREATE TABLE BITACORA (
       US_CODIGO            Usuario,
       BI_FECHA             Fecha,
       BI_HORA              HoraMinutosSegundos,
       BI_PROC_ID           Status,
       BI_TIPO              Status,
       BI_NUMERO            FolioGrande,
       BI_TEXTO             Observaciones,
       CB_CODIGO            NumeroEmpleado,
       BI_DATA              Memo,
       BI_CLASE             Status,
       BI_FEC_MOV           Fecha
)
GO

CREATE INDEX XIE1BITACORA ON BITACORA
(
       BI_NUMERO
)
GO

CREATE INDEX XIE2BITACORA ON BITACORA
(
       BI_FECHA,
       BI_HORA
)
GO

CREATE TABLE PROCESO (
       PC_PROC_ID           Status,
       PC_NUMERO            FolioGrande,
       US_CODIGO            Usuario,
       PC_FEC_INI           Fecha,
       PC_HOR_INI           HoraMinutosSegundos,
       PC_FEC_FIN           Fecha,
       PC_HOR_FIN           HoraMinutosSegundos,
       PC_ERROR             Booleano,
       CB_CODIGO            NumeroEmpleado,
       PC_MAXIMO            FolioGrande,
       PC_PASO              FolioGrande,
       US_CANCELA           Usuario,
       PC_PARAMS            Formula
)
GO

CREATE INDEX XIE1PROCESO ON PROCESO
(
       PC_FEC_INI,
       PC_ERROR
)
GO


ALTER TABLE PROCESO
       ADD CONSTRAINT PK_PROCESO PRIMARY KEY (PC_NUMERO)
GO

CREATE TABLE QUERYS (
       QU_CODIGO            Condicion,
       QU_DESCRIP           Formula,     
       QU_FILTRO            Formula,
       QU_NIVEL             Status,
       US_CODIGO            Usuario
)
go


ALTER TABLE QUERYS
       ADD CONSTRAINT PK_QUERYS PRIMARY KEY (QU_CODIGO)
go

CREATE TABLE REPORTE (
       RE_CODIGO            FolioChico,
       RE_NOMBRE            Descripcion,
       RE_TIPO              Status,
       RE_TITULO            Titulo,
       RE_ENTIDAD           Status,
       RE_SOLOT             Booleano,
       RE_FECHA             Fecha,
       RE_GENERAL           Descripcion,
       RE_REPORTE           PathArchivo,
       RE_CFECHA            NombreCampo,
       RE_IFECHA            Status,
       RE_HOJA              Status,
       RE_ALTO              Tamanio,
       RE_ANCHO             Tamanio,
       RE_PRINTER           Formula,
       RE_COPIAS            Status,
       RE_PFILE             Status,
       RE_ARCHIVO           PathArchivo,
       RE_VERTICA           Booleano,
       US_CODIGO            Usuario,
       RE_FILTRO            Formula,
       RE_COLNUM            Status,
       RE_COLESPA           Tamanio,
       RE_RENESPA           Tamanio,
       RE_MAR_SUP           Tamanio,
       RE_MAR_IZQ           Tamanio,
       RE_MAR_DER           Tamanio,
       RE_MAR_INF           Tamanio,
       RE_NIVEL             Status,
       RE_FONTNAM           WindowsName,
       RE_FONTSIZ           Status,
       QU_CODIGO            Condicion,
       RE_CLASIFI           Status,
       RE_CANDADO           Status
)
GO

CREATE UNIQUE INDEX XAK1REPORTE ON REPORTE
(
       RE_NOMBRE
)
GO

CREATE INDEX XIE1REPORTE ON REPORTE
(
       RE_CLASIFI,
       RE_NOMBRE
)
GO

ALTER TABLE REPORTE
       ADD CONSTRAINT PK_REPORTE PRIMARY KEY (RE_CODIGO)
GO

CREATE TABLE CAMPOREP (
       RE_CODIGO            FolioChico,
       CR_TIPO              Status,
       CR_POSICIO           Status,
       CR_CLASIFI           Status,
       CR_SUBPOS            Status,
       CR_TABLA             Status,
       CR_TITULO            TituloCampo,
       CR_REQUIER           Formula,
       CR_CALC              Status,
       CR_MASCARA           Mascara,
       CR_ANCHO             Status,
       CR_OPER              Status,
       CR_TFIELD            Status,
       CR_SHOW              Status,
       CR_DESCRIP           NombreCampo,
       CR_COLOR             Status,
       CR_FORMULA           Formula,
       CR_BOLD              Booleano,
       CR_ITALIC            Booleano,
       CR_SUBRAYA           Booleano,
       CR_STRIKE            Booleano,
       CR_ALINEA            Status
)
GO

ALTER TABLE CAMPOREP
       ADD CONSTRAINT PK_CAMPOREP PRIMARY KEY (RE_CODIGO, CR_TIPO, CR_POSICIO,
              CR_SUBPOS)
GO

CREATE TABLE MISREPOR (
       US_CODIGO            Usuario,
       RE_CODIGO            FolioChico )
GO

ALTER TABLE MISREPOR ADD CONSTRAINT PK_MISREPOR PRIMARY KEY (US_CODIGO,RE_CODIGO)
GO

ALTER TABLE CAMPOREP
       ADD CONSTRAINT FK_CampoRep_Reporte
       FOREIGN KEY (RE_CODIGO)
       REFERENCES REPORTE
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

ALTER TABLE MISREPOR
       ADD CONSTRAINT FK_MISREPOR_REPORTE
       FOREIGN KEY (RE_CODIGO)
       REFERENCES REPORTE
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

CREATE TRIGGER TU_QUERYS ON QUERYS AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( QU_CODIGO )
  BEGIN
	declare @NewCodigo Condicion, @OldCodigo Condicion;

	select @NewCodigo = QU_CODIGO from Inserted;
        select @OldCodigo = QU_CODIGO from Deleted;

	IF ( @OldCodigo <> '' ) AND ( @NewCodigo <> '' )
	BEGIN
	     UPDATE REPORTE SET QU_CODIGO = @NewCodigo WHERE QU_CODIGO = @OldCodigo;
	END
  END
END
GO

CREATE PROCEDURE DBO.INIT_PROCESS_LOG(
  @Proceso Integer,
  @Usuario Smallint,
  @Fecha   DateTime,
  @Hora    Varchar(8),
  @Maximo  Integer,
  @Params   varchar(255),
  @Folio   Integer OUTPUT )
AS
BEGIN
  SET NOCOUNT ON;
  select @Folio = MAX( PC_NUMERO ) from PROCESO

  if ( @Folio is NULL )
    select @Folio = 1
  else
    select @Folio = @Folio + 1

  insert into PROCESO ( PC_PROC_ID,
                        PC_NUMERO,
                        US_CODIGO,
                        PC_FEC_INI,
                        PC_HOR_INI,
                        PC_MAXIMO,
                        PC_ERROR,
			PC_PARAMS ) values
                         ( @Proceso,
                           @Folio,
                           @Usuario,
                           @Fecha,
                           @Hora,
                           @Maximo,
                           'A',
			   @Params )
END
GO

CREATE PROCEDURE DBO.UPDATE_PROCESS_LOG(
  @Proceso  Integer,
  @Empleado Integer,
  @Paso     Integer,
  @Fecha    DateTime,
  @Hora     VARCHAR( 8 ),
  @Status   SmallInt OUTPUT )
AS
BEGIN
  SET NOCOUNT ON;
  declare @Bandera Char( 1 )

  update PROCESO set
         CB_CODIGO = @Empleado,
         PC_PASO = @Paso,
         PC_FEC_FIN = @Fecha,
         PC_HOR_FIN = @Hora
  where ( PC_NUMERO = @Proceso )

  select @Bandera = PC_ERROR
  from   PROCESO
  where ( PC_NUMERO = @Proceso )

  if ( @Bandera = 'A' )
    select @Status = 0
  else
    select @Status = 1
END
GO

CREATE PROCEDURE DBO.SP_REFRESH_DICCION
AS
BEGIN
  SET NOCOUNT ON;
END
GO

CREATE PROCEDURE DBO.RECOMPILA_TODO
as
begin
     set NOCOUNT on;
     declare @TABLA sysname
     declare TEMPORAL cursor for
     select TABLE_NAME from INFORMATION_SCHEMA.TABLES
     order by TABLE_NAME
     open TEMPORAL
     fetch next from TEMPORAL into @TABLA
     while ( @@FETCH_STATUS = 0 )
     begin
          exec SP_RECOMPILE @TABLA
          fetch next from TEMPORAL into @TABLA
     end
     close TEMPORAL
     deallocate TEMPORAL
end
GO

insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 1, 'Nombre Del Proyecto', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 2, 'Ruta Ra�z', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 3, 'Ruta Para Documentos', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 4, 'Registros Por P�gina', '0', 2 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 5, 'Ruta Para Im�genes de Noticias', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 6, 'Nombre de la Empresa', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 7, 'Direcci�n', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 8, 'Ciudad', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 9, 'Estado', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 10, 'Ruta del Logo Grande', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 11, 'Ruta del Logo Chico', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 12, 'Art�culo Titular: Noticia Titu', '0', 2 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 13, 'Lista de eMails default', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 14, 'Servidor de eMail POP3', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 15, 'eMail para Auto Response', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 16, 'Refrescado de P�gina', '0', 2 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 17, 'Directorio de Fotografias para', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 18, 'Directorio de Documentos', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 19, 'Ruta de la P�gina de la Empres', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 20, 'Ruta del Directorio Virtual de', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 21, 'Ruta virtual de las fotos de e', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 22, 'Ruta en disco de las fotos de ', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 23, 'N�mero del Usuario que se toma', '0', 2 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 24, 'Movimientos que se registran e', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 25, 'Versi�n De La Base De Datos', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 26, 'Directorio De Plantillas', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 27, 'Directorio Donde Se Guardan Lo', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 28, 'URL hacia el sitio donde se en', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 29, 'Directorio Donde Residen Los A', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 30, 'Numero del Usuario Para Motor ', '0', 2 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 31, 'Nombre Del Administrador De Wo', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 32, 'eMail Del Administrador De Wor', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 33, 'Nombre Del Remitente De Los Co', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 34, 'eMail Del Remitente De Los Cor', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 35, 'Nombre Del Acusador De Recibo ', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 36, 'eMail Del Acusador De Recibo D', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 37, 'Servidor de Correos Para WorkF', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 38, 'Usuario Para Acceder Al Servid', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 39, 'Contrase�a Del Usuario Para Ac', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 40, 'Puerto Usado Por Servidor de C', '0', 2 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 41, 'Puerto Usado Por Servidor de C', '0', 2 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 42, 'XSL: Directorio', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 43, 'XSL: Tareas', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 44, 'XSL: Proceso Activar', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 45, 'XSL: Proceso Cancelar', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 46, 'XSL: Proceso Terminar', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 47, 'XSL: Paso Proceso Activar', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 48, 'XSL: Paso Proceso Cancelar', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 49, 'XSL: Paso Proceso Terminar', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 50, 'XSL: Paso Proceso Terminar', '', 4 )
GO
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( 51, 'XSL: Paso Proceso Terminar', '', 4 )
GO


