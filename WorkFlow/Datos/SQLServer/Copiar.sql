EXEC sp_addlinkedserver @server = 'TRESS_2001',
			@srvproduct = '',
			@provider = 'MSDASQL',
			@provstr = 'DRIVER={SQL Server};SERVER=TRESS_2001;UID=sa;PWD=m;'
GO

delete from DBO.USER_ROL
GO

delete from DBO.ROL
GO

delete from DBO.WTAREA
GO

delete from DBO.WARCHIVO
GO

delete from DBO.WPROSTEP
GO

delete from DBO.WPROCESO
GO

delete from DBO.WMODSTEP
GO

delete from DBO.WMODELO
GO

delete from DBO.WACCION
GO

delete from DBO.WOUTBOX
GO

update USUARIO set US_JEFE =
( select U1.US_JEFE from TRESS_2001.CompartePortal.DBO.USUARIO U1 where ( U1.US_CODIGO = USUARIO.US_CODIGO ) )
GO

update USUARIO set US_JEFE = 0 where ( US_JEFE is NULL )
GO

insert into DBO.WACCION select WA_CODIGO, WA_NOMBRE, WA_DESCRIP from TRESS_2001.CompartePortal.DBO.WACCION
GO

insert into DBO.ROL select RO_CODIGO, RO_NOMBRE, RO_DESCRIP, RO_NIVEL from TRESS_2001.CompartePortal.DBO.ROL
GO

insert into DBO.WMODELO select WM_CODIGO, WM_NOMBRE, WM_DESCRIP, WM_TIPNOT1, WM_USRNOT1,
WM_TIPNOT2, WM_USRNOT2, WM_TIPNOT3, WM_USRNOT3, WM_URL, WM_URL_EXA, WM_URL_OK,
WM_XSL_TAR, WM_STATUS, WM_XSL_ACT, WM_XSL_CAN, WM_XSL_TER, '' as WM_URL_DEL, '' as WM_FORMATO from TRESS_2001.CompartePortal.DBO.WMODELO
GO

insert into DBO.WMODSTEP select WM_CODIGO, WE_ORDEN, WA_CODIGO, WE_NOMBRE, WE_DESCRIP,
WE_TIPDEST, WE_USRDEST, WE_TIPNOT1, WE_USRNOT1, WE_TIPNOT2, WE_USRNOT2, WE_TIPNOT3,
WE_USRNOT3, WE_XSL_ACT, WE_XSL_CAN, WE_XSL_TER, WE_XSL_TAR from TRESS_2001.CompartePortal.DBO.WMODSTEP
GO

insert into DBO.WPROCESO select
WP_FOLIO, WM_CODIGO, WP_NOMBRE, WP_USR_INI, WP_FEC_INI, WP_FEC_FIN, WP_STATUS, WP_PASOS, WP_PASO, WP_XMLDATA, WP_NOTAS, WP_DATA
from TRESS_2001.CompartePortal.DBO.WPROCESO
GO

insert into DBO.WPROSTEP select WP_FOLIO, WE_ORDEN, WA_CODIGO, WM_CODIGO, WS_NOMBRE, WS_STATUS, WS_USR_INI, WS_FEC_INI, WS_FEC_FIN, WS_NEXT
from TRESS_2001.CompartePortal.DBO.WPROSTEP
GO

insert into DBO.WTAREA
select WT_GUID, WP_FOLIO, WE_ORDEN, WT_DESCRIP, WT_STATUS, WT_FEC_INI, WT_FEC_FIN, WT_USR_ORI, WT_USR_DES, WT_NOTAS, WT_AVANCE
from TRESS_2001.CompartePortal.DBO.WTAREA
GO

insert into DBO.WARCHIVO
select WP_FOLIO, WH_ORDEN, WH_NOMBRE, WH_RUTA
from TRESS_2001.CompartePortal.DBO.WARCHIVO
GO

insert into DBO.USER_ROL
select US_CODIGO, RO_CODIGO, UR_PPAL
from TRESS_2001.CompartePortal.DBO.USER_ROL
GO

insert into DBO.WOUTBOX 
select WO_GUID, WT_GUID, 0 as WP_FOLIO, WO_TO, WO_CC, WO_SUBJECT, WO_BODY, WO_FROM_NA, WO_FROM_AD, WO_SUBTYPE, 
0 as WO_STATUS, WO_ENVIADO, WO_FEC_IN, WO_FEC_OUT, WO_TSTATUS, WO_TAVANCE, 0 as US_ENVIA, 0 as US_RECIBE, '' as WO_LOG
from TRESS_2001.CompartePortal.DBO.WOUTBOX
GO

update WOUTBOX set WP_FOLIO = ( select WTAREA.WP_FOLIO from WTAREA where ( WTAREA.WT_GUID = WOUTBOX.WT_GUID ) )
where ( WOUTBOX.WT_GUID is not NULL )
GO

update WOUTBOX set WO_STATUS = 1 where ( WO_ENVIADO = 'S' )
GO

