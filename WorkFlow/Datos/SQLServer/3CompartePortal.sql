alter table USUARIO add US_JEFE Usuario
GO
update USUARIO set US_JEFE = 0 where ( US_JEFE is NULL )
GO

alter table USUARIO add US_PAG_DEF Usuario
GO
update USUARIO set US_PAG_DEF = 0 where ( US_PAG_DEF is NULL )
GO

INSERT INTO COMPANY (CM_CODIGO, CM_NOMBRE, CM_ALIAS, CM_USRNAME, CM_PASSWRD, CM_CONTROL )
             VALUES ('XCOMPARTEY', 'Comparte (No Borrar)', 'Tress Comparte', 'sa', '', '3OTRO' )
GO

DROP TRIGGER TD_USUARIO
GO

CREATE TRIGGER TD_USUARIO ON USUARIO AFTER DELETE AS
BEGIN
     declare @OldUS_CODIGO Usuario;
     select @OldUS_CODIGO = US_CODIGO from Deleted;
     delete from PAGINA where ( PAGINA.US_CODIGO = @OldUS_CODIGO );
     update USUARIO set US_JEFE = 0 where ( US_JEFE = @OldUS_CODIGO );
     update USUARIO set US_PAG_DEF = 0 where ( US_PAG_DEF = @OldUS_CODIGO );
END
GO

DROP TRIGGER TU_USUARIO
GO

CREATE TRIGGER TU_USUARIO ON USUARIO AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE( US_CODIGO )
     BEGIN
	  declare @NewUS_CODIGO Usuario, @OldUS_CODIGO Usuario;

	  select @NewUS_CODIGO = US_CODIGO from Inserted;
          select @OldUS_CODIGO = US_CODIGO from Deleted;
          update PAGINA set PAGINA.US_CODIGO = @NewUS_CODIGO
          where ( PAGINA.US_CODIGO = @OldUS_CODIGO );
          update USUARIO set US_JEFE = @NewUS_CODIGO
          where ( US_JEFE = @OldUS_CODIGO );
          update USUARIO set US_PAG_DEF = @NewUS_CODIGO
          where ( US_PAG_DEF = @OldUS_CODIGO );
     end
END
GO


