unit FGlobalGenerator;

interface

uses Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls;

type
  TGlobalSQLGenerator = class(TForm)
    Generar: TButton;
    procedure GenerarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GlobalSQLGenerator: TGlobalSQLGenerator;

implementation

uses ZGlobalTress,
     ZetaCommonClasses,
     ZetaCommonLists;

{$R *.dfm}

procedure TGlobalSQLGenerator.GenerarClick(Sender: TObject);
const
     K_SQL = 'insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO ) values ( %d, ''%s'', ''%s'', %d )';
     K_DEFAULT_CERO = '0';
var
   i: Integer;
   FLista: TStrings;
   sDefault: String;
   eTipo: eTipoGlobal;
begin
     FLista := TStringList.Create;
     try
        with FLista do
        begin
             for i := 1 to K_NUM_GLOBALES do
             begin
                  eTipo := ZGlobalTress.GetTipoGlobal( i );
                  case eTipo of
                       tgBooleano: sDefault := K_GLOBAL_NO;
                       tgFloat: sDefault := K_DEFAULT_CERO;
                       tgNumero: sDefault := K_DEFAULT_CERO;
                  else
                      sDefault := '';
                  end;
                  Add( Format( K_SQL, [ i, Copy( ZGlobalTress.GetDescripcionGlobal( i ), 1, 30 ), sDefault, Ord( eTipo ) ] ) );
                  Add( 'GO' );
             end;
             SaveToFile( 'globales.sql' );
        end;
     finally
            FreeAndNil( FLista );
     end;
end;

end.
