inherited HistProcesosEdit: THistProcesosEdit
  Left = 230
  Top = 92
  Caption = 'Proceso'
  ClientHeight = 543
  ClientWidth = 519
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 507
    Width = 519
    inherited OK: TBitBtn
      Left = 351
    end
    inherited Cancelar: TBitBtn
      Left = 436
    end
  end
  inherited PanelSuperior: TPanel
    Width = 519
  end
  inherited PanelIdentifica: TPanel
    Width = 519
    inherited ValorActivo2: TPanel
      Width = 193
    end
  end
  object PanelDatos: TPanel [3]
    Left = 0
    Top = 51
    Width = 519
    Height = 220
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object WP_FOLIOlbl: TLabel
      Left = 67
      Top = 5
      Width = 25
      Height = 13
      Alignment = taRightJustify
      Caption = 'Folio:'
    end
    object PASOACTUALlbl: TLabel
      Left = 65
      Top = 88
      Width = 27
      Height = 13
      Alignment = taRightJustify
      Caption = 'Paso:'
    end
    object WP_USR_INIlbl: TLabel
      Left = 40
      Top = 47
      Width = 52
      Height = 13
      Alignment = taRightJustify
      Caption = 'Solicitante:'
    end
    object WT_NOTASlbl: TLabel
      Left = 18
      Top = 109
      Width = 74
      Height = 13
      Alignment = taRightJustify
      Caption = 'Observaciones:'
    end
    object WP_FEC_INIlbl: TLabel
      Left = 64
      Top = 67
      Width = 28
      Height = 13
      Alignment = taRightJustify
      Caption = 'Inici'#243':'
    end
    object WP_FOLIO: TZetaDBTextBox
      Left = 96
      Top = 4
      Width = 97
      Height = 17
      AutoSize = False
      Caption = 'WP_FOLIO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WP_FOLIO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object WP_NOMBRE: TZetaDBTextBox
      Left = 96
      Top = 25
      Width = 389
      Height = 17
      AutoSize = False
      Caption = 'WP_NOMBRE'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WP_NOMBRE'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object WP_FEC_INI: TZetaDBTextBox
      Left = 96
      Top = 65
      Width = 97
      Height = 17
      AutoSize = False
      Caption = 'WP_FEC_INI'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WP_FEC_INI'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object STATUSPROCESO: TZetaDBTextBox
      Left = 200
      Top = 4
      Width = 284
      Height = 17
      AutoSize = False
      Caption = 'STATUSPROCESO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'STATUSPROCESO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object WP_NOMBRElbl: TLabel
      Left = 52
      Top = 26
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nombre:'
    end
    object PASOACTUAL: TZetaDBTextBox
      Left = 96
      Top = 86
      Width = 97
      Height = 17
      AutoSize = False
      Caption = 'PASOACTUAL'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'PASOACTUAL'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object WS_NOMBRE: TZetaDBTextBox
      Left = 200
      Top = 86
      Width = 284
      Height = 17
      AutoSize = False
      Caption = 'WS_NOMBRE'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WS_NOMBRE'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object WP_USR_INI: TZetaDBTextBox
      Left = 96
      Top = 45
      Width = 97
      Height = 17
      AutoSize = False
      Caption = 'WP_USR_INI'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WP_USR_INI'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object WP_NOM_INI: TZetaDBTextBox
      Left = 199
      Top = 45
      Width = 284
      Height = 17
      AutoSize = False
      Caption = 'WP_NOM_INI'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WP_NOM_INI'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object WP_FEC_FINlbl: TLabel
      Left = 200
      Top = 67
      Width = 78
      Height = 13
      Alignment = taRightJustify
      Caption = 'Ultima actividad:'
    end
    object WP_FEC_FIN: TZetaDBTextBox
      Left = 282
      Top = 65
      Width = 97
      Height = 17
      AutoSize = False
      Caption = 'WP_FEC_FIN'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WP_FEC_FIN'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object WP_NOTAS: TDBMemo
      Left = 96
      Top = 106
      Width = 389
      Height = 109
      Color = clBtnFace
      DataField = 'WP_NOTAS'
      DataSource = DataSource
      ReadOnly = True
      TabOrder = 0
    end
  end
  object PageControl: TPageControl [4]
    Left = 0
    Top = 271
    Width = 519
    Height = 236
    ActivePage = Correos
    Align = alClient
    TabOrder = 4
    object Pasos: TTabSheet
      Caption = 'Pasos'
      object GridPasos: TZetaDBGrid
        Left = 0
        Top = 0
        Width = 511
        Height = 208
        Align = alClient
        DataSource = dsSteps
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnTitleClick = GridPasosTitleClick
        Columns = <
          item
            Expanded = False
            FieldName = 'WE_ORDEN'
            Title.Caption = 'Orden'
            Width = 38
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WS_NOMBRE'
            Title.Caption = 'Nombre'
            Width = 136
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WT_NOM_DES'
            Title.Caption = 'Responsable'
            Width = 153
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WT_NOTAS'
            Title.Caption = 'Observaciones'
            Width = 198
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'STATUSTAREA'
            Title.Caption = 'Status'
            Width = 89
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WS_FEC_INI'
            Title.Caption = 'Inici'#243
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WS_FEC_FIN'
            Title.Caption = 'Ultima Actividad'
            Width = 80
            Visible = True
          end>
      end
    end
    object Correos: TTabSheet
      Caption = 'Correos'
      ImageIndex = 1
      object GridCorreos: TZetaDBGrid
        Left = 0
        Top = 0
        Width = 511
        Height = 208
        Align = alClient
        DataSource = dsCorreos
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnTitleClick = GridCorreosTitleClick
        Columns = <
          item
            Expanded = False
            FieldName = 'WO_FROM_NA'
            Title.Caption = 'De'
            Width = 168
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WO_TO'
            Title.Caption = 'A'
            Width = 162
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WO_SUBJECT'
            Title.Caption = 'Tema'
            Width = 262
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WO_FEC_IN'
            Title.Caption = 'Creado'
            Width = 69
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WO_ENVIADO'
            Title.Caption = 'Enviado'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WO_FEC_OUT'
            Title.Caption = 'Fecha Env'#237'o'
            Width = 75
            Visible = True
          end>
      end
    end
    object Archivos: TTabSheet
      Caption = 'Archivos'
      ImageIndex = 2
      object GridArchivos: TZetaDBGrid
        Left = 0
        Top = 0
        Width = 512
        Height = 208
        Align = alClient
        DataSource = dsArchivos
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'WH_ORDEN'
            Title.Caption = '#'
            Width = 21
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WH_NOMBRE'
            Title.Caption = 'Nombre'
            Width = 253
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WH_RUTA'
            Title.Caption = 'Archivo'
            Width = 269
            Visible = True
          end>
      end
    end
    object Mensajes: TTabSheet
      Caption = 'Mensajes'
      ImageIndex = 3
      object GridMensajes: TZetaDBGrid
        Left = 0
        Top = 0
        Width = 511
        Height = 208
        Align = alClient
        DataSource = dsMensajes
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'WE_ORDEN'
            Title.Caption = 'Paso'
            Width = 38
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WS_NOMBRE'
            Title.Caption = 'Nombre'
            Width = 113
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WN_MENSAJE'
            Title.Caption = 'Mensaje'
            Width = 97
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WN_FECHA'
            Title.Caption = 'Fecha'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WN_USR_INI'
            Title.Caption = 'Origen'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMBREINI'
            Title.Caption = 'Nombre'
            Width = 130
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WN_USR_FIN'
            Title.Caption = 'Destino'
            Width = 43
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMBREFIN'
            Title.Caption = 'Nombre'
            Width = 130
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WN_LEIDO'
            Title.Caption = 'Procesado'
            Width = 58
            Visible = True
          end>
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 4
    Top = 57
  end
  object dsSteps: TDataSource
    OnStateChange = DataSourceStateChange
    OnDataChange = DataSourceDataChange
    Left = 4
    Top = 185
  end
  object dsCorreos: TDataSource
    OnStateChange = DataSourceStateChange
    OnDataChange = DataSourceDataChange
    Left = 36
    Top = 185
  end
  object dsArchivos: TDataSource
    OnStateChange = DataSourceStateChange
    OnDataChange = DataSourceDataChange
    Left = 67
    Top = 185
  end
  object dsMensajes: TDataSource
    OnStateChange = DataSourceStateChange
    OnDataChange = DataSourceDataChange
    Left = 100
    Top = 185
  end
end
