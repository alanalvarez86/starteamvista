unit FHistProcesos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, Grids, DBGrids, Buttons, StdCtrls, Mask,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZBaseConsulta,
     ZetaDBGrid,
     ZetaKeyLookup,
     ZetaFecha,
     ZetaNumero,
     ZetaKeyCombo;

type
  THistProcesos = class(TBaseConsulta)
    ZetaDBGrid: TZetaDBGrid;
    Panel: TPanel;
    StatusLBL: TLabel;
    ModeloLBL: TLabel;
    ProcesoLBL: TLabel;
    SolicitanteLBL: TLabel;
    ResponsableLBL: TLabel;
    FechaInicioLBL: TLabel;
    Status: TZetaKeyCombo;
    Modelo: TZetaKeyCombo;
    FolioInicial: TZetaNumero;
    FolioFinal: TZetaNumero;
    FolioFinalLBL: TLabel;
    FechaInicio: TZetaFecha;
    Responsable: TZetaKeyLookup;
    Solicitante: TZetaKeyLookup;
    Filtrar: TBitBtn;
    FechaFinal: TZetaFecha;
    FechaFinalLBL: TLabel;
    PanelBotones: TPanel;
    Suspender: TBitBtn;
    Cancelar: TBitBtn;
    Reiniciar: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FiltrarClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure SuspenderClick(Sender: TObject);
    procedure ReiniciarClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
  private
    { Private declarations }
    FParametros: TZetaParams;
    procedure CargarDatos( const lPosiciona: Boolean = TRUE );
    procedure MuestraProblema( const sTitulo: String );
    procedure ParametrosInit;
    procedure SetButtons( const lHayDatos: Boolean; const eStatus: eWFProcesoStatus );
  protected
    { Protected declarations }
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeImprimir(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Modificar; override;
  public
    { Public declarations }
    property Parametros: TZetaParams read FParametros;
  end;

var
  HistProcesos: THistProcesos;

implementation

{$R *.DFM}

uses DWorkFlow,
     DSistema,
     ZHelpContext,
     FSistEditAccesos,
     ZAccesosTress,
     ZetaDialogo,
     ZetaCommonTools;

{ *********** TSistUsuarios *********** }

procedure THistProcesos.FormCreate(Sender: TObject);
var
   eValor: eWFProcesoStatus;
begin
     inherited;
     FParametros := TZetaParams.Create;
     HelpContext := H_WORKFLOWCFG_HISTORIAL_PROCESOS;
     IndexDerechos := D_HISTORIAL_PROCESOS;
     with Self.Modelo do
     begin
          dmWorkflow.ModeloLlenarLista( Lista, True );
          ItemIndex := Lista.IndexOfName( K_TODOS_KEY );
     end;
     with Self.Status do
     begin
          with Lista do
          begin
               Clear;
               Add( Format( '%d=%s', [ 0, K_TODOS_TXT ] ) );
               BeginUpdate;
               try
                  for eValor := Low( eWFProcesoStatus ) to High( eWFProcesoStatus ) do
                  begin
                       Lista.Add( Format( '%d=%s', [ Ord( eValor ) + K_OFFSET, ZetaCommonLists.ObtieneElemento( lfWFProcesoStatus, Ord( eValor ) ) ] ) );
                  end;
               finally
                      EndUpdate;
               end;
          end;
          ItemIndex := 0;
     end;
     with dmSistema do
     begin
          Solicitante.LookupDataset := cdsUsuariosLookup;
          Responsable.LookupDataset := cdsUsuariosLookup;
     end;
end;

procedure THistProcesos.FormShow(Sender: TObject);
begin
     inherited;
     FechaInicio.Valor := Date;
     FechaFinal.Valor := Date;
     {
     Se verifican Derechos Diferentes debido a
     que en el �rbol de Derecho no Aparecen
     Los Derechos De Alta y De Baja
     }
     SetButtons( False, prInactivo );
end;

procedure THistProcesos.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FParametros );
     inherited;
end;

procedure THistProcesos.Connect;
begin
     with dmWorkflow do
     begin
          DataSource.DataSet:= cdsProcesos;
     end;
     with dmSistema do
     begin
          ConectarUsuariosLookup;
     end;
end;

procedure THistProcesos.Refresh;
begin
     Filtrar.Click;
end;

function THistProcesos.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Es Posible Agregar Procesos';
end;

function THistProcesos.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Es Posible Borrar Procesos';
end;

function THistProcesos.PuedeModificar(var sMensaje: String): Boolean;
begin
     {
     Se verifica El Derecho De Alta debido a
     que en el �rbol de Derecho no Aparecen
     Los Derechos De Alta y De Baja
     }
     Result := CheckDerechos( K_DERECHO_CAMBIO ); //K_DERECHO_ALTA );
     if not Result then
        sMensaje := 'No Est� Permitido Modificar Procesos';
end;

function THistProcesos.PuedeImprimir(var sMensaje: String): Boolean;
begin
     {
     Se verifica El Derecho De Baja debido a
     que en el �rbol de Derecho no Aparecen
     Los Derechos De Alta y De Baja
     }
     Result := CheckDerechos( K_DERECHO_IMPRESION ); //K_DERECHO_BAJA );
     if not Result then
        sMensaje := 'No Est� Permitido Imprimir Procesos';
end;

procedure THistProcesos.Modificar;
begin
     dmWorkflow.cdsProcesos.Modificar;
end;

procedure THistProcesos.SetButtons( const lHayDatos: Boolean; const eStatus: eWFProcesoStatus );
begin
     Suspender.Enabled := lHayDatos and ( eStatus in [ prActivo ] ) and CheckDerechos( K_DERECHO_SUSPENDER );
     Reiniciar.Enabled := lHayDatos and ( eStatus in [ prSuspendido ] ) and CheckDerechos( K_DERECHO_REINICIAR );
     Cancelar.Enabled := lHayDatos and ( eStatus in [ prActivo, prSuspendido ] ) and CheckDerechos( K_DERECHO_CANCELAR );
end;

procedure THistProcesos.MuestraProblema( const sTitulo: String );
begin
     ZetaDialogo.zError( sTitulo, Parametros.ParamByName( K_PARAMETRO_PROBLEMA ).AsString, 0 );
end;

procedure THistProcesos.ParametrosInit;
begin
     with Parametros do
     begin
          Clear;
     end;
end;

procedure THistProcesos.CargarDatos( const lPosiciona: Boolean );
 var
    iProceso : integer;
    lActive : Boolean;
begin
     iProceso := 0;
     lActive:= DataSource.DataSet.Active;
     if lActive then
        iProceso := DataSource.DataSet.FieldByName('WP_FOLIO').AsInteger;

     ParametrosInit;
     with Parametros do
     begin
          AddInteger( 'FolioInicial', FolioInicial.ValorEntero );
          AddInteger( 'FolioFinal', FolioFinal.ValorEntero );
          AddDate( 'InicioMinimo', FechaInicio.Valor );
          AddDate( 'InicioMaximo', FechaFinal.Valor );
          AddInteger( 'Responsable', Responsable.Valor );
          AddInteger( 'Solicitante', Solicitante.Valor );
          AddBoolean( 'FiltrarModelo', ( Modelo.Llave <> K_TODOS_KEY ) );
          AddString( 'Modelo', Modelo.Llave );
          AddBoolean( 'FiltrarStatus', ( Status.Valor <> K_TODOS_INT ) );
          AddInteger( 'Status', ZetaCommonTools.iMax( 0, ( Status.Valor - K_OFFSET ) ) );
     end;
     dmWorkflow.GetListaProcesos( Parametros );

     if lActive and lPosiciona then
     begin
          with DataSource.DataSet do
               Locate( 'WP_FOLIO', iProceso, [] );
     end;
end;

procedure THistProcesos.FiltrarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        CargarDatos(FALSE);
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure THistProcesos.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if Assigned( Datasource.Dataset ) then
     begin
          with Datasource.Dataset do
          begin
               SetButtons( not IsEmpty, eWFProcesoStatus( FieldByName( 'WP_STATUS' ).AsInteger ) );
          end;
     end;
end;

procedure Espera;
begin
     Sleep(1000);
end;
procedure THistProcesos.SuspenderClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     if ZetaDialogo.ZWarningConfirm( 'Suspender', '�Esta seguro de suspender el proceso?', 0, mbCancel ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try

             ParametrosInit;
             if dmWorkflow.ProcesoSuspender( Parametros ) then
             begin
                  Espera; //Este metodo se manda llamar, para esperar que el Servicio de WorkFlow procese los mensajes;
                  CargarDatos;
             end
             else
             begin
                  MuestraProblema( '� Problema Al Suspender Proceso !' );
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure THistProcesos.ReiniciarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     if ZetaDialogo.ZWarningConfirm( 'Reiniciar', '�Esta seguro de reiniciar el proceso?', 0, mbCancel ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             ParametrosInit;
             if dmWorkflow.ProcesoReiniciar( Parametros ) then
             begin
                  Espera; //Este metodo se manda llamar, para esperar que el Servicio de WorkFlow procese los mensajes;
                  CargarDatos;
             end
             else
             begin
                  MuestraProblema( '� Problema Al Reactivar Proceso !' );
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure THistProcesos.CancelarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     if ZetaDialogo.ZWarningConfirm( 'Cancelar', '�Esta seguro de cancelar el proceso?', 0, mbCancel ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             ParametrosInit;
             if dmWorkflow.ProcesoCancelar( Parametros ) then
             begin
                  Espera; //Este metodo se manda llamar, para esperar que el Servicio de WorkFlow procese los mensajes;
                  CargarDatos;
             end
             else
             begin
                  MuestraProblema( '� Problema Al Cancelar Proceso !' );
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

end.


