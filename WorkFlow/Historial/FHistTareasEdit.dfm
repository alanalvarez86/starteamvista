inherited HistTareasEdit: THistTareasEdit
  Left = 242
  Top = 284
  Caption = 'Tarea'
  ClientHeight = 381
  ClientWidth = 498
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 345
    Width = 498
    inherited OK: TBitBtn
      Left = 330
    end
    inherited Cancelar: TBitBtn
      Left = 415
    end
  end
  inherited PanelSuperior: TPanel
    Width = 498
  end
  inherited PanelIdentifica: TPanel
    Width = 498
    inherited ValorActivo2: TPanel
      Width = 172
    end
  end
  object PanelDatos: TPanel [3]
    Left = 0
    Top = 51
    Width = 498
    Height = 294
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object WP_FOLIOlbl: TLabel
      Left = 50
      Top = 5
      Width = 42
      Height = 13
      Alignment = taRightJustify
      Caption = 'Proceso:'
    end
    object PASOACTUALlbl: TLabel
      Left = 65
      Top = 48
      Width = 27
      Height = 13
      Alignment = taRightJustify
      Caption = 'Paso:'
    end
    object STATUSTAREAlbl: TLabel
      Left = 13
      Top = 69
      Width = 79
      Height = 13
      Alignment = taRightJustify
      Caption = 'Status de Tarea:'
    end
    object WT_NOM_ORIlbl: TLabel
      Left = 56
      Top = 90
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'Origin�:'
    end
    object WT_NOTASlbl: TLabel
      Left = 18
      Top = 175
      Width = 74
      Height = 13
      Alignment = taRightJustify
      Caption = 'Observaciones:'
    end
    object WT_FEC_INIlbl: TLabel
      Left = 64
      Top = 132
      Width = 28
      Height = 13
      Alignment = taRightJustify
      Caption = 'Inici�:'
    end
    object WP_FOLIO: TZetaDBTextBox
      Left = 96
      Top = 4
      Width = 97
      Height = 17
      AutoSize = False
      Caption = 'WP_FOLIO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WP_FOLIO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object WM_CODIGO: TZetaDBTextBox
      Left = 96
      Top = 25
      Width = 389
      Height = 17
      AutoSize = False
      Caption = 'WM_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WM_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object WT_NOM_ORI: TZetaDBTextBox
      Left = 96
      Top = 88
      Width = 389
      Height = 17
      AutoSize = False
      Caption = 'WT_NOM_ORI'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WT_NOM_ORI'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object WT_FEC_INI: TZetaDBTextBox
      Left = 96
      Top = 130
      Width = 389
      Height = 17
      AutoSize = False
      Caption = 'WT_FEC_INI'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WT_FEC_INI'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object WP_NOMBRE: TZetaDBTextBox
      Left = 200
      Top = 4
      Width = 284
      Height = 17
      AutoSize = False
      Caption = 'WP_NOMBRE'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WP_NOMBRE'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object WM_CODIGOlbl: TLabel
      Left = 54
      Top = 26
      Width = 38
      Height = 13
      Alignment = taRightJustify
      Caption = 'Modelo:'
    end
    object PASOACTUAL: TZetaDBTextBox
      Left = 96
      Top = 46
      Width = 97
      Height = 17
      AutoSize = False
      Caption = 'PASOACTUAL'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'PASOACTUAL'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object WS_NOMBRE: TZetaDBTextBox
      Left = 200
      Top = 46
      Width = 284
      Height = 17
      AutoSize = False
      Caption = 'WS_NOMBRE'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WS_NOMBRE'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object STATUSTAREA: TZetaDBTextBox
      Left = 96
      Top = 67
      Width = 97
      Height = 17
      AutoSize = False
      Caption = 'STATUSTAREA'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'STATUSTAREA'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object WT_AVANCE: TZetaDBTextBox
      Left = 200
      Top = 67
      Width = 284
      Height = 17
      AutoSize = False
      Caption = 'WT_AVANCE'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WT_AVANCE'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object WT_NOM_DESlbl: TLabel
      Left = 27
      Top = 111
      Width = 65
      Height = 13
      Alignment = taRightJustify
      Caption = 'Responsable:'
    end
    object WT_NOM_DES: TZetaDBTextBox
      Left = 96
      Top = 109
      Width = 389
      Height = 17
      AutoSize = False
      Caption = 'WT_NOM_DES'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WT_NOM_DES'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object WT_FEC_FINlbl: TLabel
      Left = 14
      Top = 153
      Width = 78
      Height = 13
      Alignment = taRightJustify
      Caption = 'Ultima actividad:'
    end
    object WT_FEC_FIN: TZetaDBTextBox
      Left = 96
      Top = 151
      Width = 389
      Height = 17
      AutoSize = False
      Caption = 'WT_FEC_FIN'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WT_FEC_FIN'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object WT_NOTAS: TDBMemo
      Left = 96
      Top = 172
      Width = 389
      Height = 109
      Color = clBtnFace
      DataField = 'WT_NOTAS'
      DataSource = DataSource
      ReadOnly = True
      TabOrder = 0
    end
  end
  inherited DataSource: TDataSource
    Left = 404
    Top = 113
  end
end
