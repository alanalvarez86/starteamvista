unit FHistProcesosEdit;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ComCtrls, Grids, DBGrids,
     ZBaseEdicion,
     ZetaDBTextBox,
     ZetaDBGrid,
     ZetaSmartLists;

type
  THistProcesosEdit = class(TBaseEdicion)
    PanelDatos: TPanel;
    WP_FOLIOlbl: TLabel;
    PASOACTUALlbl: TLabel;
    WP_USR_INIlbl: TLabel;
    WT_NOTASlbl: TLabel;
    WP_FEC_INIlbl: TLabel;
    WP_FOLIO: TZetaDBTextBox;
    WP_NOMBRE: TZetaDBTextBox;
    WP_FEC_INI: TZetaDBTextBox;
    STATUSPROCESO: TZetaDBTextBox;
    WP_NOMBRElbl: TLabel;
    PASOACTUAL: TZetaDBTextBox;
    WS_NOMBRE: TZetaDBTextBox;
    WP_USR_INI: TZetaDBTextBox;
    WP_NOM_INI: TZetaDBTextBox;
    WP_FEC_FINlbl: TLabel;
    WP_FEC_FIN: TZetaDBTextBox;
    WP_NOTAS: TDBMemo;
    PageControl: TPageControl;
    Pasos: TTabSheet;
    dsSteps: TDataSource;
    Correos: TTabSheet;
    dsCorreos: TDataSource;
    Archivos: TTabSheet;
    dsArchivos: TDataSource;
    dsMensajes: TDataSource;
    Mensajes: TTabSheet;
    GridPasos: TZetaDBGrid;
    GridArchivos: TZetaDBGrid;
    GridMensajes: TZetaDBGrid;
    GridCorreos: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GridCorreosTitleClick(Column: TColumn);
    procedure GridPasosTitleClick(Column: TColumn);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeImprimir(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  HistProcesosEdit: THistProcesosEdit;

implementation

uses ZHelpContext,
     ZAccesosTress,
     ZetaCommonClasses,
     DWorkFlow;

{$R *.DFM}

{ THistCorreosEdit }

procedure THistProcesosEdit.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_WORKFLOWCFG_HISTORIAL_PROCESOS_EDIC;
     IndexDerechos := D_HISTORIAL_PROCESOS;
     AgregarBtn.Visible := False;
     BorrarBtn.Visible := False;
     ModificarBtn.Visible := False;
     CortarBtn.Visible := False;
     CopiarBtn.Visible := False;
     PegarBtn.Visible := False;
     UndoBtn.Visible := False;
     OK.Visible := False;
end;

procedure THistProcesosEdit.FormShow(Sender: TObject);
begin
     inherited;
     DBNavigator.Visible := False; {OP: 31.Diciembre.2008}
     PageControl.ActivePage := Pasos;
end;

procedure THistProcesosEdit.Connect;
begin
     with dmWorkflow do
     begin
          GetDatosProcesoActivo;
          Datasource.Dataset := cdsProcesos;
          dsSteps.Dataset := cdsProcesosPasos;
          dsCorreos.Dataset := cdsProcesosCorreos;
          dsArchivos.Dataset := cdsProcesosArchivos;
          dsMensajes.Dataset := cdsProcesosMensajes;
     end;
end;

function THistProcesosEdit.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Es Posible Agregar Procesos';
end;

function THistProcesosEdit.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Es Posible Borrar Procesos';
end;

function THistProcesosEdit.PuedeImprimir(var sMensaje: String): Boolean;
begin
     {
     Se verifica El Derecho De Cambio debido a
     que en el �rbol de Derecho no Aparecen
     Los Derechos De Alta y De Baja
     }
     Result := CheckDerechos( K_DERECHO_BAJA );
     if not Result then
        sMensaje := 'No Est� Permitido Imprimir Procesos';
end;

function THistProcesosEdit.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Es Posible Modificar Procesos';
end;

procedure THistProcesosEdit.GridCorreosTitleClick(Column: TColumn);
begin
     inherited;
     with Column do
     begin
          if ( FieldName <> 'WO_TO' ) then
             dmWorkFlow.cdsProcesosCorreos.IndexFieldNames := FieldName;
     end;

end;

procedure THistProcesosEdit.GridPasosTitleClick(Column: TColumn);
begin
     inherited;
     with Column do
     begin
          if ( FieldName <> 'WT_NOTAS' ) then
             dmWorkFlow.cdsProcesosPasos.IndexFieldNames := FieldName;
     end;
end;

end.
