inherited ReasignarTarea: TReasignarTarea
  Left = 325
  Top = 184
  Caption = 'Reasignar Tarea'
  ClientHeight = 295
  ClientWidth = 347
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 259
    Width = 347
    TabOrder = 2
    inherited OK: TBitBtn
      Left = 179
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 264
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 347
    Height = 145
    Align = alTop
    Caption = ' Reasignar A: '
    TabOrder = 0
    object ZetaDBGrid: TZetaDBGrid
      Left = 2
      Top = 15
      Width = 343
      Height = 128
      Align = alClient
      DataSource = DataSource
      Options = [dgTitles, dgIndicator, dgColumnResize, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'US_CODIGO'
          Title.Caption = 'N�mero'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'US_NOMBRE'
          Title.Caption = 'Nombre'
          Width = 239
          Visible = True
        end>
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 145
    Width = 347
    Height = 114
    Align = alClient
    Caption = ' Notas: '
    TabOrder = 1
    object mNotas: TMemo
      Left = 2
      Top = 15
      Width = 343
      Height = 97
      Align = alClient
      TabOrder = 0
    end
  end
  object DataSource: TDataSource
    Left = 32
    Top = 40
  end
end
