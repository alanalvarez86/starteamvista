unit FHistCorreosEdit;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, OleCtrls, SHDocVw,
     ZBaseEdicion,
     ZetaDBTextBox,
     ZetaSmartLists;

type
  THistCorreosEdit = class(TBaseEdicion)
    TareaGB: TGroupBox;
    WP_FOLIOlbl: TLabel;
    PASOACTUALlbl: TLabel;
    WM_CODIGOlbl: TLabel;
    PanelDatos: TPanel;
    WO_CClbl: TLabel;
    WO_SUBJECTlbl: TLabel;
    WO_BODYlbl: TLabel;
    WO_SUBTYPElbl: TLabel;
    WO_ENVIADOlbl: TLabel;
    WO_FEC_OUTlbl: TLabel;
    WO_SUBJECT: TZetaDBTextBox;
    WO_SUBTYPE: TZetaDBTextBox;
    WO_ENVIADO: TZetaDBTextBox;
    WO_FEC_OUT: TZetaDBTextBox;
    WP_FOLIO: TZetaDBTextBox;
    WM_CODIGO: TZetaDBTextBox;
    PASOACTUAL: TZetaDBTextBox;
    WS_NOMBRE: TZetaDBTextBox;
    WO_BODY: TDBMemo;
    WO_CC: TDBMemo;
    WO_STATUSlbl: TLabel;
    WO_STA_TXT: TZetaDBTextBox;
    RemitenteGB: TGroupBox;
    WO_FROM_NAlbl: TLabel;
    WO_FROM_NA: TZetaDBTextBox;
    WO_FROM_AD: TZetaDBTextBox;
    REMITENTElbl: TLabel;
    REMITENTE: TZetaDBTextBox;
    DestinatarioGB: TGroupBox;
    WO_TOlbl: TLabel;
    WO_TO: TDBMemo;
    DESTINOlbl: TLabel;
    DESTINO: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeImprimir(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  HistCorreosEdit: THistCorreosEdit;

implementation

uses ZHelpContext,
     ZAccesosTress,
     ZetaCommonClasses,
     DWorkFlow;

{$R *.DFM}

{ THistCorreosEdit }

procedure THistCorreosEdit.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_WORKFLOWCFG_HISTORIAL_CORREOS_CORTES;
     IndexDerechos := D_HISTORIAL_CORREOS;
     AgregarBtn.Visible := False;
     BorrarBtn.Visible := False;
     ModificarBtn.Visible := False;
     CortarBtn.Visible := False;
     CopiarBtn.Visible := False;
     PegarBtn.Visible := False;
     UndoBtn.Visible := False;
     OK.Visible := False;
end;

procedure THistCorreosEdit.Connect;
begin
     Datasource.Dataset := dmWorkflow.cdsCorreos;
end;

function THistCorreosEdit.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Es Posible Agregar Correos';
end;

function THistCorreosEdit.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Es Posible Borrar Correos';
end;

function THistCorreosEdit.PuedeImprimir(var sMensaje: String): Boolean;
begin
     {
     Se verifica El Derecho De Cambio debido a
     que en el �rbol de Derecho no Aparecen
     Los Derechos De Alta y De Baja
     }
     Result := CheckDerechos( K_DERECHO_BAJA );
     if not Result then
        sMensaje := 'No Est� Permitido Imprimir Correos';
end;

function THistCorreosEdit.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Es Posible Modificar Correos';
end;

end.
