inherited HistCorreos: THistCorreos
  Left = -54
  Top = 241
  ActiveControl = Status
  Caption = 'Correos'
  ClientHeight = 257
  ClientWidth = 881
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 881
    inherited Slider: TSplitter
      Left = 357
    end
    inherited ValorActivo1: TPanel
      Width = 341
    end
    inherited ValorActivo2: TPanel
      Left = 360
      Width = 521
    end
  end
  object ZetaDBGrid: TZetaDBGrid [1]
    Left = 0
    Top = 122
    Width = 881
    Height = 135
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnTitleClick = ZetaDBGridTitleClick
    Columns = <
      item
        Expanded = False
        FieldName = 'WO_FROM_NA'
        Title.Caption = 'De'
        Width = 168
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WO_TO'
        Title.Caption = 'A'
        Width = 162
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WO_SUBJECT'
        Title.Caption = 'Tema'
        Width = 262
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WO_FEC_IN'
        Title.Caption = 'Creado'
        Width = 69
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WO_ENVIADO'
        Title.Caption = 'Enviado'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WO_FEC_OUT'
        Title.Caption = 'Fecha Env'#237'o'
        Width = 75
        Visible = True
      end>
  end
  object Panel: TPanel [2]
    Left = 0
    Top = 19
    Width = 881
    Height = 103
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object StatusLBL: TLabel
      Left = 153
      Top = 11
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = '&Status:'
      FocusControl = Status
    end
    object FechaInicioLBL: TLabel
      Left = 158
      Top = 34
      Width = 28
      Height = 13
      Alignment = taRightJustify
      Caption = 'Entre:'
      FocusControl = FechaInicio
    end
    object RemitenteLBL: TLabel
      Left = 169
      Top = 57
      Width = 17
      Height = 13
      Alignment = taRightJustify
      Caption = '&De:'
      FocusControl = Remitente
    end
    object FechaFinalLBL: TLabel
      Left = 310
      Top = 34
      Width = 8
      Height = 13
      Alignment = taRightJustify
      Caption = '&y:'
      FocusControl = FechaFinal
    end
    object TemaLBL: TLabel
      Left = 156
      Top = 79
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = '&Tema:'
      FocusControl = Tema
    end
    object Status: TZetaKeyCombo
      Left = 189
      Top = 8
      Width = 114
      Height = 21
      AutoComplete = False
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
    end
    object FechaInicio: TZetaFecha
      Left = 189
      Top = 30
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 2
      Text = '13/Sep/04'
      Valor = 38243.000000000000000000
    end
    object Filtrar: TBitBtn
      Left = 803
      Top = 53
      Width = 75
      Height = 44
      Hint = 'Aplicar El Filtro Para Obtener Los Procesos Deseados'
      Caption = '&Filtrar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      OnClick = FiltrarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        555555555555555555555555555555555555555555FF55555555555559055555
        55555555577FF5555555555599905555555555557777F5555555555599905555
        555555557777FF5555555559999905555555555777777F555555559999990555
        5555557777777FF5555557990599905555555777757777F55555790555599055
        55557775555777FF5555555555599905555555555557777F5555555555559905
        555555555555777FF5555555555559905555555555555777FF55555555555579
        05555555555555777FF5555555555557905555555555555777FF555555555555
        5990555555555555577755555555555555555555555555555555}
      NumGlyphs = 2
    end
    object FechaFinal: TZetaFecha
      Left = 324
      Top = 30
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 3
      Text = '13/Sep/04'
      Valor = 38243.000000000000000000
    end
    object Remitente: TZetaEdit
      Left = 189
      Top = 53
      Width = 609
      Height = 21
      TabOrder = 4
    end
    object Tema: TZetaEdit
      Left = 189
      Top = 75
      Width = 609
      Height = 21
      TabOrder = 5
    end
    object TipoFecha: TComboBox
      Left = 14
      Top = 30
      Width = 139
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
      Items.Strings = (
        'Fecha De Registro'
        'Fecha De Env'#237'o')
    end
  end
  inherited DataSource: TDataSource
    Left = 280
    Top = 8
  end
end
