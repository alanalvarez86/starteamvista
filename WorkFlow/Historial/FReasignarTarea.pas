unit FReasignarTarea;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, Grids, DBGrids, ZetaDBGrid, Db, Buttons,
  ExtCtrls,
  ZetaCommonClasses;

type
  TReasignarTarea = class(TZetaDlgModal)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    DataSource: TDataSource;
    ZetaDBGrid: TZetaDBGrid;
    mNotas: TMemo;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    FParametros: TZetaParams;
    { Private declarations }
  public
    { Public declarations }
    property Parametros: TZetaParams read FParametros write FParametros;
  end;

var
  ReasignarTarea: TReasignarTarea;

implementation
uses DWorkFlow;

{$R *.DFM}

procedure TReasignarTarea.FormCreate(Sender: TObject);
begin
     inherited;
     Datasource.DataSet := dmWorkFlow.cdsReasignarTareaUsuarios;
     //FParametros := TZetaParams.Create;
end;

procedure TReasignarTarea.FormDestroy(Sender: TObject);
begin
     //FreeAndNil( FParametros );
     inherited;
end;

procedure TReasignarTarea.FormShow(Sender: TObject);
begin
     inherited;
     mNotas.Lines.Clear;
     {with Parametros do
     begin
     end;}

     dmWorkFlow.GetReasignarTareaUsuarios( Parametros );
end;


procedure TReasignarTarea.OKClick(Sender: TObject);
begin
     inherited;
     with Parametros do
     begin
          AddInteger( 'UsuarioReasignar', dmWorkFlow.cdsReasignarTareaUsuarios.FieldByName('US_CODIGO').AsInteger );
          AddString( 'NotasReasignar', mNotas.Lines.Text );
     end;
end;

end.
