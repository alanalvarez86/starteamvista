unit FHistCorreos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids, Buttons, StdCtrls, Mask,
     ZetaCommonClasses,
     ZBaseConsulta,
     ZetaDBGrid,
     ZetaKeyLookup,
     ZetaFecha,
     ZetaNumero,
     ZetaKeyCombo,
     ZetaEdit;

type
  THistCorreos = class(TBaseConsulta)
    ZetaDBGrid: TZetaDBGrid;
    Panel: TPanel;
    StatusLBL: TLabel;
    FechaInicioLBL: TLabel;
    Status: TZetaKeyCombo;
    RemitenteLBL: TLabel;
    FechaInicio: TZetaFecha;
    Filtrar: TBitBtn;
    FechaFinal: TZetaFecha;
    FechaFinalLBL: TLabel;
    Remitente: TZetaEdit;
    Tema: TZetaEdit;
    TemaLBL: TLabel;
    TipoFecha: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FiltrarClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ZetaDBGridTitleClick(Column: TColumn);
  private
    { Private declarations }
    FParametros: TZetaParams;
  protected
    { Protected declarations }
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeImprimir(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Modificar; override;
  public
    { Public declarations }
    property Parametros: TZetaParams read FParametros;
  end;

var
  HistCorreos: THistCorreos;

implementation

{$R *.DFM}

uses DWorkFlow,
     DCatalogos,
     DSistema,
     ZHelpContext,
     ZAccesosTress,
     ZetaCommonTools;

const
     K_TODOS_CHAR = 'T';

{ *********** THistCorreos *********** }

procedure THistCorreos.FormCreate(Sender: TObject);
begin
     inherited;
     FParametros := TZetaParams.Create;
     HelpContext := H_WORKFLOWCFG_HISTORIAL_CORREOS;
     IndexDerechos := D_HISTORIAL_CORREOS;
     with Self.Status do
     begin
          with Lista do
          begin
               Clear;
               Add( Format( '%s=%s', [ K_TODOS_CHAR, K_TODOS_TXT ] ) );
               Add( Format( '%s=%s', [ K_GLOBAL_SI, 'Enviados' ] ) );
               Add( Format( '%s=%s', [ K_GLOBAL_NO, 'Sin Enviar' ] ) );
          end;
          ItemIndex := 0;
     end;
     Self.TipoFecha.ItemIndex := 0;
end;

procedure THistCorreos.FormShow(Sender: TObject);
begin
     inherited;
     FechaInicio.Valor := Date;
     FechaFinal.Valor := Date;
end;

procedure THistCorreos.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FParametros );
     inherited;
end;

procedure THistCorreos.Connect;
begin
     with dmSistema do
     begin
          ConectarUsuariosLookup;
     end;
     with dmWorkflow do
     begin
          DataSource.DataSet:= cdsCorreos;
     end;
end;

procedure THistCorreos.Refresh;
begin
     Filtrar.Click;
end;

function THistCorreos.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Es Posible Agregar Correos';
end;

function THistCorreos.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Es Posible Borrar Correos';
end;

function THistCorreos.PuedeModificar(var sMensaje: String): Boolean;
begin
     {
     Se verifica El Derecho De Alta debido a
     que en el �rbol de Derecho no Aparecen
     Los Derechos De Alta y De Baja
     }
     Result := CheckDerechos( K_DERECHO_CAMBIO ); //K_DERECHO_ALTA );
     if not Result then
        sMensaje := 'No Est� Permitido Modificar Correos';
end;

function THistCorreos.PuedeImprimir(var sMensaje: String): Boolean;
begin
     {
     Se verifica El Derecho De Baja debido a
     que en el �rbol de Derecho no Aparecen
     Los Derechos De Alta y De Baja
     }
     Result := CheckDerechos( K_DERECHO_IMPRESION ); //K_DERECHO_BAJA );
     if not Result then
        sMensaje := 'No Est� Permitido Imprimir Correos';
end;

procedure THistCorreos.Modificar;
begin
     dmWorkflow.cdsCorreos.Modificar;
end;

procedure THistCorreos.FiltrarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with Parametros do
        begin
             Clear;
             AddDate( 'Inicio', FechaInicio.Valor );
             AddDate( 'Final', FechaFinal.Valor );
             AddString( 'Remitente', Remitente.Text );
             AddString( 'Tema', Tema.Text );
             AddBoolean( 'FiltrarStatus', ( Status.Llave <> K_TODOS_CHAR ) );
             AddBoolean( 'Enviado', ( Status.Llave = K_GLOBAL_SI ) );
             AddBoolean( 'FechaGeneracion', ( Self.TipoFecha.ItemIndex = 0 ) );
        end;
        dmWorkflow.GetListaCorreos( Parametros );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure THistCorreos.ZetaDBGridTitleClick(Column: TColumn);
begin
     inherited;
     with Column do
     begin
          if ( FieldName <> 'WO_TO' ) then
             dmWorkFlow.cdsCorreos.IndexFieldNames := FieldName;
     end;

end;

end.


