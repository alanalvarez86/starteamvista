unit FHistTareas;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids, Buttons, StdCtrls, Mask,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZBaseConsulta,
     ZetaDBGrid,
     ZetaKeyLookup,
     ZetaFecha,
     ZetaNumero,
     ZetaKeyCombo;

type
  THistTareas = class(TBaseConsulta)
    ZetaDBGrid: TZetaDBGrid;
    Panel: TPanel;
    StatusLBL: TLabel;
    ModeloLBL: TLabel;
    ProcesoLBL: TLabel;
    SolicitanteLBL: TLabel;
    ResponsableLBL: TLabel;
    FechaInicioLBL: TLabel;
    Status: TZetaKeyCombo;
    Modelo: TZetaKeyCombo;
    FolioInicial: TZetaNumero;
    FolioFinal: TZetaNumero;
    FolioFinalLBL: TLabel;
    FechaInicio: TZetaFecha;
    Responsable: TZetaKeyLookup;
    Solicitante: TZetaKeyLookup;
    Filtrar: TBitBtn;
    FechaFinal: TZetaFecha;
    FechaFinalLBL: TLabel;
    Panel1: TPanel;
    Reasignar: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FiltrarClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ReasignarClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    FParametros: TZetaParams;
    procedure ParametrosInit;
    procedure SetButtons(const lHayDatos: Boolean;const eStatus: eWFTareaStatus);
    procedure CargarDatos;
    procedure MuestraProblema(const sTitulo: String);
  protected
    { Protected declarations }
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeImprimir(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Modificar; override;
  public
    { Public declarations }
    property Parametros: TZetaParams read FParametros;
  end;

var
  HistTareas: THistTareas;

implementation

{$R *.DFM}

uses DWorkFlow,
     DSistema,
     ZHelpContext,
     ZetaDialogo,
     ZAccesosTress,
     ZetaCommonTools;

{ *********** THistTareas *********** }

procedure THistTareas.FormCreate(Sender: TObject);
var
   eValor: eWFTareaStatus;
begin
     inherited;
     FParametros := TZetaParams.Create;
     HelpContext := H_WORKFLOWCFG_HISTORIAL_TAREAS;
     IndexDerechos := D_HISTORIAL_TAREAS;
     with Self.Modelo do
     begin
          dmWorkflow.ModeloLlenarLista( Lista, True );
          ItemIndex := Lista.IndexOfName( K_TODOS_KEY );
     end;
     with Self.Status do
     begin
          with Lista do
          begin
               Clear;
               Add( Format( '%d=%s', [ 0, K_TODOS_TXT ] ) );
               BeginUpdate;
               try
                  for eValor := Low( eWFTareaStatus ) to High( eWFTareaStatus ) do
                  begin
                       Lista.Add( Format( '%d=%s', [ Ord( eValor ) + K_OFFSET, ZetaCommonLists.ObtieneElemento( lfWFTareaStatus, Ord( eValor ) ) ] ) );
                  end;
               finally
                      EndUpdate;
               end;
          end;
          ItemIndex := 0;
     end;
     with dmSistema do
     begin
          Solicitante.LookupDataset := cdsUsuariosLookup;
          Responsable.LookupDataset := cdsUsuariosLookup;
     end;
end;

procedure THistTareas.FormShow(Sender: TObject);
begin
     inherited;
     FechaInicio.Valor := Date;
     FechaFinal.Valor := Date;
end;

procedure THistTareas.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FParametros );
     inherited;
end;

procedure THistTareas.Connect;
begin
     with dmWorkflow do
     begin
          DataSource.DataSet:= cdsTareas;
     end;
     with dmSistema do
     begin
          ConectarUsuariosLookup;
     end;
end;

procedure THistTareas.Refresh;
begin
     Filtrar.Click;
end;

function THistTareas.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Es Posible Agregar Tareas';
end;

function THistTareas.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Es Posible Borrar Tareas';
end;

function THistTareas.PuedeModificar(var sMensaje: String): Boolean;
begin
     {
     Se verifica El Derecho De Alta debido a
     que en el �rbol de Derecho no Aparecen
     Los Derechos De Alta y De Baja
     }
     Result := CheckDerechos( K_DERECHO_CAMBIO ); //K_DERECHO_ALTA );
     if not Result then
        sMensaje := 'No Est� Permitido Modificar Tareas';
end;

function THistTareas.PuedeImprimir(var sMensaje: String): Boolean;
begin
     {
     Se verifica El Derecho De Baja debido a
     que en el �rbol de Derecho no Aparecen
     Los Derechos De Alta y De Baja
     }
     Result := CheckDerechos( K_DERECHO_IMPRESION ); //K_DERECHO_BAJA );
     if not Result then
        sMensaje := 'No Est� Permitido Imprimir Tareas';
end;

procedure THistTareas.Modificar;
begin
     dmWorkflow.cdsTareas.Modificar;
end;

procedure THistTareas.CargarDatos;
begin
     with Parametros do
     begin
          Clear;
          AddInteger( 'FolioInicial', FolioInicial.ValorEntero );
          AddInteger( 'FolioFinal', FolioFinal.ValorEntero );
          AddDate( 'InicioMinimo', FechaInicio.Valor );
          AddDate( 'InicioMaximo', FechaFinal.Valor );
          AddInteger( 'Responsable', Responsable.Valor );
          AddInteger( 'Solicitante', Solicitante.Valor );
          AddBoolean( 'FiltrarModelo', ( Modelo.Llave <> K_TODOS_KEY ) );
          AddString( 'Modelo', Modelo.Llave );
          AddBoolean( 'FiltrarStatus', ( Status.Valor <> K_TODOS_INT ) );
          AddInteger( 'Status', ( Status.Valor - K_OFFSET ) );
     end;
     dmWorkflow.GetListaTareas( Parametros );
end;

procedure THistTareas.FiltrarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        CargarDatos;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure THistTareas.ParametrosInit;
begin
     with Parametros do
     begin
          Clear;
     end;
end;

procedure THistTareas.ReasignarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     ParametrosInit;
     if dmWorkFlow.SeleccionaTarea( Parametros ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             if dmWorkflow.TareaReasignar( Parametros ) then
             begin
                  CargarDatos;
             end
             else
             begin
                  MuestraProblema( '� Problema Al Reasignar Proceso !' );
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure THistTareas.MuestraProblema( const sTitulo: String );
begin
     ZetaDialogo.zError( sTitulo, Parametros.ParamByName( K_PARAMETRO_PROBLEMA ).AsString, 0 );
end;

procedure THistTareas.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if Assigned( Datasource.Dataset ) then
     begin
          with Datasource.Dataset do
          begin
               SetButtons( not IsEmpty, eWFTareaStatus( FieldByName( 'WT_STATUS' ).AsInteger ) );
          end;
     end;
end;

procedure THistTareas.SetButtons( const lHayDatos: Boolean; const eStatus: eWFTareaStatus );
begin
     Reasignar.Enabled := lHayDatos and ( eStatus in [ sttActiva, sttEsperando ] ) and CheckDerechos( K_DERECHO_CAMBIO );
end;

end.


