library TressCaller;

uses
  ComServ,
  TressCaller_TLB in 'TressCaller_TLB.pas',
  FTressCalls in 'FTressCalls.pas' {TressProcessCalls: TMtsDataModule} {TressProcessCalls: CoClass},
  DBasicoCliente in '..\..\3Win_20\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
