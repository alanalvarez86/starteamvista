unit DExisteCampo;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     DBaseExisteCampo,
     DNomParam,
     ZetaTipoEntidad,
     ZetaCommonLists;

type
  TdmExisteCampo = class(TdmBaseExisteCampo)
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EntidadEspecial(const oEntidad: TipoEntidad;var sLookup: string): Boolean;override;
  public
    { Public declarations }
  end;

var
  dmExisteCampo: TdmExisteCampo;

implementation

{$R *.DFM}

function TdmExisteCampo.EntidadEspecial(const oEntidad: TipoEntidad; var sLookup:string ) : Boolean;
begin
     {$ifdef FALSE}
     Result := ( oEntidad = enEmpleado );
     sLookUp := 'COLABORA.CB_APE_PAT||'' ''||COLABORA.CB_APE_MAT||'', ''||COLABORA.CB_NOMBRES';
     {$else}
     Result := False;
     {$endif}
end;

end.
