unit DEntidadesTress;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     DEntidades,
     ZetaTipoEntidad,
     ZetaEntidad;

type
  TdmEntidadesTress = class(TdmEntidades)
  private
    { Private declarations }
  protected
    { Protected declarations }
    function AgregaEntidad(const Entidad: TipoEntidad; const sTabla, sLlave: String; sDescripcion: String = '' ): TEntidad;
    procedure CreaEntidades; override;
  public
    { Public declarations }
  end;

implementation

uses ZetaCommonLists,
     ZetaCommonTools;

{$R *.DFM}

{ **** TdmEntidadesTress **** }

function TdmEntidadesTress.AgregaEntidad(const Entidad: TipoEntidad; const sTabla, sLlave: String; sDescripcion: String = '' ): TEntidad;
begin
     if ZetaCommonTools.StrVacio( sDescripcion ) then
        sDescripcion := ZetaTipoEntidad.ObtieneEntidad( Entidad );
     Result := AddEntidad( Entidad, sTabla, sLlave, sDescripcion );
end;

procedure TdmEntidadesTress.CreaEntidades;
begin
     AgregaEntidad( enCompanys, 'COMPANY', 'CM_CODIGO' );
     with AgregaEntidad( enGrupo, 'GRUPO', 'GR_CODIGO' ) do
     begin
          AddRelacion( enGrupoPadre, 'GR_PADRE' );
     end;
     AgregaEntidad( enGrupoPadre, 'V_PADRE', 'GR_CODIGO' );
     with AgregaEntidad( enUsuarios, 'USUARIO', 'US_CODIGO' ) do
     begin
          AddRelacion( enCompanys, 'CM_CODIGO' );
          AddRelacion( enUsuarioJefe, 'US_JEFE' );
     end;
     with AgregaEntidad( enUsuarioJefe, 'V_JEFES', 'US_CODIGO' ) do
     begin
          AddRelacion( enUsuarios, 'US_CODIGO' );
     end;

     with AgregaEntidad( enBitacora, 'BITACORA', '' ) do
     begin
          AddRelacion( enWizard, 'BI_NUMERO' );
     end;
     AgregaEntidad( enWizard, 'PROCESO', 'PC_NUMERO' );
     AgregaEntidad( enDiccion, 'DICCION', 'DI_CLASIFI,DI_NOMBRE' );
     AgregaEntidad( enQuerys, 'QUERYS', 'QU_CODIGO' );
     with AgregaEntidad( enReporte, 'REPORTE', 'RE_CODIGO' ) do
     begin
          AddRelacion( enQuerys, 'QU_CODIGO' );
     end;
     with AgregaEntidad( enCampoRep, 'CAMPOREP', 'RE_CODIGO,CR_TIPO,CR_POSICIO' ) do
     begin
          AddRelacion( enReporte, 'RE_CODIGO' );
     end;
     with AgregaEntidad( enMisReportes, 'MISREPOR', 'RE_CODIGO,US_CODIGO' ) do
     begin
          AddRelacion( enReporte, 'RE_CODIGO' );
     end;
     AgregaEntidad( enRol, 'ROL', 'RO_CODIGO' );
     with AgregaEntidad( enUserRol, 'USER_ROL', 'US_CODIGO,RO_CODIGO' ) do
     begin
          AddRelacion( enRol, 'RO_CODIGO' );
          AddRelacion( enUsuarios, 'US_CODIGO' );
     end;
     AgregaEntidad( enAccion, 'WACCION', 'WA_CODIGO' );
     AgregaEntidad( enModelo, 'WMODELO', 'WM_CODIGO' );
     with AgregaEntidad( enModeloPasos, 'WMODSTEP', 'WM_CODIGO,WE_ORDEN' ) do
     begin
          AddRelacion( enModelo, 'WM_CODIGO' );
          AddRelacion( enAccion, 'WA_CODIGO' );
     end;
     with AgregaEntidad( enProceso, 'V_PROCESO', 'WP_FOLIO' ) do
     begin
          AddRelacion( enModelo, 'WM_CODIGO' );
     end;
     with AgregaEntidad( enProcesoPasos, 'V_PASOS', 'WP_FOLIO,WE_ORDEN' ) do
     begin
          AddRelacion( enModelo, 'WM_CODIGO' );
          AddRelacion( enModeloPasos, 'WM_CODIGO,WE_ORDEN' );
          AddRelacion( enProceso, 'WP_FOLIO' );
     end;
     with AgregaEntidad( enProcesoArchivos, 'WARCHIVO', 'WP_FOLIO,WH_ORDEN' ) do
     begin
          AddRelacion( enProceso, 'WP_FOLIO' );
     end;
     with AgregaEntidad( enTarea, 'V_TAREA', 'WT_GUID' ) do
     begin
          AddRelacion( enModelo, 'WM_CODIGO' );
          AddRelacion( enModeloPasos, 'WM_CODIGO,WE_ORDEN' );
          AddRelacion( enProceso, 'WP_FOLIO' );
          AddRelacion( enProcesoPasos, 'WP_FOLIO,WE_ORDEN' );
     end;
     with AgregaEntidad( enMensaje, 'WMENSAJE', 'WN_GUID' ) do
     begin
          AddRelacion( enProceso, 'WP_FOLIO' );
          AddRelacion( enProcesoPasos, 'WP_FOLIO,WE_ORDEN' );
          AddRelacion( enTarea, 'WT_GUID' );
     end;
     with AgregaEntidad( enBuzon, 'WOUTBOX', 'WO_GUID' ) do
     begin
          AddRelacion( enTarea, 'WT_GUID' );
          AddRelacion( enProceso, 'WP_FOLIO' );
     end;
end;

end.
