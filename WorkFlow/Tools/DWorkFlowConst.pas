unit DWorkFlowConst;

interface
uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs;

const

     K_EMAIL_SERVICE_NAME = 'WorkFlow Email';
     K_EMAIL_SERVICE_MSG = K_EMAIL_SERVICE_NAME + 'MSG';
     K_EMAIL_SERVICE_STATUS = K_EMAIL_SERVICE_NAME + 'STATUS';

     K_MANAGER_SERVICE_NAME = 'WorkFlow Manager';
     K_MANAGER_SERVICE_MSG = K_MANAGER_SERVICE_NAME + 'MSG';
     K_MANAGER_SERVICE_STATUS = K_MANAGER_SERVICE_NAME + 'STATUS';

     K_MANAGER_SERVICE_CONSOLE = 'WorkFlowManager';
     K_EMAIL_SERVICE_CONSOLE = 'WorkFlowEMailMgr';


implementation

end.
