unit DEmailService;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, IniFiles, Registry,
     IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdMessageClient, IdSMTP, IdMessage, IdEMailAddress,
     ZetaCommonLists;

const
     K_EMAIL_PORT = 25;
type
  TdmEmailService = class(TDataModule)
    eMail: TIdSMTP;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FIniFile: TIniFile;
    FRegistry: TRegistry;
    FLista: TStrings;
    FSubType: eEmailType;
    FPuerto: Integer;
    FTimeOut: Integer;
    FMailServer: String;
    FReplyToName: String;
    FReplyToAddress: String;
    FUser: String;
    FPassWord: String;
    FSubject: String;
    FFromName: String;
    FFromAddress: String;
    FMessageText: TStrings;
    FAttachments: TStrings;
    FToCarbonCopy: TStrings;
    FToBlindCarbonCopy: TStrings;
    FToAddress: TStrings;
    FMensaje: TIDMessage;
    FReceiptRecipientAddress: String;
    FReceiptRecipientName: String;
    FRegistryKey: String;
  protected
    { Protected declaractions }
    function GetFileName( const sFileName: String ): String;
    function GetKeyName(const sKeyName: String): String;
    function MailServerConnect(var sErrorMsg: String): Boolean;
    function MailServerDisconnect(var sErrorMsg: String): Boolean;
    procedure BuildAddressList(Lista: TStrings; Direcciones: TIdEmailAddressList);
    procedure BuildAttachmentList(Lista: TStrings; MsgParts: TIdMessageParts);
    procedure ConstruirMensaje;
    procedure InitRegistry;
    procedure ReadAddressSection( const sSeccion: String; Direcciones: TStrings );
    procedure ReadAddressKey(const sKey, sSubKey: String; Direcciones: TStrings);
    procedure ReadSectionFromRegistry(const sSectionKey, sSectionName, sKey: String; Lista: TStrings);
    procedure WriteAddressSection( const sSeccion, sSubSeccion: String; Direcciones: TStrings );
    procedure WriteAddressKey(const sKey, sSubKey: String; Direcciones: TStrings);
    procedure WriteSectionToRegistry(const sSectionKey, sSectionName, sKey: String; Lista: TStrings);
  public
    { Public declarations }
    property Attachments: TStrings read FAttachments;
    property FromAddress: String read FFromAddress write FFromAddress;
    property FromName: String read FFromName write FFromName;
    property MailServer: String read FMailServer write FMailServer;
    property MessageText: TStrings read FMessageText;
    property Password: String read FPassWord write FPassWord;
    property Port: integer read FPuerto write FPuerto default K_EMAIL_PORT;
    property ReplyToName: String read FReplyToName write FReplyToName;
    property ReplyToAddress: String read FReplyToAddress write FReplyToAddress;
    property ReceiptRecipientName: String read FReceiptRecipientName write FReceiptRecipientName;
    property ReceiptRecipientAddress: String read FReceiptRecipientAddress write FReceiptRecipientAddress;
    property RegistryKey: String read FRegistryKey write FRegistryKey;
    property Subject: String read FSubject write FSubject;
    property SubType: eEmailType read FSubType write FSubType;
    property TimeOut: Integer read FTimeOut write FTimeOut;
    property ToAddress: TStrings read FToAddress;
    property ToBlindCarbonCopy: TStrings read FToBlindCarbonCopy;
    property ToCarbonCopy: TStrings read FToCarbonCopy;
    property User: String read FUser write FUser;

    function ReadFromIniFile( const sFileName : String ) : Boolean;
    function ReadFromRegistry( const sKeyName : String ) : Boolean;
    function SendEmail( var sErrorMsg : String ): Boolean;
    function Validate( var sErrorMsg: String ): Boolean;
    function VerifyEmailAddresses( var sErrorMsg: String ): Boolean;
    function WriteToIniFile( const sFileName : String ) : Boolean;
    function WriteToRegistry( const sKeyName : String ) : Boolean;
    procedure NewBodyMessage;
    procedure NewEmail;
  end;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses;

{$R *.DFM}

const
     K_SECCION = 'EmailParameters';
     K_SECCION_TO_ADDRESS = 'EmailToAddress';
     K_SECCION_CARBON_ADDRESS = 'EmailCarbonAddress';
     K_SECCION_BLIND_ADDRESS = 'EmailBlindAddress';


     K_TO_ADDRESS = 'ToAddress%d';
     K_CARBON_ADDRESS = 'CarbonAdresss%d';
     K_BLIND_ADDRESS = 'BlindAdress%d';

     K_MAILSERVER = 'Host';
     K_PORT = 'Port';
     K_SUBTYPE = 'Type';
     K_USER = 'UserId';
     K_PASSWORD = 'PassWord';
     K_FROMADDRESS = 'Remitente';
     K_FROMNAME = 'FromName';
     K_REPLYTO = 'ReplyTo';
     K_RECEIPT = 'ReceiptRecipient';
     K_TIMEOUT = 'TimeOut';

     K_DEF_MAILSERVER = '255.255.255.255';
     K_DEF_PORT = K_EMAIL_PORT;
     K_DEF_SUBTYPE = Ord( emtTexto );
     K_DEF_USER = 'Usuario';
     K_DEF_PASSWORD = 'PassWord';
     K_DEF_FROMADDRESS = 'usuario@dominio.com';
     K_DEF_FROMNAME = 'Nombre del Usuario';
     K_DEF_REPLYTO = 'usuario@dominio.com';
     K_DEF_RECEIPT = '';
     K_DEF_TIMEOUT = 0;


{ ******** TdmEmailService ******* }

procedure TdmEmailService.DataModuleCreate(Sender: TObject);
const
     H_KEY_SOFTWARE = 'Software\Grupo Tress\TressWin\';
begin
     FMessageText := TStringList.Create;
     FToAddress := TStringList.Create;
     FToCarbonCopy := TStringList.Create;
     FToBlindCarbonCopy := TStringList.Create;
     FAttachments := TStringList.Create;
     FLista := TStringList.Create;
     FMensaje :=TIdMessage.Create( eMail );
     FRegistryKey := H_KEY_SOFTWARE;
end;

procedure TdmEmailService.DataModuleDestroy(Sender: TObject);
begin
     if Assigned( FRegistry ) then
        FreeAndNil( FRegistry );
     FreeAndNil( FMensaje );
     FreeAndNil( FLista );
     FreeAndNil( FAttachments );
     FreeAndNil( FToBlindCarbonCopy );
     FreeAndNil( FToCarbonCopy );
     FreeAndNil( FToAddress );
     FreeAndNil( FMessageText );
end;

procedure TdmEmailService.InitRegistry;
begin
     if not Assigned( FRegistry ) then
     begin
          FRegistry := TRegistry.Create;
          with FRegistry do
          begin
               RootKey := HKEY_LOCAL_MACHINE;
          end;
     end;
end;

function TdmEmailService.GetKeyName( const sKeyName: String ): String;
begin
     Result := FRegistryKey + sKeyName;
end;

function TdmEmailService.GetFileName( const sFileName: String ): String;
begin
     if ZetaCommonTools.StrVacio( SysUtils.ExtractFileDir( sFileName ) ) then
        Result := ZetaCommonTools.VerificaDir( ExtractFileDir( Application.ExeName ) ) + sFileName
     else
         Result := sFileName;
end;

procedure TdmEmailService.ReadAddressSection( const sSeccion: String; Direcciones: TStrings );
var
   i: Integer;
begin
     with FLista do
     begin
          Clear;
          FIniFile.ReadSectionValues( sSeccion, FLista );
          for i := 0 to ( Count - 1 ) do
          begin
               Direcciones.Add( Values[ Names[ i ] ] );
          end;
     end;
end;

procedure TdmEmailService.WriteAddressSection( const sSeccion, sSubSeccion : String; Direcciones: TStrings );
var
   i: Integer;
begin
     with FIniFile do
     begin
          EraseSection( sSeccion );
          for i:= 0 to ( Direcciones.Count - 1 ) do
          begin
               WriteString( sSeccion, Format( sSubSeccion, [ i ] ), Direcciones[ i ] );
          end;
     end;
end;

procedure TdmEmailService.WriteAddressKey( const sKey, sSubKey: String; Direcciones: TStrings );
var
   i: Integer;
begin
     with FRegistry do
     begin
          if OpenKey( sKey, True ) then
          begin
               for i := 0 to ( Direcciones.Count - 1 ) do
               begin
                    WriteString( Format( sSubKey, [ i ] ), Direcciones[ i ] );
               end;
          end;
          CloseKey;
     end;
end;

procedure TdmEmailService.ReadAddressKey( const sKey, sSubKey: String; Direcciones: TStrings );
var
   Llave: TRegKeyInfo;
   i: Integer;
begin
     with FRegistry do
     begin
          if OpenKey( sKey, FALSE ) and GetKeyInfo( Llave ) then
          begin
               for i:= 0 to ( Llave.NumValues - 1 ) do
               begin
                    Direcciones.Add( ReadString( Format( sSubKey, [ i ] ) ) );
               end;
               CloseKey;
          end;
     end;
end;

function TdmEmailService.ReadFromIniFile( const sFileName: String ): Boolean;
begin
     NewEmail;
     FIniFile := TIniFile.Create( GetFileName( sFileName ) );
     try
        with FIniFile do
        begin
             MailServer :=  ReadString( K_SECCION, K_MAILSERVER, K_DEF_MAILSERVER );
             Port :=  StrToInt( ReadString( K_SECCION, K_PORT, IntToStr( K_DEF_PORT ) ) );
             SubType :=  eEmailType( StrToInt( ReadString( K_SECCION, K_SUBTYPE, IntToStr( K_DEF_SUBTYPE ) ) ) );
             User :=  ReadString( K_SECCION, K_USER, K_DEF_USER );
             PassWord :=  ReadString( K_SECCION, K_PASSWORD, K_DEF_PASSWORD );
             FromAddress :=  ReadString( K_SECCION, K_FROMADDRESS, K_DEF_FROMADDRESS );
             FromName :=  ReadString( K_SECCION, K_FROMNAME, K_DEF_FROMNAME );
             ReplyToName :=  ReadString( K_SECCION, K_REPLYTO, K_DEF_REPLYTO );
             ReplyToAddress := ReplyToName;
             ReceiptRecipientName := ReadString( K_SECCION, K_RECEIPT, K_DEF_RECEIPT );
             ReceiptRecipientAddress := ReceiptRecipientName;
             TimeOut :=  StrToInt( ReadString( K_SECCION, K_TIMEOUT, IntToStr( K_DEF_TIMEOUT ) ) );

             ReadAddressSection( K_SECCION_TO_ADDRESS, FToAddress );
             ReadAddressSection( K_SECCION_CARBON_ADDRESS, FToCarbonCopy );
             ReadAddressSection( K_SECCION_BLIND_ADDRESS, FToBlindCarbonCopy );

        end;
        Result := TRUE;
     finally
            FreeAndNil( FIniFile );
     end;
end;

function TdmEmailService.WriteToIniFile( const sFileName: String ): Boolean;
begin
     FIniFile := TIniFile.Create( GetFileName( sFileName ) );
     try
        with FIniFile do
        begin
             WriteString( K_SECCION, K_MAILSERVER, MailServer );
             WriteString( K_SECCION, K_PORT, IntToStr( Port ) );
             WriteString( K_SECCION, K_SUBTYPE, IntToStr( Ord( SubType ) ) );
             WriteString( K_SECCION, K_USER, User );
             WriteString( K_SECCION, K_PASSWORD, PassWord );
             WriteString( K_SECCION, K_FROMADDRESS, FromAddress );
             WriteString( K_SECCION, K_FROMNAME, FromName );
             WriteString( K_SECCION, K_REPLYTO, ReplyToAddress );
             WriteString( K_SECCION, K_RECEIPT, ReceiptRecipientAddress );
             WriteString( K_SECCION, K_TIMEOUT, IntToStr( TimeOut ) );

             WriteAddressSection( K_SECCION_TO_ADDRESS, K_TO_ADDRESS, FToAddress );
             WriteAddressSection( K_SECCION_CARBON_ADDRESS, K_CARBON_ADDRESS, FToCarbonCopy );
             WriteAddressSection( K_SECCION_BLIND_ADDRESS, K_BLIND_ADDRESS, FToBlindCarbonCopy );

        end;
        Result := TRUE;
     finally
            FreeAndNil( FIniFile );
     end;
end;

procedure TdmEmailService.ReadSectionFromRegistry( const sSectionKey, sSectionName, sKey: String; Lista: TStrings );
begin
     with FRegistry do
     begin
          ReadAddressKey( sSectionKey + '\' + sSectionName, sKey, Lista );
     end;
end;

procedure TdmEmailService.WriteSectionToRegistry( const sSectionKey, sSectionName, sKey: String; Lista: TStrings );
begin
     with FRegistry do
     begin
          ReadAddressKey( sSectionKey + '\' + sSectionName, sKey, Lista );
     end;
end;

function TdmEmailService.ReadFromRegistry( const sKeyName: String ): Boolean;
var
   sKey: String;
begin
     NewEMail;
     InitRegistry;
     with FRegistry do
     begin
          sKey := GetKeyName( sKeyName );
          Result := OpenKey( sKey, FALSE );
          if Result then
          begin
               MailServer := ReadString( K_MAILSERVER );
               Port := ReadInteger( K_PORT );
               SubType := eEmailType( ReadInteger( K_SUBTYPE ) );
               User := ReadString( K_USER );
               PassWord := ReadString( K_PASSWORD );
               FromAddress := ReadString( K_FROMADDRESS );
               FromName := ReadString( K_FROMNAME );
               ReplyToName := ReadString( K_REPLYTO );
               ReplyToAddress := ReplyToName;
               ReceiptRecipientName := ReadString( K_RECEIPT );
               ReceiptRecipientAddress := ReceiptRecipientName;
               TimeOut := ReadInteger( K_TIMEOUT );
               ReadSectionFromRegistry( sKey, K_SECCION_TO_ADDRESS, K_TO_ADDRESS, FToAddress );
               ReadSectionFromRegistry( sKey, K_SECCION_CARBON_ADDRESS, K_CARBON_ADDRESS, FToCarbonCopy );
               ReadSectionFromRegistry( sKey, K_SECCION_BLIND_ADDRESS, K_BLIND_ADDRESS, FToBlindCarbonCopy );
          end;
          CloseKey;
     end;
end;

function TdmEmailService.WriteToRegistry( const sKeyName: String ): Boolean;
var
   sKey: String;
begin
     InitRegistry;
     with FRegistry do
     begin
          sKey := GetKeyName( sKeyName );
          Result := OpenKey( sKey, TRUE );
          if Result then
          begin
               WriteString( K_MAILSERVER, MailServer );
               WriteInteger( K_PORT, Port );
               Writeinteger( K_SUBTYPE, Ord( SubType ) );
               WriteString( K_USER, User );
               WriteString( K_PASSWORD, PassWord );
               WriteString( K_FROMADDRESS, FromAddress );
               WriteString( K_FROMNAME, FromName );
               WriteString( K_REPLYTO, ReplyToAddress );
               WriteString( K_RECEIPT, ReceiptRecipientAddress );
               WriteInteger( K_TIMEOUT, TimeOut );
               WriteSectionToRegistry( sKey, K_SECCION_TO_ADDRESS, K_TO_ADDRESS, FToAddress );
               WriteSectionToRegistry( sKey, K_SECCION_CARBON_ADDRESS, K_CARBON_ADDRESS, FToCarbonCopy );
               WriteSectionToRegistry( sKey, K_SECCION_BLIND_ADDRESS, K_BLIND_ADDRESS, FToBlindCarbonCopy );
          end;
          CloseKey;
     end;
end;

function TdmEmailService.VerifyEmailAddresses( var sErrorMsg: String ): Boolean;
begin
     Result := True;
end;

function TdmEmailService.Validate( var sErrorMsg: String ): Boolean;
begin
     Result := True;
end;

procedure TdmEmailService.NewBodyMessage;
begin
     Self.ToAddress.Clear;
     Self.ToCarbonCopy.Clear;
     Self.ToBlindCarbonCopy.Clear;
     Self.MessageText.Clear;
     Self.Attachments.Clear;
     Subject := VACIO;
end;

procedure TdmEmailService.NewEmail;
begin
     MailServer := VACIO;
     Port := K_EMAIL_PORT;
     SubType := emtTexto;
     User := VACIO;
     PassWord := VACIO;
     FromAddress := VACIO;
     FromName := VACIO;
     ReplyToName := VACIO;
     ReplyToAddress := VACIO;
     ReceiptRecipientName := VACIO;
     ReceiptRecipientAddress := VACIO;
     TimeOut := 0;
     NewBodyMessage;
end;

procedure TdmEmailService.BuildAttachmentList( Lista: TStrings; MsgParts: TIdMessageParts );
var
   i: Integer;
   sArchivo: String;
   Attachment: TIdAttachment;
begin
     with Lista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               sArchivo := Strings[ i ];
               if ZetaCommonTools.StrLleno( sArchivo ) and SysUtils.FileExists( sArchivo ) then
               begin
                    Attachment := TIdAttachment.Create( MsgParts, sArchivo );
                    with Attachment do
                    begin
                         FileIsTempFile := False;
                    end;
               end;
          end;
     end;
end;

procedure TdmEmailService.BuildAddressList( Lista: TStrings; Direcciones: TIdEmailAddressList );
var
   i: Integer;
   sDireccion: String;
begin
     with Lista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               sDireccion := Strings[ i ];
               if ZetaCommonTools.StrLleno( sDireccion ) then
               begin
                    with Direcciones.Add do
                    begin
                         Name := sDireccion;
                         Address := sDireccion;
                    end;
               end;
          end;
     end;
end;

procedure TdmEmailService.ConstruirMensaje;
begin
     with FMensaje do
     begin
          Clear;
          UseNowForDate := True;
          case Self.SubType of
               emtTexto: ContentType := 'text/plain';
               emtHTML: ContentType := 'text/html';
          end;
          with From do
          begin
               Name := Self.FromName;
               Address := Self.FromAddress;
          end;
          with Sender do
          begin
               Name := Self.FromName;
               Address := Self.FromAddress;
          end;
          with ReplyTo.Add do
          begin
               Name := Self.ReplyToName;
               Address := Self.ReplyToAddress;
          end;
          with ReceiptRecipient do
          begin
               Name := Self.ReceiptRecipientName;
               Address := Self.ReceiptRecipientAddress;
          end;
          Subject := Self.Subject;
          Body.AddStrings( Self.MessageText );
          BuildAddressList( Self.ToAddress, Recipients );
          BuildAddressList( Self.ToBlindCarbonCopy, BCCList );
          BuildAddressList( Self.ToCarbonCopy, CCList );
          BuildAttachmentList( Self.Attachments, MessageParts );
     end;
end;

function TdmEmailService.MailServerDisconnect( var sErrorMsg : String ): Boolean;
begin
     Result := False;
     with Email do
     begin
          try
             DisConnect;
             Result := True;
          except
                on Error:Exception do
                begin
                     sErrorMsg := Error.Message;
                end;
          end;
     end;
end;

function TdmEmailService.MailServerConnect( var sErrorMsg : String ): Boolean;
begin
     Result := MailServerDisconnect( sErrorMsg );
     if Result then
     begin
          with Email do
          begin
               Host := Self.MailServer;
               UserName := Self.User;
               Password := Self.Password;
               Port := Self.Port;
               if ZetaCommonTools.StrLleno( UserName ) and ZetaCommonTools.StrLleno( Password ) then
                   AuthenticationType := atLogin
               else
                    AuthenticationType := atNone;
               try
                  Connect;
                  Result := True;
               except
                     on Error:Exception do
                     begin
                          sErrorMsg := Error.Message;
                     end;
               end;
          end;
     end;
end;

function TdmEmailService.SendEmail( var sErrorMsg: String ): Boolean;
begin
     Result := False;
     if MailServerConnect( sErrorMsg ) then
     begin
          ConstruirMensaje;
          EMail.Send( FMensaje );
          Result := MailServerDisconnect( sErrorMsg );
     end;
end;

end.

