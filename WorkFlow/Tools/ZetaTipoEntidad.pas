unit ZetaTipoEntidad;

interface

uses Classes;

{$DEFINE MULTIPLES_ENTIDADES}

type
    { Tipo Entidades}
    TipoEntidad = smallint;
const
                    enNinguno    =              0;       {   'Ninguno',                          }
                    enCompanys    =             1;       {   'Empresa',                          }
                    enGrupo    =                2;       {   'Grupo',                            }
                    enGrupoPadre    =           3;       {   'Grupo Padre',                      }
                    enUsuarios    =             4;       {   'Usuario',                          }
                    enUsuarioJefe    =          5;       {   'Jefe',                             }
                    enBitacora    =             6;       {   'Bitacora',                         }
                    enWizard    =               7;       {   'Wizard',                           }
                    enDiccion    =              8;       {   'Diccionario de Datos',             }
                    enQuerys    =               9;       {   'Condiciones',                      }
                    enReporte    =              10;      {   'Datos Generales de Reportes',      }
                    enMisReportes    =          11;      {   'Mis Reportes Favoritos',           }
                    enCampoRep    =             12;      {   'Detalle de Campos de Reportes',    }
                    enFunciones    =            13;      {   'Funciones',                        }
                    enFormula    =              14;      {   'F�rmulas',                         }
                    enRol    =                  15;      {   'Rol',                              }
                    enUserRol    =              16;      {   'Asignaci�n De Roles',              }
                    enAccion    =               17;      {   'Accion',                           }
                    enModelo    =               18;      {   'Modelo',                           }
                    enModeloPasos    =          19;      {   'Pasos De Modelo',                  }
                    enProceso    =              20;      {   'Proceso',                          }
                    enProcesoPasos    =         21;      {   'Pasos De Proceso',                 }
                    enProcesoArchivos    =      22;      {   'Archivo',                          }
                    enTarea    =                23;      {   'Tarea',                            }
                    enMensaje    =              24;      {   'Mensajes',                         }
                    enBuzon =                   25;      {   'Buzon' );                          }
                    eConfiUsuarios =            26;

    MaxTipoEntidad = 25;
    {$ifndef MULTIPLES_ENTIDADES}
    ListaEntidades = set of TipoEntidad;
    {$ENDIF}

     EntidadesDescontinuadas {$IFNDEF MULTIPLES_ENTIDADES}ListaEntidades{$ENDIF} = [];
     NomParamEntidades {$IFNDEF MULTIPLES_ENTIDADES}:ListaEntidades{$ENDIF} = [];
     EntidadDefaultCondicion = enNinguno;
     EntidadConCondiciones {$IFNDEF MULTIPLES_ENTIDADES}:ListaEntidades{$ENDIF} =[];
     ReporteEspecial {$IFNDEF MULTIPLES_ENTIDADES}:ListaEntidades{$ENDIF} = [];

     ReportesSinOrdenDef = [];
     EntidadesNomina {$IFNDEF MULTIPLES_ENTIDADES}:ListaEntidades{$ENDIF} = [];


var
   aTipoEntidad: array[ 0..MaxTipoEntidad ] of PChar =

   ( 'Ninguno',                              
     'Empresa',                              
     'Grupo',
     'Grupo Padre',                          
     'Usuario',                              
     'Jefe',                                 
     'Bitacora',                             
     'Wizard',                               
     'Diccionario de Datos',                 
     'Condiciones',                          
     'Datos Generales de Reportes',          
     'Mis Reportes Favoritos',               
     'Detalle de Campos de Reportes',        
     'Funciones',                            
     'F�rmulas',                             
     'Rol',                                  
     'Asignaci�n De Roles',                  
     'Accion',                               
     'Modelo',                               
     'Pasos De Modelo',                      
     'Proceso',                              
     'Pasos De Proceso',                     
     'Archivo',                              
     'Tarea',                                
     'Mensajes',
     'Buzon' );

function ObtieneEntidad( const Index: TipoEntidad ): String;
procedure LlenaTipoPoliza( Lista: TStrings );
function Dentro( const Entidad: TipoEntidad; ListaEntidades: Array of TipoEntidad ): Boolean;


implementation

uses ZetaTipoEntidadTools;

function ObtieneEntidad( const Index: TipoEntidad ): String;
begin
     Result :=  aTipoEntidad[ Index  ];
end;

procedure LlenaTipoPoliza( Lista: TStrings );
begin
     Lista.Clear;
end;

function Dentro( const Entidad: TipoEntidad; ListaEntidades: Array of TipoEntidad ): Boolean;
begin
     {$IFDEF MULTIPLES_ENTIDADES}
     Result := ZetaTipoEntidadTools.Dentro(Entidad,ListaEntidades);
     {$ELSE}
     Result := FALSE;
     {$ENDIF}
end;

end.
