unit FConstruyeFormula;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,
  ZBaseConstruyeFormula,
  ZetaTipoEntidad,
  ZetaCommonLists;

type
  TConstruyeFormula = class(TBaseConstruyeFormula)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ConstruyeFormula: TConstruyeFormula;

implementation

uses ZHelpContext;

{$R *.DFM}

procedure TConstruyeFormula.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_TOOLS_CONSTRUYE_FORMULA;
end;

end.
