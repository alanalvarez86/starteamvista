unit ZGlobalTress;

interface

uses ZetaCommonLists,
     ZetaCommonClasses;

const
     K_NUM_GLOBALES = 60;
     K_NUM_DEFAULTS = 0;
     K_TOT_GLOBALES = K_NUM_GLOBALES + K_NUM_DEFAULTS;
     K_BASE_DEFAULTS = 2000;
     K_TOT_DESCRIPCIONES_GV = 13;

     { Globales DUMMYS que se requiren para poder compilar }

     K_GLOBAL_PRIMER_ADICIONAL         = 0;
     K_GLOBAL_LAST_ADICIONAL           = 0;
     K_GLOBAL_TEXTO_BASE               = 0;
     K_GLOBAL_NUM_BASE                 = 0;
     K_GLOBAL_LOG_BASE                 = 0;
     K_GLOBAL_FECHA_BASE               = 0;
     K_GLOBAL_TAB_BASE                 = 0;
     K_GLOBAL_DIGITO_EMPRESA           = 0;     // Se utiliza en ZFuncsGlobal

     {Constantes para Globales}

     K_GLOBAL_NOMBRE                 = 1; { Texto: Nombre Del Proyecto }
     K_GLOBAL_ROOT                   = 2; { Texto: Ruta Ra�z }
     K_GLOBAL_RUTA_DOC               = 3; { Texto: Ruta Para Documentos }
     K_GLOBAL_PAGINADOS              = 4; { Integer: Registros Por P�gina }
     K_GLOBAL_FOTOS                  = 5; { Texto: Ruta Para Im�genes de Noticias }
     K_GLOBAL_EMPRESA                = 6; { Texto: Nombre de la Empresa  }
     K_GLOBAL_RAZON_EMPRESA          = K_GLOBAL_EMPRESA;
     K_GLOBAL_DIRECCION              = 7; { Texto: Direcci�n  }
     K_GLOBAL_CIUDAD                 = 8; { Texto: Ciudad }

     K_GLOBAL_ESTADO                 = 9; { MV(01/Oct/2008): Este global se esta utilizando dentro de WorkFlowCFG.exe para obtener informaci�n
                                            del estado que se haya configurado dentro de los Globales. No provoca problema con la cosntante
                                            K_GLOBAL_NIVEL_MAX que se usa para obtener los numero de de Niveles en Sistema TRESS }
     K_GLOBAL_LOGOBIG                = 10; { Texto: Ruta del Logo Grande }
     K_GLOBAL_LOGOSMALL              = 11; { Texto: Ruta del Logo Chico }
     //K_GLOBAL_TITULAR                = 12; { Se reemplaz� : Integer: Integer: Art�culo Titular: Noticia Titular }
     K_GLOBAL_NIVEL_MAX              = 9;   { M�ximo 9 Niveles de Organigrama }
     K_GLOBAL_NIVEL_BASE             = 12; { Facilita OFFSET de Niveles }
     K_GLOBAL_EMAIL_DEFAULT          = 13; { Texto: Lista de eMails default }
     K_GLOBAL_EMAIL_SERVER           = 14; { Texto: Servidor de eMail POP3 }
     K_GLOBAL_EMAIL_AUTO             = 15; { Texto: eMail para Auto Response }
     K_GLOBAL_PAGE_REFRESH           = 16; { Integer: Refrescado de P�gina }
     K_GLOBAL_DIR_FOTOS              = 17; {Texto: Directorio de Fotografias para las Noticias}
     K_GLOBAL_DIR_DOCS               = 18; {Texto: Directorio de Documentos}
     K_GLOBAL_RUTA_PAGINA            = 19; { Texto: Ruta de la P�gina de la Empresa }
     K_GLOBAL_RUTA_PORTAL            = 20; { Texto: Ruta del Directorio Virtual del Portal }
     K_GLOBAL_DIR_EMPLEADOS          = 21; { Texto: Ruta virtual de las fotos de empleados}
     K_GLOBAL_RUTA_EMPLEADOS         = 22; { Texto: Ruta en disco de las fotos de empleados}
     K_GLOBAL_USER_DEFAULT           = 23; { Numero del Usuario que se tomar� como Default }
     K_GLOBAL_GRABA_BITACORA         = 24; { Indica si se desea que se graben los cambios en Bit�cora }
     K_GLOBAL_VERSION_DATOS          = 25; { Versi�n De La Base De Datos }
     K_GLOBAL_DIR_PLANT              = 26; { Directorio De Plantillas }

     K_WORKFLOW_FILE_PATH            = 27; { Directorio Donde Se Guardan Los Documentos De WorkFlow }
     K_WORKFLOW_FILE_URL             = 28; { URL hacia el sitio donde se encuentran los Documentos de WorkFlow }
     K_WORKFLOW_XSLFILE_PATH         = 29; { Directorio Donde Residen Los Archivos XSL }
     K_WORKFLOW_USER                 = 30; { Numero del Usuario Para Motor De WorkFlow }
     K_WORKFLOW_DIRECTORIO_RAIZ      = 55;  {URL PARA INDICAR LA RUTA DEL DIRECTORIO RAIZ DE WORKFLOW}

     K_WORKFLOW_MANAGER_NAME         = 31; { Nombre Del Administrador De WorkFlow }
     K_WORKFLOW_MANAGER_EMAIL        = 32; { eMail Del Administrador De WorkFlow }
     K_WORKFLOW_SENDER_NAME          = 33; { Nombre Del Remitente De Los Correos Enviados Por WorkFlow }
     K_WORKFLOW_SENDER_EMAIL         = 34; { eMail Del Remitente De Los Correos Enviados Por WorkFlow }
     K_WORKFLOW_RECEIPT_NAME         = 35; { Nombre Del Acusador De Recibo De Los Correos Enviados Por WorkFlow }
     K_WORKFLOW_RECEIPT_EMAIL        = 36; { eMail Del Acusador De Recibo De Los Correos Enviados Por WorkFlow }

     K_WORKFLOW_EMAIL_SERVER         = 37; { Servidor de Correos Para WorkFlow }
     K_WORKFLOW_EMAIL_USER           = 38; { Usuario Para Acceder Al Servidor de Correos }
     K_WORKFLOW_EMAIL_PASSWORD       = 39; { Contrase�a Del Usuario Para Acceder Al Servidor de Correos }
     K_WORKFLOW_EMAIL_PORT           = 40; { Puerto Usado Por Servidor de Correos Para WorkFlow }
     K_WORKFLOW_EMAIL_TIMEOUT        = 41; { Puerto Usado Por Servidor de Correos Para WorkFlow }

     K_WORKFLOW_XSL_PATH             = 42; { XSL: Directorio }
     K_WORKFLOW_XSL_TAREA            = 43; { XSL: Tareas }

     K_WORKFLOW_XSL_PROCESO_ACTIVAR  = 44; { XSL: Proceso Activar }
     K_WORKFLOW_XSL_PROCESO_CANCELAR = 45; { XSL: Proceso Cancelar }
     K_WORKFLOW_XSL_PROCESO_TERMINAR = 46; { XSL: Proceso Terminar }
     K_WORKFLOW_XSL_PROCESO_SUSPENDER= 50; { XSL: Proceso Suspender }

     K_WORKFLOW_XSL_PASO_ACTIVAR     = 47; { XSL: Paso Proceso Activar}
     K_WORKFLOW_XSL_PASO_CANCELAR    = 48; { XSL: Paso Proceso Cancelar }
     K_WORKFLOW_XSL_PASO_TERMINAR    = 49; { XSL: Paso Proceso Terminar}
     K_WORKFLOW_XSL_PASO_SUSPENDER   = 51; { XSL: Paso Proceso Suspender}
     K_WORKFLOW_XSL_PASO_MODIFICAR   = 56; { XSL: Notificacion Modificar Datos }
     K_WORKFLOW_XSL_PASO_DESPUES_INICIAR = 57; { XSL: Notificacion Despues Iniciar }
     K_WORKFLOW_XSL_PASO_ANTES_LIMITE = 58; { XSL: Notificacion Antes Limite }
     K_WORKFLOW_XSL_PASO_DESPUES_LIMITE = 59; {XSL: Notificacion Despues Limite }


     {GLOBALES PARA KIOSCO}
     K_KIOSCO_DIRECTORIO_RAIZ        = 52; {URL PARA INDICAR LA RUTA DEL DIRECTORIO RAIZ DE KIOSCO}
     K_KIOSCO_VALORES_ACTIVOS_URL    = 53; {URL PARA INDICAR LA RUTA DE LOS VALORES ACTIVOS}
     K_KIOSCO_PAGINA_PANTALLA        = 54; {URL PARA INDICAR LA RUTA DE LA PANTALLA}
     K_KIOSCO_LIMITE_IMPRESIONES     = 60; {LIMITE DE IMPRESIONES POR PERIODO }


     {Boton bitacora}

     { ��� OJO !!!   CUANDO SE AGREGUE UNA CONSTANTE NUEVA

     1) SE TIENE QUE AGREGAR EN EL METODO DGLOBAL.GETTIPOGLOBAL CUANDO EL GLOBAL NO SEA TEXTO
     2) Se debe aumentar en 1 la constante K_NUM_GLOBALES ( est� al principio de esta unidad )
     3) Se debe considerar la constante en la funci�n GetTipoGlobal ( est� al final de esta unidad )
     }

function GetTipoGlobal( const iCodigo: Integer ): eTipoGlobal;
function GlobalToPos( const iCodigo: Integer ): Integer;
function PosToGlobal( const iPos: Integer ): Integer;
function GetDescripcionGlobal( const iCodigo: integer ): string;

implementation

{ Permite compactar el arreglo de Globales }
{ Los globales arriba de 2000, les resta un offset }

function GlobalToPos( const iCodigo: Integer ): Integer;
begin
     if ( iCodigo <= K_NUM_GLOBALES ) then
        Result := iCodigo
     else
        Result := iCodigo - K_BASE_DEFAULTS + K_NUM_GLOBALES;
end;

function PosToGlobal( const iPos: Integer ): Integer;
begin
     if ( iPos <= K_NUM_GLOBALES ) then
        Result := iPos
     else
        Result := iPos + K_BASE_DEFAULTS - K_NUM_GLOBALES;
end;

function GetTipoGlobal( const iCodigo: Integer ): eTipoGlobal;
begin
     // Si se tienen Globales que no sean de Texto deber�n condicionarse para regresar otros
     // tipos Globales: tgBooleano, tgFloat, tgNumero, tgFecha, etc.
     case iCodigo of
          K_GLOBAL_PAGINADOS: Result := tgNumero;
         // K_GLOBAL_TITULAR: Result := tgNumero;
          K_GLOBAL_PAGE_REFRESH: Result := tgNumero;
          K_GLOBAL_USER_DEFAULT: Result := tgNumero;
          K_WORKFLOW_USER: Result := tgNumero;
          K_WORKFLOW_EMAIL_PORT: Result := tgNumero;
          K_WORKFLOW_EMAIL_TIMEOUT: Result := tgNumero;
          K_KIOSCO_LIMITE_IMPRESIONES: Result := tgNumero;
     else
         Result := tgTexto;
     end;
end;

function GetDescripcionGlobal( const iCodigo: integer ): String;
begin
     case iCodigo of
          K_GLOBAL_NOMBRE: Result := 'Nombre Del Proyecto';
          K_GLOBAL_ROOT: Result := 'Ruta Ra�z';
          K_GLOBAL_RUTA_DOC: Result := 'Ruta Para Documentos';
          K_GLOBAL_PAGINADOS: Result := 'Registros Por P�gina';
          K_GLOBAL_FOTOS: Result := 'Ruta Para Im�genes de Noticias';
          K_GLOBAL_EMPRESA: Result := 'Nombre de la Empresa';
          K_GLOBAL_DIRECCION: Result := 'Direcci�n';
          K_GLOBAL_CIUDAD: Result := 'Ciudad';
          K_GLOBAL_ESTADO: Result := 'Estado';
          K_GLOBAL_LOGOBIG: Result := 'Ruta del Logo Grande';
          K_GLOBAL_LOGOSMALL: Result := 'Ruta del Logo Chico';
          //K_GLOBAL_TITULAR: Result := 'Art�culo Titular: Noticia Titular';
          K_GLOBAL_EMAIL_DEFAULT: Result := 'Lista de eMails default';
          K_GLOBAL_EMAIL_SERVER: Result := 'Servidor de eMail POP3';
          K_GLOBAL_EMAIL_AUTO: Result := 'eMail para Auto Response';
          K_GLOBAL_PAGE_REFRESH: Result := 'Refrescado de P�gina';
          K_GLOBAL_DIR_FOTOS: Result := 'Directorio de Fotografias para las Noticias';
          K_GLOBAL_DIR_DOCS: Result := 'Directorio de Documentos';
          K_GLOBAL_RUTA_PAGINA: Result := 'Ruta de la P�gina de la Empresa';
          K_GLOBAL_RUTA_PORTAL: Result := 'Ruta del Directorio Virtual del Portal';
          K_GLOBAL_DIR_EMPLEADOS: Result := 'Ruta virtual de las fotos de empleados';
          K_GLOBAL_RUTA_EMPLEADOS: Result := 'Ruta en disco de las fotos de empleados';
          K_GLOBAL_USER_DEFAULT: Result := 'N�mero del Usuario que se tomar� como Default';
          K_GLOBAL_GRABA_BITACORA: Result := 'Movimientos que se registran en Bit�cora';
          K_GLOBAL_VERSION_DATOS: Result := 'Versi�n De La Base De Datos';
          K_GLOBAL_DIR_PLANT: Result := 'Directorio De Plantillas';
          K_WORKFLOW_FILE_PATH: Result := 'Directorio Donde Se Guardan Los Documentos De WorkFlow';
          K_WORKFLOW_FILE_URL: Result := 'URL hacia el sitio donde se encuentran los Documentos de WorkFlow';
          K_WORKFLOW_XSLFILE_PATH: Result := 'Directorio Donde Residen Los Archivos XSL';
          K_WORKFLOW_USER: Result := 'Numero del Usuario Para Motor De WorkFlow';
          K_WORKFLOW_DIRECTORIO_RAIZ: Result := 'URL del directorio virtual de workflow';
          K_WORKFLOW_MANAGER_NAME: Result := 'Nombre Del Administrador De WorkFlow';
          K_WORKFLOW_MANAGER_EMAIL: Result := 'eMail Del Administrador De WorkFlow';
          K_WORKFLOW_SENDER_NAME: Result := 'Nombre Del Remitente De Los Correos Enviados Por WorkFlow';
          K_WORKFLOW_SENDER_EMAIL: Result := 'eMail Del Remitente De Los Correos Enviados Por WorkFlow';
          K_WORKFLOW_RECEIPT_NAME: Result := 'Nombre Del Acusador De Recibo De Los Correos Enviados Por WorkFlow';
          K_WORKFLOW_RECEIPT_EMAIL: Result := 'eMail Del Acusador De Recibo De Los Correos Enviados Por WorkFlow';
          K_WORKFLOW_EMAIL_SERVER: Result := 'Servidor de Correos Para WorkFlow';
          K_WORKFLOW_EMAIL_USER: Result := 'Usuario Para Acceder Al Servidor de Correos';
          K_WORKFLOW_EMAIL_PASSWORD: Result := 'Contrase�a Del Usuario Para Acceder Al Servidor de Correos';
          K_WORKFLOW_EMAIL_PORT: Result := 'Puerto Usado Por Servidor de Correos Para WorkFlow';
          K_WORKFLOW_EMAIL_TIMEOUT: Result := 'Puerto Usado Por Servidor de Correos Para WorkFlow';
          K_WORKFLOW_XSL_PATH: Result := 'XSL: Directorio';
          K_WORKFLOW_XSL_TAREA: Result := 'XSL: Tareas';
          K_WORKFLOW_XSL_PROCESO_ACTIVAR: Result := 'XSL: Proceso Activar';
          K_WORKFLOW_XSL_PROCESO_CANCELAR: Result := 'XSL: Proceso Cancelar';
          K_WORKFLOW_XSL_PROCESO_TERMINAR: Result := 'XSL: Proceso Terminar';
          K_WORKFLOW_XSL_PASO_ACTIVAR: Result := 'XSL: Paso Proceso Activar';
          K_WORKFLOW_XSL_PASO_CANCELAR: Result := 'XSL: Paso Proceso Cancelar';
          K_WORKFLOW_XSL_PASO_TERMINAR: Result := 'XSL: Paso Proceso Terminar';
          K_KIOSCO_DIRECTORIO_RAIZ:  Result := 'URL para indicar la ruta del directorio ra�z';
          K_KIOSCO_VALORES_ACTIVOS_URL:  Result := 'URL para indicar la ruta de los valores activos';
          K_KIOSCO_PAGINA_PANTALLA:  Result := 'URL para indicar la ruta de la pantalla';
          K_KIOSCO_LIMITE_IMPRESIONES: Result := 'L�mite de Impresiones Por Periodo';
     end;
end;

end.
