unit ZHelpContext;

interface

const
   H_HISTORIAL                        = 1;
   H_HISTORIAL_PROCESOS               = 2;
   H_HISTORIAL_TAREAS                 = 3;
   H_HISTORIAL_CORREOS                = 4;

   {******** Consultas **********}

   H_CONSULTAS                        = 5;
   H_CONS_REPORTES                    = 6;
   H_REPORTES_CONFIDENCIALES          = 9;
   H_REPORTES_WORKFLOW                = 30;
   H_REPORTES_PORTAL                  = 31;
   H_REPORTES_SISTEMA                 = 32;
   H_REPORTES_FAVORITOS               = 33;
   H_CONS_SQL                         = 7;
   H_CONS_BITACORA                    = 8;
   H_TOOLS_CONSTRUYE_FORMULA          = 34;

   {******** Catálogos **********}

   H_CATALOGOS                        = 10;
   H_CAT_MODELOS                      = 11;
   H_CAT_PASOS_MODELOS                = 12;
   H_CAT_USUARIOS                     = 13;
   H_CAT_ROLES                        = 14;
   H_CAT_ACCIONES                     = 15;
   H_CATALOGOS_GENERALES              = 16;
   H_CAT_GRALES_CONDICIONES           = 17;
   H_CATALOGOS_CONFIGURACION          = 18;
   H_CAT_CONFI_DICCIONARIO            = 19;
   H_CAT_CONFI_GLOBALES               = 20;

   {******** Sistema **********}
   {
   Estos DEBEN de ser iguales en todas las aplicaciones
   que usan COMPARTE - ver D:\3Win_20\Tools\zAccesosTress.pas
   }
   H_SISTEMA                          = 21;
   H_SIST_DATOS                       = 22;
   H_SIST_DATOS_EMPRESAS              = 23;
   H_SIST_DATOS_USUARIOS              = 24;
   H_SIST_DATOS_GRUPOS                = 25;
   H_SIST_DATOS_GRUPOS_ACCESOS        = 26;
   H_SIST_DATOS_IMPRESORAS            = 27;
   H_SIST_PRENDE_USUARIOS             = 28;
   H_SIST_APAGA_USUARIOS              = 29;

implementation

end.
