unit FConstruyeFormula_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,
  ZBaseConstruyeFormula,
  ZetaTipoEntidad,
  ZetaCommonLists;

type
  TConstruyeFormula_DevEx = class(TBaseConstruyeFormula)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ConstruyeFormula_DevEx: TConstruyeFormula_DevEx;

implementation

uses ZHelpContext;

{$R *.DFM}

procedure TConstruyeFormula_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_TOOLS_CONSTRUYE_FORMULA;
end;

end.
