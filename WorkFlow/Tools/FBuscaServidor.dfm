inherited FindServidor: TFindServidor
  Left = 483
  Top = 314
  Caption = 'Especificar Nombre Del Servidor Remoto'
  ClientHeight = 213
  ClientWidth = 397
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 177
    Width = 397
    BevelOuter = bvNone
    inherited OK: TBitBtn
      Left = 229
      Default = True
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 314
    end
  end
  object DCOMgb: TGroupBox
    Left = 0
    Top = 8
    Width = 397
    Height = 58
    Align = alTop
    Caption = '                         '
    TabOrder = 1
    object RemoteServerLBL: TLabel
      Left = 8
      Top = 24
      Width = 82
      Height = 13
      Alignment = taRightJustify
      Caption = '&Servidor Remoto:'
      FocusControl = RemoteServer
    end
    object ServidorSeek: TSpeedButton
      Left = 344
      Top = 13
      Width = 41
      Height = 35
      Hint = 'Buscar Servidor Remoto'
      Glyph.Data = {
        06020000424D0602000000000000760000002800000028000000140000000100
        0400000000009001000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF003333333FFFFF
        FFF00000333333333333333777773333333BFBFBFBF0FFF03333333333333337
        FFF73333333FFFFFFF000000333333333333337777773333333BFBFBF0FBFBFB
        333333333FFFF733FFFF3333333F00000FF000003333333377777FF777773333
        333B0FFF0000FFF0333333337FFF7777FFF73333333F00000FF000003333333F
        777773F777773333330BFBFBF0FBFBFB3333337FF333373FFFFF33333010FFFF
        FF00000033333777FF3333777777333330170BFBFBF0FFF0333337777FF33337
        FFF73333301170FFFFF0000033333777778F3337777333330711190BFBFBFBFB
        333377777378F3333333333308819990FFFFFFFF3333733733378F3333333330
        88FF9999033333333337333333FF7333333333088FFFF0003333333333733333
        F777333333333088FFF003333333333337333337733333333333088FFF033333
        333333337F33337333333333333308FFF09333333333333378F3373333333333
        333330FF0933333333333333378F733333333333333333003333333333333333
        33773333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = ServidorSeekClick
    end
    object RemoteServer: TEdit
      Left = 93
      Top = 20
      Width = 244
      Height = 21
      TabOrder = 1
    end
    object UsarDCOM: TRadioButton
      Left = 12
      Top = -2
      Width = 81
      Height = 17
      Caption = 'Usar DCOM'
      TabOrder = 0
      OnClick = UsarDCOMClick
    end
  end
  object PanelRelleno: TPanel
    Left = 0
    Top = 0
    Width = 397
    Height = 8
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
  end
  object HTTPgb: TGroupBox
    Left = 0
    Top = 78
    Width = 397
    Height = 99
    Align = alClient
    Caption = '                        '
    TabOrder = 3
    object SitioWebLBL: TLabel
      Left = 38
      Top = 24
      Width = 51
      Height = 13
      Alignment = taRightJustify
      Caption = 'Sitio WEB:'
      FocusControl = SitioWeb
    end
    object UserNameLBL: TLabel
      Left = 50
      Top = 47
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'Usuario:'
      FocusControl = UserName
    end
    object PasswordLBL: TLabel
      Left = 32
      Top = 69
      Width = 57
      Height = 13
      Alignment = taRightJustify
      Caption = 'Contrase'#241'a:'
      FocusControl = Password
    end
    object UsarHTTP: TRadioButton
      Left = 12
      Top = -2
      Width = 81
      Height = 17
      Caption = 'Usar Internet '
      TabOrder = 0
      OnClick = UsarHTTPClick
    end
    object SitioWeb: TEdit
      Left = 93
      Top = 20
      Width = 292
      Height = 21
      TabOrder = 1
    end
    object UserName: TEdit
      Left = 93
      Top = 43
      Width = 210
      Height = 21
      TabOrder = 2
    end
    object Password: TEdit
      Left = 93
      Top = 66
      Width = 210
      Height = 21
      PasswordChar = '*'
      TabOrder = 3
    end
  end
  object PanelEnMedio: TPanel
    Left = 0
    Top = 66
    Width = 397
    Height = 12
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
  end
end
