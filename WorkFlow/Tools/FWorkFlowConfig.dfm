inherited WorkFlowCFGValues: TWorkFlowCFGValues
  Caption = 'Configurar Motores Del WorkFlow'
  ClientHeight = 354
  ClientWidth = 468
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 318
    Width = 468
    inherited OK: TBitBtn
      Left = 300
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 385
    end
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 468
    Height = 318
    ActivePage = XSL
    Align = alClient
    TabOrder = 1
    object Direcciones: TTabSheet
      Caption = 'Direcciones'
      object MotorGB: TGroupBox
        Left = 0
        Top = 0
        Width = 460
        Height = 75
        Align = alTop
        Caption = ' Administrador Del WorkFlow '
        TabOrder = 0
        object AdministradorLBL: TLabel
          Left = 89
          Top = 23
          Width = 40
          Height = 13
          Hint = 'Nombre Del Administrador Del WorkFlow'
          Alignment = taRightJustify
          Caption = 'N&ombre:'
          FocusControl = Administrador
          ParentShowHint = False
          ShowHint = True
        end
        object AdministradorEMailLBL: TLabel
          Left = 30
          Top = 44
          Width = 99
          Height = 13
          Hint = 'Direcci'#243'n De Correo Electr'#243'nico Del Administrador De WorkFlow'
          Alignment = taRightJustify
          Caption = 'D&irecci'#243'n De Correo:'
          FocusControl = AdministradorEMail
          ParentShowHint = False
          ShowHint = True
        end
        object AdministradorEMail: TEdit
          Left = 130
          Top = 40
          Width = 281
          Height = 21
          Hint = 'Direcci'#243'n De Correo Electr'#243'nico Del Administrador De WorkFlow'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Text = 'gandrade@tress.com.mx'
        end
        object Administrador: TEdit
          Left = 130
          Top = 19
          Width = 281
          Height = 21
          Hint = 'Nombre Del Administrador Del WorkFlow'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Text = 'german'
        end
      end
      object RemitenteGB: TGroupBox
        Left = 0
        Top = 75
        Width = 460
        Height = 76
        Align = alTop
        Caption = ' Remitente De Los Correos Enviados Por WorkFlow '
        TabOrder = 1
        object RemitenteLBL: TLabel
          Left = 89
          Top = 25
          Width = 40
          Height = 13
          Hint = 'Nombre Del Remitente De Los Correos De WorkFlow'
          Alignment = taRightJustify
          Caption = '&Nombre:'
          FocusControl = Remitente
          ParentShowHint = False
          ShowHint = True
        end
        object RemitenteDireccionLBL: TLabel
          Left = 30
          Top = 46
          Width = 99
          Height = 13
          Hint = 'Direcci'#243'n De Correo Del Remitente De Los Correos De WorkFlow'
          Alignment = taRightJustify
          Caption = '&Direcci'#243'n De Correo:'
          FocusControl = RemitenteDireccion
          ParentShowHint = False
          ShowHint = True
        end
        object RemitenteDireccion: TEdit
          Left = 130
          Top = 42
          Width = 281
          Height = 21
          Hint = 'Direcci'#243'n De Correo Del Remitente De Los Correos De WorkFlow'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Text = 'gandrade@tress.com.mx'
        end
        object Remitente: TEdit
          Left = 130
          Top = 21
          Width = 281
          Height = 21
          Hint = 'Nombre Del Remitente De Los Correos De WorkFlow'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Text = 'german'
        end
      end
      object ConfirmacionGB: TGroupBox
        Left = 0
        Top = 151
        Width = 460
        Height = 139
        Align = alClient
        Caption = ' Acuse De Recibo '
        TabOrder = 2
        object ConfirmacionNombreLBL: TLabel
          Left = 89
          Top = 23
          Width = 40
          Height = 13
          Hint = 
            'Nombre De Quien Recibe Los Acuses De Recibo De Los Correos De Wo' +
            'rkFlow'
          Alignment = taRightJustify
          Caption = '&Nombre:'
          FocusControl = ConfirmacionNombre
          ParentShowHint = False
          ShowHint = True
        end
        object ConfirmacionEMailLBL: TLabel
          Left = 30
          Top = 44
          Width = 99
          Height = 13
          Hint = 
            'Direcci'#243'n De Correo De Quien Recibe Los Acuses De Recibo De Los ' +
            'Correos De WorkFlow'
          Alignment = taRightJustify
          Caption = '&Direcci'#243'n De Correo:'
          FocusControl = ConfirmacionEMail
          ParentShowHint = False
          ShowHint = True
        end
        object ConfirmacionEMail: TEdit
          Left = 130
          Top = 40
          Width = 281
          Height = 21
          Hint = 
            'Direcci'#243'n De Correo De Quien Recibe Los Acuses De Recibo De Los ' +
            'Correos De WorkFlow'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Text = 'gandrade@tress.com.mx'
        end
        object ConfirmacionNombre: TEdit
          Left = 130
          Top = 19
          Width = 281
          Height = 21
          Hint = 
            'Nombre De Quien Recibe Los Acuses De Recibo De Los Correos De Wo' +
            'rkFlow'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Text = 'german'
        end
      end
    end
    object EMailServer: TTabSheet
      Caption = 'Servidor de Correos'
      ImageIndex = 1
      DesignSize = (
        460
        290)
      object ServidorLBL: TLabel
        Left = 22
        Top = 17
        Width = 98
        Height = 13
        Hint = 'Direcci'#243'n de IP '#243' Nombre Del Sevidor De Correos'
        Alignment = taRightJustify
        Caption = '&Servidor De Correos:'
        FocusControl = Servidor
        ParentShowHint = False
        ShowHint = True
      end
      object UsuarioLBL: TLabel
        Left = 81
        Top = 39
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = '&Usuario:'
        FocusControl = Usuario
      end
      object PasswordLBL: TLabel
        Left = 63
        Top = 62
        Width = 57
        Height = 13
        Hint = 'Clave De Acceso Para Entrar Al Servidor De Correos'
        Alignment = taRightJustify
        Caption = '&Contrase'#241'a:'
        FocusControl = Passsword
        ParentShowHint = False
        ShowHint = True
      end
      object PuertoLBL: TLabel
        Left = 86
        Top = 85
        Width = 34
        Height = 13
        Hint = 'Puerto TCP Para Que Usa El Servidor De Correos'
        Alignment = taRightJustify
        Caption = '&Puerto:'
        FocusControl = Puerto
        ParentShowHint = False
        ShowHint = True
      end
      object TimeOutLBL: TLabel
        Left = 77
        Top = 108
        Width = 43
        Height = 13
        Hint = 'Tiempo De Espera Para Enviar Los Correos'
        Alignment = taRightJustify
        Caption = '&TimeOut:'
        FocusControl = TimeOut
        ParentShowHint = False
        ShowHint = True
      end
      object Servidor: TEdit
        Left = 122
        Top = 14
        Width = 281
        Height = 21
        Hint = 'Direcci'#243'n de IP '#243' Nombre Del Sevidor De Correos'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Text = 'MAILSERVER'
      end
      object Usuario: TEdit
        Left = 122
        Top = 35
        Width = 281
        Height = 21
        TabOrder = 1
      end
      object Passsword: TEdit
        Left = 122
        Top = 58
        Width = 281
        Height = 21
        Hint = 'Clave De Acceso Para Entrar Al Servidor De Correos'
        ParentShowHint = False
        PasswordChar = '*'
        ShowHint = True
        TabOrder = 2
      end
      object Puerto: TZetaNumero
        Left = 122
        Top = 81
        Width = 54
        Height = 21
        Hint = 'Puerto TCP Para Que Usa El Servidor De Correos'
        Mascara = mnDias
        TabOrder = 3
        Text = '0'
      end
      object TimeOut: TZetaNumero
        Left = 122
        Top = 104
        Width = 54
        Height = 21
        Hint = 'Tiempo De Espera Para Enviar Los Correos'
        Mascara = mnDias
        TabOrder = 4
        Text = '0'
      end
      object ProbarEMail: TBitBtn
        Left = 199
        Top = 141
        Width = 90
        Height = 44
        Hint = 'Enviar Un eMail de Prueba'
        Anchors = [akTop, akRight]
        Caption = '&Probar'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = ProbarEMailClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333333333333333333FFFFFFFFFFFFFFF000000000000
          000077777777777777770FFFFFFFFFFFFFF07F3333FFF33333370FFFF777FFFF
          FFF07F333777333333370FFFFFFFFFFFFFF07F3333FFFFFF33370FFFF777777F
          FFF07F33377777733FF70FFFFFFFFFFF99907F3FFF33333377770F777FFFFFFF
          9CA07F77733333337F370FFFFFFFFFFF9A907FFFFFFFFFFF7FF7000000000000
          0000777777777777777733333333333333333333333333333333333333333333
          3333333333333333333333333333333333333333333333333333333333333333
          3333333333333333333333333333333333333333333333333333}
        Layout = blGlyphTop
        NumGlyphs = 2
      end
    end
    object XSL: TTabSheet
      Caption = 'XSL Defaults'
      ImageIndex = 2
      object XSLTareaGB: TGroupBox
        Left = 0
        Top = 36
        Width = 460
        Height = 50
        Align = alTop
        Caption = ' Tareas '
        TabOrder = 1
        object XSLTareaLBL: TLabel
          Left = 48
          Top = 20
          Width = 70
          Height = 13
          Hint = 'Archivo XSL Para Generar Correos De Notificaciones'
          Alignment = taRightJustify
          Caption = 'Notificaciones:'
          FocusControl = XSLTarea
          ParentShowHint = False
          ShowHint = True
        end
        object XSLTareaSB: TSpeedButton
          Left = 400
          Top = 17
          Width = 23
          Height = 22
          Hint = 'Buscar Archivo XSL Para Generar Correos De Notificaciones'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          OnClick = XSLTareaSBClick
        end
        object XSLTareaTestSB: TSpeedButton
          Left = 426
          Top = 17
          Width = 23
          Height = 22
          Hint = 'Probar Generaci'#243'n De Correo Para Notificar Una Tarea'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            555555555555555555555555555555555555555555FF55555555555559055555
            55555555577FF5555555555599905555555555557777F5555555555599905555
            555555557777FF5555555559999905555555555777777F555555559999990555
            5555557777777FF5555557990599905555555777757777F55555790555599055
            55557775555777FF5555555555599905555555555557777F5555555555559905
            555555555555777FF5555555555559905555555555555777FF55555555555579
            05555555555555777FF5555555555557905555555555555777FF555555555555
            5990555555555555577755555555555555555555555555555555}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = XSLTareaTestSBClick
        end
        object XSLTarea: TEdit
          Left = 120
          Top = 17
          Width = 273
          Height = 21
          Hint = 'Archivo XSL Para Generar Correos De Notificaciones'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
      end
      object XSLProcesosGB: TGroupBox
        Left = 0
        Top = 86
        Width = 460
        Height = 101
        Align = alTop
        Caption = ' Procesos  '
        TabOrder = 2
        object XSLProcesoActivarLBL: TLabel
          Left = 65
          Top = 20
          Width = 53
          Height = 13
          Hint = 'Archivo XSL Para Avisar Al Activar Un Proceso'
          Alignment = taRightJustify
          Caption = 'Activaci'#243'n:'
          FocusControl = XSLProcesoActivar
          ParentShowHint = False
          ShowHint = True
        end
        object XSLProcesoActivarSB: TSpeedButton
          Left = 400
          Top = 17
          Width = 23
          Height = 22
          Hint = 'Buscar Archivo XSL Para Avisar Al Activar Un Proceso'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          OnClick = XSLProcesoActivarSBClick
        end
        object XSLProcesoTerminarLBL: TLabel
          Left = 57
          Top = 46
          Width = 61
          Height = 13
          Hint = 'Archivo XSL Para Avisar Al Terminar Un Proceso'
          Alignment = taRightJustify
          Caption = 'Terminaci'#243'n:'
          FocusControl = XSLProcesoTerminar
          ParentShowHint = False
          ShowHint = True
        end
        object XSLProcesoTerminarSB: TSpeedButton
          Left = 400
          Top = 43
          Width = 23
          Height = 22
          Hint = 'Buscar Archivo XSL Para Avisar Al Terminar Un Proceso'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = XSLProcesoTerminarSBClick
        end
        object XSLProcesoCancelarLBL: TLabel
          Left = 56
          Top = 72
          Width = 62
          Height = 13
          Hint = 'Archivo XSL Para Avisar Al Cancelar Un Proceso'
          Alignment = taRightJustify
          Caption = 'Cancelaci'#243'n:'
          FocusControl = XSLProcesoCancelar
          ParentShowHint = False
          ShowHint = True
        end
        object XSLProcesoCancelarSB: TSpeedButton
          Left = 400
          Top = 69
          Width = 23
          Height = 22
          Hint = 'Buscar Archivo XSL Para Avisar Al Cancelar Un Proceso'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = XSLProcesoCancelarSBClick
        end
        object XSLProcesoActivarTest: TSpeedButton
          Left = 426
          Top = 17
          Width = 23
          Height = 22
          Hint = 'Probar Generaci'#243'n De Correo Para Notificar Una Tarea'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            555555555555555555555555555555555555555555FF55555555555559055555
            55555555577FF5555555555599905555555555557777F5555555555599905555
            555555557777FF5555555559999905555555555777777F555555559999990555
            5555557777777FF5555557990599905555555777757777F55555790555599055
            55557775555777FF5555555555599905555555555557777F5555555555559905
            555555555555777FF5555555555559905555555555555777FF55555555555579
            05555555555555777FF5555555555557905555555555555777FF555555555555
            5990555555555555577755555555555555555555555555555555}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = XSLProcesoActivarTestClick
        end
        object XSLProcesoTerminarTest: TSpeedButton
          Left = 426
          Top = 43
          Width = 23
          Height = 22
          Hint = 'Probar Generaci'#243'n De Correo Para Notificar Una Tarea'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            555555555555555555555555555555555555555555FF55555555555559055555
            55555555577FF5555555555599905555555555557777F5555555555599905555
            555555557777FF5555555559999905555555555777777F555555559999990555
            5555557777777FF5555557990599905555555777757777F55555790555599055
            55557775555777FF5555555555599905555555555557777F5555555555559905
            555555555555777FF5555555555559905555555555555777FF55555555555579
            05555555555555777FF5555555555557905555555555555777FF555555555555
            5990555555555555577755555555555555555555555555555555}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = XSLProcesoTerminarTestClick
        end
        object XSLProcesoCancelarTest: TSpeedButton
          Left = 426
          Top = 69
          Width = 23
          Height = 22
          Hint = 'Probar Generaci'#243'n De Correo Para Notificar Una Tarea'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            555555555555555555555555555555555555555555FF55555555555559055555
            55555555577FF5555555555599905555555555557777F5555555555599905555
            555555557777FF5555555559999905555555555777777F555555559999990555
            5555557777777FF5555557990599905555555777757777F55555790555599055
            55557775555777FF5555555555599905555555555557777F5555555555559905
            555555555555777FF5555555555559905555555555555777FF55555555555579
            05555555555555777FF5555555555557905555555555555777FF555555555555
            5990555555555555577755555555555555555555555555555555}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = XSLProcesoCancelarTestClick
        end
        object XSLProcesoActivar: TEdit
          Left = 120
          Top = 17
          Width = 273
          Height = 21
          Hint = 'Archivo XSL Para Avisar Al Activar Un Proceso'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
        object XSLProcesoTerminar: TEdit
          Left = 120
          Top = 43
          Width = 273
          Height = 21
          Hint = 'Archivo XSL Para Avisar Al Terminar Un Proceso'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
        object XSLProcesoCancelar: TEdit
          Left = 120
          Top = 69
          Width = 273
          Height = 21
          Hint = 'Archivo XSL Para Avisar Al Cancelar Un Proceso'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
      end
      object XSLPasosProcesoGB: TGroupBox
        Left = 0
        Top = 187
        Width = 460
        Height = 103
        Align = alClient
        Caption = ' Pasos De Procesos '
        TabOrder = 3
        object XSLPasoProcesoActivarLBL: TLabel
          Left = 65
          Top = 20
          Width = 53
          Height = 13
          Hint = 'Archivo XSL Para Avisar Al Activar Un Paso De Proceso'
          Alignment = taRightJustify
          Caption = 'Activaci'#243'n:'
          FocusControl = XSLPasoProcesoActivar
          ParentShowHint = False
          ShowHint = True
        end
        object XSLPasoProcesoActivarSB: TSpeedButton
          Left = 400
          Top = 17
          Width = 23
          Height = 22
          Hint = 'Buscar Archivo XSL Para Avisar Al Activar Un Paso De Proceso'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          OnClick = XSLPasoProcesoActivarSBClick
        end
        object XSLPasoProcesoTerminarLBL: TLabel
          Left = 57
          Top = 46
          Width = 61
          Height = 13
          Hint = 'Archivo XSL Para Avisar Al Terminar Un Paso De Proceso'
          Alignment = taRightJustify
          Caption = 'Terminaci'#243'n:'
          FocusControl = XSLPasoProcesoTerminar
          ParentShowHint = False
          ShowHint = True
        end
        object XSLPasoProcesoTerminarSB: TSpeedButton
          Left = 400
          Top = 43
          Width = 23
          Height = 22
          Hint = 'Buscar Archivo XSL Para Avisar Al Terminar Un Paso De Proceso'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = XSLPasoProcesoTerminarSBClick
        end
        object XSLPasoProcesoCancelarLBL: TLabel
          Left = 56
          Top = 72
          Width = 62
          Height = 13
          Hint = 'Archivo XSL Para Avisar Al Cancelar Un Paso De Proceso'
          Alignment = taRightJustify
          Caption = 'Cancelaci'#243'n:'
          FocusControl = XSLPasoProcesoCancelar
          ParentShowHint = False
          ShowHint = True
        end
        object XSLPasoProcesoActivarTest: TSpeedButton
          Left = 426
          Top = 17
          Width = 23
          Height = 22
          Hint = 'Probar Generaci'#243'n De Correo Para Notificar Una Tarea'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            555555555555555555555555555555555555555555FF55555555555559055555
            55555555577FF5555555555599905555555555557777F5555555555599905555
            555555557777FF5555555559999905555555555777777F555555559999990555
            5555557777777FF5555557990599905555555777757777F55555790555599055
            55557775555777FF5555555555599905555555555557777F5555555555559905
            555555555555777FF5555555555559905555555555555777FF55555555555579
            05555555555555777FF5555555555557905555555555555777FF555555555555
            5990555555555555577755555555555555555555555555555555}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = XSLPasoProcesoActivarTestClick
        end
        object XSLPasoProcesoTerminarTest: TSpeedButton
          Left = 426
          Top = 43
          Width = 23
          Height = 22
          Hint = 'Probar Generaci'#243'n De Correo Para Notificar Una Tarea'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            555555555555555555555555555555555555555555FF55555555555559055555
            55555555577FF5555555555599905555555555557777F5555555555599905555
            555555557777FF5555555559999905555555555777777F555555559999990555
            5555557777777FF5555557990599905555555777757777F55555790555599055
            55557775555777FF5555555555599905555555555557777F5555555555559905
            555555555555777FF5555555555559905555555555555777FF55555555555579
            05555555555555777FF5555555555557905555555555555777FF555555555555
            5990555555555555577755555555555555555555555555555555}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = XSLPasoProcesoTerminarTestClick
        end
        object XSLPasoProcesoCancelarTest: TSpeedButton
          Left = 426
          Top = 69
          Width = 23
          Height = 22
          Hint = 'Probar Generaci'#243'n De Correo Para Notificar Una Tarea'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            555555555555555555555555555555555555555555FF55555555555559055555
            55555555577FF5555555555599905555555555557777F5555555555599905555
            555555557777FF5555555559999905555555555777777F555555559999990555
            5555557777777FF5555557990599905555555777757777F55555790555599055
            55557775555777FF5555555555599905555555555557777F5555555555559905
            555555555555777FF5555555555559905555555555555777FF55555555555579
            05555555555555777FF5555555555557905555555555555777FF555555555555
            5990555555555555577755555555555555555555555555555555}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = XSLPasoProcesoCancelarTestClick
        end
        object XSLPasoProcesoCancelarSB: TSpeedButton
          Left = 400
          Top = 69
          Width = 23
          Height = 22
          Hint = 'Buscar Archivo XSL Para Avisar Al Cancelar Un Paso De Proceso'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = XSLPasoProcesoCancelarSBClick
        end
        object XSLPasoProcesoActivar: TEdit
          Left = 120
          Top = 17
          Width = 273
          Height = 21
          Hint = 'Archivo XSL Para Avisar Al Activar Un Paso De Proceso'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
        object XSLPasoProcesoTerminar: TEdit
          Left = 120
          Top = 43
          Width = 273
          Height = 21
          Hint = 'Archivo XSL Para Avisar Al Terminar Un Paso De Proceso'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
        object XSLPasoProcesoCancelar: TEdit
          Left = 120
          Top = 69
          Width = 273
          Height = 21
          Hint = 'Archivo XSL Para Avisar Al Cancelar Un Paso De Proceso'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
      end
      object PanelDefaults: TPanel
        Left = 0
        Top = 0
        Width = 460
        Height = 36
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object XSLPathLBL: TLabel
          Left = 30
          Top = 10
          Width = 86
          Height = 13
          Alignment = taRightJustify
          Caption = '&Directorio de XSL:'
          FocusControl = XSLPath
        end
        object XSLPathSeek: TSpeedButton
          Left = 424
          Top = 8
          Width = 23
          Height = 22
          Hint = 'Buscar Directorio Default de Archivos XSL'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = XSLPathSeekClick
        end
        object XSLPath: TEdit
          Tag = 61
          Left = 119
          Top = 7
          Width = 302
          Height = 21
          TabOrder = 0
        end
      end
    end
  end
  object OpenXSLDialog: TOpenDialog
    Filter = 'Archivos  XSL (*.xsl)|*.xsl|Todos (*.*)|*.*'
    FilterIndex = 0
    Title = 'Especifique El Archivo XSL Deseado'
    Left = 28
    Top = 114
  end
end
