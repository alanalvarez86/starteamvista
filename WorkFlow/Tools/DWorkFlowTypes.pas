unit DWorkFlowTypes;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs,
     ComObj, DB, Registry,
     ZetaRegistryServer,
     ZetaCommonLists,
     DZetaServerProvider;

{$define USE_REGISTRY}
{$undef USE_REGISTRY}
const
     ACCION_FIRMA = 'FIRMA';
     ACCION_GRABA_TRESS = 'GRABA3';
     ACCION_CALL_WEBSERVICE = 'CALLWS';
     MSG_ACTIVAR = 'Activar';
     MSG_ACEPTAR = 'Aceptar';
     MSG_RECHAZAR = 'Rechazar';
     MSG_DELEGAR = 'Delegar';
     MSG_SUSPENDER = 'Suspender';
     MSG_CANCELAR = 'Cancelar';
     MSG_REINICIAR = 'Reiniciar';
     MSG_NOTIFI = 'Notificar';
     MSG_AUTORIZAR = 'Autorizar';
     NULL_GUID = '{00000000-0000-0000-0000-000000000000}';
     K_MOTORCITO = 0;
     K_CIEN_PORCIENTO = 100;
     K_CINCUENTA_PORCIENTO = 50;
     K_CERO_PORCIENTO = 0;
     K_ANCHO_ACCESOS = 255;
type
  TFeedBack = procedure( const Loops, Actions: Integer ) of object;
  TUpdateStatus = procedure( const sTexto: String ) of object;
  TWFIdentifier = TGUID;
  TUsuario = Integer;
  TNotas = String;
  TWFFecha = TDateTime;
  TWFData = String;
  TLlave = {$ifdef USE_REGISTRY}String{$else}Integer{$endif};
  { Herencia Temporal De Provider }
  TdmZetaWorkFlowProvider = class(TdmZetaServerProvider)
  private
    { Private Declarations }
    FGlobalUpdate: TZetaCursor;
    FGlobalInsert: TZetaCursor;
    function GlobalWriteExecute(Cursor: TZetaCursor; const iCodigo: Integer; const sFormula: String): Boolean;
  public
    { Public Declarations }
    function CampoAsGUID( Campo: TField ): TWFIdentifier;
    procedure Start;
    procedure Stop;
    procedure GlobalWrite(const iCodigo: Integer; const sFormula: String);
    procedure GlobalWriteInteger(const iCodigo, iValor: Integer);
    procedure GlobalWriteString(const iCodigo: Integer; const sValor: String);
    procedure GlobalWriteStart;
    procedure GlobalWriteStop;
  end;
  {$ifdef USE_REGISTRY}
  TWFStorage = class( TRegIniFile )
  private
    { Private Declarations }
    FCanWrite: Boolean;
  protected
    { Protected Declarations }
  public
    { Public Declarations }
    property CanWrite: Boolean read FCanWrite;
    procedure Init(const lReadWrite: Boolean );
  end;
  {$endif}
  TRegistryBase = class( TObject )
  private
    { Private Declarations }
    FReadWrite: Boolean;
    FZetaProvider: TdmZetaWorkFlowProvider;
    {$ifdef USE_REGISTRY}
    FStorage: TWFStorage;
    property Storage: TWFStorage read FStorage;
    {$endif}
  protected
    { Protected Declarations }
    function ReadString( const Key: TLlave; const sDefault: String ): String;
    function ReadInteger( const Key: TLlave; const iDefault: Integer ): Integer;
    procedure WriteInteger( const Key: TLlave; const iValue: Integer );
    procedure WriteString( const Key: TLlave; const sValue: String );
  public
    { Public Declarations }
    constructor Create( const lReadWrite: Boolean = TRUE );
    property oZetaProvider: TdmZetaWorkFlowProvider read FZetaProvider write FZetaProvider;
    property ReadWrite: Boolean read FReadWrite;
    destructor Destroy; override;
  end;
  TWFRegistry = class( TRegistryBase )
  private
    { Private Declarations }
    function GetEMailPassWord: String;
    function GetEMailPort: Integer;
    function GetEMailReceiptRecipientAddress: String;
    function GetEMailReceiptRecipientName: String;
    function GetEMailReplyToName: String;
    function GetEMailReplyToAddress: String;
    function GetEMailServer: String;
    function GetEMailTimeOut: Integer;
    function GetEMailUser: String;
    function GetDirectorioXSL: String;
    function GetPasoModeloXSLActivo: String;
    function GetPasoModeloXSLCancelado: String;
    function GetPasoModeloXSLTerminado: String;
    function GetPasoModeloXSLSuspendido: String;
    function GetPasoModeloXSLModificar: String;
    function GetPasoModeloXSLDespuesIniciar: String;
    function GetPasoModeloXSLAntesLimite: String;
    function GetPasoModeloXSLDespuesLimite: String;
    function GetModeloXSLActivo: String;
    function GetModeloXSLCancelado: String;
    function GetModeloXSLTerminado: String;
    function GetModeloXSLSuspendido: String;
    function GetTareaXSL: String;
    function GetWorkFlowEMailAddress: String;
    function GetWorkFlowEMailName: String;
    procedure SetEMailPassWord(const Value: String);
    procedure SetEMailPort(const Value: integer);
    procedure SetEMailReceiptRecipientAddress(const Value: String);
    procedure SetEMailReceiptRecipientName(const Value: String);
    procedure SetEMailReplyToName(const Value: String);
    procedure SetEMailReplyToAddress(const Value: String);
    procedure SetEMailServer(const Value: String);
    procedure SetEMailTimeOut(const Value: Integer);
    procedure SetEMailUser(const Value: String);
    procedure SetDirectorioXSL(const Value: String);
    procedure SetPasoModeloXSLActivo(const Value: String);
    procedure SetPasoModeloXSLCancelado(const Value: String);
    procedure SetPasoModeloXSLTerminado(const Value: String);
    procedure SetPasoModeloXSLSuspendido(const Value: String);
    procedure SetPasoModeloXSLModificar(const Value: String);
    procedure SetPasoModeloXSLDespuesIniciar(const Value: String);
    procedure SetPasoModeloXSLAntesLimite(const Value: String);
    procedure SetPasoModeloXSLDespuesLimite(const Value: String);
    procedure SetModeloXSLActivo(const Value: String);
    procedure SetModeloXSLCancelado(const Value: String);
    procedure SetModeloXSLTerminado(const Value: String);
    procedure SetModeloXSLSuspendido(const Value: String);
    procedure SetTareaXSL(const Value: String);
    procedure SetWorkFlowEMailAddress(const Value: String);
    procedure SetWorkFlowEMailName(const Value: String);
  public
    { Public Declarations }
    property EMailServer: String read GetEMailServer write SetEMailServer;
    property EMailPassword: String read GetEMailPassWord write SetEMailPassWord;
    property EMailPort: Integer read GetEMailPort write SetEMailPort;
    property EMailReplyToName: String read GetEMailReplyToName write SetEMailReplyToName;
    property EMailReplyToAddress: String read GetEMailReplyToAddress write SetEMailReplyToAddress;
    property EMailReceiptRecipientAddress: String read GetEMailReceiptRecipientAddress write SetEMailReceiptRecipientAddress;
    property EMailReceiptRecipientName: String read GetEMailReceiptRecipientName write SetEMailReceiptRecipientName;
    property EMailTimeOut: Integer read GetEMailTimeOut write SetEMailTimeOut;
    property EMailUser: String read GetEMailUser write SetEMailUser;
    property DirectorioXSL: String read GetDirectorioXSL write SetDirectorioXSL;
    property ModeloXSLActivo: String read GetModeloXSLActivo write SetModeloXSLActivo;
    property ModeloXSLCancelado: String read GetModeloXSLCancelado write SetModeloXSLCancelado;
    property ModeloXSLTerminado: String read GetModeloXSLTerminado write SetModeloXSLTerminado;
    property ModeloXSLSuspendido: String read GetModeloXSLSuspendido write SetModeloXSLSuspendido;
    property PasoModeloXSLActivo: String read GetPasoModeloXSLActivo write SetPasoModeloXSLActivo;
    property PasoModeloXSLCancelado: String read GetPasoModeloXSLCancelado write SetPasoModeloXSLCancelado;
    property PasoModeloXSLTerminado: String read GetPasoModeloXSLTerminado write SetPasoModeloXSLTerminado;
    property PasoModeloXSLSuspendido: String read GetPasoModeloXSLSuspendido write SetPasoModeloXSLSuspendido;
    property PasoModeloXSLModificar: String read GetPasoModeloXSLModificar write SetPasoModeloXSLModificar;
    property PasoModeloXSLDespuesIniciar: String read GetPasoModeloXSLDespuesIniciar write SetPasoModeloXSLDespuesIniciar;
    property PasoModeloXSLAntesLimite: String read GetPasoModeloXSLAntesLimite write SetPasoModeloXSLAntesLimite;
    property PasoModeloXSLDespuesLimite: String read GetPasoModeloXSLDespuesLimite write SetPasoModeloXSLDespuesLimite;
    property TareaXSL: String read GetTareaXSL write SetTareaXSL;
    property WorkFlowEMailAddress: String read GetWorkFlowEMailAddress write SetWorkFlowEMailAddress;
    property WorkFlowEMailName: String read GetWorkFlowEMailName write SetWorkFlowEMailName;
  end;

  TWorkFlowRegistryServer = class( TZetaRegistryServer )
  private
    { Private declarations }
    function GetWorkFlowServerDependency: string;
    procedure SetWorkFlowServerDependency(const Value: string);
  public
    { Public declarations }
    property WorkFlowServerDependency: string read GetWorkFlowServerDependency write SetWorkFlowServerDependency;
  end;

implementation


{$ifdef USE_REGISTRY}
const
     K_WORKFLOW_MANAGER_NAME = 'WorkFlowEMail';
     K_WORKFLOW_MANAGER_EMAIL = 'WorkFlowEMailName';

     K_WORKFLOW_EMAIL_SERVER = 'Host';
     K_WORKFLOW_EMAIL_PORT = 'Port';
     K_WORKFLOW_EMAIL_USER = 'UserId';
     K_WORKFLOW_EMAIL_PASSWORD = 'PassWord';
     K_WORKFLOW_EMAIL_TIMEOUT = 'TimeOut';

     K_WORKFLOW_SENDER_NAME = 'ReplyToName';
     K_WORKFLOW_SENDER_EMAIL = 'ReplyToAddress';

     K_WORKFLOW_RECEIPT_NAME = 'ReceiptRecipientName';
     K_WORKFLOW_RECEIPT_EMAIL = 'ReceiptRecipientAddress';

     K_WORKFLOW_XSL_TAREA = 'TareaXSL';

     K_WORKFLOW_XSL_PROCESO_ACTIVAR = 'ModeloXSLActivo';
     K_WORKFLOW_XSL_PROCESO_CANCELAR = 'ModeloXSLCancelado';
     K_WORKFLOW_XSL_PROCESO_TERMINAR = 'ModeloXSLTerminado';
     K_WORKFLOW_XSL_PROCESO_SUSPENDER = 'ModeloXSLSuspendido';

     K_WORKFLOW_XSL_PASO_ACTIVAR = 'PasoModeloXSLActivo';
     K_WORKFLOW_XSL_PASO_CANCELAR = 'PasoModeloXSLCancelado';
     K_WORKFLOW_XSL_PASO_TERMINAR = 'PasoModeloXSLTerminado';
     K_WORKFLOW_XSL_PASO_SUSPENDER = 'PasoModeloXSLSuspendido';

     K_WORKFLOW_XSL_PASO_MODIFICAR = 'PasoModeloXSLModificar';
     K_WORKFLOW_XSL_PASO_DESPUES_INICIAR = 'PasoModeloXSLDespuesIniciar';
     K_WORKFLOW_XSL_PASO_ANTES_LIMITE = 'PasoModeloXSLAntesLimite';
     K_WORKFLOW_XSL_PASO_DESPUES_LIMITE = 'PasoModeloXSLDespuesLimite';



{$else}
uses ZetaCommonTools,
     ZetaCommonClasses,
     ZGlobalTress;

const
     WORKFLOW_SERVER_DEPENDENCY = 'WorkFlowServerDependency';

{$endif}

{ ******** DZetaWorkFlowProvider ******** }

procedure TdmZetaWorkFlowProvider.Start;
begin
     Activate;
     EmpresaActiva := Comparte;
     InitGlobales;
end;

procedure TdmZetaWorkFlowProvider.Stop;
begin
     GlobalWriteStop;     
     Deactivate;
end;

procedure TdmZetaWorkFlowProvider.GlobalWriteStart;
begin
end;

function TdmZetaWorkFlowProvider.GlobalWriteExecute( Cursor: TZetaCursor; const iCodigo: Integer; const sFormula: String): Boolean;
const
     K_ANCHO_FORMULA = 255;
begin
     ParamAsInteger( Cursor, 'Codigo', iCodigo );
     ParamAsVarChar( Cursor, 'Formula', sFormula, K_ANCHO_FORMULA );
     { Para que el resultado sea TRUE, tiene que existir el Registro }
     { Si no existe, entonces se tiene que agregar; }
     Result := ( Ejecuta( Cursor ) = 1 );
end;

procedure TdmZetaWorkFlowProvider.GlobalWrite( const iCodigo: Integer; const sFormula: String );
const
     K_QUERY_UPDATE = 'update GLOBAL set GL_FORMULA = :Formula where ( GL_CODIGO = :Codigo )';
     K_QUERY_INSERT = 'insert into GLOBAL ( GL_CODIGO, GL_FORMULA ) values ( :Codigo, :Formula )';
begin
     if not Assigned( FGlobalUpdate ) then
     begin
          FGlobalUpdate := CreateQuery( K_QUERY_UPDATE );
     end;
     EmpiezaTransaccion;
     try
        if not GlobalWriteExecute( FGlobalUpdate, iCodigo, sFormula ) then
        begin
             { Si no existe el registro, lo Agrega }
             { Como no es muy comun que suceda esto, no se crea el Query }
             { sino hasta que se necesita }
             if not Assigned( FGlobalInsert ) then
                FGlobalInsert := CreateQuery( K_QUERY_INSERT );
             GlobalWriteExecute( FGlobalInsert, iCodigo, sFormula );
        end;
        TerminaTransaccion( True );
     except
           on Error: Exception do
           begin
                TerminaTransaccion( False );
                raise;
           end;
     end;
end;

procedure TdmZetaWorkFlowProvider.GlobalWriteString( const iCodigo: Integer; const sValor: String );
begin
     GlobalWrite( iCodigo, sValor );
end;

procedure TdmZetaWorkFlowProvider.GlobalWriteInteger( const iCodigo, iValor: Integer );
begin
     GlobalWrite( iCodigo, IntToStr( iValor ) );
end;

procedure TdmZetaWorkFlowProvider.GlobalWriteStop;
begin
     if Assigned( FGlobalUpdate ) then
        FreeAndNil( FGlobalUpdate );
     if Assigned( FGlobalInsert ) then
        FreeAndNil( FGlobalInsert );
end;

{$WARNINGS OFF}
function TdmZetaWorkFlowProvider.CampoAsGUID( Campo: TField ): TWFIdentifier;
var
   ptrID: ^TWFIdentifier;
begin
     with Campo do
     begin
          if ( Campo is TGuidField ) then
          begin
               Result := TGuidField( Campo ).AsGuid;
          end
          else
              if IsNull then
                 Result := StringToGUID( NULL_GUID )
              else
              begin
                   GetMem( ptrID, DataSize );
                   try
                      if GetData( ptrID ) then
                         Result := TWFIdentifier( ptrID^ )
                      else
                          Result := StringToGUID( NULL_GUID );
                   finally
                          FreeMem( ptrID, DataSize);
                   end;
              end;
     end;
end;
{$WARNINGS ON}

{ ******** TWFStorage ******** }

{$ifdef USE_REGISTRY}
procedure TWFStorage.Init(const lReadWrite: Boolean );
const
     H_KEY_WORKFLOW = 'Software\Grupo Tress\WorkFlow\';
begin
     RootKey := HKEY_LOCAL_MACHINE;
     if lReadWrite then
        FCanWrite := OpenKey( H_KEY_WORKFLOW, TRUE )
     else
         FCanWrite := False;
     if not FCanWrite then
        OpenKeyReadOnly( H_KEY_WORKFLOW );
end;
{$endif}

{ ******** TRegistryBase ******** }

constructor TRegistryBase.Create(const lReadWrite: Boolean );
begin
     {$ifdef USE_REGISTRY}
     FStorage := TWFStorage.Create;
     Storage.Init( lReadWrite );
     {$endif}
     FReadWrite := lReadWrite;
end;

destructor TRegistryBase.Destroy;
begin
     {$ifdef USE_REGISTRY}
     FreeAndNil( FStorage );
     {$endif}
     inherited Destroy;
end;

function TRegistryBase.ReadString( const Key: TLlave; const sDefault: String ): String;
begin
     {$ifdef USE_REGISTRY}
     Result := Storage.ReadString( '', Key, sDefault );
     {$else}
     Result := oZetaProvider.GetGlobalString( Key );
     if ZetaCommonTools.StrVacio( Result ) then
        Result := sDefault;
     {$endif}
end;

function TRegistryBase.ReadInteger( const Key: TLlave; const iDefault: Integer ): Integer;
begin
     {$ifdef USE_REGISTRY}
     Result := Storage.ReadInteger( '', Key, iDefault );
     {$else}
     Result := oZetaProvider.GetGlobalInteger( Key );
     if ( Result <= 0 ) then
        Result := iDefault;
     {$endif}
end;

procedure TRegistryBase.WriteString( const Key: TLlave; const sValue: String );
begin
     if ReadWrite then
     begin
          {$ifdef USE_REGISTRY}
          Storage.WriteString( '', Key, sValue );
          {$else}
          with oZetaProvider do
          begin
               GlobalWriteStart;
               GlobalWriteString( Key, sValue );
          end;
          {$endif}
     end;
end;

procedure TRegistryBase.WriteInteger( const Key: TLlave; const iValue: Integer );
begin
     if ReadWrite then
     begin
          {$ifdef USE_REGISTRY}
          Storage.WriteInteger( '', Key, iValue );
          {$else}
          with oZetaProvider do
          begin
               GlobalWriteStart;
               GlobalWriteInteger( Key, iValue );
          end;
          {$endif}
     end;
end;

{ ******** TWFRegistry ******** }

function TWFRegistry.GetEMailPassWord: String;
begin
     Result := ReadString( K_WORKFLOW_EMAIL_PASSWORD, '' );
end;

function TWFRegistry.GetEMailPort: Integer;
begin
     Result := ReadInteger( K_WORKFLOW_EMAIL_PORT, 25 );
end;

function TWFRegistry.GetEMailReceiptRecipientAddress: String;
begin
     Result := ReadString( K_WORKFLOW_RECEIPT_EMAIL, '' );
end;

function TWFRegistry.GetEMailReceiptRecipientName: String;
begin
     Result := ReadString( K_WORKFLOW_RECEIPT_NAME, '' );
end;

function TWFRegistry.GetEMailReplyToName: String;
begin
     Result := ReadString( K_WORKFLOW_SENDER_NAME, '' );
end;

function TWFRegistry.GetEMailReplyToAddress: String;
begin
     Result := ReadString( K_WORKFLOW_SENDER_EMAIL, '' );
end;

function TWFRegistry.GetEMailServer: String;
begin
     Result := ReadString( K_WORKFLOW_EMAIL_SERVER, '' );
end;

function TWFRegistry.GetEMailTimeOut: Integer;
begin
     Result := ReadInteger( K_WORKFLOW_EMAIL_TIMEOUT, 5 );
end;

function TWFRegistry.GetEMailUser: String;
begin
     Result := ReadString( K_WORKFLOW_EMAIL_USER, '' );
end;

function TWFRegistry.GetWorkFlowEMailAddress: String;
begin
     Result := ReadString( K_WORKFLOW_MANAGER_EMAIL, 'workflowadmin@yourdomain.com'  );
end;

function TWFRegistry.GetWorkFlowEMailName: String;
begin
     Result := ReadString( K_WORKFLOW_MANAGER_NAME, 'Administrador De WorkFlow' );
end;

function TWFRegistry.GetDirectorioXSL: String;
begin
     Result := ReadString( K_WORKFLOW_XSL_PATH, '' );
end;

function TWFRegistry.GetPasoModeloXSLActivo: String;
begin
     Result := ReadString( K_WORKFLOW_XSL_PASO_ACTIVAR, '' );
end;

function TWFRegistry.GetPasoModeloXSLCancelado: String;
begin
     Result := ReadString( K_WORKFLOW_XSL_PASO_CANCELAR, '' );
end;

function TWFRegistry.GetPasoModeloXSLTerminado: String;
begin
     Result := ReadString( K_WORKFLOW_XSL_PASO_TERMINAR, '' );
end;

function TWFRegistry.GetPasoModeloXSLSuspendido: String;
begin
     Result := ReadString( K_WORKFLOW_XSL_PASO_SUSPENDER, '' );
end;

function TWFRegistry.GetPasoModeloXSLModificar: String;
begin
     Result := ReadString( K_WORKFLOW_XSL_PASO_MODIFICAR, '' );
end;

function TWFRegistry.GetPasoModeloXSLDespuesIniciar: String;
begin
     Result := ReadString( K_WORKFLOW_XSL_PASO_DESPUES_INICIAR, '' );
end;

function TWFRegistry.GetPasoModeloXSLAntesLimite: String;
begin
     Result := ReadString( K_WORKFLOW_XSL_PASO_ANTES_LIMITE, '' );
end;

function TWFRegistry.GetPasoModeloXSLDespuesLimite: String;
begin
     Result := ReadString( K_WORKFLOW_XSL_PASO_DESPUES_LIMITE, '' );
end;

function TWFRegistry.GetModeloXSLActivo: String;
begin
     Result := ReadString( K_WORKFLOW_XSL_PROCESO_ACTIVAR, '' );
end;

function TWFRegistry.GetModeloXSLCancelado: String;
begin
     Result := ReadString( K_WORKFLOW_XSL_PROCESO_CANCELAR, '' );
end;

function TWFRegistry.GetModeloXSLTerminado: String;
begin
     Result := ReadString( K_WORKFLOW_XSL_PROCESO_TERMINAR, '' );
end;

function TWFRegistry.GetModeloXSLSuspendido: String;
begin
     Result := ReadString( K_WORKFLOW_XSL_PROCESO_SUSPENDER, '' );
end;

function TWFRegistry.GetTareaXSL: String;
begin
     Result := ReadString( K_WORKFLOW_XSL_TAREA, '' );
end;

procedure TWFRegistry.SetEMailPassWord(const Value: String);
begin
     WriteString( K_WORKFLOW_EMAIL_PASSWORD, Value );
end;

procedure TWFRegistry.SetEMailPort(const Value: Integer);
begin
     WriteInteger( K_WORKFLOW_EMAIL_PORT, Value );
end;

procedure TWFRegistry.SetEMailReceiptRecipientAddress(const Value: String);
begin
     WriteString( K_WORKFLOW_RECEIPT_EMAIL, Value );
end;

procedure TWFRegistry.SetEMailReceiptRecipientName(const Value: String);
begin
     WriteString( K_WORKFLOW_RECEIPT_NAME, Value );
end;

procedure TWFRegistry.SetEMailReplyToAddress(const Value: String);
begin
     WriteString( K_WORKFLOW_SENDER_EMAIL, Value );
end;

procedure TWFRegistry.SetEMailReplyToName(const Value: String);
begin
     WriteString( K_WORKFLOW_SENDER_NAME, Value );
end;

procedure TWFRegistry.SetEMailServer(const Value: String);
begin
     WriteString( K_WORKFLOW_EMAIL_SERVER, Value );
end;

procedure TWFRegistry.SetEMailTimeOut(const Value: Integer);
begin
     WriteInteger( K_WORKFLOW_EMAIL_TIMEOUT, Value );
end;

procedure TWFRegistry.SetEMailUser(const Value: String);
begin
     WriteString( K_WORKFLOW_EMAIL_USER, Value );
end;

procedure TWFRegistry.SetWorkFlowEMailAddress(const Value: String);
begin
     WriteString( K_WORKFLOW_MANAGER_eMAIL, Value );
end;

procedure TWFRegistry.SetWorkFlowEMailName(const Value: String);
begin
     WriteString( K_WORKFLOW_MANAGER_NAME, Value );
end;

procedure TWFRegistry.SetDirectorioXSL(const Value: String);
begin
     WriteString( K_WORKFLOW_XSL_PATH, Value );
end;

procedure TWFRegistry.SetPasoModeloXSLActivo(const Value: String);
begin
     WriteString( K_WORKFLOW_XSL_PASO_ACTIVAR, Value );
end;

procedure TWFRegistry.SetPasoModeloXSLCancelado(const Value: String);
begin
     WriteString( K_WORKFLOW_XSL_PASO_CANCELAR, Value );
end;

procedure TWFRegistry.SetPasoModeloXSLTerminado(const Value: String);
begin
     WriteString( K_WORKFLOW_XSL_PASO_TERMINAR, Value );
end;

procedure TWFRegistry.SetPasoModeloXSLSuspendido(const Value: String);
begin
     WriteString( K_WORKFLOW_XSL_PASO_SUSPENDER, Value );
end;

procedure TWFRegistry.SetPasoModeloXSLModificar(const Value: String);
begin
     WriteString( K_WORKFLOW_XSL_PASO_MODIFICAR, Value );
end;

procedure TWFRegistry.SetPasoModeloXSLDespuesIniciar(const Value: String);
begin
     WriteString( K_WORKFLOW_XSL_PASO_DESPUES_INICIAR, Value );
end;

procedure TWFRegistry.SetPasoModeloXSLAntesLimite(const Value: String);
begin
     WriteString( K_WORKFLOW_XSL_PASO_ANTES_LIMITE, Value );
end;

procedure TWFRegistry.SetPasoModeloXSLDespuesLimite(const Value: String);
begin
     WriteString( K_WORKFLOW_XSL_PASO_DESPUES_LIMITE, Value );
end;

procedure TWFRegistry.SetModeloXSLActivo(const Value: String);
begin
     WriteString( K_WORKFLOW_XSL_PROCESO_ACTIVAR, Value );
end;

procedure TWFRegistry.SetModeloXSLCancelado(const Value: String);
begin
     WriteString( K_WORKFLOW_XSL_PROCESO_CANCELAR, Value );
end;

procedure TWFRegistry.SetModeloXSLTerminado(const Value: String);
begin
     WriteString( K_WORKFLOW_XSL_PROCESO_TERMINAR, Value );
end;

procedure TWFRegistry.SetModeloXSLSuspendido(const Value: String);
begin
     WriteString( K_WORKFLOW_XSL_PROCESO_SUSPENDER, Value );
end;

procedure TWFRegistry.SetTareaXSL(const Value: String);
begin
     WriteString( K_WORKFLOW_XSL_TAREA, Value );
end;

{ ***** TWorkFlowRegistryServer **** }

function TWorkFlowRegistryServer.GetWorkFlowServerDependency: string;
begin
     Result := Registry.ReadString( '', WORKFLOW_SERVER_DEPENDENCY, MSSQL_DEFAULT_DEPENDENCY );
end;

procedure TWorkFlowRegistryServer.SetWorkFlowServerDependency( const Value: string);
begin
     Registry.WriteString( '', WORKFLOW_SERVER_DEPENDENCY, Value );
end;

end.
