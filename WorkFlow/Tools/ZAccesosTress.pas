unit ZAccesosTress;

interface

const
     K_IMAGENINDEX                      = 0;
   {
   El C�digo de grupo que no tiene restricciones
   }
   D_GRUPO_SIN_RESTRICCION            = 1;
   {
   Longitud Del Arreglo de Derechos De Acceso
   Nota: Si agregas un derecho, aumenta K_MAX_DERECHOS
   }
   K_MAX_DERECHOS                     = 47;

   {******** Historial **********}

   D_HISTORIAL                        = 1;
   D_HISTORIAL_PROCESOS               = 2;
   D_HISTORIAL_TAREAS                 = 3;
   D_HISTORIAL_CORREOS                = 4;

   {******** Consultas **********}

   D_CONSULTAS                        = 5;
   D_CONS_REPORTES                    = 6;
   D_REPORTES_CONFIDENCIALES          = 9;
   D_REPORTES_WORKFLOW                = 29;
   D_REPORTES_PORTAL                  = 32;
   D_REPORTES_SISTEMA                 = 30;
   D_REPORTES_FAVORITOS               = 31;
   D_CONS_SQL                         = 7;
   D_CONS_BITACORA                    = 8;
   D_CONS_EVENT_VIEWER                = 45;

   {******** Cat�logos **********}

   D_CATALOGOS                        = 10;
   D_CAT_MODELOS                      = 11;
   D_CAT_PASOS_MODELOS                = 12;
   D_CAT_USUARIOS                     = 13;
   D_CAT_ROLES                        = 14;
   D_CAT_ACCIONES                     = 15;
   D_CATALOGOS_GENERALES              = 16;
   D_CAT_GRALES_CONDICIONES           = 17;
   D_CATALOGOS_CONFIGURACION          = 18;
   D_CAT_CONFI_DICCIONARIO            = 19;
   D_CAT_CONFI_GLOBALES               = 20;
   D_CAT_CONEXIONES                   = 46;

   { ******** Portal ********** }

   D_PORTAL_INFO                      = 33;
   D_PORTAL_INFO_NOTICIAS             = 34;
   D_PORTAL_INFO_EVENTOS              = 35;
   D_PORTAL_INFO_DOCUMENTOS           = 36;
   D_PORTAL_INFO_REPORTES             = 37;
   D_PORTAL_INFO_SUGERENCIAS          = 38;
   
   D_PORTAL_CATALOGOS                 = 39;
   D_PORTAL_CAT_NOTICIAS_TIPO         = 40;
   D_PORTAL_CAT_EVENTOS_TIPO          = 41;
   D_PORTAL_CAT_DOCUMENTOS_TIPO       = 42;
   D_PORTAL_CAT_PAGINAS_DEFAULT       = 43;
   D_PORTAL_CAT_GLOBALES              = 44;

   {******** Sistema **********}
   {
   Estos DEBEN de ser iguales en todas las aplicaciones
   que usan COMPARTE - ver D:\3Win_20\Tools\zAccesosTress.pas
   }
   D_SISTEMA                          = 21;
   D_SIST_DATOS                       = 22;
   D_SIST_DATOS_EMPRESAS              = 23;
   D_SIST_DATOS_USUARIOS              = 24;
   D_SIST_DATOS_GRUPOS                = 25;
   D_SIST_DATOS_IMPRESORAS            = 26;
   D_SIST_PRENDE_USUARIOS             = 27;
   D_SIST_APAGA_USUARIOS              = 28;
   D_SIST_BASE_DATOS                  = 47;

implementation

end.
