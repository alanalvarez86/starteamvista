unit ZetaPortalClasses;

interface

const
     K_MIN_COLUMNAS = 1;
     K_MAX_COLUMNAS = 3;
     K_MIN_PAGINAS = 1;
     K_MAX_PAGINAS = 5;
     K_NULL_PAGINA = -1;
     K_REPORTAL_MAX_COLUMNAS = 10;
     K_ERROR_OFFSET = 1000;
type
  eContenido = ( ecReporte, ecGrafica, ecTitular, ecNoticia, ecEvento );
  eTipoTabla =( eTNoticia,
                eTEvento,
                eTDocumen,
                eNoticia,
                eEvento,
                eDocument,
                eBuzonSug,
                eUsuarios,
                ePagina,
                eContiene,
                eReportal,
                eRepCols,
                eRepData,
                eAccion,
                eRol,
                eModeloR,
                eModeloW,
                ePasosModelo,
                ekCarruseles,
                ekMarcosInformacion,
                ekEstilosBotones,
                ekCarruselesInfo,
                ekBotones,
                eNotificacion,
                eConexion,
                eAccesoBoton,
                eConfiUsuarios,
                eKImpresiones,
                eEncuestas, {28}
                eOpcEncuesta, {29}
                eVotosEncuesta ); {30}

function EncryptData( const sValue: String; const iKeyValue: Word ): String;
function DecryptData( const sValue: String; const iKeyValue: Word ): String;

implementation

{ ****** Encrypt a String ****** }

const
     cKey1 = 52845;
     cKey2 = 22719;

function EncryptData( const sValue: String; const iKeyValue: Word ): String;
var
   i: Byte;
   iKey: Word;
begin
     iKey := iKeyValue;
     SetLength( Result, Length( sValue ) );
     for i := 1 to Length( sValue ) do
     begin
          Result[ i ] := Char( Byte( sValue[ i ] ) xor ( iKey shr 8 ) );
          iKey := ( Byte( Result[ i ] ) + iKey ) * cKey1 + cKey2;
    end;
end;

function DecryptData( const sValue: String; const iKeyValue: Word ): String;
var
   i: byte;
   iKey: Word;
begin
     iKey := iKeyValue;
     SetLength( Result, Length( sValue ) );
     for i := 1 to Length( sValue ) do
     begin
          Result[ i ] := Char( Byte( sValue[ i ] ) xor ( iKey shr 8 ) );
          iKey := ( Byte( sValue[ i ] ) + iKey ) * cKey1 + cKey2;
     end;
end;

end.
