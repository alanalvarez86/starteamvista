unit ZArbolTress;

interface

uses ComCtrls, Dialogs, SysUtils, DB, ZArbolTools;

function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
procedure CreaArbolDefault( oArbolMgr: TArbolMgr );
procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String );

implementation

uses ZetaDespConsulta,
     ZetaCommonTools,
     ZAccesosMgr,
     ZAccesosTress,
     ZGlobalTress,
     DGlobal,
     DSistema,
     DCliente;

const
     K_HISTORIAL                           = 1;
     K_HISTORIAL_PROCESOS                  = 2;
     K_HISTORIAL_TAREAS                    = 3;
     K_HISTORIAL_CORREOS                   = 4;
     K_CATALOGOS                           = 5;
     K_CAT_MODELOS                         = 6;
     K_CAT_PASOS_MODELOS                   = 7;
     K_CAT_USUARIOS                        = 8;
     K_CAT_ROLES                           = 9;
     K_CAT_ACCIONES                        = 10;
     K_CATALOGOS_GENERALES                 = 11;
     K_CATALOGOS_GENERALES_CONDICIONES     = 12;
     K_CATALOGOS_CONFIGURACION             = 13;
     K_CATALOGOS_CONFIGURACION_GLOBALES    = 14;
     K_CATALOGOS_CONFIGURACION_DICCIONARIO = 15;
     K_CONSULTAS                           = 16;
     K_CONSULTAS_REPORTES                  = 17;
     K_CONSULTAS_BITACORA                  = 18;
     K_CONSULTAS_PROCESOS                  = 19;
     K_CONSULTAS_SQL                       = 20;
     K_SISTEMA                             = 21;
     K_SISTEMA_DATOS                       = 22;
     {
     K_SISTEMA_DATOS_EMPRESAS              = 14;
     }
     K_SISTEMA_DATOS_GRUPOS                = 23;
     K_SISTEMA_DATOS_ROLES                 = 24;
     K_SISTEMA_DATOS_USUARIOS              = 25;
     K_SISTEMA_DATOS_IMPRESORAS            = 26;
     K_EVENT_VIEWER                        = 27;
     K_CAT_CONEXIONES                      = 28;



function Forma( const sCaption: String; const eTipo: eFormaConsulta ): TNodoInfo;
begin
     Result.EsForma := TRUE;
     Result.Caption := sCaption;
     Result.TipoForma := eTipo;
end;

function Folder( const sCaption: String; const iDerechos: Integer ): TNodoInfo;
begin
     Result.EsForma := FALSE;
     Result.Caption := sCaption;
     Result.IndexDerechos := iDerechos;
end;

function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
begin
     with GLobal do
     begin
          case iNodo of
               K_HISTORIAL                           : Result := Folder( 'Historial', D_HISTORIAL );
               K_HISTORIAL_PROCESOS                  : Result := Forma( 'Procesos', efcHistProcesos );
               K_HISTORIAL_TAREAS                    : Result := Forma( 'Tareas', efcHistTareas );
               K_HISTORIAL_CORREOS                   : Result := Forma( 'Correos', efcHistCorreos );
               K_CATALOGOS                           : Result := Folder( 'Catálogos', D_CATALOGOS );
               K_CAT_MODELOS                         : Result := Forma( 'Modelos', efcCatModelo );
               K_CAT_PASOS_MODELOS                   : Result := Forma( 'Pasos de Modelo', efcCatPasosModelo );
               K_CAT_USUARIOS                        : Result := Forma( 'Usuarios', efcCatUsuarios );
               K_CAT_ROLES                           : Result := Forma( 'Roles', efcCatRoles );
               K_CAT_ACCIONES                        : Result := Forma( 'Acciones', efcCatAcciones );
               K_CAT_CONEXIONES                      : Result := Forma( 'Conexiones', efcConexiones);
               K_CATALOGOS_GENERALES                 : Result := Folder( 'Generales', D_CATALOGOS_GENERALES );
               K_CATALOGOS_GENERALES_CONDICIONES     : Result := Forma( 'Condiciones', efcCatCondiciones );
               K_CATALOGOS_CONFIGURACION             : Result := Folder( 'Configuración', D_CATALOGOS_CONFIGURACION );
               K_CATALOGOS_CONFIGURACION_GLOBALES    : Result := Forma( 'Globales de Empresa', efcSistGlobales );
               K_CATALOGOS_CONFIGURACION_DICCIONARIO : Result := Forma( 'Diccionario de Datos', efcDiccion );
               K_CONSULTAS                           : Result := Folder( 'Consultas', D_CONSULTAS );
               K_CONSULTAS_REPORTES                  : Result := Forma( 'Reportes', efcReportes );
               K_CONSULTAS_BITACORA                  : Result := Forma( 'Bitácora', efcSistBitacora );
               K_CONSULTAS_PROCESOS                  : Result := Forma( 'Procesos', efcSistProcesos );
               K_CONSULTAS_SQL                       : Result := Forma( 'SQL', efcQueryGral );
//               K_EVENT_VIEWER                        : Result := Forma( 'Eventos de Windows', efcEventViewer );
               K_SISTEMA                             : Result := Folder( 'Sistema', D_SISTEMA );
               K_SISTEMA_DATOS                       : Result := Folder( 'Datos', D_SIST_DATOS );
               {
               K_SISTEMA_DATOS_EMPRESAS              : Result := Forma( 'Empresas', efcSistEmpresas );
               }
               K_SISTEMA_DATOS_GRUPOS                : Result := Forma( 'Grupos de Usuarios', efcSistGrupos );
               K_SISTEMA_DATOS_USUARIOS              : Result := Forma( 'Usuarios', efcSistUsuarios );
               K_SISTEMA_DATOS_IMPRESORAS            : Result := Forma( 'Impresoras', efcImpresoras );
          else
              Result := Folder( '', 0 );
          end;
     end;
end;

procedure CreaArbolDefault( oArbolMgr: TArbolMgr );

function NodoNivel( const iNivel, iNodo: Integer ): Boolean;
begin
     Result := oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo );
end;

begin
     if NodoNivel( 0, K_HISTORIAL ) then
     begin
          NodoNivel( 1, K_HISTORIAL_PROCESOS );
          NodoNivel( 1, K_HISTORIAL_TAREAS );
          NodoNivel( 1, K_HISTORIAL_CORREOS );
     end;
     if NodoNivel( 0, K_CONSULTAS ) then
     begin
          NodoNivel( 1, K_CONSULTAS_REPORTES );
          NodoNivel( 1, K_CONSULTAS_SQL );
          NodoNivel( 1, K_CONSULTAS_BITACORA );
          NodoNivel( 1, K_CONSULTAS_PROCESOS );
//          NodoNivel( 1, K_EVENT_VIEWER );
     end;
     if NodoNivel( 0, K_CATALOGOS ) then
     begin
          NodoNivel( 1, K_CAT_MODELOS );
          NodoNivel( 1, K_CAT_PASOS_MODELOS );
          NodoNivel( 1, K_CAT_USUARIOS );
          NodoNivel( 1, K_CAT_ROLES );
          NodoNivel( 1, K_CAT_ACCIONES );
          NodoNivel( 1, K_CAT_CONEXIONES);
          {if NodoNivel( 1, K_CATALOGOS_GENERALES ) then
          begin;
                NodoNivel( 2, K_CATALOGOS_GENERALES_CONDICIONES );
          end;}
          if NodoNivel( 1, K_CATALOGOS_CONFIGURACION ) then
          begin
               NodoNivel( 2, K_CATALOGOS_CONFIGURACION_GLOBALES );
               {$ifdef FALSE}
               NodoNivel( 2, K_CATALOGOS_CONFIGURACION_DICCIONARIO );
               {$endif}
          end;
     end;
     if NodoNivel( 0, K_SISTEMA ) then
     begin
          if NodoNivel( 1, K_SISTEMA_DATOS ) then
          begin
               {
               NodoNivel( 2, K_SISTEMA_DATOS_EMPRESAS );
               }
               NodoNivel( 2, K_SISTEMA_DATOS_GRUPOS );
               {
               NodoNivel( 2, K_SISTEMA_DATOS_ROLES );
               }
               NodoNivel( 2, K_SISTEMA_DATOS_USUARIOS );
               NodoNivel( 2, K_SISTEMA_DATOS_IMPRESORAS );
          end;
     end;
end;

procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String );
begin
     with oArbolMgr do
     begin
          {
          NumDefault := K_ARCH_REQUISICION ;
          }
          CreaArbolDefault( oArbolMgr );
          Arbol.FullExpand;
     end;
end;

end.
