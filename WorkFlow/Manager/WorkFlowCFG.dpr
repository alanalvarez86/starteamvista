program WorkFlowCFG;

uses
  MidasLib,
  Forms,
  ZetaClientTools,
  ZetaSplash in '..\..\Tools\ZetaSplash.pas' {SplashScreen},
  ZBaseShell in '..\..\Tools\ZBaseShell.pas' {BaseShell},
  ZBaseArbolShell in '..\..\Tools\ZBaseArbolShell.pas' {BaseArbolShell},
  FTressShell in 'FTressShell.pas' {TressShell},
  DBasicoCliente in '..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DBaseGlobal in '..\..\DataModules\dBaseGlobal.pas',
  DBaseDiccionario in '..\..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DBaseReportes in '..\..\DataModules\DBaseReportes.pas' {dmBaseReportes: TDataModule},
  DBaseSistema in '..\..\DataModules\DBaseSistema.pas' {dmBaseSistema: TDataModule},
  FSistBaseEditEmpresas in '..\..\Sistema\FSistBaseEditEmpresas.pas',
  ZBaseEditAccesos in '..\..\Tools\ZBaseEditAccesos.pas',
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  ZBaseConsulta in '..\..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZBaseEdicion in '..\..\Tools\ZBaseEdicion.pas' {BaseEdicion},
  ZBaseGlobal in '..\..\Tools\ZBaseGlobal.pas' {BaseGlobal},
  DSistema in '..\DataModules\DSistema.pas' {dmSistema: TDataModule},
  DWorkflow in '..\DataModules\DWorkflow.pas' {dmWorkflow: TDataModule};

{$R *.RES}
{$R WINDOWSXP.RES}

{procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;}

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  {SplashScreen := TSplashScreen.Create( Application );
  with SplashScreen do
  begin
       Show;
       Update;
  end;}
  Application.Title := 'Configurador WorkFlow';
  Application.HelpFile := 'Workflow.chm';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login( False ) then
       begin
            //CierraSplash;
            Show;
            Update;
            BeforeRun;
            Application.Run;
       end
       else
       begin
            //CierraSplash;
            Free;
       end;
  end;
end.

