inherited WizCopiarModelo: TWizCopiarModelo
  Left = 277
  Top = 163
  Caption = 'Copiar modelo'
  ClientHeight = 273
  ClientWidth = 455
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    Top = 237
    Width = 455
    BeforeMove = WizardBeforeMove
    inherited Anterior: TZetaWizardButton
      Left = 7
    end
    inherited Siguiente: TZetaWizardButton
      Left = 86
    end
    inherited Salir: TZetaWizardButton
      Left = 361
    end
    inherited Ejecutar: TZetaWizardButton
      Left = 278
    end
  end
  inherited PageControl: TPageControl
    Width = 455
    Height = 237
    inherited Parametros: TTabSheet
      object WM_CODIGOlbl: TLabel
        Left = 56
        Top = 72
        Width = 70
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modelo &origen:'
        FocusControl = Modelo_Origen
      end
      object Label1: TLabel
        Left = 20
        Top = 104
        Width = 106
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo modelo &nuevo:'
        FocusControl = Modelo_Destino
      end
      object Label2: TLabel
        Left = 16
        Top = 136
        Width = 110
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nom&bre modelo nuevo:'
        FocusControl = ModeloNombre
      end
      object Modelo_Origen: TZetaKeyLookup
        Left = 131
        Top = 70
        Width = 300
        Height = 21
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
      end
      object Modelo_Destino: TZetaEdit
        Left = 131
        Top = 102
        Width = 121
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 6
        TabOrder = 1
      end
      object ModeloNombre: TZetaEdit
        Left = 131
        Top = 134
        Width = 299
        Height = 21
        TabOrder = 2
      end
    end
    inherited Ejecucion: TTabSheet
      inherited Advertencia: TMemo
        Width = 447
        Lines.Strings = (
          ''
          ''
          ''
          ''
          
            'Se copiaran los pasos y notificaciones del modelo origen al mode' +
            'lo '
          'destino.')
      end
      inherited ProgressPanel: TPanel
        Top = 160
        Width = 447
      end
    end
  end
end
