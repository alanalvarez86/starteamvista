unit FWizCopiarModelo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseWizard, ComCtrls, StdCtrls, Buttons, ZetaWizard, ExtCtrls,
  ZetaKeyLookup, Mask, ZetaNumero, ZetaKeyCombo, ZetaDBTextBox, ZetaFecha,
  DBCtrls, ZetaCommonClasses, ZetaEdit;

type
  TWizCopiarModelo = class(TBaseWizard)
    WM_CODIGOlbl: TLabel;
    Modelo_Origen: TZetaKeyLookup;
    Label1: TLabel;
    Modelo_Destino: TZetaEdit;
    Label2: TLabel;
    ModeloNombre: TZetaEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);

  private
    { Private declarations }
  protected
    procedure CargaParametros; override;
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizCopiarModelo: TWizCopiarModelo;

implementation
uses
    DCliente,
    DWorkFlow,
    ZetaDialogo,
    ZetaCommonLists,
    ZBaseSelectGrid,
    ZetaCommonTools;


{$R *.dfm}

procedure TWizCopiarModelo.FormCreate(Sender: TObject);
begin
     inherited;
     Modelo_Origen.LookupDataSet := dmWorkFLow.cdsModelos;
     ParametrosControl := Modelo_Origen;
end;

procedure TWizCopiarModelo.FormShow(Sender: TObject);
begin
     inherited;
     dmWorkFlow.cdsModelos.Conectar;
     //Modelo_Origen.LLave := dmWorkFlow.Modelo;
end;

procedure TWizCopiarModelo.CargaParametros;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddString('ModeloOrigen', Modelo_Origen.Llave );
          AddString('ModeloDestino', Modelo_Destino.Text );
          AddString('ModeloNombre', ModeloNombre.Text);
     end;
end;

function TWizCopiarModelo.EjecutarWizard: Boolean;
begin
     Result := dmWorkFlow.CopiarModelo( ParameterList );
end;

procedure TWizCopiarModelo.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     with Wizard do
     begin
          if Adelante and EsPaginaActual( Parametros ) then
          begin
               if ( StrVacio( Modelo_Origen.Descripcion ) ) then
               begin
                    zError( Caption, '� El modelo origen no puede quedar vac�o !', 0 );
                    CanMove := False;
                    ActiveControl := Modelo_Origen;
               end
               else if ( StrVacio( Modelo_Destino.Text ) ) then
               begin
                    zError( Caption, '� El c�digo del modelo destino no puede quedar vac�o !', 0 );
                    CanMove := False;
                    ActiveControl := Modelo_Destino;
               end
               else if ( StrVacio( ModeloNombre.Text ) ) then
               begin
                    zError( Caption, '� El nombre del modelo destino no puede quedar vac�o !', 0 );
                    CanMove := False;
                    ActiveControl := ModeloNombre;
               end;
          end;
     end;
end;

end.
