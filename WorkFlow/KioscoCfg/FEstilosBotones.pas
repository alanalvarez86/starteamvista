unit FEstilosBotones;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid;

type
  TTOEstilosBotones = class(TBaseConsulta)
    ZetaDBGrid: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  TOEstilosBotones: TTOEstilosBotones;

implementation

uses dPortal,
     ZetaBuscador;

{$R *.DFM}

{ TCarruseles }

procedure TTOEstilosBotones.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := 0;
     IndexDerechos := 0;//D_PORTAL_INFO_DOCUMENTOS;
end;

procedure TTOEstilosBotones.Connect;
begin
     with dmPortal do
     begin
          cdsEstilosBotones.Conectar;
          DataSource.DataSet:= cdsEstilosBotones;
     end;
end;

procedure TTOEstilosBotones.Agregar;
begin
     dmPortal.cdsEstilosBotones.Agregar;
end;

procedure TTOEstilosBotones.Borrar;
begin
     dmPortal.cdsEstilosBotones.Borrar;
end;

procedure TTOEstilosBotones.Modificar;
begin
     dmPortal.cdsEstilosBotones.Modificar;
     ZetaDBGrid.Repaint;
end;

procedure TTOEstilosBotones.Refresh;
begin
     dmPortal.cdsEstilosBotones.Refrescar;
end;

procedure TTOEstilosBotones.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Folio', 'Estilos de botones', 'KE_CODIGO', dmPortal.cdsEstilosBotones );
end;

end.
