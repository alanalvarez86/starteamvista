inherited TOEstilosBotones: TTOEstilosBotones
  Left = 214
  Top = 190
  Caption = 'Estilos de botones'
  ClientWidth = 675
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 675
    inherited ValorActivo2: TPanel
      Width = 416
    end
  end
  object ZetaDBGrid: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 675
    Height = 252
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'KE_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 53
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'KE_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 134
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'KE_COLOR'
        Title.Caption = 'Color'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'KE_F_NAME'
        Title.Caption = 'Font'
        Width = 99
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'KE_F_SIZE'
        Title.Caption = 'Tama'#241'o'
        Width = 47
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'KE_F_COLR'
        Title.Caption = 'Color del font'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'KE_F_BOLD'
        Title.Caption = 'Negrita'
        Width = 41
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'KE_F_ITAL'
        Title.Caption = 'It'#225'lico'
        Width = 34
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'KE_F_SUBR'
        Title.Caption = 'Subrayado'
        Width = 56
        Visible = True
      end>
  end
end
