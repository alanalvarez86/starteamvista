inherited TOCarruseles: TTOCarruseles
  Left = 253
  Top = 211
  Caption = 'TOCarruseles'
  ClientWidth = 675
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 675
    inherited ValorActivo2: TPanel
      Width = 416
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 675
    Height = 252
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'KW_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'KW_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 134
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'KW_SCR_NOM'
        Title.Caption = 'Marco'
        Width = 156
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'KW_TIM_DAT'
        Title.Alignment = taRightJustify
        Title.Caption = 'Tiempo l'#237'mite'
        Width = 175
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'KW_GET_NIP'
        Title.Caption = 'NIP'
        Width = 120
        Visible = True
      end>
  end
end
