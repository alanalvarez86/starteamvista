inherited TOEditBotones: TTOEditBotones
  Left = 494
  Top = 169
  Caption = 'Bot'#243'n'
  ClientHeight = 367
  ClientWidth = 394
  PixelsPerInch = 96
  TextHeight = 13
  object TB_CODIGOlbl: TLabel [0]
    Left = 47
    Top = 46
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
    Enabled = False
    FocusControl = KS_CODIGO
  end
  object Label1: TLabel [1]
    Left = 53
    Top = 94
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto:'
    FocusControl = KB_TEXTO
  end
  object Label5: TLabel [2]
    Left = 53
    Top = 118
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Icono:'
    FocusControl = KB_BITMAP
  end
  object Label6: TLabel [3]
    Left = 47
    Top = 166
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Acci'#243'n:'
    FocusControl = KB_BITMAP
  end
  object lbKB_URL: TLabel [4]
    Left = 24
    Top = 190
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'P'#225'gina web:'
    FocusControl = KB_URL
  end
  object lbKB_SCREEN: TLabel [5]
    Left = 50
    Top = 214
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Marco:'
  end
  object LBKB_REPORTE: TLabel [6]
    Left = 42
    Top = 238
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Reporte:'
    FocusControl = KB_REPORTE
  end
  object lbSepara: TLabel [7]
    Left = 26
    Top = 311
    Width = 57
    Height = 13
    Alignment = taRightJustify
    Caption = 'Separaci'#243'n:'
  end
  object Label2: TLabel [8]
    Left = 40
    Top = 286
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Posici'#243'n:'
  end
  object Label3: TLabel [9]
    Left = 53
    Top = 262
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Altura:'
  end
  object Label4: TLabel [10]
    Left = 51
    Top = 70
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Orden:'
    Enabled = False
    FocusControl = KB_ORDEN
  end
  object Label7: TLabel [11]
    Left = 9
    Top = 142
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Posici'#243'n '#237'cono:'
    FocusControl = KB_BITMAP
  end
  object bBuscaImagen: TSpeedButton [12]
    Left = 364
    Top = 113
    Width = 22
    Height = 22
    Glyph.Data = {
      36050000424D3605000000000000360400002800000010000000100000000100
      0800000000000001000000000000000000000001000000010000000000000101
      0100020202000303030004040400050505000606060007070700080808000909
      09000A0A0A000B0B0B000C0C0C000D0D0D000E0E0E000F0F0F00101010001111
      1100121212001313130014141400151515001616160017171700181818001919
      19001A1A1A001B1B1B001C1C1C001D1D1D001E1E1E001F1F1F00202020002121
      2100222222002323230024242400252525002626260027272700282828002929
      29002A2A2A002B2B2B002C2C2C002D2D2D002E2E2E002F2F2F00303030003131
      3100323232003333330034343400353535003636360037373700383838003939
      39003A3A3A003B3B3B003C3C3C003D3D3D003E3E3E003F3F3F00404040004141
      4100424242004343430044444400454545004646460047474700484848004949
      49004A4A4A004B4B4B004C4C4C004D4D4D004E4E4E004F4F4F00505050005151
      5100525252005353530054545400555555005656560057575700585858005959
      59005A5A5A005B5B5B005C5C5C005D5D5D005E5E5E005F5F5F00606060006161
      6100626262006363630064646400656565006666660067676700686868006969
      69006A6A6A0074647400944D9400BB32BB00E315E300F408F400FB03FB00FD01
      FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00
      FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00
      FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00
      FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00
      FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00
      FE00FE00FE00FE00FE00FD01FE00FC03FE00FB05FE00F908FE00F50EFD00F015
      FC00E821FA00DD32F600D046F100BF61E900AF7EDF00A78DD500A19ECA00A1A6
      BF00A2AEB300AFAFAF00B5B5B500BBBBBB00C0C0C000C4C4C400C8C9C900CED0
      D000D3D6D600D6DDDD00D9E4E500DCE9EB00E0EFF000E3F2F400E5F5F600E5F6
      F800E6F7F900E5F8FA00E4F8FA00E1F8FA00DDF8FB00DAF8FB00D7F7FB00D4F7
      FB00CFF6FA00CBF6FA00C8F6FB00C4F6FB00BFF7FB00B6F8FC00B1F8FD00ACF8
      FD00A7F9FD00A4F9FE00A1F9FE009FF8FE009EF7FD009DF6FD009CF5FD009AF4
      FD0099F3FC0098F1FC0096F0FC0095ECFB0093EBFB0092EAFB008FE9FB008EE8
      FB008CE7FB008BE6FB0089E4FB0086E3FB0083E1FB0080DFFB007EDEFB007CDD
      FA007ADBFA0078DAFA0076D9FA0074D7F90072D6F90070D5F9006ED4F9006CD2
      F80069D0F70066CDF40062CAF0005FC8EE0059C4EA0053C0E6004CBBE30049B8
      E00043B5DD003FB1DB003AAED80031A8D30025A0CD001495C4000B8FC000088D
      BE00088DBE00078DBE00078DBE00078DBE00078DBE00078DBE0086FCFCFCFCFC
      FCFCFCFCFCFCFC868686FCF6EDDEEBEBEBEBEBEBEBEBF4F78686FCF0F5D5E8E8
      E8E8E8E8E8E8F3C4FC86FCE6FCC9E2E2E2E2E2E2E2E2F2C4FC86FCE1F7D5D7DD
      DEDEDDDEDEDDF1C4F786FCDEF2EEC9D8D8D8D8D8D8D8F0C4C4FCFCDAE3F6BBC4
      C4C4C4C4C4C4D5BBC2FCFCD4D4F7FCFCFCFCFCFCFCFCFCFCFCFCFCD0D1D1D0CF
      D1D0D1D0D1D1F9868686FCBBCCCCCCCCCCCCCCCCCCCCF986868686FCBBCBCBCB
      FCFCFCFCFCFC868686868686FCFCFCFC86868686868686868686868686868686
      8686868686868686868686868686868686868686868686868686868686868686
      8686868686868686868686868686868686868686868686868686}
    OnClick = bBuscaImagenClick
  end
  object bReportes: TSpeedButton [13]
    Left = 160
    Top = 233
    Width = 23
    Height = 22
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D808DDDDDDDD
      DDDD83308DDDDDDDDDDD873308DDDDDDDDDDD373308DDDDDDDDDDD3733088888
      8DDDDDD37330000088DDDDDD37338777808DDDDDD038F7F77808DDDDD08FCCCC
      C788DDDCD0F7C7F7C770DDDCC07FCCCCCF70DDDCD0F7CFFCF770DDDDC88FCCCC
      7F88DDDDDD08F7F7F80DDDDDDDD08F7F80DDDDDDDDDD80008DDD}
    OnClick = bReportesClick
  end
  inherited PanelBotones: TPanel
    Top = 331
    Width = 394
    TabOrder = 14
    inherited OK: TBitBtn
      Left = 234
    end
    inherited Cancelar: TBitBtn
      Left = 311
    end
  end
  inherited PanelSuperior: TPanel
    Width = 394
    TabOrder = 0
  end
  inherited PanelIdentifica: TPanel
    Width = 394
    TabOrder = 1
    inherited ValorActivo2: TPanel
      Width = 68
    end
  end
  object KS_CODIGO: TZetaDBEdit [17]
    Left = 86
    Top = 42
    Width = 75
    Height = 21
    CharCase = ecUpperCase
    Enabled = False
    TabOrder = 2
    ConfirmEdit = True
    DataField = 'KS_CODIGO'
    DataSource = DataSource
  end
  object KB_TEXTO: TDBEdit [18]
    Left = 86
    Top = 90
    Width = 216
    Height = 21
    DataField = 'KB_TEXTO'
    DataSource = DataSource
    TabOrder = 4
  end
  object KB_BITMAP: TDBEdit [19]
    Left = 86
    Top = 114
    Width = 275
    Height = 21
    DataField = 'KB_BITMAP'
    DataSource = DataSource
    TabOrder = 5
  end
  object KB_ACCION: TZetaDBKeyCombo [20]
    Left = 86
    Top = 162
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 7
    OnChange = KB_ACCIONChange
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    DataField = 'KB_ACCION'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object KB_URL: TDBEdit [21]
    Left = 86
    Top = 186
    Width = 300
    Height = 21
    DataField = 'KB_URL'
    DataSource = DataSource
    TabOrder = 8
  end
  object KB_SCREEN: TZetaDBKeyLookup [22]
    Left = 86
    Top = 210
    Width = 300
    Height = 21
    TabOrder = 9
    TabStop = True
    WidthLlave = 60
    DataField = 'KB_SCREEN'
    DataSource = DataSource
  end
  object KB_REPORTE: TZetaDBNumero [23]
    Left = 86
    Top = 234
    Width = 73
    Height = 21
    Mascara = mnDias
    TabOrder = 10
    Text = '0'
    DataField = 'KB_REPORTE'
    DataSource = DataSource
  end
  object KB_SEPARA: TZetaDBNumero [24]
    Left = 86
    Top = 307
    Width = 73
    Height = 21
    Mascara = mnDias
    TabOrder = 13
    Text = '0'
    DataField = 'KB_SEPARA'
    DataSource = DataSource
  end
  object KB_LUGAR: TZetaDBKeyCombo [25]
    Left = 86
    Top = 282
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 12
    OnChange = KB_LUGARChange
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    DataField = 'KB_LUGAR'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object KB_ALTURA: TZetaDBNumero [26]
    Left = 86
    Top = 258
    Width = 73
    Height = 21
    Mascara = mnDias
    TabOrder = 11
    Text = '0'
    DataField = 'KB_ALTURA'
    DataSource = DataSource
  end
  object KB_ORDEN: TZetaDBNumero [27]
    Left = 86
    Top = 66
    Width = 73
    Height = 21
    Enabled = False
    Mascara = mnDias
    TabOrder = 3
    Text = '0'
    DataField = 'KB_ORDEN'
    DataSource = DataSource
  end
  object KB_POS_BIT: TZetaDBKeyCombo [28]
    Left = 86
    Top = 138
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 6
    OnChange = KB_ACCIONChange
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    DataField = 'KB_POS_BIT'
    DataSource = DataSource
    LlaveNumerica = True
  end
  inherited DataSource: TDataSource
    Left = 301
    Top = 57
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Filter = 'Bitmaps (*.bmp)|*.bmp'
    FilterIndex = 0
    Left = 336
    Top = 56
  end
end
