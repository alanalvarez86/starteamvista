unit Comparte_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 3/10/2011 3:15:13 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_20_Vsn_2011\WorkFlow\MTS\Comparte.tlb (1)
// LIBID: {6DA800D3-3AF9-41B9-A398-159C06CA9C40}
// LCID: 0
// Helpfile: 
// HelpString: Portal Library
// DepndLst: 
//   (1) v1.0 Midas, (C:\WINDOWS\system32\midas.dll)
//   (2) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ComparteMajorVersion = 1;
  ComparteMinorVersion = 0;

  LIBID_Comparte: TGUID = '{6DA800D3-3AF9-41B9-A398-159C06CA9C40}';

  IID_IdmServerComparte: TGUID = '{F831F80A-0AC7-4081-9721-6E80B39DC055}';
  CLASS_dmServerComparte: TGUID = '{E9AB088C-23F4-4B6F-8E25-96A0CF51D3B6}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerComparte = interface;
  IdmServerComparteDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerComparte = IdmServerComparte;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  PInteger1 = ^Integer; {*}


// *********************************************************************//
// Interface: IdmServerComparte
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F831F80A-0AC7-4081-9721-6E80B39DC055}
// *********************************************************************//
  IdmServerComparte = interface(IAppServer)
    ['{F831F80A-0AC7-4081-9721-6E80B39DC055}']
    function GetGlobales: OleVariant; safecall;
    function GrabaGlobales(Delta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetTabla(Tabla: Integer): OleVariant; safecall;
    function GrabaTabla(Tabla: Integer; Delta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; 
                       out Error: WideString): OleVariant; safecall;
    function EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; 
                         out Error: WideString): WordBool; safecall;
    function GetComparte: OleVariant; safecall;
    function GetUsuarios(Grupo: Integer): OleVariant; safecall;
    function GrabaUsuarios(Usuario: Integer; Delta: OleVariant; Roles: OleVariant; 
                           out ErrorCount: Integer): OleVariant; safecall;
    function GetRoles: OleVariant; safecall;
    function GrabaRoles(const Rol: WideString; Delta: OleVariant; Usuarios: OleVariant; 
                        Modelos: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetAcciones: OleVariant; safecall;
    function GrabaAcciones(Delta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetModelos: OleVariant; safecall;
    function GrabaModelos(const Codigo: WideString; Delta: OleVariant; Roles: OleVariant; 
                          out ErrorCount: Integer): OleVariant; safecall;
    function GetPasosModelo(const Modelo: WideString; out MaxOrder: Integer): OleVariant; safecall;
    function GrabaPasosModelo(const Modelo: WideString; Delta: OleVariant; 
                              Notificaciones: OleVariant; out MaxOrder: Integer; 
                              out ErrorCount: Integer; out NotiResult: OleVariant): OleVariant; safecall;
    function PasoModeloCambiar(const Modelo: WideString; OrdenActual: Integer; OrdenNuevo: Integer): Integer; safecall;
    function GetProcesos(Parametros: OleVariant): OleVariant; safecall;
    function GetTareas(Parametros: OleVariant): OleVariant; safecall;
    function GetCorreos(Parametros: OleVariant): OleVariant; safecall;
    function GetUsuariosRol(const Rol: WideString; out Modelos: OleVariant): OleVariant; safecall;
    function GetRolesUsuario(Usuario: Integer): OleVariant; safecall;
    function GetDatosProceso(Folio: Integer; out Pasos: OleVariant; out Correos: OleVariant; 
                             out Archivos: OleVariant; out Mensajes: OleVariant): OleVariant; safecall;
    function ProcesoSuspender(Parametros: OleVariant; out Msg: WideString): Integer; safecall;
    function ProcesoReiniciar(Parametros: OleVariant; out Msg: WideString): Integer; safecall;
    function ProcesoCancelar(Parametros: OleVariant; out Msg: WideString): Integer; safecall;
    function ModeloCancelar(Parametros: OleVariant; out Msg: WideString): Integer; safecall;
    function ModeloReactivar(Parametros: OleVariant; out Msg: WideString): Integer; safecall;
    function ModeloSuspender(Parametros: OleVariant; out Msg: WideString): Integer; safecall;
    function GetContenidos(Usuario: Integer; Pagina: Integer; Columna: Integer; 
                           out Paginas: OleVariant): OleVariant; safecall;
    function GrabaContenidos(Delta: OleVariant; Usuario: Integer; Pagina: Integer; 
                             Columna: Integer; out ErrorCount: Integer): OleVariant; safecall;
    function GrabaPaginas(Delta: OleVariant; Usuario: Integer; var ErrorCount: Integer): OleVariant; safecall;
    function TareaReasignar(Parametros: OleVariant; out Msg: WideString): Integer; safecall;
    function GetReasignarTareaUsuarios(Parametros: OleVariant): OleVariant; safecall;
    function ModeloReiniciar(Parametros: OleVariant; out Msg: WideString): Integer; safecall;
    function ServiciosStatus(Parametros: OleVariant): OleVariant; safecall;
    function GetWindowsEventLog(Parametros: OleVariant): OleVariant; safecall;
    function GetNoticias: OleVariant; safecall;
    function GrabaNoticias(Delta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetEventos: OleVariant; safecall;
    function GetModeloRoles(const Modelo: WideString): OleVariant; safecall;
    function GetBotones(const sPantalla: WideString): OleVariant; safecall;
    function GetCarrusel(const sShow: WideString): OleVariant; safecall;
    function GetEstilo(const sEstilo: WideString): OleVariant; safecall;
    function GetPantalla(const sPantalla: WideString): OleVariant; safecall;
    function GetShow(const sShow: WideString): OleVariant; safecall;
    function GetKioscos: OleVariant; safecall;
    function GetValoresActivos(const Kiosco: WideString; Empleado: Integer; 
                               const Empresa: WideString): WideString; safecall;
    function GetCarruselesInfo(const Codigo: WideString): OleVariant; safecall;
    function GrabaCarruseles(Delta: OleVariant; DeltaCarruselInfo: OleVariant; 
                             const Codigo: WideString; var CarruselInfo: OleVariant; 
                             var ErrorCount: Integer; var ErrorCountInfo: Integer): OleVariant; safecall;
    function GetReportalMetadata(out RepData: OleVariant): OleVariant; safecall;
    function GrabaReportal(Folio: Integer; var Columnas: OleVariant; var Datos: OleVariant): Integer; safecall;
    function GrabaMarcosInfo(Delta: OleVariant; DeltaBotones: OleVariant; const Codigo: WideString; 
                             var oBotones: OleVariant; var ErrorCount: Integer; 
                             var ErrorCountBotones: Integer): OleVariant; safecall;
    function GetNotificaciones(Parametros: OleVariant): OleVariant; safecall;
    function CopiarModelo(Parametros: OleVariant): OleVariant; safecall;
    function GetNumeroProcesos(const Modelo: WideString): Integer; safecall;
    function GetNivelRoles: OleVariant; safecall;
    function GetConexiones: OleVariant; safecall;
    function GrabaConexiones(Delta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetValidacionUso(const Modulo: WideString): OleVariant; safecall;
    function GrabaValidacionUso(const Modulo: WideString; const Uso: WideString): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerComparteDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F831F80A-0AC7-4081-9721-6E80B39DC055}
// *********************************************************************//
  IdmServerComparteDisp = dispinterface
    ['{F831F80A-0AC7-4081-9721-6E80B39DC055}']
    function GetGlobales: OleVariant; dispid 6;
    function GrabaGlobales(Delta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 7;
    function GetTabla(Tabla: Integer): OleVariant; dispid 1;
    function GrabaTabla(Tabla: Integer; Delta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 2;
    function GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; 
                       out Error: WideString): OleVariant; dispid 3;
    function EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; 
                         out Error: WideString): WordBool; dispid 4;
    function GetComparte: OleVariant; dispid 5;
    function GetUsuarios(Grupo: Integer): OleVariant; dispid 8;
    function GrabaUsuarios(Usuario: Integer; Delta: OleVariant; Roles: OleVariant; 
                           out ErrorCount: Integer): OleVariant; dispid 9;
    function GetRoles: OleVariant; dispid 10;
    function GrabaRoles(const Rol: WideString; Delta: OleVariant; Usuarios: OleVariant; 
                        Modelos: OleVariant; out ErrorCount: Integer): OleVariant; dispid 11;
    function GetAcciones: OleVariant; dispid 12;
    function GrabaAcciones(Delta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 13;
    function GetModelos: OleVariant; dispid 14;
    function GrabaModelos(const Codigo: WideString; Delta: OleVariant; Roles: OleVariant; 
                          out ErrorCount: Integer): OleVariant; dispid 15;
    function GetPasosModelo(const Modelo: WideString; out MaxOrder: Integer): OleVariant; dispid 16;
    function GrabaPasosModelo(const Modelo: WideString; Delta: OleVariant; 
                              Notificaciones: OleVariant; out MaxOrder: Integer; 
                              out ErrorCount: Integer; out NotiResult: OleVariant): OleVariant; dispid 17;
    function PasoModeloCambiar(const Modelo: WideString; OrdenActual: Integer; OrdenNuevo: Integer): Integer; dispid 18;
    function GetProcesos(Parametros: OleVariant): OleVariant; dispid 19;
    function GetTareas(Parametros: OleVariant): OleVariant; dispid 20;
    function GetCorreos(Parametros: OleVariant): OleVariant; dispid 22;
    function GetUsuariosRol(const Rol: WideString; out Modelos: OleVariant): OleVariant; dispid 21;
    function GetRolesUsuario(Usuario: Integer): OleVariant; dispid 23;
    function GetDatosProceso(Folio: Integer; out Pasos: OleVariant; out Correos: OleVariant; 
                             out Archivos: OleVariant; out Mensajes: OleVariant): OleVariant; dispid 24;
    function ProcesoSuspender(Parametros: OleVariant; out Msg: WideString): Integer; dispid 25;
    function ProcesoReiniciar(Parametros: OleVariant; out Msg: WideString): Integer; dispid 26;
    function ProcesoCancelar(Parametros: OleVariant; out Msg: WideString): Integer; dispid 28;
    function ModeloCancelar(Parametros: OleVariant; out Msg: WideString): Integer; dispid 29;
    function ModeloReactivar(Parametros: OleVariant; out Msg: WideString): Integer; dispid 30;
    function ModeloSuspender(Parametros: OleVariant; out Msg: WideString): Integer; dispid 31;
    function GetContenidos(Usuario: Integer; Pagina: Integer; Columna: Integer; 
                           out Paginas: OleVariant): OleVariant; dispid 32;
    function GrabaContenidos(Delta: OleVariant; Usuario: Integer; Pagina: Integer; 
                             Columna: Integer; out ErrorCount: Integer): OleVariant; dispid 33;
    function GrabaPaginas(Delta: OleVariant; Usuario: Integer; var ErrorCount: Integer): OleVariant; dispid 34;
    function TareaReasignar(Parametros: OleVariant; out Msg: WideString): Integer; dispid 35;
    function GetReasignarTareaUsuarios(Parametros: OleVariant): OleVariant; dispid 36;
    function ModeloReiniciar(Parametros: OleVariant; out Msg: WideString): Integer; dispid 37;
    function ServiciosStatus(Parametros: OleVariant): OleVariant; dispid 27;
    function GetWindowsEventLog(Parametros: OleVariant): OleVariant; dispid 38;
    function GetNoticias: OleVariant; dispid 39;
    function GrabaNoticias(Delta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 40;
    function GetEventos: OleVariant; dispid 41;
    function GetModeloRoles(const Modelo: WideString): OleVariant; dispid 42;
    function GetBotones(const sPantalla: WideString): OleVariant; dispid 301;
    function GetCarrusel(const sShow: WideString): OleVariant; dispid 302;
    function GetEstilo(const sEstilo: WideString): OleVariant; dispid 303;
    function GetPantalla(const sPantalla: WideString): OleVariant; dispid 304;
    function GetShow(const sShow: WideString): OleVariant; dispid 305;
    function GetKioscos: OleVariant; dispid 306;
    function GetValoresActivos(const Kiosco: WideString; Empleado: Integer; 
                               const Empresa: WideString): WideString; dispid 307;
    function GetCarruselesInfo(const Codigo: WideString): OleVariant; dispid 308;
    function GrabaCarruseles(Delta: OleVariant; DeltaCarruselInfo: OleVariant; 
                             const Codigo: WideString; var CarruselInfo: OleVariant; 
                             var ErrorCount: Integer; var ErrorCountInfo: Integer): OleVariant; dispid 309;
    function GetReportalMetadata(out RepData: OleVariant): OleVariant; dispid 310;
    function GrabaReportal(Folio: Integer; var Columnas: OleVariant; var Datos: OleVariant): Integer; dispid 311;
    function GrabaMarcosInfo(Delta: OleVariant; DeltaBotones: OleVariant; const Codigo: WideString; 
                             var oBotones: OleVariant; var ErrorCount: Integer; 
                             var ErrorCountBotones: Integer): OleVariant; dispid 312;
    function GetNotificaciones(Parametros: OleVariant): OleVariant; dispid 313;
    function CopiarModelo(Parametros: OleVariant): OleVariant; dispid 314;
    function GetNumeroProcesos(const Modelo: WideString): Integer; dispid 315;
    function GetNivelRoles: OleVariant; dispid 316;
    function GetConexiones: OleVariant; dispid 317;
    function GrabaConexiones(Delta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 318;
    function GetValidacionUso(const Modulo: WideString): OleVariant; dispid 319;
    function GrabaValidacionUso(const Modulo: WideString; const Uso: WideString): OleVariant; dispid 320;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerComparte provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerComparte exposed by              
// the CoClass dmServerComparte. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerComparte = class
    class function Create: IdmServerComparte;
    class function CreateRemote(const MachineName: string): IdmServerComparte;
  end;

implementation

uses ComObj;

class function CodmServerComparte.Create: IdmServerComparte;
begin
  Result := CreateComObject(CLASS_dmServerComparte) as IdmServerComparte;
end;

class function CodmServerComparte.CreateRemote(const MachineName: string): IdmServerComparte;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerComparte) as IdmServerComparte;
end;

end.
