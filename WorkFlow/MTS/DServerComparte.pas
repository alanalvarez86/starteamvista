unit DServerComparte;

interface

uses  Windows, Messages, SysUtils, Classes, Controls,
     Variants,
     ComServ, ComObj, VCLCom, StdVcl, DataBkr, DBClient, Db,
     MtsRdm, Mtx, WinSvc,
     {$ifndef DOS_CAPAS}
     Comparte_TLB,
     {$endif}
     DSuperGlobal,
     DZetaServerProvider,
     ZCreator,
     ZetaPortalClasses,
     ZetaCommonLists,
     ZetaServerDataSet,
     DSeguridad;

type
    eSQLScript = ( Q_COMPANYS_NINGUNA,
                   Q_SIST_USUARIOS,               
                   Q_PASO_MODELO_MAX_ORDEN,       
                   Q_PASO_MODELO_CAMBIAR,
                   Q_PASO_MODELO_BORRAR,
                   Q_PROCESOS,
                   Q_PROCESO_PASOS,
                   Q_PROCESO_ARCHIVOS,
                   Q_PROCESO_MENSAJES,
                   Q_PROCESO_CANCELAR,
                   Q_PROCESO_REINICIAR,
                   Q_PROCESO_SUSPENDER,
                   Q_TAREAS,
                   Q_CORREOS,
                   Q_USER_ROL_INSERT,
                   Q_WMOD_ROL_INSERT,
                   Q_ROL_USUARIOS,
                   Q_ROL_MODELOS,
                   Q_ROL_USUARIOS_DELETE,
                   Q_ROL_MODELOS_DELETE,
                   Q_ROL_NIVEL,
                   Q_USUARIO_ROLES,
                   Q_USUARIO_ROLES_DELETE,          
                   Q_USUARIO_EXISTE,                
                   Q_USUARIO_AGREGA,                
                   Q_USUARIO_ACTUALIZA,             
                   Q_CONTENIDO_LEE,                 
                   Q_CONTENIDO_BORRA,               
                   Q_CONTENIDO_BORRA_TODO,          
                   Q_CONTENIDO_CAMBIA_TODO,         
                   Q_CONTENIDO_RESET,               
                   Q_PAGINAS_LEE,                   
                   Q_PAGINAS_BORRA,
                   {$ifndef ANTES}
                   Q_PAGINAS_AGREGA,
                   Q_PAGINAS_CAMBIA,
                   Q_PAGINAS_RESET,
                   {$endif}
                   Q_PAGINAS_CUENTA,
                   Q_TIPO_NOTICIAS,
                   Q_NOTICIAS,
                   Q_TITULAR_GLOBAL_SET,
                   Q_TITULAR_GLOBAL_RESET,
                   Q_TITULAR_LOCAL_SET,
                   Q_TITULAR_LOCAL_RESET,
                   Q_EVENTOS,
                   Q_TAREA_REASIGNAR,
                   Q_REASIGNAR_TAREA_USUARIOS,
                   Q_MODELO_CAMBIA_STATUS,
                   Q_MODELO_ROLES,
                   Q_MODELO_ROLES_DELETE,
                   Q_PROCESOS_MODELO,
                   Q_COPIAR_MODELO,
                   Q_GET_NUMPROCESOS,
                   Q_REP_COLS_METADATA,
                   Q_REP_DATA_METADATA,
                   Q_REP_BORRA_COLUMNAS,
                   Q_REP_BORRA_DATOS,
                   Q_REP_CUENTA_COLUMNAS,
                   Q_REP_CUENTA_RENGLONES,
                   Q_REP_UPDATE_REPORTE,
                   Q_KIOSCO2_GET_BOTONES,
                   Q_KIOSCO2_GET_CARRUSEL,
                   Q_KIOSCO2_GET_ESTILO,
                   Q_KIOSCO2_GET_PANTALLA,
                   Q_KIOSCO2_GET_SHOW,
                   Q_KIOSCO2_GET_KIOSCOS,
                   Q_KIOSCO2_INSERT_BOTON,
                   Q_KIOSCO2_INSERT_KSHOWINF,
                   Q_CONS_WVALUSO,
                   Q_GRABA_WVALUSO
                   );

  ListaProcesoStatus = set of eWFProcesoStatus;

  TdmServerComparte = class(TMtsDataModule{$ifndef DOS_CAPAS}, IdmServerComparte{$endif})
    cdsDatos: TServerDataSet;
    procedure MtsDataModuleCreate(Sender: TObject);
    procedure MtsDataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    oZetaCreator: TZetaCreator;
    FSeguridadInfo: TSeguridadInfo;
    {$ifdef ANTES}
    FContenidoBorra: TZetaCursor;
    FContenidoCambia: TZetaCursor;
    {$endif}
    FQuery: TZetaCursor;
    FFolio: Integer;
    FNoticiaTitular: Integer;
    FModelo: String;
    FUsuario: Integer;
    FSvcManager: SC_HANDLE;
    {$ifdef NO_SE_USA}FCarrusel: String;{$endif}

    function DateTimeToStrSQL(const dFecha: TDateTime): String;
    function DecodificaClaves( Datos: OleVariant; const sCampo: String ): OleVariant;
    function GetAllDay(const dValue: TDateTime): TDateTime;
    function GetConcatenatedFilter(const sValue: String): String;
    function GetPasosModeloMaxOrder(const Modelo: WideString): Integer;
    function GetNombreTabla( const eTabla : eTipoTabla ) : string;
    function SetFiltroGrupos(const iGrupo: Integer; const sPrefix: String = '' ): String;
    procedure ActualizaTitulares(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    {$ifdef ANTES}
    procedure BeforeUpdatePaginas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    {$endif}
    procedure CargaDatosModelo;
    procedure ClaveUsuarioCodifica(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure ClaveUsuarioGraba(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure Codifica( const sCampo: String; DeltaDS: TzProviderClientDataSet );
    procedure GetFolioTitular;
    procedure InitCreator;
    procedure SetTablaInfo(const eTabla: eTipoTabla);

    function ServiceManagerInit: Boolean;
    procedure ServiceManagerClose;
    function ServiceStatus(const sService: string): DWord;
    function ServiceStatusEmail: DWord;
    function ServiceStatusManager: DWord;
    function ServiceManagerIsWorking(var sMensaje: WideString): Boolean;
    function ServiceStatusIsWorking(const iStatus: DWord; var sMensaje: WideString): Boolean;
    function ProcesoCambiaStatus(const Script: eSQLScript; const eProceso: eWFProcesoStatus; const eBitacora: eClaseBitacora; Parametros: Olevariant; var Msg: WideString): integer;overload;
    function ProcesoCambiaStatus( const Script: eSQLScript; const eProceso: eWFProcesoStatus; const eBitacora: eClaseBitacora; const iProceso: integer; const sNotas: string; var Msg: WideString ): integer;overload;
    function ModeloCambiaStatus( const eModelo: eWFStatusModelo; const eBitacora: eClaseBitacora; const sStatusProceso: string; const Script: eSQLScript; Parametros: OleVariant; var Msg: WideString; const lCambiaProcesos: Boolean = TRUE): Integer;
    procedure BeforeUpdatePasosModelo(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    //procedure BeforeUpdateCarruselInfo(Sender: TObject; SourceDS: TDataSet;DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind;var Applied: Boolean);
    //procedure BeforeUpdateBotones(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    { GA: Los mov� para ac� para que no marque WARNINGS por falta de uso }
    function ServiceEmailIsWorking(var sMensaje: WideString): Boolean;
    procedure CargaDatosProceso;
    function GetNotificaciones(Parametros: OleVariant): OleVariant; safecall;
    function CopiarModelo(Parametros: OleVariant): OleVariant; safecall;
    function GetNumeroProcesos(const Modelo: WideString): Integer; safecall;
    function GetNivelRoles: OleVariant; safecall;
    function GetConexiones: OleVariant; safecall;
    function GrabaConexiones(Delta: OleVariant;
      out ErrorCount: Integer): OleVariant; safecall;
    { GA: Los mov� para ac� para que no marque WARNINGS por falta de uso }
    {$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
    {$endif}
    function EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; out Error: WideString): WordBool; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; out Error: WideString): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetContenidos(Usuario, Pagina, Columna: Integer; out Paginas: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetAcciones: OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetComparte: OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetCorreos(Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS}safecall;{$endif}
    function GetDatosProceso(Folio: Integer; out Pasos, Correos, Archivos, Mensajes: OleVariant): OleVariant;{$ifndef DOS_CAPAS}safecall;{$endif}
    function GetEventos: OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetGlobales: OleVariant;{$ifndef DOS_CAPAS}safecall;{$endif}
    function GetModelos: OleVariant;{$ifndef DOS_CAPAS}safecall;{$endif}
    function GetModeloRoles(const Modelo: WideString): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetNoticias: OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetPasosModelo(const Modelo: WideString; out MaxOrder: Integer): OleVariant;{$ifndef DOS_CAPAS}safecall;{$endif}
    function GetProcesos(Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetRoles: OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetRolesUsuario(Usuario: Integer): OleVariant;{$ifndef DOS_CAPAS}safecall;{$endif}
    function GetTabla(Tabla: Integer): OleVariant;{$ifndef DOS_CAPAS}safecall;{$endif}
    function GetTareas(Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetUsuarios(Grupo: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetUsuariosRol(const Rol: WideString; out Modelos: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GrabaAcciones(Delta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GrabaGlobales(Delta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GrabaContenidos(Delta: OleVariant; Usuario, Pagina, Columna: Integer; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GrabaPaginas(Delta: OleVariant; Usuario: Integer; var ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GrabaModelos(const Codigo: WideString; Delta, Roles: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GrabaNoticias(Delta: OleVariant;out Errorcount: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;
    function GrabaPasosModelo(const Modelo: WideString; Delta,
      Notificaciones: OleVariant; out MaxOrder, ErrorCount: Integer;
      out NotiResult: OleVariant): OleVariant; safecall;{$endif}
    function GrabaRoles(const Rol: WideString; Delta, Usuarios, Modelos: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GrabaTabla( Tabla: Integer; Delta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GrabaUsuarios(Usuario: Integer; Delta, Roles: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function ModeloSuspender(Parametros: OleVariant; out Msg: WideString): Integer;{$ifndef DOS_CAPAS}safecall;{$endif}
    function ModeloReiniciar(Parametros: OleVariant;out Msg: WideString): Integer; {$ifndef DOS_CAPAS}safecall;{$endif}
    function ModeloCancelar(Parametros: OleVariant; out Msg: WideString): Integer;{$ifndef DOS_CAPAS}safecall;{$endif}
    function ModeloReactivar(Parametros: OleVariant; out Msg: WideString): Integer;{$ifndef DOS_CAPAS}safecall;{$endif}
    function PasoModeloCambiar(const Modelo: WideString; OrdenActual, OrdenNuevo: Integer): Integer;{$ifndef DOS_CAPAS}safecall;{$endif}
    function ProcesoCancelar(Parametros: OleVariant; out Msg: WideString): Integer;{$ifndef DOS_CAPAS}safecall;{$endif}
    function ProcesoReiniciar(Parametros: OleVariant; out Msg: WideString): Integer; {$ifndef DOS_CAPAS}safecall;{$endif}
    function ProcesoSuspender(Parametros: OleVariant; out Msg: WideString): Integer;{$ifndef DOS_CAPAS}safecall;{$endif}
    function TareaReasignar(Parametros: OleVariant; out Msg: WideString): Integer; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetReasignarTareaUsuarios(Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS}safecall;{$endif}
    function ServiciosStatus(Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetWindowsEventLog(Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetBotones(const sPantalla: WideString): OleVariant;  {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetCarrusel(const sShow: WideString): OleVariant;  {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetEstilo(const sEstilo: WideString): OleVariant;  {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetKioscos: OleVariant;  {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetPantalla(const sPantalla: WideString): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetShow(const sShow: WideString): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetValoresActivos(const Kiosco: WideString; Empleado: Integer; const Empresa: WideString): WideString;  {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetCarruselesInfo(const Codigo: WideString): OleVariant;  {$ifndef DOS_CAPAS}safecall;{$endif}
    function GrabaCarruseles(Delta, DeltaCarruselInfo: OleVariant; const Codigo: WideString; var CarruselInfo: OleVariant;var ErrorCount, ErrorCountInfo: Integer): OleVariant;  {$ifndef DOS_CAPAS}safecall;{$endif}
    function GrabaMarcosInfo(Delta, DeltaBotones: OleVariant; const Codigo: WideString; var oBotones: OleVariant; var ErrorCount, ErrorCountBotones: Integer): OleVariant;  {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetReportalMetadata(out RepData: OleVariant): OleVariant;{$ifndef DOS_CAPAS}safecall;{$endif}
    function GrabaReportal(Folio: Integer; var Columnas, Datos: OleVariant): Integer; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetValidacionUso(const Modulo: WideString): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GrabaValidacionUso(const Modulo, Uso: WideString): OleVariant;{$ifndef DOS_CAPAS}safecall;{$endif}

  end;

var
   dmServerComparte: TdmServerComparte;

implementation

uses DServerReporteador,
     DWorkFlowConst,
     DEventLogViewer,
     ZetaWinAPITools,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaServerTools,
     ZGlobalTress,
     ZetaSQLBroker;

{$R *.DFM}

const
     K_OFFSET = 100;
     K_INT_TRUE = 0;
     K_INT_FALSE = 1;
     K_PARAMETRO_USUARIO = 'Usuario';
     K_USUARIOS_PREFIX = 'U1.';
     K_CAMPOS_USUARIO = '%0:sUS_CODIGO, %0:sUS_CORTO, %0:sGR_CODIGO, %0:sUS_NIVEL, %0:sUS_NOMBRE, %0:sUS_PASSWRD, %0:sUS_BLOQUEA, %0:sUS_CAMBIA, '+
                        '%0:sUS_FEC_IN, %0:sUS_FEC_OUT, %0:sUS_FEC_SUS, %0:sUS_DENTRO, %0:sUS_ARBOL, %0:sUS_FORMA, %0:sUS_BIT_LST, '+
                        '%0:sUS_FEC_PWD, %0:sUS_EMAIL, %0:sUS_FORMATO, %0:sUS_LUGAR, %0:sUS_MAQUINA, %0:sCM_CODIGO, %0:sCB_CODIGO, %0:sUS_JEFE, '+
                        '%0:sUS_BIO_ID, %0:sUS_FALLAS, %0:sUS_PAGINAS, %0:sUS_PAG_DEF, %0:sUS_ANFITRI, %0:sUS_DOMAIN, %0:sUS_WF_OUT, %0:sUS_WF_ALT ';

     K_CLAVE_USUARIO = 'US_PASSWRD';
     K_MAX_RECORDS = 1000;


     NAVEGACION = 'NAVEGACION';


var  FCuentaMSG: integer;

procedure xMSG( const s: string );overload;
begin
     {$ifdef CAROLINA}
     showmessage(s);
     {$endif}
end;

procedure xMSG(const i:integer );overload;
begin
     {$ifdef CAROLINA}
     xmsg( intToSTr(i));
     {$endif}
end;

procedure xmsg;overload;
begin
     {$ifdef CAROLINA}
     xmsg(FCuentaMSG);
     inc(FCuentaMSG);
     {$endif}
end;



function GetScript( const eScript: eSQLScript ): String;
begin
     case eScript of
          Q_COMPANYS_NINGUNA: Result := 'select CM_CODIGO, CM_NOMBRE, CM_ALIAS, CM_USRNAME, CM_PASSWRD, CM_NIVEL0, CM_DATOS, CM_DIGITO ' +
                                        'from COMPANY where ( CM_CODIGO = ''%s'' )';
          Q_SIST_USUARIOS: Result := 'select ' + Format( K_CAMPOS_USUARIO, [ K_USUARIOS_PREFIX ] ) + ','+
                                     '( select U2.US_NOMBRE from USUARIO U2 where ( U2.US_CODIGO = U1.US_JEFE ) ) as NOM_JEFE, '+
                                     'GRUPO.GR_DESCRIP GRUPO '+
                                     'from USUARIO U1 '+
                                     'left outer join GRUPO on ( GRUPO.GR_CODIGO = U1.GR_CODIGO ) '+
                                     'where ( U1.US_CODIGO <> 0 ) %s '+
                                     'order by U1.US_CODIGO';
          Q_PASO_MODELO_MAX_ORDEN: Result := 'select MAX( WE_ORDEN ) MAXIMO from WMODSTEP where ( WM_CODIGO = ''%s'' )';
          Q_PASO_MODELO_CAMBIAR: Result := 'exec DBO.SP_CAMBIA_WMODSTEP ''%s'', %d, %d';
          Q_PROCESOS: Result := 'select TOP %d WP_FOLIO, WP_NOMBRE, WP_USR_INI, WP_NOM_INI, WP_FEC_INI, WP_FEC_FIN, WP_NOTAS, WS_NOMBRE, WT_NOM_DES, WP_STATUS, '+
                                'CAST( WP_PASO AS VARCHAR( 4 )) + '' de '' + CAST( WP_PASOS AS VARCHAR(4)) PasoActual, '+
                                'DBO.GetStatusProceso( WP_STATUS ) StatusProceso '+
                                'from V_PROCESO where ( CAST( WP_FEC_INI as DateTime ) between ''%s'' and ''%s'' ) %s '+
                                'order by WP_FOLIO';
          Q_PROCESO_PASOS: Result := 'select WE_ORDEN, WS_NOMBRE, WT_NOM_DES, WT_NOTAS, WS_FEC_INI, WS_FEC_FIN, WT_AVANCE, '+
                                     'DBO.GetStatusTarea( WT_STATUS ) StatusTarea '+
                                     'from V_PASOS where ( WP_FOLIO = %d ) order by WE_ORDEN';
          Q_PROCESO_ARCHIVOS: Result := 'select WH_ORDEN, WH_NOMBRE, WH_RUTA from WARCHIVO where ( WP_FOLIO = %d ) order by WH_ORDEN';
          Q_PROCESO_MENSAJES: Result := 'select WMENSAJE.WE_ORDEN, WS_NOMBRE, WN_MENSAJE, WN_FECHA, WN_LEIDO, '+
                                        'WN_USR_INI, DBO.GetNombreUsuario( WN_USR_INI ) NombreIni, '+
                                        'WN_USR_FIN, DBO.GetNombreUsuario( WN_USR_FIN) NombreFin '+
                                        'from WMENSAJE '+
                                        'join WPROSTEP on ( WMENSAJE.WP_FOLIO = WPROSTEP.WP_FOLIO ) and ( WMENSAJE.WE_ORDEN = WPROSTEP.WE_ORDEN ) '+
                                        'where ( WMENSAJE.WP_FOLIO = %d ) order by WN_FECHA';
          Q_PROCESO_CANCELAR: Result := 'EXEC DBO.SP_PROCESO_CANCELAR %d, %s ';
          Q_PROCESO_REINICIAR: Result := 'EXEC DBO.SP_PROCESO_REINICIAR %d, %s ';
          Q_PROCESO_SUSPENDER: Result := 'EXEC DBO.SP_PROCESO_SUSPENDER %d, %s ';
          Q_TAREAS: Result := 'select TOP %d WP_FOLIO, WP_NOMBRE, WM_CODIGO, WE_ORDEN, WP_NOM_INI, WT_AVANCE, '+
                              'WT_NOM_DES, WT_FEC_INI, WT_FEC_FIN, WT_NOM_ORI, WT_NOTAS, WS_NOMBRE, '+
                              'CAST( WE_ORDEN AS VARCHAR( 4 )) + '' de '' + CAST( WP_PASOS AS VARCHAR(4)) PasoActual, '+
	                      'DBO.GetStatusTarea( WT_STATUS ) StatusTarea, '+
                              'WT_STATUS, CAST( WT_GUID AS VARCHAR(50) ) WT_GUID '+
                              'from V_TAREA where ( CAST( WT_FEC_INI as DateTime ) between ''%s'' and ''%s'' ) %s '+
                              'order by WP_FOLIO';
          Q_CORREOS: Result := 'select TOP %d C.WO_FROM_NA, C.WO_FROM_AD, C.WO_TO, C.WO_CC, C.WO_SUBJECT, C.WO_BODY, C.WO_SUBTYPE, '+
                               'C.WO_FEC_IN, C.WO_ENVIADO, C.WO_FEC_OUT, C.WO_GUID, C.WP_FOLIO, C.WO_STATUS, C.WO_LOG, C.US_ENVIA, C.US_RECIBE, '+
                               'P.WP_NOMBRE, P.WM_CODIGO, P.WS_NOMBRE, '+
                               'CAST( P.WP_PASO AS VARCHAR( 4 ) ) + '' de '' + CAST( P.WP_PASOS AS VARCHAR( 4 ) ) PasoActual '+
                               'from WOUTBOX C '+
                               'left outer join V_PROCESO P on ( P.WP_FOLIO = C.WP_FOLIO ) '+
                               'where %s order by C.WO_FEC_IN';
          Q_USER_ROL_INSERT: Result := 'insert into USER_ROL( RO_CODIGO, US_CODIGO ) values ( :RO_CODIGO, :US_CODIGO )';
          Q_WMOD_ROL_INSERT: Result := 'insert into WMOD_ROL( RO_CODIGO, WM_CODIGO ) values ( :RO_CODIGO, :WM_CODIGO )';
          Q_ROL_USUARIOS: Result := 'select USER_ROL.RO_CODIGO, USER_ROL.US_CODIGO, ROL.RO_NOMBRE, USUARIO.US_NOMBRE from USER_ROL '+
                                    'join ROL on ( ROL.RO_CODIGO = USER_ROL.RO_CODIGO ) '+
                                    'join USUARIO on ( USUARIO.US_CODIGO = USER_ROL.US_CODIGO ) '+
                                    'where ( USER_ROL.RO_CODIGO = ''%s'' ) order by USER_ROL.US_CODIGO';
          Q_ROL_MODELOS: Result := 'select WMOD_ROL.RO_CODIGO, WMOD_ROL.WM_CODIGO, WMODELO.WM_NOMBRE from WMOD_ROL '+
                                   'join WMODELO on ( WMODELO.WM_CODIGO = WMOD_ROL.WM_CODIGO ) '+
                                   'where ( WMOD_ROL.RO_CODIGO = ''%s'' ) order by WMOD_ROL.WM_CODIGO';
          Q_ROL_USUARIOS_DELETE: Result := 'delete from USER_ROL where ( USER_ROL.RO_CODIGO = ''%s'' )';
          Q_ROL_MODELOS_DELETE: Result := 'delete from WMOD_ROL where ( WMOD_ROL.RO_CODIGO = ''%s'' )';
          Q_USUARIO_ROLES: Result := 'select USER_ROL.RO_CODIGO, USER_ROL.US_CODIGO, ROL.RO_NOMBRE, USUARIO.US_NOMBRE from USER_ROL '+
                                     'join ROL on ( ROL.RO_CODIGO = USER_ROL.RO_CODIGO ) '+
                                     'join USUARIO on ( USUARIO.US_CODIGO = USER_ROL.US_CODIGO ) '+
                                     'where ( USER_ROL.US_CODIGO = %d ) order by USER_ROL.RO_CODIGO';
          Q_USUARIO_ROLES_DELETE: Result := 'delete from USER_ROL where ( USER_ROL.US_CODIGO = %d )';
          Q_USUARIO_EXISTE: Result := 'select COUNT(*) CUANTOS from USUARIO where ( US_CODIGO = %d )';
          Q_USUARIO_AGREGA: Result := 'insert into USUARIO ( US_CODIGO, US_NOMBRE, US_PAGINAS ) values ( %d, ''%s'', %d )';
          Q_USUARIO_ACTUALIZA: Result := 'update USUARIO set US_PAGINAS = %d where ( US_CODIGO = %d )';
 {$ifdef ANTES}
          Q_CONTENIDO_LEE: Result := 'select PA_ORDEN, CT_COLUMNA, CT_ORDEN, CT_TIPO, US_CODIGO, CT_MOSTRAR, CT_REPORTE, CT_CLASIFI, '+
                                     'REPORTAL.RP_TITULO as CT_DESCRIP '+
                                     'from CONTIENE '+
                                     'left outer join REPORTAL on ( REPORTAL.RP_FOLIO = CONTIENE.CT_REPORTE ) '+
                                     'where '+
                                     '( US_CODIGO = %0:d ) and '+
                                     '( PA_ORDEN = %1:d ) and '+
                                     '( CT_COLUMNA = %2:d ) and '+
                                     '( CT_TIPO in ( %3:d, %4:d ) ) '+
                                     'union '+
                                     'select PA_ORDEN, CT_COLUMNA, CT_ORDEN, CT_TIPO, US_CODIGO, CT_MOSTRAR, CT_REPORTE, CT_CLASIFI, '+
                                     'CAST( ''Titular'' as VARCHAR( 70 ) ) as CT_DESCRIP '+
                                     'from CONTIENE '+
                                     'where '+
                                     '( US_CODIGO = %0:d ) and '+
                                     '( PA_ORDEN = %1:d ) and '+
                                     '( CT_COLUMNA = %2:d ) and '+
                                     '( CT_TIPO = %5:d ) '+
                                     'union '+
                                     'select PA_ORDEN, CT_COLUMNA, CT_ORDEN, CT_TIPO, US_CODIGO, CT_MOSTRAR, CT_REPORTE, CT_CLASIFI, '+
                                     'TNOTICIA.TB_ELEMENT as CT_DESCRIP '+
                                     'from CONTIENE '+
                                     'left outer join TNOTICIA on ( TNOTICIA.TB_CODIGO = CONTIENE.CT_CLASIFI ) '+
                                     'where '+
                                     '( US_CODIGO = %0:d ) and '+
                                     '( PA_ORDEN = %1:d ) and '+
                                     '( CT_COLUMNA = %2:d ) and '+
                                     '( CT_TIPO = %6:d ) '+
                                     'union '+
                                     'select PA_ORDEN, CT_COLUMNA, CT_ORDEN, CT_TIPO, US_CODIGO, CT_MOSTRAR, CT_REPORTE, CT_CLASIFI, '+
                                     'TEVENTO.TB_ELEMENT as CT_DESCRIP '+
                                     'from CONTIENE '+
                                     'left outer join TEVENTO on ( TEVENTO.TB_CODIGO = CONTIENE.CT_CLASIFI ) '+
                                     'where '+
                                     '( US_CODIGO = %0:d ) and '+
                                     '( PA_ORDEN = %1:d ) and '+
                                     '( CT_COLUMNA = %2:d ) and '+
                                     '( CT_TIPO = %7:d )';
          {$else}
          Q_CONTENIDO_LEE: Result := 'select PA_ORDEN, CT_COLUMNA, CT_ORDEN, CT_TIPO, US_CODIGO, CT_MOSTRAR, CT_REPORTE, CT_CLASIFI, '+
                                     'REPORTAL.RP_TITULO as CT_DESCRIP '+
                                     'from CONTIENE '+
                                     'left outer join REPORTAL on ( REPORTAL.RP_FOLIO = CONTIENE.CT_REPORTE ) '+
                                     'where '+
                                     '( US_CODIGO = %0:d ) and '+
                                     '( PA_ORDEN = %1:d ) and '+
                                     '( CT_COLUMNA = %2:d ) and '+
                                     '( CT_TIPO in ( %3:d, %4:d ) ) '+
                                     'union '+
                                     'select PA_ORDEN, CT_COLUMNA, CT_ORDEN, CT_TIPO, US_CODIGO, CT_MOSTRAR, CT_REPORTE, CT_CLASIFI, '+
                                     'TNOTICIA.TB_ELEMENT as CT_DESCRIP '+
                                     'from CONTIENE '+
                                     'left outer join TNOTICIA on ( TNOTICIA.TB_CODIGO = CONTIENE.CT_CLASIFI ) '+
                                     'where '+
                                     '( US_CODIGO = %0:d ) and '+
                                     '( PA_ORDEN = %1:d ) and '+
                                     '( CT_COLUMNA = %2:d ) and '+
                                     '( CT_TIPO in ( %5:d, %6:d ) ) '+
                                     'union '+
                                     'select PA_ORDEN, CT_COLUMNA, CT_ORDEN, CT_TIPO, US_CODIGO, CT_MOSTRAR, CT_REPORTE, CT_CLASIFI, '+
                                     'TEVENTO.TB_ELEMENT as CT_DESCRIP '+
                                     'from CONTIENE '+
                                     'left outer join TEVENTO on ( TEVENTO.TB_CODIGO = CONTIENE.CT_CLASIFI ) '+
                                     'where '+
                                     '( US_CODIGO = %0:d ) and '+
                                     '( PA_ORDEN = %1:d ) and '+
                                     '( CT_COLUMNA = %2:d ) and '+
                                     '( CT_TIPO = %7:d )';
          {$endif}
          Q_CONTENIDO_BORRA: Result := 'delete from CONTIENE where '+
                                       '( US_CODIGO = %d ) and '+
                                       '( PA_ORDEN = %d ) and '+
                                       '( CT_COLUMNA = %d )';
          Q_CONTENIDO_BORRA_TODO: Result := 'delete from CONTIENE where '+
                                            '( US_CODIGO = %d ) and '+
                                            '( PA_ORDEN = :PA_ORDEN )';
          Q_CONTENIDO_CAMBIA_TODO: Result := 'update CONTIENE set PA_ORDEN = :PA_ORDEN where ' +
                                             '( US_CODIGO = %d ) and '+
                                             '( PA_ORDEN = :OLD_PA_ORDEN )';
          Q_CONTENIDO_RESET: Result := 'update CONTIENE set PA_ORDEN = PA_ORDEN - %0:d where '+
                                       '( US_CODIGO = %1:d ) and '+
                                       '( PA_ORDEN >= %0:d )';
          Q_PAGINAS_LEE: Result := 'select US_CODIGO, PA_ORDEN, PA_TITULO, PA_COLS, '+
                                   'PA_ORDEN as PA_ANTERIOR '+
                                   'from PAGINA where ( US_CODIGO = %d ) order by PA_ORDEN';
          {$ifdef ANTES}
          Q_PAGINAS_BORRA: Result := 'delete from PAGINA where ( US_CODIGO = %d )';
          {$else}
          Q_PAGINAS_BORRA: Result := 'delete from PAGINA where ( US_CODIGO = %d ) and ( PA_ORDEN = :OLD_PA_ORDEN )';
          Q_PAGINAS_AGREGA: Result := 'insert into PAGINA( US_CODIGO, PA_ORDEN, PA_TITULO, PA_COLS ) values ( %d, :PA_ORDEN, :PA_TITULO, :PA_COLS )';
          Q_PAGINAS_CAMBIA: Result := 'update PAGINA set PA_ORDEN = :PA_ORDEN, PA_TITULO = :PA_TITULO, PA_COLS = :PA_COLS where ( US_CODIGO = %d ) and ( PA_ORDEN = :OLD_PA_ORDEN )';
          Q_PAGINAS_RESET: Result := 'update PAGINA set PA_ORDEN = PA_ORDEN - %0:d where ( US_CODIGO = %1:d ) and ( PA_ORDEN >= %0:d )';
          {$endif}
          Q_PAGINAS_CUENTA: Result := 'select COUNT(*) CUANTAS from PAGINA where ( US_CODIGO = %d )';
          Q_TIPO_NOTICIAS: Result := 'select TB_CODIGO, TB_ELEMENT, TB_INGLES, CAST( TB_NUMERO as INTEGER ) as TB_NUMERO, TB_TEXTO from TNOTICIA order by TB_CODIGO';
          Q_NOTICIAS: Result := 'select N.NT_FOLIO, N.NT_FECHA, N.NT_HORA, N.NT_IMAGEN, N.NT_MEMO, N.NT_TITULO, N.NT_AUTOR, N.NT_CLASIFI, N.NT_CORTA, N.NT_SUB_TIT, '+
                                '0 as NT_USUARIO, N.NT_USUARIO as US_CODIGO, '+
                                '( case when ( N.NT_FOLIO = %0:d ) then ''%1:s'' else ''%2:s'' end ) as NT_MAIN, '+
                                '( case when ( N.NT_FOLIO = T.TB_NUMERO ) then ''%1:s'' else ''%2:s'' end ) as NT_LOCAL '+
                                'from NOTICIA N '+
                                'left outer join TNOTICIA T on ( T.TB_CODIGO = N.NT_CLASIFI ) '+
                                'order by N.NT_FOLIO desc';
          Q_TITULAR_GLOBAL_SET: Result := 'update GLOBAL set GL_FORMULA = ''%d'' where ( GL_CODIGO = %d )';
          Q_TITULAR_GLOBAL_RESET: Result := 'update GLOBAL set GL_FORMULA = '''' where ( GL_CODIGO = %d )';
          Q_TITULAR_LOCAL_SET: Result := 'update TNOTICIA set TB_NUMERO = %d where ( TB_CODIGO = ''%s'' )';
          Q_TITULAR_LOCAL_RESET: Result := 'update TNOTICIA set TB_NUMERO = 0 where ( TB_CODIGO = ''%s'' ) and ( TB_NUMERO = %d )';
          Q_EVENTOS: Result := 'select EV_FOLIO, EV_FEC_INI, EV_FEC_FIN, EV_HORA, EV_IMAGEN, EV_LUGAR, EV_MEMO, EV_TITULO, EV_USUARIO, EV_AUTOR, EV_CLASIFI, EV_URL from EVENTO order by EV_FOLIO desc';
          Q_TAREA_REASIGNAR: Result := 'execute procedure DBO.SP_DELEGA_TAREA( :Tarea, :Destino, :Notas )';
          Q_REASIGNAR_TAREA_USUARIOS: Result := 'Select US_CODIGO, US_NOMBRE FROM DBO.GetUsuariosDelega( %s, %d )';

          Q_MODELO_CAMBIA_STATUS : Result := 'update WMODELO set WM_STATUS = %d where ( WM_CODIGO = %s )';
          Q_MODELO_ROLES : Result := 'select WMOD_ROL.RO_CODIGO, WMOD_ROL.WM_CODIGO, ROL.RO_NOMBRE from WMOD_ROL '+
                                     'join ROL on ( ROL.RO_CODIGO = WMOD_ROL.RO_CODIGO ) '+
                                     'where ( WMOD_ROL.WM_CODIGO = ''%s'' ) order by WMOD_ROL.RO_CODIGO';
          Q_MODELO_ROLES_DELETE : Result := 'delete from WMOD_ROL where ( WM_CODIGO = ''%s'' )';
          Q_ROL_NIVEL : Result := 'Select distinct RO_NIVEL, ''Nivel #''+ cast(ro_nivel as varchar(3)) as ro_nombre from ROL Order By RO_NIVEL';
          Q_PROCESOS_MODELO : Result := 'select V_PROCESO.* from V_PROCESO where ( WM_CODIGO = %s ) and ( WP_STATUS in ( %s ) ) order by WP_FOLIO';
          Q_COPIAR_MODELO : Result := 'Exec DBO.SP_COPIAR_MODELO %s , %s, %s';
          Q_GET_NUMPROCESOS: Result := 'Select count(*) as NUMPROCESOS from WPROCESO where WM_CODIGO = :WM_CODIGO ';
          Q_REP_COLS_METADATA: Result := 'select RP_FOLIO, RC_ORDEN, RC_TITULO, RC_TIPO, RC_ANCHO '+
                                         'from REP_COLS where ( RP_FOLIO < 0 )';
          Q_REP_DATA_METADATA: Result := 'select RP_FOLIO, RD_ORDEN, RD_DATO1, RD_DATO2, RD_DATO3, RD_DATO4, '+
                                         'RD_DATO5, RD_DATO6, RD_DATO7, RD_DATO8, RD_DATO9, RD_DATO10 '+
                                         'from REP_DATA where ( RP_FOLIO < 0 ) and ( RD_ORDEN < 0 )';
          Q_REP_BORRA_COLUMNAS: Result := 'delete from REP_COLS where ( RP_FOLIO = %d )';
          Q_REP_BORRA_DATOS: Result := 'delete from REP_DATA where ( RP_FOLIO = %d )';
          Q_REP_CUENTA_COLUMNAS: Result := 'select COUNT(*) CUANTOS from REP_COLS where ( RP_FOLIO = %d )';
          Q_REP_CUENTA_RENGLONES: Result := 'select COUNT(*) CUANTOS from REP_DATA where ( RP_FOLIO = %d )';
          Q_REP_UPDATE_REPORTE: Result := 'update REPORTAL set RP_FECHA = ''%s'', RP_HORA = ''%s'', RP_COLS = %d, RP_ROWS = %d where '+
                                          '( RP_FOLIO = %d )';


 { ****** Scripts para Kiosco *************}
          Q_KIOSCO2_GET_BOTONES: Result := ' select KS_CODIGO,KB_ORDEN,KB_TEXTO,KB_BITMAP,KB_ACCION, '+
                                                   'KB_URL,KB_SCREEN,KB_ALTURA,KB_LUGAR,KB_SEPARA,KB_REPORTE,KB_POS_BIT '+
                                           ' from KBOTON '+
                                           ' where( KS_CODIGO = %s ) '+
                                           ' order by KB_ORDEN ';
          Q_KIOSCO2_GET_CARRUSEL: Result := 'select KW_CODIGO,KI_ORDEN,KI_TIMEOUT,KI_URL,KI_REFRESH '+
                                            'from KSHOWINF '+
                                            'where(KW_CODIGO = %s) '+
                                            'order by KI_ORDEN';
          Q_KIOSCO2_GET_ESTILO: Result := 'select KE_CODIGO,KE_NOMBRE,KE_COLOR,KE_F_NAME,KE_F_SIZE,KE_F_COLR,KE_F_BOLD,KE_F_ITAL,KE_F_SUBR,KE_BITMAP '+
                                          'from KESTILO where(KE_CODIGO = %s)';
          Q_KIOSCO2_GET_PANTALLA: Result := 'select KS_CODIGO,KS_NOMBRE,KS_PF_SIZE,KS_PF_COLR,KS_PF_ALIN,KS_PF_URL,KS_PB_SIZE, '+
                                            'KS_PB_COLR,KS_PB_ALIN,KS_BTN_ALT,KS_BTN_SEP,KS_EST_NOR,KS_EST_SEL, KS_BITMAP '+
                                            'from KSCREEN where(KS_CODIGO = %s)';
          Q_KIOSCO2_GET_SHOW: Result := 'select KW_CODIGO,KW_NOMBRE,KW_SCR_DAT,KW_TIM_DAT,KW_GET_NIP,CM_CODIGO from KSHOW where(KW_CODIGO = %s)';
          Q_KIOSCO2_GET_KIOSCOS: Result := 'select KW_CODIGO, KW_NOMBRE from KSHOW order by KW_CODIGO ';
          Q_KIOSCO2_INSERT_BOTON: Result := 'INSERT INTO KBOTON (KS_CODIGO,	KB_ORDEN,	KB_TEXTO,	KB_BITMAP,	KB_ACCION, '+
                                                                'KB_URL,	KB_SCREEN,	KB_ALTURA,	KB_LUGAR,	KB_SEPARA,KB_REPORTE,KB_POS_BIT) '+
                                                        'VALUES (:KS_CODIGO,	:KB_ORDEN,	:KB_TEXTO,	:KB_BITMAP,	:KB_ACCION, '+
                                                               	':KB_URL,	:KB_SCREEN,	:KB_ALTURA,	:KB_LUGAR,	:KB_SEPARA,:KB_REPORTE,:KB_POS_BIT) ';
          Q_KIOSCO2_INSERT_KSHOWINF: Result := 'INSERT INTO KSHOWINF (KW_CODIGO,KI_ORDEN,KI_TIMEOUT,KI_URL,KI_REFRESH ) '+
                                                            ' VALUES (:KW_CODIGO,:KI_ORDEN,:KI_TIMEOUT,:KI_URL,:KI_REFRESH ) ';
          Q_PASO_MODELO_BORRAR:  Result := 'exec DBO.SP_BORRA_WMODSTEP %s, %d';
          Q_CONS_WVALUSO: Result := 'select MODULO, USO from WVALUSO where MODULO=''%s''';
          Q_GRABA_WVALUSO: Result := 'exec DBO.SP_GRABA_WVALUSO :MODULO, :USO';
     else
         Result := '';
     end;
end;

{ ********* TdmServerComparte ************ }

class procedure TdmServerComparte.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

procedure TdmServerComparte.MtsDataModuleCreate(Sender: TObject);
begin
     FCuentaMSG := 0;
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmServerComparte.MtsDataModuleDestroy(Sender: TObject);
begin
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

procedure TdmServerComparte.InitCreator;
begin
     if not Assigned( oZetaCreator ) then
        oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
end;

function TdmServerComparte.DateTimeToStrSQL( const dFecha: TDateTime ): String;
begin
     Result := FormatDateTime( 'mm/dd/yyyy hh:nn:ss', dFecha );
end;

function TdmServerComparte.GetAllDay( const dValue: TDateTime ): TDateTime;
begin
     Result := Trunc( dValue ) + EncodeTime( 23, 59, 59, 0 );
end;

procedure TdmServerComparte.CargaDatosProceso;
begin
     with oZetaProvider.ParamList do
     begin
          FFolio := ParamByName( 'Proceso' ).AsInteger;
          FUsuario := ParamByName( K_PARAMETRO_USUARIO ).AsInteger;
     end;
end;

procedure TdmServerComparte.CargaDatosModelo;
begin
     with oZetaProvider.ParamList do
     begin
          FModelo := ParamByName( 'Modelo' ).AsString;
          FUsuario := ParamByName( K_PARAMETRO_USUARIO ).AsInteger;
     end;
end;

function TdmServerComparte.GetNombreTabla(const eTabla: eTipoTabla): string;
begin
     case eTabla of
          eTNoticia:    Result := 'TNOTICIA';
          eTEvento:     Result := 'TEVENTO';
          eTDocumen:    Result := 'TDOCUMEN';
          eNoticia:     Result := 'NOTICIA';
          eEvento:      Result := 'EVENTO';
          eDocument:    Result := 'DOCUMENT';
          eBuzonSug:    Result := 'BUZONSUG';
          eContiene:    Result := 'CONTIENE';
          eReportal:    Result := 'REPORTAL';
          eAccion:      Result := 'WACCION';
          eRol:         Result := 'ROL';
          eModeloR:     Result := 'WMODELO';
          eModeloW:     Result := 'WMODELO';
          ePasosModelo: Result := 'WMODSTEP';
          ekCarruseles: Result := 'KSHOW';
          ekCarruselesInfo: Result := 'KSHOWINF';
          ekMarcosInformacion: Result := 'KSCREEN';
          ekEstilosBotones: Result := 'KESTILO';
          ekBotones: Result := 'KBOTON';
          eNotificacion: Result := 'WMODNOTI';
          eConexion : Result := 'WCONEXION';
     end;
end;

procedure TdmServerComparte.SetTablaInfo(const eTabla: eTipoTabla);
const
     K_WMODELO_CAMPOS = 'WM_CODIGO, WM_NOMBRE, WM_DESCRIP, WM_TIPNOT1, WM_USRNOT1, WM_TIPNOT2, WM_USRNOT2, WM_TIPNOT3, WM_USRNOT3, WM_FORMATO, WM_STATUS, WM_URL, WM_URL_EXA, WM_URL_OK, WM_URL_DEL, WM_XSL_ACT, WM_XSL_CAN, WM_XSL_TER%s';
begin
     with oZetaProvider.TablaInfo do
     begin
          case eTabla of
              eNoticia:     SetInfo( 'NOTICIA',  'NT_FOLIO, NT_FECHA, NT_HORA, NT_IMAGEN, NT_MEMO, NT_TITULO, NT_USUARIO, NT_AUTOR, NT_CLASIFI,NT_CORTA,NT_SUB_TIT', 'NT_FOLIO' );
              eEvento:      SetInfo( 'EVENTO',   'EV_FOLIO, EV_FEC_INI, EV_FEC_FIN, EV_HORA, EV_IMAGEN, EV_LUGAR, EV_MEMO, EV_TITULO, EV_USUARIO, EV_AUTOR, EV_CLASIFI,EV_URL','EV_FOLIO' );
              eDocument:    Setinfo( 'DOCUMENT', 'DC_FOLIO, DC_FECHA, DC_HORA, DC_RUTA, DC_IMAGEN, DC_TITULO, DC_MEMO, DC_USUARIO,DC_AUTOR, DC_PALABRA, DC_CLASIFI', 'DC_FOLIO');
              eBuzonSug:    Setinfo( 'BUZONSUG', 'BU_FOLIO,BU_FECHA,BU_HORA,BU_MEMO,BU_TITULO,BU_USUARIO,BU_AUTOR,BU_EMAIL', 'BU_FOLIO');
              eUsuarios:    SetInfo( 'USUARIO',  Format( K_CAMPOS_USUARIO, [ '' ] ), 'US_CODIGO' );
              ePagina:      Setinfo( 'PAGINA',   'US_CODIGO,PA_ORDEN,PA_TITULO, PA_COLS', 'US_CODIGO,PA_ORDEN');
              eContiene:    SetInfo( 'CONTIENE', 'US_CODIGO, PA_ORDEN, CT_COLUMNA, CT_ORDEN, CT_TIPO, CT_MOSTRAR, CT_REPORTE, CT_CLASIFI', 'US_CODIGO, PA_ORDEN, CT_COLUMNA, CT_ORDEN' );
              eReportal:    SetInfo( 'REPORTAL', 'RP_FOLIO,RP_TITULO, RP_DESCRIP, RP_FECHA, RP_HORA, RP_IMAGEN, RP_USUARIO, RP_TIPO, RP_CLASIFI, RP_COLS, RP_ROWS', 'RP_FOLIO');
              eRepCols:     SetInfo( 'REP_COLS', 'RP_FOLIO,RC_ORDEN, RC_TITULO, RC_TIPO, RC_ANCHO', 'RP_FOLIO, RC_ORDEN' );
              eRepData:     SetInfo( 'REP_DATA', 'RP_FOLIO,RD_ORDEN, RD_DATO1, RD_DATO2, RD_DATO3, RD_DATO4, RD_DATO5, RD_DATO6, RD_DATO7, RD_DATO8, RD_DATO9, RD_DATO10', 'RP_FOLIO, RD_ORDEN' );
              eAccion:      SetInfo( 'WACCION',  'WA_CODIGO, WA_NOMBRE, WA_DESCRIP', 'WA_CODIGO' );
              eRol:         SetInfo( 'ROL',      'RO_CODIGO, RO_NOMBRE, RO_DESCRIP, RO_NIVEL', 'RO_CODIGO' );
              eModeloR:     SetInfo( 'WMODELO',  Format( K_WMODELO_CAMPOS, [ ', WM_PASOS' ] ), 'WM_CODIGO' );
              eModeloW:     SetInfo( 'WMODELO',  Format( K_WMODELO_CAMPOS, [ '' ] ), 'WM_CODIGO' );
              ePasosModelo:
                           begin
                                SetInfo( 'WMODSTEP', 'WM_CODIGO, WE_ORDEN, WA_CODIGO, WE_NOMBRE, WE_DESCRIP, WE_TIPDEST, WE_USRDEST, WE_TIPNOT1, WE_USRNOT1, WE_TIPNOT2, WE_USRNOT2, WE_TIPNOT3, WE_USRNOT3, WE_XSL_ACT, WE_XSL_CAN, WE_XSL_TER, WE_XSL_TAR,WE_LIMITE, WE_URL_OK, WX_FOLIO', 'WM_CODIGO, WE_ORDEN' );
                                BeforeUpdateRecord := BeforeUpdatePasosModelo;
                           end;
              ekCarruseles: SetInfo( 'KSHOW', 'KW_CODIGO,KW_NOMBRE,KW_SCR_DAT,KW_TIM_DAT,KW_GET_NIP,CM_CODIGO', 'KW_CODIGO' );
              ekCarruselesInfo: SetInfo( 'KSHOWINF', 'KW_CODIGO,KI_ORDEN,KI_TIMEOUT,KI_URL,KI_REFRESH', 'KW_CODIGO,KI_ORDEN' );
              ekMarcosInformacion: SetInfo( 'KSCREEN', 'KS_CODIGO,KS_NOMBRE,KS_PF_SIZE,KS_PF_COLR,KS_PF_ALIN,KS_PF_URL,KS_PB_SIZE,KS_PB_COLR,KS_PB_ALIN,KS_BTN_ALT,KS_BTN_SEP,KS_EST_NOR,KS_EST_SEL,KS_BITMAP', 'KS_CODIGO' );
              ekBotones: SetInfo('KBOTON','KS_CODIGO,KB_ORDEN,KB_TEXTO,KB_BITMAP,KB_ACCION,KB_URL,KB_SCREEN,KB_ALTURA,KB_LUGAR,KB_SEPARA,KB_REPORTE,KB_POS_BIT', 'KS_CODIGO,KB_ORDEN' );
              ekEstilosBotones: SetInfo('KESTILO', 'KE_CODIGO,KE_NOMBRE,KE_COLOR,KE_F_NAME,KE_F_SIZE,KE_F_COLR,KE_F_BOLD,KE_F_ITAL,KE_F_SUBR,KE_BITMAP', 'KE_CODIGO' );
              eNotificacion: SetInfo('WMODNOTI', 'WM_CODIGO, WE_ORDEN, WY_FOLIO, WY_QUIEN, WY_USR_NOT, WY_XSL, WY_CUANDO, WY_TIEMPO, WY_QUE, WY_MODELO, WY_TEXTO, WY_VECES, WY_CADA', 'WM_CODIGO, WE_ORDEN, WY_FOLIO');
              eConexion: SetInfo('WCONEXION', 'WX_FOLIO, WX_NOMBRE, WX_URL, WX_TIP_AUT, WX_USUARIO, WX_PASSWRD, WX_TIP_ENC', 'WX_FOLIO');
          else
              SetInfo( GetNombreTabla( eTabla ), 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO', 'TB_CODIGO' );
          end;
     end;
end;

{$ifdef DOS_CAPAS}
procedure TdmServerComparte.CierraEmpresa;
begin
end;
{$endif}

function TdmServerComparte.GetConcatenatedFilter( const sValue: String ): String;
begin
     Result := sValue;
     if ZetaCommonTools.StrLleno( Result ) then
        Result := ' AND ' + Result;
end;

function TdmServerComparte.SetFiltroGrupos( const iGrupo: Integer; const sPrefix: String = '' ): String;
begin
     if ( iGrupo <> ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION ) then
     begin
          {$ifdef INTERBASE}
          Result := Format( '( %0:sGR_CODIGO in ( select GR_HIJO from GET_DESCENDENCIA( %1:d ) ) )', [ sPrefix, iGrupo ] );
          {$endif}
          {$ifdef MSSQL}
          Result := Format( '( %0:sGR_CODIGO in ( select GR_HIJO from dbo.GET_DESCENDENCIA( %1:d ) ) )', [ sPrefix, iGrupo ] );
          {$endif}
     end;
end;

function TdmServerComparte.GetTabla( Tabla: Integer ): OleVariant;
const
     K_CLAVE_USUARIO = 'US_PASSWRD';
var
   eTabla: eTipoTabla;
begin
     eTabla := eTipoTabla( Tabla );
     try
        case eTabla of
             eTNoticia:
             begin
                  with oZetaProvider do
                  begin
                       Result := OpenSQL( Comparte, GetScript( Q_TIPO_NOTICIAS ), True );
                  end;
             end;
        else
            begin
                 SetTablaInfo( eTabla );
                 with oZetaProvider do
                 begin
                      with TablaInfo do
                      begin
                           case eTabla of
                                eUsuarios: Filtro := 'US_CODIGO <> 0';
                           end;
                      end;
                      Result := GetTabla( Comparte );
                 end;
                 case eTabla of
                      eUsuarios: Result := DecodificaClaves( Result, K_CLAVE_USUARIO );
                 end;
            end;
        end;
     except
           SetOLEVariantToNull( Result );
           raise;
     end;
     SetComplete;
end;

procedure TdmServerComparte.Codifica( const sCampo: String; DeltaDS: TzProviderClientDataSet );
begin
     with DeltaDS do
     begin
          Edit;
          FieldByName( sCampo ).AsString := ZetaServerTools.Encrypt( ZetaServerTools.CampoAsVar( FieldByName( sCampo ) ) );
          Post;
     end;
end;

procedure TdmServerComparte.ClaveUsuarioCodifica(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var
   sMensaje: String;

begin
     if ( UpdateKind in [ ukModify, ukInsert ] ) then
     begin
          with DeltaDS do
          begin
               { *** Validaci�n del Nombre del Usuario *** }
               {
               if not ChecaUsuarios( UpperCase( CampoAsVar( FieldByName( 'US_CORTO' ) ) ),
                                     CampoAsVar( FieldByName( 'US_CODIGO' ) ),
                                     sMensaje ) then
                  DataBaseError( sMensaje );
               }
               { *** Seguridad *** }
               FSeguridadInfo.CambiaPasswordLog := ZetaServerTools.CambiaCampo( FieldByName( K_CLAVE_USUARIO ) );
               Codifica( K_CLAVE_USUARIO, DeltaDS );
               if not FSeguridadInfo.ValidaPasswordLog( ZetaServerTools.CampoAsVar( FieldByName( K_CLAVE_USUARIO ) ),
                                                        ZetaServerTools.CampoAsVar( FieldByName( 'US_CODIGO' ) ),
                                                        sMensaje ) then
                  DataBaseError( sMensaje );
          end;
     end
     else
     begin
          FSeguridadInfo.CambiaPasswordLog := FALSE;
     end;
     Applied := False;
end;

procedure TdmServerComparte.ClaveUsuarioGraba(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
begin
     FSeguridadInfo.GrabaPasswordLog;        // Se Debe Invocar antes la funci�n TSeguridadInfo.ValidaPasswordLog()
end;

function TdmServerComparte.GrabaTabla( Tabla: Integer; Delta: OleVariant; out ErrorCount: Integer): OleVariant;
var
   eTabla: eTipoTabla;
begin
     eTabla := eTipoTabla( Tabla );
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          SetTablaInfo( eTabla );
     end;
     case eTabla of
          eUsuarios:
          begin
               FSeguridadInfo := TSeguridadInfo.Create( oZetaProvider );
               try
                  FSeguridadInfo.PasswordLogBegin;
                  try
                     with oZetaProvider do
                     begin
                          with TablaInfo do
                          begin
                               BeforeUpdateRecord := ClaveUsuarioCodifica;
                               if FSeguridadInfo.UsaPasswordLog then
                                  AfterUpdateRecord := ClaveUsuarioGraba;
                          end;
                          Result := GrabaTabla( Comparte, Delta, ErrorCount );
                     end;
                  finally
                         FSeguridadInfo.PasswordLogEnd;
                  end;
               finally
                      FreeAndNil( FSeguridadInfo );
               end;
          end;
     else
         begin
              with oZetaProvider do
              begin
                   Result := GrabaTabla( Comparte, Delta, ErrorCount );
              end;
         end;
     end;
     SetComplete;
end;

function TdmServerComparte.GetGlobales: OleVariant;
var
   oSuperGlobal: TdmSuperGlobal;
begin
     oSuperGlobal := TdmSuperGlobal.Create;
     try
        try
           with oZetaProvider do
           begin
                EmpresaActiva := Comparte;
           end;
           with oSuperGlobal do
           begin
                Provider := oZetaProvider;
                Result := ProcesaLectura;
           end;
           SetComplete;
        except
              SetAbort;
              raise;
        end;
     finally
            FreeAndNil( oSuperGlobal );
     end;
end;

function TdmServerComparte.GrabaGlobales(Delta: OleVariant;out ErrorCount: Integer): OleVariant;
var
   oSuperGlobal: TdmSuperGlobal;
begin
     oSuperGlobal := TdmSuperGlobal.Create;
     try
        try
           with oZetaProvider do
           begin
                EmpresaActiva := Comparte;
           end;
           with oSuperGlobal do
           begin
                Provider := oZetaProvider;
                ActualizaDiccion := False;
                Result := ProcesaEscritura( Delta );
                ErrorCount := Errores;
           end;
           SetComplete;
        except
              SetAbort;
              raise;
        end;
     finally
            FreeAndNil( oSuperGlobal );
     end;
end;

function TdmServerComparte.GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; out Error: WideString): OleVariant;
var
   Reporte: TdmServerReporteador;
begin
     InitCreator;
     Reporte := TdmServerReporteador.Create( oZetaProvider, oZetaCreator );
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaparamList( Parametros );
        end;
        Result := Reporte.GeneraSQL( Empresa,
                                     AgenteSQL,
                                     Parametros,
                                     Error );
     finally
            FreeAndNil(Reporte);
     end;
     SetComplete;
end;

function TdmServerComparte.EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; out Error: WideString): WordBool;
var
   Reporte: TdmServerReporteador;
begin
     InitCreator;
     Reporte := TdmServerReporteador.Create( oZetaProvider, oZetaCreator );
     try
        Result := Reporte.EvaluaParam( Empresa,
                                       AgenteSQL,
                                       Parametros,
                                       Error );
     finally
            FreeAndNil(Reporte);
     end;
     SetComplete;
end;

function TdmServerComparte.GetComparte: OleVariant;
const
     K_COMPARTE_CODIGO = 'XCOMPARTEY';
begin
     try
        with cdsDatos do
        begin
             with oZetaProvider do
             begin
                  EmpresaActiva := Comparte;
                  Lista := OpenSQL( Comparte, Format( GetScript( Q_COMPANYS_NINGUNA ), [ K_COMPARTE_CODIGO ] ), False );
             end;
             Append;
             FieldByName( 'CM_CODIGO' ).AsString := K_COMPARTE_CODIGO;
             FieldByName( 'CM_NOMBRE' ).AsString := 'Base De Datos Comparte';
             with oZetaProvider do
             begin
                  FieldByName( 'CM_ALIAS' ).AsString := Comparte[ P_ALIAS ];
                  FieldByName( 'CM_USRNAME' ).AsString := Comparte[ P_USER_NAME ];
                  FieldByName( 'CM_PASSWRD' ).AsString := ZetaServerTools.Encrypt( Comparte[ P_PASSWORD ] );
                  FieldByName( 'CM_DATOS' ).AsString := Comparte[ P_DATABASE ];
             end;
             FieldByName( 'CM_NIVEL0' ).AsString := '';
             FieldByName( 'CM_DIGITO' ).AsString := '';
             Post;
             Result := Lista;
        end;
        SetComplete;
     except
           SetAbort;
           raise;
     end;
end;

{ ********************* }
{ ******* Portal ****** }
{ ********************* }

procedure TdmServerComparte.GetFolioTitular;
begin
     with oZetaProvider do
     begin
          InitGlobales;
          {$ifdef ANTES}
          FNoticiaTitular := GetGlobalInteger( K_GLOBAL_TITULAR );
          {$else}
          FNoticiaTitular := 0;
          {$endif}

     end;
end;

function TdmServerComparte.GetNoticias: OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          GetFolioTitular;
          Result := OpenSQL( Comparte, Format( GetScript( Q_NOTICIAS ), [ FNoticiaTitular, K_GLOBAL_SI, K_GLOBAL_NO ] ), True );
     end;
     SetComplete;
end;

procedure TdmServerComparte.ActualizaTitulares(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
var
   lGlobal, lGlobalChanged, lLocal, lLocalChanged: Boolean;
   iNoticia: Integer;
   sClase: String;
begin
     with DeltaDS do
     begin
          lGlobal := ZetaCommonTools.zStrToBool( ZetaServerTools.CampoAsVar( FieldByName( 'NT_MAIN' ) ) );
          lLocal := ZetaCommonTools.zStrToBool( ZetaServerTools.CampoAsVar( FieldByName( 'NT_LOCAL' ) ) );
          iNoticia := ZetaServerTools.CampoAsVar( FieldByName( 'NT_FOLIO' ) );
          sClase := ZetaServerTools.CampoAsVar( FieldByName( 'NT_CLASIFI' ) );
          lGlobalChanged := True;
          lLocalChanged := True;
          case UpdateKind of
               ukModify:
               begin
                    lGlobalChanged := ZetaServerTools.CambiaCampo( FieldByName( 'NT_MAIN' ) );
                    lLocalChanged := ZetaServerTools.CambiaCampo( FieldByName( 'NT_LOCAL' ) );
               end;
          end;
     end;
     with oZetaProvider do
     begin
          if lGlobalChanged then
          begin
               if lGlobal then
               begin
                    {$ifdef ANTES}
                    ExecSQL( Comparte, Format( GetScript( Q_TITULAR_GLOBAL_SET ), [ iNoticia, K_GLOBAL_TITULAR ] ) );
                    {$else}
                    ExecSQL( Comparte, Format( GetScript( Q_TITULAR_GLOBAL_SET ), [ iNoticia, 0 ] ) );
                    {$endif}
               end
               else
               begin
                    if ( iNoticia = FNoticiaTitular ) then
                    begin
                         {$ifdef ANTES}
                         ExecSQL( Comparte, Format( GetScript( Q_TITULAR_GLOBAL_RESET ), [ K_GLOBAL_TITULAR ] ) );
                         {$else}
                         ExecSQL( Comparte, Format( GetScript( Q_TITULAR_GLOBAL_RESET ), [ 0 ] ) );
                         {$endif}
                    end;
               end;
          end;
          if lLocalChanged then
          begin
               if lLocal then
               begin
                    ExecSQL( Comparte, Format( GetScript( Q_TITULAR_LOCAL_SET ), [ iNoticia, sClase ] ) );
               end
               else
               begin
                    ExecSQL( Comparte, Format( GetScript( Q_TITULAR_LOCAL_RESET ), [ sClase, iNoticia ] ) );
               end;
          end;
     end;
end;

function TdmServerComparte.GrabaNoticias(Delta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          GetFolioTitular;
          SetTablaInfo( eNoticia );
          with TablaInfo do
          begin
               AfterUpdateRecord := ActualizaTitulares;
          end;
          Result := GrabaTabla( Comparte, Delta, ErrorCount );
     end;
     SetComplete;
end;

function TdmServerComparte.GetEventos: OleVariant;
begin
     with oZetaProvider do
     begin
          Result := OpenSQL( Comparte, GetScript( Q_EVENTOS ), True );
     end;
     SetComplete;
end;

function TdmServerComparte.GetContenidos(Usuario, Pagina, Columna: Integer; out Paginas: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          Result := OpenSQL( Comparte, Format( GetScript( Q_CONTENIDO_LEE ), [ Usuario, Pagina, Columna, Ord( ecReporte ), Ord( ecGrafica ), Ord( ecTitular ), Ord( ecNoticia ), Ord( ecEvento ) ] ), True );
          Paginas := OpenSQL( Comparte, Format( GetScript( Q_PAGINAS_LEE ), [ Usuario ] ), True );
     end;
     SetComplete;
end;

function TdmServerComparte.GrabaContenidos(Delta: OleVariant; Usuario, Pagina, Columna: Integer; out ErrorCount: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          ExecSQL( Comparte, Format( GetScript( Q_CONTENIDO_BORRA ), [ Usuario, Pagina, Columna ] ) );
          if VarIsNull( Delta ) then
             Result := Null
          else
          begin
               SetTablaInfo( eContiene );
               Result := GrabaTabla( Comparte, Delta, ErrorCount );
          end;
     end;
     SetComplete;
end;

function TdmServerComparte.GrabaPaginas(Delta: OleVariant; Usuario: Integer; var ErrorCount: Integer): OleVariant;
var
   FDataset: TZetaCursor;
   iPaginas: Integer;
   lExiste: Boolean;
   sNombre: String;
   {$ifndef ANTES}
   iOrden, iAnterior: Integer;
   FBorrar: TZetaCursor;
   FCambiar: TZetaCursor;
   FAgregar: TZetaCursor;
   {$endif}
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          {$ifdef ANTES}
          ExecSQL( Comparte, Format( GetScript( Q_PAGINAS_BORRA ), [ Usuario ] ) );
          FContenidoBorra := CreateQuery( Format( GetScript( Q_CONTENIDO_BORRA_TODO ), [ Usuario ] ) );
          try
             FContenidoCambia := CreateQuery( Format( GetScript( Q_CONTENIDO_CAMBIA_TODO ), [ Usuario ] ) );
             try
                SetTablaInfo( ePagina );
                with TablaInfo do
                begin
                     BeforeUpdateRecord := BeforeUpdatePaginas;
                end;
                Result := GrabaTabla( Comparte, Delta, ErrorCount );
             finally
                    FreeAndNil( FContenidoCambia );
             end;
          finally
                 FreeAndNil( FContenidoBorra );
          end;
          {$else}
          FBorrar := CreateQuery( Format( GetScript( Q_PAGINAS_BORRA ), [ Usuario ] ) );
          FCambiar := CreateQuery( Format( GetScript( Q_PAGINAS_CAMBIA ), [ Usuario ] ) );
          FAgregar := CreateQuery( Format( GetScript( Q_PAGINAS_AGREGA ), [ Usuario ] ) );
          try
             cdsDatos.Data := Delta;
             with cdsDatos do
             begin
                  while not Eof do
                  begin
                       iOrden := FieldByName( 'PA_ORDEN' ).AsInteger;
                       iAnterior := FieldByName( 'PA_ANTERIOR' ).AsInteger;
                       EmpiezaTransaccion;
                       try
                          if ( iOrden = K_NULL_PAGINA ) then { Borrar Contenido de Pagina Borrada }
                          begin
                               ParamAsInteger( FBorrar, 'OLD_PA_ORDEN', iAnterior );
                               Ejecuta( FBorrar );
                          end
                          else
                          begin
                               if ( iAnterior = K_NULL_PAGINA ) then { Agregar }
                               begin
                                    ParamAsInteger( FAgregar, 'PA_ORDEN', iOrden );
                                    ParamAsVarChar( FAgregar, 'PA_TITULO', FieldByName( 'PA_TITULO' ).AsString, K_ANCHO_DESCRIPCION );
                                    ParamAsInteger( FAgregar, 'PA_COLS', FieldByName( 'PA_COLS' ).AsInteger );
                                    Ejecuta( FAgregar );
                               end
                               else
                                   if ( iAnterior <> K_NULL_PAGINA ) then  { Cambiar Contenido }
                                   begin
                                        ParamAsInteger( FCambiar, 'PA_ORDEN', iOrden + K_OFFSET );
                                        ParamAsVarChar( FCambiar, 'PA_TITULO', FieldByName( 'PA_TITULO' ).AsString, K_ANCHO_DESCRIPCION );
                                        ParamAsInteger( FCambiar, 'PA_COLS', FieldByName( 'PA_COLS' ).AsInteger );
                                        ParamAsInteger( FCambiar, 'OLD_PA_ORDEN', iAnterior );
                                        Ejecuta( FCambiar );
                                   end;
                          end;
                          TerminaTransaccion( True );
                       except
                             on Error: Exception do
                             begin
                                  RollBackTransaccion;
                                  raise;
                             end;
                       end;
                       Next;
                  end;
             end;
          finally
                 FreeAndNil( FAgregar );
                 FreeAndNil( FCambiar );
                 FreeAndNil( FBorrar );
          end;
          { K_OFFSET es necesario para evitar que haya colisiones al }
          { cambiar los ordenes de dos paginas consecutivas }
          ExecSQL( Comparte, Format( GetScript( Q_PAGINAS_RESET ), [ K_OFFSET, Usuario ] ) );
          {$endif}
          { K_OFFSET es necesario para evitar que haya colisiones al }
          { cambiar los ordenes de dos paginas consecutivas }
          ExecSQL( Comparte, Format( GetScript( Q_CONTENIDO_RESET ), [ K_OFFSET, Usuario ] ) );
          FDataset := CreateQuery;
          try
             PreparaQuery( FDataset, Format( GetScript( Q_PAGINAS_CUENTA ), [ Usuario ] ) );
             with FDataset do
             begin
                  Active := True;
                  iPaginas := Fields[ 0 ].AsInteger;
                  Active := False;
             end;
             PreparaQuery( FDataset, Format( GetScript( Q_USUARIO_EXISTE ), [ Usuario ] ) );
             with FDataset do
             begin
                  Active := True;
                  lExiste := ( Fields[ 0 ].AsInteger > 0 );
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
          if lExiste then
             ExecSQL( Comparte, Format( GetScript( Q_USUARIO_ACTUALIZA ), [ iPaginas, Usuario ] ) )
          else
          begin
               if ( Usuario = 0 ) then
                  sNombre := 'Usuario Default'
               else
                   sNombre := Format( 'Usuario %d', [ Usuario ] );
               ExecSQL( Comparte, Format( GetScript( Q_USUARIO_AGREGA ), [ Usuario, sNombre, iPaginas ] ) )
          end;
     end;
     SetComplete;
end;

{$ifdef ANTES}
procedure TdmServerComparte.BeforeUpdatePaginas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var
   iOrden, iAnterior: Integer;
begin
     with DeltaDS do
     begin
          iOrden := FieldByName( 'PA_ORDEN' ).AsInteger;
          iAnterior := FieldByName( 'PA_ANTERIOR' ).AsInteger;
          if ( iOrden = K_NULL_PAGINA ) then { Borrar Contenido de Pagina Borrada }
          begin
               with oZetaProvider do
               begin
                    EmpiezaTransaccion;
                    try
                       ParamAsInteger( FContenidoBorra, 'PA_ORDEN', iAnterior );
                       Ejecuta( FContenidoBorra );
                       TerminaTransaccion( True );
                    except
                          on Error: Exception do
                          begin
                               RollBackTransaccion;
                               raise;
                          end;
                    end;
               end;
               Applied := True;
          end
          else
              if ( iAnterior <> K_NULL_PAGINA ) and ( iOrden <> iAnterior ) then  { Cambiar Contenido }
              begin
                   { K_OFFSET es necesario para evitar que haya colisiones al }
                   { cambiar los ordenes de dos paginas consecutivas }
                   with oZetaProvider do
                   begin
                        EmpiezaTransaccion;
                        try
                           ParamAsInteger( FContenidoCambia, 'OLD_PA_ORDEN', iAnterior );
                           ParamAsInteger( FContenidoCambia, 'PA_ORDEN', iOrden + K_OFFSET );
                           Ejecuta( FContenidoCambia );
                           TerminaTransaccion( True );
                        except
                              on Error: Exception do
                              begin
                                   RollBackTransaccion;
                                   raise;
                              end;
                        end;
                   end;
              end;
     end;
end;
{$endif}

function TdmServerComparte.GetReportalMetadata( out RepData: OleVariant ): OleVariant;
begin
     with oZetaProvider do
     begin
          Result := OpenSQL( Comparte, GetScript( Q_REP_COLS_METADATA ), False );
          RepData := OpenSQL( Comparte, GetScript( Q_REP_DATA_METADATA ), False );
     end;
     SetComplete;
end;

function TdmServerComparte.GrabaReportal(Folio: Integer; var Columnas, Datos: OleVariant): Integer;
var
   iColErrors, iDataErrors: Integer;
   FDataset: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          ExecSQL( Comparte, Format( GetScript( Q_REP_BORRA_COLUMNAS ), [ Folio ] ) );
          ExecSQL( Comparte, Format( GetScript( Q_REP_BORRA_DATOS ), [ Folio ] ) );
          if ( ( Columnas = Null ) or ( Datos = Null ) ) then
          begin
               Result := 0;
          end
          else
          begin
               SetTablaInfo( eRepCols );
               Columnas := GrabaTabla( Comparte, Columnas, iColErrors );
               SetTablaInfo( eRepData );
               Datos := GrabaTabla( Comparte, Datos, iDataErrors );
               Result := ( K_ERROR_OFFSET * iColErrors + iDataErrors );
          end;
          FDataset := CreateQuery;
          try
             PreparaQuery( FDataset, Format( GetScript( Q_REP_CUENTA_COLUMNAS ), [ Folio ] ) );
             with FDataset do
             begin
                  Active := True;
                  iColErrors := Fields[ 0 ].AsInteger;
                  Active := False;
             end;
             PreparaQuery( FDataset, Format( GetScript( Q_REP_CUENTA_RENGLONES ), [ Folio ] ) );
             with FDataset do
             begin
                  Active := True;
                  iDataErrors := Fields[ 0 ].AsInteger;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
          ExecSQL( Comparte, Format( GetScript( Q_REP_UPDATE_REPORTE ), [ ZetaCommonTools.DateToStrSQL( Now ), FormatDateTime( 'hhnn', Now ), iColErrors, iDataErrors, Folio ] ) );
     end;
     SetComplete;
end;


{ ********************* }
{ *** Fin de Portal *** }
{ ********************* }

{ ********************* }
{ ****** WorkFlow ***** }
{ ********************* }

{ *** Usuarios *** }

function TdmServerComparte.DecodificaClaves( Datos: OleVariant; const sCampo: String ): OleVariant;
begin
     with cdsDatos do
     begin
          Active := False;
          Data := Datos;
          while not Eof do
          begin
               try
                  Edit;
                  with FieldByName( sCampo ) do
                  begin
                       AsString := ZetaServerTools.Decrypt( AsString );
                  end;
                  Post;
               except
                     on Error: Exception do
                     begin
                          Cancel;
                     end;
               end;
               Next;
          end;
          MergeChangeLog;
          Result := Data;
          Active := False;
     end;
end;

function TdmServerComparte.GetUsuarios(Grupo: Integer): OleVariant;
var
   sFiltro: String;
begin
     sFiltro := SetFiltroGrupos( Grupo, K_USUARIOS_PREFIX );
     if ZetaCommonTools.StrLleno( sFiltro ) then
        sFiltro := Format( 'and ( %s )', [ sFiltro ] );
     with oZetaProvider do
     begin
          Result := DecodificaClaves( OpenSQL( Comparte, Format( GetScript( Q_SIST_USUARIOS ), [ sFiltro ] ), True ), K_CLAVE_USUARIO );
     end;
     SetComplete;
end;

function TdmServerComparte.GrabaUsuarios(Usuario: Integer; Delta, Roles: OleVariant; out ErrorCount: Integer): OleVariant;
var
   FAgregar: TZetaCursor;
begin
     if VarIsNull( Delta ) then
     begin
          ErrorCount := 0;
          Result := Null;
     end
     else
         with oZetaProvider do
         begin
              SetTablaInfo( eUsuarios );
              Result := GrabaTabla( Comparte, Delta, ErrorCount );
         end;
     if ( ErrorCount <= 0 ) then
     begin
          with oZetaProvider do
          begin
               EmpresaActiva := Comparte;
               FAgregar := CreateQuery( GetScript( Q_USER_ROL_INSERT ) );
               try
                  EmpiezaTransaccion;
                  try
                     ExecSQL( Comparte, Format( GetScript( Q_USUARIO_ROLES_DELETE ), [ Usuario ] ) );
                     with cdsDatos do
                     begin
                          Lista := Roles;
                          First;
                          while not Eof do
                          begin
                               ParamAsString( FAgregar, 'RO_CODIGO', FieldByName( 'RO_CODIGO' ).AsString );
                               ParamAsInteger( FAgregar, 'US_CODIGO', Usuario );
                               Ejecuta( FAgregar );
                               Next;
                          end;
                     end;
                     TerminaTransaccion( TRUE );
                  except
                        on Error: Exception do
                        begin
                             TerminaTransaccion( FALSE );
                             raise;
                        end;
                  end;
               finally
                      FreeAndNil( FAgregar );
               end;
          end;
     end;
     SetComplete;
end;

{ *** Acciones *** }

function TdmServerComparte.GetAcciones: OleVariant;
begin
     Result := GetTabla( Ord( eAccion ) );
     SetComplete;
end;

function TdmServerComparte.GrabaAcciones(Delta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     Result := GrabaTabla( Ord( eAccion ), Delta, ErrorCount );
     SetComplete;
end;

{ *** Roles *** }

function TdmServerComparte.GetRoles: OleVariant;
begin
     Result := GetTabla( Ord( eRol ) );
     SetComplete;
end;

function TdmServerComparte.GrabaRoles(const Rol: WideString; Delta, Usuarios, Modelos: OleVariant; out ErrorCount: Integer): OleVariant;
var
   FAgregar: TZetaCursor;
begin
     if VarIsNull( Delta ) then
     begin
          ErrorCount := 0;
          Result := Null;
     end
     else
         Result := GrabaTabla( Ord( eRol ), Delta, ErrorCount );
     if ( ErrorCount <= 0 ) then
     begin
          with oZetaProvider do
          begin
               EmpresaActiva := Comparte;
               { Grabar Usuarios del Rol }
               FAgregar := CreateQuery( GetScript( Q_USER_ROL_INSERT ) );
               try
                  EmpiezaTransaccion;
                  try
                     ExecSQL( Comparte, Format( GetScript( Q_ROL_USUARIOS_DELETE ), [ Rol ] ) );
                     with cdsDatos do
                     begin
                          Lista := Usuarios;
                          First;
                          while not Eof do
                          begin
                               ParamAsString( FAgregar, 'RO_CODIGO', Rol );
                               ParamAsInteger( FAgregar, 'US_CODIGO', FieldByName( 'US_CODIGO' ).AsInteger );
                               Ejecuta( FAgregar );
                               Next;
                          end;
                     end;
                     TerminaTransaccion( TRUE );
                  except
                        on Error: Exception do
                        begin
                             TerminaTransaccion( FALSE );
                             raise;
                        end;
                  end;
               finally
                      FreeAndNil( FAgregar );
               end;
               { Grabar Modelos del Rol }
               FAgregar := CreateQuery( GetScript( Q_WMOD_ROL_INSERT ) );
               try
                  EmpiezaTransaccion;
                  try
                     ExecSQL( Comparte, Format( GetScript( Q_ROL_MODELOS_DELETE ), [ Rol ] ) );
                     with cdsDatos do
                     begin
                          Lista := Modelos;
                          First;
                          while not Eof do
                          begin
                               ParamAsString( FAgregar, 'RO_CODIGO', Rol );
                               ParamAsString( FAgregar, 'WM_CODIGO', FieldByName( 'WM_CODIGO' ).AsString );
                               Ejecuta( FAgregar );
                               Next;
                          end;
                     end;
                     TerminaTransaccion( TRUE );
                  except
                        on Error: Exception do
                        begin
                             TerminaTransaccion( FALSE );
                             raise;
                        end;
                  end;
               finally
                      FreeAndNil( FAgregar );
               end;
          end;
     end;
     SetComplete;
end;

{ *** Modelos *** }

function TdmServerComparte.GetModelos: OleVariant;
begin
     Result := GetTabla( Ord( eModeloR ) );
     SetComplete;
end;

function TdmServerComparte.GetModeloRoles(const Modelo: WideString): OleVariant;
begin
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Comparte;
             Result := OpenSQL( Comparte, Format( GetScript( Q_MODELO_ROLES ), [ Modelo ] ), True );
        end;
        SetComplete;
     except
           SetAbort;
           raise;
     end;
end;

function TdmServerComparte.GrabaModelos(const Codigo: WideString; Delta, Roles: OleVariant; out ErrorCount: Integer): OleVariant;
var
   FAgregar: TZetaCursor;
begin
     if VarIsNull( Delta ) then
     begin
          ErrorCount := 0;
          Result := Null;
     end
     else
         Result := GrabaTabla( Ord( eModeloW ), Delta, ErrorCount );
     if ( ErrorCount <= 0 ) then
     begin
          with oZetaProvider do
          begin
               EmpresaActiva := Comparte;
               { Grabar Roles del Modelo }
               FAgregar := CreateQuery( GetScript( Q_WMOD_ROL_INSERT ) );
               try
                  EmpiezaTransaccion;
                  try
                     ExecSQL( Comparte, Format( GetScript( Q_MODELO_ROLES_DELETE ), [ Codigo ] ) );
                     with cdsDatos do
                     begin
                          Lista := Roles;
                          First;
                          while not Eof do
                          begin
                               ParamAsString( FAgregar, 'WM_CODIGO', Codigo );
                               ParamAsString( FAgregar, 'RO_CODIGO', FieldByName( 'RO_CODIGO' ).AsString );
                               Ejecuta( FAgregar );
                               Next;
                          end;
                     end;
                     TerminaTransaccion( TRUE );
                  except
                        on Error: Exception do
                        begin
                             TerminaTransaccion( FALSE );
                             raise;
                        end;
                  end;
               finally
                      FreeAndNil( FAgregar );
               end;
          end;
     end;
     SetComplete;
end;

{ *** Pasos De Modelo *** }

function TdmServerComparte.GetPasosModeloMaxOrder( const Modelo: WideString ): Integer;
var
   FAgregar: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          FAgregar := CreateQuery( Format( GetScript( Q_PASO_MODELO_MAX_ORDEN ), [ Modelo ] ) );
          try
             with FAgregar do
             begin
                  Active := True;
                  if Eof then
                     Result := 0
                  else
                      Result := FieldByName( 'MAXIMO' ).AsInteger;
                  Active := False;
             end;
          finally
                 FreeAndNil( FAgregar );
          end;
     end;
end;

function TdmServerComparte.GetPasosModelo(const Modelo: WideString; out MaxOrder: Integer): OleVariant;
begin
     try
        SetTablaInfo( ePasosModelo );
        with oZetaProvider do
        begin
             with TablaInfo do
             begin
                  Filtro := Format( '( WM_CODIGO = ''%s'' )', [ Modelo ] );
             end;
             Result := GetTabla( Comparte );
        end;
        MaxOrder := GetPasosModeloMaxOrder( Modelo );
        SetComplete;
     except
           SetOLEVariantToNull( Result );
           SetAbort;
           raise;
     end;
end;

function TdmServerComparte.GrabaPasosModelo(const Modelo: WideString; Delta, Notificaciones: OleVariant; out MaxOrder, ErrorCount: Integer;
  out NotiResult: OleVariant): OleVariant;
begin
     Result := null;
     NotiResult := null;
     if ( NOT VarIsNull(Delta) ) then
     begin
          with oZetaProvider do
          begin
               EmpresaActiva := Comparte;
               SetTablaInfo( ePasosModelo );
               Result := GrabaTabla( Comparte, Delta, ErrorCount );
          end;
          MaxOrder := GetPasosModeloMaxOrder( Modelo );
     end;
     if ( NOT VarIsNull (Notificaciones) ) then
        NotiResult := GrabaTabla( Ord( eNotificacion ), Notificaciones, ErrorCount );
     SetComplete;
end;

function TdmServerComparte.PasoModeloCambiar(const Modelo: WideString; OrdenActual, OrdenNuevo: Integer): Integer;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             ExecSQL( Comparte, Format( GetScript( Q_PASO_MODELO_CAMBIAR ), [ Modelo, OrdenActual, OrdenNuevo ] ) );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     RollBackTransaccion;
                     raise;
                end;
          end;
     end;
     SetComplete;
end;

procedure TdmServerComparte.BeforeUpdatePasosModelo(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     Case UpdateKind of
          ukDelete:
          begin
               oZetaProvider.ExecSQL( oZetaProvider.EmpresaActiva, Format( GetScript( Q_PASO_MODELO_BORRAR ), [ EntreComillas( DeltaDs.FieldByName('WM_CODIGO').AsString ), DeltaDS.FieldByName('WE_ORDEN').AsInteger]  ) );
               Applied := TRUE;
          end;
     end;
end;

function TdmServerComparte.GetCorreos(Parametros: OleVariant): OleVariant;
const
     K_CORREO_ENVIADO = '( C.WO_ENVIADO = ''%s'' )';
     K_CORREO_REMITENTE = '( C.WO_FROM_NA like ''%s'' )';
     K_CORREO_TEMA = '( C.WO_SUBJECT like ''%s'' )';
     K_CORREO_FECHA_IN = '( C.WO_FEC_IN between ''%s'' and ''%s'' )';
     K_CORREO_FECHA_OUT = '( C.WO_FEC_OUT between ''%s'' and ''%s'' )';
var
   sFiltro, sRemitente, sTema, sEnviado: String;
   lEnviado, lFechaGeneracion: Boolean;
   dInicio, dFinal: TDate;
begin
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Comparte;
             AsignaParamList( Parametros );
             with ParamList do
             begin
                  dInicio := ParamByName( 'Inicio' ).AsDate;
                  dFinal := GetAllDay( ParamByName( 'Final' ).AsDate );
                  lFechaGeneracion := ParamByName( 'FechaGeneracion' ).AsBoolean;
                  sRemitente := ParamByName( 'Remitente' ).AsString;
                  sTema := ParamByName( 'Tema' ).AsString;
                  lEnviado := ParamByName( 'FiltrarStatus' ).AsBoolean;
                  sEnviado := ZetaCommonTools.zBoolToStr( ParamByName( 'Enviado' ).AsBoolean );
             end;
             if lFechaGeneracion then
                sFiltro := K_CORREO_FECHA_IN
             else
                 sFiltro := K_CORREO_FECHA_OUT;
             sFiltro := Format( sFiltro, [ DateTimeToStrSQL( dInicio ), DateTimeToStrSQL( dFinal ) ] );
             if ZetaCommonTools.StrLleno( sRemitente ) then
                sFiltro := ZetaCommonTools.ConcatFiltros( sFiltro, Format( K_CORREO_REMITENTE, [ '%' + sRemitente + '%' ] ) );
             if ZetaCommonTools.StrLleno( sTema ) then
                sFiltro := ZetaCommonTools.ConcatFiltros( sFiltro, Format( K_CORREO_TEMA, [ '%' + sTema + '%' ] ) );
             if lEnviado then
                sFiltro := ZetaCommonTools.ConcatFiltros( sFiltro, Format( K_CORREO_ENVIADO, [ sEnviado ] ) );
             Result := OpenSQL( Comparte, Format( GetScript( Q_CORREOS ), [ K_MAX_RECORDS, sFiltro ] ), True );
        end;
        SetComplete;
     except
           SetAbort;
           raise;
     end;
end;

function TdmServerComparte.GetProcesos(Parametros: OleVariant): OleVariant;
const
     K_STATUS = '( WP_STATUS = %d )';
     K_MODELO = '( WM_CODIGO = ''%s'' )';
     K_FOLIO_INICIAL = '( WP_FOLIO >= %d )';
     K_FOLIO_FINAL = '( WP_FOLIO <= %d )';
     K_SOLICITANTE = '( WP_USR_INI = %d )';
     K_RESPONSABLE = '( WT_USR_DES = %d )';
var
   sFiltro, sModelo: String;
   lPorModelo, lPorStatus: Boolean;
   eStatus: eWFProcesoStatus;
   iInicial, iFinal, iResponsable, iSolicitante: Integer;
   dInicio, dFinal: TDate;
begin
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Comparte;
             AsignaParamList( Parametros );
             with ParamList do
             begin
                  iInicial := ParamByName( 'FolioInicial' ).AsInteger;
                  iFinal := ParamByName( 'FolioFinal' ).AsInteger;
                  dInicio := ParamByName( 'InicioMinimo' ).AsDate;
                  dFinal := GetAllDay( ParamByName( 'InicioMaximo' ).AsDate );
                  iResponsable := ParamByName( 'Responsable' ).AsInteger;
                  iSolicitante := ParamByName( 'Solicitante' ).AsInteger;
                  lPorModelo := ParamByName( 'FiltrarModelo' ).AsBoolean;
                  lPorStatus := ParamByName( 'FiltrarStatus' ).AsBoolean;
                  sModelo := ParamByName( 'Modelo' ).AsString;
                  eStatus := eWFProcesoStatus( ParamByName( 'Status' ).AsInteger );
             end;
             sFiltro := VACIO;
             if ( iInicial > 0 ) then
                sFiltro := ZetaCommonTools.ConcatFiltros( sFiltro, Format( K_FOLIO_INICIAL, [ iInicial ] ) );
             if ( iFinal > 0 ) then
                sFiltro := ZetaCommonTools.ConcatFiltros( sFiltro, Format( K_FOLIO_FINAL, [ iFinal ] ) );
             if ( iSolicitante > 0 ) then
                sFiltro := ZetaCommonTools.ConcatFiltros( sFiltro, Format( K_SOLICITANTE, [ iSolicitante ] ) );
             if ( iResponsable > 0 ) then
                sFiltro := ZetaCommonTools.ConcatFiltros( sFiltro, Format( K_RESPONSABLE, [ iResponsable ] ) );
             if lPorStatus then
                sFiltro := ZetaCommonTools.ConcatFiltros( sFiltro, Format( K_STATUS, [ Ord( eStatus ) ] ) );
             if lPorModelo then
                sFiltro := ZetaCommonTools.ConcatFiltros( sFiltro, Format( K_MODELO, [ sModelo ] ) );
             Result := OpenSQL( Comparte, Format( GetScript( Q_PROCESOS ), [ K_MAX_RECORDS, DateTimeToStrSQL( dInicio ), DateTimeToStrSQL( dFinal ), GetConcatenatedFilter( sFiltro ) ] ), True );
        end;
        SetComplete;
     except
           SetAbort;
           raise;
     end;
end;

function TdmServerComparte.GetTareas(Parametros: OleVariant): OleVariant;
const
     K_STATUS = '( WT_STATUS = %d )';
     K_MODELO = '( WM_CODIGO = ''%s'' )';
     K_FOLIO_INICIAL = '( WP_FOLIO >= %d )';
     K_FOLIO_FINAL = '( WP_FOLIO <= %d )';
     K_SOLICITANTE = '( WP_USR_INI = %d )';
     K_RESPONSABLE = '( WT_USR_DES = %d )';
var
   sFiltro, sModelo: String;
   lPorModelo, lPorStatus: Boolean;
   eStatus: eWFTareaStatus;
   iInicial, iFinal, iResponsable, iSolicitante: Integer;
   dInicio, dFinal: TDate;
begin
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Comparte;
             AsignaParamList( Parametros );
             with ParamList do
             begin
                  iInicial := ParamByName( 'FolioInicial' ).AsInteger;
                  iFinal := ParamByName( 'FolioFinal' ).AsInteger;
                  dInicio := ParamByName( 'InicioMinimo' ).AsDate;
                  dFinal := GetAllDay( ParamByName( 'InicioMaximo' ).AsDate );
                  iResponsable := ParamByName( 'Responsable' ).AsInteger;
                  iSolicitante := ParamByName( 'Solicitante' ).AsInteger;
                  lPorModelo := ParamByName( 'FiltrarModelo' ).AsBoolean;
                  lPorStatus := ParamByName( 'FiltrarStatus' ).AsBoolean;
                  sModelo := ParamByName( 'Modelo' ).AsString;
                  eStatus := eWFTareaStatus( ParamByName( 'Status' ).AsInteger );
             end;
             sFiltro := VACIO;
             if ( iInicial > 0 ) then
                sFiltro := ZetaCommonTools.ConcatFiltros( sFiltro, Format( K_FOLIO_INICIAL, [ iInicial ] ) );
             if ( iFinal > 0 ) then
                sFiltro := ZetaCommonTools.ConcatFiltros( sFiltro, Format( K_FOLIO_FINAL, [ iFinal ] ) );
             if ( iSolicitante > 0 ) then
                sFiltro := ZetaCommonTools.ConcatFiltros( sFiltro, Format( K_SOLICITANTE, [ iSolicitante ] ) );
             if ( iResponsable > 0 ) then
                sFiltro := ZetaCommonTools.ConcatFiltros( sFiltro, Format( K_RESPONSABLE, [ iResponsable ] ) );
             if lPorStatus then
                sFiltro := ZetaCommonTools.ConcatFiltros( sFiltro, Format( K_STATUS, [ Ord( eStatus ) ] ) );
             if lPorModelo then
                sFiltro := ZetaCommonTools.ConcatFiltros( sFiltro, Format( K_MODELO, [ sModelo ] ) );
             Result := OpenSQL( Comparte, Format( GetScript( Q_TAREAS ), [ K_MAX_RECORDS, DateTimeToStrSQL( dInicio ), DateTimeToStrSQL( dFinal ), GetConcatenatedFilter( sFiltro ) ] ), True );
        end;
        SetComplete;
     except
           SetAbort;
           raise;
     end;
end;

function TdmServerComparte.GetRolesUsuario(Usuario: Integer): OleVariant;
begin
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Comparte;
             Result := OpenSQL( Comparte, Format( GetScript( Q_USUARIO_ROLES ), [ Usuario ] ), True );
        end;
        SetComplete;
     except
           SetAbort;
           raise;
     end;
end;

function TdmServerComparte.GetUsuariosRol(const Rol: WideString; out Modelos: OleVariant): OleVariant;
begin
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Comparte;
             Result := OpenSQL( Comparte, Format( GetScript( Q_ROL_USUARIOS ), [ Rol ] ), True );
             Modelos := OpenSQL( Comparte, Format( GetScript( Q_ROL_MODELOS ), [ Rol ] ), True );
        end;
        SetComplete;
     except
           SetAbort;
           raise;
     end;
end;

function TdmServerComparte.GetDatosProceso(Folio: Integer; out Pasos, Correos, Archivos, Mensajes: OleVariant): OleVariant;
const
     K_CORREO_FOLIO = '( C.WP_FOLIO = %d )';
begin
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Comparte;
             Pasos := OpenSQL( Comparte, Format( GetScript( Q_PROCESO_PASOS ), [ Folio ] ), True );
             Correos := OpenSQL( Comparte, Format( GetScript( Q_CORREOS ), [ K_MAX_RECORDS, Format( K_CORREO_FOLIO, [ Folio ] ) ] ), True );
             Archivos := OpenSQL( Comparte, Format( GetScript( Q_PROCESO_ARCHIVOS ), [ Folio ] ), True );
             Mensajes := OpenSQL( Comparte, Format( GetScript( Q_PROCESO_MENSAJES ), [ Folio ] ), True );
             SetOLEVariantToNull( Result );
        end;
        SetComplete;
     except
           SetAbort;
           raise;
     end;
end;

function TdmServerComparte.ProcesoCambiaStatus( const Script: eSQLScript;
                                                const eProceso: eWFProcesoStatus;
                                                const eBitacora: eClaseBitacora;
                                                const iProceso: integer;
                                                const sNotas: string;
                                                var Msg: WideString ): integer;
 const
      K_BITACORA = 'Proceso %d ' + CR_LF +
                   'Cambi� su Status de %s a %s' + CR_LF +
                   'Usuario que lo Modific� : %d' + CR_LF +
                   'Comentarios: %s';

 var
    sStatusInicial, sStatusFinal: string;
begin
     Result := K_INT_FALSE;
     Msg := VACIO;
     sStatusInicial := VACIO;
     sStatusFinal := VACIO;

     if ServiceManagerIsWorking( Msg ) then
     begin
          try
             with oZetaProvider do
             begin
                  EmpresaActiva := Comparte;
                  EmpiezaTransaccion;
                  try
                     with ParamList do
                          ExecSQL( Comparte, Format( GetScript( Script ), [ iProceso, EntreComillas( sNotas ) ]  ) );

                     TerminaTransaccion( TRUE );

                     with ParamList do
                     begin
                          sStatusInicial := ObtieneElemento( lfWFProcesoStatus, ParamList.ParamByName('StatusActualProceso').AsInteger );
                          sStatusFinal := ObtieneElemento( lfWFProcesoStatus, ord(eProceso) );

                          EscribeBitacora( tbNormal, eBitacora, 0, Date,
                                           Format( 'Proceso %d: Cambi� Status a %s', [ iProceso, sStatusFinal ] ),
                                           Format( K_BITACORA, [iProceso, sStatusInicial, sStatusFinal, ParamByName('Usuario').AsInteger, sNotas] ) );
                     end;

                     Result := K_INT_TRUE;
                  except
                        on Error: Exception do
                        begin
                             RollbackTransaccion;
                             Msg := Error.Message;
                             EscribeBitacora( tbError, eBitacora, 0, Date,
                                              Format( 'Proceso %d: Error al tratar de cambiar el status', [ iProceso ] ),
                                              Format( 'Mensaje de Error: %s' + CR_LF + K_BITACORA, [ Msg, iProceso, sStatusInicial, sStatusFinal, ParamList.ParamByName('Usuario').AsInteger, sNotas] ) );

                        end;
                  end;
             end;
             SetComplete;
          except
                SetAbort;
                raise;
          end;
     end
end;

function TdmServerComparte.ProcesoCambiaStatus( const Script: eSQLScript;
                                                const eProceso: eWFProcesoStatus;
                                                const eBitacora: eClaseBitacora;
                                                Parametros : OleVariant;
                                                var Msg: WideString ): integer;
begin
     with oZetaProvider do
     begin
          AsignaParamList( Parametros );
          with ParamList do
               Result := ProcesoCambiaStatus( Script,
                                              eProceso,
                                              eBitacora,
                                              ParamByName('Proceso').AsInteger,
                                              ParamByName('Notas').AsString,
                                              Msg );

     end;
end;

function TdmServerComparte.ProcesoCancelar(Parametros: OleVariant; out Msg: WideString): Integer;
begin
     Result := ProcesoCambiaStatus( Q_PROCESO_CANCELAR, prCancelado, clbProcesoCancelar, Parametros, Msg );
end;

function TdmServerComparte.ProcesoReiniciar(Parametros: OleVariant; out Msg: WideString): Integer;
begin
     Result := ProcesoCambiaStatus( Q_PROCESO_REINICIAR, prActivo, clbProcesoReiniciar, Parametros, Msg );
end;

function TdmServerComparte.ProcesoSuspender(Parametros: OleVariant; out Msg: WideString): Integer;
begin
     Result := ProcesoCambiaStatus( Q_PROCESO_SUSPENDER, prSuspendido, clbProcesoSuspender, Parametros, Msg );
end;

function TdmServerComparte.ModeloCambiaStatus( const eModelo: eWFStatusModelo;
                                               const eBitacora: eClaseBitacora;
                                               const sStatusProceso: string;
                                               const Script: eSQLScript;
                                               Parametros: OleVariant;
                                               var Msg: WideString;
                                               const lCambiaProcesos: Boolean = TRUE ): Integer;

 const
      K_BITACORA = 'Modelo %s ' + CR_LF +
                   'Cambi� su Status de %s a %s' + CR_LF +
                   'Usuario que lo Modific� : %d' + CR_LF +
                   'Procesos afectados: %d'+ CR_LF +
                   'Comentarios: %s';
 var sStatusInicial, sStatusFinal: string;
     eProceso: eWFProcesoStatus;
     eBitacoraProceso: eClaseBitacora;
     iProcesos: integer;
begin
     Result := K_INT_FALSE;
     Msg := VACIO;
     sStatusInicial := VACIO;
     sStatusFinal :=  VACIO;
     iProcesos := 0;

     if ServiceManagerIsWorking( Msg ) then
     begin
          try
             with oZetaProvider do
             begin
                  EmpresaActiva := Comparte;
                  AsignaParamList( Parametros );
                  CargaDatosModelo;

                  if lCambiaProcesos then
                  begin
                       cdsDatos.Data := OpenSQL( EmpresaActiva, Format( GetScript( Q_PROCESOS_MODELO ), [ Comillas( FModelo ), sStatusProceso ] ), TRUE );
                       iProcesos := cdsDatos.RecordCount;

                       with cdsDatos do
                       begin
                            {$ifdef CAROLINA}
                            SaveToFile('d:\temp\datos.cds');
                            {$ENDIF}
                            while NOT EOF do
                            begin
                                 case eModelo of
                                      smwActivo:
                                      begin
                                           eProceso := prActivo;
                                           eBitacoraProceso := clbProcesoReiniciar;
                                      end;
                                      smwSuspendido:
                                      begin
                                           eProceso := prSuspendido;
                                           eBitacoraProceso := clbProcesoSuspender;
                                      end;
                                      smwCancelado:
                                      begin
                                           eProceso := prCancelado;
                                           eBitacoraProceso := clbProcesoCancelar;
                                      end;
                                      else
                                      begin
                                           eProceso := prInactivo;
                                           eBitacoraProceso := clbNinguno;
                                      end;
                                 end;

                                 ParamList.AddInteger( 'StatusActualProceso', FieldByName('WP_STATUS').AsInteger );
                                 ProcesoCambiaStatus( Script, eProceso, eBitacoraProceso, FieldByName('WP_FOLIO').AsInteger, CR_LF + 'El status del proceso fue cambiado a trav�s de su Modelo', Msg );
                                 Next;
                            end;
                       end;
                  end;

                  EmpiezaTransaccion;
                  try
                     ExecSQL( Comparte, Format( GetScript( Q_MODELO_CAMBIA_STATUS ), [ Ord( eModelo ), Comillas( FModelo ) ]  ) );
                     TerminaTransaccion( TRUE );

                     with ParamList do
                     begin
                          sStatusInicial := ObtieneElemento( lfWFStatusModelo, ParamByName('StatusActualModelo').AsInteger );
                          sStatusFinal := ObtieneElemento( lfWFStatusModelo, Ord(eModelo) );

                          EscribeBitacora( tbNormal, eBitacora, 0, Date,
                                           Format( 'Modelo %s: Cambi� Status a %s', [ FModelo, sStatusFinal ] ),
                                           Format( K_BITACORA, [FModelo, sStatusInicial, sStatusFinal, ParamByName('Usuario').AsInteger, iProcesos, ParamByName('Notas').AsString ] ) );
                     end;
                     
                     Result := K_INT_TRUE;
                     SetComplete;
                  except
                        on Error: Exception do
                        begin
                             RollbackTransaccion;
                             Msg := Error.Message;
                             with ParamList do
                                  EscribeBitacora( tbError, eBitacora, 0, Date,
                                                   Format( 'Modelo %s: Error al tratar de cambiar el status', [ FModelo ] ),
                                                   Format( 'Mensaje de Error: %s' + CR_LF + K_BITACORA, [ Msg, FModelo, sStatusInicial, sStatusFinal, ParamByName('Usuario').AsInteger, ParamByName('Notas').AsString] ) );

                        end;
                  end;
             end;
          except
                SetAbort;
                raise;
          end;
     end;
end;

function TdmServerComparte.ModeloCancelar(Parametros: OleVariant; out Msg: WideString): Integer;
begin
     Result := ModeloCambiaStatus( smwCancelado, clbModeloCancelar, IntToStr(Ord(prActivo))+','+IntToStr(Ord(prSuspendido)), Q_PROCESO_CANCELAR, Parametros, Msg );
end;

function TdmServerComparte.ModeloReiniciar(Parametros: OleVariant; out Msg: WideString): Integer;
begin
     Result := ModeloCambiaStatus( smwActivo, clbModeloReiniciar, IntToStr(Ord(prSuspendido)), Q_PROCESO_REINICIAR, Parametros, Msg );
end;

function TdmServerComparte.ModeloSuspender(Parametros: OleVariant; out Msg: WideString): Integer;
begin
     Result := ModeloCambiaStatus( smwSuspendido, clbModeloSuspender, IntToStr(Ord(prActivo)), Q_PROCESO_SUSPENDER, Parametros, Msg );
end;

function TdmServerComparte.ModeloReactivar(Parametros: OleVariant; out Msg: WideString): Integer;
begin
     Result := ModeloCambiaStatus( smwActivo, clbModeloReactivar, VACIO, Q_PROCESO_REINICIAR, Parametros, Msg, FALSE );
end;

function TdmServerComparte.TareaReasignar(Parametros: OleVariant; out Msg: WideString): Integer;
var
   iUsuario: Integer;
   GUID, sNotas: string;
   FDataSet : TZetaCursor;
begin
     Result := K_INT_FALSE;
     Msg := VACIO;
     if ServiceManagerIsWorking( Msg ) then
     begin
          try
             with oZetaProvider do
             begin
                  EmpresaActiva := Comparte;
                  AsignaParamList( Parametros );
                  //CargaDatosProceso;
                  with ParamList do
                  begin
                       GUID := ParamByName('GUID').AsString;
                       iUsuario := ParamByName('UsuarioReasignar').AsInteger;
                       sNotas := ParamByName('NotasReasignar').AsString;
                  end;
                  FDataSet := CreateQuery( GetScript( Q_TAREA_REASIGNAR ) );
                  try
                      EmpiezaTransaccion;
                      try
                         ParamAsString( FDataset, 'Tarea', GUID );
                         ParamAsInteger( FDataset, 'Destino', iUsuario );
                         ParamAsString( FDataset, 'Notas', sNotas );
                         Ejecuta( FDataSet );
                         TerminaTransaccion( TRUE );
                         Result := K_INT_TRUE;
                      except
                            on Error: Exception do
                            begin
                                 RollbackTransaccion;
                                 Msg := Error.Message;
                            end;
                      end;
                  finally
                    FreeAndNil(FDataSet);
                  end;
             end;
             SetComplete;
          except
                SetAbort;
                raise;
          end;
     end;
end;

function TdmServerComparte.GetReasignarTareaUsuarios( Parametros: OleVariant): OleVariant;
var
   iUsuario: Integer;
   GUID: string;
begin
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Comparte;
             AsignaParamList( Parametros );
             with ParamList do
             begin
                  GUID := ParamByName('GUID').AsString;
                  iUsuario := ParamByName('Usuario').AsInteger;
             end;

             Result := OpenSQL( Comparte, Format( GetScript( Q_REASIGNAR_TAREA_USUARIOS ), [ Comillas( GUID ), iUsuario ] ), True );
        end;
        SetComplete;
     except
           SetAbort;
           raise;
     end;
end;

function TdmServerComparte.ServiciosStatus(Parametros: OleVariant): OleVariant;
 var
    oParametros: TZetaParams;
    sMensaje: WideString;
    iStatus: integer;
begin
     oParametros := TZetaParams.Create;
     try
        with oParametros do
        begin
             ServiceManagerInit;
             begin
                  try
                     iStatus := ServiceStatusManager;
                     AddInteger( K_MANAGER_SERVICE_STATUS, iStatus );
                     AddBoolean( K_MANAGER_SERVICE_NAME, ServiceStatusIsWorking( iStatus, sMensaje ) );
                     AddString( K_MANAGER_SERVICE_MSG, Format( sMensaje, [K_MANAGER_SERVICE_NAME] ) );

                     iStatus := ServiceStatusEmail;
                     AddInteger( K_EMAIL_SERVICE_STATUS, iStatus );
                     AddBoolean( K_EMAIL_SERVICE_NAME, ServiceStatusIsWorking( iStatus, sMensaje ) );
                     AddString( K_EMAIL_SERVICE_MSG, Format( sMensaje, [K_EMAIL_SERVICE_NAME] ) );
                  finally
                         ServiceManagerClose;
                  end;
             end;
        end;
     finally
            Result := oParametros.VarValues;
            FreeAndNil( oParametros );
     end;
end;

function TdmServerComparte.ServiceManagerInit: Boolean;
begin
     try
        Result := ZetaWinAPITools.OpenServiceManager( FSvcManager );
     except
           Result := TRUE;
     end;

end;

procedure TdmServerComparte.ServiceManagerClose;
begin
     ZetaWinAPITools.CloseServiceManager( FSvcManager );
end;

function TdmServerComparte.ServiceStatus(const sService: string): DWord;
begin
     if ZetaWinAPITools.FindService( FSvcManager, sService ) then
        Result := ZetaWinAPITools.GetServiceStatus( FSvcManager, sService )
     else
         Result := 0;
end;

function TdmServerComparte.ServiceStatusManager: DWord;
begin
     Result := ServiceStatus( K_MANAGER_SERVICE_CONSOLE );
end;

function TdmServerComparte.ServiceStatusEmail: DWord;
begin
     Result := ServiceStatus( K_EMAIL_SERVICE_CONSOLE );
end;

function TdmServerComparte.ServiceStatusIsWorking( const iStatus: DWord; var sMensaje : WideString ): Boolean;
begin
     {$ifdef CAROLINA}
     Result := TRUE;
     {$else}
     Result := FALSE;
     {$endif}
     case iStatus of
          SERVICE_STOPPED : sMensaje := 'Detenido';
          SERVICE_START_PENDING : sMensaje := 'Pendiente de Arrancar';
          SERVICE_STOP_PENDING : sMensaje := 'Pendiente de Detener';
          SERVICE_RUNNING:
          begin
               Result := TRUE;
               sMensaje := 'Iniciado';
          end;
          SERVICE_CONTINUE_PENDING : sMensaje := 'Pendiente de Continuar';
          SERVICE_PAUSE_PENDING : sMensaje := 'Pendiente de Pause';
          SERVICE_PAUSED : sMensaje := 'en Pausa';
     else
         sMensaje := 'Sin Instalar / Deshabilitado';
     end;

     sMensaje := 'Servicio de %s ' + sMensaje;

end;

function TdmServerComparte.ServiceManagerIsWorking( var sMensaje: WideString ): Boolean;
begin
     Result := TRUE;
     {$ifdef false}
     Result := FALSE;
     if ServiceManagerInit then
     begin
          try
             Result := ServiceStatusIsWorking( ServiceStatusManager, sMensaje );
             sMensaje := Format( sMensaje, [K_MANAGER_SERVICE_NAME] );
          finally
                 ServiceManagerClose;
          end;
     end;
     {$endif}
end;

function TdmServerComparte.ServiceEmailIsWorking( var sMensaje : WideString ): Boolean;
begin
     Result := FALSE;
     if ServiceManagerInit then
     begin
          try
             Result := ServiceStatusIsWorking( ServiceStatusEmail, sMensaje );
             sMensaje := Format( sMensaje, [K_EMAIL_SERVICE_NAME] );
          finally
                 ServiceManagerClose;
          end;
     end;
end;

function TdmServerComparte.GetWindowsEventLog(Parametros: OleVariant): OleVariant;
var
    oEventLog: TdmEventLogViewer;
begin
     SetComplete;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          AsignaParamList( Parametros );
          oEventLog := TdmEventLogViewer.Create(self);
          try
             try
                with ParamList do
                     Result := oEventLog.ReadWindowsEventLog( ParamByName('ComputerName').AsString,
                                                              ParamByName('Todos').AsBoolean );
             except
                   On E:Exception do
                   begin
                        raise;
                   end;
             end;
             SetComplete;
          finally
                 FreeAndNil( oEventLog );
          end;
     end; 
end;

function TdmServerComparte.GetNotificaciones( Parametros: OleVariant): OleVariant;
var
   sModelo : String;
   iOrden : Integer;
begin
     try
        SetTablaInfo( eNotificacion );
        with oZetaProvider do
        begin
             AsignaParamList(Parametros);
             with ParamList do
             begin
                  sModelo := ParamByName('Modelo').AsString;
                  iOrden := ParamByName('Orden').AsInteger;
             end;
             with TablaInfo do
             begin
                  Filtro := Format( ' ( WM_CODIGO = ''%s'' ) and ( WE_ORDEN = %d ) ', [ sModelo, iOrden ] );
             end;
             Result := GetTabla( Comparte );
        end;
        SetComplete;
     except
           SetOLEVariantToNull( Result );
           SetAbort;
           raise;
     end;
end;

function TdmServerComparte.GetNumeroProcesos( const Modelo: WideString): Integer;
var
   FGetProcesos : TZetaCursor;
begin
     Result := 0;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          try
             FGetProcesos := CreateQuery( GetScript(Q_GET_NUMPROCESOS) );
             with FGetProcesos do
             begin
                  ParamAsString(FGetProcesos, 'WM_CODIGO', Modelo);
                  Active := TRUE;
                  if ( not EOF ) then
                  begin
                       Result := FieldByName('NUMPROCESOS').AsInteger;
                  end;
                  Active := FALSE;
             end;
          finally
                 FreeAndNil(FGetProcesos)
          end;

     end;
     setComplete;
end;

//TWizCopiarModelo
function TdmServerComparte.CopiarModelo( Parametros: OleVariant): OleVariant;
var
   sModeloOrigen, sModeloDestino, sModeloNombre: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          AsignaParamList( Parametros );
          EmpiezaTransaccion;
          with ParamList do
          begin
               sModeloOrigen := ParamByName('ModeloOrigen').AsString;
               sModeloDestino := ParamByName('ModeloDestino').AsString;
               sModeloNombre := ParamByName('ModeloNombre').AsString;
          end;
          try
             ExecSQL( Comparte, Format( GetScript( Q_COPIAR_MODELO ), [ EntreComillas(sModeloOrigen), EntreComillas(sModeloDestino), EntreComillas(sModeloNombre) ] ) );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     RollBackTransaccion;
                     raise;
                end;
          end;
     end;
     SetComplete;
end;

function TdmServerComparte.GetNivelRoles: OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, GetScript( Q_ROL_NIVEL ), True );
     end;
end;

function TdmServerComparte.GetConexiones: OleVariant;
begin
     try
        SetTablaInfo( eConexion );
        with oZetaProvider do
        begin
             EmpresaActiva := Comparte;
             Result := GetTabla( Comparte);
        end;
        SetComplete;
     except
           SetOLEVariantToNull( Result );
           SetAbort;
           raise;
     end;
end;

function TdmServerComparte.GrabaConexiones(Delta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
      with oZetaProvider do
      begin
           EmpresaActiva := Comparte;
           SetTablaInfo( eConexion );
           Result := GrabaTabla( Comparte, Delta, ErrorCount );
      end;
      SetComplete;
end;

function TdmServerComparte.GetValidacionUso(const Modulo: WideString): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          try
             Result := OpenSQL( Comparte, Format( GetScript( Q_CONS_WVALUSO ), [ Modulo ] ), True );
             SetComplete;
          except
                SetAbort;
                raise;
          end;
     end;
end;

function TdmServerComparte.GrabaValidacionUso(const Modulo, Uso: WideString): OleVariant;
var
   FCursor: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             FCursor := CreateQuery( GetScript( Q_GRABA_WVALUSO ) );
             try
                ParamAsString( FCursor, 'MODULO', Modulo );
                ParamAsString( FCursor, 'USO', Uso );
                Ejecuta( FCursor );
                TerminaTransaccion( True );
             finally
               FreeAndNil(FCursor);
             end;
          except
                on Error: Exception do
                begin
                     RollBackTransaccion;
                     raise;
                end;
          end;
     end;
     SetComplete;
end;

{ **************************** }
{ ****** Fin de WorkFlow ***** }
{ **************************** }

 
{ ***************************** }
{ ****** Inicio de Kiosco ***** }
{ ***************************** }

function TdmServerComparte.GetBotones( const sPantalla: WideString): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, Format( GetScript( Q_KIOSCO2_GET_BOTONES ), [ EntreComillas(sPantalla) ] ), True );
     end;
     SetComplete;
end;

function TdmServerComparte.GetCarrusel( const sShow: WideString): OleVariant;
begin
  with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, Format( GetScript( Q_KIOSCO2_GET_CARRUSEL ), [ EntreComillas(sShow) ] ), True );
     end;
     SetComplete;
end;

function TdmServerComparte.GetEstilo( const sEstilo: WideString): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, Format( GetScript( Q_KIOSCO2_GET_ESTILO ), [ EntreComillas(sEstilo) ] ), True );
     end;
     SetComplete;
end;

function TdmServerComparte.GetKioscos: OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, GetScript( Q_KIOSCO2_GET_KIOSCOS ), True );
     end;
     SetComplete;
end;

function TdmServerComparte.GetPantalla( const sPantalla: WideString): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, Format( GetScript( Q_KIOSCO2_GET_PANTALLA ), [ EntreComillas(sPantalla) ] ), True );
     end;
     SetComplete;
end;

function TdmServerComparte.GetShow(const sShow: WideString): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, Format( GetScript( Q_KIOSCO2_GET_SHOW ), [ EntreComillas(sShow) ] ), True );
     end;
     SetComplete;
end;

function TdmServerComparte.GetValoresActivos(const Kiosco: WideString; Empleado: Integer; const Empresa: WideString): WideString;
var
   sValorIni: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          InitGlobales;
          sValorIni := VerificaDirURL( GetGlobalString( K_KIOSCO_DIRECTORIO_RAIZ ) ) + GetGlobalString( K_KIOSCO_VALORES_ACTIVOS_URL );
     end;
     Result := Format( sValorIni, [ Kiosco, Empleado, Empresa  ] );
     SetComplete;
end;

function TdmServerComparte.GetCarruselesInfo(const Codigo: WideString): OleVariant;
begin
     SetTablaInfo( ekCarruselesInfo );
     with oZetaProvider do
     begin
          with TablaInfo do
          begin
               Filtro := Format( 'KW_CODIGO = %s', [ EntreComillas( Codigo ) ]);
               Result := GetTabla( Comparte );
          end;
     end;
end;

{procedure TdmServerComparte.BeforeUpdateCarruselInfo(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     with DeltaDs do
          if ( UpdateKind in [ ukInsert, ukModify ] ) then
          begin
               Edit;
               FieldByName( 'KW_CODIGO' ).AsString := FCarrusel;
               Post;
          end;
end;}


function TdmServerComparte.GrabaCarruseles( Delta, DeltaCarruselInfo: OleVariant; const Codigo: WideString;
                                            var CarruselInfo: OleVariant;
                                            var ErrorCount, ErrorCountInfo: Integer): OleVariant;

const
      K_DELETE = 'DELETE FROM KSHOWINF WHERE KW_CODIGO = %s';
var
   iPosicion: integer;
begin
     ErrorCount := 0;
     ErrorCountInfo := 0;
     //FCarrusel := Codigo;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          if ( Delta = null ) then
             Result := Delta
          else
          begin
               SetTablaInfo( ekCarruseles );
               Result := GrabaTabla( Comparte, Delta, ErrorCount );
          end;
          {if ( ErrorCount = 0 ) then
          begin
               if ( DeltaCarruselInfo = null ) then
                  CarruselInfo := DeltaCarruselInfo
               else
               begin
                    SetTablaInfo( ekCarruselesInfo );
                    TablaInfo.BeforeUpdateRecord := BeforeUpdateCarruselInfo;
                    CarruselInfo := GrabaTablaGrid( Comparte, DeltaCarruselInfo, ErrorCountInfo );
               end;
          end;}
          if ( ErrorCount = 0 ) then
          begin
               if ( DeltaCarruselInfo <> null ) and StrLleno( Codigo ) then
               begin
                    FQuery := CreateQuery( GetScript( Q_KIOSCO2_INSERT_KSHOWINF ) );
                    try
                        ExecSQL( EmpresaActiva, Format( K_DELETE, [ Comillas( Codigo ) ] ) );
                        iPosicion := 0;

                        with cdsDatos do
                        begin
                             IndexFieldNames := VACIO;
                             Data := DeltaCarruselInfo;
                             IndexFieldNames := 'KW_CODIGO;KI_ORDEN';
                             First;
                             while NOT EOF do
                             begin
                                  Inc(iPosicion);
                                  ParamAsString( FQuery, 'KW_CODIGO', Codigo );
                                  ParamAsInteger( FQuery, 'KI_ORDEN',  iPosicion );
                                  ParamAsFloat( FQuery,  'KI_TIMEOUT',  FieldByName('KI_TIMEOUT').AsFloat );
                                  ParamAsString( FQuery,  'KI_URL', FieldByName('KI_URL').AsString );
                                  ParamAsString( FQuery, 'KI_REFRESH', FieldByName('KI_REFRESH').AsString );
                                  Ejecuta(FQuery);

                                  Next;
                             end;
                        end;
                    finally
                           FreeAndNil(FQuery);
                    end;
               end;
               CarruselInfo := DeltaCarruselInfo
          end;

     end;
     SetComplete;
end;

function TdmServerComparte.GrabaMarcosInfo( Delta, DeltaBotones: OleVariant; const Codigo: WideString;
                                            var oBotones: OleVariant;
                                            var ErrorCount, ErrorCountBotones: Integer): OleVariant;
 const
      K_DELETE = 'DELETE FROM KBOTON WHERE KS_CODIGO = %s';
 var
    iPosicion: integer;
begin
     ErrorCount := 0;
     ErrorCountBotones := 0;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          if ( Delta = null ) then
             Result := Delta
          else
          begin
               SetTablaInfo( ekMarcosInformacion );
               //TablaInfo.BeforeUpdateRecord := BeforeUpdateSesion;
               Result := GrabaTabla( Comparte, Delta, ErrorCount );
          end;
          if ( ErrorCount = 0 ) then
          begin
               if ( DeltaBotones <> null ) and StrLleno( Codigo ) then
               begin
                    FQuery := CreateQuery( GetScript( Q_KIOSCO2_INSERT_BOTON ) );
                    try
                        ExecSQL( EmpresaActiva, Format( K_DELETE, [ Comillas( Codigo ) ] ) );
                        iPosicion := 0;

                        with cdsDatos do
                        begin
                             IndexFieldNames := VACIO;
                             Data := DeltaBotones;
                             IndexFieldNames := 'KS_CODIGO;KB_ORDEN';
                             First;
                             while NOT EOF do
                             begin
                                  Inc(iPosicion);
                                  ParamAsString( FQuery, 'KS_CODIGO', Codigo );
                                  ParamAsInteger( FQuery, 'KB_ORDEN',  iPosicion );
                                  ParamAsString( FQuery,  'KB_TEXTO',  FieldByName('KB_TEXTO').AsString );
                                  ParamAsString( FQuery,  'KB_BITMAP', FieldByName('KB_BITMAP').AsString );
                                  ParamAsInteger( FQuery, 'KB_ACCION', FieldByName('KB_ACCION').AsInteger );
                                  ParamAsString( FQuery,  'KB_URL',    FieldByName('KB_URL').AsString );
                                  ParamAsString( FQuery,  'KB_SCREEN', FieldByName('KB_SCREEN').AsString );
                                  ParamAsInteger( FQuery, 'KB_ALTURA', FieldByName('KB_ALTURA').AsInteger );
                                  ParamAsInteger( FQuery, 'KB_LUGAR',  FieldByName('KB_LUGAR').AsInteger );
                                  ParamAsInteger( FQuery, 'KB_SEPARA',  FieldByName('KB_SEPARA').AsInteger );
                                  ParamAsInteger( FQuery, 'KB_REPORTE',  FieldByName('KB_REPORTE').AsInteger );
                                  ParamAsInteger( FQuery, 'KB_POS_BIT',  FieldByName('KB_POS_BIT').AsInteger );
                                //  ParamAsString( FQuery,  'KB_EMPRESA',  FieldByName('KB_EMPRESA').AsString );
                                  Ejecuta(FQuery);

                                  Next;
                             end;
                        end;
                    finally
                           FreeAndNil(FQuery);
                    end;
               end;
               oBotones := DeltaBotones
          end;
     end;
     SetComplete;
end;

{ ***************************** }
{ ****** Fin de Kiosco ******** }
{ ***************************** }

{$ifndef DOS_CAPAS}
initialization
  TComponentFactory.Create(ComServer, TdmServerComparte,Class_dmServerComparte, ciMultiInstance, tmApartment);
{$endif}

end.





