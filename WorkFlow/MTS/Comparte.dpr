library Comparte;

uses
  ComServ,
  Comparte_TLB in 'Comparte_TLB.pas',
  DServerComparte in 'DServerComparte.pas' {dmServerComparte: TMtsDataModule} {dmServerComparte: CoClass},
  DWorkFlowConst in '..\Tools\DWorkFlowConst.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
