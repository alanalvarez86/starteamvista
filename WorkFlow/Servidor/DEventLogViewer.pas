unit DEventLogViewer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Registry, Db, Grids, DBGrids, DBClient,Mask,
  ZetaServerDataSet, ExtCtrls, DBCtrls, Buttons;

type

  PEventLogRecord = ^TEventLogRecord;
  TEventLogRecord = class
    ComputerName: string;
    Description: PChar;
    EventCategory: word;
    EventID: DWORD;
    EventType: word;
    RecordNumber: DWORD;
    SourceName: string;
    TimeGenerated: TDateTime;
    UserName: String;
  end;

  PEventLog = ^TEventLog;
  TEventLog = record
     Length             : DWord;
     Reserved           : DWord;
     RecordNumber       : DWord;
     TimeGenerated      : DWord;
     TimeWritten        : DWord;
     EventID            : DWord;
     EventType          : Word;
     NumStrings         : Word;
     EventCategory      : Word;
     ReservedFlags      : Word;
     ClosingRecordNumber: DWord;
     StringOffset       : DWord;
     UserSIDLength      : DWord;
     UserSIDOffset      : DWord;
     DataLength         : DWord;
     DataOffset         : DWord;
  end;

   TdmEventLogViewer = class(TDatamodule)
    cdsEventLog: TServerDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FRegistry: TRegistry;
    lRegistryAbierto: Boolean;
    procedure CreaDataset;

    function GetAccountName(const aSID: PSID): string;
    function GetRegValue(aKey, aValue: string): string;
    function HoursTimeZone: TDateTime;
    function UnixDateTimeToDelphiDateTime( UnixDateTime: Integer): TDateTime;
    function MuestraServicio(const sServiceName: string): Boolean;

    procedure MuestraEventLogRecord(EventLogRecord: TEventLogRecord);

  public
    { Public declarations }
    function ReadWindowsEventLog(const sComputerName: string; const lTodos: Boolean ): OleVariant;
  end;

var
  dmEventLogViewer: TdmEventLogViewer;

implementation

uses
    ZetaCommonClasses,
    DWorkFlowConst;

{$R *.DFM}
const
  // Flags of events that can be logged.
  EVENTLOG_SEQUENTIAL_READ  = $0001;
  EVENTLOG_SEEK_READ        = $0002;
  EVENTLOG_FORWARDS_READ    = $0004;
  EVENTLOG_BACKWARDS_READ   = $0008;
  // The types of events that can be logged.
  EVENTLOG_SUCCESS          = $0000;
  EVENTLOG_ERROR_TYPE       = $0001;
  EVENTLOG_WARNING_TYPE     = $0002;
  EVENTLOG_INFORMATION_TYPE = $0004;
  EVENTLOG_AUDIT_SUCCESS    = $0008;
  EVENTLOG_AUDIT_FAILURE    = $0016;

  H_KEY_EVENT_LOG ='SYSTEM\CurrentControlSet\Services\EventLog\Application\';
  H_KEY_MESSAGE_FILE = 'EventMessageFile';

{$ifdef CAROLINA}
var  FCuentaMSG:integer;
{$endif}

procedure xMSG( const s: string );overload;
begin
     {$ifdef CAROLINA}
     showmessage(s);
     {$endif}
end;

procedure xMSG(const i:integer );overload;
begin
     {$ifdef CAROLINA}
     xmsg( intToSTr(i));
     {$endif}
end;

procedure xmsg;overload;
begin
     {$ifdef CAROLINA}
     xmsg(FCuentaMSG);
     inc(FCuentaMSG);
     {$endif}
end;


{ TEventLogViewer}

procedure TdmEventLogViewer.DataModuleCreate(Sender: TObject);
begin
     {$ifdef CAROLINA}
     FCuentaMSG := 0;
     {$endif}

     FRegistry := TRegistry.Create;

     with FRegistry do
     begin
          RootKey := HKEY_LOCAL_MACHINE;
     end;

     CreaDataset;

     lRegistryAbierto := FALSE;
end;

procedure TdmEventLogViewer.DataModuleDestroy(Sender: TObject);
begin
     cdsEventLog.Close;
     FreeAndNil( cdsEventLog );
     FreeAndNil(FRegistry);
end;

procedure TdmEventLogViewer.CreaDataset;
begin
     with cdsEventLog do
     begin
          InitTempDataset;
          AddIntegerField( 'EL_TIPO' );            { EventLog Column: Type}
          AddDateField( 'EL_FECHA' );              { EventLog Column: Date}
          AddStringField( 'EL_HORA', 6 );          { EventLog Column: Time}
          AddStringField( 'EL_FUENTE', 30 );       { EventLog Column: Source}
          AddIntegerField( 'EL_CATEGORIA' );       { EventLog Column: Category}
          AddIntegerField( 'EL_EVENTID' );         { EventLog Column: Event}
          AddStringField( 'EL_USUARIO', 30 );      { EventLog Column: User}
          AddStringField( 'EL_COMPUTADORA', 30 );  { EventLog Column: Computer}
          AddMemoField( 'EL_MENSAJE', 0 );         { EventLog Field: Description}
          AddIntegerField( 'EL_NUMREGISTRO' );     { EventLog Field: RecordNumber}
          CreateTempDataset;
     end;
end;

procedure TdmEventLogViewer.MuestraEventLogRecord( EventLogRecord: TEventLogRecord );
begin
     with EventLogRecord do
     begin
          with cdsEventLog do
          begin
               Append;
               FieldByName( 'EL_TIPO' ).AsInteger := EventType;
               FieldByName( 'EL_FECHA' ).AsDateTime := TDate( TimeGenerated );
               FieldByName( 'EL_HORA' ).AsString :=  FormatDateTime( 'hhnnss',TimeGenerated);
               FieldByName( 'EL_FUENTE' ).AsString := SourceName;
               FieldByName( 'EL_CATEGORIA' ).AsInteger := EventCategory;
               FieldByName( 'EL_EVENTID' ).AsInteger := EventID;
               FieldByName( 'EL_USUARIO' ).AsString := UserName;
               FieldByName( 'EL_COMPUTADORA' ).AsString := ComputerName;
               FieldByName( 'EL_MENSAJE' ).AsString :=  Description;
               FieldByName( 'EL_NUMREGISTRO' ).AsInteger := RecordNumber;
               Post;
          end;
     end;
end;

function TdmEventLogViewer.GetRegValue(aKey, aValue: string): string;
begin

     Result := '';

     with FRegistry do
     begin
          if not lRegistryAbierto then
          begin
               RootKey := HKEY_LOCAL_MACHINE;
               lRegistryAbierto := TRUE;
          end;

          aKey := H_KEY_EVENT_LOG + aKey;

          if OpenKeyReadOnly( aKey ) then
          begin
               Result := ReadString(aValue);
               CloseKey;
          end;
     end;
end;

function TdmEventLogViewer.MuestraServicio( const sServiceName: string ): Boolean;
begin
     Result := ( sServiceName = K_MANAGER_SERVICE_CONSOLE ) or
               ( sServiceName = K_EMAIL_SERVICE_CONSOLE ) or
               ( sServiceName = 'MSSQLSERVER');
end;

function TdmEventLogViewer.GetAccountName(const aSID: PSID): string;
 var
    lpDomainName,
    lpUserName: string;
    szDomainName,
    szUserName: DWord;
    peUse: DWord;
begin
     Result := EmptyStr;
     szDomainName := 0;
     szUserName := 0;
     LookupAccountSid(nil, aSID, nil, szUserName, nil, szDomainName, peUse);
     SetLength(lpUserName, szUserName);
     SetLength(lpDomainName, szDomainName);
     if LookupAccountSid(nil, aSID, PChar(lpUserName), szUserName, PChar(lpDomainName), szDomainName, peUse) then
     begin
          SetLength(lpUserName,szUserName);
          SetLength(lpDomainName,szDomainName);
          Result:=Format('%s%s',[lpDomainName,lpUserName]);
     end;
end;


function TdmEventLogViewer.HoursTimeZone: TDateTime;
var
 lpTimeZoneInformation: TTimeZoneInformation;
 Bias: Longint;
begin
 GetTimeZoneInformation(lpTimeZoneInformation);
 Bias := lpTimeZoneInformation.Bias;
 Result := EncodeTime(Abs(Bias div 60), 0, 0, 0);
 if Bias > 0 then Result := -Result;
end;

function TdmEventLogViewer.UnixDateTimeToDelphiDateTime(UnixDateTime: LongInt):TDateTime;
begin
 Result := EncodeDate(1970, 1, 1) + (UnixDateTime / 86400) + HoursTimeZone; {86400=No. of secs. per day}
end;

function TdmEventLogViewer.ReadWindowsEventLog( const sComputerName: string;
                                                const lTodos: Boolean ): OleVariant;
var
  hModule           : THandle;
  hEventLog         : THandle;
  EventLogPtr       : pointer;
  lpBuffer          : PEventLog;
  dwEventLogRecords : DWORD;
  dwBytesRead       : DWORD;
  dwBytesNeed       : DWORD;
  dwBufSize         : DWORD;
  OutputStr         : array[0..255] of char;
  EventMessageFile  : PChar;
  FirstArgument     : PChar;
  NextArgument      : PChar;
  EventLogRecord    : TEventLogRecord;
  i, iNumStrings    : integer;
  ArgsBuffer        : array of string;
  lpMsgBuf          : PChar;
  dwArgumentOffset  : DWORD;

  sMensaje : string;

begin

     dwBytesRead := 0;

     hEventLog := OpenEventLog(PChar(sComputerName), PChar('Application'));
     if hEventLog = 0 then 
        raise Exception.Create(SysErrorMessage(GetLastError));

     if not GetNumberOfEventLogRecords(hEventLog, dwEventLogRecords) then
        dwEventLogRecords := 512;

     dwBufSize := 64*1024;

     GetMem(EventLogPtr, dwBufSize);
     lpBuffer := EventLogPtr;

     try
        while ReadEventLog(hEventLog, EVENTLOG_BACKWARDS_READ or EVENTLOG_SEQUENTIAL_READ, 0, EventLogPtr, dwBufSize, dwBytesRead, dwBytesNeed) do
        begin
             while dwBytesRead > 0 do
             begin
                  with TEventLog(lpBuffer^) do
                  begin
                       EventLogRecord := TEventLogRecord.Create;
                       try
                          EventLogRecord.EventType := EventType;
                          EventLogRecord.TimeGenerated := UnixDateTimeToDelphiDateTime(TimeGenerated);
                          EventLogRecord.SourceName := PChar(DWORD(lpBuffer)+DWORD(SizeOf(TEventLog)));
                          if lTodos or MuestraServicio( EventLogRecord.SourceName ) then
                          begin
                               EventLogRecord.EventCategory := EventCategory;
                               EventLogRecord.EventID := EventID;
                               if TEventLog(lpBuffer^).UserSIDLength > 0 then
                                  EventLogRecord.UserName := GetAccountName(PSID(DWORD(lpBuffer) + DWORD(TEventLog(lpBuffer^).UserSIDOffset)))
                               else
                                   EventLogRecord.UserName := '(blank)';
                               EventLogRecord.ComputerName := PChar(DWORD(lpBuffer)+DWORD(SizeOf(TEventLog)+System.Length(EventLogRecord.SourceName)+1));
                               FirstArgument := PChar(DWORD(lpBuffer)+DWORD(StringOffset));
                               iNumStrings := TEventLog(lpBuffer^).NumStrings;
                               SetLength(ArgsBuffer, iNumStrings);

                               try
                                  dwArgumentOffset := DWORD(StrLen(FirstArgument)+1);
                                  if ( iNumStrings > 0 ) then
                                  begin
                                       ArgsBuffer[0] := FirstArgument;
                                       sMensaje := ArgsBuffer[0];

                                  end;
                                  for i := 1 to iNumStrings-1 do
                                  begin
                                    NextArgument := PChar(DWORD(lpBuffer)+DWORD(StringOffset)+dwArgumentOffset);
                                    ArgsBuffer[i] := NextArgument;
                                    sMensaje:= sMensaje + ArgsBuffer[i] ;
                                    dwArgumentOffset := dwArgumentOffset + StrLen(NextArgument) + 1;
                                  end;
                               except
                                     SetLength(ArgsBuffer, 1);
                                     ArgsBuffer[0] := '';
                               end;

                               EventMessageFile := PChar( GetRegValue( EventLogRecord.SourceName, H_KEY_MESSAGE_FILE));
                               FillChar(OutputStr, SizeOf(OutputStr), 0);
                               ExpandEnvironmentStrings(EventMessageFile, OutputStr, SizeOf(OutputStr));
                               if OutputStr <> EventMessageFile then
                                  EventMessageFile := OutputStr;

                               hModule := LoadLibraryEx(PChar(EventMessageFile), 0, DONT_RESOLVE_DLL_REFERENCES);
                               FillChar(lpMsgBuf, SizeOf(lpMsgBuf), 0);
                               if hModule <> 0 then
                               begin
                                    try
                                       FormatMessage( FORMAT_MESSAGE_ALLOCATE_BUFFER or
                                                      FORMAT_MESSAGE_FROM_HMODULE or
                                                      FORMAT_MESSAGE_FROM_SYSTEM or
                                                      FORMAT_MESSAGE_ARGUMENT_ARRAY,
                                                      Pointer(hModule),
                                                      EventLogRecord.EventID,
                                                      0,
                                                      PChar(@lpMsgBuf), SizeOf(lpMsgBuf), ArgsBuffer);
                                    finally
                                           FreeLibrary(hModule);
                                           ArgsBuffer := nil;
                                    end;
                               end;
                               if ( lpMsgBuf = NIL ) then
                                  EventLogRecord.Description := PChar( sMensaje )
                               else
                                   EventLogRecord.Description := lpMsgBuf;

                               lpMsgBuf := nil;

                               EventLogRecord.RecordNumber := RecordNumber;

                               MuestraEventLogRecord( EventLogRecord );
                          end;
                          Application.ProcessMessages;
                       finally
                              FreeAndNil(EventLogRecord);
                       end;
                  end;
                  dwBytesRead := dwBytesRead - TEventLog(lpBuffer^).Length;
                  lpBuffer := PEventLog(DWORD(lpBuffer) + TEventLog(lpBuffer^).Length);
             end;
             lpBuffer := EventLogPtr;
        end;

        Result := cdsEventLog.Data;
     finally
            FreeMem(EventLogPtr, dwBufSize);
            CloseEventLog(hEventLog);
     end;
end;

end.




