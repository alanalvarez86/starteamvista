unit FCatModelos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids, Buttons, StdCtrls,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZBaseConsulta,
     ZetaDBGrid;

type
  TCatModelos = class(TBaseConsulta)
    ZetaDBGrid: TZetaDBGrid;
    PanelBotones: TPanel;
    Suspender: TBitBtn;
    Cancelar: TBitBtn;
    Reiniciar: TBitBtn;
    Reactivar: TBitBtn;
    Copiar: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure SuspenderClick(Sender: TObject);
    procedure ReiniciarClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure ReactivarClick(Sender: TObject);
    procedure ZetaDBGridTitleClick(Column: TColumn);
  private
    { Private declarations }
    FParametros: TZetaParams;
    procedure MuestraProblema(const sTitulo: String);
    procedure ParametrosInit;
    procedure SetButtons( const lHayDatos: Boolean; const eStatus: eWFStatusModelo );
  protected
    { Protected declarations }
    function PuedeBorrar(var sMensaje: String): Boolean;override;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    property Parametros: TZetaParams read FParametros;
  end;

var
  CatModelos: TCatModelos;

implementation

{$R *.DFM}

uses DWorkFlow,
     DCliente,
     FSistEditAccesos,
     ZHelpContext,
     ZetaDialogo,
     ZAccesosTress,
     ZetaCommonTools,
     FTressShell;

{ *********** TSistUsuarios *********** }

procedure TCatModelos.FormCreate(Sender: TObject);
begin
     inherited;
     FParametros := TZetaParams.Create;
     HelpContext := H_WORKFLOWCFG_CATALOGOS_MODELOS;
     IndexDerechos := D_CAT_MODELOS;
     Copiar.Action := TressShell._P_CopiarModelo;
end;

procedure TCatModelos.FormShow(Sender: TObject);
begin
     inherited;
     //SetButtons( False, smwActivo );
end;

procedure TCatModelos.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FParametros );
     inherited;
end;

procedure TCatModelos.Connect;
begin
     with dmWorkflow do
     begin
          cdsModelos.Conectar;
          DataSource.DataSet:= cdsModelos;
     end;
end;

procedure TCatModelos.Refresh;
begin
     dmWorkflow.cdsModelos.Refrescar;
end;

procedure TCatModelos.Agregar;
begin
     dmWorkflow.cdsModelos.Agregar;
end;

procedure TCatModelos.Borrar;
begin
     dmWorkflow.cdsModelos.Borrar;
end;

function TCatModelos.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result := FALSE;
     sMensaje := 'No es posible borrar Modelos';
end;


procedure TCatModelos.Modificar;
begin
     dmWorkflow.cdsModelos.Modificar;
end;

procedure TCatModelos.MuestraProblema( const sTitulo: String );
begin
     ZetaDialogo.zError( sTitulo, Parametros.ParamByName( K_PARAMETRO_PROBLEMA ).AsString, 0 );
end;

procedure TCatModelos.ParametrosInit;
begin
     with Parametros do
     begin
          Clear;
     end;
end;

procedure TCatModelos.SetButtons( const lHayDatos: Boolean; const eStatus: eWFStatusModelo );
begin
     Suspender.Enabled := lHayDatos and ( eStatus in [ smwActivo ] ) and CheckDerechos( K_DERECHO_SUSPENDER );
     ReIniciar.Enabled := lHayDatos and ( eStatus in [ smwSuspendido ] ) and CheckDerechos( K_DERECHO_REINICIAR );

     Cancelar.Enabled := lHayDatos and ( eStatus in [ smwActivo, smwSuspendido ] ) and CheckDerechos( K_DERECHO_CANCELAR );
     Reactivar.Enabled := lHayDatos and ( eStatus in [ smwCancelado ] ) and CheckDerechos( K_DERECHO_REACTIVAR );
end;

procedure TCatModelos.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if Assigned( Datasource.Dataset ) then
     begin
          with Datasource.Dataset do
          begin
               SetButtons( not IsEmpty, eWFStatusModelo( FieldByName( 'WM_STATUS' ).AsInteger ) );
          end;
     end;
end;

procedure TCatModelos.SuspenderClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        ParametrosInit;
        if dmWorkflow.ModeloSuspender( Parametros ) then
        begin
             Refresh;
        end
        else
        begin
             MuestraProblema( '� Problema al suspender el Modelo !' );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TCatModelos.ReiniciarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        ParametrosInit;
        if dmWorkflow.ModeloReiniciar( Parametros ) then
        begin
             Refresh;
        end
        else
        begin
             MuestraProblema( '� Problema al reiniciar el Modelo !' );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TCatModelos.CancelarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        ParametrosInit;
        if dmWorkflow.ModeloCancelar( Parametros ) then
        begin
             Refresh;
        end
        else
        begin
             MuestraProblema( '� Problema al cancelar el Modelo !' );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TCatModelos.ReactivarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        ParametrosInit;
        if dmWorkflow.ModeloReactivar( Parametros ) then
        begin
             Refresh;
        end
        else
        begin
             MuestraProblema( '� Problema al reactivar el Modelo !' );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TCatModelos.ZetaDBGridTitleClick(Column: TColumn);
begin
     inherited;
     with Column do
     begin
          if ( FieldName <> 'WM_DESCRIP' ) then
             dmWorkFlow.cdsModelos.IndexFieldNames := FieldName;
     end;
end;

end.


