inherited CatConexionesEdit: TCatConexionesEdit
  Left = 282
  Top = 121
  Caption = 'Conexi'#243'n'
  ClientHeight = 298
  PixelsPerInch = 96
  TextHeight = 13
  object WA_CODIGOlbl: TLabel [0]
    Left = 77
    Top = 50
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = '&Folio:'
  end
  object WA_NOMBRElbl: TLabel [1]
    Left = 77
    Top = 96
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = '&URL:'
    FocusControl = WX_URL
  end
  object WX_FOLIO: TZetaDBTextBox [2]
    Left = 104
    Top = 48
    Width = 113
    Height = 17
    AutoSize = False
    Caption = 'WX_FOLIO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'WX_FOLIO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Label1: TLabel [3]
    Left = 11
    Top = 120
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo &autenticaci'#243'n:'
    FocusControl = WX_TIP_AUT
  end
  object Label2: TLabel [4]
    Left = 18
    Top = 224
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = '&Tipo codificaci'#243'n:'
    FocusControl = WX_TIP_ENC
  end
  object Label3: TLabel [5]
    Left = 62
    Top = 72
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = '&Nombre:'
    FocusControl = WX_NOMBRE
  end
  inherited PanelBotones: TPanel
    Top = 262
    BevelOuter = bvNone
  end
  object WX_URL: TDBEdit [9]
    Left = 104
    Top = 92
    Width = 337
    Height = 21
    DataField = 'WX_URL'
    DataSource = DataSource
    TabOrder = 4
  end
  object WX_TIP_AUT: TZetaDBKeyCombo [10]
    Left = 104
    Top = 117
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 5
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    DataField = 'WX_TIP_AUT'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object GBAutenticacion: TGroupBox [11]
    Left = 8
    Top = 144
    Width = 457
    Height = 73
    Caption = 'Autenticaci'#243'n'
    TabOrder = 6
    object Label6: TLabel
      Left = 61
      Top = 42
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cla&ve:'
      FocusControl = WX_PASSWRD
    end
    object Label5: TLabel
      Left = 52
      Top = 21
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'U&suario:'
      FocusControl = WX_USUARIO
    end
    object WX_PASSWRD: TDBEdit
      Left = 96
      Top = 41
      Width = 145
      Height = 21
      DataField = 'WX_PASSWRD'
      DataSource = DataSource
      MaxLength = 14
      PasswordChar = '*'
      TabOrder = 0
    end
    object WX_USUARIO: TDBEdit
      Left = 96
      Top = 17
      Width = 313
      Height = 21
      DataField = 'WX_USUARIO'
      DataSource = DataSource
      TabOrder = 1
    end
  end
  object WX_TIP_ENC: TZetaDBKeyCombo [12]
    Left = 104
    Top = 221
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 7
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    DataField = 'WX_TIP_ENC'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object WX_NOMBRE: TDBEdit [13]
    Left = 104
    Top = 68
    Width = 337
    Height = 21
    DataField = 'WX_NOMBRE'
    DataSource = DataSource
    TabOrder = 3
  end
  inherited DataSource: TDataSource
    Left = 12
  end
end
