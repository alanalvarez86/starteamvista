unit FCatNotificacionesEdit;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask,
     ZBaseEdicion,
     ZetaDBTextBox, ZetaSmartLists, ZetaEdit, ZetaKeyCombo, ZetaNumero;

type
  TCatNotificacionesEdit = class(TBaseEdicion)
    WY_XSL: TDBEdit;
    lblVeces: TLabel;
    lblObservaciones: TLabel;
    WY_TEXTO: TDBMemo;
    lblQuien: TLabel;
    lblResponsable: TLabel;
    Label3: TLabel;
    lblAccion: TLabel;
    Label5: TLabel;
    TiempoMinutos: TZetaNumero;
    Label6: TLabel;
    Label7: TLabel;
    TiempoHoras: TZetaNumero;
    TiempoDias: TZetaNumero;
    lblTiempo: TLabel;
    lblIntervalo: TLabel;
    IntervaloDias: TZetaNumero;
    IntervaloHoras: TZetaNumero;
    Label10: TLabel;
    Label11: TLabel;
    IntervaloMinutos: TZetaNumero;
    Label12: TLabel;
    lblXSL: TLabel;
    WY_VECES: TZetaDBNumero;
    WY_QUE: TZetaDBKeyCombo;
    WY_CUANDO: TZetaDBKeyCombo;
    WY_QUIEN: TZetaDBKeyCombo;
    WY_USR_NOT: TZetaDBEdit;
    WY_USR_NOTFind: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure TiempoDiasExit(Sender: TObject);
    procedure TiempoHorasExit(Sender: TObject);
    procedure TiempoMinutosExit(Sender: TObject);
    procedure IntervaloDiasExit(Sender: TObject);
    procedure IntervaloHorasExit(Sender: TObject);
    procedure IntervaloMinutosExit(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure WY_USR_NOTFindClick(Sender: TObject);
    procedure WY_QUIENClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure WY_QUIENChange(Sender: TObject);
    procedure WY_CUANDOChange(Sender: TObject);
    procedure WY_QUEChange(Sender: TObject);
  private
    { Private declarations }
    iDiasTiempo, iHorasTiempo, iMinutosTiempo : Integer;
    iDiasIntervalo, iHorasIntervalo, iMinutosIntervalo : Integer;
    iTipoNotificacion : Integer;
    procedure CambiaIntervalo;
    procedure CambiaTiempo;
    procedure SetControls;
    procedure HabilitaVeces;
    procedure HabilitaQue;
    procedure HabilitaQuien;
  protected
    { Protected declarations }
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeImprimir(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
    procedure Connect; override;
    procedure CancelarCambios;
    procedure EscribirCambios; override;
  public
    { Public declarations }
  end;

var
  CatNotificacionesEdit: TCatNotificacionesEdit;

implementation

uses DWorkflow,
     ZHelpContext,
     ZetaCommonClasses,
     ZetaBuscador,
     ZAccesosTress,
     ZetaDialogo,
     ZetaCommonLists;

{$R *.DFM}

procedure TCatNotificacionesEdit.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := D_CAT_MODELOS;
     IndexDerechos := D_CAT_MODELOS;
     FirstControl := WY_QUIEN;
     LlenaNotificacionItems( WY_QUIEN.Items );
     WY_CUANDO.ListaFija := lfWFNotificacionCuando;
     WY_QUE.ListaFija := lfWFNotificacionQue;
     iTipoNotificacion := -1;
end;

procedure TCatNotificacionesEdit.Connect;
var
   iTotalMinutosTiempo, iTotalMinutosIntervalo : Integer;
begin
     Datasource.Dataset := dmWorkflow.cdsNotificaciones;
     with dmWorkflow.cdsNotificaciones do
     begin
          iTotalMinutosTiempo := FieldByName('WY_TIEMPO').AsInteger;
          iTotalMinutosIntervalo := FieldByName('WY_CADA').AsInteger;
     end;
     iDiasTiempo :=  Trunc (iTotalMinutosTiempo / 1440);
     iTotalMinutosTiempo := iToTalMinutosTiempo MOD 1440;
     iHorasTiempo := Trunc ( iTotalMinutosTiempo / 60 );
     iMinutosTiempo := iTotalMinutosTiempo MOD 60;

     iDiasIntervalo :=  Trunc (iTotalMinutosIntervalo / 1440);
     iTotalMinutosIntervalo := iToTalMinutosIntervalo MOD 1440;
     iHorasIntervalo := Trunc ( iTotalMinutosIntervalo / 60 );
     iMinutosIntervalo := iTotalMinutosIntervalo MOD 60;

     tiempoDias.Valor := iDiasTiempo;
     tiempoHoras.Valor := iHorasTiempo;
     tiempoMinutos.Valor := iMinutosTiempo;

     intervaloDias.Valor := iDiasIntervalo;
     intervaloHoras.Valor := iHorasIntervalo;
     intervaloMinutos.Valor := iMinutosIntervalo;

end;



function TCatNotificacionesEdit.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := CheckDerechos( K_DERECHO_ALTA );
     if not Result then
     sMensaje := 'No Es Posible Agregar Notificaciones';
end;

function TCatNotificacionesEdit.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := CheckDerechos( K_DERECHO_BAJA );
     if not Result then
     sMensaje := 'No Es Posible Borrar Notificaciones';
end;

function TCatNotificacionesEdit.PuedeModificar(var sMensaje: String): Boolean;
begin
     {
     Se verifica El Derecho De Alta debido a
     que en el �rbol de Derecho no Aparecen
     Los Derechos De Alta y De Baja
     }
     Result := CheckDerechos( K_DERECHO_CAMBIO );
     if not Result then
        sMensaje := 'No Est� Permitido Modificar Notificaciones';
end;

function TCatNotificacionesEdit.PuedeImprimir(var sMensaje: String): Boolean;
begin
     {
     Se verifica El Derecho De Baja debido a
     que en el �rbol de Derecho no Aparecen
     Los Derechos De Alta y De Baja
     }
     Result := CheckDerechos( K_DERECHO_IMPRESION ); //K_DERECHO_BAJA );
     if not Result then
        sMensaje := 'No Est� Permitido Imprimir Notificaciones';
end;

procedure TCatNotificacionesEdit.TiempoDiasExit(Sender: TObject);
begin
     inherited;
     if (iDiasTiempo <> TiempoDias.ValorEntero) then
     begin
          CambiaTiempo;
          iDiasTiempo := TiempoDias.ValorEntero;
     end;
end;

procedure TCatNotificacionesEdit.TiempoHorasExit(Sender: TObject);
begin
     inherited;
     if (iHorasTiempo <> TiempoHoras.ValorEntero) then
     begin
          if (TiempoHoras.ValorEntero >= 24) then
          begin
               ZError('','Horas tienes que ser menor a 24',0);
               ActiveControl := TiempoHoras;
          end
          else
          begin
               CambiaTiempo;
               iHorasTiempo := TiempoHoras.ValorEntero;
          end;
     end;
end;

procedure TCatNotificacionesEdit.TiempoMinutosExit(Sender: TObject);
begin
     inherited;
     if (iMinutosTiempo <> TiempoMinutos.ValorEntero) then
     begin
          if (TiempoMinutos.ValorEntero >= 60) then
          begin
               ZError('','Minutos tienes que ser menor a 60',0);
               ActiveControl := TiempoMinutos;
          end
          else
          begin
               CambiaTiempo;
               iMinutosTiempo := TiempoMinutos.ValorEntero;
          end;
     end;

end;

procedure TCatNotificacionesEdit.IntervaloDiasExit(Sender: TObject);
begin
     inherited;
     if (iDiasIntervalo <> IntervaloDias.ValorEntero) then
     begin
          CambiaIntervalo;
          iDiasIntervalo := IntervaloDias.ValorEntero;
     end;
end;

procedure TCatNotificacionesEdit.IntervaloHorasExit(Sender: TObject);
begin
     inherited;
     if (iHorasIntervalo <> IntervaloHoras.ValorEntero) then
     begin
          if (IntervaloHoras.ValorEntero >= 24) then
          begin
               ZError('','Horas tienes que ser menor a 24',0);
               ActiveControl := IntervaloHoras;
          end
          else
          begin
               CambiaIntervalo;
               iHorasIntervalo := IntervaloHoras.ValorEntero;
          end;
     end;

end;

procedure TCatNotificacionesEdit.IntervaloMinutosExit(Sender: TObject);
begin
     inherited;
     if (iMinutosIntervalo <> IntervaloMinutos.ValorEntero) then
     begin
          if (IntervaloMinutos.ValorEntero >= 60) then
          begin
               ZError('','Minutos tienes que ser menor a 60',0);
               ActiveControl := IntervaloMinutos;
          end
          else
          begin
               CambiaIntervalo;
               iMinutosIntervalo := IntervaloMinutos.ValorEntero;
          end;
     end;
end;

procedure TCatNotificacionesEdit.CambiaTiempo;
begin
     with dmWorkflow.cdsNotificaciones do
     begin
          if not Editing then
             Edit;
          FieldByName('WY_TIEMPO').AsInteger := TiempoDias.ValorEntero * 24 * 60 + TiempoHoras.ValorEntero * 60 + TiempoMinutos.ValorEntero;
     end;
end;

procedure TCatNotificacionesEdit.CambiaIntervalo;
begin
     with dmWorkflow.cdsNotificaciones do
     begin
          if not Editing then
             Edit;
          FieldByName('WY_CADA').AsInteger := IntervaloDias.ValorEntero * 24 * 60 + IntervaloHoras.ValorEntero * 60 + IntervaloMinutos.ValorEntero;
     end;
end;



procedure TCatNotificacionesEdit.CancelarCambios;
begin
     ClientDataSet.Cancel
end;

procedure TCatNotificacionesEdit.CancelarClick(Sender: TObject);
begin
     if Editing then
        CancelarCambios
     else
         Close;

end;

procedure TCatNotificacionesEdit.EscribirCambios;
begin
     ClientDataSet.Post;
end;

procedure TCatNotificacionesEdit.WY_USR_NOTFindClick(Sender: TObject);
begin
     inherited;
     dmWorkflow.NotificacionesModificarResponsable( eWFNotificacion( WY_QUIEN.Valor ), 'WY_USR_NOT' );
end;

procedure TCatNotificacionesEdit.WY_QUIENClick(Sender: TObject);
begin
     inherited;
     WY_USR_NOT.Enabled := ZetaCommonLists.GetNotificacionDebeEspecificar( eWFNotificacion( WY_QUIEN.Valor ) );
     WY_USR_NOTFind.Enabled := WY_USR_NOT.Enabled;
     lblResponsable.Enabled := WY_USR_NOT.Enabled;
     if ( ( iTipoNotificacion <> -1 ) and ( iTipoNotificacion <> WY_QUIEN.Valor ) ) then
        WY_USR_NOT.Text := Vacio;
     iTipoNotificacion := WY_QUIEN.Valor;
end;

procedure TCatNotificacionesEdit.SetControls;
begin
     lblQuien.Enabled := WY_QUIEN.Enabled;
     lblAccion.Enabled := WY_QUE.Enabled;
     TiempoDias.Enabled := WY_QUE.ENABLED;
     TiempoHoras.Enabled := WY_QUE.ENABLED;
     TiempoMinutos.Enabled := WY_QUE.ENABLED;
     lblTiempo.Enabled := WY_QUE.Enabled;
     IntervaloDias.Enabled := WY_QUIEN.Enabled and WY_QUE.ENABLED;
     IntervaloHoras.Enabled := WY_QUIEN.Enabled and WY_QUE.ENABLED;
     IntervaloMinutos.Enabled := WY_QUIEN.Enabled and WY_QUE.ENABLED;
     lblIntervalo.Enabled := WY_QUIEN.Enabled and WY_QUE.ENABLED;
     HabilitaVeces;
     WY_TEXTO.Enabled := WY_QUIEN.Enabled;
     WY_XSL.Enabled := WY_QUIEN.Enabled;
     lblXSL.Enabled := WY_QUIEN.Enabled;
     lblObservaciones.Enabled := WY_QUIEN.Enabled;
     WY_QUIENClick( Self );
end;



procedure TCatNotificacionesEdit.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     if ( Field <> nil ) then
        if ( ( Field.FieldName = 'WY_CADA' )  )  then
           HabilitaVeces;
end;

procedure TCatNotificacionesEdit.FormShow(Sender: TObject);
begin
     inherited;
     HabilitaQue;
     HabilitaQuien;
     SetControls;
end;

procedure TCatNotificacionesEdit.WY_QUIENChange(Sender: TObject);
begin
     inherited;
     SetControls;
end;

procedure TCatNotificacionesEdit.WY_CUANDOChange(Sender: TObject);
begin
     inherited;
     HabilitaQue;
     HabilitaQuien;
     SetControls;
end;

procedure TCatNotificacionesEdit.WY_QUEChange(Sender: TObject);
begin
     inherited;
     HabilitaQuien;
     SetControls;
end;

procedure TCatNotificacionesEdit.HabilitaVeces;
begin
     if dmWorkflow.cdsNotificaciones.FieldByName( 'WY_CADA' ).AsInteger = 0 then
     begin
          WY_VECES.Valor := 0;
          WY_VECES.Enabled := FALSE;
     end
     else
         WY_VECES.Enabled := WY_QUIEN.Enabled and WY_QUE.Enabled;
     lblVeces.Enabled := WY_VECES.Enabled;
end;

procedure  TCatNotificacionesEdit.HabilitaQuien;
begin
     if (eWFNotificacionQue ( WY_QUE.Valor ) in [ nqAutorizar, nqLanzar ] ) then
     begin
          WY_QUIEN.Valor := Ord( nvNinguno );
          WY_QUIEN.Enabled := FALSE;
     end
     else
         WY_QUIEN.Enabled := TRUE;
end;

procedure TCatNotificacionesEdit.HabilitaQue;
begin
     if ( eWFNotificacionCuando ( WY_CUANDO.Valor ) in [ncDespuesIniciar, ncAntesLimite, ncDespuesLimite] ) then
     begin
          WY_QUE.Enabled := TRUE;
     end
     else
     begin
          WY_QUE.Valor := Ord( nqNotificar );
          WY_QUE.Enabled := FALSE;
     end;
end;



end.

