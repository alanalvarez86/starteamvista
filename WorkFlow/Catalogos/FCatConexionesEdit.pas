unit FCatConexionesEdit;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask,
     ZBaseEdicion,
     ZetaDBTextBox, ZetaSmartLists, ZetaKeyCombo;

type
  TCatConexionesEdit = class(TBaseEdicion)
    WA_CODIGOlbl: TLabel;
    WX_URL: TDBEdit;
    WA_NOMBRElbl: TLabel;
    WX_FOLIO: TZetaDBTextBox;
    WX_TIP_AUT: TZetaDBKeyCombo;
    Label1: TLabel;
    GBAutenticacion: TGroupBox;
    Label2: TLabel;
    WX_TIP_ENC: TZetaDBKeyCombo;
    WX_PASSWRD: TDBEdit;
    Label6: TLabel;
    Label5: TLabel;
    WX_USUARIO: TDBEdit;
    WX_NOMBRE: TDBEdit;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  CatConexionesEdit: TCatConexionesEdit;

implementation

uses DWorkflow,
     ZHelpContext,
     ZetaCommonClasses,
     ZetaBuscador,
     ZAccesosTress,
     ZetaCommonLists;

{$R *.DFM}

procedure TCatConexionesEdit.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_WORKFLOWCFG_CATALOGOS_CONEXIONES_EDIC;
     IndexDerechos := D_CAT_CONEXIONES;
     FirstControl := WX_NOMBRE;
     WX_TIP_AUT.ListaFija := lfWFAutenticacionWebService;
     WX_TIP_ENC.ListaFija := lfWFCodificacionWebService;
end;

procedure TCatConexionesEdit.Connect;
begin
     Datasource.Dataset := dmWorkflow.cdsConexiones;
end;

procedure TCatConexionesEdit.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Conexi�n', 'Conexi�n', 'WX_FOLIO', dmWorkflow.cdsConexiones );
end;                                                 
end.
