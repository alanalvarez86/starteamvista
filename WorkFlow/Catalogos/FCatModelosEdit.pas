unit FCatModelosEdit;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask, ComCtrls,
     ZBaseEdicion,
     ZetaDBTextBox,
     ZetaEdit,
     ZetaKeyCombo, ZetaSmartLists, Grids, DBGrids, ZetaDBGrid;

type
  TCatModelosEdit = class(TBaseEdicion)
    PanelMaster: TPanel;
    WM_CODIGOlbl: TLabel;
    WM_CODIGO: TZetaDBEdit;
    WM_NOMBRE: TDBEdit;
    WM_NOMBRElbl: TLabel;
    WM_DESCRIPlbl: TLabel;
    WM_DESCRIP: TDBMemo;
    WM_STATUSlbl: TLabel;
    WM_STATUS: TZetaDBTextBox;
    dsRoles: TDataSource;
    PageControl: TPageControl;
    Paginas: TTabSheet;
    WM_URLlbl: TLabel;
    WM_URL_OKlbl: TLabel;
    WM_URL_EXAlbl: TLabel;
    WM_URL_DELlbl: TLabel;
    Formatos: TTabSheet;
    ModelosGB: TGroupBox;
    WM_FORMATOlbl: TLabel;
    Roles: TTabSheet;
    PanelRoles: TPanel;
    BorrarRol: TSpeedButton;
    AgregarRol: TSpeedButton;
    AgregarRolesTodos: TSpeedButton;
    BorrarRolesTodos: TSpeedButton;
    gridModelos: TZetaDBGrid;
    WM_URL_DEL: TDBEdit;
    WM_URL_EXA: TDBEdit;
    WM_URL_OK: TDBEdit;
    WM_URL: TDBEdit;
    WM_FORMATO: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dsRolesDataChange(Sender: TObject; Field: TField);
    procedure AgregarRolClick(Sender: TObject);
    procedure BorrarRolClick(Sender: TObject);
    procedure AgregarRolesTodosClick(Sender: TObject);
    procedure BorrarRolesTodosClick(Sender: TObject);
    procedure DataSourceStateChange(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
    function PuedeBorrar(var sMensaje: String): Boolean;override;
  public
    { Public declarations }
  end;

var
  CatModelosEdit: TCatModelosEdit;

implementation

uses DWorkflow,
     ZHelpContext,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaBuscador,
     ZetaDialogo,
     ZAccesosTress, DSistema;

{$R *.DFM}

procedure TCatModelosEdit.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_WORKFLOWCFG_CATALOGOS_MODELOS_EDIC;
     IndexDerechos := D_CAT_MODELOS;
     FirstControl := WM_CODIGO;
end;

procedure TCatModelosEdit.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := Paginas;
     Roles.TabVisible := not (DataSource.State = dsInsert);
end;

procedure TCatModelosEdit.Connect;
begin
     with dmWorkflow do
     begin
          Datasource.Dataset := cdsModelos;
          dsRoles.Dataset := cdsModeloRoles;
     end;
end;

procedure TCatModelosEdit.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Modelo', 'Modelo', 'WM_CODIGO', dmWorkflow.cdsModelos );
end;

procedure TCatModelosEdit.dsRolesDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     BorrarRol.Enabled := not dsRoles.Dataset.IsEmpty;
     BorrarRolesTodos.Enabled := BorrarRol.Enabled;
end;

procedure TCatModelosEdit.AgregarRolClick(Sender: TObject);
var
   sRol, sNombre: String;
begin
     inherited;
     with dmWorkflow do
     begin
          if dmSistema.RolBuscar( '', sRol, sNombre ) then
          begin
               dmWorkflow.ModeloRolAgregar( sRol, sNombre );
          end;
     end;
end;

procedure TCatModelosEdit.BorrarRolClick(Sender: TObject);
begin
     inherited;
     dmWorkflow.ModeloRolBorrar;
end;

procedure TCatModelosEdit.AgregarRolesTodosClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     if ZetaDialogo.zConfirm( 'Agregar Todos los Roles', '� Desea Agregar Todos Los Roles A Este Modelo ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             dmWorkFlow.ModeloRolAgregarTodos;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TCatModelosEdit.BorrarRolesTodosClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     if ZetaDialogo.zConfirm( 'Borrar Todos los Roles', '� Desea Remover Todos Los Roles De Este Modelo ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             dmWorkFlow.ModeloRolBorrarTodos;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TCatModelosEdit.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     Roles.TabVisible := not (DataSource.State = dsInsert);
end;

function TCatModelosEdit.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := FALSE;
     sMensaje := 'No es posible borrar Modelos';
end;

end.
