unit FCatUsuariosEdit;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask, Grids, DBGrids,
     ZBaseEdicion,
     ZetaDBTextBox,
     ZetaEdit,
     ZetaNumero,
     ZetaDBGrid,
     ZetaKeyCombo,
     ZetaKeyLookup, ZetaSmartLists;

type
  TCatUsuariosEdit = class(TBaseEdicion)
    PanelControles: TPanel;
    RO_NOMBRElbl: TLabel;
    US_CODIGOlbl: TLabel;
    US_JEFElbl: TLabel;
    RolesGB: TGroupBox;
    PanelAgregar: TPanel;
    BorrarRol: TSpeedButton;
    ZetaDBGrid: TZetaDBGrid;
    dsRoles: TDataSource;
    AgregarRol: TSpeedButton;
    US_CODIGO: TZetaDBTextBox;
    US_NOMBRE: TZetaDBTextBox;
    US_JEFE: TZetaDBKeyLookup;
    eMailGB: TGroupBox;
    US_FORMATOlbl: TLabel;
    US_FORMATO: TZetaDBKeyCombo;
    US_EMAILlbl: TLabel;
    US_EMAIL: TZetaDBEdit;
    AgregarRolesTodos: TSpeedButton;
    BorrarRolesTodos: TSpeedButton;
    Label1: TLabel;
    US_DOMAIN: TZetaDBEdit;
    fueraOficinaGB: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    US_WF_ALT: TZetaDBKeyLookup;
    US_WF_OUT: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure AgregarRolClick(Sender: TObject);
    procedure BorrarRolClick(Sender: TObject);
    procedure dsRolesDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure AgregarRolesTodosClick(Sender: TObject);
    procedure BorrarRolesTodosClick(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  CatUsuariosEdit: TCatUsuariosEdit;

implementation

uses DWorkFlow,
     DSistema,
     ZHelpContext,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaBuscador,
     ZetaDialogo,
     ZAccesosTress;

{$R *.DFM}

procedure TCatUsuariosEdit.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_WORKFLOWCFG_CATALOGOS_USUARIOS_EDIC;
     IndexDerechos := D_CAT_USUARIOS;
     FirstControl := US_JEFE;
     US_JEFE.LookupDataset := dmSistema.cdsUsuariosLookup;
     US_WF_ALT.LookupDataset := dmSistema.cdsUsuariosLookup;
     US_FORMATO.ListaFija := lfEMailType;
end;

procedure TCatUsuariosEdit.FormShow(Sender: TObject);
begin
     inherited;
     US_JEFE.Filtro := Format( 'US_CODIGO <> %d', [ dmSistema.GetUsuarioActual ] );
end;

procedure TCatUsuariosEdit.Connect;
begin
     with dmSistema do
     begin
          ConectarUsuariosLookup;
          Datasource.Dataset := cdsUsuariosRol;
          dsRoles.Dataset := cdsUsuarioRoles;
     end;
end;

procedure TCatUsuariosEdit.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Rol', 'Rol', 'RO_CODIGO', dmSistema.cdsRoles );
end;

function TCatUsuariosEdit.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Permite Agregar Usuarios En Esta Pantalla';
end;

function TCatUsuariosEdit.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Permite Borrar Usuarios En Esta Pantalla';
end;

procedure TCatUsuariosEdit.AgregarRolClick(Sender: TObject);
var
   sRol, sNombre: String;
begin
     inherited;
     with dmWorkflow do
     begin
          with dmSistema do
          begin
               ConectarRolesLookup;
               if cdsRoles.Search( '', sRol, sNombre ) then
               begin
                    dmSistema.UsuarioRolAgregar( sRol, sNombre );
               end;
          end;
     end;
end;

procedure TCatUsuariosEdit.BorrarRolClick(Sender: TObject);
begin
     inherited;
     dmSistema.UsuarioRolBorrar;
end;

procedure TCatUsuariosEdit.AgregarRolesTodosClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     if ZetaDialogo.zConfirm( 'Agregar Todos los Roles', '� Desea Asignar Todos Los Roles A Este Usuario ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             dmSistema.UsuarioRolAgregarTodos;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TCatUsuariosEdit.BorrarRolesTodosClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     if ZetaDialogo.zConfirm( 'Borrar Todos los Roles', '� Desea Remover Todos Los Roles Asignados A Este Usuario ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             dmSistema.UsuarioRolBorrarTodos;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TCatUsuariosEdit.dsRolesDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     BorrarRol.Enabled := not dsRoles.Dataset.IsEmpty;
     BorrarRolesTodos.Enabled := BorrarRol.Enabled;
end;

end.
