unit FCatPasosModeloEdit;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask, ComCtrls,
     ZBaseEdicionRenglon, ZetaKeyLookup, ZetaEdit, ZetaKeyCombo,
  ZetaNumero, ZetaDBTextBox, Grids, DBGrids, ZetaDBGrid, ZetaSmartLists;



type
  TCatPasosModeloEdit = class(TBaseEdicionRenglon)
    dsModelo: TDataSource;
    Label4: TLabel;
    minutosVencimiento: TZetaNumero;
    Label3: TLabel;
    horasVencimiento: TZetaNumero;
    Label2: TLabel;
    diasVencimiento: TZetaNumero;
    Label1: TLabel;
    WE_ORDENlbl: TLabel;
    WE_ORDEN: TZetaDBTextBox;
    WM_CODIGOlbl: TLabel;
    WM_CODIGO: TZetaDBTextBox;
    WM_NOMBRE: TZetaDBTextBox;
    WE_TIPDEST: TZetaDBKeyCombo;
    WE_USRDEST: TZetaDBEdit;
    WE_TIPDESTlbl: TLabel;
    WE_DESCRIP: TDBMemo;
    WE_DESCRIPlbl: TLabel;
    WE_NOMBRElb: TLabel;
    WE_NOMBRE: TZetaDBEdit;
    WA_CODIGOlbl: TLabel;
    WA_CODIGO: TZetaDBKeyLookup;
    WE_USRDESTFind: TSpeedButton;
    Formatos: TTabSheet;
    FormatosAvisosGB: TGroupBox;
    WE_XSL_TARlbl: TLabel;
    WE_XSL_TAR: TZetaDBEdit;
    PaginasWeb: TTabSheet;
    WE_URL_OK: TZetaDBEdit;
    WM_URL_OKlbl: TLabel;
    Conexiones: TTabSheet;
    Label5: TLabel;
    WX_FOLIO: TZetaDBKeyLookup;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure WE_USRDESTFindClick(Sender: TObject);
    procedure WE_TIPNOT1Click(Sender: TObject);
    procedure WE_TIPNOT2Click(Sender: TObject);
    procedure WE_TIPNOT3Click(Sender: TObject);
    procedure WE_USRNOT1FindClick(Sender: TObject);
    procedure WE_USRNOT2FindClick(Sender: TObject);
    procedure WE_USRNOT3FindClick(Sender: TObject);
    procedure diasVencimientoExit(Sender: TObject);
    procedure horasVencimientoExit(Sender: TObject);
    procedure minutosVencimientoExit(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure WE_TIPDESTChange(Sender: TObject);
    procedure WA_CODIGOValidKey(Sender: TObject);
  private
    { Private declarations }
    rDiasActual, rHorasActual, rMinutosActual : Integer;
    FTipoNotificacion : Integer;
    procedure SetControls;
    procedure SetPageControl;
    procedure CambiaLimitePaso;
    procedure VerificarDestino;
    procedure ActualizaTiempos;{OP: 31.Diciembre.2008}
  protected
    { Protected declarations }
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  CatPasosModeloEdit: TCatPasosModeloEdit;

implementation

uses DWorkflow,
     DWorkFlowTypes,
     ZHelpContext,
     ZetaCommonClasses,
     ZetaBuscador,
     ZAccesosTress,
     ZetaDialogo,
     ZetaCommonLists;{TMP}

{$R *.DFM}

procedure TCatPasosModeloEdit.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_WORKFLOWCFG_CATALOGOS_PASOSMODELOS_EDIC;
     IndexDerechos := D_CAT_MODELOS;
     SetPageControl;
     FirstControl := WA_CODIGO;
     ZetaCommonLists.LlenaNotificacionItems( WE_TIPDEST.Items );
     //ZetaCommonLists.LlenaNotificacionItems( WE_TIPNOT1.Items );
     //ZetaCommonLists.LlenaNotificacionItems( WE_TIPNOT2.Items );
     //ZetaCommonLists.LlenaNotificacionItems( WE_TIPNOT3.Items );
     WA_CODIGO.LookupDataset := dmWorkflow.cdsAcciones;
     WX_FOLIO.LookupDataset := dmWorkflow.cdsConexiones;
     FTipoNotificacion := -1;

end;

procedure TCatPasosModeloEdit.FormShow(Sender: TObject);
begin
     inherited;
     SetPageControl;
     SetControls;
end;

procedure TCatPasosModeloEdit.SetPageControl;
begin
     PageControl.ActivePage := Datos;
end;

procedure TCatPasosModeloEdit.SetControls;
begin
     ActualizaTiempos;{OP: 31.Diciembre.2008}
     VerificarDestino;
     //WE_TIPNOT1Click( Self );
     //WE_TIPNOT2Click( Self );
     //WE_TIPNOT3Click( Self );
end;

{OP: 31.Diciembre.2008}
procedure TCatPasosModeloEdit.ActualizaTiempos;
var iTotalMinutos: Integer;
begin
     iTotalMinutos := dmWorkFlow.cdsPasosModelo.FieldByName('WE_LIMITE').AsInteger;
     rDiasActual := Trunc(iTotalMinutos / 1440);
     iTotalMinutos := iTotalMinutos MOD 1440;
     rHorasActual := Trunc ( iTotalMinutos / 60 );
     rMinutosActual := iTotalMinutos MOD 60;
     diasVencimiento.Valor := rDiasActual;
     horasVencimiento.Valor := rHorasActual;
     minutosVencimiento.Valor := rMinutosActual;
end;

procedure TCatPasosModeloEdit.Connect;
//var iTotalMinutos: Integer;
begin
     with dmWorkflow do
     begin
          cdsModelos.Conectar;
          cdsAcciones.Conectar;
          cdsConexiones.Conectar;
          Datasource.Dataset := cdsPasosModelo;
          dsModelo.Dataset := cdsModelos;
          dsRenglon.Dataset := cdsNotificaciones;
          //iTotalMinutos := cdsPasosModelo.FieldByName('WE_LIMITE').AsInteger;{OP: 31.Diciembre.2008}
     end;
     {OP: 31.Diciembre.2008}
     {rDiasActual := Trunc(iTotalMinutos / 1440);
     iTotalMinutos := iTotalMinutos MOD 1440;
     rHorasActual := Trunc ( iTotalMinutos / 60 );
     rMinutosActual := iTotalMinutos MOD 60;
     diasVencimiento.Valor := rDiasActual;
     horasVencimiento.Valor := rHorasActual;
     minutosVencimiento.Valor := rMinutosActual;}
     SetControls;
end;

procedure TCatPasosModeloEdit.WE_USRDESTFindClick(Sender: TObject);
begin
     inherited;
     dmWorkflow.PasoModeloModificarResponsable( eWFNotificacion( WE_TIPDEST.Valor ), 'WE_USRDEST' );
end;

procedure TCatPasosModeloEdit.WE_TIPNOT1Click(Sender: TObject);
begin
     inherited;
     //WE_USRNOT1.Enabled := ZetaCommonLists.GetNotificacionDebeEspecificar( eWFNotificacion( WE_TIPNOT1.Valor ) );
     //WE_USRNOT1Find.Enabled := WE_USRNOT1.Enabled;
end;

procedure TCatPasosModeloEdit.WE_USRNOT1FindClick(Sender: TObject);
begin
     inherited;
     //dmWorkflow.PasoModeloModificarResponsable( eWFNotificacion( WE_TIPNOT1.Valor ), 'WE_USRNOT1' );
end;

procedure TCatPasosModeloEdit.WE_TIPNOT2Click(Sender: TObject);
begin
     inherited;
     //WE_USRNOT2.Enabled := ZetaCommonLists.GetNotificacionDebeEspecificar( eWFNotificacion( WE_TIPNOT2.Valor ) );
     //WE_USRNOT2Find.Enabled := WE_USRNOT2.Enabled;
end;

procedure TCatPasosModeloEdit.WE_USRNOT2FindClick(Sender: TObject);
begin
     inherited;
     //dmWorkflow.PasoModeloModificarResponsable( eWFNotificacion( WE_TIPNOT2.Valor ), 'WE_USRNOT2' );
end;

procedure TCatPasosModeloEdit.WE_TIPNOT3Click(Sender: TObject);
begin
     inherited;
     //WE_USRNOT3.Enabled := ZetaCommonLists.GetNotificacionDebeEspecificar( eWFNotificacion( WE_TIPNOT3.Valor ) );
     //WE_USRNOT3Find.Enabled := WE_USRNOT3.Enabled;
end;

procedure TCatPasosModeloEdit.WE_USRNOT3FindClick(Sender: TObject);
begin
     inherited;
     //dmWorkflow.PasoModeloModificarResponsable( eWFNotificacion( WE_TIPNOT3.Valor ), 'WE_USRNOT3' );
end;

procedure TCatPasosModeloEdit.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     SetControls;
     if ( ( Field = nil ) and ( dmWorkflow.cdsNotificaciones.ChangeCount = 0 ) ) then
        dmWorkFlow.cdsNotificaciones.Refrescar;
end;

Procedure TCatPasosModeloEdit.VerificarDestino;
begin
     WE_USRDEST.Enabled := ZetaCommonLists.GetNotificacionDebeEspecificar( eWFNotificacion( WE_TIPDEST.Valor ) );
     WE_USRDESTFind.Enabled := WE_USRDEST.Enabled;

     {OP: 31.Diciembre.2008}
     if( Not WE_USRDEST.Enabled )then
         WE_USRDEST.Text := '';
     {Se comenta por que esta produciendo un comportamiento equivocado.}
     {
     if  ( FTipoNotificacion <> -1 ) and ( WE_TIPDEST.Valor  <> FTipoNotificacion )  then
         WE_USRDEST.Text := '';
     FTipoNOtificacion := WE_TIPDEST.Valor ;
     }
end;





procedure TCatPasosModeloEdit.diasVencimientoExit(Sender: TObject);
begin
     inherited;
     if (rDiasActual <> diasVencimiento.ValorEntero) then
     begin
          CambiaLimitePaso;
          rDiasActual := diasVencimiento.ValorEntero;
     end;

end;

procedure TCatPasosModeloEdit.horasVencimientoExit(Sender: TObject);
begin
     inherited;
     if (rHorasActual <> horasVencimiento.ValorEntero) then
     begin
          if horasVencimiento.Valor >= 24 then
          begin
             ZError('','Horas Tiene Que Ser Menor a 24',0);
             ActiveControl:= horasVencimiento;
          end
          else
          begin
               CambiaLimitePaso;
               rHorasActual := horasVencimiento.ValorEntero;
          end;
     end;

end;

procedure TCatPasosModeloEdit.minutosVencimientoExit(Sender: TObject);
begin
     inherited;
     if (rMinutosActual <> minutosVencimiento.ValorEntero) then
     begin
          if minutosVencimiento.Valor >= 60 then
          begin
             ZError('','Minutos Tiene Que Ser Menor a 60',0);
             ActiveControl:= minutosVencimiento;
          end
          else
          begin
               CambiaLimitePaso;
               rMinutosActual := minutosVencimiento.ValorEntero;
          end;
     end;
end;

procedure TCatPasosModeloEdit.CambiaLimitePaso;
begin
     With dmWorkFlow.cdsPasosModelo do
     begin
          if not Editing then
             Edit;
          FieldByName('WE_LIMITE').AsInteger := diasVencimiento.ValorEntero * 24 * 60 + horasVencimiento.ValorEntero * 60 + minutosVencimiento.ValorEntero;
     end;
end;   

procedure TCatPasosModeloEdit.BBAgregarClick(Sender: TObject);
begin
       if CheckDerechosPadre then
       begin
            with ClientDatasetHijo do
            begin
                 Append;
                 Agregar;
            end;
       end;
end;

procedure TCatPasosModeloEdit.BBModificarClick(Sender: TObject);
begin
      if CheckDerechosPadre then
      begin
           with ClientDatasetHijo do
           begin
                Modificar;
           end;
      end;

end;

procedure TCatPasosModeloEdit.CancelarClick(Sender: TObject);
begin
     inherited;
     dmWorkflow.cdsNotificaciones.CancelUpdates;
end;

procedure TCatPasosModeloEdit.WE_TIPDESTChange(Sender: TObject);
begin
     VerificarDestino;
end;

procedure TCatPasosModeloEdit.WA_CODIGOValidKey(Sender: TObject);
begin
     inherited;
     Conexiones.TabVisible := ( WA_CODIGO.LLave = ACCION_CALL_WEBSERVICE );


end;

end.
