inherited CatUsuariosEdit: TCatUsuariosEdit
  Left = 284
  Top = 110
  ActiveControl = US_JEFE
  Caption = 'Roles Por Usuario'
  ClientHeight = 510
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 474
    BevelOuter = bvNone
  end
  object PanelControles: TPanel [3]
    Left = 0
    Top = 51
    Width = 473
    Height = 234
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object RO_NOMBRElbl: TLabel
      Left = 62
      Top = 31
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'N&ombre:'
    end
    object US_CODIGOlbl: TLabel
      Left = 66
      Top = 9
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = '&C'#243'digo:'
    end
    object US_JEFElbl: TLabel
      Left = 52
      Top = 54
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = '&Reporta a:'
      FocusControl = US_JEFE
    end
    object US_CODIGO: TZetaDBTextBox
      Left = 105
      Top = 7
      Width = 65
      Height = 17
      AutoSize = False
      Caption = 'US_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'US_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object US_NOMBRE: TZetaDBTextBox
      Left = 105
      Top = 29
      Width = 347
      Height = 17
      AutoSize = False
      Caption = 'US_NOMBRE'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'US_NOMBRE'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label1: TLabel
      Left = 19
      Top = 76
      Width = 83
      Height = 13
      Alignment = taRightJustify
      Caption = 'Usuario &windows:'
      FocusControl = US_DOMAIN
    end
    object US_JEFE: TZetaDBKeyLookup
      Left = 105
      Top = 49
      Width = 349
      Height = 21
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      DataField = 'US_JEFE'
      DataSource = DataSource
    end
    object eMailGB: TGroupBox
      Left = 0
      Top = 96
      Width = 473
      Height = 69
      Align = alBottom
      Caption = ' Correo Electr'#243'nico '
      TabOrder = 2
      object US_FORMATOlbl: TLabel
        Left = 61
        Top = 44
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = '&Formato:'
        FocusControl = US_FORMATO
      end
      object US_EMAILlbl: TLabel
        Left = 54
        Top = 20
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = '&Direcci'#243'n:'
        FocusControl = US_EMAIL
      end
      object US_FORMATO: TZetaDBKeyCombo
        Left = 105
        Top = 40
        Width = 168
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'US_FORMATO'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object US_EMAIL: TZetaDBEdit
        Left = 105
        Top = 17
        Width = 347
        Height = 21
        TabOrder = 0
        Text = 'US_EMAIL'
        DataField = 'US_EMAIL'
        DataSource = DataSource
      end
    end
    object US_DOMAIN: TZetaDBEdit
      Left = 105
      Top = 73
      Width = 347
      Height = 21
      TabOrder = 1
      Text = 'US_DOMAIN'
      DataField = 'US_DOMAIN'
      DataSource = DataSource
    end
    object fueraOficinaGB: TGroupBox
      Left = 0
      Top = 165
      Width = 473
      Height = 69
      Align = alBottom
      Caption = ' Fuera de la oficina '
      TabOrder = 3
      object Label2: TLabel
        Left = 2
        Top = 44
        Width = 100
        Height = 13
        Alignment = taRightJustify
        Caption = 'Respons&able alterno:'
      end
      object Label3: TLabel
        Left = 12
        Top = 21
        Width = 90
        Height = 13
        Alignment = taRightJustify
        Caption = '&Fuera de la oficina:'
      end
      object US_WF_ALT: TZetaDBKeyLookup
        Left = 105
        Top = 40
        Width = 349
        Height = 21
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'US_WF_ALT'
        DataSource = DataSource
      end
      object US_WF_OUT: TDBCheckBox
        Left = 105
        Top = 20
        Width = 97
        Height = 17
        DataField = 'US_WF_OUT'
        DataSource = DataSource
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
    end
  end
  object RolesGB: TGroupBox [4]
    Left = 0
    Top = 285
    Width = 473
    Height = 189
    Align = alClient
    Caption = ' Roles '
    TabOrder = 4
    object PanelAgregar: TPanel
      Left = 431
      Top = 15
      Width = 40
      Height = 172
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        40
        172)
      object BorrarRol: TSpeedButton
        Left = 4
        Top = 39
        Width = 34
        Height = 31
        Hint = 'Remover Al Usuarios Del Rol'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33333333FF33333333FF333993333333300033377F3333333777333993333333
          300033F77FFF3333377739999993333333333777777F3333333F399999933333
          33003777777333333377333993333333330033377F3333333377333993333333
          3333333773333333333F333333333333330033333333F33333773333333C3333
          330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
          993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
          333333333337733333FF3333333C333330003333333733333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = BorrarRolClick
      end
      object AgregarRol: TSpeedButton
        Left = 3
        Top = 3
        Width = 34
        Height = 31
        Hint = 'Agregar Un Usuario Al Rol'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          300033FFFFFF3333377739999993333333333777777F3333333F399999933333
          3300377777733333337733333333333333003333333333333377333333333333
          3333333333333333333F333333333333330033333F33333333773333C3333333
          330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
          993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
          333333377F33333333FF3333C333333330003333733333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = AgregarRolClick
      end
      object AgregarRolesTodos: TSpeedButton
        Left = 4
        Top = 103
        Width = 34
        Height = 31
        Hint = 'Asignar Todos Los Roles A Este Usuario'
        Anchors = [akLeft, akBottom]
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF003FFFFFFFFFFF
          FFFF33333333333FFFFF3FFFFFFFFF00000F333333333377777F33FFFFFFFF09
          990F33333333337F337F333FFFFFFF09990F33333333337F337F3333FFFFFF09
          990F33333333337FFF7F33333FFFFF00000F3333333333777773333333FFFFFF
          FFFF3FFFFF3333333F330000033FFFFF0FFF77777F3333337FF30EEE0333FFF0
          00FF7F337FFF333777FF0EEE00033F00000F7F33777F3777777F0EEE0E033000
          00007FFF7F7FF777777700000E00033000FF777773777F3777F3330EEE0E0330
          00FF337FFF7F7F3777F33300000E033000FF337777737F37773333330EEE0300
          03FF33337FFF77777333333300000333333F3333777773333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = AgregarRolesTodosClick
      end
      object BorrarRolesTodos: TSpeedButton
        Left = 4
        Top = 138
        Width = 34
        Height = 31
        Hint = 'Remover Todos Los Roles Asignados A Este Usuario'
        Anchors = [akLeft, akBottom]
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00500005000555
          555557777F777555F55500000000555055557777777755F75555005500055055
          555577F5777F57555555005550055555555577FF577F5FF55555500550050055
          5555577FF77577FF555555005050110555555577F757777FF555555505099910
          555555FF75777777FF555005550999910555577F5F77777775F5500505509990
          3055577F75F77777575F55005055090B030555775755777575755555555550B0
          B03055555F555757575755550555550B0B335555755555757555555555555550
          BBB35555F55555575F555550555555550BBB55575555555575F5555555555555
          50BB555555555555575F555555555555550B5555555555555575}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = BorrarRolesTodosClick
      end
    end
    object ZetaDBGrid: TZetaDBGrid
      Left = 2
      Top = 15
      Width = 429
      Height = 172
      Align = alClient
      DataSource = dsRoles
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
      ReadOnly = True
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'RO_CODIGO'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RO_NOMBRE'
          Title.Caption = 'Nombre'
          Width = 302
          Visible = True
        end>
    end
  end
  inherited DataSource: TDataSource
    Left = 244
    Top = 129
  end
  object dsRoles: TDataSource
    OnDataChange = dsRolesDataChange
    Left = 216
    Top = 129
  end
end
