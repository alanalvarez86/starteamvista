inherited CatNotificacionesEdit: TCatNotificacionesEdit
  Left = 301
  Top = 172
  Caption = 'Notificaciones'
  ClientHeight = 398
  PixelsPerInch = 96
  TextHeight = 13
  object lblVeces: TLabel [0]
    Left = 4
    Top = 210
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cuantas veces:'
    FocusControl = WY_VECES
  end
  object lblObservaciones: TLabel [1]
    Left = 4
    Top = 235
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observaciones:'
    FocusControl = WY_TEXTO
  end
  object lblQuien: TLabel [2]
    Left = 39
    Top = 58
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'A quien:'
    FocusControl = WY_QUIEN
  end
  object lblResponsable: TLabel [3]
    Left = 13
    Top = 82
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Caption = 'Responsable:'
    FocusControl = WY_USR_NOT
  end
  object Label3: TLabel [4]
    Left = 38
    Top = 108
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cuando:'
    FocusControl = WY_CUANDO
  end
  object lblAccion: TLabel [5]
    Left = 42
    Top = 133
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Acci'#243'n:'
    FocusControl = WY_QUE
  end
  object Label5: TLabel [6]
    Left = 280
    Top = 157
    Width = 37
    Height = 13
    Alignment = taRightJustify
    Caption = 'Minutos'
  end
  object Label6: TLabel [7]
    Left = 199
    Top = 157
    Width = 28
    Height = 13
    Alignment = taRightJustify
    Caption = 'Horas'
  end
  object Label7: TLabel [8]
    Left = 123
    Top = 157
    Width = 23
    Height = 13
    Alignment = taRightJustify
    Caption = 'D'#237'as'
  end
  object lblTiempo: TLabel [9]
    Left = 40
    Top = 156
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tiempo:'
  end
  object lblIntervalo: TLabel [10]
    Left = 34
    Top = 181
    Width = 44
    Height = 13
    Alignment = taRightJustify
    Caption = 'Intervalo:'
  end
  object Label10: TLabel [11]
    Left = 123
    Top = 182
    Width = 23
    Height = 13
    Alignment = taRightJustify
    Caption = 'D'#237'as'
  end
  object Label11: TLabel [12]
    Left = 199
    Top = 182
    Width = 28
    Height = 13
    Alignment = taRightJustify
    Caption = 'Horas'
  end
  object Label12: TLabel [13]
    Left = 280
    Top = 182
    Width = 37
    Height = 13
    Alignment = taRightJustify
    Caption = 'Minutos'
  end
  object lblXSL: TLabel [14]
    Left = 39
    Top = 334
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Plantilla:'
    FocusControl = WY_XSL
  end
  object WY_USR_NOTFind: TSpeedButton [15]
    Left = 202
    Top = 81
    Width = 23
    Height = 22
    Hint = 'Buscar Elemento'
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D808DDDDDDDD
      DDDD83308DDDDDDDDDDD873308DDDDDDDDDDD373308DDDDDDDDDDD3733088888
      8DDDDDD37330000088DDDDDD37338777808DDDDDD038F7F77808DDDDD08FCCCC
      C788DDDCD0F7C7F7C770DDDCC07FCCCCCF70DDDCD0F7CFFCF770DDDDC88FCCCC
      7F88DDDDDD08F7F7F80DDDDDDDD08F7F80DDDDDDDDDD80008DDD}
    ParentShowHint = False
    ShowHint = True
    OnClick = WY_USR_NOTFindClick
  end
  inherited PanelBotones: TPanel
    Top = 362
    BevelOuter = bvNone
  end
  object WY_XSL: TDBEdit [19]
    Left = 80
    Top = 330
    Width = 364
    Height = 21
    DataField = 'WY_XSL'
    DataSource = DataSource
    TabOrder = 15
  end
  object WY_TEXTO: TDBMemo [20]
    Left = 80
    Top = 232
    Width = 363
    Height = 89
    DataField = 'WY_TEXTO'
    DataSource = DataSource
    ScrollBars = ssVertical
    TabOrder = 14
  end
  object TiempoMinutos: TZetaNumero [21]
    Left = 237
    Top = 156
    Width = 33
    Height = 21
    Mascara = mnDias
    TabOrder = 9
    Text = '0'
    OnExit = TiempoMinutosExit
  end
  object TiempoHoras: TZetaNumero [22]
    Left = 156
    Top = 156
    Width = 33
    Height = 21
    Mascara = mnDias
    TabOrder = 8
    Text = '0'
    OnExit = TiempoHorasExit
  end
  object TiempoDias: TZetaNumero [23]
    Left = 80
    Top = 156
    Width = 33
    Height = 21
    Mascara = mnDias
    TabOrder = 7
    Text = '0'
    OnExit = TiempoDiasExit
  end
  object IntervaloDias: TZetaNumero [24]
    Left = 80
    Top = 181
    Width = 33
    Height = 21
    Mascara = mnDias
    TabOrder = 10
    Text = '0'
    OnExit = IntervaloDiasExit
  end
  object IntervaloHoras: TZetaNumero [25]
    Left = 156
    Top = 181
    Width = 33
    Height = 21
    Mascara = mnDias
    TabOrder = 11
    Text = '0'
    OnExit = IntervaloHorasExit
  end
  object IntervaloMinutos: TZetaNumero [26]
    Left = 237
    Top = 181
    Width = 33
    Height = 21
    Mascara = mnDias
    TabOrder = 12
    Text = '0'
    OnExit = IntervaloMinutosExit
  end
  object WY_VECES: TZetaDBNumero [27]
    Left = 80
    Top = 207
    Width = 33
    Height = 21
    Mascara = mnDias
    TabOrder = 13
    Text = '0'
    DataField = 'WY_VECES'
    DataSource = DataSource
  end
  object WY_QUE: TZetaDBKeyCombo [28]
    Left = 80
    Top = 131
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 6
    OnChange = WY_QUEChange
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    DataField = 'WY_QUE'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object WY_CUANDO: TZetaDBKeyCombo [29]
    Left = 80
    Top = 106
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 5
    OnChange = WY_CUANDOChange
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    DataField = 'WY_CUANDO'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object WY_QUIEN: TZetaDBKeyCombo [30]
    Left = 80
    Top = 56
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 3
    OnChange = WY_QUIENChange
    OnClick = WY_QUIENClick
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    DataField = 'WY_QUIEN'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object WY_USR_NOT: TZetaDBEdit [31]
    Left = 80
    Top = 81
    Width = 118
    Height = 21
    TabOrder = 4
    Text = 'WY_USR_NOT'
    DataField = 'WY_USR_NOT'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 20
    Top = 273
  end
end
