unit FCatRoles;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids, Buttons, StdCtrls,
     ZBaseConsulta,
     ZetaDBGrid;

type
  TCatRoles = class(TBaseConsulta)
    ZetaDBGrid: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  CatRoles: TCatRoles;

implementation

{$R *.DFM}

uses DSistema,
     ZHelpContext,
     ZAccesosTress,
     ZetaCommonTools,
     ZetaCommonClasses;

{ *********** TSistUsuarios *********** }

procedure TCatRoles.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_WORKFLOWCFG_CATALOGOS_ROLES;
     IndexDerechos := D_CAT_ROLES;
end;

procedure TCatRoles.Connect;
begin
     with dmSistema do
     begin
          cdsRoles.Conectar;
          DataSource.DataSet:= cdsRoles;
     end;
end;

procedure TCatRoles.Refresh;
begin
     dmSistema.cdsRoles.Refrescar;
end;

procedure TCatRoles.Agregar;
begin
     dmSistema.cdsRoles.Agregar;
end;

procedure TCatRoles.Borrar;
begin
     dmSistema.cdsRoles.Borrar;
end;

procedure TCatRoles.Modificar;
begin
     dmSistema.cdsRoles.Modificar;
end;

end.


