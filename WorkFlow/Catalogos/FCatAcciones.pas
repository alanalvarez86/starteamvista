unit FCatAcciones;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids, Buttons, StdCtrls,
     ZBaseConsulta,
     ZetaDBGrid;

type
  TCatAcciones = class(TBaseConsulta)
    ZetaDBGrid: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure ZetaDBGridTitleClick(Column: TColumn);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
    function PuedeImprimir(var sMensaje: String): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  CatAcciones: TCatAcciones;

implementation

{$R *.DFM}

uses DWorkflow,
     ZHelpContext,
     ZAccesosTress,
     ZetaCommonTools,
     ZetaCommonClasses;

{ *********** TSistUsuarios *********** }

procedure TCatAcciones.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_WORKFLOWCFG_CATALOGOS_ACCIONES;
     IndexDerechos := D_CAT_ACCIONES;
end;

procedure TCatAcciones.Connect;
begin
     with dmWorkflow do
     begin
          cdsAcciones.Conectar;
          DataSource.DataSet:= cdsAcciones;
     end;
end;

procedure TCatAcciones.Refresh;
begin
     dmWorkflow.cdsAcciones.Refrescar;
end;

function TCatAcciones.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Es Posible Agregar Acciones';
end;

function TCatAcciones.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Es Posible Borrar Acciones';
end;

function TCatAcciones.PuedeModificar(var sMensaje: String): Boolean;
begin
     {
     Se verifica El Derecho De Alta debido a
     que en el �rbol de Derecho no Aparecen
     Los Derechos De Alta y De Baja
     }
     Result := CheckDerechos( K_DERECHO_CAMBIO ); //K_DERECHO_ALTA );
     if not Result then
        sMensaje := 'No Est� Permitido Modificar Acciones';
end;

function TCatAcciones.PuedeImprimir(var sMensaje: String): Boolean;
begin
     {
     Se verifica El Derecho De Baja debido a
     que en el �rbol de Derecho no Aparecen
     Los Derechos De Alta y De Baja
     }
     Result := CheckDerechos( K_DERECHO_IMPRESION ); //K_DERECHO_BAJA );
     if not Result then
        sMensaje := 'No Est� Permitido Imprimir Acciones';
end;

procedure TCatAcciones.Modificar;
begin
     dmWorkflow.cdsAcciones.Modificar;
end;

procedure TCatAcciones.ZetaDBGridTitleClick(Column: TColumn);
begin
     inherited;
     with Column do
     begin
          if ( FieldName <> 'WA_DESCRIP' ) then
             dmWorkFlow.cdsAcciones.IndexFieldNames := FieldName;
     end;
end;

end.


