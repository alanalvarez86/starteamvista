inherited CatPasosModelo: TCatPasosModelo
  Left = 119
  Top = 210
  Caption = 'Pasos De Modelo'
  ClientHeight = 606
  ClientWidth = 849
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 849
    inherited Slider: TSplitter
      Left = 357
    end
    inherited ValorActivo1: TPanel
      Width = 341
    end
    inherited ValorActivo2: TPanel
      Left = 360
      Width = 489
    end
  end
  object PanelDetalle: TPanel [1]
    Left = 0
    Top = 19
    Width = 804
    Height = 587
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object ZetaDBGrid: TZetaDBGrid
      Left = 0
      Top = 35
      Width = 804
      Height = 552
      Align = alClient
      DataSource = DataSource
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnTitleClick = ZetaDBGridTitleClick
      Columns = <
        item
          Expanded = False
          FieldName = 'WE_ORDEN'
          Title.Caption = 'Orden'
          Width = 47
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'WE_NOMBRE'
          Title.Caption = 'Nombre'
          Width = 214
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'WE_DESCRIP'
          Title.Caption = 'Descripci'#243'n'
          Width = 179
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'WA_CODIGO'
          Title.Caption = 'Acci'#243'n'
          Width = 94
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'WE_TIPDEST'
          Title.Caption = 'Destino'
          Width = 116
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'WE_USRDEST'
          Title.Caption = 'Detalle'
          Width = 86
          Visible = True
        end>
    end
    object PanelCodigo: TPanel
      Left = 0
      Top = 0
      Width = 804
      Height = 35
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object ModeloLBL: TLabel
        Left = 4
        Top = 10
        Width = 38
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modelo:'
        FocusControl = Modelo
      end
      object Modelo: TZetaKeyCombo
        Left = 45
        Top = 6
        Width = 263
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
        OnClick = ModeloClick
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
      end
    end
  end
  object PanelMover: TPanel [2]
    Left = 804
    Top = 19
    Width = 45
    Height = 587
    Align = alRight
    Alignment = taLeftJustify
    BevelOuter = bvNone
    TabOrder = 2
    object Subir: TSpeedButton
      Left = 10
      Top = 84
      Width = 28
      Height = 28
      Hint = 'Mover Paso Hacia Arriba'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
        3333333333777F33333333333309033333333333337F7F333333333333090333
        33333333337F7F33333333333309033333333333337F7F333333333333090333
        33333333337F7F33333333333309033333333333FF7F7FFFF333333000090000
        3333333777737777F333333099999990333333373F3333373333333309999903
        333333337F33337F33333333099999033333333373F333733333333330999033
        3333333337F337F3333333333099903333333333373F37333333333333090333
        33333333337F7F33333333333309033333333333337373333333333333303333
        333333333337F333333333333330333333333333333733333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = SubirClick
    end
    object Bajar: TSpeedButton
      Left = 10
      Top = 124
      Width = 28
      Height = 28
      Hint = 'Mover Paso Hacia Abajo'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
        333333333337F33333333333333033333333333333373F333333333333090333
        33333333337F7F33333333333309033333333333337373F33333333330999033
        3333333337F337F33333333330999033333333333733373F3333333309999903
        333333337F33337F33333333099999033333333373333373F333333099999990
        33333337FFFF3FF7F33333300009000033333337777F77773333333333090333
        33333333337F7F33333333333309033333333333337F7F333333333333090333
        33333333337F7F33333333333309033333333333337F7F333333333333090333
        33333333337F7F33333333333300033333333333337773333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = BajarClick
    end
  end
  inherited DataSource: TDataSource
    Left = 280
    Top = 8
  end
end
