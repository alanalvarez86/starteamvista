unit FCatConexiones;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids, Buttons, StdCtrls,
     ZBaseConsulta,
     ZetaDBGrid;

type
  TCatConexiones = class(TBaseConsulta)
    ZetaDBGrid: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  CatConexiones: TCatConexiones;

implementation

{$R *.DFM}

uses DWorkflow,
     ZHelpContext,
     ZAccesosTress,
     ZetaCommonTools,
     ZetaCommonClasses;

{ *********** TSistUsuarios *********** }

procedure TCatConexiones.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_WORKFLOWCFG_CATALOGOS_CONEXIONES;
     IndexDerechos := D_CAT_CONEXIONES;
end;

procedure TCatConexiones.Connect;
begin
     with dmWorkflow do
     begin
          cdsConexiones.Conectar;
          DataSource.DataSet:= cdsConexiones;
     end;
end;

procedure TCatConexiones.Refresh;
begin
     dmWorkflow.cdsConexiones.Refrescar;
end;

procedure TCatConexiones.Modificar;
begin
     dmWorkflow.cdsConexiones.Modificar;
end;

procedure TCatConexiones.Agregar;
begin
     dmWorkflow.cdsConexiones.Agregar;
end;

end.


