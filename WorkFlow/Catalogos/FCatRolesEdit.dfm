inherited CatRolesEdit: TCatRolesEdit
  Left = 693
  Top = 158
  Caption = 'Rol'
  ClientHeight = 510
  ClientWidth = 460
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 474
    Width = 460
    BevelOuter = bvNone
    inherited OK: TBitBtn
      Left = 292
    end
    inherited Cancelar: TBitBtn
      Left = 377
    end
  end
  inherited PanelSuperior: TPanel
    Width = 460
  end
  inherited PanelIdentifica: TPanel
    Width = 460
    TabOrder = 4
    inherited ValorActivo2: TPanel
      Width = 134
      inherited textoValorActivo2: TLabel
        Width = 128
      end
    end
  end
  object PanelControles: TPanel [3]
    Left = 0
    Top = 51
    Width = 460
    Height = 170
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object RO_NOMBRElbl: TLabel
      Left = 38
      Top = 32
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'N&ombre:'
      FocusControl = RO_NOMBRE
    end
    object RO_CODIGOlbl: TLabel
      Left = 42
      Top = 10
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = '&C'#243'digo:'
      FocusControl = RO_CODIGO
    end
    object RO_DESCRIPlbl: TLabel
      Left = 19
      Top = 54
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = '&Descripci'#243'n:'
    end
    object RO_NIVELlbl: TLabel
      Left = 50
      Top = 145
      Width = 27
      Height = 13
      Alignment = taRightJustify
      Caption = '&Nivel:'
      FocusControl = RO_NOMBRE
    end
    object RO_CODIGO: TZetaDBEdit
      Left = 80
      Top = 5
      Width = 121
      Height = 21
      TabOrder = 0
      Text = 'RO_CODIGO'
      DataField = 'RO_CODIGO'
      DataSource = DataSource
    end
    object RO_NOMBRE: TDBEdit
      Left = 80
      Top = 28
      Width = 351
      Height = 21
      DataField = 'RO_NOMBRE'
      DataSource = DataSource
      TabOrder = 1
    end
    object RO_DESCRIP: TDBMemo
      Left = 80
      Top = 51
      Width = 350
      Height = 89
      DataField = 'RO_DESCRIP'
      DataSource = DataSource
      ScrollBars = ssVertical
      TabOrder = 2
    end
    object RO_NIVEL: TZetaDBNumero
      Left = 80
      Top = 142
      Width = 41
      Height = 21
      Mascara = mnDias
      TabOrder = 3
      Text = '0'
      DataField = 'RO_NIVEL'
      DataSource = DataSource
    end
  end
  object PageControl: TPageControl [4]
    Left = 0
    Top = 221
    Width = 460
    Height = 253
    ActivePage = Usuarios
    Align = alClient
    TabOrder = 2
    object Usuarios: TTabSheet
      Caption = 'Usuarios'
      object PanelAgregar: TPanel
        Left = 412
        Top = 0
        Width = 40
        Height = 225
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          40
          225)
        object BorrarUsuario: TSpeedButton
          Left = 4
          Top = 39
          Width = 34
          Height = 31
          Hint = 'Remover Al Usuario Del Rol'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33333333FF33333333FF333993333333300033377F3333333777333993333333
            300033F77FFF3333377739999993333333333777777F3333333F399999933333
            33003777777333333377333993333333330033377F3333333377333993333333
            3333333773333333333F333333333333330033333333F33333773333333C3333
            330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
            993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
            333333333337733333FF3333333C333330003333333733333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BorrarUsuarioClick
        end
        object AgregarUsuario: TSpeedButton
          Left = 3
          Top = 3
          Width = 34
          Height = 31
          Hint = 'Agregar Un Usuario Al Rol'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            300033FFFFFF3333377739999993333333333777777F3333333F399999933333
            3300377777733333337733333333333333003333333333333377333333333333
            3333333333333333333F333333333333330033333F33333333773333C3333333
            330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
            993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
            333333377F33333333FF3333C333333330003333733333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = AgregarUsuarioClick
        end
        object AgregarUsuariosTodos: TSpeedButton
          Left = 4
          Top = 156
          Width = 34
          Height = 31
          Hint = 'Asignar Todos Los Usuarios A Este Rol'
          Anchors = [akLeft, akBottom]
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF003FFFFFFFFFFF
            FFFF33333333333FFFFF3FFFFFFFFF00000F333333333377777F33FFFFFFFF09
            990F33333333337F337F333FFFFFFF09990F33333333337F337F3333FFFFFF09
            990F33333333337FFF7F33333FFFFF00000F3333333333777773333333FFFFFF
            FFFF3FFFFF3333333F330000033FFFFF0FFF77777F3333337FF30EEE0333FFF0
            00FF7F337FFF333777FF0EEE00033F00000F7F33777F3777777F0EEE0E033000
            00007FFF7F7FF777777700000E00033000FF777773777F3777F3330EEE0E0330
            00FF337FFF7F7F3777F33300000E033000FF337777737F37773333330EEE0300
            03FF33337FFF77777333333300000333333F3333777773333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = AgregarUsuariosTodosClick
        end
        object BorrarUsuariosTodos: TSpeedButton
          Left = 4
          Top = 191
          Width = 34
          Height = 31
          Hint = 'Remover Todos Los Usuarios Asignados A Este Rol'
          Anchors = [akLeft, akBottom]
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00500005000555
            555557777F777555F55500000000555055557777777755F75555005500055055
            555577F5777F57555555005550055555555577FF577F5FF55555500550050055
            5555577FF77577FF555555005050110555555577F757777FF555555505099910
            555555FF75777777FF555005550999910555577F5F77777775F5500505509990
            3055577F75F77777575F55005055090B030555775755777575755555555550B0
            B03055555F555757575755550555550B0B335555755555757555555555555550
            BBB35555F55555575F555550555555550BBB55575555555575F5555555555555
            50BB555555555555575F555555555555550B5555555555555575}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BorrarUsuariosTodosClick
        end
      end
      object gridUsuarios: TZetaDBGrid
        Left = 0
        Top = 0
        Width = 412
        Height = 225
        Align = alClient
        DataSource = dsUsuarios
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'US_CODIGO'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'US_NOMBRE'
            Title.Caption = 'Nombre'
            Width = 302
            Visible = True
          end>
      end
    end
    object Modelos: TTabSheet
      Caption = 'Modelos'
      ImageIndex = 1
      object PanelModelos: TPanel
        Left = 412
        Top = 0
        Width = 40
        Height = 225
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          40
          225)
        object BorrarModelo: TSpeedButton
          Left = 4
          Top = 39
          Width = 34
          Height = 31
          Hint = 'Remover Al Modelo Del Rol'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33333333FF33333333FF333993333333300033377F3333333777333993333333
            300033F77FFF3333377739999993333333333777777F3333333F399999933333
            33003777777333333377333993333333330033377F3333333377333993333333
            3333333773333333333F333333333333330033333333F33333773333333C3333
            330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
            993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
            333333333337733333FF3333333C333330003333333733333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BorrarModeloClick
        end
        object AgregarModelo: TSpeedButton
          Left = 3
          Top = 3
          Width = 34
          Height = 31
          Hint = 'Agregar Un Modelo Al Rol'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            300033FFFFFF3333377739999993333333333777777F3333333F399999933333
            3300377777733333337733333333333333003333333333333377333333333333
            3333333333333333333F333333333333330033333F33333333773333C3333333
            330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
            993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
            333333377F33333333FF3333C333333330003333733333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = AgregarModeloClick
        end
        object AgregarModelosTodos: TSpeedButton
          Left = 4
          Top = 156
          Width = 34
          Height = 31
          Hint = 'Asignar Todos Los Modelos A Este Rol'
          Anchors = [akLeft, akBottom]
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF003FFFFFFFFFFF
            FFFF33333333333FFFFF3FFFFFFFFF00000F333333333377777F33FFFFFFFF09
            990F33333333337F337F333FFFFFFF09990F33333333337F337F3333FFFFFF09
            990F33333333337FFF7F33333FFFFF00000F3333333333777773333333FFFFFF
            FFFF3FFFFF3333333F330000033FFFFF0FFF77777F3333337FF30EEE0333FFF0
            00FF7F337FFF333777FF0EEE00033F00000F7F33777F3777777F0EEE0E033000
            00007FFF7F7FF777777700000E00033000FF777773777F3777F3330EEE0E0330
            00FF337FFF7F7F3777F33300000E033000FF337777737F37773333330EEE0300
            03FF33337FFF77777333333300000333333F3333777773333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = AgregarModelosTodosClick
        end
        object BorrarModelosTodos: TSpeedButton
          Left = 4
          Top = 191
          Width = 34
          Height = 31
          Hint = 'Borrar Todos Los Modelos Asignados A Este Rol'
          Anchors = [akLeft, akBottom]
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00500005000555
            555557777F777555F55500000000555055557777777755F75555005500055055
            555577F5777F57555555005550055555555577FF577F5FF55555500550050055
            5555577FF77577FF555555005050110555555577F757777FF555555505099910
            555555FF75777777FF555005550999910555577F5F77777775F5500505509990
            3055577F75F77777575F55005055090B030555775755777575755555555550B0
            B03055555F555757575755550555550B0B335555755555757555555555555550
            BBB35555F55555575F555550555555550BBB55575555555575F5555555555555
            50BB555555555555575F555555555555550B5555555555555575}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BorrarModelosTodosClick
        end
      end
      object gridModelos: TZetaDBGrid
        Left = 0
        Top = 0
        Width = 412
        Height = 225
        Align = alClient
        DataSource = dsModelos
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'WM_CODIGO'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WM_NOMBRE'
            Title.Caption = 'Nombre'
            Width = 302
            Visible = True
          end>
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 28
    Top = 113
  end
  object dsUsuarios: TDataSource
    OnDataChange = dsUsuariosDataChange
    Left = 29
    Top = 144
  end
  object dsModelos: TDataSource
    OnDataChange = dsModelosDataChange
    Left = 29
    Top = 176
  end
end
