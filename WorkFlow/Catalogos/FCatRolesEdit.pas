unit FCatRolesEdit;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask, Grids, DBGrids,
     ZBaseEdicion,
     ZetaDBTextBox,
     ZetaEdit,
     ZetaNumero,
     ZetaDBGrid,
     ZetaSmartLists, ComCtrls;

type
  TCatRolesEdit = class(TBaseEdicion)
    PanelControles: TPanel;
    RO_CODIGO: TZetaDBEdit;
    RO_NOMBRE: TDBEdit;
    RO_NOMBRElbl: TLabel;
    RO_CODIGOlbl: TLabel;
    RO_DESCRIP: TDBMemo;
    RO_DESCRIPlbl: TLabel;
    RO_NIVELlbl: TLabel;
    RO_NIVEL: TZetaDBNumero;
    dsUsuarios: TDataSource;
    PageControl: TPageControl;
    Usuarios: TTabSheet;
    PanelAgregar: TPanel;
    BorrarUsuario: TSpeedButton;
    AgregarUsuario: TSpeedButton;
    gridUsuarios: TZetaDBGrid;
    Modelos: TTabSheet;
    PanelModelos: TPanel;
    BorrarModelo: TSpeedButton;
    AgregarModelo: TSpeedButton;
    gridModelos: TZetaDBGrid;
    dsModelos: TDataSource;
    AgregarModelosTodos: TSpeedButton;
    BorrarModelosTodos: TSpeedButton;
    BorrarUsuariosTodos: TSpeedButton;
    AgregarUsuariosTodos: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure AgregarUsuarioClick(Sender: TObject);
    procedure BorrarUsuarioClick(Sender: TObject);
    procedure dsUsuariosDataChange(Sender: TObject; Field: TField);
    procedure dsModelosDataChange(Sender: TObject; Field: TField);
    procedure AgregarModeloClick(Sender: TObject);
    procedure BorrarModeloClick(Sender: TObject);
    procedure AgregarModelosTodosClick(Sender: TObject);
    procedure BorrarModelosTodosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AgregarUsuariosTodosClick(Sender: TObject);
    procedure BorrarUsuariosTodosClick(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  CatRolesEdit: TCatRolesEdit;

implementation

uses DWorkflow,
     DSistema,
     ZHelpContext,
     ZetaCommonClasses,
     ZetaBuscador,
     ZetaDialogo,
     ZAccesosTress;

{$R *.DFM}

procedure TCatRolesEdit.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_WORKFLOWCFG_CATALOGOS_ROLES_EDIC;
     IndexDerechos := D_CAT_ROLES;
     FirstControl := RO_CODIGO;
end;

procedure TCatRolesEdit.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := Usuarios;
end;

procedure TCatRolesEdit.Connect;
begin
     with dmWorkflow do
     begin
          Datasource.Dataset := dmSistema.cdsRoles;
          dsUsuarios.Dataset := cdsRolUsuarios;
          dsModelos.Dataset := cdsRolModelos;
     end;
end;

procedure TCatRolesEdit.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Rol', 'Rol', 'RO_CODIGO', dmSistema.cdsRoles );
end;

procedure TCatRolesEdit.AgregarUsuarioClick(Sender: TObject);
var
   iUsuario: Integer;
   sNombre: String;
begin
     inherited;
     with dmSistema do
     begin
          if BuscarUsuario( '', iUsuario, sNombre ) then
          begin
               RolUsuarioAgregar( iUsuario, sNombre );
          end;
     end;
end;

procedure TCatRolesEdit.BorrarUsuarioClick(Sender: TObject);
begin
     inherited;
     dmSistema.RolUsuarioBorrar;
end;

procedure TCatRolesEdit.AgregarUsuariosTodosClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     if ZetaDialogo.zConfirm( 'Agregar Todos los Usuarios', '� Desea Agregar Todos Los Usuarios A Este Rol ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             dmSistema.RolUsuarioAgregarTodos;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TCatRolesEdit.BorrarUsuariosTodosClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     if ZetaDialogo.zConfirm( 'Borrar Todos los Usuarios', '� Desea Remover Todos Los Usuarios De Este Rol ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             dmSistema.RolUsuarioBorrarTodos;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TCatRolesEdit.dsUsuariosDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     BorrarUsuario.Enabled := not dsUsuarios.Dataset.IsEmpty;
     BorrarUsuariosTodos.Enabled := BorrarUsuario.Enabled;
end;

procedure TCatRolesEdit.AgregarModeloClick(Sender: TObject);
var
   sModelo, sNombre: String;
begin
     inherited;
     with dmWorkflow do
     begin
          if ModeloBuscar( '', sModelo, sNombre ) then
          begin
               dmWorkflow.RolModeloAgregar( sModelo, sNombre );
          end;
     end;
end;

procedure TCatRolesEdit.BorrarModeloClick(Sender: TObject);
begin
     inherited;
     dmWorkflow.RolModeloBorrar;
end;

procedure TCatRolesEdit.dsModelosDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     BorrarModelo.Enabled := not dsModelos.Dataset.IsEmpty;
     BorrarModelosTodos.Enabled := BorrarModelo.Enabled;
end;

procedure TCatRolesEdit.AgregarModelosTodosClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     if ZetaDialogo.zConfirm( 'Agregar Todos los Modelos', '� Desea Agregar Todos Los Modelos A Este Rol ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             dmWorkFlow.RolModeloAgregarTodos;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TCatRolesEdit.BorrarModelosTodosClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     if ZetaDialogo.zConfirm( 'Borrar Todos los Modelos', '� Desea Remover Todos Los Modelos De Este Rol ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             dmWorkFlow.RolModeloBorrarTodos;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

end.
