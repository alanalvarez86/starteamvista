inherited CatUsuarios: TCatUsuarios
  Left = 78
  Top = 218
  Caption = 'Usuarios'
  ClientHeight = 257
  ClientWidth = 849
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 849
    inherited Slider: TSplitter
      Left = 357
    end
    inherited ValorActivo1: TPanel
      Width = 341
    end
    inherited ValorActivo2: TPanel
      Left = 360
      Width = 489
    end
  end
  object ZetaDBGrid: TZetaDBGrid [1]
    Left = 0
    Top = 49
    Width = 849
    Height = 208
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'US_CODIGO'
        Title.Caption = 'N'#250'mero'
        Width = 47
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 214
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_EMAIL'
        Title.Caption = 'E-Mail'
        Width = 179
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_FORMATO'
        Title.Caption = 'Formato'
        Width = 94
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_JEFE'
        Title.Caption = 'Reporta A'
        Width = 56
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOM_JEFE'
        Title.Caption = 'Nombre'
        Width = 229
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_WF_OUT'
        Title.Caption = 'Fuera Oficina'
        Visible = True
      end>
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 19
    Width = 849
    Height = 30
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 17
      Top = 8
      Width = 33
      Height = 13
      Caption = 'Busca:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object BBusca: TSpeedButton
      Left = 177
      Top = 1
      Width = 27
      Height = 27
      Hint = 'Buscar Reporte'
      AllowAllUp = True
      GroupIndex = 1
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D808DDDDDDDD
        DDDD83308DDDDDDDDDDD873308DDDDDDDDDDD373308DDDDDDDDDDD3733088888
        8DDDDDD37330000088DDDDDD37338777808DDDDDD038F7F77808DDDDD08FCCCC
        C788DDDCD0F7C7F7C770DDDCC07FCCCCCF70DDDCD0F7CFFCF770DDDDC88FCCCC
        7F88DDDDDD08F7F7F80DDDDDDDD08F7F80DDDDDDDDDD80008DDD}
      ParentShowHint = False
      ShowHint = True
      OnClick = BBuscaClick
    end
    object EBuscaNombre: TEdit
      Left = 52
      Top = 4
      Width = 121
      Height = 21
      TabOrder = 0
      OnChange = EBuscaNombreChange
    end
  end
  inherited DataSource: TDataSource
    Left = 280
    Top = 8
  end
end
