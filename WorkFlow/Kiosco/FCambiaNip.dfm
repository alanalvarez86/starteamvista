inherited CambiaNIP: TCambiaNIP
  Left = 527
  Top = 151
  Caption = 'CambiaNIP'
  ClientHeight = 388
  Position = poDefault
  PixelsPerInch = 96
  TextHeight = 13
  inherited fcImagenFondo: TfcImager
    Height = 388
  end
  inherited fcPanel2: TfcPanel
    Height = 388
    inherited fcPanel1: TfcPanel
      inherited PanelMensaje: TfcShapeBtn
        Left = 2
        Width = 213
        NumGlyphs = 1
      end
      inherited PanelClave: TfcShapeBtn
        Left = 40
        NumGlyphs = 1
      end
    end
    inherited PanelTeclado: TfcPanel
      Height = 276
      inherited fcNumero1: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcNumero2: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcNumero3: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcNumero4: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcNumero5: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcNumero6: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcNumero7: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcNumero8: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcNumero9: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcNumero0: TfcShapeBtn
        NumGlyphs = 0
      end
    end
  end
end
