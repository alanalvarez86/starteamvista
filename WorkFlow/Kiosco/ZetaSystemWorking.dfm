object ZSystemWorking: TZSystemWorking
  Left = 374
  Top = 239
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'ZSystemWorking'
  ClientHeight = 123
  ClientWidth = 171
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelGlobal: TPanel
    Left = 0
    Top = 0
    Width = 171
    Height = 123
    Align = alClient
    BevelInner = bvLowered
    BorderWidth = 2
    TabOrder = 0
    object PanelMensaje: TLabel
      Left = 4
      Top = 106
      Width = 163
      Height = 13
      Align = alBottom
      Alignment = taCenter
      Caption = 'Conectando Empresa ...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Layout = tlCenter
    end
    object Animate: TAnimate
      Left = 4
      Top = 4
      Width = 163
      Height = 102
      Align = alClient
      Active = True
      AutoSize = False
      CommonAVI = aviFindFolder
      StopFrame = 29
    end
  end
end
