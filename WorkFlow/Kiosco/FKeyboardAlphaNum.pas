unit FKeyboardAlphaNum;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FKeyboardBase, StdCtrls, fcButton, fcImgBtn, fcShapeBtn,
  ExtCtrls, fcpanel;

type
  TKeyboardAlphaNum = class(TKeyboardBase)
    fcShapeBtn38: TfcShapeBtn;
    fcShapeBtn37: TfcShapeBtn;
    fcShapeBtn36: TfcShapeBtn;
    fcShapeBtn35: TfcShapeBtn;
    fcShapeBtn34: TfcShapeBtn;
    fcShapeBtn33: TfcShapeBtn;
    fcShapeBtn32: TfcShapeBtn;
    fcShapeBtn31: TfcShapeBtn;
    fcShapeBtn30: TfcShapeBtn;
    fcShapeBtn29: TfcShapeBtn;
    fcShapeBtn28: TfcShapeBtn;
    fcShapeBtn27: TfcShapeBtn;
    fcShapeBtn26: TfcShapeBtn;
    fcShapeBtn25: TfcShapeBtn;
    fcShapeBtn24: TfcShapeBtn;
    fcShapeBtn23: TfcShapeBtn;
    fcShapeBtn22: TfcShapeBtn;
    fcShapeBtn21: TfcShapeBtn;
    fcShapeBtn20: TfcShapeBtn;
    fcShapeBtn19: TfcShapeBtn;
    fcShapeBtn18: TfcShapeBtn;
    fcShapeBtn17: TfcShapeBtn;
    fcShapeBtn16: TfcShapeBtn;
    fcShapeBtn15: TfcShapeBtn;
    fcShapeBtn14: TfcShapeBtn;
    fcShapeBtn13: TfcShapeBtn;
    fcShapeBtn12: TfcShapeBtn;
    fcShapeBtn11: TfcShapeBtn;
    fcShapeBtn10: TfcShapeBtn;
    fcShapeBtn8: TfcShapeBtn;
    fcShapeBtn7: TfcShapeBtn;
    fcShapeBtn6: TfcShapeBtn;
    fcShapeBtn5: TfcShapeBtn;
    fcShapeBtn4: TfcShapeBtn;
    fcShapeBtn3: TfcShapeBtn;
    fcShapeBtn2: TfcShapeBtn;
    PanelBotones: TfcPanel;
    fcOK: TfcShapeBtn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  KeyboardAlphaNum: TKeyboardAlphaNum;

implementation

{$R *.dfm}

end.
