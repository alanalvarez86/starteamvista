unit FNipHtml;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OleCtrls, SHDocVw;

type
  TNipHtml = class(TForm)
    WebBrowser: TWebBrowser;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  NipHtml: TNipHtml;

implementation
uses
    ZetaCommonTools,
    DGlobalComparte;

{$R *.dfm}

procedure TNipHtml.FormCreate(Sender: TObject);
begin
     WebBrowser.Navigate( ZetaCommonTools.VerificaDirURL(GlobalComparte.GetGlobalString( K_KIOSCO_DIRECTORIO_RAIZ )) + 'NIP.HTM' );
end;

end.
