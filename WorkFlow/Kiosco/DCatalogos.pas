unit DCatalogos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient, ZetaClientDataSet,
{$ifdef DOS_CAPAS}
     {$ifdef TRESS}
     DServerCatalogos
     {$endif}
     {$ifdef SELECCION}
     DServerSeleccion
     {$endif}
{$else}
  {$ifdef TRESS}
  Catalogos_TLB
  {$endif}
  {$ifdef SELECCION}
  Seleccion_TLB
  {$endif}
{$endif};

type
  TdmCatalogos = class(TDataModule)
    cdsCondiciones: TZetaLookupDataSet;
    procedure cdsCondicionesAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }
{$ifdef DOS_CAPAS}
    function GetServerCatalogos:{$ifdef TRESS}TdmServerCatalogos{$endif}{$ifdef SELECCION}TdmServerSeleccion{$endif};

    property ServerCatalogo:{$ifdef TRESS}TdmServerCatalogos{$endif}{$ifdef SELECCION}TdmServerSeleccion{$endif} read GetServerCatalogos;
{$else}
    {$ifdef TRESS}
    FServidor: IdmServerCatalogosDisp;
    function GetServerCatalogos: IdmServerCatalogosDisp ;
    property ServerCatalogo: IdmServerCatalogosDisp read GetServerCatalogos;
    {$ENDIF}
    {$ifdef SELECCION}
    FServidor: IdmServerSeleccionDisp;
    function GetServerCatalogos: IdmServerSeleccionDisp ;
    property ServerCatalogo: IdmServerSeleccionDisp read GetServerCatalogos;
    {$ENDIF}
{$endif}

  public
    { Public declarations }
  end;

var
  dmCatalogos: TdmCatalogos;

implementation
uses DCliente;
{$R *.DFM}

{$ifdef DOS_CAPAS}
function TdmCatalogos.GetServerCatalogos: {$ifdef TRESS}TdmServerCatalogos{$endif}{$ifdef SELECCION}TdmServerSeleccion{$endif};
begin
     Result := DCliente.dmCliente.ServerCatalogos;
end;
{$else}
 {$ifdef TRESS}
 function TdmCatalogos.GetServerCatalogos: IdmServerCatalogosDisp;
 begin
      Result := IdmServerCatalogosDisp( dmCliente.CreaServidor( CLASS_dmServerCatalogos, FServidor ) );
 end;
 {$endif}
 {$ifdef SELECCION}
 function TdmCatalogos.GetServerCatalogos: IdmServerSeleccionDisp;
 begin
      Result := IdmServerSeleccionDisp( dmCliente.CreaServidor( CLASS_dmServerSeleccion, FServidor ) );
 end;
 {$endif}
{$endif}

procedure TdmCatalogos.cdsCondicionesAlAdquirirDatos(Sender: TObject);
begin
     cdsCondiciones.Data := ServerCatalogo.GetCondiciones( dmCliente.Empresa );
end;

end.
