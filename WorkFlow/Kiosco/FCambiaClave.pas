unit FCambiaClave;

interface

uses Windows, Messages, SysUtils, {$Ifndef VER130}Variants,{$endif} Classes, Graphics, Controls, Forms,
     Dialogs, ExtCtrls, fcButton, fcImgBtn, fcShapeBtn, fcpanel,
     FKeyboardNumerico, StdCtrls, OleCtrls, SHDocVw, FKeyboardBase,
  fcImager;

type
  TCambiaClave = class(TKeyboardNumerico)
    TimerSalida: TTimer;
    procedure FormShow(Sender: TObject);
    procedure TimerSalidaTimer(Sender: TObject);
    procedure edTextoChange(Sender: TObject);
  private
    { Private declarations }
    FProblema: String;
    FMensaje: String;
    FPrimeraCaptura: Boolean;
    FPrimeraClave: String;
    procedure ResetTimerSalida;
  protected
    { Protected declarations }
    property PrimeraCaptura: Boolean read FPrimeraCaptura write FPrimeraCaptura;
    property PrimeraClave: String read FPrimeraClave write FPrimeraClave;
    property Problema: String read FProblema write FProblema;
    procedure AlCapturar; override;
    procedure AlSalir; override;
    procedure AceptaClave;virtual;
    procedure SetControles;

  public
    { Public declarations }
    property Mensaje: String read FMensaje write FMensaje;
  end;

function CambiaClaveEmpleado( var sClave: String; const iMaxDigitos: Integer ): Boolean;

implementation

uses DCliente,
     ZetaCommonTools;

var
   CambiaClave: TCambiaClave;

{$R *.dfm}

function CambiaClaveEmpleado( var sClave: String; const iMaxDigitos: Integer ): Boolean;
begin
     Result := False;
     if not Assigned( CambiaClave ) then
        CambiaClave := TCambiaClave.Create( Application );

     with CambiaClave do
     begin
          Mensaje := 'Por favor capture su nueva clave';
          Mascara := '*';
          MaximoDigitos := iMaxDigitos;
          ShowModal;
          if IsOk then
          begin
               Result := True;
               sClave := Valor;
          end;
     end;
end;

{****** TPideGafete ***********}

procedure TCambiaClave.FormShow(Sender: TObject);
begin
     inherited;
     InicializaControles;
     Clear;
     SetControles;
     FProblema := '';
     FPrimeraCaptura := True;
     FPrimeraClave := '';
end;

procedure TCambiaClave.SetControles;
begin
     with PanelMensaje do
     begin
          if ZetaCommonTools.StrLleno( Problema ) then
          begin
               Font.Color := ColorError;
               Caption := Self.Problema;
          end
          else
          begin
               Font.Color := ColorNormal;
               Caption := Mensaje;
          end;
     end;
     ResetTimerSalida;
     Application.ProcessMessages;
end;

procedure TCambiaClave.ResetTimerSalida;
begin
     with TimerSalida do
     begin
          Enabled := False;
          Interval := dmCliente.TimeoutDatos;
          Enabled := True;
     end;
end;

procedure TCambiaClave.TimerSalidaTimer(Sender: TObject);
begin
     inherited;
     ModalResult := mrCancel;
end;


procedure TCambiaClave.AlCapturar;
begin
     inherited AlCapturar;
     ResetTimerSalida;
     Problema := '';
     SetControles;
end;

procedure TCambiaClave.AlSalir;
begin
     inherited AlSalir;
     dmCliente.InterceptaMensajes := True;
end;

procedure TCambiaClave.AceptaClave;
begin
     inherited;
     if PrimeraCaptura then
     begin
          Mensaje := 'Por favor confirme la clave capturada';
          PrimeraCaptura := False;
          PrimeraClave := Valor;
          Clear;
          SetControles;
     end
     else
     begin
          if ( Length( Valor ) = MaximoDigitos ) then
          begin
               if ( Self.Valor = Self.PrimeraClave ) then
               begin
                    ModalResult := mrOk;
               end
               else
               begin
                    Clear;
                    Problema := 'Clave diferente. Por favor intente de nuevo';
                    SetControles;
               end;
          end
          else
          begin
               Clear;
               Problema := 'La longitud no es correcta. Por favor intente de nuevo';
               SetControles;
          end;
     end;
end;

procedure TCambiaClave.edTextoChange(Sender: TObject);
begin
     inherited;
     if Length( edTexto.Text ) = MaximoDigitos then
        AceptaClave;

end;

end.
