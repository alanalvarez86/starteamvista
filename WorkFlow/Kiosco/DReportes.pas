unit DReportes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaClientDataSet, Db, DBClient, FileCtrl,
  ZetaCommonLists,
  ZQRReporteListado,
  DReportesGenerador;

type
  TdmReportes = class(TdmReportGenerator)
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    QRReporteListado : TQrReporteListado;

    function GetSobreTotales: Boolean;
    procedure ModificaListaCampos(Lista: TStrings);
    procedure ModificaListas;

  protected
    FMandaloPreview : boolean;
    procedure DoOnGetResultado( const lResultado : Boolean; const Error : string );override;
    procedure DoOnGetReportes( const lResultado: Boolean );override;

    function PreparaPlantilla(var sError: WideString): Boolean;override;
    procedure DesPreparaPlantilla;override;

    procedure PreparaReporte;
    procedure DesPreparaReporte;
    procedure LogError(const sTexto: String; const lEnviar: Boolean);override;
    function GetEmpresa: Olevariant;override;

  public
    { Public declarations }
    function GeneraUnReporte( const iReporte: Integer; const Preview: Boolean ): Boolean;
    function ImprimeUnReporte( const iReporte: Integer ): Boolean;
    function DirectorioPlantillas: string;
  end;

var
  dmReportes: TdmReportes;

implementation

uses DCliente,
     DGlobal,
     FDlgInformativo,
     ZetaSystemWorking,
     ZetaCommonTools,
     ZGlobalTress,
     ZReportTools,
     ZFuncsCliente,
     ZAccesosTress,
     ZReporteAscii,
     ZetaDialogo,
     DMailMerge;
{$R *.DFM}

const
     K_NOMBRE_BITACORA = 'ReportesKiosko.LOG';

{ TdmReportes }

procedure TdmReportes.DataModuleCreate(Sender: TObject);
begin
     SetLogFileName( ExtractFilePath( Application.ExeName ) + K_NOMBRE_BITACORA );
     inherited;
     FMostrarError := TRUE;
     Global.Conectar;
end;

function TdmReportes.GeneraUnReporte(const iReporte: Integer; const Preview: Boolean ): Boolean;
begin
     ZetaSystemWorking.InitAnimation( 'Procesando reporte.....' );
     try
        FMandaloPreview := Preview;
        Result := GetResultado( iReporte, TRUE );
     finally
            ZetaSystemWorking.EndAnimation
     end;
end;

function TdmReportes.ImprimeUnReporte(const iReporte: Integer ): Boolean;
begin
     ZetaSystemWorking.InitAnimation( 'Imprimiendo reporte.....' );
     try
        FUsaMismoResultado := TRUE;
        FMandaloPreview := FALSE;
        Result := GetResultado( iReporte, TRUE );
     finally
            ZetaSystemWorking.EndAnimation
     end;
end;
function TdmReportes.GetSobreTotales: Boolean;
 var i : integer;
begin
     Result := FALSE;
     for i := 0 to Campos.Count - 1 do
     begin
          with TCampoListado( Campos.Objects[ i ] ) do
          begin
               Result := Result OR (Operacion = ocSobreTotales);
          end;
     end;
end;

function TdmReportes.PreparaPlantilla( var sError : WideString ) : Boolean;
 var
    lHayImagenes : Boolean;
    sDirectorio : string;
begin
     {$ifdef KIOSCO2}
     PreparaReporte;
     {$else}
     ZQRReporteListado.PreparaReporte;
     {$endif}

     sDirectorio := ExtractFilePath( DatosImpresion.Archivo );
     Result := DirectoryExists( sDirectorio );
     if NOT Result then
        sError := Format( 'El Directorio de Plantillas " %s " No Existe', [ sDirectorio ] );

     if Result then
     begin
          DoOnGetDatosImpresion;

          with QRReporteListado do
          begin
               Init( FSQLAgente,
                     DatosImpresion,
                     Parametros.Count,
                     lHayImagenes );
               ContieneImagenes := lHayImagenes;
          end;
     end;
end;

procedure TdmReportes.DesPreparaPlantilla;
begin
     {$ifdef KIOSCO2}
     DesPreparaReporte;
     {$ELSE}
     ZQrReporteListado.DesPreparaReporte;
     {$ENDIF}
end;

procedure TdmReportes.DoOnGetReportes( const lResultado: Boolean );
begin
     if lResultado then
     begin
          if NOT(eTipoReporte( cdsReporte.FieldByName('RE_TIPO').AsInteger ) in [trForma, trListado] ) then
          begin
               Raise Exception.Create( 'Solamente se Permite Obtener Listados y/o Formas' );
          end;
     end;
end;

procedure TdmReportes.DoOnGetResultado( const lResultado : Boolean; const Error : string );

 procedure GeneraMailMerge;
  var
     oReporteAscii: TReporteAscii;
 begin
      oReporteAscii := TReporteAscii.Create;

      try
         with oReporteAscii do
         begin
              if GeneraAscii( cdsResultados,
                              DatosImpresion,
                              Campos,
                              Grupos,
                              zStrToBool( cdsReporte.FieldByName('RE_SOLOT').AsString ),
                              FALSE ) then
              begin
                   with TdmMailMerge.Create(self) do
                   begin
                        with DatosImpresion do
                             if FileExists( Archivo ) then
                                Imprime( Archivo,Exportacion )
                             else
                                 ZError('Impresión','El Documento ' + Archivo + ' No Ha Sido Encontrado.', 0);
                        Free;
                   end;
              end;
         end;
      finally
             FreeAndNil( oReporteAscii );
      end;

 end;

 var eTipo : eTipoReporte;
begin
     if cdsResultados.Active AND Not cdsResultados.IsEmpty then
     begin

          cdsResultados.First;

          ModificaListas;

          ZFuncsCliente.RegistraFuncionesCliente;
          //Asignacion de Parametros del Reporte,
          //para poder evaluar la funcion PARAM() en el Cliente;
          ZFuncsCliente.ParametrosReporte := FSQLAgente.Parametros;

          with cdsReporte do
          begin
               eTipo := eTipoReporte( FieldByName('RE_TIPO').AsInteger );
               if ( eTipo = trForma ) then
               begin
                    QRReporteListado.GeneraForma( FieldByName('RE_NOMBRE').AsString,
                                                  FMandaloPreview, {Mandarlo a Preview}
                                                  cdsResultados )
               end
               else if ( eTipo = trListado ) then
               begin
                    if ( eTipoFormato( FieldByName('RE_PFILE').AsInteger ) = tfMailMerge ) then
                    begin
                         GeneraMailMerge;
                    end
                    else
                        QRReporteListado.GeneraListado( FieldByName('RE_NOMBRE').AsString,
                                                    FMandaloPreview, {Mandarlo a Preview}
                                                    zStrToBool( FieldByName('RE_VERTICA').AsString ),
                                                    zStrToBool( FieldByName('RE_SOLOT').AsString ),
                                                    GetSobreTotales,
                                                    cdsResultados,
                                                    Campos,
                                                    Grupos );
               end;
          end;
     end;
end;

procedure TdmReportes.ModificaListaCampos( Lista : TStrings );
 var i : integer;
begin
     for i:=0 to Lista.Count -1 do
        with TCampoOpciones(Lista.Objects[i]) do
             if PosAgente >= 0 then
             begin
                  SQLColumna := FSQLAgente.GetColumna(PosAgente);
                  with FSQLAgente.GetColumna(PosAgente) do
                  begin
                       TipoImp := TipoFormula;
                       OpImp := Totalizacion;
                  end;
             end;
end;

procedure TdmReportes.ModificaListas;
 var i : integer;
begin
     ModificaListaCampos( Campos );
     for i:= 0 to Grupos.Count - 1 do
     begin
          with TGrupoOpciones( Grupos.Objects[i]) do
          begin
               ModificaListaCampos( ListaEncabezado );
               ModificaListaCampos( ListaPie );
          end;
     end;
end;

procedure TdmReportes.PreparaReporte;
begin
     if QRReporteListado = NIL then
        QRReporteListado := TQRReporteListado.Create( Application );
end;

procedure TdmReportes.DesPreparaReporte;
begin
     try
        QRReporteListado.Free;
     finally
            QRReporteListado := NIL;
     end;
end;


procedure TdmReportes.LogError(const sTexto: String; const lEnviar: Boolean );
begin
     if FMostrarError then
        FDlgInformativo.MuestraDialogoInf( sTexto );
end;

function TdmReportes.GetEmpresa: Olevariant;
 var
    sCompany: string;
begin
     with dmCliente do
     begin
          sCompany := cdsCompany.FieldByName('CM_CODIGO').AsString;
          try
             FindCompany( dmCliente.EmpresaReportes );
             Result := SetCompany;
          finally
                 FindCompany( sCompany );
                 SetCompany;
          end;
     end;
end;

function TdmReportes.DirectorioPlantillas: string;
begin
     {.$ifdef RDD}
     //with dmCliente do
          //Result := VerificaDir( ServerReportes.DirectorioPlantillas(Empresa) );
     {.$else}
     Result := VerificaDir( Global.GetGlobalString( K_GLOBAL_DIR_PLANT ));
     {.$endif}

end;

end.

