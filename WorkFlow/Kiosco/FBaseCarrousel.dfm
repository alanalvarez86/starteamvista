object BaseCarrousel: TBaseCarrousel
  Left = 257
  Top = 113
  BorderStyle = bsNone
  Caption = 'BaseCarrousel'
  ClientHeight = 399
  ClientWidth = 550
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Timer: TTimer
    Enabled = False
    OnTimer = TimerTimer
    Left = 44
    Top = 44
  end
  object TimerTeclado: TTimer
    Enabled = False
    Interval = 3000
    OnTimer = TimerTecladoTimer
    Left = 324
    Top = 52
  end
end
