program Kiosco2;

uses
  Forms,
  sysutils,
  ZetaDialogo,
  FFormaSalida,
  FFormaProblema,
  DCliente in 'DCliente.pas',
  FBaseCarrousel in 'FBaseCarrousel.pas' {BaseCarrousel},
  DBasicoCliente in '..\..\3win_20\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DReportesGenerador in '..\..\3win_20\DataModules\DReportesGenerador.pas' {dmReportGenerator: TDataModule},
  DExportEngine in '..\..\3win_20\DataModules\DExportEngine.pas' {dmExportEngine: TDataModule},
  DBaseGlobal in '..\..\3win_20\DataModules\dBaseGlobal.pas',
  ZetaSystemWorking in 'ZetaSystemWorking.pas' {ZSystemWorking},
  FKeyboardBase in 'FKeyboardBase.pas' {KeyboardBase},
  DMailMerge in '..\..\3win_20\DataModules\DMailMerge.pas' {dmMailMerge: TDataModule},
  FKeyboardNumerico in 'FKeyboardNumerico.pas' {KeyboardNumerico},
  DBaseDiccionario in '..\..\3win_20\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DGlobal in '..\..\3win_20\DataModules\DGlobal.pas',
  DGlobalComparte in '..\DataModules\DGlobalComparte.pas';

{$R *.RES}

begin
  Application.Initialize;
  try
     dmCliente := TdmCliente.Create( Application );

     try
        try
           if FindCmdLineSwitch( 'CONFIG', [ '/', '-' ], True ) then
           begin
                dmCliente.ConfiguracionKiosco;
           end
           else
           begin
                with dmCliente do
                begin
                     if Init then
                        StartShow;
                end;
           end;
           Application.Title := 'TressKiosco';
           Application.Run;
        except
              on Error: Exception do
              begin
                   dmCliente.MuestraFormaError( Error );
              end;
        end;
     finally
            dmCliente.Free;
     end;
  except
        on Error: Exception do
        begin
             FFormaProblema.MuestraExcepcion( Error );
        end;
  end;
end.
