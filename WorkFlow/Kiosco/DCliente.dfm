inherited dmCliente: TdmCliente
  OldCreateOrder = True
  Left = 501
  Top = 235
  object cdsBotones: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 112
    Top = 16
  end
  object cdsEmpleado: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnCalcFields = cdsEmpleadoCalcFields
    AlCrearCampos = cdsEmpleadoAlCrearCampos
    Left = 112
    Top = 72
  end
  object cdsPeriodoActivo: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 192
    Top = 16
  end
  object cdsReportes: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 192
    Top = 72
  end
  object cdsCarrusel: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 272
    Top = 16
  end
  object cdsPantalla: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 272
    Top = 72
  end
  object cdsEstilo: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 336
    Top = 16
  end
  object cdsShow: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 336
    Top = 72
  end
  object TimerCarrousel: TTimer
    Enabled = False
    Interval = 8000
    OnTimer = TimerCarrouselTimer
    Left = 248
    Top = 192
  end
  object cdsKioscos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsKioscosAlAdquirirDatos
    Left = 32
    Top = 128
  end
end
