unit DCliente;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient, OleCtrls, SHDocVw, ExtCtrls,
     qrprntr,
     FBaseKiosco,
     FBaseCarrousel,
     {$ifdef DOS_CAPAS}
     DServerPortalTress,
     DServerComparte,
     DServerReportes,
     {$else}
     PortalTress_TLB,
     Reportes_TLB,
     Comparte_TLB,
     {$endif}
     DBasicoCliente,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaClientDataSet;

const
     K_CAMPO_DIGITO = 'CM_DIGITO';
     K_ARROBA_KIOSCO = '@KIOSCO';
     K_ARROBA_SERVIDOR = '@SERVIDOR';
     K_ARROBA_PANTALLA = '@PANTALLA';
     K_MIL = 1000;
     //K_CAMPO_DIGITO = 'CM_ALIAS'; { Ojo si se usa COMPANYS.CM_ALIAS en el OleVariant de Empresa Activa }
type
  TPideNIPReply = ( pnrOK, pnrCancel, pnrChangePassword );
  TdmCliente = class(TBasicoCliente)
    cdsBotones: TZetaClientDataSet;
    cdsEmpleado: TZetaClientDataSet;
    cdsPeriodoActivo: TZetaClientDataSet;
    cdsReportes: TZetaClientDataSet;
    cdsCarrusel: TZetaClientDataSet;
    cdsPantalla: TZetaClientDataSet;
    cdsEstilo: TZetaClientDataSet;
    cdsShow: TZetaClientDataSet;
    TimerCarrousel: TTimer;
    cdsKioscos: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsEmpleadoAlCrearCampos(Sender: TObject);
    procedure cdsEmpleadoCalcFields(DataSet: TDataSet);
    procedure TimerCarrouselTimer(Sender: TObject);
    procedure cdsKioscosAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }

    FFormaDatos: TBaseKiosco;
    FFormaInfo: TBaseCarrousel;
    FListaFormas: TList;
    FListaBrowsers: TStringList;
    FPosicion: Integer;

    FEmpleadoNombre: String;
    FEmpresaNombre: String;
    FEmpleadoActivo: TNumEmp;
    FEmpleadoNIP: String;
    FEmpleadoNatalicio: TDate;
    FTipoNomina: eTipoPeriodo;
    FParametros: TZetaParams;
    {CV: Build4 se descontinuo el uso de los campos KW_SCR_INF y KW_TIM_INF
    FTimeoutDefault : Integer;
    }
    FTimeoutDatos : Integer;
    FNumeroEmpleado : String;
    FMensajeError: String;
    FSalirSistema: Boolean;
    FKioscoIniciado: Boolean;
    FInterceptaMensajes: Boolean;
    FMuestraFormaPideGafete: Boolean;
    FAdministrador: Boolean;
    FPideNip : Boolean;
    FPanelReportes: TWinControl;
    FQrPreview: TQRPreview;
    {$ifdef Antes}
    FId: string;
    {$endif}
    {$ifdef DOS_CAPAS}
    FServerPortalTress: TdmPortalTress;
    FServerComparte: TdmServerComparte;
    FServerReportes: TdmServerReportes;
    {$else}
    FServerPortalTress: IdmPortalTress;
    FServerComparte: IdmServerComparte;
    FServerReportes : IdmServerReportes;
    function GetServerPortalTress: IdmPortalTressDisp;
    function GetServerComparte: IdmServerComparteDisp;
    function GetServerReportes: IdmServerReportesDisp;
    {$endif}
    function GetIMSSMes: Integer;
    function GetIMSSPatron: String;
    function GetIMSSTipo: eTipoLiqIMSS;
    function GetIMSSYear: Integer;
    function SalirDelSistema: Boolean;
    function GetCodigoKioscoID: string;
    function GetEmpresaReportes: string;
  protected
    { Protected declarations }
    {$ifdef DOS_CAPAS}
    property ServerPortalTress: TdmPortalTress read FServerPortalTress;
    {$else}
    property ServerPortalTress: IdmPortalTressDisp read GetServerPortalTress;
    {$endif}
    property Parametros: TZetaParams read FParametros;
    function ValidaGafete( sGafete: String; var sError: String; var lHuboErrores: Boolean ): Boolean;
    function PideNIPEmpleado(FParent: TWinControl = NIL): Boolean;

    procedure CompanyFind( const sDigito: String );
    procedure CargaActivosIMSS( Parametros: TZetaParams );
    procedure CargaActivosPeriodo( Parametros: TZetaParams );
    procedure CargaActivosSistema( Parametros: TZetaParams );
    procedure ShowConfigure( const sShow: String );
    procedure ShowLoad( const sShow: String );
  public
    { Public declarations }
    {$ifdef DOS_CAPAS}
    property ServerReportes: TdmServerReportes read FServerReportes;
    property ServerComparte: TdmServerComparte read FServerComparte;
    {$else}
    property ServerReportes: IdmServerReportesDisp read GetServerReportes;
    property ServerComparte: IdmServerComparteDisp read GetServerComparte;
    {$endif}
    property EmpleadoActivo: TNumEmp read FEmpleadoActivo;
    property Empleado: TNumEmp read FEmpleadoActivo;
    property EmpleadoNombre: String read FEmpleadoNombre;
    property EmpleadoNIP: String read FEmpleadoNIP;
    property EmpleadoNatalicio: TDate read FEmpleadoNatalicio;
    property EmpresaNombre: String read FEmpresaNombre;
    property IMSSPatron: String read GetIMSSPatron;
    property IMSSYear: Integer read GetIMSSYear;
    property IMSSTipo: eTipoLiqIMSS read GetIMSSTipo;
    property IMSSMes: Integer read GetIMSSMes;
    property TipoNomina: eTipoPeriodo read FTipoNomina;
    property KioscoIniciado: Boolean read FKioscoIniciado;
    property NumeroEmpleado: String read FNumeroEmpleado write FNumeroEmpleado;
    property FormaDatos : TBaseKiosco read FFormaDatos;
    property TimeoutDatos : Integer read FTimeoutDatos;
    property PideNip: Boolean read FPideNip;
    {CV: Build4 se descontinuo el uso de los campos KW_SCR_INF y KW_TIM_INF
    property TimeoutDefault : Integer read FTimeoutDefault;
    }
    property MensajeError: String read FMensajeError write FMensajeError;
    property SalirSistema: Boolean read FSalirSistema write FSalirSistema;
    property InterceptaMensajes: Boolean read FInterceptaMensajes write FInterceptaMensajes;
    property MuestraFormaPideGafete: Boolean read FMuestraFormaPideGafete write FMuestraFormaPideGafete;
    property PanelReportes: TWinControl read FPanelReportes write FPanelReportes;
    property QrPreview: TQrPreview read FQrPreview write FQrPreview;
    property EmpresaReportes: string read GetEmpresaReportes;
    function GetPantalla( const sPantalla: String ): TBaseKiosco;
    function BuildValoresActivos: String;
    function GetDatosPeriodoActivo: TDatosPeriodo;
    function Init: Boolean;
    function PosicionaEmpleado(const sDigito, sCredencial: String; const iEmpleado: TNumEmp; var sTexto: String; var lHuboErrores: Boolean ): Boolean;
    function GetValorActivo( const eValor : {$ifdef RDD}eRDDValorActivo{$else}string{$endif} ): String;
    //function GetValorActivo( const eValor : {$ifdef RDD}eRDDValorActivo{$else}string{$endif} ): String; override;
    //function GetValorActivo( const sValor: String ): String;override;
    function VerificaCodigoKiosco: Boolean;
    function GetURLKiosco( const sURL : string; const eAccion: eAccionBoton = abContenido ): string;
    function CambiaNIPEmpleado(FParent: TWinControl = NIL): Boolean;
    function EscribeNIPEmpleado(const sClave: String): Boolean;

    procedure MuestraForma( const iPos: Integer);
    procedure MuestraBrowser( const iPos: Integer );
    procedure MuestraBrowserSiguiente;
    procedure StartShow;
    procedure MuestraFormaError( const Error: Exception );
    procedure MuestraErrorcito( const sError: String );
    procedure MuestraPideGafete;overload;
    procedure MuestraPideGafete( const sGafeteTeclado: string );overload;

    procedure GeneraBitacora( const sMensajeError:String );
    procedure ConfiguracionKiosco;
    procedure CompanyLoad;
    procedure CargaActivosTodos( Parametros: TZetaParams );override;
    procedure CargaReportes;
    function GeneraReporte(const iReporte: integer; const Preview: Boolean = TRUE ): Boolean;
    procedure GetBotones(const sPantalla : String );
    procedure GetCarrusel(const sCarrusel: String );
    procedure GetEstilo(const sEstilo: String );
  end;

var
  dmCliente: TdmCliente;

implementation

uses DReportes,
     DDiccionario,
     DGlobal,
     DGlobalComparte,
     ZGlobalTress,
     ZetaCommonTools,
     ZetaSystemWorking,
     ZetaDialogo,
     ZetaWinApiTools,
     FKioscoRegistry,
     FServidorName,
     FConfiguraKiosco,
     FCambiaClave,
     FCapturaClaveNueva,
     FFormaProblema,
     FFormaError,
     //FPideGafete,
     FPideNIP,
     FCambiaNip,
     FValoresActivos,
     FDlgInformativo ;

{$R *.DFM}

const
     K_LEN_ANTIGUO = 40;
     K_INVALIDO = 'Gafete inv�lido';

{ ******** TdmCliente ******** }

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     Randomize;
     {$ifdef Antes}
     FId := IntToStr( Random(High(Integer)) );
     {$endif}
     FKioscoIniciado := False;
     inherited;

     FKioscoRegistry.InitKioskoRegistry;

     {$ifdef DOS_CAPAS}
     FServerPortalTress := TdmPortalTress.Create( Self );
     FServerComparte := TdmServerComparte.Create( self );
     FServerReportes := TdmServerReportes.Create( self );
     {$endif}

     if dmDiccionario = NIL then
        dmDiccionario := TdmDiccionario.Create( Self );

     GlobalComparte := TdmGlobalComparte.Create;
     GlobalComparte.Conectar;



     FParametros:= TZetaParams.Create;

     Usuario := 999;

     FFormaDatos := nil;
     FFormaInfo := nil;
     FListaFormas := TList.Create;
     FListaBrowsers := TStringList.Create;
     FPosicion := 0;
     //FValoresActivosURL := 'http://dell-marcos/Kiosco2/ValoresActivos.aspx?Kiosco=%s&Empleado=%d&Empresa=%s';
     MuestraFormaPideGafete := True;
     InterceptaMensajes := True;
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
     TimerCarrousel.Enabled := False;
     
     FKioscoRegistry.ClearKioskoRegistry;

     FreeAndNil( FListaFormas );
     FreeAndNil( FListaBrowsers );

     FreeAndNil( FParametros );
     FreeAndNil( GlobalComparte );
     
     {$ifdef DOS_CAPAS}
     FreeAndNil( FServerReportes );
     FreeAndNil( FServerPortalTress );
     FreeAndNil( FServerComparte );
     {$endif}
     inherited;
end;

{$ifndef DOS_CAPAS}
function TdmCliente.GetServerPortalTress: IdmPortalTressDisp;
begin
     Result := IdmPortalTressDisp( CreaServidor( Class_dmPortalTress, FServerPortalTress ) );
end;

function TdmCliente.GetServerComparte: IdmServerComparteDisp;
begin
     Result := IdmServerComparteDisp( CreaServidor( Class_dmServerComparte, FServerComparte ) );
end;

function TdmCliente.GetServerReportes: IdmServerReportesDisp;
begin
     Result:= IdmServerReportesDisp( CreaServidor( CLASS_dmServerReportes, FServerReportes ) );
end;
{$endif}

{ ***** M�todos ******** }

function TdmCliente.PosicionaEmpleado(const sDigito, sCredencial: String; const iEmpleado: TNumEmp; var sTexto: String; var lHuboErrores: Boolean ): Boolean;
begin
     lHuboErrores := False;
     FInterceptaMensajes := False;
     Result := False;
     try
        CompanyFind( sDigito );
        if EmpresaAbierta then
        begin
             with cdsEmpleado do
             begin
                  Data := ServerPortalTress.KioskoEmpleado( Empresa, iEmpleado );
                  if IsEmpty then
                  begin
                       sTexto := Format( 'Empleado %d No Existe', [ iEmpleado ] );
                  end
                  else
                  begin
                       Result := ( sCredencial = FieldByName( 'CB_CREDENC' ).AsString );
                       if NOT Result then
                           sTexto := 'Letra De Credencial Incorrecta'
                       else
                       begin
                            Result := zStrToBool( FieldByName('CB_ACTIVO').AsString ) or
                                      ( FieldByName('CB_FEC_BAJ').AsDateTime > Date );
                            if NOT Result then
                               sTexto := K_INVALIDO;
                       end;

                       if Result then
                       begin
                            FInterceptaMensajes := True;
                            FEmpleadoActivo := iEmpleado;
                            FEmpleadoNombre := FieldByName( 'PRETTYNAME' ).AsString;
                            FEmpleadoNIP := FieldByName('CB_PASSWRD').AsString;
                            FEmpleadoNatalicio := FieldByName( 'CB_FEC_NAC' ).AsDateTime;
                            FTipoNomina := eTipoPeriodo( FieldByName( 'CB_NOMINA' ).AsInteger );
                            //FTipoNomina := eTipoPeriodo( FieldByName( 'TU_NOMINA' ).AsInteger );       <--- se Cambio el campo por el de la tabla COLABORA
                       end;
                  end;
             end;
        end
        else
            sTexto := Format( 'No Hay Empresas Con D�gito %s', [ sDigito ] );
     except
           on Error: Exception do
           begin
                lHuboErrores := True;
                Result := False;
                sTexto := Format( 'Error Al Buscar Empleado %d: %s', [ iEmpleado, Error.Message ] );
                MuestraErrorcito( sTexto );
           end;
     end;
end;

procedure TdmCliente.CompanyFind( const sDigito: String );
begin
     EmpresaAbierta := False;
     with cdsCompany do
     begin
          if Locate( K_CAMPO_DIGITO, sDigito, [] ) then
          begin
               SetCompany;
               EmpresaAbierta := True;
               FEmpresaNombre := FieldByName( 'CM_NOMBRE' ).AsString;
          end;
     end;
end;

procedure TdmCliente.CompanyLoad;
begin
     cdsCompany.Data := Servidor.GetCompanys( 0, Ord( tc3Datos ) );
end;

function TdmCliente.GetIMSSMes: Integer;
begin
     Result := TheMonth(Date);
end;

function TdmCliente.GetIMSSPatron: String;
begin
     Result := '';
end;

function TdmCliente.GetIMSSTipo: eTipoLiqIMSS;
begin
     Result := tlOrdinaria;
end;

function TdmCliente.GetIMSSYear: Integer;
begin
     Result := TheYear( Date );
end;

procedure TdmCliente.CargaActivosSistema;
begin
     with Parametros do
     begin
          AddDate( 'FechaAsistencia', FechaDefault );
          AddDate( 'FechaDefault', FechaDefault );
          AddInteger( 'YearDefault', TheYear( FechaDefault ) );
          AddInteger( 'EmpleadoActivo',  EmpleadoActivo );
          AddString( 'NombreUsuario', 'REP_KIOSKO' );
          AddString( 'CodigoEmpresa', GetDatosEmpresaActiva.Codigo );
     end;
end;

procedure TdmCliente.CargaActivosIMSS(Parametros: TZetaParams);
begin
     with Parametros do
     begin
          AddString( 'RegistroPatronal', ImssPatron );
          AddInteger( 'IMSSYear', ImssYear );
          AddInteger( 'IMSSMes', ImssMes );
          AddInteger( 'IMSSTipo', Ord( ImssTipo ) );
     end;
end;

procedure TdmCliente.CargaActivosPeriodo(Parametros: TZetaParams);
begin
     with cdsPeriodoActivo do
     begin
          if NOT Active or IsEmpty or ( FieldByName('PE_TIPO').AsInteger <> Ord( FTipoNomina ) ) then
             Data := Servidor.GetPeriodoInicial( Empresa, TheYear(Date), Ord( FTipoNomina ) );

          with Parametros do
          begin
               AddInteger( 'Year', FieldByName( 'PE_YEAR' ).AsInteger );
               AddInteger( 'Tipo', FieldByName( 'PE_TIPO' ).AsInteger );
               AddInteger( 'Numero', FieldByName( 'PE_NUMERO' ).AsInteger );
          end;
     end;
end;

procedure TdmCliente.CargaActivosTodos( Parametros: TZetaParams );
begin
     CargaActivosIMSS( Parametros );
     CargaActivosPeriodo( Parametros );
     CargaActivosSistema( Parametros );
end;

function TdmCliente.GetDatosPeriodoActivo: TDatosPeriodo;
begin
     with Result do
     begin
          with cdsPeriodoActivo do
          begin
               Year := FieldByName( 'PE_YEAR' ).AsInteger;
               Tipo := eTipoPeriodo(FieldByName( 'PE_TIPO' ).AsInteger);
               Numero := FieldByName( 'PE_NUMERO' ).AsInteger;
               Inicio := FieldByName( 'PE_FEC_INI' ).AsDateTime;
               Fin := FieldByName( 'PE_FEC_FIN' ).AsDateTime;
               Pago := FieldByName( 'PE_FEC_PAG' ).AsDateTime;
               Dias := FieldByName( 'PE_DIAS' ).AsInteger;
               Status := eStatusPeriodo( FieldByName( 'PE_STATUS' ).AsInteger );
               Uso := eUsoPeriodo( FieldByName( 'PE_USO' ).AsInteger );
               Mes := ZetaCommonTools.iMax( 1, FieldByName( 'PE_MES' ).AsInteger );
               SoloExcepciones := ZetaCommonTools.zStrToBool( FieldByName( 'PE_SOLO_EX' ).AsString );
               IncluyeBajas := ZetaCommonTools.zStrToBool( FieldByName( 'PE_INC_BAJ' ).AsString );
          end;
     end;
end;


{ ******** Datos Personales ********** }
procedure TdmCliente.cdsEmpleadoAlCrearCampos(Sender: TObject);
begin
     inherited;
     with cdsEmpleado do
     begin
          CreateCalculated( 'CB_ANTIGUO', ftString, K_LEN_ANTIGUO );
          CreateCalculated( 'CB_VENCE', ftDate, 0 );
     end;
end;

procedure TdmCliente.cdsEmpleadoCalcFields(DataSet: TDataSet);

function CalculaVencimiento( const dFecha: TDateTime; const iDuracion: Integer ): TDateTime;
//Tomada de D:\3Win_20\Datamodules\DRecursos.pas
begin
     if ( iDuracion = 0 ) then
         Result := 0
     else
         Result := dFecha + iDuracion - 1;
end;

begin
     inherited;
     with cdsEmpleado do
     begin
          FieldByName( 'CB_ANTIGUO' ).AsString := Copy( ZetaCommonTools.TiempoDias( Date - FieldByName( 'CB_FEC_ANT' ).AsDateTime + 1, etDias ), 1, K_LEN_ANTIGUO );
          FieldByName( 'CB_VENCE' ).AsDateTime := CalculaVencimiento( FieldByName( 'CB_FEC_CON').AsDateTime, FieldByName( 'TB_DIAS').AsInteger );
     end;
end;

procedure TdmCliente.CargaReportes;
begin
     //cdsReportes.Data :=  ServerReportes.GetReportes( Empresa, ord(crKiosco), ord(crFavoritos), VACIO , TRUE );
     cdsReportes.Data :=  ServerReportes.GetReportes( Empresa, 15, 13, 19, VACIO, TRUE );  //15 - Kiosco, 13 - Favoritos
end;

function TdmCliente.GetEmpresaReportes: string;
begin
     Result := cdsShow.FieldByName('CM_CODIGO').AsString;
end;

function TdmCliente.GeneraReporte( const iReporte: integer; const Preview: Boolean = TRUE ): Boolean;
begin
     with TdmReportes.Create( self ) do
     begin
          Result := GeneraUnReporte( iReporte, Preview );
     end;
end;

function TdmCliente.GetValorActivo( const eValor : {$ifdef RDD}eRDDValorActivo{$else}string{$endif} ): String;
begin
     {$ifdef RDD}
     with dmCliente do
     begin
          case eValor of
               vaImssPatron: Result := Comillas( IMSSPatron );
               vaImssMes:      Result := IntToStr( IMSSMes );
               vaImssTipo:     Result := IntToStr( Ord( IMSSTipo ) );
               vaImssYear:     Result := IntToStr( IMSSYear );
               vaEmpleado:     Result := IntToStr( Empleado );
               vaNumeroNomina: Result := IntToStr( GetDatosPeriodoActivo.Numero );
               vaTipoNomina:   Result := IntToStr( Ord( GetDatosPeriodoActivo.Tipo ) );
               vaYearNomina:   Result := IntToStr( GetDatosPeriodoActivo.Year );
          else
              Result := inherited GetValorActivo( eValor )
          end;
     end;
     {$else}
     with dmCliente do
     begin
          if ( sValor = K_IMSS_PATRON ) then
             Result := Comillas( IMSSPatron )
          else
              if ( sValor = K_IMSS_YEAR ) then
                 Result := IntToStr( IMSSYear )
              else
                  if ( sValor = K_IMSS_MES ) then
                     Result := IntToStr( IMSSMes )
                  else
                      if ( sValor = K_IMSS_TIPO ) then
                         Result := IntToStr( Ord( IMSSTipo ) )
                      else
                          if ( sValor = K_EMPLEADO ) then
                             Result := IntToStr( Empleado )
                          else
                              Result := inherited GetValorActivo(  sValor );
     end;
     {$endif}
end;
//begin
     //Result := Vacio;
//end;

procedure TdmCliente.GetBotones(const sPantalla: String);
begin
    with cdsBotones do
    begin
         Data := ServerComparte.GetBotones( sPantalla );
    end;
end;

procedure TdmCliente.GetCarrusel(const sCarrusel: String );
begin
     with cdsCarrusel do
     begin
          Data := ServerComparte.GetCarrusel( sCarrusel );
     end;
end;

procedure TdmCliente.GetEstilo(const sEstilo: String );
begin
     with cdsEstilo do
     begin
          Data := ServerComparte.GetEstilo( sEstilo );
     end;
end;

function TdmCliente.GetPantalla(const sPantalla: String ): TBaseKiosco;
begin
     with cdsPantalla do
     begin
          Data := ServerComparte.GetPantalla( sPantalla );
          if Eof then
             Result := nil
          else
          begin
               Application.CreateForm( TBaseKiosco, Result );
          end;
     end;
end;

procedure TdmCliente.ShowLoad( const sShow: String );
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with cdsShow do
        begin
             ZetaSystemWorking.InitAnimation( 'Iniciando Carrousel' );
             Application.ProcessMessages;
             try
                Data := ServerComparte.GetShow( sShow );
             finally
                    ZetaSystemWorking.EndAnimation;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     Application.ProcessMessages;
end;

procedure TdmCliente.ShowConfigure( const sShow: String );
var
   oCursor: TCursor;
   lPuedeSalir: Boolean;
   {CV: Build4 se descontinuo el uso de los campos KW_SCR_INF y KW_TIM_INF
   sFormaInfo,
   }
   sFormaDatos : String;
begin
     FInterceptaMensajes := True;
     FMuestraFormaPideGafete := True;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if ( cdsShow.Active ) and ( cdsShow.RecordCount > 0 ) then
        begin
             // Antes de crear cualquier forma
             with cdsShow do
             begin
                  {CV: Build4 se descontinuo el uso de los campos KW_SCR_INF y KW_TIM_INF
                  sFormaInfo      := FieldByName( 'KW_SCR_INF' ).AsString;
                  FTimeoutDefault := FieldByName( 'KW_TIM_INF' ).AsInteger;
                  }
                  sFormaDatos     := FieldByName( 'KW_SCR_DAT' ).AsString;
                  FTimeoutDatos   := FieldByName( 'KW_TIM_DAT' ).AsInteger * K_MIL;
                  FPideNip        := zStrToBool( FieldByName( 'KW_GET_NIP' ).AsString );
             end;
             {CV: Build4 se descontinuo el uso de los campos KW_SCR_INF y KW_TIM_INF
             cdsPantalla.Data := ServerComparte.GetPantalla( sFormaInfo );
             }
             FFormaInfo := TBaseCarrousel.Create( Application );

             if ( sFormaDatos <> '' ) then
             begin
                 FFormaDatos := GetPantalla( sFormaDatos );
             end
             else
             begin
                  FFormaProblema.MuestraError( Format( 'El carrusel "%s" no tiene asignado ning�n Marco de Informaci�n', [sShow] ) );
             end;
             if ( FFormaDatos <> NIL ) then
             begin
                  if FFormaInfo.PreparaCarrusel( sShow ) then
                     repeat
                        FFormaInfo.ShowCarrusel;
                        lPuedeSalir := FAdministrador;
                        if not lPuedeSalir then
                        begin
                             //lPuedeSalir := FFormaSalida.PreguntaSiPuedeSalir( KioskoRegistry.PasswordSalida )
                             with KioskoRegistry do
                                  lPuedeSalir := ( PideAdminNIP( Intentos, MaxDigitos, PasswordSalida ) = pnrOK )
                        end;
                     until( lPuedeSalir )
                  else
                  begin
                       FreeAndNil( FFormaDatos ); //Se tiene que destruir para que no procese los mensajes;
                       FFormaProblema.MuestraError( Format( 'El carrusel "%s" tiene la lista de p�ginas web vac�a', [sShow] ) );
                  end;
             end;
        end
        else
            FFormaProblema.MuestraError( 'Posibles causas:' + CR_LF +
                                         '1) No se ha asignado el c�digo de kiosco (Ejecutar con el par�metro \CONFIG)' + CR_LF +
                                         Format( '2) El c�digo "%s" de carrusel no existe', [sShow] ) + CR_LF +
                                         '3) El servidor de base de datos no est� activo' );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TdmCliente.GeneraBitacora( const sMensajeError:String );
const
     K_FILENAME = 'K2';
var
   oFile: TextFile;
   sFileName, sFecha, sHoras: String;
   sHora, sMinuto, sSeg, sMsec: Word;
begin
     sFecha := ZetaCommonTools.FechaCorta(Date);
     sFecha := StrTransAll( sFecha, '/', '' );
     DecodeTime( Time, sHora, sMinuto, sSeg, sMsec );
     sHoras := InttoStr(sHora) + ':' + InttoStr(sMinuto) + ':' + InttoStr(sSeg);
     sFileName := K_FILENAME + '-' + sFecha + '.txt';
     try
        {$I-}
        AssignFile( oFile, sFileName );
        if FileExists( sFileName ) then
           Append( oFile )
        else
            Rewrite( oFile );
        Writeln( oFile, '************** Errores para KioscoII( ' + DateToStr( Date ) +' '+ sHoras + ') ********************' );
        Writeln( oFile, sMensajeError );
        Writeln( '______________________________________________________________________________________________________' );
        CloseFile( oFile );
        {$I+}
     except on E: Exception do
            ZetaDialogo.ZError( 'Error al escribir en bit�cora', E.Message, 0 );
     end;
end;

procedure TdmCliente.StartShow;
var
   sShow: String;
begin
     ZetaSystemWorking.InitAnimation( 'Iniciando Carrousel' );
     Application.ProcessMessages;
     try
        CompanyLoad;
     finally
            ZetaSystemWorking.EndAnimation;
     end;
     sShow := KioskoRegistry.CodigoKiosco;
     ShowLoad( sShow );
     // Antes no, debido a que ShowLoad() carga el Show
     FKioscoIniciado := True;
     ShowConfigure( sShow );
end;

procedure TdmCliente.MuestraForma( const iPos: Integer);
var
   //FormaInfo: TBaseKiosco;
   FormaInfo: TBaseCarrousel;
begin
    //FormaInfo := TBaseKiosco( FListaFormas.Items[ iPos ] );
    FormaInfo := TBaseCarrousel( FListaFormas.Items[ iPos ] );
    FormaInfo.ShowContenido;
end;

procedure TdmCliente.MuestraBrowser( const iPos: Integer);
begin
     with TimerCarrousel do
     begin
          Enabled := FALSE;
          Interval := 3000;
          Enabled := TRUE;
     end;
     TWebBrowser( FListaBrowsers.Objects[ iPos ] ).Show;
end;

procedure TdmCliente.MuestraBrowserSiguiente;
var
   iAnterior : Integer;
begin
     iAnterior := FPosicion;
     FPosicion := FPosicion + 1;
     if ( FPosicion >= FListaBrowsers.Count ) then
        FPosicion := 0;
     MuestraBrowser( FPosicion );
     // Primero muestro el nuevo y luego escondo el actual
     // para que se vea mejor la transici�n entre browsers,
     // evitando que se vea el fondo gris de la forma
     TWebBrowser( FListaBrowsers.Objects[ iAnterior ] ).Hide;
end;

function TdmCliente.Init: Boolean;
var
   lOk: Boolean;
   sServidor: String;
begin
     lOk := False;
     with KioskoRegistry do
     begin
          if KioskoConfigurado then
             lOk := True
          else
          begin
               if KioskoRegistry.CanWrite then
               begin
                    if not KioskoRegistry.ClienteConfigurado then
                    begin
                         sServidor := ComputerName;
                         if FServidorName.CapturaServidor( sServidor ) then
                            ComputerName := sServidor;
                    end;
                    if FConfiguraKiosco.ConfiguraKiosco then
                       lOk := KioskoConfigurado;
               end
               else
               begin
                    {
                    No Se puede configurar el Registry debido a que se tiene
                    �nicamente derechos de lectura sobre el mismo
                    }
                    FFormaProblema.MuestraError( 'No se ha configurado este Kiosco' );
               end;
          end;
     end;
     Result := lOk;
end;

procedure TdmCliente.ConfiguracionKiosco;
var
   sServidor: String;
begin
     if FServidorName.CapturaServidor( sServidor ) then
     begin
          KioskoRegistry.ComputerName := sServidor;
          with cdsKioscos do
          begin
               Conectar;
          end;
          if cdsKioscos.Active then
          begin
               FConfiguraKiosco.ConfiguraKiosco;
          end;
     end;
end;

function TdmCliente.SalirDelSistema: Boolean;
begin
     //Result := FFormaSalida.PreguntaSiPuedeSalir( KioskoRegistry.PasswordSalida );
     with KioskoRegistry do
          Result := ( PideAdminNIP( Intentos, MaxDigitos, PasswordSalida ) = pnrOK )
end;

procedure TdmCliente.TimerCarrouselTimer(Sender: TObject);
begin
     inherited;
     MuestraBrowserSiguiente;
end;

function TdmCliente.BuildValoresActivos: String;
begin
     Result := ServerComparte.GetValoresActivos( GetCodigoKioscoID, EmpleadoActivo, cdsCompany.FieldByName( 'CM_CODIGO' ).AsString );
end;

function TdmCliente.VerificaCodigoKiosco: Boolean;
begin
     Result := ( strLleno( KioskoRegistry.CodigoKiosco ) );
end;

procedure TdmCliente.MuestraFormaError( const Error: Exception );
var
   lSalir: Boolean;
begin
     if KioscoIniciado then
        lSalir := FFormaError.MuestraExcepcion( KioskoRegistry.PasswordSalida, Error )
     else
         lSalir := FFormaProblema.MuestraExcepcion( Error );
     if lSalir then
     begin
          Application.OnMessage := nil;
          Application.Terminate;
     end;
end;

procedure TdmCliente.MuestraErrorcito( const sError: String );
begin
     GeneraBitacora( sError );
     FDlgInformativo.MuestraDialogoInf( sError );
end;

{L�gica para capturar la clave del empleado ( CB_PASSWRD ) se usara en un futuro}
{function TdmCliente.ActualizaClaveEmpleado: Boolean;
const
     K_MENSAJE_NORMAL = 'Captura tu clave de empleado';
     K_MENSAJE_CONFIRMA = 'Confirmar clave anterior';
var
   sProblema, sClave, sClaveAnt, sClaveConf: String;
   lSinClave, lLoop: Boolean;
begin
     Result := False;
     if( StrVacio( cdsEmpleado.FieldbyName('CB_PASSWRD').AsString ) )then
     begin
          lSinClave := True;
          repeat
                if lSinClave then
                begin
                     lLoop := FCambiaClave.PideClaveEmpleado( K_MENSAJE_NORMAL, sClave  );
                     sClaveAnt := sClave;
                end
                else
                begin
                     lLoop := FCambiaClave.PideClaveEmpleadoOtraVez( sProblema, K_MENSAJE_CONFIRMA, sClave );
                     sClaveConf  := sClave;
                end;
                if( StrVacio( sClave ) )then
                    sProblema := ' La clave de empleado no puede quedar vac�a'
                else
                    if not lSinclave then
                    begin
                         if ( sClaveAnt <> sClaveConf )then
                         begin
                              lSinClave := False;
                              sProblema := 'La confirmaci�n no coincide con la clave original';
                         end;
                    end
                    else
                    begin
                         if not lSinClave then
                         begin
                              try
                                 ServerPortalTress.SetClaveEmpleadoKiosko( Empresa, EmpleadoActivo, sClave );
                                 lLoop := False;
                                 Result := True;
                              except on Error: Exception do
                                     begin
                                          lLoop := True;
                                          sProblema := ' Error al actualizar la clave del empleado - Por favor intente m�s tarde';
                                     end;
                              end;
                         end;
                         lSinClave := False;
                    end;
          until not lLoop;
     end;
end;}

function TdmCliente.ValidaGafete( sGafete: String; var sError: String; var lHuboErrores: Boolean ): Boolean;
const
     K_GAFETE_DESCONOCIDO = 'Formato desconocido';
     K_VACIO = 'Gafete Vac�o';
var
   sDigito, sCredencial, sDigitosValidos: String;
   iEmpleado: Integer;
begin
     FInterceptaMensajes := False;
     Result := False;
     if( ZetaCommonTools.StrVacio( sGafete ) )then
     begin
          sError := K_VACIO;
     end
     else
     begin
          //CV: 12/Abril/2006
          //CV: Cambio que se efect�o para Electrolux
          with KioskoRegistry do
          begin
               sGafete := Trim(DigitoEmpresa) + sGafete + Trim(LetraGafete);
               sDigitosValidos := EmpresasAutorizadas;
          end;
          try
             sDigito := Copy( sGafete, 1, 1 );
             if StrLleno(sDigitosValidos) and (Pos(sDigito, sDigitosValidos) = 0) then
                sError := 'El Empleado no est� autorizado para utilizar este Kiosco'
             else
             begin
                  try
                     iEmpleado := StrToInt( Copy( sGafete, 2, Length(sGafete) - 2 ) );
                  except
                        on Error: Exception do
                        begin
                             sError := K_INVALIDO;
                             exit;
                        end;
                  end;
                  sCredencial := Copy( sGafete, Length( sGafete ), 1 );
                  Result := PosicionaEmpleado( sDigito, sCredencial, iEmpleado, sError, lHuboErrores );
                  FInterceptaMensajes := Result;
             end;
          except
                on Error: Exception do
                begin
                     sError := K_GAFETE_DESCONOCIDO;
                end;
          end;
     end;
end;

procedure TdmCliente.MuestraPideGafete;
begin
     MuestraPideGafete( VACIO );
end;

{$IFDEF PIDEGAFETE}
procedure TdmCliente.MuestraPideGafete( const sGafeteTeclado: string );
var
   sGafete, sProblema, sClave: String;
   lLoop, lPrimero, lEmpleado, lHuboErrores, lOk: Boolean;
   oCursor: TCursor;
begin
     FInterceptaMensajes := False;
     lPrimero := True;
     FAdministrador := False;
     lEmpleado := False;
     sGafete := sGafeteTeclado;

     repeat
           if StrVacio( sGafete ) then
           begin
                if lPrimero then
                   lLoop := FPideGafete.PideElGafete( sGafete )
                else
                    lLoop := FPideGafete.PideElGafeteOtraVez( sProblema, sGafete );
                lPrimero := False;
           end
           else lLoop := TRUE;
           
           if lLoop then
           begin
                if ( sGafete = KioskoRegistry.GafeteAdministrador ) then
                begin
                     { Se ley� el gafete del Administrador del Kiosko }
                     FAdministrador := True;
                     lLoop := False;
                end
                else
                begin
                     oCursor := Screen.Cursor;
                     Screen.Cursor := crHourglass;
                     try
                        if ValidaGafete( sGafete, sProblema, lHuboErrores ) then
                        begin
                             { Se ley� el gafete de un empleado }
                             lEmpleado := True;
                             lLoop := False;
                        end
                        else
                            sGafete := VACIO;
                        if( lHuboErrores ) then
                            lLoop := False;
                     finally
                            Screen.Cursor := oCursor;
                     end;
                end;
           end;
     until not lLoop;
     if FAdministrador then
     begin
          { Pedir el NIP del Administrador para Salir }
          if SalirDelSistema then
          begin
               Application.OnMessage := nil;
               Application.Terminate;
          end;
     end
     else
     begin
          if lEmpleado then
          begin
               lOk := False;
               sProblema := '';
               if ZetaCommonTools.StrLleno( Self.EmpleadoNIP )then
               begin
                    { Pedir el NIP del Empleado }
                    lOk := PideNIPEmpleado;
               end
               else
               begin
                    if CapturarClaveNueva( Self.EmpleadoNatalicio, sClave ) then
                    begin
                         lOk := Self.EscribeNIPEmpleado( sClave );
                    end;
               end;
               {
                  FDlgInformativo.MuestraDialogoInf( 'Es necesario activar su gafete para poder realizar consultas de informaci�n personal' );
               }
               if lOk then
               begin
                    { Valores Activos }
                    if FValoresActivos.EnviaValoresActivos( BuildValoresActivos, sProblema ) then
                    begin
                         { Llamar Forma de Datos }
                         with FormaDatos do
                         begin
                              ShowPrimerBoton;
                              CleanPantallaEmpAnterior;
                         end;
                    end
                    else
                    begin
                         FDlgInformativo.MuestraDialogoInf( sProblema );
                    end;
               end;
          end;
     end;
end;
{$ELSE}
procedure TdmCliente.MuestraPideGafete( const sGafeteTeclado: string );
var
   sGafete, sProblema, sClave: String;
   lEmpleado, lHuboErrores, lOk: Boolean;
   oCursor: TCursor;
begin
     FInterceptaMensajes := False;
     FAdministrador := False;
     lEmpleado := False;
     sGafete := sGafeteTeclado;

     if ( sGafete = KioskoRegistry.GafeteAdministrador ) then
     begin
          { Se ley� el gafete del Administrador del Kiosko }
          FAdministrador := True;
     end
     else
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             if ValidaGafete( sGafete, sProblema, lHuboErrores ) then
             begin
                  { Se ley� el gafete de un empleado }
                  lEmpleado := True;
             end
             else
                 FDlgInformativo.MuestraDialogoInf( sProblema );

          finally
                 Screen.Cursor := oCursor;
          end;
     end;
     if FAdministrador then
     begin
          { Pedir el NIP del Administrador para Salir }
          if (NOT FPideNip) OR SalirDelSistema then
          begin
               Application.OnMessage := nil;
               Application.Terminate;
          end;
     end
     else
     begin
          if lEmpleado then
          begin
               lOk := False;
               sProblema := '';
               if FPideNip then
               begin
                    if ZetaCommonTools.StrLleno( Self.EmpleadoNIP ) then
                    begin
                         { Pedir el NIP del Empleado }
                         lOk := PideNIPEmpleado;
                    end
                    else
                    begin
                         if CapturarClaveNueva( Self.EmpleadoNatalicio, sClave ) then
                         begin
                              lOk := Self.EscribeNIPEmpleado( sClave );
                         end;
                    end;
               end
               else
                   lOk := TRUE;
                   
               if lOk then
               begin
                    { Valores Activos }
                    if FValoresActivos.EnviaValoresActivos( BuildValoresActivos, sProblema ) then
                    begin
                         { Llamar Forma de Datos }
                         with FormaDatos do
                         begin
                              ShowPrimerBoton;
                              CleanPantallaEmpAnterior;
                         end;
                    end
                    else
                    begin
                         FDlgInformativo.MuestraDialogoInf( sProblema );
                    end;
               end;
          end;
     end;
end;
{$ENDIF}

function TdmCliente.EscribeNIPEmpleado( const sClave: String ): Boolean;
var
   oCursor: TCursor;
begin
     Result := False;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           ServerPortalTress.SetClaveEmpleadoKiosko( Empresa, EmpleadoActivo, sClave );
           Self.FEmpleadoNIP := sClave;
           Result := True;
        except
              on Error: Exception do
              begin
                   Error.Message := ' Error al actualizar la clave del empleado: ' + Error.Message;
                   raise;
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;


function TdmCliente.PideNIPEmpleado( FParent: TWinControl ): Boolean;
var
   sClaveNueva: String;
begin
     case PideClaveNIP( KioskoRegistry.Intentos, KioskoRegistry.MaxDigitos, EmpleadoNIP ) of
          pnrOK: Result := True;
          pnrChangePassword:
          begin
               Result := False;
               if CambiaClaveEmpleado( sClaveNueva, KioskoRegistry.MaxDigitos  ) then
               begin
                    Result := EscribeNIPEmpleado( sClaveNueva );
                    if Result then
                       FDlgInformativo.MuestraDialogoInf( 'Su clave ha sido cambiada' );
               end;
          end;
     else
         Result := False;
     end;
end;

function TdmCliente.CambiaNIPEmpleado(FParent: TWinControl = NIL): Boolean;

begin
     Result := FCambiaNip.CambiaNIPEmpleado( KioskoRegistry.MaxDigitos, FParent );
end;


{ ****** cdsKioscos ***********}

procedure TdmCliente.cdsKioscosAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     cdsKioscos.Data := ServerComparte.GetKioscos;
end;

function TdmCliente.GetCodigoKioscoID: string;
begin
     {$ifdef Antes}
     with KioskoRegistry do
          Result := FID + '_' + CodigoKiosco;
     {$else}
     Result := GetComputerName;
     {$endif}
end;

function TdmCliente.GetURLKiosco( const sURL : string; const eAccion: eAccionBoton = abContenido ): string;

begin
     Result := sURL;
     case eAccion of
          abMisdatos:
          begin
               Result := StrTransAll( ZetaCommonTools.VerificaDirURL(GlobalComparte.GetGlobalString( K_KIOSCO_DIRECTORIO_RAIZ )) +
                         GlobalComparte.GetGlobalString( K_KIOSCO_PAGINA_PANTALLA ), K_ARROBA_PANTALLA, Result );
          end;
     end;

     Result := StrTransAll( Result, K_ARROBA_KIOSCO, GetCodigoKioscoID );
     Result := StrTransAll( Result, K_ARROBA_SERVIDOR, GlobalComparte.GetGlobalString( K_KIOSCO_DIRECTORIO_RAIZ ) );

end;

end.





