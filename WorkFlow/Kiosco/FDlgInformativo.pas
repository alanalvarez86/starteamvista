unit FDlgInformativo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  fcImage, fcimageform, fcLabel, ExtCtrls, fcButton,
  fcImgBtn, fcShapeBtn;

type
  TDlgInformativo = class(TForm)
    fcImageForm1: TfcImageForm;
    fcTexto: TfcLabel;
    btnOk: TfcShapeBtn;
    TimerSalida: TTimer;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TimerSalidaTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FMensaje: String;
    procedure ResetTimerSalida;
  public
    { Public declarations }
    property Mensaje: String read FMensaje write FMensaje;
  end;

procedure MuestraDialogoInf( const sMensaje: String );

implementation

uses DCliente;

var
  DlgInformativo: TDlgInformativo;

{$R *.DFM}

procedure MuestraDialogoInf( const sMensaje: String );
begin
     DlgInformativo := TDlgInformativo.Create( Application );
     try
        with DlgInformativo do
        begin
             Mensaje := sMensaje;
             ShowModal;
        end;
     finally
            FreeAndNil( DlgInformativo );
     end;
end;

procedure TDlgInformativo.FormShow(Sender: TObject);
begin
     fcTexto.Caption := Mensaje;
     ResetTimerSalida;
end;

procedure TDlgInformativo.btnOkClick(Sender: TObject);
begin
     Close;
end;

procedure TDlgInformativo.ResetTimerSalida;
begin
     with TimerSalida do
     begin
          Enabled := False;
          Interval := dmCliente.TimeoutDatos;
          Enabled := True;
     end;
end;

procedure TDlgInformativo.TimerSalidaTimer(Sender: TObject);
begin
     Close;
end;

procedure TDlgInformativo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     dmCliente.InterceptaMensajes := True;
     dmCliente.MuestraFormaPideGafete := False;
end;

end.
