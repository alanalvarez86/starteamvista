unit FBaseCarrousel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OleCtrls, SHDocVw, ExtCtrls, StdCtrls, fcImager, fcpanel, Db, DbClient, Mask,
  ActiveX, DSIntf, DBCommon, StdVcl;

type
  TCarrouselBrowser = class;

  TShowInfo = class(TObject)
  public
    { Public declarations }
    URL : String;
    Browser : TCarrouselBrowser;
    Timeout : Integer;
    Refrescar: Boolean;
  end;

  TBaseCarrousel = class( TForm )
    Timer: TTimer;
    TimerTeclado: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure TimerTecladoTimer(Sender: TObject);
  private
    { Private declarations }
    FURLContenido : String;
    FTimeout : Integer;
    FListaBrowsers : TList;
    FPosBrowser : Integer;
    FGafete: string;
    FCodigoSalida: Boolean;
    procedure ManejaMensaje(var Msg: TMsg; var Handled: Boolean);

    procedure MuestraBrowser;
    procedure MuestraBrowserSiguiente;
    procedure ResetTimer( const nTimeout : Integer );
    procedure ResetTimerTeclado;
  public
    { Public declarations }
    property  Timeout : Integer read FTimeout write FTimeout;
    procedure ShowContenido;
    //procedure ShowPrimerBoton;
    procedure SetURLContenido( sURL : String );
    function PreparaCarrusel( const sCarrusel: String ): Boolean;
    procedure ShowCarrusel;
  end;

 TCarrouselBrowser = class( TWebBrowser )
    procedure AntesDeNavegar(Sender: TObject; const pDisp: IDispatch; var URL, Flags, TargetFrameName, PostData, Headers: OleVariant; var Cancel: WordBool);
    procedure TerminoCargaDocumento(Sender: TObject; const pDisp: IDispatch; var URL: OleVariant);
  private
    { Private declarations }
    FWindowsProc: TWndMethod;
    FBase: TBaseCarrousel;
    //procedure ManejaMensajes(var Message: TMessage);
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    procedure Navegar( const lRefrescar: Boolean );
  end;

var
  BaseCarrousel: TBaseCarrousel;

implementation


{$R *.DFM}

uses DCliente,
     ZetaCommonClasses,
     ZetaCommonTools;

{********** TBaseCarrousel **********}
procedure TBaseCarrousel.FormCreate(Sender: TObject);
begin
     ZetaCommonTools.Environment;
     FTimeout     := dmCliente.TimeoutDatos;
     Application.OnMessage := ManejaMensaje;
     FGafete := '';
end;

procedure TBaseCarrousel.ShowContenido;
begin
    ResetTimer( self.Timeout );
    ShowModal;
end;

procedure TBaseCarrousel.SetURLContenido(sURL: String);
begin
    FURLContenido := sURL;
end;

procedure TBaseCarrousel.ResetTimer( const nTimeout : Integer );
begin
     with Timer do
     begin
          Enabled := FALSE;
          Interval := nTimeout;
          Enabled := TRUE;
     end;
end;

procedure TBaseCarrousel.FormDestroy(Sender: TObject);
var
    i: Integer;
begin
     inherited;
     if ( FListaBrowsers <> NIL ) then
     begin
          with FListaBrowsers do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    TShowInfo( Items[ i ] ).Free;
               end;
               Free;
          end;
     end;
end;

procedure TBaseCarrousel.FormShow(Sender: TObject);
begin

end;

{ TFormaCarrusel }
procedure TBaseCarrousel.MuestraBrowser;
var
    oShowInfo : TShowInfo;
begin
    oShowInfo := FListaBrowsers.Items[ FPosBrowser ];

    ResetTimer( oShowInfo.Timeout );
    oShowInfo.Browser.Navegar( self.Visible and oShowInfo.Refrescar ) ;

end;

procedure TBaseCarrousel.MuestraBrowserSiguiente;
var
    oShowInfoAnterior : TShowInfo;
begin
     if ( FListaBrowsers.Count <> 1 ) then
     begin
          oShowInfoAnterior := FListaBrowsers.Items[ FPosBrowser ];

          FPosBrowser := FPosBrowser + 1;
          // Si ya lleg� a la �ltima pantalla del carrusel, da la vuela a la primera
          if ( FPosBrowser >= FListaBrowsers.Count ) then
                  FPosBrowser := 0;
          MuestraBrowser;

          // Primero muestro el nuevo y luego escondo el actual
          // para que se vea mejor la transici�n entre browsers,
          // evitando que se vea el fondo gris de la forma
          oShowInfoAnterior.Browser.Hide;
     end
     else
     begin
          MuestraBrowser;
     end;
end;

function TBaseCarrousel.PreparaCarrusel( const sCarrusel: String ): Boolean;
var
    oShowInfo : TShowInfo;
    oBrowser  : TCarrouselBrowser;
begin
    dmCliente.GetCarrusel( sCarrusel );
    FListaBrowsers := TList.Create;

    with dmCliente.cdsCarrusel do
    begin
        Result := not Eof;
        while not Eof do
        begin
            oBrowser := TCarrouselBrowser.Create( self );
            TWinControl( oBrowser ).Parent := self;
            oShowInfo := TShowInfo.Create;

            with oShowInfo do
            begin
                URL     := dmCliente.GetURLKiosco( FieldByName( 'KI_URL' ).AsString );
                Timeout := FieldByName( 'KI_TIMEOUT' ).AsInteger * K_MIL;
                Refrescar := ZStrToBool( FieldByName( 'KI_REFRESH').AsString );
                Browser := oBrowser;
            end;

            with oBrowser do
            begin
                 Align := alClient;
                 Hide;
                 Navigate( oShowInfo.URL );
            end;
            FListaBrowsers.Add( oShowInfo );
            Next;
        end;
    end;
end;

procedure TBaseCarrousel.ShowCarrusel;
begin
     FPosBrowser := 0;
     MuestraBrowser;
     ShowModal;
end;

procedure TBaseCarrousel.TimerTimer(Sender: TObject);
begin
     MuestraBrowserSiguiente;
end;

procedure TBaseCarrousel.ManejaMensaje(var Msg: TMsg; var Handled: Boolean);
 const
      VK_JOTA = 'm';
      VK_SALIDA = Chr( VK_CONTROL ) + VK_JOTA;
 var
    cLetra: Char;
    iLetra : integer;
begin
     if ( Screen.ActiveForm <> NIL ) and ( Screen.ActiveForm.Name = Self.Name ) then
     begin
          Handled := True;
          case Msg.Message of
               //WM_MBUTTONUP: ShowMessage( 'WM_MBUTTONUP' );

               WM_KEYDOWN:
               begin
                    if ( Msg.wParam = VK_RETURN ) or
                       ( FCodigoSalida and (Msg.wParam = 77) ) then
                    begin
                         try
                            dmCliente.MuestraPideGafete( fGafete );
                         finally
                                FGafete := VACIO;
                                FCodigoSalida := FALSE;
                         end;
                    end
                    else if ( Msg.wParam = VK_CONTROL ) then
                    begin
                         FCodigoSalida := TRUE;
                    end
                    else
                    begin
                         iLetra := Msg.wParam;

                         if ( iLetra in [96..105] ) then
                            iLetra := iLetra - 48;

                         if ( iLetra in [65..90] ) or
                            ( iLetra in [48..57] ) then
                         begin
                              cLetra := chr( iLetra );
                              ResetTimerTeclado;
                              FGafete := FGafete + cLetra;
                         end;
                    end;
               end;
               {$IFDEF PIDEGAFETE}
               WM_LBUTTONDOWN:
               begin
                    with dmCliente do
                    begin
                         MuestraPideGafete;
                    end;
               end;
               {$ENDIF}
          else
              Handled := False;
          end;
     end;
end;

{ ***** TCarrouselBrowser ***** }

constructor TCarrouselBrowser.Create(AOwner: TComponent);
begin
     FBase := TBaseCarrousel( AOwner );
     inherited Create( AOwner );
     Self.OnBeforeNavigate2 := AntesDeNavegar;
     Self.OnDocumentComplete := TerminoCargaDocumento;
     AddressBar := False;
     MenuBar := False;
     Silent := True;
     StatusBar := False;
     Offline := False;
     FWindowsProc := Self.WindowProc;
end;

procedure TCarrouselBrowser.TerminoCargaDocumento(Sender: TObject; const pDisp: IDispatch; var URL: OleVariant);
begin
     if( Assigned( Self.Document ) )then
     begin
          Self.OleObject.Document.Body.Style.OverflowX := 'hidden';
          Self.OleObject.Document.Body.Style.OverflowY := 'hidden';
     end;
end;

procedure TCarrouselBrowser.AntesDeNavegar(Sender: TObject; const pDisp: IDispatch; var URL, Flags, TargetFrameName, PostData, Headers: OleVariant; var Cancel: WordBool);
begin
     if( ReadyState = READYSTATE_COMPLETE )then
     begin
          Cancel := True;
     end;
end;

procedure TCarrouselBrowser.Navegar( const lRefrescar: Boolean );

var
   Level : Olevariant;
begin
     if lRefrescar then
     begin
          Level := REFRESH_COMPLETELY;
          Refresh2( Level );
     end;
     Show;
end;

procedure TBaseCarrousel.TimerTecladoTimer(Sender: TObject);
begin
     FGafete := VACIO;
     TimerTeclado.Enabled := FALSE;
end;

procedure TBaseCarrousel.ResetTimerTeclado;
begin
     TimerTeclado.Enabled := FALSE;
     TimerTeclado.Enabled := TRUE;
end;

end.

