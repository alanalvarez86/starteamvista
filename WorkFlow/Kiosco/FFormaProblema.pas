unit FFormaProblema;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  fcButton, fcImgBtn, fcShapeBtn, StdCtrls, fcpanel, ExtCtrls;

type
  TFormaProblema = class(TForm)
    PanelPrincipal: TPanel;
    gbError: TfcGroupBox;
    memoError: TMemo;
    fcOK: TfcShapeBtn;
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    procedure SetErrorMsg(const Value: String);
  public
    { Public declarations }
    property ErrorMsg: String write SetErrorMsg;
  end;

function MuestraError( const sError: String ): Boolean;
function MuestraExcepcion( Error: Exception ): Boolean;

implementation

uses DCliente;

{$R *.DFM}

var
  FormaProblema: TFormaProblema;

function MuestraError( const sError: String ): Boolean;
begin
     Result := False;
     if not Assigned( FormaProblema ) then
        FormaProblema := TFormaProblema.Create( Application );
     with FormaProblema do
     begin
          ErrorMsg := sError;
          WindowState := wsMaximized;
          ShowModal;
     end;
end;

function MuestraExcepcion( Error: Exception ): Boolean;
begin
     Result := MuestraError( Error.Message );
end;

procedure TFormaProblema.FormResize(Sender: TObject);
begin
     with PanelPrincipal do
     begin
          if ( Self.Height > Height ) then
             Top := Trunc( ( Self.Height - Height ) / 2 );
          if ( Self.Width > Width ) then
             Left := Trunc( ( Self.Width - Width ) / 2 );
     end;
end;

procedure TFormaProblema.SetErrorMsg(const Value: String);
begin
     with memoError.Lines do
     begin
          Clear;
          BeginUpdate;
          try
             Add( Value );
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TFormaProblema.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     dmCliente.InterceptaMensajes := True;
end;

end.
