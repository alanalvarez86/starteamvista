unit DSistema;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     {$ifndef VER130}Variants,{$endif}
     DBaseSistema,
     ZetaClientDataSet,
     ZetaServerDataSet;

type
  TdmSistema = class(TdmBaseSistema)
    cdsUsuariosRol: TZetaClientDataSet;
    cdsUsuarioRoles: TZetaClientDataSet;
    cdsRolUsuarios: TZetaLookupDataSet;
    procedure cdsUsuariosRolAlAdquirirDatos(Sender: TObject);
    procedure cdsUsuariosRolAfterOpen(DataSet: TDataSet);
    procedure cdsUsuariosRolAfterScroll(DataSet: TDataSet);
    procedure cdsUsuariosRolAlModificar(Sender: TObject);
    procedure cdsUsuariosRolAfterCancel(DataSet: TDataSet);
    procedure cdsUsuariosRolAlEnviarDatos(Sender: TObject);
    procedure cdsUsuariosLookupLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
    procedure cdsGruposLookupGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsUsuariosAlBorrar(Sender: TObject);
    procedure cdsRolesAlAdquirirDatos(Sender: TObject);
    procedure cdsRolesAlEnviarDatos(Sender: TObject);
    procedure cdsRolesAlModificar(Sender: TObject);
    procedure cdsRolesBeforePost(DataSet: TDataSet);
    procedure cdsRolesGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    procedure cdsRolesAfterCancel(DataSet: TDataSet);
    procedure cdsRolesAfterScroll(DataSet: TDataSet);
    procedure cdsRolesAfterDelete(DataSet: TDataSet);
  private
    { Private declarations }
    FEditing :Boolean;
    procedure US_JEFEGetText(Sender: TField; var Text: String; DisplayText: Boolean);

  protected
    { Protected declarations }
    procedure GetRolesUsuario(const iUsuario: Integer);
  public
    { Public declarations }
    function BuscarUsuario(const sFiltro: String; var iUsuario: Integer; var sNombre: String): Boolean;
    function GetUsuarioActual: Integer;
    procedure CambiarAccesos;
    procedure ConectarUsuariosLookup;
    procedure EditarUsuarioRol;
    procedure UsuarioRolAgregar(const sRol, sNombre: String);
    procedure UsuarioRolAgregarTodos;
    procedure UsuarioRolBorrar;
    procedure UsuarioRolBorrarTodos;
    procedure RolUsuarioAgregar( const iUsuario: Integer; const sNombre: String );
    procedure RolUsuarioBorrar;
    procedure RolEditar;
    procedure RolUsuarioAgregarTodos;
    procedure RolUsuarioBorrarTodos;
    function RolBuscar(const sFiltro: String; var sCodigo,sNombre: String): Boolean;
    procedure ConectarRolesLookup;
    procedure RolUsuarioModeloGet( const sRol: String );
    function RolGetActivo: String;
    
  end;

var
  dmSistema: TdmSistema;

implementation

uses ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,     
     ZBaseEdicion,
     ZetaDialogo,
     ZetaMsgDlg,
     FCatUsuariosEdit,
     DWorkFlow,
     FCatRoles,
     FCatRolesEdit,
     DCliente;

{$R *.DFM}

{ TdmSistema }

procedure TdmSistema.CambiarAccesos;
begin
     with cdsEmpresasAccesos do
     begin
          Refrescar;
          Append;
          with dmCliente.GetDatosEmpresaActiva do
          begin
               FieldByName( 'CM_CODIGO' ).AsString := Codigo;
               FieldByName( 'CM_NOMBRE' ).AsString := Nombre;
          end;
          FieldByName( 'ACCESOS' ).AsInteger := 1;
          Post;
          MergeChangeLog;
     end;
     DBaseSistema.CambiarAccesos;
end;

procedure TdmSistema.cdsGruposLookupGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     inherited;
     lHasRights := False;
end;

{ *** cdsUsuariosLookUp *** }

procedure TdmSistema.ConectarUsuariosLookup;
var
   iLongitud: Integer;
   lBlockSistema: WordBool;
begin
     with cdsUsuariosLookup do
     begin
          Data := ServerSistema.GetUsuarios( iLongitud, ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION, lBlockSistema );
     end;
end;

function TdmSistema.BuscarUsuario( const sFiltro: String; var iUsuario: Integer; var sNombre: String ): Boolean;
var
   sUsuario: String;
begin
     ConectarUsuariosLookup;
     Result := cdsUsuariosLookup.Search( sFiltro, sUsuario, sNombre );
     if Result then
     begin
          iUsuario := StrToIntDef( sUsuario, 0 );
          Result := ( iUsuario > 0 );
     end;
end;

procedure TdmSistema.cdsUsuariosLookupLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
begin
     inherited;
     if ( sKey = '0' ) then
     begin
          sDescription := '< Sin Asignar >';
          lOk := True;
     end
     else
     begin
          with cdsUsuariosLookup do
          begin
               OnLookupKey := nil;
               try
                  lOk := LookupKey( sKey, sFilter, sDescription );
               finally
                      OnLookupKey := Self.cdsUsuariosLookupLookupKey;
               end;
          end;
     end;
end;

{ *** cdsUsuariosRol *** }

procedure TdmSistema.cdsUsuariosRolAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          cdsUsuariosRol.Data := ServerComparte.GetUsuarios( GetGrupoActivo );
     end;
end;

procedure TdmSistema.cdsUsuariosRolAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsUsuariosRol do
     begin
          ListaFija( 'US_FORMATO', lfEmailType );
          FieldByName( 'US_JEFE' ).OnGetText := US_JEFEGetText;
     end;
end;

procedure TdmSistema.US_JEFEGetText( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     if DisplayText then
     begin
          if ( Sender.AsInteger <= 0 ) then
             Text := ''
          else
             Text := Sender.AsString;
     end;
end;

function TdmSistema.GetUsuarioActual: Integer;
begin
     Result := cdsUsuariosRol.FieldByName( 'US_CODIGO' ).AsInteger;
end;

procedure TdmSistema.GetRolesUsuario(const iUsuario: Integer);
begin
     cdsUsuarioRoles.Data := dmCliente.ServerComparte.GetRolesUsuario( iUsuario );
end;

procedure TdmSistema.EditarUsuarioRol;
begin
     with cdsUsuariosRol do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
     end;
end;

procedure TdmSistema.cdsUsuariosRolAfterScroll(DataSet: TDataSet);
begin
     inherited;
     GetRolesUsuario( GetUsuarioActual );
end;

procedure TdmSistema.cdsUsuariosRolAlModificar(Sender: TObject);
begin
     inherited;
     GetRolesUsuario( GetUsuarioActual );
     ZBaseEdicion.ShowFormaEdicion( CatUsuariosEdit, TCatUsuariosEdit );
end;

procedure TdmSistema.cdsUsuariosRolAfterCancel(DataSet: TDataSet);
begin
     inherited;
     GetRolesUsuario( GetUsuarioActual );
end;

procedure TdmSistema.cdsUsuariosRolAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   Roles, Usuarios: OleVariant;
begin
     inherited;
     ErrorCount := 0;
     with cdsUsuarioRoles do
     begin
          PostData;
          MergeChangeLog;
          Roles := Data;
     end;
     with cdsUsuariosRol do
     begin
          PostData;
          Usuarios := DeltaNull;
          if not VarIsNull( Usuarios ) or not VarIsNull( Roles ) then
          begin
               Reconcile( dmCliente.ServerComparte.GrabaUsuarios( GetUsuarioActual, Usuarios, Roles, ErrorCount ) );
          end;
     end;
end;

{ *** cdsUsuarioRoles *** }

procedure TdmSistema.UsuarioRolAgregar( const sRol, sNombre: String );
var
   iUsuario: Integer;
begin
     iUsuario := GetUsuarioActual;
     if ( iUsuario = 0 ) then
        ZetaDialogo.zError( '� C�digo De Usuario Inexistente !', 'Antes De Agregar Roles Es Necesario Especificar El C�digo Del Usuario', 0 )
     else
     begin
          with cdsUsuarioRoles do
          begin
               if not Locate( 'US_CODIGO;RO_CODIGO', VarArrayOf( [ iUsuario, sRol ] ), [] ) then
               begin
                    Append;
                    FieldByName( 'US_CODIGO' ).AsInteger := iUsuario;
                    FieldByName( 'RO_CODIGO' ).AsString := sRol;
                    FieldByName( 'RO_NOMBRE' ).AsString := sNombre;
                    Post;
               end;
          end;
          EditarUsuarioRol;
     end;
end;

procedure TdmSistema.UsuarioRolAgregarTodos;
begin
     with cdsUsuarioRoles do
     begin
          DisableControls;
          try
             with cdsRoles do
             begin
                  Conectar;
                  DisableControls;
                  try
                     First;
                     while not Eof do
                     begin
                          UsuarioRolAgregar( FieldByName( 'RO_CODIGO' ).AsString, FieldByName( 'RO_NOMBRE' ).AsString );
                          Next;
                     end;
                  finally
                         EnableControls;
                  end;
             end;
             First;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TdmSistema.UsuarioRolBorrar;
begin
     with cdsUsuarioRoles do
     begin
          Delete;
     end;
     EditarUsuarioRol;
end;

procedure TdmSistema.UsuarioRolBorrarTodos;
begin
     with cdsUsuarioRoles do
     begin
          EmptyDataset;
     end;
     EditarUsuarioRol;
end;

procedure TdmSistema.RolUsuarioAgregar(const iUsuario: Integer; const sNombre: String);
var
   sRol: String;
begin
     sRol := RolGetActivo;
     if ZetaCommonTools.StrVacio( sRol ) then
        ZetaDialogo.zError( '� C�digo De Rol Inexistente !', 'Antes De Agregar Usuarios Es Necesario Especificar El C�digo Del Rol', 0 )
     else
     begin
          with cdsRolUsuarios do
          begin
               if not Locate( 'RO_CODIGO;US_CODIGO', VarArrayOf( [ sRol, iUsuario ] ), [] ) then
               begin
                    Append;
                    FieldByName( 'RO_CODIGO' ).AsString := sRol;
                    FieldByName( 'US_CODIGO' ).AsInteger := iUsuario;
                    FieldByName( 'US_NOMBRE' ).AsString := sNombre;
                    Post;
               end;
          end;
          RolEditar;
     end;
end;

procedure TdmSistema.RolUsuarioBorrar;
begin
     with cdsRolUsuarios do
     begin
          Delete;
     end;
     RolEditar;
end;

procedure TdmSistema.RolEditar;
begin
     with cdsRoles do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
     end;
end;

procedure TdmSistema.RolUsuarioAgregarTodos;
begin
     with cdsRolUsuarios do

     begin
          DisableControls;
          try
             with dmSistema do

             begin
                  ConectarUsuariosLookup;
                  with cdsUsuariosLookup do
                  begin
                       Conectar;
                       DisableControls;
                       try
                          First;
                          while not Eof do
                          begin
                               RolUsuarioAgregar( FieldByName( 'US_CODIGO' ).AsInteger, FieldByName( 'US_NOMBRE' ).AsString );
                               Next;
                          end;
                       finally
                              EnableControls;
                       end;
                  end;
             end;
             First;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TdmSistema.RolUsuarioBorrarTodos;
begin
     with cdsRolUsuarios do
     begin
          EmptyDataset;
     end;
     RolEditar;
end;

function TdmSistema.RolGetActivo: String;
begin
     Result := cdsRoles.FieldByName( 'RO_CODIGO' ).AsString;
end;



procedure TdmSistema.cdsUsuariosAlBorrar(Sender: TObject);
const
   K_ADV_BORRAR_REGISTRO = '� Desea Borrar Este Registro ?';
{$ifdef COMPARTE_MGR}
   K_ADV_SUPERVISORES ='Este usuario puede pertenecer a un Supervisor Enrolado. Si borra este usuario puede quedar alg�n Supervisor sin enrolar.�Est� seguro que desea borrar este usuario?';
{$endif}
var
   sAdvertencia : string;
begin
    sAdvertencia := K_ADV_BORRAR_REGISTRO;

   {$ifdef COMPARTE_MGR}
    sAdvertencia := K_ADV_SUPERVISORES;
   {$endif}

   if ( ZetaMsgDlg.ConfirmaCambio( sAdvertencia ) ) then
       cdsUsuarios.Delete;
end;

procedure TdmSistema.cdsRolesAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          cdsRoles.Data := ServerComparte.GetRoles;
     end;
end;

procedure TdmSistema.cdsRolesAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   Roles, Usuarios, Modelos: OleVariant;
begin
     ErrorCount := 0;
     with cdsRolUsuarios do
     begin
          PostData;
          MergeChangeLog;
          Usuarios := Data;
     end;
     with dmWorkFlow.cdsRolModelos do
     begin
          PostData;
          MergeChangeLog;
          Modelos := Data;
     end;
     with cdsRoles do
     begin
          PostData;
          Roles := DeltaNull;
          if not VarIsNull( Roles ) or not VarIsNull( Usuarios ) or not VarIsNull( Modelos ) then
          begin
               Reconcile( dmCliente.ServerComparte.GrabaRoles( RolGetActivo, Roles, Usuarios, Modelos, ErrorCount ) );
          end;
     end;
end;

procedure TdmSistema.cdsRolesAlModificar(Sender: TObject);
begin
     FEditing := True;
     try
        RolUsuarioModeloGet( RolGetActivo );
        ZBaseEdicion.ShowFormaEdicion( CatRolesEdit, TCatRolesEdit );
     finally
            FEditing := False;
     end;
end;

procedure TdmSistema.cdsRolesBeforePost(DataSet: TDataSet);
begin
     with cdsRoles do
     begin
          if ZetaCommonTools.StrVacio( FieldByName( 'RO_CODIGO' ).AsString ) then
             DatabaseError( '� C�digo Del Rol No Puede Quedar Vac�o !' );
          if ZetaCommonTools.StrVacio( FieldByName( 'RO_NOMBRE' ).AsString ) then
             DatabaseError( '� Nombre Del Rol No Puede Quedar Vac�o !' );
     end;
end;

procedure TdmSistema.cdsRolesGetRights(Sender: TZetaClientDataSet;
  const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := False;
end;

procedure TdmSistema.cdsRolesAfterCancel(DataSet: TDataSet);
begin
     inherited;
     RolUsuarioModeloGet( RolGetActivo );
end;

procedure TdmSistema.cdsRolesAfterScroll(DataSet: TDataSet);
begin
     if FEditing then
        RolUsuarioModeloGet( RolGetActivo );
end;

procedure TdmSistema.ConectarRolesLookup;
begin
     cdsRoles.Conectar;
end;


function TdmSistema.RolBuscar( const sFiltro: String; var sCodigo, sNombre: String ): Boolean;
begin
     with cdsRoles do
     begin
          Conectar;
          Result := Search( sFiltro, sCodigo, sNombre );
     end;
     if Result then
     begin
          Result := ZetaCommonTools.StrLleno( sCodigo );
     end;
end;

procedure TdmSistema.RolUsuarioModeloGet( const sRol: String );
var
   Modelos: OleVariant;
begin
     cdsRolUsuarios.Data := dmCliente.ServerComparte.GetUsuariosRol( sRol, Modelos );
     dmWorkFlow.cdsRolModelos.Data := Modelos;
end;

procedure TdmSistema.cdsRolesAfterDelete(DataSet: TDataSet);
begin
		 RolUsuarioModeloGet( RolGetActivo );
     cdsRolUsuarios.EmptyDataSet;
		 dmWorkFlow.cdsRolModelos.EmptyDataSet;
     inherited;
     cdsRoles.Enviar;
end;

end.
