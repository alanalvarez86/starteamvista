unit DCliente;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, DBClient,Variants,
     {$ifdef DOS_CAPAS}
     DServerComparte,
     {$else}
     Comparte_TLB,
     {$endif}
     DBasicoCliente,
     ZetaTipoEntidad,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaClientDataSet;

type
  TdmCliente = class(TBasicoCliente)
    cdsEmpleadoLookUp: TZetaLookupDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure MemoGetText( Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsEmpleadoLookUpLookupSearch(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter: String; var sKey,
      sDescription: String);
    procedure cdsEmpleadoLookUpLookupKey(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter, sKey: String;
      var sDescription: String);
  private
    { Private declarations }
    FEmpresaPass: string;
    {$ifdef DOS_CAPAS}
    FServerComparte: TdmServerComparte;
    {$else}
    FServerComparte: IdmServerComparteDisp;
    function GetServerComparte: IdmServerComparteDisp;
    {$endif}
    procedure SetEmpresaPass( const Value: string );
  protected
    { Protected declarations }
  public
    { Public declarations }
    {$ifdef DOS_CAPAS}
    property ServerComparte: TdmServerComparte read FServerComparte;
    {$else}
    property ServerComparte: IdmServerComparteDisp read GetServerComparte;
    {$endif}
    property EmpresaPass : string read FEmpresaPass write SetEmpresaPass;
    function ModoTress: Boolean;
    function GetListaEmpleados( const sFiltro: String ): String;
    function InitComparteCompany: Integer;
    function IsOk( const iValue: Integer ): Boolean;
    function ComputerName: string;
    function ListaCompanies(Lista: TStrings): Boolean;
     //Para Compilar en 2.8
    //procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
    procedure NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
    procedure GetEmpleadosBuscados(const sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca: String; const oEmpresa: OleVariant; const oDataSet: TZetaClientDataSet);
    function GetURLPantalla( const sURL : string ): string;
  end;

var
  dmCliente: TdmCliente;

implementation

uses DSistema,
     ZetaBuscaEmpleado,
     ZetaRegistryCliente,
     ZBaseEdicion;

const
     K_ARROBA_SERVIDOR = '@SERVIDOR';

{$R *.DFM}

{ ***** TdmCliente ***** }

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     inherited;
     {$ifdef DOS_CAPAS}
     FServerComparte := TdmServerComparte.Create( Self );
     {$endif}
     TipoCompany := tcOtro;
     FEmpresaPass:= VACIO;
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
     FreeAndNil( FServerComparte );
     {$endif}
     inherited;
end;

function TdmCliente.ModoTress: Boolean;
begin
     Result := False;
end;

{$ifndef DOS_CAPAS}
function TdmCliente.GetServerComparte: IdmServerComparteDisp;
begin
     Result := IdmServerComparteDisp( CreaServidor( CLASS_dmServerComparte, FServerComparte ) );
end;
{$endif}
 //Para Compilar en 2.8
//procedure TdmCliente.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
procedure TdmCliente.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
begin
     {
     if ( enFolio in Entidades ) then
        cdsFolios.SetDataChange;
     }
end;

function TdmCliente.GetListaEmpleados(const sFiltro: string): string;
begin
     Result := '';
end;

function TdmCliente.InitComparteCompany: Integer;
begin
     with cdsCompany do
     begin
          if not Active or HayQueRefrescar then
          begin
               Data := ServerComparte.GetComparte;
               ResetDataChange;
          end;
          if Active then
             Result := RecordCount
          else
              Result := 0;
     end;
end;

procedure TdmCliente.MemoGetText( Sender: TField; var Text: String; DisplayText: Boolean);
begin
     { Se requiere condicionar para que al Hacer un }
     { cdsNomParam.Cancel no ocurra un Invalid BLOB Handle }
     if Assigned( Sender ) and Assigned( Sender.Dataset ) then
     begin
          if Sender.Dataset.State in [ dsBrowse ] then
             Text := Sender.AsString;
     end;
end;

function TdmCliente.IsOk( const iValue: Integer ): Boolean;
begin
     Result := ( iValue = 0 );
end;

function TdmCliente.ComputerName: string;
begin
     Result := ClientRegistry.ComputerName;
end;

function TdmCliente.ListaCompanies( Lista: TStrings ): Boolean;
begin
     with Lista do
     begin
          BeginUpdate;
          try
             Clear;
             with cdsCompany do
             begin
                  Data := Servidor.GetCompanys( Usuario, ord( tc3Datos ) );
                  while not Eof do
                  begin
                       Add( Format( '%s=%s', [ FieldByName( 'CM_CODIGO' ).AsString, FieldByName( 'CM_NOMBRE' ).AsString ] ) );
                       Next;
                  end;
             end;
          finally
                 EndUpdate;
          end;
          Result := ( Count > 0 );
     end;
end;


procedure TdmCliente.cdsEmpleadoLookUpLookupSearch( Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String;
  var sKey, sDescription: String);
begin
     inherited;
     lOk := ZetaBuscaEmpleado.BuscaEmpleadoDialogo( sFilter, sKey, sDescription );
end;

procedure TdmCliente.cdsEmpleadoLookUpLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
var
   Datos: OleVariant;
begin
     if ( FServidor = nil ) then
        lOk := Servidor.GetLookupEmpleado( Empresa, StrToIntDef( sKey, 0 ), Datos, Ord( eLookEmpGeneral ) )
     else
        lOk := FServidor.GetLookupEmpleado( Empresa, StrToIntDef( sKey, 0 ), Datos, Ord( eLookEmpGeneral ) );
     if lOk then
     begin
          with cdsEmpleadoLookUp do
          begin
               Init;
               Data := Datos;
               sDescription := GetDescription;
          end;
     end
     else
     begin
          with cdsEmpleadoLookUp do
          begin
               Init;
               Data := NULL;
               sDescription := VACIO;
          end;

     end;
end;


procedure TdmCliente.GetEmpleadosBuscados( const sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca: String; const oEmpresa: OleVariant; const oDataSet: TZetaClientDataSet );
begin
     with oDataSet do
     begin
          if ( FServidor = nil ) then
             Data := Servidor.GetEmpleadosBuscados( Empresa, sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca )
          else
             Data := FServidor.GetEmpleadosBuscados( Empresa, sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca );
     end;
end;

procedure TdmCliente.SetEmpresaPass( const Value: string );
begin
     FEmpresaPass := Value;
     if  FEmpresaPass <> '' then
     begin
          FindCompany( FEmpresaPass );
          SetCompany;
     end;
end;


function TdmCliente.GetURLPantalla(const sURL: string): string;
begin
     Result := sURL;
     //Result := StrTransAll( Result, K_ARROBA_SERVIDOR, GlobalComparte.GetGlobalString( K_KIOSCO_DIRECTORIO_RAIZ ) );
end;

end.
