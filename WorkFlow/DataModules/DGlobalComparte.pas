unit DGlobalComparte;

interface

uses Forms, Controls, DB, Classes, StdCtrls, SysUtils,
     {$ifdef DOS_CAPAS}
     DServerComparte,
     {$else}
     Comparte_TLB,
     {$endif}
     DBaseGlobal,
     ZetaCommonLists;

const
     //Estan repetidos con las constantes de \3WorkFlow\Tools\ZGlobalTress.pas
     K_KIOSCO_DIRECTORIO_RAIZ        = 52;
     K_KIOSCO_VALORES_ACTIVOS_URL    = 53;
     K_KIOSCO_PAGINA_PANTALLA        = 54;
     K_TOT_GLOBALES                  = 54;
type
  TdmGlobalComparte = class( TdmBaseGlobal )
  private
    { Private declarations }
    {$ifdef DOS_CAPAS}
    function GetServer: TdmServerComparte;
    {$else}
    function GetServer: IdmServerComparteDisp;
    {$endif}
  protected
    { Protected declarations }
    {$ifdef DOS_CAPAS}
    property Server: TdmServerComparte read GetServer;
    {$else}
    property Server: IdmServerComparteDisp read GetServer;
    {$endif}
    function GetGlobales: Variant; override;
    procedure GrabaGlobales( const aGlobalServer: Variant; const lActualizaDiccion: Boolean; var ErrorCount: Integer ); override;
    function GetTipoGlobal( const i: integer ): eTipoGlobal;override;
    function GetTotalesGlobales: integer; override;

  public
    { Public declarations }
  end;

var
   GlobalComparte: TdmGlobalComparte;

implementation

uses DCliente,
     ZGlobalTress;

{********* TdmGlobal ******** }

{$ifdef DOS_CAPAS}
function TdmGlobalComparte.GetServer: TdmServerComparte;
{$else}
function TdmGlobalComparte.GetServer: IdmServerComparteDisp;
{$endif}
begin
     Result := dmCliente.ServerComparte;
end;

{ER: Si se requiere declarar GetGlobales y GrabaGlobales, porque no se puede declarar el SERVER en la clase base}

function TdmGlobalComparte.GetGlobales: Variant;
begin
     Result := Server.GetGlobales;
end;

procedure TdmGlobalComparte.GrabaGlobales(const aGlobalServer: Variant; const lActualizaDiccion: Boolean; var ErrorCount: Integer);
begin
     Server.GrabaGlobales( aGlobalServer, ErrorCount );
end;

function TdmGlobalComparte.GetTipoGlobal( const i: integer ): eTipoGlobal;
begin
     Result := tgTexto;
end;

function TdmGlobalComparte.GetTotalesGlobales: integer;
begin
     Result := K_TOT_GLOBALES;
end;

end.
