inherited dmSistema: TdmSistema
  OldCreateOrder = True
  Left = 237
  Top = 266
  inherited cdsGrupos: TZetaLookupDataSet
    Aggregates = <
      item
        Active = True
        Expression = 'Max( GR_CODIGO ) + 0'
        Visible = False
      end>
  end
  inherited cdsUsuarios: TZetaLookupDataSet
    AlBorrar = cdsUsuariosAlBorrar
  end
  inherited cdsGruposLookup: TZetaLookupDataSet
    OnGetRights = cdsGruposLookupGetRights
  end
  inherited cdsUsuariosLookup: TZetaLookupDataSet
    OnLookupKey = cdsUsuariosLookupLookupKey
  end
  inherited cdsRoles: TZetaLookupDataSet
    IndexFieldNames = 'RO_CODIGO'
    BeforePost = cdsRolesBeforePost
    AfterCancel = cdsRolesAfterCancel
    AfterDelete = cdsRolesAfterDelete
    AfterScroll = cdsRolesAfterScroll
    AlAdquirirDatos = cdsRolesAlAdquirirDatos
    AlEnviarDatos = cdsRolesAlEnviarDatos
    AlModificar = cdsRolesAlModificar
    LookupName = 'Roles'
    LookupDescriptionField = 'RO_NOMBRE'
    LookupKeyField = 'RO_CODIGO'
    OnGetRights = cdsRolesGetRights
  end
  inherited cdsGruposTodos: TZetaLookupDataSet
    Aggregates = <
      item
        Active = True
        Expression = 'Max( GR_CODIGO ) + 0'
        Visible = False
      end>
    Left = 32
    Top = 200
  end
  object cdsUsuariosRol: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsUsuariosRolAfterOpen
    AfterCancel = cdsUsuariosRolAfterCancel
    AfterScroll = cdsUsuariosRolAfterScroll
    OnReconcileError = GenericReconcileError
    AlAdquirirDatos = cdsUsuariosRolAlAdquirirDatos
    AlEnviarDatos = cdsUsuariosRolAlEnviarDatos
    AlModificar = cdsUsuariosRolAlModificar
    Left = 112
    Top = 136
  end
  object cdsUsuarioRoles: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'US_CODIGO;RO_CODIGO'
    Params = <>
    Left = 256
    Top = 176
  end
  object cdsRolUsuarios: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = GenericReconcileError
    Left = 176
    Top = 192
  end
end
