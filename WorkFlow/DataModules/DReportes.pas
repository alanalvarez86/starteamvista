unit DReportes;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ZetaClientDataSet, Db, DBClient,
     {$ifdef DOS_CAPAS}
     DServerComparte,
     {$else}
     Comparte_TLB,
     {$endif}
     ZetaTipoEntidad,
     ZetaCommonLists,
     ZetaCommonClasses,
     DBaseReportes;

type
  TdmReportes = class(TdmBaseReportes)
  private
    { Private declarations }
{$ifdef DOS_CAPAS}
    function GetServerReportesBDE: TdmServerComparte;
    property ServerReportesBDE: TdmServerComparte read GetServerReportesBDE;
{$else}
    function GetServerReportesBDE: IdmServerComparteDisp;
    property ServerReportesBDE: IdmServerComparteDisp read GetServerReportesBDE;
{$endif}
  protected
    function GeneraSQL( var oSQLAgente: OleVariant; var ParamList: OleVariant; var sError: WideString): OleVariant;override;
    function ServidorEvaluaParam( var oSQLAgente : OleVariant; oParamList: OleVariant; var sError: Widestring ):Boolean;override;
    function ServidorPruebaFormula( Parametros : TZetaParams; var sTexto: widestring; const EntidadActiva: TipoEntidad; oParams: Olevariant ): Boolean; override;
  private
  public
    { Public declarations }
    function DirectorioPlantillas: string; override;
  end;

var
  dmReportes: TdmReportes;

implementation
uses DCliente,
     DGlobal,
     ZetaCommonTools,
     ZGlobalTress,
     ZAccesosTress;
{$R *.DFM}

{$ifdef DOS_CAPAS}
function TdmReportes.GetServerReportesBDE: TdmServerComparte;
begin
     Result := DCliente.dmCliente.ServerComparte;
end;
{$else}
function TdmReportes.GetServerReportesBDE: IdmServerComparteDisp;
begin
     Result := DCliente.dmCliente.ServerComparte;
end;
{$endif}

function TdmReportes.GeneraSQL( var oSQLAgente: OleVariant; var ParamList : OleVariant; var sError : WideString ) : OleVariant;
begin
     Result := ServerReportesBDE.GeneraSQL( dmCliente.Empresa,oSQLAgente, ParamList , sError  );
end;

function TdmReportes.ServidorEvaluaParam( var oSQLAgente : OleVariant; oParamList: OleVariant; var sError: Widestring ):Boolean;
begin
     Result := ServerReportesBDE.EvaluaParam( dmCliente.Empresa, oSQLAgente, oParamList, sError );
end;

function TdmReportes.ServidorPruebaFormula(Parametros: TZetaParams; var sTexto: widestring; const EntidadActiva: TipoEntidad; oParams: Olevariant): Boolean;
begin
     Result := False;
end;

function TdmReportes.DirectorioPlantillas: string;
begin
     {$ifdef RDD}
     with dmCliente do
          Result := VerificaDir( ServerReportes.DirectorioPlantillas(Empresa) );
     {$else}
     Result := VerificaDir( Global.GetGlobalString( K_GLOBAL_DIR_PLANT ));
     {$endif}
end;

end.
