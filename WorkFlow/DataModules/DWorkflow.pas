unit DWorkflow;

interface

{$DEFINE MULTIPLES_ENTIDADES}
uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, DBClient, ComObj,
     {$ifndef VER130}Variants,{$endif}
     {$ifdef DOS_CAPAS}
     DServerComparte,
     {$else}
     Comparte_TLB,
     {$endif}
     ZetaTipoEntidad,
     ZetaCommonLists,                               
     ZetaCommonClasses,
     ZetaClientDataSet;

const
     K_TODOS_KEY = '000000';
     K_TODOS_INT = 0;
     K_TODOS_TXT = 'Todos';
     K_OFFSET = 1;
     K_PARAMETRO_PROBLEMA = 'Problema';
     K_ESPACIO = ' ';
type
  TdmWorkflow = class(TDatamodule)
    cdsProcesos: TZetaClientDataSet;
    cdsCorreos: TZetaClientDataSet;
    cdsTareas: TZetaClientDataSet;
    cdsProcesosPasos: TZetaClientDataSet;
    cdsProcesosCorreos: TZetaClientDataSet;
    cdsProcesosArchivos: TZetaClientDataSet;
    cdsProcesosMensajes: TZetaClientDataSet;
    cdsModelos: TZetaLookupDataSet;
    cdsPasosModelo: TZetaClientDataSet;
    cdsAcciones: TZetaLookupDataSet;
    cdsRolUsuarios: TZetaClientDataSet;
    cdsReasignarTareaUsuarios: TZetaClientDataSet;
    cdsEventLog: TZetaClientDataSet;
    cdsRolModelos: TZetaClientDataSet;
    cdsModeloRoles: TZetaClientDataSet;
    cdsNotificaciones: TZetaClientDataSet;
    cdsNivelRoles: TZetaLookupDataSet;
    cdsConexiones: TZetaLookupDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsAfterPost(DataSet: TDataSet);
    procedure cdsBeforePost(DataSet: TDataSet; const sCampo: String);
    {$ifdef VER130}
    procedure cdsReconcileError( DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction );
    {$else}
    procedure cdsReconcileError( DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction );
    {$endif}
    procedure MemoGetText( Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsProcesosAfterOpen(DataSet: TDataSet);
    procedure cdsProcesosAlModificar(Sender: TObject);
    procedure cdsProcesosPasosAfterOpen(DataSet: TDataSet);
    procedure cdsProcesosCorreosAfterOpen(DataSet: TDataSet);
    procedure cdsProcesosMensajesAfterOpen(DataSet: TDataSet);
    procedure cdsTareasAfterOpen(DataSet: TDataSet);
    procedure cdsTareasAlModificar(Sender: TObject);
    procedure cdsCorreosAfterOpen(DataSet: TDataSet);
    procedure cdsCorreosAlCrearCampos(Sender: TObject);
    procedure cdsCorreosAlModificar(Sender: TObject);
    procedure cdsCorreosCalcFields(DataSet: TDataSet);
    procedure cdsModelosAfterOpen(DataSet: TDataSet);
    procedure cdsModelosAlAdquirirDatos(Sender: TObject);
    procedure cdsModelosAfterScroll(DataSet: TDataSet);
    procedure cdsModelosAfterCancel(DataSet: TDataSet);
    procedure cdsModelosAlEnviarDatos(Sender: TObject);
    procedure cdsModelosAlModificar(Sender: TObject);
    procedure cdsModelosBeforePost(DataSet: TDataSet);
    procedure cdsModelosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsModelosNewRecord(DataSet: TDataSet);
    procedure cdsPasosModeloAfterOpen(DataSet: TDataSet);
    procedure cdsPasosModeloAlAdquirirDatos(Sender: TObject);
    procedure cdsPasosModeloAlEnviarDatos(Sender: TObject);
    procedure cdsPasosModeloAlModificar(Sender: TObject);
    procedure cdsPasosModeloBeforePost(DataSet: TDataSet);
    procedure cdsPasosModeloNewRecord(DataSet: TDataSet);
    procedure cdsAccionesAfterOpen(DataSet: TDataSet);
    procedure cdsAccionesAlAdquirirDatos(Sender: TObject);
    procedure cdsAccionesAlEnviarDatos(Sender: TObject);
    procedure cdsAccionesAlModificar(Sender: TObject);
    procedure cdsAccionesGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsNivelRolesAlAdquirirDatos(Sender: TObject);
    procedure cdsDatasetAfterDelete(DataSet: TDataSet);
    procedure cdsEventLogAlCrearCampos(Sender: TObject);
    procedure cdsProcesosPasosAlCrearCampos(Sender: TObject);
    procedure cdsProcesosCorreosAlCrearCampos(Sender: TObject);
    procedure cdsNotificacionesAlModificar(Sender: TObject);
    procedure cdsNotificacionesNewRecord(DataSet: TDataSet);
    procedure cdsNotificacionesAlAdquirirDatos(Sender: TObject);
    procedure cdsNotificacionesAlBorrar(Sender: TObject);
    procedure cdsNotificacionesAfterCambios(DataSet: TDataSet);
    procedure cdsNotificacionesAlCrearCampos(Sender: TObject);
    procedure cdsNotificacionesAfterOpen(DataSet: TDataSet);
    procedure cdsNotificacionesCalcFields(DataSet: TDataSet);
    procedure cdsPasosModeloAlBorrar(Sender: TObject);
    procedure cdsConexionesAlAdquirirDatos(Sender: TObject);
    procedure cdsConexionesNewRecord(DataSet: TDataSet);
    procedure cdsConexionesAlModificar(Sender: TObject);
    procedure cdsConexionesAlEnviarDatos(Sender: TObject);
    procedure cdsConexionesAfterOpen(DataSet: TDataSet);
    procedure cdsNivelRolesGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
  private
    { Private declarations }
    FModelo: String;
    FOrdenMax: Integer;
    FEditing: Boolean;
    {$ifdef DOS_CAPAS}
    function GetServerComparte: TdmServerComparte;
    {$else}
    function GetServerComparte: IdmServerComparteDisp;
    {$endif}
    function GetFolioProcesoActivo: Integer;
    function GetModeloActivo: String;
    function EventTypeToStr( const aEventType: Word ): String;
    procedure DerechoMover(const iDelta: Integer);
    procedure ModeloCargarParametros( Parametros: TZetaParams );
    procedure ModeloEditar;
    procedure ModificarResponsable(Datos: TZetaClientDataset; const eTipo: eWFNotificacion; const sCampoResponsable: String);
    procedure ProcesoCargarParametros( Parametros: TZetaParams );
    procedure TareaCargarParametros( Parametros: TZetaParams );
    //Function GetNotificaciones : OleVariant;
    //Function GetPasosModelo : OleVariant;
    function ObtenCampoTiempo( Tiempo : Integer ) : String;
    {Event viewer}
    procedure TipoGetText( Sender: TField; var Text: String; DisplayText: Boolean );                                                               
    function ObtenConexionDefault: integer;{OP: 31.Diciembre.2008}
  protected
    { Protected declarations }
    {$ifdef DOS_CAPAS}
    property ServerComparte: TdmServerComparte read GetServerComparte;
    {$else}
    property ServerComparte: IdmServerComparteDisp read GetServerComparte;
    {$endif}
    function IsOk( const iValue: Integer ): Boolean;
    procedure ModeloRolesGet( const sModelo: String );

  public
    { Public declarations }
    property Modelo: String read FModelo write FModelo;
    function ModeloBuscar( const sFiltro: String; var sCodigo, sNombre: String ): Boolean;
    function ModeloSuspender( Parametros: TZetaParams ): Boolean;
    function ModeloReiniciar( Parametros: TZetaParams ): Boolean;
    function ModeloCancelar( Parametros: TZetaParams ): Boolean;
    function ModeloReactivar( Parametros: TZetaParams ): Boolean;
    function ProcesoCancelar( Parametros: TZetaParams ): Boolean;
    function ProcesoReiniciar( Parametros: TZetaParams ): Boolean;
    function ProcesoSuspender( Parametros: TZetaParams ): Boolean;
    function TareaReasignar( Parametros: TZetaParams ): Boolean;
    function SeleccionaTarea( Parametros: TZetaParams ): Boolean;
    function CopiarModelo( Parametros: TZetaParams ): Boolean;
    procedure DerechoBajar;
    procedure DerechoSubir;
    procedure GetDatosProcesoActivo;
    procedure GetListaCorreos( Parametros: TZetaParams );
    procedure GetListaProcesos( Parametros: TZetaParams );
    procedure GetListaTareas( Parametros: TZetaParams );
    procedure ModeloLlenarLista( Lista: TStrings; const lTodos: Boolean = False );
    procedure ModeloRolAgregar( const sRol, sNombre: String );
    procedure ModeloRolAgregarTodos;
    procedure ModeloRolBorrar;
    procedure ModeloRolBorrarTodos;
    procedure ModeloModificarResponsable(const eTipo: eWFNotificacion; const sCampoResponsable: String);
     {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: Array of TipoEntidad; const Estado: TipoEstado);
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
    {$endif}
    procedure PasoModeloModificarResponsable(const eTipo: eWFNotificacion; const sCampoResponsable: String);
    procedure RolModeloAgregar( const sModelo, sNombre: String );
    procedure RolModeloAgregarTodos;
    procedure RolModeloBorrar;
    procedure RolModeloBorrarTodos;
    procedure GetReasignarTareaUsuarios( Parametros: TZetaParams );
    procedure ServicesStatus( Parametros: TZetaParams );
    procedure GetWindowsEventLog( Parametros: TZetaParams );
    procedure NotificacionesModificarResponsable( const eTipo: eWFNotificacion; const sCampoResponsable: String );
    procedure RolUsuarioModeloGet( const sRol: String );
  end;

var
  dmWorkflow: TdmWorkflow;

implementation

uses DSistema,
     DCliente,
     ZBaseEdicion,
     ZReconcile,
     ZetaDialogo,
     ZetaCommonTools,
     ZAccesosMgr,
     ZAccesosTress,
     FEditCatCondiciones,
     FCatAccionesEdit,
     FCatRolesEdit,
     FCatModelosEdit,
     FCatPasosModeloEdit,
     FHistTareasEdit,
     FHistProcesosEdit,
     FHistCorreosEdit,
     FReasignarTarea,
     FCatNotificacionesEdit,
     FCatConexionesEdit,
     ZetaMsgDlg,
     FAutoClasses;

{$R *.DFM}

{ ***** TdmCliente ***** }

procedure TdmWorkflow.DataModuleCreate(Sender: TObject);
begin
     FOrdenMax := 0;
     FEditing := False;
     cdsModelos.OnReconcileError := cdsReconcileError;
     cdsPasosModelo.OnReconcileError := cdsReconcileError;
     cdsAcciones.OnReconcileError := cdsReconcileError;
     dmSistema.cdsRoles.OnReconcileError := cdsReconcileError;
end;

procedure TdmWorkflow.DataModuleDestroy(Sender: TObject);
begin
     FOrdenMax := 0;
     inherited;
end;

{$ifdef DOS_CAPAS}
function TdmWorkflow.GetServerComparte: TdmServerComparte;
begin
     Result := dmCliente.ServerComparte;
end;
{$else}
function TdmWorkflow.GetServerComparte: IdmServerComparteDisp;
begin
     Result := dmCliente.ServerComparte;
end;
{$endif}

{$ifdef MULTIPLES_ENTIDADES}
procedure TdmWorkflow.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
{$else}
procedure TdmWorkflow.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
{$endif}
begin
     {
     if ( enFolio in Entidades ) then
        cdsFolios.SetDataChange;
     }
end;

procedure TdmWorkflow.MemoGetText( Sender: TField; var Text: String; DisplayText: Boolean);
begin
     { Se requiere condicionar para que al Hacer un }
     { cdsNomParam.Cancel no ocurra un Invalid BLOB Handle }
     if Assigned( Sender ) and Assigned( Sender.Dataset ) then
     begin
          if Sender.Dataset.State in [ dsBrowse ] then
             Text := Sender.AsString;
     end;
end;

function TdmWorkflow.IsOk( const iValue: Integer ): Boolean;
begin
     Result := ( iValue = 0 );
end;

procedure TdmWorkflow.cdsBeforePost( DataSet: TDataSet; const sCampo: String );
begin
     with Dataset do
     begin
          if ZetaCommonTools.StrVacio( FieldByName( sCampo ).AsString ) then
             DataBaseError( 'EL C�digo No Puede Quedar Vac�o' );
     end;
end;

procedure TdmWorkflow.cdsAfterPost(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with TClientDataset( Dataset ) do
     begin
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerComparte.GrabaTabla( Tag, Delta, ErrorCount ) );
          end;
     end;
end;

{$ifdef VER130}
procedure TdmWorkflow.cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
{$else}
procedure TdmWorkflow.cdsReconcileError( DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction );
{$endif}
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;

procedure TdmWorkflow.ModificarResponsable( Datos: TZetaClientDataset; const eTipo: eWFNotificacion; const sCampoResponsable: String );
var
   sCodigo, sNombre: String;
   iUsuario: Integer;
begin
     with Datos do
     begin
          case eTipo of
               nvRol:
               begin
                    with dmSistema do
                    begin
                          cdsRoles.Conectar;
                          if cdsRoles.Search( '', sCodigo, sNombre ) then
                          begin
                               if not ( State in [ dsEdit, dsInsert ] ) then
                                  Edit;
                               FieldByName( sCampoResponsable ).AsString := sCodigo;
                          end;
                    end;
               end;
               nvUsuarioEspecifico:
               begin
                    if dmSistema.BuscarUsuario( '', iUsuario, sNombre ) then
                    begin
                         if not ( State in [ dsEdit, dsInsert ] ) then
                            Edit;
                         FieldByName( sCampoResponsable ).AsString := IntToStr( iUsuario );
                    end;
               end;
               nvNivel:
               begin
                    cdsNivelRoles.Refrescar;
                    if cdsNivelRoles.Search( '', sCodigo, sNombre ) then
                    begin
                         if not ( State in [ dsEdit, dsInsert ] ) then
                            Edit;
                         FieldByName( sCampoResponsable ).AsString := sCodigo;
                    end;
               end;
          end;
     end;
end;

procedure TdmWorkFlow.ServicesStatus( Parametros: TZetaParams );
begin
     Parametros.VarValues := ServerComparte.ServiciosStatus( Parametros.VarValues );
end;

procedure TdmWorkflow.cdsDatasetAfterDelete(DataSet: TDataSet);
begin
     TZetaClientDataSet( DataSet ).Enviar;
end;

{ **** cdsProcesos **** }

function TdmWorkflow.GetFolioProcesoActivo: Integer;
begin
     Result := cdsProcesos.FieldByName( 'WP_FOLIO' ).AsInteger;
end;

procedure TdmWorkflow.GetDatosProcesoActivo;
var
   Proceso, Pasos, Correos, Archivos, Mensajes: OleVariant;
begin
     Proceso := ServerComparte.GetDatosProceso( GetFolioProcesoActivo, Pasos, Correos, Archivos, Mensajes );
     cdsProcesosPasos.Data := Pasos;
     cdsProcesosCorreos.Data := Correos;
     cdsProcesosArchivos.Data := Archivos;
     cdsProcesosMensajes.Data := Mensajes;
end;

procedure TdmWorkflow.GetListaProcesos( Parametros: TZetaParams );
begin
     cdsProcesos.Data := ServerComparte.GetProcesos( Parametros.VarValues );
end;

procedure TdmWorkflow.cdsProcesosAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsProcesos do
     begin
          MaskFecha( 'WP_FEC_INI' );
          MaskFecha( 'WP_FEC_FIN' );
     end;
end;

procedure TdmWorkflow.cdsProcesosAlModificar(Sender: TObject);
begin
     inherited;
     ZBaseEdicion.ShowFormaEdicion( HistProcesosEdit, THistProcesosEdit );
end;

procedure TdmWorkflow.ProcesoCargarParametros( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddInteger( 'Proceso', GetFolioProcesoActivo );
          AddInteger( 'Usuario', dmCliente.Usuario );
          AddInteger( 'StatusActualProceso', cdsProcesos.FieldByName( 'WP_STATUS' ).AsInteger );
          AddString( 'Notas', VACIO );
     end;
end;

function TdmWorkflow.ProcesoCancelar( Parametros: TZetaParams ): Boolean;
var
   sMsg: WideString;
begin
     ProcesoCargarParametros( Parametros );
     Result := IsOk( ServerComparte.ProcesoCancelar( Parametros.VarValues, sMsg ) );
     if not Result then
     begin
          Parametros.AddString( K_PARAMETRO_PROBLEMA, sMsg );
     end;
end;

function TdmWorkflow.ProcesoReiniciar( Parametros: TZetaParams ): Boolean;
var
   sMsg: WideString;
begin
     ProcesoCargarParametros( Parametros );
     Result := IsOk( ServerComparte.ProcesoReiniciar( Parametros.VarValues, sMsg ) );
     if not Result then
     begin
          Parametros.AddString( K_PARAMETRO_PROBLEMA, sMsg );
     end;
end;

procedure TdmWorkflow.TareaCargarParametros( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddString( 'GUID', cdsTareas.FieldByName('WT_GUID').AsString );
          AddInteger( 'Usuario', dmCliente.Usuario );
     end;
end;

function TdmWorkflow.TareaReasignar( Parametros: TZetaParams ): Boolean;
var
   sMsg: WideString;
begin
     TareaCargarParametros( Parametros );
     Result := IsOk( ServerComparte.TareaReasignar( Parametros.VarValues, sMsg ) );
     if not Result then
     begin
          Parametros.AddString( K_PARAMETRO_PROBLEMA, sMsg );
     end;
end;

function TdmWorkFlow.SeleccionaTarea( Parametros: TZetaParams ): Boolean;
begin
     if ReasignarTarea = NIL then
        ReasignarTarea := TReasignarTarea.Create( Application );

     TareaCargarParametros( Parametros );
     ReasignarTarea.Parametros := Parametros;
     with ReasignarTarea do
     begin
          ShowModal;
          Result := ModalResult = mrOK;
     end;
end;

procedure TdmWorkflow.GetReasignarTareaUsuarios( Parametros: TZetaParams );
begin
     cdsReasignarTareaUsuarios.Data := ServerComparte.GetReasignarTareaUsuarios( Parametros.VarValues );
end;


function TdmWorkflow.ProcesoSuspender( Parametros: TZetaParams ): Boolean;
var
   sMsg: WideString;
begin
     ProcesoCargarParametros( Parametros );
     Result := IsOk( ServerComparte.ProcesoSuspender( Parametros.VarValues, sMsg ) );
     if not Result then
     begin
          Parametros.AddString( K_PARAMETRO_PROBLEMA, sMsg );
     end;
end;

{ *** cdsProcesosCorreos *** }

procedure TdmWorkflow.cdsProcesosCorreosAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsProcesosCorreos do
     begin
          MaskFecha( 'WO_FEC_IN' );
          MaskFecha( 'WO_FEC_OUT' );
          MaskBool( 'WO_ENVIADO' );
     end;
end;

{ *** cdsProcesosPasos *** }

procedure TdmWorkflow.cdsProcesosPasosAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsProcesosPasos do
     begin
          MaskFecha( 'WS_FEC_INI' );
          MaskFecha( 'WS_FEC_FIN' );
     end;
end;

{ *** cdsProcesosMensajes *** }

procedure TdmWorkflow.cdsProcesosMensajesAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsProcesosMensajes do
     begin
          MaskFecha( 'WN_FECHA' );
          MaskBool( 'WN_LEIDO' );
     end;
end;

{ **** cdsTareas **** }

procedure TdmWorkflow.GetListaTareas( Parametros: TZetaParams );
begin
     cdsTareas.Data := ServerComparte.GetTareas( Parametros.VarValues );
end;

procedure TdmWorkflow.cdsTareasAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsTareas do
     begin
          MaskFecha( 'WT_FEC_INI' );
          MaskFecha( 'WT_FEC_FIN' );
     end;
end;

procedure TdmWorkflow.cdsTareasAlModificar(Sender: TObject);
begin
     inherited;
     ZBaseEdicion.ShowFormaEdicion( HistTareasEdit, THistTareasEdit );
end;

{ **** cdsCorreos **** }

procedure TdmWorkflow.GetListaCorreos( Parametros: TZetaParams );
begin
     cdsCorreos.Data := ServerComparte.GetCorreos( Parametros.VarValues );
end;

procedure TdmWorkflow.cdsCorreosAlCrearCampos(Sender: TObject);
begin
     inherited;
     with cdsCorreos do
     begin
          CreateCalculated( 'WO_STA_TXT', ftString, 15 );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookup, 'REMITENTE', 'US_ENVIA' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookup, 'DESTINO', 'US_RECIBE' );
     end;
end;

procedure TdmWorkflow.cdsCorreosAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsCorreos do
     begin
          MaskFecha( 'WO_FEC_IN' );
          MaskFecha( 'WO_FEC_OUT' );
          MaskBool( 'WO_ENVIADO' );
          ListaFija( 'WO_SUBTYPE', lfEmailType );
          FieldByName( 'WO_TO' ).OnGetText := MemoGetText;
     end;
end;

procedure TdmWorkflow.cdsCorreosCalcFields(DataSet: TDataSet);
begin
     inherited;
     with cdsCorreos do
     begin
          FieldByName( 'WO_STA_TXT' ).AsString := ZetaCommonLists.ObtieneElemento( lfEVCorreoStatus, FieldByName( 'WO_STATUS' ).AsInteger );
     end;
end;

procedure TdmWorkflow.cdsCorreosAlModificar(Sender: TObject);
begin
     inherited;
     ZBaseEdicion.ShowFormaEdicion( HistCorreosEdit, THistCorreosEdit );
end;

{ *** cdsModelos *** }

function TdmWorkflow.GetModeloActivo: String;
begin
     Result := cdsModelos.FieldByName( 'WM_CODIGO' ).AsString;
end;

procedure TdmWorkflow.ModeloEditar;
begin
     with cdsModelos do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
     end;
end;

procedure TdmWorkflow.cdsModelosAfterOpen(DataSet: TDataSet);
begin
     with cdsModelos do
     begin
          ListaFija( 'WM_STATUS', lfWFStatusModelo );
          FieldByName( 'WM_DESCRIP' ).OnGetText := MemoGetText;
     end;
end;

procedure TdmWorkflow.cdsModelosAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsModelos.Data := ServerComparte.GetModelos;
     end;
end;

procedure TdmWorkflow.cdsModelosAfterScroll(DataSet: TDataSet);
begin
     if FEditing then
        ModeloRolesGet( GetModeloActivo );
end;

procedure TdmWorkflow.cdsModelosAfterCancel(DataSet: TDataSet);
begin
     ModeloRolesGet( GetModeloActivo );
end;

procedure TdmWorkflow.cdsModelosAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   Modelo, Roles: OleVariant;
begin
     ErrorCount := 0;
     with cdsModeloRoles do
     begin
          PostData;
          MergeChangeLog;
          Roles := Data;
     end;
     with cdsModelos do
     begin
          PostData;
          Modelo := DeltaNull;
          if not VarIsNull( Modelo ) or not VarIsNull( Roles ) then
          begin
               Reconcile( dmCliente.ServerComparte.GrabaModelos( GetModeloActivo, Modelo, Roles, ErrorCount ) );
          end;
     end;
end;

procedure TdmWorkflow.cdsModelosAlModificar(Sender: TObject);
begin
     FEditing := True;
     try
        ModeloRolesGet( GetModeloActivo );
        ZBaseEdicion.ShowFormaEdicion( CatModelosEdit, TCatModelosEdit );
     finally
            FEditing := False;
     end;
end;

procedure TdmWorkflow.cdsModelosBeforePost(DataSet: TDataSet);
var
   iValor: integer;
begin
     with cdsModelos do
     begin
          if ZetaCommonTools.StrVacio( FieldByName( 'WM_CODIGO' ).AsString ) then
             DatabaseError( '�C�digo del modelo no puede quedar vac�o!' )
          else if TryStrToInt( Copy( FieldByName( 'WM_CODIGO' ).AsString, 1, 1 ), iValor ) then
                  DatabaseError( Format( '�C�digo del modelo no puede iniciar con un valor num�rico [%d]!', [ iValor ] ) )
               else if ZetaCommonTools.StrVacio( FieldByName( 'WM_NOMBRE' ).AsString ) then
                       DatabaseError( '� Nombre del modelo no puede quedar vac�o !' );
          if State in [ dsEdit ] then
          begin
               if StrVacio( FieldByName( 'WM_DESCRIP' ).AsString )then
                  FieldByName( 'WM_DESCRIP' ).AsString := K_ESPACIO;
          end;
     end;
end;

procedure TdmWorkflow.cdsModelosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_MODELOS, iRight );
end;

procedure TdmWorkflow.cdsModelosNewRecord(DataSet: TDataSet);

procedure InitModelo;
begin
     with cdsModelos do
     begin
          FieldByName( 'WM_STATUS' ).AsInteger := Ord( smwActivo );
          FieldByName( 'WM_TIPNOT1' ).AsInteger := Ord( nvNinguno );
          FieldByName( 'WM_TIPNOT2' ).AsInteger := Ord( nvNinguno );
          FieldByName( 'WM_TIPNOT3' ).AsInteger := Ord( nvNinguno );
          FieldByName( 'WM_URL' ).AsString := 'Solicitar.aspx';
          FieldByName( 'WM_URL_OK' ).AsString := 'Aprobar.aspx';
          FieldByName( 'WM_URL_EXA' ).AsString := 'Examinar.aspx';
     end;
end;

begin
     with dmCliente do
     begin
          if (cdsModelos.RecordCount >= 5 ) then
          begin
                if(EsModoDemo) then
                     Abort
                else if ( not ModuloAutorizado(okWorkflow)) then
                begin
                     ZInformation( 'WorkFlow CFG', 'El M�dulo de WorkFlow en Sistema TRESS no est� Autorizado, consulte a su Distribuidor',  0 );
                     Abort;
                end
                else
                    InitModelo;
          end
          else
          begin
               InitModelo;
          end;
     end;
end;

procedure TdmWorkflow.ModeloLlenarLista( Lista: TStrings; const lTodos: Boolean = False );
begin
     with Lista do
     begin
          BeginUpdate;
          try
             Clear;
             if lTodos then
             begin
                  Add( Format( '%s=%s', [ K_TODOS_KEY, K_TODOS_TXT ] ) );
             end;
             with cdsModelos do
             begin
                  DisableControls;
                  try
                     if not Active then
                     begin
                          Conectar;
                          First;
                     end;
                     Self.Modelo := FieldByName( 'WM_CODIGO' ).AsString;
                     First;
                     while not EOF do
                     begin
                          Add( FieldByName( 'WM_CODIGO').AsString + '=' + FieldByName( 'WM_NOMBRE' ).AsString );
                          Next;
                     end;
                     Locate( 'WM_CODIGO', Self.Modelo, [] );
                  finally
                         EnableControls;
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TdmWorkflow.ModeloModificarResponsable( const eTipo: eWFNotificacion; const sCampoResponsable: String );
begin
     ModificarResponsable( cdsModelos, eTipo, sCampoResponsable );
end;

procedure TdmWorkflow.ModeloCargarParametros( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddString( 'Modelo', GetModeloActivo );
          AddInteger( 'Usuario', dmCliente.Usuario );
          AddInteger( 'StatusActualModelo', cdsModelos.FieldByName( 'WM_STATUS' ).AsInteger );
          AddString('Notas', VACIO );
     end;
end;

function TdmWorkflow.ModeloCancelar(Parametros: TZetaParams): Boolean;
var
   sMsg: WideString;
begin
     ModeloCargarParametros( Parametros );
     with dmCliente do
     begin
          Result := IsOk( ServerComparte.ModeloCancelar( Parametros.VarValues, sMsg ) );
     end;
     if not Result then
     begin
          Parametros.AddString( K_PARAMETRO_PROBLEMA, sMsg );
     end;
end;

function TdmWorkflow.ModeloReactivar(Parametros: TZetaParams): Boolean;
var
   sMsg: WideString;
begin
     ModeloCargarParametros( Parametros );
     with dmCliente do
     begin
          Result := IsOk( ServerComparte.ModeloReactivar( Parametros.VarValues, sMsg ) );
     end;
     if not Result then
     begin
          Parametros.AddString( K_PARAMETRO_PROBLEMA, sMsg );
     end;
end;

function TdmWorkflow.ModeloSuspender(Parametros: TZetaParams): Boolean;
var
   sMsg: WideString;
begin
     ModeloCargarParametros( Parametros );
     with dmCliente do
     begin
          Result := IsOk( ServerComparte.ModeloSuspender( Parametros.VarValues, sMsg ) );
     end;
     if not Result then
     begin
          Parametros.AddString( K_PARAMETRO_PROBLEMA, sMsg );
     end;
end;

function TdmWorkflow.ModeloReiniciar(Parametros: TZetaParams): Boolean;
var
   sMsg: WideString;
begin
     ModeloCargarParametros( Parametros );
     with dmCliente do
     begin
          Result := IsOk( ServerComparte.ModeloReiniciar( Parametros.VarValues, sMsg ) );
     end;
     if not Result then
     begin
          Parametros.AddString( K_PARAMETRO_PROBLEMA, sMsg );
     end;
end;

function TdmWorkflow.ModeloBuscar( const sFiltro: String; var sCodigo, sNombre: String ): Boolean;
begin
     with cdsModelos do
     begin
          Conectar;
          Result := Search( sFiltro, sCodigo, sNombre );
     end;
     if Result then
     begin
          Result := ZetaCommonTools.StrLleno( sCodigo );
     end;
end;

{ *** Pasos de Modelo *** }

procedure TdmWorkflow.cdsPasosModeloAfterOpen(DataSet: TDataSet);
begin
     with cdsPasosModelo do
     begin
          ListaFija( 'WE_TIPDEST', lfWFNotificacion );
          FieldByName( 'WE_DESCRIP' ).OnGetText := MemoGetText;
     end;
end;

procedure TdmWorkflow.cdsPasosModeloAlAdquirirDatos(Sender: TObject);
begin
     cdsPasosModelo.Data := dmCliente.ServerComparte.GetPasosModelo( Self.Modelo, FOrdenMax );
end;

procedure TdmWorkflow.cdsPasosModeloAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   NotificacionesResult: OleVariant;
begin
     ErrorCount := 0;
     with cdsPasosModelo do
     begin
          cdsNotificaciones.PostData;
          PostData;
          if ( ( ChangeCount > 0 ) or ( cdsNotificaciones.ChangeCount > 0 )  ) then
          begin
               Reconcile( dmCliente.ServerComparte.GrabaPasosModelo( Self.Modelo, DeltaNull, cdsNotificaciones.DeltaNull, FOrdenMax, ErrorCount, NotificacionesResult ) );
               if  (cdsNotificaciones.ChangeCount > 0) then
                 cdsNotificaciones.Reconcile(NotificacionesResult);
          end;
     end;
end;

{Function TdmWorkflow.GetNotificaciones : OleVariant;
begin
     Result := null;
     with cdsNotificaciones do
     begin
          if (Active and ( ChangeCount > 0 ) ) then
          begin
               Result := Delta;
          end;
     end;
end;}

{Function TdmWorkflow.GetPasosModelo : OleVariant;
begin
     Result := null;
     with cdsPasosModelo do
     begin
          if ( ChangeCount > 0  ) then
          begin
               Result := Delta;
          end;
     end;
end;}

procedure TdmWorkflow.cdsPasosModeloAlModificar(Sender: TObject);
begin
     ZBaseEdicion.ShowFormaEdicion( CatPasosModeloEdit, TCatPasosModeloEdit );
end;

procedure TdmWorkflow.cdsPasosModeloBeforePost(DataSet: TDataSet);
begin
     with cdsPasosModelo do
     begin
          if ZetaCommonTools.StrVacio( FieldByName( 'WA_CODIGO' ).AsString ) then
          begin
               DatabaseError( 'Se Debe Especificar Una Acci�n Para Este Paso' );
          end;
          if ( ( FieldByName( 'WE_TIPDEST' ).AsInteger = Ord( nvNinguno ) ) and ( FieldByName( 'WA_CODIGO' ).AsString <> 'GRABA3' ) ) then
          begin
               DatabaseError( 'Se Debe Especificar Un Responsable Para Este Paso' );
          end;
     end;
end;

{OP: 31.Diciembre.2008}
{Esta funci�n regresa el primer registro de conexiones que encuentre,
en el caso de no encontrar, crea uno nuevo, y regresa el c�digo}
function TdmWorkflow.ObtenConexionDefault: integer;
begin
     with cdsConexiones do
     begin
          Conectar;
          if IsEmpty then
          begin
               Append;
               FieldByName( 'WX_NOMBRE' ).AsString := 'Conexi�n default';
               Post;
               try
                  Enviar;
               except
                     on Error: Exception do
                     begin
                          //Result := 0;
                     end;
               end;
          end;
          Refrescar;
          First;
          if not EOF then
             Result := FieldByName( 'WX_FOLIO' ).AsInteger
          else
              Result := 0;
     end;
end;

procedure TdmWorkflow.cdsPasosModeloNewRecord(DataSet: TDataSet);
begin
     with cdsPasosModelo do
     begin
          FieldByName( 'WM_CODIGO' ).AsString := Self.Modelo;
          FieldByName( 'WE_ORDEN' ).AsInteger := FOrdenMax + 1;
          FieldByName( 'WE_TIPDEST' ).AsInteger := Ord( nvJefe );
          FieldByName( 'WE_TIPNOT1' ).AsInteger := Ord( nvNinguno );
          FieldByName( 'WE_TIPNOT2' ).AsInteger := Ord( nvNinguno );
          FieldByName( 'WE_TIPNOT3' ).AsInteger := Ord( nvNinguno );
          {OP: 31.Diciembre.2008}
          FieldByName( 'WX_FOLIO' ).AsInteger := ObtenConexionDefault;
     end;
end;

procedure TdmWorkflow.DerechoMover( const iDelta: Integer );
var
   sModelo: String;
   iPaso: Integer;
   iNuevoPaso: Integer;
begin
     with cdsPasosModelo do
     begin
          sModelo := FieldByName( 'WM_CODIGO' ).AsString;
          iPaso := FieldByName( 'WE_ORDEN' ).AsInteger;
          iNuevoPaso := iPaso + iDelta;
          dmCliente.ServerComparte.PasoModeloCambiar( sModelo, iPaso, ( iNuevoPaso ) );
          Refrescar;
          if ( ( iNuevoPaso > cdsPasosModelo.RecordCount )  or ( iNuevoPaso < 1 ) ) then
             iNuevoPaso := iPaso;
          Locate( 'WM_CODIGO;WE_ORDEN', VarArrayOf( [ sModelo, ( iNuevoPaso ) ] ), [] );
     end;
end;

procedure TdmWorkflow.DerechoSubir;
begin
     DerechoMover( -1 );
end;

procedure TdmWorkflow.DerechoBajar;
begin
     DerechoMover( 1 );
end;

procedure TdmWorkflow.PasoModeloModificarResponsable( const eTipo: eWFNotificacion; const sCampoResponsable: String );
begin
     ModificarResponsable( cdsPasosModelo, eTipo, sCampoResponsable );
end;


procedure TdmWorkflow.cdsPasosModeloAlBorrar(Sender: TObject);
var
iProcesosActivos: Integer;
sModelo: String;
begin
     with cdsPasosModelo do
     begin
          sModelo := FieldByName('WM_CODIGO').AsString;
          iProcesosActivos := ServerComparte.GetNumeroProcesos(sModelo);
          if (iProcesosActivos > 0 ) then
          begin
               if ZetaDialogo.ZConfirm( 'Borrar paso modelo', Format('El Modelo %s tiene %d procesos, que se pudieran ver afectados si se borra el paso.' + CR_LF + '�Desea borrar el paso?', [sModelo, iProcesosActivos]), 0, mbNo ) then
               begin
                     if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Este Registro ?' ) then
                     begin
                          Delete;
                          Refrescar;
                     end;
               end;
          end
          else
          begin
              if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Este Registro ?' ) then
              begin
                   Delete;
                   Refrescar;
              end;
          end;
     end;

end;



{ *** cdsAcciones *** }

procedure TdmWorkflow.cdsAccionesAfterOpen(DataSet: TDataSet);
begin
     with cdsAcciones do
     begin
          FieldByName( 'WA_DESCRIP' ).OnGetText := MemoGetText;
     end;
end;

procedure TdmWorkflow.cdsAccionesAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsAcciones.Data := ServerComparte.GetAcciones;
     end;
end;

procedure TdmWorkflow.cdsAccionesAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsAcciones do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( dmCliente.ServerComparte.GrabaAcciones( Delta, ErrorCount ) );
          end;
     end;
end;

procedure TdmWorkflow.cdsAccionesAlModificar(Sender: TObject);
begin
     ZBaseEdicion.ShowFormaEdicion( CatAccionesEdit, TCatAccionesEdit );
end;

procedure TdmWorkflow.cdsAccionesGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := False;
end;

{ *** cdsUserRol *** }

procedure TdmWorkflow.RolUsuarioModeloGet( const sRol: String );
var
   Modelos: OleVariant;
begin
     cdsRolUsuarios.Data := dmCliente.ServerComparte.GetUsuariosRol( sRol, Modelos );
     cdsRolModelos.Data := Modelos;
end;


{ *** cdsRolModelos *** }

procedure TdmWorkflow.RolModeloAgregar( const sModelo, sNombre: String );
var
   sRol: String;
begin
     sRol := dmSistema.RolGetActivo;
     if ZetaCommonTools.StrVacio( sRol ) then
        ZetaDialogo.zError( '� C�digo De Rol Inexistente !', 'Antes De Agregar Modelos Es Necesario Especificar El C�digo Del Rol', 0 )
     else
     begin
          with cdsRolModelos do
          begin
               if not Locate( 'RO_CODIGO;WM_CODIGO', VarArrayOf( [ sRol, sModelo ] ), [] ) then
               begin
                    Append;
                    FieldByName( 'RO_CODIGO' ).AsString := sRol;
                    FieldByName( 'WM_CODIGO' ).AsString := sModelo;
                    FieldByName( 'WM_NOMBRE' ).AsString := sNombre;
                    Post;
               end;
          end;
          dmSistema.RolEditar;
     end;
end;

procedure TdmWorkflow.RolModeloAgregarTodos;
begin
     with cdsRolModelos do
     begin
          DisableControls;
          try
             with cdsModelos do
             begin
                  Conectar;
                  DisableControls;
                  try
                     First;
                     while not Eof do
                     begin
                          RolModeloAgregar( FieldByName( 'WM_CODIGO' ).AsString, FieldByName( 'WM_NOMBRE' ).AsString );
                          Next;
                     end;
                  finally
                         EnableControls;
                  end;
             end;
             First;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TdmWorkflow.RolModeloBorrar;
begin
     with cdsRolModelos do
     begin
          Delete;
     end;
     dmSistema.RolEditar;
end;

procedure TdmWorkflow.RolModeloBorrarTodos;
begin
     with cdsRolModelos do
     begin
          EmptyDataset;
     end;
     dmSistema.RolEditar;
end;

{ *** cdsModeloRoles *** }

procedure TdmWorkflow.ModeloRolesGet( const sModelo: String );
begin
     cdsModeloRoles.Data := dmCliente.ServerComparte.GetModeloRoles( sModelo );
end;

procedure TdmWorkflow.ModeloRolAgregar( const sRol, sNombre: String );
var
   sModelo: String;
begin
     sModelo := GetModeloActivo;
     if ZetaCommonTools.StrVacio( sModelo ) then
        ZetaDialogo.zError( '� C�digo De Modelo Inexistente !', 'Antes De Agregar Roles Es Necesario Especificar El C�digo Del Modelo', 0 )
     else
     begin
          with cdsModeloRoles do
          begin
               if not Locate( 'WM_CODIGO;RO_CODIGO', VarArrayOf( [ sModelo, sRol ] ), [] ) then
               begin
                    Append;
                    FieldByName( 'WM_CODIGO' ).AsString := sModelo;
                    FieldByName( 'RO_CODIGO' ).AsString := sRol;
                    FieldByName( 'RO_NOMBRE' ).AsString := sNombre;
                    Post;
               end;
          end;
          ModeloEditar;
     end;
end;

procedure TdmWorkflow.ModeloRolAgregarTodos;
begin
     with cdsModeloRoles do
     begin
          DisableControls;
          try
             with dmSistema.cdsRoles do
             begin
                  Conectar;
                  DisableControls;
                  try
                     First;
                     while not Eof do
                     begin
                          ModeloRolAgregar( FieldByName( 'RO_CODIGO' ).AsString, FieldByName( 'RO_NOMBRE' ).AsString );
                          Next;
                     end;
                  finally
                         EnableControls;
                  end;
             end;
             First;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TdmWorkflow.ModeloRolBorrar;
begin
     with cdsModeloRoles do
     begin
          Delete;
     end;
     ModeloEditar;
end;

procedure TdmWorkflow.ModeloRolBorrarTodos;
begin
     with cdsModeloRoles do
     begin
          EmptyDataset;
     end;
     ModeloEditar;
end;

{ *** cdsEventLog *** }

procedure TdmWorkFlow.GetWindowsEventLog( Parametros: TZetaParams );
begin
     cdsEventLog.Data := ServerComparte.GetWindowsEventLog( Parametros.VarValues );
end;

procedure TdmWorkflow.cdsEventLogAlCrearCampos(Sender: TObject);
begin
     with cdsEventLog do
     begin
          MaskFecha('EL_FECHA');
          MaskTime('EL_HORA');
          FieldByName('EL_TIPO').Alignment := taLeftJustify;
          FieldByName('EL_TIPO').OnGetText := TipoGetText;
     end;
end;

function TdmWorkFlow.EventTypeToStr( const aEventType: Word ): String;
begin
     case aEventType of
          EVENTLOG_SUCCESS: Result:= 'Exito';
          EVENTLOG_ERROR_TYPE: Result:= 'Error';
          EVENTLOG_WARNING_TYPE: Result:= 'Advertencia';
          EVENTLOG_INFORMATION_TYPE: Result:= 'Informaci�n';
          EVENTLOG_AUDIT_SUCCESS: Result:= 'Auditor�a exitosa';
          EVENTLOG_AUDIT_FAILURE: Result:= 'Error en auditor�a';
     else
         Result:= 'Desconocido';
     end;
end;

procedure TdmWorkflow.TipoGetText( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     if DisplayText then
     begin
          if Sender.DataSet.IsEmpty then
             Text := ''
          else
             Text := EventTypeToStr( Sender.AsInteger );
     end
     else
         Text := Sender.AsString;
end;



procedure TdmWorkflow.cdsProcesosPasosAlCrearCampos(Sender: TObject);
begin
      cdsProcesosPasos.FieldByName( 'WT_NOTAS' ).OnGetText := MemoGetText;
end;

procedure TdmWorkflow.cdsProcesosCorreosAlCrearCampos(Sender: TObject);
begin
     cdsProcesosCorreos.FieldByName( 'WO_TO' ).OnGetText := MemoGetText;
end;

procedure TdmWorkflow.cdsNotificacionesAlModificar(Sender: TObject);
begin
      ZBaseEdicion.ShowFormaEdicion( CatNotificacionesEdit, TCatNotificacionesEdit );
end;

procedure TdmWorkflow.cdsNotificacionesNewRecord(DataSet: TDataSet);
begin
       with DataSet do
       begin
            FieldByName( 'WM_CODIGO' ).AsString := cdsPasosModelo.FieldByName( 'WM_CODIGO' ).AsString;
            FieldByName( 'WE_ORDEN' ).AsInteger := cdsPasosModelo.FieldByName( 'WE_ORDEN' ).AsInteger;
            FieldByName( 'WY_QUIEN' ).AsInteger := 0;
            FieldByName( 'WY_CUANDO' ).AsInteger := 0;
            FieldByName( 'WY_QUE' ).AsInteger := 0;
       end;
end;

procedure TdmWorkflow.cdsNotificacionesAlAdquirirDatos(Sender: TObject);
var
   FParamList: TZetaParams;
begin
     try
       FParamList := TZetaParams.Create;
       with dmCliente do
       begin
            FParamList.AddInteger('Orden', cdsPasosModelo.FieldByName('WE_ORDEN').AsInteger);
            FParamList.AddString('Modelo', cdsPasosModelo.FieldByName('WM_CODIGO').AsString);
            cdsNotificaciones.Data := ServerComparte.GetNotificaciones(FParamList.VarValues);
       end;
     finally
            FreeAndNil(FParamList);
     end;
end;

procedure TdmWorkflow.cdsNotificacionesAlBorrar(Sender: TObject);
begin
     with cdsNotificaciones do
     begin
          delete;
     end;
end;

procedure TdmWorkflow.cdsNotificacionesAfterCambios(DataSet: TDataSet);
begin
     with cdsPasosModelo do
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
end;

procedure TdmWorkflow.cdsNotificacionesAlCrearCampos(Sender: TObject);
begin
     with cdsNotificaciones do
     begin
          CreateCalculated( 'WY_TEM_STR', ftString, 30 );
          CreateCalculated( 'WY_CAD_STR', ftString, 30 );
     end;
end;

procedure TdmWorkflow.cdsNotificacionesAfterOpen(DataSet: TDataSet);
begin
     with cdsNotificaciones do
     begin
          ListaFija( 'WY_QUIEN', lfWFNotificacion );
          ListaFija( 'WY_CUANDO', lfWFNotificacionCuando );
          ListaFija( 'WY_QUE', lfWFNotificacionQue );
     end;

end;

procedure TdmWorkflow.NotificacionesModificarResponsable( const eTipo: eWFNotificacion; const sCampoResponsable: String );
begin
     ModificarResponsable( cdsNotificaciones, eTipo, sCampoResponsable );
end;

procedure TdmWorkflow.cdsNotificacionesCalcFields(DataSet: TDataSet);
begin
     with cdsNotificaciones do
     begin
          FieldByName( 'WY_TEM_STR' ).AsString := ObtenCampoTiempo ( FieldByName( 'WY_TIEMPO' ).AsInteger );
          FieldByName( 'WY_CAD_STR' ).AsString := ObtenCampoTiempo ( FieldByName( 'WY_CADA' ).AsInteger );
     end;
end;

function TdmWorkFlow.ObtenCampoTiempo( Tiempo : Integer ) : String;
begin
       Result := IntToStr( Trunc (Tiempo / 1440) ) + ' Dias ';
       Tiempo := Tiempo Mod 1440;
       Result := Result + IntToStr( Trunc (Tiempo / 60) ) + ' Horas ';
       Result := Result + IntToStr( Trunc (Tiempo MOD 60) ) + ' Minutos';
end;

//TWizCopiarModelo

function TdmWorkFlow.CopiarModelo( Parametros: TZetaParams ): Boolean;
begin
     try
        ServerComparte.CopiarModelo( Parametros.VarValues );
        ZInformation('Copiar modelo', 'Proceso terminado exitosamente', 0);
        cdsModelos.Refrescar;
        Result := true
     Except
           on Error: Exception do
           begin
                ZetaDialogo.ZError( 'Copiar modelo', 'Error al copiar modelo [' + Error.Message + ']', 0 );
                Result := false;
           end;
     end;
end;

procedure TdmWorkflow.cdsNivelRolesAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsNivelRoles.Data := ServerComparte.GetNivelRoles;
     end;
end;

procedure TdmWorkflow.cdsConexionesAlAdquirirDatos(Sender: TObject);
begin
      cdsConexiones.Data := ServerComparte.GetConexiones;
end; 

procedure TdmWorkflow.cdsConexionesNewRecord(DataSet: TDataSet);
begin
     with cdsConexiones do
     begin
          FieldByName('WX_TIP_AUT').AsInteger := 0;
          FieldByName('WX_TIP_ENC').AsInteger := 0;
     end;
end;

procedure TdmWorkflow.cdsConexionesAlModificar(Sender: TObject);
begin
     ZBaseEdicion.ShowFormaEdicion( CatConexionesEdit, TCatConexionesEdit );
end;

procedure TdmWorkflow.cdsConexionesAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsConexiones do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( dmCliente.ServerComparte.GrabaConexiones( Delta, ErrorCount ) );
          end;
     end;
end;

procedure TdmWorkflow.cdsConexionesAfterOpen(DataSet: TDataSet);
begin
     with cdsConexiones do
     begin
          ListaFija( 'WX_TIP_AUT', lfWFAutenticacionWebService );
          ListaFija( 'WX_TIP_ENC', lfWFCodificacionWebService );
     end;
end;

procedure TdmWorkflow.cdsNivelRolesGetRights(Sender: TZetaClientDataSet;
  const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := False;
end;

end.
