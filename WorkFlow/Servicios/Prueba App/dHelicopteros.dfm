object dmHelicopteros: TdmHelicopteros
  OldCreateOrder = False
  Left = 195
  Top = 161
  Height = 534
  Width = 702
  object cdsPendientes: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'XMLTransformProvider1'
    AlAdquirirDatos = cdsPendientesAlAdquirirDatos
    Left = 32
    Top = 16
  end
  object XMLTransformProvider1: TXMLTransformProvider
    TransformRead.TransformationFile = 'TransformPendientes.xml'
    Left = 32
    Top = 81
  end
  object cdsDataTarea: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'XMLTransformProvider2'
    AfterRowRequest = cdsDataTareaAfterRowRequest
    AlAdquirirDatos = cdsDataTareaAlAdquirirDatos
    Left = 160
    Top = 16
  end
  object XMLTransformProvider2: TXMLTransformProvider
    TransformRead.TransformationFile = 'TRANSHELICOPTERO.XML'
    Left = 160
    Top = 81
  end
end
