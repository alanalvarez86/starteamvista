unit FCatPendientes;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids, Buttons, StdCtrls,
     ZBaseConsulta,
     ZetaDBGrid;

type
  TCatPendientes = class(TBaseConsulta)
    ZetaDBGrid: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure ZetaDBGridDblClick(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
  public
    { Public declarations }
  end;

var
  CatPendientes: TCatPendientes;

implementation

{$R *.DFM}

uses DHelicopteros,
     ZAccesosTress,
     ZetaCommonTools,
     ZetaCommonClasses,
     Helicoptero;

{ *********** TSistUsuarios *********** }

procedure TCatPendientes.FormCreate(Sender: TObject);
begin
     inherited;
     //HelpContext := 0;
     IndexDerechos := 1;
end;

procedure TCatPendientes.Connect;
begin
     with dmHelicopteros do
     begin
          cdsPendientes.Conectar;
          DataSource.DataSet:= cdsPendientes;
     end;
end;

procedure TCatPendientes.Refresh;
begin
     dmHelicopteros.cdsPendientes.Refrescar;
end;        

function TCatPendientes.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Es Posible Agregar Tareas Pendientes';
end;

function TCatPendientes.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Es Posible Borrar Tareas Pendientes';
end;

function TCatPendientes.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := True;
end;

procedure TCatPendientes.ZetaDBGridDblClick(Sender: TObject);
var
   oHelicoptero : THelicopteros;
begin
     try
        oHelicoptero := THelicopteros.Create(Application);
        with oHelicoptero, dmHelicopteros do
        begin
             with  cdsPendientes do
             begin
                  Tarea := FieldByName('WT_GUID').AsString;
             end;
             Actualizar := True;
             ShowModal;
        end;
     finally
            FreeAndNil(oHelicoptero);
     end;
end;

end.


