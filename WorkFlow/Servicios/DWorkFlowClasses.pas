unit DWorkFlowClasses;

{$define LEONI}

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs,
     ComObj, DB, DateUtils,
     DWorkFlowTypes,
     DXMLTools,
     DZetaServerProvider,
     DCliente,
     ZetaServerDataset,
     ZetaRegistryServer,
     ZetaCommonLists,
     ZetaCommonClasses,
     IWorkFlowService1,
     WorkFlowService;

type
  ListaNotificaCuando = array of eWFNotificacionCuando;
  TGlobalData = record
    UsuarioMotor: Integer;
  end;
  TUserData = record
    Numero: Integer;
    Nombre: String;
    TipoCorreo: eEMailType;
    Direccion: String;
  end;
  TAviso = record
    Tipo: eWFNotificacion;
    Correo: String;
  end;
  TDestino = record
    Tipo: eWFNotificacion;
    Correo: String;
  end;
  TWFMensaje = class;
  TWFBuzon = class;
  TWFBase = class( TObject )
  private
    { Private Declarations }
    FMensaje: TWFMensaje;
    FDummyValues: Boolean;
    function GetProvider: TdmZetaWorkFlowProvider;
    function GetRegistry: TWFRegistry;
    function GetXMLTools: TdmXMLTools;
  protected
    { Protected Declarations }
    property DummyValues: Boolean read FDummyValues write FDummyValues;
    property Mensaje: TWFMensaje read FMensaje;
    property oZetaProvider: TdmZetaWorkFlowProvider read GetProvider;
    property Registry: TWFRegistry read GetRegistry;
    property XMLTools: TdmXMLTools read GetXMLTools;
    function NewGUID: TWFIdentifier;
    function UseDefaultIfEmpty( const sValue, sDefault: String ): String;
    procedure DoUpdateStatus(const sTexto: String);
  public
    { Public Declarations }
    constructor Create( Owner: TWFMensaje );
    procedure BuildXML; virtual; abstract;
    procedure LoadDummyValues; virtual;
    procedure Start; virtual; abstract;
    procedure Stop; virtual; abstract;
  end;
  TWFBaseTable = class( TWFBase )
  private
    { Private Declarations }
    function GetBuzon: TWFBuzon;
  protected
    { Protected Declarations }
    property Buzon: TWFBuzon read GetBuzon;
  public
    { Public Declarations }
    procedure PropiedadesLeer( Datos: TZetaCursor ); virtual;
  end;
  TWFBaseProceso = class( TWFBaseTable )
  private
    { Private Declarations }
  protected
    { Protected Declarations }
    function GetBody: String; virtual; abstract;
    function GetTema: String; virtual; abstract;
  public
    { Public Declarations }
  end;
  TWFBuzon = class( TWFBase ) //TWFBaseTable )
  private
    { Private Declarations }
    FAgregar: TZetaCursor;
    FUsuario: TZetaCursor;
    FLista: TZetaCursor;
    FRemitente: TUserData;
    FDestinatario: TUserData;
    procedure EnviarNota(const sSubject, sBody: String );
    procedure EMailBuild(const sSubject, sBody: String);
    procedure EMailPost;
  protected
    { Protected Declarations }
  public
    { Public Declarations }
    property Destinatario: TUserData read FDestinatario write FDestinatario;
    property Remitente: TUserData read FRemitente write FRemitente;
    function GetUserData(const iUsuario: Integer): TUserData;
    procedure BuildXML; override;
    procedure GetDestinatarioData(const iUsuario: Integer);
    procedure GetRemitenteData(const iUsuario: Integer);
    procedure EnviarEMail(const iAvance: Integer; const sSubject, sBody: String; const idTarea: TWFIdentifier; const eStatusTarea: eWFTareaStatus);
    procedure LoadDummyValues; override;
    procedure Notificar( Aviso: TAviso; Proceso: TWFBaseProceso ); overload;
    procedure Notificar( Aviso: TAviso; sTema, sBody: String ); overload;
    procedure Start; override;
    procedure Stop; override;
    procedure NotificarAdministrador(const sSubject, sBody: String);
  end;
  TWFPasoProceso = class;
  TWFTarea =  class;
  TWFProceso = class( TWFBaseProceso )
  private
    { Private Declarations }
    FStatus: eWFProcesoStatus;
    FFolio: Integer;
    FPasoActual: SmallInt;
    FPasos: SmallInt;
    FIDModelo: String;
    FNombre: String;
    FNotas: TNotas;
    FData: TWFData;
    FFinal: TWFFecha;
    FInicio: TWFFecha;
    FUpdateProcess: TZetaCursor;
    FUsuario: TUsuario;
    FUsuarioNombre: String;
    function GetActivo: Boolean;
    function GetPasoProceso: TWFPasoProceso;
  protected
    { Protected Declarations }
    function GetBody: String; override;
    function GetTema: String; override;
  public
    { Public Declarations }
    property Activo: Boolean read GetActivo;
    property Data: TWFData read FData write FData;
    property Folio: Integer read FFolio write FFolio;
    property Final: TWFFecha read FFinal write FFinal;
    property Inicio: TWFFecha read FInicio write FInicio;
    property IDModelo: String read FIDModelo write FIDModelo;
    property Nombre: String read FNombre write FNombre;
    property Notas: TNotas read FNotas write FNotas;
    property Pasos: SmallInt read FPasos write FPasos;
    property PasoActual: SmallInt read FPasoActual write FPasoActual;
    property PasoProceso: TWFPasoProceso read GetPasoProceso;
    property Status: eWFProcesoStatus read FStatus write FStatus;
    property Usuario: TUsuario read FUsuario write FUsuario;
    property UsuarioNombre: String read FUsuarioNombre write FUsuarioNombre;
    function GetProcesoStatus( const eStatus: eWFPasoProcesoStatus): eWFProcesoStatus;
    function Terminar( const eStatus: eWFProcesoStatus ): Boolean;
    procedure Activar;
    procedure Cancelar;
    procedure Reiniciar;
    procedure Suspender;
    procedure BuildXML; override;
    procedure Delegar;
    procedure LoadDummyValues; override;
    procedure Notificar( Aviso: TAviso ); overload;
    procedure Notificar; overload;
    procedure PropiedadesLeer( Datos: TZetaCursor ); override;
    procedure PropiedadesEscribir;
    procedure Start; override;
    procedure Stop; override;
    destructor Destroy; override;
  end;
  TWFPasoProceso = class( TWFBaseProceso )
  private
    { Private Declarations }
    FStatus: eWFPasoProcesoStatus;
    FIDProceso: Integer;
    FOrden: Integer;
    FSiguiente: SmallInt;
    FNombre: String;
    FIDModelo: String;
    FIDAccion: TCodigo;
    FUsuario: TUsuario;
    FFinal: TWFFecha;
    FInicio: TWFFecha;
    FUpdateStep: TZetaCursor;
    function GetProceso: TWFProceso;
  protected
    { Protected Declarations }
    function GetBody: String; override;
    function GetTema: String; override;
  public
    { Public Declarations }
    property Proceso: TWFProceso read GetProceso;
    property Final: TWFFecha read FFinal write FFinal;
    property Inicio: TWFFecha read FInicio write FInicio;
    property IDAccion: TCodigo read FIDAccion write FIDAccion;
    property IDModelo: String read FIDModelo write FIDModelo;
    property IDProceso: Integer read FIDProceso write FIDProceso;
    property Nombre: String read FNombre write FNombre;
    property Orden: Integer read FOrden write FOrden;
    property Siguiente: SmallInt read FSiguiente write FSiguiente;
    property Status: eWFPasoProcesoStatus read FStatus write FStatus;
    property Usuario: TUsuario read FUsuario write FUsuario;
    function HayOtroPaso: Boolean;
    procedure Activar;
    procedure ActivarSiguientePaso;
    procedure BuildXML; override;
    procedure Delegar;
    procedure LoadDummyValues; override;
    procedure Notificar( Aviso: TAviso ); overload;
    procedure Notificar; overload;
    procedure PropiedadesLeer( Datos: TZetaCursor ); override;
    procedure PropiedadesEscribir;
    procedure Start; override;
    procedure Stop; override;
    procedure Terminar( const eStatus: eWFPasoProcesoStatus );
    procedure Notificar(TipoNotificaciones: ListaNotificaCuando); overload;
    procedure Notificar(TipoNotificaciones: ListaNotificaCuando; Tarea: TWFTarea); overload;
    destructor Destroy; override;
  end;
  TWFTarea = class( TWFBaseTable )
  private
    { Private Declarations }
    FStatus: eWFTareaStatus;
    FIDProceso: Integer;
    FOrden: Integer;
    FAvance: SmallInt;
    FDescripcion: String;
    FCodigo: TWFIdentifier;
    FNotas: TNotas;
    FUsuarioDestino: TUsuario;
    FUsuarioOrigen: TUsuario;
    FFinal: TWFFecha;
    FInicio: TWFFecha;
    FActualiza: TZetaCursor;
    FDelega: TZetaCursor;
    FAgrega: TZetaCursor;
    function GetCodigoTxt: String;
    function GetPorProcesar: Boolean;
    procedure Actualizar(const eStatus: eWFTareaStatus; const iAvance: Integer; const sNotas: String);
  protected
    { Protected Declarations }
  public
    { Public Declarations }
    property Avance: SmallInt read FAvance write FAvance;
    property Codigo: TWFIdentifier read FCodigo write FCodigo;
    property CodigoTxt: String read GetCodigoTxt;
    property Descripcion: String read FDescripcion write FDescripcion;
    property Final: TWFFecha read FFinal write FFinal;
    property Inicio: TWFFecha read FInicio write FInicio;
    property IDProceso: Integer read FIDProceso write FIDProceso;
    property Notas: TNotas read FNotas write FNotas;
    property Orden: Integer read FOrden write FOrden;
    property PorProcesar: Boolean read GetPorProcesar;
    property Status: eWFTareaStatus read FStatus write FStatus;
    property UsuarioOrigen: TUsuario read FUsuarioOrigen write FUsuarioOrigen;
    property UsuarioDestino: TUsuario read FUsuarioDestino write FUsuarioDestino;
    procedure Agregar( const sDescripcion: String; const iAvance: Integer );
    procedure Cancelar( const sNotas: String );
    procedure Terminar( const sNotas: String );
    procedure BuildXML; override;
    procedure Delegar;
    procedure EnviaAceptacion(const iUsuarioDestino: Integer; const sNotas: String);
    procedure EnviaDelegacion(const iUsuarioDestino: Integer; const sNotas: String);
    procedure EnviaRechazo(const iUsuarioDestino: Integer; const sNotas: String);
    procedure EnviaRespuesta(const iUsuarioDestino: Integer; const sAccion, sNotas: String);
    procedure LoadDummyValues; override;
    procedure PropiedadesLeer( Datos: TZetaCursor ); override;
    procedure PropiedadesEscribir;
    procedure Start; override;
    procedure Stop; override;
    destructor Destroy; override;
  end;
  TWFBaseModelo = class( TWFBaseTable )
  private
    { Private Declarations }
    FXSLPath: String;
  protected
    { Protected Declarations }
    property XSLPath: String read FXSLPath;
    function AddXSLPath(const sFileName: String): String;
  public
    { Public Declarations }
    procedure Start; override;
    procedure Stop; override;
  end;
  TWFModelo = class( TWFBaseModelo )
  private
    { Private Declarations }
    FAvisoActivacion: TAviso;
    FAvisoTerminacion: TAviso;
    FAvisoCancelacion: TAviso;
    FAvisoSuspension: TAviso;
    FCodigo: String;
    FDescripcion: String;
    FNombre: String;
    FPaginaAutorizacion: String;
    FPaginaExamen: String;
    FPaginaSolicitud: String;
    FPasos: Integer;
    FXSLActivo: String;
    FXSLCancelado: String;
    FXSLTerminado: String;
    FXSLSuspendido: String;
    FAttachmentPath: String;
    FAttachmentURL: String;
    procedure SetXSLActivo(const Value: String);
    procedure SetXSLCancelado(const Value: String);
    procedure SetXSLTerminado(const Value: String);
    procedure SetXSLSuspendido(const Value: String);
  protected
    { Protected Declarations }
    function URLAutorizacion(Task: TWFTarea): String;
    function URLExaminar: String;
    function URLSolicitud: String;
  public
    { Public Declarations }
    property AttachmentPath: String read FAttachmentPath write FAttachmentPath;
    property AttachmentURL: String read FAttachmentURL write FAttachmentURL;
    property AvisoActivacion: TAviso read FAvisoActivacion write FAvisoActivacion;
    property AvisoTerminacion: TAviso read FAvisoTerminacion write FAvisoTerminacion;
    property AvisoCancelacion: TAviso read FAvisoCancelacion write FAvisoCancelacion;
    property AvisoSuspension: TAviso read FAvisoSuspension write FAvisoSuspension;
    property Codigo: String read FCodigo write FCodigo;
    property Descripcion: String read FDescripcion write FDescripcion;
    property Nombre: String read FNombre write FNombre;
    property Pasos: Integer read FPasos write FPasos;
    property PaginaAutorizacion: String read FPaginaAutorizacion write FPaginaAutorizacion;
    property PaginaExamen: String read FPaginaExamen write FPaginaExamen;
    property PaginaSolicitud: String read FPaginaSolicitud write FPaginaSolicitud;
    property XSLActivo: String read FXSLActivo write SetXSLActivo;
    property XSLCancelado: String read FXSLCancelado write SetXSLCancelado;
    property XSLTerminado: String read FXSLTerminado write SetXSLTerminado;
    property XSLSuspendido: String read FXSLSuspendido write SetXSLSuspendido;
    procedure BuildXML; override;
    procedure LoadDummyValues; override;
    procedure PropiedadesLeer( Datos: TZetaCursor ); override;
  end;
  TWFPasoModelo = class( TWFBaseModelo )
  private
    { Private Declarations }
    FAvisoCancelacion: TAviso;
    FAvisoActivacion: TAviso;
    FAvisoTerminacion: TAviso;
    FAvisoSuspension: TAviso;
    FCodigo: String;
    FDescripcion: String;
    FDestino: TDestino;
    FIDAccion: TCodigo;
    FNombre: String;
    FOrden: Integer;
    FVencimiento: Integer;
    FXSLActivo: String;
    FXSLCancelado: String;
    FXSLTarea: String;
    FXSLTerminado: String;
    FXSLSuspendido: string;
    FPaginaAutorizacion : string;
    procedure SetXSLActivo(const Value: String);
    procedure SetXSLCancelado(const Value: String);
    procedure SetXSLTarea(const Value: String);
    procedure SetXSLTerminado(const Value: String);
    procedure SetXSLSuspendido(const Value: string);
  protected
    { Protected Declarations }
  public
    { Public Declarations }
    property AvisoActivacion: TAviso read FAvisoActivacion write FAvisoActivacion;
    property AvisoTerminacion: TAviso read FAvisoTerminacion write FAvisoTerminacion;
    property AvisoCancelacion: TAviso read FAvisoCancelacion write FAvisoCancelacion;
    property AvisoSuspension: TAviso read FAvisoSuspension write FAvisoSuspension;
    property Codigo: String read FCodigo write FCodigo;
    property Descripcion: String read FDescripcion write FDescripcion;
    property Destino: TDestino read FDestino write FDestino;
    property IDAccion: TCodigo read FIDAccion write FIDAccion;
    property Nombre: String read FNombre write FNombre;
    property Orden: Integer read FOrden write FOrden;
    property Vencimiento: Integer read FVencimiento write FVencimiento;
    property XSLActivo: String read FXSLActivo write SetXSLActivo;
    property XSLCancelado: String read FXSLCancelado write SetXSLCancelado;
    property XSLTarea: String read FXSLTarea write SetXSLTarea;
    property XSLTerminado: String read FXSLTerminado write SetXSLTerminado;
    property XSLSuspendido: string read FXSLSuspendido write SetXSLSuspendido;
    property PaginaAutorizacion: string read FPaginaAutorizacion write FPaginaAutorizacion;
    procedure BuildXML; override;
    procedure LoadDummyValues; override;
    procedure PropiedadesLeer( Datos: TZetaCursor ); override;
  end;
  TWFNotificacion = class( TWFBaseModelo )
  private
    { Private Declarations }
    FCodigo: String;
    FFolio: Integer;
    FDestino: TDestino;
    FCuando: eWFNotificacionCuando;
    FAccion: eWFNotificacionQue;
    FOrden: Integer;
    FTiempo: Integer;
    FCada: Integer;
    FVeces: Integer;
    FTexto: String;
    FXSL: String;

  protected
    { Protected Declarations }
  public
    { Public Declarations }
    property Codigo: String read FCodigo write FCodigo;
    property Folio: Integer read FFolio write FFolio;
    property Destino: TDestino read FDestino write FDestino;
    property Cuando: eWFNotificacionCuando read FCuando write FCuando;
    property Accion: eWFNotificacionQue read FAccion write FAccion;
    property Orden: Integer read FOrden write FOrden;
    property Tiempo: Integer read FTiempo write FTiempo;
    property Cada: Integer read FCada write FCada;
    property Veces: Integer read FVeces write FVeces;
    property Texto: String read FTexto write FTexto;
    property XSL: String read FXSL write FXSL;
    procedure PropiedadesLeer( Datos: TZetaCursor ); override;
    procedure BuildXML; override;
    procedure LoadDummyValues; override;
  end;
  TWFConexion = class (TWFBaseTable)
  private
    { Private Declarations}
    FFolio: Integer;
    FNombre: String;
    FURL: String;
    FWSAutenticacion: eWFAutenticacionWebService;
    FWSCodificacion: eWFCodificacionWebService;
    FUsuario: String;
    FPassword: String;
  protected
    { Protected Declarations }
  public
    { Public Declarations }
    property Folio: Integer read FFolio write FFolio;
    property Nombre: String read FNombre write FNombre;
    property URL: String read FURL write FURL;
    property WSAutenticacion: eWFAutenticacionWebService read FWSAutenticacion write FWSAutenticacion;
    property WSCodificacion: eWFCodificacionWebService read FWSCodificacion write FWSCodificacion;
    property Usuario: String read FUsuario write FUsuario;
    property Password: String read FPassWord write FPassword;
    procedure BuildXML; override;
    procedure LoadDummyValues; override;
    procedure PropiedadesLeer( Datos: TZetaCursor ); override;
    procedure Start; override;
    procedure Stop; override;
  end;
  TWFAccionObtieneFirma = class;
  TWFAccionGrabaTress = class;
  TWFAccionCallWS = class;
  TWFBaseAccion = class;
  TWFMensaje = class( TObject )
  private
    { Private Declarations }
    FXMLTools: TdmXMLTools;
    FBuzon: TWFBuzon;
    FModelo: TWFModelo;
    FPasoModelo: TWFPasoModelo;
    FNotificacion: TWFNotificacion;
    FConexion: TWFConexion;
    FPasoProceso: TWFPasoProceso;
    FProceso: TWFProceso;
    FTarea: TWFTarea;
    FLeido: Boolean;
    FOrden: Integer;
    FUsuarioFinal: Integer;
    FIDProceso: Integer;
    FUsuarioInicial: Integer;
    FNotas: String;
    FMensaje: String;
    FFecha: TDateTime;
    FIDTarea: TWFIdentifier;
    FCodigo: TWFIdentifier;
    FIdNotificacion: Integer;
    FVecesEnviada: Integer;
    FProvider: TdmZetaWorkFlowProvider;
    FDatos: TZetaCursor;
    FMsgEnviar: TZetaCursor;
    FMsgResponder: TZetaCursor;
    FMsgBorrar: TZetaCursor;
    FRegistry: TWFRegistry;
    FGlobales: TGlobalData;
    FStatus: String;
    FFirma: TWFAccionObtieneFirma;
    FTress: TWFAccionGrabaTress;
    FCallWS: TWFAccionCallWS;
    FIdConexion: Integer;
    FOnUpdateStatus: TUpdateStatus;
    procedure PropiedadesLeer;
    function GetCodigoTxt: String;
  protected
    { Protected Declarations }
    procedure StatusClear;
    procedure StatusSet( const sValue: String );
  public
    { Public Declarations }
    property Buzon: TWFBuzon read FBuzon;
    property Codigo: TWFIdentifier read FCodigo write FCodigo;
    property CodigoTxt: String read GetCodigoTxt;
    property IDProceso: Integer read FIDProceso write FIDProceso;
    property IDTarea: TWFIdentifier read FIDTarea write FIDTarea;
    property Orden: Integer read FOrden write FOrden;
    property UsuarioInicial: TUsuario read FUsuarioInicial write FUsuarioInicial;
    property UsuarioFinal: TUsuario read FUsuarioFinal write FUsuarioFinal;
    property Fecha: TWFFecha read FFecha write FFecha;
    property Mensaje: String read FMensaje write FMensaje;
    property Leido: Boolean read FLeido write FLeido;
    property Notas: TNotas read FNotas write FNotas;
    property IdNotificacion: Integer read FIdNotificacion write FIdNotificacion;
    property VecesEnviada: Integer read FVecesEnviada write FVecesEnviada;
    property oZetaProvider: TdmZetaWorkFlowProvider read FProvider;
    property Registry: TWFRegistry read FRegistry;
    property Globales: TGlobalData read FGlobales;
    property XMLTools: TdmXMLTools read FXMLTools;
    property IdConexion: Integer read FIdConexion write FIdConexion;
     { Apuntadores a Instancias De Clases Subordinadas }
    property Proceso: TWFProceso read FProceso;
    property PasoProceso: TWFPasoProceso read FPasoProceso;
    property Tarea: TWFTarea read FTarea;
    property Modelo: TWFModelo read FModelo;
    property PasoModelo: TWFPasoModelo read FPasoModelo;
    property Notificacion: TWFNotificacion read FNotificacion;
    property Conexion: TWFConexion read FConexion;
    property Status: String read FStatus write StatusSet;
    property OnUpdateStatus: TUpdateStatus read FOnUpdateStatus write FOnUpdateStatus;
    constructor Create( Provider: TdmZetaWorkFlowProvider );
    destructor Destroy; override;
    function GetAllXML( Task: TWFTarea ): String; overload;
    function GetAllXML: String; overload;
    function Load( const idMensaje: TWFIdentifier ): Boolean;
    function ObtieneAccion: TWFBaseAccion;
    function Start: Boolean;
    function Stop: Boolean;
    procedure Borrar;
    procedure BuildXML;
    procedure ClearAllInstances;
    procedure DoUpdateStatus(const sTexto: String);
    procedure EnviaMensaje(const iFolio, iOrden, iSolicitante, iDestino: Integer; const sMensaje: String);
    procedure EnviaRespuesta( const iTarea: TWFIdentifier; const iDestino: Integer; const sAccion, sNotas: String );
    procedure LoadDummyValues;
  end;

  { Clase Base para Acciones }

  TWFBaseAccion = class( TObject )
  private
    { Private Declarations }
    FMensaje: TWFMensaje;
    FTareaNueva: TWFTarea;
    function GetProvider: TdmZetaWorkFlowProvider;
    function GetModelo: TWFModelo;
    function GetPasoModelo: TWFPasoModelo;
    function GetPasoProceso: TWFPasoProceso;
    function GetProceso: TWFProceso;
    function GetTarea: TWFTarea;
  protected
    { Protected Declarations }
    property oZetaProvider: TdmZetaWorkFlowProvider read GetProvider;
    property Mensaje: TWFMensaje read FMensaje;
    property Modelo: TWFModelo read GetModelo;
    property PasoModelo: TWFPasoModelo read GetPasoModelo;
    property PasoProceso: TWFPasoProceso read GetPasoProceso;
    property Proceso: TWFProceso read GetProceso;
    property Tarea: TWFTarea read GetTarea;
    property TareaNueva: TWFTarea read FTareaNueva;
    function Procesa( var sMensaje: String ): Boolean; virtual; abstract;
    function ProcesaCancelar(var sMensaje: String): Boolean;
    function ProcesaReiniciar(var sMensaje: String): Boolean;
    function ProcesaSuspender(var sMensaje: String): Boolean;
    procedure DoUpdateStatus(const sTexto: String);
    procedure PasoActivar;
    procedure PasoDelegar;
    procedure PasoTerminar( const eStatus: eWFPasoProcesoStatus );
    procedure ProcesaAceptar;
    procedure ProcesaRechazar;
    function ProcesaNotificar( var sMensaje: String ): Boolean;
    function ProcesaAutorizar( var sMensaje: String ): Boolean;
  public
    { Public Declarations }
    constructor Create(Mensaje: TWFMensaje);
    destructor Destroy; override;
    function PreProcesa( var sMensaje: String ): Boolean; virtual;
    procedure Start;
    procedure Stop;
  end;
  TWFAccionObtieneFirma = class( TWFBaseAccion )
  private
    { Private Declarations }
  protected
    { Protected Declarations }
    procedure ProcesaActivar( const lNuevo: Boolean );
  public
    { Public Declarations }
    function Procesa( var sMensaje: String ): Boolean; override;
  end;
  TWFAccionGrabaTress = class( TWFBaseAccion )
  private
    { Private Declarations }
    FClienteTress: TdmCliente;
  protected
    { Protected Declarations }
    property ClienteTress: TdmCliente read FClienteTress;
    function ProcesarTress( var sLog: String ): Boolean;
    procedure ProcesaActivar;
  public
    { Public Declarations }
    constructor Create(Mensaje: TWFMensaje);
    destructor Destroy; override;
    function Procesa( var sMensaje: String ): Boolean; override;
  end;
  TWFAccionCallWS = class( TWFBaseAccion )
  private
    { Private Declarations }
    FWSRPC : IWorkFlowService;
    FWSDocument : IWorkflowServiceserviceSoap;
  protected
    { Protected Declarations }
    function ProcesarWS( var sLog: String ): Boolean;
    procedure ProcesaActivar;
  public
    { Public Declarations }
    constructor Create(Mensaje: TWFMensaje);
    destructor Destroy; override;
    function Procesa( var sMensaje: String ): Boolean; override;
  end;
  TWFBaseAccionClass = class of TWFBaseAccion;

function CodigoToStr( const Value: TWFIdentifier ): String;

implementation

uses ZetaCommonTools,
     ZGlobalTress,
     ZetaASCIIFile;{OP: 2.Enero.2009}
     
const
     K_TAG_USUARIO_NOMBRE = 'Nombre';
     K_TAG_STATUS = 'STATUS';
     K_TAG_STATUS_TEXTO = 'STATUSTXT';

     DUMMY_USER = 1;
     DUMMY_USER2 = 2;
     DUMMY_ACTION = 'Activar';
     DUMMY_PROCESS = 1;
     DUMMY_ORDER = 1;
     DUMMY_MODEL = 'PRUEBA';

     Q_DATOS = 'select M.WN_GUID, M.WP_FOLIO, M.WE_ORDEN, M.WT_GUID, M.WN_USR_INI, M.WN_USR_FIN, M.WN_FECHA, M.WN_NOTAS, M.WN_MENSAJE, M.WN_LEIDO, M.WN_VECES,'+
               'P.WM_CODIGO, P.WP_NOMBRE, P.WP_USR_INI, P.WP_FEC_INI, P.WP_FEC_FIN, P.WP_STATUS, P.WP_PASOS, P.WP_PASO, P.WP_XMLDATA, P.WP_NOTAS, U.US_NOMBRE WP_USRNAME,'+
               'PS.WA_CODIGO, PS.WS_NOMBRE, PS.WS_STATUS, PS.WS_USR_INI, PS.WS_FEC_INI, PS.WS_FEC_FIN, PS.WS_NEXT, '+
               'MO.WM_NOMBRE, MO.WM_DESCRIP, MO.WM_PASOS, MO.WM_TIPNOT1, MO.WM_USRNOT1, MO.WM_TIPNOT2, MO.WM_USRNOT2, MO.WM_TIPNOT3, MO.WM_USRNOT3, MO.WM_URL, MO.WM_URL_OK, MO.WM_URL_EXA, MO.WM_XSL_ACT, MO.WM_XSL_CAN, MO.WM_XSL_TER, '+
               'MS.WE_NOMBRE, MS.WE_DESCRIP, MS.WE_TIPDEST, MS.WE_USRDEST, MS.WE_TIPNOT1, MS.WE_USRNOT1, MS.WE_TIPNOT2, MS.WE_USRNOT2, MS.WE_TIPNOT3, MS.WE_USRNOT3, MS.WE_XSL_ACT, MS.WE_XSL_CAN, MS.WE_XSL_TER, MS.WE_XSL_TAR, MS.WE_LIMITE, MS.WE_URL_OK, '+
               'T.WT_DESCRIP, T.WT_STATUS, T.WT_FEC_INI, T.WT_FEC_FIN, T.WT_USR_DES, T.WT_NOTAS, T.WT_AVANCE, T.WT_USR_ORI, '+
               'MN.WY_FOLIO, MN.WY_QUIEN, MN.WY_USR_NOT, MN.WY_CUANDO, MN.WY_QUE, MN.WY_TIEMPO, MN.WY_CADA, MN.WY_VECES, MN.WY_TEXTO, MN.WY_XSL, '+
               'MX.WX_FOLIO, MX.WX_NOMBRE, MX.WX_URL, MX.WX_TIP_AUT, MX.WX_USUARIO, MX.WX_PASSWRD, MX.WX_TIP_ENC ' +
               'from WMENSAJE M '+
               'left join WTAREA T on ( T.WT_GUID = M.WT_GUID ) '+
	           'join WPROCESO P on ( P.WP_FOLIO = M.WP_FOLIO ) '+
	           'join WPROSTEP PS on ( PS.WP_FOLIO = M.WP_FOLIO ) and ( PS.WE_ORDEN = M.WE_ORDEN ) '+
	           'join WMODELO MO on ( MO.WM_CODIGO = P.WM_CODIGO ) '+
               'join WMODSTEP MS on ( MS.WM_CODIGO = P.WM_CODIGO ) and ( MS.WE_ORDEN  = M.WE_ORDEN ) '+
               'left outer join WMODNOTI MN on ( MN.WM_CODIGO = P.WM_CODIGO ) and ( MN.WE_ORDEN = M.WE_ORDEN ) and ( MN.WY_FOLIO = M.WY_FOLIO ) ' +
               'join USUARIO U on ( U.US_CODIGO = P.WP_USR_INI ) '+
               'left outer join WCONEXION MX on ( MX.WX_FOLIO = MS.WX_FOLIO ) ' +
               'where ( M.WN_GUID = :WN_GUID )';
     Q_USUARIO_LEER = 'select US_EMAIL, US_NOMBRE, US_FORMATO from USUARIO where ( US_CODIGO = :US_CODIGO )';
     Q_USUARIO_LISTA = 'select US_CODIGO, US_NOMBRE from DBO.GETUSUARIOS( :Usuario, :TipoLista, :Detalle, :Tarea )';
     Q_BUZON_INSERT = 'execute procedure DBO.SP_ENVIA_OUTBOX( :ToAddress, :CCAddress, :Subject, :Body, :FromName, :FromAddress, :SubType, :GUIDTarea, :IDProceso, :Status, :Avance, :Envia, :Recibe )';
     Q_MENSAJE_CANCELA = 'update WMENSAJE set WMENSAJE.WN_LEIDO = ''%s'' where ( WMENSAJE.WN_GUID = :WN_GUID )';
     Q_MENSAJE_ENVIAR = 'insert into WMENSAJE( WN_GUID, WP_FOLIO, WE_ORDEN, WN_USR_INI, WN_USR_FIN, WN_FECHA, WN_MENSAJE ) '+
                        'values ( NEWID(), :WP_FOLIO, :WE_ORDEN, :WN_USR_INI, :WN_USR_FIN, GetDate(), :WN_MENSAJE )';
     Q_MENSAJE_RESPUESTA = 'execute procedure DBO.SP_ENVIA_RESPUESTA( :Tarea, :Destino, :Accion, :Notas )';
     Q_PROCESO_UPDATE = 'update WPROCESO set '+
                        'WP_STATUS = :WP_STATUS, '+
                        'WP_FEC_FIN = :WP_FEC_FIN, '+
                        'WP_PASO = :WP_PASO, '+
                        'WP_PROCESA = 1 '+
                        'where ( WP_FOLIO = :WP_FOLIO )';
     Q_PASO_PROCESO_UPDATE = 'update WPROSTEP set '+
                             'WS_STATUS = :WS_STATUS, '+
                             'WS_USR_INI = :WS_USR_INI, '+
                             'WS_FEC_INI = :WS_FEC_INI, '+
                             'WS_FEC_FIN = :WS_FEC_FIN, '+
                             'WS_NEXT = :WS_NEXT '+
                             'where ( WP_FOLIO = :WP_FOLIO ) and ( WE_ORDEN = :WE_ORDEN )';
     Q_TAREA_INSERT = 'insert into WTAREA( WT_GUID, WP_FOLIO, WE_ORDEN, WT_FEC_INI, WT_FEC_FIN, WT_USR_ORI, WT_USR_DES, WT_AVANCE, WT_STATUS, WT_DESCRIP, WT_NOTAS ) '+
                      'values (:WT_GUID, :WP_FOLIO, :WE_ORDEN, :WT_FEC_INI, :WT_FEC_FIN, :WT_USR_ORI, :WT_USR_DES, :WT_AVANCE, :WT_STATUS, :WT_DESCRIP, :WT_NOTAS )';
     Q_TAREA_UPDATE = 'update WTAREA set '+
                      'WT_NOTAS = :WT_NOTAS, '+
                      'WT_STATUS = :WT_STATUS, '+
                      'WT_FEC_FIN = :WT_FEC_FIN, '+
                      'WT_AVANCE = :WT_AVANCE '+
                      'where ( WT_GUID = :WT_GUID )';
     Q_TAREA_DELEGA = 'update WTAREA set '+
                      'WT_NOTAS = :WT_NOTAS, '+
                      'WT_FEC_INI = :WT_FEC_INI, '+
                      'WT_FEC_FIN = :WT_FEC_FIN, '+
                      'WT_USR_DES = :WT_USR_DES '+
                      'where ( WT_GUID = :WT_GUID )';
     Q_PASOS_PROCESO = 'select WE_ORDEN, WS_NOMBRE, WT_NOM_DES, WT_NOTAS, WT_AVANCE, WS_FEC_INI, WS_FEC_FIN, '+
                       'DBO.GetStatusTarea( WT_STATUS ) as STATUSTAREA, '+
                       '( case when WT_STATUS in ( 1, 2 ) then DBO.GetDuracion( WS_FEC_INI, GetDate()) else DBO.GetDuracion( WS_FEC_INI, WS_FEC_FIN ) end ) as DURACION, '+
                       '( case when WT_STATUS = NULL then 0 else WT_STATUS end ) as STATUSTASK '+
                       'from V_PASOS where ( WP_FOLIO = %d ) order by WE_ORDEN';
     Q_ARCHIVOS = 'select WH_ORDEN, WH_NOMBRE, WH_RUTA from WARCHIVO where ( WP_FOLIO = %d ) order by WH_NOMBRE';
     Q_NOTIFICACIONES_PASO = 'Select WM_CODIGO, WE_ORDEN, WY_FOLIO, WY_CUANDO, WY_QUE, WY_QUIEN, WY_USR_NOT, WY_TIEMPO, WY_CADA, WY_VECES, WY_TEXTO, WY_XSL from WMODNOTI where WM_CODIGO = %s and WE_ORDEN = %d and WY_CUANDO in ( %s ) order by WY_FOLIO ';
     Q_ENVIA_NOTIFICACION = 'EXEC dbo.SP_ENVIA_NOTIFICACION :WT_GUID, :WY_FOLIO, :WN_FECHA, :WN_VECES';
     Q_ENVIA_AUTORIZACION = 'EXEC dbo.SP_ENVIA_AUTORIZACION :WT_GUID, :WY_FOLIO, :WN_FECHA, 0';
     Q_ENVIA_RESPUESTA =  'EXEC dbo.SP_ENVIA_RESPUESTA :TAREA, :DESTINO, :ACCION, :NOTAS';
     Q_USUARIO_DESTINO = 'select US_CODIGO from DBO.GETUSUARIOSSIGUIENTES ( %s, %d )';

function CodigoToStr( const Value: TWFIdentifier ): String;
const
     K_CARACTERES = 2;
begin
     Result := ComObj.GUIDToString( Value );
     Result := Copy( Result, K_CARACTERES, ( Length( Result ) - K_CARACTERES ) );
end;

function FechaHoraATxt( const dValue: TDateTime ): String;
begin
     Result := FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', dValue );
end;

{ ********* TWFBase ******** }

constructor TWFBase.Create(Owner: TWFMensaje);
begin
     FMensaje := Owner;
     FDummyValues := False;
end;

function TWFBase.GetProvider: TdmZetaWorkFlowProvider;
begin
     Result := Self.Mensaje.oZetaProvider;
end;

function TWFBase.GetRegistry: TWFRegistry;
begin
     Result := Self.Mensaje.Registry;
end;

function TWFBase.GetXMLTools: TdmXMLTools;
begin
     Result := Self.Mensaje.XMLTools;
end;

function TWFBase.NewGUID: TWFIdentifier;
begin
     Result := ComObj.StringToGUID( ComObj.CreateClassID );
end;

function TWFBase.UseDefaultIfEmpty(const sValue, sDefault: String): String;
begin
     if ZetaCommonTools.StrVacio( sValue ) then
        Result := sDefault
     else
         Result := sValue;
end;

procedure TWFBase.LoadDummyValues;
begin
     DummyValues := True;
end;

procedure TWFBase.DoUpdateStatus(const sTexto: String);
begin
     Mensaje.DoUpdateStatus( sTexto );
end;

{ ********* TWFBaseTable ******** }

function TWFBaseTable.GetBuzon: TWFBuzon;
begin
     Result := Self.Mensaje.Buzon;
end;

procedure TWFBaseTable.PropiedadesLeer(Datos: TZetaCursor);
begin
     DummyValues := False;
end;

{ ****** TWFMensaje ***** }

constructor TWFMensaje.Create( Provider: TdmZetaWorkFlowProvider );
begin
     FXMLTools := TdmXMLTools.Create( nil );
     FRegistry := TWFRegistry.Create( False ); { Read-Only }
     FProvider := Provider;
     FModelo := TWFModelo.Create( Self );
     FPasoModelo := TWFPasoModelo.Create( Self );
     FProceso := TWFProceso.Create( Self );
     FPasoProceso := TWFPasoProceso.Create( Self );
     FTarea := TWFTarea.Create( Self );
     FBuzon := TWFBuzon.Create( Self );
     FNotificacion := TWFNotificacion.Create( Self );
     FConexion := TWFConexion.Create( Self );
end;

destructor TWFMensaje.Destroy;
begin
     Stop;
     if Assigned( FFirma ) then
        FreeAndNil( FFirma );
     if Assigned( FTress ) then
        FreeAndNil( FTress );
     FreeAndNil( FBuzon );
     FreeAndNil( FTarea );
     FreeAndNil( FPasoProceso );
     FreeAndNil( FProceso );
     FreeAndNil( FPasoModelo );
     FreeAndNil( FNotificacion );
     FreeAndNil( FConexion );
     FreeAndNil( FModelo );
     FreeAndNil( FRegistry );
     FreeAndNil( FXMLTools );
     inherited Destroy;
end;

function TWFMensaje.GetCodigoTxt: String;
begin
     Result := CodigoToStr( Self.Codigo );
end;

procedure TWFMensaje.DoUpdateStatus( const sTexto: String );
begin
     if Assigned( FOnUpdateStatus) then
        FOnUpdateStatus( sTexto );
end;

procedure TWFMensaje.StatusClear;
begin
     FStatus := VACIO;
end;

procedure TWFMensaje.StatusSet(const sValue: String);
begin
     FStatus := sValue;
end;

function TWFMensaje.Start: Boolean;
begin
     Result := True;
     StatusClear;
     try
        FRegistry.oZetaProvider := oZetaProvider;
        with oZetaProvider do
        begin
             {CV: Esta de mas el InitGlobales, porque ya se hace en el oZetaProvider.Start}
             InitGlobales;
             FDatos := CreateQuery( Format( Q_DATOS, [ 'CAST( '''' as VARCHAR(1) )' ] ) );
             FMsgBorrar := CreateQuery( Format( Q_MENSAJE_CANCELA, [ K_GLOBAL_SI ] ) );
             FMsgEnviar := CreateQuery( Q_MENSAJE_ENVIAR );
             FMsgResponder := CreateQuery( Q_MENSAJE_RESPUESTA );
             with FGlobales do
             begin
                  {$ifdef ANTES}
                  UsuarioMotor := K_MOTORCITO;
                  {$else}
                  UsuarioMotor := GetGlobalInteger( K_WORKFLOW_USER );
                  {$endif}
             end;
        end;
        Tarea.Start;
        Proceso.Start;
        PasoProceso.Start;
        Modelo.Start;
        PasoModelo.Start;
        Buzon.Start;
     except
           on Error: Exception do
           begin
                Status := Error.Message;
                Result := False;
           end;
     end;
end;

function TWFMensaje.Stop: Boolean;
begin
     Result := True;
     StatusClear;
     try
        Buzon.Stop;
        PasoModelo.Stop;
        Modelo.Stop;
        PasoProceso.Stop;
        Proceso.Stop;
        Tarea.Stop;
        if Assigned( FMsgResponder ) then
           FreeAndNil( FMsgResponder );
        if Assigned( FMsgEnviar ) then
           FreeAndNil( FMsgEnviar );
        if Assigned( FMsgBorrar ) then
           FreeAndNil( FMsgBorrar );
        if Assigned( FDatos ) then
           FreeAndNil( FDatos );
        if Assigned( FFirma ) then
           FFirma.Stop;
        if Assigned( FTress ) then
           FTress.Stop;
     except
           on Error: Exception do
           begin
                Status := Error.Message;
                Result := False;
           end;
     end;
end;

procedure TWFMensaje.LoadDummyValues;
begin
     { Campos de WMENSAJE }
     Codigo := ComObj.StringToGUID( ComObj.CreateClassID );
     IDTarea := ComObj.StringToGUID( ComObj.CreateClassID );
     IDProceso := DUMMY_PROCESS;
     Orden := DUMMY_ORDER;
     UsuarioInicial := DUMMY_USER;
     UsuarioFinal := DUMMY_USER2;
     Fecha := Now;
     Mensaje := DUMMY_ACTION;
     Leido := False;
     Notas := 'Datos De Prueba';
     IdNotificacion := 1;
     VecesEnviada := 0;
     IdConexion := 0;
     Tarea.LoadDummyValues;
     Proceso.LoadDummyValues;
     PasoProceso.LoadDummyValues;
     Modelo.LoadDummyValues;
     PasoModelo.LoadDummyValues;
     Notificacion.LoadDummyValues;
     Buzon.LoadDummyValues;
end;

function TWFMensaje.GetAllXML( Task: TWFTarea ): String;
begin
     with XMLTools do
     begin
          { Esto se hace para incorporar el XML del Documento directamente }
          XMLInit( 'WORKFLOW', Proceso.Data );
          try
             Self.BuildXML;
             Proceso.BuildXML;
             PasoProceso.BuildXML;
             Task.BuildXML;
             Modelo.BuildXML;
             PasoModelo.BuildXML;
             Notificacion.BuildXML;
             Conexion.BuildXML;
             Buzon.BuildXML;
          finally
                 XMLEnd;
          end;
          Result := XMLAsText;
     end;
end;

function TWFMensaje.GetAllXML: String;
begin
     Result := GetAllXML( Self.Tarea );
end;

procedure TWFMensaje.BuildXML;
var
   Nodo: TZetaXMLNode;
begin
     with XMLTools do
     begin
          WriteStartElement( 'MENSAJE' );
          try
             WriteValueGUID( 'GUID', Codigo );
             WriteValueGUID( 'TAREA', IDTarea );
             WriteValueInteger( 'FOLIO', IDProceso );
             WriteValueInteger( 'ORDEN', Orden );
             Nodo := WriteValueInteger( 'USUARIOINICIAL', UsuarioInicial );
             WriteAttributeString( K_TAG_USUARIO_NOMBRE, Buzon.GetUserData( UsuarioInicial ).Nombre, Nodo );
             Nodo := WriteValueInteger( 'USUARIOFINAL', UsuarioFinal );
             WriteAttributeString( K_TAG_USUARIO_NOMBRE, Buzon.GetUserData( UsuarioFinal ).Nombre, Nodo );
             WriteValueDate( 'FECHA', Fecha );
             WriteValueString( 'MENSAJE', Mensaje );
             WriteValueBoolean( 'LEIDO', Leido );
             WriteValueString( 'NOTAS', Notas );
             WriteValueInteger( 'VECESENVIADA', VecesEnviada);
             WriteValueInteger( 'FOLIONOTI', IdNotificacion );
            WriteValueString('URL_SERVIDOR', oZetaProvider.GetGlobalString(K_WORKFLOW_DIRECTORIO_RAIZ));
          finally
                 WriteEndElement;
          end;
     end;
end;

procedure TWFMensaje.PropiedadesLeer;
begin
     with FDatos do
     begin
          { Campos de WMENSAJE }
          Codigo := oZetaProvider.CampoAsGUID( FieldByName( 'WN_GUID' ) );
          IDTarea := oZetaProvider.CampoAsGUID( FieldByName( 'WT_GUID' ) );
          IDProceso := FieldByName( 'WP_FOLIO' ).AsInteger;
          Orden := FieldByName( 'WE_ORDEN' ).AsInteger;
          UsuarioInicial := FieldByName( 'WN_USR_INI' ).AsInteger;
          UsuarioFinal := FieldByName( 'WN_USR_FIN' ).AsInteger;
          Fecha := FieldByName( 'WN_FECHA' ).AsDateTime;
          Mensaje := FieldByName( 'WN_MENSAJE' ).AsString;
          Leido := ZetaCommonTools.zStrToBool( FieldByName( 'WN_LEIDO' ).AsString );
          Notas := FieldByName( 'WN_NOTAS' ).AsString;
          IdNotificacion := FieldByName( 'WY_FOLIO').AsInteger;
          VecesEnviada := FieldByName( 'WN_VECES' ).AsInteger;
          IdConexion := FieldByName( 'WX_FOLIO' ).AsInteger;

     end;
     Tarea.PropiedadesLeer( FDatos );
     Proceso.PropiedadesLeer( FDatos );
     PasoProceso.PropiedadesLeer( FDatos );
     Modelo.PropiedadesLeer( FDatos );
     PasoModelo.PropiedadesLeer( FDatos );
     Notificacion.PropiedadesLeer( FDatos );
     Conexion.PropiedadesLeer(FDatos);
end;

function TWFMensaje.Load( const idMensaje: TWFIdentifier ): Boolean;
begin
     Result := True;
     StatusClear;
     try
        with FDatos do
        begin
             Active := False;
             with oZetaProvider do
             begin
                  ParamAsGUID( FDatos, 'WN_GUID', idMensaje );
             end;
             Active := True;
             if Eof then
             begin
                  Result := False;
                  Status := Format( 'Mensaje %s No Existe', [ CodigoToStr( idMensaje ) ] );
             end
             else
             begin
                  PropiedadesLeer;
             end;
             Active := False;
        end;
     except
           on Error: Exception do
           begin
                Status := Error.Message;
                Result := False;
           end;
     end;
end;

procedure TWFMensaje.EnviaMensaje( const iFolio, iOrden, iSolicitante, iDestino: Integer; const sMensaje: String );
begin
     with oZetaProvider do
     begin
          ParamAsInteger( FMsgEnviar, 'WP_FOLIO', iFolio );
          ParamAsInteger( FMsgEnviar, 'WE_ORDEN', iOrden );
          ParamAsInteger( FMsgEnviar, 'WN_USR_INI', iSolicitante );
          ParamAsInteger( FMsgEnviar, 'WN_USR_FIN', iDestino );
          ParamAsVarChar( FMsgEnviar, 'WN_MENSAJE', sMensaje, K_ANCHO_DESCRIPCION );
          EmpiezaTransaccion;
          try
             Ejecuta( FMsgEnviar );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     RollBackTransaccion;
                end;
          end;
     end;
end;

procedure TWFMensaje.EnviaRespuesta( const iTarea: TWFIdentifier; const iDestino: Integer; const sAccion, sNotas: String );
begin
     with oZetaProvider do
     begin
          ParamAsGUID( FMsgResponder, 'Tarea', iTarea );
          ParamAsInteger( FMsgResponder, 'Destino', iDestino );
          ParamAsString( FMsgResponder, 'Accion', sAccion );
          ParamAsString( FMsgResponder, 'Notas', sNotas );
          EmpiezaTransaccion;
          try
	            Ejecuta( FMsgResponder );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     RollBackTransaccion;
                end;
          end;
     end;
end;

procedure TWFMensaje.Borrar;
begin
     with oZetaProvider do
     begin
          ParamAsGUID( FMsgBorrar, 'WN_GUID', Codigo );
          EmpiezaTransaccion;
          try
	            Ejecuta( FMsgBorrar );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     RollBackTransaccion;
                end;
          end;
     end;
end;

procedure TWFMensaje.ClearAllInstances;
begin
     with oZetaProvider do
     begin
          EmpiezaTransaccion;
          try
             EjecutaAndFree( 'delete from WOUTBOX' );
             EjecutaAndFree( 'delete from WARCHIVO' );
             EjecutaAndFree( 'delete from WTAREA' );
             EjecutaAndFree( 'delete from WMENSAJE' );  
             EjecutaAndFree( 'delete from WPROSTEP' );
             EjecutaAndFree( 'delete from WPROCESO' );
             TerminaTransaccion( True );
          except
                on Error: Exception do
                begin
                     RollBackTransaccion;
                end;
          end;
     end;
end;

function TWFMensaje.ObtieneAccion: TWFBaseAccion;
var
   sAccion: TCodigo;
begin
     sAccion := PasoProceso.IDAccion;
     if ( sAccion = ACCION_FIRMA ) then
     begin
          if not Assigned( FFirma ) then
          begin
               FFirma := TWFAccionObtieneFirma.Create( Self );
          end;
          FFirma.Start;
          Result := FFirma;
     end
     else
     if ( sAccion = ACCION_GRABA_TRESS ) then
     begin
          if not Assigned( FTress ) then
          begin
               FTress := TWFAccionGrabaTress.Create( Self );
          end;
          FTress.Start;
          Result := FTress;
     end
     else
     if ( sAccion = ACCION_CALL_WEBSERVICE ) then
     begin
          if not Assigned( FCallWS ) then
          begin
               FCallWS := TWFAccionCallWS.Create( Self );
          end;
          FCallWS.Start;
          Result := FCallWS;
     end
     else
         Result := nil;
end;

{ ******* TWFTarea ******* }

destructor TWFTarea.Destroy;
begin
     Stop;
     inherited Destroy;
end;

function TWFTarea.GetCodigoTxt: String;
begin
     Result := CodigoToStr( Self.Codigo );
end;

procedure TWFTarea.Start;
begin
     with oZetaProvider do
     begin
          FActualiza := CreateQuery( Q_TAREA_UPDATE );
          FDelega := CreateQuery( Q_TAREA_DELEGA );
          FAgrega := CreateQuery( Q_TAREA_INSERT );
     end;
end;

procedure TWFTarea.Stop;
begin
     if Assigned( FAgrega ) then
        FreeAndNil( FAgrega );
     if Assigned( FDelega ) then
        FreeAndNil( FDelega );
     if Assigned( FActualiza ) then
        FreeAndNil( FActualiza );
end;

procedure TWFTarea.BuildXML;
var
   Nodo: TZetaXMLNode;
begin
     with XMLTools do
     begin
          WriteStartElement( 'TAREA' );
          try
             WriteValueGUID( 'GUID', Codigo );
             WriteValueInteger( 'ORDEN', Orden );
             WriteValueInteger( 'PROCESO', IDProceso );
             WriteValueInteger( 'AVANCE', Avance );
             Nodo := WriteValueInteger( 'USUARIOORIGEN', UsuarioOrigen );
             WriteAttributeString( K_TAG_USUARIO_NOMBRE, Buzon.GetUserData( UsuarioOrigen ).Nombre, Nodo );
             Nodo := WriteValueInteger( 'USUARIODESTINO', UsuarioDestino );
             WriteAttributeString( K_TAG_USUARIO_NOMBRE, Buzon.GetUserData( UsuarioDestino ).Nombre, Nodo );
             WriteValueDateTime( 'INICIO', Inicio );
             WriteValueDateTime( 'FINAL', Final );
             WriteValueString( 'DESCRIPCION', Descripcion );
             WriteValueString( 'NOTAS', Notas );
             WriteValueInteger( K_TAG_STATUS, Ord( Status ) );
             WriteValueString( K_TAG_STATUS_TEXTO, ZetaCommonLists.ObtieneElemento( lfWFTareaStatus, Ord( Status ) ) );
             with Mensaje.Modelo do
             begin
                  WriteValueString( 'URLAPROBAR', URLAutorizacion( Self ) );
                  WriteValueString( 'URLEXAMINAR', PaginaExamen );
                  WriteValueString( 'URLSOLICITAR', PaginaSolicitud );
             end;
          finally
                 WriteEndElement;
          end;
     end;
end;

procedure TWFTarea.LoadDummyValues;
begin
     inherited LoadDummyValues;
     Codigo := Mensaje.IDTarea;
     Orden := Mensaje.Orden;
     IDProceso := Mensaje.IDProceso;
     Avance := 100;
     UsuarioOrigen := DUMMY_USER;
     UsuarioDestino := DUMMY_USER2;
     Inicio := Now;
     Final := Now;
     Descripcion := 'Tarea De Prueba';
     Notas := 'Notas De Prueba';
     Status := eWFTareaStatus( 0 );
end;

procedure TWFTarea.PropiedadesLeer(Datos: TZetaCursor);
begin
     inherited PropiedadesLeer( Datos );
     Codigo := Mensaje.IDTarea;
     Orden := Mensaje.Orden;
     IDProceso := Mensaje.IDProceso;
     with Datos do
     begin
          Avance := FieldByName( 'WT_AVANCE' ).AsInteger;
          UsuarioOrigen := FieldByName( 'WT_USR_ORI' ).AsInteger;
          UsuarioDestino := FieldByName( 'WT_USR_DES' ).AsInteger;
          Inicio := FieldByName( 'WT_FEC_INI' ).AsDateTime;
          Final := FieldByName( 'WT_FEC_FIN' ).AsDateTime;
          Descripcion := FieldByName( 'WT_DESCRIP' ).AsString;
          Notas := FieldByName( 'WT_NOTAS' ).AsString;
          Status := eWFTareaStatus( FieldByName( 'WT_STATUS' ).AsInteger );
     end;
end;

procedure TWFTarea.PropiedadesEscribir;
begin
     with oZetaProvider do
     begin
          { Llave Primaria }
          ParamAsGUID( FActualiza, 'WT_GUID', Codigo );
          { Datos }
          ParamAsBlob( FActualiza, 'WT_NOTAS', Notas );
          ParamAsDateTime( FActualiza, 'WT_FEC_FIN', Final );
          ParamAsInteger( FActualiza, 'WT_AVANCE', Avance );
          ParamAsInteger( FActualiza, 'WT_STATUS', Ord( Status ) );
          EmpiezaTransaccion;
          try
             Ejecuta( FActualiza );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     RollBackTransaccion;
                end;
          end;
     end;
end;

procedure TWFTarea.Actualizar(const eStatus: eWFTareaStatus; const iAvance: Integer; const sNotas: String);
begin
     Status := eStatus;
     Final := Now;
     Avance := iAvance;
     Notas := sNotas;
     PropiedadesEscribir;
end;

function TWFTarea.GetPorProcesar: Boolean;
begin
     Result := ( Status in [ sttActiva, sttEsperando ] );
end;

procedure TWFTarea.Cancelar( const sNotas: String );
begin
     Actualizar( sttCancelada, K_CIEN_PORCIENTO, sNotas );
end;

procedure TWFTarea.Terminar( const sNotas: String );
begin
     Actualizar( sttTerminada, K_CIEN_PORCIENTO, sNotas );
end;

procedure TWFTarea.Delegar;
var
   sTexto: string;
{$ifdef WORKFLOWTEST}
//   oTexto: TStrings;
{$endif}

const
     K_ARCH_BIT = 'DelegarBitacora.log';
     K_TEMA = 'Solicitud de Autorizaci�n de %s - %s (Proceso Delegado)';
     K_MENSAJE = 'Como Parte Del Proceso De Autorizaci�n De %1:s%0:s'+
                 'Se Le Pide Por Favor Verificar La Forma %2:s%0:s'+
                 '%0:s'+
                 '� Gracias !';
begin
     {
     Reinicia la fecha inicial, para que la
     tarea no refleje todo el tiempo al nuevo responsable
     }
     DoUpdateStatus( '*** Inicia el proceso de delegar ***' );

     Inicio := Mensaje.Fecha;
     Final := Mensaje.Fecha;
     Notas := Mensaje.Notas;
     { Nuevo responsable }
     UsuarioDestino := Mensaje.UsuarioFinal;

     DoUpdateStatus( 'Agregar par�metros Tarea.Delegar - Inicio' );
     with oZetaProvider do
     begin
          { Llave Primaria }
          DoUpdateStatus( Format( 'Codigo de Tarea a Delegar: %s', [ CodigoToStr( Codigo ) ] ) );
          //ParamAsVarChar( FDelega, 'WT_GUID', CodigoToStr( Codigo ), 0 );

          ParamAsGUID( FDelega, 'WT_GUID', Codigo );
          { Datos }
          DoUpdateStatus( 'Parametro Notas' );
          ParamAsBlob( FDelega, 'WT_NOTAS', Notas );

          DoUpdateStatus( Format( 'Fecha de Inicio: %s', [ DateToStr( Inicio ) ] ) );
          ParamAsDateTime( FDelega, 'WT_FEC_INI', Inicio );

          DoUpdateStatus( Format( 'Fecha final: %s', [ DateToStr( Final ) ] ) );
          ParamAsDateTime( FDelega, 'WT_FEC_FIN', Final );

          DoUpdateStatus( Format( 'Usuario Destino: %d', [ UsuarioDestino ] ) );
          ParamAsInteger( FDelega, 'WT_USR_DES', UsuarioDestino );

          DoUpdateStatus( 'Antes de ejecutar Delegar' );
          EmpiezaTransaccion;
          try
             Ejecuta( FDelega);
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     RollBackTransaccion;
                end;
          end;
          DoUpdateStatus( 'Despu�s de ejecutar Delegar' );
     end;
     DoUpdateStatus( 'Agregar par�metros Tarea.Delegar - Termino' );

     DoUpdateStatus( 'Buz�n Tarea.Delegar - Inicio' );
     with Buzon do
     begin
          GetRemitenteData( Mensaje.UsuarioInicial ); //?
          GetDestinatarioData( Mensaje.UsuarioFinal ); //?
          DoUpdateStatus( Format( 'Mensaje.Tarea(XML) %s', [ Mensaje.GetAllXML( Mensaje.Tarea ) ] ) );
          DoUpdateStatus( Format( 'Mensaje.PasoModelo.XSLTarea %s', [ Mensaje.PasoModelo.XSLTarea ] ) );
{$ifdef WORKFLOWTEST}
//          oTexto := TStringList.Create;
  //        try
    //         otexto.Add( Mensaje.GetAllXML( Mensaje.Tarea ) );
      //       oTexto.SaveToFile( Format( 'c:\%s%s%s.log', [ CodigoToStr( Codigo ), FormatDateTime( 'yyyymmdd', Date ), FormatDateTime( 'hhmmss', Time ) ] ) );
        //  finally
          //       FreeAndNil( oTexto );
//          end;
{$endif}
          DoUpdateStatus( Format( 'sTexto antes de transformar %s', [ sTexto ] ) );

          if not XMLTools.XMLTransform( Mensaje.GetAllXML( Mensaje.Tarea ), Mensaje.PasoModelo.XSLTarea, sTexto ) then
          begin
               DoUpdateStatus( 'Buz�n Dentro de la transformaci�n de XML - Inicio' );
               sTexto := Format( K_MENSAJE, [ CR_LF, Mensaje.Modelo.Nombre, Mensaje.Modelo.URLAutorizacion( Mensaje.Tarea ) ] );
               DoUpdateStatus( Format( 'Buz�n Dentro de la transformaci�n de XML - Termino [%s]', [ sTexto ] ) );
          end;
          DoUpdateStatus( Format( 'sTexto Despues de transformar %s', [ sTexto ] ) );
          DoUpdateStatus( 'Buz�n Antes de enviar correo' );
          EnviarEMail( 0, Format( K_TEMA, [ Mensaje.Modelo.Nombre, Buzon.Remitente.Nombre ] ), sTexto, Codigo, sttEsperando );
          DoUpdateStatus( 'Buz�n Despu�s de enviar correo' );
     end;
     DoUpdateStatus( 'Buz�n Tarea.Delegar - Termino' );
end;

procedure TWFTarea.Agregar( const sDescripcion: String; const iAvance: Integer );
begin
     Codigo := NewGUID;
     with Mensaje do
     begin
          Self.IDProceso := Proceso.Folio;
          Self.Orden := PasoProceso.Orden;
          Self.Inicio := Fecha;
          Self.Final := Fecha;
          Self.UsuarioOrigen := UsuarioInicial;
          Self.UsuarioDestino := UsuarioFinal;
          {
          GA (30/Ago/2004): Al agregarse una tarea no se debe de grabar sus
          notas; �stas se deben grabar al Actualizar la tarea
          cuando se procesa el mensaje que indica que la tarea
          fu� aceptada o rechazada.
          Self.Notas := Notas;
          }
     end;
     Status := sttActiva;
     Descripcion := sDescripcion;
     Avance := iAvance;
     with oZetaProvider do
     begin
          { Llave Primaria }
          ParamAsGUID( FAgrega, 'WT_GUID', Codigo );
          { Datos }
          ParamAsDateTime( FAgrega, 'WT_FEC_INI', Inicio );
          ParamAsDateTime( FAgrega, 'WT_FEC_FIN', Final );
          ParamAsInteger( FAgrega, 'WP_FOLIO', IDProceso );
          ParamAsInteger( FAgrega, 'WE_ORDEN', Orden );
          ParamAsInteger( FAgrega, 'WT_USR_ORI', UsuarioOrigen );
          ParamAsInteger( FAgrega, 'WT_USR_DES', UsuarioDestino );
          ParamAsInteger( FAgrega, 'WT_AVANCE', Avance );
          ParamAsInteger( FAgrega, 'WT_STATUS', Ord( Status ) );
          ParamAsVarChar( FAgrega, 'WT_DESCRIP', Descripcion, K_ANCHO_OBSERVACIONES );
          ParamAsBlob( FAgrega, 'WT_NOTAS', Notas );
          EmpiezaTransaccion;
          try
             Ejecuta( FAgrega );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     RollBackTransaccion;
                end;
          end;
     end;
end;

procedure TWFTarea.EnviaRespuesta( const iUsuarioDestino: Integer; const sAccion, sNotas: String );
begin
     Mensaje.EnviaRespuesta( Codigo, iUsuarioDestino, sAccion, sNotas );
end;

procedure TWFTarea.EnviaAceptacion( const iUsuarioDestino: Integer; const sNotas: String );
begin
     EnviaRespuesta( iUsuarioDestino, MSG_ACEPTAR, sNotas );
end;

procedure TWFTarea.EnviaRechazo( const iUsuarioDestino: Integer; const sNotas: String );
begin
     EnviaRespuesta( iUsuarioDestino, MSG_RECHAZAR, sNotas );
end;

procedure TWFTarea.EnviaDelegacion( const iUsuarioDestino: Integer; const sNotas: String );
begin
     EnviaRespuesta( iUsuarioDestino, MSG_DELEGAR, sNotas );
end;

{ ****** TWFProceso ***** }

destructor TWFProceso.Destroy;
begin
     Stop;
     inherited Destroy;
end;

function TWFProceso.GetActivo: Boolean;
begin
     Result := ( Status = prActivo );
end;

function TWFProceso.GetPasoProceso: TWFPasoProceso;
begin
     Result := Mensaje.PasoProceso;
end;

function TWFProceso.GetProcesoStatus( const eStatus: eWFPasoProcesoStatus ): eWFProcesoStatus;
begin
     case eStatus of
          ppInactivo: Result := prInactivo;
          ppActivo: Result := prActivo;
          ppEsperando: Result := prActivo;
          ppTerminado: Result := prTerminado;
          ppCancelado: Result := prCancelado;
     else
         Result := prInactivo;
     end;
end;

procedure TWFProceso.Start;
begin
     with oZetaProvider do
     begin
          FUpdateProcess := CreateQuery( Q_PROCESO_UPDATE );
     end;
end;

procedure TWFProceso.Stop;
begin
     if Assigned( FUpdateProcess ) then
        FreeAndNil( FUpdateProcess );
end;

procedure TWFProceso.BuildXML;
var
   Nodo: TZetaXMLNode;
   FArchivos: TServerDataset;
begin
     with XMLTools do
     begin
          WriteStartElement( 'PROCESO' );
          try
             WriteValueInteger( 'FOLIO', Folio );
             WriteValueString( 'MODELO', IDModelo );
             WriteValueDateTime( 'INICIO', Inicio );
             WriteValueDateTime( 'FINAL', Final );
             Nodo := WriteValueInteger( 'USUARIO', Usuario );
             WriteAttributeString( K_TAG_USUARIO_NOMBRE, UsuarioNombre, Nodo );
             WriteValueString( 'NOTAS', Notas );
             WriteValueInteger( 'PASOS', Pasos );
             WriteValueInteger( 'PASOACTUAL', PasoActual );
             WriteValueInteger( K_TAG_STATUS, Ord( Status ) );
             WriteValueString( K_TAG_STATUS_TEXTO, ZetaCommonLists.ObtieneElemento( lfWFProcesoStatus, Ord( Status ) ) );
          finally
                 WriteEndElement;
          end;
          FArchivos := TServerDataSet.Create( nil );
          try
             with oZetaProvider do
             begin
                  FArchivos.Data := OpenSQL( Comparte, Format( Q_ARCHIVOS, [ Folio ] ), not DummyValues );
             end;
             with FArchivos do
             begin
                  Active := True;
                  if DummyValues then
                  begin
                       Append;
                       FieldByName( 'WH_ORDEN' ).AsInteger := 1;
                       FieldByName( 'WH_NOMBRE' ).AsString := 'Archivo 1';
                       FieldByName( 'WH_RUTA' ).AsString := 'C:\Temp\Archivo 1';
                       Post;
                       Append;
                       FieldByName( 'WH_ORDEN' ).AsInteger := 2;
                       FieldByName( 'WH_NOMBRE' ).AsString := 'Archivo 2';
                       FieldByName( 'WH_RUTA' ).AsString := 'C:\Temp\Archivo 2';
                       Post;
                       First;
                  end;
                  XMLTools.XMLBuildDataSet( FArchivos, 'ARCHIVOS' );
                  Active := False;
             end;
          finally
                 FreeAndNil( FArchivos );
          end;
     end;
end;

procedure TWFProceso.LoadDummyValues;
begin
     inherited LoadDummyValues;
     Folio := Mensaje.IDProceso;
     IDModelo := DUMMY_MODEL;
     Inicio := Now;
     Final := Now;
     Usuario := DUMMY_USER;
     UsuarioNombre := 'Usuario De Prueba';
     Nombre := 'Proceso De Prueba';
     Notas := 'Notas De Prueba';
     Data := '<DATOS><EMPRESA>TRESS4</EMPRESA><EMPLEADO>4</EMPLEADO></DATOS>';
     Status := eWFProcesoStatus( 0 );
     Pasos := 1;
     PasoActual := 1;
end;

procedure TWFProceso.PropiedadesLeer(Datos: TZetaCursor);
begin
     inherited PropiedadesLeer( Datos );
     Folio := Mensaje.IDProceso;
     with Datos do
     begin
          IDModelo := FieldByName( 'WM_CODIGO' ).AsString;
          Inicio := FieldByName( 'WP_FEC_INI' ).AsDateTime;
          Final := FieldByName( 'WP_FEC_FIN' ).AsDateTime;
          Usuario := FieldByName( 'WP_USR_INI' ).AsInteger;
          UsuarioNombre := FieldByName( 'WP_USRNAME' ).AsString;
          Nombre := FieldByName( 'WP_NOMBRE' ).AsString;
          Notas := FieldByName( 'WP_NOTAS' ).AsString;
          Data := FieldByName( 'WP_XMLDATA' ).AsString;
          Status := eWFProcesoStatus( FieldByName( 'WP_STATUS' ).AsInteger );
          Pasos := FieldByName( 'WP_PASOS' ).AsInteger;
          PasoActual := FieldByName( 'WP_PASO' ).AsInteger;
     end;
end;

procedure TWFProceso.PropiedadesEscribir;
begin
     with oZetaProvider do
     begin
          { Llave Primaria }
          ParamAsInteger( FUpdateProcess, 'WP_FOLIO', Folio );
          { Datos }
          ParamAsDateTime( FUpdateProcess, 'WP_FEC_FIN', Final );
          ParamAsInteger( FUpdateProcess, 'WP_PASO', PasoActual );
          ParamAsInteger( FUpdateProcess, 'WP_STATUS', Ord( Status ) );
          EmpiezaTransaccion;
          try
             Ejecuta( FUpdateProcess );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     RollBackTransaccion;
                end;
          end;
     end;
end;

procedure TWFProceso.Activar;
begin
     Status := prActivo;
     Final := Mensaje.Fecha;
     PasoActual := PasoProceso.Orden;
     PropiedadesEscribir;
end;

procedure TWFProceso.Delegar;
begin
     Final := Mensaje.Fecha;
     PropiedadesEscribir;
end;

procedure TWFProceso.Cancelar;
begin
     Status := prCancelado;
     PropiedadesEscribir;
end;

procedure TWFProceso.Suspender;
begin
     Status := prSuspendido;
     PropiedadesEscribir;
end;

procedure TWFProceso.Reiniciar;
begin
     Status := prActivo;
     PropiedadesEscribir;
end;

function TWFProceso.Terminar( const eStatus: eWFProcesoStatus ): Boolean;
var
   eOldStatus: eWFProcesoStatus;
begin
     eOldStatus := Self.Status;
     if ( eStatus = prCancelado ) then
        Self.Status := prCancelado
     else
         if PasoProceso.HayOtroPaso then
            Self.Status := prActivo
         else
             Self.Status := prTerminado;
     Final := Mensaje.Fecha;
     PropiedadesEscribir;
     { Notifica cuando hay un cambio de Status }
     Result := ( Self.Status <> eOldStatus );
end;

procedure TWFProceso.Notificar( Aviso: TAviso );
begin
     Buzon.Notificar( Aviso, Self );
end;

procedure TWFProceso.Notificar;
begin
     {case Status of
          prActivo: Notificar( Mensaje.Modelo.AvisoActivacion );
          prTerminado: Notificar( Mensaje.Modelo.AvisoTerminacion );
          prCancelado: Notificar( Mensaje.Modelo.AvisoCancelacion );
          prSuspendido: Notificar( Mensaje.Modelo.AvisoSuspension );
     end;}
end;

function TWFProceso.GetTema: String;
begin
     case Status of
          prActivo: Result := Format( 'Inici� Proceso %s # %d', [ Nombre, Folio ] );
          prTerminado: Result := Format( 'Termin� Proceso %s # %d', [ Nombre, Folio ] );
          prCancelado: Result := Format( 'CANCELACION Proceso %s # %d', [ Nombre, Folio ] );
     else
         Result := VACIO;
     end;
end;

function TWFProceso.GetBody: String;
const
     K_ACTIVO = 'Proceso: %1:s%0:s'+
                'Iniciado Por: %2:s%0:s'+
                'En %3:s%0:s'+
                'Observaciones: %4:s%0:s'+
                'Para Consultar, Entre A: %5:s';
     K_TERMINADO = 'Proceso TERMINADO: %1:s%0:s'+
                   'Iniciado Por: %2:s%0:s'+
                   'Empieza En %3:s%0:s'+
                   'Termina En %4:s%0:s'+
                   'Observaciones: %5:s%0:s'+
                   'Para Consultar, Entre A: %6:s';
     K_CANCELADO = 'Proceso CANCELADO: %1:s%0:s'+
                   'Iniciado Por: %2:s%0:s'+
                   'Empieza En %3:s%0:s'+
                   'Termina En %4:s%0:s'+
                   'Observaciones: %5:s%0:s'+
                   'Para Consultar, Entre A: %6:s';
     K_SUSPENDIDO = 'Proceso SUSPENDIDO: %1:s%0:s'+
                   'Iniciado Por: %2:s%0:s'+
                   'Empieza En %3:s%0:s'+
                   'Termina En %4:s%0:s'+
                   'Observaciones: %5:s%0:s'+
                   'Para Consultar, Entre A: %6:s';
var
   sXSLFile, sTexto: String;
begin
     with Mensaje.Modelo do
     begin
          case Self.Status of
                   prActivo: sXSLFile := XSLActivo;
                   prTerminado: sXSLFile := XSLTerminado;
                   prCancelado: sXSLFile := XSLCancelado;
                   prSuspendido: sXSLFile := XSLSuspendido;
          else
              sXSLFile := VACIO;
          end;
     end;
     if XMLTools.XMLTransform( Mensaje.GetAllXML, sXSLFile, sTexto ) then
     begin
          Result := sTexto;
     end
     else
     begin
          sTexto := Mensaje.Modelo.URLExaminar;
          case Status of
               prActivo: Result := Format( K_ACTIVO, [ CR_LF, Nombre, UsuarioNombre, FechaHoraATxt( Inicio ), Notas, sTexto ] );
               prTerminado: Result := Format( K_TERMINADO, [ CR_LF, Nombre, UsuarioNombre, FechaHoraATxt( Inicio ), FechaHoraATxt( Final ), Notas, sTexto ] );
               prCancelado: Result := Format( K_CANCELADO, [ CR_LF, Nombre, UsuarioNombre, FechaHoraATxt( Inicio ), FechaHoraATxt( Final ), Notas, sTexto ] );
               prSuspendido: Result := Format( K_SUSPENDIDO, [ CR_LF, Nombre, UsuarioNombre, FechaHoraATxt( Inicio ), FechaHoraATxt( Final ), Notas, sTexto ] );
          else
              Result := VACIO;
          end;
     end;
end;

{ ******* TWFPasoProceso ******** }

destructor TWFPasoProceso.Destroy;
begin
     Stop;
     inherited Destroy;
end;

function TWFPasoProceso.GetProceso: TWFProceso;
begin
     Result := Mensaje.Proceso;
end;

function TWFPasoProceso.HayOtroPaso: Boolean;
begin
     Result := ( Siguiente > 0 );
end;

procedure TWFPasoProceso.Start;
begin
     with oZetaProvider do
     begin
          FUpdateStep := CreateQuery( Q_PASO_PROCESO_UPDATE );
     end;
end;

procedure TWFPasoProceso.Stop;
begin
     if Assigned( FUpdateStep ) then
        FreeAndNil( FUpdateStep );
end;

procedure TWFPasoProceso.BuildXML;
var
   FPasos: TServerDataSet;
   Nodo: TZetaXMLNode;
begin
     with XMLTools do
     begin
          WriteStartElement( 'PROCESOPASOACTUAL' );
          try
             WriteValueString( 'MODELO', IDModelo );
             WriteValueInteger( 'FOLIO', IDProceso );
             WriteValueInteger( 'ORDEN', Orden );
             WriteValueString( 'ACCION', IDAccion );
             WriteValueDateTime( 'INICIO', Inicio );
             WriteValueDateTime( 'FINAL', Final );
             WriteValueString( 'NOMBRE', Nombre );
             WriteValueInteger( 'SIGUIENTE', Siguiente );
             Nodo := WriteValueInteger( 'USUARIO', Usuario );
             WriteAttributeString( K_TAG_USUARIO_NOMBRE, Buzon.GetUserData( Usuario ).Nombre, Nodo );
             WriteValueInteger( K_TAG_STATUS, Ord( Status ) );
             WriteValueString( K_TAG_STATUS_TEXTO, ZetaCommonLists.ObtieneElemento( lfWFPasoProcesoStatus, Ord( Status ) ) );
          finally
                 WriteEndElement;
          end;
          FPasos := TServerDataSet.Create( nil );
          try
             with oZetaProvider do
             begin
                  FPasos.Data := OpenSQL( Comparte, Format( Q_PASOS_PROCESO, [ IDProceso ] ), not DummyValues );
             end;
             with FPasos do
             begin
                  Active := True;
                  if DummyValues then
                  begin
                       Append;
                       FieldByName( 'WE_ORDEN' ).AsInteger := 1;
                       FieldByName( 'WS_NOMBRE' ).AsString := 'Paso 1';
                       FieldByName( 'WT_NOM_DES' ).AsString := 'Paso # 1';
                       FieldByName( 'WT_NOTAS' ).AsString := 'Este Es El Paso # 1';
                       FieldByName( 'WT_AVANCE' ).AsInteger := 0;
                       FieldByName( 'WS_FEC_INI' ).AsDateTime := Now;
                       FieldByName( 'WS_FEC_FIN' ).AsDateTime := Now;
                       FieldByName( 'STATUSTAREA' ).AsString := 'Activa';
                       FieldByName( 'DURACION' ).AsString := '0 Minutos';
                       FieldByName( 'STATUSTASK' ).AsInteger := 1;
                       Post;
                       Append;
                       FieldByName( 'WE_ORDEN' ).AsInteger := 2;
                       FieldByName( 'WS_NOMBRE' ).AsString := 'Paso 2';
                       FieldByName( 'WT_NOM_DES' ).AsString := 'Paso # 2';
                       FieldByName( 'WT_NOTAS' ).AsString := 'Este Es El Paso # 2';
                       FieldByName( 'WT_AVANCE' ).AsInteger := 100;
                       FieldByName( 'WS_FEC_INI' ).AsDateTime := Now;
                       FieldByName( 'WS_FEC_FIN' ).AsDateTime := Now;
                       FieldByName( 'STATUSTAREA' ).AsString := 'Activa';
                       FieldByName( 'DURACION' ).AsString := '1 Minutos';
                       FieldByName( 'STATUSTASK' ).AsInteger := 0;
                       Post;
                       First;
                  end;
                  XMLTools.XMLBuildDataSet( FPasos, 'PROCESOPASOS' );
                  Active := False;
             end;
          finally
                 FreeAndNil( FPasos );
          end;
     end;
end;

procedure TWFPasoProceso.LoadDummyValues;
begin
     inherited LoadDummyValues;
     IDModelo := Mensaje.Proceso.IDModelo;
     IDProceso := Mensaje.IDProceso;
     Orden := Mensaje.Orden;
     IDAccion := DUMMY_ACTION;
     Inicio := Now;
     Final := Now;
     Nombre := 'Paso De Prueba';
     Siguiente := 2;
     Status := eWFPasoProcesoStatus( 0 );
     Usuario := DUMMY_USER;
end;

procedure TWFPasoProceso.PropiedadesLeer(Datos: TZetaCursor);
begin
     inherited PropiedadesLeer( Datos );
     IDModelo := Mensaje.Proceso.IDModelo;
     IDProceso := Mensaje.IDProceso;
     Orden := Mensaje.Orden;
     with Datos do
     begin
          IDAccion := FieldByName( 'WA_CODIGO' ).AsString;
          Inicio := FieldByName( 'WS_FEC_INI' ).AsDateTime;
          Final := FieldByName( 'WS_FEC_FIN' ).AsDateTime;
          Nombre := FieldByName( 'WS_NOMBRE' ).AsString;
          Siguiente := FieldByName( 'WS_NEXT' ).AsInteger;
          Status := eWFPasoProcesoStatus( FieldByName( 'WS_STATUS' ).AsInteger );
          Usuario := FieldByName( 'WS_USR_INI' ).AsInteger;
     end;
end;

procedure TWFPasoProceso.PropiedadesEscribir;
begin
     with oZetaProvider do
     begin
          { Llave Primaria }
          ParamAsInteger( FUpdateStep, 'WP_FOLIO', IDProceso );
          ParamAsInteger( FUpdateStep, 'WE_ORDEN', Orden );
          { Datos }
          ParamAsDateTime( FUpdateStep, 'WS_FEC_INI', Inicio );
          ParamAsDateTime( FUpdateStep, 'WS_FEC_FIN', Final );
          ParamAsInteger( FUpdateStep, 'WS_NEXT', Siguiente );
          ParamAsInteger( FUpdateStep, 'WS_STATUS', Ord( Status ) );
          ParamAsInteger( FUpdateStep, 'WS_USR_INI', Usuario );
          EmpiezaTransaccion;
          try
             Ejecuta( FUpdateStep );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     RollBackTransaccion;
                end;
          end;
     end;
end;

procedure TWFPasoProceso.Activar;
begin
     Status := ppActivo;
     Usuario := Mensaje.UsuarioInicial;
     Inicio := Mensaje.Fecha;
     Final := Mensaje.Fecha;
     PropiedadesEscribir;
end;

procedure TWFPasoProceso.Delegar;
begin
     Final := Mensaje.Fecha;
     PropiedadesEscribir;
end;

procedure TWFPasoProceso.Terminar( const eStatus: eWFPasoProcesoStatus );
begin
     Status := eStatus;
     Final := Mensaje.Fecha;
     PropiedadesEscribir;
end;

procedure TWFPasoProceso.ActivarSiguientePaso;
begin
     Mensaje.EnviaMensaje( IDProceso, Siguiente, Usuario, Mensaje.UsuarioFinal, MSG_ACTIVAR );
end;

procedure TWFPasoProceso.Notificar( Aviso: TAviso );
begin
     Buzon.Notificar( Aviso, Self );
end;

procedure TWFPasoProceso.Notificar;
begin
     {case Status of
          ppActivo: Notificar( Mensaje.PasoModelo.AvisoActivacion );
          ppTerminado: Notificar( Mensaje.PasoModelo.AvisoTerminacion );
     end;}
end;

procedure TWFPasoProceso.Notificar( TipoNotificaciones : ListaNotificaCuando);
begin
     Notificar( TipoNotificaciones, Mensaje.Tarea);
end;

procedure TWFPasoProceso.Notificar( TipoNotificaciones : ListaNotificaCuando; Tarea : TWFTarea);
const
     Q_FECHA_ACTUAl : String = 'Select GETDATE() as Fecha';
var
   i, iMinute : Integer;
   FEnviaNotificacion, FGetNotificaciones, FGetFechaActual : TZetaCursor;
   sFiltro : String;
   dFecha : TDateTime;
   FNotificacion : TWFNotificacion;
begin
     sFiltro := VACIO;
     for i := Low(TipoNotificaciones) to High(TipoNotificaciones) do
     begin
         sFiltro := sFiltro + IntToStr (Ord ( TipoNotificaciones[i] ) ) + ',' ;
     end;
     sFiltro := ZetaCommonTools.StrLeft( sFiltro, Length(sFiltro) - 1 );
     with oZetaProvider do
     begin
          try
              FGetFechaActual := CreateQuery(Q_FECHA_ACTUAL);
              FGetNotificaciones := CreateQuery( Format ( Q_NOTIFICACIONES_PASO , [EntreComillas( IdModelo ), Orden, sFiltro]) );
              FEnviaNotificacion := CreateQuery( Q_ENVIA_NOTIFICACION );
              FNotificacion := TWFNotificacion.Create( Mensaje );
              with FGetNotificaciones do
              begin
                   Active := TRUE;
                   While ( Not EOF ) do
                   begin
                        iMinute := 0;
                        with FNotificacion do
                        begin
                             PropiedadesLeer( FGetNotificaciones );
                             case Accion of
                                 nqAutorizar:  dFecha := Tarea.Inicio;
                                 else
                                 begin
                                      with FGetFechaActual do
                                      begin
                                           Active := True;
                                           dFecha := FieldByName('Fecha').AsDateTime;
                                           Active := False;
                                      end;
                                 end;
                             end;
                             Case Cuando of
                                  ncDespuesIniciar: iMinute := Tiempo;
                                  ncAntesLimite:    iMinute := Mensaje.PasoModelo.Vencimiento - Tiempo;
                                  ncDespuesLimite:  iMinute := Mensaje.PasoModelo.Vencimiento + Tiempo;
                             end;
                             dFecha := IncMinute(dFecha, iMinute);
                             ParamAsGUID( FEnviaNotificacion, 'WT_GUID', Tarea.Codigo);
                             ParamAsInteger( FEnviaNotificacion, 'WY_FOLIO', Folio );
                             ParamAsDateTime( FEnviaNotificacion, 'WN_FECHA', dFecha );
                             ParamAsInteger( FEnviaNotificacion, 'WN_VECES', 0);
                             EmpiezaTransaccion;
                             try
                                Ejecuta(FEnviaNotificacion);
                                TerminaTransaccion( TRUE );
                             except
                                   on Error: Exception do
                                   begin
                                        RollBackTransaccion;
                                   end;
                             end;
                             Next;
                        end;
                   end;
                   Active := FALSE;
              end;
          finally
                 FreeAndNil( FEnviaNotificacion );
                 FreeAndNil( FGetNotificaciones );
                 FreeAndNil( FGetFechaActual );
                 FreeAndNil( FNotificacion );
          end;
     end;
end;

function TWFPasoProceso.GetTema: String;
begin
     case Status of
          ppActivo: Result := Format( 'Inici� Paso # %d Proceso %s', [ Orden, Proceso.Nombre ] );
          ppTerminado: Result := Format( 'Termin� Paso # %d Proceso %s', [ Orden, Proceso.Nombre ] );
          ppCancelado: Result := Format( 'CANCELACION Paso # %d Proceso %s', [ Orden, Proceso.Nombre ] );
     else
         Result := VACIO;
     end;
end;

function TWFPasoProceso.GetBody: String;
var
   sXSLFile, sTexto: String;
begin
     with Mensaje.PasoModelo do
     begin
          case Self.Status of
               ppActivo: sXSLFile := XSLActivo;
               ppTerminado: sXSLFile := XSLTerminado;
               ppCancelado: sXSLFile := XSLCancelado;
               ppSuspendido: sXSLFile := XSLSuspendido;
          else
              sXSLFile := VACIO;
          end;
     end;
     if XMLTools.XMLTransform( Mensaje.GetAllXML, sXSLFile, sTexto ) then
     begin
          Result := sTexto;
     end
     else
     begin
          case Status of
               ppActivo: Result := Format( 'Inici� Paso # %d de %d del Proceso %s # %d', [ Orden, Proceso.Pasos, Proceso.Nombre, IDProceso ] );
               ppTerminado: Result := Format( 'Termin� Paso # %d de %d del Proceso %s # %d', [ Orden, Proceso.Pasos, Proceso.Nombre, IDProceso ] );
               ppCancelado: Result := Format( 'CANCELACION Del Paso # %d de %d del Proceso %s # %d', [ Orden, Proceso.Pasos, Proceso.Nombre, IDProceso ] );
          else
              Result := VACIO;
          end;
     end;
end;

{ ********** TWFBaseModelo ********* }

procedure TWFBaseModelo.Start;
begin
     FXSLPath := ZetaCommonTools.VerificaDir( Registry.DirectorioXSL );
end;

procedure TWFBaseModelo.Stop;
begin
end;

function TWFBaseModelo.AddXSLPath( const sFileName: String ): String;
begin
     Result := sFileName;
     if ZetaCommonTools.StrVacio( SysUtils.ExtractFilePath( sFileName ) ) then
     begin
          Result := FXSLPath + Result;
     end;
end;

{ ******* TWFModelo ******** }

procedure TWFModelo.SetXSLActivo(const Value: String);
begin
     FXSLActivo := AddXSLPath( Value );
end;

procedure TWFModelo.SetXSLCancelado(const Value: String);
begin
     FXSLCancelado := AddXSLPath( Value );
end;

procedure TWFModelo.SetXSLTerminado(const Value: String);
begin
     FXSLTerminado := AddXSLPath( Value );
end;

procedure TWFModelo.SetXSLSuspendido(const Value: string);
begin
     FXSLSuspendido := AddXSLPath( Value );
end;

function TWFModelo.URLAutorizacion(Task: TWFTarea): String;
begin
     Result := Format( '%s?MODELO=%s&&ID=%s', [ PaginaAutorizacion, Codigo, Task.CodigoTxt ] );
end;

function TWFModelo.URLExaminar: String;
begin
     Result := Format( '%s?MODELO=%s&&ID=%d', [ PaginaExamen, Codigo, Mensaje.Proceso.Folio ] );
end;

function TWFModelo.URLSolicitud: String;
begin
     Result := Format( '%s?MODELO=%s', [ PaginaSolicitud, Codigo ] );
end;

procedure TWFModelo.BuildXML;
begin
     with XMLTools do
     begin
          WriteStartElement( 'MODELO' );
          try
             WriteValueString( 'CODIGO', Codigo );
             WriteValueString( 'DESCRIPCION', Descripcion );
             WriteValueString( 'NOMBRE', Nombre );
             WriteValueInteger( 'PASOS', Pasos );
             WriteValueString( 'PAGINAAPROBAR', PaginaAutorizacion );
             WriteValueString( 'PAGINAEXAMINAR', PaginaExamen );
             WriteValueString( 'PAGINASOLICITAR', PaginaSolicitud );
          finally
                 WriteEndElement;
          end;
     end;
end;

procedure TWFModelo.LoadDummyValues;
var
  oTAvisoActivacion : TAviso;
  oTAvisoTerminacion : TAviso;
  oTAvisoCancelacion : TAviso;
  oTAvisoSuspension : TAviso;
begin
     inherited LoadDummyValues;
     Codigo := DUMMY_MODEL;
     Descripcion := 'Modelo De Prueba';
     Nombre := 'Prueba';
     Pasos := 1;
     PaginaAutorizacion := 'http://www.prueba.de.modelo.com/Autorizar.htm';
     PaginaExamen := 'http://www.prueba.de.modelo.com/Examinar.htm';
     PaginaSolicitud := 'http://www.prueba.de.modelo.com/Solicitar.htm';
     AttachmentPath := 'C:\Temp\Documentos\';
     AttachmentURL := 'http://www.prueba.de.modelo.com/Documentos/';


     oTAvisoActivacion := AvisoActivacion;
     with AvisoActivacion do
     begin
          oTAvisoActivacion.Tipo := eWFNotificacion( 0 );
          oTAvisoActivacion.Correo := 'prueba@workflow.com';
     end;
     AvisoActivacion := oTAvisoActivacion;
     FreeAndNil(oTAvisoActivacion);

     oTAvisoTerminacion  := AvisoTerminacion;
     with AvisoTerminacion do
     begin
          oTAvisoTerminacion.Tipo := eWFNotificacion( 0 );
          oTAvisoTerminacion.Correo := 'prueba@workflow.com';
     end;
     AvisoTerminacion := oTAvisoTerminacion;
     FreeAndNil(oTAvisoTerminacion);

     oTAvisoCancelacion := AvisoCancelacion;
     with AvisoCancelacion do
     begin
          oTAvisoCancelacion.Tipo := eWFNotificacion( 0 );
          oTAvisoCancelacion.Correo := 'prueba@workflow.com';
     end;
     AvisoCancelacion := oTAvisoCancelacion;
     FreeAndNil(oTAvisoCancelacion);

     oTAvisoSuspension := AvisoSuspension;
     with AvisoSuspension do
     begin
          oTAvisoSuspension.Tipo := eWFNotificacion( 0 );
          oTAvisoSuspension.Correo := 'prueba@workflow.com';
     end;
     AvisoSuspension := oTAvisoSuspension;
     FreeAndNil(oTAvisoSuspension);


     with Registry do
     begin
          XSLActivo := ModeloXSLActivo;
          XSLCancelado := ModeloXSLCancelado;
          XSLTerminado := ModeloXSLTerminado;
          XSLSuspendido:= ModeloXSLSuspendido;
     end;
end;

procedure TWFModelo.PropiedadesLeer(Datos: TZetaCursor);
var
  oTAvisoActivacion : TAviso;
  oTAvisoTerminacion : TAviso;
  oTAvisoCancelacion : TAviso;
  oTAvisoSuspension : TAviso;
begin
     inherited PropiedadesLeer( Datos );
     with Datos do
     begin
          Codigo := FieldByName( 'WM_CODIGO' ).AsString;
          Descripcion := FieldByName( 'WM_DESCRIP' ).AsString;
          Nombre := FieldByName( 'WM_NOMBRE' ).AsString;
          Pasos := FieldByName( 'WM_PASOS' ).AsInteger;
          PaginaAutorizacion := FieldByName( 'WM_URL_OK' ).AsString;
          PaginaExamen := FieldByName( 'WM_URL_EXA' ).AsString;
          PaginaSolicitud := FieldByName( 'WM_URL' ).AsString;

          oTAvisoActivacion := AvisoActivacion;
          with AvisoActivacion do
          begin
               oTAvisoActivacion.Tipo := eWFNotificacion( FieldByName( 'WM_TIPNOT1' ).AsInteger );
               oTAvisoActivacion.Correo := FieldByName( 'WM_USRNOT1' ).AsString;
          end;
          AvisoActivacion := oTAvisoActivacion;
          FreeAndNil(oTAvisoActivacion);

          oTAvisoTerminacion  := AvisoTerminacion;
          with AvisoTerminacion do
          begin
               oTAvisoTerminacion.Tipo := eWFNotificacion( FieldByName( 'WM_TIPNOT2' ).AsInteger );
               oTAvisoTerminacion.Correo := FieldByName( 'WM_USRNOT2' ).AsString;
          end;
          AvisoTerminacion := oTAvisoTerminacion;
          FreeAndNil(oTAvisoTerminacion);


          oTAvisoCancelacion := AvisoCancelacion;
          with AvisoCancelacion do
          begin
               oTAvisoCancelacion.Tipo := eWFNotificacion( FieldByName( 'WM_TIPNOT3' ).AsInteger );
               oTAvisoCancelacion.Correo := FieldByName( 'WM_USRNOT3' ).AsString;
          end;
          AvisoCancelacion := oTAvisoCancelacion;
          FreeAndNil(oTAvisoCancelacion);

     oTAvisoSuspension := AvisoSuspension;
          with AvisoSuspension do
          begin
               oTAvisoSuspension.Tipo := nvNinguno; //eWFNotificacion( FieldByName( 'WM_TIPNOT4' ).AsInteger );
               oTAvisoSuspension.Correo := Vacio; //FieldByName( 'WM_USRNOT4' ).AsString;
          end;
     AvisoSuspension := oTAvisoSuspension;
     FreeAndNil(oTAvisoSuspension);



          with Registry do
          begin
               XSLActivo := UseDefaultIfEmpty( FieldByName( 'WM_XSL_ACT' ).AsString, ModeloXSLActivo );
               XSLCancelado := UseDefaultIfEmpty( FieldByName( 'WM_XSL_CAN' ).AsString, ModeloXSLCancelado );
               XSLTerminado := UseDefaultIfEmpty( FieldByName( 'WM_XSL_TER' ).AsString, ModeloXSLTerminado );
               XSLSuspendido := UseDefaultIfEmpty( VACIO, ModeloXSLSuspendido );//UseDefaultIfEmpty( FieldByName( 'WM_XSL_SUS' ).AsString, ModeloXSLSuspension );
          end;
          with oZetaProvider do
          begin
               AttachmentPath := UseDefaultIfEmpty( '', GetGlobalString( K_WORKFLOW_FILE_PATH ) );
               AttachmentURL := UseDefaultIfEmpty( '', GetGlobalString( K_WORKFLOW_FILE_URL ) );
          end;
     end;
end;

{ ******* TWFPasoModelo ****** }

procedure TWFPasoModelo.SetXSLActivo(const Value: String);
begin
     FXSLActivo := AddXSLPath( Value );
end;

procedure TWFPasoModelo.SetXSLCancelado(const Value: String);
begin
     FXSLCancelado := AddXSLPath( Value );
end;

procedure TWFPasoModelo.SetXSLTarea(const Value: String);
begin
     FXSLTarea := AddXSLPath( Value );
end;

procedure TWFPasoModelo.SetXSLTerminado(const Value: String);
begin
     FXSLTerminado := AddXSLPath( Value );
end;

procedure TWFPasoModelo.SetXSLSuspendido(const Value: String);
begin
     FXSLSuspendido := AddXSLPath( Value );
end;

procedure TWFPasoModelo.BuildXML;
begin
     with XMLTools do
     begin
          WriteStartElement( 'PASOMODELO' );
          try
             WriteValueString( 'CODIGO', Codigo );
             WriteValueInteger( 'ORDEN', Orden );
             WriteValueString( 'ACCION', IDAccion );
             WriteValueString( 'DESCRIPCION', Descripcion );
             WriteValueString( 'NOMBRE', Nombre );
             WriteValueInteger( 'LIMITE', Vencimiento);
             WriteValueString( 'PAGINAAPROBAR', PaginaAutorizacion);
          finally
                 WriteEndElement;
          end;
     end;
end;

procedure TWFPasoModelo.LoadDummyValues;
var
  oTAvisoActivacion : TAviso;
  oTAvisoTerminacion : TAviso;
  oTAvisoCancelacion : TAviso;
  oTAvisoSuspension : TAviso;
  oTDestino : TDestino;

begin
     inherited LoadDummyValues;
     Codigo := Mensaje.Modelo.Codigo;
     Orden := Mensaje.Orden;
     IDAccion := Mensaje.PasoProceso.IDAccion;
     Descripcion := 'Paso De Modelo De Prueba';
     Nombre := 'Paso Prueba';
     Vencimiento := 10;
     PaginaAutorizacion :='http://www.prueba.de.paso.modelo.com/Autorizar.htm';;


          oTAvisoActivacion := AvisoActivacion;
     with AvisoActivacion do
     begin
          oTAvisoActivacion.Tipo := eWFNotificacion( 0 );
          oTAvisoActivacion.Correo := 'prueba@workflow.com';
     end;
          AvisoActivacion := oTAvisoActivacion;
          FreeAndNil(oTAvisoActivacion);

     oTAvisoTerminacion  := AvisoTerminacion;
     with AvisoTerminacion do
     begin
          oTAvisoTerminacion.Tipo := eWFNotificacion( 0 );
          oTAvisoTerminacion.Correo := 'prueba@workflow.com';
     end;
     AvisoTerminacion := oTAvisoTerminacion;
     FreeAndNil(oTAvisoTerminacion);


     oTAvisoCancelacion := AvisoCancelacion;
     with AvisoCancelacion do
     begin
          oTAvisoCancelacion.Tipo := eWFNotificacion( 0 );
          oTAvisoCancelacion.Correo := 'prueba@workflow.com';
     end;
     AvisoCancelacion := oTAvisoCancelacion;
     FreeAndNil(oTAvisoCancelacion);


     oTAvisoSuspension := AvisoSuspension;
     with AvisoSuspension do
     begin
          oTAvisoSuspension.Tipo := eWFNotificacion( 0 );
          oTAvisoSuspension.Correo := 'prueba@workflow.com';
     end;
     AvisoSuspension := oTAvisoSuspension;
     FreeAndNil(oTAvisoSuspension);


     oTDestino := Destino;
     with Destino do
     begin
          oTDestino.Tipo := eWFNotificacion( 0 );
          oTDestino.Correo := 'prueba@workflow.com';
     end;
     Destino:= oTDestino;
     FreeAndNil(oTDestino);


     with Registry do
     begin
          XSLActivo := PasoModeloXSLActivo;
          XSLCancelado := PasoModeloXSLCancelado;
          XSLTerminado := PasoModeloXSLTerminado;
          XSLSuspendido := PasoModeloXSLSuspendido;
     end;
end;

procedure TWFPasoModelo.PropiedadesLeer(Datos: TZetaCursor);
var
  oTAvisoActivacion : TAviso;
  oTAvisoTerminacion : TAviso;
  oTAvisoCancelacion : TAviso;
  oTAvisoSuspension : TAviso;
   oTDestino : TDestino;
begin
     inherited PropiedadesLeer( Datos );
     Codigo := Mensaje.Modelo.Codigo;
     Orden := Mensaje.Orden;
     IDAccion := Mensaje.PasoProceso.IDAccion;
     with Datos do
     begin
          Descripcion := FieldByName( 'WE_DESCRIP' ).AsString;
          Nombre := FieldByName( 'WE_NOMBRE' ).AsString;
          Vencimiento := FieldByName( 'WE_LIMITE' ).AsInteger;
          PaginaAutorizacion := FieldByName( 'WE_URL_OK' ).AsString;


          oTAvisoActivacion := AvisoActivacion;
          with AvisoActivacion do
          begin
               oTAvisoActivacion.Tipo := eWFNotificacion( FieldByName( 'WE_TIPNOT1' ).AsInteger );
               oTAvisoActivacion.Correo := FieldByName( 'WE_USRNOT1' ).AsString;
          end;
           AvisoActivacion := oTAvisoActivacion;
          FreeAndNil(oTAvisoActivacion);



     oTAvisoTerminacion  := AvisoTerminacion;
          with AvisoTerminacion do
          begin
               oTAvisoTerminacion.Tipo := eWFNotificacion( FieldByName( 'WE_TIPNOT2' ).AsInteger );
               oTAvisoTerminacion.Correo := FieldByName( 'WE_USRNOT2' ).AsString;
          end;
     AvisoTerminacion := oTAvisoTerminacion;
     FreeAndNil(oTAvisoTerminacion);


     oTAvisoCancelacion := AvisoCancelacion;
          with AvisoCancelacion do
          begin
               oTAvisoCancelacion.Tipo := eWFNotificacion( FieldByName( 'WE_TIPNOT3' ).AsInteger );
               oTAvisoCancelacion.Correo := FieldByName( 'WE_USRNOT3' ).AsString;
          end;
     AvisoCancelacion := oTAvisoCancelacion;
     FreeAndNil(oTAvisoCancelacion);

     oTAvisoSuspension := AvisoSuspension;
          with AvisoSuspension do
          begin
               oTAvisoSuspension.Tipo := nvNinguno; //eWFNotificacion( FieldByName( 'WE_TIPNOT4' ).AsInteger );
               oTAvisoSuspension.Correo := Vacio;//FieldByName( 'WE_USRNOT4' ).AsString;
          end;
     AvisoSuspension := oTAvisoSuspension;
     FreeAndNil(oTAvisoSuspension);


          oTDestino := Destino;
          with Destino do
          begin
               oTDestino.Tipo := eWFNotificacion( FieldByName( 'WE_TIPDEST' ).AsInteger );
               oTDestino.Correo := FieldByName( 'WE_USRDEST' ).AsString;
          end;
     Destino:= oTDestino;
     FreeAndNil(oTDestino);


          with Registry do
          begin
               XSLTarea := UseDefaultIfEmpty( FieldByName( 'WE_XSL_TAR' ).AsString, TareaXSL );
               XSLActivo := UseDefaultIfEmpty( FieldByName( 'WE_XSL_ACT' ).AsString, PasoModeloXSLActivo );
               XSLCancelado := UseDefaultIfEmpty( FieldByName( 'WE_XSL_CAN' ).AsString, PasoModeloXSLCancelado );
               XSLTerminado := UseDefaultIfEmpty( FieldByName( 'WE_XSL_TER' ).AsString, PasoModeloXSLTerminado );
               XSLSuspendido := UseDefaultIfEmpty( VACIO, PasoModeloXSLSuspendido );// UseDefaultIfEmpty( FieldByName( 'WE_XSL_SUS' ).AsString, PasoModeloXSLSuspension );
          end;
     end;
end;

{ ******** TWFNotificacion ****** }

procedure TWFNotificacion.BuildXML;
begin
     with XMLTools do
     begin
          WriteStartElement( 'NOTIFICACION' );
          try
             WriteValueString( 'CODIGO', Codigo );
             WriteValueInteger( 'ORDEN', Orden );
             WriteValueInteger( 'FOLIO', Folio );
             WriteValueString( 'CUANDO', ObtieneElemento(lfWFNotificacionCuando, Ord(Cuando)));
             WriteValueString( 'QUE', ObtieneElemento(lfWFNotificacionQue, Ord(Accion)) );
             WriteValueInteger( 'TIEMPO', Tiempo );
             WriteValueInteger( 'CADA', Cada );
             WriteValueInteger( 'VECES', Veces );
             WriteValueString( 'NOTAS', Texto );
          finally
                 WriteEndElement;
          end;
     end;
end;

procedure TWFNotificacion.LoadDummyValues;
begin
     inherited LoadDummyValues;
     Codigo := Mensaje.Modelo.Codigo;
     Orden := Mensaje.Orden;
     Folio := Mensaje.IdNotificacion;
     Cuando := ncDespuesIniciar;
     Accion := nqNotificar;
     Tiempo := 1;
     Cada := 2;
     Veces := 2;
     Texto := 'Notificacion de paso de prueba';
end;

procedure TWFNotificacion.PropiedadesLeer(Datos: TZetaCursor);
var
   oTDestino : TDestino;
begin
     inherited PropiedadesLeer( Datos );
     with Datos do
     begin
          Codigo := FieldByName( 'WM_CODIGO' ).AsString;
          Orden := FieldByName( 'WE_ORDEN' ).AsInteger;
          Folio := FieldByName( 'WY_FOLIO').AsInteger;
          Cuando:= eWFNotificacionCuando( FieldByName( 'WY_CUANDO' ).AsInteger );
          Accion:= eWFNotificacionQue (FieldByName( 'WY_QUE' ).AsInteger);
          Tiempo:= FieldByName( 'WY_TIEMPO' ).AsInteger;
          Cada:= FieldByName( 'WY_CADA' ).AsInteger;
          Veces:= FieldByName( 'WY_VECES' ).AsInteger;
          Texto:= FieldByName( 'WY_TEXTO' ).AsString;

          oTDestino := Destino;
          with Destino do
          begin
               oTDestino.Tipo := eWFNotificacion( FieldByName( 'WY_QUIEN' ).AsInteger );
               oTDestino.Correo := FieldByName( 'WY_USR_NOT' ).AsString;
          end;
     Destino:= oTDestino;
     FreeAndNil(oTDestino);

          with Registry do
          begin
               case Cuando of
                    ncIniciar: XSL := UseDefaultIfEmpty( FieldByName( 'WY_XSL' ).AsString, PasoModeloXSLActivo );
                    ncCancelar: XSL := UseDefaultIfEmpty( FieldByName( 'WY_XSL' ).AsString, PasoModeloXSLCancelado );
                    ncTerminar: XSL := UseDefaultIfEmpty( FieldByName( 'WY_XSL' ).AsString, PasoModeloXSLTerminado );
                    ncModificar: XSL := UseDefaultIfEmpty( FieldByName( 'WY_XSL' ).AsString, PasoModeloXSLModificar );
                    ncDespuesIniciar: XSL := UseDefaultIfEmpty( FieldByName( 'WY_XSL' ).AsString, PasoModeloXSLDespuesIniciar );
                    ncAntesLimite: XSL := UseDefaultIfEmpty( FieldByName( 'WY_XSL' ).AsString, PasoModeloXSLAntesLimite );
                    ncDespuesLimite: XSL := UseDefaultIfEmpty( FieldByName( 'WY_XSL' ).AsString, PasoModeloXSLDespuesLimite );
               end;
          end;
     end;
end;

{ ******** TWFConexion ****** }

procedure TWFConexion.BuildXML;
begin
     with XMLTools do
     begin
          WriteStartElement( 'CONEXION' );
          try
             WriteValueInteger( 'FOLIO', Folio );
             WriteValueString( 'NOMBRE', Nombre );
             WriteValueString( 'URL', URL );
             WriteValueString( 'AUTENTICACION', ObtieneElemento(lfWFAutenticacionWebService, Ord(WSAutenticacion)));
             WriteValueString( 'CODIFICACION', ObtieneElemento(lfWFCodificacionWebService, Ord(WSCodificacion)));
          finally
                 WriteEndElement;
          end;
     end;
end;

procedure TWFConexion.LoadDummyValues;
begin
     inherited LoadDummyValues;
     Folio := 1;
     Nombre := 'ConexionPrueba';
     URL := 'http://www.conexion.de.prueba.com/WebService';
     WSAutenticacion := auDefault;
     WSCodificacion := coWebServiceDocument;
end;

procedure TWFConexion.PropiedadesLeer(Datos: TZetaCursor);
begin
     inherited PropiedadesLeer( Datos );
     with Datos do
     begin
          Folio := FieldByName( 'WX_FOLIO' ).AsInteger;
          Nombre := FieldByName( 'WX_NOMBRE' ).AsString;
          URL := FieldByName( 'WX_URL').AsString;
          WSAutenticacion := eWFAutenticacionWebService( FieldByName( 'WX_TIP_AUT' ).AsInteger );
          WSCodificacion := eWFCodificacionWebService( FieldByName( 'WX_TIP_ENC' ).AsInteger);
          Usuario := FieldByName( 'WX_USUARIO' ).AsString;
          Password:= FieldByName( 'WX_PASSWRD' ).AsString;
     end;
end;

procedure TWFConexion.Start;
begin
     inherited;
     //Nada
end;

procedure TWFConexion.Stop;
begin
  	 inherited;
     //Nada
end;


{ ******** TWFBaseAccion ****** }

constructor TWFBaseAccion.Create( Mensaje: TWFMensaje );
begin
     FMensaje := Mensaje;
     FTareaNueva := TWFTarea.Create( Mensaje );
end;

destructor TWFBaseAccion.Destroy;
begin
     FreeAndNil( FTareaNueva );
     inherited Destroy;
end;

procedure TWFBaseAccion.Start;
begin
     //DoUpdateStatus( Format( 'Action %s - Starting', [ Self.ClassName ] ) );
     if Assigned( FTareaNueva ) then
        FTareaNueva.Start;
     //DoUpdateStatus( Format( 'Action %s - Started', [ Self.ClassName ] ) );
end;

procedure TWFBaseAccion.Stop;
begin
     //DoUpdateStatus( Format( 'Action %s - Stopping', [ Self.ClassName ] ) );
     if Assigned( FTareaNueva ) then
        FTareaNueva.Stop;
     //DoUpdateStatus( Format( 'Action %s - Stopped', [ Self.ClassName ] ) );
end;

function TWFBaseAccion.GetProvider: TdmZetaWorkFlowProvider;
begin
     Result := Self.Mensaje.oZetaProvider;
end;

function TWFBaseAccion.GetModelo: TWFModelo;
begin
     Result := Mensaje.Modelo;
end;

function TWFBaseAccion.GetPasoModelo: TWFPasoModelo;
begin
     Result := Mensaje.PasoModelo;
end;

function TWFBaseAccion.GetPasoProceso: TWFPasoProceso;
begin
     Result := Mensaje.PasoProceso;
end;

function TWFBaseAccion.GetProceso: TWFProceso;
begin
     Result := Mensaje.Proceso;
end;

function TWFBaseAccion.GetTarea: TWFTarea;
begin
     Result := Mensaje.Tarea;
end;

{
function TWFBaseAccion.CrearTarea( Mensaje: TWFMensaje ): TWFTarea;
begin
     if not Assigned( FTareaNueva ) then
     begin
          FTareaNueva := TWFTarea.Create( Mensaje );
          if Assigned( FTareaNueva ) then
             FTareaNueva.Start;
     end;
     Result := FTareaNueva;
end;
}

procedure TWFBaseAccion.DoUpdateStatus(const sTexto: String);
begin
     Mensaje.DoUpdateStatus( sTexto );
end;

function TWFBaseAccion.PreProcesa( var sMensaje: String ): Boolean;
 var
    sRespuesta : string;
begin
     sRespuesta := Mensaje.Mensaje;
     if ( sRespuesta = MSG_CANCELAR ) then
        Result := ProcesaCancelar( sMensaje )
     else
         if ( sRespuesta = MSG_SUSPENDER ) then
            Result := ProcesaSuspender( sMensaje )
         else
             if ( sRespuesta = MSG_REINICIAR ) then
		Result := ProcesaReiniciar( sMensaje )
             else
                 if ( sRespuesta = MSG_NOTIFI ) then
                    Result := ProcesaNotificar( sMensaje )
                 else
                     if ( sRespuesta = MSG_AUTORIZAR) then
                        Result := ProcesaAutorizar( sMensaje )
                     else
                         Result := Procesa( sMensaje );
end;

function TWFBaseAccion.ProcesaCancelar( var sMensaje: String ): Boolean;
const
     K_TEMA = 'Cancelaci�n de %s # %d - %s';
     K_MENSAJE = 'El proceso de %1:s # %2:d '+
                 'ha sido cancelado'+
                 '%0:s'+
                 'Si desea examinarlo, por favor entre a %3:s'+
                 '%0:s'+
                 '� Gracias !';
var
   sProceso, sTexto: String;
begin
     sMensaje := VACIO;
     Result := False;
     DoUpdateStatus( Format( 'Action %s - Process %d Cancelation', [ Self.ClassName, Proceso.Folio ] ) );
     try
        if not ( Proceso.Status in [ prTerminado, prCancelado ] ) then
        begin
             sProceso := Mensaje.Modelo.Nombre;
             { Cancelar El Proceso }
             Self.Proceso.Cancelar;
             { Notificar Sobre La Cancelaci�n del Proceso }
             //Self.PasoProceso.Notificar;
             {
             Notificar AL Usuario Responsable de la tarea pendiente
             sobre la cancelaci�n del Proceso
             }
             with Tarea do
             begin
	          if PorProcesar then
                  begin
                       with Buzon do
                       begin
                            GetRemitenteData( Mensaje.UsuarioInicial ); //?
                            GetDestinatarioData( Mensaje.UsuarioFinal ); //?
                            if not XMLTools.XMLTransform( Mensaje.GetAllXML( Tarea ), PasoModelo.XSLCancelado, sTexto ) then
                            begin
                                 sTexto := Format( K_MENSAJE, [ CR_LF, sProceso, Proceso.Folio, Modelo.URLExaminar ] );
                            end;
                            EnviarNota( Format( K_TEMA, [ sProceso, Proceso.Folio, Proceso.UsuarioNombre ] ), sTexto );
                       end;
                  end;
             end;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                sMensaje := Error.Message;
           end;
     end;
     DoUpdateStatus( Format( 'Action %s - Process %d Canceled', [ Self.ClassName, Proceso.Folio ] ) );
end;

function TWFBaseAccion.ProcesaSuspender( var sMensaje: String ): Boolean;
const
     K_TEMA = 'Suspender de %s # %d - %s';
     K_MENSAJE = 'El proceso de %1:s # %2:d '+
                 'ha sido suspendido'+
                 '%0:s'+
                 'Si desea examinarlo, por favor entre a %3:s'+
                 '%0:s'+
                 '� Gracias !';
var
   sProceso, sTexto: String;
begin
     sMensaje := VACIO;
     Result := False;
     DoUpdateStatus( Format( 'Action %s - Process %d Suspended', [ Self.ClassName, Proceso.Folio ] ) );
     try
        if Proceso.Status in [ prActivo ]  then
        begin
             sProceso := Mensaje.Modelo.Nombre;
             { Activar El Proceso }
             Self.Proceso.Suspender;
             //{ Notificar Sobre La Activaci�n del Proceso }
             //Self.PasoProceso.Notificar;
             {
             Notificar AL Usuario Responsable de la tarea pendiente
             sobre la activacion del Proceso
             }
             with Tarea do
             begin
	          if PorProcesar then
                  begin
                       with Buzon do
                       begin
                            GetRemitenteData( Mensaje.UsuarioInicial ); //?
                            GetDestinatarioData( Mensaje.UsuarioFinal ); //?
                            //if not XMLTools.XMLTransform( Mensaje.GetAllXML( Tarea ), PasoModelo.XSLSuspendido, sTexto ) then
                            if not XMLTools.XMLTransform( Mensaje.GetAllXML( Tarea ), PasoModelo.XSLCancelado, sTexto ) then
                            begin
                                 sTexto := Format( K_MENSAJE, [ CR_LF, sProceso, Proceso.Folio, Modelo.URLExaminar ] );
                            end;
                            EnviarNota( Format( K_TEMA, [ sProceso, Proceso.Folio, Proceso.UsuarioNombre ] ), sTexto );
                       end;
                  end;
             end;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                sMensaje := Error.Message;
           end;
     end;
     DoUpdateStatus( Format( 'Action %s - Process %d Canceled', [ Self.ClassName, Proceso.Folio ] ) );

end;

function TWFBaseAccion.ProcesaReiniciar( var sMensaje: String ): Boolean;
const
     K_TEMA = 'Reiniciar de %s # %d - %s';
     K_MENSAJE = 'El proceso de %1:s # %2:d '+
                 'ha sido reiniciado'+
                 '%0:s'+
                 'Si desea examinarlo, por favor entre a %3:s'+
                 '%0:s'+
                 '� Gracias !';
var
   sProceso, sTexto: String;
begin
     sMensaje := VACIO;
     Result := False;
     DoUpdateStatus( Format( 'Action %s - Process %d Reinitiation', [ Self.ClassName, Proceso.Folio ] ) );
     try
        if Proceso.Status in [ prSuspendido ]  then
        begin
             sProceso := Mensaje.Modelo.Nombre;
             { Activar El Proceso }
             Self.Proceso.Activar;
             { Notificar Sobre La Activaci�n del Proceso }
             //Self.PasoProceso.Notificar;
             {
             Notificar AL Usuario Responsable de la tarea pendiente
             sobre la activacion del Proceso
             }
             with Tarea do
             begin
	          if PorProcesar then
                  begin
                       with Buzon do
                       begin
                            GetRemitenteData( Mensaje.UsuarioInicial ); //?
                            GetDestinatarioData( Mensaje.UsuarioFinal ); //?
                            if not XMLTools.XMLTransform( Mensaje.GetAllXML( Tarea ), PasoModelo.XSLActivo, sTexto ) then
                            begin
                                 sTexto := Format( K_MENSAJE, [ CR_LF, sProceso, Proceso.Folio, Modelo.URLExaminar ] );
                            end;
                            EnviarNota( Format( K_TEMA, [ sProceso, Proceso.Folio, Proceso.UsuarioNombre ] ), sTexto );
                       end;
                  end;
             end;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                sMensaje := Error.Message;
           end;
     end;
     DoUpdateStatus( Format( 'Action %s - Process %d Canceled', [ Self.ClassName, Proceso.Folio ] ) );
end;

procedure TWFBaseAccion.PasoActivar;
//var
   //lNotificar: Boolean;
begin
     with PasoProceso do
     begin
          DoUpdateStatus( Format( 'Action %s - Activating Step', [ Self.ClassName ] ) );
          Activar;
          //DoUpdateStatus( Format( 'Action %s - Notifying Step Activation', [ Self.ClassName ] ) );
          //Notificar( TipoNotificaciones );
     end;
     with Proceso do
     begin
          //lNotificar := ( Status = prInactivo );
          DoUpdateStatus( Format( 'Action %s - Activating Process', [ Self.ClassName ] ) );
          Activar;
          //DoUpdateStatus( Format( 'Action %s - Notifying Process Activation', [ Self.ClassName ] ) );
          //if lNotificar then
            // Notificar;
     end;
     DoUpdateStatus( Format( 'Action %s - Activated', [ Self.ClassName ] ) );
end;

procedure TWFBaseAccion.PasoDelegar;
//var
//   lNotificar: Boolean;
begin
     with PasoProceso do
     begin
          DoUpdateStatus( Format( 'Action %s - Delegating Step (Antes)', [ Self.ClassName ] ) );
          Delegar;
          DoUpdateStatus( Format( 'Action %s - Delegating Step (Despu�s)', [ Self.ClassName ] ) );
          //Notificar;
     end;
     with Proceso do
     begin
          //lNotificar := ( Status = prInactivo );
          DoUpdateStatus( Format( 'Action %s - Delegating Process (Antes)', [ Self.ClassName ] ) );
          Delegar;
          DoUpdateStatus( Format( 'Action %s - Delegating Process (Despu�s)', [ Self.ClassName ] ) );
     end;
     with Tarea do
     begin
          DoUpdateStatus( 'Antes de Tarea.Delegar' );
          Delegar;
          DoUpdateStatus( 'Despu�s de Tarea.Delegar' );
     end;
     {with Proceso do
     begin
          DoUpdateStatus( Format( 'Action %s - Notifying Process Delegation', [ Self.ClassName ] ) );
          if lNotificar then
             Notificar;
     end;}
end;

procedure TWFBaseAccion.PasoTerminar( const eStatus: eWFPasoProcesoStatus );
var
   //lNotificar : Boolean;
   TipoNotificaciones : ListaNotificaCuando;
begin
      SetLength (TipoNotificaciones, 1);
     { Marca el Paso como 'Terminado' }
     with PasoProceso do
     begin
          DoUpdateStatus( Format( 'Action %s - Terminating Step', [ Self.ClassName ] ) );
          Terminar( eStatus );
          case eStatus of
              ppTerminado : TipoNotificaciones[0] := ncTerminar;
              ppCancelado : TipoNotificaciones[0] := ncCancelar;
          end;
          DoUpdateStatus( Format( 'Action %s - Notifying Step Termination', [ Self.ClassName ] ) );
          Notificar( TipoNotificaciones );
     end;
     { Actualiza los datos del Proceso }
     with Proceso do
     begin
          DoUpdateStatus( Format( 'Action %s - Terminating Process', [ Self.ClassName ] ) );
          //lNotificar :=
          Terminar( GetProcesoStatus( eStatus ) );
          //DoUpdateStatus( Format( 'Action %s - Notifying Process Termination', [ Self.ClassName ] ) );
          //if lNotificar then
             //Notificar;
          { Si hay otro paso, lo activa envi�ndole un mensaje 'Activar' }
          DoUpdateStatus( Format( 'Action %s - Activating Next Step', [ Self.ClassName ] ) );
          if Activo then
             PasoProceso.ActivarSiguientePaso;
     end;
     DoUpdateStatus( Format( 'Action %s - Terminated', [ Self.ClassName ] ) );
end;

procedure TWFBaseAccion.ProcesaAceptar;
begin
     { Usa utiler�a de clase abstracta para marcar los status }
     PasoTerminar( ppTerminado );
     { Marca la tarea terminada }
     Tarea.Terminar( Mensaje.Notas );
end;

procedure TWFBaseAccion.ProcesaRechazar;
begin
     { Usa utiler�a de clase abstracta para marcar los status }
     PasoTerminar( ppCancelado );
     { Marca la tarea Cancelada }
     Tarea.Cancelar( Mensaje.Notas );
end;

function TWFBaseAccion.ProcesaNotificar( var sMensaje: String ): Boolean;
const
     K_TEMA = 'Notificaci�n de %s # %d - %s';
     K_MENSAJE = 'El proceso de %1:s # %2:d, paso %3:d '+
                 '%4:s'+
                 '%0:s'+
                 'Si desea examinarlo, por favor entre a %5:s'+
                 '%0:s'+
                 '� Gracias !';
     aMensajeNoti : array [ eWFNotificacionCuando ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'ha iniciado', 'ha terminado', 'ha sido cancelado', 'ha sido modificado', 'ha sido iniciado', 'esta por vencerse', 'se ha vencido' );
var
   sProceso, sTexto: String;
   FEnviaNotificacion: TZetaCursor;
   dFecha : TDateTime;
   lNotificar : boolean;
   Aviso: TAviso;
begin
     sMensaje := VACIO;
     Result := False;
     lNotificar :=(Mensaje.Notificacion.Cuando in [ncTerminar, ncCancelar] );
     DoUpdateStatus( Format( 'Action %s - Process %d Notifying', [ Self.ClassName, Proceso.Folio ] ) );
     try
        if ( ( Proceso.Status in [ prActivo ] )or lNotificar ) then
        begin
             sProceso := Mensaje.Modelo.Nombre;
             DoUpdateStatus( Format( 'Notificar - Proceso: %s', [ sProceso ] ) );
             with Tarea do
             begin
	          if ( PorProcesar or lNotificar ) then
                  begin
                       case Mensaje.Notificacion.Accion of
                            nqNotificar:
                            begin
                                 case Mensaje.Notificacion.Cuando of
                                      ncDespuesIniciar, ncAntesLimite, ncDespuesLimite:
                                      begin
                                           if ( Mensaje.VecesEnviada < Mensaje.Notificacion.Veces ) then
                                           begin
                                                dFecha := IncMinute (Mensaje.Fecha, Mensaje.Notificacion.Cada);
                                                with oZetaProvider do
                                                begin
                                                     try
                                                        FEnviaNotificacion := CreateQuery(Format (Q_ENVIA_NOTIFICACION, [ZetaCommonTools.DateToStrSQLC(dFecha)]));
                                                        ParamAsGUID( FEnviaNotificacion, 'WT_GUID', Codigo );
                                                        ParamAsInteger( FenviaNotificacion, 'WY_FOLIO', Mensaje.Notificacion.Folio);
                                                        ParamAsDateTime( FEnviaNotificacion, 'WN_FECHA', dFecha);
                                                        ParamAsInteger( FEnviaNotificacion, 'WN_VECES', Mensaje.VecesEnviada + 1);
                                                        EmpiezaTransaccion;
                                                        try
                                                           Ejecuta( FENviaNotificacion );
                                                           TerminaTransaccion( TRUE );
                                                        except
                                                              on Error: Exception do
                                                              begin
                                                                   RollBackTransaccion;
                                                              end;
                                                        end;
                                                     finally
                                                            FreeAndNil(FEnviaNotificacion);
                                                     end;
                                                end;
                                           end;
                                      end;
                                 end;
                            end;
                            nqAutorizar:
                            begin
                                 with oZetaProvider do
                                 begin
                                      try
                                         FEnviaNotificacion := CreateQuery( Q_ENVIA_AUTORIZACION );
                                         ParamAsGUID( FEnviaNotificacion, 'WT_GUID', Codigo );
                                         ParamAsInteger( FenviaNotificacion, 'WY_FOLIO', Mensaje.Notificacion.Folio);
                                         ParamAsDateTime( FEnviaNotificacion, 'WN_FECHA', Mensaje.Fecha);
                                         EmpiezaTransaccion;
                                         try
                                            Ejecuta( FENviaNotificacion );
                                            TerminaTransaccion( TRUE );
                                         except
                                               on Error: Exception do
                                               begin
                                                    RollBackTransaccion;
                                               end;
                                         end;
                                      finally
                                             FreeAndNil(FEnviaNotificacion);
                                      end;
                                 end;
                            end;
                       end;
                       with Buzon do
                       begin
                            GetRemitenteData( Mensaje.UsuarioInicial ); //?
                            GetDestinatarioData( Mensaje.UsuarioFinal ); //?
                            //if not XMLTools.XMLTransform( Mensaje.GetAllXML( Tarea ), PasoModelo.XSLSuspendido, sTexto ) then
                            if not XMLTools.XMLTransform( Mensaje.GetAllXML( Tarea ), Mensaje.Notificacion.XSL, sTexto ) then
                            begin
                                 sTexto := Format( K_MENSAJE, [ CR_LF, sProceso, Proceso.Folio, PasoProceso.Orden, aMensajeNoti[ Mensaje.Notificacion.Cuando ],Modelo.URLExaminar ] );
                            end;
                            Aviso.Tipo := Mensaje.Notificacion.Destino.Tipo;
                            Aviso.Correo := Mensaje.Notificacion.Destino.Correo;
                            Notificar(Aviso, Format( K_TEMA, [ sProceso, Proceso.Folio, Proceso.UsuarioNombre ] ), sTexto);
                            //EnviarNota( Format( K_TEMA, [ sProceso, Proceso.Folio ] ), sTexto );
                       end;
                  end;
             end;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                sMensaje := Error.Message;
           end;
     end;
     DoUpdateStatus( Format( 'Action %s - Process %d Notified', [ Self.ClassName, Proceso.Folio ] ) );

end;

function TWFBaseAccion.ProcesaAutorizar( var sMensaje: String ): Boolean;
const
     K_TEMA = 'Autorizacion de %s # %d';
     K_MENSAJE = 'El proceso de %1:s # %2:d, paso %3:d '+
                 '%4:s'+
                 '%0:s'+
                 'Si desea examinarlo, por favor entre a %5:s'+
                 '%0:s'+
                 '� Gracias !';
     aMensajeNoti : array [ eWFNotificacionCuando ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'ha iniciado', 'ha terminado', 'ha sido cancelado', 'ha sido modificado', 'ha sido iniciado', 'esta por vencerse', 'se ha vencido' );
var
   sProceso: String;
   FEnviaRespuesta, FObtenDestino: TZetaCursor;
   iDestino : Integer;
begin
     sMensaje := VACIO;
     Result := False;
     DoUpdateStatus( Format( 'Action %s - Process %d Authorizing', [ Self.ClassName, Proceso.Folio ] ) );
     try
        if ( Proceso.Status in [ prActivo ] ) then
        begin
             sProceso := Mensaje.Modelo.Nombre;
             //DoUpdateStatus( Format( 'ProcesaAutorizar - Proceso: %s', [ sProceso ] ) );
             with Tarea do
             begin
	          if ( PorProcesar ) then
                  begin
                       with oZetaProvider do
                       begin
                            try
                               //DoUpdateStatus( Format( 'Procesar Autorizar - Usuario Destino: %s' ,[ Format( Q_USUARIO_DESTINO, [ EntreComillas( CodigoToStr( Codigo ) ), UsuarioDestino ] ) ] ) );
                               FObtenDestino := CreateQuery(Format (Q_USUARIO_DESTINO, [EntreComillas(CodigoToStr(Codigo)), UsuarioDestino]));
                               iDestino := -1;
                               with FObtenDestino do
                               begin
                                    Active := true;
                                    if (not EOF) then
                                       iDestino := FieldByName('US_CODIGO').AsInteger;
                               end;
                               FEnviaRespuesta := CreateQuery(Q_ENVIA_RESPUESTA);
                               ParamAsGUID( FEnviaRespuesta, 'TAREA', Codigo );
                               ParamAsInteger( FEnviaRespuesta, 'DESTINO', iDestino);
                               ParamAsString( FEnviaRespuesta, 'ACCION', MSG_ACEPTAR );
                               ParamAsString( FEnviaRespuesta, 'NOTAS', 'El proceso fue aprobado autom�ticamente al llegar su vencimiento');
                               EmpiezaTransaccion;
                               try
                                  Ejecuta( FEnviaRespuesta );
                                  TerminaTransaccion( TRUE );
                               except
                                     on Error: Exception do
                                     begin
                                          RollBackTransaccion;
                                     end;
                               end;
                            finally
                                   FreeAndNil(FObtenDestino);
                                   FreeAndNil(FEnviaRespuesta);
                            end;
                       end;
                  end;
             end;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                sMensaje := Error.Message;
           end;
     end;
     DoUpdateStatus( Format( 'Action %s - Process %d Authorized', [ Self.ClassName, Proceso.Folio ] ) );
end;



{ ********* TWFAccionObtieneFirma ******** }

function TWFAccionObtieneFirma.Procesa( var sMensaje: String ): Boolean;
var
   sRespuesta: String;
begin
     //DoUpdateStatus( 'Antes de Leer Mensaje' );
     sRespuesta := Mensaje.Mensaje;
     //DoUpdateStatus( 'Despues de Leer Mensaje' );
     sMensaje := VACIO;
     Result := False;
     DoUpdateStatus( Format( 'Action %s - Processing', [ Self.ClassName ] ) );
     try
        {
        Pendiente: Qu� hacer con else cuando mensaje no es v�lido?
        }
        case PasoProceso.Status of
             ppInactivo:  { �No Es ppPendiente? }
             begin
                  if ( sRespuesta = MSG_ACTIVAR ) then
                     ProcesaActivar( True );
             end;
             {$ifdef ANTES}
             ppEsperando:
             {$else}
             ppActivo:
             {$endif}
             begin
                  if ( sRespuesta = MSG_ACEPTAR ) then
                     ProcesaAceptar
                  else
                      if ( sRespuesta = MSG_RECHAZAR ) then
                         ProcesaRechazar
                      else
                          if ( sRespuesta = MSG_DELEGAR ) then
                             ProcesaActivar( False );
             end;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                sMensaje := Error.Message;
           end;
     end;
     DoUpdateStatus( Format( 'Action %s - Processed', [ Self.ClassName ] ) );
end;

procedure TWFAccionObtieneFirma.ProcesaActivar( const lNuevo: Boolean );
const
     K_TEMA = 'Solicitud de Autorizaci�n de %s - %s';
     K_MENSAJE = 'Como Parte Del Proceso De Autorizaci�n De %1:s%0:s'+
                 'Se Le Pide Por Favor Verificar La Forma %2:s%0:s'+
                 '%0:s'+
                 '� Gracias !';
var
   sProceso, sTexto: String;
   TipoNotificaciones : ListaNotificaCuando;
begin
     { Usa utiler�a de clase abstracta para marcar los status }
     sProceso := Mensaje.Modelo.Nombre;
     DoUpdateStatus( Format( 'ProcesaActivar - Proceso: %s', [ sProceso ] ) );
     SetLength (TipoNotificaciones, 4);
     TipoNotificaciones[0] := ncIniciar;
     TipoNotificaciones[1] := ncDespuesIniciar;
     TipoNotificaciones[2] := ncAntesLimite;
     TipoNotificaciones[3] := ncDespuesLimite;
     {$ifndef ANTES}
     if lNuevo then
     begin
     {$endif}
          PasoActivar;
          { Crea una tarea de 'Enviar eMail' }
          { Se necesita el GUID de la Tarea antes de enviar correo }
          with TareaNueva do
          begin
               Agregar( 'Solicita Firma Via eMail', K_CERO_PORCIENTO );
               PasoProceso.Notificar( TipoNotificaciones, TareaNueva);
               with Buzon do
               begin
                    GetRemitenteData( Mensaje.UsuarioInicial );
                    GetDestinatarioData( Mensaje.UsuarioFinal );
                    if not XMLTools.XMLTransform( Mensaje.GetAllXML( TareaNueva ), PasoModelo.XSLTarea, sTexto ) then
                    begin
                         sTexto := Format( K_MENSAJE, [ CR_LF, sProceso, Modelo.URLAutorizacion( TareaNueva ) ] );
                    end;
                    EnviarEMail( K_CINCUENTA_PORCIENTO, Format( K_TEMA, [ sProceso, Proceso.UsuarioNombre ] ), sTexto, Codigo, sttEsperando );
               end;
          end;
     {$ifndef ANTES}
     end
     else
     begin
          //DoUpdateStatus( 'Antes de Paso Delegar' );
          PasoDelegar;
          //DoUpdateStatus( 'Despu�s de Paso Delegar' );
     end;
     {$endif}
end;



{ ******** TWFAccionGrabaTress ******** }

constructor TWFAccionGrabaTress.Create(Mensaje: TWFMensaje);
begin
     inherited Create( Mensaje );
     FClienteTress := TdmCliente.Create( nil );
end;

destructor TWFAccionGrabaTress.Destroy;
begin
     FreeAndNil( FClienteTress );
     inherited Destroy;
end;

function TWFAccionGrabaTress.Procesa( var sMensaje: String ): Boolean;
var
   sRespuesta: String;
begin
     sRespuesta := Mensaje.Mensaje;
     sMensaje := VACIO;
     Result := False;
     try
        {
        Pendiente: Qu� hacer con else cuando mensaje no es v�lido?
        }
        case PasoProceso.Status of
             ppInactivo:  { �No Es ppPendiente? }
             begin
                  if ( sRespuesta = MSG_ACTIVAR ) then
                     ProcesaActivar;
             end;
             ppActivo:
             begin
                  if ( sRespuesta = MSG_ACEPTAR ) then
                     ProcesaAceptar
                  else
                      if ( sRespuesta = MSG_RECHAZAR ) then
                         ProcesaRechazar;
             end;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                sMensaje := Error.Message;
           end;
     end;
end;

procedure TWFAccionGrabaTress.ProcesaActivar;
var
   sLog: String;
begin
     { Usa utiler�a de clase abstracta para marcar los status }
     PasoActivar;
     { Crea una tarea de 'Grabar datos }
     TareaNueva.Agregar( 'Graba Datos En Tress', K_CERO_PORCIENTO );
     { Hace el trabajo de Grabar en TRESS }
     if ProcesarTress( sLog ) then
     begin
          {$ifdef ANTES}
          { Usa utiler�a de clase abstracta para marcar los status }
          PasoTerminar( ppTerminado );
          { Marca la tarea terminada }
          TareaNueva.Terminar( VACIO );
          {$else}
          TareaNueva.EnviaAceptacion( Mensaje.UsuarioFinal, sLog );
          {$endif}
     end
     else
     begin
          {$ifdef ANTES}
          { Usa utiler�a de clase abstracta para marcar los status }
          PasoTerminar( ppCancelado );
          { Marca la tarea terminada }
          TareaNueva.Cancelar( VACIO );
          {$else}
          TareaNueva.EnviaRechazo( Mensaje.UsuarioFinal, sLog );
          {$endif}
     end;
     DoUpdateStatus( Format( 'ProcesarTress - Log: %s', [ sLog ] ) );
end;

function TWFAccionGrabaTress.ProcesarTress( var sLog: String ): Boolean;
begin
     { Aqu� va la llamada a la interfase con TRESS }
     sLog := '';
     try
        with FClienteTress do
        begin
             Result := TressGrabar( Mensaje.Proceso.Data, Mensaje.Globales.UsuarioMotor, {$ifdef LEONI} Mensaje.Proceso.Folio, {$endif} sLog );
        end;
     except
           on Error: Exception do
           begin
                Result := False;
                sLog := Error.Message;
           end;
     end;
end;

{ ******** TWFAccionCallWS ******** }

constructor TWFAccionCallWS.Create(Mensaje: TWFMensaje);
begin
     inherited Create( Mensaje );
end;

destructor TWFAccionCallWS.Destroy;
begin
     inherited Destroy;
end;

function TWFAccionCallWS.Procesa( var sMensaje: String ): Boolean;
var
   sRespuesta: String;
begin
     sRespuesta := Mensaje.Mensaje;
     sMensaje := VACIO;
     Result := False;
     try
        {
        Pendiente: Qu� hacer con else cuando mensaje no es v�lido?
        }
        DoUpdateStatus( Format( 'WS: Informacion a Procesar: %s' ,[ sRespuesta ] ) );
        case PasoProceso.Status of
             ppInactivo:  { �No Es ppPendiente? }
             begin
                  if ( sRespuesta = MSG_ACTIVAR ) then
                     ProcesaActivar;
             end;
             ppActivo:
             begin
                  if ( sRespuesta = MSG_ACEPTAR ) then
                     ProcesaAceptar
                  else
                      if ( sRespuesta = MSG_RECHAZAR ) then
                         ProcesaRechazar;
             end;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                sMensaje := Error.Message;
           end;
     end;
end;

procedure TWFAccionCallWS.ProcesaActivar;
var
   sLog: String;
begin
     { Usa utiler�a de clase abstracta para marcar los status }
     PasoActivar;
     { Crea una tarea de 'Grabar datos }
     TareaNueva.Agregar( 'Llama WebService', K_CERO_PORCIENTO );
     { Hace el trabajo de Grabar en TRESS }
     if ProcesarWS( sLog ) then
     begin
          {$ifdef ANTES}
          { Usa utiler�a de clase abstracta para marcar los status }
          PasoTerminar( ppTerminado );
          { Marca la tarea terminada }
          TareaNueva.Terminar( VACIO );
          {$else}
          TareaNueva.EnviaAceptacion( Mensaje.UsuarioFinal, sLog );
          {$endif}
     end
     else
     begin
          {$ifdef ANTES}
          { Usa utiler�a de clase abstracta para marcar los status }
          PasoTerminar( ppCancelado );
          { Marca la tarea terminada }
          TareaNueva.Cancelar( VACIO );
          {$else}
          TareaNueva.EnviaRechazo( Mensaje.UsuarioFinal, sLog );
          {$endif}
     end;
end;

function TWFAccionCallWS.ProcesarWS( var sLog: String ): Boolean;
var
   ReturnValue : String;
   Nodo : TZetaXMLNode;
begin
     { Aqu� va la llamada a la interfase con TRESS }
     sLog := '';
     try
        //DoUpdateStatus( Format( 'URL a ejecutar: %s' ,[ Mensaje.Conexion.URL ] ) );
        //DoUpdateStatus( Format( 'Usuario Motor con el que se ejecuta: %d' ,[ Mensaje.Globales.UsuarioMotor ] ) );
        //DoUpdateStatus( Format( 'Usuario con el que se ejecuta: %s' ,[ Mensaje.Conexion.Usuario ] ) );
        //DoUpdateStatus( Format( 'Clave del usuario con el que se ejecuta: %s' ,[ Mensaje.Conexion.Password ] ) );
        //DoUpdateStatus( Format( 'XML del proceso: %s' ,[ Mensaje.Proceso.Data ] ) );
        {.$ifdef ANTES}
        if ( coWebServiceRPC = Mensaje.Conexion.WSCodificacion ) then
        begin
             FWSRPC := IWorkFlowService1.GetIWorkflowService( False, Mensaje.Conexion.URL);
             ReturnValue := FWSRPC.TressGrabar( Mensaje.Proceso.Data, Mensaje.Globales.UsuarioMotor );
        end
        else
        begin
             //DoUpdateStatus( Format( 'Antes de llamada: %s' ,[ 'GetIWorkflowServiceserviceSoap' ] ) );
             FWSDocument := WorkflowService.GetIWorkflowServiceserviceSoap( False, Mensaje.Conexion.URL, nil, Mensaje.Conexion.Usuario, Mensaje.Conexion.Password);
             //DoUpdateStatus( Format( 'Despues de llamada: %s' ,[ 'GetIWorkflowServiceserviceSoap' ] ) );
             //DoUpdateStatus( Format( 'Antes de llamada: %s' ,[ 'ComunicaTress' ] ) );
             ReturnValue := FWSDocument.ComunicaTress( Mensaje.Proceso.Data, Mensaje.Globales.UsuarioMotor );
             //DoUpdateStatus( Format( 'Despues de llamada: %s' ,[ 'ComunicaTress' ] ) );
        end;
        //DoUpdateStatus( Format( 'Respuesta de regreso: %s' ,[ ReturnValue ] ) );
        with Mensaje.XMLTools do
        begin
             XMLAsText := ReturnValue;
             Nodo := GetNode( 'EXITO' );
             if Assigned(Nodo) then
             begin
                  Result := True;
             end
             else
             begin
                  Result := False;
                  sLog := TagAsString(GetNode('ERROR'), '');
                  if strVacio(sLog) then
                     sLog := 'Error: El documento XML de respuesta esta mal formateado';
                  //DoUpdateStatus( Format( 'Log: %s' ,[ sLog ] ) );
             end;
        end;
        {.$else}
        {
        FWSDocument := WorkflowService.GetIWorkflowServiceserviceSoap( False, Mensaje.Conexion.URL, nil, Mensaje.Conexion.Usuario, Mensaje.Conexion.Password);
        ReturnValue := FWSDocument.Prueba;
        DoUpdateStatus( Format( 'Respuesta de regreso: %s' ,[ ReturnValue ] ) );
        with Mensaje.XMLTools do
        begin
             XMLAsText := ReturnValue;
             Nodo := GetNode( 'EXITO' );
             if Assigned(Nodo) then
             begin
                  Result := True;
             end
             else
             begin
                  Result := False;
                  sLog := TagAsString(GetNode('ERROR'), '');
                  if strVacio(sLog) then
                     sLog := 'Error: El documento XML de respuesta esta mal formateado';
                  DoUpdateStatus( Format( 'Log: %s' ,[ sLog ] ) );
             end;
        end;
        }
        {.$endif}

     except
           on Error: Exception do
           begin
                Result := False;
                //DoUpdateStatus( Format( 'Log de ERROR en CallWS: %s' ,[ Error.Message ] ) );
                sLog := Error.Message;
           end;
     end;
end;

{ ******** TWFBuzon ******* }

procedure TWFBuzon.Start;
begin
     with oZetaProvider do
     begin
          FAgregar := CreateQuery( Q_BUZON_INSERT );
          FUsuario := CreateQuery( Q_USUARIO_LEER );
          FLista := CreateQuery( Q_USUARIO_LISTA );
     end;
     DummyValues :=False;
end;

procedure TWFBuzon.Stop;
begin
     if Assigned( FAgregar ) then
        FreeAndNil( FAgregar );
     if Assigned( FUsuario ) then
        FreeAndNil( FUsuario );
     if Assigned( FLista ) then
        FreeAndNil( FLista );
end;

procedure TWFBuzon.LoadDummyValues;
var
  oRemitente : TUserData;
  oDestinatario : TUserData;
begin
     inherited LoadDummyValues;
     oRemitente := Remitente;
     with Remitente do
     begin
          oRemitente.Numero := 1;
          oRemitente.Nombre := 'Administrador';
          oRemitente.Direccion := 'admin@dominio.com';
          oRemitente.TipoCorreo := eEMailType( 0 );
     end;
     Remitente := oRemitente;
     FreeAndNil(oRemitente);


     oDestinatario := Destinatario;
     with Destinatario do
     begin
          oDestinatario.Numero := 2;
          oDestinatario.Nombre := 'Gerente';
          oDestinatario.Direccion := 'manager@dominio.com';
          oDestinatario.TipoCorreo := eEMailType( 0 );
     end;
     Destinatario := oDestinatario;
     FreeAndNil(oDestinatario);

end;

procedure TWFBuzon.BuildXML;

procedure BuildXMLCorreo( Usuario: TUserData; const sTag: String );
begin
     with XMLTools do
     begin
          WriteStartElement( sTag );
          try
             with Usuario do
             begin
                  WriteValueInteger( 'CODIGO', Numero );
                  WriteValueString( 'NOMBRE', Nombre );
                  WriteValueString( 'EMAIL', Direccion );
                  WriteValueInteger( 'FORMATO', Ord( TipoCorreo ) );
             end;
          finally
                 WriteEndElement;
          end;
     end;
end;

begin
     BuildXMLCorreo( Remitente, 'REMITENTE' );
     BuildXMLCorreo( Destinatario, 'DESTINATARIO' );
end;

procedure TWFBuzon.Notificar( Aviso: TAviso; Proceso: TWFBaseProceso );
var
   sTema: String;
begin
     sTema := Proceso.GetTema;
     GetRemitenteData( Mensaje.Globales.UsuarioMotor );
     with FLista do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FLista, 'Usuario', Mensaje.UsuarioInicial );
               ParamAsInteger( FLista, 'TipoLista', Ord( Aviso.Tipo ) );
               ParamAsVarChar( FLista, 'Detalle', Aviso.Correo, K_ANCHO_OBSERVACIONES );
               ParamAsVarChar( FLista, 'Tarea', CodigoToStr( Mensaje.Tarea.Codigo ), 0 );
          end;
          Active := True;
          while not Eof do
          begin
               GetDestinatarioData( FieldByName( 'US_CODIGO' ).AsInteger );
               EnviarNota( sTema, Proceso.GetBody );
               Next;
          end;
          Active := False;
     end;
end;

procedure TWFBuzon.Notificar( Aviso: TAviso; sTema, sBody: String );
begin
     GetRemitenteData( Mensaje.Globales.UsuarioMotor );
     with FLista do
     begin
          Active := False;
          with oZetaProvider do
          begin
               ParamAsInteger( FLista, 'Usuario', Mensaje.UsuarioInicial );
               ParamAsInteger( FLista, 'TipoLista', Ord( Aviso.Tipo ) );
               ParamAsVarChar( FLista, 'Detalle', Aviso.Correo, K_ANCHO_OBSERVACIONES );
               ParamAsVarChar( FLista, 'Tarea', CodigoToStr( Mensaje.Tarea.Codigo ), 0 );
          end;
          Active := True;
          while not Eof do
          begin
               GetDestinatarioData( FieldByName( 'US_CODIGO' ).AsInteger );
               EnviarNota( sTema, sBody );
               Next;
          end;
          Active := False;
     end;
end;

function TWFBuzon.GetUserData( const iUsuario: Integer ): TUserData;
begin
     with Result do
     begin
          Numero := iUsuario;
          Nombre := '???';
          TipoCorreo := eEMailType( 0 );
          Direccion := '???@???';
     end;
     if ( iUsuario = Mensaje.Globales.UsuarioMotor ) then
     begin
          with Result do
          begin
               Nombre := Mensaje.Registry.WorkFlowEMailName;
               Direccion := Mensaje.Registry.WorkFlowEMailAddress;
          end;
     end
     else
     begin
          with FUsuario do
          begin
               Active := False;
               with oZetaProvider do
               begin
                    ParamAsInteger( FUsuario, 'US_CODIGO', iUsuario );
               end;
               Active := True;
               if not Eof then
               begin
                    with Result do
                    begin
                         Nombre := FieldByName( 'US_NOMBRE' ).AsString;
                         TipoCorreo := eEMailType( FieldByName( 'US_FORMATO' ).AsInteger );
                         Direccion := FieldByName( 'US_EMAIL' ).AsString;
                    end;
               end;
               Active := False;
          end;
     end;
end;

procedure TWFBuzon.EMailBuild(const sSubject, sBody: String );

   function TransformaXML( const sTexto: string ): string;
   const
        K_UTF_16 = '<META http-equiv="Content-Type" content="text/html';
        K_OFFSET = 49;
   var
      sTemporal, sInicio, sFinal: string;
      iPos: integer;
   begin
        sTemporal := sTexto;
        Result := sTexto;
        iPos := pos( AnsiUpperCase( K_UTF_16 ), AnsiUppercase( sTemporal ) );
        if ( iPos > 0 ) then
        begin
             sInicio := Copy( sTemporal, 1, iPos - 1 )+ K_UTF_16;
             Delete( sTemporal, 1, iPos + K_OFFSET );
             iPos := pos( '">', AnsiUppercase( sTemporal ) );
             if ( iPos > 0 ) then
             begin
                  sFinal := Copy( sTemporal, iPos, Length( sTemporal ) );
             end;
             Result := sInicio + sFinal;
        end;
   end;

begin
     with oZetaProvider do
     begin
          ParamAsBlob( FAgregar, 'ToAddress', Destinatario.Direccion );
          ParamAsBlob( FAgregar, 'CCAddress', VACIO );
          ParamAsVarChar( FAgregar, 'Subject', sSubject, K_ANCHO_ACCESOS );
          ParamAsBlob( FAgregar, 'Body', TransformaXML( sBody ) );
          ParamAsVarChar( FAgregar, 'FromName', Remitente.Nombre, K_ANCHO_ACCESOS );
          ParamAsVarChar( FAgregar, 'FromAddress', Remitente.Direccion, K_ANCHO_ACCESOS );
          ParamAsInteger( FAgregar, 'SubType', Ord( Destinatario.TipoCorreo ) );
          ParamAsNull( FAgregar, 'GUIDTarea' );
          ParamAsInteger( FAgregar, 'IDProceso', Mensaje.IDProceso );
          ParamAsInteger( FAgregar, 'Status', 0 );
          ParamAsInteger( FAgregar, 'Avance', 0 );
          ParamAsInteger( FAgregar, 'Envia', Remitente.Numero );
          ParamAsInteger( FAgregar, 'Recibe', Destinatario.Numero );
     end;
end;

procedure TWFBuzon.EMailPost;
begin
     with oZetaProvider do
     begin
          EmpiezaTransaccion;
          try
             Ejecuta( FAgregar );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     RollBackTransaccion;
                end;
          end;
     end;
end;

procedure TWFBuzon.GetRemitenteData( const iUsuario: Integer );
begin
     Self.Remitente := GetUserData( iUsuario );
end;

procedure TWFBuzon.GetDestinatarioData( const iUsuario: Integer );
begin
     Self.Destinatario := GetUserData( iUsuario );
end;

procedure TWFBuzon.EnviarNota( const sSubject, sBody: String );
begin
     EMailBuild( sSubject, sBody );
     EMailPost;
end;

procedure TWFBuzon.EnviarEMail( const iAvance: Integer; const sSubject, sBody: String; const idTarea: TWFIdentifier; const eStatusTarea: eWFTareaStatus );
begin
     EMailBuild( sSubject, sBody );
     with oZetaProvider do
     begin
          ParamAsVarChar( FAgregar, 'GUIDTarea', CodigoToStr( idTarea ), 0 );
          ParamAsInteger( FAgregar, 'Status', Ord( eStatusTarea ) );
          ParamAsInteger( FAgregar, 'Avance', iAvance );
     end;
     EMailPost;
end;

procedure TWFBuzon.NotificarAdministrador(const sSubject, sBody: String);
begin
     GetRemitenteData( Mensaje.UsuarioInicial ); //?
     GetDestinatarioData( Mensaje.Globales.UsuarioMotor );   //?
     EMailBuild( sSubject, sBody );
     EMailPost;
end;

end.
