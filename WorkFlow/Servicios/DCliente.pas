unit DCliente;

{$define LEONI}

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,{$ifndef VER130}Variants,{$endif}
     {$ifdef DOS_CAPAS}
     DServerRecursos,
     DServerAsistencia,
     DServerNomina,
     DServerCatalogos,
     DServerTablas,
     //DZetaServerProvider,
     ZetaSQLBroker,
     DServerComparte,
     DServerLogin,
     DServerConsultas,
     {$else}
     Recursos_TLB,
     DCalcNomina_TLB,
     Asistencia_TLB,
     Comparte_TLB,
     Login_TLB,
     Consultas_TLB,
     Nomina_TLB,
     Catalogos_TLB,
     Tablas_TLB,
     {$endif}
     DZetaServerProvider,
     DBasicoCliente,
     DXMLTools,
     //ZetaCommonTypes,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZBaseThreads,
     ZetaClientDataSet, ZetaServerDataSet,
     DIncapacidad,
     DPrestamos,
     ZGlobalTress;

const
     K_QTY_CHAR = 50;
     K_QTY_BOOLEANOS = 29;
     K_QTY_VARCHAR = 80;
     K_QTY_NUMEROS = 38;
     K_QTY_FECHA = 34;
     K_QTY_DECIMALES = 36; 
     K_QRY_MAXNUMEMP = 'select ( max(CB_CODIGO) + 1 ) as max from colabora';
     K_CONSULTA_PERIODOS = 'select PE_YEAR, PE_NUMERO from periodo ' +
                           'where ( select PE_FEC_INI from periodo ' +
                                    'where (''%s'') between PE_FEC_INI and PE_FEC_FIN and PE_TIPO=%1:d and PE_NUMERO < %2:d )' +
                           'between PE_FEC_INI and PE_FEC_FIN and PE_TIPO=%1:d and PE_NUMERO < %2:d';

     K_CONSULTA_ENTIDAD = 'select TB_CODIGO from entidad where REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( UPPER( TB_TEXTO ),''�'',''A'' ),''�'',''E'' ),''�'',''I'' ),''�'',''O''  ),''�'',''U'' ) = ''%s''';

     K_TW_MULTIP      = 'MULTIP';
     K_TW_AUTO3       = 'AUTO3';
     K_TW_INSCRIPCION = 'INSCRI';
     K_TW_TIEMPOX     = 'TEXTRA';
     K_TW_PLAZA       = 'PLAZA';
     K_TW_CAMBIOPE    = 'CAMBIOPE';
     K_TW_CAMBIOPE_PRESBAJA = 'CAMBIOJP';  //Cambio Jornada Presbaja
     K_TW_MULTIP_PRESBAJA = 'MULTIPP';  //Cambio Area Presbaja
     K_TW_AUTO3MULT   = 'AUTO3MULT';
     K_TW_EXCEPNOMINA = 'EXNOMINA' ;
     K_TW_MODCOLABORA = 'MODCOLABORA';
     K_TW_INFONAVIT_AGREGAR = 'INFONAVIT_AGREGAR'; // PENSEKE Credito Infonavit
     K_TW_INFONAVIT_MODIFICAR = 'INFONAVIT_MODIFICAR'; // PENSEKE Credito Infonavit
     K_TW_INFONAVIT_SUSPENDER = 'INFONAVIT_SUSPENDER'; // PENSEKE Credito Infonavit
     K_TW_INFONAVIT_REINICIAR = 'INFONAVIT_REINICIAR'; // PENSEKE Credito Infonavit
     K_TW_INCAPACIDAD_AGREGAR = 'INCAPACIDAD_AGREGAR';
     K_TW_PRESTAMO_AGREGAR = 'PRESTAMO_AGREGAR';
     K_TW_CAMBIO_EQUIPO = 'CAMBIOEQUIPO';
     K_TW_BAJA_EMPLEADO = 'BAJA';
     K_TW_PUESTO_CAMBIAR = 'CAMBIOPUESTO';
     K_TW_TURNO_CAMBIAR = 'CAMBIOTURNO';
     K_TW_CAMBIO_SALARIO = 'CAMBIOSALARIO';
     K_TW_CAMBIO_AREA = 'CAMBIOAREAS';
     K_TW_CAMBIO_CONTRATO = 'CAMBIOCONTRATO';
     K_TW_AGREGAR_PARIENTE = 'PARIENTE_AGREGAR';
     K_TW_MODIFICAR_PARIENTE  = 'PARIENTE_MODIFICAR';
     K_TW_BORRAR_PARIENTE  = 'PARIENTE_BORRAR';
     K_TW_EMP_SEGURO = 'EMPSEGURO_AGREGAR';
     K_TW_EMP_AGREGAR = 'ALTA_EMPLEADO'; //US 1111: Agregar Alta de Empleado @DLSJ 2016-02-15
     K_TW_EMP_REINGRESO = 'REINGRESO_EMPLEADO';
     K_TW_EMP_CHECADA = 'CHECADASIMPLE';
     {$ifdef LEONI}
     K_TW_EXTRALEONI  = 'TEXTRALEONI';
     {$endif}
     K_TW_INTERCAMBIO_FESTIVO = 'INTERCAMBIOFESTIVO';

     K_NO_AUTORIZADO = 99;
     K_AUTORIZADO = -1;
     K_MAX_VAL_USO_DEMO = 5;
     K_QRY_COLABORA = 'select top 1 * from COLABORA';


     aCamposChar : array [ 1..K_QTY_CHAR, 1..2 ] of PChar = ( ('CB_EDO_CIV','1'),
                                                            ('CB_MED_TRA','1'),
                                                            ('CB_VIVECON','1'),
                                                            ('CB_SEXO','1'),
                                                            ('CB_CHECA','1'),
                                                            ('CB_EST_HOY','1'),
                                                            ('CB_HABLA','1'),
                                                            ('CB_INFMANT','1'),
                                                            ('CB_PASAPOR','1'),
                                                            ('CB_CREDENC','1'),
                                                            ('CB_ZONA_GE','1'),
                                                            ('CB_G_LOG_1','1'),
                                                            ('CB_G_LOG_2','1'),
                                                            ('CB_G_LOG_3','1'),
                                                            ('CB_ESTADO','2'),
                                                            ('CB_ESTUDIO','2'),
                                                            ('CB_VIVEEN','2'),
                                                            ('CB_G_TAB_1','6'),
                                                            ('CB_G_TAB_2','6'),
                                                            ('CB_G_TAB_3','6'),
                                                            ('CB_G_TAB_4','6'),
                                                            ('CB_NIVEL0','6'),
                                                            ('CB_CLINICA','3'),
                                                            ('CB_ENT_NAC','2'),
                                                            ('CB_COD_COL','6'),
                                                            ('CB_G_TAB_5','6'),
                                                            ('CB_G_TAB_6','6'),
                                                            ('CB_G_TAB_7','6'),
                                                            ('CB_G_TAB_8','6'),
                                                            ('CB_G_TAB_9','6'),
                                                            ('CB_G_TAB10','6'),
                                                            ('CB_G_TAB11','6'),
                                                            ('CB_G_TAB12','6'),
                                                            ('CB_G_TAB13','6'),
                                                            ('CB_G_TAB14','6'),
                                                            ('CB_G_LOG_4','1'),
                                                            ('CB_G_LOG_5','1'),
                                                            ('CB_G_LOG_6','1'),
                                                            ('CB_G_LOG_7','1'),
                                                            ('CB_G_LOG_8','1'),
                                                            ('CB_DISCAPA','1'),
                                                            ('CB_INDIGE','1'),
                                                            ('CB_EMPLEO','1'),
                                                            ('CB_BANCO','6'),
                                                            ('CB_MUNICIP','6'), { Si se agregan campos char(1) que representen booleanos, agregarlos en arreglo aCamposBooleano }
                                                            ('CB_TIP_REV','6'), //+1
                                                            ('CB_AREA','6'),
                                                            ('CB_GP_COD','6'),
                                                            ('CB_AR_HOR','4'),
                                                            ('CB_MOT_BAJ','3')
                                                            );

   aCamposBooleano: array [ 1..K_QTY_BOOLEANOS ] of PChar =  ( 'CB_INIDGE',
                                                               'CB_DISCAPA',
                                                               'CB_AUTOSAL',
                                                               'CB_CHECA',
                                                               'CB_EST_HOY',
                                                               'CB_G_LOG_1',
                                                               'CB_G_LOG_2',
                                                               'CB_G_LOG_3',
                                                               'CB_G_LOG_4',
                                                               'CB_G_LOG_5',
                                                               'CB_G_LOG_6',
                                                               'CB_G_LOG_7',
                                                               'CB_G_LOG_8',
                                                               'CB_INFMANT',
                                                               'CB_PASAPOR',
                                                               'CB_EMPLEO',
                                                               'CB_ACTIVO',
                                                               'CB_HABLA',
                                                               'CB_PORTAL',
                                                               'CB_DNN_OK',
                                                               'CB_RECONTR',
                                                               'CB_INFDISM',
                                                               'CB_INFACT',
                                                               'CB_TIE_PEN',
                                                               'CB_BRG_ACT',
                                                               'CB_BRG_ROL',
                                                               'CB_BRG_JEF',
                                                               'CB_BRG_CON',
                                                               'CB_BRG_PRA'
                                                                );

   aCamposVarChar : array [ 1..K_QTY_VARCHAR, 1..2 ] of PChar = ( ('CB_BAN_ELE','30'),
                                                                  ('CB_CIUDAD','30'),
                                                                  ('CB_CURP','30'),
                                                                  ('CB_CALLE','50'),
                                                                  ('CB_COLONIA','30'),
                                                                  ('CB_EST_HOR','30'),
                                                                  ('CB_IDIOMA','30'),
                                                                  ('CB_INFCRED','30'),
                                                                  ('CB_LA_MAT','30'),
                                                                  ('CB_LUG_NAC','30'),
                                                                  ('CB_NACION','30'),
                                                                  ('CB_RFC','30'),
                                                                  ('CB_SEGSOC','30'),
                                                                  ('CB_TEL','30'),
                                                                  ('CB_G_TEX_1','30'),
                                                                  ('CB_G_TEX_2','30'),
                                                                  ('CB_G_TEX_3','30'),
                                                                  ('CB_G_TEX_4','30'),
                                                                  ('CB_EXPERIE','100'),
                                                                  ('CB_MAQUINA','100'),
                                                                  ('CB_CARRERA','40'),
                                                                  ('CB_CODPOST','8'),
                                                                  ('CB_ID_NUM','30' ),
                                                                  ('CB_G_TEX_5','30'),
                                                                  ('CB_G_TEX_6','30'),
                                                                  ('CB_G_TEX_7','30'),
                                                                  ('CB_G_TEX_8','30'),
                                                                  ('CB_G_TEX_9','30'),
                                                                  ('CB_G_TEX10','30'),
                                                                  ('CB_G_TEX11','30'),
                                                                  ('CB_G_TEX12','30'),
                                                                  ('CB_G_TEX13','30'),
                                                                  ('CB_G_TEX14','30'),
                                                                  ('CB_G_TEX15','30'),
                                                                  ('CB_G_TEX16','30'),
                                                                  ('CB_G_TEX17','30'),
                                                                  ('CB_G_TEX18','30'),
                                                                  ('CB_G_TEX19','30'),
                                                                  ('CB_G_TEX20','30'),
                                                                  ('CB_G_TEX21','30'),
                                                                  ('CB_G_TEX22','30'),
                                                                  ('CB_G_TEX23','30'),
                                                                  ('CB_G_TEX24','30'),
                                                                  ('CB_SUB_CTA','30'),
                                                                  ('CB_FONACOT','30'),
                                                                  ('CB_NUM_EXT','10'),
                                                                  ('CB_NUM_INT','10'),
                                                                  ('CB_ESCUELA','40'),
                                                                  ('CB_CTA_GAS','30'),
                                                                  ('CB_CTA_VAL','30'),
                                                                  //Agregados
                                                                  ('CB_PUESTO','6'),
                                                                  ('CB_CLASIFI','6'),
                                                                  ('CB_TURNO','6'),
                                                                  ('CB_TABLASS','6'),
                                                                  ('CB_CONTRAT','6'),
                                                                  ('CB_NIVEL1','6'),
                                                                  ('CB_NIVEL2','6'),
                                                                  ('CB_NIVEL3','6'),
                                                                  ('CB_NIVEL4','6'),
                                                                  ('CB_NIVEL5','6'),
                                                                  ('CB_NIVEL6','6'),
                                                                  ('CB_NIVEL7','6'),
                                                                  ('CB_NIVEL8','6'),
                                                                  ('CB_NIVEL9','6'),
                                                                  ('CB_PATRON','6'),
                                                                  ('CB_APE_PAT','30'),
                                                                  ('CB_APE_MAT','30'), //+1
                                                                  ('CB_EST_HOR','30'),
                                                                  ('CB_NOMBRES','30'),
                                                                  ('CB_TSANGRE','30'),
                                                                  ('CB_BRG_NOP','30'),
                                                                  ('CB_PASSWRD','30'),
                                                                  ('CB_USRNAME','40'),
                                                                  ('CB_ZONA','8'),
                                                                  ('CB_E_MAIL','255'),
                                                                  ('CB_ALERGIA','255'),
                                                                  ('NOM_PADRE','30'),//+1
                                                                  ('NOM_MADRE','30'),
                                                                  ('COD_PERCEPS','10'),
                                                                  ('IngresoID', '30' )
                                                                  );

   aCamposNumericos : array[ 1..K_QTY_NUMEROS ] of PChar = ( 'CB_INFTIPO',
                                                             'CB_EVALUA',
                                                             'CB_INFTASA',
                                                             'CB_INF_OLD',
                                                             'CB_CANDIDA',
                                                             'CB_G_NUM_1',
                                                             'CB_G_NUM_2',
                                                             'CB_G_NUM_3',
                                                             'CB_G_NUM_4',
                                                             'CB_G_NUM_5',
                                                             'CB_G_NUM_6',
                                                             'CB_G_NUM_7',
                                                             'CB_G_NUM_8',
                                                             'CB_G_NUM_9',
                                                             'CB_G_NUM10',
                                                             'CB_G_NUM11',
                                                             'CB_G_NUM12',
                                                             'CB_G_NUM13',
                                                             'CB_G_NUM14',
                                                             'CB_G_NUM15',
                                                             'CB_G_NUM16',
                                                             'CB_G_NUM17',
                                                             'CB_G_NUM18',
                                                             'CB_NETO',
                                                             'CB_TDISCAP',
                                                             'CB_TESCUEL',
                                                             'CB_TITULO',
                                                             'CB_YTITULO',
                                                             'CB_REGIMEN',
                                                             'CB_PENSION',//+1
                                                             'CB_BRG_TIP',
                                                             'CB_NOMYEAR',
                                                             'CB_NOMTIPO',
                                                             'CB_NOMNUME',
                                                             'CB_PLAZA',
                                                             'CB_ID_BIO',
                                                             'CB_NOMINA',
                                                             'CB_CODIGO'
                                                              );

   aCamposFecha : array [ 1..K_QTY_FECHA ] of PChar = ( 'CB_FEC_RES',
                                                        'CB_FEC_NAC',
                                                        'CB_LAST_EV',
                                                        'CB_NEXT_EV',
                                                        'CB_INF_INI',
                                                        'CB_G_FEC_1',
                                                        'CB_G_FEC_2',
                                                        'CB_G_FEC_3',
                                                        'CB_G_FEC_4',
                                                        'CB_G_FEC_5',
                                                        'CB_G_FEC_6',
                                                        'CB_G_FEC_7',
                                                        'CB_G_FEC_8',
                                                        'CB_INF_ANT',
                                                        'CB_AR_FEC',
                                                        'CB_FEC_ANT',
                                                        'CB_FEC_BAJ',
                                                        'CB_FEC_BSS',
                                                        'CB_FEC_CON',
                                                        'CB_FEC_ING',
                                                        'CB_FEC_INT',
                                                        'CB_FEC_REV',
                                                        'CB_FEC_VAC',
                                                        'CB_FEC_INC',
                                                        'CB_FEC_PER',
                                                        'CB_DER_FEC',
                                                        'CB_FEC_SAL',
                                                        'CB_FEC_NIV',
                                                        'CB_FEC_PTO',
                                                        'CB_FEC_TUR',
                                                        'CB_FEC_KAR',
                                                        'CB_FEC_PLA',
                                                        'CB_FEC_NOM',
                                                        'CB_FEC_COV'
                                                         );

  aCamposDecimales : array [ 1..K_QTY_DECIMALES ] of PChar = ( 'CB_G_NUM_1',
                                                               'CB_G_NUM_2',
                                                               'CB_G_NUM_3',
                                                               'CB_G_NUM_4',
                                                               'CB_G_NUM_5',
                                                               'CB_G_NUM_6',
                                                               'CB_G_NUM_7',
                                                               'CB_G_NUM_8',
                                                               'CB_G_NUM_9',
                                                               'CB_G_NUM_10',
                                                               'CB_G_NUM_11',
                                                               'CB_G_NUM_12',
                                                               'CB_G_NUM_13',
                                                               'CB_G_NUM_14',
                                                               'CB_G_NUM_15',
                                                               'CB_G_NUM_16',
                                                               'CB_G_NUM_17',
                                                               'CB_G_NUM_18',
                                                               'CB_NETO',
                                                               'CB_SAL_INT',
                                                               'CB_SALARIO',
                                                               'CB_OLD_SAL',
                                                               'CB_OLD_INT',
                                                               'CB_PRE_INT',
                                                               'CB_PER_VAR',
                                                               'CB_SAL_TOT',
                                                               'CB_TOT_GRA',
                                                               'CB_DER_PAG', //+1
                                                               'CB_V_PAGO',
                                                               'CB_DER_GOZ',
                                                               'CB_V_GOZO',
                                                               'CB_DER_PV',
                                                               'CB_V_PRIMA',
                                                               'CB_FAC_INT',
                                                               'CB_RANGO_S',
                                                               'CB_SALARIO_BRUTO' {*** El valor que viene de la nube ***}
                                                               );

type
  TEmpleados = class( TStringList )
    private
     { Private declarations }
     function GetEmpleado( Index: Integer ): TNumEmp;
  public
     { Public declarations }
     constructor Create;
     destructor Destroy; override;
     property Empleado[ Index: Integer ]: TNumEmp read GetEmpleado;
     function AddEmpleado( const iEmpleado: TNumEmp ): TNumEmp;

  end;

TEmpleadoBasico = class( TObject )
     public
     { Public declarations }
     UsuarioWFN : TNumEmp;
     NumeroEmpleado : TNumEmp;
     Salario : TPesos;
     Puesto : TCodigo;
     Turno : TCodigo;
     Clasificacion : TCodigo;
     Year : TNumEmp;
     Tipo : TNumEmp;
     Numero : TNumEmp;
     Recontratacion : string;
     TipoContrato  : string;
     Nivel1  : string;
     Nivel2  : string;
     Nivel3  : string;
     Nivel4  : string;
     Nivel5  : string;
     Nivel6  : string;
     Nivel7  : string;
     Nivel8  : string;
     Nivel9  : string;
     Motivo : TCodigo;
     Equipo : string;
     AutoSalario : string;
     FechaRegistro : TDateTime;
     FechaContrato : TDateTime;
     FechaContratoVencimiento : TDateTime;
     FechaImss : TDateTime;
     Descripcion : string;
     Observaciones : string;

     FechaNacimiento_Par : TDateTime;
     Relacion_Par : TNumEmp;
     Folio_Par : TNumEmp;
     Sexo_Par : string;
     ApellidoPaterno_Par : string;
     ApellidoMaterno_Par : string;
     Nombre_Par : string;

     FechaOriginal: TDateTime;
     FechaNueva : TDateTime;

     constructor Create;
     destructor Destroy; override;
  end;

  TEmpleadosPE = class( TObject )
  private
     slEmpleados : TStringList;
     slSalarios  : TStringList;
     slClasifis  : TStringList;
     slFecReg    : TStringList;
     slFecIMSS   : TStringList;
     slDescripcion : TStringList;
     slObserva   : TStringList;
     slPuestos   : TStringList;
     slTurnos    : TStringList;

     { Private declarations }
     function GetEmpleado( Index: Integer ): TNumEmp;
     function GetSalario ( Index: Integer ): TPesos;
     function GetClasifi ( Index: Integer ): TCodigo;
     function GetFecReg ( Index: Integer ): TDateTime;
     function GetFecIMSS ( Index: Integer ): TDateTime;
     function GetDescripcion ( Index: Integer ): string;
     function GetObserva ( Index: Integer ): string;
     function GetPuesto ( Index: Integer ): TCodigo;
     function GetTurno ( Index: Integer ): TCodigo;

     public
     { Public declarations }
     constructor Create;
     destructor Destroy; override;
     property Empleado[ Index: Integer ]: TNumEmp read GetEmpleado;
     property Salario[ Index: Integer ] : TPesos read GetSalario;
     property Clasifi[ Index: Integer ] : TCodigo read GetClasifi;
     property Puesto[ Index: Integer ] : TCodigo read GetPuesto;
     property Turno[ Index: Integer ] : TCodigo read GetTurno;
     property FecReg[ Index: Integer ] : TDateTime read GetFecReg;
     property FecIMSS[ Index: Integer ] : TDateTime read GetFecIMSS;
     property Observa[ Index: Integer ] : string read GetObserva;
     property Descripcion[ Index: Integer ] : string read GetDescripcion;
     function AddEmpleado( const iEmpleado: TNumEmp; const pSalario : TPesos; const cClasifi : TCodigo;
                           const dFecReg : TDatetime; const dFecIMSS : TDateTime; const sObserva : string ): TNumEmp;

     function AddEmpleadoJornada( const iEmpleado: TNumEmp; const pSalario : TPesos; const cClasifi : TCodigo;
                           const dFecReg : TDatetime; const dFecIMSS : TDateTime; const sObserva : string;  const cPuesto : TCodigo;  const cTurno : TCodigo; const sDescripcion : string): TNumEmp;
  end;



  TEmpleadosCambioEquipo = class( TObject )
  private
     slEmpleados : TStringList;
     slEquipos  : TStringList;
     slFecReg    : TStringList;    
     slDescripcion : TStringList;
     slObserva   : TStringList;
     slUsuarioWFN : TStringList;

     { Private declarations }
     function GetEmpleado( Index: Integer ): TNumEmp;
     function GetEquipo ( Index: Integer ): string;
     function GetUsuarioWFN ( Index: Integer ): TNumEmp;
     function GetFecReg ( Index: Integer ): TDateTime;
     function GetDescripcion ( Index: Integer ): string;
     function GetObserva ( Index: Integer ): string;

     public
     { Public declarations }
     constructor Create;
     destructor Destroy; override;
     property Empleado[ Index: Integer ]: TNumEmp read GetEmpleado;
     property Equipo[ Index: Integer ] : string read GetEquipo;
     property UsuarioWFN[ Index: Integer ] : TNumEmp read GetUsuarioWFN;
     property FecReg[ Index: Integer ] : TDateTime read GetFecReg;
     property Observa[ Index: Integer ] : string read GetObserva;
     property Descripcion[ Index: Integer ] : string read GetDescripcion;


     function AddEmpleadoCambioEquipo( const iEmpleado: Integer; const sEquipo: string; const dFecReg : TDatetime;
                           const sDescripcion : string; const sObserva : string; const iUsuarioWFN: Integer  ): TNumEmp;

  end;


  TBajaEmpleado = class( TObject )
  private
     slEmpleados : TStringList;
     slMotivo  : TStringList;
     slFechaReg    : TStringList;
     slFechaSS    : TStringList;
     slDescripcion : TStringList;
     slObserva   : TStringList;
     slYear   : TStringList;
     slTipo   : TStringList;
     slNumero   : TStringList;
     slUsuarioWFN : TStringList;
     slRecontratacion : TStringList;

     { Private declarations }
     function GetEmpleado( Index: Integer ): TNumEmp;
     function GetMotivo ( Index: Integer ): string;
     function GetFechaReg ( Index: Integer ): TDateTime;
     function GetFechaSS ( Index: Integer ): TDateTime;
     function GetDescripcion ( Index: Integer ): string;
     function GetObserva ( Index: Integer ): string;
     function GetYear( Index: Integer ): TNumEmp;
     function GetTipo( Index: Integer ): TNumEmp;
     function GetNumero( Index: Integer ): TNumEmp;
     function GetUsuarioWFN ( Index: Integer ): TNumEmp;
     function GetRecontratacion  ( Index: Integer ): string;

     public
     { Public declarations }
     constructor Create;
     destructor Destroy; override;
     property Empleado[ Index: Integer ]: TNumEmp read GetEmpleado;
     property Motivo[ Index: Integer ] : string read GetMotivo;
     property FechaReg[ Index: Integer ] : TDateTime read GetFechaReg;
     property FechaSS[ Index: Integer ] : TDateTime read GetFechaSS;
     property Descripcion[ Index: Integer ] : string read GetDescripcion;
     property Observa[ Index: Integer ] : string read GetObserva;

     property Year[ Index: Integer ]: TNumEmp read GetYear;
     property Tipo[ Index: Integer ]: TNumEmp read GetTipo;
     property Numero[ Index: Integer ]: TNumEmp read GetNumero;

     property UsuarioWFN[ Index: Integer ] : TNumEmp read GetUsuarioWFN;

      property Recontratacion[ Index: Integer ] : string read GetRecontratacion;

     function AddBajaEmpleado( const iEmpleado: Integer; const sMotivo: string; const dFechaReg : TDatetime;  const dFechaSs : TDatetime; const sDescripcion : string; const sObserva : string; const iYear: Integer; const iTipo: Integer; const iNumero: Integer; const iUsuarioWFN: Integer  ; const sRecontratacion: string ): TNumEmp;

  end;


  TEmpleadosWFN = class( TObject )
  private
     slEmpleados : TStringList;
     slClasifis  : TStringList;
     slFecReg    : TStringList;
     slDescripcion : TStringList;
     slObserva   : TStringList;
     slPuestos   : TStringList;
     slTurnos    : TStringList;

     { Private declarations }
     function GetEmpleado( Index: Integer ): TNumEmp;
     function GetClasifi ( Index: Integer ): TCodigo;
     function GetFecReg ( Index: Integer ): TDateTime;
     function GetDescripcion ( Index: Integer ): string;
     function GetObserva ( Index: Integer ): string;
     function GetPuesto ( Index: Integer ): TCodigo;
     function GetTurno ( Index: Integer ): TCodigo;

     public
     { Public declarations }
     constructor Create;
     destructor Destroy; override;
     property Empleado[ Index: Integer ]: TNumEmp read GetEmpleado;
     property Clasifi[ Index: Integer ] : TCodigo read GetClasifi;
     property Puesto[ Index: Integer ] : TCodigo read GetPuesto;
     property Turno[ Index: Integer ] : TCodigo read GetTurno;
     property FecReg[ Index: Integer ] : TDateTime read GetFecReg;
     property Observa[ Index: Integer ] : string read GetObserva;
     property Descripcion[ Index: Integer ] : string read GetDescripcion;
     function AddEmpleadoWFN( const iEmpleado: TNumEmp; const cClasifi : TCodigo;
                           const dFecReg : TDatetime; const sObserva : string ): TNumEmp;
     function AddEmpleadosWFN( const iEmpleado: TNumEmp; const cClasifi : TCodigo;
                           const dFecReg : TDatetime; const sObserva : string;  const cPuesto : TCodigo;  const cTurno : TCodigo; const sDescripcion : string): TNumEmp;
  end;

  TAutorizacion = class( TObject )
  private
     slEmpleados : TStringList;
     slFechas  : TStringList;
     slHoras  : TStringList;
     slTipos    : TStringList;
     slMotivos   : TStringList;
     slEmpresas   : TStringList;


     { Private declarations }
     function GetEmpleado( Index: Integer ): TNumEmp;
     function GetFecha ( Index: Integer ): TDateTime;
     function GetHora ( Index: Integer ): TPesos;
     function GetTipo( Index: Integer ): TCodigo;
     function GetMotivo ( Index: Integer ): TCodigo;
     function GetEmpresa ( Index: Integer ): TCodigo;
     function GetCount:Integer;
     function StrAsFechaCliente( sFecha: String ): TDateTime;

     public
     { Public declarations }
     constructor Create;
     destructor Destroy; override;
     property Empleado[ Index: Integer ]: TNumEmp read GetEmpleado;
     property Fecha[ Index: Integer ] : TDateTime read GetFecha;
     property Hora[ Index: Integer ] : TPesos read GetHora;
     property Tipo[ Index: Integer ] : TCodigo read GetTipo;
     property Motivo[ Index: Integer ] : TCodigo read GetMotivo;
     property Empresa[ Index: Integer ] : TCodigo read GetEmpresa;
     property Count:Integer read GetCount;
     function AddAutorizacion( const iEmpleado: TNumEmp; const dFecha : TDateTime; const fHoras : TPesos;
                           const sTipo : TCodigo; const sMotivo : TCodigo; const sEmpresa : TCodigo ): TNumEmp;
     procedure Clear;

  end;

  TEmpleadoSeguroPariente = class( TObject )
     public
     { Public declarations }
     UsuarioWFN : TNumEmp;
     NumeroEmpleado : TNumEmp;
     ParentescoID : TNumEmp;
     ParienteFolio : TNumEmp;
     AseguraTipo : TNumEmp;
     AseguraCertifi : string;
     AseguraCFijo : TPesos;
     AseguraEndoso : string;
     AseguraFinal : TDateTime;
     AseguraInicial : TDateTime;
     AseguraObserva : string;
     AseguraTipoPol : TNumEmp;
     SeguroCodigo : TCodigo;
     SeguVigReferen : string;
     constructor Create;
     destructor Destroy; override;
  end;

  TdmCliente = class(TBasicoCliente)
    cdsLista: TZetaClientDataSet;
    cdsComparte: TZetaClientDataSet;
    cdsEmpleado: TZetaClientDataSet;
    cdsImportInfonavit: TServerDataSet;
    dsCredNoActivosTress: TDataSource;
    cdsProcesos: TZetaClientDataSet;
    cdsLogDetail: TZetaClientDataSet;
    cdsLookupInfonavit: TZetaClientDataSet;
    cdsHisCreInfonavit: TZetaClientDataSet;
    cdsPuestos: TZetaLookupDataSet;
    cdsClasifi: TZetaLookupDataSet;
    cdsTurnos: TZetaLookupDataSet;
    cdsMotivoBaja: TZetaLookupDataSet;
    cdsEquipoNivelSupervisor: TZetaLookupDataSet;
    ZetaLookupDataSet1: TZetaLookupDataSet;
    cdsNivel1: TZetaLookupDataSet;
    cdsNivel2: TZetaLookupDataSet;
    cdsNivel3: TZetaLookupDataSet;
    cdsNivel4: TZetaLookupDataSet;
    cdsNivel5: TZetaLookupDataSet;
    cdsNivel6: TZetaLookupDataSet;
    cdsNivel7: TZetaLookupDataSet;
    cdsNivel8: TZetaLookupDataSet;
    cdsNivel9: TZetaLookupDataSet;
    cdsContratos: TZetaLookupDataSet;
    cdsEmpParientes: TZetaClientDataSet;
    cdsSegGastosMed: TZetaLookupDataSet;
    cdsHisSGM: TZetaClientDataSet;
    cdsVigenciasSGM: TZetaLookupDataSet;
    cdsAltaEmpleadosWFN: TServerDataSet;
    cdsEstado: TZetaLookupDataSet;
    cdsEditHisKardex: TZetaClientDataSet;
    cdsEmpleadoGenerico: TServerDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsEmpleadoAlEnviarDatos(Sender: TObject);
    procedure cdsImportInfonavitBeforePost(DataSet: TDataSet);
    procedure cdsHisCreInfonavitAlAdquirirDatos(Sender: TObject);
    procedure cdsPuestosAlAdquirirDatos(Sender: TObject);
    procedure cdsClasifiAlAdquirirDatos(Sender: TObject);
    procedure cdsTurnosAlAdquirirDatos(Sender: TObject);
    procedure cdsMotivoBajaAlAdquirirDatos(Sender: TObject);
    procedure cdsEquipoNivelSupervisorAlAdquirirDatos(Sender: TObject);

    procedure cdsNivel1AlAdquirirDatos(Sender: TObject);
    procedure cdsNivel2AlAdquirirDatos(Sender: TObject);
    procedure cdsNivel3AlAdquirirDatos(Sender: TObject);
    procedure cdsNivel4AlAdquirirDatos(Sender: TObject);
    procedure cdsNivel5AlAdquirirDatos(Sender: TObject);
    procedure cdsNivel6AlAdquirirDatos(Sender: TObject);
    procedure cdsNivel7AlAdquirirDatos(Sender: TObject);
    procedure cdsNivel8AlAdquirirDatos(Sender: TObject);
    procedure cdsNivel9AlAdquirirDatos(Sender: TObject);
    procedure cdsContratosAlAdquirirDatos(Sender: TObject);
    procedure cdsEmpParientesAlAdquirirDatos(Sender: TObject);
    procedure cdsHisSGMAlAdquirirDatos(Sender: TObject);
    procedure cdsHisSGMAlEnviarDatos(Sender: TObject);
    procedure cdsHisSGMBeforePost(DataSet: TDataSet);
    procedure cdsSegGastosMedAlAdquirirDatos(Sender: TObject);
    procedure cdsHisSGMNewRecord(DataSet: TDataSet);
    procedure cdsHisSGMReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
    procedure cdsEstadoAlAdquirirDatos(Sender: TObject);
    procedure cdsEditHisKardexAlEnviarDatos(Sender: TObject);
    procedure cdsEditHisKardexBeforePost(DataSet: TDataSet);

  private
    { Private declarations }
    FXMLTools: TdmXMLTools;
    FStatus: String;
    FErrores: TStrings;
    FEmpleados: TEmpleados;
    FEmpleadosPE: TEmpleadosPE;
    FEmpleadosCE: TEmpleadosCambioEquipo;
    FEmpleadosBaja: TBajaEmpleado;
    FEmpleadosWFN: TEmpleadosWFN;
    FAutorizaciones:TAutorizacion;
    FHuboErrores, FHuboErrorEnviar : Boolean;
    AgregandoKardex, FRefrescaHisKardex : Boolean;
    FFechaIngreso: TDate;
    FFechaAntiguedad: TDate;
    FEmpleado: TNumEmp;
    FEmpleadoBOF: Boolean;
    FEmpleadoEOF: Boolean;
    FClienteProcessList : TProcessList;
    FClienteProcessData: TProcessInfo;
    FValidacionInfonavit: Boolean;
    FValidacionSGM: Boolean;
    FXMLWFN : String;
    FFolio: integer;
    Provider : TdmZetaServerProvider;
    {$ifdef DOS_CAPAS}
    function GetServerRecursos: TdmServerRecursos;
    function GetServerAsistencia: TdmServerAsistencia;
    function GetServerComparte: TdmServerComparte;
    function GetServerLogin: TdmServerLogin;
    function GetServerConsultas: TdmServerConsultas;
    fucntion GetServerNomina: TdmServerNomina;
    {$else}
    FServerRecursos: IdmServerRecursosDisp;
    FServerCalcNomina: IdmServerCalcNominaDisp;
    FServerAsistencia: IdmServerAsistenciaDisp;
    FServerComparte: IdmServerComparteDisp;
    FServerLogin: IdmServerLoginDisp;
    FServerConsultas: IdmServerConsultasDisp;
    FServerNomina: IdmServerNominaDisp;
    FServerCatalogos: IdmServerCatalogosDisp;
    FServerTablas: IdmServerTablasDisp;
    function GetServerCalcNomina: IdmServerCalcNominaDisp;
    function GetServerRecursos: IdmServerRecursosDisp;
    function GetServerAsistencia: IdmServerAsistenciaDisp;
    function GetServerComparte: IdmServerComparteDisp;
    function GetServerLogin: IdmServerLoginDisp;
    function GetServerConsultas: IdmServerConsultasDisp;
    function GetServerNomina: IdmServerNominaDisp;
    function GetServerCatalogos: IdmServerCatalogosDisp;
    function GetServerTablas: IdmServerTablasDisp;
    {$endif}
    function AgregarAutorizacionHoras( const iUsuario: Integer ): Boolean;
    function AgregarAutorizacionHorasMultiple( const iUsuario: Integer ): Boolean;
    {$ifdef LEONI}
    function AgregarExtrasLeoni( const iUsuario: integer ): boolean;
    {$endif}
    function AgregarCambiosMultiples: Boolean;
    function AgregarCambioSalario: Boolean;
    function AgregarPermiso( const iUsuario: Integer ): Boolean;
    function AgregarVacacion( const iUsuario: Integer ): Boolean;
    function FetchEmployee( oDataSet: TZetaClientDataSet; const iEmpleado: TNumEmp ): Boolean;
    function GetPercepcionesFijas( const iEmpleado: Integer; const dFecha: TDateTime ): String;
    function InscribirACurso( const iUsuario: integer): Boolean;
    function AgregaTiempoExtra( iUsuario:integer) : Boolean;
    function LogAsText: String;
    function AgregarCambioPlaza: Boolean;
    function AgregarCambiosPE: Boolean;
    function AgregarCambioSalarioWFN: Boolean;
    function AgregarCambioJornadaPresbaja : Boolean;
    function AgregarCambiosMultiplesPresbaja : Boolean;
    function AgregarCambioAreasWFN : Boolean;
    function AgregarCambioContratoWFN : Boolean;
    function CRUDParienteEmpleadoWFN (sTipo: string) : Boolean;
    function ObtenValUso( const sTipo: String ): Integer;
    function GetAuto: OleVariant;
    function AgregarExcepcionesNomina( const iUsuario: Integer ): Boolean;//Excepciones de Nomina
    function AgregarModificacionColabora( const iUsuario: Integer ): Boolean;//Modificacion  a Colabora
    function SetEmpleadoNumero( oDataSet: TZetaClientDataSet; const iEmpleado: TNumEmp ): Boolean;
    function AgregarInfonavit( const iUsuario:integer) : Boolean; //PENSKE Credito Infonavt
    function ModificarInfonavit( const iUsuario:integer) : Boolean; //PENSKE Credito Infonavit
    function SuspenderInfonavitWFN( const iUsuario:integer) : Boolean; //PENSKE Credito Infonavit
    function ReiniciarInfonavitWFN( const iUsuario:integer) : Boolean; //PENSKE Credito Infonavit
    function AgregarIncapacidadWFN( const iUsuario:integer) : Boolean; //PENSKE Agregar Incapacidad
    function AgregarPrestamoWFN( const iUsuario:integer) : Boolean;
    function AgregarCambioEquipoWFN( const iUsuario:integer) : Boolean; //Cambio de Equipo
    function BajaEmpleadoWFN( const iUsuario:integer) : Boolean; //Baja de Empleado
    function CambiarPuestoWFN: Boolean; //Cambiar Puesto Empleado
    function CambiarTurnoWFN: Boolean; //Cambiar Turno Empleado
    function AgregarSeguroEmpWFN : Boolean;//PENSKE Seguro Gasto Medico
    function EmpWFN_ValidarTarjetaDespensa( iCB_CODIGO :integer; sTarjetaDespensa: string; var sNroEmpleaTDespensaRepetida : string  ): boolean;
    function AgregarEmpWFN: Boolean; //US 1111: Agregar Alta de Empleado @DLSJ 2016-02-15
    function ReingresoEmpWFN: Boolean; //US : Reingreso de Empleado @2016-09-28
    function AgregarChecadaWFN( const iUsuario:integer ) : Boolean;
    procedure ValidaEnviar(DataSet: TZetaClientDataSet; var Texto: TStringList);
    function ValidaEnviarSGM(DataSet: TZetaClientDataSet): Boolean;//PENSKE Seguro Gastos Medicos
    procedure CargarEmpleados;
    procedure CargarEmpleadosPE;
    procedure CargarEmpleadosJornadaPresbaja;
    procedure CargarEmpleadosWFN;
    procedure CargarAutorizaciones;
    procedure InitCamposSalario( const iEmpleado: Integer; var dFechaSal: TDateTime );
    procedure InitCamposNiveles( const iEmpleado: Integer; var dFechaSal: TDateTime );
    procedure LogInit;
    procedure LogError( const iEmpleado: Integer; const sTexto: String );
    procedure ReconcileError(DataSet: {$ifdef VER130}TClientDataSet{$else}TCustomClientDataSet{$endif}; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure ReconcileErrorAuto(DataSet: {$ifdef VER130}TClientDataSet{$else}TCustomClientDataSet{$endif}; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure ReconcileErrorInscrito(DataSet: {$ifdef VER130}TClientDataSet{$else}TCustomClientDataSet{$endif}; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure UpdateNodeInfo( oDataSet: TZetaClientDataSet; const sField: String; Padre: TZetaXMLNode = nil );
    procedure GrabaValUso( const sTipo: String; const iUso: Integer );
    procedure ImportarCreditosInfonavit( var lOkProcess: Boolean; var Texto: TStringList ); //PENSKE Credito Infonavit
    procedure ActualizarInfonavit( var lOkProcess: Boolean; var Texto: TStringList ); //PENSKE Credito Infonavit
    procedure SuspenderInfonavit( var lOkProcess: Boolean; var Texto: TStringList ); //PENSKE Credito Infonavit
    procedure ReiniciarInfonavit( var lOkProcess: Boolean; var Texto: TStringList ); //PENSKE Credito Infonavit
    procedure ProcesoAltaEmpleadosWFN( var lOkProcess: Boolean; var Texto: TStringList ); //US 1111: Agregar Alta de Empleado @DLSJ 2016-02-15
    //Eventos de Bitacora
    procedure InitProcessLog();
    procedure EscribeBitacora(Resultado: OleVariant; Texto: TStringList);
    function Check( const Resultado: OleVariant; Texto: TStringList ): Boolean;
    procedure LeerMensajeBitacora( var lOkProcess: Boolean; var Texto: TStringList );
    procedure GetProcessLog(const iProceso: Integer; Texto: TStringList);
    procedure EndProcessLog();

    procedure ValidaCreditoInfo(DataSet: TDataSet);
    function EsValidoPuesto(const sCodigo: String): Boolean;
    function EsValidoClasificacion(const sCodigo: String): Boolean;
    function EsValidoTurno(const sCodigo: String): Boolean;
    function EsValidoMotivoBaja(const sCodigo: String): Boolean;
    function EsValidoNivelSupervisor(const sCodigo: String): Boolean;

    function EsValidoNivel1(const sCodigo: String): Boolean;
    function EsValidoContrato(const sCodigo: String): Boolean;
    function EsValidoNivel2(const sCodigo: String): Boolean;
    function EsValidoNivel3(const sCodigo: String): Boolean;
    function EsValidoNivel4(const sCodigo: String): Boolean;
    function EsValidoNivel5(const sCodigo: String): Boolean;
    function EsValidoNivel6(const sCodigo: String): Boolean;
    function EsValidoNivel7(const sCodigo: String): Boolean;
    function EsValidoNivel8(const sCodigo: String): Boolean;
    function EsValidoNivel9(const sCodigo: String): Boolean;

    //PENSKE ** Seguros Medicos
    function EsValidoPariente(const iParentescoID: Integer; const iParienteFolio: Integer): Boolean;
    function EsValidoTipoPoliza(const iAseguraTipoPol: Integer) : Boolean;
    function EsValidoTipoAsegurado(const iAseguraTipo: Integer) : Boolean;
    function EsValidoReferenciaMed(const sSeguroCodigo: String; const sSeguVigReferen: String): Boolean;
    function EsValidoSeguroCodigo(const sSeguroCodigo: String): Boolean;
    function GetMaxOrdenPolizas(const sSeguroCodigo,sSeguVigReferen:string):Integer;

    function IntercambiarFestivoWFN : boolean;

  protected
    { Protected declarations }
    {$ifdef DOS_CAPAS}
    property ServerRecursos :TdmServerRecursos read GetServerRecursos;
    property ServerAsistencia :TdmServerAsistencia read GetServerAsistencia;
    property ServerComparte: TdmServerComparte read GetServerComparte;
    property ServerLogin : TdmServerLogin read GetServerLogin;
    property ServerConsultas: TdmServerConsultas read GetServerConsultas;
    property ServerNomina: TdmServerNomina read GetServerNomina;
    property ServerCatalogos: TdmServerCatalogos read GetServerCatalogos;
    property ServerTablas: TdmServerTablas read GetServerTablas;
    {$else}
    property ServerRecursos: IdmServerRecursosDisp read GetServerRecursos;
    property ServerCalcNomina: IdmServerCalcNominaDisp read GetServerCalcNomina;
    property ServerAsistencia: IdmServerAsistenciaDisp read GetServerAsistencia;
    property ServerComparte: IdmServerComparteDisp read GetServerComparte;
    property ServerLogin : IdmServerLoginDisp read GetServerLogin;
    property ServerConsultas: IdmServerConsultasDisp read GetServerConsultas;
    property ServerNomina: IdmServerNominaDisp read GetServerNomina;
    property ServerCatalogos: IdmServerCatalogosDisp read GetServerCatalogos;
    property ServerTablas: IdmServerTablasDisp read GetServerTablas;
    {$endif}
    property Status: String read FStatus write FStatus;
    property ValidacionInfonavit: Boolean read FValidacionInfonavit write FValidacionInfonavit;
    property XMLWFN: String read FXMLWFN write FXMLWFN;
    function InitCompanies: Boolean;
    function SetEmpresaActiva( const sCodigo: String ): Boolean;
    function TressInvocar( const sTipo: String; const iUsuario: Integer ): Boolean;
  public
    { Public declarations }
    property ProcessData: TProcessInfo read FClienteProcessData;
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    procedure CargarAutorizacion;
    function InitComparteCompany: Integer;
    function TressGrabar(const sXML: String; const iUsuario: Integer; {$ifdef LEONI} const iFolio: integer; {$endif} var sLog: String): Boolean;
    procedure InitDatosKardexInfonavit;


  end;

var
  dmCliente: TdmCliente;

implementation

uses FAutoClasses,
     ZetaCommonTools,
     ZetaClientTools,
     FToolsRH,
     ZetaRegistryCliente,
     ZetaServerTools,
     DateUtils, XMLIntf, Contnrs,XMLDoc;


{$R *.DFM}

const
     K_EMPLEADO_NULO = 0;
     K_DEFAULT_RECONTRATO = 'N';
     K_DEFAULT_AUTOSALARIO = 'N';

{ ************ TEmpleados ************* }

constructor TEmpleados.Create;
begin
     inherited Create;
end;

destructor TEmpleados.Destroy;
begin
     inherited Destroy;
end;

function TEmpleados.AddEmpleado( const iEmpleado: Integer ): TNumEmp;
begin
     Result := iEmpleado;
     Add( IntToStr( Result ) );
end;


function TEmpleados.GetEmpleado( Index: Integer ): TNumEmp;
begin
     Result := StrToIntDef( Strings[ Index ], K_EMPLEADO_NULO );
end;

{ ************ TEmpleadosPE ************* }

constructor TEmpleadosPE.Create;
begin
     inherited Create;
     slEmpleados := TStringList.Create;
     slSalarios  := TStringList.Create;
     slClasifis  := TStringList.Create;
     slFecReg    := TStringList.Create;
     slFecIMSS   := TStringList.Create;
     slObserva   := TStringList.Create;
     slPuestos  := TStringList.Create;
     slTurnos  := TStringList.Create;
     slDescripcion  := TStringList.Create;
end;

destructor TEmpleadosPE.Destroy;
begin
     inherited Destroy;
     FreeAndNil( slEmpleados );
     FreeAndNil( slSalarios  );
     FreeAndNil( slClasifis );
     FreeAndNil( slPuestos );
     FreeAndNil( slTurnos );
     FreeAndNil( slFecReg );
     FreeAndNil( slFecIMSS);
     FreeAndNil( slObserva );
     FreeAndNil( slDescripcion );
end;

function TEmpleadosPE.AddEmpleado( const iEmpleado: Integer; const pSalario : TPesos; const cClasifi : TCodigo;
                                   const dFecReg : TDatetime; const dFecIMSS : TDateTime; const sObserva : string ): TNumEmp;
begin

     Result := iEmpleado;
     slEmpleados.Add( IntToStr( Result ) );
     slSalarios.Add( FloatToStr(pSalario) );
     slClasifis.Add( cClasifi );

     slFecReg.Add( FechaAsStr( dFecReg ) );
     slFecIMSS.Add( FechaAsStr( dFecIMSS) );
     slObserva.Add( sObserva );
end;


function TEmpleadosPE.AddEmpleadoJornada( const iEmpleado: Integer; const pSalario : TPesos; const cClasifi : TCodigo;
                                   const dFecReg : TDatetime; const dFecIMSS : TDateTime; const sObserva : string ;  const cPuesto : TCodigo;  const cTurno : TCodigo;  const sDescripcion : string  ): TNumEmp;
begin

     Result := iEmpleado;

     AddEmpleado( iEmpleado, pSalario, cClasifi, dFecReg, dFecIMSS, sObserva);
     slPuestos.Add( cPuesto );
     slTurnos.Add( cTurno );
     slDescripcion.Add( sDescripcion);


end;



function TEmpleadosPE.GetEmpleado( Index: Integer ): TNumEmp;
begin
     Result := StrToIntDef( slEmpleados.Strings[ Index ], K_EMPLEADO_NULO );
end;

function TEmpleadosPE.GetSalario ( Index: Integer ): TPesos;
begin
     Result := StrToReal( slSalarios.Strings[ Index ] );
end;

function TEmpleadosPE.GetClasifi ( Index: Integer ): TCodigo;
begin
     Result :=  slClasifis.Strings[ Index ];
end;

function TEmpleadosPE.GetFecReg ( Index: Integer ): TDateTime;
begin
     Result :=  StrAsFecha( slFecReg.Strings[ Index ] );
end;

function TEmpleadosPE.GetFecIMSS ( Index: Integer ): TDateTime;
begin
     Result :=  StrAsFecha( slFecIMSS.Strings[ Index ] );
end;

function TEmpleadosPE.GetObserva ( Index: Integer ): string;
begin
      Result :=  slObserva.Strings[ Index ];
end;

function TEmpleadosPE.GetPuesto ( Index: Integer ): TCodigo;
begin
     if ( slPuestos.Count - 1)  >= Index then
          Result :=  slPuestos.Strings[ Index ]
     else
         Result := VACIO;
end;

function TEmpleadosPE.GetTurno ( Index: Integer ): TCodigo;
begin
     if ( slTurnos.Count - 1)  >= Index then
          Result :=  slTurnos.Strings[ Index ]
     else
         Result := VACIO;
end;

function TEmpleadosPE.GetDescripcion( Index: Integer ): string;
begin
     if ( slTurnos.Count - 1)  >= Index then
         Result :=  slDescripcion.Strings[ Index ]
     else
         Result := VACIO;
end;


{ ************ TBajaEmpleado ************* }

constructor TBajaEmpleado.Create;
begin
     inherited Create;
     slEmpleados := TStringList.Create;
     slMotivo  := TStringList.Create;
     slFechaReg    := TStringList.Create;
     slFechaSS    := TStringList.Create;
     slDescripcion  := TStringList.Create;
     slObserva   := TStringList.Create;
     slYear   := TStringList.Create;
     slTipo   := TStringList.Create;
     slNumero   := TStringList.Create;
     slUsuarioWFN  := TStringList.Create;
     slRecontratacion  := TStringList.Create;
end;

destructor TBajaEmpleado.Destroy;
begin
     inherited Destroy;
     FreeAndNil( slEmpleados );
     FreeAndNil( slMotivo  );
     FreeAndNil( slFechaReg  );
     FreeAndNil( slFechaSS );
     FreeAndNil( slDescripcion );
     FreeAndNil( slObserva );
     FreeAndNil( slYear  );
     FreeAndNil( slTipo );
     FreeAndNil( slNumero );
     FreeAndNil( slUsuarioWFN );
     FreeAndNil( slRecontratacion );
end;

function TBajaEmpleado.AddBajaEmpleado( const iEmpleado: Integer; const sMotivo: string; const dFechaReg : TDatetime;  const dFechaSS : TDatetime; const sDescripcion : string; const sObserva : string; const iYear: Integer; const iTipo: Integer; const iNumero: Integer; const iUsuarioWFN: Integer ; const sRecontratacion: string ): TNumEmp;
begin
     Result := iEmpleado;
     slEmpleados.Add( IntToStr( Result ) );
     slMotivo.Add(  sMotivo );
     slFechaReg.Add( FechaAsStr( dFechaReg ) );
     slFechaSS.Add( FechaAsStr( dFechaSS ) );
     slDescripcion.Add( sDescripcion );
     slObserva.Add( sObserva );
     slYear.Add( IntToStr( iYear ) );
     slTipo.Add( IntToStr( iTipo ) );
     slNumero.Add( IntToStr( iNumero ) );
     slUsuarioWFN.Add(IntToStr(iUsuarioWFN));
     slRecontratacion.Add(sRecontratacion);
end;

function TBajaEmpleado.GetEmpleado( Index: Integer ): TNumEmp;
begin
     Result := StrToIntDef( slEmpleados.Strings[ Index ], K_EMPLEADO_NULO );
end;

function TBajaEmpleado.GetMotivo ( Index: Integer ): string;
begin
     Result :=  slMotivo.Strings[ Index ];
end;

function TBajaEmpleado.GetFechaReg ( Index: Integer ): TDateTime;
begin
     Result :=  StrAsFecha( slFechaReg.Strings[ Index ] );
end;

function TBajaEmpleado.GetFechaSS ( Index: Integer ): TDateTime;
begin
     Result :=  StrAsFecha( slFechaSS.Strings[ Index ] );
end;

function TBajaEmpleado.GetDescripcion( Index: Integer ): string;
begin
         Result :=  slDescripcion.Strings[ Index ]
end;

function TBajaEmpleado.GetObserva ( Index: Integer ): string;
begin
      Result :=  slObserva.Strings[ Index ];
end;

function TBajaEmpleado.GetUsuarioWFN ( Index: Integer ): TNumEmp;
begin
     Result := StrToIntDef( slUsuarioWFN.Strings[ Index ], K_EMPLEADO_NULO );
end;

function TBajaEmpleado.GetYear ( Index: Integer ): TNumEmp;
begin
     Result := StrToIntDef( slYear.Strings[ Index ], K_EMPLEADO_NULO );
end;

function TBajaEmpleado.GetTipo ( Index: Integer ): TNumEmp;
begin
     Result := StrToIntDef( slTipo.Strings[ Index ], K_EMPLEADO_NULO );
end;

function TBajaEmpleado.GetNumero ( Index: Integer ): TNumEmp;
begin
     Result := StrToIntDef( slNumero.Strings[ Index ], K_EMPLEADO_NULO );
end;

function TBajaEmpleado.GetRecontratacion ( Index: Integer ): string;
begin
     Result := slRecontratacion.Strings[ Index ];
end;

{ ************ TEmpleadosCambioEquipo ************* }

constructor TEmpleadosCambioEquipo.Create;
begin
     inherited Create;
     slEmpleados := TStringList.Create;
     slEquipos  := TStringList.Create;
     slUsuarioWFN  := TStringList.Create;
     slFecReg    := TStringList.Create;
     slObserva   := TStringList.Create;
     slDescripcion  := TStringList.Create;
end;

destructor TEmpleadosCambioEquipo.Destroy;
begin
     inherited Destroy;
     FreeAndNil( slEmpleados );
     FreeAndNil( slEquipos  );
     FreeAndNil( slUsuarioWFN  );
     FreeAndNil( slFecReg );
     FreeAndNil( slObserva );
     FreeAndNil( slDescripcion );
end;

function TEmpleadosCambioEquipo.AddEmpleadoCambioEquipo( const iEmpleado: Integer; const sEquipo: string; const dFecReg : TDatetime; const sDescripcion : string; const sObserva : string; const iUsuarioWFN: Integer ): TNumEmp;
begin
     Result := iEmpleado;
     slEmpleados.Add( IntToStr( Result ) );
     slEquipos.Add(  sEquipo );

     slFecReg.Add( FechaAsStr( dFecReg ) );

     slDescripcion.Add( sDescripcion );
     slObserva.Add( sObserva );

     slUsuarioWFN.Add(IntToStr(iUsuarioWFN));
end;

function TEmpleadosCambioEquipo.GetEmpleado( Index: Integer ): TNumEmp;
begin
     Result := StrToIntDef( slEmpleados.Strings[ Index ], K_EMPLEADO_NULO );
end;

function TEmpleadosCambioEquipo.GetEquipo ( Index: Integer ): string;
begin
     Result :=  slEquipos.Strings[ Index ];
end;

function TEmpleadosCambioEquipo.GetUsuarioWFN ( Index: Integer ): TNumEmp;
begin
     Result := StrToIntDef( slUsuarioWFN.Strings[ Index ], K_EMPLEADO_NULO );
end;

function TEmpleadosCambioEquipo.GetFecReg ( Index: Integer ): TDateTime;
begin
     Result :=  StrAsFecha( slFecReg.Strings[ Index ] );
end;

function TEmpleadosCambioEquipo.GetObserva ( Index: Integer ): string;
begin
      Result :=  slObserva.Strings[ Index ];
end;

function TEmpleadosCambioEquipo.GetDescripcion( Index: Integer ): string;
begin
         Result :=  slDescripcion.Strings[ Index ]
end;

{ ************ TEmpleadosWFN ************* }

constructor TEmpleadosWFN.Create;
begin
     inherited Create;
     slEmpleados := TStringList.Create;
     slClasifis  := TStringList.Create;
     slFecReg    := TStringList.Create;
     slObserva   := TStringList.Create;
     slPuestos  := TStringList.Create;
     slTurnos  := TStringList.Create;
     slDescripcion  := TStringList.Create;
end;

destructor TEmpleadosWFN.Destroy;
begin
     inherited Destroy;
     FreeAndNil( slEmpleados );
     FreeAndNil( slClasifis );
     FreeAndNil( slPuestos );
     FreeAndNil( slTurnos );
     FreeAndNil( slFecReg );
     FreeAndNil( slObserva );
     FreeAndNil( slDescripcion );
end;

function TEmpleadosWFN.AddEmpleadoWFN( const iEmpleado: Integer; const cClasifi : TCodigo;
                                   const dFecReg : TDatetime; const sObserva : string ): TNumEmp;
begin
     Result := iEmpleado;
     slEmpleados.Add( IntToStr( Result ) );
     slClasifis.Add( cClasifi );
     slFecReg.Add( FechaAsStr( dFecReg ) );
     slObserva.Add( sObserva );
end;

function TEmpleadosWFN.AddEmpleadosWFN( const iEmpleado: Integer; const cClasifi : TCodigo;
                                   const dFecReg : TDatetime; const sObserva : string ;  const cPuesto : TCodigo;  const cTurno : TCodigo;  const sDescripcion : string  ): TNumEmp;
begin
     Result := iEmpleado;
     AddEmpleadoWFN( iEmpleado, cClasifi, dFecReg, sObserva);
     slPuestos.Add( cPuesto );
     slTurnos.Add( cTurno );
     slDescripcion.Add( sDescripcion);
end;



function TEmpleadosWFN.GetEmpleado( Index: Integer ): TNumEmp;
begin
     Result := StrToIntDef( slEmpleados.Strings[ Index ], K_EMPLEADO_NULO );
end;

function TEmpleadosWFN.GetClasifi ( Index: Integer ): TCodigo;
begin
     Result :=  slClasifis.Strings[ Index ];
end;

function TEmpleadosWFN.GetFecReg ( Index: Integer ): TDateTime;
begin
     Result :=  StrAsFecha( slFecReg.Strings[ Index ] );
end;

function TEmpleadosWFN.GetObserva ( Index: Integer ): string;
begin
      Result :=  slObserva.Strings[ Index ];
end;

function TEmpleadosWFN.GetPuesto ( Index: Integer ): TCodigo;
begin
     if ( slPuestos.Count - 1)  >= Index then
          Result :=  slPuestos.Strings[ Index ]
     else
         Result := VACIO;
end;

function TEmpleadosWFN.GetTurno ( Index: Integer ): TCodigo;
begin
     if ( slTurnos.Count - 1)  >= Index then
          Result :=  slTurnos.Strings[ Index ]
     else
         Result := VACIO;
end;

function TEmpleadosWFN.GetDescripcion( Index: Integer ): string;
begin
     if ( slTurnos.Count - 1)  >= Index then
         Result :=  slDescripcion.Strings[ Index ]
     else
         Result := VACIO;
end;


{ ************ TAutorizacion ************* }

constructor TAutorizacion.Create;
begin
     inherited Create;
     slEmpleados := TStringList.Create;
     slFechas  := TStringList.Create;
     slHoras  := TStringList.Create;
     slTipos    := TStringList.Create;
     slMotivos   := TStringList.Create;
     slEmpresas :=  TStringList.Create;
end;

destructor TAutorizacion.Destroy;
begin
     inherited Destroy;
     FreeAndNil( slEmpleados );
     FreeAndNil( slFechas  );
     FreeAndNil( slHoras );
     FreeAndNil( slTipos );
     FreeAndNil( slMotivos);
     FreeAndNil( slEmpresas);
end;

function TAutorizacion.AddAutorizacion( const iEmpleado: TNumEmp; const dFecha : TDateTime; const fHoras : TPesos;
                           const sTipo : TCodigo; const sMotivo : TCodigo; const sEmpresa : TCodigo ): TNumEmp;
begin
     Result := iEmpleado;
     slEmpleados.Add( IntToStr( Result ) );
     slFechas.Add( DateToStrSQL(dFecha) );
     slHoras.Add( FloatToStr(fHoras) );
     slTipos.Add( sTipo );
     slMotivos.Add( sMotivo );
     slEmpresas.Add(sEmpresa);
end;

function TAutorizacion.GetEmpleado( Index: Integer ): TNumEmp;
begin
     Result := StrToIntDef( slEmpleados.Strings[ Index ], K_EMPLEADO_NULO );
end;

function TAutorizacion.GetHora ( Index: Integer ): TPesos;
begin
     Result := StrToReal( slHoras.Strings[ Index ] );
end;

function TAutorizacion.GetFecha ( Index: Integer ): TDateTime;
begin
     Result :=  StrAsFechaCliente( slFechas.Strings[ Index ] ); //OP: 9.Julio.2010
end;

function TAutorizacion.GetTipo ( Index: Integer ): TCodigo;
begin
     Result :=  slTipos.Strings[ Index ];
end;

function TAutorizacion.GetMotivo ( Index: Integer ): TCodigo;
begin
     Result :=  slMotivos.Strings[ Index ];
end;

function TAutorizacion.GetEmpresa ( Index: Integer ): TCodigo;
begin
     Result :=  slEmpresas.Strings[ Index ];
end;

procedure TAutorizacion.Clear;
begin
     slEmpleados.Clear;
     slFechas.Clear;
     slHoras.Clear;
     slTipos.Clear;
     slMotivos.Clear;
     slEmpresas.Clear;
end;

function TAutorizacion.GetCount:Integer;
begin
     Result := slEmpleados.Count;
end;

{ OP: 9.Julio.2010 }
function TAutorizacion.StrAsFechaCliente( sFecha: String ): TDateTime; // Formato DDMMYYYY � DDMMYY //

  function GetSiguiente: String;
  var
     iPos: Word;
  begin
       iPos := Pos( '/', sFecha );
       Result := Copy( sFecha, 0, ( iPos - 1 ) );
       sFecha := Copy( sFecha, ( iPos + 1 ), MaxInt );
  end;

var
   sDia, sMes: String;
begin
     sMes := GetSiguiente;
     sDia := GetSiguiente;
     Result := CodificaFecha( StrAsInteger( sFecha ),
                              StrAsInteger( sMes ),
                              StrAsInteger( sDia ) );
end;

{ ********* TdmCliente ********** }

constructor TdmCliente.Create(AOwner: TComponent);
begin
     ZetaCommonTools.Environment;
     ZetaRegistryCliente.InitClientRegistry;
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     {$endif}
     inherited Create( AOwner );
end;

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
    FServerRecursos:= nil;
    FServerAsistencia:= nil;
    FServerComparte:= nil;
    FServerLogin:= nil;
    FServerConsultas:= nil;
    FServerNomina:= nil;
    FServerCatalogos:= nil;
    FServerTablas:= nil;
    FServerCalcNomina:= nil;

     FXMLTools := TdmXMLTools.Create( Self );
     {$ifdef DOS_CAPAS}
     FServerRecursos := TdmServerRecursos.Create( Self );
     FServerLogin := TdmServerLogin.Create( Self );
     {$endif}
     FErrores := TStringList.Create;
     FEmpleados := TEmpleados.Create;
     FAutorizaciones := TAutorizacion.Create;
     Provider := DZetaServerProvider.GetZetaProvider ( Self );
     dmIncapacidad := TdmIncapacidad.Create( Self );
     inherited;
end;

destructor TdmCliente.Destroy;
begin
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll(TRUE);
     DZetaServerProvider.FreeAll;
     {$endif}
     ZetaRegistryCliente.ClearClientRegistry;
     FreeAndNil( dmIncapacidad );
     inherited Destroy;
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FEmpleados );
     FreeAndNil( FErrores );
     FreeAndNil( FXMLTools );
     FreeAndNil( FAutorizaciones );
     {$ifdef DOS_CAPAS}
     FreeAndNil( FServerLogin );
     FreeAndNil( FServerRecursos );
     {$endif}

     DZetaServerProvider.FreeZetaProvider( Provider );
end;

{$ifdef DOS_CAPAS}
function TdmCliente.GetServerRecursos: TdmServerRecursos;
begin
     Result:= FServerRecursos;
end;
function TdmCliente.GetServerAsistencia: TdmServerAsistencia;
begin
     Result:= FServerAsistencia;
end;
function TdmCliente.GetServerComparte: TdmServerComparte;
begin
     Result:= FServerComparte;
end;
function TdmCliente.GetServerLogin: TdmServerLogin;
begin
     Result := FServerLogin;
end;
function TdmCliente.GetServerConsultas: TdmServerConsultas;
begin
     Result := FServerConsultas;
end;
function TdmCliente.GetServerNomina: TdmServerNomina;
begin
    Result := FServerNomina;
end;
function TdmCliente.GetServerCatalogos: TdmServerCatalogos;
begin
    Result := FServerCatalogos;
end;
function TdmCliente.GetServerTablas: TdmServerTablas;
begin
    Result := FServerTablas;
end;
{$else}
function TdmCliente.GetServerRecursos: IdmServerRecursosDisp;
begin
     Result:= IdmServerRecursosDisp( Self.CreaServidor( CLASS_dmServerRecursos, FServerRecursos ) );
end;
function TdmCliente.GetServerCalcNomina: IdmServerCalcNominaDisp;
begin
     Result := IdmServerCalcNominaDisp( Self.CreaServidor( CLASS_dmServerCalcNomina, FServerCalcNomina ) );
end;
function TdmCliente.GetServerAsistencia: IdmServerAsistenciaDisp;
begin
     Result:= IdmServerAsistenciaDisp( Self.CreaServidor( CLASS_dmServerAsistencia, FServerAsistencia ) );
end;
function TdmCliente.GetServerComparte: IdmServerComparteDisp;
begin
     Result:= IdmServerComparteDisp( Self.CreaServidor( CLASS_dmServerComparte, FServerComparte ) );
end;
function TdmCliente.GetServerLogin: IdmServerLoginDisp;
begin
     Result := IdmServerLoginDisp( Self.CreaServidor( CLASS_dmServerLogin,FServerLogin ) );
end;
function TdmCliente.GetServerConsultas: IdmServerConsultasDisp;
begin
     Result := IdmServerConsultasDisp( Self.CreaServidor( CLASS_dmServerConsultas, FServerConsultas ) );
end;
function TdmCliente.GetServerNomina : IdmServerNominaDisp;
begin
     Result := IdmServerNominaDisp( Self.CreaServidor( CLASS_dmServerNomina, FServerNomina ) );
end;
function TdmCliente.GetServerCatalogos : IdmServerCatalogosDisp;
begin
     Result := IdmServerCatalogosDisp( Self.CreaServidor( CLASS_dmServerCatalogos, FServerCatalogos ) );
end;
function TdmCliente.GetServerTablas : IdmServerTablasDisp;
begin
     Result := IdmServerTablasDisp( Self.CreaServidor( CLASS_dmServerTablas, FServerTablas ) );
end;
{$endif}

function TdmCliente.GetAuto: OleVariant;
begin
     Result := GetServerLogin.GetAuto;
end;

{ ****** Manejos Internos ****** }

function TdmCliente.InitCompanies: Boolean;
begin
     with cdsCompany do
     begin
          if not Active or HayQueRefrescar then
          begin
               Data := Servidor.GetCompanys( Usuario, Ord( tc3Datos ) );
               ResetDataChange;
          end;
          Result := ( RecordCount > 0 );
     end;
end;

function TdmCliente.SetEmpresaActiva(const sCodigo: String): Boolean;
begin
     InitCompanies;
     Result := FindCompany( sCodigo );
     if Result then
     begin
          SetCompany;
     end;
end;

procedure TdmCliente.ReconcileError(DataSet: {$ifdef VER130}TClientDataSet{$else}TCustomClientDataSet{$endif}; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := raAbort;  // Aborta el reconcile para que regrese FALSE
     Status := E.Message;
end;

procedure TdmCliente.ReconcileErrorAuto(DataSet: {$ifdef VER130}TClientDataSet{$else}TCustomClientDataSet{$endif}; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := raAbort;  // Aborta el reconcile para que regrese FALSE
     if PK_Violation( E ) then
     begin
          with DataSet do
          begin
               Status := Format( 'El empleado %d ya tiene una autorizaci�n de %s para el %s', [
                                 FieldByName( 'CB_CODIGO' ).AsInteger,
                                 ZetaCommonLists.ObtieneElemento( lfAutorizaChecadas, FieldByName( 'CH_TIPO' ).AsInteger - K_OFFSET_AUTORIZACION ),
                                 ZetaCommonTools.FechaCorta( FieldByName( 'AU_FECHA' ).AsDateTime ) ] );
          end;
     end
     else
         Status := E.Message;
end;

procedure TdmCliente.ReconcileErrorInscrito(DataSet: {$ifdef VER130}TClientDataSet{$else}TCustomClientDataSet{$endif}; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := raAbort;  // Aborta el reconcile para que regrese FALSE
     if PK_Violation( E ) then
     begin
          with DataSet do
          begin
               Status := Format( 'El empleado %d ya esta inscrito en el grupo %d', [
                                 FieldByName( 'CB_CODIGO' ).AsInteger,
                                 FieldByName( 'SE_FOLIO' ).AsInteger] );
          end;
     end
     else
         Status := E.Message;
end;

function TdmCliente.FetchEmployee( oDataSet: TZetaClientDataSet; const iEmpleado: TNumEmp ): Boolean;
var
   oDatos: OleVariant;
begin
     Result := Servidor.GetEmpleado( Empresa, iEmpleado, oDatos );
     if Result then
     begin
          with oDataSet do
          begin
               Data := oDatos;
          end;
     end;
end;

procedure TdmCliente.UpdateNodeInfo( oDataSet: TZetaClientDataSet; const sField: String ; Padre: TZetaXMLNode );
{ Si el XML no incluye el nodo no se requiere procesar nada }
var
   oNodo: TZetaXMLNode;
   oCampo: TField;
begin
     if ( Padre =  nil ) then
          oNodo := FXMLTools.GetNode( sField )
     else
         oNodo := FXMLTools.GetNode( sField, Padre );

     if Assigned( oNodo ) then
     begin
          with oDataSet do
          begin
               oCampo := FindField( sField );
               if Assigned( oCampo ) then
               begin
                    if not ( state in [ dsEdit, dsInsert ] ) then
                       Edit;
                    case oCampo.DataType of
                         ftInteger, ftSmallInt, ftWord : oCampo.AsInteger := FXMLTools.TagAsInteger( oNodo, 0 );
                         ftFloat, ftCurrency, ftBCD : oCampo.AsFloat := FXMLTools.TagAsFloat( oNodo, 0 );
                         ftDate, ftTime, ftDateTime : oCampo.AsDateTime := FXMLTools.TagAsDate( oNodo, NullDateTime );
                    else
                         oCampo.AsString := FXMLTools.TagAsString( oNodo, VACIO );
                    end;
               end
               else
               begin
                    DataBaseError( 'No se encontr� el campo: ' + sField );
               end;
          end;
     end;
end;

procedure TdmCliente.LogInit;
begin
     with FErrores do
     begin
          Clear;
     end;
end;

procedure TdmCliente.LogError( const iEmpleado: Integer; const sTexto: String );
begin
     with FErrores do
     begin
          BeginUpdate;
          try
             if ( FEmpleados.Count > 1 ) then
                Add( Format( 'Empleado #%d = %s' + CR_LF, [ iEmpleado, sTexto ] ) )
             else
                 Add( sTexto );
          finally
                 EndUpdate;
          end;
     end;
end;

function TdmCliente.LogAsText: String;
begin
     with FErrores do
     begin
          Result := Text;
     end;
end;

{ ***** Cargado de XML y Determinaci�n del Proceso A Ejecutar }

function TdmCliente.TressGrabar(const sXML: String; const iUsuario: Integer; {$ifdef LEONI} const iFolio: integer; {$endif} var sLog: String): Boolean;
var
   sEmpresa, sTipo: String;
begin
     Result := FALSE;
     sLog := '';
     try
        {$ifdef LEONI}
        FFolio := iFolio;
        {$endif}
        with FXMLTools do
        begin
             XMLAsText := sXML;
             XMLWFN := sXML;
             sEmpresa := TagAsString( GetNode( 'EMPRESA' ), '' );
             sTipo := TagAsString( GetNode( 'MOV3' ), '' );
        end;
        if ZetaCommonTools.StrVacio( sEmpresa ) then
           sLog := 'Empresa No Especificada'
        else
        begin
             Usuario := iUsuario;    // Asigna el usuario activo a dmCliente - No se hace login ni se validan derechos de acceso
             if SetEmpresaActiva( sEmpresa ) then
             begin
                  LogInit;
                  Result := TressInvocar( sTipo, iUsuario );
                  sLog := LogAsText;
             end
             else
                 sLog := Format( 'Empresa %s No Existe', [ sEmpresa ] );
        end;
     except
           on Error: Exception do
           begin
                sLog := 'Error Al Examinar Documento: ' + Error.Message;
           end;
     end;
end;

function TdmCliente.TressInvocar( const sTipo: String; const iUsuario: Integer ): Boolean;
var
   iUso: Integer;
begin
     Status := '';

     iUso := ObtenValUso( sTipo );

     if( iUso < K_MAX_VAL_USO_DEMO )then
     begin
          if ( sTipo = K_T_PERM ) then
             Result := AgregarPermiso( iUsuario )
          else
          if ( sTipo = K_TW_MULTIP ) then
             Result := AgregarCambiosMultiples
          else
          if ( sTipo = K_T_CAMBIO ) then
             Result := AgregarCambioSalario
          else
          if ( sTipo = K_TW_AUTO3 ) then
             Result := AgregarAutorizacionHoras( iUsuario )
          else
          if ( sTipo = K_T_VACA ) then
             Result := AgregarVacacion( iUsuario )
          else
          if ( sTipo = K_TW_INSCRIPCION ) then
             Result := InscribirACurso( iUsuario )
          else
          if ( sTipo = K_TW_TIEMPOX ) then
             Result := AgregaTiempoExtra( iUsuario )
          else
          if ( sTipo = K_TW_PLAZA ) then
             Result := AgregarCambioPlaza
          else
          if ( sTipo = K_TW_CAMBIOPE ) then
             Result := AgregarCambiosPE
          else
          if ( sTipo = K_TW_CAMBIOPE_PRESBAJA ) then
             Result := AgregarCambioJornadaPresbaja
          else
          if ( sTipo = K_TW_MULTIP_PRESBAJA ) then
             Result := AgregarCambiosMultiplesPresbaja
          else
          if ( sTipo = K_TW_AUTO3MULT ) then
             Result := AgregarAutorizacionHorasMultiple(iUsuario)
          else
          if ( sTipo = K_TW_EXCEPNOMINA ) then
             Result := AgregarExcepcionesNomina(iUsuario)
          else
          if ( sTipo = K_TW_MODCOLABORA ) then
             Result := AgregarModificacionColabora(iUsuario)
          else
          if ( sTipo = K_TW_INFONAVIT_AGREGAR ) then //PENSKE Credito Infonavit
             Result := AgregarInfonavit(iUsuario)
          else
          if ( sTipo = K_TW_INFONAVIT_MODIFICAR ) then //PENSKE Credito Infonavit
             Result := ModificarInfonavit(iUsuario)
          else
          if ( sTipo = K_TW_INFONAVIT_SUSPENDER ) then //PENSKE Credito Infonavit
             Result := SuspenderInfonavitWFN(iUsuario)
          else
          if ( sTipo = K_TW_INFONAVIT_REINICIAR ) then //PENSKE Credito Infonavit
             Result := ReiniciarInfonavitWFN(iUsuario)
          else
          if ( sTipo = K_TW_INCAPACIDAD_AGREGAR ) then //PENSKE Agregar incapacidad
             Result := AgregarIncapacidadWFN(iUsuario)
          else
          if ( sTipo = K_TW_PRESTAMO_AGREGAR ) then //Agregar prestamos
             Result := AgregarPrestamoWFN(iUsuario)
          else
          if ( sTipo = K_TW_CAMBIO_EQUIPO ) then //Agregar Cambio Equipo (Cambio de Area de Supervisor en Tress)
             Result := AgregarCambioEquipoWFN(iUsuario)
          else
          if ( sTipo = K_TW_BAJA_EMPLEADO ) then //Baja Empleado
             Result := BajaEmpleadoWFN(iUsuario)
          else
          if ( sTipo = K_TW_PUESTO_CAMBIAR ) then //Cambiar Puesto
             Result := CambiarPuestoWFN
          else
          if ( sTipo = K_TW_TURNO_CAMBIAR ) then //Cambiar Turno
             Result := CambiarTurnoWFN
          else
          if ( sTipo = K_TW_CAMBIO_SALARIO ) then //Cambiar Salario
             Result := AgregarCambioSalarioWFN
          else
          if ( sTipo = K_TW_CAMBIO_AREA ) then //Cambiar Areas
             Result := AgregarCambioAreasWFN
          else
          if ( sTipo = K_TW_CAMBIO_CONTRATO ) then //Cambio de Contrato
             Result := AgregarCambioContratoWFN
          else
          if ( sTipo = K_TW_AGREGAR_PARIENTE ) then //Agregar Pariente
             Result := CRUDParienteEmpleadoWFN(sTipo)
          else
          if ( sTipo = K_TW_MODIFICAR_PARIENTE ) then //Modificar Pariente
             Result := CRUDParienteEmpleadoWFN(sTipo)
          else
          if ( sTipo = K_TW_BORRAR_PARIENTE ) then //Borrar Pariente
             Result := CRUDParienteEmpleadoWFN(sTipo)
          else
          if ( sTipo = K_TW_EMP_SEGURO ) then //Agregar Seguro Medico por Empleado @DLSJ 2015-11-4
             Result := AgregarSeguroEmpWFN
          else
          if ( sTipo = K_TW_EMP_AGREGAR ) then //US 1111: Agregar Alta de Empleado @DLSJ 2016-02-15
             Result := AgregarEmpWFN
          else
          if ( sTipo = K_TW_INTERCAMBIO_FESTIVO ) then //US 1111: Agregar Alta de Empleado @DLSJ 2016-02-15
             Result := IntercambiarFestivoWFN
          else
          if ( sTipo = K_TW_EMP_REINGRESO ) then //US : Reingreso de Empleado @2016-09-28
             Result := ReingresoEmpWFN
          else
          if ( sTipo = K_TW_EMP_CHECADA ) then //US : CHECADA MANUAL ZAFRAN 14 feb 2018
             Result := AgregarChecadaWFN(iUsuario)
          else
          {$ifdef LEONI}
          if ( sTipo = K_TW_EXTRALEONI ) then
        	 Result := AgregarExtrasLeoni(iUsuario)
     	  else
     	 {$endif}
          begin
               Result := False;
               LogError( 0, Format( 'Tipo de Movimiento Indeterminado ( Nodo TIPO ''%s'' )', [ sTipo ] ) );
          end;

          if Result then
          begin
               // En K_AUTORIZADO representa que no est� en modo DEMO. Por lo que no debe actualizar.
               if( iUso > K_AUTORIZADO )then
                   GrabaValUso( sTipo, ( iUso + 1 ) );
          end;
     end
     else
     begin
          Result := False;
          if( iUso = K_NO_AUTORIZADO )then
              LogError( 0, 'M�dulo No Autorizado' )
          else
              LogError( 0, 'Modo DEMO' );
     end;
end;

procedure TdmCliente.CargarEmpleados;
const
     NODO_EMPLEADO = 'EMPLEADO';
     NODO_EMPLEADOS_DATOS = 'INFO';
     NODO_EMPLEADOS_LISTA = 'EMPLEADOS';
var
   Nodo, Info: TZetaXMLNode;
begin
     FEmpleados.Clear;
     with FXMLTools do
     begin
          Nodo := GetNode( NODO_EMPLEADOS_LISTA );
          { Preguntar si son varios empleados }
          if Assigned( Nodo ) then
          begin
               Info := GetNode( NODO_EMPLEADOS_DATOS, Nodo );
               while Assigned( Info ) and HasChildren( Info ) do
               begin
                    FEmpleados.AddEmpleado( TagAsInteger( GetNode( NODO_EMPLEADO, Info ), K_EMPLEADO_NULO ) );
                    Info := GetNextSibling( Info );
               end
          end
          else
          begin
               FEmpleados.AddEmpleado( TagAsInteger( GetNode( NODO_EMPLEADO ), K_EMPLEADO_NULO ) );
          end;
     end;
     if ( FEmpleados.Count = 0 ) then
        raise Exception.Create( 'No se especific� un empleado' );
end;

procedure TdmCliente.CargarAutorizaciones;
const
     NODO_EMPLEADO = 'EMPLEADO';
     NODO_FECHA = 'FECHAAUTO';
     NODO_HORAS = 'HORAS';
     NODO_TIPO = 'TIPOAUTO';
     NODO_MOTIVO = 'MOTIVO';
     NODO_EMPLEADOS_DATOS = 'INFO';
     NODO_EMPLEADOS_LISTA = 'EMPLEADOS';
     NODO_EMPRESA = 'EMPRESA';

var
   Nodo, Info: TZetaXMLNode;
begin
     FAutorizaciones.Clear;
     with FXMLTools do
     begin
          Nodo := GetNode( NODO_EMPLEADOS_LISTA );
          { Preguntar si son varios empleados }
          if Assigned( Nodo ) then
          begin
               Info := GetNode( NODO_EMPLEADOS_DATOS, Nodo );
               while Assigned( Info ) and HasChildren( Info ) do
               begin
                      FAutorizaciones.AddAutorizacion( TagAsInteger( GetNode( NODO_EMPLEADO, Info ), K_EMPLEADO_NULO ),
                                                       TagAsDate( GetNode( NODO_FECHA, Info), NullDateTime ), // OP: 9.Julio.2010
                                                       TagAsFloat(GetNode( NODO_HORAS, Info ),0.0),
                                                       TagAsString( GetNode( NODO_TIPO, Info), VACIO),
                                                       TagAsString( GetNode( NODO_MOTIVO, Info),VACIO),
                                                       TagAsString( GetNode( NODO_EMPRESA, Info),VACIO)
                                                          );
                    Info := GetNextSibling( Info );
               end
          end
          else
          begin

          end;
     end;
     if ( FAutorizaciones.Count = 0 ) then
        raise Exception.Create( 'No se especific� una Autorizaci�n' );
end;

{ ***** Implementaciones de Llamadas al Servidor de Procesos ***** }

{ Permisos en D�as }

function TdmCliente.AgregarPermiso( const iUsuario: Integer ): Boolean;
const
     K_PERMISOS_TAG = 15;
     K_GLOBAL_PERMISOS_DIAS_HABILES = 305; // De aqu� no se tiene acceso a ZGlobalTress de TRESS, se redeclara la constante.
     K_CONSULTA = 'SELECT PERMISOS_DIAS_HABILES = CASE GL_FORMULA WHEN ''S'' THEN ''S'' ELSE ''N'' END FROM GLOBAL WHERE GL_CODIGO = ''%d''  ';
var
   dInicio: TDate;
   i, iDias, iErrores: Integer;
   iEmpleado: TNumEmp;
   lOk: Boolean;
   sGlobalPermisosDiasHabiles : String;
   oConsultaGlobalPermisos: TClientDataSet;
begin
     oConsultaGlobalPermisos := TZetaClientDataSet.Create( self );
     try
        // Obtener Global Permisos D�as H�biles
        oConsultaGlobalPermisos.Data := ServerConsultas.GetQueryGralTodos( Empresa, Format( K_CONSULTA, [ K_GLOBAL_PERMISOS_DIAS_HABILES ] ) );
        if( not oConsultaGlobalPermisos.Eof )then
            sGlobalPermisosDiasHabiles := oConsultaGlobalPermisos.FieldByName( 'PERMISOS_DIAS_HABILES' ).AsString
        else
            sGlobalPermisosDiasHabiles := 'N';
        lOk := False;
        CargarEmpleados;
        with cdsLista do
        begin
             Data := ServerRecursos.GetHisPermiso( Empresa, K_EMPLEADO_NULO );
        end;
        for i := 0 to ( FEmpleados.Count - 1 ) do
        begin
             iEmpleado := FEmpleados.Empleado[ i ];
             try
                with cdsLista do
                begin
                     EmptyDataset;
                     MergeChangeLog;
                     Append;
                     with FXMLTools do
                     begin
                          dInicio := TagAsDate( GetNode( 'INICIO' ), NullDateTime );
                          iDias := TagAsInteger( GetNode( 'DIAS' ), 0 );
                          FieldByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
                          FieldByName( 'PM_FEC_INI' ).AsDateTime := dInicio;
                          // Permisos d�as h�biles
                          if sGlobalPermisosDiasHabiles = 'S' then // K_GLOBAL_PERMISOS_DIAS_HABILES es TRUE ('S')
                          begin
                               FieldByName( 'PM_FEC_FIN' ).AsDateTime := TagAsDate( GetNode( 'RETORNO' ), NullDateTime );
                               FieldByName( 'PM_DIAS' ).AsFloat := ServerAsistencia.GetPermisoDiasHabiles (
                                            Empresa, iEmpleado, dInicio, TagAsDate( GetNode( 'RETORNO' ), NullDateTime ) );
                          end
                          else
                          begin
                               FieldByName( 'PM_DIAS' ).AsInteger := iDias;
                               FieldByName( 'PM_FEC_FIN' ).AsDateTime := dInicio + iDias;
                          end;
                          // -------------------------------------------------------------------------------------------
                          FieldByName( 'PM_CLASIFI' ).AsInteger := TagAsInteger( GetNode( 'CLASE' ), Ord( tpConGoce ) );
                          FieldByName( 'PM_COMENTA' ).AsString := TagAsString( GetNode( 'OBSERVACIONES' ), '' );
                          FieldByName( 'PM_CAPTURA' ).AsDateTime := Trunc( Now );
                          FieldByName( 'US_CODIGO' ).AsInteger := iUsuario;
                          FieldByName( 'PM_TIPO' ).AsString := TagAsString( GetNode( 'TIPO' ), '' );
                          FieldByName( 'PM_NUMERO' ).AsString := TagAsString( GetNode( 'REFERENCIA' ), '' );
                     end;
                     Post;
                     OnReconcileError := ReconcileError;
                     if Reconciliar( ServerRecursos.GrabaHistorial( Empresa, K_PERMISOS_TAG, DeltaNull, iErrores ) ) then
                        lOk := True
                     else
                     begin
                          LogError( iEmpleado, Self.Status );
                     end;
                end;
             except
             on Error: Exception do
                begin
                     LogError( iEmpleado, 'Error Al Agregar Permiso: ' + Error.Message );
                end;
             end;
        end;
     finally
            FreeAndNil( oConsultaGlobalPermisos );
     end;
     Result := lOk;
end;

{ Cambios M�ltiples de Kardex }

function TdmCliente.AgregarCambiosMultiples: Boolean;
var
   i, j, iEmpleado : Integer;
   ErrorCount : Integer;
   FOtrosDatos : Variant;
   lOk: Boolean;
begin
     lOk := False;
     CargarEmpleados;
     for i := 0 to ( FEmpleados.Count - 1 ) do
     begin
          iEmpleado := FEmpleados.Empleado[ i ];
          try
             if FetchEmployee( cdsLista, iEmpleado ) then
             begin
                  with FXMLTools do
                  begin
                       FOtrosDatos := VarArrayOf( [ TagAsDate( GetNode( 'FECHAREG' ), NullDateTime ),
                                                    TagAsString( GetNode( 'DESCRIPCION' ), VACIO ),
                                                    TagAsString( GetNode( 'OBSERVACIONES' ), VACIO ) ] );
                  end;
                  // Revisa si se incluyen los tags de cambios
                  UpdateNodeInfo( cdsLista, 'CB_PUESTO' );
                  UpdateNodeInfo( cdsLista, 'CB_CLASIFI' );
                  UpdateNodeInfo( cdsLista, 'CB_TURNO' );
                  UpdateNodeInfo( cdsLista, 'CB_CONTRAT' );
                  for j := 1 to 9 do
                  begin
                       UpdateNodeInfo( cdsLista, 'CB_NIVEL' + IntToStr( j ) );
                  end;
                  // Enviar los cambios
                  with cdsLista do
                  begin
                       PostData;
                       OnReconcileError := ReconcileError;
                       if VarIsNull( DeltaNull ) then     //MV( 21/Ago/2007) Se cambio esta parte para validar que el DataSet no va Null
                          lOK := True
                       else
                       begin
                            if Reconciliar( ServerRecursos.GrabaCambiosMultiples( Empresa, DeltaNull, FOtrosDatos, ErrorCount ) ) then
                               lOk := True
                            else
                            begin
                                 LogError( iEmpleado, Self.Status );
                            end;
                       end;
                  end;
             end
             else
             begin
                  DataBaseError( 'No se encontr� el empleado # ' + IntToStr( iEmpleado ) );
             end;
          except
                on Error: Exception do
                begin
                     LogError( iEmpleado, 'Error al agregar cambios m�ltiples: ' + Error.Message );
                end;
          end;
     end;
     Result := lOk;
end;

{ Cambio de Plaza de Empleado }
function TdmCliente.AgregarCambioPlaza: Boolean;
var
   i, iEmpleado: Integer;
   dFechaSal: TDate;
   ErrorCount: Integer;
   sPercepcionesFijas: String;
   lOk: Boolean;

begin
     lOk := False;
     CargarEmpleados;
     for i := 0 to ( FEmpleados.Count - 1 ) do
     begin
          iEmpleado := FEmpleados.Empleado[ i ];
          try
             with cdsLista do
             begin
                  Data := ServerRecursos.GetEditHisKardex( Empresa, iEmpleado, 0, VACIO );  // Llenar DataSet Vacio con formato de campos de KARDEX
                  Append;
                  // Datos Iniciales ( L�gica de dmRecursos )
                  FieldByName( 'CB_CODIGO').AsInteger := iEmpleado;
                  with FXMLTools do
                  begin
                       FieldByName( 'CB_FECHA' ).AsDateTime := TagAsDate( GetNode( 'PL_FECHA_INI' ), NullDateTime );
                       FieldByName( 'CB_FECHA_2' ).AsDateTime := FieldByName( 'CB_FECHA' ).AsDateTime
                  end;
                  FieldByName( 'CB_FAC_INT' ).AsFloat:= 0;
                  FieldByName( 'CB_GLOBAL' ).AsString  := K_GLOBAL_NO;
                  FieldByName( 'CB_REINGRE' ).AsString := K_GLOBAL_NO;
                  FieldByName( 'CB_AUTOSAL' ).AsString := K_GLOBAL_NO;
                  FieldByName( 'CB_NIVEL' ).AsInteger  := 5;
                  FieldByName( 'CB_STATUS' ).AsInteger := Ord( skCapturado );
                  FieldByName( 'CB_PLAZA' ).AsInteger := FXMLTools.TagAsInteger( FXMLTools.GetNode( 'PL_FOLIO' ), 0 );
                  FieldByName( 'CB_TIPO' ).AsString := K_T_PLAZA;
                  //InitCamposSalario( iEmpleado, dFechaSal );   // Datos tomados de COLABORA
                  dFechaSal := Now; //Evitar Warning :EZ
                  with FXMLTools do  // Datos del XML
                  begin
                       FieldByName('CB_PUESTO').AsString := TagAsString( GetNode( 'PU_CODIGO' ), VACIO );
                       FieldByName('CB_CLASIFI').AsString := TagAsString( GetNode( 'PL_CLASIFI' ), VACIO );
                       FieldByName('CB_TURNO').AsString := TagAsString( GetNode( 'PL_TURNO' ), VACIO );
                       FieldByName('CB_PATRON').AsString := TagAsString( GetNode( 'PL_PATRON' ), VACIO );
                       FieldByName('CB_NIVEL1').AsString := TagAsString( GetNode( 'PL_NIVEL1' ), VACIO );
                       FieldByName('CB_NIVEL2').AsString := TagAsString( GetNode( 'PL_NIVEL2' ), VACIO );
                       FieldByName('CB_NIVEL3').AsString := TagAsString( GetNode( 'PL_NIVEL3' ), VACIO );
                       FieldByName('CB_NIVEL4').AsString := TagAsString( GetNode( 'PL_NIVEL4' ), VACIO );
                       FieldByName('CB_NIVEL5').AsString := TagAsString( GetNode( 'PL_NIVEL5' ), VACIO );
                       FieldByName('CB_NIVEL6').AsString := TagAsString( GetNode( 'PL_NIVEL6' ), VACIO );
                       FieldByName('CB_NIVEL7').AsString := TagAsString( GetNode( 'PL_NIVEL7' ), VACIO );
                       FieldByName('CB_NIVEL8').AsString := TagAsString( GetNode( 'PL_NIVEL8' ), VACIO );
                       FieldByName('CB_NIVEL9').AsString := TagAsString( GetNode( 'PL_NIVEL9' ), VACIO );
                       FieldByName('CB_NOMINA').AsInteger := TagAsInteger( GetNode( 'PL_NOMINA' ), 1);
                       FieldByName('CB_CONTRAT').AsString := TagAsString( GetNode( 'PL_CONTRAT' ), VACIO );
                       FieldByName( 'CB_COMENTA' ).AsString := TagAsString( GetNode( 'DESCRIPCION' ), VACIO );
                       FieldByName( 'CB_NOTA' ).AsString := TagAsString( GetNode( 'OBSERVACIONES' ), VACIO );
                  end;
                  PostData;
                  OnReconcileError := ReconcileError;
                  sPercepcionesFijas := GetPercepcionesFijas( iEmpleado, dFechaSal );
                  if Reconciliar( ServerRecursos.GrabaHisKardex( Empresa, Delta, sPercepcionesFijas, ErrorCount) ) then
                     lOk := True
                  else
                  begin
                       LogError( iEmpleado, Self.Status );
                  end;
             end;
          except
                on Error: Exception do
                begin
                     LogError( iEmpleado, 'Error al agregar cambio de Plaza: ' + Error.Message );
                end;
          end;
     end;
     Result := lOk;
end;

{ Cambio de Salario Individual }

function TdmCliente.AgregarCambioSalario: Boolean;
var
   i, iEmpleado: Integer;
   ErrorCount: Integer;
   dFechaSal: TDateTime;
   sPercepcionesFijas: String;
   lOk: Boolean;
   rSalario, rPorcentaje: Currency;

begin
     lOk := False;
     CargarEmpleados;
     for i := 0 to ( FEmpleados.Count - 1 ) do
     begin
          iEmpleado := FEmpleados.Empleado[ i ];
          try
             with cdsLista do
             begin
                  Data := ServerRecursos.GetEditHisKardex( Empresa, iEmpleado, 0, VACIO );  // Llenar DataSet Vacio con formato de campos de KARDEX
                  Append;
                  // Datos Iniciales ( L�gica de dmRecursos )
                  FieldByName( 'CB_CODIGO').AsInteger := iEmpleado;
                  with FXMLTools do
                  begin
                       FieldByName( 'CB_FECHA' ).AsDateTime := TagAsDate( GetNode( 'FECHAREG' ), NullDateTime );
                       FieldByName( 'CB_FECHA_2' ).AsDateTime := TagAsDate( GetNode( 'FECHAIMSS' ), NullDateTime );
                       if ( FieldByName( 'CB_FECHA_2' ).AsDateTime = NullDateTime ) then
                          FieldByName( 'CB_FECHA_2' ).AsDateTime := FieldByName( 'CB_FECHA' ).AsDateTime;
                  end;
                  //FieldByName( 'CB_FECHA_2' ).AsDateTime := FieldByName( 'CB_FECHA').AsDateTime;
                  //FieldByName( 'CB_FEC_CON' ).AsDateTime := FieldByName( 'CB_FECHA').AsDateTime; //MV(27/Mar/2008): Se cambio para que se llene con los datos del empleado Funci�n (InitCamposSalario)
                  FieldByName( 'CB_FAC_INT' ).AsFloat:= 0;
                  FieldByName( 'CB_GLOBAL' ).AsString  := K_GLOBAL_NO;
                  FieldByName( 'CB_REINGRE' ).AsString := K_GLOBAL_NO;
                  FieldByName( 'CB_AUTOSAL' ).AsString := K_GLOBAL_NO;
                  FieldByName( 'CB_NIVEL' ).AsInteger  := 5;
                  FieldByName( 'CB_STATUS' ).AsInteger := Ord( skCapturado );
                  FieldByName( 'CB_TIPO' ).AsString := K_T_CAMBIO;
                  InitCamposSalario( iEmpleado, dFechaSal );   // Datos tomados de COLABORA
                  with FXMLTools do  // Datos del XML
                  begin
                       {$ifdef FALSE}
                       FieldByName( 'CB_SALARIO' ).AsFloat := TagAsFloat( GetNode( 'SALARIO_NEW' ), 0 );
                       FieldByName( 'CB_COMENTA' ).AsString := TagAsString( GetNode( 'DESCRIPCION' ), VACIO );
                       FieldByName( 'CB_NOTA' ).AsString := TagAsString( GetNode( 'OBSERVACIONES' ), VACIO );
                       {$else}
                       rsalario := TagAsFloat( GetNode( 'SALARIO_NEW' ), 0 );
                       if PesosIgualesP( rSalario, 0, 0 ) then
                       begin
                            rPorcentaje := ( TagAsInteger( GetNode( 'PORCENTAJE_AUMENTO' ), 0 ) / 100 );
                            if ( rPorcentaje > 0 ) then
                               FieldByName( 'CB_SALARIO' ).AsFloat := FieldByName( 'CB_SALARIO' ).AsFloat * ( 1 + rPorcentaje );
                       end
                       else
                           FieldByName( 'CB_SALARIO' ).AsFloat := rSalario;
                       FieldByName( 'CB_COMENTA' ).AsString := TagAsString( GetNode( 'DESCRIPCION' ), VACIO );
                       FieldByName( 'CB_NOTA' ).AsString := TagAsString( GetNode( 'OBSERVACIONES' ), VACIO );
                       {$endif}
                  end;
                  PostData;
                  OnReconcileError := ReconcileError;
                  sPercepcionesFijas := GetPercepcionesFijas( iEmpleado, dFechaSal );
                  if Reconciliar( ServerRecursos.GrabaHisKardex( Empresa, Delta, sPercepcionesFijas, ErrorCount) ) then
                     lOk := True
                  else
                  begin
                       LogError( iEmpleado, Self.Status );
                  end;
             end;
          except
                on Error: Exception do
                begin
                     LogError( iEmpleado, 'Error al agregar cambio de salario: ' + Error.Message );
                end;
          end;
     end;
     Result := lOk;
end;

procedure TdmCliente.InitCamposSalario( const iEmpleado: Integer; var dFechaSal: TDateTime );
var
   oDataSet : TZetaClientDataSet;
begin
     oDataSet := TZetaClientDataSet.Create( self );
     try
        if FetchEmployee( oDataSet, iEmpleado ) then
        begin
             dFechaSal := oDataSet.FieldByName( 'CB_FEC_SAL' ).AsDateTime;
             with cdsLista do
             begin
                  FieldByName( 'CB_SALARIO' ).AsFloat := oDataSet.FieldByName( 'CB_SALARIO' ).AsFloat;
                  FieldByName( 'CB_FEC_CON' ).AsDateTime := oDataSet.FieldByName( 'CB_FEC_CON' ).AsDateTime;   //MV(27/Mar/2008): Se toma la fecha de contrataci�n del empleado.
                  FieldByName( 'CB_PER_VAR' ).AsFloat := oDataSet.FieldByName( 'CB_PER_VAR' ).AsFloat;
                  FieldByName( 'CB_ZONA_GE' ).AsString := oDataSet.FieldByName( 'CB_ZONA_GE' ).AsString;
                  FieldByName( 'CB_TABLASS' ).AsString := oDataSet.FieldByName( 'CB_TABLASS' ).AsString;
                  FieldByName( 'CB_CLASIFI' ).AsString := oDataSet.FieldByName( 'CB_CLASIFI' ).AsString;
                  FieldByName( 'CB_FEC_ANT').AsDateTime := oDataSet.FieldByName( 'CB_FEC_ANT').AsDateTime;
             end;
        end;
     finally
            FreeAndNil( oDataSet );
     end;
end;

function TdmCliente.GetPercepcionesFijas( const iEmpleado: Integer; const dFecha: TDateTime ): String;
var
   oDataSet : TZetaClientDataSet;
begin
     Result := VACIO;
     oDataSet := TZetaClientDataSet.Create( self );
     try
        with oDataSet do
        begin
             Data := ServerRecursos.GetEditKarFijas( Empresa, iEmpleado, dFecha );
             First;
             while ( not EOF ) do
             begin
                  Result := ConcatString( Result, FieldByName( 'KF_CODIGO' ).AsString, ',' );
                  Next;
             end;
        end;
     finally
            FreeAndNil( oDataSet );
     end;
end;

{ Autorizaciones de Horas Extras y Permisos }

function TdmCliente.AgregarAutorizacionHoras( const iUsuario: Integer ): Boolean;
const
     K_HORA_NULA = '----';
var
   iEmpleado: TNumEmp;
   i, ErrorCount: Integer;
   ErrorData: OleVariant;
   lOk: Boolean;
   eAccion: eOperacionConflicto;
   sAccion: string;

begin
     lOk := False;
     CargarEmpleados;
     with cdsLista do
     begin
          Data := ServerAsistencia.GetAutorizaciones( Empresa, K_EMPLEADO_NULO, TRUE );
     end;
     for i := 0 to ( FEmpleados.Count - 1 ) do
     begin
          iEmpleado := FEmpleados.Empleado[ i ];
          try
             with cdsLista do
             begin
                  EmptyDataset;
                  MergeChangeLog;
                  Append;
                  FieldByName( 'US_CODIGO' ).AsInteger := iUsuario;
                  FieldByName( 'CH_SISTEMA' ).AsString := K_GLOBAL_NO;
                  FieldByName( 'CH_GLOBAL' ).AsString := K_GLOBAL_NO;
                  FieldByName( 'CH_IGNORAR' ).AsString := K_GLOBAL_NO;
                  FieldByName( 'CH_H_REAL' ).AsString := K_HORA_NULA;
                  FieldByName( 'CH_H_AJUS' ).AsString := K_HORA_NULA;
                  FieldByName( 'US_COD_OK' ).AsInteger := 0;
                  with FXMLTools do
                  begin
                       FieldByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
                       FieldByName( 'AU_FECHA' ).AsDateTime := TagAsDate( GetNode( 'FECHAAUTO' ), NullDateTime );
                       FieldByName( 'CH_TIPO' ).AsInteger := TagAsInteger( GetNode( 'TIPOAUTO' ), Ord( acExtras ) ) + K_OFFSET_AUTORIZACION;
                       FieldByName( 'HORAS' ).AsFloat := TagAsFloat( GetNode( 'HORAS' ), 0 );
                       FieldByName( 'CH_RELOJ' ).AsString := TagAsString( GetNode( 'MOTIVO' ), VACIO );
                       sAccion := TagAsString( GetNode( 'ACCION_SI_EXISTE' ), 'DEJAR_ANTERIOR' );
                  end;
                  Post;
                  OnReconcileError := ReconcileErrorAuto;
                  { ocReportar, ocIgnorar, ocSumar, ocSustituir }
                  if ( sAccion = 'SUSTITUIR' ) then
                     eAccion := ocSustituir
                  else if ( sAccion = 'SUMAR' ) then
                          eAccion := ocSumar
                       else if ( sAccion = 'DEJAR_ANTERIOR' ) then
                               eAccion := ocIgnorar
                            else
                                eAccion := ocIgnorar;
                  if Reconciliar( ServerAsistencia.GrabaAutorizaciones( Empresa, Delta, ErrorCount, ErrorData, Ord( eAccion ) ) ) then
                     lOk := True
                  else
                  begin
                       LogError( iEmpleado, Self.Status );
                  end;
             end;
          except
                on Error: Exception do
                begin
                     LogError( iEmpleado, 'Error al agregar autorizaciones de horas: ' + Error.Message );
                end;
          end;
     end;
     Result := lOk;
end;

function TdmCliente.AgregarAutorizacionHorasMultiple( const iUsuario: Integer ): Boolean;
const
     K_HORA_NULA = '----';
var
   i, ErrorCount,iSolicitante: Integer;
   ErrorData: OleVariant;
   lOk: Boolean;
   sEmpresa:string;
   eAccion: eOperacionConflicto;
   sAccion: string;

begin
     lOk := False;
     CargarAutorizaciones;

     for i := 0 to ( FAutorizaciones.Count - 1 ) do
     begin
          try
             iSolicitante := FXMLTools.TagAsInteger( FXMLTools.GetNode( 'US_CODIGO' ), 0 );
             sEmpresa := FAutorizaciones.Empresa[i];
             if SetEmpresaActiva( sEmpresa ) then
             begin
                  LogInit;
                  with cdsLista do
                  begin
                       Data := ServerAsistencia.GetAutorizaciones( Empresa, K_EMPLEADO_NULO, TRUE );
                  end;
                  with cdsLista do
                  begin
                      EmptyDataset;
                      MergeChangeLog;
                      Append;
                      FieldByName( 'US_CODIGO' ).AsInteger := iSolicitante;
                      FieldByName( 'CH_SISTEMA' ).AsString := K_GLOBAL_NO;
                      FieldByName( 'CH_GLOBAL' ).AsString := K_GLOBAL_NO;
                      FieldByName( 'CH_IGNORAR' ).AsString := K_GLOBAL_NO;
                      FieldByName( 'CH_H_REAL' ).AsString := K_HORA_NULA;
                      FieldByName( 'CH_H_AJUS' ).AsString := K_HORA_NULA;
                      FieldByName( 'US_COD_OK' ).AsInteger := 0;
                      with FXMLTools do
                      begin
                           FieldByName( 'CB_CODIGO' ).AsInteger := FAutorizaciones.Empleado[i];
                           FieldByName( 'AU_FECHA' ).AsDateTime := FAutorizaciones.Fecha[i];
                           FieldByName( 'CH_TIPO' ).AsInteger := StrToInt(FAutorizaciones.Tipo[i]) + K_OFFSET_AUTORIZACION;
                           FieldByName( 'HORAS' ).AsFloat := FAutorizaciones.Hora[i];
                           FieldByName( 'CH_RELOJ' ).AsString := FAutorizaciones.Motivo[i];
                           sAccion := TagAsString( GetNode( 'ACCION_SI_EXISTE' ), 'DEJAR_ANTERIOR' );
                      end;
                      Post;
                      OnReconcileError := ReconcileErrorAuto;
                      { ocReportar, ocIgnorar, ocSumar, ocSustituir }
                      if ( sAccion = 'SUSTITUIR' ) then
                         eAccion := ocSustituir
                      else if ( sAccion = 'SUMAR' ) then
                              eAccion := ocSumar
                           else if ( sAccion = 'DEJAR_ANTERIOR' ) then
                                   eAccion := ocIgnorar
                                else
                                    eAccion := ocIgnorar;
                      if Reconciliar( ServerAsistencia.GrabaAutorizaciones( Empresa, Delta, ErrorCount, ErrorData, Ord( eAccion ) ) ) then
                         lOk := True
                      else
                      begin
                           LogError( FAutorizaciones.Empleado[i], Self.Status );
                      end;
                  end;
             end
             else
                 LogError(FAutorizaciones.Empleado[i], Format( 'Empresa %s No Existe', [ sEmpresa ] ));
          except
                on Error: Exception do
                begin
                     LogError( FAutorizaciones.Empleado[i], 'Error al agregar autorizaciones de horas: ' + Error.Message );
                end;
          end;
     end;
     Result := lOk;
end;

{$ifdef LEONI}
function TdmCliente.AgregarExtrasLeoni( const iUsuario: Integer ): Boolean;
const
     K_HORA_NULA = '----';
     Q_GRABA_AUTORIZA = 'insert into WAUTTE( WP_FOLIO, AU_FECHA, SOLICITA, CM_CODIGO, CB_CODIGO, CH_TIPO, HORAS, CH_RELOJ ) '+
                                   'values ( :WP_FOLIO, :AU_FECHA, :SOLICITA, :CM_CODIGO, :CB_CODIGO, :CH_TIPO, :HORAS, :CH_RELOJ )';

var
   i, ErrorCount,iSolicitante: Integer;
   ErrorData: OleVariant;
   lOk: Boolean;
   sEmpresa:string;
   eAccion: eOperacionConflicto;
   sAccion: string;
   oGrabaInfo: TZetaCursor;

   procedure GrabaInformacion;
   begin
        with Provider do
        begin
             ParamAsInteger( oGrabaInfo, 'WP_FOLIO', FFolio );
             ParamAsDate( oGrabaInfo, 'AU_FECHA', cdsLista.FieldByName( 'AU_FECHA' ).AsDateTime );
             ParamAsInteger( oGrabaInfo, 'SOLICITA', cdsLista.FieldByName( 'US_CODIGO' ).AsInteger );
             ParamAsString( oGrabaInfo, 'CM_CODIGO', sEmpresa );
             ParamAsInteger( oGrabaInfo, 'CB_CODIGO', cdsLista.FieldByName( 'CB_CODIGO' ).AsInteger );
             ParamAsInteger( oGrabaInfo, 'CH_TIPO', cdsLista.FieldByName( 'CH_TIPO' ).AsInteger );
             ParamAsFLoat( oGrabaInfo, 'HORAS', cdsLista.FieldByName( 'HORAS' ).AsFloat );
             ParamAsString( oGrabaInfo, 'CH_RELOJ', cdsLista.FieldByName( 'CH_RELOJ' ).AsString  );
             EmpiezaTransaccion;
             try
                Ejecuta( oGrabaInfo );
                TerminaTransaccion( TRUE );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                   end;
             end;
        end;
   end;

begin
     lOk := False;
     CargarAutorizaciones;
     for i := 0 to ( FAutorizaciones.Count - 1 ) do
     begin
          try
             iSolicitante := FXMLTools.TagAsInteger( FXMLTools.GetNode( 'US_CODIGO' ), 0 );
             sEmpresa := FAutorizaciones.Empresa[i];
             if SetEmpresaActiva( sEmpresa ) then
             begin
                  Provider.EmpresaActiva := Empresa;
                  oGrabaInfo := Provider.CreateQuery( Q_GRABA_AUTORIZA );
                  try
                     LogInit;
                     with cdsLista do
                     begin
                          Data := ServerAsistencia.GetAutorizaciones( Empresa, K_EMPLEADO_NULO, TRUE );
                     end;
                     with cdsLista do
                     begin
                         EmptyDataset;
                         MergeChangeLog;
                         Append;
                         FieldByName( 'US_CODIGO' ).AsInteger := iSolicitante;
                         FieldByName( 'CH_SISTEMA' ).AsString := K_GLOBAL_NO;
                         FieldByName( 'CH_GLOBAL' ).AsString := K_GLOBAL_NO;
                         FieldByName( 'CH_IGNORAR' ).AsString := K_GLOBAL_NO;
                         FieldByName( 'CH_H_REAL' ).AsString := K_HORA_NULA;
                         FieldByName( 'CH_H_AJUS' ).AsString := K_HORA_NULA;
                         FieldByName( 'US_COD_OK' ).AsInteger := 0;
                         with FXMLTools do
                         begin
                              FieldByName( 'CB_CODIGO' ).AsInteger := FAutorizaciones.Empleado[i];
                              FieldByName( 'AU_FECHA' ).AsDateTime := FAutorizaciones.Fecha[i];
                              FieldByName( 'CH_TIPO' ).AsInteger := 7; // StrToInt(FAutorizaciones.Tipo[i]) + K_OFFSET_AUTORIZACION;
                              FieldByName( 'HORAS' ).AsFloat := FAutorizaciones.Hora[i];
                              FieldByName( 'CH_RELOJ' ).AsString := FAutorizaciones.Motivo[i];
                              sAccion := TagAsString( GetNode( 'ACCION_SI_EXISTE' ), 'DEJAR_ANTERIOR' );
                         end;
                         Post;
                         OnReconcileError := ReconcileErrorAuto;
                         { ocReportar, ocIgnorar, ocSumar, ocSustituir }
                         if ( sAccion = 'SUSTITUIR' ) then
                            eAccion := ocSustituir
                         else if ( sAccion = 'SUMAR' ) then
                                 eAccion := ocSumar
                              else if ( sAccion = 'DEJAR_ANTERIOR' ) then
                                      eAccion := ocIgnorar
                                   else
                                       eAccion := ocIgnorar;
                         if Reconciliar( ServerAsistencia.GrabaAutorizaciones( Empresa, Delta, ErrorCount, ErrorData, Ord( eAccion ) ) ) then
                         begin
                            lOk := True;
                            GrabaInformacion;
                         end
                         else
                         begin
                              LogError( FAutorizaciones.Empleado[i], Self.Status );
                         end;
                     end;
                  finally
                         if Assigned( oGrabaInfo ) then
                            FreeAndNil( oGrabaInfo );
                  end;
             end
             else
                 LogError(FAutorizaciones.Empleado[i], Format( 'Empresa %s No Existe', [ sEmpresa ] ));
          except
                on Error: Exception do
                begin
                     LogError( FAutorizaciones.Empleado[i], 'Error al agregar autorizaciones de horas: ' + Error.Message );
                end;
          end;
     end;
     Result := lOk;
end;
{$endif}

{ Vacaciones Individuales }

function TdmCliente.AgregarVacacion( const iUsuario: Integer ): Boolean;
var
   i, iEmpleado: Integer;
   oDatosEmpleado: OleVariant;
   FSaldosVaca: OleVariant;
   oConsultaPrima: TClientDataSet;
   ErrorCount: Integer;
   lOk: Boolean;

   lUsarGozo : Boolean;

   {$ifdef INTERIORES_AEREOS}
   rGozoTotal, rPagoTotal : TPesos;
   GetTotalFecIni: TDate;
   GetTotalFecFin: TDate;
   {$endif}

   oGenerarMultiplesNode : TZetaXMLNode;
   dSaldoVacacionMensaje: TPesos;//


const
     K_GLOBAL_PAGO_PRIMA_VACA_ANIV = 201; // De aqu� no se tiene acceso a ZGlobalTress de TRESS (es uno custom), por lo que se redeclara la constante.
     K_CONSULTA = 'select PORC_PRIMA = ( DBO.GETVACAPROPORCIONAL(%d, GETDATE()) * (select CALC_PRIMA = coalesce ((select CALC_PRIMA = case GL_FORMULA when ''S'' then 0 else 1 end from GLOBAL where GL_CODIGO=''%d''), 1)))';
     K_USAR_VA_GOZO_NO = 0;
     K_USAR_VA_GOZO_SI = 1;

     function DebeGenerarMultipleEmpleado : boolean;
     begin
         oGenerarMultiplesNode := FXMLTools.GetNode('GENERAR_MULTIPLE');
         Result := Assigned( oGenerarMultiplesNode);
     end;

     function SuficienteSaldoVacaciones( AGozarPagar : Tpesos )   : boolean;
     var
        dSaldoFecha: TPesos; //CB_TOT_GOZ
     begin
          Result := False;
          {$ifdef INTERIORES_AEREOS}
                  dSaldoFecha := FSaldosVaca[ K_VACA_PAGO_ACTUAL ]; //CB_TOT_PAG
          {$else}
                  dSaldoFecha := FSaldosVaca[ K_VACA_GOZO_ACTUAL ]; //CB_TOT_GOZ
          {$endif}
          with cdsLista do
          begin
               if ( dSaldoFecha >= AGozarPagar  ) then
                  Result := True;
          end;
     end;

     function GenerarMultiplesEmpleado : integer;
     var
        oVacacionGenerarNode : TZetaXMLNode;
     begin
          with cdsLista do
          begin
               Data := ServerRecursos.GetHisVacacion( Empresa, iEmpleado, Date, oDatosEmpleado );  // Revisa Historial al d�a de hoy - No hace diferencia con el registro nuevo
          end;

          Result := 0;
          if Assigned( oGenerarMultiplesNode ) then
          begin
               oVacacionGenerarNode := FXMLTools.GetNode( 'VACACION', oGenerarMultiplesNode );

               while Assigned( oVacacionGenerarNode ) and FXMLTools.HasChildren( oVacacionGenerarNode ) do
               begin
                     with cdsLista do
                     begin
                          Append;
                          FieldByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
                          FieldByName( 'VA_TIPO' ).AsInteger    := Ord( tvVacaciones );
                          FieldByName( 'VA_GLOBAL' ).AsString := K_GLOBAL_NO;
                          FieldByName( 'VA_CAPTURA' ).AsDateTime:= Date;
                          FieldByName( 'US_CODIGO' ).AsInteger  := iUsuario;
                     end;
                     UpdateNodeInfo( cdsLista, 'VA_COMENTA');
                     UpdateNodeInfo( cdsLista, 'VA_PERIODO' );

                     UpdateNodeInfo( cdsLista, 'VA_FEC_INI' , oVacacionGenerarNode);
                     UpdateNodeInfo( cdsLista, 'VA_FEC_FIN' , oVacacionGenerarNode);
                     UpdateNodeInfo( cdsLista, 'VA_GOZO' , oVacacionGenerarNode);
                     UpdateNodeInfo( cdsLista, 'VA_PAGO' , oVacacionGenerarNode);

                     UpdateNodeInfo( cdsLista, 'VA_NOMYEAR' , oVacacionGenerarNode);
                     UpdateNodeInfo( cdsLista, 'VA_NOMTIPO' , oVacacionGenerarNode);
                     UpdateNodeInfo( cdsLista, 'VA_NOMNUME' , oVacacionGenerarNode);


                     lUsarGozo := FALSE;
                     with FXMLTools do
                     begin
                          lUsarGozo := TagAsInteger(GetNode( 'USAR_VA_GOZO' ), K_USAR_VA_GOZO_NO ) = K_USAR_VA_GOZO_SI;
                     end;

                     // Calcular montos de Vacacion
                     with cdsLista do
                     begin
                          {$ifndef PRESBAJA_WFN}
                          if not lUsarGozo then
                          begin
                             FieldByName( 'VA_GOZO' ).AsFloat := ServerAsistencia.GetVacaDiasGozar( Empresa, iEmpleado, FieldByName( 'VA_FEC_INI' ).AsDateTime , FieldByName( 'VA_FEC_FIN' ).AsDateTime  );
                             FieldByName( 'VA_PAGO' ).AsFloat := FieldByName( 'VA_GOZO' ).AsFloat;
                          end;
                          {$endif}
                          FSaldosVaca := ServerRecursos.GetSaldosVacacion( Empresa, iEmpleado, FieldByName( 'VA_FEC_INI' ).AsDateTime );
                          if not SuficienteSaldoVacaciones ( rMax ( FieldByName( 'VA_GOZO' ).AsFloat , FieldByName( 'VA_PAGO' ).AsFloat)  )  then
                          begin
                          {$ifdef INTERIORES_AEREOS}
                                  dSaldoVacacionMensaje := FSaldosVaca[ K_VACA_PAGO_ACTUAL ];
                          {$else}
                                 dSaldoVacacionMensaje := FSaldosVaca[ K_VACA_GOZO_ACTUAL ];
                          {$endif}
                               raise Exception.CreateFmt('No tiene suficiente saldo de vacaciones : %f', [ dSaldoVacacionMensaje ]);
                               break;
                          end;
                          FieldByName( 'CB_TABLASS' ).AsString := FSaldosVaca[ K_VACA_TABLA_PREST ];
                          FieldByName( 'CB_SALARIO' ).AsFloat := FSaldosVaca[ K_VACA_SAL_DIARIO ];
                          FieldByName( 'VA_TASA_PR' ).AsFloat := FSaldosVaca[ K_VACA_PRIMA_VACA ];
                     end;

                     // Calcular los dias de prima
                     with cdsLista do
                     begin
                          oConsultaPrima.Data := ServerConsultas.GetQueryGralTodos( Empresa, Format( K_CONSULTA, [ iEmpleado, K_GLOBAL_PAGO_PRIMA_VACA_ANIV ] ) );
                          if( not oConsultaPrima.Eof )then
                              FieldByName( 'VA_P_PRIMA' ).AsFloat := oConsultaPrima.FieldByName( 'PORC_PRIMA' ).AsFloat * FieldByName( 'VA_GOZO' ).AsFloat
                          else
                              FieldByName( 'VA_P_PRIMA' ).AsFloat := 0
                     end;
                     FToolsRH.SetMontosVaca( FSaldosVaca, cdsLista );

                     inc(Result);
                     oVacacionGenerarNode := FXMLTools.GetNextSibling( oVacacionGenerarNode );
               end;
         end;

     end;


begin
     lOk := False;
     CargarEmpleados;
     oConsultaPrima := TZetaClientDataSet.Create( self );
     try
        for i := 0 to ( FEmpleados.Count - 1 ) do
        begin
             iEmpleado := FEmpleados.Empleado[ i ];
             try

                if DebeGenerarMultipleEmpleado then
                begin

{$ifdef INTERIORES_AEREOS}

        lUsarGozo := FALSE;
        with FXMLTools do
        begin
                 lUsarGozo := TagAsInteger(GetNode( 'USAR_VA_GOZO' ), K_USAR_VA_GOZO_NO ) = K_USAR_VA_GOZO_SI;
                 rGozoTotal := TagAsFloat(GetNode( 'VA_GOZO' ), 0.0) ;
                 rPagoTotal := TagAsFloat(GetNode( 'VA_PAGO' ), 0.0) ;
                 GetTotalFecIni := TagAsDate( GetNode( 'VA_FEC_INI'), NullDateTime ) ;
                 GetTotalFecFin := TagAsDate( GetNode( 'VA_FEC_FIN'), NullDateTime ) ;
        end;

        if not lUsarGozo then
        begin
             rGozoTotal := ServerAsistencia.GetVacaDiasGozar( Empresa, iEmpleado, GetTotalFecIni ,  GetTotalFecFin );
             rPagoTotal := rGozoTotal;
        end;

        FSaldosVaca := ServerRecursos.GetSaldosVacacion( Empresa, iEmpleado, GetTotalFecIni );
        if not SuficienteSaldoVacaciones ( rMax ( rGozoTotal , rPagoTotal )  ) then
        begin
             dSaldoVacacionMensaje := FSaldosVaca[ K_VACA_PAGO_ACTUAL ];
             raise Exception.CreateFmt('No tiene suficiente saldo de vacaciones : %f', [ dSaldoVacacionMensaje ]);
             break;
        end;
{$endif}

                      GenerarMultiplesEmpleado;
                end
                else
                begin
                      with cdsLista do
                      begin
                           Data := ServerRecursos.GetHisVacacion( Empresa, iEmpleado, Date, oDatosEmpleado );  // Revisa Historial al d�a de hoy - No hace diferencia con el registro nuevo
                           Append;
                           FieldByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
                           FieldByName( 'VA_TIPO' ).AsInteger    := Ord( tvVacaciones );
                           FieldByName( 'VA_GLOBAL' ).AsString := K_GLOBAL_NO;
                           FieldByName( 'VA_CAPTURA' ).AsDateTime:= Date;
                           FieldByName( 'US_CODIGO' ).AsInteger  := iUsuario;
                      end;
                      UpdateNodeInfo( cdsLista, 'VA_FEC_INI' );
                      UpdateNodeInfo( cdsLista, 'VA_FEC_FIN' );
                      UpdateNodeInfo( cdsLista, 'VA_GOZO' );
                      UpdateNodeInfo( cdsLista, 'VA_PAGO' );
                      //UpdateNodeInfo( cdsLista, 'VA_P_PRIMA' ); { No se ocupa actualizar valor de prima, con lo del XML. }
                      UpdateNodeInfo( cdsLista, 'VA_PERIODO' );
                      UpdateNodeInfo( cdsLista, 'VA_COMENTA' );
                      UpdateNodeInfo( cdsLista, 'VA_NOMYEAR' );
                      UpdateNodeInfo( cdsLista, 'VA_NOMTIPO' );
                      UpdateNodeInfo( cdsLista, 'VA_NOMNUME' );

                     lUsarGozo := FALSE;
                     with FXMLTools do
                     begin
                          lUsarGozo := TagAsInteger(GetNode( 'USAR_VA_GOZO' ), K_USAR_VA_GOZO_NO ) = K_USAR_VA_GOZO_SI;
                     end;

                      // Calcular montos de Vacacion
                      with cdsLista do
                      begin
                           {$ifndef PRESBAJA_WFN}
                           if not lUsarGozo then
                           begin
                                FieldByName( 'VA_GOZO' ).AsFloat := ServerAsistencia.GetVacaDiasGozar( Empresa, iEmpleado, FieldByName( 'VA_FEC_INI' ).AsDateTime , FieldByName( 'VA_FEC_FIN' ).AsDateTime  );
                                FieldByName( 'VA_PAGO' ).AsFloat := FieldByName( 'VA_GOZO' ).AsFloat;
                           end;
                           {$endif}
                           FSaldosVaca := ServerRecursos.GetSaldosVacacion( Empresa, iEmpleado, FieldByName( 'VA_FEC_INI' ).AsDateTime );
                           if not SuficienteSaldoVacaciones ( rMax ( FieldByName( 'VA_GOZO' ).AsFloat , FieldByName( 'VA_PAGO' ).AsFloat)  ) then
                           begin
                          {$ifdef INTERIORES_AEREOS}
                                 dSaldoVacacionMensaje := FSaldosVaca[ K_VACA_PAGO_ACTUAL ];
                          {$else}
                                 dSaldoVacacionMensaje := FSaldosVaca[ K_VACA_GOZO_ACTUAL ];
                          {$endif}
                                raise Exception.CreateFmt('No tiene suficiente saldo de vacaciones : %f', [ dSaldoVacacionMensaje ]);
                                break;
                           end;
                           FieldByName( 'CB_TABLASS' ).AsString := FSaldosVaca[ K_VACA_TABLA_PREST ];
                           FieldByName( 'CB_SALARIO' ).AsFloat := FSaldosVaca[ K_VACA_SAL_DIARIO ];
                           FieldByName( 'VA_TASA_PR' ).AsFloat := FSaldosVaca[ K_VACA_PRIMA_VACA ];
                      end;

                      // Calcular los dias de prima
                      with cdsLista do
                      begin
                           oConsultaPrima.Data := ServerConsultas.GetQueryGralTodos( Empresa, Format( K_CONSULTA, [ iEmpleado, K_GLOBAL_PAGO_PRIMA_VACA_ANIV ] ) );
                           if( not oConsultaPrima.Eof )then
                               FieldByName( 'VA_P_PRIMA' ).AsFloat := oConsultaPrima.FieldByName( 'PORC_PRIMA' ).AsFloat * FieldByName( 'VA_GOZO' ).AsFloat
                           else
                               FieldByName( 'VA_P_PRIMA' ).AsFloat := 0
                      end;

                      FToolsRH.SetMontosVaca( FSaldosVaca, cdsLista );

                end;

                with cdsLista do
                begin
                     Post;
                     OnReconcileError := ReconcileError;
                     if Reconciliar( ServerRecursos.GrabaHisVacacion( Empresa, Delta, FALSE, ErrorCount ) ) then
                        lOk := True
                     else
                     begin
                          LogError( iEmpleado, Self.Status );
                     end;
                end;
             except
                   on Error: Exception do
                   begin
                        LogError( iEmpleado, 'Error Al Agregar Vacacion: ' + Error.Message );
                   end;
             end;

        end;
     finally
            FreeAndNil( oConsultaPrima );
     end;
     Result := lOk;
end;

            {Inscribe un empleado a una sesi�n}

function TdmCliente.InscribirACurso(const iUsuario: Integer) : Boolean;
const
     K_PERMISOS_TAG = 15;
var
   i, iErrores: Integer;
   iEmpleado: TNumEmp;
   lOk: Boolean;
begin
     lOk := False;
     CargarEmpleados;
     with cdsLista do
     begin
          Data := ServerRecursos.GetInscritos(Empresa, K_EMPLEADO_NULO );
     end;
     for i := 0 to ( FEmpleados.Count - 1 ) do
     begin
          iEmpleado := FEmpleados.Empleado[ i ];
          try
             with cdsLista do
             begin
                  EmptyDataset;
                  MergeChangeLog;
                  Append;
                  with FXMLTools do
                  begin
                       FieldByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
                       FieldByName( 'SE_FOLIO' ).AsInteger := TagAsInteger( GetNode( 'CURSO' ), 0 );
                       FieldByName( 'IC_STATUS' ).AsInteger := Ord( siActivo );
                       FieldByName( 'IC_FEC_INS' ).AsDateTime := Date;
                       FieldByName( 'IC_HOR_INS' ).AsString := HoraAsStr( Time );
                  end;
                  Post;
                  OnReconcileError := ReconcileErrorInscrito;
                  if Reconciliar( ServerRecursos.GrabaInscritos( Empresa, Delta, iErrores ) ) then
                     lOk := True
                  else
                  begin
                       LogError( iEmpleado, Self.Status );
                  end;
             end;
          except
                on Error: Exception do
                begin
                     LogError( iEmpleado, 'Error Al Inscribir Usuario: ' + Error.Message );
                end;
          end;
     end;
     Result := lOk;
end;

{Aprueba tiempo extra autorizado por supervisores.}

function TdmCliente.AgregaTiempoExtra( iUsuario:integer) : Boolean;
const
     K_HORA_NULA = '----';
var
   iEmpleado: TNumEmp;
   i, ErrorCount, iUsuarioOK: Integer;
   ErrorData: OleVariant;
   lOk: Boolean;
   dFecha: TDate;
   TLstHrOk: TStringList;
   rHoras: Currency;
   Pos : TBookMark;
   {
   eAccion: eOperacionConflicto;
   sAccion: string;
   }

   procedure CargarHorasAprobadas;
   const
        NODO_HORASEXTRASOK = 'HRAPROBADA';
        NODO_EMPLEADOS_DATOS = 'INFO';
        NODO_EMPLEADOS_LISTA = 'EMPLEADOS';
   var
      Nodo, Info: TZetaXMLNode;
   begin
        TLstHrOk.Clear;
        with FXMLTools do
        begin
             Nodo := GetNode( NODO_EMPLEADOS_LISTA );
             { Preguntar si son varios empleados }
             if Assigned( Nodo ) then
             begin
                  Info := GetNode( NODO_EMPLEADOS_DATOS, Nodo );
                  while Assigned( Info ) and HasChildren( Info ) do
                  begin
                       TLstHrOk.Add( FloattoStr(TagAsFloat( GetNode( NODO_HORASEXTRASOK, Info ), 0 ) ) );
                       Info := GetNextSibling( Info );
                  end
             end;
        end;
   end;

   procedure VerificaDatos;
   begin
        with cdsLista do
        begin
             if (FieldByName( 'US_COD_OK' ).AsInteger = 0) and
                (FieldByName( 'CH_HOR_DES' ).AsFloat = 0 ) then
             begin
                  Edit;
                  FieldByName( 'US_COD_OK' ).AsInteger := iUsuarioOK;
                  FieldByName( 'CH_HOR_DES' ).AsFloat := rHoras;
                  OnReconcileError := ReconcileErrorAuto;
                  { ocReportar, ocIgnorar, ocSumar, ocSustituir }
                  {
                  if ( sAccion = 'SUSTITUIR' ) then
                     eAccion := ocSustituir
                  else if ( sAccion = 'SUMAR' ) then
                          eAccion := ocSumar
                       else if ( sAccion = 'DEJAR_ANTERIOR' ) then
                               eAccion := ocIgnorar
                            else
                                eAccion := ocIgnorar;
                  }
                  if Reconciliar( ServerAsistencia.GrabaAutorizaciones( Empresa, Delta, ErrorCount, ErrorData, Ord( ocReportar ) ) ) then
                     lOk := True
                  else
                  begin
                       LogError( iEmpleado, Self.Status );
                  end;
             end
             else
                 LogError( iEmpleado, 'La Autorizaci�n ya fue aprobada previamente' );
        end;
   end;

begin
     lOk := False;
     CargarEmpleados;
     TLstHrOk := TStringList.Create;
     try
        CargarHorasAprobadas;
        for i := 0 to ( FEmpleados.Count - 1 ) do
        begin
             iEmpleado := FEmpleados.Empleado[ i ];
             rHoras := StrToFloat( TLstHrOk.Strings[i] );
             try
                with cdsLista, FXMLTools do
                begin
                     dFecha:= TagAsDate( GetNode( 'FECHAAUTO' ), NullDateTime );
                     iUsuarioOK := TagAsInteger( GetNode('USUARIOAPRUEBA'), iUsuario);
                     //sAccion := TagAsString( GetNode( 'ACCION_SI_EXISTE' ), 'DEJAR_ANTERIOR' );

                     Data := ServerAsistencia.GetAutorizaciones( Empresa, dFecha, FALSE );
                     { Busca Registros de autorizaci�n de Horas Extras }
                     if Locate( 'CB_CODIGO;CH_TIPO;US_COD_OK', VarArrayOf( [ iEmpleado, Ord( chExtras ), 0 ] ), [] ) then
                     begin
                          VerificaDatos;
                     end
                     else if Locate( 'CB_CODIGO;CH_TIPO;US_COD_OK',VarArrayOf( [ iEmpleado,Ord( chDescanso ), 0 ] ), [] ) then
                          begin
                               Pos := GetBookMark;
                               if Locate( 'CB_CODIGO;CH_TIPO', VarArrayOf( [ iEmpleado, Ord( chExtras )] ), [] ) then
                               begin
                                    rHoras := rHoras + FieldByName('CH_HOR_DES').AsFloat;
                               end;
                               GotoBookMark(Pos);
                               FreeBookMark(Pos);
                               VerificaDatos;
                          end;
                end;
             except
                   on Error: Exception do
                   begin
                        LogError( iEmpleado, 'Error al aprobar horas extras: ' + Error.Message );
                   end;
             end;
        end;
     finally
            FreeAndNil(TLstHrOk);
     end;
     Result := lOk;
end;


{$ifdef FALSE}
function TdmCliente.AgregarCambiosPE: Boolean;
var
   i, iEmpleado: Integer;
   ErrorCount: Integer;
   dFechaSal: TDateTime;
   sPercepcionesFijas: String;
   lOk: Boolean;
   rSalario, rPorcentaje: Currency;

begin
     lOk := False;
     CargarEmpleados;
     for i := 0 to ( FEmpleados.Count - 1 ) do
     begin
          iEmpleado := FEmpleados.Empleado[ i ];
          try
             with cdsLista do
             begin
                  Data := ServerRecursos.GetEditHisKardex( Empresa, iEmpleado, 0, VACIO );  // Llenar DataSet Vacio con formato de campos de KARDEX
                  Append;
                  // Datos Iniciales ( L�gica de dmRecursos )
                  FieldByName( 'CB_CODIGO').AsInteger := iEmpleado;
                  with FXMLTools do
                  begin
                       FieldByName( 'CB_FECHA' ).AsDateTime := TagAsDate( GetNode( 'FECHAREG' ), NullDateTime );
                       FieldByName( 'CB_FECHA_2' ).AsDateTime := TagAsDate( GetNode( 'FECHAIMSS' ), NullDateTime );
                       if ( FieldByName( 'CB_FECHA_2' ).AsDateTime = NullDateTime ) then
                          FieldByName( 'CB_FECHA_2' ).AsDateTime := FieldByName( 'CB_FECHA' ).AsDateTime;
                  end;
                  FieldByName( 'CB_FAC_INT' ).AsFloat:= 0;
                  FieldByName( 'CB_GLOBAL' ).AsString  := K_GLOBAL_NO;
                  FieldByName( 'CB_REINGRE' ).AsString := K_GLOBAL_NO;
                  FieldByName( 'CB_AUTOSAL' ).AsString := K_GLOBAL_NO;
                  FieldByName( 'CB_NIVEL' ).AsInteger  := 5;
                  FieldByName( 'CB_STATUS' ).AsInteger := Ord( skCapturado );
                  FieldByName( 'CB_TIPO' ).AsString := K_T_CAMBIO;
                  InitCamposSalario( iEmpleado, dFechaSal );   // Datos tomados de COLABORA
                  with FXMLTools do  // Datos del XML
                  begin
                       rsalario := TagAsFloat( GetNode( 'SALARIO_NEW' ), 0 );
                       FieldByName( 'CB_SALARIO' ).AsFloat := rSalario;
                       FieldByName( 'CB_COMENTA' ).AsString := TagAsString( GetNode( 'DESCRIPCION' ), VACIO );
                       FieldByName( 'CB_NOTA' ).AsString := TagAsString( GetNode( 'OBSERVACIONES' ), VACIO );
                  end;
                  PostData;
                  OnReconcileError := ReconcileError;
                  sPercepcionesFijas := GetPercepcionesFijas( iEmpleado, dFechaSal );
                  if Reconciliar( ServerRecursos.GrabaHisKardex( Empresa, Delta, sPercepcionesFijas, ErrorCount) ) then
                     lOk := True
                  else
                  begin
                       LogError( iEmpleado, Self.Status );
                  end;
             end;
          except
                on Error: Exception do
                begin
                     LogError( iEmpleado, 'Error al agregar cambio de salario: ' + Error.Message );
                end;
          end;
     end;
     Result := lOk;
end;
{$else}


procedure TdmCliente.CargarEmpleadosPE;
const
     NODO_EMPLEADO = 'EMPLEADO';
     NODO_CLASIFI = 'CB_CLASIFI';
     NODO_SALARIO = 'CB_SALARIO';
     NODO_FECHA_REG = 'CB_FECHAREG';
     NODO_FECHA_IMSS = 'CB_FECHAIMSS';
     NODO_OBSERVA = 'OBSERVACIONES';
     NODO_EMPLEADOS_DATOS = 'INFO';
     NODO_EMPLEADOS_LISTA = 'EMPLEADOS';

var
   Nodo, Info: TZetaXMLNode;
begin
     FEmpleadosPE := TEmpleadosPE.Create;


     with FXMLTools do
     begin

          Nodo := GetNode( NODO_EMPLEADOS_LISTA );
          { Preguntar si son varios empleados }
          if Assigned( Nodo ) then
          begin
               Info := GetNode( NODO_EMPLEADOS_DATOS, Nodo );
               while Assigned( Info ) and HasChildren( Info ) do
               begin
                    FEmpleadosPE.AddEmpleado( TagAsInteger( GetNode( NODO_EMPLEADO, Info ), K_EMPLEADO_NULO ), TagAsFloat(GetNode( NODO_SALARIO, Info ),0.0), TagAsString( GetNode( NODO_CLASIFI, Info), VACIO),
                       TagAsDateTime( GetNode( NODO_FECHA_REG, Info), NullDateTime ),TagAsDateTime( GetNode( NODO_FECHA_IMSS, Info), NullDateTime ), TagAsString( GetNode( NODO_OBSERVA, Info),VACIO)   );
                    Info := GetNextSibling( Info );
               end
          end
          else
          begin
                   FEmpleadosPE.AddEmpleado( TagAsInteger( GetNode( NODO_EMPLEADO, Info ), K_EMPLEADO_NULO ), TagAsFloat(GetNode( NODO_SALARIO, Info ),0.0), TagAsString( GetNode( NODO_CLASIFI, Info), VACIO),
                       TagAsDateTime( GetNode( NODO_FECHA_REG, Info), NullDateTime ),TagAsDateTime( GetNode( NODO_FECHA_IMSS, Info), NullDateTime ), TagAsString( GetNode( NODO_OBSERVA, Info),VACIO)   );
          end;
     end;
     if ( FEmpleadosPE.slEmpleados.Count = 0 ) then
        raise Exception.Create( 'No se especific� un empleado' );
end;


function TdmCliente.AgregarCambiosPE: Boolean;
var
   i, iEmpleado : Integer;
   ErrorCount : Integer;
   FOtrosDatos : Variant;
   lOk: Boolean;
   dFechaSal: TDateTime;
   sPercepcionesFijas: String;
begin
     lOk := False;
     CargarEmpleadosPE;
     for i := 0 to ( FEmpleadosPE.slEmpleados.Count - 1 ) do
     begin
          iEmpleado := FEmpleadosPE.Empleado[ i ];
          try
             if FetchEmployee( cdsLista, iEmpleado ) then
             begin
                  with FXMLTools do
                  begin
                       FOtrosDatos := VarArrayOf( [FEmpleadosPE.FecReg[i], FEmpleadosPE.FecIMSS[i], FEmpleadosPE.Observa[i]] );
                  end;

                  // Enviar los cambios
                  with cdsLista do
                  begin
                       if not ( state in [ dsEdit, dsInsert ] ) then
                        Edit;

                       FieldByName('CB_SALARIO').AsFloat  :=   FEmpleadosPE.Salario[i];
                       FieldByName('CB_CLASIFI').AsString  := FEmpleadosPE.Clasifi[i];

                       PostData;
                       OnReconcileError := ReconcileError;
                       if VarIsNull( DeltaNull ) then     //MV( 21/Ago/2007) Se cambio esta parte para validar que el DataSet no va Null
                          lOK := True
                       else
                       begin
                            if Reconciliar( ServerRecursos.GrabaCambiosMultiples( Empresa, DeltaNull, FOtrosDatos, ErrorCount ) ) then
                               lOk := True
                            else
                            begin
                                 LogError( iEmpleado, Self.Status );
                            end;
                       end;
                  end;
             end
             else
             begin
                  DataBaseError( 'No se encontr� el empleado # ' + IntToStr( iEmpleado ) );
             end;
          except
                on Error: Exception do
                begin
                     LogError( iEmpleado, 'Error al agregar cambio de Puesto-Clasificacion: ' + Error.Message );
                end;
          end;


          if lOk then
          begin
             lOk := False;
             //Ahora realiza el cambio de Salario ( Esto puede ir una funcion aparte
             //FEmpleadosPE.FecReg[i], FEmpleadosPE.FecIMSS[i], FEmpleadosPE.Observa[i]
             try
                with cdsLista do
                begin
                     Data := ServerRecursos.GetEditHisKardex( Empresa, iEmpleado, 0, VACIO );  // Llenar DataSet Vacio con formato de campos de KARDEX
                     Append;
                     // Datos Iniciales ( L�gica de dmRecursos )
                     FieldByName( 'CB_CODIGO').AsInteger := iEmpleado;
                     with FXMLTools do
                     begin
                          FieldByName( 'CB_FECHA' ).AsDateTime :=  FEmpleadosPE.FecReg[i]; //TagAsDate( GetNode( 'FECHAREG' ), NullDateTime );
                          FieldByName( 'CB_FECHA_2' ).AsDateTime := FEmpleadosPE.FecIMSS[i]; //TagAsDate( GetNode( 'FECHAIMSS' ), NullDateTime );
                          if ( FieldByName( 'CB_FECHA_2' ).AsDateTime = NullDateTime ) then
                             FieldByName( 'CB_FECHA_2' ).AsDateTime := FieldByName( 'CB_FECHA' ).AsDateTime;
                     end;
                     FieldByName( 'CB_FAC_INT' ).AsFloat:= 0;
                     FieldByName( 'CB_GLOBAL' ).AsString  := K_GLOBAL_NO;
                     FieldByName( 'CB_REINGRE' ).AsString := K_GLOBAL_NO;
                     FieldByName( 'CB_AUTOSAL' ).AsString := K_GLOBAL_NO;
                     FieldByName( 'CB_NIVEL' ).AsInteger  := 5;
                     FieldByName( 'CB_STATUS' ).AsInteger := Ord( skCapturado );
                     FieldByName( 'CB_TIPO' ).AsString := K_T_CAMBIO;
                     InitCamposSalario( iEmpleado, dFechaSal );   // Datos tomados de COLABORA
                     with FXMLTools do  // Datos del XML
                     begin
                          FieldByName( 'CB_SALARIO' ).AsFloat :=  FEmpleadosPE.Salario[i];
                          FieldByName( 'CB_COMENTA' ).AsString := FEmpleadosPE.Observa[i]; ///TagAsString( GetNode( 'DESCRIPCION' ), VACIO );
                          FieldByName( 'CB_NOTA' ).AsString := FEmpleadosPE.Observa[i]; // TagAsString( GetNode( 'OBSERVACIONES' ), VACIO );
                     end;
                     PostData;
                     OnReconcileError := ReconcileError;
                     sPercepcionesFijas := GetPercepcionesFijas( iEmpleado, dFechaSal );
                     if Reconciliar( ServerRecursos.GrabaHisKardex( Empresa, Delta, sPercepcionesFijas, ErrorCount) ) then
                        lOk := True
                     else
                     begin
                          LogError( iEmpleado, Self.Status );
                     end;
                end;
             except
                   on Error: Exception do
                   begin
                        LogError( iEmpleado, 'Error al agregar cambio de salario: ' + Error.Message );
                   end;
             end;
          end;

     end;
     Result := lOk;
     FreeAndNil(FEmpleadosPE);
end;


procedure TdmCliente.CargarEmpleadosJornadaPresbaja;
const
     NODO_EMPLEADO = 'EMPLEADO';
     NODO_PUESTO = 'CB_PUESTO';
     NODO_CLASIFI = 'CB_CLASIFI';
     NODO_SALARIO = 'CB_SALARIO';
     NODO_TURNO = 'CB_TURNO';
     NODO_FECHA_REG = 'CB_FECHAREG';
     NODO_FECHA_IMSS = 'CB_FECHAIMSS';
     NODO_DESCRIPCION = 'DESCRIPCION';
     NODO_OBSERVA = 'OBSERVACIONES';
     NODO_EMPLEADOS_DATOS = 'INFO';
     NODO_EMPLEADOS_LISTA = 'EMPLEADOS';

var
   Nodo, Info: TZetaXMLNode;
begin
     FEmpleadosPE := TEmpleadosPE.Create;


     with FXMLTools do
     begin

          Nodo := GetNode( NODO_EMPLEADOS_LISTA );
          { Preguntar si son varios empleados }
          if Assigned( Nodo ) then
          begin
               Info := GetNode( NODO_EMPLEADOS_DATOS, Nodo );
               while Assigned( Info ) and HasChildren( Info ) do
               begin
                    FEmpleadosPE.AddEmpleadoJornada( TagAsInteger( GetNode( NODO_EMPLEADO, Info ), K_EMPLEADO_NULO ), TagAsFloat(GetNode( NODO_SALARIO, Info ),0.0), TagAsString( GetNode( NODO_CLASIFI, Info), VACIO),
                       TagAsDateTime( GetNode( NODO_FECHA_REG, Info), NullDateTime ),TagAsDateTime( GetNode( NODO_FECHA_IMSS, Info), NullDateTime ), TagAsString( GetNode( NODO_OBSERVA, Info),VACIO),
                       TagAsString( GetNode( NODO_PUESTO, Info),VACIO) ,  TagAsString( GetNode( NODO_TURNO, Info),VACIO), TagAsString( GetNode( NODO_DESCRIPCION, Info),VACIO)   );
                    Info := GetNextSibling( Info );
               end
          end
          else
          begin
                   FEmpleadosPE.AddEmpleadoJornada( TagAsInteger( GetNode( NODO_EMPLEADO, Info ), K_EMPLEADO_NULO ), TagAsFloat(GetNode( NODO_SALARIO, Info ),0.0), TagAsString( GetNode( NODO_CLASIFI, Info), VACIO),
                       TagAsDateTime( GetNode( NODO_FECHA_REG, Info), NullDateTime ),TagAsDateTime( GetNode( NODO_FECHA_IMSS, Info), NullDateTime ), TagAsString( GetNode( NODO_OBSERVA, Info),VACIO),
                       TagAsString( GetNode( NODO_PUESTO, Info),VACIO) ,  TagAsString( GetNode( NODO_TURNO, Info),VACIO) ,TagAsString( GetNode( NODO_DESCRIPCION, Info),VACIO)   );
          end;
     end;
     if ( FEmpleadosPE.slEmpleados.Count = 0 ) then
        raise Exception.Create( 'No se especific� un empleado' );
end;




function TdmCliente.AgregarCambioJornadaPresbaja: Boolean;
var
   i, iEmpleado : Integer;
   ErrorCount : Integer;
   FOtrosDatos : Variant;
   lOk: Boolean;
   dFechaSal: TDateTime;
   sPercepcionesFijas: String;
   fSalarioActual : TPesos;
   lCambioSalario : boolean;
begin
     lOk := False;
     lCambioSalario := False;
     CargarEmpleadosJornadaPresbaja;
     for i := 0 to ( FEmpleadosPE.slEmpleados.Count - 1 ) do
     begin
          iEmpleado := FEmpleadosPE.Empleado[ i ];
          try
             if FetchEmployee( cdsLista, iEmpleado ) then
             begin
                  lCambioSalario := FALSE;
                  with FXMLTools do
                  begin

                  {
                       dFecha := FOtrosDatos[0];
                       sDescrip := FOtrosDatos[1];
                       sObserva := FOtrosDatos[2];
                  }

                       FOtrosDatos := VarArrayOf( [FEmpleadosPE.FecReg[i], FEmpleadosPE.Descripcion[i], FEmpleadosPE.Observa[i]] );
                  end;

                  // Enviar los cambios
                  with cdsLista do
                  begin
                       if not ( state in [ dsEdit, dsInsert ] ) then
                        Edit;

                       fSalarioActual :=   FieldByName('CB_SALARIO').AsFloat;
                       if ( fSalarioActual <>  FEmpleadosPE.GetSalario(i) ) then
                       begin
                          FieldByName('CB_SALARIO').AsFloat  :=   FEmpleadosPE.GetSalario(i);
                          lCambioSalario := TRUE;
                       end
                       else
                       begin
                          lCambioSalario := FALSE;
                          FieldByName( 'CB_AUTOSAL' ).AsString := K_GLOBAL_NO;
                       end;
                       FieldByName( 'CB_CLASIFI' ).AsString  := FEmpleadosPE.GetClasifi(i);
                       FieldByName( 'CB_PUESTO'  ).AsString  := FEmpleadosPE.GetPuesto(i);
                       FieldByName( 'CB_TURNO'   ).AsString  := FEmpleadosPE.GetTurno(i);

                       PostData;
                       OnReconcileError := ReconcileError;
                       if VarIsNull( DeltaNull ) then     //MV( 21/Ago/2007) Se cambio esta parte para validar que el DataSet no va Null
                          lOK := True
                       else
                       begin
                            if Reconciliar( ServerRecursos.GrabaCambiosMultiples( Empresa, DeltaNull, FOtrosDatos, ErrorCount ) ) then
                               lOk := True
                            else
                            begin
                                 LogError( iEmpleado, Self.Status );
                            end;
                       end;
                  end;
             end
             else
             begin
                  DataBaseError( 'No se encontr� el empleado # ' + IntToStr( iEmpleado ) );
             end;
          except
                on Error: Exception do
                begin
                     LogError( iEmpleado, 'Error al agregar cambio de Puesto-Clasificacion: ' + Error.Message );
                end;
          end;


          if lOk and lCambioSalario then
          begin
             lOk := False;
             //Ahora realiza el cambio de Salario ( Esto puede ir una funcion aparte
             //FEmpleadosPE.FecReg[i], FEmpleadosPE.FecIMSS[i], FEmpleadosPE.Observa[i]
             try
                with cdsLista do
                begin
                     Data := ServerRecursos.GetEditHisKardex( Empresa, iEmpleado, 0, VACIO );  // Llenar DataSet Vacio con formato de campos de KARDEX
                     Append;
                     // Datos Iniciales ( L�gica de dmRecursos )
                     FieldByName( 'CB_CODIGO').AsInteger := iEmpleado;
                     with FXMLTools do
                     begin
                          FieldByName( 'CB_FECHA' ).AsDateTime :=  FEmpleadosPE.FecReg[i]; //TagAsDate( GetNode( 'FECHAREG' ), NullDateTime );
                          FieldByName( 'CB_FECHA_2' ).AsDateTime := FEmpleadosPE.FecIMSS[i]; //TagAsDate( GetNode( 'FECHAIMSS' ), NullDateTime );
                          if ( FieldByName( 'CB_FECHA_2' ).AsDateTime = NullDateTime ) then
                             FieldByName( 'CB_FECHA_2' ).AsDateTime := FieldByName( 'CB_FECHA' ).AsDateTime;
                     end;
                     FieldByName( 'CB_FAC_INT' ).AsFloat:= 0;
                     FieldByName( 'CB_GLOBAL' ).AsString  := K_GLOBAL_NO;
                     FieldByName( 'CB_REINGRE' ).AsString := K_GLOBAL_NO;
                     FieldByName( 'CB_AUTOSAL' ).AsString := K_GLOBAL_SI;
                     FieldByName( 'CB_NIVEL' ).AsInteger  := 5;
                     FieldByName( 'CB_STATUS' ).AsInteger := Ord( skCapturado );
                     FieldByName( 'CB_TIPO' ).AsString := K_T_CAMBIO;
                     InitCamposSalario( iEmpleado, dFechaSal );   // Datos tomados de COLABORA
                     with FXMLTools do  // Datos del XML
                     begin
                          FieldByName( 'CB_SALARIO' ).AsFloat :=  FEmpleadosPE.Salario[i];
                          FieldByName( 'CB_COMENTA' ).AsString := FEmpleadosPE.Descripcion[i]; ///TagAsString( GetNode( 'DESCRIPCION' ), VACIO );
                          FieldByName( 'CB_NOTA' ).AsString := FEmpleadosPE.Observa[i]; // TagAsString( GetNode( 'OBSERVACIONES' ), VACIO );
                     end;
                     PostData;
                     OnReconcileError := ReconcileError;
                     sPercepcionesFijas := GetPercepcionesFijas( iEmpleado, dFechaSal );
                     if Reconciliar( ServerRecursos.GrabaHisKardex( Empresa, Delta, sPercepcionesFijas, ErrorCount) ) then
                        lOk := True
                     else
                     begin
                          LogError( iEmpleado, Self.Status );
                     end;
                end;
             except
                   on Error: Exception do
                   begin
                        LogError( iEmpleado, 'Error al agregar cambio de salario: ' + Error.Message );
                   end;
             end;
          end;

     end;
     Result := lOk;
     FreeAndNil(FEmpleadosPE);
end;





function TdmCliente.AgregarCambiosMultiplesPresbaja: Boolean;
var
   i, j, iEmpleado : Integer;
   ErrorCount : Integer;
   FOtrosDatos : Variant;
   lOk: Boolean;
begin
     lOk := False;
     CargarEmpleados;
     for i := 0 to ( FEmpleados.Count - 1 ) do
     begin
          iEmpleado := FEmpleados.Empleado[ i ];
          try
             if FetchEmployee( cdsLista, iEmpleado ) then
             begin
                  with FXMLTools do
                  begin
                       FOtrosDatos := VarArrayOf( [ TagAsDate( GetNode( 'FECHAREG' ), NullDateTime ),
                                                    TagAsString( GetNode( 'DESCRIPCION' ), VACIO ),
                                                    TagAsString( GetNode( 'OBSERVACIONES' ), VACIO ) ] );
                  end;
                  // Revisa si se incluyen los tags de cambios
                  UpdateNodeInfo( cdsLista, 'CB_PUESTO' );
                  UpdateNodeInfo( cdsLista, 'CB_CLASIFI' );
                  UpdateNodeInfo( cdsLista, 'CB_TURNO' );
                  UpdateNodeInfo( cdsLista, 'CB_CONTRAT' );
                  UpdateNodeInfo( cdsLista, 'CB_NIVEL0' );
                  for j := 1 to 9 do
                  begin
                       UpdateNodeInfo( cdsLista, 'CB_NIVEL' + IntToStr( j ) );
                  end;
                  // Enviar los cambios
                  with cdsLista do
                  begin
                       PostData;
                       OnReconcileError := ReconcileError;
                       if VarIsNull( DeltaNull ) then     //MV( 21/Ago/2007) Se cambio esta parte para validar que el DataSet no va Null
                          lOK := True
                       else
                       begin
                            if Reconciliar( ServerRecursos.GrabaCambiosMultiples( Empresa, DeltaNull, FOtrosDatos, ErrorCount ) ) then
                               lOk := True
                            else
                            begin
                                 LogError( iEmpleado, Self.Status );
                            end;
                       end;
                  end;
             end
             else
             begin
                  DataBaseError( 'No se encontr� el empleado # ' + IntToStr( iEmpleado ) );
             end;
          except
                on Error: Exception do
                begin
                     LogError( iEmpleado, 'Error al agregar cambios m�ltiples: ' + Error.Message );
                end;
          end;
     end;
     Result := lOk;
end;



function TdmCliente.ObtenValUso( const sTipo: String ): Integer;
var
   sUso: String;
   iUso: Integer;
   oDataSet : TZetaClientDataSet;
begin
     Result := 0;
     Autorizacion.Cargar( GetAuto );

     {$ifdef WF_NUBE}
     Result := K_AUTORIZADO;
     {$else}

     if( Autorizacion.OkModulo( okWorkFlow ) )then // Modulo autorizado
     begin
          if( Autorizacion.EsDemo )then
          begin
               oDataSet := TZetaClientDataSet.Create( self );
               try
                  with oDataSet do
                  begin
                       Data := ServerComparte.GetValidacionUso( sTipo );
                       if Not IsEmpty then
                       begin
                            First;
                            sUso := Decrypt( FieldByName( 'USO' ).AsString );
                            iUso := StrToIntDef( sUso, 0 );
                            Result := iUso;
                       end;
                  end;
               finally
                      FreeAndNil( oDataSet );
               end;
          end
          else
              Result := K_AUTORIZADO;
     end
     else
         Result := K_NO_AUTORIZADO;
     {$endif}
end;

procedure TdmCliente.GrabaValUso( const sTipo: String; const iUso: Integer );
var
   sUso: String;
begin
     sUso := Encrypt( Format( '%d', [ iUso ] ) );
     ServerComparte.GrabaValidacionUso( sTipo, sUso );
end;
{$endif}



procedure TdmCliente.CargarAutorizacion;
begin
     Autorizacion.Cargar( GetAuto );
end;


function TdmCliente.InitComparteCompany: Integer;
begin
     with cdsComparte  do
     begin
          if not Active or HayQueRefrescar then
          begin
               Data := ServerComparte.GetComparte;
               ResetDataChange;
          end;
          if Active then
             Result := RecordCount
          else
              Result := 0;
     end;
end;

function TdmCliente.AgregarExcepcionesNomina( const iUsuario:integer) : Boolean;
const
     NODO_REGISTROS = 'REGISTROS';
     K_INTEGER_DEFAULT = 0;
     K_SEPARADOR = ',';
     K_LINE_BREAK = AnsiString(#13#10);
     K_MENSAJE_BIT = '%d|Error%s';
     K_MENSAJE_BIT_TIPO_OPERACION = '%d|%s|%s';
     K_CONSULTA = 'SELECT PE_STATUS from PERIODO where ( PE_YEAR = ''%d'' ) and  ( PE_TIPO = ''%d'' ) and ( PE_NUMERO = ''%d'' )';

var
   ErrorCount: Integer;
   lOk: Boolean;
   lOkProcess : Boolean;
   lRegistro: Boolean;
   lRegistrosAtributos : Boolean;
   Registros, Registro: TZetaXMLNode;
   Parametros: TZetaParams;
   Texto : TStringList;
   sEmpleado : string;
   sConcepto : string;
   sMonto : string;
   sReferencia : string;
   sRegistro : string;
   sEmpresa : string;
   sTipoBitacora : string;
   sPeriodoYear, sPeriodoTipo, sPeriodoNumero, sTipoOperacion, sRevisarIncapacidad : string;
   iPeriodoYear, iPeriodoTipo, iPeriodoNumero, iTipoOperacion: Integer;
   lRevisarIncapacidad : Boolean;
   lPeriodo : Boolean;
   oProcessData : TProcessInfo;
   FProcessList : TProcessList;
   sMensaje : string;
   iStatusPeriodo : Integer;
   cdsASCII : TZetaClientDataSet;
   cdsProcessLog : TZetaClientDataSet;
   oConsultaStatusPeriodo : TClientDataSet;

   function CargaParametros( Parametros: TZetaParams ): TZetaParams;
   begin
        //Generar Lista de parametros
        with Parametros do
        begin
             // Activos Sistema
             AddDate( 'FechaAsistencia',  dmCliente.FechaDefault ) ;
             AddDate( 'FechaDefault', dmCliente.FechaDefault ) ;
             AddInteger( 'YearDefault', iPeriodoYear );
             AddString( 'NombreUsuario', VACIO );
             AddString( 'CodigoEmpresa', VACIO );
             // Activos de Periodo
             AddInteger( 'Year', iPeriodoYear );
             AddInteger( 'Tipo', iPeriodoTipo );
             AddInteger( 'Numero', iPeriodoNumero );
             // Activos de IMSS
             AddString( 'RegistroPatronal', VACIO );
             AddInteger( 'IMSSYear', 0 );
             AddInteger( 'IMSSMes', 0 );
             AddInteger( 'IMSSTipo', 0 );
             //Parametros de Excepciones de Nomina
             AddInteger( 'Importacion', 0 );
             AddInteger( 'Formato', 1 );
             AddInteger( 'Operacion', iTipoOperacion );
             AddBoolean( 'NoIncapacitados', lRevisarIncapacidad );
             AddString( 'Archivo', 'Datos de WFN para Excepciones de Nomina');
        end;

        Result := Parametros;
   end;

   function ValidaNodosXML( sPeriodoYear, sPeriodoTipo, sPeriodoNumero, sTipoOperacion, sRevisarIncapacidad : string; var iPeriodoYear, iPeriodoTipo, iPeriodoNumero, iTipoOperacion: Integer; var lRevisarIncapacidad : Boolean; var Texto: TStringList): boolean;
   begin
        Result := True;
        try
           iPeriodoYear := StrToInt(sPeriodoYear);
        except
          on Exception : EConvertError do
             begin
                  iPeriodoYear := 0;
                  Texto.append( 'Error al agregar excepciones de n�mina: Atributo <PeriodoYear>' );
                  Result := False;
             end;
        end;
        try
           iPeriodoTipo := StrToInt(sPeriodoTipo);
        except
          on Exception : EConvertError do
             begin
                  iPeriodoTipo := 0;
                  Texto.append( 'Error al agregar excepciones de n�mina: Atributo <TipoNominaCodigo>' );
                  Result := False;
             end;
        end;
        try
           iPeriodoNumero := StrToInt(sPeriodoNumero);
        except
          on Exception : EConvertError do
             begin
                  iPeriodoNumero := 0;
                  Texto.append( 'Error al agregar excepciones de n�mina: Atributo <PeriodoNumero>' );
                  Result := False;
             end;
        end;
        try
           iTipoOperacion := StrToInt(sTipoOperacion);
        except
          on Exception : EConvertError do
             begin
                  iTipoOperacion := 0;
                  Texto.append( 'Error al agregar excepciones de n�mina: Atributo <TipoOperacion>' );
                  Result := False;
             end;
        end;
        try
           lRevisarIncapacidad := StrToBool(sRevisarIncapacidad);
        except
          on Exception : EConvertError do
             begin
                  lRevisarIncapacidad := True;
                  Texto.append( 'Error al agregar excepciones de n�mina: Atributo <RevisarIncapacidad>' );
                  Result := False;
             end;
        end;
   end;

   procedure LeeNodosXML( Registros: TZetaXMLNode; var sPeriodoYear, sPeriodoTipo, sPeriodoNumero, sTipoOperacion, sRevisarIncapacidad : string );
   begin
        sPeriodoYear := Registros.GetAttributeNS( 'PeriodoYear', VACIO );//Obligatorio
        sPeriodoTipo := Registros.GetAttributeNS( 'TipoNominaCodigo', VACIO );//Obligatorio
        sPeriodoNumero := Registros.GetAttributeNS( 'PeriodoNumero', VACIO );//Obligatorio
        sTipoOperacion := Registros.GetAttributeNS( 'TipoOperacion', VACIO );//Obligatorio
        sRevisarIncapacidad := Registros.GetAttributeNS( 'RevisarIncapacidad', VACIO );//Obligatorio
   end;

   function ValidaPeriodo( Parametros: TZetaParams; iPeriodoTipo: Integer ):boolean;
   var
      StatusPeriodo : eStatusPeriodo;
   begin
        oConsultaStatusPeriodo.Data := ServerConsultas.GetQueryGralTodos( Empresa, Format( K_CONSULTA, [ Parametros.ParamByName('Year').AsInteger, iPeriodoTipo , Parametros.ParamByName('Numero').AsInteger ] ) );
        if( not oConsultaStatusPeriodo.Eof )then
        begin
            iStatusPeriodo := oConsultaStatusPeriodo.FieldByName( 'PE_STATUS' ).AsInteger;
            StatusPeriodo :=  eStatusPeriodo( iStatusPeriodo );

            if( StatusPeriodo < spAfectadaParcial ) then
                Result := True
            else
                Result := False;
        end
        else
            Result := False;
   end;

   function IniciarlizarLista( cdsLista: TZetaClientDataSet ) : TZetaClientDataSet;
   begin
        with cdsLista do
        begin
             Active:= False;
             Filtered := False;
             Filter := '';
             IndexFieldNames := '';
             AddStringField('RENGLON',256);
             CreateTempDataset;
             Open;
             Insert;
        end;
        Result := cdsLista;
   end;

   function LlenarLista( Registros: TZetaXMLNode; cdsLista : TZetaClientDataSet): TZetaClientDataSet;
   var
      sEmpleado, sConcepto, sMonto, sReferencia, sRegistro : String;
   begin
        sEmpleado := Registro.GetAttributeNS( 'EmpNumero', VACIO );
        sConcepto := Registro.GetAttributeNS( 'ConceptoNumero', VACIO );
        sMonto :=  Registro.GetAttributeNS( 'MovimientoMonto', VACIO );
        sReferencia := Registro.GetAttributeNS( 'MovimientoReferencia', VACIO );
        sRegistro := sEmpleado + K_SEPARADOR + sConcepto + K_SEPARADOR + sMonto + K_SEPARADOR +  sReferencia; //
        with cdsLista do
        begin
             Append;
             FieldByName( 'RENGLON' ).AsString := sRegistro;
             Post;
        end;
        Result := cdsLista;
   end;

   function ValidaLista ( Parametros : TZetaParams; cdsLista, cdsASCII : TZetaClientDataSet; var lOk: Boolean; var Texto: TStringList ) : TZetaClientDataSet;
   var
      sMensaje : String;
      ErrorCount : Integer;
   begin
        cdsASCII.Data := ServerNomina.ImportarMovimientosGetASCII(Empresa, Parametros.VarValues, cdsLista.Data  , ErrorCount);
        if( ErrorCount > 0 ) then
        begin
             with cdsASCII do
             begin
                  Refrescar;
                  First;
                  while not Eof do
                  begin
                       if ( FieldByName( 'DESC_ERROR' ).AsString <> '' ) then
                       begin
                            lOk := False;
                            sMensaje := Format( K_MENSAJE_BIT , [ FieldByName( 'CB_CODIGO' ).AsInteger  , FieldByName( 'DESC_ERROR' ).AsString ] );
                            Texto.append( sMensaje );
                            Delete;
                       end
                       else begin
                            Next;
                       end;
                  end;
                  First;
             end;
        end;
        Result := cdsASCII;
   end;

   procedure LeerMensajeBitacora( cdsProcessLog: TZetaClientDataSet; var lOkProcess: Boolean; var Texto: TStringList );
   var
      sMensaje : String;
   begin
         with cdsProcessLog do
         begin
              Refrescar;
              First;
              lOkProcess := True;
              while not Eof do
              begin
                   case eTipoBitacora( FieldByName( 'BI_TIPO' ).AsInteger ) of
                        tbError:
                                begin
                                     sTipoBitacora := 'Error';
                                     sMensaje := Format( K_MENSAJE_BIT_TIPO_OPERACION, [ FieldByName( 'CB_CODIGO' ).AsInteger, sTipoBitacora, FieldByName( 'BI_TEXTO' ).AsString] );
                                     Texto.append( sMensaje );
                                     lOkProcess := False;
                                end;
                        tbErrorGrave:
                                begin
                                     sTipoBitacora := 'Error Grave';
                                     sMensaje := Format( K_MENSAJE_BIT_TIPO_OPERACION, [ FieldByName( 'CB_CODIGO' ).AsInteger, sTipoBitacora, FieldByName( 'BI_TEXTO' ).AsString] );
                                     Texto.append( sMensaje );
                                     lOkProcess := False;
                                end;
                   end;
                   Next;
              end;
         end;
   end;

begin
     cdsLista := TZetaClientDataSet.Create(Self);
     cdsASCII := TZetaClientDataSet.Create(Self);
     cdsProcessLog := TZetaClientDataSet.Create(Self);
     oConsultaStatusPeriodo := TZetaClientDataSet.Create( Self );
     Parametros := TZetaParams.Create;
     FProcessList :=   TProcessList.Create;
     oProcessData := TProcessInfo.Create( FProcessList );
     Texto := TStringList.Create;
     lOk := False;
     lRegistro := False;
     lRegistrosAtributos := True;
     try
        with FXMLTools do
        begin
             Registros := GetNode( 'REGISTROS' );
             sEmpresa := TagAsString( GetNode( 'EMPRESA' ), '' );
             if Assigned ( Registros ) then
             begin
                  Texto.Append( Format( '|Empresa %s|', [ sEmpresa ] ) );
                  LeeNodosXML(Registros, sPeriodoYear, sPeriodoTipo, sPeriodoNumero, sTipoOperacion, sRevisarIncapacidad); //Obtener Atributos
                  lRegistrosAtributos := ValidaNodosXML( sPeriodoYear, sPeriodoTipo, sPeriodoNumero, sTipoOperacion, sRevisarIncapacidad, iPeriodoYear, iPeriodoTipo, iPeriodoNumero, iTipoOperacion, lRevisarIncapacidad, Texto);
                  if ( lRegistrosAtributos ) then
                  begin
                       Parametros := CargaParametros(Parametros);//Carga Parametros
                       lPeriodo := ValidaPeriodo( Parametros, iPeriodoTipo );
                       if ( lPeriodo ) then
                       begin
                            cdsLista := IniciarlizarLista( cdsLista );
                            if Assigned( Registros ) then
                            begin
                                 Registro := GetNode( 'REGISTRO', Registros );
                                 while Assigned( Registro ) do
                                 begin
                                      cdsLista := LlenarLista( Registros, cdsLista ); //Genera Datos a Procesar
                                      lRegistro := True;
                                      Registro := GetNextSibling( Registro );
                                 end;
                                 if ( lRegistro ) then
                                 begin
                                      lOk := True;
                                      cdsASCII := ValidaLista( Parametros, cdsLista, cdsASCII, lOk, Texto ); //Enviar y Validar datos al Servidor
                                      oProcessData := TProcessInfo.Create( FProcessList );
                                      oProcessData.SetResultado( ServerNomina.ImportarMovimientos( Empresa, Parametros.VarValues, cdsASCII.Data ) );
                                      cdsProcessLog.Data := ServerConsultas.GetProcessLog( dmCliente.Empresa, oProcessData.Folio );
                                      LeerMensajeBitacora( cdsProcessLog, lOkProcess, Texto );
                                 end
                                 else
                                 begin
                                     Texto.append( 'Error al agregar excepciones de n�mina: Nodo <REGISTRO> no especificado' );
                                 end;
                            end;
                       end
                       else
                       begin
                            Texto.append( 'Error al agregar excepciones de n�mina: periodo de n�mina ya est� afectada' );
                       end
                  end;
             end
             else
             begin
                  Texto.append( 'Error al agregar excepciones de n�mina: Nodo <REGISTROS> no especificado' );
             end;
        end;
     except
           on Error: Exception do
           begin
                Texto.append( 'Error al agregar excepciones de n�mina: ' + Error.Message );
           end;
     end;

     if ( not lOkProcess ) then lOk := False;

     if ( not lOk ) then LogError( 0, Texto.Text );

     Result := lOk;
end;

function TdmCliente.AgregarModificacionColabora( const iUsuario:integer) : Boolean;
var
   iEmpNumero : Integer;
   sEmpresa, sNombreCompany, sBanEle, sCtaGas, sCtaVal, sSubCta, sBanco  : String;
   Registros, Registro: TZetaXMLNode;
   Texto : TStringList;
   lOk: Boolean;
   lOkAux: Boolean;
   sCampo, sValor : string;


    function EsCampoChar( const sCampo: String; var iSize: Byte ): Boolean;
   var
      i: Byte;
   begin
        Result:= FALSE;
        iSize := 0;
        for i:= 1 to K_QTY_CHAR do
            if ( aCamposChar[i,1] = sCampo ) then
            begin
                 Result:= TRUE;
                 iSize:= StrToInt( aCamposChar[i,2] );
                 Break;
            end;
   end;

   function EsCampoVarChar( const sCampo: String; var iSize: Byte ): Boolean;
   var
      i: Byte;
   begin
        Result:= FALSE;
        iSize:= 0;
        for i:= 1 to K_QTY_VARCHAR do
            if ( aCamposVarChar[i,1] = sCampo ) then
            begin
                 Result:= TRUE;
                 iSize:= StrToInt( aCamposVarChar[i,2] );
                 Break;
            end;
   end;

   function EsCampoNumerico( const sCampo: String ): Boolean;
   var
      i: Byte;
   begin
        Result:= FALSE;
        for i:= 1 to K_QTY_NUMEROS do
            if ( aCamposNumericos[i] = sCampo ) then
            begin
                 Result:= TRUE;
                 Break;
            end;
   end;

   function EsCampoFecha( const sCampo: String ): Boolean;
   var
      i: Byte;
   begin
        Result:= FALSE;
        for i:= 1 to K_QTY_FECHA do
            if ( aCamposFecha[i] = sCampo ) then
            begin
                 Result:= TRUE;
                 Break;
            end;
   end;

   function EsCampoBooleano( const sCampo: String ): Boolean;
   var
      i: Byte;
   begin
        Result:= FALSE;
        for i:= 1 to K_QTY_BOOLEANOS do
           if ( aCamposBooleano[i] = sCampo ) then
           begin
                Result:= TRUE;
                Break;
           end;
   end;

   function EsCampoValido( sCampo : String ): Boolean;
   var
      Long : Byte;
   begin
        Result := ( EsCampoChar( sCampo, Long ) or
                  EsCampoVarChar( sCampo, Long ) or
                  EsCampoNumerico( sCampo ) or
                  EsCampoFecha( sCampo ) ) ;
   end;

   function Asigna( sCampo : String; sValor : String; var Texto : TStringList ): Boolean;
   begin
        Result := False;
        with dmCliente.cdsEmpleado do
        begin
             if ( EsCampoValido( sCampo )  ) then
             begin
                  if ( EsCampoFecha(sCampo) ) then
                     FieldByName(sCampo).AsDateTime := ZetaCommonTools.scandate('yyyy-mm-dd', sValor )
                  else
                      FieldByName( sCampo ).AsString := sValor;
                  Result := True;
             end
             else
             begin
                  Texto.Add(  IntToStr( FEmpleado ) +  '|' + 'Error actualizaci�n de datos de empleado: el campo ' + sCampo + ' no es correcto');
                  Result := False;
             end;
        end;
   end;

   procedure CargaParametros( Registro: TZetaXMLNode; var iEmpNumero: Integer; var sCampo, sValor: String );
   begin
        with FXMLTools do
         begin
              while Assigned( Registro ) do
              begin
                   iEmpNumero := AttributeAsInteger(Registro, 'EmpNumero',  0  );
                   sCampo    := AttributeAsString( Registro, 'Campo', '' );
                   sValor    := AttributeAsString( Registro, 'Valor', '' );
                   break;
              end;
         end;
   end;

   function AsignaValores(sCampo, sValor : String; var Texto : TStringList ): Boolean;
   begin
        Result := Asigna( sCampo, sValor, Texto );
   end;

   procedure ActualizaDatosEmpleado( const iEmpNumero: TNumEmp; var sCampo, sValor: String; var Texto : TStringList ; var lOk: Boolean );
   begin
        if ( SetEmpleadoNumero( cdsEmpleado, iEmpNumero ) )   then
        begin
             if not ( cdsEmpleado.State in [ dsEdit ] ) then
             begin
                  cdsEmpleado.Edit;
             end;
             try
                if ( AsignaValores( sCampo, sValor, Texto )) then
                begin
                     ValidaEnviar( cdsEmpleado, Texto );
                     if ( FHuboErrores ) then lOk := true;
                end
                else
                begin
                     lOk := True;
                end;
             except
                   on Error : Exception do
                   begin
                        cdsEmpleado.CancelUpdates;
                        Texto.append( IntToStr( FEmpleado ) +  '|' + 'Error actualizaci�n de datos de empleado: ' + Error.Message );
                        lOk := true;
                   end;
             end;
        end
        else
        begin
             Texto.Append( Format( 'Empleado #%d no existe en la empresa %s:%s', [ iEmpNumero, sEmpresa, sNombreCompany ] ) );
             lOk := true;
        end;
   end;
begin
     Texto := TStringList.Create;
     lOk := False;
     lOkAux := False;
     try
        with FXMLTools do
        begin
             Registros := GetNode( 'REGISTROS' );
             sEmpresa := TagAsString( GetNode( 'EMPRESA' ), '' );
             if Assigned ( Registros ) then
             begin
                  Texto.Append( Format( '|Empresa %s|', [ sEmpresa ] ) );
                  sNombreCompany := cdsCompany.FieldByName('CM_NOMBRE').AsString;
                  if Assigned( Registros ) then
                  begin
                       Registro := GetNode( 'REGISTRO', Registros );
                       while Assigned( Registro ) do
                       begin
                            CargaParametros( Registro, iEmpNumero, sCampo, sValor );
                            ActualizaDatosEmpleado( iEmpNumero, sCampo, sValor, Texto, lOk );
                            if ( ( lOk ) and ( lOkAux = False ) ) then lOkAux := True;
                            Registro := GetNextSibling( Registro );
                       end;
                  end;
             end
             else
             begin
                  Texto.append( 'Error actualizaci�n de datos de empleado: Nodo <REGISTROS> no especificado' );
                  lOk := true;
             end;
        end;
     except
           on Error: Exception do
           begin
                Texto.append( 'Error actualizaci�n de datos de empleado: ' + Error.Message );
                lOk := true;
           end;
     end;
     if(  ( lOk ) or ( lOkAux ) ) then LogError(0, Texto.Text );
     Result := lOk;
end;

//Inclusion de CdsEmpleados
procedure TdmCliente.cdsEmpleadoAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
   FOtrosDatos : Variant;
   FListaPercepFijas : string;
begin
     ErrorCount := 0;
     FListaPercepFijas := VACIO;
     with dmCliente.cdsEmpleado do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ChangeCount > 0 then
          begin
               Reconcile ( ServerRecursos.GrabaDatosEmpleado( Empresa, Delta, FOtrosDatos, FListaPercepFijas, ErrorCount ) );
          end;
     end;
end;


procedure TdmCliente.ValidaEnviar(DataSet: TZetaClientDataSet; var Texto: TStringList);
begin
     FHuboErrorEnviar := TRUE;
     try
        DataSet.Enviar;
        FHuboErrorEnviar := FALSE;
     except
           on Error: Exception do
           begin
                FHuboErrores := True;
                DataSet.CancelUpdates;
                Texto.Add( Format ( 'Error actualizaci�n de datos de empleado %d : %s', [ FEmpleado, Error.Message] ) );
           end;
     end;
end;




function TdmCliente.SetEmpleadoNumero( oDataSet: TZetaClientDataSet; const iEmpleado: TNumEmp ): Boolean;
var
   oDatos: OleVariant;
begin
     FServidor := nil;
     Result := Servidor.GetEmpleado( Empresa, iEmpleado, oDatos );
     if Result then
     begin
          with oDataSet do
          begin
               Data := oDatos;
               FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
          end;
          FEmpleadoBOF := False;
          FEmpleadoEOF := False;
     end;
end;

//PENSKE - Agregar Cr�dito INFONAVIT
function TdmCliente.AgregarInfonavit( const iUsuario:integer ) : Boolean;
const
     K_INFO_DESCRIPCION = 'Cr�dito capturado en WFN';
var
   iEmpNumero: Integer;
   dFechaInicio: TDateTime;
   dFechaOtorgamiento: TDateTime;
   sNoCredito: string;
   iTPrestamo: integer;
   dDescuento: Real;
   Registros, Registro: TZetaXMLNode;
   Texto : TStringList;
   lOk, lOkProcess, lOkProcessAux, lRegistro: Boolean;
   sEmpresa,sNombreCompany : string;

   procedure AgregaColumna( const sCampo: String; const eTipo: eTipoGlobal; const iLongitud: Integer; var oSDataSet:TServerDataSet );
   begin
        //Agrega Campo al DataSet
        with oSDataSet do
        begin
             case eTipo of
                  tgBooleano : AddBooleanField( sCampo );
                  tgFloat : AddFloatField( sCampo );
                  tgNumero : AddIntegerField( sCampo );
                  tgFecha : AddDateField( sCampo );
             else
                  AddStringField( sCampo, iLongitud );
             end;
        end;
    end;

   procedure LimpiaEstructuraCreditos;
   begin
        cdsImportInfonavit.Close;
        cdsImportInfonavit.Fields.Clear;
        cdsImportInfonavit.FieldDefs.Clear;
   end;

   procedure DefineEstructuraCreditos;
   begin
        LimpiaEstructuraCreditos;
        with cdsImportInfonavit do
        begin
             AgregaColumna( 'CB_CODIGO', tgNumero, 9, cdsImportInfonavit );
             AgregaColumna( 'CB_INFCRED', tgTexto, 10, cdsImportInfonavit );
             AgregaColumna( 'CB_INFTIPO', tgNumero, K_ANCHO_CODIGO1, cdsImportInfonavit );
             AgregaColumna( 'CB_INFTASA', tgFloat, K_ANCHO_PESOS, cdsImportInfonavit );
             AgregaColumna( 'CB_INFDISM', tgTexto, 2, cdsImportInfonavit );
             AgregaColumna( 'CB_INF_ANT', tgFecha, K_ANCHO_FECHA, cdsImportInfonavit );
             AgregaColumna( 'DESCRIPCION', tgTexto, K_ANCHO_OBSERVACIONES, cdsImportInfonavit );
             AgregaColumna( 'CB_INF_INI', tgFecha, K_ANCHO_FECHA, cdsImportInfonavit );
             AgregaColumna( 'CB_SEGSOC', tgTexto, K_ANCHO_DESCINFO, cdsImportInfonavit );
             AgregaColumna( 'TPRESTAMO', tgNumero, 2, cdsImportInfonavit );
             AgregaColumna( 'KI_FECHA', tgFecha, K_ANCHO_FECHA, cdsImportInfonavit );
             AgregaColumna( 'KI_TIPO', tgNumero, K_ANCHO_CODIGO1, cdsImportInfonavit );
             if not Active then
             begin
                  CreateDataSet;
                  Open;
             end;
             EmptyDataSet;
        end;
   end;

   procedure CargaParametros( Registro: TZetaXMLNode; var iEmpNumero: Integer; var dFechaInicio: TDateTime; var dFechaOtorgamiento: TDateTime; var sNoCredito: string; var iTPrestamo: integer; var dDescuento: Real );
   begin
        with FXMLTools do
         begin
              while Assigned( Registro ) do
              begin
                   iEmpNumero := AttributeAsInteger(Registro, 'EmpNumero',  K_EMPLEADO_NULO );
                   dFechaInicio := AttributeAsDateTime( Registro, 'CB_INF_INI', 0 );
                   dFechaOtorgamiento := AttributeAsDateTime( Registro, 'CB_INF_ANT', 0 );
                   sNoCredito := AttributeAsString( Registro, 'CB_INFCRED', VACIO );
                   iTPrestamo := AttributeAsInteger( Registro, 'CB_INFTIPO', 0 );
                   dDescuento := AttributeAsFloat( Registro, 'CB_INFTASA', 0 );
                   break;
              end;
         end;
   end;

   procedure AgregarDatosDatasetInfonavit( var iEmpNumero: Integer; var dFechaInicio: TDateTime; var dFechaOtorgamiento: TDateTime; var sNoCredito: string; var iTPrestamo: integer; var dDescuento: Real );
   begin
        with cdsImportInfonavit do
        begin
             Append;
             cdsImportInfonavit.FieldByName('CB_CODIGO').AsInteger   := iEmpNumero;
             cdsImportInfonavit.FieldByName('DESCRIPCION').AsString  := K_INFO_DESCRIPCION;
             cdsImportInfonavit.FieldByName('CB_INFDISM').AsString   := K_GLOBAL_NO;
             cdsImportInfonavit.FieldByName('CB_INFCRED').AsString   := sNoCredito;
             cdsImportInfonavit.FieldByName('CB_INFTASA').AsFloat    := dDescuento;
             cdsImportInfonavit.FieldByName('CB_INF_INI').AsDateTime := dFechaInicio;
             cdsImportInfonavit.FieldByName('CB_INF_ANT').AsDateTime := dFechaOtorgamiento;
             cdsImportInfonavit.FieldByName('CB_INFTIPO').AsInteger  := iTPrestamo;
             cdsImportInfonavit.FieldByName('KI_FECHA').AsDateTime := dFechaInicio;
        end;
   end;

   procedure EnviarDatosDatasetInfonavit;
   begin
        with cdsImportInfonavit do
        begin
             Post;
             if ( ValidacionInfonavit ) then Delete;
        end;
   end;

begin
     Texto := TStringList.Create;
     lOk := False;
     lRegistro := False;
     lOkProcess := False;
     lOkProcessAux := False;
     try
        try
           with FXMLTools do
           begin
                Registros := GetNode( 'REGISTROS' );
                sEmpresa := TagAsString( GetNode( 'EMPRESA' ), '' );
                if Assigned ( Registros ) then
                begin
                     LogError(0 , Format( '|Empresa %s|', [ sEmpresa ] ) );
                     sNombreCompany := cdsCompany.FieldByName('CM_NOMBRE').AsString;
                     if Assigned( Registros ) then
                     begin
                          Registro := GetNode( 'REGISTRO', Registros );
                          DefineEstructuraCreditos;
                          while Assigned( Registro ) do
                          begin
                               CargaParametros( Registro, iEmpNumero, dFechaInicio, dFechaOtorgamiento, sNoCredito, iTPrestamo, dDescuento );
                               lRegistro := True;
                               if SetEmpleadoNumero( cdsEmpleado, iEmpNumero ) then
                               begin
                                    ValidacionInfonavit := False;
                                    AgregarDatosDatasetInfonavit( iEmpNumero, dFechaInicio, dFechaOtorgamiento, sNoCredito, iTPrestamo, dDescuento );
                                    EnviarDatosDatasetInfonavit;
                                    if( ( ValidacionInfonavit = False ) and ( lOkProcess = False ) ) then  lOkProcess := True; //Almenos un registro es valido para se procesado en el servidor
                                    if( ( ValidacionInfonavit ) and ( lOkProcessAux = False ) ) then  lOkProcessAux := True; //Almenos un error en el registro
                               end
                               else
                               begin
                                    LogError(0, Format( 'Empleado #%d no existe en la empresa %s:%s', [ iEmpNumero, sEmpresa, sNombreCompany ] ) );
                                    lOk := true;
                               end;
                               Registro := GetNextSibling( Registro );
                          end;
                          if ( lRegistro ) then
                          begin
                               if ( lOkProcess = True ) then
                               begin
                                    ImportarCreditosInfonavit( lOkProcess, Texto );
                                    if ( ( lOkProcess = False ) and ( lOk = False ) ) then lOk := True;
                                    if( lOk ) then LogError(0, Texto.Text );
                               end
                               else
                               begin
                                    lOk := True;
                               end;
                          end
                          else
                          begin
                               LogError( 0, 'Error agregar cr�dito INFONAVIT: Nodo <REGISTRO> no especificado' );
                               lOk := True;
                          end;
                     end;
                end
                else
                begin
                     lOk := true;
                     LogError( 0, 'Error agregar cr�dito INFONAVIT: Nodo <REGISTROS> no especificado' );
                end;
           end;
        except
              on Error: Exception do
              begin
                   lOk := true;
                   LogError( 0, 'Error agregar cr�dito INFONAVIT: ' + Error.Message  );
              end;
        end;
        if( ( lOk = False ) and ( lOkProcessAux = False ) ) then FErrores.Clear;
        Result := lOk;
     finally
            FreeAndNil(Texto);
     end;
end;


procedure TdmCliente.ImportarCreditosInfonavit( var lOkProcess: Boolean; var Texto: TStringList );
var
     Parametros : TZetaParams;
begin
     Parametros := TZetaParams.Create( Self );
     try
        with Parametros do
        begin
             AddDate( 'FechaCambio', FechaDefault );
             AddInteger( 'Formato', Ord(infoInicio) );
             AddInteger( 'FormatoImpFecha',Ord( ifDDMMYYYYs));
             AddString( 'Archivo', 'Datos de WFN para Infonavit' );
             AddMemo( 'Observaciones', 'Credito capturado en WFN' );
        end;
        EscribeBitacora( ServerRecursos.ImportarInfonavitLista( dmCliente.Empresa, cdsImportInfonavit.Data, Parametros.VarValues ), Texto );
        LeerMensajeBitacora( lOkProcess, Texto );
     finally
            FreeAndNil(Parametros);
     end;
end;


//PENSKE  MODIFICACION DE CREDITO INFONAVIT
function TdmCliente.ModificarInfonavit( const iUsuario:integer) : Boolean;
const
     K_INFO_DESCRIPCION = 'Cr�dito actualizado en WFN';
     K_TIPO_MOVIMIENTO = 3;
var
   iEmpNumero: Integer;
   dFecha: TDateTime;
   sNoCredito: string;
   iTPrestamo: integer;
   dDescuento: Real;
   Registros, Registro: TZetaXMLNode;
   Texto : TStringList;
   lOk, lOkProcess, lOkProcessAux, lRegistro, lInfocambiotd, lInfocambiovd, lInfocambionocre: Boolean;
   sEmpresa,sNombreCompany : string;
   iTipoMovimiento,iTipoMovimientoAux : Integer;

   procedure AgregaColumna( const sCampo: String; const eTipo: eTipoGlobal; const iLongitud: Integer; var oSDataSet:TServerDataSet );
   begin
        //Agrega Campo al DataSet
        with oSDataSet do
        begin
             case eTipo of
                  tgBooleano : AddBooleanField( sCampo );
                  tgFloat : AddFloatField( sCampo );
                  tgNumero : AddIntegerField( sCampo );
                  tgFecha : AddDateField( sCampo );
             else
                  AddStringField( sCampo, iLongitud );
             end;
        end;
    end;

   procedure LimpiaEstructuraCreditos;
   begin
        cdsImportInfonavit.Close;
        cdsImportInfonavit.Fields.Clear;
        cdsImportInfonavit.FieldDefs.Clear;
   end;

   procedure DefineEstructuraCreditos;
   begin
        LimpiaEstructuraCreditos;
        with cdsImportInfonavit do
        begin
             AgregaColumna( 'CB_CODIGO', tgNumero, 9, cdsImportInfonavit );
             AgregaColumna( 'TIPO_MOVIMIENTO', tgNumero, K_ANCHO_CODIGO1, cdsImportInfonavit );
             AgregaColumna( 'FECHA', tgFecha, K_ANCHO_FECHA, cdsImportInfonavit );
             AgregaColumna( 'CB_INFCRED', tgTexto, 10, cdsImportInfonavit );
             AgregaColumna( 'CB_INFTIPO', tgNumero, K_ANCHO_CODIGO1, cdsImportInfonavit );
             AgregaColumna( 'CB_INFTASA', tgFloat, K_ANCHO_PESOS, cdsImportInfonavit );
             AgregaColumna( 'CB_INFDISM', tgTexto, 2, cdsImportInfonavit );
             AgregaColumna( 'CB_INF_ANT', tgFecha, K_ANCHO_FECHA, cdsImportInfonavit );
             AgregaColumna( 'DESCRIPCION', tgTexto, K_ANCHO_OBSERVACIONES, cdsImportInfonavit );
             AgregaColumna( 'KI_FECHA', tgFecha, K_ANCHO_FECHA, cdsImportInfonavit );
             AgregaColumna( 'KI_TIPO', tgNumero, K_ANCHO_CODIGO1, cdsImportInfonavit );

             if not Active then
             begin
                  CreateDataSet;
                  Open;
             end;
             EmptyDataSet;
        end;
   end;

   procedure CargaParametros( Registro: TZetaXMLNode; var iEmpNumero: Integer; var dFecha: TDateTime; var sNoCredito: string; var iTPrestamo: integer; var dDescuento: Real );
   begin
        with FXMLTools do
         begin
              while Assigned( Registro ) do
              begin
                   iEmpNumero := AttributeAsInteger(Registro, 'EmpNumero',  K_EMPLEADO_NULO );
                   dFecha := AttributeAsDateTime( Registro, 'FECHA', 0 );
                   sNoCredito := AttributeAsString( Registro, 'CB_INFCRED', VACIO );
                   iTPrestamo := AttributeAsInteger( Registro, 'CB_INFTIPO', 0 );
                   dDescuento := AttributeAsFloat( Registro, 'CB_INFTASA', 0 );
                   break;
              end;
         end;
   end;

   procedure AgregarDatosDatasetInfonavit( var iEmpNumero: Integer; var dFecha: TDateTime; var sNoCredito: string; var iTPrestamo: integer; var dDescuento: Real; iTipoMovimiento, iTipoMovimientoAux : Integer );
   begin
        with cdsImportInfonavit do
        begin
             Append;
             cdsImportInfonavit.FieldByName('CB_CODIGO').AsInteger   := iEmpNumero;
             cdsImportInfonavit.FieldByName('DESCRIPCION').AsString  := K_INFO_DESCRIPCION;
             cdsImportInfonavit.FieldByName('CB_INFDISM').AsString   := K_GLOBAL_NO;
             cdsImportInfonavit.FieldByName('CB_INFCRED').AsString   := sNoCredito;
             cdsImportInfonavit.FieldByName('CB_INFTASA').AsFloat    := dDescuento;
             cdsImportInfonavit.FieldByName('CB_INFTIPO').AsInteger  := iTPrestamo;
             cdsImportInfonavit.FieldByName('TIPO_MOVIMIENTO').AsInteger:= iTipoMovimiento;
             cdsImportInfonavit.FieldByName('FECHA').AsDateTime:= dFecha;
             cdsImportInfonavit.FieldByName('KI_FECHA').AsDateTime := dFecha;
             cdsImportInfonavit.FieldByName('KI_TIPO').AsInteger  := iTipoMovimientoAux;
        end;
   end;

   procedure EnviarDatosDatasetInfonavit;
   begin
        with cdsImportInfonavit do
        begin
             Post;
             if ( ValidacionInfonavit ) then Delete;
        end;
   end;

   procedure TipoCambioProcesar( var lInfocambiotd, lInfocambiovd, lInfocambionocre:  Boolean; iTPrestamo: Integer; dDescuento: Real; sNoCredito: String; var iTipoMovimiento, iTipoMovimientoAux: Integer );
   begin
        with cdsEmpleado do
        begin
             if not ( FieldByName('CB_INFTIPO').AsInteger = iTPrestamo ) then
             begin
                  lInfocambiotd := True;
             end;
             if not ( FieldByName('CB_INFTASA').AsFloat = dDescuento ) then
             begin
                   lInfocambiovd := True;
             end;
             if not ( FieldByName('CB_INFCRED').AsString = sNoCredito ) then
             begin
                  lInfocambionocre := True;
             end;
             if ( lInfocambionocre ) then //Modificacion de Numero de Credito
             begin
                  iTipoMovimiento := 0;
                  iTipoMovimientoAux := 5;
             end
             else if ( ( lInfoCambiotd ) ) then //Modificacion de Tipo de Descuento
             begin
                  iTipoMovimiento := 2;
                  iTipoMovimientoAux := 3;
             end
             else if ( lInfocambiovd ) then //Modificacion de Valor de Descuento
             begin
                  iTipoMovimiento := 3;
                  iTipoMovimientoAux := 4;
             end;
        end;
   end;

begin
     Texto := TStringList.Create;
     lOk := False;
     lRegistro := False;
     lOkProcessAux := False;
     lOkProcess := False;
     lInfocambiotd := False;
     lInfocambiovd := False;
     lInfocambionocre := False;
     iTipoMovimiento := 2;
     iTipoMovimientoAux := 3;
     try
        try
           with FXMLTools do
           begin
                Registros := GetNode( 'REGISTROS' );
                sEmpresa := TagAsString( GetNode( 'EMPRESA' ), '' );
                if Assigned ( Registros ) then
                begin
                     LogError(0 , Format( '|Empresa %s|', [ sEmpresa ] ) );
                     sNombreCompany := cdsCompany.FieldByName('CM_NOMBRE').AsString;
                     if Assigned( Registros ) then
                     begin
                          Registro := GetNode( 'REGISTRO', Registros );
                          DefineEstructuraCreditos;
                          while Assigned( Registro ) do
                          begin
                               CargaParametros( Registro, iEmpNumero, dFecha, sNoCredito, iTPrestamo, dDescuento );
                               lRegistro := True;
                               if SetEmpleadoNumero( cdsEmpleado, iEmpNumero ) then
                               begin
                                    ValidacionInfonavit := False;
                                    //Verificar el Tipo de Cambio a procesar
                                    TipoCambioProcesar( lInfocambiotd, lInfocambiovd, lInfocambionocre, iTPrestamo, dDescuento, sNoCredito, iTipoMovimiento, iTipoMovimientoAux );
                                    //Registar Modificacion Dependiendo del Tipo de camvio
                                    AgregarDatosDatasetInfonavit( iEmpNumero, dFecha, sNoCredito, iTPrestamo, dDescuento, iTipoMovimiento, iTipoMovimientoAux);
                                    EnviarDatosDatasetInfonavit;
                                    if( ( ValidacionInfonavit = False ) and ( lOkProcess = False ) ) then  lOkProcess := True; //Almenos un registro es valido para se procesado en el servidor
                                    if( ( ValidacionInfonavit ) and ( lOkProcessAux = False ) ) then  lOkProcessAux := True; //Almenos un error en el registro
                               end
                               else
                               begin
                                    LogError(0, Format( 'Empleado #%d no existe en la empresa %s:%s', [ iEmpNumero, sEmpresa, sNombreCompany ] ) );
                                    lOk := true;
                               end;
                               Registro := GetNextSibling( Registro );
                          end;
                          if ( lRegistro ) then
                          begin
                               if ( lOkProcess = True ) then
                               begin
                                    ActualizarInfonavit( lOkProcess, Texto );
                                    if ( ( lOkProcess = False ) and ( lOk = False ) ) then lOk := True;
                                    if( lOk ) then LogError(0, Texto.Text );
                               end
                               else
                               begin
                                    lOk := True;
                               end;
                          end
                          else
                          begin
                               LogError( 0, 'Error al actualizar cr�dito INFONAVIT: Nodo <REGISTRO> no especificado' );
                               lOk := True;
                          end;
                     end;
                end
                else
                begin
                     lOk := true;
                     LogError( 0, 'Error actualizar cr�dito INFONAVIT: Nodo <REGISTROS> no especificado' );
                end;
           end;
        except
              on Error: Exception do
              begin
                   lOk := true;
                   LogError( 0, 'Error actualizar cr�dito INFONAVIT: ' + Error.Message );
              end;
        end;
        if( ( lOk = False ) and ( lOkProcessAux = False ) ) then FErrores.Clear;
        Result := lOk;
     finally
            FreeAndNil(Texto);
     end;
end;

procedure TdmCliente.ActualizarInfonavit( var lOkProcess: Boolean; var Texto: TStringList );
var
     Parametros : TZetaParams;
begin
     Parametros := TZetaParams.Create( Self );
     try
        with Parametros do
        begin
             AddDate( 'FechaCambio', FechaDefault );
             AddInteger( 'Formato', Ord(faASCIIDel) );
             AddInteger( 'FormatoImpFecha',Ord( ifDDMMYYYYs));
             AddString( 'Archivo', 'Datos de WFN para Infonavit' );
             AddMemo( 'Observaciones', 'Cr�dito actualizado en WFN' );
        end;
        EscribeBitacora( ServerRecursos.ImportarInfonavitLista( dmCliente.Empresa, cdsImportInfonavit.Data, Parametros.VarValues ) , Texto);
        LeerMensajeBitacora( lOkProcess, Texto );
     finally
            FreeAndNil(Parametros);
     end;
end;

//PENSKE  SUSPENDER CREDITO INFONAVIT
function TdmCliente.SuspenderInfonavitWFN( const iUsuario:integer ) : Boolean;
const
     K_INFO_DESCRIPCION = 'Cr�dito suspendido en WFN';
     K_TIPO_MOVIMIENTO = 5;
var
   iEmpNumero: Integer;
   dFecha: TDateTime;
   sNoCredito: string;
   Registros, Registro: TZetaXMLNode;
   Texto : TStringList;
   lOk, lOkProcess, lOkProcessAux, lRegistro: Boolean;
   sEmpresa,sNombreCompany : string;

   procedure AgregaColumna( const sCampo: String; const eTipo: eTipoGlobal; const iLongitud: Integer; var oSDataSet:TServerDataSet );
   begin
        //Agrega Campo al DataSet
        with oSDataSet do
        begin
             case eTipo of
                  tgBooleano : AddBooleanField( sCampo );
                  tgFloat : AddFloatField( sCampo );
                  tgNumero : AddIntegerField( sCampo );
                  tgFecha : AddDateField( sCampo );
             else
                  AddStringField( sCampo, iLongitud );
             end;
        end;
    end;

   procedure LimpiaEstructuraCreditos;
   begin
        cdsImportInfonavit.Close;
        cdsImportInfonavit.Fields.Clear;
        cdsImportInfonavit.FieldDefs.Clear;
   end;

   procedure DefineEstructuraCreditos;
   begin
        LimpiaEstructuraCreditos;
        with cdsImportInfonavit do
        begin
             AgregaColumna( 'CB_CODIGO', tgNumero, 9, cdsImportInfonavit );
             AgregaColumna( 'TIPO_MOVIMIENTO', tgNumero, K_ANCHO_CODIGO1, cdsImportInfonavit );
             AgregaColumna( 'FECHA', tgFecha, K_ANCHO_FECHA, cdsImportInfonavit );
             AgregaColumna( 'CB_INFCRED', tgTexto, 10, cdsImportInfonavit );
             AgregaColumna( 'CB_INFTIPO', tgNumero, K_ANCHO_CODIGO1, cdsImportInfonavit );
             AgregaColumna( 'CB_INFTASA', tgFloat, K_ANCHO_PESOS, cdsImportInfonavit );
             AgregaColumna( 'CB_INFDISM', tgTexto, 2, cdsImportInfonavit );
             AgregaColumna( 'CB_INF_ANT', tgFecha, K_ANCHO_FECHA, cdsImportInfonavit );
             AgregaColumna( 'DESCRIPCION', tgTexto, K_ANCHO_OBSERVACIONES, cdsImportInfonavit );
             AgregaColumna( 'KI_FECHA', tgFecha, K_ANCHO_FECHA, cdsImportInfonavit );
             AgregaColumna( 'KI_TIPO', tgNumero, K_ANCHO_CODIGO1, cdsImportInfonavit );
             if not Active then
             begin
                  CreateDataSet;
                  Open;
             end;
             EmptyDataSet;
        end;
   end;

   procedure CargaParametros( Registro: TZetaXMLNode; var iEmpNumero: Integer; var dFecha: TDateTime; var sNoCredito: string );
   begin
        with FXMLTools do
         begin
              while Assigned( Registro ) do
              begin
                   iEmpNumero := AttributeAsInteger(Registro, 'EmpNumero',  K_EMPLEADO_NULO );
                   dFecha := AttributeAsDateTime( Registro, 'FECHA', 0 );
                   sNoCredito := AttributeAsString( Registro, 'CB_INFCRED', VACIO );
                   break;
              end;
         end;
   end;

   procedure AgregarDatosDatasetInfonavit( var iEmpNumero: Integer; var dFecha: TDateTime; var sNoCredito: string );
   begin
        with cdsImportInfonavit do
        begin
             Append;
             cdsImportInfonavit.FieldByName('CB_CODIGO').AsInteger   := iEmpNumero;
             cdsImportInfonavit.FieldByName('DESCRIPCION').AsString  := K_INFO_DESCRIPCION;
             cdsImportInfonavit.FieldByName('CB_INFCRED').AsString   := sNoCredito;
             cdsImportInfonavit.FieldByName('TIPO_MOVIMIENTO').AsInteger:= K_TIPO_MOVIMIENTO;
             cdsImportInfonavit.FieldByName('FECHA').AsDateTime:= dFecha;
             cdsImportInfonavit.FieldByName('KI_FECHA').AsDateTime := dFecha;
             //Valor Default Validacion del DataSet
             cdsImportInfonavit.FieldByName('KI_TIPO').AsInteger  := 1;
             cdsImportInfonavit.FieldByName('CB_INFTIPO').AsInteger  := 1;
             cdsImportInfonavit.FieldByName('CB_INFTASA').AsInteger  := 1;
        end;
   end;

   procedure EnviarDatosDatasetInfonavit;
   begin
        with cdsImportInfonavit do
        begin
             Post;
             if ( ValidacionInfonavit ) then Delete;
        end;
   end;

begin
     Texto := TStringList.Create;
     lOk := False;
     lRegistro := False;
     lOkProcess := False;
     lOkProcessAux := False;
     try
        try
           with FXMLTools do
           begin
                Registros := GetNode( 'REGISTROS' );
                sEmpresa := TagAsString( GetNode( 'EMPRESA' ), '' );
                if Assigned ( Registros ) then
                begin
                     LogError(0 , Format( '|Empresa %s|', [ sEmpresa ] ) );
                     sNombreCompany := cdsCompany.FieldByName('CM_NOMBRE').AsString;
                     if Assigned( Registros ) then
                     begin
                          Registro := GetNode( 'REGISTRO', Registros );
                          DefineEstructuraCreditos;
                          lRegistro := True;
                          while Assigned( Registro ) do
                          begin
                               CargaParametros( Registro, iEmpNumero, dFecha, sNoCredito );
                               lRegistro := True;
                               if SetEmpleadoNumero( cdsEmpleado, iEmpNumero ) then
                               begin
                                    ValidacionInfonavit := False;
                                    AgregarDatosDatasetInfonavit( iEmpNumero, dFecha, sNoCredito );
                                    EnviarDatosDatasetInfonavit;
                                    if( ( ValidacionInfonavit = False ) and ( lOkProcess = False ) ) then  lOkProcess := True; //Almenos un registro es valido para se procesado en el servidor
                                    if( ( ValidacionInfonavit ) and ( lOkProcessAux = False ) ) then  lOkProcessAux := True; //Almenos un error en el registro
                               end
                               else
                               begin
                                    LogError(0 , Format( 'Empleado #%d no existe en la empresa %s:%s', [ iEmpNumero, sEmpresa, sNombreCompany ] ) );
                                    lOk := true;
                               end;
                               Registro := GetNextSibling( Registro );
                          end;
                          if ( lRegistro ) then
                          begin
                               if ( lOkProcess = True ) then
                               begin
                                    SuspenderInfonavit( lOkProcess, Texto );
                                    if ( ( lOkProcess = False ) and ( lOk = False ) ) then lOk := True;
                                    if( lOk ) then LogError(0, Texto.Text );
                               end
                               else
                               begin
                                    lOk := True;
                               end;
                          end
                          else
                          begin
                               LogError(0 , 'Error al suspender cr�dito INFONAVIT: Nodo <REGISTRO> no especificado' );
                               lOk := True;
                          end;
                     end;
                end
                else
                begin
                     lOk := true;
                     LogError(0 , 'Error suspender cr�dito INFONAVIT: Nodo <REGISTROS> no especificado' );
                end;
           end;
        except
              on Error: Exception do
              begin
                   lOk := true;
                   LogError(0 , 'Error suspender cr�dito INFONAVIT: ' + Error.Message );
              end;
        end;
        if( ( lOk = False ) and ( lOkProcessAux = False ) ) then FErrores.Clear;
        Result := lOk;
     finally
            FreeAndNil(Texto);
     end;
end;

procedure TdmCliente.SuspenderInfonavit( var lOkProcess: Boolean; var Texto: TStringList );
var
     Parametros : TZetaParams;
begin
     Parametros := TZetaParams.Create( Self );
     try
        with Parametros do
        begin
             AddDate( 'FechaCambio', FechaDefault );
             AddInteger( 'Formato', Ord(faASCIIDel) );
             AddInteger( 'FormatoImpFecha',Ord(ifDDMMYYYYs) );
             AddString( 'Archivo', 'Datos de WFN para Infonavit' );
             AddMemo( 'Observaciones', 'Cr�dito suspendido en WFN' );
        end;
        EscribeBitacora( ServerRecursos.ImportarInfonavitLista( dmCliente.Empresa, cdsImportInfonavit.Data, Parametros.VarValues ), Texto);
        LeerMensajeBitacora( lOkProcess, Texto );
     finally
            FreeAndNil(Parametros);
     end;
end;


//PENSKE  REINICIAR CREDITO INFONAVIT
function TdmCliente.ReiniciarInfonavitWFN( const iUsuario:integer) : Boolean;
const
     K_INFO_DESCRIPCION = 'Cr�dito reiniciado en WFN';
     K_TIPO_MOVIMIENTO = 6;
var
   iEmpNumero: Integer;
   dFecha: TDateTime;
   sNoCredito: string;
   iTPrestamo: integer;
   dDescuento: Real;
   Registros, Registro: TZetaXMLNode;
   Texto : TStringList;
   lOk, lOkProcess, lOkProcessAux, lRegistro: Boolean;
   sEmpresa,sNombreCompany : string;

   procedure AgregaColumna( const sCampo: String; const eTipo: eTipoGlobal; const iLongitud: Integer; var oSDataSet:TServerDataSet );
   begin
        //Agrega Campo al DataSet
        with oSDataSet do
        begin
             case eTipo of
                  tgBooleano : AddBooleanField( sCampo );
                  tgFloat : AddFloatField( sCampo );
                  tgNumero : AddIntegerField( sCampo );
                  tgFecha : AddDateField( sCampo );
             else
                  AddStringField( sCampo, iLongitud );
             end;
        end;
    end;

   procedure LimpiaEstructuraCreditos;
   begin
        cdsImportInfonavit.Close;
        cdsImportInfonavit.Fields.Clear;
        cdsImportInfonavit.FieldDefs.Clear;
   end;

   procedure DefineEstructuraCreditos;
   begin
        LimpiaEstructuraCreditos;
        with cdsImportInfonavit do
        begin
             AgregaColumna( 'CB_CODIGO', tgNumero, 9, cdsImportInfonavit );
             AgregaColumna( 'TIPO_MOVIMIENTO', tgNumero, K_ANCHO_CODIGO1, cdsImportInfonavit );
             AgregaColumna( 'FECHA', tgFecha, K_ANCHO_FECHA, cdsImportInfonavit );
             AgregaColumna( 'CB_INFCRED', tgTexto, 10, cdsImportInfonavit );
             AgregaColumna( 'CB_INFTIPO', tgNumero, K_ANCHO_CODIGO1, cdsImportInfonavit );
             AgregaColumna( 'CB_INFTASA', tgFloat, K_ANCHO_PESOS, cdsImportInfonavit );
             AgregaColumna( 'CB_INFDISM', tgTexto, 2, cdsImportInfonavit );
             AgregaColumna( 'CB_INF_ANT', tgFecha, K_ANCHO_FECHA, cdsImportInfonavit );
             AgregaColumna( 'DESCRIPCION', tgTexto, K_ANCHO_OBSERVACIONES, cdsImportInfonavit );
             AgregaColumna( 'CB_INF_INI', tgFecha, K_ANCHO_FECHA, cdsImportInfonavit );
             AgregaColumna( 'KI_FECHA', tgFecha, K_ANCHO_FECHA, cdsImportInfonavit );
             AgregaColumna( 'KI_TIPO', tgNumero, K_ANCHO_CODIGO1, cdsImportInfonavit );
             if not Active then
             begin
                  CreateDataSet;
                  Open;
             end;
             EmptyDataSet;
        end;
   end;

   procedure CargaParametros( Registro: TZetaXMLNode; var iEmpNumero: Integer; var dFecha: TDateTime; var sNoCredito: string; var iTPrestamo: integer; var dDescuento: Real );
   begin
        with FXMLTools do
         begin
              while Assigned( Registro ) do
              begin
                   iEmpNumero := AttributeAsInteger(Registro, 'EmpNumero',  K_EMPLEADO_NULO );
                   dFecha := AttributeAsDateTime( Registro, 'FECHA', 0 );
                   sNoCredito := AttributeAsString( Registro, 'CB_INFCRED', VACIO );
                   iTPrestamo := AttributeAsInteger( Registro, 'CB_INFTIPO', 0 );
                   dDescuento := AttributeAsFloat( Registro, 'CB_INFTASA', 0 );
                   break;
              end;
         end;
   end;

   procedure AgregarDatosDatasetInfonavit( var iEmpNumero: Integer; var dFecha: TDateTime; var sNoCredito: string; var iTPrestamo: integer; var dDescuento: Real );
   begin
        with cdsImportInfonavit do
        begin
             Append;
             cdsImportInfonavit.FieldByName('CB_CODIGO').AsInteger   := iEmpNumero;
             cdsImportInfonavit.FieldByName('DESCRIPCION').AsString  := K_INFO_DESCRIPCION;
             cdsImportInfonavit.FieldByName('CB_INFCRED').AsString   := sNoCredito;
             cdsImportInfonavit.FieldByName('CB_INFTASA').AsFloat    := dDescuento;
             cdsImportInfonavit.FieldByName('CB_INFTIPO').AsInteger  := iTPrestamo;
             cdsImportInfonavit.FieldByName('TIPO_MOVIMIENTO').AsInteger:= K_TIPO_MOVIMIENTO;
             cdsImportInfonavit.FieldByName('FECHA').AsDateTime:= dFecha;
             cdsImportInfonavit.FieldByName('KI_FECHA').AsDateTime := dFecha;
             cdsImportInfonavit.FieldByName('KI_TIPO').AsInteger  := 2;
        end;
   end;

   procedure EnviarDatosDatasetInfonavit;
   begin
        with cdsImportInfonavit do
        begin
             Post;
             if ( ValidacionInfonavit ) then Delete;
        end;
   end;

begin
     Texto := TStringList.Create;
     lOk := False;
     lRegistro := False;
     lOkProcessAux := False;
     lOkProcess := False;
     try
        try
           with FXMLTools do
           begin
                Registros := GetNode( 'REGISTROS' );
                sEmpresa := TagAsString( GetNode( 'EMPRESA' ), '' );
                if Assigned ( Registros ) then
                begin
                     LogError( 0, Format( '|Empresa %s|', [ sEmpresa ] ) );
                     sNombreCompany := cdsCompany.FieldByName('CM_NOMBRE').AsString;
                     if Assigned( Registros ) then
                     begin
                          Registro := GetNode( 'REGISTRO', Registros );
                          DefineEstructuraCreditos;
                          while Assigned( Registro ) do
                          begin
                               CargaParametros( Registro, iEmpNumero, dFecha, sNoCredito, iTPrestamo, dDescuento );
                               lRegistro := True;
                               if SetEmpleadoNumero( cdsEmpleado, iEmpNumero ) then
                               begin
                                    ValidacionInfonavit := False;
                                    AgregarDatosDatasetInfonavit( iEmpNumero, dFecha, sNoCredito, iTPrestamo, dDescuento );
                                    EnviarDatosDatasetInfonavit;
                                    if( ( ValidacionInfonavit = False ) and ( lOkProcess = False ) ) then  lOkProcess := True; //Almenos un registro es valido para se procesado en el servidor
                                    if( ( ValidacionInfonavit ) and ( lOkProcessAux = False ) ) then  lOkProcessAux := True; //Almenos un error en el registro
                               end
                               else
                               begin
                                    LogError( 0, Format( 'Empleado #%d no existe en la empresa %s:%s', [ iEmpNumero, sEmpresa, sNombreCompany ] ) );
                                    lOk := true;
                               end;
                               Registro := GetNextSibling( Registro );
                          end;
                          if ( lRegistro ) then
                          begin
                               if ( lOkProcess = True ) then
                               begin
                                    ActualizarInfonavit( lOkProcess, Texto );
                                    if ( ( lOkProcess = False ) and ( lOk = False ) ) then lOk := True;
                                    if( lOk ) then LogError(0, Texto.Text );
                               end
                               else
                               begin
                                    lOk := True;
                               end;
                          end
                          else
                          begin
                               LogError( 0, 'Error al reiniciar cr�dito INFONAVIT: Nodo <REGISTRO> no especificado' );
                               lOk := True;
                          end;
                     end;
                end
                else
                begin
                     lOk := true;
                     LogError( 0, 'Error reiniciar cr�dito INFONAVIT: Nodo <REGISTROS> no especificado' );
                end;
           end;
        except
              on Error: Exception do
              begin
                   lOk := true;
                   LogError( 0, 'Error reiniciar cr�dito INFONAVIT: ' + Error.Message );
              end;
        end;
        if( ( lOk = False ) and ( lOkProcessAux = False ) ) then FErrores.Clear;
        Result := lOk;
     finally
            FreeAndNil(Texto);
     end;
end;

procedure TdmCliente.ReiniciarInfonavit( var lOkProcess: Boolean; var Texto: TStringList );
var
     Parametros : TZetaParams;
begin
     Parametros := TZetaParams.Create( Self );
     try
        with Parametros do
        begin
             AddDate( 'FechaCambio', FechaDefault );
             AddInteger( 'Formato', Ord(faASCIIDel) );
             AddInteger( 'FormatoImpFecha',Ord( ifDDMMYYYYs));
             AddString( 'Archivo', 'Datos de WFN para Infonavit' );
             AddMemo( 'Observaciones', 'Cr�dito reinicio en WFN' );
        end;
        EscribeBitacora( ServerRecursos.ImportarInfonavitLista( dmCliente.Empresa, cdsImportInfonavit.Data, Parametros.VarValues ), Texto );
        LeerMensajeBitacora( lOkProcess, Texto );
     finally
            FreeAndNil(Parametros);
     end;
end;

//FIN

//PENSKE Agregar Incapacidad
function TdmCliente.AgregarIncapacidadWFN( const iUsuario:integer ) : Boolean;
var
   lOk : Boolean;
   sLog: string;
begin
     // True Todo Bien False Errores
     lOk := False;
     try
        dmIncapacidad := TdmIncapacidad.Create(nil);
        try
           with FXMLTools do
           begin
                XMLDocument.DOMDocument;
                lOk := dmIncapacidad.TressGrabar( XMLWFN, Usuario, FFolio, sLog );
                if ( lOK ) then
                begin
                     logError( 0, sLog );
                     lOk := False;
                end
                else
                    lOk := True;
           end;
        except
              on Error: Exception do
              begin
                   LogError( 0, 'Error al agregar incapacidad: ' + Error.Message );
              end;
        end;
        Result := lOk;
     finally
            FreeandNil( dmIncapacidad );
     end;
end;
///FIN

function TdmCliente.AgregarPrestamoWFN( const iUsuario:integer ) : Boolean;
var
   lOk : Boolean;
   sLog: string;
begin
     lOk := False; 
     try
        dmPrestamos := TdmPrestamos.Create(nil);
        try
           with FXMLTools do
           begin
                XMLDocument.DOMDocument;
                lOk := dmPrestamos.TressGrabar( XMLWFN, Usuario, FFolio, sLog );
                if ( lOK ) then
                begin
                     logError( 0, sLog );
                     lOk := False;
                end
                else
                    lOk := True;
           end;
        except
              on Error: Exception do
              begin
                   LogError( 0, 'Error al agregar Prestamo: ' + Error.Message );
              end;
        end;
        Result := lOk;
     finally
            FreeandNil( dmPrestamos );
     end;
end;


function TdmCliente.AgregarCambioEquipoWFN( const iUsuario:integer ) : Boolean;
const
     K_GLOBAL_NIVEL_SUPERVISOR = 120; // De aqu� no se tiene acceso a ZGlobalTress de TRESS, se redeclara la constante.
     K_CONSULTA = 'SELECT NIVEL_SUPERVISOR = GL_FORMULA FROM GLOBAL WHERE GL_CODIGO = ''%d'' ';
var
    i, j, iEmpleado : Integer;
    ErrorCount : Integer;
    FOtrosDatos : Variant;
    lOk, lOkAux, lRegistro: Boolean;
    sEmpresa, sOperacion: string;
    oParametros: TZetaParams;

    sGlobalNivelSupervisor : String;
    sNivel: String;

    oConsultaGlobalNivelSupervisor: TClientDataSet;

    Registros, Registro: TZetaXMLNode;

    lValido, lBandera: Boolean;

    procedure CargarEmpleadosCE;
    const
         NODO_EMPLEADO = 'EMPLEADO';
         NODO_EQUIPO = 'CB_EQUIPO';
         NODO_FECHA_REG = 'CB_FECHAREG';
         NODO_DESCRIPCION = 'DESCRIPCION';
         NODO_OBSERVA = 'OBSERVACIONES';

         NODO_USUARIOWFN = 'PersonaID';

         NODO_EMPLEADOS_DATOS = 'INFO';
         NODO_EMPLEADOS_LISTA = 'EMPLEADOS';
         CB_NIVEL = 'CB_NIVEL';

    var
       Nodo, Info: TZetaXMLNode;
    begin
         FEmpleadosCE := TEmpleadosCambioEquipo.Create;
         with FXMLTools do
         begin

              Nodo := GetNode( NODO_EMPLEADOS_LISTA );
              { Preguntar si son varios empleados }
              if Assigned( Nodo ) then
              begin
                   Info := GetNode( NODO_EMPLEADOS_DATOS, Nodo );
                   while Assigned( Info ) and HasChildren( Info ) do
                   begin
                        FEmpleadosCE.AddEmpleadoCambioEquipo( TagAsInteger( GetNode( NODO_EMPLEADO, Info ), K_EMPLEADO_NULO ), TagAsString( GetNode( NODO_EQUIPO, Info ), VACIO ), TagAsDateTime( GetNode( NODO_FECHA_REG, Info), NullDateTime ), TagAsString( GetNode( NODO_DESCRIPCION, Info),VACIO), TagAsString( GetNode( NODO_OBSERVA, Info),VACIO), TagAsInteger( GetNode( NODO_USUARIOWFN, Info ), K_EMPLEADO_NULO )   );
                        Info := GetNextSibling( Info );
                   end
              end
              else
              begin
                      FEmpleadosCE.AddEmpleadoCambioEquipo( TagAsInteger( GetNode( NODO_EMPLEADO, Info ), K_EMPLEADO_NULO ), TagAsString( GetNode( NODO_EQUIPO, Info ), VACIO ), TagAsDateTime( GetNode( NODO_FECHA_REG, Info), NullDateTime ), TagAsString( GetNode( NODO_DESCRIPCION, Info),VACIO), TagAsString( GetNode( NODO_OBSERVA, Info),VACIO), TagAsInteger( GetNode( NODO_USUARIOWFN, Info ), K_EMPLEADO_NULO )  );
              end;
         end;
         if ( FEmpleadosCE.slEmpleados.Count = 0 ) then
            raise Exception.Create( 'No se especific� un empleado' );
    end;


    procedure CargarCodigoEquipo;
    begin
         lBandera := false;

        if sNivel =  'CB_NIVEL1' then
        begin
           cdsEquipoNivelSupervisor.Tag:= 18;
           lBandera := true;
        end;
        if sNivel =  'CB_NIVEL2' then
        begin
           cdsEquipoNivelSupervisor.Tag:= 19;
           lBandera := true;
        end;
        if sNivel =  'CB_NIVEL3' then
        begin
           cdsEquipoNivelSupervisor.Tag:=20;
           lBandera := true;
        end;
        if sNivel =  'CB_NIVEL4' then
        begin
           cdsEquipoNivelSupervisor.Tag:=21;
           lBandera := true;
        end;
        if sNivel =  'CB_NIVEL5' then
        begin
           cdsEquipoNivelSupervisor.Tag:=22;
           lBandera := true;
        end;
        if sNivel =  'CB_NIVEL6' then
        begin
           cdsEquipoNivelSupervisor.Tag:=23;
           lBandera := true;
        end;
        if sNivel =  'CB_NIVEL7' then
        begin
           cdsEquipoNivelSupervisor.Tag:=24;
           lBandera := true;
        end;
        if sNivel =  'CB_NIVEL8' then
        begin
           cdsEquipoNivelSupervisor.Tag:=25;
           lBandera := true;
        end;
        if sNivel =  'CB_NIVEL9' then
        begin
           cdsEquipoNivelSupervisor.Tag:=26;
           lBandera := true;
        end;
        if lBandera = false then
        begin
           cdsEquipoNivelSupervisor.Tag:=0;
        end;

    end;


begin
     oConsultaGlobalNivelSupervisor := TZetaClientDataSet.Create( self );
        // Obtener Global Nivel Supervisor
        oConsultaGlobalNivelSupervisor.Data := ServerConsultas.GetQueryGralTodos( Empresa, Format( K_CONSULTA, [ K_GLOBAL_NIVEL_SUPERVISOR ] ) );
        if( not oConsultaGlobalNivelSupervisor.Eof )then
          begin
            sGlobalNivelSupervisor := oConsultaGlobalNivelSupervisor.FieldByName( 'NIVEL_SUPERVISOR' ).AsString;
            sNivel := 'CB_NIVEL'+sGlobalNivelSupervisor;
            if ( not StrVacio( sNivel ) )  then
            begin
              CargarCodigoEquipo;
            end;
          end
        else
          begin
            sGlobalNivelSupervisor := '0';
          end;


     lOk := False;
     CargarEmpleadosCE;
     for i := 0 to ( FEmpleadosCE.slEmpleados.Count - 1 ) do
     begin
          iEmpleado := FEmpleadosCE.Empleado[ i ];
          lValido := True;
          try
             if FetchEmployee( cdsLista, iEmpleado ) then
             begin

                       if ( StrVacio( FEmpleadosCE.GetDescripcion(i) ) ) and ( lValido ) then
                       begin
                            LogError( iEmpleado, Format('|Empleado %d|La descripci�n del Kardex no puede quedar vac�o', [ iEmpleado, FEmpleadosCE.GetDescripcion(i) ] ) );
                            lValido := False;
                       end;
                       if ( not StrVacio( FEmpleadosCE.GetEquipo(i) ) ) and ( lValido ) then
                       begin
                            if (not EsValidoNivelSupervisor( FEmpleadosCE.GetEquipo(i) )) then
                            begin
                                 LogError( iEmpleado, Format('|Empleado %d|No se encuentra el c�digo:%s en el cat�logo de Nivel Supervisor', [ iEmpleado, FEmpleadosCE.GetEquipo(i) ] ) );
                                 lValido := False;
                            end;
                       end
                       else
                       begin
                            LogError( iEmpleado, Format('|Empleado %d|El Equipo no puede quedar vac�o', [ iEmpleado, FEmpleadosCE.GetEquipo(i) ] ) );
                            lValido := False;
                       end;

                   if lValido then
                   begin
                       with FXMLTools do
                       begin
                            FOtrosDatos := VarArrayOf( [FEmpleadosCE.FecReg[i], FEmpleadosCE.Descripcion[i], FEmpleadosCE.Observa[i]] );
                       end;

                       with cdsLista do
                       begin
                            if not ( state in [ dsEdit, dsInsert ] ) then
                             Edit;
                             FieldByName('CB_NIVEL'+sGlobalNivelSupervisor).AsString  := FEmpleadosCE.GetEquipo(i);

                            PostData;
                            OnReconcileError := ReconcileError;
                            if VarIsNull( DeltaNull ) then
                               lOK := True
                            else
                            begin
                                 if Reconciliar( ServerRecursos.GrabaCambiosMultiples( Empresa, DeltaNull, FOtrosDatos, ErrorCount ) ) then
                                    lOk := True
                                 else
                                 begin
                                      LogError( iEmpleado, Format('|Empleado %d|%s', [ iEmpleado, Self.Status ] ) );
                                 end;
                            end;
                       end;
                   end;
             end
             else
             begin
                  DataBaseError( 'No se encontr� el empleado # ' + IntToStr( iEmpleado ) );
             end;
          except
                on Error: Exception do
                begin
                     LogError( iEmpleado, 'Error al agregar cambio de equipo: ' + Error.Message );
                end;
          end;
     end;
     Result := lOk;
     FreeAndNil( oConsultaGlobalNivelSupervisor );
     FreeAndNil(FEmpleadosCE);


end;



function TdmCliente.BajaEmpleadoWFN( const iUsuario:integer ) : Boolean;
var
   i, j, iEmpleado : Integer;
   ErrorCount : Integer;
   FOtrosDatos : Variant;
   lOk, lOkAux, lRegistro: Boolean;
   sEmpresa, sOperacion: string;
   oParametros: TZetaParams;

   dFechaSal: TDateTime;
   sPercepcionesFijas: String;
   fSalarioActual : TPesos;
   lBajaEmpleado : boolean;

   FechaKardex: TDate;
   TipoKardex: string;

   lValido: Boolean;

    Registros, Registro: TZetaXMLNode;

    procedure CargarEmpleadosBE;
    const
         NODO_EMPLEADO = 'EMPLEADO';
         NODO_MOTIVO = 'CB_MOTIVO';
         NODO_FECHA_REG = 'CB_FECHAREG';
         NODO_FECHASS = 'CB_FECHASS';
         NODO_DESCRIPCION = 'DESCRIPCION';
         NODO_OBSERVA = 'OBSERVACIONES';
         NODO_PE_YEAR = 'PE_YEAR';
         NODO_PE_TIPO = 'PE_TIPO';
         NODO_PE_NUMERO = 'PE_NUMERO';
         NODO_USUARIOWFN = 'PersonaID';

         NODO_CB_RECONTR = 'CB_RECONTR';

         NODO_EMPLEADOS_DATOS = 'INFO';
         NODO_EMPLEADOS_LISTA = 'EMPLEADOS';
    var
       Nodo, Info: TZetaXMLNode;
    begin
         FEmpleadosBaja := TBajaEmpleado.Create;
         with FXMLTools do
         begin

              Nodo := GetNode( NODO_EMPLEADOS_LISTA );
              { Preguntar si son varios empleados }
              if Assigned( Nodo ) then
              begin
                   Info := GetNode( NODO_EMPLEADOS_DATOS, Nodo );
                   while Assigned( Info ) and HasChildren( Info ) do
                   begin
                        FEmpleadosBaja.AddBajaEmpleado( TagAsInteger( GetNode( NODO_EMPLEADO, Info ), K_EMPLEADO_NULO ), TagAsString( GetNode( NODO_MOTIVO, Info ), VACIO ), TagAsDateTime( GetNode( NODO_FECHA_REG, Info), NullDateTime ),  TagAsDateTime( GetNode( NODO_FECHASS, Info), NullDateTime ),TagAsString( GetNode( NODO_DESCRIPCION, Info),VACIO),
                        TagAsString( GetNode( NODO_OBSERVA, Info),VACIO), TagAsInteger( GetNode( NODO_PE_YEAR, Info ), K_EMPLEADO_NULO ), TagAsInteger( GetNode( NODO_PE_TIPO, Info ), K_EMPLEADO_NULO ), TagAsInteger( GetNode( NODO_PE_NUMERO, Info ), K_EMPLEADO_NULO ), TagAsInteger( GetNode( NODO_USUARIOWFN, Info ), K_EMPLEADO_NULO ), TagAsString( GetNode( NODO_CB_RECONTR, Info ), K_DEFAULT_RECONTRATO ) );
                        Info := GetNextSibling( Info );
                   end
              end
              else
              begin
                        FEmpleadosBaja.AddBajaEmpleado( TagAsInteger( GetNode( NODO_EMPLEADO, Info ), K_EMPLEADO_NULO ), TagAsString( GetNode( NODO_MOTIVO, Info ), VACIO ), TagAsDateTime( GetNode( NODO_FECHA_REG, Info), NullDateTime ),  TagAsDateTime( GetNode( NODO_FECHASS, Info), NullDateTime ),TagAsString( GetNode( NODO_DESCRIPCION, Info),VACIO),
                        TagAsString( GetNode( NODO_OBSERVA, Info),VACIO), TagAsInteger( GetNode( NODO_PE_YEAR, Info ), K_EMPLEADO_NULO ), TagAsInteger( GetNode( NODO_PE_TIPO, Info ), K_EMPLEADO_NULO ), TagAsInteger( GetNode( NODO_PE_NUMERO, Info ), K_EMPLEADO_NULO ), TagAsInteger( GetNode( NODO_USUARIOWFN, Info ), K_EMPLEADO_NULO ), TagAsString( GetNode( NODO_CB_RECONTR, Info ), K_DEFAULT_RECONTRATO )  );
              end;
         end;
         if ( FEmpleadosBaja.slEmpleados.Count = 0 ) then
            raise Exception.Create( 'No se especific� un empleado' );
    end;

begin
     lOk := False;
     dFechaSal := NullDateTime;
     CargarEmpleadosBE;
     for i := 0 to ( FEmpleadosBaja.slEmpleados.Count - 1 ) do
     begin
          iEmpleado := FEmpleadosBaja.Empleado[ i ];
          lValido := True;
          try
             if FetchEmployee( cdsLista, iEmpleado ) then
             begin
                  with cdsLista do
                  begin

                       if ( not StrVacio( FEmpleadosBaja.GetMotivo(i) ) ) and ( lValido ) then
                       begin
                            if (not EsValidoMotivoBaja( FEmpleadosBaja.GetMotivo(i) )) then
                            begin
                                 LogError( iEmpleado, Format('|Empleado %d|No se encuentra el c�digo:%s en el cat�logo de Motivos de Baja', [ iEmpleado, FEmpleadosBaja.GetMotivo(i) ] ) );
                                 lValido := False;
                            end;
                       end
                       else
                       begin
                            LogError( iEmpleado, Format('|Empleado %d|El c�digo del motivo no puede quedar vac�o', [ iEmpleado, FEmpleadosBaja.GetMotivo(i) ] ) );
                            lValido := False;
                       end;

                       if ( StrVacio( FEmpleadosBaja.GetDescripcion(i) ) ) and ( lValido ) then
                       begin
                            LogError( iEmpleado, Format('|Empleado %d|La descripci�n del Kardex no puede quedar vac�o', [ iEmpleado, FEmpleadosBaja.GetDescripcion(i) ] ) );
                            lValido := False;
                       end;

                       if lValido then
                       begin
                         Data := ServerRecursos.GetEditHisKardex( Empresa, iEmpleado, 0, VACIO );  // Llenar DataSet Vacio con formato de campos de KARDEX
                         Append;
                         FechaKardex := FEmpleadosBaja.FechaReg[i];
                         FieldByName( 'CB_FECHA' ).AsDateTime :=  FechaKardex;
                         FieldByName( 'CB_COMENTA' ).AsString := FEmpleadosBaja.GetDescripcion(i);
                         FieldByName( 'CB_NOTA' ).AsString := FEmpleadosBaja.GetObserva(i);
                         FieldByName( 'CB_TIPO' ).AsString := K_T_BAJA;

                         FieldByName( 'CB_FECHA_2' ).AsDateTime := FEmpleadosBaja.GetFechaSS(i);
                         FieldByName( 'CB_CODIGO').AsInteger := iEmpleado;
                         FieldByName('CB_MOT_BAJ').AsString  := FEmpleadosBaja.GetMotivo(i);

                         FieldByName('CB_NOMYEAR').AsInteger  := FEmpleadosBaja.GetYear(i);
                         FieldByName('CB_NOMTIPO').AsInteger  := FEmpleadosBaja.GetTipo(i);
                         FieldByName('CB_NOMNUME').AsInteger  := FEmpleadosBaja.GetNumero(i);
                         FieldByName('CB_RECONTR').AsString  := FEmpleadosBaja.GetRecontratacion(i);

                         PostData;
                         begin
                              OnReconcileError := ReconcileError;
                              sPercepcionesFijas := GetPercepcionesFijas( iEmpleado, dFechaSal );
                              if Reconciliar(ServerRecursos.GrabaHisKardex ( Empresa, DeltaNull, sPercepcionesFijas, ErrorCount ) ) then
                              begin
                                   lOk := True;
                              end
                              else
                              begin
                                   LogError( iEmpleado, Format('|Empleado %d|%s', [ iEmpleado, Self.Status ] ) );
                              end;
                         end;
                       end;
                  end;
             end
             else
             begin
                  DataBaseError( 'No se encontr� el empleado # ' + IntToStr( iEmpleado ) );
             end;                  
          except
                on Error: Exception do
                begin
                     LogError( iEmpleado, 'Error al dar de baja empleado: ' + Error.Message );
                end;
          end;

     end;
     Result := lOk;
     FreeAndNil(FEmpleadosBaja);

end;

//Cambiar Puesto Empleado
function TdmCliente.CambiarPuestoWFN: Boolean;
var
   i, iEmpleado : Integer;
   ErrorCount : Integer;
   FOtrosDatos : Variant;
   lOk: Boolean;
   lBandera: Boolean;
   lValido: Boolean;
begin
     lOk := False;
     lBandera := False;
     CargarEmpleadosWFN;
     LogError( 0, Format( '|Empresa %s|', [ cdsCompany.FieldByName('CM_CODIGO').AsString ] ) );
     for i := 0 to ( FEmpleadosWFN.slEmpleados.Count - 1 ) do
     begin
          iEmpleado := FEmpleadosWFN.Empleado[ i ];
          lValido := True;
         try
             if FetchEmployee( cdsLista, iEmpleado ) then
             begin
                  with FXMLTools do
                  begin
                       FOtrosDatos := VarArrayOf( [FEmpleadosWFN.FecReg[i], FEmpleadosWFN.Descripcion[i], FEmpleadosWFN.Observa[i]] );
                  end;
                  // Enviar los cambios
                  with cdsLista do
                  begin
                       if not ( state in [ dsEdit, dsInsert ] ) then
                        Edit;
                       if not StrVacio( FEmpleadosWFN.GetClasifi(i) ) then //Validar
                       begin
                            if EsValidoClasificacion( FEmpleadosWFN.GetClasifi(i) ) then
                                FieldByName('CB_CLASIFI').AsString  := FEmpleadosWFN.GetClasifi(i)
                            else
                            begin
                                 LogError( iEmpleado, Format('|Empleado %d|No se encuentra el c�digo:%s en el cat�logo de Clasificaciones', [ iEmpleado, FEmpleadosWFN.GetClasifi(i) ] ) );
                                 lValido := False;
                                 if not lBandera then lBandera := True;
                            end
                       end;
                       if ( StrVacio( FEmpleadosWFN.GetPuesto(i) ) ) and ( lValido ) then
                       begin
                            LogError( iEmpleado, Format('|Empleado %d|El c�digo del puesto no puede quedar vac�o', [ iEmpleado, FEmpleadosWFN.GetPuesto(i) ] ) );
                            lValido := False;
                            if not lBandera then lBandera := True;
                       end;
                       if lValido then //Validar
                       begin
                            if EsValidoPuesto( FEmpleadosWFN.GetPuesto(i) ) then
                                FieldByName('CB_PUESTO').AsString  := FEmpleadosWFN.GetPuesto(i)
                            else
                            begin
                                LogError( iEmpleado, Format('|Empleado %d|No se encuentra el c�digo:%s en el cat�logo de Puestos', [ iEmpleado, FEmpleadosWFN.GetPuesto(i) ] ) );
                                lValido := False;
                                if not lBandera then lBandera := True;
                            end;
                       end;
                       if lValido then
                       begin
                            PostData;
                            OnReconcileError := ReconcileError;
                            if VarIsNull( DeltaNull ) then     //MV( 21/Ago/2007) Se cambio esta parte para validar que el DataSet no va Null
                               lOK := True
                            else
                            begin
                                 if Reconciliar( ServerRecursos.GrabaCambiosMultiples( Empresa, DeltaNull, FOtrosDatos, ErrorCount ) ) then
                                    lOk := True
                                 else
                                 begin
                                      LogError( iEmpleado, Format('|Empleado %d | %s', [ iEmpleado, Self.Status ] ) );
                                      if not lBandera then lBandera := True;
                                 end;
                            end;
                       end;
                  end;
             end
             else
             begin
                  LogError( 0, Format( 'Empleado #%d no existe en la empresa %s:%s', [ iEmpleado, cdsCompany.FieldByName('CM_CODIGO').AsString, cdsCompany.FieldByName('CM_NOMBRE').AsString ] ) );
             end;
          except
                on Error: Exception do
                begin
                     LogError( iEmpleado, 'Error al agregar cambio de Puesto-Clasificacion: ' + Error.Message );
                end;
          end;
     end;
     if not lBandera then FErrores.Clear;
     Result := lOk;
     FreeAndNil(FEmpleadosWFN);
end;

//Cambiar Turno Empleado
function TdmCliente.CambiarTurnoWFN: Boolean;
var
   i, iEmpleado : Integer;
   ErrorCount : Integer;
   FOtrosDatos : Variant;
   lOk: Boolean;
   lBandera: Boolean;
   lValido: Boolean;
begin
     lOk := False;
     lBandera := False;
     CargarEmpleadosWFN;
     LogError( 0, Format( '|Empresa %s|', [ cdsCompany.FieldByName('CM_CODIGO').AsString ] ) );
     for i := 0 to ( FEmpleadosWFN.slEmpleados.Count - 1 ) do
     begin
          lValido := True;
          iEmpleado := FEmpleadosWFN.Empleado[ i ];
          try
             if FetchEmployee( cdsLista, iEmpleado ) then
             begin
                  with FXMLTools do
                  begin
                       FOtrosDatos := VarArrayOf( [FEmpleadosWFN.FecReg[i], FEmpleadosWFN.Descripcion[i], FEmpleadosWFN.Observa[i]] );
                  end;
                  // Enviar los cambios
                  with cdsLista do
                  begin
                       if not ( state in [ dsEdit, dsInsert ] ) then
                        Edit;
                       if not StrVacio( FEmpleadosWFN.GetTurno(i) ) then //Validar
                       begin
                            if EsValidoTurno( FEmpleadosWFN.GetTurno(i) ) then
                                FieldByName('CB_TURNO').AsString := FEmpleadosWFN.GetTurno(i)
                            else
                            begin
                                LogError( iEmpleado, Format('|Empleado %d|No se encuentra el c�digo:%s en el cat�logo de Turnos', [ iEmpleado, FEmpleadosWFN.GetTurno(i) ] ) );
                                lValido := False;
                                if not lBandera then lBandera := True;
                            end;
                       end
                       else
                       begin
                            LogError( iEmpleado, Format('|Empleado %d|El c�digo del turno no puede quedar vac�o', [ iEmpleado, FEmpleadosWFN.GetTurno(i) ] ) );
                            lValido := False;
                            if not lBandera then lBandera := True;
                       end;
                       if lValido then
                       begin
                            PostData;
                            OnReconcileError := ReconcileError;
                            if VarIsNull( DeltaNull ) then     //MV( 21/Ago/2007) Se cambio esta parte para validar que el DataSet no va Null
                               lOK := True
                            else
                            begin
                                 if Reconciliar( ServerRecursos.GrabaCambiosMultiples( Empresa, DeltaNull, FOtrosDatos, ErrorCount ) ) then
                                    lOk := True
                                 else
                                 begin
                                      LogError( iEmpleado, Format('|Empleado %d | %s', [ iEmpleado, Self.Status ] ) );
                                      if not lBandera then lBandera := True;
                                 end;
                            end;
                       end;
                  end;
             end
             else
             begin
                  LogError( 0, Format( 'Empleado #%d no existe en la empresa %s:%s', [ iEmpleado, cdsCompany.FieldByName('CM_CODIGO').AsString, cdsCompany.FieldByName('CM_NOMBRE').AsString ] ) );
             end;
          except
                on Error: Exception do
                begin
                     LogError( iEmpleado, 'Error al agregar cambio de Turno: ' + Error.Message );
                end;
          end;
     end;
     if not lBandera then FErrores.Clear;
     Result := lOk;
     FreeAndNil(FEmpleadosWFN);
end;

procedure TdmCliente.CargarEmpleadosWFN;
const
     NODO_EMPLEADO = 'EMPLEADO';
     NODO_PUESTO = 'CB_PUESTO';
     NODO_CLASIFI = 'CB_CLASIFI';
     NODO_TURNO = 'CB_TURNO';
     NODO_FECHA_REG = 'CB_FECHAREG';
     NODO_DESCRIPCION = 'DESCRIPCION';
     NODO_OBSERVA = 'OBSERVACIONES';
     NODO_EMPLEADOS_DATOS = 'INFO';
     NODO_EMPLEADOS_LISTA = 'EMPLEADOS';

var
   Nodo, Info: TZetaXMLNode;
begin
     FEmpleadosWFN := TEmpleadosWFN.Create;


     with FXMLTools do
     begin

          Nodo := GetNode( NODO_EMPLEADOS_LISTA );
          { Preguntar si son varios empleados }
          if Assigned( Nodo ) then
          begin
               Info := GetNode( NODO_EMPLEADOS_DATOS, Nodo );
               while Assigned( Info ) and HasChildren( Info ) do
               begin
                    FEmpleadosWFN.AddEmpleadosWFN( TagAsInteger( GetNode( NODO_EMPLEADO, Info ), K_EMPLEADO_NULO ), TagAsString( GetNode( NODO_CLASIFI, Info), VACIO),
                       TagAsDateTime( GetNode( NODO_FECHA_REG, Info), NullDateTime ), TagAsString( GetNode( NODO_OBSERVA, Info),VACIO),
                       TagAsString( GetNode( NODO_PUESTO, Info),VACIO) ,  TagAsString( GetNode( NODO_TURNO, Info),VACIO), TagAsString( GetNode( NODO_DESCRIPCION, Info),VACIO)   );
                    Info := GetNextSibling( Info );
               end
          end
          else
          begin
                   FEmpleadosWFN.AddEmpleadosWFN( TagAsInteger( GetNode( NODO_EMPLEADO, Info ), K_EMPLEADO_NULO ), TagAsString( GetNode( NODO_CLASIFI, Info), VACIO),
                       TagAsDateTime( GetNode( NODO_FECHA_REG, Info), NullDateTime ), TagAsString( GetNode( NODO_OBSERVA, Info),VACIO),
                       TagAsString( GetNode( NODO_PUESTO, Info),VACIO) ,  TagAsString( GetNode( NODO_TURNO, Info),VACIO) ,TagAsString( GetNode( NODO_DESCRIPCION, Info),VACIO)   );
          end;
     end;
     if ( FEmpleadosWFN.slEmpleados.Count = 0 ) then
        raise Exception.Create( 'No se especific� un empleado' );
end;



function TdmCliente.AgregarCambioSalarioWFN: Boolean;
var
   i: Integer;
   ErrorCount : Integer;
   lOk: Boolean;
   dFechaSal: TDateTime;
   sPercepcionesFijas: String;

   ListaEmpleados: TObjectList;
   EsquemaEmpleado: TEmpleadoBasico;
   Empleado: TEmpleadoBasico;

    function GetSalClasifi( const sClasifi: String ): TPesos;
    begin
         Result:= 0;
         with cdsClasifi do
         begin
              Conectar;
              if Locate( 'TB_CODIGO', sClasifi, [loCaseInsensitive] ) then
                 Result := FieldByName( 'TB_SALARIO' ).AsFloat;
         end;
    end;

    procedure CargarEmpleadosCS;
    const
         NODO_USUARIOWFN = 'PersonaID';
         NODO_EMPLEADO = 'EMPLEADO';
         NODO_SALARIO = 'CB_SALARIO';
         NODO_AUTOSAL = 'CB_AUTOSAL';
         NODO_FECHA_REG = 'CB_FECHAREG';
         NODO_FECHASS = 'CB_FECHASS';
         NODO_DESCRIPCION = 'DESCRIPCION';
         NODO_OBSERVA = 'OBSERVACIONES';

         NODO_EMPLEADOS_DATOS = 'INFO';
         NODO_EMPLEADOS_LISTA = 'EMPLEADOS';
    var
       Nodo, Info: TZetaXMLNode;
    begin
         with FXMLTools do
         begin

              Nodo := GetNode( NODO_EMPLEADOS_LISTA );
              { Preguntar si son varios empleados }
              if Assigned( Nodo ) then
              begin
                   Info := GetNode( NODO_EMPLEADOS_DATOS, Nodo );
                   while Assigned( Info ) and HasChildren( Info ) do
                   begin
                         EsquemaEmpleado := TEmpleadoBasico.Create;

                         EsquemaEmpleado.UsuarioWFN := TagAsInteger( GetNode( NODO_USUARIOWFN, Info ), K_EMPLEADO_NULO );
                         EsquemaEmpleado.NumeroEmpleado := TagAsInteger( GetNode( NODO_EMPLEADO, Info ), K_EMPLEADO_NULO );
                         EsquemaEmpleado.Salario := TagAsFloat(GetNode( NODO_SALARIO, Info ),0.0);
                         EsquemaEmpleado.AutoSalario := TagAsString( GetNode( NODO_AUTOSAL, Info ), K_DEFAULT_AUTOSALARIO );
                         EsquemaEmpleado.FechaRegistro := TagAsDateTime( GetNode( NODO_FECHA_REG, Info), NullDateTime );
                         EsquemaEmpleado.FechaImss := TagAsDateTime( GetNode( NODO_FECHASS, Info), NullDateTime);
                         EsquemaEmpleado.Descripcion := TagAsString( GetNode( NODO_DESCRIPCION, Info),VACIO);
                         EsquemaEmpleado.Observaciones := TagAsString( GetNode( NODO_OBSERVA, Info),VACIO);

                         ListaEmpleados.Add(EsquemaEmpleado);
                         Info := GetNextSibling( Info );
                   end
              end;
         end;
         if ( ListaEmpleados.Count = 0 ) then
            raise Exception.Create( 'No se especific� un empleado' );
    end;

begin

     lOk := False;
     ListaEmpleados := TObjectList.Create(true);
     CargarEmpleadosCS;
     for i := 0 to ( ListaEmpleados.Count - 1 ) do
     begin

          Empleado := TEmpleadoBasico(ListaEmpleados[i]);
          try
             if FetchEmployee( cdsLista, Empleado.NumeroEmpleado ) then
             begin
                with cdsLista do
                begin
                     Data := ServerRecursos.GetEditHisKardex( Empresa, Empleado.NumeroEmpleado, 0, VACIO );  // Llenar DataSet Vacio con formato de campos de KARDEX
                     Append;
                     FieldByName( 'CB_CODIGO').AsInteger := Empleado.NumeroEmpleado;
                     with FXMLTools do
                     begin
                          FieldByName( 'CB_FECHA' ).AsDateTime :=  Empleado.FechaRegistro;
                          FieldByName( 'CB_FECHA_2' ).AsDateTime := Empleado.FechaImss;
                          if ( FieldByName( 'CB_FECHA_2' ).AsDateTime = NullDateTime ) then
                             FieldByName( 'CB_FECHA_2' ).AsDateTime := FieldByName( 'CB_FECHA' ).AsDateTime;
                     end;
                     FieldByName( 'CB_FAC_INT' ).AsFloat:= 0;
                     FieldByName( 'CB_GLOBAL' ).AsString  := K_GLOBAL_NO;
                     FieldByName( 'CB_REINGRE' ).AsString := K_GLOBAL_NO;
                     FieldByName( 'CB_AUTOSAL' ).AsString := Empleado.AutoSalario;
                     FieldByName( 'CB_NIVEL' ).AsInteger  := 5;
                     FieldByName( 'CB_STATUS' ).AsInteger := Ord( skCapturado );
                     FieldByName( 'CB_TIPO' ).AsString := K_T_CAMBIO;
                     InitCamposSalario( Empleado.NumeroEmpleado, dFechaSal );   // Datos tomados de COLABORA
                     with FXMLTools do  // Datos del XML
                     begin
                     if Empleado.AutoSalario = 'S' then
                     begin
                       FieldByName( 'CB_SALARIO' ).AsFloat := GetSalClasifi( FieldByName( 'CB_CLASIFI' ).AsString );
                     end
                     else
                     begin
                       FieldByName( 'CB_SALARIO' ).AsFloat :=  Empleado.Salario;
                     end;
                         FieldByName( 'CB_COMENTA' ).AsString := Empleado.Descripcion;
                         FieldByName( 'CB_NOTA' ).AsString := Empleado.Observaciones;

                     end;
                     PostData;
                     OnReconcileError := ReconcileError;
                     sPercepcionesFijas := GetPercepcionesFijas( Empleado.NumeroEmpleado, dFechaSal );
                     if Reconciliar( ServerRecursos.GrabaHisKardex( Empresa, Delta, sPercepcionesFijas, ErrorCount) ) then
                        lOk := True
                     else
                     begin
                          LogError( Empleado.NumeroEmpleado, Format('|Empleado %d|%s', [ Empleado.NumeroEmpleado, Self.Status ] ) );
                     end;
                end;
             end
             else
             begin
                  DataBaseError( 'No se encontr� el empleado # ' + IntToStr( Empleado.NumeroEmpleado ) );
             end;
          except
                   on Error: Exception do
                   begin
                        LogError( Empleado.NumeroEmpleado, 'Error al agregar cambio de salario: ' + Error.Message );
                   end;
          end;
     end;
     Result := lOk;
     FreeAndNil(ListaEmpleados);
end;


function TdmCliente.AgregarCambioAreasWFN: Boolean;
var
   i, j: Integer;
   dFechaSal: TDateTime;
   ErrorCount: Integer;
   sPercepcionesFijas: String;
   lOk, lValido: Boolean;

   ListaEmpleados: TObjectList;
   EsquemaEmpleado: TEmpleadoBasico;
   Empleado: TEmpleadoBasico;

    procedure CargarEmpleadosCambioArea;
    const
         NODO_USUARIOWFN = 'PersonaID';
         NODO_EMPLEADO = 'EMPLEADO';
         NODO_NIVEL1 = 'CB_NIVEL1';
         NODO_NIVEL2 = 'CB_NIVEL2';
         NODO_NIVEL3 = 'CB_NIVEL3';
         NODO_NIVEL4 = 'CB_NIVEL4';
         NODO_NIVEL5 = 'CB_NIVEL5';
         NODO_NIVEL6 = 'CB_NIVEL6';
         NODO_NIVEL7 = 'CB_NIVEL7';
         NODO_NIVEL8 = 'CB_NIVEL8';
         NODO_NIVEL9 = 'CB_NIVEL9';
		 NODO_TURNO  = 'CB_TURNO';		
		 NODO_PUESTO  = 'CB_PUESTO';				 
         NODO_FECHA_REG = 'CB_FECHAREG';
         NODO_DESCRIPCION = 'DESCRIPCION';
         NODO_OBSERVA = 'OBSERVACIONES';

         NODO_EMPLEADOS_DATOS = 'INFO';
         NODO_EMPLEADOS_LISTA = 'EMPLEADOS';
    var
       Nodo, Info: TZetaXMLNode;
    begin
         with FXMLTools do
         begin

              Nodo := GetNode( NODO_EMPLEADOS_LISTA );
              { Preguntar si son varios empleados }
              if Assigned( Nodo ) then
              begin
                   Info := GetNode( NODO_EMPLEADOS_DATOS, Nodo );
                   while Assigned( Info ) and HasChildren( Info ) do
                   begin

                        EsquemaEmpleado := TEmpleadoBasico.Create;

                        EsquemaEmpleado.NumeroEmpleado :=TagAsInteger( GetNode( NODO_EMPLEADO, Info ), K_EMPLEADO_NULO );
                        EsquemaEmpleado.Nivel1 := TagAsString( GetNode( NODO_NIVEL1, Info),VACIO);
                        EsquemaEmpleado.Nivel2 := TagAsString( GetNode( NODO_NIVEL2, Info),VACIO);
                        EsquemaEmpleado.Nivel3 := TagAsString( GetNode( NODO_NIVEL3, Info),VACIO);
                        EsquemaEmpleado.Nivel4 := TagAsString( GetNode( NODO_NIVEL4, Info),VACIO);
                        EsquemaEmpleado.Nivel5 := TagAsString( GetNode( NODO_NIVEL5, Info),VACIO);
                        EsquemaEmpleado.Nivel6 := TagAsString( GetNode( NODO_NIVEL6, Info),VACIO);
                        EsquemaEmpleado.Nivel7 := TagAsString( GetNode( NODO_NIVEL7, Info),VACIO);
                        EsquemaEmpleado.Nivel8 := TagAsString( GetNode( NODO_NIVEL8, Info),VACIO);
                        EsquemaEmpleado.Nivel9 := TagAsString( GetNode( NODO_NIVEL9, Info),VACIO);
						EsquemaEmpleado.Turno  := TagAsString( GetNode( NODO_TURNO, Info),VACIO);						
						EsquemaEmpleado.Puesto := TagAsString( GetNode( NODO_PUESTO, Info),VACIO);						
                        EsquemaEmpleado.FechaRegistro := TagAsDateTime( GetNode( NODO_FECHA_REG, Info), NullDateTime );
                        EsquemaEmpleado.Descripcion := TagAsString( GetNode( NODO_DESCRIPCION, Info),VACIO);
                        EsquemaEmpleado.Observaciones := TagAsString( GetNode( NODO_OBSERVA, Info),VACIO);

                        ListaEmpleados.Add(EsquemaEmpleado);
                        Info := GetNextSibling( Info );
                   end
              end;
         end;
         if ( ListaEmpleados.Count = 0 ) then
            raise Exception.Create( 'No se especific� un empleado' );
    end;

    procedure ValidarNiveles;
    begin
             with cdsLista do
             begin
                         if Empleado.Nivel1 <> VACIO then
                           begin
                                if (not EsValidoNivel1( Empleado.Nivel1 )) then
                                begin
                                     LogError( Empleado.NumeroEmpleado, Format('|Empleado %d|No se encuentra el c�digo:%s en el cat�logo de Nivel 1', [ Empleado.NumeroEmpleado, Empleado.Nivel1 ] ) );
                                     lValido := False;
                                end;
                                FieldByName('CB_NIVEL1').AsString := Empleado.Nivel1;
                           end;
                           if Empleado.Nivel2 <> VACIO then
                           begin
                                if (not EsValidoNivel2( Empleado.Nivel2 )) then
                                begin
                                     LogError( Empleado.NumeroEmpleado, Format('|Empleado %d|No se encuentra el c�digo:%s en el cat�logo de Nivel 2', [Empleado.NumeroEmpleado, Empleado.Nivel2 ] ) );
                                     lValido := False;
                                end;
                              FieldByName('CB_NIVEL2').AsString := Empleado.Nivel2;
                           end;
                           if Empleado.Nivel3 <> VACIO then
                           begin
                                if (not EsValidoNivel3( Empleado.Nivel3 )) then
                                begin
                                     LogError( Empleado.NumeroEmpleado, Format('|Empleado %d|No se encuentra el c�digo:%s en el cat�logo de Nivel 3', [ Empleado.NumeroEmpleado, Empleado.Nivel3 ] ) );
                                     lValido := False;
                                end;
                              FieldByName('CB_NIVEL3').AsString := Empleado.Nivel3;
                           end;
                           if Empleado.Nivel4 <> VACIO then
                           begin
                                if (not EsValidoNivel4( Empleado.Nivel4 )) then
                                begin
                                     LogError( Empleado.NumeroEmpleado, Format('|Empleado %d|No se encuentra el c�digo:%s en el cat�logo de Nivel 4', [ Empleado.NumeroEmpleado, Empleado.Nivel4 ] ) );
                                     lValido := False;
                                end;
                               FieldByName('CB_NIVEL4').AsString := Empleado.Nivel4;
                           end;
                           if Empleado.Nivel5 <> VACIO then
                           begin
                                if (not EsValidoNivel5( Empleado.Nivel5 )) then
                                begin
                                     LogError( Empleado.NumeroEmpleado, Format('|Empleado %d|No se encuentra el c�digo:%s en el cat�logo de Nivel 5', [ Empleado.NumeroEmpleado, Empleado.Nivel5 ] ) );
                                     lValido := False;
                                end;
                                FieldByName('CB_NIVEL5').AsString := Empleado.Nivel5;
                           end;
                           if Empleado.Nivel6 <> VACIO then
                           begin
                                if (not EsValidoNivel6( Empleado.Nivel6 )) then
                                begin
                                     LogError( Empleado.NumeroEmpleado, Format('|Empleado %d|No se encuentra el c�digo:%s en el cat�logo de Nivel 6', [ Empleado.NumeroEmpleado, Empleado.Nivel6 ] ) );
                                     lValido := False;
                                end;
                                FieldByName('CB_NIVEL6').AsString := Empleado.Nivel6;
                           end;
                           if Empleado.Nivel7 <> VACIO then
                           begin
                                if (not EsValidoNivel7( Empleado.Nivel7 )) then
                                begin
                                     LogError( Empleado.NumeroEmpleado, Format('|Empleado %d|No se encuentra el c�digo:%s en el cat�logo de Nivel 7', [ Empleado.NumeroEmpleado, Empleado.Nivel7 ] ) );
                                     lValido := False;
                                end;
                                FieldByName('CB_NIVEL7').AsString := Empleado.Nivel7;
                           end;
                           if Empleado.Nivel8 <> VACIO then
                           begin
                                if (not EsValidoNivel8( Empleado.Nivel8 )) then
                                begin
                                     LogError( Empleado.NumeroEmpleado, Format('|Empleado %d|No se encuentra el c�digo:%s en el cat�logo de Nivel 8', [ Empleado.NumeroEmpleado, Empleado.Nivel8 ] ) );
                                     lValido := False;
                                end;
                                FieldByName('CB_NIVEL8').AsString := Empleado.Nivel8;
                           end;
                           if Empleado.Nivel9 <> VACIO then
                           begin
                                if (not EsValidoNivel9( Empleado.Nivel9 )) then
                                begin
                                     LogError( Empleado.NumeroEmpleado, Format('|Empleado %d|No se encuentra el c�digo:%s en el cat�logo de Nivel 9', [ Empleado.NumeroEmpleado, Empleado.Nivel9 ] ) );
                                     lValido := False;
                                end;
                                 FieldByName('CB_NIVEL9').AsString := Empleado.Nivel9;
                           end;

                           if not StrVacio( Empleado.Turno) then
                           begin
                                if EsValidoTurno( Empleado.Turno ) then
                                begin
                                   FieldByName('CB_TURNO').AsString := Empleado.Turno;
                                end
                                else
                                begin
                                    LogError(  Empleado.NumeroEmpleado, Format('|Empleado %d|No se encuentra el c�digo:%s en el cat�logo de Turnos', [ Empleado.NumeroEmpleado, Empleado.Turno ] ) );
                                    lValido := False;
                                end;
			   end;

                           if not StrVacio( Empleado.Puesto ) then
                           begin
                                if EsValidoPuesto( Empleado.Puesto ) then
                                begin
                                   FieldByName('CB_PUESTO').AsString := Empleado.Puesto;
                                end
                                else
                                begin
                                     LogError(  Empleado.NumeroEmpleado, Format('|Empleado %d|No se encuentra el c�digo:%s en el cat�logo de Puestos', [ Empleado.NumeroEmpleado, Empleado.Puesto ] ) );
                                     lValido := False;
                                end;
                           end;
						   //end nuevo

             end;
    end;

begin
     lOk := False;
     ListaEmpleados := TObjectList.Create(true);
     CargarEmpleadosCambioArea;
     for i := 0 to ( ListaEmpleados.Count - 1 ) do
     begin
          Empleado := TEmpleadoBasico(ListaEmpleados[i]);
          try
             if FetchEmployee( cdsLista, Empleado.NumeroEmpleado ) then
             begin
                  with cdsLista do
                  begin
                  Data := ServerRecursos.GetEditHisKardex( Empresa, Empleado.NumeroEmpleado, 0, VACIO );  // Llenar DataSet Vacio con formato de campos de KARDEX
                  Append;
                       lValido := true;
                       InitCamposNiveles(Empleado.NumeroEmpleado, dFechaSal);  //Toma datos de los niveles de colabora
                       ValidarNiveles;
                       FieldByName( 'CB_TIPO' ).AsString := K_T_AREA;
                       FieldByName( 'CB_CODIGO').AsInteger := Empleado.NumeroEmpleado;
                       FieldByName( 'CB_COMENTA' ).AsString :=  Empleado.Descripcion;
                       FieldByName( 'CB_NOTA' ).AsString := Empleado.Observaciones;
                       FieldByName( 'CB_FECHA' ).AsDateTime :=  Empleado.FechaRegistro;
                       PostData;
                       if VarIsNull( DeltaNull ) then
                          lOK := True
                       else
                       begin
                         if lValido then
                         begin
                                OnReconcileError := ReconcileError;
                                sPercepcionesFijas := GetPercepcionesFijas( Empleado.NumeroEmpleado, dFechaSal );
                                if Reconciliar(ServerRecursos.GrabaHisKardex ( Empresa, DeltaNull, sPercepcionesFijas, ErrorCount ) ) then
                                begin
                                     lOk := True;
                                end
                                else
                                begin
                                     LogError( Empleado.NumeroEmpleado, Format('|Empleado %d|%s', [ Empleado.NumeroEmpleado, Self.Status ] ) );
                                end;
                         end;
                       end;
                  end;
             end
             else
             begin
                  DataBaseError( 'No se encontr� el empleado # ' + IntToStr( Empleado.NumeroEmpleado ) );
             end;
          except
                on Error: Exception do
                begin
                     LogError( Empleado.NumeroEmpleado, 'Error al agregar cambio de areas: ' + Error.Message );
                end;
          end;
     end;
     Result := lOk;
     FreeAndNil(ListaEmpleados);
end;


function TdmCliente.AgregarCambioContratoWFN: Boolean;
var
   i, j: Integer;
   dFechaSal: TDateTime;
   ErrorCount: Integer;
   sPercepcionesFijas: String;
   lOk, lValido: Boolean;

   ListaEmpleados: TObjectList;
   EsquemaEmpleado: TEmpleadoBasico;
   Empleado: TEmpleadoBasico;

    procedure CargarEmpleadosCambioContrato;
    const
         NODO_USUARIOWFN = 'PersonaID';
         NODO_EMPLEADO = 'EMPLEADO';
         NODO_CB_CONTRAT = 'CB_CONTRAT';
         NODO_CB_FECHAREG = 'CB_FECHA';
         NODO_CB_FEC_CON = 'CB_FEC_CON';
         NODO_CB_FEC_COV = 'CB_FEC_VEC';
         NODO_CB_NOTA = 'CB_NOTA';
         NODO_CB_COMENTA = 'CB_COMENTA';

         NODO_EMPLEADOS_DATOS = 'INFO';
         NODO_EMPLEADOS_LISTA = 'EMPLEADOS';
    var
       Nodo, Info: TZetaXMLNode;
    begin
         with FXMLTools do
         begin

              Nodo := GetNode( NODO_EMPLEADOS_LISTA );
              { Preguntar si son varios empleados }
              if Assigned( Nodo ) then
              begin
                   Info := GetNode( NODO_EMPLEADOS_DATOS, Nodo );
                   while Assigned( Info ) and HasChildren( Info ) do
                   begin

                        EsquemaEmpleado := TEmpleadoBasico.Create;

                        EsquemaEmpleado.NumeroEmpleado :=TagAsInteger( GetNode( NODO_EMPLEADO, Info ), K_EMPLEADO_NULO );
                        EsquemaEmpleado.TipoContrato := TagAsString( GetNode( NODO_CB_CONTRAT, Info),VACIO);
                        EsquemaEmpleado.FechaRegistro := TagAsDateTime( GetNode( NODO_CB_FECHAREG, Info), NullDateTime );
                        EsquemaEmpleado.FechaContrato := TagAsDateTime( GetNode( NODO_CB_FEC_CON, Info), NullDateTime );
                        EsquemaEmpleado.FechaContratoVencimiento := TagAsDateTime( GetNode( NODO_CB_FEC_COV, Info), NullDateTime );
                        EsquemaEmpleado.Descripcion := TagAsString( GetNode( NODO_CB_COMENTA, Info),VACIO);
                        EsquemaEmpleado.Observaciones := TagAsString( GetNode( NODO_CB_NOTA, Info),VACIO);

                        ListaEmpleados.Add(EsquemaEmpleado);
                        Info := GetNextSibling( Info );
                   end
              end;
         end;
         if ( ListaEmpleados.Count = 0 ) then
            raise Exception.Create( 'No se especific� un empleado' );
    end;

    procedure ValidarCampos;
    begin
             with cdsLista do
             begin
                 if (Empleado.FechaRegistro < 1)  then
                 begin
                     LogError( Empleado.NumeroEmpleado, Format('|Empleado %d|No se encuentra la fecha.', [ Empleado.NumeroEmpleado ] ) );
                     lValido := False;
                 end;
                 if (Empleado.FechaContrato < 1)  then
                 begin
                     LogError( Empleado.NumeroEmpleado, Format('|Empleado %d|No se encuentra la fecha de inicio del contrato.', [ Empleado.NumeroEmpleado ] ) );
                     lValido := False;
                 end;                 
                 if Empleado.TipoContrato = VACIO then
                 begin
                           LogError( Empleado.NumeroEmpleado, Format('|Empleado %d|Tipo de contrato no puede quedar vac�o.', [ Empleado.NumeroEmpleado, Empleado.Nivel4 ] ) );
                           lValido := False;
                 end
                 else
                 begin
                      if (not EsValidoContrato( Empleado.TipoContrato )) then
                      begin
                           LogError( Empleado.NumeroEmpleado, Format('|Empleado %d|No se encuentra el c�digo:%s en el cat�logo de tipos de contrato', [ Empleado.NumeroEmpleado, Empleado.TipoContrato ] ) );
                           lValido := False;
                      end;
                 end;

             end;
    end;

begin
     lOk := False;
     dFechaSal := NullDateTime;
     ListaEmpleados := TObjectList.Create(true);
     CargarEmpleadosCambioContrato;
     for i := 0 to ( ListaEmpleados.Count - 1 ) do
     begin
          Empleado := TEmpleadoBasico(ListaEmpleados[i]);
          try
             if FetchEmployee( cdsLista, Empleado.NumeroEmpleado ) then
             begin
                  with cdsLista do
                  begin
                  Data := ServerRecursos.GetEditHisKardex( Empresa, Empleado.NumeroEmpleado, 0, VACIO );  // Llenar DataSet Vacio con formato de campos de KARDEX
                  Append;
                       lValido := true;
                       ValidarCampos;
                       FieldByName( 'CB_TIPO' ).AsString := K_T_RENOVA;
                       FieldByName( 'CB_CODIGO').AsInteger := Empleado.NumeroEmpleado;
                       FieldByName( 'CB_COMENTA' ).AsString :=  Empleado.Descripcion;
                       FieldByName( 'CB_NOTA' ).AsString := Empleado.Observaciones;
                       FieldByName( 'CB_FECHA' ).AsDateTime :=  Empleado.FechaRegistro;
                       FieldByName( 'CB_CONTRAT' ).AsString :=  Empleado.TipoContrato;
                       FieldByName( 'CB_FEC_CON' ).AsDateTime :=  Empleado.FechaContrato;
                       FieldByName( 'CB_FEC_COV' ).AsDateTime :=  Empleado.FechaContratoVencimiento;
                       PostData;
                       if VarIsNull( DeltaNull ) then
                          lOK := True
                       else
                       begin
                         if lValido then
                         begin
                                OnReconcileError := ReconcileError;
                                sPercepcionesFijas := GetPercepcionesFijas( Empleado.NumeroEmpleado, dFechaSal );
                                if Reconciliar(ServerRecursos.GrabaHisKardex ( Empresa, DeltaNull, sPercepcionesFijas, ErrorCount ) ) then
                                begin
                                     lOk := True;
                                end
                                else
                                begin
                                     LogError( Empleado.NumeroEmpleado, Format('|Empleado %d|%s', [ Empleado.NumeroEmpleado, Self.Status ] ) );
                                end;
                         end;
                       end;
                  end;
             end
             else
             begin
                  DataBaseError( 'No se encontr� el empleado # ' + IntToStr( Empleado.NumeroEmpleado ) );
             end;
          except
                on Error: Exception do
                begin
                     LogError( Empleado.NumeroEmpleado, 'Error al agregar cambio de contrato: ' + Error.Message );
                end;
          end;
     end;
     Result := lOk;
     FreeAndNil(ListaEmpleados);
end;


function TdmCliente.CRUDParienteEmpleadoWFN(sTipo : string): Boolean;
var
   i, j: Integer;
   ErrorCount: Integer;
   sOperacionError: String;
   lOk, lValido: Boolean;
   sParentesco : String;

   ListaEmpleados: TObjectList;
   EsquemaEmpleado: TEmpleadoBasico;
   Empleado: TEmpleadoBasico;

    procedure CargarParientes;
    const
         NODO_USUARIOWFN = 'PersonaID';
         NODO_EMPLEADO = 'EMPLEADO';
         NODO_PA_FEC_NAC = 'PA_FEC_NAC';
         NODO_PA_RELACIO = 'PA_RELACIO';
         NODO_PA_FOLIO = 'PA_FOLIO';
         NODO_PA_SEXO = 'PA_SEXO';
         NODO_PA_APE_PAT = 'PA_APE_PAT';
         NODO_PA_APE_MAT = 'PA_APE_MAT';
         NODO_PA_NOMBRES = 'PA_NOMBRES';
         NODO_EMPLEADOS_DATOS = 'INFO';
         NODO_EMPLEADOS_LISTA = 'EMPLEADOS';
    var
       Nodo, Info: TZetaXMLNode;
    begin
         with FXMLTools do
         begin

              Nodo := GetNode( NODO_EMPLEADOS_LISTA );
              { Preguntar si son varios empleados }
              if Assigned( Nodo ) then
              begin
                   Info := GetNode( NODO_EMPLEADOS_DATOS, Nodo );
                   while Assigned( Info ) and HasChildren( Info ) do
                   begin
                        EsquemaEmpleado := TEmpleadoBasico.Create;
                        EsquemaEmpleado.NumeroEmpleado := TagAsInteger( GetNode( NODO_EMPLEADO, Info ), K_EMPLEADO_NULO );
                        EsquemaEmpleado.FechaNacimiento_Par := TagAsDateTime( GetNode( NODO_PA_FEC_NAC, Info), NullDateTime );
                        EsquemaEmpleado.Relacion_Par := TagAsInteger( GetNode( NODO_PA_RELACIO, Info ), K_EMPLEADO_NULO );
                        EsquemaEmpleado.Folio_Par := TagAsInteger( GetNode( NODO_PA_FOLIO, Info ), K_EMPLEADO_NULO );
                        EsquemaEmpleado.Sexo_Par := TagAsString( GetNode( NODO_PA_SEXO, Info),VACIO);
                        EsquemaEmpleado.ApellidoPaterno_Par := TagAsString( GetNode( NODO_PA_APE_PAT, Info),VACIO);
                        EsquemaEmpleado.ApellidoMaterno_Par := TagAsString( GetNode( NODO_PA_APE_MAT, Info),VACIO);
                        EsquemaEmpleado.Nombre_Par := TagAsString( GetNode( NODO_PA_NOMBRES, Info),VACIO);
                        ListaEmpleados.Add(EsquemaEmpleado);
                        Info := GetNextSibling( Info );
                   end
              end;
         end;
         if ( ListaEmpleados.Count = 0 ) then
            raise Exception.Create( 'No se especific� un empleado' );
    end;

    procedure ValidarCampos;
    begin
             with cdsLista do
             begin
                 if (StrVacio( Empleado.Nombre_Par )) then
                 begin
                     LogError( Empleado.NumeroEmpleado, 'Capturar nombres del pariente.' );
                     lValido := False;
                 end;
                 if ( StrVacio(Empleado.ApellidoPaterno_Par )) then
                 begin
                     LogError( Empleado.NumeroEmpleado, 'Capturar apellido paterno del pariente.' );
                     lValido := False;
                 end;
                 if (Empleado.FechaNacimiento_Par < 1)  then
                 begin
                     LogError( Empleado.NumeroEmpleado, 'Capturar fecha de nacimiento del pariente.' );
                     lValido := False;
                 end;
                 if ((Empleado.Sexo_Par <> VACIO) and((Empleado.Sexo_Par <> 'M') and (Empleado.Sexo_Par <> 'F')))  then
                 begin
                     LogError( Empleado.NumeroEmpleado, 'Capturar el sexo del pariente.' );
                     lValido := False;
                 end;
                 if (IntToStr(Empleado.Relacion_Par) = VACIO) then
                 begin
                           LogError( Empleado.NumeroEmpleado, 'Relaci�n de pariente no puede quedar vacio.' );
                           lValido := False;
                 end
                 else
                 begin
                      sParentesco := VACIO;
                      sParentesco := ZetaCommonLists.ObtieneElemento( lfTipoPariente, Ord( Empleado.Relacion_Par ) );
                      if(sParentesco = VACIO) then
                      begin
                           LogError( Empleado.NumeroEmpleado, Format('No se encuentra el c�digo:%d en la lista de parentescos', [ Empleado.Relacion_Par ] ) );
                           lValido := False;
                      end;
                 end;

             end;
    end;

    function NombreCompletoPariente: string;
    begin
           Result := (Empleado.Nombre_Par +' '+ Empleado.ApellidoPaterno_Par +' '+ Empleado.ApellidoMaterno_Par);
    end;

    function ObtenerFolioPariente( NumeroEmpleado, RelacionPar: Integer ): Integer;
    const
         K_CONSULTA = 'SELECT MAX( PA_FOLIO ) AS FOLIO FROM PARIENTE WHERE CB_CODIGO=%d AND PA_RELACIO=%d';
         K_FOLIO_PARIENTE = 1;
    var
       iFolioPariente: Integer;
       oConsultaParientes: TClientDataSet;
    begin
         oConsultaParientes := TZetaClientDataSet.Create( self );
         oConsultaParientes.Data := ServerConsultas.GetQueryGralTodos( Empresa, Format( K_CONSULTA, [ NumeroEmpleado, RelacionPar ] ) );
         if ( not oConsultaParientes.Eof ) then
            iFolioPariente := oConsultaParientes.FieldByName( 'FOLIO' ).AsInteger + 1
         else
             iFolioPariente := K_FOLIO_PARIENTE;
         Result := iFolioPariente;
    end;

    procedure AgregarPariente;
    begin
         with cdsLista do
         begin
               Empleado.Folio_Par := ObtenerFolioPariente( Empleado.NumeroEmpleado, Empleado.Relacion_Par ); //Asignar automaticamente el FolioFix US 12431: No es posible agregar 2 hijos para un empleado
               if Locate( 'CB_CODIGO;PA_RELACIO;PA_FOLIO',VarArrayOf([Empleado.NumeroEmpleado, Empleado.Relacion_Par, Empleado.Folio_Par ]),[] ) then //Fix US 12431: No es posible agregar 2 hijos para un empleado
               begin
                    DataBaseError( 'Ya existe el pariente '+ NombreCompletoPariente +'.');
               end
               else
               begin
                    Append;
                    FieldByName( 'CB_CODIGO').AsInteger := Empleado.NumeroEmpleado;
                    FieldByName( 'PA_FEC_NAC' ).AsDateTime :=  Empleado.FechaNacimiento_Par;
                    FieldByName( 'PA_RELACIO' ).AsInteger := Empleado.Relacion_Par;
                    FieldByName( 'PA_FOLIO' ).AsInteger :=  Empleado.Folio_Par;
                    FieldByName( 'PA_SEXO' ).AsString :=  Empleado.Sexo_Par;
                    FieldByName( 'PA_APE_PAT' ).AsString :=  Empleado.ApellidoPaterno_Par;
                    FieldByName( 'PA_APE_MAT' ).AsString :=  Empleado.ApellidoMaterno_Par;
                    FieldByName( 'PA_NOMBRES' ).AsString :=  Empleado.Nombre_Par;
                    FieldByName( 'PA_NOMBRE' ).AsString := NombreCompletoPariente;
                    PostData;
               end;
         end;
    end;

    procedure ModificarPariente;
    begin
         with cdsLista do
         begin
               if Locate( 'CB_CODIGO;PA_RELACIO;PA_FOLIO',VarArrayOf([Empleado.NumeroEmpleado, Empleado.Relacion_Par, Empleado.Folio_Par ]),[] ) then
               begin
                    Edit;
                    FieldByName( 'CB_CODIGO').AsInteger := Empleado.NumeroEmpleado;
                    FieldByName( 'PA_FEC_NAC' ).AsDateTime :=  Empleado.FechaNacimiento_Par;
                    FieldByName( 'PA_RELACIO' ).AsInteger := Empleado.Relacion_Par;
                    FieldByName( 'PA_FOLIO' ).AsInteger :=  Empleado.Folio_Par;
                    FieldByName( 'PA_SEXO' ).AsString :=  Empleado.Sexo_Par;
                    FieldByName( 'PA_APE_PAT' ).AsString :=  Empleado.ApellidoPaterno_Par;
                    FieldByName( 'PA_APE_MAT' ).AsString :=  Empleado.ApellidoMaterno_Par;
                    FieldByName( 'PA_NOMBRES' ).AsString :=  Empleado.Nombre_Par;
                    FieldByName( 'PA_NOMBRE' ).AsString := NombreCompletoPariente;
                    PostData;
               end
               else
               begin
                    DataBaseError( 'No se encontr� el pariente '+ NombreCompletoPariente +' registrado.');
               end;               
         end;
    end;

    procedure BorrarPariente;
    begin
         with cdsLista do
         begin
               if Locate( 'CB_CODIGO;PA_RELACIO;PA_FOLIO',VarArrayOf([Empleado.NumeroEmpleado, Empleado.Relacion_Par, Empleado.Folio_Par ]),[] ) then
               begin
                    Edit;
                    Delete;
               end
               else
               begin
                    DataBaseError( 'No se encontr� el pariente '+ NombreCompletoPariente +' registrado.');
               end;
         end;
    end;

begin
     lOk := False;
     ListaEmpleados := TObjectList.Create(true);
     CargarParientes;
     for i := 0 to ( ListaEmpleados.Count - 1 ) do
     begin
          Empleado := TEmpleadoBasico(ListaEmpleados[i]);
          try
             if FetchEmployee( cdsLista, Empleado.NumeroEmpleado ) then
             begin
                  with cdsLista do
                  begin
                       Data := ServerRecursos.GetEmpParientes( Empresa, Empleado.NumeroEmpleado );  // Llenar DataSet Vacio con formato de campos de KARDEX
                       lValido := true;
                       ValidarCampos;
                       if ( sTipo = K_TW_AGREGAR_PARIENTE ) then //Agregar Pariente
                       begin
                           sOperacionError := 'Error al agregar pariente: ';
                           AgregarPariente;
                       end;
                       if ( sTipo = K_TW_MODIFICAR_PARIENTE ) then //Modificar Pariente
                       begin
                           sOperacionError := 'Error al modificar pariente: ';
                           ModificarPariente;
                       end;
                       if ( sTipo = K_TW_BORRAR_PARIENTE ) then //Borrar Pariente
                       begin
                           sOperacionError := 'Error al borrar pariente: ';
                           BorrarPariente;
                       end;
                       if VarIsNull( DeltaNull ) then
                          lOK := True
                       else
                       begin
                         if lValido then
                         begin
                                OnReconcileError := ReconcileError;
                                if Reconciliar(ServerRecursos.GrabaEmpParientes( Empresa, DeltaNull, ErrorCount ) ) then
                                begin
                                     lOk := True;
                                end
                                else
                                begin
                                     LogError( Empleado.NumeroEmpleado, Format('|Empleado %d|%s', [ Empleado.NumeroEmpleado, Self.Status ] ) );
                                end;
                         end;
                       end;
                  end;
             end
             else
             begin
                  DataBaseError( 'No se encontr� el empleado # ' + IntToStr( Empleado.NumeroEmpleado ) );
             end;
          except
                on Error: Exception do
                begin
                     LogError( Empleado.NumeroEmpleado, sOperacionError + Error.Message );
                end;
          end;
     end;
     Result := lOk;
     FreeAndNil(ListaEmpleados);
end;


procedure TdmCliente.InitCamposNiveles( const iEmpleado: Integer; var dFechaSal: TDateTime );
var
   oDataSet : TZetaClientDataSet;
begin
     oDataSet := TZetaClientDataSet.Create( self );
     try
        if FetchEmployee( oDataSet, iEmpleado ) then
        begin
             dFechaSal := oDataSet.FieldByName( 'CB_FEC_SAL' ).AsDateTime;
             with cdsLista do
             begin
                  FieldByName( 'CB_NIVEL1' ).AsString := oDataSet.FieldByName( 'CB_NIVEL1' ).AsString;
                  FieldByName( 'CB_NIVEL2' ).AsString := oDataSet.FieldByName( 'CB_NIVEL2' ).AsString;
                  FieldByName( 'CB_NIVEL3' ).AsString := oDataSet.FieldByName( 'CB_NIVEL3' ).AsString;
                  FieldByName( 'CB_NIVEL4' ).AsString := oDataSet.FieldByName( 'CB_NIVEL4' ).AsString;
                  FieldByName( 'CB_NIVEL5' ).AsString := oDataSet.FieldByName( 'CB_NIVEL5' ).AsString;
                  FieldByName( 'CB_NIVEL6' ).AsString := oDataSet.FieldByName( 'CB_NIVEL6' ).AsString;
                  FieldByName( 'CB_NIVEL7' ).AsString := oDataSet.FieldByName( 'CB_NIVEL7' ).AsString;
                  FieldByName( 'CB_NIVEL8' ).AsString := oDataSet.FieldByName( 'CB_NIVEL8' ).AsString;
                  FieldByName( 'CB_NIVEL9' ).AsString := oDataSet.FieldByName( 'CB_NIVEL9' ).AsString;
             end;
        end;
     finally
            FreeAndNil( oDataSet );
     end;
end;

function TdmCliente.AgregarSeguroEmpWFN: Boolean;
var
   i: Integer;
   lOk: Boolean;
   ListaEmpleados: TObjectList;
   SeguroEmpleado: TEmpleadoSeguroPariente;
   Empleado: TEmpleadoSeguroPariente;

   procedure CargarEmpleadosSeguro;
   const
        NODO_USUARIOWFN = 'PersonaID';
        NODO_EMPLEADO = 'EMPLEADO';
        NODO_PA_RELACIO = 'PA_RELACIO';
        NODO_PA_FOLIO = 'PA_FOLIO';
        NODO_EP_ASEGURA = 'EP_ASEGURA';
        NODO_EP_CERTIFI = 'EP_CERTIFI';
        NODO_EP_CFIJO = 'EP_CFIJO';
        NODO_EP_END_ALT = 'EP_END_ALT';
        NODO_EP_FEC_FIN = 'EP_FEC_FIN';
        NODO_EP_FEC_INI= 'EP_FEC_INI';
        NODO_EP_OBSERVA = 'EP_OBSERVA';
        NODO_EP_TIPO = 'EP_TIPO';
        NODO_PM_CODIGO = 'PM_CODIGO';
        NODO_PV_REFEREN = 'PV_REFEREN';
        NODO_EMPLEADOS_DATOS = 'INFO';
        NODO_EMPLEADOS_LISTA = 'EMPLEADOS';
   var
      Nodo, Info: TZetaXMLNode;
   begin
        with FXMLTools do
        begin
             Nodo := GetNode( NODO_EMPLEADOS_LISTA );
             { Preguntar si son varios empleados }
             if Assigned( Nodo ) then
             begin
                  Info := GetNode( NODO_EMPLEADOS_DATOS, Nodo );
                  while Assigned( Info ) and HasChildren( Info ) do
                  begin
                        SeguroEmpleado := TEmpleadoSeguroPariente.Create;
                        SeguroEmpleado.UsuarioWFN := TagAsInteger( GetNode( NODO_USUARIOWFN, Info ), K_EMPLEADO_NULO );
                        SeguroEmpleado.NumeroEmpleado := TagAsInteger( GetNode( NODO_EMPLEADO, Info ), K_EMPLEADO_NULO );
                        SeguroEmpleado.ParentescoID := TagAsInteger( GetNode( NODO_PA_RELACIO, Info ), K_EMPLEADO_NULO );
                        SeguroEmpleado.ParienteFolio := TagAsInteger( GetNode( NODO_PA_FOLIO, Info ), K_EMPLEADO_NULO );
                        SeguroEmpleado.AseguraTipo := TagAsInteger( GetNode( NODO_EP_ASEGURA, Info ), K_EMPLEADO_NULO );
                        SeguroEmpleado.AseguraCertifi := TagAsString( GetNode( NODO_EP_CERTIFI, Info ), VACIO );
                        SeguroEmpleado.AseguraCFijo := TagAsFloat(GetNode( NODO_EP_CFIJO, Info ),0.0);
                        SeguroEmpleado.AseguraEndoso := TagAsString( GetNode( NODO_EP_END_ALT, Info ), VACIO );
                        SeguroEmpleado.AseguraFinal := TagAsDateTime( GetNode( NODO_EP_FEC_FIN, Info), NullDateTime );
                        SeguroEmpleado.AseguraInicial := TagAsDateTime( GetNode( NODO_EP_FEC_INI, Info), NullDateTime );
                        SeguroEmpleado.AseguraObserva := TagAsString( GetNode( NODO_EP_OBSERVA, Info ), VACIO );
                        SeguroEmpleado.AseguraTipoPol := TagAsInteger( GetNode( NODO_EP_TIPO, Info ), K_EMPLEADO_NULO );
                        SeguroEmpleado.SeguroCodigo := TagAsString( GetNode( NODO_PM_CODIGO, Info ), VACIO );
                        SeguroEmpleado.SeguVigReferen := TagAsString( GetNode( NODO_PV_REFEREN, Info ), VACIO );
                        ListaEmpleados.Add(SeguroEmpleado);
                        Info := GetNextSibling( Info );
                  end
             end;
        end;
        if ( ListaEmpleados.Count = 0 ) then
           raise Exception.Create( 'No se especific� un empleado' );
   end;

   procedure AgregarDatosDatasetSGM( Empleado: TEmpleadoSeguroPariente );
   begin
        with cdsHisSGM do
        begin
             Append;
             FieldByName( 'CB_CODIGO').AsInteger := Empleado.NumeroEmpleado;
             FieldByName( 'PA_RELACIO').AsInteger := Empleado.ParentescoID;
             FieldByName( 'PA_FOLIO').AsInteger := Empleado.ParienteFolio;
             FieldByName( 'EP_ASEGURA').AsInteger := Empleado.AseguraTipo;
             FieldByName( 'EP_CERTIFI').AsString := Empleado.AseguraCertifi;
             FieldByName( 'EP_CFIJO').AsFloat := Empleado.AseguraCFijo;
             FieldByName( 'EP_END_ALT').AsString := Empleado.AseguraEndoso;
             FieldByName( 'EP_FEC_FIN').AsDateTime := Empleado.AseguraFinal;
             FieldByName( 'EP_FEC_INI').AsDateTime := Empleado.AseguraInicial;
             FieldByName( 'EP_OBSERVA').AsString := Empleado.AseguraObserva;
             FieldByName( 'EP_TIPO').AsInteger := Empleado.AseguraTipoPol;
             FieldByName( 'PM_CODIGO').AsString := Empleado.SeguroCodigo;
             FieldByName( 'PV_REFEREN').AsString := Empleado.SeguVigReferen;
             if ( FieldByName( 'EP_ASEGURA').AsInteger = Ord(taEmpleado) ) then
             begin
                  FieldByName( 'PA_RELACIO').AsInteger := 0;
                  FieldByName( 'PA_FOLIO').AsInteger := 0;
             end;
        end;
   end;

begin
     lOk := False;
     ListaEmpleados := TObjectList.Create(true);
     CargarEmpleadosSeguro;
     for i := 0 to ( ListaEmpleados.Count - 1 ) do
     begin
          Empleado := TEmpleadoSeguroPariente(ListaEmpleados[i]);
          try
             if SetEmpleadoNumero( cdsEmpleado, Empleado.NumeroEmpleado ) then
             begin
                  cdsHisSGM.Conectar;
                  AgregarDatosDatasetSGM( Empleado );///LLenar DataSet
                  cdsHisSGM.PostData;
                  if( FValidacionSGM and ValidaEnviarSGM( cdsHisSGM ) ) then //Validacion de SeguroMedico BeforePost correcto
                      lOk := True;
             end
             else
             begin
                  DataBaseError( 'No se encontr� el empleado # ' + IntToStr( Empleado.NumeroEmpleado ) );
             end;
          except
                on Error: Exception do
                begin
                     LogError( Empleado.NumeroEmpleado, 'Error al agregar seguro medico empleado: ' + Error.Message );
                end;
          end;
     end;
     Result := lOk;
     FreeAndNil(ListaEmpleados);
end;

function TdmCliente.EmpWFN_ValidarTarjetaDespensa( iCB_CODIGO :integer; sTarjetaDespensa: string; var sNroEmpleaTDespensaRepetida : string  ): boolean;
    const
         K_CONSULTA_TDESPENSA = 'select CB_CTA_VAL, CB_CODIGO from COLABORA where CB_CTA_VAL = ''%s''  and CB_CODIGO <> %d  ';
    var
       sDescTarjetaDespensa: string;
       sQuery: string;
       oConsulta: TClientDataSet;
       iEmpleadoConTarjeta : integer;
    begin
         result := False;
         oConsulta := TZetaClientDataSet.Create( self );
         sDescTarjetaDespensa := VACIO;
         sNroEmpleaTDespensaRepetida := VACIO;
         iEmpleadoConTarjeta := 0;
         sQuery := VACIO;

         try
             if ( strLleno( sTarjetaDespensa ) ) then
             begin
                   sQuery := Format( K_CONSULTA_TDESPENSA, [ sTarjetaDespensa, iCB_CODIGO  ] );
                   oConsulta.Data := ServerConsultas.GetQueryGralTodos( Empresa, sQuery );
                   if ( not oConsulta.Eof ) then
                   begin
                      iEmpleadoConTarjeta := oConsulta.FieldByName( 'CB_CODIGO' ).AsInteger;
                      sDescTarjetaDespensa := oConsulta.FieldByName( 'CB_CTA_VAL' ).AsString
                   end;

                   if ( iEmpleadoConTarjeta > 0 ) then
                   begin
                        sNroEmpleaTDespensaRepetida := Format( '%d', [iEmpleadoConTarjeta] ) ;
                        Result := False;
                   end
                   else
                    Result := TRUE;
             end
             else
             begin
                  Result := true;    // SOLO SE MANTIENE FALSE CUANDO NO VIENE VACIO Y ENCUENTRA COINCIDENCIA EN DB
             end;
         finally
            FreeAndNil ( oConsulta ) ;
         end;
    end;


//US 1111: Agregar Alta de Empleado @DLSJ 2016-02-15
function TdmCliente.AgregarEmpWFN: Boolean;
var
   lOkProcess: Boolean;
   sLogBitacora : TStringList;
   lOk, lOkValidoRegistro: Boolean;
   Registros, Registro: TZetaXMLNode;
   iEmpNumero: Integer;
   sCampo: String;
   sEmpleado, sIngresoID: String;
   dSalarioBrutoMensual: TPesos;
   dFechaIngreso: TDateTime;
   sNroEmpleaTDespensaRepetida : string;
   {*** Valores Default Alta de Empleado ***}
   procedure LimpiaGlobales;
   begin
        with cdsAltaEmpleadosWFN do
        begin
             // Datos Generales del empleado
             FieldByName( 'CB_CIUDAD' ).AsString   := VACIO;
             FieldByName( 'CB_ESTADO' ).AsString   := VACIO;
             FieldByName( 'CB_MUNICIP' ).AsString  := VACIO;
             FieldByName( 'CB_CHECA' ).AsString    := K_GLOBAL_NO;
             FieldByName( 'CB_NACION' ).AsString   := VACIO;
             FieldByName( 'CB_SEXO' ).AsString     := VACIO;
             FieldByName( 'CB_CREDENC' ).AsString  := 'A';
             FieldByName( 'CB_EDO_CIV' ).AsString  := VACIO;
             FieldByName( 'CB_VIVEEN' ).AsString   := VACIO;
             FieldByName( 'CB_VIVECON' ).AsString  := VACIO;
             FieldByName( 'CB_MED_TRA' ).AsString  := VACIO;
             FieldByName( 'CB_ESTUDIO' ).AsString  := VACIO;
             FieldByName( 'CB_AUTOSAL' ).AsString  := K_GLOBAL_NO;
             FieldByName( 'CB_ZONA_GE' ).AsString  := 'A';
             FieldByName( 'CB_PATRON' ).AsString   := VACIO;
             FieldByName( 'CB_SALARIO' ).AsFloat   := 0;
             FieldByName( 'CB_CONTRAT' ).AsString  := VACIO;
             FieldByName( 'CB_PUESTO' ).AsString   := VACIO;
             FieldByName( 'CB_CLASIFI' ).AsString  := VACIO;
             FieldByName( 'CB_TURNO' ).AsString    := VACIO;
             FieldByName( 'CB_TABLASS' ).AsString  := VACIO;
             FieldByName( 'CB_NIVEL1' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL2' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL3' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL4' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL5' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL6' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL7' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL8' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL9' ).AsString   := VACIO;
             FieldByName( 'CB_NOMINA' ).AsInteger  := Ord( tpSemanal );
             FieldByName( 'CB_TDISCAP' ).AsInteger  := Ord( disSinDefinir );

             // Adicionales de Texto
             FieldByName( 'CB_G_TEX_1' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_2' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_3' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_4' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_5' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_6' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_7' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_8' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_9' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX10' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX11' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX12' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX13' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX14' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX15' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX16' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX17' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX18' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX19' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX20' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX21' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX22' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX23' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX24' ).AsString := VACIO;

             // Adicionales de n�mero
             FieldByName( 'CB_G_NUM_1' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_2' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_3' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_4' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_5' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_6' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_7' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_8' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_9' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM10' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM11' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM12' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM13' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM14' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM15' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM16' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM17' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM18' ).AsFloat := 0;

             // Adicionales de Tablas
             FieldByName( 'CB_G_TAB_1' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_2' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_3' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_4' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_5' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_6' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_7' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_8' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_9' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB10' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB11' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB12' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB13' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB14' ).AsString := VACIO;

             // Adicionales logicos
             FieldByName( 'CB_G_LOG_1' ).AsString := K_GLOBAL_NO;
             FieldByName( 'CB_G_LOG_2' ).AsString := K_GLOBAL_NO;
             FieldByName( 'CB_G_LOG_3' ).AsString := K_GLOBAL_NO;
             FieldByName( 'CB_G_LOG_4' ).AsString := K_GLOBAL_NO;
             FieldByName( 'CB_G_LOG_5' ).AsString := K_GLOBAL_NO;
             FieldByName( 'CB_G_LOG_6' ).AsString := K_GLOBAL_NO;
             FieldByName( 'CB_G_LOG_7' ).AsString := K_GLOBAL_NO;
             FieldByName( 'CB_G_LOG_8' ).AsString := K_GLOBAL_NO;

             // Adicionales de Fecha
             FieldByName( 'CB_G_FEC_1' ).AsDateTime := NullDateTime;
             FieldByName( 'CB_G_FEC_2' ).AsDateTime := NullDateTime;
             FieldByName( 'CB_G_FEC_3' ).AsDateTime := NullDateTime;
             FieldByName( 'CB_G_FEC_4' ).AsDateTime := NullDateTime;
             FieldByName( 'CB_G_FEC_5' ).AsDateTime := NullDateTime;
             FieldByName( 'CB_G_FEC_6' ).AsDateTime := NullDateTime;
             FieldByName( 'CB_G_FEC_7' ).AsDateTime := NullDateTime;
             FieldByName( 'CB_G_FEC_8' ).AsDateTime := NullDateTime;
             FieldByName( 'CB_G_FEC_8' ).AsDateTime := NullDateTime;

             //Salario Bruto Mensual
             FieldByName( 'CB_SALARIO_BRUTO' ).AsFloat := 0;
             FieldByName( 'IngresoID' ).AsString := VACIO;
        end;
   end;

   function EsCampoChar( const sCampo: String; var iSize: Byte ): Boolean;
   var
      i: Byte;
   begin
        Result:= FALSE;
        iSize := 0;
        for i:= 1 to K_QTY_CHAR do
            if ( aCamposChar[i,1] = sCampo ) then
            begin
                 Result:= TRUE;
                 iSize:= StrToInt( aCamposChar[i,2] );
                 Break;
            end;
   end;

   function EsCampoVarChar( const sCampo: String; var iSize: Byte ): Boolean;
   var
      i: Byte;
   begin
        Result:= FALSE;
        iSize:= 0;
        for i:= 1 to K_QTY_VARCHAR do
            if ( aCamposVarChar[i,1] = sCampo ) then
            begin
                 Result:= TRUE;
                 iSize:= StrToInt( aCamposVarChar[i,2] );
                 Break;
            end;
   end;

   function EsCampoNumerico( const sCampo: String ): Boolean;
   var
      i: Byte;
   begin
        Result:= FALSE;
        for i:= 1 to K_QTY_NUMEROS do
            if ( aCamposNumericos[i] = sCampo ) then
            begin
                 Result:= TRUE;
                 Break;
            end;
   end;

   function EsCampoDecimal( const sCampo: String ): Boolean;
   var
      i: Byte;
   begin
        Result:= FALSE;
        for i:= 1 to K_QTY_DECIMALES do
            if ( aCamposDecimales[i] = sCampo ) then
            begin
                 Result:= TRUE;
                 Break;
            end;
   end;
   
   function EsCampoFecha( const sCampo: String ): Boolean;
   var
      i: Byte;
   begin
        Result:= FALSE;
        for i:= 1 to K_QTY_FECHA do
            if ( aCamposFecha[i] = sCampo ) then
            begin
                 Result:= TRUE;
                 Break;
            end;
   end;

   function EsCampoBooleano( const sCampo: String ): Boolean;
   var
      i: Byte;
   begin
        Result:= FALSE;
        for i:= 1 to K_QTY_BOOLEANOS do
           if ( aCamposBooleano[i] = sCampo ) then
           begin
                Result:= TRUE;
                Break;
           end;
   end;

   function EsCampoValido( sCampo : String ): Boolean;
   var
      Long : Byte;
   begin
        Result := ( EsCampoChar( sCampo, Long ) or
                  EsCampoVarChar( sCampo, Long ) or
                  EsCampoNumerico( sCampo ) or
                  EsCampoFecha( sCampo ) or
                  EsCampoBooleano( sCampo ) or EscampoDecimal( sCampo ) ) ;
   end;
   
   {*** Asigna Valores al DataSet ***}
   function Asigna( sCampo: String; Registro: TZetaXMLNode ): Boolean;
   var
      iSize: Byte; 
   begin
        Result := False;
        with cdsAltaEmpleadosWFN, FXMLTools do
        begin
             if ( EsCampoValido( sCampo )  ) then
             begin
                  //Tipos de datos que se leen del XML
                  if ( EsCampoFecha( sCampo ) ) then
                     FieldByName( sCampo ).AsDateTime := ZetaCommonTools.scandate('yyyymmdd', Trim( AttributeAsString( Registro, sCampo, VACIO ) ) )
                  else if ( EscampoVarChar( sCampo, iSize ) ) then
                       FieldByName( sCampo ).AsString := Trim( AttributeAsString( Registro, sCampo, VACIO ) )
                  else if ( EscampoChar( sCampo, iSize ) ) then
                       FieldByName( sCampo ).AsString := Trim( AttributeAsString( Registro, sCampo, VACIO ) )
                  else if ( EsCampoNumerico( sCampo ) ) then
                       FieldByName( sCampo ).AsInteger := AttributeAsInteger( Registro, sCampo, 0 )
                  else if ( EsCampoBooleano( sCampo ) ) then
                       FieldByName( sCampo ).AsString := Trim( AttributeAsString( Registro, sCampo, K_GLOBAL_NO ) )
                  else if ( EsCampoDecimal( sCampo ) ) then
                       FieldByName( sCampo ).AsFloat := AttributeAsFloat( Registro, sCampo, 0.00 );
                  Result := True;
             end
             else
             begin
                  sLogBitacora.Add(  IntToStr( FEmpleado ) +  '|' + 'Error al agregar empleado: el campo ' + sCampo + ' no es correcto');
                  Result := False;
             end;
        end;
   end;
   
   procedure CargaParametros( Registro: TZetaXMLNode );
   var
      Atributos: IXMLNodeList;
      i: Integer;
   begin
        with FXMLTools do
        begin
             while Assigned( Registro ) do
             begin
                  Atributos := Registro.AttributeNodes;
                  for i:= 0 to Atributos.Count - 1 do
                  begin
                       sCampo := Atributos.Get(i).NodeName;//Name Atributo XML
                       Asigna( sCampo, Registro );
                  end;
                  break;
             end;
         end;
   end;
   
   {*** Agregar Campos al Dataset para realizar el alta de empleado ***}
   procedure AgregaCamposDataset;
   begin
        with cdsAltaEmpleadosWFN do
        begin
             Close;
             AddStringField( 'COD_PERCEPS', 10 );
             AddStringField( 'NOM_MADRE', K_ANCHO_DESCRIPCION );
             AddStringField( 'NOM_PADRE', K_ANCHO_DESCRIPCION );
             AddFloatField( 'CB_SALARIO_BRUTO' );
             AddStringField( 'IngresoID', K_ANCHO_DESCRIPCION ); //IngresoID
             CreateTempDataset;
        end;
   end;

   {*** Definicion de campos del DataSet para la Alta de Empleados ***}
   procedure DefineEstructuraEmpleado;
   var
      oDatos: OleVariant;
   begin
        with cdsAltaEmpleadosWFN do
        begin
             Servidor.GetEmpleado( Empresa, 0, oDatos );
             Data := oDatos;
        end;
   end;

   procedure IniciaEstructuraEmpleados;
   begin
        with cdsAltaEmpleadosWFN do
        begin
             if Active then
                EmptyDataSet
             else
             begin
                  DefineEstructuraEmpleado;
                  AgregaCamposDataset;//Agrega campos que se requieren para realizar la alta de empleado
             end;
        end;
   end;



   


   {*** Validacion de los atributos del XML por cada Registro de Empleado a procesar ***}
   function ValidaAtributosXML: Boolean;
   const
        K_MENSAJE_ERROR = '%s%s';
   var
      lContinuar: Boolean;
      sEmpleado, sSexo, sPuesto, sTemporal, sRFC, sCURP: String;
      dSalarioBrutoMensual: Currency;
   begin
        lContinuar := TRUE;
        with cdsAltaEmpleadosWFN do
        begin
             {*** Validaciones con respecto al PDF ***}
             sEmpleado :=  VACIO;

             //Validacion apellido paterno
             if ( FieldByName( 'CB_CODIGO' ).AsInteger <= 0 ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'N�mero de empleado debe ser mayor a cero, no se puede realizar la alta.' ] ) );
             end;

             if StrVacio( FieldByName( 'CB_APE_PAT' ).AsString ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'El apellido paterno del empleado est� vac�o.' ] ) );
             end;

             //Validacion de nombre de empleado
                  if StrVacio( FieldByName( 'CB_NOMBRES' ).AsString ) then
                  begin
                       lContinuar := FALSE;
                       sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'El Nombre del empleado est� vac�o.' ] ) );
                  end;

             //Validacion Sexo Empleado
             sSexo := FieldByName( 'CB_SEXO' ).AsString;
             if StrVacio( sSexo ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'El campo sexo est� vac�o.' ] ) );
             end
             else if ( sSexo = 'M' ) or ( sSexo = 'F' ) then
                  FieldByName( 'CB_SEXO' ).AsString := sSexo
             else
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'El campo de sexo no es v�lido. Valores aceptados: [F o M].' ] ) );
             end;

             //Validacion de Fecha de nacimiento
             if ( FieldByName( 'CB_FEC_NAC' ).AsDateTime = NullDateTime ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'La fecha de nacimiento del empleado es inv�lida.'] ) );
             end;
             //Validacion del RFC
             sTemporal := strTransAll( FieldByName( 'CB_RFC' ).AsString, '-', '' );
             sTemporal := Trim( sTemporal );
             if strVacio( sTemporal ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'RFC de empleado est� vac�o.' ] ) );
             end;

             //Validacion Seguro Social
             sTemporal := strTransAll( FieldByName( 'CB_SEGSOC' ).AsString, '-', '' );
             sTemporal := Trim( sTemporal );
             if strVacio( sTemporal ) then
             begin
                  lContinuar := FALSE;
                  LogError( FEmpleado, Format( K_MENSAJE_ERROR, [ sEmpleado, 'Seguro Social de empleado est� vac�o.' ] ) );
             end;

             //Validacion CURP
             sTemporal := strTransAll( FieldByName( 'CB_CURP' ).AsString, '-', '' );
             sTemporal := Trim( sTemporal );
             if strVacio( sTemporal ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'CURP de empleado est� vac�o.' ] ) );
             end;

             //Validacion Fecha Ingreso
             if ( FieldByName( 'CB_FEC_ING' ).AsDateTime = NullDateTime ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'La fecha de ingreso del empleado es inv�lida.' ] ) );
             end;
             //Validacion Fecha de Antiguedad
             if ( FieldByName( 'CB_FEC_ANT' ).AsDateTime = NullDateTime ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'La fecha de antig�edad del empleado es inv�lida.' ] ) );
             end;
             //Validacion fecha de contrato
             if ( FieldByName( 'CB_FEC_CON' ).AsDateTime = NullDateTime ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'La fecha de contrato del empleado es inv�lida.' ] ) );
             end;

             //Validar el contrato del empleado
             if StrVacio( FieldByName( 'CB_CONTRAT' ).AsString ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'Valor global para el tipo de contrato del empleado est� vac�o.' ] ) );
             end;
             //Validar el puesto del empleado
             if StrVacio( FieldByName( 'CB_PUESTO' ).AsString ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'C�digo del puesto del empleado est� vac�o.' ] ) );
             end;

             // CB_CLASIFI.
             if StrVacio( FieldByName( 'CB_CLASIFI' ).AsString ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'Clasificaci�n del empleado est� vac�o.' ] ) );
             end;

             //Validacion de CB_TURNO
             if StrVacio( FieldByName( 'CB_TURNO' ).AsString ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'El turno del empleado se encuentra vac�o.' ] ) );
             end;

             //Validacion de CB_PATRON
             if StrVacio( FieldByName( 'CB_PATRON' ).AsString ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'El registro patronal del empleado se encuentra vac�o.' ] ) );
             end;

             if StrVacio( FieldByName( 'CB_ZONA_GE' ).AsString ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'Valor para la zona geogr�fica del empleado se encuentra vac�o.'] ) );
             end;

             if StrVacio( FieldByName( 'CB_TABLASS' ).AsString ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'Valor para la tabla de prestaciones del empleado se encuentra vac�o.' ] ) );
             end;

             if ( FieldByName( 'CB_NOMINA' ).AsInteger <> Ord( tpSemanal ) ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'El tipo de n�mina del empleado no puede ser menor o igual a cero.' ] ) );
             end;
             {************************************************** VALIDACIONES PRESBAJA  ************************}
        end;
        Result := lContinuar;
   end;

   function AgregarCambioSalarioBrutoMensual( iCB_CODIGO : integer; fSalarioBrutoMensual : TPesos;  dFecha : TDateTime ): Boolean;
   const
        K_T_CAMSAL = 'CAMSAL';
   var
      ErrorCount: Integer;
   begin
        Result := TRUE;
        try
           if FetchEmployee( cdsLista, iCB_CODIGO ) then
           begin
                with cdsLista do
                begin
                     Data := ServerRecursos.GetEditHisKardex( Empresa, iCB_CODIGO, 0, VACIO );  // Llenar DataSet Vacio con formato de campos de KARDEX
                     Append;
                     FieldByName( 'CB_TIPO' ).AsString := K_T_CAMSAL;
                     FieldByName( 'CB_CODIGO').AsInteger := iCB_CODIGO;
                     FieldByName( 'CB_COMENTA' ).AsString :=  'Salario Bruto Mensual por WFN';
                     FieldByName( 'CB_NOTA' ).AsString := 'Agregado por WFN';
                     FieldByName( 'CB_MONTO' ).AsFloat := fSalarioBrutoMensual;
                     FieldByName( 'CB_FECHA' ).AsDateTime := dFecha;
                     PostData;
                     OnReconcileError := ReconcileError;
                     FServerRecursos := nil;
                     if Reconciliar(ServerRecursos.GrabaHisKardex ( Empresa, DeltaNull, VACIO, ErrorCount ) ) then
                     begin
                          lOk := True;
                     end
                     else
                     begin
                          sLogBitacora.append( Self.Status );
                     end;
                end;
           end
           else
           begin
                Result := FALSE;
                sLogBitacora.append( 'No se encontr� el empleado # ' + IntToStr( iCB_CODIGO ) );
           end;
        except
              on Error: Exception do
              begin
                   Result := FALSE;
                   sLogBitacora.append( 'Error al agregar cambio de salario bruto mensual: ' + Error.Message );
              end;
        end;
   end;

   procedure CrearXMLRespuestaErrores( var Respuesta: TZetaXMLNode );
   var
      i, iErrores: Integer;
      oError: TZetaXMLNode;
   begin
        iErrores := sLogBitacora.Count;
        FXMLTools.WriteAttributeInteger( 'Errores', iErrores, Respuesta );
        for i := 0 to sLogBitacora.Count - 1 do
        begin
             oError := Respuesta.AddChild('Error');
             oError.NodeValue := sLogBitacora.Strings[ i ];
        end;
   end;

   function CrearXMLRespuestaWFN: String;
   var
      FXMLToolsWFN: TdmXMLTools;
      oRespuestaWFN, oMovimiento, oEmpresa, oRegistros, oRegistro, Respuesta: TZetaXMLNode;
   begin
        try
           FXMLToolsWFN := TdmXMLTools.Create( Self );
           with FXMLToolsWFN do
           begin
                XMLAsText := '<DATOS></DATOS>';
                oRespuestaWFN := XMLDocument.DocumentElement;
                oMovimiento := oRespuestaWFN.AddChild('MOV3');
                oMovimiento.NodeValue := K_TW_EMP_AGREGAR;
                oEmpresa := oRespuestaWFN.AddChild('EMPRESA');
                oEmpresa.NodeValue := cdsCompany.FieldByName('CM_CODIGO').AsString;
                oRegistros := oRespuestaWFN.AddChild('REGISTROS');
                oRegistro := oRegistros.AddChild('REGISTRO');
                WriteAttributeString( 'IngresoID', sIngresoID, oRegistro );
                WriteAttributeString( 'CB_CODIGO', sEmpleado, oRegistro );
                Respuesta := oRegistro.AddChild('Respuesta');
                CrearXMLRespuestaErrores( Respuesta );
                Result := XMLDocument.XML.Text;
           end;
        finally
               FreeAndNil( FXMLToolsWFN );
        end;
   end;

   function CrearPipesRespuestaWFN: String;
   const
        K_PIPE = '|';
   var
      i: Integer;
      sRespuesta: String;
   begin
        for i := 0 to sLogBitacora.Count - 1 do
        begin
             sRespuesta := sRespuesta + K_PIPE + sLogBitacora.Strings[ i ];
        end;
        Result := sRespuesta;
   end;

   procedure ObtenerDatosProceso( var sEmpleado, sIngresoID: String );
   begin
        with FXMLTools do
        begin
             Registros := GetNode( 'REGISTROS' );
             Registro := GetNode( 'REGISTRO', Registros );
             sEmpleado := Registro.Attributes['CB_CODIGO'];
             sIngresoID := Registro.Attributes['IngresoID'];
        end;
   end;
begin
     lOkProcess := FALSE;
     lOkValidoRegistro := FALSE;
     sEmpleado := VACIO;
     sIngresoID := VACIO;
     dSalarioBrutoMensual := 0;
     dFechaIngreso := 0;
     sNroEmpleaTDespensaRepetida := VACIO;
     sLogBitacora := TStringList.Create();
     try
        lOk := TRUE;
        ObtenerDatosProceso( sEmpleado, sIngresoID );
        if not Autorizacion.EsDemo then
        begin
             with FXMLTools do
             begin
                  Registros := GetNode( 'REGISTROS' );
                  if Assigned ( Registros ) then
                  begin
                       Registro := GetNode( 'REGISTRO', Registros );
                       if Assigned( Registro ) then
                       begin
                            IniciaEstructuraEmpleados;
                            with cdsAltaEmpleadosWFN do
                            begin
                                 Append;
                                 LimpiaGlobales;
                                 CargaParametros( Registro );
                                 if not ValidaAtributosXML then
                                    cdsAltaEmpleadosWFN.Cancel
                                 else
                                 begin
	                                    if(EmpWFN_ValidarTarjetaDespensa( 0, cdsAltaEmpleadosWFN.FieldByName( 'CB_CTA_VAL' ).AsString, sNroEmpleaTDespensaRepetida)) then
                                      begin
                                            if SetEmpleadoNumero( cdsEmpleado, cdsAltaEmpleadosWFN.FieldByName( 'CB_CODIGO' ).AsInteger ) then
                                            begin
                                                 lOk := TRUE;
                                                 lOkProcess := TRUE;
                                            end
                                            else
                                            begin
                                                 ProcesoAltaEmpleadosWFN( lOkProcess, sLogBitacora );
                                            end;
                                      end
                                      else
                                      begin
                                           if sNroEmpleaTDespensaRepetida <> VACIO then
                                           begin
                                                 raise Exception.Create( 'El empleado '+ sNroEmpleaTDespensaRepetida+' tiene asignado ese n�mero de cuenta de tarjeta despensa ('+cdsAltaEmpleadosWFN.FieldByName( 'CB_CTA_VAL' ).AsString+').' );
                                                 result := false;
                                           end
                                           else
                                           begin
                                                 raise Exception.Create( 'Alta de Empleado Ya existe ese n�mero de cuenta de tarjeta despensa ('+cdsAltaEmpleadosWFN.FieldByName( 'CB_CTA_VAL' ).AsString+') en otro empleado' );
                                                 result := false;
                                           end;

                                      end;
                                 end;
                            end;
                       end
                       else
                       begin
                            sLogBitacora.Append( 'Error al agregar empleado: Nodo <REGISTRO> no especificado' );
                            lOk := FALSE;
                       end;
                  end
                  else
                  begin
                       sLogBitacora.Append( 'Error al agregar empleado: Nodo <REGISTROS> no especificado' );
                       lOk := FALSE;
                  end;
             end;
        end
        else
            sLogBitacora.Append( 'Empresa en modo DEMO, no se realiza el alta.' );

        if ( lOkProcess and lOk ) then
        begin
             {*** Cambio de Salario Bruto Mensual ***}
             dSalarioBrutoMensual := cdsAltaEmpleadosWFN.FieldByName( 'CB_SALARIO_BRUTO' ).AsFloat;
             dFechaIngreso := cdsAltaEmpleadosWFN.FieldByName( 'CB_FEC_ING' ).AsDateTime;
             iEmpNumero := cdsAltaEmpleadosWFN.FieldByName( 'CB_CODIGO' ).AsInteger;
             if ( dSalarioBrutoMensual > 0 ) then
             begin
                  if AgregarCambioSalarioBrutoMensual( iEmpNumero, dSalarioBrutoMensual, dFechaIngreso ) then
                     LogError( 0, CrearXMLRespuestaWFN ) {*** Respuesta XML WFN ***}
                  else
                  begin
                       LogError( 0, CrearPipesRespuestaWFN ); {*** Respuesta XML WFN ***}
                       lOk := FALSE;
                  end;
             end
             else
                 LogError( 0, CrearXMLRespuestaWFN ); {*** Respuesta XML WFN ***}
        end
        else
            LogError( 0, CrearPipesRespuestaWFN ); {*** Respuesta XML WFN ***}
     except
           on Error: Exception do
           begin
                sLogBitacora.Append( Error.Message );
                LogError( 0, CrearPipesRespuestaWFN ); {*** Respuesta XML WFN ***}
                lOk := FALSE;
           end;
     end;
     Result := lOk;
end;

procedure TdmCliente.ProcesoAltaEmpleadosWFN( var lOkProcess: Boolean; var Texto: TStringList );
var
     Parametros : TZetaParams;
     sObservaciones : String;
begin
     Parametros := TZetaParams.Create( Self );
     sObservaciones := '';
     try
        with Parametros do
        begin
             AddInteger( 'Formato', Ord( faASCIIFijo ) );               // No afecta, pero debe incluirse
             AddInteger( 'FormatoImpFecha', Ord( ifDDMMYYYYs ) );       // No afecta, pero debe incluirse
             AddString( 'Archivo', VACIO );                             // No se requiere
             AddMemo( 'Observaciones', sObservaciones );
             AddBoolean( 'lRegKardex', FALSE );                         // No se agregar�n registros de Kardex
             AddDate( 'Fecha', NullDateTime );                          // No se requiere por que no se incluri�n registros de kardex
        end;
        FServerRecursos := nil;
        EscribeBitacora( ServerRecursos.ImportarAltasLista( dmCliente.Empresa, cdsAltaEmpleadosWFN.Lista, Parametros.VarValues ), Texto );
        LeerMensajeBitacora( lOkProcess, Texto );

     finally
            FreeAndNil(Parametros);
     end;
end;


//----------------------------------------------------REINGRESO EMPLEADO------------------------------------------------------------------------------
function TdmCliente.ReingresoEmpWFN: Boolean;
var
   lOkProcess: Boolean;
   sLogBitacora : TStringList;
   lOk, lOkValidoRegistro: Boolean;
   Registros, Registro: TZetaXMLNode;
   iEmpNumero: Integer;
   sCampo: String;
   sEmpleado, sIngresoID: String;
   dSalarioBrutoMensual: TPesos;
   dFechaReIngreso: TDateTime;
   sVerfecha, sNroEmpleaTDespensaRepetida: string;
   Texto : TStringList;

   Parametros : TZetaParams;
   sObservaciones, sErrores : String;
   FListaPercepFijas : String;
   ErrorCount : integer;
   FOtrosDatos : Variant;
   oDatos: OleVariant;
   iEmpleado: Integer;

   {*** Valores Default Alta de Empleado ***}

   procedure SetDatosReingreso;
   begin
        with dmCliente, dmCliente.cdsEditHisKardex {dmCliente.cdsEditHisKardex} do
        begin
             FieldByName( 'CB_CODIGO' ).AsInteger := FEmpleado;
             FieldByName( 'CB_REINGRE' ).AsString := K_GLOBAL_SI;
             FieldByName( 'CB_FECHA' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_FEC_ING' ).AsDateTime;
             FieldByName( 'CB_AUTOSAL' ).AsString  := cdsAltaEmpleadosWFN.FieldByName( 'CB_AUTOSAL' ).AsString;;
             FieldByName( 'CB_CLASIFI' ).AsString  := cdsAltaEmpleadosWFN.FieldByName( 'CB_CLASIFI' ).AsString;
             FieldByName( 'CB_CONTRAT' ).AsString  := cdsAltaEmpleadosWFN.FieldByName( 'CB_CONTRAT' ).AsString;
             FieldByName( 'CB_TURNO' ).AsString    := cdsAltaEmpleadosWFN.FieldByName( 'CB_TURNO' ).AsString;
             FieldByName( 'CB_MOT_BAJ' ).AsString  := cdsAltaEmpleadosWFN.FieldByName( 'CB_MOT_BAJ' ).AsString;
             FieldByName( 'CB_PATRON' ).AsString   := cdsAltaEmpleadosWFN.FieldByName( 'CB_PATRON' ).AsString;
             FieldByName( 'CB_SALARIO' ).AsFloat   := cdsAltaEmpleadosWFN.FieldByName( 'CB_SALARIO' ).AsFloat;
             FieldByName( 'CB_TABLASS' ).AsString  := cdsAltaEmpleadosWFN.FieldByName( 'CB_TABLASS' ).AsString;
             FieldByName( 'CB_ZONA_GE' ).AsString  := cdsAltaEmpleadosWFN.FieldByName( 'CB_ZONA_GE' ).AsString;
             FieldByName( 'CB_NOMINA' ).AsInteger  := cdsAltaEmpleadosWFN.FieldByName( 'CB_NOMINA' ).AsInteger;
             FieldByName( 'CB_FEC_COV' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_FEC_COV' ).AsDateTime;

             if strLleno( cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL1' ).AsString ) then
             begin
                FieldByName( 'CB_NIVEL1' ).AsString   := cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL1' ).AsString
             end;

             if strLleno( cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL2' ).AsString ) then
             begin
                FieldByName( 'CB_NIVEL2' ).AsString   := cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL2' ).AsString
             end;

             if strLleno( cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL3' ).AsString ) then
             begin
                FieldByName( 'CB_NIVEL3' ).AsString   := cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL3' ).AsString
             end;

             if strLleno( cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL4' ).AsString ) then
             begin
                FieldByName( 'CB_NIVEL4' ).AsString   := cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL4' ).AsString
             end;

             if strLleno( cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL5' ).AsString ) then
             begin
                FieldByName( 'CB_NIVEL5' ).AsString   := cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL5' ).AsString
             end;    

             if strLleno( cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL6' ).AsString ) then
             begin
                FieldByName( 'CB_NIVEL6' ).AsString   := cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL6' ).AsString
             end;    

             if strLleno( cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL7' ).AsString ) then
             begin
                FieldByName( 'CB_NIVEL7' ).AsString   := cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL7' ).AsString
             end;    

             if strLleno( cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL8' ).AsString ) then
             begin
                FieldByName( 'CB_NIVEL8' ).AsString   := cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL8' ).AsString
             end;    

             if strLleno( cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL9' ).AsString ) then
             begin
                FieldByName( 'CB_NIVEL9' ).AsString   := cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL9' ).AsString
             end;
             FieldByName( 'CB_FEC_ING' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_FEC_ING' ).AsDateTime;
             FieldByName( 'CB_FEC_ANT' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_FEC_ANT' ).AsDateTime;
             if FieldByName( 'CB_FEC_ANT' ).AsDateTime = NullDateTime then
                FieldByName( 'CB_FEC_ANT' ).AsDateTime := FieldByName( 'CB_FEC_ANT' ).AsDateTime;
             FieldByName( 'CB_FEC_CON' ).AsDateTime :=  cdsAltaEmpleadosWFN.FieldByName( 'CB_FEC_CON' ).AsDateTime;
             FieldByName( 'CB_PUESTO' ).AsString := cdsAltaEmpleadosWFN.FieldByName( 'CB_PUESTO' ).AsString;
        end;
   end;

   procedure LimpiaGlobales;
   begin
        with cdsAltaEmpleadosWFN do
        begin
             // Datos Generales del empleado
             FieldByName( 'CB_CIUDAD' ).AsString   := VACIO;
             FieldByName( 'CB_ESTADO' ).AsString   := VACIO;
             FieldByName( 'CB_MUNICIP' ).AsString  := VACIO;
             FieldByName( 'CB_CHECA' ).AsString    := K_GLOBAL_NO;
             FieldByName( 'CB_NACION' ).AsString   := VACIO;
             FieldByName( 'CB_SEXO' ).AsString     := VACIO;
             FieldByName( 'CB_CREDENC' ).AsString  := 'A';
             FieldByName( 'CB_EDO_CIV' ).AsString  := VACIO;
             FieldByName( 'CB_VIVEEN' ).AsString   := VACIO;
             FieldByName( 'CB_VIVECON' ).AsString  := VACIO;
             FieldByName( 'CB_MED_TRA' ).AsString  := VACIO;
             FieldByName( 'CB_ESTUDIO' ).AsString  := VACIO;
             FieldByName( 'CB_AUTOSAL' ).AsString  := K_GLOBAL_NO;
             FieldByName( 'CB_ZONA_GE' ).AsString  := 'A';
             FieldByName( 'CB_PATRON' ).AsString   := VACIO;
             FieldByName( 'CB_SALARIO' ).AsFloat   := 0;
             FieldByName( 'CB_CONTRAT' ).AsString  := VACIO;
             FieldByName( 'CB_PUESTO' ).AsString   := VACIO;
             FieldByName( 'CB_CLASIFI' ).AsString  := VACIO;
             FieldByName( 'CB_TURNO' ).AsString    := VACIO;
             FieldByName( 'CB_TABLASS' ).AsString  := VACIO;
             FieldByName( 'CB_NIVEL1' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL2' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL3' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL4' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL5' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL6' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL7' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL8' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL9' ).AsString   := VACIO;
             FieldByName( 'CB_NOMINA' ).AsInteger  := Ord( tpSemanal );
             FieldByName( 'CB_TDISCAP' ).AsInteger  := Ord( disSinDefinir );

             // Adicionales de Texto
             FieldByName( 'CB_G_TEX_1' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_2' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_3' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_4' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_5' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_6' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_7' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_8' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_9' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX10' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX11' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX12' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX13' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX14' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX15' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX16' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX17' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX18' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX19' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX20' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX21' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX22' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX23' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX24' ).AsString := VACIO;

             // Adicionales de n�mero
             FieldByName( 'CB_G_NUM_1' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_2' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_3' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_4' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_5' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_6' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_7' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_8' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_9' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM10' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM11' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM12' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM13' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM14' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM15' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM16' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM17' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM18' ).AsFloat := 0;

             // Adicionales de Tablas
             FieldByName( 'CB_G_TAB_1' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_2' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_3' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_4' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_5' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_6' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_7' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_8' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_9' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB10' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB11' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB12' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB13' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB14' ).AsString := VACIO;

             // Adicionales logicos
             FieldByName( 'CB_G_LOG_1' ).AsString := K_GLOBAL_NO;
             FieldByName( 'CB_G_LOG_2' ).AsString := K_GLOBAL_NO;
             FieldByName( 'CB_G_LOG_3' ).AsString := K_GLOBAL_NO;
             FieldByName( 'CB_G_LOG_4' ).AsString := K_GLOBAL_NO;
             FieldByName( 'CB_G_LOG_5' ).AsString := K_GLOBAL_NO;
             FieldByName( 'CB_G_LOG_6' ).AsString := K_GLOBAL_NO;
             FieldByName( 'CB_G_LOG_7' ).AsString := K_GLOBAL_NO;
             FieldByName( 'CB_G_LOG_8' ).AsString := K_GLOBAL_NO;

             // Adicionales de Fecha
             FieldByName( 'CB_G_FEC_1' ).AsDateTime := NullDateTime;
             FieldByName( 'CB_G_FEC_2' ).AsDateTime := NullDateTime;
             FieldByName( 'CB_G_FEC_3' ).AsDateTime := NullDateTime;
             FieldByName( 'CB_G_FEC_4' ).AsDateTime := NullDateTime;
             FieldByName( 'CB_G_FEC_5' ).AsDateTime := NullDateTime;
             FieldByName( 'CB_G_FEC_6' ).AsDateTime := NullDateTime;
             FieldByName( 'CB_G_FEC_7' ).AsDateTime := NullDateTime;
             FieldByName( 'CB_G_FEC_8' ).AsDateTime := NullDateTime;
             FieldByName( 'CB_G_FEC_8' ).AsDateTime := NullDateTime;

             //Salario Bruto Mensual
             FieldByName( 'CB_SALARIO_BRUTO' ).AsFloat := 0;
             FieldByName( 'IngresoID' ).AsString := VACIO;
        end;
   end;

   function EsCampoChar( const sCampo: String; var iSize: Byte ): Boolean;
   var
      i: Byte;
   begin
        Result:= FALSE;
        iSize := 0;
        for i:= 1 to K_QTY_CHAR do
            if ( aCamposChar[i,1] = sCampo ) then
            begin
                 Result:= TRUE;
                 iSize:= StrToInt( aCamposChar[i,2] );
                 Break;
            end;
   end;

   function EsCampoVarChar( const sCampo: String; var iSize: Byte ): Boolean;
   var
      i: Byte;
   begin
        Result:= FALSE;
        iSize:= 0;
        for i:= 1 to K_QTY_VARCHAR do
            if ( aCamposVarChar[i,1] = sCampo ) then
            begin
                 Result:= TRUE;
                 iSize:= StrToInt( aCamposVarChar[i,2] );
                 Break;
            end;
   end;

   function EsCampoNumerico( const sCampo: String ): Boolean;
   var
      i: Byte;
   begin
        Result:= FALSE;
        for i:= 1 to K_QTY_NUMEROS do
            if ( aCamposNumericos[i] = sCampo ) then
            begin
                 Result:= TRUE;
                 Break;
            end;
   end;

   function EsCampoDecimal( const sCampo: String ): Boolean;
   var
      i: Byte;
   begin
        Result:= FALSE;
        for i:= 1 to K_QTY_DECIMALES do
            if ( aCamposDecimales[i] = sCampo ) then
            begin
                 Result:= TRUE;
                 Break;
            end;
   end;
   
   function EsCampoFecha( const sCampo: String ): Boolean;
   var
      i: Byte;
   begin
        Result:= FALSE;
        for i:= 1 to K_QTY_FECHA do
            if ( aCamposFecha[i] = sCampo ) then
            begin
                 Result:= TRUE;
                 Break;
            end;
   end;

   function EsCampoBooleano( const sCampo: String ): Boolean;
   var
      i: Byte;
   begin
        Result:= FALSE;
        for i:= 1 to K_QTY_BOOLEANOS do
           if ( aCamposBooleano[i] = sCampo ) then
           begin
                Result:= TRUE;
                Break;
           end;
   end;

   function EsCampoValido( sCampo : String ): Boolean;
   var
      Long : Byte;
   begin
        Result := ( EsCampoChar( sCampo, Long ) or
                  EsCampoVarChar( sCampo, Long ) or
                  EsCampoNumerico( sCampo ) or
                  EsCampoFecha( sCampo ) or
                  EsCampoBooleano( sCampo ) or EscampoDecimal( sCampo ) ) ;
   end;
   
   {*** Asigna Valores al DataSet ***}
   function Asigna( sCampo: String; Registro: TZetaXMLNode ): Boolean;
   var
      iSize: Byte; 
   begin
        Result := False;
        with cdsAltaEmpleadosWFN, FXMLTools do
        begin
             if ( EsCampoValido( sCampo )  ) then
             begin
                  //Tipos de datos que se leen del XML
                  if ( EsCampoFecha( sCampo ) ) then
                     FieldByName( sCampo ).AsDateTime := ZetaCommonTools.scandate('yyyymmdd', Trim( AttributeAsString( Registro, sCampo, VACIO ) ) )
                  else if ( EscampoVarChar( sCampo, iSize ) ) then
                       FieldByName( sCampo ).AsString := Trim( AttributeAsString( Registro, sCampo, VACIO ) )
                  else if ( EscampoChar( sCampo, iSize ) ) then
                       FieldByName( sCampo ).AsString := Trim( AttributeAsString( Registro, sCampo, VACIO ) )
                  else if ( EsCampoNumerico( sCampo ) ) then
                       FieldByName( sCampo ).AsInteger := AttributeAsInteger( Registro, sCampo, 0 )
                  else if ( EsCampoBooleano( sCampo ) ) then
                       FieldByName( sCampo ).AsString := Trim( AttributeAsString( Registro, sCampo, K_GLOBAL_NO ) )
                  else if ( EsCampoDecimal( sCampo ) ) then
                       FieldByName( sCampo ).AsFloat := AttributeAsFloat( Registro, sCampo, 0.00 );
                  Result := True;
             end
             else
             begin
                  sLogBitacora.Add(  IntToStr( FEmpleado ) +  '|' + 'Error al agregar empleado: el campo ' + sCampo + ' no es correcto');
                  Result := False;
             end;
        end;
   end;
   
   procedure CargaParametros( Registro: TZetaXMLNode );
   var
      Atributos: IXMLNodeList;
      i: Integer;
   begin
        with FXMLTools do
        begin
             while Assigned( Registro ) do
             begin
                  Atributos := Registro.AttributeNodes;
                  for i:= 0 to Atributos.Count - 1 do
                  begin
                       sCampo := Atributos.Get(i).NodeName;//Name Atributo XML
                       if ((sCampo = 'CB_G_TEX_4') or (sCampo = 'CB_CTA_VAL')) then
                       begin
                            sCampo := sCampo;
                       end;
                       Asigna( sCampo, Registro );
                  end;
                  break;
             end;
         end;
   end;
   
   {*** Agregar Campos al Dataset para realizar el alta de empleado ***}
   procedure AgregaCamposDataset;
   begin
        with cdsAltaEmpleadosWFN do
        begin
             Close;
             AddStringField( 'COD_PERCEPS', 10 );
             AddStringField( 'NOM_MADRE', K_ANCHO_DESCRIPCION );
             AddStringField( 'NOM_PADRE', K_ANCHO_DESCRIPCION );
             AddFloatField( 'CB_SALARIO_BRUTO' );
             AddStringField( 'IngresoID', K_ANCHO_DESCRIPCION ); //IngresoID
             CreateTempDataset;
        end;
   end;

   {*** Definicion de campos del DataSet para la Alta de Empleados ***}
   procedure DefineEstructuraEmpleado;
   var
      oDatos: OleVariant;
   begin
        with cdsAltaEmpleadosWFN do
        begin
             Servidor.GetEmpleado( Empresa, 0, oDatos );
             Data := oDatos;
        end;
   end;

   procedure IniciaEstructuraEmpleados;
   begin
        with cdsAltaEmpleadosWFN do
        begin
             if Active then
                EmptyDataSet
             else
             begin
                  DefineEstructuraEmpleado;
                  AgregaCamposDataset;//Agrega campos que se requieren para realizar la alta de empleado
             end;
        end;
   end;

   {*** Validacion de los atributos del XML por cada Registro de Empleado a procesar ***}
   function ValidaAtributosXML: Boolean;
   const
        K_MENSAJE_ERROR = '%s%s';
   var
      lContinuar: Boolean;
      sEmpleado, sSexo, sPuesto, sTemporal, sRFC, sCURP: String;
      dSalarioBrutoMensual: Currency;
   begin
        lContinuar := TRUE;
        with cdsAltaEmpleadosWFN do
        begin
             {*** Validaciones con respecto al PDF ***}
             sEmpleado :=  VACIO;

             //Validacion apellido paterno
             if ( FieldByName( 'CB_CODIGO' ).AsInteger <= 0 ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'N�mero de empleado debe ser mayor a cero, no se puede realizar la alta.' ] ) );
             end;

             //Validacion Fecha Ingreso
             if ( FieldByName( 'CB_FEC_ING' ).AsDateTime = NullDateTime ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'La fecha de ingreso del empleado es inv�lida.' ] ) );
             end;
             //Validacion Fecha de Antiguedad
             if ( FieldByName( 'CB_FEC_ANT' ).AsDateTime = NullDateTime ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'La fecha de antig�edad del empleado es inv�lida.' ] ) );
             end;
             //Validacion fecha de contrato
             if ( FieldByName( 'CB_FEC_CON' ).AsDateTime = NullDateTime ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'La fecha de contrato del empleado es inv�lida.' ] ) );
             end;

             //Validar el contrato del empleado
             if StrVacio( FieldByName( 'CB_CONTRAT' ).AsString ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'Valor global para el tipo de contrato del empleado est� vac�o.' ] ) );
             end;
             //Validar el puesto del empleado
             if StrVacio( FieldByName( 'CB_PUESTO' ).AsString ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'C�digo del puesto del empleado est� vac�o.' ] ) );
             end;

             // CB_CLASIFI.
             if StrVacio( FieldByName( 'CB_CLASIFI' ).AsString ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'Clasificaci�n del empleado est� vac�o.' ] ) );
             end;

             //Validacion de CB_TURNO
             if StrVacio( FieldByName( 'CB_TURNO' ).AsString ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'El turno del empleado se encuentra vac�o.' ] ) );
             end;

             //Validacion de CB_PATRON
             if StrVacio( FieldByName( 'CB_PATRON' ).AsString ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'El registro patronal del empleado se encuentra vac�o.' ] ) );
             end;

             if StrVacio( FieldByName( 'CB_ZONA_GE' ).AsString ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'Valor para la zona geogr�fica del empleado se encuentra vac�o.'] ) );
             end;

             if StrVacio( FieldByName( 'CB_TABLASS' ).AsString ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'Valor para la tabla de prestaciones del empleado se encuentra vac�o.' ] ) );
             end;

             if ( FieldByName( 'CB_NOMINA' ).AsInteger <> Ord( tpSemanal ) ) then
             begin
                  lContinuar := FALSE;
                  sLogBitacora.append( Format( K_MENSAJE_ERROR, [ sEmpleado, 'El tipo de n�mina del empleado no puede ser menor o igual a cero.' ] ) );
             end;
             {************************************************** VALIDACIONES PRESBAJA  ************************}
        end;
        Result := lContinuar;
   end;

   function AgregarCambioSalarioBrutoMensual( iCB_CODIGO : integer; fSalarioBrutoMensual : TPesos;  dFecha : TDateTime ): Boolean;
   const
        K_T_CAMSAL = 'CAMSAL';
   var
      ErrorCount: Integer;
   begin
        Result := TRUE;
        try
           if FetchEmployee( cdsLista, iCB_CODIGO ) then
           begin
                with cdsLista do
                begin
                     Data := ServerRecursos.GetEditHisKardex( Empresa, iCB_CODIGO, 0, VACIO );  // Llenar DataSet Vacio con formato de campos de KARDEX
                     Append;
                     FieldByName( 'CB_TIPO' ).AsString := K_T_CAMSAL;
                     FieldByName( 'CB_CODIGO').AsInteger := iCB_CODIGO;
                     FieldByName( 'CB_COMENTA' ).AsString :=  'Salario Bruto Mensual por WFN';
                     FieldByName( 'CB_NOTA' ).AsString := 'Agregado por WFN';
                     FieldByName( 'CB_MONTO' ).AsFloat := fSalarioBrutoMensual;
                     FieldByName( 'CB_FECHA' ).AsDateTime := dFecha;
                     PostData;
                     OnReconcileError := ReconcileError;
                     FServerRecursos := nil;
                     if Reconciliar(ServerRecursos.GrabaHisKardex ( Empresa, DeltaNull, VACIO, ErrorCount ) ) then
                     begin
                          lOk := True;
                     end
                     else
                     begin
                          sLogBitacora.append( Self.Status );
                     end;
                end;
           end
           else
           begin
                Result := FALSE;
                sLogBitacora.append( 'No se encontr� el empleado # ' + IntToStr( iCB_CODIGO ) );
           end;
        except
              on Error: Exception do
              begin
                   Result := FALSE;
                   sLogBitacora.append( 'Error al agregar cambio de salario bruto mensual: ' + Error.Message );
              end;
        end;
   end;

   procedure CrearXMLRespuestaErrores( var Respuesta: TZetaXMLNode );
   var
      i, iErrores: Integer;
      oError: TZetaXMLNode;
   begin
        iErrores := sLogBitacora.Count;
        FXMLTools.WriteAttributeInteger( 'Errores', iErrores, Respuesta );
        for i := 0 to sLogBitacora.Count - 1 do
        begin
             oError := Respuesta.AddChild('Error');
             oError.NodeValue := sLogBitacora.Strings[ i ];
        end;
   end;

   function CrearXMLRespuestaWFN: String;
   var
      FXMLToolsWFN: TdmXMLTools;
      oRespuestaWFN, oMovimiento, oEmpresa, oRegistros, oRegistro, Respuesta: TZetaXMLNode;
   begin
        try
           FXMLToolsWFN := TdmXMLTools.Create( Self );
           with FXMLToolsWFN do
           begin
                XMLAsText := '<DATOS></DATOS>';
                oRespuestaWFN := XMLDocument.DocumentElement;
                oMovimiento := oRespuestaWFN.AddChild('MOV3');
                oMovimiento.NodeValue := K_TW_EMP_AGREGAR;
                oEmpresa := oRespuestaWFN.AddChild('EMPRESA');
                oEmpresa.NodeValue := cdsCompany.FieldByName('CM_CODIGO').AsString;
                oRegistros := oRespuestaWFN.AddChild('REGISTROS');
                oRegistro := oRegistros.AddChild('REGISTRO');
                WriteAttributeString( 'IngresoID', sIngresoID, oRegistro );
                WriteAttributeString( 'CB_CODIGO', sEmpleado, oRegistro );
                Respuesta := oRegistro.AddChild('Respuesta');
                CrearXMLRespuestaErrores( Respuesta );
                Result := XMLDocument.XML.Text;
           end;
        finally
               FreeAndNil( FXMLToolsWFN );
        end;
   end;

   function CrearPipesRespuestaWFN: String;
   const
        K_PIPE = '|';
   var
      i: Integer;
      sRespuesta: String;
   begin
        for i := 0 to sLogBitacora.Count - 1 do
        begin
             sRespuesta := sRespuesta + K_PIPE + sLogBitacora.Strings[ i ];
        end;
        Result := sRespuesta;
   end;

  function Reingresoempleado: boolean;
   var
   ErrorCount : Integer;
   begin
        ErrorCount := 0;
        Result := FALSE;
        try
          AgregandoKardex:= TRUE;
          with cdsEditHisKardex do
          begin
               Data := ServerRecursos.GetEditHisKardex( Empresa, cdsAltaEmpleadosWFN.FieldByName( 'CB_CODIGO' ).AsInteger, 0, '' );
               Append;
               FieldByName( 'CB_TIPO' ).AsString:= K_T_ALTA;
          end;
           SetDatosReingreso;
           dmCliente.cdsEditHisKardex.FieldByName( 'CB_NOTA' ).AsString := 'Registro importado desde Sincronizador Tress Nube';
           dmCliente.cdsEditHisKardex.FieldByName( 'CB_GLOBAL' ).AsString := 'N';
           Texto := TStringList.Create;;
           ValidaEnviar( dmCliente.cdsEditHisKardex, Texto );
           if Texto.Text <> VACIO then
           begin
                sLogBitacora.Append( Format( 'Error al procesar reingreso: %s', [ Texto.Text ] ));
                Result := FALSE;
           end
           else
           begin
                Result := TRUE;
           end;
           freeandnil(Texto);
        except
              on Error : Exception do
              begin
                   sLogBitacora.Append( Format( 'Error al procesar reingreso: %s', [ Error.Message ] ));
                   Result := FALSE;
              end;
        end;
   end;

   procedure ObtenerDatosProceso( var sEmpleado, sIngresoID: String );
   begin
        with FXMLTools do
        begin
             Registros := GetNode( 'REGISTROS' );
             Registro := GetNode( 'REGISTRO', Registros );
             sEmpleado := Registro.Attributes['CB_CODIGO'];
             sIngresoID := Registro.Attributes['IngresoID'];
        end;
   end;

   Procedure ObtenerEmpleado(iEmpleado: integer );
   var
      oDatos: OleVariant;
   begin
        with cdsEmpleadoGenerico do
        begin
             Servidor.GetEmpleado( Empresa, iEmpleado, oDatos );
             Data := oDatos;
        end;

        with cdsLista do
        begin
             Servidor.GetEmpleado( Empresa, iEmpleado, oDatos );
             Data := oDatos;
        end;
   end;


   procedure AltaComoReingreso;
   begin
                  //cdsLista.FieldByName( 'CB_ACTIVO' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_ACTIVO' ).asstring;
                  //cdsLista.FieldByName( 'CB_AUTOSAL' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_AUTOSAL' ).asstring;
                  cdsLista.FieldByName( 'CB_APE_MAT' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_APE_MAT' ).asstring;
                  cdsLista.FieldByName( 'CB_APE_PAT' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_APE_PAT' ).asstring;
                  cdsLista.FieldByName( 'CB_NOMBRES' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_NOMBRES' ).asstring;
                  cdsLista.FieldByName( 'CB_CALLE' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_CALLE' ).asstring;
                  cdsLista.FieldByName( 'CB_CHECA' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_CHECA' ).asstring;
                  cdsLista.FieldByName( 'CB_CIUDAD' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_CIUDAD' ).asstring;
                  //cdsLista.FieldByName( 'CB_CLASIFI' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_CLASIFI' ).asstring;
                  cdsLista.FieldByName( 'CB_CODPOST' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_CODPOST' ).asstring;
                  cdsLista.FieldByName( 'CB_COLONIA' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_COLONIA' ).asstring;
                  //cdsLista.FieldByName( 'CB_CONTRAT' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_CONTRAT' ).asstring;
                  cdsLista.FieldByName( 'CB_CREDENC' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_CREDENC' ).asstring;
                  cdsLista.FieldByName( 'CB_CTA_VAL' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_CTA_VAL' ).asstring;
                  cdsLista.FieldByName( 'CB_CURP' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_CURP' ).asstring;
                  cdsLista.FieldByName( 'CB_E_MAIL' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_E_MAIL' ).asstring;
                  cdsLista.FieldByName( 'CB_ESTADO' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_ESTADO' ).asstring;
                  cdsLista.FieldByName( 'CB_ESTUDIO' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_ESTUDIO' ).asstring;

                  //cdsLista.FieldByName( 'CB_FEC_ANT' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_FEC_ANT' ).AsDateTime;
                  //cdsLista.FieldByName( 'CB_FEC_CON' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_FEC_CON' ).AsDateTime;
                  //cdsLista.FieldByName( 'CB_FEC_COV' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_FEC_COV' ).AsDateTime;
                  //cdsLista.FieldByName( 'CB_FEC_ING' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_FEC_ING' ).AsDateTime;
                  cdsLista.FieldByName( 'CB_FEC_NAC' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_FEC_NAC' ).AsDateTime;

                  cdsLista.FieldByName( 'CB_LUG_NAC' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_LUG_NAC' ).asstring;
                  cdsLista.FieldByName( 'CB_MUNICIP' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_MUNICIP' ).asstring;
                  cdsLista.FieldByName( 'CB_NACION' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_NACION' ).asstring;
                  cdsLista.FieldByName( 'CB_NIVEL0' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL0' ).asstring;
                  //cdsLista.FieldByName( 'CB_NIVEL1' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL1' ).asstring;
                  //cdsLista.FieldByName( 'CB_NIVEL2' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL2' ).asstring;
                  //cdsLista.FieldByName( 'CB_NIVEL3' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL3' ).asstring;
                  //cdsLista.FieldByName( 'CB_NIVEL4' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL4' ).asstring;
                  //cdsLista.FieldByName( 'CB_NIVEL5' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL5' ).asstring;
                  //cdsLista.FieldByName( 'CB_NIVEL6' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL6' ).asstring;
                  //cdsLista.FieldByName( 'CB_NIVEL7' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL7' ).asstring;
                  //cdsLista.FieldByName( 'CB_NIVEL8' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL8' ).asstring;
                  //cdsLista.FieldByName( 'CB_NIVEL9' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_NIVEL9' ).asstring;
                  //cdsLista.FieldByName( 'CB_PATRON' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_PATRON' ).asstring;
                  //cdsLista.FieldByName( 'CB_PUESTO' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_PUESTO' ).asstring;

                  //cdsLista.FieldByName( 'CB_SALARIO' ).AsFloat := cdsAltaEmpleadosWFN.FieldByName( 'CB_SALARIO' ).AsFloat;

                  //------------cdsLista.FieldByName( 'CB_SALARIO_BRUTO' ).AsFloat := cdsAltaEmpleadosWFN.FieldByName( 'CB_SALARIO_BRUTO' ).AsFloat;

                  cdsLista.FieldByName( 'CB_SEGSOC' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_SEGSOC' ).asstring;
                  cdsLista.FieldByName( 'CB_SEXO' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_SEXO' ).asstring;
                  //cdsLista.FieldByName( 'CB_TABLASS' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_TABLASS' ).asstring;
                  cdsLista.FieldByName( 'CB_TEL' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_TEL' ).asstring;
                  //cdsLista.FieldByName( 'CB_TURNO' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_TURNO' ).asstring;
                  cdsLista.FieldByName( 'CB_FEC_RES' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_FEC_RES' ).AsDateTime;
                  cdsLista.FieldByName( 'CB_EST_HOR' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_EST_HOR' ).asstring;
                  cdsLista.FieldByName( 'CB_EST_HOY' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_EST_HOY' ).asstring;
                  cdsLista.FieldByName( 'CB_EVALUA' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_EVALUA' ).asstring;
                  cdsLista.FieldByName( 'CB_EXPERIE' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_EXPERIE' ).asstring;

                  //cdsLista.FieldByName( 'CB_FEC_BAJ' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_FEC_BAJ' ).AsDateTime;
                  //cdsLista.FieldByName( 'CB_FEC_BSS' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_FEC_BSS' ).AsDateTime;
                  //cdsLista.FieldByName( 'CB_FEC_INT' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_FEC_INT' ).AsDateTime;
                  //cdsLista.FieldByName( 'CB_FEC_REV' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_FEC_REV' ).AsDateTime;
                  cdsLista.FieldByName( 'CB_FEC_VAC' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_FEC_VAC' ).AsDateTime;

                  cdsLista.FieldByName( 'CB_G_FEC_1' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_FEC_1' ).AsDateTime;
                  cdsLista.FieldByName( 'CB_G_FEC_2' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_FEC_2' ).AsDateTime;
                  cdsLista.FieldByName( 'CB_G_FEC_3' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_FEC_3' ).AsDateTime;
                  cdsLista.FieldByName( 'CB_G_FEC_4' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_FEC_4' ).AsDateTime;
                  cdsLista.FieldByName( 'CB_G_FEC_5' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_FEC_5' ).AsDateTime;
                  cdsLista.FieldByName( 'CB_G_FEC_6' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_FEC_6' ).AsDateTime;
                  cdsLista.FieldByName( 'CB_G_FEC_7' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_FEC_7' ).AsDateTime;
                  cdsLista.FieldByName( 'CB_G_FEC_8' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_FEC_8' ).AsDateTime;
                  
                  cdsLista.FieldByName( 'CB_G_LOG_1' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_LOG_1' ).asstring;
                  cdsLista.FieldByName( 'CB_G_LOG_2' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_LOG_2' ).asstring;
                  cdsLista.FieldByName( 'CB_G_LOG_3' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_LOG_3' ).asstring;
                  cdsLista.FieldByName( 'CB_G_LOG_4' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_LOG_4' ).asstring;
                  cdsLista.FieldByName( 'CB_G_LOG_5' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_LOG_5' ).asstring;
                  cdsLista.FieldByName( 'CB_G_LOG_6' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_LOG_6' ).asstring;
                  cdsLista.FieldByName( 'CB_G_LOG_7' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_LOG_7' ).asstring;
                  cdsLista.FieldByName( 'CB_G_LOG_8' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_LOG_8' ).asstring;

                  cdsLista.FieldByName( 'CB_G_NUM_1' ).AsFloat := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_NUM_1' ).AsFloat;
                  cdsLista.FieldByName( 'CB_G_NUM_2' ).AsFloat := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_NUM_2' ).AsFloat;
                  cdsLista.FieldByName( 'CB_G_NUM_3' ).AsFloat := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_NUM_3' ).AsFloat;
                  cdsLista.FieldByName( 'CB_G_NUM_4' ).AsFloat := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_NUM_4' ).AsFloat;
                  cdsLista.FieldByName( 'CB_G_NUM_5' ).AsFloat := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_NUM_5' ).AsFloat;
                  cdsLista.FieldByName( 'CB_G_NUM_6' ).AsFloat := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_NUM_6' ).AsFloat;
                  cdsLista.FieldByName( 'CB_G_NUM_7' ).AsFloat := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_NUM_7' ).AsFloat;
                  cdsLista.FieldByName( 'CB_G_NUM_8' ).AsFloat := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_NUM_8' ).AsFloat;
                  cdsLista.FieldByName( 'CB_G_NUM_9' ).AsFloat := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_NUM_9' ).AsFloat;
                  cdsLista.FieldByName( 'CB_G_NUM10' ).AsFloat := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_NUM10' ).AsFloat;
                  cdsLista.FieldByName( 'CB_G_NUM11' ).AsFloat := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_NUM11' ).AsFloat;
                  cdsLista.FieldByName( 'CB_G_NUM12' ).AsFloat := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_NUM12' ).AsFloat;
                  cdsLista.FieldByName( 'CB_G_NUM13' ).AsFloat := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_NUM13' ).AsFloat;
                  cdsLista.FieldByName( 'CB_G_NUM14' ).AsFloat := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_NUM14' ).AsFloat;
                  cdsLista.FieldByName( 'CB_G_NUM15' ).AsFloat := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_NUM15' ).AsFloat;
                  cdsLista.FieldByName( 'CB_G_NUM16' ).AsFloat := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_NUM16' ).AsFloat;
                  cdsLista.FieldByName( 'CB_G_NUM17' ).AsFloat := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_NUM17' ).AsFloat;
                  cdsLista.FieldByName( 'CB_G_NUM18' ).AsFloat := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_NUM18' ).AsFloat;

                  cdsLista.FieldByName( 'CB_G_TAB_1' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TAB_1' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TAB_2' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TAB_2' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TAB_3' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TAB_3' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TAB_4' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TAB_4' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TAB_5' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TAB_5' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TAB_6' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TAB_6' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TAB_7' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TAB_7' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TAB_8' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TAB_8' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TAB_9' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TAB_9' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TAB10' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TAB10' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TAB11' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TAB11' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TAB12' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TAB12' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TAB13' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TAB13' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TAB14' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TAB14' ).asstring;

                  cdsLista.FieldByName( 'CB_G_TEX_1' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX_1' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TEX_2' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX_2' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TEX_3' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX_3' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TEX_4' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX_4' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TEX_5' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX_5' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TEX_6' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX_6' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TEX_7' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX_7' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TEX_8' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX_8' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TEX_9' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX_9' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TEX10' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX10' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TEX11' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX11' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TEX12' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX12' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TEX13' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX13' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TEX14' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX14' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TEX15' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX15' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TEX16' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX16' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TEX17' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX17' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TEX18' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX18' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TEX19' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX19' ).asstring;

                  cdsLista.FieldByName( 'CB_HABLA' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_HABLA' ).asstring;
                  cdsLista.FieldByName( 'CB_IDIOMA' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_IDIOMA' ).asstring;
                  cdsLista.FieldByName( 'CB_INFCRED' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_INFCRED' ).asstring;
                  cdsLista.FieldByName( 'CB_INFMANT' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_INFMANT' ).asstring;

                  cdsLista.FieldByName( 'CB_INFTASA' ).AsFloat := cdsAltaEmpleadosWFN.FieldByName( 'CB_INFTASA' ).AsFloat;

                  cdsLista.FieldByName( 'CB_LA_MAT' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_LA_MAT' ).asstring;

                  cdsLista.FieldByName( 'CB_LAST_EV' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_LAST_EV' ).AsDateTime;

                  cdsLista.FieldByName( 'CB_MAQUINA' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_MAQUINA' ).asstring;
                  cdsLista.FieldByName( 'CB_MED_TRA' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_MED_TRA' ).asstring;
                  cdsLista.FieldByName( 'CB_PASAPOR' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_PASAPOR' ).asstring;
                  cdsLista.FieldByName( 'CB_VIVECON' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_VIVECON' ).asstring;
                  cdsLista.FieldByName( 'CB_VIVEEN' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_VIVEEN' ).asstring;
                  cdsLista.FieldByName( 'CB_ZONA' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_ZONA' ).asstring;
                  cdsLista.FieldByName( 'CB_TIP_REV' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_TIP_REV' ).asstring;

                  cdsLista.FieldByName( 'CB_INF_INI' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_INF_INI' ).AsDateTime;
                  cdsLista.FieldByName( 'CB_INF_OLD' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_INF_OLD' ).asstring;

                  //cdsLista.FieldByName( 'CB_OLD_SAL' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_OLD_SAL' ).asstring;
                  //cdsLista.FieldByName( 'CB_OLD_INT' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_OLD_INT' ).asstring;
                  //cdsLista.FieldByName( 'CB_PRE_INT' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_PRE_INT' ).asstring;
                  //cdsLista.FieldByName( 'CB_PER_VAR' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_PER_VAR' ).asstring;
                  //cdsLista.FieldByName( 'CB_SAL_TOT' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_SAL_TOT' ).asstring;
                  //cdsLista.FieldByName( 'CB_TOT_GRA' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_TOT_GRA' ).asstring;
                  //cdsLista.FieldByName( 'CB_FAC_INT' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_FAC_INT' ).asstring;
                  //cdsLista.FieldByName( 'CB_RANGO_S' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_RANGO_S' ).asstring;
                  cdsLista.FieldByName( 'CB_CLINICA' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_CLINICA' ).asstring;
                  cdsLista.FieldByName( 'CB_CANDIDA' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_CANDIDA' ).asstring;
                  cdsLista.FieldByName( 'CB_ID_NUM' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_ID_NUM' ).asstring;
                  cdsLista.FieldByName( 'CB_ENT_NAC' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_ENT_NAC' ).asstring;
                  cdsLista.FieldByName( 'CB_COD_COL' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_COD_COL' ).asstring;
                  cdsLista.FieldByName( 'CB_DER_PV' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_DER_PV' ).asstring;
                  cdsLista.FieldByName( 'CB_V_PRIMA' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_V_PRIMA' ).asstring;
                  cdsLista.FieldByName( 'CB_SUB_CTA' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_SUB_CTA' ).asstring;
                  cdsLista.FieldByName( 'CB_NETO' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_NETO' ).asstring;
                  //cdsLista.FieldByName( 'CB_RECONTR' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_RECONTR' ).asstring;
                  cdsLista.FieldByName( 'CB_DISCAPA' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_DISCAPA' ).asstring;
                  cdsLista.FieldByName( 'CB_INDIGE' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_INDIGE' ).asstring;
                  cdsLista.FieldByName( 'CB_FONACOT' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_FONACOT' ).asstring;
                  cdsLista.FieldByName( 'CB_EMPLEO' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_EMPLEO' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TEX20' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX20' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TEX21' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX21' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TEX22' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX22' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TEX23' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX23' ).asstring;
                  cdsLista.FieldByName( 'CB_G_TEX24' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_G_TEX24' ).asstring;
                  cdsLista.FieldByName( 'CB_INFDISM' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_INFDISM' ).asstring;
                  cdsLista.FieldByName( 'CB_INFACT' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_INFACT' ).asstring;
                  cdsLista.FieldByName( 'CB_NUM_EXT' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_NUM_EXT' ).asstring;
                  cdsLista.FieldByName( 'CB_NUM_INT' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_NUM_INT' ).asstring;

                  cdsLista.FieldByName( 'CB_INF_ANT' ).AsDateTime := cdsAltaEmpleadosWFN.FieldByName( 'CB_INF_ANT' ).AsDateTime;

                  cdsLista.FieldByName( 'CB_TDISCAP' ).AsInteger := cdsAltaEmpleadosWFN.FieldByName( 'CB_TDISCAP' ).AsInteger;

                  cdsLista.FieldByName( 'CB_ESCUELA' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_ESCUELA' ).asstring;

                  cdsLista.FieldByName( 'CB_TESCUEL' ).AsInteger := cdsAltaEmpleadosWFN.FieldByName( 'CB_TESCUEL' ).AsInteger;
                  cdsLista.FieldByName( 'CB_TITULO' ).AsInteger := cdsAltaEmpleadosWFN.FieldByName( 'CB_TITULO' ).AsInteger;
                  cdsLista.FieldByName( 'CB_YTITULO' ).AsInteger := cdsAltaEmpleadosWFN.FieldByName( 'CB_YTITULO' ).AsInteger;

                  cdsLista.FieldByName( 'CB_CTA_GAS' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_CTA_GAS' ).asstring;
                  cdsLista.FieldByName( 'CB_ID_BIO' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_ID_BIO' ).asstring;
                  cdsLista.FieldByName( 'CB_GP_COD' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_GP_COD' ).asstring;
                  cdsLista.FieldByName( 'CB_TSANGRE' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_TSANGRE' ).asstring;
                  cdsLista.FieldByName( 'CB_ALERGIA' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_ALERGIA' ).asstring;
                  cdsLista.FieldByName( 'CB_BRG_ACT' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_BRG_ACT' ).asstring;

                  cdsLista.FieldByName( 'CB_BRG_TIP' ).AsInteger := cdsAltaEmpleadosWFN.FieldByName( 'CB_BRG_TIP' ).AsInteger;

                  cdsLista.FieldByName( 'CB_BRG_ROL' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_BRG_ROL' ).asstring;
                  cdsLista.FieldByName( 'CB_BRG_JEF' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_BRG_JEF' ).asstring;
                  cdsLista.FieldByName( 'CB_BRG_CON' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_BRG_CON' ).asstring;
                  cdsLista.FieldByName( 'CB_BRG_PRA' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_BRG_PRA' ).asstring;
                  cdsLista.FieldByName( 'CB_BRG_NOP' ).asstring := cdsAltaEmpleadosWFN.FieldByName( 'CB_BRG_NOP' ).asstring;
                  cdsLista.FieldByName( 'CB_REGIMEN' ).AsInteger := cdsAltaEmpleadosWFN.FieldByName( 'CB_REGIMEN' ).AsInteger;
   end;

begin
     lOkProcess := FALSE;
     lOkValidoRegistro := FALSE;
     sEmpleado := VACIO;
     sIngresoID := VACIO;
     dSalarioBrutoMensual := 0;
     dFechaReIngreso := 0;
     sVerfecha:= VACIO;
     sNroEmpleaTDespensaRepetida:= VACIO;
     sLogBitacora := TStringList.Create();
     try
        lOk := TRUE;
        ObtenerDatosProceso( sEmpleado, sIngresoID );
        if not Autorizacion.EsDemo then
        begin
             with FXMLTools do
             begin
                  Registros := GetNode( 'REGISTROS' );
                  if Assigned ( Registros ) then
                  begin
                       Registro := GetNode( 'REGISTRO', Registros );
                       if Assigned( Registro ) then
                       begin
                            IniciaEstructuraEmpleados;
                            with cdsAltaEmpleadosWFN do
                            begin
                                 Append;
                                 LimpiaGlobales;
                                 CargaParametros( Registro );
                                 if not ValidaAtributosXML then
                                    cdsAltaEmpleadosWFN.Cancel
                                 else
                                 begin
                                      if SetEmpleadoNumero( cdsEmpleado, cdsAltaEmpleadosWFN.FieldByName( 'CB_CODIGO' ).AsInteger ) then
                                      begin
                                       //AQUI INICIA PROCESO DE REINGRESO
                                       ObtenerEmpleado(cdsAltaEmpleadosWFN.FieldByName( 'CB_CODIGO' ).AsInteger);
                                       dFechaReIngreso := cdsAltaEmpleadosWFN.FieldByName( 'CB_FEC_ING' ).AsDateTime;
                                       if cdsEmpleadoGenerico.FieldByName( 'CB_ACTIVO' ).AsString = 'N' then
                                       begin
                                           if ( dFechaReIngreso <> NullDateTime) then
                                           begin
                                                 if(EmpWFN_ValidarTarjetaDespensa(cdsAltaEmpleadosWFN.FieldByName( 'CB_CODIGO' ).AsInteger, cdsAltaEmpleadosWFN.FieldByName( 'CB_CTA_VAL' ).AsString, sNroEmpleaTDespensaRepetida)) then
                                                 begin
                                                      if ( cdsEmpleadoGenerico.FieldByName( 'CB_FEC_BAJ' ).AsDateTime < dFechaReingreso ) then
                                                      begin
                                                           if Reingresoempleado then
                                                           begin
                                                           try
                                                              cdsLista.Edit;
                                                              AltaComoReingreso;
                                                              cdsLista.Post;
                                                              ServerRecursos.GrabaDatosEmpleado( Empresa, cdsLista.Delta, FOtrosDatos, FListaPercepFijas, ErrorCount );
                                                              lOk := TRUE;
                                                              lOkProcess := TRUE;
                                                              Except
                                                                  lOk := FALSE;
                                                              End;
                                                           end
                                                           else
                                                           begin
                                                                lOk := FALSE;
                                                           end;
                                                      end
                                                      else
                                                      begin
                                                           sLogBitacora.Append( Format('Fecha de reingreso [%s] no puede ser anterior a fecha de actual de baja [%s]. Revisar informaci�n',[ FechaCorta( dFechaReingreso ), FechaCorta( cdsEmpleadoGenerico.FieldByName( 'CB_FEC_BAJ' ).AsDateTime ) ]) );
                                                           lOk := FALSE;
                                                      end;
                                                end
                                                else
                                                begin
                                                     if sNroEmpleaTDespensaRepetida <> VACIO then
                                                     begin
                                                           raise Exception.Create( 'El empleado '+ sNroEmpleaTDespensaRepetida+' tiene asignado ese n�mero de cuenta de tarjeta despensa.' );
                                                           result := false;
                                                     end
                                                     else
                                                     begin
                                                           raise Exception.Create( 'Ya existe ese n�mero de cuenta de tarjeta despensa en otro empleado' );
                                                           result := false;
                                                     end;
                                                end;
                                           end
                                           else
                                           begin
                                                sLogBitacora.Append( 'Fecha de reingreso nula. Revisar informaci�n' );
                                                lOk := FALSE;
                                           end;
                                       end
                                       else
                                       begin
                                            sLogBitacora.Append( Format('El empleado [%s] esta activo, Seleccionar otro empleado.',[cdsEmpleadoGenerico.FieldByName( 'CB_CODIGO' ).AsString]) );
                                            lOk := FALSE;
                                       end;
                                      end
                                      else
                                      begin
                                           //sLogBitacora.Append( Format( 'Empleado #%d no existe en la empresa %s:%s', [ cdsEmpleadoGenerico.FieldByName( 'CB_CODIGO' ).AsInteger , sEmpresa, sNombreCompany ] ));
                                           sLogBitacora.Append( Format( 'Empleado #%d no existe.', [ cdsEmpleadoGenerico.FieldByName( 'CB_CODIGO' ).AsInteger] ));
                                           lOk := FALSE;
                                      end;
                                 end;
                            end;
                       end
                       else
                       begin
                            sLogBitacora.Append( 'Error al agregar empleado: Nodo <REGISTRO> no especificado' );
                            lOk := FALSE;
                       end;
                  end
                  else
                  begin
                       sLogBitacora.Append( 'Error al agregar empleado: Nodo <REGISTROS> no especificado' );
                       lOk := FALSE;
                  end;
             end;
        end
        else
            sLogBitacora.Append( 'Empresa en modo DEMO, no se realiza el Reingreso.' );

        if ( lOkProcess and lOk ) then
        begin
             {*** Cambio de Salario Bruto Mensual ***}
             dSalarioBrutoMensual := cdsAltaEmpleadosWFN.FieldByName( 'CB_SALARIO_BRUTO' ).AsFloat;
             dFechaReIngreso := cdsAltaEmpleadosWFN.FieldByName( 'CB_FEC_ING' ).AsDateTime;
             iEmpNumero := cdsAltaEmpleadosWFN.FieldByName( 'CB_CODIGO' ).AsInteger;
             if ( dSalarioBrutoMensual > 0 ) then
             begin
                  if AgregarCambioSalarioBrutoMensual( iEmpNumero, dSalarioBrutoMensual, dFechaReIngreso ) then
                     LogError( 0, CrearXMLRespuestaWFN ) {*** Respuesta XML WFN ***}
                  else
                  begin
                       LogError( 0, CrearPipesRespuestaWFN ); {*** Respuesta XML WFN ***}
                       lOk := FALSE;
                  end;
             end
             else
                 LogError( 0, CrearXMLRespuestaWFN ); {*** Respuesta XML WFN ***}
        end
        else
            LogError( 0, CrearPipesRespuestaWFN ); {*** Respuesta XML WFN ***}

     except
           on Error: Exception do
           begin
                sLogBitacora.Append( Error.Message );
                LogError( 0, CrearPipesRespuestaWFN ); {*** Respuesta XML WFN ***}
                lOk := FALSE;
           end;
     end;
     Result := lOk;
end;

//---------------------------------------------------------------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// Eventos de los procesos de bit�cora
///////////////////////////////////////////////////////////////////////////////
procedure TdmCliente.InitProcessLog;
begin
      FClienteProcessList :=  TProcessList.Create;
      FClienteProcessData := TProcessInfo.Create( FClienteProcessList );
end;

procedure TdmCliente.EscribeBitacora( Resultado: OleVariant;  Texto: TStringList );
const
     K_DIA_HORA = 'hh:nn:ss AM/PM dd/mmm/yy';
begin
     InitProcessLog;
     Check( Resultado , Texto);     
     EndProcessLog;
end;


function TdmCliente.Check(const Resultado: OleVariant;  Texto: TStringList ): Boolean;
begin

     with FClienteProcessData do
     begin
          SetResultado( Resultado );
          Result := ( Status in [ epsEjecutando, epsOK ] );
     end;
     GetProcessLog( FClienteProcessData.Folio , Texto );
end;

procedure TdmCliente.GetProcessLog( const iProceso: Integer;  Texto: TStringList );
begin
     try
         with cdsLogDetail do
         begin
           FServerConsultas := nil;
           Data := ServerConsultas.GetProcessLog( Empresa, iProceso );
         end;
     finally
     end;
end;


procedure TdmCliente.LeerMensajeBitacora( var lOkProcess: Boolean; var Texto: TStringList );
const
     K_MENSAJE_BIT_TIPO_OPERACION = '%d|%s|%s';
var
   sMensaje, sTipoBitacora: String;
begin
      with cdsLogDetail do
      begin
           Refrescar;
           First;
           lOkProcess := True;
           while not Eof do
           begin
                case eTipoBitacora( FieldByName( 'BI_TIPO' ).AsInteger ) of
                     tbError:
                             begin
                                  sTipoBitacora := 'Error';
                                  sMensaje := Format( K_MENSAJE_BIT_TIPO_OPERACION, [ FieldByName( 'CB_CODIGO' ).AsInteger, sTipoBitacora, FieldByName( 'BI_TEXTO' ).AsString + ' |'+ FieldByName( 'BI_DATA' ).AsString] );
                                  Texto.append( sMensaje );
                                  lOkProcess := False;
                             end;
                     tbErrorGrave:
                             begin
                                  sTipoBitacora := 'Error Grave';
                                  sMensaje := Format( K_MENSAJE_BIT_TIPO_OPERACION, [ FieldByName( 'CB_CODIGO' ).AsInteger, sTipoBitacora, FieldByName( 'BI_TEXTO' ).AsString+ ' |'+ FieldByName( 'BI_DATA' ).AsString] );
                                  Texto.append( sMensaje );
                                  lOkProcess := False;
                             end;
                end;
                Next;
           end;
      end;
end;


procedure TdmCliente.EndProcessLog;
begin
     { FreeAndNil(  FClienteProcessList );
      FreeAndNil( FClienteProcessData );}
end;


///////////////////////////////////////////////////////////////////////////////
/// FIN Eventos de los procesos de bit�cora
///////////////////////////////////////////////////////////////////////////////

procedure TdmCliente.cdsImportInfonavitBeforePost(DataSet: TDataSet);
var
   dFecha{,dIngreso}, dFechaInicioCredito: TDate;
   eTipo: eTipoMovInfonavit;
   lEsPrimerRegistro, lCreditoActivo, lInsertando,lEsInicioSuspen,lEsInicioReinicio: Boolean;
   sMensaje: String;

const
     K_IMPOSIBLE_REGISTRAR = 'No se puede ingresar movimientos.';
     K_EMPLEADO = '|%d|';
     function ExisteMovCredito: Boolean;
     begin
          Result:= cdsLookupInfonavit.Locate('KI_FECHA',VarArrayOf([ FechaAsStr( cdsImportInfonavit.FieldByName('KI_FECHA').AsDateTime) ]), [] ) and
                   ( cdsImportInfonavit.FieldByName('KI_TIPO').AsInteger <> cdsLookupInfonavit.FieldByName('KI_TIPO').AsInteger ) ;
     end;

     function EsMovimientoMenor: Boolean;
     begin
          cdsLookupInfonavit.Last;
          Result:= not ( dFecha >= cdsLookupInfonavit.FieldByName('KI_FECHA').AsDateTime ) ;
     end;

     function ObligatoriosVacios: Boolean;
     begin
          with cdsImportInfonavit do
          begin
               if ( StrVacio ( FieldByName('CB_INFCRED').AsString ) ) then
               begin
                    sMensaje:= 'El # de cr�dito no puede quedar vac�o';
               end
               else if ( FieldByName('CB_INFTIPO').AsInteger = Ord ( tiNoTiene ) ) then
               begin
                    sMensaje:= 'Se debe seleccionar un tipo de descuento';
               end

               else if ( FieldByName('CB_INFTASA').AsFloat <= 0 ) then
               begin
                    sMensaje:= 'El valor del cr�dito debe ser mayor a 0';
               end;
               Result:= StrLleno( sMensaje );
          end;
     end;

     function ValidaFechaBimestre( const lFinalBimestre: Boolean ): Boolean;
     begin
          if ( lFinalBimestre ) then
          begin
               Result:= ( dFecha = LastDayOfBimestre(dFecha ));
          end
          else
          begin
               Result:= ( dFecha = FirstDayOfBimestre(dFecha));
          end;
     end;

     function SinCambios: Boolean;
     begin
          Result:= FALSE;
          with cdsEmpleado do
          begin
               case eTipo of
                    infocambiotd:
                    begin
                         Result:= ( FieldByName('CB_INFTIPO').AsInteger = cdsImportInfonavit.FieldByName('CB_INFTIPO').AsInteger );
                    end;
                    infocambiovd:
                    begin
                         Result:= ( FieldByName('CB_INFTASA').AsFloat = cdsImportInfonavit.FieldByName('CB_INFTASA').AsFloat );
                    end;
                    infocambionocre:
                    begin
                          Result:= ( FieldByName('CB_INFCRED').AsString = cdsImportInfonavit.FieldByName('CB_INFCRED').AsString );
                    end;
               end;
          end;
     end;


     function InvalidoPorTipoMov: Boolean;
     begin
          case eTipo of
              infoSuspen:
              begin
                   if ( lInsertando ) and ( not lCreditoActivo ) then
                   begin
                        sMensaje:= 'El cr�dito tiene que estar en status activo' ;
                   end
                   else if ( dFecha < dFechaInicioCredito )  then
                   begin
                        sMensaje:= Format( 'Fecha Suspensi�n[%s] tiene que ser mayor a la fecha de Alta del Cr�dito: %s',
                                       [ FechaCorta( dFecha ), FechaCorta( dFechaInicioCredito ) ] );
                   end
                   { La fecha de suspensi�n tiene que ser igual o menor al �ltimo mov }
                   else if ( EsMovimientoMenor ) then
                   begin
                        sMensaje:= 'La Suspensi�n debe ser el Ultimo Movimiento';
                   end;
              end;
              infoReinicio:
              begin
                   if ( lInsertando ) then
                   begin
                        if ( lCreditoActivo ) then
                        begin
                             sMensaje:= 'El cr�dito tiene que estar en status suspendido' ;
                        end
                        else
                        if ( EsMovimientoMenor ) then
                        begin
                             sMensaje:= 'El Reinicio debe ser el Ultimo Movimiento';
                        end
                   end;
              end;
              infocambiotd, infocambiovd, infocambionocre:
              begin
                   if not (  ValidaFechaBimestre( FALSE ) ) then
                   begin
                        sMensaje:= 'La fecha debe ser igual al primer d�a del bimestre' ;
                   end
                   else if ( lInsertando ) and ( SinCambios ) then
                   begin
                        sMensaje:= Format( 'No se registr� %s', [ ObtieneElemento(lfTipoMovInfonavit,Ord( eTipo ) )] )
                   end;
              end;
          end;
          Result:= StrLleno( sMensaje );
     end;
begin
      with cdsHisCreInfonavit do
      begin
           Refrescar;
           lEsPrimerRegistro:= ( RecordCount = 0 );
      end;
      with cdsImportInfonavit do
      begin
           cdsLookupInfonavit.Last;
           dFechaInicioCredito:= cdsEmpleado.FieldByName('CB_INF_INI').AsDateTime;
           dFecha:= FieldByName('KI_FECHA').AsDateTime;
           eTipo:= eTipoMovInfonavit( FieldByName('KI_TIPO').AsInteger );
           lEsInicioReinicio:= ( cdsLookupInfonavit.FieldByName('KI_TIPO').AsInteger = Ord( infoInicio ) ) and
                              ( FieldByName('KI_TIPO').AsInteger = Ord( inforeinicio ) );
           lCreditoActivo:= zStrToBool(cdsEmpleado.FieldByName( 'CB_INFACT' ).AsString );
           lInsertando:= ( State = dsInsert );
           lEsInicioSuspen:=  ( cdsLookupInfonavit.FieldByName('KI_TIPO').AsInteger = Ord( infoInicio ) ) and
                              ( FieldByName('KI_TIPO').AsInteger = Ord( infoSuspen ) );

           {El primer registro tiene que ser de tipo alta}
           if ( lEsPrimerRegistro ) and ( eTipo <> infoinicio ) then
           begin
                LogError(0, Format( K_EMPLEADO + 'El primer registro debe ser de tipo: ' + '%s',[ FEmpleado, ObtieneElemento(lfTipoMovInfonavit, Ord( infoInicio ) )]) );
                ValidacionInfonavit := True;
           end
           { No se puede hacer ningun tipo d movimiento si el cr�dito esta suspendido }
           else if ( lInsertando ) and not( eTipo in [ infoInicio, infoReinicio ] ) and ( not lCreditoActivo ) then
           begin
                LogError( 0, Format ( K_EMPLEADO + K_IMPOSIBLE_REGISTRAR + 'El cr�dito esta suspendido', [ FEmpleado ] ) );
                ValidacionInfonavit := True;
           end
           { Fecha de Movimiento menor o igual a la fecha de inicio de cr�dito, IGUAL solamente cuando sea diferente a suspensi�n. }
           else if ( not ( eTipo in [infoInicio, infoReinicio, infoSuspen ] ) and ( dFecha < dFechaInicioCredito )  ) then
           begin
                LogError(0, Format( K_EMPLEADO + 'Fecha movimiento [%s] tiene que ser mayor a fecha de Alta del Cr�dito: %s',
                                     [ FEmpleado, FechaCorta( dFecha ), FechaCorta( dFechaInicioCredito ) ] ) );
                ValidacionInfonavit := True;
           end
           {No se puede insertar un registro si existe un movimiento en esa fecha, exceptuando el inicio y suspension }
           else if ( not lEsInicioSuspen ) and ( ExisteMovCredito  ) then
           begin
                LogError(0, Format( K_EMPLEADO + 'Ya existe un movimiento de cambio en esa misma fecha', [ FEmpleado ] ) );
                ValidacionInfonavit := True;
           end
           else if ( lEsInicioReinicio ) OR ( FieldByName( 'CB_INF_ANT').AsDateTime > FieldByName('KI_FECHA').AsDateTime ) then
           begin
                LogError( 0, Format( K_EMPLEADO + 'La fecha de otorgamiento es mayor que la fecha del movimiento', [ FEmpleado ] ) );
                ValidacionInfonavit := True;
           end
           { Si los registros obligatorios estan vaci�s o el movimiento no cumple x especificaci�n de c/T. Mov }
           else if ObligatoriosVacios or InvalidoPorTipoMov then
           begin
                LogError( 0, Format ( K_EMPLEADO + '%s', [ FEmpleado , sMensaje ] ) );
                ValidacionInfonavit := True;
           end
           else
           begin
                ValidaCreditoInfo(cdsImportInfonavit);
           end;
      end;
end;



{ cdsHisCreInfonavit }
//Obtener el Historial de Infonavir para el Empleado Seleccionado
procedure TdmCliente.cdsHisCreInfonavitAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsHisCreInfonavit.Data := ServerRecursos.GetHisCreInfonavit( Empresa, FEmpleado );
     InitDatosKardexInfonavit;
end;

procedure TdmCliente.InitDatosKardexInfonavit;
begin
     with cdsLookupInfonavit do
     begin
          CloneCursor( cdsHisCreInfonavit, FALSE, TRUE );
     end;
end;

{Procedure que valida que el cr�dito de infonavit cumpla con los criterios, se debe de llamar siempre
en un beforepost}
procedure TdmCliente.ValidaCreditoInfo(DataSet: TDataSet);
const
     K_LONGITUD_INFO = 10;
     K_EMPLEADO = '|%d|';
var
   i: Integer;
   sSignos: String;
begin
     with DataSet do
     begin
          if ( FieldByName( 'CB_INFTIPO' ).AsInteger <> ord( tiNoTiene ) ) then
          begin
               if not( State in [dsEdit, dsInsert] ) then
                  Edit;
               FieldByName( 'CB_INFCRED' ).AsString:= Trim(FieldByName( 'CB_INFCRED' ).AsString);
               i := Length( FieldByName( 'CB_INFCRED' ).AsString );
               if ( i > 0 ) then
               begin
                    if ( i < K_LONGITUD_INFO ) then
                    begin
                         LogError( 0, Format( K_EMPLEADO + '# de Cr�dito INFONAVIT INCOMPLETO. Debe tener %d d�gitos', [ FEmpleado, K_LONGITUD_INFO ] ) );
                         ValidacionInfonavit := True;
                    end
                    else
                    if ( i > K_LONGITUD_INFO ) then
                    begin
                         LogError(0, Format( K_EMPLEADO + '# de Cr�dito INFONAVIT EXCEDE LIMITE. Debe tener %d d�gitos', [ FEmpleado, K_LONGITUD_INFO ] ) );
                         ValidacionInfonavit := True;
                    end
                    else
                    if ( i = K_LONGITUD_INFO ) and not ZetaCommonTools.ValidaCredito( Trim( FieldByName( 'CB_INFCRED' ).Text) ,sSignos ) then
                    begin
                         LogError( 0, Format( K_EMPLEADO + '# de Cr�dito INFONAVIT:' + sSignos, [ FEmpleado ] ) );
                         ValidacionInfonavit := True;
                    end
                    else if ( FieldByName( 'CB_INFTIPO' ).AsInteger = ord(tiPorcentaje) ) and
                           ( zStrToBool( FieldByName('CB_INFDISM').AsString )  ) and
                        not( FieldByName('CB_INFTASA').AsInteger in [ 20,25,30 ] ) then
                    begin
                         LogError( 0, Format( K_EMPLEADO + 'Al aplicar la tabla de disminuci�n %, el valor de descuento debe ser igual a 20,25 o 30', [ FEmpleado ] ) );
                         ValidacionInfonavit := True;
                    end;
               end;
          end;
     end;
end;

procedure TdmCliente.cdsPuestosAlAdquirirDatos(Sender: TObject);
begin
  cdsPuestos.Data := ServerCatalogos.GetPuestos( dmCliente.Empresa );
end;

function TdmCliente.EsValidoPuesto(const sCodigo: String): Boolean;
begin
     cdsPuestos.Conectar;
     Result := cdsPuestos.Locate( 'PU_CODIGO', sCodigo, [] );
end;

procedure TdmCliente.cdsClasifiAlAdquirirDatos(Sender: TObject);
begin
     cdsClasifi.Data := ServerCatalogos.GetClasifi( dmCliente.Empresa );
end;



function TdmCliente.EsValidoClasificacion(const sCodigo: String): Boolean;
begin
     cdsClasifi.Conectar;
     Result := cdsClasifi.Locate( 'TB_CODIGO', sCodigo, [] );
end;

procedure TdmCliente.cdsTurnosAlAdquirirDatos(Sender: TObject);
begin
     cdsTurnos.Data := ServerCatalogos.GetTurnos( dmCliente.Empresa );
end;

function TdmCliente.EsValidoTurno(const sCodigo: String): Boolean;
begin
     cdsTurnos.Conectar;
     Result := cdsTurnos.Locate( 'TU_CODIGO', sCodigo, [] );
end;

procedure TdmCliente.cdsMotivoBajaAlAdquirirDatos(Sender: TObject);
begin
  cdsMotivoBaja.Data := ServerTablas.GetTabla( dmCliente.Empresa, cdsMotivoBaja.Tag );
end;

function TdmCliente.EsValidoMotivoBaja(const sCodigo: String): Boolean;
begin
     cdsMotivoBaja.Conectar;
     Result := cdsMotivoBaja.Locate( 'TB_CODIGO', sCodigo, [] );
end;


procedure TdmCliente.cdsEquipoNivelSupervisorAlAdquirirDatos(Sender: TObject);
begin
     cdsEquipoNivelSupervisor.Data := ServerTablas.GetTabla( dmCliente.Empresa, cdsEquipoNivelSupervisor.Tag );
end;

function TdmCliente.EsValidoNivelSupervisor(const sCodigo: String): Boolean;
begin
     cdsEquipoNivelSupervisor.Conectar;
     if (cdsEquipoNivelSupervisor.Tag <> 0) then
     begin
          Result := cdsEquipoNivelSupervisor.Locate( 'TB_CODIGO', sCodigo, [] );
     end
     else
     begin
          Result :=  false;
     end;
end;

procedure TdmCliente.cdsNivel1AlAdquirirDatos(Sender: TObject);
begin
     cdsNivel1.Data := ServerTablas.GetTabla( dmCliente.Empresa, cdsNivel1.Tag );
end;

procedure TdmCliente.cdsNivel2AlAdquirirDatos(Sender: TObject);
begin
     cdsNivel2.Data := ServerTablas.GetTabla( dmCliente.Empresa, cdsNivel2.Tag );
end;

procedure TdmCliente.cdsNivel3AlAdquirirDatos(Sender: TObject);
begin
     cdsNivel3.Data := ServerTablas.GetTabla( dmCliente.Empresa, cdsNivel3.Tag );
end;

procedure TdmCliente.cdsNivel4AlAdquirirDatos(Sender: TObject);
begin
     cdsNivel4.Data := ServerTablas.GetTabla( dmCliente.Empresa, cdsNivel4.Tag );
end;

procedure TdmCliente.cdsNivel5AlAdquirirDatos(Sender: TObject);
begin
     cdsNivel5.Data := ServerTablas.GetTabla( dmCliente.Empresa, cdsNivel5.Tag );
end;

procedure TdmCliente.cdsNivel6AlAdquirirDatos(Sender: TObject);
begin
     cdsNivel6.Data := ServerTablas.GetTabla( dmCliente.Empresa, cdsNivel6.Tag );
end;

procedure TdmCliente.cdsNivel7AlAdquirirDatos(Sender: TObject);
begin
     cdsNivel7.Data := ServerTablas.GetTabla( dmCliente.Empresa, cdsNivel7.Tag );
end;

procedure TdmCliente.cdsNivel8AlAdquirirDatos(Sender: TObject);
begin
     cdsNivel8.Data := ServerTablas.GetTabla( dmCliente.Empresa, cdsNivel8.Tag );
end;

procedure TdmCliente.cdsNivel9AlAdquirirDatos(Sender: TObject);
begin
     cdsNivel9.Data := ServerTablas.GetTabla( dmCliente.Empresa, cdsNivel9.Tag );
end;

procedure TdmCliente.cdsContratosAlAdquirirDatos(Sender: TObject);
begin
     cdsContratos.Data := ServerCatalogos.GetContratos( dmCliente.Empresa)
end;

function TdmCliente.EsValidoContrato(const sCodigo: String): Boolean;
begin
     cdsContratos.Conectar;
     Result := cdsContratos.Locate( 'TB_CODIGO', sCodigo, [] );
end;

function TdmCliente.EsValidoNivel1(const sCodigo: String): Boolean;
begin
     cdsNivel1.Conectar;
     if (cdsNivel1.Tag <> 0) then
     begin
          Result := cdsNivel1.Locate( 'TB_CODIGO', sCodigo, [] );
     end
     else
     begin
          Result :=  false;
     end;
end;

function TdmCliente.EsValidoNivel2(const sCodigo: String): Boolean;
begin
     cdsNivel2.Conectar;
     if (cdsNivel2.Tag <> 0) then
     begin
          Result := cdsNivel2.Locate( 'TB_CODIGO', sCodigo, [] );
     end
     else
     begin
          Result :=  false;
     end;
end;

function TdmCliente.EsValidoNivel3(const sCodigo: String): Boolean;
begin
     cdsNivel3.Conectar;
     if (cdsNivel3.Tag <> 0) then
     begin
          Result := cdsNivel3.Locate( 'TB_CODIGO', sCodigo, [] );
     end
     else
     begin
          Result :=  false;
     end;
end;

function TdmCliente.EsValidoNivel4(const sCodigo: String): Boolean;
begin
     cdsNivel4.Conectar;
     if (cdsNivel4.Tag <> 0) then
     begin
          Result := cdsNivel4.Locate( 'TB_CODIGO', sCodigo, [] );
     end
     else
     begin
          Result :=  false;
     end;
end;

function TdmCliente.EsValidoNivel5(const sCodigo: String): Boolean;
begin
     cdsNivel5.Conectar;
     if (cdsNivel5.Tag <> 0) then
     begin
          Result := cdsNivel5.Locate( 'TB_CODIGO', sCodigo, [] );
     end
     else
     begin
          Result :=  false;
     end;
end;

function TdmCliente.EsValidoNivel6(const sCodigo: String): Boolean;
begin
     cdsNivel6.Conectar;
     if (cdsNivel6.Tag <> 0) then
     begin
          Result := cdsNivel6.Locate( 'TB_CODIGO', sCodigo, [] );
     end
     else
     begin
          Result :=  false;
     end;
end;

function TdmCliente.EsValidoNivel7(const sCodigo: String): Boolean;
begin
     cdsNivel7.Conectar;
     if (cdsNivel7.Tag <> 0) then
     begin
          Result := cdsNivel7.Locate( 'TB_CODIGO', sCodigo, [] );
     end
     else
     begin
          Result :=  false;
     end;
end;

function TdmCliente.EsValidoNivel8(const sCodigo: String): Boolean;
begin
     cdsNivel8.Conectar;
     if (cdsNivel8.Tag <> 0) then
     begin
          Result := cdsNivel8.Locate( 'TB_CODIGO', sCodigo, [] );
     end
     else
     begin
          Result :=  false;
     end;
end;

function TdmCliente.EsValidoNivel9(const sCodigo: String): Boolean;
begin
     cdsNivel9.Conectar;
     if (cdsNivel9.Tag <> 0) then
     begin
          Result := cdsNivel9.Locate( 'TB_CODIGO', sCodigo, [] );
     end
     else
     begin
          Result :=  false;
     end;
end;
{ TEmpleadoBasico }

constructor TEmpleadoBasico.Create;
begin
  inherited Create;
     UsuarioWFN := 0;
     NumeroEmpleado := 0;
     Salario := 0.0;
     Puesto := VACIO;
     Turno := VACIO;
     Clasificacion := VACIO;
     Year := 0;
     Tipo := 0;
     Numero := 0;
     Recontratacion := VACIO;
     Nivel1  := VACIO;
     Nivel2  := VACIO;
     Nivel3  := VACIO;
     Nivel4  := VACIO;
     Nivel5  := VACIO;
     Nivel6  := VACIO;
     Nivel7  := VACIO;
     Nivel8  := VACIO;
     Nivel9  := VACIO;
     Motivo := VACIO;
     Equipo := VACIO;
     AutoSalario := VACIO;
     FechaRegistro := 0;
     FechaImss := 0;
     Descripcion := VACIO;
     Observaciones := VACIO;
end;

destructor TEmpleadoBasico.Destroy;
begin
  inherited Destroy;
end;

{ TEmpleadoSeguroPariente }

constructor TEmpleadoSeguroPariente.Create;
begin
  inherited Create;
     UsuarioWFN := 0;
     NumeroEmpleado := 0;
     ParentescoID := 0;
     ParienteFolio := 0;
     AseguraTipo := 0;
     AseguraCertifi := VACIO;
     AseguraCFijo := 0.0;
     AseguraEndoso := VACIO;
     AseguraFinal := 0;
     AseguraInicial := 0;
     AseguraObserva := VACIO;
     //EP_ORDEN := 0; -- Calculada
     AseguraTipoPol := 0;
     SeguroCodigo := VACIO;
     SeguVigReferen := VACIO;
end;

destructor TEmpleadoSeguroPariente.Destroy;
begin
  inherited Destroy;
end;

procedure TdmCliente.cdsEmpParientesAlAdquirirDatos(Sender: TObject);
begin
     cdsEmpParientes.Data := ServerRecursos.GetEmpParientes( Empresa, FEmpleado );
end;

procedure TdmCliente.cdsHisSGMAlAdquirirDatos(Sender: TObject);
begin
     cdsHisSGM.Data := ServerRecursos.GetSegurosGastosMedicos( Empresa, FEmpleado );
end;

procedure TdmCliente.cdsHisSGMAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsHisSGM do
     begin
          PostData;
          if ( Changecount > 0 )then
          begin
               Reconciliar( ServerRecursos.GrabaSegurosGastosMedicos( Empresa, Delta, ErrorCount ) );
          end;
     end;
end;

procedure TdmCliente.cdsHisSGMBeforePost(DataSet: TDataSet);
begin
     FValidacionSGM := False;
     cdsSegGastosMed.Conectar; //Llenar Dataset con los datos de Vigencia. //Revisar
     with cdsHisSGM do
     begin
          if StrVacio( FieldByName('PM_CODIGO').AsString ) then
          begin
               LogError( FEmpleado, Format('|Empleado %d|C�digo de P�liza no puede quedar Vac�o.', [ FEmpleado ] ) );
          end
          else if StrVacio( FieldByName('PV_REFEREN').AsString ) then
          begin
               LogError( FEmpleado, Format('|Empleado %d|Referencia de P�liza no puede quedar Vac�o.', [ FEmpleado ] ) );
          end
          else if ( ( FieldByName('EP_ASEGURA').AsInteger = Ord(taEmpleado) ) and ( FieldByName('EP_TIPO').AsInteger = Ord(tesDependiente) ) ) then
          begin
               LogError( FEmpleado, Format('|Empleado %d|Solo tiene la opci�n de quedar como Titular de la p�liza.', [ FEmpleado ] ) );
          end
          else if (FieldByName('EP_TIPO').AsInteger = Ord(tesDependiente)) and( ( FieldByName('PA_RELACIO').AsInteger = 0 ) and ( FieldByName('PA_FOLIO').AsInteger = 0 ) )then
          begin
               LogError( FEmpleado, Format('|Empleado %d|Seleccionar Dependiente.', [ FEmpleado ] ) );
          end
          else if StrVacio( FieldByName('EP_CERTIFI').AsString ) then
          begin
               LogError( FEmpleado, Format('|Empleado %d|Certificado de P�liza no puede quedar Vac�o.', [ FEmpleado ] ) );
          end
          else if (FieldByName('EP_FEC_INI').AsDateTime >= FieldByName('EP_FEC_FIN').AsDateTime) then
          begin
               LogError( FEmpleado, Format('|Empleado %d|Fecha Final debe ser Mayor a la Fecha Inicial.', [ FEmpleado ] ) );
          end
          else if ( not EsValidoSeguroCodigo( FieldByName('PM_CODIGO').AsString ) ) then
               LogError( FEmpleado, Format('|Empleado %d|C�digo de P�liza Inv�lido.', [ FEmpleado ] ) )
          else if ( not esValidoReferenciaMed( FieldByName('PM_CODIGO').AsString, FieldByName('PV_REFEREN').AsString ) ) then
               LogError( FEmpleado, Format('|Empleado %d|Referencia de P�liza Inv�lido.', [ FEmpleado ] ) )
          else
          begin
               cdsVigenciasSGM.Locate('PM_CODIGO;PV_REFEREN',VarArrayOf([FieldByName('PM_CODIGO').AsString,FieldByName('PV_REFEREN').AsString]),[]);
               if ( FieldByName('EP_FEC_INI').AsDateTime > cdsVigenciasSGM.FieldByName('PV_FEC_FIN').AsDateTime ) OR
                  ( FieldByName('EP_FEC_FIN').AsDateTime < cdsVigenciasSGM.FieldByName('PV_FEC_INI').AsDateTime ) OR
                  ( FieldByName('EP_FEC_INI').AsDateTime < cdsVigenciasSGM.FieldByName('PV_FEC_INI').AsDateTime ) OR
                  ( FieldByName('EP_FEC_FIN').AsDateTime > cdsVigenciasSGM.FieldByName('PV_FEC_FIN').AsDateTime ) then
                    LogError( FEmpleado, Format('|Empleado %d|Las Fechas de Vigencia estan fuera de Rango de la P�liza Seleccionada.', [ FEmpleado ] ) )
               else
                   FValidacionSGM := True;
          end;
          if ( FValidacionSGM ) then
          begin
               FValidacionSGM := False;
               if ( zStrToBool(FieldByName('EP_CANCELA').AsString)) and ( FieldByName('EP_CAN_FEC').AsDateTime = NullDateTime )then
                    LogError( FEmpleado, Format('|Empleado %d|Fecha de Cancelaci�n es requerida.', [ FEmpleado ] ) )
               else if ( zStrToBool(FieldByName('EP_CANCELA').AsString)) and ( StrVacio( FieldByName('EP_CAN_MOT').AsString ) )then
                    LogError( FEmpleado, Format('|Empleado %d|Motivo de Cancelaci�n No Puede Quedar Vac�o.', [ FEmpleado ] ) )
               else
                   FValidacionSGM := True;
          end;

          if ( FValidacionSGM ) then //Si los Datos Requeridos son completados - Validar respecto a la Base de Datos
          begin
               FValidacionSGM := False;
               if ( FieldByName('EP_FEC_INI').AsDateTime > FieldByName('EP_FEC_FIN').AsDateTime) then
                  LogError( FEmpleado, Format('|Empleado %d|Fecha de Inicio Debe ser menor a Fecha Final.', [ FEmpleado ] ) )
               else if ( not EsValidoTipoPoliza( FieldByName('EP_TIPO').AsInteger ) ) then
                    LogError( FEmpleado, Format('|Empleado %d|Tipo de SGM Inv�lido.', [ FEmpleado ] ) )
               else if ( not EsValidoTipoAsegurado( FieldByName('EP_ASEGURA').AsInteger ) ) then
                    LogError( FEmpleado, Format('|Empleado %d|Tipo de Asegurado Inv�lido.', [ FEmpleado ] ) )
               else if ( FieldByName('EP_ASEGURA').AsInteger = Ord(taPariente) ) and ( FieldByName('PA_RELACIO').AsInteger <> 3 ) and ( FieldByName('PA_RELACIO').AsInteger <> 4 ) then
                    LogError( FEmpleado, Format('|Empleado %d|Tipo de Pariente Inv�lido, solo aplica para el c�nyuge o hijo(s) del empleado.', [ FEmpleado ] ) )
               else if ( ( FieldByName('EP_ASEGURA').AsInteger = Ord(taPariente) ) and ( not EsValidoPariente( FieldByName('PA_RELACIO').AsInteger, FieldByName('PA_FOLIO').AsInteger ) ) ) then
                    LogError( FEmpleado, Format('|Empleado %d|Pariente Seleccionado Inv�lido.', [ FEmpleado ] ) )
               else
                   FValidacionSGM := True;
          end;

          if ( FValidacionSGM ) then
          begin
               FieldByName('EP_ORDEN').AsInteger := GetMaxOrdenPolizas(FieldByName('PM_CODIGO').AsString, FieldByName('PV_REFEREN').AsString) + 1;
          end;

     end;
end;

procedure TdmCliente.cdsSegGastosMedAlAdquirirDatos(Sender: TObject);
var
   Vigencias :OleVariant;
begin
     cdsSegGastosMed.Data := ServerCatalogos.GetSeguroGastosMedicos( Empresa, Vigencias );
     cdsVigenciasSGM.Data :=  Vigencias;
end;

procedure TdmCliente.cdsHisSGMNewRecord(DataSet: TDataSet);
begin
     with cdsHisSGM do
     begin
          FieldByName('EP_FEC_REG').AsDateTime := dmCliente.FechaDefault;
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
     end;
end;

procedure TdmCliente.cdsHisSGMReconcileError(DataSet: TCustomClientDataSet;
  E: EReconcileError; UpdateKind: TUpdateKind;
  var Action: TReconcileAction);
begin
     Action := raAbort;  // Aborta el reconcile para que regrese FALSE
     Status := E.Message;
     LogError( FEmpleado, Format('|Empleado %d|%s', [ FEmpleado, E.Message ] ) );
end;

function TdmCliente.EsValidoPariente(const iParentescoID: Integer; const iParienteFolio: Integer): Boolean;
begin
     cdsEmpParientes.Conectar;
     Result:= cdsEmpParientes.Locate('PA_RELACIO;PA_FOLIO',VarArrayOf([iParentescoID,iParienteFolio]),[]); //Consultar la lista de parientes de Parientes Valida
end;

function TdmCliente.EsValidoSeguroCodigo(const sSeguroCodigo: String): Boolean;
begin
     Result := cdsSegGastosMed.Locate('PM_CODIGO',sSeguroCodigo,[]);
end;

function TdmCliente.esValidoReferenciaMed(const sSeguroCodigo: String; const sSeguVigReferen: String): Boolean;
begin
     Result := cdsVigenciasSGM.Locate('PM_CODIGO;PV_REFEREN',VarArrayOf([sSeguroCodigo,sSeguVigReferen]),[]); //Consultar si la Referencia es valida
end;

function TdmCliente.esValidoTipoPoliza(const iAseguraTipoPol: Integer) : Boolean;
begin
     Result := True;
     if ( iAseguraTipoPol <> Ord(tesTitular) ) and ( iAseguraTipoPol <> Ord(tesDependiente) ) then //Consultar si el Tipo de seguro es valido
        Result := False;
end;

function TdmCliente.esValidoTipoAsegurado(const iAseguraTipo: Integer) : Boolean;
begin
     Result := True;
     if ( iAseguraTipo <> Ord(taEmpleado) ) and ( iAseguraTipo <> Ord(taPariente)) then //Consultar si el Tipo de asegurado es valido
        Result := False;
end;

function TdmCliente.GetMaxOrdenPolizas(const sSeguroCodigo,sSeguVigReferen:string):Integer;
begin
     Result := ServerRecursos.GetMaxOrden( Empresa,sSeguroCodigo,sSeguVigReferen,FEmpleado );
end;

function TdmCliente.ValidaEnviarSGM( DataSet: TZetaClientDataSet ): Boolean;
begin
     Result := True;
     try
        DataSet.Enviar;
     except
           on Error: Exception do
           begin
                Result := False;
                DataSet.CancelUpdates;
                LogError( FEmpleado, Self.Status );
           end;
     end;
end;

procedure TdmCliente.cdsEstadoAlAdquirirDatos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender ) do
     begin
          Data := ServerTablas.GetTabla( dmCliente.Empresa, Tag );
     end;
end;

function TdmCliente.IntercambiarFestivoWFN: Boolean;
var
   i: Integer;
   lOk, lOkProcess: Boolean;

   ListaEmpleados: TObjectList;
   EsquemaEmpleado: TEmpleadoBasico;
   Empleado: TEmpleadoBasico;

   oParametros : TZetaParams;
   Texto : TStringList;

   function PuedeModificarTarjetasAnteriores: Boolean;
   begin
     Result := FALSE;
   end;


   procedure SetParametros( iEmpleado : integer; dFechaOriginal, dFechaNueva : TDatetime );
   begin
        oParametros := TZetaParams.Create( Self );
        with oParametros do
        begin
             AddDate( 'FechaOriginal', dFechaOriginal );
             AddDate( 'FechaNueva', dFechaNueva );
             AddBoolean( 'PuedeCambiarTarjeta', PuedeModificarTarjetasAnteriores );
             AddString( 'RangoLista', GetFiltroLista( 'CB_CODIGO', Format('%d', [iEmpleado]) )  );
             AddString( 'Condicion', VACIO );
             AddString( 'Filtro', VACIO );
        end;
   end;


   procedure CargarEmpleadosFestivo;
   const
         NODO_USUARIOWFN = 'PersonaID';
         NODO_EMPLEADO = 'EMPLEADO';
         NODO_FECHAORIGINAL = 'FECHAORIGINAL';
         NODO_FECHANUEVA = 'FECHANUEVA';
         NODO_NOTA = 'NOTA';
         NODO_COMENTA = 'COMENTA';

         NODO_EMPLEADOS_DATOS = 'INFO';
         NODO_EMPLEADOS_LISTA = 'EMPLEADOS';
    var
       Nodo, Info: TZetaXMLNode;
    begin
         with FXMLTools do
         begin

              Nodo := GetNode( NODO_EMPLEADOS_LISTA );
              { Preguntar si son varios empleados }
              if Assigned( Nodo ) then
              begin
                   Info := GetNode( NODO_EMPLEADOS_DATOS, Nodo );
                   while Assigned( Info ) and HasChildren( Info ) do
                   begin

                        EsquemaEmpleado := TEmpleadoBasico.Create;

                        EsquemaEmpleado.NumeroEmpleado :=TagAsInteger( GetNode( NODO_EMPLEADO, Info ), K_EMPLEADO_NULO );
                        EsquemaEmpleado.FechaOriginal := TagAsDateTime( GetNode( NODO_FECHAORIGINAL, Info), NullDateTime );
                        EsquemaEmpleado.FechaNueva := TagAsDateTime( GetNode( NODO_FECHANUEVA, Info), NullDateTime );
                        ListaEmpleados.Add(EsquemaEmpleado);
                        Info := GetNextSibling( Info );
                   end
              end;
         end;
         if ( ListaEmpleados.Count = 0 ) then
            raise Exception.Create( 'No se especific� un empleado' );
    end;

begin
     lOk := False;
     lOkProcess := False;
     Texto := TStringList.Create;
     ListaEmpleados := TObjectList.Create(true);
     CargarEmpleadosFestivo;
     for i := 0 to ( ListaEmpleados.Count - 1 ) do
     begin
          Empleado := TEmpleadoBasico(ListaEmpleados[i]);
          try
             if FetchEmployee( cdsLista, Empleado.NumeroEmpleado ) then
             begin

                    lOk := True; 
                    SetParametros( Empleado.NumeroEmpleado, Empleado.FechaOriginal, Empleado.FechaNueva );
                    try
                       EscribeBitacora( ServerAsistencia.IntercambioFestivo( dmCliente.Empresa, oParametros.VarValues ), Texto );
                       LeerMensajeBitacora( lOkProcess, Texto );
                    finally
                           FreeAndNil(oParametros);
                    end;
             end
             else
             begin
                  DataBaseError( 'No se encontr� el empleado # ' + IntToStr( Empleado.NumeroEmpleado ) );
             end;
          except
                on Error: Exception do
                begin
                     LogError( Empleado.NumeroEmpleado, 'Error al agregar intercambio de festivo: ' + Error.Message );
                end;
          end;
     end;

     if ( not lOkProcess ) then lOk := False;
     if ( not lOk ) then LogError( 0, Texto.Text );

     Result := lOk;

     FreeAndNil(ListaEmpleados);
     FreeAndNil(Texto);
end;



procedure TdmCliente.cdsEditHisKardexAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
   sPercepcionesFijas : string;
begin
     ErrorCount := 0;
     sPercepcionesFijas := VACIO;
     with cdsEditHisKardex do
     begin

          if State in [ dsEdit, dsInsert ] then
          begin
               if State = dsEdit then
                  FieldByName( 'CB_NOTA' ).AsString := FieldByName( 'CB_NOTA' ).AsString + ' ';
               Post;
          end;
          if ( ChangeCount > 0 ) then
          begin
               sPercepcionesFijas := GetPercepcionesFijas( cdsEditHisKardex.FieldByName( 'CB_CODIGO' ).AsInteger , cdsEditHisKardex.FieldByName( 'CB_FEC_ING' ).AsDateTime );
               OnReconcileError := ReconcileError;
               Reconcile ( ServerRecursos.GrabaHisKardex( dmCliente.Empresa, Delta, sPercepcionesFijas, ErrorCount) );
               if ErrorCount = 0 then
               begin
                    FRefrescaHisKardex:= TRUE;
                    {$ifndef INTERFAZ_AVENT}
                    {if ( FieldByName('CB_TIPO').AsString = K_T_ALTA ) and ( zStrToBool( FieldByName('CB_REINGRE').AsString ) ) then
                    begin
                         dmCliente.cdsEmpleado.Refrescar;

                         if ( FFechaIngreso <> dmCliente.cdsEmpleado.FieldByName( 'CB_FEC_ING' ).AsDateTime ) or
                            ( FFechaAntiguedad <> dmCliente.cdsEmpleado.FieldByName( 'CB_FEC_ANT' ).AsDateTime ) then
                         begin
                              SaldaVacacionesPendientes;
                         end;
                    end; }
                    {$endif}
               end
               else
               begin
                    LogError( 0, Self.Status );

               end;
          end;
     end;
end;

procedure TdmCliente.cdsEditHisKardexBeforePost(DataSet: TDataSet);
var
   sTipo : string;
   dFecha, dIngreso : TDate;
begin
     with dmCliente, cdsEditHisKardex do
     begin
          sTipo := FieldByName( 'CB_TIPO').AsString;
          dFecha := FieldByName( 'CB_FECHA').AsDateTime;
          dIngreso := cdsEmpleado.FieldByName( 'CB_FEC_ING' ).AsDateTime;
          // Valida Fecha del Movimiento
          if ( sTipo <> K_T_ALTA ) and ( dFecha < dIngreso ) then
             DataBaseError( Format( 'Fecha Movimiento [%s] Tiene que ser Mayor a Fecha de Alta del Empleado: %s',
                                    [ FechaCorta( dFecha ), FechaCorta( dIngreso ) ] ) );

          if ( sTipo = K_T_ALTA ) or ( sTipo = K_T_CAMBIO ) or ( sTipo = K_T_BAJA ) then
          begin
               if ( FieldByName( 'CB_LOT_IDS' ).AsString <> VACIO  ) and ( FieldByName( 'CB_FEC_IDS').AsDateTime = 0 ) then
               begin
                     DatabaseError('Debe capturarse la Fecha de Transacci�n IDSE');
               end;

               if ( FieldByName( 'CB_LOT_IDS' ).AsString = VACIO  ) and ( FieldByName( 'CB_FEC_IDS').AsDateTime > 0 ) then
               begin
                    DatabaseError('Debe capturarse el N�mero de Lote IDSE');
               end;

          end;
          { Se guardan las fechas para saber si se debe recalcular el ajuste por vacaciones }
          FFechaIngreso:= dIngreso;
          FFechaAntiguedad:= cdsEmpleado.FieldByName( 'CB_FEC_ANT' ).AsDateTime;
     end;
end;


function TdmCliente.AgregarChecadaWFN( const iUsuario:integer ) : Boolean;
var
   sEmpresa: string;
   Registros, Registro: TZetaXMLNode;

   dFecha : TDate;
   iEmpleado : Integer;
   sHora, sCredencial, sReloj, sLog, sNombreCompany, sMotivo, sFecha: string;

   oParams: TZetaParams;
   sResultado:string;

    procedure LlenaParametros( Parametros: TZetaParams; AliasEmpleado: String = ARROBA_TABLA );
    const
         NODO_EMPLEADO = 'EMPLEADO';
         NODO_EMPLEADOS_DATOS = 'INFO';
         NODO_EMPLEADOS_LISTA = 'EMPLEADOS';
    var
        oNodo, Info: TZetaXMLNode;
        sFechaTag : string;
        dFechaTime : TDatetime;
    begin
         with FXMLTools do
         begin
              oNodo := GetNode( NODO_EMPLEADOS_LISTA );
              if Assigned( oNodo ) then
              begin
                    Info := GetNode( NODO_EMPLEADOS_DATOS, oNodo );
                    with Parametros do
                    begin
                        dFecha := TagAsDate( GetNode( 'Fecha', Info), NullDateTime );
                        sFecha := TagAsString( GetNode( 'Fecha', Info), VACIO);
                        iEmpleado := TagAsInteger( GetNode( 'Empleado', Info ),0 );
                        sHora := TagAsString( GetNode( 'Hora', Info), VACIO);
                        sReloj := TagAsString( GetNode( 'Reloj', Info), VACIO );
                        sMotivo := TagAsString( GetNode( 'Motivo', Info), VACIO);
                        AddDate( 'Fecha', dFecha);
                        AddInteger( 'Empleado', iEmpleado);
                        AddString( 'Hora', sHora);
                        AddString( 'Credencial', sCredencial);
                        AddString( 'Reloj', sReloj);
                    end;
               end;
          end;
     end;

    function ObtenerLetraCredencial( NumeroEmpleado: Integer ): string;
    const
         K_CONSULTA_CREDENCIAL = 'SELECT  CB_CREDENC FROM Colabora where CB_CODIGO = %d';
    var
       sLetraCredencial: string;
       sQuery: string;
       oConsultaLetraCred: TClientDataSet;
    begin
         oConsultaLetraCred := TZetaClientDataSet.Create( self );
         sLetraCredencial := '';
         sQuery := '';
         sQuery := Format( K_CONSULTA_CREDENCIAL, [ NumeroEmpleado ] );
         oConsultaLetraCred.Data := ServerConsultas.GetQueryGralTodos( Empresa, sQuery );
         if ( not oConsultaLetraCred.Eof ) then
         begin
            sLetraCredencial := oConsultaLetraCred.FieldByName( 'CB_CREDENC' ).AsString
         end;
         Result := sLetraCredencial;
    end;


    function ComprobarMotivo( sMotivo: string ): boolean;
    const
         K_CONSULTA_MOTIVO = 'select TB_CODIGO, TB_ELEMENT from MOT_CHECA where TB_CODIGO = ''%s'' ';
    var
       sDescMotivo: string;
       sQuery: string;
       oConsulta: TClientDataSet;
    begin
         result := False;
         oConsulta := TZetaClientDataSet.Create( self );
         sDescMotivo := '';
         sQuery := '';
         sQuery := Format( K_CONSULTA_MOTIVO, [ sMotivo ] );
         oConsulta.Data := ServerConsultas.GetQueryGralTodos( Empresa, sQuery );
         if ( not oConsulta.Eof ) then
         begin
            sDescMotivo := oConsulta.FieldByName( 'TB_ELEMENT' ).AsString
         end;
         if (sDescMotivo <> VACIO) then
         begin
              Result := true;
         end;
    end;

    function ComprobarChecada(  const iEmpleado: integer; const  sHora : string; sFecha : string ): boolean;
    const
         K_CONSULTA_CHECADA = 'select CB_CODIGO, CH_H_REAL from checadas where CB_CODIGO = ''%d''  and CH_H_REAL = ''%s'' AND AU_FECHA = ''%s''  ';
    var
       sEmpleado: string;
       sQuery: string;
       oConsulta: TClientDataSet;
    begin
         result := False;
         oConsulta := TZetaClientDataSet.Create( self );
         sEmpleado := '';
         sQuery := '';
         sQuery := Format( K_CONSULTA_CHECADA, [iEmpleado, sHora, sFecha  ] );
         oConsulta.Data := ServerConsultas.GetQueryGralTodos( Empresa, sQuery );
         if ( not oConsulta.Eof ) then
         begin
            sEmpleado := oConsulta.FieldByName( 'CB_CODIGO' ).AsString
         end;
         if (sEmpleado <> VACIO) then
         begin
              Result := true;
         end;
    end;

    procedure UpdateChecada( const iEmpleado: integer; const sMotivo, sHora : string; sFecha : string);
    const
         K_ACTUALIZA_CHECADA = 'update checadas set CH_MOTIVO = ''%s'', US_CODIGO = ''%d''   where CB_CODIGO = ''%d''  and CH_H_REAL = ''%s'' AND AU_FECHA = ''%s'' ';
    Var
         sQuery : string;
    begin
        sQuery := '';
        if (sFecha <> VACIO) Then
        begin
             with Provider do
             begin
                  try
                     sQuery :=  Format( K_ACTUALIZA_CHECADA, [ sMotivo, iUsuario, iEmpleado, sHora, sFecha  ] );
                     ExecSQL( Empresa, sQuery);
                  except
                        on Error: Exception do
                        begin
                             sResultado := Error.Message;
                             raise Exception.Create( sResultado );
                        end;
                  end;
             end;
        end
        else
        begin
              sResultado := 'Fecha no asignada.';
              raise Exception.Create( sResultado );
        end;
    end;

begin
     Result := false;
     try
             try
                sResultado := VACIO;
                oParams := TZetaParams.Create( self );
                   sMotivo := VACIO;
                   LlenaParametros( oParams );
                   sNombreCompany := cdsCompany.FieldByName('CM_NOMBRE').AsString;
                   if ( SetEmpleadoNumero( cdsEmpleado, iEmpleado ) ) then
                   begin
                        sCredencial := ObtenerLetraCredencial(iEmpleado);
                        if (sCredencial <> VACIO) then
                        begin
                             try
                                with oParams do
                                begin
                                     AddString( 'Credencial', sCredencial);
                                end;
                                if (sMotivo <> VACIO) then
                                begin
                                      if(ComprobarMotivo(sMotivo)) then
                                      begin
                                           if((sFecha <> VACIO) and (sHora <> VACIO)) then
                                           begin
                                                if (ComprobarChecada(iEmpleado, sHora, sFecha) <> true) then
                                                begin
                                                     FServerCalcNomina := nil;
                                                     sResultado := ServerCalcNomina.ProcesarTarjetasSimple( Empresa, oParams.VarValues);
                                                     UpdateChecada(iEmpleado, sMotivo, sHora, sFecha);
                                                end
                                                else
                                                begin
                                                     sResultado := 'Ya existe una checada con esos valores.';
                                                     raise Exception.Create( sResultado );
                                                     result := false;
                                                end;
                                           end
                                           else
                                           begin
                                                 sResultado := 'Verifique Fecha, Hora.';
                                                 raise Exception.Create( sResultado );
                                                 result := false;
                                           end;

                                      end
                                      else
                                      begin
                                           sResultado := 'No existe ese motivo.';
                                           raise Exception.Create( sResultado );
                                           result := false;
                                      end;
                                end
                                else
                                begin
                                     sResultado :='No se ha seleccionado un motivo.';
                                     raise Exception.Create( sResultado );
                                     result := false;
                                end;
                             except
                             on Error: Exception do
                                begin
                                     sResultado := Error.Message;
                                     raise Exception.Create( sResultado );
                                     result := false;
                                end;
                             end;
                        end
                        else
                        begin
                             sResultado := Format( 'El Empleado #%d de la empresa %s no tiene asignada una letra de credencial.', [ iEmpleado, sNombreCompany ] );
                             raise Exception.Create( sResultado );
                        end;
                   end
                   else
                   begin
                        sResultado := Format( 'Empleado #%d no existe en la empresa %s:%s', [ iEmpleado, sEmpresa, sNombreCompany ] );
                        raise Exception.Create( sResultado );
                   end;
             finally
                       FreeAndNil( oParams );
             end;
     except
     on Error: Exception do
        begin
              LogError(iEmpleado, 'Error al checar: ' + sResultado );
              result := false;
        end;
     end;
end;


end.
