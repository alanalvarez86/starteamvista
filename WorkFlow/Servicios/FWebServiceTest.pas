unit FWebServiceTest;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ComCtrls, ExtCtrls,ZetaKeyCombo;

type
  TWebServiceTest = class(TForm)
    DestinatarioLBL: TLabel;
    Ejecutar: TBitBtn;
    URL: TEdit;
    Salir: TBitBtn;
    Label1: TLabel;
    Usuario: TEdit;
    Password: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Encoding: TZetaKeyCombo;
    Label4: TLabel;
    Autenticacion: TZetaKeyCombo;
    procedure EjecutarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

  end;
procedure ProbarWebservice;

implementation

uses ZetaDialogo,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists,
     IWorkFlowService1,
     WorkFlowService;

var
  WebServiceTest: TWebServiceTest;

{$R *.DFM}

procedure ProbarWebservice;
begin
     if not Assigned( WebServiceTest ) then
     begin
          WebServiceTest := TWebServiceTest.Create( Application );
     end;
     with WebServiceTest do
     begin
          ShowModal;
     end;
end;

{ ******** TEMailTest ************ }


procedure TWebServiceTest.FormShow(Sender: TObject);
begin
     ActiveControl := URL;
     Autenticacion.ListaFija := lfWFAutenticacionWebService;
     Encoding.ListaFija := lfWFCodificacionWebService;
end;

procedure TWebServiceTest.EjecutarClick(Sender: TObject);
var
   oCursor: TCursor;
   ws : IWorkFlowService;
   ws2 : IWorkflowServiceserviceSoap;
   ReturnValue : string ;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           if ( coWebServiceRPC = eWFCodificacionWebService( Encoding.Valor ) ) then
           begin
                ws := IWorkFlowService1.GetIWorkflowService( False, URL.Text);
                ReturnValue := ws.TressGrabar( '', 0 );
           end
           else
           begin
                ws2 := WorkflowService.GetIWorkflowServiceserviceSoap( False, URL.Text);
                ReturnValue := ws2.ComunicaTress( '<DATOS/>', 0 );
           end;
           ZetaDialogo.ZInformation('Prueba Webservice', ReturnValue, 0 );
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
            //FreeAndNil (ws);
            //FreeAndNil (ws2);
     end;
end;

end.
