object WebServiceTest: TWebServiceTest
  Left = 363
  Top = 232
  Width = 384
  Height = 210
  ActiveControl = URL
  Caption = 'Prueba de EMail'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  DesignSize = (
    376
    176)
  PixelsPerInch = 96
  TextHeight = 13
  object DestinatarioLBL: TLabel
    Left = 48
    Top = 17
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = 'URL:'
  end
  object Label1: TLabel
    Left = 34
    Top = 68
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Usuario:'
  end
  object Label2: TLabel
    Left = 16
    Top = 91
    Width = 57
    Height = 13
    Alignment = taRightJustify
    Caption = 'Contrase'#241'a:'
  end
  object Label3: TLabel
    Left = 25
    Top = 117
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Encoding:'
  end
  object Label4: TLabel
    Left = 5
    Top = 42
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Autenticaci'#243'n:'
  end
  object Ejecutar: TBitBtn
    Left = 106
    Top = 141
    Width = 75
    Height = 27
    Hint = 'Enviar Un eMail de Prueba'
    Anchors = [akTop, akRight]
    Caption = '&Enviar'
    Default = True
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnClick = EjecutarClick
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333333333333333333FFFFFFFFFFFFFFF000000000000
      000077777777777777770FFFFFFFFFFFFFF07F3333FFF33333370FFFF777FFFF
      FFF07F333777333333370FFFFFFFFFFFFFF07F3333FFFFFF33370FFFF777777F
      FFF07F33377777733FF70FFFFFFFFFFF99907F3FFF33333377770F777FFFFFFF
      9CA07F77733333337F370FFFFFFFFFFF9A907FFFFFFFFFFF7FF7000000000000
      0000777777777777777733333333333333333333333333333333333333333333
      3333333333333333333333333333333333333333333333333333333333333333
      3333333333333333333333333333333333333333333333333333}
    NumGlyphs = 2
  end
  object URL: TEdit
    Left = 76
    Top = 14
    Width = 292
    Height = 21
    TabOrder = 0
  end
  object Salir: TBitBtn
    Left = 188
    Top = 143
    Width = 75
    Height = 25
    Hint = 'Abandonar Esta Prueba'
    Caption = '&Salir'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    Kind = bkClose
  end
  object Usuario: TEdit
    Left = 76
    Top = 64
    Width = 292
    Height = 21
    TabOrder = 3
  end
  object Password: TEdit
    Left = 76
    Top = 89
    Width = 292
    Height = 21
    TabOrder = 4
  end
  object Encoding: TZetaKeyCombo
    Left = 76
    Top = 114
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 5
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
  end
  object Autenticacion: TZetaKeyCombo
    Left = 76
    Top = 39
    Width = 145
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 6
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
  end
end
