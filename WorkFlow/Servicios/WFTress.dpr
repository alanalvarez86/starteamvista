library WFTress;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  SysUtils,
  Classes,
  ActiveX,
  midasLib,
  Windows,
  DBasicoCliente in '..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  FProcesadorTress in 'FProcesadorTress.pas',
  DIncapacidad in 'DIncapacidad.pas' {dmIncapacidad: TDataModule},
  DPrestamos in 'DPrestamos.pas' {dmPrestamos: TDataModule};

{$R *.res}
var
   iGlobal : integer;
   FLock: TRTLCriticalSection;
   FObject: TObject;


function Lock: TObject;
begin
  EnterCriticalSection(FLock);
  Result:= FObject; //Note how this is called AFTER the lock is engaged
end;

procedure Unlock;
begin
  LeaveCriticalSection(FLock);
end;


// Example function takes an input integer and input string, and returns
// inputInt + 1, and inputString + ' ' + IntToStr(outputInt). If successful,
// the return result is true, otherwise errorMsgBuffer contains the the
// exception message string.
function DelphiFunction(inputInt : integer;
                        inputString : PAnsiChar;
                        out outputInt : integer;
                        outputStringBufferSize : integer;
                        var outputStringBuffer : PAnsiChar;
                        errorMsgBufferSize : integer;
                        var errorMsgBuffer : PAnsiChar)
                        : WordBool; stdcall; export;
var
  str : string;
  s : AnsiString;

begin
  outputInt := 0;
  try
    outputInt := inputInt + 1;
    s := inputString + ' ' + IntToStr(outputInt);
    StrLCopy(outputStringBuffer, PAnsiChar(s), outputStringBufferSize-1);

    errorMsgBuffer[0] := #0;
    Result := true;
  except
    on e : exception do
    begin
      StrLCopy(errorMsgBuffer, PAnsiChar(e.Message), errorMsgBufferSize-1);
      Result := false;
    end;
  end;
end;


function ProcesarPeticion( sXML : PAnsiChar;  iUsuario : integer; iFolio : integer;
                           var sRespuesta : PAnsiChar ) : WordBool; stdcall; export;

var
   procesador : TProcesadorTress;
   sLogStr : String;
   sLog : AnsiString;
begin

    try
       Lock;
       CoInitialize(nil);

    try
        procesador := TProcesadorTress.Create;
        procesador.ProcesarPeticion( string( sXML ), iUsuario,   sLogStr );
        sLog := AnsiString( sLogStr );

        sRespuesta := CoTaskMemAlloc(SizeOf(Char)*(Length(PAnsiChar(sLog))+1));
        StrCopy(sRespuesta, PAnsiChar(sLog));

        FreeAndNil ( procesador );
        Result := true;
     except
       on e : exception do
       begin
            sRespuesta := 'Error al procesar peticion en Sistema TRESS' ;
            StrCopy(sRespuesta, PAnsiChar(sLog));
            Result := false;
       end;
     end;

    finally
         CoUninitialize;
        Unlock;
    end;

end;



function GetConfiguracion( sXML : PAnsiChar;  iUsuario : integer; iFolio : integer;
                           var sRespuesta : PAnsiChar ) : WordBool; stdcall; export;

var
   procesador : TProcesadorTress;
   sLogStr : String;
   sLog : AnsiString;
begin


   try
      Lock;
      CoInitialize(nil);

     try
        procesador := TProcesadorTress.Create;
        procesador.GetConfiguracion( string( sXML ), iUsuario,   sLogStr );
        sLog := AnsiString( sLogStr );

        sRespuesta := CoTaskMemAlloc(SizeOf(Char)*(Length(PAnsiChar(sLog))+1));
        StrCopy(sRespuesta, PAnsiChar(sLog));

        FreeAndNil ( procesador );
        Result := true;
     except
       on e : exception do
       begin
            sRespuesta := 'Error al procesar peticion en Sistema TRESS' ;
            StrCopy(sRespuesta, PAnsiChar(sLog));
            Result := false;
       end;
     end;

    finally
         CoUninitialize;
        Unlock;
    end;

end;



// I would have thought having "export" at the end of the function declartion
// (above) would have been enough to export the function, but I couldn't get it
// to work without this line also.
exports DelphiFunction;

exports ProcesarPeticion;
exports GetConfiguracion;


begin
  IsMultiThread := True;
  InitializeCriticalSection(FLock);
end.
