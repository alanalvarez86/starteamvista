inherited dmIncapacidad: TdmIncapacidad
  OldCreateOrder = True
  Left = 700
  Top = 163
  Height = 277
  Width = 443
  inherited cdsCompany: TZetaClientDataSet
    Left = 40
  end
  object cdsLista: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 136
    Top = 16
  end
  object cdsComparte: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'CM_NOMBRE'
    Params = <>
    Left = 136
    Top = 72
  end
  object cdsEmpleado: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 232
    Top = 16
  end
  object cdsProcesos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 320
    Top = 16
  end
  object cdsLogDetail: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 232
    Top = 72
  end
  object cdsPassword: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 320
    Top = 80
  end
  object cdsHisIncapaci: TZetaClientDataSet
    Tag = 11
    Aggregates = <>
    IndexFieldNames = 'IN_FEC_INI'
    Params = <>
    BeforePost = cdsHisIncapaciBeforePost
    OnReconcileError = cdsHisIncapaciReconcileError
    AlAdquirirDatos = cdsHisIncapaciAlAdquirirDatos
    AlEnviarDatos = cdsHisIncapaciAlEnviarDatos
    Left = 42
    Top = 144
  end
  object cdsListaConflicto: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 132
    Top = 144
  end
  object cdsIncidencias: TZetaLookupDataSet
    Tag = 27
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    StoreDefs = True
    AlAdquirirDatos = cdsIncidenciasAlAdquirirDatos
    UsaCache = True
    LookupName = 'Incidencias'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 232
    Top = 149
  end
end
