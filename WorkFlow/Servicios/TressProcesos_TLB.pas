unit TressProcesos_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 07/08/2006 04:09:37 p.m. from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3WorkFlow\Servicios\TressProcesos.tlb (1)
// LIBID: {DBA0240B-7031-4BDC-9D05-ED0060D32B8A}
// LCID: 0
// Helpfile: 
// HelpString: TressExcel Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  TressProcesosMajorVersion = 1;
  TressProcesosMinorVersion = 0;

  LIBID_TressProcesos: TGUID = '{DBA0240B-7031-4BDC-9D05-ED0060D32B8A}';

  IID_ITressProcesos: TGUID = '{4DA1C2F4-6B85-4FCE-B9B2-6480F3583C95}';
  CLASS_TressProcesos_: TGUID = '{B405613C-75A8-4A8A-8A90-3B2D4CC1B0F6}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ITressProcesos = interface;
  ITressProcesosDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  TressProcesos_ = ITressProcesos;


// *********************************************************************//
// Interface: ITressProcesos
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {4DA1C2F4-6B85-4FCE-B9B2-6480F3583C95}
// *********************************************************************//
  ITressProcesos = interface(IDispatch)
    ['{4DA1C2F4-6B85-4FCE-B9B2-6480F3583C95}']
  end;

// *********************************************************************//
// DispIntf:  ITressProcesosDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {4DA1C2F4-6B85-4FCE-B9B2-6480F3583C95}
// *********************************************************************//
  ITressProcesosDisp = dispinterface
    ['{4DA1C2F4-6B85-4FCE-B9B2-6480F3583C95}']
  end;

// *********************************************************************//
// The Class CoTressProcesos_ provides a Create and CreateRemote method to          
// create instances of the default interface ITressProcesos exposed by              
// the CoClass TressProcesos_. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoTressProcesos_ = class
    class function Create: ITressProcesos;
    class function CreateRemote(const MachineName: string): ITressProcesos;
  end;

implementation

uses ComObj;

class function CoTressProcesos_.Create: ITressProcesos;
begin
  Result := CreateComObject(CLASS_TressProcesos_) as ITressProcesos;
end;

class function CoTressProcesos_.CreateRemote(const MachineName: string): ITressProcesos;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_TressProcesos_) as ITressProcesos;
end;

end.
