unit DPrestamos;

{$define LEONI}

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,{$ifndef VER130}Variants,{$endif}
     Recursos_TLB,
     Asistencia_TLB,
     Comparte_TLB,
     Login_TLB,
     Consultas_TLB,
     Nomina_TLB,
     Tablas_TLB,
     DZetaServerProvider,
     DBasicoCliente,
     DXMLTools,
     //ZetaCommonTypes,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZBaseThreads,
     ZetaClientDataSet, ZetaServerDataSet;
const
     K_NO_AUTORIZADO = 99;
     K_AUTORIZADO = -1;
     K_MAX_VAL_USO_DEMO = 5;
     K_TW_PRESTAMO_AGREGAR = 'PRESTAMO_AGREGAR';
type

  TdmPrestamos = class(TBasicoCliente)
    cdsLista: TZetaClientDataSet;
    cdsComparte: TZetaClientDataSet;
    cdsEmpleado: TZetaClientDataSet;
    cdsProcesos: TZetaClientDataSet;
    cdsLogDetail: TZetaClientDataSet;
    cdsPassword: TZetaClientDataSet;
    cdsHisPrestamos: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);

    procedure cdsHisPrestamosAlAdquirirDatos(Sender: TObject);
    procedure cdsHisPrestamosAlEnviarDatos(Sender: TObject);
  private
    { Private declarations }
    FXMLTools: TdmXMLTools;
    FStatus: String;
    FErrores: TStrings;
    FProcessData: TProcessInfo;
    FEmpleado: TNumEmp;
    FPrestamoAjustarFecha: integer;
    cdsPrestamos : TZetaClientDataSet;
    FCancelaAjuste : Boolean;
    FHuboCambios, FProcesandoThread, FHuboErrores, FHuboErroresInternos, FHuboMensajes, FHuboCambioEmpresa, FEmpresaValida, FErrorPrestamo : Boolean;
    FServerRecursos: IdmServerRecursosDisp;
    FServerComparte: IdmServerComparteDisp;
    FServerLogin: IdmServerLoginDisp;
    function GetServerRecursos: IdmServerRecursosDisp;
    function GetServerComparte: IdmServerComparteDisp;
    function GetServerLogin: IdmServerLoginDisp;
    function LogAsText: String;
    function GetAuto: OleVariant;
    procedure LogInit;
    procedure LogError( const iEmpleado: Integer; const sTexto: String );
    procedure ReconcileError(DataSet: {$ifdef VER130}TClientDataSet{$else}TCustomClientDataSet{$endif}; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    function ObtenValUso( const sTipo: String ): Integer;
    procedure GrabaValUso( const sTipo: String; const iUso: Integer );
    function SetEmpleadoNumero( oDataSet: TZetaClientDataSet; const iEmpleado: TNumEmp ): Boolean;
    function ValidaEnviar( DataSet : TZetaClientDataSet ): boolean;
    function GetDatosEmpleadoActivo: Boolean;


  protected
    { Protected declarations }

    property ServerRecursos: IdmServerRecursosDisp read GetServerRecursos;
    property ServerComparte: IdmServerComparteDisp read GetServerComparte;
    property ServerLogin : IdmServerLoginDisp read GetServerLogin;
    property Status: String read FStatus write FStatus;
    function InitCompanies: Boolean;
    function SetEmpresaActiva( const sCodigo: String ): Boolean;
    function TressInvocar( const sTipo: String; const iUsuario: Integer ): Boolean;
  public
    { Public declarations }
    property ProcessData: TProcessInfo read FProcessData;
    property ErrorPrestamo : Boolean read FErrorPrestamo write FErrorPrestamo;
    property PrestamoAjustarFecha : integer read FPrestamoAjustarFecha write FPrestamoAjustarFecha;
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    function TressGrabar(const sXML: String; const iUsuario: Integer; {$ifdef LEONI} const iFolio: integer; {$endif} var sLog: String): Boolean;
    function AgregarPrestamo(  const iUsuario:integer ) : Boolean;
    procedure CargarAutorizacion;
    procedure AsignarDataTemporal;
  end;

var
  dmPrestamos: TdmPrestamos;

implementation

uses FAutoClasses,
     ZetaCommonTools,
     ZetaClientTools,
     FToolsRH,
     ZetaRegistryCliente,
     ZetaServerTools,
     ZGlobalTress, DateUtils, XMLIntf, Masks;
{$R *.DFM}

const
     K_EMPLEADO_NULO = 0;

constructor TdmPrestamos.Create(AOwner: TComponent);
begin
     ZetaCommonTools.Environment;
     ZetaRegistryCliente.InitClientRegistry;
     inherited Create( AOwner );
end;

procedure TdmPrestamos.DataModuleCreate(Sender: TObject);
begin
     FXMLTools := TdmXMLTools.Create( Self );
     FErrores := TStringList.Create;
     inherited;
end;

destructor TdmPrestamos.Destroy;
begin
     ZetaRegistryCliente.ClearClientRegistry;
     inherited Destroy;
end;

procedure TdmPrestamos.DataModuleDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FErrores );
     FreeAndNil( FXMLTools );
end;
function TdmPrestamos.GetServerRecursos: IdmServerRecursosDisp;
begin
     Result:= IdmServerRecursosDisp( Self.CreaServidor( CLASS_dmServerRecursos, FServerRecursos ) );
end;

function TdmPrestamos.GetServerComparte: IdmServerComparteDisp;
begin
     Result:= IdmServerComparteDisp( Self.CreaServidor( CLASS_dmServerComparte, FServerComparte ) );
end;
function TdmPrestamos.GetServerLogin: IdmServerLoginDisp;
begin
     Result := IdmServerLoginDisp( Self.CreaServidor( CLASS_dmServerLogin,FServerLogin ) );
end;

function TdmPrestamos.GetAuto: OleVariant;
begin
     Result := GetServerLogin.GetAuto;
end;

{ ****** Manejos Internos ****** }

function TdmPrestamos.InitCompanies: Boolean;
begin
     with cdsCompany do
     begin
          if not Active or HayQueRefrescar then
          begin
               Data := Servidor.GetCompanys( Usuario, Ord( tc3Datos ) );
               ResetDataChange;
          end;
          Result := ( RecordCount > 0 );
     end;
end;

function TdmPrestamos.SetEmpresaActiva(const sCodigo: String): Boolean;
begin
     InitCompanies;
     Result := FindCompany( sCodigo );
     if Result then
     begin
          SetCompany;
     end;
end;

procedure TdmPrestamos.ReconcileError(DataSet: {$ifdef VER130}TClientDataSet{$else}TCustomClientDataSet{$endif}; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := raAbort;  // Aborta el reconcile para que regrese FALSE
     Status := E.Message;
end;

procedure TdmPrestamos.LogInit;
begin
     with FErrores do
     begin
          Clear;
     end;
end;

procedure TdmPrestamos.LogError( const iEmpleado: Integer; const sTexto: String );
begin
      with FErrores do
      begin
           BeginUpdate;
           try
              if( iEmpleado > 0 ) then
                Add( Format( 'Empleado #%d|%s' + CR_LF, [ iEmpleado, sTexto ] ) )
             else
              Add( sTexto );
           finally
                  EndUpdate;
           end;
      end;
end;

function TdmPrestamos.LogAsText: String;
begin
     with FErrores do
     begin
          Result := Text;
     end;
end;

{ ***** Metodos Bases ***** }
{ ***** Cargado de XML y Determinaci�n del Proceso A Ejecutar }

function TdmPrestamos.TressGrabar(const sXML: String; const iUsuario: Integer; {$ifdef LEONI} const iFolio: integer; {$endif} var sLog: String): Boolean;
var
   sEmpresa, sTipo: String;
begin
     Result := FALSE;
     sLog := '';
     try
        with FXMLTools do
        begin
             XMLAsText := sXML;
             sEmpresa := TagAsString( GetNode( 'EMPRESA' ), '' );
             sTipo := TagAsString( GetNode( 'MOV3' ), '' );
        end;
        if ZetaCommonTools.StrVacio( sEmpresa ) then
           sLog := 'Empresa No Especificada'
        else
        begin
             Usuario := iUsuario;    // Asigna el usuario activo a dmPrestamos - No se hace login ni se validan derechos de acceso
             if SetEmpresaActiva( sEmpresa ) then
             begin
                  LogInit;
                  Result := TressInvocar( sTipo, iUsuario );
                  sLog := LogAsText;
             end
             else
                 sLog := Format( 'Empresa %s No Existe', [ sEmpresa ] );
        end;
     except
           on Error: Exception do
           begin
                sLog := 'Error Al Examinar Documento: ' + Error.Message;
           end;
     end;
end;

function TdmPrestamos.TressInvocar( const sTipo: String; const iUsuario: Integer ): Boolean;
var
   iUso: Integer;
begin
     Status := '';
     iUso := ObtenValUso( sTipo );
     if( iUso < K_MAX_VAL_USO_DEMO )then
     begin
          if ( sTipo = K_TW_PRESTAMO_AGREGAR ) then // Agregar prestamo
             Result := AgregarPrestamo(iUsuario)
          else
          begin
               Result := False;
               LogError( 0, Format( 'Tipo de Movimiento Indeterminado ( Nodo TIPO ''%s'' )', [ sTipo ] ) );
          end;

          if Result then
          begin
               if( iUso > K_AUTORIZADO )then
                   GrabaValUso( sTipo, ( iUso + 1 ) );
          end;
     end
     else
     begin
          Result := False;
          if( iUso = K_NO_AUTORIZADO )then
              LogError( 0, 'M�dulo No Autorizado' )
          else
              LogError( 0, 'Modo DEMO' );
     end;
end;

function TdmPrestamos.ObtenValUso( const sTipo: String ): Integer;
var
   sUso: String;
   iUso: Integer;
   oDataSet : TZetaClientDataSet;
begin
     Result := 0;
     Autorizacion.Cargar( GetAuto );

     {$ifdef WF_NUBE}
     Result := K_AUTORIZADO;
     {$else}

     if( Autorizacion.OkModulo( okWorkFlow ) )then // Modulo autorizado
     begin
          if( Autorizacion.EsDemo )then
          begin
               oDataSet := TZetaClientDataSet.Create( self );
               try
                  with oDataSet do
                  begin
                       Data := ServerComparte.GetValidacionUso( sTipo );
                       if Not IsEmpty then
                       begin
                            First;
                            sUso := Decrypt( FieldByName( 'USO' ).AsString );
                            iUso := StrToIntDef( sUso, 0 );
                            Result := iUso;
                       end;
                  end;
               finally
                      FreeAndNil( oDataSet );
               end;
          end
          else
              Result := K_AUTORIZADO;
     end
     else
         Result := K_NO_AUTORIZADO;
     {$endif}
end;

procedure TdmPrestamos.GrabaValUso( const sTipo: String; const iUso: Integer );
var
   sUso: String;
begin
     sUso := Encrypt( Format( '%d', [ iUso ] ) );
     ServerComparte.GrabaValidacionUso( sTipo, sUso );
end;

procedure TdmPrestamos.CargarAutorizacion;
begin
     Autorizacion.Cargar( GetAuto );
end;


function TdmPrestamos.SetEmpleadoNumero( oDataSet: TZetaClientDataSet; const iEmpleado: TNumEmp ): Boolean;
var
   oDatos: OleVariant;
begin
     Result := Servidor.GetEmpleado( Empresa, iEmpleado, oDatos );
     if Result then
     begin
          with oDataSet do
          begin
               Data := oDatos;
               FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
          end;
     end;
end;


function TdmPrestamos.AgregarPrestamo( const iUsuario:integer ) : Boolean;
var
   Registros, Registro: TZetaXMLNode;
   sNombreCompany: string;
   sEmpresa: string;

   iEmpNumero : integer;

   oParametros: TZetaParams;

   lOk, lOkAux, lOkProcess, lOkProcessAux, lRegistro: Boolean;
   iNumero : Integer;
   sMensaje : string;

   procedure CargaParametros( Registro: TZetaXMLNode; var Parametros : TZetaParams);
   begin
        with FXMLTools do
         begin
              while Assigned( Registro ) do
              begin
                 with Parametros do
                 begin
                      AddInteger( 'iEmpNumero', AttributeAsInteger(Registro, 'EmpNumero',  K_EMPLEADO_NULO ) );//----------------
                      AddString( 'sTipoPrestamo', AttributeAsString(Registro, 'TipoDesCodigo', '' ) ); //----------------
                      AddString( 'sDescuentoReferencia',  AttributeAsString(Registro, 'DescuentoReferencia', '' ) );
                      AddDate( 'DDescuentoInicial', AttributeAsDateTime( Registro, 'DescuentoInicial', 0 ) );
                      AddFloat('fDescuentoMonto', AttributeAsFloat( Registro, 'DescuentoMonto', 0 ));
                      AddString( 'sDescuentoFormula', AttributeAsString(Registro, 'DescuentoFormula', '' ) );
                      AddString( 'sDescuentoSubCta', AttributeAsString(Registro, 'DescuentoSubCta', '' ) );
                      AddString( 'sDescuentoObserva', AttributeAsString(Registro, 'DescuentoObserva', '' ));
                      AddFloat('fDescuentoSolicita', AttributeAsFloat( Registro, 'DescuentoSolicita', 0 ));
                      AddFloat('fDescuentoInteres', AttributeAsFloat( Registro, 'DescuentoInteres', 0 ) );
                      AddFloat('fDescuentoMeses', AttributeAsFloat( Registro, 'DescuentoMeses', 0 ));
                      AddFloat('fDescuentoPagPer', AttributeAsFloat( Registro, 'DescuentoPagPer', 0 ));
                      AddFloat('fDescuentoPagos', AttributeAsFloat( Registro, 'DescuentoPagos', 0 ) );
                      AddFloat('fDescuentoTasa', AttributeAsFloat( Registro, 'DescuentoTasa', 0 ));
                 end;
                   break;
              end;
         end;
   end;

   procedure AgregarDatosDatasetPrestamo(var Parametros : TZetaParams);
   const
        VACIO = '';
   var
    iCodigo, iNumPrestamo : integer;
    sReferencia , sTipo : string;
    bEncontroClon : Boolean;
   begin
       sReferencia := Parametros.ParamByName('sDescuentoReferencia').AsString;
       sTipo := Parametros.ParamByName('sTipoPrestamo').AsString;
       iCodigo := Parametros.ParamByName( 'iEmpNumero' ).AsInteger;
       iNumPrestamo := 0;

        with cdsHisPrestamos do
        begin

        Refrescar;

			
        if  sReferencia = VACIO then
        begin
              iNumPrestamo :=   iNumPrestamo + 1;
              sReferencia :=  sTipo + IntToStr(iNumPrestamo);
              repeat
                  bEncontroClon := Locate('CB_CODIGO; PR_REFEREN; PR_TIPO', VarArrayOf([iCodigo, sReferencia, sTipo]),[]);
                  if bEncontroClon then
                  begin
                    iNumPrestamo :=   iNumPrestamo + 1;
                    sReferencia :=  sTipo + IntToStr(iNumPrestamo);
                  end;
              until (bEncontroClon = False);
			  end
        else
        begin
             bEncontroClon := Locate('CB_CODIGO; PR_REFEREN; PR_TIPO', VarArrayOf([iCodigo, sReferencia, sTipo]),[]);
             if bEncontroClon then
             begin
                DB.DataBaseError( 'Ya existe un prestamo de tipo '+sTipo+' con la referencia '+sReferencia+' para el empleado: '+ iCodigo.ToString());
             end;
        end;

			
            Append;
            FieldByName('CB_CODIGO').AsInteger := Parametros.ParamByName( 'iEmpNumero' ).AsInteger; //------------------
            FieldByName('PR_TIPO').AsString := Parametros.ParamByName('sTipoPrestamo').AsString; //---------------------
            FieldByName('PR_REFEREN').AsString := sReferencia; //------------
            FieldByName('PR_FECHA').AsDateTime := Parametros.ParamByName('DDescuentoInicial').AsDateTime;
            FieldByName('PR_MONTO').AsFloat :=  Parametros.ParamByName('fDescuentoMonto').AsFloat;
            FieldByName('PR_SALDO').AsFloat :=  Parametros.ParamByName('fDescuentoMonto').AsFloat;
            FieldByName('PR_FORMULA').AsString := Parametros.ParamByName('sDescuentoFormula').AsString;
            FieldByName('PR_SUB_CTA').AsString := Parametros.ParamByName('sDescuentoSubCta').AsString;
            FieldByName('PR_OBSERVA').AsString := Parametros.ParamByName('sDescuentoObserva').AsString;
            FieldByName('PR_MONTO_S').AsFloat :=  Parametros.ParamByName('fDescuentoSolicita').AsFloat;
            FieldByName('PR_NUMERO').AsFloat :=  iNumPrestamo;
            FieldByName('US_CODIGO').AsFloat :=  Usuario;

            FieldByName('PR_INTERES').AsFloat :=  Parametros.ParamByName('fDescuentoInteres').AsFloat;
            FieldByName('PR_MESES').AsFloat :=  Parametros.ParamByName('fDescuentoMeses').AsFloat;
            FieldByName('PR_PAG_PER').AsFloat :=  Parametros.ParamByName('fDescuentoPagPer').AsFloat;
            FieldByName('PR_PAGOS').AsFloat :=  Parametros.ParamByName('fDescuentoPagos').AsFloat;
            FieldByName('PR_TASA').AsFloat :=  Parametros.ParamByName('fDescuentoTasa').AsFloat;

        end;
   end;

   function ValidaDatos: Boolean;
   var
      lValido : Boolean;
   begin
        lValido := True;

        with cdsHisPrestamos do
        begin
          if ( FieldByName( 'CB_CODIGO' ).AsInteger <= 0 ) then
          begin
             DB.DataBaseError( 'Es necesario un n�mero de empleado mayor a cero' );
             lValido := False;
          end;

          if ZetaCommonTools.StrVacio( FieldByName( 'PR_REFEREN' ).AsString ) then
          begin
             DB.DataBaseError( 'La Referencia no puede quedar vac�a' );
             lValido := False;
          end;

          if ZetaCommonTools.StrVacio( FieldByName( 'PR_TIPO' ).AsString ) then
          begin
             DB.DataBaseError( 'El C�digo del pr�stamo no puede quedar vacio' );
             lValido := False;
          end;

        end;

        Result := lValido;
   end;

begin
     lOk := False;
     lOkAux := False;
     try
        try
           with FXMLTools do
           begin
                Registros := GetNode( 'REGISTROS' );
                sEmpresa := TagAsString( GetNode( 'EMPRESA' ), '' );
                if Assigned ( Registros ) then
                begin
                     LogError( 0, Format( '|Empresa %s|', [ sEmpresa ] ) );
                     if Assigned( Registros ) then
                     begin
                          Registro := GetNode( 'REGISTRO', Registros );
                          while Assigned( Registro ) do
                          begin
                               oParametros:= TZetaParams.Create;
                               CargaParametros( Registro, oParametros);
                               lRegistro := True;
                               iEmpNumero := oParametros.ParamByName( 'iEmpNumero' ).AsInteger;
                               if SetEmpleadoNumero( cdsEmpleado, oParametros.ParamByName( 'iEmpNumero' ).AsInteger ) then
                               begin
                                    cdsHisPrestamos.Conectar;
                                    AsignarDataTemporal;
                                    AgregarDatosDatasetPrestamo(oParametros);
                                    if( ValidaDatos ) then
                                    begin
                                        if ( ( not ValidaEnviar( cdsHisPrestamos) ) or ( ErrorPrestamo ) ) then
                                           lOk := True;
                                    end
                                    else
                                    begin
                                        cdsHisPrestamos.CancelUpdates;
                                        lOk := True;
                                    end;
                                    if ( ( lOk ) and ( lOkAux = False ) ) then lOkAux := True;
                               end
                               else
                               begin
                                    LogError( 0, Format( 'Empleado #%d no existe en la empresa %s:%s', [ iEmpNumero, sEmpresa, sNombreCompany ] ) );
                                    lOk := true;
                               end;
                               Registro := GetNextSibling( Registro );
                          end;
                          if not ( lRegistro ) then
                          begin
                               LogError( 0, 'Error al agregar prestamo: Nodo <REGISTRO> no especificado' );
                               lOk := True;
                          end;
                     end;
                end
                else
                begin
                     lOk := true;
                     LogError( 0, 'Error al agregar prestamo: Nodo <REGISTROS> no especificado' );
                end;
           end;
        except
              on Error: Exception do
              begin
                   lOk := true;
                   LogError( 0, 'Error al agregar prestamo: ' + Error.Message );
              end;
        end;
        if ( lOkAux ) then lOk := True;
        if not ( lOk ) then FErrores.Clear;
        Result := lOk;
     finally
            FreeAndNil( cdsHisPrestamos );
     end;
end;

function TdmPrestamos.ValidaEnviar ( DataSet : TZetaClientDataSet) : boolean;
begin
     Result := True;
     try
          DataSet.Enviar;
     except
          on Error : Exception do
          begin
               FHuboErrores := True;
               LogError( FEmpleado, Format( '%s', [ Error.Message ] ) );
               DataSet.CancelUpdates;
               Result := False;
          end;
     end;
end;

procedure TdmPrestamos.AsignarDataTemporal;
begin
     if cdsPrestamos = Nil then
     begin
          cdsPrestamos := TZetaClientDataSet.Create(Self);
     end;
     cdsPrestamos.Data := cdsHisPrestamos.Data;
end;

function TdmPrestamos.GetDatosEmpleadoActivo: Boolean;
begin
     with cdsEmpleado do
     begin
          Result := zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString );
     end;
end;

procedure TdmPrestamos.cdsHisPrestamosAlAdquirirDatos(Sender: TObject);
begin
      cdsHisPrestamos.Data := ServerRecursos.GetHisPrestamos( Empresa, FEmpleado );
end;

procedure TdmPrestamos.cdsHisPrestamosAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
   FMensajePrestamo : WideString;

   Parametros : TZetaParams;

begin
     ErrorCount := 0;
     with cdsHisPrestamos do
     begin
          PostData;
          if (Changecount > 0 ) then
          begin
                    begin
                       try
                         Parametros:= TZetaParams.Create;
                         with Parametros do
                         begin
                              AddInteger( 'CB_CODIGO', FieldByName('CB_CODIGO').AsInteger);//----------------
                              AddString( 'PR_TIPO',FieldByName('PR_TIPO').AsString); //----------------
                              AddString( 'PR_REFEREN', FieldByName('PR_REFEREN').AsString  );
                              AddDate( 'PR_FECHA', FieldByName('PR_FECHA').AsDateTime );
                              AddFloat('PR_MONTO', FieldByName('PR_MONTO').AsFloat);
                              AddFloat('PR_SALDO', FieldByName('PR_SALDO').AsFloat);
                              AddString( 'PR_FORMULA', FieldByName('PR_FORMULA').AsString );
                              AddString( 'PR_SUB_CTA', FieldByName('PR_SUB_CTA').AsString );
                              AddString( 'PR_OBSERVA',FieldByName('PR_OBSERVA').AsString );
                              AddFloat('PR_MONTO_S', FieldByName('PR_MONTO_S').AsFloat);
                              AddFloat('PR_INTERES', FieldByName('PR_INTERES').AsFloat );
                              AddFloat('PR_MESES',FieldByName('PR_MESES').AsFloat );
                              AddFloat('PR_PAG_PER',FieldByName('PR_PAG_PER').AsFloat);
                              AddFloat('PR_PAGOS', FieldByName('PR_PAGOS').AsFloat );
                              AddFloat('PR_TASA', FieldByName('PR_TASA').AsFloat);
                         end;
                         Reconcile ( ServerRecursos.GrabaPrestamos(Empresa, Parametros.VarValues, Delta, TRUE, FMensajePrestamo, ErrorCount));
                       finally
                             FreeAndNil(Parametros);
                       end;
                    end;
          end;
     end;
end;

end.
