program WorkFlowMgrApp;

uses
  MidasLib,
  Forms,
  FWorkFlowMgrTest in 'FWorkFlowMgrTest.pas' {WorkFlowManagerTest},
  IWorkflowService1 in 'IWorkflowService1.pas',
  WorkflowService in 'WorkflowService.pas',
  DBasicoCliente in '..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal};

{$R *.RES}
{$R WINDOWSXP.RES}

begin
  Application.Initialize;
  Application.Title := 'WorkFlow Manager';
  Application.CreateForm(TWorkFlowManagerTest, WorkFlowManagerTest);
  Application.CreateForm(TBasicoCliente, BasicoCliente);
  Application.CreateForm(TZetaDlgModal, ZetaDlgModal);
  Application.Run;
end.
