unit DIncapacidad;

{$define LEONI}

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,{$ifndef VER130}Variants,{$endif}
     {$ifdef DOS_CAPAS}
     DServerRecursos,
     DServerAsistencia,
     DServerNomina,
     ZetaSQLBroker,
     DServerComparte,
     DServerLogin,
     DServerConsultas,
     DServerTablas,
     {$else}
     Recursos_TLB,
     Asistencia_TLB,
     Comparte_TLB,
     Login_TLB,
     Consultas_TLB,
     Nomina_TLB,
     Tablas_TLB,
     {$endif}
     DZetaServerProvider,
     DBasicoCliente,
     DXMLTools,
     //ZetaCommonTypes,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZBaseThreads,
     ZetaClientDataSet, ZetaServerDataSet;
const
     K_NO_AUTORIZADO = 99;
     K_AUTORIZADO = -1;
     K_MAX_VAL_USO_DEMO = 5;
     K_TW_INCAPACIDAD_AGREGAR = 'INCAPACIDAD_AGREGAR';
type

  TdmIncapacidad = class(TBasicoCliente)
    cdsLista: TZetaClientDataSet;
    cdsComparte: TZetaClientDataSet;
    cdsEmpleado: TZetaClientDataSet;
    cdsProcesos: TZetaClientDataSet;
    cdsLogDetail: TZetaClientDataSet;
    cdsPassword: TZetaClientDataSet;
    cdsHisIncapaci: TZetaClientDataSet;
    cdsListaConflicto: TZetaClientDataSet;
    cdsIncidencias: TZetaLookupDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsHisIncapaciAlAdquirirDatos(Sender: TObject);
    procedure cdsHisIncapaciBeforePost(DataSet: TDataSet);
    procedure cdsHisIncapaciAlEnviarDatos(Sender: TObject);
    procedure cdsHisIncapaciReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
    procedure cdsIncidenciasAlAdquirirDatos(Sender: TObject);
    //procedure cdsEmpleadoAlEnviarDatos(Sender: TObject);
  private
    { Private declarations }
    FXMLTools: TdmXMLTools;
    FStatus: String;
    FErrores: TStrings;
    FProcessData: TProcessInfo;
    FEmpleado: TNumEmp;
    FIncapacidadAjustarFecha: integer;
    cdsIncapacidades : TZetaClientDataSet;
    FCancelaAjuste : Boolean;
    AgregandoKardex, FRefrescaHisKardex, FCambiosMultiples, FCambioVacaciones, FDesconectaVaca : Boolean;
    FHuboCambios, FProcesandoThread, FHuboErrores, FHuboErroresInternos, FHuboMensajes, FHuboCambioEmpresa, FEmpresaValida, FErrorIncapacidad : Boolean;
    {$ifdef DOS_CAPAS}
    function GetServerRecursos: TdmServerRecursos;
    function GetServerAsistencia: TdmServerAsistencia;
    function GetServerComparte: TdmServerComparte;
    function GetServerLogin: TdmServerLogin;
    function GetServerConsultas: TdmServerConsultas;
    fucntion GetServerNomina: TdmServerNomina;
    fucntion GetServerTablas: TdmServerTablas;
    {$else}
    FServerRecursos: IdmServerRecursosDisp;
    FServerAsistencia: IdmServerAsistenciaDisp;
    FServerComparte: IdmServerComparteDisp;
    FServerLogin: IdmServerLoginDisp;
    FServerConsultas: IdmServerConsultasDisp;
    FServerNomina: IdmServerNominaDisp;
    FServerTablas: IdmServerTablasDisp;
    function GetServerRecursos: IdmServerRecursosDisp;
    function GetServerAsistencia: IdmServerAsistenciaDisp;
    function GetServerComparte: IdmServerComparteDisp;
    function GetServerLogin: IdmServerLoginDisp;
    function GetServerConsultas: IdmServerConsultasDisp;
    function GetServerNomina: IdmServerNominaDisp;
    function GetServerTablas: IdmServerTablasDisp;
    {$endif}
    function LogAsText: String;
    function GetAuto: OleVariant;
    procedure LogInit;
    procedure LogError( const iEmpleado: Integer; const sTexto: String );
    procedure ReconcileError(DataSet: {$ifdef VER130}TClientDataSet{$else}TCustomClientDataSet{$endif}; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    function ObtenValUso( const sTipo: String ): Integer;
    procedure GrabaValUso( const sTipo: String; const iUso: Integer );
    function SetEmpleadoNumero( oDataSet: TZetaClientDataSet; const iEmpleado: TNumEmp ): Boolean;
    function ValidaEnviar ( DataSet : TZetaClientDataSet ) : boolean;
    function GetDatosEmpleadoActivo: Boolean;
    function GetFiltroIncapacidad: string;
    //Eventos de Bitacora
    procedure EscribeBitacora(Resultado: OleVariant);
    function Check( const Resultado: OleVariant ): Boolean;
    procedure LeerMensajeBitacora( var lOkProcess: Boolean; var Texto: TStringList );
    procedure GetProcessLog(const iProceso: Integer);
  protected
    { Protected declarations }
    {$ifdef DOS_CAPAS}
    property ServerRecursos :TdmServerRecursos read GetServerRecursos;
    property ServerAsistencia :TdmServerAsistencia read GetServerAsistencia;
    property ServerComparte: TdmServerComparte read GetServerComparte;
    property ServerLogin : TdmServerLogin read GetServerLogin;
    property ServerConsultas: TdmServerConsultas read GetServerConsultas;
    property ServerNomina: TdmServerNomina read GetServerNomina;
    property ServerTablas: TdmServerTablas read GetServerTablas;
    {$else}
    property ServerRecursos: IdmServerRecursosDisp read GetServerRecursos;
    property ServerAsistencia: IdmServerAsistenciaDisp read GetServerAsistencia;
    property ServerComparte: IdmServerComparteDisp read GetServerComparte;
    property ServerLogin : IdmServerLoginDisp read GetServerLogin;
    property ServerConsultas: IdmServerConsultasDisp read GetServerConsultas;
    property ServerNomina: IdmServerNominaDisp read GetServerNomina;
    property ServerTablas: IdmServerTablasDisp read GetServerTablas;
    {$endif}
    property Status: String read FStatus write FStatus;
    function InitCompanies: Boolean;
    function SetEmpresaActiva( const sCodigo: String ): Boolean;
    function TressInvocar( const sTipo: String; const iUsuario: Integer ): Boolean;
  public
    { Public declarations }
    property ProcessData: TProcessInfo read FProcessData;
    property RefrescaHisKardex : Boolean read FRefrescaHisKardex write FRefrescaHisKardex;
    property ErrorIncapacidad : Boolean read FErrorIncapacidad write FErrorIncapacidad;
    property IncapacidadAjustarFecha : integer read FIncapacidadAjustarFecha write FIncapacidadAjustarFecha;
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    function TressGrabar(const sXML: String; const iUsuario: Integer; {$ifdef LEONI} const iFolio: integer; {$endif} var sLog: String): Boolean;
    function AgregarIncapacidad(  const iUsuario:integer ) : Boolean;
    procedure CargarAutorizacion;
    procedure AsignarDataTemporal;
    function BuscarIncapacidadAnterior( const FechaInicial:TDate; const sTipo :string ):Boolean;
  end;

var
  dmIncapacidad: TdmIncapacidad;

implementation

uses FAutoClasses,
     ZetaCommonTools,
     ZetaClientTools,
     FToolsRH,
     ZetaRegistryCliente,
     ZetaServerTools,
     ZGlobalTress, DateUtils, XMLIntf, Masks;
{$R *.DFM}

const
     K_EMPLEADO_NULO = 0;

{ ********* TdmInfonavit ********** }

constructor TdmIncapacidad.Create(AOwner: TComponent);
begin
     ZetaCommonTools.Environment;
     ZetaRegistryCliente.InitClientRegistry;
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     {$endif}
     inherited Create( AOwner );
end;

procedure TdmIncapacidad.DataModuleCreate(Sender: TObject);
begin
     FXMLTools := TdmXMLTools.Create( Self );
     {$ifdef DOS_CAPAS}
     FServerRecursos := TdmServerRecursos.Create( Self );
     FServerLogin := TdmServerLogin.Create( Self );
     {$endif}
     FErrores := TStringList.Create;
     inherited;
end;

destructor TdmIncapacidad.Destroy;
begin
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll(TRUE);
     DZetaServerProvider.FreeAll;
     {$endif}
     ZetaRegistryCliente.ClearClientRegistry;
     inherited Destroy;
end;

procedure TdmIncapacidad.DataModuleDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FErrores );
     FreeAndNil( FXMLTools );
     {$ifdef DOS_CAPAS}
     FreeAndNil( FServerLogin );
     FreeAndNil( FServerRecursos );
     {$endif}
end;

{$ifdef DOS_CAPAS}
function TdmIncapacidad.GetServerRecursos: TdmServerRecursos;
begin
     Result:= FServerRecursos;
end;
function TdmIncapacidad.GetServerAsistencia: TdmServerAsistencia;
begin
     Result:= FServerAsistencia;
end;
function TdmIncapacidad.GetServerComparte: TdmServerComparte;
begin
     Result:= FServerComparte;
end;
function TdmIncapacidad.GetServerLogin: TdmServerLogin;
begin
     Result := FServerLogin;
end;
function TdmIncapacidad.GetServerConsultas: TdmServerConsultas;
begin
     Result := FServerConsultas;
end;
function TdmIncapacidad.GetServerNomina: TdmServerNomina;
begin
    Result := FServerNomina;
end;
function TdmIncapacidad.GetServerTablas: TdmServerTablas;
begin
    Result := FServerTablas;
end;
{$else}
function TdmIncapacidad.GetServerRecursos: IdmServerRecursosDisp;
begin
     Result:= IdmServerRecursosDisp( Self.CreaServidor( CLASS_dmServerRecursos, FServerRecursos ) );
end;
function TdmIncapacidad.GetServerAsistencia: IdmServerAsistenciaDisp;
begin
     Result:= IdmServerAsistenciaDisp( Self.CreaServidor( CLASS_dmServerAsistencia, FServerAsistencia ) );
end;
function TdmIncapacidad.GetServerComparte: IdmServerComparteDisp;
begin
     Result:= IdmServerComparteDisp( Self.CreaServidor( CLASS_dmServerComparte, FServerComparte ) );
end;
function TdmIncapacidad.GetServerLogin: IdmServerLoginDisp;
begin
     Result := IdmServerLoginDisp( Self.CreaServidor( CLASS_dmServerLogin,FServerLogin ) );
end;
function TdmIncapacidad.GetServerConsultas: IdmServerConsultasDisp;
begin
     Result := IdmServerConsultasDisp( Self.CreaServidor( CLASS_dmServerConsultas, FServerConsultas ) );
end;
function TdmIncapacidad.GetServerNomina : IdmServerNominaDisp;
begin
     Result := IdmServerNominaDisp( Self.CreaServidor( CLASS_dmServerNomina, FServerNomina ) );
end;
function TdmIncapacidad.GetServerTablas : IdmServerTablasDisp;
begin
     Result := IdmServerTablasDisp( Self.CreaServidor( CLASS_dmServerTablas, FServerTablas ) );
end;
{$endif}

function TdmIncapacidad.GetAuto: OleVariant;
begin
     Result := GetServerLogin.GetAuto;
end;

{ ****** Manejos Internos ****** }

function TdmIncapacidad.InitCompanies: Boolean;
begin
     with cdsCompany do
     begin
          if not Active or HayQueRefrescar then
          begin
               Data := Servidor.GetCompanys( Usuario, Ord( tc3Datos ) );
               ResetDataChange;
          end;
          Result := ( RecordCount > 0 );
     end;
end;

function TdmIncapacidad.SetEmpresaActiva(const sCodigo: String): Boolean;
begin
     InitCompanies;
     Result := FindCompany( sCodigo );
     if Result then
     begin
          SetCompany;
     end;
end;

procedure TdmIncapacidad.ReconcileError(DataSet: {$ifdef VER130}TClientDataSet{$else}TCustomClientDataSet{$endif}; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := raAbort;  // Aborta el reconcile para que regrese FALSE
     Status := E.Message;
end;

procedure TdmIncapacidad.LogInit;
begin
     with FErrores do
     begin
          Clear;
     end;
end;

procedure TdmIncapacidad.LogError( const iEmpleado: Integer; const sTexto: String );
begin
      with FErrores do
      begin
           BeginUpdate;
           try
              if( iEmpleado > 0 ) then
                Add( Format( 'Empleado #%d|%s' + CR_LF, [ iEmpleado, sTexto ] ) )
             else
              Add( sTexto );
           finally
                  EndUpdate;
           end;
      end;
end;

function TdmIncapacidad.LogAsText: String;
begin
     with FErrores do
     begin
          Result := Text;
     end;
end;

{ ***** Metodos Bases ***** }
{ ***** Cargado de XML y Determinaci�n del Proceso A Ejecutar }

function TdmIncapacidad.TressGrabar(const sXML: String; const iUsuario: Integer; {$ifdef LEONI} const iFolio: integer; {$endif} var sLog: String): Boolean;
var
   sEmpresa, sTipo: String;
begin
     Result := FALSE;
     sLog := '';
     try
        with FXMLTools do
        begin
             XMLAsText := sXML;
             sEmpresa := TagAsString( GetNode( 'EMPRESA' ), '' );
             sTipo := TagAsString( GetNode( 'MOV3' ), '' );
        end;
        if ZetaCommonTools.StrVacio( sEmpresa ) then
           sLog := 'Empresa No Especificada'
        else
        begin
             Usuario := iUsuario;    // Asigna el usuario activo a dmIncapacidad - No se hace login ni se validan derechos de acceso
             if SetEmpresaActiva( sEmpresa ) then
             begin
                  LogInit;
                  Result := TressInvocar( sTipo, iUsuario );
                  sLog := LogAsText;
             end
             else
                 sLog := Format( 'Empresa %s No Existe', [ sEmpresa ] );
        end;
     except
           on Error: Exception do
           begin
                sLog := 'Error Al Examinar Documento: ' + Error.Message;
           end;
     end;
end;

function TdmIncapacidad.TressInvocar( const sTipo: String; const iUsuario: Integer ): Boolean;
var
   iUso: Integer;
begin
     Status := '';
     iUso := ObtenValUso( sTipo );
     if( iUso < K_MAX_VAL_USO_DEMO )then
     begin
          if ( sTipo = K_TW_INCAPACIDAD_AGREGAR ) then //PENSKE Agregar incapacidad
             Result := AgregarIncapacidad(iUsuario)
          else
          begin
               Result := False;
               LogError( 0, Format( 'Tipo de Movimiento Indeterminado ( Nodo TIPO ''%s'' )', [ sTipo ] ) );
          end;

          if Result then
          begin
               // En K_AUTORIZADO representa que no est� en modo DEMO. Por lo que no debe actualizar.
               if( iUso > K_AUTORIZADO )then
                   GrabaValUso( sTipo, ( iUso + 1 ) );
          end;
     end
     else
     begin
          Result := False;
          if( iUso = K_NO_AUTORIZADO )then
              LogError( 0, 'M�dulo No Autorizado' )
          else
              LogError( 0, 'Modo DEMO' );
     end;
end;

function TdmIncapacidad.ObtenValUso( const sTipo: String ): Integer;
var
   sUso: String;
   iUso: Integer;
   oDataSet : TZetaClientDataSet;
begin
     Result := 0;
     Autorizacion.Cargar( GetAuto );

     {$ifdef WF_NUBE}
     Result := K_AUTORIZADO;
     {$else}

     if( Autorizacion.OkModulo( okWorkFlow ) )then // Modulo autorizado
     begin
          if( Autorizacion.EsDemo )then
          begin
               oDataSet := TZetaClientDataSet.Create( self );
               try
                  with oDataSet do
                  begin
                       Data := ServerComparte.GetValidacionUso( sTipo );
                       if Not IsEmpty then
                       begin
                            First;
                            sUso := Decrypt( FieldByName( 'USO' ).AsString );
                            iUso := StrToIntDef( sUso, 0 );
                            Result := iUso;
                       end;
                  end;
               finally
                      FreeAndNil( oDataSet );
               end;
          end
          else
              Result := K_AUTORIZADO;
     end
     else
         Result := K_NO_AUTORIZADO;
     {$endif}
end;

procedure TdmIncapacidad.GrabaValUso( const sTipo: String; const iUso: Integer );
var
   sUso: String;
begin
     sUso := Encrypt( Format( '%d', [ iUso ] ) );
     ServerComparte.GrabaValidacionUso( sTipo, sUso );
end;

procedure TdmIncapacidad.CargarAutorizacion;
begin
     Autorizacion.Cargar( GetAuto );
end;


function TdmIncapacidad.SetEmpleadoNumero( oDataSet: TZetaClientDataSet; const iEmpleado: TNumEmp ): Boolean;
var
   oDatos: OleVariant;
begin
     Result := Servidor.GetEmpleado( Empresa, iEmpleado, oDatos );
     if Result then
     begin
          with oDataSet do
          begin
               Data := oDatos;
               FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
          end;
     end;
end;

{ **** FIN *****}



{ ***** Implementaciones de Llamadas al Servidor de Procesos ***** }

function TdmIncapacidad.AgregarIncapacidad( const iUsuario:integer ) : Boolean;
const
     K_MESS_ERR_EMP_INACTIVO = 'Empleado Inactivo, no se puede registrar Baja.';
     K_MESS_ERR_FECHA_ING = 'La Fecha de Modificaci�n [%s] deber� ser despu�s de la Fecha de Ingreso [%s].';
     K_MESS_ERR_FECHA_INC = 'La Fecha de Incapacidad [%s] deber� ser despu�s de la Fecha de Ingreso [%s].';
     K_MESS_ERR_NO_VALIDPERIODOS_INCA = 'No hay periodo definido para la Incapacidad en Sistema TRESS. No se registra la Incapacidad.';
var
   Registros, Registro: TZetaXMLNode;
   sNombreCompany: string;
   sEmpresa: string;
   iEmpNumero: Integer;
   dIncapacidadInicial: TDateTime;
   iIncapacidadDias: Integer;
   sIncapacidadNumero: string;
   sIncapacidadTipo: string;
   sIncapacidadObserva: string;
   iIncapacidadClaseMotivo: Integer;
   iIncapacidadTipoFin: Integer;
   dIncapacidadSUAIni, dIncapacidadSUAFin, dIncapacidadFechaRH : TDateTime;
   dIncapaPermanente, dIncapacidadDiasSubsidiar: Real;
   iPeriodoYear, iTipoNominaCodigo, iPeriodoNumero: Integer;
   oConsultaPeriodo : TClientDataSet;
   iIncapacidadAjustarFecha: integer; //{0=No ajustar|1=Ajustar fecha inicio|}

   lOk, lOkAux, lOkProcess, lOkProcessAux, lRegistro: Boolean;
   dFecha : TDate;
   iNumero : Integer;
   sMensaje : string;

   procedure CargaParametros( Registro: TZetaXMLNode; var iEmpNumero: Integer; var dIncapacidadInicial: TDateTime; var iIncapacidadDias : Integer; var sIncapacidadNumero: string; var sIncapacidadTipo: string; var sIncapacidadObserva: string; var iIncapacidadClaseMotivo: Integer; var iIncapacidadTipoFin: Integer; var dIncapacidadSUAIni, dIncapacidadSUAFin, dIncapacidadFechaRH : TDateTime; var dIncapaPermanente: Real; var iPeriodoYear, iTipoNominaCodigo, iPeriodoNumero: Integer; var dIncapacidadDiasSubsidiar: Real; var iIncapacidadAjustarFecha: integer );
   begin
        with FXMLTools do
         begin
              while Assigned( Registro ) do
              begin
                   iEmpNumero := AttributeAsInteger(Registro, 'EmpNumero',  K_EMPLEADO_NULO );
                   dIncapacidadInicial := AttributeAsDateTime( Registro, 'IncapacidadInicial', 0 );
                   iIncapacidadDias := AttributeAsInteger(Registro, 'IncapacidadDias',  K_EMPLEADO_NULO );
                   sIncapacidadNumero := AttributeAsString(Registro, 'IncapacidadNumero', '' );
                   sIncapacidadTipo := AttributeAsString(Registro, 'IncapacidadTipo', '' );
                   sIncapacidadObserva := AttributeAsString(Registro, 'IncapacidadObserva', '' );
                   iIncapacidadClaseMotivo := AttributeAsInteger(Registro, 'IncapacidadClaseMotivo',  K_EMPLEADO_NULO );
                   iIncapacidadTipoFin := AttributeAsInteger(Registro, 'IncapacidadTipoFin',  K_EMPLEADO_NULO );
                   iIncapacidadAjustarFecha := AttributeAsInteger(Registro, 'IncapacidadAjustarFecha',  0 );
                   //Extras
                   dIncapacidadSUAIni := AttributeAsDateTime( Registro, 'IncapacidadSUAInicial', 0 );
                   dIncapacidadSUAFin := AttributeAsDateTime( Registro, 'IncapacidadSUAFinal', 0 );
                   dIncapacidadFechaRH := AttributeAsDateTime( Registro, 'IncapacidadFechaRH', 0 );
                   dIncapaPermanente := AttributeAsFloat( Registro, 'IncapacidadPermanente', 0 );
                   //Datos Nomina
                   iPeriodoYear := AttributeAsInteger( Registro, 'PeriodoYear', 0 );
                   iTipoNominaCodigo := AttributeAsInteger( Registro, 'TipoNominaCodigo', 0 );
                   iPeriodoNumero := AttributeAsInteger( Registro, 'PeriodoNumero', 0 );
                   dIncapacidadDiasSubsidiar := AttributeAsFloat( Registro, 'IncapacidadDiasSubsidiar', 0 );
                   break;
              end;
         end;
   end;

   procedure AgregarDatosDatasetIncapacidad( iEmpNumero: Integer; dIncapacidadInicial: TDateTime; iIncapacidadDias : Integer; sIncapacidadNumero: string; sIncapacidadTipo: string; sIncapacidadObserva: string; iIncapacidadClaseMotivo: Integer; iIncapacidadTipoFin: Integer; dIncapacidadSUAIni, dIncapacidadSUAFin, dIncapacidadFechaRH : TDateTime; dIncapaPermanente: Real; iPeriodoYear, iTipoNominaCodigo, iPeriodoNumero: Integer; dIncapacidadDiasSubsidiar: Real );
   begin
        with cdsHisIncapaci do
        begin
             Append;
             cdsHisIncapaci.FieldByName('CB_CODIGO').AsInteger   := iEmpNumero;
             cdsHisIncapaci.FieldByName('IN_DIAS').AsInteger  := iIncapacidadDias;
             cdsHisIncapaci.FieldByName('IN_NUMERO').AsString  := UpperCase( sIncapacidadNumero );
             cdsHisIncapaci.FieldByName('IN_TIPO').AsString  := sIncapacidadTipo;
             cdsHisIncapaci.FieldByName('IN_COMENTA').AsString  := sIncapacidadObserva;
             cdsHisIncapaci.FieldByName('IN_MOTIVO').AsInteger:= iIncapacidadClaseMotivo;
             cdsHisIncapaci.FieldByName('IN_FIN').AsInteger:= iIncapacidadTipoFin;
             cdsHisIncapaci.FieldByName('IN_FEC_INI').AsDateTime := dIncapacidadInicial;
             // La fecha de regreso se calcular� autom�ticamente.
             cdsHisIncapaci.FieldByName('IN_FEC_FIN').AsDateTime := dIncapacidadInicial + iIncapacidadDias;
             //Extras
             cdsHisIncapaci.FieldByName('IN_SUA_FIN').AsDateTime := dIncapacidadSUAFin;
             cdsHisIncapaci.FieldByName('IN_SUA_INI').AsDateTime := dIncapacidadSUAIni;
             cdsHisIncapaci.FieldByName( 'IN_FEC_RH' ).AsDateTime := dIncapacidadFechaRH;
             cdsHisIncapaci.FieldByName( 'IN_TASA_IP').AsFloat := dIncapaPermanente;
             //Calculo de A�o
             cdsHisIncapaci.FieldByName('IN_NOMYEAR').AsInteger := iPeriodoYear;
             //Calculo de Tipo
             cdsHisIncapaci.FieldByName('IN_NOMTIPO').AsInteger := iTipoNominaCodigo;
             //Calculo de Numero
             cdsHisIncapaci.FieldByName('IN_NOMNUME').AsInteger := iPeriodoNumero;
             cdsHisIncapaci.FieldByName('IN_DIASSUB').AsFloat := dIncapacidadDiasSubsidiar;
        end;
   end;

   function ValidarTipoIncapacidad(sIncapacidadTipo: string ): Boolean;
   var
      sDescrip: String;
   begin
        Result := True;
        cdsIncidencias.Conectar;
        if ( strLleno( sIncapacidadTipo ) ) and
             ( not cdsIncidencias.LookupKey( sIncapacidadTipo, GetFiltroIncapacidad, sDescrip ) ) then
             Result := False;
   end;

   function ValidaPeriodo( iTipoNominaCodigo, iPeriodoYear, iPeriodoNumero: Integer ): Boolean;
   const
        K_CONSULTA = 'SELECT PE_NUMERO, PE_YEAR from PERIODO where ( PE_USO=0 ) and ( PE_YEAR = ''%d'' ) and  ( PE_TIPO = ''%d'' ) and ( PE_NUMERO = ''%d'' )';
   begin
        Result := False;
        oConsultaPeriodo.Data := ServerConsultas.GetQueryGralTodos( Empresa, Format( K_CONSULTA, [ iPeriodoYear , iTipoNominaCodigo , iPeriodoNumero ] ) );
        if( not oConsultaPeriodo.Eof ) then
        begin
            Result := True;
        end;
   end;

   function ValidaDatos: Boolean;
   var
      lValido : Boolean;
      sMask : string;
   begin
        sMask := '[A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9]';
        lValido := True;
        dFecha :=  cdsHisIncapaci.FieldByName('IN_FEC_INI').AsDateTime;

        if ( cdsHisIncapaci.FieldByName( 'IN_FEC_INI').AsDateTime = 0 ) then
        begin
             LogError( FEmpleado, 'Fecha de Inicio de Incapacidad no puede quedar Vac�a' );
             lValido := False;
        end;
        if ( cdsHisIncapaci.FieldByName( 'IN_DIAS').AsInteger <= 0 ) then
        begin
             LogError( FEmpleado, 'Dias de Incapacidad debe ser mayor a 0');
             lValido := False;
        end;
        if NOT StrLleno(cdsHisIncapaci.FieldByName( 'IN_TIPO').AsString ) then
        begin
             LogError( FEmpleado, 'Tipo de Incapacidad no puede quedar vac�o') ;
             lValido := False;
        end;

        if ( cdsHisIncapaci.FieldByName( 'IN_DIAS' ).AsFloat < cdsHisIncapaci.FieldByName( 'IN_DIASSUB' ).AsFloat ) then
        begin
             LogError( FEmpleado, 'Los d�as a subsidiar son mayores a los d�as de Incapacidad' );
             lValido := False;
        end;
        
        if( lValido ) then
        begin
             if not ( GetDatosEmpleadoActivo ) then
             begin
                  //Empleado activo, no se reingresa
                  LogError ( FEmpleado , K_MESS_ERR_EMP_INACTIVO );
                  lValido := False;
             end
             else
             if ( dFecha < cdsEmpleado.FieldByName ( 'CB_FEC_ING' ).AsDateTime ) then
             begin
                  // Las fechas son invalidas
                  LogError ( FEmpleado , Format ( K_MESS_ERR_FECHA_INC, [DateToStr ( dFecha ), DateToStr ( cdsEmpleado.FieldByName ( 'CB_FEC_ING' ).AsDateTime )] ) );
                  lValido := False;
             end
             else
             begin
                  if not ( ValidarTipoIncapacidad( Trim( sIncapacidadTipo ) ) ) then //Si No es Valido
                  begin
                       LogError( FEmpleado, Format( 'No Existe el C�digo de Tipo de Incidencia: %s', [ sIncapacidadTipo ] ) );
                       lValido := False;
                  end
                  else
                  begin
                       iNumero := cdsHisIncapaci.FieldByName('IN_MOTIVO').AsInteger;
                       if  iNumero =  Ord(miSubsecuente ) then
                       begin
                            if not ( BuscarIncapacidadAnterior( dFecha, cdsHisIncapaci.FieldByName('IN_TIPO').AsString )) then
                            begin
                                 LogError ( FEmpleado, Format( 'No se encontr� una Incapacidad Inicial o Subsecuente de tipo %0:s, que ' +
                                 'termine el %1:s', [ cdsIncidencias.FieldByName('TB_ELEMENT').AsString, DateToStr ( dFecha - 1 ) ] ) );
                                 lValido := False;
                            end;
                       end;
                       //Valida Formato de Folio Incapacidad : AA999999
                       if not ( MatchesMask( cdsHisIncapaci.FieldByName('IN_NUMERO').AsString, sMask ) ) then
                       begin
                            LogError( FEmpleado, 'Folio de incapacidad incorrecto: ' + cdsHisIncapaci.FieldByName('IN_NUMERO').AsString );
                            lValido := False;
                       end
                       else if ( ( iPeriodoYear > 0 ) or ( iTipoNominaCodigo > 0 ) or ( iPeriodoNumero > 0 ) ) then
                       begin
                            if not ( ValidaPeriodo( iTipoNominaCodigo, iPeriodoYear, iPeriodoNumero ) ) then
                            begin
                                 LogError ( FEmpleado, K_MESS_ERR_NO_VALIDPERIODOS_INCA );
                                 lValido := False;
                            end;
                       end;
                  end;
             end;
        end;
        Result := lValido;
   end;

begin
     lOk := False;
     lOkAux := False;
     oConsultaPeriodo := TZetaClientDataSet.Create( Self );
     try
        try
           with FXMLTools do
           begin
                Registros := GetNode( 'REGISTROS' );
                sEmpresa := TagAsString( GetNode( 'EMPRESA' ), '' );
                if Assigned ( Registros ) then
                begin
                     LogError( 0, Format( '|Empresa %s|', [ sEmpresa ] ) );
                     sNombreCompany := cdsCompany.FieldByName('CM_NOMBRE').AsString;
                     if Assigned( Registros ) then
                     begin
                          Registro := GetNode( 'REGISTRO', Registros );
                          while Assigned( Registro ) do
                          begin
                               FIncapacidadAjustarFecha := 0;
                               CargaParametros( Registro, iEmpNumero, dIncapacidadInicial, iIncapacidadDias, sIncapacidadNumero, sIncapacidadTipo, sIncapacidadObserva, iIncapacidadClaseMotivo, iIncapacidadTipoFin, dIncapacidadSUAIni, dIncapacidadSUAFin, dIncapacidadFechaRH, dIncapaPermanente, iPeriodoYear, iTipoNominaCodigo, iPeriodoNumero, dIncapacidadDiasSubsidiar, iIncapacidadAjustarFecha );
                               FIncapacidadAjustarFecha := iIncapacidadAjustarFecha;
                               lRegistro := True;
                               if SetEmpleadoNumero( cdsEmpleado, iEmpNumero ) then
                               begin
                                    cdsHisIncapaci.Conectar;
                                    AsignarDataTemporal;
                                    AgregarDatosDatasetIncapacidad( iEmpNumero, dIncapacidadInicial, iIncapacidadDias, sIncapacidadNumero, sIncapacidadTipo, sIncapacidadObserva, iIncapacidadClaseMotivo, iIncapacidadTipoFin, dIncapacidadSUAIni, dIncapacidadSUAFin, dIncapacidadFechaRH, dIncapaPermanente, iPeriodoYear, iTipoNominaCodigo, iPeriodoNumero, dIncapacidadDiasSubsidiar );
                                    if( ValidaDatos ) then
                                    begin
                                        if ( ( not ValidaEnviar( cdsHisIncapaci ) ) or ( ErrorIncapacidad ) ) then
                                           lOk := True;
                                    end
                                    else
                                    begin
                                        cdsHisIncapaci.CancelUpdates;
                                        lOk := True;
                                    end;
                                    if ( ( lOk ) and ( lOkAux = False ) ) then lOkAux := True;
                               end
                               else
                               begin
                                    LogError( 0, Format( 'Empleado #%d no existe en la empresa %s:%s', [ iEmpNumero, sEmpresa, sNombreCompany ] ) );
                                    lOk := true;
                               end;
                               Registro := GetNextSibling( Registro );
                          end;
                          if not ( lRegistro ) then
                          begin
                               LogError( 0, 'Error al agregar incapacidad: Nodo <REGISTRO> no especificado' );
                               lOk := True;
                          end;
                     end;
                end
                else
                begin
                     lOk := true;
                     LogError( 0, 'Error al agregar incapacidad: Nodo <REGISTROS> no especificado' );
                end;
           end;
        except
              on Error: Exception do
              begin
                   lOk := true;
                   LogError( 0, 'Error al agregar incapacidad: ' + Error.Message );
              end;
        end;
        if ( lOkAux ) then lOk := True;
        if not ( lOk ) then FErrores.Clear;
        Result := lOk;
     finally
            FreeAndNil( oConsultaPeriodo );
     end;
end;


///////////////////////////////////////////////////////////////////////////////
/// Eventos de los procesos de bit�cora
///////////////////////////////////////////////////////////////////////////////

procedure TdmIncapacidad.EscribeBitacora( Resultado: OleVariant);
const
     K_DIA_HORA = 'hh:nn:ss AM/PM dd/mmm/yy';
begin
     with TProcessInfo.Create( nil ) do
     begin
          SetResultado( Resultado );
          Check( Resultado );
     end;
end;


function TdmIncapacidad.Check(const Resultado: OleVariant ): Boolean;
begin
     FProcessData := TProcessInfo.Create( nil );
     with ProcessData do
     begin
          SetResultado( Resultado );
          Result := ( Status in [ epsEjecutando, epsOK ] );
     end;
     GetProcessLog( ProcessData.Folio );
end;

procedure TdmIncapacidad.GetProcessLog( const iProceso: Integer );
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := oCursor;
     try
         with cdsLogDetail do
         begin
              Data := ServerConsultas.GetProcessLog( Empresa, iProceso );
         end;
     finally
            Screen.Cursor := oCursor;
    end;
end;


procedure TdmIncapacidad.LeerMensajeBitacora( var lOkProcess: Boolean; var Texto: TStringList );
const
     K_MENSAJE_BIT_TIPO_OPERACION = '%d|%s|%s';
var
   sMensaje, sTipoBitacora: String;
begin
      with cdsLogDetail do
      begin
           Refrescar;
           First;
           lOkProcess := True;
           while not Eof do
           begin
                case eTipoBitacora( FieldByName( 'BI_TIPO' ).AsInteger ) of
                     tbError:
                             begin
                                  sTipoBitacora := 'Error';
                                  sMensaje := Format( K_MENSAJE_BIT_TIPO_OPERACION, [ FieldByName( 'CB_CODIGO' ).AsInteger, sTipoBitacora, FieldByName( 'BI_DATA' ).AsString] );
                                  Texto.append( sMensaje );
                                  lOkProcess := False;
                             end;
                     tbErrorGrave:
                             begin
                                  sTipoBitacora := 'Error Grave';
                                  sMensaje := Format( K_MENSAJE_BIT_TIPO_OPERACION, [ FieldByName( 'CB_CODIGO' ).AsInteger, sTipoBitacora, FieldByName( 'BI_DATA' ).AsString] );
                                  Texto.append( sMensaje );
                                  lOkProcess := False;
                             end;
                end;
                Next;
           end;
      end;
end;

///////////////////////////////////////////////////////////////////////////////
/// FIN Eventos de los procesos de bit�cora
///////////////////////////////////////////////////////////////////////////////

function TdmIncapacidad.ValidaEnviar ( DataSet : TZetaClientDataSet ) : boolean;
begin
     Result := True;
     try
          DataSet.Enviar;
     except
          on Error : Exception do
          begin
               FHuboErrores := True;
               LogError( FEmpleado, Format( '%s', [ Error.Message ] ) );
               DataSet.CancelUpdates;
               Result := False;
          end;
     end;
end;

procedure TdmIncapacidad.cdsHisIncapaciAlAdquirirDatos(Sender: TObject);
begin
     cdsHisIncapaci.Data := ServerRecursos.GetHisIncapaci( Empresa, FEmpleado );
end;

procedure TdmIncapacidad.cdsHisIncapaciBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if eFinIncapacidad( FieldByName( 'IN_FIN').AsInteger ) <>  fiPermanente then
               FieldByName( 'IN_TASA_IP').AsFloat := 0;
          FieldByName( 'US_CODIGO').AsInteger   := Usuario;
          FieldByName( 'IN_CAPTURA').AsDateTime := FechaDefault;
     end;
end;

procedure TdmIncapacidad.AsignarDataTemporal;
begin
     if cdsIncapacidades = Nil then
     begin
          cdsIncapacidades := TZetaClientDataSet.Create(Self);
     end;
     cdsIncapacidades.Data := cdsHisIncapaci.Data;
end;


procedure TdmIncapacidad.cdsHisIncapaciAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
   dFechaInicioRegNuevo : TDateTime;

   procedure CrearDatosConflictos;
   begin
        with cdsListaConflicto do
        begin
             if Active then
                EmptyDataSet
             else
             begin
                  Init;
                  AddIntegerField( 'CB_CODIGO');
                  AddDateTimeField('IN_FEC_INI' );
                  AddIntegerField( 'IN_DIAS');
                  AddDateTimeField('IN_FEC_FIN');
                  AddDateTimeField('IN_SUA_INI' );
                  AddDateTimeField('IN_SUA_FIN' );
                  AddDateTimeField('IN_FEC_INI' + '_BUS' );
                  CreateTempDataset;
             end;
        end;
   end;

begin
     ErrorCount := 0;
     with Sender as TZetaClientDataSet do              // cdsHisIncapaci o cdsGridIncapaci
     begin
          if State in [ dsEdit, dsInsert ] then
               Post;
          if ChangeCount > 0 then
          begin
               FCancelaAjuste := FALSE;
               CrearDatosConflictos;
               Repeat
               Until ( FCancelaAjuste or Reconciliar( ServerRecursos.GrabaIncapacidad( Empresa, Delta, ErrorCount, cdsListaConflicto.Data ) ) );
               if ErrorCount = 0 then
               begin
                    FRefrescaHisKardex:= TRUE;
                    if not cdsListaConflicto.IsEmpty then
                    begin
                         dFechaInicioRegNuevo := FieldByName ('IN_FEC_INI').AsDateTime;
                         cdsHisIncapaci.Refrescar;
                         cdsHisIncapaci.Locate('IN_FEC_INI', FechaAsStr(dFechaInicioRegNuevo), []);
                    end;
               end;
          end;
     end;
end;

function TdmIncapacidad.BuscarIncapacidadAnterior( const FechaInicial:TDate; const sTipo :string ):Boolean;
begin
      Result := False;
      with cdsIncapacidades do
      begin
           if Not IsEmpty then
           begin
                First;
                While ( ( FieldByName('IN_FEC_INI').AsDateTime < FechaInicial ) and ( not Eof ) ) do
                begin
                     Result := (( FieldByName('IN_MOTIVO').AsInteger in [ Ord( miInicial ), Ord( miSubsecuente ) ] ) and
                                ( FieldByName('IN_TIPO').AsString = sTipo  ) and
                                ( FieldByName('IN_FEC_FIN').AsDateTime = FechaInicial )
                                );
                     if Result then
                        Break;
                     Next;
                end;
           end;
      end;
end;


function TdmIncapacidad.GetDatosEmpleadoActivo: Boolean;
begin
     with cdsEmpleado do
     begin
          Result := zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString );
     end;
end;

function TdmIncapacidad.GetFiltroIncapacidad: string;
begin
     Result := 'TB_INCIDEN =' + IntToStr( Ord( eiIncapacidad ) );
end;

procedure TdmIncapacidad.cdsHisIncapaciReconcileError(
  DataSet: TCustomClientDataSet; E: EReconcileError;
  UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sMensaje : string;
begin
     sMensaje := '';
     Action := ConflictoIncapacidadAccion( DataSet, FCancelaAjuste, K_SUB_INCAPACIDAD, 'Error al Registrar Incapacidad', E.Message,cdsListaConflicto {$ifdef WF_NUBE}, FIncapacidadAjustarFecha, sMensaje {$endif} );
     LogError( FEmpleado, 'Error al Registrar Incapacidad: ' + sMensaje);
     if not ( Action = raCorrect ) then  ErrorIncapacidad := True;
end;

procedure TdmIncapacidad.cdsIncidenciasAlAdquirirDatos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender ) do
     begin
          Data := ServerTablas.GetTabla( Empresa, Tag );
     end;
end;

end.
