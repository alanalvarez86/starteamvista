unit FProcesadorTress;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, ComObj, StdCtrls, Buttons, ExtCtrls, ComCtrls,
     DWorkFlow, DCliente,  ZetaAsciiFile, FAutoClasses, ActiveX;

type
  TProcesadorTress = class(TObject)
  private
    { Private declarations }
    FWorkFlow: TWorkFlowManager;
    FAscii: TAsciiLog;
    procedure SetControls(const lStarted: Boolean);
    procedure UpdateStatus(const Loops, Actions: Integer);
    procedure LogStart;
    procedure LogEnd;
    procedure LogMensaje(const sMensaje: string);
    procedure LogError(const sMensaje: string);
    procedure LogEvent( const sMensaje: String );
    procedure LogTimeStamp( const sMensaje: String );


  protected
    { Protected declarations }
    procedure SetStatus( const sTexto: String );
  public
    { Public declarations }
    constructor Create();
    destructor Destroy; override;
    procedure ProcesarPeticion(sXML : string; iUsuario : integer;  var sLog : string );
    procedure GetConfiguracion(sXML: string;  iUsuario: integer;  var sLog : string );

    { Public declarations }
  end;

var
  ProcesadorTress: TProcesadorTress;

implementation

uses ZetaCommonClasses, ZetaClientTools;

constructor TProcesadorTress.Create;
begin
     FAscii := TAsciiLog.Create;
     FWorkFlow := TWorkFlowManager.Create;
     with FWorkFlow do
     begin
          OnFeedBack := UpdateStatus;
          OnUpdateStatus := Self.SetStatus;
     end;
end;

destructor TProcesadorTress.Destroy;
begin
     FreeAndNil( FAscii );
     FreeAndNil( FWorkFlow );
end;

procedure TProcesadorTress.UpdateStatus( const Loops, Actions: Integer);
begin
end;

procedure TProcesadorTress.SetStatus(const sTexto: String);
begin
//     Log.Lines.Add( sTexto );
end;

procedure TProcesadorTress.LogStart;
begin
     FAscii.Init( 'ProcesadorWF.LOG');
end;

procedure TProcesadorTress.LogEnd;
begin
     FAscii.Close
end;

procedure TProcesadorTress.LogMensaje(const sMensaje: string);
var
   strAux : string;
begin
     strAux := DateTimeToStr( NOW ) + ' - ' + sMensaje;
     FAscii.WriteTexto( strAux );

end;

procedure TProcesadorTress.LogError(const sMensaje: string);
var
   strAux: string;
begin
     strAux := DateTimeToStr( NOW ) + ' - ' + sMensaje;
     FAscii.WriteTexto( strAux );

end;

procedure TProcesadorTress.LogEvent( const sMensaje: String );
var strAux : string;
begin
     strAux := DateTimeToStr( NOW ) + ' - ' + sMensaje;
     FAscii.WriteTexto( strAux );
        
end;

procedure TProcesadorTress.LogTimeStamp( const sMensaje: String );
begin
     FAscii.WriteTimeStamp( StringOfChar( '*', 15 ) +  sMensaje + StringOfChar( '*', 15 ) );
end;

procedure TProcesadorTress.SetControls(const lStarted: Boolean);
begin
//
end;

procedure TProcesadorTress.ProcesarPeticion(sXML: string;
  iUsuario: integer;  var sLog : string );
begin
     CoInitialize(nil);
     LogStart;
     LogTimeStamp( 'Inicia Log' );
     try
        SetControls( True );
        try
           dmCliente := TdmCliente.Create(nil);
           try
              dmCliente.TressGrabar(sXML,iUsuario, 9999, sLog) ;
              LogMensaje( sLog );
           except
                 on Error: Exception do
                 begin
                      sLog := 'ERROR' + Error.Message;
                 end;
           end;
        finally
               SetControls( False );
               FreeandNil( dmCliente );
        end;
     finally
            LogEnd;
            CoUninitialize;
     end;
end;


procedure TProcesadorTress.GetConfiguracion(sXML: string;
  iUsuario: integer;  var sLog : string );
var
   sRespuesta : TStringList;
begin
     LogStart;
     LogTimeStamp( 'Inicia Log' );
     try
        SetControls( True );
        try
           dmCliente := TdmCliente.Create(nil);
           try
              dmCliente.CargarAutorizacion;
              dmCliente.InitComparteCompany;

              sRespuesta := TStringList.Create();
              sRespuesta.Add( '<ConfiguracionTress>');
              sRespuesta.Add( '<Autorizacion>');
              sRespuesta.Add( Format( '<Sentinel>%d</Sentinel>' , [Autorizacion.NumeroSerie] ) );
              sRespuesta.Add( Format( '<Sistema>%d</Sistema>' , [Autorizacion.Empresa] ) );
              sRespuesta.Add( '</Autorizacion>');

              with  dmCliente.cdsComparte do
              begin
                 if ( not IsEmpty ) then
                 begin
                    First;
                    sRespuesta.Add( '<Comparte>');
                    sRespuesta.Add( Format( '<CM_DATOS>%s</CM_DATOS>' , [fieldByName('CM_DATOS' ).AsString ] ) );
                    sRespuesta.Add( Format( '<CM_USRNAME>%s</CM_USRNAME>' , [fieldByName('CM_USRNAME' ).AsString ] ) );
                    sRespuesta.Add( Format( '<CM_PASSWRD>%s</CM_PASSWRD>' , [fieldByName('CM_PASSWRD' ).AsString ] ) );
                    sRespuesta.Add( '</Comparte>');
                 end;
              end;
              sRespuesta.Add( '</ConfiguracionTress>');
              sLog := sRespuesta.Text;
              FreeAndNil( sRespuesta );
              LogMensaje( sLog );
           except
                 on Error: Exception do
                 begin
                      sLog := '<ConfiguracionTress><ERROR>' + Error.Message  + '</ERROR></ConfiguracionTress>' ;
                 end;
           end;
        finally
               SetControls( False );
               FreeandNil( dmCliente );
        end;
     finally
            LogEnd;
     end;
end;


end.
