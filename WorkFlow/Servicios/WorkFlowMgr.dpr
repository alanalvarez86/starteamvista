program WorkFlowMgr;

uses
  MidasLib,
  SvcMgr,
  ZetaClientTools,
  DWorkFlow in 'DWorkFlow.pas' {WorkFlowManager: TService},
  DBasicoCliente in '..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule};

{$R *.RES}

begin
  ZetaClientTools.InitDCom;
  Application.Initialize;
  Application.Title := 'WorkFlow Manager';
  Application.CreateForm(TWorkFlowManager, WorkFlowManager);
  Application.CreateForm(TBasicoCliente, BasicoCliente);
  Application.Run;
end.
