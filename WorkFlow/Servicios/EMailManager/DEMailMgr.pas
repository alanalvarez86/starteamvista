unit DEMailMgr;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Dialogs, ComObj, DB,
     {$ifndef EMAILTEST}
     SvcMgr,
     {$endif}
     ZetaCommonLists,
     {$IFDEF TRESS_DELPHIXE5_UP}
     DEmailService,
     {$ELSE}
     DEmailServiceD7,
     {$ENDIF}
     DZetaServerProvider,
     DWorkFlowTypes;

type
  {$ifdef WORKFLOWTEST}
  TService = class( TObject )
  end;
  {$endif}
  TWorkFlowEMailMgr = class(TService)
    procedure ServiceCreate(Sender: TObject);
    procedure ServiceDestroy(Sender: TObject);
    procedure ServiceExecute(Sender: TService);
  private
    { Private declarations }
    FZetaProvider: TdmZetaWorkFlowProvider;
    FRegistry: TWFRegistry;
    FPendientes: TZetaCursor;
    FEnviado: TZetaCursor;
    FProblema: TZetaCursor;
    FTarea: TZetaCursor;
    FPendienteUno: TZetaCursor;
    FEmailService: TdmEmailService;
    {$ifdef WORKFLOWTEST}
    FLog: TStrings;
    FOnFeedBack: TFeedBack;
    FLoops: Integer;
    FEMailCtr: Integer;
    FTerminated: Boolean;
    {$endif}
    function Start: Boolean;
    function Stop: Boolean;
    function Validar(var Mensaje: String): Boolean;
    procedure AddTo(Lista: TStrings; const sValue: String);
    {$ifdef WORKFLOWTEST}
    procedure DoFeedBack;
    {$endif}
    procedure MarcarEnviado(const idMsg: TWFIdentifier);
    procedure MarcarProblema(const idMsg: TWFIdentifier; const sLogMsg: String);
    procedure TareaActualiza(const idTarea: TWFIdentifier; const eStatus: eWFTareaStatus; const iAvance: Integer; const idMsg: TWFIdentifier);
    procedure LogBitacora( const sMensaje: string; const eTipo: eTipoBitacora = tbNormal);overload;
    procedure LogException(const sMensaje: string);
  protected
    { Protected declarations }
    procedure EscribeBitacora(const sMensaje: String);
    procedure EscribeExcepcion(const sMensaje: String; Error: Exception );
  public
    { Public declarations }
    property EmailService: TdmEmailService read FEmailService;
    property oZetaProvider: TdmZetaWorkFlowProvider read FZetaProvider;
    {$ifdef WORKFLOWTEST}
    property Log: TStrings read FLog write FLog;
    property OnFeedBack: TFeedBack read FOnFeedBack write FOnFeedBack;
    property Terminated: Boolean read FTerminated write FTerminated;
    procedure LogMessage( const sTexto: String; const iValue1, iValue2, iValue3: Integer );
    {$else}
    function GetServiceController: TServiceController; override;
    {$endif}
  end;

var
  WorkFlowEMailMgr: TWorkFlowEMailMgr;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaWinAPITools;{OP: 8.Diciembre.2008}

{$ifndef WORKFLOWTEST}
{$R *.DFM}
{$endif}

const
    Q_PENDIENTES_LEE = 'select TOP 100 WO_GUID, WO_FEC_IN '+
                        'from WOUTBOX where ( WO_STATUS = %d ) order by WO_FEC_IN';
     Q_MARCA_ENVIADO = 'update WOUTBOX set WO_ENVIADO = ''%s'', WO_STATUS = %d, WO_FEC_OUT = GetDate() where ( WO_GUID = :WO_GUID )';
     Q_MARCA_PROBLEMA = 'update WOUTBOX set WO_STATUS = %d, WO_LOG = :WO_LOG where ( WO_GUID = :WO_GUID )';
     Q_TAREA_ACTUALIZA = 'execute procedure DBO.SP_AVANZA_TAREA( :GUID, :STATUS, :AVANCE, :FECHA )';
     Q_PENDIENTE_LEE_UNO = 'select WO_GUID, WT_GUID, WO_TO, WO_CC, WO_SUBJECT, WO_BODY, '+
                        'WO_FROM_NA, WO_FROM_AD, WO_SUBTYPE, WO_ENVIADO, WO_FEC_IN, '+
                        'WO_FEC_OUT, WO_TSTATUS, WO_TAVANCE '+
                        'from WOUTBOX where ( WO_GUID = :WO_GUID )';


{$ifndef WORKFLOWTEST}
procedure ServiceController(CtrlCode: DWord); stdcall;
begin
     WorkFlowEMailMgr.Controller(CtrlCode);
end;
{$endif}

{ ******* TWorkFlowManager ******** }

{$ifndef WORKFLOWTEST}
function TWorkFlowEMailMgr.GetServiceController: TServiceController;
begin
     Result := ServiceController;
end;
{$endif}

procedure TWorkFlowEmailMgr.LogBitacora( const sMensaje : string; const eTipo: eTipoBitacora = tbNormal );
 var
    sEmailMSG: string;
begin
     try
        oZetaProvider.EscribeBitacora( eTipo,clbEmailService,0,Date,sMensaje, sMensaje );
        sEmailMSG := VACIO;
        if Validar( sEmailMSG ) then
        begin
             try

               with FEmailService do
               begin
                    NewEMail;
                    with FRegistry do
                    begin
                         MailServer := EMailServer;
                         Port := EMailPort;
                         User := EMailUser;
                         PassWord := EMailPassword;
                         FromName := WorkFlowEMailName;
                         FromAddress := WorkFlowEMailAddress;
                         {$ifdef TRESS_DELPHIXE5_UP}
                         ReplyToName := EMailReplyToName;
                         ReplyToAddress := EMailReplyToAddress;
                         ReceiptRecipientName := EMailReceiptRecipientName;
                         ReceiptRecipientAddress := EMailReceiptRecipientAddress;
                         {$endif}
                         TimeOut := EMailTimeOut;
                    end;
                    SubType := emtTexto;
                    SendEmail( sEmailMSG );
               end;
             except
                   Abort;
             end;
        end;
     except
     end;
end;

procedure TWorkFlowEmailMgr.LogException( const sMensaje : string );
begin
     LogBitacora( sMensaje, tbErrorGrave );
end;


procedure TWorkFlowEMailMgr.EscribeBitacora(const sMensaje: String);
begin
     try
        LogMessage( sMensaje, EVENTLOG_INFORMATION_TYPE, 0, 0 );
        LogBitacora( sMensaje );
     except
           LogMessage( 'Error al Escribir a Bit�cora', EVENTLOG_ERROR_TYPE, 0, 0 );
     end;
end;

procedure TWorkFlowEMailMgr.EscribeExcepcion(const sMensaje: String; Error: Exception );
begin
     try
        LogMessage( Format( '%s: %s', [ sMensaje, Error.Message ] ), EVENTLOG_ERROR_TYPE, 0, 0 );
        LogException( Format( '%s: %s', [ sMensaje, Error.Message ] ) );
     except
           LogMessage( 'Error al Escribir a Bit�cora', EVENTLOG_ERROR_TYPE, 0, 0 );
     end;
end;

{$ifdef WORKFLOWTEST}
procedure TWorkFlowEMailMgr.LogMessage( const sTexto: String; const iValue1, iValue2, iValue3: Integer );
const
     K_LOG_MAX = 100;
var
   i: Integer;
begin
     {
     ShowMessage( sTexto );
     Log.Add( sTexto );
     }
     with Log do
     begin
          BeginUpdate;
          try
             if ( Count > K_LOG_MAX ) then
             begin
                  for i := ( Count - 1 ) downto ( K_LOG_MAX - 1 ) do
                  begin
                       Delete( i );
                  end;
             end;
             Insert( 0, Format( '[ %s ] %s', [ FormatDateTime( 'hh:nn:ss', Now ), sTexto ] ) );
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TWorkFlowEMailMgr.DoFeedBack;
begin
     if Assigned( FOnFeedBack ) then
        FOnFeedBack( FLoops, FEMailCtr );
end;
{$endif}

procedure TWorkFlowEMailMgr.ServiceCreate(Sender: TObject);
{$ifndef WORKFLOWTEST}
 var
    FRegistryServer : TWorkFlowRegistryServer;
    sDependency: string;
{$endif}
begin
     FZetaProvider := TdmZetaWorkFlowProvider.Create( nil );
     FEmailService := TdmEmailService.Create( nil );
     FRegistry := TWFRegistry.Create( False );
     FRegistry.oZetaProvider := Self.oZetaProvider;

{$ifndef WORKFLOWTEST}
     {$ifdef MSSQL}
     FRegistryServer := TWorkFlowRegistryServer.Create( False );
     try
        sDependency := FRegistryServer.WorkFlowServerDependency;
     finally
            FreeAndNil( FRegistryServer );
     end;

     ZetaWinAPITools.OpenServiceManager;{OP: 8.Diciembre.2008}

     if ZetaCommonTools.StrLleno( sDependency ) and ZetaWinAPITools.FindService( sDependency ) then {OP: 8.Diciembre.2008}
     begin
          with TDependency.Create( Dependencies ) do
          begin
               IsGroup := False;
               Name := sDependency;
          end;
     end;

     ZetaWinAPITools.CloseServiceManager;{OP: 8.Diciembre.2008}

     {$endif}

     {$ifdef INTERBASE}
     if ZetaWinAPITools.GetInterbaseIsInstalled then
     begin
          with TDependency.Create( Dependencies ) do
          begin
               IsGroup := False;
               Name := INTERBASE_GUARDIAN_DEFAULT_DEPENDENCY;
          end;
          with TDependency.Create( Dependencies ) do
          begin
               IsGroup := False;
               Name := INTERBASE_DEFAULT_DEPENDENCY;
          end;
     end
     else
     begin
          if ZetaWinAPITools.GetFirebirdIsInstalled then
          begin
               with TDependency.Create( Dependencies ) do
               begin
                    IsGroup := False;
                    Name := FIREBIRD_DEFAULT_DEPENDENCY;
               end;
          end;
     end;
     {$endif}
{$endif}
end;

procedure TWorkFlowEMailMgr.ServiceDestroy(Sender: TObject);
begin
     FreeAndNil( FRegistry );
     FreeAndNil( FEmailService );
     FreeAndNil( FZetaProvider );
end;

function TWorkFlowEMailMgr.Start: Boolean;
const
     K_REGISTRY_KEY = 'EMailManager';
begin
     Result := False;
     try
        with oZetaProvider do
        begin
             Start;
             FPendientes := CreateQuery( Format( Q_PENDIENTES_LEE, [ Ord( ocsSinEnviar ) ] ) );
             FEnviado := CreateQuery( Format( Q_MARCA_ENVIADO, [ K_GLOBAL_SI, Ord( ocsEnviadoOK ) ] ) );
             FProblema := CreateQuery( Format( Q_MARCA_PROBLEMA, [ Ord( ocsEnviadoOK ) ] ) );
             FTarea := CreateQuery( Q_TAREA_ACTUALIZA );
             FPendienteUno := CreateQuery( Q_PENDIENTE_LEE_UNO );
        end;
        with FEmailService do
        begin
             NewEMail;
             with FRegistry do
             begin
                  MailServer := EMailServer;
                  Port := EMailPort;
                  User := EMailUser;
                  PassWord := EMailPassword;
                  FromName := WorkFlowEMailName;
                  FromAddress := WorkFlowEMailAddress;
                  {$ifdef TRESS_DELPHIXE5_UP}
                  ReplyToName := EMailReplyToName;
                  ReplyToAddress := EMailReplyToAddress;
                  ReceiptRecipientName := EMailReceiptRecipientName;
                  ReceiptRecipientAddress := EMailReceiptRecipientAddress;
                  {$endif}
                  TimeOut := EMailTimeOut;
             end;
             SubType := emtTexto;
        end;
        {$ifdef WORKFLOWTEST}
        Log.Clear;
        FLoops := 0;
        FEMailCtr := 0;
        Terminated := False;
        {$endif}
        Result := True;
     except
           on Error: Exception do
           begin
                EscribeExcepcion( 'Error Al Iniciar Servicio', Error );
                {$ifdef WORKFLOWTEST}
                Terminated := True;
                {$endif}
           end;
     end;
end;

function TWorkFlowEMailMgr.Validar( var Mensaje: String ): Boolean;
begin
     Result := False;
     Mensaje := '';
     with FPendienteUno do
     begin
          if ZetaCommonTools.StrVacio( FieldByName( 'WO_FROM_NA' ).AsString ) then
             Mensaje := 'Nombre Del Remitente Vac�o'
          else
              if ZetaCommonTools.StrVacio( FieldByName( 'WO_FROM_AD' ).AsString ) then
                 Mensaje := 'Direcci�n De Correo Electr�nico Del Remitente Vac�a'
              else
                  if ZetaCommonTools.StrVacio( FieldByName( 'WO_TO' ).AsString ) then
                     Mensaje := 'Direcci�n De Correo Electr�nico Del Destinatario Vac�a'
                  else
                      if ZetaCommonTools.StrVacio( FieldByName( 'WO_SUBJECT' ).AsString ) then
                         Mensaje := 'Tema Del Correo No Fu� Especificado'
                      else
                          if ZetaCommonTools.StrVacio( FieldByName( 'WO_BODY' ).AsString ) then
                             Mensaje := 'Texto Del Correo No Fu� Especificado'
                          else
                              Result := True;
     end;
end;

procedure TWorkFlowEMailMgr.ServiceExecute(Sender: TService);
const
     K_PAUSA = 15000;
var
   idMsg: TWFIdentifier;
   Tarea: TField;
   lEnviado: Boolean;
   sLogMsg: String;
begin
     if Start then
     begin
          while not Terminated do
          begin
               {$ifdef WORKFLOWTEST}
               Inc( FLoops );
               {$endif}
               try
                  with FPendientes do
                  begin
                       Active := True;
                       try
                          try
                             First;
                             while not Eof do
                             begin
                                  idMsg := oZetaProvider.CampoAsGUID( FieldByName( 'WO_GUID' ) );

                                  with oZetaProvider do
                                  begin
                                       ParamAsGUID( FPendienteUno, 'WO_GUID', idMsg );
                                       EmpiezaTransaccion;
                                       try
                                          Ejecuta( FPendienteUno );

                                          if( not FPendienteUno.Eof )then
                                          begin

                                               lEnviado := False;
                                               sLogMsg := VACIO;
                                               if Validar( sLogMsg ) then
                                               begin
                                                    try
                                                       with FEMailService do
                                                       begin
                                                            NewBodyMessage;
                                                            FromName := FPendienteUno.FieldByName( 'WO_FROM_NA' ).AsString;
                                                            FromAddress := FPendienteUno.FieldByName( 'WO_FROM_AD' ).AsString;
                                                            AddTo( ToAddress, FPendienteUno.FieldByName( 'WO_TO' ).AsString );
                                                            AddTo( ToCarbonCopy, FPendienteUno.FieldByName( 'WO_CC' ).AsString );
                                                            Subject := FPendienteUno.FieldByName( 'WO_SUBJECT' ).AsString;
                                                            AddTo( MessageText, FPendienteUno.FieldByName( 'WO_BODY' ).AsString );
                                                            SubType := eEMailType( FPendienteUno.FieldByName( 'WO_SUBTYPE' ).AsInteger );

                                                            if SendEMail( sLogMsg ) then
                                                               lEnviado := True
                                                            else
                                                            begin
                                                                 Self.EscribeBitacora( Format( 'Problema Al Enviar Correo {%s}: %s', [ GUIDToString( idMsg ), sLogMsg ] ) );
                                                            end;

                                                       end;
                                                    except
                                                          on Error: Exception do
                                                          begin
                                                               sLogMsg := Error.Message;
                                                               Self.EscribeExcepcion( Format( 'Error Al Enviar Correo {%s}', [ GUIDToString( idMsg ) ] ), Error );
                                                          end;
                                                    end;
                                               end
                                               else
                                                   Self.EscribeBitacora( Format( 'Correo Incompleto {%s}: %s', [ GUIDToString( idMsg ), sLogMsg ] ) );

                                               if lEnviado then
                                               begin
                                                    {$ifdef WORKFLOWTEST}
                                                    Inc( FEMailCtr );
                                                    {$endif}
                                                       MarcarEnviado( idMsg );
                                                       Tarea := FPendienteUno.FieldByName( 'WT_GUID' );
                                                       if not Tarea.IsNull then
                                                       begin
                                                            TareaActualiza( oZetaProvider.CampoAsGUID( Tarea ),
                                                                            eWFTareaStatus( FPendienteUno.FieldByName( 'WO_TSTATUS' ).AsInteger ),
                                                                            FPendienteUno.FieldByName( 'WO_TAVANCE' ).AsInteger,
                                                                            idMsg );
                                                       end;
                                               end
                                               else
                                               begin
                                                       MarcarProblema( idMsg, sLogMsg );
                                               end;
                                          end;
                                          TerminaTransaccion( True );
                                       except
                                             on Error: Exception do
                                             begin
                                                  RollBackTransaccion;
                                                  EscribeExcepcion( Format( 'Error consultar registro de correo {%s}', [ GUIDToString( idMsg ) ] ), Error );
                                             end;
                                       end;
                                  end;
                                  Next;
                             end;
                          except
                                on Error: Exception do
                                begin
                                     EscribeExcepcion( 'Error Al Procesar Mensajes', Error );
                                end;
                          end;
                       finally
                              Active := False;
                       end;
                  end;
               except
                     on Error: Exception do
                     begin
                          EscribeExcepcion( 'Error Al Leer Buz�n', Error );
                     end;
               end;
               { Esperar 1 Segundo Antes De Empezar El Nuevo Query }
               {$ifdef WORKFLOWTEST}
               Sleep( K_PAUSA );
               DoFeedBack;
               {$else}
               ServiceThread.ProcessRequests( False );
               Sleep( K_PAUSA );
               {$endif}
          end;
     end;
     Stop;
end;

function TWorkFlowEMailMgr.Stop: Boolean;
begin
     Result := False;
     try
        with oZetaProvider do
        begin
             FreeAndNil( FPendienteUno );
             FreeAndNil( FTarea );
             FreeAndNil( FEnviado );
             FreeAndNil( FProblema );
             FreeAndNil( FPendientes );
             Stop;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                EscribeExcepcion( 'Error Al Detener Servicio', Error );
           end;
     end;
end;

procedure TWorkFlowEMailMgr.AddTo( Lista: TStrings; const sValue: String );
begin
     if ZetaCommonTools.StrLleno( sValue ) then
     begin
          Lista.Add( sValue );
     end;
end;

procedure TWorkFlowEMailMgr.MarcarEnviado( const idMsg: TWFIdentifier );
begin
     with oZetaProvider do
     begin
          ParamAsGUID( FEnviado, 'WO_GUID', idMsg );
          EmpiezaTransaccion;
          try
             Ejecuta( FEnviado );
             TerminaTransaccion( True );
          except
                on Error: Exception do
                begin
                     RollBackTransaccion;
                     EscribeExcepcion( Format( 'Error Al Marcar Env�o De Correo {%s}', [ GUIDToString( idMsg ) ] ), Error );
                end;
          end;
     end;
end;

procedure TWorkFlowEMailMgr.MarcarProblema( const idMsg: TWFIdentifier; const sLogMsg: String );
begin
     with oZetaProvider do
     begin
          ParamAsGUID( FProblema, 'WO_GUID', idMsg );
          ParamAsString( FProblema, 'WO_LOG', sLogMsg );
          EmpiezaTransaccion;
          try
             Ejecuta( FProblema );
             TerminaTransaccion( True );
          except
                on Error: Exception do
                begin
                     RollBackTransaccion;
                     EscribeExcepcion( Format( 'Error Al Marcar Problema En Env�o De Correo {%s}', [ GUIDToString( idMsg ) ] ), Error );
                end;
          end;
     end;
end;

procedure TWorkFlowEMailMgr.TareaActualiza( const idTarea: TWFIdentifier; const eStatus: eWFTareaStatus; const iAvance: Integer; const idMsg: TWFIdentifier );
begin
     with oZetaProvider do
     begin
          ParamAsGUID( FTarea, 'GUID', idTarea );
          ParamAsInteger( FTarea, 'STATUS', Ord( eStatus ) );
          ParamAsInteger( FTarea, 'AVANCE', iAvance );
          ParamAsDateTime( FTarea, 'FECHA', Now );
          EmpiezaTransaccion;
          try
             Ejecuta( FTarea );
             TerminaTransaccion( True );
          except
                on Error: Exception do
                begin
                     RollBackTransaccion;
                     EscribeExcepcion( Format( 'Error Al Actualizar Tarea de Envio de Correo {%s}', [ GUIDToString( idMsg ) ] ), Error );
                end;
          end;
     end;
end;

end.
