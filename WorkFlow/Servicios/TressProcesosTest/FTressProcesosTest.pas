unit FTressProcesosTest;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, StdCtrls, Buttons, Mask, ZetaNumero;

type
  TTestProcesos = class(TForm)
    Paso2GB: TGroupBox;
    EmpresaLBL: TLabel;
    Archivo: TEdit;
    BtnProcesar: TButton;
    ArchivoSeek: TSpeedButton;
    OpenDialog: TOpenDialog;
    MemoResult: TMemo;
    edUsuario: TZetaNumero;
    Label1: TLabel;
    Label2: TLabel;
    cbTipo: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BtnProcesarClick(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
  private
    { Private declarations }
    function GetXML: String;
  public
    { Public declarations }
  end;

var
  TestProcesos: TTestProcesos;

implementation

uses DCliente,
     ZetaEscogeCia,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaRegistryCliente,
     ZetaDialogo;

{$R *.DFM}

procedure TTestProcesos.FormCreate(Sender: TObject);
begin
     ZetaCommonTools.Environment;
     ZetaRegistryCliente.InitClientRegistry;
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     {$endif}
     dmCliente := TdmCliente.Create( Self );
     with cbTipo do
     begin
          with Items do
          begin
               Clear;
               BeginUpdate;
               try
                  Add( K_T_CAMBIO );
                  Add( K_TW_MULTIP );
                  Add( K_T_PERM );
                  Add( K_TW_AUTO3 );
                  Add( K_T_VACA );
               finally
                      EndUpdate;
               end;
          end;
          ItemIndex := 0;
     end;
end;

procedure TTestProcesos.FormDestroy(Sender: TObject);
begin
     FreeAndNil( dmCliente );
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.FreeAll;
     {$endif}
     ZetaRegistryCliente.ClearClientRegistry;
end;

function TTestProcesos.GetXML: String;
const
     K_EMPRESA = 'EVALUAGA';
     K_XML_CAMBIO = '<DATOS>'+
                    '<MOV3>%0:s</MOV3>'+
                    '<EMPRESA Titulo="Empresa:" Tipo="T" Texto="DATOS2">%1:s</EMPRESA>'+
                    '<EMPLEADO Titulo="Empleado #:" Tipo="I" Texto="23">123456803</EMPLEADO>'+
                    '<EMPNOMBRE Titulo="Nombre:" Tipo="T" Texto="G�mez Llanos Ju�rez, Teodoro">G�mez Llanos Ju�rez, Teodoro</EMPNOMBRE>'+
                    '<FECHAREG Titulo="Fecha:" Tipo="F" Texto="01/03/2004">20041201</FECHAREG>'+
                    '<CB_SALARIO Titulo="Salario diario anterior:" Tipo="R" Texto="0.00">297.00</CB_SALARIO>'+
                    '<SALARIO_NEW Titulo="Salario diario nuevo:" Tipo="R" Texto="400.00">315.25</SALARIO_NEW>'+
                    '<DESCRIPCION Titulo="Descripci�n:" Tipo="T" Texto=""/>'+
                    '<OBSERVACIONES Titulo="Observaciones:" Tipo="T" Texto="No tenia salario diario anteriormente">No tenia salario diario anteriormente</OBSERVACIONES>'+
                    '</DATOS>';
     K_XML_CAMBIO2 = '<DATOS>'+
                     '<MOV3>%0:s</MOV3>'+
                     '<EMPRESA Titulo="Empresa:" Tipo="T" Texto="NUEVA">%1:s</EMPRESA>'+
                     '<EMPLEADOS>'+
                     '<INFO>'+
                     '<EMPLEADO Titulo="Empleado #:" Tipo="I" Texto="5610">164</EMPLEADO>'+
                     '<EMPNOMBRE Titulo="Nombre:" Tipo="T" Texto="FELIX LOPEZ, CAROLINA">FELIX LOPEZ, CAROLINA</EMPNOMBRE>'+
                     '<EMPBAJA Titulo="" Tipo="T" Texto="" />'+
                     '</INFO>'+
                     '<INFO>'+
                     '<EMPLEADO Titulo="Empleado #:" Tipo="I" Texto="5610">165</EMPLEADO>'+
                     '<EMPNOMBRE Titulo="Nombre:" Tipo="T" Texto="FELIX LOPEZ, CAROLINA">FELIX LOPEZ, CAROLINA</EMPNOMBRE>'+
                     '<EMPBAJA Titulo="" Tipo="T" Texto="" />'+
                     '</INFO>'+
                     '</EMPLEADOS>'+
                     '<EMPAUTOSAL Titulo="" Tipo="T" Texto="" />'+
                     '<FECHAREG Titulo="Fecha:" Tipo="F" Texto="07/Dec/2004">20041207</FECHAREG>'+
                     '<CB_SALARIO Titulo="Salario diario anterior:" Tipo="R" Texto="150.00">150</CB_SALARIO>'+
                     '<SALARIO_NEW Titulo="Salario diario nuevo:" Tipo="R" Texto="160.00">160</SALARIO_NEW>'+
                     '<SALARIO_MENOR Titulo="" Tipo="T" Texto="" />'+
                     '<DESCRIPCION Titulo="Descripci�n:" Tipo="T" Texto="Test">Test</DESCRIPCION>'+
                     '<OBSERVACIONES Titulo="Observaciones:" Tipo="T" Texto="Test">Test</OBSERVACIONES>'+
                     '</DATOS>';
     K_XML_PERM =   '<DATOS>'+
                    '<MOV3>%0:s</MOV3>'+
                    '<TIPO>PERMISO</TIPO>'+
                    '<EMPRESA>%1:s</EMPRESA>'+
                    '<EMPLEADO>21</EMPLEADO>'+
                    '<INICIO>20040916</INICIO>'+
                    '<DIAS>10</DIAS>'+
                    '<CLASE>2</CLASE>'+
                    '<TIPO>FJ</TIPO>'+
                    '<OBSERVACIONES>Se Rompio Una Pierna</OBSERVACIONES>'+
                    '<REFERENCIA>1005</REFERENCIA>'+
                    '<USUARIO>1</USUARIO>'+
                    '</DATOS>';
     K_XML_MULTIP = '<DATOS>'+
                    '<MOV3>%0:s</MOV3>'+
                    '<EMPRESA Titulo="Empresa:" Tipo="T" Texto="DATOS2">%1:s</EMPRESA>'+
                    '<EMPLEADO Titulo="Empleado #:" Tipo="I" Texto="165">23</EMPLEADO>'+
                    '<EMPNOMBRE Titulo="Nombre:" Tipo="T" Texto="G�mez Llanos Ju�rez, Teodoro">G�mez Llanos Ju�rez, Teodoro</EMPNOMBRE>'+
                    '<FECHAREG Titulo="Fecha:" Tipo="F" Texto="01/Dec/2004">20041201</FECHAREG>'+
                    '<CB_PUESTO Titulo="Puesto:" Tipo="T" Texto="Consultor T�cnico">83</CB_PUESTO>'+
                    '<CB_NIVEL2 Titulo="Certificado:" Tipo="T" Texto="NIVEL 3">3</CB_NIVEL2>'+
                    '<CB_NIVEL5 Titulo="Supervisor:" Tipo="T" Texto="Ninguno">B</CB_NIVEL5>'+
                    '<DESCRIPCION Titulo="Descripci�n:" Tipo="T" Texto=""/>'+
                    '<OBSERVACIONES Titulo="Observaciones:" Tipo="T" Texto="Prueba 2 Kardex workflow">Prueba 2 Kardex workflow</OBSERVACIONES>'+
                    '</DATOS>';
     K_XML_MULTIP2 = '<DATOS>'+
                     '<MOV3>%0:s</MOV3>'+
                     '<EMPRESA Titulo="Empresa:" Tipo="T" Texto="DATOS2">%1:s</EMPRESA>'+
                     '<EMPLEADOS>'+
                     '<INFO>'+
                     '<EMPLEADO Titulo="Empleado #:" Tipo="I" Texto="43">43</EMPLEADO>'+
                     '<EMPNOMBRE Titulo="Nombre:" Tipo="T" Texto="G�mez Llanos Ju�rez, Teodoro">G�mez Llanos Ju�rez, Teodoro</EMPNOMBRE>'+
                     '</INFO>'+
                     '<INFO>'+
                     '<EMPLEADO Titulo="Empleado #:" Tipo="I" Texto="43">44</EMPLEADO>'+
                     '<EMPNOMBRE Titulo="Nombre:" Tipo="T" Texto="G�mez Llanos Ju�rez, Teodoro">G�mez Llanos Ju�rez, Teodoro</EMPNOMBRE>'+
                     '</INFO>'+
                     '</EMPLEADOS>'+
                     '<FECHAREG Titulo="Fecha:" Tipo="F" Texto="01/Dec/2004">20041201</FECHAREG>'+
                     '<CB_CLASIFI Titulo="Clasificaci�n:" Tipo="T" Texto="Consultor T�cnico">D</CB_CLASIFI>'+
                     '<CB_CONTRAT Titulo="Tipo de Contrato:" Tipo="T" Texto="NIVEL 3">9</CB_CONTRAT>'+
                     '<CB_TURNO Titulo="Supervisor:" Tipo="T" Texto="Ninguno">6</CB_TURNO>'+
                     '<DESCRIPCION Titulo="Descripci�n:" Tipo="T" Texto=""/>'+
                     '<OBSERVACIONES Titulo="Observaciones:" Tipo="T" Texto="Prueba Kardex Multip2">Prueba Kardex Multip2 workflow</OBSERVACIONES>'+
                     '</DATOS>';
     K_XML_AUTO3 =  '<DATOS>'+
                    '<MOV3>%0:s</MOV3>'+
                    '<EMPRESA Titulo="Empresa:" Tipo="T" Texto="DATOS2">%1:s</EMPRESA>'+
                    '<EMPLEADO Titulo="Empleado #:" Tipo="I" Texto="399">399</EMPLEADO>'+
                    '<EMPNOMBRE Titulo="Nombre:" Tipo="T" Texto="GALLEGOS BA?AGA, ANA LUISA">GALLEGOS BA?AGA, ANA LUISA</EMPNOMBRE>'+
                    '<EMPBAJA Titulo="" Tipo="T" Texto=""/>'+
                    '<EMPAUTOSAL Titulo="" Tipo="T" Texto=""/>'+
                    '<FECHAAUTO Titulo="Fecha:" Tipo="F" Texto="15/Dec/2004">20041215</FECHAAUTO>'+
                    '<TIPOAUTO Titulo="Autorizacion:" Tipo="T" Texto="Horas Extras">3</TIPOAUTO>'+
                    '<HORAS Titulo="Horas" Tipo="R" Texto="3.00">1.5</HORAS>'+
                    '<MOTIVO Titulo="Motivo:" Tipo="T" Texto="Retrabajo"></MOTIVO>'+
                    '</DATOS>';
      K_XML_VACA =  '<DATOS>'+
                    '<MOV3>%0:s</MOV3>'+
                    '<EMPRESA>%1:s</EMPRESA>'+
                    '<EMPLEADO>21</EMPLEADO>'+
                    '<VA_FEC_INI>20041212</VA_FEC_INI>'+
                    '<VA_FEC_FIN>20041219</VA_FEC_FIN>'+
                    '<VA_GOZO>12.00</VA_GOZO>'+
                    '<VA_PAGO>12.00</VA_PAGO>'+
                    '<VA_PERIODO>Break de Diciembre</VA_PERIODO>'+
                    '<VA_COMENTA>Solicitado por WorkFlow</VA_COMENTA>'+
                    '<VA_NOMYEAR>2004</VA_NOMYEAR>'+
                    '<VA_NOMTIPO>1</VA_NOMTIPO>'+
                    '<VA_NOMNUME>50</VA_NOMNUME>'+
                    '</DATOS>';
      K_XML_VACA2 = '<DATOS>'+
                    '<MOV3>%0:s</MOV3>'+
                    '<EMPRESA Titulo="Empresa:" Tipo="T" Texto="DATOS2">%1:s</EMPRESA>'+
                    '<EMPLEADOS>'+
                    '<INFO>'+
                    '<EMPLEADO Titulo="Empleado #:" Tipo="I" Texto="165">1287</EMPLEADO>'+
                    '<EMPNOMBRE Titulo="Nombre:" Tipo="T" Texto="G?mez Llanos Ju?rez, Teodoro">G?mez Llanos Ju?rez, Teodoro</EMPNOMBRE>'+
                    '<EMPBAJA Titulo="" Tipo="T" Texto=""/>'+
                    '</INFO>'+
                    '<INFO>'+
                    '<EMPLEADO Titulo="Empleado #:" Tipo="I" Texto="165">1288</EMPLEADO>'+
                    '<EMPNOMBRE Titulo="Nombre:" Tipo="T" Texto="G?mez Llanos Ju?rez, Teodoro">G?mez Llanos Ju?rez, Teodoro</EMPNOMBRE>'+
                    '<EMPBAJA Titulo="" Tipo="T" Texto=""/>'+
                    '</INFO>'+
                    '</EMPLEADOS>'+
                    '<EMPAUTOSAL Titulo="" Tipo="T" Texto=""/>'+
                    '<VA_FEC_INI Titulo="Fecha salida:" Tipo="F" Texto="14/Dec/2004">20041212</VA_FEC_INI>'+
                    '<VA_FEC_FIN Titulo="Fecha regreso:" Tipo="F" Texto="31/Dec/2004">20041219</VA_FEC_FIN>'+
                    '<VA_GOZO Titulo="D?as a gozar:" Tipo="R" Texto="15.00">10</VA_GOZO>'+
                    '<VA_PAGO Titulo="D?as a pagar:" Tipo="R" Texto="15.00">10</VA_PAGO>'+
                    '<VA_PERIODO Titulo="Per?odo trabajado:" Tipo="T" Texto=""/>'+
                    '<VA_COMENTA Titulo="Observaciones:" Tipo="T" Texto=""/>'+
                    '<VA_NOMYEAR Titulo="A?o:" Tipo="I" Texto="2004">2004</VA_NOMYEAR>'+
                    '<VA_NOMTIPO Titulo="Tipo:" Tipo="T" Texto="Semana">1</VA_NOMTIPO>'+
                    '<VA_NOMNUME Titulo="N?mero:" Tipo="I" Texto="50">50</VA_NOMNUME> '+
                    '</DATOS>';
begin
     if ( cbTipo.Text = K_T_CAMBIO ) then
        Result := Format( K_XML_CAMBIO2, [ K_T_CAMBIO, K_EMPRESA ] )
     else
     if ( cbTipo.Text = K_TW_MULTIP ) then
        Result := Format( K_XML_MULTIP2, [ K_TW_MULTIP, K_EMPRESA ] )
     else
     if ( cbTipo.Text = K_T_PERM ) then
        Result := Format( K_XML_PERM, [ K_T_PERM, K_EMPRESA ] )
     else
     if ( cbTipo.Text = K_TW_AUTO3 ) then
        Result := Format( K_XML_AUTO3, [ K_TW_AUTO3, K_EMPRESA ] )
     else
     if ( cbTipo.Text = K_T_VACA ) then
        Result := Format( K_XML_VACA2, [ K_T_VACA, K_EMPRESA ] )
     else
         Result := '';
end;

procedure TTestProcesos.BtnProcesarClick(Sender: TObject);
var
   oCursor: TCursor;
   sLog: String;
begin
     with dmCliente do
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             if TressGrabar( GetXML, edUsuario.ValorEntero, sLog ) then
                MemoResult.Lines.Text := 'Empresa OK: ' + sLog
             else
                 MemoResult.Lines.Text := 'ERROR: ' + sLog;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TTestProcesos.ArchivoSeekClick(Sender: TObject);
begin
     with OpenDialog do
     begin
          FileName := ExtractFileName( Archivo.Text );
          InitialDir := ExtractFilePath( Archivo.Text );
          if Execute then
             Archivo.Text := FileName;
     end;
end;

end.
