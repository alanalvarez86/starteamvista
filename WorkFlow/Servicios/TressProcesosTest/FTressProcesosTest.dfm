object TestProcesos: TTestProcesos
  Left = 183
  Top = 203
  Width = 696
  Height = 480
  Caption = 'TestTressProcesos'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Paso2GB: TGroupBox
    Left = 0
    Top = 0
    Width = 688
    Height = 97
    Align = alTop
    Caption = ' Paso # 2 '
    TabOrder = 0
    object EmpresaLBL: TLabel
      Left = 21
      Top = 20
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'Archivo:'
    end
    object ArchivoSeek: TSpeedButton
      Left = 457
      Top = 15
      Width = 25
      Height = 25
      Hint = 'Buscar Archivo de Importaci'#243'n'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333333333333333333FFF333333333333000333333333
        3333777FFF3FFFFF33330B000300000333337F777F777773F333000E00BFBFB0
        3333777F773333F7F333000E0BFBF0003333777F7F3337773F33000E0FBFBFBF
        0333777F7F3333FF7FFF000E0BFBF0000003777F7F3337777773000E0FBFBFBF
        BFB0777F7F33FFFFFFF7000E0BF000000003777F7FF777777773000000BFB033
        33337777773FF733333333333300033333333333337773333333333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      OnClick = ArchivoSeekClick
    end
    object Label1: TLabel
      Left = 21
      Top = 70
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'Usuario:'
    end
    object Label2: TLabel
      Left = 36
      Top = 44
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo:'
    end
    object Archivo: TEdit
      Left = 64
      Top = 18
      Width = 385
      Height = 21
      TabOrder = 0
    end
    object BtnProcesar: TButton
      Left = 504
      Top = 14
      Width = 97
      Height = 27
      Caption = 'Procesar XML'
      TabOrder = 2
      OnClick = BtnProcesarClick
    end
    object edUsuario: TZetaNumero
      Left = 64
      Top = 66
      Width = 97
      Height = 21
      Mascara = mnDias
      TabOrder = 1
      Text = '0'
    end
    object cbTipo: TComboBox
      Left = 64
      Top = 41
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 3
    end
  end
  object MemoResult: TMemo
    Left = 0
    Top = 97
    Width = 688
    Height = 349
    Align = alClient
    Lines.Strings = (
      '')
    TabOrder = 1
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'xml'
    Filter = 'Archivos de XML (*.dat)|*.xml|Todos (*.*)|*.*'
    Options = [ofHideReadOnly]
    Title = 'Seleccione el Archivo a Procesar'
    Left = 614
    Top = 10
  end
end
