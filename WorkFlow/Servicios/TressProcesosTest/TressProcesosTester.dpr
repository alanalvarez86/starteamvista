program TressProcesosTester;



uses
  Forms,
  ZetaClientTools,
  FTressProcesosTest in 'FTressProcesosTest.pas' {TestProcesos},
  DBasicoCliente in '..\..\..\3Win_20\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  ZBaseDlgModal in '..\..\..\3Win_20\Tools\ZBaseDlgModal.pas' {ZetaDlgModal};

{$R *.RES}

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  Application.CreateForm(TTestProcesos, TestProcesos);
  Application.Run;
end.
