unit FWorkFlowMgrTest;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, ComObj, StdCtrls, Buttons, ExtCtrls, ComCtrls,
     DWorkFlow, DCliente,
     ZetaDBTextBox,
     ZetaAsciiFile;

type
  TWorkFlowManagerTest = class(TForm)
    PanelBotones: TPanel;
    Detener: TBitBtn;
    Depurar: TBitBtn;
    LogGB: TGroupBox;
    Log: TMemo;
    MensajesLBL: TStaticText;
    Mensajes: TZetaTextBox;
    GreenLED: TSpeedButton;
    Configurar: TBitBtn;
    StatusBar: TStatusBar;
    WebService: TBitBtn;
    bbBitacora: TBitBtn;
    GroupBox1: TGroupBox;
    MemoCambio: TMemo;
    Iniciar: TBitBtn;
    BitBtn1: TBitBtn;
    MemoVaca: TMemo;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    MemoModificaComparte: TMemo;
    MemoCambioSucursal: TMemo;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    MemoExcepcionesNomina: TMemo;
    MemoAgregaInfonavit: TMemo;
    BitBtn6: TBitBtn;
    MemoModificarInfonavit: TMemo;
    BitBtn7: TBitBtn;
    MemoAgregarPrestamo: TMemo;
    BitBtn8: TBitBtn;
    MemoSuspenderInfonavit: TMemo;
    BitBtn9: TBitBtn;
    MemoBajaEmpleado: TMemo;
    BitBtn10: TBitBtn;
    MemoCambioEquipo: TMemo;
    BitBtn11: TBitBtn;
    BitBtn12: TBitBtn;
    BitBtn13: TBitBtn;
    MemoCambioTurno: TMemo;
    MemoCambioPuesto: TMemo;
    MemoCambioSalario: TMemo;
    BitBtn14: TBitBtn;
    MemoCambioAreas: TMemo;
    BitBtn15: TBitBtn;
    MemoCambioContrato: TMemo;
    BitBtn16: TBitBtn;
    MemoAgregarPariente: TMemo;
    BitBtn17: TBitBtn;
    MemoBorrarPariente: TMemo;
    BitBtn18: TBitBtn;
    MemoModificarPariente: TMemo;
    BitBtn19: TBitBtn;
    MemoIntercambioFestivo: TMemo;
    BitBtn20: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure IniciarClick(Sender: TObject);
    procedure DetenerClick(Sender: TObject);
    procedure DepurarClick(Sender: TObject);
    procedure ConfigurarClick(Sender: TObject);
    procedure WebServiceClick(Sender: TObject);
    procedure bbBitacoraClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
    procedure BitBtn13Click(Sender: TObject);
    procedure BitBtn14Click(Sender: TObject);
    procedure BitBtn15Click(Sender: TObject);
    procedure BitBtn16Click(Sender: TObject);
    procedure BitBtn17Click(Sender: TObject);
    procedure BitBtn18Click(Sender: TObject);
    procedure BitBtn19Click(Sender: TObject);
  private
    { Private declarations }
    FWorkFlow: TWorkFlowManager;
    FCursor: TCursor;
    FAscii: TAsciiLog;
    procedure SetControls(const lStarted: Boolean);
    procedure UpdateStatus(const Loops, Actions: Integer);
    procedure LogStart;
    procedure LogEnd;
    procedure LogMensaje(const sMensaje: string);
    procedure LogError(const sMensaje: string);
    procedure LogEvent( const sMensaje: String );
    procedure LogTimeStamp( const sMensaje: String );
  protected
    { Protected declarations }
    procedure SetStatus( const sTexto: String );
  public
    { Public declarations }
  end;

var
  WorkFlowManagerTest: TWorkFlowManagerTest;

implementation

uses FWorkFlowConfig, FWebServiceTest,FProcesadorTress,  ZetaDialogo, ZetaCommonClasses, ZetaClientTools;

{$R *.DFM}

procedure TWorkFlowManagerTest.FormCreate(Sender: TObject);
begin
     FAscii := TAsciiLog.Create;
     FWorkFlow := TWorkFlowManager.Create;
     with FWorkFlow do
     begin
          ServiceCreate(Self);
          OnFeedBack := UpdateStatus;
          OnUpdateStatus := Self.SetStatus;
          Log := Self.Log.Lines;
     end;
end;

procedure TWorkFlowManagerTest.FormShow(Sender: TObject);
begin
     FCursor := Screen.Cursor;
     SetControls( False );
end;

procedure TWorkFlowManagerTest.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FAscii );
     FWorkFlow.ServiceDestroy(Self);
     FreeAndNil( FWorkFlow );
end;

procedure TWorkFlowManagerTest.UpdateStatus( const Loops, Actions: Integer);
begin
     GreenLED.Enabled := not GreenLED.Enabled;
     Mensajes.Caption := Trim( Format( '%10.0n', [ Actions / 1 ] ) );
     Forms.Application.ProcessMessages;
end;

procedure TWorkFlowManagerTest.SetStatus(const sTexto: String);
begin
     StatusBar.SimpleText := sTexto;
     Log.Lines.Add( sTexto );
//     LogMensaje( sTexto );
     Forms.Application.ProcessMessages;
end;

procedure TWorkFlowManagerTest.SetControls( const lStarted: Boolean );
begin
     Detener.Enabled := lStarted;
     Iniciar.Enabled := not lStarted;
     Depurar.Enabled := not lStarted;
     Configurar.Enabled := not lStarted;
     WebService.Enabled := not lStarted;
     bbBitacora.Enabled := not lStarted;
     Mensajes.Visible := lStarted;
     MensajesLBL.Visible := lStarted;
     GreenLED.Visible := lStarted;
     if lStarted then
     begin
          GreenLED.Enabled := True;
          Screen.Cursor := crHourglass;
     end
     else
     begin
          Screen.Cursor := FCursor;
     end;
     Forms.Application.ProcessMessages;
end;

procedure TWorkFlowManagerTest.IniciarClick(Sender: TObject);
var
   sLog : string;
begin
     LogStart;
     LogTimeStamp( 'Inicia Log' );
     try
        SetControls( True );
        try
           dmCliente := TdmCliente.Create(nil);
           try

              dmCliente.TressGrabar(MemoVaca.Text,1, 9999, sLog) ;
              LogMensaje( sLog );
           except
                 on Error: Exception do
                 begin
                      Application.HandleException( Error );
                 end;
           end;
        finally
               SetControls( False );
               FreeandNil( dmCliente );
        end;
     finally
            LogEnd;
     end;
end;

procedure TWorkFlowManagerTest.DetenerClick(Sender: TObject);
begin
     try
        FWorkFlow.Terminated := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
     //LogEnd;
end;

procedure TWorkFlowManagerTest.DepurarClick(Sender: TObject);
begin
     if ZetaDialogo.ZWarningConfirm( 'Depura', 'Se borraran todos los registros de WorkFlow', 0, mbNo ) then
     begin
          Screen.Cursor := crHourglass;
          try
             FWorkFlow.Depurar;
          finally
                 Screen.Cursor := FCursor;
          end;
     end;
end;

procedure TWorkFlowManagerTest.ConfigurarClick(Sender: TObject);
begin
     Screen.Cursor := crHourglass;
     try
        FWorkFlowConfig.UpdateWorkFlowRegistry( FWorkFlow );
     finally
            Screen.Cursor := FCursor;
     end;
end;

procedure TWorkFlowManagerTest.WebServiceClick(Sender: TObject);
begin
     Screen.Cursor := crHourglass;
     try
        FWebServiceTest.ProbarWebService;
     finally
            Screen.Cursor := FCursor;
     end;
end;

procedure TWorkFlowManagerTest.bbBitacoraClick(Sender: TObject);
var
   sArchivo: string;
begin
     sArchivo := Format( 'C:\Bitacora-%s%s.log', [ formatDateTime( 'DDMMYYY', Date ), FormatDateTime( 'HHMMSS', Time ) ] );
     Log.Lines.SaveToFile( sArchivo );
     if ZetaDialogo.zConfirm( Caption, 'El Archivo ' + sArchivo + ' Fu� Creado' + CR_LF + '� Desea Verlo ?', 0, mbYes ) then
     begin
          ZetaClientTools.ExecuteFile( 'NOTEPAD.EXE', sArchivo, ExtractFileDir( sArchivo ), SW_SHOWDEFAULT );
     end;
end;

procedure TWorkFlowManagerTest.LogStart;
begin
     FAscii.Init( ChangeFileExt( Application.ExeName, '.LOG' ) );
end;

procedure TWorkFlowManagerTest.LogEnd;
begin
     FAscii.Close
end;

procedure TWorkFlowManagerTest.LogMensaje(const sMensaje: string);
var
   strAux : string;
begin
     strAux := DateTimeToStr( NOW ) + ' - ' + sMensaje;
     FAscii.WriteTexto( strAux );

     {$ifdef GUI}     Log.Lines.Add( strAux );{$endif}
end;

procedure TWorkFlowManagerTest.LogError(const sMensaje: string);
var
   strAux: string;
begin
     strAux := DateTimeToStr( NOW ) + ' - ' + sMensaje;
     FAscii.WriteTexto( strAux );
          {$ifdef GUI}     Log.Lines.Add( strAux );{$endif}
end;

procedure TWorkFlowManagerTest.LogEvent( const sMensaje: String );
var strAux : string;
begin
     strAux := DateTimeToStr( NOW ) + ' - ' + sMensaje;
     FAscii.WriteTexto( strAux );
          {$ifdef GUI}     Log.Lines.Add( strAux );{$endif}
end;

procedure TWorkFlowManagerTest.LogTimeStamp( const sMensaje: String );
begin
     FAscii.WriteTimeStamp( StringOfChar( '*', 15 ) +  sMensaje + StringOfChar( '*', 15 ) );
end;

procedure TWorkFlowManagerTest.BitBtn1Click(Sender: TObject);
var
   procesador : TProcesadorTress;
   sLog : string;
begin
     procesador := TProcesadorTress.Create;
     procesador.GetConfiguracion('algo' , 1, sLog);
     LogMensaje( sLog );
     FreeAndNil( procesador ); 

end;

procedure TWorkFlowManagerTest.BitBtn2Click(Sender: TObject);
var
   sLog : string;
begin
     LogStart;
     LogTimeStamp( 'Inicia Log' );
     try
        SetControls( True );
        try
           dmCliente := TdmCliente.Create(nil);
           try

              dmCliente.TressGrabar(MemoCambio.Text,1, 9999, sLog) ;
              LogMensaje( sLog );
           except
                 on Error: Exception do
                 begin
                      Application.HandleException( Error );
                 end;
           end;
        finally
               SetControls( False );
               FreeandNil( dmCliente );
        end;
     finally
            LogEnd;
     end;
end;

procedure TWorkFlowManagerTest.BitBtn3Click(Sender: TObject);
var
   sLog : string;
begin
     LogStart;
     LogTimeStamp( 'Inicia Log' );
     try
        SetControls( True );
        try
           dmCliente := TdmCliente.Create(nil);
           try

              dmCliente.TressGrabar(MemoCambioSucursal.Text,1, 9999, sLog) ;
              LogMensaje( sLog );
           except
                 on Error: Exception do
                 begin
                      Application.HandleException( Error );
                 end;
           end;
        finally
               SetControls( False );
               FreeandNil( dmCliente );
        end;
     finally
            LogEnd;
     end;
end;

procedure TWorkFlowManagerTest.BitBtn4Click(Sender: TObject);
var
   sLog : string;
begin
     LogStart;
     LogTimeStamp( 'Inicia Log' );
     try
        SetControls( True );
        try
           dmCliente := TdmCliente.Create(nil);
           try

              dmCliente.TressGrabar(MemoExcepcionesNomina.Text,1, 9999, sLog) ;
              LogMensaje( sLog );
           except
                 on Error: Exception do
                 begin
                      Application.HandleException( Error );
                 end;
           end;
        finally
               SetControls( False );
               FreeandNil( dmCliente );
        end;
     finally
            LogEnd;
     end;
end;

procedure TWorkFlowManagerTest.BitBtn5Click(Sender: TObject);
var
   sLog : string;
begin
     LogStart;
     LogTimeStamp( 'Inicia Log' );
     try
        SetControls( True );
        try
           dmCliente := TdmCliente.Create(nil);
           try
              dmCliente.TressGrabar(MemoModificaComparte.Text,1, 9999, sLog) ;
              LogMensaje( sLog );
           except
                 on Error: Exception do
                 begin
                      Application.HandleException( Error );
                 end;
           end;
        finally
               SetControls( False );
               FreeandNil( dmCliente );
        end;
     finally
            LogEnd;
     end;
end;

//PENSKE Credito Infonavit
procedure TWorkFlowManagerTest.BitBtn6Click(Sender: TObject);
var
   sLog : string;
begin
     LogStart;
     LogTimeStamp( 'Inicia Log' );
     try
        SetControls( True );
        try
           dmCliente := TdmCliente.Create(nil);
           try
              dmCliente.TressGrabar(MemoAgregaInfonavit.Text,1, 9999, sLog) ;
              LogMensaje( sLog );
           except
                 on Error: Exception do
                 begin
                      Application.HandleException( Error );
                 end;
           end;
        finally
               SetControls( False );
               FreeandNil( dmCliente );
        end;
     finally
            LogEnd;
     end;

end;

procedure TWorkFlowManagerTest.BitBtn7Click(Sender: TObject);
var
   sLog : string;
begin
     LogStart;
     LogTimeStamp( 'Inicia Log' );
     try
        SetControls( True );
        try
           dmCliente := TdmCliente.Create(nil);
           try
              dmCliente.TressGrabar(MemoModificarInfonavit.Text,1, 9999, sLog) ;
              LogMensaje( sLog );
           except
                 on Error: Exception do
                 begin
                      Application.HandleException( Error );
                 end;
           end;
        finally
               SetControls( False );
               FreeandNil( dmCliente );
        end;
     finally
            LogEnd;
     end;
end;

procedure TWorkFlowManagerTest.BitBtn8Click(Sender: TObject);
var
   sLog : string;
begin
     LogStart;
     LogTimeStamp( 'Inicia Log' );
     try
        SetControls( True );
        try
           dmCliente := TdmCliente.Create(nil);
           try
              dmCliente.TressGrabar(MemoSuspenderInfonavit.Text,1, 9999, sLog) ;
              LogMensaje( sLog );
           except
                 on Error: Exception do
                 begin
                      Application.HandleException( Error );
                 end;
           end;
        finally
               SetControls( False );
               FreeandNil( dmCliente );
        end;
     finally
            LogEnd;
     end;
end;

procedure TWorkFlowManagerTest.BitBtn9Click(Sender: TObject);
var
   sLog : string;
begin
     LogStart;
     LogTimeStamp( 'Inicia Log' );
     try
        SetControls( True );
        try
           dmCliente := TdmCliente.Create(nil);
           try
              dmCliente.TressGrabar(MemoAgregarPrestamo.Text ,1, 9999, sLog) ;
              LogMensaje( sLog );
           except
                 on Error: Exception do
                 begin
                      Application.HandleException( Error );
                 end;
           end;
        finally
               SetControls( False );
               FreeandNil( dmCliente );
        end;
     finally
            LogEnd;
     end;
end;

procedure TWorkFlowManagerTest.BitBtn10Click(Sender: TObject);
var
   sLog : string;
begin
     LogStart;
     LogTimeStamp( 'Inicia Log' );
     try
        SetControls( True );
        try
           dmCliente := TdmCliente.Create(nil);
           try
              dmCliente.TressGrabar(MemoCambioEquipo.Text ,1, 9999, sLog) ;
              LogMensaje( sLog );
           except
                 on Error: Exception do
                 begin
                      Application.HandleException( Error );
                 end;
           end;
        finally
               SetControls( False );
               FreeandNil( dmCliente );
        end;
     finally
            LogEnd;
     end;
end;

procedure TWorkFlowManagerTest.BitBtn11Click(Sender: TObject);
var
   sLog : string;
begin
     LogStart;
     LogTimeStamp( 'Inicia Log' );
     try
        SetControls( True );
        try
           dmCliente := TdmCliente.Create(nil);
           try
              dmCliente.TressGrabar(MemoBajaEmpleado.Text ,1, 9999, sLog) ;
              LogMensaje( sLog );
           except
                 on Error: Exception do
                 begin
                      Application.HandleException( Error );
                 end;
           end;
        finally
               SetControls( False );
               FreeandNil( dmCliente );
        end;
     finally
            LogEnd;
     end;
end;

procedure TWorkFlowManagerTest.BitBtn12Click(Sender: TObject);
var
   sLog : string;
begin
     LogStart;
     LogTimeStamp( 'Inicia Log' );
     try
        SetControls( True );
        try
           dmCliente := TdmCliente.Create(nil);
           try
              dmCliente.TressGrabar(MemoCambioPuesto.Text ,1, 9999, sLog) ;
              LogMensaje( sLog );
           except
                 on Error: Exception do
                 begin
                      Application.HandleException( Error );
                 end;
           end;
        finally
               SetControls( False );
               FreeandNil( dmCliente );
        end;
     finally
            LogEnd;
     end;
end;

procedure TWorkFlowManagerTest.BitBtn13Click(Sender: TObject);
var
   sLog : string;
begin
     LogStart;
     LogTimeStamp( 'Inicia Log' );
     try
        SetControls( True );
        try
           dmCliente := TdmCliente.Create(nil);
           try
              dmCliente.TressGrabar(MemoCambioTurno.Text ,1, 9999, sLog) ;
              LogMensaje( sLog );
           except
                 on Error: Exception do
                 begin
                      Application.HandleException( Error );
                 end;
           end;
        finally
               SetControls( False );
               FreeandNil( dmCliente );
        end;
     finally
            LogEnd;
     end;
end;

procedure TWorkFlowManagerTest.BitBtn14Click(Sender: TObject);
var
   sLog : string;
begin
     LogStart;
     LogTimeStamp( 'Inicia Log' );
     try
        SetControls( True );
        try
           dmCliente := TdmCliente.Create(nil);
           try
              dmCliente.TressGrabar(MemoCambioSalario.Text ,1, 9999, sLog) ;
              LogMensaje( sLog );
           except
                 on Error: Exception do
                 begin
                      Application.HandleException( Error );
                 end;
           end;
        finally
               SetControls( False );
               FreeandNil( dmCliente );
        end;
     finally
            LogEnd;
     end;
end;

procedure TWorkFlowManagerTest.BitBtn15Click(Sender: TObject);
var
   sLog : string;
   //i : integer;
begin
     //for i := 0 to ( 10000 ) do
     //begin

     LogStart;
     LogTimeStamp( 'Inicia Log' );
     try
        SetControls( True );
        try
           dmCliente := TdmCliente.Create(nil);
           try
              dmCliente.TressGrabar(MemoCambioAreas.Text ,1, 9999, sLog) ;
              LogMensaje( sLog );
           except
                 on Error: Exception do
                 begin
                      Application.HandleException( Error );
                 end;
           end;
        finally
               SetControls( False );
               FreeandNil( dmCliente );
        end;
     finally
            LogEnd;
     end;

     //end;
end;

procedure TWorkFlowManagerTest.BitBtn16Click(Sender: TObject);
var
   sLog : string;
begin
     LogStart;
     LogTimeStamp( 'Inicia Log' );
     try
        SetControls( True );
        try
           dmCliente := TdmCliente.Create(nil);
           try
              dmCliente.TressGrabar(MemoIntercambioFestivo.Text ,1, 9999, sLog) ;
              LogMensaje( sLog );
           except
                 on Error: Exception do
                 begin
                      Application.HandleException( Error );
                 end;
           end;
        finally
               SetControls( False );
               FreeandNil( dmCliente );
        end;
     finally
            LogEnd;
     end;
end;

procedure TWorkFlowManagerTest.BitBtn17Click(Sender: TObject);
var
   sLog : string;
begin
     LogStart;
     LogTimeStamp( 'Inicia Log' );
     try
        SetControls( True );
        try
           dmCliente := TdmCliente.Create(nil);
           try
              dmCliente.TressGrabar(MemoAgregarPariente.Text ,1, 9999, sLog) ;
              LogMensaje( sLog );
           except
                 on Error: Exception do
                 begin
                      Application.HandleException( Error );
                 end;
           end;
        finally
               SetControls( False );
               FreeandNil( dmCliente );
        end;
     finally
            LogEnd;
     end;
end;

procedure TWorkFlowManagerTest.BitBtn18Click(Sender: TObject);
var
   sLog : string;
begin
     LogStart;
     LogTimeStamp( 'Inicia Log' );
     try
        SetControls( True );
        try
           dmCliente := TdmCliente.Create(nil);
           try
              dmCliente.TressGrabar(MemoBorrarPariente.Text ,1, 9999, sLog) ;
              LogMensaje( sLog );
           except
                 on Error: Exception do
                 begin
                      Application.HandleException( Error );
                 end;
           end;
        finally
               SetControls( False );
               FreeandNil( dmCliente );
        end;
     finally
            LogEnd;
     end;
end;

procedure TWorkFlowManagerTest.BitBtn19Click(Sender: TObject);
var
   sLog : string;
begin
     LogStart;
     LogTimeStamp( 'Inicia Log' );
     try
        SetControls( True );
        try
           dmCliente := TdmCliente.Create(nil);
           try
              dmCliente.TressGrabar(MemoModificarPariente.Text ,1, 9999, sLog) ;
              LogMensaje( sLog );
           except
                 on Error: Exception do
                 begin
                      Application.HandleException( Error );
                 end;
           end;
        finally
               SetControls( False );
               FreeandNil( dmCliente );
        end;
     finally
     end;
end;

end.
