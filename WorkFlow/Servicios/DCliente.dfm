inherited dmCliente: TdmCliente
  OldCreateOrder = True
  Height = 547
  Width = 807
  inherited cdsCompany: TZetaClientDataSet
    Left = 40
  end
  object cdsLista: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 136
    Top = 16
  end
  object cdsComparte: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'CM_NOMBRE'
    Params = <>
    Left = 136
    Top = 72
  end
  object cdsEmpleado: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlEnviarDatos = cdsEmpleadoAlEnviarDatos
    Left = 232
    Top = 16
  end
  object cdsImportInfonavit: TServerDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsImportInfonavitBeforePost
    Left = 40
    Top = 136
  end
  object dsCredNoActivosTress: TDataSource
    Left = 176
    Top = 136
  end
  object cdsProcesos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 320
    Top = 16
  end
  object cdsLogDetail: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 232
    Top = 72
  end
  object cdsLookupInfonavit: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 320
    Top = 80
  end
  object cdsHisCreInfonavit: TZetaClientDataSet
    Tag = 131
    Aggregates = <>
    IndexFieldNames = 'KI_FECHA'
    Params = <>
    AlAdquirirDatos = cdsHisCreInfonavitAlAdquirirDatos
    Left = 312
    Top = 144
  end
  object cdsPuestos: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'PU_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsPuestosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Puestos'
    LookupDescriptionField = 'PU_DESCRIP'
    LookupKeyField = 'PU_CODIGO'
    LookupActivoField = 'PU_ACTIVO'
    LookupConfidenField = 'PU_NIVEL0'
    Left = 32
    Top = 200
  end
  object cdsClasifi: TZetaLookupDataSet
    Tag = 4
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsClasifiAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Clasificaciones'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    LookupActivoField = 'TB_ACTIVO'
    LookupConfidenField = 'TB_NIVEL0'
    Left = 104
    Top = 200
  end
  object cdsTurnos: TZetaLookupDataSet
    Tag = 1
    Aggregates = <>
    IndexFieldNames = 'TU_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsTurnosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Turnos'
    LookupDescriptionField = 'TU_DESCRIP'
    LookupKeyField = 'TU_CODIGO'
    LookupActivoField = 'TU_ACTIVO'
    LookupConfidenField = 'TU_NIVEL0'
    Left = 168
    Top = 200
  end
  object cdsMotivoBaja: TZetaLookupDataSet
    Tag = 28
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    StoreDefs = True
    AlAdquirirDatos = cdsMotivoBajaAlAdquirirDatos
    UsaCache = True
    LookupName = 'Motivo de Baja'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 232
    Top = 200
  end
  object cdsEquipoNivelSupervisor: TZetaLookupDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    StoreDefs = True
    AlAdquirirDatos = cdsEquipoNivelSupervisorAlAdquirirDatos
    UsaCache = True
    LookupName = 'Equipo Nivel Supervisor'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 336
    Top = 200
  end
  object ZetaLookupDataSet1: TZetaLookupDataSet
    Tag = 4
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsClasifiAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Clasificaciones'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    LookupActivoField = 'TB_ACTIVO'
    LookupConfidenField = 'TB_NIVEL0'
    Left = 24
    Top = 264
  end
  object cdsNivel1: TZetaLookupDataSet
    Tag = 18
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    StoreDefs = True
    AlAdquirirDatos = cdsNivel1AlAdquirirDatos
    UsaCache = True
    LookupName = 'Nivel 1'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 104
    Top = 272
  end
  object cdsNivel2: TZetaLookupDataSet
    Tag = 19
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    StoreDefs = True
    AlAdquirirDatos = cdsNivel2AlAdquirirDatos
    UsaCache = True
    LookupName = 'Nivel 2'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 160
    Top = 272
  end
  object cdsNivel3: TZetaLookupDataSet
    Tag = 20
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    StoreDefs = True
    AlAdquirirDatos = cdsNivel3AlAdquirirDatos
    UsaCache = True
    LookupName = 'Nivel 3'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 216
    Top = 272
  end
  object cdsNivel4: TZetaLookupDataSet
    Tag = 21
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    StoreDefs = True
    AlAdquirirDatos = cdsNivel4AlAdquirirDatos
    UsaCache = True
    LookupName = 'Nivel 4'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 272
    Top = 272
  end
  object cdsNivel5: TZetaLookupDataSet
    Tag = 22
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    StoreDefs = True
    AlAdquirirDatos = cdsNivel5AlAdquirirDatos
    UsaCache = True
    LookupName = 'Nivel 5'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 328
    Top = 272
  end
  object cdsNivel6: TZetaLookupDataSet
    Tag = 23
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    StoreDefs = True
    AlAdquirirDatos = cdsNivel6AlAdquirirDatos
    UsaCache = True
    LookupName = 'Nivel 6'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 384
    Top = 272
  end
  object cdsNivel7: TZetaLookupDataSet
    Tag = 24
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    StoreDefs = True
    AlAdquirirDatos = cdsNivel7AlAdquirirDatos
    UsaCache = True
    LookupName = 'Nivel 7'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 104
    Top = 320
  end
  object cdsNivel8: TZetaLookupDataSet
    Tag = 25
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    StoreDefs = True
    AlAdquirirDatos = cdsNivel8AlAdquirirDatos
    UsaCache = True
    LookupName = 'Nivel 8'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 160
    Top = 320
  end
  object cdsNivel9: TZetaLookupDataSet
    Tag = 26
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    StoreDefs = True
    AlAdquirirDatos = cdsNivel9AlAdquirirDatos
    UsaCache = True
    LookupName = 'Nivel 9'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 216
    Top = 320
  end
  object cdsContratos: TZetaLookupDataSet
    Tag = 26
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    StoreDefs = True
    AlAdquirirDatos = cdsContratosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Contratos'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 304
    Top = 320
  end
  object cdsEmpParientes: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'PA_RELACIO'
    Params = <>
    AlAdquirirDatos = cdsEmpParientesAlAdquirirDatos
    Left = 440
    Top = 16
  end
  object cdsSegGastosMed: TZetaLookupDataSet
    Tag = 57
    Aggregates = <>
    IndexFieldNames = 'PM_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsSegGastosMedAlAdquirirDatos
    LookupName = 'P'#243'lizas de SGM'
    LookupDescriptionField = 'PM_DESCRIP'
    LookupKeyField = 'PM_CODIGO'
    Left = 464
    Top = 200
  end
  object cdsHisSGM: TZetaClientDataSet
    Tag = 131
    Aggregates = <>
    Params = <>
    BeforePost = cdsHisSGMBeforePost
    OnNewRecord = cdsHisSGMNewRecord
    OnReconcileError = cdsHisSGMReconcileError
    AlAdquirirDatos = cdsHisSGMAlAdquirirDatos
    AlEnviarDatos = cdsHisSGMAlEnviarDatos
    Left = 440
    Top = 80
  end
  object cdsVigenciasSGM: TZetaLookupDataSet
    Tag = 58
    Aggregates = <>
    IndexFieldNames = 'PM_CODIGO'
    Params = <>
    LookupName = 'Vigencias SGM'
    LookupDescriptionField = 'PV_REFEREN'
    Left = 441
    Top = 136
  end
  object cdsAltaEmpleadosWFN: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 400
    Top = 328
  end
  object cdsEstado: TZetaLookupDataSet
    Tag = 14
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    StoreDefs = True
    AlAdquirirDatos = cdsEstadoAlAdquirirDatos
    UsaCache = True
    LookupName = 'Estados del Pa'#237's'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    LookupConfidenField = 'TB_NIVEL0'
    Left = 472
    Top = 272
  end
  object cdsEditHisKardex: TZetaClientDataSet
    Tag = 18
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'cdsHisKardexIndex1'
      end>
    IndexFieldNames = 'CB_FECHA'
    Params = <>
    StoreDefs = True
    BeforePost = cdsEditHisKardexBeforePost
    AlEnviarDatos = cdsEditHisKardexAlEnviarDatos
    Left = 524
    Top = 336
  end
  object cdsEmpleadoGenerico: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 400
    Top = 384
  end
end
