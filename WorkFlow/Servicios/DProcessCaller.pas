unit DProcessCaller;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, ActiveX, Classes, ComObj, TressProcesos_TLB, StdVcl;

type
  TTressProcessCaller = class(TTypedComObject, ITressProcessCaller)
  protected
    {Declare ITressProcessCaller methods here}
  end;

implementation

uses ComServ;

initialization
  TTypedComObjectFactory.Create(ComServer, TTressProcessCaller, Class_TressProcessCaller,
    ciMultiInstance, tmApartment);
end.
