library Comparte;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  ComServ,
  DServerComparte in '..\MTS\DServerComparte.pas' {dmServerComparte: TMtsDataModule} {dmServerComparte: CoClass},
  Comparte_TLB in '..\MTS\Comparte_TLB.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ../MTS/Comparte.TLB}

{$R *.RES}

begin
end.
