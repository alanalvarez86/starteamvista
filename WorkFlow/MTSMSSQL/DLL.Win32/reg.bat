@echo off
if "%1" == "" goto :usage
%windir%\syswow64\regsvr32 /s %1
if     %errorlevel% == 0 echo Registrado %1
if not %errorlevel% == 0 echo    Error %errorlevel% %1
goto :end

:usage
echo.
echo Falta el nombre del DLL para registrar
echo  Uso
echo   "%0 archivo.dll"
pause

:end