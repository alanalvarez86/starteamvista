unit FEditGradoEstudio_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, Db,
  Buttons, ExtCtrls, ZetaEdit, ZetaKeyCombo, ZetaNumero, ZetaSmartLists,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TTOEditEstudios_DevEx = class(TBaseEdicion_DevEx)
    DBInglesLBL: TLabel;
    DBDescripcionLBL: TLabel;
    DBCodigoLBL: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    TB_INGLES: TDBEdit;
    TB_TEXTO: TDBEdit;
    TB_TIPO: TZetaDBKeyCombo;
    DBCampo1LBL: TLabel;
    TB_NUMERO: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
    
  protected
    procedure Connect;override;
    procedure DoLookup; override;
  public
  end;

var
  EditGradoEstudio: TTOEditEstudios_DevEx;

implementation

uses dTablas,
     ZetaBuscador_DevEx,
     ZetaCommonClasses,
     ZAccesosTress;

{$R *.DFM}

procedure TTOEditEstudios_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := TB_CODIGO;
     IndexDerechos := ZAccesosTress.D_TAB_PER_ESTUDIOS;
     HelpContext:= H_Grado_Estudios_Edicion;
end;

procedure TTOEditEstudios_DevEx.Connect;
begin
     with dmTablas do
     begin
          DataSource.DataSet := cdsEstudios;
          Self.Caption := cdsEstudios.LookupName;
     end;
end;

procedure TTOEditEstudios_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Self.Caption, 'TB_CODIGO', dmTablas.cdsEstudios );
end;


end.
