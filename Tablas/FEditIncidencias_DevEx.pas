unit FEditIncidencias_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, Db,
  Buttons, ExtCtrls, ZetaKeyCombo, ZetaEdit, ZetaNumero, ZetaSmartLists,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditIncidencias_DevEx = class(TBaseEdicion_DevEx)
    DBInglesLBL: TLabel;
    DBDescripcionLBL: TLabel;
    DBCodigoLBL: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    TB_INCAPALb: TLabel;
    TB_INCIDEN: TZetaDBKeyCombo;
    TB_INCAPA: TZetaDBKeyCombo;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    TB_INGLES: TDBEdit;
    TB_TEXTO: TDBEdit;
    TB_SISTEMA: TDBCheckBox;
    TB_NUMERO: TZetaDBNumero;
    lblClasePermiso: TLabel;
    TB_PERMISO: TZetaDBKeyCombo;
    procedure FormShow(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormCreate(Sender: TObject);
    procedure TB_INCIDENExit(Sender: TObject);
    procedure TB_INCIDENChange(Sender: TObject);
  private
    procedure ShowCombos;
    procedure AsignaDefaults;
  protected
    procedure Connect;override;
    procedure DoLookup; override;
  public
  end;

var
  EditIncidencias_DevEx: TEditIncidencias_DevEx;

implementation

{$R *.DFM}

uses DTablas, ZetaCommonLists, ZetaCommonClasses, ZetaDialogo, ZAccesosTress, ZetaBuscador_DevEx;

procedure TEditIncidencias_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     Caption := 'Incidencias';
     HelpContext:= H70743_Incidencias;
     IndexDerechos := ZAccesosTress.D_TAB_HIST_INCIDEN;
     TB_PERMISO.Items.Insert(0,'< Todos >');
     FirstControl := TB_CODIGO;
end;

procedure TEditIncidencias_DevEx.Connect;
begin
     DataSource.DataSet := dmTablas.cdsIncidencias;
end;

procedure TEditIncidencias_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', dmTablas.cdsIncidencias );
end;

procedure TEditIncidencias_DevEx.ShowCombos;
var
   lEsPermiso: Boolean;
begin
     if ( TB_INCIDEN.ItemIndex = Ord(eiIncapacidad) ) then
     begin
          if ( TB_INCAPA.ItemIndex = Ord(inNoIncapacidad) ) then
             TB_INCAPA.ItemIndex := Ord(inGeneral);
          TB_INCAPA.Enabled:= TRUE;
          TB_INCAPALb.Enabled:= TRUE;
     end
     else
     begin
          TB_INCAPA.ItemIndex := Ord(inNoIncapacidad);
          TB_INCAPA.Enabled   := FALSE;
          TB_INCAPALb.Enabled := FALSE;
     end;
      lEsPermiso := TB_INCIDEN.ItemIndex = Ord(eiPermiso);
      TB_PERMISO.Enabled := lEsPermiso;
      lblClasePermiso.Enabled := lEsPermiso;
end;

procedure TEditIncidencias_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     ShowCombos;
end;

procedure TEditIncidencias_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if (DataSource.DataSet.State = dsBrowse) then
        ShowCombos;
     TB_CODIGO.Enabled:= not TB_SISTEMA.Checked; // No lleva if porque TB_SISTEMA solo cambia desde fabrica
     DBCodigoLBL.Enabled:= not TB_SISTEMA.Checked;
end;

procedure TEditIncidencias_DevEx.TB_INCIDENExit(Sender: TObject);
begin
     AsignaDefaults;
end;

procedure TEditIncidencias_DevEx.AsignaDefaults;
begin
     with DataSource.DataSet do
          if (State <> dsBrowse) then
          begin
               if ( TB_INCIDEN.ItemIndex = Ord(eiIncapacidad) ) then
               begin
                    if FieldByName('TB_INCAPA').AsInteger = Ord(inNoIncapacidad) then
                       FieldByName('TB_INCAPA').AsInteger := Ord(inGeneral);
               end
               else
                   FieldByName('TB_INCAPA').AsInteger := Ord(inNoIncapacidad);
          end;
         { Tipo Incidencia:TB_INCIDEN,  ComboBox con Valores Status, Default = 1 (Falta)
		Si es igual a 2 (Incapacidad)
			-Si TB_INCAPA = 0, asignar TB_INCAPA := 1
			-Habilitar control de TB_INCAPA
		else
			-TB_INCAPA := 0
			-Deshabilitar control de TB_INCAPA }
end;

procedure TEditIncidencias_DevEx.TB_INCIDENChange(Sender: TObject);
begin
     ShowCombos;
end;

end.
