unit FEditMonedas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, Db,
  Buttons, ExtCtrls, ZetaKeyCombo,ZetaCommonLists, ZetaNumero, ZetaEdit,
  ZetaSmartLists, ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TEditMonedas_DevEx = class(TBaseEdicion_DevEx)
    DBInglesLBL: TLabel;
    DBDescripcionLBL: TLabel;
    DBCodigoLBL: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    TB_INGLES: TDBEdit;
    TB_TEXTO: TDBEdit;
    TB_VALOR: TZetaDBNumero;
    TB_NUMERO: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
  protected
    procedure Connect;override;
    procedure DoLookup; override;
  public
  end;

var
  EditMonedas_DevEx: TEditMonedas_DevEx;

implementation

uses DTablas, ZetaCommonClasses, ZAccesosTress, ZetaBuscador_DevEx;

{$R *.DFM}

procedure TEditMonedas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H70753_Monedas;
     FirstControl := TB_CODIGO;
     IndexDerechos := ZAccesosTress.D_TAB_NOM_MONEDAS;
end;

procedure TEditMonedas_DevEx.Connect;
begin
     Datasource.Dataset := dmTablas.cdsMonedas;
end;

procedure TEditMonedas_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', dmTablas.cdsMonedas );
end;

end.
