unit FEditMotivoAuto_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, Db,
  Buttons, ExtCtrls, ZetaEdit, ZetaKeyCombo, ZetaNumero, ZetaSmartLists,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TTOEditMotivoAutorizacion_DevEx = class(TBaseEdicion_DevEx)
    DBInglesLBL: TLabel;
    DBDescripcionLBL: TLabel;
    DBCodigoLBL: TLabel;
    TEXTOLBL: TLabel;
    NumeroLBL: TLabel;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    TB_INGLES: TDBEdit;
    TB_TEXTO: TDBEdit;
    TB_TIPO: TZetaDBKeyCombo;
    TB_TIPOLBL: TLabel;
    TB_NUMERO: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
  protected
    procedure Connect;override;
    procedure DoLookup; override;
  public
  end;

var
  EditMotivoAutorizacion_DevEx: TTOEditMotivoAutorizacion_DevEx;

implementation

uses dTablas,
     DGlobal,
     ZetaBuscador_DevEx,
     ZetaCommonClasses,
     ZGlobalTress,
     ZetaCommonLists,
     ZAccesosTress;

{$R *.DFM}

procedure TTOEditMotivoAutorizacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := TB_CODIGO;
     HelpContext:= H70745_Motivos_de_Autorizacion;
     IndexDerechos := D_TAB_HIST_AUT_EXTRAS;
     TB_TIPO.Items.Insert(0,'<Todos>');
end;

procedure TTOEditMotivoAutorizacion_DevEx.Connect;
begin
     with dmTablas do
     begin
          DataSource.DataSet := cdsMotAuto;
          self.Caption := cdsMotAuto.LookupName;
     end;
end;

procedure TTOEditMotivoAutorizacion_DevEx.DoLookup;
begin
     inherited;
     if dxBarButton_BuscarBtn.Enabled then
        ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', dmTablas.cdsMotAuto );
end;




end.
