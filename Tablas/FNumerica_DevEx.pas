unit FNumerica_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient, ExtCtrls,
     StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
      TressMorado2013, dxSkinsDefaultPainters,
     dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
     cxEdit, cxNavigator, cxDBData, Menus, ActnList, ImgList, cxGridLevel,
     cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
     cxGridDBTableView, cxGrid, ZetaCXGrid, Grids, DBGrids, ZetaDBGrid;

type
  TSistNumerica_DevEx = class(TBaseGridLectura_DevEx)
    NU_CODIGO: TcxGridDBColumn;
    NU_DESCRIP: TcxGridDBColumn;
    NU_INGLES: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  SistNumerica_DevEx: TSistNumerica_DevEx;

implementation

uses DTablas,
     ZetaBuscaEntero_DevEx,
     ZetaCommonClasses;

{$R *.DFM}

procedure TSistNumerica_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H70763_ISPT_numericas;
end;

procedure TSistNumerica_DevEx.Connect;
begin
     with dmTablas do
     begin
          cdsNumericas.Conectar;
          DataSource.DataSet:= cdsNumericas;
     end;
end;

procedure TSistNumerica_DevEx.Refresh;
begin
     dmTablas.cdsNumericas.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TSistNumerica_DevEx.Agregar;
begin
     dmTablas.cdsNumericas.Agregar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TSistNumerica_DevEx.Borrar;
begin
     dmTablas.cdsNumericas.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TSistNumerica_DevEx.Modificar;
begin
     dmTablas.cdsNumericas.Modificar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TSistNumerica_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscaEntero_DevEx.BuscarCodigo( 'N�mero', 'Tabla ISPT y Num�rica', 'NU_CODIGO', dmTablas.cdsNumericas );
end;

procedure TSistNumerica_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('NU_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

end.
