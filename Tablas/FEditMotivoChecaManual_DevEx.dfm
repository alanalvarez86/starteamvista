inherited TOEditMotivoChecaManual_DevEx: TTOEditMotivoChecaManual_DevEx
  Left = 396
  Top = 269
  Caption = 'Motivo de Checada Manual'
  ClientHeight = 229
  ClientWidth = 414
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 46
    Top = 61
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
  end
  object Label2: TLabel [1]
    Left = 23
    Top = 82
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object Label3: TLabel [2]
    Left = 51
    Top = 103
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ingl'#233's:'
  end
  object Label4: TLabel [3]
    Left = 42
    Top = 123
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  object Label5: TLabel [4]
    Left = 52
    Top = 144
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto:'
  end
  inherited PanelBotones: TPanel
    Top = 193
    Width = 414
    inherited OK_DevEx: TcxButton
      Left = 250
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 329
      Cancel = True
      ShowHint = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 414
    inherited Splitter: TSplitter
      Width = 2
    end
    inherited ValorActivo2: TPanel
      Left = 325
      Width = 89
      inherited textoValorActivo2: TLabel
        Width = 83
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 11
  end
  object TB_CODIGO: TZetaDBEdit [8]
    Left = 87
    Top = 57
    Width = 65
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 3
    ConfirmEdit = True
    DataField = 'TB_CODIGO'
    DataSource = DataSource
  end
  object TB_ELEMENT: TDBEdit [9]
    Left = 87
    Top = 78
    Width = 280
    Height = 21
    DataField = 'TB_ELEMENT'
    DataSource = DataSource
    TabOrder = 4
  end
  object TB_INGLES: TDBEdit [10]
    Left = 87
    Top = 99
    Width = 280
    Height = 21
    DataField = 'TB_INGLES'
    DataSource = DataSource
    TabOrder = 5
  end
  object TB_NUMERO: TZetaDBNumero [11]
    Left = 87
    Top = 119
    Width = 100
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 6
    Text = '0.00'
    DataField = 'TB_NUMERO'
    DataSource = DataSource
  end
  object TB_TEXTO: TDBEdit [12]
    Left = 87
    Top = 140
    Width = 280
    Height = 21
    DataField = 'TB_TEXTO'
    DataSource = DataSource
    TabOrder = 7
  end
  inherited DataSource: TDataSource
    Left = 284
    Top = 49
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Visible = ivAlways
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
