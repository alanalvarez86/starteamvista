unit FTipoCambio_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, ZBaseConsulta, Db, ExtCtrls,
  StdCtrls, ZBaseGridLectura_DevEx, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TTipoCambio_DevEx = class(TBaseGridLectura_DevEx)
    TC_FEC_INI: TcxGridDBColumn;
    TC_MONTO: TcxGridDBColumn;
    TC_NUMERO: TcxGridDBColumn;
    TC_TEXTO: TcxGridDBColumn;
  procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
  public
  end;

var
  TipoCambio_DevEx: TTipoCambio_DevEx;

implementation

uses dTablas, ZetaCommonClasses;

{$R *.DFM}

procedure TTipoCambio_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H70765_Tipos_de_cambio;
end;

procedure TTipoCambio_DevEx.Connect;
begin
     with dmTablas do
     begin
          cdsTipoCambio.Conectar;
          DataSource.DataSet:= cdsTipoCambio;
     end;
end;

procedure TTipoCambio_DevEx.Refresh;
begin
     dmTablas.cdsTipoCambio.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TTipoCambio_DevEx.Agregar;
begin
     dmTablas.cdsTipoCambio.Agregar;
end;

procedure TTipoCambio_DevEx.Borrar;
begin
     dmTablas.cdsTipoCambio.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TTipoCambio_DevEx.Modificar;
begin
     dmTablas.cdsTipoCambio.Modificar;
end;

procedure TTipoCambio_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TC_FEC_INI'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();

end;

end.
