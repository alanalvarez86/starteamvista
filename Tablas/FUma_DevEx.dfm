inherited Uma_DevEx: TUma_DevEx
  Left = 288
  Top = 123
  Caption = 'Unidad de Medida y Actualizaci'#243'n'
  ClientHeight = 228
  ClientWidth = 499
  ExplicitWidth = 499
  ExplicitHeight = 228
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 499
    ExplicitWidth = 283
    inherited ImgNoRecord: TImage
      Left = 3
      ExplicitLeft = 3
    end
    inherited Slider: TSplitter
      Left = 0
      ExplicitLeft = 0
    end
    inherited ValorActivo1: TPanel
      Left = 19
      ExplicitLeft = 19
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 240
      ExplicitWidth = 24
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 234
        ExplicitLeft = -62
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 499
    Height = 209
    ExplicitWidth = 283
    ExplicitHeight = 209
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object UM_FEC_INI: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'UM_FEC_INI'
      end
      object UM_VALOR: TcxGridDBColumn
        Caption = 'Diario'
        DataBinding.FieldName = 'UM_VALOR'
      end
      object UM_VALOR_MENSUAL: TcxGridDBColumn
        Caption = 'Mensual'
        DataBinding.FieldName = 'UM_VALOR_MENSUAL'
        Options.Sorting = False
      end
      object UM_VALOR_ANUAL: TcxGridDBColumn
        Caption = 'Anual'
        DataBinding.FieldName = 'UM_VALOR_ANUAL'
        Options.Sorting = False
      end
      object UM_FDESC: TcxGridDBColumn
        Caption = 'Factor de descuento'
        DataBinding.FieldName = 'UM_FDESC'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
