unit FEditTablas_DevEx;

interface
                               
uses ZBaseTablas_DevEx;

type
  TTOEditEstadoCivil_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditHabitacion_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditViveCon_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
{
  TTOEditEstudios = class( TEditLookup )
  protected
    procedure AfterCreate; override;
  end;
}
  TTOEditTransporte_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  {TTOEditEstado = class( TEditLookup )
  protected
    {Protected declarations}
    {procedure AfterCreate; override;
  end;}
  TTOEditExtra1_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditExtra2_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditExtra3_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditExtra4_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditExtra5_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditExtra6_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditExtra7_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditExtra8_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditExtra9_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditExtra10_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditExtra11_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditExtra12_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditExtra13_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditExtra14_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditTipoCursos_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditMotCheca_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOTallas_DevEx = class( TEditLookup )     //DevEx
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOTEditClasifiCurso_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

  TTOTEditOcupaNac_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

  TTOTEditAreaTemCur_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

  TTOMotivoTransfer_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

  TEditTCompetencas_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

  TEditTPerfiles_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;


var

  EditTablas_DevEx: TEditTablas_DevEx;

implementation

uses dTablas,
     DGlobal,
     ZetaBuscador_DevEx,
     ZetaCommonClasses,
     ZGlobalTress,
     ZAccesosTress;

{ TTOEditEstadoCivil_DevEx }

procedure TTOEditEstadoCivil_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsEstadoCivil;
     HelpContext:= H70711_Estado_civil;
     IndexDerechos := ZAccesosTress.D_TAB_PER_EDOCIVIL;
end;

{ TTOEditHabitacion_DevEx }

procedure TTOEditHabitacion_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsHabitacion;
     HelpContext:= H70712_En_donde_vive;
     IndexDerechos := ZAccesosTress.D_TAB_PER_VIVE_EN;
end;

{ TTOEditViveCon_DevEx }

procedure TTOEditViveCon_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsViveCon;
     HelpContext:= H70713_Con_quien_vive;
     IndexDerechos := ZAccesosTress.D_TAB_PER_VIVE_CON;
end;

{TTOEditEstudios}
{
procedure TTOEditEstudios.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsEstudios;
     HelpContext:= H70714_Grado_estudios;
     IndexDerechos := ZAccesosTress.D_TAB_PER_ESTUDIOS;
end;
}

{ TTOEditTransporte_DevEx }

procedure TTOEditTransporte_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsTransporte;
     HelpContext:= H70715_Transporte;
     IndexDerechos := ZAccesosTress.D_TAB_PER_TRANSPORTE;
end;

{ TTOEditEstado }

{procedure TTOEditEstado.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsEstado;
     HelpContext:= H70716_Estados_pais;
     IndexDerechos := ZAccesosTress.D_TAB_PER_ESTADOS;
end;}


{ TTOEditExtra1_DevEx }

procedure TTOEditExtra1_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra1;
     HelpContext:= H70073_Adicionales;
     IndexDerechos := ZAccesosTress.D_TAB_ADICION_TABLA1;
end;

{ TTOEditExtra2_DevEx }

procedure TTOEditExtra2_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra2;
     HelpContext:= H70073_Adicionales;
     IndexDerechos := ZAccesosTress.D_TAB_ADICION_TABLA2;
end;

{ TTOEditExtra3_DevEx }

procedure TTOEditExtra3_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra3;
     HelpContext:= H70073_Adicionales;
     IndexDerechos := ZAccesosTress.D_TAB_ADICION_TABLA3;
end;

{ TTOEditExtra4_DevEx }

procedure TTOEditExtra4_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra4;
     HelpContext:= H70073_Adicionales;
     IndexDerechos := ZAccesosTress.D_TAB_ADICION_TABLA4;
end;

{ TTOEditExtra5_DevEx }

procedure TTOEditExtra5_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra5;
     HelpContext:= H70073_Adicionales;
     IndexDerechos := ZAccesosTress.D_TAB_ADICION_TABLA5;
end;

{ TTOEditExtra6_DevEx }

procedure TTOEditExtra6_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra6;
     HelpContext:= H70073_Adicionales;
     IndexDerechos := ZAccesosTress.D_TAB_ADICION_TABLA6;
end;

{ TTOEditExtra7_DevEx }

procedure TTOEditExtra7_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra7;
     HelpContext:= H70073_Adicionales;
     IndexDerechos := ZAccesosTress.D_TAB_ADICION_TABLA7;
end;

{ TTOEditExtra8_DevEx }

procedure TTOEditExtra8_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra8;
     HelpContext:= H70073_Adicionales;
     IndexDerechos := ZAccesosTress.D_TAB_ADICION_TABLA8;
end;

{ TTOEditExtra9_DevEx }

procedure TTOEditExtra9_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra9;
     HelpContext:= H70073_Adicionales;
     IndexDerechos := ZAccesosTress.D_TAB_ADICION_TABLA9;
end;

{ TTOEditExtra10_DevEx }

procedure TTOEditExtra10_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra10;
     HelpContext:= H70073_Adicionales;
     IndexDerechos := ZAccesosTress.D_TAB_ADICION_TABLA10;
end;

{ TTOEditExtra11_DevEx }

procedure TTOEditExtra11_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra11;
     HelpContext:= H70073_Adicionales;
     IndexDerechos := ZAccesosTress.D_TAB_ADICION_TABLA11;
end;

{ TTOEditExtra12_DevEx }

procedure TTOEditExtra12_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra12;
     HelpContext:= H70073_Adicionales;
     IndexDerechos := ZAccesosTress.D_TAB_ADICION_TABLA12;
end;

{ TTOEditExtra13_DevEx }

procedure TTOEditExtra13_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra13;
     HelpContext:= H70073_Adicionales;
     IndexDerechos := ZAccesosTress.D_TAB_ADICION_TABLA13;
end;

{ TTOEditExtra14_DevEx }

procedure TTOEditExtra14_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra14;
     HelpContext:= H70073_Adicionales;
     IndexDerechos := ZAccesosTress.D_TAB_ADICION_TABLA14;
end;

{ TTOEditTipoCursos }

procedure TTOEditTipoCursos_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsTipoCursos;
     HelpContext:= H70744_Tipo_curso;
     IndexDerechos := ZAccesosTress.D_CAT_CAPA_TIPO_CURSO;
end;


{ TTOEditMotCheca_DevEx }

procedure TTOEditMotCheca_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsMotCheca;
     HelpContext:= H_Motivos_Checada_Manual;
     IndexDerechos := ZAccesosTress.D_TAB_HIST_CHECA_MANUAL;
end;

{ TTOTallas_DevEx }

procedure TTOTallas_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsTallas;
     HelpContext:= H60664_Tallas;
     IndexDerechos := ZAccesosTress.D_TAB_HERR_TALLAS;
end;

{ TTOTEditClasifiCurso_DevEx }

procedure TTOTEditClasifiCurso_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsClasifiCurso;
     HelpContext:= H60646_Clase_de_cursos;
     IndexDerechos := ZAccesosTress.D_CAT_CLASIFI_CURSO;
end;

{ TTOTEditOcupaNac_DevEx }
procedure TTOTEditOcupaNac_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsOcupaNac;
     HelpContext:= H_TABLA_OCUPACIONES_NACIONALES_EDICION;
     IndexDerechos := ZAccesosTress.D_TAB_OFI_STPS_CAT_NAC_OCUPA;
end;

{ TTOTEditAreaTemCur_DevEx }
procedure TTOTEditAreaTemCur_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsAreaTemCur;
     HelpContext:= H_TABLA_AREA_TEMATICA_CURSOS_EDICION;
     IndexDerechos := ZAccesosTress.D_TAB_OFI_STPS_ARE_TEM_CUR;
end;

{ TTOMotivoTransfer_DevEx }
procedure TTOMotivoTransfer_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsMotivoTransfer;
     HelpContext:= H_Tabla_Motivo_Transferencia_Edit;
     IndexDerechos := ZAccesosTress.D_COSTEO_MOTIVOS_TRANSFER;
end;

{ TTPerfiles_DevEx }

procedure TEditTPerfiles_DevEx.AfterCreate;
begin
     inherited;
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsTPerfiles;
     HelpContext:= H_Tabla_TiposPerfiles;
     IndexDerechos := ZAccesosTress.D_TAB_TPERFILES;
end;

{ TTCompetencas_DevEx }

procedure TEditTCompetencas_DevEx.AfterCreate;
begin
     inherited;
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsTCompetencias;
     HelpContext:= H_Tabla_TiposCompetencias;
     IndexDerechos := ZAccesosTress.D_TAB_TCOMPETENCIAS;
end;

end.
