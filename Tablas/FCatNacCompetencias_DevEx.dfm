inherited CatNacCompetencias_DevEx: TCatNacCompetencias_DevEx
  Left = 259
  Top = 309
  Caption = 'Cat'#225'logo Nacional de Competencias'
  ClientHeight = 281
  ClientWidth = 902
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 902
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 576
      inherited textoValorActivo2: TLabel
        Width = 570
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 902
    Height = 262
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsData.DeletingConfirmation = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object TN_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'TN_CODIGO'
        Width = 82
      end
      object TN_TITULO: TcxGridDBColumn
        Caption = 'T'#237'tulo'
        DataBinding.FieldName = 'TN_TITULO'
        Width = 371
      end
      object TN_COMITE: TcxGridDBColumn
        Caption = 'Comit'#233
        DataBinding.FieldName = 'TN_COMITE'
        Width = 149
      end
      object TN_FEC_APR: TcxGridDBColumn
        Caption = 'Fecha Aprobaci'#243'n'
        DataBinding.FieldName = 'TN_FEC_APR'
        Width = 118
      end
      object TN_NIVEL: TcxGridDBColumn
        Caption = 'Nivel'
        DataBinding.FieldName = 'TN_NIVEL'
        Width = 99
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
