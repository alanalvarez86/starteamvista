unit FEditMotTools_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ZetaKeyCombo, Mask, DBCtrls, ZetaEdit, Db,
  ExtCtrls, Buttons, ZetaNumero, ZetaSmartLists,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, 
  TressMorado2013,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  ZetaKeyLookup_DevEx, dxSkinsCore, dxSkinsDefaultPainters;

type
  TEditMotTools_DevEx = class(TBaseEdicion_DevEx)
    DBCodigoLBL: TLabel;
    TB_CODIGO: TZetaDBEdit;
    DBDescripcionLBL: TLabel;
    TB_ELEMENT: TDBEdit;
    DBInglesLBL: TLabel;
    TB_INGLES: TDBEdit;
    Label2: TLabel;
    TB_NUMERO: TZetaDBNumero;
    Label1: TLabel;
    TB_TEXTO: TDBEdit;
    TB_TIPO: TZetaDBKeyCombo;
    Label3: TLabel;
    LblPresta: TLabel;
    TB_PRESTA: TZetaDBKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure TB_TIPOChange(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure SetEditarSoloActivos;

  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditMotTools_DevEx: TEditMotTools_DevEx;

implementation

uses dTablas, ZetaBuscador_DevEx, ZetaCommonClasses, ZetaCommonLists, ZAccesosTress;

{$R *.DFM}

procedure TEditMotTools_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos :=  D_TAB_HERR_MOTTOOL;
     HelpContext:= H60665_Motivos_Devolucion;
     FirstControl := TB_CODIGO;
     TB_PRESTA.LookupDataset := dmTablas.cdsTPresta;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TEditMotTools_DevEx.Connect;
begin
     with dmTablas do
     begin
          cdsTPresta.Conectar;
          cdsMotTool.Conectar;
          DataSource.DataSet := cdsMotTool;
     end;
end;

procedure TEditMotTools_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', dmTablas.cdsMotTool );
end;

procedure TEditMotTools_DevEx.TB_TIPOChange(Sender: TObject);
begin
     inherited;
     TB_PRESTA.Enabled:= ( eMotTools( TB_TIPO.ItemIndex ) <> mtNoDescontar );
     LblPresta.Enabled := TB_PRESTA.Enabled;
end;

procedure TEditMotTools_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     if ( Field = nil ) then
        TB_TIPOChange( self );
end;

procedure TEditMotTools_DevEx.SetEditarSoloActivos;
begin
     TB_PRESTA.EditarSoloActivos := TRUE;
end;

end.


