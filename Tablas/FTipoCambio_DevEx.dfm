inherited TipoCambio_DevEx: TTipoCambio_DevEx
  Left = 182
  Top = 197
  Caption = 'Tipos de Cambio'
  PixelsPerInch = 96
  TextHeight = 13
  inherited ZetaDBGrid: TZetaCXGrid
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsData.DeletingConfirmation = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object TC_FEC_INI: TcxGridDBColumn
        Caption = 'Fecha Inicial'
        DataBinding.FieldName = 'TC_FEC_INI'
        Width = 75
      end
      object TC_MONTO: TcxGridDBColumn
        Caption = 'Monto'
        DataBinding.FieldName = 'TC_MONTO'
      end
      object TC_NUMERO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'TC_NUMERO'
        Width = 103
      end
      object TC_TEXTO: TcxGridDBColumn
        Caption = 'Texto'
        DataBinding.FieldName = 'TC_TEXTO'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
