unit FTablas_DevEx;

interface

uses Controls, ExtCtrls, Buttons, Dialogs,
     ZBaseTablasConsulta_DevEx,
     ZetaDBGrid,
     cxCustomData,
     cxButtons,
     DCliente,
     ZetaCommonLists,Graphics;

type
  TTOHabitacion_DevEx = class(TTablaLookup)   //(icm)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEstadoCivil_DevEx = class(TTablaLookup)    //(icm)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOViveCon_DevEx = class(TTablaLookup)        //(icm)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOTransporte_DevEx = class(TTablaLookup)      //(icm)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra1_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra2_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra3_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra4_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra5_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra6_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra7_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra8_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra9_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra10_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra11_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra12_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra13_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra14_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEstado_DevEx = class(TTablaLookup)           //(icm)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOTipoCursos_DevEx = class(TTablaLookup)       //DevEx (by am)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEstudios_DevEx = class(TTablaLookup)         //(icm)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOMovKardex_DevEx = class(TTablaLookup)
  protected
{Protected declarations}
    procedure AfterCreate; override;
  end;
  TTONiveles_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
    procedure AnexarColumnaUsuario( iNivel : integer);
  end;
  TTONivel1_DevEx = class(TTONiveles_DevEx)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTONivel2_DevEx = class(TTONiveles_DevEx)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTONivel3_DevEx = class(TTONiveles_DevEx)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTONivel4_DevEx = class(TTONiveles_DevEx)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTONivel5_DevEx = class(TTONiveles_DevEx)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTONivel6_DevEx = class(TTONiveles_DevEx)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTONivel7_DevEx = class(TTONiveles_DevEx)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTONivel8_DevEx = class(TTONiveles_DevEx)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTONivel9_DevEx = class(TTONiveles_DevEx)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  {$ifdef ACS}
  //Se agregaron 3 niveles del 10 al 12
  TTONivel10_DevEx = class(TTONiveles_DevEx)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTONivel11_DevEx = class(TTONiveles_DevEx)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTONivel12_DevEx = class(TTONiveles_DevEx)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  {$endif}
  TTOIncidencias_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOMotivoBaja_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TAhorroPrestamo = class(TTablaLookup)
  private
    {Private declarations}
{$ifndef CAJAAHORRO}
    procedure FijarPrioridad(Sender: TObject);
  protected
    {Protected declarations}
    procedure AfterCreate; override;
{$endif}
  end;
  TTOTAhorro_DevEx = class(TAhorroPrestamo)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOTPresta_DevEx = class(TAhorroPrestamo)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOMotAuto_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOMotCheca_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOTallas_DevEx = class(TTablaLookup)    // (icm)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOMotTool_DevEx = class(TTablaLookup)         // (icm)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOClasifiCurso_DevEx = class(TTablaLookup)    //DevEx (by am)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOColonia_DevEx = class(TTablaLookup)         // (icm)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

  TTOOcupaNac_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

  TTOAreaTematicaCursos_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

  TTOMotivoTransfer_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

  TTOTCompetencias_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

  TTOTPerfiles_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

implementation
{$R Prioridad.res}
uses DTablas,
     DGlobal,
     ZGlobalTress,
     ZetaCommonClasses,ZBaseDlgModal_DevEx, cxGridDBTableView,
     ZBaseGridLectura_DevEx,
     FEditPrioAhorrPresta_DevEx, Forms;

//{$R *.RES}

{ ****** TTOHabitacion_DevEx ****** }

procedure TTOHabitacion_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsHabitacion;
     HelpContext := H70712_En_donde_vive;

     //columna sumatoria: cuantos
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();

end;

{ TTOEstadoCivil_DevEx }

procedure TTOEstadoCivil_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsEstadoCivil;
     HelpContext := H70711_Estado_civil;

     //columna sumatoria: cuantos
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();


end;

{ TTOViveCon_DevEx }

procedure TTOViveCon_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsViveCon;
     HelpContext := H70713_Con_quien_vive;

     //columna sumatoria: cuantos
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();

end;

{ TTOTransporte_DevEx }

procedure TTOTransporte_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsTransporte;
     HelpContext := H70715_Transporte;

     //columna sumatoria: cuantos
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();

end;

{ TTOExtra1_DevEx }

procedure TTOExtra1_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra1;
     HelpContext := H70073_Adicionales;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
end;

{ TTOExtra2_DevEx }

procedure TTOExtra2_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra2;
     HelpContext := H70073_Adicionales;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
end;

{ TTOExtra3_DevEx }

procedure TTOExtra3_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra3;
     HelpContext := H70073_Adicionales;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
end;

{ TTOExtra4_DevEx }

procedure TTOExtra4_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra4;
     HelpContext := H70073_Adicionales;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
end;

{ TTOExtra5_DevEx }

procedure TTOExtra5_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra5;
     HelpContext := H70073_Adicionales;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
end;

{ TTOExtra6_DevEx }

procedure TTOExtra6_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra6;
     HelpContext := H70073_Adicionales;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
end;

{ TTOExtra7_DevEx }

procedure TTOExtra7_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra7;
     HelpContext := H70073_Adicionales;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
end;

{ TTOExtra8_DevEx }

procedure TTOExtra8_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra8;
     HelpContext := H70073_Adicionales;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
end;

{ TTOExtra9_DevEx }

procedure TTOExtra9_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra9;
     HelpContext := H70073_Adicionales;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
end;

{ TTOExtra10_DevEx }

procedure TTOExtra10_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra10;
     HelpContext := H70073_Adicionales;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
end;

{ TTOExtra11_DevEx }

procedure TTOExtra11_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra11;
     HelpContext := H70073_Adicionales;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
end;

{ TTOExtra12_DevEx }

procedure TTOExtra12_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra12;
     HelpContext := H70073_Adicionales;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
end;

{ TTOExtra13_DevEx }

procedure TTOExtra13_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra13;
     HelpContext := H70073_Adicionales;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
end;

{ TTOExtra14_DevEx }

procedure TTOExtra14_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra14;
     HelpContext := H70073_Adicionales;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
end;

{ TTOEstado_DevEx }

procedure TTOEstado_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsEstado;
     HelpContext := H70716_Estados_pais;

     //columna sumatoria: cuantos
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();

end;

{ TTOTipoCursos_DevEx }

procedure TTOTipoCursos_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsTipoCursos;
     HelpContext := H70744_Tipo_curso;

     //columna sumatoria: cuantos
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

{ TTOEstudios_DevEx }

procedure TTOEstudios_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsEstudios;
     HelpContext := H70714_Grado_estudios;

     //columna sumatoria: cuantos
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();

end;

{ TTOMovKardex_DevEx }

procedure TTOMovKardex_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsMovKardex;
     HelpContext := H70741_Tipo_kardex;

     //columna sumatoria: cuantos
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

{ TTONiveles_DevEx}
procedure TTONiveles_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     with ZetaDBGridDBTableView.CreateColumn do
     begin
         DataBinding.FieldName := 'TB_SUB_CTA';
         Caption := 'Subcuenta';
         Width := 250;
         Visible := True;
     end;
end;

procedure TTONiveles_DevEx.AnexarColumnaUsuario( iNivel : integer );
begin
{$ifndef DOS_CAPAS}
     if ( Global.GetGlobalInteger( K_GLOBAL_NIVEL_SUPERVISOR ) = iNivel ) and
        ( Global.GetGlobalBooleano( K_GLOBAL_ENROLAMIENTO_SUPERVISORES) ) then
     begin
          with ZetaDBGridDBTableView.CreateColumn do
          begin
               DataBinding.FieldName := 'US_DESCRIP';
               Caption := 'Usuario';
               Width := 200;
               Visible := True;
          end;
     end;
{$endif}
end;

{ TTONivel1_DevEx }

procedure TTONivel1_DevEx.AfterCreate;
begin
     inherited AfterCreate;

     //Desactivar gridmode (FormCreate)
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= FALSE;

     AnexarColumnaUsuario( 1 );
     LookupDataset := dmTablas.cdsNivel1;
     HelpContext := H70072_Areas;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();

end;

{ TTONivel2_DevEx }

procedure TTONivel2_DevEx.AfterCreate;
begin
     inherited AfterCreate;

     //Desactivar gridmode (FormCreate)
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= FALSE;

     AnexarColumnaUsuario( 2 );
     LookupDataset := dmTablas.cdsNivel2;
     HelpContext := H70072_Areas;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

{ TTONivel3_DevEx }

procedure TTONivel3_DevEx.AfterCreate;
begin
     inherited AfterCreate;

     //Desactivar gridmode (FormCreate)
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= FALSE;

     AnexarColumnaUsuario( 3 );
     LookupDataset := dmTablas.cdsNivel3;
     HelpContext := H70072_Areas;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

{ TTONivel4_DevEx }

procedure TTONivel4_DevEx.AfterCreate;
begin
     inherited AfterCreate;

     //Desactivar gridmode (FormCreate)
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= FALSE;

     AnexarColumnaUsuario( 4 );
     LookupDataset := dmTablas.cdsNivel4;
     HelpContext := H70072_Areas;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

{ TTONivel5_DevEx }

procedure TTONivel5_DevEx.AfterCreate;
begin
     inherited AfterCreate;

     //Desactivar gridmode (FormCreate)
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= FALSE;

     AnexarColumnaUsuario( 5 );
     LookupDataset := dmTablas.cdsNivel5;
     HelpContext := H70072_Areas;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

{ TTONivel6_DevEx }

procedure TTONivel6_DevEx.AfterCreate;
begin
     inherited AfterCreate;

     //Desactivar gridmode (FormCreate)
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= FALSE;

     AnexarColumnaUsuario( 6 );
     LookupDataset := dmTablas.cdsNivel6;
     HelpContext := H70072_Areas;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

{ TTONivel7_DevEx }

procedure TTONivel7_DevEx.AfterCreate;
begin
     inherited AfterCreate;

     //Desactivar gridmode (FormCreate)
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= FALSE;

     AnexarColumnaUsuario( 7 );
     LookupDataset := dmTablas.cdsNivel7;
     HelpContext := H70072_Areas;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

{ TTONivel8_DevEx }

procedure TTONivel8_DevEx.AfterCreate;
begin
     inherited AfterCreate;

     //Desactivar gridmode (FormCreate)
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= FALSE;

     AnexarColumnaUsuario( 8 );
     LookupDataset := dmTablas.cdsNivel8;
     HelpContext := H70072_Areas;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

{ TTONivel9_DevEx }

procedure TTONivel9_DevEx.AfterCreate;
begin
     inherited AfterCreate;

     //Desactivar gridmode (FormCreate)
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= FALSE;

     AnexarColumnaUsuario( 9 );
     LookupDataset := dmTablas.cdsNivel9;
     HelpContext := H70072_Areas;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

{$ifdef ACS}
//Se agregaron 3 niveles del 10 al 12
{ TTONivel10_DevEx }

procedure TTONivel10_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     AnexarColumnaUsuario( 10 );
     LookupDataset := dmTablas.cdsNivel10;
     HelpContext := H70072_Areas;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

{ TTONivel11_DevEx }

procedure TTONivel11_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     AnexarColumnaUsuario( 11 );
     LookupDataset := dmTablas.cdsNivel11;
     HelpContext := H70072_Areas;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

{ TTONivel12_DevEx }

procedure TTONivel12_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     AnexarColumnaUsuario( 12 );
     LookupDataset := dmTablas.cdsNivel12;
     HelpContext := H70072_Areas;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;
{$endif}
{ TTOIncidencias_DevEx }

procedure TTOIncidencias_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     //Desactivar gridmode (FormCreate)
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= FALSE;

     LookupDataset := dmTablas.cdsIncidencias;
     HelpContext := H70743_Incidencias;

     //columna sumatoria: cuantos
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

{ TTOMotivoBaja_DevEx }

procedure TTOMotivoBaja_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     //Desactivar gridmode (FormCreate)
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= FALSE;

     LookupDataset := dmTablas.cdsMotivoBaja;
     HelpContext := H70742_Motivo_baja;

     //columna sumatoria: cuantos
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

{ TAhorroPrestamo }

{$ifndef CAJAAHORRO}
procedure TAhorroPrestamo.AfterCreate;
const
     K_PANEL_HEIGHT = 30;
     K_BTN_TOP = 2;
     K_BTN_LEFT = 2;
     K_BTN_WIDTH = 100;
var
   Panel: TPanel;
   Boton: TcxButton;
begin
     Panel := TPanel.Create( Self );
     with Panel do
     begin
          Parent := Self;
          Align := alTop;
          Height := K_PANEL_HEIGHT;
          BevelInner := bvNone;
          BevelOuter := bvNone;
     end;
     Boton := tcxButton.Create( Self );
     with Boton do
     begin
          Parent := Panel;
          Height := Panel.Height - ( 2 * K_BTN_TOP );
          Width := K_BTN_WIDTH;
          Left := K_BTN_LEFT;
          Top := K_BTN_TOP;
          Caption := 'Fijar Prioridad';
          Anchors := [ akTop, akLeft ];
          OnClick := FijarPrioridad;
          ShowHint := True;
          Hint := 'Fijar Prioridades de Ahorros y Préstamos';
          Glyph.LoadFromResourceName( SysInit.HInstance, 'PRIORIDAD' );
          optionsimage.Margin := 1;
          Glyph.TransparentColor := clBlack;
     end;
     inherited AfterCreate;
end;

procedure TAhorroPrestamo.FijarPrioridad(Sender: TObject);
begin
     ZBaseDlgModal_DevEx.ShowDlgModal( EditPrioAhorrPresta_DevEx, TEditPrioAhorrPresta_DevEx );
end;
{$endif}

{ TTOTAhorro }

procedure TTOTAhorro_DevEx.AfterCreate;
begin
     inherited AfterCreate;
      CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0], K_SIN_TIPO , '', skCount);
     LookupDataset := dmTablas.cdsTAhorro;
     HelpContext := H70751_Tipo_ahorro;
end;

{ TTOTPresta }

procedure TTOTPresta_DevEx.AfterCreate;
begin
     inherited AfterCreate;
      CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0], K_SIN_TIPO , '', skCount);
     LookupDataset := dmTablas.cdsTPresta;
     {$ifdef CAJAAHORRO}
     HelpContext:= H_CAT_TIPO_PRESTAMO;
     {$else}
     HelpContext:= H70752_Tipo_prestamo;
     {$endif}
end;

{ TTOMotAuto_DevEx }

procedure TTOMotAuto_DevEx.AfterCreate;
begin
     inherited AfterCreate;

     //Desactivar gridmode (FormCreate)
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= FALSE;

     LookupDataset := dmTablas.cdsMotAuto;
     HelpContext := H70745_Motivos_de_Autorizacion;

     //columna sumatoria: cuantos
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

{ TTOMotCheca_DevEx }

procedure TTOMotCheca_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     //Desactivar gridmode (FormCreate)
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= FALSE;

     LookupDataset := dmTablas.cdsMotCheca;
     HelpContext := H_Motivos_Checada_Manual;

     //columna sumatoria: cuantos
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

{ TTOTallas_DevEx }

procedure TTOTallas_DevEx.AfterCreate;       //DevEx
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsTallas;
     HelpContext := H60664_Tallas;

     //columna sumatoria: cuantos
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();

end;

{ TTOMotTool_DevEx }

procedure TTOMotTool_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsMotTool;
     HelpContext := H60665_Motivos_Devolucion;

     //columna sumatoria: cuantos
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();

end;

{ TTOClasifiCurso_DevEx }

procedure TTOClasifiCurso_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsClasifiCurso;
     HelpContext := H60646_Clase_de_cursos;

     //columna sumatoria: cuantos
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

{ TTOColonia_DevEx }
procedure TTOColonia_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsColonia;
     HelpContext := H_Tabla_Colonias;

     //columna sumatoria: cuantos
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();

end;

{ TTOOcupaNac_DevEx }
procedure TTOOcupaNac_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsOcupaNac;
     HelpContext := H_TABLA_OCUPACIONES_NACIONALES;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

{ TTOAreaTematicaCursos_DevEx }
procedure TTOAreaTematicaCursos_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsAreaTemCur;
     HelpContext := H_TABLA_AREA_TEMATICA_CURSOS;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

{ TTOMotivoTransfer_DevEx }
procedure TTOMotivoTransfer_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsMotivoTransfer;
     HelpContext := H_Tabla_Motivo_Transferencia;

     //columna sumatoria: cuantos
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;


{ TTOTPerfiles_DevEx }

procedure TTOTPerfiles_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsTPerfiles;
     HelpContext := H_Tabla_TiposPerfiles;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

{ TTOTCompetencias_DevEx }

procedure TTOTCompetencias_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsTCompetencias;
     HelpContext := H_Tabla_TiposCompetencias;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

end.
