unit FTabOfiSatTipoJornada_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db,
  ExtCtrls, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, System.Actions;

type
  TTabOfiSatTipoJornada_DevEx = class(TBaseGridLectura_DevEx)
    TB_SAT_BAN: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
  public
    procedure DoLookup; override;
  end;

var
  TabOfiSatTipoJornada_DevEx: TTabOfiSatTipoJornada_DevEx;

implementation

uses dTablas, ZetaCommonClasses, ZetaBuscador_DevEx;

{$R *.DFM}

procedure TTabOfiSatTipoJornada_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H70076_JornadaSat;

     CanLookup := True;
end;

procedure TTabOfiSatTipoJornada_DevEx.Connect;
begin
     with dmTablas do
     begin
          cdsTipoJornadaSat.Conectar;
          DataSource.DataSet:= cdsTipoJornadaSat;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();

end;

procedure TTabOfiSatTipoJornada_DevEx.Refresh;
begin
     dmTablas.cdsTipoJornadaSat.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TTabOfiSatTipoJornada_DevEx.Agregar;
begin
     dmTablas.cdsTipoJornadaSat.Agregar;
end;

procedure TTabOfiSatTipoJornada_DevEx.Borrar;
begin
     dmTablas.cdsTipoJornadaSat.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TTabOfiSatTipoJornada_DevEx.Modificar;
begin
     dmTablas.cdsTipoJornadaSat.Modificar;
end;

procedure TTabOfiSatTipoJornada_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', dmTablas.cdsTipoJornadaSat );
end;

procedure TTabOfiSatTipoJornada_DevEx.FormShow(Sender: TObject);
begin
CreacolumaSumatoria(ZetaDbGridDbTableView.Columns[0],0,'',skCount);
  inherited;

end;

end.
