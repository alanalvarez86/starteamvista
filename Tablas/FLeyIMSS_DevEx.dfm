inherited SistLeyIMSS_DevEX: TSistLeyIMSS_DevEX
  Top = 120
  Caption = 'Cuotas de IMSS'
  PixelsPerInch = 96
  TextHeight = 13
  inherited ZetaDBGrid: TZetaCXGrid
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsData.DeletingConfirmation = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object SS_INICIAL: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'SS_INICIAL'
        Width = 75
      end
      object SS_L_A1061: TcxGridDBColumn
        Caption = 'Tope EyM'
        DataBinding.FieldName = 'SS_L_A1061'
      end
      object SS_L_IV: TcxGridDBColumn
        Caption = 'Tope IV'
        DataBinding.FieldName = 'SS_L_IV'
      end
      object SS_P_A1061: TcxGridDBColumn
        Caption = 'Cuota Fija'
        DataBinding.FieldName = 'SS_P_A1061'
      end
      object SS_P_A1062: TcxGridDBColumn
        Caption = 'Excedente Pat'
        DataBinding.FieldName = 'SS_P_A1062'
        Width = 76
      end
      object SS_P_A107: TcxGridDBColumn
        Caption = 'Dinero Pat'
        DataBinding.FieldName = 'SS_P_A107'
      end
      object SS_P_A25: TcxGridDBColumn
        Caption = 'Gastos Pat'
        DataBinding.FieldName = 'SS_P_A25'
      end
      object SS_P_SAR: TcxGridDBColumn
        Caption = 'Retiro'
        DataBinding.FieldName = 'SS_P_SAR'
        Width = 55
      end
      object SS_P_GUARD: TcxGridDBColumn
        Caption = 'Guarder'#237'as'
        DataBinding.FieldName = 'SS_P_GUARD'
      end
      object SS_P_IV: TcxGridDBColumn
        Caption = 'IV Pat'
        DataBinding.FieldName = 'SS_P_IV'
      end
      object SS_P_CV: TcxGridDBColumn
        Caption = 'CV Pat'
        DataBinding.FieldName = 'SS_P_CV'
      end
      object SS_P_INFO: TcxGridDBColumn
        Caption = 'INFONAVIT'
        DataBinding.FieldName = 'SS_P_INFO'
      end
      object SS_O_A1062: TcxGridDBColumn
        Caption = 'Excedente Emp'
        DataBinding.FieldName = 'SS_O_A1062'
        Width = 81
      end
      object SS_O_A107: TcxGridDBColumn
        Caption = 'Dinero Emp'
        DataBinding.FieldName = 'SS_O_A107'
      end
      object SS_O_A25: TcxGridDBColumn
        Caption = 'Gastos Emp'
        DataBinding.FieldName = 'SS_O_A25'
      end
      object SS_O_IV: TcxGridDBColumn
        Caption = 'IV Emp'
        DataBinding.FieldName = 'SS_O_IV'
      end
      object SS_O_CV: TcxGridDBColumn
        Caption = 'CV Emp'
        DataBinding.FieldName = 'SS_O_CV'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
