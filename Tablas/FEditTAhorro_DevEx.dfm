inherited EditTAhorro_DevEx: TEditTAhorro_DevEx
  Left = 472
  Top = 207
  Caption = 'Tipo de Ahorro'
  ClientHeight = 442
  ClientWidth = 417
  PixelsPerInch = 96
  TextHeight = 13
  object DBInglesLBL: TLabel [0]
    Left = 86
    Top = 83
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ingl'#233's:'
  end
  object DBDescripcionLBL: TLabel [1]
    Left = 58
    Top = 61
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object DBCodigoLBL: TLabel [2]
    Left = 81
    Top = 41
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
  end
  object Label1: TLabel [3]
    Left = 87
    Top = 127
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto:'
  end
  object Label2: TLabel [4]
    Left = 77
    Top = 105
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  inherited PanelBotones: TPanel
    Top = 406
    Width = 417
    TabOrder = 8
    inherited OK_DevEx: TcxButton
      Left = 252
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 332
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 417
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 91
      inherited textoValorActivo2: TLabel
        Width = 85
      end
    end
  end
  object TB_CODIGO: TZetaDBEdit [7]
    Left = 121
    Top = 37
    Width = 65
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 1
    ConfirmEdit = True
    DataField = 'TB_CODIGO'
    DataSource = DataSource
  end
  object TB_ELEMENT: TDBEdit [8]
    Left = 121
    Top = 59
    Width = 280
    Height = 21
    DataField = 'TB_ELEMENT'
    DataSource = DataSource
    TabOrder = 2
  end
  object TB_INGLES: TDBEdit [9]
    Left = 121
    Top = 81
    Width = 280
    Height = 21
    DataField = 'TB_INGLES'
    DataSource = DataSource
    TabOrder = 3
  end
  object TB_TEXTO: TDBEdit [10]
    Left = 121
    Top = 125
    Width = 280
    Height = 21
    DataField = 'TB_TEXTO'
    DataSource = DataSource
    TabOrder = 12
  end
  object TB_NUMERO: TZetaDBNumero [11]
    Left = 121
    Top = 103
    Width = 100
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 4
    Text = '0.00'
    DataField = 'TB_NUMERO'
    DataSource = DataSource
  end
  object TB_ACTIVO: TDBCheckBox [12]
    Left = 350
    Top = 37
    Width = 51
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Activo:'
    DataField = 'TB_ACTIVO'
    DataSource = DataSource
    TabOrder = 9
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 5
  end
  object CxPageControl: TcxPageControl [14]
    Left = 0
    Top = 153
    Width = 417
    Height = 253
    Align = alBottom
    TabOrder = 13
    Properties.ActivePage = TabGenerales
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 251
    ClientRectLeft = 2
    ClientRectRight = 415
    ClientRectTop = 28
    object TabGenerales: TcxTabSheet
      Caption = 'Generales'
      ImageIndex = 0
      object Label3: TLabel
        Left = 9
        Top = 49
        Width = 104
        Height = 13
        Alignment = taRightJustify
        Caption = 'Concepto Deducci'#243'n:'
      end
      object Label4: TLabel
        Left = 20
        Top = 77
        Width = 91
        Height = 13
        Alignment = taRightJustify
        Caption = 'Concepto Relativo:'
      end
      object Label6: TLabel
        Left = 74
        Top = 105
        Width = 37
        Height = 13
        Caption = 'En Alta:'
      end
      object Label5: TLabel
        Left = 39
        Top = 129
        Width = 73
        Height = 13
        Alignment = taRightJustify
        Caption = 'En Liquidaci'#243'n:'
      end
      object Label7: TLabel
        Left = 66
        Top = 153
        Width = 47
        Height = 13
        Alignment = taRightJustify
        Caption = 'Pr'#233'stamo:'
      end
      object TB_CONCEPT: TZetaDBKeyLookup_DevEx
        Left = 117
        Top = 47
        Width = 283
        Height = 21
        Filtro = 'CO_NUMERO < 1000'
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 30
        DataField = 'TB_CONCEPT'
        DataSource = DataSource
      end
      object TB_RELATIV: TZetaDBKeyLookup_DevEx
        Left = 117
        Top = 73
        Width = 283
        Height = 21
        Filtro = 'CO_NUMERO < 1000'
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 30
        DataField = 'TB_RELATIV'
        DataSource = DataSource
      end
      object TB_ALTA: TZetaDBKeyCombo
        Left = 117
        Top = 99
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 2
        ListaFija = lfAltaAhorroPrestamo
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'TB_ALTA'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object TB_LIQUIDA: TZetaDBKeyCombo
        Left = 117
        Top = 125
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 3
        ListaFija = lfTipoAhorro
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'TB_LIQUIDA'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object TB_PRESTA: TZetaDBKeyLookup_DevEx
        Left = 117
        Top = 151
        Width = 283
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 30
        DataField = 'TB_PRESTA'
        DataSource = DataSource
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Par'#225'metros'
      ImageIndex = 1
      object gbValidarRango: TGroupBox
        Left = 16
        Top = 18
        Width = 385
        Height = 83
        Caption = '      Validar Rango en que se Permite Agregar un Pr'#233'stamo  '
        TabOrder = 1
        object lblInicio: TLabel
          Left = 73
          Top = 29
          Width = 28
          Height = 13
          Alignment = taRightJustify
          Caption = 'Inicia:'
        end
        object lblFin: TLabel
          Left = 60
          Top = 53
          Width = 41
          Height = 13
          Alignment = taRightJustify
          Caption = 'Termina:'
        end
        object cbDiaIni: TComboBox
          Left = 104
          Top = 25
          Width = 49
          Height = 21
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ItemIndex = 0
          ParentCtl3D = False
          TabOrder = 0
          Text = '1'
          OnChange = AlCambiarCombo
          Items.Strings = (
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9'
            '10'
            '11'
            '12'
            '13'
            '14'
            '15'
            '16'
            '17'
            '18'
            '19'
            '20'
            '21'
            '22'
            '23'
            '24'
            '25'
            '26'
            '27'
            '28'
            '29'
            '30'
            '31')
        end
        object cbMesIni: TComboBox
          Left = 160
          Top = 25
          Width = 89
          Height = 21
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ItemIndex = 0
          ParentCtl3D = False
          TabOrder = 1
          Text = 'Enero'
          OnChange = AlCambiarCombo
          Items.Strings = (
            'Enero'
            'Febrero'
            'Marzo'
            'Abril'
            'Mayo'
            'Junio'
            'Julio'
            'Agosto'
            'Septiembre'
            'Octubre'
            'Noviembre'
            'Diciembre')
        end
        object cbDiaFin: TComboBox
          Left = 104
          Top = 49
          Width = 49
          Height = 21
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ItemIndex = 0
          ParentCtl3D = False
          TabOrder = 2
          Text = '1'
          OnChange = AlCambiarCombo
          Items.Strings = (
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9'
            '10'
            '11'
            '12'
            '13'
            '14'
            '15'
            '16'
            '17'
            '18'
            '19'
            '20'
            '21'
            '22'
            '23'
            '24'
            '25'
            '26'
            '27'
            '28'
            '29'
            '30'
            '31')
        end
        object cbMesFin: TComboBox
          Left = 160
          Top = 49
          Width = 89
          Height = 21
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ItemIndex = 0
          ParentCtl3D = False
          TabOrder = 3
          Text = 'Enero'
          OnChange = AlCambiarCombo
          Items.Strings = (
            'Enero'
            'Febrero'
            'Marzo'
            'Abril'
            'Mayo'
            'Junio'
            'Julio'
            'Agosto'
            'Septiembre'
            'Octubre'
            'Noviembre'
            'Diciembre')
        end
      end
      object GBTasas: TGroupBox
        Left = 16
        Top = 111
        Width = 385
        Height = 97
        Caption = ' Tasas de Inter'#233's'
        TabOrder = 2
        object Label8: TLabel
          Left = 58
          Top = 22
          Width = 43
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tasa #1:'
        end
        object Label9: TLabel
          Left = 58
          Top = 44
          Width = 43
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tasa #2:'
        end
        object Label10: TLabel
          Left = 58
          Top = 66
          Width = 43
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tasa #3:'
        end
        object TB_TASA1: TZetaDBNumero
          Left = 105
          Top = 20
          Width = 100
          Height = 21
          Mascara = mnTasa
          TabOrder = 0
          Text = '0.0 %'
          DataField = 'TB_TASA1'
          DataSource = DataSource
        end
        object TB_TASA2: TZetaDBNumero
          Left = 105
          Top = 42
          Width = 100
          Height = 21
          Mascara = mnTasa
          TabOrder = 1
          Text = '0.0 %'
          DataField = 'TB_TASA2'
          DataSource = DataSource
        end
        object TB_TASA3: TZetaDBNumero
          Left = 105
          Top = 64
          Width = 100
          Height = 21
          Mascara = mnTasa
          TabOrder = 2
          Text = '0.0 %'
          DataField = 'TB_TASA3'
          DataSource = DataSource
        end
      end
      object TB_VAL_RAN: TDBCheckBox
        Left = 26
        Top = 16
        Width = 15
        Height = 17
        DataField = 'TB_VAL_RAN'
        DataSource = DataSource
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = TB_VAL_RANClick
      end
    end
    object tsConfidencialidad: TcxTabSheet
      Caption = 'Confidencialidad'
      ImageIndex = 2
      object gbConfidencialidad: TGroupBox
        Left = 4
        Top = 8
        Width = 403
        Height = 207
        TabOrder = 0
        object listaConfidencialidad: TListBox
          Left = 74
          Top = 56
          Width = 259
          Height = 137
          TabStop = False
          ExtendedSelect = False
          ItemHeight = 13
          TabOrder = 0
        end
        object rbConfidenAlgunas: TRadioButton
          Left = 74
          Top = 30
          Width = 156
          Height = 17
          Caption = 'Aplica algunas'
          TabOrder = 1
          OnClick = rbConfidenAlgunasClick
        end
        object rbConfidenTodas: TRadioButton
          Left = 74
          Top = 12
          Width = 113
          Height = 17
          Caption = 'Todas'
          TabOrder = 2
          OnClick = rbConfidenTodasClick
        end
        object btSeleccionarConfiden_DevEx: TcxButton
          Left = 336
          Top = 56
          Width = 25
          Height = 25
          Hint = 'Seleccionar Confidencialidad'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = btSeleccionarConfidenClick
          LookAndFeel.SkinName = 'TressMorado2013'
          OptionsImage.Glyph.Data = {
            7A080000424D7A08000000000000360000002800000017000000170000000100
            200000000000440800000000000000000000000000000000000000B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF000000000000000000B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000
            000000000000000000000000000000B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF000000000000
            0000000000000000000000B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF000000000000000000B2
            E9FF00B2E9FF00B2E9FF458A9FA800B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF0000000000000000356F818800B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00000000000000000000
            0000000000000000000000B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF000000000000000000000000000000000000
            0000000000000000000000B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF000000000000000000000000000000000000
            00000000000000B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF80D9F4FF000000000000000000000000000000000000
            00000000000000B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00000000000000000000000000000000000000000000
            000000B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00000000000000000000000000B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF0000000000B2E9FF00B2E9FF00B2E9FF00B2E9FF000000000000
            000000B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF0000000000000000000000000000000000B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00000000000000000000000071C4DEE800B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF000000000000000000B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
          SpeedButtonOptions.CanBeFocused = False
          SpeedButtonOptions.Transparent = True
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 260
    Top = 18
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 264
    Top = 66
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Visible = ivAlways
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 4849912
  end
end
