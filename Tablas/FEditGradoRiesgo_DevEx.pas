unit FEditGradoRiesgo_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, Db,
  ExtCtrls, DBCtrls, ZetaNumero,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditGradoRiesgo_DevEx = class(TBaseEdicion_DevEx)
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label1: TLabel;
    RI_CLASE: TZetaDBNumero;
    RI_GRADO: TZetaDBNumero;
    RI_INDICE: TZetaDBNumero;
    RI_PRIMA: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure Connect;override;
  public
    { Public declarations }
  end;

var
  EditGradoRiesgo_DevEx: TEditGradoRiesgo_DevEx;

implementation

{$R *.DFM}

uses ZetaCommonClasses, ZAccesosTress, DTablas;

procedure TEditGradoRiesgo_DevEx.Connect;
begin
     DataSource.Dataset := dmTablas.cdsGradosRiesgo;
end;

procedure TEditGradoRiesgo_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H70764_Grados_riesgo;
     FirstControl := RI_CLASE;
     IndexDerechos := ZAccesosTress.D_TAB_OFI_GRADOS_RIESGO;
end;

end.
