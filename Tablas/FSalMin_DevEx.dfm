inherited SistSalMin_DevEx: TSistSalMin_DevEx
  Left = 288
  Top = 123
  Caption = 'Salarios M'#237'nimos'
  ClientWidth = 277
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 277
    inherited ImgNoRecord: TImage
      Left = 3
    end
    inherited Slider: TSplitter
      Left = 0
    end
    inherited ValorActivo1: TPanel
      Left = 19
    end
    inherited ValorActivo2: TPanel
      Width = 18
      inherited textoValorActivo2: TLabel
        Width = 12
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 277
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object SM_FEC_INI: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'SM_FEC_INI'
      end
      object SM_ZONA_A: TcxGridDBColumn
        Caption = 'Zona '#39'A'#39
        DataBinding.FieldName = 'SM_ZONA_A'
      end
      object SM_ZONA_B: TcxGridDBColumn
        Caption = 'Zona '#39'B'#39
        DataBinding.FieldName = 'SM_ZONA_B'
      end
      object SM_ZONA_C: TcxGridDBColumn
        Caption = 'Zona '#39'C'#39
        DataBinding.FieldName = 'SM_ZONA_C'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
