unit FEditEntidad_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaNumero, Mask,
  ZetaEdit, ZetaSmartLists, ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, cxContainer, cxEdit, cxGroupBox, cxListBox, cxRadioGroup;

type
  TTOEditEntidad_DevEx = class(TBaseEdicion_DevEx)
    DBCodigoLBL: TLabel;
    TB_CODIGO: TZetaDBEdit;
    DBDescripcionLBL: TLabel;
    TB_ELEMENT: TDBEdit;
    DBInglesLBL: TLabel;
    TB_INGLES: TDBEdit;
    Label2: TLabel;
    TB_NUMERO: TZetaDBNumero;
    Label1: TLabel;
    TB_TEXTO: TDBEdit;
    lblClave: TLabel;
    TB_CURP: TDBEdit;
    Label3: TLabel;
    TB_STPS: TDBEdit;
    gbConfidencialidad: TcxGroupBox;
    btSeleccionarConfiden: TcxButton;
    listaConfidencialidad: TcxListBox;
    rbConfidenTodas: TcxRadioButton;
    rbConfidenAlgunas: TcxRadioButton;
    procedure FormCreate(Sender: TObject);  
    procedure btSeleccionarConfidenClick(Sender: TObject);
    procedure rbConfidenTodasClick(Sender: TObject);
    procedure rbConfidenAlgunasClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }  
    lValoresConfidencialidad : TStringList;
    procedure FillListaConfidencialidad;
    procedure SetListConfidencialidad( sValores : string );
    procedure GetConfidencialidad;
  protected
    procedure Connect;override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  TOEditEntidad_DevEx: TTOEditEntidad_DevEx;

implementation

uses dTablas,
     ZetaBuscador_DevEx,
     ZetaCommonClasses,
     ZAccesosTress,
     FSeleccionarConfidencialidad_DevEx, dSistema, DCliente, ZetaCommonTools;

{$R *.DFM}

procedure TTOEditEntidad_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := TB_CODIGO;
     IndexDerechos := ZAccesosTress.D_TAB_PER_ESTADOS;
     HelpContext:= H_Estados_pais_Edicion;  
     lValoresConfidencialidad := TStringList.Create;
end;

procedure TTOEditEntidad_DevEx.Connect;
begin
     dmSistema.cdsNivel0.Conectar;
     with dmTablas do
     begin
          DataSource.DataSet := cdsEstado;
          Self.Caption := cdsEstado.LookupName;
     end;
end;

procedure TTOEditEntidad_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  FillListaConfidencialidad;
end;

procedure TTOEditEntidad_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Self.Caption, 'TB_CODIGO', dmTablas.cdsEstado);
end;

procedure TTOEditEntidad_DevEx.btSeleccionarConfidenClick(Sender: TObject);
var
   selConfi : TSeleccionarConfidencialidad_DevEx;
   sCM_NIVEL0Nuevo : string;
begin
  inherited;

  selConfi := TSeleccionarConfidencialidad_DevEx.Create( Self );
  sCM_NIVEL0Nuevo := selConfi.GetConfidencialidad( DataSource.DataSet.FieldByName('TB_NIVEL0').AsString, lValoresConfidencialidad, True, dmCliente.Confidencialidad );

  if (  sCM_NIVEL0Nuevo <>  DataSource.DataSet.FieldByName('TB_NIVEL0').AsString ) then
  begin
     with DataSource.DataSet do
     begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

           FieldByName('TB_NIVEL0').AsString  := sCM_NIVEL0Nuevo;
           GetConfidencialidad;
     end;
  end;

  FreeAndNil ( selConfi ) ;
end;

procedure TTOEditEntidad_DevEx.FillListaConfidencialidad;
begin
     with dmSistema.cdsNivel0 do
     begin
          First;
          lValoresConfidencialidad.Clear;
          lValoresConfidencialidad.NameValueSeparator := '=' ;
          While( not Eof )do
          begin
               lValoresConfidencialidad.Add(FieldByName('TB_CODIGO').AsString +'='+ FieldByName('TB_ELEMENT').AsString);
               Next;
          end;
     end;
end;

procedure TTOEditEntidad_DevEx.GetConfidencialidad;
begin
   if (  DataSource.DataSet  <> nil ) then
   with DataSource.DataSet do
   begin
        SetListConfidencialidad(  FieldByName('TB_NIVEL0').AsString );
   end;
end;

procedure TTOEditEntidad_DevEx.SetListConfidencialidad(sValores: string);
var
   Lista : TStringList;
   i, j : integer;
begin
    Lista := TStringList.Create;

    sValores := Trim( sValores );
    if StrLleno( sValores ) then
        Lista.CommaText  := sValores;

    listaConfidencialidad.Items.Clear;

    rbConfidenTodas.OnClick := nil;
    rbConfidenAlgunas.OnClick := nil;
    rbConfidenTodas.Checked := False;
    rbConfidenAlgunas.Checked := False;
                                                 
    if lValoresConfidencialidad.Count = 0 then
       FillListaConfidencialidad;

    for i:= 0 to lValoresConfidencialidad.Count - 1 do
    begin
        if ( Lista.Count > 0 ) then
        begin
                j := Lista.IndexOf( Trim( lValoresConfidencialidad.Names[i] )  );
                if ( j >= 0 ) then
                   listaConfidencialidad.Items.Add( lValoresConfidencialidad[i] )
        end
    end;

    rbConfidenTodas.Checked :=   listaConfidencialidad.Items.Count = 0;
    rbConfidenAlgunas.Checked := not rbConfidenTodas.Checked;

    if ( gbConfidencialidad.Enabled ) then
    begin
        listaConfidencialidad.Enabled := rbConfidenAlgunas.Checked;
        btSeleccionarConfiden.Enabled := rbConfidenAlgunas.Checked;
    end;

    rbConfidenTodas.OnClick := rbConfidenTodasClick;
    rbConfidenAlgunas.OnClick := rbConfidenAlgunasClick;

    FreeAndNil( Lista );

end;

procedure TTOEditEntidad_DevEx.rbConfidenTodasClick(Sender: TObject);
begin
  inherited;

   with DataSource.DataSet do
   begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

        FieldByName('TB_NIVEL0').AsString  :=  '';
   end;
   GetConfidencialidad;
end;

procedure TTOEditEntidad_DevEx.rbConfidenAlgunasClick(Sender: TObject);
begin
  inherited;
     if ( rbConfidenAlgunas.Checked ) then
     begin
        btSeleccionarConfidenClick( Sender );
        GetConfidencialidad;
     end;

end;

procedure TTOEditEntidad_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
     if ( Field = nil ) then
     begin
          with DataSource.DataSet do
          begin
               if ( State in [dsBrowse] ) then
                  GetConfidencialidad
               else if (State in [dsInsert]) then
                    rbConfidenTodas.Checked := TRUE;
          end;

     end;
end;

end.
