unit FEditLeyIMSS_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, Buttons, DBCtrls, StdCtrls, Mask, ComCtrls,
     ZetaNumero, ZetaFecha, ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditLeyIMSS_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Bevel3: TBevel;
    SS_MAXIMO: TZetaDBNumero;
    SS_L_A1061: TZetaDBNumero;
    SS_L_A1062: TZetaDBNumero;
    SS_L_A107: TZetaDBNumero;
    SS_L_A25: TZetaDBNumero;
    SS_L_SAR: TZetaDBNumero;
    SS_L_GUARD: TZetaDBNumero;
    SS_L_RT: TZetaDBNumero;
    SS_L_IV: TZetaDBNumero;
    SS_L_CV: TZetaDBNumero;
    SS_L_INFO: TZetaDBNumero;
    SS_P_A1061: TZetaDBNumero;
    SS_P_A1062: TZetaDBNumero;
    SS_P_A107: TZetaDBNumero;
    SS_P_A25: TZetaDBNumero;
    SS_P_SAR: TZetaDBNumero;
    SS_P_GUARD: TZetaDBNumero;
    SS_P_IV: TZetaDBNumero;
    SS_P_CV: TZetaDBNumero;
    SS_P_INFO: TZetaDBNumero;
    SS_O_A1062: TZetaDBNumero;
    SS_O_A107: TZetaDBNumero;
    SS_O_A25: TZetaDBNumero;
    SS_O_IV: TZetaDBNumero;
    SS_O_CV: TZetaDBNumero;
    Label12: TLabel;
    SS_INICIAL: TZetaDBFecha;
    Panel2: TPanel;
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Shape1: TShape;
    Label13: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect;override;
  public
    { Public declarations }
  end;

var
  EditLeyIMSS_DevEx: TEditLeyIMSS_DevEx;

implementation

uses ZetaCommonClasses,
     ZAccesosTress,
     DTablas;

{$R *.DFM}

procedure TEditLeyIMSS_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H70762_Cuotas_IMSS;
     FirstControl := SS_INICIAL;
     IndexDerechos := ZAccesosTress.D_TAB_OFI_CUOTAS_IMSS;
end;

procedure TEditLeyIMSS_DevEx.Connect;
begin
     DataSource.DataSet := dmTablas.cdsLeyImss;
end;

end.
