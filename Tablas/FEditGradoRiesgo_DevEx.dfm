inherited EditGradoRiesgo_DevEx: TEditGradoRiesgo_DevEx
  Left = 95
  Top = 225
  Caption = 'Grados de Riesgo'
  ClientHeight = 170
  ClientWidth = 387
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel [0]
    Left = 139
    Top = 65
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grado:'
  end
  object Label5: TLabel [1]
    Left = 139
    Top = 86
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Indice:'
  end
  object Label1: TLabel [2]
    Left = 142
    Top = 107
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Prima:'
  end
  object Label3: TLabel [3]
    Left = 142
    Top = 44
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Clase:'
  end
  inherited PanelIdentifica: TPanel [4]
    Width = 387
    inherited ValorActivo2: TPanel
      Width = 61
      inherited textoValorActivo2: TLabel
        Width = 55
      end
    end
  end
  inherited PanelBotones: TPanel [5]
    Top = 134
    Width = 387
    inherited OK_DevEx: TcxButton
      Left = 223
      ParentBiDiMode = True
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 302
      Cancel = True
      ParentBiDiMode = True
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Left = 0
    Top = 8
  end
  object RI_CLASE: TZetaDBNumero [7]
    Left = 178
    Top = 40
    Width = 70
    Height = 21
    Mascara = mnDias
    TabOrder = 2
    Text = '0'
    DataField = 'RI_CLASE'
    DataSource = DataSource
  end
  object RI_GRADO: TZetaDBNumero [8]
    Left = 178
    Top = 61
    Width = 70
    Height = 21
    Mascara = mnDias
    TabOrder = 3
    Text = '0'
    DataField = 'RI_GRADO'
    DataSource = DataSource
  end
  object RI_INDICE: TZetaDBNumero [9]
    Left = 178
    Top = 82
    Width = 70
    Height = 21
    Mascara = mnEmpleado
    TabOrder = 4
    DataField = 'RI_INDICE'
    DataSource = DataSource
  end
  object RI_PRIMA: TZetaDBNumero [10]
    Left = 178
    Top = 103
    Width = 70
    Height = 21
    Mascara = mnTasa
    TabOrder = 5
    Text = '0.0 %'
    DataField = 'RI_PRIMA'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 4
    Top = 134
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
