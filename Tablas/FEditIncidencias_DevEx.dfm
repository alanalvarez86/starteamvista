inherited EditIncidencias_DevEx: TEditIncidencias_DevEx
  Left = 231
  Top = 184
  Caption = 'Incidencias'
  ClientHeight = 256
  ClientWidth = 389
  PixelsPerInch = 96
  TextHeight = 13
  object DBInglesLBL: TLabel [0]
    Left = 70
    Top = 83
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ingl'#233's:'
  end
  object DBDescripcionLBL: TLabel [1]
    Left = 42
    Top = 61
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object DBCodigoLBL: TLabel [2]
    Left = 65
    Top = 39
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
  end
  object Label1: TLabel [3]
    Left = 71
    Top = 126
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto:'
  end
  object Label2: TLabel [4]
    Left = 61
    Top = 104
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  object Label3: TLabel [5]
    Left = 25
    Top = 147
    Width = 76
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Incidencia:'
  end
  object TB_INCAPALb: TLabel [6]
    Left = 15
    Top = 169
    Width = 86
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Incapacidad:'
  end
  object lblClasePermiso: TLabel [7]
    Left = 17
    Top = 191
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = 'Clase de Permiso:'
  end
  inherited PanelBotones: TPanel
    Top = 220
    Width = 389
    TabOrder = 10
    inherited OK_DevEx: TcxButton
      Left = 225
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 304
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 389
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 63
      inherited textoValorActivo2: TLabel
        Width = 57
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 15
  end
  object TB_INCIDEN: TZetaDBKeyCombo [11]
    Left = 104
    Top = 143
    Width = 149
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 6
    OnChange = TB_INCIDENChange
    OnExit = TB_INCIDENExit
    ListaFija = lfIncidencias
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'TB_INCIDEN'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object TB_INCAPA: TZetaDBKeyCombo [12]
    Left = 104
    Top = 165
    Width = 149
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 7
    ListaFija = lfIncidenciaIncapacidad
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'TB_INCAPA'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object TB_CODIGO: TZetaDBEdit [13]
    Left = 104
    Top = 35
    Width = 65
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 1
    ConfirmEdit = True
    DataField = 'TB_CODIGO'
    DataSource = DataSource
  end
  object TB_ELEMENT: TDBEdit [14]
    Left = 104
    Top = 57
    Width = 280
    Height = 21
    DataField = 'TB_ELEMENT'
    DataSource = DataSource
    TabOrder = 2
  end
  object TB_INGLES: TDBEdit [15]
    Left = 104
    Top = 78
    Width = 280
    Height = 21
    DataField = 'TB_INGLES'
    DataSource = DataSource
    TabOrder = 3
  end
  object TB_TEXTO: TDBEdit [16]
    Left = 104
    Top = 121
    Width = 280
    Height = 21
    DataField = 'TB_TEXTO'
    DataSource = DataSource
    TabOrder = 5
  end
  object TB_SISTEMA: TDBCheckBox [17]
    Left = 289
    Top = 167
    Width = 59
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Sistema:'
    DataField = 'TB_SISTEMA'
    DataSource = DataSource
    Enabled = False
    TabOrder = 8
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object TB_NUMERO: TZetaDBNumero [18]
    Left = 104
    Top = 100
    Width = 100
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 4
    Text = '0.00'
    DataField = 'TB_NUMERO'
    DataSource = DataSource
  end
  object TB_PERMISO: TZetaDBKeyCombo [19]
    Left = 104
    Top = 187
    Width = 149
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 11
    ListaFija = lfTipoPermiso
    ListaVariable = lvPuesto
    Offset = -1
    Opcional = False
    EsconderVacios = False
    DataField = 'TB_PERMISO'
    DataSource = DataSource
    LlaveNumerica = True
  end
  inherited DataSource: TDataSource
    Left = 356
    Top = 193
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Visible = ivAlways
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
