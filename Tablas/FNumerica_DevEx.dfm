inherited SistNumerica_DevEx: TSistNumerica_DevEx
  Left = 80
  Top = 119
  Caption = 'ISPT y Num'#233'ricas'
  ClientWidth = 577
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 577
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 251
      inherited textoValorActivo2: TLabel
        Width = 245
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 577
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsData.DeletingConfirmation = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object NU_CODIGO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'NU_CODIGO'
        HeaderAlignmentHorz = taRightJustify
      end
      object NU_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'NU_DESCRIP'
        Width = 249
      end
      object NU_INGLES: TcxGridDBColumn
        Caption = 'Ingl'#233's'
        DataBinding.FieldName = 'NU_INGLES'
        Width = 245
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
