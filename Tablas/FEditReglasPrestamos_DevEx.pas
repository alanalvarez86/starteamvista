unit FEditReglasPrestamos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls, Buttons, DBCtrls, StdCtrls, ComCtrls,
  ZetaNumero, Mask, ZetaTipoEntidad, ZetaKeyCombo,
  ZetaSmartLists, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, cxContainer,
  cxEdit, cxMemo, cxTextEdit, cxDBEdit, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxPC, ZetaKeyLookup_DevEx;

type
  TEditReglaPrestamo_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    PageControl: TcxPageControl;
    TabGenerales: TcxTabSheet;
    TabSheet3: TcxTabSheet;
    Label1: TLabel;
    Label3: TLabel;
    RP_ACTIVO: TDBCheckBox;
    RP_LETRERO: TDBEdit;
    RP_CODIGO: TZetaDBNumero;
    QU_CODIGO: TZetaDBKeyLookup_DevEx;
    Label8: TLabel;
    RP_FILTRO: TcxDBMemo;
    Label7: TLabel;
    PrestamosCondiciones: TGroupBox;
    RP_LIMITE: TZetaDBNumero;
    Label4: TLabel;
    Label5: TLabel;
    RP_VAL_FEC: TDBCheckBox;
    GroupBox2: TGroupBox;
    RP_TOPE: TcxDBMemo;
    rgStatusEmpleado: TRadioGroup;
    RP_ANT_INI: TZetaDBNumero;
    Label6: TLabel;
    Label9: TLabel;
    RP_ANT_FIN: TZetaDBNumero;
    Label10: TLabel;
    rgStatusPrestamos: TRadioGroup;
    PanelTiposPrestamos: TPanel;
    gpPrestamosAValidar: TGroupBox;
    rbTodosPrestamos: TRadioButton;
    rbAlgunosPrestamos: TRadioButton;
    memPrestamosLista: TcxMemo;
    btnFormula: TcxButton;
    FiltroBtn: TcxButton;
    btnAgregarTiposPresta: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure btnFormulaClick(Sender: TObject);
    procedure FiltroBtnClick(Sender: TObject);
    procedure rbTodosPrestamosClick(Sender: TObject);
    procedure rgStatusPrestamosClick(Sender: TObject);
    procedure btnAgregarTiposPrestaClick(Sender: TObject);
    procedure rbAlgunosPrestamosClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure rgStatusEmpleadoClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure RP_VAL_FECClick(Sender: TObject);
  private
    Conectando:Boolean;
    procedure LlenarTiposPrestamos;
    procedure SetControls;
    procedure ModoEditar;
  protected
    procedure Connect; override;
    procedure DoLookup; override;
    procedure EscribirCambios; override;
    procedure HabilitaControles; override;
  public
  end;

var
  EditReglaPrestamo_DevEx: TEditReglaPrestamo_DevEx;

implementation

uses dCatalogos,DTablas, dSistema, ZAccesosTress, ZConstruyeFormula, ZetaCommonLists,
     ZetaDialogo,
     ZetaCommonClasses,ZetaCommonTools, ZetaBuscaEntero_DevEx,ZBaseDlgModal_DevEx,FEditPrestaXRegla_DevEx;

{$R *.DFM}

procedure TEditReglaPrestamo_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_TAB_NOM_REGLAS_PRESTA;
     HelpContext:= H_FEditReglasPrestamo;
     FirstControl := RP_CODIGO;
     QU_CODIGO.LookupDataset := dmCatalogos.cdsCondiciones;
     Conectando := False;
end;

procedure TEditReglaPrestamo_DevEx.Connect;
begin
     memPrestamosLista.Text := VACIO;
     RP_VAL_FEC.OnClick := NIL;
     try

        dmSistema.cdsUsuarios.Conectar;
        with dmCatalogos do
        begin
             cdsCondiciones.Conectar;
             cdsPrestaXRegla.Conectar;
             Datasource.Dataset := cdsReglaPrestamo;
        end;
     finally
            RP_VAL_FEC.OnClick := RP_VAL_FECClick;
     end;
end;

procedure TEditReglaPrestamo_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     SetControls;
     Conectando := True;
     PageControl.ActivePageIndex := 0;
     {memPrestamosLista.Text := VACIO;
     {with dmCatalogos.cdsReglaPrestamo do
     begin
          if not ( Editing or Inserting ) then
             LlenarTiposPrestamos;
//          gpPrestamosAValidar.Visible := ( not Inserting );
     end;}
     rbTodosPrestamos.Checked := not zStrToBool( dmCatalogos.cdsReglaPrestamo.FieldByName('RP_LISTA').AsString );
     rbAlgunosPrestamos.Checked := zStrToBool( dmCatalogos.cdsReglaPrestamo.FieldByName('RP_LISTA').AsString );
     Conectando := False;
end;

procedure TEditReglaPrestamo_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscaEntero_DevEx.BuscarCodigo( 'N�mero', 'Regla de Validaci�n de Pr�stamos', 'RP_CODIGO', dmCatalogos.cdsReglaPrestamo );
end;

procedure TEditReglaPrestamo_DevEx.SetControls;
begin
     with dmCatalogos.cdsReglaPrestamo do
     begin
          rgStatusPrestamos.ItemIndex := FieldByName('RP_PRE_STS').AsInteger;
          rgStatusEmpleado.ItemIndex := FieldByName('RP_EMP_STS').AsInteger;
     end;
     dmCatalogos.cdsPrestaXRegla.Refrescar;

end;

procedure TEditReglaPrestamo_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with dmCatalogos.cdsReglaPrestamo do
     begin
          if not ( Editing or Inserting ) then
             LlenarTiposPrestamos;
     end;
     if ( ( Field = Nil  ) and (  not( dmCatalogos.cdsReglaPrestamo.State in [dsEdit,dsInsert] ) ) )then
     begin
          Conectando := True;
          SetControls;
          rbTodosPrestamos.Checked := not zStrToBool( dmCatalogos.cdsReglaPrestamo.FieldByName('RP_LISTA').AsString );
          rbAlgunosPrestamos.Checked := zStrToBool( dmCatalogos.cdsReglaPrestamo.FieldByName('RP_LISTA').AsString );
          Conectando := False;
     end;



     if ( dmCatalogos.cdsReglaPrestamo.State  = dsInsert ) then
     begin
          SetControls;
          rbTodosPrestamos.Checked := TRUE;
          LlenarTiposPrestamos;
     end;
     if ( Field = nil ) then
        HabilitaControles;
end;


procedure TEditReglaPrestamo_DevEx.HabilitaControles;
begin
     inherited;
     with gpPrestamosAValidar do
     begin
          Visible := ( not Inserting ) and ( not dmCatalogos.AgregaReglaPrestamo );
     end;

     RP_VAL_FEC.Visible := ( not Inserting ) and ( not dmCatalogos.AgregaReglaPrestamo );
end;

procedure TEditReglaPrestamo_DevEx.EscribirCambios;
begin
     with dmCatalogos,cdsReglaPrestamo do
     begin
          FieldByName('RP_LISTA').AsString := zBoolToStr( NOT rbTodosPrestamos.Checked );
          dmCatalogos.TiposPrestamosVacios := ( not StrLleno(memPrestamosLista.Text) );

          if zStrToBool( FieldByName('RP_VAL_FEC').AsString ) then
          begin
               if zStrToBool( FieldByName('RP_LISTA').AsString ) then
               begin
                  if (cdsPrestaXRegla.RecordCount <> 1) then
                     DataBaseError( 'Al validar fechas se debe de seleccionar un Tipo de Pr�stamo' )
               end
               else
                  DataBaseError( 'Al validar fechas se debe de seleccionar un Tipo de Pr�stamo' );
          end;

          if NOT zStrToBool( FieldByName('RP_LISTA').AsString ) then
          begin
               cdsPrestaXRegla.First;
               while NOT cdsPrestaXRegla.EOF DO
                     cdsPrestaXRegla.Delete;
               cdsPrestaXRegla.Enviar;
          end;


          FieldByName('RP_EMP_STS').AsInteger := rgStatusEmpleado.ItemIndex;
          FieldByName('RP_PRE_STS').AsInteger := rgStatusPrestamos.ItemIndex;
     end;
     inherited EscribirCambios;

end;


procedure TEditReglaPrestamo_DevEx.btnFormulaClick(Sender: TObject);
begin
     inherited;
     with dmCatalogos.cdsReglaPrestamo do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'RP_TOPE').AsString := GetFormulaConst( enEmpleado, RP_TOPE.Lines.Text, RP_TOPE.SelStart, evReglasPrestamo );
     end;
end;

procedure TEditReglaPrestamo_DevEx.FiltroBtnClick(Sender: TObject);
begin
     inherited;
     with dmCatalogos.cdsReglaPrestamo do
     begin
          ModoEditar;
          FieldByName( 'RP_FILTRO' ).AsString := GetFormulaConst( enEmpleado , RP_FILTRO.Lines.Text, RP_FILTRO.SelStart, evBase );
     end;
end;

procedure TEditReglaPrestamo_DevEx.rbTodosPrestamosClick(Sender: TObject);
begin
     inherited;
     ModoEditar;
     rbAlgunosPrestamos.Checked := FALSE;
     btnAgregarTiposPresta.Enabled := False;
     memPrestamosLista.Enabled := False;
end;

procedure TEditReglaPrestamo_DevEx.rgStatusPrestamosClick(Sender: TObject);
begin
     inherited;
     ModoEditar;
end;

procedure TEditReglaPrestamo_DevEx.btnAgregarTiposPrestaClick(Sender: TObject);
 var
    lNingunTipo:Boolean;
begin
     inherited;
     repeat
           ShowDlgModal( EditPrestaXRegla_DevEx, TEditPrestaXRegla_DevEx );
           lNingunTipo := ( dmCatalogos.cdsPrestaXRegla.RecordCount = 0 ) and ( EditPrestaXRegla_DevEx.ModalResult = mrOK );
           if lNingunTipo then
              ZError( Caption, 'No se seleccion� ning�n Tipo de Pr�stamo' ,0);

           //Cuando esta marcada la opcion de Validar Fechas solo se permite un tipo de prestamo
           if zStrToBool( dmCatalogos.cdsReglaPrestamo.FieldByName('RP_VAL_FEC').AsString ) then
           begin
                lNingunTipo := ( dmCatalogos.cdsPrestaXRegla.RecordCount > 1 ) {and ( EditPrestaXRegla_DevEx.ModalResult = mrOK )};
                if lNingunTipo then
                   ZError( Caption, 'Solo se permite un Tipo de Pr�stamo cuando se validan fechas' ,0);
           end;

      until not lNingunTipo;
      LlenarTiposPrestamos;
end;

procedure TEditReglaPrestamo_DevEx.rbAlgunosPrestamosClick(Sender: TObject);
begin
     inherited;
     ModoEditar;
     rbTodosPrestamos.Checked := FALSE;
     btnAgregarTiposPresta.Enabled := True;
     memPrestamosLista.Enabled := True;
end;

procedure TEditReglaPrestamo_DevEx.ModoEditar;
begin
     with dmCatalogos.cdsReglaPrestamo do
     begin
          if not ( Editing or Inserting ) and not Conectando then
             Edit;

     end;
end;


procedure TEditReglaPrestamo_DevEx.LlenarTiposPrestamos;
begin
     dmTablas.cdsTPresta.Conectar;
     with dmCatalogos.cdsPrestaXRegla do
     begin
          Refrescar;
          First;
          memPrestamosLista.Text := VACIO;
          while (not Eof )do
          begin
               dmTablas.cdsTPresta.Locate('TB_CODIGO',dmCatalogos.cdsPrestaXRegla.FieldByName('TB_CODIGO').AsString,[]);
               memPrestamosLista.Text := ConcatString( memPrestamosLista.Text , dmTablas.cdsTPresta.FieldByName('TB_CODIGO').AsString +' = '+ dmTablas.cdsTPresta.FieldByName('TB_ELEMENT').AsString,CR_LF);
               Next;
          end;
     end;
end;



procedure TEditReglaPrestamo_DevEx.rgStatusEmpleadoClick(Sender: TObject);
begin
     inherited;
     ModoEditar;
end;

procedure TEditReglaPrestamo_DevEx.CancelarClick(Sender: TObject);
begin
     inherited;
     with dmCatalogos,cdsReglaPrestamo do
     begin
          if NOT zStrToBool( FieldByName('RP_LISTA').AsString ) then
          begin
               memPrestamosLista.Lines.Clear;   
               cdsPrestaXRegla.First;
               while NOT cdsPrestaXRegla.EOF DO
                     cdsPrestaXRegla.Delete;
               cdsPrestaXRegla.Enviar;
          end;
     end;
end;


procedure TEditReglaPrestamo_DevEx.RP_VAL_FECClick(Sender: TObject);
begin
     inherited;
     if RP_VAL_FEC.Checked then
        rbAlgunosPrestamos.Checked := TRUE;

end;

end.
