unit FMunicipios_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, Db, ExtCtrls,
     ZBaseConsulta,
     ZetaDBGrid, StdCtrls, ZBaseGridLectura_DevEx,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Menus,
  ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  ZetaCXGrid, ZetaKeyLookup_DevEx;

type
  TMunicipios_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    ESTADO_look: TZetaKeyLookup_DevEx;
    Label1: TLabel;
    TB_CODIGO: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    ENTIDAD_TXT: TcxGridDBColumn;
    TB_INGLES: TcxGridDBColumn;
    TB_NUMERO: TcxGridDBColumn;
    TB_TEXTO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure ESTADO_lookValidKey(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar;override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Municipios_DevEx: TMunicipios_DevEx;

implementation

uses DCatalogos,
     ZetaBuscador_DevEx,
     ZetaCommonClasses,ZetaCommonTools, dTablas,DGlobal,ZGlobalTress;

{$R *.DFM}

{ TMunicipios }

procedure TMunicipios_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_TABLA_MUNICIPIOS;

end;

procedure TMunicipios_DevEx.Connect;
begin
     with dmTablas do
     begin
          cdsMunicipios.Conectar;
          DataSource.DataSet:= cdsMunicipios;
          ESTADO_look.LookupDataset := cdsEstado;
          if ESTADO_look.Llave = VACIO then
          begin
               ESTADO_look.Llave := Global.GetGlobalString(K_GLOBAL_ENTIDAD_EMPRESA);
               ESTADO_lookValidKey(Self);
          end
     end;
end;

procedure TMunicipios_DevEx.Refresh;
begin
     dmTablas.cdsMunicipios.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TMunicipios_DevEx.Agregar;
begin
     dmTablas.cdsMunicipios.Agregar;
end;

procedure TMunicipios_DevEx.Borrar;
begin
     dmTablas.cdsMunicipios.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TMunicipios_DevEx.Modificar;
begin
     dmTablas.cdsMunicipios.Modificar;
end;

procedure TMunicipios_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Municipio', 'Municipio', 'TB_CODIGO', dmTablas.cdsMunicipios );
end;

procedure TMunicipios_DevEx.ESTADO_lookValidKey(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     if StrLleno(ESTADO_look.Llave)then
     begin
          with dmTablas.cdsMunicipios do
          begin
               Filtered := False;
               Filter := Format('TB_ENTIDAD = ''%s''',[ESTADO_look.Llave ]);
               Filtered := True;
          end;
     end
     else
          dmTablas.cdsMunicipios.Filtered := False;
     try
        Refresh;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TMunicipios_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  inherited;

  //agrupar
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
  //ver la caja de agrupamiento
  ZetaDBGridDBTableView.OptionsView.GroupByBox := True;

  //columna sumatoria: cuantos
  CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TB_CODIGO'), K_SIN_TIPO , '', skCount);
  ZetaDBGridDBTableView.ApplyBestFit();
  
end;

end.
