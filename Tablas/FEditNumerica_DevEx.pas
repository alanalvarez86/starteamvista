unit FEditNumerica_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, ComCtrls, Db, DbClient,
     ExtCtrls, Buttons, DBCtrls, StdCtrls, Mask,
     ZetaDBGrid, ZetaKeyCombo, ZetaMessages,
     ZBaseEdicionRenglon_DevEx, cxGraphics,
     cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
      TressMorado2013, dxSkinsDefaultPainters,
     cxControls, dxSkinscxPCPainter, cxPCdxBarPopupMenu, dxSkinsdxBarPainter,
     dxBarExtItems, dxBar, cxClasses, ImgList, cxPC, cxNavigator,
     cxDBNavigator, cxButtons, ZetaNumero;

type
  TEditNumerica_DevEx = class(TBaseEdicionRenglon_DevEx)
    TB_CODIGOlbl: TLabel;
    NU_CODIGO: TZetaDBNumero;
    NU_DESCRIP: TDBEdit;
    TB_ELEMENTlbl: TLabel;
    Label3: TLabel;
    NU_INGLES: TDBEdit;
    NU_TEXTO: TDBEdit;
    Label5: TLabel;
    Label4: TLabel;
    NU_NUMERO: TZetaDBNumero;
    Panel3: TPanel;
    CBTArt80: TZetaKeyCombo;
    LblTArt80: TLabel;
    SBAgregarTArt80: TcxButton;
    SBBorrarTArt80: TcxButton;
    SBCambiarTArt80: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure CBTArt80Change(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure SBAgregarTArt80Click(Sender: TObject);
    procedure SBBorrarTArt80Click(Sender: TObject);
    procedure SBCambiarTArt80Click(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
  private
    { Private declarations }
    FTablaEnabled : Boolean;
    procedure SetTablaEnabled( const lEnabled: Boolean );
    procedure RefrescaComboTArt80;
    procedure HabilitaTabla( const lEnabled: Boolean );
    procedure HabilitaCombo(const lEnabled: Boolean);
  protected
    { Protected declarations }
    property TablaEnabled: Boolean write SetTablaEnabled;
    procedure Connect;override;
    procedure DoCancelChanges; override;
    //procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditNumerica_DevEx: TEditNumerica_DevEx;

implementation

uses DTablas,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZAccesosTress,
     ZetaDialogo;

{$R *.DFM}

const
     K_MESS_NO_TABLA = 'No Hay Datos Para %s';

procedure TEditNumerica_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := NU_CODIGO;
     IndexDerechos := ZAccesosTress.D_TAB_OFI_ISPT_NUMERICAS;
     HelpContext := ZetaCommonClasses.H70763_ISPT_numericas;
end;

procedure TEditNumerica_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := Tabla;
end;

procedure TEditNumerica_DevEx.Connect;
begin
     with dmTablas do
     begin
          //cdsNumericas.Conectar;   // Ya llega conectado
          Datasource.Dataset := cdsEditNumericas;

          //cdsArt80.Conectar;       // Ya llega conectado
          dsRenglon.DataSet := cdsArt80;

     end;
     RefrescaComboTArt80;
end;

{
procedure TEditNumerica.DoLookup;
begin
     inherited;
     ZetaBuscaEntero.BuscarCodigo( 'N�mero', 'Tabla ISPT y Num�rica', 'NU_CODIGO', dmTablas.cdsNumericas );
end;
}

procedure TEditNumerica_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = nil ) then    // Cuando cambia todo el registro - eg. Cancel
        TablaEnabled := TRUE
     else if ( Field.FieldName = 'NU_CODIGO' ) and ( Field.DataSet.State = dsEdit ) then   // Cambi� el c�digo de Tabla N�merica - Se cancela edici�n de los hijos
     begin
          RefrescaComboTArt80;    // Se cancelan los cambios a TArt80 y se debe refrescar el combo
          TablaEnabled := FALSE;
     end;
end;

procedure TEditNumerica_DevEx.SetTablaEnabled(const lEnabled: Boolean);
begin
     if ( FTablaEnabled <> lEnabled ) then
     begin
          HabilitaCombo( lEnabled );
          HabilitaTabla( lEnabled );
          FTablaEnabled := lEnabled;
     end;
end;

procedure TEditNumerica_DevEx.HabilitaTabla( const lEnabled: Boolean );
begin
     BBAgregar_DevEx.Enabled := lEnabled;
     BBBorrar_DevEx.Enabled := lEnabled;
     BBModificar_DevEx.Enabled := lEnabled;
     GridRenglones.ReadOnly := ( not lEnabled );
end;

procedure TEditNumerica_DevEx.HabilitaCombo( const lEnabled: Boolean );
begin
     SBAgregarTArt80.Enabled := lEnabled;
     SBBorrarTArt80.Enabled := lEnabled;
     SBCambiarTArt80.Enabled := lEnabled;
end;

procedure TEditNumerica_DevEx.RefrescaComboTArt80;
var
   sFecha: String;    // Llave para ubicar el combo
begin
     dmTablas.CargaListaTArt80( CBTArt80.Lista );
     sFecha := FechaAsStr( dmTablas.cdsTArt80.FieldByName( 'TI_INICIO' ).AsDateTime ); // Est� posicionado sobre el registro a mostrar
     with CBTArt80 do
     begin
          ItemIndex := Lista.IndexOfName( sFecha );
          HabilitaTabla( Lista.Count > 0 );
     end;
end;

procedure TEditNumerica_DevEx.CBTArt80Change(Sender: TObject);
var
   sNewFecha, sCurrFecha : String;
begin
     inherited;
     sNewFecha := CBTArt80.Llave;
     with dmTablas.cdsTArt80 do
     begin
          sCurrFecha := FechaAsStr( FieldByName( 'TI_INICIO' ).AsDateTime );
          if ( sNewFecha <> sCurrFecha ) then
             Locate( 'TI_INICIO', sNewFecha, [] );
     end;
end;

procedure TEditNumerica_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     with DataSource do
     begin
          if ( TClientDataSet( DataSet ).ChangeCount = 0 ) and         //Si se aplicaron los cambios
             ( not ( DataSet.State in [ dsEdit, dsInsert ] ) ) then    //y no se tienen cambios pendientes
             Close;
     end;
end;

procedure TEditNumerica_DevEx.DoCancelChanges;
begin
     inherited;
     RefrescaComboTArt80;
end;

procedure TEditNumerica_DevEx.SBAgregarTArt80Click(Sender: TObject);
begin
     inherited;
     if CheckDerechosPadre then
     begin
          dmTablas.cdsTArt80.Agregar;
          RefrescaComboTArt80;
     end;
end;

procedure TEditNumerica_DevEx.SBBorrarTArt80Click(Sender: TObject);
begin
     inherited;
     if CheckDerechosPadre then
     begin
          with dmTablas.cdsTArt80 do
          begin
               if IsEmpty then
                  ZetaDialogo.zInformation( self.Caption, Format( K_MESS_NO_TABLA, [ ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord(ukDelete) ) ] ), 0 )
               else
               begin
                    Borrar;
                    RefrescaComboTArt80;
               end;
          end;
     end;
end;

procedure TEditNumerica_DevEx.SBCambiarTArt80Click(Sender: TObject);
begin
     inherited;
     if CheckDerechosPadre then
     begin
          with dmTablas.cdsTArt80 do
          begin
               if IsEmpty then
                  ZetaDialogo.zInformation( self.Caption, Format( K_MESS_NO_TABLA, [ ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord(ukModify) ) ] ), 0 )
               else
               begin
                    Modificar;
                    RefrescaComboTArt80;
               end;
          end;
     end;
end;

procedure TEditNumerica_DevEx.BBAgregarClick(Sender: TObject);
begin
     if ( BBAgregar_DevEx.Enabled ) then     // Puede llegar por KeyDown
        inherited;
end;

procedure TEditNumerica_DevEx.BBBorrarClick(Sender: TObject);
begin
     if ( BBBorrar_DevEx.Enabled ) then     // Puede llegar por KeyDown
        inherited;
end;

procedure TEditNumerica_DevEx.BBModificarClick(Sender: TObject);
begin
     if ( BBModificar_DevEx.Enabled ) then     // Puede llegar por KeyDown
        inherited;
end;

end.
