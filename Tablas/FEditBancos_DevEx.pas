unit FEditBancos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx,  StdCtrls, Mask, DBCtrls, Db,
  Buttons, ExtCtrls, ZetaKeyCombo,ZetaCommonLists, ZetaNumero, ZetaEdit,
  ZetaSmartLists, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, TressMorado2013, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, dxSkinsDefaultPainters;

type
  TEditBancos_DevEx = class(TBaseEdicion_DevEx)
    DBInglesLBL: TLabel;
    DBDescripcionLBL: TLabel;
    DBCodigoLBL: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    TB_INGLES: TDBEdit;
    TB_TEXTO: TDBEdit;
    TB_NUMERO: TZetaDBNumero;
    Label4: TLabel;
    TB_NOMBRE: TDBEdit;
    Label5: TLabel;
    ZetaDBNumero1: TZetaDBNumero;
    TB_ACTIVO: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
  protected
    procedure Connect;override;
    procedure DoLookup; override;
  public
  end;

var
  EditBancos_DevEx: TEditBancos_DevEx;

implementation

uses DTablas, ZetaCommonClasses, ZAccesosTress, ZetaBuscador_DevEx;

{$R *.DFM}

procedure TEditBancos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H10139_Bancos;
     FirstControl := TB_CODIGO;
     IndexDerechos := ZAccesosTress.D_TAB_BANCOS;
end;

procedure TEditBancos_DevEx.Connect;
begin
     Datasource.Dataset := dmTablas.cdsBancos;
end;

procedure TEditBancos_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO',  dmTablas.cdsBancos);
end;

end.
