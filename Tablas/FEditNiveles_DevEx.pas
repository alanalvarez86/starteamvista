unit FEditNiveles_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseTablas_DevEx, Db, ZetaNumero, Mask, DBCtrls, StdCtrls, ZetaEdit, ExtCtrls,
  Buttons, ZetaClientDataset, dbClient, ZetaSmartLists, ZetaKeyLookup_DevEx, DGlobal, ZGlobalTress,
  ComCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator,
  cxDBNavigator, cxButtons, cxContainer, cxEdit, cxGroupBox, cxRadioGroup,
  cxListBox;

type
  TEditNiveles_DevEx = class(TEditTablas_DevEx)
    TB_SUB_CTA: TDBEdit;
    bImpSubCuentas: TdxBarButton;
    TB_ACTIVO: TDBCheckBox;
    lblTB_RELACIO: TLabel;
    TB_RELACIO: TZetaDBKeyLookup_DevEx;
    lblUsuario: TLabel;
    US_CODIGO: TZetaDBKeyLookup_DevEx;
    gbConfidencialidad: TcxGroupBox;
    btSeleccionarConfiden: TcxButton;
    listaConfidencialidad: TcxListBox;
    rbConfidenTodas: TcxRadioButton;
    rbConfidenAlgunas: TcxRadioButton;
    procedure bImpSubCuentasClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btSeleccionarConfidenClick(Sender: TObject);
    procedure rbConfidenTodasClick(Sender: TObject);
    procedure rbConfidenAlgunasClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }  
    lValoresConfidencialidad : TStringList;

    procedure SetLookupDataset(const Value: TZetaLookupDataSet);
    function GetLookupDataset: TZetaLookupDataSet;
    procedure FillListaConfidencialidad;
    procedure SetListConfidencialidad( sValores : string );
    procedure GetConfidencialidad;
  protected
    { Protected declarations }
    procedure AjustarComponentesUsuario( iNivel : integer);
    property LookupDataset: TZetaLookupDataSet read GetLookupDataset write SetLookupDataset;
    procedure Connect; override;
    procedure HabilitaControles;override;
  end;


  TTOEditNivel1_DevEx = class( TEditNiveles_DevEx )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditNivel2_DevEx = class( TEditNiveles_DevEx )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditNivel3_DevEx = class( TEditNiveles_DevEx )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditNivel4_DevEx = class( TEditNiveles_DevEx )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditNivel5_DevEx = class( TEditNiveles_DevEx )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditNivel6_DevEx = class( TEditNiveles_DevEx )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditNivel7_DevEx = class( TEditNiveles_DevEx )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditNivel8_DevEx = class( TEditNiveles_DevEx )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditNivel9_DevEx = class( TEditNiveles_DevEx )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  {$ifdef ACS}
  TTOEditNivel10_DevEx = class( TEditNiveles_DevEx )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditNivel11_DevEx = class( TEditNiveles_DevEx )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditNivel12_DevEx = class( TEditNiveles_DevEx )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  {$endif}

var
  EditNiveles_DevEx: TEditNiveles_DevEx;

implementation
uses
    DTablas,
    DSistema,
    ZImportaSubCuentas,
    ZetaCommonClasses,
    ZAccesosTress,
    ZetaCommonTools, DBaseSistema,
    FSeleccionarConfidencialidad_DevEx, DCliente;

{$R *.DFM}

{ ********* TEditLookup ********* }

procedure TEditNiveles_DevEx.Connect;
begin         
     dmSistema.cdsNivel0.Conectar;
     US_CODIGO.LookupDataset := dmSistema.cdsUsuariosLookup;
     inherited Connect;
     Caption := LookupDataset.LookupName;
end;

function TEditNiveles_DevEx.GetLookupDataset: TZetaLookupDataSet;
begin
     Result := TZetaLookupDataset( Self.ZetaDataset );
end;

procedure TEditNiveles_DevEx.SetLookupDataset(const Value: TZetaLookupDataSet);
begin
     Self.ZetaDataset := Value;
end;

procedure TEditNiveles_DevEx.bImpSubCuentasClick(Sender: TObject);
begin
     inherited;
     ZImportaSubCuentas.ImportaArchivo( TZetaClientDataset( DataSource.DataSet ), 'TB_CODIGO', 'TB_SUB_CTA' );
end;

{ TTOEditNivel1_DevEx }

procedure TTOEditNivel1_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsNivel1;
     HelpContext:= H70072_Areas;
     IndexDerechos := ZAccesosTress.D_TAB_AREAS_NIVEL1;
     {$ifdef ACS}
     {En este caso, por no tener un nivel inferior, no se habilita el control}
     TB_RELACIO.Enabled := False;
     lblTB_RELACIO.Enabled := False;
     {$endif}
     AjustarComponentesUsuario( 1 );
end;

{ TTOEditNivel2_DevEx }

procedure TTOEditNivel2_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsNivel2;
     HelpContext:= H70072_Areas;
     IndexDerechos := ZAccesosTress.D_TAB_AREAS_NIVEL2;
     {$ifdef ACS}
     {Se conecta el componente TB_RELACIO al nivel inferior}
     dmTablas.cdsNivel1.Conectar;
     TB_RELACIO.LookupDataset := dmTablas.cdsNivel1;
     lblTB_RELACIO.Enabled := True;
     TB_RELACIO.Enabled := True;
     {$endif}
     AjustarComponentesUsuario(  2  );
end;

{ TTOEditNivel3 }

procedure TTOEditNivel3_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsNivel3;
     HelpContext:= H70072_Areas;
     IndexDerechos := ZAccesosTress.D_TAB_AREAS_NIVEL3;
     {$ifdef ACS}
     {Se conecta el componente TB_RELACIO al nivel inferior}
     dmTablas.cdsNivel2.Conectar;
     TB_RELACIO.LookupDataset := dmTablas.cdsNivel2;
     lblTB_RELACIO.Enabled := True;
     TB_RELACIO.Enabled := True;
     {$endif}
     AjustarComponentesUsuario(  3  );
end;

{ TTOEditNivel4 }

procedure TTOEditNivel4_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsNivel4;
     HelpContext:= H70072_Areas;
     IndexDerechos := ZAccesosTress.D_TAB_AREAS_NIVEL4;
     {$ifdef ACS}
     {Se conecta el componente TB_RELACIO al nivel inferior}
     dmTablas.cdsNivel3.Conectar;
     TB_RELACIO.LookupDataset := dmTablas.cdsNivel3;
     lblTB_RELACIO.Enabled := True;
     TB_RELACIO.Enabled := True;
     {$endif}
     AjustarComponentesUsuario(  4  );
end;

{ TTOEditNivel5 }

procedure TTOEditNivel5_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsNivel5;
     HelpContext:= H70072_Areas;
     IndexDerechos := ZAccesosTress.D_TAB_AREAS_NIVEL5;
     {$ifdef ACS}
     {Se conecta el componente TB_RELACIO al nivel inferior}
     dmTablas.cdsNivel4.Conectar;
     TB_RELACIO.LookupDataset := dmTablas.cdsNivel4;
     lblTB_RELACIO.Enabled := True;
     TB_RELACIO.Enabled := True;
     {$endif}
     AjustarComponentesUsuario(  5  );
end;

{ TTOEditNivel6 }

procedure TTOEditNivel6_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsNivel6;
     HelpContext:= H70072_Areas;
     IndexDerechos := ZAccesosTress.D_TAB_AREAS_NIVEL6;
     {$ifdef ACS}
     {Se conecta el componente TB_RELACIO al nivel inferior}
     dmTablas.cdsNivel5.Conectar;
     TB_RELACIO.LookupDataset := dmTablas.cdsNivel5;
     lblTB_RELACIO.Enabled := True;
     TB_RELACIO.Enabled := True;
     {$endif}
     AjustarComponentesUsuario(  6  );
end;

{ TTOEditNivel7 }

procedure TTOEditNivel7_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsNivel7;
     HelpContext:= H70072_Areas;
     IndexDerechos := ZAccesosTress.D_TAB_AREAS_NIVEL7;
     {$ifdef ACS}
     {Se conecta el componente TB_RELACIO al nivel inferior}
     dmTablas.cdsNivel6.Conectar;
     TB_RELACIO.LookupDataset := dmTablas.cdsNivel6;
     lblTB_RELACIO.Enabled := True;
     TB_RELACIO.Enabled := True;
     {$endif}
     AjustarComponentesUsuario(  7  );
end;

{ TTOEditNivel8 }

procedure TTOEditNivel8_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsNivel8;
     HelpContext:= H70072_Areas;
     IndexDerechos := ZAccesosTress.D_TAB_AREAS_NIVEL8;
     {$ifdef ACS}
     {Se conecta el componente TB_RELACIO al nivel inferior}
     dmTablas.cdsNivel7.Conectar;
     TB_RELACIO.LookupDataset := dmTablas.cdsNivel7;
     lblTB_RELACIO.Enabled := True;
     TB_RELACIO.Enabled := True;
     {$endif}
     AjustarComponentesUsuario(  8  );
end;

{ TTOEditNivel9 }

procedure TTOEditNivel9_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsNivel9;
     HelpContext:= H70072_Areas;
     IndexDerechos := ZAccesosTress.D_TAB_AREAS_NIVEL9;
     {$ifdef ACS}
     {Se conecta el componente TB_RELACIO al nivel inferior}
     dmTablas.cdsNivel8.Conectar;
     TB_RELACIO.LookupDataset := dmTablas.cdsNivel8;
     lblTB_RELACIO.Enabled := True;
     TB_RELACIO.Enabled := True;
     {$endif}
     AjustarComponentesUsuario(  9  );
end;

{$ifdef ACS}
{ TTOEditNivel10 }

procedure TTOEditNivel10_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsNivel10;
     HelpContext:= H70072_Areas;
     IndexDerechos := ZAccesosTress.D_TAB_AREAS_NIVEL10;
     {$ifdef ACS}
     {Se conecta el componente TB_RELACIO al nivel inferior}
     dmTablas.cdsNivel9.Conectar;
     TB_RELACIO.LookupDataset := dmTablas.cdsNivel9;
     lblTB_RELACIO.Enabled := True;
     TB_RELACIO.Enabled := True;
     {$endif}
     AjustarComponentesUsuario(  10  );
end;
{ TTOEditNivel11 }

procedure TTOEditNivel11_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsNivel11;
     HelpContext:= H70072_Areas;
     IndexDerechos := ZAccesosTress.D_TAB_AREAS_NIVEL11;
     {$ifdef ACS}
     {Se conecta el componente TB_RELACIO al nivel inferior}
     dmTablas.cdsNivel10.Conectar;
     TB_RELACIO.LookupDataset := dmTablas.cdsNivel10;
     lblTB_RELACIO.Enabled := True;
     TB_RELACIO.Enabled := True;
     {$endif}
     AjustarComponentesUsuario(  11  );
end;
{ TTOEditNivel12 }

procedure TTOEditNivel12_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsNivel12;
     HelpContext:= H70072_Areas;
     IndexDerechos := ZAccesosTress.D_TAB_AREAS_NIVEL12;
     {$ifdef ACS}
     {Se conecta el componente TB_RELACIO al nivel inferior}
     dmTablas.cdsNivel11.Conectar;
     TB_RELACIO.LookupDataset := dmTablas.cdsNivel11;
     lblTB_RELACIO.Enabled := True;
     TB_RELACIO.Enabled := True;
     {$endif}
     AjustarComponentesUsuario(  12  );
end;
{$endif}



procedure TEditNiveles_DevEx.HabilitaControles;
begin
     inherited;
     bImpSubCuentas.Enabled := not Editing;
end;

procedure TEditNiveles_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     US_CODIGO.DataSource := DataSource;
     {$endif}

     {$ifdef ACS}
     TB_RELACIO.DataSource := DataSource;
     lblTB_RELACIO.Visible := True;
     TB_RELACIO.Visible := True;

     if ( US_CODIGO.Visible ) then
        //Height := 294 + 150
        Height := 294 + 170
     else
     begin
        TB_ACTIVO.Top := US_CODIGO.Top;
        //Height := 273 + 150;
        Height := 273 + 170;
     end;

     {$else}
     TB_ACTIVO.Top := US_CODIGO.Top;
     US_CODIGO.Top := TB_RELACIO.Top;
     lblUsuario.Top :=  lblTB_RELACIO.Top;

     if ( US_CODIGO.Visible ) then
        //Height := 273 + 150
        Height := 273 + 170
     else
     begin
        TB_ACTIVO.Top := TB_RELACIO.Top;
        //Height := 252 + 150;
        Height := 252 + 170;
     end;
     {$endif}

     gbConfidencialidad.Top := TB_ACTIVO.Top + 25;
     lValoresConfidencialidad := TStringList.Create;
end;

procedure TEditNiveles_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     FillListaConfidencialidad;
     {$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
     TB_CODIGO.Width:= K_WIDTHLLAVE;
     {$ifend}
end;

procedure TEditNiveles_DevEx.AjustarComponentesUsuario(
 iNivel : integer);
{$ifndef DOS_CAPAS}
var
   bEsNivelSupervisor : Boolean;
{$endif}
begin

{$ifndef DOS_CAPAS}
     bEsNivelSupervisor := ( Global.GetGlobalInteger( K_GLOBAL_NIVEL_SUPERVISOR ) = iNivel ) and
                           ( Global.GetGlobalBooleano( K_GLOBAL_ENROLAMIENTO_SUPERVISORES) );
{$endif}

     if ( {$ifndef DOS_CAPAS}bEsNivelSupervisor{$else}false{$endif} ) then
     begin
        lblUsuario.Visible := True;
        US_CODIGO.Visible := True;
     end
     else
     begin
        lblUsuario.Visible := False;
        US_CODIGO.Visible := False;
     end;


end;

procedure TEditNiveles_DevEx.btSeleccionarConfidenClick(Sender: TObject);
var
   selConfi : TSeleccionarConfidencialidad_DevEx;
   sCM_NIVEL0Nuevo : string;
begin
  inherited;

  selConfi := TSeleccionarConfidencialidad_DevEx.Create( Self );
  sCM_NIVEL0Nuevo := selConfi.GetConfidencialidad( DataSource.DataSet.FieldByName('TB_NIVEL0').AsString, lValoresConfidencialidad, True, dmCliente.Confidencialidad );

  if (  sCM_NIVEL0Nuevo <>  DataSource.DataSet.FieldByName('TB_NIVEL0').AsString ) then
  begin
     with DataSource.DataSet do
     begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

           FieldByName('TB_NIVEL0').AsString  := sCM_NIVEL0Nuevo;
           GetConfidencialidad;
     end;
  end;

  FreeAndNil ( selConfi ) ;
end;

procedure TEditNiveles_DevEx.FillListaConfidencialidad;
begin
     with dmSistema.cdsNivel0 do
     begin
          First;
          lValoresConfidencialidad.Clear;
          lValoresConfidencialidad.NameValueSeparator := '=' ;
          While( not Eof )do
          begin
               lValoresConfidencialidad.Add(FieldByName('TB_CODIGO').AsString +'='+ FieldByName('TB_ELEMENT').AsString);
               Next;
          end;
     end;
end;

procedure TEditNiveles_DevEx.GetConfidencialidad;
begin
   if (  DataSource.DataSet  <> nil ) then
   with DataSource.DataSet do
   begin
        SetListConfidencialidad(  FieldByName('TB_NIVEL0').AsString );
   end;
end;

procedure TEditNiveles_DevEx.SetListConfidencialidad(sValores: string);
var
   Lista : TStringList;
   i, j : integer;
begin
    Lista := TStringList.Create;

    sValores := Trim( sValores );
    if StrLleno( sValores ) then
        Lista.CommaText  := sValores;

    listaConfidencialidad.Items.Clear;

    rbConfidenTodas.OnClick := nil;
    rbConfidenAlgunas.OnClick := nil;
    rbConfidenTodas.Checked := False;
    rbConfidenAlgunas.Checked := False;
                                                 
    if lValoresConfidencialidad.Count = 0 then
       FillListaConfidencialidad;

    for i:= 0 to lValoresConfidencialidad.Count - 1 do
    begin
        if ( Lista.Count > 0 ) then
        begin
                j := Lista.IndexOf( Trim( lValoresConfidencialidad.Names[i] )  );
                if ( j >= 0 ) then
                   listaConfidencialidad.Items.Add( lValoresConfidencialidad[i] )
        end
    end;

    rbConfidenTodas.Checked :=   listaConfidencialidad.Items.Count = 0;
    rbConfidenAlgunas.Checked := not rbConfidenTodas.Checked;

    if ( gbConfidencialidad.Enabled ) then
    begin
        listaConfidencialidad.Enabled := rbConfidenAlgunas.Checked;
        btSeleccionarConfiden.Enabled := rbConfidenAlgunas.Checked;
    end;

    rbConfidenTodas.OnClick := rbConfidenTodasClick;
    rbConfidenAlgunas.OnClick := rbConfidenAlgunasClick;

    FreeAndNil( Lista );

end;

procedure TEditNiveles_DevEx.rbConfidenTodasClick(Sender: TObject);
begin
  inherited;

   with DataSource.DataSet do
   begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

        FieldByName('TB_NIVEL0').AsString  :=  '';
   end;
   GetConfidencialidad;
end;

procedure TEditNiveles_DevEx.rbConfidenAlgunasClick(Sender: TObject);
begin
  inherited;
     if ( rbConfidenAlgunas.Checked ) then
     begin
        btSeleccionarConfidenClick( Sender );
        GetConfidencialidad;
     end;

end;

procedure TEditNiveles_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
     if ( Field = nil ) then
     begin
          with DataSource.DataSet do
          begin
               if ( State in [dsBrowse] ) then
                  GetConfidencialidad
               else if (State in [dsInsert]) then
                    rbConfidenTodas.Checked := TRUE;
          end;

     end;
end;

end.
