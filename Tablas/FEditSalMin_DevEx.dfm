inherited EditSalMin_DevEx: TEditSalMin_DevEx
  Left = 174
  Top = 219
  Caption = 'Salarios M'#237'nimos'
  ClientHeight = 163
  ClientWidth = 387
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 86
    Top = 45
    Width = 92
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha de Vigencia:'
  end
  object Label2: TLabel [1]
    Left = 130
    Top = 64
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Zona "A":'
  end
  object Label3: TLabel [2]
    Left = 130
    Top = 85
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Zona "B":'
  end
  object Label4: TLabel [3]
    Left = 130
    Top = 106
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Zona "C":'
  end
  inherited PanelBotones: TPanel
    Top = 127
    Width = 387
    inherited OK_DevEx: TcxButton
      Left = 223
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 302
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 387
    Height = 0
    inherited Splitter: TSplitter
      Height = 0
    end
    inherited ValorActivo1: TPanel
      Height = 0
    end
    inherited ValorActivo2: TPanel
      Width = 61
      Height = 0
      inherited textoValorActivo2: TLabel
        Width = 55
      end
    end
  end
  object SM_FEC_INI: TZetaDBFecha [6]
    Left = 186
    Top = 39
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 2
    Text = '09/dic/97'
    Valor = 35773.000000000000000000
    ConfirmEdit = True
    DataField = 'SM_FEC_INI'
    DataSource = DataSource
  end
  object SM_ZONA_A: TZetaDBNumero [7]
    Left = 186
    Top = 61
    Width = 60
    Height = 21
    Mascara = mnPesosDiario
    TabOrder = 3
    Text = '0.00'
    DataField = 'SM_ZONA_A'
    DataSource = DataSource
  end
  object SM_ZONA_B: TZetaDBNumero [8]
    Left = 186
    Top = 81
    Width = 60
    Height = 21
    Mascara = mnPesosDiario
    TabOrder = 4
    Text = '0.00'
    DataField = 'SM_ZONA_B'
    DataSource = DataSource
  end
  object SM_ZONA_C: TZetaDBNumero [9]
    Left = 186
    Top = 101
    Width = 60
    Height = 21
    Mascara = mnPesosDiario
    TabOrder = 5
    Text = '0.00'
    DataField = 'SM_ZONA_C'
    DataSource = DataSource
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Top = 0
  end
  inherited DataSource: TDataSource
    Left = 284
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
