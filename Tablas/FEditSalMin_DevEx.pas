unit FEditSalMin_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls,
  DBCtrls, StdCtrls, Mask, ZetaFecha,
  ZetaNumero, ZetaSmartLists, ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditSalMin_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    SM_FEC_INI: TZetaDBFecha;
    SM_ZONA_A: TZetaDBNumero;
    SM_ZONA_B: TZetaDBNumero;
    SM_ZONA_C: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure Connect;override;

  public
    { Public declarations }
  end;

var
  EditSalMin_DevEx: TEditSalMin_DevEx;

implementation

uses ZetaCommonClasses, ZAccesosTress, dTablas;

{$R *.DFM}

procedure TEditSalMin_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H70761_Salarios_minimos;
     FirstControl := SM_FEC_INI;
     IndexDerechos := ZAccesosTress.D_TAB_OFI_SAL_MINIMOS;
end;

procedure TEditSalMin_DevEx.Connect;
begin
     DataSource.DataSet := dmTablas.cdsSalMin;
end;

end.
