unit FEditMotivoChecaManual_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ExtCtrls, DBCtrls, ZetaSmartLists, Buttons,
  StdCtrls, ZetaNumero, Mask, ZetaEdit, ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TTOEditMotivoChecaManual_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    TB_INGLES: TDBEdit;
    TB_NUMERO: TZetaDBNumero;
    TB_TEXTO: TDBEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Private declarations }  
    procedure Connect;override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  TOEditMotivoChecaManual_DevEx: TTOEditMotivoChecaManual_DevEx;

implementation

uses dTablas,
     ZetaCommonClasses,
     ZAccesosTress,
     ZetaBuscador_DevEx;

{$R *.dfm}

{ TTOEditMotivoChecaManual }

procedure TTOEditMotivoChecaManual_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := TB_CODIGO;
     HelpContext:= H_Motivos_Edit_Checada_Manual;
     IndexDerechos := D_TAB_HIST_CHECA_MANUAL;;
end;

procedure TTOEditMotivoChecaManual_DevEx.Connect;
begin
     inherited;
     with dmTablas do
     begin
          DataSource.DataSet := cdsMotCheca;
          self.Caption := cdsMotCheca.LookupName;
     end;
end;

procedure TTOEditMotivoChecaManual_DevEx.DoLookup;
begin
     inherited;
     if dxBarButton_BuscarBtn.Enabled then
        ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', dmTablas.cdsMotCheca );
end;

end.
