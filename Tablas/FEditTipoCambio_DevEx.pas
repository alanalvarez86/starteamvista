unit FEditTipoCambio_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, DBCtrls, StdCtrls, Mask, ZetaNumero, ZetaFecha,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditTipoCambio_DevEx = class(TBaseEdicion_DevEx)
    TC_NUMERO: TZetaDBNumero;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label1: TLabel;
    TC_FEC_INI: TZetaDBFecha;
    TC_MONTO: TZetaDBNumero;
    TC_TEXTO: TDBEdit;
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure Connect;override;
  public
  end;

var
  EditTipoCambio_DevEx: TEditTipoCambio_DevEx;

implementation

uses DTablas, ZAccesosTress, ZetaCommonClasses;

{$R *.DFM}

procedure TEditTipoCambio_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := TC_FEC_INI;
     IndexDerechos := ZAccesosTress.D_TAB_OFI_TIPOS_CAMBIO;
     HelpContext := H70765_Tipos_de_cambio
end;

procedure TEditTipoCambio_DevEx.Connect;
begin
     DataSource.DataSet := dmTablas.cdsTipoCambio;
end;

end.
