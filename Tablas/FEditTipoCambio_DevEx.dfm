inherited EditTipoCambio_DevEx: TEditTipoCambio_DevEx
  Left = 231
  Top = 184
  Caption = 'Tipos de Cambio'
  ClientHeight = 174
  ClientWidth = 386
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 22
    Top = 43
    Width = 63
    Height = 13
    Caption = 'Fecha Inicial:'
  end
  object Label2: TLabel [1]
    Left = 52
    Top = 67
    Width = 33
    Height = 13
    Caption = 'Monto:'
  end
  object Label3: TLabel [2]
    Left = 45
    Top = 91
    Width = 40
    Height = 13
    Caption = 'N'#250'mero:'
  end
  object Label4: TLabel [3]
    Left = 29
    Top = 115
    Width = 56
    Height = 13
    Caption = 'Comentario:'
  end
  inherited PanelBotones: TPanel
    Top = 138
    Width = 386
    TabOrder = 4
    inherited OK_DevEx: TcxButton
      Left = 222
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 301
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 386
    TabOrder = 5
    inherited ValorActivo2: TPanel
      Width = 60
      inherited textoValorActivo2: TLabel
        Width = 54
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Top = 16
  end
  object TC_NUMERO: TZetaDBNumero [7]
    Left = 88
    Top = 89
    Width = 121
    Height = 21
    Mascara = mnHoras
    TabOrder = 2
    Text = '0.00'
    DataField = 'TC_NUMERO'
    DataSource = DataSource
  end
  object TC_FEC_INI: TZetaDBFecha [8]
    Left = 88
    Top = 41
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 0
    Text = '01/mar/99'
    Valor = 36220.000000000000000000
    ConfirmEdit = True
    DataField = 'TC_FEC_INI'
    DataSource = DataSource
  end
  object TC_MONTO: TZetaDBNumero [9]
    Left = 88
    Top = 65
    Width = 121
    Height = 21
    Mascara = mnMillares4
    TabOrder = 1
    Text = '0.0000'
    DataField = 'TC_MONTO'
    DataSource = DataSource
  end
  object TC_TEXTO: TDBEdit [10]
    Left = 88
    Top = 113
    Width = 280
    Height = 21
    DataField = 'TC_TEXTO'
    DataSource = DataSource
    TabOrder = 3
  end
  inherited DataSource: TDataSource
    Left = 340
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
