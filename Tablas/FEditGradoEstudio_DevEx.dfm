inherited TOEditEstudios_DevEx: TTOEditEstudios_DevEx
  Left = 302
  Top = 363
  Caption = ''
  ClientHeight = 208
  ClientWidth = 381
  PixelsPerInch = 96
  TextHeight = 13
  object DBInglesLBL: TLabel [0]
    Left = 46
    Top = 82
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ingl'#233's:'
  end
  object DBDescripcionLBL: TLabel [1]
    Left = 18
    Top = 61
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object DBCodigoLBL: TLabel [2]
    Left = 41
    Top = 40
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
  end
  object Label1: TLabel [3]
    Left = 47
    Top = 124
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto:'
  end
  object Label2: TLabel [4]
    Left = 37
    Top = 103
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  object DBCampo1LBL: TLabel [5]
    Left = 53
    Top = 145
    Width = 24
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo:'
  end
  inherited PanelBotones: TPanel
    Top = 172
    Width = 381
    TabOrder = 8
    inherited OK_DevEx: TcxButton
      Left = 217
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 296
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 381
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 55
      inherited textoValorActivo2: TLabel
        Width = 49
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 12
  end
  object TB_CODIGO: TZetaDBEdit [9]
    Left = 81
    Top = 36
    Width = 65
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 1
    ConfirmEdit = True
    DataField = 'TB_CODIGO'
    DataSource = DataSource
  end
  object TB_ELEMENT: TDBEdit [10]
    Left = 81
    Top = 57
    Width = 280
    Height = 21
    DataField = 'TB_ELEMENT'
    DataSource = DataSource
    TabOrder = 2
  end
  object TB_INGLES: TDBEdit [11]
    Left = 81
    Top = 78
    Width = 280
    Height = 21
    DataField = 'TB_INGLES'
    DataSource = DataSource
    TabOrder = 3
  end
  object TB_TEXTO: TDBEdit [12]
    Left = 81
    Top = 120
    Width = 280
    Height = 21
    DataField = 'TB_TEXTO'
    DataSource = DataSource
    TabOrder = 5
  end
  object TB_TIPO: TZetaDBKeyCombo [13]
    Left = 81
    Top = 141
    Width = 125
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    DropDownCount = 9
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 6
    ListaFija = lfEstudios
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'TB_TIPO'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object TB_NUMERO: TZetaDBNumero [14]
    Left = 81
    Top = 99
    Width = 100
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 4
    Text = '0.00'
    DataField = 'TB_NUMERO'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 284
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Enabled = True
      Visible = ivAlways
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
