unit FEditPrioAhorrPresta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, Buttons, ZetaSmartLists, Db, StdCtrls, ExtCtrls;

type
  TEditPrioAhorrPresta = class(TZetaDlgModal)
    Panel1: TPanel;
    Lista: TListBox;
    Subir: TZetaSmartListsButton;
    Bajar: TZetaSmartListsButton;
    procedure FormShow(Sender: TObject);
    procedure SubirClick(Sender: TObject);
    procedure BajarClick(Sender: TObject);
    procedure ListaClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FTieneDerechos: boolean;
    procedure Inicializa;
    procedure ActivaControles;
    procedure SetOkCancelar( const lEnabled: boolean );
    procedure EscribirCambios;
  public
    { Public declarations }
  end;

var
  EditPrioAhorrPresta: TEditPrioAhorrPresta;

implementation

uses
    ZAccesosMgr,
    ZAccesosTress,
    ZetaCommonClasses,
    dTablas;

{$R *.DFM}

procedure TEditPrioAhorrPresta.FormShow(Sender: TObject);
begin
     inherited;
     Inicializa;
     FTieneDerechos := ZAccesosMgr.CheckDerecho( D_TAB_NOM_TIPO_AHORRO, K_DERECHO_CAMBIO ) and
                       ZAccesosMgr.CheckDerecho( D_TAB_NOM_TIPO_PRESTA, K_DERECHO_CAMBIO );
     ActivaControles;
     SetOkCancelar( False );
end;

procedure TEditPrioAhorrPresta.OKClick(Sender: TObject);
begin
     inherited;
     EscribirCambios;
end;

procedure TEditPrioAhorrPresta.ListaClick(Sender: TObject);
begin
     inherited;
     ActivaControles;
end;

procedure TEditPrioAhorrPresta.Inicializa;
begin
     with dmTablas.cdsPrioridAhorroPresta do
     begin
          Refrescar;
          First;
          Lista.Clear;
          while not Eof do
          begin
               with Lista.Items do
               begin
                    AddObject( Format( '(%s) %s = %s', [ FieldByName( 'TB_CLASE' ).AsString,
                                                         FieldByName( 'TB_CODIGO' ).AsString,
                                                         FieldByName( 'TB_ELEMENT' ).AsString ] ),
                               TObject( Integer( Ord( FieldByName( 'TB_CODIGO' ).AsString[ 1 ] ) ) ) );
               end;
               Next;
          end;
          lista.ItemIndex := 0;
     end;
end;

procedure TEditPrioAhorrPresta.SubirClick(Sender: TObject);
var
   i: Integer;
begin
     SetOkCancelar( True );
     with Lista do
     begin
          i := ItemIndex;
          if ( i > 0 ) then
          begin
               Items.Exchange( i, ( i - 1 ) );
          end;
     end;
     ActivaControles;
end;

procedure TEditPrioAhorrPresta.BajarClick(Sender: TObject);
var
   i: Integer;
begin
     SetOkCancelar( True );
     with Lista do
     begin
          i := ItemIndex;
          with Items do
          begin
               if ( i < ( Count - 1 ) ) then
               begin
                    Exchange( i, ( i + 1 ) );
               end;
          end;
     end;
     ActivaControles;
end;

procedure TEditPrioAhorrPresta.ActivaControles;
var
   lHayMas: Boolean;
begin
     if FTieneDerechos then
     begin
          with Lista do
          begin
               lHayMas := ( Items.Count > 0 );
               Subir.Enabled := lHayMas and ( ItemIndex > 0 );
               Bajar.Enabled := lHayMas and ( ItemIndex < ( Items.Count - 1 ) );
          end;
     end
     else
     begin
          Subir.Enabled := FTienederechos;
          Bajar.Enabled := Subir.Enabled;
     end;
end;

procedure TEditPrioAhorrPresta.SetOkCancelar( const lEnabled: boolean );
begin
     OK.Enabled := lEnabled;
     if ( OK.Enabled ) then
     begin
          with Cancelar do
          begin
               Kind := bkCancel;
               ModalResult := mrCancel;
               Cancel := False;
               Caption := '&Cancelar';
               Hint := 'Cancelar Cambios';
          end;
     end
     else
     begin
          with Cancelar do
          begin
               Kind := bkClose;
               ModalResult := mrNone;
               Cancel := True;
               Caption := '&Salir';
               Hint := 'Cerrar Pantalla y Salir';
               if Self.Active then
                  SetFocus;
          end;
     end;
end;

procedure TEditPrioAhorrPresta.EscribirCambios;
begin
     if dmTablas.GrabaPrioridad( Lista.Items ) then
        Close;
end;

procedure TEditPrioAhorrPresta.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H70754_Prioridad_de_ahorros_y_prestamos;
end;

end.
