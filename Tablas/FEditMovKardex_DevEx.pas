unit FEditMovKardex_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, Db,
  Buttons, ExtCtrls, ZetaEdit, ZetaNumero, ZetaSmartLists,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditMovKardex_DevEx = class(TBaseEdicion_DevEx)
    DBInglesLBL: TLabel;
    DBDescripcionLBL: TLabel;
    DBCodigoLBL: TLabel;
    DBCampo1LBL: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    TB_SISTEMA: TDBCheckBox;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    TB_INGLES: TDBEdit;
    TB_TEXTO: TDBEdit;
    TB_NIVEL: TDBEdit;
    TB_NUMERO: TZetaDBNumero;
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormCreate(Sender: TObject);
  protected
    procedure Connect;override;
    procedure DoLookup; override;
  public
  end;

var
  EditMovKardex_DevEx: TEditMovKardex_DevEx;

implementation

{$R *.DFM}

uses DTablas, ZetaDialogo, ZAccesosTress, ZetaCommonClasses, ZetaBuscador_DevEx;

procedure TEditMovKardex_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H70741_Tipo_kardex;
     IndexDerechos := ZAccesosTress.D_TAB_PER_ESTUDIOS;
     FirstControl := TB_CODIGO;
end;

procedure TEditMovKardex_DevEx.Connect;
begin
     DataSource.DataSet := dmTablas.cdsMovKardex;
end;

procedure TEditMovKardex_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', dmTablas.cdsMovKardex );
end;

procedure TEditMovKardex_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     TB_CODIGO.Enabled:= not TB_SISTEMA.Checked; // No lleva if porque TB_SISTEMA solo cambia desde fabrica
     DBCodigoLBL.Enabled:= not TB_SISTEMA.Checked;
end;

end.
