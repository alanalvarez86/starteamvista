inherited EditBancos_DevEx: TEditBancos_DevEx
  Left = 465
  Top = 644
  Caption = 'Banco'
  ClientHeight = 258
  ClientWidth = 629
  PixelsPerInch = 96
  TextHeight = 13
  object DBInglesLBL: TLabel [0]
    Left = 45
    Top = 131
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ingl'#233's:'
  end
  object DBDescripcionLBL: TLabel [1]
    Left = 17
    Top = 63
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object DBCodigoLBL: TLabel [2]
    Left = 40
    Top = 41
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
  end
  object Label1: TLabel [3]
    Left = 46
    Top = 177
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto:'
  end
  object Label2: TLabel [4]
    Left = 36
    Top = 154
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  object Label4: TLabel [5]
    Left = 36
    Top = 85
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nombre:'
  end
  object Label5: TLabel [6]
    Left = 12
    Top = 108
    Width = 64
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero SAT:'
  end
  inherited PanelBotones: TPanel
    Top = 222
    Width = 629
    TabOrder = 10
    inherited OK_DevEx: TcxButton
      Left = 464
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 543
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 629
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 303
      inherited textoValorActivo2: TLabel
        Width = 297
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Top = 131
    TabOrder = 14
  end
  object TB_CODIGO: TZetaDBEdit [10]
    Left = 80
    Top = 37
    Width = 65
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 1
    ConfirmEdit = True
    DataField = 'TB_CODIGO'
    DataSource = DataSource
  end
  object TB_ELEMENT: TDBEdit [11]
    Left = 80
    Top = 59
    Width = 280
    Height = 21
    DataField = 'TB_ELEMENT'
    DataSource = DataSource
    TabOrder = 2
  end
  object TB_INGLES: TDBEdit [12]
    Left = 80
    Top = 127
    Width = 280
    Height = 21
    DataField = 'TB_INGLES'
    DataSource = DataSource
    TabOrder = 5
  end
  object TB_TEXTO: TDBEdit [13]
    Left = 80
    Top = 173
    Width = 280
    Height = 21
    DataField = 'TB_TEXTO'
    DataSource = DataSource
    TabOrder = 7
  end
  object TB_NUMERO: TZetaDBNumero [14]
    Left = 80
    Top = 150
    Width = 100
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 6
    Text = '0.00'
    DataField = 'TB_NUMERO'
    DataSource = DataSource
  end
  object TB_NOMBRE: TDBEdit [15]
    Left = 80
    Top = 81
    Width = 529
    Height = 21
    DataField = 'TB_NOMBRE'
    DataSource = DataSource
    TabOrder = 3
  end
  object ZetaDBNumero1: TZetaDBNumero [16]
    Left = 80
    Top = 104
    Width = 100
    Height = 21
    Mascara = mnEmpleado
    TabOrder = 4
    DataField = 'TB_SAT_BAN'
    DataSource = DataSource
  end
  object TB_ACTIVO: TDBCheckBox [17]
    Left = 42
    Top = 195
    Width = 51
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Activo:'
    DataField = 'TB_ACTIVO'
    DataSource = DataSource
    TabOrder = 8
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  inherited DataSource: TDataSource
    Left = 580
    Top = 120
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 655928
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 528
    Top = 18
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Visible = ivAlways
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 1180104
  end
end
