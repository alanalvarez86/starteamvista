inherited EditPrioAhorrPresta: TEditPrioAhorrPresta
  Left = 280
  Top = 219
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Prioridad de Ahorros y Préstamos'
  ClientHeight = 357
  ClientWidth = 369
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 321
    Width = 369
    inherited OK: TBitBtn
      Left = 201
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 286
    end
  end
  object Panel1: TPanel
    Left = 338
    Top = 0
    Width = 31
    Height = 321
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 1
    object Subir: TZetaSmartListsButton
      Left = 3
      Top = 130
      Width = 25
      Height = 25
      Hint = 'Subir Prioridad'
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
        3333333333090333333333333309033333333333330903333333333333090333
        3333333333090333333333300009000033333330999999903333333309999903
        3333333309999903333333333099903333333333309990333333333333090333
        3333333333090333333333333330333333333333333033333333}
      ParentShowHint = False
      ShowHint = True
      OnClick = SubirClick
      Tipo = bsSubir
    end
    object Bajar: TZetaSmartListsButton
      Left = 3
      Top = 154
      Width = 25
      Height = 25
      Hint = 'Bajar Prioridad'
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
        3333333333303333333333333309033333333333330903333333333330999033
        3333333330999033333333330999990333333333099999033333333099999990
        3333333000090000333333333309033333333333330903333333333333090333
        3333333333090333333333333309033333333333330003333333}
      ParentShowHint = False
      ShowHint = True
      OnClick = BajarClick
      Tipo = bsBajar
    end
  end
  object Lista: TListBox
    Left = 0
    Top = 0
    Width = 338
    Height = 321
    Align = alClient
    ItemHeight = 13
    TabOrder = 2
    OnClick = ListaClick
  end
end
