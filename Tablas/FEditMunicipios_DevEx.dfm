inherited EditMunicipios_DevEx: TEditMunicipios_DevEx
  Left = 429
  Top = 323
  Caption = 'Municipio'
  ClientHeight = 236
  ClientWidth = 397
  PixelsPerInch = 96
  TextHeight = 13
  object DBInglesLBL: TLabel [0]
    Left = 72
    Top = 82
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ingl'#233's:'
  end
  object DBDescripcionLBL: TLabel [1]
    Left = 44
    Top = 61
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object DBCodigoLBL: TLabel [2]
    Left = 67
    Top = 40
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
  end
  object TEXTOLBL: TLabel [3]
    Left = 73
    Top = 123
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto:'
  end
  object NumeroLBL: TLabel [4]
    Left = 63
    Top = 102
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  object Label1: TLabel [5]
    Left = 67
    Top = 146
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Estado:'
  end
  object Label2: TLabel [6]
    Left = 72
    Top = 169
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'STPS:'
  end
  inherited PanelBotones: TPanel
    Top = 200
    Width = 397
    TabOrder = 7
    inherited OK_DevEx: TcxButton
      Left = 233
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 312
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 397
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 71
      inherited textoValorActivo2: TLabel
        Width = 65
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 13
  end
  object TB_CODIGO: TZetaDBEdit [10]
    Left = 107
    Top = 36
    Width = 65
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 1
    ConfirmEdit = True
    DataField = 'TB_CODIGO'
    DataSource = DataSource
  end
  object TB_ELEMENT: TDBEdit [11]
    Left = 107
    Top = 57
    Width = 280
    Height = 21
    DataField = 'TB_ELEMENT'
    DataSource = DataSource
    TabOrder = 2
  end
  object TB_INGLES: TDBEdit [12]
    Left = 107
    Top = 78
    Width = 280
    Height = 21
    DataField = 'TB_INGLES'
    DataSource = DataSource
    TabOrder = 3
  end
  object TB_TEXTO: TDBEdit [13]
    Left = 107
    Top = 119
    Width = 280
    Height = 21
    DataField = 'TB_TEXTO'
    DataSource = DataSource
    TabOrder = 5
  end
  object TB_NUMERO: TZetaDBNumero [14]
    Left = 107
    Top = 98
    Width = 100
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 4
    Text = '0.00'
    DataField = 'TB_NUMERO'
    DataSource = DataSource
  end
  object TB_ENTIDAD: TZetaDBKeyLookup_DevEx [15]
    Left = 107
    Top = 142
    Width = 284
    Height = 21
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 8
    TabStop = True
    WidthLlave = 60
    OnValidKey = TB_ENTIDADValidKey
    DataField = 'TB_ENTIDAD'
    DataSource = DataSource
  end
  object TB_STPS: TZetaDBNumero [16]
    Left = 107
    Top = 165
    Width = 102
    Height = 21
    Mascara = mnDias
    TabOrder = 9
    Text = '0'
    DataField = 'TB_STPS'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 284
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Visible = ivAlways
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
