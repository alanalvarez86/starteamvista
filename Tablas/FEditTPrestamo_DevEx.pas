unit FEditTPrestamo_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DBCtrls, ZetaKeyLookup_DevEx, Mask, Db, 
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  
  TressMorado2013, dxSkinsDefaultPainters,  cxControls,
  dxSkinsdxBarPainter, ZetaNumero, ZetaEdit, ZetaKeyCombo, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, ExtCtrls,
  cxButtons, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC ;

type
  TEditTPrestamo_DevEx = class(TBaseEdicion_DevEx)
    DBInglesLBL: TLabel;
    DBDescripcionLBL: TLabel;
    DBCodigoLBL: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    TB_INGLES: TDBEdit;
    TB_NUMERO: TZetaDBNumero;
    TB_TEXTO: TDBEdit;
    TB_ACTIVO: TDBCheckBox;
    TabConfidencialidad: TcxPageControl;
    TabGenerales: TcxTabSheet;
    tsConfidencialidad: TcxTabSheet;
    Label3: TLabel;
    TB_CONCEPT: TZetaDBKeyLookup_DevEx;
    TB_RELATIV: TZetaDBKeyLookup_DevEx;
    Label4: TLabel;
    Label6: TLabel;
    TB_ALTA: TZetaDBKeyCombo;
    TB_LIQUIDA: TZetaDBKeyCombo;
    Label5: TLabel;
    GBTasas: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    TB_TASA1: TZetaDBNumero;
    TB_TASA2: TZetaDBNumero;
    TB_TASA3: TZetaDBNumero;
    gbConfidencialidad: TGroupBox;
    rbConfidenTodas: TRadioButton;
    rbConfidenAlgunas: TRadioButton;
    listaConfidencialidad: TListBox;
    btSeleccionarConfiden_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject); 
    procedure btSeleccionarConfidenClick(Sender: TObject);
    procedure rbConfidenTodasClick(Sender: TObject);
    procedure rbConfidenAlgunasClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    lValoresConfidencialidad : TStringList;
    procedure FillListaConfidencialidad;
    procedure SetListConfidencialidad( sValores : string );
    procedure GetConfidencialidad;
  protected
    procedure Connect;override;
    procedure DoLookup; override;
  public
  end;

var
  EditTPrestamo_DevEx: TEditTPrestamo_DevEx;

implementation

{$R *.DFM}

uses dTablas, dCatalogos, ZetaCommonClasses, ZAccesosTress, ZetaBuscador_DevEx,
     FSeleccionarConfidencialidad_DevEx, dSistema, DCliente, ZetaCommonTools;

procedure TEditTPrestamo_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef CAJAAHORRO}
     HelpContext:= H_REG_TIPO_PRESTAMO;
     {$else}
      HelpContext:= H70752_Tipo_prestamo;
     {$endif}

     FirstControl := TB_CODIGO;
     IndexDerechos := ZAccesosTress.D_TAB_NOM_TIPO_PRESTA;
     TB_CONCEPT.LookupDataset := dmCatalogos.cdsConceptos;
     TB_RELATIV.LookupDataset := dmCatalogos.cdsConceptos; 
     lValoresConfidencialidad := TStringList.Create;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TEditTPrestamo_DevEx.Connect;
begin     
     dmSistema.cdsNivel0.Conectar;
     dmCatalogos.cdsConceptos.Conectar;
     DataSource.DataSet := dmTablas.cdsTPresta;
end;

procedure TEditTPrestamo_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  FillListaConfidencialidad;
end;

procedure TEditTPrestamo_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', dmTablas.cdsTPresta );
end;

procedure TEditTPrestamo_DevEx.btSeleccionarConfidenClick(Sender: TObject);
var
   selConfi : TSeleccionarConfidencialidad_DevEx;
   sCM_NIVEL0Nuevo : string;
begin
  inherited;

  selConfi := TSeleccionarConfidencialidad_DevEx.Create( Self );
  sCM_NIVEL0Nuevo := selConfi.GetConfidencialidad( DataSource.DataSet.FieldByName('TB_NIVEL0').AsString, lValoresConfidencialidad, True, dmCliente.Confidencialidad );

  if (  sCM_NIVEL0Nuevo <>  DataSource.DataSet.FieldByName('TB_NIVEL0').AsString ) then
  begin
     with DataSource.DataSet do
     begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

           FieldByName('TB_NIVEL0').AsString  := sCM_NIVEL0Nuevo;
           GetConfidencialidad;
     end;
  end;

  FreeAndNil ( selConfi ) ;
end;

procedure TEditTPrestamo_DevEx.FillListaConfidencialidad;
begin
     with dmSistema.cdsNivel0 do
     begin
          First;
          lValoresConfidencialidad.Clear;
          lValoresConfidencialidad.NameValueSeparator := '=' ;
          While( not Eof )do
          begin
               lValoresConfidencialidad.Add(FieldByName('TB_CODIGO').AsString +'='+ FieldByName('TB_ELEMENT').AsString);
               Next;
          end;
     end;
end;

procedure TEditTPrestamo_DevEx.GetConfidencialidad;
begin
   if (  DataSource.DataSet  <> nil ) then
   with DataSource.DataSet do
   begin
        SetListConfidencialidad(  FieldByName('TB_NIVEL0').AsString );
   end;
end;

procedure TEditTPrestamo_DevEx.SetListConfidencialidad(sValores: string);
var
   Lista : TStringList;
   i, j : integer;
begin
    Lista := TStringList.Create;

    sValores := Trim( sValores );
    if StrLleno( sValores ) then
        Lista.CommaText  := sValores;

    listaConfidencialidad.Items.Clear;

    rbConfidenTodas.OnClick := nil;
    rbConfidenAlgunas.OnClick := nil;
    rbConfidenTodas.Checked := False;
    rbConfidenAlgunas.Checked := False;
                                                 
    if lValoresConfidencialidad.Count = 0 then
       FillListaConfidencialidad;

    for i:= 0 to lValoresConfidencialidad.Count - 1 do
    begin
        if ( Lista.Count > 0 ) then
        begin
                j := Lista.IndexOf( Trim( lValoresConfidencialidad.Names[i] )  );
                if ( j >= 0 ) then
                   listaConfidencialidad.Items.Add( lValoresConfidencialidad[i] )
        end
    end;

    rbConfidenTodas.Checked :=   listaConfidencialidad.Items.Count = 0;
    rbConfidenAlgunas.Checked := not rbConfidenTodas.Checked;

    if ( gbConfidencialidad.Enabled ) then
    begin
        listaConfidencialidad.Enabled := rbConfidenAlgunas.Checked;
        btSeleccionarConfiden_DevEx.Enabled := rbConfidenAlgunas.Checked;
    end;

    rbConfidenTodas.OnClick := rbConfidenTodasClick;
    rbConfidenAlgunas.OnClick := rbConfidenAlgunasClick;

    FreeAndNil( Lista );

end;

procedure TEditTPrestamo_DevEx.rbConfidenTodasClick(Sender: TObject);
begin
  inherited;

   with DataSource.DataSet do
   begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

        FieldByName('TB_NIVEL0').AsString  :=  '';
   end;
   GetConfidencialidad;
end;

procedure TEditTPrestamo_DevEx.rbConfidenAlgunasClick(Sender: TObject);
begin
  inherited;
     if ( rbConfidenAlgunas.Checked ) then
     begin
        btSeleccionarConfidenClick( Sender );
        GetConfidencialidad;
     end;

end;

procedure TEditTPrestamo_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
     if ( Field = nil ) then
     begin
          with DataSource.DataSet do
          begin
               if ( State in [dsBrowse] ) then
                  GetConfidencialidad
               else if (State in [dsInsert]) then
                    rbConfidenTodas.Checked := TRUE;
          end;

     end;
end;

procedure TEditTPrestamo_DevEx.SetEditarSoloActivos;
begin
     TB_CONCEPT.EditarSoloActivos := TRUE;
     TB_RELATIV.EditarSoloActivos := TRUE;
end;

end.



