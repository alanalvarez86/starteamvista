inherited EditMovKardex_DevEx: TEditMovKardex_DevEx
  Left = 323
  Top = 228
  Caption = 'Tipo de Movimiento de Kardex'
  ClientHeight = 214
  ClientWidth = 381
  PixelsPerInch = 96
  TextHeight = 13
  object DBInglesLBL: TLabel [0]
    Left = 46
    Top = 82
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ingl'#233's:'
  end
  object DBDescripcionLBL: TLabel [1]
    Left = 18
    Top = 61
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object DBCodigoLBL: TLabel [2]
    Left = 41
    Top = 41
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
  end
  object DBCampo1LBL: TLabel [3]
    Left = 50
    Top = 144
    Width = 27
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel:'
  end
  object Label1: TLabel [4]
    Left = 47
    Top = 123
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto:'
  end
  object Label2: TLabel [5]
    Left = 37
    Top = 102
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  inherited PanelBotones: TPanel
    Top = 178
    Width = 381
    TabOrder = 9
    inherited OK_DevEx: TcxButton
      Left = 217
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 296
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 381
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 55
      inherited textoValorActivo2: TLabel
        Width = 49
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Left = 0
    Top = 0
    TabOrder = 13
  end
  object TB_SISTEMA: TDBCheckBox [9]
    Left = 35
    Top = 161
    Width = 59
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Sistema:'
    DataField = 'TB_SISTEMA'
    DataSource = DataSource
    Enabled = False
    TabOrder = 7
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object TB_CODIGO: TZetaDBEdit [10]
    Left = 81
    Top = 36
    Width = 65
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 1
    ConfirmEdit = True
    DataField = 'TB_CODIGO'
    DataSource = DataSource
  end
  object TB_ELEMENT: TDBEdit [11]
    Left = 81
    Top = 57
    Width = 280
    Height = 21
    DataField = 'TB_ELEMENT'
    DataSource = DataSource
    TabOrder = 2
  end
  object TB_INGLES: TDBEdit [12]
    Left = 81
    Top = 78
    Width = 280
    Height = 21
    DataField = 'TB_INGLES'
    DataSource = DataSource
    TabOrder = 3
  end
  object TB_TEXTO: TDBEdit [13]
    Left = 81
    Top = 119
    Width = 280
    Height = 21
    DataField = 'TB_TEXTO'
    DataSource = DataSource
    TabOrder = 5
  end
  object TB_NIVEL: TDBEdit [14]
    Left = 81
    Top = 140
    Width = 21
    Height = 21
    DataField = 'TB_NIVEL'
    DataSource = DataSource
    TabOrder = 6
  end
  object TB_NUMERO: TZetaDBNumero [15]
    Left = 81
    Top = 98
    Width = 100
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 4
    Text = '0.00'
    DataField = 'TB_NUMERO'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 284
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Visible = ivAlways
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
