inherited Monedas_DevEx: TMonedas_DevEx
  Left = 512
  Top = 178
  Caption = 'Monedas'
  ClientHeight = 163
  ClientWidth = 496
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 496
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 170
      inherited textoValorActivo2: TLabel
        Width = 164
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 496
    Height = 144
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = DataSource
      object TB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'TB_CODIGO'
        MinWidth = 70
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'TB_ELEMENT'
        MinWidth = 100
        Width = 194
      end
      object TB_INGLES: TcxGridDBColumn
        Caption = 'Ingl'#233's'
        DataBinding.FieldName = 'TB_INGLES'
        MinWidth = 100
        Width = 157
      end
      object TB_VALOR: TcxGridDBColumn
        Caption = 'Valor'
        DataBinding.FieldName = 'TB_VALOR'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 80
      end
    end
  end
  inherited DataSource: TDataSource
    Top = 88
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end