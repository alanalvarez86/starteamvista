inherited TabOfiSatTipoContrato_DevEx: TTabOfiSatTipoContrato_DevEx
  Left = 744
  Top = 325
  Caption = 'Tipos de Contrato'
  ClientHeight = 163
  ClientWidth = 1146
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 1146
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 820
      inherited textoValorActivo2: TLabel
        Width = 814
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 1146
    Height = 144
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object TB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'TB_CODIGO'
        MinWidth = 70
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'TB_ELEMENT'
        MinWidth = 100
        Width = 132
      end
      object TB_SAT_BAN: TcxGridDBColumn
        Caption = 'C'#243'digo SAT'
        DataBinding.FieldName = 'TB_COD_SAT'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 80
      end
      object TB_INGLES: TcxGridDBColumn
        Caption = 'Ingl'#233's'
        DataBinding.FieldName = 'TB_INGLES'
        MinWidth = 100
        Width = 157
      end
    end
  end
  inherited DataSource: TDataSource
    Top = 88
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
