unit FBancos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db,
  ExtCtrls, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TBancos_DevEx = class(TBaseGridLectura_DevEx)
    TB_NOMBRE: TcxGridDBColumn;
    TB_SAT_BAN: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
  public
    procedure DoLookup; override;
  end;

var
  Bancos_DevEx: TBancos_DevEx;

implementation

uses dTablas, ZetaCommonClasses, ZetaBuscador_DevEx;

{$R *.DFM}

procedure TBancos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H10139_Bancos;

     CanLookup := True;
end;

procedure TBancos_DevEx.Connect;
begin
     with dmTablas do
     begin
          cdsBancos.Conectar;
          DataSource.DataSet:= cdsBancos;
     end;
end;

procedure TBancos_DevEx.Refresh;
begin
     dmTablas.cdsBancos.Refrescar;
end;

procedure TBancos_DevEx.Agregar;
begin
     dmTablas.cdsBancos.Agregar;
end;

procedure TBancos_DevEx.Borrar;
begin
     dmTablas.cdsBancos.Borrar;
end;

procedure TBancos_DevEx.Modificar;
begin
     dmTablas.cdsBancos.Modificar;
end;

procedure TBancos_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', dmTablas.cdsBancos );
end;

procedure TBancos_DevEx.FormShow(Sender: TObject);
begin
CreacolumaSumatoria(ZetaDbGridDbTableView.Columns[0],0,'',skCount);
  inherited;

end;

end.
