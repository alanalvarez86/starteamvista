unit FSalMin_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls,
  Grids, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TSistSalMin_DevEx = class(TBaseGridLectura_DevEx)
    SM_FEC_INI: TcxGridDBColumn;
    SM_ZONA_A: TcxGridDBColumn;
    SM_ZONA_B: TcxGridDBColumn;
    SM_ZONA_C: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
  end;

var
  SistSalMin_DevEx: TSistSalMin_DevEx;

implementation

uses dTablas, ZetaCommonClasses;

{$R *.DFM}

procedure TSistSalMin_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H70761_Salarios_minimos;
end;

procedure TSistSalMin_DevEx.Connect;
begin
     with dmTablas do
     begin
          cdsSalMin.Conectar;
          DataSource.DataSet:= cdsSalMin;
     end;
end;

procedure TSistSalMin_DevEx.Refresh;
begin
     dmTablas.cdsSalMin.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TSistSalMin_DevEx.Agregar;
begin
     dmTablas.cdsSalMin.Agregar;
end;

procedure TSistSalMin_DevEx.Borrar;
begin
     dmTablas.cdsSalMin.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TSistSalMin_DevEx.Modificar;
begin
     dmTablas.cdsSalMin.Modificar;
end;

procedure TSistSalMin_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('SM_FEC_INI'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

end.
