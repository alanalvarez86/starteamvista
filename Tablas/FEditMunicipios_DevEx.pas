unit FEditMunicipios_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, Db,
  Buttons, ExtCtrls, ZetaEdit, ZetaKeyCombo, ZetaNumero, ZetaSmartLists,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, ZetaKeyLookup_DevEx;

type
  TEditMunicipios_DevEx = class(TBaseEdicion_DevEx)
    DBInglesLBL: TLabel;
    DBDescripcionLBL: TLabel;
    DBCodigoLBL: TLabel;
    TEXTOLBL: TLabel;
    NumeroLBL: TLabel;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    TB_INGLES: TDBEdit;
    TB_TEXTO: TDBEdit;
    TB_NUMERO: TZetaDBNumero;
    TB_ENTIDAD: TZetaDBKeyLookup_DevEx;
    Label1: TLabel;
    Label2: TLabel;
    TB_STPS: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
    procedure TB_ENTIDADValidKey(Sender: TObject);
  protected
    procedure Connect;override;
    procedure DoLookup; override;
  public
  end;

var
  EditMunicipios_DevEx: TEditMunicipios_DevEx;

implementation

uses dTablas,
     DGlobal,
     ZetaBuscador_DevEx,
     ZetaCommonClasses,
     ZGlobalTress,
     ZetaCommonLists,
     ZAccesosTress;

{$R *.DFM}

procedure TEditMunicipios_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := TB_CODIGO;
     HelpContext:= H_TABLA_MUNICIPIOS_EDICION;
     IndexDerechos := D_TAB_PER_MUNICIPIOS;
end;

procedure TEditMunicipios_DevEx.Connect;
begin
     with dmTablas do
     begin
          DataSource.DataSet := cdsMunicipios;
          TB_ENTIDAD.LookupDataset := cdsEstado;
     end;
end;

procedure TEditMunicipios_DevEx.DoLookup;
begin
     inherited;
     if dxBarButton_BuscarBtn.Enabled then
        ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', dmTablas.cdsMunicipios );
end;

procedure TEditMunicipios_DevEx.TB_ENTIDADValidKey(Sender: TObject);
begin
     inherited;
     dmTablas.cdsEstado.Locate('TB_CODIGO',TB_ENTIDAD.Llave,[]);
end;

end.
