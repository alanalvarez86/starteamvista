unit FTabOfiSatTipoContrato_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db,
  ExtCtrls, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, System.Actions;

type
  TTabOfiSatTipoContrato_DevEx = class(TBaseGridLectura_DevEx)
    TB_SAT_BAN: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
  public
    procedure DoLookup; override;
  end;

var
  TabOfiSatTipoContrato_DevEx: TTabOfiSatTipoContrato_DevEx;

implementation

uses dTablas, ZetaCommonClasses, ZetaBuscador_DevEx;

{$R *.DFM}

procedure TTabOfiSatTipoContrato_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H70077_ContratoSat;

     CanLookup := True;
end;

procedure TTabOfiSatTipoContrato_DevEx.Connect;
begin
     with dmTablas do
     begin
          cdsTipoContratoSat.Conectar;
          DataSource.DataSet:= cdsTipoContratoSat;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();

end;

procedure TTabOfiSatTipoContrato_DevEx.Refresh;
begin
     dmTablas.cdsTipoContratoSat.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TTabOfiSatTipoContrato_DevEx.Agregar;
begin
     dmTablas.cdsTipoContratoSat.Agregar;
end;

procedure TTabOfiSatTipoContrato_DevEx.Borrar;
begin
     dmTablas.cdsTipoContratoSat.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TTabOfiSatTipoContrato_DevEx.Modificar;
begin
     dmTablas.cdsTipoContratoSat.Modificar;
end;

procedure TTabOfiSatTipoContrato_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', dmTablas.cdsTipoContratoSat );
end;

procedure TTabOfiSatTipoContrato_DevEx.FormShow(Sender: TObject);
begin
CreacolumaSumatoria(ZetaDbGridDbTableView.Columns[0],0,'',skCount);
  inherited;

end;

end.
