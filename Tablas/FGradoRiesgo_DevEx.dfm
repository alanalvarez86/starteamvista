inherited Riesgo_DevEx: TRiesgo_DevEx
  Left = 253
  Caption = 'Grados de Riesgo'
  ClientHeight = 148
  ClientWidth = 394
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 394
    inherited Slider: TSplitter
      Left = 299
    end
    inherited ValorActivo1: TPanel
      Width = 283
      inherited textoValorActivo1: TLabel
        Width = 277
      end
    end
    inherited ValorActivo2: TPanel
      Left = 302
      Width = 92
      inherited textoValorActivo2: TLabel
        Width = 86
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 394
    Height = 129
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      OptionsData.CancelOnExit = True
      OptionsData.DeletingConfirmation = True
      OptionsView.Indicator = True
      object RI_CLASE: TcxGridDBColumn
        Caption = 'Clase'
        DataBinding.FieldName = 'RI_CLASE'
      end
      object RI_GRADO: TcxGridDBColumn
        Caption = 'Grado'
        DataBinding.FieldName = 'RI_GRADO'
        Options.Grouping = False
      end
      object RI_INDICE: TcxGridDBColumn
        Caption = 'Indice'
        DataBinding.FieldName = 'RI_INDICE'
        Options.Grouping = False
      end
      object RI_PRIMA: TcxGridDBColumn
        Caption = 'Prima'
        DataBinding.FieldName = 'RI_PRIMA'
        Options.Grouping = False
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 16
    Top = 88
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
