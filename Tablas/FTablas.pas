unit FTablas;

interface

uses Controls, ExtCtrls, Buttons, Dialogs,
     FEditPrioAhorrPresta,
     ZBaseDlgModal,     
     ZBaseTablasConsulta;

type
  TTOHabitacion = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEstadoCivil = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOViveCon = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOTransporte = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra1 = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra2 = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra3 = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra4 = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra5 = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra6 = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra7 = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra8 = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra9 = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra10 = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra11 = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra12 = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra13 = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOExtra14 = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEstado = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOTipoCursos = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEstudios = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOMovKardex = class(TTablaLookup)
  protected
{Protected declarations}
    procedure AfterCreate; override;
  end;
  TTONiveles = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
    procedure AnexarColumnaUsuario( iNivel : integer);
  end;
  TTONivel1 = class(TTONiveles)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTONivel2 = class(TTONiveles)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTONivel3 = class(TTONiveles)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTONivel4 = class(TTONiveles)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTONivel5 = class(TTONiveles)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTONivel6 = class(TTONiveles)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTONivel7 = class(TTONiveles)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTONivel8 = class(TTONiveles)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTONivel9 = class(TTONiveles)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  {$ifdef ACS}
  //Se agregaron 3 niveles del 10 al 12
  TTONivel10 = class(TTONiveles)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTONivel11 = class(TTONiveles)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTONivel12 = class(TTONiveles)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  {$endif}
  TTOIncidencias = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOMotivoBaja = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TAhorroPrestamo = class(TTablaLookup)
  private
    {Private declarations}
{$ifndef CAJAAHORRO}
    procedure FijarPrioridad(Sender: TObject);
  protected
    {Protected declarations}
    procedure AfterCreate; override;
{$endif}
  end;
  TTOTAhorro = class(TAhorroPrestamo)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOTPresta = class(TAhorroPrestamo)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOMotAuto = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOMotCheca = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOTallas = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOMotTool = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOClasifiCurso = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOColonia = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

  TTOOcupaNac = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

  TTOAreaTematicaCursos = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

  TTOMotivoTransfer = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

  TTOTCompetencias = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

  TTOTPerfiles = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

implementation

uses DTablas,
     DGlobal,
     ZetaBuscador,
     ZGlobalTress,
     ZetaCommonClasses;

{$R *.RES}

{ ****** TTOHabitacion ****** }

procedure TTOHabitacion.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsHabitacion;
     HelpContext := H70712_En_donde_vive;
end;

{ TTOEstadoCivil }

procedure TTOEstadoCivil.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsEstadoCivil;
     HelpContext := H70711_Estado_civil;
end;

{ TTOViveCon }

procedure TTOViveCon.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsViveCon;
     HelpContext := H70713_Con_quien_vive;
end;

{ TTOTransporte }

procedure TTOTransporte.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsTransporte;
     HelpContext := H70715_Transporte;
end;

{ TTOExtra1 }

procedure TTOExtra1.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra1;
     HelpContext := H70073_Adicionales;
end;

{ TTOExtra2 }

procedure TTOExtra2.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra2;
     HelpContext := H70073_Adicionales;
end;

{ TTOExtra3 }

procedure TTOExtra3.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra3;
     HelpContext := H70073_Adicionales;
end;

{ TTOExtra4 }

procedure TTOExtra4.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra4;
     HelpContext := H70073_Adicionales;
end;

{ TTOExtra5 }

procedure TTOExtra5.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra5;
     HelpContext := H70073_Adicionales;
end;

{ TTOExtra6 }

procedure TTOExtra6.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra6;
     HelpContext := H70073_Adicionales;
end;

{ TTOExtra7 }

procedure TTOExtra7.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra7;
     HelpContext := H70073_Adicionales;
end;

{ TTOExtra8 }

procedure TTOExtra8.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra8;
     HelpContext := H70073_Adicionales;
end;

{ TTOExtra9 }

procedure TTOExtra9.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra9;
     HelpContext := H70073_Adicionales;
end;

{ TTOExtra10 }

procedure TTOExtra10.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra10;
     HelpContext := H70073_Adicionales;
end;

{ TTOExtra11 }

procedure TTOExtra11.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra11;
     HelpContext := H70073_Adicionales;
end;

{ TTOExtra12 }

procedure TTOExtra12.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra12;
     HelpContext := H70073_Adicionales;
end;

{ TTOExtra13 }

procedure TTOExtra13.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra13;
     HelpContext := H70073_Adicionales;
end;

{ TTOExtra14 }

procedure TTOExtra14.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsExtra14;
     HelpContext := H70073_Adicionales;
end;

{ TTOEstado }

procedure TTOEstado.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsEstado;
     HelpContext := H70716_Estados_pais;
end;

{ TTOTipoCursos }

procedure TTOTipoCursos.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsTipoCursos;
     HelpContext := H70744_Tipo_curso;
end;

{ TTOEstudios }

procedure TTOEstudios.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsEstudios;
     HelpContext := H70714_Grado_estudios;
end;

{ TTOMovKardex }

procedure TTOMovKardex.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsMovKardex;
     HelpContext := H70741_Tipo_kardex;
end;

{ TTONiveles}
procedure TTONiveles.AfterCreate;

begin
     inherited AfterCreate;
     with DBGrid.Columns.Add do
     begin
          Expanded := False;
          FieldName := 'TB_SUB_CTA';
          Title.Caption := 'Subcuenta';
          Width := 250;
          Visible := True;
     end
end;

procedure TTONiveles.AnexarColumnaUsuario( iNivel : integer );
begin
{$ifndef DOS_CAPAS}
     if ( Global.GetGlobalInteger( K_GLOBAL_NIVEL_SUPERVISOR ) = iNivel ) and
        ( Global.GetGlobalBooleano( K_GLOBAL_ENROLAMIENTO_SUPERVISORES) ) then
     begin
        with DBGrid.Columns.Add do
        begin
             Expanded := False;
             FieldName := 'US_DESCRIP';
             Title.Caption := 'Usuario';
             Width := 200;
             Visible := True;
        end
     end;
{$endif}
end;

{ TTONivel1 }

procedure TTONivel1.AfterCreate;
begin
     inherited AfterCreate;
     AnexarColumnaUsuario( 1 );
     LookupDataset := dmTablas.cdsNivel1;
     HelpContext := H70072_Areas;

end;

{ TTONivel2 }

procedure TTONivel2.AfterCreate;
begin
     inherited AfterCreate;
     AnexarColumnaUsuario( 2 );
     LookupDataset := dmTablas.cdsNivel2;
     HelpContext := H70072_Areas;
end;

{ TTONivel3 }

procedure TTONivel3.AfterCreate;
begin
     inherited AfterCreate;
     AnexarColumnaUsuario( 3 );
     LookupDataset := dmTablas.cdsNivel3;
     HelpContext := H70072_Areas;
end;

{ TTONivel4 }

procedure TTONivel4.AfterCreate;
begin
     inherited AfterCreate;
     AnexarColumnaUsuario( 4 );
     LookupDataset := dmTablas.cdsNivel4;
     HelpContext := H70072_Areas;
end;

{ TTONivel5 }

procedure TTONivel5.AfterCreate;
begin
     inherited AfterCreate;
     AnexarColumnaUsuario( 5 );
     LookupDataset := dmTablas.cdsNivel5;
     HelpContext := H70072_Areas;
end;

{ TTONivel6 }

procedure TTONivel6.AfterCreate;
begin
     inherited AfterCreate;
     AnexarColumnaUsuario( 6 );
     LookupDataset := dmTablas.cdsNivel6;
     HelpContext := H70072_Areas;
end;

{ TTONivel7 }

procedure TTONivel7.AfterCreate;
begin
     inherited AfterCreate;
     AnexarColumnaUsuario( 7 );
     LookupDataset := dmTablas.cdsNivel7;
     HelpContext := H70072_Areas;
end;

{ TTONivel8 }

procedure TTONivel8.AfterCreate;
begin
     inherited AfterCreate;
     AnexarColumnaUsuario( 8 );
     LookupDataset := dmTablas.cdsNivel8;
     HelpContext := H70072_Areas;
end;

{ TTONivel9 }

procedure TTONivel9.AfterCreate;
begin
     inherited AfterCreate;
     AnexarColumnaUsuario( 9 );
     LookupDataset := dmTablas.cdsNivel9;
     HelpContext := H70072_Areas;
end;

{$ifdef ACS}
//Se agregaron 3 niveles del 10 al 12
{ TTONivel10 }

procedure TTONivel10.AfterCreate;
begin
     inherited AfterCreate;
     AnexarColumnaUsuario( 10 );
     LookupDataset := dmTablas.cdsNivel10;
     HelpContext := H70072_Areas;
end;

{ TTONivel11 }

procedure TTONivel11.AfterCreate;
begin
     inherited AfterCreate;
     AnexarColumnaUsuario( 11 );
     LookupDataset := dmTablas.cdsNivel11;
     HelpContext := H70072_Areas;
end;

{ TTONivel12 }

procedure TTONivel12.AfterCreate;
begin
     inherited AfterCreate;
     AnexarColumnaUsuario( 12 );
     LookupDataset := dmTablas.cdsNivel12;
     HelpContext := H70072_Areas;
end;
{$endif}
{ TTOIncidencias }

procedure TTOIncidencias.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsIncidencias;
     HelpContext := H70743_Incidencias;
end;

{ TTOMotivoBaja }

procedure TTOMotivoBaja.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsMotivoBaja;
     HelpContext := H70742_Motivo_baja;
end;

{ TAhorroPrestamo }

{$ifndef CAJAAHORRO}
procedure TAhorroPrestamo.AfterCreate;
const
     K_PANEL_HEIGHT = 30;
     K_BTN_TOP = 2;
     K_BTN_LEFT = 2;
     K_BTN_WIDTH = 100;
var
   Panel: TPanel;
   Boton: TBitBtn;
begin
     Panel := TPanel.Create( Self );
     with Panel do
     begin
          Parent := Self;
          Align := alTop;
          Height := K_PANEL_HEIGHT;
          BevelInner := bvNone;
          BevelOuter := bvNone;
     end;
     Boton := TBitBtn.Create( Self );
     with Boton do
     begin
          Parent := Panel;
          Height := Panel.Height - ( 2 * K_BTN_TOP );
          Width := K_BTN_WIDTH;
          Left := K_BTN_LEFT;
          Top := K_BTN_TOP;
          Caption := 'Fijar &Prioridad';
          Anchors := [ akTop, akLeft ];
          OnClick := FijarPrioridad;
          ShowHint := True;
          Hint := 'Fijar Prioridades de Ahorros y Préstamos';
          Glyph.LoadFromResourceName( SysInit.HInstance, 'PRIORIDADES' );
          NumGlyphs := 2;
     end;
     inherited AfterCreate;
end;

procedure TAhorroPrestamo.FijarPrioridad(Sender: TObject);
begin
     ShowDlgModal( EditPrioAhorrPresta, TEditPrioAhorrPresta );
end;
{$endif}

{ TTOTAhorro }

procedure TTOTAhorro.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsTAhorro;
     HelpContext := H70751_Tipo_ahorro;
end;

{ TTOTPresta }

procedure TTOTPresta.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsTPresta;
     {$ifdef CAJAAHORRO}
     HelpContext:= H_CAT_TIPO_PRESTAMO;
     {$else}
     HelpContext:= H70752_Tipo_prestamo;
     {$endif}
end;

{ TTOMotAuto }

procedure TTOMotAuto.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsMotAuto;
     HelpContext := H70745_Motivos_de_Autorizacion;
end;

{ TTOMotCheca }

procedure TTOMotCheca.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsMotCheca;
     HelpContext := H_Motivos_Checada_Manual;
end;

{ TTOTallas }

procedure TTOTallas.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsTallas;
     HelpContext := H60664_Tallas;
end;

{ TTOMotTool }

procedure TTOMotTool.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsMotTool;
     HelpContext := H60665_Motivos_Devolucion;
end;

{ TTOClasifiCurso }

procedure TTOClasifiCurso.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsClasifiCurso;
     HelpContext := H60646_Clase_de_cursos;
end;

{ TTOColonia }
procedure TTOColonia.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsColonia;
     HelpContext := H_Tabla_Colonias;
end;

{ TTOOcupaNac }
procedure TTOOcupaNac.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsOcupaNac;
     HelpContext := H_TABLA_OCUPACIONES_NACIONALES;
end;

{ TTOAreaTematicaCursos }
procedure TTOAreaTematicaCursos.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsAreaTemCur;
     HelpContext := H_TABLA_AREA_TEMATICA_CURSOS;
end;

procedure TTOMotivoTransfer.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsMotivoTransfer;
     HelpContext := H_Tabla_Motivo_Transferencia;
end;


{ TTOTPerfiles }

procedure TTOTPerfiles.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsTPerfiles;
     HelpContext := H_Tabla_TiposPerfiles;
end;

{ TTOTCompetencias }

procedure TTOTCompetencias.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmTablas.cdsTCompetencias;
     HelpContext := H_Tabla_TiposCompetencias;
end;

end.
