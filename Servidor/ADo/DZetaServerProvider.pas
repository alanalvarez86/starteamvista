unit DZetaServerProvider;

{$define PROFILE_DEBUG}

interface

{$DEFINE NEWCOMPARTE}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Db, Provider, DBClient, ADODB, ZGlobalTress, ZToolsPE,
  Variants, ZetaCommonClasses, ZetaCommonLists, ZetaRegistryServer, ZetaServerDataSet;

{$DEFINE QUINCENALES}
{ .$undefine QUINCENALES }

type
  TzProviderClientDataset   = TCustomClientDataset;
  TzProviderDataEvent       = procedure(Sender: TObject; DataSet: TzProviderClientDataset) of object;
  TzBeforeUpdateRecordEvent = procedure(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataset;
    UpdateKind: TUpdateKind; var Applied: Boolean) of object;
  TzAfterUpdateRecordEvent = procedure(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataset;
    UpdateKind: TUpdateKind) of object;
  TzResolverErrorEvent = procedure(Sender: TObject; DataSet: TzProviderClientDataset; E: EUpdateError; UpdateKind: TUpdateKind;
    var Response: TResolverResponse) of object;

  TQREvResultType = (resInt, resDouble, resString, resBool, resError);
  { .$IFDEF RETROACTIVO }
  eTipoEvaluacion = (teNormal, teRetroactiva);
  { .$ENDIF }

  TQREvResult = record
    resFecha: Boolean;
    case Kind: TQREvResultType of
      resInt:
        (intResult: longint);
      resDouble:
        (dblResult: double);
      resString:
        (strResult: string[255]);
      resBool:
        (booResult: Boolean);
  end;

  TZFixedCharField = class(TStringField)
  protected
    function GetAsString: string; override;
  end;

  TZADOQuery = class(TADOQuery)
  protected
    function GetFieldClass(FieldType: TFieldType): TFieldClass; override;
  public
  end;

  TZetaCursor       = TDataSet;
  TZetaCursorLocate = TDataSet;
  TZetaField        = TField;
  TZetaCursorParam  = ADODB.TParameter;
  TZetaComando      = TZADOQuery;
  TParameter        = ADODB.TParameter;

  TZPUpdate             = TZADOQuery;
  eTipoQuery            = (etqSelect, etqInsert, etqUpdate, etqDelete);
  EAdvertencia          = class(Exception); // Excepciones que se deben manejar como advertencias //
  TBitacoraHora         = string[8];
  TBitacoraTexto        = string[50];
  TLogStatus            = string[1];
  TdmZetaServerProvider = class;

  TZetaLog = class(TObject)
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    FMaxSteps    : Integer;
    FIncrement   : Integer;
    FSteps       : Integer;
    FCounter     : Integer;
    FCancelado   : Boolean;
    FErrorCount  : Word;
    FEventCount  : Word;
    FWarningCount: Word;
    FFolio       : Integer;
    FInicio      : TDateTime;
    FEmpleado    : TNumEmp;
    FProceso     : Procesos;
    FDataset     : TDataSet;
    FUpdate      : TDataSet;
    function Abierto: Boolean;
    procedure Init;
    procedure Conteo(const eTipo: eTipoBitacora);
    procedure Escribir(const eTipo: eTipoBitacora; const eClase: eClaseBitacora; const iEmpleado: TNumEmp;
      const dMovimiento: TDate; const sMensaje: TBitacoraTexto; const sTexto: string);
    function GetStatus: TLogStatus;
    function GetEnumStatus: eProcessStatus;
  public
    { Public declarations }
    property Folio: Integer read FFolio;
    constructor Create(oProvider: TdmZetaServerProvider);
    function CanContinue(const iEmpleado: TNumEmp; const lTx: Boolean): Boolean;
    function CloseProcess: OleVariant;
    function OpenProcess(const eProceso: Procesos; const iMaxSteps: Integer; const sParametros: string = VACIO;
      const sFiltro: string = VACIO; const sFormula: string = VACIO): Boolean;
    procedure Cambio(const sMensaje: TBitacoraTexto; const eClase: eClaseBitacora; const sCampoEmpleado, sCampoFecha: string;
      DataSet: TDataSet);
    procedure CancelProcess(const iFolio: Integer);
    procedure Evento(const eClase: eClaseBitacora; const iEmpleado: TNumEmp; const dMovimiento: TDate;
      const sMensaje: TBitacoraTexto); overload;
    procedure Evento(const eClase: eClaseBitacora; const iEmpleado: TNumEmp; const dMovimiento: TDate;
      const sMensaje: TBitacoraTexto; const sTexto: string); overload;
    procedure Advertencia(const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto); overload;
    procedure Advertencia(const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto; const sTexto: string); overload;
    procedure Advertencia(const eClase: eClaseBitacora; const iEmpleado: TNumEmp; const dMovimiento: TDate;
      const sMensaje: TBitacoraTexto; const sTexto: string); overload;
    procedure Error(const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto; const sTexto: string);
    procedure ErrorGrave(const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto);
    procedure Excepcion(const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto; Problema: Exception); overload;
    procedure Excepcion(const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto; Problema: Exception;
      const sTexto: string); overload;
    function HayErrores: Boolean;
    procedure HuboRollBack;
    procedure PreparaQuerys;
    procedure BorraQuerys;
  end;

  TTablaInfo = class(TObject)
  private
    FTabla             : string;
    FListaCampos       : TStringList;
    FListaKey          : TStringList;
    FListaOrder        : TStringList;
    FListaForeignKey   : TStringList;
    FSQL               : TStrings;
    FParams            : TParams;
    FFiltro            : string;
    FOnUpdateData      : TzProviderDataEvent;
    FBeforeUpdateRecord: TzBeforeUpdateRecordEvent;
    FAfterUpdateRecord : TzAfterUpdateRecordEvent;
    FOnUpdateError     : TzResolverErrorEvent;
    function ConstruyeQuery(const eTipo: eTipoQuery; Delta: TDataSet; qryCambios: TADOCommand): string;
    function ConstruyeSelectSQL: string;
    function ConstruyeUpdateSQL(const UpdateKind: TUpdateKind; Delta: TDataSet; qryCambios: TADOCommand): string;
  public
    constructor Create;
    destructor Destroy; override;
    procedure SetCampos(const sCampos: string);
    procedure SetKey(const sCampos: string);
    procedure SetOrder(const sCampos: string);
    procedure SetForeignKey(const sCampos: string);
    procedure SetInfo(const sTabla, sCampos, sKey: string);
    procedure SetInfoDetail(const sTabla, sCampos, sKey, sForeignKey: string);
    procedure SetMasterFields(Detalle: TADOTable);
    property Filtro: string read FFiltro write FFiltro;
    property OnUpdateData: TzProviderDataEvent read FOnUpdateData write FOnUpdateData;
    property BeforeUpdateRecord: TzBeforeUpdateRecordEvent read FBeforeUpdateRecord write FBeforeUpdateRecord;
    property AfterUpdateRecord: TzAfterUpdateRecordEvent read FAfterUpdateRecord write FAfterUpdateRecord;
    property OnUpdateError: TzResolverErrorEvent read FOnUpdateError write FOnUpdateError;
    property SQL: TStrings read FSQL;
    property Tabla:string read FTabla;
  end;

  TdmZetaServerProvider = class(TDataModule)
    prvUnico: TDataSetProvider;
    dsMaster: TDataSource;
    adoEmpresa: TADOConnection;
    adoComparte: TADOConnection;
    qryMaster: TADODataSet;
    qryDetail: TADOTable;
    qryCambios: TADOCommand;
    procedure dmZetaProviderCreate(Sender: TObject);
    procedure dmZetaProviderDestroy(Sender: TObject);
    procedure prvUnicoUpdateData(Sender: TObject; DataSet: TCustomClientDataset);
    procedure prvUnicoBeforeUpdateRecord(Sender: TObject; SourceDS: TDataSet; DeltaDS: TCustomClientDataset;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure prvUnicoGetData(Sender: TObject; DataSet: TCustomClientDataset);
    procedure prvUnicoAfterUpdateRecord(Sender: TObject; SourceDS: TDataSet; DeltaDS: TCustomClientDataset;
      UpdateKind: TUpdateKind);
    procedure prvUnicoUpdateError(Sender: TObject; DataSet: TCustomClientDataset; E: EUpdateError; UpdateKind: TUpdateKind;
      var Response: TResolverResponse);
    function GetCodigoGrupo: Integer;
    procedure qryMasterAfterOpen(DataSet: TDataSet);
    procedure adoEmpresaExecuteComplete(Connection: TADOConnection; RecordsAffected: Integer; const Error: Error;
      var EventStatus: TEventStatus; const Command: _Command; const Recordset: _Recordset);
    procedure adoComparteExecuteComplete(Connection: TADOConnection; RecordsAffected: Integer; const Error: Error;
      var EventStatus: TEventStatus; const Command: _Command; const Recordset: _Recordset);
  private
    { Private declarations }
    FTablaInfo : TTablaInfo;
    FDetailInfo: TTablaInfo;
    {$IFDEF NEWCOMPARTE}
    FComparte         : Variant;
    FComparteAlias    : string;
    FSQLClientProvider: string;
    FRegistryOpened   : Boolean;
    {$ELSE}
    FRegistry: TZetaRegistryServer;
    {$ENDIF}

    //Conecction Strings de Backup para la reconexion de componente
    FConnectionString_Empresa : string;
    FConnectionString_Comparte : string;

    FRecsOut    : Integer;
    FLog        : TZetaLog;
    FQueryGlobal: TZADOQuery;
    FEmpresa    : Variant;
    FConnection : TADOConnection;
    FParamList  : TZetaParams;
    // oQryInsert : TDataSet;
    FQryBitacora       : TDataSet;
    FQryBitacoraSistema: TDataSet;
    FQryBitacoraKiosco : TDataSet;
    { Valores Activos }
    FDatosPeriodo   : TDatosPeriodo;
    FPeriodoObtenido: Boolean;
    FDatosImss      : TDatosImss;
    FYearDefault    : Integer;
    FMesDefault     : Integer;
    FEmpleadoActivo : Integer;
    FNombreUsuario  : string;
    FCodigoEmpresa  : string;
    FFechaDefault   : TDate;
    FFechaAsistencia: TDate;
    FVerConfidencial: Boolean;
    FOptimizeCursor : Boolean;
    FOptimizeCursorB8 : Boolean;
    FNumeroNiveles  : Integer;
    FNombreNiveles  : array [1 .. {$IFDEF ACS}12{$ELSE}9{$ENDIF} ] of string;
    { ACSL: No se cambio a la constante global K_GLOBAL_NIVEL_MAX ya que esta no es encuentra en todos los ZGlobalTress }
    FChecadaComida     : TDatosComida;
    FDatosCalendario   : TCalendario;
    FDatosPrestamo     : TDatosPrestamo;
    FDatosNOMM         : TDatosNomm;
    FUltimaFechaBalanza: TDate;
    FEmpleadosConAjuste: TObject;

    { Valores por Nomina de la Funci�n  V_NOM_INFO }
    FCdsPeriodosNomInfo: TClientDataset;
    FDatosNomInfo      : TDatosNomInfo;
    { .$IFDEF RETROACTIVO }
    FEvaluacionTipo: eTipoEvaluacion;
    { .$ENDIF }
    FOnUpdateData      : TzProviderDataEvent;
    FBeforeUpdateRecord: TzBeforeUpdateRecordEvent;
    FAfterUpdateRecord : TzAfterUpdateRecordEvent;
    FOnUpdateError     : TzResolverErrorEvent;
    function ConectaBaseDeDatos(const Empresa: Variant): TADOConnection;
    function GetParametroPos(DataSet: TZADOQuery; const iPos: Integer): TParameter;
    procedure CreateLog;
    procedure ReleaseLog;
    procedure PreparaDetail;
    procedure PreparaTabla;
    { Grabar Tablas con Joins }
    procedure UpdateTabla(Sender: TObject; SourceDS: TDataSet; DeltaDS: TCustomClientDataset; UpdateKind: TUpdateKind;
      var Applied: Boolean);
    procedure ConectaEmpresa(const Empresa: Variant);
    { Globales }
    function LeeGlobal(const iCodigo: Integer): string;
    { Empresas }
    function GetComparte: Variant;
    procedure SetEmpresa(Empresa: Variant);
    function GetUsuario: Integer;
    function GetCodigoEmpresaActiva: string;
    function GetApplicationID: Integer;
    function GetSQLCLINProvider: string;
    function  EmpresaWF(const Empresa: Variant): Boolean;
    {$IFDEF NEWCOMPARTE}
    procedure RegistryInit;
    procedure RegistryRead;
    {$ENDIF}
    procedure EscribeParametrosBitacora(oQryInsert: TDataSet; const iTipo: Integer; const iClase: Integer;
      const iEmpleado: TNumEmp; dMovimiento: TDate; const sMensaje: TBitacoraTexto; const sTexto: string;
      const iUsuario: Integer);
    procedure SetOptimizeCursor(Value: Boolean);
    procedure SetOptimizeCursorB8(Value: Boolean);
  public
    { Public declarations }
    function GetDatasetInfo(DataSet: TDataSet; const lBorrar: Boolean; sConfidenciales: string = VACIO): string;
    function GetInsertScript(const sTabla, sExcluidos: string; Source: TZetaCursor): string;
    function GetMasterDetail(Empresa: Variant): OleVariant;
    function GetTabla(Empresa: Variant): OleVariant;
    function GrabaMasterDetail(Empresa: Variant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant;
    function GrabaTabla(Empresa: Variant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant;
    function GrabaTablaGrid(Empresa: Variant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant;
    procedure ExecSQL(Empresa: Variant; const sSQL: string);
    function OpenSQL(Empresa: Variant; const sSQL: string; const lDatos: Boolean): OleVariant; overload;
    function OpenSQL(Empresa: Variant; const sSQL: string; const nRecords: Integer): OleVariant; overload;
    { Master y Detail }
    property TablaInfo: TTablaInfo read FTablaInfo;
    property DetailInfo: TTablaInfo read FDetailInfo;
    property RecsOut: Integer read FRecsOut;
    { Bitacora }
    property Log: TZetaLog read FLog;
    function OpenProcess(const eProceso: Procesos; const iMaxSteps: Integer; sParametros: string = VACIO;
      sFormula: string = VACIO): Boolean;
    function CanContinue: Boolean; overload;
    function CanContinue(const iEmpleado: TNumEmp): Boolean; overload;
    function CanContinue(const iEmpleado: TNumEmp; const lTx: Boolean): Boolean; overload;
    function CloseProcess: OleVariant;
    procedure AbreBitacora;
    procedure CancelProcess(const iFolio: Integer);
    { F�brica de Querys }
    function CreateQuery: TDataSet; overload;
    function CreateQuery(const sScript: string): TDataSet; overload;
    function CreateStoredProc(const sProcName: string): TDataSet;
    function CreateQueryLocate: TDataSet;
    procedure PreparaQuery(Query: TDataSet; sScript: string);
    procedure PreparaStoredProc(Query: TDataSet; const sProcName: string);
    function Ejecuta(DataSet: TDataSet): Integer;
    function EjecutaAndFree(const sSQL: string): Integer;
    function AbreQueryScript(DataSet: TDataSet; const sScript: string): Boolean;
    procedure ParamAsInteger(DataSet: TDataSet; const sName: string; const iValue: Integer); overload;
    function ParamAsInteger(DataSet: TDataSet; const sName: string): Integer; overload;
    procedure ParamAsFloat(DataSet: TDataSet; const sName: string; const rValue: Extended); overload;
    function ParamAsFloat(DataSet: TDataSet; const sName: string): Extended; overload;
    procedure ParamAsDate(DataSet: TDataSet; const sName: string; const dValue: TDate); overload;
    function ParamAsDate(DataSet: TDataSet; const sName: string): TDate; overload;
    procedure ParamAsDateTime(DataSet: TDataSet; const sName: string; const dValue: TDateTime); overload;
    function ParamAsDateTime(DataSet: TDataSet; const sName: string): TDateTime; overload;
    procedure ParamAsString(DataSet: TDataSet; const sName, sValue: string); overload;
    function ParamAsString(DataSet: TDataSet; const sName: string): string; overload;
    procedure ParamAsVarChar(DataSet: TDataSet; const sName, sValue: string; const iAncho: Integer); overload;
    procedure ParamAsChar(DataSet: TDataSet; const sName, sValue: string; const iAncho: Integer); overload;
    procedure ParamAsBlob(DataSet: TDataSet; const sName: string; const sValue: string); overload;
    procedure ParamAsBoolean(DataSet: TDataSet; const sName: string; const lValue: Boolean); overload;
    function ParamAsBoolean(DataSet: TDataSet; const sName: string): Boolean; overload;
    procedure ParamVariant(DataSet: TDataSet; const iPos: Integer; const Valor: Variant); overload;
    function ParamVariant(DataSet: TDataSet; const iPos: Integer): Variant; overload;
    procedure ParamAsNull(DataSet: TDataSet; const sName: string);
    procedure ParamAsGUID(DataSet: TDataSet; const sName: string; const Valor: TGUID); overload;
    function GetParametro(DataSet: TDataSet; const sName: string): TParameter;
    procedure ParamSalida(DataSet: TDataSet; const sName: string);
    // function  Params( DataSet : TDataset ) : TParams;
    { GetGlobales }
    procedure InitGlobales;
    function GetGlobalBooleano(const iCodigo: Integer): Boolean;
    function GetGlobalString(const iCodigo: Integer): string;
    function GetGlobalInteger(const iCodigo: Integer): Integer;
    function GetGlobalReal(const iCodigo: Integer): Real;
    function GetGlobalDate(const iCodigo: Integer): TDateTime;
    function NombreNivel(const Index: Integer): string;
    function NumNiveles: Integer;
    function OK_ProyectoEspecial(const eProgEspecial: eProyectoEspecial): Boolean;

    { Empresas }
    property Comparte: Variant read GetComparte;
    property EmpresaActiva: Variant read FEmpresa write SetEmpresa;
    property UsuarioActivo: Integer read GetUsuario;
    property CodigoEmpresaActiva: string read GetCodigoEmpresaActiva;
    property ParamList: TZetaParams read FParamList;
    property CodigoGrupo: Integer read GetCodigoGrupo;
    property ApplicationID: Integer read GetApplicationID;
    function AliasComparte: string;
    procedure Activate;
    procedure Deactivate;
    procedure AsignaDataSetParams(oDataSet: TDataSet);
    procedure AsignaDataSetOtros(oDataSet: TDataSet; oListaPares: Variant);
    procedure AsignaParamsDataSet(Origen, Destino: TDataSet; const sExcluidos: string = '');
    { Valores Activos }
    property DatosPeriodo: TDatosPeriodo read FDatosPeriodo;
    property DatosImss: TDatosImss read FDatosImss;
    property DatosComida: TDatosComida read FChecadaComida;
    property DatosPrestamo: TDatosPrestamo read FDatosPrestamo write FDatosPrestamo;
    property DatosCalendario: TCalendario read FDatosCalendario;
    property DatosNomInfo: TDatosNomInfo read FDatosNomInfo write FDatosNomInfo;

    property DatosNOMM: TDatosNomm read FDatosNOMM write FDatosNOMM;
    property FechaAsistencia: TDate read FFechaAsistencia;
    property FechaDefault: TDate read FFechaDefault;
    property YearDefault: Integer read FYearDefault write FYearDefault;
    property MesDefault: Integer read FMesDefault write FMesDefault;
    property EmpleadoActivo: Integer read FEmpleadoActivo;
    property NombreUsuario: string read FNombreUsuario;
    property CodigoEmpresa: string read FCodigoEmpresa;
    property VerConfidencial: Boolean read FVerConfidencial write FVerConfidencial;
    property OptimizeCursor: Boolean read FOptimizeCursor write SetOptimizeCursor;
    property OptimizeCursorB8: Boolean read FOptimizeCursorB8 write SetOptimizeCursorB8;
    { .$IFDEF RETROACTIVO }
    property EvaluacionTipo: eTipoEvaluacion read FEvaluacionTipo write FEvaluacionTipo default teNormal;
    { .$ENDIF }

    property UltimaFechaBalanza: TDate read FUltimaFechaBalanza write FUltimaFechaBalanza;

    { Bloque de propiedades para optimizacion de V_NOM_INFO }
    property PeriodosNomInfo: TClientDataset read FCdsPeriodosNomInfo write FCdsPeriodosNomInfo;
    property EmpleadosConAjuste: TObject read FEmpleadosConAjuste write FEmpleadosConAjuste;
    procedure InicializaValoresNomINFO;
    procedure LiberaEmpleadosConAjuste;

    procedure GetDatosPeriodo;
    procedure GetDatosImss;
    procedure GetDatosActivos;
    procedure GetEncabPeriodo;

    procedure InicializaValoresActivos;
    procedure AsignaParamList(oParams: OleVariant);
    function DescripcionParams: string;

    procedure EmpiezaTransaccion;
    procedure TerminaTransaccion(lCommit: Boolean);
    procedure RollBackTransaccion;

    procedure ActualizaTabla(OrigenDS: TDataSet; UpdateKind: TUpdateKind);
    procedure CambioCatalogo(const sMensaje: TBitacoraTexto; const eClase: eClaseBitacora; DataSet: TDataSet;
      const iEmpleado: Integer = 0; const sMasInfo: string = VACIO);
    procedure BorraCatalogo(const sMensaje: TBitacoraTexto; const eClase: eClaseBitacora; DataSet: TDataSet;
      const iEmpleado: Integer = 0; const sMasInfo: string = VACIO);
    procedure EscribeBitacora(const eTipo: eTipoBitacora; const eClase: eClaseBitacora; const iEmpleado: TNumEmp;
      dMovimiento: TDate; const sMensaje: TBitacoraTexto; const sTexto: string);
    property OnUpdateData: TzProviderDataEvent read FOnUpdateData write FOnUpdateData;
    property BeforeUpdateRecord: TzBeforeUpdateRecordEvent read FBeforeUpdateRecord write FBeforeUpdateRecord;
    property AfterUpdateRecord: TzAfterUpdateRecordEvent read FAfterUpdateRecord write FAfterUpdateRecord;
    property OnUpdateError: TzResolverErrorEvent read FOnUpdateError write FOnUpdateError;
    procedure CambioCatalogoComparte(const sMensaje: TBitacoraTexto; const eClase: eClaseSistBitacora; DataSet: TDataSet;
      const iUsuario: Integer);
    procedure BorraCatalogoComparte(const sMensaje: TBitacoraTexto; const eClase: eClaseSistBitacora; DataSet: TDataSet;
      const iUsuario: Integer);
    procedure EscribeBitacoraComparte(const eTipo: eTipoBitacora; const eClase: eClaseSistBitacora; dMovimiento: TDate;
      const sMensaje: TBitacoraTexto; const sTexto: string; const iUsuario: Integer);

    procedure EscribeBitacoraKiosco(const eTipo: eTipoBitacora; const sEmpresa: string; const eClase: eClaseBitacora;
      const sTexto, sMarco, sKiosco: string; const iEmpleado: Integer);

    { Arreglos variables de aTipoPeriodo y aTipoNomina }
    function GetTiposPeriodo(Empresa: Variant): OleVariant; // acl
    procedure InitArregloTPeriodo; // ap
    procedure SetReadUncommited;
  end;

  TParameterHelper = class helper for ADODB.TParameter
    function AsInteger: Integer;
    function AsDate: TDate;
    function AsDateTime: TDateTime;
    function AsString: string;
    function AsBoolean: Boolean;
    function AsSingle: Single;
    function AsFloat: double;
    function AsByte: Integer;
    function AsSmallInt: Integer;
    function AsShortInt: Integer;
    function AsBCD: Currency;
    function AsCurrency: Currency;
  end;

function PK_Violation(E: Exception): Boolean;
function GetEmptyProcessResult(eProceso: Procesos): OleVariant;
function GetEmptyProcessResultFecha(eProceso: Procesos): OleVariant;
procedure SumaProcessResult(var Result: OleVariant; Process1, Process2: OleVariant);
procedure SetProcessOK(var Resultado: OleVariant);
procedure SetProcessError(var Resultado: OleVariant);
function CursorDelCampo(oCampo: TZetaField): TZetaCursor;
function TipoCampo(oCampo: TField): eTipoGlobal;
function AsignaResultado(oCampo: TField): TQREvResult;

function GetZetaProvider(AOwner: TComponent): TdmZetaServerProvider;
procedure FreeZetaProvider(oProvider: TdmZetaServerProvider);
{$IFDEF DOS_CAPAS}
procedure FreeAll;
procedure InitAll;
{$ENDIF}

implementation

uses
  ZetaCommonTools, ZetaServerTools;

const
  K_ANCHO_LOG_HORA   = 8;
  K_ANCHO_LOG_TEXTO  = 50;
  K_WRITE_SIMPLE_LOG =
    'insert into BITACORA (US_CODIGO, BI_FECHA, BI_HORA, BI_TIPO, BI_CLASE, BI_TEXTO, CB_CODIGO, BI_FEC_MOV, BI_DATA ) values ' +
    '( :US_CODIGO, :BI_FECHA, :BI_HORA, :BI_TIPO, :BI_CLASE, :BI_TEXTO, :CB_CODIGO, :BI_FEC_MOV, :BI_DATA )';

  K_AGREGA_BITACORA_KIOSCO =
    'execute dbo.SP_AGREGA_BITACORA_KIOSCO :CB_CODIGO, :CM_CODIGO, :BI_TIPO, :BI_ACCION, :BI_NUMERO, :BI_TEXTO, ' +
    ':BI_UBICA, :BI_KIOSCO, :BI_FEC_MOV ';

  K_TIMEOUT = 60 * 30; // 60 seg * #Minutos
  K_TIMEOUT_EXT = 60 * 600; // 60 seg * #Minutos
  K_CACHESIZE = 1000;

{CONSTANTES DE STATUS NATIVO DE SQL SERVER }
  SQLSTATE_GENERAL_WARNING = '1000';  { Driver-specific informational message. (Function returns SQL_SUCCESS_WITH_INFO.)}
  SQLSTATE_STRING_TRUNCATED = '1004';  { The buffer *OutStatementText�was not large enough to return the entire SQL string, so the SQL string was truncated. The length of the untruncated SQL string is returned in *TextLength2Ptr. (Function returns SQL_SUCCESS_WITH_INFO.)}
  SQLSTATE_CONNECTION_NOT_OPEN = '8003';  { The�ConnectionHandle�was not in a connected state.}
  SQLSTATE_COMMUNICATION_LINK_FAILURE = '08S01';  { The communication link between the driver and the data source to which the driver was connected failed before the function completed processing.}
  SQLSTATE_INVALID_DATETIME_FORMAT = '22007';  { *InStatementText�contained an escape clause with an invalid date, time, or timestamp value.}
  SQLSTATE_INVALID_CURSOR_STATE = '24000';  { The cursor referred to in the statement was positioned before the start of the result set or after the end of the result set. This error may not be returned by a driver having a native DBMS cursor implementation.}
  SQLSTATE_GENERAL_ERROR = 'HY000';  { An error occurred for which there was no specific SQLSTATE and for which no implementation-specific SQLSTATE was defined. The error message returned bySQLGetDiagRec�in the�*MessageText�buffer describes the error and its cause.}
  SQLSTATE_MEMORY_ALLOC_ERROR = 'HY001';  { The driver was unable to allocate memory required to support execution or completion of the function.}
  SQLSTATE_INVALID_NULL_POINTER = 'HY009';  { (DM) *InStatementText�was a null pointer.}
  SQLSTATE_FUNCTION_SEQ_ERROR = 'HY010';  { (DM) An asynchronously executing function was called for theConnectionHandle�and was still executing when this function was called.}
  SQLSTATE_MEMORY_MANAGE_ERROR = 'HY013';  { The function call could not be processed because the underlying memory objects could not be accessed, possibly because of low memory conditions.}
  SQLSTATE_INVALID_STRING_BUFFER_LENGTH = 'HY090';  { (DM) The argument�TextLength1�was less than 0, but not equal to SQL_NTS.}
                                                    { (DM) The argument�BufferLength�was less than 0 and the argumentOutStatementText�was not a null pointer.}
  SQLSTATE_INVALID_CURSOR_POSITION = 'HY109';  { The current row of the cursor had been deleted or had not been fetched. This error may not be returned by a driver having a native DBMS cursor implementation.}
  SQLSTATE_CONNECTION_SUSPENDED_TRANSACTION_STATE = 'HY117';  { (DM) For more information about suspended state, see�SQLEndTran Function.}
  SQLSTATE_CONNECTION_TIMEOUT_EXPIRED = 'HYT01';  { The connection timeout period expired before the data source responded to the request. The connection timeout period is set through�SQLSetConnectAttr, SQL_ATTR_CONNECTION_TIMEOUT.}
  SQLSTATE_DRIVER_NOT_SUPPORT_FUNCTION = 'IM001';  { (DM) The driver associated with the�ConnectionHandle�does not support the function.}


  {$IFDEF DOS_CAPAS}

var
  oZetaProvider: TdmZetaServerProvider;
  {$ENDIF}
  {$R *.DFM}

function GetZetaProvider(AOwner: TComponent): TdmZetaServerProvider;
begin
     {$IFDEF DOS_CAPAS}
     Result := oZetaProvider;
     {$ELSE}
     Result := TdmZetaServerProvider.Create(AOwner);
     {$ENDIF}
end;

procedure FreeZetaProvider(oProvider: TdmZetaServerProvider);
begin
     {$IFNDEF DOS_CAPAS}
     oProvider.Free;
     {$ENDIF}
end;

{$IFDEF DOS_CAPAS}

procedure InitAll;
begin
     oZetaProvider := TdmZetaServerProvider.Create(nil);
end;

procedure FreeAll;
begin
     FreeAndNil(oZetaProvider);
end;
{$ENDIF}

function TipoCampo(oCampo: TField): eTipoGlobal;
begin
     case oCampo.DataType of
       ftString, ftMemo, ftBlob, ftFmtMemo:
         Result := tgTexto;
       ftSmallint, ftInteger, ftWord, ftAutoInc:
         Result := tgNumero;
       ftBoolean:
         Result := tgBooleano;
       ftFloat, ftCurrency, ftBCD, ftFMTBcd, ftTime:
         Result := tgFloat;
       ftDate, ftDateTime:
         Result := tgFecha;
       else
         Result := tgAutomatico;
     end;
end;

function AsignaResultado(oCampo: TField): TQREvResult;
begin
     with oCampo, Result do
     begin
          case TipoCampo(oCampo) of
            tgTexto: begin
                Kind      := resString;
                strResult := AsString;
              end;
            tgNumero: begin
                Kind      := resInt;
                intResult := AsInteger;
              end;
            tgBooleano: begin
                Kind      := resBool;
                booResult := AsBoolean;
              end;
            tgFloat: begin
                Kind      := resDouble;
                resFecha  := FALSE;
                dblResult := AsFloat;
              end;
            tgFecha: begin
                Kind      := resDouble;
                resFecha  := TRUE;
                dblResult := AsFloat;
              end
            else
              Kind := resError;
          end;
     end;
end;

function CursorDelCampo(oCampo: TZetaField): TZetaCursor;
begin
     Result := oCampo.DataSet;
end;

function TimeToStrSQL(const dValue: TDateTime): string;
begin
     Result := FormatDateTime('hh:nn:ss', dValue);
end;

function PK_Violation(E: Exception): Boolean;
begin
     Result := (Pos('VIOLATION OF PRIMARY', UpperCase(E.Message)) > 0);
end;

{$ifdef FALSE}
          { Lista de campos memo e image de Sistema TRESS }
          if  // Campos Memo TRESS
             ( oCampo.FieldName = 'CB_NOTA' {15} ) or  // KARDEX Notas de Kardex
             ( oCampo.FieldName = 'NP_FORMULA' {16} ) or  // NOMPARAM - Formula de param�tros de N�mina
             ( oCampo.FieldName = 'NO_RASTREO' ) or   // NOMINA - Rastreo de N�mina
             ( oCampo.FieldName = 'CU_TEXTO1' {16} ) or ( oCampo.FieldName = 'CU_TEXTO2' ) or  // CURSO - Detalle de cursos
             ( oCampo.FieldName = 'CO_NOTA' {16} ) or // CONCEPTO - Notas de Cat. de Conceptos
             ( oCampo.FieldName = 'BI_DATA'  ) or   // BITACORA - Notas de Bit�cora
             ( oCampo.FieldName = 'CB_NOTA'  ) or  // TMPSALAR - ???
             ( oCampo.FieldName = 'NP_DETALLE'  ) or  // NIV_PTO - ???
             ( oCampo.FieldName = 'CM_DETALLE'  ) or  // COMPETEN - ???
             ( oCampo.FieldName = 'AN_DETALLE'  ) or  // ACCION - ???
             ( oCampo.FieldName = 'ND_DESCRIP'  ) or  // NIV_DIME - ???
             ( oCampo.FieldName = 'TD_ORIG'  ) or  // TMPDIMM - ???
             ( oCampo.FieldName = 'RV_DETALLE'  ) or  // RESERVA - Reservaciones de Cursos
             ( oCampo.FieldName = 'CI_DETALLE'  ) or  // CERTIFIC - Cat�logo de Certificaciones
             ( oCampo.FieldName = 'KI_OBSERVA'  ) or  // KAR_CERT - Kardex de Certificaciones
             ( oCampo.FieldName = 'PF_DESCRIP'  ) or ( oCampo.FieldName = 'PF_OBJETIV'  ) or ( oCampo.FieldName = 'PF_POSTING'  ) or // PERFIL - Perfil de Puestos
             ( oCampo.FieldName = 'DP_MEMO_01'  ) or ( oCampo.FieldName = 'DP_MEMO_02'  ) or ( oCampo.FieldName = 'DP_MEMO_03'  ) or // DESC_PTO - Descripci�n de puestos
             ( oCampo.FieldName = 'DF_VALORES'  ) or  // DESC_FLD - ???
             ( oCampo.FieldName = 'VL_COMENTA'  ) or  // VALPLANT - Valuaci�n de puestos - Plantilla
             ( oCampo.FieldName = 'VF_DESCRIP'  ) or  ( oCampo.FieldName = 'VF_DES_ING'  ) or // VFACTOR - Valuaci�n de puestos - Factores
             ( oCampo.FieldName = 'VS_DESCRIP'  ) or  ( oCampo.FieldName = 'VS_DES_ING'  ) or // VSUBFACT - Valuaci�n de puestos - Sub-Factores
             ( oCampo.FieldName = 'VN_DESCRIP'  ) or  ( oCampo.FieldName = 'VN_DES_ING'  ) or // VALNIVEL - Valuaci�n de puestos - Niveles
             ( oCampo.FieldName = 'VP_COMENTA'  ) or  // VAL_PTO - Valuaci�n de puestos - Puesto
             ( oCampo.FieldName = 'VT_COMENTA'  ) or  // VPUNTOS - Valuaci�n de puestos - Puntos
             ( oCampo.FieldName = 'FT_DETALLE'  ) or  // FON_TOT - Calculo de Fonacot - Totales
             ( oCampo.FieldName = 'RS_KEY_PR'  ) or ( oCampo.FieldName = 'RS_KEY_PU'  ) or ( oCampo.FieldName = 'RS_CERT'  ) or // RSOCIAL - Razones sociales
             ( oCampo.FieldName = 'KI_NOTA'  ) or   // KARINF - Kardex ce Infonavit - Notas
             ( oCampo.FieldName = 'HUELLA'  ) or  // HUELLA - Huellas de biometricos
             ( oCampo.FieldName = 'BM_DETALLE'  ) or  // BIT_MIG - ???
             ( oCampo.FieldName = 'PM_OBSERVA'  ) or  // POLIZA_MED - P�liza de SGMM
             ( oCampo.FieldName = 'PV_CONDIC'  ) or  // POL_VIGENC - Vigencia de p�liza de SGMM
             ( oCampo.FieldName = 'TN_PROPOSI'  ) or ( oCampo.FieldName = 'TN_ACT_NIV'  ) or ( oCampo.FieldName = 'TN_PERFIL'  ) or ( oCampo.FieldName = 'TN_CR_EVA'  ) or ( oCampo.FieldName = 'TN_AC_COMP'  ) or// C_T_NACOMP - Razones sociales
             ( oCampo.FieldName = 'CC_DETALLE'  ) or  // C_COMPETEN - Cat�logo de Competencias
             ( oCampo.FieldName = 'CP_DETALLE'  ) or  // C_PERFIL - Perfil de Competencias
             // Campos Memo Seleccion
             ( oCampo.FieldName = 'BI_DATA' {NO} ) or   // BITACORA - Notas de Bit�cora
             ( oCampo.FieldName = 'CL_OBSERVA' {16} ) or   // CLIENTE - Observaciones del cat�logo de clientes
             ( oCampo.FieldName = 'ER_DETALLE' {16} ) or   // ENTREVIS - Detalles de Entrevista
             ( oCampo.FieldName = 'EX_OBSERVA' {16}  ) or   // EXAMEN - Observaciones de Examen
             ( oCampo.FieldName = 'PU_HABILID' {16 }) or ( oCampo.FieldName = 'PU_ACTIVID' {16} ) or  // PUESTO - Habilidades y activ. requeridas del puesto
             ( oCampo.FieldName = 'RQ_OBSERVA' {16} ) or ( oCampo.FieldName = 'RQ_HABILID' {16} ) or ( oCampo.FieldName = 'RQ_ACTIVID' {NO} ) or ( oCampo.FieldName = 'RQ_ANUNCIO' {16} ) or // REQUIERE - Requisiciones de personal
             ( oCampo.FieldName = 'SO_OBSERVA' {16} ) or   // SOLICITA - Observaciones de Solicitud de empleo
             // Campos Memo Visitantes
             ( oCampo.FieldName = 'BI_DATA' {NO} ) or   // BITACORA - Notas de Bit�cora
             ( oCampo.FieldName = 'CI_OBSERVA' {16} ) or   // CITA - Observaciones de registro de Citas
             ( oCampo.FieldName = 'CO_OBSERVA' {16} ) or   // CORTE - Observaciones de Cortes
             ( oCampo.FieldName = 'LI_OBSERVA' {16} ) or   // LIBRO - Observaciones de Libro de Visitas
             // Campos Memo Comparte
             ( oCampo.FieldName = 'BI_DATA' {NO} ) or   // BITACORA - Notas de Bit�cora
             ( oCampo.FieldName = 'RP_DESCRIP'  ) or   // REPORTAL - ???
             ( oCampo.FieldName = 'BU_MEMO'  ) or   // BUZONSUG - ???
             ( oCampo.FieldName = 'DC_MEMO'  ) or   // DOCUMENT - ???
             ( oCampo.FieldName = 'EV_MEMO'  ) or   // EVENTO - ???
             ( oCampo.FieldName = 'NT_MEMO'  ) or   // NOTICIA - ???
             ( oCampo.FieldName = 'WA_DESCRIP'  ) or   // WACCION - ???
             ( oCampo.FieldName = 'WN_NOTAS'  ) or   // WMENSAJE - ???
             ( oCampo.FieldName = 'WM_DESCRIP'  ) or   // WMODELO - ???
             ( oCampo.FieldName = 'WE_DESCRIP'  ) or   // WMODSTEP - ???
             ( oCampo.FieldName = 'WO_TO' ) or ( oCampo.FieldName = 'WO_CC'  ) or ( oCampo.FieldName = 'WO_BODY'  ) or // WOUTBOX - ???
             ( oCampo.FieldName = 'SE_DATA' ) or   //  SP_PATCH - ???
             ( oCampo.FieldName = 'HU_HUELLA' ) or   //  HUELLAGTI - ???
             ( oCampo.FieldName = 'TE_XML' ) or ( oCampo.FieldName = 'TE_LOGO'  ) or ( oCampo.FieldName = 'TE_FONDO'  ) or // TERMINAL - ???
             // Campos Imagen Tress
             ( oCampo.FieldName = 'MA_IMAGEN' {15} ) or  // MAESTRO - Cat�logo de Maestros
             ( oCampo.FieldName = 'CO_D_BLOB' {15} ) or  //CONCEPTO - Archivo de Cat. de Conceptos
             ( oCampo.FieldName = 'IM_BLOB' {15} ) or    // IMAGEN - Expediente de Fotos del empleado
             ( oCampo.FieldName = 'KI_D_BLOB' {15} ) or  // KARINF - Kardex ce Infonavit - Archivo anexo
             ( oCampo.FieldName = 'MQ_IMAGEN' {15} ) or    // MAQUINA - Layout de Maquinas de Labor
             ( oCampo.FieldName = 'DO_BLOB' {15} ) or    // DOCUMENTO - Expediente de documentos del empleado
             // Campos Imagen de Seleccion
             ( oCampo.FieldName = 'SO_FOTO' {15} ) or    // SOLICITA - Foto de la solicitud de empleo
             ( oCampo.FieldName = 'DO_BLOB' {15} ) or    // DOCUMENTO - Documentos anexo de la solicitud de empleo
             // Campos Imagen de Visitantes
             ( oCampo.FieldName = 'VI_FOTO' {15} )    // VISITA - Foto del visitante
          then
          begin
               raise Exception.Create( oCampo.FieldName + ': ' + IntToStr( Ord( oCampo.DataType ) ) );
          end;
{$endif}

procedure AsignaParamCampo(oParam: ADODB.TParameter; oCampo: TField);
begin
     with oParam do
     begin
          Assign(oCampo);
          {
          if DataType = ftBlob then
          begin
               DataType := ftString;
               Value    := oCampo.AsString;
          end
          else}
          if ( DataType = ftString ) then
          begin
               if TStringField(oCampo).FixedChar then
                  DataType := ftFixedChar;
          end;
     end;
end;

{
  function GetExceptionInfo( Error: Exception ): String;
  begin
  Result := GetExceptionMsg( Error.Message );
  end;
}

function GetExceptionLogType(Problema: Exception): eTipoBitacora;
begin
     if (Problema is EAdvertencia) then
        Result := tbAdvertencia
     else
         Result := tbErrorGrave;
end;

function GetEmptyProcessResult(eProceso: Procesos): OleVariant;
begin
     Result := VarArrayOf([0, Ord(epsEjecutando), Ord(eProceso), 0, 0, 0, Now, Now, 0, 0, 0]);
end;

function GetEmptyProcessResultFecha(eProceso: Procesos): OleVariant;
begin
     Result := GetEmptyProcessResult(eProceso);
     Result[K_PROCESO_INICIO] := Now + 9999;
  // Esto es para que al sumarse varios procesos, se queda la primer fecha.
end;

procedure SumaProcessResult(var Result: OleVariant; Process1, Process2: OleVariant);
var
   iTemp: Integer;
begin
     // Este codigo es para poder reportar solamente una vez que ya termino el proceso
     Result[K_PROCESO_STATUS] := iMax(Ord(eProcessStatus(Process1[K_PROCESO_STATUS])), Ord(eProcessStatus(Process2[K_PROCESO_STATUS])));

     Result[K_PROCESO_MAXIMO]          := Process1[K_PROCESO_MAXIMO] + Process2[K_PROCESO_MAXIMO];
     Result[K_PROCESO_PROCESADOS]      := Process1[K_PROCESO_PROCESADOS] + Process2[K_PROCESO_PROCESADOS];
     Result[K_PROCESO_ULTIMO_EMPLEADO] := iMax(Process1[K_PROCESO_ULTIMO_EMPLEADO], Process2[K_PROCESO_ULTIMO_EMPLEADO]);

     Result[K_PROCESO_INICIO] := rMin(Process1[K_PROCESO_INICIO], Process2[K_PROCESO_INICIO]);
     Result[K_PROCESO_FIN]    := rMax(Process1[K_PROCESO_FIN], Process2[K_PROCESO_FIN]);

     iTemp := ( Process1[K_PROCESO_ERRORES] + Process2[K_PROCESO_ERRORES] );
     Result[K_PROCESO_ERRORES]      := iTemp;
     iTemp := ( Process1[K_PROCESO_ADVERTENCIAS] + Process2[K_PROCESO_ADVERTENCIAS] );
     Result[K_PROCESO_ADVERTENCIAS] := iTemp;
     iTemp := ( Process1[K_PROCESO_EVENTOS] + Process2[K_PROCESO_EVENTOS] );
     Result[K_PROCESO_EVENTOS]      := iTemp;
end;

procedure SetProcessOK(var Resultado: OleVariant);
begin
     Resultado[K_PROCESO_STATUS] := Ord(epsOK);
     Resultado[K_PROCESO_FIN]    := Now;
end;

procedure SetProcessError(var Resultado: OleVariant);
begin
     Resultado[K_PROCESO_STATUS] := Ord(epsError);
     Resultado[K_PROCESO_FIN]    := Now;
end;

{ ****************** TZetaLog ******************* }

constructor TZetaLog.Create(oProvider: TdmZetaServerProvider);
begin
     oZetaProvider := oProvider;
end;

procedure TZetaLog.Init;
begin
     FMaxSteps     := 0;
     FIncrement    := 0;
     FSteps        := 1;
     FCounter      := 0;
     FCancelado    := FALSE;
     FFolio        := 0;
     FInicio       := NullDateTime;
     FEmpleado     := 0;
     FErrorCount   := 0;
     FEventCount   := 0;
     FWarningCount := 0;
end;

procedure TZetaLog.Conteo(const eTipo: eTipoBitacora);
begin
     case eTipo of
       tbNormal:
         Inc(FEventCount);
       tbAdvertencia:
         Inc(FWarningCount);
       tbError:
         Inc(FErrorCount);
       tbErrorGrave:
         Inc(FErrorCount);
     end;
end;

function TZetaLog.Abierto: Boolean;
begin
     Result := (FFolio <> 0);
end;

function TZetaLog.HayErrores: Boolean;
begin
     Result := (FErrorCount > 0);
end;

function TZetaLog.GetStatus: TLogStatus;
begin
     if FCancelado then
        Result := B_PROCESO_CANCELADO
     else
         Result := ZetaCommonTools.zBoolToStr(HayErrores);
end;

function TZetaLog.GetEnumStatus: eProcessStatus;
var
   cStatus: TLogStatus;
begin
     cStatus := GetStatus;
     if (cStatus = B_PROCESO_CANCELADO) then
        Result := epsCancelado
     else if (cStatus = B_PROCESO_OK) then
        Result := epsOK
     else if (cStatus = B_PROCESO_ERROR) then
        Result := epsError
     else if (cStatus = B_PROCESO_ABIERTO) then
        Result := epsError
     else
        Result := epsCatastrofico;
end;

procedure TZetaLog.BorraQuerys;
begin
     with oZetaProvider do
     begin
          FreeAndNil(FDataset);
          FreeAndNil(FUpdate);
     end;
end;

procedure TZetaLog.PreparaQuerys;
const
     K_WRITE_LOG = 'insert into BITACORA (US_CODIGO, BI_FECHA, BI_HORA, BI_PROC_ID, BI_TIPO, BI_CLASE, BI_NUMERO, BI_TEXTO, CB_CODIGO, BI_FEC_MOV, BI_DATA ) values '
                   + '( :US_CODIGO, :BI_FECHA, :BI_HORA, :BI_PROC_ID, :BI_TIPO, :BI_CLASE, :BI_NUMERO, :BI_TEXTO, :CB_CODIGO, :BI_FEC_MOV, :BI_DATA )';
     K_UPDATE_PROCESS = 'select STATUS from UPDATE_PROCESS_LOG( %d, :Empleado, :Paso, :Fecha, :Hora )';
begin
     with oZetaProvider do
     begin
          FDataset := CreateQuery(K_WRITE_LOG); { Prepara Query Para Escribir Eventos }
          ParamAsInteger(FDataset, 'US_CODIGO', EmpresaActiva[P_USUARIO]);
          ParamAsInteger(FDataset, 'BI_PROC_ID', Ord(FProceso));
          ParamAsInteger(FDataset, 'BI_NUMERO', FFolio);
          {$IFDEF INTERBASE }
          FUpdate := CreateQuery(Format(K_UPDATE_PROCESS, [FFolio]));
          { Prepara Query Para Actualizar Status del Proceso }
          {$ELSE}
          FUpdate := CreateStoredProc('UPDATE_PROCESS_LOG');
          PreparaStoredProc(FUpdate, 'UPDATE_PROCESS_LOG');
          TADOStoredProc(FUpdate).Parameters.AddParameter.Name := '@Proceso';
          ParamAsInteger(FUpdate, '@Proceso', FFolio);
          {$ENDIF}
     end;
end;

function TZetaLog.OpenProcess(const eProceso: Procesos; const iMaxSteps: Integer; const sParametros: string = VACIO;
         const sFiltro: string = VACIO; const sFormula: string = VACIO): Boolean;
const
     {$IFDEF INTERBASE}
     K_INIT_SQL = 'select FOLIO from INIT_PROCESS_LOG( :Proceso, :Usuario, :Fecha, :Hora, :Maximo, :Param )';
     {$ELSE}
     K_INIT_SQL = '{CALL INIT_PROCESS_LOG( :Proceso, :Usuario, :Fecha, :Hora, :Maximo, :Param, :Folio )}';
     {$ENDIF}
var
   iUsuario: Integer;
   oQuery  : TDataSet;
begin
     Init;
     FInicio  := Now;
     FProceso := eProceso;
     with oZetaProvider do
     begin
          iUsuario := EmpresaActiva[P_USUARIO];
          oQuery   := CreateQuery(K_INIT_SQL);
          try
             with oQuery do
             begin
                  EmpiezaTransaccion;
                  try
                     ParamAsInteger(oQuery, 'Proceso', Ord(FProceso));
                     ParamAsInteger(oQuery, 'Usuario', iUsuario);
                     ParamAsDate(oQuery, 'Fecha', Trunc(FInicio));
                     ParamAsString(oQuery, 'Hora', TimeToStrSQL(FInicio));
                     ParamAsInteger(oQuery, 'Maximo', iMaxSteps);
                     ParamAsVarChar(oQuery, 'Param', sParametros, ZetaCommonClasses.K_MAX_VARCHAR);

                     {$IFDEF INTERBASE}
                     Active := TRUE;
                     Result := not EOF;
                     {$ENDIF}
                     {$IFDEF MSSQL}
                     ParamSalida(oQuery, 'Folio');
                     Ejecuta(oQuery);
                     Result := TRUE;
                     {$ENDIF}
                     TerminaTransaccion(TRUE);
                  except
                        on Error: Exception do
                        begin
                             TerminaTransaccion(FALSE);
                             Result := FALSE;
                        end;
                  end;
                  if Result then
                  begin
                       {$IFDEF INTERBASE}
                       FFolio := FieldByName('FOLIO').AsInteger;
                       {$ELSE}
                       // FFolio := GetParametro( oQuery, '@Folio' ).AsInteger;
                       FFolio := GetParametro(oQuery, 'Folio').Value;
                       {$ENDIF}
                       FMaxSteps  := iMaxSteps;
                       FIncrement := ZetaCommonTools.iMin(25, ZetaCommonTools.iMax(1, Trunc(0.1 * FMaxSteps)));
                       { PENDIENTE: 25 y 10% deben ser configurables }
                       PreparaQuerys;
                  end
                  else
                      DataBaseError('Error al Crear Bit�cora del Proceso');
                  Active := FALSE;
                  // Evento que se Genera cuando se pasa de 255 caracteres de longitud
                  if strLleno(sFiltro) and strLleno(sFormula) then
                     Evento(clbNinguno, 0, FInicio, 'F�rmulas y Filtros', sFormula + CR_LF + sFiltro)
                  else
                  begin
                       if strLleno(sFiltro) then
                          Evento(clbNinguno, 0, FInicio, 'Filtros', sFiltro)
                       else
                       begin
                            if strLleno(sFormula) then
                               Evento(clbNinguno, 0, FInicio, 'F�rmulas', sFormula);
                       end;
                  end;
             end;
          finally
                 // Probando
                 FreeAndNil(oQuery);
          end;
     end;
end;

procedure TZetaLog.HuboRollBack;
begin
     BorraQuerys;
     PreparaQuerys;
end;

function TZetaLog.CanContinue(const iEmpleado: TNumEmp; const lTx: Boolean): Boolean;
const
     K_ANCHO_HORA = 8;
var
   dValue: TDateTime;
begin
     FCounter  := FCounter + 1;
     FSteps    := FSteps + 1;
     FEmpleado := iEmpleado;
     if FCancelado then
        Result := FALSE
     else if (FSteps > FIncrement) then
     begin
          dValue := Now;
          with oZetaProvider do
          begin
               { � Es correcto abrir/cerrar transacci�n }
               { si el c�digo que llama a este m�todo }
               { puede estar ya dentro de una transacci�n ? }
               if lTx then
                  EmpiezaTransaccion;
               try
                  {$IFDEF INTERBASE}
                  ParamAsInteger(FUpdate, 'Empleado', iEmpleado);
                  ParamAsInteger(FUpdate, 'Paso', FCounter);
                  ParamAsDate(FUpdate, 'Fecha', Trunc(dValue));
                  ParamAsVarChar(FUpdate, 'Hora', TimeToStrSQL(dValue), K_ANCHO_HORA);
                  ParamSalida(FUpdate, 'Status'); // RCM: Es instruccion correcta? Este provider est� pensado en ADO
                  with FUpdate do
                  begin
                       Active := TRUE;
                       Result := (Fields[0].AsInteger = 0);
                       Active := FALSE;
                  end;
                  {$ELSE}
                  ParamAsInteger(FUpdate, '@Empleado', iEmpleado);
                  ParamAsInteger(FUpdate, '@Paso', FCounter);
                  ParamAsDate(FUpdate, '@Fecha', Trunc(dValue));
                  ParamAsVarChar(FUpdate, '@Hora', TimeToStrSQL(dValue), K_ANCHO_HORA);
                  ParamAsInteger(FUpdate, '@Status', 0);
                  ParamSalida(FUpdate, '@Status');
                  Ejecuta(FUpdate);
                  Result := (ParamAsInteger(FUpdate, '@Status') = 0);
                  {$ENDIF}
                  if lTx then
                     TerminaTransaccion(TRUE);
               except
                     on Error: Exception do
                     begin
                          if lTx then
                            TerminaTransaccion(FALSE);
                          Result := not FCancelado; { � Es correcto suponer esto ? }
                          // RCM: Si hay error no se actualiza PROCESO y el usuario no se entera
                          // aqui se genera una excepci�n silenciosa y se desaparece
                     end;
               end;
          end;
          FSteps     := 1;
          FCancelado := not Result;
     end
     else
         Result := not FCancelado;
end;

function TZetaLog.CloseProcess: OleVariant;
const
     K_CLOSE_SQL = 'update PROCESO set ' + 'PC_FEC_FIN = ''%s'', ' + 'PC_HOR_FIN = ''%s'', ' + 'PC_PASO = %d, ' +
                   'PC_ERROR = ''%s'', ' + 'CB_CODIGO = %d ' + 'where ( PC_NUMERO = %d )';
var
   dValue: TDateTime;
begin
     dValue := Now;
     with oZetaProvider do
     begin
          if Abierto then
          begin
               {$IFDEF MSSQL}
               EmpiezaTransaccion;
               {$ENDIF}
               ExecSQL(EmpresaActiva, Format(K_CLOSE_SQL, [DateToStrSQL(dValue), TimeToStrSQL(dValue), FCounter, GetStatus, FEmpleado,
                     FFolio]));

               {$IFDEF MSSQL}
               TerminaTransaccion(TRUE);
               {$ENDIF}
          end;
     end;
     Result                            := GetEmptyProcessResult(FProceso);
     Result[K_PROCESO_FOLIO]           := FFolio;
     Result[K_PROCESO_STATUS]          := Ord(GetEnumStatus);
     Result[K_PROCESO_MAXIMO]          := FMaxSteps;
     Result[K_PROCESO_PROCESADOS]      := FCounter;
     Result[K_PROCESO_ULTIMO_EMPLEADO] := FEmpleado;
     Result[K_PROCESO_INICIO]          := FInicio;
     Result[K_PROCESO_FIN]             := dValue;
     Result[K_PROCESO_ERRORES]         := FErrorCount;
     Result[K_PROCESO_ADVERTENCIAS]    := FWarningCount;
     Result[K_PROCESO_EVENTOS]         := FEventCount;
     BorraQuerys; // Liberar Cursores FDataSet y FUpdate
end;

procedure TZetaLog.CancelProcess(const iFolio: Integer);
const
     K_CANCEL_SQL = 'update PROCESO set ' + 'PC_FEC_FIN = ''%s'', ' + 'PC_HOR_FIN = ''%s'', ' + 'PC_ERROR = ''' + B_PROCESO_CANCELADO
                    + ''', ' + 'US_CANCELA = %d ' + 'where ( PC_NUMERO = %d )';
var
   dValue  : TDate;
   iUsuario: Integer;
begin
     with oZetaProvider do
     begin
          dValue   := Now;
          iUsuario := EmpresaActiva[P_USUARIO];
          EmpiezaTransaccion;
          try
             ExecSQL(EmpresaActiva, Format(K_CANCEL_SQL, [DateToStrSQL(dValue), TimeToStrSQL(dValue), iUsuario, iFolio]));
             TerminaTransaccion(TRUE);
          except
                on Error: Exception do
                begin
                     TerminaTransaccion(FALSE);
                     raise;
                end;
          end;
     end;
end;

procedure TZetaLog.Escribir(const eTipo: eTipoBitacora; const eClase: eClaseBitacora; const iEmpleado: TNumEmp;
          const dMovimiento: TDate; const sMensaje: TBitacoraTexto; const sTexto: string);
var
   dValor   : TDateTime;
   dGravaMov: TDate;
   sData    : string;
begin
     dValor := Now;
     if (dMovimiento = NullDateTime) then
        dGravaMov := Trunc(dValor)
       { Se usa Trunc() para que la base de datos no guarde la parte fraccionaria ( que representa la
         hora ) y los filtros de reportes puedan funcionar bien }
     else
         dGravaMov := dMovimiento;
     sData       := sTexto;
     if strVacio(sData) then
        sData := ' '; // Si no marca error al grabar el Blob
     with oZetaProvider do
     begin
          try
             ParamAsDate(FDataset, 'BI_FECHA', Trunc(dValor));
             { Se usa Trunc() para que la base de datos no guarde la parte fraccionaria ( que representa la
               hora ) y los filtros de reportes puedan funcionar bien }
             ParamAsVarChar(FDataset, 'BI_HORA', TimeToStrSQL(dValor), K_ANCHO_LOG_HORA);
             ParamAsInteger(FDataset, 'BI_TIPO', Ord(eTipo));
             ParamAsInteger(FDataset, 'BI_CLASE', Ord(eClase));
             ParamAsVarChar(FDataset, 'BI_TEXTO', Copy(sMensaje, 1, 50), K_ANCHO_LOG_TEXTO);
             ParamAsInteger(FDataset, 'CB_CODIGO', iEmpleado);
             ParamAsDate(FDataset, 'BI_FEC_MOV', dGravaMov);
             ParamAsBlob(FDataset, 'BI_DATA', sData);
             {$IFDEF MSSQL}
             {
               if FConnection.InTransaction then
               FConnection.CommitTrans
               else
               FConnection.BeginTrans;
             }
             {$ENDIF}
             Ejecuta(FDataset);
             {$IFDEF MSSQL}
             // FConnection.Commit;
             {$ENDIF}
             Conteo(eTipo);
          except
                on Error: Exception do
                begin
                     // ShowMessage( Error.Message );
                     {$IFDEF MSSQL}
                     // TerminaTransaccion( FALSE );
                     {$ENDIF}
                     Conteo(tbErrorGrave);
                     { � Que se debe hacer aqu� ? }
                end;
          end;
     end;
end;

procedure TZetaLog.Cambio(const sMensaje: TBitacoraTexto; const eClase: eClaseBitacora; const sCampoEmpleado, sCampoFecha: string;
          DataSet: TDataSet);
begin
     Escribir(tbNormal, eClase, 0, Now, sMensaje, sMensaje);
end;

procedure TZetaLog.Evento(const eClase: eClaseBitacora; const iEmpleado: TNumEmp; const dMovimiento: TDate;
          const sMensaje: TBitacoraTexto);
begin
     Escribir(tbNormal, eClase, iEmpleado, dMovimiento, sMensaje, sMensaje);
end;

procedure TZetaLog.Evento(const eClase: eClaseBitacora; const iEmpleado: TNumEmp; const dMovimiento: TDate;
          const sMensaje: TBitacoraTexto; const sTexto: string);
begin
     Escribir(tbNormal, eClase, iEmpleado, dMovimiento, sMensaje, sTexto);
end;

procedure TZetaLog.Advertencia(const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto);
begin
     Escribir(tbAdvertencia, clbNinguno, iEmpleado, NullDateTime, sMensaje, sMensaje);
end;

procedure TZetaLog.Advertencia(const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto; const sTexto: string);
begin
     Escribir(tbAdvertencia, clbNinguno, iEmpleado, NullDateTime, sMensaje, sTexto);
end;

procedure TZetaLog.Advertencia(const eClase: eClaseBitacora; const iEmpleado: TNumEmp; const dMovimiento: TDate;
          const sMensaje: TBitacoraTexto; const sTexto: string);
begin
     Escribir(tbAdvertencia, eClase, iEmpleado, dMovimiento, sMensaje, sTexto);
end;

procedure TZetaLog.Error(const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto; const sTexto: string);
begin
     Escribir(tbError, clbNinguno, iEmpleado, NullDateTime, sMensaje, sTexto);
end;

procedure TZetaLog.ErrorGrave(const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto);
begin
     Escribir(tbErrorGrave, clbNinguno, iEmpleado, NullDateTime, sMensaje, sMensaje);
end;

procedure TZetaLog.Excepcion(const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto; Problema: Exception; const sTexto: string);
begin
     Escribir(GetExceptionLogType(Problema), clbNinguno, iEmpleado, NullDateTime, sMensaje, ZetaServerTools.GetExceptionInfo(Problema) + CR_LF + sTexto);
end;

procedure TZetaLog.Excepcion(const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto; Problema: Exception);
begin
     Excepcion(iEmpleado, sMensaje, Problema, '');
end;

{ TTablaInfo }

constructor TTablaInfo.Create;
begin
     FListaCampos     := TStringList.Create;
     FListaKey        := TStringList.Create;
     FListaOrder      := TStringList.Create;
     FListaForeignKey := TStringList.Create;
     FParams          := TParams.Create;
     FSQL             := TStringList.Create;

     FFiltro             := '';
     FOnUpdateData       := nil;
     FBeforeUpdateRecord := nil;
     FAfterUpdateRecord  := nil;
     FOnUpdateError      := nil;
end;

destructor TTablaInfo.Destroy;
begin
     FListaCampos.Free;
     FListaKey.Free;
     FListaOrder.Free;
     FListaForeignKey.Free;
     FParams.Free;
     FSQL.Free;
     inherited;
end;

procedure TTablaInfo.SetCampos(const sCampos: string);
begin
     FListaCampos.CommaText := sCampos;
end;

procedure TTablaInfo.SetKey(const sCampos: string);
begin
     FListaKey.CommaText := sCampos;
     FListaOrder.Assign(FListaKey);
end;

procedure TTablaInfo.SetOrder(const sCampos: string);
begin
     FListaOrder.CommaText := sCampos;
end;

procedure TTablaInfo.SetForeignKey(const sCampos: string);
begin
     FListaForeignKey.CommaText := sCampos;
end;

procedure TTablaInfo.SetInfo(const sTabla, sCampos, sKey: string);
begin
     FTabla := sTabla;
     SetCampos(sCampos);
     SetKey(sKey);
     Filtro := '';
     SetForeignKey('');
     OnUpdateData       := nil;
     BeforeUpdateRecord := nil;
     AfterUpdateRecord  := nil;
     OnUpdateError      := nil;
end;

procedure TTablaInfo.SetInfoDetail(const sTabla, sCampos, sKey, sForeignKey: string);
begin
     SetInfo(sTabla, sCampos, sKey);
     SetForeignKey(sForeignKey);
end;

function TTablaInfo.ConstruyeQuery(const eTipo: eTipoQuery; Delta: TDataSet; qryCambios: TADOCommand): string;
var
   lEsDetail     : Boolean;
   oMasterDataSet: TDataSet;

  procedure Agrega(const sLinea: string);
  begin
    SQL.Add(sLinea);
  end;

  procedure AgregaLista(oLista: TStringList);
  begin
    Agrega(oLista.CommaText);
  end;

  procedure ConstruyeSelect;
  begin
    Agrega('SELECT');
    AgregaLista(FListaCampos);
  end;

  procedure ConstruyeDelete;
  begin
    Agrega('DELETE');
  end;

  procedure ConstruyeFrom;
  begin
    Agrega('FROM ' + FTabla);
  end;

  function CampoConstante(const Valor: Variant; const Tipo: TFieldType): string;
  begin
    Result := '';
    case Tipo of
      ftSmallint, ftInteger, ftWord, ftAutoInc:
        Result := IntToStr(Valor);
      ftString:
        Result := '''' + Valor + '''';
      ftFloat, ftCurrency, ftBCD, ftFMTBcd:
        Result := FloatToStr(Valor);
      ftDate, ftTime, ftDateTime:
        Result := '''' + FormatDateTime('mm/dd/yyyy', Valor) + '''';
    end;
  end;

  procedure RevisaCambios;
  var
    i, iPos: Integer;
  begin
    with Delta do
      for i := 0 to FieldCount - 1 do
        with Fields[i] do
          if not IsNull and (NewValue <> OldValue) then begin
            iPos := FListaCampos.IndexOf(FieldName);
            // Si el campo existe en esta tabla
            if (iPos >= 0) then
              FParams.CreateParam(DataType, FieldName, ptInput);
          end;
  end;

  procedure AgregaCamposUpdate;
  var
    i     : Integer;
    sCampo: string;
  begin
    with FParams do
      for i    := 0 to Count - 1 do begin
        sCampo := Items[i].Name;
        sCampo := sCampo + ' = :' + sCampo;
        if (i < Count - 1) then
          sCampo := sCampo + ',';
        Agrega(sCampo);
      end;
  end;

{
  insert into edocivil
  (TB_CODIGO, TB_ELEMENT, TB_INGLES, TB_NUMERO, TB_TEXTO)
  values
  (:TB_CODIGO, :TB_ELEMENT, :TB_INGLES, :TB_NUMERO, :TB_TEXTO)
}
  function EsForeignKey(const sCampo: string): Boolean;
  begin
    Result := FListaForeignKey.IndexOf(sCampo) >= 0;
  end;

  procedure RevisaValores;
  var
    i, iPos: Integer;
  begin
    with Delta do
      for i := 0 to FieldCount - 1 do
        with Fields[i] do
          if (not IsNull) or (lEsDetail and EsForeignKey(FieldName)) then begin
            iPos := FListaCampos.IndexOf(FieldName);
            // Si el campo existe en esta tabla
            if (iPos >= 0) then
              FParams.CreateParam(DataType, FieldName, ptInput);
          end;
  end;

  procedure AgregaCamposInsert;
  var
    i     : Integer;
    sCampo: string;
  begin
    with FParams do
      for i    := 0 to Count - 1 do begin
        sCampo := Items[i].Name;
        if (i < Count - 1) then
          sCampo := sCampo + ',';
        Agrega(sCampo);
      end;

  end;

  procedure AgregaValoresInsert;
  var
    i     : Integer;
    sCampo: string;
    // oField : TField;
  begin
    {
      with FParams do
      for i := 0 to Count - 1 do begin
      sCampo := Items[i].Name;
      oField := Delta.FindField(sCampo);
      if (lEsDetail and EsForeignKey(oField.FieldName)) then
      with oMasterDataSet.FieldByName(sCampo) do
      sCampo := CampoConstante(Value, DataType)
      else
      sCampo := CampoConstante(oField.Value, oField.DataType);
      if (i < Count - 1) then
      sCampo := sCampo + ',';
      Agrega(sCampo);
      end;
    }
    // Temporal: La unica diferencia con agrega campos Insert es ':'
    with FParams do
      for i    := 0 to Count - 1 do begin
        sCampo := ':' + Items[i].Name;
        if (i < Count - 1) then
          sCampo := sCampo + ',';
        Agrega(sCampo);
      end;

  end;

  procedure AsignaParams;
  var
    i     : Integer;
    sCampo: string;
    oField: TField;
  begin
    with FParams do
      for i    := 0 to Count - 1 do begin
        sCampo := Items[i].Name;
        oField := Delta.FindField(sCampo);

        if (eTipo = etqInsert) and (lEsDetail and EsForeignKey(oField.FieldName)) then
          oField := oMasterDataSet.FieldByName(sCampo);

        AsignaParamCampo(qryCambios.Parameters[i], oField);
      end;
  end;

  procedure ConstruyeInsert;
  begin
    Agrega('INSERT INTO ' + FTabla);
    Agrega('(');
    RevisaValores;
    AgregaCamposInsert;
    Agrega(') VALUES (');
    AgregaValoresInsert;
    Agrega(')');
  end;

  procedure ConstruyeUpdate;
  begin
    Agrega('UPDATE ' + FTabla);
    Agrega('SET');
    RevisaCambios;
    AgregaCamposUpdate;
  end;

  procedure ConstruyeWhereDetail;
  var
    i                         : Integer;
    sCampoMaster, sCampoDetail: string;
    sCampo                    : string;
  begin
    with FListaForeignKey do begin
      Agrega('WHERE');
      for i          := 0 to Count - 1 do begin
        sCampoMaster := Strings[i];
        // Si se tiene un Pare Name=Value
        if Pos('=', sCampoMaster) > 0 then begin
          sCampoMaster := Names[i];
          sCampoDetail := Values[sCampoMaster];
        end
        else
          sCampoDetail := FListaKey.Strings[i];
        sCampo         := sCampoDetail + ' = :' + sCampoMaster;
        if (i < Count - 1) then
          sCampo := sCampo + ' and';
        Agrega(sCampo);
      end;
    end;
  end;

  procedure ConstruyeWherePadre;
  var
    i                         : Integer;
    sCampoMaster, sCampoDetail: string;
    sCampo                    : string;
  begin
    with FListaForeignKey do begin
      Agrega('WHERE');
      for i          := 0 to Count - 1 do begin
        sCampoDetail := FListaKey.Strings[i];
        sCampoMaster := Strings[i];
        with Delta.FieldByName(sCampoMaster) do
          sCampoMaster := CampoConstante(NewValue, DataType);
        sCampo         := sCampoDetail + ' = ' + sCampoMaster;
        if (i < Count - 1) then
          sCampo := sCampo + ' and';
        Agrega(sCampo);
      end;
    end;
  end;

  procedure ConstruyeWhereFiltro;
  begin
    Agrega('WHERE');
    Agrega(FFiltro);
  end;

  procedure AgregaListaWhere(oLista: TStringList);
  var
    i     : Integer;
    sCampo: string;
    sValor: string;
    oField: TField;
    dCampo: TDate;
  begin
    with oLista do
      for i    := 0 to Count - 1 do begin
        sCampo := Strings[i];
        oField := Delta.FindField(sCampo);
        if Assigned(oField) then begin
          if oField.DataType in [ftDate, ftTime, ftDateTime] then begin
            dCampo := oField.OldValue;
            sValor := '''' + FormatDateTime('mm/dd/yyyy', dCampo) + '''';
          end
          else
            sValor := CampoConstante(oField.OldValue, oField.DataType);
          sCampo   := sCampo + ' = ' + sValor;
          if (i < Count - 1) then
            sCampo := sCampo + ' and';
          Agrega(sCampo);
        end;
      end;
  end;

  procedure ConstruyeWhere;
  begin
    Agrega('WHERE');
    AgregaListaWhere(FListaKey);
  end;

  procedure ConstruyeOrder;
  begin
    Agrega('ORDER BY');
    AgregaLista(FListaOrder);
  end;

begin // ConstruyeQuery
     FParams.Clear;
     SQL.Clear;
     lEsDetail := FListaForeignKey.Count > 0;
     if (lEsDetail) then
       oMasterDataSet := TClientDataset(Delta).DataSetField.DataSet;

     case eTipo of
       etqSelect:
       begin
           ConstruyeSelect;
           ConstruyeFrom;
           if (lEsDetail) then
             ConstruyeWhereDetail
           else if (Length(FFiltro) > 0) then
             ConstruyeWhereFiltro;
           ConstruyeOrder;
       end;
       etqDelete:
       begin
           ConstruyeDelete;
           ConstruyeFrom;
           ConstruyeWhere;
           qryCambios.CommandText := SQL.Text;
       end;
       etqUpdate:
       begin
           ConstruyeUpdate;
           ConstruyeWhere;
           qryCambios.CommandText := SQL.Text;
           AsignaParams;
       end;
       etqInsert:
       begin
           ConstruyeInsert;
           qryCambios.CommandText := SQL.Text;
           AsignaParams;
       end;
     end;
     Result := SQL.Text;
     {
       if eTipo <> etqSelect then
       ShowMessage( Result );
     }
end;

function TTablaInfo.ConstruyeUpdateSQL(const UpdateKind: TUpdateKind; Delta: TDataSet; qryCambios: TADOCommand): string;
var
   TipoQuery: eTipoQuery;
begin
     case UpdateKind of
          ukModify: TipoQuery := etqUpdate;
          ukDelete: TipoQuery := etqDelete;
     else
          TipoQuery := etqInsert;
     end;
     Result := ConstruyeQuery(TipoQuery, Delta, qryCambios);
end;

function TTablaInfo.ConstruyeSelectSQL: string;
begin
     Result := ConstruyeQuery(etqSelect, nil, nil);
end;

{ TZFixedCharField }

function TZFixedCharField.GetAsString: string;
begin
     Result := TrimRight(inherited GetAsString);
end;

{ TZADOQuery }

function TZADOQuery.GetFieldClass(FieldType: TFieldType): TFieldClass;
begin
     if (FieldType = ftFixedChar) then
        Result := TZFixedCharField
     else
         Result := inherited GetFieldClass(FieldType);
end;

{ TdmZetaProvider }

procedure TdmZetaServerProvider.dmZetaProviderCreate(Sender: TObject);
begin
     Environment;

     FDatosCalendario := TCalendario.Create;
     FDatosNomInfo    := TDatosNomInfo.Create;
     FDatosPrestamo   := TDatosPrestamo.Create;
     FDatosNOMM       := TDatosNomm.Create;
     FChecadaComida   := TDatosComida.Create;

     FTablaInfo  := TTablaInfo.Create;
     FDetailInfo := TTablaInfo.Create;
     {$IFDEF NEWCOMPARTE}
     RegistryInit;
     {$ELSE}
     {$IFDEF DOS_CAPAS}
     FRegistry := TZetaRegistryServer.Create;
     {$ELSE}
     FRegistry := TZetaRegistryServer.Create(TRUE);
     {$ENDIF}
     {$ENDIF}
     Activate;
     {$IFDEF MSSQL}
     // EZM: En Master-Detail, marca error Connection Busy
     // por abrir 2 Unidireccionales al mismo tiempo, en la misma conecci�n
     // Adem�s, se tienen problemas con BLOB's
     qryMaster.CursorType := ADODB.TCursorType.ctDynamic;
     qryDetail.CursorType := ADODB.TCursorType.ctDynamic;
     // qryCambios.UniDirectional := FALSE;
     {$ENDIF}
     with prvUnico do
     begin
          OnGetData          := prvUnicoGetData;
          OnUpdateData       := prvUnicoUpdateData;
          OnUpdateError      := prvUnicoUpdateError;
          AfterUpdateRecord  := prvUnicoAfterUpdateRecord;
          BeforeUpdateRecord := prvUnicoBeforeUpdateRecord;
     end;
     adoComparte.CommandTimeOut := K_TIMEOUT;
     adoEmpresa.CommandTimeOut  := K_TIMEOUT;
     qryMaster.CommandTimeOut   := K_TIMEOUT;
     qryDetail.CommandTimeOut   := K_TIMEOUT;
     qryCambios.CommandTimeOut  := K_TIMEOUT;
     qryMaster.CacheSize        := K_CACHESIZE;
     qryDetail.CacheSize        := K_CACHESIZE;

     //Creacion de listas
     FArregloPeriodo := TStringList.Create;
     FArregloTipoNomina := TStringList.Create;
     //Lista de objetos tipo periodo

end;

procedure TdmZetaServerProvider.SetOptimizeCursor(Value: Boolean);
begin
     FOptimizeCursor := Value;
     if FOptimizeCursor then
     begin
          qryMaster.CursorType := ctStatic; //ctOpenForwardOnly;
          qryDetail.CursorType := ctStatic; //ctOpenForwardOnly;
          qryMaster.CursorLocation := clUseServer;
          //qryDetail.CursorLocation := clUseServer;
     end;
end;

procedure TdmZetaServerProvider.SetOptimizeCursorB8(Value: Boolean);
begin
     FOptimizeCursorB8 := Value;
     if FOptimizeCursorB8 then
     begin
          qryMaster.CursorLocation := clUseServer;
     end;
end;

procedure TdmZetaServerProvider.dmZetaProviderDestroy(Sender: TObject);
begin
     ReleaseLog;
     {$IFNDEF NEWCOMPARTE}
     FRegistry.Free;
     {$ENDIF}
     FDetailInfo.Free;
     FTablaInfo.Free;
     FreeAndNil(FCdsPeriodosNomInfo);

     FreeAndNil(FDatosCalendario);
     FreeAndNil(FDatosNomInfo);
     FreeAndNil(FDatosPrestamo);
     FreeAndNil(FDatosNOMM);
     FreeAndNil(FChecadaComida);

     //Destruccion de listas
     FreeAndNil(FArregloPeriodo);
     FreeAndNil(FArregloTipoNomina);
     //Destruccion de lista de objetos
end;

procedure TdmZetaServerProvider.Activate;
begin
     { GA: Este c�digo estaba antes en el DataModuleCreate }
     ZetaCommonTools.SetVariantToNull(FEmpresa);
     FPeriodoObtenido := FALSE;
     FVerConfidencial := TRUE;
     FNumeroNiveles   := 0;
end;

procedure TdmZetaServerProvider.Deactivate;
begin
     Self.adoEmpresa.Connected  := FALSE;
     Self.adoComparte.Connected := FALSE;
end;

procedure TdmZetaServerProvider.PreparaDetail;
begin
     qryDetail.TableName := FDetailInfo.FTabla;
     FDetailInfo.SetMasterFields(qryDetail);
end;

function TdmZetaServerProvider.GetMasterDetail(Empresa: Variant): OleVariant;
begin
     dsMaster.DataSet := qryMaster;
     try
        ConectaEmpresa(Empresa);
        PreparaTabla;
        PreparaDetail;
        FRecsOut := 0;
        Result := prvUnico.GetRecords(-1, FRecsOut, Ord(grMetaData) + Ord(grReset));
     finally
            dsMaster.DataSet := nil;
     end;
end;

procedure TdmZetaServerProvider.PreparaTabla;
begin
     with qryMaster do
     begin
          Active      := FALSE; // RCM: 11/11/20013
          CommandText := FTablaInfo.ConstruyeSelectSQL;
     end;
end;

function TdmZetaServerProvider.GetTabla(Empresa: Variant): OleVariant;
begin
     ConectaEmpresa(Empresa);
     PreparaTabla;
     FRecsOut := 0;
     Result   := prvUnico.GetRecords(-1, FRecsOut, Ord(grMetaData) + Ord(grReset));
end;

function TdmZetaServerProvider.GrabaTabla(Empresa: Variant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant;
var
   OwnerData: OleVariant;
begin
     ConectaEmpresa(Empresa);
     EmpiezaTransaccion;
     Result := prvUnico.ApplyUpdates(oDelta, -1, ErrorCount, OwnerData);
     if ErrorCount = 0 then
        TerminaTransaccion(TRUE)
     else
         RollBackTransaccion;
     // TerminaTransaccion( FALSE );
end;

function TdmZetaServerProvider.GrabaTablaGrid(Empresa: Variant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant;
var
   OwnerData: OleVariant;
begin
     ConectaEmpresa(Empresa);
     EmpiezaTransaccion;
     Result := prvUnico.ApplyUpdates(oDelta, -1, ErrorCount, OwnerData);
     TerminaTransaccion(TRUE)
end;

function TdmZetaServerProvider.GrabaMasterDetail(Empresa: Variant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant;
begin
     try
        dsMaster.DataSet := qryMaster;
        Result           := GrabaTabla(Empresa, oDelta, ErrorCount);
     finally
            dsMaster.DataSet := nil;
     end;
end;

procedure TdmZetaServerProvider.UpdateTabla(Sender: TObject; SourceDS: TDataSet; DeltaDS: TCustomClientDataset;
          UpdateKind: TUpdateKind; var Applied: Boolean);
var
   TablaInfo: TTablaInfo;
begin
     if Assigned(DeltaDS.DataSetField) then
       TablaInfo := FDetailInfo
     else
       TablaInfo := FTablaInfo;
     // qryCambios.CommandText := TablaInfo.ConstruyeUpdateSQL( UpdateKind, DeltaDS );
     TablaInfo.ConstruyeUpdateSQL(UpdateKind, DeltaDS, qryCambios);
     // try
     qryCambios.Execute;
     {
       // PENDIENTE
       if qryCambios.RowsAffected > 1 then
       DatabaseError('Demasiados registros cambiaron');
       if qryCambios.RowsAffected < 1 then
       DatabaseError('Registro modificado por otro usuario');
     }
     Applied := TRUE;
     // except
     // Applied := FALSE;
     // end;
end;

procedure TdmZetaServerProvider.ExecSQL(Empresa: Variant; const sSQL: string);
begin
     ConectaEmpresa(Empresa);
     with qryCambios do
     begin
          ParamCheck  := FALSE; // Para cuando se modifican StoredProcedures (ej. Supervisores)
          CommandText := sSQL;
          Execute;
          ParamCheck := TRUE;
     end;
end;

function TdmZetaServerProvider.OpenSQL(Empresa: Variant; const sSQL: string; const lDatos: Boolean): OleVariant;
var
   nRecords: Integer;
begin
     with qryMaster do
     begin
          Active := FALSE;
          ConectaEmpresa(Empresa);
          CommandText := sSQL;
          FRecsOut    := 0;
          if (lDatos) then
             nRecords := -1
          else
              nRecords := 0;
          Result := prvUnico.GetRecords(nRecords, FRecsOut, Ord(grMetaData) + Ord(grReset));
     end;
end;

function TdmZetaServerProvider.OpenSQL(Empresa: Variant; const sSQL: string; const nRecords: Integer): OleVariant;
begin
     with qryMaster do
     begin
          Active := FALSE;
          ConectaEmpresa(Empresa);
          CommandText := sSQL;
          FRecsOut    := 0;
          Result      := prvUnico.GetRecords(nRecords, FRecsOut, Ord(grMetaData) + Ord(grReset));
     end;
end;

function  TdmZetaServerProvider.EmpresaWF(const Empresa: Variant): Boolean;
begin
     Result := FALSE;
     try
        if ( VarArrayHighBound( Empresa, 1 ) >= P_CODIGO ) then
           Result := ( Empresa[P_CODIGO] =  'XCOMPARTEY' )
     except
        Result := False;
     end;
end;

function TdmZetaServerProvider.ConectaBaseDeDatos(const Empresa: Variant): TADOConnection;
var
   sAlias, sUser, sPassword: string;
   {$IFDEF MSSQL}
   sServer, sDatabase: string;
   {$ENDIF}
begin
     sAlias := Empresa[P_DATABASE];
     {$IFDEF NEWCOMPARTE}
     //if ( sAlias = FComparte[P_DATABASE] ) then
     if ( sAlias = FComparte[P_DATABASE] ) and ( not EmpresaWF( Empresa ) ) then //Validar por si es empresaWF, usara el Decrypt
     begin
          Result    := Self.adoComparte;
          sPassword := Empresa[P_PASSWORD]; { Ya viene decifrado }
     end
     else
     begin
          Result    := Self.adoEmpresa;
          sPassword := ZetaServerTools.Decrypt(Empresa[P_PASSWORD]); { Viene cifrado }
     end;
    {$ELSE}
    with FRegistry do
    begin
         if (sAlias = Database) then
         begin
              Result    := Self.adoComparte;
              sPassword := Empresa[P_PASSWORD]; { Ya viene decifrado }
         end else
         begin
              Result    := Self.adoEmpresa;
              sPassword := ZetaServerTools.Decrypt(Empresa[P_PASSWORD]); { Viene cifrado }
         end;
    end;
    {$ENDIF}
  { Nota: No est� dise�ado para estar cambiando de Empresa.
    Una vez que se conecta, ya queda conectada con ese Alias }
  try
     with Result do
     begin
          {$IFDEF DOS_CAPAS}
          if not Connected or (Datasource <> sAlias) then
          {$ELSE}
          if not Connected then
          {$ENDIF}
          begin
               {$IFDEF DOS_CAPAS}
               Connected := FALSE;
               {$ENDIF}
               sUser := Empresa[P_USER_NAME];
               {$IFDEF INTERBASE}
               Driver := 'INTERSOLV InterBase ODBC Driver (*.gdb)';
               with Attributes do
               begin
                    Clear;
                    Add(Format('Database=%s', [sAlias]));
                    Add('ApplicationUsingThreads=1');
                    Add('LockTimeOut=0');
                    Add('WorkArounds=1');
               end;
               {$ENDIF}
               {$IFDEF MSSQL}
               Connected := FALSE;
               ZetaServerTools.GetServerDatabase(sAlias, sServer, sDatabase);
               // Cadena de la versi�n 2013: Server=%s;Database=%s;uid=%s;pwd=%s;MultipleActiveResultSets=true;Pooling=true
               ConnectionString := Format('Provider=%s;Server=%s;Database=%s;uid=%s;pwd=%s;' +
                                   'MultipleActiveResultSets=true;Pooling=true;DataTypeCompatibility=80;Language=english', [GetSQLCLINProvider, sServer, sDatabase, sUser,
                                   sPassword]);

               if Result = Self.adoEmpresa  then
                  FConnectionString_Empresa := ConnectionString;

               if Result = Self.adoComparte then
                  FConnectionString_Comparte := ConnectionString;
               {$ENDIF}
               Connected := TRUE;
          end;
     end;
   except
         on Error: Exception do
         begin
              Error.Message := Format('Error al Conectar a la Base de Datos: [ %s ]%s: %s', [sAlias, sLineBreak + Error.ClassName, Error.Message]);
              raise;
         end;
   end;
end;

function TdmZetaServerProvider.GetInsertScript(const sTabla, sExcluidos: string; Source: TZetaCursor): string;
var
   i : Integer;
   sScript, sCampos, sField: string;
begin
     sScript := 'insert into ' + sTabla + ' ( ';
     sCampos := ' ) values ( ';
     with Source do
     begin
          for i := 0 to (FieldCount - 1) do
          begin
               with Fields[i] do
               begin
                    sField := FieldName;
                    if (FieldKind = fkData) and (Pos(sField, sExcluidos) = 0) then
                    begin
                         sScript := sScript + sField + ',';
                         sCampos := sCampos + ':' + sField + ',';
                    end;
               end;
          end;
     end;
     Result := ZetaCommonTools.CortaUltimo(sScript) + ZetaCommonTools.CortaUltimo(sCampos) + ' )';
end;

procedure TdmZetaServerProvider.ConectaEmpresa(const Empresa: Variant);
begin
     FConnection := ConectaBaseDeDatos(Empresa);
     with qryMaster do
     begin
          Active     := FALSE; // RCM: 11/11/20013
          Connection := FConnection;
     end;
     with qryDetail do
     begin
          Active     := FALSE; // RCM: 11/11/20013
          Connection := FConnection;
     end;
     qryCambios.Connection := FConnection;
end;

{ ************ L�gica de Manejo de Queries / Stored Procedures ************** }

function TdmZetaServerProvider.CreateQuery: TDataSet;
begin
     Result := TZADOQuery.Create(Self);
     with TZADOQuery(Result) do
     begin
          ParamCheck     := TRUE;
          CursorType     := ctOpenForwardOnly;
          LockType       := ltReadOnly;
          EnableBCD      := FALSE; // RCM
          CommandTimeOut := K_TIMEOUT;
          CacheSize      := K_CACHESIZE;
          {$IFNDEF SERVERCAFE}
          if FOptimizeCursorB8 then
          begin
               CursorLocation := clUseServer;
               CommandTimeOut := K_TIMEOUT_EXT;
          end
          else if FOptimizeCursor then
          begin
               CursorType     := ctStatic;
               CommandTimeOut := K_TIMEOUT_EXT;
          end;
          {$ENDIF}
     end;
end;

function TdmZetaServerProvider.CreateQuery(const sScript: string): TDataSet;
begin
     Result := CreateQuery;
     try
        PreparaQuery(Result, sScript);
     except
           FreeAndNil(Result);
           raise;
     end;
end;

function TdmZetaServerProvider.CreateStoredProc(const sProcName: string): TDataSet;
begin
     Result := TADOStoredProc.Create(Self);
     with TADOStoredProc(Result) do
     begin
          ProcedureName  := sProcName;
          CursorType     := ctOpenForwardOnly;
          LockType       := ltReadOnly;
          EnableBCD      := FALSE; // RCM
          CommandTimeOut := K_TIMEOUT;
          CacheSize      := K_CACHESIZE;
          if FOptimizeCursorB8 then
          begin
               CursorLocation := clUseServer;
               CommandTimeOut := K_TIMEOUT_EXT;
          end
          else if FOptimizeCursor then
          begin
               CursorLocation := clUseServer;    // Al implementar dlls con wizards verificar si procede el cambio o debe regresarse a clUseClient
               CommandTimeOut := K_TIMEOUT_EXT;
          end;
     end;
end;

function TdmZetaServerProvider.CreateQueryLocate: TDataSet;
begin
     Result := CreateQuery;
end;

procedure TdmZetaServerProvider.PreparaQuery(Query: TDataSet; sScript: string);
begin
     {$IFDEF MSSQL}
     if UpperCase(Copy(sScript, 1, 17)) = 'EXECUTE PROCEDURE' then
        sScript := '{CALL' + Copy(sScript, 18, MAXINT) + '}';
     {$ENDIF}
     with Query as TZADOQuery do
     begin
          // Marcar los Querys que hacen SELECT pero que se ejecutan
          if UpperCase(Copy(sScript, 1, 6)) = 'SELECT' then
             Tag := 1
            {$IFDEF MSSQL}
          else if Pos('CALL ', UpperCase(sScript)) > 0 then
          begin
               Tag := 2; // Stored Procedures
               // Unidirectional := TRUE;
          end
          {$ENDIF}
          else
              Tag      := 0;
          Active     := FALSE;
          Connection := ConectaBaseDeDatos(EmpresaActiva);
          SQL.Text   := sScript;
          Prepared   := TRUE;
          {$IFDEF MSSQL}
          if ( Tag = 2 ) then
             CursorType := ADODB.TCursorType.ctOpenForwardOnly;
          {$ENDIF}
     end;
end;

procedure TdmZetaServerProvider.PreparaStoredProc(Query: TDataSet; const sProcName: string);
begin
     with Query as TADOStoredProc do
     begin
          Connection    := ConectaBaseDeDatos(EmpresaActiva);
          ProcedureName := sProcName;
          Prepared      := TRUE;
     end;
end;

function TdmZetaServerProvider.Ejecuta(DataSet: TDataSet): Integer;
begin
     Result := -1;
     if (DataSet is TZADOQuery) then
     begin
          with TZADOQuery(DataSet) do
          begin
               // Es un SELECT a un StoredProcedure que modifica la Base de Datos
               // se tiene que ejecutar con Close/Open
               if (Tag = 1) then
               begin
                    Close;
                    Open;
                    Result := 1;
               end
               else
               begin
                    ExecSQL;
                    Result := RowsAffected;
               end;
          end;
     end
     else if (DataSet is TADOStoredProc) then
     begin
          TADOStoredProc(DataSet).ExecProc;
          Result := 0;
     end
     else
     begin
          raise Exception.Create(Format('Componente Query / SP Invalido, Dataset: "%s" (Tipo incorrecto "%s")',
                [DataSet.Name, DataSet.ClassName]));
     end;
end;

function TdmZetaServerProvider.EjecutaAndFree(const sSQL: string): Integer;
var
   FDataset      : TZetaCursor;
   lEnTransaccion: Boolean;
begin
     Result         := 0;
     lEnTransaccion := FConnection.InTransaction;
     FDataset       := CreateQuery(sSQL);
     try
        try
          if not lEnTransaccion then
             EmpiezaTransaccion;
          Result := Ejecuta(FDataset);
          if not lEnTransaccion then
             TerminaTransaccion(TRUE);
        except
          if not lEnTransaccion then
            RollBackTransaccion;
          raise;
        end;
     finally
            FreeAndNil(FDataset);
     end;
end;

function TdmZetaServerProvider.GetParametro(DataSet: TDataSet; const sName: string): TParameter;
var
   ParamList: TParameters;
begin
     if (DataSet is TZADOQuery) then
     begin
          ParamList := TZADOQuery(DataSet).Parameters;
     end
     else if (DataSet is TADOStoredProc) then
     begin
          ParamList := TADOStoredProc(DataSet).Parameters;
     end
     else
     begin
          ParamList := nil;
          raise Exception.Create(Format('Componente Query / SP Invalido, Dataset: "%s" (Tipo incorrecto "%s"), Parametro: "%s"',
                [DataSet.Name, DataSet.ClassName, sName]));
     end;

     Result := ParamList.FindParam(sName);
     if (Result = nil) then
     begin
          Result      := ParamList.AddParameter;
          Result.Name := sName;
     end;
end;

procedure TdmZetaServerProvider.ParamSalida(DataSet: TDataSet; const sName: string);
begin
     GetParametro(DataSet, sName).Direction := ADODB.TParameterDirection.pdOutput;
end;

function TdmZetaServerProvider.GetParametroPos(DataSet: TZADOQuery; const iPos: Integer): TParameter;
begin
     Result := DataSet.Parameters.Items[iPos];
end;

procedure TdmZetaServerProvider.ParamVariant(DataSet: TDataSet; const iPos: Integer; const Valor: Variant);
begin
     GetParametroPos(TZADOQuery(DataSet), iPos).Value := Valor;
end;

function TdmZetaServerProvider.ParamVariant(DataSet: TDataSet; const iPos: Integer): Variant;
begin
     Result := GetParametroPos(TZADOQuery(DataSet), iPos).Value;
end;

procedure TdmZetaServerProvider.ParamAsInteger(DataSet: TDataSet; const sName: string; const iValue: Integer);
begin
     with GetParametro(DataSet, sName) do
     begin
          if DataType = ftUnknown then
             DataType := ftInteger;

          if DataType = ftSmallint then
             Value := SmallInt(iValue)
          else
              Value := iValue;
     end;
end;

function TdmZetaServerProvider.ParamAsInteger(DataSet: TDataSet; const sName: string): Integer;
begin
     with GetParametro(DataSet, sName) do
     begin
          Result := Integer(Value);
     end;
end;

procedure TdmZetaServerProvider.ParamAsFloat(DataSet: TDataSet; const sName: string; const rValue: Extended);
begin
     with GetParametro(DataSet, sName) do
     begin
          if ( DataType = ftUnknown ) or ( DataType = ftBCD ) then
             DataType := ftFloat;
          Value := rValue;
     end;
end;

function TdmZetaServerProvider.ParamAsFloat(DataSet: TDataSet; const sName: string): Extended;
begin
     with GetParametro(DataSet, sName) do
     begin
          Result := Extended(Value);
     end;
end;

procedure TdmZetaServerProvider.ParamAsDate(DataSet: TDataSet; const sName: string; const dValue: TDate);
begin
     with GetParametro(DataSet, sName) do
     begin
          if DataType = ftUnknown then
             DataType := ftDate;
          Value := dValue;
     end;
end;

function TdmZetaServerProvider.ParamAsDate(DataSet: TDataSet; const sName: string): TDate;
begin
     with GetParametro(DataSet, sName) do
     begin
          Result := TDate(Value);
     end;
end;

procedure TdmZetaServerProvider.ParamAsDateTime(DataSet: TDataSet; const sName: string; const dValue: TDateTime);
begin
     with GetParametro(DataSet, sName) do
     begin
          if DataType = ftUnknown then
             DataType := Db.TFieldType.ftDateTime;
          Value := dValue;
     end;
end;

function TdmZetaServerProvider.ParamAsDateTime(DataSet: TDataSet; const sName: string): TDateTime;
begin
     with GetParametro(DataSet, sName) do
     begin
          Result := TDateTime(Value);
     end;
end;

procedure TdmZetaServerProvider.ParamAsString(DataSet: TDataSet; const sName, sValue: string);
begin
     with GetParametro(DataSet, sName) do
     begin
          if DataType = ftUnknown then
             DataType := Db.TFieldType.ftString;
          Value := sValue;
     end;
end;

function TdmZetaServerProvider.ParamAsString(DataSet: TDataSet; const sName: string): string;
begin
     with GetParametro(DataSet, sName) do
     begin
          Result := AsString; // string(Value);
     end;
end;

procedure TdmZetaServerProvider.ParamAsVarChar(DataSet: TDataSet; const sName, sValue: string; const iAncho: Integer);
begin
     with GetParametro(DataSet, sName) do
     begin
          if DataType = ftUnknown then
             DataType := Db.TFieldType.ftString;
          if (iAncho > 0) then
             Value := Copy(sValue, 1, iAncho)
          else
              Value := sValue;
     end;
end;

procedure TdmZetaServerProvider.ParamAsChar(DataSet: TDataSet; const sName, sValue: string; const iAncho: Integer);
begin
     with GetParametro(DataSet, sName) do
     begin
          if DataType = ftUnknown then
             DataType := Db.TFieldType.ftString;
          Value := sValue;
     end;
end;

procedure TdmZetaServerProvider.ParamAsBoolean(DataSet: TDataSet; const sName: string; const lValue: Boolean);
begin
     // GetParametro( Dataset, sName ).AsString := ZetaCommonTools.zBoolToStr( lValue );
     ParamAsChar(DataSet, sName, ZetaCommonTools.zBoolToStr(lValue), 1);
end;

function TdmZetaServerProvider.ParamAsBoolean(DataSet: TDataSet; const sName: string): Boolean;
begin
     with GetParametro(DataSet, sName) do
     begin
          Result := Boolean(Value);
     end;
end;

procedure TdmZetaServerProvider.ParamAsBlob(DataSet: TDataSet; const sName: string; const sValue: string);
begin
     with GetParametro(DataSet, sName) do
     begin
          if (DataType = ftUnknown) then
             DataType := ftMemo;
          Value      := sValue;
     end;
end;

procedure TdmZetaServerProvider.ParamAsNull(DataSet: TDataSet; const sName: string);
begin
     with GetParametro(DataSet, sName) do
     begin
          Value := null;
     end;
end;

procedure TdmZetaServerProvider.ParamAsGUID(DataSet: TDataSet; const sName: string; const Valor: TGUID);
begin
     with GetParametro(DataSet, sName) do
     begin
          if (DataType = ftUnknown) then
             DataType := ftGuid;
          Value      := GuidToString(Valor);
     end;
end;

procedure TdmZetaServerProvider.CreateLog;
begin
     if not Assigned(FLog) then
     begin
          FLog := TZetaLog.Create(Self);
     end;
end;

procedure TdmZetaServerProvider.ReleaseLog;
begin
     if Assigned(FLog) then
     begin
          FreeAndNil(FLog);
     end;
end;

function TdmZetaServerProvider.CanContinue: Boolean;
begin
     Result := Log.CanContinue(0, TRUE);
end;

function TdmZetaServerProvider.CanContinue(const iEmpleado: TNumEmp): Boolean;
begin
     Result := Log.CanContinue(iEmpleado, TRUE);
end;

function TdmZetaServerProvider.CanContinue(const iEmpleado: TNumEmp; const lTx: Boolean): Boolean;
begin
     Result := Log.CanContinue(iEmpleado, lTx);
end;

function TdmZetaServerProvider.OpenProcess(const eProceso: Procesos; const iMaxSteps: Integer; sParametros: string = VACIO;
         sFormula: string = VACIO): Boolean;
var
   sFiltro: string;
   lFiltro: Boolean;

   function GetFiltro(sCampo: string): string;
   var
      oParam: TParam;
   begin
        oParam := FParamList.FindParam(sCampo);
        if (oParam <> nil) then
           Result := oParam.AsString
        else
            Result := VACIO;
   end;

begin
     CreateLog;
     sFiltro := VACIO;
     if not(FParamList = nil) then
     begin
          with FParamList do
          begin
            lFiltro := (FindParam('RangoLista') <> nil) and (FindParam('Condicion') <> nil) and (FindParam('Filtro') <> nil);
            ZetaServerTools.GetFiltroEmpleado(GetFiltro('RangoLista'), GetFiltro('Condicion'), GetFiltro('Filtro'), lFiltro,
              sParametros, sFiltro, sFormula);
          end;
     end;
     if (Length(sParametros) > ZetaCommonClasses.K_MAX_VARCHAR) then
        sParametros := Copy(sParametros, 1, ZetaCommonClasses.K_MAX_VARCHAR);
     Result := Log.OpenProcess(eProceso, iMaxSteps, sParametros, sFiltro, sFormula);
end;

function TdmZetaServerProvider.CloseProcess: OleVariant;
begin
     Result := Log.CloseProcess;
     ReleaseLog;
end;

procedure TdmZetaServerProvider.CancelProcess(const iFolio: Integer);
begin
     { Hay que crear y destruir la instancia de Log ya que }
     { Esto no se va a llamar como parte de un wizard }
     { sino que desde la pantalla de consulta del status de procesos }
     try
        CreateLog;
        Log.CancelProcess(iFolio);
     finally
            ReleaseLog;
     end;
end;

procedure TdmZetaServerProvider.AbreBitacora;
begin
     CreateLog;
end;

function TdmZetaServerProvider.AbreQueryScript(DataSet: TDataSet; const sScript: string): Boolean;
begin
     try
        with DataSet as TZADOQuery do
        begin
             Active     := FALSE;
             Connection := ConectaBaseDeDatos(EmpresaActiva);
             SQL.Text   := sScript;
             Active     := TRUE;
             Result     := TRUE;
        end;
     except
           Result := FALSE;
     end;
end;

function TdmZetaServerProvider.GetGlobalBooleano(const iCodigo: Integer): Boolean;
begin
     Result := zStrToBool(LeeGlobal(iCodigo));
end;

function TdmZetaServerProvider.GetGlobalDate(const iCodigo: Integer): TDateTime;
begin
     Result := StrToFecha(LeeGlobal(iCodigo));
end;

function TdmZetaServerProvider.GetGlobalInteger(const iCodigo: Integer): Integer;
begin
     Result := StrToIntDef(LeeGlobal(iCodigo), 0);
end;

function TdmZetaServerProvider.GetGlobalReal(const iCodigo: Integer): Real;
begin
     Result := StrToReal(LeeGlobal(iCodigo));
end;

function TdmZetaServerProvider.GetGlobalString(const iCodigo: Integer): string;
begin
     Result := LeeGlobal(iCodigo);
end;

procedure TdmZetaServerProvider.InitGlobales;
const
     K_SELECT_GLOBAL = 'select GL_FORMULA from GLOBAL where GL_CODIGO = :Codigo';
begin
     if (FQueryGlobal = nil) then
        FQueryGlobal := CreateQuery as TZADOQuery;
     PreparaQuery(FQueryGlobal, K_SELECT_GLOBAL);
end;

// Como es dentro de esta unidad, puedo saber el tipo Exacto
// del DataSet y no tengo por qu� usar los m�todos abstractos
// Esto nos da mayor velocidad.
// Al quitar los TQuerys, cambia ligeramente la implementaci�n
function TdmZetaServerProvider.LeeGlobal(const iCodigo: Integer): string;
begin
     if (FQueryGlobal = nil) then
        raise Exception.Create('Falta Llamar a InitGlobales antes de usar GetGlobal<x>');
     with FQueryGlobal do
     begin
          Active := FALSE;
          Parameters[0].Value := iCodigo;
          // Params[ 0 ].AsInteger := iCodigo;
          Active := TRUE;
          Result := Fields[0].AsString;
          Active := FALSE;
     end;
end;

function TdmZetaServerProvider.OK_ProyectoEspecial(const eProgEspecial: eProyectoEspecial): Boolean;
begin
     // Solo para version corporativa y para los ejecutables de Tress aplica esta validacion.
     {$IFDEF MSSQL}
     {$IFDEF TRESS}
     InitGlobales;
     Result := ZetaCommonTools.ProgramacionEspecial(eProgEspecial, GetGlobalString(K_GLOBAL_PROYECTOS_ESPECIALES))
     { and [sentinel].OK_ProyectoEspecial( eProgEspecial ) <-- A futuro se valida tambien contra el sentinel };
     {$ELSE}
     Result := FALSE; // Tengo que poner el FALSE en cada else, porque si no me marca un warning
     {$ENDIF}
     {$ELSE}
     Result := FALSE;
     {$ENDIF}
end;

{$IFDEF NEWCOMPARTE}

procedure TdmZetaServerProvider.RegistryInit;
const
     K_DUMMY = '********';
begin
     FRegistryOpened := FALSE;
     FComparte       := VarArrayOf([K_DUMMY, { Datos }
                                    K_DUMMY, { UserName }
                                    K_DUMMY, { Password }
                                    0]);     { # Usuario }
     FComparteAlias := K_DUMMY;
end;

procedure TdmZetaServerProvider.RegistryRead;
var
   FRegistry: TZetaRegistryServer;
begin
     if not FRegistryOpened then
     begin
          {$IFDEF DOS_CAPAS}
          FRegistry := TZetaRegistryServer.Create;
          {$ELSE}
          FRegistry := TZetaRegistryServer.Create(TRUE);
          {$ENDIF}
          try
             with FRegistry do
             begin
                  FComparte[P_DATABASE]  := Database;
                  FComparte[P_USER_NAME] := UserName;
                  FComparte[P_PASSWORD]  := Password;
                  FComparteAlias         := AliasComparte;
                  FSQLClientProvider     := SQLNCLIProvider;
             end;
          finally
                 FreeAndNil(FRegistry);
          end;
          FRegistryOpened := TRUE;
     end;
end;
{$ENDIF}

function TdmZetaServerProvider.GetComparte: Variant;
begin
     {$IFDEF NEWCOMPARTE}
     RegistryRead;
     Result := FComparte;
     {$ELSE}
     with FRegistry do
     begin
          Result := VarArrayOf([Database, UserName, Password, 0]);
     end;
     {$ENDIF}
end;

function TdmZetaServerProvider.GetSQLCLINProvider: string;
begin
     if strVacio(FSQLClientProvider) then
        RegistryRead;
     Result := FSQLClientProvider;
end;

function TdmZetaServerProvider.AliasComparte: string;
begin
     {$IFDEF NEWCOMPARTE}
     RegistryRead;
     Result := FComparteAlias;
     {$ELSE}
     with FRegistry do
     begin
          // Result := DataLinkName;
          Result := AliasComparte;
     end;
     {$ENDIF}
end;

procedure TdmZetaServerProvider.SetEmpresa(Empresa: Variant);
begin
     FEmpresa := Empresa;
     ConectaEmpresa(FEmpresa);
end;

function TdmZetaServerProvider.GetUsuario: Integer;
begin
     Result := FEmpresa[P_USUARIO];
end;

function TdmZetaServerProvider.GetCodigoEmpresaActiva: string;
begin
     Result := FEmpresa[P_CODIGO];
end;

function TdmZetaServerProvider.GetApplicationID: Integer;
begin
     try
        Result := FEmpresa[P_APPID];
     except
           on Error: Exception do
              Result := 0;
     end;
end;

procedure TdmZetaServerProvider.AsignaDataSetParams(oDataSet: TDataSet);
var
   oDatasetParams: TParameters;
   oDSParam : TParameter;
   oParam : TParam;
   i : Integer;
begin
     if (oDataSet is TZADOQuery) then
        oDatasetParams := TZADOQuery(oDataSet).Parameters
     else if (oDataSet is TADOStoredProc) then
        oDatasetParams := TADOStoredProc(oDataSet).Parameters
     else
         raise Exception.Create(Format('Componente Query / SP Invalido, Dataset: "%s" (Tipo incorrecto "%s")',
               [oDataSet.Name, oDataSet.ClassName]));

     // Barre la lista de par�metros del Dataset buscando
     // par�metros con el mismo nombre en FListaParams
     // Si existen, entonces asigna el Valor el param del Dataset
     with oDatasetParams do
     begin
          for i      := 0 to Count - 1 do
          begin
               oDSParam := Items[i];
               oParam   := FParamList.FindParam(oDSParam.Name);
               if (oParam <> nil) then
                  oDSParam.Value := oParam.Value;
          end;
     end;
end;

procedure TdmZetaServerProvider.AsignaDataSetOtros(oDataSet: TDataSet; oListaPares: Variant);
var
   oDatasetParams: TParameters;
   i, iElementos : Integer;
begin
     if (oDataSet is TZADOQuery) then
        oDatasetParams := TZADOQuery(oDataSet).Parameters
     else if (oDataSet is TADOStoredProc) then
        oDatasetParams := TADOStoredProc(oDataSet).Parameters
     else
         raise Exception.Create(Format('Componente Query / SP Invalido, Dataset: "%s" (Tipo incorrecto "%s")',
              [oDataSet.Name, oDataSet.ClassName]));

     if (VarIsArray(oListaPares)) then
     begin
          i          := VarArrayLowBound(oListaPares, 1);
          iElementos := VarArrayHighBound(oListaPares, 1);
          while (i < iElementos) do
          begin
               oDatasetParams.ParamByName(oListaPares[i]).Value := oListaPares[i + 1];
               // Pasa al siguiente 'par'
               i := i + 2;
          end;
     end;
end;

procedure TTablaInfo.SetMasterFields(Detalle: TADOTable);
var
   i : Integer;
   sCampoMaster, sMasterFields, sCampoDetail, sIndexFields: string;
begin
     sMasterFields := '';
     sIndexFields  := '';
     with FListaForeignKey do
          for i := 0 to Count - 1 do
          begin
               sCampoMaster := Strings[i];
               // Si se tiene un Par Name=Value
               if Pos('=', sCampoMaster) > 0 then
               begin
                    sCampoMaster := Names[i];
                    sCampoDetail := Values[sCampoMaster];
               end
               else
                   sCampoDetail := FListaKey.Strings[i];

               if (i < Count - 1) then
               begin
                    sCampoMaster := sCampoMaster + ';';
                    sCampoDetail := sCampoDetail + ';';
               end;
               sMasterFields := sMasterFields + sCampoMaster;
               sIndexFields  := sIndexFields + sCampoDetail;
          end;
     with Detalle do
     begin
          MasterFields    := sMasterFields;
          IndexFieldNames := sIndexFields;
     end;
end;

procedure TdmZetaServerProvider.GetDatosPeriodo;
const
     Q_DATOS_PERIODO = 'SELECT PE_YEAR, PE_TIPO, PE_NUMERO, ' + 'PE_FEC_INI, PE_FEC_FIN, PE_FEC_PAG, ' +
     {$IFDEF QUINCENALES}
       'PE_ASI_INI, PE_ASI_FIN, ' + 'TP_DIAS_BT, TP_DIAS_7, TP_DIAS_EV, TP_HORASJO, ' +
     {$ENDIF}
       'TP_TOT_EV1, TP_TOT_EV2, TP_TOT_EV3, TP_TOT_EV4, TP_TOT_EV5, ' + 'PE_DIAS, PE_USO, PE_STATUS, PE_MES, ' +
       'PE_INC_BAJ, PE_SOLO_EX, PE_AHORRO, PE_PRESTAM, ' + 'PE_PER_MES, PE_POS_MES, PE_DIAS_AC,PE_CAL, TP_CLAS, PE_MES_FON ' + 'FROM PERIODO ' +
     {$IFDEF QUINCENALES}
       'left outer join TPERIODO on TPERIODO.TP_TIPO = PERIODO.PE_TIPO ' +
     {$ENDIF}
       'WHERE PE_YEAR = %d AND PE_TIPO = %d ' + 'AND PE_NUMERO = %d ';
var
   FDataSetPeriodo: TDataSet;
begin
     if FParamList = nil then
        raise Exception.Create('No esta creada la lista de Par�metros')
     else if (not FPeriodoObtenido) then
     begin
          FDataSetPeriodo := CreateQuery;
          try
             if AbreQueryScript(FDataSetPeriodo, Format(Q_DATOS_PERIODO, [FParamList.ParamByName('Year').AsInteger,
                FParamList.ParamByName('Tipo').AsInteger, FParamList.ParamByName('Numero').AsInteger])) then
             begin
                  with FDatosPeriodo, FDataSetPeriodo do
                  begin
                       Year   := FieldByName('PE_YEAR').AsInteger;
                       Tipo   := eTipoPeriodo(FieldByName('PE_TIPO').AsInteger);
                       Clasifi:= eClasifiPeriodo(FieldByName('TP_CLAS').AsInteger);
                       Numero := FieldByName('PE_NUMERO').AsInteger;
                       Inicio := FieldByName('PE_FEC_INI').AsDateTime;
                       Fin    := FieldByName('PE_FEC_FIN').AsDateTime;
                       Pago   := FieldByName('PE_FEC_PAG').AsDateTime;
                       {$IFDEF QUINCENALES}
                       InicioAsis           := FieldByName('PE_ASI_INI').AsDateTime;
                       FinAsis              := FieldByName('PE_ASI_FIN').AsDateTime;
                       TipoDiasBase         := eTipoDiasBase(FieldByName('TP_DIAS_BT').AsInteger);
                       DiasBase             := FieldByName('TP_DIAS_7').AsFloat;
                       FormulaDias          := FieldByName('TP_DIAS_EV').AsString;
                       SumarJornadaHorarios := zStrToBool(FieldByName('TP_HORASJO').AsString);
                       {$ENDIF}
                       Dias                := FieldByName('PE_DIAS').AsInteger;
                       Status              := eStatusPeriodo(FieldByName('PE_STATUS').AsInteger);
                       Uso                 := eUsoPeriodo(FieldByName('PE_USO').AsInteger);
                       Mes                 := FieldByName('PE_MES').AsInteger;
                       MesFonacot          := FieldByName('PE_MES_FON').AsInteger;
                       SoloExcepciones     := zStrToBool(FieldByName('PE_SOLO_EX').AsString);
                       IncluyeBajas        := zStrToBool(FieldByName('PE_INC_BAJ').AsString);
                       DescuentaAhorros    := zStrToBool(FieldByName('PE_AHORRO').AsString);
                       DescuentaPrestamos  := zStrToBool(FieldByName('PE_PRESTAM').AsString);
                       PosMes              := FieldByName('PE_POS_MES').AsInteger;
                       PerMes              := FieldByName('PE_PER_MES').AsInteger;
                       DiasAcumula         := FieldByName('PE_DIAS_AC').AsInteger;
                       EsCalendario        := zStrToBool(FieldByName('PE_CAL').AsString);
                       EsUltimaNomina      := (Uso = upOrdinaria) and (PosMes = PerMes);
                       FormulaTotal1       := FieldByName('TP_TOT_EV1').AsString;
                       FormulaTotal2       := FieldByName('TP_TOT_EV2').AsString;
                       FormulaTotal3       := FieldByName('TP_TOT_EV3').AsString;
                       FormulaTotal4       := FieldByName('TP_TOT_EV4').AsString;
                       FormulaTotal5       := FieldByName('TP_TOT_EV5').AsString;
                       HayFuncionesTotales := strLleno(FormulaTotal1) or strLleno(FormulaTotal2) or strLleno(FormulaTotal3) or
                                              strLleno(FormulaTotal4) or strLleno(FormulaTotal5);
                  end;
                  FMesDefault      := FDatosPeriodo.Mes;
                  FPeriodoObtenido := TRUE;
             end
             else
                 raise Exception.Create('No existe periodo activo');
          finally
                 FreeAndNil(FDataSetPeriodo);
          end;
     end;
end;

procedure TdmZetaServerProvider.GetDatosImss;
begin
     if FParamList = nil then
        raise Exception.Create('No esta creada la lista de Par�metros')
     else
     begin
          with FDatosImss, FParamList do
          begin
               Patron := ParamByName('RegistroPatronal').AsString;
               Year   := ParamByName('ImssYear').AsInteger;
               Mes    := ParamByName('ImssMes').AsInteger;
               Tipo   := eTipoLiqIMSS(ParamByName('ImssTipo').AsInteger);
               { CV: Mientras no los requiera nadie, no se van a inicializar
                 Status := eStatusLiqIMSS(Fields[0].AsInteger);
                 Modifico := Fields[1].AsInteger;
               }
          end;
     end;
end;

procedure TdmZetaServerProvider.GetDatosActivos;
begin
     if FParamList = nil then
        raise Exception.Create('No esta creada la lista de Par�metros')
     else if FYearDefault = 0 then
        with FParamList do
        begin
             FFechaAsistencia := ParamByName('FechaAsistencia').AsDate;
             FFechaDefault    := ParamByName('FechaDefault').AsDate;
             FYearDefault     := ParamByName('YearDefault').AsInteger;
             FEmpleadoActivo  := ParamByName('EmpleadoActivo').AsInteger;
             FNombreUsuario   := ParamByName('NombreUsuario').AsString;
             FCodigoEmpresa   := ParamByName('CodigoEmpresa').AsString;
        end;
end;

procedure TdmZetaServerProvider.GetEncabPeriodo;
begin
     if FParamList = nil then
        raise Exception.Create('No esta creada la lista de Par�metros')
     else if FDatosPeriodo.Year = 0 then
        with FDatosPeriodo, FParamList do
        begin
             Year   := ParamByName('Year').AsInteger;
             Tipo   := eTipoPeriodo(ParamByName('Tipo').AsInteger);
             Numero := ParamByName('Numero').AsInteger;
        end;
end;

procedure TdmZetaServerProvider.InicializaValoresActivos;
begin
     FPeriodoObtenido   := FALSE;
     FYearDefault       := 0;
     FDatosPeriodo.Year := 0;
end;

procedure TdmZetaServerProvider.InicializaValoresNomINFO;
begin
     FreeAndNil(FCdsPeriodosNomInfo);

     with FDatosNomInfo do
     begin
          dInicialRangoNomInfo         := NullDateTime;
          dFinalRangoNomInfo           := NullDateTime;
          Empleado                     := 0;
          CuotaBimestralNomInfo        := 0;
          DiasCotizadosBimestreNomInfo := 0;
          SeguroViviendaNomInfo        := 0;
          iYearIMSS                    := 0;
          iMesIMSS                     := 0;
     end;
end;

procedure TdmZetaServerProvider.LiberaEmpleadosConAjuste;
begin
     FreeAndNil(FEmpleadosConAjuste);
end;

procedure TdmZetaServerProvider.AsignaParamList(oParams: OleVariant);
begin
     if FParamList = nil then
        FParamList := TZetaParams.Create
     else
         FParamList.Clear;
     InicializaValoresActivos;
     FParamList.VarValues := oParams;
end;

function TdmZetaServerProvider.DescripcionParams: string;
var
   i: Integer;
begin
     Result := '';
     with FParamList do
          for i := 0 to Count - 1 do
              with Items[i] do
                   Result := Result + name + '=' + AsString + CR_LF;
end;

procedure TdmZetaServerProvider.EmpiezaTransaccion;
var
   iLevel: Integer;
begin
     if not FConnection.InTransaction then // Transacciones anidadas no son soportadas en activex ADO
        iLevel := FConnection.BeginTrans;
end;

procedure TdmZetaServerProvider.TerminaTransaccion(lCommit: Boolean);
begin
     if FConnection.InTransaction then // Transacciones anidadas no son soportadas en activex ADO
        FConnection.CommitTrans;
end;

procedure TdmZetaServerProvider.RollBackTransaccion;
begin
     FConnection.RollBackTrans;
     if (Log <> nil) then
        Log.HuboRollBack;
end;

procedure TdmZetaServerProvider.AsignaParamsDataSet(Origen, Destino: TDataSet; const sExcluidos: string = '');
var
   i : Integer;
   sField: string;
begin
     with Origen do
     begin
          for i    := (FieldCount - 1) downto 0 do
          begin
               sField := Fields[i].FieldName;
               if (sExcluidos = '') or (Pos(sField, sExcluidos) = 0) then
                  AsignaParamCampo(GetParametro(Destino, sField), Fields[i]);
          end;
     end;
end;

procedure TdmZetaServerProvider.ActualizaTabla(OrigenDS: TDataSet; UpdateKind: TUpdateKind);
begin
     FTablaInfo.ConstruyeUpdateSQL(UpdateKind, OrigenDS, qryCambios);
     qryCambios.Execute;
end;

procedure TdmZetaServerProvider.adoComparteExecuteComplete(Connection: TADOConnection; RecordsAffected: Integer;
          const Error: Error; var EventStatus: TEventStatus; const Command: _Command; const Recordset: _Recordset);
begin
     {$ifdef SERVERCAFE}
     if Error <> Nil then
     begin
          if ( Error.SQLState = SQLSTATE_CONNECTION_NOT_OPEN ) or
             ( Error.SQLState = SQLSTATE_COMMUNICATION_LINK_FAILURE ) or
             ( Error.SQLState = SQLSTATE_CONNECTION_SUSPENDED_TRANSACTION_STATE ) or
             ( Error.SQLState = SQLSTATE_CONNECTION_TIMEOUT_EXPIRED ) then
             begin
                  Connection.Close ;
             end;
     end;
     {$endif}
end;

procedure TdmZetaServerProvider.adoEmpresaExecuteComplete(Connection: TADOConnection; RecordsAffected: Integer;
          const Error: Error; var EventStatus: TEventStatus; const Command: _Command; const Recordset: _Recordset);
begin
     {$ifdef SERVERCAFE}
     if Error <> Nil then
     begin
          if ( Error.SQLState = SQLSTATE_CONNECTION_NOT_OPEN ) or
             ( Error.SQLState = SQLSTATE_COMMUNICATION_LINK_FAILURE ) or
             ( Error.SQLState = SQLSTATE_CONNECTION_SUSPENDED_TRANSACTION_STATE ) or
             ( Error.SQLState = SQLSTATE_CONNECTION_TIMEOUT_EXPIRED ) then
             begin
                  Connection.Close ;
             end;
     end;
     {$endif}
end;

procedure TdmZetaServerProvider.prvUnicoUpdateData(Sender: TObject; DataSet: TCustomClientDataset);
begin
     with FTablaInfo do
          if Assigned(OnUpdateData) then
             OnUpdateData(Sender, DataSet);

     { No funciona porque se altera el ORDEN del ClientDataSet
       oCampo := DataSet.FindField( 'qryDetail') as TDataSetField;
       if (oCampo <> nil) then
       with oCampo.NestedDataSet do
       while not EOF do begin
       Edit;
       FieldByName('TB_CODIGO').AsString := DataSet.FieldByName('TB_CODIGO').AsString;
       Post;
       Next;
       end;
     }
end;

procedure TdmZetaServerProvider.prvUnicoBeforeUpdateRecord(Sender: TObject; SourceDS: TDataSet; DeltaDS: TCustomClientDataset;
          UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     // Llama al BeforeUpdateRecord del programador
     with FTablaInfo do
     begin
          if Assigned(BeforeUpdateRecord) then
             BeforeUpdateRecord(Sender, SourceDS, DeltaDS, UpdateKind, Applied);

          if (not Applied) then
             UpdateTabla(Sender, SourceDS, DeltaDS, UpdateKind, Applied);
     end;
end;

{ NOTA: Esto se tiene que hacer porque el ADO regresa los campos
  tipo CHAR con espacios a la derecha, a diferencia de los VARCHAR.
  Con el BDE esto no sucede.
  Se tiene que barrer todo el DataSet para hacerle un TRIM a los
  campos tipo 'String' identificados como 'FixedChar' }
procedure TdmZetaServerProvider.prvUnicoGetData(Sender: TObject; DataSet: TCustomClientDataset);
var
   i : Integer;
   oField : TField;
   oListaFixed: TList;
begin
     oListaFixed := TList.Create;
     with DataSet do
     begin
          for i := FieldCount - 1 downto 0 do
          begin
               oField := Fields[i];
               // Quitar los espacios a la derecha solamente a los Char(N)
               if (oField.DataType = ftString) and TStringField(oField).FixedChar then
                 oListaFixed.Add(oField);
          end;
          if (oListaFixed.Count > 0) then
          begin
               while not EOF do
               begin
                    Edit;
                    for i := oListaFixed.Count - 1 downto 0 do
                        with TField(oListaFixed.Items[i]) do
                             AsString := TrimRight(AsString);
                    Post;
                    Next;
               end;
          end;
     end;
     oListaFixed.Free;
end;

procedure TdmZetaServerProvider.prvUnicoAfterUpdateRecord(Sender: TObject; SourceDS: TDataSet; DeltaDS: TCustomClientDataset;
          UpdateKind: TUpdateKind);
begin
     with FTablaInfo do
     begin
          if Assigned(AfterUpdateRecord) then
             AfterUpdateRecord(Sender, SourceDS, DeltaDS, UpdateKind)
     end;
end;

procedure TdmZetaServerProvider.prvUnicoUpdateError(Sender: TObject; DataSet: TCustomClientDataset; E: EUpdateError;
          UpdateKind: TUpdateKind; var Response: TResolverResponse);
begin
     with FTablaInfo do
          if Assigned(OnUpdateError) then
             OnUpdateError(Sender, DataSet, E, UpdateKind, Response);
end;

procedure TdmZetaServerProvider.qryMasterAfterOpen(DataSet: TDataSet);
var
   i: Integer;
begin
     with DataSet do
     begin
          for i := FieldCount - 1 downto 0 do
          begin
               if Fields[i].ReadOnly then
                  Fields[i].ReadOnly := FALSE;
          end;
     end;
end;

procedure TdmZetaServerProvider.BorraCatalogo(const sMensaje: TBitacoraTexto; const eClase: eClaseBitacora; DataSet: TDataSet;
          const iEmpleado: Integer; const sMasInfo: string);
begin
     EscribeBitacora(tbNormal, eClase, iEmpleado, NullDateTime, 'Borrado de ' + sMensaje, sMasInfo + GetDatasetInfo(DataSet, TRUE));
end;

procedure TdmZetaServerProvider.CambioCatalogo(const sMensaje: TBitacoraTexto; const eClase: eClaseBitacora; DataSet: TDataSet;
          const iEmpleado: Integer; const sMasInfo: string);
begin
     EscribeBitacora(tbNormal, eClase, iEmpleado, NullDateTime, 'Modificaci�n de ' + sMensaje, sMasInfo + GetDatasetInfo(DataSet, FALSE));
end;

procedure TdmZetaServerProvider.EscribeBitacora(const eTipo: eTipoBitacora; const eClase: eClaseBitacora;
          const iEmpleado: TNumEmp; dMovimiento: TDate; const sMensaje: TBitacoraTexto; const sTexto: string);
begin
     InitGlobales;
     if ZetaServerTools.RegistrarBitacora(eClase, GetGlobalString(K_GLOBAL_GRABA_BITACORA)) then
     begin
          if (FQryBitacora = nil) then
             FQryBitacora := CreateQuery(K_WRITE_SIMPLE_LOG);
          EscribeParametrosBitacora(FQryBitacora, Ord(eTipo), Ord(eClase), iEmpleado, dMovimiento, sMensaje, sTexto, UsuarioActivo);
     end;
end;

procedure TdmZetaServerProvider.CambioCatalogoComparte(const sMensaje: TBitacoraTexto; const eClase: eClaseSistBitacora;
          DataSet: TDataSet; const iUsuario: Integer);
begin
     EscribeBitacoraComparte(tbNormal, eClase, NullDateTime, 'Modificaci�n de ' + sMensaje, GetDatasetInfo(DataSet, FALSE), iUsuario);
end;

procedure TdmZetaServerProvider.BorraCatalogoComparte(const sMensaje: TBitacoraTexto; const eClase: eClaseSistBitacora;
          DataSet: TDataSet; const iUsuario: Integer);
begin
     EscribeBitacoraComparte(tbNormal, eClase, NullDateTime, 'Borrado de ' + sMensaje, GetDatasetInfo(DataSet, TRUE), iUsuario);
end;

procedure TdmZetaServerProvider.EscribeBitacoraComparte(const eTipo: eTipoBitacora; const eClase: eClaseSistBitacora;
          dMovimiento: TDate; const sMensaje: TBitacoraTexto; const sTexto: string; const iUsuario: Integer);
begin
     if (FQryBitacoraSistema = nil) then
        FQryBitacoraSistema := CreateQuery(K_WRITE_SIMPLE_LOG);
     EscribeParametrosBitacora(FQryBitacoraSistema, Ord(eTipo), Ord(eClase), 0, dMovimiento, sMensaje, sTexto, iUsuario);
end;

procedure TdmZetaServerProvider.EscribeBitacoraKiosco(const eTipo: eTipoBitacora; const sEmpresa: string;
          const eClase: eClaseBitacora; const sTexto, sMarco, sKiosco: string; const iEmpleado: Integer);
begin
     InitGlobales;
     if ZetaServerTools.RegistrarBitacora(eClase, GetGlobalString(K_GLOBAL_GRABA_BITACORA)) then
     begin
          FQryBitacoraKiosco := CreateQuery(K_AGREGA_BITACORA_KIOSCO);
          try
             ParamAsInteger(FQryBitacoraKiosco, 'CB_CODIGO', iEmpleado);
             ParamAsVarChar(FQryBitacoraKiosco, 'CM_CODIGO', sEmpresa, K_ANCHO_OPERACION);
             ParamAsInteger(FQryBitacoraKiosco, 'BI_TIPO', Ord(eTipo));
             ParamAsInteger(FQryBitacoraKiosco, 'BI_ACCION', Ord(eClase));
             ParamAsInteger(FQryBitacoraKiosco, 'BI_NUMERO', 0);
             ParamAsVarChar(FQryBitacoraKiosco, 'BI_TEXTO', sTexto, K_ANCHO_FORMULA);
             ParamAsVarChar(FQryBitacoraKiosco, 'BI_UBICA', sMarco, K_ANCHO_OBSERVACIONES);
             ParamAsVarChar(FQryBitacoraKiosco, 'BI_KIOSCO', sKiosco, K_ANCHO_COMPUTERNAME);
             ParamAsDate(FQryBitacoraKiosco, 'BI_FEC_MOV', NullDateTime);
             Ejecuta(FQryBitacoraKiosco);
          except
                raise;
          end;
     end;
end;

procedure TdmZetaServerProvider.EscribeParametrosBitacora(oQryInsert: TDataSet; const iTipo: Integer; const iClase: Integer;
          const iEmpleado: TNumEmp; dMovimiento: TDate; const sMensaje: TBitacoraTexto; const sTexto: string; const iUsuario: Integer);
var
   dBitacora: TDateTime;
   sData    : string;
begin
     try
        dBitacora := Now;
        sData     := sTexto;
        if strVacio(sData) then
           sData := ' '; // Si no marca error al grabar el Blob
        if (dMovimiento = 0) then
           dMovimiento := dBitacora;

        ParamAsInteger(oQryInsert, 'US_CODIGO', iUsuario);
        ParamAsDate(oQryInsert, 'BI_FECHA', Trunc(dBitacora));
        ParamAsVarChar(oQryInsert, 'BI_HORA', TimeToStrSQL(dBitacora), K_ANCHO_LOG_HORA);
        ParamAsInteger(oQryInsert, 'BI_TIPO', iTipo);
        ParamAsInteger(oQryInsert, 'BI_CLASE', iClase);
        ParamAsVarChar(oQryInsert, 'BI_TEXTO', Copy(sMensaje, 1, K_ANCHO_LOG_TEXTO), K_ANCHO_LOG_TEXTO);
        ParamAsInteger(oQryInsert, 'CB_CODIGO', iEmpleado);
        ParamAsDate(oQryInsert, 'BI_FEC_MOV', dMovimiento);
        ParamAsBlob(oQryInsert, 'BI_DATA', sData);
        Ejecuta(oQryInsert);
     except
           raise;
     end;
end;

function TdmZetaServerProvider.GetDatasetInfo(DataSet: TDataSet; const lBorrar: Boolean; sConfidenciales: string): string;
var
   i : Integer;
   sCampo, sOld, sNew: string;
begin
  Result := '';
  with DataSet do
    for i := 0 to FieldCount - 1 do begin
      if (DataSet.Fields[i].DataType <> ftDataset) then begin
        sOld := ZetaServerTools.CampoOldAsVar(Fields[i]);
        sNew := ZetaServerTools.CampoAsVar(Fields[i]);
        with Fields[i] do
          if (lBorrar) or (not IsNull and (sNew <> sOld)) then begin
            if strLleno(sConfidenciales) then begin
              if (Pos(Fields[i].FieldName, sConfidenciales) > 0) then begin
                sNew := Replicate('*', Length(sNew));
                sOld := Replicate('*', Length(sOld));
              end;
            end;
            // (JB) Se realiza la validacion que el Delta no traiga un campo llave
            // Ya que en el detail de tablas se realiza un select * from y es
            // posible que traiga campo llave. Fue Validado con ER.
            if (FieldName <> 'LLAVE') then begin
              if (lBorrar) then
                sCampo := FieldName + ' = ' + sOld
              else
                sCampo := FieldName + CR_LF + ' De: ' + sOld + CR_LF + ' A : ' + sNew;
              Result   := Result + sCampo + CR_LF;
            end;
          end;
      end;
    end;
end;

function TdmZetaServerProvider.NombreNivel(const Index: Integer): string;
begin
     Result := FNombreNiveles[index];
end;

function TdmZetaServerProvider.NumNiveles: Integer;
const
     // K_GLOBAL_NIVEL1  = 13; { Nombre de Nivel #1}
     // K_GLOBAL_NIVEL9  = 21; { Nombre de Nivel #9}

     {$IFDEF ACS}
     Q_GET_NIVELES =
       'select GL_FORMULA from GLOBAL where ( GL_CODIGO >= 13 and GL_CODIGO <= 21 ) and ( GL_CODIGO >= 234 and GL_CODIGO <= 236) order by GL_CODIGO';
     {$ELSE}
     Q_GET_NIVELES = 'select GL_FORMULA from GLOBAL where GL_CODIGO >= 13 and GL_CODIGO <= 21 order by GL_CODIGO';
     {$ENDIF}
var
   oQueryNiveles: TDataSet;
   sNivel : string;
begin
     if (FNumeroNiveles = 0) then
     begin
          oQueryNiveles := CreateQuery(Q_GET_NIVELES);
          try
             with oQueryNiveles do
             begin
                  Active := TRUE;
                  while not EOF do
                  begin
                       sNivel := Fields[0].AsString;
                       if strLleno(sNivel) then
                       begin
                            Inc(FNumeroNiveles);
                            FNombreNiveles[FNumeroNiveles] := sNivel;
                       end
                       else
                           break;
                       Next;
                  end;
             end;
          finally
                 FreeAndNil(oQueryNiveles);
          end;
     end;
     Result := FNumeroNiveles;
end;

function TdmZetaServerProvider.GetCodigoGrupo: Integer;
begin
     Result := FEmpresa[P_GRUPO];
end;

{ M�todos para llenar el arrego de aTipoPeriodo y aTipoNomina }

{ acl }
function TdmZetaServerProvider.GetTiposPeriodo(Empresa: Variant): OleVariant;
begin
     Result := OpenSQL(Empresa, 'select TP_TIPO, TP_NOMBRE, TP_DESCRIP, TP_NIVEL0, TP_CLAS from TPERIODO', TRUE)
end;

//Ocupamos la lista de objetos y la lista de strings para los combos que devuelven la descripcion.
procedure TdmZetaServerProvider.InitArregloTPeriodo;
var
   sValorLista: String;
   iPos: Integer;
begin
     { Inicializa Lista Tipo Periodo: Para llamar este m�todo la propiedad de
       Empresa tiene que estar asignada y tiene que ser una empresa de Tress. }

     with TClientDataset.Create(Self) do
     begin
          try
             Data := GetTiposPeriodo(FEmpresa);
             if not Assigned( FArregloPeriodo ) then FArregloPeriodo := TStringList.Create;
             if not Assigned( FArregloTipoNomina ) then FArregloTipoNomina := TStringList.Create;
             FArregloPeriodo.BeginUpdate;
             FArregloTipoNomina.BeginUpdate;

             FArregloPeriodo.Clear;
             FArregloTipoNomina.Clear;

             while not EOF do
             begin
                  sValorLista:= FieldByName('TP_DESCRIP').AsString;
                  iPos:= Pos ( '/', sValorLista );

                  //Llena lista del periodo
                  if ( ( iPos > 0 ) and ( sValorLista[iPos+1] <> '' ) and ( sValorLista[iPos-1] <> '' ) ) then
                       FArregloPeriodo.Add(FieldByName('TP_TIPO').AsString + '=' + Copy( sValorLista, 1, iPos - 1 ))   //Ejemplo: 1 = Semanal
                  else
                       FArregloPeriodo.Add(FieldByName('TP_TIPO').AsString + '=' + sValorLista);

                  //Llena lista de la nomina
                  FArregloTipoNomina.Add(FieldByName('TP_TIPO').AsString + '=' + Copy( sValorLista, iPos + 1, Length ( sValorLista )));  //Ejemplo: 1 = Semana
                  Next;
             end;
          finally
                 Free; // Liberar el ClientDataSet creado
          end;
     end;
end;

procedure TdmZetaServerProvider.SetReadUncommited;
begin
     Self.adoEmpresa.IsolationLevel := ilReadUncommitted;
  // Sirve para ejecutar queries sobre registros que no todavia no tienen el Commit.
end;

{ TParameterHelper }

function TParameterHelper.AsInteger: Integer;
begin
     Result := Integer(Self.Value);
end;

function TParameterHelper.AsDate: TDate;
begin
     Result := TDate(Self.Value);
end;

function TParameterHelper.AsDateTime: TDateTime;
begin
     Result := TDateTime(Self.Value);
end;

function TParameterHelper.AsString: string;
const
     adFldFixed = $10;
begin
     Result := TrimRight(string(Self.Value))
end;

function TParameterHelper.AsBoolean: Boolean;
begin
     Result := Boolean(Self.Value);
end;

function TParameterHelper.AsSingle: Single;
begin
     Result := Single(Self.Value);
end;

function TParameterHelper.AsFloat: double;
begin
     Result := double(Self.Value);
end;

function TParameterHelper.AsByte: Integer;
begin
     Result := Integer(Self.Value);
end;

function TParameterHelper.AsSmallInt: Integer;
begin
     Result := Integer(Self.Value);
end;

function TParameterHelper.AsShortInt: Integer;
begin
     Result := Integer(Self.Value);
end;

function TParameterHelper.AsBCD: Currency;
begin
     Result := Currency(Self.Value);
end;

function TParameterHelper.AsCurrency: Currency;
begin
     Result := Currency(Self.Value);
end;

end.

