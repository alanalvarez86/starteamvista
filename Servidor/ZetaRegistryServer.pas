unit ZetaRegistryServer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Registry, ADODB, ADOInt, OleDB, DB,
  ComObj;

const
  {$IFDEF PORTAL}
  {$IFDEF INTERBASE}
  SERVER_REGISTRY_PATH = 'Software\Grupo Tress\Portal\Interbase';
  {$ENDIF}
  {$IFDEF MSSQL}
  SERVER_REGISTRY_PATH = 'Software\Grupo Tress\Portal\ServerMSSQL';
  {$ENDIF}
  {$ELSE}
  {$IFDEF INTERBASE}
  SERVER_REGISTRY_PATH = 'Software\Grupo Tress\TressWin\Server';
  {$ENDIF}
  {$IFDEF MSSQL}
  SERVER_REGISTRY_PATH = 'Software\Grupo Tress\TressWin\ServerMSSQL';
  {$ENDIF}
  {$ENDIF}
  SERVER_ALIAS_COMPARTE     = 'AliasName';
  SERVER_BINARIOS           = 'Binaries';
  SERVER_INSTALLATION       = 'Installation';
  SERVER_PASSWORD           = 'Password';
  SERVER_USERNAME           = 'UserName';
  SERVER_DATABASE           = 'DataBase';
  SERVER_T_LOGIN            = 'TipoLogin';
  SERVER_CHECKSUM           = 'Checksum';
  SERVER_PROVIDER           = 'Provider';
  SERVER_PATH_CONFIG        = 'PathConfig';
  SERVER_PATH_SERVER_CONFIG = 'PathServerConfig';
  SERVER_SERVER_DB          = 'ServerDB';
  SERVER_DATABASE_ADO       = 'DataBaseADO';
  SERVER_PATH_MSSQL         = 'PathMSSQL';
  SERVER_WS3DT_URL          = 'WS3DTURL';

  // Declarado el mas nuevo primero
  SQLNCLIs: array[0..2] of string = ('SQLNCLI11', 'SQLNCLI10', 'SQLNCLI');

type
  TZetaRegistryServer = class(TObject)
  private
    { Private declarations }
    FRegistry: TRegIniFile;
    FCanWrite: Boolean;
    function GetAliasComparte: String;
    function GetBinarios: String;
    function GetInstallation: String;
    function GetPassword: String;
    function GetPasswordRaw: String;
    function GetUserName: String;
    function GetDatabase: String;
    function GetTipoLogin: Integer;
    function GetChecksum: String;
    function GetChecksumRaw: String;
    function GetPathConfig: String;
    function GetPathServerConfig: String;
    function GetSQLNCLIProvider: String;
    function GetServer: String;
    function GetDataBaseADO: String;
    function GetPathMSSQL: String;
    function GetWS3DTURL: String;
    procedure SetAliasComparte(const Value: String);
    procedure SetBinarios(const Value: String);
    procedure SetPassword(const Value: String);
    procedure SetPasswordRaw(const Value: String);
    procedure SetUserName(const Value: String);
    procedure SetDatabase(const Value: String);
    procedure SetTipoLogin(const Value: Integer);
    procedure SetChecksum(const Value: String);
    procedure SetChecksumRaw(const Value: String);
    procedure SetSQLNCLIProvider(const Value: String);
    procedure SetPathConfig(const Value: String);
    procedure SetPathServerConfig(const Value: String);
    procedure SetServer(const Value: String);
    procedure SetDataBaseADO(const Value: String);
    procedure SetPathMSSQL(const Value: String);
    procedure SetWS3DTURL(const Value: String);

  protected
    { Protected declarations }
    property Registry: TRegIniFile read FRegistry;
  public
    { Public declarations }
    constructor Create(const lReadWrite: Boolean = TRUE); virtual;
    destructor Destroy; override;
    property CanWrite       : Boolean read FCanWrite;
    property AliasComparte  : String  read GetAliasComparte   write SetAliasComparte;
    property Binarios       : String  read GetBinarios        write SetBinarios;
    property Installation   : String  read GetInstallation;
    property Password       : String  read GetPassword        write SetPassword;
    property PasswordRaw    : String  read GetPasswordRaw     write SetPasswordRaw;
    property UserName       : String  read GetUserName        write SetUserName;
    property Database       : string  read GetDatabase        write SetDatabase;
    property TipoLogin      : Integer read GetTipoLogin       write SetTipoLogin;
    property Checksum       : String  read GetChecksum        write SetChecksum;
    property ChecksumRaw    : String  read GetChecksumRaw     write SetChecksumRaw;
    property SQLNCLIProvider: String  read GetSQLNCLIProvider write SetSQLNCLIProvider;
    property PathConfig     : String  read GetPathConfig      write SetPathConfig;
    property PathServerConfig     : String  read GetPathServerConfig      write SetPathServerConfig;
    property ServerDB       : String  read GetServer          write SetServer;
    property DataBaseADO    : String  read GetDataBaseADO     write SetDataBaseADO;
    property PathMSSQL     : String  read GetPathMSSQL write SetPathMSSQL;
    property WS3DTURL     : String  read GetWS3DTURL write SetWS3DTURL;

    function IsValid        : Boolean;
    function IsValidAlias   : Boolean;
    function IsKeyExists(key: string)   : Boolean;
    function GetADOVersion: string;
    function GetFileVersion(FileName: string): string;
  end;

implementation

uses
  ZetaServerTools, ZetaCommonTools;

{ ************ TZetaRegistryServer *********** }

constructor TZetaRegistryServer.Create(const lReadWrite: Boolean);
{$IFDEF VER150}
const
  // Constants defined by Microsoft:
  KEY_WOW64_32KEY = $0200;
  KEY_WOW64_64KEY = $0100;
  //SERVER_REGISTRY_PATH = '';
{$ENDIF}
begin
  inherited Create;
  //KEY_WOW64_32KEY: Redirecci�n incondicional al registro de windows de 32 bits, no importa si el
  //                 programa es de 32 o 64 bits
  FRegistry := TRegIniFile.Create(KEY_ALL_ACCESS or KEY_WOW64_32KEY);
  with FRegistry do begin
    RootKey := HKEY_LOCAL_MACHINE;
    if lReadWrite then
      FCanWrite := OpenKey(SERVER_REGISTRY_PATH, TRUE)
    else
      FCanWrite := False;
    if not FCanWrite then
      OpenKeyReadOnly(SERVER_REGISTRY_PATH);
    // LazyWrite := False;          Comentado el 10/10/2002 - Para evitar que dispare evento RegFlushKey
  end;
end;

destructor TZetaRegistryServer.Destroy;
begin
  FRegistry.Free;
  inherited;
end;

function TZetaRegistryServer.IsValid: Boolean;
begin
  with FRegistry do begin
    Result := ValueExists(SERVER_DATABASE) and ValueExists(SERVER_USERNAME) and
      ValueExists(SERVER_PASSWORD);
  end;
end;

function TZetaRegistryServer.IsValidAlias: Boolean;
begin
  with FRegistry do begin
    Result := ValueExists(SERVER_ALIAS_COMPARTE) and ValueExists(SERVER_USERNAME) and
      ValueExists(SERVER_PASSWORD);
  end;
end;

function TZetaRegistryServer.IsKeyExists(key : String): Boolean;
begin
  with FRegistry do begin
    Result :=  ValueExists(key);
  end;
end;

function TZetaRegistryServer.GetAliasComparte: String;
begin
  Result := FRegistry.ReadString('', SERVER_ALIAS_COMPARTE, '');
end;

function TZetaRegistryServer.GetBinarios: String;
begin
  Result := ZetaCommonTools.VerificaDir(FRegistry.ReadString('', SERVER_BINARIOS,
      ExtractFilePath(Application.ExeName)));
end;

function TZetaRegistryServer.GetInstallation: String;
begin
  Result := ZetaCommonTools.VerificaDir(FRegistry.ReadString('', SERVER_INSTALLATION,
      ExtractFilePath(Application.ExeName)));
end;

function TZetaRegistryServer.GetDatabase: String;
begin
  Result := FRegistry.ReadString('', SERVER_DATABASE, '');
end;

function TZetaRegistryServer.GetDataBaseADO: String;
begin
     Result := FRegistry.ReadString('', SERVER_DATABASE_ADO, '');
end;

function TZetaRegistryServer.GetUserName: String;
begin
  Result := FRegistry.ReadString('', SERVER_USERNAME, '');
end;

function TZetaRegistryServer.GetPassword: String;
begin
  Result := ZetaServerTools.Decrypt(GetPasswordRaw);
end;

function TZetaRegistryServer.GetPasswordRaw: String;
begin
  Result := FRegistry.ReadString('', SERVER_PASSWORD, '');
end;

function TZetaRegistryServer.GetPathConfig: String;
begin
    Result := FRegistry.ReadString('', SERVER_PATH_CONFIG, '');
end;

function TZetaRegistryServer.GetPathMSSQL: String;
begin
    Result := FRegistry.ReadString('', SERVER_PATH_MSSQL, '');
end;

function TZetaRegistryServer.GetWS3DTURL: String;
begin
    Result := FRegistry.ReadString('', SERVER_WS3DT_URL, '');
end;

function TZetaRegistryServer.GetPathServerConfig: String;
begin
    Result := FRegistry.ReadString('', SERVER_PATH_SERVER_CONFIG, '');
end;

function TZetaRegistryServer.GetTipoLogin: Integer;
begin
  Result := FRegistry.ReadInteger('', SERVER_T_LOGIN, 0);
end;

procedure TZetaRegistryServer.SetAliasComparte(const Value: String);
begin
  FRegistry.WriteString('', SERVER_ALIAS_COMPARTE, Value);
end;

procedure TZetaRegistryServer.SetBinarios(const Value: String);
begin
  FRegistry.WriteString('', SERVER_BINARIOS, ZetaCommonTools.VerificaDir(Value));
end;

procedure TZetaRegistryServer.SetDatabase(const Value: String);
begin
  FRegistry.WriteString('', SERVER_DATABASE, Value);
end;

procedure TZetaRegistryServer.SetDataBaseADO(const Value: String);
begin
     FRegistry.WriteString('', SERVER_DATABASE_ADO , Value);
end;

procedure TZetaRegistryServer.SetUserName(const Value: String);
begin
  FRegistry.WriteString('', SERVER_USERNAME, Value);
end;

procedure TZetaRegistryServer.SetPassword(const Value: String);
begin
  SetPasswordRaw(ZetaServerTools.Encrypt(Value));
end;

procedure TZetaRegistryServer.SetPasswordRaw(const Value: String);
begin
  FRegistry.WriteString('', SERVER_PASSWORD, Value);
end;

procedure TZetaRegistryServer.SetPathConfig(const Value: String);
begin
      FRegistry.WriteString('', SERVER_PATH_CONFIG , Value);
end;

procedure TZetaRegistryServer.SetPathServerConfig(const Value: String);
begin
      FRegistry.WriteString('', SERVER_PATH_SERVER_CONFIG , Value);
end;

procedure TZetaRegistryServer.SetTipoLogin(const Value: Integer);
begin
  FRegistry.WriteInteger('', SERVER_T_LOGIN, Value);
end;

procedure TZetaRegistryServer.SetPathMSSQL(const Value: String);
begin
      FRegistry.WriteString('', SERVER_PATH_MSSQL , Value);
end;

procedure TZetaRegistryServer.SetWS3DTURL(const Value: String);
begin
      FRegistry.WriteString('', SERVER_WS3DT_URL , Value);
end;

function TZetaRegistryServer.GetChecksum: String;
begin
  Result := ZetaServerTools.DecryptDB(GetChecksumRaw);
end;

procedure TZetaRegistryServer.SetChecksum(const Value: String);
begin
  SetChecksumRaw(ZetaServerTools.EncryptDB(Value));
end;

function TZetaRegistryServer.GetChecksumRaw: String;
begin
  Result := FRegistry.ReadString('', SERVER_CHECKSUM, '');
end;

procedure TZetaRegistryServer.SetChecksumRaw(const Value: String);
begin
  FRegistry.WriteString('', SERVER_CHECKSUM, Value);
end;

function TZetaRegistryServer.GetServer: String;
begin
     Result := FRegistry.ReadString('', SERVER_SERVER_DB, '');
end;

function TZetaRegistryServer.GetFileVersion(FileName: string): string;
var
  VerInfoSize: DWORD;
  VerInfo: Pointer;
  VerValueSize: DWORD;
  VerValue: PVSFixedFileInfo;
  Dummy: DWORD;
begin
  VerInfoSize := GetFileVersionInfoSize({$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(FileName), Dummy);
  GetMem(VerInfo, VerInfoSize);
  GetFileVersionInfo({$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(FileName), 0, VerInfoSize, VerInfo);
  VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
  with VerValue^ do
  begin
    Result := IntToStr(dwFileVersionMS shr 16);
    Result := Result + '.' + IntToStr(dwFileVersionMS and $FFFF);
    Result := Result + '.' + IntToStr(dwFileVersionLS shr 16);
    Result := Result + '.' + IntToStr(dwFileVersionLS and $FFFF);
  end;
  FreeMem(VerInfo, VerInfoSize);
end;

function TZetaRegistryServer.GetADOVersion: string;
var
ADO: OLEVariant;
begin
try
ADO := CreateOLEObject('adodb.connection');
Result := ADO.Version;
except
Result := 'No se encuentra versi�n';
end;
end;

function TZetaRegistryServer.GetSQLNCLIProvider: String;
var
  Providers  : TStringList;
  iCount     : Integer;
  sClientProv: string;
begin
  // Recuperar el cliente nativo prederminado
  Result := FRegistry.ReadString('', SERVER_PROVIDER, '');
  if Result <> '' then Exit;

  // Obtener la lista de proveedores instalados
  Providers := TStringList.Create;
  GetProviderNames(Providers);
  Providers.Sort;

  // Buscar los clientes preferido de TRESS
  for iCount := Low(SQLNCLIs) to High(SQLNCLIs) do begin
    sClientProv := SQLNCLIs[iCount];
    if Providers.IndexOf(sClientProv) <> -1 then begin
      Result := sClientProv;
      Break;
    end;
  end;

  if Result = '' then
    // No hay cliente nativo conocido, buscar el mas reciente de los instalados
    for iCount := Providers.Count - 1 downto 0 do begin
      sClientProv := Providers[iCount];
      if Pos('SQLNCLI', Trim(sClientProv)) = 1 then begin
        Result := sClientProv;
        Break;
      end;
    end;
  FreeAndNil(Providers);

  if Result = '' then
    // ToDo: No se encontr� cliente nativo, que hay qu� hacer? Excepci�n? Selecci�n del usuario?
  else if CanWrite then
    FRegistry.WriteString('', SERVER_PROVIDER, Result);
end;

procedure TZetaRegistryServer.SetServer(const Value: String);
begin
     FRegistry.WriteString('', SERVER_SERVER_DB, Value);
end;

procedure TZetaRegistryServer.SetSQLNCLIProvider(const Value: String);
begin
  FRegistry.WriteString('', SERVER_PROVIDER, Value);
end;

end.
