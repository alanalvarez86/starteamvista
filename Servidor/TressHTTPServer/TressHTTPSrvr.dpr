library TressHTTPSrvr;

uses
  WebBroker,
  ISAPIApp,
  TressHTTPIntr in 'TressHTTPIntr.pas' {TressHTTPServer: TWebModule};

{$R *.RES}

exports
  GetExtensionVersion,
  HttpExtensionProc,
  TressHTTPIntr.TerminateExtension;

begin
  Application.Initialize;
  Application.CreateForm(TTressHTTPServer, TressHTTPServer);
  Application.Run;
end.
